local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local QnSelectableButton=import "L10.UI.QnSelectableButton"

CLuaQMPKJingCaiConfirmWnd=class()
RegistClassMember(CLuaQMPKJingCaiConfirmWnd,"m_YinLiangLabel")
RegistClassMember(CLuaQMPKJingCaiConfirmWnd,"m_DaiBiLabel")
RegistClassMember(CLuaQMPKJingCaiConfirmWnd,"m_ValueChoice")
RegistClassMember(CLuaQMPKJingCaiConfirmWnd,"m_ValueLabel")
RegistClassMember(CLuaQMPKJingCaiConfirmWnd,"m_Choice")
RegistClassMember(CLuaQMPKJingCaiConfirmWnd,"m_YinLiang")
RegistClassMember(CLuaQMPKJingCaiConfirmWnd,"m_All")
RegistClassMember(CLuaQMPKJingCaiConfirmWnd,"m_Slider")
RegistClassMember(CLuaQMPKJingCaiConfirmWnd,"m_XiaZhuValue")

RegistClassMember(CLuaQMPKJingCaiConfirmWnd,"m_DescLabel")
RegistClassMember(CLuaQMPKJingCaiConfirmWnd,"m_ChoiceButtons")


function CLuaQMPKJingCaiConfirmWnd:Init()
    self.m_XiaZhuValue=0

    local titleLabel=FindChild(self.transform,"TitleLabel"):GetComponent(typeof(UILabel))
    local type={0,1,2,1,1,1,2,1}

    local jingcaiData=QuanMinPK_JingCai.GetData(CLuaQMPKMgr.m_JingCaiIndex)
    local lookup = {0, 1, 1, 2, 3, 4, 4, 5}

    if type[CLuaQMPKMgr.m_JingCaiIndex]==0 then--总胜负
        titleLabel.text=SafeStringFormat3( LocalString.GetString("下注：%s获胜"),CLuaQMPKMgr.m_SelectIndex==1 and jingcaiData.Choice1 or jingcaiData.Choice2)
    elseif type[CLuaQMPKMgr.m_JingCaiIndex]==1 then--胜负
        titleLabel.text=SafeStringFormat3( LocalString.GetString("下注：第%s局%s获胜"),lookup[CLuaQMPKMgr.m_JingCaiIndex],CLuaQMPKMgr.m_SelectIndex==1 and jingcaiData.Choice1 or jingcaiData.Choice2)
    elseif type[CLuaQMPKMgr.m_JingCaiIndex]==2 then--人头差
        titleLabel.text=SafeStringFormat3( LocalString.GetString("下注：第%s局人头差%s"),lookup[CLuaQMPKMgr.m_JingCaiIndex],CLuaQMPKMgr.m_SelectIndex==1 and jingcaiData.Choice1 or jingcaiData.Choice2)
    end

    self.m_DescLabel=FindChild(self.transform,"DescLabel"):GetComponent(typeof(UILabel))
    self.m_DescLabel.text=SafeStringFormat3( LocalString.GetString("赔率1:%s 赢则赚%d积分"),CLuaQMPKMgr.m_Peilv,0 )

    self.m_Slider=FindChild(self.transform,"Slider"):GetComponent(typeof(UISlider))
    self.m_Slider.OnChangeValue=DelegateFactory.Action_float(function(val)
        self.m_XiaZhuValue=math.floor(self.m_All*val)
        -- self.m_ValueLabel.text=tostring(self.m_XiaZhuValue)..LocalString.GetString("万")
        self:RefreshUI()
    end)
    
    self.m_ValueChoice={1,3,10,50,200}
    self.m_ValueLabel=FindChild(self.transform,"ValueLabel"):GetComponent(typeof(UILabel))
    self.m_ValueLabel.text="0"..LocalString.GetString("万")

    self.m_YinLiangLabel=FindChild(self.transform,"YinLiang"):GetComponent(typeof(UILabel))
    self.m_YinLiang=0
    if CClientMainPlayer.Inst then
        self.m_YinLiang=CClientMainPlayer.Inst.Silver+CClientMainPlayer.Inst.FreeSilver
    end
    self.m_YinLiangLabel.text=tostring(self.m_YinLiang)

    self.m_DaiBiLabel=FindChild(self.transform,"DaiBi"):GetComponent(typeof(UILabel))
    self.m_DaiBiLabel.text=tostring(CLuaQMPKMgr.m_DaiBi)
    self.m_All=math.floor(CLuaQMPKMgr.m_DaiBi/10000)+math.floor(self.m_YinLiang/10000)


    local button=FindChild(self.transform,"ConfirmButton").gameObject
    UIEventListener.Get(button).onClick=DelegateFactory.VoidDelegate(function(go)
        --playIndex, jingcaiIndex, selectIndex, touzhuNum
        Gac2Gas.RequestQmpkJingCaiVote(CLuaQMPKMgr.m_PlayIndex,CLuaQMPKMgr.m_JingCaiIndex,CLuaQMPKMgr.m_SelectIndex,self.m_XiaZhuValue)
        CUIManager.CloseUI(CLuaUIResources.QMPKJingCaiConfirmWnd)
    end)

    local buttonsTf=FindChild(self.transform,"Buttons")

    self.m_ChoiceButtons={}
    for i=1,5 do
        local btn=buttonsTf:GetChild(i-1).gameObject
        local cmp = btn:GetComponent(typeof(QnSelectableButton))
        
        if self.m_ValueChoice[i]>self.m_All then
            cmp.Enabled=false
        else
            cmp.Enabled=true
            table.insert( self.m_ChoiceButtons,cmp )
        end
    end
    --全下
    table.insert( self.m_ChoiceButtons,buttonsTf:GetChild(5):GetComponent(typeof(QnSelectableButton)) )
    
    for i=1,6 do
        local btn=buttonsTf:GetChild(i-1).gameObject
        local cmp = btn:GetComponent(typeof(QnSelectableButton))
        UIEventListener.Get(btn).onClick=DelegateFactory.VoidDelegate(function(go)
            --设置状态
            local selectedbtn=go:GetComponent(typeof(QnSelectableButton))
            for j,v in ipairs(self.m_ChoiceButtons) do
                if v==selectedbtn then
                    v:SetSelected(true,false)
                else
                    v:SetSelected(false,false)
                end
            end

            local index=go.transform:GetSiblingIndex()
            if self.m_ValueChoice[index+1] then
                self.m_XiaZhuValue=self.m_ValueChoice[index+1]
            elseif index==5 then
                --全下
                self.m_XiaZhuValue=self.m_All
            else
                self.m_XiaZhuValue=0
            end
            self.m_Slider.mValue=self.m_XiaZhuValue/self.m_All
            self.m_Slider:ForceUpdate()
            self:RefreshUI()
        end)
    end
end

function CLuaQMPKJingCaiConfirmWnd:RefreshUI()
    local val=math.floor(self.m_XiaZhuValue*CLuaQMPKMgr.m_Peilv*10)
    self.m_DescLabel.text=SafeStringFormat3( LocalString.GetString("赔率1:%s 赢则赚%d积分"),CLuaQMPKMgr.m_Peilv,val )
    self.m_ValueLabel.text=tostring(self.m_XiaZhuValue)..LocalString.GetString("万")
end

return CLuaQMPKJingCaiConfirmWnd