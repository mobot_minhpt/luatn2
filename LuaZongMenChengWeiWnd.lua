local UITabBar = import "L10.UI.UITabBar"
local CButton = import "L10.UI.CButton"
local QnTabButton = import "L10.UI.QnTabButton"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType1 = import "CPlayerInfoMgr+AlignType"
local CIMMgr = import "L10.Game.CIMMgr"

LuaZongMenChengWeiWnd = class()

RegistChildComponent(LuaZongMenChengWeiWnd,"m_Tabs","Tabs", UITabBar)
RegistChildComponent(LuaZongMenChengWeiWnd,"m_TabTemplate","TabTemplate", GameObject)
RegistChildComponent(LuaZongMenChengWeiWnd,"m_LeftScrollView","LeftScrollView", UIScrollView)
RegistChildComponent(LuaZongMenChengWeiWnd,"m_NameLabel","NameLabel", UILabel)
RegistChildComponent(LuaZongMenChengWeiWnd,"m_Description","Description", UILabel)
RegistChildComponent(LuaZongMenChengWeiWnd,"m_RightScrollView","RightScrollView", UIScrollView)
RegistChildComponent(LuaZongMenChengWeiWnd,"m_RightGrid","RightGrid", UIGrid)
RegistChildComponent(LuaZongMenChengWeiWnd,"m_PlayerInfoTemplate","PlayerInfoTemplate", GameObject)
RegistChildComponent(LuaZongMenChengWeiWnd,"m_SelectButton","SelectButton", CButton)
RegistChildComponent(LuaZongMenChengWeiWnd,"m_RightView","RightView", GameObject)
RegistChildComponent(LuaZongMenChengWeiWnd,"m_SomeOneGetChengWeiLabel","SomeOneGetChengWeiLabel", GameObject)
RegistChildComponent(LuaZongMenChengWeiWnd,"m_TipButton","TipButton", GameObject)

RegistClassMember(LuaZongMenChengWeiWnd, "m_Table")
RegistClassMember(LuaZongMenChengWeiWnd, "m_Titles")
RegistClassMember(LuaZongMenChengWeiWnd, "m_ChildTabs")
RegistClassMember(LuaZongMenChengWeiWnd, "m_SelectTab")
RegistClassMember(LuaZongMenChengWeiWnd, "m_OwnTitleIdList")
RegistClassMember(LuaZongMenChengWeiWnd, "m_SelectChildTab")
RegistClassMember(LuaZongMenChengWeiWnd, "m_CurShowTitleId")
function LuaZongMenChengWeiWnd:Init()
    self.m_OwnTitleIdList = {}
    self.m_CurShowTitleId = 0
    self.m_TabTemplate:SetActive(false)
    self.m_PlayerInfoTemplate:SetActive(false)
    self.m_RightView:SetActive(false)
    self.m_SomeOneGetChengWeiLabel:SetActive(false)
    self:InitTitles()
    self.m_Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnTabChange(index)
    end)
    self.m_Tabs:ChangeTab(0, false)
    if CClientMainPlayer.Inst then
        Gac2Gas.QueryMySectTitleInfo(CClientMainPlayer.Inst.BasicProp.SectId,"overview",0)
    end
    UIEventListener.Get(self.m_SelectButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnSelectButtonClicked()
    end)
    UIEventListener.Get(self.m_TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        g_MessageMgr:ShowMessage("ZongMenChengWeiWnd_ReadMe")
    end)
end

function LuaZongMenChengWeiWnd:OnSelectButtonClicked()
    if CClientMainPlayer.Inst then
        local key = self.m_Titles[self.m_SelectTab][self.m_SelectChildTab]
        Gac2Gas.RequestSetSectShowTitle(CClientMainPlayer.Inst.BasicProp.SectId, self.m_CurShowTitleId == key and 0 or key)
    end
end

function LuaZongMenChengWeiWnd:InitTitles()
    self.m_Titles = {}
    self.m_ChildTabs = {}
    Menpai_Title.Foreach(function(key,data)
        local type = data.Type
        if data.Status ~= 3 then
            if not self.m_Titles[type] then self.m_Titles[type] = {} end
            table.insert(self.m_Titles[type], key)
        end
    end)
    for type, list in pairs(self.m_Titles) do
        local t = self.m_Tabs.transform:GetChild(type - 1)
        local uitable = t.transform:Find("Table"):GetComponent(typeof(UITable))
        self.m_ChildTabs[type] = {btns = {},uitable = uitable}
        for _,key in pairs(list) do
            local go = NGUITools.AddChild(uitable.gameObject, self.m_TabTemplate)
            go:SetActive(false)
            go.transform:Find("Own").gameObject:SetActive(false)
            local btn = go:GetComponent(typeof(QnTabButton))
            local data = Menpai_Title.GetData(key)
            btn.Text = data.Name
            table.insert(self.m_ChildTabs[type].btns, btn)
        end
        uitable:Reposition()
        local tabBar = uitable.gameObject:AddComponent(typeof(UITabBar))
        tabBar:Init()
        tabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
            self:OnChildTabChange(index)
        end)
        tabBar:ChangeTab(0, true)
        self.m_ChildTabs[type].tabBar = tabBar
    end
    self.m_Table = self.m_Tabs.gameObject:GetComponent(typeof(UITable))
    self.m_Table:Reposition()
    self.m_LeftScrollView:ResetPosition()
end

function LuaZongMenChengWeiWnd:OnTabChange(index)
    self.m_SelectTab = index + 1
    for type, data in pairs(self.m_ChildTabs) do
        local btns = data.btns
        local uitable = data.uitable
        local tabBar = data.tabBar
        for _,btn in pairs(btns) do
            btn.gameObject:SetActive(type == self.m_SelectTab)
        end
        uitable:Reposition()
        tabBar:ChangeTab(0, type ~= self.m_SelectTab)
    end
    self.m_Table:Reposition()
    self.m_LeftScrollView:ResetPosition()
end


function LuaZongMenChengWeiWnd:OnChildTabChange(index)
    self.m_SelectChildTab = index + 1
    local key = self.m_Titles[self.m_SelectTab][self.m_SelectChildTab]
    if CClientMainPlayer.Inst then
        Gac2Gas.QuerySectHasTitleMember(CClientMainPlayer.Inst.BasicProp.SectId,  key)
        --Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId,"title", key)
    end
    self:InitRightView(Menpai_Title.GetData(key))
end

function LuaZongMenChengWeiWnd:InitRightView(data)
    self.m_NameLabel.text = data.Name
    self.m_Description.text = data.Condition
    self.m_SelectButton.gameObject:SetActive(false)
    self.m_RightView:SetActive(true)

    local selectkey = self.m_Titles[self.m_SelectTab][self.m_SelectChildTab]
    self.m_SelectButton.gameObject:SetActive(self.m_OwnTitleIdList[selectkey])
    self.m_SelectButton.Text =selectkey == self.m_CurShowTitleId and LocalString.GetString("取消显示") or LocalString.GetString("显示称谓")
end

function LuaZongMenChengWeiWnd:OnEnable()
    g_ScriptEvent:AddListener("OnZongMenOwnTitleIdList", self, "OnZongMenOwnTitleIdList")
    g_ScriptEvent:AddListener("OnZongMenHasTitleMemberInfoResult", self, "OnSectHasTitleMemberInfoResult")
    g_ScriptEvent:AddListener("OnSyncSetSectShowTitle", self, "OnSyncSetSectShowTitle")
end

function LuaZongMenChengWeiWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnZongMenOwnTitleIdList", self, "OnZongMenOwnTitleIdList")
    g_ScriptEvent:RemoveListener("OnZongMenHasTitleMemberInfoResult", self, "OnSectHasTitleMemberInfoResult")
    g_ScriptEvent:RemoveListener("OnSyncSetSectShowTitle", self, "OnSyncSetSectShowTitle")
end

function LuaZongMenChengWeiWnd:OnSyncSetSectShowTitle(currentTitle)  
    self.m_CurShowTitleId = currentTitle
    local selectkey = self.m_Titles[self.m_SelectTab][self.m_SelectChildTab]
    for type, list in pairs(self.m_Titles) do
        local t = self.m_ChildTabs[type]
        local btns = t.btns
        for index,key in pairs(list) do
            local btn = btns[index]
            local color = btn.background.color
            color.a = self.m_OwnTitleIdList[key] and 1 or 0.5
            btn.background.color = color 
            btn.transform:Find("Own").gameObject:SetActive(key == currentTitle)
            if key == selectkey then
                self.m_SelectButton.gameObject:SetActive(self.m_OwnTitleIdList[key])
                self.m_SelectButton.Text = key == currentTitle and LocalString.GetString("取消显示") or LocalString.GetString("显示称谓")
            end
            color = btn.label.color
            color.a = self.m_OwnTitleIdList[key] and 1 or 0.7
            btn.label.color = color
        end
    end
end

function LuaZongMenChengWeiWnd:OnZongMenOwnTitleIdList(list,currentTitle)
    self.m_OwnTitleIdList = {}
    self.m_CurShowTitleId = currentTitle
    for i = 0, list.Count -1 do
        local list2 = list[i]
        for j = 0, list2.Count -1 do
            local key = list2[j]
            self.m_OwnTitleIdList[key] = true
        end
    end
    self:OnSyncSetSectShowTitle(currentTitle) 
    -- for type, t in pairs(self.m_ChildTabs) do
    --     for index, btn in pairs(t.btns) do
    --         local key = self.m_Titles[type][index]
            
    --     end
    -- end
end

function LuaZongMenChengWeiWnd:OnSectHasTitleMemberInfoResult(list)
    Extensions.RemoveAllChildren(self.m_RightGrid.transform)
    for _,info in pairs(list) do
        local go = CUICommonDef.AddChild(self.m_RightGrid.gameObject, self.m_PlayerInfoTemplate)
        go.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = info.Name
        go.transform:Find("Portrait"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(info.Class, info.Gender, -1),false)
        go.transform:Find("Portrait/LvLabel"):GetComponent(typeof(UILabel)).text = info.Level
        go:SetActive(true)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (p)
            local pos = Vector3(540, 0)
            pos = go.transform:TransformPoint(pos)
            self:ShowPlayerPopupMenu(info.PlayerId, pos)
        end)
    end
    self.m_RightGrid:Reposition()
    self.m_RightScrollView:ResetPosition()
    self.m_SomeOneGetChengWeiLabel:SetActive(#list > 0)
end

function LuaZongMenChengWeiWnd:ShowPlayerPopupMenu(id, pos)
    CPlayerInfoMgr.ShowPlayerPopupMenu(id, CIMMgr.Inst:IsMyFriend(id) and EnumPlayerInfoContext.FriendInFriendWnd or EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, pos, AlignType1.Default)
end