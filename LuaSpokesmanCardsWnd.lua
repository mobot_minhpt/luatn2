local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local UITabBar = import "L10.UI.UITabBar"
local CUICommonDef = import "L10.UI.CUICommonDef"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"

LuaSpokesmanCardsWnd = class()

RegistChildComponent(LuaSpokesmanCardsWnd, "m_TopInfoLabel","TopInfoLabel",UILabel)
RegistChildComponent(LuaSpokesmanCardsWnd, "m_TopHintLabel","TopHintLabel",UILabel)
RegistChildComponent(LuaSpokesmanCardsWnd, "m_ProgressLabel","ProgressLabel",UILabel)
RegistChildComponent(LuaSpokesmanCardsWnd, "m_UITabBar","UITabBar",UITabBar)
RegistChildComponent(LuaSpokesmanCardsWnd, "m_Grid","Grid",UIGrid)
RegistChildComponent(LuaSpokesmanCardsWnd, "m_Template","Template",GameObject)
RegistChildComponent(LuaSpokesmanCardsWnd, "m_BreakButton","BreakButton",GameObject)
RegistChildComponent(LuaSpokesmanCardsWnd, "m_AddBagButton","AddBagButton",GameObject)
RegistChildComponent(LuaSpokesmanCardsWnd, "m_ShowBigPictButton","ShowBigPictButton",GameObject)
RegistChildComponent(LuaSpokesmanCardsWnd, "m_ShareButton","ShareButton",GameObject)
RegistChildComponent(LuaSpokesmanCardsWnd, "m_BatchBreakButton","BatchBreakButton",GameObject)
RegistChildComponent(LuaSpokesmanCardsWnd, "m_DuplicateCradsNumLabel","DuplicateCradsNumLabel",UILabel)
RegistChildComponent(LuaSpokesmanCardsWnd, "m_OpenShopButton","OpenShopButton",GameObject)
RegistChildComponent(LuaSpokesmanCardsWnd, "m_ComposeButton","ComposeButton",GameObject)
RegistChildComponent(LuaSpokesmanCardsWnd, "m_ReadMeButton","ReadMeButton",GameObject)
RegistChildComponent(LuaSpokesmanCardsWnd, "m_Pool","Pool",GameObject)
RegistChildComponent(LuaSpokesmanCardsWnd, "m_ButtonsBg","ButtonsBg",GameObject)
RegistChildComponent(LuaSpokesmanCardsWnd, "m_BlankWidget","BlankWidget",GameObject)

RegistClassMember(LuaSpokesmanCardsWnd, "m_CardsList")
RegistClassMember(LuaSpokesmanCardsWnd, "m_LastClickCardID")
RegistClassMember(LuaSpokesmanCardsWnd, "m_RepateCardsNum")
RegistClassMember(LuaSpokesmanCardsWnd, "m_LastSelectTabIndex")
RegistClassMember(LuaSpokesmanCardsWnd, "m_CardsNum")
RegistClassMember(LuaSpokesmanCardsWnd, "m_UITabBarCUITextures")
RegistClassMember(LuaSpokesmanCardsWnd, "m_IsGetAllCards")

function LuaSpokesmanCardsWnd:Init()
    self.m_TopInfoLabel.text = CUICommonDef.TranslateToNGUIText(g_MessageMgr:FormatMessage("SpokesmanCardsWnd_TopInfoText"))
    self.m_TopHintLabel.text = CUICommonDef.TranslateToNGUIText(g_MessageMgr:FormatMessage("SpokesmanCardsWnd_TopHintText"))
    self.m_Template:SetActive(false)
    self.m_Pool:SetActive(false)
    self.m_ButtonsBg:SetActive(false)
    self.m_OpenShopButton:SetActive(false)
    UIEventListener.Get(self.m_BreakButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        self:BreakSingleCard()
    end)
    UIEventListener.Get(self.m_AddBagButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:AddCardToBag()
    end)
    UIEventListener.Get(self.m_ShowBigPictButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:ShowBigPicture()
    end)
    UIEventListener.Get(self.m_ShareButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:Share()
    end)
    UIEventListener.Get(self.m_TopInfoLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        local index = self.m_TopInfoLabel:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
        self:OnTopInfoLabelClick(self.m_TopInfoLabel.text,index)
    end)
    UIEventListener.Get(self.m_BatchBreakButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:BatchBreakCards()
    end)
    UIEventListener.Get(self.m_ReadMeButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:ReadMe()
    end)
    UIEventListener.Get(self.m_ComposeButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OpenComposeWnd()
    end)
    UIEventListener.Get(self.m_BlankWidget).onClick = DelegateFactory.VoidDelegate(function (p)
        self.m_ButtonsBg:SetActive(false)
    end)
    self.m_UITabBarCUITextures = {}
    self.m_UITabBar:Start()
    for i =0,self.m_UITabBar.tabButtons.Length - 1 do
        local tab = self.m_UITabBar:GetTabGoByIndex(i)
        self.m_UITabBarCUITextures[i] = tab.transform:GetComponent(typeof(CUITexture))
    end
    self.m_UITabBarCUITextures.normalArray = {"shangce_normal","zhongce_normal","xiace"}
    self.m_UITabBarCUITextures.hightlightArray = {"shangce","zhongce_highlight","xiace_highlight"}
    self.m_UITabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnTabChange(go, index)
    end)
    Gac2Gas.RequestSpokesmanTCGData()
end

function LuaSpokesmanCardsWnd:OnEnable()
    g_ScriptEvent:AddListener("SpokesmanCardsWnd_LoadData",self,"LoadData")
    g_ScriptEvent:AddListener("ShareFinished", self, "OnShareFinished")
    g_ScriptEvent:AddListener("PersonalSpaceShareFinished", self, "OnPersonalSpaceShareFinished")
end

function LuaSpokesmanCardsWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SpokesmanCardsWnd_LoadData",self,"LoadData")
    g_ScriptEvent:RemoveListener("ShareFinished", self, "OnShareFinished")
    g_ScriptEvent:RemoveListener("PersonalSpaceShareFinished", self, "OnPersonalSpaceShareFinished")
end

function LuaSpokesmanCardsWnd:LoadData(data)
    local list = data.list
    self.m_CardsList = {}
    Extensions.RemoveAllChildren(self.m_Pool.transform)
    Extensions.RemoveAllChildren(self.m_Grid.transform)

    local ownerNum = 0
    local cardsNum = 0
    self.m_RepateCardsNum = 0

    for i = 1,#list do

        local d = list[i]
        local id = d.id
        local owner = d.owner
        local isnew = d.isnew
        local num = d.num

        local t = {}
        t.owner = owner
        cardsNum = cardsNum + 1
        ownerNum = ownerNum + (owner and 1 or 0)
        self.m_RepateCardsNum = self.m_RepateCardsNum + ((num > 1) and (num - 1) or 0)
        t.tcgdata = SpokesmanTCG_TCG.GetData(id)
        local obj = NGUITools.AddChild(self.m_Pool.gameObject,self.m_Template)
        t.obj = obj
        local script = obj:GetComponent(typeof(CCommonLuaScript))
        script:Awake()
        script = script.m_LuaSelf
        t.script = script
        script:InitCard(id, owner, isnew, num)
        obj:SetActive(true)
        t.num = num
        t.isnew = isnew
        t.id = id

        UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (p)
            self:OnCardClick(id)
        end)

        self.m_CardsList[id] = t
    end
    self.m_CardsNum = cardsNum
    self.m_ProgressLabel.text = SafeStringFormat3("%d/%d",ownerNum, cardsNum)
    self.m_DuplicateCradsNumLabel.text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3(self.m_RepateCardsNum == 0 and LocalString.GetString("重复卡片x[#ff0000]%d")
            or LocalString.GetString("重复卡片x%d"), self.m_RepateCardsNum))
    Extensions.SetLocalPositionZ(self.m_BatchBreakButton.transform, self.m_RepateCardsNum == 0 and -1 or 0)

    self.m_IsGetAllCards = ownerNum == cardsNum
    if ownerNum == cardsNum then
        self.m_OpenShopButton:SetActive(true)
        UIEventListener.Get(self.m_OpenShopButton).onClick = DelegateFactory.VoidDelegate(function (p)
            self:OpenShop()
        end)
    end

    self.m_UITabBar:ChangeTab(self.m_LastSelectTabIndex and self.m_LastSelectTabIndex or 0, false)
end

function LuaSpokesmanCardsWnd:OnCardClick(clickID)
    if not self.m_CardsList then return end
    if self.m_LastClickCardID then
        self.m_CardsList[self.m_LastClickCardID].script:SetSelect(false)
    end
    if (not self.m_CardsNum) or (clickID > self.m_CardsNum) then return end

    local t = self.m_CardsList[clickID]
    t.script:OnCardClick()
    self.m_LastClickCardID = clickID
    self.m_ButtonsBg:SetActive(t.owner)
    if t.owner then
        self.m_ButtonsBg.transform.localPosition = Vector3(
                t.obj.transform.localPosition.x + 243 + self.m_Grid.transform.localPosition.x,
                t.obj.transform.localPosition.y + self.m_Grid.transform.localPosition.y,
                t.obj.transform.localPosition.z
        )
    end
    if t.isnew then
        Gac2Gas.SpokesmanTCGReadCard(clickID)
        t.isnew = false
        t.script:InitCard(t.id, t.owner, t.isnew, t.num)
    end
end

function LuaSpokesmanCardsWnd:BreakSingleCard()
    if not self.m_LastClickCardID then return end
    if not self.m_CardsList then return end
    local t = self.m_CardsList[self.m_LastClickCardID]
    local decomposeGet = t.tcgdata.DecomposeGet[0]
    local itemName = Item_Item.GetData(decomposeGet[0]).Name
    local num = decomposeGet[1]
    local cost = t.tcgdata.DecomposeCostYinLiang
    local msg = g_MessageMgr:FormatMessage( "SpokesmanCardsWnd_SingleCardBreak",t.tcgdata.Name, num,itemName)
    QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.YinLiang, msg, cost, DelegateFactory.Action(function ( ... )
        Gac2Gas.SpokesmanTCGDecomposeCard(self.m_LastClickCardID, 1)
    end), nil, false, true, EnumPlayScoreKey.NONE, false)
end

function LuaSpokesmanCardsWnd:AddCardToBag()
    if not self.m_LastClickCardID then return end
    local t = self.m_CardsList[self.m_LastClickCardID]
    if t.num <= 1 then
        g_MessageMgr:ShowMessage("SpokesmanCardsWnd_AddCardToBag_NoneEnoughCards")
        return
    end
    CLuaNumberInputMgr.ShowNumInputBox(1, t.num - 1, 1, function (val)
        Gac2Gas.SpokesmanTCGRetrieveCard(self.m_LastClickCardID, val)
    end, LocalString.GetString("可将重复的卡片放入包裹赠送给其他玩家"), -1)
end

function LuaSpokesmanCardsWnd:ShowBigPicture()
    if not self.m_LastClickCardID then return end
    CLuaSpokesmanMgr.m_SpokesmanPreviewWndPreviewCardID = self.m_LastClickCardID
    CUIManager.ShowUI(CLuaUIResources.SpokesmanPreviewWnd)
end

function LuaSpokesmanCardsWnd:Share()
    self.m_ButtonsBg:SetActive(false)
    if not self.m_LastClickCardID then return end
    if not CClientMainPlayer.Inst then return end
    CUICommonDef.CaptureScreen(
            "screenshot",
            true,
            false,
            self.m_ShareButton,
            DelegateFactory.Action_string_bytes(
                    function(fullPath, jpgBytes)
                        if  CLuaShareMgr.IsOpenShare() then
                            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
                        else
                            CLuaShareMgr.SimpleShareLocalImage2PersonalSpace(fullPath)
                        end
                    end
            ),
            false
    )
end

function LuaSpokesmanCardsWnd:OnTopInfoLabelClick(text, index)

end

function LuaSpokesmanCardsWnd:OpenShop()
    local data = SpokesmanTCG_Setting.GetData("MysteryShopID")
    if data then
        CLuaNPCShopInfoMgr.ShowScoreShopById(tonumber(data.Value))
    end
end

function LuaSpokesmanCardsWnd:OnShareFinished(argv)
    local code, result = argv[0], argv[1]
    if result then
        Gac2Gas.SpokesmanTCGShareDone(self.m_LastClickCardID)
    end
end

function LuaSpokesmanCardsWnd:OnPersonalSpaceShareFinished()
    Gac2Gas.SpokesmanTCGShareDone(self.m_LastClickCardID)
end

function LuaSpokesmanCardsWnd:ReadMe()
    g_MessageMgr:ShowMessage("SpokesmanCardsWnd_ReadMe")
end

function LuaSpokesmanCardsWnd:OpenComposeWnd()
    if self.m_IsGetAllCards then
        g_MessageMgr:ShowMessage("SpokesmanCardsWnd_GetAllCardsTip")
    end
    CUIManager.ShowUI(CLuaUIResources.SpokesmanCardsComposeWnd)
end

function LuaSpokesmanCardsWnd:BatchBreakCards()
    if not self.m_CardsList then return end
    local haiTangItemId = tonumber(SpokesmanTCG_Setting.GetData("HaiTangItemId").Value)
    local superHaiTangItemId = tonumber(SpokesmanTCG_Setting.GetData("SuperHaiTangItemId").Value)
    local haiTangItem = Item_Item.GetData(haiTangItemId)
    local superHaiTangItem = Item_Item.GetData(superHaiTangItemId)
    local haiTangNum = 0
    local superHaiTangNum = 0
    local cost = 0
    for id ,t in pairs(self.m_CardsList) do
        if t.num > 1 then
            local decomposeGet = t.tcgdata.DecomposeGet
            for i = 0,decomposeGet.Length - 1 do
                local itemId = decomposeGet[i][0]
                local num = decomposeGet[i][1]
                if itemId == haiTangItemId then
                    haiTangNum = haiTangNum + num * (t.num - 1)
                end
                if itemId == superHaiTangItemId then
                    superHaiTangNum = superHaiTangNum + num * (t.num - 1)
                end
            end
            cost = cost + t.tcgdata.DecomposeCostYinLiang *  (t.num - 1)
        end
    end
    local msg = g_MessageMgr:FormatMessage( "SpokesmanCardsWnd_BatchBreakCards",
            self.m_RepateCardsNum, haiTangItem.Name,haiTangNum, superHaiTangItem.Name, superHaiTangNum)
    QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.YinLiang, msg, cost, DelegateFactory.Action(function ( ... )
        Gac2Gas.SpokesmanTCGBatchDecomposeCard()
    end), nil, false, true, EnumPlayScoreKey.NONE, false)
end

function LuaSpokesmanCardsWnd:OnTabChange(go, index)
    if not self.m_CardsList then return end
    local startIndex,endIndex
    if self.m_LastSelectTabIndex then
        self:OnTabChangeNoraml(self.m_LastSelectTabIndex,true)
        startIndex = 1 + self.m_LastSelectTabIndex * 10
        endIndex = math.min(startIndex + 9,#self.m_CardsList)
        for i = startIndex, endIndex do
            local t = self.m_CardsList[i]
            t.obj.transform.parent = self.m_Pool.transform
        end
    end
    self:OnTabChangeNoraml(index,false)
    startIndex = 1 + index * 10
    endIndex = math.min(startIndex + 9,#self.m_CardsList)
    for i = startIndex, endIndex do
        local t = self.m_CardsList[i]
        t.obj.transform.parent = self.m_Grid.transform
    end
    self.m_Grid:Reposition()

    self.m_LastSelectTabIndex = index
    self.m_ButtonsBg:SetActive(false)
    --self:OnCardClick(startIndex)
end

function LuaSpokesmanCardsWnd:OnTabChangeNoraml(index,noraml)
    if not self.m_UITabBarCUITextures then return end
    local tab =  self.m_UITabBarCUITextures[index]
    local array = noraml and self.m_UITabBarCUITextures.normalArray or self.m_UITabBarCUITextures.hightlightArray
    if tab then
        local path = SafeStringFormat3("UI/Texture/Transparent/Material/chuanshijianpu_%s.mat",array[index + 1])
        tab:LoadMaterial(path)
    end
end