local UInt32 = import "System.UInt32"
local L10 = import "L10"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumGender = import "L10.Game.EnumGender"
local Debug = import "UnityEngine.Debug"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CShiTuMgr = import "L10.Game.CShiTuMgr"
local CScene = import "L10.Game.CScene"
local CPos = import "L10.Engine.CPos"
local CLoveFlowerMgr = import "L10.Game.CLoveFlowerMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

CLuaItemUsageWnd = class()
RegistClassMember(CLuaItemUsageWnd,"iconTexture")
RegistClassMember(CLuaItemUsageWnd,"nameLabel")
RegistClassMember(CLuaItemUsageWnd,"applyButton")
RegistClassMember(CLuaItemUsageWnd,"closeButton")
RegistClassMember(CLuaItemUsageWnd,"item")

function CLuaItemUsageWnd:Awake()
    self.iconTexture = self.transform:Find("Anchor/ItemCell/IconTexture"):GetComponent(typeof(CUITexture))
    self.nameLabel = self.transform:Find("Anchor/Label"):GetComponent(typeof(UILabel))
    self.applyButton = self.transform:Find("Anchor/ApplyButton").gameObject
    self.closeButton = self.transform:Find("Anchor/CloseButton").gameObject
--self.item = ?

    UIEventListener.Get(self.applyButton).onClick =  DelegateFactory.VoidDelegate(function(go) 
        self:OnApplyButtonClick(go)
    end)

    UIEventListener.Get(self.iconTexture.gameObject).onClick =  DelegateFactory.VoidDelegate(function(go) 
        self:OnIconClick(go)
    end)
end

-- Auto Generated!!
function CLuaItemUsageWnd:Init( )
    self.iconTexture:Clear()
    self.nameLabel.text = ""
    if CItemInfoMgr.quickUseTemplateID ~= 0 and CItemMgr.Inst:IsLoveFlowerItem(CItemInfoMgr.quickUseTemplateID) then
        local pos
        local itemid
        local default
        default, pos, itemid = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, CItemInfoMgr.quickUseTemplateID)
        if default then
            self.item = CItemMgr.Inst:GetById(itemid)
        else
            CUIManager.CloseUI(CIndirectUIResources.ItemUsageWnd)
            return
        end
    else
        self.item = CItemMgr.Inst:GetById(CItemInfoMgr.taskTriggeredTaskItemId)
    end
    if self.item ~= nil then
        local iconPath = self.item.Icon
        local name = self.item.Name

        if not System.String.IsNullOrEmpty(iconPath) then
            self.iconTexture:LoadMaterial(iconPath)
        end
        self.nameLabel.text = name
    else
        Debug.LogError("item not found!")
    end
end
function CLuaItemUsageWnd:OnApplyButtonClick( go) 
    if self.item ~= nil then
        if CItemMgr.Inst:IsLoveFlowerItem(self.item.TemplateId) then
            self:useLoveFlowerItem()
        else
            self:useTaskItem()
        end
        CUIManager.CloseUI(CIndirectUIResources.ItemUsageWnd)
    end
end
function CLuaItemUsageWnd:useTaskItem( )
    local place = EnumItemPlace.Bag
    if CItemMgr.Inst:IsTaskItem(self.item.TemplateId) then
        place = EnumItemPlace.Task
    end

    local pos = CItemMgr.Inst:FindItemPosition(place, self.item.Id)
    if pos > 0 then
        if self.item.IsItem and self.item.Item.Type == EnumItemType_lua.GreenHandGift then
            CUIManager.ShowUI(CUIResources.WelfareWnd)
        else
            local taskTemplate = Task_Task.GetData(CItemInfoMgr.taskTriggeredTaskId)
            if taskTemplate ~= nil and not System.String.IsNullOrEmpty(taskTemplate.GamePlay) then
                if taskTemplate.GamePlay == "CaiChengYu" then
                    --TODO 打开输入成语的对话框,输入完成后 Gac2Gas.RequestUseItem((uint)place, pos, item.Id, 输入的成语);
                    CShiTuMgr.Inst:ShowChengYuWnd(place, pos, self.item.Id)
                    CUIManager.CloseUI(CIndirectUIResources.ItemUsageWnd)
                    return
                end

                if taskTemplate.GamePlay == "ShanHaiJing" then
                    if CommonDefs.DictContains(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), CItemInfoMgr.taskTriggeredTaskId) then
                        local currentTask = CommonDefs.DictGetValue(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), CItemInfoMgr.taskTriggeredTaskId)
                        local location = CTaskMgr.Inst:GetTaskTrackPos(currentTask)
                        if CScene.MainScene.SceneTemplateId == location.sceneTemplateId then
                            local trackpos = L10.Engine.Utility.GridPos2PixelPos(CreateFromClass(CPos, location.targetX, location.targetY))
                            local dist = L10.Engine.Utility.Grid2Pixel(2)
                            local itemId = self.item.Id
                            CTrackMgr.Inst:TaskTrack(location.taskId, location.sceneId, location.sceneTemplateId, 0, trackpos, dist, DelegateFactory.Action(function () 
                                Gac2Gas.RequestUseItem(EnumToInt(place), pos, itemId, "")
                                CUIManager.CloseUI(CIndirectUIResources.ItemUsageWnd)
                            end), DelegateFactory.Action(function () 
                                CUIManager.CloseUI(CIndirectUIResources.ItemUsageWnd)
                            end))
                            return
                        end
                    end
                end
            end
            if (CommonDefs.IS_CN_CLIENT or CommonDefs.IS_HMT_CLIENT) and Item_Item.GetData(self.item.TemplateId).Readable > 0 then
                CItemInfoMgr.ShowLetterDisplayWnd(EnumToInt(place), self.item.Id, pos)
            else
                if CItemInfoMgr.taskTriggeredTaskId > 0 then
                    Gac2Gas.RequestUseItemWithTaskId(EnumToInt(place), pos, self.item.Id, "", CItemInfoMgr.taskTriggeredTaskId)
                else
                    Gac2Gas.RequestUseItem(EnumToInt(place), pos, self.item.Id, "")
                end
            end
        end
    end
end
function CLuaItemUsageWnd:useLoveFlowerItem( )
    local place = EnumItemPlace.Bag
    local gender = EnumGender.Male
    if CClientMainPlayer.Inst ~= nil then
        gender = CClientMainPlayer.Inst.Gender
    end
    if self.item.TemplateId == CLoveFlowerMgr.Instance.growItemID and (gender ~= EnumGender.Male) then
        return
    end
    if self.item.TemplateId == CLoveFlowerMgr.Instance.upgradeItemID and (gender ~= EnumGender.Female) then
        return
    end

    local pos = CItemMgr.Inst:FindItemPosition(place, self.item.Id)
    if pos > 0 then
        --   Gac2Gas.RequestUseItem((uint)place, pos, item.Id, "");
        if self.item.TemplateId == CLoveFlowerMgr.Instance.growItemID then
            --               Gac2Gas.RequestUseItem((uint)place, pos, item.Id, "");
            Gac2Gas.RequestUseCommonFlowerSeed(CLoveFlowerMgr.Instance.engineID)
        end
        if self.item.TemplateId == CLoveFlowerMgr.Instance.upgradeItemID then
            --            Gac2Gas.RequestUseItem((uint)place, pos, item.Id, "");
            Gac2Gas.RequestUseLoveFlowerSeed(CLoveFlowerMgr.Instance.engineID)
        end
    end
end

function CLuaItemUsageWnd:OnIconClick(go)
    CItemInfoMgr.ShowLinkItemInfo(self.item.Id, false, nil, AlignType.Default, 0, 0, 0, 0);
end
