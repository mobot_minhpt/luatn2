local UILabel = import "UILabel"
local CUITexture=import "L10.UI.CUITexture"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local QnTableItem = import "L10.UI.QnTableItem"
local LayerDefine = import "L10.Engine.LayerDefine"

local Quaternion = import "UnityEngine.Quaternion"
local Zhuangshiwu_ZswTemplate = import "L10.Game.Zhuangshiwu_ZswTemplate"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CButton = import "L10.UI.CButton"

local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CYuanbaoMarketMgr = import "L10.UI.CYuanbaoMarketMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local AlignType2=import "L10.UI.CPopupMenuInfoMgr+AlignType"
local AlignType1 = import "L10.UI.CItemInfoMgr+AlignType"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local Vector3 = import "UnityEngine.Vector3"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"

CLuaZswTemplateFillingComfirmWnd = class()

RegistChildComponent(CLuaZswTemplateFillingComfirmWnd, "m_TableView","TableView", QnAdvanceGridView)
RegistChildComponent(CLuaZswTemplateFillingComfirmWnd, "m_DefaultItem","DefaultItem", QnTableItem)
RegistChildComponent(CLuaZswTemplateFillingComfirmWnd, "m_ShopBtn","GoToShopBtn", GameObject)
RegistChildComponent(CLuaZswTemplateFillingComfirmWnd, "m_FillBtn","FillBtn", CButton)
RegistClassMember(CLuaZswTemplateFillingComfirmWnd, "m_SelectPartType")
RegistClassMember(CLuaZswTemplateFillingComfirmWnd, "m_ShowTemplateIds")
RegistClassMember(CLuaZswTemplateFillingComfirmWnd, "m_DefaultTemplateId")
RegistClassMember(CLuaZswTemplateFillingComfirmWnd, "m_IsSelectDefault")
RegistClassMember(CLuaZswTemplateFillingComfirmWnd, "m_SelectFillingTid")
RegistClassMember(CLuaZswTemplateFillingComfirmWnd, "m_SelectFillIndex")
function CLuaZswTemplateFillingComfirmWnd:Awake()
    local data = Zhuangshiwu_Zhuangshiwu.GetData(CLuaZswTemplateMgr.CurSelectPartTid)
    if data then
        self.m_SelectPartType = data.Type
    else
        self.m_SelectPartType = nil
    end
    self.m_DefaultTemplateId = CLuaZswTemplateMgr.CurSelectPartTid
    self.m_IsSelectDefault = false
    self.m_SelectFillingTid =self.m_DefaultTemplateId


    local defaultItemId = Zhuangshiwu_Zhuangshiwu.GetData(self.m_DefaultTemplateId).ItemId
    self.m_DefaultItem.OnSelected = DelegateFactory.Action_GameObject(function (go)
        self.m_TableView:SetSelectRow(-1,true)
        self.m_SelectFillingTid = self.m_DefaultTemplateId
        if not self.m_IsSelectDefault then
            self.m_FillBtn.Enabled = false
        else
            self.m_FillBtn.Enabled = true
        end
        local mask = self.m_DefaultItem.transform:Find("Mask").gameObject
        if mask.activeSelf then
            CItemAccessListMgr.Inst:ShowItemAccessInfo(defaultItemId,true,nil,AlignType.Right)
        else
            CItemInfoMgr.ShowLinkItemTemplateInfo(defaultItemId,false,nil,AlignType1.Default, 0, 0, 0, 0)
        end
    end)

    UIEventListener.Get(self.m_ShopBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnShopBtnClick()
    end)
    UIEventListener.Get(self.m_FillBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:FillTemplateWithTid()
    end)

end

function CLuaZswTemplateFillingComfirmWnd:InitClearView()
    self.transform:Find("Anchor").gameObject:SetActive(false)
    local selectClearItem=DelegateFactory.Action_int(function(index)
        self:ClearOneAction()
    end)
    local t={}
    local item=PopupMenuItemData(LocalString.GetString("卸下"),selectClearItem,false,nil)
    table.insert(t, item)
    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    local row = CLuaZswTemplateMgr.SelectRoIndex
    --local go = self.m_TableView:GetItem(row)
    CPopupMenuInfoMgr.ShowPopupMenu(array, CLuaZswTemplateMgr.SelectItem.transform, AlignType2.Right,1,nil,600,true,266)
end

function CLuaZswTemplateFillingComfirmWnd:Init()
    if not self.m_SelectPartType then
        return
    end

    self:InitDefaultItem()
    if not self.m_IsSelectDefault then
        self.m_FillBtn.Enabled = false
    end
    self.m_DefaultItem:SetSelected(self.m_IsSelectDefault)

    self.m_ShowTemplateIds = {}
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.m_ShowTemplateIds
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self.m_FillBtn.Enabled = true
        self.m_SelectFillingTid = self.m_ShowTemplateIds[row+1]
        self.m_SelectFillIndex = row + 1
        self.m_DefaultItem:SetSelected(false)
        local itemId = 0
        local data = Zhuangshiwu_Zhuangshiwu.GetData(self.m_SelectFillingTid)
        if data then
            itemId = data.ItemId
        end
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false,nil,AlignType1.Default, 0, 0, 0, 0)
        end)

    --local ids = CLuaClientFurnitureMgr.GetIdsByType(self.m_SelectPartType)
    local bIsMingyuan = false
    if CClientHouseMgr.Inst.mBasicProp ~= nil and CClientHouseMgr.Inst.mBasicProp.IsMingyuan ~= 0 then
        bIsMingyuan = true
    end

    local key = CLuaZswTemplateMgr.HuaMu
    if self.m_SelectPartType ~= EnumFurnitureType_lua.eHuamu then
        key = CLuaZswTemplateMgr.Other
    end

    if CLuaZswTemplateMgr.CurSelectTypeTids[key] == nil then
        local tbl = {}
        --local ids = {}
        if key == CLuaZswTemplateMgr.Other then
            local canUseTypeList = Zhuangshiwu_Setting.GetData().ZswTemplateComponentTypes
            if canUseTypeList then
                for i=0,canUseTypeList.Length-1,1 do
                    local type = canUseTypeList[i]
                    local tempIds = CLuaClientFurnitureMgr.GetIdsByType(type)
                    for i,v in ipairs(tempIds) do
                        local data = Zhuangshiwu_Zhuangshiwu.GetData(v)
                        if not (not bIsMingyuan and data.IsMingyuan == 1) then
                            local count = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(v)
                            table.insert(tbl, data.FurnishTemplate)
                        end
                    end
                end
            end
        else
            local ids = CLuaClientFurnitureMgr.GetIdsByType(self.m_SelectPartType)
            for i,v in ipairs(ids) do
                local data = Zhuangshiwu_Zhuangshiwu.GetData(v)
                if not (not bIsMingyuan and data.IsMingyuan == 1) then
                    local count = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(v)
                    table.insert(tbl, data.FurnishTemplate)
                end
            end
        end

        local compare = function (a, b)
            local data1 = Zhuangshiwu_Zhuangshiwu.GetData(a)
            local data2 = Zhuangshiwu_Zhuangshiwu.GetData(b)
            if data1.Type == data2.Type then
                return data1.Grade < data2.Grade
            else
                return data1.Type < data2.Type
            end
            --return data1.Grade < data2.Grade
        end
        table.sort(tbl, compare)
        CLuaZswTemplateMgr.CurSelectTypeTids[key] = tbl
    else
    end

    for i,id in ipairs(CLuaZswTemplateMgr.CurSelectTypeTids[key]) do--self.m_SelectPartType
        if id ~= self.m_DefaultTemplateId then
            local defaultRevolveSwitch = Zhuangshiwu_Zhuangshiwu.GetData(self.m_DefaultTemplateId).RevolveSwitch
            local revolveSwitch = Zhuangshiwu_Zhuangshiwu.GetData(id).RevolveSwitch
            if defaultRevolveSwitch == revolveSwitch or defaultRevolveSwitch ~= 1 then
                local count = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(id)
                local consumed = CLuaZswTemplateMgr.GetConsumedById(id)
                count = count - consumed
                if count > 0 then
                    table.insert(self.m_ShowTemplateIds,id)
                end
            end
        end
    end 
    self.m_TableView:ReloadData(true, true)

    local index = CLuaZswTemplateMgr.SelectRoIndex
    local roIndex = 0
    if CLuaZswTemplateMgr.CurSelectBarIndex == 0 then
        roIndex = CLuaZswTemplateMgr.TemplateIds4Huamu[index + 1].index
    elseif CLuaZswTemplateMgr.CurSelectBarIndex == 1 then
        roIndex = CLuaZswTemplateMgr.TemplateIds4Other[index + 1].index
    end
    self.m_SelectRoInfo = CLuaZswTemplateMgr.CurTemplateRoTbl[roIndex]--.ro--index+1
    self.m_SelectRoIndexEachType = roIndex

    if CLuaZswTemplateMgr.IsSelectItemFilled then
        self:InitClearView()
    end
end

function CLuaZswTemplateFillingComfirmWnd:InitItem(item, row)
    local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local countLabel = item.transform:Find("Count"):GetComponent(typeof(UILabel))
    local data = Zhuangshiwu_Zhuangshiwu.GetData(self.m_ShowTemplateIds[row + 1])
    if data then
        if data.Type ~= EnumFurnitureType_lua.eJianzhu then
            local itemtid = data.ItemId
            local itemdata = Item_Item.GetData(itemtid)
            if itemdata ~= nil then
                icon:LoadMaterial(itemdata.Icon)
            end
        else
            icon:LoadMaterial(data.Icon)
        end
        local count = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(data.FurnishTemplate)
        local consumed = CLuaZswTemplateMgr.GetConsumedById(data.FurnishTemplate)
        count = count - consumed
        countLabel.gameObject:SetActive(count > 0)
        countLabel.text = tostring(count)
        item.gameObject:SetActive(count > 0 and data.FurnishTemplate ~= self.m_DefaultTemplateId)
    end

  
end

function CLuaZswTemplateFillingComfirmWnd:InitDefaultItem()
    local icon = self.m_DefaultItem.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local countLabel = self.m_DefaultItem.transform:Find("Count"):GetComponent(typeof(UILabel))
    local mask = self.m_DefaultItem.transform:Find("Mask").gameObject
    local name = self.m_DefaultItem.transform:Find("Name"):GetComponent(typeof(UILabel))
    local desc = self.m_DefaultItem.transform:Find("Desc"):GetComponent(typeof(UILabel))
    local data = Zhuangshiwu_Zhuangshiwu.GetData(self.m_DefaultTemplateId)
    if data then
        if data.Type ~= EnumFurnitureType_lua.eJianzhu then
            local itemtid = data.ItemId
            local itemdata = Item_Item.GetData(itemtid)
            if itemdata ~= nil then
                icon:LoadMaterial(itemdata.Icon)
            end
        else
            icon:LoadMaterial(data.Icon)
        end
        local count = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(self.m_DefaultTemplateId)
        local consumed = CLuaZswTemplateMgr.GetConsumedById(self.m_DefaultTemplateId)
        count = count - consumed
        countLabel.gameObject:SetActive(count > 0)
        countLabel.text = tostring(count)
        mask:SetActive(count < 1)
        self.m_IsSelectDefault = count > 0
        name.text = data.Name
        desc.text = SafeStringFormat3(LocalString.GetString("%d级%s 家园%d级"),data.Grade,CClientFurnitureMgr.GetTypeNameByType(data.Type),data.HouseGrade)
    end
end

function CLuaZswTemplateFillingComfirmWnd:ClearOneAction()
    local data = Zhuangshiwu_Zhuangshiwu.GetData(self.m_DefaultTemplateId)
    local resName = data.ResName
    local resid = SafeStringFormat3("_%02d",data.ResId)
    local prefabname = "Assets/Res/Character/Jiayuan/" .. resName .. "/Prefab/" .. resName .. resid .. ".prefab"
    local ro = CLuaZswTemplateMgr.CurTemplateRoTbl[self.m_SelectRoIndexEachType].ro
    local fx = CEffectMgr.Inst:AddObjectFX(CLuaZswTemplateMgr.MaterialFxId, ro, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
    ro:AddFX(CLuaZswTemplateMgr.MaterialFxId, fx, -1)
    --ro:RemoveFX(CLuaZswTemplateMgr.MaterialFxId)
    ro:LoadMain(prefabname,nil,true,false,false)
    ro:RemoveFX("selfFx")
    local roIndex = self.m_SelectRoIndexEachType
    local lastFilledId = CLuaZswTemplateMgr.GetFillIdByRoIndex(roIndex)
    if lastFilledId then
        CLuaZswTemplateMgr.RevertConsumeById(lastFilledId)
    end
    CLuaZswTemplateMgr.CurTemplateRoTbl[roIndex].isFilled = false
    CLuaZswTemplateMgr.CurTemplateRoTbl[roIndex].id = self.m_DefaultTemplateId
    g_ScriptEvent:BroadcastInLua("OnFillPartOfZswTemplate",self.m_DefaultTemplateId,false)

    CUIManager.CloseUI(CLuaUIResources.ZswTemplateFillingComfirmWnd)
end

function CLuaZswTemplateFillingComfirmWnd:FillTemplateWithTid()
    local data = Zhuangshiwu_Zhuangshiwu.GetData(self.m_SelectFillingTid)
    local resName = data.ResName
    local resid = SafeStringFormat3("_%02d",data.ResId)
    local prefabname = "Assets/Res/Character/Jiayuan/" .. resName .. "/Prefab/" .. resName .. resid .. ".prefab"

    local templateData = Zhuangshiwu_ZswTemplate.GetData(CLuaZswTemplateMgr.SelectEditTemplateId)
    if not templateData then 
        return 
    end

    local templateIds = templateData.DefaultComponents
    local offsets = templateData.Offset
    local scales = templateData.Scale
    local rotations = templateData.Rotation
    
    --local ro = self.m_SelectRoInfo.ro
    local ro = CLuaZswTemplateMgr.CurTemplateRoTbl[self.m_SelectRoIndexEachType].ro
    local index = self.m_SelectRoIndexEachType - 1
    ro:RemoveFX(CLuaZswTemplateMgr.MaterialFxId)
    ro:RemoveFX("selfFx")
    ro:SetOutLinedEffect(false, Color.white)
    ro:LoadMain(prefabname,nil,true,false,false)
    ro.transform.localPosition = Vector3(offsets[index*3]/100,offsets[index*3+1]/100,offsets[index*3+2]/100)
    ro.Scale = scales[index]
    ro.transform.localRotation =Quaternion.Euler(Vector3(rotations[index*3],rotations[index*3+1],rotations[index*3+2]))-- Vector3(self.elurrotx[i],self.elurroty[i],self.elurrotz[i])
    ro.Layer = LayerDefine.ModelForNGUI_3D
    if data.FX >0 and self.m_SelectFillingTid ~= 9336 then
        local selffx = CEffectMgr.Inst:AddObjectFX(data.FX, ro, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
        ro:AddFX("selfFx", selffx, -1)
    end

    local roIndex = self.m_SelectRoIndexEachType
    local lastFilledId = CLuaZswTemplateMgr.GetFillIdByRoIndex(roIndex)
    if lastFilledId then
        CLuaZswTemplateMgr.RevertConsumeById(lastFilledId)
    end
    CLuaZswTemplateMgr.CurTemplateRoTbl[roIndex].isFilled = true
    CLuaZswTemplateMgr.CurTemplateRoTbl[roIndex].id = self.m_SelectFillingTid
    CLuaZswTemplateMgr.AddConsumeById(self.m_SelectFillingTid)
    g_ScriptEvent:BroadcastInLua("OnFillPartOfZswTemplate",self.m_SelectFillingTid,true)
    CUIManager.CloseUI(CLuaUIResources.ZswTemplateFillingComfirmWnd)
end

function CLuaZswTemplateFillingComfirmWnd:OnDestroy()
    --local ro = CLuaZswTemplateMgr.CurTemplateRoTbl[roIndex].ro--index+1\
    if self.m_SelectRoInfo then
        self.m_SelectRoInfo.ro:SetOutLinedEffect(false, Color.white)
    end
end

function CLuaZswTemplateFillingComfirmWnd:OnShopBtnClick()
    --跳转到玩家商店 家园 装饰物目录下
    CYuanbaoMarketMgr.ShowPlayerShopBuySection(7, 1)
    CUIManager.CloseUI(CLuaUIResources.ZswTemplateFillingComfirmWnd)
end

function CLuaZswTemplateFillingComfirmWnd:OnEnable()
    g_ScriptEvent:AddListener("SetRepositoryFurnitureCount",self,"Init")
end

function CLuaZswTemplateFillingComfirmWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SetRepositoryFurnitureCount",self,"Init")
end
