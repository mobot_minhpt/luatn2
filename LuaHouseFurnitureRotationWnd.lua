local CMainCamera = import "L10.Engine.CMainCamera"
local CCommonLuaWnd=import "L10.UI.CCommonLuaWnd"
local CHouseRollerCoasterMgr=import "L10.Game.CHouseRollerCoasterMgr"
local Vector3 = import "UnityEngine.Vector3"
local Quaternion = import "UnityEngine.Quaternion"
local Space = import "UnityEngine.Space"
local CUITexture=import "L10.UI.CUITexture"
local UICamera = import "UICamera"
local Camera = import "UnityEngine.Camera"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local UISlider = import "UISlider"
local ETickType = import "L10.Engine.ETickType"
local EnumFurnitureSliderOT = import "L10.UI.EnumFurnitureSliderOT"
local CZuoanFurnitureMgr = import "L10.UI.CZuoanFurnitureMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local CFurnitureSliderMgr = import "L10.UI.CFurnitureSliderMgr"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local QnNewSlider=import "L10.UI.QnNewSlider"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CClientFurniture = import "L10.Game.CClientFurniture"
local CTooltip=import "L10.UI.CTooltip"
local LineRenderer = import "UnityEngine.LineRenderer"
local Material = import "UnityEngine.Material"
local Shader = import "UnityEngine.Shader"
local LayerDefine = import "L10.Engine.LayerDefine"
local Physics = import "UnityEngine.Physics"
local CapsuleCollider = import "UnityEngine.CapsuleCollider"
local SphereCollider = import "UnityEngine.SphereCollider"
local MeshRenderer = import "UnityEngine.MeshRenderer"
local MeshCollider = import "UnityEngine.MeshCollider"
local MeshFilter = import "UnityEngine.MeshFilter"
local Vector2 = import "UnityEngine.Vector2"
local Mesh = import "UnityEngine.Mesh"
local BoxCollider = import "UnityEngine.BoxCollider"
local Screen = import "UnityEngine.Screen"
local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"

CLuaHouseFurnitureRotationWnd = class()
CLuaHouseFurnitureRotationWnd.s_ForceHideScaleSlider = false
CLuaHouseFurnitureRotationWnd.s_ForceHideHeightSlider = false
CLuaHouseFurnitureRotationWnd.s_OnlyRotateAxisY = false
CLuaHouseFurnitureRotationWnd.m_TargetFurniture = nil

RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_Data")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_Furniture")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_RO")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_ModelTexture")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_ModelTextureLoader")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_RotationBase")

RegistClassMember(CLuaHouseFurnitureRotationWnd,"Slider")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"FurnitureId")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_VerticalSlider")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_CircularSliderDatas")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_ScaleLabel")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_FloatingLabel")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_BackGround")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_BallCamera")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_LastDragVector")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_TemplateId")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_SubType")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_Wnd")

RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_AxisX")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_AxisY")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_AxisZ")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_AxisRoot")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_CurRotationMesh")

RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_MaxHeightValue")
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_Inited")



function CLuaHouseFurnitureRotationWnd:OnEnable()

end

function CLuaHouseFurnitureRotationWnd:OnDestroy()
    if CFurnitureSliderMgr.mRotateRpcTick ~= nil then
        invoke(CFurnitureSliderMgr.mRotateRpcTick)
        CFurnitureSliderMgr.mRotateRpcTick = nil
    end
    if CFurnitureSliderMgr.mHeightRpcTick ~= nil then
        invoke(CFurnitureSliderMgr.mHeightRpcTick)
        CFurnitureSliderMgr.mHeightRpcTick = nil
    end

    self:DoRotate()
    if self.m_Inited and not CLuaHouseFurnitureRotationWnd.s_ForceHideHeightSlider then
        self:OnVerticalValueChanged(self.m_VerticalSlider.m_Value, true)
    end

    if self.m_ModelTexture then
        self.m_ModelTexture.mainTexture=nil
    end
    CUIManager.DestroyModelTexture("__HouseHuaMuRotator__")

    if self.m_LineRenderers then
        for _, lr in pairs(self.m_LineRenderers) do
            if lr and lr.gameObject then
                GameObject.Destroy(lr.gameObject)
            end
        end
        self.m_LineRenderers = {}
    end

    if self.m_AssistSector then
        GameObject.Destroy(self.m_AssistSector)
        self.m_AssistSector = nil
    end

    if self.m_CurRotationMesh then
        GameObject.Destroy(self.m_CurRotationMesh)
        self.m_CurRotationMesh = nil
    end

    CLuaHouseFurnitureRotationWnd.s_ForceHideScaleSlider = false
    CLuaHouseFurnitureRotationWnd.s_ForceHideHeightSlider = false
    CLuaHouseFurnitureRotationWnd.s_OnlyRotateAxisY = false
    CLuaHouseFurnitureRotationWnd.m_TargetFurniture = nil
end

function CLuaHouseFurnitureRotationWnd:InitBall()
    local collider = self.transform:Find("AnchorRot/Base").gameObject
    self.m_ModelTexture = self.transform:Find("AnchorRot/Sprite"):GetComponent(typeof(CUITexture))
    self.m_RO = nil
    --self.m_Rot = nil
    UIEventListener.Get(collider).onDrag = DelegateFactory.VectorDelegate(function(g,delta)
        if CLuaHouseFurnitureRotationWnd.s_OnlyRotateAxisY then return end
        self:OnDrag(g,delta)
    end)
    UIEventListener.Get(collider).onDragEnd = DelegateFactory.VoidDelegate(function(g)
        if CLuaHouseFurnitureRotationWnd.s_OnlyRotateAxisY then return end
        self:OnDragEnd(g)
    end)

    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        ro.Scale = 6
        ro.transform.localPosition = Vector3(0.0,0.0,8)
        self.m_BallCamera = ro.transform.parent.parent.gameObject:GetComponent(typeof(Camera))
        self.m_RO = ro

        local inverse_cam = Quaternion.Inverse(CMainCamera.Main.transform.rotation)
        self.m_RO.transform.localRotation = inverse_cam

        self.m_Furniture = CLuaHouseMgr.s_EnableAdjustmentWhenAddFurniture and CLuaHouseFurnitureRotationWnd.m_TargetFurniture or CClientFurnitureMgr.Inst:GetFurniture(self.FurnitureId)
        self.m_RO.transform:Rotate(self.m_Furniture.RO.transform.localRotation.eulerAngles, Space.Self)
        ro:LoadMain("Fx/UI/Prefab/ball.prefab",nil,false,false)
        ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(RO) 
            self.m_ModelTexture.alpha = 1
            if CFurnitureScene.IsOpen360Rotation() then 
                self:InitAxisCtrl()
            end
        end))
    end)
    
    local texture = CUIManager.CreateModelTexture("__HouseHuaMuRotator__", self.m_ModelTextureLoader,0,0,0,0,false,true,1,false)
    RegisterTickOnce(function()
        if not CommonDefs.IsNull(self.m_ModelTexture) and not CommonDefs.IsNull(texture) then
            self.m_ModelTexture.mainTexture = texture
        end
    end, 33)
    self.m_BallCamera.fieldOfView = 40
end

function CLuaHouseFurnitureRotationWnd:OnDragStart( g)

end

function CLuaHouseFurnitureRotationWnd:VectorAdd(a, b)
    if a and b then return Vector3(a.x + b.x, a.y + b.y, a.z + b.z) end
end

function CLuaHouseFurnitureRotationWnd:VectorSub(a, b)
    if a and b then return Vector3(a.x - b.x, a.y - b.y, a.z - b.z) end
end

function CLuaHouseFurnitureRotationWnd:ScaleVector(a, _scale)
    if a and _scale then return Vector3(a.x * _scale, a.y * _scale, a.z * _scale) end
end

function CLuaHouseFurnitureRotationWnd:GetBallIntersection(g)
    --将光标位置反向投影至视空间，再与球求交，球的视空间中心点为（0,0,8）半径为2
    local pos = g.transform:InverseTransformPoint(UICamera.lastWorldPosition)
    local remapped = {pos.x * self.m_BallCamera.pixelWidth / 250 + (self.m_BallCamera.pixelWidth * 0.5), pos.y * self.m_BallCamera.pixelHeight / 250 + (self.m_BallCamera.pixelHeight * 0.5)}
    local ws_pt = self.m_BallCamera.transform:InverseTransformPoint(self.m_BallCamera:ScreenToWorldPoint(Vector3(remapped[1], remapped[2], 100))).normalized
    local pt_center = Vector3(0,0,8)
    local dot = (pt_center.x * ws_pt.x + pt_center.y * ws_pt.y + pt_center.z * ws_pt.z)
    local intersect_vect = Vector3(dot * ws_pt.x - pt_center.x, dot * ws_pt.y - pt_center.y, dot * ws_pt.z - pt_center.z)
    local pt_radius = 2
    local sqrLeng = pt_radius * pt_radius - intersect_vect.sqrMagnitude
    if(sqrLeng >= 0)then
        local leng = math.sqrt(sqrLeng)
        local return_val = self:VectorSub(self:ScaleVector(ws_pt, dot), self:ScaleVector(ws_pt, leng))
        return return_val
    end
    return nil
end

function CLuaHouseFurnitureRotationWnd:OnDrag(g, delta)
    self.m_Furniture = CLuaHouseMgr.s_EnableAdjustmentWhenAddFurniture and CLuaHouseFurnitureRotationWnd.m_TargetFurniture or CClientFurnitureMgr.Inst:GetFurniture(self.FurnitureId)
    if(self.m_Furniture)then
        local pos = self:GetBallIntersection(g)
        if(pos)then
            local new_vect = self:VectorSub(pos, {x = 0, y = 0, z = 8})
            if(self.m_LastDragVector)then
                --先得到视空间下的起始向量和终向量，转换至世界空间，然后使用FromToRotation得到相对旋转量，附加在物体上
                local last_vec = Vector3(self.m_LastDragVector.x, self.m_LastDragVector.y, self.m_LastDragVector.z)
                local new_vec =  Vector3(new_vect.x, new_vect.y, new_vect.z)
                local ws_last_vec = CommonDefs.op_Multiply_Quaternion_Vector3(CMainCamera.Main.transform.rotation, last_vec)
                local ws_new_vec = CommonDefs.op_Multiply_Quaternion_Vector3(CMainCamera.Main.transform.rotation, new_vec)
                --local delta_rot = Quaternion.FromToRotation(Vector3(self.m_LastDragVector.x, self.m_LastDragVector.y, self.m_LastDragVector.z).normalized, Vector3(new_vect.x, new_vect.y, new_vect.z).normalized)
                local wsdelta_rot = Quaternion.FromToRotation(ws_last_vec, ws_new_vec)
                self.m_Furniture.RO.transform.localRotation = CommonDefs.op_Multiply_Quaternion_Quaternion(wsdelta_rot, self.m_Furniture.RO.transform.localRotation)
                local inverse_cam = Quaternion.Inverse(CMainCamera.Main.transform.rotation)
                self.m_RO.transform.localRotation = inverse_cam
                self.m_RO.transform:Rotate(self.m_Furniture.RO.transform.localRotation.eulerAngles, Space.Self)
            end
            self.m_LastDragVector = new_vect
        else
            --当鼠标越界时，使用delta进行更新
            self.m_LastDragVector = nil
            local delta_ws_vec = CommonDefs.op_Multiply_Quaternion_Vector3(CMainCamera.Main.transform.rotation, Vector3(delta.x,delta.y,-32).normalized)
            local default_ws_last_vec = CommonDefs.op_Multiply_Quaternion_Vector3(CMainCamera.Main.transform.rotation, Vector3(0,0,-1))
            local wsdelta_rot = Quaternion.FromToRotation(default_ws_last_vec, delta_ws_vec)
            self.m_Furniture.RO.transform.localRotation = CommonDefs.op_Multiply_Quaternion_Quaternion(wsdelta_rot, self.m_Furniture.RO.transform.localRotation)
            local inverse_cam = Quaternion.Inverse(CMainCamera.Main.transform.rotation)
            self.m_RO.transform.localRotation = inverse_cam
            self.m_RO.transform:Rotate(self.m_Furniture.RO.transform.localRotation.eulerAngles, Space.Self)
        end
    end
end

function CLuaHouseFurnitureRotationWnd:OnDragEnd(g, delta)
    -- 只能水平360°旋转的障碍物，取消后要恢复原来的旋转角度, 不能进行RpcTick记录当前旋转角度
    if CLuaHouseFurnitureRotationWnd.s_OnlyRotateAxisY then return end 

    self.m_LastDragVector = nil
    if CFurnitureSliderMgr.mRotateRpcTick ~= nil then
        invoke(CFurnitureSliderMgr.mRotateRpcTick)
        CFurnitureSliderMgr.mRotateRpcTick = nil
    end
    CLuaClientFurnitureMgr.m_ProcessedFurnitureId = self.FurnitureId

    CFurnitureSliderMgr.mRotateRpcTick = CTickMgr.Register(DelegateFactory.Action(function()
        local furnitureId = CLuaClientFurnitureMgr.m_ProcessedFurnitureId
        self.m_Furniture = CLuaHouseMgr.s_EnableAdjustmentWhenAddFurniture and CLuaHouseFurnitureRotationWnd.m_TargetFurniture or CClientFurnitureMgr.Inst:GetFurniture(furnitureId)
        if self.m_Furniture and self.m_Furniture:IsPlacedInServer() then
            local eulerAngles = self.m_Furniture.RO.transform.localEulerAngles
            local rx = math.max(0,math.floor(eulerAngles.x/360*256 + 0.5))
            local ry = math.max(0,math.floor(eulerAngles.y/360*256 + 0.5))
            local rz = math.max(0,math.floor(eulerAngles.z/360*256 + 0.5))
            Gac2Gas.RotateFurniture(furnitureId, rx,ry,rz)
        end
    end), 2000, ETickType.Once)
end
--关闭窗口就直接保存
function CLuaHouseFurnitureRotationWnd:DoRotate()
     -- 只能水平360°旋转的障碍物，取消后要恢复原来的旋转角度, 不能进行RpcTick保存当前旋转角度
    if CLuaHouseFurnitureRotationWnd.s_OnlyRotateAxisY then return end
    self.m_Furniture = CLuaHouseMgr.s_EnableAdjustmentWhenAddFurniture and CLuaHouseFurnitureRotationWnd.m_TargetFurniture or CClientFurnitureMgr.Inst:GetFurniture(self.FurnitureId)
    if self.m_Furniture and self.m_Furniture.RO and self.m_Furniture:IsPlacedInServer() then
        local eulerAngles = self.m_Furniture.RO.transform.localEulerAngles
        local rx = math.max(0,math.floor(eulerAngles.x/360*256 + 0.5))
        local ry = math.max(0,math.floor(eulerAngles.y/360*256 + 0.5))
        local rz = math.max(0,math.floor(eulerAngles.z/360*256 + 0.5))
        Gac2Gas.RotateFurniture(self.FurnitureId, rx,ry,rz)
    end
end

--复用Furniture Sliders
function CLuaHouseFurnitureRotationWnd:Awake()
    self.Slider = self.transform:Find("Sliders/Anchor/NumberSlider"):GetComponent(typeof(QnNewSlider))
    self.m_VerticalSlider = self.transform:Find("Sliders/Anchor/VerticalSlider"):GetComponent(typeof(QnNewSlider))
    self.m_ScaleLabel = self.transform:Find("Sliders/Anchor/NumberSlider/Label"):GetComponent(typeof(UILabel))
    self.m_FloatingLabel = self.transform:Find("Sliders/Anchor/VerticalSlider/Label"):GetComponent(typeof(UILabel))
    self.m_BackGround = self.transform:Find("BackGround").gameObject

    self.m_RotationBase = self.transform:Find("AnchorRot").gameObject

    self.m_AxisRoot = self.m_RotationBase.transform:Find("AxisVal")
    self.m_AxisX = self.m_AxisRoot:Find("AxisX/Val"):GetComponent(typeof(UILabel))
    self.m_AxisY = self.m_AxisRoot:Find("AxisY/Val"):GetComponent(typeof(UILabel))
    self.m_AxisZ = self.m_AxisRoot:Find("AxisZ/Val"):GetComponent(typeof(UILabel))

    self.FurnitureId = 0

    --huamu
    self.m_VerticalSlider.gameObject:SetActive(false)

    self.m_Wnd=self.gameObject:GetComponent(typeof(CCommonLuaWnd))

    local heightInterval = Zhuangshiwu_Setting.GetData().PlantOffsetInterval
    self.m_MaxHeightValue = math.floor((heightInterval[1] - heightInterval[0]) / (8 - heightInterval[0]) * 100)
end


function CLuaHouseFurnitureRotationWnd:Init()
    self.m_Furniture = CLuaHouseMgr.s_EnableAdjustmentWhenAddFurniture and CLuaHouseFurnitureRotationWnd.m_TargetFurniture or CClientFurnitureMgr.Inst:GetFurniture(CFurnitureSliderMgr.CurFurnitureId)

    local enableXuanFu = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.IsOpenHouseXuanFuEdit>0 or false

    --花木类支持竖直方向移动
    if self.m_Furniture ~= nil then
        self.m_TemplateId = self.m_Furniture.TemplateId
        local data = Zhuangshiwu_Zhuangshiwu.GetData(self.m_Furniture.TemplateId)
        self.m_Data = data
        self.m_SubType = data.SubType
        
        if data and data.RevolveSwitch == 1 then
            if data.SubType ~= EnumFurnitureSubType_lua.ePoolFurniture then
                if(enableXuanFu)then
                    self.m_VerticalSlider.gameObject:SetActive(true)
                end
            else
                if data.ID == 9315 or data.ID == 9316 or data.ID == 9317 then -- 小黄鸭特殊处理, 可以调大小，但只能绕Y轴360°旋转
                    CLuaHouseFurnitureRotationWnd.s_OnlyRotateAxisY = true
                end
            end
        end
        if self.m_SubType == EnumFurnitureSubType_lua.eRollerCoasterTrack then
            self.Slider.gameObject:SetActive(false)
            self.m_VerticalSlider.gameObject:SetActive(true)
        end

        self:InitFurniture(CFurnitureSliderMgr.CurFurnitureId)

        if self.m_SubType == EnumFurnitureSubType_lua.eRollerCoasterTrack then
            self.transform:Find("AnchorRot").gameObject:SetActive(false)
        else
            self:InitBall()
        end

        if CFurnitureScene.IsOpen360Rotation() then
            self.m_RotationBase:SetActive(data.RevolveSwitch ~= 0 or data.Revolve90 ~= 0)
            self.m_BackGround:SetActive(data.RevolveSwitch ~= 0 or data.Revolve90 ~= 0)
            self.m_AxisRoot.gameObject:SetActive(data.RevolveSwitch ~= 0 or data.Revolve90 ~= 0)
            
            if CLuaHouseFurnitureRotationWnd.s_OnlyRotateAxisY then
                self.m_AxisX.transform.parent.gameObject:SetActive(false)
                self.m_AxisZ.transform.parent.gameObject:SetActive(false)
            end
        else
            if self.m_ModelTexture then
                for i = 0, self.m_ModelTexture.transform.childCount - 1 do
                    self.m_ModelTexture.transform:GetChild(i).gameObject:SetActive(false)
                end
            end
            self.m_RotationBase:SetActive(data.RevolveSwitch == 1)
            self.m_BackGround:SetActive(data.RevolveSwitch == 1)
            self.m_AxisRoot.gameObject:SetActive(false)
        end

        self.Slider.OnValueChanged = DelegateFactory.Action_float(function(v)
            self:OnValueChanged(v)
        end)

        self.m_VerticalSlider.OnValueChanged = DelegateFactory.Action_float(function(v)
            self:OnVerticalValueChanged(v)
        end)
        
        CommonDefs.AddOnClickListener(self.Slider.transform:Find("plus_icon").gameObject, 
            DelegateFactory.Action_GameObject(function()
                self.Slider.m_Value = self.Slider.m_Value + 1/99
            end), 
        false)

        CommonDefs.AddOnClickListener(self.Slider.transform:Find("min_icon").gameObject, 
            DelegateFactory.Action_GameObject(function()
                self.Slider.m_Value = self.Slider.m_Value - 1/99
            end), 
        false)

        CommonDefs.AddOnClickListener(self.m_VerticalSlider.transform:Find("plus_icon").gameObject, 
            DelegateFactory.Action_GameObject(function()
                self.m_VerticalSlider.m_Value = self.m_VerticalSlider.m_Value + 1/self.m_MaxHeightValue
            end), 
        false)

        CommonDefs.AddOnClickListener(self.m_VerticalSlider.transform:Find("min_icon").gameObject, 
            DelegateFactory.Action_GameObject(function()
                self.m_VerticalSlider.m_Value = self.m_VerticalSlider.m_Value - 1/self.m_MaxHeightValue
            end), 
        false)
    end

    if CLuaHouseFurnitureRotationWnd.s_ForceHideScaleSlider then
        self.Slider.gameObject:SetActive(false)
    end
    if CLuaHouseFurnitureRotationWnd.s_ForceHideHeightSlider then 
        self.m_VerticalSlider.gameObject:SetActive(false)
    end

    self.m_Inited = true
end

function CLuaHouseFurnitureRotationWnd:OnDisable( )
    self.m_Furniture = CLuaHouseMgr.s_EnableAdjustmentWhenAddFurniture and CLuaHouseFurnitureRotationWnd.m_TargetFurniture or CClientFurnitureMgr.Inst:GetFurniture(self.FurnitureId)
    if not CommonDefs.IsNull(self.m_Furniture) then
        if CClientFurnitureMgr.Inst:IsPenjingFurniture(self.m_Furniture.TemplateId) then
            CZuoanFurnitureMgr.Inst:ShowHuamuPopupMenu()
        else
            CClientFurnitureMgr.Inst.CurFurniture = self.m_Furniture
            self.m_Furniture.Selected = true
            self.m_Furniture.Moving = true
            self.m_Furniture:ShowActionPopupMenu()
        end
    end
end

function CLuaHouseFurnitureRotationWnd:InitFurniture( furnitureId) 
    self.m_Furniture = CLuaHouseMgr.s_EnableAdjustmentWhenAddFurniture and CLuaHouseFurnitureRotationWnd.m_TargetFurniture or CClientFurnitureMgr.Inst:GetFurniture(furnitureId)
    if self.m_Furniture then
        self.FurnitureId = furnitureId
        --self:InitCirculateSliders()

        if CFurnitureSliderMgr.CurOT == EnumFurnitureSliderOT.eScale or CFurnitureSliderMgr.CurOT == EnumFurnitureSliderOT.ePenzaiHuamuScale then
            self:InitForScale(self.m_Furniture)
        end

        --可上下移动
        if self.m_VerticalSlider.gameObject.activeSelf then
            local y = self.m_Furniture.RO.Position.y
            local x = self.m_Furniture.RO.Position.x
            local z = self.m_Furniture.RO.Position.z
            local xOffset = x - math.floor(x)
            local zOffset = z - math.floor(z)

            local xPos = math.floor(x) + 0.5
            local zPos = math.floor(z) + 0.5

            local posY = CClientFurnitureMgr.Inst:GetLogicHeightByTemplateId(self.m_Furniture.TemplateId,xPos, zPos)

            if self.m_SubType == EnumFurnitureSubType_lua.eRollerCoasterTrack then
                posY = CHouseRollerCoasterMgr.GetBaseHeight()
            end
            local yOffset = y - posY

            local heightInterval = Zhuangshiwu_Setting.GetData().PlantOffsetInterval
            local percent = (yOffset - heightInterval[0]) / (heightInterval[1] - heightInterval[0])
            percent = math.min(math.max(percent, 0), 1)

            -- self.m_CircularSliderDatas[1].Angle = self.m_CircularSliderDatas[2].MaxAngle * percent
            -- self.m_CircularSliderDatas[1].Value = percent
            -- self:UpdateCircularSlider(self.m_CircularSliderDatas[1], {x = 0, y = 0})

            self.m_VerticalSlider:GetComponent(typeof(UISlider)).value = percent
        end
    end
end
function CLuaHouseFurnitureRotationWnd:InitForScale( furniture) 
    local fScale = 1
    if CFurnitureSliderMgr.CurOT == EnumFurnitureSliderOT.eScale then
        fScale = furniture.RO.Scale
    elseif CFurnitureSliderMgr.CurOT == EnumFurnitureSliderOT.ePenzaiHuamuScale and furniture.PenzaiRO ~= nil then
        fScale = furniture.PenzaiRO.Scale
    end

    if furniture.IsWinter then
        fScale = furniture.m_OriginScale
    end

    local scaleInterval = Zhuangshiwu_Setting.GetData().FurnitureScaleInterval
    local percent = 0.5
    if fScale >= 1 then
        percent = percent + ((fScale - 1) / (scaleInterval[0] - 1) * 0.5)
    else
        percent = (fScale - scaleInterval[1]) / (1 - scaleInterval[1]) * 0.5
    end

    -- self.m_CircularSliderDatas[2].Angle = self.m_CircularSliderDatas[2].MaxAngle * percent
    -- self.m_CircularSliderDatas[2].Value = percent
    -- self:UpdateCircularSlider(self.m_CircularSliderDatas[2], {x = 0, y = 0})
    CommonDefs.GetComponent_Component_Type(self.Slider, typeof(UISlider)).value = percent
end
function CLuaHouseFurnitureRotationWnd:OnValueChanged(value) 
    self.m_ScaleLabel.text = tostring(math.min(99, math.floor(value * 100)))
    self.m_Furniture = CLuaHouseMgr.s_EnableAdjustmentWhenAddFurniture and CLuaHouseFurnitureRotationWnd.m_TargetFurniture or CClientFurnitureMgr.Inst:GetFurniture(self.FurnitureId)
    if self.m_Furniture then
        local newScale = 1
        local percent = value

        local scaleInterval = Zhuangshiwu_Setting.GetData().FurnitureScaleInterval
        if percent >= 0.5 then
            newScale = (percent - 0.5) / 0.5 * (scaleInterval[0] - 1) + 1
        else
            newScale = percent / 0.5 * (1 - scaleInterval[1]) + scaleInterval[1]
        end
        local ui8Scale = CClientFurnitureMgr.GetScaleFloatToUI8(newScale)

        if CFurnitureSliderMgr.CurOT == EnumFurnitureSliderOT.eScale then
            self.m_Furniture.RO.Scale = newScale
            self.m_Furniture:UpdateWinterSkinScale(newScale)
        elseif CFurnitureSliderMgr.CurOT == EnumFurnitureSliderOT.ePenzaiHuamuScale then
            self.m_Furniture.PenzaiHuamuScale = newScale
        end
        if self.m_Data then
            self.m_Furniture.RO:RemoveFX("SelfFx")
            if self.m_Furniture.IsWinter and self.m_Data.FXXue > 0 then
                CClientFurniture.AddSelfFx(self.m_Data.FXXue, self.m_Furniture)
            else
                CClientFurniture.AddSelfFx(self.m_Data.FX, self.m_Furniture)
            end
        end

        self:RegisterScaleTick(self.FurnitureId, ui8Scale)
    end
end

function CLuaHouseFurnitureRotationWnd:OnVerticalValueChanged(value, doUpdate)
    local heightInterval = Zhuangshiwu_Setting.GetData().PlantOffsetInterval
    local fixvalue = value * (heightInterval[1] - heightInterval[0])
    fixvalue = fixvalue / (8 - heightInterval[0])

    self.m_FloatingLabel.text = tostring(math.floor(fixvalue * 100))
    self.m_Furniture = CLuaHouseMgr.s_EnableAdjustmentWhenAddFurniture and CLuaHouseFurnitureRotationWnd.m_TargetFurniture or CClientFurnitureMgr.Inst:GetFurniture(self.FurnitureId)
    if self.m_Furniture then
        local percent =value
        local heightInterval = Zhuangshiwu_Setting.GetData().PlantOffsetInterval
        local yOffset = heightInterval[0]+(heightInterval[1]-heightInterval[0])*percent

        if self.m_SubType == EnumFurnitureSubType_lua.eRollerCoasterTrack then
            yOffset = math.floor(yOffset+0.5)--取整
            -- yOffset = math.max(0,yOffset)
        end

        local y = self.m_Furniture.RO.Position.y
        local x = self.m_Furniture.RO.Position.x
        local z = self.m_Furniture.RO.Position.z

        local xPos = math.floor( x )+0.5
        local zPos = math.floor( z )+0.5

        local posY = CClientFurnitureMgr.Inst:GetLogicHeightByTemplateId(self.m_Furniture.TemplateId,xPos, zPos)
        if self.m_SubType == EnumFurnitureSubType_lua.eRollerCoasterTrack then
            --找一个基准高度，50,50是名园和普通家园的内部
            posY = CHouseRollerCoasterMgr.GetBaseHeight()
        end
        self.m_Furniture.RO.Position = Vector3(x,posY+yOffset+CClientFurniture.s_YOffset,z)

        local xOffset = x-math.floor( x )
        local zOffset = z-math.floor( z )

        if CFurnitureSliderMgr.mHeightRpcTick ~= nil then
            invoke(CFurnitureSliderMgr.mHeightRpcTick)
            CFurnitureSliderMgr.mHeightRpcTick = nil
        end
        CLuaClientFurnitureMgr.m_ProcessedFurnitureId = self.FurnitureId

        if doUpdate then
            if CLuaClientFurnitureMgr.m_ProcessedFurnitureId and self.m_Furniture and self.m_Furniture:IsPlacedInServer() then
                Gac2Gas.ShiftFurniture(CLuaClientFurnitureMgr.m_ProcessedFurnitureId, xOffset, yOffset, zOffset)
            end
        else
            CFurnitureSliderMgr.mHeightRpcTick = CTickMgr.Register(DelegateFactory.Action(function()
                if CLuaClientFurnitureMgr.m_ProcessedFurnitureId and self.m_Furniture and self.m_Furniture:IsPlacedInServer() then
                    Gac2Gas.ShiftFurniture(CLuaClientFurnitureMgr.m_ProcessedFurnitureId, xOffset, yOffset, zOffset)
                end
            end), 1000, ETickType.Once)
        end
    end
end

function CLuaHouseFurnitureRotationWnd:RegisterScaleTick( fid, scale) 
    -- 带障碍装饰物，取消后要恢复原来的缩放, 不能进行RpcTick保存当前缩放
    if CLuaHouseFurnitureRotationWnd.s_OnlyRotateAxisY then return end

    self:UnRegisterScaleTick()
    CLuaClientFurnitureMgr.m_ProcessedFurnitureId = fid
    CFurnitureSliderMgr.mScaleRpcTick = CTickMgr.Register(DelegateFactory.Action(function () 
        local funitureId = CLuaClientFurnitureMgr.m_ProcessedFurnitureId
        self.m_Furniture = CLuaHouseMgr.s_EnableAdjustmentWhenAddFurniture and CLuaHouseFurnitureRotationWnd.m_TargetFurniture or CClientFurnitureMgr.Inst:GetFurniture(funitureId)
        if self.m_Furniture and self.m_Furniture:IsPlacedInServer() then
            -- print(CFurnitureSliderMgr.CurOT,EnumFurnitureSliderOT.eScale)
            if CFurnitureSliderMgr.CurOT == EnumFurnitureSliderOT.eScale then
                Gac2Gas.ScaleFurniture(funitureId, scale)
            elseif CFurnitureSliderMgr.CurOT == EnumFurnitureSliderOT.ePenzaiHuamuScale then --PenjingFurniture已经没用了
                Gac2Gas.ScalePenzaiHuamu(funitureId, scale)
            end
            CFurnitureSliderMgr.mScaleRpcTick = nil
        end
    end), 1000, ETickType.Once)
end


function CLuaHouseFurnitureRotationWnd:UnRegisterScaleTick( )
    if CFurnitureSliderMgr.mScaleRpcTick ~= nil then
        invoke(CFurnitureSliderMgr.mScaleRpcTick)
        CFurnitureSliderMgr.mScaleRpcTick = nil
    end
end

function CLuaHouseFurnitureRotationWnd:Update()
    if CFurnitureScene.IsOpen360Rotation() then
        self:UpdateAxisDisplay()
    end

    local furRightTopWnd = CUIManager.instance:GetGameObject(CUIResources.FurnitureRightTopWnd)
    local camBtn = furRightTopWnd.transform:Find("TopLeft/Grid/CameraBtn").gameObject
    local viewBtn = furRightTopWnd.transform:Find("TopLeft/Grid/CameraBtn/ViewButtons").gameObject
    for i = 0, 4 do
        UIEventListener.Get(viewBtn.transform:GetChild(i).gameObject):OnPress(false)
    end

    if Input.GetMouseButtonDown(0) then
        local hitOut = false
        local hitCnt = 0
        local cam = UICamera.currentCamera
        local hits = Physics.RaycastAll(cam:ScreenPointToRay(Input.mousePosition), cam.farClipPlane, cam.cullingMask)
        if hits then
            for i = 0, hits.Length - 1 do
                local hitData = hits[i]
                if hitData.collider.gameObject == camBtn then
                    UIEventListener.Get(camBtn):OnClick()
                    return
                elseif hitData.collider.transform.parent.gameObject == viewBtn then
                    UIEventListener.Get(hitData.collider.gameObject):OnPress(true)
                    if hitData.collider.gameObject.name == "Reset" then
                        UIEventListener.Get(hitData.collider.gameObject):OnClick()
                    end
                    return
                elseif CUICommonDef.IsChild(self.transform, hitData.collider.transform) then
                    hitCnt = hitCnt + 1
                    if hitData.collider.gameObject.name == "RaycastCheck" then
                        hitOut = true
                    end
                end
            end
            if hitOut and hitCnt == 1 then 
                if self.m_Wnd then
                    self.m_Wnd:Close()
                end
            end
        end
    elseif Input.GetMouseButton(0) then
        local cam = UICamera.currentCamera
        local hits = Physics.RaycastAll(cam:ScreenPointToRay(Input.mousePosition), cam.farClipPlane, cam.cullingMask)
        if hits then
            for i = 0, hits.Length - 1 do
                local hitData = hits[i]
                if not CommonDefs.IsNull(hitData.collider.transform.parent) then
                    if hitData.collider.transform.parent.gameObject == viewBtn and hitData.collider.gameObject.name ~= "Reset" then 
                        UIEventListener.Get(hitData.collider.gameObject):OnPress(true)
                    end
                end
            end
        end
    end
end

--@region 旋转控制辅助

CLuaHouseFurnitureRotationWnd.s_AxisArcRadius = 1.8 -- 旋转轴对应弧线的半径
CLuaHouseFurnitureRotationWnd.s_AxisArcSegments = 50 -- 弧线的分割数

CLuaHouseFurnitureRotationWnd.s_AssistSectorRadius = 1.8 -- 旋转辅助扇形的外半径
CLuaHouseFurnitureRotationWnd.s_AssistSectorInnerRadius = 0 -- 扇形的内半径
CLuaHouseFurnitureRotationWnd.s_AssistSectorSegments = 60 -- 扇形的分割数

RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_LineRenderers") 
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_AssistSector") -- 旋转辅助扇形
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_AxisBtn") -- 各轴控制按钮
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_AxisBtnOriPos") -- 按钮的初始屏幕坐标
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_LastAxisBtnPos") -- 拖动中按钮上次的屏幕坐标
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_BallCenter") -- 球心的屏幕坐标
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_AxisRing") -- 控制按钮所在的环
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_AxisRingRadius") -- 控制按钮所在的环的屏幕半径
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_AxisColor") -- 各轴lineRenderer颜色
RegistClassMember(CLuaHouseFurnitureRotationWnd,"m_DraggedAngle") -- 本次已拖动的总角度


function CLuaHouseFurnitureRotationWnd:InitAxisCtrl()
    self.m_AxisColor = {}
    self.m_AxisColor[0] = NGUIText.ParseColor24("3cb7fc", 0) -- x
    self.m_AxisColor[1] = NGUIText.ParseColor24("fcff05", 0) -- y
    self.m_AxisColor[2] = NGUIText.ParseColor24("ff4a4a", 0) -- z
    self.m_AxisColor[3] = NGUIText.ParseColor24("ffa800", 0) -- highlighted

    self.m_AxisRing = {}
    self.m_AxisRing[0] = self.m_ModelTexture.transform:Find("Ring").gameObject
    self.m_AxisRing[1] = self.m_ModelTexture.transform:Find("Ring_Full").gameObject

    self.m_AxisBtn = {}
    self.m_LineRenderers = {} 
    for i = 0, 2 do
        self.m_AxisBtn[i] = self.m_ModelTexture.transform:Find("Btn"..i)
        UIEventListener.Get(self.m_AxisBtn[i].gameObject).onDragStart = DelegateFactory.VoidDelegate(function(g)
            self:OnAxisBtnDragStart(i)
        end)
        UIEventListener.Get(self.m_AxisBtn[i].gameObject).onDrag = DelegateFactory.VectorDelegate(function(g, delta)
            self:OnAxisBtnDrag(delta, i)
        end)
        UIEventListener.Get(self.m_AxisBtn[i].gameObject).onDragEnd = DelegateFactory.VoidDelegate(function(g)
            self:OnAxisBtnDragEnd(i)
        end)

        if not CLuaHouseFurnitureRotationWnd.s_OnlyRotateAxisY or i == 1 then
            self.m_AxisBtn[i]:Find("Normal").gameObject:SetActive(true)
        else
            self.m_AxisBtn[i].gameObject:SetActive(false)
        end

        local go = CreateFromClass(GameObject, "LineRendererAxis"..i)
        go.layer = LayerDefine.ModelForNGUI_3D
        go.transform.parent = self.m_RO.transform
        go.transform.localPosition = Vector3.zero

        --local col = CLuaHouseFurnitureRotationWnd.s_OnlyRotateAxisY and i ~= 1 and Color.white or self.m_AxisColor[i]
        self.m_LineRenderers[i] = go:AddComponent(typeof(LineRenderer))
        self.m_LineRenderers[i].material = CreateFromClass(Material, Shader.Find("Sprites/Default"))
        self.m_LineRenderers[i]:SetColors(self.m_AxisColor[3], self.m_AxisColor[3])
        self.m_LineRenderers[i]:SetWidth(0.05, 0.05)
        self.m_LineRenderers[i]:SetVertexCount(CLuaHouseFurnitureRotationWnd.s_AxisArcSegments + 1)
        self.m_LineRenderers[i].useWorldSpace = true
    end

    self.m_AssistSector = CreateFromClass(GameObject, "AssistSector")
    self.m_AssistSector.layer = LayerDefine.ModelForNGUI_3D
    self.m_AssistSector.transform.parent = self.m_RO.transform
    self.m_AssistSector.transform.localPosition = Vector3.zero
    self.m_AssistSector:SetActive(false)
    self.m_AssistSector:AddComponent(typeof(MeshFilter))
    local mr = self.m_AssistSector:AddComponent(typeof(MeshRenderer))
    local col = Color.white
    col.a = 0.3
    mr.material = CreateFromClass(Material, Shader.Find("Sprites/Default"))
    mr.material.color = col
    
    if CLuaHouseFurnitureRotationWnd.s_OnlyRotateAxisY then
        self.m_AxisRing[0]:SetActive(false)
        self.m_AxisRing[1]:SetActive(true)
    else
        self.m_AxisRing[0]:SetActive(true)
        self.m_AxisRing[1]:SetActive(false)
    end
end

--[[function CLuaHouseFurnitureRotationWnd:CreateSectorMesh(angle)
    --print("CreateSectorMesh:", angle)
    local radius = CLuaHouseFurnitureRotationWnd.s_AssistSectorRadius
    local inRadius = CLuaHouseFurnitureRotationWnd.s_AssistSectorInnerRadius
    local seg = CLuaHouseFurnitureRotationWnd.s_AssistSectorSegments
    local angleRad = math.rad(angle)

    local vCnt = seg * 2 + 2 -- 扇形内圆+外圆的圆弧上顶点的个数
    local vertices = {}
    local curRad = angleRad
    local delta = angleRad / seg
    for i = 1, vCnt, 2 do
        local cosa = math.cos(curRad)
        local sina = math.sin(curRad)
        vertices[i] = Vector3(radius * cosa, radius * sina, 0)
        vertices[i + 1] = Vector3(inRadius * cosa, inRadius * sina, 0)
        curRad = curRad - delta
    end

    local tCnt = seg * 6 -- 三角形个数
    local triangles = {}
    local ti = 1
    local vi = 0 -- After conversion to Vector3, idx starts at 0
    while ti <= tCnt do
        triangles[ti] = vi
        triangles[ti + 1] = vi + 3
        triangles[ti + 2] = vi + 1
        triangles[ti + 3] = vi + 2
        triangles[ti + 4] = vi + 3
        triangles[ti + 5] = vi
        ti = ti + 6
        vi = vi + 2
    end

    local uvs = {}
    for i = 1, vCnt do
        local x = tonumber(vertices[i].x)
        local y = tonumber(vertices[i].y)
        if x and y then
            uvs[i] = Vector2(math.floor(x / radius / 2) + 0.5, math.floor(y / radius / 2) + 0.5)
        else
            return
        end
    end

    local mesh = CreateFromClass(Mesh)
    mesh.vertices = Table2Array(vertices, MakeArrayClass(Vector3))
    mesh.triangles = Table2Array(triangles, MakeArrayClass(Int32))
    mesh.uv = Table2Array(uvs, MakeArrayClass(Vector2))

    return mesh
end--]]

function CLuaHouseFurnitureRotationWnd:DrawAssistSector(axis, angle)
    if angle < 0 then angle = math.max(angle, -720)
    else angle = math.min(angle, 720) end
    --local mesh = self:CreateSectorMesh(angle)
    self.m_CurRotationMesh = CClientFurnitureMgr.Inst:CreateSectorMeshForRotation(angle, self.m_CurRotationMesh, 
        CLuaHouseFurnitureRotationWnd.s_AssistSectorSegments, 
        CLuaHouseFurnitureRotationWnd.s_AssistSectorRadius, 
        CLuaHouseFurnitureRotationWnd.s_AssistSectorInnerRadius)
    if self.m_CurRotationMesh then
        self.m_AssistSector:GetComponent(typeof(MeshFilter)).mesh = self.m_CurRotationMesh
        local ball = self.m_RO.transform
        local axisDir = axis == 0 and ball.right or (axis == 1 and ball.up or ball.forward) -- 轴方向的世界坐标
        self.m_AssistSector.transform:LookAt(self:VectorAdd(ball.position, self:ScaleVector(axisDir, CLuaHouseFurnitureRotationWnd.s_AxisArcRadius)))
    end
end

-- 绘制绕坐标轴的圆弧 
function CLuaHouseFurnitureRotationWnd:DrawAxisArc(axis) 
    local radius = CLuaHouseFurnitureRotationWnd.s_AxisArcRadius
    local seg = CLuaHouseFurnitureRotationWnd.s_AxisArcSegments
    local delta = 360 / seg

    local ball = self.m_RO.transform
    local axisDir = axis == 0 and ball.right or (axis == 1 and ball.up or ball.forward) -- 轴方向的世界坐标
    local vec = axis == 1 and ball.right or ball.up -- 初始向量

    for i = 0, seg do
        self.m_LineRenderers[axis]:SetPosition(i, self:VectorAdd(ball.position, self:ScaleVector(CommonDefs.op_Multiply_Quaternion_Vector3(Quaternion.AngleAxis(delta * i, axisDir), vec), radius)))
    end 
end

-- 计算圆和线段的交点
function CLuaHouseFurnitureRotationWnd:GetCircleIntersection(circleCenter, circleRadius, point1, point2)
    local t
    local dx = point2.x - point1.x
    local dy = point2.y - point1.y

    local a = dx * dx + dy * dy
    local b = 2 * (dx * (point1.x - circleCenter.x) + dy * (point1.y - circleCenter.y))
    local c = (point1.x - circleCenter.x) * (point1.x - circleCenter.x) + (point1.y - circleCenter.y) * (point1.y - circleCenter.y) - circleRadius * circleRadius

    local determinate = b * b - 4 * a * c
    if a <= 0.0000001 or determinate < -0.0000001 then -- No real solutions.
        return
    elseif determinate < 0.0000001 and determinate > -0.0000001 then -- One solution.
        t = -b / (2 * a)
        local ret = Vector3(point1.x + t * dx, point1.y + t * dy, 0)
        return ret, ret
    else -- Two solutions.
        local ret1, ret2
        t = (-b - math.sqrt(determinate)) / (2 * a)
        ret1 = Vector3(point1.x + t * dx, point1.y + t * dy, 0)
        t = (-b + math.sqrt(determinate)) / (2 * a)
        ret2 = Vector3(point1.x + t * dx, point1.y + t * dy, 0)
        return ret1, ret2
    end
end

function CLuaHouseFurnitureRotationWnd:OnAxisBtnDragStart(axis)
    self.m_DraggedAngle = 0

    if not CLuaHouseFurnitureRotationWnd.s_OnlyRotateAxisY then
        self.m_AxisRing[1]:SetActive(true)
        self.m_AxisRing[0]:SetActive(false)
    end
    
    --self.m_LineRenderers[axis]:SetColors(self.m_AxisColor[3], self.m_AxisColor[3])
    --self.m_LineRenderers[axis]:SetWidth(0.15, 0.15)
    self.m_AssistSector:SetActive(true)

    self.m_AxisBtnOriPos = self.m_AxisBtn[axis].position
    self.m_LastAxisBtnPos = UICamera.currentCamera:WorldToScreenPoint(self.m_AxisBtnOriPos)
    self.m_LastAxisBtnPos.z = 0
    self.m_BallCenter = UICamera.currentCamera:WorldToScreenPoint(self.m_ModelTexture.texture.worldCenter)
    self.m_BallCenter.z = 0
    self.m_AxisRingRadius = Vector3.Distance(self.m_LastAxisBtnPos, self.m_BallCenter)

    for i = 0, 2 do
        self.m_LineRenderers[i].gameObject:SetActive(false)
        self.m_AxisBtn[i].gameObject:SetActive(false)
    end
    self.m_LineRenderers[axis].gameObject:SetActive(true)
    self.m_AxisBtn[axis].gameObject:SetActive(true)
    self.m_AxisBtn[axis]:Find("Selected").gameObject:SetActive(true)

    self:DrawAxisArc(axis)
end

function CLuaHouseFurnitureRotationWnd:OnAxisBtnDrag(delta, axis)
    local mousePos = Input.mousePosition
    local mouseVec = self:VectorSub(mousePos, self.m_BallCenter) 

    -- 用同心圆算出坐标
    local lastPos = self.m_LastAxisBtnPos
    local curPos = self:VectorAdd(self.m_BallCenter, self:ScaleVector(mouseVec.normalized, self.m_AxisRingRadius))
    self.m_AxisBtn[axis].transform.position = UICamera.currentCamera:ScreenToWorldPoint(curPos)
    
    -- 由坐标得到旋转角度
    local angle = Vector3.Angle(self:VectorSub(lastPos, self.m_BallCenter), self:VectorSub(curPos, self.m_BallCenter))
    
    -- 顺时针正方向
    if  lastPos.y > self.m_BallCenter.y and lastPos.x > self.m_BallCenter.x and curPos.x <= self.m_BallCenter.x or 
        lastPos.y < self.m_BallCenter.y and lastPos.x < self.m_BallCenter.x and curPos.x >= self.m_BallCenter.x or
        lastPos.x < self.m_BallCenter.x and lastPos.y > curPos.y or lastPos.x > self.m_BallCenter.x and lastPos.y < curPos.y then 
        angle = -angle
    end

    self.m_DraggedAngle = self.m_DraggedAngle + angle

    local ball = self.m_RO.transform
    local fur = self.m_Furniture.RO.transform
    local dir1 = axis == 0 and ball.right or (axis == 1 and ball.up or ball.forward)
    local dir2 = axis == 0 and fur.right or (axis == 1 and fur.up or fur.forward)
    ball.rotation = CommonDefs.op_Multiply_Quaternion_Quaternion(Quaternion.AngleAxis(angle, dir1), ball.rotation)
    fur.rotation = CommonDefs.op_Multiply_Quaternion_Quaternion(Quaternion.AngleAxis(angle, dir2), fur.rotation)

    self:DrawAssistSector(axis, self.m_DraggedAngle, 720)

    self.m_LastAxisBtnPos = curPos
end

function CLuaHouseFurnitureRotationWnd:OnAxisBtnDragEnd(axis)
    if not CLuaHouseFurnitureRotationWnd.s_OnlyRotateAxisY then
        self.m_AxisRing[0]:SetActive(true)
        self.m_AxisRing[1]:SetActive(false)
    end

    --self.m_LineRenderers[axis]:SetColors(self.m_AxisColor[axis], self.m_AxisColor[axis])
    --self.m_LineRenderers[axis]:SetWidth(0.1, 0.1)

    self.m_AssistSector:SetActive(false)
    self.m_AxisBtn[axis].position = self.m_AxisBtnOriPos

    if not CLuaHouseFurnitureRotationWnd.s_OnlyRotateAxisY then
        for i = 0, 2 do
            self.m_LineRenderers[i].gameObject:SetActive(false)
            self.m_AxisBtn[i].gameObject:SetActive(true)
        end
    else
        self.m_LineRenderers[1].gameObject:SetActive(false)
    end
    self.m_AxisBtn[axis]:Find("Selected").gameObject:SetActive(false)

    self:OnDragEnd()
end

function CLuaHouseFurnitureRotationWnd:UpdateAxisDisplay()
    if not self.m_LineRenderers or CommonDefs.IsNull(self.m_Furniture) or CommonDefs.IsNull(self.m_Furniture.RO) then
        return 
    end

    self.m_AxisX.text = math.abs(math.floor(self.m_Furniture.RO.transform.localEulerAngles.x))
    self.m_AxisY.text = math.abs(math.floor(self.m_Furniture.RO.transform.localEulerAngles.y))
    self.m_AxisZ.text = math.abs(math.floor(self.m_Furniture.RO.transform.localEulerAngles.z))
end

--@endregion
