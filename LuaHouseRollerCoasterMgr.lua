local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"

LuaHouseRollerCoasterMgr = {}

--当前运行的过山车信息
LuaHouseRollerCoasterMgr.s_PlayInfo = {}
function LuaHouseRollerCoasterMgr.SetPlayInfo(huacheId,playInfo)
    -- print("SetPlayInfo",huacheId,playInfo)
    if not LuaHouseRollerCoasterMgr.s_PlayInfo then
        LuaHouseRollerCoasterMgr.s_PlayInfo = {}
    end
    LuaHouseRollerCoasterMgr.s_PlayInfo[huacheId] = playInfo
    local huacheFur = CClientFurnitureMgr.Inst:GetFurniture(huacheId)
    if huacheFur then
        local zhantaiFur = CClientFurnitureMgr.Inst:GetFurniture(huacheFur.ParentId)
        if zhantaiFur then
            zhantaiFur.NotUpdateChildrenPosition = playInfo and true or false
        end
    end
    
    if not next(LuaHouseRollerCoasterMgr.s_PlayInfo) then 
        CUIManager.CloseUI(CLuaUIResources.HouseRollerCoasterPlayWnd)
        return 
    end
end
function LuaHouseRollerCoasterMgr.GetPlayInfo(huacheId)
    if LuaHouseRollerCoasterMgr.s_PlayInfo then
        return LuaHouseRollerCoasterMgr.s_PlayInfo[huacheId]
    end
    return nil
end
function LuaHouseRollerCoasterMgr.ForeachPlayInfo(callback)
    for k,v in pairs(LuaHouseRollerCoasterMgr.s_PlayInfo) do
        callback(k,v)
    end
end
function LuaHouseRollerCoasterMgr.ClearPlayInfo()
    for k,v in pairs(LuaHouseRollerCoasterMgr.s_PlayInfo) do
        LuaHouseRollerCoasterMgr.SetPlayInfo(k,nil)
    end
end

--断线重连，清空
function LuaHouseRollerCoasterMgr.OnMainPlayerDestroyed()
    LuaHouseRollerCoasterMgr.ClearPlayInfo()
end


--装载
function LuaHouseRollerCoasterMgr.UseZhanTai(id)
    LuaHouseRollerCoasterMgr.SetCurParentFurnitureId(id)
    CUIManager.ShowUI(CLuaUIResources.HouseRollerCoasterUseZhanTaiWnd)
end
--上车
function LuaHouseRollerCoasterMgr.UseHuaChe(id,index)
    local child = CClientFurnitureMgr.Inst:GetFurniture(id)
    if not child then return end
    child:RequestUse(index)
end


LuaHouseRollerCoasterMgr.CurParentFurnitureId = 0
function LuaHouseRollerCoasterMgr.SetCurParentFurnitureId(id)
    LuaHouseRollerCoasterMgr.CurParentFurnitureId = id
end

function LuaHouseRollerCoasterMgr.GetCurParentFurnitureId()
    return LuaHouseRollerCoasterMgr.CurParentFurnitureId
end


function LuaHouseRollerCoasterMgr:SetCamera()
    local vec3 = Vector3(0, 20, 5)
    CameraFollow.Inst.targetRZY = vec3
    CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
    CameraFollow.Inst.m_CameraBindEnable = true
end
function LuaHouseRollerCoasterMgr.ResetCamera()
    local vec3 = Vector3(237.5, 12, 9.5)
    CameraFollow.Inst.targetRZY = vec3
    CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
    CameraFollow.Inst.m_CameraBindEnable = false
end


local _modules={
    [9584] = true,
    [9585] = true,
    [9586] = true,
    [9587] = true,
    -- [9588] = true,--花车
    -- [9589] = true,--站台
}
--轨道
function LuaHouseRollerCoasterMgr.IsTrackModule(templateId)
    if _modules[templateId] then return true end
    return false
end
function LuaHouseRollerCoasterMgr.IsStationModule(templateId)
    return templateId == 9589
end

LuaEnumRollerCosterNodeType={
    Start = 1,
    Line = 2,
    Slope = 3,
    Arc = 4,
    SlopeArc = 5,
    End = 6,
    Station = 7,--站台，路径上的站台
}
--[[
    1表示端点，默认是竖直方向的轨道，这个点必须摆放同为1的点
    2表示端点，默认是水平方向的轨道，
    旋转之后，1变成2，2变成1
    可以解释为奇数是竖直方向的，偶数是水平方向
    -1表示邻居节点。因为除了保证2个点能连接，还要保证不能重叠
]]
local _start_config={
    joints = {0,  0,  0,  0,  0,  -1,  1},--从左下角开始排列
    xCount = 1,
    zCount = 7,
}
local _end_config={
    joints = {1,  -1,  0,  0,  0,  0,  0},--另外半截
    xCount = 1,
    zCount = 7,
}
local _station_config={
    joints = {1,  -1,  0,   0,  0,  -1,  1},
    xCount = 1,
    zCount = 7,
}
local _line_config={
    joints = {1,  -1,  0,   0,  0,  -1,  1},
    xCount = 1,
    zCount = 7,
}
local _slope_config={
--[[
     1,
    -1,
     0,
     0,
     0,
    -1,
     1,
]]
    joints = {1,  -1,  0,   0,  0,  -1,  1},
    xCount = 1,
    zCount = 7,
    yOffset = {4,0,0,0,0,0,0,0},
}
local _arc_config = {
--[[
    2,-1, 0, 0, 0, 0, 0
    0, 0, 0, 0, 0, 0, 0
    0, 0, 0, 0, 0, 0, 0
    0, 0, 0, 0, 0, 0, 0
    0, 0, 0, 0, 0, 0, 0
    0, 0, 0, 0, 0, 0,-1
    3, 0, 0, 0, 0, 0, 1
]]
    joints = {3,0,0,0,0,0,1,  0,0,0,0,0,0,-1,  0,0,0,0,0,0,0,  0,0,0,0,0,0,0,  0,0,0,0,0,0,0,  0,0,0,0,0,0,0,  2,-1,0,0,0,0,0},
    xCount = 7,
    zCount = 7,
    radius = 5,
}
local _slope_arc_config = {
--[[
    2,-1, 0, 0, 0, 0, 0
    0, 0, 0, 0, 0, 0, 0
    0, 0, 0, 0, 0, 0, 0
    0, 0, 0, 0, 0, 0, 0
    0, 0, 0, 0, 0, 0, 0
    0, 0, 0, 0, 0, 0,-1
    3, 0, 0, 0, 0, 0, 1
]]
    joints = {3,0,0,0,0,0,1,  0,0,0,0,0,0,-1,  0,0,0,0,0,0,0,  0,0,0,0,0,0,0,  0,0,0,0,0,0,0,  0,0,0,0,0,0,0,  2,-1,0,0,0,0,0},
    xCount = 7,
    zCount = 7,
    radius = 5,
    height = 2,--最高点和最低点的高度差
--[[
     0,0,0,0,0,0,0
     0,0,0,0,0,0,0
     0,0,0,0,0,0,0
     0,0,0,0,0,0,0
     0,0,0,0,0,0,0
     0,0,0,0,0,0,0
     0,0,0,0,0,0,2
]]
    yOffset = {0,0,0,0,0,0,2,  0,0,0,0,0,0,0,  0,0,0,0,0,0,0,  0,0,0,0,0,0,0,  0,0,0,0,0,0,0,  0,0,0,0,0,0,0,  0,0,0,0,0,0,0},
}

_config = {
    _start_config,_line_config,_slope_config,_arc_config,_slope_arc_config,_end_config,_station_config
}

--获取轨道的节点，站台只有一个节点，其他轨道都是2个节点
function LuaHouseRollerCoasterMgr.GetJoints(node)
    local ret = {}
    local neighbors = {}
    local config = _config[node.type]
    local rot = node.rot
    local xCount,zCount,xCenter,zCenter = config.xCount,config.zCount,math.floor(config.xCount/2),math.floor(config.zCount/2)

    local joints = config.joints

	for i = 0, zCount - 1 do
		for j = 0, xCount - 1 do
            local offsetX,offsetZ = 0,0
            local neighborOffsetX,neighborOffsetZ = 0,0
            if rot==90 then
                offsetX,offsetZ = i-zCenter,xCount - 1 - j - xCenter
            elseif rot==180 then
                offsetX,offsetZ = xCount - 1 - j - xCenter,zCount - 1 - i - zCenter
            elseif rot==270 then
                offsetX,offsetZ = zCount - 1 - i - zCenter,j - xCenter
            else
                offsetX,offsetZ = j - xCenter,i - zCenter
            end
            local v = joints[i*xCount+j+1]
            if v==1 or v==2 then
                if rot%180==90 then
                    v = 3-v --1变成2，2变成1
                end
                local joint = {
                    value = v,
                    offsetX = offsetX,
                    offsetZ = offsetZ,
                    offsetY = 0,
                }
                if config.yOffset then
                    joint.offsetY = config.yOffset[i*xCount+j+1]
                end

                table.insert(ret,joint)
            elseif v==-1 then
                table.insert(neighbors,{
                    offsetX = offsetX,
                    offsetZ = offsetZ,
                })
            elseif v==3 then--圆心
                node.centerPos = {
                    x = node.x+offsetX+0.5,
                    z = node.z+offsetZ+0.5,
                    y = node.y,
                }
            end
        end
    end
    for i,v in ipairs(ret) do
        for i2,v2 in ipairs(neighbors) do

            if v.offsetX==v2.offsetX then--y必然是相邻的
                if v.offsetZ==v2.offsetZ+1 or v.offsetZ==v2.offsetZ-1 then
                    --找到了
                    if v.neighbor then
                        print("impossible logic 1")--只会有一个邻居节点
                    end
                    v.neighbor = {
                        offsetX = v2.offsetX,
                        offsetZ = v2.offsetZ,
                    }
                end
            elseif v.offsetZ==v2.offsetZ then
                if v.offsetX==v2.offsetX+1 or v.offsetX==v2.offsetX-1 then
                    --找到了
                    if v.neighbor then
                        print("impossible logic 2")--只会有一个邻居节点
                    end
                    v.neighbor = {
                        offsetX = v2.offsetX,
                        offsetZ = v2.offsetZ,
                    }
                end
            end
        end
    end
    return ret
end
--[[
    RollerCoasterPathNode = {
        type,
        rot,
        x,
        z,
        y,
        startPos = {x,y,z},
        endPos = {x,y,z}
        joints = nil,
        moveInfo = {
            percent,
            length,
            currentPos,--当前移动位置
        }
        isLast = false
    }
]]

function LuaHouseRollerCoasterMgr.CreateStartPathNode(fur)
    if fur.TemplateId==9589 then
        local ret = {
            id = fur.ID,
            rot = (fur.ServerRotateY+360)%360,
            x = math.floor(fur.ServerPos.x),
            z = math.floor(fur.ServerPos.z),
            y = math.floor(fur.ServerPos.y+0.1),--发现有些装饰物的位置是-0.01
        }
        ret.type = LuaEnumRollerCosterNodeType.Start
        ret.startPos = {
            x = ret.x+0.5,
            z = ret.z+0.5,
            y = ret.y,
        }
        return ret
    end
    return nil
end
function LuaHouseRollerCoasterMgr.CreateEndPathNode(fur)
    if fur.TemplateId==9589 then
        local ret = {
            id = fur.ID,
            rot = (fur.ServerRotateY+360)%360,
            x = math.floor(fur.ServerPos.x),
            z = math.floor(fur.ServerPos.z),
            y = math.floor(fur.ServerPos.y+0.1),--发现有些装饰物的位置是-0.01
        }
        ret.type = LuaEnumRollerCosterNodeType.End
        ret.endPos = {
            x = ret.x+0.5,
            z = ret.z+0.5,
            y = ret.y,
        }
        return ret
    end
    return nil
end
function LuaHouseRollerCoasterMgr.CreatePathNode(fur)
    local ret = {
        id = fur.ID,
        rot = (fur.ServerRotateY+360)%360,
        x = math.floor(fur.ServerPos.x),
        z = math.floor(fur.ServerPos.z),
        y = math.floor(fur.ServerPos.y+0.1),--发现有些装饰物的位置是-0.01
    }

    if fur.TemplateId==9589 then
        ret.type = LuaEnumRollerCosterNodeType.Station
    elseif fur.TemplateId==9584 then
        ret.type = LuaEnumRollerCosterNodeType.Line
    elseif fur.TemplateId==9585 then
        ret.type = LuaEnumRollerCosterNodeType.Slope
    elseif fur.TemplateId==9586 then
        ret.type = LuaEnumRollerCosterNodeType.Arc
    elseif fur.TemplateId==9587 then
        ret.type = LuaEnumRollerCosterNodeType.SlopeArc
    end
    return ret
end

function LuaHouseRollerCoasterMgr:TryConnected(preNode,currentNode)
    -- if not preNode.joints then
    --     preNode.joints = LuaHouseRollerCoasterMgr.GetJoints(preNode)--获取节点
    -- end
    -- if not currentNode.joints then
    --     currentNode.joints = LuaHouseRollerCoasterMgr.GetJoints(currentNode)--获取节点
    -- end
    --找前一个节点能用的joint，看能不能和新的节点连接起来
    local find = false
    local preJoint = preNode.joints
    for i=1,#preNode.joints do
        if not preNode.joints[i].used then
            local joint = preNode.joints[i]
            
            for j=1,#currentNode.joints do
                local curJoint = currentNode.joints[j]
                if joint.value==curJoint.value then
                    local px,pz,py = preNode.x+joint.offsetX,preNode.z+joint.offsetZ,preNode.y+joint.offsetY
                    local cx,cz,cy = currentNode.x+curJoint.offsetX,currentNode.z+curJoint.offsetZ,currentNode.y+curJoint.offsetY
                    if px == cx and pz == cz and py == cy then
                        --节点重合，再判断有没有重叠
                        if preNode.x+joint.neighbor.offsetX == currentNode.x+curJoint.neighbor.offsetX 
                            and preNode.z+joint.neighbor.offsetZ == currentNode.z+curJoint.neighbor.offsetZ then
                            --重合了，不允许连接
                        else
                            joint.used = true
                            if not preNode.endPos then
                                preNode.endPos = {
                                    x = preNode.x+joint.offsetX+0.5,
                                    z = preNode.z+joint.offsetZ+0.5,
                                    y = preNode.y+joint.offsetY,
                                }
                            end

                            curJoint.used = true
                            currentNode.startPos = {
                                x = currentNode.x+curJoint.offsetX+0.5,
                                z = currentNode.z+curJoint.offsetZ+0.5,
                                y = currentNode.y+curJoint.offsetY,
                            }
                            --endPos自然就是另外一个点了
                            if #currentNode.joints==2 then
                                local otherJoint = currentNode.joints[3-j]--另一个点
                                currentNode.endPos = {
                                    x = currentNode.x+otherJoint.offsetX+0.5,
                                    z = currentNode.z+otherJoint.offsetZ+0.5,
                                    y = currentNode.y+otherJoint.offsetY,
                                }
                                -- print("otherpos",otherJoint.offsetY,currentNode.otherPos.y)
                            end

                            find = true
                            break
                        end
                    end
                end
            end
        end
    end

    return find
end

function LuaHouseRollerCoasterMgr.GenerateRollerCoasterPath(zhantaiFurId)
    local isClosePath = false
    local FurnitureList = CClientFurnitureMgr.Inst.FurnitureList

    local startFur = CClientFurnitureMgr.Inst:GetFurniture(zhantaiFurId)
    if not startFur then return end
    local startNode = LuaHouseRollerCoasterMgr.CreateStartPathNode(startFur)
    startNode.joints = LuaHouseRollerCoasterMgr.GetJoints(startNode)

    local t = {}
    CommonDefs.DictIterate(CClientFurnitureMgr.Inst.FurnitureList, DelegateFactory.Action_object_object(function (k,v) 
        --从花车开始
        --可能有好几个花车
        --重叠摆放怎么办，选哪个呢？
        local subType = v:GetSubType()
        if LuaHouseRollerCoasterMgr.IsTrackModule(v.TemplateId) and k~=zhantaiFurId then
            local node = LuaHouseRollerCoasterMgr.CreatePathNode(v)
            node.joints = LuaHouseRollerCoasterMgr.GetJoints(node)--获取节点
            table.insert( t,node )
        elseif LuaHouseRollerCoasterMgr.IsStationModule(v.TemplateId) and k~=zhantaiFurId then
            local node = LuaHouseRollerCoasterMgr.CreatePathNode(v)
            node.joints = LuaHouseRollerCoasterMgr.GetJoints(node)--获取节点
            table.insert( t,node )
        end
    end))

    table.sort( t,function(a,b)
        return a.id<b.id
    end )

    local lastNode = startNode
    local path = {startNode}
    while true do
        local idx = 0
        for i=1,#t do
            if LuaHouseRollerCoasterMgr:TryConnected(lastNode,t[i]) then
                idx = i
                break
            end
        end
        if idx>0 then
            lastNode = t[idx]
            table.insert(path,lastNode)
            table.remove( t,idx )
        else
            --没找到
            break
        end
    end

    --只有一节站台
    if #path==1 then
        local joint = startNode.joints[1]
        startNode.endPos = {
            x = startNode.x+joint.offsetX+0.5,
            z = startNode.z+joint.offsetZ+0.5,
            y = startNode.y+joint.offsetY,
        }
    end
    local endNode = LuaHouseRollerCoasterMgr.CreateEndPathNode(startFur)
    endNode.joints = LuaHouseRollerCoasterMgr.GetJoints(endNode)
    if LuaHouseRollerCoasterMgr:TryConnected(lastNode,endNode) then
        table.insert(path,endNode)
        isClosePath = true
    end

    --初始化
    for i,v in ipairs(path) do
        -- print(v.id)
        v.moveInfo = {
            percent = 0,
            length = 0,
        }
        if v.type == LuaEnumRollerCosterNodeType.Start then
            v.moveInfo.length = 3
        elseif v.type == LuaEnumRollerCosterNodeType.End then
            v.moveInfo.length = 3
        elseif v.type == LuaEnumRollerCosterNodeType.Station then
            v.moveInfo.length = 6
        elseif v.type == LuaEnumRollerCosterNodeType.Line then
            v.moveInfo.length = 6
        elseif v.type == LuaEnumRollerCosterNodeType.Slope then
            v.moveInfo.length = 6*1.41421356--斜边长度
        elseif v.type == LuaEnumRollerCosterNodeType.Arc then
            local radius = _arc_config.radius
            v.moveInfo.length = PI*radius*2/4 -- 半径是3
        elseif v.type == LuaEnumRollerCosterNodeType.SlopeArc then
            --算向量夹角
            local pos0 = v.startPos
            local pos1 = v.endPos
            local center = v.centerPos
            local radius = _arc_config.radius
            --长的那一段半径
            local radius2 = 5.3851648--5*5+2*2开根号
            --椭圆周长计算公式
            v.moveInfo.length = (2*PI*radius+4*(radius2-radius))/4
        end
    end

    return path,isClosePath
end
function LuaHouseRollerCoasterMgr.Lerp(node,percent)
    -- print("Lerp",node.id,percent)
    if node.type==LuaEnumRollerCosterNodeType.Line 
        or node.type == LuaEnumRollerCosterNodeType.Slope 
        or node.type == LuaEnumRollerCosterNodeType.Station then
        local pos0 = node.startPos
        local pos1 = node.endPos
        local pos = {}
        pos.x = pos0.x+(pos1.x-pos0.x)*percent
        pos.y = pos0.y+(pos1.y-pos0.y)*percent
        pos.z = pos0.z+(pos1.z-pos0.z)*percent
        return pos
    elseif node.type == LuaEnumRollerCosterNodeType.Arc then--or node.type == LuaEnumRollerCosterNodeType.SlopeArc then
        local pos = {}
        local center = node.centerPos
        local pos0 = node.startPos
        local pos1 = node.endPos
        local radius = _arc_config.radius
        local vector = Vector3.Slerp(
            Vector3(pos0.x-center.x,pos0.y-center.y,pos0.z-center.z),
            Vector3(pos1.x-center.x,pos1.y-center.y,pos1.z-center.z),percent)--差值
        return {
            x = center.x+vector.x,
            y = center.y+vector.y,
            z = center.z+vector.z,
        }
    elseif node.type == LuaEnumRollerCosterNodeType.SlopeArc then
        local pos = {}
        local center = node.centerPos
        local pos0 = node.startPos
        local pos1 = node.endPos
        -- print("endPos",pos0.y)
        local radius = _slope_arc_config.radius

        local vector = Vector3.Slerp(
            Vector3(pos0.x-center.x,0,pos0.z-center.z),
            Vector3(pos1.x-center.x,0,pos1.z-center.z),percent)--差值
        local offsetY = 0
        if pos0.y>pos1.y then
            --从高处往低处移动
            offsetY = (1-percent)*_slope_arc_config.height
        else
            --从低处往高处移动
            offsetY = percent*_slope_arc_config.height
        end
        return {
            x = center.x+vector.x,
            y = center.y+offsetY,
            z = center.z+vector.z,
        }
    elseif node.type==LuaEnumRollerCosterNodeType.Start then
        local pos0 = node.startPos
        local pos1 = node.endPos
        local pos = {}
        pos.x = pos0.x+(pos1.x-pos0.x)*percent
        pos.y = pos0.y+(pos1.y-pos0.y)*percent
        pos.z = pos0.z+(pos1.z-pos0.z)*percent
        return pos
    elseif node.type==LuaEnumRollerCosterNodeType.End then
        local pos0 = node.startPos
        local pos1 = node.endPos
        local pos = {}
        pos.x = pos0.x+(pos1.x-pos0.x)*percent
        pos.y = pos0.y+(pos1.y-pos0.y)*percent
        pos.z = pos0.z+(pos1.z-pos0.z)*percent
        return pos
    end
    print("impossible")

end

function LuaHouseRollerCoasterMgr.MoveOneStep(v,distance)
    --2个点差值 
    local len = v.moveInfo.length*v.moveInfo.percent+distance
    if len>v.moveInfo.length then
        v.moveInfo.percent = 1
        v.moveInfo.currentPos = v.endPos--不需要了
        -- print("currentPos",v.moveInfo.currentPos)
        return len-v.moveInfo.length
    else
        v.moveInfo.percent = len/v.moveInfo.length
        v.moveInfo.currentPos = LuaHouseRollerCoasterMgr.Lerp(v,v.moveInfo.percent)
        return 0
    end
end

function LuaHouseRollerCoasterMgr.BeginPlay(playerId, cabinFId, cabinTId, stationFId)
    local huacheFur = CClientFurnitureMgr.Inst:GetFurniture(cabinFId)
    local zhantaiFur = CClientFurnitureMgr.Inst:GetFurniture(stationFId)
    if not zhantaiFur then return end
    if not huacheFur then return end
    

    local path,isClosePath = LuaHouseRollerCoasterMgr.GenerateRollerCoasterPath(stationFId)
    local setting = Zhuangshiwu_Setting.GetData()
    local initSpeed = setting.RollerCoasterv0

    local playInfo = {
        stationId = stationFId,
        cabinId = cabinFId,
        path = path,
        pathIndex = 1,
        speed = initSpeed,
        driverId = playerId,
        isClose = isClosePath,--是否闭合
    }
    LuaHouseRollerCoasterMgr.SetPlayInfo(cabinFId,playInfo)


    if not CUIManager.IsLoaded(CLuaUIResources.HouseRollerCoasterPlayWnd) 
        and not CUIManager.IsLoading(CLuaUIResources.HouseRollerCoasterPlayWnd) then
        CUIManager.ShowUI(CLuaUIResources.HouseRollerCoasterPlayWnd)
    end

    g_ScriptEvent:BroadcastInLua("HouseRollerCoasterBeginPlay",cabinFId)
end
function LuaHouseRollerCoasterMgr.OnStop(playerId, driverId, fid, fTid, bAuto)
    --手动停止
    if not bAuto then
        local playInfo = LuaHouseRollerCoasterMgr.GetPlayInfo(fid)
        if playInfo then
            playInfo.pause = true
        end
    else
        --自动停下表示到达终点了
        LuaHouseRollerCoasterMgr.SetPlayInfo(fid,nil)
    end
    g_ScriptEvent:BroadcastInLua("HouseRollerCoasterStop",fid,bAuto)
end

function LuaHouseRollerCoasterMgr.OnResume(playerId, fid, fTid)
    local playInfo = LuaHouseRollerCoasterMgr.GetPlayInfo(fid)
    if playInfo then
        playInfo.pause = false
    end
    g_ScriptEvent:BroadcastInLua("HouseRollerCoasterResume",playerId, fid, fTid)
end

function LuaHouseRollerCoasterMgr.RunRollerCoasterEnd(playerId)
    LuaHouseRollerCoasterMgr.ForeachPlayInfo(function(huacheId,playInfo)
        if playInfo.driverId == playerId then
            --司机下车
            LuaHouseRollerCoasterMgr.ResetCabinPos(huacheId)
            LuaHouseRollerCoasterMgr.SetPlayInfo(huacheId,nil)
        end
    end)
end
function LuaHouseRollerCoasterMgr.ResetCabinPos(huacheId)
    local huacheFur = CClientFurnitureMgr.Inst:GetFurniture(huacheId)
    if huacheFur then
        local zhantaiFur = CClientFurnitureMgr.Inst:GetFurniture(huacheFur.ParentId)
        zhantaiFur:UpdateChildrenPosition()
    end
end

function LuaHouseRollerCoasterMgr.SyncRollerCoasterProgress(driverId, fid, pathIndex, segmentPercent, speed)
    -- print("SyncRollerCoasterProgress",pathIndex, segmentPercent)
    local playInfo = LuaHouseRollerCoasterMgr.GetPlayInfo(fid)
    if not playInfo then
        local huacheFur = CClientFurnitureMgr.Inst:GetFurniture(fid)
        if not huacheFur then return end
        local zhantaiFur = CClientFurnitureMgr.Inst:GetFurniture(huacheFur.ParentId)
        if not zhantaiFur then return end
        local stationId = zhantaiFur.ID
        local path,isClosePath = LuaHouseRollerCoasterMgr.GenerateRollerCoasterPath(stationId)
        playInfo = {
            stationId = stationId,
            cabinId = fid,
            path = path,
            driverId = driverId,
            pause = false,
            isClose = isClosePath,--是否闭合
        }
        LuaHouseRollerCoasterMgr.SetPlayInfo(fid,playInfo)
    end
    playInfo.pathIndex = pathIndex
    playInfo.speed = speed

    local percent = playInfo.path[playInfo.pathIndex].moveInfo.percent
    --司机不需要
    local playerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    if playerId ~= driverId then
        local newPercent = segmentPercent/100
        if math.abs(percent-newPercent)>0.2 then--如果偏差大于20%了 强制同步一下
            playInfo.path[playInfo.pathIndex].moveInfo.percent = newPercent
        end
    end

    if not CUIManager.IsLoaded(CLuaUIResources.HouseRollerCoasterPlayWnd) 
        and not CUIManager.IsLoading(CLuaUIResources.HouseRollerCoasterPlayWnd) then
        CUIManager.ShowUI(CLuaUIResources.HouseRollerCoasterPlayWnd)
    end
end
function LuaHouseRollerCoasterMgr.BatchSyncRollerCoasterProgress(infoUd)
    LuaHouseRollerCoasterMgr.ClearPlayInfo()

    local needPlay = false
    local info = g_MessagePack.unpack(infoUd)
    for i=1,#info,8 do
        local driverId = info[i]
        local cabinId = info[i+1]
        local cabinTemplateId = info[i+2]
        local stationId = info[i+3]
        local isStop = info[i+4]>0 and true or false
        local pathIndex = info[i+5]
        local segmentPercent = info[i+6]
        local speed = info[i+7]

        if pathIndex==0 then pathIndex=1 end

        local path,isClosePath = LuaHouseRollerCoasterMgr.GenerateRollerCoasterPath(stationId)
        local playInfo = {
            stationId = stationId,
            cabinId = cabinId,
            path = path,
            pathIndex = pathIndex,
            speed = speed,
            driverId = driverId,
            pause = isStop,
            isClose = isClosePath,--是否闭合
        }
        LuaHouseRollerCoasterMgr.SetPlayInfo(cabinId,playInfo)
        path[pathIndex].moveInfo.percent = segmentPercent/100
        needPlay = true
    end

    if needPlay then
        if not CUIManager.IsLoaded(CLuaUIResources.HouseRollerCoasterPlayWnd) 
            and not CUIManager.IsLoading(CLuaUIResources.HouseRollerCoasterPlayWnd) then
            CUIManager.ShowUI(CLuaUIResources.HouseRollerCoasterPlayWnd)
        end
    end
end