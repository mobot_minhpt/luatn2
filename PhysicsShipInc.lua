
CPhysicsShip = class()
RegistClassMember(CPhysicsShip, "m_POId")
RegistClassMember(CPhysicsShip, "m_TemplateId")

RegistClassMember(CPhysicsShip, "m_TargetSpeed")
RegistClassMember(CPhysicsShip, "m_CmdMulSpeed")
RegistClassMember(CPhysicsShip, "m_MulSpeed")
RegistClassMember(CPhysicsShip, "m_SpeedCmdEndTime")
RegistClassMember(CPhysicsShip, "m_SpeedCmd2CDEnd")

RegistClassMember(CPhysicsShip, "m_RotationTarget")
RegistClassMember(CPhysicsShip, "m_IsFreeze")
RegistClassMember(CPhysicsShip, "m_FreezeTemplateId")
RegistClassMember(CPhysicsShip, "m_FreezeCountDown")
RegistClassMember(CPhysicsShip, "m_TimeStepInSecond")
RegistClassMember(CPhysicsShip, "m_EffectorInfo")
RegistClassMember(CPhysicsShip, "m_LinearDamping")
RegistClassMember(CPhysicsShip, "m_AngularDamping")
RegistClassMember(CPhysicsShip, "m_Mass")
RegistClassMember(CPhysicsShip, "m_I")
RegistClassMember(CPhysicsShip, "m_MaxW")
RegistClassMember(CPhysicsShip, "m_OriSpeed")

-- server input buffer
RegistClassMember(CPhysicsShip, "m_CmdQ")
RegistClassMember(CPhysicsShip, "m_LastMoveCmd")

RegistClassMember(CPhysicsShip, "m_ClientPhysicsObjectHandler")
RegistClassMember(CPhysicsShip, "m_InputType")

EnumPhysicsCmdType = {
	eNone = 0,
	eMoveLeft = 1,
	eMoveRight = 2,
	eSpeed = 3,
}

EnumPhysicsCmdTypeReverse = {
	[0] = EnumPhysicsCmdType.eNone,
	[1] = EnumPhysicsCmdType.eMoveLeft,
	[2] = EnumPhysicsCmdType.eMoveRight,
	[3] = EnumPhysicsCmdType.eSpeed,
}

for _, i in pairs(EnumPhysicsCmdType) do
	local b = EnumPhysicsCmdTypeReverse
	local a = b[i]
	assert(a)
end

EnumPhysicsController = {
	eNone = 0,
	ePhysicsShip = 1,
	ePhysicsAIShip = 2,
}

EnablePhysicsShipOutputLog = false

require("common/haizhan/PhysicsShip")


