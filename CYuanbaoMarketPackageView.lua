-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemCell = import "L10.UI.CItemCell"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CYuanbaoMarketPackageView = import "L10.UI.CYuanbaoMarketPackageView"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local NGUITools = import "NGUITools"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CYuanbaoMarketPackageView.m_Init_CS2LuaHook = function (this, packageSelectId) 

    if CClientMainPlayer.Inst == nil then
        return
    end
    local n = CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag)
    Extensions.RemoveAllChildren(this.grid.transform)
    CommonDefs.ListClear(this.itemCells)
    CommonDefs.ListClear(this.itemList)
    this.curSelect = ""

    local firstItemId = ""
    local firstItemPos = 0
    local firstItemGo = nil

    local itemProp = CClientMainPlayer.Inst.ItemProp

    do
        local i = 0
        while i < n do
            local itemId = itemProp:GetItemAt(EnumItemPlace.Bag, i + 1)
            local item = CItemMgr.Inst:GetById(itemId)
            if item ~= nil and not item.IsBinded and item.IsItem and item.Item.isSell2Market then
                local instance = NGUITools.AddChild(this.grid.gameObject, this.itemCellTemplate)
                instance:SetActive(true)
                local cell = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CItemCell))
                CommonDefs.ListAdd(this.itemCells, typeof(CItemCell), cell)
                cell:SetSelected(false)
                CommonDefs.ListAdd(this.itemList, typeof(GameObject), instance)
                if System.String.IsNullOrEmpty(firstItemId) then
                    firstItemId = itemId
                    firstItemPos = i + 1
                    firstItemGo = instance
                end
                UIEventListener.Get(instance).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(instance).onClick, MakeDelegateFromCSFunction(this.OnItemSelect, VoidDelegate, this), true)
                cell:Init(itemId, i + 1, i + 1 > CClientMainPlayer.Inst.ItemProp.BagPlaceSize and i + 1 <= CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag))
                if not System.String.IsNullOrEmpty(packageSelectId) and itemId == packageSelectId then
                    this.curSelect = itemId
                    this:OnItemClick(itemId, i + 1)
                end
            end
            i = i + 1
        end
    end
    this.grid:Reposition()
    this.scrollView:ResetPosition()

    if this.itemList.Count == 0 then
        this.tips:SetActive(true)
    else
        this.tips:SetActive(false)
        if not System.String.IsNullOrEmpty(this.curSelect) then
            local hasFound = false
            do
                local i = 0
                while i < this.itemCells.Count do
                    if this.itemCells[i].ItemId == this.curSelect then
                        this:OnItemSelect(this.itemList[i])
                        hasFound = true
                    end
                    i = i + 1
                end
            end
            if not hasFound then
                this:OnItemSelect(firstItemGo)
            end
        else
            this:OnItemSelect(firstItemGo)
        end
    end
end
CYuanbaoMarketPackageView.m_OnItemSelect_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.itemList.Count do
            this.itemCells[i]:SetSelected(go == this.itemList[i])
            if go == this.itemList[i] then
                this.curSelect = this.itemCells[i].ItemId
                local pos = this.itemCells[i].PackagePos
                this:OnItemClick(this.curSelect, pos)
            end
            i = i + 1
        end
    end
end
