local CScene = import "L10.Game.CScene"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UISprite = import "UISprite"
local UIGrid = import "UIGrid"
local UISlider = import "UISlider"

LuaGuildTerritorialWarsTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildTerritorialWarsTopRightWnd, "ExpandButton", "ExpandButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsTopRightWnd, "CombatView", "CombatView", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsTopRightWnd, "NonCombatView", "NonCombatView", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsTopRightWnd, "Bg", "Bg", UISprite)
RegistChildComponent(LuaGuildTerritorialWarsTopRightWnd, "CombatGrid", "CombatGrid", UIGrid)
RegistChildComponent(LuaGuildTerritorialWarsTopRightWnd, "CombatViewOccupySlider", "CombatViewOccupySlider", UISlider)
RegistChildComponent(LuaGuildTerritorialWarsTopRightWnd, "NonCombatViewOccupySlider", "NonCombatViewOccupySlider", UISlider)
RegistChildComponent(LuaGuildTerritorialWarsTopRightWnd, "SliderTemplate", "SliderTemplate", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsTopRightWnd, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsTopRightWnd, "MapBtn", "MapBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaGuildTerritorialWarsTopRightWnd, "m_BgInitialHeight")

function LuaGuildTerritorialWarsTopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)

	UIEventListener.Get(self.MapBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMapBtnClick()
	end)

	self.SliderTemplate.gameObject:SetActive(false)
	self.m_BgInitialHeight = self.Bg.height - self.CombatGrid.cellHeight * 3
    --@endregion EventBind end
end

function LuaGuildTerritorialWarsTopRightWnd:Init()
	self.CombatGrid.gameObject:SetActive(false)
	self.CombatView.gameObject:SetActive(false)
	self.NonCombatView.gameObject:SetActive(true)
	self.Bg.height = self.m_BgInitialHeight
	self.MapBtn.gameObject:SetActive(not LuaGuildTerritorialWarsMgr:IsInGuide())
	self:OnSyncTerritoryWarPlayProgress()
end

function LuaGuildTerritorialWarsTopRightWnd:OnEnable()
	g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
	g_ScriptEvent:AddListener("OnSyncTerritoryWarPlayProgress", self, "OnSyncTerritoryWarPlayProgress")
	g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
end

function LuaGuildTerritorialWarsTopRightWnd:OnDisable()
	g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
	g_ScriptEvent:RemoveListener("OnSyncTerritoryWarPlayProgress", self, "OnSyncTerritoryWarPlayProgress")
	g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
end

function LuaGuildTerritorialWarsTopRightWnd:OnHideTopAndRightTipWnd()
	self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

function LuaGuildTerritorialWarsTopRightWnd:OnSyncTerritoryWarPlayProgress()
	local progressList = LuaGuildTerritorialWarsMgr.m_ProgressList
	local progress = progressList[0] and progressList[0] or 0
	self.CombatViewOccupySlider.value = progress
	self:InitSlider(self.CombatViewOccupySlider, progress)
	self:InitSlider(self.NonCombatViewOccupySlider, progress)
	Extensions.RemoveAllChildren(self.CombatGrid.transform)
	local isInCombat = false
	if progress ~= 100 then
		for i = 1,3 do
			progress = progressList[i]
			if progress then
				isInCombat = true
				local isMyIdx = i == LuaGuildTerritorialWarsMgr.m_SceneIdx
				local obj = NGUITools.AddChild(self.CombatGrid.gameObject,self.SliderTemplate.gameObject)
				obj.gameObject:SetActive(true)
				local slider = obj:GetComponent(typeof(UISlider))
				self:InitSlider(slider, progress)
				local label = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
				label.text = SafeStringFormat3(LocalString.GetString("分线%d"), i)
				label.color = isMyIdx and Color.green or Color.white
				local curSprite = obj.transform:Find("CurSprite")
				curSprite.gameObject:SetActive(isMyIdx)
			end
		end
	end
	self.CombatGrid.gameObject:SetActive(isInCombat)
	self.CombatView.gameObject:SetActive(isInCombat)
	self.NonCombatView.gameObject:SetActive(not isInCombat)
	self.CombatGrid:Reposition()
	self.Bg.height = self.m_BgInitialHeight + self.CombatGrid.cellHeight * self.CombatGrid.transform.childCount
end

function LuaGuildTerritorialWarsTopRightWnd:OnSceneRemainTimeUpdate()
	local totalSeconds = 0
    if CScene.MainScene then
		totalSeconds = CScene.MainScene.ShowTime
    end
	self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("地块争夺中%02d:%02d"), math.floor(totalSeconds / 60), totalSeconds % 60)
end

function LuaGuildTerritorialWarsTopRightWnd:InitSlider(slider, val)
	local occupyProgressLabel = slider.transform:Find("OccupyProgressLabel"):GetComponent(typeof(UILabel))
	occupyProgressLabel.text = math.floor(val) .."%"
	slider.value = val / 100
end
--@region UIEvent

function LuaGuildTerritorialWarsTopRightWnd:OnExpandButtonClick()
	self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaGuildTerritorialWarsTopRightWnd:OnMapBtnClick()
	Gac2Gas.RequestTerritoryWarMapOverview()
end

--@endregion UIEvent
function LuaGuildTerritorialWarsTopRightWnd:GetGuideGo(methodName) 
    if methodName == "GetGuildTerritorialWarsPlayViewMapBtn" then
        return self.MapBtn
    end
    return nil
end