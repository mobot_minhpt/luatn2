-- Auto Generated!!
local Application = import "UnityEngine.Application"
local CGuildMemberExportWnd = import "L10.UI.CGuildMemberExportWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIDataSheetRow = import "L10.UI.CUIDataSheetRow"
local GuildMemberExportMgr = import "L10.Game.GuildMemberExportMgr"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildMemberExportWnd.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.exportBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.exportBtn).onClick, MakeDelegateFromCSFunction(this.OnExportButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.generateChartBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.generateChartBtn).onClick, MakeDelegateFromCSFunction(this.OnGenerateChartButtonClick, VoidDelegate, this), true)
end
CGuildMemberExportWnd.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 
    if index < 0 or index >= GuildMemberExportMgr.Inst.rows.Count then
        return nil
    end
    local cellIdentifier = "RowCell"

    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(this.dataRowTemplate, cellIdentifier)
    end
    CommonDefs.GetComponent_GameObject_Type(cell, typeof(CUIDataSheetRow)):Init(GuildMemberExportMgr.Inst.rows[index], GuildMemberExportMgr.Inst.cellWidthList, GuildMemberExportMgr.Inst.horizonalPadding, index)

    return cell
end
CGuildMemberExportWnd.m_OnRowSelected_CS2LuaHook = function (this, index) 
end
CGuildMemberExportWnd.m_Init_CS2LuaHook = function (this) 
    this.tableView.dataSource = this
    this.tableView.eventDelegate = this
    this.tableView:LoadData(0, false)
    this.dataHeader:Init(GuildMemberExportMgr.Inst.headers, GuildMemberExportMgr.Inst.cellWidthList, GuildMemberExportMgr.Inst.horizonalPadding)
    this.exportBtn:SetActive(Application.platform ~= RuntimePlatform.IPhonePlayer)
end
