-- Auto Generated!!
local CInviteFriendMgr = import "L10.Game.CInviteFriendMgr"
local CInviteFriendTotalInviteWnd = import "L10.UI.CInviteFriendTotalInviteWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local MsgPackImpl = import "MsgPackImpl"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
CInviteFriendTotalInviteWnd.m_Init_CS2LuaHook = function (this) 
    this.TotalInviteWndTemplate:SetActive(false)
    Extensions.RemoveAllChildren(this.TotalInviteWndTable.transform)

    if CInviteFriendMgr.Inst.TotalInviteInfo ~= nil then
        local info = TypeAs(MsgPackImpl.unpack(CInviteFriendMgr.Inst.TotalInviteInfo), typeof(MakeGenericClass(List, Object)))
        local num = math.floor(info.Count / 3)
        do
            local i = 0
            while i < num do
                local itemNode = NGUITools.AddChild(this.TotalInviteWndTable.gameObject, this.TotalInviteWndTemplate)
                itemNode:SetActive(true)
                CommonDefs.GetComponent_Component_Type(itemNode.transform:Find("nameLabel"), typeof(UILabel)).text = ToStringWrap(info[i * 3 + 1])
                CommonDefs.GetComponent_Component_Type(itemNode.transform:Find("levelLabel"), typeof(UILabel)).text = ToStringWrap(info[i * 3 + 2])
                i = i + 1
            end
        end
    end

    this.TotalInviteWndTable:Reposition()
    this.TotalInviteWndScrollView:ResetPosition()

    UIEventListener.Get(this.TotalInviteWndExitButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)
end
