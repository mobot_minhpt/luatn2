local GameObject = import "UnityEngine.GameObject"
local CTopAndRightTipWnd = import "L10.UI.CTopAndRightTipWnd"
local CScene=import "L10.Game.CScene"

LuaOffWorldPlay1TopRightWnd = class()

RegistChildComponent(LuaOffWorldPlay1TopRightWnd, "rightBtn", "rightBtn", GameObject)
RegistChildComponent(LuaOffWorldPlay1TopRightWnd, "node1", "node1", GameObject)
RegistChildComponent(LuaOffWorldPlay1TopRightWnd, "HPItem", "HPItem", UISprite)
RegistChildComponent(LuaOffWorldPlay1TopRightWnd, "title", "title", UILabel)
RegistChildComponent(LuaOffWorldPlay1TopRightWnd, "num", "num", UILabel)
RegistChildComponent(LuaOffWorldPlay1TopRightWnd, "precent", "precent", UILabel)
RegistChildComponent(LuaOffWorldPlay1TopRightWnd, "Stone1", "Stone1", GameObject)
RegistChildComponent(LuaOffWorldPlay1TopRightWnd, "Stone2", "Stone2", GameObject)
RegistChildComponent(LuaOffWorldPlay1TopRightWnd, "Stone3", "Stone3", GameObject)

RegistClassMember(LuaOffWorldPlay1TopRightWnd, "percentMaxLength")

function LuaOffWorldPlay1TopRightWnd:OnEnable()
  if LuaOffWorldPassMgr.m_CurrScore ~=nil then
    self.num.text = LuaOffWorldPassMgr.m_CurrScore
  end
	g_ScriptEvent:AddListener("UpdateJiaoShanBloodstoneHp", self, "UpdateTop")
	g_ScriptEvent:AddListener("UpdateJiaoShanStoneCount", self, "UpdataCount")
end

function LuaOffWorldPlay1TopRightWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateJiaoShanBloodstoneHp", self, "UpdateTop")
	g_ScriptEvent:RemoveListener("UpdateJiaoShanStoneCount", self, "UpdataCount")
end

function LuaOffWorldPlay1TopRightWnd:UpdateTop()
  local hp = LuaOffWorldPassMgr.TotalHp

  -- 更新图标
  for i=1, 3 do
    local data = OffWorldPass_JiaoShanReward.GetData(i)
    if data and hp >= data.hpMin and hp <= data.hpMax then
      self:SetProgress(i)
      break
    end
  end

  if hp then
    --hp = 100 - hp
    local percent = hp/100
    self.HPItem.gameObject:SetActive(hp>0)
    self.HPItem.width = self.percentMaxLength * percent
    self.precent.text = hp..'%'
  end
end

function LuaOffWorldPlay1TopRightWnd:SetProgress(i)
  self.Stone1:SetActive(i == 1)
  self.Stone2:SetActive(i == 2)
  self.Stone3:SetActive(i == 3)
end

function LuaOffWorldPlay1TopRightWnd:UpdataCount(count)
  self.num.text = count
end

function LuaOffWorldPlay1TopRightWnd:Init()
	UIEventListener.Get(self.rightBtn).onClick = LuaUtils.VoidDelegate(function(go)
		LuaUtils.SetLocalRotation(self.rightBtn.transform,0,0,0)
		CTopAndRightTipWnd.showPackage = false
		CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
	end)

  OffWorldPass_JiaoShan.Foreach(function(key, data)
    if data.mapId == CScene.MainScene.SceneTemplateId then
        self.title.text = data.name
    end
  end)

  self.percentMaxLength = 312
  self.precent.text = '0%'
  self.num.text = 0
  
  if LuaOffWorldPassMgr.m_CurrScore ~=nil then
    self.num.text = LuaOffWorldPassMgr.m_CurrScore
  end
  self.HPItem.width = 0
  self.HPItem.gameObject:SetActive(false)

  if LuaOffWorldPassMgr.TotalHp ~= nil then
    self:UpdateTop()
  end
end

--@region UIEvent

--@endregion
