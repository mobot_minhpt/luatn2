-- Auto Generated!!
local CInviteFriendMgr = import "L10.Game.CInviteFriendMgr"
local CInviteFriendRankWnd = import "L10.UI.CInviteFriendRankWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local MsgPackImpl = import "MsgPackImpl"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
CInviteFriendRankWnd.m_Init_CS2LuaHook = function (this) 
    this.RankWndItemTemplate:SetActive(false)
    Extensions.RemoveAllChildren(this.RankWndTable.transform)

    if CInviteFriendMgr.Inst.RankInfo ~= nil then
        local info = TypeAs(MsgPackImpl.unpack(CInviteFriendMgr.Inst.RankInfo), typeof(MakeGenericClass(List, Object)))

        local num = math.floor(info.Count / 4)
        do
            local i = 0
            while i < num do
                local itemNode = NGUITools.AddChild(this.RankWndTable.gameObject, this.RankWndItemTemplate)
                itemNode:SetActive(true)
                CommonDefs.GetComponent_Component_Type(itemNode.transform:Find("rankLabel"), typeof(UILabel)).text = ToStringWrap(info[i * 4])
                CommonDefs.GetComponent_Component_Type(itemNode.transform:Find("nameLabel"), typeof(UILabel)).text = ToStringWrap(info[i * 4 + 2])
                CommonDefs.GetComponent_Component_Type(itemNode.transform:Find("inviteNumLabel"), typeof(UILabel)).text = ToStringWrap(info[i * 4 + 3])
                i = i + 1
            end
        end
    end

    this.RankWndTable:Reposition()
    this.RankWndScrollView:ResetPosition()

    UIEventListener.Get(this.RankWndExitButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)
end
