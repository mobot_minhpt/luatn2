-- Auto Generated!!
local Boolean = import "System.Boolean"
local CAuctionHistoryItem = import "L10.UI.CAuctionHistoryItem"
local CAuctionItem = import "L10.UI.CAuctionItem"
local CAuctionMgr = import "L10.Game.CAuctionMgr"
local CAuctionWnd = import "L10.UI.CAuctionWnd"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonListButtonItem = import "CCommonListButtonItem"
local CommonDefs = import "L10.Game.CommonDefs"
local CRedDotNotifier = import "L10.UI.CRedDotNotifier"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumRedDotNotifierType = import "L10.Game.EnumRedDotNotifierType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local LuaGac2Gas = Gac2Gas
local Gac2Gas = import "L10.Game.Gac2Gas"
local GameObject = import "UnityEngine.GameObject"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUITools = import "NGUITools"
local Object = import "UnityEngine.Object"
local QnButton = import "L10.UI.QnButton"
local SearchOption = import "L10.UI.SearchOption"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UIGrid = import "UIGrid"
local UInt32 = import "System.UInt32"
local UIScrollView = import "UIScrollView"
local UIWidget = import "UIWidget"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CButton = import "L10.UI.CButton"
local EnumItemAuctionType = import "L10.Game.CAuctionItemInfo+EnumItemAuctionType"

LuaAuctionWnd = {}
LuaAuctionWnd.m_Wnd = nil                                  -- CAuctionWnd类
LuaAuctionWnd.m_AuctionWndRow = -1                         -- 当前选择行
LuaAuctionWnd.m_AuctionWndSection = -1                     -- 当前选择列
LuaAuctionWnd.m_IsServerKouDaoEmpty = true                 -- 全服拍卖寇岛是否为空
LuaAuctionWnd.m_IsServerKouDaoQueryFinished = false        -- 帮会拍卖寇岛查询是否完成
LuaAuctionWnd.m_IsServerHengYuDangKouEmpty = true          -- 全服拍卖横屿荡寇是否为空
LuaAuctionWnd.m_IsServerHengYuDangKouQueryFinished = false -- 帮会拍卖横屿荡寇查询是否完成
LuaAuctionWnd.m_ServerTable = {}                           -- 全服拍卖横屿荡寇非空栏目表
LuaAuctionWnd.m_IsGuildKouDaoEmpty = true                  -- 帮会拍卖寇岛是否为空
LuaAuctionWnd.m_IsGuildKouDaoQueryFinished = false         -- 帮会拍卖寇岛查询是否完成
LuaAuctionWnd.m_IsGuildHengYuDangKouEmpty = true           -- 帮会拍卖横屿荡寇是否为空
LuaAuctionWnd.m_IsGuildHengYuDangKouQueryFinished = false  -- 帮会拍卖横屿荡寇查询是否完成
LuaAuctionWnd.m_GuildTable = {}                            -- 帮会拍卖横屿荡寇非空栏目表

-- 拍卖类型
LuaAuctionWnd.EnumAuctionType = {
    Guild = 1,  -- 帮会拍卖
    Server = 2, -- 全服拍卖
}

-- 拍卖Tab类型
LuaAuctionWnd.EnumTabType = {
    ZhuangBeiFaBao = 0, -- 装备法宝
    JiNengMiJi = 1,     -- 技能秘籍
    QiZhenYiBao = 2,    -- 奇珍异宝
    Lianghao = 3,       -- 靓号
    KouDaoChanChu = 4,  -- 寇岛产出
    HengYuDangKou = 5,  -- 横屿荡寇
    DangQianPaiMai = 6, -- 当前拍卖
    LiShiJiLu = 7,      -- 历史记录
}
LuaAuctionWnd.GuildTypeList = {}  -- 帮会拍卖Tab类型列表
LuaAuctionWnd.ServerTypeList = {} -- 全服拍卖Tab类型列表

-- 初始化拍卖物品信息
function LuaAuctionWnd:InitItemInfo()
    -- 拍卖物体Itemid以及对应名称字典
    self.EnumItemType = {
        BaoXiaYaoShi = HengYuDangKou_Setting.GetData().KeyItemId,
        JinGangZuan = HengYuDangKou_Setting.GetData().JinGangZuanItemId,
        Equip150 = HengYuDangKou_Equip.GetData(150).BoxWeight[0][0], -- 以xx级装备宝箱的id作为这类物品的id
        Equip137 = HengYuDangKou_Equip.GetData(137).BoxWeight[0][0],
        Equip129 = HengYuDangKou_Equip.GetData(129).BoxWeight[0][0],
        Equip109 = HengYuDangKou_Equip.GetData(109).BoxWeight[0][0],
        Equip89 = HengYuDangKou_Equip.GetData(89).BoxWeight[0][0],
        Equip69 = HengYuDangKou_Equip.GetData(69).BoxWeight[0][0],
    }
    self.DictItemName = {
        [self.EnumItemType.BaoXiaYaoShi] = LocalString.GetString("宝匣钥匙"),
        [self.EnumItemType.JinGangZuan] = LocalString.GetString("金刚钻"),
        [self.EnumItemType.Equip150] = LocalString.GetString("150级装备"),
        [self.EnumItemType.Equip137] = LocalString.GetString("137级装备"),
        [self.EnumItemType.Equip129] = LocalString.GetString("129级装备"),
        [self.EnumItemType.Equip109] = LocalString.GetString("109级装备"),
        [self.EnumItemType.Equip89] = LocalString.GetString("89级装备"),
        [self.EnumItemType.Equip69] = LocalString.GetString("69级装备"),
    }
    -- 寇岛tab的物品列表
    self.KouDaoItemList = {
        self.EnumItemType.BaoXiaYaoShi,
        self.EnumItemType.JinGangZuan,
    }
    -- 横屿荡寇tab的物品列表
    self.HengYuDangKouItemList = {
        self.EnumItemType.BaoXiaYaoShi,
        self.EnumItemType.JinGangZuan,
        self.EnumItemType.Equip150,
        self.EnumItemType.Equip137,
        self.EnumItemType.Equip129,
        self.EnumItemType.Equip109,
        self.EnumItemType.Equip89,
        self.EnumItemType.Equip69,
    }
end

-- 获取table第index个元素
function LuaAuctionWnd:GetTableItemByIndex(tbl, index)
    if index < 1 or index > #tbl then return -1 end
    return tbl[index]
end

LuaAuctionWnd.m_BenZhouYiPaiChuCount = nil -- 本周已拍出个数
LuaAuctionWnd.m_BenZhouYiPaiChuTotal = nil -- 本周已拍出总个数
-- 显示本周已拍出
function LuaAuctionWnd:ShowBenZhouYiPaiChu()
    local count, total = self.m_BenZhouYiPaiChuCount, self.m_BenZhouYiPaiChuTotal
    if count and total then
        self.m_Wnd.BenZhouYiPaiChuLabel.text = LocalString.GetString("本周已拍出 " .. count .. '/' .. total)
        self.m_Wnd.BenZhouYiPaiChuLabel.gameObject:SetActive(true)
    else -- 查询失败不显示
        self.m_Wnd.BenZhouYiPaiChuLabel.gameObject:SetActive(false)
    end
end
-- 隐藏本周已拍出
function LuaAuctionWnd:HideBenZhouYiPaiChu()
    self.m_BenZhouYiPaiChuCount = nil
    self.m_BenZhouYiPaiChuTotal = nil
    self.m_Wnd.BenZhouYiPaiChuLabel.gameObject:SetActive(false)
end

CAuctionWnd.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.refreshButton).onClick = MakeDelegateFromCSFunction(this.onRefreshClick, VoidDelegate, this)
    UIEventListener.Get(this.selfRefreshButton).onClick = MakeDelegateFromCSFunction(this.onSelfRefreshClick, VoidDelegate, this)
    UIEventListener.Get(this.publicityRefreshButton).onClick = MakeDelegateFromCSFunction(this.onRefreshClick, VoidDelegate, this)
    UIEventListener.Get(this.bidButton).onClick = MakeDelegateFromCSFunction(this.onBidClick, VoidDelegate, this)
    UIEventListener.Get(this.increaseButton).onClick = MakeDelegateFromCSFunction(this.onIncreaseClick, VoidDelegate, this)
    UIEventListener.Get(this.decreaseButton).onClick = MakeDelegateFromCSFunction(this.onDecreaseClick, VoidDelegate, this)
    --UIEventListener.Get(this.hintButton).onClick = MakeDelegateFromCSFunction(this.onHintButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.auditButton).onClick = MakeDelegateFromCSFunction(this.onAuditButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.noticeWhenLogin.gameObject).onClick = MakeDelegateFromCSFunction(this.onLoginNoticeClick, VoidDelegate, this)
    UIEventListener.Get(this.noticeWhenNew.gameObject).onClick = MakeDelegateFromCSFunction(this.onNewcomeNoticeClick, VoidDelegate, this)
    UIEventListener.Get(this.SettingBG).onClick = MakeDelegateFromCSFunction(this.onSettingBGClick, VoidDelegate, this)
    UIEventListener.Get(this.settingBtn).onClick = MakeDelegateFromCSFunction(this.onSettingClick, VoidDelegate, this)
    this.radioBox.OnSelect = MakeDelegateFromCSFunction(this.showContent, MakeGenericClass(Action2, QnButton, Int32), this)
    this.searchView.OnSearchCallback = MakeDelegateFromCSFunction(this.onSearchButtonClick, MakeGenericClass(Action3, UInt32, SearchOption, Boolean), this)
    this:showContent(nil, 0)
end
CAuctionWnd.m_OnEnable_CS2LuaHook = function (this) 
    CAuctionMgr.SetTotalNotifyFlag(false)
    --关闭红点
    EventManager.AddListener(EnumEventType.OnGuildAuctionItemListRefresh, MakeDelegateFromCSFunction(this.refreshGuildAuctionItems, Action0, this))
    EventManager.AddListener(EnumEventType.OnServerAuctionItemListRefresh, MakeDelegateFromCSFunction(this.refreshServerAuctionItems, Action0, this))
    EventManager.AddListener(EnumEventType.OnSelfInvestAuctionItemListRefresh, MakeDelegateFromCSFunction(this.refreshSelfInvestAuctionItems, Action0, this))
    EventManager.AddListener(EnumEventType.OnAuctionSearchItemListRefresh, MakeDelegateFromCSFunction(this.refreshSearchAuctionItems, Action0, this))
    EventManager.AddListener(EnumEventType.OnGuildAuctionHistoryListRefresh, MakeDelegateFromCSFunction(this.refreshGuildAuctionHistoryItems, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.OnAuctionBuySuccess, MakeDelegateFromCSFunction(this.onBuySuccess, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.OnAuctionRemindWayRefresh, MakeDelegateFromCSFunction(this.onNoticeWayRefresh, MakeGenericClass(Action1, UInt32), this))
    LuaAuctionWnd:OnEnable(this)
end
CAuctionWnd.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListener(EnumEventType.OnGuildAuctionItemListRefresh, MakeDelegateFromCSFunction(this.refreshGuildAuctionItems, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnServerAuctionItemListRefresh, MakeDelegateFromCSFunction(this.refreshServerAuctionItems, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnSelfInvestAuctionItemListRefresh, MakeDelegateFromCSFunction(this.refreshSelfInvestAuctionItems, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnAuctionSearchItemListRefresh, MakeDelegateFromCSFunction(this.refreshSearchAuctionItems, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnGuildAuctionHistoryListRefresh, MakeDelegateFromCSFunction(this.refreshGuildAuctionHistoryItems, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.OnAuctionBuySuccess, MakeDelegateFromCSFunction(this.onBuySuccess, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnAuctionRemindWayRefresh, MakeDelegateFromCSFunction(this.onNoticeWayRefresh, MakeGenericClass(Action1, UInt32), this))
    LuaAuctionWnd:OnDisable(this)
end
CAuctionWnd.m_showContent_CS2LuaHook = function (this, button, index) 
    repeat
        local default = this.showIndex
        if default == 0 then
            this:hideGuildAuction()
            break
        elseif default == 1 then
            this:hideServerAuction()
            break
        elseif default == 2 then
            this:hideSelfAuction()
            break
        elseif default == 3 then
            this:hideServerAuction()
            break
        end
    until 1

    this.showIndex = index
    repeat
        local extern = this.showIndex
        if extern == 0 then
            this:showGuildAuction()
            break
        elseif extern == 1 then
            this:showServerAuction(false)
            break
        elseif extern == 2 then
            this:showSelfAuction()
            break
        elseif extern == 3 then
            this:showServerAuction(true)
            break
        end
    until 1
end
CAuctionWnd.m_showGuildAuction_CS2LuaHook = function(this)
    this.rightScrollView.gameObject:SetActive(true)
    if(this.NormalScrollBG~=nil) then
        this.NormalScrollBG.gameObject:SetActive(true)
    end
    this.selfAuctionShop.gameObject:SetActive(false)
    this.normalBottom:SetActive(true)
    this.onSellArea:SetActive(true)
    this.onPublicityArea:SetActive(false)
    this.rightGrid.gameObject:SetActive(true)
    LuaAuctionWnd:QueryAuctionTabsCount_Start(LuaAuctionWnd.EnumAuctionType.Guild) -- 查询并去除空子Tab
end
CAuctionWnd.m_hideGuildAuction_CS2LuaHook = function(this)
    LuaAuctionWnd:HideBenZhouYiPaiChu()
    this.GuildTableView.gameObject:SetActive(false)
    this.rightScrollView.panel.topAnchor.absolute = 0
    this.rightScrollView.panel:ResetAndUpdateAnchors()
    this:hideEverything()
    this.rightGrid.arrangement = UIGrid.Arrangement.Horizontal
    this.rightGrid.maxPerLine = 2
    Extensions.RemoveAllChildren(this.leftGrid.transform)
    this:clearGrid(this.rightGrid)
    this.leftTabIndex = - 1
end
CAuctionWnd.m_ShowAuctionLayout_CS2LuaHook = function (this) 
    this.rightScrollView.gameObject:SetActive(true)
    if(this.NormalScrollBG~=nil) then
        this.NormalScrollBG.gameObject:SetActive(true)
    end
    this.normalBottom:SetActive(true)
    this.rightGrid.gameObject:SetActive(true)
    this:clearGrid(this.rightGrid)
    Extensions.RemoveAllChildren(this.leftGrid.transform)

    local w = CommonDefs.GetComponent_GameObject_Type(this.auctionItemPrefab, typeof(UIWidget))
    this.rightGrid.cellHeight = w.height
    this.rightGrid.cellWidth = w.width
    this.rightGrid.maxPerLine = 2
end
CAuctionWnd.m_showServerAuction_CS2LuaHook = function(this, isPublicity)
    this:ShowAuctionLayout()
    this.onSellArea:SetActive(not isPublicity)
    this.onPublicityArea:SetActive(isPublicity)
    if not isPublicity then
        LuaAuctionWnd:QueryAuctionTabsCount_Start(LuaAuctionWnd.EnumAuctionType.Server) -- 查询并去除空子Tab
    else
        this:ShowPublicityAuctionTabs()
    end
end
CAuctionWnd.m_hideServerAuction_CS2LuaHook = function (this)
    LuaAuctionWnd:HideBenZhouYiPaiChu()
    this.ShopBuyTableView.gameObject:SetActive(false)
    this:hideEverything()
    Extensions.RemoveAllChildren(this.leftGrid.transform)
    this:clearGrid(this.rightGrid)
    this.leftTabIndex = - 1
end
CAuctionWnd.m_showSelfAuction_CS2LuaHook = function (this) 
    this.rightScrollView.gameObject:SetActive(true)
    if(this.NormalScrollBG~=nil) then
        this.NormalScrollBG.gameObject:SetActive(true)
    end
    this.normalBottom:SetActive(true)
    this.onSellArea:SetActive(true)
    this.onPublicityArea:SetActive(false)
    Extensions.RemoveAllChildren(this.leftGrid.transform)

    this:showSelfAuctionShopTabs()
end
CAuctionWnd.m_hideEverything_CS2LuaHook = function(this)
    this.searchView.gameObject:SetActive(false)
    this.rightScrollView.gameObject:SetActive(false)
    if (this.NormalScrollBG ~= nil) then
        this.NormalScrollBG.gameObject:SetActive(false)
    end
    this.selfAuctionShop.gameObject:SetActive(false)
    this.normalBottom:SetActive(false)
    this.selfAuctionBottom:SetActive(false)
    this.historyRecordTitle:SetActive(false)
    this.noItemLabel.text = ""
end
CAuctionWnd.m_showGuildAuctionTabs_CS2LuaHook = function(this)
    -- 寇岛或横屿荡寇拍卖非空时，显示带有子tab的GuildTableView
    if not LuaAuctionWnd.m_IsGuildKouDaoEmpty or not LuaAuctionWnd.m_IsGuildHengYuDangKouEmpty then
        CommonDefs.ListClear(this.leftTabs)
        CUICommonDef.ClearTransform(this.leftGrid.transform)
        this.GuildTableView.gameObject:SetActive(true)

        local strArr = {}
        LuaAuctionWnd.GuildTypeList = {}
        local typetable = {}
        local List_String = MakeGenericClass(List, cs_string)
        local dict = CreateFromClass(MakeGenericClass(Dictionary, String, List_String))

        if not LuaAuctionWnd.m_IsGuildKouDaoEmpty then
            local newStr = LocalString.GetString("寇岛产出")
            table.insert(strArr, newStr)
            table.insert(LuaAuctionWnd.GuildTypeList, LuaAuctionWnd.EnumTabType.KouDaoChanChu)
            table.insert(typetable, EnumRedDotNotifierType.None)
        end
        if not LuaAuctionWnd.m_IsGuildHengYuDangKouEmpty then
            local newStr = LocalString.GetString("征海荡寇")
            table.insert(strArr, newStr)
            table.insert(LuaAuctionWnd.GuildTypeList, LuaAuctionWnd.EnumTabType.HengYuDangKou)
            local newTable = {}
            -- 添加非空的子tab
            for i, v in ipairs(LuaAuctionWnd.m_GuildTable) do
                table.insert(newTable, LuaAuctionWnd.DictItemName[v])
            end
            CommonDefs.DictAdd(dict, typeof(String), newStr, typeof(List_String), Table2List(newTable, List_String))
            table.insert(typetable, EnumRedDotNotifierType.None)
        end
        local newStr = LocalString.GetString("历史记录")
        table.insert(strArr, newStr)
        table.insert(LuaAuctionWnd.GuildTypeList, LuaAuctionWnd.EnumTabType.LiShiJiLu)
        table.insert(typetable, EnumRedDotNotifierType.None)

        local titles = Table2List(strArr, List_String)
        local typeList = Table2List(typetable, MakeGenericClass(List, EnumRedDotNotifierType))
        this.GuildTableView.OnBuySectionSelect = nil -- 先把之前的绑定事件取消，否则可能会导致多次查询
        this.GuildTableView:Init(titles, dict, typeList, true)
        CommonDefs.GetComponentsInChildren_Component_Type(this, typeof(CRedDotNotifier))
        LuaAuctionWnd.m_AuctionWndSection = 0 -- 初始化行列
        LuaAuctionWnd.m_AuctionWndRow = 0
        this.GuildTableView:SelectRowIndex(0, 0) -- 此时还没有绑定事件，手动切换
        this:onGuildAuctionTabClick(0)
        -- 绑定Table点击事件
        this.GuildTableView.OnBuySectionSelect = DelegateFactory.Action_int_int(function(section, row)
            if LuaAuctionWnd.m_AuctionWndRow ~= row or LuaAuctionWnd.m_AuctionWndSection ~= section then
                LuaAuctionWnd.m_AuctionWndSection = section
                LuaAuctionWnd.m_AuctionWndRow = row
                this:onGuildAuctionTabClick(section)
            end
        end)
        return
    end
    LuaAuctionWnd.GuildTypeList = {
        LuaAuctionWnd.EnumTabType.DangQianPaiMai,
        LuaAuctionWnd.EnumTabType.LiShiJiLu,
    }
    CommonDefs.ListClear(this.leftTabs)
    CUICommonDef.ClearTransform(this.leftGrid.transform)
    local titles = InitializeList(CreateFromClass(MakeGenericClass(List, String)), String, LocalString.GetString("当前拍卖"), LocalString.GetString("历史记录"))
    this:addCommonButtonToLeftGrid(titles,
        MakeDelegateFromCSFunction(this.onGuildAuctionTabClick, MakeGenericClass(Action1, Int32), this), 0)
    this.leftGrid:Reposition()
    this.leftScrollView:InvalidateBounds()
    this.leftScrollView:SetDragAmount(0, 0, false)
    this:onGuildAuctionTabClick(0)
end

CAuctionWnd.m_showServerAuctionTabs_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.leftTabs)
    this.ShopBuyTableView.gameObject:SetActive(true)

    local searchBtnPos = this.ShopBuyTableView.transform:Find("SearchBtnPos")
    Extensions.RemoveAllChildren(searchBtnPos.transform)
    local go = NGUITools.AddChild(searchBtnPos.gameObject, this.searchButtonPrefab)
    go:SetActive(true)
    this.searchButton = go

    local strArr = {
        LocalString.GetString("装备法宝"),
        LocalString.GetString("技能秘籍"),
        LocalString.GetString("奇珍异宝"),
        LocalString.GetString("靓号"),
    }
    LuaAuctionWnd.ServerTypeList = {
        LuaAuctionWnd.EnumTabType.ZhuangBeiFaBao,
        LuaAuctionWnd.EnumTabType.JiNengMiJi,
        LuaAuctionWnd.EnumTabType.QiZhenYiBao,
        LuaAuctionWnd.EnumTabType.Lianghao,
    }
    local typetable = { -- 红点提醒列表
        EnumRedDotNotifierType.Auction_ZhuangbeiAuction,
        EnumRedDotNotifierType.Auction_JinengAuction,
        EnumRedDotNotifierType.Auction_QizhenyibaoAuction,
        EnumRedDotNotifierType.None,
    }
    local List_String = MakeGenericClass(List,cs_string)
    local dict = CreateFromClass(MakeGenericClass(Dictionary, String, List_String))

    if not LuaAuctionWnd.m_IsServerKouDaoEmpty then -- 添加寇岛产出栏目
        local newStr = LocalString.GetString("寇岛产出")
        table.insert(strArr, newStr)
        table.insert(LuaAuctionWnd.ServerTypeList, LuaAuctionWnd.EnumTabType.KouDaoChanChu)
        CommonDefs.DictAdd(dict, typeof(String), newStr, typeof(List_String),
            Table2List({ LocalString.GetString("宝匣钥匙"), LocalString.GetString("金刚钻") }, List_String))
        table.insert(typetable, EnumRedDotNotifierType.Auction_KouDao) -- 添加寇岛红点事件
    end
    if not LuaAuctionWnd.m_IsServerHengYuDangKouEmpty then -- 添加横屿荡寇栏目
        local newStr = LocalString.GetString("征海荡寇")
        table.insert(strArr, newStr)
        table.insert(LuaAuctionWnd.ServerTypeList, LuaAuctionWnd.EnumTabType.HengYuDangKou)
        local newTable = {}
        -- 添加非空的子tab
        for i, v in ipairs(LuaAuctionWnd.m_ServerTable) do
            table.insert(newTable, LuaAuctionWnd.DictItemName[v])
        end
        CommonDefs.DictAdd(dict, typeof(String), newStr, typeof(List_String), Table2List(newTable, List_String))
        table.insert(typetable, EnumRedDotNotifierType.Auction_HengYuDangKou) -- 添加横屿荡寇红点事件
    end

    local titles = Table2List(strArr, List_String)
    local typeList = Table2List(typetable, MakeGenericClass(List, EnumRedDotNotifierType))
    this.ShopBuyTableView.OnBuySectionSelect = nil -- 先把之前的绑定事件取消，否则可能会导致多次查询
    this.ShopBuyTableView:Init(titles, dict, typeList, true)
    CommonDefs.GetComponentsInChildren_Component_Type(this, typeof(CRedDotNotifier))
    LuaAuctionWnd.m_AuctionWndSection = 0 -- 初始化行列
    LuaAuctionWnd.m_AuctionWndRow = 0
    this.ShopBuyTableView:SelectRowIndex(0, 0)
    this:onServerAuctionTabClick(1) -- 此时还没有绑定事件，手动切换
    -- 绑定Table点击事件
    this.ShopBuyTableView.OnBuySectionSelect = DelegateFactory.Action_int_int(function(section, row)
        if LuaAuctionWnd.m_AuctionWndRow ~= row or LuaAuctionWnd.m_AuctionWndSection ~= section then
            LuaAuctionWnd.m_AuctionWndSection = section
            LuaAuctionWnd.m_AuctionWndRow = row
            this:onServerAuctionTabClick(section + 1)
        end
    end)
    UIEventListener.Get(this.searchButton).onClick = MakeDelegateFromCSFunction(this.onSearchTabClick, VoidDelegate, this)
end
CAuctionWnd.m_ShowPublicityAuctionTabs_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.leftTabs)
    local titles = InitializeList(CreateFromClass(MakeGenericClass(List, String)), String, LocalString.GetString("所有公示"), LocalString.GetString("装备法宝"), LocalString.GetString("技能秘籍"), LocalString.GetString("奇珍异宝"), LocalString.GetString("社区凭证"))
    local list = this:addCommonButtonToLeftGrid(titles, MakeDelegateFromCSFunction(this.OnPublicityAuctionTabClick, MakeGenericClass(Action1, Int32), this), 0)
    this.leftGrid:Reposition()
    this.leftScrollView:InvalidateBounds()
    this.leftScrollView:SetDragAmount(0, 0, false)
    this:OnPublicityAuctionTabClick(0)
end
CAuctionWnd.m_showSelfAuctionShopTabs_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.leftTabs)
    local titles = InitializeList(CreateFromClass(MakeGenericClass(List, String)), String, LocalString.GetString("竞拍物品"), LocalString.GetString("上架物品"))
    this:addCommonButtonToLeftGrid(titles, MakeDelegateFromCSFunction(this.onSelfAuctionTabClick, MakeGenericClass(Action1, Int32), this), 0)
    this.leftGrid:Reposition()
    this.leftScrollView:InvalidateBounds()
    this.leftScrollView:SetDragAmount(0, 0, false)
    this:onSelfAuctionTabClick(0)
end
CAuctionWnd.m_addCommonButtonToLeftGrid_CS2LuaHook = function (this, titles, clickCallback, startIndex) 

    local goList = CreateFromClass(MakeGenericClass(List, GameObject))
    do
        local i = 0
        while i < titles.Count do
            local go = NGUITools.AddChild(this.leftGrid.gameObject, this.tabPrefab)
            local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CCommonListButtonItem))
            CommonDefs.ListAdd(this.leftTabs, typeof(CCommonListButtonItem), item)
            item:SetNameLabel(titles[i])
            item.clickAction = clickCallback
            item.Index = i + startIndex
            go:SetActive(true)
            CommonDefs.ListAdd(goList, typeof(GameObject), go)
            i = i + 1
        end
    end
    return goList
end
CAuctionWnd.m_showCurrentAuctionItems_CS2LuaHook = function (this) 
    local pageVolume = 8
    if this.showIndex == 2 then
        pageVolume = 6
    end

    CAuctionMgr.Inst.FocusInstanceID = ""
    this:clearGrid(this.rightGrid)
    local gridItem = nil

    CommonDefs.ListClear(this.currentAuctionItems)
    local dataList = CAuctionMgr.Inst.CurrentItemInfoList
    local tableList = {}
    for i = 0, dataList.Count - 1 do
        table.insert(tableList, dataList[i])
    end
    for _, info in pairs(tableList) do
        local go = CommonDefs.Object_Instantiate(this.auctionItemPrefab)
        if gridItem == nil then
            gridItem = go
        end
        go:SetActive(true)
        local trans = go.transform
        this.rightGrid:AddChild(trans)
        trans.localScale = Vector3.one
        local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CAuctionItem))
        item.clickAction = MakeDelegateFromCSFunction(this.onAuctionItemClick, MakeGenericClass(Action1, String), this)
        item.ItemInfo = info -- 更新物品信息并显示
        item:SetHighLight(false)
        CommonDefs.ListAdd(this.currentAuctionItems, typeof(CAuctionItem), item)
        if this.showIndex == 2 then
            item:SetMode(1)
        else
            item:SetMode(0)
        end
    end

    if gridItem ~= nil then
        this.rightScrollView.panel.topAnchor.absolute = 0
        this.rightScrollView.panel:ResetAndUpdateAnchors()
        this.rightGrid.arrangement = UIGrid.Arrangement.Horizontal
        this.rightGrid.maxPerLine = 2
        local w = CommonDefs.GetComponent_GameObject_Type(gridItem, typeof(UIWidget))
        this.rightGrid.cellHeight = w.height + 2
        this.rightGrid.cellWidth = w.width + 5

        this.rightGrid:Reposition()
        this.rightScrollView.movement = UIScrollView.Movement.Unrestricted
        this.rightScrollView:InvalidateBounds()
        this.rightScrollView:SetDragAmount(0, 0, false)
        this.rightScrollView.movement = UIScrollView.Movement.Vertical
    end

    local totalPage = 0
    if CAuctionMgr.Inst.TotalCount > 0 and gridItem ~= nil then
        totalPage = (math.floor((CAuctionMgr.Inst.TotalCount - 1) / pageVolume)) + 1
        this:setPageMaxValue(totalPage)
        if (CAuctionMgr.Inst.PageIndex + 1) > totalPage then
            CAuctionMgr.Inst.PageIndex = math.min(CAuctionMgr.Inst.PageIndex, totalPage - 1)
        end
        this:showPageCount(System.String.Format("{0}/{1}", CAuctionMgr.Inst.PageIndex + 1, totalPage))
        CommonDefs.GetComponent_GameObject_Type(this.decreaseButton, typeof(QnButton)).Enabled = CAuctionMgr.Inst.PageIndex > 0
        CommonDefs.GetComponent_GameObject_Type(this.increaseButton, typeof(QnButton)).Enabled = CAuctionMgr.Inst.PageIndex < (totalPage - 1)
        this.noItemLabel.text = ""
        this:onAuctionItemClick(this.currentAuctionItems[0].ItemInfo.AuctionID) -- 选中第一个拍卖品
    else
        this:setPageMaxValue(totalPage)
        CommonDefs.GetComponent_GameObject_Type(this.increaseButton, typeof(QnButton)).Enabled = false
        CommonDefs.GetComponent_GameObject_Type(this.decreaseButton, typeof(QnButton)).Enabled = false

        repeat
            local default = this.showIndex
            if default == 0 or default == 1 then
                this.noItemLabel.text = LocalString.GetString("暂时没有物品拍卖")
                break
            elseif default == 2 then
                this.noItemLabel.text = LocalString.GetString("您暂时没有竞拍任何物品")
                break
            end
        until 1

        this:showPageCount("0/0")
    end

    this.historyRecordTitle:SetActive(false)
end
CAuctionWnd.m_refreshGuildAuctionHistoryItems_CS2LuaHook = function (this) 
    this:clearGrid(this.rightGrid)
    this.rightGrid.arrangement = UIGrid.Arrangement.Vertical
    this.rightGrid.maxPerLine = 0
    this.rightScrollView.panel.topAnchor.absolute = - 70
    this.rightScrollView.panel:ResetAndUpdateAnchors()
    local w = CommonDefs.GetComponent_GameObject_Type(this.historyAuctionItemPrefab, typeof(UIWidget))
    this.rightGrid.cellHeight = w.height + 4
    this.rightGrid.cellWidth = w.width
    local dataList = CAuctionMgr.Inst.CurrentItemInfoList

    if dataList.Count == 0 then
        this.noItemLabel.text = LocalString.GetString("没有拍卖历史纪录")
    else
        this.noItemLabel.text = ""
        do
            local i = 0
            while i < dataList.Count do
                local go = CommonDefs.Object_Instantiate(this.historyAuctionItemPrefab)
                go:SetActive(true)
                local trans = go.transform
                this.rightGrid:AddChild(trans)
                trans.localScale = Vector3.one
                local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CAuctionHistoryItem))
                local info = dataList[i]
                item:SetInfo(info)
                i = i + 1
            end
        end
        this.rightScrollView.movement = UIScrollView.Movement.Unrestricted
        this.rightScrollView:InvalidateBounds()
        this.rightScrollView:SetDragAmount(0, 0, false)
        this.rightScrollView.movement = UIScrollView.Movement.Vertical
    end
    this.historyRecordTitle:SetActive(true)
end
CAuctionWnd.m_showSelfInvestItems_CS2LuaHook = function (this) 
    this.rightGrid.gameObject:SetActive(true)
    this.normalBottom:SetActive(true)
    this.onSellArea:SetActive(true)
    this.onPublicityArea:SetActive(false)
    if(this.NormalScrollBG~=nil) then
        this.NormalScrollBG.gameObject:SetActive(true)
    end
    this.selfAuctionShop.gameObject:SetActive(false)
    this.selfAuctionBottom:SetActive(false)
    this:onPageValueChange(1)
end
CAuctionWnd.m_setRightScrollViewParams_CS2LuaHook = function (this, mode) 
    local w = nil
    repeat
        local default = mode
        if default == 0 then
            this.rightScrollView.panel.topAnchor.absolute = 0
            this.rightGrid.arrangement = UIGrid.Arrangement.Horizontal
            this.rightGrid.maxPerLine = 2
            w = CommonDefs.GetComponent_GameObject_Type(this.auctionItemPrefab, typeof(UIWidget))
            this.rightGrid.cellHeight = 167 --[[CAuctionItem.NormalHeight + 2]]
            this.rightGrid.cellWidth = w.width
            break
        elseif default == 1 then
            this.rightScrollView.panel.topAnchor.absolute = 0
            this.rightGrid.arrangement = UIGrid.Arrangement.Horizontal
            this.rightGrid.maxPerLine = 2
            w = CommonDefs.GetComponent_GameObject_Type(this.auctionItemPrefab, typeof(UIWidget))
            this.rightGrid.cellHeight = 217 --[[CAuctionItem.SelfAuctionItemHeight + 2]]
            this.rightGrid.cellWidth = w.width
            break
        elseif default == 2 then
            this.rightGrid.arrangement = UIGrid.Arrangement.Vertical
            this.rightGrid.maxPerLine = 0
            this.rightScrollView.panel.topAnchor.absolute = - 70
            w = CommonDefs.GetComponent_GameObject_Type(this.historyAuctionItemPrefab, typeof(UIWidget))
            this.rightGrid.cellHeight = w.height + 4
            this.rightGrid.cellWidth = w.width
            break
        end
    until 1
end
CAuctionWnd.m_showSelfAuctionShelf_CS2LuaHook = function (this) 
    this.rightGrid.gameObject:SetActive(false)
    this.normalBottom:SetActive(false)
    this.noItemLabel.text = ""
    if(this.NormalScrollBG~=nil) then
        this.NormalScrollBG.gameObject:SetActive(false)
    end
    this.selfAuctionShop.gameObject:SetActive(true)
    this.selfAuctionBottom:SetActive(true)
end
CAuctionWnd.m_refreshGuildAuctionItems_CS2LuaHook = function (this) 
    if this.showIndex ~= 0 then
        return
    end
    if this.leftTabIndex ~= 0 then
        return
    end

    this:showCurrentAuctionItems()
    this:setPageValue(CAuctionMgr.Inst.PageIndex + 1)
end
CAuctionWnd.m_refreshServerAuctionItems_CS2LuaHook = function (this) 
    if this.showIndex ~= 1 and this.showIndex ~= 3 then
        return
    end
    this:showCurrentAuctionItems()
    this:setPageValue(CAuctionMgr.Inst.PageIndex + 1)
end
CAuctionWnd.m_refreshSelfInvestAuctionItems_CS2LuaHook = function (this) 
    if this.showIndex ~= 2 then
        return
    end
    if this.leftTabIndex ~= 0 then
        return
    end
    this:showCurrentAuctionItems()
    this:setPageValue(CAuctionMgr.Inst.PageIndex + 1)
end
CAuctionWnd.m_refreshSearchAuctionItems_CS2LuaHook = function(this)
    if this.showIndex ~= 1 then
        return
    end
    if this.leftTabIndex ~= 0 then
        return
    end

    this.searchView.gameObject:SetActive(false)
    this.rightGrid.gameObject:SetActive(true)
    this:showCurrentAuctionItems()
    this:setPageValue(CAuctionMgr.Inst.PageIndex + 1)
end
CAuctionWnd.m_onGuildAuctionTabClick_CS2LuaHook = function (this, index) 
    LuaAuctionWnd:HideBenZhouYiPaiChu()
    this.leftTabIndex = index
    do
        local i = 0
        while i < this.leftTabs.Count do
            if i == index then
                this.leftTabs[i]:SetHighLight(true)
            else
                this.leftTabs[i]:SetHighLight(false)
            end
            i = i + 1
        end
    end
    repeat
        local tp = LuaAuctionWnd:GetTableItemByIndex(LuaAuctionWnd.GuildTypeList, index + 1)
        if tp ~= LuaAuctionWnd.EnumTabType.LiShiJiLu then -- 非历史记录
            this.historyRecordTitle:SetActive(false)
            this.normalBottom:SetActive(true)
            this.onSellArea:SetActive(true)
            this.onPublicityArea:SetActive(false)
            this:clearGrid(this.rightGrid)
            this:onPageValueChange(1)
            break
        else -- 历史记录
            this.normalBottom:SetActive(false)
            this.historyRecordTitle:SetActive(true)
            this:clearGrid(this.rightGrid)
            this:onPageValueChange(1)
            break
        end
    until 1
end
CAuctionWnd.m_onServerAuctionTabClick_CS2LuaHook = function (this, index) 
    LuaAuctionWnd:HideBenZhouYiPaiChu()
    this.leftTabIndex = index
    if index == 0 then
        this.searchView.gameObject:SetActive(true)
        this.searchView:RefreshHistory()
        CommonDefs.GetComponent_GameObject_Type(this.searchButton, typeof(CButton)).Selected = true
        this.rightGrid.gameObject:SetActive(false)
        this.noItemLabel.text = ""
        do
            local i = 0
            while i < this.leftTabs.Count do
                this.leftTabs[i]:SetHighLight(false)
                i = i + 1
            end
        end
    else
        local tp = LuaAuctionWnd:GetTableItemByIndex(LuaAuctionWnd.ServerTypeList, index)
        this.rightGrid.gameObject:SetActive(true)
        this.searchView.gameObject:SetActive(false)
        do
            local i = 0
            while i < this.leftTabs.Count do
                if (i + 1) == index then
                    this.leftTabs[i]:SetHighLight(true)
                else
                    this.leftTabs[i]:SetHighLight(false)
                end
                i = i + 1
            end
        end
        CommonDefs.GetComponent_GameObject_Type(this.searchButton, typeof(CButton)).Selected = false
        if tp == LuaAuctionWnd.EnumTabType.Lianghao then -- 靓号按钮，弹出靓号窗口
            LuaLiangHaoMgr.ShowLiangHaoPurchaseWnd()
            CUIManager.CloseUI(CUIResources.YuanBaoMarketWnd)
        elseif tp == LuaAuctionWnd.EnumTabType.KouDaoChanChu then -- 寇岛产出
            if LuaAuctionMgr:IsEnableNewKouDaoAuction() and this.showIndex == 1 then
                CAuctionMgr.Inst:SetWorldCurrentAuctionNeedNotifier(4, false)
            end
            this:onPageValueChange(1)
        elseif tp == LuaAuctionWnd.EnumTabType.HengYuDangKou then -- 横屿荡寇
            if LuaAuctionMgr:IsEnableHengYuDangKouAuction() and this.showIndex == 1 then
                CAuctionMgr.Inst:SetWorldCurrentAuctionNeedNotifier(5, false)
            end
            this:onPageValueChange(1)
        else
            CAuctionMgr.Inst:SetWorldCurrentAuctionNeedNotifier(index - 1, false)
            this:onPageValueChange(1)
        end
    end
end
CAuctionWnd.m_OnPublicityAuctionTabClick_CS2LuaHook = function (this, index) 
    this.leftTabIndex = index
    this.rightGrid.gameObject:SetActive(true)
    do
        local i = 0
        while i < this.leftTabs.Count do
            if i == index then
                this.leftTabs[i]:SetHighLight(true)
            else
                this.leftTabs[i]:SetHighLight(false)
            end
            i = i + 1
        end
    end
    CAuctionMgr.Inst:SetWorldCurrentAuctionNeedNotifier(index, false)
    this:onPageValueChange(1)
end
CAuctionWnd.m_onSelfAuctionTabClick_CS2LuaHook = function (this, index) 
    this.leftTabIndex = index
    do
        local i = 0
        while i < this.leftTabs.Count do
            if i == index then
                this.leftTabs[i]:SetHighLight(true)
            else
                this.leftTabs[i]:SetHighLight(false)
            end
            i = i + 1
        end
    end
    repeat
        local default = index
        if default == 0 then
            this:showSelfInvestItems()
            break
        elseif default == 1 then
            this:showSelfAuctionShelf()
            break
        end
    until 1
end
CAuctionWnd.m_onAuctionItemClick_CS2LuaHook = function (this, ID) 
    do
        local i = 0
        while i < this.currentAuctionItems.Count do
            if this.currentAuctionItems[i].ItemInfo.AuctionID == ID then
                this.currentAuctionItems[i]:SetHighLight(true)
                if this.currentAuctionItems[i].iteminfo.Price == 0 then
                    this.bidButtonLabel.text = LocalString.GetString("一口价")
                else
                    this.bidButtonLabel.text = LocalString.GetString("竞  拍")
                end
                LuaAuctionMgr.m_CurSelectItem = this.currentAuctionItems[i].ItemInfo
            else
                this.currentAuctionItems[i]:SetHighLight(false)
            end
            i = i + 1
        end
    end
    CAuctionMgr.Inst.FocusInstanceID = ID
end
CAuctionWnd.m_clearGrid_CS2LuaHook = function (this, grid) 
    local childList = grid:GetChildList()
    do
        local i = 0
        while i < childList.Count do
            local trans = childList[i]
            trans.parent = nil
            Object.Destroy(trans.gameObject)
            i = i + 1
        end
    end
end
CAuctionWnd.m_onSearchButtonClick_CS2LuaHook = function (this, templateID, option, jumpToZhuShaBi) 
    CAuctionMgr.Inst:AddHistory(templateID)
    CAuctionMgr.Inst.SearchingTemplateID = templateID
    --    setPageValue(1);
    this:onPageValueChange(1)
    this.searchView:RefreshHistory()
end
CAuctionWnd.m_onBidClick_CS2LuaHook = function (this, go) 
    if System.String.IsNullOrEmpty(CAuctionMgr.Inst.FocusInstanceID) then
        return
    end
    if this.bidButtonLabel.text == LocalString.GetString("一口价") then
        local info = CAuctionMgr.Inst:GetCurrentItemInfo(CAuctionMgr.Inst.FocusInstanceID)
        if info == nil then
            return
        end
        local item = CAuctionMgr.Inst:GetItem(info.InstanceID)
        if item == nil then
            return
        end
        local text = System.String.Format(LocalString.GetString("你确定要以{0}灵玉一口价购买1个{1}吗？"), info.FixPrice, item.Name)
        MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(function () 
            Gac2Gas.RequestPaimaiBet(info.AuctionID, info.TemplateID, info.FixPrice)
        end), nil, nil, nil, false)
    else
        local info = CAuctionMgr.Inst:GetCurrentItemInfo(CAuctionMgr.Inst.FocusInstanceID)
        if info and info.AuctionType == EnumItemAuctionType.System then
            CUIManager.ShowUI("NewAuctionBidWnd")
            return
        end
        CUIManager.ShowUI(CUIResources.AuctionBidWnd)
    end
end
CAuctionWnd.m_onPageValueChange_CS2LuaHook = function(this, page)
    page = page - 1
    if page < 0 then page = 0 end
    repeat
        local default = this.showIndex
        if default == 0 then
            this:onPageChangeInGuildAcution(page)
            break
        elseif default == 1 and page >= 0 then
            if this.leftTabIndex == 5 then
                this:onPageChangeInServerAuction(page, 4) -- 中间多了个靓号
            else
                this:onPageChangeInServerAuction(page, this.leftTabIndex)
            end
            break
        elseif default == 2 then
            this:onPageChangeInSelfInvestAuction(page)
            break
        elseif default == 3 then
            this:OnPageChangeToPublicityAuction(page, this.leftTabIndex)
            break
        end
    until 1
end
CAuctionWnd.m_onPageChangeInGuildAcution_CS2LuaHook = function (this, page) 
    local tp = LuaAuctionWnd:GetTableItemByIndex(LuaAuctionWnd.GuildTypeList, this.leftTabIndex + 1)
    if tp ~= LuaAuctionWnd.EnumTabType.LiShiJiLu then
        -- 加载寇岛产出拍卖内容
        if tp == LuaAuctionWnd.EnumTabType.KouDaoChanChu then
            Gac2Gas.GetGuildPaiMaiOnShelfGoods(page * 8 + 1, (page + 1) * 8, page, 1)
            return
        end
        -- 加载横屿荡寇拍卖内容
        if tp == LuaAuctionWnd.EnumTabType.HengYuDangKou then
            local itemid = LuaAuctionWnd:GetTableItemByIndex(LuaAuctionWnd.m_GuildTable, LuaAuctionWnd.m_AuctionWndRow + 1)
            if itemid >= 0 then
                this:setPageValue(CAuctionMgr.Inst.PageIndex + 1)
                LuaGac2Gas.QueryGamePlayPaiMaiOnShelfItem(
                    LuaAuctionWnd.EnumAuctionType.Guild, LuaAuctionMgr.HengYuDangKouGamePlayId,
                    page * 8 + 1, (page + 1) * 8, page, itemid
                )
            else
                CAuctionMgr.Inst.PageIndex = 0
                CAuctionMgr.Inst.TotalCount = 0
                CommonDefs.ListClear(CAuctionMgr.Inst.CurrentItemInfoList)
                this:showCurrentAuctionItems()
                this:setPageValue(CAuctionMgr.Inst.PageIndex + 1)
            end
            return
        end
        Gac2Gas.GetGuildPaiMaiOnShelfGoods(page * 8 + 1, (page + 1) * 8, page, 1)
    else
        Gac2Gas.GetGuildPaimaiHistory()
    end
end
CAuctionWnd.m_onPageChangeInServerAuction_CS2LuaHook = function (this, page, clazz) 
    if clazz ~= 0 then
        local tp = LuaAuctionWnd:GetTableItemByIndex(LuaAuctionWnd.ServerTypeList, this.leftTabIndex)
        -- 加载寇岛产出拍卖内容
        if tp == LuaAuctionWnd.EnumTabType.KouDaoChanChu then
            local itemTemplateId = LuaAuctionWnd.m_AuctionWndRow == 0 and 21000708 or 21000486
            this:setPageValue(CAuctionMgr.Inst.PageIndex + 1)
            LuaGac2Gas.QueryPaimaiSystemOnShelfItem(page * 8 + 1, (page + 1) * 8, page, itemTemplateId)
            return
        end
        -- 加载横屿荡寇拍卖内容
        if tp == LuaAuctionWnd.EnumTabType.HengYuDangKou then
            local itemid = LuaAuctionWnd:GetTableItemByIndex(LuaAuctionWnd.m_ServerTable, LuaAuctionWnd.m_AuctionWndRow + 1)
            if itemid >= 0 then
                this:setPageValue(CAuctionMgr.Inst.PageIndex + 1)
                LuaGac2Gas.QueryGamePlayPaiMaiOnShelfItem(
                    LuaAuctionWnd.EnumAuctionType.Server, LuaAuctionMgr.HengYuDangKouGamePlayId,
                    page * 8 + 1, (page + 1) * 8, page, itemid
                )
            else
                CAuctionMgr.Inst.PageIndex = 0
                CAuctionMgr.Inst.TotalCount = 0
                CommonDefs.ListClear(CAuctionMgr.Inst.CurrentItemInfoList)
                this:showCurrentAuctionItems()
                this:setPageValue(CAuctionMgr.Inst.PageIndex + 1)
            end
            return
        end
        Gac2Gas.GetWorldPaiMaiOnShelfGoods(page * 8 + 1, (page + 1) * 8, page, clazz)
    else
        Gac2Gas.GetPaiMaiGoodsByTemplateId(CAuctionMgr.Inst.SearchingTemplateID, page * 8 + 1, (page + 1) * 8, page, 0)
    end
end
CAuctionWnd.m_onIncreaseClick_CS2LuaHook = function (this, go) 
    this.currentPageValue = this.currentPageValue + 1
    if this.currentPageValue > this.maxPageValue then
        this.currentPageValue = this.maxPageValue
        return
    end
    this:onPageValueChange(this.currentPageValue)
end
CAuctionWnd.m_onDecreaseClick_CS2LuaHook = function (this, go) 
    this.currentPageValue = this.currentPageValue - 1
    if this.currentPageValue < 1 then
        this.currentPageValue = 1
        return
    end
    this:onPageValueChange(this.currentPageValue)
end
CAuctionWnd.m_showPageCount_CS2LuaHook = function (this, s) 
    this.pageLabel.text = s
end
CAuctionWnd.m_onHintButtonClick_CS2LuaHook = function (this, go) 
    repeat
        local default = this.showIndex
        if default == 0 then
            g_MessageMgr:ShowMessage("message_bangpaipaimai_readme")
            break
        elseif default == 1 then
            g_MessageMgr:ShowMessage("message_quanfupaimai_readme")
            break
        elseif default == 2 then
            g_MessageMgr:ShowMessage("message_wodepaimai_readme")
            break
        end
    until 1
end
CAuctionWnd.m_onAuditButtonClick_CS2LuaHook = function (this, go) 
    Gac2Gas.GetPaiMaiFrozenInfo()
end
CAuctionWnd.m_onSettingClick_CS2LuaHook = function (this, go) 
    if this.SettingPanel.activeSelf then
        this.SettingPanel:SetActive(false)
    else
        this:refreshSettingCheckbox()
    end
end
CAuctionWnd.m_onLoginNoticeClick_CS2LuaHook = function (this, go) 
    if CClientMainPlayer.Inst.PlayProp.PaiMaiRemindWay ~= 0 then
        Gac2Gas.SetPaiMaiRemindWay(0)
    end
end
CAuctionWnd.m_onNewcomeNoticeClick_CS2LuaHook = function (this, go) 
    if CClientMainPlayer.Inst.PlayProp.PaiMaiRemindWay ~= 1 then
        Gac2Gas.SetPaiMaiRemindWay(1)
    end
end
CAuctionWnd.m_onNoticeWayRefresh_CS2LuaHook = function (this, index) 
    CClientMainPlayer.Inst.PlayProp.PaiMaiRemindWay = index
    this:refreshSettingCheckbox()
end
CAuctionWnd.m_onSettingBGClick_CS2LuaHook = function (this, go) 
    this.SettingPanel:SetActive(false)
end
CAuctionWnd.m_refreshSettingCheckbox_CS2LuaHook = function (this) 
    this.SettingPanel:SetActive(true)
    local index = CClientMainPlayer.Inst.PlayProp.PaiMaiRemindWay
    if index == 0 then
        this:setCheckBox(this.noticeWhenLogin, true)
        this:setCheckBox(this.noticeWhenNew, false)
    else
        this:setCheckBox(this.noticeWhenLogin, false)
        this:setCheckBox(this.noticeWhenNew, true)
    end
end
CAuctionWnd.m_setCheckBox_CS2LuaHook = function (this, go, flag) 
    local trans = CUICommonDef.TraverseFindChild("Checkmark", go.transform)
    if trans then
        trans.gameObject:SetActive(flag)
    end
end
CAuctionWnd.m_setPageValue_CS2LuaHook = function (this, v) 
    this.currentPageValue = v
    if this.currentPageValue < 1 then
        this.currentPageValue = 1
    end
    if this.currentPageValue > this.maxPageValue then
        this.currentPageValue = this.maxPageValue
    end
end

function LuaAuctionWnd:OnEnable(this)
    self:InitItemInfo()
    self.m_Wnd = this
    g_ScriptEvent:AddListener("OnSendSystemOnShelfItem", self, "OnSendSystemOnShelfItem")
    g_ScriptEvent:AddListener("OnPlayerRequestBetSystemResult", self, "OnPlayerRequestBetSystemResult")
    g_ScriptEvent:AddListener("OnSyncSystemPaimaiDelay", self, "OnSyncSystemPaimaiDelay")
    g_ScriptEvent:AddListener("OnSendGamePlayPaiMaiOnShelfCount", self, "OnSendGamePlayPaiMaiOnShelfCount")
    g_ScriptEvent:AddListener("OnSendGamePlayPaiMaiOnShelfItem", self, "OnSendGamePlayPaiMaiOnShelfItem")
    g_ScriptEvent:AddListener("OnRequestGamePlayPaiMaiBetResult", self, "OnRequestGamePlayPaiMaiBetResult")
end

function LuaAuctionWnd:OnDisable(this)
    g_ScriptEvent:RemoveListener("OnSendSystemOnShelfItem", self, "OnSendSystemOnShelfItem")
    g_ScriptEvent:RemoveListener("OnPlayerRequestBetSystemResult", self, "OnPlayerRequestBetSystemResult")
    g_ScriptEvent:RemoveListener("OnSyncSystemPaimaiDelay", self, "OnSyncSystemPaimaiDelay")
    g_ScriptEvent:RemoveListener("OnSendGamePlayPaiMaiOnShelfCount", self, "OnSendGamePlayPaiMaiOnShelfCount")
    g_ScriptEvent:RemoveListener("OnSendGamePlayPaiMaiOnShelfItem", self, "OnSendGamePlayPaiMaiOnShelfItem")
    g_ScriptEvent:RemoveListener("OnRequestGamePlayPaiMaiBetResult", self, "OnRequestGamePlayPaiMaiBetResult")
    self.m_Wnd = nil
    LuaAuctionWnd.m_AuctionWndRow = -1
    LuaAuctionWnd.m_AuctionWndSection = -1
end

function LuaAuctionWnd:OnSendSystemOnShelfItem()
    if not self.m_Wnd then return end
    local this = self.m_Wnd
    if this.showIndex ~= 1 then
        return
    end
    if this.leftTabIndex ~= 5 then
        return
    end
    this.rightGrid.gameObject:SetActive(true)
    this.searchView.gameObject:SetActive(false)
    this:showCurrentAuctionItems()
    this:setPageValue(CAuctionMgr.Inst.PageIndex + 1)
    this.noItemLabel.text = CAuctionMgr.Inst.TotalCount == 0 and LocalString.GetString("暂时没有物品拍卖") or ""
end

-- 请求玩法拍卖上架商品
-- Gac2Gas.QueryGamePlayPaiMaiOnShelfItem
-- Gas2Gac.SendGamePlayPaiMaiOnShelfItem
function LuaAuctionWnd:OnSendGamePlayPaiMaiOnShelfItem(targetType, playId, validTotalCount, infoRpcUd, pageNum, itemTemplateId, onShelfCurCount, onShelfTotalCount)
    if not self.m_Wnd then return end
    local this = self.m_Wnd
    local targetindex = 0 -- 目标拍卖类型
    local lefttabtype = -1 -- 左侧tab类型
    local lefttabplayid = -1 -- 左侧tab玩法id
    if targetType == LuaAuctionWnd.EnumAuctionType.Guild then
        targetindex = 0
        lefttabtype = LuaAuctionWnd:GetTableItemByIndex(LuaAuctionWnd.GuildTypeList, this.leftTabIndex + 1)
    else
        targetindex = 1
        lefttabtype = LuaAuctionWnd:GetTableItemByIndex(LuaAuctionWnd.ServerTypeList, this.leftTabIndex)
    end
    if lefttabtype == LuaAuctionWnd.EnumTabType.KouDaoChanChu then
        lefttabplayid = LuaAuctionMgr.KouDaoGamePlayId
    elseif lefttabtype == LuaAuctionWnd.EnumTabType.HengYuDangKou then
        lefttabplayid = LuaAuctionMgr.HengYuDangKouGamePlayId
    end
    if this.showIndex ~= targetindex or lefttabplayid ~= playId then return end -- 已经不在查询页
    this.rightGrid.gameObject:SetActive(true)
    this.searchView.gameObject:SetActive(false)
    this:showCurrentAuctionItems()
    this:setPageValue(CAuctionMgr.Inst.PageIndex + 1)
    -- 显示本周已拍出
    self.m_BenZhouYiPaiChuCount = onShelfTotalCount - onShelfCurCount
    self.m_BenZhouYiPaiChuTotal = onShelfTotalCount
    self:ShowBenZhouYiPaiChu()
end

function LuaAuctionWnd:OnPlayerRequestBetSystemResult()
    if not self.m_Wnd then return end
    local this = self.m_Wnd
    if this.showIndex ~= 2 then
        return
    end
    this:onPageValueChange(this.currentPageValue)
end

-- 请求购买的结果（GamePlay版）
-- Gac2Gas.RequestGamePlayPaiMaiBet
-- Gas2Gac.RequestGamePlayPaiMaiBetResult
function LuaAuctionWnd:OnRequestGamePlayPaiMaiBetResult()
    if not self.m_Wnd then return end
    local this = self.m_Wnd
    if this.showIndex > 2 then return end
    this:onPageValueChange(this.currentPageValue)
end

function LuaAuctionWnd:OnSyncSystemPaimaiDelay()
    if not self.m_Wnd then return end
    local this = self.m_Wnd
    if this.showIndex ~= 2 then
        return
    end
    this:onPageValueChange(this.currentPageValue)
end

-- 查询 帮会/全服拍卖 寇岛 和 横屿荡寇 页签 | 查询开始
function LuaAuctionWnd:QueryAuctionTabsCount_Start(targetType)
    if targetType == self.EnumAuctionType.Guild then
        self.m_IsGuildKouDaoEmpty = true
        self.m_IsGuildKouDaoQueryFinished = false
        self.m_IsGuildHengYuDangKouEmpty = true
        self.m_IsGuildHengYuDangKouQueryFinished = false
        self.m_GuildTable = {}
    else
        self.m_IsServerKouDaoEmpty = true
        self.m_IsServerKouDaoQueryFinished = false
        self.m_IsServerHengYuDangKouEmpty = true
        self.m_IsServerHengYuDangKouQueryFinished = false
        self.m_ServerTable = {}
    end
    if LuaAuctionMgr:IsEnableNewKouDaoAuction() then -- 寇岛拍卖启动才查询
        LuaGac2Gas.QueryGamePlayPaiMaiOnShelfCount(targetType, LuaAuctionMgr.KouDaoGamePlayId, g_MessagePack.pack(self.KouDaoItemList))
    else
        self:QueryAuctionTabsCountKouDaoFinish(targetType)
    end
    if LuaAuctionMgr:IsEnableHengYuDangKouAuction() then -- 横屿荡寇拍卖启动才查询
        LuaGac2Gas.QueryGamePlayPaiMaiOnShelfCount(targetType, LuaAuctionMgr.HengYuDangKouGamePlayId, g_MessagePack.pack(self.HengYuDangKouItemList))
    else
        self:QueryAuctionTabsCountHengYuDangKouFinish(targetType)
    end
end
-- 寇岛查询完成
function LuaAuctionWnd:QueryAuctionTabsCountKouDaoFinish(targetType)
    if targetType == self.EnumAuctionType.Guild then
        self.m_IsGuildKouDaoQueryFinished = true
        if self.m_IsGuildHengYuDangKouQueryFinished then
            self:QueryAuctionTabsCount_End(targetType)
        end
    else
        self.m_IsServerKouDaoQueryFinished = true
        if self.m_IsServerHengYuDangKouQueryFinished then
            self:QueryAuctionTabsCount_End(targetType)
        end
    end
end
-- 横屿荡寇查询完成
function LuaAuctionWnd:QueryAuctionTabsCountHengYuDangKouFinish(targetType)
    if targetType == self.EnumAuctionType.Guild then
        self.m_IsGuildHengYuDangKouQueryFinished = true
        if self.m_IsGuildKouDaoQueryFinished then
            self:QueryAuctionTabsCount_End(targetType)
        end
    else
        self.m_IsServerHengYuDangKouQueryFinished = true
        if self.m_IsServerKouDaoQueryFinished then
            self:QueryAuctionTabsCount_End(targetType)
        end
    end
end
-- 查询 帮会/全服拍卖 寇岛 和 横屿荡寇 页签 | 查询结束
function LuaAuctionWnd:QueryAuctionTabsCount_End(targetType)
    if not self.m_Wnd then return end
    local this = self.m_Wnd
    local targetindex = (targetType == self.EnumAuctionType.Guild and 0 or 1) -- 目标拍卖类型
    if this.showIndex ~= targetindex then return end -- 已经不是当前页，则不显示拍卖Tabs
    -- 查询结束后，再显示拍卖Tabs
    if targetType == self.EnumAuctionType.Guild then
        this:showGuildAuctionTabs()
    else
        this:showServerAuctionTabs()
    end
end
-- 查询不同页签上架的数量
-- Gac2Gas.QueryGamePlayPaiMaiOnShelfCount
-- Gas2Gac.SendGamePlayPaiMaiOnShelfCount
function LuaAuctionWnd:OnSendGamePlayPaiMaiOnShelfCount(targetType, playId, CountResultTbl)
    if playId == LuaAuctionMgr.KouDaoGamePlayId then
        if CountResultTbl then
            for i, v in ipairs(self.KouDaoItemList) do
                if CountResultTbl[v] and CountResultTbl[v] > 0 then
                    if targetType == self.EnumAuctionType.Guild then
                        self.m_IsGuildKouDaoEmpty = false
                    else
                        self.m_IsServerKouDaoEmpty = false
                    end
                    break
                end
            end
        end
        self:QueryAuctionTabsCountKouDaoFinish(targetType)
    else
        if CountResultTbl then
            for i, v in ipairs(self.HengYuDangKouItemList) do
                if CountResultTbl[v] and CountResultTbl[v] > 0 then
                    if targetType == self.EnumAuctionType.Guild then
                        self.m_IsGuildHengYuDangKouEmpty = false
                        table.insert(self.m_GuildTable, v)
                    else
                        self.m_IsServerHengYuDangKouEmpty = false
                        table.insert(self.m_ServerTable, v)
                    end
                end
            end
        end
        self:QueryAuctionTabsCountHengYuDangKouFinish(targetType)
    end
end

-- 手动刷新当前页签，提供给外部使用
function LuaAuctionWnd:OnRefreshClick()
    if self.m_Wnd then
        self.m_Wnd:onRefreshClick(nil)
    end
end