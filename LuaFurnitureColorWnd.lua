local EventDelegate=import "EventDelegate"
local FurnitureColorChanger=import "L10.Game.FurnitureColorChanger"
local Renderer=import "UnityEngine.Renderer"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local Color=import "UnityEngine.Color"
local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"
local UISlider=import "UISlider"
local CCurentMoneyCtrl=import "L10.UI.CCurentMoneyCtrl"
local Collider=import "UnityEngine.Collider"
local ShaderEx = import "ShaderEx"
local CEffectMgr = import "L10.Game.CEffectMgr"

CLuaFurnitureColorWnd = class()
RegistChildComponent(CLuaFurnitureColorWnd,"m_ColorSlider","Color",UISlider)
RegistChildComponent(CLuaFurnitureColorWnd,"m_SaturateSlider","Saturate",UISlider)
RegistChildComponent(CLuaFurnitureColorWnd,"m_BrightSlider","Bright",UISlider)
RegistClassMember(CLuaFurnitureColorWnd,"m_MoneyCtrl")
RegistClassMember(CLuaFurnitureColorWnd,"m_DefaultColorBlockSprite")
RegistClassMember(CLuaFurnitureColorWnd,"m_DefaultColor")
RegistClassMember(CLuaFurnitureColorWnd,"m_Cost")
RegistClassMember(CLuaFurnitureColorWnd,"m_Colors")
RegistClassMember(CLuaFurnitureColorWnd,"m_IsFxColor") -- 是否是特效变色

CLuaFurnitureColorWnd.CurFurnitureId = 0

function CLuaFurnitureColorWnd:Awake()
    local fur = CClientFurnitureMgr.Inst:GetFurniture(CLuaFurnitureColorWnd.CurFurnitureId)
    if fur then
        self.m_IsFxColor = Zhuangshiwu_ZhuangshiwuColor.GetData(fur.TemplateId).Fxcolor
    end
    self.m_IsFxColor = self.m_IsFxColor and self.m_IsFxColor or 0
    self.m_DefaultColor = {}
    self.m_MoneyCtrl = self.transform:Find("BottomRight/QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
    self.m_MoneyCtrl:SetCost(0)


    local button = self.transform:Find("BottomRight/Button").gameObject
    UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function(go)
        self:RequestSetColor()
    end)
    
    self.m_DefaultColorBlockSprite = self.transform:Find("TopLeft/DefaultColorBlock"):GetComponent(typeof(UISprite))

    local block= self.transform:Find("TopLeft/ColorBlock").gameObject
    block:SetActive(false)

    local grid = self.transform:Find("TopLeft/Grid").gameObject
    for i=1,8 do
        local g=NGUITools.AddChild(grid,block)
        g:SetActive(true)
    end
    self:RefreshColors()

    grid:GetComponent(typeof(UIGrid)):Reposition()
    

    EventDelegate.Add(self.m_ColorSlider.onChange, DelegateFactory.Callback(function () 
        self:SetColor()
    end))
    EventDelegate.Add(self.m_SaturateSlider.onChange, DelegateFactory.Callback(function () 
        self:SetColor()
    end))
    EventDelegate.Add(self.m_BrightSlider.onChange, DelegateFactory.Callback(function () 
        self:SetColor()
    end))
end

function CLuaFurnitureColorWnd:SetSliderValue(R,G,B)
    local h,s,v = rgb2hsv(R/255,G/255,B/255)
    self.m_ColorSlider.value = h
    self.m_SaturateSlider.value = s
    self.m_BrightSlider.value = v
end
function CLuaFurnitureColorWnd:GetSliderValue()
    local val1 = self.m_ColorSlider.value--self:ConvertValue(self.m_ColorSlider.value)
    local val2 = self.m_SaturateSlider.value--self:ConvertValue(self.m_SaturateSlider.value)
    local val3 = self.m_BrightSlider.value--self:ConvertValue(self.m_BrightSlider.value)
    
    local r,g,b = hsv2rgb(val1,val2,val3)
    r,g,b= math.floor(r*255),math.floor(g*255),math.floor(b*255)

    if math.abs(self.m_DefaultColor[1]-r)<=2 
    and math.abs(self.m_DefaultColor[2]-g)<=2 
    and math.abs(self.m_DefaultColor[3]-b)<=2 then
        return self.m_DefaultColor[1],self.m_DefaultColor[2],self.m_DefaultColor[3],true--is default
    else
        --上一次颜色
        local fid = CLuaFurnitureColorWnd.CurFurnitureId
        local dic = CClientHouseMgr.Inst and CClientHouseMgr.Inst.mCurFurnitureProp2.FurnitureColor
        if dic and CommonDefs.DictContains_LuaCall(dic,fid) then
            local color = dic[fid]
            if math.abs(color.R-r)<=2 
                and math.abs(color.G-g)<=2 
                and math.abs(color.B-b)<=2 then
                return color.R,color.G,color.B
            end
        end
        return r,g,b,false--not is default
    end
end

function CLuaFurnitureColorWnd:Init()
    self.m_Colors = CLuaPlayerSettings.GetFurnitureColors()


    local fid = CLuaFurnitureColorWnd.CurFurnitureId
    local fur = CClientFurnitureMgr.Inst:GetFurniture(fid)
    if fur then
        local defaultColor = Color.white
        local tf = fur.ro.m_MainObject.transform
        local t = {tf}
        for i=1,tf.childCount do
            table.insert( t,tf:GetChild(i-1) )
        end
        if self.m_IsFxColor > 0 then
            -- 特效默认颜色
            local fxid = Zhuangshiwu_Zhuangshiwu.GetData(fur.TemplateId).FX
            if Fx_FX.Exists(fxid) then
                -- 特效表有自定义颜色
                local colors = CEffectMgr.ConvertColors(Fx_FX.GetData(fxid).Colors)
                if colors and colors.Length > 0 then
                    defaultColor = colors[0]
                end
            end
        else
            -- 贴图默认颜色
            for i,v in ipairs(t) do
                local r = v:GetComponent(typeof(Renderer))
                if r and r.material and r.material.shader then
                    if ShaderEx.CheckMatIsEnabledRGBColor(r.material) then
                        defaultColor = r.material:GetColor(FurnitureColorChanger.MatProperty)
                        break
                    end
                end
            end
        end
        self.m_DefaultColor = { math.floor(defaultColor.r*255),math.floor(defaultColor.g*255),math.floor(defaultColor.b*255)}
        
        local dic = CClientHouseMgr.Inst.mCurFurnitureProp2.FurnitureColor
        if CommonDefs.DictContains_LuaCall(dic,fid) then
            local color = dic[fid]
            self:SetSliderValue(color.R,color.G,color.B)
        else
            self:SetSliderValue(math.floor(defaultColor.r*255),math.floor(defaultColor.g*255),math.floor(defaultColor.b*255))
        end

        local design = Zhuangshiwu_ZhuangshiwuColor.GetData(fur.TemplateId)
        if design then
            self.m_Cost = design.Type
            self.m_MoneyCtrl:SetCost(design.Type)
        end


        self.m_DefaultColorBlockSprite.color = defaultColor
        UIEventListener.Get(self.m_DefaultColorBlockSprite.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:SetSliderValue(self.m_DefaultColor[1],self.m_DefaultColor[2],self.m_DefaultColor[3])

            self:SetColor()
        end)
    end
end

function CLuaFurnitureColorWnd:SetColor()
    local r,g,b,isdefault = self:GetSliderValue()
    local fur = CClientFurnitureMgr.Inst:GetFurniture(CLuaFurnitureColorWnd.CurFurnitureId)
    if fur then
        if self.m_IsFxColor > 0 then
            -- 特效换色
            local colors = Table2ArrayWithCount({ Color(r / 255, g / 255, b / 255) }, 1, MakeArrayClass(Color))
            local h, s, v = rgb2hsv(r / 255, g / 255, b / 255)
            local hsvs = Table2ArrayWithCount({ Vector3(h, s, v) }, 1, MakeArrayClass(Vector3))
            fur:SetFxColor(colors, hsvs)
        else
            -- 贴图换色
            fur:SetColor(r/255,g/255,b/255)
        end
    end
    --默认颜色 cost 0
    if isdefault then
        self.m_MoneyCtrl:SetCost(0)
    else
        self.m_MoneyCtrl:SetCost(self.m_Cost)
    end
end

--保留2位有效小数
function CLuaFurnitureColorWnd:ConvertValue(v)
    return math.floor(v*1000)/1000
end

function CLuaFurnitureColorWnd:RequestSetColor()
    local r,g,b,isdefault = self:GetSliderValue()

    local fid = CLuaFurnitureColorWnd.CurFurnitureId
    local dic = CClientHouseMgr.Inst.mCurFurnitureProp2.FurnitureColor
    if CommonDefs.DictContains_LuaCall(dic,fid) then
        local color = dic[fid]
        if color.R==r and color.G==g and color.B==b then
            g_MessageMgr:ShowMessage("Furniture_Color_Not_Change")
            return
        end
    elseif isdefault then--本来就是默认颜色
        g_MessageMgr:ShowMessage("Furniture_Color_Not_Change")
        return
    end

    if isdefault then
        Gac2Gas.RequestResetFurnitureColor(CLuaFurnitureColorWnd.CurFurnitureId)
        return
    end
    if self.m_MoneyCtrl.moneyEnough then
        Gac2Gas.RequestSetFurnitureColor(CLuaFurnitureColorWnd.CurFurnitureId, r,g,b)
    else
        g_MessageMgr:ShowMessage("TianQi_Shop_Lingyu_Not_Enough")
    end
end

function CLuaFurnitureColorWnd:OnEnable()
    CClientFurnitureMgr.Inst.FurnishMode=false
    --隐藏
    CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EHouseFurnishUI, nil, true, false, false)
    g_ScriptEvent:AddListener("SyncHouseFurnitureColor",self,"OnSyncHouseFurnitureColor")
    g_ScriptEvent:AddListener("RemoveHouseFurnitureColor",self,"OnRemoveHouseFurnitureColor")
    
end

function CLuaFurnitureColorWnd:OnDisable()
    CClientFurnitureMgr.Inst.FurnishMode=true

    CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EHouseFurnishUI, nil, true, false)
    g_ScriptEvent:RemoveListener("SyncHouseFurnitureColor",self,"OnSyncHouseFurnitureColor")
    g_ScriptEvent:RemoveListener("RemoveHouseFurnitureColor",self,"OnRemoveHouseFurnitureColor")
end

function CLuaFurnitureColorWnd:OnSyncHouseFurnitureColor(fid,r,g,b)
    table.insert( self.m_Colors,1,{r,g,b} )
    CLuaPlayerSettings.SetFurnitureColors(self.m_Colors)
    self:RefreshColors()
end

function CLuaFurnitureColorWnd:OnRemoveHouseFurnitureColor(fid)
    table.insert( self.m_Colors,1,self.m_DefaultColor )
    CLuaPlayerSettings.SetFurnitureColors(self.m_Colors)
    self:RefreshColors()
end

function CLuaFurnitureColorWnd:RefreshColors()
    local colors = CLuaPlayerSettings.GetFurnitureColors()

    local grid = self.transform:Find("TopLeft/Grid").gameObject
    for i=1,grid.transform.childCount do
        local child = grid.transform:GetChild(i-1)
        local sprite = child:GetComponent(typeof(UISprite))
        if i<=#colors then
            local r,g,b = colors[i][1],colors[i][2],colors[i][3]
            sprite.color = Color(r/255, g/255, b/255)
            sprite.alpha=1
            child:GetComponent(typeof(Collider)).enabled=true
            UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                local sp = go:GetComponent(typeof(UISprite))
                local c = sp.color
                self:SetSliderValue(r,g,b)
                self:SetColor()
            end)
        else
            sprite.color = Color.black
            sprite.alpha=0.2
            child:GetComponent(typeof(Collider)).enabled=false
            UIEventListener.Get(child.gameObject).onClick = nil
        end
    end
end

function CLuaFurnitureColorWnd:OnDestroy()
    local fid = CLuaFurnitureColorWnd.CurFurnitureId
    local fur = CClientFurnitureMgr.Inst:GetFurniture(fid)
    if fur then
        local dic = CClientHouseMgr.Inst.mCurFurnitureProp2.FurnitureColor
        local r, g, b
        if CommonDefs.DictContains_LuaCall(dic, fid) then
            local color = dic[fid]
            r, g, b = color.R / 255, color.G / 255, color.B / 255
        else
            r, g, b = self.m_DefaultColor[1] / 255, self.m_DefaultColor[2] / 255, self.m_DefaultColor[3] / 255
        end
        if self.m_IsFxColor > 0 then
            -- 特效换色
            local colors = Table2ArrayWithCount({ Color(r, g, b) }, 1, MakeArrayClass(Color))
            local h, s, v = rgb2hsv(r, g, b)
            local hsvs = Table2ArrayWithCount({ Vector3(h, s, v) }, 1, MakeArrayClass(Vector3))
            fur:SetFxColor(colors, hsvs)
        else
            -- 贴图换色
            fur:SetColor(r, g, b)
        end
    end
end
