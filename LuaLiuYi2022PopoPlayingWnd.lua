local UILabel = import "UILabel"
local CScene=import "L10.Game.CScene"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Profession = import "L10.Game.Profession"

LuaLiuYi2022PopoPlayingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLiuYi2022PopoPlayingWnd, "ExpandButton", "ExpandButton", GameObject)
RegistChildComponent(LuaLiuYi2022PopoPlayingWnd, "Overview", "Overview", GameObject)
RegistChildComponent(LuaLiuYi2022PopoPlayingWnd, "Info", "Info", GameObject)
RegistChildComponent(LuaLiuYi2022PopoPlayingWnd, "RemainTimeLabel", "RemainTimeLabel", UILabel)

--@endregion RegistChildComponent end

function LuaLiuYi2022PopoPlayingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)


    --@endregion EventBind end
end

function LuaLiuYi2022PopoPlayingWnd:Init()

end

function LuaLiuYi2022PopoPlayingWnd:OnTeamInfo()
	local data = LuaDuanWu2022Mgr.teamInfo
	if data == nil then
		return
	end

	local selfId = 0
	if CClientMainPlayer.Inst then
		selfId = CClientMainPlayer.Inst.Id
	end

	for i=1,#data do
		local team = data[i]
		
		local teamItem = self.Overview.transform:Find(tostring(i))
		local count = teamItem:Find("Count"):GetComponent(typeof(UILabel))
		local name = teamItem:Find("Name"):GetComponent(typeof(UILabel))
		local icon = teamItem:Find("OwnerIcon").gameObject

		-- 阵营名
		if i == 1 then
			name.text = LocalString.GetString("柠檬糖")
		else
			name.text = LocalString.GetString("草莓卷")
		end

		local isSelfTeam = false

		for id, info in pairs(team.members) do
			if id == selfId then
				isSelfTeam = true
			end
		end

		local j = 0
		if isSelfTeam then
			for i=1, 3 do
				self.Info.transform:Find(tostring(i)).gameObject:SetActive(false)
			end

			for id, info in pairs(team.members) do
				j = j + 1
				local color = ""
				if id == selfId then
					color = "[00FF60]"
				end
				local playerItem = self.Info.transform:Find(tostring(j))
				playerItem.gameObject:SetActive(true)
				local clsSp = playerItem:Find("ClsSprite"):GetComponent(typeof(UISprite))
				local name = playerItem:Find("Name"):GetComponent(typeof(UILabel))
				local playerCount = playerItem:Find("Count"):GetComponent(typeof(UILabel))

				name.text = color.. info.name
				playerCount.text = color..info.contribution
				clsSp.spriteName = Profession.GetIconByNumber(info.class)
			end
		end

		count.text = team.score
		icon:SetActive(isSelfTeam)
	end
end

function LuaLiuYi2022PopoPlayingWnd:OnRemainTimeUpdate()
    local mainScene = CScene.MainScene
    if mainScene then
        self.RemainTimeLabel.text = self:GetRemainTimeText(mainScene.ShowTime)
    end
end

function LuaLiuYi2022PopoPlayingWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds<0 then
        totalSeconds = 0
    end
    if totalSeconds>=3600 then
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}:{2:00}[-]"),  
         math.floor(totalSeconds / 3600),
         math.floor((totalSeconds % 3600) / 60), 
         totalSeconds % 60)
    else
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}[-]"), math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaLiuYi2022PopoPlayingWnd:OnEnable()
	self:OnTeamInfo()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("FKPP_SyncTeamInfo", self, "OnTeamInfo")
	g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
end

function LuaLiuYi2022PopoPlayingWnd:OnDisable()
	LuaDuanWu2022Mgr.teamInfo = nil
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("FKPP_SyncTeamInfo", self, "OnTeamInfo")
	g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end
end

function LuaLiuYi2022PopoPlayingWnd:OnHideTopAndRightTipWnd()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end


--@region UIEvent

function LuaLiuYi2022PopoPlayingWnd:OnExpandButtonClick()
	self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

--@endregion UIEvent

