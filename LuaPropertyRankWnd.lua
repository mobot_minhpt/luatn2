local UITable = import "UITable"
local UILabel = import "UILabel"
local UIDefaultTableView = import "L10.UI.UIDefaultTableView"
local UIDefaultTableViewCell=import "L10.UI.UIDefaultTableViewCell"
local CellIndexType=import "L10.UI.UITableView+CellIndexType"
local CellIndex=import "L10.UI.UITableView+CellIndex"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"
local QnTableView = import "L10.UI.QnTableView"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CButton = import "L10.UI.CButton"
local QnButton = import "L10.UI.QnButton"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local EnumClass = import "L10.Game.EnumClass"
local Profession = import "L10.Game.Profession"
local QnTipButton = import "L10.UI.QnTipButton"
local CPlayerShopPopupMenuInfoMgr = import "L10.UI.CPlayerShopPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgrAlignType = import  "L10.UI.CPopupMenuInfoMgr+AlignType"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local PlayConfig_ServerIdEnabled = import "L10.Game.PlayConfig_ServerIdEnabled"
local CLoginMgr = import "L10.Game.CLoginMgr"
LuaPropertyRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPropertyRankWnd, "TableScrollView", "TableScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaPropertyRankWnd, "PropertyTable", "PropertyTable", UITable)
RegistChildComponent(LuaPropertyRankWnd, "RowCellTemplate", "RowCellTemplate", GameObject)
RegistChildComponent(LuaPropertyRankWnd, "SectionCellTemplate", "SectionCellTemplate", GameObject)
RegistChildComponent(LuaPropertyRankWnd, "TabScollView", "TabScollView", UITabBar)
RegistChildComponent(LuaPropertyRankWnd, "TabTemplate", "TabTemplate", GameObject)
RegistChildComponent(LuaPropertyRankWnd, "Property", "Property", UILabel)
RegistChildComponent(LuaPropertyRankWnd, "myRankItem", "myRankItem", GameObject)
RegistChildComponent(LuaPropertyRankWnd, "TableBody", "TableBody", QnTableView)
RegistChildComponent(LuaPropertyRankWnd, "SwitchButton", "SwitchButton", QnSelectableButton)
RegistChildComponent(LuaPropertyRankWnd, "ShareButton", "ShareButton", CButton)
RegistChildComponent(LuaPropertyRankWnd, "TipButton", "TipButton", QnButton)
RegistChildComponent(LuaPropertyRankWnd, "MasterView", "MasterView", UIDefaultTableView)
RegistChildComponent(LuaPropertyRankWnd, "WordFilterButton", "WordFilterButton", QnTipButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaPropertyRankWnd, "m_PropertyID")
RegistClassMember(LuaPropertyRankWnd, "m_PropertyData")
RegistClassMember(LuaPropertyRankWnd, "m_ChildTab")
RegistClassMember(LuaPropertyRankWnd, "m_SelectTabIndex")
RegistClassMember(LuaPropertyRankWnd, "m_SectionInfo")
RegistClassMember(LuaPropertyRankWnd, "m_SectionIndex")
RegistClassMember(LuaPropertyRankWnd, "m_RowSelectIndex")
RegistClassMember(LuaPropertyRankWnd, "m_HasInit")
RegistClassMember(LuaPropertyRankWnd, "m_CurIndex")	-- 查询失败的情况临时记录下
RegistClassMember(LuaPropertyRankWnd, "m_RankData")
RegistClassMember(LuaPropertyRankWnd, "m_MyRankRow")
RegistClassMember(LuaPropertyRankWnd, "m_FilterActions")
RegistClassMember(LuaPropertyRankWnd, "m_GuideCompareBtn")
RegistClassMember(LuaPropertyRankWnd, "m_ShareStatus")
RegistClassMember(LuaPropertyRankWnd, "m_IsCross")
function LuaPropertyRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)


	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)
	UIEventListener.Get(self.SwitchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSwitchButtonClick()
	end)
    --@endregion EventBind end
	self.m_PropertyID = nil
	self.m_PropertyData = nil
	self.m_ChildTab = {LocalString.GetString("全部职业")}
	self.m_RankData = nil
	self.m_SelectTabIndex = 0
	self.m_SectionInfo = nil
	self.m_SectionIndex = -1
	self.m_RowSelectIndex = -1
	self.m_HasInit = false
	self.m_CurIndex = {-1,-1}
	self.m_MyRankRow = 0
	self.m_FilterActions = nil
	self.m_GuideCompareBtn = nil

	self.m_IsCross = false
end

function LuaPropertyRankWnd:Init()
	if not LuaPlayerPropertyMgr.PropertyGuideID then return end
	self.m_IsCross = LuaPlayerPropertyMgr.IsCross
	if not self.m_HasInit then
		--self:InitTab()
		self:InitFilterButton()
		self:InitSelectBtn()
	end
	self:InitTableView()
	self:UpdateShareBtn()
	self:UpdateSwitch()
	self:IsOpenCrossServer()

	self.ShareButton.gameObject:SetActive(not self.m_IsCross)
	
end
function LuaPropertyRankWnd:InitFilterButton()
	self.m_FilterActions = {}
	for i = 1,EnumToInt(EnumClass.Undefined)-1 do
		table.insert(self.m_ChildTab,Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), i)))
	end
	for i,name in pairs(self.m_ChildTab) do
		table.insert(self.m_FilterActions,PopupMenuItemData(name, DelegateFactory.Action_int(function (index) 
			self.WordFilterButton.Text = name
			self:OnSelectIndex(index)
		end), false, nil))
	end
	UIEventListener.Get(self.WordFilterButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnWorldFilterButtonClick()
	end)
	self.WordFilterButton:SetTipStatus(true)
end
function LuaPropertyRankWnd:OnSelectIndex(index)
	self.m_MyRankRow = 0
	self:UpdateTableView(index)
end
function LuaPropertyRankWnd:OnWorldFilterButtonClick()
	self.WordFilterButton:SetTipStatus(false)
	CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenuWithdefaultSelectedIndex(Table2Array(self.m_FilterActions, MakeArrayClass(PopupMenuItemData)), self.m_SelectTabIndex, self.WordFilterButton.transform, CPopupMenuInfoMgrAlignType.Bottom, 2, nil, nil, DelegateFactory.Action(function ()
        self.WordFilterButton:SetTipStatus(true)
    end), 600,300)
end
function LuaPropertyRankWnd:InitSelectBtn()
	self.m_SectionInfo = {}
	local selectBtnCount = 0
	
	PropGuide_Rank.Foreach(function(k,v)
		local t = {}
		local rowCount = 0
		for i = 0,v.Content.Length - 1 do
			local id = v.Content[i]
			local data = PropGuide_Property.GetData(id)
			if data and data.ShowBtn == 1 then
				table.insert(t,id)
				if id == LuaPlayerPropertyMgr.PropertyGuideID then
					self.m_CurIndex[1] = selectBtnCount
					self.m_CurIndex[2] = rowCount
				end
				rowCount = rowCount + 1
			end
		end
		if t and #t > 0 then
			table.insert(self.m_SectionInfo,{name = v.TitleName, content = t})
			selectBtnCount = selectBtnCount + 1
		end
	end)
	self:InitSelectBtnTableView()
end

function LuaPropertyRankWnd:InitSelectBtnTableView()
	self.MasterView:Init(
		function () return #self.m_SectionInfo end,		-- 一级按钮个数
		function (section) return #self.m_SectionInfo[section + 1].content end,	-- 每个一级按钮下二级按钮的个数
		function (cell,index) self:InitTableViewCell(cell,index) end,			-- 每个按钮的初始化
		function (index,expanded) self:OnSectionClicked(index,expanded) end,-- 一级按钮点击事件
        function (index) self:OnRowBtnClick(index) end	-- 二级按钮点击事件
	)
	self.MasterView:LoadData(CellIndex(self.m_CurIndex[1], self.m_CurIndex[2], CellIndexType.Row), true)
end
function LuaPropertyRankWnd:InitTableViewCell(cell,index)
    local nameLabel = cell.transform:Find("Name"):GetComponent(typeof(UILabel))
	if index.type == CellIndexType.Section then
		nameLabel.text = self.m_SectionInfo[index.section + 1].name
	else
		local content = self.m_SectionInfo[index.section + 1].content
		local propid = content[index.row + 1]
		local data = PropGuide_Property.GetData(propid)
		if data then 
			nameLabel.text = data.Name
		end
	end
end
function LuaPropertyRankWnd:OnSectionClicked(index,expanded)
	local curIndex = CellIndex(self.m_SectionIndex, 0, CellIndexType.Section)
	local curSelectBtn = self.MasterView:GetCellByIndex(curIndex)
	if curSelectBtn then curSelectBtn:GetComponent(typeof(CButton)).Selected = true end
	if curSelectBtn and self.m_SectionIndex == index.section and expanded then
		local curRowBtn = self.MasterView:GetCellByIndex(CellIndex(self.m_SectionIndex, self.m_RowSelectIndex, CellIndexType.Row))
		if curRowBtn then curRowBtn:GetComponent(typeof(CButton)).Selected = true end
	end
end
function LuaPropertyRankWnd:OnRowBtnClick(index)
	if self.m_SectionIndex == index.section and self.m_RowSelectIndex == index.row then return end
	if self.m_PropertyID == self.m_SectionInfo[index.section + 1].content[index.row + 1] then return end
	self.m_CurIndex = {index.section,index.row}
	if self.m_HasInit then
		local content = self.m_SectionInfo[index.section + 1].content
		local propid = content[index.row + 1]
		if self.m_IsCross then
			-- 查跨服
			LuaPlayerPropertyMgr:QueryCrossPropRank(propid)
		else
			-- 查本服
			LuaPlayerPropertyMgr:QueryPropRank(propid)
		end
	end
end
-- function LuaPropertyRankWnd:InitTab()
-- 	Extensions.RemoveAllChildren(self.TabScollView.transform)
-- 	self.TabTemplate.gameObject:SetActive(false)
-- 	for i = 1,#self.m_ChildTab do
-- 		local Tab = CUICommonDef.AddChild(self.TabScollView.gameObject,self.TabTemplate.gameObject)
-- 		Tab.transform:Find("Label"):GetComponent(typeof(UILabel)).text = self.m_ChildTab[i]
--         Tab.gameObject:SetActive(true)
-- 	end
-- 	self.TabScollView.transform:GetComponent(typeof(UIGrid)):Reposition()
--     self.TabScollView.transform:GetComponent(typeof(CUIRestrictScrollView)):ResetPosition()
-- 	self.TabScollView.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
-- 		self:OnTabChange(index)
-- 	end)
-- 	self.TabScollView:ChangeTab(self.m_SelectTabIndex,false)
-- end

-- function LuaPropertyRankWnd:OnTabChange(index)
-- 	-- 排行榜查询服务器压力大，Tab分类先不做
-- end
function LuaPropertyRankWnd:InitTableView()
	self.myRankItem.gameObject:SetActive(false)
	self.m_MyRankRow = 0
    local data = PropGuide_Property.GetData(LuaPlayerPropertyMgr.PropertyGuideID)
    if not data then return end
	self.m_GuideCompareBtn = nil
	self:UpdateBtnSelect()
    self.m_PropertyID = LuaPlayerPropertyMgr.PropertyGuideID
	self.m_PropertyData = data
	self.Property.text = data.Name
	self:InitRankData()
	local classId = self.m_SelectTabIndex
	if not self.m_HasInit then
		if CClientMainPlayer.Inst then classId = EnumToInt(CClientMainPlayer.Inst.Class)
		else classId = 0 end
		self.m_HasInit = true
	end
	self:UpdateTableView(classId)
	self.WordFilterButton.Text = self.m_ChildTab[classId + 1]
	self.m_HasInit = true
end
function LuaPropertyRankWnd:UpdateBtnSelect()
	local needUpdate = false
	if self.m_PropertyID == LuaPlayerPropertyMgr.PropertyGuideID then 
		self.m_CurIndex[1] = self.m_SectionIndex
		self.m_CurIndex[2] = self.m_RowSelectIndex
		return
	elseif (self.m_CurIndex[1] == self.m_SectionIndex and self.m_CurIndex[2] == self.m_RowSelectIndex)
	or self.m_PropertyID ~= self.m_SectionInfo[self.m_CurIndex[1] + 1].content[self.m_CurIndex[2] + 1]  then
		local selectBtnCount = 0
		needUpdate = true
		PropGuide_Rank.Foreach(function(k,v)
			local t = {}
			local rowCount = 0
			for i = 0,v.Content.Length - 1 do
				local id = v.Content[i]
				local data = PropGuide_Property.GetData(id)
				if data and data.ShowBtn == 1 then
					table.insert(t,id)
					if id == LuaPlayerPropertyMgr.PropertyGuideID then
						self.m_CurIndex[1] = selectBtnCount
						self.m_CurIndex[2] = rowCount
					end
					rowCount = rowCount + 1
				end
			end
			if t and #t > 0 then
				selectBtnCount = selectBtnCount + 1
			end
		end)
	end
	local curSelectBtn = self.MasterView:GetCellByIndex(CellIndex(self.m_SectionIndex, 0, CellIndexType.Section))
	local curRowBtn = self.MasterView:GetCellByIndex(CellIndex(self.m_SectionIndex, self.m_RowSelectIndex, CellIndexType.Row))
	if curSelectBtn then curSelectBtn:GetComponent(typeof(CButton)).Selected = false  end
	if curRowBtn then curRowBtn:GetComponent(typeof(CButton)).Selected = false end
	

	local selectBtn = self.MasterView:GetCellByIndex(CellIndex(self.m_CurIndex[1], 0, CellIndexType.Section))
	local rowBtn = self.MasterView:GetCellByIndex(CellIndex(self.m_CurIndex[1], self.m_CurIndex[2], CellIndexType.Row))
	if selectBtn then selectBtn:GetComponent(typeof(CButton)).Selected = true end
	if rowBtn then rowBtn:GetComponent(typeof(CButton)).Selected = true end
	
	self.m_SectionIndex = self.m_CurIndex[1]
	self.m_RowSelectIndex = self.m_CurIndex[2]
	if needUpdate then
		self.MasterView:SetSectionSelected(CellIndex(self.m_SectionIndex, 0, CellIndexType.Section))
	end
end
function LuaPropertyRankWnd:UpdateMyRankView()
	if not CClientMainPlayer.Inst then return end
	self.myRankItem.gameObject:SetActive(true)
	if self.m_MyRankRow ~= 0 then
		self:InitItemGo(self.myRankItem,self.m_MyRankRow,true)
	else
		local rank = self.myRankItem.transform:Find("Table/rank"):GetComponent(typeof(UILabel))
		local rankSprite = rank.transform:Find("rankImage"):GetComponent(typeof(UISprite))
		rank.text = LocalString.GetString("未上榜")
		rankSprite.gameObject:SetActive(false)
		self.myRankItem.transform:Find("Table/nameclass/name"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.RealName
		self.myRankItem.transform:Find("Table/nameclass/class"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), (EnumToInt(CClientMainPlayer.Inst.Class))))
		self.myRankItem.transform:Find("Table/server"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst:GetMyServerName()
		local value = self.myRankItem.transform:Find("Table/value"):GetComponent(typeof(UILabel))
		value.text = self:GetPropertyValue()
	end
end

function LuaPropertyRankWnd:GetPropertyValue()
	if not CClientMainPlayer.Inst or not self.m_PropertyData then return  "" end
	local fightPropId = self.m_PropertyData.FightPropID
	local type = CommonDefs.ConvertIntToEnum(typeof(EPlayerFightProp), fightPropId)
	if fightPropId and fightPropId > 0 then
		local val = CClientMainPlayer.Inst.FightProp:GetParam(type)
		if type == EPlayerFightProp.mSpeed or type == EPlayerFightProp.pSpeed then    -- 攻速
			if val < 1E-06 then val = 1 end
			return System.String.Format("{0:F2}",val)
		elseif type == EPlayerFightProp.pFatalDamage or type == EPlayerFightProp.mFatalDamage
		or type == EPlayerFightProp.AntipFatalDamage or type == EPlayerFightProp.AntimFatalDamage then
			return System.String.Format("{0}%",tostring(NumberTruncate(val, 3) * 100))
		elseif type == EPlayerFightProp.MF then
			return System.String.Format(tostring(NumberTruncate(val, 3)))
		else
			return tostring((math.floor(val)))
		end
	else
		return ""
	end
end
function LuaPropertyRankWnd:InitRankData()
	if not LuaPlayerPropertyMgr.PropRankData then return end
	local totalRankData = LuaPlayerPropertyMgr.PropRankData
	table.sort(totalRankData, function(a,b) 
		return a.rank < b.rank 
	end)
	local rankData = {}
	for i,name in pairs(self.m_ChildTab) do
		rankData[i] = {}
	end
	for i = 1,#totalRankData do
		local class = totalRankData[i].class
		class = tonumber(class)
		local classData = totalRankData[i]
		classData.classRank = #rankData[class] + 1
		table.insert(rankData[class],classData)
	end
	rankData[0] = totalRankData
	self.m_RankData = rankData
end
function LuaPropertyRankWnd:UpdateTableView(tabIndex)
	self.m_SelectTabIndex = tabIndex
	self.TableBody.m_DataSource = DefaultTableViewDataSource.Create2(
        function()
            if self.m_RankData[self.m_SelectTabIndex] then
                return #self.m_RankData[self.m_SelectTabIndex]
            end
            return 0
        end,
        function(view,row)
            local item = nil
            item = view:GetFromPool(0)
            self:InitItemGo(item,row+1,false)
            return item
        end
    )
	self.TableBody.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local data = self.m_RankData[self.m_SelectTabIndex][row+1]
        if data then
			LuaPlayerPropertyMgr:ShowPlayerPopupMenu(self.m_PropertyID,data)
        end
    end)
    self.TableBody:ReloadData(true,true)
	self:UpdateMyRankView()
	self.TableBody.transform:GetComponent(typeof(UIScrollView)):ResetPosition()
end

function LuaPropertyRankWnd:InitItemGo(itemGo,row,isMy)
	if not self.m_RankData[self.m_SelectTabIndex] or row == 0 then return end
	local data = self.m_RankData[self.m_SelectTabIndex][row]
	if not data then return end
	local rankData = data.rank
	if self.m_SelectTabIndex ~= 0 then rankData = data.classRank end
	local item = itemGo:GetComponent(typeof(QnButton))
	if not isMy then
		item:SetBackgroundTexture(rankData % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
	end
	local rank = itemGo.transform:Find("Table/rank"):GetComponent(typeof(UILabel))
	local rankSprite = rank.transform:Find("rankImage"):GetComponent(typeof(UISprite))
	rank.text = rankData
	rankSprite.gameObject:SetActive(false)
	if rankData == 1 then
		rank.text = ""
		rankSprite.gameObject:SetActive(true)
		rank.transform:Find("rankImage"):GetComponent(typeof(UISprite)).spriteName = "Rank_No.1"
	elseif rankData == 2 then
		rank.text = ""
		rankSprite.gameObject:SetActive(true)
		rank.transform:Find("rankImage"):GetComponent(typeof(UISprite)).spriteName = "Rank_No.2png"
	elseif rankData == 3 then
		rank.text = ""
		rankSprite.gameObject:SetActive(true)
		rank.transform:Find("rankImage"):GetComponent(typeof(UISprite)).spriteName = "Rank_No.3png"
	end
	itemGo.transform:Find("Table/nameclass/name"):GetComponent(typeof(UILabel)).text = data.name
	itemGo.transform:Find("Table/nameclass/class"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), data.class))
	itemGo.transform:Find("Table/server"):GetComponent(typeof(UILabel)).text = data.serverName
	itemGo.transform:Find("Table/value"):GetComponent(typeof(UILabel)).text = math.floor(data.score)
	local campareBtn = itemGo.transform:Find("Table/campareBtn")
	if  campareBtn and CClientMainPlayer.Inst and data.id == CClientMainPlayer.Inst.Id then
		self.m_MyRankRow = row
		campareBtn.gameObject:SetActive(false)
	elseif campareBtn then
		campareBtn.gameObject:SetActive(true)
		if not self.m_GuideCompareBtn then
			self.m_GuideCompareBtn = campareBtn
		end
		if not LuaPlayerPropertyMgr.PlayerIsShareProp or ( CClientMainPlayer.Inst and (EnumToInt(CClientMainPlayer.Inst.Class) ~= tonumber(data.class))) then
			campareBtn:GetComponent(typeof(UIWidget)).alpha = 0.3
		else
			campareBtn:GetComponent(typeof(UIWidget)).alpha = 1
		end
		UIEventListener.Get(campareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			if CClientMainPlayer.Inst and (EnumToInt(CClientMainPlayer.Inst.Class) ~= tonumber(data.class)) then
				g_MessageMgr:ShowMessage("PROPGUIDE_CLASS_NOT_MATCH")
			elseif not LuaPlayerPropertyMgr.PlayerIsShareProp then
				g_MessageMgr:ShowMessage("PROPGUIDE_SELF_NOT_SHARED",self.m_PropertyData.Name)
			else
				LuaPlayerPropertyMgr:ShowPlayerPropertyCompareWnd(self.m_PropertyID,data)
			end
		end)
	end
	itemGo.transform:Find("Table"):GetComponent(typeof(UITable)):Reposition()
end
function LuaPropertyRankWnd:UpdateShareBtn()
	if self.m_PropertyID ~= LuaPlayerPropertyMgr.PropertyGuideID then return end
	self.m_ShareStatus = LuaPlayerPropertyMgr.PlayerIsShareProp
	local sprite = self.ShareButton.transform:GetComponent(typeof(UISprite))
	local label = self.ShareButton.transform:Find("Label"):GetComponent(typeof(UILabel))
    if LuaPlayerPropertyMgr.PlayerIsShareProp then
        sprite.spriteName = "common_btn_01_blue"
		label.text = LocalString.GetString("取消分享该数据")
		label.color = NGUIText.ParseColor("0E3254", 0)
    else
        sprite.spriteName = "common_btn_01_yellow"
		label.text = LocalString.GetString("分享该数据")
		label.color = NGUIText.ParseColor("361F01", 0)
    end
end

function LuaPropertyRankWnd:GetGuideGo(methodName)
	if methodName == "GetCompareBtn" then
		if self.m_GuideCompareBtn then return self.m_GuideCompareBtn.gameObject end
		return nil
	end
end

function LuaPropertyRankWnd:OnSwitchButtonClick()
	local data = PropGuide_Property.GetData(self.m_PropertyID)
    if not data then return end
    local type = CommonDefs.ConvertIntToEnum(typeof(EPlayerFightProp), data.FightPropID)
    local typeString = CommonDefs.Convert_ToString(type)
    if self.m_IsCross then
        -- 查本服
        LuaPlayerPropertyMgr:QueryPropRank(self.m_PropertyID)
    else
        -- 查跨服
        LuaPlayerPropertyMgr:QueryCrossPropRank(self.m_PropertyID)
    end
end

function LuaPropertyRankWnd:UpdateSwitch()
	local sprite = self.SwitchButton.transform:GetComponent(typeof(UISprite))
    if not self.m_IsCross then
        sprite.spriteName = "common_thumb_open"
    else
        sprite.spriteName = "common_thumb_close"
    end
end
function LuaPropertyRankWnd:IsOpenCrossServer()
	self.SwitchButton.gameObject:SetActive(LuaPlayerPropertyMgr.IsOpenCrossServer and LuaPlayerPropertyMgr.PropertyOpenCross)
end
function LuaPropertyRankWnd:OnEnable()
	g_ScriptEvent:AddListener("OnPlayerQueryPropShareResult", self, "UpdateShareBtn")
	g_ScriptEvent:AddListener("UpdatePropertyRankData", self, "InitTableView")
	g_ScriptEvent:AddListener("OnPropGuideIsOpenCrossServer", self, "IsOpenCrossServer")
end
function LuaPropertyRankWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnPlayerQueryPropShareResult", self, "UpdateShareBtn")
	g_ScriptEvent:RemoveListener("UpdatePropertyRankData", self, "InitTableView")
	g_ScriptEvent:RemoveListener("OnPropGuideIsOpenCrossServer", self, "IsOpenCrossServer")
	LuaPlayerPropertyMgr.queryRank = false
end
--@region UIEvent

function LuaPropertyRankWnd:OnShareButtonClick()
	if not self.m_PropertyID then return end
    local data = PropGuide_Property.GetData(self.m_PropertyID)
    if not data then return end
	LuaPlayerPropertyMgr.TemplatePropertyGuideID = self.m_PropertyID
    local type = CommonDefs.ConvertIntToEnum(typeof(EPlayerFightProp), data.FightPropID)
    local typeString = CommonDefs.Convert_ToString(type)
    if self.m_ShareStatus then
        -- 取消分享
		LuaPlayerPropertyMgr.TemplatePropertyGuideID = self.m_PropertyID
        Gac2Gas.PlayerCancelSharePropComposition(typeString)
		--Gac2Gas.PlayerQueryPropShareRecord(typeString) 
    else
        -- 请求分享
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("PropGuide_ShareProperty_Confim",data.Name), function()
			LuaPlayerPropertyMgr.TemplatePropertyGuideID = self.m_PropertyID
            Gac2Gas.PlayerRequestSharePropComposition(typeString,0)
            --Gac2Gas.PlayerQueryPropShareRecord(typeString) 
        end, nil, nil, nil, false)
    end
end

function LuaPropertyRankWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("PropertyGuide_Rank_Tip")
end

--@endregion UIEvent

