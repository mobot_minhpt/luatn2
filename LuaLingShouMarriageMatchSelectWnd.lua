require("3rdParty/ScriptEvent")
require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local LuaUtils=import "LuaUtils"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local CLingShouBaseMgr=import "L10.Game.CLingShouBaseMgr"
local EnumLingShouGender=import "L10.Game.EnumLingShouGender"

CLuaLingShouMarriageMatchSelectWnd=class()
RegistClassMember(CLuaLingShouMarriageMatchSelectWnd,"m_Title")
RegistClassMember(CLuaLingShouMarriageMatchSelectWnd,"m_Template")
RegistClassMember(CLuaLingShouMarriageMatchSelectWnd,"m_Node")

function CLuaLingShouMarriageMatchSelectWnd:Init()
    self.m_Title=LuaGameObject.GetChildNoGC(self.transform,"TitleLabel").label
    
    self.m_Template=LuaGameObject.GetChildNoGC(self.transform,"Template").gameObject
    self.m_Template:SetActive(false)
    self.m_Node=LuaGameObject.GetChildNoGC(self.transform,"Node").transform
    LuaUtils.SetLocalPosition(self.m_Node,540,0,0)

    self.m_Title.text=LocalString.GetString("我的灵兽")

    --请求所有的灵兽
    CLingShouMgr.Inst:RequestLingShouList()
end
function CLuaLingShouMarriageMatchSelectWnd:OnEnable()
    g_ScriptEvent:AddListener("GetAllLingShouOverview", self, "OnGetAllLingShouOverview")
end
function CLuaLingShouMarriageMatchSelectWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GetAllLingShouOverview", self, "OnGetAllLingShouOverview")
end
function CLuaLingShouMarriageMatchSelectWnd:OnGetAllLingShouOverview()
    local function onClick(go)
        self:OnClickItem(go)
	end
    --实例化列表
    local grid=LuaGameObject.GetChildNoGC(self.transform,"Grid").grid
    local list=CLingShouMgr.Inst:GetLingShouOverviewList()
    local len=list.Count
    for i=1,len do
        local item=list[i-1]
        local go=NGUITools.AddChild(grid.gameObject,self.m_Template)
        go:SetActive(true)
        LuaGameObject.GetChildNoGC(go.transform,"NameLabel").label.text=item.name
        LuaGameObject.GetChildNoGC(go.transform,"LevelLabel").label.text=tostring(item.level)
        local icon=CLingShouBaseMgr.GetLingShouIcon(item.templateId)
        LuaGameObject.GetChildNoGC(go.transform,"Icon").cTexture:LoadNPCPortrait(icon)

        local genderLabel=LuaGameObject.GetChildNoGC(go.transform,"DescLabel").label
        if item.gender==EnumLingShouGender.Male then
            genderLabel.text=LocalString.GetString("阳")
        elseif item.gender==EnumLingShouGender.Female then
            genderLabel.text=LocalString.GetString("阴")
        elseif item.gender==EnumLingShouGender.Double then
            genderLabel.text=LocalString.GetString("阴阳")
        else
            genderLabel.text=""
        end

        CommonDefs.AddOnClickListener(go,DelegateFactory.Action_GameObject(onClick),false)
    end
    grid:Reposition()
end

function CLuaLingShouMarriageMatchSelectWnd:OnClickItem(go)
    CUIManager.CloseUI(CUIResources.ItemListWnd)
    --发消息给上个界面
    local index=go.transform:GetSiblingIndex()
    local list=CLingShouMgr.Inst:GetLingShouOverviewList()
    local item=list[index]
    g_ScriptEvent:BroadcastInLua("LingShouMarriageSelectDone",item.templateId,item.id)
end

return CLuaLingShouMarriageMatchSelectWnd