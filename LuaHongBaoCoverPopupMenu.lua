local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CButton = import "L10.UI.CButton"
LuaHongBaoCoverPopupMenu = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHongBaoCoverPopupMenu, "ImageButton", "ImageButton", GameObject)
RegistChildComponent(LuaHongBaoCoverPopupMenu, "Background", "Background", GameObject)
RegistChildComponent(LuaHongBaoCoverPopupMenu, "ButtonsTable", "ButtonsTable", GameObject)

--@endregion RegistChildComponent end

function LuaHongBaoCoverPopupMenu:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaHongBaoCoverPopupMenu:Init()
    if not CLuaHongBaoMgr.m_PopupMenuData then return end
    local worldPos = CLuaHongBaoMgr.m_PopupMenuData.worldPos
    local localPos=self.transform:InverseTransformPoint(worldPos)
    LuaUtils.SetLocalPosition(FindChild(self.transform,"Anchor"),localPos.x,localPos.y-280,0)
    self.ImageButton.gameObject:SetActive(false)

    self:AddBtns()
end

function LuaHongBaoCoverPopupMenu:AddBtns()
    Extensions.RemoveAllChildren(self.ButtonsTable.transform)
    local BtnList = CLuaHongBaoMgr.m_PopupMenuData.ButtonList
    local padding_Y = self.ButtonsTable:GetComponent(typeof(UITable)).padding.y
    local cellHeight = self.ImageButton:GetComponent(typeof(UISprite)).height
    self.Background:GetComponent(typeof(UISprite)).height = math.min(500,(cellHeight + padding_Y) * #BtnList + 25)

    for i=1,#BtnList do
        local BtnObj = CUICommonDef.AddChild(self.ButtonsTable.gameObject,self.ImageButton.gameObject)
        BtnObj.transform:Find("Texture").gameObject:GetComponent(typeof(CUITexture)):LoadMaterial(BtnList[i].texturePath)
        BtnObj.transform:Find("Label").gameObject:GetComponent(typeof(UILabel)).text = BtnList[i].name
        BtnObj.transform:Find("Des").gameObject:GetComponent(typeof(UILabel)).text = BtnList[i].describe
        local SelectSprite = BtnObj.transform:Find("Select").gameObject
        if CLuaHongBaoMgr.m_PopupMenuData.selectIndex and i == CLuaHongBaoMgr.m_PopupMenuData.selectIndex then
            SelectSprite:SetActive(true)
        else
            SelectSprite:SetActive(false)
        end
        BtnObj.gameObject:SetActive(true)
        CommonDefs.AddOnClickListener(BtnObj.gameObject, DelegateFactory.Action_GameObject(function (go)
            self:OnClick(go,i)
        end), false)
    end
    self.ButtonsTable:GetComponent(typeof(UITable)):Reposition()
end

function LuaHongBaoCoverPopupMenu:OnClick(go,index)
    CUIManager.CloseUI(CLuaUIResources.HongBaoCoverPopupMenu)
    CLuaHongBaoMgr.OnClickPopupMenuBtn(index)
end
--@region UIEvent

--@endregion UIEvent

