require("common/common_include")
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CWorldEventMgr = import "L10.Game.CWorldEventMgr"

LuaWorldEventMgr = {}

LuaWorldEventMgr.HorShowPic = nil
LuaWorldEventMgr.HorShowTime = 0
LuaWorldEventMgr.VerShowPic = nil
LuaWorldEventMgr.DrawLineTaskId = nil

--LuaWorldEventMgr.MatchTable = {
--[1]= {1,2},
--[2]= {2,3},
--[3]= {3,4},
--[4]= {4,5},
--[5]= {5,1},
--[6]= {1,7},
--[7]= {7,6},
--[8]= {6,1}}
LuaWorldEventMgr.MatchTable = {
[1]= {1,2},
[2]= {2,3},
[3]= {3,4},
[4]= {4,1},
}

LuaWorldEventMgr.DrawFinishFx = "fx/ui/prefab/UI_yibihua_hudie02.prefab"
LuaWorldEventMgr.DrawMoveNodeFx = "fx/ui/prefab/UI_yibihua01.prefab"
LuaWorldEventMgr.SaveFormerNode = nil
LuaWorldEventMgr.SaveLinkedTable = {}

LuaWorldEventMgr.MatchFucntion = function(a,b)
  for i,v in pairs(LuaWorldEventMgr.MatchTable) do
    if (v[1] == a and v[2] == b) or (v[1] == b and v[2] == a) then
      return i
    end
  end
  return 0
end

LuaWorldEventMgr.EventTimeTable = {}

--
LuaWorldEventMgr.PintuDis = 168

LuaWorldEventMgr.PintuTaskId = 0
LuaWorldEventMgr.PintuTime = 0

function Gas2Gac.OpenUnlockWithKeyWnd(taskId, countdown)
  LuaWorldEventMgr.PintuTaskId = taskId
  LuaWorldEventMgr.PintuTime = countdown
  CUIManager.ShowUI(CUIResources.WorldEventPintuWnd)
end

function Gas2Gac.CloseUnlockWithKeyWnd(taskId)
  CUIManager.CloseUI(CUIResources.WorldEventPintuWnd)
end

function Gas2Gac.OpenLanRuoSiShareWnd()
  CUIManager.ShowUI(CLuaUIResources.WorldEventShareWnd)
end

function Gas2Gac.ShareLanRuoSiSuccess()
  CUIManager.CloseUI(CLuaUIResources.WorldEventShareWnd)
end

function Gas2Gac.SyncViewedMessageIdx(messageIdx,messageTbl)
  CWorldEventMgr.Inst.worldReadMessageIdx = messageIdx
  LuaWorldEventMgr.EventTimeTable = {}
  local list = MsgPackImpl.unpack(messageTbl)
	if not list then return end
	for i = 0, list.Count - 1, 2 do
    LuaWorldEventMgr.EventTimeTable[list[i]] = list[i+1]
    --local info = {}
    --info.id = list[i]
    --info.time = list[i+1]
    --table.insert(LuaWorldEventMgr.EventTimeTable,info)
	end
  CUIManager.ShowUI(CUIResources.WorldEventHisWnd)
end
