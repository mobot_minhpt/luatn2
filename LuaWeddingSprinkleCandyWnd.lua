local GameObject      = import "UnityEngine.GameObject"
local DelegateFactory = import "DelegateFactory"

LuaWeddingSprinkleCandyWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWeddingSprinkleCandyWnd, "CandyButton", "CandyButton", GameObject)
--@endregion RegistChildComponent end

function LuaWeddingSprinkleCandyWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.CandyButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCandyButtonClick()
	end)

    --@endregion EventBind end
end

function LuaWeddingSprinkleCandyWnd:Init()
end

--@region UIEvent

function LuaWeddingSprinkleCandyWnd:OnCandyButtonClick()
	LuaWeddingIterationMgr:OpenCandySelectWnd()
end

--@endregion UIEvent

