require("common/common_include")

local MessageMgr = import "L10.Game.MessageMgr"
local CButton = import "L10.UI.CButton"
local QnCheckBox = import "L10.UI.QnCheckBox"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local QnTableView = import "L10.UI.QnTableView"
local UIGrid = import "UIGrid"
local RelationshipType = import "L10.Game.RelationshipType"
local GroupInfo = import "L10.UI.CFriendContactListView+GroupInfo"
local CIMMgr = import "L10.Game.CIMMgr"
local FriendItemData = import "FriendItemData"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local CUITexture = import "L10.UI.CUITexture"
local Constants = import "L10.Game.Constants"
local CWeddingMgr = import "L10.Game.CWeddingMgr"
local CShiTuMgr = import "L10.Game.CShiTuMgr"
local CJieBaiMgr = import "L10.Game.CJieBaiMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Object = import "System.Object"

LuaFengXianInviteWnd = class()

-- select friend
RegistChildComponent(LuaFengXianInviteWnd, "FriendTypeSelectBtn", CButton)
RegistChildComponent(LuaFengXianInviteWnd, "Arrow", GameObject)
RegistChildComponent(LuaFengXianInviteWnd, "OnlineCheckBox", QnCheckBox)

-- friends
RegistChildComponent(LuaFengXianInviteWnd, "TableView", QnTableView)

-- selected
RegistChildComponent(LuaFengXianInviteWnd, "InviteBtn", CButton)
RegistChildComponent(LuaFengXianInviteWnd, "InvitedGrid", UIGrid)
RegistChildComponent(LuaFengXianInviteWnd, "InivitedTemplate", GameObject)

RegistClassMember(LuaFengXianInviteWnd, "TableViewDataSource")
RegistClassMember(LuaFengXianInviteWnd, "FriendGroupList") --list
RegistClassMember(LuaFengXianInviteWnd, "SelectedGroupIndex")

RegistClassMember(LuaFengXianInviteWnd, "AllData") -- 所有好友信息
RegistClassMember(LuaFengXianInviteWnd,"TableViewDataSource")

RegistClassMember(LuaFengXianInviteWnd, "SelectedPlayerIds") -- playerId, FriendItemData

function LuaFengXianInviteWnd:Init()
	self.InivitedTemplate:SetActive(false)
	self.AllData = CreateFromClass(MakeGenericClass(List, FriendItemData))

	self.OnlineCheckBox:SetSelected(false, true)
	self:InitGroup()
	self.SelectedGroupIndex = 0
	self.SelectedPlayerIds = {}

	local function initItem(item,index)
    	self:InitFriendItem(item, index)
    end

	self.TableViewDataSource = DefaultTableViewDataSource.Create(function ()
		return self.AllData.Count
	end, initItem)
	self.TableView.m_DataSource = self.TableViewDataSource
	self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)

	self.OnlineCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (value)
		self:OnOnlineCheckBoxClicked(value)
	end)

	CommonDefs.AddOnClickListener(self.FriendTypeSelectBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
		self:OnFriendTypeSelectBtnClicked(go)
	end), false)

	CommonDefs.AddOnClickListener(self.InviteBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
		self:OnInviteBtnClicked(go)
	end), false)

	self:UpdateFengXianInvited()
	self:OnGroupSelected(0)
end

-- 初始化好友类型
function LuaFengXianInviteWnd:InitGroup()
	self.FriendGroupList = CreateFromClass(MakeGenericClass(List, GroupInfo))
	for i = 1, CIMMgr.MAX_FRIEND_GROUP_NUM do
		local groupName = CIMMgr.Inst:GetFriendGroupName(i)
		local info = CreateFromClass(GroupInfo, i, groupName, RelationshipType.Friend)
		CommonDefs.ListAdd(self.FriendGroupList, typeof(GroupInfo), info)
	end
end

-- 点击展开选择好友类型的tip
function LuaFengXianInviteWnd:OnFriendTypeSelectBtnClicked(go)

	local menuList = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
	for i = 0, self.FriendGroupList.Count-1 do
		CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(self.FriendGroupList[i].groupName, DelegateFactory.Action_int(function (idx) 
        	self:OnGroupSelected(idx)
    	end), false, nil, EnumPopupMenuItemStyle.Light))
	end
	CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(menuList), go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, nil, 350, true, 330)
	
end

-- 选中好友类型
function LuaFengXianInviteWnd:OnGroupSelected(index)
	for i = 0, self.FriendGroupList.Count do
		if i == index then
			self.FriendTypeSelectBtn.Text = self.FriendGroupList[i].groupName
			self.SelectedGroupIndex = index
			self:LoadContactList(self.FriendGroupList[i])
		end
	end
end

-- 请求邀请好友
function LuaFengXianInviteWnd:OnInviteBtnClicked(go)

	if self:GetSelectedCount() > 3 then
		MessageMgr.Inst:ShowMessage("Feng_Xian_Invite_Max", {})
		return
	end

	local selectedList = CreateFromClass(MakeGenericClass(List, Object))
	for k, v in pairs(self.SelectedPlayerIds) do
		CommonDefs.ListAdd(selectedList, typeof(UInt64), k)
	end
	Gac2Gas.RequestInviteWatchFengXian(MsgPackImpl.pack(selectedList))
end

-- 选择是否在线的过滤
function LuaFengXianInviteWnd:OnOnlineCheckBoxClicked(value)
	self:LoadContactList(self.FriendGroupList[self.SelectedGroupIndex])
end

-- 更新好友列表<param: groupInfo>
function LuaFengXianInviteWnd:LoadContactList(groupInfo)
	CommonDefs.ListClear(self.AllData)
	self.TableView:Clear()

	if  CClientMainPlayer.Inst then

		if groupInfo.type == RelationshipType.Friend then
			local friends = CIMMgr.Inst:GetFriendsByGroup(groupInfo.groupId)
			for i = 0, friends.Count-1 do
				local playerId = friends[i]
				local friendData = CreateFromClass(FriendItemData, CIMMgr.Inst:GetBasicInfo(playerId))
				-- 是否在线的过滤
				if self.OnlineCheckBox.Selected and friendData.isOnline then
					CommonDefs.ListAdd_LuaCall(self.AllData, friendData)
				elseif not self.OnlineCheckBox.Selected then
					CommonDefs.ListAdd_LuaCall(self.AllData, friendData)
				end
				
			end
		end
	end
	self.TableView:ReloadData(true, false)
end

function LuaFengXianInviteWnd:InitFriendItem(item, index)
	local data = self.AllData[index]

	local PlayerPortrait = item.transform:Find("PlayerPortrait"):GetComponent(typeof(CUITexture))
	local LevelLabel = item.transform:Find("PlayerPortrait/LevelLabel"):GetComponent(typeof(UILabel))
	local PlayerNameLabel = item.transform:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
	local OnlineLabel = item.transform:Find("OnlineLabel"):GetComponent(typeof(UILabel))
	local FriendlinessLabel = item.transform:Find("FriendlinessLabel"):GetComponent(typeof(UILabel))
	local QnCheckBox = item.transform:Find("QnCheckBox"):GetComponent(typeof(QnCheckBox))
	QnCheckBox:SetSelected(self:IsPlayerAlreadySelected(data.playerId), true)

	PlayerPortrait:LoadNPCPortrait(data.portraitName, false)
	local playerInfo = self:GetInfoText(data.playerId, true)
	if not System.String.IsNullOrEmpty(playerInfo) then
		PlayerNameLabel.text = SafeStringFormat3("[EE7038]%s[-]%s", data.name, playerInfo)
	else
		PlayerNameLabel.text = SafeStringFormat3("[EE7038]%s[-]", data.name)
	end
	
	if data.isInXianShenStatus then
		LevelLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]%s[-][/c]"), Constants.ColorOfFeiSheng, tostring(data.level))
	else
		LevelLabel.text = SafeStringFormat3(LocalString.GetString("[c]%s[/c]"), tostring(data.level))
	end
	
	if data.isOnline then
		OnlineLabel.text = LocalString.GetString("在线")
	else
		local offlineTime = CIMMgr.Inst:GetFriendOfflineTime(data.playerId)
		if offlineTime ~= 0 then
			local offlineData = CServerTimeMgr.ConvertTimeStampToZone8Time(offlineTime)
			OnlineLabel.text = SafeStringFormat3(LocalString.GetString("[B2B2B2]离线 %s[-]"), offlineData:ToString("yyyy-MM-dd"))
		else
			OnlineLabel.text = nil
		end
		
	end
	FriendlinessLabel.text = SafeStringFormat3(LocalString.GetString("友好度:%s"), tostring(data.friendliness))

	QnCheckBox.Enabled = not self:IsPlayerAlreadyInivited(data.playerId)
	QnCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (value)
		self:OnAddOrRemoveInvitePlayer(value, data, QnCheckBox)
	end)
end

-- 玩家是否已经被邀请
function LuaFengXianInviteWnd:IsPlayerAlreadyInivited(playerId)
	if not CLuaXianzhiMgr.m_FengXianInvitedIds then return false end

	for i = 1, #CLuaXianzhiMgr.m_FengXianInvitedIds do
		if CLuaXianzhiMgr.m_FengXianInvitedIds[i] == playerId then
			return true
		end
	end
	return false
end

-- 玩家是否被选中
function LuaFengXianInviteWnd:IsPlayerAlreadySelected(playerId)
	return self.SelectedPlayerIds[playerId] ~= nil
end

-- 邀请/取消邀请某个玩家
function LuaFengXianInviteWnd:OnAddOrRemoveInvitePlayer(value, data, qnCheckBox)
	if value then
		-- 选中
		if self:GetSelectedCount() >= 3 then
			MessageMgr.Inst:ShowMessage("Feng_Xian_Invite_Max", {})
			qnCheckBox:SetSelected(false, true)
			return
		end
		self.SelectedPlayerIds[data.playerId] = data
	else
		-- 取消
		self.SelectedPlayerIds[data.playerId] = nil
	end

	self:UpdateInvited()
end

-- 更新已邀请列表
function LuaFengXianInviteWnd:UpdateInvited()

	CUICommonDef.ClearTransform(self.InvitedGrid.transform)
	for k, v in pairs(self.SelectedPlayerIds) do
		local item = NGUITools.AddChild(self.InvitedGrid.gameObject, self.InivitedTemplate)
		self:InitInvitedTemplate(item, v)
		item:SetActive(true)
	end
	self.InvitedGrid:Reposition()

	-- 更新按钮
	local count = self:GetSelectedCount()
	self.InviteBtn.Text = SafeStringFormat3(LocalString.GetString("邀请(%s/3)"), tostring(count))

end

-- 获得已经邀请的玩家数量
function LuaFengXianInviteWnd:GetSelectedCount()
	local playerCount = 0
	for k, v in pairs(self.SelectedPlayerIds) do
		if v then
 			playerCount = playerCount + 1
 		end
	end
	return playerCount
end

function LuaFengXianInviteWnd:OnSelectAtRow(row)
	
end

function LuaFengXianInviteWnd:InitInvitedTemplate(go, data)
	local PlayerPortrait = go.transform:Find("PlayerPortrait"):GetComponent(typeof(CUITexture))
	local LevelLabel = go.transform:Find("PlayerPortrait/LevelLabel"):GetComponent(typeof(UILabel))
	local PlayerNameLabel = go.transform:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
	local RelationshipLabel = go.transform:Find("RelationshipLabel"):GetComponent(typeof(UILabel))
	local StatusSprite = go.transform:Find("StatusSprite").gameObject
	StatusSprite:SetActive(false)

	PlayerPortrait:LoadNPCPortrait(data.portraitName, false)
	PlayerNameLabel.text = SafeStringFormat3("[EE7038]%s[-]", data.name)
	if data.isInXianShenStatus then
		LevelLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]%s[-][/c]"), Constants.ColorOfFeiSheng, tostring(data.level))
	else
		LevelLabel.text = SafeStringFormat3(LocalString.GetString("[c]%s[/c]"), tostring(data.level))
	end
	RelationshipLabel.text = self:GetInfoText(data.playerId, false)
	StatusSprite:SetActive(self:IsPlayerAlreadyInivited(data.playerId))
end

-- 获得玩家与主角关系的名称
function LuaFengXianInviteWnd:GetInfoText(playerId, needBracket)
	if CWeddingMgr.Inst:IsMyHusband(playerId) then
		if needBracket then
			return LocalString.GetString("[FFAEF2](夫君)[-]")
		else
			return LocalString.GetString("[FFAEF2]夫君[-]")
		end
		
	elseif CWeddingMgr.Inst:IsMyWife(playerId) then
		if needBracket then
			return LocalString.GetString("[FFAEF2](娘子)[-]")
		else
			return LocalString.GetString("[FFAEF2]娘子[-]")
		end
	elseif CShiTuMgr.Inst:IsCurrentShiFu(playerId) then
		if needBracket then
			return LocalString.GetString("(师父)")
		else
			return LocalString.GetString("师父")
		end
		
	elseif CShiTuMgr.Inst:IsCurrentTuDi(playerId) then
		if needBracket then
			return LocalString.GetString("(徒弟)")
		else
			return LocalString.GetString("徒弟")
		end
		
	else
		local text = CJieBaiMgr.Inst:GetExistJieBaiRelationByID(playerId, false)
		if not System.String.IsNullOrEmpty(text) then
			if needBracket then
				return SafeStringFormat3("(%s)", text)
			else
				return text
			end
		end		
	end
	return nil
end

-- 更新已经被邀请的玩家信息
function LuaFengXianInviteWnd:UpdateFengXianInvited()
	self.SelectedPlayerIds = {}
	for i = 1, #CLuaXianzhiMgr.m_FengXianInvitedIds do
		local playerId = CLuaXianzhiMgr.m_FengXianInvitedIds[i]
		local friendData = CreateFromClass(FriendItemData, CIMMgr.Inst:GetBasicInfo(playerId))
		if friendData then
			self.SelectedPlayerIds[playerId] = friendData
		end
	end
	self:UpdateInvited()
	self:LoadContactList(self.FriendGroupList[self.SelectedGroupIndex])
end

function LuaFengXianInviteWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateFengXianInvited", self, "UpdateFengXianInvited")
end

function LuaFengXianInviteWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateFengXianInvited", self, "UpdateFengXianInvited")
end

return LuaFengXianInviteWnd
