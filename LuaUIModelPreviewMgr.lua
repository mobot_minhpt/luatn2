local GestureRecognizer = import "L10.Engine.GestureRecognizer"
local GestureType = import "L10.Engine.GestureType"

if rawget(_G, "LuaUIModelPreviewMgr") then
    LuaUIModelPreviewMgr:RemoveListener()
end

LuaUIModelPreviewMgr = {}
LuaUIModelPreviewMgr.ModelScaleList = nil
LuaUIModelPreviewMgr.onPinchInDelegate = nil
LuaUIModelPreviewMgr.onPinchOutDelegate = nil

function LuaUIModelPreviewMgr:RemoveListener()
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchIn, self.onPinchInDelegate)
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchOut, self.onPinchOutDelegate)
    g_ScriptEvent:RemoveListener("MouseScrollWheel",self,"OnMouseScrollWheel")
end

function LuaUIModelPreviewMgr:AddListener()
    self.onPinchInDelegate = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchIn(gesture) end)
    self.onPinchOutDelegate = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchOut(gesture) end)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchIn, self.onPinchInDelegate)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchOut, self.onPinchOutDelegate)
    g_ScriptEvent:AddListener("MouseScrollWheel",self,"OnMouseScrollWheel")
end
LuaUIModelPreviewMgr:AddListener()


function LuaUIModelPreviewMgr:AddModelPreviewScaleTransformListener(identifier,defaultPosition,defaultScale,maxPreviewScale,maxpreviewPosition,uiModel,onlyShowTopWnd)
    if self.ModelScaleList == nil then self.ModelScaleList = {} end
    GestureRecognizer.Inst:AddNeedUIPinchWnd(uiModel)
    local modelPreview = {}
    modelPreview.identifier = identifier
    modelPreview.defaultPosition = defaultPosition
    modelPreview.defaultScale = defaultScale
    modelPreview.localModelScale = defaultScale
    modelPreview.maxPreviewScale = maxPreviewScale
    modelPreview.maxPreviewPosition = maxpreviewPosition
    modelPreview.uiModel = uiModel
    modelPreview.onlyShowTopWnd = onlyShowTopWnd
    self.ModelScaleList[identifier] = modelPreview
    self:SetModelPosAndScale(identifier,defaultPosition,defaultScale)
end


function LuaUIModelPreviewMgr:RemoveModelPreviewScaleTransformListener(identifier)
    if not self.ModelScaleList then return end
    self.ModelScaleList[identifier] = nil
end


function LuaUIModelPreviewMgr:OnMouseScrollWheel()
    local v = Input.GetAxis("Mouse ScrollWheel")
    self:OnPinch(v)
end

function LuaUIModelPreviewMgr:OnPinchIn(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0),1)
    self:OnPinch(-pinchScale)
end

function LuaUIModelPreviewMgr:OnPinchOut(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0),1)
    self:OnPinch(pinchScale)
end

function LuaUIModelPreviewMgr:OnPinch(delta)
    if not self.ModelScaleList then return end
    for k,v in pairs(self.ModelScaleList) do
        local uiModel = v.uiModel
        if v.onlyShowTopWnd and CUIManager.instance:GetTopPopUI() == uiModel or not v.onlyShowTopWnd then
            local maxScale = v.maxPreviewScale
            local defaulePos = v.defaultPosition
            local maxpos = v.maxPreviewPosition
            local localScale = v.localModelScale
            local d = (maxScale) - 1
            local t = (localScale - 1) /  d
            t = t + delta
            t = math.max(math.min(1,t),0)
            local s = math.lerp(1,maxScale,t)
            local x = math.lerp(defaulePos.x,maxpos.x,t)
            local y = math.lerp(defaulePos.y,maxpos.y,t) 
            local z = math.lerp(defaulePos.z,maxpos.z,t)
            local pos = Vector3(x, y, z)
            self:SetModelPosAndScale(k, pos, s)
        end
    end
end

function LuaUIModelPreviewMgr:SetModelPosAndScale(identifier,pos,scale)
    if not self.ModelScaleList then return end
    local modelPreview = self.ModelScaleList[identifier]
    if not modelPreview then return end
    self.ModelScaleList[identifier].localModelScale = scale
    local pos = Vector3(pos.x , pos.y , pos.z )
    CUIManager.SetModelPosition(identifier, pos)
end