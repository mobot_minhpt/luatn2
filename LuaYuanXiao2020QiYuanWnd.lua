local CButton = import "L10.UI.CButton"
local CIMMgr = import "L10.Game.CIMMgr"
local UIInput = import "UIInput"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local Constants = import "L10.Game.Constants"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Money = import "L10.Game.Money"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"

LuaYuanXiao2020QiYuanWnd = class()

RegistChildComponent(LuaYuanXiao2020QiYuanWnd,"m_Grid","Grid", UIGrid)
RegistChildComponent(LuaYuanXiao2020QiYuanWnd,"m_SelectBtn","SelectBtn", CButton)
RegistChildComponent(LuaYuanXiao2020QiYuanWnd,"m_NameLabel","NameLabel", UILabel)
RegistChildComponent(LuaYuanXiao2020QiYuanWnd,"m_BuyBtn","BuyBtn", CButton)
RegistChildComponent(LuaYuanXiao2020QiYuanWnd,"m_StatusText","StatusText", UIInput)

RegistClassMember(LuaYuanXiao2020QiYuanWnd, "m_SelectedTargetPlayerId")
RegistClassMember(LuaYuanXiao2020QiYuanWnd, "m_SelectedTargetFangDengIndex")
RegistClassMember(LuaYuanXiao2020QiYuanWnd, "m_FangDengList")

function LuaYuanXiao2020QiYuanWnd:Init()
    UIEventListener.Get(self.m_SelectBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        LuaYuanXiao2020Mgr:ShowPlayListWnd()
    end)
    UIEventListener.Get(self.m_BuyBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:OnBuyButtonClick()
    end)

    self.m_FangDengList = {}
    for i = 1,3 do
        local go = self.m_Grid.transform:GetChild(i - 1).gameObject
        self:InitFangDeng(go, i)

        go:SetActive(true)
    end
    self.m_Grid:Reposition()
    self:SelectFangDeng()
end

function LuaYuanXiao2020QiYuanWnd:OnEnable()
    g_ScriptEvent:AddListener("YuanXiao2020_SelectPlayer", self, "OnSelectPlayer")
end

function LuaYuanXiao2020QiYuanWnd:OnDisable()
    g_ScriptEvent:RemoveListener("YuanXiao2020_SelectPlayer", self, "OnSelectPlayer")
end

function LuaYuanXiao2020QiYuanWnd:InitFangDeng(go, index)
    local t = {}
    t.Bg = go.transform:Find("Bg"):GetComponent(typeof(CUITexture))
    t.NameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    t.MoneyIcon = go.transform:Find("MoneyIcon"):GetComponent(typeof(UISprite))
    t.PriceLabel = go.transform:Find("MoneyIcon/PriceLabel"):GetComponent(typeof(UILabel))
    t.Selected = go.transform:Find("Selected"):GetComponent(typeof(UISprite))
    t.Index = index
    local lanternType = YuanXiao_LanternType.GetData(t.Index)
    t.NameLabel.text = lanternType.Name
    t.MoneyIcon.spriteName = Money.GetIconName((lanternType.PriceType == 1) and EnumMoneyType.LingYu or EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE)
    t.PriceLabel.text = lanternType.Price
    UIEventListener.Get(t.Bg.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:SelectFangDeng(t)
    end)
    t.isJade = lanternType.PriceType == 1
    table.insert(self.m_FangDengList,t)
    return t
end


function LuaYuanXiao2020QiYuanWnd:SelectFangDeng(t)
    self.m_SelectedTargetFangDengIndex = (t == nil) and 1 or t.Index
    for i = 1,3 do
        self.m_FangDengList[i].Selected.enabled = (self.m_SelectedTargetFangDengIndex == self.m_FangDengList[i].Index)
    end
end

function LuaYuanXiao2020QiYuanWnd:OnSelectPlayer(playerId)
    self:ShowNameLabel(playerId)
end

function LuaYuanXiao2020QiYuanWnd:ShowNameLabel(playerId)

    local player = CClientMainPlayer.Inst
    if not player then
        return
    end

    if playerId == nil and self.m_SelectedTargetPlayerId == nil then
        self.m_NameLabel.alpha = 0.3
        self.m_NameLabel.text = LocalString.GetString("尚未添加对象")
        return
    end

    self.m_NameLabel.alpha = 1.0

    if playerId == player.Id then
        self.m_NameLabel.text = player.Name
    else
        local basicInfo = CIMMgr.Inst:GetBasicInfo(playerId)
        if basicInfo then
            self.m_NameLabel.text = basicInfo.Name
        end
    end
    self.m_SelectedTargetPlayerId = playerId
end

function LuaYuanXiao2020QiYuanWnd:CheckMoneyEnough()
    local player = CClientMainPlayer.Inst
    if not player then
        return false
    end
    local needJade = tonumber(self.m_FangDengList[self.m_SelectedTargetFangDengIndex].PriceLabel.text)
    local isJade = self.m_FangDengList[self.m_SelectedTargetFangDengIndex].isJade
    if isJade and (not CShopMallMgr.TryCheckEnoughJade(needJade, 0)) then
        return false
    elseif not isJade then
        if needJade > player.Silver then
            local txt = g_MessageMgr:FormatMessage("NotEnough_Silver_QuickBuy")
            MessageWndManager.ShowOKCancelMessage(txt, DelegateFactory.Action(function ()
                CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
            end), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
            return false
        end
    end
    return true
end

function LuaYuanXiao2020QiYuanWnd:OnBuyButtonClick()

    if not self.m_SelectedTargetPlayerId then
        g_MessageMgr:ShowMessage("YuanXiao2020_FangDeng_Not_Select_Target")
        return
    end

    local wishMsg = CUICommonDef.Trim(self.m_StatusText.value)
    if wishMsg == "" then
        g_MessageMgr:ShowMessage("YuanXiao2020_FangDeng_Not_Input_Wish")
        return
    end

    local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(wishMsg, false)
    if ret.msg == nil or ret.shouldBeIgnore then
        g_MessageMgr:ShowMessage("YuanXiao2020_Speech_Violation")
        return
    end

    if not self:CheckMoneyEnough() then
        return
    end

    local isJade = self.m_FangDengList[self.m_SelectedTargetFangDengIndex].isJade
    local selectTargetName = self.m_NameLabel.text
    local needJade = tonumber(self.m_FangDengList[self.m_SelectedTargetFangDengIndex].PriceLabel.text)
    --local fangdengName = self.m_FangDengList[self.m_SelectedTargetFangDengIndex].NameLabel.text
    local message = g_MessageMgr:FormatMessage("YuanXiao2020_FangDeng_Confirm",
            needJade,isJade and LocalString.GetString("灵玉") or LocalString.GetString("银两"), selectTargetName)
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
        LuaYuanXiao2020Mgr:RequestFangDeng(self.m_SelectedTargetPlayerId, selectTargetName, self.m_SelectedTargetFangDengIndex, wishMsg)

    end), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
end

