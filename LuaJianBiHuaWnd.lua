local Object=import "System.Object"
local Input = import "UnityEngine.Input"
local Color=import "UnityEngine.Color"
local TweenColor=import "TweenColor"
local UIWidget=import "UIWidget"
--简笔画

CLuaJianBiHuaWnd=class()

CLuaJianBiHuaWnd.m_Key = nil
RegistClassMember(CLuaJianBiHuaWnd,"m_DragObject")
RegistClassMember(CLuaJianBiHuaWnd,"m_BlinkTick")
RegistClassMember(CLuaJianBiHuaWnd,"m_CloseTick")
RegistClassMember(CLuaJianBiHuaWnd,"m_ShowTick")
RegistClassMember(CLuaJianBiHuaWnd,"m_ShowFxTick")

function CLuaJianBiHuaWnd:Init()
    self.transform:Find("Anchor").gameObject:SetActive(true)
    self.transform:Find("Result").gameObject:SetActive(false)
    self.m_DragObject = self.transform:Find("DragObject").gameObject

    local yu = self.transform:Find("Anchor/Yu").gameObject
    local ying = self.transform:Find("Anchor/Ying").gameObject
    local shu = self.transform:Find("Anchor/Shu").gameObject
    local dunpai = self.transform:Find("Anchor/DunPai").gameObject

    if CLuaJianBiHuaWnd.m_Key == "JianBiHua1" then
        yu:SetActive(true)
        shu:SetActive(false)
        dunpai:SetActive(false)
        ying:SetActive(false)
        self:Setup(yu.transform)
    elseif CLuaJianBiHuaWnd.m_Key == "JianBiHua2" then
        yu:SetActive(false)
        shu:SetActive(true)
        dunpai:SetActive(false)
        ying:SetActive(false)
        self:Setup(shu.transform)
    elseif CLuaJianBiHuaWnd.m_Key == "JianBiHua3" then
        yu:SetActive(false)
        shu:SetActive(false)
        dunpai:SetActive(true)
        ying:SetActive(false)
        self:Setup(dunpai.transform)
    elseif CLuaJianBiHuaWnd.m_Key == "JianBiHua4" then
        yu:SetActive(false)
        shu:SetActive(false)
        dunpai:SetActive(false)
        ying:SetActive(true)
        self:Setup(ying.transform)
    end


end

function CLuaJianBiHuaWnd:Blink(widget)
    widget:GetComponent(typeof(TweenColor)).enabled = true

    -- if self.m_BlinkTick then
    --     UnRegisterTick(self.m_BlinkTick)
    --     self.m_BlinkTick = nil
    -- end
    -- LuaTweenUtils.TweenColor(widget.transform,0.5,0.2,0.3)
    -- LuaTweenUtils.SetDelay(LuaTweenUtils.TweenColor(widget.transform,0.2,0.5,0.3),0.3)

    -- self.m_BlinkTick = RegisterTick(function()
    --     LuaTweenUtils.TweenColor(widget.transform,0.5,0.2,0.5)
    --     LuaTweenUtils.SetDelay(LuaTweenUtils.TweenColor(widget.transform,0.2,0.5,0.5),0.5)
    -- end,600)
end
function CLuaJianBiHuaWnd:Unblink(targets)
    for j,k in ipairs(targets) do
        k:GetComponent(typeof(TweenColor)).enabled = false
    end
end

function CLuaJianBiHuaWnd:Setup(tf)
    -- local finish = tf:Find("finish").gameObject
    -- finish:SetActive(false)

    local targets = {}
    local sucai = tf:Find("sucai")
    local fills = {}
    for i=1,sucai.childCount do
        fills[i] = false
        local child = sucai:Find(tostring(i)).gameObject
        targets[i] = tf:Find(tostring(i)):GetComponent(typeof(UIWidget))
        targets[i].alpha = 0.2
        UIEventListener.Get(child).onPress = DelegateFactory.BoolDelegate(function(g,b)
            -- print(g,b)
            self:Unblink(targets)

            if b then
                for j,k in ipairs(targets) do
                    if j==i then
                        --闪烁
                        self:Blink(k)
                    else
                        k.alpha = fills[j] and 1 or 0.2
                    end
                end

                self.m_DragObject:SetActive(true)
                local uiTex = self.m_DragObject:GetComponent(typeof(UITexture))
                local targetTex = child:GetComponent(typeof(UITexture))
                uiTex.transform.rotation = child.transform.rotation
                uiTex.flip = targetTex.flip
                uiTex.material = targetTex.material
                uiTex.width = targetTex.width
                uiTex.height = targetTex.height

                g:GetComponent(typeof(UIWidget)).enabled = false
            else

                self.m_DragObject:SetActive(false)


                --判断一下是否拖到了指定位置，
                local success = false
                local lastPos = UICamera.lastWorldPosition
                for j=1,sucai.childCount do
                    -- print(j,i)
                    local widget = targets[j]:GetComponent(typeof(UIWidget))
                    if targets[j].transform.childCount>0 then--有子节点 按子节点的大小
                        widget = targets[j].transform:GetChild(0):GetComponent(typeof(UIWidget))
                    end
                    local corners = widget.worldCorners
                    if self:IsPointInMatrix(corners[0],corners[1],corners[2],corners[3],lastPos) then
                        if j==i then
                            success = true
                            fills[i] = true
                            break
                        end
                    end
                end

                g:GetComponent(typeof(UIWidget)).enabled = not success

                for j,k in ipairs(targets) do
                    -- k.alpha=fills[j] and 1 or 0.2
                    k.color = fills[j] and Color.white or Color(1,1,1,0.2)
                end

                local result = true
                for i,v in ipairs(fills) do
                    if v==false then
                        result = false
                    end
                end
                if result then
                    -- print("place over")
                    self:OnEnd()
                end
            end
        end)
    end
end

-- //判断点p是否在p1p2p3p4的正方形内
function CLuaJianBiHuaWnd:IsPointInMatrix(p1, p2, p3, p4, p)
    local function GetCross(p1,p2,p)
        return (p2.x - p1.x) * (p.y - p1.y) - (p.x - p1.x) * (p2.y - p1.y)
    end
    local isPointIn = GetCross(p1, p2, p) * GetCross(p3, p4, p) >= 0 and GetCross(p2, p3, p) * GetCross(p4, p1, p) >= 0
    return isPointIn
end

function CLuaJianBiHuaWnd:Update()
    if self.m_DragObject.activeSelf then
        self.m_DragObject.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
    end
end

function CLuaJianBiHuaWnd:OnEnd()
    self.transform:Find("Anchor").gameObject:SetActive(false)
    local result = self.transform:Find("Result")
    result.gameObject:SetActive(true)
    
    local activeIdx = 0
    if CLuaJianBiHuaWnd.m_Key == "JianBiHua1" then
        activeIdx = 1
    elseif CLuaJianBiHuaWnd.m_Key == "JianBiHua2" then
        activeIdx = 2
    elseif CLuaJianBiHuaWnd.m_Key == "JianBiHua3" then
        activeIdx = 3
    elseif CLuaJianBiHuaWnd.m_Key == "JianBiHua4" then
        activeIdx = 4
    end
    local childTf = nil
    for i=1,result.childCount do
        if i==activeIdx then
            result:GetChild(i-1).gameObject:SetActive(true)
            childTf = result:GetChild(i-1)
        else
            result:GetChild(i-1).gameObject:SetActive(false)
        end
    end



    LuaTweenUtils.TweenAlpha(result,0,1,0.3)
    LuaTweenUtils.SetDelay(LuaTweenUtils.TweenPosition(result,20,0,0,1),0.3)

    if childTf then
        local tf1 = childTf:Find("ok")
        tf1.gameObject:SetActive(false)
        local tf2 = childTf:Find("raw")
        tf2.gameObject:SetActive(true)

        --做特效，特效结束，显示最终图形
        self.m_ShowTick = RegisterTickOnce(function()
            local tf1 = childTf:Find("ok")
            tf1.gameObject:SetActive(true)
            LuaTweenUtils.TweenAlpha(tf1,0,1,0.5)

            local tf2 = childTf:Find("raw")
            LuaTweenUtils.TweenAlpha(tf2,1,0,0.5)
        end,2000)
        self.m_ShowFxTick = RegisterTickOnce(function()
            result:Find("Fx").gameObject:SetActive(true)
        end,1000)
    end


    local taskId = 0
    if CLuaJianBiHuaWnd.m_Key == "JianBiHua1" then
        taskId = 22203820
    elseif CLuaJianBiHuaWnd.m_Key == "JianBiHua2" then
        taskId = 22203825
    elseif CLuaJianBiHuaWnd.m_Key == "JianBiHua3" then
        taskId = 22203840
    elseif CLuaJianBiHuaWnd.m_Key == "JianBiHua4" then
        taskId = 22203842
    end
    

    self.m_CloseTick = RegisterTickOnce(function()
        CUIManager.CloseUI(CLuaUIResources.JianBiHuaWnd)
        local empty = CreateFromClass(MakeGenericClass(List, Object))
        Gac2Gas.FinishClientTaskEvent(taskId,CLuaJianBiHuaWnd.m_Key,MsgPackImpl.pack(empty))
    end,4500)
end

function CLuaJianBiHuaWnd:OnDestroy()
    if self.m_BlinkTick then
        UnRegisterTick(self.m_BlinkTick)
        self.m_BlinkTick = nil
    end
    if self.m_CloseTick then
        UnRegisterTick(self.m_CloseTick)
        self.m_CloseTick = nil
    end
    if self.m_ShowTick then
        UnRegisterTick(self.m_ShowTick)
        self.m_ShowTick = nil
    end
    if self.m_ShowFxTick then
        UnRegisterTick(self.m_ShowFxTick)
        self.m_ShowFxTick = nil
    end
end