require("common/common_include")
require("design/Equipment/ShenBing")
local UIEventListener = import "UIEventListener"
local LuaUtils = import "LuaUtils"
local NGUITools = import "NGUITools"
local UITable = import "UITable"
local UILabel = import "UILabel"
local NGUIText = import "NGUIText"
local CUITexture = import "L10.UI.CUITexture"
local Color = import "UnityEngine.Color"

CLuaShenbingColorChooseWnd = class()

RegistClassMember(CLuaShenbingColorChooseWnd, "m_ColorTemplateObj")
RegistClassMember(CLuaShenbingColorChooseWnd, "m_ColorTable")
RegistClassMember(CLuaShenbingColorChooseWnd, "m_TitleLabel")
RegistClassMember(CLuaShenbingColorChooseWnd, "m_EmptyObj")

RegistClassMember(CLuaShenbingColorChooseWnd, "m_ColorObj2IdTable")

function CLuaShenbingColorChooseWnd:Awake( ... )
	self.m_ColorTemplateObj = self.transform:Find("Anchor/Offset/Colors/ScrollView/Color").gameObject
	self.m_ColorTemplateObj:SetActive(false)
	self.m_TitleLabel = self.transform:Find("Anchor/Offset/Background/TitleLabel"):GetComponent(typeof(UILabel))
	self.m_EmptyObj = self.transform:Find("Anchor/Offset/Background/EmptyLabel").gameObject
	self.m_EmptyObj:SetActive(false)
	self.m_ColorTable = self.transform:Find("Anchor/Offset/Colors/ScrollView/Table"):GetComponent(typeof(UITable))
end

function CLuaShenbingColorChooseWnd:Init( ... )
	UIEventListener.Get(self.transform:Find("Anchor/Offset/ComposeBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		CUIManager.ShowUI(CLuaUIResources.ShenBingColorComposeWnd)
		CUIManager.CloseUI(CLuaUIResources.ShenBingColorChooseWnd)
	end)
	local titleStr = LocalString.GetString("基础系染料")
	if CLuaShenbingColorationWnd.m_ColorType == 2 then
		titleStr = LocalString.GetString("明暗系染料")
	elseif CLuaShenbingColorationWnd.m_ColorType == 3 then
		titleStr = LocalString.GetString("饱和系染料")
	end
	self.m_TitleLabel.text = titleStr

	local cnt = 0
	self.m_ColorObj2IdTable = {}

	-- empty color
	local emptyObj = NGUITools.AddChild(self.m_ColorTable.gameObject, self.m_ColorTemplateObj)
	emptyObj:SetActive(true)
	emptyObj.transform:Find("Selected").gameObject:SetActive(false)
	emptyObj.transform:Find("Count").gameObject:SetActive(false)
	emptyObj.transform:Find("Empty").gameObject:SetActive(true)
	self.m_ColorObj2IdTable[emptyObj] = 0
	UIEventListener.Get(emptyObj).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:SelectColor(...)
	end)

	local existColTbl = {}
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp then
		CommonDefs.DictIterate(CClientMainPlayer.Inst.ItemProp.ShenBingRanLiao, DelegateFactory.Action_object_object(function (k, v)
			if ShenBing_Coloration.GetData(k).Type == CLuaShenbingColorationWnd.m_ColorType and v > 0 then
				existColTbl[k] = 1
				self:InitItem(k, v)
				cnt = cnt + 1
			end
		end))
	end

	-- all color can be previewed
	--if CLuaShenbingColorationWnd.m_ColorType == 1 then
		for i = 1, ShenBing_Coloration.GetDataCount() do
			if ShenBing_Coloration.GetData(i).Type == CLuaShenbingColorationWnd.m_ColorType and not existColTbl[i] then
				self:InitItem(i, 0)
			end
		end
	--else
	--	self.m_EmptyObj:SetActive(cnt <= 0)
	--end

	self.m_ColorTable:Reposition()
end

function CLuaShenbingColorChooseWnd:InitItem(k, cnt)
	local obj = NGUITools.AddChild(self.m_ColorTable.gameObject, self.m_ColorTemplateObj)
	obj:SetActive(true)
	UIEventListener.Get(obj).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:SelectColor(...)
	end)
	obj.transform:Find("Selected").gameObject:SetActive(false)
	obj.transform:Find("Count").gameObject:SetActive(true)
	obj.transform:Find("Empty").gameObject:SetActive(false)
	local colTex = obj:GetComponent(typeof(CUITexture))
	if ShenBing_Coloration.GetData(k).Type == 1 then
		colTex:LoadMaterial(LuaShenbingMgr.BasicColorTexPath)
		colTex.color = NGUIText.ParseColor(ShenBing_Coloration.GetData(k).RGB, 0)
	elseif ShenBing_Coloration.GetData(k).Type == 2 then
		if ShenBing_Coloration.GetData(k).RGB == "1" then
			colTex:LoadMaterial(LuaShenbingMgr.ValueIncColorTexPath)
		else
			colTex:LoadMaterial(LuaShenbingMgr.ValueDecColorTexPath)
		end
	elseif ShenBing_Coloration.GetData(k).Type == 3 then
		if ShenBing_Coloration.GetData(k).RGB == "1" then
			colTex:LoadMaterial(LuaShenbingMgr.SaturationIncColorTexPath)
		else
			colTex:LoadMaterial(LuaShenbingMgr.SaturationDecColorTexPath)
		end
	end

	local countLabel = obj.transform:Find("Count"):GetComponent(typeof(UILabel))
	countLabel.text = cnt
	if cnt <= 0 then countLabel.color = Color.red end
	self.m_ColorObj2IdTable[obj] = k
end

function CLuaShenbingColorChooseWnd:SelectColor(go)
	if not self.m_ColorObj2IdTable[go] then
		return
	end
	g_ScriptEvent:BroadcastInLua("ShenbingColorSelected", self.m_ColorObj2IdTable[go])
	CUIManager.CloseUI(CLuaUIResources.ShenBingColorChooseWnd)
end

return CLuaShenbingColorChooseWnd
