local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local QnTableView = import "L10.UI.QnTableView"
local QnTableItem = import "L10.UI.QnTableItem"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CUITexture = import "L10.UI.CUITexture"
local Animation = import "UnityEngine.Animation"
local BoxCollider = import "UnityEngine.BoxCollider"
local ParticleSystem = import "UnityEngine.ParticleSystem"

LuaCompetitionHonorFameHallWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaCompetitionHonorFameHallWnd, "Anchor", GameObject)
RegistChildComponent(LuaCompetitionHonorFameHallWnd, "MoreBtn", GameObject)
RegistChildComponent(LuaCompetitionHonorFameHallWnd, "BackBtn", GameObject)
RegistChildComponent(LuaCompetitionHonorFameHallWnd, "TipBtn", GameObject)
RegistChildComponent(LuaCompetitionHonorFameHallWnd, "ScoreLabel", UILabel)
RegistChildComponent(LuaCompetitionHonorFameHallWnd, "RankModel", CCommonLuaScript)
RegistChildComponent(LuaCompetitionHonorFameHallWnd, "RankView", QnTableView)
RegistChildComponent(LuaCompetitionHonorFameHallWnd, "HonorView", GameObject)
RegistChildComponent(LuaCompetitionHonorFameHallWnd, "HonorGrid", UIGrid)
RegistChildComponent(LuaCompetitionHonorFameHallWnd, "HonorTemplate", GameObject)
RegistChildComponent(LuaCompetitionHonorFameHallWnd, "AllHonorGrid", UIGrid)
RegistChildComponent(LuaCompetitionHonorFameHallWnd, "AllHonorTemplate", GameObject)
RegistChildComponent(LuaCompetitionHonorFameHallWnd, "HonorPage", GameObject)
RegistChildComponent(LuaCompetitionHonorFameHallWnd, "PreBtn", GameObject)
RegistChildComponent(LuaCompetitionHonorFameHallWnd, "NextBtn", GameObject)
RegistChildComponent(LuaCompetitionHonorFameHallWnd, "PageLabel", UILabel)

RegistClassMember(LuaCompetitionHonorFameHallWnd, "m_Animation")
RegistClassMember(LuaCompetitionHonorFameHallWnd, "m_FakeModelSwitchFx")
RegistClassMember(LuaCompetitionHonorFameHallWnd, "m_HonorChangePageAnim")
RegistClassMember(LuaCompetitionHonorFameHallWnd, "m_RankData")
RegistClassMember(LuaCompetitionHonorFameHallWnd, "m_RankItemList")
RegistClassMember(LuaCompetitionHonorFameHallWnd, "m_SelectIndex")
RegistClassMember(LuaCompetitionHonorFameHallWnd, "m_HonorData")
RegistClassMember(LuaCompetitionHonorFameHallWnd, "m_HonorCurPage")
RegistClassMember(LuaCompetitionHonorFameHallWnd, "m_HonorTotalPage")
RegistClassMember(LuaCompetitionHonorFameHallWnd, "m_IsChangeModel")     -- 之后是否需要播放切换角色的动效
RegistClassMember(LuaCompetitionHonorFameHallWnd, "m_ShowAllHonor")     -- 是否显示所有荣誉
--@endregion RegistChildComponent end

function LuaCompetitionHonorFameHallWnd:Awake()
    self.m_Animation = self.transform:GetComponent(typeof(Animation))
    self.m_FakeModelSwitchFx = self.transform:Find("Anchor/RankModel/FakeModel/CUIFx/switchfx"):GetComponent(typeof(ParticleSystem))
    self.m_HonorChangePageAnim = self.HonorView:GetComponent(typeof(Animation))
    UIEventListener.Get(self.MoreBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnMoreBtnClick()
    end)
    UIEventListener.Get(self.BackBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnBackBtnClick()
    end)
    UIEventListener.Get(self.TipBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnTipBtnClick()
    end)
    UIEventListener.Get(self.PreBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnPageBtnClick(-1)
    end)
    UIEventListener.Get(self.NextBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnPageBtnClick(1)
    end)
end

function LuaCompetitionHonorFameHallWnd:Init()
    self.m_IsChangeModel = false
    self.m_ShowAllHonorBtnCount = 3
    self.m_OnePageHonorCount = 9
    self.m_HonorData = {}
    self.m_HonorTotalPage = 0
    self.HonorTemplate.gameObject:SetActive(false)
    self.AllHonorTemplate.gameObject:SetActive(false)
    self:InitShowData()
    self:InitRank()
    self.m_Animation:Play("competitionhonor_show")
    self:SetView(false)
    self:OnPlayerQueryCompetitionHonorRankReturn()
end

function LuaCompetitionHonorFameHallWnd:InitShowData()
    self.m_HonorShowData = {}
    for i = 1, CompetitionHonor_GameShowInfo.GetDataCount() do
        table.insert(self.m_HonorShowData, CompetitionHonor_GameShowInfo.GetData(i))
    end
end

function LuaCompetitionHonorFameHallWnd:SetView(showAllHonor)
    self.m_ShowAllHonor = showAllHonor
    self.RankView.gameObject:SetActive(not showAllHonor)
    self.HonorView.gameObject:SetActive(showAllHonor)
    self.MoreBtn:SetActive(not showAllHonor and #self.m_HonorData > self.m_ShowAllHonorBtnCount)
    self.BackBtn:SetActive(showAllHonor)
    self:UpdatePage()
    if showAllHonor then
        self:InitAllHonor(1)
    end
end

function LuaCompetitionHonorFameHallWnd:InitRank()
    self.m_RankData = {}
    self.RankView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.m_RankData
    end, function(item, index)
        self:InitRankItem(item, index + 1)
    end)
    self.RankView:ReloadData(false, false)
end

function LuaCompetitionHonorFameHallWnd:InitRankItem(item, index)
    self.m_RankItemList[index] = item:GetComponent(typeof(QnTableItem))
    local data = self.m_RankData[index]
    -- 排名
    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankImage = item.transform:Find("RankImage"):GetComponent(typeof(CUITexture))
    rankLabel.gameObject:SetActive(data.rank > 3)
    rankLabel.text = tostring(data.rank)
    local imgIndex = data.rank
    if imgIndex >= 4 and imgIndex <= 10 then
        imgIndex = 4
    elseif imgIndex >= 11 then
        imgIndex = 5
    end
    rankImage:LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/competitionhonorfamehallwnd_icon_ranking_%d.mat", imgIndex))
    -- 其他
    if data.class ~= "" then 
        item.transform:Find("ClassSprite"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), data.class))
    end
    item.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = data.name
    item.transform:Find("ServerLabel"):GetComponent(typeof(UILabel)).text = data.sever
    item.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel)).text = data.score
    item.transform:Find("My").gameObject:SetActive(index == 1 and LuaCompetitionHonorMgr.m_SelfRank ~= 0)
    -- 点击
    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRankItemSelect(index)
    end)
end

function LuaCompetitionHonorFameHallWnd:OnPlayerQueryCompetitionHonorRankReturn()
    if LuaCompetitionHonorMgr.m_RankData == nil then return end

    self.m_RankData = {}
    local myData = nil
    local viewId = LuaCompetitionHonorMgr.m_ViewId
    if LuaCompetitionHonorMgr.m_SelfRank > 0 and CClientMainPlayer.Inst then
        myData = {}
        myData.rank = LuaCompetitionHonorMgr.m_SelfRank
        myData.Id = CClientMainPlayer.Inst.Id
        myData.class = EnumToInt(CClientMainPlayer.Inst.Class)
        myData.name = CClientMainPlayer.Inst.Name
        myData.sever = CClientMainPlayer.Inst:GetMyServerName()
        myData.gender = EnumToInt(CClientMainPlayer.Inst.Gender)
        myData.score = LuaCompetitionHonorMgr.m_SelfScore
    end
    local rankData = g_MessagePack.unpack(LuaCompetitionHonorMgr.m_RankData)
    for i = 1, #rankData, 7 do
        local data = {}
        data.rank = rankData[i]
        data.Id = rankData[i + 1]
        data.class = rankData[i + 2]
        data.name = rankData[i + 3]
        data.sever = rankData[i + 4]
        data.score = rankData[i + 5]
        data.gender = rankData[i + 6]
        table.insert(self.m_RankData, data)
        if myData and myData.Id == data.Id then
            myData = data
        end
    end
    if myData then
        viewId = viewId + 1
        table.insert(self.m_RankData, 1, myData)
    end
    self.m_RankItemList = {}
    self.RankView:ReloadData(false, false)
    self:OnRankItemSelect(viewId)
end

function LuaCompetitionHonorFameHallWnd:OnQueryCompetitionHonorPlayerInfoResult(targetId, score, appearanceProp, honorList)
    if self.m_SelectIndex and targetId ~= self.m_RankData[self.m_SelectIndex].Id then return end
    if self.m_IsChangeModel then
        -- 播放切换特效
        self.m_FakeModelSwitchFx.gameObject:SetActive(true)
        self.m_FakeModelSwitchFx:Play()
    end
    self.m_IsChangeModel = true
    self:InitModel(appearanceProp)
    self.ScoreLabel.text = SafeStringFormat3(LocalString.GetString("%d"), score)
    self.ScoreLabel.transform:Find("Label"):GetComponent(typeof(UILabel)):ResetAndUpdateAnchors()
    self.m_HonorData = {}
    local honorList = g_MessagePack.unpack(honorList)
    for i = 1, #honorList, 4 do
        local data = {}
        data.rank = honorList[i + 2]
        if data.rank <= 4 then
            data.gameId = honorList[i]
            data.season = honorList[i + 1]
            data.group = honorList[i + 3]
            local gameInfo = self:GetGameInfo(data.gameId, data.season)
            if gameInfo then
                local year, month = string.match(gameInfo.Time,"(%d+),(%d+)")
                gameInfo.Year = tonumber(year)
                gameInfo.Month = tonumber(month)
            end
            data.gameInfo = gameInfo
            table.insert(self.m_HonorData, data)
        end
    end
    self:SortHonorData()
    self:InitThreeHonor()
    self.m_HonorCurPage = 1
    self.m_HonorTotalPage = math.ceil(#self.m_HonorData / self.m_OnePageHonorCount)
    self.MoreBtn:SetActive(#self.m_HonorData > self.m_ShowAllHonorBtnCount)
    self.RankModel.gameObject:SetActive(true)
end

function LuaCompetitionHonorFameHallWnd:SortHonorData()
    table.sort(self.m_HonorData, function (a, b)
        -- return a.rank < b.rank
        if a.rank ~= b.rank  then return a.rank < b.rank end
        if a.gameInfo.Year ~= b.gameInfo.Year then return a.gameInfo.Year > b.gameInfo.Year end
        if a.gameInfo.Month ~= b.gameInfo.Month then return a.gameInfo.Month > b.gameInfo.Month end
        if a.gameId == 1 and b.gameId ~= 1 then return true end
        if a.gameId ~= 1 and b.gameId == 1 then return false end
        if a.gameId == 3 and b.gameId ~= 3 then return true end
        if a.gameId ~= 3 and b.gameId == 3 then return false end
    end)
end

function LuaCompetitionHonorFameHallWnd:InitThreeHonor()
    Extensions.RemoveAllChildren(self.HonorGrid.transform)
    for i = 1, 3 do
        if self.m_HonorData[i] ~= nil then
            local item = NGUITools.AddChild(self.HonorGrid.gameObject, self.HonorTemplate)
            self:InitHonorItem(item, i, self.m_HonorData[i])
        end
    end
    self.HonorGrid:Reposition()
end

function LuaCompetitionHonorFameHallWnd:InitAllHonor(beginIndex)
    Extensions.RemoveAllChildren(self.AllHonorGrid.transform)
    for i = 1, self.m_OnePageHonorCount do
        local index = beginIndex + i - 1 
        if self.m_HonorData[index] ~= nil then
            local item = NGUITools.AddChild(self.AllHonorGrid.gameObject, self.AllHonorTemplate)
            self:InitHonorItem(item, i, self.m_HonorData[index])
        end
    end
    self.AllHonorGrid:Reposition()
end

function LuaCompetitionHonorFameHallWnd:InitHonorItem(item, index, honorData)
    item.gameObject:SetActive(true)
    if honorData.gameInfo == nil then return end  -- 没有比赛信息
    local showData = self.m_HonorShowData[honorData.gameId]
    local groupText = honorData.group ~= -1 and showData.GroupNames and showData.GroupNames[honorData.rank - 1] or ""
    local rankText = self:GetResultText(honorData.rank)
    item.transform:Find("EditionLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("第%s届"), honorData.gameInfo.Season)
    item.transform:Find("GameTitleLable"):GetComponent(typeof(UILabel)).text = showData.Name
    item.transform:Find("LevelLabel"):GetComponent(typeof(UILabel)).text = groupText .. rankText
    local timeLabel = item.transform:Find("TimeLabel")
    if timeLabel then timeLabel:GetComponent(typeof(UILabel)).text = SafeStringFormat3("%d.%d", honorData.gameInfo.Year, honorData.gameInfo.Month) end
    local textureLeft = item.transform:Find("TextureLeft"):GetComponent(typeof(CUITexture))
    local textureRight = item.transform:Find("TextureRight"):GetComponent(typeof(CUITexture))
    textureLeft:LoadMaterial(showData.HonorIcons[honorData.rank - 1])
    textureRight:LoadMaterial(showData.HonorIcons[honorData.rank - 1])
end

function LuaCompetitionHonorFameHallWnd:InitModel(appearanceProp)
    local data = self.m_RankData[self.m_SelectIndex]
    self.RankModel:UpdateModel(data.Id, data.class, data.gender, data.name, data.sever, data.rank, appearanceProp, Vector3(0, -1, 7.5))
end

function LuaCompetitionHonorFameHallWnd:UpdatePage()
    self.HonorPage:SetActive(self.m_ShowAllHonor and self.m_HonorTotalPage > 1)
    self.PageLabel.text = SafeStringFormat3("%d/%d", self.m_HonorCurPage, self.m_HonorTotalPage)
    local posPre = self.PreBtn.transform.localPosition
    posPre.z = self.m_HonorCurPage ~= 1 and 1 or -1
    self.PreBtn.transform.localPosition = posPre
    self.PreBtn:GetComponent(typeof(BoxCollider)).enabled = self.m_HonorCurPage ~= 1
    local posNext = self.NextBtn.transform.localPosition
    posNext.z = self.m_HonorCurPage ~= self.m_HonorTotalPage and 1 or -1
    self.NextBtn.transform.localPosition = posNext
    self.NextBtn:GetComponent(typeof(BoxCollider)).enabled = self.m_HonorCurPage ~= self.m_HonorTotalPage
end

function LuaCompetitionHonorFameHallWnd:GetGameInfo(gameId, season)
    if gameId == 1 then
        return CompetitionHonor_DouHunTanSeason.GetData(season)
    elseif gameId == 2 then
        return CompetitionHonor_QMPKSeason.GetData(season)
    elseif gameId == 3 then
        return CompetitionHonor_StarBiWuSeason.GetData(season)
    end
end

function LuaCompetitionHonorFameHallWnd:GetResultText(rank)
    if rank == 1 then
        return LocalString.GetString("冠军")
    elseif rank == 2 then
        return LocalString.GetString("亚军")
    elseif rank == 3 then
        return LocalString.GetString("季军")
    elseif rank == 4 then
        return LocalString.GetString("殿军")
    end
	return ""
end

--@region UIEvent

function LuaCompetitionHonorFameHallWnd:OnRankItemSelect(index)
    self.m_SelectIndex = index
    if self.m_RankData[index].name == "" then return end
    Gac2Gas.QueryCompetitionHonorPlayerInfo(self.m_RankData[index].Id)
    for i = 1, #self.m_RankItemList do
        self.m_RankItemList[i]:SetSelected(i == index)
    end
end

function LuaCompetitionHonorFameHallWnd:OnTipBtnClick()
	g_MessageMgr:ShowMessage("CompetitionHonor_FameHall_HonorScore_Tips")
end

function LuaCompetitionHonorFameHallWnd:OnPageBtnClick(change)
    self.m_HonorChangePageAnim:Play("competitionhonor_switchallhonor")
    self.m_HonorCurPage = self.m_HonorCurPage + change
    self:UpdatePage()
    local beginIndex = self.m_OnePageHonorCount * (self.m_HonorCurPage - 1) + 1
    self:InitAllHonor(beginIndex)
end

function LuaCompetitionHonorFameHallWnd:OnMoreBtnClick()
    self.m_Animation:Play("competitionhonor_switchtohonor")
    self:SetView(true)
end

function LuaCompetitionHonorFameHallWnd:OnBackBtnClick()
    self.m_Animation:Play("competitionhonor_switchtomain")
    self:SetView(false)
end

--@endregion UIEvent
function LuaCompetitionHonorFameHallWnd:OnEnable()
    g_ScriptEvent:AddListener("PlayerQueryCompetitionHonorRankReturn", self, "OnPlayerQueryCompetitionHonorRankReturn")
    g_ScriptEvent:AddListener("QueryCompetitionHonorPlayerInfoResult", self, "OnQueryCompetitionHonorPlayerInfoResult")
end

function LuaCompetitionHonorFameHallWnd:OnDisable()
    g_ScriptEvent:RemoveListener("PlayerQueryCompetitionHonorRankReturn", self, "OnPlayerQueryCompetitionHonorRankReturn")
    g_ScriptEvent:RemoveListener("QueryCompetitionHonorPlayerInfoResult", self, "OnQueryCompetitionHonorPlayerInfoResult")
end