require("common/common_include")

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CBaseWnd = import "L10.UI.CBaseWnd"
local UILabel = import "UILabel"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"

LuaLiangHaoReqCustomizeCardWnd = class()

RegistClassMember(LuaLiangHaoReqCustomizeCardWnd, "m_TitleLabel")
RegistClassMember(LuaLiangHaoReqCustomizeCardWnd, "m_AvaliableNumLabel")
RegistClassMember(LuaLiangHaoReqCustomizeCardWnd, "m_PlayerNumLabel")
RegistClassMember(LuaLiangHaoReqCustomizeCardWnd, "m_OtherPlayerRoot")
RegistClassMember(LuaLiangHaoReqCustomizeCardWnd, "m_NoOtherPlayerInfoLabel")
RegistClassMember(LuaLiangHaoReqCustomizeCardWnd, "m_OtherPlayerInfoTipLabel")
RegistClassMember(LuaLiangHaoReqCustomizeCardWnd, "m_OtherPlayerInfoLabel")
RegistClassMember(LuaLiangHaoReqCustomizeCardWnd, "m_OtherPlayerPriceTipLabel")
RegistClassMember(LuaLiangHaoReqCustomizeCardWnd, "m_OtherPlayerPriceLabel")
RegistClassMember(LuaLiangHaoReqCustomizeCardWnd, "m_AddSubInputButton")
RegistClassMember(LuaLiangHaoReqCustomizeCardWnd, "m_MyPriceLabel")
RegistClassMember(LuaLiangHaoReqCustomizeCardWnd, "m_AuctionButton")

function LuaLiangHaoReqCustomizeCardWnd:Init()

	self.m_TitleLabel = self.transform:Find("Anchor/HeadBg/TitleLabel"):GetComponent(typeof(UILabel))
	self.m_AvaliableNumLabel = self.transform:Find("Anchor/HeadBg/AvaliableLabel/ValueLabel"):GetComponent(typeof(UILabel))
	self.m_PlayerNumLabel = self.transform:Find("Anchor/HeadBg/PlayerLabel/ValueLabel"):GetComponent(typeof(UILabel))
	self.m_OtherPlayerRoot = self.transform:Find("Anchor/OtherPlayer").gameObject
	self.m_NoOtherPlayerInfoLabel = self.transform:Find("Anchor/NoOtherPlayerInfoLabel").gameObject
	self.m_OtherPlayerInfoTipLabel = self.transform:Find("Anchor/OtherPlayer/PlayerInfoTipLabel"):GetComponent(typeof(UILabel))
	self.m_OtherPlayerInfoLabel = self.transform:Find("Anchor/OtherPlayer/PlayerInfoLabel"):GetComponent(typeof(UILabel))
	self.m_OtherPlayerPriceTipLabel = self.transform:Find("Anchor/OtherPlayer/PlayerPriceTipLabel"):GetComponent(typeof(UILabel))
	self.m_OtherPlayerPriceLabel = self.transform:Find("Anchor/OtherPlayer/PriceLabel"):GetComponent(typeof(UILabel))
	self.m_AddSubInputButton = self.transform:Find("Anchor/MyAuction/AddSubInputButton"):GetComponent(typeof(QnAddSubAndInputButton))
	self.m_MyPriceLabel = self.transform:Find("Anchor/MyAuction/PriceLabel"):GetComponent(typeof(UILabel))
	self.m_AuctionButton = self.transform:Find("Anchor/AuctionButton").gameObject
	
	CommonDefs.AddOnClickListener(self.m_AuctionButton, DelegateFactory.Action_GameObject(function(go) self:OnAuctionButtonClick() end), false)

	local data = LuaLiangHaoMgr.ReqCustomizeCardInfo

	self.m_TitleLabel.text = LuaLiangHaoMgr.GetCardTitleName(data.digitNum)
	self.m_AvaliableNumLabel.text = tostring(data.toufangNum)
	self.m_PlayerNumLabel.text = tostring(data.playerNum)
	if data.playerNum>0 then
		self.m_OtherPlayerRoot:SetActive(true)
		self.m_NoOtherPlayerInfoLabel:SetActive(false)
		self.m_OtherPlayerInfoTipLabel.text = SafeStringFormat3(LocalString.GetString("第%s名玩家"), math.min(data.playerNum, data.toufangNum))
		self.m_OtherPlayerInfoLabel.text = data.roleName
		self.m_OtherPlayerPriceTipLabel.text = SafeStringFormat3(LocalString.GetString("第%s名出价"), math.min(data.playerNum, data.toufangNum))
		self.m_OtherPlayerPriceLabel.text = tostring(data.currentPrice)
	else
		self.m_OtherPlayerRoot:SetActive(false)
		self.m_NoOtherPlayerInfoLabel:SetActive(true)
	end

	self.m_AddSubInputButton:SetMinMax(data.minPrice, data.maxPrice, data.priceStep)
  	self.m_AddSubInputButton:SetValue(data.minPrice, true)
  	self.m_AddSubInputButton:SetNumberInputAlignType(CTooltipAlignType.Right)
	self.m_MyPriceLabel.text = tostring(math.max(0, data.minPrice - data.myPrice))

	self.m_AddSubInputButton.onValueChanged = DelegateFactory.Action_uint(function (val)
		self:OnAuctionValueChanged(val)
	end)
end

function LuaLiangHaoReqCustomizeCardWnd:OnAuctionValueChanged(val)
	local data = LuaLiangHaoMgr.ReqCustomizeCardInfo
	self.m_MyPriceLabel.text = tostring(math.max(0, val - data.myPrice))
end

function LuaLiangHaoReqCustomizeCardWnd:OnAuctionButtonClick()
	local data = LuaLiangHaoMgr.ReqCustomizeCardInfo
	local lingyu = self.m_AddSubInputButton:GetValue()
	local lingyuDisplay = lingyu - data.myPrice
	local digitNum = data.digitNum
	MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Lianghao_DingZhi_Comfirm",lingyuDisplay, lingyu, data.digitNum), 
		DelegateFactory.Action(function() 
			LuaLiangHaoMgr.DoAuctionCustomizeCard(digitNum, lingyu)
			self:Close()
		end),nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
	
end

function LuaLiangHaoReqCustomizeCardWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end
