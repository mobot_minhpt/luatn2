local Extensions = import "Extensions"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local UISlider = import "UISlider"
local CButton = import "L10.UI.CButton"

LuaShiTuWeeklyHomeworkView = class()

RegistChildComponent(LuaShiTuWeeklyHomeworkView, "m_Grid","Grid", UIGrid)
RegistChildComponent(LuaShiTuWeeklyHomeworkView, "m_Template","Template", GameObject)
RegistChildComponent(LuaShiTuWeeklyHomeworkView, "m_ScorllView2","ScorllView2", UIScrollView)
RegistClassMember(LuaShiTuWeeklyHomeworkView,"m_AllHomework")

function LuaShiTuWeeklyHomeworkView:Awake()
    self.m_Template:SetActive(false)
end

function LuaShiTuWeeklyHomeworkView:OnEnable()
    g_ScriptEvent:AddListener("ShiTuTrainingHandbook_SyncShiTuWeekTaskInfo", self, "OnSyncShiTuWeekTaskInfo")
    g_ScriptEvent:AddListener("ShiTuTrainingHandbookWnd_ChooseTudiPlayerId", self, "OnChooseTudiPlayerId")
    self:OnChooseTudiPlayerId()
end

function LuaShiTuWeeklyHomeworkView:OnDisable()
    g_ScriptEvent:RemoveListener("ShiTuTrainingHandbook_SyncShiTuWeekTaskInfo", self, "OnSyncShiTuWeekTaskInfo")
    g_ScriptEvent:RemoveListener("ShiTuTrainingHandbookWnd_ChooseTudiPlayerId", self, "OnChooseTudiPlayerId")
end

function LuaShiTuWeeklyHomeworkView:OnChooseTudiPlayerId()
    local playerId = LuaShiTuTrainingHandbookMgr:GetPartnerPlayerId()
    if playerId == 0 then
        return
    end
    Gac2Gas.QueryShiTuCultivateInfo(LuaShiTuTrainingHandbookMgr.wndtype.WeeklyHomework, playerId)
end

function LuaShiTuWeeklyHomeworkView:OnSyncShiTuWeekTaskInfo(data)
    for i = 1, #data do
        local t = data[i]
        local weekTask = ShiTuCultivate_WeekTasks.GetData(data[i].weekTaskId)
        t.weekTask = weekTask
        local teacherRewards = {}
        table.insert(teacherRewards, {scorekey = EnumPlayScoreKey.QYJF, num = weekTask.ShifuQingyi})
        if weekTask.ShifuItems then
            for j = 0, weekTask.ShifuItems.Length - 1, 2 do
                local itemId, num = weekTask.ShifuItems[j], weekTask.ShifuItems[j + 1]
                table.insert(teacherRewards, {item = Item_Item.GetData(itemId), num = num})
            end
        end
        t.teacherRewards = teacherRewards
        local studentRewards = {}
        table.insert(studentRewards, {scorekey = EnumPlayScoreKey.QYJF, num = weekTask.TudiQingyi})
        table.insert(studentRewards, {scorekey = EnumPlayScoreKey.GIFT, num = weekTask.TudiGiftLimit})
        if weekTask.TudiItems then
            for j = 0, weekTask.TudiItems.Length - 1, 2 do
                local itemId, num = weekTask.TudiItems[j], weekTask.TudiItems[j + 1]
                table.insert(studentRewards, {item = Item_Item.GetData(itemId), num = num})
            end
        end
        t.studentRewards = studentRewards
        t.isFinishedTask = t.currentValue == t.weekTask.Num
        t.state = t.isRewarded and 3 or (t.isFinishedTask and 1 or 2)

    end
    local alert = false
    for i = 1, #data do
        local t = data[i]
        if t.state == 1 then
            alert = true
        end
    end
    g_ScriptEvent:BroadcastInLua("ShiTuTrainingHandbook_Alert", LuaShiTuTrainingHandbookMgr.wndtype.WeeklyHomework, alert)
    table.sort(data,function(a, b)
        if a.state == b.state then
            return a.weekTask.ID < b.weekTask.ID
        end
        return a.state < b.state
    end)
    self:InitAllHomework(data)
end

function LuaShiTuWeeklyHomeworkView:InitAllHomework(data)
    Extensions.RemoveAllChildren(self.m_Grid.transform)
    self.m_AllHomework = {}
    for i = 1, #data do
        local go = NGUITools.AddChild(self.m_Grid.gameObject, self.m_Template)
        go:SetActive(true)
        local homework = self:InitComponent(go)
        self:InitHomework(homework, data[i])
        table.insert(self.m_AllHomework, homework)
    end
    self.m_Grid:Reposition()
    if self.m_ScorllView2 then
        self.m_ScorllView2:ResetPosition()
    end
end

function LuaShiTuWeeklyHomeworkView:InitComponent(go)
    local homework = {}
    homework.conditionLabel = go.transform:Find("Top/ConditionLabel"):GetComponent(typeof(UILabel))
    homework.slider = go.transform:Find("Top/Slider"):GetComponent(typeof(UISlider))
    homework.slider.value = 0
    homework.progressLabel = go.transform:Find("Top/Slider/ProgressLabel"):GetComponent(typeof(UILabel))
    homework.studentGrid = go.transform:Find("Bottom/StudentGrid"):GetComponent(typeof(UIGrid))
    homework.teacherGrid = go.transform:Find("Bottom/TeacherGrid"):GetComponent(typeof(UIGrid))
    Extensions.RemoveAllChildren(homework.studentGrid.transform)
    Extensions.RemoveAllChildren(homework.teacherGrid.transform)
    homework.rewardTemplate = go.transform:Find("Bottom/RewardTemplate").gameObject
    homework.rewardTemplate:SetActive(false)
    homework.finishedLabel = go.transform:Find("State/FinishedLabel"):GetComponent(typeof(UILabel))
    homework.ongoingLabel = go.transform:Find("State/OngoingLabel"):GetComponent(typeof(UILabel))
    homework.getRewardButton = go.transform:Find("State/GetRewardButton"):GetComponent(typeof(CButton))
    homework.switchButton = go.transform:Find("State/OngoingLabel/SwitchButton"):GetComponent(typeof(CButton))
    return homework
end

function LuaShiTuWeeklyHomeworkView:InitHomework(homework, data)
    homework.conditionLabel.text = SafeStringFormat3(data.weekTask.Description, data.weekTask.Num)
    homework.slider.value = (data.weekTask.Num >= 0) and (data.currentValue / data.weekTask.Num) or 0
    homework.progressLabel.text =  SafeStringFormat3("%d/%d",data.currentValue ,data.weekTask.Num)

    self:InitRewards(homework, data)
    self:InitHomeworkState(homework, data)
end

function LuaShiTuWeeklyHomeworkView:InitRewards(homework, data)
    Extensions.RemoveAllChildren(homework.teacherGrid.transform)
    Extensions.RemoveAllChildren(homework.studentGrid.transform)
    for i = 1, #data.teacherRewards do
        self:InitReward(homework.teacherGrid, data.teacherRewards[i], homework)
    end
    for i = 1, #data.studentRewards do
        self:InitReward(homework.studentGrid, data.studentRewards[i], homework)
    end
    homework.teacherGrid:Reposition()
    homework.studentGrid:Reposition()
end

function LuaShiTuWeeklyHomeworkView:InitReward(grid,data, homework)
    if (data.num == 0) and data.scorekey then return end
    local item = data.item
    local go = NGUITools.AddChild(grid.gameObject, homework.rewardTemplate)
    local icon = go:GetComponent(typeof(CUITexture))
    if item then icon:LoadMaterial(item.Icon) end
    if data.scorekey then icon:LoadMaterial(LuaShiTuTrainingHandbookMgr:GetPlayScore(data.scorekey).ScoreIcon) end

    local label = go.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
    label.text = data.num
    label.gameObject:SetActive(data.num > 1)
    if item and item.ID then
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function()
            CItemInfoMgr.ShowLinkItemTemplateInfo(item.ID, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
        end)
    end
    go:SetActive(true)
end

function LuaShiTuWeeklyHomeworkView:InitHomeworkState(homework, data)
    homework.finishedLabel.gameObject:SetActive(data.state == 3)
    homework.ongoingLabel.gameObject:SetActive(data.state == 2)
    homework.getRewardButton.gameObject:SetActive(data.state == 1)
    UIEventListener.Get(homework.switchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:SwitchHomework(data)
    end)
    if data.state == 1 then
        UIEventListener.Get(homework.getRewardButton.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            self:GetRewards(data)
        end)
    end
end

function LuaShiTuWeeklyHomeworkView:GetRewards(data)
    Gac2Gas.ShiTuWeekTaskGetReward(LuaShiTuTrainingHandbookMgr:GetPartnerPlayerId(), data.weekTaskId)
end

function LuaShiTuWeeklyHomeworkView:SwitchHomework(data)
    --local des = g_MessageMgr:FormatMessage("ShiTu_SwitchWeeklyHomework")
    --QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.YinPiao, des, 1000, DelegateFactory.Action(function ()
    --    Gac2Gas.ShiTuWeekTaskShiFuRefreshTask(LuaShiTuTrainingHandbookMgr:GetTudiPlayerId(), data.weekTaskId)
    --end), nil, false, false, EnumPlayScoreKey.NONE, false)
end
