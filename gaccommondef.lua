
---- 定义一下只在客户端用的变量

-- 道路信息类型
ERoadInfo = {
	eRI_ROAD = 0,
  eRI_SAFE = 1,
  eRI_NOMONSTER = 2,
  eRI_VENDOR = 3,
  eRI_SKILL = 4,
  eRI_FISHING = 5,
  eRI_WATER = 6,
  eRI_PLANTING=7,
  eRI_PLAY1 = 8,
  eRI_PLAY2 = 9,
  eRI_UNUSED6 = 10,
  eRI_HUNTING = 11,
  eRI_UNUSED8 = 12,
  eRI_UNUSED9 = 13,
  eRI_DIGGING = 14,
  eRI_LOGGING = 15,
  eRI_PLAY5 = 16,
}

--- 装饰物使用类型
EnumFurnitureUseType = {
	eFurnitureNormal = 0,
	eFurnitureWall = 1,
	eFurnitureGround = 2,
	eFurnitureAnimal = 3,
	eFurnitureOnWater = 4,
  eFurnitureInWater = 5,
}

EnumFurnitureLocation = 
{
	eUnknown = 0,
	eYardLand = 1,
	eRoomLand = 2,
	eLand = 3,
	eRoomWall = 4,
	eDesk = 5,
	eYardFenceAndRoomWall = 8,
  eWater = 9,
	ePool = 10,
	eWarmPool = 11,
}


--- 障碍类型
EBarrierType = {
	eBT_OutRange = -1, 
	eBT_NoBarrier = 0,
	eBT_LowBarrier = 1,
	eBT_MidBarrier = 2,
	eBT_HighBarrier = 3,
}

EnumFurnitureType = {
  eUnknown = 0,
  eChuangta = 1,
  eChugui = 2,
  eZhuoan = 3,
  eZuoju = 4,
  ePingfeng = 5,
  eZawu = 6,
  eGuashi = 7,
  eQinggong = 8,
  eChuanjiabao = 9,
  eXiaojing = 10,
  eHuamu = 11,
  eJianzhu = 12,
  eNpc = 13,
  eLingshou = 14,
  ePuppet = 15,
  eMapi = 16,
  eSkyBox = 17,
  eZswTemplate = 18,
  eWallPaper = 19,
  ePoolFurniture = 20,
  eHouseFish = 21,
}


EnumFurnitureSubType = {
	eUnknown = 0,
  eChuangta = 1,
  eChugui = 2,
  eZhuoan = 3,
  eZuoju = 4,
  ePingfeng = 5,
  eZawu = 6,
  eGuashi = 7,
  eQinggong = 8,
  eChuanjiabao = 9,
  eXiaojing = 10,
  eHuamu = 11,
  eZhengwu = 12,
  eCangku = 13,
  eXiangfang = 14,
  eMiaopu = 15,
  eNpc = 16,
  eWeiqiang = 17,
  eLingshou = 18,
  ePuppet = 19,
  eMapi = 20,
  eSkyBox = 21,
  eZswTemplate = 22,
  eWallPaper = 23,
  ePoolFurniture = 24,
  eFish = 25,
  eRollerCoasterCabin = 26,
  eRollerCoasterTrack = 27,
  eRollerCoasterStation = 28,
}


EnumQmpkExcellentDataShareType = {
	LianZhan = 1,
	Kill = 2,
	Heal = 3,
	Relive = 4,
	Ctrl = 5,
	KillLingShou = 6,
	DPS = 7,
}
EnumQmpkPlayStage = {
  eEnd        = 0,
  eQieCuoSai  = 1,
  ePiPeiSai   = 2,
  eJiFenSai   = 3,
  eTaoTaiSai  = 4,
  eZongJueSai = 5,
  eReShenSai  = 6,
  eMeiRiYiZhanSai = 7,
}

EnumGuanNingWeekDataKey = {
  -- 月总数据
  eJoinNum = 1,
  eVictoryNum = 2,
  eLianShengNum = 3,
  eTotalKillNum = 4,
  eTotalHelpNum = 5,
  eTotalDieNum = 6,
  eTotalReliveNum = 7,
  eTotalOccupyHighLandNum = 8,
  eTotalOccupyForestNum = 9,
  eTotalOccupyCaveNum = 10,
  eTotalSubmitFlagNum = 11,

  -- 单场最佳
  eMaxPlayLianShaNum = 12,
  eMaxPlayScore = 13,
  eMaxPlayKillNum = 14,
  eMaxPlayHelpNum = 15,
  eMaxPlayDieNum = 16,
  eMaxPlayReliveNum = 17,
  eMaxPlayOccupyHighLandNum = 18,
  eMaxPlayOccupyForestNum = 19,
  eMaxPlayOccupyCaveNum = 20,
  eMaxPlaySubmitFlagNum = 21,

  -- 不包括本场的单场最佳
  eLastMaxPlayScore = 22,
  eLastMaxPlayKillNum = 23,
  eLastMaxPlayDieNum = 24,
  eLastMaxPlayHelpNum = 25,
  eLastMaxPlayReliveNum = 26,

  -- 单场己方最佳
  eMaxDpsInForce = 27,
  eMaxUnderDamageInForce = 28,
  eMaxHealInForce = 29,
  eMaxBaoShiNumInForce = 30,
  eMaxCtrlNumInForce = 31,
  eMaxOccupyScoreInForce = 32,
  eMaxSubmitFlagNumInForce = 33,

  -- 新加
  eMaxLianShengNum = 34,
  eTotalScore = 35,

  -- 安期岛之战
  eMaxCannonPickInForce = 36,
  eMaxBosskillInForce = 37,

  -- 点赞
  eTotalSendDianZanNum = 38,
	eTotalGetDianZanNum = 39,
	eMaxGetDianZanNum = 40,
}

EnumBiaoCheStatus = { --和服务器保持一致
  eSubmitRes = 0,   -- 提交资源
  eYaYun = 1,     -- 押运状态
  eYaYunEnd = 2,    -- 押运结束
}

local Color = import "UnityEngine.Color"
EnumBiaoCheStatusTextColor = {
  eSubmitting = Color.yellow,
  eSubmitDone = Color.green,
  eYaYun = Color.red,
  eYaYunEnd = Color.white,
}

 CityWarMapZone2ResIcon = {
  [0] = "",   -- 金元洞天 用不到
  [1] = "skill_attextmode_huo",   -- 赤天
  [2] = "skill_attextmode_mu",   -- 青天
  [3] = "skill_attextmode_bing",   -- 玄天
  [4] = "skill_attextmode_feng",   -- 素天
}

local NGUIText = import "NGUIText"
CityWarMapZone2ResColor = {
  [0] = Color.white,   -- 金元洞天 用不到
  [1] = NGUIText.ParseColor24("ff5b52", 0),   -- 赤天
  [2] = NGUIText.ParseColor24("74ee7e", 0),   -- 青天
  [3] = NGUIText.ParseColor24("3ae5ff", 0),   -- 玄天
  [4] = NGUIText.ParseColor24("5db3fd", 0),   -- 素天
}


EnumCityWarContributionSortOrder = {
  eDefault = 0, --默认降序
  eDescendingOrder = 0, --降序
  eAscendingOrder = 1, --升序
}

EnumCityWarContributionSortIndex = {
  eDefault = 0, --是否在线
  eName = 1, --名称
  eLevel = 2, --等级
  eYaYunTimes = 3, --押镖次数
  eZhuangTianGongFengTimes = 4, --装填/供奉次数 优先按照供奉，然后按装填 - by尹辉
  eZiCaiTimes = 5, --城建资材提交次数
  eGateProgressTimes = 6, --建灵飞镜
  ePublicMapScore = 7, --野外评价
  eMirrorWarScore = 8, --宣战评价
}

EnumQueryCityDataAction = {
  eCityData = 0,  -- 标记从城池按钮打开窗口
  eJoinYaYun = 1, -- 标记从活动日程打开窗口
}

EnumCityWarPrimaryMapWndTabIndex = {
  eDefault = 2,
  eTerritory = 1, -- 领土
  eOccupy = 2, -- 占领
  eYaYun = 3,  -- 押镖
}

-- 宝宝作诗的类型
EnumBabyChatSkill = {
  eNormal = "",
  ePoem = "poem_writing",
  eDivination = "fortune_telling",
}

EnumShowBabyChatContext = {
  eFromNPC = 1,
  eFromBabyWnd = 2,
}

EnumCityWarUnitType = {
  eWall = 1, -- 城墙
  eCorner = 2, -- 城墙转角
  eGate = 3, -- 城门
  eHuoPao = 4, -- 喷射火炮
  eZhongPao = 5, --重炮
  eBomb = 6, -- 炸弹
  eTanSheBan = 7, -- 弹射板
  eTrap = 8, -- 陷阱
  eBindTower = 9, -- 定身塔
  eDizzyTower = 10, -- 眩晕塔
  eChaosTower = 11, -- 混乱塔
  eSilenceTower = 12, -- 沉默塔
  eSleepTower = 13, -- 睡眠塔
  eTieTower = 14, -- 束缚塔
  eWindTower = 15, -- 强风塔
  eFireTower = 16, -- 烈焰塔
  eIceTower = 17, -- 寒冰塔
  eThunderTower = 18, -- 厉雷塔
  ePoisonTower = 19, -- 剧毒塔
  eLightTower = 20, -- 炫光塔
  eIllusionTower = 21, -- 幻惑塔
  eWaterTower = 22, -- 渊水塔
  eHall = 23, -- 城镇大厅
  eRepertory = 24, -- 仓库
  eTower = 25, -- 复活点
}

--战况类型 (对应Cs下的EnumSituationType)
EnumSituationType = {
  eKoudao = 0,
  eBangHua = 1,
  ePGQY = 2,
  eTianMenShan = 3,
  eGuildTerritorialWarsChallenge = 4,
  eHengYuDangKou = 5,
}

--通用战况窗口标签页 (对应Cs代码中EnumStatType)
EnumSituationWndTab = {
  Undefined = 0,
  eDps = 1,
  eHeal = 2,
  eSuffer = 3,
  eGuildHero = 4,
}

--消息名枚举，将消息名定义到这里，通过EnumMessageName来访问，可以避免消息重复
EnumMessage = {
  UpdateCityWarUnitInfoForTip = "UpdateCityWarUnitInfoForTip",
}

--QTE类型
EnumQTEType = {
  Shake = 1,
  SingleClick = 2,
  ConsecutiveClick = 3,
  SequentialClick = 4,
  Slide = 5,
}

--QTE滑动方向 
EnumQTESlideDirection = {
  Up = 1,
  Down = 2,
  Left = 3,
  Right = 4,
}

EnumQueryVoiceTranslationContext = {
  OnIM = "im",
  OnIMThenVoicePlatform = "im_then_voice_platform",
}

EnumClientLifeSkillType = {
  eFishing = 1,
  eGathering = 2,
  eMakeDrug = 3,
  eCook = 4,
}

EnumLiangHaoPurchaseInfoReplyType = { --靓号数据返回的类别
  eGacRequest = "gac", --打开窗口请求
  ePurchase = "buy", --玩家购买
  eAuction = "jingpai", --玩家竞拍
  eCustomize = "customize", --定制靓号
  eRefresh = "refresh", --刷新请求
}

EnumNeteaseMemberRightType = { --和服务器端定义保持一致
  eBindGame = 1,
  eBindGift = 2,
  eSequentGift = 3,
  eSingleGift = 4,
  eActivationGift = 5,
  eMaxRightType = 5,
  --这两个给客户端用的
  eHolidayGift = 10,
  eShopMall = 11,
}

--标签处理类型
EnumFamilyTreeTagOperationType = {
	eAdd = 0,
	eModify = 1,
	eDel = 2,
	eDelOther = 3,
}

--影灵职业形态，和服务器定义保持一致
EnumYingLingState = {
  eDefault = 0,
  eMale = 1, --男
  eFemale = 2, --女
  eMonster = 3, --妖
}

--鉴定技能消耗品类别
EnumIdentifyWashItemType = {
  TaiChuJingYuan = 0,
  JianDingTianShu = 1,

  JianDingTianShu_1 = 2,
  JianDingTianShu_2 = 3,
  JianDingTianShu_3 = 4,
  JianDingTianShu_4 = 5,
  JianDingTianShu_5 = 6,
}

--天赋需求修为类型
TianFuNeedXiuweiType = {
  Xiuwei40 = 0,
  Xiuwei60 = 1,
  Xiuwei80 = 2,
  Xiuwei100 = 3
}

EnumJingLingQuickPurchaseType = {
  eRMB = 1,
  eLingYu = 2,
  eYuanBao = 3,
  eLingYuAndYuanBao = 4
}

-- 图集中的图标名引用定义
-- 新增内容请放到g_Sprites中
AllSpriteNames = {
  achievement_star_normal = "main_playerwnd_achievement_star_normal",
  achievement_star_highlight =  "main_playerwnd_achievement_star_highlight",

}

EnumSummonTeammateWndType = {
  eDefault = 0,
  eDieKe = 1,
}

 -- 家园室内、厢房墙纸地板
EnumHouseRoomType = {
    Room = 1,
    Xiangfang = 2,
}

EnumFishBasketType = {
	Fish = 0,
	Other = 1,
}

EnumFishBasketReorderType = {
	Level = 1,
	Type = 2,
}

EnumHousePaperPlaceType = {
    Floor = 1,
    Wall = 2,
}

EnumSMSWPlayState = {
	Idle = 0,
	Prepare = 1,
	Stage_1 = 2,
	Stage_2 = 3,
	Stage_3 = 4,
	Reward = 5,
	Destroy = 6,
  	Delay = 7,
}


EnumSMSWPlayerLevelStage = {
	Stage_69 = 1,
	Stage_89 = 2,
	Stage_109 = 3,
	Stage_129 = 4,
	Stage_160 = 5,
}

EnumSectOffice = {
    eZhangMen = 1,
    eZhangLao = 2,
    eDiZi = 100,
}

EnumMusicBoxState = {
  Unstart = 0,
  Playing = 1,
  Pause = 2,
}

EnumShiMenMiShuConsumeType = {
  OnlyGNJCScore = 1,
  GNJCScoreAndMoney = 2,
}
EnumSeasonRankType = {
	eShengXiaoCard = 1001,
  eXinShengHuaJi = 1002,
  eQiXi2022YYS = 1004,
  -- 端午2022驱五毒各职业排行榜预留2000 ~ 2000 + 职业数(12) 
	eSheShouDuanWu2022QuWuDu = 2001,
	eJiaShiDuanWu2022QuWuDu = 2002,
	eDaoKeDuanWu2022QuWuDu = 2003,
	eXiaKeDuanWu2022QuWuDu = 2004,
	eFangShiDuanWu2022QuWuDu = 2005,
	eYiShiDuanWu2022QuWuDu = 2006,
	eMeiZheDuanWu2022QuWuDu = 2007,
	eYiRenDuanWu2022QuWuDu = 2008,
	eYanShiDuanWu2022QuWuDu = 2009,
	eHuaHunDuanWu2022QuWuDu = 2010,
	eYingLingDuanWu2022QuWuDu = 2011,
	eDieKeDuanWu2022QuWuDu = 2012,
	--
}

EnumBeginnerGuideTaskStatus = {
	eNew = 0,
	eMax = 120,
  eCanSubmit = 126,
	eFinish = 127,
}

EnumBeginnerGuideStage = {
  eClose = 0,
	eTasking = 1,
	eOpen = 2,
	eFinalRewarding = 3,
	eFinish = 4,
}

EnumBeginnerGuideDayStatus = {
	eEmpty = 0,
	eAccept = 1,
	eFinish = 2,
}

EnumGuanNingPlayStatus = {
  eBeiZhan = 0,
  ePrepare = 1,
  eStart = 2,
  eFinish = 3,
}

EnumGuildCustomBuildPreTaskStatus = {
	eInit = 0,
	eUnlocked = 1,
	eTask1 = 2,
	eTask2 = 3,
	eTask3 = 4,
	eFinish = 5,
}

-- 横屿荡寇副本难度类型
EnumHengYuDangKouStage = {
  eNormal = 0,	--普通模式
  eHard = 1,		--精英模式
}