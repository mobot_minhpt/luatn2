local CButton           = import "L10.UI.CButton"
local UITable           = import "UITable"
local UILabel           = import "UILabel"
local GameObject        = import "UnityEngine.GameObject"
local CommonDefs        = import "L10.Game.CommonDefs"
local DelegateFactory   = import "DelegateFactory"
local Extensions        = import "Extensions"
local CMessageTipMgr    = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local CUICommonDef      = import "L10.UI.CUICommonDef"
local Color             = import "UnityEngine.Color"

LuaKanYiDaoEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaKanYiDaoEnterWnd, "EnterButton", "EnterButton", CButton)
RegistChildComponent(LuaKanYiDaoEnterWnd, "RuleTable", "RuleTable", UITable)
RegistChildComponent(LuaKanYiDaoEnterWnd, "ParagraphTemplate", "ParagraphTemplate", GameObject)
RegistChildComponent(LuaKanYiDaoEnterWnd, "CountLabel", "CountLabel", UILabel)
RegistChildComponent(LuaKanYiDaoEnterWnd, "TimeLabel", "TimeLabel", UILabel)

--@endregion RegistChildComponent end

function LuaKanYiDaoEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.EnterButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterButtonClick()
	end)

	self.ParagraphTemplate:SetActive(false)
    --@endregion EventBind end
end

function LuaKanYiDaoEnterWnd:Init()
    self:InitTimeText()
	self:InitLeftTimes()
    self:ParseRuleText()
end

-- 初始化时间信息
function LuaKanYiDaoEnterWnd:InitTimeText()
    self.TimeLabel.text = Double11_Setting.GetData().KanYiDao_Time --从配置表中读取时间数据
end

-- 解析规则信息
function LuaKanYiDaoEnterWnd:ParseRuleText()
    Extensions.RemoveAllChildren(self.RuleTable.transform)
    local msg =	g_MessageMgr:FormatMessage("KanYiDao_Rule",{});
    if System.String.IsNullOrEmpty(msg) then
        return
    end

    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if not info then
		return
	end

    do 
		local i = 0
		while i < info.paragraphs.Count do							-- 根据信息创建并显示段落
			local paragraphGo = CUICommonDef.AddChild(self.RuleTable.gameObject, self.ParagraphTemplate) -- 添加段落Go
			paragraphGo:SetActive(true)
			local tip = CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem))
			tip:Init(info.paragraphs[i], 4294967295)				-- 初始化段落，color = 4294967295（0xFFFFFFFF）
			i = i + 1
		end
	end
	self.RuleTable:Reposition()
end

-- 初始化剩余次数
function LuaKanYiDaoEnterWnd:InitLeftTimes()
    local count = 1 - CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eDouble11KYDRewardTimes)
    local color = (count == 0) and Color(1, 0.3125, 0.3125, 1) or Color(1, 1, 1, 1)

	self.CountLabel.text = tostring(count)
    self.CountLabel.color = color
end

--@region UIEvent

function LuaKanYiDaoEnterWnd:OnEnterButtonClick()
	Gac2Gas.Double11KYD_EnterPlay()
end

--@endregion UIEvent

