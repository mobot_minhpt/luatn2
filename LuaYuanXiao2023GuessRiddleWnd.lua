local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local CChatHelper = import "L10.UI.CChatHelper"
local EChatPanel = import "L10.Game.EChatPanel"
local CButton = import "L10.UI.CButton"

LuaYuanXiao2023GuessRiddleWnd = class()

LuaYuanXiao2023GuessRiddleWnd.s_npcEngineId = nil
LuaYuanXiao2023GuessRiddleWnd.s_riddleId = nil
LuaYuanXiao2023GuessRiddleWnd.s_senderId = nil
LuaYuanXiao2023GuessRiddleWnd.s_senderName = nil

RegistClassMember(LuaYuanXiao2023GuessRiddleWnd, "Input")
RegistClassMember(LuaYuanXiao2023GuessRiddleWnd, "QuestionLabel")
RegistClassMember(LuaYuanXiao2023GuessRiddleWnd, "NameLabel")
RegistClassMember(LuaYuanXiao2023GuessRiddleWnd, "AnswerLabel")
RegistClassMember(LuaYuanXiao2023GuessRiddleWnd, "RightLabel")
RegistClassMember(LuaYuanXiao2023GuessRiddleWnd, "WrongLabel")
RegistClassMember(LuaYuanXiao2023GuessRiddleWnd, "AnsweringLabel")
RegistClassMember(LuaYuanXiao2023GuessRiddleWnd, "ConfirmButton")
RegistClassMember(LuaYuanXiao2023GuessRiddleWnd, "HelpButton")
RegistClassMember(LuaYuanXiao2023GuessRiddleWnd, "WorldChannelButton")
RegistClassMember(LuaYuanXiao2023GuessRiddleWnd, "BanghuiChannelButton")
RegistClassMember(LuaYuanXiao2023GuessRiddleWnd, "EnterChatButton")
RegistClassMember(LuaYuanXiao2023GuessRiddleWnd, "HelpChoices")
RegistClassMember(LuaYuanXiao2023GuessRiddleWnd, "BgMask")

RegistClassMember(LuaYuanXiao2023GuessRiddleWnd, "m_helpString")
RegistClassMember(LuaYuanXiao2023GuessRiddleWnd, "m_questionState")


function LuaYuanXiao2023GuessRiddleWnd:Awake()
    self.m_helpString = ""
    self.m_questionState = 0

    self.Input = self.transform:Find("ShowArea/Puzzle/AnswerArea/WriteArea/Input"):GetComponent(typeof(UIInput))
    self.QuestionLabel = self.transform:Find("ShowArea/Puzzle/QuestionArea/QuestionLabel"):GetComponent(typeof(UILabel))
    self.QuestionLabel.pivot = UIWidget.Pivot.Top
    LuaUtils.SetLocalPositionY(self.QuestionLabel.transform:Find("prefixLabel"), -15)
    self.NameLabel = self.transform:Find("ShowArea/Puzzle/QuestionArea/NameLabel"):GetComponent(typeof(UILabel))
    self.AnswerLabel = self.transform:Find("ShowArea/Puzzle/AnswerArea/WriteArea/Input/Label"):GetComponent(typeof(UILabel))
    self.RightLabel = self.transform:Find("ShowArea/Puzzle/QuestionArea/Right_Label").gameObject
    self.WrongLabel = self.transform:Find("ShowArea/Puzzle/QuestionArea/Wrong_Label").gameObject
    self.AnsweringLabel = self.transform:Find("ShowArea/Puzzle/QuestionArea/Answering_Label").gameObject
    self.ConfirmButton = self.transform:Find("ShowArea/Puzzle/AnswerArea/WriteArea/ConfirmButton"):GetComponent(typeof(CButton))
    self.HelpButton = self.transform:Find("ShowArea/Puzzle/QuestionArea/HelpButton"):GetComponent(typeof(CButton))
    --self.BgMask = self.transform:Find("BgPanel/BgMask").gameObject

    if System.String.IsNullOrEmpty(LuaYuanXiao2023GuessRiddleWnd.s_senderName) then
        --LuaUtils.SetLocalPositionY(self.WorldChannelButton.transform, 45)
        --LuaUtils.SetLocalPositionY(self.BanghuiChannelButton.transform, -45)
        --self.HelpChoices:GetComponent(typeof(UISprite)).height = 200
        --self.EnterChatButton:SetActive(false)
    end

    UIEventListener.Get(self.ConfirmButton.gameObject).onClick = LuaUtils.VoidDelegate(function (go)
        self:OnConfirmButtonClick()
    end)
    UIEventListener.Get(self.HelpButton.gameObject).onClick = LuaUtils.VoidDelegate(function (go)
        self:OnHelpButtonClick()
    end)
    --[[UIEventListener.Get(self.BgMask).onClick = LuaUtils.VoidDelegate(function (go)
        self.BgMask:SetActive(false)
        CUIManager.CloseUI(CUIResources.SocialWnd)
    end)--]]
    UIEventListener.Get(self.Input.gameObject).onSelect = LuaUtils.BoolDelegate(function (go, stat)
        if stat and self.WrongLabel.activeSelf then 
            self.m_questionState = 0
            self.Input.value = ""
            self:ShowPuzzle()
        end
    end)
end

function LuaYuanXiao2023GuessRiddleWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncGuessRiddleResult2023", self, "SyncGuessRiddleResult2023")
end

function LuaYuanXiao2023GuessRiddleWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncGuessRiddleResult2023", self, "SyncGuessRiddleResult2023")
end

function LuaYuanXiao2023GuessRiddleWnd:Init()
    self:ShowPuzzle()
end

function LuaYuanXiao2023GuessRiddleWnd:OnDestroy()
    LuaYuanXiao2023GuessRiddleWnd.s_npcEngineId = nil
    LuaYuanXiao2023GuessRiddleWnd.s_riddleId = nil
    LuaYuanXiao2023GuessRiddleWnd.s_senderId = nil
    LuaYuanXiao2023GuessRiddleWnd.s_senderName = nil
end

function LuaYuanXiao2023GuessRiddleWnd:Update()
    --[[if not CUIManager.IsLoaded(CUIResources.SocialWnd) and not CUIManager.IsLoading(CUIResources.SocialWnd) then
        self.BgMask:SetActive(false)
    end--]]
end

function LuaYuanXiao2023GuessRiddleWnd:OnConfirmButtonClick()
    if not LuaYuanXiao2023GuessRiddleWnd.s_npcEngineId or not LuaYuanXiao2023GuessRiddleWnd.s_riddleId then return end
    local str = self.Input.value
    if str and str ~= "" then
        str = string.sub(StringTrim(str), 1, 10)
        Gac2Gas.RequestGuessRiddle2023(LuaYuanXiao2023GuessRiddleWnd.s_npcEngineId, LuaYuanXiao2023GuessRiddleWnd.s_riddleId, str)
    else
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请先输入答案!"))
    end
end

function LuaYuanXiao2023GuessRiddleWnd:OnHelpButtonClick()
    --self.HelpChoices:SetActive(not self.HelpChoices.activeSelf)
end


function LuaYuanXiao2023GuessRiddleWnd:ShowPuzzle()
    if not LuaYuanXiao2023GuessRiddleWnd.s_riddleId then return end
    local question = YuanXiao_QuestionPool2.GetData(LuaYuanXiao2023GuessRiddleWnd.s_riddleId)
    if not question then UIManager.CloseUI(CLuaUIResources.YuanXiao2023GuessRiddleWnd) end

    self.Input.characterLimit = CUICommonDef.GetStrByteLength(question.Answer) * 8
    self.QuestionLabel.text = question.Question
    if System.String.IsNullOrEmpty(LuaYuanXiao2023GuessRiddleWnd.s_senderName) then
        self.NameLabel.text = LocalString.GetString("出题人：上元仙子")

    else
        self.NameLabel.text = LocalString.GetString("出题人：")..LuaYuanXiao2023GuessRiddleWnd.s_senderName
    end
    
    self.m_helpString = LocalString.GetString("【元宵灯谜求助】")..question.Question

    if self.m_questionState == 0 then
        self.AnsweringLabel.gameObject:SetActive(true)
        self.RightLabel.gameObject:SetActive(false)
        self.WrongLabel.gameObject:SetActive(false)

        self.Input.enabled = true
        self.ConfirmButton.Enabled = true
        self.HelpButton.Enabled = true
    elseif self.m_questionState == 1 then
        self.AnsweringLabel.gameObject:SetActive(false)
        self.RightLabel.gameObject:SetActive(true)
        self.WrongLabel.gameObject:SetActive(false)

        self.Input.enabled = false
        self.ConfirmButton.Enabled = false
        self.HelpButton.Enabled = false
        UIEventListener.Get(self.Input.gameObject).onSelect = nil
    elseif self.m_questionState == 2 then
        self.AnsweringLabel.gameObject:SetActive(false)
        self.RightLabel.gameObject:SetActive(false)
        self.WrongLabel.gameObject:SetActive(true)

        --self.Input.enabled = false
        --self.ConfirmButton.Enabled = false
        --self.HelpButton.Enabled = false
    end
end

function LuaYuanXiao2023GuessRiddleWnd:SyncGuessRiddleResult2023(riddleId, isCorrect)
    if riddleId ~= LuaYuanXiao2023GuessRiddleWnd.s_riddleId then return end
    self.m_questionState = isCorrect and 1 or 2
    self:ShowPuzzle()
end

