local String					= import "System.String"
local CItemInfoMgr				= import "L10.UI.CItemInfoMgr"
local AlignType					= import "L10.UI.CItemInfoMgr+AlignType"
local CItemChooseMgr			= import "L10.UI.CItemChooseMgr"
local CUITexture				= import "L10.UI.CUITexture"
local MessageWndManager			= import "L10.UI.MessageWndManager"
local CUIManager                = import "L10.UI.CUIManager"
local CUICommonDef              = import "L10.UI.CUICommonDef"
local CItemMgr					= import "L10.Game.CItemMgr"
local EnumItemPlace				= import "L10.Game.EnumItemPlace"
local CClientMainPlayer         = import "L10.Game.CClientMainPlayer"
local CommonDefs                = import "L10.Game.CommonDefs"
local DelegateFactory		    = import "DelegateFactory"
local UIEventListener		    = import "UIEventListener"
local UILabel                   = import "UILabel"
local UISprite                  = import "UISprite"
local LocalString               = import "LocalString"

local CCommonItemSelectCellData = import "L10.UI.CCommonItemSelectCellData"

--五色丝拓本界面
CLuaDwWusesiTaben2020Wnd = class()

RegistClassMember(CLuaDwWusesiTaben2020Wnd,"instruction")
RegistClassMember(CLuaDwWusesiTaben2020Wnd,"okButton")
RegistClassMember(CLuaDwWusesiTaben2020Wnd,"cancelButton")
RegistClassMember(CLuaDwWusesiTaben2020Wnd,"weddingring")
RegistClassMember(CLuaDwWusesiTaben2020Wnd,"equip")
RegistClassMember(CLuaDwWusesiTaben2020Wnd,"m_TargetId")--目标道具id

CLuaDwWusesiTaben2020Wnd.OriID = nil --五色丝道具id

CLuaDwWusesiTaben2020Wnd.TitleStr = nil
CLuaDwWusesiTaben2020Wnd.WndTitleStr = nil
CLuaDwWusesiTaben2020Wnd.ConfirmMsg = nil
CLuaDwWusesiTaben2020Wnd.Gac2Gas = nil

function CLuaDwWusesiTaben2020Wnd:Awake()
    self.m_TargetId = nil
    self.instruction = self.transform:Find("Anchor/Instruction"):GetComponent(typeof(UILabel))
    self.okButton = self.transform:Find("Anchor/OKButton").gameObject
    self.cancelButton = self.transform:Find("Anchor/CancelButton").gameObject

    UIEventListener.Get(self.okButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnOkButtonClick(go)
    end)

    if CLuaDwWusesiTaben2020Wnd.TitleStr then
        self.instruction.text = CLuaDwWusesiTaben2020Wnd.TitleStr
    end
    if CLuaDwWusesiTaben2020Wnd.WndTitleStr then
        self.transform:Find("Wnd_Bg_Secondary_2/TitleLabel"):GetComponent(typeof(UILabel)).text = CLuaDwWusesiTaben2020Wnd.WndTitleStr
    end
end

function CLuaDwWusesiTaben2020Wnd:Init()
    self:InitTabenInfo(CLuaDwWusesiTaben2020Wnd.OriID)
    self:InitTargetInfo(nil)
end

function CLuaDwWusesiTaben2020Wnd:OnDestroy()
    CLuaDwWusesiTaben2020Wnd.TitleStr = nil
    CLuaDwWusesiTaben2020Wnd.WndTitleStr = nil
    CLuaDwWusesiTaben2020Wnd.ConfirmMsg = nil
    CLuaDwWusesiTaben2020Wnd.Gac2Gas = nil
end

function CLuaDwWusesiTaben2020Wnd:OnOkButtonClick(go) 
    if self.m_TargetId == nil then
        g_MessageMgr:ShowMessage("Taben_Equip_Material_Not_Exist")
    else
        local msg = ""
        if CLuaDwWusesiTaben2020Wnd.ConfirmMsg then
            msg = CLuaDwWusesiTaben2020Wnd.ConfirmMsg
        else
            msg = CItemMgr.Inst:GetItemInfo(CLuaDwWusesiTaben2020Wnd.OriID).templateId == DuanWu2020_Setting.GetData().QingXiWuSeSiTemplateId
                and "QXWUSESI_TABEN_CONFIRM" or "WUSESI_TABEN_CONFIRM"
        end
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage(msg),
            DelegateFactory.Action(function() self:DoTaben() end),nil, nil, nil, false)
    end
end
function CLuaDwWusesiTaben2020Wnd:DoTaben()
    local s = CItemMgr.Inst:GetItemInfo(CLuaDwWusesiTaben2020Wnd.OriID)
    local t = CItemMgr.Inst:GetItemInfo(self.m_TargetId)
	--发送服务器
    if CLuaDwWusesiTaben2020Wnd.Gac2Gas then
        Gac2Gas[CLuaDwWusesiTaben2020Wnd.Gac2Gas](s.pos,s.itemId,t.pos,t.itemId)
    else
        Gac2Gas.Request2020CombianWuSeSiAndBangle(s.pos,s.itemId,t.pos,t.itemId)
    end
    CUIManager.CloseUI(CLuaUIResources.DwWusesiTaben2020Wnd)
end

function CLuaDwWusesiTaben2020Wnd:InitTabenInfo(itemId)

	local ring = CItemMgr.Inst:GetById(itemId)
	if ring == nil then return end

    local transform = FindChild(self.transform,"WeddingRing")
    local icon = transform:Find("Texture"):GetComponent(typeof(CUITexture))
    local quality = transform:Find("quality"):GetComponent(typeof(UISprite))
    local bind = transform:Find("Bind"):GetComponent(typeof(UISprite))
    local name = transform:Find("Name"):GetComponent(typeof(UILabel))

    

    icon:LoadMaterial(ring.Icon)
	if ring.Equip ~= nil then
		quality.spriteName = CUICommonDef.GetItemCellBorder(ring.Equip.QualityType)
	end
    bind.spriteName = ring.BindOrEquipCornerMark
    name.text = ring.ColoredName
    

    UIEventListener.Get(transform.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemInfo(ring, false, nil, AlignType.Default, 0, 0, 0, 0, 0) 
    end)
end

function CLuaDwWusesiTaben2020Wnd:InitTargetInfo(itemId)
    local transform = FindChild(self.transform,"AimRing")
    local icon = transform:Find("Texture"):GetComponent(typeof(CUITexture))
    local quality = transform:Find("quality"):GetComponent(typeof(UISprite))
    local bind = transform:Find("Bind"):GetComponent(typeof(UISprite))
    local add = transform:Find("Add").gameObject
    local name = transform:Find("Name"):GetComponent(typeof(UILabel))

    UIEventListener.Get(transform.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OpenEquipSelectWnd()
    end)

    if not itemId then
        icon.material = nil
        add:SetActive(true)
        quality.spriteName = CUICommonDef.GetItemCellBorder()
        bind.spriteName = ""
        name.text = "[c][acf8ff]"..LocalString.GetString("添加手镯")
        return
    end

    local ring = CItemMgr.Inst:GetById(itemId)
    add:SetActive(false)

    icon:LoadMaterial(ring.Icon)
    quality.spriteName = CUICommonDef.GetItemCellBorder(ring.Equip.QualityType)
    bind.spriteName = ring.BindOrEquipCornerMark
    name.text = ring.ColoredName
end

function CLuaDwWusesiTaben2020Wnd:OpenEquipSelectWnd( )
    if not CClientMainPlayer.Inst then return end
    local title = LocalString.GetString("选择要制作拓本的手镯")
    local datas = CreateFromClass(MakeGenericClass(List, CCommonItemSelectCellData))
    local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    do
        local i = 1
        while i <= bagSize do
            local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
            if not (id==nil or id=="") then
                local equip = CItemMgr.Inst:GetById(id)
                local templateId = equip.TemplateId

                if equip ~= nil and equip.IsBinded and equip.IsEquip then
                    local template = EquipmentTemplate_Equip.GetData(templateId)
                    if template.Type == EnumBodyPosition_lua.Bracelet then
                        local data = CreateFromClass(CCommonItemSelectCellData, equip.Id, equip.TemplateId, equip.IsBinded, equip.Amount)
                        CommonDefs.ListAdd(datas, typeof(CCommonItemSelectCellData), data)
                    end
                end
            end
            i = i + 1
        end
    end

    if datas.Count == 0 then
        g_MessageMgr:ShowMessage("WUSESI_TABEN_HAVE_NO_BRACELET")
    else
        local initfunc = function()
            return datas
        end
        local selectfunc = function(itemId, templateId)
            self.m_TargetId = itemId
            local equipment = EquipmentTemplate_Equip.GetData(templateId)
            if not (equipment ~= nil and equipment.Type == EnumBodyPosition_lua.Bracelet) then
                return
            end
            self:InitTargetInfo(itemId)
        end
        LuaCommonItemSelectMgr.ShowSelectWnd(title,nil,initfunc,selectfunc)
    end
end
