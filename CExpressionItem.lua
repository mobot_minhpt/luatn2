-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CExpressionItem = import "L10.UI.CExpressionItem"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local CIMMgr = import "L10.Game.CIMMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local ExpressionHead_Expression = import "L10.Game.ExpressionHead_Expression"
CExpressionItem.m_Init_CS2LuaHook = function (this, expression) 
    this.mExpression = expression
    if CClientMainPlayer.Inst == nil then
        this.descLabel.text = nil
        this.icon:Clear()
        this.currentMark:SetActive(false)
        return
    end
    local expressionId = CIMMgr.Inst:Expression2ExpressionId(expression, EnumToInt(CClientMainPlayer.Inst.Class), EnumToInt(CClientMainPlayer.Inst.Gender))
    local data = ExpressionHead_Expression.GetData(expressionId)
    if data ~= nil then
        this.mTemplateId = data.ItemID
        this.descLabel.text = data.Descrition
        if expression == CClientMainPlayer.Inst.BasicProp.Expression then
            this.currentMark:SetActive(true)
        else
            this.currentMark:SetActive(false)
        end

        local portraitName = CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, expression)
        if CExpressionMgr.Inst:HasGetExpression(expression) then
            this.icon:LoadNPCPortrait(portraitName, false)
            this.getNode:SetActive(false)
        else
            this.icon:LoadNPCPortrait(portraitName, true)
            this.getNode:SetActive(true)
        end
    else
        this.icon:Clear()
        this.descLabel.text = nil
        this.currentMark:SetActive(false)
    end
end
CExpressionItem.m_UpdateExpressionState_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    if this.mExpression == CClientMainPlayer.Inst.BasicProp.Expression then
        this.currentMark:SetActive(true)
    else
        this.currentMark:SetActive(false)
    end
end
