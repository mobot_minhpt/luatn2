require("common/common_include")
require("ui/biwudahui/LuaBWDHGameVideoMgr")
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local AlignType=import "L10.UI.CPopupMenuInfoMgr+AlignType"
local LuaGameObject=import "LuaGameObject"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local Gac2Gas=import "L10.Game.Gac2Gas"
local MessageWndManager=import "L10.UI.MessageWndManager"
local CBWDHGameVideoMgr=import "L10.Game.CBWDHGameVideoMgr"
local MessageMgr=import "L10.Game.MessageMgr"
local QnTableItem=import "L10.UI.QnTableItem"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local CGameReplayMgr = import "L10.Game.CGameReplayMgr"
local Constants=import "L10.Game.Constants"

CLuaBWDHGameVideoWnd=class()
RegistClassMember(CLuaBWDHGameVideoWnd,"m_Time")
RegistClassMember(CLuaBWDHGameVideoWnd,"m_TimeLabel")
RegistClassMember(CLuaBWDHGameVideoWnd,"m_TimeTable")
RegistClassMember(CLuaBWDHGameVideoWnd,"m_TimeInitTable")

RegistClassMember(CLuaBWDHGameVideoWnd,"m_Group")
RegistClassMember(CLuaBWDHGameVideoWnd,"m_GroupLabel")
RegistClassMember(CLuaBWDHGameVideoWnd,"m_GroupTable")
RegistClassMember(CLuaBWDHGameVideoWnd,"m_GroupInitTable")


RegistClassMember(CLuaBWDHGameVideoWnd,"m_NameInput")
RegistClassMember(CLuaBWDHGameVideoWnd,"m_TableViewDataSource")
RegistClassMember(CLuaBWDHGameVideoWnd,"m_TableView")

RegistClassMember(CLuaBWDHGameVideoWnd,"m_AllDataList")
RegistClassMember(CLuaBWDHGameVideoWnd,"m_DataList")
RegistClassMember(CLuaBWDHGameVideoWnd,"m_VideoFileName")
RegistClassMember(CLuaBWDHGameVideoWnd,"m_SelectedRecord")

RegistClassMember(CLuaBWDHGameVideoWnd,"m_DeleteButton")
RegistClassMember(CLuaBWDHGameVideoWnd,"m_WatchButton")


RegistClassMember(CLuaBWDHGameVideoWnd,"m_FilterYear")
RegistClassMember(CLuaBWDHGameVideoWnd,"m_FilterGroup")

RegistClassMember(CLuaBWDHGameVideoWnd,"m_IsInSearchState")
RegistClassMember(CLuaBWDHGameVideoWnd,"m_SearchText")
RegistClassMember(CLuaBWDHGameVideoWnd,"m_DefaultYear")


function CLuaBWDHGameVideoWnd:Init()
    self.m_GroupInitTable={
        LocalString.GetString("新锐组"),
        LocalString.GetString("英武组"),
        LocalString.GetString("神勇组"),
        LocalString.GetString("天罡组"),
        LocalString.GetString("天元组")}
    self.m_GroupTable={}
    for k,v in ipairs(self.m_GroupInitTable) do
        table.insert( self.m_GroupTable,v )
    end
    self.m_TimeInitTable={}
    self.m_TimeTable={}
    
    local g = LuaGameObject.GetChildNoGC(self.transform,"CloseButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CUIResources.BWDHGameVideoWnd)
    end)

    self.m_TimeLabel=LuaGameObject.GetChildNoGC(self.transform,"TimeLabel").label
    self.m_GroupLabel=LuaGameObject.GetChildNoGC(self.transform,"GroupLabel").label
    self.m_NameInput=LuaGameObject.GetChildNoGC(self.transform,"NameInput").input
    CommonDefs.AddEventDelegate(self.m_NameInput.onChange, DelegateFactory.Action(function ()
        self:OnInputChange(self.m_NameInput.value)
    end))

    self.m_DeleteButton=LuaGameObject.GetChildNoGC(self.transform,"DeleteButton").gameObject
    self.m_DeleteButton:SetActive(false)
    local g = LuaGameObject.GetChildNoGC(self.transform,"DeleteButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        --删除文件
        local index=self.m_TableView.currentSelectRow
        if index>-1 then
            if index<#self.m_DataList then
                if self.m_DataList[index+1] then
                    local fileName=self.m_DataList[index+1].FileName
                    CGameReplayMgr.Instance:DeleteFile(fileName)
                    self:RefreshItem(fileName)
                end
            end
        end
    end)
    self.m_WatchButton=LuaGameObject.GetChildNoGC(self.transform,"WatchButton").gameObject
    self.m_WatchButton:SetActive(false)
    UIEventListener.Get(self.m_WatchButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:WatchVideo()
    end)
    -- TimeChangeButton
    local function SelectTimeAction(index)
        if self.m_TimeTable and #self.m_TimeTable>index then
            self.m_FilterYear=self.m_TimeTable[index+1]
            self:RefreshList()
        end
    end
    local selectTime=DelegateFactory.Action_int(SelectTimeAction)
    local g = LuaGameObject.GetChildNoGC(self.transform,"TimeChangeButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        local t={}
        --如果没有搜索内容
        if not self.m_IsInSearchState then
            self:InitTimes(false)
        else
            self:InitTimes(true)                
        end
        if #self.m_TimeTable>0 then
            for i=1,#self.m_TimeTable do
                local item=PopupMenuItemData(tostring(self.m_TimeTable[i]),selectTime,false,nil)
                table.insert(t, item)
            end
        end
        local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
        CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType.Bottom,1,nil,600,true,296);
    end)
    local function SelectGroupAction(index)
        if self.m_GroupTable and #self.m_GroupTable>index then
            self.m_FilterGroup=index+1
            self:RefreshList()
        end
    end
    local selectGroup=DelegateFactory.Action_int(SelectGroupAction)
    local g = LuaGameObject.GetChildNoGC(self.transform,"GroupChangeButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        local t={}
        if not self.m_IsInSearchState then
            self:InitGroups(false)
        else
            self:InitGroups(true)
        end
        --如果没有搜索内容
        for i=1,#self.m_GroupTable do
            local item=PopupMenuItemData(tostring(self.m_GroupTable[i]),selectGroup,false,nil)
            table.insert(t, item)
        end
        local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
        CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType.Bottom,1,nil,600,true,296)
    end)
    local g = LuaGameObject.GetChildNoGC(self.transform,"SearchButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        if cs_string.IsNullOrEmpty(self.m_NameInput.value) then
            --请输入搜索内容
        else
            self.m_SearchText=self.m_NameInput.value
            self.m_IsInSearchState=true
            self:RefreshList()
        end
    end)

    self.m_TableView=LuaGameObject.GetChildNoGC(self.transform,"ContentTable").tableView
    local function InitItem(item,index)
        -- print(item,index)
        if index % 2 == 1 then
            item:SetBackgroundTexture(Constants.NewEvenBgSprite)
        else
            item:SetBackgroundTexture(Constants.NewOddBgSprite)
        end
        self:InitItem(item,index)
    end
    self.m_TableViewDataSource=DefaultTableViewDataSource.CreateByCount(0,InitItem)
    self.m_TableView.m_DataSource=self.m_TableViewDataSource
    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        -- print("select ",row)
        self:OnSelectAtRow(row)
    end)

    --初始化列表
    self.m_DefaultYear=CServerTimeMgr.Inst:GetZone8Time().Year
    if not self.m_FilterGroup then
        self.m_FilterGroup=1
    end
    self.m_GroupLabel.text=self.m_GroupTable[self.m_FilterGroup]
    if not self.m_FilterYear then
        self.m_FilterYear=self.m_DefaultYear
    end
    self.m_TimeLabel.text=tostring(self.m_FilterYear)

    self.m_SelectedRecord=nil
    CBWDHGameVideoMgr.DownloadBWDHPlayList()
end

function  CLuaBWDHGameVideoWnd:OnInputChange( text)
    -- print("OnInputChange",text)
    --如果没有输入内容 则必然进入搜索状态
    if cs_string.IsNullOrEmpty(text) then
        self.m_IsInSearchState=false
        self.m_SearchText=nil
        self:InitTimes(false)
        self:InitGroups(false)
        -- print("OnInputChange1",self.m_FilterGroup,self.m_FilterYear)
        local intable=false
        for k, v in ipairs(self.m_GroupTable) do
            -- print("item",k)
            if k == self.m_FilterGroup then
                intable=true
            end
        end
        if not intable then
            self.m_FilterGroup=1
        end
        local intable2=false
        for _, value in ipairs(self.m_TimeTable) do
            if value == self.m_FilterYear then
                intable2=true
            end
        end
        if not intable2 then
            if #self.m_TimeTable>0 then
                self.m_FilterYear=self.m_TimeTable[#self.m_TimeTable]
            else
                self.m_FilterYear=self.m_DefaultYear
            end
        end
        -- print("OnInputChange2",self.m_FilterGroup,self.m_FilterYear)
        self:RefreshList()
    end  
end

function CLuaBWDHGameVideoWnd:SearchTeam()
    local name=self.m_NameInput.value

    self:InitGroups(true)
    self:InitTimes(true)

end

function CLuaBWDHGameVideoWnd:InitGroups(showAll)
    self.m_GroupTable={}
    for k,v in ipairs(self.m_GroupInitTable) do
        table.insert( self.m_GroupTable,v )
    end
    if showAll then
        table.insert( self.m_GroupTable,LocalString.GetString("所有组"))
    end
end
function CLuaBWDHGameVideoWnd:InitTimes(showAll)
    self.m_TimeTable={}
    for k,v in pairs(self.m_TimeInitTable) do
        table.insert( self.m_TimeTable,v )
    end
    table.sort(self.m_TimeTable,function(a,b)
        return a<b
    end)
    if showAll then
        table.insert( self.m_TimeTable,LocalString.GetString("所有年份"))
    end
end

function CLuaBWDHGameVideoWnd:OnClickTeam1(go)
    CLuaBWDHGameVideoMgr.m_TeamInfo=nil
    local cmp=go.transform.parent.gameObject:GetComponent(typeof(QnTableItem))
    if cmp and #self.m_DataList>cmp.Row then
        local data=self.m_DataList[cmp.Row+1]
        if data and data.TeamInfo then
            CLuaBWDHGameVideoMgr.m_TeamInfo=data.TeamInfo[0]
            CUIManager.ShowUI(CUIResources.BWDHGameVideoMemberWnd)
        end
    end
end
function CLuaBWDHGameVideoWnd:OnClickTeam2(go)
    CLuaBWDHGameVideoMgr.m_TeamInfo=nil
    local cmp=go.transform.parent.gameObject:GetComponent(typeof(QnTableItem))
    if cmp and #self.m_DataList>cmp.Row then
        local data=self.m_DataList[cmp.Row+1]
        if data and data.TeamInfo then
            CLuaBWDHGameVideoMgr.m_TeamInfo=data.TeamInfo[1]
            CUIManager.ShowUI(CUIResources.BWDHGameVideoMemberWnd)
        end
    end
end
function CLuaBWDHGameVideoWnd:InitItem(item,index)
    local tf=item.transform
    local timeLabel=LuaGameObject.GetChildNoGC(tf,"TimeLabel").label
    timeLabel.text=" "
    local team1NameLabel=LuaGameObject.GetChildNoGC(tf,"NameLabel1").label
    team1NameLabel.text=" "
    local result1Sprite=LuaGameObject.GetChildNoGC(tf,"ResultSprite1").sprite
    result1Sprite.spriteName=" "
    local result2Sprite=LuaGameObject.GetChildNoGC(tf,"ResultSprite2").sprite
    result2Sprite.spriteName=" "
    local team2NameLabel=LuaGameObject.GetChildNoGC(tf,"NameLabel2").label
    team2NameLabel.text=" "
    
    -- local cannotwatchMark=LuaGameObject.GetChildNoGC(tf,"CannotWatchMark").gameObject
    -- cannotwatchMark:SetActive(false)
    local downloadingMark=LuaGameObject.GetChildNoGC(tf,"DownloadingMark").gameObject
    downloadingMark:SetActive(false)
    local okMark=LuaGameObject.GetChildNoGC(tf,"OkMark").gameObject
    okMark:SetActive(false)

    CommonDefs.AddOnClickListener(team1NameLabel.gameObject,DelegateFactory.Action_GameObject(function(go)
        self:OnClickTeam1(go)
    end),false)
    CommonDefs.AddOnClickListener(team2NameLabel.gameObject,DelegateFactory.Action_GameObject(function(go)
        self:OnClickTeam2(go)
    end),false)



    if index<#self.m_DataList then
        local record=self.m_DataList[index+1]
        -- if record.TeamInfo.Length==2 then
        if record and record.TeamInfo and record.TeamInfo.Length==2 then
            team1NameLabel.text=record.TeamInfo[0].Name
            team2NameLabel.text=record.TeamInfo[1].Name
            --结果
            if record.Result1==1 then
                result1Sprite.spriteName="common_fight_result_win"
            elseif record.Result1==2 then
                result1Sprite.spriteName="common_fight_result_lose"
            end
            if record.Result2==1 then
                result2Sprite.spriteName="common_fight_result_win"
            elseif record.Result2==2 then
                result2Sprite.spriteName="common_fight_result_lose"
            end
            timeLabel.text=record.TimeDesc

            local isDownloaded=CGameReplayMgr.Instance:IsFileDownloaded(record.FileName)
            if isDownloaded then--已经下载完毕
                okMark:SetActive(true)
                -- cannotwatchMark:SetActive(false)
                downloadingMark:SetActive(false)
            else--没有下载或者下载中
                okMark:SetActive(false)
                if CGameReplayMgr.Instance:IsFileDownloading(record.FileName) then
                    downloadingMark:SetActive(true)
                    -- cannotwatchMark:SetActive(false)
                else
                    downloadingMark:SetActive(false)
                    -- cannotwatchMark:SetActive(true)
                end
            end
        end
    end
end
function  CLuaBWDHGameVideoWnd:OnSelectAtRow(row )
    self.m_WatchButton:SetActive(true)
    -- body
    self.m_VideoFileName=""
    self.m_SelectedRecord=nil
    if self.m_DataList and row<#self.m_DataList then
        local record=self.m_DataList[row+1]
        if record and record.TeamInfo and record.TeamInfo.Length==2 then
            local isDownloaded=CGameReplayMgr.Instance:IsFileDownloaded(record.FileName)
            self.m_DeleteButton:SetActive(isDownloaded)
            self.m_VideoFileName=record.FileName
            self.m_SelectedRecord=record
            -- print(self.m_VideoFileName)
        end
    end
    local label=LuaGameObject.GetChildNoGC(self.m_WatchButton.transform,"Label").label
    local sprite=LuaGameObject.GetLuaGameObjectNoGC(self.m_WatchButton.transform).sprite
    if label and sprite then
        if CGameReplayMgr.Instance:IsFileDownloaded(self.m_VideoFileName) then
            --进入观战
            label.effectColor=Color(0.45,0.27,0.12)
            label.text=LocalString.GetString("进入观战")
            sprite.spriteName="common_btn_01_yellow"
            
        else
            --观战
            label.effectColor=Color(0.12,0.28,0.45)
            label.text=LocalString.GetString("观战")
            sprite.spriteName="common_btn_01_blue"
        end
    end
end


--拿到所有数据之后 进行分析
function CLuaBWDHGameVideoWnd:OnGetAllData()
    self.m_TimeInitTable={}

    self.m_AllDataList = {}
    local lookup = {}
    --同一个月份，同一个stage，只取round最大的
    if CBWDHGameVideoMgr.videoRecord then
        local len=CBWDHGameVideoMgr.videoRecord.Count
        if len>0 then
            for i=0,len-1 do
                local record = CBWDHGameVideoMgr.videoRecord[i]
                local year=CBWDHGameVideoMgr.videoRecord[i].Year
                self.m_TimeInitTable[year]=year

                if not lookup[record.Stage] then
                    lookup[record.Stage] = {}
                end
                local pre = lookup[record.Stage][record.date]
                if not pre then
                    lookup[record.Stage][record.date] = record
                else
                    if pre.Round<record.Round then
                        lookup[record.Stage][record.date] = record
                    end
                end
            end
        end
    end
    for key, value in pairs(lookup) do
        for key2, value2 in pairs(value) do
            table.insert(self.m_AllDataList, value2)
        end
    end
    table.sort(self.m_AllDataList,function(a,b)
        return a.PlayStartTime>b.PlayStartTime
    end)

    self.m_TimeTable={}
    for k,v in pairs(self.m_TimeInitTable) do
        table.insert( self.m_TimeTable,v )
    end

    if #self.m_TimeTable>0 then
        table.sort(self.m_TimeTable,function(a,b)
            return a<b
        end)
        self.m_FilterYear=self.m_TimeTable[#self.m_TimeTable]
    else
        self.m_FilterYear=self.m_DefaultYear
    end
    self.m_FilterGroup=1

    self:RefreshList()
end


function CLuaBWDHGameVideoWnd:RefreshList()
    if not self.m_FilterGroup then
        self.m_FilterGroup=1
    end
    self.m_GroupLabel.text=self.m_GroupTable[self.m_FilterGroup]
    if not self.m_FilterYear then
        self.m_FilterYear=self.m_DefaultYear
    end
    self.m_TimeLabel.text=tostring(self.m_FilterYear)

    local isSearch=false
    if self.m_IsInSearchState then
        if not cs_string.IsNullOrEmpty(self.m_SearchText) then
            isSearch=true
        end
    end
    -- end

    self.m_DataList={}

    if self.m_AllDataList then
        for i=1,#self.m_AllDataList do
            local check=true
            local record=self.m_AllDataList[i]
            if self.m_FilterYear==LocalString.GetString("所有年份") then
            else
                if record and record.Year~=self.m_FilterYear then
                    check=false
                end
            end
            --逻辑依赖表 所有组
            if self.m_FilterGroup==6 then
            else
                if record and record.Stage~=self.m_FilterGroup then
                    check=false
                end
            end
            if isSearch then
                if not record:IsMatchSearch(self.m_SearchText) then
                    check=false
                end
            end
            if check then
                table.insert(self.m_DataList, record)
            end
        end
    end
    self.m_TableViewDataSource.count=#self.m_DataList
    self.m_TableView:ReloadData(true,false)
    self.m_DeleteButton:SetActive(false)
    self.m_WatchButton:SetActive(false)
    if #self.m_DataList>0 then
        self.m_TableView:SetSelectRow(0,true)
    end
end

-- function CLuaBWDHGameVideoWnd:SearchAndRefreshList()

-- end

function CLuaBWDHGameVideoWnd:WatchVideo()
    if self.m_TableView.currentSelectRow<0 then
        return
    end

    if CGameReplayMgr.Instance:IsFileDownloaded(self.m_VideoFileName) then
        Gac2Gas.RequestEnterGameVideoScene(Constants.BiWuWatchTemplateSceneId,self.m_VideoFileName)
        CUIManager.CloseUI(CUIResources.BWDHGameVideoWnd)
    else
        local function onOk()
            local url = CBWDHGameVideoMgr.GetFullFileUrl(self.m_VideoFileName,self.m_SelectedRecord.url)
            CGameReplayMgr.Instance:DownloadFile(self.m_VideoFileName,url,self.m_SelectedRecord.zip)
            --设置状态为下载中
            self:RefreshItem(self.m_VideoFileName)
        end
        --判断是否wifi环境
        if CommonDefs.IsInWifiNetwork then
            local str=MessageMgr.Inst:FormatMessage("BWDH_WatchVideo_NeedDownload",{})
            MessageWndManager.ShowOKCancelMessage(str,DelegateFactory.Action(onOk),nil,LocalString.GetString("下载"),nil,false)
        else
            --传入大小
            local str=MessageMgr.Inst:FormatMessage("BWDH_WatchVideo_NeedDownload_NotWifi",{self:GetCurrentSize()})
            MessageWndManager.ShowOKCancelMessage(str,DelegateFactory.Action(onOk),nil,LocalString.GetString("下载"),nil,false)
        end
    end
end
function CLuaBWDHGameVideoWnd:OnEnable()
    g_ScriptEvent:AddListener("GameVideoListDownloaded", self, "OnGameVideoListDownloaded")
    g_ScriptEvent:AddListener("GameVideoDownloaded", self, "OnGameVideoDownloaded")
end

function CLuaBWDHGameVideoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GameVideoListDownloaded", self, "OnGameVideoListDownloaded")
    g_ScriptEvent:RemoveListener("GameVideoDownloaded", self, "OnGameVideoDownloaded")
end
function  CLuaBWDHGameVideoWnd:OnGameVideoDownloaded( args )
    local success=args[0]
    local fileName=args[1]

    self:RefreshItem(fileName)
end
function CLuaBWDHGameVideoWnd:RefreshItem(fileName)
    --遍历 然后设置状态
    local index=-1
    for k,v in ipairs(self.m_DataList) do
        if v.FileName==fileName then
            index=k-1
        end
    end
    if index>-1 then
        self:InitItem(self.m_TableView:GetItemAtRow(index),index)
        --选中的item可能刷新了
        if index==self.m_TableView.currentSelectRow then
            self:OnSelectAtRow(self.m_TableView.currentSelectRow)
        end
    end
end
function CLuaBWDHGameVideoWnd:GetCurrentSize()
    if self.m_TableView.currentSelectRow>-1 then
        local data=self.m_DataList[self.m_TableView.currentSelectRow+1]
        if data then
            return SafeStringFormat3("%.2f",data.Size/1000000)
        end
    end
    return 0
end

function  CLuaBWDHGameVideoWnd:OnGameVideoListDownloaded( args )
    local success=args[0]
    local playName=args[1]
    if success then
        if playName=="bwdh" then
            self:OnGetAllData()
        end
    end
end
return CLuaBWDHGameVideoWnd
