local CCommonUITween = import "L10.UI.CCommonUITween"

local QnTableView = import "L10.UI.QnTableView"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CItem = import "L10.Game.CItem"
local QualityColor = import "L10.Game.QualityColor"
local EnumQualityType = import "L10.Game.EnumQualityType"

LuaHengYuDangKouZhanLiPinWnd = class()
RegistClassMember(LuaHengYuDangKouZhanLiPinWnd, "GamePlayId") -- 横屿荡寇玩法id
RegistClassMember(LuaHengYuDangKouZhanLiPinWnd, "NumPerPage") -- 每页显示个数
RegistClassMember(LuaHengYuDangKouZhanLiPinWnd, "PageCount")  -- 总页面量
RegistClassMember(LuaHengYuDangKouZhanLiPinWnd, "ShowList")   -- 当前页数据表

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHengYuDangKouZhanLiPinWnd, "PageButton", "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaHengYuDangKouZhanLiPinWnd, "PageLabel", "Label", UILabel)
RegistChildComponent(LuaHengYuDangKouZhanLiPinWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaHengYuDangKouZhanLiPinWnd, "QuestBtn", "QuestBtn", GameObject)
RegistChildComponent(LuaHengYuDangKouZhanLiPinWnd, "DataTimeLabel", "DataTimeLabel", UILabel)
RegistChildComponent(LuaHengYuDangKouZhanLiPinWnd, "Anchor", "Anchor", CCommonUITween)
RegistChildComponent(LuaHengYuDangKouZhanLiPinWnd, "CloseButton", "CloseButton", GameObject)

--@endregion RegistChildComponent end

function LuaHengYuDangKouZhanLiPinWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

-- 根据 物品id / 装备id 获取名称以及颜色
function LuaHengYuDangKouZhanLiPinWnd:GetItemById(lbl, itemid)
    local item = Item_Item.GetData(itemid)
    if item then
        lbl.text = item.Name
        lbl.color = QualityColor.GetRGBValue(CItem.GetQualityType(itemid))
    else -- 不是物品，查询装备
        local equipment = EquipmentTemplate_Equip.GetData(itemid)
        if equipment then
            lbl.text = equipment.Name
            lbl.color = QualityColor.GetRGBValue(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), equipment.Color))
        end
    end
end
-- 初始化战利品项目信息
function LuaHengYuDangKouZhanLiPinWnd:InitItem(tf, i)
    local data = self.ShowList[i + 1]
    self:GetItemById(tf:Find("zhanlipin"):GetComponent(typeof(UILabel)), data.itemTemplateId)
    tf:Find("shuliang"):GetComponent(typeof(UILabel)).text = data.itemCount
    if data.type == 1 then
        tf:Find("jiaoyizhuangtai"):GetComponent(typeof(UILabel)).text = LocalString.GetString("成交")
        tf:Find("jiaoyizhuangtai"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24("69ffb4", 0)
        tf:Find("guishu"):GetComponent(typeof(UILabel)).text = LocalString.GetString("帮会拍卖")
    elseif data.type == 2 then
        tf:Find("jiaoyizhuangtai"):GetComponent(typeof(UILabel)).text = LocalString.GetString("成交")
        tf:Find("jiaoyizhuangtai"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24("69ffb4", 0)
        tf:Find("guishu"):GetComponent(typeof(UILabel)).text = LocalString.GetString("全服拍卖")
    else
        tf:Find("jiaoyizhuangtai"):GetComponent(typeof(UILabel)).text = LocalString.GetString("流拍")
        tf:Find("jiaoyizhuangtai"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24("ff8370", 0)
        tf:Find("guishu"):GetComponent(typeof(UILabel)).text = data.buyername
    end
    -- 奇偶列不同背景
    tf:GetComponent(typeof(UISprite)).spriteName = (i % 2 == 0 and "common_bg_mission_background_n" or "common_bg_mission_background_s")
end
-- 询问战利品第 page 页
function LuaHengYuDangKouZhanLiPinWnd:QueryPage(page)
    Gac2Gas.QueryGamePlayPaiMaiHistory(self.GamePlayId, (page - 1) * self.NumPerPage + 1, page * self.NumPerPage, page)
end
-- 显示战利品第 page 页
function LuaHengYuDangKouZhanLiPinWnd:ShowPage(page)
    self.TableView:ReloadData(true, false) -- 显示列表
    self.PageButton:SetMinMax(1, self.PageCount, 1)
    self.PageButton:SetValue(page, false)
    if self.PageCount == 0 then
        self.PageLabel.text = 0 .. "/" .. 0
    else
        self.PageLabel.text = page .. "/" .. self.PageCount
    end
end
-- 初始化
function LuaHengYuDangKouZhanLiPinWnd:Init()
    -- 关闭按钮绑定关闭动画
    UIEventListener.Get(self.CloseButton).onClick = DelegateFactory.VoidDelegate(function(g)
        self.Anchor:PlayDisapperAnimation(DelegateFactory.Action(function()
            CUIManager.CloseUI("HengYuDangKouZhanLiPinWnd")
        end))
    end)

    self.GamePlayId = HengYuDangKou_Setting.GetData().GamePlayId
    self.NumPerPage = 8

    -- 绑定ShowList到TableView上
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() return #self.ShowList end,
        function(item, i) self:InitItem(item.transform, i) end
    )
    -- 绑定QuestBtn点击事件
    UIEventListener.Get(self.QuestBtn).onClick = DelegateFactory.VoidDelegate(function(g)
        g_MessageMgr:ShowMessage("HengYuDangKou_ZhanLiPinWnd_TIPS")
    end)
    -- 数据展示周期 目前弃用
    -- local startm, startd = 7, 21
    -- local endm, endd = 7, 28
    -- self.DataTimeLabel.text = SafeStringFormat(LocalString.GetString("数据展示周期 %s月%s日-%s月%s日"), startm, startd, endm, endd)

    self.PageButton:SetMinMax(1, 0, 1)
    self.PageLabel.text = ""
    self:QueryPage(1)
    -- 切换页面事件
    self.PageButton.onValueChanged = DelegateFactory.Action_uint(function(page)
        local Max = self.PageButton:GetMaxValue()
        if Max == 0 then
            self.PageLabel.text = 0 .. "/" .. 0
        else
            self.PageLabel.text = page .. "/" .. Max
        end
        self:QueryPage(page)
    end)
end
-- 玩法拍卖查询战利品界面用到的数据
-- Gac2Gas.QueryGamePlayPaiMaiHistory
-- Gas2Gac.QueryGamePlayPaiMaiHistoryResult
function LuaHengYuDangKouZhanLiPinWnd:OnQueryGamePlayPaiMaiHistoryResult(pageNum, totalCount, ResultTbl)
    self.PageCount = math.ceil(totalCount / self.NumPerPage)
    self.ShowList = {}
    local itemtable = {} -- 每个物品的信息
    for k, v in ipairs(ResultTbl) do
        itemtable = {
            itemTemplateId = v[1],
            itemCount = v[2],
            type = v[3],
        }
        if itemtable.type == 3 then
            itemtable.buyername = v[4]
        end
        table.insert(self.ShowList, itemtable)
    end
    self:ShowPage(pageNum)
end
function LuaHengYuDangKouZhanLiPinWnd:OnEnable()
    self.Anchor:PlayAppearAnimation()
    g_ScriptEvent:AddListener("OnQueryGamePlayPaiMaiHistoryResult", self, "OnQueryGamePlayPaiMaiHistoryResult")
end
function LuaHengYuDangKouZhanLiPinWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnQueryGamePlayPaiMaiHistoryResult", self, "OnQueryGamePlayPaiMaiHistoryResult")
end

--@region UIEvent

--@endregion UIEvent

