require("common/lianliankan/lianliankan")

CLuaLianLianKanMgr={}

CLuaLianLianKanMgr.m_Play=nil
CLuaLianLianKanMgr.m_Question=nil
CLuaLianLianKanMgr.m_Answer1=nil
CLuaLianLianKanMgr.m_Answer2=nil
CLuaLianLianKanMgr.m_PlayerInfos=nil
CLuaLianLianKanMgr.m_ResultInfo=nil
CLuaLianLianKanMgr.m_ResultCostTime=0

function CLuaLianLianKanMgr.OnMainPlayerCreated()
    g_ScriptEvent:AddListener("ChessmanObjectSelected", CLuaLianLianKanMgr, "OnChessmanObjectSelected")
end
function CLuaLianLianKanMgr.OnMainPlayerDestroyed()
    if CLuaLianLianKanMgr.m_Play then
        CLuaLianLianKanMgr.m_Play:End()
        CLuaLianLianKanMgr.m_Play=nil
    end
    CLuaLianLianKanMgr.m_PlayerInfos=nil
    CLuaLianLianKanMgr.m_ResultInfo=nil
    g_ScriptEvent:RemoveListener("ChessmanObjectSelected", CLuaLianLianKanMgr, "OnChessmanObjectSelected")
end

--选中
function CLuaLianLianKanMgr:OnChessmanObjectSelected(args)
    local engineId=tonumber(args[0])
    CLuaLianLianKanMgr.m_Play:OnChessmanObjectSelected(engineId)
end
