local DelegateFactory = import "DelegateFactory"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"

LuaWorldCup2022HomeTeamScheduleView = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWorldCup2022HomeTeamScheduleView, "template")
RegistClassMember(LuaWorldCup2022HomeTeamScheduleView, "table")
RegistClassMember(LuaWorldCup2022HomeTeamScheduleView, "scrollView")

function LuaWorldCup2022HomeTeamScheduleView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self.template = self.transform:Find("Template").gameObject
    self.table = self.transform:Find("ScrollView/Table"):GetComponent(typeof(UITable))
    self.scrollView = self.transform:Find("ScrollView"):GetComponent(typeof(UIScrollView))
    self.template:SetActive(false)
end

function LuaWorldCup2022HomeTeamScheduleView:OnEnable()
    g_ScriptEvent:AddListener("WorldCupJingCai_UpdateAllJingCaiData", self, "UpdateAllJingCaiData")
    g_ScriptEvent:AddListener("WorldCupJingCai_SendPeriodAndRoundData", self, "SendPeriodAndRoundData")
    self:UpdateInfo()
end

function LuaWorldCup2022HomeTeamScheduleView:OnDisable()
    g_ScriptEvent:RemoveListener("WorldCupJingCai_UpdateAllJingCaiData", self, "UpdateAllJingCaiData")
    g_ScriptEvent:RemoveListener("WorldCupJingCai_SendPeriodAndRoundData", self, "SendPeriodAndRoundData")
end

function LuaWorldCup2022HomeTeamScheduleView:UpdateAllJingCaiData()
    self:UpdateInfo()
end

function LuaWorldCup2022HomeTeamScheduleView:SendPeriodAndRoundData()
    self:UpdateInfo()
end

function LuaWorldCup2022HomeTeamScheduleView:UpdateInfo()
    local matchInfo = self:GetHomeTeamMatchInfo()

    local num = #matchInfo
    if num == 0 then return end
    self:UpdateChildNum(self.table.transform, self.template, num)

    local homeTeamId = LuaWorldCup2022Mgr.lotteryInfo.jingCaiData.m_HomeTeamId
    for i = num, 1, -1 do
        local id = matchInfo[i].id
        local child = self.table.transform:GetChild(num - i)
        child.gameObject:SetActive(true)

        child:Find("Id"):GetComponent(typeof(UILabel)).text = i
        child:Find("Type"):GetComponent(typeof(UILabel)).text = WorldCup_PlayInfo.GetData(id).MatchName

        local data = LuaWorldCup2022Mgr.lotteryInfo.allRoundResultData[id]
        local home = child:Find("Home")
        local homeIcon = home:Find("Icon"):GetComponent(typeof(CUITexture))
        local homeData = WorldCup_TeamInfo.GetData(data[1])
        homeIcon:LoadMaterial(homeData.Icon)
        home:Find("Name"):GetComponent(typeof(UILabel)).text = homeData.Name
        home:Find("HomeTeam").gameObject:SetActive(homeTeamId == data[1])

        local away = child:Find("Away")
        local awayIcon = away:Find("Icon"):GetComponent(typeof(CUITexture))
        local awayData = WorldCup_TeamInfo.GetData(data[2])
        awayIcon:LoadMaterial(awayData.Icon)
        away:Find("Name"):GetComponent(typeof(UILabel)).text = awayData.Name
        away:Find("HomeTeam").gameObject:SetActive(homeTeamId == data[2])

        LuaUtils.SetLocalPositionZ(homeIcon.transform, (data[6] >= 0 and data[6] < data[7]) and -1 or 0)
        LuaUtils.SetLocalPositionZ(awayIcon.transform, (data[6] >= 0 and data[6] > data[7]) and -1 or 0)

        self:UpdateVS(child, matchInfo[i].matchStartTime, data[6], data[7])

        local time = child:Find("Time"):GetComponent(typeof(UILabel))
        time.text = SafeStringFormat3(LocalString.GetString("%02d月%02d日 周%s\n%02d:%02d"), matchInfo[i].month, matchInfo[i].day, matchInfo[i].week, matchInfo[i].hour, matchInfo[i].min)
    end

    self.table:Reposition()
    self.scrollView:ResetPosition()
end

function LuaWorldCup2022HomeTeamScheduleView:UpdateVS(child, matchStartTime, score1, score2)
    local vs = child:Find("VS")
    local score = vs:Find("Score"):GetComponent(typeof(UILabel))
    local noOpen = vs:Find("NoOpen").gameObject
    local going = vs:Find("Going").gameObject

    score.gameObject:SetActive(false)
    noOpen:SetActive(false)
    going:SetActive(false)

    if CServerTimeMgr.Inst.timeStamp < matchStartTime then
        noOpen:SetActive(true)
    elseif score1 >= 0 and score2 >= 0 then
        score.gameObject:SetActive(true)
        if score1 > score2 then
            score.text = SafeStringFormat3("[FF5050]%d[-] : %d", score1, score2)
        elseif score1 < score2 then
            score.text = SafeStringFormat3("%d : [FF5050]%d[-]", score1, score2)
        else
            score.text = SafeStringFormat3("[FF5050]%d : %d[-]", score1, score2)
        end
    else
        going:SetActive(true)
    end
end

-- 获取主队比赛信息
function LuaWorldCup2022HomeTeamScheduleView:GetHomeTeamMatchInfo()
    local matchInfo = {}
    if not LuaWorldCup2022Mgr.lotteryInfo.allRoundResultData then return matchInfo end

    local teamId = LuaWorldCup2022Mgr.lotteryInfo.jingCaiData.m_HomeTeamId
    local weekDict = {
        [0] = LocalString.GetString("日"),
        [1] = LocalString.GetString("一"),
        [2] = LocalString.GetString("二"),
        [3] = LocalString.GetString("三"),
        [4] = LocalString.GetString("四"),
        [5] = LocalString.GetString("五"),
        [6] = LocalString.GetString("六"),
    }
    for id, data in pairs(LuaWorldCup2022Mgr.lotteryInfo.allRoundResultData) do
        if data[1] == teamId or data[2] == teamId then
            local playTime = WorldCup_PlayInfo.GetData(id).Time

            local month, day, hour, min = string.match(playTime, "(%d+).(%d+) (%d+):(%d+)")
            local year = WorldCup_Setting.GetData().Year
            local matchStartTime = CServerTimeMgr.Inst:GetTimeStampByStr(SafeStringFormat3("%d-%d-%d %d:%d", year, month, day, hour, min))
            local dataTime = CServerTimeMgr.ConvertTimeStampToZone8Time(matchStartTime)
            local week = weekDict[EnumToInt(dataTime.DayOfWeek)]
            table.insert(matchInfo, {id = id, month = month, day = day, week = week, hour = hour, min = min, matchStartTime = matchStartTime})
        end
    end

    table.sort(matchInfo, function(a, b)
        if a.matchStartTime ~= b.matchStartTime then
            return a.matchStartTime < b.matchStartTime
        else
            return a.id < b.id
        end
    end)
    return matchInfo
end

function LuaWorldCup2022HomeTeamScheduleView:UpdateChildNum(parent, template, num)
    local childCount = parent.childCount
    for i = 1, childCount do
        parent:GetChild(i - 1).gameObject:SetActive(false)
    end

    if childCount < num then
        for i = childCount + 1, num do
            NGUITools.AddChild(parent.gameObject, template)
        end
    end
end

--@region UIEvent
--@endregion UIEvent
