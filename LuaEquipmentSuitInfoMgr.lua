local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CPackagePlayerEquipmentView = import "L10.UI.CPackagePlayerEquipmentView"

CLuaEquipmentSuitInfoMgr={}
CLuaEquipmentSuitInfoMgr.IsShowMainPlayerSuit = true
CLuaEquipmentSuitInfoMgr.IntesifySuitId = 0
CLuaEquipmentSuitInfoMgr.IntesifySuitFxIndex = 0

-- 新强化&锻造界面开关
function CLuaEquipmentSuitInfoMgr:IsEquipmentNewSuitEnable()
    return false
end

function CLuaEquipmentSuitInfoMgr:ShowMainPlayerSuit()
    self.IsShowMainPlayerSuit = true
	if self:IsEquipmentNewSuitEnable() then
		CUIManager.ShowUI(CLuaUIResources.EquipmentNewSuitWnd)
	else
		CUIManager.ShowUI(CUIResources.EquipmentSuitWnd)
	end
end
function CLuaEquipmentSuitInfoMgr:ShowOtherPlayerSuit(intesifySuitId, intesifySuitFxIndex)
	self.IsShowMainPlayerSuit = false
	self.IntesifySuitId = intesifySuitId
	self.IntesifySuitFxIndex = intesifySuitFxIndex

	if EquipIntensify_Suit.GetData(intesifySuitId) == nil then
		g_MessageMgr:ShowMessage("Message_View_Player_Not_Have_QiangHua_TaoZhuang")
		return
	end

	CUIManager.ShowUI(CUIResources.EquipmentSuitWnd)
end

CPackagePlayerEquipmentView.m_hookOnAdditionButtonClick = function(this, go)
	CLuaEquipmentSuitInfoMgr:ShowMainPlayerSuit()
end
