-- Auto Generated!!
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CTradeMgr = import "L10.Game.CTradeMgr"
local CTradeTip = import "L10.UI.CTradeTip"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTradeTip.m_Init_CS2LuaHook = function (this) 
    if CTradeMgr.Inst.GiftDtls.Count == 3 then
        local p1List = CTradeMgr.Inst.GiftDtls[0]
        if p1List ~= nil and p1List.Count >= 2 then
            if p1List[0] then
                this.p1_1.color = Color.white
            else
                this.p1_1.color = Color.red
            end

            if p1List[1] then
                this.p1_2.color = Color.white
            else
                this.p1_2.color = Color.red
            end
        end

        local p2List = CTradeMgr.Inst.GiftDtls[1]
        if p2List ~= nil and p2List.Count >= 2 then
            if p2List[0] then
                this.p2_1.color = Color.white
            else
                this.p2_1.color = Color.red
            end

            if p2List[1] then
                this.p2_2.color = Color.white
            else
                this.p2_2.color = Color.red
            end
        end

        local p3List = CTradeMgr.Inst.GiftDtls[2]
        if p3List ~= nil and p3List.Count >= 1 then
            if p3List[0] then
                this.p3_1.color = Color.white
            else
                this.p3_1.color = Color.red
            end
        end

        CommonDefs.ListClear(CTradeMgr.Inst.GiftDtls)
    else
        this:Close()
    end

    UIEventListener.Get(this.darkbg).onClick = MakeDelegateFromCSFunction(this.OnCloseBtnClick, VoidDelegate, this)
end
