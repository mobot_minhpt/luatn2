local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local UIRoot           = import "UIRoot"
local Screen           = import "UnityEngine.Screen"
local CMainCamera      = import "L10.Engine.CMainCamera"

LuaCarnival2023PVEPlayerCountWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaCarnival2023PVEPlayerCountWnd, "table")
RegistClassMember(LuaCarnival2023PVEPlayerCountWnd, "template")

function LuaCarnival2023PVEPlayerCountWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.table = self.transform:Find("Anchor/Table")
    self.template = self.transform:Find("Anchor/Template").gameObject
    self.template:SetActive(false)
end

function LuaCarnival2023PVEPlayerCountWnd:Init()

end

function LuaCarnival2023PVEPlayerCountWnd:Update()
    local info = LuaCarnival2023Mgr.pvePlayerCountInfo
    if not info then return end

    local i = 0
    for engineId, data in pairs(info) do
        local obj = CClientObjectMgr.Inst:GetObject(engineId)
        if obj and obj.Visible and obj.RO then
            i = i + 1
            local child
            if self.table.childCount < i then
                child = NGUITools.AddChild(self.table.gameObject, self.template).transform
            else
                child = self.table:GetChild(i - 1)
            end
            child.gameObject:SetActive(true)
            child.transform:Find("Count"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("%d/%d", data.currentPlayerNum, data.needPlayerNum)
            self:Layout(child, obj)
        end
    end
    local childCount = self.table.childCount
    if childCount > i then
        for j = i + 1, childCount do
            self.table:GetChild(j - 1).gameObject:SetActive(false)
        end
    end
end

function LuaCarnival2023PVEPlayerCountWnd:Layout(trans, obj)
	local target = obj.RO:GetSlotTransform("TopAnchor", false)
    local screenPos = CMainCamera.Main:WorldToScreenPoint(target.position)
    screenPos.z = 0
    local nguiWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
    trans.position = nguiWorldPos

    local localPos = trans.localPosition
    local bounds = NGUIMath.CalculateRelativeWidgetBounds(trans)
    local centerX = localPos.x
    local centerY = localPos.y

    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenWidth = Screen.width * CUIManager.UIMainCamera.rect.width * scale
    local virtualScreenHeight = Screen.height * scale

    -- 左右不超出屏幕范围
    if centerX - bounds.size.x * 0.5 < -virtualScreenWidth * 0.5 then
        centerX = -virtualScreenWidth * 0.5 + bounds.size.x * 0.5
    end
    if centerX + bounds.size.x * 0.5 > virtualScreenWidth * 0.5 then
        centerX = virtualScreenWidth * 0.5 - bounds.size.x * 0.5
    end

    -- 上下不超出屏幕范围
    if centerY + bounds.size.y * 0.5 > virtualScreenHeight * 0.5 then
        centerY = virtualScreenHeight * 0.5 - bounds.size.y * 0.5
    end
    if centerY - bounds.size.y * 0.5 < -virtualScreenHeight * 0.5 then
        centerY = -virtualScreenHeight * 0.5 + bounds.size.y * 0.5
    end

    trans.localPosition = Vector3(centerX, centerY, 0)
end

function LuaCarnival2023PVEPlayerCountWnd:OnDestroy()
    LuaCarnival2023Mgr.pvePlayerCountInfo = {}
end

--@region UIEvent

--@endregion UIEvent
