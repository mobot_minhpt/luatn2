-- Auto Generated!!
local CEquipHandIntensifyItem = import "L10.UI.CEquipHandIntensifyItem"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local EquipIntensify_Intensify = import "L10.Game.EquipIntensify_Intensify"
local LocalString = import "LocalString"
local NGUIMath = import "NGUIMath"
CEquipHandIntensifyItem.m_UpdateData_CS2LuaHook = function (this, info) 
    this.m_Info = info

    local item = CItemMgr.Inst:GetById(CEquipmentProcessMgr.Inst.SelectEquipment.itemId)
    local currentLevel = item.Equip.IntensifyLevel

    this.LevelLabel.text = System.String.Format(LocalString.GetString("{0}级"), this.m_Info.Level)

    local addValueStr = "?"
    if this.m_Info.addValue > 0 then
        addValueStr = tostring(this.m_Info.addValue)

        if this.m_Info.addValue == this.m_Info.maxAddValue then
            this.percentLabel.text = System.String.Format("{0}%", addValueStr)
        else
            this.percentLabel.text = System.String.Format("{0}%({1}%)", addValueStr, this.m_Info.maxAddValue)
        end
        this.slider.gameObject:SetActive(true)
    else
        this.percentLabel.text = System.String.Format(LocalString.GetString("未强化({0}-{1})%"), this.m_Info.minAddValue, this.m_Info.maxAddValue)
        this.slider.gameObject:SetActive(false)
    end

    do
        local config = EquipIntensify_Intensify.GetData(info.Level)
        this.slider:SetMinMax(config.Range[0], config.Range[config.Range.Length - 1])
        if info.addValue > 0 then
            if this.m_Info.addValue == this.m_Info.minAddValue then
                this.slider:SetMinValue()
            else
                this.slider:SetValue(this.m_Info.addValue)
            end
        else
            this.slider:SetValue(info.addValue)
        end
    end

    this:UpdateAppearance()

    this:UpdateColor()


    this:TryShowLastResult()
end
CEquipHandIntensifyItem.m_OnGetEquipGradeByIntensifyLevelResult_CS2LuaHook = function (this, itemId, newGrade, canEquip) 
    if CEquipmentProcessMgr.Inst.SelectEquipment == nil then
        return
    end
    if this.m_Info.addValue ~= 0 then
        return
    end
    if itemId == CEquipmentProcessMgr.Inst.SelectEquipment.itemId then
        CEquipHandIntensifyItem.s_canEquip = canEquip
        if canEquip then
            --Debug.Log("result " + newGrade+" "+canEquip);
            this.percentLabel.color = NGUIMath.HexToColor(4286054655)
            this.LevelLabel.color = this.percentLabel.color
            this.percentLabel.text = System.String.Format(LocalString.GetString("可强化({0}-{1})%"), this.m_Info.minAddValue, this.m_Info.maxAddValue)
        end
    end
end
CEquipHandIntensifyItem.m_TryShowLastResult_CS2LuaHook = function (this) 
    if CEquipmentProcessMgr.Inst.SelectEquipment == nil then
        return
    end
    if this.m_Info.addValue > 0 then
        return
    end
    if CEquipHandIntensifyItem.s_lastRequestItemId == CEquipmentProcessMgr.Inst.SelectEquipment.itemId then
        if CEquipHandIntensifyItem.s_lastRequestLevel == this.m_Info.Level and CEquipHandIntensifyItem.s_canEquip then
            this.percentLabel.color = NGUIMath.HexToColor(4286054655)
            this.LevelLabel.color = this.percentLabel.color
            this.percentLabel.text = System.String.Format(LocalString.GetString("可强化({0}-{1})%"), this.m_Info.minAddValue, this.m_Info.maxAddValue)
        end
    end
end
