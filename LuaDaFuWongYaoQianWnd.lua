local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local ETickType = import "L10.Engine.ETickType"
local SoundManager = import "SoundManager"
local CTickMgr = import "L10.Engine.CTickMgr"
local Animation = import "UnityEngine.Animation"
LuaDaFuWongYaoQianWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongYaoQianWnd, "QianTong", "QianTong", GameObject)
RegistChildComponent(LuaDaFuWongYaoQianWnd, "Qian", "Qian", GameObject)
RegistChildComponent(LuaDaFuWongYaoQianWnd, "QianName", "QianName", CUITexture)
RegistChildComponent(LuaDaFuWongYaoQianWnd, "YaoYiYao", "YaoYiYao", UILabel)
RegistChildComponent(LuaDaFuWongYaoQianWnd, "Fx", "Fx", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongYaoQianWnd, "m_QianTongAni")
RegistClassMember(LuaDaFuWongYaoQianWnd, "m_QianAni")
RegistClassMember(LuaDaFuWongYaoQianWnd, "m_isShaked")
RegistClassMember(LuaDaFuWongYaoQianWnd, "m_AniTick")
RegistClassMember(LuaDaFuWongYaoQianWnd, "m_CanShake")
RegistClassMember(LuaDaFuWongYaoQianWnd, "m_ShowQianYunTime")
RegistClassMember(LuaDaFuWongYaoQianWnd, "m_ShakeVoiceTick")
RegistClassMember(LuaDaFuWongYaoQianWnd, "m_ShakeVoice")
RegistClassMember(LuaDaFuWongYaoQianWnd, "m_PlayerHead")
function LuaDaFuWongYaoQianWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_QianTongAni = self.QianTong:GetComponent(typeof(Animation))
    self.m_QianAni = self.gameObject:GetComponent(typeof(Animation))
    self.m_AniTick = nil
    self.m_CanShake = false
    self.m_ShowQianYunTime = DaFuWeng_Setting.GetData().YaoQianShowTime
    self.m_ShakeVoiceTick = nil
    self.m_ShakeVoice = nil
    self.m_PlayerHead = self.transform:Find("PlayerHead").gameObject
end

function LuaDaFuWongYaoQianWnd:Init()
    self.m_isShaked = false
    self.m_QianAni:Play("dafuwongyaoqianwnd_show")
    if self.m_AniTick then UnRegisterTick(self.m_AniTick) self.m_AniTick = nil end
    self.m_AniTick = RegisterTickOnce(function ()
        if self.m_QianTongAni then
            self.m_CanShake = true
            self.m_QianTongAni:Play("dafuwongyaoqianwnd_yaoloop")
        end
    end,(self.m_QianTongAni.clip.length - 0.8) * 1000)

    local count = 0
    self.m_ShakeVoiceTick = RegisterTick(function ()
        count = count + 1
            if self.m_ShakeVoice then
                SoundManager.Inst:StopSound(self.m_ShakeVoice)
                self.m_ShakeVoice=nil
            end
            self.m_ShakeVoice = SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_ChouQian", Vector3.zero, nil, 0)
    end, 2000)

    local totalTime = DaFuWeng_Countdown.GetData(EnumDaFuWengStage.RoundGridFoYuan).Time 
    local CountDown = totalTime - self.m_ShowQianYunTime - LuaDaFuWongMgr.GetCardTime
    local endFunc = function() self:OnShowShake(EnumDaFuWengOperateEvent.YaoQian) end
	local durationFunc = function(time) end
	LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.YaoQian,CountDown,durationFunc,endFunc)
    self:ShowPlayerHead()
end

function LuaDaFuWongYaoQianWnd:ShowPlayerHead()
    if LuaDaFuWongMgr.IsMyRound then
        self.m_PlayerHead.gameObject:SetActive(false)
    else
        self.m_PlayerHead.gameObject:SetActive(true)
        local playerData = LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[LuaDaFuWongMgr.CurPlayerRound]
        local Portrait = self.m_PlayerHead.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
        local bg = self.m_PlayerHead.transform:Find("BG"):GetComponent(typeof(CUITexture))
        local Name = self.m_PlayerHead.transform:Find("Name"):GetComponent(typeof(UILabel))
        if playerData then
            Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(playerData.class, playerData.gender, -1), false)
			bg:LoadMaterial(LuaDaFuWongMgr:GetHeadBgPath(playerData.round))
			Name.transform:Find("Texture"):GetComponent(typeof(CUITexture)):LoadMaterial(LuaDaFuWongMgr:GetNameBgPath(playerData.round))
            Name.text = playerData.name
        end
    end
end

function LuaDaFuWongYaoQianWnd:SetQian()
    local id = LuaDaFuWongMgr.CurRoundArgs[1]
    if id then
        local data = DaFuWeng_YaoQian.GetData(id)
        if data then
            self.QianName:LoadMaterial(data.QianPath)
            self.transform:Find("QianYu/Label"):GetComponent(typeof(UILabel)).text = data.Describe
        end
    end
end
function LuaDaFuWongYaoQianWnd:OnPlayerShakeDevice()
    if self.m_isShaked or (not self.m_CanShake) then return end
    if not LuaDaFuWongMgr.IsMyRound then 
        -- local id = LuaDaFuWongMgr.CurRoundPlayer
        -- local name = LuaDaFuWongMgr.PlayerInfo[id].name
        -- g_MessageMgr:ShowMessage("DaFuWeng_OtherPlayerOperate",name)
        return
    end
    -- 发玩家摇签RPC
    Gac2Gas.DaFuWengFoYuanYaoQian()
end

function LuaDaFuWongYaoQianWnd:OnShowShake(type)
    if type ~= EnumDaFuWengOperateEvent.YaoQian then return end
    if self.m_isShaked or (not self.m_CanShake) then return end
    LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.YaoQian)
    self.m_CanShake = false
    self.m_isShaked = true
    self:SetQian()
    self:StartShake()
end


function LuaDaFuWongYaoQianWnd:StartShake()
    if self.m_QianTongAni then 
        self.m_QianTongAni:Stop()
    end
    if self.m_QianAni then
        self.m_QianAni:Play("dafuwongyaoqianwnd_kai")
        local time = self.m_ShowQianYunTime
        if self.m_AniTick then UnRegisterTick(self.m_AniTick) self.m_AniTick = nil end
        self.m_AniTick = RegisterTickOnce(function ()
            g_ScriptEvent:BroadcastInLua("OnDaFuWengFinishCurStage",EnumDaFuWengStage.RoundGridFoYuan)
            CUIManager.CloseUI(CLuaUIResources.DaFuWongYaoQianWnd)
        end,time * 1000)
    end
    if self.m_ShakeVoiceTick then UnRegisterTick(self.m_ShakeVoiceTick) self.m_ShakeVoiceTick = nil end
    if self.m_ShakeVoice then
        SoundManager.Inst:StopSound(self.m_ShakeVoice)
        self.m_ShakeVoice=nil
    end
    self.m_ShakeVoice = SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_ChouQian", Vector3.zero, nil, 2500)
    
end

function LuaDaFuWongYaoQianWnd:OnStageUpdate(CurStage)
	if CurStage ~= EnumDaFuWengStage.RoundGridFoYuan then CUIManager.CloseUI(CLuaUIResources.DaFuWongYaoQianWnd) end
end

--@region UIEvent

--@endregion UIEvent
function LuaDaFuWongYaoQianWnd:OnEnable()
    g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
    g_ScriptEvent:AddListener("PlayerShakeDevice", self, "OnPlayerShakeDevice")
    g_ScriptEvent:AddListener("OnDaFuWongPlayerOpreateEvent",self,"OnShowShake")
end

function LuaDaFuWongYaoQianWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
    g_ScriptEvent:RemoveListener("OnDaFuWongPlayerOpreateEvent",self,"OnShowShake")
    g_ScriptEvent:RemoveListener("PlayerShakeDevice", self, "OnPlayerShakeDevice")
    LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.YaoQian)
    if self.m_AniTick then UnRegisterTick(self.m_AniTick) self.m_AniTick = nil end
    if self.m_ShakeVoiceTick then UnRegisterTick(self.m_ShakeVoiceTick) self.m_ShakeVoiceTick = nil end
    if self.m_ShakeVoice then
        SoundManager.Inst:StopSound(self.m_ShakeVoice)
        self.m_ShakeVoice=nil
    end
end
