local CGamePlayMgr = import "L10.Game.CGamePlayMgr"

LuaYuanXiao2021TangYuanSettlementWnd = class()

RegistChildComponent(LuaYuanXiao2021TangYuanSettlementWnd,"m_LeaveBtn","LeaveBtn", GameObject)
RegistChildComponent(LuaYuanXiao2021TangYuanSettlementWnd,"m_ExchangeBtn","ExchangeBtn", GameObject)
RegistChildComponent(LuaYuanXiao2021TangYuanSettlementWnd,"m_Tip","Tip", UILabel)
RegistChildComponent(LuaYuanXiao2021TangYuanSettlementWnd,"m_Time","Time", UILabel)
RegistChildComponent(LuaYuanXiao2021TangYuanSettlementWnd,"m_Score","Score", UILabel)
RegistChildComponent(LuaYuanXiao2021TangYuanSettlementWnd,"m_BestScore","BestScore", UILabel)
RegistChildComponent(LuaYuanXiao2021TangYuanSettlementWnd,"m_Rank","Rank", UILabel)

function LuaYuanXiao2021TangYuanSettlementWnd:Init()
    Gac2Gas.RequestWatchTangYuan2021()

    self.m_Score.text = tostring(LuaYuanXiao2021Mgr.tangyuanScore)
    local lastTime = LuaYuanXiao2021Mgr.tangyuanDuration - Yuanxiao2021_Setting.GetData().StartPlayCountDownTime
    if lastTime < 0 then
        lastTime = 0
    end
    self.m_Time.text = tostring(lastTime)
    self.m_BestScore.text = tostring(LuaYuanXiao2021Mgr.todayTangYuanScore)
    self.m_Rank.text = tostring(LuaYuanXiao2021Mgr.tangYuanRank)

    local tipsCount = Yuanxiao2021_TangYuanTips.GetDataCount()
    local key = math.floor(math.random(1, tipsCount))
    local data = Yuanxiao2021_TangYuanTips.GetData(key)
    self.m_Tip.text = data.Tip

    UIEventListener.Get(self.m_ExchangeBtn).onClick = DelegateFactory.VoidDelegate(function()
        CLuaNPCShopInfoMgr.ShowScoreShop("TangYuan2021")
    end)
    UIEventListener.Get(self.m_LeaveBtn).onClick = DelegateFactory.VoidDelegate(function()
        if not LuaYuanXiao2021Mgr:IsInTangYuanPlay() then
            g_MessageMgr:ShowMessage("TANGYUAN_ALREADY_LEFT")
        else
            CGamePlayMgr.Inst:LeavePlay()
        end 
    end)
end
