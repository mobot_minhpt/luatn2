local UIGrid = import "UIGrid"
local CCommonLuaWnd = import "L10.UI.CCommonLuaWnd"
local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaButterflyCrisisRewardWnd = class()

RegistChildComponent(LuaButterflyCrisisRewardWnd, "RewardGrid", UIGrid)
RegistChildComponent(LuaButterflyCrisisRewardWnd, "RewardItem", GameObject)
RegistChildComponent(LuaButterflyCrisisRewardWnd, "TakeBtn", GameObject)

RegistClassMember(LuaButterflyCrisisRewardWnd, "m_Wnd")

function LuaButterflyCrisisRewardWnd:Init()
    self.RewardItem:SetActive(false)
    self.m_Wnd = self.gameObject:GetComponent(typeof(CCommonLuaWnd))

    self:UpdateRewards()

    CommonDefs.AddOnClickListener(self.TakeBtn, DelegateFactory.Action_GameObject(function (go)
        self.m_Wnd:Close()
    end), false)
end

function LuaButterflyCrisisRewardWnd:UpdateRewards()

    if not LuaButterflyCrisisMgr.m_RewardTakenInfos or #LuaButterflyCrisisMgr.m_RewardTakenInfos <= 0 then
        self.m_Wnd:Close()
        return
    end

    CUICommonDef.ClearTransform(self.RewardGrid.transform)

    for i = 1, #LuaButterflyCrisisMgr.m_RewardList do
        local go = NGUITools.AddChild(self.RewardGrid.gameObject, self.RewardItem)
        self:InitRewardItem(go, LuaButterflyCrisisMgr.m_RewardList[i])
        go:SetActive(true)
    end

    self.RewardGrid:Reposition()
end

function LuaButterflyCrisisRewardWnd:InitRewardItem(item, info)
    if not item or not info then return end

    local iconTexture = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    iconTexture:Clear()

    local qualitySprite = item.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
    qualitySprite.spriteName = nil

    local amountLabel = item.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
    amountLabel.text = nil

    local itemTemplate = Item_Item.GetData(info.ItemId)

    if not itemTemplate then return end

    iconTexture:LoadMaterial(itemTemplate.Icon)
    amountLabel.text = tostring(info.Count)
    qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(itemTemplate, nil, false)

    CommonDefs.AddOnClickListener(item, DelegateFactory.Action_GameObject(function (go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(info.ItemId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
    end), false)
end
