local UIGrid = import "UIGrid"

local UISlider = import "UISlider"

local UILabel = import "UILabel"

local GameObject = import "UnityEngine.GameObject"

LuaGuildTerritorialWarsGongFengView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "Fire", "Fire", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "Wood", "Wood", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "Ice", "Ice", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "Wind", "Wind", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "ActivateValueLabel", "ActivateValueLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "FireProgressBar", "FireProgressBar", UISlider)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "WoodProgressBar", "WoodProgressBar", UISlider)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "IceProgressBar", "IceProgressBar", UISlider)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "WindProgressBar", "WindProgressBar", UISlider)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "FireProgressLabel", "FireProgressLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "WoodProgressLabel", "WoodProgressLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "IceProgressLabel", "IceProgressLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "WindProgressLabel", "WindProgressLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "FireValLabel", "FireValLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "WoodValLabel", "WoodValLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "IceValLabel", "IceValLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "WindValLabel", "WindValLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "GongFengButton", "GongFengButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "RightGrid", "RightGrid", UIGrid)
RegistChildComponent(LuaGuildTerritorialWarsGongFengView, "RightTemplate", "RightTemplate", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaGuildTerritorialWarsGongFengView,"m_ShengTanList")
RegistClassMember(LuaGuildTerritorialWarsGongFengView,"m_ShengTanFxList")
RegistClassMember(LuaGuildTerritorialWarsGongFengView,"m_MonsterId2NumLabelMap")

function LuaGuildTerritorialWarsGongFengView:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

	UIEventListener.Get(self.GongFengButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGongFengButtonClick()
	end)

    --@endregion EventBind end
end

function LuaGuildTerritorialWarsGongFengView:Start()
    self:InitShengTan()
    self:InitRightView()
    self:ShowResourceInfo()
end

function LuaGuildTerritorialWarsGongFengView:InitShengTan()
    self.m_ShengTanList = {self.Fire, self.Wood, self.Ice, self.Wind}
    self.m_ShengTanFxList = {"fx/ui/prefab/UI_shentan_huo.prefab","fx/ui/prefab/UI_shentan_mu.prefab","fx/ui/prefab/UI_shentan_bing.prefab","fx/ui/prefab/UI_shentan_feng.prefab"}
    local altarInfo = LuaGuildTerritorialWarsMgr.m_AltarInfo
    local activedNum = 0
    for index = 1, 4 do
        local show = false
        if altarInfo and altarInfo.activedAltarInfo then
            show = altarInfo.activedAltarInfo[index]
            if show then
                activedNum = activedNum + 1
            end
        end
        self:ShowShengTan(index, show)
    end
    self.ActivateValueLabel.text = SafeStringFormat3("%d/4",activedNum, self.IceValLabel) 
end

function LuaGuildTerritorialWarsGongFengView:ShowResourceInfo()
    local altarInfo = LuaGuildTerritorialWarsMgr.m_AltarInfo
    local valLabelArr = {self.FireValLabel,self.WoodValLabel,self.IceValLabel,self.WindValLabel}
    for idx, label in pairs(valLabelArr) do
        label.text = altarInfo.haveResourceCount and altarInfo.haveResourceCount[idx] or false
    end
    local maxResourceCount = GuildTerritoryWar_RelatedPlaySetting.GetData().ActiveAltarNeedResourceCount
    local progressBarArr = {self.FireProgressBar, self.WoodProgressBar, self.IceProgressBar, self.WindProgressBar}
    for idx, progressBar in pairs(progressBarArr) do
        progressBar.value = (altarInfo.altarResourceInfo and altarInfo.altarResourceInfo[idx] or 0) / maxResourceCount
    end
    local progressLabelArr = {self.FireProgressLabel, self.WoodProgressLabel, self.IceProgressLabel, self.WindProgressLabel}
    for idx, progressLabel in pairs(progressLabelArr) do
        progressLabel.text = SafeStringFormat3("%d/%d",(altarInfo.altarResourceInfo and altarInfo.altarResourceInfo[idx] or 0)  , maxResourceCount)
    end
end

function LuaGuildTerritorialWarsGongFengView:ShowShengTan(index, show)
    local obj = self.m_ShengTanList[index]
    local tex = obj.transform:Find("Texture"):GetComponent(typeof(UITexture))
    local fx = obj.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    tex.alpha = show and 1 or 0.3
    if show then
        local fxPath =  self.m_ShengTanFxList[index]
        fx:LoadFx(fxPath)
    else
        fx:DestroyFx()
    end
end

function LuaGuildTerritorialWarsGongFengView:InitRightView()
    Extensions.RemoveAllChildren(self.RightGrid.transform)
    self.RightTemplate.gameObject:SetActive(false)
    local monsterGenerateInfo = GuildTerritoryWar_RelatedPlaySetting.GetData().MonsterGenerateInfo
    self.m_MonsterId2NumLabelMap = {}
    for i = 0, monsterGenerateInfo.Length - 1 do
        local obj = NGUITools.AddChild(self.RightGrid.gameObject, self.RightTemplate.gameObject)
        obj.gameObject:SetActive(true)
        local monsterId = monsterGenerateInfo[i][1]
        local num = 0 
        self:InitRightTemplate(obj, monsterId, num)
    end
    self.RightGrid:Reposition()
end

function LuaGuildTerritorialWarsGongFengView:InitRightTemplate(obj, monsterId, num)
    local icon = obj.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local nameLabel = obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local numLabel = obj.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
    local button = obj.transform:Find("Button")
    UIEventListener.Get(button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsPeripheryMonsterWnd)
	end)
    local monsterData = Monster_Monster.GetData(monsterId)
    icon:LoadNPCPortrait(monsterData.HeadIcon, false)
    nameLabel.text = monsterData.Name
    numLabel.text = ""

    self.m_MonsterId2NumLabelMap[monsterId] = numLabel
end

function LuaGuildTerritorialWarsGongFengView:OnEnable()
    g_ScriptEvent:AddListener("SendGTWAltarInfo", self, "OnSendGTWAltarInfo")
    Gac2Gas.RequestGTWAltarInfo()
end

function LuaGuildTerritorialWarsGongFengView:OnDisable()
    g_ScriptEvent:RemoveListener("SendGTWAltarInfo", self, "OnSendGTWAltarInfo")
end

function LuaGuildTerritorialWarsGongFengView:OnSendGTWAltarInfo(altarInfo)
    self:InitShengTan()
    self:InitRightView()
    self:ShowResourceInfo()
end

--@region UIEvent

function LuaGuildTerritorialWarsGongFengView:OnTipButtonClick()
    g_MessageMgr:ShowMessage("GuildTerritorialWarsGongFengView_ReadMe")
end


function LuaGuildTerritorialWarsGongFengView:OnGongFengButtonClick()
    Gac2Gas.SubmitGTWAltarResourceItem()
end


--@endregion UIEvent

