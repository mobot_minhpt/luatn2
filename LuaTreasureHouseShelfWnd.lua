require("3rdParty/ScriptEvent")
require("common/common_include")

local UITable = import "UITable"
local MessageMgr = import "L10.Game.MessageMgr"
local Cangbaoge_Condition = import "L10.Game.Cangbaoge_Condition"

LuaTreasureHouseShelfWnd=class()

RegistClassMember(LuaTreasureHouseShelfWnd, "InfoTable")
RegistClassMember(LuaTreasureHouseShelfWnd, "ParagraphTemplate")
RegistClassMember(LuaTreasureHouseShelfWnd, "ShelfBtn")

RegistClassMember(LuaTreasureHouseShelfWnd, "ShelfConditionTable")
RegistClassMember(LuaTreasureHouseShelfWnd, "CanShelf")

RegistClassMember(LuaTreasureHouseShelfWnd, "BangDingConditionId")

function LuaTreasureHouseShelfWnd:Init()
	self.BangDingConditionId = 9
	self:InitClassMembers()
	self:UpdateRequestInfos(LuaTreasureHouseMgr.ShelfInfo)
end

function LuaTreasureHouseShelfWnd:InitClassMembers()
	self.InfoTable = self.transform:Find("Anchor/bg/ContentScrollView/InfoTable"):GetComponent(typeof(UITable))
	self.ParagraphTemplate = self.transform:Find("Anchor/bg/Templates/ParagraphTemplate").gameObject
	self.ParagraphTemplate:SetActive(false)
	self.ShelfBtn = self.transform:Find("Anchor/Buttons/ShelfBtn").gameObject

	self.ShelfConditionTable = {}
	self.CanShelf = false

	local onShelfBtnClicked = function (go)
		self:OnShelfBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.ShelfBtn,DelegateFactory.Action_GameObject(onShelfBtnClicked),false)
end

function LuaTreasureHouseShelfWnd:OnEnable()
	g_ScriptEvent:AddListener("CBGSendShelfConditionsInfo", self, "UpdateRequestInfos")
end

function LuaTreasureHouseShelfWnd:OnDisable()
	g_ScriptEvent:RemoveListener("CBGSendShelfConditionsInfo", self, "UpdateRequestInfos")
end

-- 更新登记要求
function LuaTreasureHouseShelfWnd:UpdateRequestInfos(infos)
	if not infos then 
		CUIManager.CloseUI(CLuaUIResources.TreasureHouseShelfWnd)
		return
	end

	self.ShelfConditionTable = infos
	local canShelf = true
	CUICommonDef.ClearTransform(self.InfoTable.transform)
	for k, v in ipairs(self.ShelfConditionTable) do

		local condition = Cangbaoge_Condition.GetData(v.conditionIdx)
		-- 部分条件隐藏不显示
		if condition.Hide == 0 then
			local go = NGUITools.AddChild(self.InfoTable.gameObject, self.ParagraphTemplate)
			go:SetActive(true)
			self:InitRequestInfo(go, k, v)
		end
		canShelf = canShelf  and v.ret
	end
	self.CanShelf = canShelf
	self.InfoTable:Reposition()
end

function LuaTreasureHouseShelfWnd:InitRequestInfo(go, index, info)

	local requestLabel = go.transform:Find("Label"):GetComponent(typeof(UILabel))
	local resultSuccess = go.transform:Find("ResultSuccess").gameObject
	local resultFail = go.transform:Find("ResultFail").gameObject
	local bindPhoneBtn = go.transform:Find("BindPhoneBtn").gameObject

	resultSuccess:SetActive(false)
	resultFail:SetActive(false)
	bindPhoneBtn:SetActive(false)

	if not info then return end

	local condition = Cangbaoge_Condition.GetData(info.conditionIdx)
	if not condition then return end

	if info.ret then
		resultSuccess:SetActive(true)
		requestLabel.text = SafeStringFormat3("[FFFFFF]%s[-]", condition.Value)
	else
		resultFail:SetActive(true)
		requestLabel.text = SafeStringFormat3("[FF0000]%s[-]", condition.Value)
		
		if info.conditionIdx == self.BangDingConditionId then
			bindPhoneBtn:SetActive(true)
			resultFail:SetActive(false)
			local onBindPhoneBtnClicked = function (go)
				self:OnBindPhoneBtnClicked(go)
			end
			CommonDefs.AddOnClickListener(bindPhoneBtn,DelegateFactory.Action_GameObject(onBindPhoneBtnClicked),false)
		end
	end
end


function LuaTreasureHouseShelfWnd:OnShelfBtnClicked(go)
	if self.CanShelf then
		CUIManager.ShowUI(CLuaUIResources.TreasureHousePutToSellWnd)
	else
		-- 提示无法上架的信息
		MessageMgr.Inst:ShowMessage("CBG_CANNOT_ONSHELF", {})
	end
end

function LuaTreasureHouseShelfWnd:OnBindPhoneBtnClicked(go)
	Gac2Gas.CBG_RequestOnShelfSmsCode(CClientMainPlayer.Inst.Id, "")
end

return LuaTreasureHouseShelfWnd
