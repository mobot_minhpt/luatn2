local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"

LuaStarBiwuQuDaoBattleStatusWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaStarBiwuQuDaoBattleStatusWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaStarBiwuQuDaoBattleStatusWnd, "ChampionNameLabel", "ChampionNameLabel", UILabel)
RegistChildComponent(LuaStarBiwuQuDaoBattleStatusWnd, "FinalMatchTimeLabel", "FinalMatchTimeLabel", UILabel)
RegistChildComponent(LuaStarBiwuQuDaoBattleStatusWnd, "ChampionWatchButton", "ChampionWatchButton", GameObject)
RegistChildComponent(LuaStarBiwuQuDaoBattleStatusWnd, "LeftTeamItem", "LeftTeamItem", GameObject)
RegistChildComponent(LuaStarBiwuQuDaoBattleStatusWnd, "RightTeamItem", "RightTeamItem", GameObject)
RegistChildComponent(LuaStarBiwuQuDaoBattleStatusWnd, "LeftTeamGrid", "LeftTeamGrid", UIGrid)
RegistChildComponent(LuaStarBiwuQuDaoBattleStatusWnd, "RightTeamGrid", "RightTeamGrid", UIGrid)
RegistChildComponent(LuaStarBiwuQuDaoBattleStatusWnd, "LeftLineItem1", "LeftLineItem1", GameObject)
RegistChildComponent(LuaStarBiwuQuDaoBattleStatusWnd, "LeftLineItem2", "LeftLineItem2", GameObject)
RegistChildComponent(LuaStarBiwuQuDaoBattleStatusWnd, "LeftLineItem3", "LeftLineItem3", GameObject)
RegistChildComponent(LuaStarBiwuQuDaoBattleStatusWnd, "RightLineItem1", "RightLineItem1", GameObject)
RegistChildComponent(LuaStarBiwuQuDaoBattleStatusWnd, "RightLineItem2", "RightLineItem2", GameObject)
RegistChildComponent(LuaStarBiwuQuDaoBattleStatusWnd, "RightLineItem3", "RightLineItem3", GameObject)
RegistChildComponent(LuaStarBiwuQuDaoBattleStatusWnd, "LeftLineGrid1", "LeftLineGrid1", UIGrid)
RegistChildComponent(LuaStarBiwuQuDaoBattleStatusWnd, "LeftLineGrid2", "LeftLineGrid2", UIGrid)
RegistChildComponent(LuaStarBiwuQuDaoBattleStatusWnd, "RightLineGrid1", "RightLineGrid1", UIGrid)
RegistChildComponent(LuaStarBiwuQuDaoBattleStatusWnd, "RightLineGrid2", "RightLineGrid2", UIGrid)
RegistChildComponent(LuaStarBiwuQuDaoBattleStatusWnd, "WaitChampion", "WaitChampion", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaStarBiwuQuDaoBattleStatusWnd,"m_ItemList")
RegistClassMember(LuaStarBiwuQuDaoBattleStatusWnd,"m_MatchItemInfo")
RegistClassMember(LuaStarBiwuQuDaoBattleStatusWnd,"m_ItemIndex2Match")
RegistClassMember(LuaStarBiwuQuDaoBattleStatusWnd,"m_ItemIndex2Round")
RegistClassMember(LuaStarBiwuQuDaoBattleStatusWnd,"m_lineList")
RegistClassMember(LuaStarBiwuQuDaoBattleStatusWnd,"m_MatchLineInfo")
RegistClassMember(LuaStarBiwuQuDaoBattleStatusWnd,"m_lineIndex2Round")
RegistClassMember(LuaStarBiwuQuDaoBattleStatusWnd,"m_MatchRoundInfo")
RegistClassMember(LuaStarBiwuQuDaoBattleStatusWnd,"m_Round2NextRound")

function LuaStarBiwuQuDaoBattleStatusWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.ChampionWatchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChampionWatchButtonClick()
	end)


    --@endregion EventBind end
end

function LuaStarBiwuQuDaoBattleStatusWnd:Init()
	self.TimeLabel.text = g_MessageMgr:FormatMessage("StarBiwuQuDaoBattleStatusWnd_TimeLabel")
	self.LeftTeamItem.gameObject:SetActive(false)
	self.LeftLineItem1.gameObject:SetActive(false)
	self.LeftLineItem2.gameObject:SetActive(false)
	self.RightTeamItem.gameObject:SetActive(false)
	self.RightLineItem1.gameObject:SetActive(false)
	self.RightLineItem2.gameObject:SetActive(false)
	self.WaitChampion.gameObject:SetActive(true)
	self.ChampionWatchButton.gameObject:SetActive(false)
	self.ChampionNameLabel.text = ""
	self.FinalMatchTimeLabel.text = SafeStringFormat3(LocalString.GetString("决赛 %s"), StarBiWuShow_QDZhengShiSaiTime.GetData(15).PlayTime) 
	self:InitMatchItemInfo()
	self:CreateItems()
	self:CreateLines()
	self:OnSyncStarBiwuZhengShiSaiZK()
end

function LuaStarBiwuQuDaoBattleStatusWnd:InitMatchItemInfo()
	--场次-> {匹配玩家idx，itemIndex}
	self.m_MatchItemInfo = {
		[1] = {match = {1, 16}, itemIndex = {1, 2}}, 
		[2] = {match = {2, 15}, itemIndex = {9, 10}}, 
		[3] = {match = {3, 14}, itemIndex = {13, 14}}, 
		[4] = {match = {4, 13}, itemIndex = {5, 6}}, 
		[5] = {match = {5, 12}, itemIndex = {7, 8}}, 
		[6] = {match = {6, 11}, itemIndex = {15, 16}},
		[7] = {match = {7, 10}, itemIndex = {11, 12}},
		[8] = {match = {8, 9}, itemIndex = {3, 4}},
	}
	self.m_ItemIndex2Match = {}
	self.m_ItemIndex2Round = {}
	for round, info in pairs(self.m_MatchItemInfo) do
		self.m_ItemIndex2Match[info.itemIndex[1]] = info.match[1]
		self.m_ItemIndex2Match[info.itemIndex[2]] = info.match[2]
		self.m_ItemIndex2Round[info.itemIndex[1]] = round
		self.m_ItemIndex2Round[info.itemIndex[2]] = round
	end

	self.m_lineIndex2Round = {}
	self.m_MatchLineInfo = {
		[1] = {lineIndex = 1}, 
		[2] = {lineIndex = 5}, 
		[3] = {lineIndex = 7}, 
		[4] = {lineIndex = 3}, 
		[5] = {lineIndex = 4}, 
		[6] = {lineIndex = 8}, 
		[7] = {lineIndex = 6}, 
		[8] = {lineIndex = 2}, 
		[9] = {lineIndex = 9}, 
		[10] = {lineIndex = 11}, 
		[11] = {lineIndex = 12}, 
		[12] = {lineIndex = 10}, 
		[13] = {lineIndex = 13}, 
		[14] = {lineIndex = 14}, 
	}
	for round, info in pairs(self.m_MatchLineInfo) do
		self.m_lineIndex2Round[info.lineIndex] = round
	end

	self.m_MatchRoundInfo = {
		[9] = {1, 8},
		[10] = {2, 7},
		[11] = {3, 6},
		[12] = {4, 5},
		[13] = {9, 12},
		[14] = {10, 11},
		[15] = {13, 14},
	}
	self.m_Round2NextRound = {}
	for round, list in pairs(self.m_MatchRoundInfo) do
		self.m_Round2NextRound[list[1]] = round
		self.m_Round2NextRound[list[2]] = round
	end
end

function LuaStarBiwuQuDaoBattleStatusWnd:CreateItems()
	self.m_ItemList = {}

	Extensions.RemoveAllChildren(self.LeftTeamGrid.transform)
	for i = 1, 8 do
		local item = NGUITools.AddChild(self.LeftTeamGrid.gameObject, self.LeftTeamItem.gameObject)
		table.insert(self.m_ItemList, item)
		self:CreateItem(item, i % 2 == 0, i)
	end
	self.LeftTeamGrid:Reposition()

	Extensions.RemoveAllChildren(self.RightTeamGrid.transform)
	for i = 9, 16 do
		local item = NGUITools.AddChild(self.RightTeamGrid.gameObject, self.RightTeamItem.gameObject)
		table.insert(self.m_ItemList, item)
		self:CreateItem(item, i % 2 == 0, i)
	end
	self.RightTeamGrid:Reposition()
end

function LuaStarBiwuQuDaoBattleStatusWnd:CreateItem(item, isTop, itemIndex)
	item.gameObject:SetActive(true)
	local topLine = item.transform:Find("TopLine")
	local bottomLine = item.transform:Find("BottomLine")
	local numLabel = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
	local teamNameLabel = item.transform:Find("TeamNameLabel"):GetComponent(typeof(UILabel))
	if topLine then
		topLine.gameObject:SetActive(isTop)
	end
	if bottomLine then
		bottomLine.gameObject:SetActive(not isTop)
	end
	if numLabel then
		local matchIndex = self.m_ItemIndex2Match[itemIndex]
		numLabel.text = Extensions.ToChinese(matchIndex)
	end
	if teamNameLabel then
		teamNameLabel.text = LocalString.GetString("待定")
		teamNameLabel.alpha = 0.3
	end
end

function LuaStarBiwuQuDaoBattleStatusWnd:CreateLines()
	self.m_lineList = {}

	Extensions.RemoveAllChildren(self.LeftLineGrid1.transform)
	Extensions.RemoveAllChildren(self.RightLineGrid1.transform)
	for i = 1, 8 do
		local grid = i < 5 and self.LeftLineGrid1 or self.RightLineGrid1
		local template = i < 5 and self.LeftLineItem1 or self.RightLineItem1
		local item = NGUITools.AddChild(grid.gameObject, template.gameObject)
		table.insert(self.m_lineList, item)
		self:CreateLine(item, i % 2 == 0, i)
	end
	self.LeftLineGrid1:Reposition()
	self.RightLineGrid1:Reposition()

	Extensions.RemoveAllChildren(self.LeftLineGrid2.transform)
	Extensions.RemoveAllChildren(self.RightLineGrid2.transform)
	for i = 9, 12 do
		local grid = i < 11 and self.LeftLineGrid2 or self.RightLineGrid2
		local template = i < 11 and self.LeftLineItem2 or self.RightLineItem2
		local item = NGUITools.AddChild(grid.gameObject, template.gameObject)
		table.insert(self.m_lineList, item)
		self:CreateLine(item, i % 2 == 0, i)
	end
	self.LeftLineGrid2:Reposition()
	self.RightLineGrid2:Reposition()

	table.insert(self.m_lineList, self.LeftLineItem3)
	self:CreateLine(self.LeftLineItem3, false, 13)
	table.insert(self.m_lineList, self.RightLineItem3)
	self:CreateLine(self.RightLineItem3, false, 14)
end

function LuaStarBiwuQuDaoBattleStatusWnd:CreateLine(item, isTop, lineIndex)
	item.gameObject:SetActive(true)
	local topLine = item.transform:Find("TopLine")
	local bottomLine = item.transform:Find("BottomLine")
	local wait = item.transform:Find("Wait")
	local watchButton = item.transform:Find("WatchButton")
	local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
	local numLabel = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
	if topLine then
		topLine.gameObject:SetActive(isTop)
	end
	if bottomLine then
		bottomLine.gameObject:SetActive(not isTop)
	end
	timeLabel.text = StarBiWuShow_QDZhengShiSaiTime.GetData(self.m_lineIndex2Round[lineIndex]).PlayTime
	numLabel.gameObject:SetActive(false)
	wait.gameObject:SetActive(true)
	watchButton.gameObject:SetActive(false)

	UIEventListener.Get(watchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		Gac2Gas.RequestWatchStarBiwuZhengShiSai_QD(self.m_lineIndex2Round[lineIndex])
	end)
end

function LuaStarBiwuQuDaoBattleStatusWnd:OnEnable()
	g_ScriptEvent:AddListener("OnSyncStarBiwuZhengShiSaiZK", self, "OnSyncStarBiwuZhengShiSaiZK")
end

function LuaStarBiwuQuDaoBattleStatusWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSyncStarBiwuZhengShiSaiZK", self, "OnSyncStarBiwuZhengShiSaiZK")
end

function LuaStarBiwuQuDaoBattleStatusWnd:OnSyncStarBiwuZhengShiSaiZK()
	for i = 1, #self.m_ItemList do
		local item = self.m_ItemList[i]
		local matchIndex = self.m_ItemIndex2Match[i]
		local info = CLuaStarBiwuMgr.m_ZhengShiSaiZhanduiInfo[matchIndex]
		local round = self.m_ItemIndex2Round[i]
		local statusInfo = CLuaStarBiwuMgr.m_ZhengShiSaiStatusInfo[round]
		local isWinner = false
		if info then
			local teamNameLabel = item.transform:Find("TeamNameLabel"):GetComponent(typeof(UILabel))
			if teamNameLabel then
				if info.zhanduiId == 0 then
					teamNameLabel.text = (statusInfo and statusInfo.status ~= EnumStarBiwuFightCommonResult.eNotBegin) and LocalString.GetString("轮空") or LocalString.GetString("待定")
					teamNameLabel.alpha = 0.3
				else
					teamNameLabel.text = info.zhanduiName
					teamNameLabel.alpha = 1
				end
			end
			local idx = CLuaStarBiwuMgr.m_ZhengShiSaiZhanduiId2Idx[statusInfo.winnerId]
			isWinner = statusInfo and statusInfo.status == 2 and matchIndex == idx
		end
		self:SetWinner(item, isWinner)
	end
	for i = 1, #self.m_lineList do
		local item = self.m_lineList[i]
		local round = self.m_lineIndex2Round[i]
		local statusInfo = CLuaStarBiwuMgr.m_ZhengShiSaiStatusInfo[round]
		local isWinner = false
		if statusInfo then
			self:InitLine(item, statusInfo)
			local nextRound = self.m_Round2NextRound[round]
			local nextStatusInfo = CLuaStarBiwuMgr.m_ZhengShiSaiStatusInfo[nextRound]
			if nextStatusInfo then
				isWinner = nextStatusInfo.status == 2 and statusInfo.winnerId ~= 0 and statusInfo.winnerId == nextStatusInfo.winnerId
			end
		end
		self:SetWinner(item, isWinner)
	end

	local lastStatusInfo = CLuaStarBiwuMgr.m_ZhengShiSaiStatusInfo[15]
	if lastStatusInfo then
		self.WaitChampion.gameObject:SetActive(lastStatusInfo.status == 0)
		self.ChampionWatchButton.gameObject:SetActive(lastStatusInfo.status == 1)
		local idx = CLuaStarBiwuMgr.m_ZhengShiSaiZhanduiId2Idx[lastStatusInfo.winnerId]
		local info = idx and CLuaStarBiwuMgr.m_ZhengShiSaiZhanduiInfo[idx]
		local name = info and info.zhanduiName or ""
		self.ChampionNameLabel.text = lastStatusInfo.status == 2 and name or ""
	end
end

function LuaStarBiwuQuDaoBattleStatusWnd:SetWinner(item, isWinner)
	local topLine = item.transform:Find("TopLine")
	local bottomLine = item.transform:Find("BottomLine")
	local line = item.transform:Find("Line")
	if topLine then
		local sprites = CommonDefs.GetComponentsInChildren_Component_Type(topLine.transform, typeof(UISprite))
		for i = 0, sprites.Length - 1 do
			sprites[i].color = isWinner and Color.yellow or Color.white
		end
	end
	if bottomLine then
		local sprites = CommonDefs.GetComponentsInChildren_Component_Type(bottomLine.transform, typeof(UISprite))
		for i = 0, sprites.Length - 1 do
			sprites[i].color = isWinner and Color.yellow or Color.white
		end
	end
	if line then
		local sprites = CommonDefs.GetComponentsInChildren_Component_Type(line.transform, typeof(UISprite))
		for i = 0, sprites.Length - 1 do
			sprites[i].color = isWinner and Color.yellow or Color.white
		end
	end
end

function LuaStarBiwuQuDaoBattleStatusWnd:InitLine(item, statusInfo)
	local wait = item.transform:Find("Wait")
	local watchButton = item.transform:Find("WatchButton")
	local numLabel = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))

	numLabel.gameObject:SetActive(statusInfo.status == 2)
	wait.gameObject:SetActive(statusInfo.status == 0)
	watchButton.gameObject:SetActive(statusInfo.status == 1)

	local idx = CLuaStarBiwuMgr.m_ZhengShiSaiZhanduiId2Idx[statusInfo.winnerId]
	numLabel.text = idx and Extensions.ToChinese(idx) or ""
end
--@region UIEvent

function LuaStarBiwuQuDaoBattleStatusWnd:OnChampionWatchButtonClick()
	Gac2Gas.RequestWatchStarBiwuZhengShiSai_QD(15)
end

--@endregion UIEvent

