require("3rdParty/ScriptEvent")
require("common/common_include")

local QnPopupList = import "L10.UI.QnPopupList"
local QnNewSlider = import "L10.UI.QnNewSlider"
local QnButton = import "L10.UI.QnButton"
local QnModelPreviewer = import "L10.UI.QnModelPreviewer"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local RotateMode = import "DG.Tweening.RotateMode"
local Random = import "UnityEngine.Random"
local QMPKStatueData = import "L10.Game.QMPKStatueData"


LuaQMPKStatueBuildWnd=class()

RegistClassMember(LuaQMPKStatueBuildWnd, "AniPopupList")
RegistClassMember(LuaQMPKStatueBuildWnd, "AniSlider")
RegistClassMember(LuaQMPKStatueBuildWnd, "RandomButton")
RegistClassMember(LuaQMPKStatueBuildWnd, "ModelPreviewer")
RegistClassMember(LuaQMPKStatueBuildWnd, "SaveButton")

RegistClassMember(LuaQMPKStatueBuildWnd, "AniList")
RegistClassMember(LuaQMPKStatueBuildWnd, "CurAniIndex")


RegistClassMember(LuaQMPKStatueBuildWnd, "StatueAniId")
RegistClassMember(LuaQMPKStatueBuildWnd, "StatuePreviewTime")

function LuaQMPKStatueBuildWnd:Init()

	if CQuanMinPKMgr.Inst.CurrentStatue==nil then
        CUIManager.CloseUI(CUIResources.QMPKStatuePopupMenu)
        return
    end

	self:InitClassMember()

end

function LuaQMPKStatueBuildWnd:InitClassMember()
	
	self.AniPopupList = self.transform:Find("Anchor/QnPopupList"):GetComponent(typeof(QnPopupList))
	self.AniSlider = self.transform:Find("Anchor/Label/NumberSlider"):GetComponent(typeof(QnNewSlider))
	self.RandomButton = self.transform:Find("Anchor/Label/RandomButton"):GetComponent(typeof(QnButton))
	self.ModelPreviewer = self.transform:Find("Anchor/QnModelPreviewer"):GetComponent(typeof(QnModelPreviewer))
	self.SaveButton = self.transform:Find("Anchor/SaveButton"):GetComponent(typeof(QnButton))
	self.CurAniIndex = -1

	--初始化动作下拉按钮
	self.AniPopupList.OnValueChanged = DelegateFactory.Action_int_string(function (index, name)
		self:OnAniValueChanged(index, name)
	end)
	-- 初始化帧进度条
	self.AniSlider.OnValueChanged = DelegateFactory.Action_float(function (percent)
		self:OnSliderValueChanged(percent)
	end)
	-- 随机数按钮
	self.RandomButton.OnClick = DelegateFactory.Action_QnButton(function (qnButton)
		self:OnRandomAniButtonClicked(qnButton)
	end)
	-- 保存设置
	self.SaveButton.OnClick = DelegateFactory.Action_QnButton(function (qnButton)
		self:OnSaveButtonClicked(qnButton)
	end)


	self.AniList = CreateFromClass(MakeGenericClass(List, UInt32))

	local aniNames = CreateFromClass(MakeGenericClass(List, cs_string))
	local cls = EnumToInt(CQuanMinPKMgr.Inst.CurrentStatue.FakeAppear.Class)

	-- 类别
	QuanMinPK_StatueAnimation.ForeachKey(function (key) 
		local data = QuanMinPK_StatueAnimation.GetData(key)
		if data.Class == cls then
			self.AniList:Add(key)
			aniNames:Add(data.AniName)
		end
    end)

    self.AniPopupList:Init(aniNames:ToArray())
    self.ModelPreviewer:PreviewFakeAppear(CQuanMinPKMgr.Inst.CurrentStatue.FakeAppear)
    self:ApplyGuildStatueData()
	
end

function LuaQMPKStatueBuildWnd:ApplyGuildStatueData()

	local statueData = QMPKStatueData.UnPack(CQuanMinPKMgr.Inst.CurrentStatue.FakeAppear.ExtraData.Data)
	if statueData then
		-- 加载动作
		local aniId = statueData.AnimationId
		local data = QuanMinPK_StatueAnimation.GetData(aniId)
		if data then
			for i = 0, self.AniList.Count-1 do
				if (self.AniList[i] == aniId) then
					self.CurAniIndex = i
					self.AniPopupList:ChangeTo(self.CurAniIndex)
				end
			end
			local ani = self.ModelPreviewer:GetAniName(data.Ani)
			self.StatueAniId = aniId
			self.AniSlider.m_Value = statueData.PreviewTime / self.ModelPreviewer:GetAniLength(ani)
			self.ModelPreviewer:PreviewAni(ani, self.StatuePreviewTime, data.HideWeapon > 0) 
		else
			self:OnRandomAniButtonClicked(nil)
		end

		-- 加载特效
		local fxId = statueData.FxId
		if fxId > 0 then
			self.ModelPreviewer:RemoveFx("qmpk_statue_fx")
			self.ModelPreviewer:AddFx("qmpk_statue_fx", fxId)
		end
	else
		self:OnRandomAniButtonClicked(nil)
	end
end

-- 切换动作
function LuaQMPKStatueBuildWnd:OnAniValueChanged(index, name)
	self.CurAniIndex = index
	local aniId = self.AniList[index]
	self.StatueAniId = aniId
	local data = QuanMinPK_StatueAnimation.GetData(aniId)
	if data then
		self.ModelPreviewer:DoAni(data.Ani, 0.5, data.HideWeapon > 0)
		self.AniSlider.OnValueChanged = nil
		self.AniSlider.m_Value = 1
		self.AniSlider.OnValueChanged = DelegateFactory.Action_float(function (percent)
			self:OnSliderValueChanged(percent)
		end)
	end
end

-- 切换帧
function LuaQMPKStatueBuildWnd:OnSliderValueChanged(percent)

	if self.CurAniIndex >= 0 and self.CurAniIndex <= self.AniList.Count then
		local aniId = self.AniList[self.CurAniIndex]
		local data = QuanMinPK_StatueAnimation.GetData(aniId)
		if data then
			local ani = self.ModelPreviewer:GetAniName(data.Ani)
			self.StatueAniId = aniId
			self.StatuePreviewTime = percent * self.ModelPreviewer:GetAniLength(ani)
			self.ModelPreviewer:PreviewAni(ani, self.StatuePreviewTime, data.HideWeapon > 0)
		end
	end
end

-- 随机数
function LuaQMPKStatueBuildWnd:OnRandomAniButtonClicked(go)
	if go then
		self:DoRandomAnimation(go.gameObject, 0)
	end
	self.AniPopupList:ChangeTo(Random.Range(0, self.AniList.Count))
	self.AniSlider.m_Value = Random.value
end

-- 筛子的动画
function LuaQMPKStatueBuildWnd:DoRandomAnimation(go, times)
	local v = go.transform.eulerAngles
	v.z = v.z - 180
	CommonDefs.OnComplete_Tweener(LuaTweenUtils.DORotate(go.transform, v, 0.05, RotateMode.Fast), DelegateFactory.TweenCallback(function () 
        if times < 6 then
        	self:DoRandomAnimation(go, times + 1)
        end
    end))
end

-- 保存
function LuaQMPKStatueBuildWnd:OnSaveButtonClicked(go)
	Gac2Gas.ReqeustChangeChampionStatue(CQuanMinPKMgr.Inst.CurrentStatue.EngineId, self.StatueAniId, self.StatuePreviewTime)
	CUIManager.CloseUI(CLuaUIResources.QMPKStatueBuildWnd)
end



function LuaQMPKStatueBuildWnd:OnEnable()
	--g_ScriptEvent:AddListener("RequestExchangeQianYuanSuccess", self, "Init")

end

function LuaQMPKStatueBuildWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("RequestExchangeQianYuanSuccess", self, "Init")
end


return LuaQMPKStatueBuildWnd
