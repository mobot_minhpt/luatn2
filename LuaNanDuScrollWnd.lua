local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Screen = import "UnityEngine.Screen"
local UIRoot = import "UIRoot"
local Ease = import "DG.Tweening.Ease"
local TweenScale = import "TweenScale"
local TweenPosition = import "TweenPosition"
local Animation = import "UnityEngine.Animation"

LuaNanDuScrollWnd = class()

--@region RegistChildComponent: Dont Modify Manually!


--@endregion RegistChildComponent end
LuaNanDuScrollWnd.animationName = "nanduscrollwnd_show"
LuaNanDuScrollWnd.duration = 10000
function LuaNanDuScrollWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.transform:GetComponent(typeof(UIPanel)).depth = CUIManager.LOADING_DEPTH + 2
end

function LuaNanDuScrollWnd:OnEnable()
    self.transform:GetComponent(typeof(UIPanel)).depth = CUIManager.LOADING_DEPTH + 2
end

function LuaNanDuScrollWnd:OnDisable()

end

function LuaNanDuScrollWnd:Init()
    self.transform:GetComponent(typeof(Animation)):Play(LuaNanDuScrollWnd.animationName)
    self.autoHideWndTicker = RegisterTickOnce(function()
        CUIManager.CloseUI(CLuaUIResources.NanDuScrollWnd)
    end, LuaNanDuScrollWnd.duration)
end

function LuaNanDuScrollWnd:OnDestroy()
    if self.autoHideWndTicker then
        UnRegisterTick(self.autoHideWndTicker)
    end
end


--@region UIEvent

--@endregion UIEvent

