local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnTableView = import "L10.UI.QnTableView"
local UISprite = import "UISprite"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local AlignType2=import "L10.UI.CPopupMenuInfoMgr+AlignType"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CRankData=import "L10.UI.CRankData"

LuaQiaoDuoRuYiRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQiaoDuoRuYiRankWnd, "MyRankItem", "MyRankItem", GameObject)
RegistChildComponent(LuaQiaoDuoRuYiRankWnd, "EmptyTipLabel", "EmptyTipLabel", GameObject)
RegistChildComponent(LuaQiaoDuoRuYiRankWnd, "TableView", "TableView", QnTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaQiaoDuoRuYiRankWnd, "m_RankData")
function LuaQiaoDuoRuYiRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_RankData = {}
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RankData
        end,
        function(item,row)
            self:InitItem(item,row)
        end)
end

function LuaQiaoDuoRuYiRankWnd:Init()
    self:RefreshDataView()
    Gac2Gas.QueryRank(41000261)
end

function LuaQiaoDuoRuYiRankWnd:RefreshDataView()
    self.TableView:ReloadData(false,false)
    self:InitMyRank()
end

function LuaQiaoDuoRuYiRankWnd:InitItem(item,row)
    local bg1 = item.transform:Find("bg1")
    local bg2 = item.transform:Find("bg2")
    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankSprite = item.transform:Find("RankLabel/RankSprite"):GetComponent(typeof(UISprite))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))

    bg1.gameObject:SetActive(row%2==0)
    bg2.gameObject:SetActive(row%2~=0)
    local data = self.m_RankData[row + 1]
    if data then
        local rank = tonumber(data.rank)
        rankLabel.text = rank
        if rank <= 3 then
            rankSprite.gameObject:SetActive(true)
            rankSprite.spriteName = "Rank_No."..rank
        else
            rankSprite.gameObject:SetActive(false)
        end
        local time = data.time
        local min = math.floor(time/60)
        local sec = time - min*60
        timeLabel.text = SafeStringFormat3(LocalString.GetString("%d分%d秒"),min,sec)
        nameLabel.text = data.name
    end
end

function LuaQiaoDuoRuYiRankWnd:InitMyRank()
    local item = self.MyRankItem
    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankSprite = item.transform:Find("RankLabel/RankSprite"):GetComponent(typeof(UISprite))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    rankSprite.gameObject:SetActive(false)
    local myRankInfo = CRankData.Inst.MainPlayerRankInfo
    if CClientMainPlayer.Inst then
        nameLabel.text = CClientMainPlayer.Inst.RealName
    else
        nameLabel.text = nil
    end
    
    if not myRankInfo or myRankInfo.Rank == 0  then
        rankLabel.text = LocalString.GetString("未上榜")
    else
        rankLabel.text = myRankInfo.Rank
    end
    if myRankInfo and myRankInfo.Value~=0 then
        local time = myRankInfo.Value
        local min = math.floor(time/60)
        local sec = time - min*60
        timeLabel.text = SafeStringFormat3(LocalString.GetString("%d分%d秒"),min,sec)
    else
        timeLabel.text = LocalString.GetString("无")
    end
end

function LuaQiaoDuoRuYiRankWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaQiaoDuoRuYiRankWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end
function LuaQiaoDuoRuYiRankWnd:OnRankDataReady( ... )
	self.m_RankData = {}

	if CRankData.Inst.RankList and CRankData.Inst.RankList.Count > 0 then
		do
			local i = 0
			while i < CRankData.Inst.RankList.Count do
				local info = CRankData.Inst.RankList[i]
        		local extraList = MsgPackImpl.unpack(info.extraData)
				local data = {name = info.Name,rank = info.Rank,time = info.Value}
                table.insert(self.m_RankData,data)
				i = i + 1
			end
			if i == 0 then
				self.EmptyTipLabel.gameObject:SetActive(true)
			else
				self.EmptyTipLabel.gameObject:SetActive(false)
			end
		end
    else
    	self.EmptyTipLabel.gameObject:SetActive(true)
	end

	self:RefreshDataView()
end

--@region UIEvent

--@endregion UIEvent

