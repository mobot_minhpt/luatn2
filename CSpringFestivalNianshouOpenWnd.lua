-- Auto Generated!!
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local ChunJie_NianShouDaZuoZhan = import "L10.Game.ChunJie_NianShouDaZuoZhan"
local ChunJie_NianShouMessage = import "L10.Game.ChunJie_NianShouMessage"
local CSpringFestivalMgr = import "L10.Game.CSpringFestivalMgr"
local CSpringFestivalNianshouOpenWnd = import "L10.UI.CSpringFestivalNianshouOpenWnd"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
CSpringFestivalNianshouOpenWnd.m_Init_CS2LuaHook = function (this) 
    this.labelTemplateNode:SetActive(false)
    this.iconTemplateNode:SetActive(false)

    UIEventListener.Get(this.closeBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)

    local restNum = (ChunJie_NianShouDaZuoZhan.GetData().DailyAwardLimit - CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eNSDZZReward))
    if restNum < 0 then
        restNum = 0
    end

    this.restTimeLabel.text = LocalString.GetString("剩余奖励次数:") .. tostring(restNum)

    if CSpringFestivalMgr.Instance.nianshouGameSignUp then
        CommonDefs.GetComponent_Component_Type(this.checkBtn.transform:Find("Label"), typeof(UILabel)).text = LocalString.GetString("取消报名")
        this.signedLabelNode:SetActive(true)
        UIEventListener.Get(this.checkBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
            Gac2Gas.RequestCancelSignUpNSDZZ()
            this:Close()
        end)
    else
        CommonDefs.GetComponent_Component_Type(this.checkBtn.transform:Find("Label"), typeof(UILabel)).text = LocalString.GetString("报 名")
        this.signedLabelNode:SetActive(false)
        UIEventListener.Get(this.checkBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
            Gac2Gas.RequestSignUpNSDZZ()
            this:Close()
        end)
    end

    local dataCount = ChunJie_NianShouMessage.GetDataCount()

    do
        local i = 1
        while i <= dataCount do
            local info = ChunJie_NianShouMessage.GetData(i)
            if info ~= nil then
                if info.Type == 1 then
                    local node = NGUITools.AddChild(this.label_table.gameObject, this.labelTemplateNode)
                    node:SetActive(true)
                    CommonDefs.GetComponent_Component_Type(node.transform:Find("text"), typeof(UILabel)).text = info.Description
                elseif info.Type == 2 then
                    local node = NGUITools.AddChild(this.icon_table.gameObject, this.iconTemplateNode)
                    node:SetActive(true)
                    CommonDefs.GetComponent_Component_Type(node.transform:Find("text"), typeof(UILabel)).text = info.Title .. info.Description
                    CommonDefs.GetComponent_Component_Type(node.transform:Find("icon"), typeof(CUITexture)):LoadMaterial(info.Icon)
                end
            end
            i = i + 1
        end
    end
    this.label_table:Reposition()
    this.label_scrollView:ResetPosition()
    this.icon_table:Reposition()
    this.icon_scrollView:ResetPosition()
end
