-- Auto Generated!!
local CChristmasCardWnd = import "L10.UI.CChristmasCardWnd"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local EnumVoicePlatform = import "L10.Game.EnumVoicePlatform"
local VoicePlayInfo = import "L10.Game.VoicePlayInfo"
local CTickMgr = import "L10.Engine.CTickMgr"
local ETickType = import "L10.Engine.ETickType"
CChristmasCardWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_CloseButton).onClick = MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.m_HasVoiceRoot).onClick = MakeDelegateFromCSFunction(this.OnPlayVoice, VoidDelegate, this)
    if CChristmasCardWnd.s_CardInfo ~= nil then
        this.m_RecieverNameLabel.text = CChristmasCardWnd.s_CardInfo.RecieverName
        this.m_SenderNameLabel.text = CChristmasCardWnd.s_CardInfo.SenderName .. LocalString.GetString(" 送")
        this.m_WishContentLabel.text = CChristmasCardWnd.s_CardInfo.wishContent
        this.m_HasVoiceRoot:SetActive(not System.String.IsNullOrEmpty(CChristmasCardWnd.s_CardInfo.voiceUrl))
        this.m_VoiceLengthLabel.text = System.String.Format(LocalString.GetString("{0:N0}秒"), CChristmasCardWnd.s_CardInfo.voideDuration)
        if System.String.IsNullOrEmpty(CChristmasCardWnd.s_CardInfo.SenderPortrait) then
            this.m_PortraitTexture:Init(CChristmasCardWnd.s_CardInfo.SenderId, CChristmasCardWnd.s_CardInfo.SenderClass, CChristmasCardWnd.s_CardInfo.SenderGender)
        end
        if CChristmasCardWnd.s_CardInfo.SenderTime > 0 then
            this.m_TimeLabel.gameObject:SetActive(true)
            local dateTime = CServerTimeMgr.ConvertTimeStampToZone8Time(CChristmasCardWnd.s_CardInfo.SenderTime)
            this.m_TimeLabel.text = LocalString.GetString("赠送时间：") .. ToStringWrap(dateTime, "yyyy-MM-dd HH:mm")
        else
            this.m_TimeLabel.gameObject:SetActive(false)
        end
        this:SetVoiceUI(CChristmasCardWnd.s_CardInfo.voiceUrl, CChristmasCardWnd.s_CardInfo.voideDuration)
    end
end
CChristmasCardWnd.m_SetVoiceUI_CS2LuaHook = function (this, voiceId, voiceDuration) 
    if System.String.IsNullOrEmpty(voiceId) then
        this.m_HasVoiceRoot.gameObject:SetActive(false)
    else
        this.m_HasVoiceRoot.gameObject:SetActive(true)
        this.m_SpriteAnimation:Pause()
        this.m_VoiceLengthLabel.text = System.String.Format(LocalString.GetString("{0:N0}秒"), voiceDuration)
        this.m_VoiceLengthSprite.width = math.min(800, math.floor(tonumber(voiceDuration * 80 or 0)))
    end
end
CChristmasCardWnd.m_hookOnPlayVoice = function (this, go) 
    if CChristmasCardWnd.s_CardInfo ~= nil then
        this.m_SpriteAnimation:Play()
        CTickMgr.Register(DelegateFactory.Action(function () 
            if this ~= nil and this.m_SpriteAnimation ~= nil then
                this.m_SpriteAnimation:Pause()
                this.m_SpriteAnimation.gameObject:GetComponent(typeof(UISprite)).spriteName = "chatwnd_voice_icon_2"
            end
        end), 1000 * CChristmasCardWnd.s_CardInfo.voideDuration, ETickType.Once)
        CUIManager.PlayVoice(CreateFromClass(VoicePlayInfo, CChristmasCardWnd.s_CardInfo.voiceUrl, EnumVoicePlatform.Default, nil))
    end
end
