-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CEquipmentInlayMgr = import "L10.Game.CEquipmentInlayMgr"
local CEquipmentIntensifyMgr = import "L10.Game.CEquipmentIntensifyMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CEquipmentBaptizeMgr = import "L10.Game.CEquipmentBaptizeMgr"
local CItemInfo = import "L10.Game.CItemInfo"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CStatusMgr = import "L10.Game.CStatusMgr"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumEquipmentTarget = import "L10.Game.EnumEquipmentTarget"
local EnumEvent = import "L10.Game.EnumEvent"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumQualityType = import "L10.Game.EnumQualityType"
local EPropStatus = import "L10.Game.EPropStatus"
local EquipBaptize_Color = import "L10.Game.EquipBaptize_Color"
local EquipBaptize_Cost = import "L10.Game.EquipBaptize_Cost"
local EquipBaptize_WordOption = import "L10.Game.EquipBaptize_WordOption"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local IdPartition = import "L10.Game.IdPartition"
local Object = import "System.Object"
local Constants = import "L10.Game.Constants"
CEquipmentProcessMgr.m_hookShowWithEquip = function (tabIndex, subTabIndex, itemId, place, pos, closeTipWhenShow) 

    local result = CStatusMgr.Inst:CheckConflict(CClientMainPlayer.Inst, EnumEvent.GetId("EquipmentOperation"))
    if result ~= nil then
        if result.msgs ~= nil then
            local datas = CreateFromClass(MakeGenericClass(List, Object))
            do
                local i = 1
                while i < result.msgs.Length do
                    CommonDefs.ListAdd(datas, typeof(Object), result.msgs[i])
                    i = i + 1
                end
            end
            g_MessageMgr:ShowMessage(result.msgs[0], List2Params(datas))
        end
        return
    end

    if System.String.IsNullOrEmpty(itemId) then
        return
    end

    local item = CItemMgr.Inst:GetById(itemId)
    if item == nil or (not IdPartition.IdIsEquip(item.TemplateId)) or IdPartition.IdIsTalisman(item.TemplateId) then
        return
    end

    if closeTipWhenShow then
        CItemInfoMgr.CloseItemInfoWnd()
    end

    local equipInfo = CreateFromClass(CItemInfo)
    equipInfo.templateId = item.TemplateId
    equipInfo.itemId = itemId
    equipInfo.place = place
    equipInfo.pos = pos
    CEquipmentProcessMgr.Inst.selectedEquip = equipInfo

    CEquipmentProcessMgr.Inst.TabIndex = tabIndex
    CEquipmentProcessMgr.Inst.SubTabIndex = subTabIndex
    CUIManager.ShowUI(CUIResources.EquipmentProcessWnd)
end
CEquipmentProcessMgr.m_hookIsItemMatchForTarget = function (this, itemId, target) 

    if target == EnumEquipmentTarget.Baptize then
        return this:CanBaptize(itemId)
    end
    if target == EnumEquipmentTarget.Intensify then
        return this:CanIntensify(itemId)
    end
    if target == EnumEquipmentTarget.Inlay then
        return this:CanInlay(itemId)
    end
    if target == EnumEquipmentTarget.ZhiHuan then
        return this:CanZhiHuan(itemId)
    end
    return true
end
CEquipmentProcessMgr.m_hookCheckBaptizeCostNum = function (this) 
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    if equip == nil then
        return false
    end

    local equipItem = CEquipmentProcessMgr.Inst.BaptizingEquipment
    local cost = CEquipmentProcessMgr.Inst:GetBaptizeCost(equip)
    if cost ~= nil then
        local wordCostMatNum = this:GetWordCostMatNumInBag()

        if cost.WordCost[1] > wordCostMatNum then
            return false
        end
    end
    return true
end
CEquipmentProcessMgr.m_hookGetWordCostMatNumInBag = function (this) 

    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    if equipInfo == nil then
        return 0
    end
    local equipment = CItemMgr.Inst:GetById(equipInfo.itemId)
    local cost = CEquipmentProcessMgr.Inst:GetBaptizeCost(equipInfo)
    if cost == nil then
        return 0
    end
    if equipment == nil then
        return -1
    end

    local wordCostMatNum = 0

    local bindCount, notBindCount
    bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(cost.WordCost[0])
    wordCostMatNum = wordCostMatNum + (bindCount + notBindCount)
	
	-- 红蓝净瓶积分
	if CSwitchMgr.EnableJingPingScore and CClientMainPlayer.Inst ~= nil then
		if cost.WordCost[0] == Constants.JingPing_Red then
			local playScore = CClientMainPlayer.Inst.PlayProp.Scores[LuaEnumPlayScoreKey.HongJingPing] or 0
			wordCostMatNum = wordCostMatNum + playScore
		elseif cost.WordCost[0] == Constants.JingPing_Blue then
			local playScore = CClientMainPlayer.Inst.PlayProp.Scores[LuaEnumPlayScoreKey.LanJingPing] or 0
			wordCostMatNum = wordCostMatNum + playScore
		end
	end

    if equipment.Equip.Color == EnumQualityType.Blue or equipment.Equip.Color == EnumQualityType.Red then
        bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(this.baptizeWordCostBaseItemId)
        wordCostMatNum = wordCostMatNum + (bindCount + notBindCount)
    end
    --不绑定 不消耗玉净瓶
    if CSwitchMgr.EnableYuJingPing then
        if equipment.IsBinded then
            local bindCount2, notBindCount2
            bindCount2, notBindCount2 = CItemMgr.Inst:CalcItemCountInBagByTemplateIdExclueExpireTime(CEquipmentProcessMgr.YuJingPingItemId)
            wordCostMatNum = wordCostMatNum + (bindCount2 + notBindCount2)
        end
    end

    return wordCostMatNum
end
CEquipmentProcessMgr.m_hookGetWordOption = function (this, type, subType) 
    local data = nil
    EquipBaptize_WordOption.ForeachKey(DelegateFactory.Action_object(function (k) 
        if data == nil then
            local temp = EquipBaptize_WordOption.GetData(k)
            if temp.Type == type and temp.SubType == subType then
                data = temp
            end
        end
    end))
    return data
end
CEquipmentProcessMgr.m_hookGetBaptizeCost = function (this, equip) 
    if equip == nil then
        return nil
    end

    local commonItem = CItemMgr.Inst:GetById(equip.itemId)
    if commonItem == nil then
        return nil
    end
    local template = EquipmentTemplate_Equip.GetData(commonItem.TemplateId)

    local cost = nil
    local grade = commonItem.Equip.Grade
    local color = EnumToInt(commonItem.Equip.Color)
    local list = CreateFromClass(MakeGenericClass(List, EquipBaptize_Cost))
	EquipBaptize_Cost.ForeachKey(DelegateFactory.Action_object(function (key)
		local val = EquipBaptize_Cost.GetData(key)
		if val.Color == color then
            CommonDefs.ListAdd(list, typeof(EquipBaptize_Cost), val)
		end
	end))
    do
        local i = 0
        while i < list.Count do
            local val = list[i]
            if grade >= val.TCRange[0] and grade <= val.TCRange[1] then
                cost = val
            end
            i = i + 1
        end
    end
    return cost
end
CEquipmentProcessMgr.m_hookCanBaptize = function (this, itemId) 

    local commonItem = CItemMgr.Inst:GetById(itemId)
    if commonItem ~= nil and commonItem.IsEquip then
        local template = EquipmentTemplate_Equip.GetData(commonItem.TemplateId)
        local find = false
        EquipBaptize_Color.ForeachKey(DelegateFactory.Action_object(function (key) 
			local val = EquipBaptize_Color.GetData(key)
			if val.Color == EnumToInt(commonItem.Equip.Color) then
                find = true
            end
		end))
        return find
    end
    return false
end
CEquipmentProcessMgr.m_hookCanIntensify = function (this, itemId) 
    if CEquipmentIntensifyMgr.Inst:CheckEquipTypeCanIntensify(itemId) then
        return true
    end
    return false
end
CEquipmentProcessMgr.m_hookCanInlay = function (this, itemId) 
    if CEquipmentInlayMgr.Inst:CheckEquipTypeCanInlay(itemId) then
        return true
    end
    return false
end
CEquipmentProcessMgr.m_hookCanZhiHuan = function(this, itemId)
	if CEquipmentBaptizeMgr.Inst:CanZhiHuan(itemId) then
		return true
	end
	return false
end
CEquipmentProcessMgr.m_hookCheckOperationConflict = function (this, equip) 
    if equip == nil then
        return true
    end
    local operation = "EquipmentOperation"
    if equip.place == EnumItemPlace.Bag then
        operation = "BagEquipmentOperation"
    end
    local result = CStatusMgr.Inst:CheckConflict(CClientMainPlayer.Inst, EnumEvent.GetId(operation))
    if result ~= nil then
        if result.msgs ~= nil then
            local datas = CreateFromClass(MakeGenericClass(List, Object))
            do
                local i = 1
                while i < result.msgs.Length do
                    CommonDefs.ListAdd(datas, typeof(Object), result.msgs[i])
                    i = i + 1
                end
            end
            g_MessageMgr:ShowMessage(result.msgs[0], List2Params(datas))
        end
        return false
    end

    return true
end
CEquipmentProcessMgr.m_hookOnMainPlayerCreate = function (this) 
    --尝试修复一下打造状态
    if not CUIManager.IsLoaded(CUIResources.EquipmentProcessWnd) and CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("EquipForge")) == 1 then
        CStatusMgr.Inst:SetStatusWithSync(CClientMainPlayer.Inst, EPropStatus.GetId("EquipForge"), 0)
    end
end

