local CUITexture = import "L10.UI.CUITexture"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"
local EnumGender = import "L10.Game.EnumGender"
local Animation = import "UnityEngine.Animation"
local QnTabButton = import "L10.UI.QnTabButton"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local QnRadioBox = import "L10.UI.QnRadioBox"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CPinchFaceCinemachineCtrlMgr = import "L10.Game.CPinchFaceCinemachineCtrlMgr"
local CButton = import "L10.UI.CButton"

LuaPinchFacePreselectionView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPinchFacePreselectionView, "TwoDimenSlider", "TwoDimenSlider", CCommonLuaScript)
RegistChildComponent(LuaPinchFacePreselectionView, "RandomButton", "RandomButton", GameObject)
RegistChildComponent(LuaPinchFacePreselectionView, "FaceTabBar", "FaceTabBar", UITabBar)
RegistChildComponent(LuaPinchFacePreselectionView, "TabBar1", "TabBar1", UITabBar)
RegistChildComponent(LuaPinchFacePreselectionView, "LeftStyleScrollView", "LeftStyleScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaPinchFacePreselectionView, "LeftStyleQnRadioBox", "LeftStyleQnRadioBox", QnRadioBox)
RegistChildComponent(LuaPinchFacePreselectionView, "StyleTemplate", "StyleTemplate", GameObject)
RegistChildComponent(LuaPinchFacePreselectionView, "StyleView", "StyleView", GameObject)
RegistChildComponent(LuaPinchFacePreselectionView, "LuoZhuangBtn", "LuoZhuangBtn", CButton)
RegistChildComponent(LuaPinchFacePreselectionView, "ZhuShiButton", "ZhuShiButton", CButton)
RegistChildComponent(LuaPinchFacePreselectionView, "ResetXYBtn", "ResetXYBtn", CButton)
RegistChildComponent(LuaPinchFacePreselectionView, "SoundSettingBtn", "SoundSettingBtn", CButton)
--@endregion RegistChildComponent end
RegistClassMember(LuaPinchFacePreselectionView, "m_XValue")
RegistClassMember(LuaPinchFacePreselectionView, "m_YValue")
RegistClassMember(LuaPinchFacePreselectionView, "m_XValue2")
RegistClassMember(LuaPinchFacePreselectionView, "m_YValue2")
RegistClassMember(LuaPinchFacePreselectionView, "m_IsInit")
RegistClassMember(LuaPinchFacePreselectionView, "m_WaitLoadResourcesFinishedTick")
RegistClassMember(LuaPinchFacePreselectionView, "m_TwoDimenSliderFxArr")
RegistClassMember(LuaPinchFacePreselectionView, "m_StyleDataList")
RegistClassMember(LuaPinchFacePreselectionView, "m_TabBar1Index")
RegistClassMember(LuaPinchFacePreselectionView, "m_FaceTabIndex")
RegistClassMember(LuaPinchFacePreselectionView, "m_HasClickedBtn2")

function LuaPinchFacePreselectionView:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.RandomButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRandomButtonClick()
	end)


    --@endregion EventBind end
	self.FaceTabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnFaceTabBarChange(go, index)
	end)

	self.TabBar1:ChangeTab(0, true)
	self.TabBar1.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabBar1Change(go, index)
	end)

	UIEventListener.Get(self.LuoZhuangBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLuoZhuangBtnClick()
	end)

	UIEventListener.Get(self.ZhuShiButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnZhuShiButtonClick()
	end)

	UIEventListener.Get(self.SoundSettingBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSoundSettingBtnClick()
	end)
end

function LuaPinchFacePreselectionView:Start()
	if not LuaPinchFaceMgr.m_InitialPreselectionX then LuaPinchFaceMgr.m_InitialPreselectionX = 0 end
	if not LuaPinchFaceMgr.m_InitialPreselectionY then LuaPinchFaceMgr.m_InitialPreselectionY = 0 end
	if not self.m_XValue then self.m_XValue = LuaPinchFaceMgr.m_InitialPreselectionX end
	if not self.m_YValue then self.m_YValue = LuaPinchFaceMgr.m_InitialPreselectionY end
	if not self.m_XValue2 then self.m_XValue2 = LuaPinchFaceMgr.m_InitialPreselectionX end
	if not self.m_YValue2 then self.m_YValue2 = LuaPinchFaceMgr.m_InitialPreselectionY end
	if not LuaPinchFaceMgr.m_PreselectionIndex then 
		LuaPinchFaceMgr.m_PreselectionIndex = LuaPinchFaceMgr.m_InitialPreselectionIndex
	end
	self:InitTwoDimenSliderFxArr()
	self.TwoDimenSlider.transform:Find("CoordinateAxisBg/Thumb").gameObject:SetActive(true)
	self.StyleTemplate.gameObject:SetActive(false)
	self.m_FaceTabIndex = 0
	self:Init()
	self:ShowView()
end

function LuaPinchFacePreselectionView:Init()
	self:InitTwoDimenSlider()
	self:InitFaceTabBar()
	self:InitStyleView()
	self.m_IsInit = true
end

function LuaPinchFacePreselectionView:ShowView()
	if not self.m_IsInit then return end
	if CPinchFaceCinemachineCtrlMgr.Inst then
        CPinchFaceCinemachineCtrlMgr.Inst:ShowCam(1)
    end
	self.TabBar1:ChangeTab(LuaPinchFaceMgr.m_PreselectionViewTabIndex, false)
	self:ProcessTab1Change(self.m_TabBar1Index)
	LuaPinchFaceMgr:RemoveUIFacialEffectMat()
	local mainAni = self.transform:GetComponent(typeof(Animation))
	mainAni:Stop()
	mainAni:Play()
    self.LuoZhuangBtn.gameObject:SetActive(true)
    self:OnShowLuoZhuangPinchFace()
	self:OnSetPinchFaceIKLookAtTo()
	LuaPinchFaceMgr:RotateRoleModel(180)

	if not self.m_HasClickedBtn2 then
		local btn = self.TabBar1:GetTabGoByIndex(1)
		local fx = btn.transform:Find("Normal_vfx")
		fx.gameObject:SetActive(false)
		fx.gameObject:SetActive(true)
	end
end

function LuaPinchFacePreselectionView:GetMyXYValue(index)
	if self.m_FaceTabIndex == 0 then
		return self.m_XValue, self.m_YValue
	else
		return self.m_XValue2, self.m_YValue2
	end
end

function LuaPinchFacePreselectionView:ShowInitialTwoDimenSliderFx()
	local x, y = self:GetMyXYValue()
	self:InitTwoDimenSliderFxArr()
	self:ShowTwoDimenSliderFx(x, y)
	local curObj = self:GetTwoDimenSliderFx(x, y)
	if curObj then
		curObj.gameObject:SetActive(true)
		local curAni = curObj:GetComponent(typeof(Animation))
		curAni:Stop()
		curAni:Play("pinchfacepreselectionview_xiangxian_show")
	end
end

function LuaPinchFacePreselectionView:InitFaceTabBar()
	local skinData = LuaPinchFaceMgr:GetCurFaceSkinData()
	local hasFeisheng = LuaPinchFaceMgr:IsHasFeiSheng()
	if skinData then
		local btn1 = self.FaceTabBar.transform:GetChild(0):GetComponent(typeof(QnTabButton))
		local btn2 = self.FaceTabBar.transform:GetChild(1):GetComponent(typeof(QnTabButton))
		btn1.transform:GetComponent(typeof(CUITexture)):LoadMaterial(hasFeisheng and skinData.Face3 or skinData.Face1)
		btn2.transform:GetComponent(typeof(CUITexture)):LoadMaterial(hasFeisheng and skinData.Face1 or skinData.Face2)
	end
	local defaultPreselectionIndex = LuaPinchFaceMgr.m_IsModifyXianSheng and 1 or 0
	local defaultTabIndex = defaultPreselectionIndex == LuaPinchFaceMgr.m_InitialPreselectionIndex and 0 or 1
	self.FaceTabBar:ChangeTab(defaultTabIndex, true)
    self.FaceTabBar.tabButtons[0].transform:Find("Highlight").gameObject:SetActive(defaultTabIndex == 0)
    self.FaceTabBar.tabButtons[1].transform:Find("Highlight").gameObject:SetActive(defaultTabIndex == 1)
end

function LuaPinchFacePreselectionView:InitTwoDimenSlider()
	if not LuaPinchFaceMgr.m_CurEnumGender then return end
	local desArr = LuaPinchFaceMgr.m_CurEnumGender == EnumGender.Male and 
		{LocalString.GetString("狂"),LocalString.GetString("雅"),LocalString.GetString("敦"),LocalString.GetString("锐")} or
		{LocalString.GetString("柔"),LocalString.GetString("飒"),LocalString.GetString("稚"),LocalString.GetString("媚")}
	self.TwoDimenSlider:SetLabels(LocalString.GetString("气质"), desArr)
	self.TwoDimenSlider:Init(function (x, y)
			self:ShowTwoDimenSliderFx(x, y)
			self:SetPreselection(x, y)
		end)
	self.TwoDimenSlider.gameObject:SetActive(LuaPinchFaceMgr:IsLoadedAllPreselectionModelAvatorData())
	local x, y = self:GetMyXYValue()
	self.TwoDimenSlider:SetValue(x, y, false)
	self:ShowInitialTwoDimenSliderFx()
end

function LuaPinchFacePreselectionView:InitStyleView()
	self.m_StyleDataList = {}
	PinchFace_PreselectionCustomStyle.ForeachKey(function (k)
		local data = PinchFace_PreselectionCustomStyle.GetData(k)
		if  data.Race == nil or (data.Race and LuaPinchFaceMgr.m_CurEnumClass and CommonDefs.HashSetContains(data.Race, typeof(UInt32), EnumToInt(LuaPinchFaceMgr.m_CurEnumClass))) then
            if data.GenderLimit == 0 or (LuaPinchFaceMgr.m_CurEnumGender == EnumGender.Male and data.GenderLimit == 1) or (LuaPinchFaceMgr.m_CurEnumGender == EnumGender.Female and data.GenderLimit == 2) then
                table.insert(self.m_StyleDataList, data)
            end
        end
	end)
	table.sort(self.m_StyleDataList, function (a, b)
		if a.SelectionSeq == b.SelectionSeq then
			return a.ID < b.ID
		end
		return a.SelectionSeq < b.SelectionSeq
	end)
	self.LeftStyleQnRadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), #self.m_StyleDataList)
    local grid = self.LeftStyleQnRadioBox.m_Table
    for i = 1, #self.m_StyleDataList do
        local go = NGUITools.AddChild(grid.gameObject, self.StyleTemplate.gameObject)
        go.gameObject:SetActive(true)
        local btn = go.gameObject:GetComponent(typeof(QnSelectableButton))
		
		local label = btn.transform:Find("Label"):GetComponent(typeof(UILabel))
		label.text = self.m_StyleDataList[i].SelectionName
        
		btn:SetSelected(false, false)
        self.LeftStyleQnRadioBox.m_RadioButtons[i - 1] = btn
		btn.OnClick = MakeDelegateFromCSFunction(self.LeftStyleQnRadioBox.On_Click, MakeGenericClass(Action1, QnButton), self.LeftStyleQnRadioBox)
    end
    self.LeftStyleQnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
		self:OnSelectLeftStyleQnRadioBox(btn,index)
    end)
    grid:Reposition()
    self.LeftStyleScrollView:ResetPosition()
end

function LuaPinchFacePreselectionView:ShowTwoDimenSliderFx(x, y)
	self.TwoDimenSlider.transform:Find("CoordinateAxisBg/CUIFx_xh").gameObject:SetActive(x == 0 and y == 0)
	local lastx, lasty = self:GetMyXYValue()
	local lastObj = self:GetTwoDimenSliderFx(lastx, lasty)
	local curObj = self:GetTwoDimenSliderFx(x, y)
	if lastObj ~= curObj then 
		if lastObj then
			lastObj.gameObject:SetActive(true)
			local lastAni = lastObj:GetComponent(typeof(Animation))
			lastAni:Stop()
			lastAni:Play("pinchfacepreselectionview_xiangxian_hide")
		end
		if curObj then
			curObj.gameObject:SetActive(true)
			local curAni = curObj:GetComponent(typeof(Animation))
			curAni:Stop()
			curAni:Play("pinchfacepreselectionview_xiangxian_show")
		end
	end
end

function LuaPinchFacePreselectionView:SetPreselection(x, y)
	if self.m_FaceTabIndex == 0 then
		self.m_XValue, self.m_YValue = x, y
	else
		self.m_XValue2, self.m_YValue2 = x, y
	end
	LuaPinchFaceMgr:SetPreselection(x, y)
end

function LuaPinchFacePreselectionView:GetTwoDimenSliderFx(x, y)
	if x and y then
		if x > 0 and y > 0 then
			return self.m_TwoDimenSliderFxArr[1]
		elseif x > 0 and y < 0 then
			return self.m_TwoDimenSliderFxArr[2]
		elseif x < 0 and y > 0 then
			return self.m_TwoDimenSliderFxArr[3]
		elseif x < 0 and y < 0 then
			return self.m_TwoDimenSliderFxArr[4]
		end
	end
	return nil
end

function LuaPinchFacePreselectionView:InitTwoDimenSliderFxArr()
	self.m_TwoDimenSliderFxArr = {}
	table.insert(self.m_TwoDimenSliderFxArr, self.TwoDimenSlider.transform:Find("CoordinateAxisBg/CUIFx_dianji/right1"))
	table.insert(self.m_TwoDimenSliderFxArr, self.TwoDimenSlider.transform:Find("CoordinateAxisBg/CUIFx_dianji/right2"))
	table.insert(self.m_TwoDimenSliderFxArr, self.TwoDimenSlider.transform:Find("CoordinateAxisBg/CUIFx_dianji/left1"))
	table.insert(self.m_TwoDimenSliderFxArr, self.TwoDimenSlider.transform:Find("CoordinateAxisBg/CUIFx_dianji/left2"))
	for i = 1, #self.m_TwoDimenSliderFxArr do
		self.m_TwoDimenSliderFxArr[i].gameObject:SetActive(false)
	end
end
--@region UIEvent

function LuaPinchFacePreselectionView:OnRandomButtonClick()
	local x = math.random()
	local y = math.random()
	self.TwoDimenSlider:SetValue(x * 2 - 1, y * 2 - 1, true)
end

function LuaPinchFacePreselectionView:OnFaceTabBarChange(go, index)
	self.m_FaceTabIndex = index
	local x, y = self:GetMyXYValue()
	if not x then x = LuaPinchFaceMgr.m_InitialPreselectionX end
	if not y then y = LuaPinchFaceMgr.m_InitialPreselectionY end
	self.TwoDimenSlider:SetValue(x, y, true)
	local preselectionIndex = index
	if LuaPinchFaceMgr:IsHasFeiSheng() then
		preselectionIndex = preselectionIndex == 0 and 1 or 0
	end
	LuaPinchFaceMgr:SetCharacterPreselectionIndex(preselectionIndex)
	LuaPinchFaceMgr:SetCharacterPreselectionHair(preselectionIndex)
	self:ShowInitialTwoDimenSliderFx()
end

function LuaPinchFacePreselectionView:OnTabBar1Change(go, index)
	if index == 1 and not self.m_HasClickedBtn2 then
		self.m_HasClickedBtn2 = true
		local btn = self.TabBar1:GetTabGoByIndex(1)
		local fx = btn.transform:Find("Normal_vfx")
		fx.gameObject:SetActive(false)
	end
	if (LuaPinchFaceMgr.m_IsFaceChanged or LuaPinchFaceMgr.m_UseCurFacialData) and self.m_TabBar1Index == 1 and index == 0 then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("CharacterCreationRebackDetailViewConfirm"), DelegateFactory.Action(function ()
			LuaPinchFaceMgr.m_IsFaceChanged = false
			LuaPinchFaceMgr.m_UseCurFacialData = false
			self:ProcessTab1Change(index)
			if LuaPinchFaceMgr.m_IsInCreateMode then
				LuaPinchFaceMgr:ResetHairColor()
			end
			LuaPinchFaceMgr:SetCharacterPreselectionIndex(LuaPinchFaceMgr.m_PreselectionIndex)
			local x, y = self:GetMyXYValue()
			self.TwoDimenSlider:SetValue(x, y,true)
			LuaPinchFaceMgr:InitCommandMode()
        end), DelegateFactory.Action(function ()
			self.TabBar1:ChangeTab(1, false)
        end), nil, nil, false)
	else
		self:ProcessTab1Change(index)
	end
end

function LuaPinchFacePreselectionView:ProcessTab1Change(index)
	self.m_TabBar1Index = index
	self.FaceTabBar.gameObject:SetActive(index == 0)
	self.TwoDimenSlider.gameObject:SetActive(index == 0)
	self.StyleView.gameObject:SetActive(index == 1)
	self.TabBar1.tabButtons[0].transform:Find("Normal").gameObject:SetActive(index ~= 0)
    self.TabBar1.tabButtons[0].transform:Find("Highlight").gameObject:SetActive(index == 0)
    self.TabBar1.tabButtons[1].transform:Find("Normal").gameObject:SetActive(index ~= 1)
    self.TabBar1.tabButtons[1].transform:Find("Highlight").gameObject:SetActive(index == 1)
	self.RandomButton.gameObject:SetActive(index == 0)
	self.ResetXYBtn.gameObject:SetActive(index == 0)
	if CPinchFaceCinemachineCtrlMgr.Inst then
        CPinchFaceCinemachineCtrlMgr.Inst:ShowCam(index == 0 and 1 or 0)
    end
	self:ShowInitialTwoDimenSliderFx()
	LuaPinchFaceMgr:PreviewStand(self.m_TabBar1Index == 0 and EnumPinchFaceAniState.PreviewStand01 or EnumPinchFaceAniState.PlayStand01)
end

function LuaPinchFacePreselectionView:OnSelectLeftStyleQnRadioBox(btn,index)
	local data = self.m_StyleDataList[index + 1]

	local fanshionData = {isDefault = true}
	fanshionData = {data= PinchFace_PreviewFashion.GetData(data.PreviewFashion)}
	LuaPinchFaceMgr:SetFashion(fanshionData, EnumPinchFaceAniState.PlayStand01, false) 

	local environmentFxData = {isDefault = true}
	if data.PreviewEnvironmentFx and data.PreviewEnvironmentFx > 0 then
		environmentFxData = PinchFace_PreviewEnvironmentFx.GetData(data.PreviewEnvironmentFx)
	end
	LuaPinchFaceMgr:SetEnvironmentFx(environmentFxData)

	local bgColorData = PinchFace_BgColor.GetData(data.BgColor)
	LuaPinchFaceMgr:SetCameraBgColor(bgColorData)
end

function LuaPinchFacePreselectionView:OnLuoZhuangBtnClick()
    LuaPinchFaceMgr.m_IsShowLuoZhuangHair = not LuaPinchFaceMgr.m_IsShowLuoZhuangHair
    self:OnShowLuoZhuangPinchFace()
    LuaPinchFaceMgr:SetFashion(LuaPinchFaceMgr.m_FashionData, self.m_TabBar1Index == 0 and EnumPinchFaceAniState.PreviewStand01 or EnumPinchFaceAniState.PlayStand01, LuaPinchFaceMgr.m_IsShowLuoZhuangHair)
end

function LuaPinchFacePreselectionView:OnZhuShiButtonClick()
	self.ZhuShiButton.Selected = not self.ZhuShiButton.Selected
    LuaPinchFaceMgr:SetIKLookAtTo(self.ZhuShiButton.Selected)
end

function LuaPinchFacePreselectionView:OnSoundSettingBtnClick()
	self.SoundSettingBtn.Selected = true
	LuaMiniVolumeSettingsMgr:OpenWnd(self.SoundSettingBtn.gameObject, self.SoundSettingBtn.transform.position, UIWidget.Pivot.Top, function ()
		self.SoundSettingBtn.Selected = false
	end)
end
--@endregion UIEvent

function LuaPinchFacePreselectionView:OnEnable()
	g_ScriptEvent:AddListener("OnLoadedAllPreselectionModelAvatarData", self, "OnLoadedAllPreselectionModelAvatarData")
	g_ScriptEvent:AddListener("OnShowLuoZhuangPinchFace", self, "OnShowLuoZhuangPinchFace")
	g_ScriptEvent:AddListener("OnSetPinchFaceIKLookAtTo", self, "OnSetPinchFaceIKLookAtTo")
	self:ShowView()
end

function LuaPinchFacePreselectionView:OnDisable()
	g_ScriptEvent:RemoveListener("OnLoadedAllPreselectionModelAvatarData", self, "OnLoadedAllPreselectionModelAvatarData")
	g_ScriptEvent:RemoveListener("OnShowLuoZhuangPinchFace", self, "OnShowLuoZhuangPinchFace")
	g_ScriptEvent:RemoveListener("OnSetPinchFaceIKLookAtTo", self, "OnSetPinchFaceIKLookAtTo")
end

function LuaPinchFacePreselectionView:OnSetPinchFaceIKLookAtTo()
    self.ZhuShiButton.Selected = LuaPinchFaceMgr.m_IsUseIKLookAt
end

function LuaPinchFacePreselectionView:OnLoadedAllPreselectionModelAvatarData()
	self.TwoDimenSlider.gameObject:SetActive(LuaPinchFaceMgr:IsLoadedAllPreselectionModelAvatorData())
end

function LuaPinchFacePreselectionView:OnShowLuoZhuangPinchFace()
	self.LuoZhuangBtn.Selected = not LuaPinchFaceMgr.m_IsShowLuoZhuangHair
end