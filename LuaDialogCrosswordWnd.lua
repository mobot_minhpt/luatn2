local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Transform = import "UnityEngine.Transform"
local UILabel = import "UILabel"
local CUIFx = import "L10.UI.CUIFx"
local CButton = import "L10.UI.CButton"
local Vector2 = import "UnityEngine.Vector2"

LuaDialogCrosswordWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDialogCrosswordWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaDialogCrosswordWnd, "Words", "Words", Transform)
RegistChildComponent(LuaDialogCrosswordWnd, "TargetLab", "TargetLab", UILabel)
RegistChildComponent(LuaDialogCrosswordWnd, "BackLab", "BackLab", UILabel)
RegistChildComponent(LuaDialogCrosswordWnd, "FinishFx", "FinishFx", CUIFx)
RegistChildComponent(LuaDialogCrosswordWnd, "DescTitle", "DescTitle", UILabel)
RegistChildComponent(LuaDialogCrosswordWnd, "CloseButton", "CloseButton", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaDialogCrosswordWnd, "m_Tick")
RegistClassMember(LuaDialogCrosswordWnd, "m_Words")
RegistClassMember(LuaDialogCrosswordWnd, "m_WordsGo")
RegistClassMember(LuaDialogCrosswordWnd, "m_DragOffset")
RegistClassMember(LuaDialogCrosswordWnd, "m_Fields")
RegistClassMember(LuaDialogCrosswordWnd, "m_SuccessFields")
RegistClassMember(LuaDialogCrosswordWnd, "m_Mask")

function LuaDialogCrosswordWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_Mask = self.transform:Find("Mask").gameObject
    self.Template:SetActive(false)
end

function LuaDialogCrosswordWnd:Start()
    CommonDefs.AddOnClickListener(self.CloseButton.gameObject, DelegateFactory.Action_GameObject(function(go)
        self:OnCloseBtnClicked(go)
    end), false)
end

function LuaDialogCrosswordWnd:InitGame(gameId, callback)
    UnRegisterTick(self.m_Tick)
    self.m_Mask:SetActive(true)
    self.CloseButton.gameObject:SetActive(true)
    self:LoadDesignData(gameId)
    
    self.FinishFx:DestroyFx()
    self:InitCrosswordFields()
    self:InitWords()

    self.m_IsPlaying = true
    self.m_Callback = callback
end

function LuaDialogCrosswordWnd:LoadDesignData(gameId)
    local data = ScrollTask_CrosswordGame.GetData(gameId)
    if data == nil then
        return
    end

    self.m_TargetText = data.Crossword
    --self.m_Words = data.m_Words
    self.m_Words = {}
    local length = data.Words.Length < 8 and data.Words.Length or 8
    for i = 0, length - 1 do
        table.insert(self.m_Words, data.Words[i])
    end
end
--@region UIEvent

--@endregion UIEvent

function LuaDialogCrosswordWnd:InitCrosswordFields()
    self.m_Fields = {}
    self.m_SuccessFields = {}
    

    -- 处理前景，替换其他字符
    local foregroundText = string.gsub(self.m_TargetText, "[^%$%d%s]*", function (s)
        return s ~= "" and SafeStringFormat3("[c][ffffffff]%s[-][/c]", s) or s
    end)

    -- 处理前景，替换[]
    foregroundText = string.gsub(foregroundText, "%$%d+", function (s)
        local index = tonumber(string.match(s, "%$(%d+)"))
        local wordStr = self.m_Words[index]
        table.insert(self.m_Fields, wordStr)
        local replaceStr = SafeStringFormat3("[c][ffffffff][url=%s]%s[/url][-][/c]", #self.m_Fields, " " .. string.rep("_", string.len(wordStr)) .. " ")
        table.insert(self.m_SuccessFields, false)
        return replaceStr
    end)

    self.TargetLab.text = foregroundText

    self.BackLab.text = ""
end

function LuaDialogCrosswordWnd:RefreshCrosswordFields()
    local foregroundText = self.TargetLab.text
    for i = 1, #self.m_SuccessFields do
        if self.m_SuccessFields[i] == true then
            local s = self.m_Fields[i]
            local replaceStr = "%[c%]%[ffffffff%]%[url="..i.."%]".." "..string.rep("_", string.len(s)) .. " ".."%[%/url%]%[%-%]%[%/c%]"
            foregroundText = string.gsub(foregroundText, replaceStr, CommonDefs.IS_VN_CLIENT and (s .." ") or s)
        end
    end
    self.TargetLab.text = foregroundText
end

function LuaDialogCrosswordWnd:IsGameFinish()
    for i = 1, #self.m_SuccessFields do
        if self.m_SuccessFields[i] == false then
            return false
        end
    end

    return true
end

function LuaDialogCrosswordWnd:InitWords()
    if self.m_WordsGo then
        for i, v in ipairs(self.m_WordsGo) do
            GameObject.Destroy(v)
        end
    end

    self.m_WordsGo = {}
    for i = 1, #self.m_Words do
        local wordGo = NGUITools.AddChild(self.Words:GetChild(i-1).gameObject, self.Template)
        self:InitWord(wordGo, self.m_Words[i])
        wordGo:SetActive(true)
        table.insert(self.m_WordsGo, wordGo)
    end
end

function LuaDialogCrosswordWnd:InitWord(go, word)
    local lab = go:GetComponent(typeof(UILabel))
    lab.text = word

    CommonDefs.AddOnDragListener(go, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
		self:OnScreenDrag(go, delta)
    end), false)
    
    UIEventListener.Get(go).onDragStart = DelegateFactory.VoidDelegate(function(go)
        self:OnDragStart(go)
    end)

    UIEventListener.Get(go).onDragEnd = DelegateFactory.VoidDelegate(function(go)
        self:OnDragEnd(go, word)
    end)
end

function LuaDialogCrosswordWnd:OnScreenDrag(go, delta)
    local currentPos = UICamera.currentTouch.pos
    go.transform.position = UICamera.currentCamera:ScreenToWorldPoint(Vector3(currentPos.x,currentPos.y,0))
end

function LuaDialogCrosswordWnd:OnDragStart(go)
    local currentPos = UICamera.currentTouch.pos
end

function LuaDialogCrosswordWnd:OnDragEnd(go, word)
    local localPos = self.TargetLab.transform:InverseTransformPoint(UICamera.lastWorldPosition)
    local delta = 25
    for i = -4, 4 do 
        for j = -4, 4 do 
            local localPosV2 = Vector2(localPos.x + i * delta, localPos.y + j * delta)
            local url = self.TargetLab:GetUrlAtPosition(localPosV2)
            if not System.String.IsNullOrEmpty(url) and self.m_Fields[tonumber(url)] == word then
                self:TryCrossword(go, url, word) 
                LuaUtils.SetLocalPosition(go.transform, 0, 0, 0)
                return
            end
        end
    end
    LuaUtils.SetLocalPosition(go.transform, 0, 0, 0)
end

function LuaDialogCrosswordWnd:TryCrossword(go, url, word)
    local index = tonumber(url)
    if self.m_Fields[index] == word then
        go:SetActive(false)
        self.m_SuccessFields[index] = true
        self:RefreshCrosswordFields()
        if self:IsGameFinish() and self.m_IsPlaying then
            self.m_IsPlaying = false
            self:OnSuccess(true)
        end
    end
end

function LuaDialogCrosswordWnd:OnSuccess(playFx)
    if playFx then
        self.FinishFx:LoadFx("fx/ui/prefab/UI_juanzhou_gushipinjie_wancheng.prefab")
    end

    if self.m_WordsGo then
        for i, v in ipairs(self.m_WordsGo) do
            if v.activeSelf then
                local lab = v:GetComponent(typeof(UILabel))
                local destroyFx = v.transform:Find("DestroyFx"):GetComponent(typeof(CUIFx))
                local bg = v.transform:Find("QiPao"):GetComponent(typeof(UITexture))
                lab.text = ""
                bg.alpha = 0
                destroyFx:LoadFx("fx/ui/prefab/UI_juanzhou_zhitiaoxiaoshi.prefab")
            end
        end
    end

    if self.m_Callback then
        self.m_Callback()
    end
    UnRegisterTick(self.m_Tick)
    self.m_Mask:SetActive(false)
    self.CloseButton.gameObject:SetActive(false)
    self.m_Tick = RegisterTickOnce(function()
        self:OnGameFinish()
    end,self.m_FinishDelay or 2000)
end

function LuaDialogCrosswordWnd:OnGameFinish()
    if self.m_WordsGo then
        for i, v in ipairs(self.m_WordsGo) do
            GameObject.Destroy(v)
        end
    end
end

function LuaDialogCrosswordWnd:OnDestroy()
    UnRegisterTick(self.m_Tick)
end

function LuaDialogCrosswordWnd:OnCloseBtnClicked(go)
    local msg = g_MessageMgr:FormatMessage("DialogCrosswordWnd_Leave_MakeSure")
    g_MessageMgr:ShowOkCancelMessage(msg, function()
        Gac2Gas.RequestLeavePlay()
    end, nil, nil, nil, false)
end
