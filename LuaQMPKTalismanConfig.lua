local Word_Word = import "L10.Game.Word_Word"
local EnumQualityType=import "L10.Game.EnumQualityType"
local Talisman_Suit = import "L10.Game.Talisman_Suit"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
-- local Object = import "System.Object"
local IdPartition = import "L10.Game.IdPartition"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
-- local DelegateFactory = import "DelegateFactory"
local CTalismanMgr = import "L10.Game.CTalismanMgr"
local CQMPKTitleTemplate = import "L10.UI.CQMPKTitleTemplate"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonLuaScript=import "L10.UI.CCommonLuaScript"
local QnRadioBox=import "L10.UI.QnRadioBox"
local CQMPKWordsListTemplate=import "L10.UI.CQMPKWordsListTemplate"
local CBodyEquipSlot=import "L10.UI.CBodyEquipSlot"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local CCommonItem=import "L10.Game.CCommonItem"

CLuaQMPKTalismanConfig = class()
RegistClassMember(CLuaQMPKTalismanConfig,"m_EquipView")
RegistClassMember(CLuaQMPKTalismanConfig,"m_WordsRadioBox")
RegistClassMember(CLuaQMPKTalismanConfig,"m_BasicWordsRoot")
RegistClassMember(CLuaQMPKTalismanConfig,"m_SuitWordsRoot")
RegistClassMember(CLuaQMPKTalismanConfig,"m_BasicTitle")
RegistClassMember(CLuaQMPKTalismanConfig,"m_BasicWordsList")
RegistClassMember(CLuaQMPKTalismanConfig,"m_ConfirmObj")
RegistClassMember(CLuaQMPKTalismanConfig,"m_CurrentEquip")
RegistClassMember(CLuaQMPKTalismanConfig,"m_CurrentEquipName")
RegistClassMember(CLuaQMPKTalismanConfig,"m_RedSuitRoot")
RegistClassMember(CLuaQMPKTalismanConfig,"m_RedTitle")
RegistClassMember(CLuaQMPKTalismanConfig,"m_RedLabel")
RegistClassMember(CLuaQMPKTalismanConfig,"m_XianJiaSuitRoot")
RegistClassMember(CLuaQMPKTalismanConfig,"m_XianJiaWord")
RegistClassMember(CLuaQMPKTalismanConfig,"m_XianJiaTable")
RegistClassMember(CLuaQMPKTalismanConfig,"m_TipObj")
RegistClassMember(CLuaQMPKTalismanConfig,"m_IsInited")
RegistClassMember(CLuaQMPKTalismanConfig,"m_RedTalismanTemplateId")
RegistClassMember(CLuaQMPKTalismanConfig,"m_RedTalismanName")
RegistClassMember(CLuaQMPKTalismanConfig,"m_XianJiaSuit")
RegistClassMember(CLuaQMPKTalismanConfig,"m_XianJiaSuitTitle")
RegistClassMember(CLuaQMPKTalismanConfig,"m_XianJiaSuitWord")
RegistClassMember(CLuaQMPKTalismanConfig,"m_TalismanTemplate2Id")
RegistClassMember(CLuaQMPKTalismanConfig,"m_KangxingConflict")
RegistClassMember(CLuaQMPKTalismanConfig,"m_HushiKangxingConflict")
RegistClassMember(CLuaQMPKTalismanConfig,"m_CurrentTemplateId")
RegistClassMember(CLuaQMPKTalismanConfig,"m_CurrentItemId")
RegistClassMember(CLuaQMPKTalismanConfig,"m_CurrentRedType")
RegistClassMember(CLuaQMPKTalismanConfig,"m_RedSelectedIndex")
RegistClassMember(CLuaQMPKTalismanConfig,"m_XianJiaGrade")
RegistClassMember(CLuaQMPKTalismanConfig,"m_CurrentXianJiaLevel")
RegistClassMember(CLuaQMPKTalismanConfig,"m_XianJiaSuitIndex")
RegistClassMember(CLuaQMPKTalismanConfig,"m_OldXianJiaSuitIndex")
RegistClassMember(CLuaQMPKTalismanConfig,"m_CurrentXianJiaTitle")
RegistClassMember(CLuaQMPKTalismanConfig,"m_CurrentXianJiaPopupList")
RegistClassMember(CLuaQMPKTalismanConfig,"m_BasicWordIndex")
RegistClassMember(CLuaQMPKTalismanConfig,"m_ExtraWordIndex")

RegistClassMember(CLuaQMPKTalismanConfig,"m_WenShiRoot")

require("ui/quanminpk/config/LuaQMPKTalismanConfig_WenShi")

RegistClassMember(CLuaQMPKTalismanConfig,"m_TipLab")
RegistClassMember(CLuaQMPKTalismanConfig,"m_OtherAlertTipLab")
RegistClassMember(CLuaQMPKTalismanConfig,"m_IsOtherPlayer")
RegistClassMember(CLuaQMPKTalismanConfig,"m_OtherPlayerInfo")
RegistClassMember(CLuaQMPKTalismanConfig,"m_IsEnableSecondaryTalisman")
RegistClassMember(CLuaQMPKTalismanConfig,"m_Class")
RegistClassMember(CLuaQMPKTalismanConfig,"m_ChangeView")
RegistClassMember(CLuaQMPKTalismanConfig,"m_NoneLabel")
RegistClassMember(CLuaQMPKTalismanConfig,"m_HasChanged")
RegistClassMember(CLuaQMPKTalismanConfig,"m_CurTab")
function CLuaQMPKTalismanConfig:Awake()
    self.m_IsOtherPlayer = CLuaQMPKMgr.s_IsOtherPlayer
    self.m_OtherPlayerInfo = CLuaQMPKMgr.s_PlayerInfo
    if self.m_IsOtherPlayer then
        self.m_IsEnableSecondaryTalisman = self.m_OtherPlayerInfo.TalismanIndex == 2
        self.m_Class = CLuaQMPKMgr.s_PlayerCls
    else
        self.m_IsEnableSecondaryTalisman = CClientMainPlayer.Inst.ItemProp.IsEnableSecondaryTalisman
        self.m_Class = EnumToInt(CClientMainPlayer.Inst.Class)
    end

    local script=self.transform:Find("EquipView"):GetComponent(typeof(CCommonLuaScript))
    script:Awake()
    self.m_EquipView = script.m_LuaSelf
    self.m_CurTab = 0
    -- self.m_EquipView = self.transform:Find("EquipView"):GetComponent(typeof(CQMPKTalismanEquipView))
    self.m_ChangeView = self.transform:Find("ChangeView").gameObject
    self.m_NoneLabel = self.transform:Find("NoneLabel").gameObject
    self.m_WordsRadioBox = self.transform:Find("ChangeView/Offset/RadioBox"):GetComponent(typeof(QnRadioBox))
    self.m_WordsRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        local endFunc = function() self:OnRadioBoxSlected(btn,index) self.m_CurTab = index end
        self:OnPageCloseOrChange(endFunc)
        
    end)

    self.m_BasicWordsRoot = self.transform:Find("ChangeView/BasicWords").gameObject
    self.m_SuitWordsRoot = self.transform:Find("ChangeView/SuitWords").gameObject
    --纹饰
    self.m_WenShiRoot=self.transform:Find("ChangeView/Wenshi").gameObject
    FindChild(self.m_WenShiRoot.transform,"TitleTemplate").gameObject:SetActive(false)
    FindChild(self.m_WenShiRoot.transform,"WordTemplate").gameObject:SetActive(false)

    
    self.m_BasicTitle = {}
    self.m_BasicTitle[1]=FindChild(self.transform,"TitleTemplate1"):GetComponent(typeof(CQMPKTitleTemplate))
    self.m_BasicTitle[2]=FindChild(self.transform,"TitleTemplate2"):GetComponent(typeof(CQMPKTitleTemplate))
    self.m_BasicWordsList = {}
    self.m_BasicWordsList[1]=FindChild(self.transform,"WordTemplate1"):GetComponent(typeof(CQMPKWordsListTemplate))
    self.m_BasicWordsList[2]=FindChild(self.transform,"WordTemplate2"):GetComponent(typeof(CQMPKWordsListTemplate))


    self.m_ConfirmObj = self.transform:Find("ChangeView/ConfirmBtn").gameObject
    self.m_CurrentEquip = self.transform:Find("CurrentEquipment"):GetComponent(typeof(CBodyEquipSlot))
    self.m_CurrentEquipName = self.transform:Find("CurrentEquipment/CurrentEquipmentName"):GetComponent(typeof(UILabel))
    self.m_RedSuitRoot = self.transform:Find("ChangeView/SuitWords/Red").gameObject
    self.m_RedTitle = self.transform:Find("ChangeView/SuitWords/Red/Words/TitleTemplate"):GetComponent(typeof(CQMPKTitleTemplate))
    self.m_RedLabel = self.transform:Find("ChangeView/SuitWords/Red/Words/WordTemplate"):GetComponent(typeof(UILabel))
    self.m_XianJiaSuitRoot = self.transform:Find("ChangeView/SuitWords/Xianjia").gameObject
    self.m_XianJiaWord = self.transform:Find("ChangeView/SuitWords/Xianjia/WordTemplate").gameObject
    self.m_XianJiaWord:SetActive(false)
    self.transform:Find("ChangeView/SuitWords/Xianjia/TitleTemplate").gameObject:SetActive(false)

    self.m_XianJiaTable = self.transform:Find("ChangeView/SuitWords/Xianjia/WordsTable"):GetComponent(typeof(UITable))
    self.m_TipObj = self.transform:Find("Tip").gameObject
    self.m_TipLab = self.transform:Find("Label"):GetComponent(typeof(UILabel))
    self.m_OtherAlertTipLab = self.transform:Find("ChangeView/AlertLab"):GetComponent(typeof(UILabel))


--self.m_IsInited = ?
--self.m_RedTalismanTemplateId = ?
--self.m_RedTalismanName = ?
--self.m_XianJiaSuit = ?
--self.m_XianJiaSuitTitle = ?
--self.m_XianJiaSuitWord = ?
--self.m_TalismanTemplate2Id = ?
--self.m_KangxingConflict = ?
--self.m_HushiKangxingConflict = ?
--self.m_CurrentTemplateId = ?
self.m_CurrentItemId = nil
self.m_CurrentRedType = 0
self.m_RedSelectedIndex = 0
self.m_XianJiaGrade = {60, 90, 120, 150}

--self.m_CurrentXianJiaLevel = ?
self.m_XianJiaSuitIndex ={0,0,0,0,0}-- CreateFromClass(MakeGenericClass(List, UInt32))
self.m_OldXianJiaSuitIndex ={0,0,0,0,0}-- CreateFromClass(MakeGenericClass(List, UInt32))
self.m_CurrentXianJiaTitle = 0
--self.m_CurrentXianJiaPopupList = ?
self.m_BasicWordIndex = 0
self.m_ExtraWordIndex = 0
-- self.m_IsBasicTab = true
self.m_HasChanged = false
end

-- Auto Generated!!

function CLuaQMPKTalismanConfig:OnEnable( )
    if not self.m_IsOtherPlayer then
        g_ScriptEvent:AddListener("EquipSuccess", self, "OnEquipSuccess")
        g_ScriptEvent:AddListener("OnTalismanMenuSelect", self, "OnTalismanMenuSelect")
    end
    g_ScriptEvent:AddListener("QMPKTalismanClick", self, "OnQMPKTalismanClick")
    g_ScriptEvent:AddListener("OnOtherTalismanMenuSelect", self, "OnOtherTalismanMenuSelect")
    
    if self.m_IsInited then
        return
    end
    self.m_IsInited = true
    self:Init()

    UIEventListener.Get(self.m_TipObj).onClick = DelegateFactory.VoidDelegate(function (go) 
        g_MessageMgr:ShowMessage("QMPK_Talisman_Config_Tip")
    end)

    UIEventListener.Get(self.m_CurrentEquip.gameObject).onClick =DelegateFactory.VoidDelegate(function(go)
        self:OnIconClick(go)
    end)
end
function CLuaQMPKTalismanConfig:OnDisable( )
    if not self.m_IsOtherPlayer then
        g_ScriptEvent:RemoveListener("EquipSuccess", self, "OnEquipSuccess")
        g_ScriptEvent:RemoveListener("OnTalismanMenuSelect", self, "OnTalismanMenuSelect")
    end
    g_ScriptEvent:RemoveListener("QMPKTalismanClick", self, "OnQMPKTalismanClick")
    g_ScriptEvent:RemoveListener("OnOtherTalismanMenuSelect", self, "OnOtherTalismanMenuSelect")
end
function CLuaQMPKTalismanConfig:OnQMPKTalismanClick(itemId,templateId)
    local endFunc = function()  self:InitWords(itemId,templateId) end
    if self.m_CurrentItemId ~= itemId  then
        self:OnPageCloseOrChange(endFunc)
    else
        endFunc()
    end
end
function CLuaQMPKTalismanConfig:OnTalismanMenuSelect()
    self:RefreshIsNone()
end
function CLuaQMPKTalismanConfig:OnOtherTalismanMenuSelect()
    self:RefreshIsNone()
end
function CLuaQMPKTalismanConfig:RefreshIsNone()
    if self.m_IsOtherPlayer then
        local delta = self.m_OtherPlayerInfo.TalismanIndex == 2 and 8 or 0
        local startIdx = EnumBodyPosition_lua.TalismanQian + delta
        local endIdx = EnumBodyPosition_lua.TalismanDui + delta
        local list = {}

        for i = startIdx, endIdx do
            table.insert(list, self.m_OtherPlayerInfo.Pos2Equip[i])
        end

        self:InitNone(#list == 0)
    else
        local list = CTalismanMgr.Inst:GetTalismanListOnBody()
        self:InitNone(list.Count == 0)
    end
end
function CLuaQMPKTalismanConfig:Init( )
    self.m_RedTalismanTemplateId ={}-- CreateFromClass(MakeGenericClass(List, MakeGenericClass(List, UInt32)))
    self.m_RedTalismanName ={}-- CreateFromClass(MakeGenericClass(List, MakeGenericClass(List, String)))
    self.m_XianJiaSuit ={}-- CreateFromClass(MakeGenericClass(List, CQMPKTalismanSuit))
    self.m_CurrentXianJiaPopupList ={}-- CreateFromClass(MakeGenericClass(List, CQMPKTalismanSuit))

    self.m_XianJiaSuitTitle ={}-- CreateFromClass(MakeGenericClass(List, CQMPKTitleTemplate))
    self.m_XianJiaSuitWord ={}-- CreateFromClass(MakeGenericClass(List, UILabel))
    self.m_TalismanTemplate2Id ={}-- CreateFromClass(MakeGenericClass(Dictionary, UInt32, UInt32))
    self.m_KangxingConflict ={}-- CreateFromClass(MakeGenericClass(HashSet, UInt32))
    self.m_HushiKangxingConflict ={}-- CreateFromClass(MakeGenericClass(HashSet, UInt32))

    if nil == CClientMainPlayer.Inst then
        return
    end

    local keys={}
    QuanMinPK_TalismanReplace.ForeachKey(function (key) 
        table.insert( keys,key )
    end)
    for i,v in ipairs(keys) do
        local data = QuanMinPK_TalismanReplace.GetData(v)

        local template_t={}
        for i=1,data.ReplaceStr.Length do
            table.insert( template_t,data.ReplaceStr[i-1] )
        end
        table.insert( self.m_RedTalismanTemplateId,template_t )

        local name_t={}
        for i=1,data.Description.Length do
            table.insert( name_t,data.Description[i-1] )
        end
        table.insert( self.m_RedTalismanName,name_t )
    end

    local keys={}
    QuanMinPK_TalismanSuit.ForeachKey(function (key) 
        table.insert( keys,key )
    end)
    for i,v in ipairs(keys) do
        local data = QuanMinPK_TalismanSuit.GetData(v)
        if data.Replace > 0 then
            table.insert( self.m_XianJiaSuit,{m_WordCls=data.WordCls,m_Description=data.Description,m_Id=data.ID} )
        end
    end

    local keys={}
    QuanMinPK_Talisman.ForeachKey(function (key) 
        table.insert( keys,key )
    end)

    for i,v in ipairs(keys) do
        local data = QuanMinPK_Talisman.GetData(v)
        if data.Class == self.m_Class then
            self.m_TalismanTemplate2Id[data.TemplateId]=data.ID
        end
    end


    local setting = QuanMinPK_Setting.GetData()
    if setting then
        for i=1,setting.Talismansuit_Kangxing_Conflict.Length do
            local val=setting.Talismansuit_Kangxing_Conflict[i-1]
            self.m_KangxingConflict[val]=true
        end
        for i=1,setting.Talismansuit_HushiKangxing_Conflict.Length do
            local val=setting.Talismansuit_HushiKangxing_Conflict[i-1]
            self.m_HushiKangxingConflict[val]=true
        end
    end

    self.m_EquipView:Init(self.m_IsEnableSecondaryTalisman and 1 or 0)

    
    UIEventListener.Get(self.m_BasicTitle[0+1].gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_IsOtherPlayer then
            return
        end
        self:OnBasicTitleClick(go)
    end)
    UIEventListener.Get(self.m_BasicTitle[1+1].gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_IsOtherPlayer then
            return
        end
        self:OnExtraTitleClick(go)
    end)
    UIEventListener.Get(self.m_ConfirmObj).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_IsOtherPlayer then
            return
        end
        self:OnConfirmBtnClick(go)
    end)
    UIEventListener.Get(self.m_RedTitle.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_IsOtherPlayer then
            return
        end
        self:OnRedTitleClick(go)
    end)
    -- init xianjia talisman suit
    for i = 0, 4 do
        local go = NGUITools.AddChild(self.m_XianJiaTable.gameObject, self.m_RedTitle.gameObject)
        go:SetActive(true)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnXianJiaTitleClick(go)
        end)

        local title =go:GetComponent(typeof(CQMPKTitleTemplate))-- CommonDefs.GetComponent_GameObject_Type(go, typeof(CQMPKTitleTemplate))
        title:Expand(i ~= 0)
        table.insert( self.m_XianJiaSuitTitle,title )

        go = NGUITools.AddChild(self.m_XianJiaTable.gameObject, self.m_XianJiaWord)
        table.insert( self.m_XianJiaSuitWord,go:GetComponent(typeof(UILabel)) )
        go:SetActive(i == 0)
    end

    if self.m_IsOtherPlayer then
        local delta = self.m_OtherPlayerInfo.TalismanIndex == 2 and 8 or 0
        local startIdx = EnumBodyPosition_lua.TalismanQian + delta
        local endIdx = EnumBodyPosition_lua.TalismanDui + delta
        local list = {}

        for i = startIdx, endIdx do
            table.insert(list, self.m_OtherPlayerInfo.Pos2Equip[i])
        end

        if #list > 0 then
            self:InitWords(list[1].Id, list[1].TemplateId)
        else
            self:InitWords(nil, 0)
            self.m_CurrentEquipName.text = ""
            self:RefreshIsNone()
            return
        end
    else
        local list = CTalismanMgr.Inst:GetTalismanListOnBody()
        if list.Count > 0 then
            self:InitWords(list[0].Id, list[0].TemplateId)
        else
            self:InitWords(nil, 0)
            self.m_CurrentEquipName.text = ""
            self:RefreshIsNone()
            return
        end
    end
    if self.m_IsOtherPlayer then
        self:InitOtherPlayerConfig()
    end
    self:RefreshIsNone()
end
function CLuaQMPKTalismanConfig:OnEquipSuccess( args) 
    local pos=args[0]
    local itemId=args[1]

    local item = CItemMgr.Inst:GetById(itemId)
    if not IdPartition.IdIsTalisman(item.TemplateId) and IdPartition.IdIsXianJiaTalisman(item.TemplateId) then
        return
    end
    self:InitWords(itemId, item.TemplateId)
end
function CLuaQMPKTalismanConfig:InitWords( itemId, templateId) 
    self.m_CurrentItemId = itemId
    self.m_CurrentTemplateId = templateId
    local line = debug.getinfo(2).currentline

    if itemId==nil then
        self.m_BasicTitle[1].gameObject:SetActive(false)
        self.m_BasicTitle[2].gameObject:SetActive(false)
        self.m_BasicWordsList[1].gameObject:SetActive(false)
        self.m_BasicWordsList[2].gameObject:SetActive(false)
        self:InitWenShiNil()
        self.m_XianJiaSuitRoot:SetActive(false)
        self.m_RedSuitRoot:SetActive(false)
        self.m_CurrentEquip:Init(nil,EnumQualityType.None,false, nil, 0)
        self.m_CurrentEquipName.text=nil
    else
        self.m_BasicTitle[1].gameObject:SetActive(true)
        self.m_BasicTitle[2].gameObject:SetActive(true)
        self.m_BasicWordsList[1].gameObject:SetActive(true)
        self.m_BasicWordsList[2].gameObject:SetActive(true)
    end

    if not self.m_TalismanTemplate2Id[templateId] then
        return
    end

    local talisman = QuanMinPK_Talisman.GetData(self.m_TalismanTemplate2Id[templateId])
    if nil == talisman then
        return
    end

    local basicWords = talisman.BasicWords
    local extraWords = talisman.ExtraWords
    local item = self:GetItemById(itemId)
    self.m_ExtraWordIndex = 1 self.m_BasicWordIndex = self.m_ExtraWordIndex
    if item.Equip.ExtraData.Data.varbinary ~= nil then
		local splitStr=g_LuaUtil:StrSplit(item.Equip.ExtraData.Data.varbinary.StringData,";")
        self.m_BasicWordIndex=tonumber(splitStr[1])
        self.m_ExtraWordIndex=tonumber(splitStr[2])

        --纹饰索引
        self.m_WenShiIndexTbl={}
        local t=g_LuaUtil:StrSplit(splitStr[3],",")
        for i,v in ipairs(t) do
            local index=tonumber(v)
            if index and index>0 then
                table.insert( self.m_WenShiIndexTbl,tonumber(v) )
            end
        end
        -- end
    end

    self.m_BasicTitle[1]:Init(LocalString.GetString("[AAFFFF]基础[-]").. string.rep(' ', 32) .. talisman.BasicDescription[self.m_BasicWordIndex - 1])
    self.m_BasicTitle[2]:Init(LocalString.GetString("[AAFFFF]额外[-]").. string.rep(' ', 32) .. talisman.ExtraDescription[self.m_ExtraWordIndex - 1])
    self.m_BasicWordsList[1]:Init(basicWords[self.m_BasicWordIndex - 1])
    self.m_BasicWordsList[2]:Init(extraWords[self.m_ExtraWordIndex - 1])

    self.m_CurrentEquip:Init(item.Icon, item.Equip.Color, false, item.Equip, 0)
    self.m_CurrentEquipName.text = item.ColoredName

    if IdPartition.IdIsXianJiaTalisman(templateId) then
        self.m_XianJiaSuitRoot:SetActive(true)
        self.m_RedSuitRoot:SetActive(false)
        self:InitXianJiaSuit(itemId, templateId)
    else
        self.m_XianJiaSuitRoot:SetActive(false)
        self.m_RedSuitRoot:SetActive(true)
        self:GetRedType()
        self:OnRedTitleSelected(self.m_RedSelectedIndex)
    end

    --纹饰一定是3个
    self:InitWenShi()
end


function CLuaQMPKTalismanConfig:GetRedType( )
    for i,v in ipairs(self.m_RedTalismanTemplateId) do
        for j,k in ipairs(v) do
            if k==self.m_CurrentTemplateId then
                self.m_CurrentRedType = i-1
                self.m_RedSelectedIndex=j-1
                break
            end
        end
    end
end
function CLuaQMPKTalismanConfig:OnRedTitleClick( go) 
    self:GetRedType()
    local popupList={}
    if self.m_RedTalismanTemplateId[self.m_CurrentRedType+1] then
        for i,v in ipairs(self.m_RedTalismanTemplateId[self.m_CurrentRedType+1]) do
            local data = PopupMenuItemData(self.m_RedTalismanName[self.m_CurrentRedType+1][i], DelegateFactory.Action_int(function(index) self:OnRedTitleSelected(index) end),false, nil, EnumPopupMenuItemStyle.Light)
            table.insert( popupList,data )
        end
    end
    local array=Table2Array(popupList,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, self.m_RedTitle.transform, CPopupMenuInfoMgr.AlignType.Bottom, #popupList >= 8 and 2 or 1, nil, 600, true, 296)
end
function CLuaQMPKTalismanConfig:OnRedTitleSelected( index)
    if self.m_RedSelectedIndex ~= index then self.m_HasChanged = true end
    self.m_RedSelectedIndex = index
    self.m_RedTitle:Init(self.m_RedTalismanName[self.m_CurrentRedType+1][index+1])
    local templateId = self.m_RedTalismanTemplateId[self.m_CurrentRedType+1][index+1]
    if not IdPartition.IdIsTalisman(templateId) and IdPartition.IdIsXianJiaTalisman(templateId) then
        return
    end
    -- local context = NewStringBuilderWraper(StringBuilder)
    local context=""
    local talismanDesign = EquipmentTemplate_Equip.GetData(templateId)
    if nil ~= talismanDesign then
        local data = Talisman_SuitConsist.GetData(talismanDesign.TalismanType)
        if nil ~= data then
            local suit = Talisman_Suit.GetData(data.ActivateSuits[0].SuitId)
            do
                local i = 0 local cnt = suit.Words.Length
                while i < cnt do
                    local word = Word_Word.GetData(suit.Words[i])
                    if nil ~= word then
                        context=context..SafeStringFormat3( LocalString.GetString("【%s联·%s】%s(评分+%s)"),
                            data.ActivateSuits[0].NeedCount,
                            self.m_RedTalismanName[self.m_CurrentRedType+1][index+1],
                            word.Description, 
                            word.Mark )
                    end
                    i = i + 1
                end
            end
        end
    end
    self.m_RedLabel.text =context-- ToStringWrap(context)
end
function CLuaQMPKTalismanConfig:InitXianJiaSuit( itemId, templateId) 
    local item = self:GetItemById(itemId)
    for i = 1, 4 do
        if item.Grade == self.m_XianJiaGrade[i] then
            self.m_CurrentXianJiaLevel = i-- + 1
            break
        end
    end

    if CClientMainPlayer.Inst == nil then
        return
    end
    local suitIdList={}
    do
        local keys={}
        Talisman_Suit.ForeachKey(DelegateFactory.Action_object(function (key) 
            table.insert( keys,key )
        end))
        for i,v in ipairs(keys) do
            local data = Talisman_Suit.GetData(keys[i])
            if data.XianJiaLevel == self.m_CurrentXianJiaLevel and data.XianJiaClass == self.m_Class then
                table.insert( suitIdList,keys[i] )
            end
        end
    end

    for i,v in ipairs(suitIdList) do
        local suit = Talisman_Suit.GetData(v)
        if suit ~= nil then
            local wordId = CTalismanMgr.Inst:GetSuitUserWordId((self.m_IsEnableSecondaryTalisman and 2 or 1), suit, true)
            if wordId == 0 then
                wordId = suit.Words[0]
            end
            local word = Word_Word.GetData(wordId)
            if word then
                local title =g_LuaUtil:StrSplit(word.Description," ")[1]
                for j,v in ipairs(self.m_XianJiaSuit) do
                    if v.m_Description==title then
                        self.m_XianJiaSuitIndex[i] = v.m_Id
                        self.m_OldXianJiaSuitIndex[i] = self.m_XianJiaSuitIndex[i]
                        break
                    end
                end
    
    
                if i-1 == 4 then
                    title = title .. LocalString.GetString(" （不可替）")
                end
                self.m_XianJiaSuitTitle[i]:Init(title)
                self.m_XianJiaSuitWord[i].text = SafeStringFormat3( LocalString.GetString("(%s)【%s】%s(评分+%s)"),i-1 + 2, word.Name, word.Description, word.Mark )
            end
        end
    end
end
function CLuaQMPKTalismanConfig:OnXianJiaTitleClick( go) 
    local oldTitle = self.m_CurrentXianJiaTitle
    for i,v in ipairs(self.m_XianJiaSuitTitle) do
        if go==v.gameObject then
            self.m_CurrentXianJiaTitle=i-1
            v:Expand(false)
            self.m_XianJiaSuitWord[i].gameObject:SetActive(true)
        else
            v:Expand(true)
            self.m_XianJiaSuitWord[i].gameObject:SetActive(false)
        end
    end

    self.m_XianJiaTable:Reposition()
    if self.m_XianJiaSuitTitle[4+1].gameObject == go then
        return
    end
    if oldTitle ~= self.m_CurrentXianJiaTitle then
        return
    end
    if self.m_IsOtherPlayer then
        return
    end

    self.m_CurrentXianJiaPopupList={}
    
    local popupList={}
    for i,v in ipairs(self.m_XianJiaSuit) do
        local m=false
        for j,k in ipairs(self.m_XianJiaSuitIndex) do
            if k==v.m_Id then
                m=true
                break
            end
        end
        if m then
        elseif self:IsXianJiaSuitConflict(v.m_Id) then
        else
            local data = PopupMenuItemData(v.m_Description, DelegateFactory.Action_int(function(index) self:OnXianJiaTitleSelected(index) end),false, nil, EnumPopupMenuItemStyle.Light)
            table.insert( popupList,data )
            table.insert( self.m_CurrentXianJiaPopupList,v )
        end
    end

    local array=Table2Array(popupList,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, CPopupMenuInfoMgr.AlignType.Bottom, #popupList >= 8 and 2 or 1, nil, 600, true, 296)
end
function CLuaQMPKTalismanConfig:IsXianJiaSuitConflict( id) 
    if self.m_KangxingConflict[id] then--法宝套装抗性冲突
        for i = 0, 4 do
            local continue
            repeat
                if i == self.m_CurrentXianJiaTitle then
                    continue = true
                    break
                end
                if self.m_KangxingConflict[self.m_XianJiaSuitIndex[i+1]] then
                    return true
                end
                continue = true
            until 1
            if not continue then
                break
            end
        end
    end
    if self.m_HushiKangxingConflict[id] then--法宝套装忽视抗性冲突
        for i = 0, 4 do
            local continue
            repeat
                if i == self.m_CurrentXianJiaTitle then
                    continue = true
                    break
                end
                if self.m_HushiKangxingConflict[self.m_XianJiaSuitIndex[i+1]] then
                    return true
                end
                continue = true
            until 1
            if not continue then
                break
            end
        end
    end
    return false
end
function CLuaQMPKTalismanConfig:OnXianJiaTitleSelected( index) 
    self.m_XianJiaSuitTitle[self.m_CurrentXianJiaTitle+1]:Init(self.m_CurrentXianJiaPopupList[index+1].m_Description)
    if self.m_XianJiaSuitIndex[self.m_CurrentXianJiaTitle+1] ~= self.m_CurrentXianJiaPopupList[index+1].m_Id then self.m_HasChanged = true end
    self.m_XianJiaSuitIndex[self.m_CurrentXianJiaTitle+1] = self.m_CurrentXianJiaPopupList[index+1].m_Id
    local wordId = self.m_CurrentXianJiaPopupList[index+1].m_WordCls * 100 + self.m_CurrentXianJiaLevel
    local word = Word_Word.GetData(wordId)
    if nil == word then
        return
    end
    self.m_XianJiaSuitWord[self.m_CurrentXianJiaTitle+1].text = 
        SafeStringFormat3( LocalString.GetString("(%s)【%s】%s(评分+%s)"), self.m_CurrentXianJiaTitle + 2, word.Name, word.Description, word.Mark)
end
function CLuaQMPKTalismanConfig:OnBasicTitleClick( go) 
    if not self.m_TalismanTemplate2Id[self.m_CurrentTemplateId] then
        return
    end
    local talisman = QuanMinPK_Talisman.GetData(self.m_TalismanTemplate2Id[self.m_CurrentTemplateId])
    if nil == talisman then
        return
    end
    local basicDescription = talisman.BasicDescription
    local popupList={}
    do
        local i = 0 local len = basicDescription.Length
        while i < len do
            local data = PopupMenuItemData(basicDescription[i], DelegateFactory.Action_int(function(index) self:OnBasicTitleSelected(index) end),false, nil, EnumPopupMenuItemStyle.Light)
            table.insert( popupList,data )
            i = i + 1
        end
    end
    local array=Table2Array(popupList,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, CPopupMenuInfoMgr.AlignType.Bottom, #popupList >= 8 and 2 or 1, nil, 600, true, 296)
end

function CLuaQMPKTalismanConfig:OnBasicTitleSelected( index) 
    if not self.m_TalismanTemplate2Id[self.m_CurrentTemplateId] then
        return
    end
    local talisman = QuanMinPK_Talisman.GetData(self.m_TalismanTemplate2Id[self.m_CurrentTemplateId])
    if nil == talisman then
        return
    end
    local basicWords = talisman.BasicWords
    self.m_BasicTitle[0+1]:Init(LocalString.GetString("[AAFFFF]基础[-]") .. string.rep(' ', 32) .. talisman.BasicDescription[index])
    self.m_BasicWordsList[0+1]:Init(basicWords[index])
    if self.m_BasicWordIndex ~= index + 1 then self.m_HasChanged = true end
    self.m_BasicWordIndex = index + 1
end

function CLuaQMPKTalismanConfig:OnExtraTitleClick( go) 
    if not self.m_TalismanTemplate2Id[self.m_CurrentTemplateId] then
        return
    end
    local talisman = QuanMinPK_Talisman.GetData(self.m_TalismanTemplate2Id[self.m_CurrentTemplateId])
    if nil == talisman then
        return
    end
    local extraDescription = talisman.ExtraDescription
    local popupList={}
    do
        local i = 0 local len = extraDescription.Length
        while i < len do
            local data = PopupMenuItemData(extraDescription[i], DelegateFactory.Action_int(function(index) self:OnExtraTitleSelected(index) end),false, nil, EnumPopupMenuItemStyle.Light)
            table.insert( popupList,data )
            i = i + 1
        end
    end
    local array=Table2Array(popupList,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, CPopupMenuInfoMgr.AlignType.Bottom, #popupList >= 8 and 2 or 1, nil, 600, true, 296)
end
function CLuaQMPKTalismanConfig:OnExtraTitleSelected( index) 
    if not self.m_TalismanTemplate2Id[self.m_CurrentTemplateId] then
        return
    end
    local talisman = QuanMinPK_Talisman.GetData(self.m_TalismanTemplate2Id[self.m_CurrentTemplateId])
    if nil == talisman then
        return
    end
    local extraWords = talisman.ExtraWords
    self.m_BasicTitle[1+1]:Init(LocalString.GetString("[AAFFFF]额外[-]") .. string.rep(' ', 32) .. talisman.ExtraDescription[index])
    self.m_BasicWordsList[1+1]:Init(extraWords[index])
    if self.m_ExtraWordIndex ~= index + 1 then self.m_HasChanged = true end
    self.m_ExtraWordIndex = index + 1
end
function CLuaQMPKTalismanConfig:OnRadioBoxSlected( btn, index) 
    -- 没有法宝时
    -- if not self.m_CurrentItemId then
    --     self.m_BasicWordsRoot:SetActive(false)
    --     self.m_SuitWordsRoot:SetActive(false)
    --     self.m_WenShiRoot:SetActive(false)
    --     return
    -- end
    if index == 0 then
        self.m_BasicWordsRoot:SetActive(true)
        self.m_SuitWordsRoot:SetActive(false)
        self.m_WenShiRoot:SetActive(false)
        -- 词条Table从deactive到active，并没有进行排序
        for i,v in ipairs(self.m_BasicWordsList) do
            v:Reposition()
        end
    elseif index==1 then
        self.m_BasicWordsRoot:SetActive(false)
        self.m_SuitWordsRoot:SetActive(true)
        self.m_WenShiRoot:SetActive(false)
    else
        --纹饰
        self.m_BasicWordsRoot:SetActive(false)
        self.m_SuitWordsRoot:SetActive(false)
        self.m_WenShiRoot:SetActive(true)
        FindChild(self.m_WenShiRoot.transform,"Table"):GetComponent(typeof(UITable)):Reposition()
    end
end

function CLuaQMPKTalismanConfig:OnConfirmBtnClick( go) 
    if not self.m_CurrentItemId then
        return
    end
    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Body, self.m_CurrentItemId)

    if self.m_CurTab==0 then
        Gac2Gas.RequestSetQmpkTalismanBasicWord(EnumItemPlace_lua.Body, pos, self.m_CurrentItemId, self.m_BasicWordIndex)
        Gac2Gas.RequestSetQmpkTalismanExtraWord(EnumItemPlace_lua.Body, pos, self.m_CurrentItemId, self.m_ExtraWordIndex)
    elseif self.m_CurTab==1 then
        --仙家法宝
        if IdPartition.IdIsXianJiaTalisman(self.m_CurrentTemplateId) then
            local str=""
            for i,v in ipairs(self.m_XianJiaSuitIndex) do
                if v~=self.m_OldXianJiaSuitIndex[i] then
                    self.m_OldXianJiaSuitIndex[i] = v
                    str=str..SafeStringFormat3( "%s,%s;",i, self.m_XianJiaSuitIndex[i])
                end
            end
            Gac2Gas.RequestReplaceQmpkXiaJiaTalismanSuit((self.m_IsEnableSecondaryTalisman and 2 or 1), str)
        else
            Gac2Gas.RequestReplaceQmpkRedTalisman(EnumItemPlace_lua.Body, pos, self.m_CurrentItemId, self.m_RedTalismanTemplateId[self.m_CurrentRedType+1][self.m_RedSelectedIndex+1])
        end
    elseif self.m_CurTab==2 then
        --纹饰
        self:OnWenShiConfirmClicked(pos)
    end
    self.m_HasChanged = false
end
function CLuaQMPKTalismanConfig:OnIconClick( go) 
    if not self.m_CurrentItemId then
        return
    end

    if self.m_IsOtherPlayer then
        local item = self:GetItemById(self.m_CurrentItemId)
        CItemInfoMgr.ShowLinkItemInfo(item, true, nil, AlignType.Default, 0, 0, 0, 0, 0)
    else 
        CItemInfoMgr.ShowBodyItemInfo(self.m_CurrentItemId, CItemMgr.Inst:FindItemPosition(EnumItemPlace.Body, self.m_CurrentItemId), nil)
    end
end

function CLuaQMPKTalismanConfig:GetItemById(itemId)
    if self.m_IsOtherPlayer then
        local equip = self.m_OtherPlayerInfo.ItemId2Equip[itemId]
        return CreateFromClass(CCommonItem, equip)
    else
        return CItemMgr.Inst:GetById(itemId)
    end
end

function CLuaQMPKTalismanConfig:InitOtherPlayerConfig()
    self.m_ConfirmObj:SetActive(false)
    self.m_TipLab.gameObject:SetActive(false)
    self.m_TipObj:SetActive(false)
    self.m_OtherAlertTipLab.gameObject:SetActive(true)
end

function CLuaQMPKTalismanConfig:InitNone(isNone)
    self.m_ConfirmObj:SetActive(not self.m_IsOtherPlayer)
    self.m_OtherAlertTipLab.gameObject:SetActive(self.m_IsOtherPlayer)
    self.m_NoneLabel:SetActive(isNone)
    self.m_ChangeView:SetActive(not isNone)
    self.m_CurrentEquip.gameObject:SetActive(not isNone)
    self.m_CurrentEquipName.gameObject:SetActive(not isNone)
    self.m_TipObj:SetActive(not isNone and not self.m_IsOtherPlayer)
    self.m_TipLab.gameObject:SetActive(not isNone and not self.m_IsOtherPlayer)
end
function CLuaQMPKTalismanConfig:OnPageCloseOrChange(finalFunc)
    if self.m_HasChanged then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("QMPK_ConfigSave_Confirm"), function()
            self:OnConfirmBtnClick(self.m_ConfirmBtn)
            if finalFunc then finalFunc() end
        end, function()
            if finalFunc then finalFunc() end
        end, nil, nil, false)
        self.m_HasChanged = false
        return true
    else
        if finalFunc then finalFunc() end
    end
    
    return false
end

return CLuaQMPKTalismanConfig
