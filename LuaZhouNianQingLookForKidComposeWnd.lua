local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local ShowType = import "L10.UI.CItemInfoMgr+ShowType"
local CItemInfoMgr_AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local LinkItemInfo = import "L10.UI.CItemInfoMgr+LinkItemInfo"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltip = import "L10.UI.CTooltip"

LuaZhouNianQingLookForKidComposeWnd = class()

RegistClassMember(LuaZhouNianQingLookForKidComposeWnd, "m_AddSubBtn")
RegistClassMember(LuaZhouNianQingLookForKidComposeWnd, "m_ChipId")
RegistClassMember(LuaZhouNianQingLookForKidComposeWnd, "m_ChipNow")
RegistClassMember(LuaZhouNianQingLookForKidComposeWnd, "m_ChipReq")

function LuaZhouNianQingLookForKidComposeWnd:Awake()
    self.m_AddSubBtn = self.transform:Find("Anchor/Offset/QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))

    local chips = ZhouNianQing2023_Setting.GetData().Tabensuipian
    self.m_ChipNow = {CItemMgr.Inst:GetItemCount(chips[0][0]), CItemMgr.Inst:GetItemCount(chips[1][0]), CItemMgr.Inst:GetItemCount(chips[2][0])}
    self.m_ChipReq = {chips[0][1], chips[1][1], chips[2][1]}

    UIEventListener.Get(self.transform:Find("Anchor/Offset/ComposeBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        local num = self.m_AddSubBtn:GetValue()
        if num > 0 then
            self.m_ChipNow = {CItemMgr.Inst:GetItemCount(chips[0][0]), CItemMgr.Inst:GetItemCount(chips[1][0]), CItemMgr.Inst:GetItemCount(chips[2][0])}
            local ok = true
            for i = 1, 3 do
                if self.m_ChipNow[i] / self.m_ChipReq[i] < num then
                    ok = false
                    break
                end 
            end
            if ok then
                Gac2Gas.Anniv2023_RequestMakeTaben(num)
                CUIManager.CloseUI(CLuaUIResources.ZhouNianQingLookForKidComposeWnd)
            else
                g_MessageMgr:ShowCustomMsg(LocalString.GetString("合成拓本所需材料不足"))
            end
        end
    end)
end

function LuaZhouNianQingLookForKidComposeWnd:Init()
    local chips = ZhouNianQing2023_Setting.GetData().Tabensuipian
    local tabenId = ZhouNianQing2023_Setting.GetData().TabenItemId
    self:InitOneItem(self.transform:Find("Anchor/Offset/Items/Rubbing").gameObject, tabenId)
    local tabenData = Item_Item.GetData(tabenId)
    self.transform:Find("Anchor/TitleLabel"):GetComponent(typeof(UILabel)).text = tabenData.Name

    local maxCnt = 1
    for i = 1, 3 do
        maxCnt = math.max(maxCnt, math.floor(self.m_ChipNow[i] / self.m_ChipReq[i]))
        self:InitOneItem(self.transform:Find("Anchor/Offset/Items/Item"..i).gameObject, chips[i - 1][0], self.m_ChipNow[i], self.m_ChipReq[i])
    end
    maxCnt = math.min(maxCnt, tabenData.Overlap)
    self.m_AddSubBtn:SetMinMax(1, maxCnt, 1)
    self.m_AddSubBtn.onValueChanged = DelegateFactory.Action_uint(function (value)

    end)
end


function LuaZhouNianQingLookForKidComposeWnd:InitOneItem(curItem, itemID, now, req)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)

    if ItemData then texture:LoadMaterial(ItemData.Icon) end

    if now and req then
        local nowLabel = curItem.transform:Find("Now"):GetComponent(typeof(UILabel))
        nowLabel.text = now
        if now < req then 
            nowLabel.color = Color.red 
            curItem.transform:Find("Got").gameObject:SetActive(true)
        else
            curItem.transform:Find("Got").gameObject:SetActive(false)
        end
        curItem.transform:Find("Req"):GetComponent(typeof(UILabel)).text = req
    end

    UIEventListener.Get(curItem.transform:Find("Border").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        local default = nil
        if now and req and now < req then
            default = DefaultItemActionDataSource.Create(1, {function ( ... )
                CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
                CItemAccessListMgr.Inst:ShowItemAccessInfo(itemID, false, nil, CTooltip.AlignType.Right)
            end}, {LocalString.GetString("获取")})
        end
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, default, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
end

