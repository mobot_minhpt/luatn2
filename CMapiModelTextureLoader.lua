-- Auto Generated!!
local CClientLingShou = import "L10.Game.CClientLingShou"
local CClientObject = import "L10.Game.CClientObject"
local CLingShouBaseMgr = import "L10.Game.CLingShouBaseMgr"
local CLogMgr = import "L10.CLogMgr"
local CMapiModelTextureLoader = import "L10.UI.CMapiModelTextureLoader"
local CRenderObject = import "L10.Engine.CRenderObject"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local LayerDefine = import "L10.Engine.LayerDefine"
local LocalString = import "LocalString"
local Vector3 = import "UnityEngine.Vector3"
CMapiModelTextureLoader.m_Init_CS2LuaHook = function (this, mapiInfo, prop1, prop2, angle, zuoqiId) 
    this.mCurMapiInfo = mapiInfo
    this.mCurLingShouDetail1 = prop1
    this.mCurLingShouDetail2 = prop2
    this.mRotAngle = angle

    this.mCurZuoQiId = zuoqiId

    this.texture.mainTexture = this:CreateTexture()
end
CMapiModelTextureLoader.m_OnUpdateMapiZuojuAppearance_CS2LuaHook = function (this, zqid, toushiId, maanId, tunshiId) 
    if this.mCurZuoQiId == zqid and this.mCurMapiInfo ~= nil then
        this.mCurMapiInfo.MaanId = maanId
        this.mCurMapiInfo.ToushiId = toushiId
        this.mCurMapiInfo.TunshiId = tunshiId

        this.texture.mainTexture = this:CreateTexture()
    end
end
CMapiModelTextureLoader.m_Load_CS2LuaHook = function (this, renderObj) 
    if this.mCurMapiInfo ~= nil then
        local app = CZuoQiMgr.CreateMapiAppearanceFromMapiProp(this.mCurMapiInfo)
        CClientObject.LoadMapiRenderObject(renderObj, app, false)
        renderObj.Scale = CMapiModelTextureLoader.sModelScale

        local Name
        local resName
        local resId
        local scale
        local FX
        local fxColor
        local fxScale = 1
        local evolveGrade
        local templateId
        local piXiangTemplateId
        local yirongFx = 0
        if this.mCurLingShouDetail1 ~= nil then
            templateId = this.mCurLingShouDetail1.data.TemplateId

            if this.mCurLingShouDetail1.data.Props.TemplateIdForSkin > 0 then
                piXiangTemplateId = this.mCurLingShouDetail1.data.Props.TemplateIdForSkin
                evolveGrade = this.mCurLingShouDetail1.data.Props.EvolveGradeForSkin
            else
                piXiangTemplateId = 0
                evolveGrade = this.mCurLingShouDetail1.data.Props.EvolveGrade
            end

            if evolveGrade <= 0 then
                CLogMgr.Log(LocalString.GetString("evolveGrade 不对：") .. this.mCurLingShouDetail1.data.Props.EvolveGrade)
                evolveGrade = 1
            end


            local default
            default, Name, resName, resId, scale, FX, fxColor, fxScale, yirongFx = CLingShouBaseMgr.GetLingShouModel(templateId, piXiangTemplateId, evolveGrade, true)
            if default then
                local lingshou1RO = CRenderObject.CreateRenderObject(renderObj.gameObject, "Lingshou1")
                local scaleMulti = CMapiModelTextureLoader.sLingshouLocalScale
                local fxScaleMulti = scaleMulti*renderObj.Scale
                CClientLingShou.LoadResource(lingshou1RO, Name, resName, resId, scale*scaleMulti, FX, fxColor, fxScale*fxScaleMulti, true, yirongFx)
                lingshou1RO.Layer = LayerDefine.ModelForNGUI_3D
                lingshou1RO.gameObject.transform.localPosition = CMapiModelTextureLoader.sLingshou1Offset1
                lingshou1RO.gameObject.transform.localEulerAngles = Vector3.zero
            end
        end

        if this.mCurLingShouDetail2 ~= nil then
            templateId = this.mCurLingShouDetail2.data.TemplateId

            if this.mCurLingShouDetail2.data.Props.TemplateIdForSkin > 0 then
                piXiangTemplateId = this.mCurLingShouDetail2.data.Props.TemplateIdForSkin
                evolveGrade = this.mCurLingShouDetail2.data.Props.EvolveGradeForSkin
            else
                piXiangTemplateId = 0
                evolveGrade = this.mCurLingShouDetail2.data.Props.EvolveGrade
            end

            if evolveGrade <= 0 then
                CLogMgr.Log(LocalString.GetString("evolveGrade 不对：") .. this.mCurLingShouDetail2.data.Props.EvolveGrade)
                evolveGrade = 1
            end


            local extern
            extern, Name, resName, resId, scale, FX, fxColor, fxScale, yirongFx = CLingShouBaseMgr.GetLingShouModel(templateId, piXiangTemplateId, evolveGrade, true)
            if extern then
                local lingshou2RO = CRenderObject.CreateRenderObject(renderObj.gameObject, "Lingshou2")
                local scaleMulti = CMapiModelTextureLoader.sLingshouLocalScale
                local fxScaleMulti = scaleMulti*renderObj.Scale
                CClientLingShou.LoadResource(lingshou2RO, Name, resName, resId, scale*scaleMulti, FX, fxColor, fxScale*fxScaleMulti, true, yirongFx)
                lingshou2RO.Layer = LayerDefine.ModelForNGUI_3D
                lingshou2RO.gameObject.transform.localPosition = CMapiModelTextureLoader.sLingshou1Offset2
                lingshou2RO.gameObject.transform.localEulerAngles = Vector3.zero
            end
        end
    end
end
