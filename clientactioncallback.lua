local ClientAction = import "L10.UI.ClientAction"
local CTaskMgr=import "L10.Game.CTaskMgr"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CUIActionMgr = import "L10.UI.CUIActionMgr"
local CScheduleMgr = import "L10.Game.CScheduleMgr"

g_ClientActionCallback={}
ClientAction.m_ExecuteInLua = function (actionName, param1, param2, param3)
	if g_ClientActionCallback[actionName] then
		g_ClientActionCallback[actionName](actionName, param1, param2, param3)
	end
end

g_ClientActionCallback["opensdkjingling"]=function(actionName, param1, param2, param3)
	LuaJingLingMgr.OpenSDKJingLing(param1)--参数为空是打开默认页
end

g_ClientActionCallback["OpenUrlInGame"]=function(actionName, param1, param2, param3)
	if param1 and param1~="" then
		CWebBrowserMgr.Inst:OpenUrl(param1)
	end
end

g_ClientActionCallback["openvideo"]=function(actionName, param1, param2, param3)
	LuaJingLingMgr.PlayRemoteVideo(param1)
end

g_ClientActionCallback["AutoAcceptTask"]=function(actionName, param1, param2, param3)
	local taskId = tonumber(param1)
	if not taskId or taskId == 0 then
		return
	end

	Gac2Gas.RequestAutoAcceptTask(taskId)
end

g_ClientActionCallback["OpenCityWarAwardWnd"]=function(actionName, param1, param2, param3)
	Gac2Gas.QueryTerritoryOccupyAwardList()
end

g_ClientActionCallback["OpenActivityWithBuiltInExplorer"] = function(actionName, param1, param2, param3)
	if param1 and param1~="" then
		if param1=="JiaYuan2022Index" then
			CLuaWebBrowserMgr.OpenHouseActivityUrl("JiaYuan2022Index")
		elseif param1 == "ZongPaiXuanMei2021" then
			CWebBrowserMgr.Inst:OpenUrl("https://qnm.163.com/2021/mrt/mh/#/list/")
		elseif param1 == "2021wymusic_ad" then
			CWebBrowserMgr.Inst:OpenUrl("https://music.163.com/m/at/60ebf2e2911517fcd95374bb?market=qiannvshouyou")
		--盒马鲜生
		elseif param1 == "HeMa2021Index" then
			CWebBrowserMgr.Inst:OpenUrl("https://qnm.163.com/2021/hmxs/mh/")
		elseif param1 == "HeMa2021Prize" then
			CWebBrowserMgr.Inst:OpenUrl("https://qnm.163.com/2021/hmxs/mh//#/prize/")
		elseif param1 == "HeMa2021Rank" then
			CWebBrowserMgr.Inst:OpenUrl("https://qnm.163.com/2021/hmxs/mh//#/rank/")
		elseif param1 == "HeMa2021Personal" then
			CWebBrowserMgr.Inst:OpenUrl("https://qnm.163.com/2021/hmxs/mh//#/personal/")
		else
			CWebBrowserMgr.Inst:OpenActivityUrl(param1)
		end
	end
end

g_ClientActionCallback["YouHuaChe"] = function(actionName, param1, param2, param3)
	Gac2Gas.RequestEnterYouHuaChePlay()
end

g_ClientActionCallback["QianYingChuWenShowMessage"] = function(actionName, param1, param2, param3)
	LuaQianYingChuWenMgr:ShowMessage(param1, param2, param3)
end

g_ClientActionCallback["JoinZhongQiu2022Schedule"] = function(actionName, param1, param2, param3)
	if param1 and param2 and param3 then
		if CLuaScheduleMgr:GetScheduleInfo(tonumber(param1)) and CScheduleMgr.Inst:IsCanJoinSchedule(tonumber(param1), true) then
			local p = {}
			for d in string.gmatch(param3, "%d+") do
				table.insert(p, tonumber(d))
			end
			if #p == 4 then
				CTrackMgr.Inst:FindNPC(p[1], p[2], p[3], p[4], nil, nil)
				CUIManager.CloseUI(CLuaUIResources.ZhongQiu2023MainWnd)
			end
		else
			g_MessageMgr:ShowMessage(param2)
		end
	end
end

g_ClientActionCallback["ShowYaoZhengMsg"] = function(actionName, param1, param2, param3)
	g_MessageMgr:ShowMessage("YaoZhengQingYin_FindWay")
	CUIManager.CloseUI(CLuaUIResources.ZYJ2022MainWnd)
end

g_ClientActionCallback["QiXi2022FindNpcButton"]=function(actionName, param1, param2, param3)
	CUIManager.CloseUI(CLuaUIResources.QiXi2022ScheduleWnd)
	local data = QiXi2022_Setting.GetData().XuanBaNpc
	CTrackMgr.Inst:FindNPC(data[0], data[1], data[2], data[3], nil, nil)
end

g_ClientActionCallback["GuildTerritoryWarTeleportToMyCity"]=function(actionName, param1, param2, param3)
	LuaGuildTerritorialWarsMgr:TeleportToMyCity()
end

g_ClientActionCallback["FindNpcAndCloseWnd"]=function(actionName, param1, param2, param3)
	local p = {}
	if param1 and param2 then
		for d in string.gmatch(param1, "%d+") do
			table.insert(p, tonumber(d))
		end
		if #p == 4 then
			CTrackMgr.Inst:FindNPC(p[1], p[2], p[3], p[4], nil, nil)
		end
		CUIManager.CloseUI(CLuaUIResources[param2])
	end
end

g_ClientActionCallback["JoinGaoChang"]=function(actionName, param1, param2, param3)
	if LuaGaoChangJiDouMgr:IsOpen() then
		CUIActionMgr.Inst:Show("YuanXiaoGaoChangEnterWnd", nil, nil, nil, nil, nil)
	else
		CScheduleMgr.Inst:DoScheduleTask(42000018)
	end
end

g_ClientActionCallback["QingMing2023Schedule"]=function(actionName, param1, param2, param3)
	LuaQingMing2023Mgr:ShowQingMing2023ScheduleInfo(param1)
end

g_ClientActionCallback["OpenPlayScoreShop"] = function(actionName, param1, param2, param3)
	local itemTemplateId = tonumber(param2)
	CLuaNPCShopInfoMgr.ShowScoreShop(param1, itemTemplateId)
end
