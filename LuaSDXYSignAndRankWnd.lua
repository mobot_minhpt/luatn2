local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UIScrollView = import "UIScrollView"
local UITable = import "UITable"
local QnRadioBox = import "L10.UI.QnRadioBox"
local Extensions = import "Extensions"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CommonDefs = import "L10.Game.CommonDefs"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"

LuaSDXYSignAndRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSDXYSignAndRankWnd, "RuleView", "RuleView", GameObject)
RegistChildComponent(LuaSDXYSignAndRankWnd, "SignBottom", "SignBottom", GameObject)
RegistChildComponent(LuaSDXYSignAndRankWnd, "MatchingBottom", "MatchingBottom", GameObject)
RegistChildComponent(LuaSDXYSignAndRankWnd, "RestCountLabel", "RestCountLabel", UILabel)
RegistChildComponent(LuaSDXYSignAndRankWnd, "RuleScrollView", "RuleScrollView", UIScrollView)
RegistChildComponent(LuaSDXYSignAndRankWnd, "RuleTable", "RuleTable", UITable)
RegistChildComponent(LuaSDXYSignAndRankWnd, "ParagraphTemplate", "ParagraphTemplate", GameObject)
RegistChildComponent(LuaSDXYSignAndRankWnd, "RadioBox", "RadioBox", QnRadioBox)
RegistChildComponent(LuaSDXYSignAndRankWnd, "GroupSignBtn", "GroupSignBtn", GameObject)
RegistChildComponent(LuaSDXYSignAndRankWnd, "SingleSignBtn", "SingleSignBtn", GameObject)
RegistChildComponent(LuaSDXYSignAndRankWnd, "CacelMatchBtn", "CacelMatchBtn", GameObject)
RegistChildComponent(LuaSDXYSignAndRankWnd, "SDXYRankView", "SDXYRankView", GameObject)

--@endregion RegistChildComponent end
--RegistClassMember(LuaSDXYSignAndRankWnd,"m_RuleInited")
function LuaSDXYSignAndRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.GroupSignBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGroupSignBtnClick()
	end)


	
	UIEventListener.Get(self.SingleSignBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSingleSignBtnClick()
	end)


	
	UIEventListener.Get(self.CacelMatchBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCacelMatchBtnClick()
	end)


    --@endregion EventBind end

	--self.m_RuleInited = false
    self.ParagraphTemplate:SetActive(false)
	self.RadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
        if index == 0 then
            self.RuleView:SetActive(true)
            self.SDXYRankView:SetActive(false)
        elseif index == 1 then
            if not self.m_HasReceivedRankData then
                self.RuleView:SetActive(false)
                self.SDXYRankView:SetActive(true)
                Gac2Gas.QueryXingYuRank()
                -- g_ScriptEvent:BroadcastInLua("SDXYQueryRankResult")
            else
                self.RuleView:SetActive(false)
                self.SDXYRankView:SetActive(true)
            end
        end
    end)
end

function LuaSDXYSignAndRankWnd:Init()
	self.RadioBox:ChangeTo(0)
	local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(g_MessageMgr:FormatMessage("2021ChristmasSDXY_RULE"))
	Extensions.RemoveAllChildren(self.RuleTable.transform)
	if info ~= nil then
        do
            local i = 0
            while i < info.paragraphs.Count do
                local paragraphGo = CUICommonDef.AddChild(self.RuleTable.gameObject, self.ParagraphTemplate)
                paragraphGo:SetActive(true)
                CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem)):Init(info.paragraphs[i], 4294967295)
                i = i + 1
            end
        end
        self.RuleTable:Reposition()
    end

    self:InitBottom()

    if LuaChristmas2021Mgr.IsNeedOpenSDXYRankTab then
        self:OnSDXYQueryRankResult()
    end
end

function LuaSDXYSignAndRankWnd:InitBottom()
    self.MatchingBottom:SetActive(LuaChristmas2021Mgr.ISXingYunMatching)
    self.SignBottom:SetActive(not LuaChristmas2021Mgr.ISXingYunMatching)
    local joinedTime = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eXingyuEnterTimes)
    local limitTime = Christmas2021_Setting.GetData().XingYuMaxTimePerDay
    self.RestCountLabel.text = SafeStringFormat3("%d/%d",joinedTime,limitTime)
end

function LuaSDXYSignAndRankWnd:OnEnable()
    g_ScriptEvent:AddListener("RefreshSDXYMatchSate",self,"OnRefreshSDXYMatchSate")
    g_ScriptEvent:AddListener("SDXYQueryRankResult",self,"OnSDXYQueryRankResult")
end

function LuaSDXYSignAndRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RefreshSDXYMatchSate",self,"OnRefreshSDXYMatchSate")
    g_ScriptEvent:RemoveListener("SDXYQueryRankResult",self,"OnSDXYQueryRankResult")
    LuaChristmas2021Mgr.IsNeedOpenSDXYRankTab = false

end


--@region UIEvent

function LuaSDXYSignAndRankWnd:OnGroupSignBtnClick()
    local isTeam = true
    Gac2Gas.RequestSignXingyu2021(isTeam)
end

function LuaSDXYSignAndRankWnd:OnSingleSignBtnClick()
    local isTeam = false
    Gac2Gas.RequestSignXingyu2021(isTeam)
end

function LuaSDXYSignAndRankWnd:OnCacelMatchBtnClick()
    Gac2Gas.RequestCancelSignXingyu2021()
end

--@endregion UIEvent

------callback-----------
function LuaSDXYSignAndRankWnd:OnSDXYQueryRankResult()
    self.m_HasReceivedRankData = true
    self.RadioBox:ChangeTo(1,true)

    local rankView = self.SDXYRankView:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
    rankView:Refresh()
end

function LuaSDXYSignAndRankWnd:OnRefreshSDXYMatchSate(isMatching)
    LuaChristmas2021Mgr.ISXingYunMatching = isMatching
    self:InitBottom()
end
