local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CRankMasterView = import "L10.UI.CRankMasterView"
local GameObject = import "UnityEngine.GameObject"
local CRankDetailView = import "L10.UI.CRankDetailView"
local CRankItemData = import "L10.UI.CRankItemData"
local CRankData = import "L10.UI.CRankData"
local EnumRankSheet = import "L10.UI.EnumRankSheet"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local UITableTween = import "L10.UI.UITableTween"

LuaNewRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaNewRankWnd, "MasterView", "MasterView", CRankMasterView)
RegistChildComponent(LuaNewRankWnd, "AppearView", "AppearView", GameObject)
RegistChildComponent(LuaNewRankWnd, "DetailView", "DetailView", CRankDetailView)
RegistChildComponent(LuaNewRankWnd, "FullDetailView", "FullDetailView", CRankDetailView)

--@endregion RegistChildComponent end
RegistClassMember(LuaNewRankWnd, "m_CurDetailView")
RegistClassMember(LuaNewRankWnd, "m_TabItem")
RegistClassMember(LuaNewRankWnd, "m_DetailViewIndicator")
RegistClassMember(LuaNewRankWnd, "m_FullDetailViewIndicator")

function LuaNewRankWnd:Awake()
    self.m_DetailViewIndicator = self.DetailView.transform:Find("RankList/ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator))
    self.m_FullDetailViewIndicator = self.FullDetailView.transform:Find("RankList/ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator))
    self.m_FullDetailTable = self.FullDetailView.transform:Find("RankList/TableBody/Table"):GetComponent(typeof(UITable))
    self.m_FullDetailTemplate = self.FullDetailView.transform:Find("RankList/ItemTemplate").gameObject
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_CurDetailView = self.DetailView

end

function LuaNewRankWnd:Init()
    self.MasterView.onListItemClick = DelegateFactory.Action_List_CRankItemData(function(tabItem)
        self:UpdateTabItem(tabItem)
    end)
end

function LuaNewRankWnd:Update()
    --self.m_FullDetailTable:Reposition()
end

function LuaNewRankWnd:OnEnable()
	g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
    g_ScriptEvent:AddListener("OpenOrCloseWinSocialWnd", self, "OnScreenChange")
    if CommonDefs.Is_UNITY_STANDALONE_WIN() then
        g_ScriptEvent:AddListener("OnWinScreenChangeAfterResolutionCheck", self, "OnScreenChange")
    else
        g_ScriptEvent:AddListener("OnScreenChange", self, "OnScreenChange")
    end
end

function LuaNewRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
    g_ScriptEvent:RemoveListener("OpenOrCloseWinSocialWnd", self, "OnScreenChange")
    if CommonDefs.Is_UNITY_STANDALONE_WIN() then
        g_ScriptEvent:RemoveListener("OnWinScreenChangeAfterResolutionCheck", self, "OnScreenChange")
    else
        g_ScriptEvent:RemoveListener("OnScreenChange", self, "OnScreenChange")
    end
end

--@region UIEvent

--@endregion UIEvent

function LuaNewRankWnd:UpdateTabItem(tabItem)
    self.m_TabItem = tabItem
    self.m_CurDetailView:InitWithTabs(tabItem)
end

function LuaNewRankWnd:OnRankDataReady()
    local info = CRankData.Inst.MainPlayerRankInfo
    if info then
        local nextDetail = nil
        local setting = Rank_SettingNew.GetData(CRankData.GetRankSheetKey(info.rankSheet))
        if setting.ShowModel > 0 then
            nextDetail = self.DetailView
        else
            nextDetail = self.FullDetailView
        end

        if nextDetail ~= self.m_CurDetailView then
            self.m_CurDetailView.gameObject:SetActive(false)
            nextDetail.gameObject:SetActive(true)
            nextDetail:UpdateInfo()
            if self.m_TabItem then
                nextDetail:InitWithTabs(self.m_TabItem)
            end
        end
        self.m_CurDetailView = nextDetail
    end
end

function LuaNewRankWnd:OnScreenChange()
    self.m_DetailViewIndicator:Layout()
    self.m_FullDetailViewIndicator:Layout()
    self.m_FullDetailTable:Reposition()
end