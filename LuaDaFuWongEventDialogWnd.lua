local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local Animation = import "UnityEngine.Animation"
LuaDaFuWongEventDialogWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongEventDialogWnd, "Icon", "Icon", GameObject)
RegistChildComponent(LuaDaFuWongEventDialogWnd, "Dialog", "Dialog", UILabel)
RegistChildComponent(LuaDaFuWongEventDialogWnd, "BgTx", "BgTx", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongEventDialogWnd, "m_anim")
RegistClassMember(LuaDaFuWongEventDialogWnd, "m_aniTick")
function LuaDaFuWongEventDialogWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_anim = self.gameObject:GetComponent(typeof(Animation))
    self.m_aniTick = nil
    --self.gameObject:SetActive(false)
end

function LuaDaFuWongEventDialogWnd:Init()
    if LuaDaFuWongMgr.EventDialogInfo then
        self.Dialog.text = SafeStringFormat3("[915844]%s[-]",LuaDaFuWongMgr.EventDialogInfo.text) 
    end

    if self.m_aniTick then
        UnRegisterTick(self.m_aniTick)
        self.m_aniTick = nil
    end
    self.m_aniTick = RegisterTickOnce(function()
        self:PlayEnd()
    end,3000)
end
function LuaDaFuWongEventDialogWnd:PlayEnd()

    CUIManager.CloseUI(CLuaUIResources.DaFuWongEventDialogWnd)
end

function LuaDaFuWongEventDialogWnd:OnDisable()
    if self.m_aniTick then
        UnRegisterTick(self.m_aniTick)
        self.m_aniTick = nil
    end
end

--@region UIEvent

--@endregion UIEvent

