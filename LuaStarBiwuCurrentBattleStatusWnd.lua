local ShareMgr=import "ShareMgr"
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local AlignType=import "L10.UI.CPopupMenuInfoMgr+AlignType"
local UIGrid = import "UIGrid"
local System = import "System"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local Main = import "L10.Engine.Main"
local Json = import "L10.Game.Utils.Json"
local HTTPHelper = import "L10.Game.HTTPHelper"
local ETickType = import "L10.Engine.ETickType"
local EShareType = import "L10.UI.EShareType"
local CTickMgr = import "L10.Engine.CTickMgr"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local Constants = import "L10.Game.Constants"
local Color = import "UnityEngine.Color"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CFileTools = import "CFileTools"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Profession = import "L10.Game.Profession"
local Extensions = import "Extensions"

CLuaStarBiwuCurrentBattleStatusWnd = class()
RegistClassMember(CLuaStarBiwuCurrentBattleStatusWnd,"CloseButton")
RegistClassMember(CLuaStarBiwuCurrentBattleStatusWnd,"NameLabels")
RegistClassMember(CLuaStarBiwuCurrentBattleStatusWnd,"ScoreLabels")
RegistClassMember(CLuaStarBiwuCurrentBattleStatusWnd,"WinLoseMarkSprites")
RegistClassMember(CLuaStarBiwuCurrentBattleStatusWnd,"m_MyTeamSprites")
RegistClassMember(CLuaStarBiwuCurrentBattleStatusWnd,"m_Grid")
RegistClassMember(CLuaStarBiwuCurrentBattleStatusWnd,"m_BattleItem")
RegistClassMember(CLuaStarBiwuCurrentBattleStatusWnd,"m_ShareBtn")
RegistClassMember(CLuaStarBiwuCurrentBattleStatusWnd,"m_StopButton")
RegistClassMember(CLuaStarBiwuCurrentBattleStatusWnd,"m_IsReplyFightData")
RegistClassMember(CLuaStarBiwuCurrentBattleStatusWnd,"m_StatusList")
RegistClassMember(CLuaStarBiwuCurrentBattleStatusWnd,"m_PicCoroutine")
RegistClassMember(CLuaStarBiwuCurrentBattleStatusWnd,"m_UrlCoroutine")

--分享参数
RegistClassMember(CLuaStarBiwuCurrentBattleStatusWnd,"m_TeamName")
RegistClassMember(CLuaStarBiwuCurrentBattleStatusWnd,"m_PlayStage")

function CLuaStarBiwuCurrentBattleStatusWnd:Awake()
    self.CloseButton = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
self.NameLabels = {}
self.NameLabels[0]=FindChild(self.transform,"NameLabel1"):GetComponent(typeof(UILabel))
self.NameLabels[1]=FindChild(self.transform,"NameLabel2"):GetComponent(typeof(UILabel))
self.ScoreLabels = {}
self.ScoreLabels[0]=FindChild(self.transform,"ScoreLabel1"):GetComponent(typeof(UILabel))
self.ScoreLabels[1]=FindChild(self.transform,"ScoreLabel2"):GetComponent(typeof(UILabel))
self.WinLoseMarkSprites = {}
self.WinLoseMarkSprites[0]=FindChild(self.transform,"ResSprite1"):GetComponent(typeof(UISprite))
self.WinLoseMarkSprites[1]=FindChild(self.transform,"ResSprite2"):GetComponent(typeof(UISprite))
self.WinLoseMarkSprites[0].spriteName = nil
self.WinLoseMarkSprites[1].spriteName = nil
self.m_MyTeamSprites = {}
self.m_MyTeamSprites[0]=FindChild(self.transform,"TeamSprite1"):GetComponent(typeof(UISprite))
self.m_MyTeamSprites[1]=FindChild(self.transform,"TeamSprite2"):GetComponent(typeof(UISprite))

    self.m_Grid = self.transform:Find("ShowArea/ContentArea/AdvView/Scroll View/Grid").gameObject
    self.m_BattleItem = self.transform:Find("ShowArea/ContentArea/AdvView/BattleItem").gameObject
    self.m_ShareBtn = self.transform:Find("ShowArea/ShareBtn").gameObject
self.m_StopButton = self.transform:Find("ShowArea/StopButton").gameObject
self.m_IsReplyFightData = false
self.m_StatusList = nil
self.m_PicCoroutine = nil
self.m_UrlCoroutine = nil

local dataButton=FindChild(self.transform,"DataButton").gameObject
--如果观战的话 就不显示这个按钮
    dataButton:SetActive(true)
    UIEventListener.Get(dataButton).onClick=DelegateFactory.VoidDelegate(function(go)
        CLuaStarBiwuMgr.m_BattleDataWnd_Season = CLuaStarBiwuMgr.m_CurrentBattleStatus.m_JieShu
        CUIManager.ShowUI(CLuaUIResources.StarBiwuBattleDataWnd)
    end)

self.m_StopButton:SetActive(false)
UIEventListener.Get(self.m_StopButton).onClick = DelegateFactory.VoidDelegate(function(go)
    local msg = g_MessageMgr:FormatMessage("STAR_BIWU_STOP_CONFIRM")
    MessageWndManager.ShowConfirmMessage(msg, 10, false, DelegateFactory.Action(function()
        Gac2Gas.RequestStopStarBiwuFight()
    end),nil,false)
end)

end

-- Auto Generated!!



function CLuaStarBiwuCurrentBattleStatusWnd:OnEnable( )
	g_ScriptEvent:AddListener("ReplyStarBiWuZhanKuang", self, "OnReplyStatus")
    g_ScriptEvent:AddListener("ReplyStarBiwuAllMemberFightData", self, "OnReplyFightData")
    g_ScriptEvent:AddListener("StarBiWuPlayEnd", self, "StarBiWuPlayEnd")

end
function CLuaStarBiwuCurrentBattleStatusWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("ReplyStarBiWuZhanKuang", self, "OnReplyStatus")
	g_ScriptEvent:RemoveListener("ReplyStarBiwuAllMemberFightData", self, "OnReplyFightData")
    g_ScriptEvent:RemoveListener("StarBiWuPlayEnd", self, "StarBiWuPlayEnd")
end
function CLuaStarBiwuCurrentBattleStatusWnd:StarBiWuPlayEnd()
    --再请求一次，看看是否可以分享
    Gac2Gas.QueryStarBiwuAllMemberFightData()
end

function CLuaStarBiwuCurrentBattleStatusWnd:OnReplyFightData( playStage,playIndex )
    self.m_PlayStage=playStage
    self.m_IsReplyFightData = true

    if nil == CClientMainPlayer.Inst then
        return
    end

    local found=false
    local myId=CClientMainPlayer.Inst.Id
    for i=1,#CLuaStarBiwuMgr.m_ZhanduiFightData do
        local data=CLuaStarBiwuMgr.m_ZhanduiFightData[i]
        if data.m_PlayerId==myId then
            found=true
            break
        end
    end

    if found then
        --self.m_ShareBtn:SetActive(true)
    end
end
function CLuaStarBiwuCurrentBattleStatusWnd:OnReplyStatus( )
    local battleData=CLuaStarBiwuMgr.m_CurrentBattleStatus
    self.m_StatusList = battleData.m_Status

    Extensions.RemoveAllChildren(self.m_Grid.transform)
    local winnerNeedPoints = 3
    if battleData.m_JieShu >= 11 then
        battleData.m_AttackWinCount = 0
        battleData.m_DefendWinCount = 0
        winnerNeedPoints = StarBiWuShow_Setting.GetData().Win_Need_Score 
    end
    local scoreArrList = StarBiWuShow_Setting.GetData().Round_Duration_And_Score
    do
        local i = 0
        while i < #self.m_StatusList do
            local go = NGUITools.AddChild(self.m_Grid, self.m_BattleItem)
            go:SetActive(true)
            self:InitItem(go.transform,self.m_StatusList[i+1], i)
            if battleData.m_JieShu >= 11 then
                if self.m_StatusList[i+1].m_Win == 1 then
                    battleData.m_AttackWinCount =  battleData.m_AttackWinCount + scoreArrList[i][1]
                elseif self.m_StatusList[i+1].m_Win == 0 then
                    battleData.m_DefendWinCount = battleData.m_DefendWinCount + scoreArrList[i][1]
                end
            end
            i = i + 1
        end
    end

    self.m_Grid:GetComponent(typeof(UIGrid)):Reposition()


    self.NameLabels[0].text = battleData.m_AttackName
    self.NameLabels[1].text = battleData.m_DefendName
    self.ScoreLabels[0].text = tostring(battleData.m_AttackWinCount)
    self.ScoreLabels[1].text = tostring(battleData.m_DefendWinCount)

    if battleData.m_AttackWinCount >= winnerNeedPoints or battleData.m_DefendWinCount >= winnerNeedPoints then
        if battleData.m_AttackWinCount > battleData.m_DefendWinCount then
            self.WinLoseMarkSprites[0].spriteName = Constants.WinSprite
            self.WinLoseMarkSprites[1].spriteName = Constants.LoseSprite
        else
            self.WinLoseMarkSprites[0].spriteName = Constants.LoseSprite
            self.WinLoseMarkSprites[1].spriteName = Constants.WinSprite
        end
        Extensions.SetLocalPositionZ(self.WinLoseMarkSprites[0].transform, battleData.m_AttackWinCount > battleData.m_DefendWinCount and 0 or -1)
        Extensions.SetLocalPositionZ(self.WinLoseMarkSprites[1].transform, battleData.m_AttackWinCount > battleData.m_DefendWinCount and -1 or 0)
    else
        self.WinLoseMarkSprites[0].gameObject:SetActive(false)
        self.WinLoseMarkSprites[1].gameObject:SetActive(false)
        Extensions.SetLocalPositionZ(self.WinLoseMarkSprites[0].transform, 0)
        Extensions.SetLocalPositionZ(self.WinLoseMarkSprites[1].transform, 0)
    end
    self.m_MyTeamSprites[0].gameObject:SetActive(battleData.m_SelfForce == EnumCommonForce_lua.eAttack)
    self.m_MyTeamSprites[1].gameObject:SetActive(battleData.m_SelfForce == EnumCommonForce_lua.eDefend)
    self.NameLabels[0].color = battleData.m_SelfForce == EnumCommonForce_lua.eAttack and Color.green or Color.white
    self.NameLabels[1].color = battleData.m_SelfForce == EnumCommonForce_lua.eDefend and Color.green or Color.white

    if battleData.m_SelfForce == EnumCommonForce_lua.eAttack then
        self.m_TeamName=battleData.m_AttackName
    else
        self.m_TeamName=battleData.m_DefendName
    end

    if not CLuaStarBiwuMgr.m_IsGameVideoBattleStatus and CLuaStarBiwuMgr.m_PlayStage == CLuaStarBiwuMgr.EnumStarBiwuShowPlayStatus.eZongJueSai and CLoginMgr.Inst:IsNetEaseOfficialLogin() then
        self.m_StopButton:SetActive(true)
    end
end
function CLuaStarBiwuCurrentBattleStatusWnd:Start( )
    UIEventListener.Get(self.m_ShareBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnShareBtnClicked(go) end)
    self.m_ShareBtn:SetActive(false)
end
function CLuaStarBiwuCurrentBattleStatusWnd:Init( )
    self.m_BattleItem:SetActive(false)

    if CLuaStarBiwuMgr.m_IsGameVideoBattleStatus then
        self:OnReplyStatus()
    else
        CLuaStarBiwuMgr.m_CurrentBattleStatus=nil
        Gac2Gas.QueryStarBiWuZhanKuang()
        Gac2Gas.QueryStarBiwuAllMemberFightData()
    end

end
function CLuaStarBiwuCurrentBattleStatusWnd:OnShareBtnClicked( go)
    local function SelectAction(index)
        if index==0 then
            self:Share(1)--梦岛
        else
            self:Share(2)--普通
        end
    end
    local selectShareItem=DelegateFactory.Action_int(SelectAction)

    local t={}
    local item=PopupMenuItemData(LocalString.GetString("至报名角色梦岛"),selectShareItem,false,nil)
    table.insert(t, item)
    local item=PopupMenuItemData(LocalString.GetString("至游戏外"),selectShareItem,false,nil)
    table.insert(t, item)

    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType.Top,1,nil,600,true,360)
end

function CLuaStarBiwuCurrentBattleStatusWnd:ShareTicketImage( imageUrl)
    if self.m_PicCoroutine ~= nil then
        Main.Inst:StopCoroutine(self.m_PicCoroutine)
        self.m_PicCoroutine = nil
    end

    self.m_PicCoroutine = Main.Inst:StartCoroutine(HTTPHelper.DownloadImage(imageUrl, DelegateFactory.Action_bool_byte_Array(function (picResult, picData)
        local filename = Utility.persistentDataPath .. "/QMPKFightShare.jpg"
        System.IO.File.WriteAllBytes(filename, picData)
        CFileTools.SetFileNoBackup(filename)

        CTickMgr.Register(DelegateFactory.Action(function ()
            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, {filename})
        end), 1000, ETickType.Once)
    end)))
end
function CLuaStarBiwuCurrentBattleStatusWnd:InitItem(transform, status, index)

    local m_Dps1 = transform:Find("DamageLabel1"):GetComponent(typeof(UILabel))
    local m_Dps2 = transform:Find("DamageLabel2"):GetComponent(typeof(UILabel))
    local m_Name1 = transform:Find("SingleNameRoot/NameLabel1"):GetComponent(typeof(UILabel))
    local m_Name2 = transform:Find("SingleNameRoot/NameLabel2"):GetComponent(typeof(UILabel))
    local m_WinObj1 = transform:Find("ResSprite1"):GetComponent(typeof(UISprite))
    local m_WinObj2 = transform:Find("ResSprite2"):GetComponent(typeof(UISprite))
    local m_SigleObj = transform:Find("Single").gameObject
    local m_ThreeObj = transform:Find("Three").gameObject
    local m_GroupObj = transform:Find("Group").gameObject
    local m_TwoObj = transform:Find("Two").gameObject
    local m_Class1 = transform:Find("SingleNameRoot/Class1"):GetComponent(typeof(UISprite))
    local m_Class2 = transform:Find("SingleNameRoot/Class2"):GetComponent(typeof(UISprite))
    local m_Score = transform:Find("Score"):GetComponent(typeof(UILabel))
    local m_Kill1 = transform:Find("Kill1"):GetComponent(typeof(UILabel))
    local m_Kill2 = transform:Find("Kill2"):GetComponent(typeof(UILabel))
    local noneWinRet = status.m_Win == 2
    m_Kill1.text = noneWinRet and "" or tostring(status.m_Kill1)
    m_Kill2.text = noneWinRet and "" or tostring(status.m_Kill2)

    local m_Wait1 = transform:Find("Wait1"):GetComponent(typeof(UILabel))
    local m_Wait2 = transform:Find("Wait2"):GetComponent(typeof(UILabel))
    local myIsLeft = CLuaStarBiwuMgr.m_CurrentBattleStatus.m_SelfForce == EnumCommonForce_lua.eAttack
    local myIsRight = CLuaStarBiwuMgr.m_CurrentBattleStatus.m_SelfForce == EnumCommonForce_lua.eDefend
    local isShowWaitRoot1 = (index > 2) and (CLuaStarBiwuMgr.m_CurrentBattleStatus.m_PlayRound <= 3 and CLuaStarBiwuMgr.m_CurrentBattleStatus.m_PlayRound >= 0) and (myIsRight or CLuaStarBiwuMgr.m_IsGameVideoBattleStatus)
    local isShowWaitRoot2 = (index > 2) and (CLuaStarBiwuMgr.m_CurrentBattleStatus.m_PlayRound <= 3 and CLuaStarBiwuMgr.m_CurrentBattleStatus.m_PlayRound >= 0) and (myIsLeft or CLuaStarBiwuMgr.m_IsGameVideoBattleStatus)
    m_Wait1.gameObject:SetActive(isShowWaitRoot1)
    m_Wait2.gameObject:SetActive(isShowWaitRoot2)
    local m_SingleNameRoot = transform:Find("SingleNameRoot").gameObject
    local m_GroupNameRoot = transform:Find("GroupNameRoot").gameObject
    local m_ThreeNameRoot = transform:Find("ThreeNameRoot").gameObject

    local m_GroupNameGrid={}
    m_GroupNameGrid[0]=FindChild(transform,"Grid1"):GetComponent(typeof(UIGrid))
    m_GroupNameGrid[1]=FindChild(transform,"Grid2"):GetComponent(typeof(UIGrid))
    local m_ThreeNameGrid={}
    m_ThreeNameGrid[0]=FindChild(transform,"Grid3"):GetComponent(typeof(UIGrid))
    m_ThreeNameGrid[1]=FindChild(transform,"Grid4"):GetComponent(typeof(UIGrid))
    if index == ((CLuaStarBiwuMgr.m_CurrentBattleStatus.m_JieShu >= 11) and 4 or 3) then
        m_GroupNameGrid[0].gameObject:SetActive(not isShowWaitRoot1)
        m_GroupNameGrid[1].gameObject:SetActive(not isShowWaitRoot2)
    end
    if index == ((CLuaStarBiwuMgr.m_CurrentBattleStatus.m_JieShu >= 11) and 5 or 4) then
        m_ThreeNameGrid[0].gameObject:SetActive(not isShowWaitRoot1)
        m_ThreeNameGrid[1].gameObject:SetActive(not isShowWaitRoot2)
    end
     m_Dps1.text = noneWinRet and "" or tostring(status.m_Dps1)
     m_Dps2.text = noneWinRet and "" or tostring(status.m_Dps2)
     --m_Name1.text = status.m_Name1;
     --m_Name2.text = status.m_Name2;
     m_WinObj1.gameObject:SetActive(true)
     m_WinObj2.gameObject:SetActive(true)
     local scoreArrList = StarBiWuShow_Setting.GetData().Round_Duration_And_Score
     m_Score.text = (CLuaStarBiwuMgr.m_CurrentBattleStatus.m_JieShu >= 11) and SafeStringFormat3(LocalString.GetString("%d分"),scoreArrList[index][1]) or ""
     if status.m_Win == 1 then
         m_WinObj1.spriteName = Constants.WinSprite
         m_WinObj2.spriteName = Constants.LoseSprite
     elseif status.m_Win == 0 then
         m_WinObj1.spriteName = Constants.LoseSprite
         m_WinObj2.spriteName = Constants.WinSprite
     elseif status.m_Win == 2 then
         m_WinObj1.gameObject:SetActive(false)
         m_WinObj2.gameObject:SetActive(false)
     end
     Extensions.SetLocalPositionZ(m_WinObj1.transform, status.m_Win == 0 and -1 or 0)
     Extensions.SetLocalPositionZ(m_WinObj2.transform, status.m_Win == 1 and -1 or 0)

     transform:GetComponent(typeof(UISprite)).spriteName= index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite

     m_SigleObj:SetActive(false)
     m_ThreeObj:SetActive(false)
     m_GroupObj:SetActive(false)
     m_TwoObj:SetActive(false)
     m_GroupNameRoot:SetActive(false)
     m_SingleNameRoot:SetActive(false)
     m_ThreeNameRoot:SetActive(false)
     if index == 0 or index == ((CLuaStarBiwuMgr.m_CurrentBattleStatus.m_JieShu >= 11) and 4 or 3) then
         m_GroupObj:SetActive(true)
         m_GroupNameRoot:SetActive(true)
         Extensions.RemoveAllChildren(m_GroupNameGrid[0].transform)
         do
             local i = 0 local cnt = #status.m_Class1
             while i < cnt do
                 local go = NGUITools.AddChild(m_GroupNameGrid[0].gameObject, m_Class1.gameObject)
                 local sprite = go:GetComponent(typeof(UISprite))
                 if sprite ~= nil then
                     sprite.spriteName = Profession.GetIconByNumber(status.m_Class1[i+1])
                 end
                 i = i + 1
             end
         end
         m_GroupNameGrid[0]:Reposition()
         Extensions.RemoveAllChildren(m_GroupNameGrid[1].transform)
         do
             local i = 0 local cnt = #status.m_Class2
             while i < cnt do
                 local go = NGUITools.AddChild(m_GroupNameGrid[1].gameObject, m_Class1.gameObject)
                 local sprite = go:GetComponent(typeof(UISprite))
                 if sprite ~= nil then
                     sprite.spriteName = Profession.GetIconByNumber(status.m_Class2[i+1])
                 end
                 i = i + 1
             end
         end
         m_GroupNameGrid[1]:Reposition()
     elseif index == 1 or index == ((CLuaStarBiwuMgr.m_CurrentBattleStatus.m_JieShu >= 11) and 5 or 4) then
         m_SigleObj:SetActive(true)
         m_SingleNameRoot:SetActive(true)
         if #status.m_Class1 > 0 and not isShowWaitRoot1 then
             m_Name1.text = status.m_Name1[1]
             m_Class1.spriteName = Profession.GetIconByNumber( status.m_Class1[1] )
         else
             m_Name1.text = ""
             m_Class1.spriteName = nil
         end

         if #status.m_Class2 > 0 and not isShowWaitRoot2 then
             m_Name2.text = status.m_Name2[1]
             m_Class2.spriteName = Profession.GetIconByNumber( status.m_Class2[1] )
         else
             m_Name2.text = ""
             m_Class2.spriteName = nil
         end
     else
        if CLuaStarBiwuMgr.m_CurrentBattleStatus.m_JieShu >= 11 then
            m_ThreeObj:SetActive(index ~= 2)
            m_TwoObj:SetActive(index == 2) 
        else
            m_ThreeObj:SetActive(true)
            m_TwoObj:SetActive(false)
        end
         m_ThreeNameRoot:SetActive(true)
         Extensions.RemoveAllChildren(m_ThreeNameGrid[0].transform)
         do
             local i = 0 local cnt = #status.m_Class1
             while i < cnt and not isShowWaitRoot1 do
                 local go = NGUITools.AddChild(m_ThreeNameGrid[0].gameObject, m_Class1.gameObject)
                 local sprite = go:GetComponent(typeof(UISprite))
                 if sprite ~= nil then
                     sprite.spriteName = Profession.GetIconByNumber(status.m_Class1[i+1])
                 end
                 i = i + 1
             end
         end
         Extensions.RemoveAllChildren(m_ThreeNameGrid[1].transform)
         do
             local i = 0 local cnt = #status.m_Class2
             while i < cnt and not isShowWaitRoot2 do
                 local go = NGUITools.AddChild(m_ThreeNameGrid[1].gameObject, m_Class1.gameObject)
                 local sprite = go:GetComponent(typeof(UISprite))
                 if sprite ~= nil then
                     sprite.spriteName = Profession.GetIconByNumber(status.m_Class2[i+1])
                 end
                 i = i + 1
             end
         end
     end
 end

function CLuaStarBiwuCurrentBattleStatusWnd:Share(type)
    if self.m_ImgUrl then
		if type==1 then
            -- ShareMgr.ShareWebImage2PersonalSpace(self.m_ImgUrl,nil)
            local id=CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
            local origin_id=CQuanMinPKMgr.Inst.m_MyPersonalInfo and CQuanMinPKMgr.Inst.m_MyPersonalInfo.m_FromCharacterId or id
            CLuaShareMgr.ShareQMPKImage2PersonalSpace(id,origin_id,self.m_ImgUrl)
		else
			ShareMgr.ShareWebImage2Other(self.m_ImgUrl)
		end
		return
	end

	if self.m_UrlCoroutine ~= nil then
        Main.Inst:StopCoroutine(self.m_UrlCoroutine)
	end

    local teamdata="["
    for i,v in ipairs(CLuaStarBiwuMgr.m_ZhanduiFightData) do
        local item=SafeStringFormat3([[{"careerid":%s,"gender":%s,"rolename":%s}]],v.m_PlayerClass,v.m_PlayerGender,Json.Serialize(v.m_PlayerName))

        if i==1 then
            teamdata=teamdata..item
        else
            teamdata=teamdata..","..item
        end
    end
    teamdata=teamdata.."]"


	local url=CLuaStarBiwuMgr.m_ShareUrl.."shareImg/match?"
	local inst=CClientMainPlayer.Inst
	if inst then
		local serverName=CLoginMgr.Inst:GetSelectedGameServer().name

        local myInfo=nil
        local myId=inst.Id

        for i=1,#CLuaStarBiwuMgr.m_ZhanduiFightData do
            local data=CLuaStarBiwuMgr.m_ZhanduiFightData[i]
            if data.m_PlayerId==myId then
                myInfo=data
                break
            end
        end
        local matchdata=""

        if myInfo then
            local item1=SafeStringFormat3( [["%s":{"number":"%s","is_best":%s}]],"dps",myInfo.m_Dps, myInfo.m_BestDps )
            local item2=SafeStringFormat3( [["%s":{"number":"%s","is_best":%s}]],"kill",myInfo.m_KillNum, myInfo.m_BestKillNum )
            local item3=SafeStringFormat3( [["%s":{"number":"%s","is_best":%s}]],"die",myInfo.m_Die,myInfo.m_BestDie )
            local item4=SafeStringFormat3( [["%s":{"number":"%s","is_best":%s}]],"ctrl",myInfo.m_Ctrl, myInfo.m_BestCtrl)
            local item5=SafeStringFormat3( [["%s":{"number":"%s","is_best":%s}]],"relive",myInfo.m_Relive, myInfo.m_BestRelive)
            local item6=SafeStringFormat3( [["%s":{"number":"%s","is_best":%s}]],"heal",myInfo.m_Heal, myInfo.m_BestHeal )
            local item7=SafeStringFormat3( [["%s":{"number":"%s","is_best":%s}]],"dectrl",myInfo.m_RmCtrl,myInfo.m_BestRmCtrl )
            matchdata=SafeStringFormat3("{%s,%s,%s,%s,%s,%s,%s}",item1,item2,item3,item4,item5,item6,item7)
        end

        url = url..SafeStringFormat3("roleid=%s&rolename=%s&gender=%s&careerid=%s&stage=%s&teamname=%s&teamdata=%s&matchdata=%s",
        inst.Id,
        inst.Name,
        EnumToInt(inst.Gender),
        EnumToInt(inst.Class),
        CLuaStarBiwuMgr.GetPlayName(self.m_PlayStage),
        self.m_TeamName,
        teamdata,
        matchdata
    )
    end
	self.m_UrlCoroutine = Main.Inst:StartCoroutine(HTTPHelper.GetUrl(url, DelegateFactory.Action_bool_string(function (success, ret)
		if success then
			local dict = Json.Deserialize(ret)
			local code = CommonDefs.DictGetValue_LuaCall(dict,"code")
			if code and code==1 then
				--成功
                self.m_ImgUrl = CommonDefs.DictGetValue_LuaCall(dict,"imgurl")
				if self.m_ImgUrl then
                    if type==1 then
                        -- if CQuanMinPKMgr.Inst.m_MyPersonalInfo then
                        --     CLuaShareMgr.ShareWebImage2TargetPersonalSpace(CQuanMinPKMgr.Inst.m_MyPersonalInfo.m_FromCharacterId,self.m_ImgUrl)
                        -- end
                        local id=CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
                        local origin_id=CQuanMinPKMgr.Inst.m_MyPersonalInfo and CQuanMinPKMgr.Inst.m_MyPersonalInfo.m_FromCharacterId or id
                        CLuaShareMgr.ShareQMPKImage2PersonalSpace(id,origin_id,self.m_ImgUrl)
					else
						ShareMgr.ShareWebImage2Other(self.m_ImgUrl)
					end
				end
			end
		end
	end)))
end
