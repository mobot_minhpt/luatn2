local CBaseEquipmentItem = import "L10.UI.CBaseEquipmentItem"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local EnumBodyPosition = import "L10.Game.EnumBodyPosition"
local EnumQualityType = import "L10.Game.EnumQualityType"

LuaEquipCompositePane = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaEquipCompositePane, "MainEquip", "MainEquip", CBaseEquipmentItem)
RegistChildComponent(LuaEquipCompositePane, "MaterialEquip", "MaterialEquip", CBaseEquipmentItem)
RegistChildComponent(LuaEquipCompositePane, "ComfirmBtn", "ComfirmBtn", GameObject)
RegistChildComponent(LuaEquipCompositePane, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaEquipCompositePane, "MainEquipDesc", "MainEquipDesc", GameObject)
RegistChildComponent(LuaEquipCompositePane, "MaterialEquipDesc", "MaterialEquipDesc", GameObject)
RegistChildComponent(LuaEquipCompositePane, "SelectBtn", "SelectBtn", GameObject)
RegistChildComponent(LuaEquipCompositePane, "MatSelectBtn", "MatSelectBtn", GameObject)

--@endregion RegistChildComponent end

function LuaEquipCompositePane:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ComfirmBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnComfirmBtnClick()
	end)


	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.SelectBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSelectBtnClick()
	end)


	
	UIEventListener.Get(self.MatSelectBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMatSelectBtnClick()
	end)


    --@endregion EventBind end
end

function LuaEquipCompositePane:OnSelect(args)
	local itemId = args[0]
	--self:InitNull()

	if itemId and itemId ~= "" then
		if self.transform.gameObject.activeSelf and CLuaEquipMgr.m_MainEquipItemId ~=  itemId then
			-- 检查是否有熔炼装备
			CLuaEquipMgr:CheckUnsetComposite(itemId)
			CLuaEquipMgr:CheckUnsetBaptize(itemId)
			CLuaEquipMgr:CheckUnsetRecreate(itemId)
		end
		CLuaEquipMgr.m_MainEquipItemId = itemId
		self:InitEquip(self.MainEquip, self.MainEquipDesc, itemId)
	end

	-- 选择后重置材料装备
	CLuaEquipMgr.m_MaterialEquipItemId = ""

	self:OnRefreshState()

	CUIManager.CloseUI(CLuaUIResources.CommonItemChooseWnd)
end

function LuaEquipCompositePane:OnSelectMaterial(itemId)
	if itemId and itemId ~= "" then
		CLuaEquipMgr.m_MaterialEquipItemId = itemId
		self.MaterialEquip.gameObject:SetActive(true)
		self:InitEquip(self.MaterialEquip, self.MaterialEquipDesc, itemId)
	end

	self:OnRefreshState()
end

-- 重新选择装备后刷新
function LuaEquipCompositePane:OnRefreshState()
	self:UpdateItem()
	local hasMat = CLuaEquipMgr.m_MaterialEquipItemId and CLuaEquipMgr.m_MaterialEquipItemId ~= ""

	self.MaterialEquipDesc:SetActive(hasMat)
	self.MatSelectBtn:SetActive(not hasMat)

	if not hasMat then
		self.MaterialEquip:UpdateData("")
		self.MaterialEquip.EquipIcon:LoadMaterial("")
		self.MaterialEquip.BindSprite.spriteName = ""
		self.MaterialEquip.gameObject:GetComponent(typeof(UISprite)).spriteName = "common_itemcell_border"
	end
end

function LuaEquipCompositePane:InitNull()
	CLuaEquipMgr.m_MaterialEquipItemId = ""
	CLuaEquipMgr.m_MainEquipItemId = ""

	-- 清空状态
	self.MainEquip:UpdateData("")
	self.MaterialEquip:UpdateData("")

	self.MainEquipDesc:SetActive(false)
	self.MaterialEquipDesc:SetActive(false)
end

function LuaEquipCompositePane:InitEquip(equipItem, descRoot, itemId)
	-- 图标/绑定等等信息
	equipItem:UpdateData(itemId)

	local itemData = CItemMgr.Inst:GetById(itemId)
	local equip = itemData.Equip

	local positionLabel = descRoot.transform:Find("Position/Label"):GetComponent(typeof(UILabel))
	local forgeLabel = descRoot.transform:Find("ForgeLevel/Label"):GetComponent(typeof(UILabel))
	local needLvLabel = descRoot.transform:Find("NeedLevel/Label"):GetComponent(typeof(UILabel))
	local wordCountLabel = descRoot.transform:Find("WordCount/Label"):GetComponent(typeof(UILabel))
	local qualityLabel = descRoot.transform:Find("Quality/Label"):GetComponent(typeof(UILabel))

	local data = EquipmentTemplate_Equip.GetData(equip.TemplateId)

	-- 锻造等级
	if equip.IsFeiShengAdjusted >0 then
		forgeLabel.text = tostring(equip.AdjustedFeiShengForgeLevel)
	else
		forgeLabel.text = tostring(equip.ForgeLevel)
	end

	-- 位置
	local typeData = EquipmentTemplate_Type.GetData(data.Type)
	positionLabel.text = typeData.Name

	-- 穿戴等级
	needLvLabel.text = data.Grade

	-- 品质 只有红紫鬼 可以参与
	if data.Color == EnumToInt(EnumQualityType.Red) then
		qualityLabel.text = LocalString.GetString("红装")
	elseif data.Color == EnumToInt(EnumQualityType.Purple) then
		qualityLabel.text = LocalString.GetString("紫装")
	elseif data.Color == EnumToInt(EnumQualityType.Ghost) then
		qualityLabel.text = LocalString.GetString("鬼装")
	end

	wordCountLabel.text = tostring(equip.WordsCount)
end

function LuaEquipCompositePane:UpdateItem()
    -- 新装备融炼相关
    if CLuaEquipMgr.m_MaterialEquipItemId == nil or CLuaEquipMgr.m_MaterialEquipItemId == "" or
        CItemMgr.Inst:GetById(CLuaEquipMgr.m_MaterialEquipItemId) == nil then
        CLuaEquipMgr.m_MaterialEquipItemId = ""
    end
    if CLuaEquipMgr.m_MainEquipItemId == nil or CLuaEquipMgr.m_MainEquipItemId == "" or
        CItemMgr.Inst:GetById(CLuaEquipMgr.m_MainEquipItemId) == nil then
        CLuaEquipMgr.m_MainEquipItemId = ""
    end
    if CLuaEquipMgr.m_ResultEquipItemId == nil or CLuaEquipMgr.m_ResultEquipItemId == "" or
        CItemMgr.Inst:GetById(CLuaEquipMgr.m_ResultEquipItemId) == nil then
        CLuaEquipMgr.m_ResultEquipItemId = ""
    end
end

function LuaEquipCompositePane:OnItemUpdate()
    self:UpdateItem()
	self:OnRefreshState()
end

function LuaEquipCompositePane:OnEnable()
	
	g_ScriptEvent:AddListener("SendItem", self, "OnItemUpdate")
	g_ScriptEvent:AddListener("SetItemAt", self, "OnItemUpdate")
	g_ScriptEvent:AddListener("Equip_Composite_Select_Material_Equip", self, "OnSelectMaterial")
end

function LuaEquipCompositePane:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "OnItemUpdate")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "OnItemUpdate")
	g_ScriptEvent:RemoveListener("Equip_Composite_Select_Material_Equip", self, "OnSelectMaterial")
end

--@region UIEvent

function LuaEquipCompositePane:OnComfirmBtnClick()
	if CLuaEquipMgr.m_MainEquipItemId and CLuaEquipMgr.m_MainEquipItemId ~= "" and
		CLuaEquipMgr.m_MaterialEquipItemId and CLuaEquipMgr.m_MaterialEquipItemId ~= "" then
		
		CUIManager.ShowUI(CLuaUIResources.EquipCompositeWnd)
	else
		g_MessageMgr:ShowMessage("Equip_Composite_Not_Choose_Equip")
	end
end

function LuaEquipCompositePane:OnTipButtonClick()
	g_MessageMgr:ShowMessage("Equip_Composite_Pane_Desc")
end

function LuaEquipCompositePane:OnSelectBtnClick()
	print("OnSelectBtnClick")
	CLuaEquipMgr:SelectMaterialEquip()
end

function LuaEquipCompositePane:OnMatSelectBtnClick()
	CLuaEquipMgr:SelectMaterialEquip()
end


--@endregion UIEvent

