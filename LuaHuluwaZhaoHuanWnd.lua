require("3rdParty/ScriptEvent")
require("common/common_include")

local SoundManager =  import "SoundManager"
local HuluBrothers_Setting = import "L10.Game.HuluBrothers_Setting"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CHuluBrothersZhaoHuanData = import "L10.UI.CHuluBrothersZhaoHuanData"
local Mathf = import "UnityEngine.Mathf"
local EUIModuleGroup = import "L10.UI.CUIManager+EUIModuleGroup"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Vector3 = import "UnityEngine.Vector3"
local CClientObject = import "L10.Game.CClientObject"
local CCCChatMgr = import "L10.Game.CCCChatMgr"
local EnumTeamCCSettingType = import "L10.Game.EnumTeamCCSettingType"

CLuaHuluwaZhaoHuanWnd=class()

RegistClassMember(CLuaHuluwaZhaoHuanWnd, "m_LyricCtrl")
RegistClassMember(CLuaHuluwaZhaoHuanWnd, "m_DropCtrl")
RegistClassMember(CLuaHuluwaZhaoHuanWnd, "m_ProgressBar")
RegistClassMember(CLuaHuluwaZhaoHuanWnd, "m_SoundEvent")


local excepts = {"ThumbnailChatWindow", "HeadInfoWnd", "PopupNoticeWnd"}
function CLuaHuluwaZhaoHuanWnd:Init()
	local List_String = MakeGenericClass(List,cs_string)
    CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
    CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)
	self:UpdateProgress()
	self.m_LyricCtrl:InitLyrics(CHuluBrothersZhaoHuanData.Inst.SingOrderIds)
	self:SetTeamMembersVisble(false)
	
	local targetNpc =  CClientObjectMgr.Inst:GetObject(CHuluBrothersZhaoHuanData.Inst.NpcEngineId)
	if targetNpc then
		--增加标记主角的自身特效
		CameraFollow.Inst:SetFollowObj(targetNpc.RO, nil)
	end
	CameraFollow.Inst:EnableZoom(false)
	
	--停掉背景音乐，播放音乐
	SoundManager.Inst:StopBGMusic()
	if self.m_SoundEvent then
		SoundManager.Inst:StopSound(self.m_SoundEvent)
		self.m_SoundEvent = nil
	end
	local passedTime = CServerTimeMgr.Inst.timeStamp - CHuluBrothersZhaoHuanData.Inst.CreateTime
	passedTime = passedTime * 1000
	local musicId = HuluBrothers_Setting.GetData().MusicID
	self.m_SoundEvent = SoundManager.Inst:PlaySound(musicId, Vector3.zero, 1, nil, passedTime)
end

function CLuaHuluwaZhaoHuanWnd:OnDestroy()
	local List_String = MakeGenericClass(List,cs_string)
    CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
    CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)
    self:SetTeamMembersVisble(true)
    if CameraFollow.Inst then
		CameraFollow.Inst:EnableZoom(true)
		CameraFollow.Inst:SetFollowObj(CClientMainPlayer.Inst.RO,nil)
	end
	if SoundManager.Inst then
		SoundManager.Inst:StartBGMusic()
		if self.m_SoundEvent then
			SoundManager.Inst:StopSound(self.m_SoundEvent)
			self.m_SoundEvent = nil
		end
	end
	--麦克风恢复设置改为在服务器进行了
end

function CLuaHuluwaZhaoHuanWnd:UpdateProgress()
	local passedTime = CServerTimeMgr.Inst.timeStamp - CHuluBrothersZhaoHuanData.Inst.CreateTime
	local duration = CHuluBrothersZhaoHuanData.Inst.EndTime - CHuluBrothersZhaoHuanData.Inst.CreateTime
	local progress = Mathf.Clamp01(passedTime / duration)
	self.m_ProgressBar:SetProgress(progress)
end

function CLuaHuluwaZhaoHuanWnd:SetTeamMembersVisble(visible)
	if CClientMainPlayer.Inst and CTeamMgr.Inst:IsPlayerInTeam(CClientMainPlayer.Inst.Id) then
		CClientMainPlayer.Inst.Visible = visible
		if  visible and CClientObject.s_UseDelayUpdateAppearance then 
			CClientMainPlayer.Inst:RefreshAppearance()
		end
		local members = CTeamMgr.Inst:GetTeamMembersExceptMe()
		for i = 0, members.Count - 1 do
			local player = CClientPlayerMgr.Inst:GetPlayer(members[i].m_MemberId)
			if player then
				player.Visible = visible
				if  visible and CClientObject.s_UseDelayUpdateAppearance then 
                	player:RefreshAppearance()
				end
			end
		end
	end
end

function CLuaHuluwaZhaoHuanWnd:Update()
	self:UpdateProgress()
end

function CLuaHuluwaZhaoHuanWnd:OnEnable()	
	g_ScriptEvent:AddListener("SyncHuLuDrop", self, "OnSyncHuLuDrop")
end

function CLuaHuluwaZhaoHuanWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncHuLuDrop", self, "OnSyncHuLuDrop")
end

function CLuaHuluwaZhaoHuanWnd:OnSyncHuLuDrop(args)
	local dropTimes = args[0]
	local dropIds = args[1] 
	local dropCounts = args[2]
	self.m_DropCtrl:OnSyncHuLuDrop(dropTimes, dropIds, dropCounts)
end

return CLuaHuluwaZhaoHuanWnd
