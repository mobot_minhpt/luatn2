local CJuQingDialogMgr=import "L10.UI.CJuQingDialogMgr"
local EnumContentType = import "L10.UI.CJuQingDialogMgr+EnumContentType"

CLuaJuQingDialogMgr = {}
CLuaJuQingDialogMgr.m_EngineId = 0 
CLuaJuQingDialogMgr.m_ClientDialogCallback = nil
function CLuaJuQingDialogMgr.Init(engineId,clientCallback)
    CLuaJuQingDialogMgr.m_EngineId = engineId
    CLuaJuQingDialogMgr.m_ClientDialogCallback = clientCallback
end
--todo 将C#的逻辑删掉

--客户端的对话框逻辑，需要传callback
function CLuaJuQingDialogMgr.ShowClientDialog(dialogId,callback)
    CLuaJuQingDialogMgr.Init(0,callback)
    
    local dialog = Dialog_TaskDialog.GetData(dialogId)
    if dialog then
        CJuQingDialogMgr.Inst.contentType = EnumContentType.Independent
        CJuQingDialogMgr.Inst.TargetNPCId = 0
        CJuQingDialogMgr.Inst.AIDialogId = dialogId
        CJuQingDialogMgr.Inst.Content = dialog.Dialog
    
        CUIManager.ShowUI(CUIResources.JuQingDialogWnd)
    end
end



function CLuaJuQingDialogMgr.ShowDialog(engineId,dialogId,npcId,content)
    -- print("ShowDialog",engineId,dialogId,npcId,content)
    CLuaJuQingDialogMgr.Init(engineId)

    CJuQingDialogMgr.Inst.contentType = EnumContentType.Independent
    CJuQingDialogMgr.Inst.TargetNPCId = npcId
    CJuQingDialogMgr.Inst.AIDialogId = dialogId
    CJuQingDialogMgr.Inst.Content = content

    CUIManager.ShowUI(CUIResources.JuQingDialogWnd)
end


function CLuaJuQingDialogMgr.ShowSerialDialog(content)
    -- print("ShowSerialDialog",content)
    CLuaJuQingDialogMgr.Init(0)

    if CJuQingDialogMgr.Inst.contentType ~= EnumContentType.Serial then
        CJuQingDialogMgr.Inst.Content = ""
    end

    CJuQingDialogMgr.Inst.contentType = EnumContentType.Serial
    CJuQingDialogMgr.Inst.TargetNPCId = 0
    CJuQingDialogMgr.Inst.AIDialogId = 0

    CJuQingDialogMgr.Inst.Content = CJuQingDialogMgr.Inst.Content..content

    CUIManager.ShowUI(CUIResources.JuQingDialogWnd)
end

