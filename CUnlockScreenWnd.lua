-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuaJiMgr = import "L10.Game.CGuaJiMgr"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CommonDefs = import "L10.Game.CommonDefs"
local CPerformanceMgr = import "L10.Engine.CPerformanceMgr"
local CStatusMgr = import "L10.Game.CStatusMgr"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUnlockScreenMgr = import "L10.UI.CUnlockScreenMgr"
local CUnlockScreenWnd = import "L10.UI.CUnlockScreenWnd"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EPropStatus = import "L10.Game.EPropStatus"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local LimitRpcStatusDefine = import "L10.Game.LimitRpcStatusDefine"
local OnDragFinished = import "UIProgressBar+OnDragFinished"
local PlayerSettings = import "L10.Game.PlayerSettings"
local TweenAlpha = import "TweenAlpha"
CUnlockScreenWnd.m_Init_CS2LuaHook = function (this) 
    if not this.inited then
        this.m_EnableSavePowerMode = CPerformanceMgr.Inst.EnableSavePowerMode
        --保留原始省点模式信息，为了避免切场景重复调用Init，这里限制只初始化一次
        this.inited = true
    end
    CPerformanceMgr.Inst.EnableSavePowerMode = true
    local ta = CommonDefs.GetComponent_Component_Type(this.m_EffectSprite, typeof(TweenAlpha))
    if ta ~= nil then
        this.m_EffectDuration = math.floor((ta.duration * 1000))
    else
        this.m_EffectDuration = 1000
    end
    this:CancelCloseWndTick()

    this:DisplayLimitRpcHint()
    this:DisplaySwitchButton()
end
CUnlockScreenWnd.m_DisplayLimitRpcHint_CS2LuaHook = function (this) 
    if CUnlockScreenMgr.Inst.IsLimitRpcMode and CClientMainPlayer.Inst ~= nil then
        if CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("GuaJi")) ~= 0 and not CGuaJiMgr.Inst.IsInterrupting then
            this.hintLabel.text = g_MessageMgr:FormatMessage("SaveFlowMode_Fight_Tips")
        else
            this.hintLabel.text = g_MessageMgr:FormatMessage("SaveFlowMode_NoFight_Tips")
        end
    else
        this.hintLabel.text = ""
    end
end
CUnlockScreenWnd.m_OnEnable_CS2LuaHook = function (this) 
    this.m_UnlockSlider.onDragFinished = CommonDefs.CombineListner_OnDragFinished(this.m_UnlockSlider.onDragFinished, MakeDelegateFromCSFunction(this.OnDragFinished, OnDragFinished, this), true)
    EventManager.AddListener(EnumEventType.Disconnect, MakeDelegateFromCSFunction(this.OnDisconnect, Action0, this))
    EventManager.AddListener(EnumEventType.GuaJiStatusChanged, MakeDelegateFromCSFunction(this.OnGuaJiStatusChanged, Action0, this))
    EventManager.AddListener(EnumEventType.OnMainPlayerLimitRPCStatusChanged, MakeDelegateFromCSFunction(this.OnMainPlayerLimitRPCStatusChanged, Action0, this))

    this.m_TweenAlpha = CommonDefs.GetComponent_Component_Type(this, typeof(TweenAlpha))
    if this.m_TweenAlpha ~= nil then
        this.m_TweenAlpha.enabled = false
    end
end
CUnlockScreenWnd.m_OnDisable_CS2LuaHook = function (this) 
    this.m_UnlockSlider.onDragFinished = CommonDefs.CombineListner_OnDragFinished(this.m_UnlockSlider.onDragFinished, MakeDelegateFromCSFunction(this.OnDragFinished, OnDragFinished, this), false)
    EventManager.RemoveListener(EnumEventType.Disconnect, MakeDelegateFromCSFunction(this.OnDisconnect, Action0, this))
    EventManager.RemoveListener(EnumEventType.GuaJiStatusChanged, MakeDelegateFromCSFunction(this.OnGuaJiStatusChanged, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnMainPlayerLimitRPCStatusChanged, MakeDelegateFromCSFunction(this.OnMainPlayerLimitRPCStatusChanged, Action0, this))
    this:CancelCloseWndTick()

    CUnlockScreenMgr.Inst:CancelRpcLimit()
end
CUnlockScreenWnd.m_CancelCloseWndTick_CS2LuaHook = function (this) 
    if this.m_CloseWndTick ~= nil then
        invoke(this.m_CloseWndTick)
        this.m_CloseWndTick = nil
    end
end
CUnlockScreenWnd.m_OnDragFinished_CS2LuaHook = function (this) 
    if this.m_UnlockSlider.value < CUnlockScreenWnd.DELTA then
        this.m_UnlockSlider.value = 0
        this.m_UnlockLabel.gameObject:SetActive(true)
    else
        this.m_EffectSprite.gameObject:SetActive(true)
        if this.m_TweenAlpha ~= nil then
            this.m_TweenAlpha.enabled = true
        end
        if this.m_CloseWndTick ~= nil then
            invoke(this.m_CloseWndTick)
        end
        this.m_CloseWndTick = CTickMgr.Register(DelegateFactory.Action(function () 
            CPerformanceMgr.Inst.EnableSavePowerMode = this.m_EnableSavePowerMode
            CUnlockScreenMgr.Inst:CancelRpcLimit()
            this:Close()
        end), this.m_EffectDuration + 30, ETickType.Once)
    end
end
CUnlockScreenWnd.m_OnLimitRPCSwitchButtonClick_CS2LuaHook = function (this, go) 
    if this.switchBtn.Selected then
        --转为非省流量模式
        this.switchBtn.Selected = false
        --先修改状态，待服务器返回结果时刷新
        CUnlockScreenMgr.Inst:CancelRpcLimit()
    else
        --转为省流量模式
        if CUnlockScreenMgr.Inst.EnableOpenLimitRPC then
            this.switchBtn.Selected = true
            --先修改状态，待服务器返回结果时刷新
            CUnlockScreenMgr.Inst:OpenRpcLimit()
        else
            g_MessageMgr:ShowMessage("PC_Not_Support_Limit_RPC")
        end
    end
end

CUnlockScreenMgr.m_ShowUnlockScreenWnd_CS2LuaHook = function (this) 
    if CSwitchMgr.EnableLimitRpc and this.EnableOpenLimitRPC then
        if CClientMainPlayer.Inst == nil then
            return
        end
        if this.IsLimitRpcMode or not PlayerSettings.ClientLimitRPC then
            this:DoShowUnlockScreenWnd()
        else
            Gac2Gas.LimitRpc_ClientRequestOperation(CClientMainPlayer.Inst.Id, EnumPlayerLimitRpcOperationCode_lua.eEnter, "")
        end
    else
        this:DoShowUnlockScreenWnd()
    end
end
CUnlockScreenMgr.m_SetUnlockScreenWndByRpcLimitStatus_CS2LuaHook = function (this) 
    if CSwitchMgr.EnableLimitRpc then
        if CClientMainPlayer.Inst == nil then
            return
        end
        if CClientMainPlayer.Inst.BasicProp.LimitRpcStatus == LimitRpcStatusDefine.NORMAL then
            --CUIManager.CloseUI(CIndirectUIResources.UnlockScreenWnd);
        elseif not CUIManager.IsLoaded(CIndirectUIResources.UnlockScreenWnd) then
            this:DoShowUnlockScreenWnd()
        end
        EventManager.Broadcast(EnumEventType.OnMainPlayerLimitRPCStatusChanged)
    end
end
CUnlockScreenMgr.m_CancelRpcLimit_CS2LuaHook = function (this) 
    if CSwitchMgr.EnableLimitRpc then
        if CClientMainPlayer.Inst ~= nil then
            Gac2Gas.LimitRpc_ClientRequestOperation(CClientMainPlayer.Inst.Id, EnumPlayerLimitRpcOperationCode_lua.eLeave, "")
        end
    end
end
CUnlockScreenMgr.m_OpenRpcLimit_CS2LuaHook = function (this) 
    if CSwitchMgr.EnableLimitRpc and this.EnableOpenLimitRPC then
        if not this.IsLimitRpcMode then
            Gac2Gas.LimitRpc_ClientRequestOperation(CClientMainPlayer.Inst.Id, EnumPlayerLimitRpcOperationCode_lua.eEnter, "")
        end
    end
end
