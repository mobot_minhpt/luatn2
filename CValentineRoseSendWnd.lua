-- Auto Generated!!
local AlignType = import "CPlayerInfoMgr+AlignType"
local CButton = import "L10.UI.CButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CValentineMgr = import "L10.UI.CValentineMgr"
local CValentineRoseSendWnd = import "L10.UI.CValentineRoseSendWnd"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local Gac2Gas = import "L10.Game.Gac2Gas"
local L10 = import "L10"
local LocalString = import "LocalString"
local Vector3 = import "UnityEngine.Vector3"
CValentineRoseSendWnd.m_Init_CS2LuaHook = function (this) 
    if CValentineMgr.Inst.SendRose then
        if CClientMainPlayer.Inst == nil then
            this:Close()
            return
        end
        this.m_SenderName.text = CClientMainPlayer.Inst.Name
        this.m_Input.characterLimit = CValentineRoseSendWnd.SEND_ROSE_CHAR_LIMIT
        this.m_Input.enabled = true
        this.m_ApplyButton.Text = LocalString.GetString("种玫瑰")
    else
        this.m_Input.enabled = false
        this.m_ApplyButton.Text = LocalString.GetString("领取玫瑰")
        this.m_AddButton:SetActive(false)
        this.m_SenderPlayerId = CValentineMgr.Inst.SendIndfo.SenderId

        this.m_ReveiverName.text = CClientMainPlayer.Inst.Name
        local senderInfo = CValentineMgr.Inst.SendIndfo
        local portraitName = L10.UI.CUICommonDef.GetPortraitName(senderInfo.SenderClass, senderInfo.SenderGender, -1)
        this.m_Portrait:LoadNPCPortrait(portraitName, false)
        this.m_SenderName.text = senderInfo.SenderName
        this.m_Input.value = senderInfo.SendMsg
    end
end
CValentineRoseSendWnd.m_OnApplyButtonClicked_CS2LuaHook = function (this, go) 

    if CValentineMgr.Inst.SendRose then
        -- 接收方不能为空
        if this.m_ReceiverPlayerId == 0 then
            g_MessageMgr:ShowMessage("ROSE_RECIEVER_IS_EMPTY")
            return
        end

        -- 赠言不能为空
        if System.String.IsNullOrEmpty(this.m_Input.value) then
            g_MessageMgr:ShowMessage("ROSE_CONTENT_IS_EMPTY")
            return
        end

        if CommonDefs.StringLength(this.m_Input.value) > CValentineRoseSendWnd.SEND_ROSE_CHAR_LIMIT then
            g_MessageMgr:ShowMessage("ROSE_CONTENT_IS_TOO_LONG")
            return
        end

        -- 过滤
        local ret = CWordFilterMgr.Inst:DoFilterOnSend(this.m_Input.value, nil, nil, true)
        local msg = ret.msg
        if msg == nil then
            return
        end

        CommonDefs.GetComponent_Component_Type(this.m_ApplyButton, typeof(CButton)).Enabled = false
        Gac2Gas.RequestUseMysticRoseSeed(CValentineMgr.Inst.RoseSeedPlace, CValentineMgr.Inst.RoseSeedPos, CValentineMgr.Inst.RoseSeedItemId, this.m_ReceiverPlayerId, msg)
        this:Close()
    else
        Gac2Gas.RequestCollectMysticRose(CValentineMgr.Inst.CollectNPCEngineId)
        this:Close()
    end
end
CValentineRoseSendWnd.m_OnPortraitClicked_CS2LuaHook = function (this, go) 
    if CValentineMgr.Inst.SendRose then
        CCommonPlayerListMgr.Inst:ShowPlayerToSendRose()
    else
        CPlayerInfoMgr.ShowPlayerPopupMenu(math.floor(CValentineMgr.Inst.SendIndfo.SenderId), EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
    end
end
CValentineRoseSendWnd.m_OnZhongQiuSelectPlayer_CS2LuaHook = function (this, playerId, playerName, portraitName) 
    this.m_ReveiverName.text = playerName
    this.m_ReceiverPlayerId = playerId

    this.m_AddButton:SetActive(false)
    this.m_Portrait:LoadNPCPortrait(portraitName, false)
end
