local UIGrid = import "UIGrid"
local QnButton = import "L10.UI.QnButton"
local UIVariableScrollView = import "L10.UI.UIVariableScrollView"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CommonDefs = import "L10.Game.CommonDefs"
local CItemMgr = import "L10.Game.CItemMgr"
local DelegateFactory  = import "DelegateFactory"

LuaShenYaoExclusiveWnd = class()
LuaShenYaoExclusiveWnd.s_ItemId = nil
LuaShenYaoExclusiveWnd.s_Info = nil

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShenYaoExclusiveWnd, "OkButton", "OkButton", QnButton)
RegistChildComponent(LuaShenYaoExclusiveWnd, "CancelButton", "CancelButton", QnButton)
RegistChildComponent(LuaShenYaoExclusiveWnd, "Panel", "Panel", GameObject)
RegistChildComponent(LuaShenYaoExclusiveWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaShenYaoExclusiveWnd, "SelectedBg", "SelectedBg", GameObject)
RegistChildComponent(LuaShenYaoExclusiveWnd, "InfoTemplate", "InfoTemplate", GameObject)
RegistChildComponent(LuaShenYaoExclusiveWnd, "InfoGrid", "InfoGrid", UIGrid)
RegistChildComponent(LuaShenYaoExclusiveWnd, "Grid", "Grid", UIGrid)

--@endregion RegistChildComponent end

RegistClassMember(LuaShenYaoExclusiveWnd, "m_MaxLevel")
RegistClassMember(LuaShenYaoExclusiveWnd, "m_CenteringTick")
RegistClassMember(LuaShenYaoExclusiveWnd, "m_ScrollView")
RegistClassMember(LuaShenYaoExclusiveWnd, "m_CurLevel")
RegistClassMember(LuaShenYaoExclusiveWnd, "m_Idx2Level")
RegistClassMember(LuaShenYaoExclusiveWnd, "m_Idx2NpcId")
RegistClassMember(LuaShenYaoExclusiveWnd, "m_RandomPlay")

function LuaShenYaoExclusiveWnd:Awake()
    if CommonDefs.IS_VN_CLIENT then
        local jieLabel = self.transform:Find("Anchor/ScrollView/Label")
        jieLabel.transform.localPosition = Vector3(-90, 115.9, 0)
    end
    
    self.m_MaxLevel = 0
    XinBaiLianDong_ShenYaoNpc.Foreach(function(k, v)
        self.m_MaxLevel = math.max(self.m_MaxLevel, k)
    end)

    self.Template:SetActive(false)
    self.InfoTemplate:SetActive(false)


    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)


	
	UIEventListener.Get(self.CancelButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelButtonClick()
	end)


    --@endregion EventBind end

    self.m_ScrollView = self.Panel:GetComponent(typeof(UIScrollView))
    self.m_ScrollView.onDragFinished = DelegateFactory.OnDragNotification(function(go)
        self:Centering()
    end)
    self.m_ScrollView.onMomentumMove = DelegateFactory.OnDragNotification(function(go)
        self:Centering()
    end)

    self.m_Idx2NpcId = {}
    local i = 1
    local data = XinBaiLianDong_ShenYaoNpc.GetData(i)
    while data do
        self.m_Idx2NpcId[i] = {}
        self.m_Idx2NpcId[i].len = 0
        for npcId, playId in string.gmatch(data.RandPool, "(%d+),(%d+);?") do
            npcId, playId = tonumber(npcId), tonumber(playId)
            self.m_Idx2NpcId[i][self.m_Idx2NpcId[i].len] = npcId
            self.m_Idx2NpcId[i].len = self.m_Idx2NpcId[i].len + 1
		end
        i = i + 1
        data = XinBaiLianDong_ShenYaoNpc.GetData(i)
    end
end

function LuaShenYaoExclusiveWnd:Init()
    self.m_Idx2Level = {}
    Gac2Gas.ShenYao_QueryUnlockLevel()

    Extensions.RemoveAllChildren(self.InfoGrid.transform)
    if LuaShenYaoExclusiveWnd.s_Info then
        for _, p in ipairs(LuaShenYaoExclusiveWnd.s_Info) do
            local trans = NGUITools.AddChild(self.InfoGrid.gameObject, self.InfoTemplate).transform
            trans.gameObject:SetActive(true)
            trans:Find("Leader").gameObject:SetActive(p.IsLeader)
            trans:Find("Name"):GetComponent(typeof(UILabel)).text = p.Name
            local rewardLabel = trans:Find("RewardTimes"):GetComponent(typeof(UILabel))
            rewardLabel.text = p.RewardTimes
            if p.RewardTimes == 0 then rewardLabel.color = NGUIText.ParseColor24("ff0000", 0) end
            local joinLabel = trans:Find("JoinTimes"):GetComponent(typeof(UILabel))
            joinLabel.text = p.JoinTimes
            if p.JoinTimes == 0 then joinLabel.color = NGUIText.ParseColor24("ff0000", 0) end
        end
    end
    self.InfoGrid:Reposition()
end

function LuaShenYaoExclusiveWnd:OnEnable()
    g_ScriptEvent:AddListener("ShenYao_QueryUnlockLevelResult", self, "ShenYao_QueryUnlockLevelResult")
end

function LuaShenYaoExclusiveWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ShenYao_QueryUnlockLevelResult", self, "ShenYao_QueryUnlockLevelResult")
end

function LuaShenYaoExclusiveWnd:OnDestroy()
    --self:CancelCenteringTick()
end

--@region UIEvent

function LuaShenYaoExclusiveWnd:OnOkButtonClick()
    if not LuaShenYaoExclusiveWnd.s_ItemId or not self.m_CurLevel then return end
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("SHENYAO_PERSONAL_TIPS", self.m_CurLevel..LocalString.GetString("阶")), function()
        -- 多选一的情况会返还一个道具用于召唤同时发送Gas2Gac.ShenYao_OpenSelectNpcWnd(string: itemId)
        -- 参数npcId=0表示让服务器随机选一个; 目前还没有指定NPC,即RandomPlay=0的情况
        Gac2Gas.ShenYao_CreatePersonalNpc(LuaShenYaoExclusiveWnd.s_ItemId, self.m_CurLevel, 0)--self.m_RandomPlay > 0 and 0 or self.m_Idx2NpcId[self.m_CurLevel][math.random(self.m_Idx2NpcId[self.m_CurLevel].len) - 1])
        LuaShenYaoExclusiveWnd.s_ItemId = nil
        CUIManager.CloseUI(CLuaUIResources.ShenYaoExclusiveWnd)
    end, nil, nil, nil, false)
end

function LuaShenYaoExclusiveWnd:OnCancelButtonClick()
    CUIManager.CloseUI(CLuaUIResources.ShenYaoExclusiveWnd)
end


--@endregion UIEvent

function LuaShenYaoExclusiveWnd:ShenYao_QueryUnlockLevelResult(lv)
    Extensions.RemoveAllChildren(self.Grid.transform)
    local data = XinBaiLianDong_ShenYaoTicket.GetData(LuaShenYaoExclusiveWnd.s_ItemId or 1)
    if data then
        self.m_RandomPlay = data.RandomPlay
        if data.StaticLevel then
            for i = 0, data.StaticLevel.Length - 1 do
                local obj = NGUITools.AddChild(self.Grid.gameObject, self.Template)
                obj:SetActive(true)
                obj:GetComponent(typeof(UILabel)).text = data.StaticLevel[i]
                self.m_Idx2Level[i] = data.StaticLevel[i]
            end
        elseif data.UpLevel and data.DownLevel then
            local idx = 0
            for i = math.max(1, lv - data.DownLevel), math.min(self.m_MaxLevel, lv + data.UpLevel) do
                local obj = NGUITools.AddChild(self.Grid.gameObject, self.Template)
                obj:SetActive(true)
                obj:GetComponent(typeof(UILabel)).text = i
                self.m_Idx2Level[idx] = i
                idx = idx + 1
            end
        end
    end
    self.Grid:Reposition()
    self.m_ScrollView:MoveRelative(Vector3(0, 114514, 0))
    self:Centering()

    --self:CancelCenteringTick()
    --self.m_CenteringTick = RegisterTick(function()
    --    self:Centering()
    --end, 500)
end

--function LuaShenYaoExclusiveWnd:CancelCenteringTick()
--    if self.m_CenteringTick then
--        invoke(self.m_CenteringTick)
--        self.m_CenteringTick = nil
--    end
--end

function LuaShenYaoExclusiveWnd:Centering()
    local minDis = 114514
    local signedDis = 0
    local selectedPos = self.Panel.transform:InverseTransformPoint(self.SelectedBg.transform.position) 
    for i = 0, self.Grid.transform.childCount - 1 do
        local trans = self.Grid.transform:GetChild(i)
        local pos = self.Panel.transform:InverseTransformPoint(trans.position)
        local dis = math.abs(selectedPos.y - pos.y)
        if dis < minDis then
            minDis = dis
            signedDis = selectedPos.y - pos.y
            self.m_CurLevel = self.m_Idx2Level[i]
        end
    end
    if minDis < 114514 then
        self.m_ScrollView:MoveRelative(Vector3(0, signedDis, 0))
    end
end
