local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local ClientAction = import "L10.UI.ClientAction"
local Animation = import "UnityEngine.Animation"
local UITableTween = import "L10.UI.UITableTween"
local TouchPhase = import "UnityEngine.TouchPhase"
local CTrackMgr = import "L10.Game.CTrackMgr"

LuaWuYi2023JZLYWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuYi2023JZLYWnd, "MainView", GameObject)
RegistChildComponent(LuaWuYi2023JZLYWnd, "TaskView", GameObject)
RegistChildComponent(LuaWuYi2023JZLYWnd, "CloseButton", GameObject)
RegistChildComponent(LuaWuYi2023JZLYWnd, "DragItem", GameObject)
RegistChildComponent(LuaWuYi2023JZLYWnd, "MianShiWenShu", GameObject)
RegistChildComponent(LuaWuYi2023JZLYWnd, "JinBoShuYin", GameObject)
RegistChildComponent(LuaWuYi2023JZLYWnd, "YinBoShuYin", GameObject)
RegistClassMember(LuaWuYi2023JZLYWnd, "m_MianShiWenShuItemId")
RegistClassMember(LuaWuYi2023JZLYWnd, "m_MianShiWenShuIcon")
RegistClassMember(LuaWuYi2023JZLYWnd, "m_MianShiWenShuCount")
RegistClassMember(LuaWuYi2023JZLYWnd, "m_UseMianShiWenShuMsg")
RegistClassMember(LuaWuYi2023JZLYWnd, "m_Animation")

-- 主界面
RegistChildComponent(LuaWuYi2023JZLYWnd, "LiuYi", GameObject)
RegistChildComponent(LuaWuYi2023JZLYWnd, "KaoYanReward", CQnReturnAwardTemplate)
RegistChildComponent(LuaWuYi2023JZLYWnd, "LiuYiReward", CQnReturnAwardTemplate)
RegistClassMember(LuaWuYi2023JZLYWnd, "m_LiuYiInfoTbl")
RegistClassMember(LuaWuYi2023JZLYWnd, "m_LiuYi2TaskTbl")
RegistClassMember(LuaWuYi2023JZLYWnd, "m_Task2LiuYiTbl")
RegistClassMember(LuaWuYi2023JZLYWnd, "m_WenShuRemain")

-- 任务界面
RegistChildComponent(LuaWuYi2023JZLYWnd, "Page", GameObject)
RegistChildComponent(LuaWuYi2023JZLYWnd, "NextArrow", GameObject)
RegistChildComponent(LuaWuYi2023JZLYWnd, "LastArrow", GameObject)
RegistChildComponent(LuaWuYi2023JZLYWnd, "TaskTabel", UITable)
RegistChildComponent(LuaWuYi2023JZLYWnd, "TaskTemplate", GameObject)
RegistClassMember(LuaWuYi2023JZLYWnd, "m_SelectLiuYiIndex")
RegistClassMember(LuaWuYi2023JZLYWnd, "m_PageTbl")
RegistClassMember(LuaWuYi2023JZLYWnd, "m_TaskTbl")
RegistClassMember(LuaWuYi2023JZLYWnd, "m_GoToTaskIdTbl")
RegistClassMember(LuaWuYi2023JZLYWnd, "m_TaskTabelTween")
RegistClassMember(LuaWuYi2023JZLYWnd, "m_isTaskView")

--@endregion RegistChildComponent end

function LuaWuYi2023JZLYWnd:Awake()
    self.m_Animation = self.transform:GetComponent(typeof(Animation))
    self.m_TaskTabelTween = self.TaskTabel.transform:GetComponent(typeof(UITableTween))
    UIEventListener.Get(self.CloseButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)
end

function LuaWuYi2023JZLYWnd:Init()
    self.m_Animation:Play("wuyi2023jzlywnd_show")
	if CUIManager.IsLoaded(CLuaUIResources.WuYi2023XXSCMainWnd) then
        RegisterTickOnce(function ()
            CUIManager.CloseUI(CLuaUIResources.WuYi2023XXSCMainWnd)
        end, 200)
    end
    self.MainView:SetActive(true)
    self.TaskView:SetActive(false)
    self:InitLeftView()
    self:InitMainView()
    self:InitTaskView()
    self:SetMode(false)
    Gac2Gas.XinXiangShiChengQueryCoinAmount()
    Gac2Gas.JZLYQueryStampProgress()
end

-- 左侧界面
function LuaWuYi2023JZLYWnd:InitLeftView()
    -- 免试文书
    self.DragItem:SetActive(false)
    local data = WuYi2023_JunZiLiuYi.GetData()
    self.m_UseMianShiWenShuMsg = data.UseMianShiWenShuMsg
    self.m_MianShiWenShuItemId = data.MianShiWenShuItemId
    self.m_MianShiWenShuIcon = self.MianShiWenShu.transform:Find("Icon").gameObject
    UIEventListener.Get(self.m_MianShiWenShuIcon).onDragStart = DelegateFactory.VoidDelegate(function (go)
        self:OnDragStart()
    end)
    self:RefreshItem()
    -- 金箔书印
    local jinBoOpenBtn = self.JinBoShuYin.transform:Find("OpenBtn")
    UIEventListener.Get(jinBoOpenBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnOpenBtnClick(0)
    end)
    self:UpdateIconCount(self.JinBoShuYin, 0, false)
    -- 铜箔书印
    local yinBoOpenBtn = self.YinBoShuYin.transform:Find("OpenBtn")
    UIEventListener.Get(yinBoOpenBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnOpenBtnClick(1)
    end)
    self:UpdateIconCount(self.YinBoShuYin, 0, false)
end

function LuaWuYi2023JZLYWnd:UpdateIconCount(item, count, isMianShiWenShu)
    local countLabel = item.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
    countLabel.text = tostring(count)

    if isMianShiWenShu then
        local descLabel = item.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
        if not self.m_WenShuRemain and count == 0 then
            descLabel.text = LocalString.GetString("已使用")
        else
            descLabel.text = count == 0 and LocalString.GetString("可从节日签到获取") or LocalString.GetString("拖拽至未完成任务上使用可立即完成该任务")
        end
    end
end

function LuaWuYi2023JZLYWnd:Update()
    if self.m_LastDragPos == nil then self.m_LastDragPos = Vector3.zero end
    if not self.m_IsDragging then return end
    if CommonDefs.IsInMobileDevice() then
        for i = 0,Input.touchCount - 1 do
            local touch = Input.GetTouch(i)
            if touch.fingerId == self.m_FingerIndex then
                if touch.phase ~= TouchPhase.Ended and touch.phase ~= TouchPhase.Canceled then
                    if touch.phase ~= TouchPhase.Stationary then
                        local pos = touch.position
                        self.m_LastDragPos = Vector3(pos.x, pos.y, 0)
                        self.DragItem.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_LastDragPos)
                    end
                    return
                else
                    break
                end
            end
        end
    else
        if Input.GetMouseButton(0) then
            self.DragItem.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
            self.m_LastDragPos = Input.mousePosition
            if not Input.GetMouseButtonUp(0) then
                return
            end
        end
    end
    self.DragItem.gameObject:SetActive(false)
    local hoveredObject = CUICommonDef.SelectedUIWithRacast
    if hoveredObject then
        self:OnDragEnd(hoveredObject)
    end
    self.m_IsDragging = false
end

function LuaWuYi2023JZLYWnd:OnDragStart()
    if not self.m_isTaskView or self.m_MianShiWenShuCount == 0 then return end
    self.m_MianShiWenShuIcon:SetActive(false)
    self.DragItem:SetActive(true)
    self.m_IsDragging = true
    self.m_LastDragPos = Input.mousePosition
    if CommonDefs.IsInMobileDevice() then
        self.m_FingerIndex = Input.GetTouch(0).fingerId
        self.m_LastDragPos = Input.GetTouch(0).position
    end
end

function LuaWuYi2023JZLYWnd:OnDragEnd(hoveredObject)
    self.m_MianShiWenShuIcon:SetActive(true)
    if self.m_GoToTaskIdTbl[hoveredObject] then
        local taskId = self.m_GoToTaskIdTbl[hoveredObject]
        local data = WuYi2023_JunZiLiuYiTask.GetData(taskId)
        local str = g_MessageMgr:FormatMessage(self.m_UseMianShiWenShuMsg, data.Description)
        local time = self.m_LiuYi2TaskTbl[data.Location][taskId].time
        if time < data.NeedTimes then
            g_MessageMgr:ShowOkCancelMessage(str, function()
                Gac2Gas.JZLYRequestUseFreeStamp(taskId)
            end, nil, nil, nil, false)
        else
            g_MessageMgr:ShowMessage("JZLY_ALREADY_FULL")
        end
    end
end

-- 右侧主界面
function LuaWuYi2023JZLYWnd:InitLiuYiTaskTime()
    self.m_LiuYi2TaskTbl = {}
    self.m_Task2LiuYiTbl = {}
    WuYi2023_JunZiLiuYiTask.Foreach(function(k, v)
        local liuyi = tonumber(v.Location)
        if self.m_LiuYi2TaskTbl[liuyi] == nil then
            self.m_LiuYi2TaskTbl[liuyi] = {}
        end
        self.m_LiuYi2TaskTbl[liuyi][k] = {}
        self.m_LiuYi2TaskTbl[liuyi][k].time = 0
        self.m_LiuYi2TaskTbl[liuyi][k].needTime = v.NeedTimes
        self.m_Task2LiuYiTbl[k] = liuyi
    end)
    self:UpdateLiuYiCount()
end

function LuaWuYi2023JZLYWnd:UpdateLiuYiCount()
    for i = 1, self.LiuYi.transform.childCount do
        local count = 0
        if self.m_LiuYi2TaskTbl[i] then
            for k, v in pairs(self.m_LiuYi2TaskTbl[i]) do
                if v.time >= v.needTime then count = count + 1 end
            end
            self:SetProgress(self.m_LiuYiInfoTbl[i], count, 5, 1)
        end
    end
end

function LuaWuYi2023JZLYWnd:InitMainView()
    -- 奖励
    local data = WuYi2023_JunZiLiuYi.GetData()
    self.KaoYanReward:Init(CItemMgr.Inst:GetItemTemplate(data.KaoYanRewardItemId), 1)
    self.LiuYiReward:Init(CItemMgr.Inst:GetItemTemplate(data.LiuYiRewardItemId), 1)
    -- 六艺进度
    self.m_LiuYiInfoTbl = {}
    for i = 0, self.LiuYi.transform.childCount - 1 do
        local index = i + 1
        local item = self.LiuYi.transform:GetChild(i)
        self.m_LiuYiInfoTbl[index] = {}
        self.m_LiuYiInfoTbl[index].item = item
        self.m_LiuYiInfoTbl[index].progress = item:Find("Progress/Fg"):GetComponent(typeof(UITexture))
        self.m_LiuYiInfoTbl[index].icon = item:Find("Progress/Icon").gameObject
        self.m_LiuYiInfoTbl[index].get = item:Find("Progress/Get").gameObject
        UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self.m_SelectLiuYiIndex = index
            self:ShowLiuYiTask()
        end)
    end
    self:InitLiuYiTaskTime()
end

function LuaWuYi2023JZLYWnd:SetProgress(info, count, fullCount, alpha)
    count = count > fullCount and fullCount or count
    local ratio = count / fullCount
    info.progress.fillAmount = ratio
    info.icon:SetActive(ratio < 1)
    info.get:SetActive(ratio >= 1)
    if info.label then info.label.text = SafeStringFormat3(LocalString.GetString("%d/%d"), count, info.fullCount) end
    local panel = info.item:GetComponent(typeof(UIPanel))
    if panel then panel.alpha = ratio >= 1 and alpha or 1 end
    if info.toDoBtn then info.toDoBtn:SetActive(ratio < 1) end
    if info.finishTip then info.finishTip:SetActive(ratio >= 1) end
end

-- 右侧任务界面
function LuaWuYi2023JZLYWnd:InitTaskView()
    self.TaskTemplate:SetActive(false)
    self.m_PageTbl = {}
    for i = 0, self.Page.transform.childCount - 1 do
        local index = i + 1
        local item = self.Page.transform:GetChild(i)
        self.m_PageTbl[index] = item:GetComponent(typeof(UITexture))
        UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self.m_SelectLiuYiIndex = index
            self:ShowLiuYiTask()
        end)
    end

    UIEventListener.Get(self.NextArrow).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnArrowClick(1)
	end)
    UIEventListener.Get(self.LastArrow).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnArrowClick(-1)
	end)
end

function LuaWuYi2023JZLYWnd:ShowLiuYiTask()
    if not self.m_isTaskView then
        self.m_Animation:Play("wuyi2023jzlywnd_qiehuan")
    end
    self:SetMode(true)
    self.MainView:SetActive(false)
    self.TaskView:SetActive(true)
    for i = 1, #self.m_PageTbl do
        self.m_PageTbl[i].alpha = i == self.m_SelectLiuYiIndex and 1 or 0.7
        self.m_PageTbl[i].transform.localScale = i == self.m_SelectLiuYiIndex and Vector3(1.67, 1.67, 1.67) or Vector3.one
    end
    self.NextArrow:SetActive(self.m_SelectLiuYiIndex ~= #self.m_PageTbl)
    self.LastArrow:SetActive(self.m_SelectLiuYiIndex ~= 1)
    self:InitTaskTable()
end

function LuaWuYi2023JZLYWnd:InitTaskTable()
    Extensions.RemoveAllChildren(self.TaskTabel.transform)
    self.m_TaskTbl = {}
    self.m_GoToTaskIdTbl = {}
    local taskTbl = self.m_LiuYi2TaskTbl[self.m_SelectLiuYiIndex]
    if taskTbl == nil then return end
    local beginIndex = (self.m_SelectLiuYiIndex - 1) * 5
    for i = 1, 5 do
        local k = beginIndex + i
        local value = WuYi2023_JunZiLiuYiTask.GetData(k)
        local go = NGUITools.AddChild(self.TaskTabel.gameObject, self.TaskTemplate)
        go:SetActive(true)
        go.transform:Find("DescLabel"):GetComponent(typeof(UILabel)).text = value.Description
        self.m_TaskTbl[k] = {}
        self.m_TaskTbl[k].item = go
        go.transform:Find("ProgressLabel/Label"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24(value.TextColor,0)
        self.m_TaskTbl[k].label = go.transform:Find("ProgressLabel"):GetComponent(typeof(UILabel))
        self.m_TaskTbl[k].label.color = NGUIText.ParseColor24(value.TextColor,0)
        self.m_TaskTbl[k].progress = go.transform:Find("Progress/Fg"):GetComponent(typeof(UITexture))
        self.m_TaskTbl[k].progress.color = NGUIText.ParseColor24(value.CircleColor,0)
        self.m_TaskTbl[k].icon = go.transform:Find("Progress/Icon").gameObject
        self.m_TaskTbl[k].get = go.transform:Find("Progress/Get").gameObject
        self.m_TaskTbl[k].finishTip = go.transform:Find("FinishLabel").gameObject
        self.m_TaskTbl[k].fullCount = value.NeedTimes
        local btn = go.transform:Find("StartBtn").gameObject
        self.m_TaskTbl[k].toDoBtn = btn
        self.m_GoToTaskIdTbl[go] = k
        self:SetProgress(self.m_TaskTbl[k], self.m_LiuYi2TaskTbl[self.m_SelectLiuYiIndex][k].time, self.m_TaskTbl[k].fullCount, 0.5)
        UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function (go)
            ClientAction.DoAction(value.Action)
        end)
    end
    self.TaskTabel:Reposition()
end

function LuaWuYi2023JZLYWnd:RefreshItem(args)
    self.m_MianShiWenShuCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_MianShiWenShuItemId)
    self:UpdateIconCount(self.MianShiWenShu, self.m_MianShiWenShuCount, true)
end

function LuaWuYi2023JZLYWnd:JZLYSyncStampProgress(stampProgressU, wenshuRemainCount)
    local data = g_MessagePack.unpack(stampProgressU)
    if data then
        for stampId, progress in pairs(data) do
            local liuyiId = self.m_Task2LiuYiTbl[stampId]
            self.m_LiuYi2TaskTbl[liuyiId][stampId].time = progress
            if self.m_TaskTbl and self.m_TaskTbl[stampId] then
                self:SetProgress(self.m_TaskTbl[stampId], progress, self.m_TaskTbl[stampId].fullCount, 0.5)
            end
        end
    end
    self:UpdateLiuYiCount()
    self.m_WenShuRemain = wenshuRemainCount > 0
    self:RefreshItem()
end

function LuaWuYi2023JZLYWnd:XinXiangShiChengSendCoinAmount(gold, silver)
    self:UpdateIconCount(self.JinBoShuYin, gold, false)
    self:UpdateIconCount(self.YinBoShuYin, silver, false)
end

function LuaWuYi2023JZLYWnd:SetMode(isTaskView)
    self.m_isTaskView = isTaskView
    local path = "UI/Texture/Transparent/Material/zhongqiu2022mainwnd_btn_close_03.mat"
    if isTaskView then path = "UI/Texture/Transparent/Material/zhongqiu2022mainwnd_btn_close_04.mat" end
    self.CloseButton.transform:Find("Texture"):GetComponent(typeof(CUITexture)):LoadMaterial(path)
end

function LuaWuYi2023JZLYWnd:TrackSymbolStatusChange()
    if CTrackMgr.Inst.ShowTrackingTitle then
        g_MessageMgr:ShowMessage("WuYi2023JZLY_PathFinding")
    end
end

--@region UIEvent
function LuaWuYi2023JZLYWnd:OnCloseButtonClick()
    if self.m_isTaskView then
        self:SetMode(false)
        self.m_Animation:Play("wuyi2023jzlywnd_qiehuanhui")
    else
        CUIManager.CloseUI(CLuaUIResources.WuYi2023JZLYWnd)
    end
end

function LuaWuYi2023JZLYWnd:OnOpenBtnClick(index)
    LuaWuYi2023Mgr.m_XXSCDefaultIndex = index
    Gac2Gas.XinXiangShiChengQueryLotteryInfo()
end

function LuaWuYi2023JZLYWnd:OnArrowClick(delta)
    self.m_SelectLiuYiIndex = self.m_SelectLiuYiIndex + delta
    self:ShowLiuYiTask()
end
--@endregion UIEvent

function LuaWuYi2023JZLYWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "RefreshItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "RefreshItem")
	g_ScriptEvent:AddListener("JZLYSyncStampProgress", self, "JZLYSyncStampProgress")
	g_ScriptEvent:AddListener("XinXiangShiChengSendCoinAmount", self, "XinXiangShiChengSendCoinAmount")
    g_ScriptEvent:AddListener("TrackSymbolStatusChange", self, "TrackSymbolStatusChange")
end

function LuaWuYi2023JZLYWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "RefreshItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "RefreshItem")
	g_ScriptEvent:RemoveListener("JZLYSyncStampProgress", self, "JZLYSyncStampProgress")
	g_ScriptEvent:RemoveListener("XinXiangShiChengSendCoinAmount", self, "XinXiangShiChengSendCoinAmount")
    g_ScriptEvent:RemoveListener("TrackSymbolStatusChange", self, "TrackSymbolStatusChange")
end