
--属性切换是否开启
function IsAttrGroupOpen()
	return true
end

function IsNpcHaoGanDuOpen()
	-- playconfig
	return CommonDefs.IsPlayEnabled("NpcHaoGanDu")
end
--朱砂笔/传家宝洗炼
function IsZhuShaBiXiLianOpen()
	return true
end

local CLoginMgr = import "L10.Game.CLoginMgr"
local PlayConfig_ServerIdEnabled = import "L10.Game.PlayConfig_ServerIdEnabled"

function IsQYCodeUseOpen()
	if LuaCloudGameFaceMgr:IsCloudGameFace() then return false end
	-- 这个拿到的serverid可能会被合服，注意下。
	local gameServer = CLoginMgr.Inst:GetSelectedGameServer()
	local serverId = gameServer and gameServer.id
	if not serverId then return end

	local config = PlayConfig_ServerIdEnabled.GetData(serverId)
	local value = config and config.QYCodeUse
	if value and value ~= "" then
		return true
	end
	return false
end

--跨服队伍标识,据说用一段时间之后会取消
function IsCrossServerTeamInfoDisplayOpen()
	return false
end

function GetCrossServerTeamInfoDisplayText(teamId)
	if not IsCrossServerTeamInfoDisplayOpen() or not teamId then return "" end
	-- 王浩提供的规则 EnumTeamConstant.CROSS_ORIGINAL_ID = 500000
	if teamId>=500000 then 
		return LocalString.GetString("跨服")
	else 
		return LocalString.GetString("本服")
	end
end
-- END 跨服队伍标识

--是否开启仓库绑仓
function IsBindRepoDisplayOpen()
	return true
end

--哈猫任务按钮是否显示
function IsHaMaoButtonShow()
	return false
end

function IsShiMenMapChanged()
	return false
end

function IsSpecialBuildingOpen()
	return true
end

function IsTransformableFashionOpen()
	return true
end

function IsHouseFishOpen()
	return true
end
function IsLingShouLevelLimitOpen()
	return true
end
function IsOpenHouseTerrainEdit()
	return true
end
function IsOpenHouseGroundEdit()
	return true
end
--葫蘆娃 炼丹炉按钮是否显示
function IsHuLuWaLianDanLuBtnShow()
	return false
end
--宗门迭代是否开启
function IsZongPaiIterationOpen()
	return true
end
--是否开启家园装饰物连铺
function IsOpenFurnitureLianPu()
	return false
end
--是否开启府库
function IsOpenStoreRoom()
	return true
end
--是否开启查看他人外观迭代
function IsOpenNewPlayerAppearance()
	return true
end
--是否开启排行榜迭代
function IsOpenNewRankWnd()
	return true
end

function IsEnableChuangJue2023()
	return true
end
--是否开启自定义装饰物模板
function IsOpenCustomZswTemplate()
	return true
end