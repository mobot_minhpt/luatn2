local GameObject                = import "UnityEngine.GameObject"
local Vector3                   = import "UnityEngine.Vector3"
local Quaternion                = import "UnityEngine.Quaternion"
local CItemInfoMgr				= import "L10.UI.CItemInfoMgr"
local AlignType					= import "L10.UI.CItemInfoMgr+AlignType"
local CUITexture				= import "L10.UI.CUITexture"
local DelegateFactory		    = import "DelegateFactory"
local UIEventListener		    = import "UIEventListener"
local UILabel                   = import "UILabel"
local LuaGameObject             = import "LuaGameObject"
local LocalString               = import "LocalString"
local UIGrid                    = import "UIGrid"
local CUICommonDef 				= import "L10.UI.CUICommonDef"

LuaOffWorldPassDetailWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaOffWorldPassDetailWnd, "Grid1", "Grid1", UIGrid)
RegistChildComponent(LuaOffWorldPassDetailWnd, "BuyAdvPassBtn1", "BuyAdvPassBtn1", GameObject)
RegistChildComponent(LuaOffWorldPassDetailWnd, "BuyAdvPassBtn2", "BuyAdvPassBtn2", GameObject)
RegistChildComponent(LuaOffWorldPassDetailWnd, "ItemCell", "ItemCell", GameObject)
RegistChildComponent(LuaOffWorldPassDetailWnd, "Grid2", "Grid2", UIGrid)
RegistChildComponent(LuaOffWorldPassDetailWnd, "Count1", "Count1", UILabel)
RegistChildComponent(LuaOffWorldPassDetailWnd, "Count2", "Count2", UILabel)
RegistChildComponent(LuaOffWorldPassDetailWnd, "BtnLabel1", "BtnLabel1", UILabel)
RegistChildComponent(LuaOffWorldPassDetailWnd, "BtnLabel2", "BtnLabel2", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaOffWorldPassDetailWnd,  "ItemTbl1")
RegistClassMember(LuaOffWorldPassDetailWnd,  "ItemTbl2")
RegistClassMember(LuaOffWorldPassDetailWnd,  "SelectedItem")

function LuaOffWorldPassDetailWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.BuyAdvPassBtn1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuyAdvPassBtn1Click()
	end)

	UIEventListener.Get(self.BuyAdvPassBtn2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuyAdvPassBtn2Click()
	end)

    --@endregion EventBind end
end

function LuaOffWorldPassDetailWnd:Init()
    self.Count1.text = tostring(OffWorldPass_RmbPass.GetData(2).additionReward)
    self.Count2.text = tostring(OffWorldPass_RmbPass.GetData(3).additionReward)
    self.ItemTbl1 = {}
    self.ItemTbl2 = {}

    if LuaOffWorldPassMgr.page1data.pass2enable > 0 then
        self.BtnLabel1.text = LocalString.GetString("已解锁")
        CUICommonDef.SetActive(self.BuyAdvPassBtn1, false, true)
    end

    if LuaOffWorldPassMgr.page1data.pass3enable > 0 then
        self.BtnLabel2.text = LocalString.GetString("已解锁")
        CUICommonDef.SetActive(self.BuyAdvPassBtn2, false, true)
    end

    -- 遍历获取信息
    OffWorldPass_PassReward.Foreach(function(i,v)
        if v.luxury and v.luxury.Length >0 then
            local itemId = v.luxury[0]
            local count = v.luxury[1]
            if self.ItemTbl1[itemId] then
                self.ItemTbl1[itemId] = self.ItemTbl1[itemId] + count
            else
                self.ItemTbl1[itemId] = count
            end
        end

        if v.epic and v.epic.Length >0 then
            local itemId = v.epic[0]
            local count = v.epic[1]
            if self.ItemTbl2[itemId] then
                self.ItemTbl2[itemId] = self.ItemTbl2[itemId] + count
            else
                self.ItemTbl2[itemId] = count
            end
        end
    end)
    
    self:FillItems(self.ItemTbl1, self.Grid1, true)
    self:FillItems(self.ItemTbl2, self.Grid2, true)
end

function LuaOffWorldPassDetailWnd:FillItems(tbl, grid, isleft)
    CUICommonDef.ClearTransform(grid.transform)
    for k,v in pairs(tbl) do
        local go = GameObject.Instantiate(self.ItemCell, Vector3.zero, Quaternion.identity)
        go.transform.parent = grid.transform
        go.transform.localScale = Vector3.one
        self:FillItem(go,k,v,isleft)
        go:SetActive(true)
    end
    grid:Reposition()
end

function LuaOffWorldPassDetailWnd:FillItem(item,itemid,itemcount,isleft)
    local itemcfg = Item_Item.GetData(itemid)
    local iconTex = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local ctTxt = item.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
    if itemcfg then
        iconTex:LoadMaterial(itemcfg.Icon)
    end

    if tonumber(itemcount) <= 1 then
        ctTxt.text = ""
    else
        ctTxt.text = itemcount
    end

    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        local atype
        if isleft then
            atype = AlignType.ScreenRight
        else
            atype = AlignType.ScreenLeft
        end
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, atype, 0, 0, 0, 0) 
        if self.SelectedItem ~= go then
            if self.SelectedItem then
                self:SelectItem(self.SelectedItem,false)
            end
            self.SelectedItem = go
            self:SelectItem(self.SelectedItem,true)
        end
    end)
end

function LuaOffWorldPassDetailWnd:UpdateStatus()
    if LuaOffWorldPassMgr.page1data.pass2enable > 0 then
        self.BtnLabel1.text = LocalString.GetString("已解锁")
        CUICommonDef.SetActive(self.BuyAdvPassBtn1, false, true)
    end

    if LuaOffWorldPassMgr.page1data.pass3enable > 0 then
        self.BtnLabel2.text = LocalString.GetString("已解锁")
        CUICommonDef.SetActive(self.BuyAdvPassBtn2, false, true)
    end
end

function LuaOffWorldPassDetailWnd:SelectItem(itemgo,selected)
    local selectgo = LuaGameObject.GetChildNoGC(itemgo.transform,"SelectedSprite").gameObject
    selectgo:SetActive(selected)
end

function LuaOffWorldPassDetailWnd:OnEnable( )
    g_ScriptEvent:AddListener("UpdateBuyOffWorldPassResult", self, "UpdateStatus")
end

function LuaOffWorldPassDetailWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("UpdateBuyOffWorldPassResult", self, "UpdateStatus")
end

--@region UIEvent

function LuaOffWorldPassDetailWnd:OnBuyAdvPassBtn1Click()
    Gac2Gas.PlayerRequestBuyOffWorldPass(2,OffWorldPass_RmbPass.GetData(2).name)
end

function LuaOffWorldPassDetailWnd:OnBuyAdvPassBtn2Click()
    Gac2Gas.PlayerRequestBuyOffWorldPass(3,OffWorldPass_RmbPass.GetData(3).name)
end


--@endregion UIEvent

