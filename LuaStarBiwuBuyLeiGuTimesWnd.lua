local DelegateFactory  = import "DelegateFactory"
local QnRadioBox = import "L10.UI.QnRadioBox"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local GameObject = import "UnityEngine.GameObject"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"

LuaStarBiwuBuyLeiGuTimesWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaStarBiwuBuyLeiGuTimesWnd, "QnRadioBox", "QnRadioBox", QnRadioBox)
RegistChildComponent(LuaStarBiwuBuyLeiGuTimesWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaStarBiwuBuyLeiGuTimesWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaStarBiwuBuyLeiGuTimesWnd, "BuyButton", "BuyButton", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaStarBiwuBuyLeiGuTimesWnd,"m_Index")

function LuaStarBiwuBuyLeiGuTimesWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.BuyButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuyButtonClick()
	end)


    --@endregion EventBind end
end

function LuaStarBiwuBuyLeiGuTimesWnd:Init()
	for i = 0, self.QnRadioBox.m_RadioButtons.Length - 1 do
		local btn = self.QnRadioBox.m_RadioButtons[i]
		local timesLabel = btn.transform:Find("TimesLabel/Label"):GetComponent(typeof(UILabel))
		local times = CLuaStarBiwuMgr.m_LeiguId2Times[i + 1]
		timesLabel.text = SafeStringFormat3(LocalString.GetString("%d") ,times and times or 0)
	end
	self.QnCostAndOwnMoney:SetType(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
	self.QnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
		self:OnSelect(index)
	end)
	self.QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function(v)
		self:OnValueChanged(v)
    end)
	self.QnIncreseAndDecreaseButton:SetMinMax(1, 999)
	self.m_Index = CLuaStarBiwuMgr.m_LeiGuTimesWndDefaultIndex
	self.QnRadioBox:ChangeTo(self.m_Index, true)
	self.QnIncreseAndDecreaseButton:SetValue(1, true)
end

function LuaStarBiwuBuyLeiGuTimesWnd:OnEnable()
	g_ScriptEvent:AddListener("OnSyncStarBiwuLeiGuTimes",self,"OnSyncStarBiwuLeiGuTimes")
end

function LuaStarBiwuBuyLeiGuTimesWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSyncStarBiwuLeiGuTimes",self,"OnSyncStarBiwuLeiGuTimes")
end

function LuaStarBiwuBuyLeiGuTimesWnd:UpdateLeiGuTimes()
	local times = CLuaStarBiwuMgr.m_LeiguId2Times[self.m_Index + 1]
	self.QnIncreseAndDecreaseButton:SetMinMax(1, math.max(1,999 - (times and times or 0)),1)
	for i = 0, self.QnRadioBox.m_RadioButtons.Length - 1 do
		local btn = self.QnRadioBox.m_RadioButtons[i]
		local timesLabel = btn.transform:Find("TimesLabel/Label"):GetComponent(typeof(UILabel))
		local times = CLuaStarBiwuMgr.m_LeiguId2Times[i + 1]
		timesLabel.text = SafeStringFormat3(LocalString.GetString("%d") ,times and times or 0)
	end
end

function LuaStarBiwuBuyLeiGuTimesWnd:OnSyncStarBiwuLeiGuTimes()
	self:UpdateLeiGuTimes()
end
--@region UIEvent

function LuaStarBiwuBuyLeiGuTimesWnd:OnBuyButtonClick()
	Gac2Gas.StarBiwuRequestByLeiGuTimes(self.m_Index + 1, self.QnIncreseAndDecreaseButton:GetValue())
end

function LuaStarBiwuBuyLeiGuTimesWnd:OnValueChanged(v)
	self:SetCost()
end

function LuaStarBiwuBuyLeiGuTimesWnd:OnSelect(index)
	self.m_Index = index
	self:SetCost()
	self:UpdateLeiGuTimes()
end

--@endregion UIEvent

function LuaStarBiwuBuyLeiGuTimesWnd:SetCost()
	local leiguData = StarBiWuShow_LeiGu.GetData(self.m_Index + 1)
	if leiguData then
		self.QnCostAndOwnMoney:SetCost(leiguData.LingYu * self.QnIncreseAndDecreaseButton:GetValue())
	end
end