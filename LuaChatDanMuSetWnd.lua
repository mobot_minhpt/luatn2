local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UISimpleTableView = import "L10.UI.UISimpleTableView"
local Vector3 = import "UnityEngine.Vector3"
local CUITexture = import "L10.UI.CUITexture"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local Profession = import "L10.Game.Profession"
local DefaultUISimpleTableViewDataSource = import "L10.UI.DefaultUISimpleTableViewDataSource"

LuaChatDanMuSetWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaChatDanMuSetWnd, "CountLabel", "CountLabel", UILabel)
RegistChildComponent(LuaChatDanMuSetWnd, "RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaChatDanMuSetWnd, "LeftTable", "LeftTable", UITable)
RegistChildComponent(LuaChatDanMuSetWnd, "AttentionPlayerTemplate", "AttentionPlayerTemplate", GameObject)

RegistChildComponent(LuaChatDanMuSetWnd, "SearchInput", "SearchInput", UIInput)
RegistChildComponent(LuaChatDanMuSetWnd, "SearchButton", "SearchButton", GameObject)
RegistChildComponent(LuaChatDanMuSetWnd, "ClearSearchButton", "ClearSearchButton", GameObject)
RegistChildComponent(LuaChatDanMuSetWnd, "PlayerScrollView", "PlayerScrollView", UISimpleTableView)
RegistChildComponent(LuaChatDanMuSetWnd, "PlayerTemplate", "PlayerTemplate", GameObject)

RegistClassMember(LuaChatDanMuSetWnd, "m_Data")
RegistClassMember(LuaChatDanMuSetWnd, "m_PlayerList")
RegistClassMember(LuaChatDanMuSetWnd, "m_AttentionPlayerList")
RegistClassMember(LuaChatDanMuSetWnd, "m_AttentionPlayerItemList")

--@endregion RegistChildComponent end

function LuaChatDanMuSetWnd:Awake()
    UIEventListener.Get(self.RuleBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRuleBtnClick()
    end)
    UIEventListener.Get(self.SearchButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnSearchButtonClick()
    end)
    UIEventListener.Get(self.ClearSearchButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnClearSearchButtonClick()
    end)
    CommonDefs.AddEventDelegate(self.SearchInput.onChange, DelegateFactory.Action(function ()
        self:OnInputValueChanged()
    end))
end

function LuaChatDanMuSetWnd:Init()
    self.m_AttentionPlayerItemList = {}
    Extensions.RemoveAllChildren(self.LeftTable.transform)
    self.PlayerTemplate:SetActive(false)
    self.AttentionPlayerTemplate:SetActive(false)
    Gac2Gas.QuerySpecialConcernCandidatePlayers()
end

function LuaChatDanMuSetWnd:InitLeftView()
    self.CountLabel.text = SafeStringFormat3(LocalString.GetString("特别关注玩家 %d/%d"), #self.m_AttentionPlayerList, LuaChatMgr.m_DanMuAttentionPlayerMaxCount)
    for i = 1, #self.m_AttentionPlayerItemList do
        self.m_AttentionPlayerItemList[i]:SetActive(false)
    end
    for i = 1, #self.m_AttentionPlayerList do
        local index = i
        local playerInfo = self.m_AttentionPlayerList[i]
        local cell = self.m_AttentionPlayerItemList[i]
        if cell == nil then
            cell = NGUITools.AddChild(self.LeftTable.gameObject, self.AttentionPlayerTemplate)
            table.insert(self.m_AttentionPlayerItemList, cell)
        end
        cell:SetActive(true)
        self:InitPlayerItem(cell, playerInfo, true)
        local delBtn = cell.transform:Find("DelBtn").gameObject
        UIEventListener.Get(delBtn).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnDelBtnClick(index)
        end)
    end
    self.LeftTable:Reposition()
end

function LuaChatDanMuSetWnd:InitRightView()
    if not self.m_DefaultSimpleTableViewDataSource then
		self.m_DefaultSimpleTableViewDataSource = DefaultUISimpleTableViewDataSource.Create(function ()
			return #self.m_Data
		end,
		function (index)
			return self:CellForRowAtIndex(index)
		end)
	end
    self.PlayerScrollView.dataSource = self.m_DefaultSimpleTableViewDataSource
end

function LuaChatDanMuSetWnd:SyncSpecialConcernCandidatePlayers(playersTblU)
    local attentionTbl = {}
    for i = 1, #LuaChatMgr.m_DanMuAttentionPlayerList do
        attentionTbl[LuaChatMgr.m_DanMuAttentionPlayerList[i]] = true
    end
    self.m_AttentionPlayerList = {}
    self.m_PlayerList = {}
    local data = g_MessagePack.unpack(playersTblU)
    for _, playerInfo in ipairs(data) do
        local info = {}
        info.playerId = playerInfo[1]
        info.playerName = playerInfo[2]
        info.level = playerInfo[3]
        info.class = playerInfo[4]
        info.gender = playerInfo[5]
        info.portraitName = CUICommonDef.GetPortraitName(info.class, info.gender, playerInfo[6])
        info.expressionTxt = playerInfo[7]
        info.profileFrame = playerInfo[9]
        info.hasFeiSheng = playerInfo[11]
        info.friend = playerInfo[12]
        info.relation = ""

        if playerInfo[13] > 0 then
        elseif playerInfo[14] == 1 then
            info.relation = LocalString.GetString("师父")
        elseif playerInfo[15] == 1 then
            info.relation = LocalString.GetString("徒弟")
        end

        if attentionTbl[info.playerId] then
            table.insert(self.m_AttentionPlayerList, info)
        else
            table.insert(self.m_PlayerList, info)
        end
    end
    self:InitLeftView()
    self:InitRightView()
    self:OnClearSearchButtonClick()
end

function LuaChatDanMuSetWnd:CellForRowAtIndex(index)
    local playerInfo = self.m_Data[index + 1]
    if playerInfo then
        local cellIdentifier = "PlayerItem"
        local cell = self.PlayerScrollView:DequeueReusableCellWithIdentifier(cellIdentifier)
        if cell == nil then
            cell = self.PlayerScrollView:AllocNewCellWithIdentifier(self.PlayerTemplate, cellIdentifier)
        end
        cell.gameObject:SetActive(true)
        self:InitPlayerItem(cell, playerInfo, false)
        local addBtn = cell.transform:Find("AddBtn").gameObject
        UIEventListener.Get(addBtn).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnAddBtnClick(index + 1)
        end)
        return cell
    end
    return nil
end

function LuaChatDanMuSetWnd:InitPlayerItem(item, playerInfo, isAttention)
    item.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = playerInfo.playerName
    local lvLabel = item.transform:Find("LvLabel"):GetComponent(typeof(UILabel))
    lvLabel.text = isAttention and playerInfo.level or SafeStringFormat3("Lv.%d", playerInfo.level)
    lvLabel.color = playerInfo.hasFeiSheng and NGUIText.ParseColor24("fe7900", 0) or Color.white

    local relationLabel = item.transform:Find("RelationLabel")
    if relationLabel then
        relationLabel:GetComponent(typeof(UILabel)).text = playerInfo.relation
    end
    local classIcon = item.transform:Find("ClassIcon")
    if classIcon then
        classIcon:GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(playerInfo.class)
    end

    local texture = item.transform:Find("PortraitRoot/Portrait"):GetComponent(typeof(CUITexture))
    local profileFrame = item.transform:Find("PortraitRoot/ProfileFrame"):GetComponent(typeof(CUITexture))
    local expressionTxt = item.transform:Find("PortraitRoot/ExpressionText"):GetComponent(typeof(CUITexture))
    texture:LoadNPCPortrait(playerInfo.portraitName, false)
    if profileFrame ~= nil and CExpressionMgr.EnableProfileFrame then
        profileFrame:LoadMaterial(CUICommonDef.GetProfileFramePath(playerInfo.profileFrame))
    end
    expressionTxt:LoadMaterial(CUICommonDef.GetExpressionTxtPath(playerInfo.expressionTxt))
end

--@region UIEvent
function LuaChatDanMuSetWnd:OnRuleBtnClick()
    g_MessageMgr:ShowMessage("ChatDanMu_AttentionPlayerSet_Rule")
end

function LuaChatDanMuSetWnd:OnSearchButtonClick()
    local resTable = {}
    local searchKey = self.SearchInput.value
    if System.String.IsNullOrEmpty(searchKey) or #searchKey < 0 then
        g_MessageMgr:ShowMessage("CommonPlayerList_No_Search_Input")
        return
    end

    local default, searchId = System.UInt64.TryParse(searchKey)
    for i = 1, #self.m_PlayerList do
        local data = self.m_PlayerList[i]
        if default and data.playerId == searchId then
            table.insert(resTable, data)
        elseif string.find(data.playerName, searchKey, 1, true) then
            local name = CommonDefs.StringReplace(data.playerName, searchKey, SafeStringFormat3("[c][FF9052]%s[-][/c]", searchKey))
            local _data = {}
            for key, value in pairs(data) do
                _data[key] = value
            end
            _data.playerName = name
            _data.originalData = data
            table.insert(resTable, _data)
        end
    end
    if #resTable == 0 then
        g_MessageMgr:ShowMessage("CommonPlayerList_No_Search_Result")
        return
    end
    self.m_Data = resTable
    self.PlayerScrollView:Clear()
    self.PlayerScrollView:LoadData(0, false)
end

function LuaChatDanMuSetWnd:OnClearSearchButtonClick()
    self.SearchInput.value = ""
    self:OnInputValueChanged()
    self.m_Data = self.m_PlayerList
    self.PlayerScrollView:Clear()
    self.PlayerScrollView:LoadData(0, false)
end

function LuaChatDanMuSetWnd:OnInputValueChanged()
    self.ClearSearchButton.gameObject:SetActive(not System.String.IsNullOrEmpty(self.SearchInput.value))
end

function LuaChatDanMuSetWnd:OnAddBtnClick(index)
	if #LuaChatMgr.m_DanMuAttentionPlayerList < LuaChatMgr.m_DanMuAttentionPlayerMaxCount then
        local playerInfo = self.m_Data[index]
        table.remove(self.m_Data, index)
        for i = 1, #self.m_PlayerList do
            if self.m_PlayerList[i].playerId == playerInfo.playerId then
                table.remove(self.m_PlayerList, i)
                break
            end
        end
		table.insert(self.m_AttentionPlayerList, playerInfo.originalData and playerInfo.originalData or playerInfo)
        self:InitLeftView()
        self.PlayerScrollView:LoadData(math.max(index - 3, 0), false)
	else
		g_MessageMgr:ShowMessage("ChatDanMu_AttentionPlayerSet_MaxCountLimit")
	end
    LuaChatMgr:UpdateDanMuAttentionPlayer(self.m_AttentionPlayerList)
end

function LuaChatDanMuSetWnd:OnDelBtnClick(index)
    local playerInfo = self.m_AttentionPlayerList[index]
    table.remove(self.m_AttentionPlayerList, index)
    table.insert(self.m_PlayerList, playerInfo)
    self:InitLeftView()
    table.sort(self.m_PlayerList, function (a, b)
        if a.friend == b.friend then
            return a.playerId < b.playerId
        end
        return a.friend > b.friend
    end)
    self:OnClearSearchButtonClick()
    LuaChatMgr:UpdateDanMuAttentionPlayer(self.m_AttentionPlayerList)
end
--@endregion UIEvent

function LuaChatDanMuSetWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncSpecialConcernCandidatePlayers", self, "SyncSpecialConcernCandidatePlayers")
end

function LuaChatDanMuSetWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncSpecialConcernCandidatePlayers", self, "SyncSpecialConcernCandidatePlayers")
    LuaChatMgr:UpdateDanMuAttentionPlayer(self.m_AttentionPlayerList)
end