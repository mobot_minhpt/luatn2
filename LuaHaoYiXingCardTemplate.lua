
LuaHaoYiXingCardTemplate = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHaoYiXingCardTemplate, "NumLabel", "NumLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardTemplate, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardTemplate, "BlueTexture", "BlueTexture", GameObject)
RegistChildComponent(LuaHaoYiXingCardTemplate, "RedTexture", "RedTexture", GameObject)
RegistChildComponent(LuaHaoYiXingCardTemplate, "PurpleTexture", "PurpleTexture", GameObject)
RegistChildComponent(LuaHaoYiXingCardTemplate, "OwnerTexture", "OwnerTexture", CUITexture)
RegistChildComponent(LuaHaoYiXingCardTemplate, "SketchTexture", "SketchTexture", CUITexture)
RegistChildComponent(LuaHaoYiXingCardTemplate, "SuiPianTexture", "SuiPianTexture", UITexture)
RegistChildComponent(LuaHaoYiXingCardTemplate, "NewTipTexture", "NewTipTexture", GameObject)
RegistChildComponent(LuaHaoYiXingCardTemplate, "Border", "Border", UITexture)
RegistChildComponent(LuaHaoYiXingCardTemplate, "Fx", "Fx", CUIFx)
RegistChildComponent(LuaHaoYiXingCardTemplate, "SuiPianLabel", "SuiPianLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardTemplate, "HideSprite", "HideSprite", UITexture)
RegistChildComponent(LuaHaoYiXingCardTemplate, "SelectSprite", "SelectSprite", GameObject)
RegistChildComponent(LuaHaoYiXingCardTemplate, "WenHao", "WenHao", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaHaoYiXingCardTemplate, "m_IsOwner")
RegistClassMember(LuaHaoYiXingCardTemplate, "m_IsSelected")

function LuaHaoYiXingCardTemplate:Awake()
    --@region EventBind: Dont Modify Manually!
    self.gameObject:SetActive(false)
    self.NewTipTexture:SetActive(false)
    self.BlueTexture:SetActive(false)
    self.RedTexture:SetActive(false)
    self.PurpleTexture:SetActive(false)
    self.OwnerTexture.gameObject:SetActive(false)
    self.SketchTexture.gameObject:SetActive(false)
    self.m_IsSelected = false
    self.Border.gameObject:SetActive(self.m_IsSelected)
    --self.QiDaiLabel:SetActive(false)
    self.HideSprite.gameObject:SetActive(false)
    self.SelectSprite.gameObject:SetActive(false)
    if self.WenHao then
        self.WenHao.gameObject:SetActive(false)
    end
    --@endregion EventBind end
end

function LuaHaoYiXingCardTemplate:InitCard(tcgID,owner,isNew,num,pieceNum,taskStatus, isComposeResultWnd)
    self.m_IsOwner = owner
    local data = SpokesmanTCG_ErHaTCG.GetData(tcgID)
    self.OwnerTexture.gameObject:SetActive(owner or (pieceNum ~= 0 and pieceNum < data.PieceCount and num < 1) or isComposeResultWnd)
    self.OwnerTexture.texture.width = data.Type == 1 and 200 or 300
    self.OwnerTexture.texture.height = data.Type == 1 and 200 or 350
    self.SketchTexture.gameObject:SetActive(true)
    self.OwnerTexture:LoadMaterial(data.Res)
    self.SuiPianTexture.gameObject:SetActive(pieceNum ~= 0 and pieceNum < data.PieceCount and num < 1 and not owner)
    self.SketchTexture:LoadMaterial(data.Silhouette)
    self.BlueTexture:SetActive(data.Type == 1)
    self.RedTexture:SetActive(data.Type == 2)
    self.PurpleTexture:SetActive(data.Type == 3 or data.Type == 4)
    self.gameObject:SetActive(true)
    self.NewTipTexture:SetActive(isNew)
    self.NumLabel.gameObject:SetActive(num > 0)
    self.NumLabel.text = num
    self.NameLabel.text = data.Name
    self.SuiPianLabel.text = SafeStringFormat3(LocalString.GetString("碎片%d/%d"),pieceNum,data.PieceCount) 
    self.SuiPianLabel.gameObject:SetActive(data.PieceCount > 0)
    self.HideSprite.gameObject:SetActive(data.IsHidden ~= 0)
    self.HideSprite.color = NGUIText.ParseColor24(data.Type == 1 and "A8E0F3" or (data.Type == 2 and "FFC7A8" or "EAB7FF"), 0)
    self.HideSprite.transform:Find("HideLabel"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24(data.Type == 1 and "383c6b" or (data.Type == 2 and "843f31" or "532a87"), 0)
    if self.WenHao then
        self.WenHao.gameObject:SetActive(owner and data.Type == 4 and taskStatus == 0 and data.AcceptTask ~= 0 )
    end
    self:SetSelect(false)
end

function LuaHaoYiXingCardTemplate:OnCardClick()
    self.NewTipTexture:SetActive(false)
    self:SetSelect(not self.m_IsSelected)
end

function LuaHaoYiXingCardTemplate:SetSelect(isSelected)
    self.m_IsSelected = isSelected
    self.Border.gameObject:SetActive(self.m_IsSelected)
end
--@region UIEvent

--@endregion UIEvent

