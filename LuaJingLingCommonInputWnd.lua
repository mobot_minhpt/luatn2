require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local UILabel = import "UILabel"
local UITexture = import "UITexture"
local UIInput = import "UIInput"
local DelegateFactory = import "DelegateFactory"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local CJingLingWebImageMgr = import "L10.Game.CJingLingWebImageMgr"
local GameObject = import "UnityEngine.GameObject"

LuaJingLingCommonInputWnd = class()
RegistClassMember(LuaJingLingCommonInputWnd,"closeBtn")
RegistClassMember(LuaJingLingCommonInputWnd,"titleLabel")
--模板1
RegistClassMember(LuaJingLingCommonInputWnd,"firstWnd_Root")
RegistClassMember(LuaJingLingCommonInputWnd,"firstWnd_Label1")
RegistClassMember(LuaJingLingCommonInputWnd,"firstWnd_Input1")
RegistClassMember(LuaJingLingCommonInputWnd,"firstWnd_Label2")
RegistClassMember(LuaJingLingCommonInputWnd,"firstWnd_Input2")

--模板2
RegistClassMember(LuaJingLingCommonInputWnd,"secondWnd_Root")
RegistClassMember(LuaJingLingCommonInputWnd,"secondWnd_Label")
RegistClassMember(LuaJingLingCommonInputWnd,"secondWnd_Texture")
RegistClassMember(LuaJingLingCommonInputWnd,"secondWnd_Input")

--模板3
RegistClassMember(LuaJingLingCommonInputWnd,"thirdWnd_Root")
RegistClassMember(LuaJingLingCommonInputWnd,"thirdWnd_Label1")
RegistClassMember(LuaJingLingCommonInputWnd,"thirdWnd_Input1")
RegistClassMember(LuaJingLingCommonInputWnd,"thirdWnd_Label2")
RegistClassMember(LuaJingLingCommonInputWnd,"thirdWnd_Input2")
RegistClassMember(LuaJingLingCommonInputWnd,"thirdWnd_Label3")
RegistClassMember(LuaJingLingCommonInputWnd,"thirdWnd_Input3")

--模板4
RegistClassMember(LuaJingLingCommonInputWnd,"fourthWnd_Root")
RegistClassMember(LuaJingLingCommonInputWnd,"fourthWnd_GLLabel1")
RegistClassMember(LuaJingLingCommonInputWnd,"fourthWnd_Label1")
RegistClassMember(LuaJingLingCommonInputWnd,"fourthWnd_Input1")
RegistClassMember(LuaJingLingCommonInputWnd,"fourthWnd_Label2")
RegistClassMember(LuaJingLingCommonInputWnd,"fourthWnd_Input2")
RegistClassMember(LuaJingLingCommonInputWnd,"fourthWnd_GLLabel2")
RegistClassMember(LuaJingLingCommonInputWnd,"fourthWnd_Input3")

RegistClassMember(LuaJingLingCommonInputWnd,"okBtn")
RegistClassMember(LuaJingLingCommonInputWnd,"cancelBtn")


RegistClassMember(LuaJingLingCommonInputWnd,"currentUIIndex")
RegistClassMember(LuaJingLingCommonInputWnd,"imagePath")

function LuaJingLingCommonInputWnd:Awake()
    
end

function LuaJingLingCommonInputWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaJingLingCommonInputWnd:Init()
	self.closeBtn = self.transform:Find("Wnd_Bg_Secondary_1/CloseButton").gameObject
	self.titleLabel = self.transform:Find("Wnd_Bg_Secondary_1/TitleLabel"):GetComponent(typeof(UILabel))
	self.okBtn = self.transform:Find("Anchor/OKButton").gameObject
	self.cancelBtn =  self.transform:Find("Anchor/CancelButton").gameObject
	self.currentUIIndex = CJingLingMgr.Inst.m_CommonInputLinkInfo.index

	self.firstWnd_Root = self.transform:Find("Anchor/Tpl1").gameObject
	self.secondWnd_Root = self.transform:Find("Anchor/Tpl2").gameObject
	self.thirdWnd_Root = self.transform:Find("Anchor/Tpl3").gameObject
	self.fourthWnd_Root = self.transform:Find("Anchor/Tpl4").gameObject

	self.firstWnd_Root:SetActive(true)
	self.secondWnd_Root:SetActive(true)
	self.thirdWnd_Root:SetActive(true)
	self.fourthWnd_Root:SetActive(true)

	self.firstWnd_Label1 = self.firstWnd_Root.transform:Find("Label1"):GetComponent(typeof(UILabel))
	self.firstWnd_Input1 = self.firstWnd_Root.transform:Find("Input1"):GetComponent(typeof(UIInput))
	self.firstWnd_Label2 = self.firstWnd_Root.transform:Find("Label2"):GetComponent(typeof(UILabel))
	self.firstWnd_Input2 = self.firstWnd_Root.transform:Find("Input2"):GetComponent(typeof(UIInput))

	self.secondWnd_Label = self.secondWnd_Root.transform:Find("Label"):GetComponent(typeof(UILabel))
	self.secondWnd_Texture = self.secondWnd_Root.transform:Find("Texture"):GetComponent(typeof(UITexture))
	self.secondWnd_Input = self.secondWnd_Root.transform:Find("Input"):GetComponent(typeof(UIInput))

	self.thirdWnd_Label1 = self.thirdWnd_Root.transform:Find("Label1"):GetComponent(typeof(UILabel))
	self.thirdWnd_Input1 = self.thirdWnd_Root.transform:Find("Input1"):GetComponent(typeof(UIInput))
	self.thirdWnd_Label2 = self.thirdWnd_Root.transform:Find("Label2"):GetComponent(typeof(UILabel))
	self.thirdWnd_Input2 = self.thirdWnd_Root.transform:Find("Input2"):GetComponent(typeof(UIInput))
	self.thirdWnd_Label3 = self.thirdWnd_Root.transform:Find("Label3"):GetComponent(typeof(UILabel))
	self.thirdWnd_Input3 = self.thirdWnd_Root.transform:Find("Input3"):GetComponent(typeof(UIInput))

	self.fourthWnd_GLLabel1 = self.fourthWnd_Root.transform:Find("GLLabel1"):GetComponent(typeof(UILabel))
	self.fourthWnd_Label1 = self.fourthWnd_Root.transform:Find("Label1"):GetComponent(typeof(UILabel))
	self.fourthWnd_Input1 = self.fourthWnd_Root.transform:Find("Input1"):GetComponent(typeof(UIInput))
	self.fourthWnd_Label2 = self.fourthWnd_Root.transform:Find("Label2"):GetComponent(typeof(UILabel))
	self.fourthWnd_Input2 = self.fourthWnd_Root.transform:Find("Input2"):GetComponent(typeof(UIInput))
	self.fourthWnd_GLLabel2 = self.fourthWnd_Root.transform:Find("GLLabel2"):GetComponent(typeof(UILabel))
	self.fourthWnd_Input3 = self.fourthWnd_Root.transform:Find("Input3"):GetComponent(typeof(UIInput))

	self.firstWnd_Root:SetActive(self.currentUIIndex == 1)
	self.secondWnd_Root:SetActive(self.currentUIIndex == 2)
	self.thirdWnd_Root:SetActive(self.currentUIIndex == 3)
	self.fourthWnd_Root:SetActive(self.currentUIIndex == 4)

	local info = CJingLingMgr.Inst.m_CommonInputLinkInfo
	self.titleLabel.text = info.title
	self.imagePath = info.imagePath
	if self.currentUIIndex == 1 then
		self.firstWnd_Label1.text = info.label1
		self.firstWnd_Input1.defaultText = info.default1
		self.firstWnd_Label2.text = info.label2
		self.firstWnd_Input2.defaultText = info.default2
	elseif self.currentUIIndex == 2 then
		self.secondWnd_Label.text = info.label1
		self.secondWnd_Texture.mainTexture = nil
		if self:IsNullOrEmpty(self.imagePath) then
			self.secondWnd_Label.text = info.label1
			self.secondWnd_Texture.mainTexture = nil
		else
			self.secondWnd_Label.text = nil
			self.secondWnd_Texture.mainTexture = nil
			local tex = CJingLingWebImageMgr.Inst:LoadImageByUrl(self.imagePath)
			if tex then
				self:LoadImage(tex)
			else
				CJingLingWebImageMgr.Inst:DownloadImage(self.imagePath)
			end
		end
		self.secondWnd_Input.defaultText = info.default1
	elseif self.currentUIIndex == 3 then
		self.thirdWnd_Label1.text = info.label1
		self.thirdWnd_Input1.defaultText = info.default1
		self.thirdWnd_Label2.text = info.label2
		self.thirdWnd_Input2.defaultText = info.default2
		self.thirdWnd_Label3.text = info.gl1
		self.thirdWnd_Input3.defaultText = info.default3
	elseif self.currentUIIndex == 4 then
		self.fourthWnd_GLLabel1.text = info.gl1
		self.fourthWnd_Label1.text = info.label1
		self.fourthWnd_Input1.defaultText = info.default1
		self.fourthWnd_Label2.text = info.label2
		self.fourthWnd_Input2.defaultText = info.default2
		self.fourthWnd_GLLabel2.text = info.gl2
		self.fourthWnd_Input3.defaultText = info.default3
	end

	CommonDefs.AddOnClickListener(self.closeBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)
	CommonDefs.AddOnClickListener(self.okBtn, DelegateFactory.Action_GameObject(function(go) self:OnOKButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.cancelBtn, DelegateFactory.Action_GameObject(function(go) self:OnCancelButtonClick() end), false)
	
end

function LuaJingLingCommonInputWnd:OnEnable()
	g_ScriptEvent:AddListener("OnJingLingWebImageReady", self, "OnImageReady")
end

function LuaJingLingCommonInputWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnJingLingWebImageReady", self, "OnImageReady")
end

function LuaJingLingCommonInputWnd:OnImageReady(args)
	if self.imagePath == args[0] then
		local tex = CJingLingWebImageMgr.Inst:LoadImageByUrl(self.imagePath)
		if tex then
			self:LoadImage(tex)
		end
	end
end

function LuaJingLingCommonInputWnd:LoadImage(tex)
	if self.secondWnd_Texture.mainTexture ~= nil then
        GameObject.Destroy(self.secondWnd_Texture.mainTexture)
    end
    self.secondWnd_Texture.mainTexture = tex
end

function LuaJingLingCommonInputWnd:OnOKButtonClick()
	local info = CJingLingMgr.Inst.m_CommonInputLinkInfo
	self.titleLabel.text = info.title
	if self.currentUIIndex == 1 then
		local input1 = self:Trim(self.firstWnd_Input1.value)
		local input2 = self:Trim(self.firstWnd_Input2.value)
		if self:IsNullOrEmpty(input1) or self:IsNullOrEmpty(input2) then
			g_MessageMgr:ShowMessage("JingLing_Dynamic_UI_Input_Cannot_Empty")
		else
			CJingLingMgr.Inst:SubmitCommonInput(info.index, info.title, input1, nil, input2)
			self:Close()
		end
	elseif self.currentUIIndex == 2 then
		local input1 = self:Trim(self.secondWnd_Input.value)
		if self:IsNullOrEmpty(input1) then
			g_MessageMgr:ShowMessage("JingLing_Dynamic_UI_Input_Cannot_Empty")
		else
			CJingLingMgr.Inst:SubmitCommonInput(info.index, info.title, nil, nil, input1)
			self:Close()
		end
	elseif self.currentUIIndex == 3 then
		local input1 = self:Trim(self.thirdWnd_Input1.value)
		local input2 = self:Trim(self.thirdWnd_Input2.value)
		local input3 = self:Trim(self.thirdWnd_Input3.value)
		if self:IsNullOrEmpty(input1) or self:IsNullOrEmpty(input2) or self:IsNullOrEmpty(input3) then
			g_MessageMgr:ShowMessage("JingLing_Dynamic_UI_Input_Cannot_Empty")
		else
			CJingLingMgr.Inst:SubmitCommonInput(info.index, info.title, input1, input2, input3)
			self:Close()
		end
	elseif self.currentUIIndex == 4 then
		local input1 = self:Trim(self.fourthWnd_Input1.value)
		local input2 = self:Trim(self.fourthWnd_Input2.value)
		local input3 = self:Trim(self.fourthWnd_Input3.value)
		if self:IsNullOrEmpty(input1) or self:IsNullOrEmpty(input2) or self:IsNullOrEmpty(input3) then
			g_MessageMgr:ShowMessage("JingLing_Dynamic_UI_Input_Cannot_Empty")
		else
			CJingLingMgr.Inst:SubmitCommonInput(info.index, info.title, input1, input2, input3)
			self:Close()
		end
	end
end

function LuaJingLingCommonInputWnd:OnCancelButtonClick()
	self:Close()
end

function LuaJingLingCommonInputWnd:Trim(str)
	if str== nil or str == "" then
		return str
	end
	str = string.gsub(str, "^[ \t\n\r]+", "")
	return string.gsub(str, "[ \t\n\r]+$", "")
end

function LuaJingLingCommonInputWnd:IsNullOrEmpty(str)
	return str == nil or str == ""
end


return LuaJingLingCommonInputWnd