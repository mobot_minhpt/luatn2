local CUITexture=import "L10.UI.CUITexture"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local Zhuangshiwu_Zhuangshiwu = import "L10.Game.Zhuangshiwu_Zhuangshiwu"
local Item_Item = import "L10.Game.Item_Item"
local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"
local GestureRecognizer = import "L10.Engine.GestureRecognizer"
local GestureType = import "L10.Engine.GestureType"
local DelegateFactory = import "DelegateFactory"
local Vector3 = import "UnityEngine.Vector3"
local Camera = import "UnityEngine.Camera"
local Zhuangshiwu_Setting = import "L10.Game.Zhuangshiwu_Setting"
local Quaternion = import "UnityEngine.Quaternion"


CLuaZhuangshiwuPreviewWnd = class()

RegistClassMember(CLuaZhuangshiwuPreviewWnd, "m_ModelTextureLoader")
RegistClassMember(CLuaZhuangshiwuPreviewWnd, "m_Camera")
RegistClassMember(CLuaZhuangshiwuPreviewWnd, "m_RotateBtnLeft")
RegistClassMember(CLuaZhuangshiwuPreviewWnd, "m_RotateBtnRight")
RegistClassMember(CLuaZhuangshiwuPreviewWnd, "m_ResetBtn")
RegistClassMember(CLuaZhuangshiwuPreviewWnd, "m_IdentifierStr")
RegistClassMember(CLuaZhuangshiwuPreviewWnd, "m_Rotation")
RegistClassMember(CLuaZhuangshiwuPreviewWnd, "m_RotateIncrement")
RegistClassMember(CLuaZhuangshiwuPreviewWnd, "m_ModelTexture")
RegistClassMember(CLuaZhuangshiwuPreviewWnd, "m_TemplateId")
RegistClassMember(CLuaZhuangshiwuPreviewWnd, "m_NeedTopView")
RegistClassMember(CLuaZhuangshiwuPreviewWnd, "m_Previewer")
RegistClassMember(CLuaZhuangshiwuPreviewWnd, "m_Ro")

RegistClassMember(CLuaZhuangshiwuPreviewWnd,"onPinchInDelegate")
RegistClassMember(CLuaZhuangshiwuPreviewWnd,"onPinchOutDelegate")
RegistClassMember(CLuaZhuangshiwuPreviewWnd,"defaultModelLocalPos")
RegistClassMember(CLuaZhuangshiwuPreviewWnd,"localModelScale")
RegistClassMember(CLuaZhuangshiwuPreviewWnd,"maxPreviewScale")


function CLuaZhuangshiwuPreviewWnd:Awake()
    self.m_RotateBtnLeft = self.transform:Find("Anchor/Previewer/Model/RotateButton/Left").gameObject
    self.m_RotateBtnRight = self.transform:Find("Anchor/Previewer/Model/RotateButton/Right").gameObject
    self.m_ResetBtn = self.transform:Find("Anchor/Previewer/Model/ResetButton").gameObject
    self.m_Previewer = self.transform:Find("Anchor/Previewer").gameObject
    self.m_ModelTexture = self.transform:Find("Anchor/Previewer/Model/FakeModel/Texture"):GetComponent(typeof(CUITexture))
    self.m_IdentifierStr = "__jiayuanZhuangshiwuPreviewer141293__"
    
    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadModel(ro)
    end)

    UIEventListener.Get(self.m_RotateBtnLeft).onClick = DelegateFactory.VoidDelegate(function(go)
        self.m_Rotation = self.m_Rotation + self.m_RotateIncrement
        self:RotateModel()
    end)

    UIEventListener.Get(self.m_RotateBtnRight).onClick = DelegateFactory.VoidDelegate(function(go)
        self.m_Rotation = self.m_Rotation - self.m_RotateIncrement
        self:RotateModel()
    end)

    UIEventListener.Get(self.m_Previewer).onDrag = DelegateFactory.VectorDelegate(function(go, delta)
        local rot = delta.x / 3
        self.m_Rotation = self.m_Rotation - rot
        self:RotateModel()
    end)

    UIEventListener.Get(self.m_ResetBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_NeedTopView then
            self.m_Rotation = 0
        else
            self.m_Rotation = 180
        end
        self:RotateModel()
        self:SetModelPosAndScale(1)
    end)
end

function CLuaZhuangshiwuPreviewWnd:Init()
    self.m_TemplateId = CLuaZhuangshiwuPreviewMgr.TemplateId


    local topViewItems = Zhuangshiwu_Setting.GetData().TopViewPreview

    local data = Item_Item.GetData(self.m_TemplateId)
    local zswtid = CClientFurnitureMgr.GetZhuangshiwuIdFromItemData(data)
    local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(zswtid)

    self.m_NeedTopView = false
    for i = 0, topViewItems.Length-1 do
        if topViewItems[i] == zswtid or topViewItems[i] == zswdata.FurnishTemplate then
            self.m_NeedTopView = true
        end
    end

    if self.m_NeedTopView then
        self.m_Rotation = 0
        self.m_RotateIncrement = -10
    else
        self.m_Rotation = 180
        self.m_RotateIncrement = 10
    end

    self.defaultModelLocalPos = Vector3.zero
    self.defaultModelLocalPos.x, self.defaultModelLocalPos.y, self.defaultModelLocalPos.z = 0, -1.5, 7
    self.maxPreviewScale = 1.5
    self.localModelScale = 1
    self.m_ModelTexture.mainTexture = CUIManager.CreateModelTexture(self.m_IdentifierStr, self.m_ModelTextureLoader,180,self.defaultModelLocalPos.x, self.defaultModelLocalPos.y, self.defaultModelLocalPos.z,false,true,1)
end

function CLuaZhuangshiwuPreviewWnd:LoadModel(ro)
    self.m_Ro = ro
    local data = Item_Item.GetData(self.m_TemplateId)
    local zswtid = CClientFurnitureMgr.GetZhuangshiwuIdFromItemData(data)
    local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(zswtid)
    local resid = SafeStringFormat3("_%02d",zswdata.ResId)
    local prefabname = "Assets/Res/Character/Jiayuan/" .. zswdata.ResName .. "/Prefab/" .. zswdata.ResName .. resid .. ".prefab"
    ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(renderObject)
        self:OnLoadFinished(renderObject)
    end))
    ro:LoadMain(prefabname)
end

function CLuaZhuangshiwuPreviewWnd:OnLoadFinished(ro)
    self.m_Ro = ro
    self.m_Camera = ro.transform.parent.parent:GetComponent(typeof(Camera))
    local fov = 0
    if self.m_Camera~=nil then
        self.m_Camera.farClipPlane = 100
        fov = self.m_Camera.fieldOfView
    end
    local yMax = -9999
    local yMin = 9999
    local xMax = -9999
    local xMin = 9999
    local zMax = -9999
    local zMin = 9999
    for i= 0, ro.m_Renderers.Count-1 do
        local boundsYMin = ro.m_Renderers[i].bounds.min.y
        local boundsYMax = ro.m_Renderers[i].bounds.max.y
        local boundsXMin = ro.m_Renderers[i].bounds.min.x
        local boundsXMax = ro.m_Renderers[i].bounds.max.x
        local boundsZMin = ro.m_Renderers[i].bounds.min.z
        local boundsZMax = ro.m_Renderers[i].bounds.max.z
        yMax = boundsYMax > yMax and boundsYMax or yMax
        yMin = boundsYMin < yMin and boundsYMin or yMin
        xMax = boundsXMax > xMax and boundsXMax or xMax
        xMin = boundsXMin < xMin and boundsXMin or xMin
        zMax = boundsZMax > zMax and boundsZMax or zMax
        zMin = boundsZMin < zMin and boundsZMin or zMin
    end
    local ySize = yMax - yMin
    local xSize = xMax - xMin
    local zSize = zMax - zMin
    local xzSize = xSize>zSize and xSize or zSize
    local xyzSize = xzSize>ySize and xzSize or ySize

    local zDistance = 0
    if fov~=0 then
        zDistance = (xyzSize*1.3)*0.5/ math.tan(math.rad(fov * 0.5))
    else
        zDistance = 7 + xyzSize
    end
    self.defaultModelLocalPos.z = zDistance

    if self.m_NeedTopView then
        ro.transform.localRotation = Quaternion.Euler(90, 0, 0);
        self.defaultModelLocalPos.y = 0
    else
        self.defaultModelLocalPos.y = 0.2 - ySize /2
    end

    self:SetModelPosAndScale(1)
end

function CLuaZhuangshiwuPreviewWnd:RotateModel()
    if self.m_NeedTopView then
        if self.m_Rotation > 60 or self.m_Rotation<-120 then
            self.m_Rotation = 60
        elseif self.m_Rotation <-60 or self.m_Rotation>120 then
            self.m_Rotation = -60
        end
        self.m_Ro.transform.localRotation = Quaternion.Euler(90, self.m_Rotation, 0);
    else
        CUIManager.SetModelRotation(self.m_IdentifierStr, self.m_Rotation)
    end
end

function CLuaZhuangshiwuPreviewWnd:OnEnable()
    self.onPinchInDelegate = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchIn(gesture) end)
    self.onPinchOutDelegate = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchOut(gesture) end)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchIn, self.onPinchInDelegate)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchOut, self.onPinchOutDelegate)
    g_ScriptEvent:AddListener("MouseScrollWheel",self,"OnMouseScrollWheel")
end

function CLuaZhuangshiwuPreviewWnd:OnDisable()
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchIn, self.onPinchInDelegate)
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchOut, self.onPinchOutDelegate)
    g_ScriptEvent:RemoveListener("MouseScrollWheel",self,"OnMouseScrollWheel")
end

function CLuaZhuangshiwuPreviewWnd:OnDestroy()
    CUIManager.DestroyModelTexture(self.m_IdentifierStr)
    CLuaZhuangshiwuPreviewMgr:Reset()
end

function CLuaZhuangshiwuPreviewWnd:SetModelPosAndScale(scale)
    self.localModelScale = scale
    local pos = Vector3(self.defaultModelLocalPos.x, self.defaultModelLocalPos.y, self.defaultModelLocalPos.z)
    pos = Vector3(pos.x / scale, pos.y / scale, pos.z /scale)
    CUIManager.SetModelPosition(self.m_IdentifierStr, pos)
end

function CLuaZhuangshiwuPreviewWnd:OnPinchIn(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0),1)
    self:OnPinch(-pinchScale)
end

function CLuaZhuangshiwuPreviewWnd:OnPinchOut(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0),1)
    self:OnPinch(pinchScale)
end

function CLuaZhuangshiwuPreviewWnd:OnMouseScrollWheel()
    local v = Input.GetAxis("Mouse ScrollWheel")
    self:OnPinch(v)
end

function CLuaZhuangshiwuPreviewWnd:OnPinch(delta)
    local d = self.maxPreviewScale - 1
    local t = (self.localModelScale -1) /  d
    t = t + delta
    t = math.max(math.min(1,t),0)
    local s = math.lerp(1,self.maxPreviewScale,t)
    self:SetModelPosAndScale(s)
end
