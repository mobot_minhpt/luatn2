local UIDefaultTableView=import "L10.UI.UIDefaultTableView"
local CellIndexType=import "L10.UI.UITableView+CellIndexType"
local CellIndex=import "L10.UI.UITableView+CellIndex"
local UIWidget = import "UIWidget"
local UITweener = import "UITweener"
local UIRoot = import "UIRoot"
local TweenPosition = import "TweenPosition"
local MessageWndManager = import "L10.UI.MessageWndManager"
local EventDelegate = import "EventDelegate"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CUICommonDef = import "L10.UI.CUICommonDef"

CLuaHouseCompetitionWnd = class()
RegistClassMember(CLuaHouseCompetitionWnd,"masterView")
RegistClassMember(CLuaHouseCompetitionWnd,"mapView")
RegistClassMember(CLuaHouseCompetitionWnd,"expandBtn")
RegistClassMember(CLuaHouseCompetitionWnd,"simpleMasterView")
RegistClassMember(CLuaHouseCompetitionWnd,"upBtn")
RegistClassMember(CLuaHouseCompetitionWnd,"downBtn")
RegistClassMember(CLuaHouseCompetitionWnd,"communityNameLabel")
RegistClassMember(CLuaHouseCompetitionWnd,"CurRowIdx")
RegistClassMember(CLuaHouseCompetitionWnd,"tweenerList")
RegistClassMember(CLuaHouseCompetitionWnd,"HouseItems")
RegistClassMember(CLuaHouseCompetitionWnd,"mbShowSimpleMasterView")

CLuaHouseCompetitionWnd.sLeft = 1765
CLuaHouseCompetitionWnd.sRight = 15
CLuaHouseCompetitionWnd.sLeft1 = -100
CLuaHouseCompetitionWnd.sRight1 = 0
CLuaHouseCompetitionWnd.sLeft2 = -239
CLuaHouseCompetitionWnd.sRight2 = 25
CLuaHouseCompetitionWnd.sWidth = 2350

function CLuaHouseCompetitionWnd:Awake()
    self.masterView = self.transform:Find("Left/LeftPanel/Panel/Panel/MasterView"):GetComponent(typeof(UIDefaultTableView))

    self.masterView:Init(function()
            return 1
        end,
        function(section)
            if CLuaHouseCompetitionMgr.HouseList ~= nil then
                return math.ceil(#CLuaHouseCompetitionMgr.HouseList / 30) + 2
            end
            return 0
        end,
        function(cell,index)
            local label = CommonDefs.GetComponentInChildren_GameObject_Type(cell, typeof(UILabel))
            if index.type == CellIndexType.Section then
                label.text = LocalString.GetString("家园设计大赛")
            elseif index.type == CellIndexType.Row then
                if index.row == 0 then
                    label.text = LocalString.GetString("本服区")
                elseif index.row == 1 then
                    label.text = LocalString.GetString("推荐区")
                else
                    label.text = SafeStringFormat3(LocalString.GetString("设计%s区"), CUICommonDef.IntToChinese(index.row - 1))
                end
            end

        end,
        function(index,expanded)
            if index.type == CellIndexType.Section then
            end
        end,
        function(index)
            g_ScriptEvent:BroadcastInLua("OnHouseCompetitionMasterViewClick",index.row)
        end
    )
    self.masterView:LoadData(CellIndex(0, 0, CellIndexType.Row), true)


    self.mapView = self.transform:Find("Texture").gameObject
    self.expandBtn = self.transform:Find("Left/LeftPanel/ExpandButton").gameObject
    self.simpleMasterView = self.transform:Find("Left/LeftPanel/SimpleMasterView").gameObject
    self.upBtn = self.transform:Find("Left/LeftPanel/SimpleMasterView/UpButton").gameObject
    self.downBtn = self.transform:Find("Left/LeftPanel/SimpleMasterView/DownButton").gameObject
    self.communityNameLabel = self.transform:Find("Left/LeftPanel/SimpleMasterView/NameLabel"):GetComponent(typeof(UILabel))
    self.CurRowIdx = -1
    self.HouseItems = {}
    self.mbShowSimpleMasterView = false


    UIEventListener.Get(self.expandBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:onExpandBtnClicked(go)
    end)
    UIEventListener.Get(self.upBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnUpBtnClicked(go)
    end)
    UIEventListener.Get(self.downBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnDownBtnClicked(go)
    end)

    for i=1,30 do
        local name = SafeStringFormat3(LocalString.GetString("House%d"), i)
        local goTrans = self.mapView.transform:Find(name)
        if goTrans ~= nil then
            self.HouseItems[i] = goTrans.gameObject
            local sprite = self.HouseItems[i].transform:Find("Sprite")
            if sprite ~= nil then
                UIEventListener.Get(sprite.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                    self:OnSingleHouseClick(go)
                end)
            end
            self.HouseItems[i]:SetActive(false)
        end
    end


    local tweener = self.expandBtn:GetComponent(typeof(TweenPosition))
    EventDelegate.Add(tweener.onFinished, DelegateFactory.Callback(function() self:onTweenFinished() end))

    self.tweenerList = {}
    local _tweeners = CommonDefs.GetComponentsInChildren_Component_Type(self.transform, typeof(UITweener))
    do
        local i = 0
        while i < _tweeners.Length do
            table.insert( self.tweenerList, _tweeners[i])
            i = i + 1
        end
    end
    for i,v in ipairs(self.tweenerList) do
        v.enabled = false
    end
end

function CLuaHouseCompetitionWnd:Init()
    self:IPhoneXAdaptation()
    self.masterView:LoadData(CellIndex(0, 1, CellIndexType.Row), true)

    local searchButton = self.transform:Find("Right/SearchButton").gameObject
    UIEventListener.Get(searchButton).onClick = DelegateFactory.VoidDelegate(function(go)
        local inputVal = self.transform:Find("Right/Input"):GetComponent(typeof(UIInput)).value
        local id = tonumber(inputVal)
        local houseInfo = nil
        for i,v in ipairs(CLuaHouseCompetitionMgr.HouseList) do
            if v.OwnerId==id then
                houseInfo = v
                break
            end
        end
        if houseInfo then
            local msg = g_MessageMgr:FormatMessage("House_Competition_Enter_Confirm", houseInfo.Name)
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
                Gac2Gas.RequestEnterHouseCompetitionYard(houseInfo.OwnerId)
            end), nil, nil, nil, false)
        else
            g_MessageMgr:ShowMessage("SEARCH_NO_SUCH_TEAM")
        end
    end)
end

function CLuaHouseCompetitionWnd:OnEnable( )
    g_ScriptEvent:AddListener("OnHouseCompetitionMasterViewClick",self,"OnHouseCompetitionMasterViewClick")

    for i,v in ipairs(self.HouseItems) do
        v.gameObject:SetActive(false)
    end
end
function CLuaHouseCompetitionWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("OnHouseCompetitionMasterViewClick",self,"OnHouseCompetitionMasterViewClick")
end
function CLuaHouseCompetitionWnd:OnSingleHouseClick( go) 
    local idx = 0

    for i,v in ipairs(self.HouseItems) do
        local name = SafeStringFormat3(LocalString.GetString("House%d"), i)
        local goTrans = self.mapView.transform:Find(name)
        if goTrans ~= nil then
            local sprite = v.transform:Find("Sprite")
            if sprite.gameObject == go then
                idx = i
                break
            end
        end
    end

    if idx > 0 then
        if self.CurRowIdx == 0 then
            -- 本服区
            local index = 1
            for i,v in ipairs(CLuaHouseCompetitionMgr.HouseList) do
                if v.GasId == CClientMainPlayer.Inst:GetMyServerId() then
                    if idx == index then
                        local ownerId = v.OwnerId
                        local name = v.Name
                        local msg = g_MessageMgr:FormatMessage("House_Competition_Enter_Confirm", name)
                        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
                            Gac2Gas.RequestEnterHouseCompetitionYard(ownerId)
                        end), nil, nil, nil, false)
                        break
                    end
                    index = index + 1
                end
            end

        elseif self.CurRowIdx == 1 then
            -- 推荐区
            local houseListIdx = {}
            for i,v in ipairs(CLuaHouseCompetitionMgr.HouseList) do
                local bNeedAdd = true
                for j,w in ipairs(houseListIdx) do
                    if v.VoteCount > CLuaHouseCompetitionMgr.HouseList[houseListIdx[j]].VoteCount then
                        table.insert( houseListIdx, j, i)
                        bNeedAdd=false
                        break
                    end
                end
                if bNeedAdd and #houseListIdx < CLuaHouseCompetitionMgr.HouseCountInSingleCommunity then
                    table.insert( houseListIdx,i )
                end
            end
            if idx<= #houseListIdx then
                local houseInfoIdx = houseListIdx[idx]
                if #CLuaHouseCompetitionMgr.HouseList > houseInfoIdx-1 then
                    local name = CLuaHouseCompetitionMgr.HouseList[houseInfoIdx].Name
                    local msg = g_MessageMgr:FormatMessage("House_Competition_Enter_Confirm", name)
                    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
                        Gac2Gas.RequestEnterHouseCompetitionYard(CLuaHouseCompetitionMgr.HouseList[houseInfoIdx].OwnerId)
                    end), nil, nil, nil, false)
                end
            end

        else
            local startIdx = CLuaHouseCompetitionMgr.HouseCountInSingleCommunity * (self.CurRowIdx - 2)
            local houseInfoIdx = startIdx + idx

            if #CLuaHouseCompetitionMgr.HouseList > houseInfoIdx-1 then
                local name = CLuaHouseCompetitionMgr.HouseList[houseInfoIdx].Name
                local msg = g_MessageMgr:FormatMessage("House_Competition_Enter_Confirm", name)
                MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
                    Gac2Gas.RequestEnterHouseCompetitionYard(CLuaHouseCompetitionMgr.HouseList[houseInfoIdx].OwnerId)
                end), nil, nil, nil, false)
            end
        end
    end
end
function CLuaHouseCompetitionWnd:OnHouseCompetitionMasterViewClick( index) 
    self.CurRowIdx = index

    local houseListIdx = {}
    if index == 0 then
        -- 本服区
        for i,v in ipairs(CLuaHouseCompetitionMgr.HouseList) do
            if v.GasId == CClientMainPlayer.Inst:GetMyServerId() then
                table.insert( houseListIdx,i )
            end
        end

        CUICommonDef.SetActive(self.upBtn, false, true)
        CUICommonDef.SetActive(self.downBtn, true, true)
    elseif index == 1 then
        -- 推荐区
        for i,v in ipairs(CLuaHouseCompetitionMgr.HouseList) do
            local bNeedAdd = true
            for j,w in ipairs(houseListIdx) do
                if v.VoteCount > CLuaHouseCompetitionMgr.HouseList[houseListIdx[j]].VoteCount then
                    table.insert( houseListIdx, j, i)
                    bNeedAdd=false
                    break
                end
            end
            if bNeedAdd and #houseListIdx < CLuaHouseCompetitionMgr.HouseCountInSingleCommunity then
                table.insert( houseListIdx,i )
            end
        end

        CUICommonDef.SetActive(self.upBtn, true, true)
        CUICommonDef.SetActive(self.downBtn, true, true)
    else
        CUICommonDef.SetActive(self.upBtn, true, true)
        CUICommonDef.SetActive(self.downBtn, true, true)

        local startIdx = CLuaHouseCompetitionMgr.HouseCountInSingleCommunity * (index - 2)

        -- 普通设计区
        for i=1,CLuaHouseCompetitionMgr.HouseCountInSingleCommunity do
            if startIdx + i <= #CLuaHouseCompetitionMgr.HouseList then
                table.insert( houseListIdx,startIdx + i )
            end
        end
    end

    for i=1,CLuaHouseCompetitionMgr.HouseCountInSingleCommunity do
        if i>#houseListIdx then
            self.HouseItems[i]:SetActive(false)
        else
            self.HouseItems[i]:SetActive(true)
            local houseInfo = CLuaHouseCompetitionMgr.HouseList[houseListIdx[i]]
            local idxSprite = self.HouseItems[i].transform:Find("Sprite")
            if idxSprite ~= nil then
                local spriteName = "jiayuanshequ_01"
                if houseInfo.GasId == CClientMainPlayer.Inst:GetMyServerId() then
                    spriteName = "jiayuanshequ_06"
                end
                idxSprite:GetComponent(typeof(UISprite)).spriteName = spriteName
            end
            local presentCountLabel = self.HouseItems[i].transform:Find("PresentCountLabel")
            if presentCountLabel ~= nil then
                presentCountLabel:GetComponent(typeof(UILabel)).text = tostring(houseInfo.PresentCount)
            end
    
            local ownerSprite = self.HouseItems[i].transform:Find("OwnerSprite")
            if ownerSprite ~= nil then
                ownerSprite.gameObject:SetActive(houseInfo.OwnerId == CClientMainPlayer.Inst.Id)
            end
        end
    end

end


function CLuaHouseCompetitionWnd:OnUpBtnClicked( go) 
    if self.CurRowIdx>0 then
    -- if self.CurRowIdx >= 2 then
        self:OnHouseCompetitionMasterViewClick((self.CurRowIdx + CLuaHouseCompetitionMgr.HouseCountInSingleCommunity - 1) % CLuaHouseCompetitionMgr.HouseCountInSingleCommunity)
        self:UpdateCommunityNameLabel( )
    end
end
function CLuaHouseCompetitionWnd:OnDownBtnClicked( go) 
    local maxIndex = math.ceil(#CLuaHouseCompetitionMgr.HouseList / 30) + 2 - 1
    if self.CurRowIdx<maxIndex then
    -- if self.CurRowIdx >= 2 then
        self:OnHouseCompetitionMasterViewClick(((self.CurRowIdx + 1) % CLuaHouseCompetitionMgr.HouseCountInSingleCommunity))
        self:UpdateCommunityNameLabel()
    end
end
function CLuaHouseCompetitionWnd:onTweenFinished( )
    if self.mbShowSimpleMasterView then
        self.simpleMasterView:SetActive(true)
        if UIRoot.EnableIPhoneXAdaptation then
            self.masterView.transform.parent.gameObject:SetActive(false)
        end

        self:UpdateCommunityNameLabel()
    end
end
function CLuaHouseCompetitionWnd:UpdateCommunityNameLabel( )
    if self.CurRowIdx == 0 then
        self.communityNameLabel.text = LocalString.GetString("本服区")
    elseif self.CurRowIdx == 1 then
        self.communityNameLabel.text = LocalString.GetString("推荐区")
    else
        self.communityNameLabel.text = SafeStringFormat3(LocalString.GetString("设计%s区"), CUICommonDef.IntToChinese(self.CurRowIdx - 1))
    end

    -- local text = ""
    -- local communityNameList = House_Setting.GetData().Community_Name
    -- if communityNameList.Length > CCommunityMgr.Inst.CurStreetType then
    --     local communityFormat = communityNameList[CCommunityMgr.Inst.CurStreetType]
    --     print(communityFormat)
    --     -- local nameFormat = string.gsub(communityFormat, "%s", "{0}")
    --     text =SafeStringFormat3(communityFormat,EnumChineseDigits.GetDigit(CCommunityMgr.Inst.CurStreetIdx + 1))
    --     -- text = System.String.Format(nameFormat, EnumChineseDigits.GetDigit(CCommunityMgr.Inst.CurStreetIdx + 1))
    -- end
    -- self.communityNameLabel.text = text
end
function CLuaHouseCompetitionWnd:onExpandBtnClicked( go) 
    for i,v in ipairs(self.tweenerList) do
        v.enabled = true
    end


    self.mbShowSimpleMasterView = not self.mbShowSimpleMasterView
    if not self.mbShowSimpleMasterView then
        self.simpleMasterView:SetActive(false)
    end

    if self.mbShowSimpleMasterView then
        for i,v in ipairs(self.tweenerList) do
            v:PlayForward()
        end
    else
        if UIRoot.EnableIPhoneXAdaptation then
            self.masterView.transform.parent.gameObject:SetActive(true)
        end
        for i,v in ipairs(self.tweenerList) do
            v:PlayReverse()
        end
    end
end
function CLuaHouseCompetitionWnd:IPhoneXAdaptation( )
    if UIRoot.EnableIPhoneXAdaptation then
        local tf = self.transform:Find("Left")
        if tf then
            local w = tf:GetComponent(typeof(UIWidget))
            w.leftAnchor.absolute = CLuaHouseCompetitionWnd.sLeft + math.floor(UIRoot.VirtualIPhoneXWidthMargin)
            w.rightAnchor.absolute = CLuaHouseCompetitionWnd.sRight + math.floor(UIRoot.VirtualIPhoneXWidthMargin)
        end
        local tf = self.transform:Find("CloseButton")
        if tf then
            local w1 = tf:GetComponent(typeof(UIWidget))
            w1.leftAnchor.absolute = CLuaHouseCompetitionWnd.sLeft1
            w1.rightAnchor.absolute = CLuaHouseCompetitionWnd.sRight1
        end
        local tf = self.transform:Find("Texture")
        if tf then
            local tex = tf:GetComponent(typeof(UITexture))
            tex.width = CLuaHouseCompetitionWnd.sWidth
        end
    end
end

