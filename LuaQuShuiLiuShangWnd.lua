local DelegateFactory  = import "DelegateFactory"
local TweenAlpha       = import "TweenAlpha"
local Animation        = import "UnityEngine.Animation"
local CConversationMgr = import "L10.Game.CConversationMgr"
local Ease             = import "DG.Tweening.Ease"
local UIRoot           = import "UIRoot"
local RotateMode       = import "DG.Tweening.RotateMode"

LuaQuShuiLiuShangWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaQuShuiLiuShangWnd, "bg")
RegistClassMember(LuaQuShuiLiuShangWnd, "baseWnd")
RegistClassMember(LuaQuShuiLiuShangWnd, "drawWnd")
RegistClassMember(LuaQuShuiLiuShangWnd, "icons")
RegistClassMember(LuaQuShuiLiuShangWnd, "leftTop")
RegistClassMember(LuaQuShuiLiuShangWnd, "brush")
RegistClassMember(LuaQuShuiLiuShangWnd, "upLabel")
RegistClassMember(LuaQuShuiLiuShangWnd, "portrait")
RegistClassMember(LuaQuShuiLiuShangWnd, "smallPortrait")
RegistClassMember(LuaQuShuiLiuShangWnd, "part")
RegistClassMember(LuaQuShuiLiuShangWnd, "liuShuiFx1")
RegistClassMember(LuaQuShuiLiuShangWnd, "liuShuiFx2")
RegistClassMember(LuaQuShuiLiuShangWnd, "flowerFx")

RegistClassMember(LuaQuShuiLiuShangWnd, "picData")
RegistClassMember(LuaQuShuiLiuShangWnd, "hasData")
RegistClassMember(LuaQuShuiLiuShangWnd, "flowerAnimClip")
RegistClassMember(LuaQuShuiLiuShangWnd, "flowerAnim")
RegistClassMember(LuaQuShuiLiuShangWnd, "tick")
RegistClassMember(LuaQuShuiLiuShangWnd, "iconId") -- 进行到了第几个图标
RegistClassMember(LuaQuShuiLiuShangWnd, "isYouAi") -- 是否是尤艾

RegistClassMember(LuaQuShuiLiuShangWnd, "panel") -- 用于控制绘制部件逐渐显现
RegistClassMember(LuaQuShuiLiuShangWnd, "needChangePanel") -- 是否需要改变panel
RegistClassMember(LuaQuShuiLiuShangWnd, "deltaSizeY") -- 每秒size的改变值
RegistClassMember(LuaQuShuiLiuShangWnd, "startTime")
RegistClassMember(LuaQuShuiLiuShangWnd, "drawTime")

function LuaQuShuiLiuShangWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitActive()
end

-- 初始化UI组件
function LuaQuShuiLiuShangWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.baseWnd = anchor:Find("BaseWnd")
    self.drawWnd = anchor:Find("DrawWnd")

    self.icons = self.baseWnd:Find("Icons")
    self.leftTop = self.baseWnd:Find("LeftTop")
    self.flowerAnim = self.baseWnd:Find("Flower"):GetComponent(typeof(Animation))
    self.flowerFx = self.flowerAnim.transform:Find("Fx"):GetComponent(typeof(CUIFx))

    self.upLabel = self.drawWnd:Find("Label"):GetComponent(typeof(UILabel))
    self.brush = self.drawWnd:Find("Brush")

    self.part = anchor:Find("Part").gameObject
    self.portrait = self.drawWnd:Find("Portrait").gameObject
    self.panel = self.drawWnd:Find("Panel"):GetComponent(typeof(UIPanel))
    self.smallPortrait = self.leftTop:Find("Portrait").gameObject

    self.bg = self.transform:Find("BgWnd/Bg"):GetComponent(typeof(TweenAlpha))
    self.liuShuiFx1 = self.bg.transform:Find("LiuShuiFx1"):GetComponent(typeof(CUIFx))
    self.liuShuiFx2 = self.bg.transform:Find("LiuShuiFx2"):GetComponent(typeof(CUIFx))
end

-- 初始化active
function LuaQuShuiLiuShangWnd:InitActive()
    self:SetWndAvtive(false, false)
    self.leftTop.gameObject:SetActive(false)
end

-- 设置basewnd和drawwnd的active
function LuaQuShuiLiuShangWnd:SetWndAvtive(baseActive, drawActive)
    self.baseWnd.gameObject:SetActive(baseActive)
    self.drawWnd.gameObject:SetActive(drawActive)
end


function LuaQuShuiLiuShangWnd:Init()
    self:BgFadeIn()
    self:PlayWaterRunFx()

    self:InitAnim()
    self:InitIcons()
    self:InitTaskInfo()
    self:GenerateFinishedPortrait()
    self.needChangePanel = false
end

-- 初始化人物图标
function LuaQuShuiLiuShangWnd:InitIcons()
    for i = 1, self.icons.childCount do
        local child = self.icons:GetChild(i - 1)

        local matPath = SafeStringFormat3("%s.mat", WuMenHuaShi_QuShuiLiuShang.GetData(i).Icon)
        child:GetComponent(typeof(CUITexture)):LoadMaterial(matPath)
        child:Find("Select").gameObject:SetActive(false)
        child:Find("Name"):GetComponent(typeof(UILabel)).text = WuMenHuaShi_QuShuiLiuShang.GetData(i).Name
    end
end

-- 初始化任务信息
function LuaQuShuiLiuShangWnd:InitTaskInfo()
    WuMenHuaShi_QuShuiLiuShang.Foreach(function(id, data)
        if data.TaskId == LuaWuMenHuaShiMgr.qslsTaskId then
            self.iconId = id
        end
    end)

    local name = WuMenHuaShi_QuShuiLiuShang.GetData(self.iconId).Name
    self.isYouAi = name == LocalString.GetString("尤艾") and true or false
end

-- 背景图逐渐浮现
function LuaQuShuiLiuShangWnd:BgFadeIn()
    self.bg:ResetToBeginning()
	self.bg:PlayForward()
    self.bg:SetOnFinished(DelegateFactory.Callback(function ()
        self:SetWndAvtive(true, false)
        self:IconFadeIn()
	end))
end

-- 人物图标依次浮现
function LuaQuShuiLiuShangWnd:IconFadeIn()
    for i = 0, self.icons.childCount - 1 do
        local tweenAlpha = self.icons:GetChild(i):GetComponent(typeof(TweenAlpha))
        tweenAlpha:ResetToBeginning()
        tweenAlpha:PlayForward()

        if i == self.icons.childCount - 1 then
            tweenAlpha:SetOnFinished(DelegateFactory.Callback(function ()
                self.leftTop.gameObject:SetActive(true)
                self:PlayFlowerAnim()
            end))
        end
    end
end

-- 播放流水特效
function LuaQuShuiLiuShangWnd:PlayWaterRunFx()
    self.liuShuiFx1:LoadFx(g_UIFxPaths.QuShuiLiuShangLiuShuiFx1Path)
    self.liuShuiFx2:LoadFx(g_UIFxPaths.QuShuiLiuShangLiuShuiFx2Path)
end

-- 初始化动画
function LuaQuShuiLiuShangWnd:InitAnim()
    self.flowerAnimClip = "QuShuiLiuShang_Flower"
end

-- 播放花瓣动画
function LuaQuShuiLiuShangWnd:PlayFlowerAnim()
    self.flowerFx:LoadFx(g_UIFxPaths.QuShuiLiuShangHuaBanPath)
    local clip = self.flowerAnim:GetClip(self.flowerAnimClip)

    local endFrame = WuMenHuaShi_QuShuiLiuShang.GetData(self.iconId).FlowerEndFrame
    -- unity AddClip存在bug，得到的clip的长度都是1s，因此这里通过提高帧率把整个动画时长压缩在1s之内，并通过调整speed控制播放速度
    self.flowerAnim:AddClip(clip, self.flowerAnimClip, 0, endFrame)
    self.flowerAnim:get_Item(self.flowerAnimClip).time = 0
    self.flowerAnim:get_Item(self.flowerAnimClip).speed = 120 / 2000
    self.flowerAnim:Play(self.flowerAnimClip)

    self:ClearTick()
    local time = endFrame / 120
    self.tick = RegisterTickOnce(function()
        self:SetIconSelect()
        self:FlowerSwing()
    end, 1000 * time)
end

-- 设置icon为选择状态
function LuaQuShuiLiuShangWnd:SetIconSelect()
    local icon = self.icons:GetChild(self.iconId - 1)
    icon:Find("Bg").gameObject:SetActive(false)
    icon:Find("Select").gameObject:SetActive(true)

    icon:Find("Name"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24("FFFFFF", 0)
    icon:Find("Name"):Find("Bg").gameObject:SetActive(false)
    icon:Find("Name"):Find("Selected").gameObject:SetActive(true)

    icon:Find("BranchFx"):GetComponent(typeof(CUIFx)):LoadFx(g_UIFxPaths.QuShuiLiuShangHuaZhiFxPath)
end

-- 花瓣摇摆
function LuaQuShuiLiuShangWnd:FlowerSwing()
    local trans = self.flowerAnim.transform
    local rot = trans.localEulerAngles
    rot.z = rot.z + 15
    local tween = LuaTweenUtils.DOLocalRotate(trans, rot, 1, RotateMode.Fast)
    LuaTweenUtils.SetLoops(tween, -1, true)
    LuaTweenUtils.SetEase(tween, Ease.Linear)

    self:ClearTick()
    self.tick = RegisterTickOnce(function()
        self:SetWndAvtive(false, false)
        local taskDialogId = WuMenHuaShi_QuShuiLiuShang.GetData(self.iconId).BeforeDrawDialogId
        local dialogMsg = Dialog_TaskDialog.GetData(taskDialogId).Dialog
        CConversationMgr.Inst:OpenStoryDialog(dialogMsg, DelegateFactory.Action(function ()
            self:InitDrawWnd()
        end))
    end, 1000 * 3)
end

-- 初始化绘制界面
function LuaQuShuiLiuShangWnd:InitDrawWnd()
    local icon = self.drawWnd:Find("Icon")
    local matPath = SafeStringFormat3("%s.mat", WuMenHuaShi_QuShuiLiuShang.GetData(self.iconId).Icon)
    icon:GetComponent(typeof(CUITexture)):LoadMaterial(matPath)
    icon:Find("Name"):GetComponent(typeof(UILabel)).text = WuMenHuaShi_QuShuiLiuShang.GetData(self.iconId).Name
    self:InitUpLabel()
    self:InitBrush()

    self:SetWndAvtive(false, true)
end

function LuaQuShuiLiuShangWnd:InitUpLabel()
    if self.isYouAi then
        self.upLabel.text = LocalString.GetString("请在双眼位置涂抹绘制")
    else
        self.upLabel.text = LocalString.GetString("正在作画中...")
    end
end

-- 初始化画笔
function LuaQuShuiLiuShangWnd:InitBrush()
    local data = WuMenHuaShi_QuShuiLiuShang.GetData(self.iconId)

    local tbl = LuaWuMenHuaShiMgr:ParseXY(data.BrushPosition)
    self.brush.localPosition = Vector3(tbl[1].x, tbl[1].y, 0)
    local tween = LuaTweenUtils.TweenPositionX(self.brush, tbl[2].x, 1)
    LuaTweenUtils.SetLoops(tween, -1, true)
    LuaTweenUtils.SetEase(tween, Ease.Linear)

    if self.isYouAi then
        local brushTex = self.brush:Find("Texture"):GetComponent(typeof(UITexture))
        brushTex.alpha = 0.5

        local drawRegion = self.drawWnd:Find("DrawRegion").gameObject
        UIEventListener.Get(drawRegion).onDragStart = DelegateFactory.VoidDelegate(function(go)
            brushTex.alpha = 1
            LuaTweenUtils.Kill(tween, true)
        end)

        UIEventListener.Get(drawRegion).onDrag = DelegateFactory.VectorDelegate(function(go, delta)
            self:OnDrag(go, delta)
        end)

        UIEventListener.Get(drawRegion).onDragEnd = DelegateFactory.VoidDelegate(function(go)
            self:OnCompleteDraw()
            self:GenerateDrawingPortrait()
        end)
        self:ChangePanelSizeY(780)
    else
        local tweenPosY = LuaTweenUtils.TweenPositionY(self.brush, tbl[2].y, data.DrawTime)
        LuaTweenUtils.OnComplete(LuaTweenUtils.SetEase(tweenPosY, Ease.Linear), function()
            LuaTweenUtils.Kill(tween, true)
            self:OnCompleteDraw()
        end)

        self:GenerateDrawingPortrait()
        self:InitPanel(tbl[1].y, tbl[2].y, data.DrawTime)
    end
end

-- 绘制结束
function LuaQuShuiLiuShangWnd:OnCompleteDraw()
    self.brush.gameObject:SetActive(false)
    self.upLabel.gameObject:SetActive(false)

    local taskDialogId = WuMenHuaShi_QuShuiLiuShang.GetData(self.iconId).AfterDrawDialogId
    local dialogMsg = Dialog_TaskDialog.GetData(taskDialogId).Dialog
    CConversationMgr.Inst:OpenStoryDialog(dialogMsg, DelegateFactory.Action(function ()
        local taskId = LuaWuMenHuaShiMgr.qslsTaskId
        local event = WuMenHuaShi_QuShuiLiuShang.GetData(self.iconId).FinishEvent
        LuaWuMenHuaShiMgr:SendFinishEvent(taskId, event, false)

        self:SetWndAvtive(false, false)
        self.bg.from, self.bg.to = self.bg.to, self.bg.from
        self.bg.duration = 1
        self.bg:ResetToBeginning()
        self.bg:PlayForward()
        self.bg:SetOnFinished(DelegateFactory.Callback(function ()
            CUIManager.CloseUI(CLuaUIResources.QuShuiLiuShangWnd)
        end))
    end))
end

-- 生成已完成的画像
function LuaQuShuiLiuShangWnd:GenerateFinishedPortrait()
    self.picData = {}
    self.hasData = false
    for i = 1, self.iconId - 1 do
        local choice = WuMenHuaShi_QuShuiLiuShang.GetData(i).Choice
        self:Parse2PicData(LuaWuMenHuaShiMgr:ParseXY(choice))
    end

    if self.hasData then
        local sexType = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiSexType
        self.picData[sexType] = 1
        LuaWuMenHuaShiMgr:GeneratePortrait(self.portrait, self.part, self.picData)
        LuaWuMenHuaShiMgr:GeneratePortrait(self.smallPortrait, self.part, self.picData)
    end
end

-- 生成画的部分
function LuaQuShuiLiuShangWnd:GenerateDrawingPortrait()
    self.picData = {}
    if not self.hasData then
        local sexType = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiSexType
        self.picData[sexType] = 1
    end

    local choice = WuMenHuaShi_QuShuiLiuShang.GetData(self.iconId).Choice
    self:Parse2PicData(LuaWuMenHuaShiMgr:ParseXY(choice))
    LuaWuMenHuaShiMgr:GeneratePortrait(self.panel.gameObject, self.part, self.picData)
end

-- 解析成picData
function LuaQuShuiLiuShangWnd:Parse2PicData(tbl)
    if not tbl then return end
    for i = 1, #tbl do
        local type = tbl[i].x
        local choice = tbl[i].y

        self.picData[type] = choice
    end
    self.hasData = true
end

-- panel 初始化
function LuaQuShuiLiuShangWnd:InitPanel(startY, endY, drawTime)
    local softnessY = self.panel.clipSoftness.y
    self:ChangePanelOffsetY(startY + softnessY)
    self:ChangePanelSizeY(0.01)

    self.drawTime = drawTime
    self.startTime = Time.realtimeSinceStartup
    self.deltaSizeY = (startY - endY + softnessY) * 2 / drawTime
    self.needChangePanel = true
end

-- 用于panel显示
function LuaQuShuiLiuShangWnd:Update()
    if not self.needChangePanel then return end

    local time = Time.realtimeSinceStartup - self.startTime
    self:ChangePanelSizeY(time * self.deltaSizeY + 0.01)

    if time >= self.drawTime then
        self.needChangePanel = false
    end
end

-- 改变panel的offset Y
function LuaQuShuiLiuShangWnd:ChangePanelOffsetY(y)
    local clipRegion = self.panel.baseClipRegion
    clipRegion.y = y
    self.panel.baseClipRegion = clipRegion
end

-- 改变panel的size Y
function LuaQuShuiLiuShangWnd:ChangePanelSizeY(y)
    local clipRegion = self.panel.baseClipRegion
    clipRegion.w = y
    self.panel.baseClipRegion = clipRegion
end

-- 清除计时器
function LuaQuShuiLiuShangWnd:ClearTick()
    if self.tick then
		UnRegisterTick(self.tick)
		self.tick = nil
	end
end

function LuaQuShuiLiuShangWnd:OnDestroy()
    self:ClearTick()

    LuaTweenUtils.DOKill(self.brush)
end

--@region UIEvent
function LuaQuShuiLiuShangWnd:OnDrag(go, delta)
    local scale = UIRoot.GetPixelSizeAdjustment(go)
    local pos = self.brush.localPosition
    pos.x = pos.x + delta.x * scale
    pos.y = pos.y + delta.y * scale
    self.brush.localPosition = pos
end


--@endregion UIEvent
