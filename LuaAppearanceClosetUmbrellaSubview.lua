local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local EnumGender = import "L10.Game.EnumGender"
local UISprite = import "UISprite"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaAppearanceClosetUmbrellaSubview = class()

RegistChildComponent(LuaAppearanceClosetUmbrellaSubview,"m_ClosetUmbrellaItem", "CommonClosetSingleLineItemCell", GameObject)
RegistChildComponent(LuaAppearanceClosetUmbrellaSubview,"m_ItemDisplay", "AppearanceCommonButtonDisplayView", CCommonLuaScript)
RegistChildComponent(LuaAppearanceClosetUmbrellaSubview,"m_ContentTable", "ContentTable", UITable)
RegistChildComponent(LuaAppearanceClosetUmbrellaSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)

RegistClassMember(LuaAppearanceClosetUmbrellaSubview, "m_MyUmbrellas")
RegistClassMember(LuaAppearanceClosetUmbrellaSubview, "m_SelectedDataId")

function LuaAppearanceClosetUmbrellaSubview:Awake()
end

function LuaAppearanceClosetUmbrellaSubview:Init()
    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceClosetUmbrellaSubview:LoadData()
    self.m_MyUmbrellas = LuaAppearancePreviewMgr:GetAllUmbrellaInfo(true)
    Extensions.RemoveAllChildren(self.m_ContentTable.transform)
    self.m_ContentTable.gameObject:SetActive(true)

    for i = 1, # self.m_MyUmbrellas do
        local child = CUICommonDef.AddChild(self.m_ContentTable.gameObject, self.m_ClosetUmbrellaItem)
        child:SetActive(true)
        self:InitItem(child, self.m_MyUmbrellas[i])
    end
    self.m_ContentTable:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceClosetUmbrellaSubview:SetDefaultSelection()
    self.m_SelectedDataId = LuaAppearancePreviewMgr:GetCurrentInUseUmbrella()
end

function LuaAppearanceClosetUmbrellaSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end

    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        local appearanceData = self.m_MyUmbrellas[i+1]
        if appearanceData.id == self.m_SelectedDataId then
            self:OnItemClick(childGo)
            break
        end
    end
end

function LuaAppearanceClosetUmbrellaSubview:InitItem(itemGo, appearanceData)
    local iconTexture = itemGo.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local cornerGo = itemGo.transform:Find("Item/Corner").gameObject
    local disabledGo = itemGo.transform:Find("Item/Disabled").gameObject
    local nameLabel = itemGo.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local conditionLabel = itemGo.transform:Find("ConditionLabel"):GetComponent(typeof(UILabel))
    if CClientMainPlayer.Inst then
        iconTexture:LoadMaterial(CClientMainPlayer.Inst.Gender == EnumGender.Male and appearanceData.icon or appearanceData.femaleIcon)
    else
        iconTexture:Clear()
    end
    itemGo:GetComponent(typeof(CButton)).Selected = (self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id or false)
    cornerGo:SetActive(LuaAppearancePreviewMgr:IsCurrentInUseUmbrella(appearanceData.id))
    disabledGo:SetActive(LuaAppearancePreviewMgr:IsUmbrellaExpired(appearanceData.id))
    nameLabel.text = appearanceData.name
    conditionLabel.text = LuaAppearancePreviewMgr:GetUmbrellaConditionText(appearanceData.id)

    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go)
    end)
end

function LuaAppearanceClosetUmbrellaSubview:OnItemClick(go)
    local expressionId = 0
    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:GetComponent(typeof(CButton)).Selected = true
            self.m_SelectedDataId = self.m_MyUmbrellas[i+1].id
            expressionId = self.m_MyUmbrellas[i+1].expressionId
        else
            childGo.transform:GetComponent(typeof(CButton)).Selected = false
        end
    end
    self:UpdateButtonsDisplay()
    g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerUmbrella", expressionId)
end

function LuaAppearanceClosetUmbrellaSubview:UpdateButtonsDisplay()
    if self.m_SelectedDataId == 0 then
        self.m_ItemDisplay.gameObject:SetActive(false)
        return
    end
    self.m_ItemDisplay.gameObject:SetActive(true)
    local buttonTbl = {}
    local exist = self.m_SelectedDataId and LuaAppearancePreviewMgr:MainPlayerHasUmbrella(self.m_SelectedDataId) or false
    local inUse = self.m_SelectedDataId and LuaAppearancePreviewMgr:IsCurrentInUseUmbrella(self.m_SelectedDataId) or false
    local expired = exist and LuaAppearancePreviewMgr:IsUmbrellaExpired(self.m_SelectedDataId) or false
    if exist and not inUse and not expired then
        table.insert(buttonTbl, {text=LocalString.GetString("更换"), isYellow=false, action=function(go) self:OnApplyButtonClick() end})
    end
    if self.m_SelectedDataId>0 and not exist or expired then
        if expired then
            table.insert(buttonTbl, {text=LocalString.GetString("丢弃"), isYellow=false, action=function(go) self:OnDiscardButtonClick() end})
        else
            table.insert(buttonTbl, {text=LocalString.GetString("获取"), isYellow=true, action=function(go) self:OnRenewalButtonClick(go) end})
        end
    end
    self.m_ItemDisplay:Init(buttonTbl)
end

function LuaAppearanceClosetUmbrellaSubview:OnApplyButtonClick()
    local exist = LuaAppearancePreviewMgr:MainPlayerHasUmbrella(self.m_SelectedDataId)
    if exist then
        LuaAppearancePreviewMgr:RequestSetUmbrella(self.m_SelectedDataId)
    end
end

function LuaAppearanceClosetUmbrellaSubview:OnDiscardButtonClick()
    local exist = LuaAppearancePreviewMgr:MainPlayerHasUmbrella(self.m_SelectedDataId)
    if exist then
        LuaAppearancePreviewMgr:RequestDiscardUmbrella(self.m_SelectedDataId)
    end
end

function LuaAppearanceClosetUmbrellaSubview:OnRenewalButtonClick(go)
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    local appearanceData = nil
    for i=1,#self.m_MyUmbrellas do
        if self.m_MyUmbrellas[i].id == id then
            appearanceData = self.m_MyUmbrellas[i]
            break
        end
    end
    if appearanceData then
        LuaItemAccessListMgr:ShowItemAccessInfoAtLeft(appearanceData.itemGetId, true, go.transform)
    end
end

function LuaAppearanceClosetUmbrellaSubview:OnEnable()
    g_ScriptEvent:AddListener("UpdatePlayerExpressionAppearance", self, "OnUpdatePlayerExpressionAppearance")
end

function LuaAppearanceClosetUmbrellaSubview:OnDisable()
    g_ScriptEvent:RemoveListener("UpdatePlayerExpressionAppearance", self, "OnUpdatePlayerExpressionAppearance")
end

function LuaAppearanceClosetUmbrellaSubview:OnUpdatePlayerExpressionAppearance(args)
    local exist = LuaAppearancePreviewMgr:MainPlayerHasUmbrella(self.m_SelectedDataId)
    if not exist then
        self:SetDefaultSelection()
    end
    self:LoadData()
end
