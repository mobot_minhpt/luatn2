local CUITexture = import "L10.UI.CUITexture"
local BoxCollider = import "UnityEngine.BoxCollider"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UIProgressBar = import "UIProgressBar"

LuaOlympicGamePlayWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaOlympicGamePlayWnd, "TopLabel", "TopLabel", UILabel)
RegistChildComponent(LuaOlympicGamePlayWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaOlympicGamePlayWnd, "Progress", "Progress", UIProgressBar)
RegistChildComponent(LuaOlympicGamePlayWnd, "HpLabel", "HpLabel", UILabel)
RegistChildComponent(LuaOlympicGamePlayWnd, "BossNameLabel", "BossNameLabel", UILabel)
RegistChildComponent(LuaOlympicGamePlayWnd, "NumLabel1", "NumLabel1", UILabel)
RegistChildComponent(LuaOlympicGamePlayWnd, "NumLabel2", "NumLabel2", UILabel)
RegistChildComponent(LuaOlympicGamePlayWnd, "NumLabel3", "NumLabel3", UILabel)
RegistChildComponent(LuaOlympicGamePlayWnd, "NumLabel4", "NumLabel4", UILabel)
RegistChildComponent(LuaOlympicGamePlayWnd, "NumLabel5", "NumLabel5", UILabel)
RegistChildComponent(LuaOlympicGamePlayWnd, "Button1", "Button1", BoxCollider)
RegistChildComponent(LuaOlympicGamePlayWnd, "Button2", "Button2", BoxCollider)
RegistChildComponent(LuaOlympicGamePlayWnd, "Button3", "Button3", BoxCollider)
RegistChildComponent(LuaOlympicGamePlayWnd, "Button4", "Button4", BoxCollider)
RegistChildComponent(LuaOlympicGamePlayWnd, "Button5", "Button5", BoxCollider)
RegistChildComponent(LuaOlympicGamePlayWnd, "BossIcon", "BossIcon", CUITexture)
RegistChildComponent(LuaOlympicGamePlayWnd, "Fx", "Fx", CUIFx)

RegistClassMember(LuaOlympicGamePlayWnd,"m_BossFullHp")
RegistClassMember(LuaOlympicGamePlayWnd,"m_MapIdArray")
RegistClassMember(LuaOlympicGamePlayWnd,"m_ButtonArray")
RegistClassMember(LuaOlympicGamePlayWnd,"m_NumLabelArray")
--@endregion RegistChildComponent end

function LuaOlympicGamePlayWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

    --@endregion EventBind end
end

function LuaOlympicGamePlayWnd:Init()
	self.TopLabel.text = g_MessageMgr:FormatMessage("OlympicGamePlayWnd_TopLabelText")
	local setting = ShuJia2021_OlympicBoss_Setting.GetData()
	local monserData = Monster_Monster.GetData(setting.BossMonsterId)
	if monserData then
		self.BossNameLabel.text = monserData.Name
	end
	self.m_BossFullHp = setting.BossFullHp
	self.m_MapIdArray = {16000008,16000009,16000014,16000015,16000016}
	self.m_ButtonArray = {self.Button1,self.Button2,self.Button3,self.Button4,self.Button5}
	self.m_NumLabelArray = {self.NumLabel1,self.NumLabel2,self.NumLabel3,self.NumLabel4,self.NumLabel5}
	for index, btn in pairs(self.m_ButtonArray) do
		UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			Gac2Gas.TrackToOlympicBossEntranceNpc(self.m_MapIdArray[index])
		end)
	end
	self.Fx:LoadFx("fx/ui/prefab/UI_shuqiaoyunshijieBOSS.prefab")
	Gac2Gas.RequestOlympicBossInfo()
end

--@region UIEvent

function LuaOlympicGamePlayWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("OlympicGamePlayWnd_ReadMe")
end

--@endregion UIEvent

function LuaOlympicGamePlayWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncOlympicBossHp", self, "OnSyncOlympicBossHp")
	g_ScriptEvent:AddListener("SyncOlympicBossInfo", self, "OnSyncOlympicBossInfo")
end

function LuaOlympicGamePlayWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncOlympicBossHp", self, "OnSyncOlympicBossHp")
	g_ScriptEvent:RemoveListener("SyncOlympicBossInfo", self, "OnSyncOlympicBossInfo")
end

function LuaOlympicGamePlayWnd:OnSyncOlympicBossHp(bossEngineId, bossHp)
	self.HpLabel.text = bossHp
	self.Progress.value = bossHp / self.m_BossFullHp
end

function LuaOlympicGamePlayWnd:OnSyncOlympicBossInfo(currentBossHp, mapId2CountDict)
	self.HpLabel.text = currentBossHp
	self.Progress.value = currentBossHp / self.m_BossFullHp
	for index, label in pairs(self.m_NumLabelArray) do
		local mapId = self.m_MapIdArray[index]
		local count = mapId2CountDict[mapId] and mapId2CountDict[mapId] or 0
		label.text = count
		label.color = count == 0 and Color.red or Color.white
		local btn = self.m_ButtonArray[index]
		Extensions.SetLocalPositionZ(btn.transform, count == 0 and -1 or 0)
		btn.enabled = count > 0
	end
end
