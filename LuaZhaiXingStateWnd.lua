local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local CScene = import "L10.Game.CScene"
local CGamePlayMgr=import "L10.Game.CGamePlayMgr"
local TweenAlpha = import "TweenAlpha"
local Ease = import "DG.Tweening.Ease"
local RenderTexture = import "UnityEngine.RenderTexture"
local Screen = import "UnityEngine.Screen"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local TextureWrapMode = import "UnityEngine.TextureWrapMode"
local FilterMode = import "UnityEngine.FilterMode"
local CMainCamera = import "L10.Engine.CMainCamera"
local Color = import "UnityEngine.Color"
local RenderSettings = import "UnityEngine.RenderSettings"
local CUIFx = import "L10.UI.CUIFx"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local ParticleSystem = import "UnityEngine.ParticleSystem"
local UIRoot = import "UIRoot"
local Constants = import "L10.Game.Constants"
local Rect = import "UnityEngine.Rect"

LuaZhaiXingStateWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaZhaiXingStateWnd, "m_ScoreLab", "Score", UILabel)
RegistChildComponent(LuaZhaiXingStateWnd, "m_RemainTimeLabel", "RemainTimeLabel", UILabel)
RegistChildComponent(LuaZhaiXingStateWnd, "m_LeaveButton", "LeaveButton", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaZhaiXingStateWnd, "m_TweenAlpha")
RegistClassMember(LuaZhaiXingStateWnd, "m_Duration")
RegistClassMember(LuaZhaiXingStateWnd, "m_Tweener")
RegistClassMember(LuaZhaiXingStateWnd, "m_WhiteColor")
RegistClassMember(LuaZhaiXingStateWnd, "m_RedColor")
RegistClassMember(LuaZhaiXingStateWnd, "m_Score")
RegistClassMember(LuaZhaiXingStateWnd, "m_CachedSkybox")

RegistClassMember(LuaZhaiXingStateWnd, "m_Stage1")
RegistClassMember(LuaZhaiXingStateWnd, "m_Stage2")
RegistClassMember(LuaZhaiXingStateWnd, "m_Stage1Tex1")
RegistClassMember(LuaZhaiXingStateWnd, "m_Stage1Tex2")
RegistClassMember(LuaZhaiXingStateWnd, "m_Stage1Tex3")
RegistClassMember(LuaZhaiXingStateWnd, "m_LastPhase")
RegistClassMember(LuaZhaiXingStateWnd, "m_PhaseTweener")
RegistClassMember(LuaZhaiXingStateWnd, "m_Colors")
RegistClassMember(LuaZhaiXingStateWnd, "m_MoveFx")
RegistClassMember(LuaZhaiXingStateWnd, "m_NameToOriSpeed")
RegistClassMember(LuaZhaiXingStateWnd, "m_Tick")
RegistClassMember(LuaZhaiXingStateWnd, "m_Stage2Fx1")
RegistClassMember(LuaZhaiXingStateWnd, "m_Stage2Fx2")
RegistClassMember(LuaZhaiXingStateWnd, "m_Stage2Fx3")
RegistClassMember(LuaZhaiXingStateWnd, "m_IsWinSocialWndOpened")

function LuaZhaiXingStateWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitComponents()
    self.m_TweenAlpha = self.m_RemainTimeLabel:GetComponent(typeof(TweenAlpha))
    self.m_Duration = 0.3
    self.m_WhiteColor = NGUIText.ParseColor24("FFFFFF", 0)
    self.m_RedColor = NGUIText.ParseColor24("FF5050", 0)
    self.m_ScoreLab.text = LuaZhaiXingMgr.m_Score
    self.m_Score = LuaZhaiXingMgr.m_Score
    self.m_RemainTimeLabel.text = self:GetRemainTimeText(0)
    self.m_LastPhase = 1

    self.m_Colors = {NGUIText.ParseColor32("283E897F", 0),
                    NGUIText.ParseColor32("622ECB7F", 0),
                    NGUIText.ParseColor32("FF56757F", 0),
                    NGUIText.ParseColor32("FF88C380", 0),}
    self:IPhoneXAdaptation()
end

function LuaZhaiXingStateWnd:InitComponents()
    self.m_GameTexture  = self.transform:Find("LayerMiddle/GameTexture"):GetComponent(typeof(UITexture))
    self.m_Stage1       = self.transform:Find("LayerBottom/Stage1").gameObject
    self.m_Stage2       = self.transform:Find("LayerBottom/Stage2").gameObject
    self.m_Stage1Tex1   = self.transform:Find("LayerBottom/Stage1/Texture1"):GetComponent(typeof(UITexture))
    self.m_Stage1Tex2   = self.transform:Find("LayerBottom/Stage1/Texture2"):GetComponent(typeof(UITexture))
    self.m_Stage1Tex3   = self.transform:Find("LayerBottom/Stage1/Texture3"):GetComponent(typeof(UITexture))
    self.m_Stage2Tex    = self.transform:Find("LayerBottom/Stage2/Texture"):GetComponent(typeof(UITexture))
    self.m_MoveFx       = self.transform:Find("LayerBottom/Stage1/BgFx"):GetComponent(typeof(CUIFx))
    self.m_Stage2Fx1    = self.transform:Find("LayerBottom/Stage2/fx1"):GetComponent(typeof(CUIFx))
    self.m_Stage2Fx2    = self.transform:Find("LayerBottom/Stage2/fx2"):GetComponent(typeof(CUIFx))
    self.m_Stage2Fx3    = self.transform:Find("LayerBottom/Stage2/fx3"):GetComponent(typeof(CUIFx))

    self.m_Stage2Fx1.gameObject:SetActive(false)
    self.m_Stage2Fx2.gameObject:SetActive(false)
    self.m_Stage2Fx3.gameObject:SetActive(false)
end

function LuaZhaiXingStateWnd:Init()
    self:ChangeStage(LuaZhaiXingMgr.m_stage or 1)
    LuaZhaiXingMgr:StartZhaiXing()
    self:InitGameTexture()
    UIEventListener.Get(self.m_LeaveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(p)		
		CGamePlayMgr.Inst:LeavePlay()
    end)

    self.m_MoveFx.OnLoadFxFinish = DelegateFactory.Action(function()
        self:OnMoveFxLoaded()
        self:SetFxSpeedRate(LuaZhaiXingMgr.m_SpeedRate)
        --self.m_FxNeedHide = self.m_MoveFx.transform:GetChild(0):GetChild(0):GetChild(0):GetComponent(typeof(Renderer))
    end)

    UnRegisterTick(self.m_Tick)
    self.m_Tick = RegisterTick(function()
        self:OnTick()
    end, 1000)
end

--@region UIEvent

--@endregion UIEvent

function LuaZhaiXingStateWnd:OnEnable()
    CMainCamera.Inst:SetCullingGroup(false)
	g_ScriptEvent:AddListener("CanLeaveGamePlay", self, "OnCanLeaveGamePlay")
    g_ScriptEvent:AddListener("SyncZhaiXingScore", self, "SyncZhaiXingScore")
    g_ScriptEvent:AddListener("SyncZhaiXingComponent", self, "SyncZhaiXingComponent")
    g_ScriptEvent:AddListener("ZhaiXingSpeedRateChange", self, "OnZhaiXingSpeedRateChange")
    g_ScriptEvent:AddListener("MainPlayerCreated", self, "OnMainPlayCreated")
    local scaler = CMainCamera.Inst.m_ResolutionScaler
    if scaler then
        scaler.IsCaptureing = true
    end
end

function LuaZhaiXingStateWnd:OnDisable()
    CMainCamera.Inst:SetCullingGroup(true)
	g_ScriptEvent:RemoveListener("CanLeaveGamePlay", self, "OnCanLeaveGamePlay")
    g_ScriptEvent:RemoveListener("SyncZhaiXingScore", self, "SyncZhaiXingScore")
    g_ScriptEvent:RemoveListener("SyncZhaiXingComponent", self, "SyncZhaiXingComponent")
    g_ScriptEvent:RemoveListener("ZhaiXingSpeedRateChange", self, "OnZhaiXingSpeedRateChange")
    g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "OnMainPlayCreated")
    local scaler = CMainCamera.Inst.m_ResolutionScaler
    if scaler then
        scaler.IsCaptureing = false
    end
end

function LuaZhaiXingStateWnd:OnTick()
    local endTime = LuaZhaiXingMgr.m_FightEndTime - CServerTimeMgr.Inst.timeStamp

    if endTime < 0 then
        endTime = 0
    end

    self:OnTimeUpdate(endTime)
end

function LuaZhaiXingStateWnd:OnTimeUpdate(endTime)
    local bTo10s = endTime ~= 0 and endTime <= 10 
    self.m_RemainTimeLabel.text = SafeStringFormat3(LocalString.GetString("%s"), self:GetRemainTimeText(endTime))
    self.m_RemainTimeLabel.color = bTo10s and self.m_RedColor or self.m_WhiteColor
    self.m_TweenAlpha.enabled = bTo10s
    self:ProcessPhase(endTime)
end

function LuaZhaiXingStateWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3(LocalString.GetString("%02d:%02d:%02d"), math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3(LocalString.GetString("%02d:%02d"), math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaZhaiXingStateWnd:OnCanLeaveGamePlay()
    if CScene.MainScene then
        self.m_LeaveButton.gameObject:SetActive(CScene.MainScene.ShowLeavePlayButton)
    end
end

function LuaZhaiXingStateWnd:SyncZhaiXingScore(score, fightEndTime)
    self:RefreshScore(self.m_Score, score)
end

function LuaZhaiXingStateWnd:SyncZhaiXingComponent()
    self:ChangeStage(LuaZhaiXingMgr.m_stage)
end

function LuaZhaiXingStateWnd:ChangeStage(stage)
    if stage == nil then
        return
    end

    self.m_Stage1:SetActive(stage == 1)
    self.m_Stage2:SetActive(stage == 2)

    if stage == 1 then
        LuaZhaiXingMgr:ChangeColor(self.m_Colors[1])
    elseif stage == 2 then
        LuaZhaiXingMgr:ChangeColor(self.m_Colors[4])
    end
end

function LuaZhaiXingStateWnd:ProcessPhase(time)
    if LuaZhaiXingMgr.m_stage == 1 then
        self:ProcessPhase1(time)
    elseif LuaZhaiXingMgr.m_stage == 2 then
        self:ProcessPhase2(time)
    end
end

function LuaZhaiXingStateWnd:ProcessPhase1(time)
    if time == 0 then
        return
    end

    local curPhase = 1
    if LuaZhaiXingMgr.m_TotalTime + LuaZhaiXingMgr.m_PrepareTime - 120 > time then
        curPhase = 3
    elseif LuaZhaiXingMgr.m_TotalTime + LuaZhaiXingMgr.m_PrepareTime - 60 > time then
        curPhase = 2
    end

    if self.m_LastPhase ~= curPhase then
        self:ChangePhase1(curPhase)
    end
end

function LuaZhaiXingStateWnd:ProcessPhase2(time)
    if time == 0 then
        return
    end

    local curPhase = 1
    if LuaZhaiXingMgr.m_TotalTime + LuaZhaiXingMgr.m_PrepareTime - 120 > time then
        curPhase = 4
    elseif LuaZhaiXingMgr.m_TotalTime + LuaZhaiXingMgr.m_PrepareTime - 70 > time then
        curPhase = 3
    elseif LuaZhaiXingMgr.m_TotalTime + LuaZhaiXingMgr.m_PrepareTime - 20 > time then
        curPhase = 2 
    end

    if self.m_LastPhase ~= curPhase then
        self:ChangePhase2(curPhase)
    end
end

function LuaZhaiXingStateWnd:ChangePhase1(phase)
    self.m_Stage1Tex1.alpha = phase == 3 and 0 or 1
    self.m_Stage1Tex2.alpha = 1
    self.m_Stage1Tex3.alpha = 1
    LuaTweenUtils.Kill(self.m_PhaseTweener , false)
    local startColor = self.m_Colors[self.m_LastPhase]
    local endColor =  self.m_Colors[phase]
    self.m_PhaseTweener = LuaTweenUtils.TweenFloat(0, 1, 10, function(val)
        if phase == 2 then
            self.m_Stage1Tex1.alpha = 1 - val
        elseif phase == 3 then
            self.m_Stage1Tex2.alpha = 1 - val
        end
        local color = Color(math.lerp(startColor.r, endColor.r, val),
                        math.lerp(startColor.g, endColor.g, val),
                        math.lerp(startColor.b, endColor.b, val),
                        math.lerp(startColor.a, endColor.a, val))
        LuaZhaiXingMgr:ChangeColor(color)
    end)
    
    self.m_LastPhase = phase
end

function LuaZhaiXingStateWnd:ChangePhase2(phase)
    if phase == 1 then
        return
    end

    local fxName = "m_Stage2Fx".. (phase - 1)
    local fx = self[fxName]
    -- local tickName = "m_TickFx" .. (phase - 1)

    -- local time = 10
    -- if phase == 2 then
    --     time = 30
    -- elseif phase == 3 then
    --     time = 60
    -- elseif phase == 4 then
    --     time = 40
    -- end

    fx.gameObject:SetActive(true)
    fx:Reset()
    -- UnRegisterTick(self[tickName])
    -- self[tickName] = RegisterTick(function()
    --     fx:DestroyFx()
    -- end, 1000 * time)

    self.m_LastPhase = phase
end

function LuaZhaiXingStateWnd:RefreshScore(oldScore, newScore)
    LuaTweenUtils.Kill(self.m_Tweener, true)
    self.m_Tweener = LuaTweenUtils.TweenFloat(oldScore, newScore, self.m_Duration, function ( val )
		self.m_ScoreLab.text = tonumber(SafeStringFormat3(LocalString.GetString("%.f"), val))
	end)
	LuaTweenUtils.SetEase(self.m_Tweener, Ease.Linear)
    self.m_Score = newScore
end

function LuaZhaiXingStateWnd:OnDestroy()
    UnRegisterTick(self.m_Tick)
    LuaTweenUtils.Kill(self.m_Tweener, true)
    LuaTweenUtils.Kill(self.m_PhaseTweener , false)
    LuaZhaiXingMgr:EndZhaiXing()
    self:DestroyGameTexture()
    -- UnRegisterTick(self.m_TickFx1)
    -- UnRegisterTick(self.m_TickFx2)
    -- UnRegisterTick(self.m_TickFx3)
    LuaZhaiXingMgr.m_FightEndTime = 0
end

function LuaZhaiXingStateWnd:InitGameTexture()
    if CMainCamera.CaptureCamera == nil then
        return
    end

    local percent = 1
    if CommonDefs.Is_PC_PLATFORM() then
        if CWinSocialWndMgr == nil then
            CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
        end
        if CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
            percent = (1 - Constants.WinSocialWndRatio)
        end
        
        self.m_Width = Screen.width
        self.m_Height = Screen.height
        self.m_IsWinSocialWndOpened = CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened 
    end
    local rt = CreateFromClass(RenderTexture, Screen.width * percent, Screen.height, 24 , RenderTextureFormat.ARGB32, RenderTextureReadWrite.Default)
    rt.wrapMode = TextureWrapMode.Clamp
    rt.filterMode = FilterMode.Bilinear
    rt.anisoLevel = 2
    rt.name = "ZhaiXing_GameRT"

    self.m_CachedSkybox = RenderSettings.skybox
    RenderSettings.skybox = nil
    
    if not self.m_OldHeight then
        self.m_OldHeight = CMainCamera.Inst.m_Height
    end
    CMainCamera.Inst.m_Height = CMainCamera.Inst.m_OriScreenHeight
    CMainCamera.CaptureCamera.rect = CreateFromClass(Rect, 0, 0, 1, 1)
    CMainCamera.CaptureCamera.targetTexture = rt
    CMainCamera.CaptureCamera.farClipPlane = 6000
    self.m_GameTexture.mainTexture = rt
end


function LuaZhaiXingStateWnd:DestroyGameTexture()
    if RenderSettings.skybox == nil then
        RenderSettings.skybox = self.m_CachedSkybox
    end
    if CMainCamera.CaptureCamera then
        if self.m_OldHeight then
            CMainCamera.Inst.m_Height = self.m_OldHeight
        end
        CMainCamera.CaptureCamera.targetTexture = nil
        local percent = 1
        if CommonDefs.Is_PC_PLATFORM() then
            if CWinSocialWndMgr == nil then
                CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
            end
            if CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
                percent = (1 - Constants.WinSocialWndRatio)
            end
        end
        CMainCamera.CaptureCamera.rect = CreateFromClass(Rect, 0, 0, percent, 1)
        CMainCamera.CaptureCamera.backgroundColor = Color.black
        CMainCamera.CaptureCamera.farClipPlane = 600
    end
    if self.m_GameTexture then
        GameObject.Destroy(self.m_GameTexture)
    end
end

function LuaZhaiXingStateWnd:OnMoveFxLoaded()
    local particles = CommonDefs.GetComponentsInChildren_Component_Type(self.m_MoveFx, typeof(ParticleSystem))
    if particles then
        self.m_NameToOriSpeed = {}
        for i= 0, particles.Length-1 do
            local particle = particles[i]
            self.m_NameToOriSpeed[particle.gameObject.name] = CommonDefs.GetParticleSystemSpeed(particle)
        end
    end
    -- body
end

function LuaZhaiXingStateWnd:SetFxSpeedRate(rate)
    local particles = CommonDefs.GetComponentsInChildren_Component_Type(self.m_MoveFx, typeof(ParticleSystem))
    if particles then
        for i= 0, particles.Length-1 do
            local particle = particles[i]
            CommonDefs.SetParticleSystemSpeed(particle, self.m_NameToOriSpeed[particle.gameObject.name] * rate)
        end
    end
end

function LuaZhaiXingStateWnd:OnZhaiXingSpeedRateChange()
    self:SetFxSpeedRate(LuaZhaiXingMgr.m_SpeedRate)
end

function LuaZhaiXingStateWnd:OnMainPlayCreated()
    LuaZhaiXingMgr:StartZhaiXing()
end

function LuaZhaiXingStateWnd:IPhoneXAdaptation()
    if UIRoot.EnableIPhoneXAdaptation then
        if CommonDefs.IsAndroidPlatform() then return end
        self:ProcessWidget(self.m_GameTexture)
        self:ProcessWidget(self.m_Stage1Tex1)
        self:ProcessWidget(self.m_Stage1Tex2)
        self:ProcessWidget(self.m_Stage1Tex3)
        self:ProcessWidget(self.m_Stage2Tex)
    end
end

function LuaZhaiXingStateWnd:ProcessWidget(w)
    w.leftAnchor.absolute = - math.floor(UIRoot.VirtualIPhoneXWidthMargin)
    w.rightAnchor.absolute =  math.floor(UIRoot.VirtualIPhoneXWidthMargin)
    w:UpdateAnchors()
end

function LuaZhaiXingStateWnd:Update()
    if CommonDefs.Is_PC_PLATFORM() then
        if CWinSocialWndMgr == nil then
            CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
        end
        if self.m_IsWinSocialWndOpened ~= CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened or self.m_Height ~= Screen.height or self.m_Width ~= Screen.width then
            self:InitGameTexture()
        end
    end
end
