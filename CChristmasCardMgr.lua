-- Auto Generated!!
local CChristmasCardEditWnd = import "L10.UI.CChristmasCardEditWnd"
local CChristmasCardInfo = import "L10.UI.CChristmasCardInfo"
local CChristmasCardWnd = import "L10.UI.CChristmasCardWnd"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local EnumClass = import "L10.Game.EnumClass"
local EnumGender = import "L10.Game.EnumGender"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local LocalString = import "LocalString"
local ShengDan_Setting = import "L10.Game.ShengDan_Setting"
Gas2Gac.ShowChristmasCardEditWnd = function (templateId, itemId, place, pos) 
    CChristmasCardEditWnd.s_ItemId = itemId
    CChristmasCardEditWnd.s_Place = place
    CChristmasCardEditWnd.s_Pos = pos
    CChristmasCardEditWnd.s_PlayerId = 0
    CChristmasCardEditWnd.s_PlayerName = ""
    CUIManager.ShowUI("ChristmasCardEditWnd")
end
Gas2Gac.ShowEditedChristCardListWnd = function () 
    local editedCardId = ShengDan_Setting.GetData().EditedCardTemplateId
    local itemInfos = CItemMgr.Inst:GetPlayerItemsAtPlaceByTemplateId(EnumItemPlace.Bag, editedCardId)
    if itemInfos.Count > 0 then
        CUIManager.ShowUI("ChristmasCardPopupMenu")
    else
        g_MessageMgr:ShowMessage("NO_EDITED_CARD_IN_PACKAGE")
    end
end
Gas2Gac.SendCardToSender = function (senderId, senderName) 
    local emptyCardId = ShengDan_Setting.GetData().EmptyCardTemplateId
    local itemInfos = CItemMgr.Inst:GetPlayerItemsAtPlaceByTemplateId(EnumItemPlace.Bag, emptyCardId)
    if itemInfos ~= nil and itemInfos.Count > 0 then
        CChristmasCardEditWnd.s_ItemId = itemInfos[0].itemId
        CChristmasCardEditWnd.s_Place = EnumToInt(itemInfos[0].place)
        CChristmasCardEditWnd.s_Pos = itemInfos[0].pos
        CChristmasCardEditWnd.s_PlayerId = math.floor(tonumber(senderId or 0))
        CChristmasCardEditWnd.s_PlayerName = senderName
        CUIManager.ShowUI("ChristmasCardEditWnd")
    else
        --缺少空贺卡
        g_MessageMgr:ShowMessage("LACK_OF_EMPTY_CARD")
    end
end
Gas2Gac.ShowChristmasCardWnd = function (senderId, senderClass, senderGender, senderName, recieverId, recieverName, voiceUrl, voiceDuration, wishContent, senderTime) 
    CChristmasCardWnd.s_CardInfo = CreateFromClass(CChristmasCardInfo)
    CChristmasCardWnd.s_CardInfo.SenderId = math.floor(senderId)
    CChristmasCardWnd.s_CardInfo.SenderClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), senderClass)
    CChristmasCardWnd.s_CardInfo.SenderGender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), senderGender)
    CChristmasCardWnd.s_CardInfo.SenderName = senderName
    CChristmasCardWnd.s_CardInfo.RecieverId = math.floor(recieverId)
    CChristmasCardWnd.s_CardInfo.RecieverName = recieverName
    CChristmasCardWnd.s_CardInfo.voiceUrl = voiceUrl
    CChristmasCardWnd.s_CardInfo.voideDuration = voiceDuration
    CChristmasCardWnd.s_CardInfo.wishContent = wishContent
    CChristmasCardWnd.s_CardInfo.SenderTime = math.floor(senderTime)
    CUIManager.ShowUI("ChristmasCardWnd")
end
Gas2Gac.ShowExchangeCardToYinPiaoCount = function () 
    local emptyCardId = ShengDan_Setting.GetData().EmptyCardTemplateId
    if CClientMainPlayer.Inst ~= nil then
        local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, emptyCardId)
        if count <= 0 then
            g_MessageMgr:ShowMessage("LACK_OF_EMPTY_CARD")
            return
        end
        CLuaNumberInputMgr.ShowNumInputBox(1, count, 1, function (v) 
            Gac2Gas.RequestExchangeCardToYinPiao(v)
        end, LocalString.GetString("请输入你要兑换的贺卡个数"), -1)
    end
end
