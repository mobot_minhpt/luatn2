local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local Vector3 = import "UnityEngine.Vector3"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CCurentMoneyCtrl=import "L10.UI.CCurentMoneyCtrl"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaDoubleOneQLDShowWnd = class()
RegistChildComponent(LuaDoubleOneQLDShowWnd,"closeBtn", GameObject)
RegistChildComponent(LuaDoubleOneQLDShowWnd,"enterBtn", GameObject)
RegistChildComponent(LuaDoubleOneQLDShowWnd,"leftTimeLabel", UILabel)
RegistChildComponent(LuaDoubleOneQLDShowWnd,"freeNumNode", GameObject)
RegistChildComponent(LuaDoubleOneQLDShowWnd,"freeNumLabel", UILabel)
RegistChildComponent(LuaDoubleOneQLDShowWnd,"costShowNode", GameObject)
RegistChildComponent(LuaDoubleOneQLDShowWnd,"buyItemBtn", GameObject)
RegistChildComponent(LuaDoubleOneQLDShowWnd,"itemTemplate", GameObject)
RegistChildComponent(LuaDoubleOneQLDShowWnd,"itemFatherNode", GameObject)
RegistChildComponent(LuaDoubleOneQLDShowWnd,"showText", UILabel)

RegistClassMember(LuaDoubleOneQLDShowWnd, "clickNode")
RegistClassMember(LuaDoubleOneQLDShowWnd, "itemDataTable")
RegistClassMember(LuaDoubleOneQLDShowWnd, "m_CountDownTick")
RegistClassMember(LuaDoubleOneQLDShowWnd, "m_FirstClick")

function LuaDoubleOneQLDShowWnd:Close()
	CUIManager.CloseUI("DoubleOneQLDShowWnd")
end

function LuaDoubleOneQLDShowWnd:OnEnable()
	g_ScriptEvent:AddListener("DoubleOneQLDShowInfoRefresh", self, "Init")
end

function LuaDoubleOneQLDShowWnd:OnDisable()
	g_ScriptEvent:RemoveListener("DoubleOneQLDShowInfoRefresh", self, "Init")
end

function LuaDoubleOneQLDShowWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
	self.m_FirstClick = nil
	self.clickNode = nil

	local onEnterClick = function(go)
		Gac2Gas.RequestEnterQiLinDongPlay()
	end
	CommonDefs.AddOnClickListener(self.enterBtn,DelegateFactory.Action_GameObject(onEnterClick),false)
	self.itemTemplate:SetActive(false)
	self.costShowNode:SetActive(false)
	self.buyItemBtn:SetActive(false)

	if LuaDoubleOne2019Mgr.QLDLeftFreeTimes > 0 then
		self.freeNumNode:SetActive(true)
		self.freeNumLabel.text = LuaDoubleOne2019Mgr.QLDLeftFreeTimes
	else
		self.freeNumNode:SetActive(false)
	end

	self:InitData()
	self:UpdateTimeCount()
  if self.m_CountDownTick then
    UnRegisterTick(self.m_CountDownTick)
    self.m_CountDownTick = nil
  end
  self.m_CountDownTick = RegisterTick(function() self:UpdateTimeCount() end, 1000)

	CUICommonDef.ClearTransform(self.itemFatherNode.transform)

	local itemWidth = 106
	if LuaDoubleOne2019Mgr.QLDItemTable then
		for i,v in ipairs(LuaDoubleOne2019Mgr.QLDItemTable) do
			local go = NGUITools.AddChild(self.itemFatherNode, self.itemTemplate)
			go:SetActive(true)
			go.transform.localPosition = Vector3((i-1)*itemWidth,0,0)
			self:InitItemNode(go,v)
		end
	end

	self.showText.text = g_MessageMgr:FormatMessage('QLD_SHOW_TIP')
end

function LuaDoubleOneQLDShowWnd:UpdateTimeCount()
	local leftTime = LuaDoubleOne2019Mgr.QLDItemExpiredTime - CServerTimeMgr.Inst.timeStamp
	if leftTime and leftTime > 0 then
		local hour = math.floor(leftTime / 3600)
		local fen = math.floor((leftTime - hour * 3600) / 60)
		local second = math.floor(leftTime - hour * 3600 - fen * 60)
		if fen < 10 then
			fen = '0' .. fen
		end
		if second < 10 then
			second = '0' .. second
		end
		self.leftTimeLabel.text = hour .. ':' .. fen .. ':' .. second .. LocalString.GetString('后刷新')
	else
		self.leftTimeLabel.text = '0:00:00'
		self:Close()
	end
end

function LuaDoubleOneQLDShowWnd:InitData()
	self.itemDataTable = {}
	QiLinDong_Item.Foreach(function (key, data)
		self.itemDataTable[data.ShopItemId] = data.ShopPrice
	end)

end

function LuaDoubleOneQLDShowWnd:InitItemNode(node,itemId)
	local item = CItemMgr.Inst:GetItemTemplate(itemId)
	if item == nil then
		return
	end
	node.transform:Find('IconTexture'):GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
	local highlightNode = node.transform:Find('highlight').gameObject
	local existNode = node.transform:Find('GetHint').gameObject
	highlightNode:SetActive(false)
	existNode:SetActive(false)
	if LuaDoubleOne2019Mgr.QLDExistItemTable and LuaDoubleOne2019Mgr.QLDExistItemTable[itemId] then
		existNode:SetActive(true)
		local onClick = function(go)
			if self.clickNode then
				self.clickNode:SetActive(false)
			end
			self.clickNode = highlightNode
			self.clickNode:SetActive(true)
			self.costShowNode:SetActive(false)
			self.buyItemBtn:SetActive(false)

			CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
		end
		CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(onClick),false)

	else
		existNode:SetActive(false)
		local onClick = function(go)
			if self.clickNode then
				self.clickNode:SetActive(false)
			end
			self.clickNode = highlightNode
			self.clickNode:SetActive(true)
			self.costShowNode:SetActive(true)
			self.buyItemBtn:SetActive(true)
			local moneyCtrl = self.costShowNode:GetComponent(typeof(CCurentMoneyCtrl))
			moneyCtrl:SetType(EnumMoneyType.YinLiang,EnumPlayScoreKey.NONE, true)
			moneyCtrl:SetCost(self.itemDataTable[itemId])

			CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
			local onBuyClick = function(go)
				MessageWndManager.ShowOKCancelMessage(SafeStringFormat3(LocalString.GetString("是否花费%s银两购买%s？"),self.itemDataTable[itemId],item.Name),
				DelegateFactory.Action(function ()
					Gac2Gas.RequestBuyQiLinDongPlayShopItem(itemId)
				end), nil, LocalString.GetString("购买"), nil, false)

			end
			CommonDefs.AddOnClickListener(self.buyItemBtn,DelegateFactory.Action_GameObject(onBuyClick),false)
		end
		CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(onClick),false)
		if not self.m_FirstClick then
			onClick()
			self.m_FirstClick = true
		end
	end
end

function LuaDoubleOneQLDShowWnd:OnDestroy()
  if self.m_CountDownTick then
    UnRegisterTick(self.m_CountDownTick)
    self.m_CountDownTick = nil
  end
end

return LuaDoubleOneQLDShowWnd
