-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CKabeDonMessageBox = import "L10.UI.CKabeDonMessageBox"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local ETickType = import "L10.Engine.ETickType"
local L10 = import "L10"
local LocalString = import "LocalString"
local UICamera = import "UICamera"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CKabeDonMessageBox.m_Init_CS2LuaHook = function (this) 
    local player = CClientPlayerMgr.Inst:GetPlayer(math.floor(CKabeDonMessageBox.LauncherId))
    this:CancelTick()

    local default
    if (player ~= nil) then
        default = player.Name
    else
        default = ""
    end
    local name = default
    local message = g_MessageMgr:FormatMessage("ASK_FOR_BIDONG", CKabeDonMessageBox.LauncherId, name)
    this.textLabel.text = message

    this.aliveDuration = CKabeDonMessageBox.DEFAULT_REFUSE_TIME
    this.cancelButton.Text = System.String.Format("{0}({1})", LocalString.GetString("拒绝"), math.floor(this.aliveDuration))
    this.m_TickAction = L10.Engine.CTickMgr.Register(DelegateFactory.Action(function () 
        this:DelayCancelButtonAction(this.aliveDuration)
    end), 500, ETickType.Loop)
end
CKabeDonMessageBox.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.okButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.okButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnOkButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.shyButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.shyButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnShyButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.cancelButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.cancelButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnCancelButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.textLabel.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.textLabel.gameObject).onClick, MakeDelegateFromCSFunction(this.OnTextLabelClick, VoidDelegate, this), true)
    if this.closeButton ~= nil then
        UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    end
end
CKabeDonMessageBox.m_DelayCancelButtonAction_CS2LuaHook = function (this, duration) 
    if duration <= 0 then
        this.cancelButton.Text = LocalString.GetString("拒绝")
        this.cancelButton.Enabled = true
        this:CancelTick()
        Gac2Gas.RequestBiDongPlayerAnswer(CKabeDonMessageBox.LauncherId, 2)
        this:Close()
    else
        this.cancelButton.Text = System.String.Format("{0}({1})", LocalString.GetString("拒绝"), math.floor(duration))
    end
end
CKabeDonMessageBox.m_CancelTick_CS2LuaHook = function (this) 
    if this.m_TickAction ~= nil then
        invoke(this.m_TickAction)
        this.m_TickAction = nil
    end
end
CKabeDonMessageBox.m_OnTextLabelClick_CS2LuaHook = function (this, go) 
    local url = this.textLabel:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url ~= nil then
        CChatLinkMgr.ProcessLinkClick(url, nil)
    end
end
