local CSceneSimpleButton=import "L10.UI.CSceneSimpleButton"
local CHouseRollerCoasterMgr=import "L10.Game.CHouseRollerCoasterMgr"
local CMiniMapPopWnd=import "L10.UI.CMiniMapPopWnd"
local CZuoanFurnitureMgr = import "L10.UI.CZuoanFurnitureMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CTooltip = import "L10.UI.CTooltip"
local ShowType = import "L10.UI.CItemInfoMgr+ShowType"
local LinkItemInfo = import "L10.UI.CItemInfoMgr+LinkItemInfo"
local CItemInfoMgr_AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CFurnitureProp2=import "L10.Game.CFurnitureProp2"
local CFurnitureProp3=import "L10.Game.CFurnitureProp3"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CHouseDataBasicProp = import "L10.Game.CHouseDataBasicProp"
local CGardenProp = import "L10.Game.CGardenProp"
local CProgressProp = import "L10.Game.CProgressProp"
local CConstructProp = import "L10.Game.CConstructProp"
local CNewsProp=import "L10.Game.CNewsProp"
local CChuanjiabaoGroupType = import "L10.Game.CChuanjiabaoGroupType"
local CHouseIntroWnd=import "L10.UI.CHouseIntroWnd"
local CCommunityMgr=import "L10.UI.CCommunityMgr"
local CClientFurniture=import "L10.Game.CClientFurniture"
local CRenderScene=import "L10.Engine.Scene.CRenderScene"

local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local MessageWndManager=import "L10.UI.MessageWndManager"
local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"
local HousePopupMenuItemData=import "L10.UI.HousePopupMenuItemData"

local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CHouseCompetitionMgr = import "L10.Game.CHouseCompetitionMgr"
local CHousePopupMgr = import "L10.UI.CHousePopupMgr"
local CHousePuppetMgr = import "L10.Game.CHousePuppetMgr"
local CScene = import "L10.Game.CScene"
local CYangyangPhotoVoteMgr = import "L10.UI.CYangyangPhotoVoteMgr"
local EnumClosestoolStatus = import "L10.Game.EnumClosestoolStatus"
local EnumMapiActionType = import "L10.Game.EnumMapiActionType"
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local CLingshouFurnitureProp = import "L10.Game.CLingshouFurnitureProp"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local Zhuangshiwu_Zhuangshiwu=import "L10.Game.Zhuangshiwu_Zhuangshiwu"
local COpenEntryMgr=import "L10.Game.COpenEntryMgr"
local CPuppetClothesMgr=import "L10.UI.CPuppetClothesMgr"

local CoreScene = import "L10.Engine.Scene.CoreScene"
local CPos = import "L10.Engine.CPos"
local CTrackMgr = import "L10.Game.CTrackMgr"
local EnumFurnitureSliderOT = import "L10.UI.EnumFurnitureSliderOT"
local MouseInputHandler = import "L10.Engine.MouseInputHandler"
local Utility = import "L10.Engine.Utility"
local CFurnitureSliderMgr = import "L10.UI.CFurnitureSliderMgr"
local EnumCropPhase=import "L10.Game.EnumCropPhase"
local CClientNpc = import "L10.Game.CClientNpc"
local BabyMgr = import "L10.Game.BabyMgr"

local CClientChuanJiaBaoMgr = import "L10.Game.CClientChuanJiaBaoMgr"
local CQiYuanMgr = import "L10.UI.CQiYuanMgr"
local CFurnitureProp = import "L10.Game.CFurnitureProp"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

--local House_SkyBoxKind = import "L10.Game.House_SkyBoxKind"
local CHouseExtraInfoType = import "L10.Game.CHouseExtraInfoType"


local DateTime = import "System.DateTime"
local Byte = import "System.Byte"
local EnumEventType = import "EnumEventType"
local MsgPackImpl = import "MsgPackImpl"
local MeshRenderer = import "UnityEngine.MeshRenderer"
local EnumHousePaperType = import "L10.Game.EnumHousePaperType"
local GameObject = import "UnityEngine.GameObject"
local CClientObjectRoot=import "L10.Game.CClientObjectRoot"
local CMultipleFurnitureRoot = import "L10.Game.CMultipleFurnitureRoot"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CWallAndFloorPaperChanger = import "L10.Game.CWallAndFloorPaperChanger"

CLuaHouseMgr = {}
CLuaHouseMgr.RoomDecorationPrivalege = false
CLuaHouseMgr.YardDecorationPrivalege = false

CLuaHouseMgr.currentHouseIntro = {}

CLuaHouseMgr.ClickItemInfo = nil

CLuaHouseMgr.MultiPlaceCandidate = nil
CLuaHouseMgr.CrtMultiPlaceFur = nil

--[[ {key:pos,value:{
                {templateId,isWinter},
                {templateId,isWinter},
    }}
--]]
CLuaHouseMgr.MultipleWinterSkinCache = {}

CLuaHouseMgr.s_EnableAdjustmentWhenAddFurniture = true

function CLuaHouseMgr:AddListener()
    g_ScriptEvent:RemoveListener("OnUpdateSingleCommunityData", self, "TryToQueryHouseMemberAtHomeInfo")
    g_ScriptEvent:AddListener("OnUpdateSingleCommunityData", self, "TryToQueryHouseMemberAtHomeInfo")
end

CLuaHouseMgr:AddListener()


function CLuaHouseMgr.ClearYarkPick()
	g_YarkPick={}
end

function CLuaHouseMgr.ShowHousePopupMenu(items,target,ro)
    CHousePopupMgr.Items=items
    CHousePopupMgr.Target = target
    CHousePopupMgr.RO = ro
    CUIManager.ShowUI(CUIResources.HousePopupMenu)
end

function CLuaHouseMgr:TryToQueryHouseMemberAtHomeInfo(args)
    if CLuaHouseMgr.IsOpenHouseMenberAtHome() then
        if CClientHouseMgr.Inst.mCurCommunityData ~= nil then
            Gac2Gas.QueryHouseMemberAtHomeInfo(CClientHouseMgr.Inst.mCurCommunityData.Id)
        end
    end
end

function CLuaHouseMgr.IsOpenHouseMenberAtHome()
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local timestr = House_Setting.GetData().ShowAtHomeStartTime
    local year, month, day, hour, min= string.match(timestr, "(%d+)-(%d+)-(%d+) (%d+):(%d+)")  
    local startTime = CreateFromClass(DateTime, year, month, day, hour, min, 0)
    local endTime

    timestr = House_Setting.GetData().ShowAtHomeEndTime
    year, month, day, hour, min= string.match(timestr, "(%d+)-(%d+)-(%d+) (%d+):(%d+)")  
    endTime = CreateFromClass(DateTime, year, month, day, hour, min, 0)

    if DateTime.Compare(startTime, now) < 0 and DateTime.Compare(now, endTime) < 0 then      
        return true
    end

    return false
end

function CLuaHouseMgr.QueryHouseMemberAtHomeInfoResult(communityId, dataUD)
    local atHomePos = CreateFromClass(MakeGenericClass(List, Byte))
	local list = MsgPackImpl.unpack(dataUD)
    for i=0, list.Count-1, 1 do
        local pos = list[i]

        CommonDefs.ListAdd(atHomePos, typeof(Byte), pos)
    end

    EventManager.BroadcastInternalForLua(EnumEventType.OnQueryHouseMemberAtHome, {atHomePos})
end

function CLuaHouseMgr.IsHouseBuff(buffId)
    local baseBuffId = math.floor( buffId/100 )
    return baseBuffId==641478 or baseBuffId==643885
end

--包裹界面是否显示使用按钮
function CLuaHouseMgr.CanShowUseButtonInPackage(zswdata)
    local zswId = zswdata.ID
    -- 斗地主牌桌 星月之辉 全民争霸赛奖杯 不显示使用按钮
    if zswId==9030 or zswId==9064 or zswId==9073 then
        return false
    end
    if zswdata.ForbidQiShuFurniture>0 then
        return false
    end
    return true
end

CClientFurniture.m_hookShowActionPopupMenu = function (this)
  if not CLuaHouseMgr.sPujiacunQiuqianId then
    CLuaHouseMgr.sPujiacunQiuqianId = Zhuangshiwu_Setting.GetData().PujiacunQiuqianId
  end
  if not CLuaHouseMgr.sYinXianWanChairId then
    CLuaHouseMgr.sYinXianWanChairId = GameplayItem_YinXianWan.GetData().FurnitureIds[0]
  end
  if this.TemplateId == CLuaHouseMgr.sPujiacunQiuqianId then
    Gac2Gas.QueryPuJiaCunQiuQianInfo(this.ID)
  elseif this.TemplateId == CLuaHouseMgr.sYinXianWanChairId then
    Gac2Gas.YinXianWan_QueryFurnitureInfo(this.ID)
  else
    local itemActionPairs = CLuaHouseMgr.GetPopupMenuItemData(this)

    local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(this.TemplateId)
    if zswdata ~= nil and zswdata.SubType == 15 then
        if not COpenEntryMgr.Inst:GetGuideRecord(44) then
            local triggered = CGuideMgr.Inst:StartGuide(44, 1)
            if triggered then
                COpenEntryMgr.Inst:SetGuideRecord(44)
            end
        end
    end

    if #itemActionPairs > 0 then
        CLuaHouseMgr.ShowHousePopupMenu(Table2Array(itemActionPairs,MakeArrayClass(HousePopupMenuItemData)),
            this.RO:GetSlotTransform("TopAnchor", false),this.RO)
    end
  end
end

CClientFurniture.m_hookCheckCanSelect=function(this)
    if CClientFurnitureMgr.Inst.TerrainEditMode then
        return false
    end
    
    --家园木桩
    if this.IsWoodPile then
        return true
    end

    local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(this.TemplateId)
    if zswdata and zswdata.SubType == LuaEnumFurnitureSubType.eWeiqiang then--eWeiqiang
        return false
    end
    local itemActionPairs = CLuaHouseMgr.GetPopupMenuItemData(this)
    return #itemActionPairs > 0
end

CLuaHouseMgr.sPujiacunQiuqianId = nil --Zhuangshiwu_Setting.GetData().PujiacunQiuqianId
CLuaHouseMgr.sYinXianWanChairId = nil

CClientFurniture.m_OnAddFurnitureInteractPlayer = function(this,pid,slotIdx)
    CLuaClientFurniture.ProcessAddPuJiacunQiuQianInteractPlayer(this,pid, slotIdx)
    CLuaClientFurniture.ProcessAddYinXianWanChairInteractPlayer(this,pid, slotIdx)
end

CClientFurniture.m_OnRemoveFurnitureInteractPlayer = function(this,pid)
    CLuaClientFurniture.ProcessRemovePuJiacunQiuQianInteractPlayer(this,pid)
    CLuaClientFurniture.ProcessRemoveYinXianWanChairInteractPlayer(this,pid)
end

function CLuaHouseMgr.IsInHuyitianHouse()
    if CScene.MainScene == nil then
        return false
    end
    local setting = HuYiTianHouse_Setting.GetData()
    return CScene.MainScene.SceneTemplateId == setting.MapId
end

function CLuaHouseMgr.IsInSpokesmanHouse()
    --根据放兄的指导读取mCurExtraInfo.IsSpokesmanHouse，目前在C#下使用
    return CClientHouseMgr.Inst:IsPlayerInHouse() and 
        CClientHouseMgr.Inst.mCurExtraInfo and CClientHouseMgr.Inst.mCurExtraInfo.IsSpokesmanHouse >0 
end

function CLuaHouseMgr.IsUsedByMainPlayer(this)
    if CClientMainPlayer.Inst == nil then
        return false
    end
    local pid = CClientMainPlayer.Inst.Id
    if this.Children ~= nil then
        do
            local i = 0
            while i < this.Children.Length do
                if pid == this.Children[i] then
                    return true
                end
                i = i + 1
            end
        end
    end
    return false
end
function CLuaHouseMgr.RequestInteractWithMatong(this,status)
    if status == EnumClosestoolStatus.eNone then
        Gac2Gas.RequestClosestoolProduce(this.ID)
    elseif status == EnumClosestoolStatus.eReady then
        Gac2Gas.RequestGetClosestoolProduction(this.ID)
    end
    CClientFurnitureMgr.Inst:ClearCurFurniture()
end

function CLuaHouseMgr.RequestUseZhengwu(this)
    local pos = CreateFromClass(CPos, math.floor(this.RO.Position.x), math.floor(this.RO.Position.z))
    pos = Utility.GridPos2PixelPos(pos)

    CTrackMgr.Inst:Track(pos, Utility.Grid2Pixel(CClientFurniture.sHouseGridTrackPos), DelegateFactory.Action(function ()
        Gac2Gas.RequestEnterHouseRoom()
    end), nil, true)
end
function CLuaHouseMgr.RequestUseXiangfang(this)
    local zhengwuGrade = 0
    if CClientHouseMgr.Inst.mConstructProp ~= nil then
        zhengwuGrade = CClientHouseMgr.Inst.mConstructProp.ZhengwuGrade
    end
    if zhengwuGrade < Zhuangshiwu_Setting.GetData().XiangfangNeedZhengwuGrade then
        g_MessageMgr:ShowMessage("Xiangfang_Not_Open")
        return
    end
    CUIManager.ShowUI(CUIResources.CXiangfangManageWnd)
end
function CLuaHouseMgr.RequestUseCangku(this)
    local pos = CreateFromClass(CPos, math.floor(this.RO.Position.x), math.floor(this.RO.Position.z))
    pos = Utility.GridPos2PixelPos(pos)

    CTrackMgr.Inst:Track(pos, Utility.Grid2Pixel(CClientFurniture.sHouseGridTrackPos), DelegateFactory.Action(function ()
        CUIManager.ShowUI(CUIResources.PlantExchangeWnd)
    end), nil, true)
end
function CLuaHouseMgr.RequestScaleFurniture(this)
    CFurnitureSliderMgr.CurOT = EnumFurnitureSliderOT.eScale
    CFurnitureSliderMgr.CurFurnitureId = this.ID

    CLuaHouseFurnitureRotationWnd.m_TargetFurniture = this
    CUIManager.ShowUI(CLuaUIResources.HouseFurnitureRotationWnd)
    --CUIManager.ShowUI(CUIResources.FurnitureSlider)
    CUIManager.CloseUI(CUIResources.HousePopupMenu)
end
function CLuaHouseMgr.RequestAdjustObstacleFurniture(this, canScale) -- 调整有障碍装饰物, 可绕Y轴任意旋转, 可调大小, 不能调高度
    if this.TemplateId == Zhuangshiwu_Setting.GetData().MajiuZhuangshiwuId and CZuoQiMgr.Inst.m_MajiuMapiInfo and CZuoQiMgr.Inst.m_MajiuMapiInfo.Count > 0 then
        g_MessageMgr:ShowMessage("Majiu_Cant_Rotate_With_Mapi")
        return
    end

    CFurnitureSliderMgr.CurOT = EnumFurnitureSliderOT.eScale
    CFurnitureSliderMgr.CurFurnitureId = this.ID

    if not CFurnitureScene.IsOpenObstacleScaling() or not canScale then
        CLuaHouseFurnitureRotationWnd.s_ForceHideScaleSlider = true
    end
    CLuaHouseFurnitureRotationWnd.s_ForceHideHeightSlider = true
    CLuaHouseFurnitureRotationWnd.s_OnlyRotateAxisY = true
    CLuaHouseFurnitureRotationWnd.m_TargetFurniture = this
    CUIManager.ShowUI(CLuaUIResources.HouseFurnitureRotationWnd)
    --CUIManager.ShowUI(CUIResources.FurnitureSlider)
    CUIManager.CloseUI(CUIResources.HousePopupMenu)
end
function CLuaHouseMgr.RequestShiftFurniture(this)
    CFurnitureSliderMgr.CurOT = EnumFurnitureSliderOT.eShift
    CFurnitureSliderMgr.CurFurnitureId = this.ID

    CUIManager.ShowUI(CUIResources.GuashiSlider)
    CUIManager.CloseUI(CUIResources.HousePopupMenu)
end

function CLuaHouseMgr.ProcessRotate(this,zswdata,itemActionPairs)
    --特殊处理伞

    if zswdata.SubType == EnumFurnitureSubType_lua.eHuamu then
        table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
            CLuaHouseMgr.RequestRotateHuamu(this)
        end), "UI/Texture/Transparent/Material/housepopupmenu_xuanzhuan.mat", false, true, -1))
    else
        table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
            CLuaHouseMgr.RequestRotateFurniture(this)
        end), "UI/Texture/Transparent/Material/housepopupmenu_xuanzhuan.mat", false, true, -1))
    end
end

function CLuaHouseMgr.RequestRotateFurniture(this)
    if this.TemplateId == Zhuangshiwu_Setting.GetData().MajiuZhuangshiwuId and CZuoQiMgr.Inst.m_MajiuMapiInfo and CZuoQiMgr.Inst.m_MajiuMapiInfo.Count > 0 then
        g_MessageMgr:ShowMessage("Majiu_Cant_Rotate_With_Mapi")
        return
    end
    local y = math.floor(this.RO.transform.eulerAngles.y)
    y = (y+360)%360
    --if CFurnitureScene.IsOpen360Rotation() then
    --    y = math.ceil(y/5)*5
    --    local delta = 5
    --    local newY = (y + delta) % 360
    --    this.RO.transform.eulerAngles = Vector3(0, newY, 0)
    --else
        y = math.floor(y/90)*90
        local delta = 90
        local newY = (y + delta) % 360
        this.RO.transform.eulerAngles = Vector3(0, newY, 0)
    --end
    
    EventManager.Broadcast(EnumEventType.OnFurnitureRotated)
end
function CLuaHouseMgr.RequestRotateHuamu(this)
    local y = math.floor(this.RO.transform.eulerAngles.y)
    local delta = 5
    local newY = (y + delta) % 360
    this.RO.transform.eulerAngles = Vector3(0, newY, 0)
    this.ServerRX = 0
    this.ServerRY = 0
    this.ServerRZ = 0
    if this:IsPlacedInServer() then
        this:RegisterRotateTick(this.ID, newY)
    end
end
function CLuaHouseMgr.RequestPlacePuppet(this)
    local bCanPut = this:CheckMoveFurniture()
    if bCanPut then
        if this.RO == nil then
            this:Destroy()
        else
            local yRot = math.floor(this.RO.transform.eulerAngles.y)
            local x = this.RO.Position.x
            local z = this.RO.Position.z
            local y = this.RO.Position.y

            --double xPos = Math.Floor(x) + 0.5;
            --double zPos = Math.Floor(z) + 0.5;
            --float terrainY = CClientFurnitureMgr.Inst.GetOrigLogicHeight((float)xPos, (float)zPos);
            --double yOffset = y - terrainY - CClientFurniture.s_YOffset;

            if this.EngineId ~= 0 then
                yRot = (math.floor(yRot / 90)) * 90
            end
            --Gac2Gas.MoveFurniture(fur.ID, fur.TemplateId, x, yOffset, z, yRot, false);
            Gac2Gas.RequestMovePuppet(this.EngineId, math.floor(x), math.floor(z), yRot)
            CClientFurnitureMgr.Inst.CurFurniture = nil
            --this.RO.Visible = false;
        end

        this.Moving = false
        MouseInputHandler.Inst:ClearFurnitureOffset()
    else
        g_MessageMgr:ShowMessage("FURNITURE_CANNOT_PUT_IN_PLACE")
    end
end

function CLuaHouseMgr.RequestFightHis(engineId)
    CHousePuppetMgr.Inst.SaveFightDataPuppetEngineId = engineId
    CUIManager.ShowUI(CUIResources.PuppetFightDataWnd)
end
function CLuaHouseMgr.RequestShowInfo(this)
    if CLuaHouseMgr.ClickItemInfo ~= nil then
        local furTemplateId = CLuaHouseMgr.ClickItemInfo.TemplateId
        if furTemplateId==this.TemplateId then--相等
            if CLuaHouseMgr.ClickItemInfo.ChuanjiabaoItemId == nil and furTemplateId and furTemplateId ~= 0 then
                CLuaFurnitureSmallTypeItem.CurShowFurnitureTemplateId = furTemplateId
                CItemInfoMgr.showType = ShowType.FurnitureRepository

                local cnt,actions,names = CLuaHouseMgr.GetFurnitureItemActionPairs( nil, furTemplateId)
                local actionSource = DefaultItemActionDataSource.Create(cnt,actions,names)

                CItemInfoMgr.linkItemInfo = LinkItemInfo(furTemplateId, false, actionSource)
                CItemInfoMgr.linkItemInfo.alignType = CItemInfoMgr_AlignType.ScreenRight
                CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)

            elseif CLuaHouseMgr.ClickItemInfo.ChuanjiabaoItemId ~= nil and this.TemplateId == 0 then
                local chuanjiabaoId = CLuaHouseMgr.ClickItemInfo.ChuanjiabaoItemId
                local item = CItemMgr.Inst:GetById(chuanjiabaoId)
                if item ~= nil then
                    CLuaFurnitureSmallTypeItem.CurShowChuanjiabaoItemId = chuanjiabaoId
                    CItemInfoMgr.showType = ShowType.ChuanjiabaoRepository

                    local cnt,actions,names = CLuaHouseMgr.GetFurnitureItemActionPairs( chuanjiabaoId, 0)
                    local actionSource = DefaultItemActionDataSource.Create(cnt,actions,names)

                    CItemInfoMgr.linkItemInfo = LinkItemInfo(item, false, actionSource)
                    CItemInfoMgr.linkItemInfo.alignType = AlignType.ScreenRight
                    CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
                end

            end
        end
    end
end
function CLuaHouseMgr.RequestPlaceFurniture(this)
    local bCanPut = this:CheckMoveFurniture()
    if bCanPut then
        local data = Zhuangshiwu_Zhuangshiwu.GetData(this.TemplateId)
        local isZswTemplate = data.Type == EnumFurnitureType_lua.eZswTemplate
        if isZswTemplate then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ZswTemplate_BaiFang_Tip"), DelegateFactory.Action(function ()            
                local bNeedDestroy = CLuaHouseMgr.TryAddFurniture(this)
                this.Moving = false
                if bNeedDestroy then
                    this:Destroy()
                    --清空装饰物模板装填数据
                    CLuaZswTemplateMgr.HouseZswTemplateFillData[tostring(CLuaZswTemplateMgr.SelectEditTemplateId)] = nil
                    CLuaZswTemplateMgr.OnSaveTemplateFillData()
                end
                MouseInputHandler.Inst:ClearFurnitureOffset()
            end), nil, nil, nil, false)
        else
            local bNeedDestroy = CLuaHouseMgr.TryAddFurniture(this)
            this.Moving = false
            if bNeedDestroy then
                this:Destroy()
            end
            MouseInputHandler.Inst:ClearFurnitureOffset()
        end
    else
        if CLuaClientFurnitureMgr.m_LatColorGridOverlap then
            g_MessageMgr:ShowMessage("ZswTemplate_Overlap_CannotPut")
        else
            g_MessageMgr:ShowMessage("FURNITURE_CANNOT_PUT_IN_PLACE")
        end
    end
end

function CLuaHouseMgr.TryAddFurniture(fur)--CClientFurniture
    if not CommonDefs.DictContains(CClientFurnitureMgr.Inst.FurnitureList, typeof(UInt32), fur.ID) then
        if fur.RO ~= nil then
            if CLuaHouseWoodPileMgr.IsWoodPile(fur.TemplateId) then
                Gac2Gas.RequestCreateWoodPile(fur.TemplateId,
                    math.floor(fur.Position.x),
                    math.floor(fur.Position.z),
                    math.floor(fur.RO.transform.eulerAngles.y))
            else
                --摆放装饰物模板
                local data = Zhuangshiwu_Zhuangshiwu.GetData(fur.TemplateId)
                if data.Type == EnumFurnitureType_lua.eZswTemplate then
                    local zswTemData = Zhuangshiwu_ZswTemplate.GetData(fur.TemplateId)
                    if fur.Children then
                        --先找到最高的terrainY
                        local highestTerrainY = 0
                        local placeInfo = {}
                        for i=0,fur.Children.Length -1,1 do
                            local info ={}
                            table.insert(placeInfo,info)
                            if fur.Children[i] > 0 then                              
                                local fid = fur.Children[i]
                                local childfur = CClientFurnitureMgr.Inst:GetFurniture(fid)
                                local eulerAngles = childfur.RO.transform.localEulerAngles
                                local y = childfur.Position.y
                                local x = childfur.Position.x
                                local z = childfur.Position.z                   
                                local zdata = Zhuangshiwu_Zhuangshiwu.GetData(childfur.TemplateId)
                                local offsetY = zswTemData.Offset[3*i+1]
                                local rx = eulerAngles.x
                                local ry = eulerAngles.y
                                local rz = eulerAngles.z
                                info.rot = math.floor(ry+0.5)
                                if zdata.RevolveSwitch == 1 then
                                    rx = math.max(0,math.floor(rx/360*256))
                                    ry = math.max(0,math.floor(ry/360*256))
                                    rz = math.max(0,math.floor(rz/360*256))
                                elseif zdata.Revolve90 == 1 then
                                    rx = 0
                                    ry = math.max(0,math.floor(ry/360*256))
                                    rz = 0
                                else
                                    rx = 0
                                    ry = 0
                                    rz = 0
                                end
                                local scale = zswTemData.Scale[i]
                                local ui8Scale = CClientFurnitureMgr.GetScaleFloatToUI8(scale)
                                info.templateId = childfur.TemplateId
                                info.x = math.floor(x)
                                info.y = CLuaHouseMgr.IsFurnitureAllowToChgY(zdata) and offsetY/100 or 0
                                info.z = math.floor(z)
                                info.rx = rx
                                info.ry = ry
                                info.rz = rz
                                info.chuanjiabaoId = childfur.ChuanjiabaoId
                                info.ui8Scale = ui8Scale
                                local xPos = math.floor(x) + 0.5
                                local zPos = math.floor(z) + 0.5
                                local terrainY = CClientFurnitureMgr.Inst:GetLogicHeightByTemplateId(info.templateId,xPos, zPos)

                                if zdata.SubType == EnumFurnitureSubType_lua.eRollerCoasterTrack then
                                    terrainY = CHouseRollerCoasterMgr.GetBaseHeight()
                                end
                                info.terrainY = terrainY
                                if terrainY > highestTerrainY then
                                    highestTerrainY = terrainY
                                end
                                placeInfo[i+1] = info
                            end
                        end
                        for index,info in ipairs(placeInfo) do
                            if info and next(info) then
                                local y = info.y
                                local terrainY = info.terrainY
                                --rot值必然是整数，如果是89.9999之类的数，最后存的就是89了。
                                Gac2Gas.CreateAndAddFurniture(info.templateId,
                                    info.x,
                                    info.y + highestTerrainY - terrainY,
                                    info.z,
                                    info.rot,
                                    false,
                                    info.chuanjiabaoId,
                                    CommonDefs.EmptyDictionaryMsgPack,
                                    info.rx,
                                    info.ry,
                                    info.rz,
                                    info.ui8Scale
                                )
                            end
                        end
                    end
                    fur:Destroy()
                else
                    Gac2Gas.AddFurniture(fur.ID, fur.TemplateId,
                    math.floor(fur.Position.x),
                    0,
                    math.floor(fur.Position.z),
                    math.floor(fur.RO.transform.eulerAngles.y),
                    false,
                    fur.ChuanjiabaoId,
                    CommonDefs.EmptyDictionaryMsgPack)

                    if CFurnitureScene.IsOpenObstacleScaling() then
                        Gac2Gas.ScaleFurniture(fur.ID, CClientFurnitureMgr.GetScaleFloatToUI8(fur.RO.Scale)) -- 保存缩放
                    end
                end

            end
            if CClientFurnitureMgr.Inst.mbIsContinued and CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(fur.TemplateId) > 1 then
                CLuaClientFurnitureMgr.CreateFurnitureFromRepositoryContinued(fur)
            end
        end
        return true
    else
        if fur.RO == nil then
            return true
        end
        local yRot = math.floor(fur.RO.transform.eulerAngles.y)%360
        local x = fur.Position.x
        local z = fur.Position.z
        local y = fur.Position.y

        local xPos = math.floor(x) + 0.5
        local zPos = math.floor(z) + 0.5
        local terrainY = CClientFurnitureMgr.Inst:GetLogicHeightByTemplateId(fur.TemplateId,xPos, zPos)

		local isInYard = CClientHouseMgr.Inst:GetCurSceneType() == EnumHouseSceneType_lua.eYard
		local data = Zhuangshiwu_Zhuangshiwu.GetData(fur.TemplateId)
		if isInYard and data.Location == EnumFurnitureLocation_lua.eYardFenceAndRoomWall then
			terrainY = CRenderScene.Inst:SampleLogicHeight(xPos, zPos)
		end
        if data.SubType == EnumFurnitureSubType_lua.eRollerCoasterTrack then
            terrainY = CHouseRollerCoasterMgr.GetBaseHeight()
        end

        local yOffset = y - terrainY - CClientFurniture.s_YOffset
        --可以视为y方向没有移动
        if yOffset < 0.001 and yOffset>-0.001 then
            yOffset = 0
        end
        yOffset = CLuaHouseMgr.IsFurnitureAllowToChgY(data) and yOffset or 0

        if fur.EngineId ~= 0 then
            yRot = (math.floor(yRot / 90)) * 90
        end
        if CLuaHouseWoodPileMgr.IsWoodPile(fur.TemplateId) then
            Gac2Gas.RequestMoveWoodPile(fur.EngineId,x,z,yRot)
        else
            Gac2Gas.MoveFurniture(fur.ID, fur.TemplateId, x, yOffset, z, yRot, false)
            if CFurnitureScene.IsOpenObstacleScaling() then
                Gac2Gas.ScaleFurniture(fur.ID, CClientFurnitureMgr.GetScaleFloatToUI8(fur.RO.Scale)) -- 保存缩放
            end
        end

        CClientFurnitureMgr.Inst.CurFurniture = nil
        fur.RO.Visible = false
        return false
    end
end

function CLuaHouseMgr.RequestUseYangyangPingfeng(this)
    local pos = CreateFromClass(CPos, math.floor(this.RO.Position.x), math.floor(this.RO.Position.z))
    pos = Utility.GridPos2PixelPos(pos)

    CTrackMgr.Inst:Track(pos, Utility.Grid2Pixel(CClientFurniture.sFurnitureTrackPos), DelegateFactory.Action(function ()
        Gac2Gas.GetScreenVoteInfo()
    end), nil, true)
end
function CLuaHouseMgr.RequestPlaceBackFurniture(this)
    local bNeedDestroy = false

    --带engineId的不能删除
    if this.EngineId>0 then
        this:OnCantPlaceExistedFurniture()
    else
        if not this:IsPlacedInServer() then
            bNeedDestroy = true
        else
            this:OnCantPlaceExistedFurniture()
        end
    end

    this.Moving = false

    if bNeedDestroy then
        this:Destroy()
    end
end
function CLuaHouseMgr.StealCrop(this)
    if CClientMainPlayer.Inst == nil or this.m_Crop == nil then
        return
    end
    if System.String.IsNullOrEmpty(CClientMainPlayer.Inst.ItemProp.HouseId) then
        g_MessageMgr:ShowMessage("PLAYER_NO_HOUSE")
        return
    end
    if this.m_Crop.m_HarvestXiuwe > CClientMainPlayer.Inst.BasicProp.XiuWeiGrade then
        g_MessageMgr:ShowMessage("Harvest_Xiuwei_Not_Enough", tostring(this.m_Crop.m_HarvestXiuwe))
        return
    end
    local data = House_CangkuUpgrade.GetData(CClientHouseMgr.Inst.mConstructProp.CangkuGrade)
    if data == nil then
        return
    end
    if CClientHouseMgr.Inst:GetCurrentDayStealCropCount() >= data.MaxStealCrop then
        g_MessageMgr:ShowMessage("Player_Reach_Max_Steal_Count")
        return
    end
    --uint leftProduct, totalProduct;
    --m_Crop.GetProduct(out leftProduct, out totalProduct);
    --if (leftProduct <= Mathf.CeilToInt(totalProduct*House_Setting.GetData().Crop_Product_Num_Hold))
    --{
    --    MessageMgr.Inst.ShowMessage("Garden_Reach_Max_Steal_Count");
    --    return;
    --}
    local gardenIdx = this:GetGardenIndex()
    if CommonDefs.DictContains(CClientHouseMgr.Inst.mCurGardenProp.GardenList, typeof(Byte), gardenIdx) then
        local garden = CommonDefs.DictGetValue(CClientHouseMgr.Inst.mCurGardenProp.GardenList, typeof(Byte), gardenIdx)
        if CommonDefs.DictContains(garden.StealInfo, typeof(UInt64), CClientMainPlayer.Inst.Id) and CommonDefs.DictGetValue(garden.StealInfo, typeof(UInt64), CClientMainPlayer.Inst.Id) >= 3 then
            g_MessageMgr:ShowMessage("Player_Reach_Garden_Max_Steal_Count")
            return
        end
    end
    Gac2Gas.RequestStealCrop(gardenIdx)
    PreventPluginMgr:CheckActivate("StealCrop", {})
end

function CLuaHouseMgr.RequestReviveCrop(this)
    local str = g_MessageMgr:FormatMessage("Rebirth_Confirm", tostring(House_Setting.GetData().CropRevivePrice))
    MessageWndManager.ShowOKCancelMessage(str, DelegateFactory.Action(function ()
        Gac2Gas.RequestReviveCrop(this:GetGardenIndex())
    end), nil, nil, nil, false)
end

function CLuaHouseMgr.IsUsedByBaby(this, zswData)
    if CClientMainPlayer.Inst == nil then
        return false
    end
    if zswData.SlotNum > 1 then
        return false
    end
    if this.Children ~= nil then
        do
            local i = 0
            while i < this.Children.Length do
                local obj = CClientObjectMgr.Inst:GetObject(math.floor(this.Children[i]))
                if obj ~= nil and TypeIs(obj, typeof(CClientNpc)) then
                    local npc = TypeAs(obj, typeof(CClientNpc))
                    return BabyMgr.Inst:IsBaby(npc.TemplateId)
                end
                i = i + 1
            end
        end
    end
    return false
end

function CLuaHouseMgr.GetWoodPilePopupMenuItemData(itemActionPairs,this)
    if this.Moving then
        table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
            if CLuaHouseWoodPileMgr.IsFighting(this.EngineId) then
		        g_MessageMgr:ShowMessage("PILE_FIGHT_WITH_OTHER")
                CLuaHouseMgr.RequestPlaceBackFurniture(this)
                return
            end
            local bCanPut = this:CheckMoveFurniture()
            if bCanPut then
                if this.RO == nil then
                    this:Destroy()
                else
                    local yRot = math.floor(this.RO.transform.eulerAngles.y)
                    local x = this.RO.Position.x
                    local z = this.RO.Position.z
                    local y = this.RO.Position.y

                    if this.EngineId ~= 0 then
                        yRot = (math.floor(yRot / 90)) * 90
                    end

                    Gac2Gas.RequestMoveWoodPile(this.EngineId, math.floor(x), math.floor(z), yRot)
                    CClientFurnitureMgr.Inst.CurFurniture = nil
                end
                this.Moving = false
                MouseInputHandler.Inst:ClearFurnitureOffset()
            else
                g_MessageMgr:ShowMessage("FURNITURE_CANNOT_PUT_IN_PLACE")
            end
        end), "UI/Texture/Transparent/Material/housepopupmenu_loc.mat", true, true, -1))

        table.insert( itemActionPairs,   HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
            CLuaHouseMgr.RequestPlaceBackFurniture(this)
        end), "UI/Texture/Transparent/Material/housepopupmenu_delete.mat", true, true, -1))

        table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
            local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(this.TemplateId)
            local msg = SafeStringFormat3(LocalString.GetString("确定将装饰物%s收起吗？"), zswdata.Name)
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
                Gac2Gas.RequestPackWoodPile(this.EngineId)
                CUIManager.CloseUI(CUIResources.HousePopupMenu)
            end), nil, nil, nil, false)
        end), "UI/Texture/Transparent/Material/housepopupmenu_shouhuicangku.mat", true, true, -1))

        table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
            CLuaHouseMgr.RequestRotateFurniture(this)
        end), "UI/Texture/Transparent/Material/housepopupmenu_xuanzhuan.mat", false, true, -1))

        return itemActionPairs
    else
        local engineId = this.EngineId
        local obj = CClientObjectMgr.Inst:GetObject(this.EngineId)
        if obj then
            if CLuaHouseWoodPileMgr.IsFightingWithSelf(engineId) then
                table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                    Gac2Gas.RequestStopFightWithPile(engineId)

                    if CLuaHouseWoodPileMgr.m_Damages then
                        local pileId = CommonDefs.DictGetValue_LuaCall(CClientFurnitureMgr.Inst.WoodPileClientObjectList,engineId)
                        --停止战斗把该木桩的战斗信息去掉
                        if CLuaHouseWoodPileMgr.m_Damages[pileId] then
                            CLuaHouseWoodPileMgr.m_Damages[pileId] = nil
                        end
                        if not next(CLuaHouseWoodPileMgr.m_Damages) then
                            CUIManager.CloseUI(CLuaUIResources.HouseWoodPileStatusWnd)
                        end
                    end
                end), "UI/Texture/Transparent/Material/housepopupmenu_stop.mat", true, true, -1))
            else
                --切磋
                table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                    Gac2Gas.RequestFightWithWoodPile(engineId)
                    --显示界面
                    CLuaHouseWoodPileMgr.StartFightWithWoodPile(this.EngineId,this.TemplateId)
                end), "UI/Texture/Transparent/Material/housepopupmenu_qiecuo.mat", true, true, -1))
                --历史
                table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                    CLuaHouseWoodPileDataHistoryWnd.m_EngineId = engineId
                	local pileId = CommonDefs.DictGetValue_LuaCall(CClientFurnitureMgr.Inst.WoodPileClientObjectList,engineId)
                    CLuaHouseWoodPileDataHistoryWnd.m_PileId = pileId
                    CLuaHouseWoodPileDataHistoryWnd.m_PileTemplateId = this.TemplateId
                    CUIManager.ShowUI(CLuaUIResources.HouseWoodPileDataHistoryWnd)
                end), "UI/Texture/Transparent/Material/housepopupmenu_tongji.mat", true, true, -1))

                --查看
                table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                    CLuaHouseWoodPileSwitchLevelWnd.m_EngineId = engineId
                	local pileId = CommonDefs.DictGetValue_LuaCall(CClientFurnitureMgr.Inst.WoodPileClientObjectList,engineId)
                    CLuaHouseWoodPileSwitchLevelWnd.m_PileId = pileId
                    CLuaHouseWoodPileSwitchLevelWnd.m_PileTemplateId = this.TemplateId
                    CUIManager.ShowUI(CLuaUIResources.HouseWoodPileSwitchLevelWnd)
                end), "UI/Texture/Transparent/Material/housepopupmenu_shezhi.mat", true, true, -1))
            end
        end
        return itemActionPairs
    end
end
function CLuaHouseMgr.GetPopupMenuItemData(this)
    -- print("GetPopupMenuItemData",this)
    local itemActionPairs = {}-- CreateFromClass(MakeGenericClass(List, HousePopupMenuItemData))

    --家园木桩
    if this.IsWoodPile then
        return CLuaHouseMgr.GetWoodPilePopupMenuItemData(itemActionPairs,this)
    end

    if this.SpePuppet then
        if this.Moving then
            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                CLuaHouseMgr.RequestPlacePuppet(this)
            end), "UI/Texture/Transparent/Material/housepopupmenu_loc.mat", true, true, -1))

            table.insert( itemActionPairs,   HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                CLuaHouseMgr.RequestPlaceBackFurniture(this)
            end), "UI/Texture/Transparent/Material/housepopupmenu_delete.mat", true, true, -1))

            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                CLuaHouseMgr.RequestRotateFurniture(this)
            end), "UI/Texture/Transparent/Material/housepopupmenu_xuanzhuan.mat", false, true, -1))
            return itemActionPairs
        else
            local obj = CClientObjectMgr.Inst:GetObject(this.EngineId)
            if obj ~= nil then
                if CHousePuppetMgr.Inst:CheckPuppetInFight(obj) then
                    table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                        -- this:RequestStopFightWithPuppet()
                        Gac2Gas.RequestStopFightWithPuppet(this.EngineId)
                    end), "UI/Texture/Transparent/Material/housepopupmenu_stop.mat", true, true, -1))
                else
                    local idx = 0
                    if CommonDefs.DictContains(CHousePuppetMgr.Inst.EngineToIdxDic, typeof(UInt32), this.EngineId) then
                        idx = CommonDefs.DictGetValue(CHousePuppetMgr.Inst.EngineToIdxDic, typeof(UInt32), this.EngineId)
                    end

                    if CClientHouseMgr.Inst.mCurFurnitureProp ~= nil and CommonDefs.DictContains(CClientHouseMgr.Inst.mCurFurnitureProp.PuppetInfo, typeof(Byte), idx) then
                        local puppetInfo = CommonDefs.DictGetValue(CClientHouseMgr.Inst.mCurFurnitureProp.PuppetInfo, typeof(Byte), idx)
                        local puppetData = HousePuppet_CPPuppet.GetData(puppetInfo.Id)
                        if puppetData then
                          --if puppetData.RoleIdx < 100 then
                            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                              --RequestCPPuppetDetailInfo(cf);
                              -- this:RequestPuppetDetailInfo(this.EngineId)
                              Gac2Gas.RequestViewPuppetDetailInfo(this.EngineId)
                            end), "UI/Texture/Transparent/Material/housepopupmenu_shuxing.mat", true, true, -1))
                          --end
                        else
                          table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                            --RequestCPPuppetDetailInfo(cf);
                            -- this:RequestPuppetDetailInfo(this.EngineId)
                            Gac2Gas.RequestViewPuppetDetailInfo(this.EngineId)
                          end), "UI/Texture/Transparent/Material/housepopupmenu_shuxing.mat", true, true, -1))
                        end
                        --CPuppetFurniture cf = CClientHouseMgr.Inst.mCurFurnitureProp.PuppetInfo[(byte)idx];
                    elseif CClientHouseMgr.Inst.mCurFurnitureProp ~= nil and CommonDefs.DictContains(CClientHouseMgr.Inst.mCurFurnitureProp.FriendPuppetInfo, typeof(Byte), idx) then
                        table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                            -- this:RequestPuppetDetailInfo(this.EngineId)
                            Gac2Gas.RequestViewPuppetDetailInfo(this.EngineId)
                        end), "UI/Texture/Transparent/Material/housepopupmenu_shuxing.mat", true, true, -1))
                    end

                    table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                        -- this:RequestFightWithPuppet()
                        Gac2Gas.RequestFightWithPuppet(this.EngineId)
                    end), "UI/Texture/Transparent/Material/housepopupmenu_qiecuo.mat", true, true, -1))

                    table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                        CLuaHouseMgr.RequestFightHis(this.EngineId)
                    end), "UI/Texture/Transparent/Material/housepopupmenu_tongji.mat", true, true, -1))

                    if CClientHouseMgr.Inst.mCurFurnitureProp ~= nil and CommonDefs.DictContains(CClientHouseMgr.Inst.mCurFurnitureProp.FriendPuppetInfo, typeof(Byte), idx) then
                        table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                            -- this:RequestInteractWithPuppt()
                            CHousePuppetMgr.Inst.SaveInteractPuppetEngineId = this.EngineId
                            CUIManager.ShowUI(CUIResources.PuppetInteractWnd)
                        end), "UI/Texture/Transparent/Material/housepopupmenu_hudong.mat", true, true, -1))
                      elseif CommonDefs.DictContains(CClientHouseMgr.Inst.mCurFurnitureProp.PuppetInfo, typeof(Byte), idx) and CHousePuppetMgr.EnableCPPuppetChangeDress then
                        local puppetInfo = CommonDefs.DictGetValue(CClientHouseMgr.Inst.mCurFurnitureProp.PuppetInfo, typeof(Byte), idx)
                        local puppetData = HousePuppet_CPPuppet.GetData(puppetInfo.Id)
                        if puppetData then
                          if puppetData.RoleIdx < 100 then
                            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                              CPuppetClothesMgr.OpenClothesPress(idx)
                            end), "UI/Texture/Transparent/Material/housepopupmenu_fashion_tag_dress.mat", true, true, -1))
                          end
                        else
                          table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                            CPuppetClothesMgr.OpenClothesPress(idx)
                          end), "UI/Texture/Transparent/Material/housepopupmenu_fashion_tag_dress.mat", true, true, -1))
                        end
                      end

                    return itemActionPairs
                    --TODO
                end
            end
        end
    end

    local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(this.TemplateId)

    if not CClientFurnitureMgr.Inst.FurnishMode and zswdata then
        if zswdata.SubType == EnumFurnitureSubType_lua.eRollerCoasterStation then
            CLuaHouseMgr.TryAddZhanTaiUseMenu(this,zswdata,itemActionPairs)
        end
    end

    if CLuaHouseMgr.IsInHuyitianHouse() or CHouseCompetitionMgr.Inst:IsInFusaiPlay() then
        return CLuaHouseMgr.GetHuyitianHousePopupMenuItemData(this,zswdata,itemActionPairs)
    end

    if CYangyangPhotoVoteMgr.sIsInYangyangHouse then
        return CLuaHouseMgr.GetYangyangHousePopupMenuItemData(this)
    end


    if not CClientHouseMgr.Inst:IsPlayerInHouse() and CScene.MainScene then
        local mapDesignData = PublicMap_PublicMap.GetData(CScene.MainScene.SceneTemplateId)
        if not (mapDesignData ~= nil and mapDesignData.EnableFurnitureInteract > 0 and zswdata ~= nil and CommonDefs.StringIndexOf_String(zswdata.Action, "PlayerInteractWithFurniture") ~= - 1) then
            return itemActionPairs
        end
    end

    if CClientFurnitureMgr.Inst.FurnishMode and zswdata ~= nil then

        -- 过山车站台的装载按钮
        if zswdata.SubType == EnumFurnitureSubType_lua.eRollerCoasterStation then 
            CLuaHouseMgr.ProcessZhanTaiPopupMenu(this,zswdata,itemActionPairs)

        end

        if zswdata.UseIcon ~= nil and this:IsPlacedInServer() and not this:IsDead() then
            do
                local i = 0
                while i < zswdata.UseIcon.Length do
                    local displayMode = 0
                    local default
                    default, displayMode = System.UInt32.TryParse(zswdata.UseIcon[i])
                    if default then
                        local bCheckOwner = (displayMode >= 10)
                        if not bCheckOwner or CClientHouseMgr.Inst:IsInOwnHouse() then
                            if (displayMode % 10 == 2 or displayMode % 10 == 3) then
                                local idx = math.floor(i / 2)
                                table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                                    this:RequestUseFurniture(idx, zswdata)
                                end), zswdata.UseIcon[i + 1], true, true, -1))
                            end
                        end
                    end
                    i = i + 2
                end
            end
        end

        -- 确认、取消按钮
        if this.Moving then
            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                CLuaHouseMgr.RequestPlaceFurniture(this)
            end), "UI/Texture/Transparent/Material/housepopupmenu_loc.mat", true, true, -1))

            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                CLuaHouseMgr.RequestPlaceBackFurniture(this)
            end), "UI/Texture/Transparent/Material/housepopupmenu_delete.mat", true, true, -1))
        end

        -- 收回仓库按钮
        CLuaHouseMgr.TryAddPackMenu(this,zswdata,itemActionPairs)

        if this.Moving then
            CLuaHouseMgr.ProcessWinterHousePopupMenu(this,itemActionPairs)
        end

        -- 旋转按钮
        CLuaHouseMgr.ProcessRotate(this,zswdata,itemActionPairs)

        -- 有障碍装饰物绕Y轴360°旋转与缩放
        if CFurnitureScene.IsOpen360Rotation() then
            -- 可绕Y轴360°任意角旋转的有障碍装饰物(不可悬浮,即不可绕任意轴360°旋转)
            if zswdata.Revolve90 ~= 0 and (CLuaHouseMgr.s_EnableAdjustmentWhenAddFurniture or this:IsPlacedInServer()) and not CLuaHouseMgr.CanXuanFuByDesign(zswdata) then
                table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                    CLuaHouseMgr.RequestAdjustObstacleFurniture(this, zswdata.CanScale ~= 0)
                end), (CFurnitureScene.IsOpenObstacleScaling() and zswdata.CanScale ~= 0)
                    and "UI/Texture/Transparent/Material/housepopupmenu_fangsuo.mat" 
                    or "UI/Texture/Transparent/Material/housepopupmenu_xuanzhuan2.mat", false, true, -1))
            end
        end

        if zswdata.SubType == EnumFurnitureSubType_lua.eGuashi and this:IsPlacedInServer() then
            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                CLuaHouseMgr.RequestShiftFurniture(this)
            end), "UI/Texture/Transparent/Material/housepopupmenu_huadong.mat", false, true, -1))
        end

        -- 控制悬浮装饰物的按钮
        CLuaHouseMgr.ProcessXuanFu(this,zswdata,itemActionPairs)

        if CClientFurnitureMgr.Inst:IsPenjingFurniture(this.TemplateId) and this:IsPlacedInServer() then
            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                this:RequestUsePenzai()
            end), "UI/Texture/Transparent/Material/housedeco_006.mat", false, true, -1))
        end

        -- 详情按钮（当前只有确认、取消、旋转三个按钮时显示）
        if #itemActionPairs == 3 or CLuaHouseMgr.s_EnableAdjustmentWhenAddFurniture and not this:IsPlacedInServer() then
            if this.Moving and CLuaHouseMgr.ClickItemInfo ~= nil then
                table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                    CLuaHouseMgr.RequestShowInfo(this)
                end), "UI/Texture/Transparent/Material/common_checkdetail.mat", false, true, -1))
            end
        end
        
        if this.Moving then
            CLuaHouseMgr.ProcessChangeColorPopupMenu(this,itemActionPairs)
        end

        --是否示连铺按钮
        if this.Moving then            
            CLuaHouseMgr.ProcessLianPuPopupMenu(this,itemActionPairs)
        end
    else
        --如果是床的话，则增加认床按钮
        if zswdata.Type == LuaEnumFurnitureSubType.eChuangta then
            CLuaHouseMgr.ProcessChuangTaPopupMenu(this,itemActionPairs)
        end

        if zswdata.Type == LuaEnumFurnitureSubType.eZhuoan then
            CLuaHouseMgr.GetChildrenZhuoanActionPairs(this,itemActionPairs)
        end
        
        CLuaHouseMgr.ProcessNotUsed(this,zswdata,itemActionPairs)
        CLuaHouseMgr.ProcessBaby(this,zswdata,itemActionPairs)

        if this:IsDead() then
            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                -- this:RequestReviveLingshouFurniture()
                Gac2Gas.ReviveLingshouFurniture(this.ID)
            end), "UI/Texture/Transparent/Material/housepopupmenu_fusheng.mat", true, true, -1))

            CLuaHouseMgr.ShowLingshouKillerMenu(this,itemActionPairs)
        end

        if zswdata.SubType == EnumFurnitureSubType_lua.eMiaopu then
            CLuaHouseMgr.GetGardenMenu(this,itemActionPairs)
            CClientFurnitureMgr.Inst.m_SelectedGardenIdx = this:GetGardenIndex()
        elseif CClientHouseMgr.Inst:IsInOwnHouse() and zswdata.SubType == 13 then--eCangku CheckCanUseCangku
            if not CClientFurniture.sbDisableJianzhuPopMenu then
                table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                    CLuaHouseMgr.RequestUseCangku(this)
                end), "UI/Texture/Transparent/Material/housepopupmenu_duihuan.mat", true, true, -1))
            end
        elseif zswdata.SubType == LuaEnumFurnitureSubType.eZhengwu then--eZhengwu CheckCanUseZhengwu
            if not CClientFurniture.sbDisableJianzhuPopMenu then
                table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                    CLuaHouseMgr.RequestUseZhengwu(this)
                end), "UI/Texture/Transparent/Material/housepopupmenu_jinru.mat", true, true, -1))
            end
        elseif zswdata.SubType == LuaEnumFurnitureSubType.eXiangfang then--eXiangfang CheckCanUseXiangfang
            if not CClientFurniture.sbDisableJianzhuPopMenu then
                table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                    CLuaHouseMgr.RequestUseXiangfang(this)
                end), "UI/Texture/Transparent/Material/housepopupmenu_battletype.mat", true, true, -1))
            end
        end

        if zswdata.SubType == LuaEnumFurnitureSubType.eXiangfang and CClientHouseMgr.Inst:IsCurHouseOwner() then--eXiangfang CheckCanInviteRoomer
            if not CClientFurniture.sbDisableJianzhuPopMenu then
                table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                    -- this:RequestInviteTongzhuPlayer()
                    Gac2Gas.QueryFriendsFitTongzhuRequirement()
                end), "UI/Texture/Transparent/Material/housepopupmenu_user.mat", true, true, -1))
            end
        end

        -- if this:CheckCanEnterXiangfang(zswdata) then
        if zswdata.SubType == LuaEnumFurnitureSubType.eXiangfang and CClientHouseMgr.Inst:IsInOwnHouse() then--eXiangfang
            if not CClientFurniture.sbDisableJianzhuPopMenu then
                table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                    Gac2Gas.RequestEnterXiangfang()
                end), "UI/Texture/Transparent/Material/housepopupmenu_jinru.mat", true, true, -1))
            end
        end

        if CClientFurnitureMgr.Inst:IsMatongFurnitureId(this.TemplateId) then
            local status = CClientFurnitureMgr.Inst:CheckMatongStatus(this.ID)
            if status == EnumClosestoolStatus.eNone then
                table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                    CLuaHouseMgr.RequestInteractWithMatong(this,status)
                end), "UI/Texture/Transparent/Material/housewnd_shengchan.mat", true, true, -1))
            elseif status == EnumClosestoolStatus.eReady then
                table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                    CLuaHouseMgr.RequestInteractWithMatong(this,status)
                end), "UI/Texture/Transparent/Material/housepopupmenu_lingqu.mat", true, true, -1))
            end
        end
        local  setting = Zhuangshiwu_Setting.GetData()
        -- if this:CheckIsMapiFurniture(zswdata) then
        if zswdata.ID == setting.MapiTemplateId then
            table.insert( itemActionPairs,  HousePopupMenuItemData(LocalString.GetString("抚摸"), DelegateFactory.Action_int(function (index)
                this:RequestInteractWithMapi(EnumMapiActionType.eTouch)
            end), "UI/Texture/Transparent/Material/housepopupmenu_fumo.mat", true, true, -1))

            table.insert( itemActionPairs,  HousePopupMenuItemData(LocalString.GetString("洗刷"), DelegateFactory.Action_int(function (index)
                this:RequestInteractWithMapi(EnumMapiActionType.eWash)
            end), "UI/Texture/Transparent/Material/housepopupmenu_xishua.mat", true, true, -1))

            table.insert( itemActionPairs,  HousePopupMenuItemData(LocalString.GetString("喂食"), DelegateFactory.Action_int(function (index)
                this:RequestInteractWithMapi(EnumMapiActionType.eFeed)
            end), "UI/Texture/Transparent/Material/housepopupmenu_weishi.mat", true, true, -1))
        end

        -- if this:CheckIsMapiBabyFurniture(zswdata) then
        if zswdata.ID == setting.MapiBabyTemplateId then
            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                if this.EngineId ~= 0 then
                    Gac2Gas.RequestGetMapiBaby(this.ID)
                end
            end), "UI/Texture/Transparent/Material/housepopupmenu_lingqu.mat", true, true, -1))
        end

        -- if this:CheckCanUpdateBedBeiziStatus(zswdata) then
        if CLuaHouseMgr.CheckCanUpdateBedBeiziStatus(this,zswdata) then
            if CClientFurnitureMgr.Inst:CheckBedBeiziFurnitureStatus(this.ID) then
                table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                    CLuaHouseMgr.RequestUpdateBedBeiziStatus(this,false)
                end), "UI/Texture/Transparent/Material/housepopupmenu_xianbeizi.mat", true, true, -1))
            else
                table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                    CLuaHouseMgr.RequestUpdateBedBeiziStatus(this,true)
                end), "UI/Texture/Transparent/Material/housepopupmenu_gaibeizi.mat", true, true, -1))
            end
        end
    end
    return itemActionPairs
end
function CLuaHouseMgr.TryAddZhanTaiUseMenu(this,zswdata,itemActionPairs)
    local fur = CClientFurnitureMgr.Inst:GetFurniture(this.ID)
    if fur and fur.Children and fur.Children.Length>0 then
        local childId = fur.Children[0]
        if childId>0 then
            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                --上车
                LuaHouseRollerCoasterMgr.UseHuaChe(childId,0)
            end), "UI/Texture/Transparent/Material/housepopupmenu_shangche.mat", true, true, -1))

            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                LuaHouseRollerCoasterMgr.UseHuaChe(childId,1)
            end), "UI/Texture/Transparent/Material/babyclothwnd_babyuse.mat", true, true, -1))
        end
    end
end


function CLuaHouseMgr.TryAddPackMenu(this,zswdata,itemActionPairs)
    if zswdata.SubType~=12 and zswdata.SubType~=13 and zswdata.SubType~=14 and zswdata.ID~=7011 and zswdata.ID~=7012 and this:IsPlacedInServer() then
        table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
            -- this:RequestPackFurniture(zswdata)
            if this:IsFurnitureInUse() then
                g_MessageMgr:ShowMessage("Cant_Pack_Furniture_In_Use")
                return
            elseif this:IsFurnitureHasDecoration() then
                g_MessageMgr:ShowMessage("Cant_Pack_Furniture_With_Decoration")
                return
            elseif this.m_Crop ~= nil then
                g_MessageMgr:ShowMessage("Cant_Pack_Garden_With_Crop")
                return
            end
			if zswdata.ID==9448 then--温泉池 看下是否有温泉装饰物在上面
                if IsAnythingOnWenQuanChi(this.ID,zswdata.SpaceWidth,zswdata.SpaceHeight) then
                    g_MessageMgr:ShowMessage("Cant_Pack_Furniture_With_Decoration")
                    return
                end
            end

            local name = zswdata.Name

            local hasColor = false
            local msg = SafeStringFormat3(LocalString.GetString("确定将装饰物%s收起吗？"), name)
            if Zhuangshiwu_ZhuangshiwuColor.Exists(this.FurnishTemplateId) then
                local furnitureColor =  CClientHouseMgr.Inst and CClientHouseMgr.Inst.mCurFurnitureProp2.FurnitureColor
                if furnitureColor then
                    local color = CommonDefs.DictGetValue_LuaCall(furnitureColor,this.ID)
                    if color then
                        msg = g_MessageMgr:FormatMessage("FurnitureColor_Pack_Confirm", name)
                    end
                end
            end
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
                if System.String.IsNullOrEmpty(this.ChuanjiabaoId) then
                    Gac2Gas.ExchangeFurniture(EnumFurniturePlace_lua.eFurniture, this.ID, EnumFurniturePlace_lua.eRepository, 0, CClientMainPlayer.Inst.Id, "")
                else
                    Gac2Gas.ExchangeFurniture(EnumFurniturePlace_lua.eFurniture, this.ID, EnumFurniturePlace_lua.eChuanjiabao, this.ID, CClientMainPlayer.Inst.Id, this.ChuanjiabaoId)
                end
                CUIManager.CloseUI(CUIResources.HousePopupMenu)
            end), nil, nil, nil, false)
        end), "UI/Texture/Transparent/Material/housepopupmenu_shouhuicangku.mat", true, true, -1))
    end
end

function CLuaHouseMgr.IsFurnitureAllowToChgY(zswdata)
	return zswdata.SubType == EnumFurnitureSubType_lua.eHuamu or zswdata.RevolveSwitch == 1 or
        zswdata.SubType == EnumFurnitureSubType_lua.eRollerCoasterTrack or zswdata.SubType == EnumFurnitureSubType.eGuashi or zswdata.SubType == EnumFurnitureSubType.eZawu or zswdata.SubType == EnumFurnitureSubType.eXiaojing
end

function CLuaHouseMgr.CanXuanFuByDesign(zswdata)
    return zswdata.SubType == EnumFurnitureSubType_lua.eHuamu or zswdata.RevolveSwitch == 1 or
        zswdata.SubType == EnumFurnitureSubType_lua.eRollerCoasterTrack
end

function CLuaHouseMgr.CheckCanXuanFu(this, zswdata)
    if (zswdata.SubType == EnumFurnitureSubType_lua.eHuamu or zswdata.RevolveSwitch == 1 or
        zswdata.SubType == EnumFurnitureSubType_lua.eRollerCoasterTrack) and this:IsPlacedInServer() then
        return true
    end
    return false
end

function CLuaHouseMgr.ProcessXuanFu(this,zswdata,itemActionPairs)
    if CClientMainPlayer.Inst == nil then
        return
    end
    local xuanfu_icon_name = "UI/Texture/Transparent/Material/housepopupmenu_suofang.mat"
    if CClientMainPlayer.Inst.BasicProp.IsOpenHouseXuanFuEdit>0 then
        xuanfu_icon_name = "UI/Texture/Transparent/Material/housepopupmenu_xuanfu.mat"
    end
    if zswdata.SubType == EnumFurnitureSubType_lua.eRollerCoasterTrack then -- 过山车只能调高度
        xuanfu_icon_name = "UI/Texture/Transparent/Material/housepopupmenu_huadong.mat"
    end
    if CLuaHouseMgr.CheckCanXuanFu(this, zswdata) then
        table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
            CLuaHouseMgr.RequestScaleFurniture(this)
        end), xuanfu_icon_name, false, true, -1))
    end
    -- if zswdata.SubType == EnumFurnitureSubType_lua.eHuamu and this:IsPlacedInServer() then
    --     table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
    --         CLuaHouseMgr.RequestScaleFurniture(this)
    --     end), "UI/Texture/Transparent/Material/housepopupmenu_suofang.mat", false, true, -1))
    -- end
end


function CLuaHouseMgr.ProcessZhanTaiPopupMenu(this,zswdata,itemActionPairs)
    if this:IsPlacedInServer() and not this:IsDead() then
        --站台特殊处理
        table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
            LuaHouseRollerCoasterMgr.UseZhanTai(this.ID)
        end), "UI/Texture/Transparent/Material/housepopupmenu_zhuangzai.mat", true, true, -1))
    end
end

function CLuaHouseMgr.CheckCanUpdateBedBeiziStatus(this, zswdata)
    if CClientMainPlayer.Inst == nil then
        return false
    end
    if not CommonDefs.DictContains(CClientFurnitureMgr.Inst.sBedBeiziFurnitureList, typeof(UInt32), zswdata.ID) then
        return false
    end

    local count = 0
    if this.Children ~= nil and this.Children.Length == 2 then
        for i = 0, 1 do
            if this.Children[i] ~= 0 then
                count = count + 1
            end
        end
    end
    return count > 0
end

-- 公共场景的装饰物不支持宝宝的交互行为，所以这里特殊处理，公共场景如果使用了这种装饰物，就屏蔽交互按钮
function CLuaHouseMgr.IsHideBabyUseButton()
    local id = CScene.MainScene and CScene.MainScene.SceneTemplateId or 0
    return id == 16000505 or id == 16000506
end

function CLuaHouseMgr.ProcessNotUsed(this,zswdata,itemActionPairs)
    if zswdata.UseIcon ~= nil and not this:IsDead() and not CLuaHouseMgr.IsUsedByMainPlayer(this) and not CLuaHouseMgr.IsUsedByBaby(this,zswdata) then
        local hideBabyButton = CLuaHouseMgr.IsHideBabyUseButton()
        do
            local i = 0
            while i < zswdata.UseIcon.Length do
                local displayMode = 0
                local extern
                extern, displayMode = System.UInt32.TryParse(zswdata.UseIcon[i])
                if extern then
                    local actions = g_LuaUtil:StrSplit(zswdata.Action,";")
                    local bCheckOwner = (displayMode >= 10)
                    if not bCheckOwner or CClientHouseMgr.Inst:IsInOwnHouse() then
                        if displayMode % 10 == 1 or displayMode % 10 == 3 then
                            local idx = math.floor(i / 2)
                            local isClickClose = true
                            local drumList = Zhuangshiwu_Setting.GetData().DrumFurnitureTempId
                            do
                                local j = 0
                                while j < drumList.Length do
                                    if zswdata.FurnishTemplate == drumList[j] then
                                        isClickClose = false
                                        break
                                    end
                                    j = j + 1
                                end
                            end
                            if hideBabyButton and string.find( actions[idx+1],"BabyInteractWithFurniture") then
                            else
                                table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                                    this:RequestUseFurniture(idx, zswdata)
                                end), zswdata.UseIcon[i + 1], isClickClose, true, -1))
                            end
                        end
                    end
                end
                i = i + 2
            end
        end
    end
end
function CLuaHouseMgr.ProcessBaby(this,zswdata,itemActionPairs)
    if CLuaHouseMgr.IsHideBabyUseButton() then return end

    if zswdata.UseIcon ~= nil and not this:IsDead() and zswdata.BabyUse==1 then
        local actions = g_LuaUtil:StrSplit(zswdata.Action,";")
        -- 宝宝使用中，去掉宝宝使用的按钮
        if CLuaHouseMgr.IsUsedByBaby(this,zswdata) and not CLuaHouseMgr.IsUsedByMainPlayer(this) then
            for i=1,zswdata.UseIcon.Length,2 do
                local displayMode = tonumber(zswdata.UseIcon[i-1])
                local bCheckOwner = (displayMode >= 10)
                if not bCheckOwner or CClientHouseMgr.Inst:IsInOwnHouse() then
                    if (displayMode % 10 == 1 or displayMode % 10 == 3) then
                        local idx = math.floor((i-1) / 2)
                        if not string.find( actions[idx+1],"BabyInteractWithFurniture") then
                            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                                this:RequestUseFurniture(idx, zswdata)
                            end), zswdata.UseIcon[i], true, true, -1))
                        end
                    end
                end
            end
        end
        -- 玩家使用中（且是宝宝可使用的家具），添加宝宝使用的按钮
        if CLuaHouseMgr.IsUsedByMainPlayer(this) and not CLuaHouseMgr.IsUsedByBaby(this,zswdata) then
            for i=1,zswdata.UseIcon.Length,2 do
                local displayMode = tonumber(zswdata.UseIcon[i-1])
                local bCheckOwner = (displayMode >= 10)
                if not bCheckOwner or CClientHouseMgr.Inst:IsInOwnHouse() then
                    if (displayMode % 10 == 1 or displayMode % 10 == 3) then
                        local idx = math.floor((i-1) / 2)
                        if string.find( actions[idx+1],"BabyInteractWithFurniture") then
                            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                                this:RequestUseFurniture(idx, zswdata)
                            end), zswdata.UseIcon[i], true, true, -1))
                        end
                    end
                end
            end
        end
    end
end


function CLuaHouseMgr.GetYangyangHousePopupMenuItemData(this)
    local itemActionPairs = {}--CreateFromClass(MakeGenericClass(List, HousePopupMenuItemData))

    local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(this.TemplateId)

    if zswdata == nil then
        return itemActionPairs
    end

    local yangyangpingfeng = HouseOfYangyang_Setting.GetData().FurnitureForVote
    if this.TemplateId == yangyangpingfeng then
        table.insert( itemActionPairs, HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
            CLuaHouseMgr.RequestUseYangyangPingfeng(this)
        end), "UI/Texture/Transparent/Material/housepopupmenu_shiyong.mat", true, true, -1))
    end

    if zswdata.UseIcon ~= nil and zswdata.Action ~= nil then
        -- local spliter = Table2ArrayWithCount({";"}, 1, MakeArrayClass(String))
        local actions = CommonDefs.StringSplit_ArrayChar(zswdata.Action, ";")
        do
            local i = 0
            while i < zswdata.UseIcon.Length do
                local displayMode = 0
                local default
                default, displayMode = System.UInt32.TryParse(zswdata.UseIcon[i])
                if default then
                    local bCheckOwner = (displayMode >= 10)
                    if not bCheckOwner or CClientHouseMgr.Inst:IsInOwnHouse() then
                        if (displayMode % 10 == 1 or displayMode % 10 == 3) and CommonDefs.StringIndexOf_String(actions[math.floor(i / 2)], "PlayerInteractWithFurniture") ~= - 1 then
                            local idx = math.floor(i / 2)
                            table.insert( itemActionPairs, HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                                this:RequestUseFurniture(idx, zswdata)
                            end), zswdata.UseIcon[i + 1], true, true, -1))
                        end
                    end
                end
                i = i + 2
            end
        end
    end

    if zswdata.SubType == LuaEnumFurnitureSubType.eZhengwu and not CClientFurniture.sbDisableJianzhuPopMenu then
        CommonDefs.ListAdd(itemActionPairs, typeof(HousePopupMenuItemData), HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
            if this.RO == nil then
                return
            end
            local pos = CreateFromClass(CPos, math.floor(this.RO.Position.x), math.floor(this.RO.Position.z))
            pos = Utility.GridPos2PixelPos(pos)

            CTrackMgr.Inst:Track(pos, Utility.Grid2Pixel(CClientFurniture.sHouseGridTrackPos), DelegateFactory.Action(function ()
                Gac2Gas.RequestEnterYangYangRoom()
            end), nil, true)
        end), "UI/Texture/Transparent/Material/housepopupmenu_jinru.mat", true, true, -1))
    end

    if CLuaHouseMgr.CheckCanUpdateBedBeiziStatus(this,zswdata) then
        if CClientFurnitureMgr.Inst:CheckBedBeiziFurnitureStatus(this.ID) then
            table.insert( itemActionPairs, HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                CLuaHouseMgr.RequestUpdateBedBeiziStatus(this,false)
            end), "UI/Texture/Transparent/Material/housepopupmenu_xianbeizi.mat", true, true, -1))
        else
            table.insert( itemActionPairs, HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                CLuaHouseMgr.RequestUpdateBedBeiziStatus(this,true)
            end), "UI/Texture/Transparent/Material/housepopupmenu_gaibeizi.mat", true, true, -1))
        end
    end

    return itemActionPairs
end
function CLuaHouseMgr.RequestUpdateBedBeiziStatus(this,bStatus)
    Gac2Gas.RequestUpdateBedBeiziStatus(this.ID, bStatus)
    CClientFurnitureMgr.Inst:ClearCurFurniture()
end
function CLuaHouseMgr.ShowLingshouKillerMenu(this, itemActionPairs)
    local data = Zhuangshiwu_Zhuangshiwu.GetData(this.TemplateId)
    if data ~= nil and data.Type == EnumFurnitureType_lua.eLingshou then
        if CClientHouseMgr.Inst.mCurFurnitureProp ~= nil then
            local prop = nil
            if (function ()
                local __try_get_result
                __try_get_result, prop = CommonDefs.DictTryGet(CClientHouseMgr.Inst.mCurFurnitureProp.LingshouInfo, typeof(UInt32), this.ID, typeof(CLingshouFurnitureProp))
                return __try_get_result
            end)() and prop.IsDead > 0 and prop.KillerId ~= 0 then
                table.insert( itemActionPairs, HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id == prop.KillerId then
                        g_MessageMgr:ShowMessage("Lingshou_Tomb_Killer_Self")
                    else
                        CPlayerInfoMgr.ShowPlayerInfoWnd(prop.KillerId, "")
                    end
                end), "UI/Texture/Transparent/Material/housepopupmenu_shuxing.mat", true, true, -1))
            end
        end
    end
end
function CLuaHouseMgr.GetHuyitianHousePopupMenuItemData(this,zswdata,itemActionPairs)
    if zswdata == nil then
        return itemActionPairs
    end

    if zswdata.UseIcon ~= nil and zswdata.Action ~= nil then
        -- local spliter = Table2ArrayWithCount({";"}, 1, MakeArrayClass(String))
        local actions = CommonDefs.StringSplit_ArrayChar(zswdata.Action, ";")
        do
            local i = 0
            while i < zswdata.UseIcon.Length do
                local displayMode = 0
                local default
                default, displayMode = System.UInt32.TryParse(zswdata.UseIcon[i])
                if default then
                    local bCheckOwner = (displayMode >= 10)
                    if not bCheckOwner then
                        if (displayMode % 10 == 1 or displayMode % 10 == 3) and CommonDefs.StringIndexOf_String(actions[math.floor(i / 2)], "PlayerInteractWithFurniture") ~= - 1 then
                            local idx = math.floor(i / 2)
                            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                                this:RequestUseFurniture(idx, zswdata)
                            end), zswdata.UseIcon[i + 1], true, true, -1))
                        end
                    end
                end
                i = i + 2
            end
        end
    end

    if CLuaHouseMgr.CheckCanUpdateBedBeiziStatus(this,zswdata) then
        if CClientFurnitureMgr.Inst:CheckBedBeiziFurnitureStatus(this.ID) then
            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                CLuaHouseMgr.RequestUpdateBedBeiziStatus(this,false)
            end), "UI/Texture/Transparent/Material/housepopupmenu_xianbeizi.mat", true, true, -1))
        else
            table.insert( itemActionPairs, HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                CLuaHouseMgr.RequestUpdateBedBeiziStatus(this,true)
            end), "UI/Texture/Transparent/Material/housepopupmenu_gaibeizi.mat", true, true, -1))
        end
    end

    return itemActionPairs
end
function CLuaHouseMgr.GetChildrenZhuoanActionPairs(this, itemActionPairs)
    if nil == this.Children then
        return
    end
    -- 桌案上的装饰物加入UseIcon
    do
        local j = 0 local len = this.Children.Length
        while j < len do
            local childId = math.floor(this.Children[j])
            local fur = CClientFurnitureMgr.Inst:GetFurniture(childId)
            if fur ~= nil then
                local childData = Zhuangshiwu_Zhuangshiwu.GetData(fur.TemplateId)
                if childData.UseIcon ~= nil then
                    do
                        local i = 0
                        while i < childData.UseIcon.Length do
                            local displayMode = 0
                            local default
                            default, displayMode = System.UInt32.TryParse(childData.UseIcon[i])
                            if default then
                                local bCheckOwner = (displayMode >= 10)
                                if not bCheckOwner or CClientHouseMgr.Inst:IsInOwnHouse() then
                                    if displayMode % 10 == 1 then
                                        local idx = math.floor(i / 2)
                                        table.insert( itemActionPairs, HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                                            local pos = CreateFromClass(CPos, math.floor(fur.Position.x), math.floor(fur.Position.z))
                                            pos = Utility.GridPos2PixelPos(pos)
                                            local trackDis = Utility.Grid2Pixel(CClientFurniture.sFurnitureTrackPos)
                                            CTrackMgr.Inst:Track(pos, trackDis, DelegateFactory.Action(function ()
                                                Gac2Gas.RequestUseFurniture(childId, idx)
                                            end), nil, true)
                                        end), childData.UseIcon[i + 1], true, true, -1))
                                    end
                                end
                            end
                            i = i + 2
                        end
                    end
                end
            end
            j = j + 1
        end
    end
end
function CLuaHouseMgr.GetGardenMenu(this, itemActionPairs)
    if not CClientHouseMgr.Inst:IsInOwnHouse() then
        if this.m_Crop ~= nil and this.m_Crop:GetCropPhase() == EnumCropPhase.ePhaseThree then
            table.insert( itemActionPairs, HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                CLuaHouseMgr.StealCrop(this)
            end), "UI/Texture/Transparent/Material/housepopupmenu_caizhai.mat", true, true, -1))
        elseif this.m_Crop ~= nil and this.m_Crop:GetCropPhase() == EnumCropPhase.ePhaseFour then
            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                CLuaHouseMgr.RequestReviveCrop(this)
            end), "UI/Texture/Transparent/Material/housepopupmenu_fusheng.mat", true, true, -1))
        end
    else
        if this.m_Crop == nil then
            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                CUIManager.ShowUI(CUIResources.PlantWnd)
            end), "UI/Texture/Transparent/Material/housepopupmenu_zhongzhi.mat", true, true, -1))
        else
            if this.m_Crop ~= nil and this.m_Crop:GetCropPhase() == EnumCropPhase.ePhaseThree then
                table.insert( itemActionPairs, HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                    Gac2Gas.RequestHarvestCrop(this:GetGardenIndex())
                    PreventPluginMgr:CheckActivate("HarvestCrop", {})
                end), "UI/Texture/Transparent/Material/housepopupmenu_caizhai.mat", true, true, -1))
            elseif this.m_Crop ~= nil and this.m_Crop:GetCropPhase() == EnumCropPhase.ePhaseFour then
                table.insert( itemActionPairs, HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                    CLuaHouseMgr.RequestReviveCrop(this)
                end), "UI/Texture/Transparent/Material/housepopupmenu_fusheng.mat", true, true, -1))
            end
            if this.m_Crop ~= nil and this.m_Crop:GetCropPhase() ~= EnumCropPhase.ePhaseThree then
                table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                    MessageWndManager.ShowOKCancelMessage(LocalString.GetString("你确认要移除该作物么？"), DelegateFactory.Action(function ()
                        Gac2Gas.RequestRemoveCrop(this:GetGardenIndex())
                    end), nil, nil, nil, false)
                end), "UI/Texture/Transparent/Material/housepopupmenu_yichu.mat", true, true, -1))
            end
        end
    end
end

--认床功能
function CLuaHouseMgr.ProcessChuangTaPopupMenu(this,itemActionPairs)
    if not CClientMainPlayer.Inst then return end

    if CClientHouseMgr.Inst:IsInOwnHouse() then
        local basicProp = CClientMainPlayer.Inst.BasicProp
        local wakeupBedX = basicProp.WakeupBedX
        local wakeupBedY = basicProp.WakeupBedY
        local wakeupBedRoom = basicProp.WakeupBedRoom
        local wakeupBedTemplateId = basicProp.WakeupBedTemplateId

        local isTheBed = false
        local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
        if sceneType == wakeupBedRoom and this.TemplateId == wakeupBedTemplateId then
            if math.floor(this.ServerPos.x)==wakeupBedX and math.floor(this.ServerPos.z)==wakeupBedY then
                isTheBed = true
            end
        end
        if isTheBed then
            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("House_RenChuang_Cancel_Confirm"), DelegateFactory.Action(function()
                    Gac2Gas.SaveWakeupBed(0)
                end), nil, nil, nil, false)
            end), "UI/Texture/Transparent/Material/housepopupmenu_quxiaorenchuang.mat", true, true, -1))
        else
            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("House_RenChuang_Confirm"), DelegateFactory.Action(function()
                    Gac2Gas.SaveWakeupBed(this.ID)
                end), nil, nil, nil, false)
            end), "UI/Texture/Transparent/Material/housepopupmenu_renchuang.mat", true, true, -1))
        end
    end
end

function CLuaHouseMgr.ProcessWinterHousePopupMenu(this,itemActionPairs)
    local prop = CClientHouseMgr.Inst.mCurFurnitureProp
    if prop ~= nil and prop.IsEnableWinterScene > 0 then
        if this:IsPlacedInServer() then
            if WinterHouse_Zhuangshiwu.Exists(this.FurnishTemplateId) then
                local needBuy = false
                if not CommonDefs.DictContains(prop.WinterFuniture, typeof(UInt16), this.FurnishTemplateId) then
                    needBuy = true
                end
                local designData = WinterHouse_Zhuangshiwu.GetData(this.FurnishTemplateId)
                if designData.Currency == 4 then
                    needBuy = false
                end

                if needBuy then
                    table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                        CClientFurniture_RequestBuyWinterSkin(this)
                    end), "UI/Texture/Transparent/Material/WinterHouseUnlockWnd_xuejing.mat", true, true, -1))
                else
                    table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                        CClientFurniture_RequestChangeWinterSkin(this)
                    end), "UI/Texture/Transparent/Material/WinterHouseUnlockWnd_snow.mat", true, true, -1))
                end
            end
        end
    end
end

function CLuaHouseMgr.ProcessChangeColorPopupMenu(this,itemActionPairs)
    if this:IsPlacedInServer() then
        if Zhuangshiwu_ZhuangshiwuColor.Exists(this.FurnishTemplateId) then
            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
                CLuaFurnitureColorWnd.CurFurnitureId = this.ID
                CLuaHouseMgr.RequestPlaceBackFurniture(this)
                CUIManager.ShowUI(CLuaUIResources.FurnitureColorWnd)
            end), "UI/Texture/Transparent/Material/furniturecolor_icon_ranse.mat", true, true, -1))
        end
    end
end

function CClientFurniture_RequestBuyWinterSkin(this)
    if CClientHouseMgr.Inst:IsCurHouseRoomer() then
        g_MessageMgr:ShowMessage("WINTER_OPEN_FANGKE_NOT_ALLOW")
        return
    end
    CLuaWinterHouseUnlockPreviewMgr.OpenPreviewWnd(this.ID, this.TemplateId)
end
function CClientFurniture_RequestChangeWinterSkin(this)
    if CClientFurnitureMgr.Inst.m_IsWinter then
        Gac2Gas.RequestSwitchFurnitureWinterAppearance(this.ID)
    else
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("WinterHouse_ChangeSkin_Off_Season_Confirm"), DelegateFactory.Action(function()
            Gac2Gas.RequestSwitchFurnitureWinterAppearance(this.ID)
        end), nil, nil, nil, false)
    end
end

function CLuaHouseMgr.IsWinter()
    local month1, day1, month2,day2 = string.match(WinterHouse_Setting.GetData().WinterPeriod, "(%d+).(%d+)~(%d+).(%d+)")
    local month1, day1, month2,day2 = tonumber(month1),tonumber(day1),tonumber(month2),tonumber(day2)

    local time=CServerTimeMgr.Inst:GetZone8Time()
    local month,day=time.Month,time.Day

    local open = false
    if month1>month2 then--跨年
        if (month>month1 or (month==month1 and day>=day1)) or (month<month2 or (month==month2 and day<day2)) then
            open=true
        end
    else
        if (month>month1 or (month==month1 and day>=day1)) and (month<month2 or (month==month2 and day<day2)) then
            open=true
        end
    end
    return open
end


function CLuaHouseMgr.OnSendHouseIntro(ownerId, ownerName, ownerId2, ownerName2, houseId, communityId, communityNo, communityStreetId, houseBasicProp, houseContructProp, houseGardenProp, houseProgressProp, houseName, ownerCJB, coupleCJB, furnitureCount, fangkeCount, qishu, isSpokesmanHouse)
    CLuaHouseMgr.currentHouseIntro={
        ownerId = ownerId,
        ownerId2 = ownerId2,
        houseId = houseId,
        basicProp = houseBasicProp,
        gardenProp = houseGardenProp,
        progressProp = houseProgressProp,
        ownerName = ownerName,
        ownerName2 = ownerName2,
        constructProp = houseContructProp,
        houseName = houseName,

        fangkeCount = fangkeCount,
        qishu = qishu,
        furnitureCount = furnitureCount,
        ownerCJBGroup = ownerCJB,
        coupleCJBGroup = coupleCJB,
        isSpokesmanHouse = isSpokesmanHouse,
    }
    CHouseIntroWnd.sCurCommunityId = math.floor(communityId)
    CHouseIntroWnd.sCurCommunityPos = communityNo - 1
    CHouseIntroWnd.sCurCommmunityStreetId = communityStreetId

    CCommunityMgr.Inst.CurHouseId = houseId
    CUIManager.ShowUI(CUIResources.HouseIntroWnd)
end

function Gas2Gac.SendHouseIntro(ownerId, ownerName, ownerId2, ownerName2, houseId, communityId, communityNo, streetId, basicProp, constructProp, gardenProp, progressProp, houseName, ownerCJBStr, coupleCJBStr, furnitureCount, fangkeCount, qishu, 
	isSpokesmanHouse --1 for true, 0 for false
	)
    local houseBasicProp = CHouseDataBasicProp()
    houseBasicProp:LoadFromString(basicProp)
    local houseGardenProp = CGardenProp()
    houseGardenProp:LoadFromString(gardenProp)
    local houseProgressProp = CProgressProp()
    houseProgressProp:LoadFromString(progressProp)

    local houseConstructProp = CConstructProp()
    houseConstructProp:LoadFromString(constructProp)

    local ownerCJB = CChuanjiabaoGroupType()
    ownerCJB:LoadFromString(ownerCJBStr)
    local coupleCJB = CChuanjiabaoGroupType()
    coupleCJB:LoadFromString(coupleCJBStr)
    CLuaHouseMgr.OnSendHouseIntro(ownerId, ownerName, ownerId2, ownerName2, houseId, communityId, communityNo, streetId, houseBasicProp, houseConstructProp, houseGardenProp, houseProgressProp, houseName, ownerCJB, coupleCJB, furnitureCount, fangkeCount, qishu, isSpokesmanHouse)
end



function Gas2Gac.ShiftFurniture(id,xOffset,yOffset,zOffset)
    -- print("ShiftFurniture",id,xOffset,yOffset,zOffset)
    local fur = CClientFurnitureMgr.Inst:GetFurniture(id)
    if fur then
        local x = fur.Position.x
        local z = fur.Position.z

        local xGrid = math.floor(x)
        local zGrid = math.floor(z)
        
        local posY = yOffset + CClientFurnitureMgr.Inst:GetLogicHeightByTemplateId(fur.TemplateId,xGrid + 0.5, zGrid + 0.5) + CClientFurniture.s_YOffset

        local data = Zhuangshiwu_Zhuangshiwu.GetData(fur.TemplateId)
		if (CClientHouseMgr.Inst:GetCurSceneType() == EnumHouseSceneType_lua.eYard and data.Location == EnumFurnitureLocation_lua.eYardFenceAndRoomWall) then
			posY = yOffset + CRenderScene.Inst:SampleLogicHeight(xGrid+0.5, zGrid+0.5) + CClientFurniture.s_YOffset
		end
        if data.SubType == EnumFurnitureSubType_lua.eRollerCoasterTrack then
            posY = yOffset + CHouseRollerCoasterMgr.GetBaseHeight() + CClientFurniture.s_YOffset
        end

        fur.Position = Vector3(xGrid+xOffset, posY, zGrid+zOffset)
        --fur.ServerPos = Vector3(xGrid+xOffset, yOffset, zGrid+zOffset)
    end
end

function Gas2Gac.RotateFurniture(id,rx,ry,rz)
    --print("RotateFurniture",id,rx,ry,rz)
    local fur = CClientFurnitureMgr.Inst:GetFurniture(id)
    if fur then
        --旋转
        local x = math.floor(rx*360/256)
        local y = math.floor(ry*360/256)
        local z = math.floor(rz*360/256)
        fur.ServerRotateY = 0
        fur.RO.transform.localEulerAngles = Vector3(x,y,z)
        fur.ServerRX=rx
        fur.ServerRY=ry
        fur.ServerRZ=rz
    end
end

function Gas2Gac.AddDecoration(id,templateId,parentid,slot)
    CClientFurnitureMgr.Inst:AddDecoration(id, templateId, parentid, slot - 1, false, 0, 0, 0)

    --清供默认雪景状态
    local deco = CClientFurnitureMgr.Inst:GetFurniture(id)
    if deco then
        local isWinter = CClientFurnitureMgr.Inst:IsDefaultFreeWinterStyleFurniture(templateId)
        deco.IsWinter = isWinter
        deco:RefreshWinterSkin()
    end

	if CLuaHouseMgr.CrtMultiPlaceFur then
		local info = CLuaHouseMgr.CrtMultiPlaceFur
		if info.Tid == templateId and info.ParentId == parentid then
			CLuaHouseMgr.MultiFurniturePlaceNext()
		end
	end
end

function Gas2Gac.RemoveDecoration(id,parentid,slot)
    -- print("RemoveDecoration",id,parentid,slot)
    CClientFurnitureMgr.Inst:RemoveDecoration(id, parentid, slot - 1)
end

function  Gas2Gac.MoveFurniture( id, templateId, x, y, z, rot )
    -- print("MoveFurniture",id, templateId, x, y, z, rot)
    local fur = id and CClientFurnitureMgr.Inst:GetFurniture(id)
    local scale = fur and fur.ServerScale or 127
    CClientFurnitureMgr.Inst:MoveFurniture(id, templateId, x, y, z, rot, scale)
end
function Gas2Gac.RemoveFurniture(id)
    -- print("RemoveFurniture",id)
    CClientFurnitureMgr.Inst:RemoveFurniture(id)
end

function Gas2Gac.ScaleFurniture(id,scale)
    -- print("ScaleFurniture",id,scale)
    CClientFurnitureMgr.Inst:ScaleFurniture(id, scale)
end

function Gas2Gac.SyncOpenHouseXuanFuEdit(state)
    if not CClientMainPlayer.Inst then return end
    CClientMainPlayer.Inst.BasicProp.IsOpenHouseXuanFuEdit = state
end





function Gas2Gac.QueryChuanjiabaoInHouseResult(houseId, maxChuanjiabaoNum, chuanjiabaoIds, currentSelectSuite)
    CClientChuanJiaBaoMgr.Inst.houseId = houseId
    CClientChuanJiaBaoMgr.Inst.maxChuanjiabaoNum = maxChuanjiabaoNum
    CClientChuanJiaBaoMgr.Inst.currentSelectSuite = currentSelectSuite

    local group = CChuanjiabaoGroupType()
    group:LoadFromString(chuanjiabaoIds)
    CClientChuanJiaBaoMgr.Inst.chuanjiabaoGroup = group

    if CommonDefs.DictContains(group.Chuanjiabaos, typeof(Byte), currentSelectSuite) then
        CClientChuanJiaBaoMgr.Inst.currentChuanjiabaoIds = CommonDefs.DictGetValue(group.Chuanjiabaos, typeof(Byte), currentSelectSuite).ChuanjiabaoIds
    else
        CClientChuanJiaBaoMgr.Inst.currentChuanjiabaoIds = CreateFromClass(MakeArrayClass(System.String), 4)
    end

    g_ScriptEvent:BroadcastInLua("QueryChuanjiabaoInHouseResult")
    CQiYuanMgr.OnChuanjiabaoInHouseReturn()
end

function Gas2Gac.SetHouseChuanjiabaoInfo(houseId, furnitureStr)
    CUIManager.CloseUI(CUIResources.ChuanjiabaoSelectWnd)
    CItemInfoMgr.CloseItemInfoWnd()
    CClientChuanJiaBaoMgr.Inst.houseId = houseId
    local funitureProp = CFurnitureProp()
    funitureProp:LoadFromString(furnitureStr)

    local data = funitureProp.ChuanjiabaoData
    local id = CClientMainPlayer.Inst.Id
    if CommonDefs.DictContains(data, typeof(UInt64), id) then
        local group = CommonDefs.DictGetValue(data, typeof(UInt64), id)
        CClientChuanJiaBaoMgr.Inst.currentSelectSuite = group.CurrentActiveSuite

        if CommonDefs.DictContains(group.Chuanjiabaos, typeof(Byte), group.CurrentActiveSuite) then
            CClientChuanJiaBaoMgr.Inst.currentChuanjiabaoIds = CommonDefs.DictGetValue(group.Chuanjiabaos, typeof(Byte), group.CurrentActiveSuite).ChuanjiabaoIds
        else
            CClientChuanJiaBaoMgr.Inst.currentChuanjiabaoIds = CreateFromClass(MakeArrayClass(System.String), 4)
        end
    end

    CClientHouseMgr.Inst.mFurnitureProp:LoadFromString(furnitureStr)
    g_ScriptEvent:BroadcastInLua("QueryChuanjiabaoInHouseResult")
    -- EventManager.Broadcast(EnumEventType.QueryChuanjiabaoInHouseResult)
end

function Gas2Gac.SendCurHouseProp(BasicProp, ConstructProp, ProgressProp, GardenProp, FurnitureProp, FurnitureProp2, FurnitureProp3, ExtraProp)
    print("SendCurHouseProp")
    local basicProp = CreateFromClass(CHouseDataBasicProp)
    basicProp:LoadFromString(BasicProp)

    local constructProp = CreateFromClass(CConstructProp)
    constructProp:LoadFromString(ConstructProp)

    local progress = CreateFromClass(CProgressProp)
    progress:LoadFromString(ProgressProp)

    local garden = CreateFromClass(CGardenProp)
    garden:LoadFromString(GardenProp)

    local furniture = CreateFromClass(CFurnitureProp)
    furniture:LoadFromString(FurnitureProp)

    local furniture2 = CFurnitureProp2()
    furniture2:LoadFromString(FurnitureProp2)

    local furniture3 = CFurnitureProp3()
    furniture3:LoadFromString(FurnitureProp3)
    

    local extraInfo = CreateFromClass(CHouseExtraInfoType)
    extraInfo:LoadFromString(ExtraProp)

    -- CClientHouseMgr.Inst:OnSendCurHouseProp(basicProp, constructProp, progress, garden, furniture)

    local inst = CClientHouseMgr.Inst
    inst.mCurBasicProp = basicProp
    inst.mCurConstruectProp = constructProp
    inst.mCurProgressProp = progress
    inst.mCurGardenProp = garden
    inst.mCurFurnitureProp = furniture
    inst.mCurFurnitureProp2 = furniture2
    inst.mCurFurnitureProp3 = furniture3
    inst.mCurExtraInfo = extraInfo

    if (CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.ItemProp.HouseId == basicProp.Id) or CLuaSpokesmanHomeMgr.IsOperateSpokesmanHouse() then
        inst.mBasicProp = basicProp
        inst.mConstructProp = constructProp
        inst.mProgressProp = progress
        inst.mGardenProp = garden
        inst.mFurnitureProp = furniture
        inst.mFurnitureProp2 = furniture2
        inst.mFurnitureProp3 = furniture3
        inst.mExtraProp = extraInfo
    end
        
    CClientHouseMgr.Inst:ClearGrassEditData()
    CClientHouseMgr.Inst:RefreshGrass()
    CClientHouseMgr.Inst:ClearGroundEditData()
    CClientHouseMgr.Inst:UpdateGround()
end



function CLuaHouseMgr.GetFurnitureItemActionPairs( chuanjiabaoItemId, templateId)
    local actions,names = {},{}
    --传家宝
    if chuanjiabaoItemId ~= nil and templateId == 0 then
        local bPlaced = CClientFurnitureMgr.Inst:IsChuanjiabaoPlaced(chuanjiabaoItemId)
        if not bPlaced then
            --摆在场景里
            if CZuoanFurnitureMgr.Inst:GetCurParentFurnitureId() == 0 then
                table.insert( actions, function()
                    -- self:OnPlaceChuanjiabao()
                    local item = CItemMgr.Inst:GetById(chuanjiabaoItemId)
                    local tid = Zhuangshiwu_Setting.GetData().DimianChuanjiabaoId
                    if item ~= nil then
                        CClientFurnitureMgr.Inst:SetContinuation(false)
                        Gac2Gas.CreateFurnitureFromRepository(tid, chuanjiabaoItemId)
                    end
                    CClientFurnitureMgr.Inst:ResetbContinuation()
                    CItemInfoMgr.CloseItemInfoWnd()

                end )
                table.insert( names, LocalString.GetString("摆放"))
            else
                table.insert( actions, function()
                    -- self:OnPutInChuanjiabao()
                    local count = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(templateId)
                    if not CZuoanFurnitureMgr.Inst:CheckCanPutQinggong() then
                        g_MessageMgr:ShowMessage("Zhuangshiwu_Must_Placed_At", Zhuangshiwu_Location.GetData(EnumFurnitureLocation_lua.eDesk).Location)
                    else
                        Gac2Gas.ExchangeFurniture(EnumFurniturePlace_lua.eChuanjiabao, 0,
                            EnumFurniturePlace_lua.eDecoration,
                            CZuoanFurnitureMgr.Inst:GetCurParentFurnitureId(),
                            CClientMainPlayer.Inst.Id,
                            chuanjiabaoItemId)
                    end

                    CItemInfoMgr.CloseItemInfoWnd()
                end )
                table.insert( names, LocalString.GetString("放入"))
            end
        end
        return #actions,actions,names
    end

    local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(templateId)
    if zswdata == nil then
        return #actions,actions,names
    end
    if zswdata.SubType == EnumFurnitureSubType_lua.eRollerCoasterCabin then
        local count = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(templateId)
        if count>0 and LuaHouseRollerCoasterMgr.GetCurParentFurnitureId()>0 then
            table.insert( actions, function()
                Gac2Gas.ExchangeFurniture(
                    EnumFurniturePlace_lua.eRepository, 
                    templateId,
                    EnumFurniturePlace_lua.eDecoration,
                    LuaHouseRollerCoasterMgr.GetCurParentFurnitureId(),
                    CClientMainPlayer.Inst.Id,
                    "")
                CItemInfoMgr.CloseItemInfoWnd()
            end)
            table.insert( names, LocalString.GetString("放入"))
        end
    end

    -- 天空盒依靠解锁信息而不是count来判定action
    if zswdata.Type == EnumFurnitureType_lua.eSkyBox then
        if CClientHouseMgr.Inst:IsCurHouseRoomer() then
            return #actions,actions,names
        end
        if CLuaHouseMgr.IsSkyBoxUnlock(zswdata) then
            -- 是否使用中
            if not CLuaHouseMgr.IsCurrentSkyBox(zswdata) then
                table.insert( actions, function()
                    local skyKind = 0
                    House_SkyBoxKind.ForeachKey(function (key)
                        local skyData = House_SkyBoxKind.GetData(key)
                        if skyData.ItemID == zswdata.ItemId then
                            skyKind = key
                        end
                    end)
                    if skyKind ~= 0 then
                        Gac2Gas.RequestSwitchHouseSkyBox(skyKind)
                    end
                end )
                table.insert( names, LocalString.GetString("使用"))
            else
                table.insert( actions, function()
                    Gac2Gas.RequestSwitchHouseSkyBox(0)
                end )
                table.insert( names, LocalString.GetString("撤下"))
            end
        else
            -- 未解锁显示获取
            table.insert( actions, function()
                local skyKind = 0
                House_SkyBoxKind.ForeachKey(function (key)
                    local skyData = House_SkyBoxKind.GetData(key)
                    if skyData.ItemID == zswdata.ItemId then
                        skyKind = key
                    end
                end)
                if skyKind ~= 0 then
                CLuaHouseMgr.ShowUnlockSkyWnd(skyKind)
            end
        end )
            table.insert( names, LocalString.GetString("解锁"))
        end
        return #actions,actions,names
    end
    if zswdata.Type == EnumFurnitureType_lua.eWallPaper then
        return CLuaHouseMgr.GetWallPaperItemActionPairs(zswdata.ItemId)
    end
    --水池布置
    if zswdata.Type == EnumFurnitureType_lua.ePoolFurniture and LuaPoolMgr.IsPoolEnterZswTemplate(templateId) then
        table.insert( actions, function()
            --LuaPoolMgr.OpenPoolEditer()
            Gac2Gas.RequestEditPool(false)
            CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
        end )
        table.insert( names, LocalString.GetString("编辑模式"))
        return #actions,actions,names
    end

    if LuaHouseTerrainMgr.IsEditTerrainTemplateId(templateId) then
        table.insert( actions, function()
            if IsOpenHouseTerrainEdit() then
                LuaHouseTerrainMgr.RequestEditTerrain()
            else
                g_MessageMgr:ShowMessage("DiBiao_Edit_Not_Open")
            end
            CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
        end )
        table.insert( names, LocalString.GetString("编辑模式"))
        return #actions,actions,names
    end

    local count = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(templateId)
    local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
    if count > 0 then

        local function putin()
            local count = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(templateId)
            if not CZuoanFurnitureMgr.Inst:CheckCanPutQinggong() then
                g_MessageMgr:ShowMessage("Zhuangshiwu_Must_Placed_At", Zhuangshiwu_Location.GetData(EnumFurnitureLocation_lua.eDesk).Location)
            else
                Gac2Gas.ExchangeFurniture(EnumFurniturePlace_lua.eRepository,
                    templateId,
                    EnumFurniturePlace_lua.eDecoration,
                    CZuoanFurnitureMgr.Inst:GetCurParentFurnitureId(),
                    CClientMainPlayer.Inst.Id, "")
            end

            CItemInfoMgr.CloseItemInfoWnd()
        end

        --花木
        if zswdata.SubType == EnumFurnitureSubType_lua.eHuamu and CZuoanFurnitureMgr.Inst:CheckCanPutHuamu() then
            table.insert( actions, function()
                -- self:OnPutInPenzai()
                if not CZuoanFurnitureMgr.Inst:CheckCanPutHuamu() then
                    return
                end
                Gac2Gas.ExchangeFurniture(EnumFurniturePlace_lua.eRepository,
                    templateId,
                    EnumFurniturePlace_lua.eHuapen,
                    CZuoanFurnitureMgr.Inst:GetCurParentFurnitureId(),
                    CClientMainPlayer.Inst.Id, "")
                CItemInfoMgr.CloseItemInfoWnd()
            end )
            table.insert( names, LocalString.GetString("放入"))
            --desk
        elseif zswdata.Location == EnumFurnitureLocation_lua.eDesk then
            table.insert( actions, putin )
            table.insert( names, LocalString.GetString("放入"))

        elseif zswdata.Location == EnumFurnitureLocation_lua.eDeskOrLand then
            if CZuoanFurnitureMgr.Inst:GetCurParentFurnitureId() ~= 0 then
                table.insert( actions, putin )
                table.insert( names, LocalString.GetString("放入"))
            else
            end
        else
        end
    else
        if zswdata.Type ~= EnumFurnitureType_lua.eJianzhu then
            table.insert( actions, function()
                -- self:OnGetItem()
                local data = Zhuangshiwu_Zhuangshiwu.GetData(templateId)
                if data ~= nil and data.ItemId ~= 0 then
                    CItemAccessListMgr.Inst:ShowItemAccessInfo(data.ItemId, false, nil, CTooltip.AlignType.Top)
                end
            end )
            table.insert( names, LocalString.GetString("获取"))
        end
    end

    local placedCnt = CLuaClientFurnitureMgr.GetPlacedFurnitureCountByTemplateId(templateId)
    if placedCnt > 0 and zswdata.Type ~= EnumFurnitureType_lua.eJianzhu then
        table.insert( actions, function()
            -- self:OnShowPosition()
            CMiniMapPopWnd.m_ZhuangshiwuTemplateId = templateId
            CUIManager.ShowUI(CUIResources.MiniMapPopWnd)
            CItemInfoMgr.CloseItemInfoWnd()
        end )
        table.insert( names, LocalString.GetString("显示位置"))
        table.insert( actions, function()
            -- self:OnPackAll()
            local data = Zhuangshiwu_Zhuangshiwu.GetData(templateId)
            if nil == data then
                return
            end
            local text = g_MessageMgr:FormatMessage("Pack_All_Furniture_Msg", data.Name)
            MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(function ()

                CLuaClientFurnitureMgr.PackAllFurnitureByTemplateId(templateId)
                CItemInfoMgr.CloseItemInfoWnd()
            end), nil, nil, nil, false)

        end )
        table.insert( names, LocalString.GetString("收起全部"))
    end


    if sceneType == EnumHouseSceneType_lua.eYard then
        local houseGrade = CClientHouseMgr.Inst.mConstructProp.ZhengwuGrade
        if (zswdata.SubType == EnumFurnitureSubType_lua.eZhengwu
            or zswdata.SubType == EnumFurnitureSubType_lua.eCangku
            or zswdata.SubType == EnumFurnitureSubType_lua.eXiangfang
            or zswdata.SubType == EnumFurnitureSubType_lua.eWeiqiang) and houseGrade >= zswdata.HouseGrade then

            CLuaHouseMgr.GetJianZhuItemActionPairs(zswdata,templateId,actions,names)
        end
    end

    return #actions,actions,names
end

function CLuaHouseMgr.GetJianZhuItemActionPairs(zswdata,templateId,actions,names)
    local subType = zswdata.SubType
    local curBuilding = CClientFurnitureMgr.Inst:GetFurnitureBySubtype(subType)
    
    --判断新外观是否解锁
    CLuaClientFurnitureMgr.InitSpecialBuilding()

    local unlock = false

    local speicalBuilding = CClientHouseMgr.Inst.mFurnitureProp2.SpecialBuilding
    local idx = CLuaClientFurnitureMgr.m_SpecialBuildingLookup[templateId]
    if idx then 
        unlock = speicalBuilding:GetBit(idx)
    end
    local isNew = CLuaClientFurnitureMgr.m_SpecialBuildingRelationLookupNew[templateId] and true or false

    local oldTid = isNew and CLuaClientFurnitureMgr.m_SpecialBuildingRelationLookupNew[templateId] or templateId
    local newTid = isNew and templateId or CLuaClientFurnitureMgr.m_SpecialBuildingRelationLookup[templateId]
    --如果新建筑没开放，就不显示了
    local newStatus = 3
    local newData = Zhuangshiwu_Zhuangshiwu.GetData(newTid)
    if newData then
        newStatus = newData.Status
    end

    local oldName,oldAction = LocalString.GetString("原外观"),function()
            local text = g_MessageMgr:FormatMessage("Change_Building_Confirm")
            MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(function ()
                Gac2Gas.RequestChangeBuildingAppearance(subType, oldTid)
            end), nil, nil, nil, false)
            CItemInfoMgr.CloseItemInfoWnd()
        end
    local newName,newAction = LocalString.GetString("新外观"),function()
            local text = g_MessageMgr:FormatMessage("Change_Building_Confirm")
            MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(function ()
                Gac2Gas.RequestChangeBuildingAppearance(subType, newTid)
            end), nil, nil, nil, false)
            CItemInfoMgr.CloseItemInfoWnd()
        end
    local unlockName,unlockAction = LocalString.GetString("新外观解锁"),function()
            LuaHouseUnlockJianZhuWnd.s_ZhuangshiwuId = templateId
            CUIManager.ShowUI(CLuaUIResources.HouseUnlockJianZhuWnd)
        end


    if curBuilding ~= nil and curBuilding.TemplateId ~= templateId then
        --如果已经解锁了新外观，显示原外观、新外观
        if unlock and IsSpecialBuildingOpen() then
            table.insert( actions, oldAction )
            table.insert( names, oldName)

            table.insert( actions, newAction )
            table.insert( names, newName)
        else
            table.insert( actions, function()
                local text = g_MessageMgr:FormatMessage("Change_Building_Confirm")
                MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(function ()
                    Gac2Gas.RequestChangeBuildingAppearance(subType, templateId)
                end), nil, nil, nil, false)
                CItemInfoMgr.CloseItemInfoWnd()
            end )
            table.insert( names, LocalString.GetString("选择"))

            if IsSpecialBuildingOpen() and newStatus~=3 then
                table.insert( actions, unlockAction )
                table.insert( names, unlockName)
            end
        end
    end

    --选了当前
    if curBuilding and curBuilding.TemplateId == templateId  then
        if unlock and IsSpecialBuildingOpen() then
            if isNew then
                table.insert( actions, oldAction )
                table.insert( names, oldName)
            else
                table.insert( actions, newAction )
                table.insert( names, newName)
            end
        else
            if IsSpecialBuildingOpen() and newStatus~=3 then
                table.insert( actions, unlockAction )
                table.insert( names, unlockName)
            end
        end
    end
end

function CLuaHouseMgr.GetWallPaperItemActionPairs(itemId)
    local actions,names = {},{}
    local unlockWallFloorType = CClientHouseMgr.Inst.mCurFurnitureProp2.UnlockWallFloorType
    local paperData = House_WallPaper.GetDataBySubKey("ItemID",itemId)
    if not paperData then
        return #actions,actions,names
    end
    local paperId = paperData.Id
    if CLuaHouseMgr.IsWallPaperUnlock(paperId) then
        -- 是否使用中
        if not CLuaHouseMgr.IsCurrentWallPaer(paperId) then
            table.insert( actions, function()
                if paperId and paperId~=0 then
                    local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
                    if sceneType == EnumHouseSceneType_lua.eRoom then
                        if not CClientHouseMgr.Inst:IsHouseOwner() and not CLuaHouseMgr.RoomDecorationPrivalege then
                            g_MessageMgr:ShowMessage("ROOMER_CANNOT_USE")
                            return
                        end
                        sceneType = EnumHouseRoomType.Room
                    elseif sceneType == EnumHouseSceneType_lua.eXiangfang then
                        sceneType = EnumHouseRoomType.Xiangfang
                    end
                    Gac2Gas.RequestSetWallOrFloorPaperType(sceneType,paperData.Place,paperId)
                end
            end )
            table.insert( names, LocalString.GetString("使用"))
        else
            table.insert( actions, function()
                local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
				if sceneType == EnumHouseSceneType_lua.eRoom then
                    if not CClientHouseMgr.Inst:IsHouseOwner() and not CLuaHouseMgr.RoomDecorationPrivalege then
                        g_MessageMgr:ShowMessage("ROOMER_CANNOT_USE")
                        return
                    end
                    sceneType = EnumHouseRoomType.Room
                elseif sceneType == EnumHouseSceneType_lua.eXiangfang then
                    sceneType = EnumHouseRoomType.Xiangfang
                end
                Gac2Gas.RequestSetWallOrFloorPaperType(sceneType,paperData.Place,0)
            end )
            table.insert( names, LocalString.GetString("撤下"))
        end
    else
        -- 未解锁显示获取
        table.insert( actions, function()
            if CClientMainPlayer.Inst == nil then
                return false
            end
            local curHouseBasicProp = CClientHouseMgr.Inst.mCurBasicProp
            local ownerId = curHouseBasicProp.OwnerId
            local ownerId2 = curHouseBasicProp.OwnerId2
            local playerId = CClientMainPlayer.Inst.Id
            if playerId ~= ownerId and playerId ~= ownerId2 then
                g_MessageMgr:ShowMessage("WallPaper_Unlock_NeedOwner")
                return
            end
            local paperData = House_WallPaper.GetDataBySubKey("ItemID",itemId)
            if paperData then
                local paperId = paperData.Id
                if paperId and paperId~=0 then
                    CLuaHouseMgr.ShowUnlockPaperWnd(paperId)
                end
            end
        end)
        table.insert( names, LocalString.GetString("解锁"))
    end
    return #actions,actions,names
end

CLuaHouseMgr.m_IsWallPaperPlayOpen = false
CLuaHouseMgr.m_SelectedPaperId = 0
function CLuaHouseMgr.ShowUnlockPaperWnd(paperId)
    CLuaHouseMgr.m_SelectedPaperId = paperId
    CUIManager.ShowUI(CLuaUIResources.HouseUnlockWallpaperWnd)
end

CLuaHouseMgr.m_SelectedSkyKind = 0
function CLuaHouseMgr.ShowUnlockSkyWnd(skyKind)
    CLuaHouseMgr.m_SelectedSkyKind = skyKind
    CUIManager.ShowUI(CLuaUIResources.HouseUnlockSkyboxWnd)
end

--@region 家园天空盒

local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CSceneDynamicStuff = import "L10.Engine.Scene.CSceneDynamicStuff"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local Vector3 = import "UnityEngine.Vector3"

--@desc 切换天空盒
function CLuaHouseMgr.ChangeSkyBox(skyKindId)
    -- 做一个映射
    if skyKindId == 0 then skyKindId = 6 end
    local skyData = House_SkyBoxKind.GetData(skyKindId)
    if not skyData then return end

    -- 打开/关闭黄昏
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.RO then
        if skyKindId == 6 then
            local fx = CEffectMgr.Inst:AddObjectFX(88800593,CClientMainPlayer.Inst.RO,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
            CClientMainPlayer.Inst.RO:AddFX(88800593, fx, -1)
        else
            CClientMainPlayer.Inst.RO:RemoveFX(88800593)
        end
    end

    local atmosphereId = skyData.Atmosphere
    local oriSkyKindId, combineSkyKindId
    if CClientHouseMgr.IsWinterStyle() then
        oriSkyKindId = skyData.WinterKind
        combineSkyKindId = atmosphereId
    else
        oriSkyKindId = atmosphereId
        combineSkyKindId = 0
    end
    CLuaHouseMgr.NewSkyBoxKind(oriSkyKindId, combineSkyKindId)
end

function CLuaHouseMgr.NewSkyBoxKind(skyKindId, combineSkyKindId)
    if CClientHouseMgr.Inst and CClientHouseMgr.Inst:IsPlayerInYard() then
        CRenderScene.Inst:SetAtmosphere(skyKindId, combineSkyKindId)
    end
end

--@desc 天空盒是否解锁
function CLuaHouseMgr.IsSkyBoxUnlock(zhuangshiwu)
    if not zhuangshiwu or zhuangshiwu.Type ~= EnumFurnitureType_lua.eSkyBox then return false end 

    local skyBox = nil
    House_SkyBoxKind.ForeachKey(function (key)
        local skyData = House_SkyBoxKind.GetData(key)
        if skyData and skyData.ItemID == zhuangshiwu.ItemId then
            skyBox = skyData
        end
    end)

    if not skyBox then return false end

    local extraData = CClientHouseMgr.Inst.mExtraProp
    if not extraData or not extraData.SkyBoxUnlocked then return false end
    local isUnlock = extraData.SkyBoxUnlocked:GetBit(skyBox.Kind)

    return isUnlock
end

--@desc 装饰物是否是正在使用的天空盒
function CLuaHouseMgr.IsCurrentSkyBox(zhuangshiwu)
    if not zhuangshiwu or zhuangshiwu.Type ~= EnumFurnitureType_lua.eSkyBox then return false end

    local skyBox = nil
    House_SkyBoxKind.ForeachKey(function (key)
        local skyData = House_SkyBoxKind.GetData(key)
        if skyData and skyData.ItemID == zhuangshiwu.ItemId then
            skyBox = skyData
        end
    end)

    if not skyBox then return false end

    local extraData = CClientHouseMgr.Inst.mExtraProp
    if not extraData or not extraData.SkyBoxKind then return false end

    return extraData.SkyBoxKind == skyBox.Kind
end

--@desc 是否是解锁天空盒的item
function CLuaHouseMgr.IsSkyBoxItem(item)
    local result = false
    House_SkyBoxKind.ForeachKey(function (key)
        local skyData = House_SkyBoxKind.GetData(key)
        if skyData and skyData.ItemID == item.ID then
            result = true
        end
    end)
    return result
end

--@desc 根据装饰物Id获得对应的skybox对象
function CLuaHouseMgr.GetSkyBoxItemByZSWId(zswId)
    if not zswId then return nil end
    local zhuangshiwu = Zhuangshiwu_Zhuangshiwu.GetData(zswId)
    if not zhuangshiwu then return nil end

    local skyBox = nil
    House_SkyBoxKind.ForeachKey(function (key)
        local skyData = House_SkyBoxKind.GetData(key)
        if skyData and skyData.ItemID == zhuangshiwu.ItemId then
            skyBox = skyData
        end
    end)

    return skyBox
end

--@desc 天空盒是否在可购买时间段
function CLuaHouseMgr.IsSkyBoxInBuyDuration(skyBoxKind)
    if skyBoxKind.BuyDuration == "~" then
        return true
    end

    local inBuyDuration = false

    local sy, sM, sd, sh, sm, ey, eM, ed, eh, em = string.match(skyBoxKind.BuyDuration, "(%d+)/(%d+)/(%d+) (%d+):(%d+) %- (%d+)/(%d+)/(%d+) (%d+):(%d+)")
    local buyStartDate = CreateFromClass(DateTime, tonumber(sy), tonumber(sM), tonumber(sd), tonumber(sh), tonumber(sm), 0)
    local buyEndDate = CreateFromClass(DateTime, tonumber(ey), tonumber(eM), tonumber(ed), tonumber(eh), tonumber(em), 0)

    local nowDate = CServerTimeMgr.Inst:GetZone8Time()
    inBuyDuration = DateTime.Compare(nowDate, buyStartDate) >= 0 and DateTime.Compare(nowDate, buyEndDate) < 0
    return inBuyDuration
end

--@desc 天空盒是否在可使用时间段
function CLuaHouseMgr.IsSkyBoxInUseDuration(skyBoxKind)
    if skyBoxKind.UseDuration == "~" then
        return true
    end

    local inUseDuration = false

    local sy, sM, sd, sh, sm, ey, eM, ed, eh, em = string.match(skyBoxKind.UseDuration, "(%d+)/(%d+)/(%d+) (%d+):(%d+) %- (%d+)/(%d+)/(%d+) (%d+):(%d+)")
    local useStartDate = CreateFromClass(DateTime, tonumber(sy), tonumber(sM), tonumber(sd), tonumber(sh), tonumber(sm), 0)
    local useEndDate = CreateFromClass(DateTime, tonumber(ey), tonumber(eM), tonumber(ed), tonumber(eh), tonumber(em), 0)

    local nowDate = CServerTimeMgr.Inst:GetZone8Time()
    inUseDuration = DateTime.Compare(nowDate, useStartDate) >= 0 and DateTime.Compare(nowDate, useEndDate) < 0
    return inUseDuration
end

--@desc 切换天空盒
function Gas2Gac.UpdateClientHouseSkyboxInfo(houseId, skyboxIdx)
    CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
    CUIManager.CloseUI(CLuaUIResources.HouseUnlockSkyboxWnd)
    if CClientHouseMgr.Inst.mCurBasicProp and CClientHouseMgr.Inst.mCurBasicProp.Id == houseId then
        CLuaHouseMgr.ChangeSkyBox(skyboxIdx)
    end

    if CClientHouseMgr.Inst.mBasicProp and CClientHouseMgr.Inst.mBasicProp.Id == houseId and CClientHouseMgr.Inst.mExtraProp then
        CClientHouseMgr.Inst.mExtraProp.SkyBoxKind = skyboxIdx
        g_ScriptEvent:BroadcastInLua("UpdateCurrentSkyBox")
    end
end

--@desc 解锁天空盒成功
function Gas2Gac.NewHouseSkyboxUnlocked(houseId, skyboxIdx)
    if not  CClientHouseMgr.Inst.mBasicProp then return end
    
    if  CClientHouseMgr.Inst.mBasicProp and CClientHouseMgr.Inst.mBasicProp.Id == houseId and CClientHouseMgr.Inst.mExtraProp then
        CClientHouseMgr.Inst.mExtraProp.SkyBoxUnlocked:SetBit(skyboxIdx, true)
        g_ScriptEvent:BroadcastInLua("UpdateSkyBoxUnlock")
    end
end

--@endregion


--@region 家园界面迭代相关

--@desc 家园建设界面提示（适配C#转lua代码）
CLuaHouseMgr.m_ConstructHouseMsg = nil

--@msg: 消息
--@override: 是否覆盖
function CLuaHouseMgr.UpdateConstructHouseMsg(msg, override)
    if CLuaHouseMgr.m_ConstructHouseMsg and override then
        CLuaHouseMgr.m_ConstructHouseMsg = msg
    elseif not CLuaHouseMgr.m_ConstructHouseMsg then
        CLuaHouseMgr.m_ConstructHouseMsg = msg
    end
end

--@desc 展示家园建筑升级/建造条件不满足时的提示
function CLuaHouseMgr.ShowConstructHouseMsg()
    if CLuaHouseMgr.m_ConstructHouseMsg then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", CLuaHouseMgr.m_ConstructHouseMsg)
    end
end

--@endregion

function Gas2Gac.HouseScreenshotReportSuccess()
    CUIManager.CloseUI(CLuaUIResources.HouseScreenshotReportWnd)
end

function Gas2Gac.CreateFurnitureFromRepository(id,templateId,chuanjiabaoId)
    CLuaClientFurnitureMgr.CreateFurnitureFromRepository(id, templateId, chuanjiabaoId)
end

function Gas2Gac.SendHouseData(BasicProp, ConstructProp, FurnitureProp, ProgressProp, GardenProp, NewsProp, ExtraProp, communityId, communityName, communityNO, houseName, zuoling, youshe, ownerName1, ownerName2,FurnitureProp2, FurnitureProp3)
    print("SendHouseData")
    --如果是代言人家园，则不会处理家园数据的同步
    if CLuaSpokesmanHomeMgr.IsOperateSpokesmanHouse() then
        return
    end

    local basicProp = CHouseDataBasicProp()
    basicProp:LoadFromString(BasicProp)

    local constructProp = CConstructProp()
    constructProp:LoadFromString(ConstructProp)

    local furnitureProp = CFurnitureProp()
    furnitureProp:LoadFromString(FurnitureProp)

    local furnitureProp2 = CFurnitureProp2()
    furnitureProp2:LoadFromString(FurnitureProp2)

    local furnitureProp3 = CFurnitureProp3()
    furnitureProp3:LoadFromString(FurnitureProp3)

    local progress = CProgressProp()
    progress:LoadFromString(ProgressProp)

    local garden = CGardenProp()
    garden:LoadFromString(GardenProp)

    local news = CNewsProp()
    news:LoadFromString(NewsProp)

    local extraInfo = CHouseExtraInfoType()
    extraInfo:LoadFromString(ExtraProp)

    CClientHouseMgr.Inst:OnSendHouseData(basicProp, constructProp, furnitureProp, progress, garden, news, extraInfo, communityId, communityName, communityNO, houseName, zuoling, youshe, ownerName1, ownerName2,furnitureProp2,furnitureProp3)

    CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
        obj:UpdateSnowFootPrint()
    end))
end

CLuaHouseMgr.s_WallPaperSwitchOn = true
--@desc 墙纸壁纸是否解锁
function CLuaHouseMgr.IsWallPaperUnlock(paperId)
    local unlockWallFloorType = CClientHouseMgr.Inst.mCurFurnitureProp2.UnlockWallFloorType
    local isUnlock = unlockWallFloorType:GetBit(paperId)

    return isUnlock
end

--@desc 装饰物是否是正在使用的墙纸壁纸
function CLuaHouseMgr.IsCurrentWallPaer(paperId)

    local curWallPaperId,curFloorPaperId
    local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
    if sceneType == EnumHouseSceneType_lua.eRoom then
        curWallPaperId = CClientHouseMgr.Inst.mCurFurnitureProp2.RoomWallType
        curFloorPaperId = CClientHouseMgr.Inst.mCurFurnitureProp2.RoomFloorType
    elseif sceneType == EnumHouseSceneType_lua.eXiangfang then
        curWallPaperId = CClientHouseMgr.Inst.mCurFurnitureProp2.XiangfangWallType
        curFloorPaperId = CClientHouseMgr.Inst.mCurFurnitureProp2.XiangfangFloorType
    end

    if paperId == curWallPaperId or paperId == curFloorPaperId then        
            return true
    else
        return false
    end
end

--@desc 是否是解锁天空盒的item
function CLuaHouseMgr.IsWallPaperItem(item)
    local result = false
    if House_WallPaper.GetDataBySubKey("ItemID",item.ID) then
        result = true
    end
    return result
end

CLuaHouseMgr.m_DefaultWallPaper = nil
CLuaHouseMgr.m_DefaultFloorPaper = nil
function CLuaHouseMgr.GetDefaultWallFloorPaper()
    local node = CRenderScene.Inst.transform:Find("Models")
    if not node then return end
    local meshs = CommonDefs.GetComponentsInChildren_Component_Type(node, typeof(MeshRenderer))
    local isFindWall,isFindFloor
    if meshs then
        for i=0,meshs.Length-1,1 do
            local mesh = meshs[i]
            if mesh.sharedMaterial then
                local matname = mesh.sharedMaterial.name
                if StringStartWith(matname,"wallpaper_") and not isFindWall then                   
                    isFindWall = true
                    CLuaHouseMgr.m_DefaultWallPaper = mesh.sharedMaterial
                elseif StringStartWith(matname,"floorpaper_") and not isFindFloor then                   
                    isFindFloor = true
                    CLuaHouseMgr.m_DefaultFloorPaper = mesh.sharedMaterial
                end
                if isFindWall and isFindFloor then
                    return
                end
            end
        end
    end
end

function CLuaHouseMgr.SetWallPaper()
    if not CLuaHouseMgr.s_WallPaperSwitchOn then return end
    if not CClientHouseMgr.Inst then return end
    if not CClientHouseMgr.Inst.mCurFurnitureProp2 then return end
    
    local wallstr = "jyptsn4_new_005"
    local path = "character/jiayuan/jyqiangzhi_01/model/materials/"
    
    local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
    local wallUsingId
    if sceneType == EnumHouseSceneType_lua.eRoom then
        wallUsingId = CClientHouseMgr.Inst.mCurFurnitureProp2.RoomWallType
    elseif sceneType == EnumHouseSceneType_lua.eXiangfang then
        wallUsingId = CClientHouseMgr.Inst.mCurFurnitureProp2.XiangfangWallType
    end

    local wallPath = "scenes/models/jiayuansn/jiayuanptsn/materials/wallpaper_jiayuanptsn01_002_001_dr.mat"
    local type = EnumToInt(EnumHousePaperType.eWall)

    local hideRoot = CRenderScene.Inst.transform:Find("HideRoot")
    local dynamic
    if hideRoot then
        dynamic = hideRoot:GetComponent(typeof(CSceneDynamicStuff))
        dynamic:RemoveStuff("WallPaperDecoration")
    end
    local roomName = CoreScene.MainCoreScene.SceneName
    if wallUsingId and wallUsingId ~= 0 then
        local data = House_WallPaper.GetData(wallUsingId)
        if data then
            wallPath = data.Res
            local s = string.match(wallPath, ".+/(.+)")
            local e = string.find(s,".mat")
            local wallname = string.sub(s,1,e-1)
            wallPath = path..wallname..".mat"
            if data.Decorations and data.Decorations ~= "" then
                local staffPath = SafeStringFormat3("Assets/Res/Levels/Dynamic/%s_HideStuffs_%s.prefab",roomName,data.Decorations)
                if dynamic then
                    dynamic:ShowStuff(true,staffPath,"WallPaperDecoration")
                end
            end
        end
        CWallAndFloorPaperChanger.CorrectWallPath = wallPath
        EventManager.BroadcastInternalForLua(EnumEventType.OnChangeHousePaper, {type,wallPath})
    else
        CWallAndFloorPaperChanger.CorrectWallPath = nil
        EventManager.BroadcastInternalForLua(EnumEventType.OnChangeHousePaper, {type,nil})
    end
    
end
function CLuaHouseMgr.SetFloorPaper()
    if not CLuaHouseMgr.s_WallPaperSwitchOn then return end
    if not CoreScene.MainCoreScene then return end
    if not CRenderScene.Inst then return end
    if not CClientHouseMgr.Inst then return end
    if not CClientHouseMgr.Inst.mCurFurnitureProp2 then return end
    
    local floorstr = "jyptsn4_07"
    local floorPath = "scenes/models/jiayuansn/jiayuanptsn/Materials/floorpaper_jiayuanptsn01_002_001_dr.mat"
    local path = "character/jiayuan/jyqiangzhi_01/model/materials/"
    
    local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
    local floorUsingId
    if sceneType == EnumHouseSceneType_lua.eRoom then
        floorUsingId = CClientHouseMgr.Inst.mCurFurnitureProp2.RoomFloorType
    elseif sceneType == EnumHouseSceneType_lua.eXiangfang then
        floorUsingId = CClientHouseMgr.Inst.mCurFurnitureProp2.XiangfangFloorType
    end

    local type = EnumToInt(EnumHousePaperType.eFloor)
    if floorUsingId and floorUsingId ~= 0 then
        local data = House_WallPaper.GetData(floorUsingId)
        if data then
            floorPath = data.Res
            local s = string.match(floorPath, ".+/(.+)")
            local e = string.find(s,".mat")
            local floorname = string.sub(s,1,e-1)
            floorPath = path..floorname..".mat"
        end
        CWallAndFloorPaperChanger.CorrectFloorPath = floorPath
        EventManager.BroadcastInternalForLua(EnumEventType.OnChangeHousePaper, {type,floorPath})
    else
        CWallAndFloorPaperChanger.CorrectFloorPath = nil
        EventManager.BroadcastInternalForLua(EnumEventType.OnChangeHousePaper, {type,nil})
    end
end

function CLuaHouseMgr.SetDesignDataRoomPapers()
    if not CScene.MainScene then return end
    local sceneId = CScene.MainScene.SceneTemplateId
    print("sceneId",sceneId)
    local data = PublicMap_PublicMap.GetData(sceneId)
    local path = "character/jiayuan/jyqiangzhi_01/model/materials/"
    if data then
        local papers = data.RoomPaper 
        print(papers)
        if papers then
            for i=0,papers.Length-1,1 do 
                local paperId = papers[i]
                print(i,paperId)
                local pdata = House_WallPaper.GetData(paperId)
                if pdata then
                    local paperPath = pdata.Res
                    local s = string.match(paperPath, ".+/(.+)")
                    local e = string.find(s,".mat")
                    local papername = string.sub(s,1,e-1)
                    paperPath = path..papername..".mat"
                    EventManager.BroadcastInternalForLua(EnumEventType.OnChangeHousePaper, {3-pdata.Place,paperPath})
                end
                
            end
        end
    end
end

function CLuaHouseMgr.CheckSceneType(type,isInHouse)
    local list
    if isInHouse then
        list = Zhuangshiwu_Setting.GetData().ShowTypeInside
    else
        list = Zhuangshiwu_Setting.GetData().ShowTypeOutside
    end
    if list then
        for i=0,list.Length-1,1 do
            if list[i] == type then
                return true
            end
        end
    end
    return false
end

local CloseDistanceConfirmTick = nil
local CloseConfirmTick = nil
function Gas2Gac.EnterHouseRoomConfirm(roomType)
    if CClientFurnitureMgr.Inst.FurnishMode or CClientMainPlayer.Inst == nil then
        return
    end
    if CUIManager.IsLoaded(CLuaUIResources.PoolEditWnd) then
        return
    end
    local pos = CClientMainPlayer.Inst.RO.Position
    if  CloseConfirmTick then
        UnRegisterTick(CloseConfirmTick)
        CloseConfirmTick = nil
    end
    if CloseDistanceConfirmTick then
        UnRegisterTick(CloseDistanceConfirmTick)
        CloseDistanceConfirmTick = nil
    end
    CloseDistanceConfirmTick = RegisterTick(function()
        if (not (CClientHouseMgr.Inst:IsPlayerInYard() and CClientMainPlayer.Inst and Vector3.Distance(CClientMainPlayer.Inst.RO.Position, pos) < 3)) then
                CUIManager.CloseUI(CUIResources.SceneSimpleButton)
                if CloseDistanceConfirmTick then
                    UnRegisterTick(CloseDistanceConfirmTick)
                    CloseDistanceConfirmTick = nil                       
                end
                if CloseConfirmTick then
                    UnRegisterTick(CloseConfirmTick)
                    CloseConfirmTick = nil
                end
            end
        end,1000)
    CloseConfirmTick = RegisterTickOnce(function()
            CUIManager.CloseUI(CUIResources.SceneSimpleButton)
            if CloseConfirmTick then
                UnRegisterTick(CloseConfirmTick)
                CloseConfirmTick = nil
            end
            if CloseDistanceConfirmTick then
                UnRegisterTick(CloseDistanceConfirmTick)
                CloseDistanceConfirmTick = nil
            end
        end, 5 * 1000)

    CSceneSimpleButton.sClickAction = DelegateFactory.Action(function()
        if CYangyangPhotoVoteMgr.sIsInYangyangHouse then
            Gac2Gas.RequestEnterYangYangRoom()
            return
        end
        if roomType == 0 then
            Gac2Gas.RequestEnterHouseRoom()
        elseif roomType == 1 then
            Gac2Gas.RequestEnterXiangfang()
        elseif roomType == 10 then
            Gac2Gas.RequestEnterHouseCompetitionRoom()
        elseif roomType == 11 then
            Gac2Gas.RequestEnterHouseCompetitionXiangfang()
        end
    end)

    local fur = nil
    local furlist = {}
    CommonDefs.DictIterate(CClientFurnitureMgr.Inst.FurnitureList, DelegateFactory.Action_object_object(function (k,v) 
        local subType = v:GetSubType()
        if roomType==0 or roomType==10 then
            if subType==EnumFurnitureSubType_lua.eZhengwu then
                table.insert( furlist,v )
            end
        elseif roomType==1 or roomType==11 then
            if subType==EnumFurnitureSubType_lua.eXiangfang then
                table.insert( furlist,v )
            end
        end
    end))
    local tmppos = CClientMainPlayer.Inst.RO.Position
    local mindist = nil
    --找到离自己距离最近的那一个
    for i,v in ipairs(furlist) do
        local dist = Vector3.Distance(tmppos,v.ServerPos)
        if mindist then
            if mindist>dist then
                fur = v
                mindist = dist
            end
        else
            mindist = dist
            fur = v
        end
    end
    if fur then
        local xPos = fur.ServerPos.x
		local zPos = fur.ServerPos.z
		local rot = fur.ServerRotateY
        local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(fur.TemplateId)
		local column = zswdata.SpaceHeight
		local ox = math.ceil(column/2)
		
		local offx = 0
		local offz = ox
		if rot == 90 then
			offx = ox
			offz = 0
		elseif rot == 180 then
			offx = 0
			offz = -ox
		elseif rot == 270 then
			offx, offz = -ox, 0
		end

        local x, z = xPos+offx, zPos+offz
        CSceneSimpleButton.sScenePosition = Vector3(x,tmppos.y+2,z)
        CUIManager.ShowUI(CUIResources.SceneSimpleButton)
    end
end

function CLuaHouseMgr.NewHouseWallFloorPaperUnlocked(houseId, paperType, isInDuration,oodTime)
    --print("NewHouseWallFloorPaperUnlocked",houseId, paperType, isInDuration)
    local unlockWallFloorType = CClientHouseMgr.Inst.mCurFurnitureProp2.UnlockWallFloorType
	if isInDuration == 1 then--新增
		local now = CServerTimeMgr.Inst.timeStamp
		unlockWallFloorType:SetBit(paperType,true)
		CUIManager.CloseUI(CLuaUIResources.HouseUnlockWallpaperWnd)

		local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
		if sceneType == EnumHouseSceneType_lua.eRoom then
			if not CClientHouseMgr.Inst:IsHouseOwner() and not CLuaHouseMgr.RoomDecorationPrivalege then 
				return 
			end
			sceneType = EnumHouseRoomType.Room
		elseif sceneType == EnumHouseSceneType_lua.eXiangfang then
			sceneType = EnumHouseRoomType.Xiangfang
		end
		local paperData = House_WallPaper.GetData(paperType)
        --print("ooTime",oodTime)
        if oodTime ~= 0 then
            local outDateMap = CClientHouseMgr.Inst.mCurFurnitureProp2.WallFloorOutOfDateTime
            if  not CommonDefs.DictContains(outDateMap, typeof(Byte), paperType) then
                CommonDefs.DictAdd(outDateMap, typeof(Byte), paperType, typeof(UInt32), oodTime)
            else
                CommonDefs.DictSet(outDateMap, typeof(Byte), paperType, typeof(UInt32), oodTime)
            end
            print(CClientHouseMgr.Inst.mCurFurnitureProp2.WallFloorOutOfDateTime)
        end
		Gac2Gas.RequestSetWallOrFloorPaperType(sceneType,paperData.Place,paperType)
		g_ScriptEvent:BroadcastInLua("UpdateWallPaperUnlock")
		CItemInfoMgr.CloseItemInfoWnd()
	else--过期
		unlockWallFloorType:SetBit(paperType,false)
		g_ScriptEvent:BroadcastInLua("UpdateWallPaperUnlock")
	end
end

function Gas2Gac.AddChuanjiabaoToScene(id, chuanjiabaoItemid, parentId, slotIdx)
    CClientFurnitureMgr.Inst:AddChuanjiabaoToScene(id, chuanjiabaoItemid, parentId, slotIdx - 1)
end
function Gas2Gac.RemoveChuanjiabaoFromScene(id, parentId, slotIdx)
    CClientFurnitureMgr.Inst:RemoveChuanjiabaoFromScene(id, parentId, slotIdx - 1)
end

--#region 批量操作
CLuaHouseMgr.s_OpenFurnitureMultipleEdit = true

function CLuaHouseMgr.ShowMultipleFurnishPopWnd()

    if not CLuaHouseMgr.MultipleRoot then
        return
    end
    if not CLuaHouseMgr.MultipleRoot.mChildList or CLuaHouseMgr.MultipleRoot.mChildList.Count == 0 then
        return
    end
    local fur = CLuaHouseMgr.MultipleRoot.mChildList[0]
    if not fur or not fur.RO then
        return
    end

    local list = CommonDefs.ListCreate(typeof(HousePopupMenuItemData))
    
    CommonDefs.ListAdd_LuaCall(list, HousePopupMenuItemData("", DelegateFactory.Action_int(function ( index )
        CLuaHouseMgr.PlaceBack_Multiple()
    end), "UI/Texture/Transparent/Material/housepopupmenu_delete.mat", false, true, -1))

    if CLuaZswTemplateMgr.IsInCustomMode then
        CommonDefs.ListAdd_LuaCall(list, HousePopupMenuItemData("", DelegateFactory.Action_int(function ( index )
            CLuaHouseMgr.SaveAsCustomTemplate_Multiple()
        end), "UI/Texture/Transparent/Material/housepopupmenu_muban.mat", false, true, -1))
    else
        CommonDefs.ListAdd_LuaCall(list, HousePopupMenuItemData("", DelegateFactory.Action_int(function ( index )
            if CLuaZswTemplateMgr.IsCustomPlacing then
                CLuaHouseMgr.PlaceCustomTemplate_Multiple()
            else
                CLuaHouseMgr.PlaceFurniture_Multiple()
            end          
        end), "UI/Texture/Transparent/Material/housepopupmenu_loc.mat", false, true, -1))

        CommonDefs.ListAdd_LuaCall(list, HousePopupMenuItemData("", DelegateFactory.Action_int(function ( index )
            CLuaHouseMgr.ShouHuiCangKu_Multiple()
        end), "UI/Texture/Transparent/Material/housepopupmenu_shouhuicangku.mat", false, true, -1))

        CommonDefs.ListAdd_LuaCall(list, HousePopupMenuItemData("", DelegateFactory.Action_int(function ( index )
            CLuaHouseMgr.Rotate_Multiple()
        end), "UI/Texture/Transparent/Material/housepopupmenu_xuanzhuan.mat", false, true, -1))

        if CLuaHouseMgr.CheckNeedShowXuanFu_Multiple() then
            CommonDefs.ListAdd_LuaCall(list, HousePopupMenuItemData("", DelegateFactory.Action_int(function ( index )
                g_ScriptEvent:BroadcastInLua("UpdateXuanFuSliderActive",true)
            end), "UI/Texture/Transparent/Material/housepopupmenu_xuanfu.mat", false, true, -1))
        end
    end

    CHousePopupMgr.Items = CommonDefs.ListToArray(list)
    CHousePopupMgr.Target = CLuaHouseMgr.MultipleRoot.transform--fur.RO.transform
    CUIManager.ShowUI(CLuaUIResources.MultipleFurnishPopupMenu)
end

function CLuaHouseMgr.CheckNeedShowXuanFu_Multiple()
    --飞花
    if not CClientMainPlayer.Inst then
        return false
    end

    if CClientMainPlayer.Inst.BasicProp.IsOpenHouseXuanFuEdit<=0 then
        return false
    end
    --RevolveSwitch
    for i=0,CLuaHouseMgr.MultipleRoot.mChildList.Count-1,1 do
        local fur = CLuaHouseMgr.MultipleRoot.mChildList[i]
        if fur then
            local zswId = fur.TemplateId
            local data = Zhuangshiwu_Zhuangshiwu.GetData(zswId)
            if CLuaHouseMgr.CheckCanXuanFu(fur, data) then
                return true
            end
        end
    end

    return false
end


function CLuaHouseMgr.TryAddFurnitureWithoutFurObj(fid, tid, x, y, z, xRot, yRot, zRot, chuanjiabaoId )
    CLuaClientFurnitureMgr.CreateFurnitureFromRepositoryContinued(tid)
    if CLuaHouseWoodPileMgr.IsWoodPile(tid) then
        Gac2Gas.RequestCreateWoodPile(tid,
            math.floor(x),
            math.floor(z),
            math.floor(yRot))
    else
        Gac2Gas.AddFurniture(fid, tid,
			math.floor(x),
        	0,
        	math.floor(z),
        	math.floor(yRot),
        	false,
        	chuanjiabaoId,
        	CommonDefs.EmptyDictionaryMsgPack)
    end

    local xPos = math.floor(x) + 0.5
    local zPos = math.floor(z) + 0.5
    local terrainY = CClientFurnitureMgr.Inst:GetLogicHeightByTemplateId(tid,xPos, zPos)

	local isInYard = CClientHouseMgr.Inst:GetCurSceneType() == EnumHouseSceneType_lua.eYard
	local data = Zhuangshiwu_Zhuangshiwu.GetData(tid)
	if isInYard and data.Location == EnumFurnitureLocation_lua.eYardFenceAndRoomWall then
		terrainY = CRenderScene.Inst:SampleLogicHeight(xPos, zPos)
	end
    if data.SubType == EnumFurnitureSubType_lua.eRollerCoasterTrack then
        terrainY = CHouseRollerCoasterMgr.GetBaseHeight()
    end

    local yOffset = y - terrainY - CClientFurniture.s_YOffset
    --可以视为y方向没有移动
    if yOffset < 0.001 and yOffset>-0.001 then
        yOffset = 0
    end

    yRot = (math.floor(yRot / 90)) * 90
    if CLuaHouseWoodPileMgr.IsWoodPile(tid) then
		--Do nothing
    else
        Gac2Gas.MoveFurniture(fid, tid, x, yOffset, z, yRot, false)
    end
end

function Gas2Gac.AddFurniture(id, templateId, x, y, z, rot, EngineId, chuanjiabaoId, extraOption)
	local CSGas2Gac=import "L10.Game.Gas2Gac"
	CSGas2Gac.m_Instance:AddFurniture(id, templateId, x, y, z, rot, EngineId, chuanjiabaoId, extraOption)

	if CLuaHouseMgr.CrtMultiPlaceFur then
		local info = CLuaHouseMgr.CrtMultiPlaceFur
		if info.Tid == templateId and info.x == x and info.z == z then
			CLuaHouseMgr.MultiFurniturePlaceNext(id)
		end
	end

    if CLuaHouseMgr.MultipleWinterSkinCache and next(CLuaHouseMgr.MultipleWinterSkinCache) then
        local keypos = Vector3(x,y,z)
        keypos = SafeStringFormat3("%s,%s,%s",keypos.x,keypos.y,keypos.z)
        local cacheList = CLuaHouseMgr.MultipleWinterSkinCache[keypos]

        if cacheList then
            for i=1,#cacheList,1 do
                local v = cacheList[i]
                if v.templateId == templateId then
                    local fur = CClientFurnitureMgr.Inst:GetFurniture(id)
                    if fur.IsWinter ~= v.isWinter then
                        CClientFurniture_RequestChangeWinterSkin(fur)
                    end
                    table.remove(cacheList,i)
                    break
                end
                
            end
        end
    end

end


function CLuaHouseMgr.MultiFurniturePlaceNext(parentId)
	local info = CLuaHouseMgr.CrtMultiPlaceFur
	if not info or info.SendStatus == "Done" then
		if not next(CLuaHouseMgr.MultiPlaceCandidate) then return end
		local idx, info = next(CLuaHouseMgr.MultiPlaceCandidate)
		CLuaHouseMgr.MultiPlaceCandidate[idx] = nil
		CLuaHouseMgr.CrtMultiPlaceFur = info
		local rot = info.rot
		local data = Zhuangshiwu_Zhuangshiwu.GetData(info.Tid)
		local isXuanfu = CLuaHouseMgr.CanXuanFuByDesign(data)

        local xPos = math.floor(info.x) + 0.5
        local zPos = math.floor(info.z) + 0.5
		local terrainY = CClientFurnitureMgr.Inst:GetLogicHeightByTemplateId(info.Tid,xPos, zPos)
		if data.SubType == EnumFurnitureSubType_lua.eRollerCoasterTrack then
            --找一个基准高度，50,50是名园和普通家园的内部
            terrainY = CHouseRollerCoasterMgr.GetBaseHeight()
        end
        --CLuaHouseMgr.MultipleWinterSkinCache
        local keypos = Vector3(info.x,isXuanfu and info.y-terrainY-CClientFurniture.s_YOffset or 0,info.z)
        keypos = SafeStringFormat3("%s,%s,%s",keypos.x,keypos.y,keypos.z)
        local cacheList = CLuaHouseMgr.MultipleWinterSkinCache[keypos]
        if not cacheList then
            cacheList = {}
        end
        local skininfo = {
            templateId = info.Tid,
            isWinter = info.isWinter,
        }
        table.insert(cacheList,skininfo)
        CLuaHouseMgr.MultipleWinterSkinCache[keypos] = cacheList
		Gac2Gas.CreateAndAddFurniture(info.Tid,
			info.x, isXuanfu and info.y-terrainY-CClientFurniture.s_YOffset or 0, info.z,
			rot, false, info.chuanjiabaoId or "",
            CommonDefs.EmptyDictionaryMsgPack,
			info.xRot, info.yRot, info.zRot,
			info.Scale)
		if info.Child and next(info.Child) then
			info.SendStatus = "Sended"
		else
			info.SendStatus = "Done"
		end

	elseif info.SendStatus == "Sended" then
		if info.Child and next(info.Child) then
			local childInfo = nil
			for i = 1, #info.Child do
				childInfo = info.Child[i]
				if childInfo.chuanjiabaoId and childInfo.chuanjiabaoId ~= "" then
					Gac2Gas.ExchangeFurniture(
						EnumFurniturePlace_lua.eChuanjiabao,
						childInfo.Tid,
						EnumFurniturePlace_lua.eDecoration,
						parentId, 
						CClientMainPlayer.Inst.Id, childInfo.chuanjiabaoId)
				else
					Gac2Gas.ExchangeFurniture(
						EnumFurniturePlace_lua.eRepository,
						childInfo.Tid,
						EnumFurniturePlace_lua.eDecoration,
						parentId, 
						CClientMainPlayer.Inst.Id, "")
				end
			end
			CLuaHouseMgr.CrtMultiPlaceFur = childInfo
			childInfo.SendStatus = "Done"
			childInfo.ParentId = parentId
		end
	end
end

function CLuaHouseMgr.HasCandidatesInInterationOrUse()
    if CLuaHouseMgr.MultipleRoot then
        for i=0,CLuaHouseMgr.MultipleRoot.mChildList.Count-1,1 do
            local fur = CLuaHouseMgr.MultipleRoot.mChildList[i]
            local children = fur.Children
            if children then
                --装饰物上有其他装饰物或者人
                for j=0, children.Length-1,1 do
                    local childFid = children[j]
                    if childFid > 0 then
                        --IsInUse
                        local obj = CClientPlayerMgr.Inst:GetPlayer(childFid)
                        if obj then
                            g_MessageMgr:ShowMessage("Cant_Pack_Furniture_In_Use")
                            return true
                        end
                        obj = CClientObjectMgr.Inst:GetObject(childFid)
                        if obj then
                            g_MessageMgr:ShowMessage("Cant_Pack_Furniture_In_Use")
                            return true
                        end
                        --IsHasDecoration
                        local cfur = CClientFurnitureMgr.Inst:GetFurniture(childFid)
                        if cfur then
                            g_MessageMgr:ShowMessage("Cant_Pack_Furniture_With_Decoration")
                            return true
                        end
                        --chuanjiabao         
                        g_MessageMgr:ShowMessage("Cant_Pack_Furniture_With_Decoration")
                        return true
                    else
                        local obj = CClientObjectMgr.Inst:GetObject(math.abs(childFid))
                        if obj then
                            g_MessageMgr:ShowMessage("Cant_Pack_Furniture_In_Use")
                            return true
                        end
                            
                    end
                end
                
            end
            --马厩里有没有马
            if fur.TemplateId == CClientFurnitureMgr.Inst.sMajiuTemplateId then
                if CZuoQiMgr.Inst.m_MajiuMapiInfo and CZuoQiMgr.Inst.m_MajiuMapiInfo.Count > 0 then
                    g_MessageMgr:ShowMessage("Majiu_Cant_Move_With_Mapi")                 
                    return true
                end
            end
            --温泉池 看下是否有温泉装饰物在上面
            if fur.TemplateId ==9448 then
                local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(9448)
                if IsAnythingOnWenQuanChi(fur.ID,zswdata.SpaceWidth,zswdata.SpaceHeight) then
                    g_MessageMgr:ShowMessage("Cant_Pack_Furniture_With_Decoration")
                    return true
                end
            end
            --过山车运行时 所有过山车相关装饰物都不能编辑
            if LuaHouseRollerCoasterMgr.s_PlayInfo and next(LuaHouseRollerCoasterMgr.s_PlayInfo) then 
                local subType = fur:GetSubType()
                if subType == EnumFurnitureSubType_lua.eRollerCoasterStation or subType == EnumFurnitureSubType_lua.eRollerCoasterTrack or subType == EnumFurnitureSubType_lua.eRollerCoasterCabin then
                    g_MessageMgr:ShowMessage("Cant_Pack_Roller_Coaster_In_Use")
                    return true
                end
            end
        end
    end
    return false
end

function CLuaHouseMgr.PlaceCustomTemplate_Multiple()
    if not CLuaHouseMgr.CanPlaceAll_Multiple() then
        g_MessageMgr:ShowMessage("Cannot_MultiplePlace_Barrier")
        return
    end
    local function pack_fur_info(fur)
		if not fur then return nil end
        local zdata = Zhuangshiwu_Zhuangshiwu.GetData(fur.TemplateId)
        local ry = fur.RO.transform.eulerAngles.y
		local rx = fur.RO.transform.eulerAngles.x
		local rz = fur.RO.transform.eulerAngles.z
        local rotation = math.floor(ry+0.5)      

        rx = math.max(0,math.floor(rx/360*256))
        ry = math.max(0,math.floor(ry/360*256))
        rz = math.max(0,math.floor(rz/360*256))

        if zdata.RevolveSwitch ~= 1 and (rotation % 90) == 0 then 
            rx, ry, rz = 360, math.max(0, math.floor(ry/360*256)), 360
        end

		local info = {
			Tid = fur.TemplateId,
			x = fur.RO.Position.x,
			z = fur.RO.Position.z,
			y = fur.RO.Position.y,
			yRot = ry,
			xRot = rx,
			zRot = rz,
            rot = rotation,
			chuanjiabaoId = fur.ChuanjiabaoId,
			Scale = CClientFurnitureMgr.GetScaleFloatToUI8(fur.RO.Scale),
		}
		return info
	end
    if CLuaHouseMgr.MultipleRoot then
        CLuaHouseMgr.MultipleRoot:RevertAllChildrenParentsRoot()
        Gac2Gas.BeginApplyCustomFurnitureTemplate(CLuaZswTemplateMgr.CurPlacingCustomTemplateIdx,CLuaHouseMgr.MultipleRoot.mChildList.Count)
        for i=0,CLuaHouseMgr.MultipleRoot.mChildList.Count-1,1 do
            local fur = CLuaHouseMgr.MultipleRoot.mChildList[i]
			local info = pack_fur_info(fur)
			local data = Zhuangshiwu_Zhuangshiwu.GetData(info.Tid)
		    local isXuanfu = CLuaHouseMgr.CanXuanFuByDesign(data)
            local rot = info.rot
            local xPos = math.floor(info.x) + 0.5
            local zPos = math.floor(info.z) + 0.5
		    local terrainY = CClientFurnitureMgr.Inst:GetLogicHeightByTemplateId(info.Tid,xPos, zPos)
		    if data.SubType == EnumFurnitureSubType_lua.eRollerCoasterTrack then
                --找一个基准高度，50,50是名园和普通家园的内部
                terrainY = CHouseRollerCoasterMgr.GetBaseHeight()
            end
		    Gac2Gas.CreateAndAddFurniture(info.Tid,
			    info.x, isXuanfu and info.y-terrainY-CClientFurniture.s_YOffset or 0, info.z,
			    rot, false, info.chuanjiabaoId or "",
                CommonDefs.EmptyDictionaryMsgPack,
			    info.xRot, info.yRot, info.zRot,
			    info.Scale)
        end
        Gac2Gas.EndApplyCustomFurnitureTemplate(CLuaZswTemplateMgr.CurPlacingCustomTemplateIdx)
    end
    local tindex = CLuaZswTemplateMgr.CustomFillingData and CLuaZswTemplateMgr.CustomFillingData.templateIdx or 1
    CLuaZswTemplateMgr.FastClearCustomTemplate(tindex)
    CUIManager.CloseUI(CLuaUIResources.MultipleFurnishPopupMenu)
    CUIManager.CloseUI(CLuaUIResources.MultipleFurnitureEditWnd)
end

function CLuaHouseMgr.PlaceFurniture_Multiple()
    if CLuaHouseMgr.HasCandidatesInInterationOrUse() then
        return
    end

    if not CLuaHouseMgr.CanPlaceAll_Multiple() then
        g_MessageMgr:ShowMessage("Cannot_MultiplePlace_Barrier")
        return
    end
	local function pack_fur_info(fur)
		if not fur then return nil end
        local zdata = Zhuangshiwu_Zhuangshiwu.GetData(fur.TemplateId)
        local ry = fur.RO.transform.eulerAngles.y
		local rx = fur.RO.transform.eulerAngles.x
		local rz = fur.RO.transform.eulerAngles.z
        local rotation = math.floor(ry+0.5)      

        rx = math.max(0,math.floor(rx/360*256))
        ry = math.max(0,math.floor(ry/360*256))
        rz = math.max(0,math.floor(rz/360*256))

        if zdata.RevolveSwitch ~= 1 and (rotation % 90) == 0 then 
            rx, ry, rz = 360, math.max(0, math.floor(ry/360*256)), 360
        end

		local info = {
			Tid = fur.TemplateId,
			x = fur.RO.Position.x,
			z = fur.RO.Position.z,
			y = fur.RO.Position.y,
			yRot = ry,
			xRot = rx,
			zRot = rz,
            rot = rotation,
			chuanjiabaoId = fur.ChuanjiabaoId,
			Scale = CClientFurnitureMgr.GetScaleFloatToUI8(fur.RO.Scale),
            isWinter = fur.IsWinter,
		}
		return info
	end

    if CLuaHouseMgr.MultipleRoot then
		local allFurniture = {}
        CLuaHouseMgr.MultipleRoot:RevertAllChildrenParentsRoot()
        for i=0,CLuaHouseMgr.MultipleRoot.mChildList.Count-1,1 do
            local fur = CLuaHouseMgr.MultipleRoot.mChildList[i]
			local info = pack_fur_info(fur)
			if fur:IsFurnitureHasDecoration() then
				info.Child = {}
				for i = 0, fur.Children.Length do
					local fid = fur.Children[i]
					local child = fid and CClientFurnitureMgr.Inst:GetFurniture(fid)
					if child then
						local childInfo = pack_fur_info(child)
						info.Child[i+1] = childInfo
						if childInfo.chuanjiabaoId and childInfo.chuanjiabaoId ~= "" then
							Gac2Gas.ExchangeFurniture(EnumFurniturePlace_lua.eDecoration, fur.ID,
								EnumFurniturePlace_lua.eChuanjiabao, i+1, CClientMainPlayer.Inst.Id, childInfo.chuanjiabaoId)
						else
							Gac2Gas.ExchangeFurniture(EnumFurniturePlace_lua.eDecoration, fur.ID,
								EnumFurniturePlace_lua.eRepository, i+1, CClientMainPlayer.Inst.Id, tostring(child.ID))
						end
					end
				end
			end
			table.insert(allFurniture, info)
			Gac2Gas.ExchangeFurniture(EnumFurniturePlace_lua.eFurniture, fur.ID,
				EnumFurniturePlace_lua.eRepository, 0, CClientMainPlayer.Inst.Id, "")
        end

		CLuaHouseMgr.MultiPlaceCandidate = allFurniture

		CLuaHouseMgr.MultiFurniturePlaceNext()
        CLuaHouseMgr.MultipleRoot:ClearChildren(false)
        g_ScriptEvent:BroadcastInLua("UpdateMultipleFurnitureChoice")
    end

    CUIManager.CloseUI(CLuaUIResources.MultipleFurnishPopupMenu)
end

function CLuaHouseMgr.PlaceBack_Multiple()
    if CLuaHouseMgr.MultipleRoot then
        CLuaHouseMgr.MultipleRoot:RevertAllChildrenParentsRoot()
        CLuaHouseMgr.MultipleRoot:ClearChildren(true)
        g_ScriptEvent:BroadcastInLua("UpdateMultipleFurnitureChoice")
    end
    CUIManager.CloseUI(CLuaUIResources.MultipleFurnishPopupMenu)
end

function CLuaHouseMgr.ShouHuiCangKu_Multiple()
    if CLuaHouseMgr.HasCandidatesInInterationOrUse() then
        return
    end
    local hasColor = false
    local msg = LocalString.GetString("确定将所有选中装饰物收起吗？")
    local hasColor = false

    if CLuaHouseMgr.MultipleRoot then
        CLuaHouseMgr.MultipleRoot:RevertAllChildrenParentsRoot()
        for i=0,CLuaHouseMgr.MultipleRoot.mChildList.Count-1,1 do
            local fur = CLuaHouseMgr.MultipleRoot.mChildList[i]
            if Zhuangshiwu_ZhuangshiwuColor.Exists(fur.FurnishTemplateId) then
                local furnitureColor =  CClientHouseMgr.Inst and CClientHouseMgr.Inst.mCurFurnitureProp2.FurnitureColor
                if furnitureColor then
                    local color = CommonDefs.DictGetValue_LuaCall(furnitureColor,fur.ID)
                    if color then
                        hasColor = true
                        break
                    end
                end
            end
        end
    end

    if hasColor then
        msg = LocalString.GetString("确定将所有选中装饰物收起吗？（含有带颜色的装饰物）")
    end

    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
        for i=0,CLuaHouseMgr.MultipleRoot.mChildList.Count-1,1 do
            local fur = CLuaHouseMgr.MultipleRoot.mChildList[i]
            if System.String.IsNullOrEmpty(fur.ChuanjiabaoId) then
                Gac2Gas.ExchangeFurniture(EnumFurniturePlace_lua.eFurniture, fur.ID, EnumFurniturePlace_lua.eRepository, 0, CClientMainPlayer.Inst.Id, "")
            else
                Gac2Gas.ExchangeFurniture(EnumFurniturePlace_lua.eFurniture, fur.ID, EnumFurniturePlace_lua.eChuanjiabao, fur.ID, CClientMainPlayer.Inst.Id, fur.ChuanjiabaoId)
            end
        end

        CUIManager.CloseUI(CLuaUIResources.MultipleFurnishPopupMenu)
        CLuaHouseMgr.MultipleRoot:ClearChildren(true)
        g_ScriptEvent:BroadcastInLua("UpdateMultipleFurnitureChoice")
    end), nil, nil, nil, false)
end

function CLuaHouseMgr.Rotate_Multiple()
    if CLuaHouseMgr.MultipleRoot and CLuaHouseMgr.MultipleRoot.transform then
        local y = math.floor(CLuaHouseMgr.MultipleRoot.mRootRot)
        y = (y+360)%360
        y = math.floor(y/90)*90
        local delta = 90
        local newY = (y + delta) % 360
        CLuaHouseMgr.MultipleRoot:SetRootRot(newY)
    end
end

function CLuaHouseMgr.GetMultipleRoot()
    if CLuaHouseMgr.MultipleRoot and CLuaHouseMgr.MultipleRoot.transform then
        return CLuaHouseMgr.MultipleRoot
    end
    local rootGo = GameObject("MultipleFurnitureRoot")
    local houseRoot = CClientObjectRoot.Inst:GetParent("House");

    rootGo.transform.parent = houseRoot.transform
    rootGo.transform.position = Vector3(0,0,0)
    rootGo.transform.localEulerAngles = Vector3(0, 0, 0)
    CLuaHouseMgr.MultipleRoot = rootGo:AddComponent(typeof(CMultipleFurnitureRoot))

    return CLuaHouseMgr.MultipleRoot
end

function CLuaHouseMgr.CanPlaceAll_Multiple()
    local canPutAll = true
    for i=0,CLuaHouseMgr.MultipleRoot.mChildList.Count-1,1 do
        local fur = CLuaHouseMgr.MultipleRoot.mChildList[i]
        if not fur:CheckMoveFurniture() then
            canPutAll = false
            return
        end
    end
    return canPutAll
end

function CLuaHouseMgr.IsInMultipleEditing(fid)
    if not CLuaHouseMgr.MultipleRoot then
        return false
    end
    if not CLuaHouseMgr.MultipleRoot.mEditingDict then
        return false
    end
    return CommonDefs.DictContains_LuaCall(CLuaHouseMgr.MultipleRoot.mEditingDict, fid)
end

function CLuaHouseMgr.SaveAsCustomTemplate_Multiple()
    if CLuaHouseMgr.HasCandidatesInInterationOrUse() then
        return
    end

    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("AddCustomTemplate_PlaceBack_Comfire"), DelegateFactory.Action(function ()            
		--存自定义模板
        CLuaZswTemplateMgr.SaveAsCustomTemplate_Multiple()   
        --收回仓库
        if CLuaHouseMgr.MultipleRoot then
            CLuaHouseMgr.MultipleRoot:RevertAllChildrenParentsRoot()
        end
        for i=0,CLuaHouseMgr.MultipleRoot.mChildList.Count-1,1 do
            local fur = CLuaHouseMgr.MultipleRoot.mChildList[i]
            if System.String.IsNullOrEmpty(fur.ChuanjiabaoId) then
                Gac2Gas.ExchangeFurniture(EnumFurniturePlace_lua.eFurniture, fur.ID, EnumFurniturePlace_lua.eRepository, 0, CClientMainPlayer.Inst.Id, "")
            else
                Gac2Gas.ExchangeFurniture(EnumFurniturePlace_lua.eFurniture, fur.ID, EnumFurniturePlace_lua.eChuanjiabao, fur.ID, CClientMainPlayer.Inst.Id, fur.ChuanjiabaoId)
            end
        end
        CUIManager.CloseUI(CLuaUIResources.MultipleFurnishPopupMenu)
        CLuaHouseMgr.MultipleRoot:ClearChildren(true)
        g_ScriptEvent:BroadcastInLua("UpdateMultipleFurnitureChoice")
        --退出批量操作
        CUIManager.CloseUI(CLuaUIResources.MultipleFurnitureEditWnd)
	end), nil, nil, nil, false)
end

--连铺
function CLuaHouseMgr.ProcessLianPuPopupMenu(this,itemActionPairs)
    if not IsOpenFurnitureLianPu() then
        return
    end
    --只支持90度 连铺
    local mUintRotateYContinued = this.ServerRotateY
    if mUintRotateYContinued%180~=90 and mUintRotateYContinued%180~=0 then
        return
    end

    if this:IsPlacedInServer() then
        if this.TemplateId <= 9146 and this.TemplateId >= 9142 then
            table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)            
                CLuaHouseMgr.mCurLianPuFur = this
                CLuaHouseMgr.RequestPlaceFurniture(this)
                --CUIManager.CloseUI(CUIResources.HousePopupMenu)
                CUIManager.ShowUI(CLuaUIResources.FurnitureLianPuWnd)
            end), "UI/Texture/Transparent/Material/housepopupmenu_suofang.mat", true, true, -1))
        end
    end
end

function CLuaHouseMgr.TryFoldQishuMessage(content,uuid,isExpand)
    if StringStartWith(content,"Furniture_Duration_Zero_New_1") then
        local templateIds = {}
        for segment in string.gmatch(content, "([^,]+)") do
            local id = tonumber(segment)
            if id and id > 0 then
                table.insert(templateIds,id)
            end
        end
        local count = #templateIds
        --只有一个 不需要折叠
        if count == 1 then
            content = SafeStringFormat3(LocalString.GetString("%s耐久度为0。#r<link button=点击前往处理,GoHomeAndOpenQishuWnd>"),Item_Item.GetData(templateIds[1]).Name)
            content = CChatLinkMgr.TranslateToNGUIText(content, false)
            return content
        end
        --需要折叠
        if templateIds and next(templateIds) then
            local newcontent = ""--LocalString.GetString("以下气数装饰物耐久度为0:\n")
            for idx,id in ipairs(templateIds) do
                local data = Item_Item.GetData(id)
                if not isExpand then
                    newcontent = SafeStringFormat3("%s%s......#r",newcontent,g_MessageMgr:FormatMessage("Furniture_Duration_Zero_New_2",data.Name))
                    break
                end
                if data then
                    if idx == count then
                        newcontent = SafeStringFormat3("%s%s",newcontent,g_MessageMgr:FormatMessage("Furniture_Duration_Zero",data.Name))
                    else
                        newcontent = SafeStringFormat3("%s%s",newcontent,g_MessageMgr:FormatMessage("Furniture_Duration_Zero_New_2",data.Name))
                    end
                    
                end
            end
            local btnName = "#cFFFFFF%s#n"
            if isExpand then
                newcontent = SafeStringFormat3(LocalString.GetString("%s#r<link button=#c1A3452#u收起#n#n,QiShuFoldExpand,%s,%s>"),newcontent,uuid,"0")
            else
                newcontent = SafeStringFormat3(LocalString.GetString("%s#r<link button=#c1A3452#u展开#n#n,QiShuFoldExpand,%s,%s>"),newcontent,uuid,"1")
            end
            newcontent = g_MessageMgr:FormatMessage("Furniture_Duration_Zero_New_1","",newcontent)


            return newcontent
        end
        
    end
    return content
end