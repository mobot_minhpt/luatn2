require("common/common_include")
require("design/Equipment/ShenBing")

local UITable = import "UITable"
local Extensions = import "Extensions"
local UIEventListener = import "UIEventListener"
local LuaUtils = import "LuaUtils"
local NGUITools = import "NGUITools"
local NGUIText = import "NGUIText"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local QnRadioBox = import "L10.UI.QnRadioBox"
local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local DefaultItemActionDataSource = import "L10.UI.DefaultItemActionDataSource"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local AlignType3 = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"

CLuaShenbingColorComposeWnd = class()
RegistClassMember(CLuaShenbingColorComposeWnd, "m_ColorTypeUITable")
RegistClassMember(CLuaShenbingColorComposeWnd, "m_ColorTypeObjTable")
RegistClassMember(CLuaShenbingColorComposeWnd, "m_ColorTypeDirTransTable")
RegistClassMember(CLuaShenbingColorComposeWnd, "m_ColorTypeSVRootTable")
RegistClassMember(CLuaShenbingColorComposeWnd, "m_ColorTypeListTable")
RegistClassMember(CLuaShenbingColorComposeWnd, "m_ColorTemplateObj")

-- DATA
RegistClassMember(CLuaShenbingColorComposeWnd, "m_ColorObj2IdTable")
RegistClassMember(CLuaShenbingColorComposeWnd, "m_CurrentSelectedColorObj")
RegistClassMember(CLuaShenbingColorComposeWnd, "m_CurrentColorId")
RegistClassMember(CLuaShenbingColorComposeWnd, "m_CurrentMethodIndex")

RegistClassMember(CLuaShenbingColorComposeWnd, "m_ComposeColorTable")

RegistClassMember(CLuaShenbingColorComposeWnd, "m_ComposeByColorIndex")

-- Compose
RegistClassMember(CLuaShenbingColorComposeWnd, "m_FinalColorTexture")
RegistClassMember(CLuaShenbingColorComposeWnd, "m_FinalColorNameLabel")
RegistClassMember(CLuaShenbingColorComposeWnd, "m_FinalColorCntInput")
RegistClassMember(CLuaShenbingColorComposeWnd, "m_QnRadioBox")
RegistClassMember(CLuaShenbingColorComposeWnd, "m_ComposeSelectorRoot")

-- Material
RegistClassMember(CLuaShenbingColorComposeWnd, "m_MaterialNameLabelTable")
RegistClassMember(CLuaShenbingColorComposeWnd, "m_MaterialCountLabelTable")
RegistClassMember(CLuaShenbingColorComposeWnd, "m_MaterialSpriteTable")
RegistClassMember(CLuaShenbingColorComposeWnd, "m_MaterialTextureTable")
RegistClassMember(CLuaShenbingColorComposeWnd, "m_MaterialGotObjTable")
RegistClassMember(CLuaShenbingColorComposeWnd, "m_MaterialItemTemplateIdTable")
RegistClassMember(CLuaShenbingColorComposeWnd, "m_CurrentSelectedItemIndex")

function CLuaShenbingColorComposeWnd:ParseDesignData( ... )
	self.m_ComposeColorTable = {}
	local cnt = #Lua_ShenBing_ColorationTransform
	for i = 1, cnt do
		for j = 1, cnt do
			if Lua_ShenBing_ColorationTransform[i][j] > 0 then
				local composeId = Lua_ShenBing_ColorationTransform[i][j]
				if not self.m_ComposeColorTable[composeId] then
					self.m_ComposeColorTable[composeId] = {}
				end

				local exist = false
				for k = 1, #self.m_ComposeColorTable[composeId], 2 do
					if (self.m_ComposeColorTable[composeId][k] == i and self.m_ComposeColorTable[composeId][k+1] == j) or (self.m_ComposeColorTable[composeId][k] == j and self.m_ComposeColorTable[composeId][k+1] == i) then
						exist = true
						break
					end
				end
				if not exist then
					table.insert(self.m_ComposeColorTable[composeId], i)
					table.insert(self.m_ComposeColorTable[composeId], j)
				end
			end
		end
	end
end

function CLuaShenbingColorComposeWnd:Awake( ... )
	self:ParseDesignData()
	self.m_ColorTypeUITable = self.transform:Find("ColorRoot/Table"):GetComponent(typeof(UITable))
	self.m_ColorTypeObjTable = {}
	self.m_ColorTypeDirTransTable = {}
	self.m_ColorTypeSVRootTable = {}
	self.m_ColorTypeListTable = {}
	local types = {"H", "V", "S"}
	for i = 1, #types do
		local root = self.m_ColorTypeUITable.transform:Find(types[i])
		table.insert(self.m_ColorTypeObjTable, root:Find("TitleBg").gameObject)
		table.insert(self.m_ColorTypeDirTransTable, root:Find("TitleBg/Sprite"))
		table.insert(self.m_ColorTypeSVRootTable, root:Find("Scroll View").gameObject)
		table.insert(self.m_ColorTypeListTable, root:Find("Scroll View/Table"):GetComponent(typeof(UITable)))
		UIEventListener.Get(self.m_ColorTypeObjTable[i]).onClick = LuaUtils.VoidDelegate(function (go)
			self:OnColorTypeClick(go)
		end)
		if i == 1 then
			self.m_ColorTemplateObj = self.m_ColorTypeSVRootTable[i].transform:Find("Template").gameObject
			self.m_ColorTemplateObj:SetActive(false)
		end
	end

	self.m_FinalColorTexture = self.transform:Find("ComposeRoot/Color"):GetComponent(typeof(CUITexture))
	self.m_FinalColorNameLabel = self.transform:Find("ComposeRoot/Color/Name"):GetComponent(typeof(UILabel))
	self.m_FinalColorCntInput = self.transform:Find("ComposeRoot/CountInput"):GetComponent(typeof(QnAddSubAndInputButton))
	self.m_FinalColorCntInput:SetMinMax(0, 0, 1)

	self.m_QnRadioBox = self.transform:Find("ComposeRoot/QnRadioBox"):GetComponent(typeof(QnRadioBox))
	self.m_QnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function (btn, index)
		self:SelectComposeMethod(index)
	end)

	self.m_MaterialTextureTable = {}
	self.m_MaterialSpriteTable = {}
	self.m_MaterialCountLabelTable = {}
	self.m_MaterialNameLabelTable = {}
	self.m_MaterialGotObjTable = {}
	for i = 1, 2 do
		local root = self.transform:Find("ComposeRoot/Material/Item"..i)
		--table.insert(self.m_MaterialSpriteTable, root:GetComponent(typeof(UISprite)))
		table.insert(self.m_MaterialTextureTable, root:Find("Texture"):GetComponent(typeof(CUITexture)))
		table.insert(self.m_MaterialGotObjTable, root:Find("Got").gameObject)
		table.insert(self.m_MaterialNameLabelTable, root:Find("Name"):GetComponent(typeof(UILabel)))
		table.insert(self.m_MaterialCountLabelTable, root:Find("Count"):GetComponent(typeof(UILabel)))
	end

	UIEventListener.Get(self.transform:Find("ComposeRoot/ComposeBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:ComposeColor()
	end)

	-- color compose selector
	local composeSelectorTrans = self.transform:Find("ComposeRoot/ComposeSelector")
	self.m_ComposeSelectorRoot = composeSelectorTrans.gameObject
	UIEventListener.Get(composeSelectorTrans.gameObject).onClick = LuaUtils.VoidDelegate(function (go)
		self:ComposeSelectorOnClick(go)
	end)
end

function CLuaShenbingColorComposeWnd:ComposeColor( ... )
	local cnt = self.m_FinalColorCntInput:GetValue()
	if cnt <= 0 then
		g_MessageMgr:ShowMessage("ShenBing_Compose_Color_Fail")
		return
	end
	if self.m_CurrentMethodIndex == 0 then
		Gac2Gas.ProduceShenBingRanLiaoByMaterial(self.m_CurrentColorId, cnt)
	elseif self.m_CurrentMethodIndex == 1 then
		local ids = self.m_ComposeColorTable[self.m_CurrentColorId]
		Gac2Gas.ProduceShenBingRanLiaoByRanLiao(ids[self.m_ComposeByColorIndex * 2 + 1], ids[self.m_ComposeByColorIndex * 2 + 2], cnt)
	end
end

function CLuaShenbingColorComposeWnd:SelectComposeMethod( index )
	self.m_CurrentMethodIndex = index
	self.m_ComposeSelectorRoot:SetActive(index == 1)
	-- compose by material
	if index == 0 then
		if not ShenBing_ColorationFormula.GetData(self.m_CurrentColorId) then
			return
		end
		self.m_MaterialItemTemplateIdTable = {}
		for i = 1, 2 do
			local itemId = i == 1 and ShenBing_ColorationFormula.GetData(self.m_CurrentColorId).Item1 or ShenBing_ColorationFormula.GetData(self.m_CurrentColorId).Item2
			local designData = Item_Item.GetData(itemId)

			self.m_MaterialTextureTable[i].gameObject:SetActive(true)
			self.m_MaterialTextureTable[i]:LoadMaterial(designData.Icon)
			self.m_MaterialTextureTable[i].color = Color.white

			self.m_MaterialGotObjTable[i]:SetActive(false)

			self.m_MaterialNameLabelTable[i].text = designData.Name
			table.insert(self.m_MaterialItemTemplateIdTable, itemId)
			UIEventListener.Get(self.m_MaterialTextureTable[i].gameObject).onClick = LuaUtils.VoidDelegate(function ( go )
				-- compose by color
				if self.m_CurrentMethodIndex == 1 then return end
				local default = DefaultItemActionDataSource.Create(1, {function ( ... )
					CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
					CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_MaterialItemTemplateIdTable[self.m_CurrentSelectedItemIndex], false, self.m_MaterialTextureTable[self.m_CurrentSelectedItemIndex].transform, AlignType.Right)
				end}, {LocalString.GetString("获取")})
				for j = 1, 2 do
					if go == self.m_MaterialTextureTable[j].gameObject then
						CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_MaterialItemTemplateIdTable[j], false, default, AlignType2.Right, 0, 0, 0, 0)
						self.m_CurrentSelectedItemIndex = j
						break
					end
				end
			end)
			self:UpdateItemCount()
		end
	elseif index == 1 then
		--compose by color
		self:InitColorComposeByIndex(0)
	end
end

function CLuaShenbingColorComposeWnd:ComposeSelectorOnClick(go)
	if not self.m_ComposeColorTable[self.m_CurrentColorId] then return end
	local composeTable = {}
	local popupMenuItemTable = {}
	for i = 1, #self.m_ComposeColorTable[self.m_CurrentColorId], 2 do
		local id1, id2 = self.m_ComposeColorTable[self.m_CurrentColorId][i], self.m_ComposeColorTable[self.m_CurrentColorId][i + 1]
		local name1, name2 = ShenBing_Coloration.GetData(id1).Name, ShenBing_Coloration.GetData(id2).Name
		local hasId1, hasId2 = self:GetColorCnt(id1) > 0, self:GetColorCnt(id2) > 0
		local composeName = SafeStringFormat3("[c][%s]%s[-][/c]+[c][%s]%s[/c]", hasId1 and "FFFFFF" or "FF0000", name1, hasId2 and "FFFFFF" or "FF0000", name2)
		table.insert(composeTable, composeName)
		table.insert(popupMenuItemTable, PopupMenuItemData(composeName, DelegateFactory.Action_int(function ( index )
			self:InitColorComposeByIndex(math.floor(i / 2))
		end), false, nil, EnumPopupMenuItemStyle.Light))
	end

	local popupMenuItemArray = Table2Array(popupMenuItemTable, MakeArrayClass(PopupMenuItemData))
	CPopupMenuInfoMgr.ShowPopupMenu(popupMenuItemArray, go.transform, AlignType3.Bottom, 1, nil, 600, true, 296)
end

function CLuaShenbingColorComposeWnd:InitColorComposeByIndex(index)
	if self.m_CurrentMethodIndex ~= 1 then return end
	self.m_ComposeByColorIndex = index
	local maxCnt = 0
	local composeColorName
	for i = 1, 2 do
		--self.m_MaterialTextureTable[i].gameObject:SetActive(false)
		self.m_MaterialGotObjTable[i]:SetActive(false)
		local id = self.m_ComposeColorTable[self.m_CurrentColorId][index * 2 + i]
		self.m_MaterialNameLabelTable[i].text = ShenBing_Coloration.GetData(id).Name
		--self.m_MaterialSpriteTable[i].color = NGUIText.ParseColor(ShenBing_Coloration.GetData(id).RGB, 0)
		self.m_MaterialTextureTable[i]:LoadMaterial(LuaShenbingMgr.BasicColorTexPath)
		self.m_MaterialTextureTable[i].color = NGUIText.ParseColor(ShenBing_Coloration.GetData(id).RGB, 0)
		local cnt = self:GetColorCnt(id)
		self.m_MaterialCountLabelTable[i].text = cnt.."/1"
		if i == 1 then
			maxCnt = cnt
			composeColorName = ShenBing_Coloration.GetData(id).Name
		else
			maxCnt = math.min(maxCnt, cnt)
			composeColorName = composeColorName.."+"..ShenBing_Coloration.GetData(id).Name
		end
	end
	self.m_FinalColorCntInput:SetMinMax(0, maxCnt, 1)
	if maxCnt > 0 then self.m_FinalColorCntInput:SetValue(1) end
end

function CLuaShenbingColorComposeWnd:UpdateItemCount( ... )
	if self.m_CurrentMethodIndex ~= 0 then return end

	local maxCnt = 0
	for i = 1, 2 do
		local itemCnt = i == 1 and ShenBing_ColorationFormula.GetData(self.m_CurrentColorId).count1 or ShenBing_ColorationFormula.GetData(self.m_CurrentColorId).count2
		local itemId = self.m_MaterialItemTemplateIdTable[i]
		local ownCnt = CItemMgr.Inst:GetItemCount(itemId)
		self.m_MaterialCountLabelTable[i].text = ownCnt.."/"..itemCnt
		if i == 1 then
			maxCnt = math.floor(ownCnt / itemCnt)
		else
			maxCnt = math.min(maxCnt, math.floor(ownCnt / itemCnt))
		end

		self.m_MaterialGotObjTable[i]:SetActive(ownCnt < itemCnt)
	end
	self.m_FinalColorCntInput:SetMinMax(0, maxCnt, 1)
	if maxCnt > 0 then self.m_FinalColorCntInput:SetValue(1) end
end

function CLuaShenbingColorComposeWnd:OnColorTypeClick( go )
	for i = 1, 3 do
		local clicked = go == self.m_ColorTypeObjTable[i]
		self.m_ColorTypeSVRootTable[i]:SetActive(clicked)
		local rotZ = clicked and 180 or 0
		Extensions.SetLocalRotationZ(self.m_ColorTypeDirTransTable[i], rotZ)
	end
	self.m_ColorTypeUITable:Reposition()
end

function CLuaShenbingColorComposeWnd:OnClorClick( go )
	if self.m_CurrentSelectedColorObj then
		self.m_CurrentSelectedColorObj.transform:Find("Selected").gameObject:SetActive(false)
	end
	for k, v in pairs(self.m_ColorObj2IdTable) do
		if k == go then
			k.transform:Find("Selected").gameObject:SetActive(true)
			self.m_CurrentSelectedColorObj = go
			self:InitComposeInfo(v)
			break
		end
	end
end

function CLuaShenbingColorComposeWnd:InitComposeInfo( id )
	self.m_CurrentColorId = id

	if ShenBing_Coloration.GetData(id).Type == 1 then
		self.m_FinalColorTexture:LoadMaterial(LuaShenbingMgr.BasicColorTexPath)
		self.m_FinalColorTexture.color = NGUIText.ParseColor(ShenBing_Coloration.GetData(id).RGB, 0)
	elseif ShenBing_Coloration.GetData(id).Type == 2 then
		if ShenBing_Coloration.GetData(id).RGB == "1" then
			self.m_FinalColorTexture:LoadMaterial(LuaShenbingMgr.ValueIncColorTexPath)
		else
			self.m_FinalColorTexture:LoadMaterial(LuaShenbingMgr.ValueDecColorTexPath)
		end
		self.m_FinalColorTexture.color = Color.white
	elseif ShenBing_Coloration.GetData(id).Type == 3 then
		if ShenBing_Coloration.GetData(id).RGB == "1" then
			self.m_FinalColorTexture:LoadMaterial(LuaShenbingMgr.SaturationIncColorTexPath)
		else
			self.m_FinalColorTexture:LoadMaterial(LuaShenbingMgr.SaturationDecColorTexPath)
		end
		self.m_FinalColorTexture.color = Color.white
	end
	self.m_FinalColorNameLabel.text = ShenBing_Coloration.GetData(id).Name

	local visibleGroup = {}
	if ShenBing_ColorationFormula.GetData(id) then
		table.insert(visibleGroup, 0)
	end
	if self.m_ComposeColorTable[id] then
		table.insert(visibleGroup, 1)
	end

	self.m_QnRadioBox:SetVisibleGroup(Table2Array(visibleGroup, MakeArrayClass(Int32)), true)
end

function CLuaShenbingColorComposeWnd:Init( ... )
	self:InitColors()
	self:OnColorTypeClick(self.m_ColorTypeObjTable[CLuaShenbingColorationWnd.m_ColorType])
end

function CLuaShenbingColorComposeWnd:InitColors( ... )
	for i = 1, 3 do
		Extensions.RemoveAllChildren(self.m_ColorTypeListTable[i].transform)
	end
	self.m_ColorObj2IdTable = {}
	local firstColor = false
	for i = 1, ShenBing_Coloration.GetDataCount() do
		local colorType = ShenBing_Coloration.GetData(i).Type
		local obj = NGUITools.AddChild(self.m_ColorTypeListTable[colorType].gameObject, self.m_ColorTemplateObj)
		obj:SetActive(true)
		self.m_ColorObj2IdTable[obj] = i
		obj.transform:Find("Selected").gameObject:SetActive(false)
		UIEventListener.Get(obj).onClick = LuaUtils.VoidDelegate(function ( go )
			self:OnClorClick(go)
		end)
		obj.transform:Find("Count"):GetComponent(typeof(UILabel)).text = self:GetColorCnt(i)

		local colTex = obj:GetComponent(typeof(CUITexture))
		if ShenBing_Coloration.GetData(i).Type == 1 then
			colTex:LoadMaterial(LuaShenbingMgr.BasicColorTexPath)
			colTex.color = NGUIText.ParseColor(ShenBing_Coloration.GetData(i).RGB, 0)
		elseif ShenBing_Coloration.GetData(i).Type == 2 then
			if ShenBing_Coloration.GetData(i).RGB == "1" then
				colTex:LoadMaterial(LuaShenbingMgr.ValueIncColorTexPath)
			else
				colTex:LoadMaterial(LuaShenbingMgr.ValueDecColorTexPath)
			end
		elseif ShenBing_Coloration.GetData(i).Type == 3 then
			if ShenBing_Coloration.GetData(i).RGB == "1" then
				colTex:LoadMaterial(LuaShenbingMgr.SaturationIncColorTexPath)
			else
				colTex:LoadMaterial(LuaShenbingMgr.SaturationDecColorTexPath)
			end
		end

		if not firstColor and colorType == CLuaShenbingColorationWnd.m_ColorType then
			firstColor = true
			self:OnClorClick(obj)
		end
	end
	for i = 1, 3 do
		self.m_ColorTypeListTable[i]:Reposition()
	end
end

function CLuaShenbingColorComposeWnd:UpdateColorCnt( ... )
	for k, v in pairs(self.m_ColorObj2IdTable) do
		k.transform:Find("Count"):GetComponent(typeof(UILabel)).text = self:GetColorCnt(v)
	end
end

function CLuaShenbingColorComposeWnd:GetColorCnt(id)
	if not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.ItemProp then
		return 0
	end
	local cnt = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.ItemProp.ShenBingRanLiao, id)
	if not cnt then return 0 end
	return cnt
end

function CLuaShenbingColorComposeWnd:RefreshItem( ... )
	if self.m_CurrentMethodIndex ~= 0 then
		return
	end
	self:UpdateItemCount()
end

function CLuaShenbingColorComposeWnd:ProduceShenBingColorSuccess( ... )
	self:UpdateColorCnt()
	self:InitColorComposeByIndex(self.m_ComposeByColorIndex)
end

function CLuaShenbingColorComposeWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("SendItem", self, "RefreshItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "RefreshItem")
	g_ScriptEvent:AddListener("ProduceShenBingColorSuccess", self, "ProduceShenBingColorSuccess")
end

function CLuaShenbingColorComposeWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("SendItem", self, "RefreshItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "RefreshItem")
	g_ScriptEvent:RemoveListener("ProduceShenBingColorSuccess", self, "ProduceShenBingColorSuccess")
end

return CLuaShenbingColorComposeWnd
