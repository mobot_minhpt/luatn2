require("3rdParty/ScriptEvent")
require("common/common_include")

local HuluBrothers_Music = import "L10.Game.HuluBrothers_Music"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CHuluBrothersZhaoHuanData = import "L10.UI.CHuluBrothersZhaoHuanData"
local NGUITools = import "NGUITools"
local NGUIText = import "NGUIText"
local UILabel = import "UILabel"
local CUICenterOnChild = import "L10.UI.CUICenterOnChild"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Gac2Gas = import "L10.Game.Gac2Gas"
local CommonDefs = import "L10.Game.CommonDefs"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Extensions = import "Extensions"
local UITable = import "UITable"
local CUIFx = import "L10.UI.CUIFx"

CLuaHuluwaLyricCtrl=class()
RegistClassMember(CLuaHuluwaLyricCtrl,"m_LyricTable")
RegistClassMember(CLuaHuluwaLyricCtrl,"m_LyricTemplate")
RegistClassMember(CLuaHuluwaLyricCtrl,"m_SingButton")
RegistClassMember(CLuaHuluwaLyricCtrl,"m_SingFxRoot")

--data
RegistClassMember(CLuaHuluwaLyricCtrl, "m_PlayerLyricList")

RegistClassMember(CLuaHuluwaLyricCtrl, "m_CurrentLyricId")
RegistClassMember(CLuaHuluwaLyricCtrl, "m_StartPressTime")
RegistClassMember(CLuaHuluwaLyricCtrl, "m_EndPressTime")

RegistClassMember(CLuaHuluwaLyricCtrl, "m_FirstLyricStartTime")
RegistClassMember(CLuaHuluwaLyricCtrl, "m_LastUpdateTime")
RegistClassMember(CLuaHuluwaLyricCtrl, "m_StartSongTime")
RegistClassMember(CLuaHuluwaLyricCtrl, "m_EndSongTime")

function CLuaHuluwaLyricCtrl:Awake()
	self.m_LyricTemplate:SetActive(false)

	local function OnSingButtonPress(go, press)
		if press then
			self.m_StartPressTime = CServerTimeMgr.Inst.timeStamp
		else
			self.m_EndPressTime = CServerTimeMgr.Inst.timeStamp
			self:CheckPressButton(self.m_StartPressTime - self.m_StartSongTime, self.m_EndPressTime - self.m_StartSongTime)
		end
	end
	CommonDefs.AddOnPressListener(self.m_SingButton, DelegateFactory.Action_GameObject_bool(OnSingButtonPress), false)

	local uifx = self.m_SingFxRoot:GetComponent(typeof(CUIFx))
	uifx:LoadFx("Fx/UI/Prefab/UI_huluyuyintishi.prefab", -1, false)
	self.m_SingFxRoot:SetActive(false)

	local data = HuluBrothers_Music.GetData(1)
	self.m_FirstLyricStartTime = data.StartTime
	
	self.m_CurrentLyricId = 0
	self.m_StartPressTime = 0
	self.m_EndPressTime = 0
	self.m_LastUpdateTime = 0
	self.m_StartSongTime = 0
	self.m_EndSongTime = 0
end

function CLuaHuluwaLyricCtrl:CheckPressButton(startPressTime, endPressTime)
	local maxFactor = 0
	for lyricId, playerId in ipairs(self.m_PlayerLyricList) do
		if CClientMainPlayer.Inst.Id == playerId then
			local data = HuluBrothers_Music.GetData(lyricId)
			if data and not (data.StartTime > endPressTime and data.EndTime < startPressTime) then
				local deltaTime = math.abs(startPressTime - data.StartTime) + math.abs(endPressTime - data.EndTime)
				local matchedTime = data.EndTime - data.StartTime - deltaTime
				if matchedTime < 0 then matchedTime = 0 end
				local factor = matchedTime / (data.EndTime - data.StartTime)
				maxFactor = math.max(maxFactor, factor)
			end
		end
	end
	if(maxFactor > 0) then
		Gac2Gas.RequestSingInHuLuSummon(maxFactor)
	end
end

function CLuaHuluwaLyricCtrl:Update()
	local curentTimeStamp = CServerTimeMgr.Inst.timeStamp
	if curentTimeStamp - self.m_LastUpdateTime > 0.5 then
		local passedTime = curentTimeStamp - self.m_StartSongTime
		if self.m_CurrentLyricId == 0 then
			if passedTime > self.m_FirstLyricStartTime then
				self.m_CurrentLyricId = 1
				self:CenterOnLyric(self.m_CurrentLyricId)
			end
		else
			local data = HuluBrothers_Music.GetData(self.m_CurrentLyricId)
			if data and passedTime > data.EndTime then
				self.m_CurrentLyricId = self.m_CurrentLyricId + 1
				self:CenterOnLyric(self.m_CurrentLyricId)
			end
		end
		self.m_LastUpdateTime = CServerTimeMgr.Inst.timeStamp
	end
end

--初始化歌词列表
function CLuaHuluwaLyricCtrl:InitLyrics(playerIds)	
	self.m_PlayerLyricList = {}

	self.m_StartSongTime = CHuluBrothersZhaoHuanData.Inst.CreateTime
	self.m_EndSongTime = CHuluBrothersZhaoHuanData.Inst.EndTime 
	--reload lyric list
	Extensions.RemoveAllChildren(self.m_LyricTable.transform)
	--add title
	local title =  NGUITools.AddChild(self.m_LyricTable ,self.m_LyricTemplate)
	title:SetActive(true)
	local titleLabel = title:GetComponent(typeof(UILabel))
	titleLabel.text = LocalString.GetString("歌名: 葫芦娃")

	local count = playerIds.Count
	for i = 1, count do
		local player = CClientPlayerMgr.Inst:GetPlayer(playerIds[i - 1])
		local musicData = HuluBrothers_Music.GetData(i)
		if player and musicData then
			self.m_PlayerLyricList[i] = playerIds[i - 1]
			local child =  NGUITools.AddChild(self.m_LyricTable ,self.m_LyricTemplate)
			child:SetActive(true)
			local label = child:GetComponent(typeof(UILabel))
			label.text = player.Name .. LocalString.GetString("(唱):") .. musicData.Word
			self:ChangeLyricColor(child, false)
		end
	end
	self.m_LyricTable:GetComponent(typeof(UITable)):Reposition()
	self:CenterOnLyric(0)
	self.m_CurrentLyricId = 0
end

function CLuaHuluwaLyricCtrl:CenterOnLyric(lyricId)
	if(self.m_LyricTable.transform.childCount > lyricId) then
		local child = self.m_LyricTable.transform:GetChild(lyricId)
		self:ChangeLyricColor(child, true)
		if lyricId >= 1 then
			local prechild = self.m_LyricTable.transform:GetChild(lyricId - 1)
			self:ChangeLyricColor(prechild, false)
			if self.m_PlayerLyricList[lyricId] == CClientMainPlayer.Inst.Id then
				self.m_SingFxRoot:SetActive(true)
			else
				self.m_SingFxRoot:SetActive(false)
			end
		end
		local ctrl = self.m_LyricTable:GetComponent(typeof(CUICenterOnChild))
		ctrl:CenterOnInstant(child)
	end
end

function CLuaHuluwaLyricCtrl:ChangeLyricColor(child, highlight)
	if not child then return end
	local label = child:GetComponent(typeof(UILabel))
	--设置字体颜色
	if highlight then
		label.color = NGUIText.ParseColor24("ffffff", 0)
	else
		label.color = NGUIText.ParseColor24("acacac", 0)
	end
end

return CLuaHuluwaLyricCtrl








