local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local GameObject = import "UnityEngine.GameObject"
local CExpressionAppearanceMgr = import "L10.Game.CExpressionAppearanceMgr"

LuaExpressionEquipRenewalWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaExpressionEquipRenewalWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaExpressionEquipRenewalWnd, "Checkbox", "Checkbox", QnCheckBox)
RegistChildComponent(LuaExpressionEquipRenewalWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaExpressionEquipRenewalWnd, "OkButton", "OkButton", GameObject)

--@endregion RegistChildComponent end

function LuaExpressionEquipRenewalWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)

    --@endregion EventBind end
end

function LuaExpressionEquipRenewalWnd:Init()
	local data = Expression_Appearance.GetData(LuaExpressionMgr.m_RenewalEquipApperanceId)
	self.QnCostAndOwnMoney:SetCost(data.RenewalPrice)
	self.NameLabel.text = data.Name
	self.Checkbox:SetSelected(true, false)
	CExpressionAppearanceMgr.AppearancePreviewID = data.PreviewDefine
end

--@region UIEvent

function LuaExpressionEquipRenewalWnd:OnOkButtonClick()
	if self.Checkbox.Selected then
		Gac2Gas.RequestRenewExpressionAppearance(1,LuaExpressionMgr.m_RenewalEquipApperanceId,30)
		CUIManager.CloseUI(CLuaUIResources.ExpressionEquipRenewalWnd)
	else
		g_MessageMgr:ShowMessage("None_Select_ExpressionEquipRenewal")
	end
end

--@endregion UIEvent

