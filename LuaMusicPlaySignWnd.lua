local CUIFx = import "L10.UI.CUIFx"

local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

local UIPanel = import "UIPanel"

local GameObject = import "UnityEngine.GameObject"
local UITabBar = import "L10.UI.UITabBar"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Animator = import "UnityEngine.Animator"
local CMainPlayerInfoMgr = import "L10.UI.CMainPlayerInfoMgr"

LuaMusicPlaySignWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaMusicPlaySignWnd, "ChooseMusicWnd", "ChooseMusicWnd", CCommonLuaScript)
RegistChildComponent(LuaMusicPlaySignWnd, "MainWnd", "MainWnd", GameObject)
RegistChildComponent(LuaMusicPlaySignWnd, "PlayModelTabBar", "PlayModelTabBar", UITabBar)
RegistChildComponent(LuaMusicPlaySignWnd, "AchivementBtn", "AchivementBtn", GameObject)
RegistChildComponent(LuaMusicPlaySignWnd, "EnterChooseBtn", "EnterChooseBtn", GameObject)
RegistChildComponent(LuaMusicPlaySignWnd, "BackToMainBtn", "BackToMainBtn", GameObject)
RegistChildComponent(LuaMusicPlaySignWnd, "CloseButton", "CloseButton", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaMusicPlaySignWnd,"m_PlayModelIndex")--0 单人 1 双人
RegistClassMember(LuaMusicPlaySignWnd,"m_MainWndAni")

function LuaMusicPlaySignWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.AchivementBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAchivementBtnClick()
	end)
	--改为排行榜按钮
	UIEventListener.Get(self.EnterChooseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    --self:OnEnterChooseBtnClick()
		LuaMeiXiangLouMgr.OpenMusicPlayRankWnd()
	end)

	UIEventListener.Get(self.BackToMainBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBackToMainBtnClick()
	end)

	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)

    --@endregion EventBind end
	local tab1 = self.PlayModelTabBar:GetTabGoByIndex(0)
	local tab2 = self.PlayModelTabBar:GetTabGoByIndex(1)
	UIEventListener.Get(tab1).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPlayModelTabChange(go, 0)
	end)

	UIEventListener.Get(tab2).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPlayModelTabChange(go, 1)
	end)

	self.m_MainWndAni = self.transform:GetComponent(typeof(Animator))
	self.m_PlayModelIndex = 0
	LuaMeiXiangLouMgr.m_PlayModelIndex = self.m_PlayModelIndex
	self.TipBtn = self.transform:Find("MainWnd/TipBtn")
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)
end

function LuaMusicPlaySignWnd:Init()

end

function LuaMusicPlaySignWnd:OnPlayModelTabChange(go, index)
	self.m_PlayModelIndex = index
	LuaMeiXiangLouMgr.m_PlayModelIndex = index
	--print("LuaMeiXiangLouMgr.m_PlayModelIndex",LuaMeiXiangLouMgr.m_PlayModelIndex)
	local hightLightTab = self.PlayModelTabBar:GetTabGoByIndex(index)
	local normalTab = self.PlayModelTabBar:GetTabGoByIndex(1-index)
	hightLightTab.transform:Find("Highlight").gameObject:SetActive(true)
	normalTab.transform:Find("Highlight").gameObject:SetActive(false)

	self:OnEnterChooseBtnClick()
end

--@region UIEvent

function LuaMusicPlaySignWnd:OnAchivementBtnClick()
	--CMainPlayerInfoMgr.ShowMainPlayerWnd(2);
	LuaAchievementMgr:ShowDetailWndByAchievementId(61000667)
end

function LuaMusicPlaySignWnd:OnEnterPlayBtnClick()
end

function LuaMusicPlaySignWnd:OnTipBtnClick()
	g_MessageMgr:ShowMessage("MXL_SONGPLAY_RULES")
end

function LuaMusicPlaySignWnd:OnBackToMainBtnClick()
	if self.m_MainWndAni then
		self.m_MainWndAni:Play('musicplaysignwnd_shown',0,0)
	end
end


function LuaMusicPlaySignWnd:OnEnterChooseBtnClick()
	if self.m_MainWndAni then
		self.m_MainWndAni:Play('musicplaysignwnd_shown_qiehuan',0,0)
	end
end


function LuaMusicPlaySignWnd:OnCloseButtonClick()
end


--@endregion UIEvent

