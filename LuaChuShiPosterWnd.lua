local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CItemMgr = import "L10.Game.CItemMgr"
local CTitleMgr = import "L10.Game.CTitleMgr"
local NativeTools = import "L10.Engine.NativeTools"
local EShareType = import "L10.UI.EShareType"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local ETickType = import "L10.Engine.ETickType"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Animator = import "UnityEngine.Animator"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local AlignType = import "CPlayerInfoMgr+AlignType"

LuaChuShiPosterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChuShiPosterWnd, "TuDiPortrait", "TuDiPortrait", CUITexture)
RegistChildComponent(LuaChuShiPosterWnd, "ShiFuPortrait", "ShiFuPortrait", CUITexture)
RegistChildComponent(LuaChuShiPosterWnd, "TuDiNameLabel", "TuDiNameLabel", UILabel)
RegistChildComponent(LuaChuShiPosterWnd, "ShiFuNameLabel", "ShiFuNameLabel", UILabel)
RegistChildComponent(LuaChuShiPosterWnd, "TuDiTitleLabel", "TuDiTitleLabel", UILabel)
RegistChildComponent(LuaChuShiPosterWnd, "ShiFuTitleLabel", "ShiFuTitleLabel", UILabel)
RegistChildComponent(LuaChuShiPosterWnd, "ChuShiTimeLabel", "ChuShiTimeLabel", UILabel)
RegistChildComponent(LuaChuShiPosterWnd, "BaiShiTimeLabel", "BaiShiTimeLabel", UILabel)
RegistChildComponent(LuaChuShiPosterWnd, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaChuShiPosterWnd, "FinishedHomeworkLabel", "FinishedHomeworkLabel", UILabel)
RegistChildComponent(LuaChuShiPosterWnd, "FinishedTaskNumLabel", "FinishedTaskNumLabel", UILabel)
RegistChildComponent(LuaChuShiPosterWnd, "ScoreNumLabel", "ScoreNumLabel", UILabel)
RegistChildComponent(LuaChuShiPosterWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaChuShiPosterWnd, "TimeLabel2", "TimeLabel2", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaChuShiPosterWnd, "m_Animator")

function LuaChuShiPosterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)


    --@endregion EventBind end
end

function LuaChuShiPosterWnd:Init()
	self.m_Animator = self.transform:GetComponent(typeof(Animator))
	self.m_Animator.enabled = true
	self.m_Animator:CrossFade(LuaShiTuMgr.m_IsChangingToChuShiPosterWnd and "baishiposterwnd_baizhuanchu" or "chushiposterwnd__shown",0.5,0,0)
	local item, place, pos = CItemMgr.Inst:GetItemFromPackage(LuaShiTuMgr.m_ChuShiPosterItemid)
	if item then
		if item.Item.ExtraVarData ~= nil and item.Item.ExtraVarData.Data ~= nil then
			local raw = item.Item.ExtraVarData.Data
			self:InitWithExtraData(raw)
		end
	else
		self:InitWithExtraData(LuaShiTuMgr.m_ChuShiPosterExtraData)
	end
end

function LuaChuShiPosterWnd:InitWithExtraData(raw)
	local list = g_MessagePack.unpack(raw)
	if #list ~= 11 then
		CUIManager.CloseUI(CLuaUIResources.ChuShiPosterWnd)
	end
	local shifuId, shifuName, shifuGender, shifuClass, shifuTitle, tudiTitle,baishiTime,chushiTime,keYe,riChang,shouZeng = 
	list[1], list[2],list[3], list[4], list[5], list[6], list[7], list[8], list[9], list[10], list[11]
	local titleData1, titleData2 = Title_Title.GetData(shifuTitle),Title_Title.GetData(tudiTitle)
	local wenDaoStageName = ShiTu_Setting.GetData().WenDaoStageName
	if titleData1 then
		self.ShiFuTitleLabel.text = titleData1.Name
		self.ShiFuTitleLabel.color = CTitleMgr.Inst:GetTitleColor(shifuTitle)
	end
	self.ShiFuTitleLabel.gameObject:SetActive(titleData1 ~= nil)
	if titleData2 then
		self.TuDiTitleLabel.text = titleData2.Name
		self.TuDiTitleLabel.color = CTitleMgr.Inst:GetTitleColor(tudiTitle)
	end
	self.TuDiTitleLabel.gameObject:SetActive(titleData2 ~= nil)
	self.ShiFuPortrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(shifuClass, shifuGender, -1), false)
	self.TuDiPortrait:LoadNPCPortrait(CClientMainPlayer.Inst and CClientMainPlayer.Inst.PortraitName, false)
	self.ShiFuNameLabel.text = shifuName
	self.TuDiNameLabel.text = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name
	local baishiDateTime = CServerTimeMgr.ConvertTimeStampToZone8Time(baishiTime)
	local chushiDateTime = CServerTimeMgr.ConvertTimeStampToZone8Time(chushiTime)
	if CommonDefs.IS_VN_CLIENT then
        self.BaiShiTimeLabel.text = ToStringWrap(baishiDateTime, LocalString.GetString("yyyy年MM月dd日 ")) .. LocalString.GetString("结为师徒")
        self.ChuShiTimeLabel.text = ToStringWrap(chushiDateTime, LocalString.GetString("yyyy年MM月dd日 ")) .. LocalString.GetString("出师")
        self.TimeLabel2.text = ToStringWrap(baishiDateTime, LocalString.GetString("yyyy年MM月dd日 ")) .. LocalString.GetString("结为师徒")
    else
		self.BaiShiTimeLabel.text = ToStringWrap(baishiDateTime, LocalString.GetString("于yyyy年MM月dd日结为师徒"))
		self.ChuShiTimeLabel.text = ToStringWrap(chushiDateTime, LocalString.GetString("于yyyy年MM月dd日出师"))
		self.TimeLabel2.text = ToStringWrap(baishiDateTime, LocalString.GetString("于yyyy年MM月dd日结为师徒"))
    end
	self.FinishedHomeworkLabel.text = SafeStringFormat3(LocalString.GetString("%s阶段"),wenDaoStageName[keYe])
	self.FinishedTaskNumLabel.text = SafeStringFormat3(LocalString.GetString("为师父完成%d次任务"),riChang)
	self.ScoreNumLabel.text = SafeStringFormat3(LocalString.GetString("获得%d受赠积分"),shouZeng)

	UIEventListener.Get(self.ShiFuPortrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		CPlayerInfoMgr.ShowPlayerPopupMenu(shifuId, EnumPlayerInfoContext.PlotDiscussion, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
	end)
end

function LuaChuShiPosterWnd:OnDisable()
	LuaShiTuMgr.m_IsChangingToChuShiPosterWnd = false
end
--@region UIEvent

function LuaChuShiPosterWnd:OnShareButtonClick()
	self.CloseButton.gameObject:SetActive(false)
	self.ShareButton.gameObject:SetActive(false)
	CTickMgr.Register(DelegateFactory.Action(function ()
		local filename = Utility.persistentDataPath .. "/screenshot.jpg"
		local data = CUICommonDef.DoCaptureScreen(filename, true, true, false)
		NativeTools.SaveImage(data)
		CTickMgr.Register(DelegateFactory.Action(function ()
			ShareBoxMgr.ImagePath = filename
			ShareBoxMgr.ShareType = EShareType.ShareChatPanelImage2
			CUIManager.ShowUI(CUIResources.ShareBox3)
			self.CloseButton.gameObject:SetActive(true)
			self.ShareButton.gameObject:SetActive(true)
		end), 1000, ETickType.Once)
	end), 200, ETickType.Once)
end

--@endregion UIEvent

