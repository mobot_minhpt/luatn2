local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaYuanXiaoDaLuanDouTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYuanXiaoDaLuanDouTopRightWnd, "ExpandButton", "ExpandButton", GameObject)
RegistChildComponent(LuaYuanXiaoDaLuanDouTopRightWnd, "LastTimeLabel", "LastTimeLabel", UILabel)
RegistChildComponent(LuaYuanXiaoDaLuanDouTopRightWnd, "LeftNode", "LeftNode", GameObject)
RegistChildComponent(LuaYuanXiaoDaLuanDouTopRightWnd, "RightNode", "RightNode", GameObject)

--@endregion RegistChildComponent end

function LuaYuanXiaoDaLuanDouTopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!

		UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)	    self:OnExpandButtonClick()	end)

    --@endregion EventBind end
end

function LuaYuanXiaoDaLuanDouTopRightWnd:Init()

end

--@region UIEvent

function LuaYuanXiaoDaLuanDouTopRightWnd:OnExpandButtonClick()
end


--@endregion UIEvent

