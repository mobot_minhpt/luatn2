local UIRoot=import "UIRoot"
local UICamera                      = import "UICamera"
local Vector3                       = import "UnityEngine.Vector3"
local Physics                       = import "UnityEngine.Physics"
local CUIFx                         = import "L10.UI.CUIFx"
local Object                        = import "System.Object"

LuaLeafPicWnd = class()      --LeafPicWnd

--------RegistChildComponent-------
RegistChildComponent(LuaLeafPicWnd,      "Pic_1",                    GameObject)
RegistChildComponent(LuaLeafPicWnd,      "Pic_2",                    GameObject)
RegistChildComponent(LuaLeafPicWnd,      "Pic_3",                    GameObject)
RegistChildComponent(LuaLeafPicWnd,      "Pic_4",                    GameObject)
RegistChildComponent(LuaLeafPicWnd,      "Pic_5",                    GameObject)
RegistChildComponent(LuaLeafPicWnd,      "Pic_6",                    GameObject)
RegistChildComponent(LuaLeafPicWnd,      "BG",                       GameObject)
RegistChildComponent(LuaLeafPicWnd,      "CompletePic",              GameObject)
RegistChildComponent(LuaLeafPicWnd,      "DisLabel",                 GameObject)
RegistChildComponent(LuaLeafPicWnd,      "CompletepicEffect",        CUIFx)

---------RegistClassMember-------
RegistClassMember(LuaLeafPicWnd,         "PicsTable")
RegistClassMember(LuaLeafPicWnd,         "PicStateTable")
RegistClassMember(LuaLeafPicWnd,         "IsComplete")
RegistClassMember(LuaLeafPicWnd,         "JigsawEffectTick")                 --特效计时器

RegistClassMember(LuaLeafPicWnd,         "CountDownTick")

LuaLeafPicWnd.taskId = 0

function LuaLeafPicWnd:Awake()
    -- Gac2Gas.RequestHalloweenCardOrder()
    self.CompletepicEffect.gameObject:SetActive(false)
    self.IsComplete =false
    self.CompletePic:SetActive(false)
    self.BG:SetActive(true)
    self.DisLabel:SetActive(true)

    self:InitPicsTable()
    self:InitPicStateTable()
end

function LuaLeafPicWnd:InitPicsTable()
    self.PicsTable = {self.Pic_1,self.Pic_2,self.Pic_3,self.Pic_4,self.Pic_5,self.Pic_6}
    for i,v in ipairs(self.PicsTable) do
        UIEventListener.Get(self.PicsTable[i]).onDrag = DelegateFactory.VectorDelegate(function(go,delta)
            self:OnPicDrag(go,delta)
        end)
        UIEventListener.Get(self.PicsTable[i]).onDragEnd = DelegateFactory.VoidDelegate(function(go)
            self:OnPicDragEnd(go,i)
        end)
    end
end

function LuaLeafPicWnd:InitPicStateTable()
    self.PicStateTable = {}
    for i=1,#self.PicsTable do
        self.PicStateTable[i] = 0
    end
end
function LuaLeafPicWnd:OnDisable()
    UnRegisterTick(self.JigsawEffectTick)
    UnRegisterTick(self.CountDownTick)
end

function LuaLeafPicWnd:OnPicDrag(go,delta)
    if self.IsComplete then return end
    -- go.transform.position = UICamera.mainCamera:ScreenToWorldPoint(Vector3(Input.mousePosition.x,Input.mousePosition.y,0))
    local scale = UIRoot.GetPixelSizeAdjustment(go.transform.parent.gameObject);
    local trans = go.transform
    local pos = trans.localPosition
    pos.x = pos.x + delta.x*scale
    pos.y = pos.y + delta.y*scale
    trans.localPosition = pos
end

function LuaLeafPicWnd:OnPicDragEnd(go,index)
    if self.IsComplete then return end
    local currentPos = UICamera.currentTouch.pos
    local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
    local hits = Physics.RaycastAll(ray, 99999,  UICamera.currentCamera.cullingMask)

    local transParent = self.transform:Find("Content/Pic/Origin_"..index)    
    local transScale = 0.8
    local HitNum = 0
    for i=0,hits.Length-1 do
        local collider = hits[i].collider.gameObject
        if (string.find(collider.name, "Col_", 1, true) ~= nil) then
            HitNum = tonumber(string.sub(collider.name, -1))
            if HitNum == index then
                transParent = collider.transform
                transScale = 1
            end
        end
    end
    self:RefreshGrid(go.transform,HitNum,transParent,index,transScale)
end

function LuaLeafPicWnd:RefreshGrid(pic,HitNum,transParent,index,transScale)
    if HitNum == index then       --放到了拼图板上且位置正确
        self.PicStateTable[index] = index
        self.DisLabel:SetActive(false)
        self:FixPos(pic,transParent,transScale)
    else
        self.PicStateTable[index] = 0
        self:FixPos(pic,transParent,transScale)
    end
    self:CheckComplete()
end

function LuaLeafPicWnd:CheckComplete()
    self.IsComplete = true
    for i=1,#self.PicsTable do
        if self.PicStateTable[i] == 0 then
            self.IsComplete = false
        end
    end
    if self.IsComplete then
        self:ShowComplete()
    end
end

function LuaLeafPicWnd:ShowComplete()
    self.CompletepicEffect.gameObject:SetActive(true)

    local empty = CreateFromClass(MakeGenericClass(List, Object))
    Gac2Gas.FinishClientTaskEvent(LuaLeafPicWnd.taskId,"LeafPic",MsgPackImpl.pack(empty))

    if self.JigsawEffectTick ~= nil then
        invoke(self.JigsawEffectTick)
    end

    self.JigsawEffectTick = RegisterTickOnce(function () 
        self.CompletepicEffect.gameObject:SetActive(false)
        self.BG:SetActive(false)
        self.CompletePic:SetActive(true)
    end, 1650)

    if self.CountDownTick ~= nil then
        invoke(self.CountDownTick)
    end
    
    self.CountDownTick = RegisterTickOnce(function()
        CUIManager.CloseUI(CLuaUIResources.LeafPicWnd)
    end, 10000)
end

function LuaLeafPicWnd:FixPos(pic,transParent,transScale)
    pic:SetParent(transParent)
    pic.localPosition = Vector3.zero
    pic.localRotation = Vector3.zero
    pic.localScale = Vector3(transScale,transScale,1)
end
