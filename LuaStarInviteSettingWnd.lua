local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local CUITexture = import "L10.UI.CUITexture"
local UIScrollView = import "UIScrollView"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local UIGrid = import "UIGrid"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"

LuaStarInviteSettingWnd=class()
RegistChildComponent(LuaStarInviteSettingWnd,"closeBtn", GameObject)
RegistChildComponent(LuaStarInviteSettingWnd,"backBtn", GameObject)
RegistChildComponent(LuaStarInviteSettingWnd,"saveBtn", GameObject)
RegistChildComponent(LuaStarInviteSettingWnd,"chatInput", UIInput)
RegistChildComponent(LuaStarInviteSettingWnd,"freBtn", QnAddSubAndInputButton)
RegistChildComponent(LuaStarInviteSettingWnd,"templateNode", GameObject)
RegistChildComponent(LuaStarInviteSettingWnd,"selfWinLabel", UILabel)
RegistChildComponent(LuaStarInviteSettingWnd,"selfWinGrid", UIGrid)
RegistChildComponent(LuaStarInviteSettingWnd,"otherWinLabel", UILabel)
RegistChildComponent(LuaStarInviteSettingWnd,"otherWinGrid", UIGrid)

RegistClassMember(LuaStarInviteSettingWnd, "defaultSelfWinText")
RegistClassMember(LuaStarInviteSettingWnd, "defaultOtherWinText")
RegistClassMember(LuaStarInviteSettingWnd, "selfWinHighLight")
RegistClassMember(LuaStarInviteSettingWnd, "otherWinHighLight")

RegistClassMember(LuaStarInviteSettingWnd, "id")
RegistClassMember(LuaStarInviteSettingWnd, "selfWinExpressionId")
RegistClassMember(LuaStarInviteSettingWnd, "otherWinExpressionId")
RegistClassMember(LuaStarInviteSettingWnd, "gossipValue")
RegistClassMember(LuaStarInviteSettingWnd, "gossipFre")

function LuaStarInviteSettingWnd:OnEnable()
	--g_ScriptEvent:AddListener("UpdateTaoQuanInfo", self, "UpdateInfo")
	LuaStarInviteMgr.CancelUpdate = true
end

function LuaStarInviteSettingWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("UpdateTaoQuanInfo", self, "UpdateInfo")
	LuaStarInviteMgr.CancelUpdate = false
end

function LuaStarInviteSettingWnd:LeaveWnd()
	CUIManager.CloseUI(CLuaUIResources.StarInviteSettingWnd)
	--Gac2Gas.RequestLeavePlay()
end

function LuaStarInviteSettingWnd:SubmitInfo()
	if self.id then
		local text = CWordFilterMgr.Inst:DoFilterOnSendPersonalSpace(self.chatInput.value,true)

		Gac2Gas.StarBiwuSetAudienceAction(self.id,text or '',self.gossipFre,self.selfWinExpressionId or 0,self.otherWinExpressionId or 0)
		self:LeaveWnd()
	end
end

function LuaStarInviteSettingWnd:Init()
	local onCloseClick = function(go)
		self:LeaveWnd()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
	CommonDefs.AddOnClickListener(self.backBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
	CommonDefs.AddOnClickListener(self.saveBtn,DelegateFactory.Action_GameObject(function(go)
		self:SubmitInfo()
	end),false)

	self.templateNode:SetActive(false)
	self:InitInfo()
end

function LuaStarInviteSettingWnd:InitExpressGrid(dataTable,dataType)
	local grid = self.selfWinGrid
	if dataType == 2 then
		grid = self.otherWinGrid
	end
	local scrollView = grid.transform:GetComponent(typeof(UIScrollView))
	Extensions.RemoveAllChildren(grid.transform)

	for i,v in ipairs(dataTable) do
		local id = tonumber(v)
		if id then
			local expressionData = Expression_Show.GetData(id)
			if expressionData then
				local node = NGUITools.AddChild(grid.gameObject,self.templateNode)
				node:SetActive(true)
				node:GetComponent(typeof(CUITexture)):LoadMaterial(expressionData.Icon)
				local highLight = node.transform:Find('Image').gameObject
				highLight:SetActive(false)
				local btn = node
				local name = expressionData.ExpName

				local onBtnClick = function(go)
					highLight:SetActive(true)
					if dataType == 2 then
						if self.otherWinHighLight then
							self.otherWinHighLight:SetActive(false)
						end
						self.otherWinHighLight = highLight
						self.otherWinExpressionId = id
					else
						if self.selfWinHighLight then
							self.selfWinHighLight:SetActive(false)
						end
						self.selfWinHighLight = highLight
						self.selfWinExpressionId = id
					end
				end
				CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(onBtnClick),false)

	--LuaStarInviteMgr.AudienceSettingInfo = {id = id, gossip = gossip, gossipFre = gossipFre, winSelfAction = winSelfAction, winEnemyAction = winEnemyAction}
				if LuaStarInviteMgr.AudienceSettingInfo then
					if dataType == 2 then
						if LuaStarInviteMgr.AudienceSettingInfo.winEnemyAction and LuaStarInviteMgr.AudienceSettingInfo.winEnemyAction == id then
								onBtnClick()
						end
					else
						if LuaStarInviteMgr.AudienceSettingInfo.winSelfAction and LuaStarInviteMgr.AudienceSettingInfo.winSelfAction == id then
								onBtnClick()
						end
					end
				end

			end
		end
	end
	grid:Reposition()
	scrollView:ResetPosition()
end

function LuaStarInviteSettingWnd:InitInfo()
	self.defaultSelfWinText = self.selfWinLabel.text
	self.defaultOtherWinText = self.otherWinLabel.text

	local settingData = StarBiWuShow_Setting.GetData()
	local minGossip = settingData.QinYouPopIntervalMin
	local maxGossip = settingData.QinYouPopIntervalMax
	local sepGossip = settingData.QinYouPopChange

	self.otherWinHighLight = nil
	self.selfWinHighLight = nil

	local winExpressTable = LuaStarInviteMgr.split(settingData.QinYouWinExpression,',')
	self:InitExpressGrid(winExpressTable,1)
	local otherExpressTable = LuaStarInviteMgr.split(settingData.QinYouLoseExpression,',')
	self:InitExpressGrid(otherExpressTable,2)

  self.freBtn.onValueChanged = DelegateFactory.Action_uint(function (value)
		self.gossipFre = value
  end)
	self.freBtn:SetMinMax(minGossip,maxGossip,sepGossip)
	if LuaStarInviteMgr.AudienceSettingInfo and LuaStarInviteMgr.AudienceSettingInfo.gossipFre then
		self.freBtn:SetValue(LuaStarInviteMgr.AudienceSettingInfo.gossipFre, true)
	else
		self.freBtn:SetValue(minGossip, true)
	end

	if LuaStarInviteMgr.AudienceSettingInfo and LuaStarInviteMgr.AudienceSettingInfo.id then
		self.id = LuaStarInviteMgr.AudienceSettingInfo.id
	end
	if LuaStarInviteMgr.AudienceSettingInfo and LuaStarInviteMgr.AudienceSettingInfo.gossip and LuaStarInviteMgr.AudienceSettingInfo.gossip ~= '' then
		self.chatInput.value = LuaStarInviteMgr.AudienceSettingInfo.gossip
	end
end

function LuaStarInviteSettingWnd:OnDestroy()
end

return LuaStarInviteSettingWnd
