local CScene=import "L10.Game.CScene"

LuaChildrenDay2020Mgr = {}

LuaChildrenDay2020Mgr.itemId = nil
LuaChildrenDay2020Mgr.score = 0
LuaChildrenDay2020Mgr.TaskInfoTable = {}
LuaChildrenDay2020Mgr.TaskRewardTable = {}

LuaChildrenDay2020Mgr.gameSignUp = nil
LuaChildrenDay2020Mgr.gameSignUpType = nil
LuaChildrenDay2020Mgr.gamePlayerInfo = {}

function LuaChildrenDay2020Mgr.InGamePlay()
  if not CScene.MainScene then
    return false
  end

  local gamePlayId = CScene.MainScene.GamePlayDesignId
  if gamePlayId == LiuYi2020_Setting.GetData().GameplayId then
    return true
  end
  return false
end

function LuaChildrenDay2020Mgr.OpenRankWnd()
  if CUIManager.IsLoaded(CLuaUIResources.ChildrenDayPlayFightInfo2020Wnd) then
  else
    CUIManager.ShowUI(CLuaUIResources.ChildrenDayPlayFightInfo2020Wnd)
  end
end
---
