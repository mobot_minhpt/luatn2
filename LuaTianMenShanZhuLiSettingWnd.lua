local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CSortButton = import "L10.UI.CSortButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local QnRadioBox = import "L10.UI.QnRadioBox"
local QnTableView = import "L10.UI.QnTableView"
local Profession = import "L10.Game.Profession"
local Double = import "System.Double"
local TianMenShanBattle_Setting = import "L10.Game.TianMenShanBattle_Setting"

LuaTianMenShanZhuLiSettingWnd = class()
RegistClassMember(LuaTianMenShanZhuLiSettingWnd, "tableView")
RegistClassMember(LuaTianMenShanZhuLiSettingWnd, "onlineNumLabel")
RegistClassMember(LuaTianMenShanZhuLiSettingWnd, "mainForceNumLabel")
RegistClassMember(LuaTianMenShanZhuLiSettingWnd, "headerRadioBox")
RegistClassMember(LuaTianMenShanZhuLiSettingWnd, "cancelAllBtn")
RegistClassMember(LuaTianMenShanZhuLiSettingWnd, "leagueButton")
RegistClassMember(LuaTianMenShanZhuLiSettingWnd, "refreshBtn")
RegistClassMember(LuaTianMenShanZhuLiSettingWnd, "tipButton")

RegistClassMember(LuaTianMenShanZhuLiSettingWnd, "m_MainPlayerGuildInfo") --玩家信息
RegistClassMember(LuaTianMenShanZhuLiSettingWnd, "m_GuildMemberInfoList") --其他成员信息
RegistClassMember(LuaTianMenShanZhuLiSettingWnd, "m_SortFlags")

function LuaTianMenShanZhuLiSettingWnd:Awake()
    self.m_SortFlags = {-1, -1, -1, -1, -1, -1, -1, -1}
    self.m_GuildMemberInfoList = {}
    self.m_MainPlayerGuildInfo = nil

    self.tableView = self.transform:Find("Anchor/TableView"):GetComponent(typeof(QnTableView))
    self.onlineNumLabel = self.transform:Find("Anchor/Docker/OnlineNumLabel"):GetComponent(typeof(UILabel))
    self.mainForceNumLabel = self.transform:Find("Anchor/Docker/MainForceNumLabel"):GetComponent(typeof(UILabel))
    self.headerRadioBox = self.transform:Find("Anchor/TableView/Header"):GetComponent(typeof(QnRadioBox))
    self.cancelAllBtn = self.transform:Find("Anchor/Docker/CancelAllBtn").gameObject
    self.leagueButton = self.transform:Find("Anchor/Docker/LeagueButton").gameObject
    self.refreshBtn = self.transform:Find("Anchor/Docker/RefreshBtn").gameObject
    self.tipButton = self.transform:Find("Anchor/Docker/TipButton").gameObject


    self.headerRadioBox.OnSelect =
        DelegateFactory.Action_QnButton_int(
        function(btn, index)
            self:OnRadioBoxSelect(btn, index)
        end
    )

    UIEventListener.Get(self.cancelAllBtn).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnClickCancelAllButtion(go)
        end
    )

    UIEventListener.Get(self.leagueButton).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            CUIManager.ShowUI(CLuaUIResources.GuildLeagueWeeklyInfoWnd)
        end
    )

    UIEventListener.Get(self.refreshBtn).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnClickRefreshButton(go)
        end
    )

    UIEventListener.Get(self.tipButton).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnClickTipButton(go)
        end
    )
end

function LuaTianMenShanZhuLiSettingWnd:OnRadioBoxSelect(btn, index)
    local sortButton = TypeAs(btn, typeof(CSortButton))

    for i, v in ipairs(self.m_SortFlags) do
        if i == index + 1 then
            self.m_SortFlags[i] = -self.m_SortFlags[i]
        else
            self.m_SortFlags[i] = -1
        end
    end

    sortButton:SetSortTipStatus(self.m_SortFlags[index + 1] < 0)
    self:Sort(self.m_GuildMemberInfoList, index)
    self.tableView:ReloadData(true, false)
end

function LuaTianMenShanZhuLiSettingWnd:OnClickCancelAllButtion(go)
    local message = g_MessageMgr:FormatMessage("TianMenShanMainForce_CancelAll", self.recvName)
    MessageWndManager.ShowOKCancelMessage(
        message,
        DelegateFactory.Action(
            function()
                Gac2Gas.RequestOperationInGuild(
                    CClientMainPlayer.Inst.BasicProp.GuildId,
                    "ClearTianMenShanBattleZhuLi",
                    "",
                    ""
                )
            end
        ),
        nil,
        nil,
        nil,
        false
    )
end

function LuaTianMenShanZhuLiSettingWnd:OnClickRefreshButton(go)
    CLuaGuildMgr.m_TianMenShanInfo = nil
    Gac2Gas.RequestGetGuildInfo("TianMenShanBattleZhuLiInfo")
end

function LuaTianMenShanZhuLiSettingWnd:OnClickTipButton(go)
    g_MessageMgr:ShowMessage("TianMenShanMainForce_Tips")
end

--排序标签
function LuaTianMenShanZhuLiSettingWnd:Sort(tab, sortIndex)
    local flag = self.m_SortFlags[sortIndex + 1]
    local SortTag = {"Class", "Level", "XiuWei", "XiuLian", "EquipScore"}

    local defaultSort = function(a, b)
        if a.IsInCopyScene == 1 and b.IsInCopyScene ~= 1 then
            return true
        elseif a.IsInCopyScene ~= 1 and b.IsInCopyScene == 1 then
            return false
        elseif a.IsZhuLi > 0 and b.IsZhuLi == 0 then
            return true
        elseif a.IsZhuLi == 0 and b.IsZhuLi > 0 then
            return false
        else
            return a.PlayerId < b.PlayerId
        end
    end

    local sortFunction = defaultSort

    if sortIndex >= 0 and sortIndex <= 4 then -- 前五个字段排序
        sortFunction = function(a, b)
            local ret = flag
            if a[SortTag[sortIndex + 1]] < b[SortTag[sortIndex + 1]] then
                ret = flag
            elseif a[SortTag[sortIndex + 1]] > b[SortTag[sortIndex + 1]] then
                ret = -flag
            else
                return defaultSort(a, b)
            end
            return ret < 0
        end
    elseif sortIndex == 5 then --联赛评价
        sortFunction = function(a, b)
            local ret = flag
            local sum1 = a.CommentLevelR1 + a.CommentLevelR2 + a.CommentLevelR3
            local sum2 = b.CommentLevelR1 + b.CommentLevelR2 + b.CommentLevelR3
            if sum1 < sum2 then
                ret = flag
            elseif sum1 > sum2 then
                ret = -flag
            else
                return defaultSort(a, b)
            end
            return ret < 0
        end
    elseif sortIndex == 6 then --是否入场
        sortFunction = function(a, b)
            local ret = flag
            local va = a.IsInCopyScene
            local vb = b.IsInCopyScene
            if va == 1 and vb ~= 1 then
                ret = -flag
            elseif va ~= 1 and vb == 1 then
                ret = flag
            else
                return defaultSort(a, b)
            end
            return ret < 0
        end
    elseif sortIndex == 7 then --主力 / default
        sortFunction = function(a, b)
            local ret = flag
            local va = a.IsZhuLi
            local vb = b.IsZhuLi
            if va < vb then
                ret = -flag
            elseif va > vb then
                ret = flag
            else
                return defaultSort(a, b)
            end
            return ret < 0
        end
    end

    table.sort(tab, sortFunction)
end

function LuaTianMenShanZhuLiSettingWnd:Init()
    self.tableView.m_DataSource =
        DefaultTableViewDataSource.CreateByCount(
        1,
        function(item, row)
            self:InitItem(item, row)
        end
    )

    CLuaGuildMgr.m_TianMenShanInfo = nil
    Gac2Gas.RequestGetGuildInfo("TianMenShanBattleZhuLiInfo")
end

-- 创建单条数据(CreateByCount的回调)
function LuaTianMenShanZhuLiSettingWnd:InitItem(item, row)
    local info
    if row == 0 then
        info = self.m_MainPlayerGuildInfo
    else
        info = self.m_GuildMemberInfoList[row]
    end
    --背景黑白间隔
    if row % 2 == 0 then
        item:SetBackgroundTexture("common_textbg_02_dark")
    else
        item:SetBackgroundTexture("common_textbg_02_light")
    end

    local itransform = item.transform
    local clsSprite = itransform:Find("ClassSprite"):GetComponent(typeof(UISprite))
    local nameLabel = itransform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local levelLabel = itransform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local xiuweiLabel = itransform:Find("XiuweiLabel"):GetComponent(typeof(UILabel))
    local xiulianLabel = itransform:Find("XiulianLabel"):GetComponent(typeof(UILabel))
    local equipScoreLabel = itransform:Find("EquipScoreLabel"):GetComponent(typeof(UILabel))
    local inGameLabel = itransform:Find("InGameLabel"):GetComponent(typeof(UILabel))
    local tickSprite = itransform:Find("TickSprite"):GetComponent(typeof(UISprite))

    clsSprite.spriteName = Profession.GetIconByNumber(info.Class)
    nameLabel.text = info.Name

    CUICommonDef.SetActive(clsSprite.gameObject, info.IsOnline > 0, true)

    local levelText
    if info.XianFanStatus > 0 then
        levelText = SafeStringFormat3("[c][ff7900]Lv.%d[-][/c]", info.Level)
    else
        levelText = SafeStringFormat3("Lv.%d", info.Level)
    end
    levelLabel.text = levelText

    --修为一位小数
    xiuweiLabel.text = NumberComplexToString(NumberTruncate(info.XiuWei, 1), typeof(Double), "F1")

    xiulianLabel.text = tostring(info.XiuLian)

    equipScoreLabel.text = tostring(info.EquipScore)

    local league = itransform:Find("GuildLeague")
    local CommentLevelName = {"CommentLevelR1", "CommentLevelR2", "CommentLevelR3"}
    for i = 1, 3 do
        local curLabel = league:GetChild(i - 1):GetComponent(typeof(UILabel))
        CLuaGuildMgr.SetLeagueCommentLevel(curLabel, info[CommentLevelName[i]])
    end

    self:SetItemMainForce(itransform, row, info.IsZhuLi > 0)

    if info.IsInCopyScene == 1 then
        inGameLabel.text = LocalString.GetString("是")
        inGameLabel.color = Color(0.066, 0.17, 0.3)
    elseif info.IsInCopyScene == 0 then
        inGameLabel.text = LocalString.GetString("否")
        inGameLabel.color = Color.red
    else
        inGameLabel.text = "_"
        inGameLabel.color = Color(0.066, 0.17, 0.3)
    end

    UIEventListener.Get(tickSprite.transform:GetChild(0).gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            if tickSprite.enabled then
                Gac2Gas.RequestOperationInGuild(
                    CClientMainPlayer.Inst.BasicProp.GuildId,
                    "SetTianMenShanBattleZhuLi",
                    tostring(info.PlayerId),
                    "0"
                )
            else
                Gac2Gas.RequestOperationInGuild(
                    CClientMainPlayer.Inst.BasicProp.GuildId,
                    "SetTianMenShanBattleZhuLi",
                    tostring(info.PlayerId),
                    "1"
                )
            end
        end
    )
end

function LuaTianMenShanZhuLiSettingWnd:OnEnable()
    g_ScriptEvent:AddListener("SendGuildInfo_TianMenShanBattleZhuLiInfo", self, "OnSendGuildInfo_TianMenShanBattleZhuLiInfo")
    g_ScriptEvent:AddListener("RequestOperationInGuildSucceed", self, "OnRequestOperationInGuildSucceed")
end

function LuaTianMenShanZhuLiSettingWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendGuildInfo_TianMenShanBattleZhuLiInfo", self, "OnSendGuildInfo_TianMenShanBattleZhuLiInfo")
    g_ScriptEvent:RemoveListener("RequestOperationInGuildSucceed", self, "OnRequestOperationInGuildSucceed")
end

function LuaTianMenShanZhuLiSettingWnd:OnDestroy()
end

--更新列表信息
function LuaTianMenShanZhuLiSettingWnd:OnSendGuildInfo_TianMenShanBattleZhuLiInfo()
    self.m_MainPlayerGuildInfo = {}
    self.m_GuildMemberInfoList = {}
    local playerID = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id or 0

    if CLuaGuildMgr.m_TianMenShanInfo then
        for k, v in pairs(CLuaGuildMgr.m_TianMenShanInfo) do
            if k == playerID then
                self.m_MainPlayerGuildInfo = v
            else
                if v.IsZhuLi > 0 or v.IsOnline > 0 then --是主力或者在线
                    table.insert(self.m_GuildMemberInfoList, v)
                end
            end
        end
    end

    self.headerRadioBox:ChangeTo(-1, true)
    self:Sort(self.m_GuildMemberInfoList, 7)

    self.tableView.m_DataSource.count = #self.m_GuildMemberInfoList + (self.m_MainPlayerGuildInfo and 1 or 0)
    self.tableView:ReloadData(true, false)

    self:UpdateOnlineNumLabel()
    self:UpdateMainForceNum()
end

--操作成功后的更新
function LuaTianMenShanZhuLiSettingWnd:OnRequestOperationInGuildSucceed(args)
    local requestType, paramStr = args[0], args[1]

    if requestType == "SetTianMenShanBattleZhuLi" then --修改单条
        local splits = g_LuaUtil:StrSplit(paramStr, ",")
        if #splits == 2 then
            local playerId = tonumber(splits[1])
            local isZhuLi = tonumber(splits[2])

            if CLuaGuildMgr.m_TianMenShanInfo then
                local val = CLuaGuildMgr.m_TianMenShanInfo[playerId]
                if val then
                    val.IsZhuLi = isZhuLi
                end
            end

            local parent = self.tableView.m_Grid.transform
            if self.m_MainPlayerGuildInfo.PlayerId == playerId then
                self:SetItemMainForce(parent:GetChild(0), 0, isZhuLi > 0)
            else
                for i, v in ipairs(self.m_GuildMemberInfoList) do
                    if v.PlayerId == playerId then
                        self:SetItemMainForce(parent:GetChild(i), i, isZhuLi > 0)
                        break
                    end
                end
            end
        end
    elseif requestType == "ClearTianMenShanBattleZhuLi" then --全部撤销
        if CLuaGuildMgr.m_TianMenShanInfo then
            for k, v in pairs(CLuaGuildMgr.m_TianMenShanInfo) do
                v.IsZhuLi = 0
            end
        end

        local parent = self.tableView.m_Grid.transform
        for i = 1, parent.childCount do
            self:SetItemMainForce(parent:GetChild(i - 1), i - 1, false)
        end
    end
    self:UpdateMainForceNum()
end

function LuaTianMenShanZhuLiSettingWnd:UpdateOnlineNumLabel()
    local num = 0
    for i, v in ipairs(self.m_GuildMemberInfoList) do
        if v.IsOnline > 0 then
            num = num + 1
        end
    end
    local me = (self.m_MainPlayerGuildInfo and 1 or 0)
    num = num + me

    self.onlineNumLabel.text =
        SafeStringFormat3(LocalString.GetString("在线人数：[00ff00]%d[-]/%d"), num, #self.m_GuildMemberInfoList + me)
end

function LuaTianMenShanZhuLiSettingWnd:UpdateMainForceNum()
    local num = 0

    for i, v in ipairs(self.m_GuildMemberInfoList) do
        if v.IsZhuLi > 0 then
            num = num + 1
        end
    end

    if self.m_MainPlayerGuildInfo then
        if self.m_MainPlayerGuildInfo.IsZhuLi > 0 then
            num = num + 1
        end
    end
    local topNum = TianMenShanBattle_Setting.GetData().MaxZhuLiPlayerNum
    self.mainForceNumLabel.text = SafeStringFormat3(LocalString.GetString("勇士人数：[00ff00]%d[-]/%d"), num, topNum)
end

function LuaTianMenShanZhuLiSettingWnd:SetItemMainForce(itransform, row, isZhuLi)
    itransform:Find("TickSprite"):GetComponent(typeof(UISprite)).enabled = isZhuLi
end

return LuaTianMenShanZhuLiSettingWnd
