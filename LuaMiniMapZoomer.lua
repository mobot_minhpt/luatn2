local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UIDrawCall = import "UIDrawCall"
local UIScrollView = import "UIScrollView"
local Vector2 = import "UnityEngine.Vector2"
local LuaUtils = import "LuaUtils"
local CScene = import "L10.Game.CScene"
local BoxCollider = import "UnityEngine.BoxCollider"
local TouchPhase = import "UnityEngine.TouchPhase"
local Vector3 = import "UnityEngine.Vector3"
local LuaTweenUtils = import "LuaTweenUtils"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Input = import "UnityEngine.Input"

LuaMiniMapZoomer = class()

--@region RegistChildComponent: Dont Modify Manually!

--@endregion RegistChildComponent end

RegistClassMember(LuaMiniMapZoomer, "Panel")
RegistClassMember(LuaMiniMapZoomer, "ScrollView")
RegistClassMember(LuaMiniMapZoomer, "Map")
RegistClassMember(LuaMiniMapZoomer, "ZoomBtn")
RegistClassMember(LuaMiniMapZoomer, "suoxiao")
RegistClassMember(LuaMiniMapZoomer, "fangda")
RegistClassMember(LuaMiniMapZoomer, "ZoomMark")
RegistClassMember(LuaMiniMapZoomer, "IsZoomed")


function LuaMiniMapZoomer:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaMiniMapZoomer:InitComponent()
    self.Panel = self.transform.parent:GetComponent(typeof(UIPanel))
    self.ScrollView = self.transform.parent:GetComponent(typeof(UIScrollView))
    self.Map = self.transform.parent:Find("Map").gameObject
    self.ZoomBtn = self.transform.parent.parent:Find("TablePanel/ZoomBtn").gameObject

    self.suoxiao = self.transform.parent.parent:Find("TablePanel/ZoomBtn/suoxiao").gameObject
    self.fangda = self.transform.parent.parent:Find("TablePanel/ZoomBtn/fangda").gameObject

    self.ZoomMark = self.transform.parent.parent:Find("TablePanel/ZoomMark").gameObject

    UIEventListener.Get(self.ZoomBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:DoZoom(not self.m_IsZoomed)
	end)

    self.ScrollView.onDragStarted = LuaUtils.OnDragNotification(function()
		self.ZoomMark:SetActive(true)
    end)
    self.ScrollView.onDragFinished = LuaUtils.OnDragNotification(function()
		self.ZoomMark:SetActive(false)
    end)

    self.ZoomBtn:SetActive(false)
    -- 判断是否可以缩放
    if CScene.MainScene then
        local id = CScene.MainScene.SceneTemplateId
        local mapData = PublicMap_PublicMap.GetData(id)

        if mapData and mapData.CanZoom then
            self.ZoomBtn:SetActive(true)
        end
    end
end

function LuaMiniMapZoomer:Update()
	-- 玩家拖拽时的情况，移动端和pc端
    if self.ZoomMark.activeSelf then
        if CommonDefs.IsInMobileDevice() then
            for i = 0, Input.touchCount-1 do
                local touch = Input.GetTouch(i)
                if touch.phase ~= TouchPhase.Ended and touch.phase ~= TouchPhase.Canceled then
                    if touch.phase ~= TouchPhase.Stationary then
                        local pos = touch.position
                        self.m_LastDragPos = Vector3(pos.x, pos.y, 0)
                        self.ZoomMark.position = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_LastDragPos)
                    end
                end
            end
        else
            if Input.GetMouseButton(0) then
                self.ZoomMark.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
                self.m_LastDragPos = Input.mousePosition
            end
        end
    end
end

function LuaMiniMapZoomer:DoZoom(zoom)
    self.Panel.clipOffset = Vector2(0,0)

    self.suoxiao:SetActive(zoom)
    self.fangda:SetActive(not zoom)

    -- 还需要改变按钮样式
    if zoom then
        self.Map.transform.localScale = Vector3(2,2,2)
        self.Panel.clipping = UIDrawCall.Clipping.SoftClip
    else
        self.ZoomMark:SetActive(false)
        self.Map.transform.localScale = Vector3(1,1,1)
        self.Panel.clipping = UIDrawCall.Clipping.None
    end

    self.ScrollView:ResetPosition()
    self.ScrollView.enabled = zoom
    
    self.Map.transform.localPosition = Vector3(0,0,0)
    self.Panel.transform.localPosition = Vector3(-15,-10,0)
    self.m_IsZoomed = zoom
end

function LuaMiniMapZoomer:OnEnable()
    self:InitComponent()
    self:DoZoom(false)
end

function LuaMiniMapZoomer:OnDisable()
    self:DoZoom(false)
    self.ZoomBtn:SetActive(false)
end

--@region UIEvent

--@endregion UIEvent

