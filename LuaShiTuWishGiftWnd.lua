local CButton = import "L10.UI.CButton"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local UIInput = import "UIInput"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CQnSymbolParser = import "CQnSymbolParser"
local CChatInputLink = import "CChatInputLink"
local Constants = import "L10.Game.Constants"

LuaShiTuWishGiftWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShiTuWishGiftWnd, "Item", "Item", GameObject)
RegistChildComponent(LuaShiTuWishGiftWnd, "AddItemBtn", "AddItemBtn", GameObject)
RegistChildComponent(LuaShiTuWishGiftWnd, "NumLabel", "NumLabel", UILabel)
RegistChildComponent(LuaShiTuWishGiftWnd, "IconTexture", "IconTexture", CUITexture)
RegistChildComponent(LuaShiTuWishGiftWnd, "StatusText", "StatusText", UIInput)
RegistChildComponent(LuaShiTuWishGiftWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaShiTuWishGiftWnd, "Button", "Button", CButton)
RegistChildComponent(LuaShiTuWishGiftWnd, "TitleLabel2", "TitleLabel2", UILabel)
RegistChildComponent(LuaShiTuWishGiftWnd, "TitleLabel", "TitleLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaShiTuWishGiftWnd,"m_AddGift")

function LuaShiTuWishGiftWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnItemClick()
	end)


	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)
    --@endregion EventBind end
end

function LuaShiTuWishGiftWnd:Init()
	local title = (LuaShiTuMgr.m_WishGifType == 0) and (LuaShiTuMgr.s_SendGiftToShiFu and LocalString.GetString("师父心愿") or LocalString.GetString("徒弟心愿")) or LocalString.GetString("我的心愿")
	self.TitleLabel.text = title
	self.TitleLabel2.text = title
	self.Button.Text = (LuaShiTuMgr.m_WishGifType ~= 0 ) and LocalString.GetString("许愿") or LocalString.GetString("前往赠礼")
	local itemId = (LuaShiTuMgr.m_WishGifType == 0) and LuaShiTuMgr.m_PrepareGiftRet.targetWinshItemId or LuaShiTuMgr.m_PrepareGiftRet.myWinshItemId
	local num = (LuaShiTuMgr.m_WishGifType == 0) and LuaShiTuMgr.m_PrepareGiftRet.targetWishItemCount or LuaShiTuMgr.m_PrepareGiftRet.myWishItemCount
	local content = (LuaShiTuMgr.m_WishGifType == 0) and LuaShiTuMgr.m_PrepareGiftRet.targetWishContent or LuaShiTuMgr.m_PrepareGiftRet.myWishContent
	local itemData = Item_Item.GetData(itemId)
	self.AddItemBtn.gameObject:SetActive(itemData == nil)
	if itemData then
		self.IconTexture:LoadMaterial(itemData.Icon)
		self.m_AddGift = {itemId = itemId, num = num}
	end
	self.NumLabel.text = num
	self.NumLabel.gameObject:SetActive(num > 1)
	if String.IsNullOrEmpty(content) then
		local randomIndex = math.random(ShiTu_WishList.GetDataCount())
		local data = ShiTu_WishList.GetData(randomIndex)
		content = data.Content
	end
	self.StatusText.value = content
end

function LuaShiTuWishGiftWnd:OnEnable()
	g_ScriptEvent:AddListener("OnShopMallItemSelectWndSelectItem", self, "OnSelectShopMallItemItem")
	g_ScriptEvent:AddListener("OnSetShiTuGiftWishSucc", self, "OnSetShiTuGiftWishSucc")
end

function LuaShiTuWishGiftWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnShopMallItemSelectWndSelectItem", self, "OnSelectShopMallItemItem")
	g_ScriptEvent:RemoveListener("OnSetShiTuGiftWishSucc", self, "OnSetShiTuGiftWishSucc")
	CLuaShopMallMgr.m_ShopMallSelectItem = nil
end

function LuaShiTuWishGiftWnd:OnSetShiTuGiftWishSucc(isShifu, otherPlayerId, winshItemId, num, content)
	local itemId = winshItemId
	local itemData = Item_Item.GetData(itemId)
	self.IconTexture:LoadMaterial(itemData.Icon)
	self.NumLabel.text = num
	self.StatusText.value = content
end

function LuaShiTuWishGiftWnd:OnSelectShopMallItemItem()
	local itemData = Item_Item.GetData(CLuaShopMallMgr.m_ShopMallSelectItem.itemId)
	local num = CLuaShopMallMgr.m_ShopMallSelectItem.num
	if LuaShiTuMgr.m_SelectShopItemType == 2 then
		self.m_AddGift = CLuaShopMallMgr.m_ShopMallSelectItem
		self.IconTexture:LoadMaterial(itemData.Icon)
		self.NumLabel.gameObject:SetActive(num > 1)
		self.NumLabel.text = num
		self.AddItemBtn.gameObject:SetActive(false)
	end
end

--@region UIEvent

function LuaShiTuWishGiftWnd:OnItemClick()
	local itemId = (LuaShiTuMgr.m_WishGifType == 0) and LuaShiTuMgr.m_PrepareGiftRet.targetWinshItemId or LuaShiTuMgr.m_PrepareGiftRet.myWinshItemId
	if itemId ~= 0 and LuaShiTuMgr.m_WishGifType == 0 then
		CItemInfoMgr.ShowLinkItemTemplateInfo(itemId)
	else
		LuaShiTuMgr.m_SelectShopItemType = 2
		CUIManager.ShowUI(CLuaUIResources.ShopMallItemSelectWnd)
	end
end

function LuaShiTuWishGiftWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("ShiTuWishGiftWnd_ReadMe")
end

function LuaShiTuWishGiftWnd:OnButtonClick()
	if LuaShiTuMgr.m_WishGifType == 0 then
		LuaShiTuMgr.m_SelectShopItemType = 0
		CUIManager.ShowUI(CLuaUIResources.ShopMallItemSelectWnd)	
	else
		if not self.m_AddGift then
			g_MessageMgr:ShowMessage("ShiTuWishGiftWnd_NotAddGift")
			return
		end
		local text = self:GetSendMsg()
		if text == nil then
			return
		end
		Gac2Gas.RequestSetShiTuGiftWish(not LuaShiTuMgr.s_SendGiftToShiFu, text, self.m_AddGift.itemId, self.m_AddGift.num)
	end
	CUIManager.CloseUI(CLuaUIResources.ShiTuWishGiftWnd)
end

--@endregion UIEvent

function LuaShiTuWishGiftWnd:GetSendMsg()
	local input = self.StatusText
	local sendInfo = input.value
	if System.String.IsNullOrEmpty(sendInfo) then
        g_MessageMgr:ShowMessage("ENTER_TEXT_TIP")
        return nil
    end
	input.value = CQnSymbolParser.FilterExceededEmoticons(StringTrim(sendInfo), Constants.MaxEmotionNum)
	local msg = input.value
	local sendText = CChatInputLink.Encapsulate(msg, self.m_ExistingLinks)
	sendText = CWordFilterMgr.Inst:DoFilterOnSendPersonalSpace(sendText, true)
	return sendText
end
