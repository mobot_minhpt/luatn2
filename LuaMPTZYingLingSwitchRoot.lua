local QnButton = import "L10.UI.QnButton"
local CommonDefs = import "L10.Game.CommonDefs"

LuaMPTZYingLingSwitchRoot = class()

RegistChildComponent(LuaMPTZYingLingSwitchRoot,"m_Root","Root", GameObject)
RegistChildComponent(LuaMPTZYingLingSwitchRoot,"m_Bg","Bg", GameObject)
RegistChildComponent(LuaMPTZYingLingSwitchRoot,"m_Table","Table", GameObject)

RegistClassMember(LuaMPTZYingLingSwitchRoot,"m_IsInit")
RegistClassMember(LuaMPTZYingLingSwitchRoot,"m_CurStateFlagIcons")
RegistClassMember(LuaMPTZYingLingSwitchRoot,"m_YingLingSwitchStateTable")
RegistClassMember(LuaMPTZYingLingSwitchRoot,"m_SwitchStateHighlightSpriteNameTable")
RegistClassMember(LuaMPTZYingLingSwitchRoot,"m_SwitchStateNormalSpriteNameTable")
RegistClassMember(LuaMPTZYingLingSwitchRoot,"m_IsSwitchingYingLingState")
RegistClassMember(LuaMPTZYingLingSwitchRoot,"m_YingLingSwitchStateBtns")
RegistClassMember(LuaMPTZYingLingSwitchRoot,"m_CurSwitchYingLingState")


function LuaMPTZYingLingSwitchRoot:Awake()

    local mainPlayer = CClientMainPlayer.Inst
    if (mainPlayer == nil) or (not mainPlayer.IsYingLing) or (not LuaPVPMgr.IsRobotOpen()) then
        self.gameObject:SetActive(false)
        return
    end

    self.m_Bg:SetActive(false)
    self.m_Table:SetActive(false)
    self.m_YingLingSwitchStateTable = {1, 2, 3}
    self.m_YingLingSwitchStateBtns = {}
    table.insert(self.m_YingLingSwitchStateBtns,
            self.m_Root.transform:Find("CurSwitch"):GetComponent(typeof(QnButton)))
    table.insert(self.m_YingLingSwitchStateBtns,
            self.m_Root.transform:Find("Table/SwitchBtn1"):GetComponent(typeof(QnButton)))
    table.insert(self.m_YingLingSwitchStateBtns,
            self.m_Root.transform:Find("Table/SwitchBtn2"):GetComponent(typeof(QnButton)))
    self.m_CurStateFlagIcons = {}
    for i = 1, 3 do
        table.insert(self.m_CurStateFlagIcons, self.m_YingLingSwitchStateBtns[i].transform:Find("CurState").gameObject)
    end
    self.m_SwitchStateHighlightSpriteNameTable = {"skillwnd_yinglingswitchroot_yang_highlight","skillwnd_yinglingswitchroot_yin_highlight","skillwnd_yinglingswitchroot_ying_highlight"}
    self.m_SwitchStateNormalSpriteNameTable = {"skillwnd_yinglingswitchroot_yang_normal","skillwnd_yinglingswitchroot_yin_normal","skillwnd_yinglingswitchroot_ying_normal"}
    self:UpdateStateFlagIcons()
    self.m_IsInit = true

    local curYingLingState4MPTZ = LuaPVPMgr.GetCurrentYingLingStateBySkillAI()
    if curYingLingState4MPTZ == EnumYingLingState.eDefault then
        self.m_CurSwitchYingLingState = LuaSkillMgr.GetDefaultYingLingState()
        LuaPVPMgr.m_CurrentYingLingState = self.m_CurSwitchYingLingState
    else
        self.m_CurSwitchYingLingState = curYingLingState4MPTZ
        LuaPVPMgr.m_CurrentYingLingState = self.m_CurSwitchYingLingState
    end
end

function LuaMPTZYingLingSwitchRoot:Start()
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end

    self.m_Root:SetActive(false)
    if LuaSkillMgr.CheckMainPlayerYingLingBianShenSkill() then
        self:EnableYingLingBianShenSkill()
    end
end

function LuaMPTZYingLingSwitchRoot:UpdateStateFlagIcons()
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then return end

    local curYingLingState4MPTZ = LuaPVPMgr.GetCurrentYingLingStateBySkillAI()
    if curYingLingState4MPTZ == EnumYingLingState.eDefault then
        curYingLingState4MPTZ = mainPlayer.CurYingLingState
    end

    for i = 1, #self.m_YingLingSwitchStateTable do
        self.m_CurStateFlagIcons[i].gameObject:SetActive(self.m_YingLingSwitchStateTable[i] == EnumToInt(curYingLingState4MPTZ))
    end
end

function LuaMPTZYingLingSwitchRoot:HideOrShowSwitchRootTable()
    self.m_IsSwitchingYingLingState = not self.m_Table.gameObject.activeSelf
    self.m_Table:SetActive(self.m_IsSwitchingYingLingState)
    self.m_Bg:SetActive(self.m_IsSwitchingYingLingState)
end

function LuaMPTZYingLingSwitchRoot:EnableYingLingBianShenSkill()
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then return end
    if not self.m_IsInit then
        return
    end

    self.m_Root:SetActive(true)

    local curYingLingState4MPTZ = EnumToInt(mainPlayer.CurYingLingState)
    local persistPlayData = mainPlayer.PlayProp.PersistPlayData
    local data = CommonDefs.DictGetValue(persistPlayData, typeof(UInt32), LuaPVPMgr.eYingLingRobotDefaultSkill)
    if data then
        local datastring = data.StringData
        if datastring ~= nil and datastring ~= "" and datastring ~= "0" then
            curYingLingState4MPTZ = tonumber(datastring)
        end
    end
    
    self:SwitchYingLingState(EnumToInt(curYingLingState4MPTZ))
    --self:SwitchYingLingState(EnumToInt(mainPlayer.CurYingLingState))

    self.m_YingLingSwitchStateBtns[1].OnClick = DelegateFactory.Action_QnButton(function (button)
        self:HideOrShowSwitchRootTable()
    end)
    for i = 2,3 do
        local index = i
        self.m_YingLingSwitchStateBtns[i].OnClick = DelegateFactory.Action_QnButton(function (button)
            self:HideOrShowSwitchRootTable()
            self:SwitchYingLingState(self.m_YingLingSwitchStateTable[index ])
        end)
    end
end

function LuaMPTZYingLingSwitchRoot:SwitchYingLingState(state)
    if not self.m_IsInit then
        return
    end

    local a = state + 1
    local b = math.fmod(a + 1,3)
    if a == 4 then a = 1 end
    if b == 0 then b = 3 end
    self.m_YingLingSwitchStateTable  = {state, a, b}
    self:UpdateStateFlagIcons()

    self.m_YingLingSwitchStateBtns[1].m_BackGround.spriteName = self.m_SwitchStateHighlightSpriteNameTable[state]
    self.m_YingLingSwitchStateBtns[2].m_BackGround.spriteName = self.m_SwitchStateNormalSpriteNameTable[a]
    self.m_YingLingSwitchStateBtns[3].m_BackGround.spriteName = self.m_SwitchStateNormalSpriteNameTable[b]
    self.m_CurSwitchYingLingState = state
    LuaPVPMgr.m_CurrentYingLingState = self.m_CurSwitchYingLingState
    EventManager.BroadcastInternalForLua(EnumEventType.SwitchYingLingMenPaiRobotState, {})
end
