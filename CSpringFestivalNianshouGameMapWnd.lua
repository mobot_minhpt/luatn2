-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local ChunJie_NianShouDaZuoZhan = import "L10.Game.ChunJie_NianShouDaZuoZhan"
local CommonDefs = import "L10.Game.CommonDefs"
local CSpringFestivalMgr = import "L10.Game.CSpringFestivalMgr"
local CSpringFestivalNianshouGameMapWnd = import "L10.UI.CSpringFestivalNianshouGameMapWnd"
local Gac2Gas = import "L10.Game.Gac2Gas"
local GameObject = import "UnityEngine.GameObject"
local NGUITools = import "NGUITools"
local UInt32 = import "System.UInt32"
local Vector3 = import "UnityEngine.Vector3"
CSpringFestivalNianshouGameMapWnd.m_Init_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.otherNode.Length do
            this.otherNode[i]:SetActive(false)
            i = i + 1
        end
    end
    do
        local i = 0
        while i < this.selfNode.Length do
            this.selfNode[i]:SetActive(false)
            i = i + 1
        end
    end


    this.numLabel.text = (tostring(ChunJie_NianShouDaZuoZhan.GetData().NianHuoCount) .. "/") .. tostring(ChunJie_NianShouDaZuoZhan.GetData().NianHuoCount)
    Gac2Gas.RequestNSDZZNPCPosInfo()
end
CSpringFestivalNianshouGameMapWnd.m_PlayExplore_CS2LuaHook = function (this, enginId) 
end
CSpringFestivalNianshouGameMapWnd.m_UpdateMiniMap_CS2LuaHook = function (this, type) 
    if CClientMainPlayer.Inst == nil then
        return
    end

    if CommonDefs.DictContains(this.nodeCache, typeof(UInt32), type) then
        local list = CommonDefs.DictGetValue(this.nodeCache, typeof(UInt32), type)
        do
            local i = 0
            while i < list.Count do
                GameObject.Destroy(list[i])
                i = i + 1
            end
        end
        CommonDefs.ListClear(list)
    else
        CommonDefs.DictSet(this.nodeCache, typeof(UInt32), type, typeof(MakeGenericClass(List, GameObject)), CreateFromClass(MakeGenericClass(List, GameObject)))
    end

    if CommonDefs.DictContains(CSpringFestivalMgr.Instance.miniMapInfo, typeof(UInt32), type) then
        local dataList = CommonDefs.DictGetValue(CSpringFestivalMgr.Instance.miniMapInfo, typeof(UInt32), type)
        do
            local i = 0
            while i < dataList.Count do
                local data = dataList[i]
                if data.engineId == CClientMainPlayer.Inst.EngineId then
                    if this.selfNode.Length >= type then
                        local node = NGUITools.AddChild(this.calMapTexture.gameObject, this.selfNode[type - 1])
                        node:SetActive(true)
                        node.transform.localPosition = this:CalculateMapPos(Vector3(data.x, 0, data.y))
                        CommonDefs.ListAdd(CommonDefs.DictGetValue(this.nodeCache, typeof(UInt32), type), typeof(GameObject), node)
                    end
                else
                    if this.otherNode.Length >= type then
                        local node = NGUITools.AddChild(this.calMapTexture.gameObject, this.otherNode[type - 1])
                        node:SetActive(true)
                        node.transform.localPosition = this:CalculateMapPos(Vector3(data.x, 0, data.y))
                        CommonDefs.ListAdd(CommonDefs.DictGetValue(this.nodeCache, typeof(UInt32), type), typeof(GameObject), node)
                    end
                end
                i = i + 1
            end
        end

        if type == 5 then
            this.numLabel.text = (tostring(dataList.Count) .. "/") .. tostring(ChunJie_NianShouDaZuoZhan.GetData().NianHuoCount)
        end
    end
end
