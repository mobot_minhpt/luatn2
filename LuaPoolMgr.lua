local CMainCamera = import "L10.Engine.CMainCamera"
local CClientFurniture =import "L10.Game.CClientFurniture"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CTerrainFast=import "L10.Engine.CTerrainFast"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local PoolGridHelper = import "L10.Engine.PoolGridHelper"
local CClientObjectRoot=import "L10.Game.CClientObjectRoot"
local CoreScene = import "L10.Engine.Scene.CoreScene"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"
local CRenderObject = import "L10.Engine.CRenderObject"
local LayerDefine = import "L10.Engine.LayerDefine"
local CPoolType = import "L10.Game.CPoolType"
local CPlayerDataMgr = import "L10.Game.CPlayerDataMgr"
local CWater = import "L10.Engine.CWater"
local Engine = import "L10.Engine"
local CUIResources = import "L10.UI.CUIResources"
local Boolean = import "System.Boolean"
local Camera = import "UnityEngine.Camera"
local Quaternion = import "UnityEngine.Quaternion"
local CSEBarrierType = import "L10.Engine.EBarrierType"
local Object = import "System.Object"

if not LuaPoolMgr then
    LuaPoolMgr = {}
end
LuaPoolMgr.m_Water = nil
LuaPoolMgr.m_IsWinterStyle = false

--水域编辑试投放开关
LuaPoolMgr.s_Pool_Editor_Switch = true
--海梦泽开关
LuaPoolMgr.s_House_Pool_Switch_On = true


--   ^
--   |
--1-[2]-->
--3--4
--编码为p[4] + p[3]<<1 + p[2]<<2 + p[1]<<3
--[石头模板的编码] {美术资源的后缀}
LuaPoolMgr.Code2Template = {
    --00
    --01
    [1] = {6},--0001
    --00
    --10
    [2] = {7},--0010
    --11
    --00
    [3] = {2,14},--[[该编下有两种美术资源]]
    --01
    --00
    [4] = {9},
    --01
    --01
    [5] = {4,17},
    --01
    --10
    [6] = {5},
    --01
    --11
    [7] = {8},

    --[[
        8 两格 横
        9 两格 竖
        10 大拐角
        11 大拐角
        12 大拐角
        13 大拐角
    ]]
}

LuaPoolMgr.Code2Path = {
    [8] = {
        "Character/Jiayuan/%s_shuichi_01/Prefab/%s_shuichi_01_01.prefab",
        "Character/Jiayuan/%s_shuichi_15/Prefab/%s_shuichi_15_01.prefab"
    },
    [9] = {
        "Character/Jiayuan/%s_shuichi_03/Prefab/%s_shuichi_03_01.prefab",
        "Character/Jiayuan/%s_shuichi_16/Prefab/%s_shuichi_16_01.prefab"
    },
    [10] = {"Character/Jiayuan/%s_shuichi_10/Prefab/%s_shuichi_10_01.prefab"},--[[右上角]]
    [11] = {"Character/Jiayuan/%s_shuichi_11/Prefab/%s_shuichi_11_01.prefab"},--[[右下角]] 
    [12] = {"Character/Jiayuan/%s_shuichi_12/Prefab/%s_shuichi_12_01.prefab"},--[[左下角]]
    [13] = {"Character/Jiayuan/%s_shuichi_13/Prefab/%s_shuichi_13_01.prefab"}--[[左上角]]
}

LuaPoolMgr.Code2PlacedIndexOffset = nil

LuaPoolMgr.RandomRotations = {
    0,
    180,
}

LuaPoolMgr.CurPoolCount = 0
LuaPoolMgr.MaxCameraY = 50
LuaPoolMgr.MinCameraY = 36
BarrierType2Int = {[CSEBarrierType.eBT_NoBarrier] = 0, [CSEBarrierType.eBT_LowBarrier] = 1, [CSEBarrierType.eBT_MidBarrier] = 2, [CSEBarrierType.eBT_HighBarrier] = 3, [CSEBarrierType.eBT_OutRange] = 4}
function LuaPoolMgr.OpenPoolEditer()
    CClientFurnitureMgr.Inst.mbContinuedDown = false
    CClientFurnitureMgr.Inst.mbIsContinued = false
    CClientFurnitureMgr.Inst.FurnishMode = false
    CUIManager.CloseUI(CUIResources.FurnitureRightTopWnd)
    CUIManager.CloseUI(CUIResources.FurnitureSmallTypeWnd)
    CUIManager.CloseUI(CUIResources.FurnitureBigTypeWnd)
    CUIManager.CloseUI(CUIResources.UseZuoanWnd)
    CUIManager.CloseUI(CUIResources.FurnitureSlider)
    CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, CClientFurnitureMgr.Inst.m_Except, false, false)
    CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, false, false)
    Engine.CameraControl.CameraFollow.Inst.m_IsInRoomFurnish = false
    if CClientHouseMgr.Inst:IsInOwnHouse() then
        CUIManager.ShowUI(CUIResources.HouseMinimap)
    end

    CClientFurnitureMgr.Inst:ClearCurFurniture()

    LuaPoolMgr.MapWidth = CRenderScene.Inst.MapWidth
    LuaPoolMgr.m_LandList = {}
    LuaPoolMgr.m_WarmLandList = {}
    LuaPoolMgr.BackUpTerrianTypeList()

    --camera
    local isMingYuan = CClientFurnitureMgr.Inst.m_IsMingYuan
    LuaPoolMgr.Basex, LuaPoolMgr.Basey = GetPoolBase(isMingYuan, false)
    LuaPoolMgr.MaxHeight, LuaPoolMgr.MaxWidth = GetPoolSize(isMingYuan)


    local basex,basey,height,width = LuaPoolMgr.Basex, LuaPoolMgr.Basey,LuaPoolMgr.MaxHeight, LuaPoolMgr.MaxWidth
    PoolGridHelper.Inst:DrawHelpMeshs(basex,basey,width,height)
    LuaPoolMgr.UpdatePoolGridHelper()

    CClientObjectRoot.Inst:HideMainPlayer(true)

    LuaPoolMgr.CameraGo = GameObject("PoolEditorCamera")
    local player = CClientMainPlayer.Inst
    if player then 
        local y = LuaPoolMgr.MinCameraY + (LuaPoolMgr.MaxCameraY - LuaPoolMgr.MinCameraY)/2
        LuaPoolMgr.CameraGo.transform.localPosition = Vector3(player.RO.Position.x,y,player.RO.Position.z)
    end
    LuaPoolMgr.CameraGo.transform.localRotation = Quaternion.Euler(90,90,0)
    LuaPoolMgr.PoolEditorCamera = CommonDefs.AddCameraComponent(LuaPoolMgr.CameraGo)

    CUIManager.ShowUI(CLuaUIResources.PoolEditWnd)
    CMainCamera.Inst:SetCameraEnableStatus(false,"use_another_camera", false)
end

function LuaPoolMgr.QuitPoolEditer()
    CMainCamera.Inst:SetCameraEnableStatus(true,"use_another_camera", false)
    CameraFollow.Inst:ResetToDefault()
    PoolGridHelper.Inst:DestroyGrid()
    CClientObjectRoot.Inst:HideMainPlayer(false)

    --断线的时候，会关掉编辑界面，进到这里
    if CClientHouseMgr.Inst.mCurFurnitureProp2 then
        LuaPoolMgr.LoadPreClipData()
        LuaPoolMgr.GenBorder(0)
        LuaPoolMgr.GenBorder(1)
    end
    
    GameObject.DestroyImmediate(LuaPoolMgr.CameraGo)
    LuaPoolMgr.CameraGo = nil
    LuaPoolMgr.PoolEditorCamera = nil
    LuaPoolMgr.IsEditChanged = false
end

function LuaPoolMgr.GenTerrainFromPoolMark()
    if not CRenderScene.Inst then
        return
    end
    LuaPoolMgr.MapWidth = CRenderScene.Inst.MapWidth
    LuaPoolMgr.LoadPreClipList()

    LuaPoolMgr.RefreshIce()
end

function LuaPoolMgr.RefreshIce()
    if LuaPoolMgr.m_Water then
        LuaPoolMgr.m_Water:RefreshPoolMaskTexture()
    end
end
function LuaPoolMgr.TrySetIce(x,y,active)
    if LuaPoolMgr.m_Water then
        LuaPoolMgr.m_Water:SetIce(x,y,active)
    end
end

function LuaPoolMgr.GetChunkInfoFromPos(x,y,terrainFast)
    if not terrainFast then return nil end
    local info = {}
    local cx = math.floor(x / CTerrainFast.CHUNK_SIZE)
    local cy = math.floor(y / CTerrainFast.CHUNK_SIZE)
    local nx = math.floor((x/terrainFast.m_HeightMapScale.x)%CTerrainFast.CHUNK_RESOLUTION)
    local ny = math.floor((y/terrainFast.m_HeightMapScale.z)%CTerrainFast.CHUNK_RESOLUTION)
    info.cgridX = nx
    info.cgridY = ny
    info.chunkIndex = cx + cy*terrainFast.m_ChunkWidth
    info.position = {
        x = x,
        z = y
    }
    info.isPool = true
    return info
end

function LuaPoolMgr.BackUpTerrianTypeList()
    LuaPoolMgr.ServerTerrianTypeList = {}
    for index,type in pairs(LuaPoolMgr.TerrianTypeList) do
        LuaPoolMgr.ServerTerrianTypeList[index] = type
    end
end

function LuaPoolMgr.LoadPreClipData()
    LuaPoolMgr.TerrianTypeList = {}
    LuaPoolMgr.m_PoolPointList = {}

    LuaPoolMgr.m_WarmPoolPointList = {}

    LuaPoolMgr.m_PoolList = {}
    LuaPoolMgr.m_WarmPoolList = {}

    -- LuaPoolMgr.m_StonePut = {}
    -- LuaPoolMgr.m_WarmStonePut = {}
    if not CRenderScene.Inst then
        return
    end
    LuaPoolMgr.MapWidth = CRenderScene.Inst.MapWidth

    local pool = CClientFurnitureMgr.Inst.m_Pool
    local warmPool = CClientFurnitureMgr.Inst.m_WarmPool
    if not pool or not warmPool then
        return
    end

    local isMingYuan = CClientFurnitureMgr.Inst.m_IsMingYuan
    local basex, basey = GetPoolBase(isMingYuan,false)
    local heigh, width = GetPoolSize(isMingYuan)

    if pool.PoolInited == 0 then
        pool = LuaPoolMgr.LoadDefaultPool(isMingYuan,basex, basey,heigh, width, false)
    end
    if warmPool.PoolInited == 0 and isMingYuan then
        warmPool = LuaPoolMgr.LoadDefaultPool(isMingYuan,basex, basey,heigh, width,true)
    end

    
    local mark = pool.PoolMark
    local warmMark = warmPool.PoolMark

    LuaPoolMgr.CurPoolCount = 0
    local terrainFast = CRenderScene.Inst.TerrainFast
    CommonDefs.DictClear(terrainFast.clipDict)

    local CHUNK_SIZE = CTerrainFast.CHUNK_SIZE
    local ChunkWidth = terrainFast.m_ChunkWidth
    for h = 0,heigh-1,1 do
        for w=0,width-1,1 do
            local i = h*width + w
            local ismark = mark:GetBit(i)
            local iswarmmark = warmMark:GetBit(i)
            if ismark or iswarmmark then
                LuaPoolMgr.CurPoolCount = LuaPoolMgr.CurPoolCount + 1
                local y = h + basey
                local x = w + basex

                local cx = math.floor(x / CHUNK_SIZE)
                local cy = math.floor(y / CHUNK_SIZE)
                local chunkIndex = cx + cy*ChunkWidth

                local gridIndex = y * LuaPoolMgr.MapWidth + x

                if not CommonDefs.DictContains(terrainFast.clipDict, typeof(Int32), chunkIndex) then
                    CommonDefs.DictAdd(terrainFast.clipDict, typeof(Int32), chunkIndex, typeof(MakeGenericClass(Dictionary, Int32, Boolean)), CreateFromClass(MakeGenericClass(Dictionary, Int32, Boolean)))
                end
                CommonDefs.DictAdd(terrainFast.clipDict[chunkIndex],typeof(Int32),gridIndex,typeof(Boolean),true)
                
                if iswarmmark then
                    table.insert(LuaPoolMgr.m_WarmPoolPointList,x)
                    table.insert(LuaPoolMgr.m_WarmPoolPointList,y)
                    LuaPoolMgr.TerrianTypeList[y*LuaPoolMgr.MapWidth + x] = LuaPoolMgr.WarmPoolType
                elseif ismark then
                    table.insert(LuaPoolMgr.m_PoolPointList,x)
                    table.insert(LuaPoolMgr.m_PoolPointList,y)
                    LuaPoolMgr.TerrianTypeList[y*LuaPoolMgr.MapWidth + x] = LuaPoolMgr.PoolType
                end
            end
        end
    end
end

function LuaPoolMgr.LoadPreClipList()   
    LuaPoolMgr.LoadPreClipData()
    CClientFurnitureMgr.Inst:UpdateSpecialPoolRegion()
    local terrainFast = CRenderScene.Inst.TerrainFast
    terrainFast:GenerateTerrainMesh()
    LuaPoolMgr.GenBorder(0)
    LuaPoolMgr.GenBorder(1)
end

function LuaPoolMgr.LoadDefaultPool(isMingYuan,basex, basey,heigh, width,isWarmPool)
    if not CRenderScene.Inst then
        return 
    end
    local terrainFast = CRenderScene.Inst.TerrainFast
    local defaultPool
    if not isWarmPool then
        defaultPool = isMingYuan and GetDefaultMingYuanPool(false) or GetDefaultNormalPool()
    else
        defaultPool = isMingYuan and GetDefaultMingYuanPool(true) or nil
    end
    return defaultPool
end

LuaPoolMgr.m_PoolList = {}
LuaPoolMgr.m_LandList = {}
LuaPoolMgr.m_WarmPoolList = {}
LuaPoolMgr.m_WarmLandList = {}

if not LuaPoolMgr.m_PoolStoneList then
    LuaPoolMgr.m_PoolStoneList = {}
end

if not LuaPoolMgr.m_WarmPoolStoneList then
    LuaPoolMgr.m_WarmPoolStoneList = {}
end

LuaPoolMgr.MAX_HEIGHT_PER_MESH = 2

LuaPoolMgr.PoolType_All = 1
LuaPoolMgr.PoolType_Pool = 2 
LuaPoolMgr.PoolType_WarmPool = 3

function LuaPoolMgr.IsPoolAt(x,y,type)
    if type == LuaPoolMgr.PoolType_Pool and LuaPoolMgr.TerrianTypeList[y*LuaPoolMgr.MapWidth + x] == LuaPoolMgr.PoolType then
         return true
    elseif type == LuaPoolMgr.PoolType_WarmPool and LuaPoolMgr.TerrianTypeList[y*LuaPoolMgr.MapWidth + x] == LuaPoolMgr.WarmPoolType then
             return true
    elseif not type or type == LuaPoolMgr.PoolType_All then
        return LuaPoolMgr.TerrianTypeList[y*LuaPoolMgr.MapWidth + x] == LuaPoolMgr.PoolType or LuaPoolMgr.TerrianTypeList[y*LuaPoolMgr.MapWidth + x] == LuaPoolMgr.WarmPoolType
    else
         return false
    end
end

if not LuaPoolMgr.TerrianTypeList then
    LuaPoolMgr.TerrianTypeList = {}
end

LuaPoolMgr.WarmPoolType = 0
LuaPoolMgr.PoolType = 1
LuaPoolMgr.LandType = 2
LuaPoolMgr.UnknowType = 3

if not LuaPoolMgr.m_PoolPointList then
    LuaPoolMgr.m_PoolPointList = {}
end

if not LuaPoolMgr.m_WarmPoolPointList  then
    LuaPoolMgr.m_WarmPoolPointList = {}
end

function LuaPoolMgr.GenBorder(poolType,isEditing,editingX,editingY)
    local type = poolType == 0 and LuaPoolMgr.PoolType_Pool or LuaPoolMgr.PoolType_WarmPool
    local poolPointList
    local borderList = {}

    local housename 
    if poolType == 0 then--普通水池
        housename = "jyputong"
        local x,y,index
        if not isEditing then
            --所有石头重新围边
            for i, stone in pairs(LuaPoolMgr.m_PoolStoneList) do
                if stone and stone.ro then
                    index = stone.cachedIndex
                    LuaPoolMgr.PutStoneInCaches(poolType,stone.borderCode,stone.ro,index)
                    stone.ro = nil
                end
            end
            LuaPoolMgr.m_PoolStoneList = {}
            LuaPoolMgr.m_StonePut = {}
        else
            --编辑状态下 删除选中格子四个角上的石头 
            for k=0,1,1 do
                for h = 0,1,1 do
                    x = editingX+k
                    y = editingY + h
                    local putIndex = LuaPoolMgr.CheckStonePut(x,y,poolType)
                    if putIndex then
                        local stone = LuaPoolMgr.m_PoolStoneList[putIndex]
                        if stone and stone.ro then
                            local code = stone.borderCode
                            LuaPoolMgr.PutStoneInCaches(poolType,code,stone.ro,putIndex)
                            for i,pindex in ipairs(stone.placedIndexs) do
                                x = pindex % LuaPoolMgr.MapWidth
                                y = math.floor(pindex / LuaPoolMgr.MapWidth)
                                LuaPoolMgr.ClearStonePut(x, y, poolType)
                            end
                        end
                        LuaPoolMgr.m_PoolStoneList[putIndex] = nil
                    end
                end
            end
        end
        poolPointList = LuaPoolMgr.m_PoolPointList
    elseif poolType == 1 then--温泉
        housename = "jymingyuan"
        local x,y,index
        if not isEditing then
            for i, stone in pairs(LuaPoolMgr.m_WarmPoolStoneList) do
                if stone.ro then
                    index = stone.cachedIndex
                    LuaPoolMgr.PutStoneInCaches(poolType,stone.borderCode,stone.ro,index)
                    stone.ro = nil
                end
            end
            LuaPoolMgr.m_WarmPoolStoneList = {}
            LuaPoolMgr.m_WarmStonePut = {}
        else
            for k=0,1,1 do
                for h = 0,1,1 do
                    x = editingX+k
                    y = editingY + h
                    local putIndex = LuaPoolMgr.CheckStonePut(x,y,poolType)
                    if putIndex then
                        local stone = LuaPoolMgr.m_WarmPoolStoneList[putIndex]
                        if stone and stone.ro then
                            LuaPoolMgr.PutStoneInCaches(poolType,stone.borderCode,stone.ro,putIndex)
                            for i,pindex in ipairs(stone.placedIndexs) do
                                x = pindex % LuaPoolMgr.MapWidth
                                y = math.floor(pindex / LuaPoolMgr.MapWidth)
                                LuaPoolMgr.ClearStonePut(x, y, poolType)
                            end
                        end
                        LuaPoolMgr.m_WarmPoolStoneList[putIndex] = nil
                    end
                end
            end
        end

        poolPointList = LuaPoolMgr.m_WarmPoolPointList
    end

    local borderList = {}
    local sortBorderList = {}
    for i=1,#poolPointList,2 do
        local x = poolPointList[i]
        local y = poolPointList[i+1]

        if not borderList[x] then
            borderList[x] = {}
        end
        if not borderList[x+1] then
            borderList[x+1] = {}
        end
        if not borderList[x][y] then
        table.insert(sortBorderList,{x=x,y=y})
        end
        if not borderList[x][y+1] then
        table.insert(sortBorderList,{x=x,y=y+1})
        end
        if not borderList[x+1][y] then
        table.insert(sortBorderList,{x=x+1,y=y})
        end
        if not borderList[x+1][y+1] then
        table.insert(sortBorderList,{x=x+1,y=y+1})
        end
        borderList[x][y]=1
        borderList[x][y+1]=1
        borderList[x+1][y]=1
        borderList[x+1][y+1]=1
    end
    table.sort(sortBorderList, function(a, b)
        if a.x == b.x then
            return a.y < b.y 
        end
		return a.x < b.x
	end)

    --先填大的模板
    for i,pos in ipairs(sortBorderList) do
        if not LuaPoolMgr.CheckStonePut(pos.x,pos.y,poolType) then
            LuaPoolMgr.CheckTemplate(pos.x,pos.y,poolType,isEditing)
        end
    end

    --再用小的模板填空缺
    LuaPoolMgr.FillBorderWithSingleStone(poolType,sortBorderList)
end

function LuaPoolMgr.FillBorderWithSingleStone(poolType,sortBorderList)
    local fillIndexs = {}
    local poolStoneList
    if poolType == 0 then
        poolStoneList = LuaPoolMgr.m_PoolStoneList
    elseif poolType == 1 then
        poolStoneList = LuaPoolMgr.m_WarmPoolStoneList
    else
        return
    end

    local root = CClientFurnitureMgr.sPoolStoneRoot.transform
    local type = poolType == 0 and LuaPoolMgr.PoolType_Pool or LuaPoolMgr.PoolType_WarmPool

    for _,pos in ipairs(sortBorderList) do
        local x = pos.x
        local y = pos.y
        local ii = y*LuaPoolMgr.MapWidth+x

        if not LuaPoolMgr.CheckStonePut(pos.x,pos.y,poolType) then
            local isPool1 = LuaPoolMgr.IsPoolAt(x-1,y,type) and 1 or 0
            local isPool2 = LuaPoolMgr.IsPoolAt(x,y,type) and 1 or 0
            local isPool3 = LuaPoolMgr.IsPoolAt(x-1,y-1,type) and 1 or 0
            local isPool4 = LuaPoolMgr.IsPoolAt(x,y-1,type) and 1 or 0
            local broaderCode = isPool4 + isPool3*2 + isPool2*4 + isPool1*8
            if broaderCode > 7 then
                broaderCode = 15 - broaderCode--相当于异或
            end
            local code = broaderCode

            if broaderCode ~= 0 then
                local tbl = LuaPoolMgr.Code2Template[tonumber(broaderCode)]
                local rotation = 0
                if broaderCode == 3 or broaderCode == 5 then
                    rotation = (x + y) % #LuaPoolMgr.RandomRotations + 1
                    rotation = LuaPoolMgr.RandomRotations[rotation]
                end
                broaderCode = tbl[1]
                        
                local stoneRo = LuaPoolMgr.GetStoneFromCaches(poolType,code,ii)
                stoneRo.transform.parent = root
                local height = CClientFurnitureMgr.Inst:GetOrigLogicHeightIgnorePool(x+0.5, y+0.5)

                stoneRo.transform.position = Vector3(x,height,y)
                stoneRo.transform.localEulerAngles = Vector3(0, rotation, 0)
                --stoneRo.Scale = 0.5

                local index = y*LuaPoolMgr.MapWidth + x
                if poolType == 0 then
                    local stone = {}
                    stone.ro = stoneRo
                    stone.placedIndexs = {index}
                    stone.borderCode = code
                    stone.cachedIndex = index
                    LuaPoolMgr.m_PoolStoneList[index] = stone
                elseif poolType == 1 then
                    local stone = {}
                    stone.ro = stoneRo
                    stone.placedIndexs = {index}
                    stone.borderCode = code
                    stone.cachedIndex = index
                    LuaPoolMgr.m_WarmPoolStoneList[index] = stone--ro
                end
                LuaPoolMgr.SetStonePut(x,y,poolType,index)
            end
        end
    end
end

if not LuaPoolMgr.m_StonePut then
    LuaPoolMgr.m_StonePut= {}
end
if not LuaPoolMgr.m_WarmStonePut then
    LuaPoolMgr.m_WarmStonePut = {}
end
function LuaPoolMgr.CheckTemplate(x,y,poolType,isEditing)
    if isEditing then
        return
    end
    local type = poolType == 0 and LuaPoolMgr.PoolType_Pool or LuaPoolMgr.PoolType_WarmPool
    local root = CClientFurnitureMgr.sPoolStoneRoot.transform

    local isPool1 = LuaPoolMgr.IsPoolAt(x-1,y,type) and 1 or 0
    local isPool2 = LuaPoolMgr.IsPoolAt(x,y,type) and 1 or 0
    local isPool3 = LuaPoolMgr.IsPoolAt(x-1,y-1,type) and 1 or 0
    local isPool4 = LuaPoolMgr.IsPoolAt(x,y-1,type) and 1 or 0

    local isPool5 = LuaPoolMgr.IsPoolAt(x+1,y-1,type) and 1 or 0
    local isPool6 = LuaPoolMgr.IsPoolAt(x-1,y+1,type) and 1 or 0
    local isPool7 = LuaPoolMgr.IsPoolAt(x,y+1,type) and 1 or 0
    local isPool8 = LuaPoolMgr.IsPoolAt(x+1,y+1,type) and 1 or 0
    local isPool9 = LuaPoolMgr.IsPoolAt(x+1,y,type) and 1 or 0

    if not LuaPoolMgr.Code2PlacedIndexOffset then
        LuaPoolMgr.Code2PlacedIndexOffset = {
            [8] = {0, 1, 2},--1000
            [9] = {0, LuaPoolMgr.MapWidth, 2*LuaPoolMgr.MapWidth},--1001
            [10] = {0, -LuaPoolMgr.MapWidth, -1},--1010
            [11] = {0, -1, LuaPoolMgr.MapWidth},--1011
            [12] = {0, LuaPoolMgr.MapWidth, 1},--1100
            [13] = {0, 1, -LuaPoolMgr.MapWidth},--1101
        }
    end
    local IsTemplate = function (poolstr,templatestr,code,px,py)
        if poolstr == templatestr then
            local index = y*LuaPoolMgr.MapWidth + x
            local stone = {}

            stone.placedIndexs = {}
            stone.borderCode = code
            local indexoffset = LuaPoolMgr.Code2PlacedIndexOffset[code]
            for i, offset in ipairs(indexoffset) do
                table.insert(stone.placedIndexs,index + offset)--
            end

            local stoneRo = LuaPoolMgr.GetStoneFromCaches(poolType,code,index)
            stoneRo.transform.parent = root
            local height = CClientFurnitureMgr.Inst:GetOrigLogicHeightIgnorePool(x+0.5, y+0.5)
            stoneRo.transform.position = Vector3(px,height,py)
            stone.ro = stoneRo
            stone.cachedIndex = index
            if poolType == 0 then
                LuaPoolMgr.m_PoolStoneList[index] = stone
            elseif poolType == 1 then
                LuaPoolMgr.m_WarmPoolStoneList[index] = stone
            end
            return true
        end
        return false
    end

    --左下角
    local index = y*LuaPoolMgr.MapWidth + x
    if not LuaPoolMgr.CheckStonePut(x,y+1,poolType) and not LuaPoolMgr.CheckStonePut(x+1,y,poolType) then
        local b = IsTemplate(isPool1..isPool2..isPool3..isPool4..isPool5..isPool6..isPool7..isPool9,"01000011",12,x,y)
        if b then
            LuaPoolMgr.SetStonePut(x,y,poolType,index)
            LuaPoolMgr.SetStonePut(x+1,y,poolType,index)
            LuaPoolMgr.SetStonePut(x,y+1,poolType,index)
            return true
        end
    end
    local p5 = LuaPoolMgr.IsPoolAt(x-2,y,type) and 1 or 0
    local p6 = LuaPoolMgr.IsPoolAt(x-2,y-1,type) and 1 or 0
    local p8 = LuaPoolMgr.IsPoolAt(x-1,y-2,type) and 1 or 0
    local p9 = LuaPoolMgr.IsPoolAt(x,y-2,type) and 1 or 0
    --10 右上角
    if not LuaPoolMgr.CheckStonePut(x-1,y,poolType) and not LuaPoolMgr.CheckStonePut(x,y-1,poolType) then
        local b = IsTemplate(isPool1..isPool2..isPool3..isPool4..p5..p6..p8..p9,"00100110",10,x,y)
        if b then
            LuaPoolMgr.SetStonePut(x,y,poolType,index)
            LuaPoolMgr.SetStonePut(x-1,y,poolType,index)
            LuaPoolMgr.SetStonePut(x,y-1,poolType,index)
            return true
        end
    end
    --13 左上角
    if not LuaPoolMgr.CheckStonePut(x+1,y,poolType) and not LuaPoolMgr.CheckStonePut(x,y-1,poolType) then
        local b = IsTemplate(isPool1..isPool2..isPool3..isPool4..isPool5..isPool9..p8..p9,"00011001",13,x,y) 
        if b then
            LuaPoolMgr.SetStonePut(x,y,poolType,index)
            LuaPoolMgr.SetStonePut(x+1,y,poolType,index)
            LuaPoolMgr.SetStonePut(x,y-1,poolType,index)
            return true
        end
    end
    --11 右下角
    p8 = LuaPoolMgr.IsPoolAt(x-1,y+2,type) and 1 or 0
    p9 = LuaPoolMgr.IsPoolAt(x,y+2,type) and 1 or 0
    if not LuaPoolMgr.CheckStonePut(x-1,y,poolType) and not LuaPoolMgr.CheckStonePut(x,y+1,poolType) then
        local b = IsTemplate(isPool1..isPool2..isPool3..isPool4..p5..p6..isPool6..isPool7,"10001010",11,x,y)--10001010
        if b then
            LuaPoolMgr.SetStonePut(x,y,poolType,index)
            LuaPoolMgr.SetStonePut(x-1,y,poolType,index)
            LuaPoolMgr.SetStonePut(x,y+1,poolType,index)
            return true
        end
    end
    --两格横线
    local p1 = LuaPoolMgr.IsPoolAt(x+2,y,type) and 1 or 0
    local p2 = LuaPoolMgr.IsPoolAt(x+2,y-1,type) and 1 or 0
    if not LuaPoolMgr.CheckStonePut(x+1,y,poolType) and not LuaPoolMgr.CheckStonePut(x+2,y,poolType) then
        local b = (IsTemplate(isPool2..isPool9..isPool4..isPool5..p1..isPool1..isPool3..p2,"11001100",8,x+1,y) or IsTemplate(isPool2..isPool9..isPool4..isPool5..p1..isPool1..isPool3..p2,"00110011",8,x+1,y))
        if b then
            LuaPoolMgr.SetStonePut(x,y,poolType,index)
            LuaPoolMgr.SetStonePut(x+1,y,poolType,index)
            LuaPoolMgr.SetStonePut(x+2,y,poolType,index)
            return
        end
    end
    --两格竖线
    local p1 = LuaPoolMgr.IsPoolAt(x-1,y+2,type) and 1 or 0
    local p2 = LuaPoolMgr.IsPoolAt(x,y+2,type) and 1 or 0
    if not LuaPoolMgr.CheckStonePut(x,y+1,poolType) and not LuaPoolMgr.CheckStonePut(x,y+2,poolType) then
        local b = (IsTemplate(isPool6..isPool7..isPool1..isPool2..p1..p2..isPool3..isPool4,"01010101",9,x,y+1) or IsTemplate(isPool6..isPool7..isPool1..isPool2..p1..p2..isPool3..isPool4,"10101010",9,x,y+1))
        if b then
            LuaPoolMgr.SetStonePut(x,y,poolType,index)
            LuaPoolMgr.SetStonePut(x,y+1,poolType,index)
            LuaPoolMgr.SetStonePut(x,y+2,poolType,index)
            return
        end
    end
    return
end

function LuaPoolMgr.CheckStonePut(x,y,poolType)
    if poolType == 0 then
        if LuaPoolMgr.m_StonePut[x] then
            if LuaPoolMgr.m_StonePut[x][y] then
                return LuaPoolMgr.m_StonePut[x][y]
            else
                return nil
            end
        else
            return nil
        end
    elseif poolType == 1 then
        if LuaPoolMgr.m_WarmStonePut[x] then
            if LuaPoolMgr.m_WarmStonePut[x][y] then
                return LuaPoolMgr.m_WarmStonePut[x][y]
            else
                return nil
            end
        else
            return nil
        end
    end
end

function LuaPoolMgr.SetStonePut(x,y,poolType,index)
    if poolType == 0 then
        if LuaPoolMgr.m_StonePut[x] then
            LuaPoolMgr.m_StonePut[x][y] = index
        else
            LuaPoolMgr.m_StonePut[x] = {}
            LuaPoolMgr.m_StonePut[x][y] = index
        end
    elseif poolType == 1 then
        if LuaPoolMgr.m_WarmStonePut[x] then
            LuaPoolMgr.m_WarmStonePut[x][y] = index
        else
            LuaPoolMgr.m_WarmStonePut[x] = {}
            LuaPoolMgr.m_WarmStonePut[x][y] = index
        end
    end
end

function LuaPoolMgr.ClearStonePut(x,y,poolType)
    if poolType == 0 then
        if LuaPoolMgr.m_StonePut[x] then
            LuaPoolMgr.m_StonePut[x][y] = nil
        end
    elseif poolType == 1 then
        if LuaPoolMgr.m_WarmStonePut[x] then
            LuaPoolMgr.m_WarmStonePut[x][y] = nil
        end
    end
end

function LuaPoolMgr.EnterJiaYuan()
    --初始化变量，尝试取水池数据
    local basicProp = CClientHouseMgr.Inst.mCurBasicProp
    if basicProp then
        CClientFurnitureMgr.Inst.m_IsMingYuan = basicProp.IsMingyuan==1
    end
    local curFurnitureProp2 = CClientHouseMgr.Inst.mCurFurnitureProp2
    if curFurnitureProp2 then
        CClientFurnitureMgr.Inst.m_Pool = curFurnitureProp2.Pool
        CClientFurnitureMgr.Inst.m_WarmPool = curFurnitureProp2.WarmPool
    end

    if CClientFurnitureMgr.sPoolStoneRoot and CClientFurnitureMgr.sPoolStoneRoot.transform then
        --Extensions.RemoveAllChildren(CClientFurnitureMgr.sPoolStoneRoot.transform)
    end
    if not LuaPoolMgr.m_StonePool or not LuaPoolMgr.m_StonePool.transform then
        LuaPoolMgr.m_StonePool = GameObject("StonePool")
    end
    LuaPoolMgr.m_StonePool.transform.parent = CClientFurnitureMgr.sPoolStoneRoot.transform
    LuaPoolMgr.m_StoneCaches = {}
    LuaPoolMgr.m_WarmStoneCaches = {}

    
    local waternode = CRenderScene.Inst.transform:Find("OtherObjects/Water/Water_in")
    if waternode then
        LuaPoolMgr.m_Water = waternode:GetComponent(typeof(CWater))
        local prop = CClientHouseMgr.Inst.mCurFurnitureProp
        if prop and CClientFurnitureMgr.Inst.m_IsWinter then
            if prop.IsEnableWinterScene>0 and prop.WinterSceneStyle==0 then
                LuaPoolMgr.m_IsWinterStyle = true
            else
                LuaPoolMgr.m_IsWinterStyle = false
            end
            LuaPoolMgr.m_Water.m_HaveIce = LuaPoolMgr.m_IsWinterStyle
        else
            LuaPoolMgr.m_IsWinterStyle = false
            LuaPoolMgr.m_Water.m_HaveIce = false
        end
    end
end

function LuaPoolMgr.LeaveJiaYuan()
    CClientFurnitureMgr.Inst.m_IsMingYuan = false
    CClientFurnitureMgr.Inst.m_Pool = nil
    CClientFurnitureMgr.Inst.m_WarmPool = nil
    CClientHouseMgr.Inst:LeaveScene()

    if not LuaPoolMgr.m_PoolStoneList and not LuaPoolMgr.m_WarmPoolStoneList then
        return
    end
    local index,x,y
    for i, stone in pairs(LuaPoolMgr.m_PoolStoneList) do
        if stone.ro then
            index = stone.cachedIndex
            LuaPoolMgr.PutStoneInCaches(0,stone.borderCode,stone.ro,index)
            stone.ro = nil
        end
    end
    for i, stone in pairs(LuaPoolMgr.m_WarmPoolStoneList) do
        if stone.ro then
            index = stone.cachedIndex
            LuaPoolMgr.PutStoneInCaches(1,stone.borderCode,stone.ro,index)
            stone.ro = nil
        end
    end

    for code,cachlist in pairs(LuaPoolMgr.m_StoneCaches)do
        for index,cachtbl in pairs(cachlist) do
            for _,ro in ipairs(cachtbl) do
                ro:Destroy()
            end
        end 
    end

    for code,cachlist in pairs(LuaPoolMgr.m_WarmStoneCaches)do
        for index,cachtbl in pairs(cachlist) do
            for _,ro in ipairs(cachtbl) do
                ro:Destroy()
            end
        end 
    end
    LuaPoolMgr.m_PoolStoneList = {}
    LuaPoolMgr.m_WarmPoolStoneList = {}

    LuaPoolMgr.m_StoneCaches = {}
    LuaPoolMgr.m_WarmStoneCaches = {}

    LuaPoolMgr.m_StonePut = {}
    LuaPoolMgr.m_WarmStonePut = {}
    LuaPoolMgr.m_PoolList = {}
    LuaPoolMgr.m_WarmPoolList = {}
    LuaPoolMgr.Code2PlacedIndexOffset = nil
    LuaPoolMgr.m_Water = nil
end

function LuaPoolMgr.OnUpdateHousePool(houseId,poolud, warmpoolud,updateTerrainMesh)
    LuaPoolMgr.SetPoolDataDirty()
	local pool = CPoolType()
    local warmPool = CPoolType()

    if pool:LoadFromString(poolud) then
        CClientHouseMgr.Inst.mCurFurnitureProp2.Pool = pool
        CClientFurnitureMgr.Inst.m_Pool = pool
    end

    if warmPool:LoadFromString(warmpoolud) then
        CClientHouseMgr.Inst.mCurFurnitureProp2.WarmPool = warmPool
        CClientFurnitureMgr.Inst.m_WarmPool = warmPool
    end

    LuaPoolMgr.OnUpdateAllKindPool(updateTerrainMesh)
end

LuaPoolMgr.IsEditChanged = false

--编辑时，不需要重新生成地形
function LuaPoolMgr.OnUpdateAllKindPool(updateTerrainMesh)
    g_MessageMgr:ShowMessage("Save_HousePool_Succeed")
    LuaPoolMgr.m_PoolPointList = {}
	LuaPoolMgr.m_WarmPoolPointList = {}

    if not CRenderScene.Inst then
        return 
    end
    local mapWidth = CRenderScene.Inst.MapWidth
    local isMingYuan = CClientFurnitureMgr.Inst.m_IsMingYuan
	local basex, basey = GetPoolBase(isMingYuan,false)
    local heigh, width = GetPoolSize(isMingYuan)

	local terrainFast = CRenderScene.Inst.TerrainFast
    local poolMark = CClientFurnitureMgr.Inst.m_Pool.PoolMark
    local warmPoolMark = CClientFurnitureMgr.Inst.m_WarmPool.PoolMark

    CommonDefs.DictClear(terrainFast.clipDict)
    LuaPoolMgr.TerrianTypeList = {}
    LuaPoolMgr.CurPoolCount = 0
    local CHUNK_SIZE = CTerrainFast.CHUNK_SIZE
    local ChunkWidth = terrainFast.m_ChunkWidth
    for h = 0,heigh-1,1 do
        for w=0,width-1,1 do
            local i = h*width + w
            local ismark = poolMark:GetBit(i)
            local iswarmmark = warmPoolMark:GetBit(i)
            local y = h + basey
            local x = w + basex
            local cx = math.floor(x / CHUNK_SIZE)
            local cy = math.floor(y / CHUNK_SIZE)
            local chunkIndex = cx + cy*ChunkWidth
            local gridIndex = y * LuaPoolMgr.MapWidth + x

            if ismark or iswarmmark then     
                if not CommonDefs.DictContains(terrainFast.clipDict, typeof(Int32), chunkIndex) then
                    CommonDefs.DictAdd(terrainFast.clipDict, typeof(Int32), chunkIndex, typeof(MakeGenericClass(Dictionary, Int32, Boolean)), CreateFromClass(MakeGenericClass(Dictionary, Int32, Boolean)))
                end
                CommonDefs.DictAdd(terrainFast.clipDict[chunkIndex],typeof(Int32),gridIndex,typeof(Boolean),true)

                if iswarmmark then
                    LuaPoolMgr.TerrianTypeList[gridIndex] = LuaPoolMgr.WarmPoolType
                    table.insert(LuaPoolMgr.m_WarmPoolPointList,x)
                    table.insert(LuaPoolMgr.m_WarmPoolPointList,y)
                elseif ismark then
                    LuaPoolMgr.TerrianTypeList[gridIndex] = LuaPoolMgr.PoolType
                    table.insert(LuaPoolMgr.m_PoolPointList,x)
                    table.insert(LuaPoolMgr.m_PoolPointList,y)
                end
                LuaPoolMgr.CurPoolCount = LuaPoolMgr.CurPoolCount + 1
            else
                LuaPoolMgr.TerrianTypeList[gridIndex] = nil
            end
        end
    end

    --刷新温泉池装饰物的水池
    CClientFurnitureMgr.Inst:UpdateSpecialPoolRegion()

    if updateTerrainMesh then
        terrainFast:GenerateTerrainMesh()
    end
	LuaPoolMgr.GenBorder(0)
    LuaPoolMgr.GenBorder(1)
    LuaPoolMgr.UpdatePoolGridHelper()
    
    LuaPoolMgr.m_LandList = {}
    LuaPoolMgr.m_WarmLandList = {}
    LuaPoolMgr.m_PoolList = {}
    LuaPoolMgr.m_WarmPoolList = {}

    LuaPoolMgr.BackUpTerrianTypeList()
    LuaPoolMgr.SetPoolDataDirty()
    LuaPoolMgr.IsEditChanged = false
    g_ScriptEvent:BroadcastInLua("UpdateAllKindPool")
end

function LuaPoolMgr.UpdatePoolGridHelper()
    local colorTexture =PoolGridHelper.Inst.m_Texture
    if not colorTexture then
        return
    end

    local kuozhangLvl = CClientFurnitureMgr.Inst:GetKuoZhangLv()

    local notExtendedMask = 0
    local extOrders = {ERoadInfo.eRI_PLAY2, ERoadInfo.eRI_UNUSED6, ERoadInfo.eRI_HUNTING, ERoadInfo.eRI_UNUSED8, ERoadInfo.eRI_UNUSED9}
    for lvl = 1, 5 do
        if (kuozhangLvl-1) < lvl then
            notExtendedMask = bit.bor(notExtendedMask, bit.lshift(1, extOrders[lvl]))
        end
    end

    local checkKuozhangLvl = function(t)
            return bit.band(notExtendedMask, t) == 0
        end

    local width = colorTexture.width
    local height = colorTexture.height
    local basex, basey = PoolGridHelper.Inst.m_Basex,PoolGridHelper.Inst.m_Basey
    local helper = PoolGridHelper.Inst

    local barrierColor = PoolGridHelper.s_BarrierColor
    local waterColor = PoolGridHelper.s_WaterColor
    local landColor = PoolGridHelper.s_LandColor
    local coreScene = CoreScene.MainCoreScene
    for x=0,width-1,1 do
        for y=0,height-1,1 do
            local dx = x + basex
            local dy = y + basey

            local c = nil
            local roadinfo = coreScene:GetRoadInfo(dx, dy)
            if not checkKuozhangLvl(roadinfo) then--未开放
                c = barrierColor
            elseif CClientFurnitureMgr.Inst:IsPoolAt(dx,dy) or CClientFurnitureMgr.Inst:IsSpecialPoolAt(dx,dy) then
                c = waterColor
            else
                local barrierIgnorePoolBarrier = coreScene:GetBarriervIgnorePoolBarrier(dx, dy, false)
                barrierIgnorePoolBarrier = BarrierType2Int[barrierIgnorePoolBarrier]

                --原始障碍
                if barrierIgnorePoolBarrier~=0 then--动态障碍，非水池边缘造成的障碍
                    c = barrierColor
                elseif CClientFurnitureMgr.Inst:IsBridgeMustOnLandPart(dx,dy) then--桥两端不能刷
                    c = barrierColor
                elseif CClientFurnitureMgr.Inst:IsByWarmPoolStair(dx,dy) then
                    c = barrierColor
                else
                    colorTexture:SetPixel(x, y, PoolGridHelper.s_LandColor)
                    c = landColor
                end
            end
            if c then
                colorTexture:SetPixel(x, y, c)
            end
        end
    end
    colorTexture:Apply()

    LuaPoolMgr.RefreshIce()
end


function LuaPoolMgr.IsPoolEnterZswTemplate(id)
    return id == 9312
end
--导出水池数据

function LuaPoolMgr.ExportDesignPool(type)
    local fileName = "NormalPoolExport"
    if type == LuaPoolMgr.PoolType_Pool then--普通水池 2update
    elseif type == LuaPoolMgr.PoolType_WarmPool then--温泉水池 3
        fileName = "WarmPoolExport"
    end

    local pools = {}
    local marks = {}
    local setting = House_Setting.GetData()
    local isMingYuan = CClientFurnitureMgr.Inst.m_IsMingYuan
	local basex, basey
    local height, width
    if isMingYuan then
        basex, basey = setting.MingYuanPoolBase[0], setting.MingYuanPoolBase[1]
        height, width = setting.MingYuanPoolMaxSize[0], setting.MingYuanPoolMaxSize[1]
    else
        basex, basey = setting.NormalPoolBase[0], setting.NormalPoolBase[1]
        height, width = setting.PoolMaxSize[0], setting.PoolMaxSize[1]
    end

    local IsInArea = function(x,y)
        if marks[(y-1)*width + x] == true then
            return true
        end
        return false
    end
    if not CRenderScene.Inst then
        return
    end
    width = CRenderScene.Inst.MapWidth
    height = CRenderScene.Inst.MapHeight
    for x = 0,width,1 do
        for y = 0,height,1 do
            local isInPool = LuaPoolMgr.IsPoolAt(x,y,type)
            local isInArea = IsInArea(x,y)
            if (isInPool and not isInArea) then
                local x1,x2 = x,x
                local y1,y2 = y,y
                for xx = x1,width,1 do
                    if not LuaPoolMgr.IsPoolAt(xx,y,type) then
                        x2 = xx -1
                        break
                    end
                end
                for xx = x1,x2,1 do
                    for yy = y1,height,1 do
                        if not LuaPoolMgr.IsPoolAt(x,yy,type) then
                            if yy -1 < y2 then
                                y2 = yy -1
                            end
                        end
                    end
                end
                
                local area = {x1,y1,x2,y2}
                table.insert(pools,area)
                for i = x1,x2,1 do
                    for j = y1,y2,1 do
                        marks[(j-1)*width + i] = true
                    end
                end
            end
        end
    end
    local str = ""
    for _,area in ipairs(pools) do
        for i,v in ipairs(area) do
            str = str..v
            if i<4 then
                str = str..","
            else
                str = str..";"
            end
        end
    end
    CPlayerDataMgr.Inst:SavePlayerData(fileName,str)
end

--客户端缓存了一些家园是否水池、以及是否是特殊障碍点（比如桥和台阶可以改变水池边缘障碍）的数据
--当相关数据需要变更，需要清一下cache
function LuaPoolMgr.SetPoolDataDirty()
    CClientFurnitureMgr.Inst:SetPoolDataDirty()
end

--收费
LuaPoolMgr.NeedStoneCount = 0
LuaPoolMgr.NewMaxCount = 0
LuaPoolMgr.AddCount = 0
function LuaPoolMgr.OpenBuyPoolComfirmWnd(needStone, newMaxCount,addCount)
    LuaPoolMgr.NeedStoneCount = needStone
    LuaPoolMgr.NewMaxCount = newMaxCount
    LuaPoolMgr.AddCount = addCount
    CUIManager.ShowUI(CLuaUIResources.BuyPoolComfirmWnd)
end

--模板缓存池
LuaPoolMgr.m_StoneCaches = {}
LuaPoolMgr.m_WarmStoneCaches = {}

function LuaPoolMgr.PutStoneInCaches(poolType,code,stoneRo,posIndex)
    local cached = poolType == 0 and LuaPoolMgr.m_StoneCaches or LuaPoolMgr.m_WarmStoneCaches
    if not cached then
        cached = {}
    end

    local tbl
    if code <=7 then
        tbl = LuaPoolMgr.Code2Template[code]
    else
        tbl = LuaPoolMgr.Code2Path[code]
    end

    if not cached[code] then
        cached[code] = {}
        for i=1,#tbl,1 do 
            table.insert(cached[code],{})
        end
    end
    
    local index = 1
    if poolType == 0 then
        if #tbl > 1 then
            index = posIndex % 2 + 1
        end
    end
    
    stoneRo.gameObject:SetActive(false)
    stoneRo.transform.parent = LuaPoolMgr.m_StonePool.transform
    table.insert(cached[code][index],stoneRo)
end

function LuaPoolMgr.GetStoneFromCaches(poolType,code,posIndex)
    code = tonumber(code)
    local caches =  poolType == 0 and LuaPoolMgr.m_StoneCaches or LuaPoolMgr.m_WarmStoneCaches
    local stoneRo
    local stones
    local path
    local index = posIndex%2 + 1
    if poolType == 1 then
        index = 1
    else
        if LuaPoolMgr.Code2Template[code] then
            index = #LuaPoolMgr.Code2Template[code] > 1 and index or 1
        else
            index = #LuaPoolMgr.Code2Path[code] > 1 and index or 1
        end
    end
    

    if caches then
        stones = caches[code]
    end
     
    if stones and stones[index] and next(stones[index]) then
        local count = #stones[index]
        stoneRo = table.remove(stones[index],count)
    end

    if not stoneRo then
        local housename
        if poolType == 0 then--普通水池
            housename = "jyputong"
        else
            housename = "jymingyuan"
        end
        local root = CClientFurnitureMgr.sPoolStoneRoot.transform
        
        if code <= 7 then
            local tbl = LuaPoolMgr.Code2Template[code]       
            local broaderCode = tbl[index]        
            path = SafeStringFormat3("Character/Jiayuan/%s_shuichi_%02d/Prefab/%s_shuichi_%02d_01.prefab",housename,broaderCode,housename,broaderCode)
        else
            local tbl2 = LuaPoolMgr.Code2Path[code]
            path = tbl2[index]
            path = SafeStringFormat3(path,housename,housename)
        end
        
        local ro = CRenderObject.CreateRenderObject(root.gameObject, "shuichi")
        -- if CCullingMgr.s_EnableCulling then
        --     ro.CanCulling = true
        -- end
        ro:LoadMain(path,nil,false,false)
        ro.Layer = LayerDefine.WaterReflection
        --stoneRo = ro.gameObject
        -- return ro
        stoneRo = ro
    else
        stoneRo.gameObject:SetActive(true)
        --stoneRo:SetActive(true)
    end
    stoneRo:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(renderObject)
        if LuaPoolMgr.m_IsWinterStyle then
            CClientFurniture.SetSnowLevel(stoneRo,true,0)
        else
            CClientFurniture.SetSnowLevel(stoneRo,false,0)
        end
    end))
    return stoneRo
end


function LuaPoolMgr.SavePoolAppearanceToString()
    local isMingyuan = CClientFurnitureMgr.Inst.m_IsMingYuan
	local str = ""
	--pool
	--warmpool
	local basex, basey
    local height, width
	local wbasex,wbasey
	if isMingyuan then
		basex, basey = GetPoolBase(true,false)
		height, width = GetPoolSize(true)
		wbasex,wbasey = GetPoolBase(true,true)
	else
		basex, basey = GetPoolBase(false,false)
		height, width = GetPoolSize(false)
        wbasex,wbasey = GetPoolBase(false,true)
	end

    local pool = CClientFurnitureMgr.Inst.m_Pool.PoolMark
    local warmPool = CClientFurnitureMgr.Inst.m_WarmPool.PoolMark

	local function exportPoolStr(iswarm)
		local maxx = (iswarm and wbasex or basex)+width-1
		local maxy = (iswarm and wbasey or basey)+height-1

		local pools = {}
		local marks = {}

		local IsInArea = function(x,y)
			if marks[y*1000 + x] == true then
				return true
			end
			return false
		end
		local IsPoolAt = function(x,y)
			local ret = false
			local idx = 0
			if iswarm then
				idx = (y - wbasey) * width + (x - wbasex)
				if idx<width*height then
					ret = warmPool:GetBit(idx)
				end
			else
				idx = (y - basey) * width + (x - basex)
				if idx<width*height then
					ret = pool:GetBit(idx)
				end
			end
			return ret
		end

		for h = 0,height-1,1 do
			for w = 0,width-1,1 do
				local y =h + (iswarm and wbasey or basey)
				local x = w + (iswarm and wbasex or basex)
				local isInPool = IsPoolAt(x,y)
				local isInArea = IsInArea(x,y)
				if isInPool and not isInArea then
					local x1,x2 = x,x
					for xx = x+1,maxx do
						if IsPoolAt(xx,y) then
							x2 = xx
						else
							break
						end
					end
					local area = SafeStringFormat3("%d,%d,%d,%d;",x1,y,x2,y)
                	table.insert(pools,area)
					for i = x1,x2,1 do
						marks[y*1000 + i] = true
					end
				end
			end
		end

		local str = table.concat(pools,'')
		return str
	end

	local str1 = exportPoolStr(false)
	local str2 = isMingyuan and exportPoolStr(true) or ""
    return str1,str2
end

function LuaPoolMgr.LoadPoolAppearanceFromString(poolstr,warmpoolstr)
    local isMingyuan = CClientFurnitureMgr.Inst.m_IsMingYuan

    local function parse_pool_region(pool, regionStr, bx, by, width)
        if not regionStr then return end
        local marks = {}
        for sx, sy, ex, ey in string.gmatch(regionStr, "(%d+),(%d+),(%d+),(%d+);") do
            sx, sy, ex, ey = tonumber(sx), tonumber(sy), tonumber(ex), tonumber(ey)
            -- assert(sx and sy and ex and ey)
            sx, ex = math.min(sx, ex), math.max(sx, ex)
            sy, ey = math.min(sy, ey), math.max(sy, ey)
            for x = sx, ex do 
                for y = sy, ey do
                    local idx = (y-by)*width + (x-bx)
                    if not pool:GetBit(idx) then
                        table.insert( marks, {x,y} )
                        -- print(x,y)
                    end
                end
            end
        end
        return marks
    end

    local heigh, width = GetPoolSize(isMingyuan)
    local bx,by
    if isMingyuan then
        bx,by = GetPoolBase(true,false)
    else
        bx,by = GetPoolBase(false,false)
    end

    local pool = CClientFurnitureMgr.Inst.m_Pool.PoolMark

    local marks = parse_pool_region(pool, poolstr, bx, by, width)
    local warmmarks = nil
    if isMingyuan then
        local warmPool = CClientFurnitureMgr.Inst.m_WarmPool.PoolMark
        if isMingyuan then
            bx,by = GetPoolBase(true,true)
        else
            bx,by = GetPoolBase(false,true)
        end
        warmmarks = parse_pool_region(warmPool, warmpoolstr, bx, by, width)
    end


	return marks,warmmarks
end

function LuaPoolMgr.RequestLoadPool(poolstr,warmpoolstr)
    if not poolstr then return end
    --室内不加载水池
    local sceneName = CoreScene.MainCoreScene.SceneName
    if StringStartWith(sceneName,"jiayuanptsn") or StringStartWith(sceneName,"jiayuanmysn") then
        return
    end

    Gac2Gas.RequestEditPool(true)
    Gac2Gas.RequestSetPoolBegin()

    local normalmarks,warmmarks = LuaPoolMgr.LoadPoolAppearanceFromString(poolstr,warmpoolstr)

    local convert = function(marks)
        local SegmentCount = 60
        local t = {}
        for i=1,math.ceil(#marks/SegmentCount) do
            local list = CreateFromClass(MakeGenericClass(List, Object))
            local listx = CreateFromClass(MakeGenericClass(List, UInt32))
            local listy = CreateFromClass(MakeGenericClass(List, UInt32))
        
            for j=1,SegmentCount do
                local mark = marks[(i-1)*SegmentCount+j]
                if mark then
                    -- print(mark[1],mark[2])
                    CommonDefs.ListAdd(listx, typeof(UInt32), math.floor(mark[1]))
                    CommonDefs.ListAdd(listy, typeof(UInt32), math.floor(mark[2]))
                end
            end
            CommonDefs.ListAdd(list, typeof(Object), CommonDefs.ListConvert(listx))
            CommonDefs.ListAdd(list, typeof(Object), CommonDefs.ListConvert(listy))

            table.insert( t,MsgPackImpl.pack(list) )
        end
        return t
    end

    local x = CreateFromClass(MakeGenericClass(List, Object))
    local y = CreateFromClass(MakeGenericClass(List, Object))
    local empty = CreateFromClass(MakeGenericClass(List, Object))
    CommonDefs.ListAdd(empty, typeof(Object), x)
    CommonDefs.ListAdd(empty, typeof(Object), y)
    local u = MsgPackImpl.pack(empty)
    
    local t1 = convert(normalmarks)
    for i=1,math.ceil(#t1/4) do
        local arg1 = t1[(i-1)*4+1] and t1[(i-1)*4+1] or u
        local arg2 = t1[(i-1)*4+2] and t1[(i-1)*4+2] or u
        local arg3 = t1[(i-1)*4+3] and t1[(i-1)*4+3] or u
        local arg4 = t1[(i-1)*4+4] and t1[(i-1)*4+4] or u
        Gac2Gas.RequestSetPool(arg1,arg2,arg3,arg4)
    end

    if warmmarks and warmmarks~="" then
        local t2 = convert(warmmarks)
        for i=1,math.ceil(#t2/4) do
            local arg1 = t2[(i-1)*4+1] and t2[(i-1)*4+1] or u
            local arg2 = t2[(i-1)*4+2] and t2[(i-1)*4+2] or u
            local arg3 = t2[(i-1)*4+3] and t2[(i-1)*4+3] or u
            local arg4 = t2[(i-1)*4+4] and t2[(i-1)*4+4] or u
            Gac2Gas.RequestSetWarmPool(arg1,arg2,arg3,arg4)
        end
    end

    Gac2Gas.RequestSetPoolEnd()
    Gac2Gas.RequestCancelEditPool()
end
