require("3rdParty/ScriptEvent")
require("common/common_include")
local QnTabView = import "L10.UI.QnTabView"
local ClientMainPlayer = import"L10.Game.CClientMainPlayer"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local EnumPosition = import "L10.Game.CTeamGroupMgr.EnumTeamGroupMemberPosition"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"

CLuaTeamGroupWnd = class()

RegistClassMember(CLuaTeamGroupWnd,"DetailTab")
RegistClassMember(CLuaTeamGroupWnd,"ApplicationTab")

function CLuaTeamGroupWnd:Init()
	local tabView = self.gameObject:GetComponent(typeof(QnTabView))
	self.DetailTab = tabView.m_TabButtons[0].gameObject
	self.ApplicationTab = tabView.m_TabButtons[1].gameObject
	tabView:ChangeTo(0)

	EventManager.Broadcast(EnumEventType.TeamInfoChange)
	self:showTabs()
	--用于判断右键玩家是否显示城战管理
	Gac2Gas.CheckMyRights("CityBuild", 0)
end

function CLuaTeamGroupWnd:OnEnable()
	g_ScriptEvent:AddListener("TeamGroupSelfPositionChange", self, "showTabs")
	g_ScriptEvent:AddListener("MainPlayerCreated", self, "showTabs")
end

function CLuaTeamGroupWnd:OnDisable()
	g_ScriptEvent:RemoveListener("TeamGroupSelfPositionChange", self, "showTabs")
	g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "showTabs")
end
function CLuaTeamGroupWnd:showTabs()
	if ClientMainPlayer.Inst ~= nil then
		local Pos = CTeamGroupMgr.Instance:GetPlayerPos(ClientMainPlayer.Inst.Id)
		local isManager = (Pos == EnumPosition.Leader or Pos == EnumPosition.Manager)
		self.ApplicationTab:SetActive(isManager)
	end
end
return CLuaTeamGroupWnd
