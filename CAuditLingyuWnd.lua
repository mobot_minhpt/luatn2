-- Auto Generated!!
local CAuditLingyuWnd = import "L10.UI.CAuditLingyuWnd"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CAuditLingyuWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.detailButton).onClick = MakeDelegateFromCSFunction(this.onDetailButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.withdrawButton).onClick = MakeDelegateFromCSFunction(this.onWithdrawButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.onCloseButtonClick, VoidDelegate, this)
    this:show()
end
