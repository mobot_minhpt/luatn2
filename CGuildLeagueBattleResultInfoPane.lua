-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuildLeagueBattleResultInfoPane = import "L10.UI.CGuildLeagueBattleResultInfoPane"
local CGuildLeagueBattleResultItem = import "L10.UI.CGuildLeagueBattleResultItem"
local CGuildLeagueInfoItem = import "L10.UI.CGuildLeagueInfoItem"
local CommonDefs = import "L10.Game.CommonDefs"
local CSortButton = import "L10.UI.CSortButton"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local ESortType = import "L10.UI.CGuildLeagueBattleResultInfoPane+ESortType"
local GameObject = import "UnityEngine.GameObject"
local GuildLeaguePlayerInfo = import "L10.Game.GuildLeaguePlayerInfo"
local LocalString = import "LocalString"
local String = import "System.String"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local Vector3 = import "UnityEngine.Vector3"
CGuildLeagueBattleResultInfoPane.m_Awake_CS2LuaHook = function (this)
    this.nameLabel.text = ""
    this.itemPrefab:SetActive(false)
    this.listTf.gameObject:SetActive(true)
    LuaGuildLeagueBattleResultInfoPane:OnAwake(this)
end
CGuildLeagueBattleResultInfoPane.m_CompareByDefault_CS2LuaHook = function (this, p1, p2) 
    local default
    if CClientMainPlayer.Inst ~= nil then
        default = CClientMainPlayer.Inst.Name
    else
        default = ""
    end
    local myName = default

    if p1.playerName == myName then
        return - 1
    elseif p2.playerName == myName then
        return 1
    elseif p1.killCount > p2.killCount then
        return - 1
    elseif p1.killCount < p2.killCount then
        return 1
    elseif p1.deadCount < p2.deadCount then
        return - 1
    elseif p1.deadCount > p2.deadCount then
        return 1
    elseif p1.level > p2.level then
        return - 1
    elseif p1.level < p2.level then
        return 1
    elseif p1.dpsNum > p2.dpsNum then
        return - 1
    elseif p1.dpsNum < p2.dpsNum then
        return 1
    elseif p1.underDamage > p2.underDamage then
        return - 1
    elseif p1.underDamage < p2.underDamage then
        return 1
    else
        return 0
    end
end
CGuildLeagueBattleResultInfoPane.m_CompareByLv_CS2LuaHook = function (this, p1, p2) 
    --1：从大倒小 -1：从小到大
    local flag = this.sortFlags[ESortType_lua.Level]
    --1：从大倒小 -1：从小到大
    local default
    if CClientMainPlayer.Inst ~= nil then
        default = CClientMainPlayer.Inst.Name
    else
        default = ""
    end
    local myName = default
    if CGuildLeagueBattleResultInfoPane.SELF_FIRST then
        if p1.playerName == myName then
            return - 1
        elseif p2.playerName == myName then
            return 1
        end
    end
    if p1.level > p2.level then
        return - flag
    elseif p1.level < p2.level then
        return flag
    else
        return this:CompareByDefault(p1, p2)
    end
end
CGuildLeagueBattleResultInfoPane.m_CompareByKillNum_CS2LuaHook = function (this, p1, p2) 
    local flag = this.sortFlags[ESortType_lua.KillNum]

    local default
    if CClientMainPlayer.Inst ~= nil then
        default = CClientMainPlayer.Inst.Name
    else
        default = ""
    end
    local myName = default
    if CGuildLeagueBattleResultInfoPane.SELF_FIRST then
        if p1.playerName == myName then
            return - 1
        elseif p2.playerName == myName then
            return 1
        end
    end
    if p1.killCount > p2.killCount then
        return - flag
    elseif p1.killCount < p2.killCount then
        return flag
    else
        return this:CompareByDefault(p1, p2)
    end
end
CGuildLeagueBattleResultInfoPane.m_CompareByDieNum_CS2LuaHook = function (this, p1, p2) 
    local flag = this.sortFlags[ESortType_lua.DieNum]

    local default
    if CClientMainPlayer.Inst ~= nil then
        default = CClientMainPlayer.Inst.Name
    else
        default = ""
    end
    local myName = default
    if CGuildLeagueBattleResultInfoPane.SELF_FIRST then
        if p1.playerName == myName then
            return - 1
        elseif p2.playerName == myName then
            return 1
        end
    end
    if p1.deadCount > p2.deadCount then
        return - flag
    elseif p1.deadCount < p2.deadCount then
        return flag
    else
        return this:CompareByDefault(p1, p2)
    end
end
CGuildLeagueBattleResultInfoPane.m_CompareByBaoShi_CS2LuaHook = function (this, p1, p2) 
    local flag = this.sortFlags[ESortType_lua.BaoShiNum]

    local default
    if CClientMainPlayer.Inst ~= nil then
        default = CClientMainPlayer.Inst.Name
    else
        default = ""
    end
    local myName = default

    if CGuildLeagueBattleResultInfoPane.SELF_FIRST then
        if p1.playerName == myName then
            return - 1
        elseif p2.playerName == myName then
            return 1
        end
    end
    if p1.baoshiCount > p2.baoshiCount then
        return - flag
    elseif p1.baoshiCount < p2.baoshiCount then
        return flag
    else
        return this:CompareByDefault(p1, p2)
    end
end
CGuildLeagueBattleResultInfoPane.m_CompareByFuhuo_CS2LuaHook = function (this, p1, p2) 
    local flag = this.sortFlags[ESortType_lua.FuHuoNum]

    local default
    if CClientMainPlayer.Inst ~= nil then
        default = CClientMainPlayer.Inst.Name
    else
        default = ""
    end
    local myName = default
    if CGuildLeagueBattleResultInfoPane.SELF_FIRST then
        if p1.playerName == myName then
            return - 1
        elseif p2.playerName == myName then
            return 1
        end
    end
    if p1.fuhuoCount > p2.fuhuoCount then
        return - flag
    elseif p1.fuhuoCount < p2.fuhuoCount then
        return flag
    else
        return this:CompareByDefault(p1, p2)
    end
end
CGuildLeagueBattleResultInfoPane.m_CompareByControl_CS2LuaHook = function (this, p1, p2) 
    local flag = this.sortFlags[ESortType_lua.ControlNum]

    local default
    if CClientMainPlayer.Inst ~= nil then
        default = CClientMainPlayer.Inst.Name
    else
        default = ""
    end
    local myName = default
    if CGuildLeagueBattleResultInfoPane.SELF_FIRST then
        if p1.playerName == myName then
            return - 1
        elseif p2.playerName == myName then
            return 1
        end
    end
    if p1.controlCount > p2.controlCount then
        return - flag
    elseif p1.controlCount < p2.controlCount then
        return flag
    else
        return this:CompareByDefault(p1, p2)
    end
end
CGuildLeagueBattleResultInfoPane.m_CompareByRmcontrol_CS2LuaHook = function (this, p1, p2) 
    local flag = this.sortFlags[ESortType_lua.RmControlNum]

    local default
    if CClientMainPlayer.Inst ~= nil then
        default = CClientMainPlayer.Inst.Name
    else
        default = ""
    end
    local myName = default
    if CGuildLeagueBattleResultInfoPane.SELF_FIRST then
        if p1.playerName == myName then
            return - 1
        elseif p2.playerName == myName then
            return 1
        end
    end
    if p1.rmControlCount > p2.rmControlCount then
        return - flag
    elseif p1.rmControlCount < p2.rmControlCount then
        return flag
    else
        return this:CompareByDefault(p1, p2)
    end
end
CGuildLeagueBattleResultInfoPane.m_CompareByRepairNum_CS2LuaHook = function (this, p1, p2) 
    local flag = this.sortFlags[ESortType_lua.RepairNum]

    local default
    if CClientMainPlayer.Inst ~= nil then
        default = CClientMainPlayer.Inst.Name
    else
        default = ""
    end
    local myName = default
    if CGuildLeagueBattleResultInfoPane.SELF_FIRST then
        if p1.playerName == myName then
            return - 1
        elseif p2.playerName == myName then
            return 1
        end
    end
    if p1.repairCount > p2.repairCount then
        return - flag
    elseif p1.repairCount < p2.repairCount then
        return flag
    else
        return this:CompareByDefault(p1, p2)
    end
end
CGuildLeagueBattleResultInfoPane.m_CompareByDpsNum_CS2LuaHook = function (this, p1, p2) 
    local flag = this.sortFlags[ESortType_lua.DpsNum]

    local default
    if CClientMainPlayer.Inst ~= nil then
        default = CClientMainPlayer.Inst.Name
    else
        default = ""
    end
    local myName = default
    if CGuildLeagueBattleResultInfoPane.SELF_FIRST then
        if p1.playerName == myName then
            return - 1
        elseif p2.playerName == myName then
            return 1
        end
    end
    if p1.dpsNum > p2.dpsNum then
        return - flag
    elseif p1.dpsNum < p2.dpsNum then
        return flag
    else
        return this:CompareByDefault(p1, p2)
    end
end
CGuildLeagueBattleResultInfoPane.m_CompareByHealNum_CS2LuaHook = function (this, p1, p2) 
    local flag = this.sortFlags[ESortType_lua.HealNum]

    local default
    if CClientMainPlayer.Inst ~= nil then
        default = CClientMainPlayer.Inst.Name
    else
        default = ""
    end
    local myName = default
    if CGuildLeagueBattleResultInfoPane.SELF_FIRST then
        if p1.playerName == myName then
            return - 1
        elseif p2.playerName == myName then
            return 1
        end
    end
    if p1.healNum > p2.healNum then
        return - flag
    elseif p1.healNum < p2.healNum then
        return flag
    else
        return this:CompareByDefault(p1, p2)
    end
end
CGuildLeagueBattleResultInfoPane.m_CompareByUnderDamage_CS2LuaHook = function (this, p1, p2) 
    local flag = this.sortFlags[ESortType_lua.UnderDamage]

    local default
    if CClientMainPlayer.Inst ~= nil then
        default = CClientMainPlayer.Inst.Name
    else
        default = ""
    end
    local myName = default
    if CGuildLeagueBattleResultInfoPane.SELF_FIRST then
        if p1.playerName == myName then
            return - 1
        elseif p2.playerName == myName then
            return 1
        end
    end
    if p1.underDamage > p2.underDamage then
        return - flag
    elseif p1.underDamage < p2.underDamage then
        return flag
    else
        return this:CompareByDefault(p1, p2)
    end
end
CGuildLeagueBattleResultInfoPane.m_Sort_CS2LuaHook = function (this, list, type, selfFirst) 
    CGuildLeagueBattleResultInfoPane.SELF_FIRST = selfFirst
    if list == nil then
        return
    end
    repeat
        local default = type
        if default == ESortType.Level then
            CommonDefs.ListSort1(list, typeof(GuildLeaguePlayerInfo), MakeDelegateFromCSFunction(this.CompareByLv, MakeGenericClass(Comparison, GuildLeaguePlayerInfo), this))
            break
        elseif default == ESortType.KillNum then
            CommonDefs.ListSort1(list, typeof(GuildLeaguePlayerInfo), MakeDelegateFromCSFunction(this.CompareByKillNum, MakeGenericClass(Comparison, GuildLeaguePlayerInfo), this))
            break
        elseif default == ESortType.DieNum then
            CommonDefs.ListSort1(list, typeof(GuildLeaguePlayerInfo), MakeDelegateFromCSFunction(this.CompareByDieNum, MakeGenericClass(Comparison, GuildLeaguePlayerInfo), this))
            break
        elseif default == ESortType.BaoShiNum then
            CommonDefs.ListSort1(list, typeof(GuildLeaguePlayerInfo), MakeDelegateFromCSFunction(this.CompareByBaoShi, MakeGenericClass(Comparison, GuildLeaguePlayerInfo), this))
            break
        elseif default == ESortType.FuHuoNum then
            CommonDefs.ListSort1(list, typeof(GuildLeaguePlayerInfo), MakeDelegateFromCSFunction(this.CompareByFuhuo, MakeGenericClass(Comparison, GuildLeaguePlayerInfo), this))
            break
        elseif default == ESortType.ControlNum then
            CommonDefs.ListSort1(list, typeof(GuildLeaguePlayerInfo), MakeDelegateFromCSFunction(this.CompareByControl, MakeGenericClass(Comparison, GuildLeaguePlayerInfo), this))
            break
        elseif default == ESortType.RmControlNum then
            CommonDefs.ListSort1(list, typeof(GuildLeaguePlayerInfo), MakeDelegateFromCSFunction(this.CompareByRmcontrol, MakeGenericClass(Comparison, GuildLeaguePlayerInfo), this))
            break
        elseif default == ESortType.DpsNum then
            CommonDefs.ListSort1(list, typeof(GuildLeaguePlayerInfo), MakeDelegateFromCSFunction(this.CompareByDpsNum, MakeGenericClass(Comparison, GuildLeaguePlayerInfo), this))
            break
        elseif default == ESortType.UnderDamage then
            CommonDefs.ListSort1(list, typeof(GuildLeaguePlayerInfo), MakeDelegateFromCSFunction(this.CompareByUnderDamage, MakeGenericClass(Comparison, GuildLeaguePlayerInfo), this))
            break
        elseif default == ESortType.HealNum then
            CommonDefs.ListSort1(list, typeof(GuildLeaguePlayerInfo), MakeDelegateFromCSFunction(this.CompareByHealNum, MakeGenericClass(Comparison, GuildLeaguePlayerInfo), this))
            break
        elseif default == ESortType.RepairNum then
            CommonDefs.ListSort1(list, typeof(GuildLeaguePlayerInfo), MakeDelegateFromCSFunction(this.CompareByRepairNum, MakeGenericClass(Comparison, GuildLeaguePlayerInfo), this))
            break
        elseif default == ESortType.Default then
            CommonDefs.ListSort1(list, typeof(GuildLeaguePlayerInfo), MakeDelegateFromCSFunction(this.CompareByDefault, MakeGenericClass(Comparison, GuildLeaguePlayerInfo), this))
            break
        end
    until 1
end
CGuildLeagueBattleResultInfoPane.m_findMaxValue_CS2LuaHook = function (this, type) 
    if this.playerInfos == nil or this.playerInfos.Count == 0 then
        return nil
    end
    local maxValPlayer = this.playerInfos[0]
    local maxVal = 0
    if type == ESortType.KillNum then
        maxVal = maxValPlayer.killCount
    elseif type == ESortType.ControlNum then
        maxVal = maxValPlayer.controlCount
    elseif type == ESortType.HealNum then
        maxVal = maxValPlayer.healNum
    elseif type == ESortType.BaoShiNum then
        maxVal = maxValPlayer.baoshiCount
    elseif type == ESortType.RepairNum then
        maxVal = maxValPlayer.repairCount
    end

    do
        local i = 0
        while i < this.playerInfos.Count do
            local player = this.playerInfos[i]
            if type == ESortType.KillNum then
                if player.killCount > maxValPlayer.killCount then
                    maxValPlayer = player
                    maxVal = player.killCount
                end
            elseif type == ESortType.ControlNum then
                if player.controlCount > maxValPlayer.controlCount then
                    maxValPlayer = player
                    maxVal = player.controlCount
                end
            elseif type == ESortType.HealNum then
                if player.healNum > maxValPlayer.healNum then
                    maxValPlayer = player
                    maxVal = player.healNum
                end
            elseif type == ESortType.BaoShiNum then
                if player.baoshiCount > maxValPlayer.baoshiCount then
                    maxValPlayer = player
                    maxVal = player.baoshiCount
                end
            elseif type == ESortType.RepairNum then
                if player.repairCount > maxValPlayer.repairCount then
                    maxValPlayer = player
                    maxVal = player.repairCount
                end
            end
            i = i + 1
        end
    end
    if maxVal == 0 then
        return nil
    end
    return maxValPlayer
end
CGuildLeagueBattleResultInfoPane.m_OnRadioBoxSelect_CS2LuaHook = function (this, btn, index) 
    local sortButton = TypeAs(btn, typeof(CSortButton))

    this.sortFlags[index] = - this.sortFlags[index]
    do
        local i = 0
        while i < this.sortFlags.Length do
            if i ~= index then
                this.sortFlags[i] = - 1
            end
            i = i + 1
        end
    end
    sortButton:SetSortTipStatus(this.sortFlags[index] < 0)
    this:Sort(this.playerInfos, CommonDefs.ConvertIntToEnum(typeof(ESortType), (index)), true)
    this.tableView:ReloadData(true, false)
end
CGuildLeagueBattleResultInfoPane.m_Init_GuildLeagueMainBattleResultInfo_Boolean_CS2LuaHook = function (this, info, isLeft) 
    this.isMain = true
    this.playerInfos = info.playerInfos

    --playerInfosForSort = new List<GuildLeaguePlayerInfo>();
    --playerInfosForSort.AddRange(playerInfos);

    this.nameLabel.text = info.name
    LuaGuildLeagueBattleResultInfoPane.InitServerNameAndResultInfo(this, info)
    local dic = CreateFromClass(MakeGenericClass(Dictionary, String, String))
    this.isJiangJunWei = isLeft

    if isLeft then
        CommonDefs.DictAdd(dic, typeof(String), LocalString.GetString("将军威耐久"), typeof(String), System.String.Format("{0}/{1}", info.hp, info.maxHp))
    else
        CommonDefs.DictAdd(dic, typeof(String), LocalString.GetString("玄霄怒耐久"), typeof(String), System.String.Format("{0}/{1}", info.hp, info.maxHp))
    end

    CommonDefs.DictAdd(dic, typeof(String), LocalString.GetString("主战场人数"), typeof(String), tostring(info.mainScenePlayerNum))
    local killCount = 0
    CommonDefs.ListIterate(this.playerInfos, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        killCount = killCount + item.killCount
    end))
    CommonDefs.DictAdd(dic, typeof(String), LocalString.GetString("主战场灭敌数"), typeof(String), tostring(killCount))
    CommonDefs.DictAdd(dic, typeof(String), LocalString.GetString("当前箭塔数"), typeof(String), tostring(info.towerNum))

    CUICommonDef.ClearTransform(this.grid.transform)
    CommonDefs.DictIterate(dic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local item = {}
        item.Key = ___key
        item.Value = ___value
        local obj = TypeAs(CommonDefs.Object_Instantiate(this.itemPrefab), typeof(GameObject))
        obj.transform.parent = this.grid.transform
        obj.transform.localScale = Vector3.one
        obj:SetActive(true)
        local cmp = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CGuildLeagueInfoItem))
        cmp:Init(item.Key, item.Value)
    end))
    this.grid:Reposition()

    --默认排序
    this.radioBox:ChangeTo(- 1, true)
    this:Sort(this.playerInfos, ESortType.Default, true)
    this:showBattleFieldOtherInfo()
    this.tableView.m_DataSource = this
    this.tableView:ReloadData(true, false)
end
CGuildLeagueBattleResultInfoPane.m_showBattleFieldOtherInfo_CS2LuaHook = function (this) 
    this.Line1:SetActive(this.isMain)
    this.Line2:SetActive(not this.isMain)
    do
        local i = 0
        while i < this.infoList.Count do
            local go = this.infoList[i]
            repeat
                local default = i
                if default == 0 then
                    go:SetActive(this.isMain)
                    if this.isMain then
                        this:setOtherInfoValue(go, ESortType.KillNum)
                    end
                    break
                elseif default == 1 then
                    go:SetActive(true)
                    if this.isMain then
                        this:setOtherInfoValue(go, ESortType.ControlNum)
                    else
                        this:setOtherInfoValue(go, ESortType.KillNum)
                    end
                    break
                elseif default == 2 then
                    go:SetActive(this.isMain)
                    if this.isMain then
                        this:setOtherInfoValue(go, ESortType.BaoShiNum)
                    end
                    break
                elseif default == 3 then
                    go:SetActive(true)
                    if this.isMain then
                        this:setOtherInfoValue(go, ESortType.HealNum)
                    else
                        this:setOtherInfoValue(go, ESortType.RepairNum)
                    end
                    break
                end
            until 1
            i = i + 1
        end
    end
end
CGuildLeagueBattleResultInfoPane.m_setOtherInfoValue_CS2LuaHook = function (this, go, type) 

    local sprite = CommonDefs.GetComponentInChildren_GameObject_Type(go, typeof(UISprite))
    local label = CommonDefs.GetComponentInChildren_GameObject_Type(go, typeof(UILabel))
    local player = this:findMaxValue(type)
    local str = LocalString.GetString("无")
    local spriteName = ""
    local v = 0
    repeat
        local default = type
        if default == ESortType.KillNum then
            if player ~= nil then
                v = player.killCount
            end
            spriteName = "common_zhankuangtongji_sha"
            break
        elseif default == ESortType.ControlNum then
            if player ~= nil then
                v = player.controlCount
            end
            spriteName = "common_zhankuangtongji_kong"
            break
        elseif default == ESortType.BaoShiNum then
            if player ~= nil then
                v = player.baoshiCount
            end
            spriteName = "common_zhankuangtongji_bao"
            break
        elseif default == ESortType.HealNum then
            if player ~= nil then
                v = player.healNum
            end
            spriteName = "common_zhankuangtongji_zhi"
            break
        elseif default == ESortType.RepairNum then
            if player ~= nil then
                v = player.repairCount
            end
            spriteName = "common_zhankuangtongji_xiu"
            break
        end
    until 1
    if label then
        local extern
        if v > 0 then
            extern = player.playerName
        else
            extern = LocalString.GetString("无")
        end
        label.text = extern
    end
    if sprite then
        sprite.spriteName = spriteName
    end
end
CGuildLeagueBattleResultInfoPane.m_Init_GuildLeagueViceBattleResultInfo_Boolean_CS2LuaHook = function (this, info, isLeft) 
    --weixiuTf.gameObject.SetActive(true);
    this.isMain = false

    this.isJiangJunWei = isLeft

    this.playerInfos = info.playerInfos

    --playerInfosForSort = new List<GuildLeaguePlayerInfo>();
    --playerInfosForSort.AddRange(playerInfos);

    this.nameLabel.text = info.name
    LuaGuildLeagueBattleResultInfoPane.InitServerNameAndResultInfo(this, info)
    local dic = CreateFromClass(MakeGenericClass(Dictionary, String, String))
    if isLeft then
        CommonDefs.DictAdd(dic, typeof(String), LocalString.GetString("将军威耐久"), typeof(String), System.String.Format("{0}/{1}", info.hp, info.maxHp))
    else
        CommonDefs.DictAdd(dic, typeof(String), LocalString.GetString("玄霄怒耐久"), typeof(String), System.String.Format("{0}/{1}", info.hp, info.maxHp))
    end

    CommonDefs.DictAdd(dic, typeof(String), LocalString.GetString("副战场人数"), typeof(String), tostring(info.viceScenePlayerNum))
    local name = ""
    local maxKillNum = - 1
    local maxRepairNum = - 1
    local repairName = ""
    local killCount = 0
    local repairCount = 0
    CommonDefs.ListIterate(this.playerInfos, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        if maxKillNum < item.killCount then
            maxKillNum = item.killCount
            name = item.playerName
        end
        if maxRepairNum < item.repairCount then
            maxRepairNum = item.repairCount
            repairName = item.playerName
        end
        killCount = killCount + item.killCount
        repairCount = repairCount + item.repairCount
    end))

    --dic.Add("副战场杀敌最多者", name);
    CommonDefs.DictAdd(dic, typeof(String), LocalString.GetString("副战场灭敌数"), typeof(String), tostring(killCount))
    CommonDefs.DictAdd(dic, typeof(String), LocalString.GetString("杀死神兽数"), typeof(String), SafeStringFormat3(LocalString.GetString("大神兽%d个、小神兽%d个"), info.killBossNum, info.killSmallBossNum))

    CommonDefs.DictAdd(dic, typeof(String), LocalString.GetString("总维修次数"), typeof(String), tostring(repairCount))

    CUICommonDef.ClearTransform(this.grid.transform)
    CommonDefs.DictIterate(dic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local item = {}
        item.Key = ___key
        item.Value = ___value
        local obj = TypeAs(CommonDefs.Object_Instantiate(this.itemPrefab), typeof(GameObject))
        obj.transform.parent = this.grid.transform
        obj.transform.localScale = Vector3.one
        obj:SetActive(true)
        local cmp = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CGuildLeagueInfoItem))
        cmp:Init(item.Key, item.Value)
    end))
    this.grid:Reposition()

    this.radioBox:ChangeTo(- 1, true)
    this:Sort(this.playerInfos, ESortType.Default, true)
    this:showBattleFieldOtherInfo()
    this.tableView.m_DataSource = this
    this.tableView:ReloadData(true, false)
end
CGuildLeagueBattleResultInfoPane.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if row < this.playerInfos.Count then
        local item = TypeAs(this.tableView:GetFromPool(0), typeof(CGuildLeagueBattleResultItem))
        item:Init(this.playerInfos[row], row, this.isMain)
        return item
    end
    return nil
end
CGuildLeagueBattleResultInfoPane.m_OnGuildLeagueSwitchDisplay_CS2LuaHook = function (this, isJJW) 
    if this.isJiangJunWei == isJJW then
        this.listTf.gameObject:SetActive(true)
    else
        this.listTf.gameObject:SetActive(false)
    end
    --所有的排序都恢复默认排序
    do
        local i = 0
        while i < this.sortFlags.Length do
            this.sortFlags[i] = - 1
            i = i + 1
        end
    end
    this.radioBox:ChangeTo(- 1, true)
end

LuaGuildLeagueBattleResultInfoPane = {}

function LuaGuildLeagueBattleResultInfoPane:OnAwake(this)
    local serverNameLabel = this.transform:Find("Statics/ServerNameLabel")
    if serverNameLabel then
        serverNameLabel:GetComponent(typeof(UILabel)).text = ""
    end
    local battleResultIcon = this.transform:Find("Statics/BattleResultIcon")
    if battleResultIcon then
        battleResultIcon.gameObject:SetActive(false)
    end
end

function LuaGuildLeagueBattleResultInfoPane.InitServerNameAndResultInfo(this, info)
    local serverNameLabel = this.transform:Find("Statics/ServerNameLabel")
    if serverNameLabel then
        serverNameLabel:GetComponent(typeof(UILabel)).text = info.serverName
    end
    local battleResultIcon = this.transform:Find("Statics/BattleResultIcon")
    if battleResultIcon then
        local bg = this.transform:Find("Statics/BattleResultIcon/BattleResultBg")
        CUICommonDef.SetGrey(bg.gameObject, info.hasResult and not info.isWin)
        if info.hasResult then
            battleResultIcon.gameObject:SetActive(true)
            battleResultIcon:GetComponent(typeof(UISprite)).spriteName = info.isWin and g_sprites.CommonWinSpriteName or g_sprites.CommonLoseSpriteName
        else
            battleResultIcon.gameObject:SetActive(false)
        end
    end
end

