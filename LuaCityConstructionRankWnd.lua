require("common/common_include")

local UIGrid = import "UIGrid"
local Extensions = import "Extensions"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CRankData = import "L10.UI.CRankData"
local NGUITools = import "NGUITools"
local Constants = import "L10.Game.Constants"

CLuaCityConstructionRankWnd = class()
CLuaCityConstructionRankWnd.Path = "ui/citywar/LuaCityConstructionRankWnd"    

RegistClassMember(CLuaCityConstructionRankWnd, "m_MyGuildObj")
RegistClassMember(CLuaCityConstructionRankWnd, "m_FullProgressValue")              

function CLuaCityConstructionRankWnd:Awake( ... )
	Gac2Gas.QueryRank(41500001)
	self.m_MyGuildObj = self.transform:Find("Rank/MyGuild").gameObject
	self.m_MyGuildObj:SetActive(false)
	self.m_FullProgressValue = tonumber(CityWar_Setting.GetData().MaxGateProgress)
end

function CLuaCityConstructionRankWnd:Init()
	
end

function CLuaCityConstructionRankWnd:OnRankDataReady( ... )
	if CLuaRankData.m_CurRankId ~= 41500001 then return end
	local templateObj = self.transform:Find("Rank/AdvView/Pool/Template").gameObject
	templateObj:SetActive(false)
	local grid = self.transform:Find("Rank/AdvView/Scroll View/Grid"):GetComponent(typeof(UIGrid))
	self.m_MyGuildObj:SetActive(true)
	self:InitItem(self.m_MyGuildObj.transform, CRankData.Inst.MainPlayerRankInfo, true, true)
	Extensions.RemoveAllChildren(grid.transform)
	for i = 0, CRankData.Inst.RankList.Count - 1 do
		local obj = NGUITools.AddChild(grid.gameObject, templateObj)
		obj:SetActive(true)
		self:InitItem(obj.transform, CRankData.Inst.RankList[i], false, i % 2 == 0)
	end
	grid:Reposition()
end

function CLuaCityConstructionRankWnd:InitItem(rootTrans, data, isMyGuild, isOdd)
	rootTrans:Find("GuildNameLabel"):GetComponent(typeof(UILabel)).text = data.Guild_Name
	rootTrans:Find("ServerNameLabel"):GetComponent(typeof(UILabel)).text = data.serverName
	rootTrans:Find("GuildGradeLabel"):GetComponent(typeof(UILabel)).text = data.Level
	rootTrans:Find("GuildPeopleLabel"):GetComponent(typeof(UILabel)).text = data.Member
	rootTrans:Find("ProgressLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("%s(%s%%)", data.Value, math.floor(data.Value/self.m_FullProgressValue * 100))
	if not isMyGuild then
		rootTrans:GetComponent(typeof(UISprite)).spriteName = isOdd and Constants.GreyItemBgSpriteName or Constants.WhiteItemBgSpriteName
	end

	if isMyGuild then print(data.Value) end

	local rankSprite = rootTrans:Find("RankSprite"):GetComponent(typeof(UISprite))
	rankSprite.gameObject:SetActive(data.Rank <= 3 and data.Rank >= 1)
	if data.Rank > 3 then
		rootTrans:Find("RankLabel"):GetComponent(typeof(UILabel)).text = data.Rank
	elseif data.Rank == 0 then
		rootTrans:Find("RankLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("未上榜")
	else
		local spriteNames = {"Rank_No.1", "Rank_No.2png", "Rank_No.3png"}
		rankSprite.spriteName = spriteNames[data.Rank]
	end
end

function CLuaCityConstructionRankWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function CLuaCityConstructionRankWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end