local UITable = import "UITable"
local UIGrid = import "UIGrid"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local CButton = import "L10.UI.CButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local Animation = import "UnityEngine.Animation"

LuaGuildTerritorialWarsServerGroupWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuildTerritorialWarsServerGroupWnd, "TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaGuildTerritorialWarsServerGroupWnd, "RankWndButton", "RankWndButton", CButton)
RegistChildComponent(LuaGuildTerritorialWarsServerGroupWnd, "BottomTimeLabel", "BottomTimeLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsServerGroupWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsServerGroupWnd, "GroupView", "GroupView", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsServerGroupWnd, "NoneGroupView", "NoneGroupView", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsServerGroupWnd, "NoneGroupViewGrid", "NoneGroupViewGrid", UIGrid)
RegistChildComponent(LuaGuildTerritorialWarsServerGroupWnd, "ServerNameTemplate", "ServerNameTemplate", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsServerGroupWnd, "GroupViewTemplate", "GroupViewTemplate", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsServerGroupWnd, "GroupViewTable", "GroupViewTable", UITable)
RegistChildComponent(LuaGuildTerritorialWarsServerGroupWnd, "GroupViewContainer", "GroupViewContainer", UIWidget)
RegistChildComponent(LuaGuildTerritorialWarsServerGroupWnd, "NoneGroupViewContainer", "NoneGroupViewContainer", UIWidget)
RegistChildComponent(LuaGuildTerritorialWarsServerGroupWnd, "TabTemplate", "TabTemplate", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsServerGroupWnd, "TopScrollView", "TopScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaGuildTerritorialWarsServerGroupWnd, "ScrollView1", "ScrollView1", CUIRestrictScrollView)

--@endregion RegistChildComponent end
RegistClassMember(LuaGuildTerritorialWarsServerGroupWnd,"m_TabIndex")
RegistClassMember(LuaGuildTerritorialWarsServerGroupWnd,"m_ServerGroupDataMap")
RegistClassMember(LuaGuildTerritorialWarsServerGroupWnd,"m_WarStartTimeStamp")
RegistClassMember(LuaGuildTerritorialWarsServerGroupWnd,"m_Ani")

function LuaGuildTerritorialWarsServerGroupWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabBarTabChange(index)
	end)

	UIEventListener.Get(self.RankWndButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankWndButtonClick()
	end)

	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

    --@endregion EventBind end
end

function LuaGuildTerritorialWarsServerGroupWnd:Init()
    self.BottomTimeLabel.text = ""
    self.ServerNameTemplate.gameObject:SetActive(false)
    self.GroupViewTemplate.gameObject:SetActive(false)
    self.TabTemplate.gameObject:SetActive(false)
    self.m_ServerGroupDataMap = {}
    local labeltexts = GuildTerritoryWar_RelatedPlaySetting.GetData().PreSeverGroup
    Extensions.RemoveAllChildren(self.TabBar.transform)
    for i = 0, labeltexts.Length - 1 do
        local obj = NGUITools.AddChild(self.TabBar.gameObject, self.TabTemplate.gameObject)
        obj:SetActive(true)
    end
    local tabbarTable = self.TabBar.transform:GetComponent(typeof(UITable))
    tabbarTable.columns = labeltexts.Length
    tabbarTable:Reposition()
    self.TabBar:Init(true)
    self.TabBar:InitWithLabels(labeltexts)
    GuildTerritoryWar_ServerGroup.Foreach(function (k,v)
        if self.m_ServerGroupDataMap[v.PreGroupName] == nil then
            self.m_ServerGroupDataMap[v.PreGroupName] = {}
        end
        local data = GuildTerritoryWar_ServerGroup.GetData(k)
        if data.IsValid > 0 then
            table.insert(self.m_ServerGroupDataMap[v.PreGroupName], data)
        end
    end)
    local warStartTimeStr = GuildTerritoryWar_RelatedPlaySetting.GetData().WarStartTime
    self.m_WarStartTimeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(warStartTimeStr)
    self.TabBar:ChangeTab(0,false)
    self.m_Ani = self.transform:GetComponent(typeof(Animation))
end

function LuaGuildTerritorialWarsServerGroupWnd:InitGroupView()
    Extensions.RemoveAllChildren(self.GroupViewTable.transform)
    local groupViewHeight = 0
    local labeltexts = GuildTerritoryWar_RelatedPlaySetting.GetData().PreSeverGroup
    local key = labeltexts[self.m_TabIndex]
    local arr = self.m_ServerGroupDataMap[key]
    local groupArr = {}
    for i = 1, #arr do
        local data = arr[i]
        if groupArr[data.CrossSeverName] == nil then
            groupArr[data.CrossSeverName] = {}
        end
        table.insert(groupArr[data.CrossSeverName],data)
    end
    local sortList = {}
    for k,arr in pairs(groupArr) do
        local index = tonumber(string.match(k, '(%d+)')) 
        table.insert(sortList,{k = k, arr = arr, index = index})
    end
    table.sort(sortList,function(a, b)
        return a.index < b.index
    end)
    for _,t in pairs(sortList) do 
        groupViewHeight = self:InitGroupViewTemplate(t.k, t.arr, groupViewHeight)
    end
    self.GroupViewContainer.height = groupViewHeight
    self.GroupViewTable:Reposition()
end

function LuaGuildTerritorialWarsServerGroupWnd:InitGroupViewTemplate(k, arr, groupViewHeight)
    local serverNameTemplateHeight = self.ServerNameTemplate.gameObject:GetComponent(typeof(UIWidget)).height
    local obj = NGUITools.AddChild(self.GroupViewTable.gameObject, self.GroupViewTemplate.gameObject)
    obj.gameObject:SetActive(true)
    local groupNameLabel = obj.transform:Find("GroupNameLabel"):GetComponent(typeof(UILabel)) 
    local groupViewGrid = obj.transform:Find("GroupViewGrid"):GetComponent(typeof(UIGrid))
    Extensions.RemoveAllChildren(groupViewGrid.transform)
    groupNameLabel.text = k
    local len = #arr
    for i = 1, len do 
        local data = arr[i]
        local go = NGUITools.AddChild(groupViewGrid.gameObject, self.ServerNameTemplate.gameObject)
        local serverNameLabel = go.transform:Find("ServerNameLabel"):GetComponent(typeof(UILabel))
        go.gameObject:SetActive(true)
        serverNameLabel.text = data.SeverName
        if data.ServerId == CClientMainPlayer.Inst:GetMyServerId() then
            serverNameLabel.color = Color.green
        end
    end
    local cellNum, yu = math.modf(len / groupViewGrid.maxPerLine)
    groupViewHeight = groupViewHeight + 64
    groupViewHeight = groupViewHeight + (yu > 0 and (groupViewGrid.cellHeight * cellNum + serverNameTemplateHeight) or (groupViewGrid.cellHeight * (cellNum - 1) + serverNameTemplateHeight))
    groupViewHeight = groupViewHeight + self.GroupViewTable.padding.y
    groupViewGrid:Reposition()
    return groupViewHeight
end

function LuaGuildTerritorialWarsServerGroupWnd:InitNoneGroupView()
    Extensions.RemoveAllChildren(self.NoneGroupViewGrid.transform)
    local labeltexts = GuildTerritoryWar_RelatedPlaySetting.GetData().PreSeverGroup
    local key = labeltexts[self.m_TabIndex]
    local arr = self.m_ServerGroupDataMap[key]
    for i = 1, #arr do 
        local data = arr[i]
        local obj = NGUITools.AddChild(self.NoneGroupViewGrid.gameObject, self.ServerNameTemplate.gameObject)
        local serverNameLabel = obj.transform:Find("ServerNameLabel"):GetComponent(typeof(UILabel)) --TODO
        obj.gameObject:SetActive(true)
        serverNameLabel.text = data.SeverName
        if data.ServerId == CClientMainPlayer.Inst:GetMyServerId() then
            serverNameLabel.color = Color.green
        end
    end
    local noneGroupViewHeight = self.NoneGroupViewGrid.cellHeight * math.ceil(#arr / self.NoneGroupViewGrid.maxPerLine)
    self.NoneGroupViewContainer.height = noneGroupViewHeight
    self.NoneGroupViewGrid:Reposition()
end

--@region UIEvent

function LuaGuildTerritorialWarsServerGroupWnd:OnTabBarTabChange(index)
    self.m_TabIndex = index
    if self:IsShowGroupView() then
        self:InitGroupView()
        self.RankWndButton.gameObject:SetActive(self.m_WarStartTimeStamp <= CServerTimeMgr.Inst.timeStamp)
        self.BottomTimeLabel.gameObject:SetActive(self.m_WarStartTimeStamp > CServerTimeMgr.Inst.timeStamp)
        self.BottomTimeLabel.text = g_MessageMgr:FormatMessage("GuildTerritorialWarsServerGroupWnd_OpenText")
    else
        self:InitNoneGroupView()
        self.BottomTimeLabel.gameObject:SetActive(not LuaGuildTerritorialWarsMgr.m_ServerGroupDecided)
        self.RankWndButton.gameObject:SetActive(LuaGuildTerritorialWarsMgr.m_ServerGroupDecided)
        if not LuaGuildTerritorialWarsMgr.m_ServerGroupDecided then
            PlayerPrefs.SetInt("GuildTerritorialWarsServerGroupWnd_OpenGroupView",0)
        end
        self.BottomTimeLabel.text = g_MessageMgr:FormatMessage("GuildTerritorialWarsServerGroupWnd_NotOpenText")
    end
    --self.GroupView.gameObject:SetActive(self:IsShowGroupView())
    --self.NoneGroupView.gameObject:SetActive(not self:IsShowGroupView())
    self.RankWndButton.Text = self:IsShowGroupView() and LocalString.GetString("查看入选帮会名单") or LocalString.GetString("查看棋局分组结果")
    self.ScrollView1:ResetPosition()
end

function LuaGuildTerritorialWarsServerGroupWnd:IsShowGroupView()
    return LuaGuildTerritorialWarsMgr.m_ServerGroupDecided and (PlayerPrefs.GetInt("GuildTerritorialWarsServerGroupWnd_OpenGroupView") == 1)
end

function LuaGuildTerritorialWarsServerGroupWnd:OnRankWndButtonClick()
    if not self:IsShowGroupView() then
        PlayerPrefs.SetInt("GuildTerritorialWarsServerGroupWnd_OpenGroupView",1)
        self.m_Ani:Play()
        self:OnTabBarTabChange(self.m_TabIndex)
    else
        Gac2Gas.RequsetTerritoryWarRankInfo(1)
    end
end


function LuaGuildTerritorialWarsServerGroupWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("GuildTerritorialWarsServerGroupWnd_ReadMe")
end


--@endregion UIEvent

