local QnTableView = import "L10.UI.QnTableView"


local GameObject = import "UnityEngine.GameObject"

local DelegateFactory  = import "DelegateFactory"

LuaShengXiaoCardChooseWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShengXiaoCardChooseWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaShengXiaoCardChooseWnd, "Item", "Item", GameObject)
RegistChildComponent(LuaShengXiaoCardChooseWnd, "TableView", "TableView", QnTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaShengXiaoCardChooseWnd,"m_DisplayId")

LuaShengXiaoCardChooseWnd.s_ShengXiao = 0
function LuaShengXiaoCardChooseWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)


    --@endregion EventBind end
	self.Item:SetActive(false)
end

function LuaShengXiaoCardChooseWnd:Init()

	local data = {}
	ShengXiaoCard_ShengXiao.Foreach(function(k,v)
		table.insert(data,v.Name)
	end)

	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #data
        end,
        function(item,row)
            local tf = item.transform
            local label = tf:Find("Label"):GetComponent(typeof(UILabel))
            label.text = data[row+1]
			local sprite = tf:GetComponent(typeof(UISprite))
			
			if LuaShengXiaoCardMgr.m_PlayedCards[row+1] then
				sprite.color = Color(1,1,1,1)
			else
				sprite.color = Color(0.5,0.5,0.5,1)
			end
        end)
	self.TableView:ReloadData(false,false)
	local idx = 0
	if LuaShengXiaoCardChooseWnd.s_ShengXiao>0 then
		idx = LuaShengXiaoCardChooseWnd.s_ShengXiao-1
	end
    self.TableView:SetSelectRow(idx,true)
end

--@region UIEvent

function LuaShengXiaoCardChooseWnd:OnButtonClick()
	CUIManager.CloseUI(CLuaUIResources.ShengXiaoCardChooseWnd)
end


--@endregion UIEvent

function LuaShengXiaoCardChooseWnd:OnDestroy()
	LuaShengXiaoCardChooseWnd.s_ShengXiao = 0

	if LuaShengXiaoCardMgr.m_State==EnumShengXiaoCardState.WaitShow then
		Gac2Gas.ShengXiaoCard_Show(LuaShengXiaoCardMgr.m_SelectedCardId,self.TableView.currentSelectRow+1)
	elseif LuaShengXiaoCardMgr.m_State==EnumShengXiaoCardState.PassShow then
		Gac2Gas.ShengXiaoCard_PassShow(self.TableView.currentSelectRow+1)
	end
end
