local DelegateFactory = import "DelegateFactory"
local UILabel         = import "UILabel"
local UITexture       = import "UITexture"
local GameObject      = import "UnityEngine.GameObject"
local EnumGender      = import "L10.Game.EnumGender"

local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CPropertyAppearance          = import "L10.Game.CPropertyAppearance"
local CPropertySkill               = import "L10.Game.CPropertySkill"

LuaWeddingInvitationCardWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWeddingInvitationCardWnd, "GroomName", "GroomName", UILabel)
RegistChildComponent(LuaWeddingInvitationCardWnd, "BrideName", "BrideName", UILabel)
RegistChildComponent(LuaWeddingInvitationCardWnd, "GroomModel", "GroomModel", UITexture)
RegistChildComponent(LuaWeddingInvitationCardWnd, "BrideModel", "BrideModel", UITexture)
RegistChildComponent(LuaWeddingInvitationCardWnd, "SignLabel", "SignLabel", UILabel)
RegistChildComponent(LuaWeddingInvitationCardWnd, "EnterButton", "EnterButton", GameObject)
RegistChildComponent(LuaWeddingInvitationCardWnd, "RecieverName", "RecieverName", UILabel)

--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingInvitationCardWnd, "groomIdentifier")
RegistClassMember(LuaWeddingInvitationCardWnd, "brideIdentifier")
RegistClassMember(LuaWeddingInvitationCardWnd, "groomRotation")
RegistClassMember(LuaWeddingInvitationCardWnd, "brideRotation")
RegistClassMember(LuaWeddingInvitationCardWnd, "groomRO")
RegistClassMember(LuaWeddingInvitationCardWnd, "brideRO")

function LuaWeddingInvitationCardWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.EnterButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterButtonClick()
	end)

    --@endregion EventBind end
end

function LuaWeddingInvitationCardWnd:OnDestroy()
	CUIManager.DestroyModelTexture(self.groomIdentifier)
	CUIManager.DestroyModelTexture(self.brideIdentifier)
end

function LuaWeddingInvitationCardWnd:Init()
	self:InitLabels()
	self:InitModel()
    self:InitBgFx()
end

-- 初始化label
function LuaWeddingInvitationCardWnd:InitLabels()
	self.RecieverName.text = CClientMainPlayer.Inst.Name
	self.GroomName.text = LuaWeddingIterationMgr.cardInfo.groomName
	self.BrideName.text = LuaWeddingIterationMgr.cardInfo.brideName
	self.SignLabel.text = LuaWeddingIterationMgr.cardInfo.sign
end

-- 初始化模型
function LuaWeddingInvitationCardWnd:InitModel()
	self.groomIdentifier = SafeStringFormat3("__%s__", tostring(self.GroomModel.gameObject:GetInstanceID()))
	self.brideIdentifier = SafeStringFormat3("__%s__", tostring(self.BrideModel.gameObject:GetInstanceID()))

	local groomClass = LuaWeddingIterationMgr.cardInfo.groomClass
	local brideClass = LuaWeddingIterationMgr.cardInfo.brideClass

	self:CreateModel(self.groomIdentifier, self.GroomModel, EnumGender.Male, groomClass)
	self:CreateModel(self.brideIdentifier, self.BrideModel, EnumGender.Female, brideClass)

	local expressionId = LuaWeddingIterationMgr.cardInfo.expressionId
	LuaWeddingIterationMgr:PlayActionInInvitationCard(self.groomRO, self.brideRO, self.groomIdentifier, self.brideIdentifier, expressionId)
end

-- 创建人物模型
function LuaWeddingInvitationCardWnd:CreateModel(identifier, tex, gender, class)
    local loader = self:GetModelLoader(class, gender)

    tex.mainTexture = CUIManager.CreateModelTexture(identifier, loader,
        180, 0.05, -1, 4.66, false, true, 1.0, false, false)
end

-- 生成外观
function LuaWeddingInvitationCardWnd:GetModelLoader(class, gender)
    return LuaDefaultModelTextureLoader.Create(function (ro)
        local appearanceData = CPropertyAppearance.GetDataFromCustom(class, gender,
            CPropertySkill.GetDefaultYingLingStateByGender(gender), nil, nil, 0, 0, 0, 0,
            WeddingIteration_Setting.GetData().WeddingDressHeadId,
            WeddingIteration_Setting.GetData().WeddingDressBodyId,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, nil, nil, nil, nil, 0, 0, 0, 0, 0, 0, nil, 0)

        CClientMainPlayer.LoadResource(ro, appearanceData, true, 1.0,
            0, false, 0, false, 0, false, nil, false)

        if gender == EnumGender.Male then
            self.groomRO = ro
        else
            self.brideRO = ro
        end
    end)
end

-- 加载背景特效
function LuaWeddingInvitationCardWnd:InitBgFx()
    local cuiFx = self.transform:Find("Bg"):Find("Fx"):GetComponent(typeof(CUIFx))
    cuiFx:LoadFx(g_UIFxPaths.WeddingQingJianFxPath)
end

--@region UIEvent

function LuaWeddingInvitationCardWnd:OnEnterButtonClick()
	local sceneId = LuaWeddingIterationMgr.cardInfo.sceneId
	local invitationType = LuaWeddingIterationMgr.cardInfo.type
	Gac2Gas.CheckEnterWeddingByNewInvitation(sceneId, invitationType)
end

--@endregion UIEvent

