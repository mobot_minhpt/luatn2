-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuildInvitationItem = import "L10.UI.CGuildInvitationItem"
local CGuildInvitationWnd = import "L10.UI.CGuildInvitationWnd"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CIMMgr = import "L10.Game.CIMMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CRelationshipBasicPlayerInfo = import "L10.Game.CRelationshipBasicPlayerInfo"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local L10 = import "L10"
local Quaternion = import "UnityEngine.Quaternion"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt64 = import "System.UInt64"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildInvitationWnd.m_Awake_CS2LuaHook = function (this) 
    this.noFriendNode:SetActive(false)
    this.itemTemplate:SetActive(false)
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
end
CGuildInvitationWnd.m_Init_CS2LuaHook = function (this) 
    this:ClearData()
    this.titleLabel.text = nil



    Gac2Gas.QueryFriendsNoGuild()
end
CGuildInvitationWnd.m_OnQueryFriendsNoGuildResult_CS2LuaHook = function (this, noGuildList) 
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end

    local list = CreateFromClass(MakeGenericClass(List, CRelationshipBasicPlayerInfo))
    local friends = CIMMgr.Inst.Friends

    CommonDefs.EnumerableIterate(friends, DelegateFactory.Action_object(function (___value) 
        local playerId = ___value
        local continue
        repeat
            if not CommonDefs.ListContains(noGuildList, typeof(UInt64), playerId) then
                continue = true
                break
            end
            local info = CIMMgr.Inst:GetBasicInfo(playerId)
            if info.Level >= CGuildInvitationWnd.LevelRequire then
                if CIMMgr.Inst:IsOnline(playerId) then
                    if CGuildMgr.Inst.m_GuildMemberInfo ~= nil and CGuildMgr.Inst.m_GuildMemberInfo.Infos ~= nil then
                        if not CommonDefs.DictContains(CGuildMgr.Inst.m_GuildMemberInfo.Infos, typeof(UInt64), playerId) then
                            CommonDefs.ListAdd(list, typeof(CRelationshipBasicPlayerInfo), info)
                        end
                    end
                end
            end
            continue = true
        until 1
        if not continue then
            return
        end
    end))
    this:LoadData(list)
end
CGuildInvitationWnd.m_LoadData_CS2LuaHook = function (this, itemInfos) 
    if itemInfos.Count > 0 then
        this.noFriendNode:SetActive(false)
        this.hasFriendNode:SetActive(true)
    else
        this.noFriendNode:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(this.noFriendNode, typeof(UILabel)).text = g_MessageMgr:FormatMessage("Friend_RuBang_Fail_All_Have_BangHui")
        this.hasFriendNode:SetActive(false)
    end
    this:ClearData()

    do
        local i = 0
        while i < itemInfos.Count do
            local instance = TypeAs(GameObject.Instantiate(this.itemTemplate, Vector3.zero, Quaternion.identity), typeof(GameObject))
            instance.transform.parent = this.table.transform
            instance.transform.localScale = Vector3.one
            local item = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CGuildInvitationItem))
            local portraitName = L10.UI.CUICommonDef.GetPortraitName(itemInfos[i].Class, itemInfos[i].Gender, itemInfos[i].Expression)
            item:Init(itemInfos[i].ID, itemInfos[i].Name, portraitName, itemInfos[i].Level)

            item.OnOpButtonClickDelegate = MakeDelegateFromCSFunction(this.InviteToJoinTeam, MakeGenericClass(Action1, UInt64), this)
            instance:SetActive(true)
            i = i + 1
        end
    end
    this.table:Reposition()
    this.scrollView:ResetPosition()
end
