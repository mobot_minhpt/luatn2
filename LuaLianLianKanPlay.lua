local CClientChessmanObject=import "L10.Game.CClientChessmanObject"
local Utility=import "L10.Engine.Utility"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CPos=import "L10.Engine.CPos"
local CLLKChessman=import "L10.Game.CLLKChessman"
local CLianLianKanLine=import "CLianLianKanLine"
--现在只有一张地图
CLuaLianLianKanPlay=class()
RegistClassMember(CLuaLianLianKanPlay,"m_ClientObjects")
RegistClassMember(CLuaLianLianKanPlay,"m_MainPlayerId")
RegistClassMember(CLuaLianLianKanPlay,"m_SelectionLookup")
RegistClassMember(CLuaLianLianKanPlay,"m_Round")
RegistClassMember(CLuaLianLianKanPlay,"m_Lines1")
RegistClassMember(CLuaLianLianKanPlay,"m_Lines2")
RegistClassMember(CLuaLianLianKanPlay,"m_PlayerId1")
RegistClassMember(CLuaLianLianKanPlay,"m_PlayerId2")
RegistClassMember(CLuaLianLianKanPlay,"m_Positions")

RegistClassMember(CLuaLianLianKanPlay,"m_Ticks")

function CLuaLianLianKanPlay:Ctor()
    self.m_Round = CLianLianKanRound:new()
    self.m_PlayerId1=0
    self.m_PlayerId2=0

    self.m_Ticks={}

    self.m_SelectionLookup={}

    self.m_ClientObjects={}
    self.m_Positions={}
    --创建6条line
    local root = GameObject.Find("SceneRoot").transform
    self.m_Lines1={}
    self.m_Lines2={}
    for i=1,6 do
        local go = GameObject("line")
        go.transform.parent = root
        go.transform.localScale=Vector3(1,1,1)
        local cmp = go:AddComponent(typeof(CLianLianKanLine))
        if i<4 then
            self.m_Lines1[i]=cmp
            self.m_Lines1[i]:SetColor(Color(0,1,0.25))
        else
            self.m_Lines2[i-3]=cmp
            self.m_Lines2[i-3]:SetColor(Color(0.1,0.22,1))
        end
    end
end
function CLuaLianLianKanPlay:Init(roundIndex)
    for k,v in pairs(self.m_ClientObjects) do
        for k2,v2 in pairs(v) do
            local npc = v2 and CClientObjectMgr.Inst:GetObject(v2)
            if npc then
                npc:Destroy()
            end
        end
    end
    self.m_ClientObjects={}
    for i,v in ipairs(self.m_Lines1) do
        if v then
            v:Hide()
        end
    end
    for i,v in ipairs(self.m_Lines2) do
        if v then
            v:Hide()
        end
    end

    self.m_SelectionLookup={}

    -- self.m_ClientObjects={}
    self.m_Positions={}

    self.m_MainPlayerId=CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0

    local designData = LianLianKan2018_Matrix.GetData(roundIndex)
    self.m_Round:Init(designData.Matrix,designData.NpcNum)
    for i=1,self.m_Round.m_Row do
        self.m_ClientObjects[i]={}
    end
end
function CLuaLianLianKanPlay:End()
    for k,v in pairs(self.m_ClientObjects) do
        for k2,v2 in pairs(v) do
            local npc = v2 and CClientObjectMgr.Inst:GetObject(v2)
            if npc then
                npc:Destroy()
            end
        end
    end
    self.m_ClientObjects={}
    for i,v in ipairs(self.m_Lines1) do
        if v and v.gameObject then
            GameObject.Destroy(v.gameObject)
        end
    end
    self.m_Lines1=nil
    for i,v in ipairs(self.m_Lines2) do
        if v and v.gameObject then
            GameObject.Destroy(v.gameObject)
        end
    end
    self.m_Lines2=nil
end
function CLuaLianLianKanPlay:HideLines(playerId)
    local lines=self.m_Lines1
    if self.m_MainPlayerId ~= playerId then
        lines=self.m_Lines2
    end
    if lines then
        for i,v in ipairs(lines) do
            v:Hide()
        end
    end
end

--两个人同时玩 可能有2条线
function CLuaLianLianKanPlay:DrawLines(playerId,startX,startY,endX,endY)
    if not self.m_Round then return end
    local getval=function(val)
        if val>0 then return 1
        elseif val<0 then return -1
        else return 0 end
    end
    local pixels = (32+15)/64
    local path = self.m_Round:GetPath(startX,startY,endX,endY)
    local lines=self.m_Lines1

    if self.m_MainPlayerId ~= playerId then
        lines=self.m_Lines2
    end
    for i,v in ipairs(lines) do
        if v then v:Hide() end
    end
    if #path>0 then
        for i=2,#path do
            local a=path[i-1]
            local b=path[i]
            local sub=Vector3(getval(a[1]-b[1])*pixels,0,getval(a[2]-b[2])*pixels)
            local pos1=self.m_Positions[a[1]][a[2]]
            local pos2=self.m_Positions[b[1]][b[2]]
            if lines[i-1] then
                lines[i-1]:Show()
                if #path==2 then
                    lines[i-1]:UpdatePosition(Vector3(pos1.x-sub.x,pos1.y,pos1.z-sub.z), Vector3(pos2.x+sub.x,pos2.y,pos2.z+sub.z))
                elseif i==2 then
                    lines[i-1]:UpdatePosition(Vector3(pos1.x-sub.x,pos1.y,pos1.z-sub.z), pos2)
                elseif i==#path then
                    lines[i-1]:UpdatePosition(pos1, Vector3(pos2.x+sub.x,pos2.y,pos2.z+sub.z))
                else
                    lines[i-1]:UpdatePosition(pos1, pos2)
                end
            end
        end
    end
end


function CLuaLianLianKanPlay:UpdateLianLianKanChessData(t)
    local centerX,centerY=38,35
    -- local row=math.sqrt( #t )
    self.m_Round:RemoveAll()
    --清理
    for k,v in pairs(self.m_ClientObjects) do
        for k2,v2 in pairs(v) do
            local npc = v2 and CClientObjectMgr.Inst:GetObject(v2)
            if npc then
                npc:Destroy()
            end
            -- if v2 then
                -- v2:Destroy()
            -- end
        end
    end
    self.m_ClientObjects={}

    self.m_SelectionLookup={}

    for i=1,self.m_Round.m_Row do
        self.m_ClientObjects[i]={}
        self.m_Positions[i]={}
    end

    local half1 = self.m_Round.m_Row/2
    local half2 = self.m_Round.m_Column/2

    for i=1,self.m_Round.m_Row do
        for j=1,self.m_Round.m_Column do
            local type = t[(i-1)*self.m_Round.m_Column+j] or 0
            local x=math.floor(centerX*64+ (i-half1)*(64+50))
            local y=math.floor(centerY*64+ (j-half2)*(64+50))

            local pos=CPos(x,y)
            self.m_Positions[i][j]=Utility.PixelPos2WorldPos(pos)
            if type>0 then
                local npc = CClientChessmanObject.CreatePerformerNpc(pos)
                local chessman = CLLKChessman()
                chessman.x = i
                chessman.y = j
                local design = LianLianKan2018_Template.GetData(type)
                local resPath = SafeStringFormat3( "Character/Jiayuan/%s/Prefab/%s_%02d.prefab",design.ResName,design.ResName,design.ResId )

                npc:Init(type,chessman,resPath,design.Scale)
                self.m_Round:Add(i,j,type)

                self.m_ClientObjects[i][j]=npc.EngineId
            else

            end
        end
    end
end

function CLuaLianLianKanPlay:PlayerSelectOneChessman(playerId, x, y)
    for k,v in pairs(self.m_SelectionLookup) do
        if (playerId~=k and v.x==x and v.y==y) then
            self.m_SelectionLookup[k]=nil
        end
    end
    
    local engineId = self.m_ClientObjects[x][y]
    if self.m_SelectionLookup[playerId] then
        local preEngineId = self.m_SelectionLookup[playerId].engineId
        if preEngineId~=engineId then
            --说明太快了
            local prenpc = preEngineId and CClientObjectMgr.Inst:GetObject(preEngineId)
            if prenpc then prenpc:HideFx() end
        end
    end

    local npc = engineId and CClientObjectMgr.Inst:GetObject(engineId)
    if npc then
        self.m_SelectionLookup[playerId]={ engineId = engineId,x = x,y = y }
        self:ShowFx(npc,playerId)
    else
        self.m_SelectionLookup[playerId]=nil
    end
end
function CLuaLianLianKanPlay:ShowFx(npc,playerId)
    if playerId==self.m_MainPlayerId then
        npc:ShowGreenFx()
    else
        npc:ShowBlueFx()
    end
end

function CLuaLianLianKanPlay:PlayerCancelSelectOneChessman(playerId, x, y)
    -- print("PlayerCancelSelectOneChessman")
    local cache = self.m_SelectionLookup[playerId]
    if cache then
        if cache.x==x and cache.y==y then
            self.m_SelectionLookup[playerId]=nil
        end
    end

    local engineId = self.m_ClientObjects[x][y]
    local npc = engineId and CClientObjectMgr.Inst:GetObject(engineId)
    if npc then
        npc:HideFx()
    else
    end
end
function CLuaLianLianKanPlay:Test()
    for i=1,20 do
        local x = math.random( self.m_Round.m_Row )
        local y = math.random( self.m_Round.m_Column )
        local engineId = self.m_ClientObjects[x][y]
        if engineId then
            self:OnChessmanObjectSelected(engineId)
        end
    end

end
--客户端选中一个模型的时候 发送rpc给服务器
function CLuaLianLianKanPlay:OnChessmanObjectSelected(engineId)
    local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if not obj then
        return
    end
    local x1,y1=obj.Chessman.x,obj.Chessman.y

    local selectedEngineId = 0
    if self.m_SelectionLookup[self.m_MainPlayerId] then
        selectedEngineId = self.m_SelectionLookup[self.m_MainPlayerId].engineId or 0
    end

    --看看之前选中的物体还在不在
    local preObj = CClientObjectMgr.Inst:GetObject(selectedEngineId)
    if not preObj then
        self.m_SelectionLookup[self.m_MainPlayerId]=nil
        selectedEngineId=0 
    end

    --已经选了一个了
    if selectedEngineId>0 then
        if selectedEngineId==engineId then
            --取消选择
            -- print("LianLianKan2018CancelSelectOneChessman",x,y)
            self:CancleSelection(self.m_MainPlayerId)
            Gac2Gas.LianLianKan2018CancelSelectOneChessman(x1,y1)
        else
            if preObj then
                local x2,y2 = preObj.Chessman.x,preObj.Chessman.y
                if preObj.TemplateId == obj.TemplateId then
                    --预先判断是否连通
                    if self.m_Round:CanMatch(x1,y1,x2,y2) then
                        -- print("LianLianKan2018RequestMatchTwoChessman",x1,y1,x2,y2)
                        self:DrawLines(self.m_MainPlayerId,x1,y1,x2,y2)
                        -- print("LianLianKan2018SelectOneChessman",x,y)
                        Gac2Gas.LianLianKan2018SelectOneChessman(x1,y1)
                        Gac2Gas.LianLianKan2018RequestMatchTwoChessman(x1,y1,x2,y2)
                        self.m_SelectionLookup[self.m_MainPlayerId]=nil
                        return
                    end
                end

                preObj:HideFx()
                --先取消之前的选择 在选择现在的选择
                Gac2Gas.LianLianKan2018CancelSelectOneChessman(x2,y2)
                Gac2Gas.LianLianKan2018SelectOneChessman(x1,y1)
                self.m_SelectionLookup[self.m_MainPlayerId]={
                    engineId=engineId,
                    x = x1,
                    y = y1
                }
                self:HideLines(self.m_MainPlayerId)
            else
                Gac2Gas.LianLianKan2018SelectOneChessman(x1,y1)
                self.m_SelectionLookup[self.m_MainPlayerId]={ engineId=engineId,x = x1,y = y1 }
                --哪里搞错了
                -- print("error!")
            end

        end
    else
        --第一次选
        Gac2Gas.LianLianKan2018SelectOneChessman(x1,y1)
        self.m_SelectionLookup[self.m_MainPlayerId]={ engineId=engineId,x = x1,y = y1 }
    end
end


function CLuaLianLianKanPlay:PlayerMatchChessSuccess(playerId, x1, y1, x2, y2)
    -- print("PlayerMatchChessSuccess", x1, y1, x2, y2)
    for k,v in pairs(self.m_SelectionLookup) do
        if (v.x==x1 and v.y==y1) or (v.x==x2 and v.y==y2) then
            self:CancleSelection(k)--如果是playerid自己的选择
        end
    end

    local engineId = self.m_ClientObjects[x1][y1]
    local npc1 = engineId and CClientObjectMgr.Inst:GetObject(engineId)
    if npc1 then
        npc1:ShowDestroyFx()
    end
    self.m_ClientObjects[x1][y1]=nil

    engineId = self.m_ClientObjects[x2][y2]
    local npc2 = engineId and CClientObjectMgr.Inst:GetObject(engineId)
    if npc2 then
        npc2:ShowDestroyFx()
    end
    self.m_ClientObjects[x2][y2]=nil

    self:DrawLines(playerId,x1,y1,x2,y2)
    if self.m_Ticks[playerId] then
        UnRegisterTick(self.m_Ticks[playerId])
    end
    self.m_Ticks[playerId] = RegisterTickOnce(function()
        self.m_Ticks[playerId]=nil
        self:HideLines(playerId)
    end,1000)

    self.m_Round:Remove(x1,y1)
    self.m_Round:Remove(x2,y2)

    if not self.m_Round:HaveSolution() then
        Gac2Gas.LianLianKanRequestResetChess()
    end
end

function CLuaLianLianKanPlay:PlayerMatchChessFail(playerId,  x1, y1, x2, y2)
    -- print("PlayerMatchChessFail",x1, y1, x2, y2)
    for k,v in pairs(self.m_SelectionLookup) do
        if (v.x==x1 and v.y==y2) or (v.x==x2 and v.y==y2) then
            self.m_SelectionLookup[k]=nil
        end
    end

    local engineId = self.m_ClientObjects[x1][y1]
    local npc1 = engineId and CClientObjectMgr.Inst:GetObject(engineId)
    if npc1 then npc1:HideFx() end

    engineId = self.m_ClientObjects[x2][y2]
    local npc2 = engineId and CClientObjectMgr.Inst:GetObject(engineId)
    if npc2 then npc2:HideFx() end

    self:HideLines(playerId)
end

function CLuaLianLianKanPlay:PlayerSelectOneChessmanFail(playerId, x, y)
    -- print("PlayerSelectOneChessmanFail",x,y)
    local cache = self.m_SelectionLookup[playerId]
    if cache then
        if cache.x==x and cache.y==y then
            self.m_SelectionLookup[playerId]=nil
        end
    end

    local engineId = self.m_ClientObjects[x][y]
    local npc = engineId and CClientObjectMgr.Inst:GetObject(engineId)
    if npc then npc:HideFx() end
end

function CLuaLianLianKanPlay:CancleSelection(playerId)
    if self.m_SelectionLookup[playerId] then
        local engineId = self.m_SelectionLookup[playerId].engineId
        local npc = engineId and CClientObjectMgr.Inst:GetObject(engineId)
        if npc then
            npc:HideFx()
        end
    end
    self.m_SelectionLookup[playerId]=nil
end

function CLuaLianLianKanPlay:AllHideFx()
    for k,v in pairs(self.m_ClientObjects) do
        for k2,v2 in pairs(v) do
            local npc = v2 and CClientObjectMgr.Inst:GetObject(v2)
            if npc then
                npc:HideFx()
            end
        end
    end
end