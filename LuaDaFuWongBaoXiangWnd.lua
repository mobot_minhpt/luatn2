local UITexture = import "UITexture"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local GameObject = import "UnityEngine.GameObject"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local StringBuilder = import "System.Text.StringBuilder"
local Animation = import "UnityEngine.Animation"
LuaDaFuWongBaoXiangWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaFuWongBaoXiangWnd, "Baoxiang", "Baoxiang", GameObject)
RegistChildComponent(LuaDaFuWongBaoXiangWnd, "CardTemplate", "CardTemplate", GameObject)
RegistChildComponent(LuaDaFuWongBaoXiangWnd, "LongPressSprite", "LongPressSprite", UITexture)
RegistChildComponent(LuaDaFuWongBaoXiangWnd, "CardTable", "CardTable", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongBaoXiangWnd, "m_StartPressTime")
RegistClassMember(LuaDaFuWongBaoXiangWnd, "m_IsPress")
RegistClassMember(LuaDaFuWongBaoXiangWnd, "m_CanPress")
RegistClassMember(LuaDaFuWongBaoXiangWnd, "m_HasOpen")
RegistClassMember(LuaDaFuWongBaoXiangWnd, "m_NeedPressTime")
RegistClassMember(LuaDaFuWongBaoXiangWnd, "m_BaoXiangTween")
RegistClassMember(LuaDaFuWongBaoXiangWnd, "m_ShowBaoXiangResultTick")
RegistClassMember(LuaDaFuWongBaoXiangWnd, "m_ShowBaoXianTime")
RegistClassMember(LuaDaFuWongBaoXiangWnd, "DesLabel_close")
RegistClassMember(LuaDaFuWongBaoXiangWnd, "DesLabel_open")
RegistClassMember(LuaDaFuWongBaoXiangWnd, "m_CurProgress")
RegistClassMember(LuaDaFuWongBaoXiangWnd, "m_LoopTick")
RegistClassMember(LuaDaFuWongBaoXiangWnd, "m_Anim")
RegistClassMember(LuaDaFuWongBaoXiangWnd, "m_Sound")
RegistClassMember(LuaDaFuWongBaoXiangWnd, "m_PlayerHead")
function LuaDaFuWongBaoXiangWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    UIEventListener.Get(self.Baoxiang.gameObject).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        self:OnBaoXiangPress(go, isPressed)
    end)
    self.m_StartPressTime = 0
    self.m_IsPress = false
    self.m_CanPress = false
    self.m_NeedPressTime = DaFuWeng_Setting.GetData().BaoXiangOperateTime
    self.m_BaoXiangTween = nil
    self.m_CurProgress = 0
    self.m_HasOpen = false
    self.m_ShowBaoXiangResultTick = nil
    self.m_ShowBaoXianTime = DaFuWeng_Setting.GetData().BaoXiangShowTime
    self.DesLabel_close = self.transform:Find("Anchor/Content/DesLabel_close"):GetComponent(typeof(UILabel))
    self.DesLabel_open = self.transform:Find("Anchor/Content/DesLabel_open"):GetComponent(typeof(UILabel))
    self.m_PlayerHead = self.transform:Find("Anchor/PlayerHead").gameObject
    self.m_Anim = self.gameObject:GetComponent(typeof(Animation))
end

function LuaDaFuWongBaoXiangWnd:Update()
    if self.m_IsPress then
        local value =  (CServerTimeMgr.Inst.timeStamp - self.m_StartPressTime) / self.m_NeedPressTime
        self.LongPressSprite.fillAmount = math.max(value,self.m_CurProgress)
        self.m_CurProgress = value
        if CServerTimeMgr.Inst.timeStamp - self.m_StartPressTime >= self.m_NeedPressTime then 
            -- 发RPC
            Gac2Gas.DaFuWengRandomEventOpenTreasure()
            self.LongPressSprite.gameObject:SetActive(false)
            --self.m_BaoXiangTween.enabled = false
            --self.m_BaoXiangTween:ResetToBeginning()
            --self:OnOpenBaoXiang(1)

            self.m_CanPress = false
            self.m_IsPress = false
        end
    end
end

function LuaDaFuWongBaoXiangWnd:Init()
    self.m_Sound = SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_Chest_1", Vector3.zero, nil, 0)
    self.DesLabel_close.gameObject:SetActive(true)
    self.DesLabel_open.gameObject:SetActive(false)
    self.CardTemplate.gameObject:SetActive(false)
    self.LongPressSprite.gameObject:SetActive(false)
    --self.m_BaoXiangTween = self.Baoxiang:GetComponent(typeof(TweenRotation))
    --self.m_BaoXiangTween.enabled = false
    Extensions.RemoveAllChildren(self.CardTable.transform)
    local totalTime = DaFuWeng_Countdown.GetData(EnumDaFuWengStage.RandomEventTreasure).Time 
    local CountDown = totalTime - self.m_ShowBaoXianTime - LuaDaFuWongMgr.GetCardTime
    local endFunc = function() self:OnOpenBaoXiang(EnumDaFuWengOperateEvent.BaoXiang) end
	local durationFunc = function(time) end
	LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.RandomEvent,CountDown,durationFunc,endFunc)
    if self.m_Anim then
        self.m_Anim:Play("dafuweng_baoxiang_appera")
    end
    if self.m_LoopTick then UnRegisterTick(self.m_LoopTick) self.m_LoopTick = nil end
    self.m_LoopTick = RegisterTickOnce(function()
        self.m_CanPress = true
        if self.m_Anim then
            self.m_Anim:Play("dafuweng_baoxiang_loop")
        end
        if self.m_Sound then
            SoundManager.Inst:StopSound(self.m_Sound)
            self.m_Sound = nil
        end
        self.m_Sound = SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_Chest_2", Vector3.zero, nil, 0)
    end,1200)
    self:ShowPlayerHead()
end

function LuaDaFuWongBaoXiangWnd:OnBaoXiangPress(go, isPressed)
    if not self.m_CanPress then 

        return
    end
    if not LuaDaFuWongMgr.IsMyRound then 
        local id = LuaDaFuWongMgr.CurRoundPlayer
        local name = LuaDaFuWongMgr.PlayerInfo[id].name
        g_MessageMgr:ShowMessage("DaFuWeng_OtherPlayerOperate",name)
        return
    end
    
    if isPressed then
        self.m_StartPressTime = CServerTimeMgr.Inst.timeStamp
        self.m_IsPress = true
        self.LongPressSprite.gameObject:SetActive(true)
        --self.m_BaoXiangTween.enabled = true
        --self.m_BaoXiangTween:ResetToBeginning()
        --self.m_BaoXiangTween:PlayForward()
    else
        self.m_CurProgress = 0
        self.m_StartPressTime = 0
        self.m_IsPress = false
        self.LongPressSprite.gameObject:SetActive(false)
        --self.m_BaoXiangTween.enabled = false
        --self.m_BaoXiangTween:ResetToBeginning()
    end

end

function LuaDaFuWongBaoXiangWnd:OnOpenBaoXiang(type)
    LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.RandomEvent)
    if type ~= EnumDaFuWengOperateEvent.BaoXiang then return end
    if self.m_HasOpen then return end
    self.m_CanPress = false
    self.m_HasOpen = true
    self.m_IsPress = false
    --self.m_BaoXiangTween.enabled = false
    self.LongPressSprite.gameObject:SetActive(false)
    --self.m_BaoXiangTween:ResetToBeginning()
    -- 播动效
    if self.m_Sound then
        SoundManager.Inst:StopSound(self.m_Sound)
        self.m_Sound = nil
    end
    self.m_Sound = SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_Chest_3", Vector3.zero, nil, 0)
    -- 显示道具
    local id = LuaDaFuWongMgr.CurRoundArgs[1]
    local ids = {}
    local rewardInfo = DaFuWeng_Treasure.GetData(id)
    if rewardInfo and rewardInfo.Card then
        for i = 0,rewardInfo.Card.Length - 1 do
            ids[rewardInfo.Card[i][0]] = rewardInfo.Card[i][1]
        end
    end
    local nametext = NewStringBuilderWraper(StringBuilder)
    local data = {}
    local cardList = {}
    cardList[1] = self.CardTemplate.gameObject
    cardList[2] = self.transform:Find("Anchor/Content/CardTemplate2").gameObject
    local index = 0
    for k,v in pairs(ids) do
        local cardInfo = DaFuWeng_Card.GetData(k)
        if v > 0 then
            index = index + 1
            for i = 1,v do
                table.insert(data,cardInfo)
            end
            if index ~= 1 then nametext:Append(LocalString.GetString("、")) end
            nametext:Append(cardInfo.Name)
            nametext:Append(LocalString.GetString(" * "))
            nametext:Append(tostring(v))
        end
        
    end
    for i = 1,2 do
        if data[i] then
            local go = cardList[i].gameObject
            go.gameObject:SetActive(true)
            go.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(data[i].Icon)
            go.transform:Find("Name"):GetComponent(typeof(UILabel)).text = LocalString.StrH2V(data[i].Name,true)
        else
            cardList[i].gameObject:SetActive(false)
        end
    end
    if #data <= 1 and self.m_Anim then
        self.m_Anim:Play("dafuweng_baoxiang_open1")
    elseif #data >= 2 and self.m_Anim then
        self.m_Anim:Play("dafuweng_baoxiang_open2")
    end
    --self.CardTable:GetComponent(typeof(UITable)):Reposition()
    self.DesLabel_close.gameObject:SetActive(false)
    self.DesLabel_open.gameObject:SetActive(true)
    self.DesLabel_open.text = g_MessageMgr:FormatMessage("DaFuWeng_ShowBaoXiangResult",nametext:ToString())

    if self.m_ShowBaoXiangResultTick then UnRegisterTick(self.m_ShowBaoXiangResultTick) self.m_ShowBaoXiangResultTick = nil end
    self.m_ShowBaoXiangResultTick = RegisterTickOnce(function()
        g_ScriptEvent:BroadcastInLua("OnDaFuWengFinishCurStage",EnumDaFuWengStage.RandomEventTreasure)
        CUIManager.CloseUI(CLuaUIResources.DaFuWongBaoXiangWnd)
    end,self.m_ShowBaoXianTime * 1000)
end

function LuaDaFuWongBaoXiangWnd:OnStageUpdate(CurStage)
    if CurStage ~= EnumDaFuWengStage.RandomEventTreasure then
        CUIManager.CloseUI(CLuaUIResources.DaFuWongBaoXiangWnd)
    end
end
function LuaDaFuWongBaoXiangWnd:ShowPlayerHead()
    if LuaDaFuWongMgr.IsMyRound then
        self.m_PlayerHead.gameObject:SetActive(false)
    else
        self.m_PlayerHead.gameObject:SetActive(true)
        local playerData = LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[LuaDaFuWongMgr.CurPlayerRound]
        local Portrait = self.m_PlayerHead.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
        local bg = self.m_PlayerHead.transform:Find("BG"):GetComponent(typeof(CUITexture))
        local Name = self.m_PlayerHead.transform:Find("Name"):GetComponent(typeof(UILabel))
        if playerData then
            Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(playerData.class, playerData.gender, -1), false)
			bg:LoadMaterial(LuaDaFuWongMgr:GetHeadBgPath(playerData.round))
            Name.transform:Find("Texture"):GetComponent(typeof(CUITexture)):LoadMaterial(LuaDaFuWongMgr:GetNameBgPath(playerData.round))
			Name.text = playerData.name
        end
    end
end
function LuaDaFuWongBaoXiangWnd:OnEnable()
    g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
    g_ScriptEvent:AddListener("OnDaFuWongPlayerOpreateEvent",self,"OnOpenBaoXiang")
    if self.m_Sound then
        SoundManager.Inst:StopSound(self.m_Sound)
        self.m_Sound = nil
    end
end

function LuaDaFuWongBaoXiangWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
    g_ScriptEvent:RemoveListener("OnDaFuWongPlayerOpreateEvent",self,"OnOpenBaoXiang")
    LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.RandomEvent)
    if self.m_ShowBaoXiangResultTick then UnRegisterTick(self.m_ShowBaoXiangResultTick) self.m_ShowBaoXiangResultTick = nil end
    if self.m_LoopTick then UnRegisterTick(self.m_LoopTick) self.m_LoopTick = nil end
end

--@region UIEvent

--@endregion UIEvent

