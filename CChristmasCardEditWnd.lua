-- Auto Generated!!
local BoolDelegate = import "UIEventListener+BoolDelegate"
local CChristmasCardEditWnd = import "L10.UI.CChristmasCardEditWnd"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local CIMMgr = import "L10.Game.CIMMgr"
local CRecordingInfoMgr = import "L10.UI.CRecordingInfoMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local DelegateFactory = import "DelegateFactory"
local Double = import "System.Double"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumEventType = import "EnumEventType"
local EnumRecordContext = import "L10.Game.EnumRecordContext"
local EnumVoicePlatform = import "L10.Game.EnumVoicePlatform"
local EventManager = import "EventManager"
local LocalString = import "LocalString"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt64 = import "System.UInt64"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local VoicePlayInfo = import "L10.Game.VoicePlayInfo"
local CTickMgr = import "L10.Engine.CTickMgr"
local ETickType = import "L10.Engine.ETickType"
CChristmasCardEditWnd.m_Init_CS2LuaHook = function (this) 
    this.m_PlayerId = 0
    this.m_PlayerName = ""
    if CChristmasCardEditWnd.s_PlayerId ~= 0 then
        this.m_PlayerId = CChristmasCardEditWnd.s_PlayerId
    end
    if not System.String.IsNullOrEmpty(CChristmasCardEditWnd.s_PlayerName) then
        this.m_PlayerName = CChristmasCardEditWnd.s_PlayerName
        this.m_PlayerNameLabel.text = this.m_PlayerName
    end

    this.m_VoiceId = ""
    this.m_VoiceDuration = 0
    if CClientMainPlayer.Inst ~= nil then
        this.m_PortraitTexture:Init(CClientMainPlayer.Inst.Id, CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender)
    end
    this:SetVoiceUI()
    this.m_SenderLabel.text = CClientMainPlayer.Inst.Name .. LocalString.GetString(" 送")
end
CChristmasCardEditWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_GenerateCardButton).onClick = MakeDelegateFromCSFunction(this.OnGenerateCardButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.m_CloseButton).onClick = MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.m_SearchButton).onClick = MakeDelegateFromCSFunction(this.OnSearchButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.m_SelectFriendButton).onClick = MakeDelegateFromCSFunction(this.OnSelectFriendButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.m_VoiceButton).onPress = MakeDelegateFromCSFunction(this.OnVoiceButtonPress, BoolDelegate, this)
    UIEventListener.Get(this.m_ListenVoiceButton).onClick = MakeDelegateFromCSFunction(this.OnPlayVoice, VoidDelegate, this)
    EventManager.AddListenerInternal(EnumEventType.SelectPlayer, MakeDelegateFromCSFunction(this.OnSelectPlayer, MakeGenericClass(Action2, UInt64, String), this))
    EventManager.AddListenerInternal(EnumEventType.UploadChristmasCardVoiceFinished, MakeDelegateFromCSFunction(this.OnUploadVoiceSuccess, MakeGenericClass(Action3, String, Double, EnumRecordContext), this))
end
CChristmasCardEditWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_CloseButton).onClick = nil
    UIEventListener.Get(this.m_SearchButton).onClick = nil
    UIEventListener.Get(this.m_SelectFriendButton).onClick = nil
    UIEventListener.Get(this.m_VoiceButton).onClick = nil
    EventManager.RemoveListenerInternal(EnumEventType.SelectPlayer, MakeDelegateFromCSFunction(this.OnSelectPlayer, MakeGenericClass(Action2, UInt64, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.UploadChristmasCardVoiceFinished, MakeDelegateFromCSFunction(this.OnUploadVoiceSuccess, MakeGenericClass(Action3, String, Double, EnumRecordContext), this))
end
CChristmasCardEditWnd.m_SetVoiceUI_CS2LuaHook = function (this) 
    if System.String.IsNullOrEmpty(this.m_VoiceId) then
        this.m_HasVoiceRoot.gameObject:SetActive(false)
        this.m_NoVoiceTipLabel.gameObject:SetActive(true)
    else
        this.m_HasVoiceRoot.gameObject:SetActive(true)
        this.m_NoVoiceTipLabel.gameObject:SetActive(false)
        this.m_SpriteAnimation:Pause()
        this.m_VoiceLengthLabel.text = System.String.Format(LocalString.GetString("{0:N0}秒"), this.m_VoiceDuration)
        this.m_VoiceLengthSprite.width = math.min(800, math.floor(tonumber(this.m_VoiceDuration * 80 or 0)))
    end
end
CChristmasCardEditWnd.m_OnVoiceButtonPress_CS2LuaHook = function (this, go, pressed) 
    if pressed then
        CRecordingInfoMgr.ShowRecordingWnd(EnumRecordContext.ChristmasCard, EChatPanel.Undefined, go, EnumVoicePlatform.Default, false)
    else
        CRecordingInfoMgr.CloseRecordingWnd(not go:Equals(CUICommonDef.SelectedUI))
    end
end
CChristmasCardEditWnd.m_OnUploadVoiceSuccess_CS2LuaHook = function (this, voiceId, voiceDuration, context) 
    if context == EnumRecordContext.ChristmasCard then
        this.m_VoiceId = voiceId
        this.m_VoiceDuration = voiceDuration
        this:SetVoiceUI()
    end
end
CChristmasCardEditWnd.m_OnGenerateCardButtonClick_CS2LuaHook = function (this, go) 
    if this.m_PlayerId == 0 or System.String.IsNullOrEmpty(this.m_PlayerName) then
        g_MessageMgr:ShowMessage("SEND_PLAYER_IS_EMPTY")
        return
    end
    if System.String.IsNullOrEmpty(this.m_WishContentInput.Text) then
        g_MessageMgr:ShowMessage("WISHCONTENT_IS_EMPTY")
        return
    end
    if CommonDefs.StringLength(this.m_WishContentInput.Text) > 30 then
        g_MessageMgr:ShowMessage("WISHCONTENT_IS_TOO_LONG")
        return
    end
    local ret = CWordFilterMgr.Inst:DoFilterOnSend(this.m_WishContentInput.Text, nil, nil, true)
    local msg = ret.msg
    if msg == nil then
        return
    end

    Gac2Gas.RequestEditCard(CChristmasCardEditWnd.s_ItemId, CChristmasCardEditWnd.s_Place, CChristmasCardEditWnd.s_Pos, this.m_PlayerId, this.m_PlayerName, msg, this.m_VoiceId, this.m_VoiceDuration)
    this:Close()
end
CChristmasCardEditWnd.m_OnSelectFriendButtonClick_CS2LuaHook = function (this, go) 
    CCommonPlayerListMgr.Inst:ShowFriendsToGivePresent(DelegateFactory.Action_ulong(function (playerId) 
        this.m_PlayerId = playerId
        local basicInfo = CIMMgr.Inst:GetBasicInfo(playerId)
        this.m_PlayerName = basicInfo.Name
        this.m_PlayerNameLabel.text = basicInfo.Name
    end))
end
CChristmasCardEditWnd.m_hookOnPlayVoice = function (this, go) 
    this.m_SpriteAnimation:Play()
    CTickMgr.Register(DelegateFactory.Action(function () 
        if this ~= nil and this.m_SpriteAnimation ~= nil then
            this.m_SpriteAnimation:Pause()
            this.m_SpriteAnimation.gameObject:GetComponent(typeof(UISprite)).spriteName = "chatwnd_voice_icon_2"
        end
    end), 1000 * this.m_VoiceDuration, ETickType.Once)
    CUIManager.PlayVoice(CreateFromClass(VoicePlayInfo, this.m_VoiceId, EnumVoicePlatform.Default, nil))
end
