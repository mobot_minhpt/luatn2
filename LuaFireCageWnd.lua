local Object = import "System.Object"

local Input                 = import "UnityEngine.Input"

local CameraFollow          = import "L10.Engine.CameraControl.CameraFollow"
local CMainCamera           = import "L10.Engine.CMainCamera"
local CEffectMgr            = import "L10.Game.CEffectMgr"
local EnumWarnFXType        = import "L10.Game.EnumWarnFXType"
local CPos                  = import "L10.Engine.CPos"
local EUIModuleGroup        = import "L10.UI.CUIManager+EUIModuleGroup"
local Vector3               = import "UnityEngine.Vector3"

local Physics               = import "UnityEngine.Physics"
local Tags                  = import "L10.Game.Tags"
local UIWidget              = import "UIWidget"

LuaFireCageWnd = class()
--------RegistChildComponent-------
RegistChildComponent(LuaFireCageWnd,    "TouchPanel",       GameObject)         --  长按按钮
RegistChildComponent(LuaFireCageWnd,    "UIEffect",         GameObject)         --  火烧全屏特效
RegistChildComponent(LuaFireCageWnd,    "Tips",             GameObject)         --  tips
RegistChildComponent(LuaFireCageWnd,   "Sprite",           UIWidget)

---------RegistClassMember-------
RegistClassMember(LuaFireCageWnd,       "m_Create")         -- 目前创建的数量
RegistClassMember(LuaFireCageWnd,       "m_CompleteNum")    -- 完成所需创建的数量
RegistClassMember(LuaFireCageWnd,       "m_MainCamera")
RegistClassMember(LuaFireCageWnd,       "m_PosTable")       -- 所有已存在特效的坐标
RegistClassMember(LuaFireCageWnd,       "m_MinDis")         -- 特效间的最小距离
RegistClassMember(LuaFireCageWnd,       "m_IsOver")         -- 是否完成

RegistClassMember(LuaFireCageWnd,      "m_TimeTick")

function LuaFireCageWnd:Awake()
    self.m_Create = 0
    self.m_CompleteNum = 7
    self.m_MainCamera = CMainCamera.Main
    self.m_PosTable = {}
    self.m_MinDis = 3
    self.m_IsOver = false
    self.UIEffect:SetActive(false)
    self.Sprite.alpha = 0

    UIEventListener.Get(self.TouchPanel).onClick = DelegateFactory.VoidDelegate(function(p)
        self:OnTouch()
    end)
    UIEventListener.Get(self.TouchPanel).onDrag = DelegateFactory.VectorDelegate(function(go,delta)
        self:OnTouch()
    end)

end

function LuaFireCageWnd:OnEnable()
    local excepts = {"MiddleNoticeCenter"}
    local List_String = MakeGenericClass(List,cs_string)
    CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
    
    CameraFollow.Inst:StopFollow()
    CameraFollow.Inst.transform.localPosition = Vector3(110,49,73)
    CameraFollow.Inst.rotateObj.eulerAngles = Vector3(38,25,0)
    CClientMainPlayer.Inst.Visible = false
end 

function LuaFireCageWnd:OnDisable()
    UnRegisterTick(self.m_TimeTick)
    if CameraFollow.Inst ~= nil and CClientMainPlayer.Inst ~= nil then
        CameraFollow.Inst:SetFollowObj(CClientMainPlayer.Inst.RO, nil)
        CClientMainPlayer.Inst.Visible = true
    end
    local excepts = {"MiddleNoticeCenter"}
    local List_String = MakeGenericClass(List,cs_string)
    CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
end

function LuaFireCageWnd:OnTouch()
    if self.m_IsOver then return end        --  一旦完成任务就不在继续监测
    if self.Tips.activeSelf then
        self.Tips:SetActive(false)
    end
    local ray = self.m_MainCamera:ScreenPointToRay(Input.mousePosition)
    local dist = self.m_MainCamera.farClipPlane - self.m_MainCamera.nearClipPlane
    local hits = Physics.RaycastAll(ray, dist, self.m_MainCamera.cullingMask)
    local pos
    local minDis = 0
    for i=0,hits.Length-1 do
        local collider = hits[i].collider.gameObject
        if collider:CompareTag(Tags.Ground) then
            if minDis == 0 then
                minDis = hits[i].distance
                pos = hits[i].point
            else
                if hits[i].distance < minDis then
                    minDis = hits[i].distance
                    pos = hits[i].point
                end
            end
        end
    end
    if pos then
        self:FireByTouch(pos)
    end
end

function LuaFireCageWnd:FireByTouch(pos)      --  播放特效
    if self:CheckDis(pos) then
        self:CheckOver()
        local fxid = 88801337
        if self.m_Create % 3 == 1 then
            fxid = 88801338
        end
        local target = CPos(pos.x*64,pos.z*64)
        local fx = CEffectMgr.Inst:AddWorldPositionFX(fxid,target,0,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
    end
end

function LuaFireCageWnd:CheckDis(pos)
    for i=1,#self.m_PosTable do
        if Vector3.Distance(self.m_PosTable[i],pos) < self.m_MinDis then
            return false
        end
    end
    table.insert( self.m_PosTable, pos )
    return true
end

function LuaFireCageWnd:CheckOver()        --  检测特效是否创建完了
    self.m_Create = self.m_Create + 1
    if self.m_Create >= self.m_CompleteNum then
        self.m_IsOver = true
        self.UIEffect:SetActive(true)
        self:ShouComplete()
    end
end

LuaFireCageWnd.taskId = 0
function LuaFireCageWnd:ShouComplete()
    self.m_TimeTick = RegisterTickOnce(function ()
        LuaTweenUtils.TweenAlpha(self.Sprite, 0,1.5, 4,function() 
            CUIManager.CloseUI(CLuaUIResources.FireCageWnd)
            local empty = CreateFromClass(MakeGenericClass(List, Object))
            Gac2Gas.FinishClientTaskEvent(LuaFireCageWnd.taskId,"FireCage",MsgPackImpl.pack(empty))
        end)
    end, 3 * 1000)
end
