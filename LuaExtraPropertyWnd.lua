local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local CChatLinkMgr = import "CChatLinkMgr"

LuaExtraPropertyWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaExtraPropertyWnd, "OpenWndButton", "OpenWndButton", GameObject)
RegistChildComponent(LuaExtraPropertyWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaExtraPropertyWnd, "CorLabel", "CorLabel", UILabel)
RegistChildComponent(LuaExtraPropertyWnd, "StaLabel", "StaLabel", UILabel)
RegistChildComponent(LuaExtraPropertyWnd, "StrLabel", "StrLabel", UILabel)
RegistChildComponent(LuaExtraPropertyWnd, "IntLabel", "IntLabel", UILabel)
RegistChildComponent(LuaExtraPropertyWnd, "AgiLabel", "AgiLabel", UILabel)

--@endregion RegistChildComponent end

function LuaExtraPropertyWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.OpenWndButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOpenWndButtonClick()
	end)


	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


    --@endregion EventBind end
end

function LuaExtraPropertyWnd:Init()
	if CClientMainPlayer.Inst then
		local fightProp = CClientMainPlayer.Inst.FightProp
		local c1,c2,c3,c4,c5,v1,v2,v3,v4,v5 = 
			math.floor(fightProp:GetParam(EPlayerFightProp.PermanentCor) + fightProp:GetParam(EPlayerFightProp.RevisePermanentCor)),
			math.floor(fightProp:GetParam(EPlayerFightProp.PermanentSta) + fightProp:GetParam(EPlayerFightProp.RevisePermanentSta)),
			math.floor(fightProp:GetParam(EPlayerFightProp.PermanentStr) + fightProp:GetParam(EPlayerFightProp.RevisePermanentStr)),
			math.floor(fightProp:GetParam(EPlayerFightProp.PermanentInt) + fightProp:GetParam(EPlayerFightProp.RevisePermanentInt)),
			math.floor(fightProp:GetParam(EPlayerFightProp.PermanentAgi) + fightProp:GetParam(EPlayerFightProp.RevisePermanentAgi)),
			math.floor(fightProp:GetParam(EPlayerFightProp.AdjustPermanentCor)),
			math.floor(fightProp:GetParam(EPlayerFightProp.AdjustPermanentSta)),
			math.floor(fightProp:GetParam(EPlayerFightProp.AdjustPermanentStr)),
			math.floor(fightProp:GetParam(EPlayerFightProp.AdjustPermanentInt)),
			math.floor(fightProp:GetParam(EPlayerFightProp.AdjustPermanentAgi))
		self.CorLabel.text = CChatLinkMgr.TranslateToNGUIText(c1 == v1 and c1 or SafeStringFormat("[ff7900]%d[ffffff](%d)", v1, c1))
		self.StaLabel.text = CChatLinkMgr.TranslateToNGUIText(c2 == v2 and c2 or SafeStringFormat("[ff7900]%d[ffffff](%d)", v2, c2)) 
		self.StrLabel.text = CChatLinkMgr.TranslateToNGUIText(c3 == v3 and c3 or SafeStringFormat("[ff7900]%d[ffffff](%d)", v3, c3))
		self.IntLabel.text = CChatLinkMgr.TranslateToNGUIText(c4 == v4 and c4 or SafeStringFormat("[ff7900]%d[ffffff](%d)", v4, c4)) 
		self.AgiLabel.text = CChatLinkMgr.TranslateToNGUIText(c5 == v5 and c5 or SafeStringFormat("[ff7900]%d[ffffff](%d)", v5, c5))
	end
end

--@region UIEvent

function LuaExtraPropertyWnd:OnOpenWndButtonClick()
	CUIManager.ShowUI(CLuaUIResources.WashExtraPropertyWnd)
	CUIManager.CloseUI(CLuaUIResources.ExtraPropertyWnd)
end

function LuaExtraPropertyWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("ExtraPropertyWnd_ReadMe")
end

--@endregion UIEvent

