

LuaDragonBoatGamePlayWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDragonBoatGamePlayWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaDragonBoatGamePlayWnd, "RuleLabel", "RuleLabel", UILabel)
RegistChildComponent(LuaDragonBoatGamePlayWnd, "JoinButton", "JoinButton", GameObject)
RegistChildComponent(LuaDragonBoatGamePlayWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaDragonBoatGamePlayWnd, "RewardNumLabel", "RewardNumLabel", UILabel)
RegistChildComponent(LuaDragonBoatGamePlayWnd, "CancelButton", "CancelButton", GameObject)

--@endregion RegistChildComponent end

function LuaDragonBoatGamePlayWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.JoinButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJoinButtonClick()
	end)


	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.CancelButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelButtonClick()
	end)


    --@endregion EventBind end
end

function LuaDragonBoatGamePlayWnd:Init()
	self.TimeLabel.text = g_MessageMgr:FormatMessage("DragonBoatGamePlayWnd_Time")
	self.RuleLabel.text = g_MessageMgr:FormatMessage("DragonBoatGamePlayWnd_Rule")
	self.CancelButton.gameObject:SetActive(false)
	self.JoinButton.gameObject:SetActive(true)
	Gac2Gas.OpenLongZhouPlaySignUpWnd()
end

--@region UIEvent

function LuaDragonBoatGamePlayWnd:OnJoinButtonClick()
	Gac2Gas.RequestSignUpLongZhouPlay()
end


function LuaDragonBoatGamePlayWnd:OnTipButtonClick()
	LuaDuanWu2021Mgr:ShowOffWorldPlay2TipWnd()
end

function LuaDragonBoatGamePlayWnd:OnCancelButtonClick()
	Gac2Gas.RequestCancelSignUpLongZhouPlay()
end


--@endregion UIEvent

function LuaDragonBoatGamePlayWnd:OnEnable()
	g_ScriptEvent:AddListener("OnSyncLongZhouPlaySignUpResult", self, "OnLoadData")
end

function LuaDragonBoatGamePlayWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSyncLongZhouPlaySignUpResult", self, "OnLoadData")
end

function LuaDragonBoatGamePlayWnd:OnLoadData(isJoined,num)
	if num then
		self.RewardNumLabel.text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3(num == 0 and LocalString.GetString("[#ff5050]%d")
		or LocalString.GetString("%d"), num))
	end
	self.JoinButton.gameObject:SetActive(not isJoined)
	self.CancelButton.gameObject:SetActive(isJoined)
end
