local Collider = import "UnityEngine.Collider"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local Money = import "L10.Game.Money"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local PlayerSettings = import "L10.Game.PlayerSettings"

--代言人雇佣契约
LuaSpokesmanHireContractWnd = class()

RegistChildComponent(LuaSpokesmanHireContractWnd, "m_Reward","Reward",QnButton)
RegistChildComponent(LuaSpokesmanHireContractWnd, "m_RewardFx","RewardFx",CUIFx)
RegistChildComponent(LuaSpokesmanHireContractWnd, "m_ScrollView","ScrollView",UIScrollView)
RegistChildComponent(LuaSpokesmanHireContractWnd, "m_Template","Template",GameObject)
RegistChildComponent(LuaSpokesmanHireContractWnd, "m_WordTextLabel","WordTextLabel",UILabel)
RegistChildComponent(LuaSpokesmanHireContractWnd, "m_Grid","Grid",UIGrid)
RegistChildComponent(LuaSpokesmanHireContractWnd, "m_GetRewardSprite","GetRewardSprite",GameObject)

RegistClassMember(LuaSpokesmanHireContractWnd, "m_List")
RegistClassMember(LuaSpokesmanHireContractWnd, "m_AllFinished")
RegistClassMember(LuaSpokesmanHireContractWnd, "m_Sound")
RegistClassMember(LuaSpokesmanHireContractWnd, "m_FirstGuideButton")
RegistClassMember(LuaSpokesmanHireContractWnd, "m_SecondGuideButton")
RegistClassMember(LuaSpokesmanHireContractWnd, "m_CanGetAllFinishedReward")
RegistClassMember(LuaSpokesmanHireContractWnd, "m_LastAudioId")
RegistClassMember(LuaSpokesmanHireContractWnd, "m_FirstUnRewardId")
function LuaSpokesmanHireContractWnd:Init()
    self.m_RewardFx:LoadFx("fx/ui/prefab/UI_fx_yellow.prefab")
    self.m_RewardFx.gameObject:SetActive(false)
    self.m_CanGetAllFinishedReward = false
    self.m_Template:SetActive(false)
    self.m_WordTextLabel.gameObject:SetActive(false)
    self.m_GetRewardSprite:SetActive(false)
    Gac2Gas.QuerySpokesmanContractDetail()
end

function LuaSpokesmanHireContractWnd:OnEnable()
    g_ScriptEvent:AddListener("SpokesmanHireContractWnd_UpdatenContract",self,"OnUpdateContract")
    g_ScriptEvent:AddListener("SpokesmanHireContractWnd_LoadData",self,"LoadData")
    g_ScriptEvent:AddListener("SpokesmanHireContractWnd_ContractAwardSuccess",self,"OnAwardSuccess")
    g_ScriptEvent:AddListener("SpokesmanHireContractWnd_AllContractAwardSuccess",self,"OnAllContractAwardSuccess")
end

function LuaSpokesmanHireContractWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SpokesmanHireContractWnd_UpdatenContract",self,"OnUpdateContract")
    g_ScriptEvent:RemoveListener("SpokesmanHireContractWnd_LoadData",self,"LoadData")
    g_ScriptEvent:RemoveListener("SpokesmanHireContractWnd_ContractAwardSuccess",self,"OnAwardSuccess")
    g_ScriptEvent:RemoveListener("SpokesmanHireContractWnd_AllContractAwardSuccess",self,"OnAllContractAwardSuccess")
    if self.m_List then
        for _,t in pairs(self.m_List) do
            if t.tick then
                UnRegisterTick(t.tick)
                t.tick = nil
            end
        end
    end
    self:StopAudio(self.m_LastAudioId)
end

function LuaSpokesmanHireContractWnd:OnAwardSuccess(contractId)
    local t = self.m_List[contractId]
    t.rewardFx:SetActive(false)
    t.getRewardSprite:SetActive(true)
    t.isFinish = true
    --self:OnAudioPlay(contractId)
end

function LuaSpokesmanHireContractWnd:OnAllContractAwardSuccess()
    self.m_RewardFx.gameObject:SetActive(false)
    self.m_CanGetAllFinishedReward = false
    self.m_GetRewardSprite:SetActive(true)
end

function LuaSpokesmanHireContractWnd:OnUpdateContract(contractId, progress)
    local t = self.m_List[contractId]
    local isLock = progress < t.contract.NeedProgress
    t.audioBtn:SetActive(not isLock)
    t.lockSprite:SetActive(isLock)
    t.progressLabel.text = SafeStringFormat3("%d/%d",progress, t.contract.NeedProgress)
    t.progressSprite.fillAmount = (t.contract.NeedProgress ~= 0) and (progress / t.contract.NeedProgress) or 0
    t.finishSprite:SetActive(not isLock)
    t.rewardFx:SetActive(not isLock)
end

function LuaSpokesmanHireContractWnd:OnAudioPlay(id)
    local t = self.m_List[id]
    if not t then return end
    if PlayerSettings.VolumeSetting == 0 then
        PlayerSettings.VolumeSetting = 1
    end
    PlayerSettings.SoundEnabled = true
    self:StopAudio(self.m_LastAudioId)
    self.m_LastAudioId = id
    self.m_Sound = SoundManager.Inst:PlayOneShot(t.contract.Audio, Vector3.zero, nil, 0, -1)
    self.m_WordTextLabel.text = t.contract.AudioWords
    self.m_WordTextLabel.gameObject:SetActive(true)
    t.audioBtnBg.enabled = true
    t.audioSprite:SetActive(true)
    local _id = id
    if self.m_List[_id].tick then
        UnRegisterTick(self.m_List[_id].tick)
        self.m_List[_id].tick = nil
    end
    self.m_List[_id].tick = RegisterTickOnce(function ()
        self:StopAudio(_id)
    end,t.contract.AudioLength * 1000)
end

function LuaSpokesmanHireContractWnd:StopAudio(id)
    if self.m_Sound then
        SoundManager.Inst:StopSound(self.m_Sound)
        self.m_Sound = nil
    end
    if not id then return end
    if self.m_List[id].tick then
        UnRegisterTick(self.m_List[id].tick)
        self.m_List[id].tick = nil
    end
    local t = self.m_List[id]
    self.m_WordTextLabel.gameObject:SetActive(false)
    t.audioBtnBg.enabled = false
    t.audioSprite:SetActive(false)
end

function LuaSpokesmanHireContractWnd:LoadData(data)
    self.m_List = {}
    local allAwarded = data.allAwarded
    self.m_AllFinished = true
    Extensions.RemoveAllChildren(self.m_Grid.transform)
    local len = #data.list
    for i = 1,len do
        self:InitTemplate(data, i)
    end
    self.m_Grid:Reposition()
    self.m_ScrollView:ResetPosition()
    self.m_WordTextLabel.gameObject:SetActive(false)
    self.m_RewardFx.gameObject:SetActive(self.m_AllFinished and (not allAwarded))
    self.m_CanGetAllFinishedReward = (self.m_AllFinished and (not allAwarded))
    self.m_GetRewardSprite:SetActive(self.m_AllFinished and allAwarded)
    local contractFinishAllAwardItemTemplateId = Spokesman_Setting.GetData().ContractFinishAllAwardItem[0]
    UIEventListener.Get(self.m_Reward.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        if CGuideMgr.Inst:IsInPhase(EnumGuideKey.SpokesmanHireContractWndGuide) then
            return
        end
        if not self.m_CanGetAllFinishedReward then
            CItemInfoMgr.ShowLinkItemTemplateInfo(contractFinishAllAwardItemTemplateId)
            return
        end
        Gac2Gas.RequestFinishAllSpokesmanContractAward()
    end)
    self:ScrollToReward()

    --self.m_ScrollView:SetDragAmount(0,(len== 0) and 0 or (firstUnfinished / len),false)
    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.SpokesmanHireContractWndGuide)
end

function LuaSpokesmanHireContractWnd:InitTemplateComponent()
    local t = {}
    local obj = NGUITools.AddChild(self.m_Grid.gameObject,self.m_Template)
    obj:SetActive(true)
    t.progressSprite = obj.transform:Find("Reward/ProgressSprite"):GetComponent(typeof(UITexture))
    t.audioBtn = obj.transform:Find("AudioBtn").gameObject
    t.audioSprite = obj.transform:Find("AudioBtn/AudioSprite").gameObject
    t.audioSprite:SetActive(false)
    t.audioBtnBg = obj.transform:Find("AudioBtn/AudioBtnBg"):GetComponent(typeof(UISprite))
    t.audioBtnBg.enabled = false
    t.lockSprite = obj.transform:Find("LockSprite").gameObject
    t.titleLabel = obj.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
    t.finishSprite = obj.transform:Find("TitleLabel/FinishSprite").gameObject
    t.finishSprite:SetActive(false)
    t.desLabel = obj.transform:Find("DesLabel"):GetComponent(typeof(UILabel))
    t.progressLabel = obj.transform:Find("ProgressLabel"):GetComponent(typeof(UILabel))
    t.rewardFx = obj.transform:Find("Fx").gameObject
    t.rewardBtn = obj.transform:Find("Reward"):GetComponent(typeof(Collider))
    t.rewardSprite = obj.transform:Find("Reward"):GetComponent(typeof(UISprite))
    t.getRewardSprite  = obj.transform:Find("GetRewardSprite").gameObject
    return t
end

function LuaSpokesmanHireContractWnd:InitTemplate(data, i)
    local t = self:InitTemplateComponent()

    local d = data.list[i]
    local id = d.contractId
    local contract = Spokesman_Contract.GetData(id)
    local isLock = d.progress < contract.NeedProgress
    local progress = d.progress
    local isFinish = d.isAwarded
    self.m_AllFinished = self.m_AllFinished and (not isLock)
    self.m_List[id] = t
    t.rewardSprite.spriteName = Money.GetIconName(CommonDefs.ConvertIntToEnum(typeof(EnumMoneyType), contract.RewardType),EnumPlayScoreKey.NONE)
    local index = i
    UIEventListener.Get(t.audioBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnAudioBtnClick(index,id)
    end)
    UIEventListener.Get(t.rewardBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnTaskRewardBtnClick(index,id)
    end)

    if i == 1 then
        self.m_FirstGuideButton = t.rewardBtn.gameObject
        self.m_SecondGuideButton = isLock and t.lockSprite.gameObject or t.audioBtn.gameObject
    end

    t.isLock = isLock
    t.isFinish = isFinish
    t.contract = contract
    t.audioBtn:SetActive(not isLock)
    t.lockSprite:SetActive(isLock)
    t.titleLabel.text = contract.Title
    t.desLabel.text = contract.Desc
    t.progressLabel.text = SafeStringFormat3("%d/%d",progress, contract.NeedProgress)
    t.progressSprite.fillAmount = (contract.NeedProgress ~= 0) and (progress / contract.NeedProgress) or 0
    t.finishSprite:SetActive(not isLock)
    t.rewardFx:SetActive((not isFinish) and (not isLock))
    if not self.m_FirstUnRewardId and (not isFinish) and (not isLock) then
        self.m_FirstUnRewardId = i
    end
    t.getRewardSprite:SetActive(isFinish)
end

function LuaSpokesmanHireContractWnd:OnAudioBtnClick(index,id)
    if index == 1 and CGuideMgr.Inst:IsInPhase(EnumGuideKey.SpokesmanHireContractWndGuide) then
        return
    end
    self:OnAudioPlay(id)
end

function LuaSpokesmanHireContractWnd:OnTaskRewardBtnClick(index,id)
    local _t = self.m_List[id]
    if not _t.isLock and not _t.isFinish then
        Gac2Gas.RequestFinishContractAward(id)
    else
        if CGuideMgr.Inst:IsInPhase(EnumGuideKey.SpokesmanHireContractWndGuide) then
            return
        end
        if _t.contract then
            CItemInfoMgr.ShowLinkItemTemplateInfo(_t.contract.AwardItem[0])
        end
    end
end

function LuaSpokesmanHireContractWnd:GetGuideGo(methodName)
    if methodName == "GetFirstGuideButton" then
        return self.m_FirstGuideButton
    elseif methodName == "GetSecondGuideButton" then
        return self.m_SecondGuideButton
    elseif methodName == "GetThirdGuideButton" then
        return self.m_Reward.gameObject
    end
end


function LuaSpokesmanHireContractWnd:ScrollToReward()
    if not self.m_FirstUnRewardId then return end
    local height = self.m_Grid.cellHeight
    local offset = math.min((self.m_FirstUnRewardId - 1) * height / (#self.m_List * height - self.m_ScrollView.panel.height), 1)
    self.m_ScrollView:SetDragAmount(0, offset, false)
end