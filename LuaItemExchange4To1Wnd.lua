local CItemExchangeMaterial = import "L10.UI.CItemExchangeMaterial"
local UITweener = import "UITweener"
local CUIFx = import "L10.UI.CUIFx"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local Item_Item = import "L10.Game.Item_Item"

LuaItemExchange4To1Wnd = class()

RegistChildComponent(LuaItemExchange4To1Wnd, "m_ItemConsume1","ItemConsume1",CItemExchangeMaterial)
RegistChildComponent(LuaItemExchange4To1Wnd, "m_ItemConsume2","ItemConsume2",CItemExchangeMaterial)
RegistChildComponent(LuaItemExchange4To1Wnd, "m_ItemConsume3","ItemConsume3",CItemExchangeMaterial)
RegistChildComponent(LuaItemExchange4To1Wnd, "m_ItemConsume4","ItemConsume4",CItemExchangeMaterial)
RegistChildComponent(LuaItemExchange4To1Wnd, "m_ItemResult","ItemResult",CUITexture)
RegistChildComponent(LuaItemExchange4To1Wnd, "m_Button","Button",GameObject)
RegistChildComponent(LuaItemExchange4To1Wnd, "m_Tweener1","Fx1",UITweener)
RegistChildComponent(LuaItemExchange4To1Wnd, "m_Tweener2","Fx2",UITweener)
RegistChildComponent(LuaItemExchange4To1Wnd, "m_Tweener3","Fx3",UITweener)
RegistChildComponent(LuaItemExchange4To1Wnd, "m_Tweener4","Fx4",UITweener)
RegistChildComponent(LuaItemExchange4To1Wnd, "m_Tweener4","Fx4",UITweener)
RegistChildComponent(LuaItemExchange4To1Wnd, "m_CountInput","CountInput",QnAddSubAndInputButton)

RegistClassMember(LuaItemExchange4To1Wnd, "m_Tick")
RegistClassMember(LuaItemExchange4To1Wnd, "m_Btneable")
RegistClassMember(LuaItemExchange4To1Wnd, "m_Fxs")
RegistClassMember(LuaItemExchange4To1Wnd, "m_Tweeners")
RegistClassMember(LuaItemExchange4To1Wnd, "m_Data")

function LuaItemExchange4To1Wnd:Init()
    self.m_Data = Exchange_Exchange.GetData(g_ComposeMgr.m_ExchangeId)
    self:InitItemsAndBtn()
    UIEventListener.Get(self.m_Button.gameObject).onClick =DelegateFactory.VoidDelegate(function()
        self:OnButtonClick()
    end)
    self.m_Tick = nil

    self.m_Tweeners = {}
    self.m_Tweeners[1] = self.m_Tweener1
    self.m_Tweeners[2] = self.m_Tweener2
    self.m_Tweeners[3] = self.m_Tweener3
    self.m_Tweeners[4] = self.m_Tweener4
    self.m_Fxs = {}
    for i=1, #self.m_Tweeners do
        self.m_Fxs[i] = self.m_Tweeners[i].gameObject:GetComponent(typeof(CUIFx))
    end

    self.m_CountInput:SetValue(1)

    local resultItemId = self.m_Data.NewItems[0][0]
    local itemData = Item_Item.GetData(resultItemId)
    self.m_ItemResult:LoadMaterial(itemData.Icon)
    UIEventListener.Get(self.m_ItemResult.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(resultItemId);
    end)
end

function LuaItemExchange4To1Wnd:InitItemsAndBtn()
    self.m_ItemConsume1:Init(self.m_Data.ConsumeItems[0][0],self.m_Data.ConsumeItems[0][1],1, true)
    self.m_ItemConsume2:Init(self.m_Data.ConsumeItems[1][0],self.m_Data.ConsumeItems[1][1],1, true)
    self.m_ItemConsume3:Init(self.m_Data.ConsumeItems[2][0],self.m_Data.ConsumeItems[2][1],1, true)
    self.m_ItemConsume4:Init(self.m_Data.ConsumeItems[3][0],self.m_Data.ConsumeItems[3][1],1, true)
    self.m_Tweener1.enabled = false
    self.m_Tweener2.enabled = false
    self.m_Tweener3.enabled = false
    self.m_Tweener4.enabled = false
    
    local maxCompose = math.min(self.m_ItemConsume1.maxComposeCount, math.min(self.m_ItemConsume2.maxComposeCount, math.min(self.m_ItemConsume3.maxComposeCount, self.m_ItemConsume4.maxComposeCount)))
    self.m_CountInput:SetMinMax(1, maxCompose, 1)
    self.m_Tick = nil
end

function LuaItemExchange4To1Wnd:OnButtonClick()
    if self.m_Tick then
        return
    end

    self.m_Btneable = self.m_ItemConsume1.countAvailable and self.m_ItemConsume2.countAvailable and
        self.m_ItemConsume3.countAvailable and self.m_ItemConsume4.countAvailable

    if not self.m_Btneable then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", g_ComposeMgr.m_ComposeLackOfItemMsg)
        return
    end

    Gac2Gas.RequestExchangeItems(g_ComposeMgr.m_ExchangeId, self.m_CountInput:GetValue())
    g_MessageMgr:ShowMessage("CUSTOM_STRING2", g_ComposeMgr.m_ComposeSuccessMsg)
    self:PlayFx()

    if not self.m_Tick then
        self.m_Tick = RegisterTickOnce(function()
            self:InitItemsAndBtn()
        end,1000)
    end
end

function LuaItemExchange4To1Wnd:OnDisable()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end

function LuaItemExchange4To1Wnd:PlayFx()
    for i=1,#self.m_Tweeners do
        self.m_Fxs[i].FxRoot.gameObject:SetActive(false)
        self.m_Tweeners[i]:ResetToBeginning()
        self.m_Fxs[i].FxRoot.gameObject:SetActive(true)
        self.m_Tweeners[i]:Play(true)
    end
end