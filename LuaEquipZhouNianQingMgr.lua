--周年庆道具 支机石、十锤、百锤、千锤
local CItemMgr=import "L10.Game.CItemMgr"
local EnumItemPlace=import "L10.Game.EnumItemPlace"
local IdPartition=import "L10.Game.IdPartition"

CLuaEquipZhouNianQingMgr={}
CLuaEquipZhouNianQingMgr.m_ZhiJiShiItemId=21040196

CLuaEquipZhouNianQingMgr.m_ChuiLianItemId=0

CLuaEquipZhouNianQingMgr.m_ChuiLianCost=nil

function CLuaEquipZhouNianQingMgr.GetChuiLianCost(grade)
	--初始化
	if not CLuaEquipZhouNianQingMgr.m_ChuiLianCost then
		CLuaEquipZhouNianQingMgr.m_ChuiLianCost={}
		EquipBaptize_ChuiLian.Foreach(function(itemId, data)
			local info={}
			for i=1,data.Cost.Length do
				local ci = {
					lower = data.Cost[i-1][0],
					upper = data.Cost[i-1][1],
					count = data.Cost[i-1][2],
				}
				table.insert(info,ci)
			end
			CLuaEquipZhouNianQingMgr.m_ChuiLianCost[itemId]=info
		end)
	end
	for i, info in ipairs(CLuaEquipZhouNianQingMgr.m_ChuiLianCost[CLuaEquipZhouNianQingMgr.m_ChuiLianItemId]) do
		if info.lower <= grade and  grade <= info.upper then
			return info.count
		end
	end
	return 0
end


function CLuaEquipZhouNianQingMgr.GetEquipList()
	local stringList =  CreateFromClass(MakeGenericClass(List,cs_string))

	-- 周年庆道具屏蔽对额外装备的使用
	local function checkEquip(itemId)
		local equip = CItemMgr.Inst:GetById(itemId)
		if equip and equip.IsEquip and not equip.Equip.IsExtraEquipment then
			local quality=EnumToInt(equip.Equip.Color)
			if quality==5 or quality==6 or quality==7 then
				return true
			end
		end
		return false
	end
	--身上的
	local equipListOnBody = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Body)
	for i=1,equipListOnBody.Count do
		local itemId=equipListOnBody[i-1].itemId
		if not IdPartition.IdIsTalisman(equipListOnBody[i-1].templateId) then
			if checkEquip(itemId) then		
				CommonDefs.ListAdd(stringList,typeof(string),itemId)
			end
		end
	end
	local list={}
	local equipListOnPackage = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Bag)
	for i=1,equipListOnPackage.Count do
		local itemId=equipListOnPackage[i-1].itemId
		if not IdPartition.IdIsTalisman(equipListOnPackage[i-1].templateId) then
			if checkEquip(itemId) then
				table.insert( list, itemId )
			end
		end
	end

	table.sort( list, function(t1,t2)
		local equip1 = CItemMgr.Inst:GetById(t1)
		local equip2 = CItemMgr.Inst:GetById(t2)

		if equip1 and equip2 then
			local bind1=equip1.IsBinded
			local bind2=equip2.IsBinded
			if bind1 and not bind2 then
				return true
			elseif not bind1 and bind2 then
				return false
			else
				local quality1=EnumToInt(equip1.Equip.QualityType)
				local quality2=EnumToInt(equip2.Equip.QualityType)
				return quality1>quality2
			end
		end
		return true
	end)
	for i,v in ipairs(list) do
		CommonDefs.ListAdd(stringList,typeof(string),v)
	end
	return stringList
end

