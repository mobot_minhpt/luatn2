local CPlayerDataMgr = import "L10.Game.CPlayerDataMgr"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local AlignType2=import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local Vector3 = import "UnityEngine.Vector3"

CLuaZswTemplateMgr = class()

CLuaZswTemplateMgr.CurSelectPartTid = nil

CLuaZswTemplateMgr.CurSelectTypeTids = {}
CLuaZswTemplateMgr.CurTemplateRoTbl = {}
CLuaZswTemplateMgr.SelectRoIndex=nil

--用于模板编辑界面的显示
CLuaZswTemplateMgr.SelectEditTemplateId = nil
CLuaZswTemplateMgr.CurSelectBarIndex = 0
CLuaZswTemplateMgr.TemplateIds4Huamu = {}
CLuaZswTemplateMgr.TemplateIds4Other = {}
CLuaZswTemplateMgr.FileName = "HouseZswTemplateFillData"
CLuaZswTemplateMgr.RenameFileName = "HouseZswTemplateRenameData"
CLuaZswTemplateMgr.HouseZswTemplateFillData = {}--本地保存的装饰物模板数据
CLuaZswTemplateMgr.HouseZswTemplateRenameData = {}--本地保存的装饰物重命名数据
CLuaZswTemplateMgr.Id2Consumed = {}
CLuaZswTemplateMgr.MaterialFxId = 88000116
CLuaZswTemplateMgr.HuaMu = 1
CLuaZswTemplateMgr.Other = 2
--自定义模板
CLuaZswTemplateMgr.CustomFileName = "CustomZswTemplateFillData"
CLuaZswTemplateMgr.CustomTemplateFillData = {}--本地保存的自定义装饰物模板数据
CLuaZswTemplateMgr.CustomFillingData = {}--编辑界面的填充数据

function CLuaZswTemplateMgr.OnSaveTemplateFillData()    
    local json = luaJson.table2json(CLuaZswTemplateMgr.HouseZswTemplateFillData)
    CPlayerDataMgr.Inst:SavePlayerData(CLuaZswTemplateMgr.FileName,json)
end
function CLuaZswTemplateMgr.OnSaveTemplateRenameData()    
    local json2 = luaJson.table2json(CLuaZswTemplateMgr.HouseZswTemplateRenameData)
    CPlayerDataMgr.Inst:SavePlayerData(CLuaZswTemplateMgr.RenameFileName,json2)
end

function CLuaZswTemplateMgr.LoadTemplateFillData()
    CLuaZswTemplateMgr.HouseZswTemplateFillData = {}
    local json = CPlayerDataMgr.Inst:LoadPlayerData(CLuaZswTemplateMgr.FileName)
    if json~=nil and json~="" then
        CLuaZswTemplateMgr.HouseZswTemplateFillData = luaJson.json2lua(json)
    end
    if not CLuaZswTemplateMgr.HouseZswTemplateFillData then
        CLuaZswTemplateMgr.HouseZswTemplateFillData = {}
    end
end

function CLuaZswTemplateMgr.LoadTemplateRenameData()
    CLuaZswTemplateMgr.HouseZswTemplateRenameData = {}
    local json = CPlayerDataMgr.Inst:LoadPlayerData(CLuaZswTemplateMgr.RenameFileName)
    if json~=nil and json~="" then
        CLuaZswTemplateMgr.HouseZswTemplateRenameData = luaJson.json2lua(json)
    end
    if not CLuaZswTemplateMgr.HouseZswTemplateRenameData then
        CLuaZswTemplateMgr.HouseZswTemplateRenameData = {}
    end
end

function CLuaZswTemplateMgr.GetConsumedById(id)
    if type(id) ~= "number" then id = tonumber(id) end
    local consumed = CLuaZswTemplateMgr.Id2Consumed[id]
    if not consumed then
        consumed = 0
    end
    return consumed
end

function CLuaZswTemplateMgr.CheckCanConsume(id)
    local consumed = CLuaZswTemplateMgr.GetConsumedById(id)
    consumed = consumed + 1
    local count = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(id)
    count = count - consumed
    if count < 0 then
        return false
    else
        return true
    end
end

function CLuaZswTemplateMgr.AddConsumeById(id)
    local consumed = CLuaZswTemplateMgr.GetConsumedById(id)
    consumed = consumed + 1
    if type(id) ~= "number" then id = tonumber(id) end
    CLuaZswTemplateMgr.Id2Consumed[id] = consumed
end

function CLuaZswTemplateMgr.RevertConsumeById(id)
    local consumed = CLuaZswTemplateMgr.GetConsumedById(id)
    if consumed == 0 then return end
    consumed = consumed - 1
    if type(id) ~= "number" then id = tonumber(id) end
    CLuaZswTemplateMgr.Id2Consumed[id] = consumed
end

function CLuaZswTemplateMgr.GetFillIdByRoIndex(index)
    local info = CLuaZswTemplateMgr.CurTemplateRoTbl[index]
    if not info then
        return nil
    end
    if info.isFilled then
        return info.id
    end
    return nil
end

function CLuaZswTemplateMgr.GetRoIndexByItemIndex(index)
    local roIndex = 0
    if CLuaZswTemplateMgr.CurSelectBarIndex == 0 then
        roIndex = CLuaZswTemplateMgr.TemplateIds4Huamu[index + 1].index
    elseif CLuaZswTemplateMgr.CurSelectBarIndex == 1 then
        roIndex = CLuaZswTemplateMgr.TemplateIds4Other[index + 1].index
    end
    return roIndex
end

--模板商城预览界面/清单
CLuaZswTemplateMgr.CurPreviewTemplateId = nil

function CLuaZswTemplateMgr.ShowZswTemplatePreview(zswTemplateId)
    CLuaZswTemplateMgr.CurPreviewTemplateId = zswTemplateId
    CUIManager.CloseUI(CUIResources.ShoppingMallWnd)
    CUIManager.ShowUI(CLuaUIResources.ZswTemplatePreviewWnd)
end

--退出预览
CLuaZswTemplateMgr.WeiqiangId = 7041
CLuaZswTemplateMgr.WeiqiangPos = {["x"]=43,["z"]=82}
CLuaZswTemplateMgr.CurPreviewInHouseFur = nil
CLuaZswTemplateMgr.CurPreviewInHouseId = nil
function CLuaZswTemplateMgr.OnPlayEnd()
    if CLuaZswTemplateMgr.CurPreviewInHouseFur then
        CLuaZswTemplateMgr.CurPreviewInHouseFur:Destroy()
        CLuaZswTemplateMgr.CurPreviewInHouseFur = nil
        CLuaZswTemplateMgr.CurTemplateRoTbl = {}
        CLuaZswTemplateMgr.CurPreviewInHouseId = nil
        CameraFollow.Inst.MaxSpHeight = 0
    end
end

----------------自定义装饰物模板
function CLuaZswTemplateMgr.OnClickMenueButton(go)
    local function SelectAction(index)               
        if index==0 then
            CUIManager.ShowUI(CLuaUIResources.ZswTemplateTopRightWnd)
        elseif index==1 then
            if CClientHouseMgr.Inst:IsCurHouseRoomer() then
                g_MessageMgr:ShowMessage("WINTER_OPEN_FANGKE_NOT_ALLOW")
            else
                CUIManager.ShowUI(CLuaUIResources.CustomZswTemplateTopRightWnd)
            end
        end       
    end

    local selectShareItem=DelegateFactory.Action_int(SelectAction)
    local t={}
    local item=PopupMenuItemData(LocalString.GetString("商城模板"),selectShareItem,false,nil)
    table.insert(t, item)
    local item=PopupMenuItemData(LocalString.GetString("自定义模板"),selectShareItem,false,nil)
    table.insert(t, item)

    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType2.Bottom,1,nil,600,true,266)
end

function CLuaZswTemplateMgr.OpenCustomEditWndWithIndex(index)
    CLuaZswTemplateMgr.CurCustomTemplateIdx = index
    CUIManager.ShowUI(CLuaUIResources.CustomZswTemplateEditWnd)
end

function CLuaZswTemplateMgr.LoadLocalCustomTemplate()
    CLuaZswTemplateMgr.CustomTemplateFillData = {}
    local json = CPlayerDataMgr.Inst:LoadPlayerData(CLuaZswTemplateMgr.CustomFileName)
    if json~=nil and json~="" then
        CLuaZswTemplateMgr.CustomTemplateFillData = luaJson.json2lua(json)
    end
    if not CLuaZswTemplateMgr.CustomTemplateFillData then
        CLuaZswTemplateMgr.CustomTemplateFillData = {}
        local limitCount = Zhuangshiwu_Setting.GetData().MaxCustomTemplateSlotNum
        for i=1,limitCount,1 do
            CLuaZswTemplateMgr.CustomTemplateFillData[tostring(i)] = {}
            --table.insert(CLuaZswTemplateMgr.CustomTemplateFillData,{})
        end
    end
end

function CLuaZswTemplateMgr.SaveCustomTemplateToLocal()
    if CClientMainPlayer.Inst then
        local cjson = luaJson.table2json(CLuaZswTemplateMgr.CustomTemplateFillData)
        CPlayerDataMgr.Inst:SavePlayerData(CLuaZswTemplateMgr.CustomFileName,cjson)
    end
end

function CLuaZswTemplateMgr.GetCustomTemplateByIndex(index)
    --print(index,tostring(index),CLuaZswTemplateMgr.CustomTemplateFillData)
    index = tostring(index)
    if not CLuaZswTemplateMgr.CustomTemplateFillData or not next(CLuaZswTemplateMgr.CustomTemplateFillData) then
        CLuaZswTemplateMgr.LoadLocalCustomTemplate()
    end
    local filldata = {}
    if not CLuaZswTemplateMgr.CustomTemplateFillData or not next(CLuaZswTemplateMgr.CustomTemplateFillData) then
        return nil
    end
    filldata = CLuaZswTemplateMgr.CustomTemplateFillData[index]
    return filldata
end

function CLuaZswTemplateMgr.EnterCustomTemplateEditingMode(index,limitCount)
    CLuaZswTemplateMgr.CurCustomTemplateIdx = index
    CLuaZswTemplateMgr.IsInCustomMode = true
    CLuaZswTemplateMgr.CurCustomComponentLimit = limitCount
    --权限检查
    if not CLuaZswTemplateMgr.CheckRoomerPrivalege() then
        g_MessageMgr:ShowMessage("ROOMER_CANNOT_USE")
        return
    end

    CUIManager.ShowUI(CLuaUIResources.MultipleFurnitureEditWnd)
end

function CLuaZswTemplateMgr.CheckRoomerPrivalege()
    if CClientHouseMgr.Inst:IsCurHouseRoomer() then
        local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
        if sceneType == EnumHouseSceneType_lua.eRoom and CLuaHouseMgr.RoomDecorationPrivalege then
            return true
        elseif sceneType == EnumHouseSceneType_lua.eYard and CLuaHouseMgr.YardDecorationPrivalege then
            return true
        elseif sceneType == EnumHouseSceneType_lua.eXiangfang then
            return true
        else
            return false
        end
    else
        return true
    end
end

function CLuaZswTemplateMgr.SaveAsCustomTemplate_Multiple()
    local templateData = {}
    templateData.name = SafeStringFormat3(LocalString.GetString("自定义装饰物模板%d"),CLuaZswTemplateMgr.CurCustomTemplateIdx)
    templateData.num = 0
    templateData.components = {}
    templateData.icon = nil

    if CLuaHouseMgr.MultipleRoot.mChildList.Count > 0 then
        local fur = CLuaHouseMgr.MultipleRoot.mChildList[0]
        local lowestPosY = fur.Position.y
        local lowestDecorationIndex = 0
        local lowestPosX = fur.Position.x
        local highestPosX = fur.Position.x
        local lowestPosZ = fur.Position.z
        local highestPosZ = fur.Position.z
        for i=0,CLuaHouseMgr.MultipleRoot.mChildList.Count-1,1 do
            local childFur = CLuaHouseMgr.MultipleRoot.mChildList[i]
            if childFur.Position.y < lowestPosY then
                lowestPosY = childFur.Position.y
                lowestDecorationIndex = i
            end
            if childFur.Position.x < lowestPosX then
                lowestPosX = childFur.Position.x
            end
            if childFur.Position.x > highestPosX then
                highestPosX = childFur.Position.x
            end
            if childFur.Position.z < lowestPosZ then
                lowestPosZ = childFur.Position.z
            end
            if childFur.Position.z > highestPosZ then
                highestPosZ = childFur.Position.z
            end
        end

        local rootGridX = math.floor((lowestPosX + highestPosX) / 2)
        local rootGridZ = math.floor((lowestPosZ + highestPosZ) / 2)
        local height = CClientFurnitureMgr.Inst:GetOrigLogicHeight(rootGridX + 0.5, rootGridZ + 0.5)
        --local rootPos = Vector3(rootGridX, lowestPosY, rootGridZ)
        local rootPos = {
            x = rootGridX,
            y = lowestPosY,
            z = rootGridZ
        }
        local highFromFloor = rootPos.y - height
        for i=0,CLuaHouseMgr.MultipleRoot.mChildList.Count-1,1 do
            local childFur = CLuaHouseMgr.MultipleRoot.mChildList[i]
            local x = childFur.Position.x - rootPos.x
            local y = childFur.Position.y - rootPos.y + highFromFloor
            local z = childFur.Position.z - rootPos.z

            local component = {}
            local offsetx = math.floor((x * 100) + 0.5)
            local offsety = math.floor((y * 100) + 0.5)
            local offsetz = math.floor((z * 100) + 0.5)
            --local offset = Vector3(offsetx, offsety, offsetz)
            local offset = {
                x = offsetx,
                y = offsety,
                z = offsetz
            }
            component.id = childFur.TemplateId
            component.offset = offset
            local eulerAngle = childFur.RO.transform.localRotation.eulerAngles
            --component.rotation = eulerAngle
            component.rotation = {
                x = eulerAngle.x,
                y = eulerAngle.y,
                z = eulerAngle.z
            }
            component.scale = childFur.RO.Scale
            table.insert(templateData.components, component)
        end
        templateData.num = CLuaHouseMgr.MultipleRoot.mChildList.Count
    end
    if not CLuaZswTemplateMgr.CustomTemplateFillData or not next(CLuaZswTemplateMgr.CustomTemplateFillData) then
        CLuaZswTemplateMgr.LoadLocalCustomTemplate()
    end
    CLuaZswTemplateMgr.CustomTemplateFillData[tostring(CLuaZswTemplateMgr.CurCustomTemplateIdx)] = templateData
    CLuaZswTemplateMgr.SaveCustomTemplateToLocal()
end

function CLuaZswTemplateMgr.DeleteCustomTemplateWithIndex(index)
    if not CLuaZswTemplateMgr.CustomTemplateFillData or not next(CLuaZswTemplateMgr.CustomTemplateFillData) then
        CLuaZswTemplateMgr.LoadLocalCustomTemplate()
    end
    CLuaZswTemplateMgr.CustomTemplateFillData[tostring(index)] = nil
    CLuaZswTemplateMgr.SaveCustomTemplateToLocal()
end

function CLuaZswTemplateMgr.OpenCustomListWnd(index)
    CLuaZswTemplateMgr.CustomIndexForListWnd = index
    CUIManager.ShowUI(CLuaUIResources.ZswTemplateComponentListWnd)
end

function CLuaZswTemplateMgr.OpenCustomFillingWnd(index,slotIdx)
    CLuaZswTemplateMgr.CustomIndex = index
    CLuaZswTemplateMgr.FillSlotIdx = slotIdx
    CUIManager.ShowUI(CLuaUIResources.CustomZswTemplateFillingWnd)
end

function CLuaZswTemplateMgr.InitCustomFillingData(tindex)
    if not CLuaZswTemplateMgr.CustomFillingData or tindex ~= CLuaZswTemplateMgr.CustomFillingData.templateIdx then
        CLuaZswTemplateMgr.CustomFillingData = {}
        CLuaZswTemplateMgr.CustomFillingData.templateIdx = tindex
        CLuaZswTemplateMgr.CustomFillingData.zswId2Count = {}
        CLuaZswTemplateMgr.CustomFillingData.slots = {}
        local templateData = CLuaZswTemplateMgr.GetCustomTemplateByIndex(tindex)
        local num = templateData.num
        for i=1,num,1 do
            table.insert(CLuaZswTemplateMgr.CustomFillingData.slots,0)
        end
    end
end

function CLuaZswTemplateMgr.GetCanFillZswCount(tindex,zswId)
    CLuaZswTemplateMgr.InitCustomFillingData(tindex)
    local canFillCount = 0
    local repoCount = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(zswId)
    local filledCount = CLuaZswTemplateMgr.CustomFillingData.zswId2Count[zswId]
    filledCount = filledCount and filledCount or 0
    canFillCount = repoCount - filledCount
    return canFillCount
end

function CLuaZswTemplateMgr.IsSlotFilled(tindex,slotIdx)
    CLuaZswTemplateMgr.InitCustomFillingData(tindex)
    local zswId = CLuaZswTemplateMgr.CustomFillingData.slots[slotIdx]
    if zswId and zswId ~= 0 then
        return true
    else
        return false
    end
    return false
end

function CLuaZswTemplateMgr.IsAnySlotFilled(tindex)
    local count = 0
    if not CLuaZswTemplateMgr.CustomFillingData then
        return false,count
    end
    if tindex ~= CLuaZswTemplateMgr.CustomFillingData.templateIdx then
        return false,count
    end
    for i,zswId in ipairs(CLuaZswTemplateMgr.CustomFillingData.slots) do
        if zswId and zswId ~= 0 then
            count = count + 1
        end
    end
    return count>0,count
end

function CLuaZswTemplateMgr.AddFillingZsw(tindex,zswId,slotIdx,broacast)
    CLuaZswTemplateMgr.InitCustomFillingData(tindex)
    local filledCount = CLuaZswTemplateMgr.CustomFillingData.zswId2Count[zswId]
    if not filledCount then
        CLuaZswTemplateMgr.CustomFillingData.zswId2Count[zswId] = 0
        filledCount = 0
    end
    filledCount = filledCount + 1
    CLuaZswTemplateMgr.CustomFillingData.zswId2Count[zswId] = filledCount
    CLuaZswTemplateMgr.CustomFillingData.slots[slotIdx] = zswId
    if broacast then
        g_ScriptEvent:BroadcastInLua("RefreshCustomFillingData", tindex,zswId,slotIdx)
    end
end

function CLuaZswTemplateMgr.RevertFillingZsw(tindex,zswId,slotIdx,broacast)
    if not CLuaZswTemplateMgr.CustomFillingData then
        return
    end
    if tindex ~= CLuaZswTemplateMgr.CustomFillingData.templateIdx then
        return
    end

    local filledCount = CLuaZswTemplateMgr.CustomFillingData.zswId2Count[zswId]
    if not filledCount or filledCount == 0 then
        return
    end
    filledCount = filledCount - 1
    CLuaZswTemplateMgr.CustomFillingData.zswId2Count[zswId] = filledCount
    CLuaZswTemplateMgr.CustomFillingData.slots[slotIdx] = 0
    if broacast then
        g_ScriptEvent:BroadcastInLua("RefreshCustomFillingData", tindex,zswId,slotIdx)
    end
end

function CLuaZswTemplateMgr.FastFillCustomTemplate(tindex)
    local templateData = CLuaZswTemplateMgr.GetCustomTemplateByIndex(tindex)
    local components = templateData.components
    for i=1,#components,1 do
        local component = components[i]
        local zswId = component.id
        if not CLuaZswTemplateMgr.IsSlotFilled(tindex,i) and CLuaZswTemplateMgr.GetCanFillZswCount(tindex,zswId)>0 then
            CLuaZswTemplateMgr.AddFillingZsw(tindex,zswId,i,false)
        end
    end
    g_ScriptEvent:BroadcastInLua("RefreshCustomFillingData", tindex,nil,nil)
end

function CLuaZswTemplateMgr.FastClearCustomTemplate(tindex)
    if not CLuaZswTemplateMgr.CustomFillingData then
        return
    end
    if tindex ~= CLuaZswTemplateMgr.CustomFillingData.templateIdx then
        return
    end

    for i,zswId in ipairs(CLuaZswTemplateMgr.CustomFillingData.slots) do
        if zswId and zswId ~= 0 then
            CLuaZswTemplateMgr.RevertFillingZsw(tindex,zswId,i,false)
        end
    end
    g_ScriptEvent:BroadcastInLua("RefreshCustomFillingData", tindex,nil,nil)
end

function CLuaZswTemplateMgr.PlaceCustomTemplateInHouse(tindex)
    --检查有没有正在摆放的装饰物
    if CClientFurnitureMgr.Inst.CurFurniture and not CClientFurnitureMgr.Inst.FurnitureList:ContainsKey(CClientFurnitureMgr.Inst.CurFurniture.ID) then
        CClientFurnitureMgr.Inst.CurFurniture:Destroy()
        CClientFurnitureMgr.Inst.CurFurniture = nil
        CUIManager.CloseUI(CUIResources.HousePopupMenu)
    end

    local firstZswId = 0
    local templateData = CLuaZswTemplateMgr.GetCustomTemplateByIndex(tindex)
    for i=1,#CLuaZswTemplateMgr.CustomFillingData.slots,1 do
        local zswId = CLuaZswTemplateMgr.CustomFillingData.slots[i]
        if zswId and zswId ~= 0 then
            firstZswId = zswId
            break
        end
    end
    local data = Zhuangshiwu_Zhuangshiwu.GetData(firstZswId)
    if not CLuaClientFurnitureMgr.CheckLocation(data) then
        return 
    end
    --检查数量限制
    local maxNum = CLuaClientFurnitureMgr.GetMaxNum()
    local bSetLimitStatus = CClientHouseMgr.Inst and CClientHouseMgr.Inst.mConstructProp.FurnitureLimitStatus > 0 or false
    local curNum = CLuaClientFurnitureMgr.GetTotalPlacedFurnitureCount()
    if bSetLimitStatus then
        curNum = CLuaClientFurnitureMgr.GetPlacedFurnitureCount(2)
    end
    local _,filledCount = CLuaZswTemplateMgr.IsAnySlotFilled(tindex)
    if curNum + filledCount > maxNum then
        g_MessageMgr:ShowMessage("Cant_Add_Furniture_Exceed_Total")
        return
    end
    for zswId,count in pairs(CLuaZswTemplateMgr.CustomFillingData.zswId2Count) do
        local zdata = Zhuangshiwu_Zhuangshiwu.GetData(zswId)
        local putNum = CLuaClientFurnitureMgr.GetPlacedFurnitureCountByTemplateId(zswId)
        local numlimit = 0
        if CLuaHouseWoodPileMgr.IsWoodPile(zswId) then
            numlimit = CLuaHouseWoodPileMgr.m_NumLimit
        else
            numlimit = CLuaClientFurnitureMgr.GetRepositoryNumLimit(CClientHouseMgr.Inst:GetCurHouseGrade(), zdata.Type, zdata.SubType)
        end
        if numlimit < putNum + count then
            g_MessageMgr:ShowMessage("AddZhuangshiwu_Limit")
            return
        end
    end

    CUIManager.CloseUI(CLuaUIResources.CustomZswTemplateTopRightWnd)
    CUIManager.CloseUI(CLuaUIResources.CustomZswTemplateEditWnd)
    CLuaZswTemplateMgr.IsCustomPlacing = true
    CLuaZswTemplateMgr.CurPlacingCustomTemplateIdx = tindex
    CUIManager.ShowUI(CLuaUIResources.MultipleFurnitureEditWnd)
end

function CLuaZswTemplateMgr.ClearCustomTemplateDataCache()
    CLuaZswTemplateMgr.HouseZswTemplateFillData = {}
    CLuaZswTemplateMgr.HouseZswTemplateRenameData = {}
    CLuaZswTemplateMgr.Id2Consumed = {}
    CLuaZswTemplateMgr.CustomTemplateFillData = {}
    CLuaZswTemplateMgr.CustomFillingData = {}
end

function CLuaZswTemplateMgr.RefreshHomeDecorationPicList()
    local ids = ""
    local indexTbl = {}
    for idx=1,10,1 do
        local templateData = CLuaZswTemplateMgr.GetCustomTemplateByIndex(idx)
        if templateData and templateData.picId then
            ids = ids .. templateData.picId .. ","
            table.insert(indexTbl,idx)
        end
    end
    LuaPersonalSpaceMgrReal.GetHomeDecorationPicWithPicIds(ids,function(sdata)
        local scode = sdata.code
        local sinfo = sdata.data
        local list = sinfo.list

        if list and list.Count >= #indexTbl then
            for i=0,#indexTbl-1,1 do
                local templateData = CLuaZswTemplateMgr.GetCustomTemplateByIndex(indexTbl[i+1])
                templateData.icon = tostring(list[i].url)
                templateData.auditStatus = tonumber(list[i].auditStatus)
                --print(i,indexTbl[i+1],templateData.icon,templateData.auditStatus)
            end
        end
        g_ScriptEvent:BroadcastInLua("UpdateHomeDecorationPicList")
    end)
end