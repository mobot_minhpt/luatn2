local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CScene = import "L10.Game.CScene"

LuaRuYiDongResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaRuYiDongResultWnd, "PlayerPassTime", "PlayerPassTime", UILabel)
RegistChildComponent(LuaRuYiDongResultWnd, "ServerTopName", "ServerTopName", UILabel)
RegistChildComponent(LuaRuYiDongResultWnd, "ServerTopPassTime", "ServerTopPassTime", UILabel)
RegistChildComponent(LuaRuYiDongResultWnd, "RankBtn", "RankBtn", GameObject)
RegistChildComponent(LuaRuYiDongResultWnd, "GameName", "GameName", UILabel)

--@endregion RegistChildComponent end

function LuaRuYiDongResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.RankBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankBtnClick()
	end)

    --@endregion EventBind end
end

function LuaRuYiDongResultWnd:Init()
	self.PlayerPassTime.text = self:SecondToTring(LuaHuLuWa2022Mgr.m_SelfUseTime)
	self.ServerTopPassTime.text = self:SecondToTring(LuaHuLuWa2022Mgr.m_RecordUseTime)
	self.ServerTopName.text = LuaHuLuWa2022Mgr.m_RecordPlayerName
	local name = LuaHuLuWa2022Mgr.m_IsRuYiDongResult and LocalString.GetString("勇闯如意洞") or LocalString.GetString("巧夺如意")
	self.GameName.text = name
end

function LuaRuYiDongResultWnd:SecondToTring(seconds)
	if not seconds or seconds <= 0 then
		return LocalString.GetString("暂无")
	end
	local res = ""
	local min = math.floor(seconds / 60)
	local sed = seconds - min*60
	res = SafeStringFormat3(LocalString.GetString("%02d分%02d秒"),min,sed)
	return res
end

--@region UIEvent

function LuaRuYiDongResultWnd:OnRankBtnClick()
	local gamePlayId = CScene.MainScene.GamePlayDesignId

	if LuaHuLuWa2022Mgr.m_IsRuYiDongResult then
		LuaHuLuWa2022Mgr.OpenRuYiDongRankWnd(gamePlayId)
	else
		LuaHuLuWa2022Mgr.OpenQiaoDuoRuYiRankWnd()
	end
end

--@endregion UIEvent

