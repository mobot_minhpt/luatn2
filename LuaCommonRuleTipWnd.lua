require("common/common_include")

local UITable = import "UITable"
local Extensions = import "Extensions"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local CTipTitleItem = import "CTipTitleItem"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"

LuaCommonRuleTipWnd = class()

RegistChildComponent(LuaCommonRuleTipWnd, "TitleLabel", UILabel)
RegistChildComponent(LuaCommonRuleTipWnd, "Table", UITable)
RegistChildComponent(LuaCommonRuleTipWnd, "TitleTemplate", GameObject)
RegistChildComponent(LuaCommonRuleTipWnd, "ParagraphTemplate", GameObject)
RegistChildComponent(LuaCommonRuleTipWnd, "ContentScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaCommonRuleTipWnd, "BtnSignUp", GameObject)
RegistChildComponent(LuaCommonRuleTipWnd, "BtnCancel", GameObject)

function LuaCommonRuleTipWnd:Init()
	self.TitleLabel.text = LuaBabyMgr.m_CommonTitle

    self:ParseRuleText()
    
    local onBtnSignUpClicked = function (go)
    	self:OnBtnSignUpClicked(go)
    end
    CommonDefs.AddOnClickListener(self.BtnSignUp, DelegateFactory.Action_GameObject(onBtnSignUpClicked), false)

    local onBtnCancelClicked = function (go)
    	self:OnBtnCancelClicked(go)
    end
    CommonDefs.AddOnClickListener(self.BtnCancel, DelegateFactory.Action_GameObject(onBtnCancelClicked), false)
end

function LuaCommonRuleTipWnd:ParseRuleText()
	Extensions.RemoveAllChildren(self.Table.transform)
	local msg = LuaBabyMgr.m_CommonMsg

	if System.String.IsNullOrEmpty(msg) then
        return
    end

    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)

    if info == nil then
        return
    end

    if info.titleVisible then
        local titleGo = CUICommonDef.AddChild(self.Table.gameObject, self.TitleTemplate)
        titleGo:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(titleGo, typeof(CTipTitleItem)):Init(info.title)
    end

    do
        local i = 0
        while i < info.paragraphs.Count do
            local paragraphGo = CUICommonDef.AddChild(self.Table.gameObject, self.ParagraphTemplate)
            paragraphGo:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem)):Init(info.paragraphs[i], 4294967295)
            i = i + 1
        end
    end

    self.ContentScrollView:ResetPosition()
    self.Table:Reposition()

end

function LuaCommonRuleTipWnd:OnBtnSignUpClicked(go)
	if LuaBabyMgr.m_OnOKAction then
		LuaBabyMgr.m_OnOKAction()
		LuaBabyMgr.m_OnOKAction = nil
	end
end


function LuaCommonRuleTipWnd:OnBtnCancelClicked(go)
	if LuaBabyMgr.m_OnCancelAction then
		LuaBabyMgr.m_OnCancelAction()
		LuaBabyMgr.m_OnCancelAction = nil
	end
	CUIManager.CloseUI(CLuaUIResources.CommonRuleTipWnd)
end

return LuaCommonRuleTipWnd
