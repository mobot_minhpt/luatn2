local UITabBar = import "L10.UI.UITabBar"
local QnCheckBox = import "L10.UI.QnCheckBox"
local QnTabButton = import "L10.UI.QnTabButton"
local Object = import "System.Object"

LuaZongMenJurisdictionSettingWnd = class()

RegistChildComponent(LuaZongMenJurisdictionSettingWnd,"m_Tabs","Tabs", UITabBar)
RegistChildComponent(LuaZongMenJurisdictionSettingWnd,"m_TabTemplate","TabTemplate", GameObject)
RegistChildComponent(LuaZongMenJurisdictionSettingWnd,"m_LeftScrollView","LeftScrollView", UIScrollView)
RegistChildComponent(LuaZongMenJurisdictionSettingWnd,"m_TopRightScrollView","TopRightScrollView", UIScrollView)
RegistChildComponent(LuaZongMenJurisdictionSettingWnd,"m_RebackButton","RebackButton", GameObject)
RegistChildComponent(LuaZongMenJurisdictionSettingWnd,"m_SaveButton","SaveButton", GameObject)
RegistChildComponent(LuaZongMenJurisdictionSettingWnd,"m_Template","Template", GameObject)
RegistChildComponent(LuaZongMenJurisdictionSettingWnd,"m_TopRightGrid1","TopRightGrid1", UIGrid)
RegistChildComponent(LuaZongMenJurisdictionSettingWnd,"m_TopRightGrid2","TopRightGrid2", UIGrid)
RegistChildComponent(LuaZongMenJurisdictionSettingWnd,"m_TopRightView","TopRightView", GameObject)
RegistChildComponent(LuaZongMenJurisdictionSettingWnd,"m_NoneZhangLaoLabel","NoneZhangLaoLabel", UILabel)

RegistClassMember(LuaZongMenJurisdictionSettingWnd, "m_SelectTab")
RegistClassMember(LuaZongMenJurisdictionSettingWnd, "m_Table")
RegistClassMember(LuaZongMenJurisdictionSettingWnd, "m_ChildTabs")
RegistClassMember(LuaZongMenJurisdictionSettingWnd, "m_SelectChildTab")
RegistClassMember(LuaZongMenJurisdictionSettingWnd, "m_Key2SettingBtn")
RegistClassMember(LuaZongMenJurisdictionSettingWnd, "m_ChangedSetting")
RegistClassMember(LuaZongMenJurisdictionSettingWnd, "m_PlayerIds")
RegistClassMember(LuaZongMenJurisdictionSettingWnd, "m_ServerSectOfficeRightInfo")
RegistClassMember(LuaZongMenJurisdictionSettingWnd, "m_IsTabsInit")

function LuaZongMenJurisdictionSettingWnd:Init()
    self.m_Template:SetActive(false)
    self.m_TabTemplate:SetActive(false)
    self.m_NoneZhangLaoLabel.text = g_MessageMgr:FormatMessage("ZongMenJurisdictionSettingWnd_NoneZhangLao")
    self.m_NoneZhangLaoLabel.gameObject:SetActive(false)
    self.m_ChildTabs = {}
    self.m_Table = self.m_Tabs.gameObject:GetComponent(typeof(UITable))
    self:InitRightView()
    self.m_Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnTabChange(index)
    end)
    self.m_Tabs:ChangeTab(0, false)
    UIEventListener.Get(self.m_RebackButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnRebackButtonClicked()
    end)
    UIEventListener.Get(self.m_SaveButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnSaveButtonClicked()
    end)
    if CClientMainPlayer.Inst then
        Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId, "rights", 0)
    end
end

function LuaZongMenJurisdictionSettingWnd:GetCurPlayerId()
    local index = self.m_SelectTab == 1 and 1 or (self.m_SelectChildTab == 1 and 2 or 3)
    return self.m_PlayerIds and self.m_PlayerIds[index] or 0, index
end

function LuaZongMenJurisdictionSettingWnd:OnRebackButtonClicked()
    local Dictionary_String_Object = MakeGenericClass(Dictionary, String, Object)
    local dic = CreateFromClass(Dictionary_String_Object)
    local dataDic = CreateFromClass(Dictionary_String_Object)
    local playerId,idIndex = self:GetCurPlayerId()
    Menpai_Job.Foreach(function (k,v)
        local quanxian = idIndex == 1 and v.Head or v.Elder
        if quanxian == 2 or quanxian  == 3 then
            CommonDefs.DictAdd(dataDic, typeof(String), tostring(k), typeof(Int32), quanxian == 2 and 1 or 0)
        end
    end)
    CommonDefs.DictAdd(dic, typeof(String), tostring(playerId), typeof(Object), dataDic)
    if CClientMainPlayer.Inst then
        Gac2Gas.RequestSetSectRights(CClientMainPlayer.Inst.BasicProp.SectId,MsgPackImpl.pack(dic))
    end
end

function LuaZongMenJurisdictionSettingWnd:OnSaveButtonClicked()
    local Dictionary_String_Object = MakeGenericClass(Dictionary, String, Object)
    local dic = CreateFromClass(Dictionary_String_Object)
    local dataDic = CreateFromClass(Dictionary_String_Object)
    local playerId = self:GetCurPlayerId()
    for k,v in pairs(self.m_ChangedSetting) do
        CommonDefs.DictAdd(dataDic, typeof(String), tostring(k), typeof(Int32), v)
    end
    CommonDefs.DictAdd(dic, typeof(String), tostring(playerId), typeof(Object), dataDic)
    if CClientMainPlayer.Inst then
        Gac2Gas.RequestSetSectRights(CClientMainPlayer.Inst.BasicProp.SectId,MsgPackImpl.pack(dic))
    end
    self.m_ChangedSetting = {}
end

function LuaZongMenJurisdictionSettingWnd:InitRightView()
    self.m_Key2SettingBtn = {}
    self.m_ChangedSetting = {}
    Menpai_Job.Foreach(function (key, data)
        local go = NGUITools.AddChild(data.Type == 1 and self.m_TopRightGrid1.gameObject or self.m_TopRightGrid2.gameObject,self.m_Template)
        go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = data.Job
        local checkbox = go.transform:Find("Checkbox"):GetComponent(typeof(QnCheckBox))
        local ownMark = go.transform:Find("OwnMark").gameObject
        local unchangeableMark = go.transform:Find("UnchangeableMark").gameObject
        checkbox.gameObject:SetActive(false)
        ownMark:SetActive(false)
        go:SetActive(true)
        unchangeableMark:SetActive(false)
        local id = data.ID
        checkbox.OnValueChanged = DelegateFactory.Action_bool(
            function(select)
                self.m_ChangedSetting[id] = select and 1 or 0
            end
        )
        self.m_Key2SettingBtn[key] = {checkbox = checkbox, ownMark = ownMark,unchangeableMark = unchangeableMark}
    end)
    self.m_TopRightGrid1:Reposition()
    self.m_TopRightGrid2:Reposition()
end

function LuaZongMenJurisdictionSettingWnd:RefreshRightView()
    local playerId ,idIndex = self:GetCurPlayerId()
    Menpai_Job.Foreach(function (key, data)
        local t = self.m_Key2SettingBtn[key]
        local quanxian = idIndex == 1 and data.Head or data.Elder
        t.checkbox.gameObject:SetActive(quanxian == 2 or quanxian == 3)
        t.checkbox:SetSelected(quanxian == 2, false)
        t.ownMark.gameObject:SetActive(quanxian == 1)
        t.unchangeableMark.gameObject:SetActive(quanxian == 0)
    end)
    self.m_ChangedSetting = {}
    if not self.m_ServerSectOfficeRightInfo then return end
    local right = self.m_ServerSectOfficeRightInfo[playerId]
    self.m_TopRightView:SetActive(right ~= nil)
    if not right then
        return
    end
    if right.Data then
        local dict = MsgPackImpl.unpack(right.Data)
        if dict.Count ~= 0 then
            CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function (key, value)
                local t = self.m_Key2SettingBtn[tonumber(key)]
                t.checkbox:SetSelected(value == 1, false)
            end))
        end
    end
end

function LuaZongMenJurisdictionSettingWnd:OnTabChange(index)
    self.m_SelectTab = index + 1
    if not self.m_ChildTabs then return end
    self.m_SaveButton:SetActive(self.m_SelectTab == 2)
    self.m_RebackButton:SetActive(self.m_SelectTab == 2)
    self.m_NoneZhangLaoLabel.gameObject:SetActive(false)
    for type, data in pairs(self.m_ChildTabs) do
        local btns = data.btns
        local uitable = data.uitable
        local tabBar = data.tabBar
        for _,btn in pairs(btns) do
            btn.gameObject:SetActive(type == self.m_SelectTab)
        end
        uitable:Reposition()
        if tabBar then
            tabBar:ChangeTab(0, type ~= self.m_SelectTab)
        end
        if #btns == 0 and type == self.m_SelectTab and self.m_SelectTab == 2 then
            self.m_NoneZhangLaoLabel.gameObject:SetActive(true)
            self.m_SaveButton:SetActive(false)
            self.m_RebackButton:SetActive(false)
        end
    end
    self.m_Table:Reposition()
    self.m_LeftScrollView:ResetPosition()
    self:RefreshRightView()
end

function LuaZongMenJurisdictionSettingWnd:OnChildTabChange(index)
    self.m_SelectChildTab = index + 1
    self.m_TopRightScrollView:ResetPosition()
    self:RefreshRightView()
end

function LuaZongMenJurisdictionSettingWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSectOfficeRightResult", self, "OnSectOfficeRightResult")
end

function LuaZongMenJurisdictionSettingWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSectOfficeRightResult", self, "OnSectOfficeRightResult")
end

function LuaZongMenJurisdictionSettingWnd:OnSectOfficeRightResult(list)
    self.m_ServerSectOfficeRightInfo = {}
    for _,data in pairs(list) do
        self.m_ServerSectOfficeRightInfo[data.PlayerId] = data.Right
    end
    self:InitTabs(list)
    self:RefreshRightView()
end

function LuaZongMenJurisdictionSettingWnd:InitTabs(list)
    if self.m_IsTabsInit then return end
    self.m_IsTabsInit = true
    self.m_PlayerIds = {}

    local t = self.m_Tabs.transform:GetChild(1)
    local uitable = t.transform:Find("Table"):GetComponent(typeof(UITable))
    self.m_ChildTabs[2] = {btns = {},uitable = uitable}
    local zhangLaoIndex = 2
    for _,info in pairs(list) do
        if info.Office == EnumSectOffice.eZhangLao then
            local go = NGUITools.AddChild(uitable.gameObject, self.m_TabTemplate)
            go:SetActive(false)
            local btn = go:GetComponent(typeof(QnTabButton))
            btn.Text = info.Name
            table.insert(self.m_ChildTabs[2].btns, btn)
            self.m_PlayerIds[zhangLaoIndex] = info.PlayerId
            zhangLaoIndex = zhangLaoIndex + 1
        else
            self.m_PlayerIds[1] = info.PlayerId
        end
    end
    uitable:Reposition()
    if uitable.transform.childCount > 0 then
        local tabBar = uitable.gameObject:AddComponent(typeof(UITabBar))
        tabBar:Init()
        tabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
            self:OnChildTabChange(index)
        end)
        tabBar:ChangeTab(0, true)
        self.m_ChildTabs[2].tabBar = tabBar
    end

    self.m_Table:Reposition()
    self.m_LeftScrollView:ResetPosition()
    self.m_Tabs:ChangeTab(0, false)
end