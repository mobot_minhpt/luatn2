local CButton = import "L10.UI.CButton"

local GameObject = import "UnityEngine.GameObject"

local QnRadioBox = import "L10.UI.QnRadioBox"
local UIGrid = import "UIGrid"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"

local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"

LuaChristmas2023GiftForSelfMakeGiftWnd = class()
RegistClassMember(LuaChristmas2023GiftForSelfMakeGiftWnd, "m_SelectCount")       -- 有多少选择槽
RegistClassMember(LuaChristmas2023GiftForSelfMakeGiftWnd, "m_SelectIconList")    -- 材料选择-图标列表
RegistClassMember(LuaChristmas2023GiftForSelfMakeGiftWnd, "m_SelectHLList")      -- 材料选择-高亮背景列表
RegistClassMember(LuaChristmas2023GiftForSelfMakeGiftWnd, "m_SelectIdList")      -- 材料选择-材料id列表
RegistClassMember(LuaChristmas2023GiftForSelfMakeGiftWnd, "m_MaterialList")      -- 材料列表
RegistClassMember(LuaChristmas2023GiftForSelfMakeGiftWnd, "m_LongPressInterval") -- 按钮长按间隔时间
RegistClassMember(LuaChristmas2023GiftForSelfMakeGiftWnd, "m_LongPressTick")     -- 按钮长按定时任务
RegistClassMember(LuaChristmas2023GiftForSelfMakeGiftWnd, "m_IsLongPress")       -- 当前是否是长按

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChristmas2023GiftForSelfMakeGiftWnd, "MaterialSelect", "MaterialSelect", QnRadioBox)
RegistChildComponent(LuaChristmas2023GiftForSelfMakeGiftWnd, "MaterialGrid", "MaterialGrid", UIGrid)
RegistChildComponent(LuaChristmas2023GiftForSelfMakeGiftWnd, "MakeButton", "MakeButton", CButton)

--@endregion RegistChildComponent end

function LuaChristmas2023GiftForSelfMakeGiftWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    UIEventListener.Get(self.MakeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnMakeButtonClick()
    end)
end
-- 选择第index个槽
function LuaChristmas2023GiftForSelfMakeGiftWnd:Changeto(index)
    self.MaterialSelect:ChangeTo(index, false)
    for i = 0, self.m_SelectCount - 1 do
        if self.m_SelectHLList[i] then
            self.m_SelectHLList[i]:SetActive(index == i)
        end
    end
end
-- 初始化材料选择Grid
function LuaChristmas2023GiftForSelfMakeGiftWnd:InitSelectGrid()
    self.m_SelectCount = 3
    self.m_SelectIconList = {}
    self.m_SelectHLList = {}
    self.m_SelectIdList = {}
    for i = 0, self.m_SelectCount - 1 do
        local tf = self.MaterialSelect.transform:GetChild(i)
        local icon = tf:Find("IconImage"):GetComponent(typeof(CUITexture))
        local highlight = tf:Find("Selected").gameObject
        self.m_SelectIconList[i] = icon
        self.m_SelectHLList[i] = highlight
        self.m_SelectIdList[i] = -1 -- 初始化为没有选择材料
        icon.gameObject:SetActive(false)
        highlight:SetActive(false)
        tf.gameObject.name = tostring(i) -- 编号方便区分
        UIEventListener.Get(tf.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            local index = tonumber(go.name)
            self:Changeto(index)
            if not self.m_SelectIdList[index] or self.m_SelectIdList[index] < 0 then return end
            local itemid = self.m_MaterialList[self.m_SelectIdList[index]].itemid
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, AlignType.Default, 0, 0, 0, 0)
        end)
    end
    self:Changeto(0)
    self.MakeButton.Enabled = false
end
-- 初始化材料Grid
function LuaChristmas2023GiftForSelfMakeGiftWnd:InitMaterialGrid()
    self.m_MaterialList = {}
    local itemiddict = Christmas2023_Setting.GetData().GiftCardItem
    for i = 0, 5 do
        local material = self.MaterialGrid.transform:GetChild(i)
        local overlay = material:Find("Overlay").gameObject
        local selected = overlay.transform:Find("Selected").gameObject
        local noitem = overlay.transform:Find("NoItem").gameObject
        local icon = material:Find("IconImage"):GetComponent(typeof(CUITexture))
        local label = material:Find("Count/Label"):GetComponent(typeof(UILabel))

        local itemid = itemiddict[i + 1] and itemiddict[i + 1] or 0
        local item = Item_Item.GetData(itemid)
        local iconpath = ""
        local count = 0
        if item then
            icon:LoadMaterial(item.Icon)
            iconpath = item.Icon
            count = CItemMgr.Inst:GetItemCount(itemid)
        end

        if count > 0 then
            label.text = count
            label.gameObject:SetActive(count > 1)
            overlay:SetActive(false)
            selected:SetActive(true)
            noitem:SetActive(false)
        else
            label.gameObject:SetActive(false)
            overlay:SetActive(true)
            selected:SetActive(false)
            noitem:SetActive(true)
        end

        table.insert(self.m_MaterialList, {
            itemid = itemid,
            icon = icon,
            iconpath = iconpath,
            label = label,
            overlay = overlay,
            selected = selected,
            noitem = noitem,
            count = count,
            IsSelected = false, -- 材料是否被选中
        })

        icon.gameObject.name = tostring(i + 1) -- 编号方便区分
        -- 绑定材料点击事件
        UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            if not self.m_IsLongPress then -- 如果触发了长按就不再触发点击事件
                self:OnMaterialClick(tonumber(go.name))
            end
        end)
        -- 绑定材料长按事件
        UIEventListener.Get(icon.gameObject).onPress = DelegateFactory.BoolDelegate(function(go, ispressed)
            if self.m_LongPressTick then
                UnRegisterTick(self.m_LongPressTick)
                self.m_LongPressTick = nil
            end
            if ispressed then
                self.m_IsLongPress = false
                self.m_LongPressTick = RegisterTickOnce(function()
                    local itemid = self.m_MaterialList[tonumber(go.name)].itemid
                    CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, AlignType.Default, 0, 0, 0, 0)
                    self.m_IsLongPress = true -- 触发长按
                end, self.m_LongPressInterval)
            end
        end)
    end
end
-- 清空选择，初始化为最初的状态
function LuaChristmas2023GiftForSelfMakeGiftWnd:Restart()
    for i = 0, self.m_SelectCount - 1 do
        self.m_SelectIdList[i] = -1
        self.m_SelectIconList[i].gameObject:SetActive(false)
    end
    self:Changeto(0)
    self.MakeButton.Enabled = false

    for i, tbl in ipairs(self.m_MaterialList) do
        local count = CItemMgr.Inst:GetItemCount(tbl.itemid)
        tbl.count = count
        if count > 0 then
            tbl.label.text = count
            tbl.label.gameObject:SetActive(count > 1)
            tbl.overlay:SetActive(false)
            tbl.selected:SetActive(true)
            tbl.noitem:SetActive(false)
        else
            tbl.label.gameObject:SetActive(false)
            tbl.overlay:SetActive(true)
            tbl.selected:SetActive(false)
            tbl.noitem:SetActive(true)
        end
        tbl.IsSelected = false
    end
end
-- 材料是否全选择完毕
function LuaChristmas2023GiftForSelfMakeGiftWnd:IsSelectOver()
    for i = 0, self.m_SelectCount - 1 do
        if self.m_SelectIdList[i] <= 0 then
            return false
        end
    end
    return true
end
-- 点击材料
function LuaChristmas2023GiftForSelfMakeGiftWnd:OnMaterialClick(index)
    local tbl = self.m_MaterialList[index]
    if tbl.count <= 0 then
        CItemAccessListMgr.Inst:ShowItemAccessInfo(tbl.itemid, false, tbl.icon.transform, CTooltipAlignType.Top) -- 获取材料
        return
    end
    local selectid = self.MaterialSelect.CurrentSelectIndex -- 当前是第几个选择槽
    if selectid < 0 or selectid >= self.m_SelectCount then return end
    if index == self.m_SelectIdList[selectid] then -- 选择槽已经是这个材料了
        if selectid < self.m_SelectCount - 1 then self:Changeto(selectid + 1) end -- 移动到下一个槽
        return
    end
    if tbl.IsSelected then -- 材料被选过了
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("不可重复选择材料"))
        return
    end
    if self.m_SelectIdList[selectid] > 0 then -- 先取消上次的选择
        local last = self.m_MaterialList[self.m_SelectIdList[selectid]]
        last.IsSelected = false
        last.overlay:SetActive(false)
    end
    tbl.IsSelected = true
    tbl.overlay:SetActive(true)
    self.m_SelectIdList[selectid] = index
    self.MakeButton.Enabled = self:IsSelectOver()
    self.m_SelectIconList[selectid].gameObject:SetActive(true)
    self.m_SelectIconList[selectid]:LoadMaterial(tbl.iconpath)
    if selectid < self.m_SelectCount - 1 then self:Changeto(selectid + 1) end -- 移动到下一个槽
end
-- 制作按钮按下
function LuaChristmas2023GiftForSelfMakeGiftWnd:OnMakeButtonClick()
    if not self:IsSelectOver() then return end
    Gac2Gas.Christmas2023CarftCard(
        self.m_MaterialList[self.m_SelectIdList[0]].itemid,
        self.m_MaterialList[self.m_SelectIdList[1]].itemid,
        self.m_MaterialList[self.m_SelectIdList[2]].itemid
    )
    self:Restart() -- 清空选择，初始化为最初的状态
end
-- 物品数量更新，更新材料列表
function LuaChristmas2023GiftForSelfMakeGiftWnd:UpdateItemCount()
    for i, tbl in ipairs(self.m_MaterialList) do
        local count = CItemMgr.Inst:GetItemCount(tbl.itemid)
        tbl.count = count
        if count > 0 then
            tbl.label.text = count
            tbl.label.gameObject:SetActive(count > 1)
            tbl.overlay:SetActive(tbl.IsSelected)
            tbl.selected:SetActive(true)
            tbl.noitem:SetActive(false)
        else
            tbl.label.gameObject:SetActive(false)
            tbl.overlay:SetActive(true)
            tbl.selected:SetActive(false)
            tbl.noitem:SetActive(true)
        end
    end
end
function LuaChristmas2023GiftForSelfMakeGiftWnd:Init()
    self.m_LongPressInterval = 500
    self.m_LongPressTick = nil
    self:InitSelectGrid()
    self:InitMaterialGrid()
end
function LuaChristmas2023GiftForSelfMakeGiftWnd:OnEnable()
    LuaChristmas2023Mgr:IncUpWndCount()
    g_ScriptEvent:AddListener("SendItem", self, "UpdateItemCount")
    g_ScriptEvent:AddListener("SetItemAt", self, "UpdateItemCount")
end
function LuaChristmas2023GiftForSelfMakeGiftWnd:OnDisable()
    LuaChristmas2023Mgr:DecUpWndCount()
    g_ScriptEvent:RemoveListener("SendItem", self, "UpdateItemCount")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "UpdateItemCount")
    if self.m_LongPressTick then
        UnRegisterTick(self.m_LongPressTick)
        self.m_LongPressTick = nil
    end
end

--@region UIEvent

--@endregion UIEvent

