local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaNanDuFanHuaWorldEventsMainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNanDuFanHuaWorldEventsMainWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaNanDuFanHuaWorldEventsMainWnd, "FuJiaNanDuEntrance", "FuJiaNanDuEntrance", GameObject)
RegistChildComponent(LuaNanDuFanHuaWorldEventsMainWnd, "FanDuShengShiEntrance", "FanDuShengShiEntrance", GameObject)
RegistChildComponent(LuaNanDuFanHuaWorldEventsMainWnd, "ChengZhuRiChengEntrance", "ChengZhuRiChengEntrance", GameObject)
RegistChildComponent(LuaNanDuFanHuaWorldEventsMainWnd, "TaoZhuMiJuanEntrance", "TaoZhuMiJuanEntrance", GameObject)

--@endregion RegistChildComponent end

function LuaNanDuFanHuaWorldEventsMainWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:RefreshConstInfo()
    self:InitUIEvent()

    self.FuJiaNanDuEntrance.transform:Find("DaiWanCheng").gameObject:SetActive(false)
    self.TaoZhuMiJuanEntrance.transform:Find("Hint").gameObject:SetActive(false)
    self.FuJiaNanDuEntrance.transform:Find("RedDot").gameObject:SetActive(false)
    self.TaoZhuMiJuanEntrance.transform:Find("RedDot").gameObject:SetActive(false)
    self.FuJiaNanDuEntrance.transform:Find("Hint"):GetComponent(typeof(UILabel)).text = LocalString.GetString("9:00-次日2:00")

    Gac2Gas.RequestDaFuWengPlayData()
    
    self:RefreshLordTaskSchedule()
end

function LuaNanDuFanHuaWorldEventsMainWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaNanDuFanHuaWorldEventsMainWnd:OnEnable()
    g_ScriptEvent:AddListener("OnTongXingZhengDataUpdate", self, "OnTongXingZhengDataUpdate")
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey", self, "RefreshLordTaskSchedule")

end

function LuaNanDuFanHuaWorldEventsMainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnTongXingZhengDataUpdate", self, "OnTongXingZhengDataUpdate")
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey", self, "RefreshLordTaskSchedule")

end

function LuaNanDuFanHuaWorldEventsMainWnd:OnTongXingZhengDataUpdate()
    self.TaoZhuMiJuanEntrance.transform:Find("RedDot").gameObject:SetActive(LuaDaFuWongMgr:TongXingZhengReward())
    
    local res = LuaDaFuWongMgr:GetTongXingZhengDataInfo()
    if res then
        self.FuJiaNanDuEntrance.transform:Find("DaiWanCheng").gameObject:SetActive(res.hasDailyTask)
        self.TaoZhuMiJuanEntrance.transform:Find("Hint").gameObject:SetActive(true)
        if res.isMaxLv then
            self.TaoZhuMiJuanEntrance.transform:Find("Hint"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(res.lv))
        else
            self.TaoZhuMiJuanEntrance.transform:Find("Hint"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(res.lv)) .. res.curExp .. "/" .. res.maxExp
        end
    else
        -- nil
        self.FuJiaNanDuEntrance.transform:Find("DaiWanCheng").gameObject:SetActive(false)
        self.TaoZhuMiJuanEntrance.transform:Find("Hint").gameObject:SetActive(false)
    end
end

function LuaNanDuFanHuaWorldEventsMainWnd:RefreshLordTaskSchedule()
    local scheduleData = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordSchedulePlayDataKey) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordSchedulePlayDataKey)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            scheduleData = data
        end
    end
    local showHints = false
    for i = 1, 3 do
        local scheduleInfo = scheduleData[i]
        if scheduleInfo then
            if scheduleInfo[2] == 1 then
                showHints = true
                break
            end
        else
            showHints = true
            break
        end
    end
    self.ChengZhuRiChengEntrance.transform:Find("DaiWanCheng").gameObject:SetActive(showHints)
end

function LuaNanDuFanHuaWorldEventsMainWnd:RefreshConstInfo()
    local entranceList = {self.FanDuShengShiEntrance, self.FuJiaNanDuEntrance, self.ChengZhuRiChengEntrance}
    for i = 1, #entranceList do
        entranceList[i].transform:Find("RedDot").gameObject:SetActive(false)
    end
end

function LuaNanDuFanHuaWorldEventsMainWnd:InitUIEvent()
    UIEventListener.Get(self.FuJiaNanDuEntrance).onClick = DelegateFactory.VoidDelegate(function ()
        CUIManager.ShowUI(CLuaUIResources.DaFuWongEnterWnd)
    end)

    UIEventListener.Get(self.TaoZhuMiJuanEntrance).onClick = DelegateFactory.VoidDelegate(function ()
        CUIManager.ShowUI(CLuaUIResources.DaFuWongTongXingZhengWnd)
    end)


    UIEventListener.Get(self.FanDuShengShiEntrance).onClick = DelegateFactory.VoidDelegate(function ()
        local activityId = 42000488
        local scheduleData = Task_Schedule.GetData(activityId)
        CLuaScheduleMgr.selectedScheduleInfo = {
            activityId = activityId,
            taskId = scheduleData.TaskID[0],
            TotalTimes = 0,
            DailyTimes = 0,
            ActivityTime = scheduleData.ExternTip,
        }
        CLuaScheduleInfoWnd.s_UseCustomInfo = true
        CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
    end)


    local openLordWay = function()
        local isFinishTask = CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(NanDuFanHua_LordSetting.GetData().OpenLordWndTask)
        if isFinishTask then
            LuaNanDuFanHuaLordMainWnd.OpenDefaultTabIndex = 2
            CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaLordMainWnd)
        else
            g_MessageMgr:ShowMessage('NanDuFanHua_NotFinishLordTask')
        end
    end
    UIEventListener.Get(self.ChengZhuRiChengEntrance).onClick = DelegateFactory.VoidDelegate(function ()
        openLordWay()
    end)
    UIEventListener.Get(self.ChengZhuRiChengEntrance.transform:Find("Item").gameObject).onClick = DelegateFactory.VoidDelegate(function ()
        openLordWay()
    end)
end

function LuaNanDuFanHuaWorldEventsMainWnd:GetGuideGo(methodName)
    if methodName == "GetNanDuLordMainButton" then
        return self.ChengZhuRiChengEntrance.transform:Find("Item").gameObject
    end
end