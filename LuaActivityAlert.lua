local COpenEntryMgr = import "L10.Game.COpenEntryMgr"
local Extensions = import "Extensions"
local ETickType = import "L10.Engine.ETickType"
local EnumGiftAlertType = import "L10.UI.EnumGiftAlertType"
local CWelfareMgr = import "L10.UI.CWelfareMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Color = import "UnityEngine.Color"
local CGameVideoMgr = import "L10.Game.CGameVideoMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CActivityAlertMgr = import "L10.UI.CActivityAlertMgr"
local CActivityAlertItem = import "L10.UI.CActivityAlertItem"
local CUIGameObjectPool = import "L10.UI.CUIGameObjectPool"
local CScene = import "L10.Game.CScene"
local CTaskMgr = import "L10.Game.CTaskMgr"
local UICommonDef = import "L10.UI.CUICommonDef"
local CTrackMgr = import "L10.Game.CTrackMgr"
local EnumHongbaoType = import "L10.UI.EnumHongbaoType"
local Constants = import "L10.Game.Constants"
local CHongBaoMgr = import "L10.UI.CHongBaoMgr"
local CScheduleMgr = import "L10.Game.CScheduleMgr"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local PathType = import "DG.Tweening.PathType"
local Ease = import "DG.Tweening.Ease"
local Vector4 = import "UnityEngine.Vector4"
local PathMode = import "DG.Tweening.PathMode"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
CLuaActivityAlert = class()
RegistClassMember(CLuaActivityAlert, "pool")
RegistClassMember(CLuaActivityAlert, "grid")
RegistClassMember(CLuaActivityAlert, "root")
RegistClassMember(CLuaActivityAlert, "alertButton")
RegistClassMember(CLuaActivityAlert, "houseCompetitionButton")
RegistClassMember(CLuaActivityAlert, "ZNQDiscount")
RegistClassMember(CLuaActivityAlert, "doubleOneActivityNode")
RegistClassMember(CLuaActivityAlert, "yayunButton")
RegistClassMember(CLuaActivityAlert, "houseReportButton")
RegistClassMember(CLuaActivityAlert, "xinBaiButton")
RegistClassMember(CLuaActivityAlert, "xinBaiButtonInstance")
RegistClassMember(CLuaActivityAlert, "qianYingChuWenButton")
RegistClassMember(CLuaActivityAlert, "qianYingChuWenButtonInstance")
RegistClassMember(CLuaActivityAlert, "qianYingChuWenFx")
RegistClassMember(CLuaActivityAlert, "m_CountDownTick")
RegistClassMember(CLuaActivityAlert, "m_HuiliuCountDownTick") -- 回流倒计时与其他倒计时区分开
RegistClassMember(CLuaActivityAlert, "m_RecommendGiftCountDownTick")
RegistClassMember(CLuaActivityAlert, "m_TianJiangBaoXiangCountDownTick")
RegistClassMember(CLuaActivityAlert, "m_ShenYaoCountDownTick")
RegistClassMember(CLuaActivityAlert, "m_ZNQ2023PassTick")
RegistClassMember(CLuaActivityAlert, "m_ShuJia2023WorldEventTick")
RegistClassMember(CLuaActivityAlert, "m_PermanentFashionLotteryTick")
RegistClassMember(CLuaActivityAlert, "recommendGiftButton")
RegistClassMember(CLuaActivityAlert, "m_ShenYaoPlayStartTime")
RegistClassMember(CLuaActivityAlert, "m_CurHongbaoType")
RegistClassMember(CLuaActivityAlert, "m_CurHongbaoAmount")
RegistClassMember(CLuaActivityAlert, "m_HongbaoTick")
RegistClassMember(CLuaActivityAlert, "m_OriginGrid2Pos")
RegistClassMember(CLuaActivityAlert, "lingyuGiftRecommendButton")
RegistClassMember(CLuaActivityAlert, "lingyuGiftRecommendFx")
RegistClassMember(CLuaActivityAlert, "commonPassportButton")
RegistClassMember(CLuaActivityAlert, "YaoYeManJuanButton")
RegistClassMember(CLuaActivityAlert, "IconWithSliderTemplate")
RegistClassMember(CLuaActivityAlert, "m_ScheduleDatas")

RegistChildComponent(CLuaActivityAlert, "HuiLiuButton", GameObject)
RegistChildComponent(CLuaActivityAlert, "HaMaoButton", GameObject)
RegistChildComponent(CLuaActivityAlert, "TianJiangBaoXiangButton", GameObject)
RegistChildComponent(CLuaActivityAlert, "SeaFishingPlayButton", GameObject)
RegistChildComponent(CLuaActivityAlert, "ShenYaoButton", GameObject)
RegistChildComponent(CLuaActivityAlert, "Grid2", UIGrid)
RegistChildComponent(CLuaActivityAlert, "HongBaoButton", GameObject)
RegistChildComponent(CLuaActivityAlert, "SectHongBaoButton", GameObject)
RegistChildComponent(CLuaActivityAlert, "JieRiButton", GameObject)
RegistChildComponent(CLuaActivityAlert, "QMPKCertificationButton", GameObject)

CLuaActivityAlert.s_HouseCompetitionZhiBoUrl = "https://qnm.163.com/m/2018/xczbh/"
CLuaActivityAlert.s_HouseCompetitionStatus = false

CLuaActivityAlert.s_ZNQDiscountStatus = false
CLuaActivityAlert.s_ZNQTimeRemain = 0
CLuaActivityAlert.s_DoubleOneActivityStatus = false
CLuaActivityAlert.s_DoubleOneBonusStatus = false
CLuaActivityAlert.s_DoubleOneBonusTimeTotalRemain = 0
CLuaActivityAlert.s_DoubleOneBonusTimeRemain = 0
CLuaActivityAlert.s_DoubleOneBonusTimeBegin = 0
CLuaActivityAlert.s_HuiLiuGiftStatus = false
CLuaActivityAlert.s_HuiLiuGiftTimeRemain = 0
CLuaActivityAlert.s_HuiLiuGiftEndTime = 0
CLuaActivityAlert.s_RecommendGiftStatus = false
CLuaActivityAlert.s_RecommendGiftTimeRemain = 0
CLuaActivityAlert.s_TianJiangBaoXiangTimeRemain = 0
CLuaActivityAlert.s_HanJia2023BattlePassStatus = false
CLuaActivityAlert.s_LastHanJia2023SyncTimeStamp = 0
CLuaActivityAlert.s_HouseScreenShotReportStatus = false
CLuaActivityAlert.s_TianJiangBaoXiangStatus = false
CLuaActivityAlert.s_SeaFishingEntryStatus = false
CLuaActivityAlert.s_ShenYaoPlayStatus = false
CLuaActivityAlert.s_QMPKCertificationStatus = false
CLuaActivityAlert.s_QMPKCertificationClicked = false
CLuaActivityAlert.s_YaoYeManJuanPlayStatus = false

function CLuaActivityAlert:Awake()
    self.pool = self.transform:Find("Anchor/Content/Pool"):GetComponent(typeof(CUIGameObjectPool))
    self.grid = self.transform:Find("Anchor/Content/Grid"):GetComponent(typeof(UIGrid))
    self.root = self.transform:Find("Anchor/Content").gameObject
    self.alertButton = self.transform:Find("Anchor/Content/AlertButton").gameObject
    self.houseCompetitionButton = self.transform:Find("Anchor/Content/HouseCompetitionButton").gameObject
    self.ZNQDiscount = self.transform:Find("Anchor/Content/ZNQDiscount").gameObject
    self.doubleOneActivityNode = self.transform:Find("Anchor/Content/DoubleOneActivity").gameObject
    self.yayunButton = self.transform:Find("Anchor/Content/YaYunButton").gameObject
    self.houseReportButton = self.transform:Find("Anchor/Content/HouseReportButton").gameObject
    self.xinBaiButton = self.transform:Find("Anchor/Content/XinBaiButton").gameObject
    self.qianYingChuWenButton = self.transform:Find("Anchor/Content/QianYingChuWenButton").gameObject
    self.qianYingChuWenFx = self.transform:Find("Anchor/Content/FxRoot/QianYingChuWenFx"):GetComponent(typeof(CUIFx))
    self.recommendGiftButton = self.transform:Find("Anchor/Content/RecommendGiftButton").gameObject
    self.QMPKCertificationButton = self.transform:Find("Anchor/Content/QMPKCertificationButton").gameObject
    self.lingyuGiftRecommendButton = self.transform:Find("Anchor/Content/LingyuGiftRecommendButton").gameObject
    self.lingyuGiftRecommendFx = self.transform:Find("Anchor/Content/FxRoot/LingyuGiftRecommendFx"):GetComponent(typeof(CUIFx))
    self.hanjia2023BattlePassButton = self.transform:Find("Anchor/Content/HanJia2023BattlePassButton").gameObject
    self.ZNQ2023BattlePassButton = self.transform:Find("Anchor/Content/ZNQ2023BattlePassButton").gameObject
    self.nandufanhuaButton = self.transform:Find("Anchor/Content/NanDuFanHuaButton").gameObject
    self.ShuJia2023WorldEventButton = self.transform:Find("Anchor/Content/ShuJia2023WorldEventButton").gameObject
    self.commonPassportButton = self.transform:Find("Anchor/Content/CommonPassportButton").gameObject
    self.professionTransferButton = self.transform:Find("Anchor/Content/ProfessionTransferButton").gameObject
    self.fashionLotteryButton = self.transform:Find("Anchor/Content/FashionLotteryButton").gameObject
    self.YaoYeManJuanButton = self.transform:Find("Anchor/Content/YaoYeManJuanButton").gameObject
    self.IconWithSliderTemplate = FindChildWithType(self.transform, "Anchor/Content/IconWithSliderTemplate", typeof(GameObject))

    self.m_OriginGrid2Pos = self.Grid2.transform.localPosition
    self.m_CountDownTick = nil
    self.m_HuiliuCountDownTick = nil
    self.m_TianJiangBaoXiangCountDownTick = nil
    self.m_ShenYaoCountDownTick = nil

    self.alertButton:SetActive(false)
    self.houseCompetitionButton:SetActive(false)
    self.ZNQDiscount:SetActive(false)
    self.doubleOneActivityNode:SetActive(false)
    self.houseReportButton:SetActive(false)
    self.xinBaiButton:SetActive(false)
    self.qianYingChuWenButton:SetActive(false)
    self.lingyuGiftRecommendButton:SetActive(false)
    self.HuiLiuButton:SetActive(false)
    self.recommendGiftButton:SetActive(false)
    self.HaMaoButton:SetActive(false)
    self.TianJiangBaoXiangButton:SetActive(false)
    self.SeaFishingPlayButton:SetActive(false)
    self.ShenYaoButton:SetActive(false)
    self.HongBaoButton:SetActive(false)
    self.SectHongBaoButton:SetActive(false)
    self.JieRiButton:SetActive(false)
    self.QMPKCertificationButton:SetActive(false)
    self.hanjia2023BattlePassButton:SetActive(false)
    self.ZNQ2023BattlePassButton:SetActive(false)
    self.nandufanhuaButton:SetActive(false)
    self.professionTransferButton:SetActive(false)
    self.ShuJia2023WorldEventButton:SetActive(false)
    self.commonPassportButton:SetActive(false)
    self.fashionLotteryButton:SetActive(false)
    self.YaoYeManJuanButton:SetActive(false)
    CScheduleMgr.Inst:RequestActivity()
    CLuaActivityAlert.s_TianJiangBaoXiangStatus = LuaTianJiangBaoXiangMgr.IsPlayOpen
    CLuaActivityAlert.s_QMPKCertificationStatus = CLuaQMPKMgr.ShowQmpkCertificationInfoAlert

    self:RegistScheduleCtrlGo()
end

function CLuaActivityAlert:RegistScheduleCtrlGo()
    self.m_ScheduleDatas = {}
    self.m_ScheduleDatas[42010153] = {
        Func = self.AddUpDownWorldAlertBtn
    }
end

function CLuaActivityAlert:GetScheduleData(scheduleid)
    return self.m_ScheduleDatas[scheduleid]
end

--@region UpDownWorldAlert

function CLuaActivityAlert:AddUpDownWorldAlertBtn()
    local stb = LuaActivityAlertMgr.AlertScheduleIDs[42010153]
    if not stb then return end

    local data = stb[1]
    local time = data.Time
     --预热的时候，是开启的时间，开启的时候，是结束的时间
    local stime = CServerTimeMgr.Inst.timeStamp
    if data.State == 2 and stime >= time then --已经结束
        LuaActivityAlertMgr.AlertScheduleIDs[42010153] = nil
        return
    end

    local ctrlgo = self.IconWithSliderTemplate
    local go = NGUITools.AddChild(self.Grid2.gameObject, ctrlgo)
    local slider = FindChildWithType(go.transform, "TimesLab/Slider", typeof(UISlider))
    go:SetActive(true)
    UIEventListener.Get(go).onClick =
        DelegateFactory.VoidDelegate(
        function(g)
            LuaHanJia2024Mgr:ReqUpDownWorldData()
        end
    )

    local uidate = self:GetScheduleData(42010153)
    local setting = HanJia2024_UpDownWorldSetting.GetData()
    local prepareTime = setting.PrepareDuration
    local keepTime = setting.StartDuration

    UnRegisterTick(uidate.Tick)
    uidate.Tick = RegisterTick(function()
        local curtime = CServerTimeMgr.Inst.timeStamp
        local totleTime = 1
        local delta = time - curtime
        if data.State == 1 then --预热
            totleTime = prepareTime
        else
            totleTime = keepTime
        end
        if delta <= 0 then
            UnRegisterTick(uidate.Tick)
            self:OnSyncDynamicActivitySimpleInfo()
            return
        end

        local p = delta / totleTime
        slider.value = p
        local sp = slider.foregroundWidget
        if data.State == 1 then
            sp.color = Color(163/255,176/255,179/255, 1)
        else
            if delta <= 20 * 60 then --小于20分钟
                sp.color = Color(1, 84/255, 71/255, 1)
            else
                sp.color = Color(252/255,241/255,137/255, 1)
            end
        end
    end, 1000)
end

--@endregion

function CLuaActivityAlert:OnEnable()
    self:OnXianKeLaiPlayState(true)
    self:OnShenYaoSyncPlayState(LuaXinBaiLianDongMgr.IsShenYaoOpen, LuaXinBaiLianDongMgr.IsShenYaoStart, LuaXinBaiLianDongMgr.ShenYaoPlayStartTime)
    self:OnYaoYeManJuanUpdateIconStatus(LuaYaoYeManJuanMgr.m_IconStatus)

    g_ScriptEvent:AddListener("SyncDynamicActivitySimpleInfo", self, "OnSyncDynamicActivitySimpleInfo")
    g_ScriptEvent:AddListener("SyncZhiBoStatus", self, "OnSyncHuTianYiZhiBoStatus")
    g_ScriptEvent:AddListener("OnSyncHouseCompetitionFinalStatus", self, "OnSyncHouseCompetitionFinalStatus")
    g_ScriptEvent:AddListener("GasDisconnect", self, "Reset")
    g_ScriptEvent:AddListener("ZhouNianShopAlertStatusQueryResult", self, "OnZhouNianShopAlertStatusQueryResult")
    --g_ScriptEvent:AddListener("DoubleOne2019BonusResult",self,"OnDoubleOne2019BonusResult")
    --g_ScriptEvent:AddListener("DoubleOne2019ActivityResult",self,"OnDoubleOne2019ActivityResult")
    g_ScriptEvent:AddListener("SyncHuiLiuGiftInfos", self, "OnHuiLiuGiftUpdate")
    g_ScriptEvent:AddListener("SyncRecommendGiftInfos", self, "OnRecommendGiftUpdate")
    g_ScriptEvent:AddListener("MainPlayerCreated", self, "OnMainPlayerCreated")
    g_ScriptEvent:AddListener("HideHaMaoButton", self, "OnHideHaMaoButton")
    g_ScriptEvent:AddListener("SyncTianJiangBaoXiangStatus", self, "OnShowTianJiangBaoXiangBtn")
    g_ScriptEvent:AddListener("SyncSeaFishingPlayEntranceStatus", self, "OnSyncSeaFishingPlayEntranceStatus")
    g_ScriptEvent:AddListener("ShenYao_SyncPlayState", self, "OnShenYaoSyncPlayState")
    g_ScriptEvent:AddListener("XianKeLaiSyncPlayState", self, "OnXianKeLaiPlayState")
    g_ScriptEvent:AddListener("HanJia2023_SyncXianKeLianData", self, "OnUpdateXianKeLaiData")
    g_ScriptEvent:AddListener("OnTongXingZhengDataUpdate", self, "OnDaFuWenDataUpdate")

    g_ScriptEvent:AddListener("ZNQ2023_SyncZhanLingPlayData", self, "OnSyncDynamicActivitySimpleInfo")
    g_ScriptEvent:AddListener("ShuJia2023WorldEventsSyncStageTwoStatus", self, "OnSyncDynamicActivitySimpleInfo")

    g_ScriptEvent:AddListener("OnChangeQmpkCertificationStatus", self, "OnChangeQmpkCertificationStatus")
    g_ScriptEvent:AddListener("PlayerSendHongBao", self, "OnPlayerSendHongBao")
    g_ScriptEvent:AddListener("UpdateActivity", self, "OnUpdateActivity")
    g_ScriptEvent:AddListener("MainPlayerLevelChange", self, "OnMainPlayerLevelChange")
    g_ScriptEvent:AddListener("YaoYeManJuan_UpdateIconStatus", self, "OnYaoYeManJuanUpdateIconStatus")
end
function CLuaActivityAlert:OnDisable()
    g_ScriptEvent:RemoveListener("SyncDynamicActivitySimpleInfo", self, "OnSyncDynamicActivitySimpleInfo")
    g_ScriptEvent:RemoveListener("SyncZhiBoStatus", self, "OnSyncHuTianYiZhiBoStatus")
    g_ScriptEvent:RemoveListener("OnSyncHouseCompetitionFinalStatus", self, "OnSyncHouseCompetitionFinalStatus")
    g_ScriptEvent:RemoveListener("GasDisconnect", self, "Reset")
    g_ScriptEvent:RemoveListener("ZhouNianShopAlertStatusQueryResult", self, "OnZhouNianShopAlertStatusQueryResult")
    --g_ScriptEvent:RemoveListener("DoubleOne2019BonusResult",self,"OnDoubleOne2019BonusResult")
    --g_ScriptEvent:RemoveListener("DoubleOne2019ActivityResult",self,"OnDoubleOne2019ActivityResult")
    g_ScriptEvent:RemoveListener("SyncHuiLiuGiftInfos", self, "OnHuiLiuGiftUpdate")
    g_ScriptEvent:RemoveListener("SyncRecommendGiftInfos", self, "OnRecommendGiftUpdate")
    g_ScriptEvent:RemoveListener("HideHaMaoButton", self, "OnHideHaMaoButton")
    g_ScriptEvent:RemoveListener("SyncTianJiangBaoXiangStatus", self, "OnShowTianJiangBaoXiangBtn")
    g_ScriptEvent:RemoveListener("SyncSeaFishingPlayEntranceStatus", self, "OnSyncSeaFishingPlayEntranceStatus")
    g_ScriptEvent:RemoveListener("ShenYao_SyncPlayState", self, "OnShenYaoSyncPlayState")
    g_ScriptEvent:RemoveListener("XianKeLaiSyncPlayState", self, "OnXianKeLaiPlayState")
    g_ScriptEvent:RemoveListener("HanJia2023_SyncXianKeLianData", self, "OnUpdateXianKeLaiData")
    g_ScriptEvent:RemoveListener("OnTongXingZhengDataUpdate", self, "OnDaFuWenDataUpdate")
    g_ScriptEvent:RemoveListener("ZNQ2023_SyncZhanLingPlayData", self, "OnSyncDynamicActivitySimpleInfo")
    g_ScriptEvent:RemoveListener("ShuJia2023WorldEventsSyncStageTwoStatus", self, "OnSyncDynamicActivitySimpleInfo")

    g_ScriptEvent:RemoveListener("OnChangeQmpkCertificationStatus", self, "OnChangeQmpkCertificationStatus")
    g_ScriptEvent:RemoveListener("PlayerSendHongBao", self, "OnPlayerSendHongBao")
    g_ScriptEvent:RemoveListener("UpdateActivity", self, "OnUpdateActivity")
    g_ScriptEvent:RemoveListener("MainPlayerLevelChange", self, "OnMainPlayerLevelChange")
    g_ScriptEvent:RemoveListener("YaoYeManJuan_UpdateIconStatus", self, "OnYaoYeManJuanUpdateIconStatus")

    self:CancelHuiliuTick()
    self:CancelRecommendGiftTick()
    self:CancelHongBaoTick()
    CLuaScheduleMgr.m_IsShowJieRiButtonFx = false
end

function CLuaActivityAlert:OnMainPlayerLevelChange()
    self:OnSyncDynamicActivitySimpleInfo()
end

function CLuaActivityAlert:OnMainPlayerCreated()
    self:OnSyncDynamicActivitySimpleInfo()
end
function CLuaActivityAlert:OnHideHaMaoButton()
    self:OnSyncDynamicActivitySimpleInfo()
end

function CLuaActivityAlert:OnShenYaoSyncPlayState(bIsPlayOpen, bIsPlayStart, playStartTime)
    CLuaActivityAlert.s_ShenYaoPlayStatus = bIsPlayOpen and bIsPlayStart
    self.m_ShenYaoPlayStartTime = playStartTime
    self:OnSyncDynamicActivitySimpleInfo()
end

function CLuaActivityAlert:OnXianKeLaiPlayState(initState)
    if initState then
        if LuaHanJia2023Mgr.isXKLOpen then
            CLuaActivityAlert.s_HanJia2023BattlePassStatus = true
        end
    else
        CLuaActivityAlert.s_HanJia2023BattlePassStatus = true
        self:OnSyncDynamicActivitySimpleInfo()
    end
end

function CLuaActivityAlert:OnChangeQmpkCertificationStatus(bIsNeedCertification)
    CLuaActivityAlert.s_QMPKCertificationStatus = bIsNeedCertification
    self:OnSyncDynamicActivitySimpleInfo()
end

function CLuaActivityAlert:OnSyncSeaFishingPlayEntranceStatus(isOpen, closeTime)
    CLuaActivityAlert.s_SeaFishingEntryStatus = isOpen
    self:OnSyncDynamicActivitySimpleInfo()
end

function CLuaActivityAlert:OnShowTianJiangBaoXiangBtn()
    CLuaActivityAlert.s_TianJiangBaoXiangStatus = LuaTianJiangBaoXiangMgr.IsPlayOpen
    self:OnSyncDynamicActivitySimpleInfo()
end

function CLuaActivityAlert:OnSyncHuTianYiZhiBoStatus(bOpen)
    CLuaActivityAlert.s_ZhiBoStatus = bOpen
    self:OnSyncDynamicActivitySimpleInfo()
end

function CLuaActivityAlert:OnSyncHouseCompetitionFinalStatus(bStatus, url)
    CLuaActivityAlert.s_HouseCompetitionZhiBoUrl = url
    CLuaActivityAlert.s_HouseCompetitionStatus = bStatus

    self:OnSyncDynamicActivitySimpleInfo()
end

function CLuaActivityAlert:OnZhouNianShopAlertStatusQueryResult(bAlert, timeRemain)
    CLuaActivityAlert.s_ZNQDiscountStatus = bAlert
    CLuaActivityAlert.s_ZNQTimeRemain = timeRemain

    self:OnSyncDynamicActivitySimpleInfo()
end

function CLuaActivityAlert:OnHuiLiuGiftUpdate(isOpen, timeRemain)
    CLuaActivityAlert.s_HuiLiuGiftStatus = isOpen
    CLuaActivityAlert.s_HuiLiuGiftTimeRemain = timeRemain

    self:OnSyncDynamicActivitySimpleInfo()
end
function CLuaActivityAlert:OnRecommendGiftUpdate(isOpen)
    CLuaActivityAlert.s_RecommendGiftStatus = isOpen
    self:OnSyncDynamicActivitySimpleInfo()
end
function CLuaActivityAlert:OnDoubleOne2019BonusResult()
    self:OnSyncDynamicActivitySimpleInfo()
end
function CLuaActivityAlert:OnDoubleOne2019ActivityResult()
    self:OnSyncDynamicActivitySimpleInfo()
end

function CLuaActivityAlert:OnYaoYeManJuanUpdateIconStatus(bShow)
    CLuaActivityAlert.s_YaoYeManJuanPlayStatus = bShow
    self:OnSyncDynamicActivitySimpleInfo()
end

function CLuaActivityAlert:CheckPosition()
    if CClientHouseMgr.Inst:IsPlayerInHouse() or CClientHouseMgr.Inst:IsPlayerInHaiMengZe() then
        self.grid.transform.parent.localPosition = CreateFromClass(Vector3, -107, 0, 0)
        self.Grid2.transform.localPosition = CreateFromClass(Vector3, self.m_OriginGrid2Pos.x + 107, self.m_OriginGrid2Pos.y, 0)
    else
        self.grid.transform.parent.localPosition = Vector3.zero
        self.Grid2.transform.localPosition = self.m_OriginGrid2Pos
    end
    g_ScriptEvent:BroadcastInLua("RefreshDynamicActivity")
end

function CLuaActivityAlert:IsMeCrossServerPlayer()
    if CClientMainPlayer.Inst then
        return CClientMainPlayer.Inst.IsCrossServerPlayer
    end
    return false
end

function CLuaActivityAlert:CheckShowButtonIfInCrossServer(btnName)
    if self:IsMeCrossServerPlayer() then
        --如果是在跨服环境下的话, 去配置表中查看是否需要显示
        local lowerBtnName = string.lower(btnName)
        local ret = false
        Gameplay_ActivityAlert.Foreach(function (_, v)
            if lowerBtnName == string.lower(v.AlertButtonName) then
                ret = v.OpenOnCrossServer == 1
            end
        end)
        return ret
    else
        return true
    end
end

function CLuaActivityAlert:TryAddAlertButton(btnName, func)
    if self:CheckShowButtonIfInCrossServer(btnName) then
        func(self)
    end
end

--2023.8之后新增的显示函数 需要用新接口TryAddAlertButton包装一下
--增加新按钮前需要问一下策划, 是否需要在跨服环境下显示 (这是客户端层的屏蔽,不填默认为不开启)
--如果是在跨服环境下开启的话, 需要去Gameplay_ActivityAlert中填表
function CLuaActivityAlert:OnSyncDynamicActivitySimpleInfo( )
    if not self.root then return end
    if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.IsInGamePlay or LuaWeddingIterationMgr.IsOnParade or CGameVideoMgr.Inst:IsInGameVideoScene() then
        self.root:SetActive(false)
        return
    end
    self.root:SetActive(true)

    self.yayunButton.transform.parent = self.pool.transform
    self.yayunButton:SetActive(false)

    Extensions.RemoveAllChildren(self.grid.transform)
    Extensions.RemoveAllChildren(self.Grid2.transform)
    self:CancelTick()
    self:CancelHuiliuTick()
    self:CancelTianJiangBaoXiangTick()

    if self:CheckQMPKShow() then return end -- 全民PK服除了全民pk实名认证，不显示其他活动

    self:ShowHuiLiuGift()

    self:ShowInfoList()

    self:ShwoRecommendGift()
    self:ShowLingyuGiftRecommend()

    -- 押镖
    if CActivityAlertMgr.Inst:ExistYaYunNpc(CActivityAlertMgr.Inst.YaYunNpcId) and CActivityAlertMgr.Inst.YaYunNpcCount > 0 then
        self.yayunButton.transform.parent = self.grid.transform
        self.yayunButton:SetActive(true)

        self.yayunButton:GetComponent(typeof(CUITexture)):LoadMaterial(CActivityAlertMgr.Inst:GetYaYunNpcIcon(CActivityAlertMgr.Inst.YaYunNpcId))
        self.yayunButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("余%d"), CActivityAlertMgr.Inst.YaYunNpcCount)
        UIEventListener.Get(self.yayunButton).onClick = DelegateFactory.VoidDelegate(function(g)
            if CScene.MainScene then
                Gac2Gas.RequestGoDynamicActivitySite(7, CScene.MainScene.SceneTemplateId, "")
            end
        end)
    end

    -- 直播按钮
    if CLuaActivityAlert.s_ZhiBoStatus and not CClientHouseMgr.Inst:IsPlayerInHouse() then
        local go = NGUITools.AddChild(self.grid.gameObject, self.alertButton)
        go:SetActive(true)
    end

    -- 家园大赛直播
    if CLuaActivityAlert.s_HouseCompetitionStatus then
        local go = NGUITools.AddChild(self.grid.gameObject, self.houseCompetitionButton)
        go:SetActive(true)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
            if CLuaActivityAlert.s_HouseCompetitionStatus then
                CLuaWebBrowserMgr.OpenHouseActivityUrl("houseDesignMatchlive")
            end
        end)
    end

    if CLuaActivityAlert.s_DoubleOneActivityStatus then
        local go = NGUITools.AddChild(self.grid.gameObject, self.doubleOneActivityNode)
        go:SetActive(true)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
            Gac2Gas.QueryBaoTuanZuoZhanApplyInfo()
        end)
    end

    local uniformdatas = LuaActivityAlertMgr.AlertScheduleIDs
    for scheduleID, scheduleData in pairs(uniformdatas) do
        if scheduleData then
            local tempdata = Task_JieRiSchedule.GetData(scheduleID)
            local uidate = self:GetScheduleData(scheduleID)
            if uidate then
                self:TryAddAlertButton(tostring(scheduleID), uidate.Func)
            end
        end
    end

    --函数头部有注释, 注意查收
    self:TryAddAlertButton(self.professionTransferButton.name, self.ShowProfessionTransferButton)
    self:TryAddAlertButton(self.fashionLotteryButton.name, self.ShowFashionLotteryButton)

    self:ShowDoubleOneBonus()
    self:ProcessHaMaoEntry()
    self:ProcessTianJiangBaoXiangEntry()
    self:ProcessSeaFishingPlayEntry()
    self:ProcessShenYaoEntry()
    self:ProcessXinBaiEntry()
    self:ShowHanJia2023BattlePassButton()
    self:ShowCommonPassportButton()
    self:ShowZNQ2023BattlePassButton()
    self:ShowShuJia2023WorldEventButton()
    self:ProcessQianYingChuWenEntry()
    self:ShowZNQDiscount()
    self:ShowNanDuButton()
    self:ShowJieRiEntry()
    self:ShowHouseReportButton()
    self:ShowHongBaoButton()
    self:ShowYaoYeManJuanButton()

    self.grid:Reposition()
    self.Grid2:Reposition()
    self:CheckPosition()
end

function CLuaActivityAlert:CheckQMPKShow()
    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
        self:ShowQMPKCertificationEntry()
        self.grid:Reposition()
        self.Grid2:Reposition()
        self:CheckPosition()
        return true
    end
    return false
end

function CLuaActivityAlert:ShowInfoList()
    local infoList = {}
    CommonDefs.ListIterate(CActivityAlertMgr.Inst.infoList, DelegateFactory.Action_object(function (___value)
        local item = ___value
        table.insert(infoList, item)  
    end))
    table.sort(infoList,function (a, b)
        return a.activityType < b.activityType
    end)
    for i = 1,#infoList do
        local item = infoList[i]
        if item.count > 0 then
            local obj = self.pool:GetFromPool(0)
            obj.transform.parent = self.Grid2.transform
            obj.transform.localScale = Vector3.one
            obj:SetActive(true)
            local cmp = obj:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
            cmp:Init(item)
        end
    end
end

function CLuaActivityAlert:ShowHuiLiuGift()
    CLuaActivityAlert.s_HuiLiuGiftStatus = CLuaActivityAlert.s_HuiLiuGiftStatus and LuaHuiLiuMgr.GiftCountDownEndTime and LuaHuiLiuMgr.GiftCountDownEndTime > CServerTimeMgr.Inst.timeStamp
    CLuaActivityAlert.s_HuiLiuGiftTimeRemain = math.floor(LuaHuiLiuMgr.GiftCountDownEndTime - CServerTimeMgr.Inst.timeStamp)
    if CLuaActivityAlert.s_HuiLiuGiftStatus then
        local go = NGUITools.AddChild(self.grid.gameObject, self.HuiLiuButton)
        go:SetActive(CLuaActivityAlert.s_HuiLiuGiftTimeRemain > 0)

        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
            self:OnHuiLiuButtonClick(g)
        end)

        local countDownLabel = CommonDefs.GetComponent_Component_Type(CUICommonDef.TraverseFindChild("CountDownLabel", go.transform), typeof(UILabel))
        local bg = CUICommonDef.TraverseFindChild("Sprite", go.transform).gameObject
        local hour = math.floor(CLuaActivityAlert.s_HuiLiuGiftTimeRemain / 3600)
        local minute = math.floor(CLuaActivityAlert.s_HuiLiuGiftTimeRemain % 3600 / 60)
        local second = CLuaActivityAlert.s_HuiLiuGiftTimeRemain % 60
        countDownLabel.text = SafeStringFormat3("%02d:%02d:%02d", hour, minute, second)

        self.m_HuiliuCountDownTick = CTickMgr.Register(DelegateFactory.Action(function ()
            CLuaActivityAlert.s_HuiLiuGiftTimeRemain = math.floor(LuaHuiLiuMgr.GiftCountDownEndTime - CServerTimeMgr.Inst.timeStamp)
                    if CLuaActivityAlert.s_HuiLiuGiftTimeRemain > 0 then
                        local h = math.floor(CLuaActivityAlert.s_HuiLiuGiftTimeRemain / 3600)
                        local m = math.floor(CLuaActivityAlert.s_HuiLiuGiftTimeRemain % 3600 / 60)
                        local s = CLuaActivityAlert.s_HuiLiuGiftTimeRemain % 60
                        if countDownLabel ~= nil then
                            countDownLabel.text = SafeStringFormat3("%02d:%02d:%02d", h, m, s)
                        end
                    else
                self:CancelHuiliuTick()
                go:SetActive(false)
                self.grid:Reposition()
            end
        end), 1000, ETickType.Loop)
    end
end

--南都繁华 Begin
function CLuaActivityAlert:ShowNanDuButton()
    if not CClientMainPlayer.Inst or CClientMainPlayer.Inst.BasicProp.Level < 50 then return end
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsCrossServerPlayer then return end

    if LuaYiRenSiMgr:ShowActivityAlert() then
        if LuaYiRenSiMgr.isActivityAlertFirstSyncDaFuWen then
            Gac2Gas.RequestDaFuWengPlayData()
            LuaYiRenSiMgr.isActivityAlertFirstSyncDaFuWen = false
        end
        self.nandufanhuaButtonInstance = NGUITools.AddChild(self.grid.gameObject, self.nandufanhuaButton)
        self.nandufanhuaButtonInstance:SetActive(true)
        UIEventListener.Get(self.nandufanhuaButtonInstance).onClick = DelegateFactory.VoidDelegate(function(g)
            LuaYiRenSiMgr.showBlueLineFx = false
            LuaYiRenSiMgr.showDaFuWenRedPoint = false
            CUIManager.ShowUI("NanDuFanHuaWorldEventsMainWnd")
        end)

        self:OnDaFuWenDataUpdate()

        local fxComp = self.nandufanhuaButtonInstance.transform:Find("blueLineFx"):GetComponent(typeof(CUIFx))
        if LuaYiRenSiMgr.showBlueLineFx then
            local b = NGUIMath.CalculateRelativeWidgetBounds(self.nandufanhuaButtonInstance.transform)
            local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
            fxComp:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
            fxComp.transform.localPosition = waypoints[0]
            LuaTweenUtils.DOLuaLocalPath(fxComp.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, -1, Ease.Linear)
        else
            fxComp:DestroyFx()
            LuaTweenUtils.DOKill(fxComp.transform, true)
        end
    end
end

function CLuaActivityAlert:OnDaFuWenDataUpdate()
    if not CommonDefs.IsNull(self.nandufanhuaButtonInstance) then
        local showNanDuRedPoint = false
        --通行证是否有奖励
        showNanDuRedPoint = showNanDuRedPoint or LuaDaFuWongMgr:TongXingZhengReward()

        --大富翁日常任务是否完成, 且当前为登陆后第一次点击
        local res = LuaDaFuWongMgr:GetTongXingZhengDataInfo()
        if res then
            showNanDuRedPoint = showNanDuRedPoint or (res.hasDailyTask and LuaYiRenSiMgr.showDaFuWenRedPoint)
        end
        self.nandufanhuaButtonInstance.transform:Find("Alert").gameObject:SetActive(showNanDuRedPoint)
    end
end
--南都繁华 End

--战狂转职 Begin
function CLuaActivityAlert:ShowProfessionTransferButton()
    if not CClientMainPlayer.Inst or CClientMainPlayer.Inst.BasicProp.Level < 20 then return end
    
    if LuaProfessionTransferMgr.ShowProfessionTransferAlert() then
        self.ProfessionTransferButtonInstance = NGUITools.AddChild(self.grid.gameObject, self.professionTransferButton)
        self.ProfessionTransferButtonInstance:SetActive(true)
        UIEventListener.Get(self.ProfessionTransferButtonInstance).onClick = DelegateFactory.VoidDelegate(function(g)
            CUIManager.ShowUI("ProfessionTransferMainWnd")
        end)
    end
end
--战狂转职 End

--妖叶漫卷 Begin
function CLuaActivityAlert:ShowYaoYeManJuanButton()
    if CLuaActivityAlert.s_YaoYeManJuanPlayStatus then
        local go = NGUITools.AddChild(self.Grid2.gameObject, self.YaoYeManJuanButton)
        local timesLab = go.transform:Find("TimesLab"):GetComponent(typeof(UILabel))
        go:SetActive(CClientMainPlayer.Inst and not CClientMainPlayer.Inst.IsCrossServerPlayer and CClientMainPlayer.Inst.MaxLevel >= tonumber(YaoYeManJuan_Setting.GetData("LevelLimit").Value))
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
            g_MessageMgr:ShowMessage("YaoYeManJuan_Enter_Rule")
        end)
        local dailyRewardTimesLimit = tonumber(YaoYeManJuan_Setting.GetData("DailyRewardTimesLimit").Value)
        local weeklyRewardTimesLimit = tonumber(YaoYeManJuan_Setting.GetData("WeeklyRewardTimesLimit").Value)

        local dailyRewardTimes = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eYaoYeManJuan_DailyRewardTimes)
        local weeklyRewardTimes = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eYaoYeManJuan_WeeklyRewardTimes)

        if weeklyRewardTimes >= weeklyRewardTimesLimit then
            timesLab.text = LocalString.GetString("本周完成")
        elseif dailyRewardTimes >= dailyRewardTimesLimit then
            timesLab.text = LocalString.GetString("今日完成")
        else
            timesLab.text = SafeStringFormat3(LocalString.GetString("今日%d/%d"), dailyRewardTimes, dailyRewardTimesLimit)
        end
    end
end
--妖叶漫卷 End

function CLuaActivityAlert:ShowDoubleOneBonus()
    if CLuaActivityAlert.s_DoubleOneBonusStatus then
        local go = NGUITools.AddChild(self.grid.gameObject, self.ZNQDiscount)
        CLuaActivityAlert.s_DoubleOneBonusTimeRemain = math.floor(CLuaActivityAlert.s_DoubleOneBonusTimeTotalRemain - (CServerTimeMgr.Inst.timeStamp - CLuaActivityAlert.s_DoubleOneBonusTimeBegin))
        go:SetActive(CLuaActivityAlert.s_DoubleOneBonusTimeRemain > 0 or CLuaActivityAlert.s_GiftAlertType == EnumGiftAlertType.JiaNianHua)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
            CUIManager.ShowUI("DoubleOneBonusTimeLimitWnd")
        end)

        local countDownLabel = CommonDefs.GetComponent_Component_Type(CUICommonDef.TraverseFindChild("CountDownLabel", go.transform), typeof(UILabel))
        local bg = CUICommonDef.TraverseFindChild("Sprite", go.transform).gameObject
        local hour = math.floor(CLuaActivityAlert.s_DoubleOneBonusTimeRemain / 3600)
        local minute = math.floor(CLuaActivityAlert.s_DoubleOneBonusTimeRemain % 3600 / 60)
        local second = CLuaActivityAlert.s_DoubleOneBonusTimeRemain % 60
        countDownLabel.text = SafeStringFormat3("%02d:%02d:%02d", hour, minute, second)

        self.m_CountDownTick = CTickMgr.Register(DelegateFactory.Action(function ()
            CLuaActivityAlert.s_DoubleOneBonusTimeRemain = math.floor(CLuaActivityAlert.s_DoubleOneBonusTimeTotalRemain - (CServerTimeMgr.Inst.timeStamp - CLuaActivityAlert.s_DoubleOneBonusTimeBegin))
            if CLuaActivityAlert.s_DoubleOneBonusTimeRemain > 0 then
                local h = math.floor(CLuaActivityAlert.s_DoubleOneBonusTimeRemain / 3600)
                local m = math.floor(CLuaActivityAlert.s_DoubleOneBonusTimeRemain % 3600 / 60)
                local s = CLuaActivityAlert.s_DoubleOneBonusTimeRemain % 60
                        if countDownLabel ~= nil then
                            countDownLabel.text = SafeStringFormat3("%02d:%02d:%02d", h, m, s)
                        end
                    else
                        self:CancelTick()
                        if CLuaActivityAlert.s_GiftAlertType == EnumGiftAlertType.JiaNianHua then
                            countDownLabel.gameObject:SetActive(false)
                            bg:SetActive(false)
                        end
                    end
            go:SetActive(CLuaActivityAlert.s_DoubleOneBonusTimeRemain > 0 or CLuaActivityAlert.s_GiftAlertType == EnumGiftAlertType.JiaNianHua)
            self.grid:Reposition()
        end), 1000, ETickType.Loop)
    end
end

function CLuaActivityAlert:ShwoRecommendGift()
    --系统推荐礼包
    self:CancelRecommendGiftTick()
    if CLuaActivityAlert.s_RecommendGiftStatus then
        local go = NGUITools.AddChild(self.grid.gameObject, self.recommendGiftButton)
        LuaRecommendGiftMgr.RemainTime = math.floor(LuaRecommendGiftMgr.ExpireTime - CServerTimeMgr.Inst.timeStamp)
        go:SetActive(LuaRecommendGiftMgr.RemainTime >= 0)

        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
            self:OnRecommendGiftButtonClick(g)
        end)
        local countDownLabel = go.transform:Find("CountDownLabel"):GetComponent(typeof(UILabel))
        local hour = math.floor(LuaRecommendGiftMgr.RemainTime / 3600)
        local minute = math.floor(LuaRecommendGiftMgr.RemainTime % 3600 / 60)
        local second = LuaRecommendGiftMgr.RemainTime % 60
        if countDownLabel then
            countDownLabel.text = SafeStringFormat3("%02d:%02d:%02d", hour, minute, second)
        end
        self.m_RecommendGiftCountDownTick = CTickMgr.Register(DelegateFactory.Action(function ()
            LuaRecommendGiftMgr.RemainTime = math.floor(LuaRecommendGiftMgr.ExpireTime - CServerTimeMgr.Inst.timeStamp)
            if LuaRecommendGiftMgr.RemainTime >= 0 then
                local h = math.floor(LuaRecommendGiftMgr.RemainTime / 3600)
                local m = math.floor(LuaRecommendGiftMgr.RemainTime % 3600 / 60)
                local s = LuaRecommendGiftMgr.RemainTime % 60
                if countDownLabel then
                    countDownLabel.text = SafeStringFormat3("%02d:%02d:%02d", h, m, s)
                end
            else
                self:CancelRecommendGiftTick()
                go:SetActive(false)
                CLuaActivityAlert.s_RecommendGiftStatus = false
                self:OnSyncDynamicActivitySimpleInfo()
            end
        end), 1000, ETickType.Loop)
    end
end

function CLuaActivityAlert:ShowZNQDiscount()
       -- 周年庆限时商店
    if CLuaActivityAlert.s_ZNQDiscountStatus then
        local go = NGUITools.AddChild(self.grid.gameObject, self.ZNQDiscount)
        go:SetActive(CLuaActivityAlert.s_ZNQTimeRemain > 0 or CLuaActivityAlert.s_GiftAlertType == EnumGiftAlertType.JiaNianHua)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
            self:OnZNQDiscountClick(g)
        end)

        local countDownLabel = CommonDefs.GetComponent_Component_Type(CUICommonDef.TraverseFindChild("CountDownLabel", go.transform), typeof(UILabel))
        local bg = CUICommonDef.TraverseFindChild("Sprite", go.transform).gameObject
        local hour = math.floor(CLuaActivityAlert.s_ZNQTimeRemain / 3600)
        local minute = math.floor(CLuaActivityAlert.s_ZNQTimeRemain % 3600 / 60)
        local second = CLuaActivityAlert.s_ZNQTimeRemain % 60
        countDownLabel.text = SafeStringFormat3("%02d:%02d:%02d", hour, minute, second)
        if CLuaActivityAlert.s_GiftAlertType == EnumGiftAlertType.JiaNianHua then
            countDownLabel.color = Color.red
            if CLuaActivityAlert.s_ZNQTimeRemain <= 0 then
                countDownLabel.gameObject:SetActive(false)
                bg:SetActive(false)
            end
        end

        self.m_CountDownTick = CTickMgr.Register(DelegateFactory.Action(function ()
            if CLuaActivityAlert.s_ZNQTimeRemain > 0 then
                CLuaActivityAlert.s_ZNQTimeRemain = CLuaActivityAlert.s_ZNQTimeRemain - 1
                local h = math.floor(CLuaActivityAlert.s_ZNQTimeRemain / 3600)
                local m = math.floor(CLuaActivityAlert.s_ZNQTimeRemain % 3600 / 60)
                local s = CLuaActivityAlert.s_ZNQTimeRemain % 60
                if countDownLabel ~= nil then
                    countDownLabel.text = SafeStringFormat3("%02d:%02d:%02d", h, m, s)
                end
            else
                        self:CancelTick()
                        if CLuaActivityAlert.s_GiftAlertType == EnumGiftAlertType.JiaNianHua then
                            countDownLabel.gameObject:SetActive(false)
                            bg:SetActive(false)
                        end
                    end
            go:SetActive(CLuaActivityAlert.s_ZNQTimeRemain > 0 or CLuaActivityAlert.s_GiftAlertType == EnumGiftAlertType.JiaNianHua)
            self.grid:Reposition()
        end), 1000, ETickType.Loop)
    end
end

function CLuaActivityAlert:ShowHouseReportButton()
    --家园截图举报按钮
    if CClientHouseMgr.Inst:IsPlayerInHouse() and not CClientHouseMgr.Inst:IsInOwnHouse() then
        if CClientHouseMgr.Inst.mCurExtraInfo.IsSpokesmanHouse == 1 then --代言人的家园不显示举报按钮
        elseif CommonDefs.IS_HMT_CLIENT then--hmt屏蔽家园截图举报按钮
        elseif CommonDefs.IS_VN_CLIENT then--vn屏蔽家园截图举报按钮
        else
            local go = NGUITools.AddChild(self.grid.gameObject, self.houseReportButton)
            go:SetActive(true)
            UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
                UICommonDef.CaptureScreen(
                "house_report_screenshot",
                true,
                false,
                nil,
                DelegateFactory.Action_string_bytes(
                    function(fullPath, jpgBytes)
                        CLuaHouseScreenshotReportMgr.ImagePath = fullPath
                        CUIManager.ShowUI(CLuaUIResources.HouseScreenshotReportWnd)
                    end
                ),
                true
                )
            end)
        end
    end
end

function CLuaActivityAlert:ShowJieRiEntry()
    local items = CLuaScheduleMgr:GetTodayFestivals()
    if not items or #items == 0 then 
        return
    end
    local go = NGUITools.AddChild(self.grid.gameObject, self.JieRiButton)
    go:SetActive(CClientMainPlayer.Inst and not CClientMainPlayer.Inst.IsCrossServerPlayer and CClientMainPlayer.Inst.MaxLevel >= FestivalHelper_Setting.GetData().FestivalBtnOpenLevel)
    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
        self:OnJieRiButtonClicked()
    end)
end

function CLuaActivityAlert:OnUpdateActivity()
    self:OnSyncDynamicActivitySimpleInfo()
end

function CLuaActivityAlert:OnJieRiButtonClicked()
    local items = CLuaScheduleMgr:GetTodayFestivals()
    if not items or #items == 0 then 
        return
    end

    if #items > 1 then
        CUIManager.ShowUI(CLuaUIResources.JieRiGroupScheduleWnd)
        return
    end
    local info = items[1]

    CLuaScheduleMgr.OnJieRiGroupScheduleClick(info)
    --CLuaScheduleMgr.TrackSchedule(info)
end

function CLuaActivityAlert:ShowQMPKCertificationEntry()
    if not CLuaActivityAlert.s_QMPKCertificationStatus then
        return
    end
    local go = NGUITools.AddChild(self.grid.gameObject, self.QMPKCertificationButton)
    go:SetActive(true)
    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
        self:OnQMPKCertificationButtonClicked(g)
    end)
end

function CLuaActivityAlert:OnQMPKCertificationButtonClicked(g)
    CLuaActivityAlert.s_QMPKCertificationClicked = true
    local alert = g.transform:Find("Alert").gameObject
    alert:SetActive(false)
    CUIManager.ShowUI(CLuaUIResources.QMPKCertificationWnd)
end

function CLuaActivityAlert:ShowShuJia2023WorldEventButton()
    self:CancelShuJia2023WorldEventTick()
    if LuaShuJia2023Mgr.m_StageTwoStatus == 0 then return end
    local time1 = tonumber(ShuJia2023_Setting.GetData("GenerateBossTime").Value)
    local time2 = tonumber(ShuJia2023_Setting.GetData("BossLiveTime").Value)
    local cd = math.floor(LuaShuJia2023Mgr.m_StageTwoStartTime + time1 + time2 - CServerTimeMgr.Inst.timeStamp)
    if cd <= 0 then return end

    local go = NGUITools.AddChild(self.grid.gameObject, self.ShuJia2023WorldEventButton)
    go:SetActive(true)
    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
        LuaShuJia2023Mgr:OpenWorldEventWnd()
    end)

    local cdLabel = go.transform:Find("CountDownLabel"):GetComponent(typeof(UILabel))
    cdLabel.text = SafeStringFormat3(LocalString.GetString("%02d:%02d"), cd / 60, cd % 60)
    self.m_ShuJia2023WorldEventTick = RegisterTick(function()
        cd = math.floor(LuaShuJia2023Mgr.m_StageTwoStartTime + time1 + time2 - CServerTimeMgr.Inst.timeStamp)
        if cd <= 0 then
            self:CancelShuJia2023WorldEventTick()
            if go then
                go:SetActive(false)
            end
            if self.grid then
                self.grid:Reposition()
            end
        else
            cdLabel.text = SafeStringFormat3(LocalString.GetString("%02d:%02d"), cd / 60, cd % 60)
        end
    end, 500)
end

function CLuaActivityAlert:ShowZNQ2023BattlePassButton()
    self:CancelZNQ2023PassTick()
    if not LuaZhouNianQing2023Mgr:IsPassOpen() or not CClientMainPlayer.Inst or CClientMainPlayer.Inst.BasicProp.Level < 30 then return end
    local go = NGUITools.AddChild(self.grid.gameObject, self.ZNQ2023BattlePassButton)
    go:SetActive(true)
    local alert = go.transform:Find("Alert").gameObject
    alert:SetActive(LuaZhouNianQing2023Mgr.m_bAlert)
    local lvLabel = go.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    lvLabel.text = "Lv."..LuaZhouNianQing2023Mgr:GetCurZhanLingLevel()
    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
        LuaZhouNianQing2023Mgr:OpenZhanLingWnd()
    end)
    
    self.m_ZNQ2023PassTick = CTickMgr.Register(DelegateFactory.Action(function ()
        if not LuaZhouNianQing2023Mgr:IsPassOpen() then
            self:CancelZNQ2023PassTick()
            if go then
                go:SetActive(false)
            end
            if self.grid then
                self.grid:Reposition()
            end
        else
            if alert then 
                alert:SetActive(LuaZhouNianQing2023Mgr.m_bAlert)
            end
        end
    end), 1000, ETickType.Loop)
end

function CLuaActivityAlert:ShowHanJia2023BattlePassButton()
    if not CLuaActivityAlert.s_HanJia2023BattlePassStatus then
        return
    end

    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.Level >= HanJia2023_XianKeLaiSetting.GetData().ValidPlayerLevel then

        local hanjiaJieRiId = 27
        local battlePassActivityId = 42030297
        local openFestivalTasks = CLuaScheduleMgr.m_OpenFestivalTasks[hanjiaJieRiId] or {}
        local scheduleData = Task_Schedule.GetData(battlePassActivityId)
        local taskId = scheduleData.TaskID[0]
        local battlePassOn = false
        for i = 1, #openFestivalTasks do
            if openFestivalTasks[i] == taskId then
                battlePassOn = true
            end
        end
        if battlePassOn then
            self.hanjia2023BattlePassButtonInstance = NGUITools.AddChild(self.grid.gameObject, self.hanjia2023BattlePassButton)
            self.hanjia2023BattlePassButtonInstance:SetActive(true)
            UIEventListener.Get(self.hanjia2023BattlePassButtonInstance).onClick = DelegateFactory.VoidDelegate(function(g)
                LuaHanJia2023Mgr:LoadCfg(true)
                CUIManager.ShowUI(CLuaUIResources.HanJia2023PassMainWnd)
            end)

            if LuaHanJia2023Mgr.XKLData and LuaHanJia2023Mgr.XKLData.Lv then
                if LuaHanJia2023Mgr.XKLData.addRate == 0 then
                    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.HanJia2023BattlePassOldServerIntrodutionGuide)
                else
                    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.HanJia2023BattlePassIntrodutionGuide)
                end
                self.hanjia2023BattlePassButtonInstance.transform:Find("LevelLabel"):GetComponent(typeof(UILabel)).text = "Lv." .. LuaHanJia2023Mgr.XKLData.Lv
                self.hanjia2023BattlePassButtonInstance.transform:Find("Alert").gameObject:SetActive(LuaHanJia2023Mgr:CheckHaveReward())
            else
                self.hanjia2023BattlePassButtonInstance.transform:Find("LevelLabel"):GetComponent(typeof(UILabel)).text = ""
                local curtime = CServerTimeMgr.Inst.timeStamp
                if curtime - self.s_LastHanJia2023SyncTimeStamp > 1 then
                    self.s_LastHanJia2023SyncTimeStamp = curtime
                    Gac2Gas.QueryXianKeLaiPlayData()
                end
            end
        end
    end
end

function CLuaActivityAlert:ShowFashionLotteryButton()
    self:CancelPermanentFashionLotteryTick()
    local showAlert = LuaFashionLotteryMgr:ShowActivityAlert()
    if showAlert then
        local showBtn, iconName, remainTimeStr = LuaFashionLotteryMgr:GetRemainTimeStr()
        if not showBtn then return end
        local go = NGUITools.AddChild(self.grid.gameObject, self.fashionLotteryButton)
        go:SetActive(true)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
            LuaFashionLotteryMgr:OpenFashionLotteryWnd()
        end)

        local cdLabel = go.transform:Find("CountDownLabel"):GetComponent(typeof(UILabel))
        local iconSprite = go.transform:GetComponent(typeof(UISprite))
        local initBtn = function ()
            if cdLabel then
                cdLabel.gameObject:SetActive(remainTimeStr)
                cdLabel.text = remainTimeStr
            end
            if iconName and iconSprite then
                iconSprite.spriteName = iconName
            end
        end
        initBtn()
        self.m_PermanentFashionLotteryTick = RegisterTick(function()
            showBtn, iconName, remainTimeStr = LuaFashionLotteryMgr:GetRemainTimeStr()
            if showBtn then
                initBtn()
            else
                self:CancelPermanentFashionLotteryTick()
                if go then
                    go:SetActive(false)
                end
                if self.grid then
                    self.grid:Reposition()
                end
            end
        end, 500)
    end
end

function CLuaActivityAlert:ShowCommonPassportButton()
    local openPassIdTbl = LuaCommonPassportMgr.openPassIdTbl
    if #openPassIdTbl > 0 then
        for _, id in ipairs(openPassIdTbl) do
            local buttonIcon = Pass_ClientSetting.GetData(id).Icon
            if not System.String.IsNullOrEmpty(buttonIcon) and LuaCommonPassportMgr:IsPlayerLevelOk(id) then
                local child = NGUITools.AddChild(self.grid.gameObject, self.commonPassportButton)
                child:SetActive(true)
                child.transform:GetComponent(typeof(CCommonLuaScript)):Init({id})
            end
        end
    end
end

function CLuaActivityAlert:OnUpdateXianKeLaiData()
    if not CommonDefs.IsNull(self.hanjia2023BattlePassButtonInstance) then
        if LuaHanJia2023Mgr.XKLData and LuaHanJia2023Mgr.XKLData.Lv then
            self.hanjia2023BattlePassButtonInstance.transform:Find("LevelLabel"):GetComponent(typeof(UILabel)).text = "Lv." .. LuaHanJia2023Mgr.XKLData.Lv
            self.hanjia2023BattlePassButtonInstance.transform:Find("Alert").gameObject:SetActive(LuaHanJia2023Mgr:CheckHaveReward())
        else
            self.hanjia2023BattlePassButtonInstance.transform:Find("LevelLabel"):GetComponent(typeof(UILabel)).text = ""
        end
    end
end

function CLuaActivityAlert:OnPlayerSendHongBao(args)
    local channelType, amount = args[0],args[1]
    self.m_CurHongbaoAmount = amount
    if channelType == EnumHongbaoType_lua.WorldHongbao or channelType == EnumHongbaoType_lua.SystemHongbao then
        self.m_CurHongbaoType = EnumHongbaoType.WorldHongbao
    elseif channelType == EnumHongbaoType_lua.GuildHongbao then
        self.m_CurHongbaoType = EnumHongbaoType.GuildHongbao
    elseif channelType == EnumHongbaoType_lua.SectHongbao then
        self.m_CurHongbaoType = EnumHongbaoType.SectHongbao
    end
    self:OnSyncDynamicActivitySimpleInfo()
    self:CancelHongBaoTick()
    local cd =  self.m_CurHongbaoType == EnumHongbaoType.WorldHongbao and HongBao_Setting.GetData().Show_CD_World or HongBao_Setting.GetData().Show_CD_Guild
    self.m_HongbaoTick = RegisterTickOnce(function ()
        self:HideHongBaoBtn()
    end,cd * 1000)
end

function CLuaActivityAlert:ShowHongBaoButton()
    if not self.m_CurHongbaoType then return end
    if self.m_CurHongbaoType == EnumHongbaoType.WorldHongbao or self.m_CurHongbaoType == EnumHongbaoType.GuildHongbao then
        local go = NGUITools.AddChild(self.Grid2.gameObject, self.HongBaoButton)
        go:SetActive(true)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
            self:OnHongBaoBtnClicked()
        end)
        local hongbaoTypeSprite = go.transform:Find("HongBaoTypeSprite"):GetComponent(typeof(UISprite))
        hongbaoTypeSprite.spriteName = self.m_CurHongbaoType == EnumHongbaoType.WorldHongbao and Constants.WorldChannelSpriteName or Constants.GuildChannelSpriteName
        local texture = go:GetComponent(typeof(CUITexture))
        if texture ~= nil then
            texture:LoadMaterial(CHongBaoMgr.Inst:GetHongBaoIconByAmount(self.m_CurHongbaoAmount, false,CHongBaoMgr.Inst.m_HongbaoCoverType))
        end
    elseif self.m_CurHongbaoType == EnumHongbaoType.SectHongbao and LuaZongMenMgr.m_HongBaoIsOpen then
        local go = NGUITools.AddChild(self.Grid2.gameObject, self.SectHongBaoButton)
        go:SetActive(true)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
            self:OnHongBaoBtnClicked()
        end)
    end
end

function CLuaActivityAlert:CancelHongBaoTick()
    if self.m_HongbaoTick then
        UnRegisterTick(self.m_HongbaoTick)
        self.m_HongbaoTick = nil
    end
end

function CLuaActivityAlert:OnHongBaoBtnClicked()
    if self.m_CurHongbaoType == EnumHongbaoType.WorldHongbao or self.m_CurHongbaoType == EnumHongbaoType.GuildHongbao or 
    self.m_CurHongbaoType == EnumHongbaoType.SectHongbao then
        CHongBaoMgr.ShowHongBaoWnd(self.m_CurHongbaoType)
    end
    self:HideHongBaoBtn()
end

function CLuaActivityAlert:HideHongBaoBtn()
    self.m_CurHongbaoType = nil
    self:OnSyncDynamicActivitySimpleInfo()
end

function CLuaActivityAlert:ProcessShenYaoEntry()
    self:CancelShenYaoTick()
    if not CLuaActivityAlert.s_ShenYaoPlayStatus then return end
    local go = NGUITools.AddChild(self.Grid2.gameObject, self.ShenYaoButton)
    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
        CUIManager.ShowUI(CLuaUIResources.ShenYaoLeftTimeWnd)
    end)
    
    local duration = XinBaiLianDong_Setting.GetData().ShenYao_StartCronStrDuration
    local expiredTime = self.m_ShenYaoPlayStartTime and self.m_ShenYaoPlayStartTime + duration * 60 or 0
    go:SetActive(CServerTimeMgr.Inst.timeStamp <= expiredTime and CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.Level >= XinBaiLianDong_Setting.GetData().ShenYao_LevelLimit)
    self.m_ShenYaoCountDownTick = CTickMgr.Register(DelegateFactory.Action(function ()
        if CServerTimeMgr.Inst.timeStamp > expiredTime then
            self:CancelShenYaoTick()
            if go then
                go:SetActive(false)
            end
            if self.Grid2 then
                self.Grid2:Reposition()
            end
        end
    end), 1000, ETickType.Loop)
end

function CLuaActivityAlert:ProcessHaMaoEntry()
    if not IsHaMaoButtonShow() then return end
    
    if CClientMainPlayer.Inst then
        if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.HaMaoEntry) then
            if CClientMainPlayer.Inst.Level>=10 then
                local taskFinished = CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(22203440)
                if taskFinished then
                    show=true
                    local go = NGUITools.AddChild(self.grid.gameObject, self.HaMaoButton)
                    go:SetActive(true)

                    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(go)
                        CUIManager.ShowUI(CLuaUIResources.HaMaoTaskMessageBox)
                    end)
                end
            end
        end
    end
end

function CLuaActivityAlert:ProcessSeaFishingPlayEntry()
    if CLuaActivityAlert.s_SeaFishingEntryStatus then
        local go = NGUITools.AddChild(self.Grid2.gameObject, self.SeaFishingPlayButton)
        go:SetActive(true)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
            local setting = HaiDiaoFuBen_Settings.GetData()
            local entranceStr = setting.EntranceNpc
            local npcId,posx,posz = string.match(entranceStr, "(%d+),(%d+),(%d+)")
            local yinxianwan = setting.YinXianHaiMapId
            CTrackMgr.Inst:FindNPC(npcId, yinxianwan, posx, posz, nil, nil)
        end)
    end
end

function CLuaActivityAlert:ProcessTianJiangBaoXiangEntry()
    if not CLuaActivityAlert.s_TianJiangBaoXiangStatus then return end 
    local go = NGUITools.AddChild(self.grid.gameObject, self.TianJiangBaoXiangButton)
        go:SetActive(CLuaActivityAlert.s_TianJiangBaoXiangTimeRemain > 0)
    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
        Gac2Gas.TianJiangBaoXiang_OpenWnd(tostring(EnumTianJiangBaoXiangDoor.None))
    end)
    local countDownLabel = go.transform:Find("CountDownLabel"):GetComponent(typeof(UILabel))
    CLuaActivityAlert.s_TianJiangBaoXiangTimeRemain = math.floor(LuaTianJiangBaoXiangMgr.StartPlayTimeStamp + LuaTianJiangBaoXiangMgr.PlayOpenTime - CServerTimeMgr.Inst.timeStamp)
        if CLuaActivityAlert.s_TianJiangBaoXiangTimeRemain > 0 then
            local h = math.floor(CLuaActivityAlert.s_TianJiangBaoXiangTimeRemain / 3600)
            local m = math.floor(CLuaActivityAlert.s_TianJiangBaoXiangTimeRemain % 3600 / 60)
            local s = CLuaActivityAlert.s_TianJiangBaoXiangTimeRemain % 60
            if countDownLabel ~= nil then
                countDownLabel.text = SafeStringFormat3("%02d:%02d:%02d", h, m, s)
            end
        end
    self:CancelTianJiangBaoXiangTick()
    self.m_TianJiangBaoXiangCountDownTick = CTickMgr.Register(DelegateFactory.Action(function ()
        CLuaActivityAlert.s_TianJiangBaoXiangTimeRemain = math.floor(LuaTianJiangBaoXiangMgr.StartPlayTimeStamp + LuaTianJiangBaoXiangMgr.PlayOpenTime - CServerTimeMgr.Inst.timeStamp)
        if CLuaActivityAlert.s_TianJiangBaoXiangTimeRemain > 0 then
            local h = math.floor(CLuaActivityAlert.s_TianJiangBaoXiangTimeRemain / 3600)
            local m = math.floor(CLuaActivityAlert.s_TianJiangBaoXiangTimeRemain % 3600 / 60)
            local s = CLuaActivityAlert.s_TianJiangBaoXiangTimeRemain % 60
            if countDownLabel ~= nil then
                countDownLabel.text = SafeStringFormat3("%02d:%02d:%02d", h, m, s)
            end
        else
            self:CancelTianJiangBaoXiangTick()
            CLuaActivityAlert.s_TianJiangBaoXiangStatus = false
            if go then
                go:SetActive(false)
            end
            if self.grid then
                self.grid:Reposition()
            end
        end
    end), 1000, ETickType.Loop)
end

function CLuaActivityAlert:ProcessXinBaiEntry()
    if LuaXinBaiLianDongMgr:CanShowXinBaiButton() then
        self.xinBaiButtonInstance = NGUITools.AddChild(self.grid.gameObject, self.xinBaiButton)
        self.xinBaiButtonInstance:SetActive(true)
        self.xinBaiButtonInstance.transform:GetComponent(typeof(CCommonLuaScript)):Init({})
    end
end

function CLuaActivityAlert:ProcessQianYingChuWenEntry()
    if LuaQianYingChuWenMgr:IsQianYingChuWenOpen() then
        self.qianYingChuWenButtonInstance = NGUITools.AddChild(self.grid.gameObject, self.qianYingChuWenButton)
        self.qianYingChuWenButtonInstance:SetActive(true)
        self.qianYingChuWenButtonInstance.transform:GetComponent(typeof(CCommonLuaScript)):Init({self.qianYingChuWenFx})
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.QianYingChuWenGuide)
    else
        self.qianYingChuWenFx:DestroyFx()
    end
end

function CLuaActivityAlert:ShowLingyuGiftRecommend()
    if LuaLingyuGiftRecommendMgr:HasTriggeredGift() then
        local go = NGUITools.AddChild(self.grid.gameObject, self.lingyuGiftRecommendButton)
        go:SetActive(true)
        go.transform:GetComponent(typeof(CCommonLuaScript)):Init({self.lingyuGiftRecommendFx})
    else
        self.lingyuGiftRecommendFx:DestroyFx()
    end
end

function CLuaActivityAlert:OnHuiLiuButtonClick(go)
    -- todo 弹出购买界面
    CUIManager.ShowUI(CLuaUIResources.HuiLiuGiftWnd)
end

function CLuaActivityAlert:OnRecommendGiftButtonClick(go)
    CUIManager.ShowUI(CLuaUIResources.SystemRecommendGiftWnd)
end

function CLuaActivityAlert:OnDestroy()
    self:Reset()
    CLuaActivityAlert.s_QMPKCertificationStatus = false
    CLuaActivityAlert.s_QMPKCertificationClicked = false
end

function CLuaActivityAlert:Reset()
    CLuaActivityAlert.s_ZhiBoStatus = false
    CLuaActivityAlert.s_HouseCompetitionStatus = false
    CLuaActivityAlert.s_ZNQDiscountStatus = false
    CLuaActivityAlert.s_DoubleOneActivityStatus = false
    CLuaActivityAlert.s_DoubleOneBonusStatus = false
    CLuaActivityAlert.s_HanJia2023BattlePassStatus = false

    CLuaActivityAlert.s_GiftAlertType = EnumGiftAlertType.Undefined

    CLuaActivityAlert.s_HouseScreenShotReportStatus = false
    CLuaActivityAlert.s_HuiLiuGiftStatus = false
	CLuaActivityAlert.s_RecommendGiftStatus = false
    CLuaActivityAlert.s_TianJiangBaoXiangStatus = false
    CLuaActivityAlert.s_SeaFishingEntryStatus = false
    CLuaActivityAlert.s_ShenYaoPlayStatus = false
    CLuaActivityAlert.s_YaoYeManJuanPlayStatus = false
    -- CLuaActivityAlert.s_QMPKCertificationStatus = false
    -- CLuaActivityAlert.s_QMPKCertificationClicked = false
    self:CancelTick()
    self:CancelHuiliuTick()
    self:CancelTianJiangBaoXiangTick()
    self:CancelShenYaoTick()
    self:CancelZNQ2023PassTick()
    self:CancelShuJia2023WorldEventTick()
    self:CancelPermanentFashionLotteryTick()
end

function CLuaActivityAlert:OnZNQDiscountClick( go)
    if CLuaActivityAlert.s_GiftAlertType == EnumGiftAlertType.ZNQ then
        CWelfareMgr.OpenWelfareWnd(LocalString.GetString("周年庆限时商店"))
    elseif CLuaActivityAlert.s_GiftAlertType == EnumGiftAlertType.JiaNianHua then
        CWelfareMgr.OpenWelfareWnd(LocalString.GetString("倩女嘉年华"))
    end
end

function CLuaActivityAlert:CancelTick( )
    if self.m_CountDownTick ~= nil then
        invoke(self.m_CountDownTick)
        self.m_CountDownTick = nil
    end

    for sid,uidate in pairs(self.m_ScheduleDatas) do
        if uidate then
            UnRegisterTick(uidate.Tick)
            uidate.Tick = nil
        end
    end
end

function CLuaActivityAlert:CancelHuiliuTick()
    if self.m_HuiliuCountDownTick then
        invoke(self.m_HuiliuCountDownTick)
        self.m_HuiliuCountDownTick = nil
    end
end

function CLuaActivityAlert:CancelRecommendGiftTick()
    if self.m_RecommendGiftCountDownTick then
        invoke(self.m_RecommendGiftCountDownTick)
        self.m_RecommendGiftCountDownTick = nil
    end
end

function CLuaActivityAlert:CancelTianJiangBaoXiangTick()
    if self.m_TianJiangBaoXiangCountDownTick then
        invoke(self.m_TianJiangBaoXiangCountDownTick)
        self.m_TianJiangBaoXiangCountDownTick = nil
    end
end

function CLuaActivityAlert:CancelShenYaoTick()
    if self.m_ShenYaoCountDownTick then
        invoke(self.m_ShenYaoCountDownTick)
        self.m_ShenYaoCountDownTick = nil
    end
end

function CLuaActivityAlert:CancelZNQ2023PassTick()
    if self.m_ZNQ2023PassTick then
        invoke(self.m_ZNQ2023PassTick)
        self.m_ZNQ2023PassTick = nil
    end
end

function CLuaActivityAlert:CancelShuJia2023WorldEventTick()
    if self.m_ShuJia2023WorldEventTick then
        invoke(self.m_ShuJia2023WorldEventTick)
        self.m_ShuJia2023WorldEventTick = nil
    end
end

function CLuaActivityAlert:CancelPermanentFashionLotteryTick()
    if self.m_PermanentFashionLotteryTick then
        invoke(self.m_PermanentFashionLotteryTick)
        self.m_PermanentFashionLotteryTick = nil
    end
end

function CLuaActivityAlert:GetGuideGo(methodName)
    if methodName == "GetQianYingChuWenButton" then
        return self.qianYingChuWenButtonInstance
    elseif methodName == "GetXinBaiButton" then
        return self.xinBaiButtonInstance
    elseif methodName == "GetHanJia2023BattlePassButton" then
        if not CommonDefs.IsNull(self.hanjia2023BattlePassButtonInstance) then
            return self.hanjia2023BattlePassButtonInstance
        end
    elseif methodName == "GetNanDuButton" then
        if not CommonDefs.IsNull(self.nandufanhuaButtonInstance) then
            return self.nandufanhuaButtonInstance
        end
    end
end
