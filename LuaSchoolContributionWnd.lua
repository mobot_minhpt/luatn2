
local DefaultTableViewDataSource    = import "L10.UI.DefaultTableViewDataSource"
local QnTableView                   = import "L10.UI.QnTableView"
local UILabel                       = import "UILabel"
local CUITexture                    = import "L10.UI.CUITexture"
local UITexture                     = import "UITexture"
local CTitleMgr                     = import "L10.Game.CTitleMgr"
local CClientMainPlayer             = import "L10.Game.CClientMainPlayer"
local Ease                          = import "DG.Tweening.Ease"
LuaSchoolContributionWnd = class()      --SchoolContributionWnd

--------RegistChildComponent-------
RegistChildComponent(LuaSchoolContributionWnd,         "AdvView",               QnTableView)
RegistChildComponent(LuaSchoolContributionWnd,         "LevelLabel",            UILabel)
RegistChildComponent(LuaSchoolContributionWnd,         "ProgressBar",           UITexture)
RegistChildComponent(LuaSchoolContributionWnd,         "Icon",                  CUITexture)
RegistChildComponent(LuaSchoolContributionWnd,         "TotalScore",            UILabel)
RegistChildComponent(LuaSchoolContributionWnd,         "CurrentScore",          UILabel)
RegistChildComponent(LuaSchoolContributionWnd,         "RuleBtn",               GameObject)
RegistChildComponent(LuaSchoolContributionWnd,         "StoreBtn",              GameObject)

---------RegistClassMember-------
RegistClassMember(LuaSchoolContributionWnd,            "m_AchievementTable")
RegistClassMember(LuaSchoolContributionWnd,            "m_LevelTable")          --  贡献等级表
RegistClassMember(LuaSchoolContributionWnd,            "m_ClassLevelTable")     --  职业等级名称表
RegistClassMember(LuaSchoolContributionWnd,            "m_CurrentLevel")        --  当前等级
RegistClassMember(LuaSchoolContributionWnd,            "m_Progress")            --  当前进度
RegistClassMember(LuaSchoolContributionWnd,            "m_CounterNameTable")    --  计数表
RegistClassMember(LuaSchoolContributionWnd,            "m_ShopId")    --  shopId

function LuaSchoolContributionWnd:Init()
    self:InitData()
    self:RefreshScore()
    UIEventListener.Get(self.RuleBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        g_MessageMgr:ShowMessage("SchoolContribution_Rule")
    end)
    UIEventListener.Get(self.StoreBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        CUIManager.CloseUI(CLuaUIResources.SchoolContributionWnd)
        CLuaNPCShopInfoMgr:OpenMenPaiContributionShop()
    end)
    self:InitAdvView()
end

function LuaSchoolContributionWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncSchoolAchievementStatus", self, "SyncSchoolAchievementStatus")
    g_ScriptEvent:AddListener("SyncSchoolContribution", self, "SyncSchoolContribution")
    g_ScriptEvent:AddListener("SyncSchoolAchievementCounter", self, "SyncSchoolAchievementCounter")
end

function LuaSchoolContributionWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncSchoolAchievementStatus", self, "SyncSchoolAchievementStatus")
    g_ScriptEvent:RemoveListener("SyncSchoolContribution", self, "SyncSchoolContribution")
    g_ScriptEvent:RemoveListener("SyncSchoolAchievementCounter", self, "SyncSchoolAchievementCounter")
end

function LuaSchoolContributionWnd:InitData()
    self.m_AchievementTable = {}
    self.m_LevelTable = {}
    self.m_ClassLevelTable = {}
    self.m_Progress = 0
    self.m_CounterNameTable = {}
    local myID = EnumToInt(CClientMainPlayer.Inst.Class) * 100 + EnumToInt(CClientMainPlayer.Inst.Gender)
    self.Icon:LoadMaterial(Initialization_Init.GetData(myID).SmIcon)
    for i=1,SchoolContribution_Achievement.GetDataCount() do
        local t = SchoolContribution_Achievement.GetData(i)
        t.status = 0
        table.insert( self.m_AchievementTable, t)
    end
    for i=1,SchoolContribution_Stage.GetDataCount() do
        local level = SchoolContribution_Stage.GetData(i).Value
        table.insert( self.m_LevelTable, level)
        local className = SchoolContribution_Stage.GetData(i).Name
        table.insert( self.m_ClassLevelTable, className)
    end
    for i=1,SchoolContribution_AchievementCounter.GetDataCount() do
        local t = SchoolContribution_AchievementCounter.GetData(i)
        table.insert( self.m_CounterNameTable, t)
    end
end

function LuaSchoolContributionWnd:InitAdvView()
    self.AdvView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.m_AchievementTable
    end, function(item, index)
        self:InitItem(item, index+1)
    end)

    for i=1,#self.m_AchievementTable do
        local prop = CLuaSchoolContributionMgr:GetSchoolContributionProp()
        if not prop.FinishedAchievement:GetBit(self.m_AchievementTable[i].ID) then     --  未达成
            self.m_AchievementTable[i].status = 1
        else
            if not prop.AwardedAchievement:GetBit(self.m_AchievementTable[i].ID) then  --  未领取
                self.m_AchievementTable[i].status = 2
            else                                                                            --  已领取
                self.m_AchievementTable[i].status = 0
            end
        end
    end

    table.sort(self.m_AchievementTable, function(a, b) return a.status > b.status end)
    self.AdvView:ReloadData(true,false)
end

function LuaSchoolContributionWnd:InitItem(item,index)
    item.name = index
    local TipsLabel = item.transform:Find("TipsLabel"):GetComponent(typeof(UILabel))
    local TitleLabel = item.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
    local ValueLabel = item.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
    local GetBtn = item.transform:Find("GetBtn").gameObject
    TipsLabel.text = self.m_AchievementTable[index].Tips
    local title = Title_Title.GetData(self.m_AchievementTable[index].TitleReward)
    if title ~= nil then
        TitleLabel.text = title.Name
        TitleLabel.color = CTitleMgr.Inst:GetTitleColor(title.ID)
    else
        TitleLabel.text = ""
    end
    ValueLabel.text = LocalString.GetString("贡献度+")..self.m_AchievementTable[index].ContributionReward

    UIEventListener.Get(GetBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        Gac2Gas.RequestAwardSchoolAchievement(self.m_AchievementTable[index].ID)
    end)

    self:RefreshItem(item,index)
end

function LuaSchoolContributionWnd:RefreshItem(item,index)
    local ProgressLabel = item.transform:Find("ProgressLabel"):GetComponent(typeof(UILabel))
    local GetBtn = item.transform:Find("GetBtn").gameObject
    local IsFinish = item.transform:Find("IsFinish").gameObject

    local prop = CLuaSchoolContributionMgr:GetSchoolContributionProp()
    local key = 0
    local value = 0
    for i=1,#self.m_CounterNameTable do
        if self.m_AchievementTable[index].CounterName == self.m_CounterNameTable[i].CounterName then
            key = self.m_CounterNameTable[i].ID
        end
    end
    if not prop.FinishedAchievement:GetBit(self.m_AchievementTable[index].ID) then     --  未达成
        local tryGetResult
        tryGetResult, value = CommonDefs.DictTryGet(prop.AchievementCounter, typeof(UInt16), key, typeof(UInt64))
        if not tryGetResult then
            value = 0
        end
        ProgressLabel.text = value.."/"..self.m_AchievementTable[index].Value
        self.m_AchievementTable[index].status = 1
    else
        if not prop.AwardedAchievement:GetBit(self.m_AchievementTable[index].ID) then  --  未领取
            self.m_AchievementTable[index].status = 2
        else                                                                            --  已领取
            self.m_AchievementTable[index].status = 0
        end
    end
    ProgressLabel.gameObject:SetActive(self.m_AchievementTable[index].status == 1)
    GetBtn:SetActive(self.m_AchievementTable[index].status == 2)
    IsFinish:SetActive(self.m_AchievementTable[index].status == 0)
end

function LuaSchoolContributionWnd:RefreshScore()
    local prop = CLuaSchoolContributionMgr:GetSchoolContributionProp()
    local level = 1
    self.m_CurrentLevel = self.m_LevelTable[1]
    for i=1,#self.m_LevelTable do
        if prop.HistoryContribution >= self.m_LevelTable[i] then
            if i+1 > #self.m_LevelTable then
                self.m_CurrentLevel = self.m_LevelTable[i]
            else
                self.m_CurrentLevel = self.m_LevelTable[i+1]
            end
            level = i
        end
    end

    self.LevelLabel.text = self.m_ClassLevelTable[level][EnumToInt(CClientMainPlayer.Inst.Class)-1]
    self.TotalScore.text = prop.HistoryContribution.."/"..self.m_CurrentLevel
    self.CurrentScore.text = prop.CurrentContribution
    local progress = prop.HistoryContribution/self.m_CurrentLevel
    if prop.HistoryContribution == 0 then
        progress = 0
    end
	local tweener = LuaTweenUtils.TweenFloat(self.m_Progress, progress, 2, function ( val )
        if self.ProgressBar then
            self.ProgressBar.fillAmount = val
        end
    end)
	LuaTweenUtils.SetEase(tweener, Ease.OutExpo)
    self.m_Progress = progress
end 

function LuaSchoolContributionWnd:SyncSchoolAchievementStatus(achievementId)
    local index = 0
    for i=1,#self.m_AchievementTable do
        if self.m_AchievementTable[i].ID == achievementId then
            index = i
        end
    end
    local item = self.AdvView:GetItemAtRow(index - 1)
    self:RefreshItem(item,index)
end

function LuaSchoolContributionWnd:SyncSchoolContribution()
    self:RefreshScore()
end

function LuaSchoolContributionWnd:SyncSchoolAchievementCounter()
    self.AdvView:Clear()
    for i=1,#self.m_AchievementTable do
        local prop = CLuaSchoolContributionMgr:GetSchoolContributionProp()
        if not prop.FinishedAchievement:GetBit(self.m_AchievementTable[i].ID) then     --  未达成
            self.m_AchievementTable[i].status = 1
        else
            if not prop.AwardedAchievement:GetBit(self.m_AchievementTable[i].ID) then  --  未领取
                self.m_AchievementTable[i].status = 2
            else                                                                            --  已领取
                self.m_AchievementTable[i].status = 0
            end
        end
    end

    table.sort(self.m_AchievementTable, function(a, b) return a.status > b.status end)
    self.AdvView:ReloadData(true,false)
end
