local PlayerSettings=import "L10.Game.PlayerSettings"
local Transform = import "UnityEngine.Transform"
local LayerDefine=import "L10.Engine.LayerDefine"
local Physics=import "UnityEngine.Physics"

local UILabel = import "UILabel"
local SphereCollider=import "UnityEngine.SphereCollider"

local CEffectMgr            = import "L10.Game.CEffectMgr"
local EnumWarnFXType        = import "L10.Game.EnumWarnFXType"
local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Object = import "System.Object"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local Screen=import "UnityEngine.Screen"
local Camera=import "UnityEngine.Camera"

LuaZhuErDanGuanChaXinZangWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhuErDanGuanChaXinZangWnd, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaZhuErDanGuanChaXinZangWnd, "Tip1", "Tip1", GameObject)
RegistChildComponent(LuaZhuErDanGuanChaXinZangWnd, "Tip2", "Tip2", GameObject)
RegistChildComponent(LuaZhuErDanGuanChaXinZangWnd, "Tip3", "Tip3", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaZhuErDanGuanChaXinZangWnd, "m_ModelTexture")
RegistClassMember(LuaZhuErDanGuanChaXinZangWnd, "m_IdentifierStr")
RegistClassMember(LuaZhuErDanGuanChaXinZangWnd, "m_Rotation")
RegistClassMember(LuaZhuErDanGuanChaXinZangWnd, "m_Tips")
RegistClassMember(LuaZhuErDanGuanChaXinZangWnd, "m_TipAudios")
RegistClassMember(LuaZhuErDanGuanChaXinZangWnd, "m_GameOver")
RegistClassMember(LuaZhuErDanGuanChaXinZangWnd, "m_RO")
RegistClassMember(LuaZhuErDanGuanChaXinZangWnd, "m_FxPositions")
RegistClassMember(LuaZhuErDanGuanChaXinZangWnd,"m_SoundEvent")

function LuaZhuErDanGuanChaXinZangWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_IdentifierStr = "_ZhuErDan_XinZang_Preview_"

    self.DescLabel.text = g_MessageMgr:FormatMessage("ZhuErDan_GuanChaXinZang_Tip")

    self.Tip1:SetActive(false)
    self.Tip2:SetActive(false)
    self.Tip3:SetActive(false)

    self.m_Tips = {"Tip1","Tip2","Tip3"}
    for index, value in ipairs(self.m_Tips) do
        self[value]:GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("ZhuErDan_GuanChaXinZang_"..tostring(index))
    end

    self.m_TipAudios = {
        "event:/L10/L10_vocal/zhuerdan/zhuerdan_45",
        "event:/L10/L10_vocal/linju/linju_02",
        "event:/L10/L10_vocal/zhuerdan/zhuerdan_47"
    }
    self.m_FxPositions = {
        ["p1"]={-0.0512,0.0521,-0.0263},
        ["p2"]={-0.0512,0.1113,-0.084},
        ["p3"]={0.0756,0.0973,-0.051}
    }

    self.m_Rotation = 250
    self.m_GameOver=false
end

function LuaZhuErDanGuanChaXinZangWnd:Init()
    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadModel(ro)
    end)
    self.m_ModelTexture = self.transform:Find("Preview"):GetComponent(typeof(CUITexture))
    self.m_ModelTexture.mainTexture = CUIManager.CreateModelTexture(self.m_IdentifierStr, self.m_ModelTextureLoader,self.m_Rotation,0.05,-1,4.66,false,true,1)


    UIEventListener.Get(self.m_ModelTexture.gameObject).onDrag = DelegateFactory.VectorDelegate(function(go, delta)
        self.m_Rotation = self.m_Rotation - delta.x/Screen.width*360
        CUIManager.SetModelRotation(self.m_IdentifierStr, self.m_Rotation)
    end)

    UIEventListener.Get(self.m_ModelTexture.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickModel()
    end)
end

--@region UIEvent

--@endregion UIEvent

function LuaZhuErDanGuanChaXinZangWnd:LoadModel(ro)
    self.m_RO = ro
    ro:LoadMain("Character/NPC/lnpc898/Prefab/lnpc898_01.prefab",nil,false,false)--变成狗
    ro.Scale=8
    ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function (renderObject)
        CEffectMgr.Inst:AddObjectFX(88803067,ro,0,1,1.0,nil,false,EnumWarnFXType.None,Vector3.zero, Vector3.zero, nil)
        local pos = self.m_FxPositions["p1"]
        local fx1 = CEffectMgr.Inst:AddObjectFX(88803069,ro,0,1,1.0,nil,false,EnumWarnFXType.None,Vector3(pos[1],pos[2],pos[3]), Vector3.zero, nil)
        ro:AddFX("p1",fx1)
        local pos = self.m_FxPositions["p2"]
        local fx2 = CEffectMgr.Inst:AddObjectFX(88803069,ro,0,1,1.0,nil,false,EnumWarnFXType.None,Vector3(pos[1],pos[2],pos[3]), Vector3.zero, nil)
        ro:AddFX("p2",fx2)
        local pos = self.m_FxPositions["p3"]
        local fx3 = CEffectMgr.Inst:AddObjectFX(88803069,ro,0,1,1.0,nil,false,EnumWarnFXType.None,Vector3(pos[1],pos[2],pos[3]), Vector3.zero, nil)
        ro:AddFX("p3",fx3)
    end))

    --创建3个观察点
    local function addPoint(name,x,y,z)
        local go = GameObject(name)
        go.transform.parent =ro.transform
        go.transform.localPosition = Vector3(x,y,z)
        go.transform.localScale = Vector3(0.03,0.03,0.03)
        local sc = go:AddComponent(typeof(SphereCollider))
        sc.radius = 0.5
        go.layer = LayerDefine.ModelForNGUI_3D
    end
    local pos = self.m_FxPositions["p1"]
    addPoint("p1",pos[1],pos[2],pos[3])
    local pos = self.m_FxPositions["p2"]
    addPoint("p2",pos[1],pos[2],pos[3])
    local pos = self.m_FxPositions["p3"]
    addPoint("p3",pos[1],pos[2],pos[3])
end

function LuaZhuErDanGuanChaXinZangWnd:OnClickModel()
    if self.m_GameOver then return end

    local worldPos = self.m_ModelTexture.transform.position
    local screenPos = Input.mousePosition
    local clickWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)

    --相对modelTexture的坐标
    local relativePos = self.m_ModelTexture.transform:InverseTransformPoint(clickWorldPos)

    local width,height = self.m_ModelTexture.texture.width,self.m_ModelTexture.texture.height
    local viewPos = Vector3(relativePos.x/width+0.5,relativePos.y/height+0.5,0)

    -- print(relativePos)

    local camera = CUIManager.instance.transform:Find(self.m_IdentifierStr):GetComponent(typeof(Camera))
    -- print(camera,viewPos)
    
    local ray = camera:ViewportPointToRay(viewPos)
    local hits = Physics.RaycastAll(ray, 20, camera.cullingMask)
    -- if not hits then return end
    -- print(hits.Length)
    for i=0,hits.Length-1 do
        local collider = hits[i].collider.gameObject
        local name = collider.gameObject.name
        if name=="p1" or name=="p2" or name=="p3" then
            collider:GetComponent(typeof(SphereCollider)).enabled = false

            self.m_RO:RemoveFX(name)
            local pos = self.m_FxPositions[name]
            local fx = CEffectMgr.Inst:AddObjectFX(88803068,self.m_RO,0,1,1.0,nil,false,EnumWarnFXType.None,Vector3(pos[1],pos[2],pos[3]), Vector3.zero, nil)
            self.m_RO:AddFX(name,fx)

            for i,v in ipairs(self.m_Tips) do
                if not self[v].activeSelf then
                    self[v]:SetActive(true)
                    if PlayerSettings.SoundEnabled then
                        if self.m_SoundEvent then
                            SoundManager.Inst:StopSound(self.m_SoundEvent)
                        end
                        self.m_SoundEvent = SoundManager.Inst:PlayOneShot(self.m_TipAudios[i],Vector3.zero, nil, 0)
                    end            
                    for i2,v2 in ipairs(self.m_Tips) do
                        if self[v2].activeSelf and v2~=v then
                            --其他的置灰
                            CUICommonDef.SetActive(self[v2], false, true)
                        end
                    end
                    --语音
                    break
                end
            end
            break
        end
    end

    local over = true
    for i,v in ipairs(self.m_Tips) do
        if not self[v].activeSelf then
            over = false
            break
        end
    end
    if over then
        self.m_GameOver = true
        self:EndGame()
    end
end


function LuaZhuErDanGuanChaXinZangWnd:EndGame()
    local empty = CreateFromClass(MakeGenericClass(List, Object))
    empty = MsgPackImpl.pack(empty)
    Gac2Gas.FinishClientTaskEvent(22120564,"ZhuErDan_GuanChaXinZang",empty)
	RegisterTickOnce(function ()
		CUIManager.CloseUI(CLuaUIResources.ZhuErDanGuanChaXinZangWnd)
	end, 8000)
end

function LuaZhuErDanGuanChaXinZangWnd:OnDestroy()
    CUIManager.DestroyModelTexture(self.m_IdentifierStr)
    if self.m_SoundEvent then
        SoundManager.Inst:StopSound(self.m_SoundEvent)
        self.m_SoundEvent = nil
    end
end
