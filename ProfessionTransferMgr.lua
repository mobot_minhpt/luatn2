local CUIManager = import "L10.UI.CUIManager"
local LocalString = import "LocalString"
local MsgPackImpl = import "MsgPackImpl"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChargeWnd = import "L10.UI.CChargeWnd"

LuaProfessionTransferMgr = class()

function LuaProfessionTransferMgr.OnPlayerLogin()
	LuaProfessionTransferMgr.Reset()
end

function LuaProfessionTransferMgr.Reset()
	LuaProfessionTransferMgr.FundEndTime = 0
	LuaProfessionTransferMgr.FundTaskExpireTime = 0
	LuaProfessionTransferMgr.FundNowTime = 0
	LuaProfessionTransferMgr.FundPurchased = false
	LuaProfessionTransferMgr.FundAllAwarded = false
	LuaProfessionTransferMgr.FundShowAlert = false
	LuaProfessionTransferMgr.FundProgressInfos = nil
	LuaProfessionTransferMgr.bIsTransfered = false
	LuaProfessionTransferMgr.kaishuData= {}
	
	LuaProfessionTransferMgr.m_waitShareBoxCloseTick = nil
	LuaProfessionTransferMgr.openShareBoxOnce = false
end

LuaProfessionTransferMgr.m_waitShareBoxCloseTick = nil
LuaProfessionTransferMgr.openShareBoxOnce = false

LuaProfessionTransferMgr.JueJiExchangeTitle = ""
LuaProfessionTransferMgr.JueJiExchangeType = 0
LuaProfessionTransferMgr.TargetJueJiLevel = 0

LuaProfessionTransferMgr.SourceSkillBookTemplateId = 0

function LuaProfessionTransferMgr.ShowProfessionTransferAlert()
	--默认false, 投放时打客户端patch改为true, 关闭时打客户端patch改为false
	return false
end

function LuaProfessionTransferMgr.ShowBuyBookActivity()
	--默认false, 投放时打客户端patch改为true, 关闭时打客户端patch改为false
	return false
end

function LuaProfessionTransferMgr.ShowZhuanzhiFundActivity()
	local condition1 = LuaProfessionTransferMgr.ShowZhuanzhiFundActivityCondition1()
	
	--服务器通过SyncClassTransferFundInfo来同步数据, 
	local nowTime = CServerTimeMgr.Inst.timeStamp
	local condition2 = (nowTime <= math.max(LuaProfessionTransferMgr.FundEndTime, LuaProfessionTransferMgr.FundTaskExpireTime))
	return condition1 or condition2
end

function LuaProfessionTransferMgr.ShowZhuanzhiFundActivityCondition1()
	--默认false, 投放时打客户端patch改为true, 关闭时打客户端patch改为false
	return false
end

function LuaProfessionTransferMgr.ShowZhuanzhiReturnActivity()
	--当condition1=false, 且奖励不再能领取时为关闭
	local condition1 = LuaProfessionTransferMgr.ShowZhuanzhiReturnActivityCondition1()
	local condition2 = false
	if CClientMainPlayer.Inst then
		local cardInfo = CClientMainPlayer.Inst.ItemProp.ZhuanZhiReturn
		if cardInfo then
			local leftDay = CChargeWnd.HuahunMonthCardLeftDay(cardInfo)
			condition2 = leftDay > 0
		end
	end
	
	return condition1 or condition2
end

function LuaProfessionTransferMgr.ShowZhuanzhiReturnActivityCondition1()
	--默认false, 投放时打客户端patch改为true, 关闭时打客户端patch改为false
	return false
end

function LuaProfessionTransferMgr.ShowJueJiExchangeWndByTargetJueJiLevel(targetJueJiLevel)
	if targetJueJiLevel == 70 then
		LuaProfessionTransferMgr.JueJiExchangeTitle = LocalString.GetString("兑换70绝技")
		LuaProfessionTransferMgr.JueJiExchangeType = 0 --从NPC处兑换
		LuaProfessionTransferMgr.TargetJueJiLevel = 70
	elseif targetJueJiLevel == 100 then
		LuaProfessionTransferMgr.JueJiExchangeTitle = LocalString.GetString("兑换100绝技")
		LuaProfessionTransferMgr.JueJiExchangeType = 0
		LuaProfessionTransferMgr.TargetJueJiLevel = 100
	else
		return
	end
	LuaProfessionTransferMgr.SourceSkillBookTemplateId = 0
	CUIManager.ShowUI("JueJiExchangeWnd")
end

function LuaProfessionTransferMgr.ShowJueJiExchangeWndByJueJiSkillBook(itemId, templateId)
	if LuaProfessionTransferMgr.IsJueJiSkillBook(templateId) then
		LuaProfessionTransferMgr.JueJiExchangeTitle = LocalString.GetString("兑换绝技")
		LuaProfessionTransferMgr.JueJiExchangeType = 1 --从包裹处兑换
		LuaProfessionTransferMgr.TargetJueJiLevel = 0
		LuaProfessionTransferMgr.SourceSkillBookTemplateId = templateId
		CUIManager.ShowUI("JueJiExchangeWnd")
	end
end

function LuaProfessionTransferMgr.IsJueJiSkillBook(templateId)
	return LuaProfessionTransferMgr.Is70JueJiSkillBook(templateId) or LuaProfessionTransferMgr.Is100JueJiSkillBook(templateId)
end

function LuaProfessionTransferMgr.Is70JueJiSkillBook(templateId)
	if not templateId then return false end
	return CommonDefs.HashSetContains(ProfessionTransfer_Setting.GetData()._70SkillItemId, typeof(uint), templateId)
end

function LuaProfessionTransferMgr.Is100JueJiSkillBook(templateId)
	if not templateId then return false end
	return CommonDefs.HashSetContains(ProfessionTransfer_Setting.GetData()._100SkillItemId, typeof(uint), templateId)
end


--向Gas请求兑换技能书
function LuaProfessionTransferMgr.RequestExchangeNewJueJiItem(exchangeId, templateIdOfForgetJueJi70, templateIdOfForgetJueJi100, templateIdOfJueJi70, templateIdOfJueJi100)
	Gac2Gas.RequestExchangeNewJueJiItem(exchangeId, templateIdOfForgetJueJi70, templateIdOfForgetJueJi100, templateIdOfJueJi70, templateIdOfJueJi100)
end


--@region 转职基金

LuaProfessionTransferMgr.FundEndTime = 0 -- 购买截止时间
LuaProfessionTransferMgr.FundTaskExpireTime = 0 -- 领取截止时间
LuaProfessionTransferMgr.FundNowTime = 0
LuaProfessionTransferMgr.FundPurchased = false -- 是否购买
LuaProfessionTransferMgr.FundAllAwarded = false -- 奖励是否全部领取
LuaProfessionTransferMgr.FundShowAlert = false -- 转职基金是否需要显示小红点
LuaProfessionTransferMgr.FundProgressInfos = nil
LuaProfessionTransferMgr.bIsTransfered = false


function LuaProfessionTransferMgr.SyncClassTransferFundInfo(endTime, taskExpireTime, nowTime, purchased, allAwarded, showAlert, progressInfos, bIsTransfered)
	LuaProfessionTransferMgr.FundEndTime = endTime
	LuaProfessionTransferMgr.FundTaskExpireTime = taskExpireTime
	LuaProfessionTransferMgr.FundNowTime = nowTime
	LuaProfessionTransferMgr.FundPurchased = purchased
	LuaProfessionTransferMgr.FundAllAwarded = allAwarded
	LuaProfessionTransferMgr.FundShowAlert = showAlert
	LuaProfessionTransferMgr.FundProgressInfos = progressInfos
	LuaProfessionTransferMgr.bIsTransfered = bIsTransfered

	g_ScriptEvent:BroadcastInLua("SyncZhuanZhiFundInfo")
end

function LuaProfessionTransferMgr.TransferInDuration4Fund()
	return LuaProfessionTransferMgr.bIsTransfered
end

function LuaProfessionTransferMgr.TransferInDuration4Return()
	-- 获得当前的转职时间, 跟2023/8/10 0:0:0(1691596800)比较
	-- 如果大于这个时间, 则认为活动期间内有转职过
	-- 小心使用这个接口
	local ret = false
	if CClientMainPlayer.Inst then
		local cardInfo = CClientMainPlayer.Inst.ItemProp.ZhuanZhiReturn
		if cardInfo then
			local zhuanzhiTime = cardInfo.ZhuanZhiTime
			local standardCompareTime = 1691596800
			ret = zhuanzhiTime > standardCompareTime
		end
	end
	return ret
end

--@desc 是否有基金奖励可领取
function LuaProfessionTransferMgr.HasFundAwardToTake()
	if not LuaProfessionTransferMgr.FundPurchased or not LuaProfessionTransferMgr.FundProgressInfos then return false end

	local hasAward = false
	for key, value in pairs(LuaProfessionTransferMgr.FundProgressInfos) do
		if value.isFinished and not value.isAwarded then
			hasAward = true
		end
	end
	return hasAward
end

function LuaProfessionTransferMgr.NotifyBuyClassTransferFundDone(taskExpireTime)
	LuaProfessionTransferMgr.FundTaskExpireTime = taskExpireTime
	LuaProfessionTransferMgr.FundPurchased = true
	g_ScriptEvent:BroadcastInLua("SyncZhuanZhiFundInfo")
end

function LuaProfessionTransferMgr.UpdateFundProgressInfo(taskExpireTime, id, progress, finished)
	LuaProfessionTransferMgr.FundTaskExpireTime = taskExpireTime
	LuaProfessionTransferMgr.FundProgressInfos[id] = {
		progress = progress,
		isFinished = finished,
		isAwarded = false,
	}
	g_ScriptEvent:BroadcastInLua("UpdateZhuanZhiFundProgressInfo")
end

function LuaProfessionTransferMgr.GetFundAwardSuccess(taskExpireTime, id, allAwarded)
	LuaProfessionTransferMgr.FundTaskExpireTime = taskExpireTime
	LuaProfessionTransferMgr.FundAllAwarded = allAwarded

	local info = LuaProfessionTransferMgr.FundProgressInfos[id]
	if info then
		info.isAwarded = true
	end
	g_ScriptEvent:BroadcastInLua("GetZhuanZhiFundAwardSuccess")
end

-- @desc 同步转职基金信息
function Gas2Gac.SyncClassTransferFundInfo(endTime, taskExpireTime, nowTime, purchased, allAwarded, finished, awarded, progressListUD, showAlert, bIsTransfered)
	local progressList = MsgPackImpl.unpack(progressListUD)
	if not progressList then return end
	local progressInfo = {}
	local tempInfo = {}

	for i = 0, progressList.Count - 1, 2 do
		local id = progressList[i]
		local progress = progressList[i+1]
		tempInfo[id] = progress

	end

	ProfessionTransfer_FundTask.ForeachKey(function (key)
		local isFinished = bit.band(finished, bit.lshift(1, key - 1)) > 0
		local isAwarded = bit.band(awarded, bit.lshift(1, key - 1)) > 0
		local progress = tempInfo[key] or 0
		progressInfo[key] = {
			progress = progress,
			isFinished = isFinished,
			isAwarded = isAwarded,
		}
	end)
	LuaProfessionTransferMgr.SyncClassTransferFundInfo(endTime, taskExpireTime, nowTime, purchased, allAwarded, showAlert, progressInfo, bIsTransfered)
end

--@desc 转职基金购买成功的回调
function Gas2Gac.NotifyBuyClassTransferFundDone(taskExpireTime)

	LuaProfessionTransferMgr.NotifyBuyClassTransferFundDone(taskExpireTime)
end

--@desc 更新转职基金任务进度
function Gas2Gac.UpdateClassTransferFundTaskProgress(taskExpireTime, id, progress, finished)

	LuaProfessionTransferMgr.UpdateFundProgressInfo(taskExpireTime, id, progress, finished)
end

--@desc 领取转职基金奖励成功
function Gas2Gac.GetClassTransferFundTaskAwardSuccess(taskExpireTime, id, allAwarded)

	LuaProfessionTransferMgr.GetFundAwardSuccess(taskExpireTime, id, allAwarded)
end
--@endregion

LuaProfessionTransferMgr.kaishuData = {}
function LuaProfessionTransferMgr:QuanMinKaiShu_SyncPlayData(playDataU)
	local playData = g_MessagePack.unpack(playDataU)
	LuaProfessionTransferMgr.kaishuData.kaiShuTimes = playData.kaiShuTimes and playData.kaiShuTimes or 0
	LuaProfessionTransferMgr.kaishuData.rewardData = playData.rewardData and playData.rewardData or {}
	LuaProfessionTransferMgr.kaishuData.rongLuScore = playData.rongLuScore and playData.rongLuScore or 0
	g_ScriptEvent:BroadcastInLua("QuanMinKaiShu_SyncPlayData")
end

function LuaProfessionTransferMgr:QuanMinKaiShu_GetTianShuSuccess(count, score)
	if self.m_delayShowRewardTick then
		UnRegisterTick(self.m_delayShowRewardTick)
		self.m_delayShowRewardTick = nil
	end
	
	if (count == 0) then
		g_ScriptEvent:BroadcastInLua("QuanMinKaiShu_PlayAnimation", 2, score)
	else
		g_ScriptEvent:BroadcastInLua("QuanMinKaiShu_PlayAnimation", 1, score)

		self.m_delayShowRewardTick = RegisterTickOnce(function()
			local num = count
			local list = {
				{ItemID = Constants.DunJiaTianShuTemplateId, Count = num}
			}
			local buttonList = {
				{spriteName = "blue", buttonLabel = LocalString.GetString("确定"), clickCB = function() CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd) end},
			}
			LuaCommonGetRewardWnd.m_materialName = "gongxihuode"
			LuaCommonGetRewardWnd.m_Reward1List = list
			LuaCommonGetRewardWnd.m_Reward2Label = nil
			LuaCommonGetRewardWnd.m_Reward2List = {}
			LuaCommonGetRewardWnd.m_hint = ""
			LuaCommonGetRewardWnd.m_button = buttonList
			CUIManager.ShowUI(CLuaUIResources.CommonGetRewardWnd)
		end, 1000)
	end
end 