require("common/common_include")

local Vector3 = import "UnityEngine.Vector3"
local CUIManager = import "L10.UI.CUIManager"
local Screen = import "UnityEngine.Screen"
local Random = import "UnityEngine.Random"
local Time = import "UnityEngine.Time"
local UITexture = import "UITexture"

LuaFallingDownWnd = class()

RegistClassMember(LuaFallingDownWnd, "ToyTemplate")

RegistClassMember(LuaFallingDownWnd, "EmitIntervalTime") -- 发射间隔时间
RegistClassMember(LuaFallingDownWnd, "EmitTime") -- 总共发射的时间

RegistClassMember(LuaFallingDownWnd, "TotalTime")
RegistClassMember(LuaFallingDownWnd, "Time")

RegistClassMember(LuaFallingDownWnd, "Tick")

function LuaFallingDownWnd:Init()
	self:InitClassMembers()
	self:InitValues()

end

function LuaFallingDownWnd:InitClassMembers()

	self.ToyTemplate = self.transform:Find("ToyTemplate").gameObject
	self.ToyTemplate:SetActive(false)
end

function LuaFallingDownWnd:InitValues()
	self.EmitIntervalTime = 0.07
	self.EmitTime = 5

	self.TotalTime = 0
	self.Time = 0

	local uitexture = self.ToyTemplate:GetComponent(typeof(UITexture))
	LuaZhuJueJuQingMgr.m_BottomLines = {}

	local counter = math.ceil( 1920 / uitexture.width)
	for i = 1, counter do
		table.insert(LuaZhuJueJuQingMgr.m_BottomLines, - (1080 / 2))
	end

	self.Tick = RegisterTickOnce(function ()
		CUIManager.CloseUI(CLuaUIResources.FallingDownWnd)
	end, 15000)

end

-- 发射玩具
function LuaFallingDownWnd:EmitToy()
	local pos = Vector3.zero
	pos = CUIManager.UIMainCamera:ScreenToWorldPoint(Vector3(Random.Range(0, Screen.width), Screen.height, 0))
	local toy = GameObject.Instantiate(self.ToyTemplate, pos, self.gameObject.transform.rotation)
	if toy then
		toy.transform.parent = self.transform
		toy.transform.localScale = Vector3.one
		toy:SetActive(true)
	end
end

function LuaFallingDownWnd:OnEnable()
	
end

function LuaFallingDownWnd:OnDisable()
	if self.Tick ~= nil then
      UnRegisterTick(self.Tick)
      self.Tick=nil
    end
end

function LuaFallingDownWnd:Update()

	if self.TotalTime > self.EmitTime then return end
	self.TotalTime = self.TotalTime + Time.deltaTime
	self.Time = self.Time + Time.deltaTime
	if self.Time > self.EmitIntervalTime then
		self.Time = 0
		self:EmitToy()
	end
end


return LuaFallingDownWnd
