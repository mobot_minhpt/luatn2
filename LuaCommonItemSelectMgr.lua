LuaCommonItemSelectMgr={}
LuaCommonItemSelectMgr.Title = ""
LuaCommonItemSelectMgr.Menu =""
LuaCommonItemSelectMgr.OnInitFunc = nil
LuaCommonItemSelectMgr.OnSelectFunc = nil

function LuaCommonItemSelectMgr.ShowSelectWnd(title,menu,onInitFunc,onSelectFunc)
    LuaCommonItemSelectMgr.Title = title
    LuaCommonItemSelectMgr.Menu = menu
    LuaCommonItemSelectMgr.OnInitFunc = onInitFunc
    LuaCommonItemSelectMgr.OnSelectFunc = onSelectFunc
    CUIManager.ShowUI(CLuaUIResources.CommonItemSelectWnd)
end

function LuaCommonItemSelectMgr.CloseSelectWnd()
    CUIManager.CloseUI(CLuaUIResources.CommonItemSelectWnd)
end