local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUIFx = import "L10.UI.CUIFx"

LuaBusinessDailyEndView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaBusinessDailyEndView, "Fx", "Fx", CUIFx)

--@endregion RegistChildComponent end
RegistClassMember(LuaBusinessDailyEndView, "m_Tick")
RegistClassMember(LuaBusinessDailyEndView, "m_Bg")
RegistClassMember(LuaBusinessDailyEndView, "m_Tweener")
RegistClassMember(LuaBusinessDailyEndView, "m_Tweener2")
RegistClassMember(LuaBusinessDailyEndView, "m_LastDayLab")
RegistClassMember(LuaBusinessDailyEndView, "m_NewDayLab")

function LuaBusinessDailyEndView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_Bg           = self.transform:Find("Bg"):GetComponent(typeof(UITexture))
    self.m_LastDayLab   = self.transform:Find("LastDayLab"):GetComponent(typeof(UILabel))
    self.m_NewDayLab    = self.transform:Find("NewDayLab"):GetComponent(typeof(UILabel))
end

function LuaBusinessDailyEndView:Start()
end

--@region UIEvent

--@endregion UIEvent

function LuaBusinessDailyEndView:OnEnable()
    self.m_LastDayLab.text = LuaBusinessMgr.m_CurDay
    self.m_NewDayLab.text = LuaBusinessMgr.m_CurDay + 1

    local isStoreMode = LuaBusinessMgr.m_IsStoryMode

    LuaTweenUtils.Kill(self.m_Tweener2, false)
    if isStoreMode then
        self.m_LastDayLab.alpha = 1
        self.m_NewDayLab.alpha = 0

        self.m_Tweener2 = LuaTweenUtils.TweenFloat(0, 1, 2.6, function ( val )
            self.m_LastDayLab.alpha = 1 - val * 2
            self.m_NewDayLab.alpha = val * 2 - 1
        end)
        CommonDefs.OnComplete_Tweener(self.m_Tweener2, DelegateFactory.TweenCallback(function()
            LuaTweenUtils.TweenAlpha(self.m_NewDayLab, 1, 0, 1)
        end))
    else
        self.m_LastDayLab.alpha = 0
        self.m_NewDayLab.alpha = 1
    end

    local fxPath = isStoreMode and "fx/ui/prefab/UI_zhouye_transform.prefab" or "fx/ui/prefab/UI_zhouye_rise.prefab"
    local duration = isStoreMode and 2.6 or 0.6
    self.Fx:DestroyFx()
    self.Fx:LoadFx(fxPath)
    self.m_Bg.alpha = 1

    UnRegisterTick(self.m_Tick)
    self.m_Tick = RegisterTickOnce(function()
        self:ToEndDay()
        if not isStoreMode then
            LuaTweenUtils.TweenAlpha(self.m_NewDayLab, 1, 0, 1)
        end
    end, duration * 1000)
end

function LuaBusinessDailyEndView:ToEndDay()
    LuaTweenUtils.Kill(self.m_Tweener, false)
    self.m_Tweener = LuaTweenUtils.TweenAlpha(self.m_Bg, 1, 0, 1)
    CommonDefs.OnComplete_Tweener(self.m_Tweener, DelegateFactory.TweenCallback(function()
        self.gameObject:SetActive(false)
    end))
end

function LuaBusinessDailyEndView:OnDestroy()
    UnRegisterTick(self.m_Tick)
    LuaTweenUtils.Kill(self.m_Tweener, false)
    LuaTweenUtils.Kill(self.m_Tweener2, false)
end