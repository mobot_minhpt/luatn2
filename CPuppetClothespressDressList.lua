-- Auto Generated!!
local Byte = import "System.Byte"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClothespressEmptyTemplate = import "L10.UI.CClothespressEmptyTemplate"
local CClothespressFashionTemplate = import "L10.UI.CClothespressFashionTemplate"
local CFashionMgr = import "L10.Game.CFashionMgr"
local CFashionVehicleRenewalMgr = import "L10.UI.CFashionVehicleRenewalMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPuppetClothesMgr = import "L10.UI.CPuppetClothesMgr"
local CPuppetClothespressDressList = import "L10.UI.CPuppetClothespressDressList"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Fashion_Fashion = import "L10.Game.Fashion_Fashion"
local Fashion_Setting = import "L10.Game.Fashion_Setting"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local QnButton = import "L10.UI.QnButton"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local RenewalType = import "L10.UI.RenewalType"
local String = import "System.String"
local System = import "System"
local UIEventListener = import "UIEventListener"
local UInt64 = import "System.UInt64"
local UIPanel = import "UIPanel"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CPuppetClothespressDressList.m_Init_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.qnSelectButtonList)
    if CClientMainPlayer.Inst == nil or CPuppetClothesMgr.selectIdx == 0 then
        return
    end

    if CommonDefs.DictContains(CClientHouseMgr.Inst.mCurFurnitureProp.PuppetInfo, typeof(Byte), CPuppetClothesMgr.selectIdx) then
        this.showFashionSet = CommonDefs.DictGetValue(CClientHouseMgr.Inst.mCurFurnitureProp.PuppetInfo, typeof(Byte), CPuppetClothesMgr.selectIdx).FashionInfo
    else
        return
    end

    this.numLimitLabel.text = System.String.Format(LocalString.GetString("衣橱({0}/{1})"), this.showFashionSet.Fashions.Count, this.showFashionSet.MaxFashionNum)
    this:OnFashionChange(0)
    Extensions.RemoveAllChildren(this.table.transform)
    this.fashionTemplate:SetActive(false)
    this.addNumLimitButton:SetActive(false)
    if CClientMainPlayer.Inst == nil then
        return
    end
    this:InitFashion()
    if CFashionMgr.openFashionExpand then
        this:InitEmptyFashion()
    end
    this.table:Reposition()
    if CFashionMgr.Inst.SelectedFashionId > 0 then
        this:SelectFashion(CFashionMgr.Inst.SelectedFashionId)
    else
        this.scrollView:ResetPosition()
    end
end
CPuppetClothespressDressList.m_InitFashion_CS2LuaHook = function (this) 
    local fashionList = this.showFashionSet.Fashions
    CommonDefs.DictIterate(fashionList, DelegateFactory.Action_object_object(function (___key, ___value) 
        local info = {}
        info.Key = ___key
        info.Value = ___value
        local instance = NGUITools.AddChild(this.table.gameObject, this.fashionTemplate)
        instance:SetActive(true)
        local fashion = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CClothespressFashionTemplate))
        fashion:Init(info.Key, true)
        local button = CommonDefs.GetComponent_GameObject_Type(instance, typeof(QnSelectableButton))
        CommonDefs.ListAdd(this.qnSelectButtonList, typeof(QnSelectableButton), button)
        button:SetSelected(false, false)
        button.OnClick = CommonDefs.CombineListner_Action_QnButton(button.OnClick, MakeDelegateFromCSFunction(this.OnSelectButtonClick, MakeGenericClass(Action1, QnButton), this), true)
        fashion.onQnButtonClick = CommonDefs.CombineListner_Action_QnButton(fashion.onQnButtonClick, MakeDelegateFromCSFunction(this.OnSelectButtonClick, MakeGenericClass(Action1, QnButton), this), true)
    end))
end
CPuppetClothespressDressList.m_InitEmptyFashion_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.showFashionSet.MaxFashionNum - this.showFashionSet.Fashions.Count do
            local instance = NGUITools.AddChild(this.table.gameObject, this.addNumLimitButton)
            instance:SetActive(true)
            local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CClothespressEmptyTemplate))
            if template ~= nil then
                template:Init(false, false, true)
            end
            i = i + 1
        end
    end
    local max = System.UInt32.Parse(Fashion_Setting.GetData("Final_Fashion_Max").Value)
    if this.showFashionSet.NumLimit < max then
        local instance = NGUITools.AddChild(this.table.gameObject, this.addNumLimitButton)
        instance:SetActive(true)
        local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CClothespressEmptyTemplate))
        if template ~= nil then
            template:Init(true, false, true)
        end
    end
end
CPuppetClothespressDressList.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListener(EnumEventType.OnAddFashion, MakeDelegateFromCSFunction(this.Init, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.OnTakeOnOffFashion, MakeDelegateFromCSFunction(this.OnFashionChange, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListener(EnumEventType.OnSyncMainPlayerAppearanceProp, MakeDelegateFromCSFunction(this.OnSyncAppearance, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.OnFreightSubmitEquipSelected, MakeDelegateFromCSFunction(this.OnAddFashionByItem, MakeGenericClass(Action2, String, String), this))
    EventManager.AddListener(EnumEventType.OnFashionOrVehicleLimitChanged, MakeDelegateFromCSFunction(this.OnFashionLimitChanged, Action0, this))
    EventManager.AddListener(EnumEventType.ChangeFashionStatus, MakeDelegateFromCSFunction(this.Init, Action0, this))
    UIEventListener.Get(this.onekeyRenewalBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.onekeyRenewalBtn).onClick, MakeDelegateFromCSFunction(this.OnRenewalBtnClick, VoidDelegate, this), true)
end
CPuppetClothespressDressList.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListener(EnumEventType.OnAddFashion, MakeDelegateFromCSFunction(this.Init, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.OnTakeOnOffFashion, MakeDelegateFromCSFunction(this.OnFashionChange, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListener(EnumEventType.OnSyncMainPlayerAppearanceProp, MakeDelegateFromCSFunction(this.OnSyncAppearance, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.OnFreightSubmitEquipSelected, MakeDelegateFromCSFunction(this.OnAddFashionByItem, MakeGenericClass(Action2, String, String), this))
    EventManager.RemoveListener(EnumEventType.OnFashionOrVehicleLimitChanged, MakeDelegateFromCSFunction(this.OnFashionLimitChanged, Action0, this))
    EventManager.RemoveListener(EnumEventType.ChangeFashionStatus, MakeDelegateFromCSFunction(this.Init, Action0, this))
    UIEventListener.Get(this.onekeyRenewalBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.onekeyRenewalBtn).onClick, MakeDelegateFromCSFunction(this.OnRenewalBtnClick, VoidDelegate, this), false)
end
CPuppetClothespressDressList.m_OnSelectButtonClick_CS2LuaHook = function (this, button) 
    do
        local i = 0
        while i < this.qnSelectButtonList.Count do
            this.qnSelectButtonList[i]:SetSelected(button == this.qnSelectButtonList[i], false)
            i = i + 1
        end
    end
end
CPuppetClothespressDressList.m_SelectFashion_CS2LuaHook = function (this, fashionId) 
    local index = 0
    do
        local i = 0
        while i < this.qnSelectButtonList.Count do
            if fashionId == CommonDefs.GetComponent_Component_Type(this.qnSelectButtonList[i], typeof(CClothespressFashionTemplate)).fashionID then
                this.qnSelectButtonList[i]:SetSelected(true, false)
                index = i
            else
                this.qnSelectButtonList[i]:SetSelected(false, false)
            end
            i = i + 1
        end
    end
    local offset = math.min(index * (this.fashionSprite.height + this.table.padding.y) / (this.table.transform.childCount * (this.fashionSprite.height + this.table.padding.y) - CommonDefs.GetComponent_Component_Type(this.scrollView, typeof(UIPanel)).height), 1)
    --scrollView.SetDragAmount(0, offset, false);
    this:StartCoroutine(this:SetDragAmount(offset))
    CFashionMgr.Inst:ResetSelectedFashionId()
end
CPuppetClothespressDressList.m_OnRenewalBtnClick_CS2LuaHook = function (this, go) 

    CFashionVehicleRenewalMgr.type = RenewalType.PUPPET_FASHION
    CommonDefs.ListClear(CFashionVehicleRenewalMgr.renewalList)
    CommonDefs.DictIterate(this.showFashionSet.Fashions, DelegateFactory.Action_object_object(function (___key, ___value) 
        local fashion = {}
        fashion.Key = ___key
        fashion.Value = ___value
        local info = Fashion_Fashion.GetData(math.floor(tonumber(math.floor(fashion.Key / 10000) or 0)))
        if info ~= nil and fashion.Value.ExpireTime < CServerTimeMgr.Inst.timeStamp and info.CanRenewal == 1 then
            CommonDefs.ListAdd(CFashionVehicleRenewalMgr.renewalList, typeof(UInt64), fashion.Key)
        end
    end))
    if CFashionVehicleRenewalMgr.renewalList.Count > 0 then
        CUIManager.ShowUI(CUIResources.FashionRenewalWnd)
    else
        g_MessageMgr:ShowMessage("NO_FASHION_NEED_RENEW")
    end
end
CPuppetClothespressDressList.m_OnFashionChange_CS2LuaHook = function (this, id) 
    local headId = (math.floor(this.showFashionSet.HeadFashionId / 10000))
    local bodyId = (math.floor(this.showFashionSet.BodyFashionId / 10000))
    local weaponId = (math.floor(this.showFashionSet.WeaponFashionId / 10000))
    local beishiId = (math.floor(this.showFashionSet.BackPendantFashionId / 10000))

    local head = Fashion_Fashion.GetData(headId)
    local body = Fashion_Fashion.GetData(bodyId)
    local beishi = Fashion_Fashion.GetData(beishiId)

    local headTaben = EquipmentTemplate_Equip.GetData(headId)
    local bodyTaben = EquipmentTemplate_Equip.GetData(bodyId)
    local weaponTaben = EquipmentTemplate_Equip.GetData(weaponId)
    local beishiTaben = EquipmentTemplate_Equip.GetData(beishiId)

    if head ~= nil then
        this.headIconTexture:LoadMaterial(head.Icon)
    elseif headTaben ~= nil then
        this.headIconTexture:LoadMaterial(headTaben.Icon)
    else
        this.headIconTexture.material = nil
    end
    if body ~= nil then
        this.bodyIconTexture:LoadMaterial(body.Icon)
    elseif bodyTaben ~= nil then
        this.bodyIconTexture:LoadMaterial(bodyTaben.Icon)
    else
        this.bodyIconTexture.material = nil
    end
    if weaponTaben ~= nil then
        this.weaponIconTexture:LoadMaterial(weaponTaben.Icon)
    else
        this.weaponIconTexture.material = nil
    end

    if beishi ~= nil then
        this.beishiIconTexture:LoadMaterial(beishi.Icon)
    elseif beishiTaben ~= nil then
        this.beishiIconTexture:LoadMaterial(beishiTaben.Icon)
    else
        this.beishiIconTexture.material = nil
    end
end
