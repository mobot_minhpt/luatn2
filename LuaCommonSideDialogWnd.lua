local QnTableView = import "L10.UI.QnTableView"
local DelegateFactory  = import "DelegateFactory"
local CChatLinkMgr = import "CChatLinkMgr"
local CButton = import "L10.UI.CButton"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UILabelOverflow = import "UILabel+Overflow"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local QnCheckBox = import "L10.UI.QnCheckBox"

LuaCommonSideDialogWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCommonSideDialogWnd, "QnTabView", "QnTabView", QnTableView)
RegistChildComponent(LuaCommonSideDialogWnd, "ClearBtn", "ClearBtn", CButton)
RegistChildComponent(LuaCommonSideDialogWnd, "CloseBtn", "CloseBtn", CButton)
RegistChildComponent(LuaCommonSideDialogWnd, "ListView", "ListView", GameObject)
RegistChildComponent(LuaCommonSideDialogWnd, "SingleView", "SingleView", GameObject)
RegistChildComponent(LuaCommonSideDialogWnd, "HideListViewButton", "HideListViewButton", CButton)
RegistChildComponent(LuaCommonSideDialogWnd, "TipsBtn", "TipsBtn", CButton)
RegistChildComponent(LuaCommonSideDialogWnd, "TipsNumLabel", "TipsNumLabel", UILabel)
RegistChildComponent(LuaCommonSideDialogWnd, "ItemTemplatePos", "ItemTemplatePos", GameObject)
RegistChildComponent(LuaCommonSideDialogWnd, "TopLabel", "TopLabel", UILabel)
RegistChildComponent(LuaCommonSideDialogWnd, "BlankWidget", "BlankWidget", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaCommonSideDialogWnd,"m_ActionTable")
RegistClassMember(LuaCommonSideDialogWnd,"m_EvtTypeList")
RegistClassMember(LuaCommonSideDialogWnd,"m_RepositionTick")
RegistClassMember(LuaCommonSideDialogWnd,"m_InitialTopAnchorAbsolute")

function LuaCommonSideDialogWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ClearBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnClearBtnClick()
	end)


	
	UIEventListener.Get(self.CloseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseBtnClick()
	end)


	
	UIEventListener.Get(self.HideListViewButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnHideListViewButtonClick()
	end)


	
	UIEventListener.Get(self.TipsBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipsBtnClick()
	end)


    --@endregion EventBind end
	UIEventListener.Get(self.BlankWidget.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBlankWidgetClick()
	end)
end

function LuaCommonSideDialogWnd:Init()
	self.m_ActionTable = {}
	self.m_EvtTypeList = {}
	self.QnTabView.m_DataSource = DefaultTableViewDataSource.Create2(
        function() return #LuaCommonSideDialogMgr.m_DialogList end,
        function(view,row)
			local data = LuaCommonSideDialogMgr.m_DialogList[row + 1]
			local templateIndex = self:GetTemplateIndex(data)
            local item = view:GetFromPool(templateIndex)
			self:InitTemplate(templateIndex, item, row)
            return item
        end
    )
	self.ListView.gameObject:SetActive(false)
	self.SingleView.gameObject:SetActive(true)
	self:OnUpdateCommonSideDialogList()

	self:CancelRepositionTick()
	self.m_RepositionTick = RegisterTick(function()
		if self.QnTabView then
			self.QnTabView.m_Table:Reposition()
		end
	end,100)
end

function LuaCommonSideDialogWnd:GetTemplateIndex(data)
	local templateIndex = 0
	if data.isNotify then
		templateIndex = 2
		if data.notifyInfo.bgSpriteName == "commonsidedialog_diban_05" then
			templateIndex = 3
		end
	else
		if data.checkBoxInfo then
			templateIndex = 1
		end
	end
	return templateIndex
end

function LuaCommonSideDialogWnd:InitTemplate(templateIndex, item, row, isSingleView)
	if templateIndex == 0 then
		self:InitTemplate0(item, row, isSingleView)
	elseif templateIndex == 1 then
		self:InitTemplate1(item, row, isSingleView)
	elseif templateIndex == 2 or templateIndex == 3 then
		self:InitTemplate2(item, row, isSingleView)
	end
end

function LuaCommonSideDialogWnd:InitTemplate0(item, row, isSingleView)
	local infoLabel = item.transform:Find("InfoLabel"):GetComponent(typeof(UILabel))
	local blueButtonTemplate = item.transform:Find("BlueButtonTemplate"):GetComponent(typeof(CButton))
	local yellowButtonTemplate = item.transform:Find("YellowButtonTemplate"):GetComponent(typeof(CButton))
	local deleteBtn = item.transform:Find("DeleteBtn"):GetComponent(typeof(CButton))
	local btn1Pos = item.transform:Find("Btn1Pos")
	local btn2Pos = item.transform:Find("Btn2Pos")
	local background = item.transform:Find("Background"):GetComponent(typeof(UIWidget))
	blueButtonTemplate.gameObject:SetActive(false)
	yellowButtonTemplate.gameObject:SetActive(false)

	local data = LuaCommonSideDialogMgr.m_DialogList[row + 1]
	deleteBtn.gameObject:SetActive(not data.hideCloseBtn)
	local templateIndex = self:GetTemplateIndex(data)
	local template = self.QnTabView.m_Pool:GetTemplateById(templateIndex)
	local bgInitialHeight = template.transform:Find("Background"):GetComponent(typeof(UIWidget)).height
	local labelInitialHeight = template.transform:Find("InfoLabel"):GetComponent(typeof(UIWidget)).height

	infoLabel.overflowMethod = UILabelOverflow.ResizeHeight
	infoLabel.text = CUICommonDef.TranslateToNGUIText(data.msg)
	infoLabel:ProcessText()
	infoLabel:ResizeCollider()
	background.height = bgInitialHeight - labelInitialHeight + infoLabel.height
	background:ResizeCollider()
	self.QnTabView.m_Table:Reposition()
	Extensions.RemoveAllChildren(btn1Pos.transform)
	Extensions.RemoveAllChildren(btn2Pos.transform)
	local btn1Info = data.btn1Info 
	local btn2Info = data.btn2Info 
	self:InitBtn(btn1Pos, btn1Info, data, yellowButtonTemplate, blueButtonTemplate)
	self:InitBtn(btn2Pos, btn2Info, data, yellowButtonTemplate, blueButtonTemplate)
	UIEventListener.Get(deleteBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		LuaCommonSideDialogMgr:DeleteDialog(data)
	end)
	UIEventListener.Get(infoLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		local index = infoLabel:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
		if index == 0 then
			index = - 1
		end
		local url = infoLabel:GetUrlAtCharacterIndex(index)
		if url then
			CChatLinkMgr.ProcessLinkClick(url,nil)
		end
	end)
	if not self.m_InitialTopAnchorAbsolute then
		self.m_InitialTopAnchorAbsolute = template.transform:Find("InfoLabel/Container"):GetComponent(typeof(UIWidget)).topAnchor.absolute
	end
	local container = item.transform:Find("InfoLabel/Container"):GetComponent(typeof(UIWidget))
	container.topAnchor.absolute = row == 0 and 0 or self.m_InitialTopAnchorAbsolute
	container:ResizeCollider()
end

function LuaCommonSideDialogWnd:InitTemplate1(item, row, isSingleView)
	local data = LuaCommonSideDialogMgr.m_DialogList[row + 1]

	self:InitTemplate0(item, row, isSingleView)

	local checkBox = item.transform:Find("Checkbox"):GetComponent(typeof(QnCheckBox))
	local checkBoxArea = item.transform:Find("Checkbox/CheckboxArea")

	UIEventListener.Get(checkBoxArea.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		data.checkBoxInfo.onClickAction()
	end)
	checkBox.Selected = data.checkBoxInfo.isSelectAction()
	local evtType = data.checkBoxInfo.evtType
	if self.m_EvtTypeList[evtType] then
		table.insert(self.m_EvtTypeList[evtType],function () 
			if checkBox and data then
				checkBox.Selected = data.checkBoxInfo.isSelectAction()
			end
		end)
	end
end

function LuaCommonSideDialogWnd:InitTemplate2(item, row, isSingleView)
	local infoLabel = item.transform:Find("InfoLabel"):GetComponent(typeof(UILabel))
	local labaIcon = item.transform:Find("ChatBubble/Icon"):GetComponent(typeof(UISprite))
	local labaIcon01 = item.transform:Find("ChatBubble/Icon01"):GetComponent(typeof(UISprite))
	local labaIcon02 = item.transform:Find("ChatBubble/Icon02"):GetComponent(typeof(UISprite))
	local labaIcon03 = item.transform:Find("ChatBubble/Icon03"):GetComponent(typeof(UISprite))
	local bg = item.transform:Find("ChatBubble"):GetComponent(typeof(CUITexture))
	local deleteBtn = item.transform:Find("DeleteBtn"):GetComponent(typeof(CButton))
	local blueButtonTemplate = item.transform:Find("BlueButtonTemplate"):GetComponent(typeof(CButton))
	local yellowButtonTemplate = item.transform:Find("YellowButtonTemplate"):GetComponent(typeof(CButton))
	local btnPos = item.transform:Find("BtnPos")
	local btnPos2 = item.transform:Find("BtnPos2")
	blueButtonTemplate.gameObject:SetActive(false)
	yellowButtonTemplate.gameObject:SetActive(false)
	deleteBtn.gameObject:SetActive(false)

	local data = LuaCommonSideDialogMgr.m_DialogList[row + 1]
	local templateIndex = self:GetTemplateIndex(data)
	local template = self.QnTabView.m_Pool:GetTemplateById(templateIndex)
	local bgInitialHeight = template.transform:Find("ChatBubble"):GetComponent(typeof(UIWidget)).height
	local labelInitialHeight = template.transform:Find("InfoLabel"):GetComponent(typeof(UIWidget)).height
	
	Extensions.RemoveAllChildren(btnPos.transform)
	Extensions.RemoveAllChildren(btnPos2.transform)
	self:InitBtn(btnPos, data.btn1Info, data, yellowButtonTemplate, blueButtonTemplate)
	self:InitBtn(btnPos2, data.btn2Info, data, yellowButtonTemplate, blueButtonTemplate)
	UIEventListener.Get(deleteBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		LuaCommonSideDialogMgr:DeleteDialog(data)
	end)
	infoLabel.overflowMethod = UILabelOverflow.ResizeHeight
	local msg = g_MessageMgr:FormatMessage(data.notifyInfo.messageName, data.notifyInfo.notifySenderId, data.notifyInfo.notifySenderName)
	infoLabel.text = System.String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor24(infoLabel.color), CChatLinkMgr.TranslateToNGUIText(msg, false))
	infoLabel:ProcessText()
	infoLabel:ResizeCollider()
	bg:GetComponent(typeof(UITexture)).height = bgInitialHeight - labelInitialHeight + infoLabel.height
	bg:GetComponent(typeof(UITexture)):ResizeCollider()
	bg:LoadMaterial("UI/Texture/Transparent/Material/" .. data.notifyInfo.bgSpriteName .. ".mat")
	labaIcon.spriteName = data.notifyInfo.labaIconSpriteName
	labaIcon01.spriteName = data.notifyInfo.cornerMark1SpriteName
	labaIcon02.spriteName = data.notifyInfo.cornerMark2SpriteName
	labaIcon03.spriteName = data.notifyInfo.cornerMark3SpriteName
	UIEventListener.Get(infoLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		local index = infoLabel:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
		if index == 0 then
			index = - 1
		end
		local url = infoLabel:GetUrlAtCharacterIndex(index)
		if url then
			CChatLinkMgr.ProcessLinkClick(url,nil)
		end
	end)
end

function LuaCommonSideDialogWnd:InitBtn(btnPos, btnInfo, data, yellowButtonTemplate, blueButtonTemplate)
	if btnInfo then
		local btnTemplate = btnInfo.isYellowBtn and yellowButtonTemplate or blueButtonTemplate
		local btn = NGUITools.AddChild(btnPos.gameObject, btnTemplate.gameObject)
		btn.gameObject:SetActive(true)
		local cbtn = btn:GetComponent(typeof(CButton))
		cbtn.Text = btnInfo.btnName
		UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			LuaCommonSideDialogMgr:OnBtnClicked(btnInfo, data)
		end)
		if btnInfo.btnDuration and btnInfo.btnDuration > 0 and btnInfo.btnDefaultSelected then
			local seconds = data.showDialogTime + btnInfo.btnDuration - CServerTimeMgr.Inst.timeStamp
			cbtn.Text = SafeStringFormat3(LocalString.GetString("%s(%d)"), btnInfo.btnName, seconds)
			local tweener = LuaTweenUtils.TweenInt(seconds, 0, seconds, function (val)
				cbtn.Text = SafeStringFormat3(LocalString.GetString("%s(%d)"),btnInfo.btnName,val)
			end)
			LuaTweenUtils.SetTarget(tweener,btn.transform)
			LuaTweenUtils.OnComplete(tweener,function ()
				LuaCommonSideDialogMgr:OnBtnClicked(btnInfo, data)
			end)
		end
		return btn
	end
end

function LuaCommonSideDialogWnd:OnEnable()
	g_ScriptEvent:AddListener("OnUpdateCommonSideDialogList", self, "OnUpdateCommonSideDialogList")
	LuaCommonSideDialogMgr:CancelDeleteCountDownTick()
end

function LuaCommonSideDialogWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnUpdateCommonSideDialogList", self, "OnUpdateCommonSideDialogList")
	for evtType, actionList in pairs(self.m_EvtTypeList) do
		g_ScriptEvent:RemoveListener(evtType, self.m_ActionTable, evtType)
	end
	self:CancelRepositionTick()
	LuaCommonSideDialogMgr:RegisterDeleteCountDownTick()
end

function LuaCommonSideDialogWnd:OnUpdateCommonSideDialogList()
	self:ResetCheckBoxActionTable()
	self:InitSingleView()
	self:InitListView()
	local num = #LuaCommonSideDialogMgr.m_DialogList 
	self.TipsNumLabel.text = SafeStringFormat3(LocalString.GetString("共[ffc800]%d条消息[FFFFFF]待确认"),num)
	--self.TipsNumLabel.gameObject:SetActive(num > 1)
	self.TopLabel.text = SafeStringFormat3(LocalString.GetString("你有%d条提醒待处理"),num)
	if num == 0 then
		CUIManager.CloseUI(CLuaUIResources.CommonSideDialogWnd)
	end
end

function LuaCommonSideDialogWnd:ResetCheckBoxActionTable()
	for evtType, actionList in pairs(self.m_EvtTypeList) do
		g_ScriptEvent:RemoveListener(evtType, self.m_ActionTable, evtType)
	end
	self.m_EvtTypeList = {}
	for i, data in pairs(LuaCommonSideDialogMgr.m_DialogList) do
		if data.checkBoxInfo then
			local evtType = data.checkBoxInfo.evtType
			self.m_EvtTypeList[evtType] = {}
			self.m_ActionTable[evtType] = function () 
				local actionList = self.m_EvtTypeList[evtType]
				if #actionList > 0 then
					for i = 1, #actionList do 
						actionList[i]()
					end
				end
			end
			g_ScriptEvent:AddListener(evtType,self.m_ActionTable,evtType)
		end
	end
end

function LuaCommonSideDialogWnd:InitSingleView()
	local topDialogData = LuaCommonSideDialogMgr:GetTopDialogData()
	Extensions.RemoveAllChildren(self.ItemTemplatePos.transform)
	if topDialogData == nil and #LuaCommonSideDialogMgr.m_DialogList <= 1 then
		if self.SingleView.gameObject.activeSelf then
			CUIManager.CloseUI(CLuaUIResources.CommonSideDialogWnd)
		end
		return
	end
	if topDialogData then
		local templateIndex = self:GetTemplateIndex(topDialogData)
		local item = self.QnTabView.m_Pool:GetTemplateById(templateIndex)
		local go = NGUITools.AddChild(self.ItemTemplatePos.gameObject,item.gameObject)
		go:SetActive(true)
		go.transform.localPosition = Vector3.zero
		self:InitTemplate(templateIndex, go, 0, true)
	end
	self.TipsNumLabel.gameObject:SetActive(#LuaCommonSideDialogMgr.m_DialogList > 1)
end

function LuaCommonSideDialogWnd:InitListView()
	self.QnTabView:ReloadData(false, true)
	RegisterTickOnce(function()
		if self.QnTabView then
			self.QnTabView.m_Table:Reposition()
			self.QnTabView.m_ScrollView:ResetPosition()
		end
	end,50)
end

function LuaCommonSideDialogWnd:CancelRepositionTick()
	if self.m_RepositionTick then
		UnRegisterTick(self.m_RepositionTick)
		self.m_RepositionTick = nil
	end
end

--@region UIEvent

function LuaCommonSideDialogWnd:OnClearBtnClick()
	LuaCommonSideDialogMgr:RefuseAll()
end

function LuaCommonSideDialogWnd:OnCloseBtnClick()
	CUIManager.CloseUI(CLuaUIResources.CommonSideDialogWnd)
	local topDialogData = LuaCommonSideDialogMgr:GetTopDialogData()
	if topDialogData and topDialogData.onCloseAction then
		topDialogData.onCloseAction()
	end
end

function LuaCommonSideDialogWnd:OnHideListViewButtonClick()
	self.ListView.gameObject:SetActive(false)
	self.SingleView.gameObject:SetActive(true)
	local labels = CommonDefs.GetComponentsInChildren_Component_Type(self.SingleView.transform, typeof(UILabel))
	for i = 0, labels.Length - 1 do
		labels[i]:ResizeCollider()
    end
	local num = #LuaCommonSideDialogMgr.m_DialogList 
	if num == 0 then
		CUIManager.CloseUI(CLuaUIResources.CommonSideDialogWnd)
	end
end

function LuaCommonSideDialogWnd:OnTipsBtnClick()
	self.ListView.gameObject:SetActive(true)
	self.SingleView.gameObject:SetActive(false)
	self:InitListView()
end

function LuaCommonSideDialogWnd:OnBlankWidgetClick()
	self:OnHideListViewButtonClick()
end
--@endregion UIEvent

