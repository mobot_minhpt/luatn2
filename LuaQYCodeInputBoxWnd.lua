require("common/common_include")
local UIInput = import "UIInput"
local UILabel = import "UILabel"
local CommonDefs = import "L10.Game.CommonDefs"
local Gac2Login = import "L10.Game.Gac2Login"
local CUIManager = import "L10.UI.CUIManager"

LuaQYCodeInputBoxWnd = class()

RegistClassMember(LuaQYCodeInputBoxWnd, "m_OKButton")
RegistClassMember(LuaQYCodeInputBoxWnd, "m_CancelButton")
RegistClassMember(LuaQYCodeInputBoxWnd, "m_TitleLabel")
RegistClassMember(LuaQYCodeInputBoxWnd, "m_InputLabel")
RegistClassMember(LuaQYCodeInputBoxWnd, "m_Input")
RegistClassMember(LuaQYCodeInputBoxWnd, "m_WndBg")
RegistClassMember(LuaQYCodeInputBoxWnd, "m_MaxContentWidth")

function LuaQYCodeInputBoxWnd:BindComponentsAndInitFields()
	self.m_OKButton = self.transform:Find("Anchor/OkButton").gameObject
	self.m_CancelButton = self.transform:Find("Anchor/CancelButton").gameObject
    self.m_TitleLabel = self.transform:Find("Anchor/TextLabel").gameObject:GetComponent(typeof(UILabel))
    self.m_TitleLabel.text = LocalString.GetString("导入已有账号")
    self.m_InputLabel = self.transform:Find("Anchor/Input/Label").gameObject:GetComponent(typeof(UILabel))
    self.m_InputLabel.text = LocalString.GetString("请输入账号迁移码")
    self.m_Input = self.transform:Find("Anchor/Input"):GetComponent(typeof(UIInput))
    self.m_WndBg = self.transform:Find("Wnd_Bg_Box").gameObject
end

function LuaQYCodeInputBoxWnd:Awake()
    self:BindComponentsAndInitFields()
    CommonDefs.AddOnClickListener(self.m_OKButton, DelegateFactory.Action_GameObject(function(go) self:OnOkButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_CancelButton, DelegateFactory.Action_GameObject(function(go) self:OnCancelButtonClick(go) end), false)
    CommonDefs.AddEventDelegate(self.m_Input.onChange, DelegateFactory.Action(function ( ... ) self:OnInputChanged() end))
end

function LuaQYCodeInputBoxWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.QYCodeInputBoxWnd)
end

function LuaQYCodeInputBoxWnd:OnCancelButtonClick(go)
    self:Close()
end

function LuaQYCodeInputBoxWnd:OnOkButtonClick(go)
    local bValid, formattedCode = self:TryParseQyCode(self.m_Input.text)
    if not (bValid and formattedCode) then
        g_MessageMgr:ShowMessage("QYCODE_USE_BAD_FORMAT")
        return
    end
    Gac2Login.RequestUseQYCode(formattedCode)
    self.m_Input.text = ""
end

function LuaQYCodeInputBoxWnd:OnInputChanged()
    local text = self.m_Input.text or ""
    self.m_Input.text = string.upper(text)
end

function LuaQYCodeInputBoxWnd:TryParseQyCode(input)
    if not input then return end

    -- 1. 只能包含A-Z 0-9 和 -
    if string.match(input, "[^A-Z0-9%-]") then return end
    -- 2. 去掉所有-
    local qycode = input:gsub("-", "")
    -- 3. 长度为16
    if string.len(qycode) ~= 16 then return end

    return true, qycode
end

return LuaQYCodeInputBoxWnd
