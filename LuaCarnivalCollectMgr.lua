local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaCarnivalCollectMgr = {}

LuaCarnivalCollectMgr.locationId = -1

-- 解析Location --> stampIds
function LuaCarnivalCollectMgr:ParseLocation2StampIds()
	local tbl = {}
	JiaNianHua_Stamp.Foreach(function(k, v)
		local location = tonumber(v.Location)
		if not tbl[location] then
			tbl[location] = {}
		end

		table.insert(tbl[location], k)
	end)
	return tbl
end

function LuaCarnivalCollectMgr:GetCollectData()
	local mainPlayer = CClientMainPlayer.Inst
	if not mainPlayer or not mainPlayer.PlayProp then
		return
	end

	return mainPlayer.PlayProp.CarnivalCollectData
end

-- 获取一个任务的进度
function LuaCarnivalCollectMgr:GetStampProgress(stampId)
	local collectData = self:GetCollectData()
	if not collectData then return 0 end

	if CommonDefs.DictContains_LuaCall(collectData.StampProgress, stampId) then
		return tonumber(CommonDefs.DictGetValue_LuaCall(collectData.StampProgress, stampId))
	end
	return 0
end

-- 获取一个任务需要的总进度
function LuaCarnivalCollectMgr:GetStampTotalProgress(stampId)
	return JiaNianHua_Stamp.GetData(stampId).NeedTimes
end

-- 一个地点的抽奖券已领取
function LuaCarnivalCollectMgr:GetLocationPrized(locationId)
	local collectData = self:GetCollectData()
	if not collectData then return false end

	return collectData.LocationPrized:GetBit(locationId)
end

-- 获取金券数量
function LuaCarnivalCollectMgr:GetBigTicketCount()
	local collectData = self:GetCollectData()
	if not collectData then return 0 end

	return collectData.BigTicketCount
end

-- 获取银券数量
function LuaCarnivalCollectMgr:GetSmallTicketCount()
	local collectData = self:GetCollectData()
	if not collectData then return 0 end

	return collectData.SmallTicketCount
end

-- ---------------------
-- Gac2Gas:
-- 	RequestConfirmStamp 进度满了,盖章
-- 	RequestUseFreeStamp 对某个进度使用自由章
-- 	RequestSmallPrizeDraw 某个进度满了,抽小奖,传进度id (Lua_JiaNianHua_Stamp 中的id)
-- 	RequestBigPrizeDraw 所有进度满,抽大奖
--
-- Gas2Gac:
--  RequestSmallPrizeDraw -> SyncSmallPrizeDrawIndex
--	RequestBigPrizeDraw -> SyncBigPrizeDrawIndex
-- ---------------------

-- 抽小奖抽到的物品(代表 Lua_JiaNianHua_SmallPrizeWeight 中的一行)
function Gas2Gac.SyncSmallPrizeDrawIndex(smallPrizeItem)
	g_ScriptEvent:BroadcastInLua("SyncSmallPrizeDrawIndex", smallPrizeItem)
end

-- 抽大奖抽到的物品(代表 Lua_JiaNianHua_BigPrizeWeight 中的一行)
function Gas2Gac.SyncBigPrizeDrawIndex(bigPrizeItem)
	g_ScriptEvent:BroadcastInLua("SyncBigPrizeDrawIndex", bigPrizeItem)
end

-- 集章的进度发生变化
function Gas2Gac.SyncStampCollectProgress(stampId, newCount)
	-- 如果 newCount 大于等于需要的总进度,那么就是进度满了
	if not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.PlayProp then
		return
	end
	CommonDefs.DictSet_LuaCall(CClientMainPlayer.Inst.PlayProp.CarnivalCollectData.StampProgress, stampId, newCount)
	g_ScriptEvent:BroadcastInLua("SyncStampCollectProgress", stampId, newCount)
end

-- 查询全服礼包的结果
-- Gac2Gas.QueryCarnivalTicketStock 的对应返回
-- status 对应以下枚举
--[[ ---------------------
EnumCarnivalTicketStatus = {
	eNotStart = 0,	-- 尚未开始
	ePrepare = 1,	-- 备货
	eOnSell = 2,	-- 正在售卖
	eSoldout = 3,	-- 卖完
}
-- ---------------------]]
-- stockInfoUd 可以解出来一个表, 表里面是一个id后面一个count,表示对应id剩余多少个
-- 需要结合 PropertyPlay->CarnivalCollectData->BuyTicketId 和 BuyTicketExpireTime 判断当前id是否买过
function Gas2Gac.SyncCarnivalTickStock(status, onShelfTime, stockInfoUd)
	g_ScriptEvent:BroadcastInLua("SyncCarnivalTickStock", status, onShelfTime, stockInfoUd)
end

-- 购买成功,用以设置 PropertyPlay->CarnivalCollectData->BuyTicketId, 记录买过哪些
-- Gac2Gas.RequestBuyCarnivalTicket
function Gas2Gac.SyncBuyCarnivalTickDone(id)
	if not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.PlayProp then
		return
	end
	CClientMainPlayer.Inst.PlayProp.CarnivalCollectData.BuyTicketId:SetBit(id, true)
	g_ScriptEvent:BroadcastInLua("SyncBuyCarnivalTickDone", id)
end

-- 某个地点抽完奖,设置prop
function Gas2Gac.SyncCarnivalPrizedLocationId(locationId)
	if not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.PlayProp then
		return
	end
	CClientMainPlayer.Inst.PlayProp.CarnivalCollectData.LocationPrized:SetBit(locationId, true)
	g_ScriptEvent:BroadcastInLua("SyncCarnivalPrizedLocationId", locationId)
end

-- 抽完大奖
function Gas2Gac.SyncCarnivalBigPrized()
	if not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.PlayProp then
		return
	end
	CClientMainPlayer.Inst.PlayProp.CarnivalCollectData.BigPrized = 1
	g_ScriptEvent:BroadcastInLua("SyncCarnivalBigPrized")
end

-- 需要设置 PropertyPlay->CarnivalCollectData->ExpireTime 为 expireTime
-- bResetData 如果为true,则需要清理 StampProgress 、 LocationPrized 、 BigPrized 数据
function Gas2Gac.SyncCarnivalExpireTimeChange(expireTime, bResetData)
	if not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.PlayProp then return end
	CClientMainPlayer.Inst.PlayProp.CarnivalCollectData.ExpireTime = expireTime
	if bResetData then
		CClientMainPlayer.Inst.PlayProp.CarnivalCollectData.BigPrized = 0
		CommonDefs.DictClear(CClientMainPlayer.Inst.PlayProp.CarnivalCollectData.StampProgress)
		local locationPrized = CClientMainPlayer.Inst.PlayProp.CarnivalCollectData.LocationPrized
		for i = 0, locationPrized:GetBitSize() - 1 do
			locationPrized:SetBit(i, false)
		end
	end
end

function Gas2Gac.SyncCarnivalBigTicketCount(newCount)
	local mainPlayer = CClientMainPlayer.Inst
	if not mainPlayer or not mainPlayer.PlayProp then
		return
	end
	mainPlayer.PlayProp.CarnivalCollectData.BigTicketCount = newCount
	g_ScriptEvent:BroadcastInLua("SyncCarnivalBigTicketCount")
end

function Gas2Gac.SyncCarnivalSmallTicketCount(newCount)
	local mainPlayer = CClientMainPlayer.Inst
	if not mainPlayer or not mainPlayer.PlayProp then
		return
	end
	mainPlayer.PlayProp.CarnivalCollectData.SmallTicketCount = newCount
	g_ScriptEvent:BroadcastInLua("SyncCarnivalSmallTicketCount")
end

function LuaCarnivalCollectMgr:PlayerGetBigTicketCarnivalCollect()
	g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("CARNIVALCOLLECT_GET_BIG_TICKET"), function()
		CUIManager.ShowUI(CLuaUIResources.CarnivalCollectWnd)
	end, nil, nil, nil, false)
end

function LuaCarnivalCollectMgr:PlayerGetSmallTicketCarnivalCollect()
	g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("CARNIVALCOLLECT_GET_SMALL_TICKET"), function()
		CUIManager.ShowUI(CLuaUIResources.CarnivalCollectWnd)
	end, nil, nil, nil, false)
end

function LuaCarnivalCollectMgr:CarnivalCollectTicketCheck()
	return (self:GetSmallTicketCount() + self:GetBigTicketCount()) > 0
end
