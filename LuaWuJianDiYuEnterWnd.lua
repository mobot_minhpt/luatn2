local CUIManager = import "L10.UI.CUIManager"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local MessageWndManager = import "L10.UI.MessageWndManager"
local PlayerSettings = import "L10.Game.PlayerSettings"

CLuaWuJianDiYuEnterWnd = class()
RegistClassMember(CLuaWuJianDiYuEnterWnd, "m_Root")

function CLuaWuJianDiYuEnterWnd:Init()
    local enterButton = self.transform:Find("Root/EnterButton").gameObject
    local Bg = self.transform:Find("_BgMask_").gameObject
    local AdditionalBg = self.transform:Find("AdditionalBgMask").gameObject
    Extensions.RemoveAllChildren(AdditionalBg.transform)
    NGUITools.AddChild(AdditionalBg,Bg)
    NGUITools.AddChild(AdditionalBg,Bg)
    UIEventListener.Get(enterButton).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.RequestStartUnintermittentHell()
        CUIManager.CloseUI(CLuaUIResources.WuJianDiYuEnterWnd)
    end)

    self:TryShowWarning()
end

function CLuaWuJianDiYuEnterWnd:Awake()
    self.m_Root = self.transform:Find("Root").gameObject
end

function CLuaWuJianDiYuEnterWnd:OnEnable()
    --Messages shoule be parsed like Tips
    local MessageTable = self.transform:Find("Root/MessageTable").gameObject
    Extensions.RemoveAllChildren(MessageTable.transform)
    local TipTable = self.transform:Find("Root/Templates/TipTable").gameObject
    TipTable:SetActive(false)
    local raw_msg = g_MessageMgr:FormatMessage("WUJIANDIYU_ENTER_INTERFACE_MSG")
    local info = CMessageTipMgr.regex_Paragraph:Matches(raw_msg)
    do
        local i = 0
        while i < info.Count do
            local new_tip = CUICommonDef.AddChild(MessageTable, TipTable)
            local messageLabel = new_tip.transform:Find("MessageLabel"):GetComponent(typeof(UILabel))
            messageLabel.text = ToStringWrap(info[i].Groups[1])
            new_tip:SetActive(true)
            i = i + 1
        end
    end
end

--TODO 将player_setting_val替换为设定变量
function CLuaWuJianDiYuEnterWnd:TryShowWarning()
    local player_setting_val = PlayerSettings.WuJianDiYuWarningDefaultConfirm
    if(not player_setting_val)then
        self.m_Root:SetActive(false)
        MessageWndManager.ShowMessageWithCheckBox(LocalString.GetString("无间地狱玩法有[c][FF0000]轻微恐怖[-]内容（不宜儿童观看），确定要参加吗？"), LocalString.GetString("下次不再提示"), false, DelegateFactory.Action_bool(function (sign) 
            PlayerSettings.WuJianDiYuWarningDefaultConfirm = sign
            self.m_Root:SetActive(true)
        end), DelegateFactory.Action_bool(function (sign) 
            PlayerSettings.WuJianDiYuWarningDefaultConfirm = sign
            CUIManager.CloseUI(CLuaUIResources.WuJianDiYuEnterWnd)
        end), nil, nil)
    end
end
