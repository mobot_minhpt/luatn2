
-- CFurnitureScene
-- furniture scene for gas and gac usage
CFurnitureScene = class()
RegistClassMember(CFurnitureScene, "m_Line")
RegistClassMember(CFurnitureScene, "m_Column")
RegistClassMember(CFurnitureScene, "m_GridWidth")
RegistClassMember(CFurnitureScene, "m_GridHeight")

RegistClassMember(CFurnitureScene, "m_FenceGuashiPos")

RegistClassMember(CFurnitureScene, "m_TempLine")
RegistClassMember(CFurnitureScene, "m_TempColumn")
RegistClassMember(CFurnitureScene, "m_TempInfo")
RegistClassMember(CFurnitureScene, "TempId")
RegistClassMember(CFurnitureScene, "m_Furnitures")


EnumFurnitureUseType = {
	eFurnitureNormal = 0,
	eFurnitureWall = 1,
	eFurnitureGround = 2,
	eFurnitureAnimal = 3,
	eFurnitureOnWater = 4,
	eFurnitureInWater = 5,
}

require "common/house/furniturescene"
