local Money = import "L10.Game.Money"
local IdPartition = import "L10.Game.IdPartition"
local EquipmentTemplate_SubType = import "L10.Game.EquipmentTemplate_SubType"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local CItemMgr = import "L10.Game.CItemMgr"
local CItem = import "L10.Game.CItem"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChatLinkMgr = import "CChatLinkMgr"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CEquipment = import "L10.Game.CEquipment"
local QnTableView=import "L10.UI.QnTableView"
local QnAddSubAndInputButton=import "L10.UI.QnAddSubAndInputButton"
local CUITexture=import "L10.UI.CUITexture"
local CButton=import "L10.UI.CButton"
local CCurentMoneyCtrl=import "L10.UI.CCurentMoneyCtrl"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"


CLuaNPCShopWnd2 = class()
RegistClassMember(CLuaNPCShopWnd2,"titleLabel")
RegistClassMember(CLuaNPCShopWnd2,"goodsTable")
RegistClassMember(CLuaNPCShopWnd2,"nameLabel")
RegistClassMember(CLuaNPCShopWnd2,"levelLabel")
RegistClassMember(CLuaNPCShopWnd2,"typeLabel")
RegistClassMember(CLuaNPCShopWnd2,"descLabel")
RegistClassMember(CLuaNPCShopWnd2,"descScrollView")
RegistClassMember(CLuaNPCShopWnd2,"numberInput")
RegistClassMember(CLuaNPCShopWnd2,"icon")
RegistClassMember(CLuaNPCShopWnd2,"buyBtn")
RegistClassMember(CLuaNPCShopWnd2,"selectedItemIndex")
RegistClassMember(CLuaNPCShopWnd2,"moneyCtrl")
RegistClassMember(CLuaNPCShopWnd2,"moneyCtrl2")
RegistClassMember(CLuaNPCShopWnd2,"m_ItemLookup")

function CLuaNPCShopWnd2:Awake()
    self.titleLabel = self.transform:Find("Wnd_Bg_Primary_Normal/TitleLabel"):GetComponent(typeof(UILabel))
    self.goodsTable = self.transform:Find("GoodsGrid"):GetComponent(typeof(QnTableView))
    self.nameLabel = self.transform:Find("Detail/NameLabel"):GetComponent(typeof(UILabel))
    self.levelLabel = self.transform:Find("Detail/LevelLabel"):GetComponent(typeof(UILabel))
    self.typeLabel = self.transform:Find("Detail/Type/TypeLabel"):GetComponent(typeof(UILabel))
    self.descLabel = self.transform:Find("Detail/DescRegion/DescScrollView/DescLabel"):GetComponent(typeof(UILabel))
    self.descScrollView = self.transform:Find("Detail/DescRegion/DescScrollView"):GetComponent(typeof(UIScrollView))
    self.numberInput = self.transform:Find("Detail/QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
    self.icon = self.transform:Find("Detail/Icon"):GetComponent(typeof(CUITexture))
    self.buyBtn = self.transform:Find("Detail/BuyBtn"):GetComponent(typeof(CButton))
    self.selectedItemIndex = 0

    self.m_ItemLookup={}

    UIEventListener.Get(self.buyBtn.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        if not self.moneyCtrl:IsEnough() then
            self.moneyCtrl:AddMoney(nil)
            return
        end
        if not self.moneyCtrl2:IsEnough() then
            self.moneyCtrl2:AddMoney(nil)
            return
        end

        local templateId = CLuaNPCShopInfoMgr.RowAt(self.selectedItemIndex)
        Gac2Gas.BuyNpcShopItem(CLuaNPCShopInfoMgr.m_npcShopInfo.NpcEngineId, CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId, templateId, self.numberInput:GetValue())
    end)
end

function CLuaNPCShopWnd2:Init()
    self.moneyCtrl = self.transform:Find("Detail/QnCostAndOwnMoney1"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
    self.moneyCtrl2 = self.transform:Find("Detail/QnCostAndOwnMoney2"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
    
    self.titleLabel.text = CLuaNPCShopInfoMgr.LastOpenShopName
    local type = CLuaNPCShopInfoMgr.m_npcShopInfo.MoneyType
    local scoreKey = CLuaNPCShopInfoMgr.m_npcShopInfo.PlayScoreKey
    local initKey = CLuaNPCShopInfoMgr.m_npcShopInfo.InitKey
    local default
    if type == EnumMoneyType.Score then
        default = LocalString.GetString("兑换")
    else
        default = LocalString.GetString("购买")
    end
    self.buyBtn.Text = (default)
    
    self.moneyCtrl.updateAction = function ()
        self:RefreshInputMinMax(self.goodsTable.currentSelectRow)
    end

    self.moneyCtrl2.updateAction = function ()
        self:RefreshInputMinMax(self.goodsTable.currentSelectRow)
    end
    
    if type == EnumMoneyType.Score then
        if initKey == "HanJiaScore" then
            --是一个临时积分
            self.moneyCtrl:InitTempPlayScore(EnumTempPlayScoreKey_lua[initKey])
        else
            self.moneyCtrl:SetType(EnumMoneyType.Score, scoreKey, true)
        end
    else
        self.moneyCtrl:SetType(type, EnumPlayScoreKey.NONE, true)
    end
    self.moneyCtrl2:SetType(EnumMoneyType.YinLiang,EnumPlayScoreKey.NONE, true)
    

    local getNumFunc=function() return #CLuaQMPKMgr.m_MatchRecordList end
    local initItemFunc=function(item,index) 
        if index % 2 == 1 then
            item:SetBackgroundTexture("common_textbg_02_dark")
        else
            item:SetBackgroundTexture("common_textbg_02_light")
        end

        self:InitItem(item,index,CLuaQMPKMgr.m_MatchRecordList[index+1])
    end

    self.goodsTable.m_DataSource=DefaultTableViewDataSource.Create(
        function()
            return CLuaNPCShopInfoMgr.Count()
        end,function(item,index)
            self:InitItem(item.transform,index)
        end)

    self.goodsTable.OnSelectAtRow=DelegateFactory.Action_int(function(row)
        self.selectedItemIndex = row
        self:UpdateDescription()

        self:RefreshInputMinMax(row)
    end)

    self.goodsTable:ReloadData(false, false)
    self.goodsTable:SetSelectRow(0, true)
end
function CLuaNPCShopWnd2:RefreshInputMinMax( row) 
    --刷新最大最小值
    local templateId = CLuaNPCShopInfoMgr.RowAt(row)
    local limit = CLuaNPCShopInfoMgr.GetRemainCount(templateId)
    local price = CLuaNPCShopInfoMgr.GetPrice(templateId)
    if price == 0 then
        price = 1
    end
    --以防出现价格为0的情况
    local maxNum = math.floor(self.moneyCtrl:GetOwn() / price)
    maxNum = math.min(maxNum, 999)

    local silverCost = CLuaNPCShopInfoMgr.m_npcShopInfo.ExtraSliverCost[templateId] or 0
    local freeSilverCost = CLuaNPCShopInfoMgr.m_npcShopInfo.ExtraFreeSliverCost[templateId] or 0
    if silverCost>0 then
        local maxNum2 = math.min(math.floor(self.moneyCtrl2:GetOwn() / silverCost),999)
        maxNum = math.min(maxNum,maxNum2)
    elseif freeSilverCost>0 then
        local maxNum2 = math.min(math.floor(self.moneyCtrl2:GetOwn() / freeSilverCost),999)
        maxNum = math.min(maxNum,maxNum2)
    end

    if limit == 0 then
        self.numberInput:SetMinMax(1, 1, 1)
    elseif maxNum == 0 then
        self.numberInput:SetMinMax(1, 1, 1)
    else
        if maxNum >= limit then
            self.numberInput:SetMinMax(1, limit, 1)
        else
            self.numberInput:SetMinMax(1, maxNum, 1)
        end
    end
end
function CLuaNPCShopWnd2:UpdateDescription( )
    local name = ""
    local level = 0
    local desc = ""
    local itemType = ""
    local price = ""

    local templateId = CLuaNPCShopInfoMgr.RowAt(self.selectedItemIndex)
    if IdPartition.IdIsItem(templateId) then
        local item = CItemMgr.Inst:GetItemTemplate(templateId)
        name = item.Name
        level = CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(item)
        desc = CItem.GetItemDescription(item.ID,true)
        self.icon:LoadMaterial(item.Icon)
        local data = Item_Type.GetData(item.Type)
        if data ~= nil then
            itemType = data.Name
        end
    elseif IdPartition.IdIsEquip(templateId) then
        local equip = EquipmentTemplate_Equip.GetData(templateId)
        local equipment = CLuaNPCShopInfoMgr.GetEquipment(templateId)
        name = equip.Name
        level = equipment.Grade
        desc = equipment.WordDescription
        self.icon:LoadMaterial(equip.Icon)
        local subTypeTemplate = EquipmentTemplate_SubType.GetData(equip.Type * 100 + equip.SubType)
        if subTypeTemplate ~= nil then
            itemType = subTypeTemplate.Name
        end
    end
    
    -- self.numberInput.onValueChanged = MakeDelegateFromCSFunction(self.BuyCountChg, MakeGenericClass(Action1, UInt32), self)
    self.numberInput.onValueChanged = DelegateFactory.Action_uint(function(val)
        local templateId = CLuaNPCShopInfoMgr.RowAt(self.selectedItemIndex)
        self.moneyCtrl:SetCost(val * CLuaNPCShopInfoMgr.GetPrice(templateId))

        local silverCost = CLuaNPCShopInfoMgr.m_npcShopInfo.ExtraSliverCost[templateId] or 0
        local freeSilverCost = CLuaNPCShopInfoMgr.m_npcShopInfo.ExtraFreeSliverCost[templateId] or 0
        self.moneyCtrl2:SetCost(val * (silverCost + freeSilverCost))
    end)

    self.numberInput:SetValue(1, true)

    self.nameLabel.text = name
    local default
    if level > 0 then
        default = "Lv." .. tostring(level)
    else
        default = nil
    end
    local levelStr = default
    if CClientMainPlayer.Inst ~= nil then
        if CClientMainPlayer.Inst.Level < level then
            levelStr = System.String.Format("[c][ff0000]{0}[-][/c]", levelStr)
        end
    end
    self.levelLabel.text = levelStr
    self.typeLabel.text = itemType
    self.descLabel.text = CChatLinkMgr.TranslateToNGUIText(desc, false)
    self.descScrollView:ResetPosition()
    self.moneyCtrl:SetCost(CLuaNPCShopInfoMgr.GetPrice(templateId))

    local silverCost = CLuaNPCShopInfoMgr.m_npcShopInfo.ExtraSliverCost[templateId] or 0
    local freeSilverCost = CLuaNPCShopInfoMgr.m_npcShopInfo.ExtraFreeSliverCost[templateId] or 0
    if silverCost>0 then
        self.moneyCtrl2:SetCost(silverCost)
        self.moneyCtrl2.gameObject:SetActive(silverCost > 0)
    elseif freeSilverCost>0 then
        self.moneyCtrl2:SetCost(freeSilverCost)
        self.moneyCtrl2.gameObject:SetActive(freeSilverCost > 0)
    else
        self.moneyCtrl2.gameObject:SetActive(false)
    end
end

function CLuaNPCShopWnd2:InitItem(transform,row)
    local templateId = CLuaNPCShopInfoMgr.RowAt(row)
    self.m_ItemLookup[templateId]=transform

    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local priceLabel = transform:Find("PriceLabel"):GetComponent(typeof(UILabel))
    local moneySprite = transform:Find("MoneySprite"):GetComponent(typeof(UISprite))
    local needSprite = transform:Find("NeedSprite"):GetComponent(typeof(UISprite))
    local disableSprite = transform:Find("Icon/DisableSprite"):GetComponent(typeof(UISprite))
    local levelLabel = transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local numLimitLabel = transform:Find("NumLimitLabel"):GetComponent(typeof(UILabel))

    local silverCost = CLuaNPCShopInfoMgr.m_npcShopInfo.ExtraSliverCost[templateId] or 0
    local freeSilverCost = CLuaNPCShopInfoMgr.m_npcShopInfo.ExtraFreeSliverCost[templateId] or 0
    if silverCost>0 then
        self.moneyCtrl2:SetType(EnumMoneyType.YinLiang,EnumPlayScoreKey.NONE, true)
    elseif freeSilverCost>0 then
        --积分商店购买处, 银两不会自动转化成银票
        self.moneyCtrl2:SetType(EnumMoneyType.YinPiao,EnumPlayScoreKey.NONE, false)
    end

    nameLabel.text = nil
    levelLabel.text = nil
    icon:Clear()
    -- moneySprite.spriteName = iconName
    moneySprite.spriteName = Money.GetIconName(CLuaNPCShopInfoMgr.m_npcShopInfo.MoneyType, CLuaNPCShopInfoMgr.m_npcShopInfo.PlayScoreKey)
    
    if IdPartition.IdIsEquip(templateId) then
        if CTaskMgr.IsEquipSubmitItem(templateId) then
            needSprite.alpha = 1
        else
            needSprite.alpha = 0
        end
    else
        if CLuaNPCShopInfoMgr.taskNeededItems[templateId] then
            needSprite.alpha = 1
        else
            needSprite.alpha = 0
        end
    end

    disableSprite.enabled = false
    local gradeRequire = 0
    if IdPartition.IdIsItem(templateId) then
        local item = CItemMgr.Inst:GetItemTemplate(templateId)
        if item ~= nil then
            nameLabel.text = item.Name
            gradeRequire = CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(item)
            icon:LoadMaterial(item.Icon)
            disableSprite.enabled = not CItem.GetMainPlayerIsFit(templateId, true)
        end
    elseif IdPartition.IdIsEquip(templateId) then
        --装备
        local equip = EquipmentTemplate_Equip.GetData(templateId)
        nameLabel.text = equip.Name
        gradeRequire = equip.Grade

        icon:LoadMaterial(equip.Icon)
        disableSprite.enabled = not CEquipment.GetMainPlayerIsFit(templateId, true)
    end

    local default
    if gradeRequire > 0 then
        default = "Lv." .. tostring(gradeRequire)
    else
        default = nil
    end
    local gradeStr = default
    --等级不够 标红
    if gradeStr then
        if CClientMainPlayer.Inst ~= nil then
            if CClientMainPlayer.Inst.Level < gradeRequire then
                gradeStr = SafeStringFormat3("[c][ff0000]%s[-][/c]", gradeStr)
            end
        end
    end
    levelLabel.text = gradeStr

    priceLabel.text = tostring(CLuaNPCShopInfoMgr.GetPrice(templateId))
    self:UpdateRemainCountInfo(templateId)
end

function CLuaNPCShopWnd2:OnNPCShopItemRemainCountChange( shopId, templateId, remainCount) 
    if CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId == shopId then
        CLuaNPCShopInfoMgr.SetRemainCount(templateId,remainCount)
        self:RefreshInputMinMax(self.goodsTable.currentSelectRow)

        self:UpdateRemainCountInfo(templateId)
    end
end

function CLuaNPCShopWnd2:UpdateRemainCountInfo(templateId)
    if not self.m_ItemLookup[templateId] then
        return 
    end
    local numLimitLabel = self.m_ItemLookup[templateId]:Find("NumLimitLabel"):GetComponent(typeof(UILabel))
    local limit = CLuaNPCShopInfoMgr.GetRemainCount(templateId)
    local limitType = CLuaNPCShopInfoMgr.GetLimitType(templateId)
    if limitType == 1 then
        numLimitLabel.text = SafeStringFormat3(LocalString.GetString("剩余%d"), limit)
    elseif limitType == 2 then
        numLimitLabel.text = SafeStringFormat3(LocalString.GetString("本月剩余%d"), limit)
    else
        numLimitLabel.text = nil
    end
end

function CLuaNPCShopWnd2:OnEnable()
	g_ScriptEvent:AddListener("UpdateNpcShopLimitItemCount", self, "OnNPCShopItemRemainCountChange")
	g_ScriptEvent:AddListener("MainPlayerLevelChange", self, "UpdateDisableStatus")
    
end
function CLuaNPCShopWnd2:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateNpcShopLimitItemCount", self, "OnNPCShopItemRemainCountChange")
	g_ScriptEvent:RemoveListener("MainPlayerLevelChange", self, "UpdateDisableStatus")
end

function CLuaNPCShopWnd2:UpdateDisableStatus()
    for k,transform in pairs(self.m_ItemLookup) do
        local templateId=k
        local disableSprite = transform:Find("Icon/DisableSprite"):GetComponent(typeof(UISprite))
        if IdPartition.IdIsItem(templateId) then
            disableSprite.enabled = not CItem.GetMainPlayerIsFit(templateId, false)
        elseif IdPartition.IdIsEquip(templateId) then
            disableSprite.enabled = not CEquipment.GetMainPlayerIsFit(templateId, false)
        end
    end
end
