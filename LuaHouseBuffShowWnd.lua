local CPlayerInfoMgr=import "CPlayerInfoMgr"
local UIWidget = import "UIWidget"
local UIPanel = import "UIPanel"
local NGUIMath = import "NGUIMath"
local Extensions = import "Extensions"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CHouseBuffShowMgr = import "L10.UI.CHouseBuffShowMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonLuaWnd=import "L10.UI.CCommonLuaWnd"
local CCommonLuaScript=import "L10.UI.CCommonLuaScript"
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"

CLuaHouseBuffShowWnd = class()
RegistClassMember(CLuaHouseBuffShowWnd,"scrollView")
RegistClassMember(CLuaHouseBuffShowWnd,"table")
RegistClassMember(CLuaHouseBuffShowWnd,"buffTemplate")
RegistClassMember(CLuaHouseBuffShowWnd,"shareBtnNode")
RegistClassMember(CLuaHouseBuffShowWnd,"infoBtnNode")
RegistClassMember(CLuaHouseBuffShowWnd,"background")
RegistClassMember(CLuaHouseBuffShowWnd,"contentBg")
RegistClassMember(CLuaHouseBuffShowWnd,"maxHeight")
RegistClassMember(CLuaHouseBuffShowWnd,"showColor")
RegistClassMember(CLuaHouseBuffShowWnd,"addstring")
RegistClassMember(CLuaHouseBuffShowWnd,"m_Wnd")

function CLuaHouseBuffShowWnd:Awake()
    self.m_Wnd=self.gameObject:GetComponent(typeof(CCommonLuaWnd))

    self.scrollView = self.transform:Find("ScrollView"):GetComponent(typeof(UIScrollView))
    self.table = self.transform:Find("ScrollView/Table"):GetComponent(typeof(UITable))
    self.buffTemplate = self.transform:Find("ScrollView/BuffItem").gameObject
    self.shareBtnNode = self.transform:Find("ShareBtn").gameObject
    self.infoBtnNode = self.transform:Find("InfoBtn").gameObject
    self.background = self.transform:Find("Background"):GetComponent(typeof(UISprite))
    self.contentBg = self.transform:Find("ContentBg"):GetComponent(typeof(UIWidget))
    self.maxHeight = 744
    self.showColor = nil
    self.addstring = LocalString.GetString("的祈愿")

    self.buffTemplate:SetActive(false)
    self.shareBtnNode:SetActive(false)
    self.infoBtnNode:SetActive(false)

end
function CLuaHouseBuffShowWnd:Update()
    self.m_Wnd:ClickThroughToClose()
end

function CLuaHouseBuffShowWnd:Init( )
    if CClientMainPlayer.Inst == nil then
        CUIManager.CloseUI(CIndirectUIResources.HouseBuffShowWnd)
        return
    end

    self.shareBtnNode:SetActive(false)
    self.infoBtnNode:SetActive(false)

    Extensions.RemoveAllChildren(self.table.transform)

    local buffItem = NGUITools.AddChild(self.table.gameObject, self.buffTemplate)
    buffItem:SetActive(true)
    local script = buffItem:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf


    self.showColor = script:Init(CHouseBuffShowMgr.PlayerId, self)
    script.OnDescLabelChanged = function()
        self:UpdateBackground()
    end

    if CHouseBuffShowMgr.EnableBuffShare then
        if CHouseBuffShowMgr.EnableCheckInfo then
            if CHouseBuffShowMgr.PlayerId == CClientMainPlayer.Inst.Id then
                CommonDefs.GetComponent_Component_Type(self.scrollView, typeof(UIPanel)).bottomAnchor:Set(0, 20)
                self.shareBtnNode:SetActive(false)
                self.infoBtnNode:SetActive(false)
            else
                CommonDefs.GetComponent_Component_Type(self.scrollView, typeof(UIPanel)).bottomAnchor:Set(0, 117)
                self.infoBtnNode:SetActive(true)
                UIEventListener.Get(self.infoBtnNode).onClick = DelegateFactory.VoidDelegate(function(go)
                    CPlayerInfoMgr.ShowPlayerInfoWnd(CHouseBuffShowMgr.PlayerId, CHouseBuffShowMgr.PlayerName)
                end)
            end
        else
            CommonDefs.GetComponent_Component_Type(self.scrollView, typeof(UIPanel)).bottomAnchor:Set(0, 117)
            self.shareBtnNode:SetActive(true)
            UIEventListener.Get(self.shareBtnNode).onClick = DelegateFactory.VoidDelegate(function(go)
                local showstring = CHouseBuffShowMgr.PlayerName .. self.addstring
                local msg = nil
                if self.showColor then
                    msg = SafeStringFormat3("<link button=#c%s%s#n,HouseBuffClick,%s,%s>",self.showColor,showstring,CHouseBuffShowMgr.PlayerId, CHouseBuffShowMgr.PlayerName)
                else
                    msg = SafeStringFormat3("<link button=%s,HouseBuffClick,%s,%s>",showstring,CHouseBuffShowMgr.PlayerId, CHouseBuffShowMgr.PlayerName)
                end
                LuaInGameShareMgr.ShareHouseBuff(msg,go.transform,CPopupMenuInfoMgr.AlignType.Top)
            end)
        end
    else
        CommonDefs.GetComponent_Component_Type(self.scrollView, typeof(UIPanel)).bottomAnchor:Set(0, 20)
        self.shareBtnNode:SetActive(false)
        self.infoBtnNode:SetActive(false)
    end

    self:UpdateBackground()
end
function CLuaHouseBuffShowWnd:UpdateBackground( )
    self.table:Reposition()
    self.scrollView:ResetPosition()
    --计算背景
    local contentHeight = NGUIMath.CalculateRelativeWidgetBounds(self.table.transform).size.y + math.abs(self.scrollView.panel.topAnchor.absolute) + math.abs(self.scrollView.panel.bottomAnchor.absolute)
    self.background.height = math.floor(math.min(contentHeight, self.maxHeight))

    self.scrollView.panel:ResetAndUpdateAnchors()
    CommonDefs.GetComponent_Component_Type(self.table, typeof(UIWidget)):ResetAndUpdateAnchors()
    self.scrollView:ResetPosition()
    self.contentBg:ResetAndUpdateAnchors()
end

function CLuaHouseBuffShowWnd:CloseWnd()
    CUIManager.CloseUI(CIndirectUIResources.HouseBuffShowWnd)
end
