local PathType = import "DG.Tweening.PathType"
local PathMode = import "DG.Tweening.PathMode"
local MessageWndManager = import "L10.UI.MessageWndManager"
local LuaTweenUtils = import "LuaTweenUtils"
local Ease = import "DG.Tweening.Ease"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CScene=import "L10.Game.CScene"
local Vector4=import "UnityEngine.Vector4"
local CForcesMgr = import "L10.Game.CForcesMgr"

require "ui/guanning/LuaGuanNingResultPane"

CLuaAnQiDaoResultWnd = class()
RegistClassMember(CLuaAnQiDaoResultWnd,"haicangHealthSlider")
RegistClassMember(CLuaAnQiDaoResultWnd,"dafuHealthSlider")
RegistClassMember(CLuaAnQiDaoResultWnd,"haicangHealthFore")
RegistClassMember(CLuaAnQiDaoResultWnd,"dafuHealthFore")
RegistClassMember(CLuaAnQiDaoResultWnd,"haicangSelfNode")
RegistClassMember(CLuaAnQiDaoResultWnd,"dafuSelfNode")
RegistClassMember(CLuaAnQiDaoResultWnd,"requestDoubleScoreBtn")
RegistClassMember(CLuaAnQiDaoResultWnd,"requestDoubleScoreNode")
RegistClassMember(CLuaAnQiDaoResultWnd,"scoreLabel")
RegistClassMember(CLuaAnQiDaoResultWnd,"uiFx")
RegistClassMember(CLuaAnQiDaoResultWnd,"fxWidget")
RegistClassMember(CLuaAnQiDaoResultWnd,"battleDataButton")
RegistClassMember(CLuaAnQiDaoResultWnd,"clsRankButton")
RegistClassMember(CLuaAnQiDaoResultWnd,"clsRankLabel")
RegistClassMember(CLuaAnQiDaoResultWnd,"haicangResultSprite")
RegistClassMember(CLuaAnQiDaoResultWnd,"dafuResultSprite")
RegistClassMember(CLuaAnQiDaoResultWnd,"resultPane1")
RegistClassMember(CLuaAnQiDaoResultWnd,"resultPane2")
RegistClassMember(CLuaAnQiDaoResultWnd,"shareButton")
RegistClassMember(CLuaAnQiDaoResultWnd,"FilterCls")
RegistClassMember(CLuaAnQiDaoResultWnd,"mHaicangPlayInfoList")
RegistClassMember(CLuaAnQiDaoResultWnd,"mDafuPlayInfoList")
RegistClassMember(CLuaAnQiDaoResultWnd,"mSliderColors")

RegistClassMember(CLuaAnQiDaoResultWnd,"m_IsTrainScene")

function CLuaAnQiDaoResultWnd:Awake()
    self.mSliderColors = {"ff0000", "fe790a", "fcb824", "fbfd3e", "79ff1f"}
    self.m_IsTrainScene=false
    if CScene.MainScene and CScene.MainScene.GamePlayDesignId==51100208 then
        self.m_IsTrainScene=true
    end
    
    self.haicangHealthSlider = self.transform:Find("Anchor/Left/Slider"):GetComponent(typeof(UISlider))
    self.dafuHealthSlider = self.transform:Find("Anchor/Right/Slider"):GetComponent(typeof(UISlider))
    self.haicangHealthFore = self.transform:Find("Anchor/Left/Slider/Foreground"):GetComponent(typeof(UITexture))
    self.dafuHealthFore = self.transform:Find("Anchor/Right/Slider/Foreground"):GetComponent(typeof(UITexture))
    self.haicangSelfNode = self.transform:Find("Anchor/Left/Statics/SelfNode").gameObject
    self.dafuSelfNode = self.transform:Find("Anchor/Right/Statics/SelfNode").gameObject
    self.requestDoubleScoreBtn = self.transform:Find("Anchor/RequestDoubleScore/RquestDoubelScoreBtn").gameObject
    self.requestDoubleScoreNode = self.transform:Find("Anchor/RequestDoubleScore").gameObject
    self.scoreLabel = self.transform:Find("Anchor/RequestDoubleScore/ScoreLabel"):GetComponent(typeof(UILabel))
    self.uiFx = self.transform:Find("Anchor/RequestDoubleScore/RquestDoubelScoreBtn/Fx"):GetComponent(typeof(CUIFx))
    self.fxWidget = self.transform:Find("Anchor/RequestDoubleScore/RquestDoubelScoreBtn"):GetComponent(typeof(UIWidget))
    self.battleDataButton = self.transform:Find("Anchor/BattleDataButon").gameObject
    self.clsRankButton = self.transform:Find("Anchor/ClassRankButton").gameObject
    self.clsRankLabel = self.transform:Find("Anchor/ClassRankButton/Label"):GetComponent(typeof(UILabel))
    self.haicangResultSprite = self.transform:Find("Anchor/Sprite/Sprite"):GetComponent(typeof(UISprite))
    self.dafuResultSprite = self.transform:Find("Anchor/Sprite/Sprite (1)"):GetComponent(typeof(UISprite))

    self.resultPane1 = CLuaGuanNingResultPane:new()
    self.resultPane2 = CLuaGuanNingResultPane:new()
    self.resultPane1:Init(self.transform:Find("Anchor/Left"))
    self.resultPane2:Init(self.transform:Find("Anchor/Right"))

    self.shareButton = self.transform:Find("Anchor/ShareButton").gameObject
    -- self.m_ShareIndex = 0
    self.FilterCls = false
    self.mHaicangPlayInfoList = {}
    self.mDafuPlayInfoList = {}

    self.haicangResultSprite.gameObject:SetActive(false)
    self.dafuResultSprite.gameObject:SetActive(false)
    self.battleDataButton:SetActive(false)

    self.uiFx.gameObject:SetActive(false)

    UIEventListener.Get(self.requestDoubleScoreBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:RequestGetDoubleScore(go) end)

    UIEventListener.Get(self.battleDataButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClickBattleDataButton(go) end)
    UIEventListener.Get(self.clsRankButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClickClsRankButton(go) end)
    self.FilterCls = false
    self.clsRankLabel.text = LocalString.GetString("本职业排行")

    UIEventListener.Get(self.shareButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClickShareButton(go) end)
    --渠道服只能分享至梦岛
    if CLuaGuanNingMgr.IsOnlyShare2PersonalSpace() then
        self.shareButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text=LocalString.GetString("分享至梦岛")
    end
    self.shareButton:SetActive(false)
end

function CLuaAnQiDaoResultWnd:Init( )
    --每次加载，需要重新请求一次，刷新界面
    self:Request()

    if CLuaGuanNingMgr.m_IsGameEnd then
        self.scoreLabel.text = SafeStringFormat3(LocalString.GetString("获得关宁积分：%d"), CLuaGuanNingMgr.m_Score or 0)
    else
        self.scoreLabel.text = ""
    end

    self:UpdateDoubleScoreShow()
    
    self:OnUpdatePlayerForceInfo()
end

function CLuaAnQiDaoResultWnd:UpdateDoubleScoreShow( )
    local label = self.requestDoubleScoreBtn.transform:Find("Label").gameObject:GetComponent(typeof(UILabel))
    if label ~= nil then
        local DoubleScoreTime = GuanNing_Setting.GetData().DoubleScoreTime
        local times = CLuaGuanNingMgr.m_LeftDoubleTimes
        label.text = SafeStringFormat3(LocalString.GetString("领取本周双倍(%d/%d)"),times,DoubleScoreTime)

        if times == 0 then
            CUICommonDef.SetActive(self.requestDoubleScoreBtn, false, true)
        else
            CUICommonDef.SetActive(self.requestDoubleScoreBtn, true, true)
        end
    end
    if self.m_IsTrainScene then
        self.requestDoubleScoreBtn:SetActive(false)
    end
    --游戏没结束 
    if not CLuaGuanNingMgr.m_IsGameEnd then
        CUICommonDef.SetActive(self.requestDoubleScoreBtn, false, true)
    end

    if CLuaGuanNingMgr.m_Score == 0 then
        CUICommonDef.SetActive(self.requestDoubleScoreBtn, false, true)
    end
end

function CLuaAnQiDaoResultWnd:OnClickShareButton( go) 
    CLuaGuanNingMgr.Share2Web(1, true)
end

function CLuaAnQiDaoResultWnd:OnClickBattleDataButton( go) 
    CLuaGuanNingMgr.ShowLastBattleData = true
    CUIManager.ShowUI(CUIResources.GuanNingBattleDataWnd)
end
function CLuaAnQiDaoResultWnd:OnClickClsRankButton( go) 
    --默认是全职业排行
    if not self.FilterCls then
        self.FilterCls = true
        self.clsRankLabel.text = LocalString.GetString("全职业排行")
        --过滤
        self:FilterMyCls()
    else
        self.FilterCls = false
        self.clsRankLabel.text = LocalString.GetString("本职业排行")
        self:FilterAllCls()
    end
    self.resultPane1:InitData(self.mHaicangPlayInfoList)
    self.resultPane2:InitData(self.mDafuPlayInfoList)
end
function CLuaAnQiDaoResultWnd:FilterAllCls( )
    -- CommonDefs.ListClear(self.mHaicangPlayInfoList)
    -- CommonDefs.ListClear(self.mDafuPlayInfoList)
    self.mHaicangPlayInfoList={}
    self.mDafuPlayInfoList={}

    
    for i,v in ipairs(CLuaAnQiDaoMgr.m_HaicangPlayInfoList) do
        table.insert( self.mHaicangPlayInfoList,v )
    end
    for i,v in ipairs(CLuaAnQiDaoMgr.m_DafuPlayInfoList) do
        table.insert( self.mDafuPlayInfoList,v )
    end
end
function CLuaAnQiDaoResultWnd:FilterMyCls( )
    self.mHaicangPlayInfoList={}
    self.mDafuPlayInfoList={}
    if CClientMainPlayer.Inst == nil then
        return
    end
    local cls = EnumToInt(CClientMainPlayer.Inst.Class)

    for i,v in ipairs(CLuaAnQiDaoMgr.m_HaicangPlayInfoList) do
        if v.cls==cls then
            table.insert( self.mHaicangPlayInfoList,v )
        end
    end
    
    for i,v in ipairs(CLuaAnQiDaoMgr.m_DafuPlayInfoList) do
        if v.cls==cls then
            table.insert( self.mDafuPlayInfoList,v )
        end
    end
end
function CLuaAnQiDaoResultWnd:RequestGetDoubleScore( go) 
    local msg = g_MessageMgr:FormatMessage("GuanNing_DoubleScore_Confirm")
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
        Gac2Gas.RequestGetDoubleScore()
        --self.Close();
    end), nil, nil, nil, false)
end
function CLuaAnQiDaoResultWnd:Request( )
    --查询结果
    Gac2Gas.QueryAnQiIslandPlayInfo()
    if CLuaGuanNingMgr.m_IsGameEnd then
        Gac2Gas.QueryGuanNingHistoryWeekData()
    end
end
function CLuaAnQiDaoResultWnd:OnEnable( )
    g_ScriptEvent:AddListener("QueryAnQiIslandPlayInfo", self, "OnQueryAnQiIslandPlayInfo")
    g_ScriptEvent:AddListener("UpdatePlayerAnQiIslandScore", self, "OnUpdatePlayerAnQiIslandScore")
    g_ScriptEvent:AddListener("GetGuanNingDoubleScoreSuccess", self, "OnGetGuanNingDoubleScoreSuccess")
    g_ScriptEvent:AddListener("UpdatePlayerForceInfo", self, "OnUpdatePlayerForceInfo")
end
function CLuaAnQiDaoResultWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("QueryAnQiIslandPlayInfo", self, "OnQueryAnQiIslandPlayInfo")
    g_ScriptEvent:RemoveListener("UpdatePlayerAnQiIslandScore", self, "OnUpdatePlayerAnQiIslandScore")
    g_ScriptEvent:RemoveListener("GetGuanNingDoubleScoreSuccess", self, "OnGetGuanNingDoubleScoreSuccess")
    g_ScriptEvent:RemoveListener("UpdatePlayerForceInfo", self, "OnUpdatePlayerForceInfo")
end
function CLuaAnQiDaoResultWnd:OnGetGuanNingDoubleScoreSuccess( leftDoubleTimes) 
    self:UpdateDoubleScoreShow()
    CUICommonDef.SetActive(self.requestDoubleScoreBtn, false, true)
end
function CLuaAnQiDaoResultWnd:OnUpdatePlayerForceInfo()
    local MainPlayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    if MainPlayerId ~= 0 then
        --CForcesMgr.Inst:GetPlayerForce在未分配阵营时会返回0,0代表defend
        local bFind, force = CommonDefs.DictTryGet_LuaCall(CForcesMgr.Inst.m_PlayForceInfo, MainPlayerId)
        if bFind == true then
            CLuaAnQiDaoMgr.m_MainPlayerForce = force
            self.haicangSelfNode:SetActive(CLuaAnQiDaoMgr.m_MainPlayerForce == 0)
            self.dafuSelfNode:SetActive(CLuaAnQiDaoMgr.m_MainPlayerForce ~= 0)
        else
            self.haicangSelfNode:SetActive(false)
            self.dafuSelfNode:SetActive(false)
        end
    end
end
function CLuaAnQiDaoResultWnd:OnUpdatePlayerAnQiIslandScore( score, expNum, yinPiao) 
    self.scoreLabel.text = SafeStringFormat3(LocalString.GetString("获得关宁积分：%d"), score)
    if score >= 400 then
        self:PlayEffect()
    end
    self:UpdateDoubleScoreShow()
end
function CLuaAnQiDaoResultWnd:OnQueryAnQiIslandPlayInfo( )
    if CClientMainPlayer.Inst == nil then
        return
    end
    self:UpdateDoubleScoreShow()
    if CLuaGuanNingMgr.m_IsGameEnd then
        self.haicangResultSprite.gameObject:SetActive(true)
        self.dafuResultSprite.gameObject:SetActive(true)
        self.battleDataButton:SetActive(true)
        self.shareButton:SetActive(true)

        if CLuaAnQiDaoMgr.m_HaicangHealth > CLuaAnQiDaoMgr.m_DafuHealth then
            --
            CUICommonDef.SetActive(self.haicangResultSprite.gameObject, true, true)
            CUICommonDef.SetActive(self.dafuResultSprite.gameObject, false, true)
            self.haicangResultSprite.spriteName = "common_fight_result_win"
            self.dafuResultSprite.spriteName = "common_fight_result_lose"
        elseif CLuaAnQiDaoMgr.m_HaicangHealth < CLuaAnQiDaoMgr.m_DafuHealth then
            CUICommonDef.SetActive(self.haicangResultSprite.gameObject, false, true)
            CUICommonDef.SetActive(self.dafuResultSprite.gameObject, true, true)
            self.haicangResultSprite.spriteName = "common_fight_result_lose"
            self.dafuResultSprite.spriteName = "common_fight_result_win"
        else
            --平局
            CUICommonDef.SetActive(self.haicangResultSprite.gameObject, true, true)
            CUICommonDef.SetActive(self.dafuResultSprite.gameObject, true, true)
            self.haicangResultSprite.spriteName = "common_fight_result_tie"
            self.dafuResultSprite.spriteName = "common_fight_result_tie"
        end
    end

    --训练场不显示按钮
    if self.m_IsTrainScene then
        self.requestDoubleScoreBtn:SetActive(false)
        self.scoreLabel.gameObject:SetActive(false)

        self.battleDataButton:SetActive(false)
        self.shareButton:SetActive(false)
    end

    if CLuaGuanNingMgr.lastBattleData == nil then
        self.battleDataButton:SetActive(false)
        self.shareButton:SetActive(false)
    end

    if CLuaAnQiDaoMgr.enableShare then
        if not CSwitchMgr.EnableGuanNingShare then
            self.shareButton:SetActive(false)
        end
    else
        self.shareButton:SetActive(false)
    end

    --先找到位置

    if self.FilterCls then
        self:FilterMyCls()
    else
        self:FilterAllCls()
    end

    self.resultPane1:InitData(self.mHaicangPlayInfoList)
    self.resultPane2:InitData(self.mDafuPlayInfoList)
    
    self.resultPane1:Scroll2MyRow()
    self.resultPane2:Scroll2MyRow()

    self.haicangHealthSlider.value = CLuaAnQiDaoMgr.m_HaicangHealth / 5
    self.dafuHealthSlider.value = CLuaAnQiDaoMgr.m_DafuHealth / 5
    self.haicangHealthFore.color = NGUIText.ParseColor24(self.mSliderColors[CLuaAnQiDaoMgr.m_HaicangHealth > 0 and CLuaAnQiDaoMgr.m_HaicangHealth or 1], 0)
    self.dafuHealthFore.color = NGUIText.ParseColor24(self.mSliderColors[CLuaAnQiDaoMgr.m_DafuHealth > 0 and CLuaAnQiDaoMgr.m_DafuHealth or 1], 0)
end


function CLuaAnQiDaoResultWnd:DoAni( bounds, isEllipsePath) 
    LuaTweenUtils.DOKill(self.uiFx.transform, false)
    
    local waypoints = CUICommonDef.GetWayPoints(bounds, 1, isEllipsePath)
    self.uiFx.transform.localPosition = waypoints[0]
    LuaTweenUtils.DOLuaLocalPath(self.uiFx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
end
function CLuaAnQiDaoResultWnd:PlayEffect( )
    self.uiFx:DestroyFx()
    self.uiFx:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
    self:DoAni(Vector4(-self.fxWidget.width * 0.5, self.fxWidget.height * 0.5, self.fxWidget.width, self.fxWidget.height), false)
end

function CLuaAnQiDaoResultWnd:OnDestroy()
    CLuaGuanNingMgr.m_PlayerServerNameCache = {}
end

return CLuaAnQiDaoResultWnd
