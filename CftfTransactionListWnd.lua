-- Auto Generated!!
local CftfTransactionListWnd = import "L10.UI.CftfTransactionListWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CTradeMgr = import "L10.Game.CTradeMgr"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CftfTransactionListWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.acceptBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.acceptBtn).onClick, MakeDelegateFromCSFunction(this.OnAcceptBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.rejectBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.rejectBtn).onClick, MakeDelegateFromCSFunction(this.OnRejectBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.clearBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.clearBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseBtnClick, VoidDelegate, this), false)

    CommonDefs.DictClear(CTradeMgr.Inst.tradeApplyList)
end
