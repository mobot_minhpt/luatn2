local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"
local QnRadioBox = import "L10.UI.QnRadioBox"
local EnumTempPlayDataKey = import "L10.Game.EnumTempPlayDataKey"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaYuanDan2022XuYuanWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYuanDan2022XuYuanWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaYuanDan2022XuYuanWnd, "XinYuanNameLabel", "XinYuanNameLabel", UILabel)
RegistChildComponent(LuaYuanDan2022XuYuanWnd, "XinYuanDescLabel", "XinYuanDescLabel", UILabel)
RegistChildComponent(LuaYuanDan2022XuYuanWnd, "RewardItemCell", "RewardItemCell", GameObject)
RegistChildComponent(LuaYuanDan2022XuYuanWnd, "RewardIconTexture", "RewardIconTexture", CUITexture)
RegistChildComponent(LuaYuanDan2022XuYuanWnd, "RewardQualitySprite", "RewardQualitySprite", UISprite)
RegistChildComponent(LuaYuanDan2022XuYuanWnd, "RewardNumLabel", "RewardNumLabel", UILabel)
RegistChildComponent(LuaYuanDan2022XuYuanWnd, "CostItemNumLabel", "CostItemNumLabel", UILabel)
RegistChildComponent(LuaYuanDan2022XuYuanWnd, "FinishedSign", "FinishedSign", GameObject)
RegistChildComponent(LuaYuanDan2022XuYuanWnd, "QnRadioBox", "QnRadioBox", QnRadioBox)
RegistChildComponent(LuaYuanDan2022XuYuanWnd, "GetRewardSign", "GetRewardSign", GameObject)
RegistChildComponent(LuaYuanDan2022XuYuanWnd, "CostNumLabel", "CostNumLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaYuanDan2022XuYuanWnd,"m_DataList")
RegistClassMember(LuaYuanDan2022XuYuanWnd,"m_SelectIndex")
RegistClassMember(LuaYuanDan2022XuYuanWnd,"m_ItemRewardItemId")
RegistClassMember(LuaYuanDan2022XuYuanWnd,"m_NeedItemId")
RegistClassMember(LuaYuanDan2022XuYuanWnd,"m_XinYuanData")
function LuaYuanDan2022XuYuanWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)

	UIEventListener.Get(self.RewardItemCell.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRewardItemCellClick()
	end)
    --@endregion EventBind end
end

function LuaYuanDan2022XuYuanWnd:Init()
	self:InitXinYuanData()
	self.QnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
		self:OnSelect(btn, index)
	end)
	self.m_DataList = {}
	YuanDan2022_XinYuan.ForeachKey(function(key)
		table.insert(self.m_DataList,YuanDan2022_XinYuan.GetData(key))
	end)
	for i = 0, self.QnRadioBox.m_RadioButtons.Length - 1 do
		local index = i + 1
		local btn = self.QnRadioBox.m_RadioButtons[i]
		self:InitRadioButton(btn, index)
	end
	self.QnRadioBox:ChangeTo(0, true)
end

function LuaYuanDan2022XuYuanWnd:InitXinYuanData()
	if CClientMainPlayer.Inst and CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(uint), EnumToInt(EnumTempPlayDataKey.eYuanDan2022XinYuanData)) then
		local data = CClientMainPlayer.Inst.PlayProp.TempPlayData[EnumToInt(EnumTempPlayDataKey.eYuanDan2022XinYuanData)]
		local dict = MsgPackImpl.unpack(data.Data.Data)
		self.m_XinYuanData = {}
		if CommonDefs.IsList(dict) then
			for i = 0,dict.Count - 1 do
				table.insert(self.m_XinYuanData,dict[i])
			end
		elseif CommonDefs.IsDic(dict) then
			CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function(key,val)
				self.m_XinYuanData[tonumber(key)] = val
			end))
		end
	end
end

function LuaYuanDan2022XuYuanWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "SendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
	g_ScriptEvent:AddListener("OnGetYuanDanXinYuanRewardsSuccess", self, "OnGetYuanDanXinYuanRewardsSuccess")
end

function LuaYuanDan2022XuYuanWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "SendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
	g_ScriptEvent:RemoveListener("OnGetYuanDanXinYuanRewardsSuccess", self, "OnGetYuanDanXinYuanRewardsSuccess")
end

function LuaYuanDan2022XuYuanWnd:SendItem(args)
	local itemId = args[0]
	local item = CItemMgr.Inst:GetById(itemId)
    if item then
        local templateId = item.TemplateId
		if templateId == self.m_NeedItemId then
			self:InitCostItemNumLabel()
		end
	end
end

function LuaYuanDan2022XuYuanWnd:OnSetItemAt(args)
	local  place, position, oldItemId, newItemId = args[0],args[1],args[2],args[3]
	local item = CItemMgr.Inst:GetById( newItemId)
    if item then
        local templateId = item.TemplateId
		if templateId == self.m_NeedItemId then
			self:InitCostItemNumLabel()
		end
	end
end

function LuaYuanDan2022XuYuanWnd:OnGetYuanDanXinYuanRewardsSuccess(index)
	if not self.m_XinYuanData then self.m_XinYuanData = {} end
	self.m_XinYuanData[index] = 1
	local btn = self.QnRadioBox.m_RadioButtons[index - 1]
	local fx =  btn.transform:Find("Fx"):GetComponent(typeof(CUIFx))
	fx:LoadFx("fx/ui/prefab/UI_dagongshouce_shengji.prefab")
	self:InitRadioButton(btn, index)
	if self.m_SelectIndex == index then
		self.QnRadioBox:ChangeTo(index - 1, true)
	end
end

function LuaYuanDan2022XuYuanWnd:InitRadioButton(btn,index)
	local data = self.m_DataList[index]
	local nameLabel = btn.transform:Find("XinYuanNameLabel"):GetComponent(typeof(UILabel))
	local highlight = btn.transform:Find("Highlight").gameObject
	local deng = btn.transform:Find("Deng").gameObject
	-- local tween =  btn:GetComponent(typeof(TweenPosition))
	nameLabel.text = data.Down
	-- tween.from = Vector3(btn.transform.localPosition.x,btn.transform.localPosition.y - 20,btn.transform.localPosition.z)
	-- tween.to = Vector3(btn.transform.localPosition.x,btn.transform.localPosition.y + 20,btn.transform.localPosition.z)
	local isGetReward = self:IsGetReward(index)
	highlight.gameObject:SetActive(isGetReward)
	deng.gameObject:SetActive(not isGetReward)
	-- tween.enabled = isGetReward
end

function LuaYuanDan2022XuYuanWnd:IsGetReward(index)
	return self.m_XinYuanData and self.m_XinYuanData[index]
end
--@region UIEvent

function LuaYuanDan2022XuYuanWnd:OnButtonClick()
	Gac2Gas.GetYuanDan2022XinYuanRewards(self.m_SelectIndex)
end

function LuaYuanDan2022XuYuanWnd:OnRewardItemCellClick()
	if self.m_ItemRewardItemId then
		CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_ItemRewardItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
	end
end

function LuaYuanDan2022XuYuanWnd:OnSelect(btn, index)
	self.m_SelectIndex = index + 1
	local data = self.m_DataList[self.m_SelectIndex]

	local itemReward = data.ItemRewards
	local itemRewardItemId,num = tonumber(data.ItemRewards[0][0]),tonumber(data.ItemRewards[0][1])
	self.m_ItemRewardItemId = itemRewardItemId
	local itemRewardData = Item_Item.GetData(itemRewardItemId)
	self.m_NeedItemId = data.NeedItem
	local isGetReward = self:IsGetReward(self.m_SelectIndex)

	self.XinYuanNameLabel.text = data.Name
	self.XinYuanDescLabel.text = data.Desc
	self.RewardIconTexture:LoadMaterial(itemRewardData.Icon)
	self.RewardQualitySprite.spriteName = CUICommonDef.GetItemCellBorder(itemRewardData, nil, false)
	self.RewardNumLabel.text = num
	self.FinishedSign:SetActive(isGetReward)
	self.GetRewardSign:SetActive(isGetReward)
	self.Button:SetActive(not isGetReward)
	self.CostNumLabel.text = SafeStringFormat3("x%d",data.NeedItemCount)
	self:InitCostItemNumLabel()
end
--@endregion UIEvent

function LuaYuanDan2022XuYuanWnd:InitCostItemNumLabel()
	local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_NeedItemId)
	self.CostItemNumLabel.text = SafeStringFormat3("x%d",count)
end