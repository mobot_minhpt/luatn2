local CUITexture = import "L10.UI.CUITexture"
local BabyMgr = import "L10.Game.BabyMgr"
local Baby_Fashion = import "L10.Game.Baby_Fashion"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CClientObject = import "L10.Game.CClientObject"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UIGrid = import "UIGrid"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local QnButton = import "L10.UI.QnButton"
local NGUITools = import "NGUITools"
local QnLabel = import "L10.UI.QnLabel"
local CItemMgr = import "L10.Game.CItemMgr"
local CItem = import "L10.Game.CItem"
local CEquipment = import "L10.Game.CEquipment"
local IdPartition = import "L10.Game.IdPartition"
local Time = import "UnityEngine.Time"
local Screen = import "UnityEngine.Screen"

LuaShopMallBabyFashionPreviewWnd = class()

RegistChildComponent(LuaShopMallBabyFashionPreviewWnd, "RotateLeftButton", GameObject)
RegistChildComponent(LuaShopMallBabyFashionPreviewWnd, "RotateRightButton", GameObject)
RegistChildComponent(LuaShopMallBabyFashionPreviewWnd, "Texture", CUITexture)
RegistChildComponent(LuaShopMallBabyFashionPreviewWnd, "QnModelPreviewer", GameObject)

RegistChildComponent(LuaShopMallBabyFashionPreviewWnd, "ShoppingMallItem", GameObject)
RegistChildComponent(LuaShopMallBabyFashionPreviewWnd, "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaShopMallBabyFashionPreviewWnd, "Grid", UIGrid)

RegistChildComponent(LuaShopMallBabyFashionPreviewWnd, "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaShopMallBabyFashionPreviewWnd, "BuyButton", QnButton)
RegistChildComponent(LuaShopMallBabyFashionPreviewWnd, "TakeOffButton", GameObject)

-- ModelTextureLoader
RegistClassMember(LuaShopMallBabyFashionPreviewWnd, "ModelTextureName")
RegistClassMember(LuaShopMallBabyFashionPreviewWnd, "ModelTextureLoader")
RegistClassMember(LuaShopMallBabyFashionPreviewWnd, "SelectedBaby")

RegistClassMember(LuaShopMallBabyFashionPreviewWnd, "SelectedHeadFashionId") -- itemId, not baby_fashion id
RegistClassMember(LuaShopMallBabyFashionPreviewWnd, "SelectedBodyFashionId") -- itemId, not baby_fashion id
RegistClassMember(LuaShopMallBabyFashionPreviewWnd, "SelectedFashionId") -- itemId, not baby_fashion id

-- 时装
RegistClassMember(LuaShopMallBabyFashionPreviewWnd, "BabyFashionList") --table of ShopMallTemlate
RegistClassMember(LuaShopMallBabyFashionPreviewWnd, "BabyFashionItemList") -- table of <itemId, fahsion item>


-- 旋转相关
RegistClassMember(LuaShopMallBabyFashionPreviewWnd, "Rotation")
RegistClassMember(LuaShopMallBabyFashionPreviewWnd, "DefaultRotation")
RegistClassMember(LuaShopMallBabyFashionPreviewWnd, "LeftPressed")
RegistClassMember(LuaShopMallBabyFashionPreviewWnd, "RightPressed")
RegistClassMember(LuaShopMallBabyFashionPreviewWnd, "StartPressTime")
RegistClassMember(LuaShopMallBabyFashionPreviewWnd, "DeltaTime")
RegistClassMember(LuaShopMallBabyFashionPreviewWnd, "DeltaRot")
RegistClassMember(LuaShopMallBabyFashionPreviewWnd, "Scale")


function LuaShopMallBabyFashionPreviewWnd:Init()
    if not LuaBabyMgr.m_PreviewFashionBaby then CUIManager.CloseUI(CLuaUIResources.ShopMallBabyFashionPreviewWnd) return end
    self.SelectedBaby = LuaBabyMgr.m_PreviewFashionBaby
    local fashion = Baby_Fashion.GetData(LuaBabyMgr.m_PreviewFashinId)
    if not fashion then CUIManager.CloseUI(CLuaUIResources.ShopMallBabyFashionPreviewWnd) return end

    self.SelectedFashionId = LuaBabyMgr.m_PreviewFashionItemId
    if self:IsHeadFashion(fashion) then
        self.SelectedHeadFashionId = LuaBabyMgr.m_PreviewFashionItemId
        self.SelectedBodyFashionId = 0
    else
        self.SelectedBodyFashionId = LuaBabyMgr.m_PreviewFashionItemId
        self.SelectedHeadFashionId = 0
    end
    LuaBabyMgr.m_PreviewFashionBaby = nil
    self.ModelTextureName = SafeStringFormat3("__%s__", tostring(self.gameObject:GetInstanceID()))

    self.ShoppingMallItem:SetActive(false)
    self:InitParams()
    self:InitModel()
    self:InitBabyFashions()
end

function LuaShopMallBabyFashionPreviewWnd:InitParams()
    self.Rotation = 180
	self.DefaultRotation = 180
	self.LeftPressed = false
	self.RightPressed = false
	self.StartPressTime = 0
	self.DeltaTime = 0.025
    self.DeltaRot = -5
    self.Scale = 1

    CommonDefs.AddOnPressListener(self.RotateLeftButton, DelegateFactory.Action_GameObject_bool(function (go, isPressed)
		self:OnRotateLeftButtonPressed(go, isPressed)
	end), true)

	CommonDefs.AddOnPressListener(self.RotateRightButton, DelegateFactory.Action_GameObject_bool(function (go, isPressed)
		self:OnRotateRightButtonPressed(go, isPressed)
    end), true)
    
    CommonDefs.AddOnDragListener(self.QnModelPreviewer, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
		self:OnScreenDrag(go, delta)
	end), false)

	CommonDefs.AddOnClickListener(self.TakeOffButton, DelegateFactory.Action_GameObject(function (go)
		self:OnTakeOffButtonClicked(go)
    end), false)

    CommonDefs.AddOnClickListener(self.BuyButton.gameObject, DelegateFactory.Action_GameObject(function (go)
		self:OnBuyButtonClicked(go)
    end), false)
end


function LuaShopMallBabyFashionPreviewWnd:OnRotateLeftButtonPressed(go, isPressed)
    self.LeftPressed = isPressed
end

function LuaShopMallBabyFashionPreviewWnd:OnRotateRightButtonPressed(go, isPressed)
	self.RightPressed = isPressed
end

function LuaShopMallBabyFashionPreviewWnd:OnScreenDrag(go, delta)
    local rot = -delta.x / Screen.width * 360
	self.Rotation = self.Rotation + rot
	CUIManager.SetModelRotation(self.ModelTextureName, self.Rotation)
end

function LuaShopMallBabyFashionPreviewWnd:OnTakeOffButtonClicked(go)
    self.SelectedFashionId = 0
    self.SelectedHeadFashionId = 0
    self.SelectedBodyFashionId = 0
    self:UpdateBabyFashion()
    self:SetShoppingMallItemsSelected()
end

function LuaShopMallBabyFashionPreviewWnd:OnBuyButtonClicked(go)
    if self.SelectedFashionId == 0 then

        return
    end
    local item = self:GetShopppingMallFashionByItemId(self.SelectedFashionId)
    if not item then return end
    if CShopMallMgr.TryCheckEnoughJade(item.Price, self.SelectedFashionId) then
        Gac2Gas.BuyMallItem(EShopMallRegion_lua.ELingyuMall, item.ItemId, 1)
    end
end

function LuaShopMallBabyFashionPreviewWnd:IsHeadFashion(fashion)
    return fashion.Position == 0
end

function LuaShopMallBabyFashionPreviewWnd:Update()
	if self.LeftPressed and Time.realtimeSinceStartup - self.StartPressTime > self.DeltaTime then
		self.Rotation = self.Rotation + self.DeltaRot
		CUIManager.SetModelRotation(self.ModelTextureName, self.Rotation)
		self.StartPressTime = Time.realtimeSinceStartup
	elseif self.RightPressed and Time.realtimeSinceStartup - self.StartPressTime > self.DeltaTime then
		self.Rotation = self.Rotation - self.DeltaRot
		CUIManager.SetModelRotation(self.ModelTextureName, self.Rotation)
		self.StartPressTime = Time.realtimeSinceStartup
	end
end

function LuaShopMallBabyFashionPreviewWnd:InitModel()
    self.ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
		self:LoadModel(ro)
    end)
    self:UpdateBabyFashion()
end

function LuaShopMallBabyFashionPreviewWnd:LoadModel(ro)
    local appear = self:GetBabyPreviewAppearance()
	CClientObject.LoadBabyRenderObject(ro, appear, true, 0, 0)
end

function LuaShopMallBabyFashionPreviewWnd:UpdateBabyFashion()
    local tx = CUIManager.CreateModelTexture(self.ModelTextureName, self.ModelTextureLoader, 180, 0.05, -1, 4.66, false, true, 1.6, false)
	self.Texture.mainTexture = tx
end


function LuaShopMallBabyFashionPreviewWnd:GetBabyPreviewAppearance()
    local realAppearance = BabyMgr.Inst:ToBabyAppearance(self.SelectedBaby)
    local headFashion = self:GetBabyFashionByItemId(self.SelectedHeadFashionId)
	local bodyFashion = self:GetBabyFashionByItemId(self.SelectedBodyFashionId)

    if headFashion then
        realAppearance.FashionHeadId = headFashion.ID
    end

    if bodyFashion then
        realAppearance.FashionBodyId = bodyFashion.ID
        realAppearance.ColorId = 0 -- 清除染色信息
    end
    realAppearance.BodyWeight = 8
	return realAppearance
end

function LuaShopMallBabyFashionPreviewWnd:GetBabyFashionByItemId(itemId)
    local fashion = nil
    Baby_Fashion.ForeachKey(DelegateFactory.Action_object(function (key)
        local data = Baby_Fashion.GetData(key)
        if data.ItemID == itemId then
            fashion = data
        end
    end))
    return fashion
end


function LuaShopMallBabyFashionPreviewWnd:InitBabyFashions()
    -- step 1 获得所有时装
    self.BabyFashionList = {}
    local list = CShopMallMgr.GetLingYuMallInfo(3, 2)

    -- 增加学童装
    local tempList = CShopMallMgr.GetLingYuMallInfo(2, 8)
    for i = 0, tempList.Count-1 do
        if tempList[i].ItemId == 21040308 or tempList[i].ItemId == 21040309 or tempList[i].ItemId == 21031584 then
            list:Add(tempList[i])
        end
    end

    -- step 2 是否是宝宝时装，阶段是否符合，性别是否符合
    for i = 0, list.Count-1 do
        local isSuitable = self:IsSuitableForSelectedBaby(list[i].ItemId)
        if isSuitable then
            table.insert(self.BabyFashionList, list[i])
        end
    end

    self:UpdateFashionList()
end



function LuaShopMallBabyFashionPreviewWnd:IsSuitableForSelectedBaby(templateId)
    local bFoundBabyFashion = false
    Baby_Fashion.ForeachKey(DelegateFactory.Action_object(function (key)
        local fashion = Baby_Fashion.GetData(key)
        if fashion.ItemID == templateId then
            if fashion.BabyStage == 3 or self.SelectedBaby.Props.ExtraData.ShowStatus >= EnumBabyStatus.eChild then
                bFoundBabyFashion = true
            elseif self.SelectedBaby.Props.ExtraData.ShowStatus == (fashion.BabyStage+1) then
                bFoundBabyFashion = true
            end
        end
     end))
     return bFoundBabyFashion
end

function LuaShopMallBabyFashionPreviewWnd:UpdateFashionList()
    self.BabyFashionItemList = {}
    Extensions.RemoveAllChildren(self.Grid.transform)
    
    if not self.BabyFashionList or #self.BabyFashionList <= 0 then return end

    for i = 1, #self.BabyFashionList do
        local go = NGUITools.AddChild(self.Grid.gameObject, self.ShoppingMallItem)
        self:InitShoppingMallItem(go, self.BabyFashionList[i])
        go:SetActive(true)
        self.BabyFashionItemList[self.BabyFashionList[i].ItemId] = go
        local templateId = self.BabyFashionList[i].ItemId
        go:GetComponent(typeof(QnButton)).OnClick = DelegateFactory.Action_QnButton(function (qButton)
            self:ShoppingMallItemClicked(qButton, templateId)
            self:UpdateBabyFashion()
        end)
    end

    self.Grid:Reposition()
    self.ScrollView:ResetPosition()
    self:SetShoppingMallItemsSelected()
end

function LuaShopMallBabyFashionPreviewWnd:InitShoppingMallItem(go, info)
    if not go or not info then return end

    local owner = go.transform:Find("Owner").gameObject
    local texture = go.transform:Find("Texture"):GetComponent(typeof(CUITexture))
    local previewTag = go.transform:Find("PreviewTag").gameObject
    local name = go.transform:Find("Name"):GetComponent(typeof(QnLabel))
    local priceLabel = go.transform:Find("Price"):GetComponent(typeof(QnLabel))
    local discountLabel = go.transform:Find("Discount"):GetComponent(typeof(QnLabel))
    local disableSprite = go.transform:Find("Texture/DisableSprite"):GetComponent(typeof(UISprite))

    local item = CItemMgr.Inst:GetItemTemplate(info.ItemId)

    texture:LoadMaterial(item.Icon)
    name.Text = item.Name
    priceLabel.Text = tostring(info.Price)

    previewTag:SetActive(self.SelectedBodyFashionId == info.ItemId or self.SelectedHeadFashionId == info.ItemId)

    local showDiscount = info.Discount
    local price = info.Price
    local discountInfo
    if info.Discount > 0.05 and not info.PreSell then
        if CommonDefs.IS_VN_CLIENT then
            showDiscount = showDiscount * 10
            discountInfo = String.Format(LocalString.GetString("{0:N1}折"), showDiscount)
        elseif CommonDefs.IS_KR_CLIENT then
            showDiscount = showDiscount * 10
            discountInfo = String.Format("{0:N1}%", showDiscount)
        else
            discountInfo = String.Format(LocalString.GetString("{0:N1}折"), showDiscount)
        end
    end
    if not String.IsNullOrEmpty(discountInfo) then
        discountLabel.Text = discountInfo
        discountLabel.gameObject:SetActive(true)
    else
        discountLabel.gameObject:SetActive(false)
    end

    disableSprite.enabled = false
        if IdPartition.IdIsItem(info.ItemId) then
            disableSprite.enabled = not CItem.GetMainPlayerIsFit(info.ItemId, true)
        elseif IdPartition.IdIsEquip(info.ItemId) then
            disableSprite.enabled = not CEquipment.GetMainPlayerIsFit(info.ItemId, true)
        end
end


function LuaShopMallBabyFashionPreviewWnd:ShoppingMallItemClicked(qButton, templateId)
   local fashion = self:GetBabyFashionByItemId(templateId)
   if not fashion then return end

    if self:IsHeadFashion(fashion) then
        if self.SelectedHeadFashionId == templateId then
            self.SelectedHeadFashionId = 0
            self.SelectedFashionId = self.SelectedBodyFashionId
            -- 取消选中，并尝试跳转到body
            self:SetShoppingMallItemsSelected()
        else
            self.SelectedHeadFashionId = templateId
            self.SelectedFashionId = templateId
            -- 选中
            self:SetShoppingMallItemsSelected()
        end
    else
        if self.SelectedBodyFashionId == templateId then
            self.SelectedBodyFashionId = 0
            self.SelectedFashionId = self.SelectedHeadFashionId
            self:SetShoppingMallItemsSelected()
        else
            self.SelectedBodyFashionId = templateId
            self.SelectedFashionId = templateId
            self:SetShoppingMallItemsSelected()
        end
    end
end

function LuaShopMallBabyFashionPreviewWnd:UpdateCost()
    local cost = 0
    if self.SelectedFashionId ~= 0 then
        local selectedFashion = self:GetShopppingMallFashionByItemId(self.SelectedFashionId)
        if selectedFashion then
            cost = selectedFashion.Price
        end
    end
    self.QnCostAndOwnMoney:SetCost(cost)
end

function LuaShopMallBabyFashionPreviewWnd:GetShopppingMallFashionByItemId(itemId)
    if not self.BabyFashionList then return nil end
    for i = 1, #self.BabyFashionList do
        if self.BabyFashionList[i].ItemId == itemId then
            return self.BabyFashionList[i]
        end
    end
    return nil
end

function LuaShopMallBabyFashionPreviewWnd:SetShoppingMallItemsSelected()
    for i, v in pairs(self.BabyFashionItemList) do
        self:SetItemSelected(v, i == self.SelectedFashionId)
        self:SetItemPreviewed(v, i == self.SelectedBodyFashionId or i == self.SelectedHeadFashionId)
    end
    self:UpdateCost()
end

function LuaShopMallBabyFashionPreviewWnd:SetItemSelected(go, isSelected)
    go:GetComponent(typeof(QnButton)).m_BackGround.spriteName = isSelected and "common_btn_07_highlight" or "common_btn_07_normal"
end

function LuaShopMallBabyFashionPreviewWnd:SetItemPreviewed(go, isPreviewed)
    -- todo
    local previewTag = go.transform:Find("PreviewTag").gameObject
    previewTag:SetActive(isPreviewed)
end

function LuaShopMallBabyFashionPreviewWnd:OnEnable()
    -- body
end

function LuaShopMallBabyFashionPreviewWnd:OnDisable()
    self.Texture.mainTexture = nil
    CUIManager.DestroyModelTexture(self.ModelTextureName)
end
