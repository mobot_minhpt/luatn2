-- Auto Generated!!
local CFoeListMgr = import "L10.UI.CFoeListMgr"
local CFoeSelectPlayerTemplate = import "L10.UI.CFoeSelectPlayerTemplate"
local CFoeSelectWnd = import "L10.UI.CFoeSelectWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUITools = import "NGUITools"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CFoeSelectWnd.m_Init_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.buttonList)
    Extensions.RemoveAllChildren(this.table.transform)
    this.nameTemplate:SetActive(false)
    this.selectIndex = - 1

    do
        local i = 0
        while i < CFoeListMgr.foeList.Count do
            local instance = NGUITools.AddChild(this.table.gameObject, this.nameTemplate)
            instance:SetActive(true)
            local button = CommonDefs.GetComponent_GameObject_Type(instance, typeof(QnSelectableButton))
            if button ~= nil then
                button:SetSelected(false, false)
            end
            local player = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CFoeSelectPlayerTemplate))
            if player ~= nil then
                player:Init(CFoeListMgr.foeList[i].Name, CFoeListMgr.foeList[i].ID)
            end

            CommonDefs.ListAdd(this.buttonList, typeof(GameObject), instance)
            UIEventListener.Get(instance).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(instance).onClick, MakeDelegateFromCSFunction(this.OnQnButtonSelect, VoidDelegate, this), true)
            i = i + 1
        end
    end
end
CFoeSelectWnd.m_OnQnButtonSelect_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.buttonList.Count do
            if go == this.buttonList[i] then
                local button = CommonDefs.GetComponent_GameObject_Type(go, typeof(QnSelectableButton))
                local hun = CommonDefs.GetComponentInChildren_GameObject_Type(go, typeof(CFoeSelectPlayerTemplate))
                if hun ~= nil and hun.isGhost then
                    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ALREADY_HUNFEIPOSAN"), DelegateFactory.Action(function () 
                        this:SetTarget(i)
                    end), DelegateFactory.Action(function () 
                        if button ~= nil then
                            button:SetSelected(false, false)
                        end
                    end), nil, nil, false)
                    break
                else
                    this:SetTarget(i)
                    break
                end
            end
            i = i + 1
        end
    end
end
