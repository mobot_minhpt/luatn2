-- Auto Generated!!
local CPlayerShop_DeletePlace = import "L10.UI.CPlayerShop_DeletePlace"
local CPlayerShopData = import "L10.UI.CPlayerShopData"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local LocalString = import "LocalString"
local QnButton = import "L10.UI.QnButton"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CPlayerShop_DeletePlace.m_Init_CS2LuaHook = function (this) 
    local count = this:GetCanDeleteCount()
    this.m_DeleteLabel.text = System.String.Format(LocalString.GetString("请选择删除的格子数(当前共[00ff00]{0}[-]个格子可删除）"), count)
    if count > 0 then
        this.m_DeleteCountButton:SetMinMax(1, count, 1)
        this.m_DeleteCountButton:SetValue(1, true)
    else
        this.m_DeleteCountButton:SetMinMax(0, 0, 1)
        this.m_DeleteCountButton:SetValue(0, true)
    end
end
CPlayerShop_DeletePlace.m_OnEnable_CS2LuaHook = function (this) 
    this.m_DeleteCountButton.onValueChanged = MakeDelegateFromCSFunction(this.OnDeleteCountChanged, MakeGenericClass(Action1, UInt32), this)
    this.m_OkButton.OnClick = MakeDelegateFromCSFunction(this.OnOkButtonClick, MakeGenericClass(Action1, QnButton), this)
    this.m_CancelButton.OnClick = MakeDelegateFromCSFunction(this.OnCancelButtonClick, MakeGenericClass(Action1, QnButton), this)
    UIEventListener.Get(this.m_CloseButton).onClick = MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this)
end
CPlayerShop_DeletePlace.m_OnDisable_CS2LuaHook = function (this) 
    this.m_DeleteCountButton.onValueChanged = nil
    this.m_OkButton.OnClick = nil
    this.m_CancelButton.OnClick = nil
    UIEventListener.Get(this.m_CloseButton).onClick = nil
end
CPlayerShop_DeletePlace.m_OnDeleteCountChanged_CS2LuaHook = function (this, v) 
    local leftCount = 0
    if CPlayerShopData.Main.MaxShelfCount > v then
        leftCount = math.floor(tonumber(CPlayerShopData.Main.MaxShelfCount - v or 0))
    end
    local fund = math.floor(CPlayerShopData.Main.ActiveFund * leftCount / CPlayerShopData.Main.MaxShelfCount)
    this.m_OperatingFundLabel.text = tostring(fund)
end
CPlayerShop_DeletePlace.m_GetCanDeleteCount_CS2LuaHook = function (this) 
    local spaceCount = 0
    if CPlayerShopData.Main.MaxShelfCount > CPlayerShopData.Main.CurrentShelfCount then
        spaceCount = math.floor(tonumber(CPlayerShopData.Main.MaxShelfCount - CPlayerShopData.Main.CurrentShelfCount or 0))
    end
    local maxDeleteCount = 0
    if CPlayerShopData.Main.MaxShelfCount > CPlayerShopMgr.Counter_Num_Min then
        maxDeleteCount = math.floor(tonumber(CPlayerShopData.Main.MaxShelfCount - CPlayerShopMgr.Counter_Num_Min or 0))
    end
    return math.min(spaceCount, maxDeleteCount)
end
