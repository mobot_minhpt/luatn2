local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local CRankData=import "L10.UI.CRankData"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"
local Profession = import "L10.Game.Profession"

LuaFruitPartyRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaFruitPartyRankWnd, "m_MyRankLabel", "MyRankLabel", UILabel)
RegistChildComponent(LuaFruitPartyRankWnd, "m_MyCareerIcon", "MyCareerIcon", UISprite)
RegistChildComponent(LuaFruitPartyRankWnd, "m_MyNameLabel", "MyNameLabel", UILabel)
RegistChildComponent(LuaFruitPartyRankWnd, "m_MyMaxComboLabel", "MyMaxComboLabel", UILabel)
RegistChildComponent(LuaFruitPartyRankWnd, "m_MyMaxScoreLabel", "MyMaxScoreLabel", UILabel)
RegistChildComponent(LuaFruitPartyRankWnd, "m_TableView", "TableView", QnTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaFruitPartyRankWnd, "m_RankList")

function LuaFruitPartyRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaFruitPartyRankWnd:Init()
    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text = CClientMainPlayer.Inst.Name
    end
    self.m_RankList={}
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RankList
        end,
        function(item,index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

            self:InitItem(item,index,self.m_RankList[index+1])
        end
    )
    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local data = self.m_RankList[row+1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
        end
    end)

    Gac2Gas.QueryRank(41000211)
end

--@region UIEvent

--@endregion UIEvent

function LuaFruitPartyRankWnd:InitItem(item,index,info)
    local tf=item.transform
    local maxComboLabel=tf:Find("MaxComboLabel"):GetComponent(typeof(UILabel))
    maxComboLabel.text=""
    local maxScoreLabel=tf:Find("MaxScoreLabel"):GetComponent(typeof(UILabel))
    maxScoreLabel.text=""
    local iconSprite=tf:Find("CareerIcon"):GetComponent(typeof(UISprite))
    iconSprite.spriteName=""
    local nameLabel=tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text=""
    local rankLabel=tf:Find("RankLabel"):GetComponent(typeof(UILabel))
    rankLabel.text=""
    local rankSprite=tf:Find("RankLabel/RankImage"):GetComponent(typeof(UISprite))
    rankSprite.spriteName=""
    local rank=info.Rank
    if rank==1 then
        rankSprite.spriteName="Rank_No.1"
    elseif rank==2 then
        rankSprite.spriteName="Rank_No.2png"
    elseif rank==3 then
        rankSprite.spriteName="Rank_No.3png"
    else
        rankLabel.text=tostring(rank)
    end
    
    iconSprite.spriteName = Profession.GetIcon(info.Job)
    nameLabel.text = info.Name
    maxComboLabel.text = info.Value
    maxScoreLabel.text = info.Value
end

function LuaFruitPartyRankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaFruitPartyRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaFruitPartyRankWnd:OnRankDataReady()
    if CLuaRankData.m_CurRankId ~= 41000211 then return end
    local myInfo = CRankData.Inst.MainPlayerRankInfo
    self.m_MyCareerIcon.spriteName =  Profession.GetIcon(CClientMainPlayer.Inst.Class)
    self.m_MyRankLabel.text = myInfo.Rank > 0 and myInfo.Rank or LocalString.GetString("未上榜")
    self.m_MyNameLabel.text = myInfo.Name
    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text = CClientMainPlayer.Inst.Name
    end
    
    -- TODO
    self.m_MyMaxComboLabel.text = myInfo.Value > 0 and myInfo.Value or 0
    self.m_MyMaxScoreLabel.text = myInfo.Value > 0 and myInfo.Value or 0
    
    self.m_RankList = {}
    for i=1,CRankData.Inst.RankList.Count do
        table.insert(self.m_RankList, CRankData.Inst.RankList[i-1])
    end
    self.m_TableView:ReloadData(true,false)
end
