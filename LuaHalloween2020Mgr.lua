local CScene = import "L10.Game.CScene"
local MsgPackImpl = import "MsgPackImpl"

CLuaHalloween2020Mgr = {}

CLuaHalloween2020Mgr.playerForce = nil
CLuaHalloween2020Mgr.winForce = nil
CLuaHalloween2020Mgr.attackInfo = {}
CLuaHalloween2020Mgr.defendInfo = {}

function CLuaHalloween2020Mgr.Halloween2020ArrestPlaySignUpResult(bSuccess)


    if bSuccess == true then
        CUIManager.ShowUI(CLuaUIResources.MengGuiJiePiPeiWnd)
    end
end

function CLuaHalloween2020Mgr.Halloween2020ArrestPlayCancelSignUpResult(bSuccess)


    if bSuccess == true then
        CUIManager.CloseUI(CLuaUIResources.MengGuiJiePiPeiWnd)
    end
end

function CLuaHalloween2020Mgr.QueryHalloween2020ArrestPlayMatchInfoResult(bInMatching)
    if bInMatching == true then
        CUIManager.ShowUI(CLuaUIResources.MengGuiJiePiPeiWnd)
    end
end

function CLuaHalloween2020Mgr.QueryHalloween2020ArrestPlayInfoResult(winForce, attackData, defendData)

    
    CLuaHalloween2020Mgr.winForce = winForce
    
    CLuaHalloween2020Mgr.attackInfo = {}
	local list = MsgPackImpl.unpack(attackData)
    for i=0, list.Count-1, 5 do
    	local playerId = list[i]
    	local playerName = list[i+1]
    	local baoxiangNum = list[i+2]
    	local lingdouNum = list[i+3]
        local killNum = list[i+4]
        
        table.insert(CLuaHalloween2020Mgr.attackInfo, {playerId = playerId, playerName = playerName, baoxiangNum = baoxiangNum, lingdouNum = lingdouNum, killNum = killNum})

    end

    CLuaHalloween2020Mgr.defendInfo = {}
	local list = MsgPackImpl.unpack(defendData)
    for i=0, list.Count-1, 5 do
    	local playerId = list[i]
    	local playerName = list[i+1]
    	local baoxiangNum = list[i+2]
    	local lingdouNum = list[i+3]
        local killNum = list[i+4]
        
        table.insert(CLuaHalloween2020Mgr.defendInfo, {playerId = playerId, playerName = playerName, baoxiangNum = baoxiangNum, lingdouNum = lingdouNum, killNum = killNum})

    end

    CUIManager.ShowUI(CLuaUIResources.MengGuiJieResultWnd)
end

function CLuaHalloween2020Mgr.EnterHalloween2020ArrestPlay(force)

    
    CLuaHalloween2020Mgr.playerForce = force
    CUIManager.ShowUI(CLuaUIResources.MengGuiJieTipWnd)
end

function CLuaHalloween2020Mgr.ReplyHalloweenTerrifyAndDanShiInfo(terrifyCount, maxTerrifyCount, level, param1, param2)

    
    g_ScriptEvent:BroadcastInLua("ReplyHalloweenTerrifyAndDanShiInfo", terrifyCount, maxTerrifyCount, level, param1, param2)
end

function CLuaHalloween2020Mgr.SyncHalloween2020ArrestPlayInfo(restCount, totalCount, playData)

    
    local playerInfos = {}
    local list = MsgPackImpl.unpack(playData)
    for i=0, list.Count-1, 5 do
    	local playerId = list[i]
    	local force = list[i+1]
    	local x = list[i+2]
    	local y = list[i+3]
        local flag = list[i+4]
        table.insert(playerInfos, {playerId = playerId, force = force, x = x, y = y, flag = flag})

    end

    g_ScriptEvent:BroadcastInLua("SyncHalloween2020ArrestPlayInfo", restCount, totalCount, playerInfos)
end

function CLuaHalloween2020Mgr.IsInPlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == Halloween2020_MengGuiJie.GetData().GamePlayId then
            return true
        end
    end
    return false
end
