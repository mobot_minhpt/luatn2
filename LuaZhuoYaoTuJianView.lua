local DelegateFactory  = import "DelegateFactory"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local GameObject = import "UnityEngine.GameObject"
local QnCheckBox = import "L10.UI.QnCheckBox"
local UISprite = import "UISprite"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local UIScrollView = import "UIScrollView"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Ease = import "DG.Tweening.Ease"
local Vector4 = import "UnityEngine.Vector4"

LuaZhuoYaoTuJianView = class()

RegistChildComponent(LuaZhuoYaoTuJianView, "m_SelectSorts", "SelectSorts", QnSelectableButton)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_SelectSortsArrowSprite", "SelectSortsArrowSprite", GameObject)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_QnCheckBox", "QnCheckBox", QnCheckBox)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_TipButton", "TipButton", GameObject)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_QnTableView", "QnTableView", QnTableView)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_GradeLabel", "GradeLabel", UILabel)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_Portrait1", "Portrait1", CUITexture)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_Portrait2", "Portrait2", CUITexture)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_DesLabel", "DesLabel", UILabel)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_CatchNumLabel", "CatchNumLabel", UILabel)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_LianHuaTimeLabel", "LianHuaTimeLabel", UILabel)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_SuccessRateLabel", "SuccessRateLabel", UILabel)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_CatchWayLabel", "CatchWayLabel", UILabel)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_GoToCatchWayButton", "GoToCatchWayButton", GameObject)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_QualitySprite", "QualitySprite", UISprite)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_QualityLabel", "QualityLabel", UILabel)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_NoneLabel", "NoneLabel", GameObject)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_MonsterMaxLevelLabel", "MonsterMaxLevelLabel", UILabel)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_UnLockView", "UnLockView", GameObject)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_Item1", "Item1", GameObject)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_Item2", "Item2", GameObject)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_LockSprite", "LockSprite", GameObject)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_UnLockWidget", "UnLockWidget", CUIFx)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_UnLockLabel", "UnLockLabel", GameObject)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_RightView", "RightView", GameObject)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_Tab3AlertSprite", "Tab3AlertSprite", GameObject)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_ScrollView", "ScrollView", UIScrollView)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_TemplateLockFx", "TemplateLockFx", CUIFx)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_FullQualityFlag", "FullQualityFlag", GameObject)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_FullQualityGift", "FullQualityGift", GameObject)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_FullQualityGiftTexture", "FullQualityGiftTexture", GameObject)
RegistChildComponent(LuaZhuoYaoTuJianView, "m_FullQualityGiftFx", "FullQualityGiftFx", CUIFx)

RegistClassMember(LuaZhuoYaoTuJianView, "m_SelectSortIndex")
RegistClassMember(LuaZhuoYaoTuJianView, "m_YaoGuaiList")
RegistClassMember(LuaZhuoYaoTuJianView, "m_IsHideNeverCatch")
RegistClassMember(LuaZhuoYaoTuJianView, "m_CatchList")
RegistClassMember(LuaZhuoYaoTuJianView, "m_SelectRow")
RegistClassMember(LuaZhuoYaoTuJianView, "m_UnLockIds")
RegistClassMember(LuaZhuoYaoTuJianView, "m_IsSelectCatched")
RegistClassMember(LuaZhuoYaoTuJianView, "m_ReardItemId1")
RegistClassMember(LuaZhuoYaoTuJianView, "m_ReardItemId2")

function LuaZhuoYaoTuJianView:Start()
    if CommonDefs.IS_VN_CLIENT then
        self.m_RightView.transform:Find("Label2"):GetComponent(typeof(UILabel)).spacingX = 0
    end
    --self.m_Tab3AlertSprite:SetActive(false)
    self.m_RightView:SetActive(false)
    self.m_SelectSortIndex = 1
    self.m_SelectSorts.Text = LocalString.GetString("捕获成功率")
    self.m_UnLockView:SetActive(false)
    self.m_IsSelectCatched = not LuaZongMenMgr.m_OpenNewSystem
    self.m_SelectSorts.OnButtonSelected = DelegateFactory.Action_bool(function (selected )
        self:OnSelectSortsClick(selected)
    end)

    self.m_QnCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (selected )
        self:OnQnCheckBoxClick(selected)
    end)
    UIEventListener.Get(self.m_TipButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnTipButtonClick()
    end)
    UIEventListener.Get(self.m_GoToCatchWayButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnGoToCatchWayButtonClick()
    end)
    UIEventListener.Get(self.m_Item1).onClick = DelegateFactory.VoidDelegate(function (go)
        if not self.m_ReardItemId1 then return end
        CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_ReardItemId1) 
    end)
    UIEventListener.Get(self.m_Item2).onClick = DelegateFactory.VoidDelegate(function (go)
        if not self.m_ReardItemId2 then return end
        CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_ReardItemId2) 
    end)
    UIEventListener.Get(self.m_UnLockWidget.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        local tujianData = self.m_YaoGuaiList[self.m_SelectRow]
        if tujianData  then
            self.m_UnLockWidget:LoadFx("fx/ui/prefab/UI_lingheshengji.prefab")
            Gac2Gas.RequestGetYaoGuaiTuJianReward(tujianData.ID)
        end
    end)
    UIEventListener.Get(self.m_FullQualityGift).onClick = DelegateFactory.VoidDelegate(function (go)
        local tujianData = self.m_YaoGuaiList[self.m_SelectRow]
        if tujianData  then
            Gac2Gas.RequestGetYaoGuaiTuJianFullQualityReward(tujianData.ID)
        end
    end)
    self:Init()
end

function LuaZhuoYaoTuJianView:OnGoToCatchWayButtonClick()
    local tujianData = self.m_YaoGuaiList[self.m_SelectRow]
    if tujianData  then
        local getPath = tujianData.GetPath
        LuaZhuoYaoMgr:DoGetpathAction(getPath)
    end
end

function LuaZhuoYaoTuJianView:Init()
    self.m_CatchList = {}
    self.m_UnLockIds = {}
    self:SortYaoGuaiList()
    self.m_QnTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self.m_YaoGuaiList and #self.m_YaoGuaiList or 0
        end,
        function(item, index)
            self:ItemAt(item,index)
        end)
    self.m_QnTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
    self.m_TemplateLockFx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
    self.m_TemplateLockFx.ScrollView = self.m_ScrollView
    self.m_UnLockView.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LuaZongMenMgr.m_OpenNewSystem and LocalString.GetString("初次捕获奖励") or LocalString.GetString("解锁奖励") 
    self.m_RightView.transform:Find("Label3"):GetComponent(typeof(UILabel)).text = LuaZongMenMgr.m_OpenNewSystem and LocalString.GetString("解锁途径") or LocalString.GetString("捕获途径") 
    CLuaScheduleMgr.BuildInfos()
    Gac2Gas.RequestYaoGuaiTuJianData()
end

function LuaZhuoYaoTuJianView:ItemAt(item,index)
    item.gameObject.name = tostring(index)
    local portrait = item.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
    local mark = item.transform:Find("Mark").gameObject
    local levelLabel = item.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local fx = item.transform:Find("TemplateLockFx"):GetComponent(typeof(CUIFx))
    
    local tujianData = self.m_YaoGuaiList[index + 1]
    if not tujianData then
        return 
    end
    local monsterTempId = tujianData.MonsterIds[0]
    local icon = tujianData.Icon
    local catchData = self.m_CatchList[tujianData.ID]
    local border = item.transform:Find("Border"):GetComponent(typeof(UISprite))
    local isAlert = false
    border.spriteName = ""
    local alertSprite = item.transform:Find("AlertSprite").gameObject
    if catchData then
        local isFullQualit = catchData and catchData.CatchQuality >= tujianData.QualityRange[1]
        local canGetFullQualitGift = isFullQualit and catchData and not catchData.isFullQualityRewarded
        if canGetFullQualitGift then
            isAlert = true
        end
        if catchData.isRewarded then
            local quality = catchData and catchData.CatchQuality or 0
            local borderSpriteName = LuaZhuoYaoMgr:GetYaoGuaiQualityBorderColor(tujianData, quality)
            border.spriteName = borderSpriteName
        else
            isAlert = true
        end
    end
    alertSprite.gameObject:SetActive(isAlert)
    mark:SetActive(self.m_CatchList[tujianData.ID] == nil)
    levelLabel.text = tujianData.Grade
    portrait:LoadNPCPortrait(icon)
    --fx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
    fx.ScrollView = self.m_ScrollView
    if not (catchData and catchData.isRewarded == false) then
        LuaTweenUtils.DOKill(fx.transform, false)
    end
    fx.gameObject:SetActive(catchData and catchData.isRewarded == false)
end

function LuaZhuoYaoTuJianView:OnSelectAtRow(row)
    self.m_SelectRow = row + 1
    local tujianData = self.m_YaoGuaiList[self.m_SelectRow]
    if not tujianData then
        self.m_RightView:SetActive(false)
        return 
    end
    local monsterTempId = tujianData.MonsterIds[0]
    local catchData = self.m_CatchList[tujianData.ID]
    local headIcon = tujianData.Icon
    local quality = catchData and catchData.CatchQuality or 0
    local borderSpriteName = LuaZhuoYaoMgr:GetYaoGuaiQualityBorderColor(tujianData, quality)
    local time = CServerTimeMgr.ConvertTimeStampToZone8Time(catchData and catchData.CatchTime or 0)
    self.m_Portrait1:LoadNPCPortrait(headIcon)
    if LuaZongMenMgr.m_OpenNewSystem then
        self.m_Portrait1.color = catchData == nil and Color.black or Color.white
    end
    self.m_Portrait2:LoadNPCPortrait(headIcon)
    local isFullQualit = catchData and catchData.CatchQuality >= tujianData.QualityRange[1]
    local canGetFullQualitGift = isFullQualit and catchData and not catchData.isFullQualityRewarded
    self.m_FullQualityFlag:SetActive(isFullQualit)
    self.m_FullQualityGift:SetActive(canGetFullQualitGift)
    self.m_FullQualityGiftTexture:SetActive(canGetFullQualitGift)
    self:ShowkuangFx(self.m_FullQualityGiftFx, true)
    self.m_GradeLabel.text = tujianData.Grade
    self.m_NameLabel.text = tujianData.Name
    self.m_DesLabel.text = tujianData.Description
    self.m_LianHuaTimeLabel.text = math.floor(tujianData.Time/ 3600) .. LocalString.GetString("小时")
    self.m_CatchNumLabel.text = catchData and catchData.CatchNum or 0 
    self.m_SuccessRateLabel.text  = SafeStringFormat3("%d",math.modf(LuaZhuoYaoMgr:GetCatchRate(tujianData) * 100)) .."%"
    local getPathdata = ZhuoYao_GetPathInfo.GetData(tujianData.GetPath)
    if getPathdata then
        self.m_CatchWayLabel.text = getPathdata.PathName
    end
    self.m_QualitySprite.spriteName = borderSpriteName
    self.m_QualityLabel.text = quality
    self.m_NoneLabel.gameObject:SetActive(catchData == nil)
    self.m_Portrait2.gameObject:SetActive(catchData ~= nil)
    self.m_TimeLabel.text = ToStringWrap(time, "yyyy-MM-dd")
    self.m_TimeLabel.gameObject:SetActive(catchData ~= nil)
    self.m_MonsterMaxLevelLabel.text = catchData and catchData.Level or 0
    self.m_UnLockView:SetActive(catchData == nil or catchData.isRewarded == false)
    self.m_DesLabel.gameObject:SetActive(catchData and catchData.isRewarded)
    self.m_LockSprite:SetActive(catchData == nil)
    self.m_UnLockWidget.gameObject:SetActive(catchData and catchData.isRewarded == false)
    self.m_UnLockLabel.gameObject:SetActive(catchData and catchData.isRewarded == false)
    self.m_RightView:SetActive(true)
    self:InitItemCell(self.m_Item1,tujianData.UnlockReward[0],tujianData.UnlockReward[1],catchData and catchData.isRewarded == false)
    self.m_Item2.gameObject:SetActive(tujianData.UnlockReward.Length > 2)
    self.m_ReardItemId1 = tujianData.UnlockReward[0]
    self.m_ReardItemId2 = tujianData.UnlockReward.Length > 2 and tujianData.UnlockReward[2] or nil
    if tujianData.UnlockReward.Length > 2 then
        self:InitItemCell(self.m_Item2,tujianData.UnlockReward[2],tujianData.UnlockReward[3],catchData and catchData.isRewarded == false)
    end
    self.m_UnLockWidget:DestroyFx()
end

function LuaZhuoYaoTuJianView:InitItemCell(item, itemId, num, showFx)
    local data = Item_Item.GetData(itemId)
    item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(data.Icon)
    item.transform:Find("NumLabel"):GetComponent(typeof(UILabel)).text = num
    local fx = item.transform:Find("TemplateLockFx"):GetComponent(typeof(CUIFx))
    fx:DestroyFx()
    if showFx then
        self:ShowkuangFx(fx)
    else
        LuaTweenUtils.DOKill(fx.transform, false)
    end
end

function LuaZhuoYaoTuJianView:ShowkuangFx(fx, isCurved)
    LuaTweenUtils.DOKill(fx.transform, false)
    fx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
    local waypoints = CUICommonDef.GetWayPoints(Vector4(-57, 57, 114, 114), 1, isCurved == true)
    fx.transform.localPosition = waypoints[0]
    local tween = LuaTweenUtils.DODefaultLocalPath(fx.transform, waypoints , 2)
    LuaTweenUtils.SetTarget(tween,fx.transform)
end

function LuaZhuoYaoTuJianView:OnEnable()
    g_ScriptEvent:AddListener("OnSyncYaoGuaiTuJian", self, "OnSyncYaoGuaiTuJian")
    g_ScriptEvent:AddListener("OnSyncGetTuJianRewardSucc", self, "OnSyncGetTuJianRewardSucc")
    g_ScriptEvent:AddListener("OnSyncGetTuJianFullQualityRewardSucc", self, "OnSyncGetTuJianFullQualityRewardSucc")
    self:Init()
end

function LuaZhuoYaoTuJianView:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncYaoGuaiTuJian", self, "OnSyncYaoGuaiTuJian")
    g_ScriptEvent:RemoveListener("OnSyncGetTuJianRewardSucc", self, "OnSyncGetTuJianRewardSucc")
    g_ScriptEvent:RemoveListener("OnSyncGetTuJianFullQualityRewardSucc", self, "OnSyncGetTuJianFullQualityRewardSucc")
end

function LuaZhuoYaoTuJianView:OnSyncGetTuJianFullQualityRewardSucc(yaoguaiTempId)
    local t = self.m_CatchList[yaoguaiTempId]
    if t then 
        self.m_CatchList[yaoguaiTempId].isFullQualityRewarded = true
        local tujianData = self.m_YaoGuaiList[self.m_SelectRow]
        local item = self.m_QnTableView:GetItemAtRow(self.m_SelectRow - 1)
        if tujianData.ID == yaoguaiTempId then
            self.m_QnTableView:SetSelectRow(self.m_SelectRow - 1, true)
        end
        self:ReInitTab2AlertSprite()
        self:ItemAt(item.transform,self.m_SelectRow - 1)
    end
end

function LuaZhuoYaoTuJianView:OnSyncGetTuJianRewardSucc(yaoguaiTempId)
    local t = self.m_CatchList[yaoguaiTempId]
    if t then 
        self.m_CatchList[yaoguaiTempId].isRewarded = true
        self.m_UnLockIds[yaoguaiTempId] = false
        local curTujianData = self.m_YaoGuaiList[self.m_SelectRow]
        local item = self.m_QnTableView:GetItemAtRow(self.m_SelectRow - 1)
        if curTujianData.ID == yaoguaiTempId then
            self.m_QnTableView:SetSelectRow(self.m_SelectRow - 1, true)
        end
        self:ReInitTab2AlertSprite()
        self:ItemAt(item.transform,self.m_SelectRow - 1)
    end
end

function LuaZhuoYaoTuJianView:ReInitTab2AlertSprite()
    self.m_Tab3AlertSprite:SetActive(false)
    for id, isUnLock in pairs(self.m_UnLockIds) do
        local tujianData = ZhuoYao_TuJian.GetData(id)
        if isUnLock and tujianData.Status == 0 then
            self.m_Tab3AlertSprite:SetActive(true)
        end
    end
    for tuJianId,t in pairs(self.m_CatchList) do
        local tujianData = ZhuoYao_TuJian.GetData(tuJianId)
        local isFullQualit = t.CatchQuality >= tujianData.QualityRange[1]
        local canGetFullQualitGift = isFullQualit and not t.isFullQualityRewarded
        if canGetFullQualitGift and tujianData.Status == 0 then
            self.m_Tab3AlertSprite:SetActive(true)
        end
    end
end

function LuaZhuoYaoTuJianView:OnSyncYaoGuaiTuJian(list)
    self.m_CatchList = {}
    self.m_Tab3AlertSprite:SetActive(false)
    for _,t in pairs(list) do
        local yaoguaiTempId = t.yaoguaiTempId
        local tuJianId = yaoguaiTempId
        if not t.isRewarded then
            self.m_UnLockIds[yaoguaiTempId] = true
        end
        self.m_CatchList[tuJianId] = {isFullQualityRewarded = t.isFullQualityRewarded, CatchQuality = t.maxQuality, CatchTime = t.maxQualityCatchedTime, CatchNum = t.catchedCount,Level = t.maxQualityLevel,isRewarded = t.isRewarded}
    end
    --self.m_QnTableView:ReloadData(true, true)
    self.m_QnCheckBox.Selected = self.m_IsSelectCatched
    self:ReInitTab2AlertSprite()
end

function LuaZhuoYaoTuJianView:InitYaoGuaiList()
    self.m_YaoGuaiList = {}
    ZhuoYao_TuJian.Foreach(function(key,data)
        if data.Status == 0 then
            local t = ZhuoYao_TuJian.GetData(key)
            if self.m_IsHideNeverCatch then
                if self.m_CatchList[key] then
                    table.insert(self.m_YaoGuaiList, t)
                end
            else
                table.insert(self.m_YaoGuaiList, t)
            end
        end
    end)
end

function LuaZhuoYaoTuJianView:SortYaoGuaiList()
    self:InitYaoGuaiList()
    local defaultSort = function (a,b)
        local ratea,rateb = LuaZhuoYaoMgr:GetCatchRate(a) , LuaZhuoYaoMgr:GetCatchRate(b)
        if ratea == rateb then return a.Grade > b.Grade  end
        return ratea > rateb
    end
    if self.m_SelectSortIndex == 5 then
        table.sort(self.m_YaoGuaiList,function(a, b)
            if a.Grade == b.Grade then return defaultSort(a,b)  end
            return a.Grade > b.Grade 
        end)
    elseif self.m_SelectSortIndex == 2 then
        table.sort(self.m_YaoGuaiList,function(a, b)
            local t1,t2 = self.m_CatchList[a.ID],self.m_CatchList[b.ID]
            if t1 == nil and t2 == nil then 
                return defaultSort(a,b)
            elseif t1 == nil then 
                return false 
            elseif t2 == nil then 
                return true 
            end
            if t1.CatchTime == t2.CatchTime then return defaultSort(a,b) end
            return t1.CatchTime > t2.CatchTime
        end)
    elseif self.m_SelectSortIndex == 3 then
        table.sort(self.m_YaoGuaiList,function(a, b)
            local t1,t2 = self.m_CatchList[a.ID],self.m_CatchList[b.ID]
            if t1 == nil and t2 == nil then 
                return defaultSort(a,b)
            elseif t1 == nil then 
                return false 
            elseif t2 == nil then 
                return true 
            end
            if t1.CatchQuality == t2.CatchQuality then return defaultSort(a,b) end
            return t1.CatchQuality > t2.CatchQuality
        end)
    elseif self.m_SelectSortIndex == 4 then
        table.sort(self.m_YaoGuaiList,function(a, b)
            local t1,t2 = self.m_CatchList[a.ID],self.m_CatchList[b.ID]
            if t1 == nil and t2 == nil then 
                return defaultSort(a,b)
            elseif t1 == nil then 
                return false 
            elseif t2 == nil then 
                return true 
            end
            if t1.CatchNum == t2.CatchNum then return defaultSort(a,b)  end
            return t1.CatchNum > t2.CatchNum
        end)
    elseif self.m_SelectSortIndex == 6 then
        table.sort(self.m_YaoGuaiList,function(a, b)
            if a.GetPath == b.GetPath then 
                return a.Grade < b.Grade
            end
            return a.GetPath < b.GetPath
        end)
    elseif self.m_SelectSortIndex == 1 then
        table.sort(self.m_YaoGuaiList,function(a, b)
            return defaultSort(a,b)
        end)
    end
end

--@region UIEvent

function LuaZhuoYaoTuJianView:OnSelectSortsClick(selected)
    self.m_IsSelectCatched = selected
    local textArray = {LocalString.GetString("捕获成功率"),LocalString.GetString("捕获时间"),LocalString.GetString("捕获品质"),LocalString.GetString("捕获数量"),LocalString.GetString("评级"),LuaZongMenMgr.m_OpenNewSystem and LocalString.GetString("解锁途径") or LocalString.GetString("捕获途径")}
    Extensions.SetLocalRotationZ(self.m_SelectSortsArrowSprite.transform, selected and 180 or 0)
    if not selected then return end
    local t = {}
    for k,text in pairs(textArray) do
        local item=PopupMenuItemData(text,DelegateFactory.Action_int(function (idx)
            self.m_SelectSorts.Text = textArray[idx + 1]
            self:OnChooseSortClicked(idx + 1)
        end),false,nil)
        table.insert(t, item)
    end
    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, self.m_SelectSorts.m_Label.transform, AlignType.Bottom,1,DelegateFactory.Action(function()
        Extensions.SetLocalRotationZ(self.m_SelectSortsArrowSprite.transform, 0)
    end),600,true,296)
end

function LuaZhuoYaoTuJianView:OnChooseSortClicked(index)
    self.m_SelectSortIndex = index
    self:SortYaoGuaiList()
    self.m_QnTableView:ReloadData(true, false)
    if #self.m_YaoGuaiList > 0 then
        self.m_QnTableView:SetSelectRow(0, true)
        --self.m_QnTableView:ScrollToRow(0)
    end
end

function LuaZhuoYaoTuJianView:OnQnCheckBoxClick(selected)
    self.m_IsHideNeverCatch = selected
    self:SortYaoGuaiList()
    self.m_QnTableView:ReloadData(true, false)
    if #self.m_YaoGuaiList > 0 then
        self.m_QnTableView:SetSelectRow(0, true)
        --self.m_QnTableView:ScrollToRow(0)
    else
        self.m_RightView:SetActive(false)
    end
end

function LuaZhuoYaoTuJianView:OnTipButtonClick()
    if LuaZongMenMgr.m_OpenNewSystem then
        g_MessageMgr:ShowMessage("ZhuoYao_TuJian_ReadMe2")
        return
    end
    g_MessageMgr:ShowMessage("ZhuoYao_TuJian_ReadMe")
end

--@endregion

