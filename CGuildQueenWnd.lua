-- Auto Generated!!
local CGuildQueenWnd = import "L10.UI.CGuildQueenWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local EGuildQueenStatus = import "L10.UI.EGuildQueenStatus"
CGuildQueenWnd.m_QueryBangHuaCurStageStatusResult_CS2LuaHook = function (this, stage, status) 
    if stage == 1 then
        this.m_Level1:UpdateData(1, CommonDefs.ConvertIntToEnum(typeof(EGuildQueenStatus), status))
        this.m_Level2:UpdateData(2, EGuildQueenStatus.NotStarted)
        this.m_Level3:UpdateData(3, EGuildQueenStatus.NotStarted)
    elseif stage == 2 then
        this.m_Level1:UpdateData(1, EGuildQueenStatus.Finished)
        this.m_Level2:UpdateData(2, CommonDefs.ConvertIntToEnum(typeof(EGuildQueenStatus), status))
        this.m_Level3:UpdateData(3, EGuildQueenStatus.NotStarted)
    elseif stage == 3 then
        this.m_Level1:UpdateData(1, EGuildQueenStatus.Finished)
        this.m_Level2:UpdateData(2, EGuildQueenStatus.Finished)
        this.m_Level3:UpdateData(3, CommonDefs.ConvertIntToEnum(typeof(EGuildQueenStatus), status))
    else
        this.m_Level1:UpdateData(1, EGuildQueenStatus.NotStarted)
        this.m_Level2:UpdateData(2, EGuildQueenStatus.NotStarted)
        this.m_Level3:UpdateData(3, EGuildQueenStatus.NotStarted)
    end
end
