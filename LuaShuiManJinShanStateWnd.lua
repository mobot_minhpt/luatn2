local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local UITable = import "UITable"
local Transform = import "UnityEngine.Transform"
local CUITexture = import "L10.UI.CUITexture"
local Color = import "UnityEngine.Color"
local TweenAlpha = import "TweenAlpha"
local UIPanel = import "UIPanel"
local HousePopupMenuItemData = import "L10.UI.HousePopupMenuItemData"
local CHousePopupMgr = import "L10.UI.CHousePopupMgr"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"

LuaShuiManJinShanStateWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaShuiManJinShanStateWnd, "StageType1", "StageType1", GameObject)
RegistChildComponent(LuaShuiManJinShanStateWnd, "StageType2", "StageType2", GameObject)
RegistChildComponent(LuaShuiManJinShanStateWnd, "StageNameLab1", "StageNameLab1", UILabel)
RegistChildComponent(LuaShuiManJinShanStateWnd, "StageNameLab2", "StageNameLab2", UILabel)
RegistChildComponent(LuaShuiManJinShanStateWnd, "Progress", "Progress", UISprite)
RegistChildComponent(LuaShuiManJinShanStateWnd, "Table", "Table", UITable)
RegistChildComponent(LuaShuiManJinShanStateWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaShuiManJinShanStateWnd, "SkillProgress", "SkillProgress", Transform)
RegistChildComponent(LuaShuiManJinShanStateWnd, "SkillIcon", "SkillIcon", CUITexture)
RegistChildComponent(LuaShuiManJinShanStateWnd, "SkillMask", "SkillMask", UISprite)
RegistChildComponent(LuaShuiManJinShanStateWnd, "SkillStopAlert", "SkillStopAlert", GameObject)
RegistChildComponent(LuaShuiManJinShanStateWnd, "StageBg", "StageBg", CUITexture)
RegistChildComponent(LuaShuiManJinShanStateWnd, "ExpandButton", "ExpandButton", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaShuiManJinShanStateWnd, "m_Stage")
RegistClassMember(LuaShuiManJinShanStateWnd, "m_Progress")
RegistClassMember(LuaShuiManJinShanStateWnd, "m_MaxProgress")
RegistClassMember(LuaShuiManJinShanStateWnd, "m_SubProgress")
RegistClassMember(LuaShuiManJinShanStateWnd, "m_MaxSubProgress")
RegistClassMember(LuaShuiManJinShanStateWnd, "m_Pause")
RegistClassMember(LuaShuiManJinShanStateWnd, "m_SubProgressWidget")
RegistClassMember(LuaShuiManJinShanStateWnd, "m_SubProgressFlash")
RegistClassMember(LuaShuiManJinShanStateWnd, "m_Force1HighlightCol")
RegistClassMember(LuaShuiManJinShanStateWnd, "m_Force1LowlightCol")
RegistClassMember(LuaShuiManJinShanStateWnd, "m_Force2HighlightCol")
RegistClassMember(LuaShuiManJinShanStateWnd, "m_Force2LowlightCol")

RegistClassMember(LuaShuiManJinShanStateWnd, "m_Tick")
RegistClassMember(LuaShuiManJinShanStateWnd, "m_CurNPC")
RegistClassMember(LuaShuiManJinShanStateWnd, "m_Force")
RegistClassMember(LuaShuiManJinShanStateWnd, "m_IsOpendPopupMenu")
RegistClassMember(LuaShuiManJinShanStateWnd, "m_Panel")

function LuaShuiManJinShanStateWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_Force1HighlightCol   = NGUIText.ParseColor24("B5CBEC", 0)
    self.m_Force1LowlightCol    = NGUIText.ParseColor24("6580A9", 0)
    self.m_Force2HighlightCol   = NGUIText.ParseColor24("ECEBB5", 0)
    self.m_Force2LowlightCol    = NGUIText.ParseColor24("998861", 0)

    self.m_SubProgressWidget = self.SkillProgress:GetComponent(typeof(UIWidget))
    self.m_SubProgressFlash = self.SkillProgress:GetComponent(typeof(TweenAlpha))
    UIEventListener.Get(self.ExpandButton).onClick = DelegateFactory.VoidDelegate( function(p) 
        self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
    self.Template:SetActive(false)

    self.m_Panel = self.gameObject:GetComponent(typeof(UIPanel))
    if CameraFollow.Inst.IsBezierCamera or CameraFollow.Inst.IsAICamera or CameraFollow.Inst.IsToricSpaceCamera then
        self.m_Panel.alpha = 0
    end
end

function LuaShuiManJinShanStateWnd:Init()
    self:UpdateData()
    self:RefreshState()
    UnRegisterTick(self.m_Tick)
    self.m_Tick = RegisterTick(function()
        self:Tick()
    end, 100)
end

--@region UIEvent

--@endregion UIEvent

function LuaShuiManJinShanStateWnd:OnEnable()
    g_ScriptEvent:AddListener("OnQteFinished", self, "OnQteFinished")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("ShuiManJinShan_SyncProgress", self, "OnShuiManJinShan_SyncProgress")
end

function LuaShuiManJinShanStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnQteFinished", self, "OnQteFinished")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("ShuiManJinShan_SyncProgress", self, "OnShuiManJinShan_SyncProgress")

    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIManager.TopAndRightTipWnd)
    end
end

function LuaShuiManJinShanStateWnd:OnHideTopAndRightTipWnd()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

function LuaShuiManJinShanStateWnd:OnShuiManJinShan_SyncProgress()
    self:UpdateData()
    self:RefreshState()
end

function LuaShuiManJinShanStateWnd:UpdateData()
    self.m_Stage = LuaShuiManJinShanMgr.m_Stage
    self.m_Progress = LuaShuiManJinShanMgr.m_Progress
    self.m_MaxProgress = LuaShuiManJinShanMgr.m_MaxProgress
    self.m_SubProgress = LuaShuiManJinShanMgr.m_SubProgress
    self.m_MaxSubProgress = LuaShuiManJinShanMgr.m_MaxSubProgress
    self.m_Pause = LuaShuiManJinShanMgr.m_Pause
end

function LuaShuiManJinShanStateWnd:RefreshState()
    local curForce = LuaShuiManJinShanMgr:GetForceByGameplayId(CClientMainPlayer.Inst.PlayProp.PlayId)
    local stageData = XinBaiLianDong_ShuiManJinShanStage.GetData(self.m_Stage)
    if stageData == nil then
        return
    end

    local isStageType2 = self.m_MaxProgress ~= 0
    local hasSub = self.m_MaxSubProgress ~= 0
    self.StageType1:SetActive(not isStageType2)
    self.StageType2:SetActive(isStageType2)
    self.SkillProgress.gameObject:SetActive(isStageType2 and hasSub)
    local str = (curForce == 1001 and "bai0" or "jin0") .. stageData.BackgroundImage
    self.StageBg:LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/shuimanjinshanstatewnd_%s.mat", str))
    if isStageType2 then
        self.StageNameLab2.text = SafeStringFormat3(stageData.StageInfoSpecial, self.m_Progress, self.m_MaxProgress)
        self:InitProgress()
        if hasSub then
            self:SubProgress()
        end
    else
        self.StageNameLab1.text = stageData.StageInfoSpecial 
    end
end

function LuaShuiManJinShanStateWnd:InitProgress()
    local curForce = LuaShuiManJinShanMgr:GetForceByGameplayId(CClientMainPlayer.Inst.PlayProp.PlayId)
    local countOffset = self.m_MaxProgress - self.Table.transform.childCount
    if countOffset >= 0 then
        for i = 1, countOffset do
            local go = NGUITools.AddChild(self.Table.gameObject, self.Template)
            go:SetActive(true)
        end
    else
        local needDestroyGo = {}
        for i = 1, -countOffset do 
            local item = self.Table.transform:GetChild(self.m_MaxProgress + i - 1)
            table.insert(needDestroyGo, item.gameObject)
        end
        for i = 1, #needDestroyGo do
            GameObject.Destroy(needDestroyGo[i])
        end
    end

    local totalWidth = self.Progress.width
    local paddingX = self.Table.padding.x
    local itemWidth = totalWidth / self.m_MaxProgress - paddingX * 2

    local highCol = curForce == 1001 and self.m_Force1HighlightCol or self.m_Force2HighlightCol
    local lowCol = curForce == 1001 and self.m_Force1LowlightCol or self.m_Force2LowlightCol
    for i = 1, self.m_MaxProgress do
        local item = self.Table.transform:GetChild(i - 1):GetComponent(typeof(UISprite))
        item.width = 100
        LuaUtils.SetLocalScale(item.transform,itemWidth / 100,1,1)
        item.color = i <= self.m_Progress and highCol or lowCol
    end

    self.Table:Reposition()
end

function LuaShuiManJinShanStateWnd:SubProgress()
    if self.m_MaxSubProgress == 0 then
        return 
    end
    if self.m_Progress < self.m_MaxProgress then
        local targetItem = self.Table.transform:GetChild(self.m_Progress)
        self.SkillProgress.position = targetItem.position
    else
        self.SkillProgress.gameObject:SetActive(false)
    end
    self.SkillMask.fillAmount = 1 - self.m_SubProgress / self.m_MaxSubProgress
    self.SkillStopAlert:SetActive(self.m_Pause)
    Extensions.SetLocalPositionZ(self.SkillProgress, self.m_Pause and -1 or 0)
    if self.m_SubProgress == self.m_MaxSubProgress then
        self.m_SubProgressFlash.enabled = true
    else
        self.m_SubProgressFlash.enabled = false
        self.m_SubProgressWidget.alpha = 1
    end
end

function LuaShuiManJinShanStateWnd:OnDestroy()
    UnRegisterTick(self.m_Tick)
    CUIManager.CloseUI(CUIResources.HousePopupMenu)
    LuaShuiManJinShanMgr:ClearProgress()
end

function LuaShuiManJinShanStateWnd:Tick()
    if not LuaShuiManJinShanMgr.m_AllNPC or( self.m_Stage ~= 6 and self.m_Stage ~= 12) then
        if self.m_IsOpendPopupMenu then
            self.m_IsOpendPopupMenu = false
            CUIManager.CloseUI(CUIResources.HousePopupMenu)
        end
        return
    end
    local curInfo
    for i = 1, #LuaShuiManJinShanMgr.m_AllNPC do 
        local info = LuaShuiManJinShanMgr.m_AllNPC[i]
        local npcTransform = info.transform
        if not CommonDefs.IsUnityObjectNull(npcTransform) then
            local myPos = CClientMainPlayer.Inst.RO.transform.position
            local npcPos = npcTransform.position
            myPos.y = npcPos.y
            local dis = Vector3.Distance(myPos, npcPos)
            if dis <= 2 then
                curInfo = info
                break
            end
        end
    end

    if not curInfo and self.m_IsOpendPopupMenu then
        self.m_IsOpendPopupMenu = false
        CUIManager.CloseUI(CUIResources.HousePopupMenu)
        return
    elseif curInfo and not (self.m_CurNPC == curInfo.npc) then
        if not CUIManager.IsLoaded(CUIResources.HousePopupMenu) then
            self:ShowPopupMenu(curInfo)
        end
    else
        if curInfo == nil then 
            self.m_CurNPC = nil
        end
    end
end

function LuaShuiManJinShanStateWnd:ShowPopupMenu(info)
    self.m_IsOpendPopupMenu = true
    if not self.m_Force then
        self.m_Force = LuaShuiManJinShanMgr:GetForceByGameplayId(CClientMainPlayer.Inst.PlayProp.PlayId)
    end

    local data = NPC_NPC.GetData(info.npcId)
    if data == nil then
        return 
    end
    local dialogId = data.DialogId
    local iconPath = self.m_Force == 1002 and "UI/Texture/Transparent/Material/housepopupmenu_jianli.mat" or "UI/Texture/Transparent/Material/housepopupmenu_pohuai.mat"
    local list = CommonDefs.ListCreate(typeof(HousePopupMenuItemData))
    CommonDefs.ListAdd_LuaCall(list, HousePopupMenuItemData("", DelegateFactory.Action_int(function ( index )
        self.m_CurNPC = info.npc
        Gac2Gas.SelectNpcDialogChoice(info.engineId, dialogId, info.npcId, 1)
    end), iconPath, true, true, -1))
    CHousePopupMgr.ShowHousePopupMenu(CommonDefs.ListToArray(list), info.transform)
end

function LuaShuiManJinShanStateWnd:OnQteFinished(success)
    -- 失败则再弹
    if not success then
        self.m_CurNPC = nil
    end
end

function LuaShuiManJinShanStateWnd:OnDestroy()
    LuaShuiManJinShanMgr:Clear()
end