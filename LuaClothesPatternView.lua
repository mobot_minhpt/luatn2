local Extensions = import "Extensions"
local UIPanel = import "UIPanel"
local UIBasicSprite = import "UIBasicSprite"

LuaClothesPatternView = class()

RegistChildComponent(LuaClothesPatternView, "m_TextureTemplate","TextureTemplate", GameObject)
RegistChildComponent(LuaClothesPatternView, "m_Root","Root", GameObject)

function LuaClothesPatternView:Awake()
    self.m_TextureTemplate:SetActive(false)
end

function LuaClothesPatternView:Init(data)
    local rect = self.gameObject:GetComponent(typeof(UIPanel))
    if not rect then rect = self.gameObject:GetComponent(typeof(UIWidget)) end
    if not rect then return end
    local info = data[0]
    Extensions.RemoveAllChildren(self.m_Root.transform)
    local scaleRootX = rect.width / 600
    local scaleRootY = rect.height / 600
    self.m_Root.transform.localScale = Vector3(scaleRootX,scaleRootY,1)
    local parentSprite = self.gameObject:GetComponent(typeof(UIBasicSprite))
    for i = 0,info.Count - 1,7 do
        local x,y,scaleX,scaleY,rotationZ,color,pattern = info[i + 0],info[i + 1],info[i + 2],info[i + 3],info[i + 4],info[i + 5],info[i + 6]
        local obj = NGUITools.AddChild(self.m_Root.gameObject, self.m_TextureTemplate)
        obj.transform.localPosition = Vector3(x, y, 0)
        obj.transform.localEulerAngles = Vector3(0, 0, rotationZ)
        obj:SetActive(true)
        local tex = obj:GetComponent(typeof(CUITexture))
        tex:LoadMaterial(LuaMakeClothesMgr:GetPicture(pattern, color))
        local sprite =  obj:GetComponent(typeof(UIBasicSprite))
        sprite.width = 240 * scaleX
        sprite.height = 240 * scaleY
        if parentSprite then
            sprite.flip = parentSprite.flip
            if sprite.flip == UIBasicSprite.Flip.Horizontally then
                sprite.transform.localPosition = Vector3(-sprite.transform.localPosition.x,sprite.transform.localPosition.y,sprite.transform.localPosition.z)
            end
        end
    end
end