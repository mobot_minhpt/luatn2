local SdkU3d = import "NtUniSdk.Unity3d.SdkU3d"
local CPersonalSpaceMgr=import "L10.Game.CPersonalSpaceMgr"
local Main = import "L10.Engine.Main"
local Json = import "L10.Game.Utils.Json"
local CHTTPForm = import "L10.Game.CHTTPForm"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local ShareBoxMgr=import "L10.UI.ShareBoxMgr"
local EShareType=import "L10.UI.EShareType"
local MessageWndManager=import "L10.UI.MessageWndManager"
local LocalString = import "LocalString"
local ShareMgr = import "ShareMgr"

CLuaShareMgr={}
CLuaShareMgr.m_Url=nil
CLuaShareMgr.m_ShareCallback=nil

CLuaShareMgr.m_ShareHouseCompetition = false

--@region 七夕2019

--七夕2019的数据，包含参赛状态和本日上传次数
CLuaShareMgr.QixiData=nil
--每日可以上传是数量上限
CLuaShareMgr.QixiUploadCountMax=10

--[[
    @desc: 是否开启七夕2019模块
    author:CodeGize
    time:2019-06-24 15:36:36
    @return:
]]
function CLuaShareMgr.IsQixiShareModuleEnable()
    return CLuaShareMgr.QixiData ~= nil and CLuaShareMgr.QixiData.State ~= -1
end

--[[
    @desc:请求服务器玩家的七夕数据 
    author:CodeGize
    time:2019-06-24 15:44:07
    @return:
]]
function CLuaShareMgr:RequireQixiData()

    Gac2Gas.QXJTQueryRegistered()
end

function CLuaShareMgr.UpLoadQixiCompleted(url)
    if CLuaShareMgr.QixiData.Count>=CLuaShareMgr.QixiUploadCountMax then
        local res = LocalString.GetString("您上传的图片数量已超过上限，请上传10张以内的图片！")
        MessageWndManager.ShowOKMessage(res,nil)
        return
    end
    CLuaShareMgr.QixiData.Count = CLuaShareMgr.QixiData.Count+1

    Gac2Gas.QXJTUploadImage(CLuaShareMgr.QixiData.Count,url)
end

--@endregion

-- #### 全民争霸分享
-- POST /qnm/activity/qmpk/share

-- #### 请求参数
-- | 参数     | 必选    | 类型     | 说明                      |
-- | :----- | :---- | :----- | ----------------------- |
-- | roleid | true  | string | 玩家角色Id                  |
-- | origin_roleid | true  | string | 原服玩家角色id |
-- | text | true  | string | 分享文本 |
-- | url | true  | string | 分享图url |

-- #### 返回示例
-- ```json
--   {
--     "code": 0,
--     "data": {
--       "id": 1 // 心情id
--     }
--   }
CLuaShareMgr.m_ShareQMPKImage2PersonalSpaceCallback = nil
function CLuaShareMgr.ShareQMPKImage2PersonalSpace(roleid,origin_roleid,imgurl)
    CLuaShareMgr.m_ShareCallback=function(text)
        local url = CPersonalSpaceMgr.BASE_URL.."/qnm/activity/qmpk/share"

        local form = CHTTPForm()
        local time = math.floor(CServerTimeMgr.Inst.timeStamp * 1000)
        local skey = CPersonalSpaceMgr.Inst.Token
    
        form:AddField("time", tostring(time))
        form:AddField("roleid", roleid)
        form:AddField("origin_roleid", origin_roleid)
        form:AddField("text", text)
        form:AddField("url", imgurl)
        form:AddField("skey", skey)
    
        Main.Inst:StartCoroutine(CPersonalSpaceMgr.Inst:DoPost(url, form, DelegateFactory.Action_bool_string(function (success, json)
            if CLuaShareMgr.m_ShareQMPKImage2PersonalSpaceCallback then
                CLuaShareMgr.m_ShareQMPKImage2PersonalSpaceCallback()
            end
            if success then
                local dict = Json.Deserialize(json)
                local code = CommonDefs.DictGetValue_LuaCall(dict,"code")
                CUIManager.CloseUI(CLuaUIResources.CommonShare2PersonalSpaceWnd)
                CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(origin_roleid)
            end
        end)))
    end
    CLuaShareMgr.m_Url=imgurl
    CUIManager.ShowUI(CLuaUIResources.CommonShare2PersonalSpaceWnd)
end

function CLuaShareMgr.ShareGuanNingImage2PersonalSpace(imgurl)
    CLuaShareMgr.m_ShareCallback=function(text)
        -- ShareMgr.Share2PersonalSpace(shareText)
        local url = CPersonalSpaceMgr.BASE_URL.."/qnm/addmoment"

        local form = CHTTPForm()
        local time = math.floor(CServerTimeMgr.Inst.timeStamp * 1000)
        local skey = CPersonalSpaceMgr.Inst.Token
        local serverId = CPersonalSpaceMgr.Inst:GetOriginalServerId()
        local roleid=CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0

        form:AddField("time", tostring(time))
        form:AddField("roleid", roleid)
        form:AddField("serverid", tostring(serverId))
        form:AddField("skey", skey)

        form:AddField("text", text or "")
        
        form:AddField("imglist", SafeStringFormat3([=[["%s"]]=],imgurl))


        -- print(url)
        Main.Inst:StartCoroutine(CPersonalSpaceMgr.Inst:DoPost(url, form, DelegateFactory.Action_bool_string(function (success, json)
            if success then
                local dict = Json.Deserialize(json)
                local code = CommonDefs.DictGetValue_LuaCall(dict,"code")
                if code==0 then
                    CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(roleid)
                else
                    local msg=CommonDefs.DictGetValue_LuaCall(dict,"msg")
                    g_MessageMgr:ShowMessage("CUSTOM_STRING2", msg)
                end
                CUIManager.CloseUI(CLuaUIResources.CommonShare2PersonalSpaceWnd)
            end
        end)))

    end
    CLuaShareMgr.m_Url=imgurl
    CUIManager.ShowUI(CLuaUIResources.CommonShare2PersonalSpaceWnd)
end

--全民pk每周战报
function CLuaShareMgr.ShareQMPKZhanBao()
    if not CClientMainPlayer.Inst then return end
    ShareBoxMgr.OpenShareBox(EShareType.CommonUrl,0,{
        "https://qnm.163.com/m/zhoubao/#/"..tostring(CClientMainPlayer.Inst.Id),
        LocalString.GetString("热血重燃！倩女手游全民争霸赛每周战报！"),
        LocalString.GetString("《倩女幽魂》手游全民争霸赛最强数据汇总！玩儿得6不6，一目了然！"),
        "https://qnm.res.netease.com/xt/game/zhoubaoh5/share.jpg"
    })
end



function CLuaShareMgr.TryShareHouseCompetition(fileName)
    CLuaHouseCompetitionMgr.CheckMirrorServer()
    local bInOwnHouseAndHouseCompetition = CLuaHouseCompetitionMgr.CanShareHouseCompetition()
    CLuaShareMgr.m_ShareHouseCompetition = bInOwnHouseAndHouseCompetition

    --不是家园设计大赛就直接返回
    if not bInOwnHouseAndHouseCompetition then return false end


    if bInOwnHouseAndHouseCompetition then
        ShareBoxMgr.ShareType = EShareType.ShareImage
        ShareBoxMgr.TaskId = 0
        ShareBoxMgr.ImagePath = fileName

        CUIManager.ShowUI(CUIResources.ShareBox3)
    end
    return bInOwnHouseAndHouseCompetition
end

--=0表示没有初始化，=1表示true =2表示false
CLuaShareMgr.m_IsOpenShareValue = 0
CLuaShareMgr.m_IsHuaWei = false
--是否开放分享
--不开分享：安智、内涵段子、今日头条、金立 37wan、华为、易信、pptv
function CLuaShareMgr.IsOpenShare()
    if CLuaShareMgr.m_IsOpenShareValue==0 then
        local channel = SdkU3d.getChannel()
		if channel=="allysdk" then
			channel = SdkU3d.getAppChannel()
		end
        local needHook = not (channel=="xblm" or channel=="meizu_sdk")
        CLuaShareMgr.m_IsHuaWei = channel=="huawei"
        if channel=="netease" or needHook then
            CLuaShareMgr.m_IsOpenShareValue=1
        else
            CLuaShareMgr.m_IsOpenShareValue=2
        end
    end
    if CLuaShareMgr.m_IsOpenShareValue==1 then 
        return true
    else
        return false
    end
end

function CLuaShareMgr.SimpleShareLocalImage2PersonalSpace(fullpath)
    if not CPersonalSpaceMgr.Inst:OpenPersonalSpaceCheck(false) then
        g_MessageMgr:ShowMessage("PERSONAL_SPACE_NOT_OPEN")
        return
    end

    ShareMgr.ShareLocalImage2PersonalSpace(fullpath, DelegateFactory.Action(function ()
        g_ScriptEvent:BroadcastInLua("PersonalSpaceShareFinished")
        CUIManager.CloseUI(CUIResources.Share2PersonalSpaceWnd)
        g_MessageMgr:ShowMessage("SHARE_SUCCESS")
    end))
end

CLuaShareMgr.m_ShareChannels = {}
CLuaShareMgr.m_TopLabelText = ""
CLuaShareMgr.m_ShareMsg = nil
function CLuaShareMgr:ShowCommonShareBox(shareChannels, topLabelText, shareMsg)
    self.m_ShareChannels, self.m_TopLabelText, self.m_ShareMsg = shareChannels, topLabelText, shareMsg
    CUIManager.ShowUI(CLuaUIResources.CommonShareBox)
end
