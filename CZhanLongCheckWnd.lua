-- Auto Generated!!
local CZhanLongCheckWnd = import "L10.UI.CZhanLongCheckWnd"
local CZhanLongMgr = import "L10.Game.CZhanLongMgr"
local L10 = import "L10"
local LocalString = import "LocalString"
CZhanLongCheckWnd.m_Init_CS2LuaHook = function (this) 
    this.replaceTimesLabel.text = tostring(CZhanLongMgr.Inst.replaceTimes)
    this.guildSilverAdjLabel.text = (LocalString.GetString("跑商奖励剩余") .. tostring(CZhanLongMgr.Inst.guildSilverAdj)) .. "%"
    this.guildSilverAdjSlider.value = CZhanLongMgr.Inst.guildSilverAdj / 100
    this.damageTimesLabel.text = tostring(CZhanLongMgr.Inst.damageTimes)
    this.vehicleHpAdjLabel.text = (LocalString.GetString("跑商货车血量剩余") .. tostring(CZhanLongMgr.Inst.vehicleHpAdj)) .. "%"
    this.vehicleHpAdjSlider.value = CZhanLongMgr.Inst.vehicleHpAdj / 100
    this.stolenMoneyLabel.text = tostring(CZhanLongMgr.Inst.stolenMoney)
end
CZhanLongCheckWnd.m_Awake_CS2LuaHook = function (this) 
    if CZhanLongCheckWnd.Instance ~= nil then
        L10.CLogMgr.LogError("more than one instance running!")
    end
    CZhanLongCheckWnd.Instance = this
end
