local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local EnumGender = import "L10.Game.EnumGender"
local UISprite = import "UISprite"
local LuaTweenUtils = import "LuaTweenUtils"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CCustomSelector = import "L10.UI.CCustomSelector"
local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"

LuaAppearanceFashionSuitSubview = class()

RegistChildComponent(LuaAppearanceFashionSuitSubview,"m_FashionItem", "FashionItem", GameObject)
RegistChildComponent(LuaAppearanceFashionSuitSubview,"m_ItemDisplay", "AppearanceCommonFashionDisplayView", CCommonLuaScript)
RegistChildComponent(LuaAppearanceFashionSuitSubview,"m_ContentGrid", "FashionContentGrid", UIGrid)
RegistChildComponent(LuaAppearanceFashionSuitSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)
RegistChildComponent(LuaAppearanceFashionSuitSubview, "m_AppearanceFashionTabBar", "AppearanceFashionTabBar", CCommonLuaScript)
RegistChildComponent(LuaAppearanceFashionSuitSubview, "m_SortSelector", "SortSelector", CCustomSelector)

RegistClassMember(LuaAppearanceFashionSuitSubview, "m_SortNames")
RegistClassMember(LuaAppearanceFashionSuitSubview, "m_SortIndex")
RegistClassMember(LuaAppearanceFashionSuitSubview, "m_AllFashionSuits")
RegistClassMember(LuaAppearanceFashionSuitSubview, "m_SelectedDataId")
RegistClassMember(LuaAppearanceFashionSuitSubview, "m_GroupFashionData")

function LuaAppearanceFashionSuitSubview:Awake()
end

function LuaAppearanceFashionSuitSubview:InitSortSelector()
    self.m_SortSelector.gameObject:SetActive(true)
    self.m_SortNames = LuaAppearancePreviewMgr:GetAllFashionSuitInfoSortNames()
    local names = Table2List(self.m_SortNames, MakeGenericClass(List,cs_string))
    self.m_SortSelector:Init(names, nil, DelegateFactory.Action_int(function ( index )
        self.m_SortIndex = index + 1
		self.m_SortSelector:SetName(names[index])
        self:LoadData()
    end))
    self.m_SortIndex = 1
    self.m_SortSelector:SetName(self.m_SortNames[self.m_SortIndex])
end

function LuaAppearanceFashionSuitSubview:Init()
    self:InitSortSelector()
    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceFashionSuitSubview:LoadData()
    self.m_AllFashionSuits = LuaAppearancePreviewMgr:GetAllFashionSuitInfo(self.m_SortIndex)
    Extensions.RemoveAllChildren(self.m_ContentGrid.transform)
    self.m_ContentGrid.gameObject:SetActive(true)

    for i = 1, #self.m_AllFashionSuits do
        local child = CUICommonDef.AddChild(self.m_ContentGrid.gameObject, self.m_FashionItem)
        child:SetActive(true)
        self:InitItem(child, self.m_AllFashionSuits[i])
    end
   
    self.m_ContentGrid:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceFashionSuitSubview:SetDefaultSelection()
    local previewId = LuaAppearancePreviewMgr.m_PreviewFashionSuitId
    local bShowSuit = LuaAppearancePreviewMgr:IsShowFashion(EnumAppearanceFashionPosition.eHead, false) and LuaAppearancePreviewMgr:IsShowFashion(EnumAppearanceFashionPosition.eBody, false)
    self.m_SelectedDataId = previewId>0 and previewId or (bShowSuit and LuaAppearancePreviewMgr:GetCurrentInUseFashionSuit() or 0)
end

function LuaAppearanceFashionSuitSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end

    local childCount = self.m_ContentGrid.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        local appearanceData = self.m_AllFashionSuits[i+1]
        if appearanceData and self:IsSelectedFashionOrGroup(self.m_SelectedDataId, appearanceData) then
            self:OnItemClick(childGo)
            break
        end
    end
end

function LuaAppearanceFashionSuitSubview:GetGroupIndexById(id, appearanceData)
    if id and appearanceData and #appearanceData.fashionTbl>0 then
        for i=1,#appearanceData.fashionTbl do
            if id==appearanceData.fashionTbl[i].id then
                return i 
            end
        end
    end
    return -1
end

function LuaAppearanceFashionSuitSubview:IsSelectedFashionOrGroup(id, appearanceData)
    return id and appearanceData and ((id==appearanceData.id) or self:GetGroupIndexById(id, appearanceData)>0)
end

--对于多色的时装,依次选择预览款、已装备、已解锁、第一个
function LuaAppearanceFashionSuitSubview:GetDefaultSelectedFashionSuit(appearanceData)
    if #appearanceData.fashionTbl>0 then
        local firstId = appearanceData.fashionTbl[1].id
        local usingId = 0
        local firstOwnedId = 0
        for __,data in pairs(appearanceData.fashionTbl) do
            if data.id == LuaAppearancePreviewMgr.m_PreviewFashionSuitId then
                return data
            elseif data.isInUse then
                usingId = data.id
            elseif data.isOwned and firstOwnedId==0 then
                firstOwnedId = data.id
            end
        end
        if usingId==0 then usingId = firstOwnedId end
        if usingId==0 then usingId = firstId end
        local defaultSelectedIdx = self:GetGroupIndexById(usingId,appearanceData)
        if defaultSelectedIdx<1 then defaultSelectedIdx = 1 end
        return appearanceData.fashionTbl[defaultSelectedIdx]
    end
    return appearanceData
end
--判断是否存在已经解锁并可用的套装
function LuaAppearanceFashionSuitSubview:AvaliableFashionSuitExists(appearanceData)
    if #appearanceData.fashionTbl>0 then
        for __,data in pairs(appearanceData.fashionTbl) do
            if self:IsOneFashionSuitAvailable(data) then return true end   
        end
    else
        return self:IsOneFashionSuitAvailable(appearanceData)
    end
    return false
end

function LuaAppearanceFashionSuitSubview:IsOneFashionSuitAvailable(appearanceData)
    if LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(appearanceData.bodyId) then
        if LuaAppearancePreviewMgr:GetEntiretyHead(appearanceData.bodyId)>0 or LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(appearanceData.headId) then
            return true
        end
    end
    return false
end

--判断是否存在新品推荐套装
function LuaAppearanceFashionSuitSubview:NewDisplayFashionSuitExists(appearanceData)
    if #appearanceData.fashionTbl>0 then
        for __,data in pairs(appearanceData.fashionTbl) do
            if data.bShowNewDisplay then
                return true
            end
        end
    else
        return appearanceData.bShowNewDisplay
    end
    return false
end

function LuaAppearanceFashionSuitSubview:InitItem(itemGo, appearanceData)
    local iconTexture = itemGo.transform:GetComponent(typeof(CUITexture))
    local borderSprite = itemGo.transform:Find("Border"):GetComponent(typeof(UISprite))
    local cornerGo = itemGo.transform:Find("Corner").gameObject
    local cornerNewGo = itemGo.transform:Find("CornerNew").gameObject
    local disableGo = itemGo.transform:Find("Disabled").gameObject
    local selectedGo = itemGo.transform:Find("Selected").gameObject
    local grid = itemGo.transform:Find("Grid"):GetComponent(typeof(UIGrid))
    local expressionGo = itemGo.transform:Find("Grid/ExpressionIcon").gameObject
    local multipleStyleGo = itemGo.transform:Find("Grid/MultipleStyleIcon").gameObject
    local transformGo = itemGo.transform:Find("Grid/TransformIcon").gameObject
    local progressSprite = itemGo.transform:Find("Progress"):GetComponent(typeof(UIWidget))
    local tryOnGo = itemGo.transform:Find("TryOnIcon").gameObject

    local icon = appearanceData.bigIcon --使用默认的图标，不需要选择当前选中的那个
    appearanceData = self:GetDefaultSelectedFashionSuit(appearanceData)

    iconTexture:LoadMaterial(icon)
    borderSprite.spriteName = LuaAppearancePreviewMgr:GetFashionQualityBorder(appearanceData.quality)
    cornerGo:SetActive(appearanceData.isInUse)
    cornerNewGo:SetActive(self:NewDisplayFashionSuitExists(appearanceData) and not appearanceData.isInUse)
    disableGo:SetActive(not self:AvaliableFashionSuitExists(appearanceData))
    selectedGo:SetActive(self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id or false)
    expressionGo:SetActive(appearanceData.specialExpression>0)
    multipleStyleGo:SetActive(appearanceData.groupId>0)
    transformGo:SetActive(appearanceData.transformFemale>0)
    grid:Reposition()
    progressSprite.gameObject:SetActive(false)
    tryOnGo:SetActive(LuaAppearancePreviewMgr.m_PreviewFashionSuitId == appearanceData.id)

    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go, true)
    end)
end

function LuaAppearanceFashionSuitSubview:OnItemClick(go, isClick)
    self.m_AppearanceFashionTabBar.gameObject:SetActive(false)
    self.m_GroupFashionData = nil
    local childCount = self.m_ContentGrid.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        local tryOnGo = childGo.transform:Find("TryOnIcon").gameObject
        if childGo == go then
            childGo.transform:Find("Selected").gameObject:SetActive(true)
            tryOnGo:SetActive(true)
            if #self.m_AllFashionSuits[i+1].fashionTbl>0 then
                self.m_GroupFashionData = self.m_AllFashionSuits[i+1]
            else
                self.m_SelectedDataId = self.m_AllFashionSuits[i+1].id
            end
        else
            childGo.transform:Find("Selected").gameObject:SetActive(false)
            tryOnGo:SetActive(false)
        end
    end

    if self.m_GroupFashionData then
        local ret = self:GetDefaultSelectedFashionSuit(self.m_GroupFashionData)
        local defaultSelectedIdx = 1
        for idx,data in pairs(self.m_GroupFashionData.fashionTbl) do
            if ret==data then
                defaultSelectedIdx = idx
                break
            end
        end 
        self.m_SelectedDataId = ret.id
        self.m_AppearanceFashionTabBar.gameObject:SetActive(true)
        self.m_AppearanceFashionTabBar:Init(self.m_GroupFashionData.fashionTbl, function(go, index, isClick) self:OnSpecialAppearTabChange(go, index, isClick) end)
        self.m_AppearanceFashionTabBar:ChangeTab(defaultSelectedIdx-1, false, isClick)      
    else
        self:UpdateItemDisplay(isClick)
    end
end

function LuaAppearanceFashionSuitSubview:OnSpecialAppearTabChange(go, index, isClick)
    self.m_SelectedDataId = self.m_GroupFashionData.fashionTbl[index+1].id
    -- g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerTalismanAppearance", appear.id)
    -- if not LuaAppearancePreviewMgr:IsShowTalismanAppear() then
    --     g_MessageMgr:ShowMessage("Appearance_Preview_Hide_Talisman_Appearance_Tip")
    -- end
    self:UpdateItemDisplay(isClick)
end

function LuaAppearanceFashionSuitSubview:UpdateItemDisplay(isClick)
    self.m_ItemDisplay.gameObject:SetActive(true)
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    if id==0 then
        self.m_ItemDisplay:Clear()
        g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerFashionSuit", 0, 0, 0)
        return
    end
    local appearanceData = self:GetSelectedAppearanceData()
    g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerFashionSuit", appearanceData.id, appearanceData.headId, appearanceData.bodyId)
    if id>0 and isClick then
        LuaAppearancePreviewMgr:CheckAndShowHideFashionTip(EnumAppearanceFashionPosition.eHead)
        LuaAppearancePreviewMgr:CheckAndShowHideFashionTip(EnumAppearanceFashionPosition.eBody)
    end
    local icon = appearanceData.icon
    local disabled = true
    local locked = true
    local buttonTbl = {}
    local qualityBgColor = LuaAppearancePreviewMgr:GetFashionQualityColor(appearanceData.quality)
    local qualityText = SafeStringFormat3(LocalString.GetString("%s 风尚度+%d"), appearanceData.qualityName, appearanceData.fashionability)
    local available = self:IsOneFashionSuitAvailable(appearanceData)
    local inUse = LuaAppearancePreviewMgr:IsCurrentInUseFashion(EnumAppearanceFashionPosition.eHead, appearanceData.headId) and LuaAppearancePreviewMgr:IsCurrentInUseFashion(EnumAppearanceFashionPosition.eBody, appearanceData.bodyId)
    local bShowProgress = false
    local curVal = 0
    local needVal = 1
    disabled = not available
    locked = not available
    --只显示更换和获取
    if available and not inUse then
        table.insert(buttonTbl, {text=LocalString.GetString("更换"), isYellow=false, action=function(go) self:OnApplyButtonClick() end})
    elseif inUse then
        table.insert(buttonTbl, {text=LocalString.GetString("脱下"), isYellow=false, action=function(go) self:OnTakeOffButtonClick() end})
    else
        table.insert(buttonTbl, {text=LocalString.GetString("获取"), isYellow=true, action=function(go) self:OnMoreButtonClick(go) end})
    end
    self.m_ItemDisplay:Init(icon, appearanceData.name, qualityBgColor, qualityText, bShowProgress, curVal, needVal, disabled, locked, buttonTbl)
end

function LuaAppearanceFashionSuitSubview:GetSelectedAppearanceData()
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    local appearanceData = nil
    for i=1,#self.m_AllFashionSuits do
        if self:IsSelectedFashionOrGroup(self.m_SelectedDataId, self.m_AllFashionSuits[i]) then
            if #self.m_AllFashionSuits[i].fashionTbl>0 then
                local index = self:GetGroupIndexById(self.m_SelectedDataId, self.m_AllFashionSuits[i])
                appearanceData = self.m_AllFashionSuits[i].fashionTbl[index]
            else
                appearanceData = self.m_AllFashionSuits[i]
            end
            break
        end
    end
    return appearanceData
end


function LuaAppearanceFashionSuitSubview:OnTakeOffButtonClick()
    local appearanceData = self:GetSelectedAppearanceData()
    LuaAppearancePreviewMgr:RequestTakeOffFashionSuit_Permanent(appearanceData.id)
end

function LuaAppearanceFashionSuitSubview:OnApplyButtonClick()
    local appearanceData = self:GetSelectedAppearanceData()
    LuaAppearancePreviewMgr:RequestTakeOnFashionSuit_Permanent(appearanceData.id)
end

function LuaAppearanceFashionSuitSubview:OnMoreButtonClick(go)
    local appearanceData = self:GetSelectedAppearanceData()
    local itemGetId = nil
    --缺哪个就去Fashion表找ItemGet，如果两个都缺，优先找Body的ItemGet
    if not LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(appearanceData.bodyId) then
        itemGetId = appearanceData.bodyItemGetId
    elseif not LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(appearanceData.headId) then
        itemGetId = appearanceData.headItemGetId
    end
    if not itemGetId then return end
    LuaItemAccessListMgr:ShowItemAccessInfoAtLeft(itemGetId, true, go.transform)
end

function LuaAppearanceFashionSuitSubview:OnEnable()
    g_ScriptEvent:AddListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:AddListener("OnSyncWardrobeProperty",self,"OnSyncWardrobeProperty")
    g_ScriptEvent:AddListener("OnTakeOffFashionSuit_Permanent",self,"OnTakeOffFashionSuit_Permanent")
    g_ScriptEvent:AddListener("OnSetFashionHideSuccess_Permanent",self,"OnSetFashionHideSuccess_Permanent")
end

function LuaAppearanceFashionSuitSubview:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:RemoveListener("OnSyncWardrobeProperty",self,"OnSyncWardrobeProperty")
    g_ScriptEvent:RemoveListener("OnSetFashionHideSuccess_Permanent",self,"OnSetFashionHideSuccess_Permanent")
    g_ScriptEvent:RemoveListener("OnTakeOffFashionSuit_Permanent",self,"OnTakeOffFashionSuit_Permanent")
end

--仙凡切换 或其他sync appearance情况
function LuaAppearanceFashionSuitSubview:SyncMainPlayerAppearancePropUpdate()
    self:LoadData()
end

function LuaAppearanceFashionSuitSubview:OnSyncWardrobeProperty(reason)
    self:LoadData()
end

function LuaAppearanceFashionSuitSubview:OnTakeOffFashionSuit_Permanent(suitId)
    if LuaAppearancePreviewMgr.m_PreviewFashionSuitId==suitId then
        LuaAppearancePreviewMgr.m_PreviewFashionSuitId = 0
        LuaAppearancePreviewMgr.m_PreviewHeadFashionId = 0
        LuaAppearancePreviewMgr.m_PreviewBodyFashionId = 0
        self:SetDefaultSelection()
        self:LoadData()
    end
end

function LuaAppearanceFashionSuitSubview:OnSetFashionHideSuccess_Permanent(pos,value)
    local bHide = value>0
    if bHide then
        if pos == EnumAppearanceFashionPosition.eHead then
            LuaAppearancePreviewMgr.m_PreviewHeadFashionId = 0
            LuaAppearancePreviewMgr.m_PreviewFashionSuitId = 0
        elseif pos == EnumAppearanceFashionPosition.eBody then
            LuaAppearancePreviewMgr.m_PreviewBodyFashionId = 0
            LuaAppearancePreviewMgr.m_PreviewFashionSuitId = 0
        end
    end
    self:SetDefaultSelection()
    self:LoadData()
end