local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local Item_Item              = import "L10.Game.Item_Item"
local Animation              = import "UnityEngine.Animation"

LuaValentine2023LGTMResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

function LuaValentine2023LGTMResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end


function LuaValentine2023LGTMResultWnd:Init()
    local animation = self.transform:GetComponent(typeof(Animation))
    animation:Play(LuaValentine2023Mgr.LGTMInfo.isWin and "common_result_win" or "common_result_lose")
    self:InitAward()
end

-- 奖励
function LuaValentine2023LGTMResultWnd:InitAward()
    if not LuaValentine2023Mgr.LGTMInfo.isWin then
        return
    end

    local winInfo = self.transform:Find("Info/WinStyle/Win_Info")
    local grid = winInfo:Find("Grid"):GetComponent(typeof(UIGrid))
    local template = winInfo:Find("Template").gameObject
    template:SetActive(false)

    local str = "21031812,1"
    Extensions.RemoveAllChildren(grid.transform)
    for itemId, count in string.gmatch(str, "(%d+),(%d+)") do
        local child = NGUITools.AddChild(grid.gameObject, template)
        child.gameObject:SetActive(true)
        local qnReturnAward = child:GetComponent(typeof(CQnReturnAwardTemplate))
        qnReturnAward:Init(Item_Item.GetData(tonumber(itemId)), tonumber(count))
    end
    grid:Reposition()
end

--@region UIEvent
--@endregion UIEvent
