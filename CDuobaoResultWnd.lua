-- Auto Generated!!
local CDuoBaoMgr = import "L10.UI.CDuoBaoMgr"
local CDuobaoResultWnd = import "L10.UI.CDuobaoResultWnd"
local CommonDefs = import "L10.Game.CommonDefs"
CDuobaoResultWnd.m_Init_CS2LuaHook = function (this) 
    this.m_Table.m_DataSource = this
    this.m_PageButton:SetMinMax(1, 1, 1)
    this.m_PageButton:OverrideText("1/1")
    this.m_NoShopItemLabel.gameObject:SetActive(false)
    this:DoSearch(1)
end
CDuobaoResultWnd.m_DoSearch_CS2LuaHook = function (this, page)
    this.m_CurrentPage = page
    this.m_PageButton:OverrideText(System.String.Format("{0}/{1}", this.m_CurrentPage, this.m_TotalPages))
    local startIndex = ((this.m_CurrentPage - 1) * CDuobaoResultWnd.m_CountPerPage + 1)
    local endIndex = (this.m_CurrentPage * CDuobaoResultWnd.m_CountPerPage)
    --清空数据再查询
    CommonDefs.ListClear(this.m_SearchResultData)
    this.m_Table:ReloadData(false, false)

    CDuoBaoMgr.Inst:QueryDuoBaoResultInfo(startIndex, endIndex)
end
CDuobaoResultWnd.m_OnReplyDuoBaoResultEnd_CS2LuaHook = function (this) 
    this.m_TotalCount = CDuoBaoMgr.Inst.TotalResultCount
    this.m_TotalPages = math.ceil(1 * this.m_TotalCount / CDuobaoResultWnd.m_CountPerPage)
    if this.m_TotalPages <= 0 then
        this.m_TotalPages = 1
    end
    this.m_PageButton:SetMinMax(1, this.m_TotalPages, 1)
    this.m_PageButton:OverrideText(System.String.Format("{0}/{1}", this.m_CurrentPage, this.m_TotalPages))

    this.m_SearchResultData = CDuoBaoMgr.Inst.DuobaoResultInfos
    this.m_Table:ReloadData(true, false)
    this.m_SelectItemIndex = - 1
    this.m_NoShopItemLabel:SetActive(this.m_SearchResultData.Count == 0)
end
