-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuildTrainItemCell = import "L10.UI.CGuildTrainItemCell"
local CGuildTrainMgr = import "L10.Game.CGuildTrainMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Practice_Practice = import "L10.Game.Practice_Practice"
local UInt32 = import "System.UInt32"
CGuildTrainItemCell.m_Init_CS2LuaHook = function (this, _categoryIndex, _itemIndex) 
    this.nameLabel.text = ""
    this.progress.gameObject:SetActive(false)
    this.levelLabel.text = ""
    this.notOpen:SetActive(false)
    if _categoryIndex < 0 or _categoryIndex >= CGuildTrainMgr.Inst.trainCategoryList.Count then
        return
    end
    if _itemIndex < 0 or _itemIndex >= CGuildTrainMgr.Inst.trainCategoryList[_categoryIndex].trainItemList.Count then
        return
    end
    if CClientMainPlayer.Inst == nil then
        return
    end
    this.categoryIndex = _categoryIndex
    this.itemIndex = _itemIndex
    this.baseInfo = CGuildTrainMgr.Inst.trainCategoryList[this.categoryIndex].trainItemList[this.itemIndex]
    this.nameLabel.text = this.baseInfo.name
    local trainId = this:GetTrainId()
    if this.baseInfo.hasSelected then
        this.progress.gameObject:SetActive(true)
        this:InitMaxLevel(trainId)
        this:InitProgress(trainId)
    else
        this.notOpen:SetActive(true)
    end
end
CGuildTrainItemCell.m_GetTrainId_CS2LuaHook = function (this) 
    local trainId = 0
    if CommonDefs.DictContains(CClientMainPlayer.Inst.SkillProp.PracticeLevel, typeof(UInt32), EnumToInt(this.baseInfo.type)) then
        trainId = this.baseInfo.skillId * 100 + CommonDefs.DictGetValue(CClientMainPlayer.Inst.SkillProp.PracticeLevel, typeof(UInt32), EnumToInt(this.baseInfo.type)) + 1
        this.PracticeSkillId = trainId - 1
    else
        trainId = this.baseInfo.skillId * 100 + 1
        this.PracticeSkillId = trainId
    end
    return trainId
end
CGuildTrainItemCell.m_InitMaxLevel_CS2LuaHook = function (this, trainId) 
    this.practice = Practice_Practice.GetData(trainId)
    if this.practice == nil then
        this.progress.value = 1
        this.levelLabel.text = System.String.Format("lv.{0}", CGuildTrainItemCell.constMaxLevel)
        this.maxLevel = CGuildTrainItemCell.constMaxLevel
        return
    end
    do
        local i = 2
        while i < CClientMainPlayer.Inst.MaxLevel do
            local practiceId = this.baseInfo.skillId * 100 + i
            local p = Practice_Practice.GetData(practiceId)
            if p == nil then
                break
            end
            if p.PlayerLearnLv <= CClientMainPlayer.Inst.MaxLevel then
                this.maxLevel = p.Level
            else
                break
            end
            i = i + 1
        end
    end
end
CGuildTrainItemCell.m_InitProgress_CS2LuaHook = function (this, trainId) 
    this.practice = Practice_Practice.GetData(trainId)
    if this.practice == nil then
        return
    end
    if this.baseInfo.hasSelected then
        this.levelLabel.text = System.String.Format("lv.{0}", this.practice.Level - 1)
    end
    if CommonDefs.DictContains(CClientMainPlayer.Inst.SkillProp.PracticeExp, typeof(UInt32), EnumToInt(this.baseInfo.type)) then
        this.progress.value = CommonDefs.DictGetValue(CClientMainPlayer.Inst.SkillProp.PracticeExp, typeof(UInt32), EnumToInt(this.baseInfo.type)) / this.practice.InExperience
    else
        this.progress.value = 0
    end
end
