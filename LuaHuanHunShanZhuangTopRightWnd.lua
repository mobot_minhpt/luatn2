local CUIFx = import "L10.UI.CUIFx"

local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

local QnButton = import "L10.UI.QnButton"
local Color = import "UnityEngine.Color"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CButton = import "L10.UI.CButton"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local BoxCollider = import "UnityEngine.BoxCollider"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CPos = import "L10.Engine.CPos"
local CScene = import "L10.Game.CScene"
local Utility = import "L10.Engine.Utility"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CTopAndRightTipWnd = import "L10.UI.CTopAndRightTipWnd"
local CScreenCaptureWnd = import "L10.UI.CScreenCaptureWnd"

LuaHuanHunShanZhuangTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHuanHunShanZhuangTopRightWnd, "ExpandButton", "ExpandButton", CButton)
RegistChildComponent(LuaHuanHunShanZhuangTopRightWnd, "Btn1", "Btn1", QnButton)
RegistChildComponent(LuaHuanHunShanZhuangTopRightWnd, "Btn2", "Btn2", QnButton)
RegistChildComponent(LuaHuanHunShanZhuangTopRightWnd, "Btn3", "Btn3", QnButton)
RegistChildComponent(LuaHuanHunShanZhuangTopRightWnd, "Btn4", "Btn4", QnButton)
RegistChildComponent(LuaHuanHunShanZhuangTopRightWnd, "Btn5", "Btn5", QnButton)
RegistChildComponent(LuaHuanHunShanZhuangTopRightWnd, "ShengSiMen", "ShengSiMen", GameObject)
RegistChildComponent(LuaHuanHunShanZhuangTopRightWnd, "YinYangZhen", "YinYangZhen", GameObject)
RegistChildComponent(LuaHuanHunShanZhuangTopRightWnd, "LeftLabel", "LeftLabel", UILabel)
RegistChildComponent(LuaHuanHunShanZhuangTopRightWnd, "RightLabel", "RightLabel", UILabel)
RegistChildComponent(LuaHuanHunShanZhuangTopRightWnd, "YinYangZhenFx", "YinYangZhenFx", CUIFx)

--@endregion RegistChildComponent end

RegistClassMember(LuaHuanHunShanZhuangTopRightWnd, "m_Panel")
RegistClassMember(LuaHuanHunShanZhuangTopRightWnd, "m_UpdateTick")

function LuaHuanHunShanZhuangTopRightWnd:Awake()
    self.m_Panel = self.transform:GetComponent(typeof(UIPanel))
    self.m_Panel.alpha = CScreenCaptureWnd.IsInCaptureMode and 0 or 1

    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)


	
	UIEventListener.Get(self.Btn1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtn1Click()
	end)


	
	UIEventListener.Get(self.Btn2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtn2Click()
	end)


	
	UIEventListener.Get(self.Btn3.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtn3Click()
	end)


	
	UIEventListener.Get(self.Btn4.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtn4Click()
	end)


	
	UIEventListener.Get(self.Btn5.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtn5Click()
	end)


    --@endregion EventBind end
end

function LuaHuanHunShanZhuangTopRightWnd:Init()
    self:CancelUpdateTick()
    self.YinYangZhenFx:DestroyFx()
    if LuaHuanHunShanZhuangMgr.m_Stage == 1 then
        self.YinYangZhen:SetActive(true)
        self.ShengSiMen:SetActive(false)
        self.LeftLabel.text = LuaHuanHunShanZhuangMgr.YinNum
        self.RightLabel.text = LuaHuanHunShanZhuangMgr.YangNum
        if not CScreenCaptureWnd.IsInCaptureMode then
            self.YinYangZhenFx:LoadFx("Fx/UI/Prefab/UI_yinyang_chixu.prefab")
        end
    elseif LuaHuanHunShanZhuangMgr.m_Stage == 3 then
        self.YinYangZhen:SetActive(false)
        self.ShengSiMen:SetActive(true)
    else
        self.YinYangZhen:SetActive(false)
        self.ShengSiMen:SetActive(false)
    end
    self:UpdateTickFunc()
    self.m_UpdateTick = RegisterTick(function()
        self:UpdateTickFunc()
    end, 1000)
end

function LuaHuanHunShanZhuangTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncHuanHunShanZhuangPlayStageChange", self, "SyncHuanHunShanZhuangPlayStageChange")
    g_ScriptEvent:AddListener("SendHuanHunShanZhuangShengSiMen", self, "UpdateTickFunc")
    g_ScriptEvent:AddListener("SyncHuanHunShanZHuangYinYang", self, "SyncYinYangZhen")
    g_ScriptEvent:AddListener("CaptureModeOnOff", self, "CaptureModeOnOff")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")

end

function LuaHuanHunShanZhuangTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncHuanHunShanZhuangPlayStageChange", self, "SyncHuanHunShanZhuangPlayStageChange")
    g_ScriptEvent:RemoveListener("SendHuanHunShanZhuangShengSiMen", self, "UpdateTickFunc")
    g_ScriptEvent:RemoveListener("SyncHuanHunShanZHuangYinYang", self, "SyncYinYangZhen")
    g_ScriptEvent:RemoveListener("CaptureModeOnOff", self, "CaptureModeOnOff")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
end

function LuaHuanHunShanZhuangTopRightWnd:OnDestroy()
    self:CancelUpdateTick()
end

--@region UIEvent

function LuaHuanHunShanZhuangTopRightWnd:OnBtn1Click()
    self:OnBtnClick(1)
end

function LuaHuanHunShanZhuangTopRightWnd:OnBtn2Click()
    self:OnBtnClick(2)
end

function LuaHuanHunShanZhuangTopRightWnd:OnBtn3Click()
    self:OnBtnClick(3)
end

function LuaHuanHunShanZhuangTopRightWnd:OnBtn4Click()
    self:OnBtnClick(4)
end

function LuaHuanHunShanZhuangTopRightWnd:OnBtn5Click()
    self:OnBtnClick(5)
end

function LuaHuanHunShanZhuangTopRightWnd:OnExpandButtonClick()
    LuaUtils.SetLocalRotation(self.ExpandButton.transform, 0, 0, 0)
	CTopAndRightTipWnd.showPackage = false
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end


--@endregion UIEvent

function LuaHuanHunShanZhuangTopRightWnd:OnBtnClick(i)
    local flag = LuaHuanHunShanZhuangMgr.m_Flags[i]
    if flag then
        if flag.isTrigger then
            g_MessageMgr:ShowMessage("HuanHunShanZhuang_FaZhen_IsTrigger")
        elseif flag.isFound or flag.isInView then
            local pos = CPos(flag.x, flag.y)
            pos = Utility.GridPos2PixelPos(pos)
            CTrackMgr.Inst:Track(pos, Utility.Grid2Pixel(0), nil, nil)
        end
    end
end

function LuaHuanHunShanZhuangTopRightWnd:UpdateTickFunc()
    self.m_Panel.depth = 1
    if not LuaHuanHunShanZhuangMgr.m_Stage == 3 then
        return
    end
    for i = 1, 5 do
        local btn = self["Btn"..i]
        local flag = LuaHuanHunShanZhuangMgr.m_Flags[i]
        if flag then
            if flag.isTrigger then
                btn:GetComponent(typeof(BoxCollider)).enabled = true
                btn.transform:Find("Selected").gameObject:SetActive(true)
                btn.transform:Find("Normal").gameObject:SetActive(true)
            else
                if flag.isFound then
                    btn:GetComponent(typeof(BoxCollider)).enabled = true
                    btn.transform:Find("Selected").gameObject:SetActive(false)
                    btn.transform:Find("Normal").gameObject:SetActive(true)
                else
                    flag.isInView = self:CheckFlagIsInView(CClientObjectMgr.Inst:GetObject(flag.engineId))
                    if flag.isInView then
                        Gac2Gas.HuanHunShanZhuangPlayerFindZhengYan(i, flag.engineId)
                        btn:GetComponent(typeof(BoxCollider)).enabled = true
                        btn.transform:Find("Selected").gameObject:SetActive(false)
                        btn.transform:Find("Normal").gameObject:SetActive(true)
                    else
                        btn:GetComponent(typeof(BoxCollider)).enabled = false
                        btn.transform:Find("Selected").gameObject:SetActive(false)
                        btn.transform:Find("Normal").gameObject:SetActive(false)
                    end
                end
            end
        end
    end
end

function LuaHuanHunShanZhuangTopRightWnd:CheckFlagIsInView(obj)
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.RO and obj and obj.RO then -- 能拿到对象就是进入视野了
        return true
        --[[
        local dis = Vector3.Distance(CClientMainPlayer.Inst.RO.transform.localPosition, obj.RO.transform.localPosition)
        if dis <= 6 then
            return true
        end
        --]]
    end
    return false
end

function LuaHuanHunShanZhuangTopRightWnd:CaptureModeOnOff(onoff)
    self.m_Panel.alpha = onoff and 0 or 1
    if LuaHuanHunShanZhuangMgr.m_Stage == 1 then
        if onoff then
            self.YinYangZhenFx:DestroyFx()
        else
            self.YinYangZhenFx:LoadFx("Fx/UI/Prefab/UI_yinyang_chixu.prefab")
        end
    end
end

function LuaHuanHunShanZhuangTopRightWnd:OnHideTopAndRightTipWnd()
    LuaUtils.SetLocalRotation(self.ExpandButton.transform, 0, 0, 180)
end

function LuaHuanHunShanZhuangTopRightWnd:CancelUpdateTick()
    if self.m_UpdateTick then
        invoke(self.m_UpdateTick)
        self.m_UpdateTick = nil
    end
end

function LuaHuanHunShanZhuangTopRightWnd:SyncYinYangZhen(yin, yang)
    self.LeftLabel.text = yin
    self.RightLabel.text = yang
end

function LuaHuanHunShanZhuangTopRightWnd:SyncHuanHunShanZhuangPlayStageChange(stage, isShowFx)
    if stage == 2 then
        if isShowFx then
            self.YinYangZhenFx:LoadFx("Fx/UI/Prefab/UI_yinyang_tixing.prefab")
        end
    end
end
