local CMessageTipMgr = import "L10.UI.CMessageTipMgr"

LuaShanYaoJieYuanStoryWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaShanYaoJieYuanStoryWnd, "title")
RegistClassMember(LuaShanYaoJieYuanStoryWnd, "story")

function LuaShanYaoJieYuanStoryWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
end

function LuaShanYaoJieYuanStoryWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.title = anchor:Find("Title"):GetComponent(typeof(UILabel))
    self.story = anchor:Find("Story"):GetComponent(typeof(UILabel))
end


function LuaShanYaoJieYuanStoryWnd:Init()
    local msg =	g_MessageMgr:FormatMessage(LuaXinBaiLianDongMgr.SYJYStoryMsg)
    if System.String.IsNullOrEmpty(msg) then return end

    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if not info then return end

    self.title.text = info.title

    local story = ""
    do
		local i = 0
		while i < info.paragraphs.Count do
            local interval = i == 0 and "        " or "\n        "
            story = SafeStringFormat3("%s%s%s", story, interval, info.paragraphs[i])
			i = i + 1
		end
	end
    self.story.text = story
end

--@region UIEvent
--@endregion UIEvent
