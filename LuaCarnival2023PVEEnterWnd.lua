local CButton                = import "L10.UI.CButton"
local UITabBar               = import "L10.UI.UITabBar"
local CMessageTipMgr         = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem      = import "L10.UI.CTipParagraphItem"
local CServerTimeMgr         = import "L10.Game.CServerTimeMgr"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local Item_Item              = import "L10.Game.Item_Item"
local QnTabButton            = import "L10.UI.QnTabButton"
local Animation              = import "UnityEngine.Animation"

LuaCarnival2023PVEEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCarnival2023PVEEnterWnd, "needTable", "NeedTable", UITable)
RegistChildComponent(LuaCarnival2023PVEEnterWnd, "time", "Time", UILabel)
RegistChildComponent(LuaCarnival2023PVEEnterWnd, "level", "Level", UILabel)
RegistChildComponent(LuaCarnival2023PVEEnterWnd, "textTable", "TextTable", UITable)
RegistChildComponent(LuaCarnival2023PVEEnterWnd, "textTemplate", "TextTemplate", GameObject)
RegistChildComponent(LuaCarnival2023PVEEnterWnd, "addPointButton", "AddPointButton", CButton)
RegistChildComponent(LuaCarnival2023PVEEnterWnd, "pointNum", "PointNum", UILabel)
RegistChildComponent(LuaCarnival2023PVEEnterWnd, "skillGrid", "SkillGrid", UIGrid)
RegistChildComponent(LuaCarnival2023PVEEnterWnd, "skillTemplate", "SkillTemplate", GameObject)
RegistChildComponent(LuaCarnival2023PVEEnterWnd, "modeTabs", "Tabs", UITabBar)
RegistChildComponent(LuaCarnival2023PVEEnterWnd, "notOpen", "NotOpen", UILabel)
RegistChildComponent(LuaCarnival2023PVEEnterWnd, "daily", "Daily", Transform)
RegistChildComponent(LuaCarnival2023PVEEnterWnd, "challenge", "Challenge", Transform)
--@endregion RegistChildComponent end

RegistClassMember(LuaCarnival2023PVEEnterWnd, "isWeekend")

RegistClassMember(LuaCarnival2023PVEEnterWnd, "teamLabel")
RegistClassMember(LuaCarnival2023PVEEnterWnd, "battleLabel")
RegistClassMember(LuaCarnival2023PVEEnterWnd, "awardLabel")
RegistClassMember(LuaCarnival2023PVEEnterWnd, "awardGrid")

RegistClassMember(LuaCarnival2023PVEEnterWnd, "tab1TimeLabel")
RegistClassMember(LuaCarnival2023PVEEnterWnd, "tab2TimeLabel")
RegistClassMember(LuaCarnival2023PVEEnterWnd, "tab1Button")
RegistClassMember(LuaCarnival2023PVEEnterWnd, "skillRoot")
RegistClassMember(LuaCarnival2023PVEEnterWnd, "anim")

RegistClassMember(LuaCarnival2023PVEEnterWnd, "rankNumLabel")
RegistClassMember(LuaCarnival2023PVEEnterWnd, "dailyRewardTimesLabel")
RegistClassMember(LuaCarnival2023PVEEnterWnd, "challengeRewardTimesLabel")

function LuaCarnival2023PVEEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.addPointButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddPointButtonClick()
	end)
    --@endregion EventBind end

	self.anim = self.transform:GetComponent(typeof(Animation))

	local detailTable = self.transform:Find("Anchor/ModeSelect/tab_neirong/DetailTable")
	self.teamLabel = detailTable:Find("Team/Label"):GetComponent(typeof(UILabel))
	self.battleLabel = detailTable:Find("Battle/Label"):GetComponent(typeof(UILabel))
	self.awardLabel = detailTable:Find("Award/Label"):GetComponent(typeof(UILabel))
	self.awardGrid = detailTable:Find("Award/Grid"):GetComponent(typeof(UIGrid))
	self.textTemplate:SetActive(false)

	self.tab1TimeLabel = self.modeTabs.transform:Find("Tab1/Time"):GetComponent(typeof(UILabel))
	self.tab2TimeLabel = self.modeTabs.transform:Find("Tab2/Time"):GetComponent(typeof(UILabel))
	self.tab1Button = self.modeTabs.transform:Find("Tab1"):GetComponent(typeof(QnTabButton))
	self.skillTemplate:SetActive(false)
	self.skillRoot = self.transform:Find("Anchor/Skill").gameObject

	self.rankNumLabel = self.challenge:Find("RankNum"):GetComponent(typeof(UILabel))
	self.dailyRewardTimesLabel = self.daily:Find("RewardTimes"):GetComponent(typeof(UILabel))
	self.challengeRewardTimesLabel = self.challenge:Find("RewardTimes"):GetComponent(typeof(UILabel))
end

function LuaCarnival2023PVEEnterWnd:OnEnable()
	g_ScriptEvent:AddListener("Carnival2023RequestPlayDataResult", self, "OnCarnival2023RequestPlayDataResult")
end

function LuaCarnival2023PVEEnterWnd:OnDisable()
	g_ScriptEvent:RemoveListener("Carnival2023RequestPlayDataResult", self, "OnCarnival2023RequestPlayDataResult")
end

function LuaCarnival2023PVEEnterWnd:OnCarnival2023RequestPlayDataResult(mReminTime, bReminTime, rReminTime, rank)
	self.skillRoot:SetActive(true)

	local skillInfo = LuaCarnival2023Mgr.pveSkillInfo
	self.pointNum.text = SafeStringFormat3(LocalString.GetString("待分配 %d"), skillInfo.skillPoint)
	self.addPointButton.Enabled = skillInfo.skillPoint > 0

	local rewardTimesLabel = self.isWeekend and self.challengeRewardTimesLabel or self.dailyRewardTimesLabel
	local data = self.isWeekend and Carnival2023_Boss.GetData() or Carnival2023_Normal.GetData()
	local maxAwardTime = data.MaxAwardTime
	local remainTime = self.isWeekend and bReminTime or mReminTime
	rewardTimesLabel.text = remainTime > 0 and SafeStringFormat3(LocalString.GetString("奖励次数 %d/%d"), maxAwardTime - remainTime, maxAwardTime) or LocalString.GetString("奖励次数耗尽")
	rewardTimesLabel.color = NGUIText.ParseColor24(remainTime > 0 and "9AB7D2" or "FF7373", 0)
	self:UpdateSkill()

	if rank >= 0 then
		local rankStr = rank == 0 and LocalString.GetString("未上榜") or rank
		self.rankNumLabel.text = SafeStringFormat3(LocalString.GetString("当前 %s"), rankStr)
	end
end

function LuaCarnival2023PVEEnterWnd:Init()
	self:InitNeedTable()
	self:InitRule()
	self:InitTabBar()
	self:InitSkill()

	self.rankNumLabel.text = ""
	self.dailyRewardTimesLabel.text = ""
	self.challengeRewardTimesLabel.text = ""

	if self.isWeekend then
		UIEventListener.Get(self.challenge:Find("EnterButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnEnterButtonClick()
		end)
		UIEventListener.Get(self.challenge:Find("RankButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnRankButtonClick()
		end)
	else
		UIEventListener.Get(self.daily:Find("EnterButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnEnterButtonClick()
		end)
	end

	Gac2Gas.Carnival2023RequestPlayData()
end

function LuaCarnival2023PVEEnterWnd:InitNeedTable()
	local normalData = Carnival2023_Normal.GetData()
	self.time.text = normalData.ActivityTime
	self.level.text = SafeStringFormat3(LocalString.GetString("≥%d级"), normalData.MinGrade)
	self.needTable:Reposition()
end

function LuaCarnival2023PVEEnterWnd:InitRule()
	Extensions.RemoveAllChildren(self.textTable.transform)
	local msg = g_MessageMgr:FormatMessage("CARNIVAL2023_PVE_RULE")
	if System.String.IsNullOrEmpty(msg) then return end

    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then return end
    do
        local i = 0
        while i < info.paragraphs.Count do
            local paragraphGo = CUICommonDef.AddChild(self.textTable.gameObject, self.textTemplate)
            paragraphGo:SetActive(true)
			paragraphGo.transform:GetComponent(typeof(CTipParagraphItem)):Init(info.paragraphs[i], 4294967295)
            i = i + 1
        end
    end
    self.textTable:Reposition()
end

function LuaCarnival2023PVEEnterWnd:InitTabBar()
	self.modeTabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabChange(go, index)
	end)

	local dateTime = CServerTimeMgr.Inst:GetZone8Time()
	local dayofweek = EnumToInt(dateTime.DayOfWeek)
	self.isWeekend = dayofweek == 0 or dayofweek == 6
	self.modeTabs:ChangeTab(self.isWeekend and 1 or 0, false)

	local passAward = self.awardGrid.transform:Find("PassAward"):GetComponent(typeof(CQnReturnAwardTemplate))
	passAward:Init(Item_Item.GetData(LuaCarnival2023Mgr:GetPVEPassAwardItemId()), 0)
	local topAward = self.awardGrid.transform:Find("TopAward"):GetComponent(typeof(CQnReturnAwardTemplate))
	topAward:Init(Item_Item.GetData(LuaCarnival2023Mgr:GetPVERankAwardItemId()), 0)

	local getAwardNeedRank = Carnival2023_Boss.GetData().GetAwardNeedRank
	topAward.transform:Find("Target"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("前%d"), getAwardNeedRank)
end

function LuaCarnival2023PVEEnterWnd:InitSkill()
	Extensions.RemoveAllChildren(self.skillGrid.transform)
	local normalData = Carnival2023_Normal.GetData()
	local buffIds = {normalData.AttBuffCls, normalData.DefBuffCls, normalData.SpeedBuffCls}
	local matPaths = {
		"UI/Texture/Transparent/Material/pengdao_icon_gongji.mat",
		"UI/Texture/Transparent/Material/pengdao_icon_shengcun.mat",
		"UI/Texture/FestivalActivity/Festival_Carnival/Carnival2023/Material/carnival2023pveenterwnd_icon_minjie.mat",
	}
	for _i, id in ipairs(buffIds) do
		local child = NGUITools.AddChild(self.skillGrid.gameObject, self.skillTemplate)
		child:SetActive(true)
		local icon = child.transform:Find("Mask/Icon")
		icon:GetComponent(typeof(CUITexture)):LoadMaterial(matPaths[_i])
	end
	self.skillGrid:Reposition()
	self.skillRoot:SetActive(false)
end

function LuaCarnival2023PVEEnterWnd:UpdateSkill()
	local normalData = Carnival2023_Normal.GetData()
	local buffIds = {normalData.AttBuffCls, normalData.DefBuffCls, normalData.SpeedBuffCls}
	local skillInfo = LuaCarnival2023Mgr.pveSkillInfo
	local levels = {skillInfo.attackBuffLevel, skillInfo.defBuffLevel, skillInfo.speedBuffLevel}
	for _i, id in ipairs(buffIds) do
		local child = self.skillGrid.transform:GetChild(_i - 1)
		local buffId = id * 100 + (levels[_i] == 0 and 1 or levels[_i])
		child:Find("Panel/Level"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%d级"), levels[_i])
		UIEventListener.Get(child.transform:Find("Mask/Icon").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			LuaBuffInfoWndMgr:ShowWnd(buffId, true, {})
		end)
	end
end

--@region UIEvent

function LuaCarnival2023PVEEnterWnd:OnAddPointButtonClick()
	LuaCarnival2023Mgr.pveSkillInfo.needQueryData = false
	CUIManager.ShowUI(CLuaUIResources.Carnival2023PVESkillAddPointWnd)
end

function LuaCarnival2023PVEEnterWnd:OnRankButtonClick()
	CUIManager.ShowUI(CLuaUIResources.Carnival2023PVERankWnd)
end

function LuaCarnival2023PVEEnterWnd:OnEnterButtonClick()
	if self.isWeekend then
		Gac2Gas.Carnival2023BossEnterGame()
	else
		Gac2Gas.Carnival2023MonsterEnterGame()
	end
end

function LuaCarnival2023PVEEnterWnd:OnTabChange(go, index)
	local isNormal = index == 0

	self.daily.gameObject:SetActive(isNormal and not self.isWeekend)
	self.challenge.gameObject:SetActive(not isNormal and self.isWeekend)
	self.anim:Play(isNormal and "carnival2023pveenterwnd_tab01" or "carnival2023pveenterwnd_tab02")
	if isNormal and self.isWeekend then
		self.notOpen.gameObject:SetActive(true)
		self.notOpen.text = LocalString.GetString("仅周一至周五开放")
	elseif not isNormal and not self.isWeekend then
		self.notOpen.gameObject:SetActive(true)
		self.notOpen.text = LocalString.GetString("仅周末开放")
	else
		self.notOpen.gameObject:SetActive(false)
	end

	local data = isNormal and Carnival2023_Normal.GetData() or Carnival2023_Boss.GetData()
	self.teamLabel.text = data.TeamText
	self.battleLabel.text = data.BattleText
	self.awardLabel.text = isNormal and data.AwardText or ""
	self.awardLabel.gameObject:SetActive(isNormal)
	self.awardGrid.gameObject:SetActive(not isNormal)
	self.awardGrid:Reposition()

	self.tab1TimeLabel.color = isNormal and self.tab1Button.highlightedFontColor or self.tab1Button.normalFontColor
	self.tab2TimeLabel.color = isNormal and self.tab1Button.normalFontColor or self.tab1Button.highlightedFontColor
end

--@endregion UIEvent
