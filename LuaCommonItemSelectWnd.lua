local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UIGrid = import "UIGrid"
local UILabel = import "UILabel"
local CCommonItemSelectCell = import "L10.UI.CCommonItemSelectCell"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaCommonItemSelectWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaCommonItemSelectWnd, "Background", "Background", GameObject)
RegistChildComponent(LuaCommonItemSelectWnd, "ItemsScrollView", "ItemsScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaCommonItemSelectWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaCommonItemSelectWnd, "ItemCell", "ItemCell", GameObject)
RegistChildComponent(LuaCommonItemSelectWnd, "TitleLabel", "TitleLabel", UILabel)

--@endregion RegistChildComponent end

RegistClassMember(LuaCommonItemSelectWnd,"SelectedIndex")
RegistClassMember(LuaCommonItemSelectWnd,"Items")
RegistClassMember(LuaCommonItemSelectWnd,"Datas")
RegistClassMember(LuaCommonItemSelectWnd,"ItemActionsDataSource")

function LuaCommonItemSelectWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.TitleLabel.text = LuaCommonItemSelectMgr.Title
end

function LuaCommonItemSelectWnd:Init()
    self.SelectedIndex = 0 --从1开始
    self.ItemCell:SetActive(false)
    self:LoadData()
end

function LuaCommonItemSelectWnd:LoadData()
    Extensions.RemoveAllChildren(self.Grid.transform)
    self.Items = {}
    self.Datas = LuaCommonItemSelectMgr.OnInitFunc()--返回List<CCommonItemSelectCellData>
    if self.Datas == nil then return end
    local count = self.Datas.Count
    if count == 0 then return end

    local t_name={}
    if String.IsNullOrEmpty(LuaCommonItemSelectMgr.Menu) then
        t_name[1] = LocalString.GetString("选择")
    else
	    t_name[1] = LuaCommonItemSelectMgr.Menu
    end
	local t_action={}
	t_action[1] = function ()
        self:OnSelectItem()
	end
    self.ItemActionsDataSource = DefaultItemActionDataSource.Create(1,t_action,t_name)

    for i=1,count do
        local obj = CUICommonDef.AddChild(self.Grid.gameObject, self.ItemCell)
        obj:SetActive(true)
        local item = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CCommonItemSelectCell))
        item:Init(i, self.Datas[i-1])
        UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnItemClick(go)
        end)
        self.Items[i] = item
    end
    self.Grid:Reposition()
    self.ItemsScrollView:ResetPosition()

    if #self.Items > 0 then
        self:OnItemClick(self.Items[1].gameObject)
    end
end

function LuaCommonItemSelectWnd:OnItemClick(go)
    for i=1,#self.Items do
        local selected = (self.Items[i].gameObject == go)
        self.Items[i].Selected = selected
        if selected then
            self.SelectedIndex = i
            local b1 = NGUIMath.CalculateRelativeWidgetBounds(self.Background.transform)
            local height = b1.size.y
            local worldCenterY = self.Background.transform:TransformPoint(b1.center).y
            local width = b1.size.x
            local worldCenterX = self.Background.transform:TransformPoint(b1.center).x
            if not System.String.IsNullOrEmpty(self.Items[i].ItemId) then
                CItemInfoMgr.ShowLinkItemInfo(self.Items[i].ItemId, false, self.ItemActionsDataSource, CItemInfoMgr.AlignType.Right, worldCenterX, worldCenterY, width, height)
            else
                CItemInfoMgr.ShowLinkItemTemplateInfo(self.Items[i].TemplateId, false, self.ItemActionsDataSource, CItemInfoMgr.AlignType.Right, worldCenterX, worldCenterY, width, height)
            end
        end
    end
end

function LuaCommonItemSelectWnd:OnSelectItem()
    local index = self.SelectedIndex-1
    if index >= 0 and index < self.Datas.Count then
        CUIManager.CloseUI(CLuaUIResources.CommonItemSelectWnd)
        CItemInfoMgr.CloseItemInfoWnd();
        if LuaCommonItemSelectMgr.OnSelectFunc ~= nil then
            LuaCommonItemSelectMgr.OnSelectFunc(self.Datas[index].itemId,self.Datas[index].templateId)
        end
    end
end

--@region UIEvent

--@endregion UIEvent

