local GameObject                = import "UnityEngine.GameObject"
local Vector3                   = import "UnityEngine.Vector3"
local Quaternion                = import "UnityEngine.Quaternion"
local CommonDefs                = import "L10.Game.CommonDefs"
local CPayMgr                   = import "L10.Game.CPayMgr"
local CItemInfoMgr				= import "L10.UI.CItemInfoMgr"
local AlignType					= import "L10.UI.CItemInfoMgr+AlignType"
local CUITexture				= import "L10.UI.CUITexture"
local DelegateFactory		    = import "DelegateFactory"
local UIEventListener		    = import "UIEventListener"
local UILabel                   = import "UILabel"
local LuaGameObject             = import "LuaGameObject"
local LocalString               = import "LocalString"
local UIGrid                    = import "UIGrid"

--天朔画轴购买界面
LuaTsjzBuyAdvPassWnd = class()

RegistChildComponent(LuaTsjzBuyAdvPassWnd, "BuyAdvPassBtn1",   GameObject)
RegistChildComponent(LuaTsjzBuyAdvPassWnd, "BuyAdvPassBtn2",   GameObject)
RegistChildComponent(LuaTsjzBuyAdvPassWnd, "ItemCell",         GameObject)
RegistChildComponent(LuaTsjzBuyAdvPassWnd, "Grid1",            UIGrid)
RegistChildComponent(LuaTsjzBuyAdvPassWnd, "Grid21",           UIGrid)
RegistChildComponent(LuaTsjzBuyAdvPassWnd, "Grid22",           UIGrid)
RegistChildComponent(LuaTsjzBuyAdvPassWnd, "Title1PriceLabel", UILabel)
RegistChildComponent(LuaTsjzBuyAdvPassWnd, "Title2PriceLabel", UILabel)
RegistChildComponent(LuaTsjzBuyAdvPassWnd, "ItemIcon1",        CUITexture)
RegistChildComponent(LuaTsjzBuyAdvPassWnd, "ItemIcon2",        CUITexture)

RegistClassMember(LuaTsjzBuyAdvPassWnd,  "SelectedItem")

function LuaTsjzBuyAdvPassWnd:Awake()
    self.SelectedItem = nil
    UIEventListener.Get(self.BuyAdvPassBtn1).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyAdvPassBtnClick(1)
    end)
    UIEventListener.Get(self.BuyAdvPassBtn2).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyAdvPassBtnClick(2)
    end)
end

function LuaTsjzBuyAdvPassWnd:Init( )
    local setting = HanJia2022_TianShuoSetting.GetData()

    self.Title1PriceLabel.text = setting.OriPrice1
    self.Title2PriceLabel.text = setting.OriPrice2

    --local iconpath = "UI/Texture/Item_Other/Material/other_1234.mat"--todo
    --self.ItemIcon1:LoadMaterial(iconpath)
    --self.ItemIcon2:LoadMaterial(iconpath)

    local passtype = LuaHanJiaMgr.TcjhMainData.PassType
    local buybtnlb1 = LuaGameObject.GetChildNoGC(self.BuyAdvPassBtn1.transform,"Label").label
    if passtype == 1 or passtype == 3 then
        buybtnlb1.text = LocalString.GetString("已解锁")
        CUICommonDef.SetActive(self.BuyAdvPassBtn1,false,true)
    else
        buybtnlb1.text = setting.BuyPrice1
    end

    local buybtnlb2 = LuaGameObject.GetChildNoGC(self.BuyAdvPassBtn2.transform,"Label").label
    local buybtnextlb2 = LuaGameObject.GetChildNoGC(self.BuyAdvPassBtn2.transform,"ExtLabel").label

    local op = System.StringSplitOptions.RemoveEmptyEntries
    local splits = Table2ArrayWithCount({";"}, 1, MakeArrayClass(System.String))
    local btnstrs = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(setting.BuyPrice2, splits, op)
    if passtype == 2 or passtype == 3 then
        buybtnlb2.text = LocalString.GetString("已解锁")
        CUICommonDef.SetActive(self.BuyAdvPassBtn2,false,true)
    else
        buybtnlb2.text = btnstrs[0]
    end
    buybtnextlb2.text = btnstrs[1]
    local des1 = setting.VIP1Des
    local des2 = setting.VIP2Des

    local nmldesarray = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(des1, splits, op)
    local advdesarray = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(des2, splits, op)
    self:FillItems1(nmldesarray)
    self:FillItems2(advdesarray)
end

function LuaTsjzBuyAdvPassWnd:FillItems(desarray,grid,isleft)
    local op = System.StringSplitOptions.RemoveEmptyEntries
    local splits = Table2ArrayWithCount({","}, 1, MakeArrayClass(System.String))
    local strs = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(desarray, splits, op)
    local len = strs.Length
    for i=0,len-1,2 do
        local go = GameObject.Instantiate(self.ItemCell, Vector3.zero, Quaternion.identity)
        go.transform.parent = grid.transform
        go.transform.localScale = Vector3.one
        self:FillItem(go,strs[i],strs[i+1],isleft)
        go:SetActive(true)
    end
    grid:Reposition()
end

function LuaTsjzBuyAdvPassWnd:FillItems1(desarray)
    self:FillItems(desarray[0],self.Grid1,true)
end

function LuaTsjzBuyAdvPassWnd:FillItems2(desarray)
    self:FillItems(desarray[0],self.Grid21,false)
    self:FillItems(desarray[1],self.Grid22,false)
end

function LuaTsjzBuyAdvPassWnd:FillItem(item,itemid,itemcount,isleft)
    local itemcfg = Item_Item.GetData(itemid)
    local iconTex = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local ctTxt = item.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
    if itemcfg then
        iconTex:LoadMaterial(itemcfg.Icon)
    end

    if tonumber(itemcount) <= 1 then
        ctTxt.text = ""
    else
        ctTxt.text = itemcount
    end

    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        local atype
        if isleft then
            atype = AlignType.ScreenRight
        else
            atype = AlignType.ScreenLeft
        end
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, atype, 0, 0, 0, 0) 
        if self.SelectedItem ~= go then
            if self.SelectedItem then
                self:SelectItem(self.SelectedItem,false)
            end
            self.SelectedItem = go
            self:SelectItem(self.SelectedItem,true)
        end
    end)
end

function LuaTsjzBuyAdvPassWnd:SelectItem(itemgo,selected)
    local selectgo = LuaGameObject.GetChildNoGC(itemgo.transform,"SelectedSprite").gameObject
    selectgo:SetActive(selected)
end

--@region UIEvent

function LuaTsjzBuyAdvPassWnd:OnBuyAdvPassBtnClick(type)
    local setting = HanJia2022_TianShuoSetting.GetData()
    if type ==1 then
        CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(setting.Vip1PID), 0)
        CUIManager.CloseUI(CLuaUIResources.TcjhBuyAdvPassWnd)
    else
        CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(setting.Vip2PID), 0)
        CUIManager.CloseUI(CLuaUIResources.TcjhBuyAdvPassWnd)
    end
end

--@endregion
