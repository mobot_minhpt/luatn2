local CBaseWnd = import "L10.UI.CBaseWnd"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local UILabel = import "UILabel"

LuaSchoolTaskOptionWnd = class()
RegistClassMember(LuaSchoolTaskOptionWnd,"m_CloseButton") --关闭按钮
RegistClassMember(LuaSchoolTaskOptionWnd,"m_TitleLabel") --标题
RegistClassMember(LuaSchoolTaskOptionWnd,"m_InfoButton") --说明按钮
RegistClassMember(LuaSchoolTaskOptionWnd,"m_TipLabel") --提示
RegistClassMember(LuaSchoolTaskOptionWnd,"m_ItemRoot") --修习项目根节点
RegistClassMember(LuaSchoolTaskOptionWnd,"m_StatLabel") -- 得分统计
RegistClassMember(LuaSchoolTaskOptionWnd,"m_ChooseButton") --确认选择按钮

RegistClassMember(LuaSchoolTaskOptionWnd,"m_ItemTbl")
RegistClassMember(LuaSchoolTaskOptionWnd,"m_ItemSelectedIdx")

function LuaSchoolTaskOptionWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaSchoolTaskOptionWnd:Init()

	self.m_CloseButton = self.transform:Find("CloseButton").gameObject
	CommonDefs.AddOnClickListener(self.m_CloseButton, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

	self.m_TitleLabel = self.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
	self.m_InfoButton = self.transform:Find("InfoButton").gameObject
	self.m_TipLabel = self.transform:Find("TipLabel"):GetComponent(typeof(UILabel))
	self.m_ItemRoot = self.transform:Find("Grid")
	self.m_StatLabel = self.transform:Find("StatLabel"):GetComponent(typeof(UILabel))
	self.m_ChooseButton = self.transform:Find("ChooseButton").gameObject

	CommonDefs.AddOnClickListener(self.m_InfoButton, DelegateFactory.Action_GameObject(function(go) self:OnInfoButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_ChooseButton, DelegateFactory.Action_GameObject(function(go) self:OnChooseButtonClick() end), false)
	self:InitContent()
end

function LuaSchoolTaskOptionWnd:OnInfoButtonClick()
	g_MessageMgr:ShowMessage("NewBieSchoolTaskTips")
end

function LuaSchoolTaskOptionWnd:OnChooseButtonClick()
	if not self.m_ItemTbl or not self.m_ItemSelectedIdx then return end
	if self.m_ItemSelectedIdx<1 or self.m_ItemSelectedIdx> #self.m_ItemTbl then
		g_MessageMgr:ShowMessage("Newbie_School_Task_Need_Choose_One_Task")
		return
	end
	LuaSchoolTaskMgr:RequestChooseCourse(self.m_ItemSelectedIdx)
	self:Close()
end

function LuaSchoolTaskOptionWnd:InitContent()

	self.m_TitleLabel.text = LuaSchoolTaskMgr:GetCurrentStageName()
	self.m_TipLabel.text = LuaSchoolTaskMgr:GetCurrentStageTaskChosenTip()
	self.m_StatLabel.text = LuaSchoolTaskMgr:GetCurrentStageStatInfo()

	self.m_ItemTbl = {}
	self.m_ItemSelectedIdx = -1
	local n = self.m_ItemRoot.childCount
	for i=0,n-1 do
		local child = self.m_ItemRoot:GetChild(i)
		local courseNameLabel = child:Find("NameLabel"):GetComponent(typeof(UILabel))
		local descLabel = child:Find("DescLabel"):GetComponent(typeof(UILabel))
		local iconTexture = child:Find("IconTexture"):GetComponent(typeof(CUITexture))
		local obtainedScoreLabel = child:Find("ObtainedScoreLabel"):GetComponent(typeof(UILabel))
		local selectedIcon = child:Find("SelectedIcon").gameObject

		CommonDefs.AddOnClickListener(child.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnItemClick(go) end), false)

		local tbl = {}
		tbl.go = child.gameObject
		tbl.courseNameLabel = courseNameLabel
		tbl.descLabel = descLabel
		tbl.iconTexture = iconTexture
		tbl.obtainedScoreLabel = obtainedScoreLabel
		tbl.selectedIcon = selectedIcon
		table.insert(self.m_ItemTbl, tbl)
	end

	for index, item in pairs(self.m_ItemTbl) do
		self:InitOneItem(item, index)
	end
end

function LuaSchoolTaskOptionWnd:InitOneItem(item, index)
	item.selectedIcon:SetActive(false)
	item.courseNameLabel.text = LuaSchoolTaskMgr.m_OptionInfo.courseNames[index]
	item.descLabel.text = LuaSchoolTaskMgr.m_OptionInfo.courseDescs[index]
	item.obtainedScoreLabel.text = SafeStringFormat3(LocalString.GetString("累计获得%d学分"), LuaSchoolTaskMgr.m_OptionInfo.detailCredit[index])
end

function LuaSchoolTaskOptionWnd:OnItemClick(go)
	for index, item in pairs(self.m_ItemTbl) do
		if item.go == go then
			self.m_ItemSelectedIdx = index --下标从1开始
			item.selectedIcon:SetActive(true)
		else
			item.selectedIcon:SetActive(false)
		end
	end
end
