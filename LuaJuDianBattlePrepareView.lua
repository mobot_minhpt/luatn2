local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local UICamera = import "UICamera"

LuaJuDianBattlePrepareView = class()
RegistChildComponent(LuaJuDianBattlePrepareView, "SubmitBtn", GameObject)
RegistChildComponent(LuaJuDianBattlePrepareView, "RuleBtn", GameObject)
RegistChildComponent(LuaJuDianBattlePrepareView, "BuffCheckBtn", GameObject)
RegistChildComponent(LuaJuDianBattlePrepareView, "DescLb", UILabel)
RegistChildComponent(LuaJuDianBattlePrepareView, "CurLingQiLb", UILabel)
-- 凤血玉
RegistChildComponent(LuaJuDianBattlePrepareView, "TodayLingQiLb", UILabel)
RegistChildComponent(LuaJuDianBattlePrepareView, "RewardStateLb", UILabel)
RegistChildComponent(LuaJuDianBattlePrepareView, "HaveGotTag", GameObject)
-- 棋局积分
RegistChildComponent(LuaJuDianBattlePrepareView, "WeekLingQiLb", UILabel)
RegistChildComponent(LuaJuDianBattlePrepareView, "WeekScoreLb", UILabel)
-- 棋局增益
RegistChildComponent(LuaJuDianBattlePrepareView, "SeasonLingQiLb", UILabel)
RegistChildComponent(LuaJuDianBattlePrepareView, "FullLb", UILabel)
RegistChildComponent(LuaJuDianBattlePrepareView, "LevelStateLb", UILabel)
RegistChildComponent(LuaJuDianBattlePrepareView, "LevelLb", UILabel)
RegistChildComponent(LuaJuDianBattlePrepareView, "Popup", UISprite)

RegistClassMember(LuaJuDianBattlePrepareView, "m_LingQiCount")
RegistClassMember(LuaJuDianBattlePrepareView, "m_BuffLv")
RegistClassMember(LuaJuDianBattlePrepareView, "m_LingQiItemID")
RegistClassMember(LuaJuDianBattlePrepareView, "m_LingQiNeedCount")
RegistClassMember(LuaJuDianBattlePrepareView, "m_BuffAddValueList")
RegistClassMember(LuaJuDianBattlePrepareView, "m_LvUpNeedLingQiCountList")

function LuaJuDianBattlePrepareView:Awake()
    UIEventListener.Get(self.SubmitBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSubmitBtnClick()
	end)
    UIEventListener.Get(self.RuleBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)
    UIEventListener.Get(self.BuffCheckBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuffCheckBtnClick()
	end)
end

function LuaJuDianBattlePrepareView:Start()
    self.Popup.gameObject:SetActive(false)
    self.DescLb.text = g_MessageMgr:FormatMessage("JuDian_Battle_Prepare_View_Title_Desc")
    local setData = GuildOccupationWar_DailySetting.GetData()
    self.m_LingQiItemID = setData.ResourceItemId
    self.m_LingQiNeedCount = setData.NumExchangeFengXueYu
    self.m_BuffAddValueList = setData.BuffLevelToAddValueList
    self.m_LvUpNeedLingQiCountList = {}
    local buffInfo = setData.TotalNumberTransformBuff
    for i = 0, buffInfo.Length - 1, 3 do
        table.insert(self.m_LvUpNeedLingQiCountList, buffInfo[i])
    end
    table.insert(self.m_LvUpNeedLingQiCountList, buffInfo[buffInfo.Length - 2])
    self:UpdatePersonalInfo(0)
    self:UpdateScore(0, 0)
    self:UpdateBuff(0, 0)
end

function LuaJuDianBattlePrepareView:Update()
    if Input.GetMouseButtonDown(0) and self.Popup.gameObject.activeSelf then
        -- 点击技能详情框外部隐藏
        local corners = self.Popup.worldCorners
        local lastPos = UICamera.lastWorldPosition;
        if lastPos.x < corners[0].x or lastPos.x > corners[2].x or lastPos.y < corners[0].y or lastPos.y > corners[2].y then
            self.Popup.gameObject:SetActive(false)
        end
    end
end

function LuaJuDianBattlePrepareView:UpdateLingQiInfo(lingQiInfo)
    self:UpdatePersonalInfo(lingQiInfo[11])
    self:UpdateScore(lingQiInfo[1], lingQiInfo[2])
    self:UpdateBuff(lingQiInfo[4], lingQiInfo[5])
end

function LuaJuDianBattlePrepareView:UpdatePersonalInfo(todaySubmitCount)
    -- 提交按钮上方，个人背包中的剩余灵气
    local itemId = GuildOccupationWar_DailySetting.GetData().ResourceItemId
    self.m_LingQiCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemId)
    self.CurLingQiLb.text = tostring(self.m_LingQiCount)
    -- 今日个人提交灵气情况
    local hasGet = todaySubmitCount >= self.m_LingQiNeedCount
    self.TodayLingQiLb.text = SafeStringFormat3("%d/%d", todaySubmitCount, self.m_LingQiNeedCount)
    self.RewardStateLb.text = hasGet and LocalString.GetString("已领取今日凤血玉") or LocalString.GetString("达标后获取今日凤血玉")
    self.HaveGotTag:SetActive(hasGet)
end

function LuaJuDianBattlePrepareView:UpdateScore(lingQiCount, scoreValue)
    self.WeekLingQiLb.text = tostring(lingQiCount)
    self.WeekScoreLb.text = tostring(scoreValue)
end

function LuaJuDianBattlePrepareView:UpdateBuff(seasonSubmitCount, buffLv)
    local isMaxLv = buffLv == #self.m_LvUpNeedLingQiCountList - 1
    self.SeasonLingQiLb.text = SafeStringFormat3("%d/%d", seasonSubmitCount, self.m_LvUpNeedLingQiCountList[buffLv + 1])
    self.LevelLb.text = SafeStringFormat3(LocalString.GetString("Lv.%d"), buffLv)
    self.LevelStateLb.text = isMaxLv and LocalString.GetString("已达最高增益等级") or LocalString.GetString("达标后提升到下一级")
    self.SeasonLingQiLb.gameObject:SetActive(not isMaxLv)
    self.FullLb.gameObject:SetActive(isMaxLv)

    -- 技能介绍弹窗
    local nowLvTitleLb = self.Popup.transform:Find("Now"):GetComponent(typeof(UILabel))
    local nowLvDescLb = nowLvTitleLb.transform:Find("Label"):GetComponent(typeof(UILabel))
    local UpgradeTitleLb = self.Popup.transform:Find("Upgrade"):GetComponent(typeof(UILabel))
    local UpgradeDescLb = UpgradeTitleLb.transform:Find("Label"):GetComponent(typeof(UILabel))
    local maxLvTitleLb = self.Popup.transform:Find("Full").gameObject
    nowLvTitleLb.text = SafeStringFormat3(LocalString.GetString("当前Lv.%d增益"), buffLv)
    UpgradeTitleLb.gameObject:SetActive(not isMaxLv)
    maxLvTitleLb.gameObject:SetActive(isMaxLv)

    if buffLv > 0 then
        nowLvDescLb.text = g_MessageMgr:FormatMessage("JuDian_Battle_Prepare_View_Buff_Desc", self.m_BuffAddValueList[buffLv - 1])
    else
        nowLvDescLb.text = LocalString.GetString("暂无增益")
    end
    if not isMaxLv then
        UpgradeDescLb.text = g_MessageMgr:FormatMessage("JuDian_Battle_Prepare_View_Buff_Desc", self.m_BuffAddValueList[buffLv])
    end
end

--@region UIEvent
function LuaJuDianBattlePrepareView:OnSubmitBtnClick()
    if CClientMainPlayer.Inst ~= nil then
        if self.m_LingQiCount <= 0 then
            g_MessageMgr:ShowMessage("Lack_Of_Ling_Qi_Item")
		else
			CLuaNumberInputMgr.ShowNumInputBox(1, self.m_LingQiCount, 1, function (v)
				-- 提交灵气
				Gac2Gas.GuildJuDianSubmitLingqi(v)
			end, LocalString.GetString("请输入提交的数量"), -1)
        end
    end
end 

function LuaJuDianBattlePrepareView:OnRuleBtnClick()
    g_MessageMgr:ShowMessage("GUILD_JUDIAN_SUBMIT_INTERFACE_TIPS")
end

function LuaJuDianBattlePrepareView:OnBuffCheckBtnClick()
    self.Popup.gameObject:SetActive(true)
end
--@endregion UIEvent

function LuaJuDianBattlePrepareView:OnEnable()
    g_ScriptEvent:AddListener("GuildJuDianSendLingQiSubmitInfoNew", self, "UpdateLingQiInfo")
	Gac2Gas.GuildJuDianQueryLingQiSubmitInfoNew()
end

function LuaJuDianBattlePrepareView:OnDisable()
    g_ScriptEvent:RemoveListener("GuildJuDianSendLingQiSubmitInfoNew", self, "UpdateLingQiInfo")
end