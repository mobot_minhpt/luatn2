require("common/common_include")

local NGUIMath = import "NGUIMath"
local PlayerShowDifProType = import "L10.Game.PlayerShowDifProType"
local BabyMgr = import "L10.Game.BabyMgr"
local Baby_YingEr = import "L10.Game.Baby_YingEr"
local Baby_YouEr = import "L10.Game.Baby_YouEr"
--local Baby_ShaoNian = import "L10.Game.Baby_ShaoNian"
--local Baby_WaiGuan=import "L10.Game.Baby_WaiGuan"
local AMPlayer = import "L10.Game.CutScene.AMPlayer"
local SubtitileMgr = import "L10.UI.SubtitileMgr"
local Baby_Setting = import "L10.Game.Baby_Setting"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CClientNpc = import "L10.Game.CClientNpc"
local CBabyAppearance = import "L10.Game.CBabyAppearance"
local Baby_Fashion = import "L10.Game.Baby_Fashion"

local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CHousePopupMgr = import "L10.UI.CHousePopupMgr"
local HousePopupMenuItemData = import "L10.UI.HousePopupMenuItemData"
local EnumBabyInteractPlace = import "L10.Game.EnumBabyInteractPlace"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"

local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
--local Expression_BabyShow = import "L10.Game.Expression_BabyShow"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"

EnumBabyFeature = {
	HairColor = 1,
	SkinColor = 2,
	Gender = 3,
	BodyWeight = 4,
	Nature = 5,
}

EnumBabyStatus = {
	eBorn = 1,		-- 婴儿阶段
	eChild = 2,		-- 幼儿阶段
	eYoung = 3,		-- 少年阶段
}

EnumBabyInteract = {
	eBabyOnOwner = 1,
	eBabyNotOnOwner = 2,
	eBabyNotOwner = 3,
}

EnumBabyBornProp = {
	Hunger = 1,
	Feeling = 2,
	Health = 3,
}

EnumBabyChildProp = {
	ZhiBian = 1,
	YongWu = 2,
	FengYa = 3,
	JiQiao = 4,
	LiFa = 5,
	ZuiE = 6,
	End = 7,
}

EnumBabyYoungProp = {
	MouLue = 1,
	DaoFa = 2,
	WuYi = 3,
	BingFa = 4,
	ShiCi = 5,
	YaYi = 6,
	TianYan = 7,
	GongYi = 8,
	DeXing = 9,
	YiQi = 10,
	BaoNue = 11,
	XieMei = 12,
	End = 13,
}


EnumYangYuOpenType = {
	Qualification = 1,
	PregnantDesc = 2,
	InspectDiary = 3,	-- 打开产检日记
	BabyNotOnPlayer = 4,
	BabyOnPlayer = 5,
	BabyOnPlayerAndPregnant = 6,
}

EnumBabyInteractMenuType = {
	eHouseMenu = 1,
	eTargetMenu = 2,
}

EnumInteractWithBabyType = {--弹出菜单使用的了，EnumBabyInteractType被占用了，换个名字
    eBabyOnPlayerPlayerOpFollow = 1, --宝宝带上身上，跟随状态
    eBabyOnPlayerPartnerOp = 2, --宝宝在伴侣身上
    eBabyOnPlayerNotParentsOp = 3, --非家长，宝宝带在身上
    eBabyInHouseParentsOp = 4, --家长未将宝宝带在身上
    eBabyInHouseNotParentsOp = 5, --非家长玩家，宝宝没有带在父母身上
    eBabyOnPlayerPlayerOpPlay = 6, --宝宝带上身上，玩耍状态
}

EnumBabyLuckMoneyHandOverType = {
	eFather = 1, --红包给了父亲
	eMother = 2, --红包给了母亲
}

LuaBabyMgr = {}

-- 准生证相关
LuaBabyMgr.m_IsCoupleBirthPermission = true
LuaBabyMgr.m_BirthPermissionResult = nil

-- 产检相关
LuaBabyMgr.m_InspectInfo = nil
LuaBabyMgr.m_InspectTime = nil
LuaBabyMgr.m_OpenByDoctor = false
LuaBabyMgr.m_InspectOpenType = 0

-- 答题相关
LuaBabyMgr.m_AnswerList = nil
LuaBabyMgr.m_Question = nil
LuaBabyMgr.m_CurrentQuestionIndex = 0
LuaBabyMgr.m_QuestionCountDown = 0
LuaBabyMgr.m_CurrentScore = 0
LuaBabyMgr.m_RightAnswerIndex = 0

LuaBabyMgr.m_FinalScore = 0
LuaBabyMgr.m_PassScore = 0

-- 取名相关
LuaBabyMgr.m_NamingBabyId = nil
LuaBabyMgr.m_IsRename = false

-- 成长手记
LuaBabyMgr.m_SelectedQiYuId = 0

-- 日程派遣
LuaBabyMgr.m_AddTargetPos = nil
LuaBabyMgr.m_AddTargetColumn = 0 -- 用于显示plan详情(0, 1, 2)
LuaBabyMgr.m_SelectedPlan = nil -- 用于排除list中已经被选择过的plan

LuaBabyMgr.m_SelectedDetailPlanPos = nil
LuaBabyMgr.m_SelectedDetailPlanId = 0

-- 宝宝属性名称
LuaBabyMgr.m_BabyChildPropName = nil
LuaBabyMgr.m_BabyYoungPropName = nil

-- 是否需要显示怀孕中的第二个孩子
LuaBabyMgr.m_BabyOnPlayerAndPregnant = false

-- 宝宝基金
LuaBabyMgr.m_BabyFundShow = nil
LuaBabyMgr.m_BabyFundBuy = nil
LuaBabyMgr.m_BabyFundInfo = nil

-- 宝宝表情动作
LuaBabyMgr.m_ExpressionBabyEngineId = nil
LuaBabyMgr.m_BabyExpressionList = nil --List

LuaBabyMgr.m_NowShowExpressionId = nil

LuaBabyMgr.m_ShowGetBabyInfo = {}
LuaBabyMgr.m_ShowGetBabyType = 0

function LuaBabyMgr.ClearBabyFundInfo()
	LuaBabyMgr.m_BabyFundShow = nil
	LuaBabyMgr.m_BabyFundBuy = nil
	LuaBabyMgr.m_BabyFundInfo = nil
end

-- 作诗内容
LuaBabyMgr.m_PoemContent = ""
LuaBabyMgr.m_PoemInscribe = nil

--打开写诗窗口
LuaBabyMgr.ShowPoemWnd = function(contentStr,babyID)
	LuaBabyMgr.m_PoemContent = contentStr

	local name = ""
	if LuaBabyMgr.CurrentChatBabyContext == EnumShowBabyChatContext.eFromBabyWnd then
		local baby = BabyMgr.Inst:GetBabyById(babyID)
		name = baby and baby.Name or ""
	elseif LuaBabyMgr.CurrentChatBabyContext == EnumShowBabyChatContext.eFromNPC then
		local npc = CClientObjectMgr.Inst:GetObject(LuaBabyMgr.CurrentChatBabyEngineId or 0)
		name = npc and npc.Name or ""
	end

	LuaBabyMgr.m_PoemInscribe = LocalString.GetString("你的宝贝 ") .. name
	CUIManager.ShowUI("BabyPoemWnd")
end

-- 胎梦
LuaBabyMgr.m_TaiMengId = 0
function LuaBabyMgr.GetBabyPortrait(status,gender,hairstyle)

	hairstyle = hairstyle or 1
	local keys={}
	Baby_WaiGuan.ForeachKey(function (key)
        table.insert(keys, key)
	end)
	for i,v in ipairs(keys) do
		local data=Baby_WaiGuan.GetData(v)
		if data.Sex==gender and data.Stage==status and data.Hairstyle==hairstyle then
			return data.Portrait
		end
	end
	return nil
end

function LuaBabyMgr.GetSchedulePlanColor(index)
	if index == 1 then
		return "FFFE91"
	elseif index == 2 then
		return "519FFF"
	elseif index == 3 then
		return "FF5050"
	elseif index == 4 then
		return "C000C0"
	end
	return "FFFFFF"
end

function LuaBabyMgr.GetBabyPlanGainStr(plan)
	if not plan then return "" end
	local gainStr = ""
	local status = BabyMgr.Inst:GetSelectedBabyStatus()
	-- 根据宝宝的状态显示属性
	if status == EnumBabyStatus.eChild then
		local count = plan.ChildPropDelta.Length
		for i = 0, count-1 do
			local property = plan.ChildPropDelta[i][0]
			local delta = plan.ChildPropDelta[i][1]

			local addLabel = "+"
			if delta < 0 then
				addLabel = "-"
			end
			if i == count-1 then
				gainStr = SafeStringFormat3("%s%s%s%d", gainStr, LuaBabyMgr.GetPropName(EnumBabyStatus.eChild, property), addLabel, math.abs(delta))
			else
				gainStr = SafeStringFormat3(LocalString.GetString("%s%s%s%d,"), gainStr, LuaBabyMgr.GetPropName(EnumBabyStatus.eChild, property), addLabel, math.abs(delta))
			end
			
		end
		
	elseif status == EnumBabyStatus.eYoung then
		local count = plan.YoungPropDelta.Length
		for i = 0, count-1 do
			local property = plan.YoungPropDelta[i][0]
			local delta = plan.YoungPropDelta[i][1]

			local addLabel = "+"
			if delta < 0 then
				addLabel = "-"
			end
			if i == count-1 then
				gainStr = SafeStringFormat3("%s%s%s%d", gainStr, LuaBabyMgr.GetPropName(EnumBabyStatus.eYoung, property), addLabel, math.abs(delta))
			else
				gainStr = SafeStringFormat3(LocalString.GetString("%s%s%s%d,"), gainStr, LuaBabyMgr.GetPropName(EnumBabyStatus.eYoung, property), addLabel, math.abs(delta))
			end
		end
	end
	return gainStr
end

function LuaBabyMgr.ShowBabySchedulePlanList(targetTransform, column)
	local b = NGUIMath.CalculateRelativeWidgetBounds(targetTransform)
	LuaBabyMgr.m_AddTargetPos = targetTransform.transform:TransformPoint(b.center)
	LuaBabyMgr.m_AddTargetColumn = column
	CUIManager.ShowUI(CLuaUIResources.BabySchedulePlanList)
end

LuaBabyMgr.m_SelectedPlanInfo = nil
LuaBabyMgr.m_IsSchedulePlanned = false
LuaBabyMgr.m_SelectedDetailPlanIndex = 0
function LuaBabyMgr.ShowBabySchedulePlanDetail(targetTransform, index, isPlanned, planInfo)
	local b = NGUIMath.CalculateRelativeWidgetBounds(targetTransform)
	LuaBabyMgr.m_SelectedDetailPlanPos = targetTransform.transform:TransformPoint(b.center)
	LuaBabyMgr.m_IsSchedulePlanned = isPlanned
	LuaBabyMgr.m_SelectedPlanInfo = planInfo
	LuaBabyMgr.m_SelectedDetailPlanIndex = index
	CUIManager.ShowUI(CLuaUIResources.BabySchedulePlanDetailWnd)
end

function LuaBabyMgr.GetPropName(status, index)
	if status == EnumBabyStatus.eBorn then
		return LuaBabyMgr.GetBabyBornPropName(index)
	elseif status == EnumBabyStatus.eChild then
		return LuaBabyMgr.GetBabyChildPropName(index)
	elseif status == EnumBabyStatus.eYoung then
		return LuaBabyMgr.GetBabyYoungPropName(index)
	end
	return ""
end

function LuaBabyMgr.GetBabyBornPropName(index)
	local data = Baby_YingEr.GetData(index)
	if data then
		return data.Name
	else
		return ""
	end
end

function LuaBabyMgr.GetBabyChildPropName(index)
	local data = Baby_YouEr.GetData(index)
	if data then
		return data.Name
	else
		return ""
	end
end

function LuaBabyMgr.GetBabyYoungPropName(index)
	local data = Baby_ShaoNian.GetData(index)
	if data then
		return data.Name
	else
		return ""
	end
end

-- 宝宝属性变化
LuaBabyMgr.m_SaveBabyId = nil
LuaBabyMgr.m_SaveBabyStatus = 0
LuaBabyMgr.m_BabyChildPropSave = nil -- dictionary
LuaBabyMgr.m_BabyYoungPropSave = nil -- dictionary
LuaBabyMgr.m_BabyPropDiffList = nil

-- 打开收获界面时保存
function LuaBabyMgr.SaveProp()
	local selectedBaby = BabyMgr.Inst:GetSelectedBaby()
	if not selectedBaby then return end

	LuaBabyMgr.m_SaveBabyId = BabyMgr.Inst.SelectedBabyId
	LuaBabyMgr.m_SaveBabyStatus = selectedBaby.Status
	LuaBabyMgr.m_BabyChildPropSave = CreateFromClass(MakeGenericClass(Dictionary, UInt16, UInt16))
	if selectedBaby.Props.ChildProp then
		CommonDefs.DictIterate(selectedBaby.Props.ChildProp, DelegateFactory.Action_object_object(function(key, val)
			CommonDefs.DictAdd_LuaCall(LuaBabyMgr.m_BabyChildPropSave, key, val)
		end))
	end

	LuaBabyMgr.m_BabyYoungPropSave = CreateFromClass(MakeGenericClass(Dictionary, UInt16, UInt16))
	if selectedBaby.Props.YoungProp then
		CommonDefs.DictIterate(selectedBaby.Props.YoungProp, DelegateFactory.Action_object_object(function(key, val)
			CommonDefs.DictAdd_LuaCall(LuaBabyMgr.m_BabyYoungPropSave, key, val)
		end))
	end

end


function LuaBabyMgr.ShowBabyProDif()
	local selectedBaby = BabyMgr.Inst:GetSelectedBaby()
	if not selectedBaby then return end

	LuaBabyMgr.m_BabyPropDiffList = CreateFromClass(MakeGenericClass(List, PlayerShowDifProType))

	-- 如果不是同一个阶段，则不显示属性变化（因为不同阶段属性不同，无法对比）
	if selectedBaby.Status ~= LuaBabyMgr.m_SaveBabyStatus then
		return
	end

	local nowChildProp = selectedBaby.Props.ChildProp
	local nowYoungProp = selectedBaby.Props.YoungProp

	if LuaBabyMgr.m_BabyChildPropSave then
		CommonDefs.DictIterate(nowChildProp, DelegateFactory.Action_object_object(function(key, val)
			local nowValue = val
			local saveValue = LuaBabyMgr.m_BabyChildPropSave[key]
			if math.abs(nowValue - saveValue) >= 1 then
				local diffPro = PlayerShowDifProType(saveValue, nowValue, LuaBabyMgr.GetPropName(EnumBabyStatus.eChild, key))
				CommonDefs.ListAdd_LuaCall(LuaBabyMgr.m_BabyPropDiffList, diffPro)
			end
		end))
	end

	if LuaBabyMgr.m_BabyYoungPropSave then
		CommonDefs.DictIterate(nowYoungProp, DelegateFactory.Action_object_object(function(key, val)
			local nowValue = val
			local saveValue = LuaBabyMgr.m_BabyYoungPropSave[key]
			if math.abs(nowValue - saveValue) >= 1 then
				local diffPro = PlayerShowDifProType(saveValue, nowValue, LuaBabyMgr.GetPropName(EnumBabyStatus.eYoung, key))
				CommonDefs.ListAdd_LuaCall(LuaBabyMgr.m_BabyPropDiffList, diffPro)
			end
		end))
	end

	LuaBabyMgr.SaveProp()

	if LuaBabyMgr.m_BabyPropDiffList and LuaBabyMgr.m_BabyPropDiffList.Count > 0 then
		CUIManager.ShowUI(CLuaUIResources.BabyProDifShowWnd)
	end
end


function LuaBabyMgr.SavePropForSelectedBaby(selectedBaby)
	if not selectedBaby then return end

	LuaBabyMgr.m_SaveBabyId = BabyMgr.Inst.SelectedBabyId
	LuaBabyMgr.m_SaveBabyStatus = selectedBaby.Status

	LuaBabyMgr.m_BabyChildPropSave = CreateFromClass(MakeGenericClass(Dictionary, UInt16, UInt16))
	if selectedBaby.Props.ChildProp then
		CommonDefs.DictIterate(selectedBaby.Props.ChildProp, DelegateFactory.Action_object_object(function(key, val)
			CommonDefs.DictAdd_LuaCall(LuaBabyMgr.m_BabyChildPropSave, key, val)
		end))
	end

	LuaBabyMgr.m_BabyYoungPropSave = CreateFromClass(MakeGenericClass(Dictionary, UInt16, UInt16))
	if selectedBaby.Props.YoungProp then
		CommonDefs.DictIterate(selectedBaby.Props.YoungProp, DelegateFactory.Action_object_object(function(key, val)
			CommonDefs.DictAdd_LuaCall(LuaBabyMgr.m_BabyYoungPropSave, key, val)
		end))
	end

end

function LuaBabyMgr.ShowBabyProDifForSelectedBaby(selectedBaby)
	if not selectedBaby then return end

	-- 如果不是同一个阶段，则不显示属性变化（因为不同阶段属性不同，无法对比）
	if selectedBaby.Status ~= LuaBabyMgr.m_SaveBabyStatus then
		return
	end
	LuaBabyMgr.m_BabyPropDiffList = CreateFromClass(MakeGenericClass(List, PlayerShowDifProType))

	local nowChildProp = selectedBaby.Props.ChildProp
	local nowYoungProp = selectedBaby.Props.YoungProp

	if LuaBabyMgr.m_BabyChildPropSave then
		CommonDefs.DictIterate(nowChildProp, DelegateFactory.Action_object_object(function(key, val)
			local nowValue = val
			local saveValue = LuaBabyMgr.m_BabyChildPropSave[key]
			if math.abs(nowValue - saveValue) >= 1 then
				local diffPro = PlayerShowDifProType(saveValue, nowValue, LuaBabyMgr.GetPropName(EnumBabyStatus.eChild, key))
				CommonDefs.ListAdd_LuaCall(LuaBabyMgr.m_BabyPropDiffList, diffPro)
			end
		end))
	end

	if LuaBabyMgr.m_BabyYoungPropSave then
		CommonDefs.DictIterate(nowYoungProp, DelegateFactory.Action_object_object(function(key, val)
			local nowValue = val
			local saveValue = LuaBabyMgr.m_BabyYoungPropSave[key]
			if math.abs(nowValue - saveValue) >= 1 then
				local diffPro = PlayerShowDifProType(saveValue, nowValue, LuaBabyMgr.GetPropName(EnumBabyStatus.eYoung, key))
				CommonDefs.ListAdd_LuaCall(LuaBabyMgr.m_BabyPropDiffList, diffPro)
			end
		end))
	end

	LuaBabyMgr.SaveProp()

	if LuaBabyMgr.m_BabyPropDiffList and LuaBabyMgr.m_BabyPropDiffList.Count > 0 then
		CUIManager.ShowUI(CLuaUIResources.BabyProDifShowWnd)
	end
end

LuaBabyMgr.CurrentChatBabyId = nil
LuaBabyMgr.CurrentChatBabyEngineId = nil
LuaBabyMgr.CurrentChatBabyIsMyBaby = nil
LuaBabyMgr.CurrentChatBabyContext = nil
LuaBabyMgr.BabyId2UnreadMsg = {}
function LuaBabyMgr.OpenBabyChatWnd(babyId, engineId, mainplayerIsOwner, context)

	if not context then return end
	if context~=EnumShowBabyChatContext.eFromNPC and context~=EnumShowBabyChatContext.eFromBabyWnd then return end

	LuaBabyMgr.CurrentChatBabyId = babyId
	LuaBabyMgr.CurrentChatBabyEngineId = engineId
	LuaBabyMgr.CurrentChatBabyIsMyBaby = mainplayerIsOwner -- 不带在自己身上的这里也会是false，字段只是供服务器做检查用的
	LuaBabyMgr.CurrentChatBabyContext = context
	CUIManager.ShowUI("BabyChatWnd")
end

function LuaBabyMgr.ReplyChatMessageToBaby(babyId, success, skillName, originMessageId, message)
	g_ScriptEvent:BroadcastInLua("ReplyChatMessageToBaby", babyId, success, skillName, originMessageId, message)
end

function LuaBabyMgr.HasUnreadMsg(babyId)
	return babyId and LuaBabyMgr.BabyId2UnreadMsg and LuaBabyMgr.BabyId2UnreadMsg[babyId]
end

function LuaBabyMgr.GetUnreadMsg(babyId)
	if babyId and LuaBabyMgr.BabyId2UnreadMsg and LuaBabyMgr.BabyId2UnreadMsg[babyId] then
		return LuaBabyMgr.BabyId2UnreadMsg[babyId]
	else
		return nil
	end
end

function LuaBabyMgr.ConsumeUnreadMsg(babyId)
	if babyId and LuaBabyMgr.BabyId2UnreadMsg and LuaBabyMgr.BabyId2UnreadMsg[babyId] then
		LuaBabyMgr.BabyId2UnreadMsg[babyId] = nil
		g_ScriptEvent:BroadcastInLua("BabyUnreadMsgChanged", babyId)
	end
end

function LuaBabyMgr.PushBabyChatMessage(babyId, skillName, message)
	if not babyId then return end
	if not message then return end
	if not LuaBabyMgr.BabyId2UnreadMsg then
		LuaBabyMgr.BabyId2UnreadMsg = {}
	end
	LuaBabyMgr.BabyId2UnreadMsg[babyId] = {msg=message, skill=skillName}
	g_ScriptEvent:BroadcastInLua("BabyUnreadMsgChanged", babyId)
end

LuaBabyMgr.m_SelectedBabyNPCEngineId = nil
LuaBabyMgr.m_SelectedBabyNPCGrade = 0
LuaBabyMgr.m_SelectedBabyNPCExp = 0
LuaBabyMgr.m_SelectedBabyNPCLeftFeedTimes = 0

function LuaBabyMgr.ShowBabyFeedWnd(npcEngineId, grade, exp, leftTimes)
	LuaBabyMgr.m_SelectedBabyNPCEngineId = npcEngineId
	LuaBabyMgr.m_SelectedBabyNPCGrade = grade
	LuaBabyMgr.m_SelectedBabyNPCExp = exp
	LuaBabyMgr.m_SelectedBabyNPCLeftFeedTimes = leftTimes
	if CUIManager.IsLoaded(CLuaUIResources.BabyFeedWnd) then
		g_ScriptEvent:BroadcastInLua("UpdateBabyGradeAndExp", grade, exp, leftTimes, false)
	else
		CUIManager.ShowUI(CLuaUIResources.BabyFeedWnd)
	end
end


function LuaBabyMgr.ShowProductionInspectionWndFromLink()
	if not BabyMgr.Inst.InspectInfoList then return end

	LuaBabyMgr.m_InspectTime = BabyMgr.Inst.InspectTime
	LuaBabyMgr.m_InspectInfo = {}

	for i = 0, BabyMgr.Inst.InspectInfoList.Count-1 do
		table.insert(LuaBabyMgr.m_InspectInfo, BabyMgr.Inst.InspectInfoList[i])
	end

	if BabyMgr.Inst.InspectOpenPlayerId == CClientMainPlayer.Inst.Id then
		LuaBabyMgr.m_InspectOpenType = 1
	else
		LuaBabyMgr.m_InspectOpenType = 3
	end

	if CUIManager.IsLoaded(CLuaUIResources.ProductionInspectionWnd) then
        g_ScriptEvent:BroadcastInLua("UpdateProductionInspection", LuaBabyMgr.m_InspectTime, LuaBabyMgr.m_InspectInfo)
    else
        CUIManager.ShowUI(CLuaUIResources.ProductionInspectionWnd)
    end
end

-- 展示宝宝选择界面
LuaBabyMgr.m_NoBabyToChooseMsg = nil
LuaBabyMgr.m_OnChooseBabyAction = nil
LuaBabyMgr.m_FilterBabyAction = nil
LuaBabyMgr.m_ChooseWndTitle = nil
LuaBabyMgr.m_ChosenBaby = nil
function LuaBabyMgr.ShowBabyChooseWnd(noMsg, onChooseAction, filterAction, title)
	LuaBabyMgr.m_NoBabyToChooseMsg = noMsg
	LuaBabyMgr.m_OnChooseBabyAction = onChooseAction
	LuaBabyMgr.m_FilterBabyAction = filterAction
	LuaBabyMgr.m_ChooseWndTitle = title
	CUIManager.ShowUI(CLuaUIResources.BabyChooseWnd)
end

-- 打开气场鉴定选择宝宝界面
function LuaBabyMgr.OpenQiChangJianDing()
	local msg = g_MessageMgr:FormatMessage("BABY_QICHANG_JIANDING_REQUEST")
	LuaBabyMgr.ShowBabyChooseWnd(msg, function (babyId)
		LuaBabyMgr.m_ChosenBaby = BabyMgr.Inst:GetBabyById(babyId)
		CUIManager.CloseUI(CLuaUIResources.BabyChooseWnd)
		CUIManager.ShowUI(CLuaUIResources.BabyQiChangJianDingWnd)
	end, function (babyId)
		local baby = BabyMgr.Inst:GetBabyById(babyId)
		if baby then
			return baby.Status > EnumBabyStatus.eChild
		end
		return false
	end, LocalString.GetString("气场鉴定"))
end

LuaBabyMgr.m_BabyXiLianType = 0
-- 打开宝宝洗练界面
function LuaBabyMgr.OpenBabyXiLian(index)
	local msg = g_MessageMgr:FormatMessage("BABY_XILIAN_REQUEST")
	LuaBabyMgr.ShowBabyChooseWnd(msg, function (babyId)
		LuaBabyMgr.m_BabyXiLianType = index
		LuaBabyMgr.m_ChosenBaby = BabyMgr.Inst:GetBabyById(babyId)
		CUIManager.CloseUI(CLuaUIResources.BabyChooseWnd)
		CUIManager.ShowUI(CLuaUIResources.BabyPropXiLianWnd)
	end, function (babyId)
		local baby = BabyMgr.Inst:GetBabyById(babyId)
		if baby then
			return baby.Status > EnumBabyStatus.eChild
		end
		return false
	end, LocalString.GetString("宝宝属性洗炼"))
end

-- 打开气场tip
LuaBabyMgr.m_SelectedQiChangTipId = 0
function LuaBabyMgr.OpenQiChangTip(qiChangId)
	LuaBabyMgr.m_SelectedQiChangTipId = qiChangId
	CUIManager.ShowUI(CLuaUIResources.BabyQiChangRequestTip)
end


LuaBabyMgr.m_CommonMsg = nil
LuaBabyMgr.m_OnOKAction = nil
LuaBabyMgr.m_OnCancelAction = nil
LuaBabyMgr.m_CommonTitle = nil
function LuaBabyMgr.OpenCommonRuleTip(msg, okAction, cancelAction, title)
	LuaBabyMgr.m_CommonMsg = msg
	LuaBabyMgr.m_OnOKAction = okAction
	LuaBabyMgr.m_OnCancelAction = cancelAction
	LuaBabyMgr.m_CommonTitle = title
	CUIManager.ShowUI(CLuaUIResources.CommonRuleTipWnd)
end


-- 0-4 白色到紫色
function LuaBabyMgr.GetColor(index)
	if index == 0 then
		return "FFFFFF"
	elseif index == 1 then
		return "FFFE91"
	elseif index == 2 then
		return "519FFF"
	elseif index == 3 then
		return "FF5050"
	elseif index == 4 then
		return "C000C0"
	end
	return "FFFFFF"
end

-- 0-5 白色到紫色（比上一个方法多了橙色）
function LuaBabyMgr.GetQiChangNameColor(index)
	if index == 0 then
		return "ffffff"
	elseif index == 1 then
		return "fffd65"
	elseif index == 2 then
		return "ff7800"
	elseif index == 3 then
		return "0078ff"
	elseif index == 4 then
		return "ff0000"
	elseif index == 5 then
		return "9600ff"
	end
	return "ffffff"
end

-- 气场树颜色调整
function LuaBabyMgr.GetQiChangShuColor(index)
	if index == 0 then
		return "FFFFFF"
	elseif index == 1 then
		return "FFFE91"
	elseif index == 2 then
		return "FA800A"
	elseif index == 3 then
		return "519FFF"
	elseif index == 4 then
		return "FF5050"
	elseif index == 5 then
		return "FF88FF"
	end
	return "FFFFFF"
end

-- 少年期需要转化为幼儿期
function LuaBabyMgr.GetPropBG(key)
	if key == 1 then
		return "UI/Texture/Transparent/Material/BabyWnd_zhimou.mat"
	elseif key == 2 then
		return "UI/Texture/Transparent/Material/BabyWnd_wuli.mat"
	elseif key == 3 then
		return "UI/Texture/Transparent/Material/BabyWnd_fengya.mat"
	elseif key == 4 then
		return "UI/Texture/Transparent/Material/BabyWnd_jiqiao.mat"
	elseif key == 5 then
		return "UI/Texture/Transparent/Material/BabyWnd_lifa.mat"
	elseif key == 6 then
		return "UI/Texture/Transparent/Material/BabyWnd_zuie.mat"
	end
	return ""
end

-- 获得气场对应的特效
function LuaBabyMgr.GetQiChangFx(index)
	if index == 3 then
		return CUIFxPaths.BabyQiChangFX1
	elseif index == 4 then
		return CUIFxPaths.BabyQiChangFX2
	elseif index == 5 then
		return CUIFxPaths.BabyQiChangFX3
	end
	return nil
end

-- 有新的气场奖励没有获取
function LuaBabyMgr.HasNewQiChangReward(selectedBaby)
	local hasNew = false
	Baby_QiChangReward.ForeachKey(function (key)
		hasNew = hasNew or LuaBabyMgr.HasNewQiChangRewardForType(key, selectedBaby)
	end)

	return hasNew
end

--  某个类型的气场
function LuaBabyMgr.HasNewQiChangRewardForType(qichangType, selectedBaby)
	local reward = Baby_QiChangReward.GetData(qichangType)
	if reward and selectedBaby then
		-- 奖品没有获取
		local activedReward = selectedBaby.Props.QiChangData.ActivedRewardStatus
		if not activedReward or not CommonDefs.DictContains_LuaCall(activedReward, qichangType) then
			-- 气场全部解锁
			local allActived = true
			local activedQiChang = selectedBaby.Props.QiChangData.ActivedQiChang

			Baby_QiChang.Foreach(function(key, qichang)
				if qichang.Quality == qichangType then
					local isActive = CommonDefs.DictContains_LuaCall(activedQiChang, key)
					allActived = allActived and isActive
				end
			end)

			return allActived
		end
	end
	return false
end

-- 根据性别和气场质量，获得气场背景(1-6)
function LuaBabyMgr.GetQiChangBG(gender, quality)
	if gender == 0 then
		return SafeStringFormat3("UI/Texture/Transparent/Material/baobaoqichang_nan_0%s.mat", tostring(quality))
	else
		return SafeStringFormat3("UI/Texture/Transparent/Material/baobaoqichang_nv_0%s.mat", tostring(quality))
	end
	return "UI/Texture/Transparent/Material/baobaoqichang_nan_01.mat"
end

-- 少年期属性转幼儿期属性
function LuaBabyMgr.GetYoungPropToChildProp(baby, index)
	local shaonian = Baby_ShaoNian.GetData(index)
	local result = 0
	if shaonian then
		local youerPropIndex = shaonian.YouErName
		Baby_ShaoNian.ForeachKey(function (key)
			local value = Baby_ShaoNian.GetData(key)
			if value.YouErName == youerPropIndex then
				result = result + baby.Props.YoungProp[key]
			end
		end)
	end
	return result
end

-- 根据幼儿期index获得少年期属性和
function LuaBabyMgr.GetYoundPropByGroup(baby, groupIndex)
	local result = 0
	Baby_ShaoNian.ForeachKey(function (key)
		local value = Baby_ShaoNian.GetData(key)
		if value.YouErName == groupIndex then
			result = result + baby.Props.YoungProp[key]
		end
	end)
	return result
end

LuaBabyMgr.m_IsLandMine = false
LuaBabyMgr.m_LandMineMsg = nil
function LuaBabyMgr.ShowParentExamResultLandMine(landMineMsg)
	LuaBabyMgr.m_IsLandMine = true
	LuaBabyMgr.m_LandMineMsg = landMineMsg
	CUIManager.ShowUI(CLuaUIResources.ParentExamResultWnd)
end

LuaBabyMgr.m_SelectedSkillId = 0
function LuaBabyMgr.ShowBabySkillTip(skillId)
	LuaBabyMgr.m_SelectedSkillId = skillId
	CUIManager.ShowUI(CLuaUIResources.BabySkillTip)
end

LuaBabyMgr.m_BabySkillPlayerPrefsKey = "BabySkillActivedIds"
function LuaBabyMgr.GetActivedBabySkill()
	-- 返回一个table
	local skillIds = {}
	local skills = PlayerPrefs.GetString(LuaBabyMgr.m_BabySkillPlayerPrefsKey, "")
	local t = g_LuaUtil:StrSplit(skills, ";")
	for i = 1, #t do
		if t[i] and t[i] ~= "" then
			table.insert(skillIds, tonumber(t[i]))
		end
	end
	return skillIds
end

function LuaBabyMgr.SaveActivedBabySkill(skillIds)
	local savedSkills = LuaBabyMgr.GetActivedBabySkill()
	local skillsToSave = LuaBabyMgr.GetActivedBabySkill()

	for i = 1, #skillIds do
		if LuaBabyMgr.IsNewBabySkill(skillIds[i], savedSkills) then
			table.insert(skillsToSave, skillIds[i])
		end
	end
	local str = ""
	for i = 1, #skillsToSave do
		str = SafeStringFormat3("%s%s;", str, tostring(skillsToSave[i]))
	end
	PlayerPrefs.SetString(LuaBabyMgr.m_BabySkillPlayerPrefsKey, str)
end

function LuaBabyMgr.IsNewBabySkill(skillId, savedSkills)
	for i = 1, #savedSkills do
		if skillId == savedSkills[i] then
			return false
		end
	end
	return true
end

-- 占卜的按钮
LuaBabyMgr.m_ZhanBuOpen = true

function LuaBabyMgr.GetBabyAppearance(status, gender, hairColor, skinColor, hairstyle, headId, bodyId, colorId, wingId, qiChangId)
	--  {status, gender, hairColor, skinColor, hairstyle, grade, headId, bodyId, colorId, backId, qiChangId}
	local appearance = CBabyAppearance()
	appearance.Status = status or 0
	appearance.Gender = gender or 0
	appearance.HairStyle = hairstyle or 0
	appearance.HairColor = hairColor or 0
	appearance.SkinColor = skinColor or 0
	appearance.WingId = wingId or 0
	appearance.FashionHeadId = headId or 0
	appearance.FashionBodyId = bodyId or 0
	appearance.BodyWeight = 8
	appearance.ColorId = colorId or 0
	appearance.QiChangId = qiChangId or 0
	return appearance
end

LuaBabyMgr.m_PreviewFashinId = 0
LuaBabyMgr.m_PreviewFashionItemId = 0
LuaBabyMgr.m_PreviewFashionBaby = nil
LuaBabyMgr.m_PreviewFashionBabyTable = nil
function LuaBabyMgr.ShowBabyFashionPreview(fashionId)

	if fashionId == 0 then return end
	local fashion = Baby_Fashion.GetData(fashionId)

	LuaBabyMgr.m_PreviewFashinId = fashionId
	LuaBabyMgr.m_PreviewFashionItemId = fashion.ItemID
	
	local babyIdList = BabyMgr.Inst:GetBabyIdList()
	local babyTable = {}

	if not babyIdList or babyIdList.Count == 0 then
		g_MessageMgr:ShowMessage("BABY_FASHION_NO_BABY_WITH_PLAYER")
		return
	end

	for i = 0, babyIdList.Count-1 do
		local babyId = babyIdList[i]
		local baby = BabyMgr.Inst:GetBabyById(babyId)
		if fashion.BabyStage == 3 and baby.Props.ExtraData.ShowStatus >= EnumBabyStatus.eChild then
			table.insert(babyTable, baby)
		elseif baby.Props.ExtraData.ShowStatus == (fashion.BabyStage+1) then
			table.insert(babyTable, baby)
		end
	end

	if #babyTable == 0 then
		g_MessageMgr:ShowMessage("BABY_FASHION_NO_QUALIFIED_BABY")
		return
	end

	if #babyTable == 1 then
		LuaBabyMgr.m_PreviewFashionBaby = babyTable[1]
		CUIManager.ShowUI(CLuaUIResources.ShopMallBabyFashionPreviewWnd)
		return
	end

	if #babyTable > 1 then
		LuaBabyMgr.m_PreviewFashionBabyTable = babyTable
		CUIManager.ShowUI(CLuaUIResources.BabyFashionChooseWnd)
		return
	end

end


LuaBabyMgr.BabyEngingIdToSendLuckMoney = nil
LuaBabyMgr.BabyNameInHistoryWnd = nil
LuaBabyMgr.BabyPortraitInHistoryWnd = nil
function LuaBabyMgr.ShowSendLuckMoneyWnd(engineId)
	LuaBabyMgr.BabyEngingIdToSendLuckMoney = engineId
	CUIManager.ShowUI("BabySendHongBaoWnd")
end

function LuaBabyMgr.RequestSendLuckMoney(money)
	if not LuaBabyMgr.BabyEngingIdToSendLuckMoney  or not money then return end

	local npc = CClientObjectMgr.Inst:GetObject(LuaBabyMgr.BabyEngingIdToSendLuckMoney)
	if npc == nil then
		--去掉一个策划表没填的消息 Baby_Send_Luck_Money_Baby_Disappear_Tip
        return
    end

	Gac2Gas.RequestSendLuckyMoneyToBaby(LuaBabyMgr.BabyEngingIdToSendLuckMoney, money)
end

function LuaBabyMgr.OnSendLuckMoneySuccess()
	CUIManager.CloseUI("BabySendHongBaoWnd")
end

function LuaBabyMgr.ShowLuckMoneyHistoryWnd()
	CUIManager.ShowUI("BabyHongBaoHistoryWnd")
end

function LuaBabyMgr.QueryBabyLuckyMoneyHistory()
	if not LuaBabyMgr.BabyEngingIdToSendLuckMoney then return end
	Gac2Gas.QueryBabyLuckyMoneyHistory(LuaBabyMgr.BabyEngingIdToSendLuckMoney)
end

function LuaBabyMgr.QueryBabyLuckyMoneyHistoryDone(name, status, gender, hairstyle, dataTbl)
	LuaBabyMgr.BabyNameInHistoryWnd = name
	LuaBabyMgr.BabyPortraitInHistoryWnd = BabyMgr.Inst:GetBabyPortrait(status, gender, hairstyle)

	table.sort(dataTbl, function (a, b)
		if a.finishTime == b.finishTime then
			return a.playerId < b.playerId
		else
			return b.finishTime < a.finishTime
		end
	end)

	g_ScriptEvent:BroadcastInLua("QueryBabyLuckyMoneyHistoryDone", dataTbl)
end

LuaBabyMgr.m_BabyWndTab = 0
function LuaBabyMgr.OpenBabyWnd(tab, selectedBabyId)
	BabyMgr.Inst.SelectedBabyId = selectedBabyId
	local selectedBaby = BabyMgr.Inst:GetSelectedBaby()
	if selectedBaby and selectedBaby.Status > EnumBabyStatus.eBorn then
		LuaBabyMgr.m_BabyWndTab = tab
		CUIManager.ShowUI(CLuaUIResources.BabyWnd)
	end
end

----Begin 宝宝占卜
LuaBabyMgr.DivinationTbl = {}
function LuaBabyMgr.ShowBabyDivinationWnd(text)
	if text == nil or text == "" then return end
	local title, line1,line2 = string.match(text,LocalString.GetString("(.+)：(.+)%s(.+)"))
	if title and line1 and line2 then
		local data = {}
		data.title = title
		data.content = line1.."\n"..line2
		LuaBabyMgr.DivinationTbl = data
		CUIManager.ShowUI("BabyDivinationWnd")
	end
end

----End 宝宝占卜

LuaBabyMgr.m_AddTiLiBabyId = nil
function LuaBabyMgr.ShowGetBabyTiLiTip(babyId)
	if not babyId then return end
	LuaBabyMgr.m_AddTiLiBabyId = babyId
	CUIManager.ShowUI(CLuaUIResources.BabyTiLiGetTip)
end

----Begin Interact内容转Lua----

function LuaBabyMgr.GetBabyOnPlayerPlayerOpByMenuType(babyInteractMenuType, mgr, isBornBaby, isFollow, engineId, isBornBabyWithNurse)
	local ret = nil
	if EnumBabyInteractMenuType.eHouseMenu == babyInteractMenuType then
		ret = CommonDefs.ListCreate(typeof(HousePopupMenuItemData))
	elseif EnumBabyInteractMenuType.eTargetMenu == babyInteractMenuType then
		ret = CommonDefs.ListCreate(typeof(PopupMenuItemData))
	else
		return ret
	end

    if isFollow then
        LuaBabyMgr.AddPlayMenu(ret, mgr, babyInteractMenuType)
    else
        if not isBornBabyWithNurse then
        	LuaBabyMgr.AddFollowMenu(ret, mgr, babyInteractMenuType)
		end
    end

    if CSwitchMgr.EnableBabyChat and not isBornBabyWithNurse then
    	LuaBabyMgr.AddChatMenu(ret, mgr, babyInteractMenuType)
    end

    if not isBornBabyWithNurse then
    	LuaBabyMgr.AddShowExpressionMenu(ret, mgr, babyInteractMenuType)
    end

    if isBornBaby then
    	LuaBabyMgr.AddFeedMenu(ret, mgr, babyInteractMenuType)
    end

    LuaBabyMgr.AddLuckMoneyMenu(ret, mgr, babyInteractMenuType)--红包

    return ret
end

function LuaBabyMgr.GetBabyOnPlayerPartnerOpByMenuType(babyInteractMenuType, mgr, isBornBaby, engineId, isBornBabyWithNurse)
	local ret = nil
	if EnumBabyInteractMenuType.eHouseMenu == babyInteractMenuType then
		ret = CommonDefs.ListCreate(typeof(HousePopupMenuItemData))
	elseif EnumBabyInteractMenuType.eTargetMenu == babyInteractMenuType then
		ret = CommonDefs.ListCreate(typeof(PopupMenuItemData))
	else
		return ret
	end

    LuaBabyMgr.AddLoadBabyMenu(ret, mgr, babyInteractMenuType)

	if CSwitchMgr.EnableBabyChat and not isBornBabyWithNurse then
    	LuaBabyMgr.AddChatMenu(ret, mgr, babyInteractMenuType)
    end

    if not isBornBabyWithNurse then
    	LuaBabyMgr.AddShowExpressionMenu(ret, mgr, babyInteractMenuType)
    end

    if isBornBaby then
    	LuaBabyMgr.AddFeedMenu(ret, mgr, babyInteractMenuType)
    end

    LuaBabyMgr.AddLuckMoneyMenu(ret, mgr, babyInteractMenuType)--红包

    return ret
end

function LuaBabyMgr.GetBabyOnPlayerNotParentsOpByMenuType(babyInteractMenuType, mgr, isBornBaby, engineId, isBornBabyWithNurse, isBabyWithParent)

	local ret = nil
	if EnumBabyInteractMenuType.eHouseMenu == babyInteractMenuType then
		ret = CommonDefs.ListCreate(typeof(HousePopupMenuItemData))
	elseif EnumBabyInteractMenuType.eTargetMenu == babyInteractMenuType then
		ret = CommonDefs.ListCreate(typeof(PopupMenuItemData))
	else
		return ret
	end

    if not isBornBabyWithNurse then
    	LuaBabyMgr.AddShowExpressionMenu(ret, mgr, babyInteractMenuType)
    end

    if CSwitchMgr.EnableBabyChat and not isBornBabyWithNurse and isBabyWithParent and CSwitchMgr.EnableOthersBabyChat then
    	LuaBabyMgr.AddChatMenu(ret, mgr, babyInteractMenuType)
    end

    if isBabyWithParent then
    	LuaBabyMgr.AddLuckMoneyMenu(ret, mgr, babyInteractMenuType)--红包
    end

    return ret
end

function LuaBabyMgr.GeteBabyInHouseParentsOpByMenuType(babyInteractMenuType, mgr, isBornBaby, engineId, isBornBabyWithNurse)

	local ret = nil
	if EnumBabyInteractMenuType.eHouseMenu == babyInteractMenuType then
		ret = CommonDefs.ListCreate(typeof(HousePopupMenuItemData))
	elseif EnumBabyInteractMenuType.eTargetMenu == babyInteractMenuType then
		ret = CommonDefs.ListCreate(typeof(PopupMenuItemData))
	else
		return ret
	end

	LuaBabyMgr.AddLoadBabyInHouseMenu(ret, mgr, babyInteractMenuType)

	if not isBornBabyWithNurse then
    	LuaBabyMgr.AddShowExpressionMenu(ret, mgr, babyInteractMenuType)
    end

    if isBornBaby then
    	LuaBabyMgr.AddFeedMenu(ret, mgr, babyInteractMenuType)
    end

    return ret
end

function LuaBabyMgr.AddLoadBabyMenu(list, mgr, menuType)

	if menuType==EnumBabyInteractMenuType.eHouseMenu then
		CommonDefs.ListAdd_LuaCall(list, HousePopupMenuItemData("", DelegateFactory.Action_int(function ( index )
	    	if mgr.SelectedBabyNPC then
	        	Gac2Gas.RequestLoadBabyFromPlayer(mgr.SelectedBabyNPC.EngineId)
	        end
	    end), "UI/Texture/Transparent/Material/babyclothwnd_fuyang.mat", true, true, -1))
	elseif  menuType==EnumBabyInteractMenuType.eTargetMenu then
		CommonDefs.ListAdd_LuaCall(list, PopupMenuItemData(LocalString.GetString("抚育"), DelegateFactory.Action_int(function ( index )
	    	if mgr.SelectedBabyNPC then
	        	Gac2Gas.RequestLoadBabyFromPlayer(mgr.SelectedBabyNPC.EngineId)
	        end
	    end), false, nil))
	end
end

function LuaBabyMgr.AddLoadBabyInHouseMenu(list, mgr, menuType)

	if menuType==EnumBabyInteractMenuType.eHouseMenu then
		CommonDefs.ListAdd_LuaCall(list, HousePopupMenuItemData("", DelegateFactory.Action_int(function ( index )
	    	if mgr.SelectedBabyNPC then
	        	Gac2Gas.RequestLoadBabyFromHouse(mgr.SelectedBabyNPC.EngineId)
	        end
	    end), "UI/Texture/Transparent/Material/babyclothwnd_fuyang.mat", true, true, -1))
	elseif  menuType==EnumBabyInteractMenuType.eTargetMenu then
		CommonDefs.ListAdd_LuaCall(list, PopupMenuItemData(LocalString.GetString("抚育"), DelegateFactory.Action_int(function ( index )
	    	if mgr.SelectedBabyNPC then
	        	Gac2Gas.RequestLoadBabyFromHouse(mgr.SelectedBabyNPC.EngineId)
	        end
	    end), false, nil))
	end
end

function LuaBabyMgr.AddPlayMenu(list, mgr, menuType)

	if menuType==EnumBabyInteractMenuType.eHouseMenu then
		CommonDefs.ListAdd_LuaCall(list, HousePopupMenuItemData("", DelegateFactory.Action_int(function ( index )
	    	if mgr.SelectedBabyNPC then
	        	Gac2Gas.RequestSetBabyHaveFun(mgr.SelectedBabyNPC.EngineId)
	        end
	    end), "UI/Texture/Transparent/Material/babyclothwnd_wanshua.mat", true, true, -1))
	elseif  menuType==EnumBabyInteractMenuType.eTargetMenu then
		CommonDefs.ListAdd_LuaCall(list, PopupMenuItemData(LocalString.GetString("玩耍"), DelegateFactory.Action_int(function ( index )
	    	if mgr.SelectedBabyNPC then
	        	Gac2Gas.RequestSetBabyHaveFun(mgr.SelectedBabyNPC.EngineId)
	        end
	    end), false, nil))
	end
end

function LuaBabyMgr.AddFollowMenu(list, mgr , menuType)

	if menuType==EnumBabyInteractMenuType.eHouseMenu then
		CommonDefs.ListAdd_LuaCall(list, HousePopupMenuItemData("", DelegateFactory.Action_int(function ( index )
	    	if mgr.SelectedBabyNPC then
	        	Gac2Gas.RequestSetBabyFollow(mgr.SelectedBabyNPC.EngineId)
	        end
		end), "UI/Texture/Transparent/Material/babyclothwnd_gensui.mat", true, true, -1))
	elseif  menuType==EnumBabyInteractMenuType.eTargetMenu then
		CommonDefs.ListAdd_LuaCall(list, PopupMenuItemData(LocalString.GetString("跟随"), DelegateFactory.Action_int(function ( index )
	    	if mgr.SelectedBabyNPC then
	        	Gac2Gas.RequestSetBabyFollow(mgr.SelectedBabyNPC.EngineId)
	        end
	    end), false, nil))
	end
end

function LuaBabyMgr.AddChatMenu(list, mgr, menuType)

	if menuType==EnumBabyInteractMenuType.eHouseMenu then
		CommonDefs.ListAdd_LuaCall(list, HousePopupMenuItemData("", DelegateFactory.Action_int(function ( index )
	    	if mgr.SelectedBabyNPC then
	        	Gac2Gas.QueryBabyIdByEngineId(mgr.SelectedBabyNPC.EngineId)
	        end
    	end), "UI/Texture/Transparent/Material/babyclothwnd_liaotian.mat", true, true, -1))
	elseif  menuType==EnumBabyInteractMenuType.eTargetMenu then
		CommonDefs.ListAdd_LuaCall(list, PopupMenuItemData(LocalString.GetString("聊天"), DelegateFactory.Action_int(function ( index )
	    	if mgr.SelectedBabyNPC then
	        	Gac2Gas.QueryBabyIdByEngineId(mgr.SelectedBabyNPC.EngineId)
	        end
	    end), false, nil))
	end
end

function LuaBabyMgr.AddShowExpressionMenu(list, mgr, menuType)

	if menuType==EnumBabyInteractMenuType.eHouseMenu then
		CommonDefs.ListAdd_LuaCall(list, HousePopupMenuItemData("", DelegateFactory.Action_int(function ( index )
	    	if mgr.SelectedBabyNPC then
	        	mgr:RequestShowBabyExpressionAction(mgr.SelectedBabyNPC.EngineId)
	        end
    	end), "UI/Texture/Transparent/Material/babyclothwnd_biaoqing.mat", true, true, -1))
	elseif  menuType==EnumBabyInteractMenuType.eTargetMenu then
		CommonDefs.ListAdd_LuaCall(list, PopupMenuItemData(LocalString.GetString("互动"), DelegateFactory.Action_int(function ( index )
	    	if mgr.SelectedBabyNPC then
	        	mgr:RequestShowBabyExpressionAction(mgr.SelectedBabyNPC.EngineId)
	        end
	    end), false, nil))
	end
end

function LuaBabyMgr.AddFeedMenu(list, mgr, menuType)

	if menuType==EnumBabyInteractMenuType.eHouseMenu then
		CommonDefs.ListAdd_LuaCall(list, HousePopupMenuItemData("", DelegateFactory.Action_int(function ( index )
	    	if mgr.SelectedBabyNPC then
	        	Gac2Gas.QueryBabyGradeAndExp(mgr.SelectedBabyNPC.EngineId)
	        end
	    end), "UI/Texture/Transparent/Material/babyclothwnd_weishi.mat", true, true, -1))
	elseif  menuType==EnumBabyInteractMenuType.eTargetMenu then
		CommonDefs.ListAdd_LuaCall(list, PopupMenuItemData(LocalString.GetString("喂食"), DelegateFactory.Action_int(function ( index )
	    	if mgr.SelectedBabyNPC then
	        	Gac2Gas.QueryBabyGradeAndExp(mgr.SelectedBabyNPC.EngineId)
	        end
	    end), false, nil))
	end
end

function LuaBabyMgr.AddLuckMoneyMenu(list, mgr, menuType)

	if menuType==EnumBabyInteractMenuType.eHouseMenu then
		CommonDefs.ListAdd_LuaCall(list, HousePopupMenuItemData("", DelegateFactory.Action_int(function ( index )
	    	if mgr.SelectedBabyNPC then
	    		LuaBabyMgr.ShowSendLuckMoneyWnd(mgr.SelectedBabyNPC.EngineId)
	        end
	    end), "UI/Texture/Transparent/Material/yasuiqian_hongbao.mat", true, true, -1))
	elseif  menuType==EnumBabyInteractMenuType.eTargetMenu then
		CommonDefs.ListAdd_LuaCall(list, PopupMenuItemData(LocalString.GetString("发红包"), DelegateFactory.Action_int(function ( index )
	    	if mgr.SelectedBabyNPC then
	        	LuaBabyMgr.ShowSendLuckMoneyWnd(mgr.SelectedBabyNPC.EngineId)
	        end
	    end), false, nil))
	end
end

function LuaBabyMgr.GetBabyOnPlayerPlayerOp(mgr, isBornBaby, isFollow, isBornBabyWithNurse)
	if mgr.SelectedBabyNPC then
		return LuaBabyMgr.GetBabyOnPlayerPlayerOpByMenuType(EnumBabyInteractMenuType.eHouseMenu,
			mgr, isBornBaby, isFollow, mgr.SelectedBabyNPC.EngineId , isBornBabyWithNurse)
	end
end

function LuaBabyMgr.GetBabyOnPlayerPartnerOp(mgr, isBornBaby, isBornBabyWithNurse)
	if mgr.SelectedBabyNPC then
		return LuaBabyMgr.GetBabyOnPlayerPartnerOpByMenuType(EnumBabyInteractMenuType.eHouseMenu,
			mgr, isBornBaby, mgr.SelectedBabyNPC.EngineId , isBornBabyWithNurse)
	end
end

function LuaBabyMgr.GetBabyOnPlayerNotParentsOp(mgr, isBornBaby, isBornBabyWithNurse, isBabyWithParent)
	if mgr.SelectedBabyNPC then
		return LuaBabyMgr.GetBabyOnPlayerNotParentsOpByMenuType(EnumBabyInteractMenuType.eHouseMenu,
			mgr, isBornBaby, mgr.SelectedBabyNPC.EngineId , isBornBabyWithNurse, isBabyWithParent)
	end
end

function LuaBabyMgr.GetBabyInHouseParentsOp(mgr, isBornBaby, isBornBabyWithNurse)
	if mgr.SelectedBabyNPC then
		return LuaBabyMgr.GeteBabyInHouseParentsOpByMenuType(EnumBabyInteractMenuType.eHouseMenu,
			mgr, isBornBaby, mgr.SelectedBabyNPC.EngineId , isBornBabyWithNurse)
	end
end

function LuaBabyMgr.GetBabyOnPlayerPlayerOp_InTargetWnd(mgr, isBornBaby, isFollow, engineId, isBornBabyWithNurse)

	if mgr.SelectedBabyNPC then
		return LuaBabyMgr.GetBabyOnPlayerPlayerOpByMenuType(EnumBabyInteractMenuType.eTargetMenu,
			mgr, isBornBaby, isFollow, engineId , isBornBabyWithNurse)
	end
end

function LuaBabyMgr.GetBabyOnPlayerPartnerOp_InTargetWnd(mgr, isBornBaby, engineId, isBornBabyWithNurse)
	if mgr.SelectedBabyNPC then
		return LuaBabyMgr.GetBabyOnPlayerPartnerOpByMenuType(EnumBabyInteractMenuType.eTargetMenu,
			mgr, isBornBaby, mgr.SelectedBabyNPC.EngineId , isBornBabyWithNurse)
	end
end

function LuaBabyMgr.GetBabyOnPlayerNotParentsOp_InTargetWnd(mgr, isBornBaby, engineId, isBornBabyWithNurse, isBabyWithParent)
	if mgr.SelectedBabyNPC then
		return LuaBabyMgr.GetBabyOnPlayerNotParentsOpByMenuType(EnumBabyInteractMenuType.eTargetMenu,
			mgr, isBornBaby, mgr.SelectedBabyNPC.EngineId , isBornBabyWithNurse, isBabyWithParent)
	end
end

function LuaBabyMgr.GetBabyInHouseParentsOp_InTargetWnd(mgr, isBornBaby, engineId, isBornBabyWithNurse)
	if mgr.SelectedBabyNPC then
		return LuaBabyMgr.GeteBabyInHouseParentsOpByMenuType(EnumBabyInteractMenuType.eTargetMenu,
			mgr, isBornBaby, mgr.SelectedBabyNPC.EngineId , isBornBabyWithNurse)
	end
end

LuaBabyMgr.EnableBabyInteraction = true
function LuaBabyMgr.InteractWithBaby(babyIteractType)
	--拍照的时候有个拖动宝宝的操作，需要不弹出交互菜单
	if not LuaBabyMgr.EnableBabyInteraction then return end

	local mgr = BabyMgr.Inst
	local npc = mgr.SelectedBabyNPC
	if npc== nil then
		return
	end

	if not mgr:IsBaby(npc.TemplateId) then
		return
	end

	local list = nil
    local isBornBaby = mgr:IsBornBaby(npc)
    local isBornWithNurse = mgr:IsBornBabyWithNurse(npc)
    if babyIteractType == EnumInteractWithBabyType.eBabyOnPlayerPlayerOpFollow then --宝宝带上身上，跟随状态
    	list = LuaBabyMgr.GetBabyOnPlayerPlayerOp(mgr, isBornBaby, true, isBornWithNurse)
    elseif babyIteractType == EnumInteractWithBabyType.eBabyOnPlayerPlayerOpPlay then --宝宝带上身上，玩耍状态
    	list = LuaBabyMgr.GetBabyOnPlayerPlayerOp(mgr, isBornBaby, false, isBornWithNurse)
    elseif babyIteractType == EnumInteractWithBabyType.eBabyOnPlayerPartnerOp then --宝宝在伴侣身上
    	list = LuaBabyMgr.GetBabyOnPlayerPartnerOp(mgr, isBornBaby, isBornWithNurse)
    elseif babyIteractType == EnumInteractWithBabyType.eBabyOnPlayerNotParentsOp then --非家长，宝宝带在身上
    	list = LuaBabyMgr.GetBabyOnPlayerNotParentsOp(mgr, isBornBaby, isBornWithNurse, true)
    elseif babyIteractType == EnumInteractWithBabyType.eBabyInHouseNotParentsOp then --非家长玩家，宝宝没有带在父母身上
    	list = LuaBabyMgr.GetBabyOnPlayerNotParentsOp(mgr, isBornBaby, isBornWithNurse, false)
    elseif babyIteractType == EnumInteractWithBabyType.eBabyInHouseParentsOp then --家长未将宝宝戴在身上
    	list = LuaBabyMgr.GetBabyInHouseParentsOp(mgr, isBornBaby, isBornWithNurse)
    end

    if list~=nil and list.Count>0 then
    	CHousePopupMgr.ShowHousePopupMenu(CommonDefs.ListToArray(list), mgr.SelectedBabyNPC.RO:GetSlotTransform("TopAnchor"))
   	end

   	CLuaGuideMgr.OnInteractWithBaby(babyIteractType)
end

function LuaBabyMgr.InterWithBaby_InTargetWnd(babyIteractType)
	local mgr = BabyMgr.Inst
	if CClientMainPlayer.Inst ==nil or CClientMainPlayer.Inst.Target == nil then
       	return
    end

    if TypeIs(CClientMainPlayer.Inst.Target, typeof(CClientNpc)) then
        local npc = CClientMainPlayer.Inst.Target
        if mgr:IsBaby(npc.TemplateId) then
            local list = nil
            local isBornBaby = mgr:IsBornBaby(npc)
            local isBornWithNurse = mgr:IsBornBabyWithNurse(npc)
            if babyIteractType == EnumInteractWithBabyType.eBabyOnPlayerPlayerOpFollow then --宝宝带上身上，跟随状态
            	list = LuaBabyMgr.GetBabyOnPlayerPlayerOp_InTargetWnd(mgr, isBornBaby, true, npc.EngineId, isBornWithNurse)
            elseif babyIteractType == EnumInteractWithBabyType.eBabyOnPlayerPlayerOpPlay then --宝宝带上身上，玩耍状态
            	list = LuaBabyMgr.GetBabyOnPlayerPlayerOp_InTargetWnd(mgr, isBornBaby, false, npc.EngineId, isBornWithNurse)
            elseif babyIteractType == EnumInteractWithBabyType.eBabyOnPlayerPartnerOp then --宝宝在伴侣身上
            	list = LuaBabyMgr.GetBabyOnPlayerPartnerOp_InTargetWnd(mgr, isBornBaby, npc.EngineId, isBornWithNurse)
            elseif babyIteractType == EnumInteractWithBabyType.eBabyOnPlayerNotParentsOp then --非家长，宝宝带在家长身上
            	list = LuaBabyMgr.GetBabyOnPlayerNotParentsOp_InTargetWnd(mgr, isBornBaby, npc.EngineId, isBornWithNurse, true)
            elseif babyIteractType == EnumInteractWithBabyType.eBabyInHouseNotParentsOp then --非家长，宝宝没有带在家长身上
            	list = LuaBabyMgr.GetBabyOnPlayerNotParentsOp_InTargetWnd(mgr, isBornBaby, npc.EngineId, isBornWithNurse, false)
            elseif babyIteractType == EnumInteractWithBabyType.eBabyInHouseParentsOp then --家长未将宝宝戴在身上
            	list = LuaBabyMgr.GetBabyInHouseParentsOp_InTargetWnd(mgr, isBornBaby, npc.EngineId, isBornWithNurse)
            end

            if list~=nil and list.Count>0 then
            	CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(list), mgr.InteractPos, CPopupMenuInfoMgr.AlignType.Bottom)
           	end
        end
    end
end
----End Interact内容转Lua----




----------------------------  C#下BabyMgr的一些方法转换成lua  ----------------------------

BabyMgr.m_hookUpdateBabyData = function (mgr, babyId, babyUD)
	if CommonDefs.DictContains_LuaCall(mgr.BabyDictionary, babyId) then
		local baby = mgr.BabyDictionary[babyId]
		baby:LoadFromString(babyUD)
		EventManager.Broadcast(EnumEventType.MainPlayerUpdateMoney)
		g_ScriptEvent:BroadcastInLua("UpdateBabyData")
	else
	end
end

-- 获得宝宝已经解锁的表情动作
BabyMgr.m_hookGetValidBabySingleExpression = function (mgr, baby)
	local list = CreateFromClass(MakeGenericClass(List, UInt32))
	local showStatus = baby.Props.ExtraData.ShowStatus
	Expression_BabyShow.ForeachKey(function (key)
		local show = Expression_BabyShow.GetData(key)
		if showStatus == show.BabyStatus and show.Type == 1 and show.Display then
			if show.LockGroup == 0 then
				CommonDefs.ListAdd_LuaCall(list, key)
			else
				if baby.Props.UnLockExpression and show.LockGroup < baby.Props.UnLockExpression.Length then
					if baby.Props.UnLockExpression[show.LockGroup] == 1 then
						CommonDefs.ListAdd_LuaCall(list, key)
					end
				end
			end
		end
	end)
	return list
end

----------------------------  End C#下BabyMgr的一些方法转换成lua  ----------------------------





----------------------------  C#下的BabyMgr方法扩展到Lua下使用  ----------------------------

BabyMgr.m_OnPlayerLoginResetFunc = function (mgr)
	LuaBabyMgr.BabyId2UnreadMsg = {}
end

BabyMgr.m_QueryBabyOwnerPlayerIdFunc = function (clientnpc)
	Gac2Gas.QueryBabyOwnerPlayerId(clientnpc.EngineId, 1)
end


-------------------------------养育迭代2023-------------------------------------------------
function LuaBabyMgr.GetQiChangRequestPropByIndex(index, qichang)
    if index == 1 then
        return qichang.Strategy
    elseif index == 2 then
        return qichang.Taoism
    elseif index == 3 then
        return qichang.Martial
    elseif index == 4 then
        return qichang.Military
    elseif index == 5 then
        return qichang.Poetry
    elseif index == 6 then
        return qichang.Yayi
    elseif index == 7 then
        return qichang.Heaven
    elseif index == 8 then
        return qichang.Process
    elseif index == 9 then
        return qichang.Virtue
    elseif index == 10 then
        return qichang.Ambiguity
    elseif index == 11 then
        return qichang.Tyrannical
    elseif index == 12 then
        return qichang.Evil
    end
    return ""
end

function LuaBabyMgr.IsQiChangUnlocked(qichangId,SelectedBaby)
	local isSatisfied = true
	local qichang = Baby_QiChang.GetData(qichangId)
	local preQichangId = qichang.PreQichang
    local activedQiChang = SelectedBaby.Props.QiChangData.ActivedQiChang
    -- step 1 前置气场
    if preQichangId ~= 0 then
        local preQiChang = Baby_QiChang.GetData(preQichangId)
        if preQiChang then
            -- 检查前置气场是否鉴定
            if not CommonDefs.DictContains_LuaCall(activedQiChang, preQiChang.ID) then
				isSatisfied = false
				return false
            end
        end     
    end

    -- Step 2 获得奇遇
    local qiyuDict = SelectedBaby.Props.ScheduleData.QiYuData
    if qichang.QiyuGet ~= 0 then
        local qiyu = Baby_QiYu.GetData(qichang.QiyuGet)
        if qiyu then
            if not CommonDefs.DictContains_LuaCall(qiyuDict, qiyu.ID) then
				isSatisfied = false
				return false
            end
        end
    end

    -- Step 3 属性要求, 有12个
    for i = 1, 12 do
        if LuaBabyMgr.GetQiChangRequestPropByIndex(i, qichang) > 0 then
            local request = LuaBabyMgr.GetQiChangRequestPropByIndex(i, qichang)
            local own = 0
            if SelectedBaby.Props.YoungProp and CommonDefs.DictContains_LuaCall(SelectedBaby.Props.YoungProp, i) then
                own = SelectedBaby.Props.YoungProp[i]
            end
            local shaoNian = Baby_ShaoNian.GetData(i)
            local youEr = Baby_YouEr.GetData(shaoNian.YouErName)
            if shaoNian and youEr then          
                if own < request then
					isSatisfied = false
					return false
                end
            end
        end
    end

    -- Step 4 属性最高
    if qichang.TopAttribute ~= 0 and qichang.Quality >= 5 then
        local youer = Baby_YouEr.GetData(qichang.TopAttribute)

        if youer and SelectedBaby.Props.YoungProp then
            local highest = 0

            if CommonDefs.DictContains_LuaCall(SelectedBaby.Props.YoungProp, qichang.TopAttribute) then
                highest = LuaBabyMgr.GetYoundPropByGroup(SelectedBaby, qichang.TopAttribute)
            end
            local isHighest = true

            if SelectedBaby.Props.YoungProp and SelectedBaby.Props.YoungProp.Count >= 12 then
                for i = 1, 6, 1 do
                    if qichang.TopAttribute ~=i and LuaBabyMgr.GetYoundPropByGroup(SelectedBaby, i) > highest then
                        isHighest = false
                    end
                end
            end
            if not isHighest then
				isSatisfied = false
				return false
            end
        end
    end
	return isSatisfied
end

function LuaBabyMgr.MakeQiChangTree(qiChangId)
	local tree = {}
	table.insert(tree,qiChangId)
	--所有前置
	local data = Baby_QiChang.GetData(qiChangId)
	while data and data.PreQichang>0 do
		table.insert(tree,data.PreQichang)
		data = Baby_QiChang.GetData(data.PreQichang)	
	end

	--找后置
	--local nextId = qiChangId
	--Baby_QiChang.Foreach(function(key, v)
    --    if(v.PreQichang == nextId) then
	--		nextId = key
	--		table.insert(tree,key)
	--	end
    --end)
	table.sort(tree,function(a,b)
		return a<b
	end)
	return tree
end

function LuaBabyMgr.OpenSetTargetQiChangWnd(qiChangId)
	--SetTargetQiChangWnd
	LuaBabyMgr.mSetTargetWndQiChangId = qiChangId
	CUIManager.ShowUI(CLuaUIResources.SetTargetQiChangWnd)
end

function LuaBabyMgr.isAllQiChangActive(selectedBaby)
	local allActived = true
	local activedQiChang = selectedBaby.Props.QiChangData.ActivedQiChang

	Baby_QiChang.Foreach(function(key, qichang)
		local isActive = CommonDefs.DictContains_LuaCall(activedQiChang, key)
		allActived = allActived and isActive
	end)
	
	return allActived
	
end
----------------------------  Gas2Gac  ----------------------------

--Baby交互
function Gas2Gac.QueryBabyNpcInteractSuccess(interactType, queryType)
	if queryType == EnumToInt(EnumBabyInteractPlace.eNPC) then
    	LuaBabyMgr.InteractWithBaby(interactType)
    elseif queryType == EnumToInt(EnumBabyInteractPlace.eTarget) then
        LuaBabyMgr.InterWithBaby_InTargetWnd(interactType)
    end
end

-- 入口按钮处理
function Gas2Gac.OpenYangYuWnd(openType)

	if openType == EnumYangYuOpenType.Qualification then
		-- 尚未获得养育资格，提示去获得
		g_MessageMgr:ShowMessage("NO_YANGYU_QUALIFICATION")
	elseif openType == EnumYangYuOpenType.PregnantDesc then
		-- 获得养育资格，提示需要怀孕
		g_MessageMgr:ShowMessage("NO_YANGYU_PREGNANT")
	elseif openType == EnumYangYuOpenType.InspectDiary then
		-- 显示产检手册
		Gac2Gas.RequestInspectInfo()
	elseif openType == EnumYangYuOpenType.BabyNotOnPlayer then
		-- 宝宝没有带在玩家身上，进行提示
		local msg = g_MessageMgr:FormatMessage("NO_BABY_ON_PLAYER")
		MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
			CClientHouseMgr.Inst:GoBackHome()
		end), nil, LocalString.GetString("回家"), nil, false)
		--MessageMgr.Inst:ShowMessage("NO_BABY_ON_PLAYER", {})
	elseif openType == EnumYangYuOpenType.BabyOnPlayer then
		-- 身上带有宝宝，打开界面
		LuaBabyMgr.m_BabyOnPlayerAndPregnant = false
		CUIManager.ShowUI(CLuaUIResources.BabyWnd)
	elseif openType == EnumYangYuOpenType.BabyOnPlayerAndPregnant then
		-- 身上带有宝宝且怀孕
		LuaBabyMgr.m_BabyOnPlayerAndPregnant = true
		CUIManager.ShowUI(CLuaUIResources.BabyWnd)
	end
end

-- 派遣宝宝成功
function Gas2Gac.PlayerPlanBabySuccess(babyId)
	g_ScriptEvent:BroadcastInLua("PlayerPlanBabySuccess", babyId)
end

-- 领取奖励成功
function Gas2Gac.ReceiveBabyPlanAwardSuccess(babyId)
	-- 刷新日程奖励界面

	-- TODO eurekaliu 判断babyId是否是选中的babyId
	if CUIManager.IsLoaded(CLuaUIResources.BabyScheduleGainWnd) then
        g_ScriptEvent:BroadcastInLua("UpdateBabyPlanAward")
    else
        CUIManager.ShowUI(CLuaUIResources.BabyScheduleGainWnd)
    end
    LuaBabyMgr.ShowBabyProDif()
end

-- 刷新可派遣任务成功
function Gas2Gac.RefreshBabyPlanSuccess(babyId)
	LuaBabyMgr.m_SelectedPlan = {}
	if CUIManager.IsLoaded(CLuaUIResources.BabySchedulePlanList) then
        g_ScriptEvent:BroadcastInLua("RefreshBabyPlanSuccess")
    else
        CUIManager.ShowUI(CLuaUIResources.BabySchedulePlanList)
    end
end

-- 准生证校验结果
function Gas2Gac.CheckBirthCertificateSuccess(isCouple, conditionResult)
	-- 单人或多人准生证
	LuaBabyMgr.m_IsCoupleBirthPermission = isCouple

	local infos = MsgPackImpl.unpack(conditionResult)
    if not infos then return end

	LuaBabyMgr.m_BirthPermissionResult = {}

	for i = 0, infos.Count - 1, 1 do
		local result = infos[i]
		table.insert(LuaBabyMgr.m_BirthPermissionResult, result)
	end

	if CUIManager.IsLoaded(CLuaUIResources.BirthPermissionWnd) then
        g_ScriptEvent:BroadcastInLua("UpdateBirthPermissionResult", LuaBabyMgr.m_BirthPermissionResult)
    else
        CUIManager.ShowUI(CLuaUIResources.BirthPermissionWnd)
    end

end

-- 通知医生结果
function Gas2Gac.InspectBabySuccess(inspectTime, inpsectInfoUD)
	local infoList = MsgPackImpl.unpack(inpsectInfoUD)
	if not infoList then return end

	LuaBabyMgr.m_InspectTime = inspectTime
	LuaBabyMgr.m_InspectInfo = {}
	LuaBabyMgr.m_InspectOpenType = 2
	LuaBabyMgr.m_OpenByDoctor = true

	for i = 0, infoList.Count - 1, 1 do
		table.insert(LuaBabyMgr.m_InspectInfo, infoList[i])
	end

	if CUIManager.IsLoaded(CLuaUIResources.ProductionInspectionWnd) then
		CUIManager.CloseUI(CLuaUIResources.ProductionInspectionWnd)
    end

	local setting = Baby_Setting.GetData()
	SubtitileMgr.ShowSubtitle(setting.InspectSubtitleId, DelegateFactory.Action(function ()
		CUIManager.ShowUI(CLuaUIResources.ProductionInspectionWnd)
	end))

end

-- 返回产检信息
function Gas2Gac.SendInspectInfo(inspectTime, inpsectInfoUD)

	local infoList = MsgPackImpl.unpack(inpsectInfoUD)
	if not infoList then return end

	LuaBabyMgr.m_InspectTime = inspectTime
	LuaBabyMgr.m_InspectInfo = {}
	LuaBabyMgr.m_InspectOpenType = 1
	LuaBabyMgr.m_OpenByDoctor = false

	for i = 0, infoList.Count - 1, 1 do
		table.insert(LuaBabyMgr.m_InspectInfo, infoList[i])
	end

	if CUIManager.IsLoaded(CLuaUIResources.ProductionInspectionWnd) then
        g_ScriptEvent:BroadcastInLua("UpdateProductionInspection", LuaBabyMgr.m_InspectTime, LuaBabyMgr.m_InspectInfo)
    else
		CUIManager.ShowUI(CLuaUIResources.ProductionInspectionWnd)
    end
end

-- 开始选择对话
function Gas2Gac.StartSelectDialog()
	CUIManager.ShowUI(CLuaUIResources.PrenatalMessageWnd)
end

-- 接任务成功
function Gas2Gac.AcceptTaskFromBabySuccess(babyId)
		g_ScriptEvent:BroadcastInLua("AcceptTaskFromBabySuccess", babyId)
end

-- 重命名成功
function Gas2Gac.PlayerRenameBabySuccess(babyId)
	if LuaBabyMgr.m_NamingBabyId == babyId then
		CUIManager.CloseUI(CLuaUIResources.BabyNamingWnd)
	end
	g_ScriptEvent:BroadcastInLua("PlayerRenameBabySuccess", babyId)
end

-- 打开取名界面 废弃
function Gas2Gac.OpenNamingBabyWnd(babyId)
	LuaBabyMgr.m_NamingBabyId = babyId
	LuaBabyMgr.m_IsRename = false
	CUIManager.ShowUI(CLuaUIResources.BabyNamingWnd)
end


function Gas2Gac.QueryRestorableBabySuccess(babyInfoTblUD)
	LuaBabyMgr.m_ShowGetBabyInfo = {}
	LuaBabyMgr.m_ShowGetBabyType = 1
	local info = MsgPackImpl.unpack(babyInfoTblUD)
	if not info then return end
	for i = 0, info.Count - 1 do
		local babyId = info[i][0]
		local grade = info[i][1]
		local gender = info[i][2]
		local name = info[i][3]
		local status = info[i][4]
		local hairstyle = info[i][5]
		table.insert(LuaBabyMgr.m_ShowGetBabyInfo,{babyId,grade,gender,name,status,hairstyle})
	end
	CUIManager.ShowUI(CLuaUIResources.BabyGetBackWnd)
end

function Gas2Gac.SendOrphanInfo(babyInfoTblUD)
	LuaBabyMgr.m_ShowGetBabyInfo = {}
	LuaBabyMgr.m_ShowGetBabyType = 2
	local info = MsgPackImpl.unpack(babyInfoTblUD)
	if not info then return end
	for i = 0, info.Count - 1 do
		local babyId = info[i][0]
		local grade = info[i][1]
		local gender = info[i][2]
		local name = info[i][3]
		local status = info[i][4]
		local hairstyle = info[i][5]
		table.insert(LuaBabyMgr.m_ShowGetBabyInfo,{babyId,grade,gender,name,status,hairstyle})
	end
	CUIManager.ShowUI(CLuaUIResources.BabyGetBackWnd)
end

-- 激活宝宝表情动作成功
function Gas2Gac.ActiveBabyExpressionSuccess(babyId)
	g_ScriptEvent:BroadcastInLua("ActiveBabyExpressionSuccess", babyId)
end

-- 宝宝喂食成功（对宝宝主人的回调）
function Gas2Gac.PlayerFeedingBabyNpcSuccess(babyId)
	--if BabyMgr.Inst.SelectedBabyNPC and BabyMgr.Inst.SelectedBabyNPC.
	--喂养成功之后 检查等级
	-- CLuaGuideMgr.TryTriggerBabyUpgradeGuide(babyId)
end

-- 请求宝宝是否召唤回调
function Gas2Gac.QueryBabyFollowStatusDone(bFollow)
	g_ScriptEvent:BroadcastInLua("QueryBabyFollowStatusDone", bFollow)
end

-- 接生动画
function Gas2Gac.FinishedDeliveryBaby(isSingleMale)
	if isSingleMale then
		AMPlayer.Inst:PlayCG("yangyu_boy_batch.mp4", nil, 1)
	else
		AMPlayer.Inst:PlayCG("yangyu_girl_batch.mp4", nil, 1)
	end
end

-- 返回npcengineid的等级和经验
function Gas2Gac.SendBabyGradeAndExp(npcEngineId, grade, exp, remainTimes)
	LuaBabyMgr.ShowBabyFeedWnd(npcEngineId, grade, exp, remainTimes)
end

-- 玩家进入胎梦
function Gas2Gac.PlayerEnterPregnantStatus(taiMengId, isSingle)
	LuaBabyMgr.m_TaiMengId = taiMengId

	if not isSingle then
		SubtitileMgr.ShowSubtitle(34001601, DelegateFactory.Action(function ()
			AMPlayer.Inst:PlayCG("yangyu_taimeng.mp4", DelegateFactory.Action(function ()
				SubtitileMgr.ShowSubtitle(34001603, DelegateFactory.Action(function ()
					CUIManager.ShowUI(CLuaUIResources.BabyTaiMengWnd)
				end))
			end), 1)
		end))
	else
		SubtitileMgr.ShowSubtitle(34001603, DelegateFactory.Action(function ()
			CUIManager.ShowUI(CLuaUIResources.BabyTaiMengWnd)
		end))
	end
end

-- 玩家领取奇遇奖励成功
function Gas2Gac.ReceiveQiYuAwardSuccess(babyId)
	g_ScriptEvent:BroadcastInLua("ReceiveQiYuAwardSuccess", babyId)
end

-- 领取baby的奇遇任务成功
function Gas2Gac.AcceptQiYuTaskFromBabySuccess(babyId)
	g_ScriptEvent:BroadcastInLua("AcceptQiYuTaskFromBabySuccess", babyId)
end

function Gas2Gac.OpenBabyChatWnd(babyId)
	-- TODO 待移除
end
function Gas2Gac.ReplyChatMessageToBabySuccess(babyId, originMessageId, skillName, message)
	LuaBabyMgr.ReplyChatMessageToBaby(babyId, true, skillName, originMessageId, message)
end

function Gas2Gac.ReplyChatMessageToBabyFail(babyId, originMessageId, skillName)
	LuaBabyMgr.ReplyChatMessageToBaby(babyId, false, skillName, originMessageId, nil)
end

function Gas2Gac.PushBabyChatMessage(babyId, skillName, message)
	LuaBabyMgr.PushBabyChatMessage(babyId, skillName, message)
end

-- 获得父母的名字
function Gas2Gac.QueryBabyParentsNameDone(babyId, fatherName, motherName)
	g_ScriptEvent:BroadcastInLua("QueryBabyParentsNameDone", babyId, fatherName, motherName)
end

---------- 养育资格 ----------

function Gas2Gac.RearingTestSendQuestion(questionId, question, answer, leftTime, currentScore)

	local answerInfos = MsgPackImpl.unpack(answer)
	if not answerInfos then return end

	LuaBabyMgr.m_CurrentQuestionIndex = questionId
	LuaBabyMgr.m_AnswerList = {}
	LuaBabyMgr.m_Question = question

	for i = 0, answerInfos.Count - 1, 1 do
		table.insert(LuaBabyMgr.m_AnswerList, answerInfos[i])
	end
	LuaBabyMgr.m_QuestionCountDown = leftTime
	LuaBabyMgr.m_CurrentScore = currentScore
	CUIManager.ShowUI(CLuaUIResources.ParentExamWnd)
end

function Gas2Gac.RearingTestSendAnswer(answerIdx, curSubmitAnswer, curLandMineIdx, CurLandMineMsg)
	LuaBabyMgr.m_RightAnswerIndex = answerIdx
	g_ScriptEvent:BroadcastInLua("RearingTestAnswerUpdate")

	-- 注册一个tick
	RegisterTickOnce(function ()
		if curSubmitAnswer == curLandMineIdx and curSubmitAnswer ~= 0 then
			LuaBabyMgr.ShowParentExamResultLandMine(CurLandMineMsg)
		end
	end, 3000)
end

function Gas2Gac.RearingTestStop(reason)
	CUIManager.CloseUI(CLuaUIResources.ParentExamWnd)
end

function Gas2Gac.RearingTestFinished(score, passScore)

	LuaBabyMgr.m_FinalScore = score
	LuaBabyMgr.m_PassScore = passScore
	LuaBabyMgr.m_IsLandMine = false
	-- 关闭界面
	CUIManager.CloseUI(CLuaUIResources.ParentExamWnd)
	CUIManager.ShowUI(CLuaUIResources.ParentExamResultWnd)
end

function Gas2Gac.AmusementParkTakePhoto(reason)
	CUIManager.ShowUI(CLuaUIResources.AutoTakingPhotoWnd)
end

--#region 养育基金

function Gas2Gac.BabyFundRequestShowEntranceResult(show, paid, infoU)
	-- show == 1 显示, paid == 1 已经买过
	LuaBabyMgr.ClearBabyFundInfo()

	LuaBabyMgr.m_BabyFundShow = show
	LuaBabyMgr.m_BabyFundBuy = paid
	LuaBabyMgr.m_BabyFundInfo = {}

	-- paid == 1 时才有数据
	local info = MsgPackImpl.unpack(infoU)
	if not info then return end
	for i = 0, info.Count - 1, 2 do
		local packageId = info[i]

		-- 状态 eCanNotRecv, eCanRecv, eRecved = 0, 1, 2
		local status = info[i+1]
		LuaBabyMgr.m_BabyFundInfo[packageId] = status
	end

	g_ScriptEvent:BroadcastInLua("ReceiveBabyFundInfo")
end

function Gas2Gac.BabyFundRequestBuyFundSuccess()

	Gac2Gas.BabyFundRequestShowEntrance()
end

function Gas2Gac.BabyFundRequestRecvPackageSuccess(packageId)

	Gac2Gas.BabyFundRequestShowEntrance()
end

function Gas2Gac.ReplyBabyUnLockExpressionGroupInfo(babyEngineId, unlockGroupData, showStatus)
	BabyMgr.Inst.ExpressionBabyEngineId = babyEngineId

    local obj = CClientObjectMgr.Inst:GetObject(babyEngineId)
    if not obj then
        return
    end

    if not TypeIs(obj, typeof(CClientNpc)) then
    	return
    end

    if not BabyMgr.Inst:IsBaby(obj.TemplateId) then
        return
    end

	local babyExpressionList = BabyMgr.Inst:GetBabyExpressionsListInfo(showStatus, unlockGroupData)
	if not babyExpressionList then return end


	LuaBabyMgr.m_ExpressionBabyEngineId = babyEngineId
	LuaBabyMgr.m_BabyExpressionList = babyExpressionList

	if CUIManager.IsLoaded(CLuaUIResources.BabyExpressionActionWnd) then
		g_ScriptEvent:BroadcastInLua("UpdateBabyExpressionUnlockGroup")
	else
		CUIManager.ShowUI(CLuaUIResources.BabyExpressionActionWnd)
	end

end

function Gas2Gac.UnLockBabyExpression(expressionId)
	LuaBabyMgr.m_NowShowExpressionId = expressionId
	CUIManager.ShowUI(CLuaUIResources.BabyExpressionUnlockWnd)
end

-- 解锁气场成功
function Gas2Gac.ActiveBabyQiChangSuccess(babyId, newQiChangId)
	g_ScriptEvent:BroadcastInLua("ActiveBabyQiChangSuccess", babyId, newQiChangId)
end

function Gas2Gac.SyncPlayerOwnBabyInfo(qiChangId, babyGrade)
	CClientMainPlayer.Inst.PlayProp.OwnBabyQiChangId = qiChangId
	CClientMainPlayer.Inst.PlayProp.OwnBabyGrade = babyGrade
end

-- 更新结伴气场,如果bSuccess 为 true, 则更新 playProp -> JieBanQiChangId,数据就存在这里
function Gas2Gac.SetJieBanBabyQiChangRes(bSuccess, babyId, qiChangId)
	if bSuccess and CClientMainPlayer.Inst then
		CClientMainPlayer.Inst.PlayProp.JieBanQiChangId = qiChangId
		g_ScriptEvent:BroadcastInLua("SetJieBanBabyQiChangRes", bSuccess, babyId, qiChangId)
	end
end

-- 同步已洗练次数
function Gas2Gac.SyncAdmonishTimes(expireTime, babyId, admonishTimes, enhanceTimes)
	local baby = BabyMgr.Inst:GetBabyById(babyId)
	if baby then
		if baby.Props.ExtraData.WashExpireTime <= expireTime then
			baby.Props.ExtraData.AdmonishTimes = admonishTimes
			baby.Props.ExtraData.EnhanceTimes = enhanceTimes
			 baby.Props.ExtraData.WashExpireTime = expireTime
		end
	end
	g_ScriptEvent:BroadcastInLua("BabySyncAdmonishTimes")
end

-- isAdd 表示选定一个属性增加,也就是规仪
function Gas2Gac.SyncAdmonishRes(babyId, isAdd, chosenPropId, newChosenProp, targetPropId, newTargetProp)
	g_ScriptEvent:BroadcastInLua("SyncAdmonishRes", babyId, isAdd, chosenPropId, newChosenProp, targetPropId, newTargetProp)
end

-- 洗练失败, failType是失败的各种情况,现在应该还用不到,因为提示信息服务端已经提示过了
function Gas2Gac.SyncAdmonishBabyFail(failType)
	g_ScriptEvent:BroadcastInLua("SyncAdmonishBabyFail")
end

function Gas2Gac.SyncQueryBabyOwnerPlayerId(npcEngineId, actionType, ownerId)
	-- 获得npc
	if ownerId == CClientMainPlayer.Inst.Id then return end

	local npc = CClientObjectMgr.Inst:GetObject(npcEngineId)
	local player = CClientPlayerMgr.Inst:GetPlayer(ownerId)
	if npc and player then
		npc.m_BabyOwnerPlayerId = ownerId
		player.BabyEngineId = npcEngineId
		npc.Visible = player.Visible
	end
end

function Gas2Gac.SyncQueryBabyIdByEngineIdRes(npcEngineId, babyId, currentOwnerId)
	local mainplayerIsOwner = (CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == currentOwnerId)
	LuaBabyMgr.OpenBabyChatWnd(babyId, npcEngineId, mainplayerIsOwner, EnumShowBabyChatContext.eFromNPC)
end

--#endregion

function Gas2Gac.SetShowQiChangIdSuccess(babyId, qiChangId)
	g_ScriptEvent:BroadcastInLua("SetShowQiChangIdSuccess", babyId, qiChangId)
end

function Gas2Gac.ChangeBabyStatusSuccess(babyId)
	g_ScriptEvent:BroadcastInLua("ChangeBabyStatusSuccess", babyId)
end

-- 宝宝去收菜或种田
function Gas2Gac.PlantAndHarvestInHouseSuccess(babyId)
	g_ScriptEvent:BroadcastInLua("PlantAndHarvestInHouseSuccess", babyId)
end

-- 宝宝种田回来了
function Gas2Gac.BabyPlantAndHarvestComeBack(babyId)
	g_ScriptEvent:BroadcastInLua("BabyPlantAndHarvestComeBack", babyId)
end

function Gas2Gac.SendLuckyMoneyToBabySuccess()
	LuaBabyMgr.OnSendLuckMoneySuccess()
end

function Gas2Gac.QueryBabyLuckyMoneyHistoryDone(name, status, gender, hairstyle, historyInfoUD)
	local list = MsgPackImpl.unpack(historyInfoUD)
	if not list then return end

	local dataTbl = {}
	for i=0,list.Count-1,6 do

		local data = {}
		data.finishTime = tonumber(list[i])
		data.playerName = tostring(list[i+1])
		data.silverNum = tonumber(list[i+2])
		data.messageId = tonumber(list[i+3])
		data.handOver = tonumber(list[i+4])
		data.playerId = tonumber(list[i+5])

		table.insert(dataTbl, data)
	end
	LuaBabyMgr.QueryBabyLuckyMoneyHistoryDone(name, status, gender, hairstyle, dataTbl)
end

-- 健体丸使用成功
function Gas2Gac.QuickAddBabyTiliSuccess(babyId)
	g_ScriptEvent:BroadcastInLua("QuickAddBabyTiliSuccess", babyId)
end

function Gas2Gac.ReceiveQiChangActiveRewardSuccess(babyId)
	print("ReceiveQiChangActiveRewardSuccess", babyId)
	g_ScriptEvent:BroadcastInLua("ReceiveQiChangActiveRewardSuccess", babyId)
end

function Gas2Gac.SyncRecommendQiChang(babyId, qiChangId)
	print("SyncRecommendQiChang", babyId, qiChangId)
	LuaBabyMgr.mRecormmendQiChangId = qiChangId
	g_ScriptEvent:BroadcastInLua("SyncRecommendQiChang", babyId, qiChangId)
end

function Gas2Gac.SyncSetTargetQiChangRes(babyId, qiChangId)
	print("SyncSetTargetQiChangRes", babyId, qiChangId)
	LuaBabyMgr.mTargetQiChangId = qiChangId
	g_ScriptEvent:BroadcastInLua("SyncSetTargetQiChangRes", babyId, qiChangId)
end

function Gas2Gac.SyncTargetQiChangInfo(babyId, targetQiChangId, recommendQiChangId)
	print("SyncTargetQiChangInfo", babyId, targetQiChangId, recommendQiChangId)
	LuaBabyMgr.mTargetQiChangId = targetQiChangId
	--LuaBabyMgr.mRecormmendQiChangId = recommendQiChangId
	g_ScriptEvent:BroadcastInLua("SyncTargetQiChangInfo", babyId, targetQiChangId, recommendQiChangId)
end
