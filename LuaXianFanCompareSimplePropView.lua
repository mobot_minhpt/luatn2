require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local EPlayerFightProp=import "L10.Game.EPlayerFightProp"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CTooltip=import "L10.UI.CTooltip"
local MessageMgr=import "L10.Game.MessageMgr"
local Color=import "UnityEngine.Color"

--仙凡属性 基础属性 详细属性
CLuaXianFanCompareSimplePropView=class()
--基础属性
RegistClassMember(CLuaXianFanCompareSimplePropView,"m_Cor")--根骨
RegistClassMember(CLuaXianFanCompareSimplePropView,"m_AttackType")--攻击类型：物理攻击、法术攻击
RegistClassMember(CLuaXianFanCompareSimplePropView,"m_Attack")--攻击数值：“最小攻击-最大攻击”

RegistClassMember(CLuaXianFanCompareSimplePropView,"m_Sta")--精力
RegistClassMember(CLuaXianFanCompareSimplePropView,"m_HpFull")--最大气血
RegistClassMember(CLuaXianFanCompareSimplePropView,"m_MpFull")--最大法力

RegistClassMember(CLuaXianFanCompareSimplePropView,"m_Str")--力量
RegistClassMember(CLuaXianFanCompareSimplePropView,"m_HitType")--命中类型：物理命中、法术命中
RegistClassMember(CLuaXianFanCompareSimplePropView,"m_Hit")--命中数值
RegistClassMember(CLuaXianFanCompareSimplePropView,"m_FatalType")--致命类型：物理致命、法术致命
RegistClassMember(CLuaXianFanCompareSimplePropView,"m_Fatal")--致命数值

RegistClassMember(CLuaXianFanCompareSimplePropView,"m_Int")--智力
RegistClassMember(CLuaXianFanCompareSimplePropView,"m_pDef")--物理防御
RegistClassMember(CLuaXianFanCompareSimplePropView,"m_mDef")--物理防御

RegistClassMember(CLuaXianFanCompareSimplePropView,"m_Agi")--敏捷
RegistClassMember(CLuaXianFanCompareSimplePropView,"m_pMiss")--物理躲避
RegistClassMember(CLuaXianFanCompareSimplePropView,"m_mMiss")--法术躲避

RegistClassMember(CLuaXianFanCompareSimplePropView,"m_FightProp")

RegistClassMember(CLuaXianFanCompareSimplePropView,"m_IsLeft")



function CLuaXianFanCompareSimplePropView:Init(tf,fightProp,left)
    self.m_IsLeft=left
    if not fightProp then return end
    self.m_FightProp=fightProp
    if CClientMainPlayer.Inst==nil then return end
    local physical=CClientMainPlayer.Inst.IsPhysicalAttackType

    local t={}
    self.m_Cor=LuaGameObject.GetChildNoGC(tf,"Cor").label
    table.insert( t,self.m_Cor )
    self.m_AttackType=LuaGameObject.GetChildNoGC(tf,"AttackType").label
    self.m_Attack=LuaGameObject.GetChildNoGC(tf,"Attack").label
    table.insert( t,self.m_Attack )
    self.m_Sta=LuaGameObject.GetChildNoGC(tf,"Sta").label
    table.insert( t,self.m_Sta )
    self.m_HpFull=LuaGameObject.GetChildNoGC(tf,"HpFull").label
    table.insert( t,self.m_HpFull )
    self.m_MpFull=LuaGameObject.GetChildNoGC(tf,"MpFull").label
    table.insert( t,self.m_MpFull )
    
    self.m_Str=LuaGameObject.GetChildNoGC(tf,"Str").label
    table.insert( t,self.m_Str )
    self.m_HitType=LuaGameObject.GetChildNoGC(tf,"HitType").label
    self.m_Hit=LuaGameObject.GetChildNoGC(tf,"Hit").label
    table.insert( t,self.m_Hit )
    self.m_FatalType=LuaGameObject.GetChildNoGC(tf,"FatalType").label
    self.m_Fatal=LuaGameObject.GetChildNoGC(tf,"Fatal").label
    table.insert( t,self.m_Fatal )
    
    self.m_Int=LuaGameObject.GetChildNoGC(tf,"Int").label
    table.insert( t,self.m_Int )
    self.m_pDef=LuaGameObject.GetChildNoGC(tf,"pDef").label
    table.insert( t,self.m_pDef )
    self.m_mDef=LuaGameObject.GetChildNoGC(tf,"mDef").label
    table.insert( t,self.m_mDef )
    
    self.m_Agi=LuaGameObject.GetChildNoGC(tf,"Agi").label
    table.insert( t,self.m_Agi )
    self.m_pMiss=LuaGameObject.GetChildNoGC(tf,"pMiss").label
    table.insert( t,self.m_pMiss )
    self.m_mMiss=LuaGameObject.GetChildNoGC(tf,"mMiss").label
    table.insert( t,self.m_mMiss )
    
    self.m_Cor.text=tostring(math.floor(self.m_FightProp:GetParam(EPlayerFightProp.Cor)))
    if physical then
        self.m_AttackType.text=LocalString.GetString("物理攻击")
        self.m_Attack.text=SafeStringFormat3("%s-%s",
            math.floor(self.m_FightProp:GetParam(EPlayerFightProp.pAttMin)),
            math.floor(self.m_FightProp:GetParam(EPlayerFightProp.pAttMax)))
        self.m_HitType.text=LocalString.GetString("物理命中")
        self.m_Hit.text=tostring(math.floor(self.m_FightProp:GetParam(EPlayerFightProp.pHit)))
        self.m_FatalType.text=LocalString.GetString("物理致命")
        self.m_Fatal.text=tostring(math.floor(self.m_FightProp:GetParam(EPlayerFightProp.pFatal)))
    else
        self.m_AttackType.text=LocalString.GetString("法术攻击")
        self.m_Attack.text=SafeStringFormat3("%s-%s",
            math.floor(self.m_FightProp:GetParam(EPlayerFightProp.mAttMin)),
            math.floor(self.m_FightProp:GetParam(EPlayerFightProp.mAttMax)))
        self.m_HitType.text=LocalString.GetString("法术命中")
        self.m_Hit.text=tostring(math.floor(self.m_FightProp:GetParam(EPlayerFightProp.mHit)))
        self.m_FatalType.text=LocalString.GetString("法术致命")
        self.m_Fatal.text=tostring(math.floor(self.m_FightProp:GetParam(EPlayerFightProp.mFatal)))
    end
    self.m_Sta.text=tostring(math.floor(self.m_FightProp:GetParam(EPlayerFightProp.Sta)))
    self.m_HpFull.text=tostring(math.floor(self.m_FightProp:GetParam(EPlayerFightProp.HpFull)))
    self.m_MpFull.text=tostring(math.floor(self.m_FightProp:GetParam(EPlayerFightProp.MpFull)))

    self.m_Str.text=tostring(math.floor(self.m_FightProp:GetParam(EPlayerFightProp.Str)))
    
    self.m_Int.text=tostring(math.floor(self.m_FightProp:GetParam(EPlayerFightProp.Int)))
    self.m_pDef.text=tostring(math.floor(self.m_FightProp:GetParam(EPlayerFightProp.pDef)))
    self.m_mDef.text=tostring(math.floor(self.m_FightProp:GetParam(EPlayerFightProp.mDef)))
    
    self.m_Agi.text=tostring(math.floor(self.m_FightProp:GetParam(EPlayerFightProp.Agi)))
    self.m_pMiss.text=tostring(math.floor(self.m_FightProp:GetParam(EPlayerFightProp.pMiss)))
    self.m_mMiss.text=tostring(math.floor(self.m_FightProp:GetParam(EPlayerFightProp.mMiss)))
    
    local t2={}
    local onClick=function(go)
        local msg=""
        local physical = CClientMainPlayer.Inst.IsPhysicalAttackType
        if go==t2[1] then--cor
            msg=MessageMgr.Inst:FormatMessage("FightProp_Tips_Cor",{})
        elseif go==t2[2] then--attack
            if physical then
                msg=MessageMgr.Inst:FormatMessage("FightProp_Tips_pAtt_Message",{})
            else
                msg=MessageMgr.Inst:FormatMessage("FightProp_Tips_mAtt_Message",{})
            end
        elseif go==t2[3] then--sta
            msg=MessageMgr.Inst:FormatMessage("FightProp_Tips_Sta",{})
        elseif go==t2[4] then--hpfull
            msg=MessageMgr.Inst:FormatMessage("FightProp_Tips_HpFull",{})
        elseif go==t2[5] then--mpfull
            msg=MessageMgr.Inst:FormatMessage("FightProp_Tips_MpFull",{})
        elseif go==t2[6] then--str
            msg=MessageMgr.Inst:FormatMessage("FightProp_Tips_Str",{})
        elseif go==t2[7] then--hit
            if physical then
                msg=MessageMgr.Inst:FormatMessage("FightProp_Tips_pHit",{})
            else
                msg=MessageMgr.Inst:FormatMessage("FightProp_Tips_mHit",{})
            end
        elseif go==t2[8] then--fatal
            if physical then
                msg=MessageMgr.Inst:FormatMessage("FightProp_Tips_pFatal",{})
            else
                msg=MessageMgr.Inst:FormatMessage("FightProp_Tips_mFatal",{})
            end
        elseif go==t2[9] then--int
            msg=MessageMgr.Inst:FormatMessage("FightProp_Tips_Int",{})
        elseif go==t2[10] then--pDef
            msg=MessageMgr.Inst:FormatMessage("FightProp_Tips_pDef",{})
        elseif go==t2[11] then--mDef
            msg=MessageMgr.Inst:FormatMessage("FightProp_Tips_mDef",{})
        elseif go==t2[12] then--agi
            msg=MessageMgr.Inst:FormatMessage("FightProp_Tips_Agi",{})
        elseif go==t2[13] then--pMiss
            msg=MessageMgr.Inst:FormatMessage("FightProp_Tips_pMiss",{})
        elseif go==t2[14] then--mMiss
            msg=MessageMgr.Inst:FormatMessage("FightProp_Tips_mMiss",{})
        end
        CTooltip.Show(msg, go.transform, CTooltip.AlignType.Top);
    end
    local color=Color(172/255,248/255,1,1)
    if self.m_IsLeft then
        color=Color(246/255,193/255,1,1)
    end
    for i,v in ipairs(t) do
        local go=v.transform:GetChild(0).gameObject
        local label=LuaGameObject.GetLuaGameObjectNoGC(go.transform).label
        label.color=color
        table.insert( t2,go )
        CommonDefs.AddOnClickListener(go,DelegateFactory.Action_GameObject(onClick),false)
    end
end

return CLuaXianFanCompareSimplePropView