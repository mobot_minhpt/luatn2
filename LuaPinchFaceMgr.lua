local CFacialMgr = import "L10.Game.CFacialMgr"
local CSharpResourceLoader = import "L10.Game.CSharpResourceLoader"
local EnumGender = import "L10.Game.EnumGender"
local CPropertySkill = import "L10.Game.CPropertySkill"
local CPropertyAppearance = import "L10.Game.CPropertyAppearance"
local EWeaponVisibleReason = import "L10.Engine.EWeaponVisibleReason"
local EHideKuiLeiStatus = import "L10.Engine.EHideKuiLeiStatus"
local EmDisplayType = import "L10.Engine.CFacialAsset+EmDisplayType"
local ColorExt = import "L10.Game.ColorExt"
local CCustomFacialData = import "L10.Game.CCustomFacialData"
local CPinchFaceCinemachineCtrlMgr = import "L10.Game.CPinchFaceCinemachineCtrlMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CMaterialEffect = import "L10.Game.CMaterialEffect"
local CClientObject = import "L10.Game.CClientObject"
local AvatarHelper = import "L10.Game.AvatarHelper"
local CPlayerDataMgr = import "L10.Game.CPlayerDataMgr"
local CObjectFX = import "L10.Game.CObjectFX"
local CFX = import "L10.Game.CEffectMgr+CFX"
local Quaternion = import "UnityEngine.Quaternion"
local LayerDefine = import "L10.Engine.LayerDefine"
local EnumClass = import "L10.Game.EnumClass"
local CAnimtorEffectBase = import "L10.Game.CAnimtorEffectBase"
local VideoRecordMgr = import "L10.Game.VideoRecordMgr"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local Screen = import "UnityEngine.Screen"
local CFuxiFaceDNAMgr = import "L10.Game.CFuxiFaceDNAMgr"
local CPinchFaceHubMgr = import "L10.Game.CPinchFaceHubMgr"
local DelegateFactory  = import "DelegateFactory"
local ImageConversion = import "UnityEngine.ImageConversion"
local LoadPicMgr = import "L10.Game.LoadPicMgr"
local Texture2D = import "UnityEngine.Texture2D"
local TextureFormat = import "UnityEngine.TextureFormat"
local Rect = import "UnityEngine.Rect"
local TextureWrapMode = import "UnityEngine.TextureWrapMode"
local RenderTexture = import "UnityEngine.RenderTexture"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local GameObject = import "UnityEngine.GameObject"
local CLoadingWnd = import "L10.UI.CLoadingWnd"
local CRenderObject = import "L10.Engine.CRenderObject"
local CMainCamera = import "L10.Engine.CMainCamera"
local NativeTools = import "L10.Engine.NativeTools"
local Main = import "L10.Engine.Main"
local SdkU3d = import "NtUniSdk.Unity3d.SdkU3d"
local CLoginMgr = import "L10.Game.CLoginMgr"
local EasyCodeScanner = import "EasyCodeScanner"
local CResourceMgr = import "L10.Engine.CResourceMgr"
local ShaderEx = import "ShaderEx"
local EClearOption = import "L10.Engine.EClearOption"

LuaPinchFaceMgr = {}

LuaPinchFaceMgr.m_RO = nil
LuaPinchFaceMgr.m_CurEnumClass = nil
LuaPinchFaceMgr.m_CurEnumGender = nil
LuaPinchFaceMgr.m_IsInCreateMode = true     --创角流程的捏脸界面
LuaPinchFaceMgr.m_IsInModifyMode = false    --老玩家捏脸流程的捏脸界面
LuaPinchFaceMgr.m_IsOffline = false         --云捏脸
LuaPinchFaceMgr.m_HeadId = 0                --发色index
LuaPinchFaceMgr.m_HairId = 0                --发型
LuaPinchFaceMgr.m_IsFaceChanged = false
LuaPinchFaceMgr.m_PreselectionIndex = 0     --预设仙凡
LuaPinchFaceMgr.m_IsModifyXianSheng = false --老玩家捏脸流程修改仙身捏脸
LuaPinchFaceMgr.m_IsUseIKLookAt = false     --是否注视相机
LuaPinchFaceMgr.m_CacheCustomFacialData = nil
LuaPinchFaceMgr.m_EnableHubQRCode = true
LuaPinchFaceMgr.m_PreselectionViewTabIndex = 0 --预设界面tab索引
--@region ShowWnd

function LuaPinchFaceMgr:ShowWndWithCache()
    if self.m_ShowWndTick then return end
    self.m_IsUseIKLookAt = false
    self.m_CacheCustomFacialData = nil
    self:OnDestroy()
    self.m_IsInCreateMode = true
    self.m_IsInModifyMode = false
    self.m_HeadId = 0
    local data = self:LoadOfflineDataCache()
    if data then
        local msg = g_MessageMgr:FormatMessage("LoadFacialDataCacheConfirm")
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
            self.m_CurEnumClass = data.Job
            self.m_CurEnumGender = data.Gender
            self.m_HeadId = data.HairColorIndex
            self.m_HairId = data.HairId
            local hairId = PlayerPrefs.GetInt("PinchFaceHairId")
            if hairId then
                self.m_HairId = hairId
            end
            local headId = PlayerPrefs.GetInt("PinchFaceHeadId")
            if headId then
                self.m_HeadId = headId
            end
            if self.m_HairId == 9 then
                self.m_HairId = self:GetPreselectionHairId(self.m_PreselectionIndex)
            end
            local bgColorId = PlayerPrefs.GetInt("PinchFaceBgColorId")
            if bgColorId then
                self.m_BgColorData = PinchFace_BgColor.GetData(bgColorId)
            end
            self:ClearPartDatasPaths(data.PartDatas)
            local customFacialData = CFacialMgr.GetCustomFacialData(data.FaceDnaData, data.PartDatas)
            self.m_CacheCustomFacialData = customFacialData
            self:CacheWndData()
            if data.FaceId > 0 then
                self.m_InitialPreselectionIndex = data.FaceId - 1
            end
            local fashionId = PlayerPrefs.GetInt("PinchFaceFashionId")
            self:SetFashionData(fashionId)
            self.m_InitialPreselectionX = PlayerPrefs.GetFloat("PinchFacePreselectionX", 0)
            self.m_InitialPreselectionY = PlayerPrefs.GetFloat("PinchFacePreselectionY", 0)
            CLoadingWnd.Inst:ShowPinchFaceFx(1, nil)	
            RegisterTickOnce(function ()
                CUIManager.CloseUI(CUIResources.BeforeEnterDengZhongShiJieWnd)
                CUIManager.CloseUI(CUIResources.CharacterCreation2023Wnd)
                CMainCamera.Inst:SetCameraEnableStatus(true, "Enter_ChuangJue", false)
                self:CancelShowWndTick()
                CUIManager.CloseUI(CLuaUIResources.PinchFaceWnd)
                CUIManager.ShowUI(CLuaUIResources.PinchFaceWnd)
            end, 1000)
        end), DelegateFactory.Action(function()
            self:ClearOfflineData()
        end), nil, nil, false)
        return
    end
end

LuaPinchFaceMgr.m_ShowWndTick = nil
function LuaPinchFaceMgr:CancelShowWndTick()
    if self.m_ShowWndTick then
        UnRegisterTick(self.m_ShowWndTick)
        self.m_ShowWndTick = nil
    end
end

function LuaPinchFaceMgr:ShowCreateWnd(enumClass, enumGender)
    if self.m_ShowWndTick then return end
    self.m_CurEnumClass = enumClass
    self.m_CurEnumGender = enumGender
    self.m_IsUseIKLookAt = false
    self.m_CacheCustomFacialData = nil
    if enumClass and enumGender then
        self:OnDestroy()
        self:CacheWndData()
        self.m_IsInCreateMode = true
        self.m_IsInModifyMode = false
        self.m_HeadId = 0
        CLoadingWnd.Inst:ShowPinchFaceFx(1, nil)	
        self.m_ShowWndTick = RegisterTickOnce(function ()
            CUIManager.CloseUI(CUIResources.CharacterCreation2023Wnd)
            self:CancelShowWndTick()
            CUIManager.CloseUI(CLuaUIResources.PinchFaceWnd)
            CUIManager.ShowUI(CLuaUIResources.PinchFaceWnd)
            if LuaCloudGameFaceMgr:IsCloudGameFace() then LuaCloudGameFaceMgr:CacheFacialData() end -- 切换职业时云游戏缓存一下
        end, 1000)
    end
end

LuaPinchFaceMgr.m_IsReadyShowModifyWnd = false
function LuaPinchFaceMgr:ShowModifyWnd(enumClass, enumGender, isModifyXianSheng)
    if CUIManager.IsLoaded(CLuaUIResources.PinchFaceWnd) or self.m_IsReadyShowModifyWnd then
        return
    end
    self.m_CurEnumClass = enumClass
    self.m_CurEnumGender = enumGender
    self.m_IsUseIKLookAt = false
    if enumClass and enumGender then
        self:OnDestroy()
        self.m_IsInModifyMode = true
        self.m_IsInCreateMode = false
        self.m_IsModifyXianSheng = isModifyXianSheng
        self.m_HeadId = 0
        self:CacheWndData()
        local data = self:LoadOfflineDataCache()
        if data and data.Gender == self.m_CurEnumGender and data.Job == self.m_CurEnumClass then
            local msg = g_MessageMgr:FormatMessage("LoadFacialDataCacheConfirm")
            self.m_IsReadyShowModifyWnd = true
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
                self.m_CurEnumClass = data.Job
                self.m_CurEnumGender = data.Gender
                self.m_HeadId = data.HairColorIndex
                self.m_HairId = data.HairId
                local hairId = PlayerPrefs.GetInt("PinchFaceHairId2")
                if hairId then
                    self.m_HairId = hairId
                end
                local headId = PlayerPrefs.GetInt("PinchFaceHeadId2")
                if headId then
                    self.m_HeadId = headId
                end
                if self.m_HairId == 9 then
                    self.m_HairId = self:GetPreselectionHairId(self.m_PreselectionIndex)
                end
                local bgColorId = PlayerPrefs.GetInt("PinchFaceBgColorId2")
                if bgColorId then
                    self.m_BgColorData = PinchFace_BgColor.GetData(bgColorId)
                end
                self:ClearPartDatasPaths(data.PartDatas)
                local customFacialData = CFacialMgr.GetCustomFacialData(data.FaceDnaData, data.PartDatas)
                self.m_CacheCustomFacialData = customFacialData
                self:CacheWndData()
                if data.FaceId > 0 then
                    self.m_InitialPreselectionIndex =  data.FaceId - 1
                end
                local fashionId = PlayerPrefs.GetInt("PinchFaceFashionId2")
                if fashionId then
                    for i = 1, #self.m_FashionDataArr do
                        local data = self.m_FashionDataArr[i]
                        if data and data.data and data.data.ID == fashionId then
                            self.m_FashionData = data
                            break
                        end
                    end
                end
                self.m_InitialPreselectionX = PlayerPrefs.GetFloat("PinchFacePreselectionX2", 0)
                self.m_InitialPreselectionY = PlayerPrefs.GetFloat("PinchFacePreselectionY2", 0)
                CUIManager.ShowUI(CLuaUIResources.PinchFaceWnd)     
                self.m_IsReadyShowModifyWnd = false
            end), DelegateFactory.Action(function()
                self:ClearOfflineData()
                self.m_IsReadyShowModifyWnd = false
                Gac2Gas.QueryCustomFacialData(isModifyXianSheng and 1 or 0, EnumToInt(enumGender))
            end), nil, nil, false)
            return
        end
        Gac2Gas.QueryCustomFacialData(isModifyXianSheng and 1 or 0, EnumToInt(enumGender))
    end
end

LuaPinchFaceMgr.m_HasCustomFacialData = false
LuaPinchFaceMgr.m_UseCurFacialData = false
function LuaPinchFaceMgr:QueryCustomFacialDataResult(xianfanStatus, array_U)
    if nil == array_U then return end
    if CUIManager.IsLoaded(CLuaUIResources.PinchFaceWnd) then
        return
    end
    local customFacialData = CCustomFacialData()
    customFacialData:LoadFromString(array_U)
    local isNullData = true
    for i = 0, customFacialData.FacialData.Length - 1 do
        if customFacialData.FacialData[i] ~= 0 then
            isNullData = false
            break
        end
    end
    if not isNullData then
        self.m_CacheCustomFacialData = customFacialData.FacialData
        self.m_HairId = customFacialData.HairId
        self.m_UseCurFacialData = true
        self.m_HasCustomFacialData = true
    end
    if customFacialData.FaceId > 0 then
        self.m_InitialPreselectionIndex = customFacialData.FaceId - 1
    end
    if CClientMainPlayer.Inst then
        local appearanceProp = CClientMainPlayer.Inst.AppearanceProp
        self.m_HeadId =  appearanceProp.HeadId
        self.m_AdvancedHairColorIndex = appearanceProp.AdvancedHairColorId
        self.m_SkinColorIndex = appearanceProp.SkinColor
        if not cs_string.IsNullOrEmpty(appearanceProp.AdvancedHairColor) then
            RanFaJi_RanFaJi.Foreach(function (key, data)
                if data.ColorRGB == appearanceProp.AdvancedHairColor then
                    self.m_AdvancedHairColorIndex = key
                end
            end)
        end
    end
    CUIManager.ShowUI(CLuaUIResources.PinchFaceWnd)
end

function LuaPinchFaceMgr:OpenPinchFaceModifyWnd()
    if not CClientMainPlayer.Inst then return end
    if CClientMainPlayer.Inst.HasFeiSheng then
        CUIManager.ShowUI(CLuaUIResources.PinchFaceModifyWnd)
        return
    end
    local gender = CClientMainPlayer.Inst.AppearanceGender
    gender = gender == EnumGender.Monster and CClientMainPlayer.Inst.Gender or gender
    self:ShowModifyWnd(CClientMainPlayer.Inst.Class, gender, false)
end

LuaPinchFaceMgr.m_CaptureMovieFilePath = nil
function LuaPinchFaceMgr:ShowShareWnd()
    CUIManager.ShowUI(CLuaUIResources.PinchFaceShareWnd)
end

LuaPinchFaceMgr.m_ReportPlayerId = nil
LuaPinchFaceMgr.m_ReportTitle = nil
LuaPinchFaceMgr.m_ReportShareId = nil
function LuaPinchFaceMgr:ShowReportWnd(playerId, title, shareId)
    if playerId and title and shareId then
        self.m_ReportPlayerId = playerId
        self.m_ReportTitle = title
        self.m_ReportShareId = shareId
        CUIManager.ShowUI(CLuaUIResources.PinchFaceHubReportWnd)
    end
end

--@endregion ShowWnd

LuaPinchFaceMgr.m_InitialPreselectionIndex = 0
function LuaPinchFaceMgr:CacheWndData()
    self:LoadPreselectionModelAvatarData()
    self:CacheAllFacialSlider2Data()
    self:CacheFacialStylePath2MainTexColor()
    self:CacheFacialEyeKey()
    self:InitHairColorDataArr()
    self:InitFashionDataArr()
    self.m_InitialPreselectionIndex = self.m_IsModifyXianSheng and 1 or 0
end

function LuaPinchFaceMgr:SetFashionData(fashionId)
    if fashionId then
        for i = 1, #self.m_FashionDataArr do
            local data = self.m_FashionDataArr[i]
            if data and data.data and data.data.ID == fashionId then
                self.m_FashionData = data
                break
            end
        end
    end
end

function LuaPinchFaceMgr:OnDestroy()
    if self.m_RO then
        self.m_RO:Destroy()
        self.m_RO = nil
    end
    self.m_AllFacialSlider2Data = {}
    self.m_PreselectionModelAvatorDataArr = {}
    self.m_FacialStylePath2MainTexColor = {}
    self.m_PreselectionPartDatas = {}
    self:InitCommandMode()
    self.m_Capture = nil
    self.m_CaptureMovieFilePath = nil
    self.m_IsEnableRotateRole = true
    self.m_CacheCustomFacialData = nil
    self.m_SkinColorIndex = 0
    self.m_AdvancedHairColorIndex = 0
    self.m_BgColorData = nil
    self.m_EnvironmentFxData = nil
    self.m_FashionData = nil
    self.m_EquipmentData = nil
    self.m_IsPlayerOperating = false
    self.m_PreselectionViewTabIndex = 0
    self.m_CurPresetData = nil
    self.m_IsEnableIK = true
    self.m_LastFaceTabDefaultAngle = 0
    self.m_IsEnableBlurTex = false
    self.m_CurFacialData = nil
    self.m_PreselectionX = 0
    self.m_PreselectionY = 0
    self.m_IsHairChanged = false
    self.m_IsFaceChanged = false
    self.m_UseCurFacialData = false
    self.m_HasCustomFacialData = false
    self.m_IsModifyXianSheng = false
    self.m_InitialPreselectionX = 0
    self.m_InitialPreselectionY = 0
    self:CancelMovieCaptureTick()
    self:KillRotateRoleTweener()
end

function LuaPinchFaceMgr:IsFacialDataChanged()
    return self.m_IsFaceChanged or  self.m_IsHairChanged or self.m_PreselectionX ~= 0 or self.m_PreselectionY ~= 0 
end

LuaPinchFaceMgr.m_IsPlayerOperating = false
function LuaPinchFaceMgr:IsPlayerOperating()
    return self.m_IsPlayerOperating
end

function LuaPinchFaceMgr:GetInitializationData()
    if self.m_CurEnumClass and self.m_CurEnumGender then
        local id = EnumToInt(self.m_CurEnumClass) * 100 + EnumToInt(self.m_CurEnumGender)
        local initializationData = Initialization_Init.GetData(id)
        return initializationData
    end
end

function LuaPinchFaceMgr:GetCurFaceSkinData()
    if self.m_CurEnumClass and self.m_CurEnumGender then
        local id = EnumToInt(self.m_CurEnumClass) * 100 + EnumToInt(self.m_CurEnumGender)
        local faceSkinData = PinchFace_HeadSkin.GetData(id)
        return faceSkinData
    end
end

function LuaPinchFaceMgr:IsHasFeiSheng()
    return self.m_IsInModifyMode and self.m_IsModifyXianSheng
end

function LuaPinchFaceMgr:IsLoadedAllPreselectionModelAvatorData()
    return self.m_NumOfCurPreselectionModelAvatorData == 4
end

LuaPinchFaceMgr.m_LeftEyeFacialTabKey = 0
LuaPinchFaceMgr.m_RightEyeFacialTabKey = 0
function LuaPinchFaceMgr:CacheFacialEyeKey()
    if self.m_LeftEyeFacialTabKey ~= 0 or self.m_RightEyeFacialTabKey ~= 0 then return end
    PinchFace_FacialTabBarData.Foreach(function(k, v)
        if string.find(v.SliderName, LocalString.GetString("左瞳孔")) then
            self.m_LeftEyeFacialTabKey = k
        elseif string.find(v.SliderName, LocalString.GetString("右瞳孔")) then
            self.m_RightEyeFacialTabKey = k
        end
    end)
end

LuaPinchFaceMgr.m_FacialStylePath2MainTexColor = {}
function LuaPinchFaceMgr:CacheFacialStylePath2MainTexColor()
    self.m_FacialStylePath2MainTexColor = {}
    PinchFace_FacialStyle.Foreach(function(k, v)
        local path = "Assets/Res/Character/Player3/FacialStyle/" .. v.Path
        self.m_FacialStylePath2MainTexColor[string.lower(path)] = v.MainTexColor
    end)
end

LuaPinchFaceMgr.m_AllFacialSlider2Data = {}
function LuaPinchFaceMgr:CacheAllFacialSlider2Data()
    self.m_AllFacialSlider2Data = {}
    if self.m_CurEnumClass and self.m_CurEnumGender then
        PinchFace_FacialSlider2.Foreach(function(k, v)
            if v.GenderLimit == 0 or ((EnumToInt(self.m_CurEnumGender) + 1) == v.GenderLimit) then
                if v.RaceLimit == nil or (CommonDefs.HashSetContains(v.RaceLimit, typeof(UInt32), EnumToInt(self.m_CurEnumClass))) then
                    self.m_AllFacialSlider2Data[v.FacialTabBarDataID * 100 + v.SliderIndex] = v.RangeVal
                end
            end
        end)
    end
end

LuaPinchFaceMgr.m_PreselectionPartDatas = {}
function LuaPinchFaceMgr:CachePreselectionPartDatas()
    self.m_PreselectionPartDatas = {}
    local partDatas = self.m_RO.FacialPartData
    if not partDatas then return end
    for i = 0, partDatas.Length - 1 do
        local arr = self:GetFacialPartArrData(i)
        self.m_PreselectionPartDatas[i] = arr
    end
end

LuaPinchFaceMgr.m_FashionDataArr = {}
LuaPinchFaceMgr.m_ShenBingFashionIndex = 1
function LuaPinchFaceMgr:InitFashionDataArr()
    local list = {}
    PinchFace_PreviewFashion.ForeachKey(function(key)
        local data = PinchFace_PreviewFashion.GetData(key)
        if data.GenderLimit == 0 or (self.m_CurEnumGender == EnumGender.Male and data.GenderLimit == 1) or (self.m_CurEnumGender == EnumGender.Female and data.GenderLimit == 2) then
            table.insert(list, { data = data, index = #list })
            if string.find(data.Description, LocalString.GetString("神兵")) then
                self.m_ShenBingFashionIndex = #list
            end
        end
    end)
    self.m_FashionDataArr = list
end

LuaPinchFaceMgr.m_HairColorDataArr = {}
function LuaPinchFaceMgr:InitHairColorDataArr()
    local list = {}
    table.insert(list, { isDefault = true })
    RanFaJi_RanFaJi.ForeachKey(function(key)
        table.insert(list, RanFaJi_RanFaJi.GetData(key))
    end)
    self.m_HairColorDataArr = list
end

function LuaPinchFaceMgr:GetFacialSliderRangeVal(tabBarKey, styleId, sliderId)
    local min, max = 0, 1
    local range = self.m_AllFacialSlider2Data[tabBarKey * 100 + sliderId]
    if range then
        min, max = range[0], range[1]
    end
    if styleId == 0 then
        local presetPath = self:GetCurPresetDataStylePath(tabBarKey)
        if presetPath then
            presetPath = string.lower(presetPath)
            local subPath = string.lower("Assets/Res/Character/Player3/FacialStyle/")
            presetPath = string.gsub(presetPath, subPath,"")
            local reflectionStyleData = PinchFace_FacialStyle.GetDataBySubKey("Path",presetPath)
            if reflectionStyleData then
                styleId = reflectionStyleData.Id
            end
        end
    end
    local id = styleId * 100 + sliderId
    local sliderData = PinchFace_FacialSlider.GetData(id)
    if sliderData and sliderData.FacialStyle then
        min, max = sliderData.FacialStyle[0], sliderData.FacialStyle[1]
    end
    return min, max
end

function LuaPinchFaceMgr:GetFaceSliderValRange(sliderData)
    local rmin, rmax = 0, 1
    if sliderData and sliderData.SliderValRange then
        if sliderData.SliderValRange.Length == 1 and sliderData.SliderValRange[0].Length == 2 then
            rmin, rmax = sliderData.SliderValRange[0][0], sliderData.SliderValRange[0][1]
        elseif sliderData.SliderValRange.Length > 1 then
            for i = 0, sliderData.SliderValRange.Length - 1 do
                if sliderData.SliderValRange[i].Length == 3 then
                    local g, _rmin, _rmax = sliderData.SliderValRange[i][0], sliderData.SliderValRange[i][1],
                        sliderData.SliderValRange[i][2]
                    if g == 1 and self.m_CurEnumGender == EnumGender.Male then
                        rmin, rmax = _rmin, _rmax
                        break
                    elseif g == 2 and self.m_CurEnumGender == EnumGender.Female then
                        rmin, rmax = _rmin, _rmax
                        break
                    end
                end
            end
        end
    end
    local faceValMin, faceValMax = -1, 1
    local min, max = math.lerp(faceValMin, faceValMax, rmin), math.lerp(faceValMin, faceValMax, rmax)
    return min, max
end

LuaPinchFaceMgr.m_PreselectionModelAvatorDataArr = {}
LuaPinchFaceMgr.m_NumOfCurPreselectionModelAvatorData = 0
function LuaPinchFaceMgr:LoadPreselectionModelAvatarData()
    if self.m_CurEnumClass and self.m_CurEnumGender then
        local initializationData = self:GetInitializationData()
        if initializationData then
            self.m_PreselectionModelAvatorDataArr = {}
            self.m_NumOfCurPreselectionModelAvatorData = 0
            for i = 1, 4 do
                local path = SafeStringFormat3("Assets/Res/Character/Player3/Prefab/%s/%s0%d.ms.asset",
                    initializationData.ResName, initializationData.ResName, i)
                CSharpResourceLoader.Inst:LoadScriptableObject(path,
                    DelegateFactory.Action_ScriptableObject(function(so)
                        self.m_PreselectionModelAvatorDataArr[i] = so
                        if so then
                            self.m_NumOfCurPreselectionModelAvatorData = self.m_NumOfCurPreselectionModelAvatorData + 1
                        end
                        if self.m_NumOfCurPreselectionModelAvatorData == 4 then
                            g_ScriptEvent:BroadcastInLua("OnLoadedAllPreselectionModelAvatarData")
                        end
                    end))
            end
        end
    end
end

LuaPinchFaceMgr.m_PreselectionX = 0
LuaPinchFaceMgr.m_PreselectionY = 0
function LuaPinchFaceMgr:SetPreselection(x, y)
    self.m_PreselectionX = x
    self.m_PreselectionY = y
    if not self.m_RO or not self:IsLoadedAllPreselectionModelAvatorData() then return end
    local absx = math.abs(x)
    local absy = math.abs(y)
    local py = (y > 0) and self.m_PreselectionModelAvatorDataArr[1] or self.m_PreselectionModelAvatorDataArr[2]
    local px = (x < 0) and self.m_PreselectionModelAvatorDataArr[3] or self.m_PreselectionModelAvatorDataArr[4]
    self:StopBlendShapeTemporarily()
    CFacialMgr.LerpFacial(self.m_RO, px, absx, py, absy)
    local customFacialData = CFacialMgr.GetCustomFacialData(self.m_RO.FaceDnaData, self.m_RO.FacialPartData)
    self:OnCurFacialDataUpdate(customFacialData)
end

function LuaPinchFaceMgr:SetCharacterPreselectionIndex(index)
    if not self.m_RO then return end
    self.m_RO.FaceId = index + 1
    if LuaPinchFaceMgr.m_IsInCreateMode then
        self:ClearOfflineData()
    end
    self.m_RO:ApplyFacialData(nil, nil)
    self.m_CurFacialData = nil
    self:SetPreselection(self.m_PreselectionX, self.m_PreselectionY)
    CClientMainPlayer.ChangeSunBurnSkinColor(self.m_RO, self.m_SkinColorIndex)
    if self.m_PreselectionIndex ~= index then
        self.m_PreselectionIndex = index
        self:CachePresetFacialStylePaths()
    end
end

function LuaPinchFaceMgr:SetCharacterPreselectionHair(index)
    local hairId = self:GetPreselectionHairId(index)
    self:OnSelectHair(hairId)
end

function LuaPinchFaceMgr:GetPreselectionHairId(preselectionIndex)
    local hairId = 1
    if preselectionIndex == 0 then
        hairId = 1
    else
        if not self:IsHasFeiSheng() then
            local curFaceSkinData = self:GetCurFaceSkinData()
            if curFaceSkinData then
                hairId = curFaceSkinData.PinchFaceHairstyle02
            end
        else
            hairId = 2
        end
    end
    return hairId
end

function LuaPinchFaceMgr:GetDefaultHairId()
    local hairId = 1
    if self.m_PreselectionIndex == 1 then
        local hasFeisheng = self:IsHasFeiSheng()
        hairId = hasFeisheng and 2 or hairId
    end
    return hairId
end

LuaPinchFaceMgr.m_CurPresetData = nil
LuaPinchFaceMgr.m_CurPresetDataStylePathsCache = {}
function LuaPinchFaceMgr:CachePresetFacialStylePaths()
    if self.m_RO then
        local curPresetData = self.m_RO:GetCurPresetData()
        if self.m_CurPresetData ~= curPresetData then
            self.m_CurPresetData = curPresetData
            self.m_CurPresetDataStylePathsCache = {}
            if curPresetData then
                for i = 0, curPresetData.PartDatas.Length - 1 do
                    local stylePath = curPresetData.PartDatas[i].Path
                    if not System.String.IsNullOrEmpty(stylePath) then
                        local path = stylePath
                        self.m_CurPresetDataStylePathsCache[i] = string.lower(path)
                    end
                end
            end
            g_ScriptEvent:BroadcastInLua("OnCachePresetFacialStylePaths")
        end
    end
end

function LuaPinchFaceMgr:GetCurPresetDataStylePath(k)
    local cachePresetPath = self.m_CurPresetDataStylePathsCache[k]
    return cachePresetPath
end

function LuaPinchFaceMgr:CheckFacialStyleIsUseful(k, id)
    local styleData = PinchFace_FacialStyle.GetData(id)
    if styleData and self.m_CurEnumClass and self.m_CurEnumGender then
        local canUseStyle = true
        local class = EnumToInt(self.m_CurEnumClass)
        if styleData.Race and not CommonDefs.HashSetContains(styleData.Race, typeof(UInt32), class) then
            canUseStyle = false
        end
        local gender = self.m_CurEnumGender == EnumGender.Male and 1 or 0
        if styleData.GenderLimit and not CommonDefs.HashSetContains(styleData.GenderLimit, typeof(UInt32), gender) then
            canUseStyle = false
        end
        local cachePresetPath = self:GetCurPresetDataStylePath(k)
        if cachePresetPath and string.find(cachePresetPath, string.lower(styleData.Path)) then
            canUseStyle = false
        end
        if System.String.IsNullOrEmpty(styleData.Icon) then
            canUseStyle = false
        end
        if self.m_IsInCreateMode and styleData.SN and styleData.SN > 0 then
            canUseStyle = false
        end
        return canUseStyle
    end
    return false
end

LuaPinchFaceMgr.m_UIFacialFxKey = "UIFacialEffect"
function LuaPinchFaceMgr:NewShowUIFacialEffectMat(data)
    local initializationData = self:GetInitializationData()
    if self.m_RO and initializationData then
        local facialEffectData = PinchFace_Setting.GetData().UIFacialEffect
        local fxId = 0
        local isPartFx = data and data.UIMatMaskValue > 0
        local stdModelName = initializationData.StdModelName
        for i = 0, facialEffectData.Length - 1 do
            if stdModelName == facialEffectData[i][0] then
                fxId = isPartFx and tonumber(facialEffectData[i][2]) or tonumber(facialEffectData[i][1])
                break
            end
        end
        self:RemoveUIFacialEffectMat()
        if fxId == 0 or (data and data.UIMatMaskValue <= 0) then
            return
        end
        local fx = CFX()
        fx.Id = fxId
        fx.Visible = true
        local info = Fx_FX.GetData(fxId)
        CSharpResourceLoader.Inst:LoadGameObject(info.Prefab, DelegateFactory.Action_GameObject(function(obj)
            if obj then
                local go = GameObject.Instantiate(obj, Vector3.zero, Quaternion.identity)
                go.transform.parent = self.m_RO.transform
                go.transform.localPosition = Vector3.zero
                go.transform.localScale = Vector3.one
                local cfx = go:GetComponent(typeof(CObjectFX))
                local effect = CommonDefs.GetComponentInChildren_GameObject_Type(go, typeof(CMaterialEffect))
                if effect and effect.m_Material and isPartFx then
                    effect.m_Material:SetFloat("_MaskValue", data.UIMatMaskValue)
                    effect.m_Material:SetFloat("_MaskRange", data.MaskRange)
                end
                cfx:Init(fx, self.m_RO, 0, 1, 1, nil, false, Vector3.zero, Vector3.zero, nil, 0)
                NGUITools.SetLayer(go.gameObject, LayerDefine.Effect_3D)
                fx.m_FX = cfx
            end
        end))
        self.m_RO:AddFX(self.m_UIFacialFxKey, fx)
    end
end

function LuaPinchFaceMgr:RemoveUIFacialEffectMat()
    if not self.m_RO then return end
    self.m_RO:RemoveFX(self.m_UIFacialFxKey)
end

LuaPinchFaceMgr.m_IsEnableCommandMode = true
LuaPinchFaceMgr.m_LastFaceSliderData = {}
LuaPinchFaceMgr.m_LastFacialPartindex2ColorId = {}
LuaPinchFaceMgr.m_LastFacialPartindex2Color = {}
LuaPinchFaceMgr.m_LastFacialPartIndex2ArrData = {}
function LuaPinchFaceMgr:InitCommandMode()
    self.m_LastFaceSliderData = {}
    self.m_LastFacialPartIndex2ArrData = {}
    self.m_LastFacialPartindex2ColorId = {}
    self.m_LastFacialPartindex2Color = {}
    LuaCommandMgr:Clear(100)
end

--@region get、set face data
function LuaPinchFaceMgr:RecordLastFaceSliderData(id2ValueMap)
    if not self.m_IsEnableCommandMode then return end
    if id2ValueMap then
        for id, value in pairs(id2ValueMap) do
            self.m_LastFaceSliderData[id] = value
        end
    end
end

function LuaPinchFaceMgr:SetFaceSliderData(id2ValueMap, isRecord)
    if not self.m_RO then return end
    self.m_IsFaceChanged = true
    if isRecord and self.m_IsEnableCommandMode then
        local lastMapInfo = {}
        local curMapInfo = {}
        if id2ValueMap then
            for id, value in pairs(id2ValueMap) do
                lastMapInfo[id] = self.m_LastFaceSliderData[id]
                curMapInfo[id] = value
            end
        end
        print("LuaCommandMgr:Regist:SetFaceSliderData")
        LuaCommandMgr:Regist(function()
            self:OnFaceDNAChanged(curMapInfo)
        end, function()
            self:OnFaceDNAChanged(lastMapInfo)
        end)
        return
    end
    self:OnFaceDNAChanged(id2ValueMap)
end

function LuaPinchFaceMgr:OnFaceDNAChanged(id2ValueMap)
    if not self.m_RO then return end
    local faceDnaData = self.m_RO.FaceDnaData
    if id2ValueMap and faceDnaData then
        for id, value in pairs(id2ValueMap) do
            if faceDnaData.Length > id and id >= 0 then
                faceDnaData[id] = value
            end
        end
        self:OnCurFacialDataUpdate()
        self:StopBlendShapeTemporarily()
        self.m_RO:ApplyFaceDNA(faceDnaData)
        g_ScriptEvent:BroadcastInLua("OnFaceDNAChanged", id2ValueMap)
    end
end

function LuaPinchFaceMgr:GetFaceDnaDataVal(id)
    if self.m_RO and self.m_RO.FaceDnaData then
        if self.m_RO.FaceDnaData.Length > id and id >= 0 then
            return self.m_RO.FaceDnaData[id]
        end
    end
    return 0
end

--@endregion get、set face data

--@region get、set Facial data
function LuaPinchFaceMgr:GetSlidersLength(partIndex)
    local fc = self.m_RO.FacialAsset
    if fc and partIndex >= 0 and partIndex < fc.FacialPartDatas.Count then
        local fapd = fc.FacialPartDatas[partIndex]
        return fapd.Sliders.Count
    end
    return 0
end

function LuaPinchFaceMgr:GetFacialPartArrData(partIndex)
    if not self.m_RO then return end
    local partDatas = self.m_RO.FacialPartData
    if partDatas then
        if partIndex < partDatas.Length and partIndex >= 0 then
            local pd = partDatas[partIndex]
            if pd and pd.SliderInfos then
                local arr = {}
                table.insert(arr, pd.Style)
                table.insert(arr, pd.Path)
                local slidersLength = self:GetSlidersLength(partIndex)
                for i = 0, pd.SliderInfos.Length - 1 do
                    table.insert(arr, pd.SliderInfos[i])
                end
                for i = pd.SliderInfos.Length, slidersLength - 1 do
                    table.insert(arr, 0)
                end
                return arr
            end
        end
    end
    return nil
end

function LuaPinchFaceMgr:OnSetFacialPartData(partId2Arr)
    local partDatas = self.m_RO.FacialPartData
    if partDatas then
        for partindex, arr in pairs(partId2Arr) do
            if partindex < partDatas.Length and partindex >= 0 then
                local pd = partDatas[partindex]
                if pd and arr and #arr >= 2 then
                    pd.Style = arr[1]
                    pd.Path = arr[2]
                    local slidersLength = self:GetSlidersLength(partindex)
                    if pd.SliderInfos.Length < slidersLength then
                        local list = {}
                        for i = 0, slidersLength - 1 do
                            table.insert(list, arr[3 + i])
                        end
                        pd.SliderInfos = Table2Array(list, MakeArrayClass(float))
                    else
                        for i = 0, slidersLength - 1 do
                            if arr[3 + i] then
                                pd.SliderInfos[i] = arr[3 + i]
                            end
                        end
                    end
                end
            end
        end
        self:OnCurFacialDataUpdate()
        self:StopBlendShapeTemporarily()
        self.m_RO:ApplyFacialPartData(partDatas)
    end
end

function LuaPinchFaceMgr:GetFacialPartId2ArrAfterTheChange(partIndex2styleId, tabBarKey2sliderId2val, tabBarKey2colorid)
    if not self.m_RO then return end
    local partDatas = self.m_RO.FacialPartData
    if not partDatas then return end
    local partId2Arr = {}
    self:GetFacialPartId2ArrAfterTheStyleChange(partDatas, partId2Arr, partIndex2styleId)
    self:GetFacialPartId2ArrAfterTheSliderValChange(partDatas, partId2Arr, tabBarKey2sliderId2val)
    self:GetFacialPartId2ArrAfterTheFashionColorChange(partDatas, partId2Arr, tabBarKey2colorid)
    return partId2Arr
end

function LuaPinchFaceMgr:GetFacialPartId2ArrAfterTheStyleChange(partDatas, partId2Arr, partIndex2styleId)
    if not self.m_RO then return end
    local presetData = self.m_RO:GetCurPresetData()
    if partIndex2styleId and presetData and presetData.PartDatas then
        for partIndex, styleId in pairs(partIndex2styleId) do
            if partIndex < partDatas.Length and partIndex >= 0 then
                local pd = partDatas[partIndex]
                if pd then
                    local arr = {}
                    local presetPath = self.m_CurPresetDataStylePathsCache[partIndex]
                    local path = presetPath
                    local data = PinchFace_FacialStyle.GetData(styleId)
                    if data then
                        path = "Assets/Res/Character/Player3/FacialStyle/" .. data.Path
                    end
                    table.insert(arr, styleId)
                    table.insert(arr, path)
                    local isEmptyPresetStyle = (not presetPath) or (string.find(presetPath, "empty.style.asset"))
                    local slidersLength = self:GetSlidersLength(partIndex)
                    for i = 0, slidersLength - 1 do
                        table.insert(arr, 0)
                    end
                    for i = 0, presetData.PartDatas[partIndex].SliderInfos.Length - 1 do
                        local val = presetData.PartDatas[partIndex].SliderInfos[i]
                        if self.m_PreselectionPartDatas[partIndex] then
                            val = self.m_PreselectionPartDatas[partIndex][3 + i]
                        end
                        if isEmptyPresetStyle and self.m_RO.FacialAsset.FacialPartDatas and partIndex < self.m_RO.FacialAsset.FacialPartDatas.Count then
                            local fpd = self.m_RO.FacialAsset.FacialPartDatas[partIndex]
                            if fpd and fpd.Sliders and i < fpd.Sliders.Count and fpd.Sliders[i] and fpd.Sliders[i].MaxValue ~= fpd.Sliders[i].MinValue then
                                val = (fpd.Sliders[i].Value - fpd.Sliders[i].MinValue) /
                                (fpd.Sliders[i].MaxValue - fpd.Sliders[i].MinValue)
                            end
                        end
                        arr[3 + i] = val
                    end
                    partId2Arr[partIndex] = arr
                end
            end
        end
    end
end

function LuaPinchFaceMgr:GetFacialPartId2ArrAfterTheSliderValChange(partDatas, partId2Arr, tabBarKey2sliderId2val)
    if tabBarKey2sliderId2val then
        for partIndex, sliderId2val in pairs(tabBarKey2sliderId2val) do
            local arr = partId2Arr[partIndex]
            if arr == nil then
                arr = self:GetFacialPartArrData(partIndex)
            end
            if arr and #arr > 2 then
                for sliderId, val in pairs(sliderId2val) do
                    if arr[3 + sliderId] then
                        arr[3 + sliderId] = val
                    end
                end
                partId2Arr[partIndex] = arr
            end
        end
    end
end

function LuaPinchFaceMgr:GetFacialPartId2ArrAfterTheFashionColorChange(partDatas, partId2Arr, tabBarKey2colorid)
    if not tabBarKey2colorid then return end
    local tabBarKey2sliderId2val = {}
    for tabBarKey, colorid in pairs(tabBarKey2colorid) do
        local tabBarData = PinchFace_FacialTabBarData.GetData(tabBarKey)
        local colorData = PinchFace_FashionColor.GetData(colorid)
        local key2hsv = {}
        local sliderId1, sliderId2, sliderId3 = 0, 0, 0
        local fashionColorInfo = tabBarData.FashionColorInfo
        if fashionColorInfo.Length >= 3 then
            sliderId1, sliderId2, sliderId3 = fashionColorInfo[0], fashionColorInfo[1], fashionColorInfo[2]
            local h, s, v = colorData.Hue, colorData.Saturation, colorData.Brightness
            local hsv = Vector3(h, s, v)
            key2hsv[tabBarKey] = hsv
        end
        tabBarKey2sliderId2val[tabBarKey] = {}
        if fashionColorInfo.Length >= 4 then
            local sliderId = fashionColorInfo[3]
            if sliderId > 0 then
                tabBarKey2sliderId2val[tabBarKey][sliderId] = colorData.Intensity
            end
        end
        if fashionColorInfo.Length >= 5 then
            local sliderId = fashionColorInfo[4]
            if sliderId > 0 then
                tabBarKey2sliderId2val[tabBarKey][sliderId] = colorData.Gloss
            end
        end
        if partDatas then
            for partindex, hsv in pairs(key2hsv) do
                local arr = partId2Arr[partindex]
                if arr == nil then
                    arr = self:GetFacialPartArrData(partindex)
                end
                local slidersLength = self:GetSlidersLength(partindex)
                if partindex < partDatas.Length and partindex >= 0 then
                    local pd = partDatas[partindex]
                    local fc = self.m_RO.FacialAsset
                    if pd and fc and partindex >= 0 and partindex < fc.FacialPartDatas.Count then
                        local fapd = fc.FacialPartDatas[partindex]
                        local displayType = fapd:GetSliderDisplayType(sliderId1)
                        local minSliderIndex = math.min(math.min(sliderId1, sliderId2), sliderId3)
                        local maxSliderIndex = math.max(math.max(sliderId1, sliderId2), sliderId3)
                        if minSliderIndex >= 0 and maxSliderIndex < slidersLength then
                            if displayType == EmDisplayType.Color_HSV then
                                arr[3 + sliderId1] = hsv.x
                                arr[3 + sliderId2] = hsv.y
                                arr[3 + sliderId3] = hsv.z
                            elseif displayType == EmDisplayType.Color_RGB or displayType == EmDisplayType.Color_RGBA then
                                local color = Color.HSVToRGB(hsv.x, hsv.y, hsv.z)
                                arr[3 + sliderId1] = color.r
                                arr[3 + sliderId2] = color.g
                                arr[3 + sliderId3] = color.b
                            end
                        end
                    end
                end
                partId2Arr[partindex] = arr
            end
        end
        self:GetFacialPartId2ArrAfterTheSliderValChange(partDatas, partId2Arr, tabBarKey2sliderId2val)
    end
end

function LuaPinchFaceMgr:GetFacialPartId2ArrAfterTheColorHSVChange(key2hsv, sliderIndex1, sliderIndex2, sliderIndex3,
                                                                   styleData)
    if not self.m_RO then return end
    local partDatas = self.m_RO.FacialPartData
    if not partDatas then return end
    local partId2Arr = {}
    for partindex, hsv in pairs(key2hsv) do
        if partindex < partDatas.Length and partindex >= 0 then
            local arr = partId2Arr[partindex]
            if arr == nil then
                arr = self:GetFacialPartArrData(partindex)
            end
            local pd = partDatas[partindex]
            local fc = self.m_RO.FacialAsset
            local slidersLength = self:GetSlidersLength(partindex)
            if pd and fc and partindex >= 0 and partindex < fc.FacialPartDatas.Count then
                local fapd = fc.FacialPartDatas[partindex]
                local displayType = fapd:GetSliderDisplayType(sliderIndex1)
                local minSliderIndex = math.min(math.min(sliderIndex1, sliderIndex2), sliderIndex3)
                local maxSliderIndex = math.max(math.max(sliderIndex1, sliderIndex2), sliderIndex3)
                if minSliderIndex >= 0 and maxSliderIndex < slidersLength then
                    if displayType == EmDisplayType.Color_HSV then
                        local colorRGBA = styleData.MainTexColor
                        if colorRGBA and colorRGBA.Length == 4 then
                            local color = Color(colorRGBA[0], colorRGBA[1], colorRGBA[2], colorRGBA[3])
                            local h, s, v = ColorExt.RGB2HSV(color)
                            arr[3 + sliderIndex1] = fapd:GetCurSliderInterpolation(sliderIndex1, hsv.x - h)
                            arr[3 + sliderIndex2] = fapd:GetCurSliderInterpolation(sliderIndex2, hsv.y - s)
                            arr[3 + sliderIndex3] = fapd:GetCurSliderInterpolation(sliderIndex3, hsv.z - v)
                        end
                    elseif displayType == EmDisplayType.Color_RGB or displayType == EmDisplayType.Color_RGBA then
                        local color = Color.HSVToRGB(hsv.x, hsv.y, hsv.z)
                        arr[3 + sliderIndex1] = color.r
                        arr[3 + sliderIndex2] = color.g
                        arr[3 + sliderIndex3] = color.b
                    end
                end
            end
            partId2Arr[partindex] = arr
        end
    end
    return partId2Arr
end

function LuaPinchFaceMgr:SelectFacialPartStyle(key, styleData, forceSet)
    local changePartindex2StyleId = nil
    local curstyleId = LuaPinchFaceMgr:GetPartStyleId(key)
    if curstyleId ~= styleData.Id or forceSet then
        changePartindex2StyleId = {}
        changePartindex2StyleId[key] = styleData.Id
        changePartindex2StyleId[self.m_RightEyeFacialTabKey] = changePartindex2StyleId[self.m_LeftEyeFacialTabKey]
    end
    if changePartindex2StyleId then
        local tabBarKey2colorid = nil
        local tabBarKey2sliderId2val = nil
        local tabBarData = PinchFace_FacialTabBarData.GetData(key)
        local presetColorList = styleData.PresetColorList and styleData.PresetColorList or tabBarData.PresetColorList
        if presetColorList then
            tabBarKey2colorid = {}
            tabBarKey2colorid[key] = presetColorList[0]
            tabBarKey2colorid[self.m_RightEyeFacialTabKey]  = tabBarKey2colorid[self.m_LeftEyeFacialTabKey]
        end  
        if changePartindex2StyleId[self.m_LeftEyeFacialTabKey] then
            local sliderId = 0 --虹膜缩放需要特殊处理
            local val =  self:GetFacialPartDataVal(self.m_LeftEyeFacialTabKey, sliderId)
            local sliderId2val = {[sliderId] = val}
            tabBarKey2sliderId2val = {[self.m_LeftEyeFacialTabKey] = sliderId2val, [self.m_RightEyeFacialTabKey] = sliderId2val}
        end
        self:ChangeFacialPartStyle(changePartindex2StyleId, tabBarKey2sliderId2val, tabBarKey2colorid, true)
    end
end

function LuaPinchFaceMgr:ChangeFacialPartStyle(partIndex2styleId, tabBarKey2sliderId2val, tabBarKey2colorid, isRecord)
    self.m_IsFaceChanged = true
    local newPartId2Arr = self:GetFacialPartId2ArrAfterTheChange(partIndex2styleId, tabBarKey2sliderId2val,
        tabBarKey2colorid)
    if isRecord and self.m_IsEnableCommandMode then
        local lastPartId2Arr = {}
        local lasttabBarKey2colorid = {}
        for partIndex, styleId in pairs(partIndex2styleId) do
            lastPartId2Arr[partIndex] = self:GetFacialPartArrData(partIndex)
            lasttabBarKey2colorid[partIndex] = self.m_LastFacialPartindex2ColorId[partIndex]
        end
        print("LuaCommandMgr:Regist:ChangeFacialPartStyle")
        LuaCommandMgr:Regist(function()
            self:OnChangeFacialPartStyle(newPartId2Arr, tabBarKey2colorid)
        end, function()
            self:OnChangeFacialPartStyle(lastPartId2Arr, lasttabBarKey2colorid)
        end)
        return
    end
    self:OnChangeFacialPartStyle(newPartId2Arr, tabBarKey2colorid)
end

function LuaPinchFaceMgr:OnChangeFacialPartStyle(partId2Arr, tabBarKey2colorid)
    self:OnSetFacialPartData(partId2Arr)
    local partIndex2styleId = {}
    for partindex, arr in pairs(partId2Arr) do
        if arr and #arr > 0 then
            partIndex2styleId[partindex] = arr[1]
        end
        self.m_LastFacialPartindex2ColorId[partindex] = tabBarKey2colorid and tabBarKey2colorid[partindex] or nil
    end
    g_ScriptEvent:BroadcastInLua("OnChangeFacialPartStyle", partIndex2styleId)
end

function LuaPinchFaceMgr:GetPartStyleId(partindex)
    if self.m_RO then
        local partDatas = self.m_RO.FacialPartData
        if partDatas and partindex < partDatas.Length and partindex >= 0 then
            return partDatas[partindex].Style
        end
    end
    return 0
end

function LuaPinchFaceMgr:RecordLastFacialPartData(partindex)
    self.m_LastFacialPartIndex2ArrData[partindex] = self:GetFacialPartArrData(partindex)
end

function LuaPinchFaceMgr:SetFacialPartDataVal(tabBarKey2sliderId2val, isRecord)
    self.m_IsFaceChanged = true
    local newPartId2Arr = self:GetFacialPartId2ArrAfterTheChange(nil, tabBarKey2sliderId2val, nil)
    if isRecord and self.m_IsEnableCommandMode then
        if tabBarKey2sliderId2val then
            local lastPartId2Arr = {}
            for partindex, sliderId2val in pairs(tabBarKey2sliderId2val) do
                if self.m_LastFacialPartIndex2ArrData[partindex] then
                    lastPartId2Arr[partindex] = self.m_LastFacialPartIndex2ArrData[partindex]
                else
                    lastPartId2Arr[partindex] = self:GetFacialPartArrData(partindex)
                end
            end
            print("LuaCommandMgr:Regist:SetFacialPartDataVal")
            LuaCommandMgr:Regist(function()
                self:OnFacialPartDataValChanged(newPartId2Arr)
            end, function()
                self:OnFacialPartDataValChanged(lastPartId2Arr)
            end)
            return
        end
    end
    self:OnFacialPartDataValChanged(newPartId2Arr)
end

function LuaPinchFaceMgr:OnFacialPartDataValChanged(partId2Arr)
    self:OnSetFacialPartData(partId2Arr)
    local tabBarKey2sliderId2val = {}
    for partindex, arr in pairs(partId2Arr) do
        if arr and #arr > 2 then
            local sliderId2val = {}
            for i = 3, #arr do
                sliderId2val[i - 3] = arr[i]
            end
            tabBarKey2sliderId2val[partindex] = sliderId2val
        end
    end
    g_ScriptEvent:BroadcastInLua("OnFacialPartDataValChanged", tabBarKey2sliderId2val)
end

function LuaPinchFaceMgr:GetFacialPartDataVal(partindex, sliderIndex)
    if self.m_RO then
        local partDatas = self.m_RO.FacialPartData
        if partDatas and partindex < partDatas.Length and partindex >= 0 then
            local pd = partDatas[partindex]
            if pd and sliderIndex < pd.SliderInfos.Length and sliderIndex >= 0 then
                return pd.SliderInfos[sliderIndex]
            end
        end
    end
    return 0
end

function LuaPinchFaceMgr:SetFacialFashionColor(tabBarKey2colorid, isRecord)
    self.m_IsFaceChanged = true
    local newPartId2Arr = self:GetFacialPartId2ArrAfterTheChange(nil, nil, tabBarKey2colorid)
    if self.m_IsEnableCommandMode and isRecord then
        local newkey2id = {}
        local lastkey2id = {}
        local lastPartId2Arr = {}
        for partindex, id in pairs(tabBarKey2colorid) do
            newkey2id[partindex] = id
            if self.m_LastFacialPartindex2ColorId[partindex] then
                lastkey2id[partindex] = self.m_LastFacialPartindex2ColorId[partindex]
            end
            lastPartId2Arr[partindex] = self:GetFacialPartArrData(partindex)
        end
        print("LuaCommandMgr:Regist:SetFacialFashionColor")
        LuaCommandMgr:Regist(function()
            self:OnSetFacialFashionColor(newPartId2Arr, newkey2id)
        end, function()
            self:OnSetFacialFashionColor(lastPartId2Arr, lastkey2id)
        end)
        return
    end
    self:OnSetFacialFashionColor(newPartId2Arr, tabBarKey2colorid)
end

function LuaPinchFaceMgr:OnSetFacialFashionColor(partId2Arr, tabBarKey2colorid)
    self:OnFacialPartDataValChanged(partId2Arr)
    for partindex, id in pairs(tabBarKey2colorid) do
        self.m_LastFacialPartindex2ColorId[partindex] = id
    end
    g_ScriptEvent:BroadcastInLua("OnSetFacialFashionColor", tabBarKey2colorid)
end

function LuaPinchFaceMgr:GetFashionColor(partindex, sliderIndex1, sliderIndex2, sliderIndex3, styleData, fashionColorData)
    if self.m_RO and styleData and fashionColorData then
        local hue, saturation, brightness = fashionColorData.Hue, fashionColorData.Saturation,
            fashionColorData.Brightness
        local fc = self.m_RO.FacialAsset
        if fc and partindex >= 0 and partindex < fc.FacialPartDatas.Count then
            local fapd = fc.FacialPartDatas[partindex]
            local displayType = fapd:GetSliderDisplayType(sliderIndex1)
            local v1 = fapd:GetCurSliderValue(sliderIndex1, hue)
            local v2 = fapd:GetCurSliderValue(sliderIndex2, saturation)
            local v3 = fapd:GetCurSliderValue(sliderIndex3, brightness)
            if displayType == EmDisplayType.Color_HSV then
                local colorRGBA = styleData.MainTexColor
                if colorRGBA and colorRGBA.Length == 4 then
                    local color = Color(colorRGBA[0], colorRGBA[1], colorRGBA[2], colorRGBA[3])
                    local h, s, v = 0, 0, 0
                    h, s, v = ColorExt.RGB2HSV(color)
                    v1 = v1 + h
                    v2 = v2 + s
                    v3 = v3 + v
                    v2 = (v2 + 0.2) / 1.2
                    v3 = (v3 + 0.33) / 1.33
                end
                return Color.HSVToRGB(v1 - math.floor(v1), math.max(0, math.min(1, v2)), math.max(0, math.min(1, v3)))
            elseif displayType == EmDisplayType.Color_RGB or displayType == EmDisplayType.Color_RGBA then
                return Color.HSVToRGB(v1, v2, v3)
            end
        end
    end
    return Color.black
end

function LuaPinchFaceMgr:RecordFacialPartDataColorHSV(key2hsv)
    for partindex, color in pairs(key2hsv) do
        self.m_LastFacialPartindex2Color[partindex] = color
        self:RecordLastFacialPartData(partindex)
    end
end

function LuaPinchFaceMgr:SetFacialPartDataColorHSV(key2hsv, sliderIndex1, sliderIndex2, sliderIndex3, styleData, isRecord)
    self.m_IsFaceChanged = self.m_IsFaceChanged or isRecord
    local newPartId2Arr = self:GetFacialPartId2ArrAfterTheColorHSVChange(key2hsv, sliderIndex1, sliderIndex2,
        sliderIndex3, styleData)
    if self.m_IsEnableCommandMode and isRecord then
        local newkey2hsv = {}
        local lastkey2hsv = {}
        local lastPartId2Arr = {}
        for partindex, color in pairs(key2hsv) do
            newkey2hsv[partindex] = color
            if self.m_LastFacialPartindex2Color[partindex] then
                lastkey2hsv[partindex] = self.m_LastFacialPartindex2Color[partindex]
            end
            if self.m_LastFacialPartIndex2ArrData[partindex] then
                lastPartId2Arr[partindex] = self.m_LastFacialPartIndex2ArrData[partindex]
            else
                lastPartId2Arr[partindex] = self:GetFacialPartArrData(partindex)
            end
        end
        print("LuaCommandMgr:Regist:SetFacialPartDataColorHSV")
        LuaCommandMgr:Regist(function()
            self:OnSetFacialPartDataColorHSV(newPartId2Arr, newkey2hsv, sliderIndex1, sliderIndex2, sliderIndex3, true)
        end, function()
            self:OnSetFacialPartDataColorHSV(lastPartId2Arr, lastkey2hsv, sliderIndex1, sliderIndex2, sliderIndex3, true)
        end)
        return
    end
    self:OnSetFacialPartDataColorHSV(newPartId2Arr, key2hsv, sliderIndex1, sliderIndex2, sliderIndex3)
end

function LuaPinchFaceMgr:OnSetFacialPartDataColorHSV(partId2Arr, key2hsv, sliderIndex1, sliderIndex2, sliderIndex3,
                                                     updateView)
    self:OnSetFacialPartData(partId2Arr)
    for partindex, arr in pairs(partId2Arr) do
        self.m_LastFacialPartindex2ColorId[partindex] = nil
    end
    if updateView then
        g_ScriptEvent:BroadcastInLua("OnSetFacialPartDataColorHSV", key2hsv, sliderIndex1, sliderIndex2, sliderIndex3)
    end
end

function LuaPinchFaceMgr:GetFacialPartDataColorHSV(partindex, sliderIndex1, sliderIndex2, sliderIndex3, styleData)
    if not self.m_RO then return end
    local partDatas = self.m_RO.FacialPartData
    if not partDatas then return end
    local res = Vector3.zero
    local slidersLength = self:GetSlidersLength(partindex)
    if partindex < partDatas.Length and partindex >= 0 then
        local pd = partDatas[partindex]
        local fc = self.m_RO.FacialAsset
        if pd and fc and partindex >= 0 and partindex < fc.FacialPartDatas.Count then
            local fapd = fc.FacialPartDatas[partindex]
            local displayType = fapd:GetSliderDisplayType(sliderIndex1)
            local minSliderIndex = math.min(math.min(sliderIndex1, sliderIndex2), sliderIndex3)
            local maxSliderIndex = math.max(math.max(sliderIndex1, sliderIndex2), sliderIndex3)
            if minSliderIndex >= 0 and maxSliderIndex < slidersLength then
                local a = self:GetFacialPartDataVal(partindex, sliderIndex1)
                local b = self:GetFacialPartDataVal(partindex, sliderIndex2)
                local c = self:GetFacialPartDataVal(partindex, sliderIndex3)
                if displayType == EmDisplayType.Color_HSV then
                    local colorRGBA = styleData.MainTexColor
                    if colorRGBA and colorRGBA.Length == 4 then
                        local color = Color(colorRGBA[0], colorRGBA[1], colorRGBA[2], colorRGBA[3])
                        local h, s, v = ColorExt.RGB2HSV(color)
                        local v1 = fapd:GetCurSliderValue(sliderIndex1, a)
                        local v2 = fapd:GetCurSliderValue(sliderIndex2, b)
                        local v3 = fapd:GetCurSliderValue(sliderIndex3, c)
                        res = Vector3(h + v1, s + v2, v + v3)
                    end
                elseif displayType == EmDisplayType.Color_RGB or displayType == EmDisplayType.Color_RGBA then
                    local color = Color(a, b, c)
                    local h, s, v = ColorExt.RGB2HSV(color)
                    res = Vector3(h, s, v)
                end
            end
        end
    end
    return res
end

function LuaPinchFaceMgr:SetCustomColor(key2hsv, sliderIndex1, sliderIndex2, sliderIndex3, tabBarKey2sliderId2val,
                                        styleData, isRecord)
    if not self.m_RO then return end
    local partDatas = self.m_RO.FacialPartData
    if not partDatas then return end
    self.m_IsFaceChanged = self.m_IsFaceChanged or isRecord
    local newPartId2Arr = self:GetFacialPartId2ArrAfterTheColorHSVChange(key2hsv, sliderIndex1, sliderIndex2,
        sliderIndex3, styleData)
    self:GetFacialPartId2ArrAfterTheSliderValChange(partDatas, newPartId2Arr, tabBarKey2sliderId2val)
    if self.m_IsEnableCommandMode and isRecord then
        local newkey2hsv = {}
        local lastkey2hsv = {}
        local lastPartId2Arr = {}
        for partindex, color in pairs(key2hsv) do
            newkey2hsv[partindex] = color
            if self.m_LastFacialPartindex2Color[partindex] then
                lastkey2hsv[partindex] = self.m_LastFacialPartindex2Color[partindex]
            end
            if self.m_LastFacialPartIndex2ArrData[partindex] then
                lastPartId2Arr[partindex] = self.m_LastFacialPartIndex2ArrData[partindex]
            else
                lastPartId2Arr[partindex] = self:GetFacialPartArrData(partindex)
            end
        end
        print("LuaCommandMgr:Regist:SetCustomColor")
        LuaCommandMgr:Regist(function()
            self:OnSetCustomColor(newPartId2Arr, newkey2hsv, sliderIndex1, sliderIndex2, sliderIndex3)
        end, function()
            self:OnSetCustomColor(lastPartId2Arr, lastkey2hsv, sliderIndex1, sliderIndex2, sliderIndex3)
        end)
        return
    end
    self:OnSetCustomColor(newPartId2Arr, key2hsv, sliderIndex1, sliderIndex2, sliderIndex3)
end

function LuaPinchFaceMgr:OnSetCustomColor(partId2Arr, key2hsv, sliderIndex1, sliderIndex2, sliderIndex3)
    self:OnFacialPartDataValChanged(partId2Arr)
    g_ScriptEvent:BroadcastInLua("OnSetFacialPartDataColorHSV", key2hsv, sliderIndex1, sliderIndex2, sliderIndex3)
end

LuaPinchFaceMgr.m_IsHairChanged = false
function LuaPinchFaceMgr:SelectHair(id, isRecord, hairChanged)
    if self.m_HairId == id then
        if not self.m_IsShowLuoZhuangHair then
            self:OnSelectHair(self.m_HairId)
        end
        return
    end
    self.m_IsHairChanged = self.m_IsHairChanged or hairChanged
    if isRecord and self.m_IsEnableCommandMode then
        local lastHairId = self.m_HairId
        local curHairId = id
        local lastIsShowLuoZhuangHair = self.m_IsShowLuoZhuangHair
        print("LuaCommandMgr:Regist:SelectHair")
        LuaCommandMgr:Regist(function()
            self:OnSelectHair(curHairId)
        end, function()
            if not lastIsShowLuoZhuangHair then
                self.m_HairId = -1
                self:SetFashion(self.m_FashionData, EnumPinchFaceAniState.PreviewStand01, false)
            else
                self:OnSelectHair(lastHairId)
            end
        end)
        return
    end
    self:OnSelectHair(id)
end

function LuaPinchFaceMgr:OnSelectHair(id)
    if not self.m_RO or not id then return end
    if id == -1 then
        self:SetFashion(self.m_FashionData, EnumPinchFaceAniState.PreviewStand01, false)
        return
    end
    local hairdata = PinchFace_Hair.GetData(id)
    local initializationData = self:GetInitializationData()
    if initializationData and hairdata then
        self.m_HairId = id
        self.m_IsShowLuoZhuangHair = true
        g_ScriptEvent:BroadcastInLua("OnShowLuoZhuangPinchFace", self.m_IsShowLuoZhuangHair)
        local path = SafeStringFormat3("Character/Player3/Prefab/%s/%s_%s_hd_head.prefab", initializationData.ResName,
            initializationData.ResName, hairdata.fashionName)
        self.m_RO:AddChild(path, "head")
        self:SetAdvancedHairColorIndex(self.m_AdvancedHairColorIndex)
        g_ScriptEvent:BroadcastInLua("OnSelectPinchFaceHair", id)
        self:CacheOfflineData()
    end
end

LuaPinchFaceMgr.m_LastColor = nil
function LuaPinchFaceMgr:ChangeHairColor(lastColor, color, newHeadId)
    if self.m_AdvancedHairColorIndex ~= 0 then
        self:SetAdvancedHairColorIndex(0)
    end
    if self.m_HeadId == newHeadId and self.m_IsShowLuoZhuangHair then
        return
    end
    self.m_LastColor = lastColor
    if self.m_IsInCreateMode then
        self.m_IsHairChanged = true
    end
    if self.m_IsEnableCommandMode and self.m_LastColor and self.m_IsInCreateMode then
        local lastColor = self.m_LastColor
        local curColor = color
        local lastHeadId = self.m_HeadId
        local lastIsShowLuoZhuangHair = self.m_IsShowLuoZhuangHair
        print("LuaCommandMgr:Regist:ChangeHairColor")
        LuaCommandMgr:Regist(function()
            self:OnChangeHairColor(curColor, newHeadId)
        end, function()
            if not lastIsShowLuoZhuangHair then
                self.m_HeadId = lastHeadId
                self:CacheOfflineData()
                self.m_LastColor = lastColor
                self.m_AdvancedHairColorIndex = 0
                self:SetFashion(self.m_FashionData, EnumPinchFaceAniState.PreviewStand01, false)
            else
                self:OnChangeHairColor(lastColor, lastHeadId)
            end
        end)
        return
    end
    self:OnChangeHairColor(color, newHeadId)
end

function LuaPinchFaceMgr:OnChangeHairColor(color, headId)
    if headId then
        self.m_HeadId = headId
        self:CacheOfflineData()
        g_ScriptEvent:BroadcastInLua("OnChangeHairColor", color, headId)
    end
    if self.m_RO then
        self.m_AdvancedHairColorIndex = 0
        if self:IsDressedFashionHair() then
            self:OnSelectHair(self.m_HairId)
        else
            self:SetAdvancedHairColorIndex(self.m_AdvancedHairColorIndex)
        end
        self.m_LastColor = color
    end
end

--@endregion get、set Facial data
function LuaPinchFaceMgr:RotateRoleModel(newEulerAngleY)
    if not self.m_RO then return end
    local t = self.m_RO.transform
    self:KillRotateRoleTweener()
    t.transform.localEulerAngles = Vector3(0, newEulerAngleY, 0)
end

LuaPinchFaceMgr.m_LastFaceTabDefaultAngle = 0
LuaPinchFaceMgr.m_RotateRoleTweener = nil
function LuaPinchFaceMgr:SetFaceTabDefaultAngle(data)
    if not self.m_RO then return end
    if self.m_LastFaceTabDefaultAngle ~= data.DefaultAngle then
        local t = self.m_RO.transform
        local initialAngle = t.localEulerAngles.y
        local endAngle = data.DefaultAngle
        if math.abs(endAngle - initialAngle) > math.abs(360 - endAngle - initialAngle) then
            endAngle = 360 - endAngle
        end
        self:KillRotateRoleTweener()
        self.m_RotateRoleTweener = LuaTweenUtils.TweenFloat(initialAngle, endAngle, 2, function(val)
            t.localEulerAngles = Vector3(0, val, 0)
        end)
        self.m_LastFaceTabDefaultAngle = data.DefaultAngle
    end
end

function LuaPinchFaceMgr:KillRotateRoleTweener()
    if self.m_RotateRoleTweener then
        LuaTweenUtils.Kill(self.m_RotateRoleTweener, false)
        self.m_RotateRoleTweener = nil
    end
end

LuaPinchFaceMgr.m_IsEnableIK = true
function LuaPinchFaceMgr:SetEnableIk(isEnable)
    self.m_IsEnableIK = isEnable
    self:SetIKLookAtTo(self.m_IsUseIKLookAt)
end

function LuaPinchFaceMgr:SetIKLookAtTo(isSelected)
    self.m_IsUseIKLookAt = isSelected
    g_ScriptEvent:BroadcastInLua("OnSetPinchFaceIKLookAtTo", isSelected)
end

LuaPinchFaceMgr.m_BgColorData = nil
function LuaPinchFaceMgr:SetCameraBgColor(data)
    self.m_BgColorData = data
    g_ScriptEvent:BroadcastInLua("OnSetPinchFaceCameraBgColor", data)
end

LuaPinchFaceMgr.m_EnvironmentFxData = nil
function LuaPinchFaceMgr:SetEnvironmentFx(data)
    self.m_EnvironmentFxData = data
    g_ScriptEvent:BroadcastInLua("OnSetPinchFaceEnvironmentFx", data)
end

LuaPinchFaceMgr.m_FashionData = nil
function LuaPinchFaceMgr:SetFashion(data, anistate, showLuoZhuangHair, customFacialData)
    if showLuoZhuangHair == nil then
        showLuoZhuangHair = self.m_IsShowLuoZhuangHair
    end
    self.m_FashionData = data
    if CClientMainPlayer.Inst and data == nil then
        self:LoadPlayerRes(self.m_IsModifyXianSheng, showLuoZhuangHair, nil, nil, nil, nil, anistate, customFacialData)
    elseif not data or data.isDefault then
        self:LoadPlayerRes(false, showLuoZhuangHair, 0, 0, false, false, anistate, customFacialData)
    elseif string.find(data.data.Description, LocalString.GetString("凡身")) then
        self:LoadPlayerRes(false, showLuoZhuangHair, 0, 0, false, false, anistate, customFacialData)
    elseif string.find(data.data.Description, LocalString.GetString("飞升")) then
        self:LoadPlayerRes(true, showLuoZhuangHair, 0, 0, true, false, anistate, customFacialData)
    elseif string.find(data.data.Description, LocalString.GetString("150级鬼装")) then
        self:LoadPlayerRes(true, showLuoZhuangHair, 0, 0, false, true, anistate, customFacialData)
    elseif string.find(data.data.Description, LocalString.GetString("神兵")) then
        self:LoadPlayerRes(true, showLuoZhuangHair, 0, 0, false, false, anistate, customFacialData)
    else
        local fashionIdArr = data.data.FashionId
        local headFashionId = (fashionIdArr and fashionIdArr.Length >= 1) and fashionIdArr[0] or 0
        local bodyFashionId = (fashionIdArr and fashionIdArr.Length >= 2) and fashionIdArr[1] or 0
        self:LoadPlayerRes(true, showLuoZhuangHair, headFashionId, bodyFashionId, false, false, anistate,
            customFacialData)
    end
end

function LuaPinchFaceMgr:IsInInCustomFashion()
    return self.m_FashionData and self.m_FashionData.data and self.m_FashionData.data.FashionId and
    self.m_FashionData.data.FashionId.Length > 0
end

function LuaPinchFaceMgr:SetWeaponVisible(visible)
    local weaponVisible = (visible and self.m_EquipmentData and self.m_EquipmentData.EquipmentId) and true or false
    self.m_RO:SetWeaponVisible(EWeaponVisibleReason.Expression, weaponVisible)
    self.m_RO:ForceHideKuiLei(weaponVisible and EHideKuiLeiStatus.None or EHideKuiLeiStatus.ForceHideKuiLeiAndClose)
end

LuaPinchFaceMgr.m_EquipmentData = nil
function LuaPinchFaceMgr:SetEquipment(data)
    self.m_EquipmentData = data
    self:SetFashion(self.m_FashionData)
end

LuaPinchFaceMgr.m_SkinColorIndex = 0
function LuaPinchFaceMgr:SetSkinColor(index)
    if self.m_RO then
        self.m_SkinColorIndex = index
        self:SetFashion(self.m_FashionData)
    end
end

LuaPinchFaceMgr.m_AdvancedHairColorIndex = 0
function LuaPinchFaceMgr:SetAdvancedHairColorIndex(index)
    local data = self.m_HairColorDataArr[index + 1]
    if not self.m_RO then return end
    self.m_AdvancedHairColorIndex = 0
    self.m_RO:ClearAvatarColorChanger(AvatarHelper.Avatar3Hair)
    if data.isDefault and self.m_RO and self.m_CurEnumClass and self.m_CurEnumGender then
        CClientMainPlayer.ChangeHairColorIndex(self.m_RO, self.m_CurEnumClass, self.m_CurEnumGender, self.m_HeadId)
    elseif data then
        self:OnChangeAdvancedHairColor(data)
        self.m_AdvancedHairColorIndex = index
    end
end

function LuaPinchFaceMgr:GetIKOffsetXY()
    local initializationData = self:GetInitializationData()
    return initializationData.IKOffsetX, initializationData.IKOffsetY
end

function LuaPinchFaceMgr:IsDressedFashionHair()
    return not self.m_IsShowLuoZhuangHair and (self:IsInInCustomFashion() or (self.m_AppearanceData and self.m_AppearanceData.HeadFashionId > 0 and self.m_AppearanceData.HideHelmetEffect == 0))
end

LuaPinchFaceMgr.m_AppearanceData = nil
function LuaPinchFaceMgr:GetAppearanceProp(isXianShen, showLuoZhuangHair, headFashionId, bodyFashionId, feishengputong,
    is150Ghost, anistate, customFacialData)
    local equipmentInfos, shenBingFirstModelId, shenBingSecondModelId, shenBingTrainLvFxId, shenBingRanSeData = self
        :GetEquipmentInfos(isXianShen, is150Ghost)
    local appearanceData = CPropertyAppearance.GetDataFromCustom(self.m_CurEnumClass, self.m_CurEnumGender,
        CPropertySkill.GetDefaultYingLingStateByGender(self.m_CurEnumGender), equipmentInfos, nil, 0,
        LuaPinchFaceMgr.m_HeadId, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, isXianShen and 1 or 0, shenBingFirstModelId, shenBingSecondModelId,
        shenBingTrainLvFxId, shenBingRanSeData, 0, 0, 0, 0, 0, 0, nil, 0)
    if CClientMainPlayer.Inst then
        appearanceData = CClientMainPlayer.Inst.AppearanceProp:Clone()
        appearanceData.HeadId = LuaPinchFaceMgr.m_HeadId
        appearanceData.YingLingState = EnumToInt(self.m_CurEnumGender) + 1
        appearanceData.AdvancedHairColor = ""
        appearanceData.HideSuitIdToOtherPlayer = 1
        appearanceData.HideGemFxToOtherPlayer = 1
        appearanceData.HideTalismanAppearance = 1
        appearanceData.HideWing = 1
        appearanceData.HideShenBingBackOrnament = 1
        appearanceData.HideBackPendantFashionEffect = 1
        appearanceData.HideBackPendant = 1
        if is150Ghost ~= nil then
            appearanceData.Equipment = equipmentInfos
            appearanceData.ShenBingFirstModelId = shenBingFirstModelId
            appearanceData.ShenBingSecondModelId = shenBingSecondModelId
            appearanceData.ShenBingTrainLvFxId = shenBingTrainLvFxId
            appearanceData.ShenBingRanSeData = shenBingRanSeData
            appearanceData.HeadFashionId = 0
            appearanceData.BodyFashionId = 0
            appearanceData.HideShenBingHeadwear = 0
            appearanceData.HideShenBingClothes = 0
            appearanceData.Hide150GhostClothes = 0
            appearanceData.Hide150GhostHeadwear = 0
            appearanceData.HideHeadFashionEffect = 0
            appearanceData.HideBodyFashionEffect = 0
        end
        appearanceData.FeiShengAppearanceXianFanStatus = isXianShen and 1 or 0
    end
    appearanceData.HideZhanKuangMask = 1
    local facialData = CCustomFacialData()
    if not customFacialData then
        facialData.FacialData = self.m_CurFacialData
    else
        facialData.FacialData = customFacialData
        self:OnCurFacialDataUpdate(customFacialData)
    end
    if showLuoZhuangHair then
        facialData.HairId = self.m_HairId
    end
    facialData.HasFacialData = true
    facialData.FaceId = self.m_PreselectionIndex + 1
    local id = EnumToInt(self.m_CurEnumClass) * 100 + EnumToInt(self.m_CurEnumGender) * 10 + 0
    appearanceData.CustomFacialDatas[id] = facialData
    appearanceData.CustomFacialDatas[id + 1] = facialData
    appearanceData.HideHelmetEffect = showLuoZhuangHair and 1 or 0
    appearanceData.ShowHDHead = true
    appearanceData.SkinColor = self.m_SkinColorIndex
    if headFashionId then
        appearanceData.HeadFashionId = headFashionId
    end
    if bodyFashionId then
        appearanceData.BodyFashionId = bodyFashionId
    end
    if feishengputong or is150Ghost then
        appearanceData.HideShenBingHeadwear = 1
        appearanceData.HideShenBingClothes = 1
    end
    if feishengputong then
        appearanceData.Hide150GhostClothes = 1
        appearanceData.Hide150GhostHeadwear = 1
    end
    appearanceData.AdvancedHairColorId = self.m_AdvancedHairColorIndex
    appearanceData.SkinColor = self.m_SkinColorIndex
    self.m_AppearanceData = appearanceData
    return appearanceData,facialData
end

LuaPinchFaceMgr.m_IsShowLuoZhuangHair = false
LuaPinchFaceMgr.m_CurFacialData = nil
function LuaPinchFaceMgr:LoadPlayerRes(isXianShen, showLuoZhuangHair, headFashionId, bodyFashionId, feishengputong,
                                       is150Ghost, anistate, customFacialData)
    if self.m_CurEnumClass and self.m_CurEnumGender and self.m_RO then
        local appearanceData, facialData = self:GetAppearanceProp(isXianShen, showLuoZhuangHair, headFashionId, bodyFashionId, feishengputong,
        is150Ghost, anistate, customFacialData)
        self.m_IsShowLuoZhuangHair = showLuoZhuangHair
        g_ScriptEvent:BroadcastInLua("OnShowLuoZhuangPinchFace", self.m_IsShowLuoZhuangHair)
        local isPreivew = true
        self.m_FxScale = 1
        if self.m_RO.m_KuiLeiRO then
            self.m_RO.m_KuiLeiRO:Destroy()
        end
        self.m_RO:ForceHideKuiLei(EHideKuiLeiStatus.None)
        self:RemoveUIFacialEffectMat()
        self.m_RO.EnableDelayLoad = false
        self:SetWeaponVisible(anistate ~= EnumPinchFaceAniState.PreviewStand01 and
        anistate ~= EnumPinchFaceAniState.PlayStand01)
        LuaPinchFaceMgr:SetIKLookAtTo(self.m_IsUseIKLookAt)
        self.m_RO.Visible = false
        self.m_RO:RemoveMainObject()
        self.m_RO:DestroyAllFX()
        CClientMainPlayer.LoadResource(self.m_RO, appearanceData, true, self.m_FxScale, 0, isPreivew, 0, true, 0, false,
            nil, true, false)
        self.m_RO.FaceId = self.m_PreselectionIndex + 1
        CClientMainPlayer.ChangeSunBurnSkinColor(self.m_RO, self.m_SkinColorIndex)
        self.m_RO:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(ro)
            self:OnROLoaded(ro, facialData, anistate)
        end))
        --万一加载资源报错了，有个保底方案
        if CLoadingWnd.Inst.PinchFaceFx.gameObject.activeSelf then
            RegisterTickOnce(function ()
                if self.m_IsInCreateMode and CLoadingWnd.Inst.PinchFaceFx.gameObject.activeSelf then
                    CLoadingWnd.Inst:HidePinchFaceFx()
                end
            end, 10000)
        end
        
        self:StopBlendShapeTemporarily()
    end
end

function LuaPinchFaceMgr:OnROLoaded(ro, facialData, anistate)
    ro.Visible = true
    self.m_RO:SetFacialData(facialData.FacialData, self.m_PreselectionIndex + 1)
    self:CachePresetFacialStylePaths()
    ro.NeedUpdateAABB = true
    self.m_RO:DoAni("stand01", true, 0, 1, 0.15, true, 1)
    self:PreviewStand(anistate)
    LuaPinchFaceMgr:SetIKLookAtTo(self.m_IsUseIKLookAt)
    CClientMainPlayer.Update150GhostFx(self.m_RO, 0, 0, 0)
    if self.m_IsInCreateMode then
        CLoadingWnd.Inst:HidePinchFaceFx()
    end
    self:SetShengBingMat(ro)
end

function LuaPinchFaceMgr:SetShengBingMat(ro)
    if ro.m_Renderers and (Main.Inst.EngineVersion >= ShaderEx.s_MinVersion or Main.Inst.EngineVersion == -1) then
        local shader1 = ShaderEx.Find("L10_New/Character/Universial")
        local shader2 = ShaderEx.Find("L10_New/Character/Alpha")
		for i = 0, ro.m_Renderers.Count - 1 do
			local mats = ro.m_Renderers[i].materials
			for j = 0, mats.Length - 1 do
                local mat = mats[j]
                if mat and (mat.shader == shader1 or mat.shader == shader2) then
                    mat:DisableKeyword("_USE_OLD_LIGHT")
                end
			end
		end
	end
end

function LuaPinchFaceMgr:OnCurFacialDataUpdate(facialData)
    if facialData == nil then
        facialData = CFacialMgr.GetCustomFacialData(self.m_RO.FaceDnaData, self.m_RO.FacialPartData)
    end
    self.m_CurFacialData = facialData
    if LuaCloudGameFaceMgr:IsCloudGameFace() then
        LuaCloudGameFaceMgr:CacheOfflineData(facialData)
    else
        self:CacheOfflineData()
    end
end

function LuaPinchFaceMgr:PreviewStand(anistate)
    if not self.m_RO then return end
    self:SetWeaponVisible(anistate ~= EnumPinchFaceAniState.PreviewStand01 and
    anistate ~= EnumPinchFaceAniState.PlayStand01)
    self:DoDefaultAni(anistate)
end

EnumPinchFaceAniState = {
    PreviewStand01 = 1,
    PlayStand01 = 2,
    Other = 3,
}

function LuaPinchFaceMgr:DoDefaultAni(anistate)
    if not self.m_RO then return end
    self:HandleExpressionFxOnLeave()
    if anistate == EnumPinchFaceAniState.PreviewStand01 then
        self.m_RO:Preview("stand01", 0)
    elseif anistate ~= EnumPinchFaceAniState.PlayStand01 and self.m_EquipmentData and self.m_EquipmentData.EquipmentId then
        self.m_RO:DoAni(self.m_RO.NewRoleStandAni, true, 0, 1, 0.15, true, 1)
    else
        self.m_RO:DoAni("stand01", true, 0, 1, 0.15, true, 1)
    end
    local aniEffects = CommonDefs.GetComponentsInChildren_Component_Type(self.m_RO.transform, typeof(CAnimtorEffectBase))
    if aniEffects then
        for i = 0, aniEffects.Length - 1 do
            aniEffects[i].m_Animator.enabled = anistate ~= EnumPinchFaceAniState.PreviewStand01
        end
    end
end

function LuaPinchFaceMgr:OnChangeAdvancedHairColor(data)
    if self.m_RO and data and data.ID then
        self.m_AdvancedHairColorIndex = data.ID
        CClientMainPlayer.ChangeHairRanFaJiColor(self.m_RO, data.ID)
    end
end

function LuaPinchFaceMgr:GetEquipmentInfos(isXianShen, is150Ghost)
    local initializationData = self:GetInitializationData()
    local equipInfos = {}
    local shenBingFirstModelId = { 0, 0 }
    local shenBingSecondModelId = { 0, 0 }
    local shenBingTrainLvFxId = { 0, 0 }
    local shenBingRanSeData = { 0, 0 }
    if isXianShen and is150Ghost then
        equipInfos = { 23021004, 23031004 }
        if self.m_CurEnumClass == EnumClass.ZhanKuang then
            equipInfos            = { 0, 0, 0, 23021004, 23031004 }
            shenBingFirstModelId  = { 0, 0, 0, 0, 0 }
            shenBingSecondModelId = { 0, 0, 0, 0, 0 }
            shenBingTrainLvFxId   = { 0, 0, 0, 0, 0 }
            shenBingRanSeData     = { 0, 0, 0, 0, 0 }
        end
    elseif isXianShen and initializationData.ShenBingWaiGuan then
        for i = 0, initializationData.ShenBingWaiGuan.Length - 1 do
            local equipTemplateId = initializationData.ShenBingWaiGuan[i][0]
            local model1Id = initializationData.ShenBingWaiGuan[i][1]
            local model2Id = initializationData.ShenBingWaiGuan[i][2]
            local fxId = initializationData.ShenBingWaiGuan[i][3]
            equipInfos[i + 1] = equipTemplateId
            shenBingFirstModelId[i + 1] = model1Id
            shenBingSecondModelId[i + 1] = model2Id
            shenBingTrainLvFxId[i + 1] = fxId
            shenBingRanSeData[i + 1] = 0
        end
    else
        for i = 0, initializationData.NewRoleEquip3.Length - 1 do
            equipInfos[i + 1] = initializationData.NewRoleEquip3[i]
        end
    end
    if self.m_EquipmentData and self.m_EquipmentData.EquipmentId then
        local weaponIdex = (self.m_CurEnumClass == EnumClass.ZhanKuang) and 2 or 3
        if self.m_EquipmentData.ShenBingOrNot == 1 then
            local index = weaponIdex
            for i = 0, self.m_EquipmentData.EquipmentId.Length - 1, 4 do
                equipInfos[index] = self.m_EquipmentData.EquipmentId[i]
                shenBingFirstModelId[index] = self.m_EquipmentData.EquipmentId[i + 1]
                shenBingSecondModelId[index] = self.m_EquipmentData.EquipmentId[i + 2]
                shenBingTrainLvFxId[index] = self.m_EquipmentData.EquipmentId[i + 3]
                index = index + 1
            end
        elseif self.m_EquipmentData.ShenBingOrNot == 0 then
            local index = weaponIdex
            local len = (self.m_CurEnumClass == EnumClass.ZhanKuang) and 1 or
            (self.m_EquipmentData.EquipmentId.Length - 1)
            for i = 0, len do
                equipInfos[index] = i < self.m_EquipmentData.EquipmentId.Length and self.m_EquipmentData.EquipmentId[i] or
                0
                shenBingFirstModelId[index] = 0
                shenBingSecondModelId[index] = 0
                shenBingTrainLvFxId[index] = 0
                index = index + 1
            end
        end
    end
    return Table2Array(equipInfos, MakeArrayClass(uint)), Table2Array(shenBingFirstModelId, MakeArrayClass(uint)),
        Table2Array(shenBingSecondModelId, MakeArrayClass(uint)),
        Table2Array(shenBingTrainLvFxId, MakeArrayClass(uint)), Table2Array(shenBingRanSeData, MakeArrayClass(uint))
end

LuaPinchFaceMgr.m_IsEnableAutoSaveData = false --是否允许自动保存数据
function LuaPinchFaceMgr:SaveData()
    if not self.m_RO or not self.m_CurEnumClass or not self.m_CurEnumGender then return end
    CFacialMgr.SaveFacialData(self.m_RO, self.m_CurEnumClass, self.m_CurEnumGender, self.m_HairId, self.m_HeadId)
end

function LuaPinchFaceMgr:ClearPartDatasPaths(partDatas)
    if not partDatas then return end
    for i = 0, partDatas.Length - 1 do
        local pd = partDatas[i]
        if pd then
            pd.Path = ""
        end
    end
    if partDatas.Length > 8 then
        local pd = partDatas[8]
        if pd.SliderInfos.Length > 8 then
            local val = pd.SliderInfos[8]
            if val ~= 0 and val ~= 1 then
                pd.SliderInfos[8] = 0 --修复镜像参数不为0的情况
            end
        end
    end
end

function LuaPinchFaceMgr:ImportData()
    CFuxiFaceDNAMgr.Inst:ClearFuxiLogState()
    if self.m_EnableHubQRCode then
        if CommonDefs.IsAndroidPlatform() or CommonDefs.IsIOSPlatform() then
            self:UseMobileQRCodeReader()
            return
        end
        CFacialMgr.ReadFacialHubDataFromTempImage(DelegateFactory.Action(function()
            CResourceMgr.Inst:DoClear(EClearOption.ForceUnloadAsset)
            local data = CFacialMgr.CurFacialSaveData

            if self:CheckIsMatchCurData(data) and EnumToInt(data.Job) ~= EnumToInt(self.m_CurEnumClass) then
                -- 职业不一致 但是可以应用时 弹一条提示
                g_MessageMgr:ShowMessage("PinchFaceHub_Apply_NotSameClass")
            end
            self:OnImportData(data, true)
        end))
        return
    end
    CFacialMgr.ReadFacialDataFromTempImage(DelegateFactory.Action(function()
        local data = CFacialMgr.CurFacialSaveData
        self:OnImportData(data, true)
    end))
end

local s_MinQRSupportVersion = 199645
local s_MinNtQRSupportVersion = 443088 --渠道最低支持unisdk扫码的版本号
local s_MinNeteaseNtQRSupportVersion = 339783--官网最低支持unisdk扫码的版本号
function LuaPinchFaceMgr:UseMobileQRCodeReader()
    if NativeTools.IsSupportRequestPermission() then
        if not NativeTools.HasUserAuthorizedPermission(NativeTools.PermissionNames.CAMERA) then
            NativeTools.RequestUserPermissionOrPromptIfDenied(NativeTools.PermissionNames.CAMERA, nil);
            return
        end
    end
    local engineVersion = Main.Inst.EngineVersion

    if engineVersion > 0 and engineVersion < s_MinQRSupportVersion then
        MessageWndManager.ShowOKMessage(LocalString.GetString("你的客户端内核版本太旧，如需使用该功能，请下载最新的客户端重新安装"), nil)
        return
    end

    if not CLoginMgr.Inst.UniSdk_LoginDone then
        CLoginMgr.Inst:UniSdkLogin(true)
        return
    end

	if SdkU3d.s_EnableNtQRCodeScanner and CommonDefs.IsIOSPlatform() then
		SdkU3d.ntPresentQRCodeScanner()
	elseif SdkU3d.s_EnableNtQRCodeScanner and CommonDefs.IsAndroidPlatform() and CLoginMgr.Inst:IsNetEaseOfficialLogin() and engineVersion>=s_MinNeteaseNtQRSupportVersion then
		if SdkU3d.IsCloudGameCloudClient() then
            SdkU3d.ntPresentQRCodeScanner("",0)
        else
            SdkU3d.ntPresentQRCodeScanner()
        end
    elseif SdkU3d.s_EnableNtQRCodeScanner and CommonDefs.IsAndroidPlatform() and not CLoginMgr.Inst:IsNetEaseOfficialLogin() and engineVersion>=s_MinNtQRSupportVersion then
        SdkU3d.ntPresentQRCodeScanner("",21);
    else
		EasyCodeScanner.Initialize()
		EasyCodeScanner.launchScanner(true, LocalString.GetString("请将取景框对准二维码"), EasyCodeScanner.s_Symbology, true)
	end
end

function LuaPinchFaceMgr:ImportJsonData()
    local data = CFacialMgr.ReadFacialData()
    self:OnImportData(data, true)
end

function LuaPinchFaceMgr:OnImportData(data, isRecord)
    if data then
        self:ClearPartDatasPaths(data.PartDatas)
        if self:CheckIsMatchCurData(data) then
            local lastData = {
                Gender = self.m_CurEnumGender,
                Job = self.m_CurEnumClass,
                PartDatas = self.m_CurFacialData and CFacialMgr.GetPartDatasByCustomFacialData(self.m_CurFacialData, CRenderObject.s_FACE_DNA_LEN) or nil,
                FaceDnaData = self.m_CurFacialData and CFacialMgr.GetFaceDnaDataByCustomFacialData(self.m_CurFacialData, CRenderObject.s_FACE_DNA_LEN) or nil,
                HairId = self.m_HairId,
                HairColorIndex = self.m_HeadId,
                FaceId = self.m_PreselectionIndex + 1,
            }
            if self.m_IsEnableCommandMode and isRecord then
                LuaCommandMgr:Regist(function()
                    self:ApplyData(data, true)
                end, function()
                    self:ApplyData(lastData, true)
                end, "PinchFaceImportData")
                return
            end
            self:ApplyData(data, true)
        else
            g_MessageMgr:ShowMessage("PinchFace_ImportData_NotMatch")
        end
    else
        g_MessageMgr:ShowMessage("PinchFace_ImportData_Failed")
    end
end

function LuaPinchFaceMgr:ApplyData(data, changeHair)
    self:ApplyHeadId(data, changeHair)
    local hairIsChanged = false
    if changeHair and data.Gender == self.m_CurEnumGender then
        hairIsChanged = self.m_HairId ~= data.HairId or self.m_HairId == 9
        self.m_HairId = data.HairId
        if self.m_HairId == 9 then
            self.m_HairId = self:GetPreselectionHairId(self.m_PreselectionIndex)
        end
    end
    local customFacialData = CFacialMgr.GetCustomFacialData(data.FaceDnaData, data.PartDatas)
    self.m_IsFaceChanged = true
    if hairIsChanged then
        if data.FaceDnaData == nil and data.PartDatas == nil then
            customFacialData = nil
            self.m_CurFacialData = nil
        end
        self:SetFashion(self.m_FashionData, EnumPinchFaceAniState.PreviewStand01, self.m_IsShowLuoZhuangHair, customFacialData)
    else
        if data.FaceDnaData == nil and data.PartDatas == nil then
            self.m_CurFacialData = nil
            self.m_RO:SetFacialData(nil, self.m_PreselectionIndex + 1)
        else
            local facialData = CCustomFacialData()
            if self.m_IsShowLuoZhuangHair then
                facialData.HairId = self.m_HairId
            end
            if not customFacialData then
                facialData.FacialData = self.m_CurFacialData
            else
                facialData.FacialData = customFacialData
                self:OnCurFacialDataUpdate(customFacialData)
            end
            self.m_RO:SetFacialData(facialData.FacialData, self.m_PreselectionIndex + 1)
        end
    end
    g_ScriptEvent:BroadcastInLua("OnUpdatePinchFaceMainView")
end

function LuaPinchFaceMgr:ApplyHeadId(data, changeHair)
    local headIdChanged = false
    if changeHair and data.Gender == self.m_CurEnumGender then
        if self.m_IsInCreateMode then
            headIdChanged = self.m_HeadId ~= data.HairColorIndex
            self.m_HeadId = data.HairColorIndex
            if headIdChanged then
                local initializationData = self:GetInitializationData()
                if initializationData then
                    local colorList = {}
                    for i = 0, initializationData.HairColor.Length - 1 do
                        table.insert(colorList, NGUIText.ParseColor24(initializationData.HairColor[i], 0))
                    end
                    local newColor = colorList[self.m_HeadId]
                    self:OnChangeHairColor(newColor, self.m_HeadId)
                end
            end
        end
    end
end

function LuaPinchFaceMgr:CheckIsMatchCurData(data)
    if not data then
        return false
    end
    local initializationData = self:GetInitializationData()
    local newIData = Initialization_Init.GetData(EnumToInt(data.Job) * 100 + EnumToInt(data.Gender))
    if newIData then
        if not initializationData then return false end
        if initializationData.StdModelName ~= newIData.StdModelName then return false end
    end
    local partDatas = data.PartDatas
    if partDatas == nil then return false end
    for i = 0, partDatas.Length - 1 do
        local pd = partDatas[i]
        local styleId = pd.Style
        local styleData = PinchFace_FacialStyle.GetData(styleId)
        local class = EnumToInt(self.m_CurEnumClass)
        if styleData and styleData.Race and not CommonDefs.HashSetContains(styleData.Race, typeof(UInt32), class) then
            if i == 8 then
                pd.Style = 0
            else
                return false
            end
        end
    end
    return true
end

-- 捏脸站数据使用
function LuaPinchFaceMgr:ReadPinchFaceHubData(data, isRecord)
    if data then
        self:OnImportData(data, isRecord)
    end
end

LuaPinchFaceMgr.m_CanCacheOfflineData = false
function LuaPinchFaceMgr:CacheOfflineData()
    PlayerPrefs.SetInt(self.m_IsInModifyMode and "PinchFaceBgColorId2" or "PinchFaceBgColorId", self.m_BgColorData and self.m_BgColorData.ID or 0)
    PlayerPrefs.SetInt(self.m_IsInModifyMode and "PinchFaceFashionId2" or "PinchFaceFashionId", (self.m_FashionData and self.m_FashionData.data) and self.m_FashionData.data.ID or 0)
    PlayerPrefs.SetFloat(self.m_IsInModifyMode and "PinchFacePreselectionX2" or "PinchFacePreselectionX", self.m_PreselectionX)
    PlayerPrefs.SetFloat(self.m_IsInModifyMode and "PinchFacePreselectionY2" or "PinchFacePreselectionY", self.m_PreselectionY)
    PlayerPrefs.SetInt(self.m_IsInModifyMode and "PinchFaceHairId2" or "PinchFaceHairId", self.m_HairId > 0 and self.m_HairId or 0) 
    PlayerPrefs.SetInt(self.m_IsInModifyMode and "PinchFaceHeadId2" or "PinchFaceHeadId", self.m_HeadId > 0 and self.m_HeadId or 0) 
    if not self.m_RO or not self.m_CurEnumClass or not self.m_CurEnumGender or not self.m_CanCacheOfflineData then return end
    print("LuaPinchFaceMgr:CacheOfflineData")
    local path = self:GetCacheDataPath()
    CFacialMgr.CacheFacialData(path, self.m_RO, self.m_CurEnumClass, self.m_CurEnumGender, self.m_HairId, self.m_HeadId, self.m_PreselectionIndex + 1)
    self.m_CanCacheOfflineData = false
end

function LuaPinchFaceMgr:LoadOfflineDataCache()
    local path = self:GetCacheDataPath()
    local data = CFacialMgr.LoadFacialDataCache(path)
    return data
end

function LuaPinchFaceMgr:ClearOfflineData()
    local path = self:GetCacheDataPath()
    CFacialMgr.ClearFacialDataCache(path)
end

function LuaPinchFaceMgr:GetCacheDataPath()
    if self.m_IsInModifyMode then
        return CPlayerDataMgr.CLOBAL_DATA_PATH .. "ModifyModeFacialDataCache.txt"
    end
    return CPlayerDataMgr.CLOBAL_DATA_PATH .. "FacialDataCache.txt"
end

function LuaPinchFaceMgr:AnalyzeCurEvaluation()
    if not self.m_RO or not self.m_CurEnumGender then return end
    local fdna = self.m_RO.FaceDnaData
    local partDatas = self.m_RO.FacialPartData
    local f1 = GetFormula(1561)
    local f2 = GetFormula(1562)
    local f3 = GetFormula(1563)
    local pfdna1 = self.m_PreselectionModelAvatorDataArr[1] and self.m_PreselectionModelAvatorDataArr[1].FaceDNAs
    local pfdna2 = self.m_PreselectionModelAvatorDataArr[2] and self.m_PreselectionModelAvatorDataArr[2].FaceDNAs
    local pfdna3 = self.m_PreselectionModelAvatorDataArr[3] and self.m_PreselectionModelAvatorDataArr[3].FaceDNAs
    local pfdna4 = self.m_PreselectionModelAvatorDataArr[4] and self.m_PreselectionModelAvatorDataArr[4].FaceDNAs
    local pfdnaArr = { pfdna1, pfdna2, pfdna3, pfdna4 }
    if not fdna or not partDatas or not f1 or not f2 or not f3 or not pfdna1 or not pfdna2 or not pfdna3 or not pfdna4 then return end
    local originalVarianceArr = { 0, 0, 0, 0 }
    local facialStyleVarianceArr = { 0, 0, 0, 0 }
    local extremeParametersArr = { 0, 0, 0, 0 }
    local finalVarianceArr = { 0, 0, 0, 0 }
    for i = 0, fdna.Length - 1 do
        local v0 = fdna[i]
        local evaluationData = PinchFace_Evaluation.GetData(i)
        if evaluationData then
            for j = 1, 4 do
                local v1 = pfdnaArr[j][i]
                local weight = self.m_CurEnumGender == EnumGender.Male and evaluationData.EvaLuationWeight or
                evaluationData.FemaleEvaLuationWeight
                originalVarianceArr[j] = originalVarianceArr[j] + f1(nil, nil, { v0, v1, weight })
                if evaluationData.ExtremeParameters and evaluationData.ExtremeParameters.Length == 2 then
                    local a, b = evaluationData.ExtremeParameters[0], evaluationData.ExtremeParameters[1]
                    local sliderData = PinchFace_FaceSliderData.GetData(i)
                    local min, max = self:GetFaceSliderValRange(sliderData)
                    if (v0 < (min + a)) or (v0 > (max - a)) then
                        extremeParametersArr[j] = extremeParametersArr[j] + b
                    end
                end
            end
        end
    end
    for i = 0, partDatas.Length - 1 do
        local pd = partDatas[i]
        local styleId = pd.Style
        local styleData = PinchFace_FacialStyle.GetData(styleId)
        if styleData and styleData.EvaluationAdjust and styleData.EvaluationAdjust.Length == 4 then
            for j = 1, 4 do
                facialStyleVarianceArr[j] = facialStyleVarianceArr[j] + styleData.EvaluationAdjust[j - 1]
            end
        end
    end
    for j = 1, 4 do
        finalVarianceArr[j] = f3(nil, nil, { originalVarianceArr[j], facialStyleVarianceArr[j], extremeParametersArr[j] })
    end
    local defaultIndex = 1
    local minSum = finalVarianceArr[1]
    for j = 2, 4 do
        if finalVarianceArr[j] < minSum then
            minSum = finalVarianceArr[j]
            defaultIndex = j
        end
    end
    local retLevel = f2(nil, nil, { minSum })
    if retLevel > 6 then
        defaultIndex = 5
    end
    local commentDataKey = 0
    PinchFace_EvaluationComment.Foreach(function(key, data)
        if data.GenderLimit == 0 or (data.GenderLimit == 1 and self.m_CurEnumGender == EnumGender.Male) or (data.GenderLimit == 2 and self.m_CurEnumGender == EnumGender.Female) then
            if data.EvaluationSeq == defaultIndex and data.Threshold == retLevel then
                commentDataKey = key
            end
        end
    end)
    print(commentDataKey, defaultIndex, retLevel)
    return PinchFace_EvaluationComment.GetData(commentDataKey)
end

function LuaPinchFaceMgr:SetPinchEnabled(enabled)
    g_ScriptEvent:BroadcastInLua("OnSetPinchFacePinchEnabled", enabled)
end

LuaPinchFaceMgr.m_IsEnableBlurTex = false
function LuaPinchFaceMgr:SetBlurTexVisible(visible)
    self.m_IsEnableBlurTex = visible
    g_ScriptEvent:BroadcastInLua("OnSetPinchFaceBlurTexVisible", visible)
end

LuaPinchFaceMgr.m_IsEnableVideoCapture = true
LuaPinchFaceMgr.m_CaptureMovieTotalTime = 0
LuaPinchFaceMgr.m_MovieCaptureTick = nil

function LuaPinchFaceMgr:CanUseMovieCapture()
    return self.m_IsEnableVideoCapture and VideoRecordMgr.IsPlatformSupport()
end

function LuaPinchFaceMgr:StartCapture(rt)
    if self:CanUseMovieCapture() then
        local filePath = VideoRecordMgr.GetVideoOutputPath()
        local fileName = VideoRecordMgr.GenerateVideoOutputFileName()

        VideoRecordMgr.StartRecordEx(filePath, fileName, 1280, 720, -1, rt)

        VideoRecordMgr.OnRecordFinished = DelegateFactory.Action(function()
            LuaPinchFaceMgr.m_CaptureMovieFilePath = filePath .. "/" .. fileName
            g_ScriptEvent:BroadcastInLua("OnCompletedPinchFaceCaptureMovieFileWriting")
        end)
    end
end

function LuaPinchFaceMgr:StopCapture()
    if self:CanUseMovieCapture() then
        VideoRecordMgr.StopRecord()
    end
end

LuaPinchFaceMgr.m_IsEnableRotateRole = true
LuaPinchFaceMgr.m_IsInPlayMovie = false
function LuaPinchFaceMgr:PlayMovie(useMovieCapture, onFinishedAction, onStartAction, enableRotate, rt)
    local expressionId = self.m_CurEnumGender == EnumGender.Male and
    PinchFace_Setting.GetData().PreviewVideoExression_Male or PinchFace_Setting.GetData().PreviewVideoExression_Female
    local data = Expression_Define.GetData(expressionId)
    local previewVideoTime = PinchFace_Setting.GetData().PreviewVideoTime
    if not data or previewVideoTime.Length < 5 or not self.m_RO then
        if onFinishedAction then
            onFinishedAction()
        end
        return
    end
    self:SetIKLookAtTo(false)
    self:SetPinchEnabled(false)
    self:HandleExpressionFxOnLeave()
    self.m_CaptureMovieTotalTime = 0
    for i = 0, (math.min(5, previewVideoTime.Length) - 1) do
        self.m_CaptureMovieTotalTime = self.m_CaptureMovieTotalTime + previewVideoTime[i]
    end
    local t = self.m_RO.transform
    t.localEulerAngles = Vector3(0, 180, 0)
    self.m_IsEnableRotateRole = enableRotate
    self.m_RO:Preview("stand01", 0)
    self.m_IsInPlayMovie = true
    if onStartAction then
        onStartAction()
    end
    if useMovieCapture and self:CanUseMovieCapture() then
        self:StopCapture()
        self:StartCapture(rt)
    end

    CPinchFaceCinemachineCtrlMgr.Inst:ShowCam(2, true)
    CPinchFaceCinemachineCtrlMgr.Inst:ShowCamTween(2, 1, previewVideoTime[0])
    self.m_RO:StopBlendShape()
    self:CancelMovieCaptureTick()
    self.m_MovieCaptureTick = RegisterTickOnce(function()
        self.m_RO:DoBlendShape("yindie01", 0, 1)
        CClientObject.ShowExpressionAction(self.m_RO, expressionId, EnumToInt(self.m_CurEnumGender))
        CPinchFaceCinemachineCtrlMgr.Inst:ShowCamTween(1, 5, previewVideoTime[2])

        self:CancelMovieCaptureTick()
        self.m_MovieCaptureTick = RegisterTickOnce(function()
            CPinchFaceCinemachineCtrlMgr.Inst:ShowCamTween(5, 4, previewVideoTime[3])

            self:CancelMovieCaptureTick()
            self.m_MovieCaptureTick = RegisterTickOnce(function()
                self:StopCapture()
                self.m_IsInPlayMovie = false
                if onFinishedAction then
                    onFinishedAction()
                end
                self.m_RO:RemoveFX(data.AniName)
                self.m_IsEnableRotateRole = true
                self:SetPinchEnabled(true)
                self.m_RO:DoRandomBlendShape()
            end, (previewVideoTime[3] + previewVideoTime[4]) * 1000)
        end, (previewVideoTime[2]) * 1000)
    end, (previewVideoTime[0] + previewVideoTime[1]) * 1000)
end

function LuaPinchFaceMgr:StopPlayMovie()
    if self.m_IsInPlayMovie then
        if self.m_Capture then
            self.m_Capture.CompletedFileWritingAction = nil
        end
        self:StopCapture()
        self.m_IsInPlayMovie = false
        self:CancelMovieCaptureTick()
        if self.m_RO then
            self.m_RO.transform.localEulerAngles = Vector3(0, 180, 0)
        end
        self.m_IsEnableRotateRole = true
        self:DoDefaultAni()
        self:SetPinchEnabled(true)
        if CPinchFaceCinemachineCtrlMgr.Inst then
            CPinchFaceCinemachineCtrlMgr.Inst:ShowCam(2, true)
        end
        local expressionId = self.m_CurEnumClass == EnumGender.Male and
        PinchFace_Setting.GetData().PreviewVideoExression_Male or
        PinchFace_Setting.GetData().PreviewVideoExression_Female
        local data = Expression_Define.GetData(expressionId)
        self.m_RO:RemoveFX(data.AniName)
    end
end

function LuaPinchFaceMgr:CancelMovieCaptureTick()
    if self.m_MovieCaptureTick then
        UnRegisterTick(self.m_MovieCaptureTick)
        self.m_MovieCaptureTick = nil
    end
end

function LuaPinchFaceMgr:ShowExpressionAction(ExpressionID, percent)
    self:SetIKLookAtTo(false)
    local data = Expression_Define.GetData(ExpressionID)
    if data == nil or self.m_RO == nil or percent == nil then return 0 end
    self:HandleExpressionFxOnEnter(ExpressionID)
    local len = self:GetExpressionAnimationTime(ExpressionID)
    local time = percent * len
    local animationName = data.AniName
    local curAniLen = self.m_RO:GetAniLength(animationName)
    while (time > curAniLen) do
        time = time - curAniLen
        local nextData = Expression_Define.GetData(data.NextExpressionID)
        if nextData == nil then return end
        ExpressionID = data.NextExpressionID
        animationName = nextData.AniName
        curAniLen = self.m_RO:GetAniLength(animationName)
        data = nextData
    end
    self.m_RO:SetWeaponVisible(EWeaponVisibleReason.Expression, false)
    self.m_RO:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(renderObj)
        self.m_RO:Preview(animationName, time)
        if percent >= 1 then
            self:HandleExpressionFxOnLeave()
        end
    end))
end

function LuaPinchFaceMgr:GetExpressionAnimationTime(ExpressionID)
    local data = Expression_Define.GetData(ExpressionID)
    local len = 0
    if self.m_RO and data then
        len = self.m_RO:GetAniLength(data.AniName)
    end
    return len
end

LuaPinchFaceMgr.m_HandleExpressionId = 0
function LuaPinchFaceMgr:HandleExpressionFxOnEnter(expressionId)
    if expressionId ~= self.m_HandleExpressionId then
        self:HandleExpressionFxOnLeave()
    end
    local data = Expression_Define.GetData(expressionId)
    if self.m_RO and data then
        local gender = self.m_CurEnumGender
        if not gender then
            gender = EnumGender.Undefined
        end
        local fxids = GetExpressionDefineFx(data, EnumToInt(gender))
        for i = 1, #fxids do
            local key = fxids[i]
            if not self.m_RO:ContainFx(key) then
                local fx = CEffectMgr.Inst:AddObjectFX(key, self.m_RO, 0, 1, 1, nil, false, EnumWarnFXType.None,
                Vector3.zero, Vector3.zero, nil)
                self.m_RO:AddFX(key, fx)
            end
        end
    end
    self.m_HandleExpressionId = expressionId
end

function LuaPinchFaceMgr:HandleExpressionFxOnLeave()
    local data = Expression_Define.GetData(self.m_HandleExpressionId)
    if self.m_RO and data then
        local gender = self.m_CurEnumGender
        if not gender then
            gender = EnumGender.Undefined
        end
        local fxids = GetExpressionDefineFx(data, EnumToInt(gender))
        for i = 1, #fxids do
            self.m_RO:RemoveFX(fxids[i])
        end

        self.m_RO:RemoveFX(data.AniName)
    end
    self.m_HandleExpressionId = 0
end

function LuaPinchFaceMgr:ResetHairColor()
    local initializationData = self:GetInitializationData()
    if initializationData then
        local colorList = {}
        for i = 0, initializationData.HairColor.Length - 1 do
            table.insert(colorList, NGUIText.ParseColor24(initializationData.HairColor[i], 0))
        end
        local lastColor = colorList[self.m_HeadId + 1]
        local newColor = colorList[1]
        self:ChangeHairColor(lastColor, newColor, 0)
    end
end

LuaPinchFaceMgr.m_FuxiRandomOutTimeTick = nil

function LuaPinchFaceMgr:RequestAndApplyFuxiRandomData()
    if self.m_FuxiRandomOutTimeTick ~= nil then
        g_MessageMgr:ShowMessage("PinchFace_FuxiRandomProcessing")
        return
    end
    self:StartOutTimeTick(5000)
    CFuxiFaceDNAMgr.m_RO = LuaPinchFaceMgr.m_RO
    CFuxiFaceDNAMgr.m_Class = LuaPinchFaceMgr.m_CurEnumClass
    CFuxiFaceDNAMgr.m_Gender = LuaPinchFaceMgr.m_CurEnumGender
    CFuxiFaceDNAMgr.Inst:PostRequestFuxiRandomDNAData(DelegateFactory.Action_int(function (code)
        if code == 200 then
            local data = CFacialMgr.CurFacialSaveData
            if data then
                self:OnImportData(data, true)
            end
        else
            g_MessageMgr:ShowMessage("PinchFace_FuxiRandomFailed")
        end
        self:EndOutTimeTick()
    end))
end

function LuaPinchFaceMgr:StartOutTimeTick(time)
    UnRegisterTick(self.m_FuxiRandomOutTimeTick)
    self.m_FuxiRandomOutTimeTick = RegisterTickOnce(function()
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("与服务器连接超时，请稍后重试！"))
        self.m_FuxiRandomOutTimeTick = nil
    end, time)
end

function LuaPinchFaceMgr:EndOutTimeTick()
    UnRegisterTick(self.m_FuxiRandomOutTimeTick)
    self.m_FuxiRandomOutTimeTick = nil
end

function LuaPinchFaceMgr:UploadWork(roleName)
    if not CPinchFaceCinemachineCtrlMgr.Inst then return end
    local camera = CPinchFaceCinemachineCtrlMgr.Inst.m_Cam

    local resizeTex = CUICommonDef.DoCaptureWithClip(camera, 4/3)
    local byte = nil
    -- 编码时候 降低质量
    if resizeTex then
        byte = ImageConversion.EncodeToJPG(resizeTex, 50)
        GameObject.Destroy(resizeTex)
    end

    local data = nil
    if LuaPinchFaceMgr.m_RO ~= nil then
        data = CFacialMgr.GenerateHubData(LuaPinchFaceMgr.m_RO, EnumToInt(LuaPinchFaceMgr.m_CurEnumClass), EnumToInt(LuaPinchFaceMgr.m_CurEnumGender),
            LuaPinchFaceMgr.m_HairId, LuaPinchFaceMgr.m_HeadId)
    end

    local title = LocalString.GetString("历史面容")
    local desc = LocalString.GetString("曾用面容数据")
    -- 私有
    local vis = 1

    if byte and data then
        CPinchFaceHubMgr.Inst:UploadWork(byte, data, title, desc, roleName, EnumToInt(LuaPinchFaceMgr.m_CurEnumGender),
            vis, 0, EnumToInt(LuaPinchFaceMgr.m_CurEnumClass), nil, nil)
    end
end

-- 用于UI捏脸时暂停表情，过会再恢复
function LuaPinchFaceMgr:StopBlendShapeTemporarily()
    if self.m_RO then
        -- 把表情停了
        self.m_RO:StopBlendShape()
        -- 里面有个tick，不会立刻开始做表情，以此实现UI不捏脸一段时间后再开始做表情
        self.m_RO:DoRandomBlendShape()
    end
end
