local CUITexture = import "L10.UI.CUITexture"
local UIProgressBar = import "UIProgressBar"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"

LuaGuardZongMenWnd = class()

RegistChildComponent(LuaGuardZongMenWnd, "MonsterTexture", "MonsterTexture", CUITexture)
RegistChildComponent(LuaGuardZongMenWnd, "Progress", "Progress", UIProgressBar)
RegistChildComponent(LuaGuardZongMenWnd, "HpLabel", "HpLabel", UILabel)
RegistChildComponent(LuaGuardZongMenWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaGuardZongMenWnd, "SixSectBottomViewGrid", "SixSectBottomViewGrid", UIGrid)
RegistChildComponent(LuaGuardZongMenWnd, "RankButton1", "RankButton1", GameObject)
RegistChildComponent(LuaGuardZongMenWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaGuardZongMenWnd, "MonsterFailedSign", "MonsterFailedSign", GameObject)
RegistChildComponent(LuaGuardZongMenWnd, "DestroyStateSign", "DestroyStateSign", GameObject)
RegistChildComponent(LuaGuardZongMenWnd, "TipButton", "TipButton", GameObject)

function LuaGuardZongMenWnd:Start()
    local t = LuaZongMenMgr.m_SMSWQueryPlayInfoResultData
    local data = ShiMenShouWei_Day.GetData(t.day)
    self.Template:SetActive(false)
    Extensions.RemoveAllChildren(self.SixSectBottomViewGrid.transform)
    self:InitTeleports(data)
    self.SixSectBottomViewGrid:Reposition()
    UIEventListener.Get(self.RankButton1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        Gac2Gas.SMSW_QueryRank()
    end)
    UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        g_MessageMgr:ShowMessage("GuardZongMenWnd_ReadMe")
    end)
    local monsterId = data.BossStage3[0]
    local monsterData = Monster_Monster.GetData(monsterId)
    self.NameLabel.text = monsterData.Name
    self.MonsterTexture:LoadNPCPortrait(monsterData.HeadIcon)
    self.HpLabel.text = math.modf(t.leftHp)
    self:UpdateState()
end

function LuaGuardZongMenWnd:InitTeleports(data)
    -- ShiMenShouWei_Ruin.ForeachKey(function (key)
    --     LuaMiniMapMgr:ChangePopWndWorldMapInfo(key)
    -- end)
    local list, widthHeightInfo = self:InitRuinInfo(data)
    for _, t in pairs(list) do
        local sceneName = t.sceneName
        local hasScene = t.hasScene
        local key = t.key
        local rootName = sceneName == "kunlunshan" and "kunlunpai" or sceneName
        local posunSceneName = ""
        if LuaMiniMapMgr.m_PopWndWorldMapReplaceInfoDict and LuaMiniMapMgr.m_PopWndWorldMapReplaceInfoDict[rootName] then
            local Scenes = data.Scenes
            local data = ShiMenShouWei_Scene.GetData(key)
            if data then
                hasScene = CommonDefs.HashSetContains(Scenes, typeof(UInt32), data.TeleportRedirect)
                if hasScene then
                    posunSceneName = data.ReplaceIcons
                end
            end
        end
        local obj = NGUITools.AddChild(self.SixSectBottomViewGrid.gameObject, self.Template)
        obj:SetActive(true)
        local map = obj.transform:Find("Map"):GetComponent(typeof(UISprite))
        map.width = widthHeightInfo[sceneName][1]
        map.height = widthHeightInfo[sceneName][2]
        map.spriteName = (posunSceneName ~= "" and posunSceneName or sceneName).."_building"
        obj.transform:Find("Texture"):GetComponent(typeof(UISprite)).spriteName = sceneName
        obj.transform:Find("FindNpcLabel").gameObject:SetActive(hasScene)
        obj.transform:Find("SafeLabel").gameObject:SetActive(not hasScene)
        if hasScene then
            obj.transform:Find("Map/Fx"):GetComponent(typeof(CUIFx)):LoadFx("fx/ui/prefab/UI_shimenruqin.prefab")
        end
        local sceneId = key
        if hasScene then
            UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (go)
                self:LevelTeleport(sceneName,sceneId)
            end)
        end
    end
end

function LuaGuardZongMenWnd:InitRuinInfo(data)
    local list = {}
    ShiMenShouWei_Ruin.ForeachKey(function (key)
        local hasScene = CommonDefs.HashSetContains(data.Scenes, typeof(UInt32), key)
        local sceneName = PublicMap_MiniMapRes.GetData(key).ResName
        table.insert(list, {hasScene = hasScene, sceneName = sceneName, key = key})
        
    end)
    table.sort(list,function(a,b)
        if a.hasScene and not b.hasScene then 
            return true
        elseif b.hasScene and not a.hasScene then
            return false
        else 
            return a.key < b.key
        end
    end)
    local widthHeightInfo = {}
    widthHeightInfo["shenjiying"] ={117,86}
    widthHeightInfo["wanyaogong"] ={79,95}
    widthHeightInfo["xiaoyaoguan"] ={108,110}
    widthHeightInfo["kunlunshan"] ={64,110}
    widthHeightInfo["tiangongge"] ={106,110}
    widthHeightInfo["jinshajing"] ={120,92}
    return list, widthHeightInfo
end

function LuaGuardZongMenWnd:LevelTeleport(sceneName,sceneId)
    local rootName = sceneName == "kunlunshan" and "kunlunpai" or sceneName
    if LuaMiniMapMgr.m_PopWndWorldMapReplaceInfoDict and LuaMiniMapMgr.m_PopWndWorldMapReplaceInfoDict[rootName] then
        local data = ShiMenShouWei_Scene.GetData(sceneId)
        if data then
            Gac2Gas.LevelTeleport(data.TeleportRedirect)
            return
        end
    end
    Gac2Gas.LevelTeleport(sceneId)
end

function LuaGuardZongMenWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSMSWBossInfoUpdate", self, "UpdateState")
    g_ScriptEvent:AddListener("RefreshShiMenRuQinTaskViewVisibleStatus", self, "UpdateState")
end

function LuaGuardZongMenWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSMSWBossInfoUpdate", self, "UpdateState")
    g_ScriptEvent:RemoveListener("RefreshShiMenRuQinTaskViewVisibleStatus", self, "UpdateState")
end

function LuaGuardZongMenWnd:UpdateState()
    local t = LuaZongMenMgr.m_SMSWQueryPlayInfoResultData
    if t and t.leftHp and t.maxHp and t.maxHp ~= 0 then
		self.Progress.value = t.leftHp / t.maxHp
	end
    self.Progress.gameObject:SetActive(LuaZongMenMgr.m_SMSW_PlayState ~= EnumSMSWPlayState.Reward and LuaZongMenMgr.m_SMSW_PlayState ~= EnumSMSWPlayState.Destroy and LuaZongMenMgr.m_SMSW_PlayState ~= EnumSMSWPlayState.Delay)
    self.MonsterFailedSign.gameObject:SetActive(LuaZongMenMgr.m_SMSW_PlayState == EnumSMSWPlayState.Reward or LuaZongMenMgr.m_SMSW_PlayState == EnumSMSWPlayState.Destroy or LuaZongMenMgr.m_SMSW_PlayState == EnumSMSWPlayState.Delay)
    self.DestroyStateSign.gameObject:SetActive(false)
end