-- Auto Generated!!
local CPreciousMgr = import "L10.Game.CPreciousMgr"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local PreciousEquipment_Setting = import "L10.Game.PreciousEquipment_Setting"
CPreciousMgr.m_hookIsPreciousChuanjiabaoInfo = function (this, wordInfo) 
	local star = wordInfo.WashedStar and wordInfo.WashedStar > 0 and wordInfo.WashedStar or wordInfo.Star
    if star >= 4 and wordInfo.WordNum >= CPreciousMgr.PreciouseZhushabiMinWordNum then
        return true
    end

    if star >= 6 then
        return true
    end

    if not CSwitchMgr.IsEnableSpecialPriciousZhushabi then
        return false
    end

    local isNeedCalculate = this:IsNeedCalculateZhushabiWordScore(wordInfo)

    local score
    if not isNeedCalculate then
        score = wordInfo.WordPreciousScore
    else
        score = this:GetZhushabiWordScore(wordInfo)
    end
    local setting = PreciousEquipment_Setting.GetData()
    if star == 3 and score >= setting.Precious3StarZhushabiScore then
        return true
    end
    if star == 4 and score >= setting.Precious4StarZhushabiScore then
        return true
    end
    if star == 5 and score >= setting.Precious5StarZhushabiScore then
        return true
    end
    return false
end
CPreciousMgr.m_hookIsPreciousZhushabiWordInfo = function (this, wordInfo) 
	local star = wordInfo.WashedStar and wordInfo.WashedStar > 0 and wordInfo.WashedStar or wordInfo.Star
    if star >= 4 and wordInfo.WordNum >= CPreciousMgr.PreciouseZhushabiMinWordNum then
        return true
    end

    if not CSwitchMgr.IsEnableSpecialPriciousZhushabi then
        return false
    end

    local isNeedCalculate = this:IsNeedCalculateZhushabiWordScore(wordInfo)

    local score
    --if (!isNeedCalculate)
    --    score = wordInfo.WordPreciousScore;
    --else
    score = this:GetZhushabiWordScore(wordInfo)
    local setting = PreciousEquipment_Setting.GetData()
    if star == 3 and score >= setting.Precious3StarZhushabiScore then
        return true
    end
    if star == 4 and score >= setting.Precious4StarZhushabiScore then
        return true
    end
    if star == 5 and score >= setting.Precious5StarZhushabiScore then
        return true
    end
    return false
end

