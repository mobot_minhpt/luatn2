local CPaintTexture=import "L10.UI.CPaintTexture"
local Input = import "UnityEngine.Input"
CLuaHuaZhuWnd=class()
RegistClassMember(CLuaHuaZhuWnd,"m_OkButton")
RegistClassMember(CLuaHuaZhuWnd,"m_PaintTexture")
RegistClassMember(CLuaHuaZhuWnd,"m_PaintOver")


function CLuaHuaZhuWnd:Init()
    self.m_PaintOver=false
    self.m_OkButton = FindChild(self.transform,"OkButton").gameObject
    UIEventListener.Get(self.m_OkButton).onClick=DelegateFactory.VoidDelegate(function(go)
        if self.m_PaintOver then
            Gac2Gas.FinishHuaZhuZiPicTask(22202154)
        else
            g_MessageMgr:ShowMessage("HuaZhu_NotFinish")
        end
    end)

    local brush = FindChild(self.transform,"DrawMoveNode")

    self.m_PaintTexture=FindChild(self.transform,"PaintTexture"):GetComponent(typeof(CPaintTexture))
    self.m_PaintTexture.m_TouchUpdate_Lua=function(cmp)
        --更新画笔位置
        -- local mouse = Input.mousePosition
        local posTo = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition )
        local relLocalPos = brush.transform.parent:InverseTransformPoint(posTo)
        brush.localPosition = relLocalPos
    end

    self.m_PaintTexture.OnPaintOver = DelegateFactory.Action(function()
        self.m_PaintOver=true
        brush.gameObject:SetActive(false)
    end)


    local tf=FindChild(self.m_PaintTexture.transform,"Texture")
    local count = tf.childCount
    for i=1,count do
        local go=tf:GetChild(i-1).gameObject
        self.m_PaintTexture:RegisterTrigger(go)
    end

end

function CLuaHuaZhuWnd:OnEnable()
	g_ScriptEvent:AddListener("FinishTask", self, "OnFinishTask")
    
end

function CLuaHuaZhuWnd:OnDisable()
	g_ScriptEvent:RemoveListener("FinishTask", self, "OnFinishTask")

end
function CLuaHuaZhuWnd:OnFinishTask(args)
    local taskId = tonumber(args[0])
    if 22202154==taskId then
        EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {"Fx/UI/Prefab/UI_wancheng.prefab"})
        CUIManager.CloseUI(CLuaUIResources.HuaZhuWnd)
    end
end
