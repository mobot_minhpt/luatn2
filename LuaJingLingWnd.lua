local CContactAssistantMgr = import "L10.Game.CContactAssistantMgr"
local CExpertTeamMgr = import "L10.Game.CExpertTeamMgr"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CRecordingInfoMgr = import "L10.UI.CRecordingInfoMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumQueryJingLingContext = import "L10.Game.EnumQueryJingLingContext"
local EnumRecordContext = import "L10.Game.EnumRecordContext"
local EnumVoicePlatform = import "L10.Game.EnumVoicePlatform"
local JingLingMessageExtraInfoKey = import "L10.Game.JingLingMessageExtraInfoKey"
local LocalString = import "LocalString"
local Object = import "System.Object"
local UILabel = import "UILabel"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CBaseWnd = import "L10.UI.CBaseWnd"
local CJingLingChatView = import "L10.UI.CJingLingChatView"
local CJingLingAssistView = import "L10.UI.CJingLingAssistView"
local UITabBar = import "L10.UI.UITabBar"
local CJingLingHistoryMenu = import "L10.UI.CJingLingHistoryMenu"

local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local DelegateFactory = import "DelegateFactory"
local EventDelegate = import "EventDelegate"
local NativeTools = import "L10.Engine.NativeTools"
local TouchScreenKeyboard = import "UnityEngine.TouchScreenKeyboard"

LuaJingLingWnd = class()

RegistClassMember(LuaJingLingWnd, "m_HotspotView")          --精灵热点
RegistClassMember(LuaJingLingWnd, "m_NewHotspotView")       --新精灵热点(暂时只在国服用)
RegistClassMember(LuaJingLingWnd, "m_RecommendationView")   --精灵推荐
RegistClassMember(LuaJingLingWnd, "m_ChatView")             --精灵问答    
RegistClassMember(LuaJingLingWnd, "m_AssistView")           --精灵助手

RegistClassMember(LuaJingLingWnd, "m_TabBar")               --子页面切换TabBar
RegistClassMember(LuaJingLingWnd, "m_CloseButton")          --关闭按钮
RegistClassMember(LuaJingLingWnd, "m_TitleLabel")           --标题
RegistClassMember(LuaJingLingWnd, "m_ChatInputRoot")        --文本输入区域根节点
RegistClassMember(LuaJingLingWnd, "m_ChatInput")            --文本输入框
RegistClassMember(LuaJingLingWnd, "m_SendButton")           --发送按钮
RegistClassMember(LuaJingLingWnd, "m_VoiceButton")          --语音输入按钮
RegistClassMember(LuaJingLingWnd, "m_HistoryButton")        --提问历史按钮
RegistClassMember(LuaJingLingWnd, "m_HistoryMenu")          --提问历史菜单
RegistClassMember(LuaJingLingWnd, "m_AskExpertButton")      --问倩女知道按钮

RegistClassMember(LuaJingLingWnd, "m_LastVoiceText")        --最近一次记录的语音翻译文本
RegistClassMember(LuaJingLingWnd, "m_ContactAssistantIndex") --精灵助手Tab Index
RegistClassMember(LuaJingLingWnd, "m_TotalTabCount")        --Tab总数
RegistClassMember(LuaJingLingWnd, "m_OnTabChangeDelegate")  --TabChange回调代理，用于在enable时持有引用，disable时解除引用

RegistClassMember(LuaJingLingWnd, "m_HotWordsMenu")                --提问热词菜单
RegistClassMember(LuaJingLingWnd, "m_HotWordsMenuInitialPosY")     --热词菜单初始位置
RegistClassMember(LuaJingLingWnd, "m_lastInputisSelected")         --Input输入状态
RegistClassMember(LuaJingLingWnd, "m_HotWordsMenuPosOffset")       --热词框位置偏移

RegistClassMember(LuaJingLingWnd, "m_KefuButton")   -- 客服按钮（迭代后客户按钮不参与tab管理，点击后拉起网页）

function LuaJingLingWnd:Awake()
    CJingLingMgr.Inst.m_JingLingWndOpenTime = CServerTimeMgr.Inst.timeStamp
    self.m_ContactAssistantIndex = 3
    self.m_TotalTabCount = 3
    self:BindComponents()
end

function LuaJingLingWnd:BindComponents()
    self.m_HotspotView = self.transform:Find("Anchor/HotspotView").gameObject
    self.m_NewHotspotView = self.transform:Find("Anchor/NewHotspotView").gameObject
    self.m_RecommendationView = self.transform:Find("Anchor/RecommendationView").gameObject
    self.m_ChatView = self.transform:Find("Anchor/ChatView"):GetComponent(typeof(CJingLingChatView))
    self.m_KefuButton = self.transform:Find("Anchor/KefuButton").gameObject
    --self.m_AssistView = self.transform:Find("Anchor/AssistView"):GetComponent(typeof(CJingLingAssistView))

    self.m_TabBar = self.transform:GetComponent(typeof(UITabBar))
    self.m_CloseButton = self.transform:Find("Wnd_Bg_Primary_Tab/CloseButton").gameObject
    self.m_TitleLabel = self.transform:Find("Wnd_Bg_Primary_Tab/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_ChatInputRoot = self.transform:Find("Anchor/ChatInput").gameObject
    self.m_ChatInput = self.transform:Find("Anchor/ChatInput/Input"):GetComponent(typeof(UIInput))
    self.m_SendButton = self.transform:Find("Anchor/ChatInput/SendBtn").gameObject
    self.m_VoiceButton = self.transform:Find("Anchor/ChatInput/VoiceBtn").gameObject
    self.m_HistoryButton = self.transform:Find("Anchor/ChatInput/HistoryBtn").gameObject
    self.m_HistoryMenu = self.transform:Find("Anchor/ChatInput/QuestionHistory"):GetComponent(typeof(CJingLingHistoryMenu))
    self.m_AskExpertButton = self.transform:Find("Anchor/ChatInput/AskExpertBtn").gameObject
    self.m_HotWordsMenu = self.transform:Find("Anchor/ChatInput/HotWordsMenu"):GetComponent(typeof(CCommonLuaScript))
    if self.m_HotWordsMenu then
        self.m_HotWordsMenu:Init({})
    end
end

function LuaJingLingWnd:Init()

    Gac2Gas.OpenJingLingWnd()

    CommonDefs.AddOnClickListener(self.m_CloseButton, DelegateFactory.Action_GameObject(function(go) self:OnCloseButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_SendButton, DelegateFactory.Action_GameObject(function(go) self:OnSendButtonClick(go) end), false)
    CommonDefs.AddOnPressListener(self.m_VoiceButton, DelegateFactory.Action_GameObject_bool(function(go, pressed) self:OnVoiceButtonPress(go, pressed) end), false)
    CommonDefs.AddOnClickListener(self.m_HistoryButton, DelegateFactory.Action_GameObject(function(go) self:OnHistoryButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_AskExpertButton, DelegateFactory.Action_GameObject(function(go) self:OpenExpertWnd(go) end), false)

    CommonDefs.ListAdd(self.m_ChatInput.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        self:OnChatInputChange()
    end)))

    CommonDefs.AddOnClickListener(self.m_KefuButton, DelegateFactory.Action_GameObject(function(go) self:OnKefuButtonClick(go) end), false)

    
    self.m_ChatInput.onReturnKeyPressed = DelegateFactory.Action_GameObject(function(go) self:OnSendButtonClick(go) end)
    self.m_HistoryMenu.OnItemSelected =  DelegateFactory.Action_int(function(index) self:OnHistoryItemSelected(index) end)
    --self.m_AssistView.OnItemClickDelegate = DelegateFactory.Action_string_bool(function(text, needEvaluate) self:OnAssitantItemClick(text, needEvaluate) end)

    if CExpertTeamMgr.Inst.ExistsNewAnswer then
        local redpoint = self.m_AskExpertButton.transform:Find("RedPoint").gameObject
        if redpoint ~= nil then
            redpoint:SetActive(true)
        end
    end
    
    self.m_KefuButton:SetActive(CContactAssistantMgr.Inst:ContactAssitantOpenCheck())
    if CJingLingMgr.Inst.TabIndex ~= self.m_ContactAssistantIndex then
        if CJingLingMgr.Inst.TabIndex >= 0 and CJingLingMgr.Inst.TabIndex < self.m_TotalTabCount then
            self.m_TabBar:ChangeTab(CJingLingMgr.Inst.TabIndex, false)
        else
            self.m_TabBar:ChangeTab(0, false)
        end
    else
        self.m_TabBar:ChangeTab(0, false)
        self:OnKefuButtonClick(nil)
    end

    if CommonDefs.IS_HMT_CLIENT then
        --self.m_TabBar:GetTabGoByIndex(self.m_ContactAssistantIndex):SetActive(false)
        self.m_KefuButton:SetActive(false)
        self.m_AskExpertButton:SetActive(false)
    end
    
    self.m_HistoryMenu.gameObject:SetActive(false)
    self.m_HotWordsMenuInitialPosY = self.m_HotWordsMenu.transform.localPosition.y
    self.m_HotWordsMenuPosOffset = self:CalulateHotWordsMenuPosOffset()
    LuaJingLingMgr:QuerySpriteGameYWToken()
    LuaJingLingMgr:RequestRecommendedHotWords() -- --获得探索热词（仅开启时一次）
end

function LuaJingLingWnd:OnEnable()
    self.m_OnTabChangeDelegate = DelegateFactory.Action_GameObject_int(function(go, index)
        self:OnTabChange(go, index)
    end)
    self.m_TabBar.OnTabChange = CommonDefs.CombineListner_Action_GameObject_int(self.m_TabBar.OnTabChange, self.m_OnTabChangeDelegate, true)
    g_ScriptEvent:AddListener("OnJingLingProcessTextInputReady", self, "OnJingLingProcessTextInputReady")
    g_ScriptEvent:AddListener("OnJingLingHotspotItemClick", self, "OnJingLingHotspotItemClick")
    g_ScriptEvent:AddListener("OnJingLingHotWordsItemClick", self, "OnJingLingHotWordsItemClick")
    g_ScriptEvent:AddListener("OnJingLingHotWordsReady", self, "OnJingLingHotWordsReady")
end

function LuaJingLingWnd:OnDisable()
    self.m_TabBar.OnTabChange = CommonDefs.CombineListner_Action_GameObject_int(self.m_TabBar.OnTabChange, self.m_OnTabChangeDelegate, false)
    g_ScriptEvent:RemoveListener("OnJingLingProcessTextInputReady", self, "OnJingLingProcessTextInputReady")
    g_ScriptEvent:RemoveListener("OnJingLingHotspotItemClick", self, "OnJingLingHotspotItemClick")
    g_ScriptEvent:RemoveListener("OnJingLingHotWordsItemClick", self, "OnJingLingHotWordsItemClick")
    g_ScriptEvent:RemoveListener("OnJingLingHotWordsReady", self, "OnJingLingHotWordsReady")
end

function LuaJingLingWnd:OnKefuButtonClick(go)
    Gac2Gas.QuerySmartGMToken()
end

function LuaJingLingWnd:OnCloseButtonClick(go)
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaJingLingWnd:Update()
    if not CommonDefs.IsAndroidPlatform() then
        self:OnChatInputSelectedChange()
        if CommonDefs.IsIOSPlatform() then
            self:OnChatInputPosChange()
        end
    end
end

--点击发送按钮
function LuaJingLingWnd:OnSendButtonClick(go)
    local isVoice = (self.m_ChatInput.value ~= nil and self.m_ChatInput.value == self.m_LastVoiceText)
    self:ChatJingLingWithFilterOption(self.m_ChatInput.value, false, isVoice, true, true, "manual", true)
    self.m_ChatInput.value = nil
    self.m_LastVoiceText = nil
    -- 将输入框设为未选中状态，强制关闭热词框，避免回车无法正确显示
    self.m_ChatInput.isSelected = false
    self.m_HotWordsMenu.gameObject:SetActive(false)    
end
--按下语音输入按钮
function LuaJingLingWnd:OnVoiceButtonPress(go, pressed)
    if pressed then
        CRecordingInfoMgr.ShowRecordingWnd(EnumRecordContext.JingLing, EChatPanel.Undefined, go, EnumVoicePlatform.Default, false)
    else
        CRecordingInfoMgr.CloseRecordingWnd(not go:Equals(CUICommonDef.SelectedUI))
    end
end
--点击历史按钮
function LuaJingLingWnd:OnHistoryButtonClick(go)
    if self.m_HistoryMenu.gameObject.activeSelf then
        self.m_HistoryMenu.gameObject:SetActive(false)
    else
        self.m_HistoryMenu.gameObject:SetActive(true)
        self.m_HistoryMenu:Init()
    end
end
--点击热点按钮
function LuaJingLingWnd:OnJingLingHotspotItemClick(text)
    self:ChatJingLingWithFilterOption(text, true, false, false, true, "hp", false) --不过滤
end
--历史条目选中回调
function LuaJingLingWnd:OnHistoryItemSelected(index)
    if index >= 0 and index < CJingLingMgr.Inst.ReservedQuestions.Count then
        self:ChatJingLingWithFilterOption(CJingLingMgr.Inst.ReservedQuestions[index], false, false, true, true, "history", true)
    end
    self.m_HistoryMenu.gameObject:SetActive(false)
end
--助手
function LuaJingLingWnd:OnAssitantItemClick(text, needEvaluate)
    self:ChatJingLingWithFilterOption(text, true, false, false, needEvaluate, "others", false) --不过滤
end
--语音翻译返回结果
function LuaJingLingWnd:OnJingLingProcessTextInputReady(args)
    
    local text, isClick, isVoice = args[0], args[1], args[2]
    if isVoice then
        self.m_ChatInput.value = text
        self.m_LastVoiceText = text
    else
        self:ChatJingLingWithFilterOption(text, isClick, false, false, true, "manual",true)
    end
end
--发起精灵提问
function LuaJingLingWnd:ChatJingLingWithFilterOption(question, isClick, isVoice, isManual, allowEvaluate, queryMethod, needFilter)
    if System.String.IsNullOrEmpty(question) then
        return
    end
    question = StringTrim(question)
    if CommonDefs.StringLength(question) > 0 then
        local extraInfo = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
        if isManual then
            CommonDefs.DictAdd_LuaCall(extraInfo, JingLingMessageExtraInfoKey.Manual, "1")
        end
        CommonDefs.DictAdd_LuaCall(extraInfo, JingLingMessageExtraInfoKey.QueryMethod, queryMethod)
        CJingLingMgr.Inst:QueryJingLingWithFilterOption(EnumQueryJingLingContext.Default, question, true, isClick, isVoice, allowEvaluate, false, true, extraInfo, needFilter)
        if self.m_TabBar.SelectedIndex ~= 2 then
            self.m_TabBar:ChangeTab(2, false)
        end
    end
end
--打开倩女知道(精灵专家团)
function LuaJingLingWnd:OpenExpertWnd(go)
    CExpertTeamMgr.Inst:OpenExpertTeamWnd(self.m_ChatInput.value)
    local redpoint = self.m_AskExpertButton.transform:Find("RedPoint").gameObject
    if redpoint ~= nil then
        redpoint:SetActive(false)
    end
end
--点击窗口右侧tab按钮
function LuaJingLingWnd:OnTabChange(go, index)
    self.m_TitleLabel.text = LocalString.GetString("精灵") .. string.gsub(CommonDefs.GetComponentInChildren_GameObject_Type(go, typeof(UILabel)).text, "\n", "")
    
    if CommonDefs.IS_CN_CLIENT then
        self.m_NewHotspotView:SetActive(index == 0)
        self.m_HotspotView:SetActive(false)
    else
        self.m_HotspotView:SetActive(index == 0)
        self.m_NewHotspotView:SetActive(false)
    end

    
    self.m_RecommendationView.gameObject:SetActive(index==1)
    self.m_ChatView.gameObject:SetActive(index==2)
    --self.m_AssistView.gameObject:SetActive(index==3)

    if index == 0 then
        --hotspotview init onenable
    elseif index == 1 then
        --recommendationview init onenable
    elseif index == 2 then
        self.m_ChatView:Init()
    elseif index == 3 then
        --self.m_AssistView:Init()
    end

    self.m_ChatInputRoot:SetActive(index~=3)
end
--窗口即将关闭
function LuaJingLingWnd:OnWillClose()
    LuaJingLingMgr.OnRecordQueryDurationEnd()
    CJingLingMgr.Inst.m_JingLingWndOpenTime = 0
    CJingLingMgr.Inst:ClearQueryJingLingFromParam()
end

-----------------------------
--BEGIN 精灵热词显示--
-----------------------------
--输入框被选中状态改变
function LuaJingLingWnd:OnChatInputSelectedChange()
    if CommonDefs.IS_CN_CLIENT and self.m_lastInputisSelected ~= self.m_ChatInput.isSelected then
        self.m_lastInputisSelected = self.m_ChatInput.isSelected
        if self.m_ChatInput.isSelected and self.m_HotWordsMenu.m_LuaSelf ~= nil then
            --选中状态下更新热词内容
            self.m_HotWordsMenu.m_LuaSelf:ShowHotWords(self.m_ChatInput.value)
        end
    end
end

--根据虚拟键盘位置改变热词显示位置
function LuaJingLingWnd:OnChatInputPosChange()
    if TouchScreenKeyboard.visible then
        local ratio = NativeTools.GetMobileKeyboardHeightRatio()
        local virtualScreenHeight = CUICommonDef.GetVirtualScreenSize().y
        local pos = self.m_HotWordsMenu.transform.localPosition
        pos.y = self.m_HotWordsMenuPosOffset + virtualScreenHeight * ratio
        self.m_HotWordsMenu.transform.localPosition = pos
    else
        local pos = self.m_HotWordsMenu.transform.localPosition
        pos.y = self.m_HotWordsMenuInitialPosY
        self.m_HotWordsMenu.transform.localPosition = pos
    end
end

--热词显示位置偏移
function LuaJingLingWnd:CalulateHotWordsMenuPosOffset()
    local height = self.transform:Find("Anchor/ChatInput/HotWordsMenu/Background"):GetComponent(typeof(UISprite)).height
    local chatInputRootPosY = self.m_ChatInputRoot.transform.localPosition.y
    local virtualScreenHeight = CUICommonDef.GetVirtualScreenSize().y
    return  -chatInputRootPosY + height * 0.5 - virtualScreenHeight * 0.5 
end

--输入内容改变
function LuaJingLingWnd:OnChatInputChange()
    if CommonDefs.IsAndroidPlatform() or not self.m_ChatInput.isSelected then return end
    if CommonDefs.IS_CN_CLIENT and self.m_HotWordsMenu.m_LuaSelf ~= nil then
        self.m_HotWordsMenu.m_LuaSelf:ShowHotWords(self.m_ChatInput.value)
    end
end

--获得新热词内容
function LuaJingLingWnd:OnJingLingHotWordsReady(question, list, showRecommendedTip)
    if question ~= Extensions.RemoveBlank(self.m_ChatInput.value) then return end
    self.m_HotWordsMenu.m_LuaSelf:UpdateHotWordsBtnText(list, showRecommendedTip)
end

--点击热词条目选择
function LuaJingLingWnd:OnJingLingHotWordsItemClick(text, method)
    self:ChatJingLingWithFilterOption(text, false, false, true, true, method, true)
    self.m_ChatInput.value = nil
    self.m_LastVoiceText = nil
end
-----------------------------
--END 精灵热词查询--
-----------------------------