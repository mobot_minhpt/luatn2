local CPlayerInfoMgr=import "CPlayerInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local Skill_AllSkills=import "L10.Game.Skill_AllSkills"
local Profession=import "L10.Game.Profession"
local CUITexture=import "L10.UI.CUITexture"
local UIGrid=import "UIGrid"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CGameVideoMgr=import "L10.Game.CGameVideoMgr"
-- local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"


CLuaQMPKBattleDataWnd=class()
RegistClassMember(CLuaQMPKBattleDataWnd,"m_SkillRoot")
RegistClassMember(CLuaQMPKBattleDataWnd,"m_BaseRoot")
RegistClassMember(CLuaQMPKBattleDataWnd,"m_PlayPopupData")
RegistClassMember(CLuaQMPKBattleDataWnd,"m_DataPopupData")
RegistClassMember(CLuaQMPKBattleDataWnd,"m_PlayNames")
RegistClassMember(CLuaQMPKBattleDataWnd,"m_DataTypeNames")

RegistClassMember(CLuaQMPKBattleDataWnd,"m_PlaySelectButton")
RegistClassMember(CLuaQMPKBattleDataWnd,"m_DataTypeSelectButton")

RegistClassMember(CLuaQMPKBattleDataWnd,"m_PlayIndex")
RegistClassMember(CLuaQMPKBattleDataWnd,"m_DataTypeIndex")
RegistClassMember(CLuaQMPKBattleDataWnd,"m_SkillItemTemplate")
RegistClassMember(CLuaQMPKBattleDataWnd,"m_DataItemTemplate")

RegistClassMember(CLuaQMPKBattleDataWnd,"m_MainPlayerId")

function CLuaQMPKBattleDataWnd:Init()




    self.m_SkillRoot=FindChild(self.transform,"Skill")
    self.m_BaseRoot=FindChild(self.transform,"Base")
    self.m_SkillRoot.gameObject:SetActive(false)
    self.m_BaseRoot.gameObject:SetActive(false)
    self.m_SkillItemTemplate=FindChild(self.m_SkillRoot,"ItemTemplate").gameObject
    self.m_SkillItemTemplate:SetActive(false)
    self.m_DataItemTemplate=FindChild(self.m_BaseRoot,"ItemTemplate").gameObject
    self.m_DataItemTemplate:SetActive(false)


    if CClientMainPlayer.Inst then
        self.m_MainPlayerId=CClientMainPlayer.Inst.Id
    end


    self.m_PlayNames={
        LocalString.GetString("第1场团队赛"),
        LocalString.GetString("第2场单人赛"),
        LocalString.GetString("第3场三人赛"),
        LocalString.GetString("第4场双人赛"),
        LocalString.GetString("第5场团队赛")
    }
    local popuplist={}
    for i,v in ipairs(self.m_PlayNames) do
        local data = PopupMenuItemData(v, DelegateFactory.Action_int(function(index) self:OnPlaySelected(index) end), false, nil, EnumPopupMenuItemStyle.Light)
        table.insert( popuplist,data )
    end
    self.m_PlayPopupData=Table2Array(popuplist,MakeArrayClass(PopupMenuItemData))
    self.m_PlaySelectButton=FindChild(self.transform,"Button1").gameObject
    UIEventListener.Get(self.m_PlaySelectButton).onClick=DelegateFactory.VoidDelegate(function(go)
        CPopupMenuInfoMgr.ShowPopupMenu(self.m_PlayPopupData, go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, DelegateFactory.Action(function () 
            -- self.m_ClassTitle:Expand(false)
        end), 600, true, 296)
    end)

    self.m_DataTypeNames={LocalString.GetString("详细数据"),LocalString.GetString("技能使用")}
    local popuplist2={}
    for i,v in ipairs(self.m_DataTypeNames) do
        local data = PopupMenuItemData(v, DelegateFactory.Action_int(function(index) self:OnDataTypeSelected(index) end), false, nil, EnumPopupMenuItemStyle.Light)
        table.insert( popuplist2,data )
    end
    self.m_DataPopupData=Table2Array(popuplist2,MakeArrayClass(PopupMenuItemData))
    self.m_DataTypeSelectButton=FindChild(self.transform,"Button2").gameObject
    UIEventListener.Get(self.m_DataTypeSelectButton).onClick=DelegateFactory.VoidDelegate(function(go)
        CPopupMenuInfoMgr.ShowPopupMenu(self.m_DataPopupData, go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, DelegateFactory.Action(function () 
            -- self.m_ClassTitle:Expand(false)
        end), 600, true, 296)
    end)
    
    -- local array=Table2Array(self.m_ClassPopupList,MakeArrayClass(PopupMenuItemData))
    
    -- CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, CPopupMenuInfoMgr.AlignType.Bottom, #self.m_ClassPopupList >= 8 and 2 or 1, DelegateFactory.Action(function () 
    --     self.m_ClassTitle:Expand(false)
    -- end), 600, true, 296)

    self.m_DataTypeIndex=1
    self:OnPlaySelected(0)
end
function CLuaQMPKBattleDataWnd:OnPlaySelected(index)
    local label=self.m_PlaySelectButton.transform:Find("Label"):GetComponent(typeof(UILabel))
    label.text=self.m_PlayNames[index+1]

    self.m_PlayIndex=index+1
    --看看有没有数据
    if  CLuaQMPKMgr.m_FightData[index+1] then
        self:InitList()
    else
        --清掉界面
        self:InitList()
        if CGameVideoMgr.Inst:IsLiveMode() or CGameVideoMgr.Inst:IsVideoMode() then
        else
            Gac2Gas.QueryQmpkDetailFightData(index+1)
        end
    end
end

function CLuaQMPKBattleDataWnd:OnDataTypeSelected(index)
local label=self.m_DataTypeSelectButton.transform:Find("Label"):GetComponent(typeof(UILabel))
    label.text=self.m_DataTypeNames[index+1]

    --看看有没有数据
    self.m_DataTypeIndex=index+1

    self:InitList()
end

function CLuaQMPKBattleDataWnd:OnEnable()
    g_ScriptEvent:AddListener("ReplyQmpkDetailFightData", self, "OnReplyQmpkDetailFightData")
    -- ReplyQmpkDetailFightData
end
function CLuaQMPKBattleDataWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReplyQmpkDetailFightData", self, "OnReplyQmpkDetailFightData")
end

function CLuaQMPKBattleDataWnd:OnReplyQmpkDetailFightData(round, attackData, defendData)
    -- if round==self.m_PlayIndex then

    -- end
    local data=CLuaQMPKMgr.m_FightData[self.m_PlayIndex]
    if not data then return end

    self:InitList()
    -- if self.m_DataTypeIndex==1 then
    --     --详细数据
    --     local data1=data[1]
    --     local data2=data[2]

    -- elseif self.m_DataTypeIndex==2 then
    --     --技能使用
    -- end
end
function CLuaQMPKBattleDataWnd:InitList()

    local grid1=nil
    local grid2=nil
    local itemTemplate=nil
    local teamNameLabel1=nil
    local teamNameLabel2=nil
    
    if self.m_DataTypeIndex==1 then
        self.m_BaseRoot.gameObject:SetActive(true) 
        self.m_SkillRoot.gameObject:SetActive(false) 
        grid1=FindChild(self.m_BaseRoot,"Grid1")
        grid2=FindChild(self.m_BaseRoot,"Grid2")
        itemTemplate=FindChild(self.m_BaseRoot,"ItemTemplate").gameObject
        teamNameLabel1=FindChild(self.m_BaseRoot,"TeamNameLabel1"):GetComponent(typeof(UILabel))
        teamNameLabel2=FindChild(self.m_BaseRoot,"TeamNameLabel2"):GetComponent(typeof(UILabel))
    elseif self.m_DataTypeIndex==2 then
        self.m_BaseRoot.gameObject:SetActive(false) 
        self.m_SkillRoot.gameObject:SetActive(true)
        grid1=FindChild(self.m_SkillRoot,"Grid1")
        grid2=FindChild(self.m_SkillRoot,"Grid2")
        itemTemplate=FindChild(self.m_SkillRoot,"ItemTemplate").gameObject
        teamNameLabel1=FindChild(self.m_SkillRoot,"TeamNameLabel1"):GetComponent(typeof(UILabel))
        teamNameLabel2=FindChild(self.m_SkillRoot,"TeamNameLabel2"):GetComponent(typeof(UILabel))
    end
    teamNameLabel1.text=nil
    teamNameLabel2.text=nil

    CUICommonDef.ClearTransform(grid1)
    CUICommonDef.ClearTransform(grid2)
    
    local data=CLuaQMPKMgr.m_FightData[self.m_PlayIndex]
    if not data then return end
    local data1=data[1]
    local data2=data[2]
    teamNameLabel1.text=data.name1
    teamNameLabel2.text=data.name2
    if self.m_DataTypeIndex==1 then
        --详细数据
        local count=math.max(5,#data1)
        for i=1,count do
            local go=NGUITools.AddChild(grid1.gameObject,itemTemplate)
            go:SetActive(true)
            self:InitDataItem(go.transform,data1[i],i)
        end

        count=math.max(5,#data2)
        for i=1,count do
            local go=NGUITools.AddChild(grid2.gameObject,itemTemplate)
            go:SetActive(true)
            self:InitDataItem(go.transform,data2[i],i)
        end


    elseif self.m_DataTypeIndex==2 then
        --技能使用
        local count=math.max(5,#data1)
        for i=1,count do
            local go=NGUITools.AddChild(grid1.gameObject,itemTemplate)
            go:SetActive(true)
            self:InitSkillItem(go.transform,data1[i],i)
        end

        count=math.max(5,#data2)
        for i=1,count do
            local go=NGUITools.AddChild(grid2.gameObject,itemTemplate)
            go:SetActive(true)
            self:InitSkillItem(go.transform,data2[i],i)
        end

    end
    grid1:GetComponent(typeof(UIGrid)):Reposition()
    grid2:GetComponent(typeof(UIGrid)):Reposition()
end
function  CLuaQMPKBattleDataWnd:InitDataItem(tf,itemData,index)
    local bg=tf:Find("Bg").gameObject
    if index % 2==0 then
        bg:SetActive(true)
    else
        bg:SetActive(false)
    end

    local labelsTf=tf:Find("Labels")
    local nameLabel=tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    local classSprite=tf:Find("ClassSprite"):GetComponent(typeof(UISprite))
    local btn=tf:Find("Button").gameObject

    if itemData then
        local fightdata=itemData.fightdata
        for i=1,7 do
            labelsTf:GetChild(i-1):GetComponent(typeof(UILabel)).text=tostring(fightdata[i])
        end
        nameLabel.text=itemData.name
        classSprite.spriteName=Profession.GetIconByNumber(itemData.clazz)

        if self.m_MainPlayerId==itemData.playerId then
            btn:SetActive(false)
        else
            btn:SetActive(true)
            UIEventListener.Get(btn).onClick=DelegateFactory.VoidDelegate(function(go)
                CPlayerInfoMgr.ShowPlayerInfoWnd(itemData.playerId, itemData.name)
            end)
        end
    else
        nameLabel.text=nil
        classSprite.spriteName=nil
        labelsTf.gameObject:SetActive(false)
        btn:SetActive(false)
    end
end
function CLuaQMPKBattleDataWnd:InitSkillItem(tf,itemData,index)
    local bg=tf:Find("Bg").gameObject
    if index % 2==0 then
        bg:SetActive(true)
    else
        bg:SetActive(false)
    end

    local labelsTf=tf:Find("Labels")
    local iconsTf=tf:Find("Icons")
    local nameLabel=tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    local classSprite=tf:Find("ClassSprite"):GetComponent(typeof(UISprite))
    local btn=tf:Find("Button").gameObject

    if itemData then
        for i=1,8 do
            local skillcls=itemData.skilldata[i*2-1]
            local label=labelsTf:GetChild(i-1):GetComponent(typeof(UILabel))
            label.text=""
            if skillcls then
                local skillId=itemData.skilldata[i*2-1]*100+1
                local skillData=Skill_AllSkills.GetData(skillId)
                if skillData then
                    iconsTf:GetChild(i-1):GetComponent(typeof(CUITexture)):LoadSkillIcon(skillData.SkillIcon)
                    label.text=tostring(itemData.skilldata[i*2] or 0)
                else
                    label.text=""
                end
            end
        end
        nameLabel.text=itemData.name
        classSprite.spriteName=Profession.GetIconByNumber(itemData.clazz)

        if self.m_MainPlayerId==itemData.playerId then
            btn:SetActive(false)
        else
            btn:SetActive(true)
            UIEventListener.Get(btn).onClick=DelegateFactory.VoidDelegate(function(go)
                CPlayerInfoMgr.ShowPlayerInfoWnd(itemData.playerId, itemData.name)
            end)
        end
    else
        nameLabel.text=nil
        classSprite.spriteName=nil
        iconsTf.gameObject:SetActive(false)
        labelsTf.gameObject:SetActive(false)
        btn:SetActive(false)
    end
end


return CLuaQMPKBattleDataWnd