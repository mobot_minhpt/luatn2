local SoundManager=import "SoundManager"
local TweenAlpha=import "TweenAlpha"
local TweenScale= import "TweenScale"
local Ease            = import "DG.Tweening.Ease"

local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIGameObjectPool=import "L10.UI.CUIGameObjectPool"
local CCMiniAPIMgr=import "L10.Game.CCMiniAPIMgr"
local EnumCCMiniStreamType = import "L10.Game.EnumCCMiniStreamType"
local CCCChatMgr=import "L10.Game.CCCChatMgr"
LuaShengXiaoCardPlayerView = class()

RegistChildComponent(LuaShengXiaoCardPlayerView, "ReadyMark",  GameObject)
RegistChildComponent(LuaShengXiaoCardPlayerView, "CardNumLabel",  UILabel)
RegistChildComponent(LuaShengXiaoCardPlayerView, "Player",  GameObject)
RegistChildComponent(LuaShengXiaoCardPlayerView, "NoPlayer", GameObject)
RegistChildComponent(LuaShengXiaoCardPlayerView, "CardTemplate",  GameObject)
RegistChildComponent(LuaShengXiaoCardPlayerView, "HandCards",  Transform)
RegistChildComponent(LuaShengXiaoCardPlayerView, "HandCards2",  Transform)
RegistChildComponent(LuaShengXiaoCardPlayerView, "ShowCards", Transform)
-- RegistChildComponent(LuaShengXiaoCardPlayerView, "SealCards", QnTableView)
RegistChildComponent(LuaShengXiaoCardPlayerView, "NameLabel", UILabel)
RegistChildComponent(LuaShengXiaoCardPlayerView, "MsgLabel", UILabel)
RegistChildComponent(LuaShengXiaoCardPlayerView, "PortraitTexture", CUITexture)
RegistChildComponent(LuaShengXiaoCardPlayerView, "CountdownLabel", UILabel)
RegistChildComponent(LuaShengXiaoCardPlayerView, "StatusLabel", UILabel)
RegistChildComponent(LuaShengXiaoCardPlayerView, "Slider", UISlider)
RegistChildComponent(LuaShengXiaoCardPlayerView, "Wei", GameObject)
RegistChildComponent(LuaShengXiaoCardPlayerView, "PopupButton", GameObject)
RegistChildComponent(LuaShengXiaoCardPlayerView, "VoiceButton", GameObject)
-- RegistChildComponent(LuaShengXiaoCardPlayerView, "IsSpeaking", GameObject)
RegistChildComponent(LuaShengXiaoCardPlayerView, "StateIcon", UISprite)
RegistChildComponent(LuaShengXiaoCardPlayerView, "Fx", CUIFx)
RegistChildComponent(LuaShengXiaoCardPlayerView, "RoundEndFx", CUIFx)
RegistChildComponent(LuaShengXiaoCardPlayerView, "SealCardGrid", UIGrid)
RegistChildComponent(LuaShengXiaoCardPlayerView, "SealCardPool", CUIGameObjectPool)
RegistChildComponent(LuaShengXiaoCardPlayerView, "ExpressionLabel", UILabel)
RegistChildComponent(LuaShengXiaoCardPlayerView, "PlayingMark", GameObject)
RegistChildComponent(LuaShengXiaoCardPlayerView, "Circle01", UISprite)
RegistChildComponent(LuaShengXiaoCardPlayerView, "Circle02", UISprite)
RegistChildComponent(LuaShengXiaoCardPlayerView, "Circle03", UISprite)

RegistClassMember(LuaShengXiaoCardPlayerView,"m_ViewIndex")
RegistClassMember(LuaShengXiaoCardPlayerView,"m_Index")
RegistClassMember(LuaShengXiaoCardPlayerView,"m_PlayerId")

RegistClassMember(LuaShengXiaoCardPlayerView,"m_HandCards")
RegistClassMember(LuaShengXiaoCardPlayerView,"m_SealCardData")
RegistClassMember(LuaShengXiaoCardPlayerView,"m_Ticks")
RegistClassMember(LuaShengXiaoCardPlayerView,"m_Circles")

function LuaShengXiaoCardPlayerView:Awake()
    self.m_Circles = {self.Circle01,self.Circle02,self.Circle03}
    self.m_Ticks = {}
    self.ReadyMark:SetActive(false)
    self.CountdownLabel.gameObject:SetActive(false)
    if self.StatusLabel then
        self.StatusLabel.text = nil
    end
    -- self.HandCards.gameObject:SetActive(false)
    if self.HandCards2 then
        self.HandCards2.gameObject:SetActive(false)
    end
    -- self.ShowCards.gameObject:SetActive(false)
    -- self.ShowCards.gameObject:SetActive(false)
    if self.Slider then
        self.Slider.gameObject:SetActive(false)
    end
    self.MsgLabel.text = nil
    self.Wei:SetActive(false)

    self.VoiceButton:SetActive(false)
    -- self.IsSpeaking:SetActive(false)
    self.PlayingMark:SetActive(false)

    if self.PopupButton then
        UIEventListener.Get(self.PopupButton).onClick = DelegateFactory.VoidDelegate(function(go)
            LuaShengXiaoCardPopupMenu.s_Position = self.PopupButton.transform.position
            LuaShengXiaoCardPopupMenu.s_TargetId = 0
            if self.m_ViewIndex~=1 then--自己
                for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
                    if v.Index==self.m_Index then
                        LuaShengXiaoCardPopupMenu.s_TargetId = k
                    end
                end
            end
            CUIManager.ShowUI(CLuaUIResources.ShengXiaoCardPopupMenu)
        end)
    end

    self:RefreshVoiceButton()
end

function LuaShengXiaoCardPlayerView:InitView(viewIndex)
    self.m_ViewIndex = viewIndex
    self.m_SealCardData = {}
end

--Index Class Name Gender
function LuaShengXiaoCardPlayerView:InitProfile(profile)
    self.m_PlayerId = 0
    self.MsgLabel.text = nil
    self.StateIcon.gameObject:SetActive(false)

    if not profile then
        if self.m_ViewIndex~=1 then
            if self.Player then self.Player:SetActive(false) end
            if self.NoPlayer then self.NoPlayer:SetActive(true) end
        end
        return
    end
    --防止fx再次播放
    if self.Player and not self.Player.activeSelf then self.Player:SetActive(true) end
    if self.NoPlayer then self.NoPlayer:SetActive(false) end

    self.m_Index = profile.Index
    self.NameLabel.text = profile.Name
    self.m_PlayerId = profile.Id

    self.PlayingMark:SetActive(false)
    if profile.GameInfo then
        local resetTweener = function()
            for i,v in ipairs(self.m_Circles) do
                local tween1 = v.gameObject:GetComponent(typeof(TweenAlpha))
                tween1:ResetToBeginning()
                local tween2 = v.gameObject:GetComponent(typeof(TweenScale))
                tween2:ResetToBeginning()
            end
        end
        --猜牌
        if LuaShengXiaoCardMgr.m_State == EnumShengXiaoCardState.WaitGuess 
            or LuaShengXiaoCardMgr.m_State == EnumShengXiaoCardState.PassGuess then
            if profile.GameInfo.Role == EnumShengXiaoCardRole.Guess then
                self.MsgLabel.color = NGUIText.ParseColor24("5BB4FF", 0)
                for i,v in ipairs(self.m_Circles) do
                    v.color = NGUIText.ParseColor24("1DA1FF", 0)
                end

                self.MsgLabel.text = LocalString.GetString("猜牌中")
                self.PlayingMark:SetActive(true)
                resetTweener()
            end
        --出牌
        elseif LuaShengXiaoCardMgr.m_State == EnumShengXiaoCardState.WaitShow then
            if profile.GameInfo.Role == EnumShengXiaoCardRole.Show then
                self.MsgLabel.color = NGUIText.ParseColor24("FF5050", 0)

                for i,v in ipairs(self.m_Circles) do
                    v.color = NGUIText.ParseColor24("FF2929", 0)
                end

                self.MsgLabel.text = LocalString.GetString("出牌中")
                self.PlayingMark:SetActive(true)
                resetTweener()
            end 
        --传牌
        elseif LuaShengXiaoCardMgr.m_State == EnumShengXiaoCardState.PassShow then
            if profile.GameInfo.Role == EnumShengXiaoCardRole.Pass then
                self.MsgLabel.color = NGUIText.ParseColor24("FF9627", 0)

                for i,v in ipairs(self.m_Circles) do
                    v.color = NGUIText.ParseColor24("FF8300", 0)
                end

                self.MsgLabel.text = LocalString.GetString("传递中")
                self.PlayingMark:SetActive(true)
                resetTweener()
            end 
        end
    end

    local portraitName=CUICommonDef.GetPortraitName(profile.Class, profile.Gender, -1)
    self.PortraitTexture:LoadNPCPortrait(portraitName)
    --准备状态
    if profile.Prepared then
        self.ReadyMark:SetActive(true)
        if self.m_ViewIndex==1 then
            -- self.StatusLabel.text = LocalString.GetString("准备完毕，等待其他玩家准备")
        end
    else
        self.ReadyMark:SetActive(false)
        -- self.StatusLabel.text = nil
    end
    if profile.GameInfo and self.m_ViewIndex==1 then
        self.m_HandCards = {}
        Extensions.RemoveAllChildren(self.HandCards)

        --Role Seals Hands Guaji Escape
        local handcards = profile.GameInfo.Hands
        if handcards then
            for i,v in ipairs(handcards) do
                local card = NGUITools.AddChild(self.HandCards.gameObject,self.CardTemplate)
                card:SetActive(true)
                local cardCmp=LuaShengXiaoCard:new()
                cardCmp:Init(card.transform,v.Id,v.ShengXiao,i,#handcards)
                table.insert(self.m_HandCards,cardCmp)
            end
        end
        --发牌动画
        if LuaShengXiaoCardMgr.m_State == EnumShengXiaoCardState.Start then
            for i,v in ipairs(self.m_HandCards) do
                v:PlayFaPaiEffect()
            end
        end
    else
        Extensions.RemoveAllChildren(self.HandCards)
    end
    if profile.GameInfo then
        local handcards = profile.GameInfo.Hands
        if self.HandCards2 and handcards then
            self.HandCards2.gameObject:SetActive(true)
            self.CardNumLabel.text = tostring(#handcards)
        end

        local lable = self.StateIcon.transform:Find("Label"):GetComponent(typeof(UILabel))
        if profile.GameInfo.Role==EnumShengXiaoCardRole.Show then
            self.StateIcon.gameObject:SetActive(true)
            lable.text = LocalString.GetString("出")
            self.StateIcon.color = NGUIText.ParseColor24("FF5050", 0)
        elseif profile.GameInfo.Role==EnumShengXiaoCardRole.Guess then
            self.StateIcon.gameObject:SetActive(true)
            lable.text = LocalString.GetString("猜")
            self.StateIcon.color = NGUIText.ParseColor24("4F99FF", 0)
        elseif profile.GameInfo.Role==EnumShengXiaoCardRole.Watch then
            self.StateIcon.gameObject:SetActive(true)
            lable.text = LocalString.GetString("观")
            self.StateIcon.color = NGUIText.ParseColor24("FF8D4F", 0)
        elseif profile.GameInfo.Role==EnumShengXiaoCardRole.Pass then
            self.StateIcon.gameObject:SetActive(true)
            lable.text = LocalString.GetString("传")
            self.StateIcon.color = NGUIText.ParseColor24("FF8D4F", 0)

        else
            self.StateIcon.gameObject:SetActive(false)
        end
    else
        if self.HandCards2 then
            self.HandCards2.gameObject:SetActive(false)
        end
        self.StateIcon.gameObject:SetActive(false)
    end

    --先回收
    local count = self.SealCardGrid.transform.childCount
    if count>0 then
        for i=count,1,-1 do
            local child = self.SealCardGrid.transform:GetChild(i-1)
            LuaTweenUtils.DOKill(child,false)
            self.SealCardPool:Recycle(child.gameObject)
        end
    end

    if profile.GameInfo and profile.GameInfo.Seals then
        local sealcards = profile.GameInfo.Seals
        if next(sealcards) then
            LuaShengXiaoCardMgr.TryTriggerGuide(6)
        end

        self.m_SealCardData = {}
        for k,v in pairs(sealcards) do
            table.insert(self.m_SealCardData,{card = k,count = v,wei=false})
        end
        table.sort( self.m_SealCardData,function(a,b)
            return a.card<b.card
        end )

        -- local sealcards = profile.GameInfo.Seals
        if LuaShengXiaoCardMgr.m_State~=EnumShengXiaoCardState.RoundEnd then
            self:SyncSeals(profile)
        else
            local yesOrNo = LuaShengXiaoCardMgr.m_GuessInfo.YesOrNo
            local realShengXiao = LuaShengXiaoCardMgr.m_ShowInfo.Card.ShengXiao
            local displayShengXiao = LuaShengXiaoCardMgr.m_ShowInfo.DisplayId
            local targetPlayerId = 0
            if (realShengXiao==displayShengXiao and yesOrNo>0) or (realShengXiao~=displayShengXiao and yesOrNo==0) then
                --飞到出牌人
                targetPlayerId = LuaShengXiaoCardMgr.m_ShowInfo.Actor
            else
                --飞到猜牌人
                targetPlayerId = LuaShengXiaoCardMgr.m_GuessInfo.Actor
            end
            local doani = false
            for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
                if k==targetPlayerId and self.m_Index==v.Index then
                    doani = true
                end
            end
            if doani then
                self:SyncSeals(profile,LuaShengXiaoCardMgr.m_ShowInfo.Card.ShengXiao)
            else
                self:SyncSeals(profile)
            end
        end
    else
        self.m_SealCardData = {}

    end

end
function LuaShengXiaoCardPlayerView:SyncSeals(profile,newcard)
    local newcardindex = 0
    local newcardprenum = 0

    for i,v in ipairs(self.m_SealCardData) do
        if newcard and v.card==newcard then
            newcardindex = i
            newcardprenum = v.count-1
            v.precount = newcardprenum
        else
            v.precount = v.count
        end
    end

    local isDangerous = false

    local sealCount = 0
    for i,v in ipairs(self.m_SealCardData) do
        if v.precount>0 then
            sealCount = sealCount+1
        end
    end
    if sealCount>=5 then
        isDangerous=true
        for i,v in ipairs(self.m_SealCardData) do
            v.wei = true
        end
    end

    local handcards = profile.GameInfo.Hands
    if handcards and #handcards==1 then
        isDangerous = true
    end

    for i,v in ipairs(self.m_SealCardData) do
        if v.precount>=2 then
            isDangerous = true
            v.wei = true
        end
    end
    if isDangerous then
        if not self.Wei.activeSelf then
            self.Wei:SetActive(true)
            LuaShengXiaoCardMgr.TryTriggerGuide(7)
            SoundManager.Inst:PlayOneShot(LuaShengXiaoCardMgr.Sounds.danger, Vector3(0,0,0),nil,0)

        end
    else
        self.Wei:SetActive(false)
    end

    for i,v in ipairs(self.m_SealCardData) do
        local go = self.SealCardPool:GetFromPool(0)
        go.transform.parent = self.SealCardGrid.transform
        go.transform.localScale = Vector3.one
        go:SetActive(true)

        if not newcard then
            self:InitSealCard(go,v,i,#self.m_SealCardData)
        else
            if i==newcardindex then
                self:InitSealCard(go,v,i,#self.m_SealCardData)
                if newcardprenum==0 then--新牌
                    --做一个放大动画
                    go.transform.localScale = Vector3(0.01,0.01,0.01)
                    LuaTweenUtils.SetDelay(LuaTweenUtils.TweenScaleXY(go.transform,1,1,0.5,Ease.OutElastic),3.5)
                else
                    --数字变一下
                    local tf = go.transform
                    local numberLabel = tf:Find("NumberLabel"):GetComponent(typeof(UILabel))
                    numberLabel.text = tostring(v.count-1)
                    if self.m_Ticks["changecount"] then 
                        UnRegisterTick(self.m_Ticks["changecount"]) 
                        self.m_Ticks["changecount"]=nil 
                    end
                    
                    self.m_Ticks["changecount"] = RegisterTickOnce(function()
                        self.m_Ticks["changecount"] = nil
                        numberLabel.text = tostring(v.count)
                        numberLabel.transform.localScale = Vector3(0.01,0.01,0.01)
                        LuaTweenUtils.TweenScaleXY(numberLabel.transform,1,1,0.5,Ease.OutElastic)

                        if #self.m_SealCardData>5 then
                            --所有的卡牌都变红
                            local count = self.SealCardGrid.transform.childCount
                            if count>0 then
                                for i=count,1,-1 do
                                    local child = self.SealCardGrid.transform:GetChild(i-1)
                                    local wei = tf:Find("Wei").gameObject
                                    wei:SetActive(true)
                                end
                            end
                        else
                            if v.count>=2 then
                                local wei = tf:Find("Wei").gameObject
                                wei:SetActive(true)
                            end
                        end
                    end,3500)
                end
            else
                if newcardprenum==0 then
                    local newX = 0
                    local num = #self.m_SealCardData
                    if self.m_ViewIndex==2 then
                        newX = -78*(num-i)
                    else
                        newX = 78*(i-1)
                    end

                    if i<newcardindex then
                        self:InitSealCard(go,v,i,#self.m_SealCardData-1)
                    else
                        self:InitSealCard(go,v,i-1,#self.m_SealCardData-1)
                    end
                    LuaTweenUtils.SetDelay(LuaTweenUtils.DOLocalMoveX(go.transform,newX,1,false),3)
                else
                    --不需要做动画
                    self:InitSealCard(go,v,i,#self.m_SealCardData)
                end

            end
        end
    end
end
function LuaShengXiaoCardPlayerView:InitSealCard(go,data,idx,num)
    local tf = go.transform
    if self.m_ViewIndex==2 then
        tf.localPosition = Vector3(-78*(num-idx),0,0)
    else
        tf.localPosition = Vector3(78*(idx-1),0,0)
    end
    local typeLabel = tf:Find("TypeLabel"):GetComponent(typeof(UILabel))
    local numberLabel = tf:Find("NumberLabel"):GetComponent(typeof(UILabel))
    -- local data = self.m_SealCardData[idx]

    local design = ShengXiaoCard_ShengXiao.GetData(data.card)
    typeLabel.text = design.Name
    numberLabel.text = tostring(data.count)
    typeLabel.color = NGUIText.ParseColor24(design.Color, 1)
    numberLabel.color = typeLabel.color
    local wei = tf:Find("Wei").gameObject

    if data.wei then
        wei:SetActive(true)
    else
        wei:SetActive(false)
    end
end
-- Role = EnumShengXiaoCardRole.None,
-- Hands = {},
-- Seals = {},
-- Escape = false,
-- Guaji = false,
function LuaShengXiaoCardPlayerView:Sync(playerInfo)
    if self.m_ViewIndex~=1 then
        self.CardNumLabel.text = #playerInfo.Hands
    else
        --判断下 有没有出牌
        if self.m_HandCards and #self.m_HandCards>0 then

        else
            --手上没有牌，创建
            local allCardNum = 0
            local cards = {}
            for k,v in pairs(playerInfo.Hands) do
                table.insert( cards,k )
                allCardNum = allCardNum+v
            end
            --从小到大
            -- table.sort( cards ,function(a,b) return a<b end)
            -- local index = 0
            -- for i,v in ipairs(cards) do
            --     local num = playerInfo.Hands[v]
            --     for i=1,num do
            --         local card = NGUITools.AddChild(self.ShowCards,self.m_CardTemplate)
            --         card:SetActive(true)
            --         local cardCmp=LuaShengXiaoCard:new()
            --         index = index+1
            --         cardCmp:Init(card.transform,v,index,allCardNum)
            --         table.insert(self.m_HandCards,cardCmp)
            --     end
            -- end

        end
    end




end

function LuaShengXiaoCardPlayerView:OnShengXiaoCard_SyncPrepare(bPrepare)
    self.ReadyMark:SetActive(bPrepare)
    if self.m_ViewIndex==1 then
        -- self.StatusLabel.text = LocalString.GetString("准备完毕，等待其他玩家准备")
    end

end

function LuaShengXiaoCardPlayerView:ChuPai(cardValue)
    --找到第一张指定牌


end
--轮到我出牌
function LuaShengXiaoCardPlayerView:WaitShow()
    if self.m_ViewIndex==1 then
        --显示出牌按钮
    else
        --显示倒计时
    end
end

function LuaShengXiaoCardPlayerView:OnEnable()
    g_ScriptEvent:AddListener("ShengXiaoCard_OnClickCard", self, "OnShengXiaoCard_OnClickCard")
    g_ScriptEvent:AddListener("CC_RemovePlayerEId", self, "OnCC_RemovePlayerEId")
    g_ScriptEvent:AddListener("CC_SetPlayerEId", self, "OnCC_SetPlayerEId")
    g_ScriptEvent:AddListener("CC_ClearPlayerEId", self, "OnCC_ClearPlayerEId")
    g_ScriptEvent:AddListener("OnGetCCSpeakingList", self, "OnGetCCSpeakingList")
end
function LuaShengXiaoCardPlayerView:OnDisable()
    g_ScriptEvent:RemoveListener("ShengXiaoCard_OnClickCard", self, "OnShengXiaoCard_OnClickCard")
    g_ScriptEvent:RemoveListener("CC_RemovePlayerEId", self, "OnCC_RemovePlayerEId")
    g_ScriptEvent:RemoveListener("CC_SetPlayerEId", self, "OnCC_SetPlayerEId")
    g_ScriptEvent:RemoveListener("CC_ClearPlayerEId", self, "OnCC_ClearPlayerEId")
    g_ScriptEvent:RemoveListener("OnGetCCSpeakingList", self, "OnGetCCSpeakingList")
end

function LuaShengXiaoCardPlayerView:OnGetCCSpeakingList(args)
    local type = args[0]
    local eids = args[1]
    local eid = 0
    for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
        if v.Index==self.m_Index then
            eid = CCCChatMgr.Inst:GetPlayerEId(k)
        end
    end
    if eid>0 and type==EnumToInt(EnumCCMiniStreamType.ePlayCC) and CommonDefs.HashSetContains(eids,eid) then
        self.VoiceButton:SetActive(true)
    else
        self.VoiceButton:SetActive(false)
    end
end

function LuaShengXiaoCardPlayerView:OnCC_RemovePlayerEId(args)
    self:RefreshVoiceButton()
end
function LuaShengXiaoCardPlayerView:OnCC_SetPlayerEId(args)
    self:RefreshVoiceButton()
end
function LuaShengXiaoCardPlayerView:RefreshVoiceButton()
    local isInStream = CCMiniAPIMgr.Inst:IsInPlayCCStream()
    if not isInStream then
        self.VoiceButton:SetActive(false)
        -- self.IsSpeaking:SetActive(false)
        return
    end
    for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
        if v.Index==self.m_Index then
            local eid = CCCChatMgr.Inst:GetPlayerEId(k)
            self.VoiceButton:SetActive(eid>0)
            if eid==0 then
                -- self.IsSpeaking:SetActive(false)
            else
                -- if not self.IsSpeaking.activeSelf then
                --     self.IsSpeaking:SetActive(true)
                -- end
            end
        end
    end
end
function LuaShengXiaoCardPlayerView:OnCC_ClearPlayerEId()
    self.VoiceButton:SetActive(false)
    -- self.IsSpeaking:SetActive(false)
end
function LuaShengXiaoCardPlayerView:OnShengXiaoCard_SyncGuaji(guaji)
    if self.StatusLabel then
        self.StatusLabel.text = guaji and LocalString.GetString("托管中...") or nil
    end
end
function LuaShengXiaoCardPlayerView:OnShengXiaoCard_OnClickCard(id,selected)
    if self.m_HandCards then
        if selected then
            for i,v in ipairs(self.m_HandCards) do
                if v.m_Id ~= id then
                    v:TakeBack()
                end
            end
        end
    end
end

local leftTime = 0
local sliderTweener = nil
function LuaShengXiaoCardPlayerView:CancelCountdown()
    if self.m_Ticks["countdown"] then UnRegisterTick(self.m_Ticks["countdown"]) self.m_Ticks["countdown"] = nil end
    self.CountdownLabel.gameObject:SetActive(false)

    if self.Slider then
        self.Slider.gameObject:SetActive(false)
    end
    if sliderTweener then
        LuaTweenUtils.Kill(sliderTweener,false)--tweenfloat的这种不是依赖于transform的，必须采用这种方式kill
        sliderTweener = nil
    end
end

function LuaShengXiaoCardPlayerView:StartCountdown(expireTime)
    self:CancelCountdown()

    leftTime = math.floor(expireTime - CServerTimeMgr.Inst.timeStamp)
    if leftTime>0 then
        self.CountdownLabel.gameObject:SetActive(true)
        self.CountdownLabel.text = tostring(leftTime)

        self.m_Ticks["countdown"] = RegisterTickWithDuration(function ()
            leftTime=leftTime-1
            if leftTime>=0 then
                self.CountdownLabel.text = tostring(leftTime)
            else
                self.CountdownLabel.gameObject:SetActive(false)
            end
            if leftTime<0 then 
                self:CancelCountdown()
            end
            if leftTime<=5 then
                SoundManager.Inst:PlayOneShot(LuaShengXiaoCardMgr.Sounds.countdown, Vector3(0,0,0),nil,0)
            end

            --倒计时10秒开始显示进度条
            if not sliderTweener and self.m_ViewIndex==1 and leftTime<=10 then
                self.Slider.gameObject:SetActive(true)
                local val = leftTime/10
                sliderTweener=LuaTweenUtils.TweenFloat(val,0,val*10,
                    function(val)
                        if self.Slider then
                            self.Slider.value=val
                            if val<0.05 then--最后时刻 隐藏
                                self.Slider.gameObject:SetActive(false)
                                LuaTweenUtils.Kill(sliderTweener,false)
                                sliderTweener = nil
                            end
                        end
                    end)
            end
        end,1000,(leftTime)*1000)
    end

    
end

function LuaShengXiaoCardPlayerView:GetSelectedCard()
    for i,v in ipairs(self.m_HandCards) do
        if v.m_Selected then
            return i,v
        end
    end
    return nil,nil
end
function LuaShengXiaoCardPlayerView:PlayCard(idx,card)
    table.remove( self.m_HandCards,idx )
    local num = #self.m_HandCards
    for i,v in ipairs(self.m_HandCards) do
        v.m_Index = i
        v.m_CardNum = num
        v:MoveToNewPos()
    end
    card:Disappear()
end

function LuaShengXiaoCardPlayerView:OnQuitCCStream(...)
    self.VoiceButton:SetActive(false)
end
function LuaShengXiaoCardPlayerView:ShowLoseEffect()
    if self.m_Ticks["lose"] then UnRegisterTick(self.m_Ticks["lose"]) self.m_Ticks["lose"]=nil end

    self.Fx:DestroyFx()
    self.Fx:LoadFx("Fx/UI/Prefab/UI_hwcb_lose001.prefab")
    self.m_Ticks["lose"] = RegisterTickOnce(function()
        self.m_Ticks["lose"] = nil
        self.Fx:DestroyFx()
    end,3000)
end
function LuaShengXiaoCardPlayerView:ShowRoundEndEffect(right)
    self.RoundEndFx:DestroyFx()
    if self.m_ViewIndex==1 then
        if right then
            self.RoundEndFx:LoadFx("Fx/UI/Prefab/UI_hwcb_caiduile001.prefab")
            SoundManager.Inst:PlayOneShot(LuaShengXiaoCardMgr.Sounds.right, Vector3(0,0,0),nil,0)
        else
            self.RoundEndFx:LoadFx("Fx/UI/Prefab/UI_hwcb_caicuole001.prefab")
            SoundManager.Inst:PlayOneShot(LuaShengXiaoCardMgr.Sounds.wrong, Vector3(0,0,0),nil,0)
        end
    end
end
function LuaShengXiaoCardPlayerView:ShowExpression(id)
    if self.m_Ticks["expression"] then UnRegisterTick(self.m_Ticks["expression"]) self.m_Ticks["expression"] = nil end

    self.ExpressionLabel.text = "#"..tostring(id)
    self.m_Ticks["expression"] = RegisterTickOnce(function()
        self.m_Ticks["expression"] = nil
        self.ExpressionLabel.text = nil
    end,3000)
end

function LuaShengXiaoCardPlayerView:OnDestroy()
    for k,v in pairs(self.m_Ticks) do
        UnRegisterTick(v)
        self.m_Ticks[k] = nil
    end
end
