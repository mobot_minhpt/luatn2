local UITabBar = import "L10.UI.UITabBar"

local QnTableView = import "L10.UI.QnTableView"

local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUITexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"
local UILabel = import "UILabel"
local QnCheckBox = import "L10.UI.QnCheckBox"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local AlignType2=import "L10.UI.CPopupMenuInfoMgr+AlignType"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local DefaultItemActionDataSource=import "L10.UI.DefaultItemActionDataSource"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CQingQiuShiJianWnd = import "L10.UI.CQingQiuShiJianWnd"

LuaSeaFishingPackageWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSeaFishingPackageWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaSeaFishingPackageWnd, "TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaSeaFishingPackageWnd, "TuJianBtn", "TuJianBtn", GameObject)
RegistChildComponent(LuaSeaFishingPackageWnd, "SeaFishingShopBtn", "SeaFishingShopBtn", GameObject)
RegistChildComponent(LuaSeaFishingPackageWnd, "NormalButtons", "NormalButtons", GameObject)
RegistChildComponent(LuaSeaFishingPackageWnd, "PutBackButtons", "PutBackButtons", GameObject)
RegistChildComponent(LuaSeaFishingPackageWnd, "ChangeScoreButtons", "ChangeScoreButtons", GameObject)
RegistChildComponent(LuaSeaFishingPackageWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaSeaFishingPackageWnd, "CanChangeScoreLabel", "CanChangeScoreLabel", UILabel)
RegistChildComponent(LuaSeaFishingPackageWnd, "ArrangeBtn", "ArrangeBtn", GameObject)
RegistChildComponent(LuaSeaFishingPackageWnd, "TakeOutViewBtn", "TakeOutViewBtn", GameObject)
RegistChildComponent(LuaSeaFishingPackageWnd, "ChangeViewBtn", "ChangeViewBtn", GameObject)
RegistChildComponent(LuaSeaFishingPackageWnd, "ChangeBackNormalBtn", "ChangeBackNormalBtn", GameObject)
RegistChildComponent(LuaSeaFishingPackageWnd, "ChangeBtn", "ChangeBtn", GameObject)
RegistChildComponent(LuaSeaFishingPackageWnd, "PutBackNormalBtn", "PutBackNormalBtn", GameObject)
RegistChildComponent(LuaSeaFishingPackageWnd, "TakeOutBtn", "TakeOutBtn", GameObject)
RegistChildComponent(LuaSeaFishingPackageWnd, "SelectAllCheckbox", "SelectAllCheckbox", QnCheckBox)
RegistChildComponent(LuaSeaFishingPackageWnd, "SelectAllNormalFishCheckbox", "SelectAllNormalFishCheckbox", QnCheckBox)
RegistChildComponent(LuaSeaFishingPackageWnd, "SortbyLabel", "SortbyLabel", UILabel)
RegistChildComponent(LuaSeaFishingPackageWnd, "ShowSortbyViewBtn", "ShowSortbyViewBtn", GameObject)
RegistChildComponent(LuaSeaFishingPackageWnd, "SortView", "SortView", GameObject)
RegistChildComponent(LuaSeaFishingPackageWnd, "FishAlert", "FishAlert", GameObject)
RegistChildComponent(LuaSeaFishingPackageWnd, "OtherAlert", "OtherAlert", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaSeaFishingPackageWnd, "m_PackageList")
RegistClassMember(LuaSeaFishingPackageWnd, "m_FishList")
RegistClassMember(LuaSeaFishingPackageWnd, "m_OtherList")
RegistClassMember(LuaSeaFishingPackageWnd, "m_SelectBarIndex")

RegistClassMember(LuaSeaFishingPackageWnd, "m_CurStatus")
RegistClassMember(LuaSeaFishingPackageWnd, "m_SelectedItemList")
RegistClassMember(LuaSeaFishingPackageWnd, "m_BasketReorderType")
RegistClassMember(LuaSeaFishingPackageWnd, "m_IsSelectAll")
RegistClassMember(LuaSeaFishingPackageWnd, "m_IsSelectAllNormalFish")

RegistClassMember(LuaSeaFishingPackageWnd, "m_MaxSlotCount")
RegistClassMember(LuaSeaFishingPackageWnd, "m_CurSlotCount")
RegistClassMember(LuaSeaFishingPackageWnd, "m_MoveToBagTick")
RegistClassMember(LuaSeaFishingPackageWnd, "m_ChangeScoreTick")
RegistClassMember(LuaSeaFishingPackageWnd, "m_BasketType")

RegistClassMember(LuaSeaFishingPackageWnd, "m_ExchangeCount")
RegistClassMember(LuaSeaFishingPackageWnd, "m_TakeOutCount")
RegistClassMember(LuaSeaFishingPackageWnd, "m_NeedExchangeCount")
RegistClassMember(LuaSeaFishingPackageWnd, "m_NeedTakeOutCount")
RegistClassMember(LuaSeaFishingPackageWnd, "m_CurRefreshType")--1 takeout 2 score
RegistClassMember(LuaSeaFishingPackageWnd, "m_ShowFishAlert")
RegistClassMember(LuaSeaFishingPackageWnd, "m_ShowOtherAlert")

function LuaSeaFishingPackageWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TuJianBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTuJianBtnClick()
	end)


	
	UIEventListener.Get(self.SeaFishingShopBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSeaFishingShopBtnClick()
	end)


	
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)


	
	UIEventListener.Get(self.ArrangeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnArrangeBtnClick()
	end)


	
	UIEventListener.Get(self.TakeOutViewBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTakeOutViewBtnClick()
	end)


	
	UIEventListener.Get(self.ChangeViewBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChangeViewBtnClick()
	end)


	
	UIEventListener.Get(self.ChangeBackNormalBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChangeBackNormalBtnClick()
	end)


	
	UIEventListener.Get(self.ChangeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChangeBtnClick()
	end)


	
	UIEventListener.Get(self.PutBackNormalBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPutBackNormalBtnClick()
	end)


	
	UIEventListener.Get(self.TakeOutBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTakeOutBtnClick()
	end)


	
	UIEventListener.Get(self.ShowSortbyViewBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShowSortbyViewBtnClick(go)
	end)


    --@endregion EventBind end
    self.m_MaxSlotCount = HouseFish_Setting.GetData().MaxFish 
    self.m_ExchangeCount = 0
    self.m_TakeOutCount = 0
    self.m_NeedExchangeCount = 0
    self.m_NeedTakeOutCount = 0
    self.m_ShowFishAlert = false
    self.m_ShowOtherAlert = false
end

function LuaSeaFishingPackageWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateFishBasket",self,"OnUpdateFishBasket")
    g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate",self,"OnMainPlayerPlayPropUpdate")
    g_ScriptEvent:AddListener("RemoveShinningHouseFishItem",self,"OnRemoveShinningHouseFishItem")
    g_ScriptEvent:AddListener("MarkNewShinningHouseFishItem",self,"OnMarkNewShinningHouseFishItem")
end

function LuaSeaFishingPackageWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateFishBasket",self,"OnUpdateFishBasket")
    g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate",self,"OnMainPlayerPlayPropUpdate")
    g_ScriptEvent:RemoveListener("RemoveShinningHouseFishItem",self,"OnRemoveShinningHouseFishItem")
    g_ScriptEvent:RemoveListener("MarkNewShinningHouseFishItem",self,"OnMarkNewShinningHouseFishItem")
    if self.m_MoveToBagTick then
        UnRegisterTick(self.m_MoveToBagTick)
        self.m_MoveToBagTick = nil
    end
    if self.m_ChangeScoreTick then
        UnRegisterTick(self.m_ChangeScoreTick)
        self.m_ChangeScoreTick = nil
    end
    LuaZhenZhaiYuMgr.m_LastPackageIndex = 0
    g_ScriptEvent:BroadcastInLua("RemoveShinningHouseFishItem",nil)
end

function LuaSeaFishingPackageWnd:Init()
    if CUIManager.IsLoaded(CIndirectUIResources.ItemInfoWnd) then
		CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
	end

    self.m_PackageList = {}
    self.m_FishList = {}
    self.m_OtherList = {}
    self.m_SelectedItemList = {}

    --slot
    self.m_CurSlotCount = self:GetUnlockedSlotCount()

    self:InitFishBasket()
    self.m_CurStatus = 0
    self:InitButtons()

    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self.m_MaxSlotCount
        end,
        function(item,row)
            self:InitLeftItem(item,row)
        end)

    self.TableView:ReloadData(true,true)

    self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self.m_SelectBarIndex = index
        self.m_SelectedItemList = {}
        self.SelectAllCheckbox:SetSelected(false,true)
        self.SelectAllNormalFishCheckbox:SetSelected(false,true)
        self.TableView:ReloadData(true, true)
        --self.SortView:SetActive(index==0)
        if self.m_SelectBarIndex == 0 then--鱼
            self.m_BasketType = EnumFishBasketType.Fish
        else--其他
            self.m_BasketType = EnumFishBasketType.Other
        end
        LuaZhenZhaiYuMgr.m_LastPackageIndex = index
    end)

    self.TabBar:ChangeTab(LuaZhenZhaiYuMgr.m_LastPackageIndex, false)
    self.m_SelectBarIndex = LuaZhenZhaiYuMgr.m_LastPackageIndex
    self.m_BasketReorderType = 1

    self.m_IsSelectAll = false
    self.m_IsSelectAllNormalFish = false
    self.SelectAllCheckbox:SetSelected(self.m_IsSelectAll,false)
    self.SelectAllNormalFishCheckbox:SetSelected(self.m_IsSelectAllNormalFish,false)

    self.SelectAllCheckbox.OnValueChanged = DelegateFactory.Action_bool(function (value)
		self.m_IsSelectAll = value
        for index,selected in pairs(self.m_SelectedItemList) do
            self.m_SelectedItemList[index] = value
        end
        self.TableView:ReloadData(true,true)
	end)

    self.SelectAllNormalFishCheckbox.OnValueChanged = DelegateFactory.Action_bool(function (value)
		self.m_IsSelectAllNormalFish = value
        if self.m_SelectBarIndex == 0 then--鱼
            for index,selected in pairs(self.m_SelectedItemList) do
                local fish = self.m_FishList[index]
                local data = HouseFish_AllFishes.GetData(fish.itemId)
                if data and data.IsGuanShang ~= 1 and data.IsZhenZhai ~= 1 then
                    self.m_SelectedItemList[index] = value 
                else
                    self.m_SelectedItemList[index] = false
                end
            end
            self:CalculateSelectScore()
            self.TableView:ReloadData(true,true)
        end
	end)
end

function LuaSeaFishingPackageWnd:GetUnlockedSlotCount()
    local cls = HouseFish_Setting.GetData().FishSkillId
	local curSkillId = CClientMainPlayer.Inst.SkillProp:GetSkillIdWithDeltaByCls(cls, CClientMainPlayer.Inst.Level)
	local skillLevel = CLuaDesignMgr.Skill_AllSkills_GetLevel(curSkillId)

    local slotCount = 0
    local formulaId = HouseFish_Setting.GetData().BasketSlotNumFormulaId
    local formula = AllFormulas.Action_Formula[formulaId].Formula
    if formula ~= nil then
        slotCount = formula(nil, nil, {skillLevel})
    end
    return slotCount
end

function LuaSeaFishingPackageWnd:InitButtons()
    self.NormalButtons:SetActive(self.m_CurStatus == 0)--正常状态
    self.PutBackButtons:SetActive(self.m_CurStatus == 1)--取出多选界面
    self.ChangeScoreButtons:SetActive(self.m_CurStatus == 2)--兑换多选界面
end

function LuaSeaFishingPackageWnd:InitFishBasket()
    local fishbasket = CClientMainPlayer.Inst.ItemProp.FishBasket
	if not fishbasket then return end 
	local itemIdArray = fishbasket.Fishs
	local countArray = fishbasket.Count
    local otherArray = fishbasket.Others
    local otherCountArray = fishbasket.OtherCount
    local shinningFish = fishbasket.ShinningFish
    local shinningOther = fishbasket.ShinningOther
    --red dot
    self.m_ShowFishAlert = false
    self.m_ShowOtherAlert = false
    
	for i=1,itemIdArray.Length-1,1 do
		local t = {}
		t.itemId = itemIdArray[i]
		t.count = countArray[i]
		t.fishbagpos = i
        t.bagType = LuaZhenZhaiYuMgr.BagType.Fish
        
		local data = HouseFish_AllFishes.GetData(t.itemId)        
        --鱼
		if data and t.count>0 then
            self.m_FishList[i] = t
            local fishIdx = data.ShinningIdx
            t.needShinning = shinningFish:GetBit(fishIdx)
		end
        --red dot
        if t.needShinning then
            self.m_ShowFishAlert = true
        end
	end
    for i=1,otherArray.Length-1,1 do
		local t = {}
		t.itemId = otherArray[i]
		t.count = otherCountArray[i]
		t.fishbagpos = i
        t.bagType = LuaZhenZhaiYuMgr.BagType.Other
		local data = HouseFish_AllOther.GetData(t.itemId)
        if data and t.count>0 then
            self.m_OtherList[i] = t
            local idx = data.ShinningIdx
            t.needShinning = shinningOther:GetBit(idx)
		end
        --red dot
        if t.needShinning then
            self.m_ShowOtherAlert = true
        end
	end

    self:InitRedDots()
end

function LuaSeaFishingPackageWnd:InitRedDots()
    self.FishAlert:SetActive(self.m_ShowFishAlert)
    self.OtherAlert:SetActive(self.m_ShowOtherAlert)
end

function LuaSeaFishingPackageWnd:InitLeftItem(item,row)
    local unlock = item.transform:Find("Unlock").gameObject
    local lock = item.transform:Find("Lock").gameObject
    local iconTexture = item.transform:Find("Unlock/IconTexture"):GetComponent(typeof(CUITexture))
    local qualitySprite = item.transform:Find("Unlock/QualitySprite"):GetComponent(typeof(UISprite))
    local bindSprite = item.transform:Find("Unlock/BindSprite")
    local bindLabel = item.transform:Find("Unlock/BindSprite/Label"):GetComponent(typeof(UILabel))
    local amountLabel = item.transform:Find("Unlock/AmountLabel"):GetComponent(typeof(UILabel))
    local checkBox = item.transform:Find("Unlock/Checkbox"):GetComponent(typeof(QnCheckBox))
    local border = item.transform:Find("Unlock/Border"):GetComponent(typeof(UISprite))
    local fxNode = item.transform:Find("Unlock/FxNode"):GetComponent(typeof(CUIFx))
    local isLocked = false
    local isEmpty = false
    fxNode:DestroyFx()
    if self.m_SelectBarIndex == 0 then--鱼
        if row + 1 > self.m_CurSlotCount then--未解锁
            isLocked = true
        elseif self.m_FishList[row+1] == nil then
            isEmpty = true
        end
    else--其他
        if row + 1 > self.m_CurSlotCount then--未解锁
            isLocked = true
        elseif self.m_OtherList[row+1] == nil then
            isEmpty = true
        end
    end

    lock:SetActive(isLocked)
    unlock:SetActive(not isLocked)
    if isLocked then
        UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            g_MessageMgr:ShowMessage("Upgrade_SeaFishingSkill_ForMoreSlot")
        end)
        return
    elseif isEmpty then
        UIEventListener.Get(item.gameObject).onClick = nil
        qualitySprite.enabled = false
        bindSprite.gameObject:SetActive(false)
        amountLabel.text = nil
        checkBox.gameObject:SetActive(false)
        iconTexture.texture.enabled = false
        border.enabled = false
        return
    end
    border.enabled = true
    qualitySprite.enabled = true
    iconTexture.texture.enabled = true
    checkBox:SetSelected(self.m_SelectedItemList[row+1]==true,false)

    local info = nil
    if self.m_SelectBarIndex == 0 then--鱼
        info = self.m_FishList[row + 1]
        bindSprite.gameObject:SetActive(true)
    else--其他
        info = self.m_OtherList[row + 1]
        bindSprite.gameObject:SetActive(false)
    end
    local data = Item_Item.GetData(info.itemId)
    if data then
        iconTexture:LoadMaterial(data.Icon)
        amountLabel.text = info.count
        qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(data.Quality)
        border.spriteName = CUICommonDef.GetItemCellBorder(data.NameColor)
    else
        item.gameObject:SetActive(false)
    end

    local fishData = HouseFish_AllFishes.GetData(info.itemId)
    if fishData and fishData.IsZhenZhai == 1 then
        bindLabel.text = LocalString.GetString("佑")
    elseif fishData and fishData.IsGuanShang == 1 then
        bindLabel.text = LocalString.GetString("观")
    else
        bindSprite.gameObject:SetActive(false)
    end

    if self.m_CurStatus == 0 then--正常状态
        checkBox.gameObject:SetActive(false)
    elseif self.m_CurStatus == 1 then--取出多选界面
        checkBox.gameObject:SetActive(true)
    elseif self.m_CurStatus == 2 then--兑换多选界面
        local otherData = HouseFish_AllOther.GetData(info.itemId)
        if otherData and otherData.Level == 0 then
            checkBox.gameObject:SetActive(false)
        else
            checkBox.gameObject:SetActive(true)
        end
    end
    
    if info and info.needShinning then
        fxNode:DestroyFx()
        fxNode:LoadFx(CQingQiuShiJianWnd.s_AlertFxPath)
    else
        fxNode:DestroyFx()
    end

    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if self.m_CurStatus ~= 0 then--正常状态
            self.m_SelectedItemList[row+1] = not self.m_SelectedItemList[row+1]
            checkBox:SetSelected(self.m_SelectedItemList[row+1]==true,false)
        else
            local actions = self:GetActionsFormItemId(info)
            CItemInfoMgr.ShowLinkItemTemplateInfo(info.itemId,false,actions,AlignType.Default, 0, 0, 0, 0)
        end  
        if info.needShinning then
            Gac2Gas.RemoveShinningHouseFishItem(info.itemId)
        end    
	end)
    
    checkBox.OnValueChanged = DelegateFactory.Action_bool(function (value)
		self.m_SelectedItemList[row+1] = value
        self:CalculateSelectScore()
	end)
end

function LuaSeaFishingPackageWnd:GetActionsFormItemId(info)
    local itemId = info.itemId
    local names={}
    local actions={}

    local fishData = HouseFish_AllFishes.GetData(itemId)
    local otherData = HouseFish_AllOther.GetData(itemId) 
    if not fishData and not otherData then
        return DefaultItemActionDataSource.Create(#names,actions,names)
    end
    local isGuanShang = fishData and fishData.IsGuanShang == 1 or false
    local isZhenZhai = fishData and fishData.IsZhenZhai == 1 or false
    if isGuanShang then
        table.insert( names,LocalString.GetString("放入装修仓库"))
        table.insert( actions,function()
            Gac2Gas.MoveFishFromBasketToHouseRepo(info.fishbagpos)--fishbagpos
        end)      
    end
    if isZhenZhai then
        table.insert( names,LocalString.GetString("佑宅福鱼"))
        table.insert( actions,function()
            CUIManager.ShowUI(CLuaUIResources.ZhenZhaiYuWnd)
        end)
    end

    local canFangSheng = LuaZhenZhaiYuMgr.CheckCanFangSheng(info.itemId)
    if canFangSheng then
        table.insert( names,LocalString.GetString("放生"))
        table.insert( actions,function()
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("FISH_FREE_CONFIRM"),
            DelegateFactory.Action(function ()
                Gac2Gas.RequestFangShengZhengzhaiFishInBasket(info.fishbagpos) 
            end),
            nil,
            LocalString.GetString("确定"), LocalString.GetString("取消"), false)
        end)
    end

    if not (otherData and otherData.Level == 0) then
        table.insert( names,LocalString.GetString("兑换积分"))
        table.insert( actions,function()
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("FISH_USE_CONFIRM"),
                DelegateFactory.Action(function ()
                    self.m_ExchangeCount = 0
                    self.m_NeedExchangeCount = 1
                    self.m_CurRefreshType = 2
                    Gac2Gas.RequestExchangeFishToScore(info.bagType,info.fishbagpos,1)
                end),
                nil,
                LocalString.GetString("确定"), LocalString.GetString("取消"), false)
        end)
    end

    table.insert( names,LocalString.GetString("取出"))
    table.insert( actions,function()
        self.m_TakeOutCount = 0
        self.m_NeedTakeOutCount = 1
        self.m_CurRefreshType = 1
        Gac2Gas.MoveFishBasketToBag(self.m_BasketType,info.fishbagpos,1) 
    end)
               
    return DefaultItemActionDataSource.Create(#names,actions,names)
end
--@region UIEvent

--整理包裹
function LuaSeaFishingPackageWnd:OnTuJianBtnClick()
    LuaSeaFishingMgr.ShowTuJianWnd()
end

function LuaSeaFishingPackageWnd:OnSeaFishingShopBtnClick()
    CLuaNPCShopInfoMgr.ShowScoreShop("FishingScore")
end

function LuaSeaFishingPackageWnd:OnShowSortbyViewBtnClick(go)
    local function SelectAction(index)
        self:OnSortItem(index)    
    end

    local selectShareItem=DelegateFactory.Action_int(SelectAction)
    local t={}
    local item1=PopupMenuItemData(LocalString.GetString("等级"),selectShareItem,false,nil)
    local item2=PopupMenuItemData(LocalString.GetString("类型"),selectShareItem,false,nil)

    if self.m_SelectBarIndex == 0 then--鱼
        table.insert(t, item1)
        table.insert(t, item2)
    else--其他 只支持按等级排序
        table.insert(t, item1)
    end

    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType2.Bottom,1,nil,600,true,266)
end

function LuaSeaFishingPackageWnd:OnSortItem(index)
    local name = {LocalString.GetString("等级"),LocalString.GetString("类型")}
    self.SortbyLabel.text = name[index+1]

    self.m_BasketReorderType = index + 1
    self:OnArrangeBtnClick()
end


function LuaSeaFishingPackageWnd:OnTipBtnClick()
    g_MessageMgr:ShowMessage("SeaFishing_Package_Tip")
end

function LuaSeaFishingPackageWnd:OnArrangeBtnClick()
    --{"BasketReorder", "II", 100, nil, "lua"}, -- reorder basket type, reorder field
    local basketType = self.m_BasketType
    Gac2Gas.BasketReorder(basketType,self.m_BasketReorderType)
end

function LuaSeaFishingPackageWnd:OnTakeOutViewBtnClick()
    self.m_CurStatus = 1
    self:InitButtons()
    self.TableView:ReloadData(true,true)
end

function LuaSeaFishingPackageWnd:OnChangeViewBtnClick()
    self.m_CurStatus = 2
    self:InitButtons()
    self.TableView:ReloadData(true,true)
end


function LuaSeaFishingPackageWnd:OnChangeBackNormalBtnClick()
    self.m_CurStatus = 0
    self:InitButtons()
    self.m_SelectedItemList = {}
    self.SelectAllCheckbox:SetSelected(false,true)
    self.SelectAllNormalFishCheckbox:SetSelected(false,true)
    self.TableView:ReloadData(true,true)
end

function LuaSeaFishingPackageWnd:OnChangeBtnClick()
    --RequestExchangeFishToScore fishPos
    self.m_ExchangeCount = 0
    local list
    if self.m_SelectBarIndex == 0 then--鱼
        list = self.m_FishList
    else--其他
        list = self.m_OtherList
    end

    local changeScoreList = {}
    local hasSpecial = false
    for index,selected in pairs(self.m_SelectedItemList) do
        if selected == true then
            local fish = list[index]
            table.insert(changeScoreList,index)
            if LuaZhenZhaiYuMgr.CheckCanFangSheng(fish.itemId) then
                hasSpecial = true
            end
            local data = HouseFish_AllFishes.GetData(fish.itemId)
            if data and (data.IsZhenZhai == 1 or data.IsGuanShang == 1) then
                hasSpecial = true
            end
        end
    end
    self.m_NeedExchangeCount = #changeScoreList
    self.m_CurRefreshType = 2
    local changeFunc = function()
        if self.m_ChangeScoreTick then
            g_MessageMgr:ShowMessage("RPC_TOO_FREQUENT")
        else
            local i=1
            self.m_ChangeScoreTick = RegisterTickWithDuration(function ()
                local index = changeScoreList[i]
                local fishPos = list[index].fishbagpos
                local count = list[index].count
                local bagType = list[index].bagType
                Gac2Gas.RequestExchangeFishToScore(bagType,fishPos,count) 
                i = i+1
                if i== #changeScoreList+1 then
                    UnRegisterTick(self.m_ChangeScoreTick)
                    self.m_ChangeScoreTick = nil
                end
            end,10,10*#changeScoreList)
        end
    end

    if hasSpecial then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("HaveSpecialFish_ChangeScore_Comfire"),
            DelegateFactory.Action(function ()
                changeFunc()
            end),
            nil,
            LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    else
        changeFunc()
    end
end

function LuaSeaFishingPackageWnd:OnPutBackNormalBtnClick()
    self.m_CurStatus = 0
    self:InitButtons()
    self.m_SelectedItemList = {}
    self.SelectAllCheckbox:SetSelected(false,true)
    self.SelectAllNormalFishCheckbox:SetSelected(false,true)
    self.TableView:ReloadData(true,true)
end

function LuaSeaFishingPackageWnd:OnTakeOutBtnClick()
    --MoveFishBasketToBag basketType, fishPos
    self.m_TakeOutCount = 0
    local list
    if self.m_SelectBarIndex == 0 then--鱼
        list = self.m_FishList
    else--其他
        list = self.m_OtherList
    end
    local basketType = self.m_BasketType

    local moveToBagList = {}
    for index,selected in pairs(self.m_SelectedItemList) do
        if selected == true then
            local fish = list[index]
            table.insert(moveToBagList,index)
        end
    end
    self.m_NeedTakeOutCount = #moveToBagList
    self.m_CurRefreshType = 1
    if self.m_MoveToBagTick then
        g_MessageMgr:ShowMessage("RPC_TOO_FREQUENT")
    else
        local i=1
        self.m_MoveToBagTick = RegisterTickWithDuration(function ()
            local index = moveToBagList[i]
            local fishPos = list[index].fishbagpos
            local count = list[index].count
            Gac2Gas.MoveFishBasketToBag(basketType,fishPos,count) 
            i = i+1
            if i== #moveToBagList+1 then
                UnRegisterTick(self.m_MoveToBagTick)
                self.m_MoveToBagTick = nil
            end
        end,10,10*#moveToBagList)
    end
    
end

function LuaSeaFishingPackageWnd:CalculateSelectScore()
    if self.m_CurStatus ~= 2 then return end
    local list
    if self.m_SelectBarIndex == 0 then--鱼
        list = self.m_FishList
    else--其他
        list = self.m_OtherList
    end
    local score = 0
    for index,selected in pairs(self.m_SelectedItemList) do
        if selected == true then
            local fish = list[index]
            local itemId = fish.itemId
            local data
            if self.m_SelectBarIndex == 0 then--鱼
                data = HouseFish_AllFishes.GetData(itemId)
            else--其他
                data = HouseFish_AllOther.GetData(itemId)
            end
            score = score + data.Score * fish.count
        end
    end
    self.CanChangeScoreLabel.text = score
end


--@endregion UIEvent

--整理包裹

function LuaSeaFishingPackageWnd:OnUpdateFishBasket(fishBasketType)
	CClientMainPlayer.Inst.ItemProp.FishBasket = fishBasketType
    
    if self.m_CurRefreshType == 1 then
        self.m_TakeOutCount = self.m_TakeOutCount + 1
        if self.m_TakeOutCount >= self.m_NeedTakeOutCount and self.m_NeedTakeOutCount ~= 0 then
            self.m_CurRefreshType = 0
            self:Init()
        end
    elseif self.m_CurRefreshType == 2 then
        if self.m_ExchangeCount >= self.m_NeedExchangeCount and self.m_NeedExchangeCount ~= 0 then
            self.m_CurRefreshType = 0
            self:Init()
        end
    else
        self:Init()
    end
end

function LuaSeaFishingPackageWnd:OnMainPlayerPlayPropUpdate()
    self.m_ExchangeCount = self.m_ExchangeCount + 1
end

function LuaSeaFishingPackageWnd:OnRemoveShinningHouseFishItem(templateId)
    self:InitFishBasket()
    self.TableView:ReloadData(false,false)
end

function LuaSeaFishingPackageWnd:OnMarkNewShinningHouseFishItem(templateId)
    self:InitFishBasket()
    self.TableView:ReloadData(false,false)
end


