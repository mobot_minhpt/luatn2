local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local EnumTempPlayDataKey = import "L10.Game.EnumTempPlayDataKey"

LuaYuanDan2022FuDanGamePlayWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYuanDan2022FuDanGamePlayWnd, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaYuanDan2022FuDanGamePlayWnd, "Table", "Table", UITable)
RegistChildComponent(LuaYuanDan2022FuDanGamePlayWnd, "TemplateNode", "TemplateNode", GameObject)
RegistChildComponent(LuaYuanDan2022FuDanGamePlayWnd, "TeamMatchButton", "TeamMatchButton", GameObject)
RegistChildComponent(LuaYuanDan2022FuDanGamePlayWnd, "SingleMatchButton", "SingleMatchButton", GameObject)
RegistChildComponent(LuaYuanDan2022FuDanGamePlayWnd, "CancelMatchButton", "CancelMatchButton", GameObject)
RegistChildComponent(LuaYuanDan2022FuDanGamePlayWnd, "CancelMatchRoot", "CancelMatchRoot", GameObject)
RegistChildComponent(LuaYuanDan2022FuDanGamePlayWnd, "MatchNumLabel", "MatchNumLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaYuanDan2022FuDanGamePlayWnd,"m_IsLeader")
RegistClassMember(LuaYuanDan2022FuDanGamePlayWnd,"m_IsSignUp")

function LuaYuanDan2022FuDanGamePlayWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TeamMatchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTeamMatchButtonClick()
	end)


	
	UIEventListener.Get(self.SingleMatchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSingleMatchButtonClick()
	end)


	
	UIEventListener.Get(self.CancelMatchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelMatchButtonClick()
	end)


    --@endregion EventBind end
end

function LuaYuanDan2022FuDanGamePlayWnd:Init()
    self.TemplateNode:SetActive(false)
	self.m_IsSignUp = false
	self:UpdateButtons()
	local dailyAwardTimes = 0
	if CClientMainPlayer.Inst then
		local data = CClientMainPlayer.Inst.PlayProp:GetTempPlayStringData(EnumToInt(EnumTempPlayDataKey.eYuanDan2022FuDanTimes))
		if data then
			dailyAwardTimes = tonumber(data)
		end
	end
	self.MatchNumLabel.text = SafeStringFormat3(LocalString.GetString("今日奖励次数%d/%d"),dailyAwardTimes,YuanDan2022_Setting.GetData().FuDanAwardPerDay)
	if CClientMainPlayer.Inst then
        Gac2Gas.RequestQeuryPlayerYuanDan2022FuDanSignUpInfo(CClientMainPlayer.Inst.Id)
    end
	Extensions.RemoveAllChildren(self.Table.transform)
	local msg = g_MessageMgr:FormatMessage("YuanDan2022FuDanGamePlayWnd_ReadMe")
	if System.String.IsNullOrEmpty(msg) then
        return
    end
	local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then
        return
    end
	for i = 0,info.paragraphs.Count - 1 do
		local paragraphGo = CUICommonDef.AddChild(self.Table.gameObject, self.TemplateNode)
		paragraphGo:SetActive(true)
		CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem)):Init(info.paragraphs[i], 4294967295)
	end
	self.ScrollView:ResetPosition()
    self.Table:Reposition()
end

function LuaYuanDan2022FuDanGamePlayWnd:OnEnable()
	g_ScriptEvent:AddListener("OnSyncPlayerYuanDan2022FuDanSignUpResult",self,"OnSyncPlayerYuanDan2022FuDanSignUpResult")
	g_ScriptEvent:AddListener("OnQueryPlayerYuanDan2022FuDanSignUpInfoResult",self,"OnQueryPlayerYuanDan2022FuDanSignUpInfoResult")
end

function LuaYuanDan2022FuDanGamePlayWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSyncPlayerYuanDan2022FuDanSignUpResult",self,"OnSyncPlayerYuanDan2022FuDanSignUpResult")
	g_ScriptEvent:RemoveListener("OnQueryPlayerYuanDan2022FuDanSignUpInfoResult",self,"OnQueryPlayerYuanDan2022FuDanSignUpInfoResult")
end

function LuaYuanDan2022FuDanGamePlayWnd:OnSyncPlayerYuanDan2022FuDanSignUpResult(isMatching)
	self.m_IsSignUp = isMatching
	self:UpdateButtons()
end

function LuaYuanDan2022FuDanGamePlayWnd:OnQueryPlayerYuanDan2022FuDanSignUpInfoResult(playerId, isInMatching)
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == playerId then
		self.m_IsSignUp = isInMatching
		self:UpdateButtons()
	end
end

function LuaYuanDan2022FuDanGamePlayWnd:UpdateButtons()
	self.SingleMatchButton.gameObject:SetActive(false)
	self.TeamMatchButton.gameObject:SetActive(not self.m_IsSignUp)
	self.CancelMatchRoot.gameObject:SetActive(self.m_IsSignUp)
end

--@region UIEvent

function LuaYuanDan2022FuDanGamePlayWnd:OnTeamMatchButtonClick()
    Gac2Gas.RequestSignUpYuanDan2022FuDan()
end

function LuaYuanDan2022FuDanGamePlayWnd:OnSingleMatchButtonClick()
    Gac2Gas.RequestSignUpYuanDan2022FuDan()
end

function LuaYuanDan2022FuDanGamePlayWnd:OnCancelMatchButtonClick()
    Gac2Gas.RequestCancelSignUpYuanDan2022FuDan()
end


--@endregion UIEvent

