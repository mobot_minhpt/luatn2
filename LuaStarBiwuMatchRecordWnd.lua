local QnButton=import "L10.UI.QnButton"
local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"


CLuaStarBiwuMatchRecordWnd = class()
RegistClassMember(CLuaStarBiwuMatchRecordWnd,"m_WatchBtn")
RegistClassMember(CLuaStarBiwuMatchRecordWnd,"m_AdvGridView")
RegistClassMember(CLuaStarBiwuMatchRecordWnd,"m_CurrentRow")
RegistClassMember(CLuaStarBiwuMatchRecordWnd,"m_DataSource")

function CLuaStarBiwuMatchRecordWnd:Awake()
    self.m_WatchBtn = self.transform:Find("ShowArea/ViewButton"):GetComponent(typeof(QnButton))
    self.m_AdvGridView = self.transform:Find("ShowArea/QnAdvView"):GetComponent(typeof(QnTableView))
    self.m_CurrentRow = -1
end

function CLuaStarBiwuMatchRecordWnd:OnEnable( )
    UIEventListener.Get(self.m_WatchBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnWatchBtnClicked(go) end)
end

function CLuaStarBiwuMatchRecordWnd:Init( )
    self.m_WatchBtn.Enabled = false

    local getNumFunc=function() return #CLuaStarBiwuMgr.m_MatchRecordList end
    local initItemFunc=function(item,index)
        if index % 2 == 1 then
            item:SetBackgroundTexture(g_sprites.OddBgSpirite)
        else
            item:SetBackgroundTexture(g_sprites.EvenBgSprite)
        end

        self:InitItem(item,index,CLuaStarBiwuMgr.m_MatchRecordList[index+1])
    end

    self.m_DataSource=DefaultTableViewDataSource.Create(getNumFunc,initItemFunc)
    self.m_AdvGridView.m_DataSource=self.m_DataSource
    self.m_AdvGridView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self.m_CurrentRow = row
        if CLuaStarBiwuMgr.m_MatchRecordList[self.m_CurrentRow+1].m_Win < 0
            and CLuaStarBiwuMgr.m_MatchRecordList[self.m_CurrentRow+1].m_IsMatching then
            self.m_WatchBtn.Enabled = true
        else
            self.m_WatchBtn.Enabled = false
        end
    end)
    self.m_AdvGridView:ReloadData(true,false)

    local refreshButton=FindChild(self.transform,"RefreshButton").gameObject
    UIEventListener.Get(refreshButton).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.QueryStarBiwuWatchList()
    end)
end
function CLuaStarBiwuMatchRecordWnd:OnWatchBtnClicked( go)
    Gac2Gas.RequestWatchStarBiwuJiFenSai(7, CLuaStarBiwuMgr.m_MatchRecordList[self.m_CurrentRow+1].m_Zhandui1, CLuaStarBiwuMgr.m_MatchRecordList[self.m_CurrentRow+1].m_Zhandui2)
end

function CLuaStarBiwuMatchRecordWnd:InitItem(item,index,record)
    local transform=item.transform
    local m_NameLabel={}
    m_NameLabel[0]=FindChild(transform,"NameLabel1"):GetComponent(typeof(UILabel))
    m_NameLabel[1]=FindChild(transform,"NameLabel2"):GetComponent(typeof(UILabel))
    local m_ResultSprite={}
    m_ResultSprite[0]=FindChild(transform,"ResultSprite1"):GetComponent(typeof(UISprite))
    m_ResultSprite[1]=FindChild(transform,"ResultSprite2"):GetComponent(typeof(UISprite))

    UIEventListener.Get(item.transform:Find("TeamItem1").gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
        CLuaStarBiwuMgr:ShowZhanDuiMemberWnd(0, record.m_Zhandui1, 1)
    end)
    UIEventListener.Get(item.transform:Find("TeamItem2").gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
        CLuaStarBiwuMgr:ShowZhanDuiMemberWnd(0, record.m_Zhandui2, 1)
    end)
    local m_CannotWatchObj = transform:Find("CannotWatchMark").gameObject
    local m_MatchingLabel = transform:Find("Label"):GetComponent(typeof(UILabel))
    m_NameLabel[0].text = record.m_TeamName1
    m_NameLabel[1].text = record.m_TeamName2
    if record.m_Win >= 0 then
        m_CannotWatchObj:SetActive(true)
        m_MatchingLabel.text = LocalString.GetString("VS")
        m_ResultSprite[0].gameObject:SetActive(true)
        m_ResultSprite[1].gameObject:SetActive(true)

        local default
        if record.m_Win == record.m_Zhandui1 then
            default = "common_fight_result_win"
        else
            default = "common_fight_result_lose"
        end
        m_ResultSprite[0].spriteName = default

        local extern
        if record.m_Win == record.m_Zhandui2 then
            extern = "common_fight_result_win"
        else
            extern = "common_fight_result_lose"
        end
        m_ResultSprite[1].spriteName = extern
    else
        if record.m_IsMatching then
            m_MatchingLabel.text = LocalString.GetString("进行中")
            m_ResultSprite[0].gameObject:SetActive(false)
            m_ResultSprite[1].gameObject:SetActive(false)
            m_CannotWatchObj:SetActive(false)
        else
            m_MatchingLabel.text = LocalString.GetString("准备中")
            m_ResultSprite[0].gameObject:SetActive(false)
            m_ResultSprite[1].gameObject:SetActive(false)
            m_CannotWatchObj:SetActive(true)
        end
    end
end
