local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

CLuaStarBiwuRewardWnd = class()
RegistClassMember(CLuaStarBiwuRewardWnd,"m_TableView")
RegistClassMember(CLuaStarBiwuRewardWnd,"m_RewardList")
function CLuaStarBiwuRewardWnd:Init()
    self:InitRewardList()
    local isQuDao = not CLuaStarBiwuMgr:GetPlayerIsGuanFu()
    local data = isQuDao and self.m_RewardList.QuDao or self.m_RewardList.GuanFu

    self.m_TableView=self.transform:Find("TableView"):GetComponent(typeof(QnTableView))
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create2(
        function() 
            return #data
        end,
        function(view,row)
            local item = self.m_TableView:GetFromPool(0)
            local designData = data[row + 1]
            local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))

            local texture = item.transform:Find("Texture"):GetComponent(typeof(CUITexture))
            
            local grid = item.transform:Find("Grid").gameObject

            if designData.Name == LocalString.GetString("冠军") then
                texture:LoadMaterial("UI/Texture/Transparent/Material/starbiwurewardwnd_guanjun.mat")
                nameLabel.text = nil
            elseif designData.Name == LocalString.GetString("亚军") then
                texture:LoadMaterial("UI/Texture/Transparent/Material/starbiwurewardwnd_yajun.mat")
                nameLabel.text = nil
            elseif designData.Name == LocalString.GetString("季军") then
                texture:LoadMaterial("UI/Texture/Transparent/Material/starbiwurewardwnd_jijun.mat")
                nameLabel.text = nil
            else
                texture:Clear()
                nameLabel.text = designData.Name
            end

            local ids = designData.ids
            for i=1,ids.Length,2 do
                local item2 = self.m_TableView:GetFromPool(1)
                item2.transform.parent = grid.transform
                local icon = item2.transform:Find("Texture"):GetComponent(typeof(CUITexture))
                local border = item2.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
                local countLabel = item2.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))

                local id = ids[i-1]
                local count=ids[i]
                countLabel.text=count>1 and tostring(count) or nil
                local itemData = Item_Item.GetData(id)
                if itemData then
                    icon:LoadMaterial(itemData.Icon)
                    border.spriteName = CUICommonDef.GetItemCellBorder(itemData.NameColor)
                end
                UIEventListener.Get(item2.gameObject).onClick = DelegateFactory.VoidDelegate(function(g)
                    CItemInfoMgr.ShowLinkItemTemplateInfo(id)
                end)
            end
            grid:GetComponent(typeof(UIGrid)):Reposition()
            return item
        end)

    self.m_TableView:ReloadData(true,false)
end

function CLuaStarBiwuRewardWnd:InitRewardList()
    self.m_RewardList = {}
    self.m_RewardList.QuDao = {}
    self.m_RewardList.GuanFu = {}
    StarBiWuShow_FinalReward.Foreach(function(k,v)
        if v.RewardID then table.insert(self.m_RewardList.GuanFu,{Name = v.Name, ids = v.RewardID}) end
        if v.QuDaoRewardID then table.insert(self.m_RewardList.QuDao,{Name = v.Name, ids = v.QuDaoRewardID}) end
    end)
end

