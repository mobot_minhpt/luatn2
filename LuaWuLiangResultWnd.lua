local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"

LuaWuLiangResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuLiangResultWnd, "LevelLevel", "LevelLevel", UILabel)
RegistChildComponent(LuaWuLiangResultWnd, "Result", "Result", GameObject)
RegistChildComponent(LuaWuLiangResultWnd, "Info", "Info", GameObject)
RegistChildComponent(LuaWuLiangResultWnd, "Success", "Success", GameObject)
RegistChildComponent(LuaWuLiangResultWnd, "Fail", "Fail", GameObject)
RegistChildComponent(LuaWuLiangResultWnd, "tiaozhanchenggong", "tiaozhanchenggong", GameObject)
RegistChildComponent(LuaWuLiangResultWnd, "tiaozhanfenwei", "tiaozhanfenwei", GameObject)

--@endregion RegistChildComponent end

function LuaWuLiangResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaWuLiangResultWnd:Init()
    local data = LuaWuLiangMgr.resultData

    -- list {EngineId, Heal, DPS, Hp, UnderDamage, Level, Tid, InheritLevel, Attr, SkillLevelTbl}
	local huobanInfo = data["HuoBanInfo"]

	-- Level, Heal, Name, DPS, Id, Class, UnderDamage, Gender
	local playerInfo = data["PlayerInfo"]

	local playId = data["PlayId"]

    self.tiaozhanchenggong:SetActive(data.bSuccess)
    self.tiaozhanfenwei:SetActive(data.bSuccess)

    -- 播放失败动效
    if not data.bSuccess then
        CCommonUIFxWnd.AddUIFx(89000136, 1, false, false)
    end

    -- 设置标题
    -- 查表
    local data = XinBaiLianDong_WuLiangPlay.GetDataBySubKey("GameplayId", playId)
    local diffLabelList = {LocalString.GetString("苦境"), LocalString.GetString("集境"), LocalString.GetString("灭境")}
    local diff = data.Id%10
    local level = math.floor(data.Id/100)
    self.LevelLevel.text = SafeStringFormat3(LocalString.GetString("%s·第%s层"), diffLabelList[diff], Extensions.ConvertToChineseString(level))

    self.m_Level = level
    -- 计算总量
    self.m_SumData = {}
    local sumKey = {"DPS", "Heal", "UnderDamage"}
    for i, key in ipairs(sumKey) do
        self.m_SumData[key] = 0
    end

    local huoBanList = {}
    for k, v in pairs(huobanInfo) do
        for i, key in ipairs(sumKey) do
            if v[key] then
                self.m_SumData[key] = self.m_SumData[key] + v[key]
            end
        end

        -- 重新设置一下数据
        v.MonsterId = k
        table.insert(huoBanList, v)
    end

    for i, key in ipairs(sumKey) do
        if playerInfo[key] then
            self.m_SumData[key] = self.m_SumData[key] + playerInfo[key]
        end
    end

    -- 防止除零错误
    for i, key in ipairs(sumKey) do
        if self.m_SumData[key] == nil or self.m_SumData[key] == 0 then
            self.m_SumData[key] = 1
        end

        print(self.m_SumData[key], i, key)
    end

    -- 设置
    for i= 1, 3 do
        local item = self.Info.transform:Find(tostring(i))

        if i==1 then
            -- 玩家
            self:InitItem(item, playerInfo, true)
        else
            -- 伙伴
            local data = huoBanList[i-1]
            if data then
                self:InitItem(item, data, false)
            else
                item.gameObject:SetActive(false)
            end
        end
    end
end

-- 设置显示
function LuaWuLiangResultWnd:InitItem(item, data, isPlayer)
    local icon = item:Find("Icon"):GetComponent(typeof(CUITexture))
    local nameLabel = item:Find("NameLabel"):GetComponent(typeof(UILabel))
    local cls = item:Find("ClsSprite"):GetComponent(typeof(UISprite))

    local dps = item:Find("Dps"):GetComponent(typeof(UILabel))
    local dpsPercent = item:Find("Dps/Percent"):GetComponent(typeof(UILabel))
    local heal = item:Find("Cure"):GetComponent(typeof(UILabel))
    local healPercent = item:Find("Cure/Percent"):GetComponent(typeof(UILabel))
    local underDamage = item:Find("Bare"):GetComponent(typeof(UILabel))
    local underDamagePercent = item:Find("Bare/Percent"):GetComponent(typeof(UILabel))

    dps.text = math.ceil(data.DPS)
    dpsPercent.text = SafeStringFormat3("(%s%%)", math.floor(data.DPS *100 / self.m_SumData.DPS))

    heal.text = math.ceil(data.Heal)
    healPercent.text = SafeStringFormat3("(%s%%)", math.floor(data.Heal *100 / self.m_SumData.Heal ))

    underDamage.text = math.ceil(data.UnderDamage)
    underDamagePercent.text = SafeStringFormat3("(%s%%)", math.floor(data.UnderDamage *100 / self.m_SumData.UnderDamage ))

    if isPlayer then
        nameLabel.text = data.Name
        if CClientMainPlayer.Inst then
            icon:LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), false)
        end
    else
        local hData = XinBaiLianDong_WuLiangHuoBan.GetData(data.Tid)
        nameLabel.text = hData.Name

        local monsterData = Monster_Monster.GetData(data.Tid)
        icon:LoadNPCPortrait(monsterData.HeadIcon, false)
    end
end

function LuaWuLiangResultWnd:OnEnable()
    if self.m_OnMainPlayerCreatedAction == nil then
        self.m_OnMainPlayerCreatedAction = DelegateFactory.Action(function()
            print(self.m_Level)
            if self.m_Level and self.m_Level <= 5 then
                CUIManager.CloseUI(CLuaUIResources.WuLiangResultWnd)
            end
        end)
    end
    EventManager.AddListenerInternal(EnumEventType.MainPlayerDestroyed, self.m_OnMainPlayerCreatedAction)
end

function LuaWuLiangResultWnd:OnDisable()
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerDestroyed, self.m_OnMainPlayerCreatedAction)
end


--@region UIEvent

--@endregion UIEvent

