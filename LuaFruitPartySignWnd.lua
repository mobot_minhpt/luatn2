local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local UIGrid = import "UIGrid"
local GameObject = import "UnityEngine.GameObject"
local UIScrollView = import "UIScrollView"
local UITable = import "UITable"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local QnButton = import "L10.UI.QnButton"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CUITexture = import "L10.UI.CUITexture"
local MessageMgr = import "L10.Game.MessageMgr"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaFruitPartySignWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaFruitPartySignWnd, "m_TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaFruitPartySignWnd, "m_Grid", "Grid", UIGrid)
RegistChildComponent(LuaFruitPartySignWnd, "m_Templete", "Templete", GameObject)
RegistChildComponent(LuaFruitPartySignWnd, "m_Score", "Score", UILabel)
RegistChildComponent(LuaFruitPartySignWnd, "m_RuleScrollView", "RuleScrollView", UIScrollView)
RegistChildComponent(LuaFruitPartySignWnd, "m_RuleTable", "RuleTable", UITable)
RegistChildComponent(LuaFruitPartySignWnd, "m_ParagraphTemplate", "ParagraphTemplate", GameObject)
RegistChildComponent(LuaFruitPartySignWnd, "m_RankButton", "RankButton", QnButton)
RegistChildComponent(LuaFruitPartySignWnd, "m_SignButton", "SignButton", QnButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaFruitPartySignWnd, "m_Rewards")
RegistClassMember(LuaFruitPartySignWnd, "m_PlayData")

function LuaFruitPartySignWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.m_RankButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankButtonClick()
	end)


	
	UIEventListener.Get(self.m_SignButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSignButtonClick()
	end)


    --@endregion EventBind end
end

function LuaFruitPartySignWnd:Init()
	self.m_TimeLabel.text = ShuiGuoPaiDui_Setting.GetData().Time

	Extensions.RemoveAllChildren(self.m_RuleTable.transform)
    local ruleMessage = MessageMgr.Inst:FormatMessage("FRUITPARTY_SIGNWND_TIP", {})
    local ruleTipInfo = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(ruleMessage)
    if ruleTipInfo ~= nil then
        for i = 0, ruleTipInfo.paragraphs.Count - 1 do
            local rule = CUICommonDef.AddChild(self.m_RuleTable.gameObject, self.m_ParagraphTemplate):GetComponent(typeof(CTipParagraphItem))
            rule.gameObject:SetActive(true)
            rule:Init(ruleTipInfo.paragraphs[i], 0xffffffff)
        end
    end
    self.m_RuleTable:Reposition()

	self.m_PlayData = self:LoadTempPlayData()
	self:InitRewards()
end

--@region UIEvent

function LuaFruitPartySignWnd:OnRankButtonClick()
	CUIManager.ShowUI(CLuaUIResources.FruitPartyRankWnd)
end

function LuaFruitPartySignWnd:OnSignButtonClick()
	Gac2Gas.RequestEnterShuiGuoPaiDui()
end

--@endregion UIEvent

function LuaFruitPartySignWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
end

function LuaFruitPartySignWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
end

function LuaFruitPartySignWnd:InitRewards()
	self.m_Score.text = self.m_PlayData[4]
	self.m_Rewards = {}
	for i = 0, ShuiGuoPaiDui_Setting.GetData().RewardInfo.Length - 1 do
		local reward = CUICommonDef.AddChild(self.m_Grid.gameObject, self.m_Templete)
		reward:SetActive(true)
		table.insert(self.m_Rewards, reward)
	end
	self.m_Grid:Reposition()

	self:RefreshRewards({self.m_PlayData[1],self.m_PlayData[2],self.m_PlayData[3]})
end

function LuaFruitPartySignWnd:RefreshRewards(states)
	local info = {}

	for i = 0, ShuiGuoPaiDui_Setting.GetData().RewardInfo.Length - 1 do
		local data = ShuiGuoPaiDui_Setting.GetData().RewardInfo[i]
		table.insert(info, {data[2], data[1]})
	end

	table.sort(info, function(a, b)
		return a[2] < b[2]
	end)

	for i = 1, #self.m_Rewards do
		local reward = self.m_Rewards[i]
		self:InitReward(reward, info[i], states[i])
	end
end

function LuaFruitPartySignWnd:InitReward(item, info, state)
	local icon 	= item.transform:Find("Clip/Icon"):GetComponent(typeof(CUITexture))
	local mask 	= item.transform:Find("Clip/Mask").gameObject
	local score = item.transform:Find("Score"):GetComponent(typeof(UILabel))

	local id	= info[1]
	local itemData = Item_Item.GetData(id)
	icon:LoadMaterial(itemData.Icon)
	score.text = info[2]

	UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		CItemInfoMgr.ShowLinkItemTemplateInfo(id, false, nil, AlignType.Default, 0, 0, 0, 0)
	end)

	mask:SetActive(state == 1)
	Extensions.SetLocalPositionZ(item.transform, state == 1 and 0 or - 1)
end

function LuaFruitPartySignWnd:OnUpdateTempPlayTimesWithKey(args)
    local key = args[0]
    if key == EnumTempPlayDataKey_lua.eShuiGuoPaiDuiMaxScore or key == EnumTempPlayDataKey_lua.eShuiGuoPaiDuiReward then
        self.m_PlayData = self:LoadTempPlayData()
		self.m_Score.text = self.m_PlayData[4]
		self:RefreshRewards({self.m_PlayData[1],self.m_PlayData[2],self.m_PlayData[3]})
    end
end

function LuaFruitPartySignWnd:LoadTempPlayData()
	if CClientMainPlayer.Inst then
		local ret = {}
        local data=CClientMainPlayer.Inst.PlayProp.TempPlayData
        local maxScoreTempData=CommonDefs.DictGetValue(data,typeof(UInt32),EnumTempPlayDataKey_lua.eShuiGuoPaiDuiMaxScore)
		local rewardTempData=CommonDefs.DictGetValue(data,typeof(UInt32),EnumTempPlayDataKey_lua.eShuiGuoPaiDuiReward)

		if rewardTempData and rewardTempData.ExpiredTime > CServerTimeMgr.Inst.timeStamp then
            local info = MsgPackImpl.unpack(rewardTempData.Data.Data)
            for i = 1, 3 do
				ret[i] = info[i-1]
			end
		else
			for i = 1, 3 do
				ret[i] = 0
			end
        end
		if maxScoreTempData and maxScoreTempData.ExpiredTime > CServerTimeMgr.Inst.timeStamp then
            ret[4] = tonumber(maxScoreTempData.Data.StringData)
        else 
			ret[4] = 0
		end

		return ret
	end

	return {0,0,0,0}
end