local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CItemMgr = import "L10.Game.CItemMgr"

LuaGuoQing2023ProtectHZWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuoQing2023ProtectHZWnd, "Grid", UIGrid)
RegistChildComponent(LuaGuoQing2023ProtectHZWnd, "ItemTemplate", GameObject)

RegistChildComponent(LuaGuoQing2023ProtectHZWnd, "BottomLabel", UILabel)
RegistChildComponent(LuaGuoQing2023ProtectHZWnd, "ItemCell", CQnReturnAwardTemplate)

--@endregion RegistChildComponent end

function LuaGuoQing2023ProtectHZWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaGuoQing2023ProtectHZWnd:Init()
    self.ItemTemplate:SetActive(false)

    local setData = GuoQing2023_ProtectHangZhou.GetData()
    self.activityType = setData.ActivityType
    self.sceneTemlpateId = setData.SceneTemlpateId
    self.ItemCell:Init(CItemMgr.Inst:GetItemTemplate(setData.RewardId), 1)
    Gac2Gas.GetDynamicActivityInfo(self.activityType)
end

function LuaGuoQing2023ProtectHZWnd:OnProtectHangZhouSyncSceneNpcCount(sceneNpcCountU, playRemainCount)
    self.BottomLabel.text = SafeStringFormat3(LocalString.GetString("今日可挑战%d次，挑战成功可获得"), playRemainCount)
    local sceneNpcCount = g_MessagePack.unpack(sceneNpcCountU)
	Extensions.RemoveAllChildren(self.Grid.transform)
    for sceneId, sceneInfo in pairs(sceneNpcCount) do
		local cell = NGUITools.AddChild(self.Grid.gameObject, self.ItemTemplate)
        self:InitOneCell(cell, sceneInfo)
    end
    self.Grid:Reposition()
end

function LuaGuoQing2023ProtectHZWnd:InitOneCell(cell, sceneInfo)
    cell.gameObject:SetActive(true)
    local stateSprite = cell.transform:Find("StateSprite"):GetComponent(typeof(UISprite))
    local lineLabel = cell.transform:Find("LineLabel"):GetComponent(typeof(UILabel))
    local playerLabel = cell.transform:Find("PlayerLabel"):GetComponent(typeof(UILabel))
    local npcLabel = cell.transform:Find("NpcLabel"):GetComponent(typeof(UILabel))
    local finishLabel = cell.transform:Find("FinishLabel"):GetComponent(typeof(UILabel))
    local sceneId = sceneInfo[1]
    local sceneIndex = sceneInfo[2]
    local playerCount = sceneInfo[3]
    local npcCount = sceneInfo[4]
    lineLabel.text = SafeStringFormat3(LocalString.GetString("分线%d"), sceneIndex)
    playerLabel.text = SafeStringFormat3(LocalString.GetString("%d人"), playerCount)
    self:SetStateSprite(stateSprite, playerCount)
    if npcCount > 0 then
        npcLabel.gameObject:SetActive(true)
        finishLabel.gameObject:SetActive(false)
        npcLabel.text =  SafeStringFormat3(LocalString.GetString("剩余%d"), npcCount)
        local gotoBtn = cell.transform:Find("NpcLabel/GotoBtn").gameObject
        UIEventListener.Get(gotoBtn).onClick = DelegateFactory.VoidDelegate(function (go)
            Gac2Gas.RequestGoDynamicActivitySite(self.activityType, self.sceneTemlpateId, sceneId)
            CUIManager.CloseUI(CLuaUIResources.GuoQing2023ProtectHZWnd)
        end)
    else
        npcLabel.gameObject:SetActive(false)
        finishLabel.gameObject:SetActive(true)
    end
end


function LuaGuoQing2023ProtectHZWnd:SetStateSprite(statusIcon, playerNumber)
    local baomanIconSpriteName = "loginwnd_severstate_baoman"
    local busyIconSpriteName = "loginwnd_severstate_busy"
    local normalIconSpriteName = "loginwnd_severstate_smooth"
    if playerNumber >= 100 then
        statusIcon.spriteName = baomanIconSpriteName
    elseif playerNumber >= 80 then
        statusIcon.spriteName = busyIconSpriteName
    else
        statusIcon.spriteName = normalIconSpriteName
    end
end
--@region UIEvent

--@endregion UIEvent

function LuaGuoQing2023ProtectHZWnd:OnEnable()
    g_ScriptEvent:AddListener("ProtectHangZhouSyncSceneNpcCount", self, "OnProtectHangZhouSyncSceneNpcCount")
end

function LuaGuoQing2023ProtectHZWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ProtectHangZhouSyncSceneNpcCount", self, "OnProtectHangZhouSyncSceneNpcCount")
end