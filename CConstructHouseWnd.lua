-- Auto Generated!!
local AlignType1 = import "L10.UI.CTooltip+AlignType"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local Byte = import "System.Byte"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CConstructHouseWnd = import "L10.UI.CConstructHouseWnd"
local CGarden = import "L10.Game.CGarden"
local CHouseProgress = import "L10.Game.CHouseProgress"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"
local EHouseProgress = import "L10.Game.EHouseProgress"
local EnumChineseDigits = import "L10.Game.EnumChineseDigits"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local ETickType = import "L10.Engine.ETickType"
local EventDelegate = import "EventDelegate"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local House_CangkuUpgrade = import "L10.Game.House_CangkuUpgrade"
local House_GardenUpgrade = import "L10.Game.House_GardenUpgrade"
local House_XiangfangUpgrade = import "L10.Game.House_XiangfangUpgrade"
local House_ZhengwuUpgrade = import "L10.Game.House_ZhengwuUpgrade"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local Zhuangshiwu_Setting = import "L10.Game.Zhuangshiwu_Setting"

LuaConstructHouseWnd = class()
RegistClassMember(LuaConstructHouseWnd, "m_ShuiyuButton")

function LuaConstructHouseWnd:OnShuiyuButtonClick(this,go)
    this.mLastClickGO = go
    this.CurConsumeItemCount[0] = 0
    this.CurConsumeItemCount[1] = 0
    this.KuoZhangMapGO:SetActive(false)
    this.InfoGO:SetActive(true)

    local mgr = CClientHouseMgr.Inst
    if mgr.mBasicProp == nil or mgr.mConstructProp == nil or mgr.mGardenProp == nil or mgr.mProgressProp == nil then
        return
    end

    this:ShowButtons(EHouseProgress_lua.eShuiyuProgressId)
    local shuiyuGrade = mgr.mConstructProp.PoolGrade
    local zhengwuGrade = mgr.mConstructProp.ZhengwuGrade
    if shuiyuGrade == 0 then shuiyuGrade = 1 end
    this.mbCanConstruct = LuaConstructHouseWnd:CheckAndShowShuiyuInfo(this,shuiyuGrade, zhengwuGrade)
end

function LuaConstructHouseWnd:CheckAndShowShuiyuInfo(this,shuiyuGrade,zhengwuGrade)
    local bCanUpgrade = true
    local needZhengwuGrade = Zhuangshiwu_Setting.GetData().PoolNeedZhengwuGrade
    local bEnableShuiyu = (needZhengwuGrade <= zhengwuGrade)

    this.CurConstructType = EHouseProgress_lua.eShuiyuProgressId
    if not bEnableShuiyu then
        local data = House_PoolUpgrade.GetData(1)
        this.Texture:LoadMaterial(data.Icon)

        this.CurLevelLabel.text = House_Setting.GetData().PoolUpgradeFunction
        this.NeedTypeLabel.text = LocalString.GetString("开放需求")
        this.NeedLabel.text = System.String.Format(LocalString.GetString("[c][FF0000]需要{0}级正屋[-][/c] "), needZhengwuGrade)
        this.UpgradeLabel.text = LocalString.GetString("开    房")
        this.TitleLabel.text = LocalString.GetString("海梦泽 未开放")
        this.NextLevelLabel.text = data.Function
        this.ConsumePart:SetActive(false)

        bCanUpgrade = false
    elseif shuiyuGrade >= CConstructHouseWnd.MaxGrade then
        local icon = nil
        local sydata = House_PoolUpgrade.GetData(CConstructHouseWnd.MaxGrade)
        if sydata ~= nil then
            icon = sydata.Icon
        end
        local func = sydata.Function
        bCanUpgrade = this:CheckAndShowInfo(shuiyuGrade, 0, 0, 0, 0, func, System.String.Empty, 0, 0, 0, 0, nil, 0, LocalString.GetString("海梦泽 {0}"), icon)
    else
        local curData = House_PoolUpgrade.GetData(shuiyuGrade)
        local nextData = House_PoolUpgrade.GetData(shuiyuGrade + 1)
        if curData ~= nil and nextData ~= nil then
            this.UpgradeTargetText = System.String.Format(LocalString.GetString("{0}级海梦泽"), shuiyuGrade + 1)
            local curFunc = curData.Function
            local nextFunc = nextData.Function
            local mgr = CClientHouseMgr.Inst
            bCanUpgrade = this:CheckAndShowInfo(shuiyuGrade, nextData.NeedZhengwuGrade, 0, 0, 0, curFunc, nextFunc, nextData.NeedFood, nextData.NeedWood, nextData.NeedStone, nextData.NeedTime, nextData.ConsumeItem, nextData.NeedMoney, LocalString.GetString("海梦泽 {0}"), nextData.Icon)
        else
            bCanUpgrade = false
        end
    end

    return bCanUpgrade
end

CConstructHouseWnd.m_Init_CS2LuaHook = function (this) 
    this:OnClickZhengwuButton(this.ZhengwuButton)
end
CConstructHouseWnd.m_OnEnable_CS2LuaHook = function (this) 
    LuaConstructHouseWnd.m_ShuiyuButton = this.ZhengwuButton.transform.parent:Find("ShuiyuCell")
    UIEventListener.Get(LuaConstructHouseWnd.m_ShuiyuButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    LuaConstructHouseWnd:OnShuiyuButtonClick(this,go)
	end)
    UIEventListener.Get(this.ZhengwuButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.ZhengwuButton).onClick, MakeDelegateFromCSFunction(this.OnClickZhengwuButton, VoidDelegate, this), true)
    UIEventListener.Get(this.CangkuButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.CangkuButton).onClick, MakeDelegateFromCSFunction(this.OnClickCangkuButton, VoidDelegate, this), true)
    UIEventListener.Get(this.XiangfangButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.XiangfangButton).onClick, MakeDelegateFromCSFunction(this.OnClickXiangfangButton, VoidDelegate, this), true)
    UIEventListener.Get(this.KuozhangButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.KuozhangButton).onClick, MakeDelegateFromCSFunction(this.OnClickKuozhangButton, VoidDelegate, this), true)
    UIEventListener.Get(this.UpgradeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.UpgradeButton).onClick, MakeDelegateFromCSFunction(this.OnClickUpgradeButton, VoidDelegate, this), true)
    UIEventListener.Get(this.MiaopuButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.MiaopuButton).onClick, MakeDelegateFromCSFunction(this.OnClickMiaopuButton, VoidDelegate, this), true)
    do
        local i = 0
        while i < this.ConsumeItemButton.Length do
            UIEventListener.Get(this.ConsumeItemButton[i]).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.ConsumeItemButton[i]).onClick, MakeDelegateFromCSFunction(this.OnClickConsumeItemButton, VoidDelegate, this), true)
            i = i + 1
        end
    end
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)

    EventManager.AddListener(EnumEventType.OnUpdateHouseProgress, MakeDelegateFromCSFunction(this.OnUpdateHouseProgress, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.AddListener(EnumEventType.MainPlayerUpdateMoney, MakeDelegateFromCSFunction(this.OnUpdateMoney, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))

    this.CurConstructType = 0
    this.mbCanConstruct = false

    this.autoCostSilver.value = false
    this.CurCostSilver = 0
    this.CurNeedItemUseSilverTId = 0
    this.CurCanUpgradeWithSilver = true
    CommonDefs.ListAdd(this.autoCostSilver.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        this:UpdateAutoCostSilverState()
    end)))

    this.CostMoneyCtl:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, true)
    CLuaHouseMgr.m_ConstructHouseMsg = nil
end
CConstructHouseWnd.m_OnUpdateMoney_CS2LuaHook = function (this) 
    if this.mLastClickGO ~= nil then
        this.mbNeedPopUpMiaopuMenu = false
        invoke(UIEventListener.Get(this.mLastClickGO).onClick,this.mLastClickGO)
    end
end
CConstructHouseWnd.m_OnSendItem_CS2LuaHook = function (this, itemid) 
    local item = CItemMgr.Inst:GetById(itemid)
    if item ~= nil and this.mLastClickGO ~= nil then
        local bAddConsumeItem = false
        if this.CurConsumeItem ~= nil then
            do
                local i = 0
                while i < this.CurConsumeItem.Length do
                    if this.CurConsumeItem[i] == item.TemplateId then
                        bAddConsumeItem = true
                        break
                    end
                    i = i + 1
                end
            end
        end

        if bAddConsumeItem then
            this.mbNeedPopUpMiaopuMenu = false
            invoke(UIEventListener.Get(this.mLastClickGO).onClick,this.mLastClickGO)
        end
    end
end
CConstructHouseWnd.m_UpdateAutoCostSilverState_CS2LuaHook = function (this) 
    local bUseSilver = this.autoCostSilver.value
    if bUseSilver then
        local data = Item_Item.GetData(this.CurNeedItemUseSilverTId)
        if data ~= nil and this.CurCostSilver > 0 then
            local text = g_MessageMgr:FormatMessage("Upgrade_House_Use_Silver_Confirm", data.Name)
            QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.YinLiang, text, this.CurCostSilver, DelegateFactory.Action(function () 
                if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Silver >= this.CurCostSilver then
                    --CUICommonDef.SetActive(this.UpgradeButton, this.CurCanUpgradeWithSilver, true)
                    CUICommonDef.SetActive(this.UpgradeButton, not this.CurUnderConstruct, true)
                    this.autoCostSilver.value = true
                    this.mbCanConstruct = this.CurCanUpgradeWithSilver
                else
                    g_MessageMgr:ShowMessage("NO_ENOUGH_SILVER")
                end
            end), DelegateFactory.Action(function () 
                this.autoCostSilver.value = false
            end), false, true, EnumPlayScoreKey.NONE, false)
        end
    else
        if this.CurCostSilver > 0 then
            --CUICommonDef.SetActive(this.UpgradeButton, false, true)
            CUICommonDef.SetActive(this.UpgradeButton, not this.CurUnderConstruct, true)
            this.mbCanConstruct = false
            CLuaHouseMgr.UpdateConstructHouseMsg(SafeStringFormat3("%s%s", this.UpgradeLabel.text:gsub("%s+",""), LocalString.GetString("所需银两不足")), false)
        end
    end
end
CConstructHouseWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.ZhengwuButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.ZhengwuButton).onClick, MakeDelegateFromCSFunction(this.OnClickZhengwuButton, VoidDelegate, this), false)
    UIEventListener.Get(this.CangkuButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.CangkuButton).onClick, MakeDelegateFromCSFunction(this.OnClickCangkuButton, VoidDelegate, this), false)
    UIEventListener.Get(this.XiangfangButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.XiangfangButton).onClick, MakeDelegateFromCSFunction(this.OnClickXiangfangButton, VoidDelegate, this), false)
    UIEventListener.Get(this.KuozhangButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.KuozhangButton).onClick, MakeDelegateFromCSFunction(this.OnClickKuozhangButton, VoidDelegate, this), false)
    UIEventListener.Get(this.UpgradeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.UpgradeButton).onClick, MakeDelegateFromCSFunction(this.OnClickUpgradeButton, VoidDelegate, this), false)
    UIEventListener.Get(this.MiaopuButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.MiaopuButton).onClick, MakeDelegateFromCSFunction(this.OnClickMiaopuButton, VoidDelegate, this), false)

    EventManager.RemoveListener(EnumEventType.OnUpdateHouseProgress, MakeDelegateFromCSFunction(this.OnUpdateHouseProgress, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListener(EnumEventType.MainPlayerUpdateMoney, MakeDelegateFromCSFunction(this.OnUpdateMoney, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))

    do
        local i = 0
        while i < this.ConsumeItemButton.Length do
            UIEventListener.Get(this.ConsumeItemButton[i]).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.ConsumeItemButton[i]).onClick, MakeDelegateFromCSFunction(this.OnClickConsumeItemButton, VoidDelegate, this), false)
            i = i + 1
        end
    end
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)

    this:UnRegisterTick()
    CLuaHouseMgr.m_ConstructHouseMsg = nil
end
CConstructHouseWnd.m_OnUpdateHouseProgress_CS2LuaHook = function (this) 
    if this.mLastClickGO ~= nil then
        invoke(UIEventListener.Get(this.mLastClickGO).onClick,this.mLastClickGO)
    end
end
CConstructHouseWnd.m_OnCloseButtonClick_CS2LuaHook = function (this, go) 
    this:Close()
end
--@desc 显示左侧按钮
CConstructHouseWnd.m_ShowButtons_CS2LuaHook = function (this, type) 
    local mgr = CClientHouseMgr.Inst
    if mgr.mBasicProp == nil or mgr.mConstructProp == nil or mgr.mGardenProp == nil or mgr.mProgressProp == nil then
        return
    end

    local progress
    this.ZhengwuSelectedSprite.gameObject:SetActive((type == EHouseProgress_lua.eZhengwuProgressId))
    local zhengwuGrade = mgr.mConstructProp.ZhengwuGrade
    if (function () 
        local __try_get_result
        __try_get_result, progress = CommonDefs.DictTryGet(mgr.mProgressProp.ProgressList, typeof(Byte), EHouseProgress_lua.eZhengwuProgressId, typeof(CHouseProgress))
        return __try_get_result
    end)() then
        this.ZhengwuLabel.text = System.String.Format(LocalString.GetString("正屋 {0}级 建设中"), zhengwuGrade)
    else
        this.ZhengwuLabel.text = System.String.Format(LocalString.GetString("正屋 {0}级"), zhengwuGrade)
    end
    local zwdata = House_ZhengwuUpgrade.GetData(zhengwuGrade)
    if zwdata ~= nil then
        local text = CommonDefs.GetComponentInChildren_GameObject_Type(this.ZhengwuButton, typeof(CUITexture))
        if text ~= nil then
            text:LoadMaterial(zwdata.Icon)
        end
    end

    this.XiangfangSelectedSprite.gameObject:SetActive((type == EHouseProgress_lua.eXiangfangProgressId))
    local xiangfangNeedZWGrade = Zhuangshiwu_Setting.GetData().XiangfangNeedZhengwuGrade
    local bEnableXiangfang = (zhengwuGrade >= xiangfangNeedZWGrade)

    -- 厢房可能没开放
    local xiangfangGrade = mgr.mConstructProp.XiangfangGrade
    if bEnableXiangfang then
        if (function () 
            local __try_get_result
            __try_get_result, progress = CommonDefs.DictTryGet(mgr.mProgressProp.ProgressList, typeof(Byte), EHouseProgress_lua.eXiangfangProgressId, typeof(CHouseProgress))
            return __try_get_result
        end)() then
            this.XiangfangLabel.text = System.String.Format(LocalString.GetString("厢房 {0}级 建设中"), xiangfangGrade)
        else
            this.XiangfangLabel.text = System.String.Format(LocalString.GetString("厢房 {0}级"), xiangfangGrade)
        end
    else
        this.XiangfangLabel.text = LocalString.GetString("厢房 未开放")
    end

    local xfdata = House_XiangfangUpgrade.GetData(xiangfangGrade)
    if xfdata ~= nil then
        local text = CommonDefs.GetComponentInChildren_GameObject_Type(this.XiangfangButton, typeof(CUITexture))
        if text ~= nil then
            text:LoadMaterial(xfdata.Icon, not bEnableXiangfang)
        end
    end

    this.CangkuSelectedSprite.gameObject:SetActive((type == EHouseProgress_lua.eCangkuProgressId))

    local cangkuGrade = mgr.mConstructProp.CangkuGrade
    if (function () 
        local __try_get_result
        __try_get_result, progress = CommonDefs.DictTryGet(mgr.mProgressProp.ProgressList, typeof(Byte), EHouseProgress_lua.eCangkuProgressId, typeof(CHouseProgress))
        return __try_get_result
    end)() then
        this.CangkuLabel.text = System.String.Format(LocalString.GetString("仓库 {0}级 建设中"), cangkuGrade)
    else
        this.CangkuLabel.text = System.String.Format(LocalString.GetString("仓库 {0}级"), cangkuGrade)
    end
    local ckdata = House_CangkuUpgrade.GetData(cangkuGrade)
    if ckdata ~= nil then
        local text = CommonDefs.GetComponentInChildren_GameObject_Type(this.CangkuButton, typeof(CUITexture))
        if text ~= nil then
            text:LoadMaterial(ckdata.Icon)
        end
    end

    local name = EnumChineseDigits.GetDigit(this.CurMiaopuIdx + 1)
    local garden = nil
    local miaopuGrade = 1

    if (function () 
        local __try_get_result
        __try_get_result, garden = CommonDefs.DictTryGet(mgr.mGardenProp.GardenList, typeof(Byte), this.CurMiaopuIdx, typeof(CGarden))
        return __try_get_result
    end)() then
        miaopuGrade = garden.Grade
        if miaopuGrade > 0 then
            this.MiaopuLabel.text = System.String.Format(LocalString.GetString("苗圃{0} {1}级"), name, garden.Grade)
        end
    else
        this.MiaopuLabel.text = System.String.Format(LocalString.GetString("苗圃{0} {1}"), name, LocalString.GetString("未建设"))
    end
    local mpdata = House_GardenUpgrade.GetData(miaopuGrade)
    if mpdata ~= nil then
        local text = CommonDefs.GetComponentInChildren_GameObject_Type(this.MiaopuButton, typeof(CUITexture))
        if text ~= nil then
            text:LoadMaterial(mpdata.Icon)
        end
    end
    this.MiaopuSelectedSprite.gameObject:SetActive((type >= EHouseProgress_lua.eGardenProgressBase) and (type < 16 --[[(byte)EHouseProgress.eGardenProgressBase + 6]]))


    this.KuozhangSelectedSprite.gameObject:SetActive((type == EHouseProgress_lua.eKuozhangProgressId))
    local kuozhangTex = CommonDefs.GetComponentInChildren_GameObject_Type(this.KuozhangButton, typeof(CUITexture))
    if kuozhangTex ~= nil then
        kuozhangTex:LoadMaterial("UI/Texture/Transparent/Material/house_ui_mianji.mat")
    end

    LuaConstructHouseWnd.m_ShuiyuButton.gameObject:SetActive(LuaPoolMgr.s_House_Pool_Switch_On)
    if LuaPoolMgr.s_House_Pool_Switch_On then
        local shuiyuGrade = mgr.mConstructProp.PoolGrade
        local bEnableShuiyu = (zhengwuGrade >= Zhuangshiwu_Setting.GetData().PoolNeedZhengwuGrade)
        local ShuiyuSelectedSprite = LuaConstructHouseWnd.m_ShuiyuButton:Find("SelectedSprite")
        local ShuiyuLabel = LuaConstructHouseWnd.m_ShuiyuButton:Find("Label"):GetComponent(typeof(UILabel))
        ShuiyuSelectedSprite.gameObject:SetActive((type == EHouseProgress_lua.eShuiyuProgressId))
        if shuiyuGrade <=0 then shuiyuGrade = 1 end
        if bEnableShuiyu then
            if (function () 
                local __try_get_result
                __try_get_result, progress = CommonDefs.DictTryGet(mgr.mProgressProp.ProgressList, typeof(Byte), EHouseProgress_lua.eShuiyuProgressId, typeof(CHouseProgress))
                return __try_get_result
            end)() then
                ShuiyuLabel.text = System.String.Format(LocalString.GetString("海梦泽 {0}级 建设中"), shuiyuGrade)
            else
                ShuiyuLabel.text = System.String.Format(LocalString.GetString("海梦泽 {0}级"), shuiyuGrade)
            end
        else
            ShuiyuLabel.text = LocalString.GetString("海梦泽 未开放")
        end
        local sydata = House_PoolUpgrade.GetData(shuiyuGrade)
        if sydata then
            local text = CommonDefs.GetComponentInChildren_GameObject_Type(LuaConstructHouseWnd.m_ShuiyuButton.gameObject, typeof(CUITexture))
            if text ~= nil then
                text:LoadMaterial(sydata.Icon, not bEnableShuiyu)
            end
        end
    end
end
CConstructHouseWnd.m_ShowMaxGradeLabels_CS2LuaHook = function (this, curLvlText) 
    this.NeedLabel.text = LocalString.GetString("已满级")
    this.NextLevelLabel.text = LocalString.GetString("已满级")
    this.CurLevelLabel.text = curLvlText
    this.ConsumneFoodLabel.text = LocalString.GetString("0")
    this.ConsumneFoodLabel.color = Color.green
    this.ConsumeWoodLabel.text = LocalString.GetString("0")
    this.ConsumeWoodLabel.color = Color.green
    this.ConsumeStoneLabel.text = LocalString.GetString("0")
    this.ConsumeStoneLabel.color = Color.green
    do
        local i = 0
        while i < this.ConsumeItemButton.Length do
            this.ConsumeItemButton[i]:SetActive(false)
            i = i + 1
        end
    end
    this.UpgradeTimeLabel.text = LocalString.GetString("已满级")

    CUICommonDef.SetActive(this.UpgradeButton, false, true)

    this.ConsumePart:SetActive(false)
end
CConstructHouseWnd.m_UnRegisterTick_CS2LuaHook = function (this) 
    if this.mProgressTick ~= nil then
        invoke(this.mProgressTick)
        this.mProgressTick = nil
    end
end
CConstructHouseWnd.m_RegisterTick_CS2LuaHook = function (this) 
    this:UnRegisterTick()

    this.mProgressTick = CTickMgr.Register(DelegateFactory.Action(function () 
        this:OnProgressTick()
    end), 1000, ETickType.Loop)
end
CConstructHouseWnd.m_OnProgressTick_CS2LuaHook = function (this) 
    local restTime = this.CurConstructionTime - math.floor(CServerTimeMgr.Inst.timeStamp)
    restTime = restTime > 0 and restTime or 0
    local timeText = SafeStringFormat3(LocalString.GetString("剩余时间：%02d:%02d:%02d"), math.floor(restTime / 3600), math.floor((restTime % 3600) / 60), restTime % 60)
    this.UpgradeTimeLabel.text = timeText

    if restTime == 0 then
        this:UnRegisterTick()
    end
end
CConstructHouseWnd.m_CheckAndShowInfo_CS2LuaHook = function (this, CurGrade, needZhengwuGrade, needCangkuGrade, needXiangfangGrade, needQishu, curLvlFuncStr, nextLvlFuncStr, needFood, needWood, needStone, needTime, consumeItems, costMoney, titleFormat, IconTexture) 
    this.CurUnderConstruct = false
    this.ConsumeResource = needStone
    CLuaHouseMgr.m_ConstructHouseMsg = nil

    this:UnRegisterTick()

    this.CurCostSilver = 0
    this.CurNeedItemUseSilverTId = 0
    this.autoCostSilver.value = false

    if IconTexture ~= nil then
        this.Texture:LoadMaterial(IconTexture)
    end

    local bCanUpgrade = true
    if CurGrade == 0 then
        this.NeedTypeLabel.text = LocalString.GetString("建设需求")
        this.UpgradeLabel.text = LocalString.GetString("建    设")
        this.ConsumeLabel.text = LocalString.GetString("建设消耗")
        this.TitleLabel.text = System.String.Format(titleFormat, LocalString.GetString("未建设"))
    else
        this.NeedTypeLabel.text = LocalString.GetString("升级需求")
        this.UpgradeLabel.text = LocalString.GetString("升    级")
        this.ConsumeLabel.text = LocalString.GetString("升级消耗")
        this.TitleLabel.text = System.String.Format(titleFormat, System.String.Format(LocalString.GetString("{0}级"), CurGrade))
    end

    local sameProgress = nil
    if (function () 
        local __try_get_result
        __try_get_result, sameProgress = CommonDefs.DictTryGet(CClientHouseMgr.Inst.mProgressProp.ProgressList, typeof(Byte), this.CurConstructType, typeof(CHouseProgress))
        return __try_get_result
    end)() then
        bCanUpgrade = false
        this.UpgradeLabel.text = LocalString.GetString("建 设 中")
        this.CurUnderConstruct = true
    end

    if this.CurUnderConstruct then
        this.TitleLabel.text = System.String.Format(titleFormat, LocalString.GetString("建设中"))
    end

    if CurGrade >= CConstructHouseWnd.MaxGrade then
        bCanUpgrade = false
        this:ShowMaxGradeLabels(curLvlFuncStr)
    else
        this.ConsumePart:SetActive(true)

        local text = System.String.Empty

        local curCangkuGrade = CClientHouseMgr.Inst.mConstructProp.CangkuGrade
        local curZhengwuGrade = CClientHouseMgr.Inst.mConstructProp.ZhengwuGrade
        local curXiangfangGrade = CClientHouseMgr.Inst.mConstructProp.XiangfangGrade
        local curQishu = CClientHouseMgr.Inst.mQishu

        if needZhengwuGrade > 0 then
            if needZhengwuGrade > curZhengwuGrade then
                text = text .. System.String.Format(LocalString.GetString("[c][FF0000]需要{0}级正屋[-][/c] "), needZhengwuGrade)
            else
                text = text .. System.String.Format(LocalString.GetString("[c][00FF00]需要{0}级正屋[-][/c] "), needZhengwuGrade)
            end
        end
        if needXiangfangGrade > 0 then
            if needXiangfangGrade > curXiangfangGrade then
                text = text .. System.String.Format(LocalString.GetString("[c][FF0000]需要{0}级厢房[-][/c] "), needXiangfangGrade)
            else
                text = text .. System.String.Format(LocalString.GetString("[c][00FF00]需要{0}级厢房[-][/c] "), needXiangfangGrade)
            end
        end
        if needCangkuGrade > 0 then
            if needCangkuGrade > curCangkuGrade then
                text = text .. System.String.Format(LocalString.GetString("[c][FF0000]需要{0}级仓库[-][/c] "), needCangkuGrade)
            else
                text = text .. System.String.Format(LocalString.GetString("[c][00FF00]需要{0}级仓库[-][/c] "), needCangkuGrade)
            end
        end
        if needQishu > 0 then
            if needQishu > curQishu then
                text = text .. System.String.Format(LocalString.GetString("[c][FF0000]需要{0}气数[-][/c] "), needQishu)
            else
                text = text .. System.String.Format(LocalString.GetString("[c][00FF00]需要{0}气数[-][/c] "), needQishu)
            end
        end
        this.NeedLabel.text = text

        if not (curQishu >= needQishu and curCangkuGrade >= needCangkuGrade and curZhengwuGrade >= needZhengwuGrade and curXiangfangGrade >= needXiangfangGrade) then
            bCanUpgrade = false
            CLuaHouseMgr.UpdateConstructHouseMsg(SafeStringFormat3(LocalString.GetString("未满足%s需求"), this.UpgradeLabel.text:gsub("%s+","")), false)
        end

        this.CurLevelLabel.text = curLvlFuncStr
        this.NextLevelLabel.text = nextLvlFuncStr

        local food = CClientHouseMgr.Inst.mConstructProp.Food
        local stone = CClientHouseMgr.Inst.mConstructProp.Stone
        local wood = CClientHouseMgr.Inst.mConstructProp.Wood

        if this.CurUnderConstruct then
            this.ConsumneFoodLabel.color = Color.green
            this.ConsumneFoodLabel.text = System.String.Format(LocalString.GetString("{0}"), needFood)
            this.ConsumeFoodBar.value = 1
        else
            local color = nil
            if needFood > food then
                bCanUpgrade = false
                color = LuaColorUtils.RedColorTx
                this.ConsumeFoodBar.value = food / needFood
            else
                color = LuaColorUtils.GreenColorTx
                this.ConsumeFoodBar.value = 1
            end
            this.ConsumneFoodLabel.color = Color.white
            this.ConsumneFoodLabel.text = SafeStringFormat3("[%s]%d[-][%s]/%d[-]", color, food, LuaColorUtils.WhiteColorTx, needFood)
        end

        if this.CurUnderConstruct then
            this.ConsumeWoodLabel.color = Color.green
            this.ConsumeWoodLabel.text = System.String.Format(LocalString.GetString("{0}"), needWood)
            this.ConsumeWoodBar.value = 1
        else
            local color = nil
            if needWood > wood then
                bCanUpgrade = false
                color = LuaColorUtils.RedColorTx
                this.ConsumeWoodBar.value = wood / needWood
            else
                color = LuaColorUtils.GreenColorTx
                this.ConsumeWoodBar.value = 1
            end
            this.ConsumeWoodLabel.color = Color.white
            this.ConsumeWoodLabel.text = SafeStringFormat3("[%s]%d[-][%s]/%d[-]", color, wood, LuaColorUtils.WhiteColorTx, needWood)
        end

        if this.CurUnderConstruct then
            this.ConsumeStoneLabel.color = Color.green
            this.ConsumeStoneLabel.text = System.String.Format(LocalString.GetString("{0}"), needStone)
            this.ConsumeStoneBar.value = 1
        else
            local color = nil
            if needStone > stone then
                bCanUpgrade = false
                color = LuaColorUtils.RedColorTx
                this.ConsumeStoneBar.value = stone / needStone
            else
                color = LuaColorUtils.GreenColorTx
                this.ConsumeStoneBar.value = 1
            end
            this.ConsumeStoneLabel.color = Color.white
            this.ConsumeStoneLabel.text = SafeStringFormat3("[%s]%d[-][%s]/%d[-]", color, stone, LuaColorUtils.WhiteColorTx, needStone)
        end

        if needFood > food or needStone > stone or needWood > wood then
            CLuaHouseMgr.UpdateConstructHouseMsg(SafeStringFormat3("%s%s", this.UpgradeLabel.text:gsub("%s+",""), LocalString.GetString("所需资材不足")), false)
        end
        
        if this.CurUnderConstruct then
            local totalTime = 0
            local default
            default, totalTime = CClientHouseMgr.Inst:GetProgressEndTime(this.CurConstructType, sameProgress, CClientHouseMgr.Inst.mGardenProp, CClientHouseMgr.Inst.mConstructProp)
            local endTime = default
            local restTime = endTime - math.floor(CServerTimeMgr.Inst.timeStamp)
            restTime = restTime > 0 and restTime or 0
            local timeText = SafeStringFormat3(LocalString.GetString("剩余时间：%02d:%02d:%02d"), math.floor(restTime / 3600), math.floor((restTime % 3600) / 60), restTime % 60)
            this.UpgradeTimeLabel.text = timeText

            if restTime > 0 then
                this.CurConstructionTime = endTime
                this:RegisterTick()
            end
        else
            local time = needTime
            local timeText = SafeStringFormat3(LocalString.GetString("需要时间：%02d:%02d:%02d"), math.floor(time / 3600), math.floor((time % 3600) / 60), time % 60)
            this.UpgradeTimeLabel.text = timeText
        end


        if consumeItems == nil or consumeItems.Length == 0 then
            this.ConsumeItemsGO:SetActive(false)
            this.CostMoneyCtl.gameObject:SetActive(true)
            for i = 0, 1 do
                this.CurConsumeItem[i] = 0
                this.CurConsumeItemCount[i] = 0
            end

            this.CostMoneyCtl:SetCost(costMoney)
            if costMoney > CClientMainPlayer.Inst.Silver then
                bCanUpgrade = false
                CLuaHouseMgr.UpdateConstructHouseMsg(SafeStringFormat3("%s%s",this.UpgradeLabel.text:gsub("%s+",""), LocalString.GetString("所需银两不足")), false)
            end
        else
            this.ConsumeItemsGO:SetActive(true)
            this.CostMoneyCtl.gameObject:SetActive(false)

            this.CurCanUpgradeWithSilver = bCanUpgrade

            for i = 0, 1 do
                if consumeItems.Length >= 2 * (i + 1) then
                    this.ConsumeItemButton[i]:SetActive(true)
                    local itemtid = consumeItems[2 * i]
                    local count = consumeItems[2 * i + 1]
                    local itemdata = Item_Item.GetData(itemtid)
                    if itemdata ~= nil then
                        this.ConsumeItemTexture[i]:LoadMaterial(itemdata.Icon)
                        this.CurConsumeItem[i] = itemtid
                        this.CurConsumeItemCount[i] = count

                        if this.CurUnderConstruct then
                            this.ConsumeItemCountLabel[i].color = Color.green
                            this.ConsumeItemGetMask[i]:SetActive(false)
                            this.ConsumeItemCountLabel[i].text = System.String.Format(LocalString.GetString("{0}"), count)
                        else
                            local curCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemtid)
                            if curCount >= count then
                                this.ConsumeItemCountLabel[i].color = Color.green
                                this.ConsumeItemGetMask[i]:SetActive(false)
                            else
                                this.ConsumeItemCountLabel[i].color = Color.red
                                this.ConsumeItemGetMask[i]:SetActive(true)

                                this.CurNeedItemUseSilverTId = itemtid
                                this.CurCostSilver = this.CurCostSilver + ((count - curCount) * costMoney)
                                bCanUpgrade = false
                                CLuaHouseMgr.UpdateConstructHouseMsg(SafeStringFormat3("%s%s", this.UpgradeLabel.text:gsub("%s+",""), LocalString.GetString("所需琉璃瓦不足")), false)
                            end
                            this.ConsumeItemCountLabel[i].text = System.String.Format(LocalString.GetString("{0}/{1}"), curCount, count)
                        end
                    end
                else
                    this.CurConsumeItem[i] = 0
                    this.CurConsumeItemCount[i] = 0
                    this.ConsumeItemButton[i]:SetActive(false)
                end
                this.autoCostSilver.gameObject.transform.parent.gameObject:SetActive(this.CurCostSilver > 0)
            end
        end

        if this.CurUnderConstruct then
            CUICommonDef.SetActive(this.UpgradeButton, false, true)
        else
            CUICommonDef.SetActive(this.UpgradeButton, true, true)
        end
        --CUICommonDef.SetActive(this.UpgradeButton, bCanUpgrade, true)
        
    end
    return bCanUpgrade
end
CConstructHouseWnd.m_CheckAndShowZhengwuInfo_CS2LuaHook = function (this, zhengwuGrade, bIsMingyuan) 
    local bCanUpgrade = true

    this.CurConstructType = EHouseProgress_lua.eZhengwuProgressId
    if zhengwuGrade >= CConstructHouseWnd.MaxGrade then
        local icon = nil
        local zwdata = House_ZhengwuUpgrade.GetData(CConstructHouseWnd.MaxGrade)
        if zwdata ~= nil then
            icon = zwdata.Icon
        end
        local func = zwdata.Function
        if bIsMingyuan then
            func = zwdata.MYFunction
        end
        bCanUpgrade = this:CheckAndShowInfo(zhengwuGrade, 0, 0, 0, 0, func, System.String.Empty, 0, 0, 0, 0, nil, 0, LocalString.GetString("正屋 {0}"), icon)
    else
        local curData = House_ZhengwuUpgrade.GetData(zhengwuGrade)
        local nextData = House_ZhengwuUpgrade.GetData(zhengwuGrade + 1)
        if curData ~= nil and nextData ~= nil then
            this.UpgradeTargetText = System.String.Format(LocalString.GetString("{0}级正屋"), zhengwuGrade + 1)
            local curFunc = curData.Function
            local nextFunc = nextData.Function
            if bIsMingyuan then
                curFunc = curData.MYFunction
                nextFunc = nextData.MYFunction
            end
            bCanUpgrade = this:CheckAndShowInfo(zhengwuGrade, 0, nextData.NeedCangkuGrade, nextData.NeedXiangfangGrade, nextData.NeedQishu, curFunc, nextFunc, nextData.NeedFood, nextData.NeedWood, nextData.NeedStone, nextData.NeedTime, nextData.ConsumeItem, nextData.NeedMoney, LocalString.GetString("正屋 {0}"), nextData.Icon)
        else
            bCanUpgrade = false
        end
    end

    return bCanUpgrade
end
CConstructHouseWnd.m_OnClickZhengwuButton_CS2LuaHook = function (this, go) 
    this.mLastClickGO = go
    this.CurConsumeItemCount[0] = 0
    this.CurConsumeItemCount[1] = 0
    this.KuoZhangMapGO:SetActive(false)
    this.InfoGO:SetActive(true)

    local mgr = CClientHouseMgr.Inst
    if mgr.mBasicProp == nil or mgr.mConstructProp == nil or mgr.mGardenProp == nil or mgr.mProgressProp == nil then
        return
    end

    this:ShowButtons(EHouseProgress_lua.eZhengwuProgressId)
    local zhengwuGrade = mgr.mConstructProp.ZhengwuGrade
    this.mbCanConstruct = this:CheckAndShowZhengwuInfo(zhengwuGrade, (mgr.mBasicProp.IsMingyuan ~= 0))
end
CConstructHouseWnd.m_CheckAndShowCangkuInfo_CS2LuaHook = function (this, grade) 
    local bCanUpgrade = true

    this.CurConstructType = EHouseProgress_lua.eCangkuProgressId

    if grade >= CConstructHouseWnd.MaxGrade then
        local icon = nil
        local data = House_CangkuUpgrade.GetData(CConstructHouseWnd.MaxGrade)
        if data ~= nil then
            icon = data.Icon
        end

        bCanUpgrade = this:CheckAndShowInfo(grade, 0, 0, 0, 0, data.Function, System.String.Empty, 0, 0, 0, 0, nil, 0, LocalString.GetString("仓库 {0}"), icon)
    else
        local curData = House_CangkuUpgrade.GetData(grade)
        local nextData = House_CangkuUpgrade.GetData(grade + 1)
        if curData ~= nil and nextData ~= nil then
            this.UpgradeTargetText = System.String.Format(LocalString.GetString("{0}级仓库"), grade + 1)
            bCanUpgrade = this:CheckAndShowInfo(grade, nextData.NeedZhengwuGrade, 0, 0, 0, curData.Function, nextData.Function, nextData.NeedFood, nextData.NeedWood, nextData.NeedStone, nextData.NeedTime, nextData.ConsumeItem, nextData.NeedMoney, LocalString.GetString("仓库 {0}"), nextData.Icon)
        else
            bCanUpgrade = false
        end
    end

    return bCanUpgrade
end
CConstructHouseWnd.m_OnClickCangkuButton_CS2LuaHook = function (this, go) 
    this.mLastClickGO = go
    this.CurConsumeItemCount[0] = 0
    this.CurConsumeItemCount[1] = 0

    this.KuoZhangMapGO:SetActive(false)
    this.InfoGO:SetActive(true)

    local mgr = CClientHouseMgr.Inst
    if mgr.mBasicProp == nil or mgr.mConstructProp == nil or mgr.mGardenProp == nil or mgr.mProgressProp == nil then
        return
    end

    this:ShowButtons(EHouseProgress_lua.eCangkuProgressId)
    local grade = mgr.mConstructProp.CangkuGrade
    this.mbCanConstruct = this:CheckAndShowCangkuInfo(grade)
end
CConstructHouseWnd.m_CheckAndShowXiangfangInfo_CS2LuaHook = function (this, grade, zhengwuGrade) 
    local bCanUpgrade = true
    local needZhengwuGrade = Zhuangshiwu_Setting.GetData().XiangfangNeedZhengwuGrade
    local bEnableXiangfang = (needZhengwuGrade <= zhengwuGrade)

    this.CurConstructType = EHouseProgress_lua.eXiangfangProgressId

    if not bEnableXiangfang then
        local data = House_XiangfangUpgrade.GetData(1)
        this.Texture:LoadMaterial(data.Icon)

        this.CurLevelLabel.text = LocalString.GetString("未开放")
        this.NeedTypeLabel.text = LocalString.GetString("开放需求")
        this.NeedLabel.text = System.String.Format(LocalString.GetString("[c][FF0000]需要{0}级正屋[-][/c] "), needZhengwuGrade)
        this.UpgradeLabel.text = LocalString.GetString("开    房")
        this.TitleLabel.text = LocalString.GetString("厢房 未开放")
        this.NextLevelLabel.text = data.Function
        this.ConsumePart:SetActive(false)

        bCanUpgrade = false
    elseif grade >= CConstructHouseWnd.MaxGrade then
        local icon = nil
        local data = House_XiangfangUpgrade.GetData(CConstructHouseWnd.MaxGrade)
        if data ~= nil then
            icon = data.Icon
        end

        bCanUpgrade = this:CheckAndShowInfo(grade, 0, 0, 0, 0, data.Function, System.String.Empty, 0, 0, 0, 0, nil, 0, LocalString.GetString("厢房 {0}"), icon)
    else
        local curData = House_XiangfangUpgrade.GetData(grade)
        local nextData = House_XiangfangUpgrade.GetData(grade + 1)
        if curData ~= nil and nextData ~= nil then
            this.UpgradeTargetText = System.String.Format(LocalString.GetString("{0}级厢房"), grade + 1)
            bCanUpgrade = this:CheckAndShowInfo(grade, nextData.NeedZhengwuGrade, 0, 0, 0, curData.Function, nextData.Function, nextData.NeedFood, nextData.NeedWood, nextData.NeedStone, nextData.NeedTime, nextData.ConsumeItem, nextData.NeedMoney, LocalString.GetString("厢房 {0}"), nextData.Icon)
        else
            bCanUpgrade = false
        end
    end

    return bCanUpgrade
end
CConstructHouseWnd.m_OnClickXiangfangButton_CS2LuaHook = function (this, go) 
    this.mLastClickGO = go
    this.CurConsumeItemCount[0] = 0
    this.CurConsumeItemCount[1] = 0

    this.KuoZhangMapGO:SetActive(false)
    this.InfoGO:SetActive(true)

    local mgr = CClientHouseMgr.Inst
    if mgr.mBasicProp == nil or mgr.mConstructProp == nil or mgr.mGardenProp == nil or mgr.mProgressProp == nil then
        return
    end

    this:ShowButtons(EHouseProgress_lua.eXiangfangProgressId)
    local grade = mgr.mConstructProp.XiangfangGrade
    local zhengwuGrade = mgr.mConstructProp.ZhengwuGrade
    this.mbCanConstruct = this:CheckAndShowXiangfangInfo(grade, zhengwuGrade)
end
CConstructHouseWnd.m_CheckAndShowMiaopuInfo_CS2LuaHook = function (this, grade, progressPos) 
    local bCanUpgrade = true

    this.CurConstructType = progressPos
    local gardenCount = progressPos - EHouseProgress_lua.eGardenProgressBase + 1
    local titleFormat = System.String.Format(LocalString.GetString("苗圃{0}"), EnumChineseDigits.GetDigit(gardenCount))
    titleFormat = titleFormat .. LocalString.GetString(" {0}")

    if grade == 0 then
        local needZhengwuGrade = - 1
        do
            local i = 1
            while i <= CConstructHouseWnd.MaxGrade do
                local curdata = House_ZhengwuUpgrade.GetData(i)
                if curdata.GardenNum >= gardenCount then
                    needZhengwuGrade = i
                    break
                end
                i = i + 1
            end
        end

        local data = House_GardenUpgrade.GetData(1)
        if data ~= nil then
            this.UpgradeTargetText = System.String.Format(LocalString.GetString("{0}级苗圃{1}"), 1, EnumChineseDigits.GetDigit(gardenCount))
            bCanUpgrade = this:CheckAndShowInfo(grade, needZhengwuGrade, 0, 0, 0, LocalString.GetString("未建设"), data.Function, data.NeedFood, data.NeedWood, data.NeedStone, data.NeedTime, data.ConsumeItem, data.NeedMoney, titleFormat, data.Icon)
        end
    elseif grade >= CConstructHouseWnd.MaxGrade then
        local icon = nil
        local data = House_GardenUpgrade.GetData(CConstructHouseWnd.MaxGrade)
        if data ~= nil then
            icon = data.Icon
        end
        bCanUpgrade = this:CheckAndShowInfo(grade, 0, 0, 0, 0, data.Function, System.String.Empty, 0, 0, 0, 0, nil, 0, titleFormat, icon)
    else
        local curData = House_GardenUpgrade.GetData(grade)
        local nextData = House_GardenUpgrade.GetData(grade + 1)
        if curData ~= nil and nextData ~= nil then
            this.UpgradeTargetText = System.String.Format(LocalString.GetString("{0}级苗圃{1}"), grade + 1, EnumChineseDigits.GetDigit(gardenCount))
            bCanUpgrade = this:CheckAndShowInfo(grade, nextData.NeedZhengwuGrade, 0, 0, 0, curData.Function, nextData.Function, nextData.NeedFood, nextData.NeedWood, nextData.NeedStone, nextData.NeedTime, nextData.ConsumeItem, nextData.NeedMoney, titleFormat, nextData.Icon)
        else
            bCanUpgrade = false
        end
    end

    return bCanUpgrade
end
CConstructHouseWnd.m_OnMiaopuClicked_CS2LuaHook = function (this, idx) 
    local mgr = CClientHouseMgr.Inst
    if mgr.mBasicProp == nil or mgr.mConstructProp == nil or mgr.mGardenProp == nil or mgr.mProgressProp == nil then
        return
    end

    this.CurMiaopuIdx = idx
    local pos = (EHouseProgress_lua.eGardenProgressBase + idx)
    this:ShowButtons(pos)

    local garden = nil
    local grade = 0
    if (function () 
        local __try_get_result
        __try_get_result, garden = CommonDefs.DictTryGet(mgr.mGardenProp.GardenList, typeof(Byte), idx, typeof(CGarden))
        return __try_get_result
    end)() then
        grade = garden.Grade
    end
    this.mbCanConstruct = this:CheckAndShowMiaopuInfo(grade, pos)
end
CConstructHouseWnd.m_OnClickMiaopuButton_CS2LuaHook = function (this, go) 
    this.mLastClickGO = go
    this.CurConsumeItemCount[0] = 0
    this.CurConsumeItemCount[1] = 0

    this.KuoZhangMapGO:SetActive(false)
    this.InfoGO:SetActive(true)

    local mgr = CClientHouseMgr.Inst
    if mgr.mBasicProp == nil or mgr.mConstructProp == nil or mgr.mGardenProp == nil or mgr.mProgressProp == nil then
        return
    end

    local idx = this.CurMiaopuIdx

    if idx >= 0 then
        this:OnMiaopuClicked(idx)

        local garden = nil

        if this.mbNeedPopUpMiaopuMenu then
            local houseProgress
            local text
            local rows = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))

            for i = 0, 5 do
                local curIndex = i
                local bUnderConstruction = (function () 
                    local __try_get_result
                    __try_get_result, houseProgress = CommonDefs.DictTryGet(mgr.mProgressProp.ProgressList, typeof(Byte), EnumToInt(EHouseProgress.eGardenProgressBase) + i, typeof(CHouseProgress))
                    return __try_get_result
                end)()
                local bHasGarden = (function () 
                    local __try_get_result
                    __try_get_result, garden = CommonDefs.DictTryGet(mgr.mGardenProp.GardenList, typeof(Byte), i, typeof(CGarden))
                    return __try_get_result
                end)()

                if bUnderConstruction then
                    if bHasGarden then
                        text = System.String.Format(LocalString.GetString("苗圃{0} 升级中"), EnumChineseDigits.GetDigit(1 + i))
                    else
                        text = System.String.Format(LocalString.GetString("苗圃{0} 建设中"), EnumChineseDigits.GetDigit(1 + i))
                    end
                else
                    if bHasGarden then
                        text = System.String.Format(LocalString.GetString("苗圃{0} {1}级"), EnumChineseDigits.GetDigit(1 + i), garden.Grade)
                    else
                        text = System.String.Format(LocalString.GetString("苗圃{0} 未建设"), EnumChineseDigits.GetDigit(1 + i))
                    end
                end

                CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(text, DelegateFactory.Action_int(function (index) 
                    this:OnMiaopuClicked(curIndex)
                end), false, nil))
            end

            CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(rows), this.MiaopuButton.transform, CPopupMenuInfoMgr.AlignType.Right)
        end
    end
    this.mbNeedPopUpMiaopuMenu = true
end
CConstructHouseWnd.m_OnClickKuozhangButton_CS2LuaHook = function (this, go) 
    this.mLastClickGO = nil
    this.CurConsumeItemCount[0] = 0
    this.CurConsumeItemCount[1] = 0

    this:ShowButtons(EHouseProgress_lua.eKuozhangProgressId)

    this.KuoZhangMapGO:SetActive(true)
    this.InfoGO:SetActive(false)

    this:InitKuozhangStatus()
end
CConstructHouseWnd.m_InitKuozhangStatus_CS2LuaHook = function (this) 
    local mgr = CClientHouseMgr.Inst
    if not (mgr ~= nil and mgr.mConstructProp ~= nil and mgr.mBasicProp ~= nil) then
        return
    end

    local houseGrade = mgr.mConstructProp.ZhengwuGrade
    local data = House_ZhengwuUpgrade.GetData(houseGrade)
    if data == nil then
        return
    end
    local kuozhangLevel = data.KuozhangLevel - 1

    if mgr.mBasicProp.IsMingyuan ~= 0 then
        this.MingyuanKuoZhangMapGO:SetActive(true)
        this.PutongZhangMapGO:SetActive(false)

        local len = math.floor(math.min(kuozhangLevel, this.MingyuanKuozhangAreas.Length))
        do
            local i = 0
            while i < len do
                this.MingyuanKuozhangAreas[i]:SetActive(false)
                i = i + 1
            end
        end
        do
            local i = len
            while i < this.MingyuanKuozhangAreas.Length do
                this.MingyuanKuozhangAreas[i]:SetActive(true)
                i = i + 1
            end
        end

        do
            local i = 0
            while i < this.MingyuanAreaEdges.Length do
                this.MingyuanAreaEdges[i]:SetActive(i == (data.KuozhangLevel - 1))
                i = i + 1
            end
        end
    else
        this.MingyuanKuoZhangMapGO:SetActive(false)
        this.PutongZhangMapGO:SetActive(true)

        local len = math.floor(math.min(kuozhangLevel, this.KuozhangAreas.Length))
        do
            local i = 0
            while i < len do
                this.KuozhangAreas[i]:SetActive(false)
                i = i + 1
            end
        end
        do
            local i = len
            while i < this.KuozhangAreas.Length do
                this.KuozhangAreas[i]:SetActive(true)
                i = i + 1
            end
        end

        do
            local i = 0
            while i < this.AreaEdges.Length do
                this.AreaEdges[i]:SetActive(i == (data.KuozhangLevel - 1))
                i = i + 1
            end
        end
    end
end
CConstructHouseWnd.m_OnClickUpgradeButton_CS2LuaHook = function (this, go) 
    
    local mgr = CClientHouseMgr.Inst
    if mgr.mBasicProp == nil or mgr.mConstructProp == nil or mgr.mGardenProp == nil or mgr.mProgressProp == nil then
        return
    end

    -- todo 升级按钮点击事件，进行提示
    if not this.mbCanConstruct then
        CLuaHouseMgr.ShowConstructHouseMsg()
        return
    end

    if this.mbCanConstruct then
        if not CClientHouseMgr.Inst:IsInOwnHouse() then
            g_MessageMgr:ShowMessage("OPERATION_NEED_IN_OWN_HOUSE")
            return
        end

        local consumeItemMsg = ""
        if this.CurConsumeItem ~= nil and this.CurConsumeItem.Length > 0 and this.CurConsumeItemCount ~= nil and this.CurConsumeItemCount.Length > 0 and this.CurConsumeItemCount[0] > 0 then
            local data = Item_Item.GetData(this.CurConsumeItem[0])
            if data ~= nil then
                consumeItemMsg = consumeItemMsg .. System.String.Format(LocalString.GetString("{0}个{1}"), this.CurConsumeItemCount[0], data.Name)
            end
        else
            consumeItemMsg = consumeItemMsg .. System.String.Format(LocalString.GetString("{0}银两"), this.CostMoneyCtl:GetCost())
        end
        local text = g_MessageMgr:FormatMessage("CONSTRUCTION_CONFIRM", this.ConsumeResource, consumeItemMsg)
        MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(function () 
            Gac2Gas.RequestUpgradeHouseBuilding(this.CurConstructType)
            this:Close()
        end), nil, nil, nil, false)
    end
end
CConstructHouseWnd.m_OnClickConsumeItemButton_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.ConsumeItemButton.Length do
            if go == this.ConsumeItemButton[i] and this.CurConsumeItem[i] ~= 0 and this.CurConsumeItemCount[i] ~= 0 then
                local curCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, this.CurConsumeItem[i])
                local count = this.CurConsumeItemCount[i]
                if (curCount < count) and not this.CurUnderConstruct then
                    CItemAccessListMgr.Inst:ShowItemAccessInfo(this.CurConsumeItem[i], curCount >= count, go.transform, AlignType1.Right)
                else
                    CItemInfoMgr.ShowLinkItemTemplateInfo(this.CurConsumeItem[i], false, nil, AlignType2.Default, 0, 0, 0, 0)
                end
            end
            i = i + 1
        end
    end
end
