local CShopMallMgr=import "L10.UI.CShopMallMgr"
local Item_Item=import "L10.Game.Item_Item"
local CUITexture=import "L10.UI.CUITexture"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local UILabel = import "UILabel"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local BoxCollider = import "UnityEngine.BoxCollider"
local CommonDefs = import "L10.Game.CommonDefs"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

CLuaShuangshiyi2020ZhongCaoWnd=class()
RegistClassMember(CLuaShuangshiyi2020ZhongCaoWnd,"m_ZhongCaoButton")
RegistClassMember(CLuaShuangshiyi2020ZhongCaoWnd,"m_BaCaoButton")
RegistClassMember(CLuaShuangshiyi2020ZhongCaoWnd,"m_TableView")
RegistClassMember(CLuaShuangshiyi2020ZhongCaoWnd,"m_NumInput")
RegistClassMember(CLuaShuangshiyi2020ZhongCaoWnd,"m_CurCao")
RegistClassMember(CLuaShuangshiyi2020ZhongCaoWnd,"m_CurCount")
RegistClassMember(CLuaShuangshiyi2020ZhongCaoWnd,"m_DailyCount")
RegistClassMember(CLuaShuangshiyi2020ZhongCaoWnd,"m_BaCaoTip")
RegistClassMember(CLuaShuangshiyi2020ZhongCaoWnd,"m_ReleaseTime")
RegistClassMember(CLuaShuangshiyi2020ZhongCaoWnd,"m_BcStartTime")
RegistClassMember(CLuaShuangshiyi2020ZhongCaoWnd,"m_Progress")
RegistClassMember(CLuaShuangshiyi2020ZhongCaoWnd,"m_TimeLabel1")
RegistClassMember(CLuaShuangshiyi2020ZhongCaoWnd,"m_TimeLabel2")

function CLuaShuangshiyi2020ZhongCaoWnd:InitComponents()
    self.m_TableView = self.transform:Find("GoodsGrid"):GetComponent(typeof(QnTableView))
    self.m_ZhongCaoButton = self.transform:Find("Detail/ZhongCao/ZhongCaoButton").gameObject
    self.m_BaCaoButton = self.transform:Find("Detail/BaCao/BaCaoButton").gameObject
    self.m_CurCount = self.transform:Find("Detail/ZhongCao/Icon/CurCount"):GetComponent(typeof(UILabel))
    self.m_DailyCount = self.transform:Find("Detail/ZhongCao/Icon/DailyCount"):GetComponent(typeof(UILabel))

    self.m_TimeLabel1 = self.transform:Find("Detail/ZhongCaoLabel/Label1"):GetComponent(typeof(UILabel))
    self.m_TimeLabel2 = self.transform:Find("Detail/BaCaoLabel/Label1"):GetComponent(typeof(UILabel))
    if CommonDefs.IS_HMT_CLIENT then
        self.m_TimeLabel1.text = "10.30 00:00 ~ 11.03 22:00"
        self.m_TimeLabel2.text = "11.04 00:00 ~ 11.08 23:59"
    else
        self.m_TimeLabel1.text = g_MessageMgr:FormatMessage("Shuangshiyi_ZhongCao_Stage_Time")
        self.m_TimeLabel2.text = g_MessageMgr:FormatMessage("Shuangshiyi_BaCao_Stage_Time")
    end

    local zhongCaoDescription = self.transform:Find("Detail/ZhongCaoLabel/Label2"):GetComponent(typeof(UILabel))
    local neteaseOffical = (CommonDefs.IS_CN_CLIENT and CLoginMgr.Inst:IsNetEaseOfficialLogin())
    local mainplayer = CClientMainPlayer.Inst
    if neteaseOffical and mainplayer then
        zhongCaoDescription.text = LocalString.GetString("对喜欢的时装进行种草，每日可种草3次！（完成神秘任务时每天会有额外的种草次数！）")
    else    
        zhongCaoDescription.text = LocalString.GetString("对喜欢的时装进行种草，每日可种草3次！")
    end
end

function CLuaShuangshiyi2020ZhongCaoWnd:Init()
    self:InitComponents()
    CLuaShuangshiyi2020Mgr.CheckPlayerID()
    CLuaShuangshiyi2020Mgr.m_CountLookup={}
    CLuaShuangshiyi2020Mgr.m_HotLookup={}

    self.m_TableView.m_DataSource=DefaultTableViewDataSource.Create(
        function()
            return #CLuaShuangshiyi2020Mgr.m_DataList
        end,function(item,index)
            self:InitItem(item.transform,CLuaShuangshiyi2020Mgr.m_DataList[index+1])
        end)
    
    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function(row)
        local itemId = CLuaShuangshiyi2020Mgr.m_DataList[self.m_TableView.currentSelectRow+1].ItemId
    end)

    CLuaShuangshiyi2020Mgr.m_DataList={}
    Double11_OldFashion.ForeachKey(function(k)
        table.insert( CLuaShuangshiyi2020Mgr.m_DataList,Double11_OldFashion.GetData(k))
    end)

    local maxZhongcaoPerDay = Double11_Setting.GetData().MaxZhongcaoPerDay
    local extraZhongcaoTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eDouble11ZhongCaoAdjustLimit) or 0
    maxZhongcaoPerDay = maxZhongcaoPerDay + extraZhongcaoTimes
    self.m_DailyCount.text = "/" .. maxZhongcaoPerDay

    

    UIEventListener.Get(self.m_ZhongCaoButton).onClick=DelegateFactory.VoidDelegate(function(go)
        local currentSelected = self.m_TableView.currentSelectRow
        if currentSelected>-1 then
            local itemId = CLuaShuangshiyi2020Mgr.m_DataList[currentSelected+1].ItemId
            CLuaShuangshiyi2020Mgr.m_ZhongCaoItem = itemId
            Gac2Gas.RequestZhongCao2020(itemId, 1)
        else
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请选择要种草的时装"))
        end
    end)

    UIEventListener.Get(self.m_BaCaoButton).onClick=DelegateFactory.VoidDelegate(function(go)
        self:OnClickBaCaoButton(go)
    end)

    self.m_CurCao = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eShuangshiyi2020ZhongCao)
    self.m_CurCount.text = maxZhongcaoPerDay - self.m_CurCao

    local bacao = FindChild(self.transform,"BaCao")
    local bacaoTipGo=FindChild(bacao,"TipLabel").gameObject
    self.m_BaCaotip = bacaoTipGo:GetComponent(typeof(UILabel))
    local bcTimeStr = Double11_Setting.GetData().BaCaoStartTime
    local bcStamp = CServerTimeMgr.Inst:GetTimeStampByStr(bcTimeStr)
    self.m_BcStartTime = CServerTimeMgr.ConvertTimeStampToZone8Time(bcStamp)
    self.m_ReleaseTime = CommonDefs.op_Subtraction_DateTime_DateTime(self.m_BcStartTime, CServerTimeMgr.Inst:GetZone8Time())
    self.m_BaCaotip.text = SafeStringFormat3("%02d:%02d:%02d", self.m_ReleaseTime.Days, self.m_ReleaseTime.Hours, self.m_ReleaseTime.Seconds)

    self:SetProgress(0)
    
    Gac2Gas.QueryAllZhongCaoCount2020()
end

function CLuaShuangshiyi2020ZhongCaoWnd:Update()
    if self.m_Progress == 3 then
        self.m_ReleaseTime = CommonDefs.op_Subtraction_DateTime_DateTime(self.m_BcStartTime, CServerTimeMgr.Inst:GetZone8Time())
        if self.m_ReleaseTime.Days>=0 and self.m_ReleaseTime.Hours>=0 and self.m_ReleaseTime.Minutes>=0 and self.m_ReleaseTime.Seconds>=0 then
            self.m_BaCaotip.text = SafeStringFormat3("%02d:%02d:%02d", self.m_ReleaseTime.Hours, self.m_ReleaseTime.Minutes, self.m_ReleaseTime.Seconds)
        else
            -- 小于零时，进入拔草阶段
            self:SetProgress(2)
            self.m_BaCaotip.text = "00:00:00"
        end
    end
end

function CLuaShuangshiyi2020ZhongCaoWnd:OnClickBaCaoButton(go)
    CShopMallMgr.SelectMallIndex = 0
    CShopMallMgr.SelectCategory = 6
    if self.m_TableView.currentSelectRow>=0 then
        local itemId = CLuaShuangshiyi2020Mgr.m_DataList[self.m_TableView.currentSelectRow+1].ItemId
        CShopMallMgr.ShowLinyuShoppingMall(itemId)
    else
        local itemId = CLuaShuangshiyi2020Mgr.m_DataList[1].ItemId
        CShopMallMgr.ShowLinyuShoppingMall(itemId)
    end
end

function CLuaShuangshiyi2020ZhongCaoWnd:SetColor(go, show)
    local label = go:GetComponent(typeof(UILabel))
    local label1 = FindChild(go.transform,"Label1"):GetComponent(typeof(UILabel))
    local label2 = FindChild(go.transform,"Label2"):GetComponent(typeof(UILabel))
    local texture = FindChild(go.transform,"Texture"):GetComponent(typeof(UITexture))
    if show then
        label.color = Color(146/255,1,84/255)  --NGUIText.ParseColor24("ff5b52", 0)
        label1.color = Color(146/255,1,84/255)
        label2.color = Color(1,1,1)
        texture.color = Color(146/255,1,84/255)
    else
        label.color = Color(0.54,0.54,0.54)
        label1.color = Color(0.54,0.54,0.54)
        label2.color = Color(0.54,0.54,0.54)
        texture.color = Color(0.54,0.54,0.54)
    end
end

function CLuaShuangshiyi2020ZhongCaoWnd:InitItem(transform,info)
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local priceLabel = transform:Find("PriceLabel"):GetComponent(typeof(UILabel))
    local count = CLuaShuangshiyi2020Mgr.m_CountLookup[info.ItemId] or 0
    priceLabel.text=tostring(count)
    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local hotSprite = transform:Find("HotSprite").gameObject
    if CLuaShuangshiyi2020Mgr.m_HotLookup[info.ItemId] then
        hotSprite:SetActive(true)
    else
        hotSprite:SetActive(false)
    end

    local template = Item_Item.GetData(info.ItemId)
    if template then
        icon:LoadMaterial(template.Icon)
        nameLabel.text=template.Name
    end

    if self.m_Progress == 1 then
        UIEventListener.Get(icon.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
            local fashionId = info.FashionID
            local itemId = info.ItemId
            if fashionId>0 then
                CLuaShuangshiyi2020Mgr.m_PreviewItem = itemId
                CLuaShuangshiyi2020Mgr.SetFashionId(fashionId)
                CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2020FashionPreviewWnd)
            end
        end)
        local box = icon.gameObject:GetComponent(typeof(BoxCollider))
        box.enabled = true
    elseif self.m_Progress ~= nil then
        local box = icon.gameObject:GetComponent(typeof(BoxCollider))
        box.enabled = false
    end
end

function CLuaShuangshiyi2020ZhongCaoWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryAllZhongCaoCountRet2020", self, "OnQueryAllZhongCaoCountRet")
    g_ScriptEvent:AddListener("PlayerZhongCaoRet2020", self, "OnPlayerZhongCaoRet")
end

function CLuaShuangshiyi2020ZhongCaoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryAllZhongCaoCountRet2020", self, "OnQueryAllZhongCaoCountRet")
    g_ScriptEvent:RemoveListener("PlayerZhongCaoRet2020", self, "OnPlayerZhongCaoRet")
end

function CLuaShuangshiyi2020ZhongCaoWnd:OnQueryAllZhongCaoCountRet(progress,t)
    CLuaShuangshiyi2020Mgr.m_CountLookup=t
    self.m_Progress = progress

    -- 排序dataList
    table.sort( CLuaShuangshiyi2020Mgr.m_DataList, function(a,b)
        local itemId1 = a.ItemId
        local itemId2 = b.ItemId
        if t[itemId1]>t[itemId2] then
            return true
        elseif t[itemId1]<t[itemId2] then
            return false
        else
            return itemId1<itemId2
        end
    end )

    -- 前五名标记为热 记录hotLookup的itemId
    CLuaShuangshiyi2020Mgr.m_HotLookup={}
    local max = math.min(5, #CLuaShuangshiyi2020Mgr.m_DataList)
    for i=1,max do
        local itemId = CLuaShuangshiyi2020Mgr.m_DataList[i].ItemId
        local count = CLuaShuangshiyi2020Mgr.m_CountLookup[itemId] or 0
        if count > 500 then
            CLuaShuangshiyi2020Mgr.m_HotLookup[itemId] = true
        end
    end

    --拔草阶段只显示前5个
    if progress==2 or progress == 3 then
        local temp={}
        for i=1,max do
            table.insert( temp,CLuaShuangshiyi2020Mgr.m_DataList[i] )
        end
        CLuaShuangshiyi2020Mgr.m_DataList=temp
    end

    self.m_TableView:ReloadData(false, false)

    -- 设置之前选中的item
    if CLuaShuangshiyi2020Mgr.m_ZhongCaoItem~=nil then
        for i=1,#CLuaShuangshiyi2020Mgr.m_DataList do
            if CLuaShuangshiyi2020Mgr.m_DataList[i].ItemId == CLuaShuangshiyi2020Mgr.m_ZhongCaoItem then
                self.m_TableView:SetSelectRow(i-1,true)
            end
        end
    end

    self:SetProgress(progress)
end

function CLuaShuangshiyi2020ZhongCaoWnd:SetProgress(progress)
    self.m_Progress = progress
    local zhaongcao = FindChild(self.transform,"ZhongCao")
    local bacao = FindChild(self.transform,"BaCao")
    local zhongcaoLabelGo = FindChild(self.transform,"ZhongCaoLabel").gameObject
    local bacaoLabelGo = FindChild(self.transform,"BaCaoLabel").gameObject
    -- 倒计时
    local bacaoTipGo=FindChild(bacao,"TipLabel").gameObject

    if progress==0 then
        --活动没开始
        zhaongcao.gameObject:SetActive(false)
        bacao.gameObject:SetActive(false)
        self:SetColor(zhongcaoLabelGo,false)
        self:SetColor(bacaoLabelGo,false)
    elseif progress==1 then
        --种草阶段
        zhaongcao.gameObject:SetActive(true)
        bacao.gameObject:SetActive(false)
        self:SetColor(zhongcaoLabelGo,true)
        self:SetColor(bacaoLabelGo,false)
    elseif progress==2 then
        --拔草阶段
        zhaongcao.gameObject:SetActive(false)
        bacao.gameObject:SetActive(true)
        self:SetColor(zhongcaoLabelGo,false)
        self:SetColor(bacaoLabelGo,true)
        bacaoTipGo:SetActive(false)
        CUICommonDef.SetActive(self.m_BaCaoButton,true,true)
    elseif progress==3 then
        --种草阶段结束
        zhaongcao.gameObject:SetActive(false)
        bacao.gameObject:SetActive(true)
        self:SetColor(zhongcaoLabelGo,false)
        self:SetColor(bacaoLabelGo,false)
        bacaoTipGo:SetActive(true)
        CUICommonDef.SetActive(self.m_BaCaoButton,false,true)
    end
end

function CLuaShuangshiyi2020ZhongCaoWnd:OnPlayerZhongCaoRet(mallItemId, count)
    CLuaShuangshiyi2020Mgr.m_CountLookup[mallItemId] = (CLuaShuangshiyi2020Mgr.m_CountLookup[mallItemId] or 0) + count

    table.sort( CLuaShuangshiyi2020Mgr.m_DataList, function(a,b)
        local itemId1 = a.ItemId
        local itemId2 = b.ItemId
        if CLuaShuangshiyi2020Mgr.m_CountLookup[itemId1]>CLuaShuangshiyi2020Mgr.m_CountLookup[itemId2] then
            return true
        elseif CLuaShuangshiyi2020Mgr.m_CountLookup[itemId1]<CLuaShuangshiyi2020Mgr.m_CountLookup[itemId2] then
            return false
        else
            return itemId1<itemId2
        end
    end )
    
    CLuaShuangshiyi2020Mgr.m_HotLookup={}
    local max = math.min(5, #CLuaShuangshiyi2020Mgr.m_DataList)
    for i=1,max do
        local itemId = CLuaShuangshiyi2020Mgr.m_DataList[i].ItemId
        local count = CLuaShuangshiyi2020Mgr.m_CountLookup[itemId] or 0
        if count > 500 then
            CLuaShuangshiyi2020Mgr.m_HotLookup[itemId] = true
        end
    end

    self.m_TableView:ReloadData(false, false)

    for i=1,#CLuaShuangshiyi2020Mgr.m_DataList do
        if CLuaShuangshiyi2020Mgr.m_DataList[i].ItemId == mallItemId then
            self.m_TableView:SetSelectRow(i-1,true)
        else
            local itemFx = self.m_TableView:GetItemAtRow(i-1).transform:Find("ItemFx"):GetComponent(typeof(CUIFx))
            itemFx.gameObject:SetActive(false)
        end
    end

    local currentSelected = self.m_TableView.currentSelectRow
    local itemFx = self.m_TableView:GetItemAtRow(currentSelected).transform:Find("ItemFx"):GetComponent(typeof(CUIFx))
    itemFx.gameObject:SetActive(true)
    itemFx:DestroyFx()
    itemFx:LoadFx(CUIFxPaths.GQJCTeamNumChangeFx)

    self.m_CurCount.text = tostring(self.m_CurCount.text - 1)
end
