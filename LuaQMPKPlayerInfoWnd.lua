local CPlayerInfoMgr=import "CPlayerInfoMgr"
local UIEventListener = import "UIEventListener"
local Profession = import "L10.Game.Profession"
local LocalString = import "LocalString"
local Gac2Gas = import "L10.Game.Gac2Gas"
local EnumClass = import "L10.Game.EnumClass"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CChatHelper = import "L10.UI.CChatHelper"
local Initialization_Init=import "L10.Game.Initialization_Init"

CLuaQMPKPlayerInfoWnd = class()
RegistClassMember(CLuaQMPKPlayerInfoWnd,"m_PortraitTex")
RegistClassMember(CLuaQMPKPlayerInfoWnd,"m_PlayerNameLabel")
RegistClassMember(CLuaQMPKPlayerInfoWnd,"m_LevelLabel")
RegistClassMember(CLuaQMPKPlayerInfoWnd,"m_FromServerLabel")
RegistClassMember(CLuaQMPKPlayerInfoWnd,"m_FromCharacterLabel")
RegistClassMember(CLuaQMPKPlayerInfoWnd,"m_RemarkLabel")
RegistClassMember(CLuaQMPKPlayerInfoWnd,"m_GoodAtClassSprite")
RegistClassMember(CLuaQMPKPlayerInfoWnd,"m_TimeLabel")
RegistClassMember(CLuaQMPKPlayerInfoWnd,"m_CommanderLabel")
RegistClassMember(CLuaQMPKPlayerInfoWnd,"m_TeamNameLabel")
RegistClassMember(CLuaQMPKPlayerInfoWnd,"m_AddFriendBtn")
RegistClassMember(CLuaQMPKPlayerInfoWnd,"m_SendMsgBtn")
RegistClassMember(CLuaQMPKPlayerInfoWnd,"m_PersonalSpaceBtn")

function CLuaQMPKPlayerInfoWnd:Awake()
    self.m_PortraitTex = FindChild(self.transform,"Icon"):GetComponent(typeof(CUITexture))
    self.m_LevelLabel = FindChild(self.transform,"FromLevelLabel"):GetComponent(typeof(UILabel))
    self.m_FromServerLabel = FindChild(self.transform,"ServerName"):GetComponent(typeof(UILabel))
    self.m_RemarkLabel = FindChild(self.transform,"Remark"):GetComponent(typeof(UILabel))
    self.m_GoodAtClassSprite = {}
    self.m_GoodAtClassSprite[1]=FindChild(self.transform,"ClassSprite1"):GetComponent(typeof(UISprite))
    self.m_GoodAtClassSprite[2]=FindChild(self.transform,"ClassSprite2"):GetComponent(typeof(UISprite))
    self.m_GoodAtClassSprite[3]=FindChild(self.transform,"ClassSprite3"):GetComponent(typeof(UISprite))

    -- self.m_LocationLabel = self.transform:Find("Anchor/Info/Location/Label"):GetComponent(typeof(UILabel))
    self.m_TimeLabel = FindChild(self.transform,"TimeLabel"):GetComponent(typeof(UILabel))
    self.m_CommanderLabel = FindChild(self.transform,"CommanderLabel"):GetComponent(typeof(UILabel))
    self.m_TeamNameLabel = FindChild(self.transform,"TeamLabel"):GetComponent(typeof(UILabel))

    self.m_AddFriendBtn = self.transform:Find("Anchor/Bottom/AddFriendBtn").gameObject
    self.m_SendMsgBtn = self.transform:Find("Anchor/Bottom/SendMsgBtn").gameObject
    self.m_PersonalSpaceBtn = self.transform:Find("Anchor/Bottom/SpaceBtn").gameObject

    UIEventListener.Get(self.m_AddFriendBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnAddBtnClicked(go) end)
    UIEventListener.Get(self.m_SendMsgBtn).onClick =DelegateFactory.VoidDelegate(function(go) self:OnSendBtnClicked(go) end)
    UIEventListener.Get(FindChild(self.transform,"InfoButton").gameObject).onClick=DelegateFactory.VoidDelegate(function(go) 
        local playerId=CLuaQMPKMgr.m_OtherPersonalInfo.m_FromCharacterId
        local playerName=CLuaQMPKMgr.m_OtherPersonalInfo.m_FromCharacterName
        CPlayerInfoMgr.ShowPlayerInfoWnd(playerId, playerName)
    end)
    UIEventListener.Get(FindChild(self.transform,"ChatButton").gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        CChatHelper.ShowFriendWnd(CLuaQMPKMgr.m_OtherPersonalInfo.m_Id, CLuaQMPKMgr.m_OtherPersonalInfo.m_Name)
    end)
end

function CLuaQMPKPlayerInfoWnd:Init()
    if CPersonalSpaceMgr.OpenPersonalSpaceEntrance then
        self.m_PersonalSpaceBtn:SetActive(true)
        UIEventListener.Get(self.m_PersonalSpaceBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnPersonalSpaceBtnClicked(go) end)
    else
        self.m_PersonalSpaceBtn:SetActive(false)
    end

    local info =CLuaQMPKMgr.m_OtherPersonalInfo-- CQuanMinPKMgr.Inst.m_OtherPersonalInfo
    if info == nil then
        CUIManager.CloseUI(CLuaUIResources.QMPKPlayerInfoWnd)
        return
    end

    self.m_FromServerLabel.text = info.m_FromServerName
    self.m_LevelLabel.text = tostring(info.m_Grade)

    local data = Initialization_Init.GetData(info.m_FromClass * 100 + info.m_FromGender)
    if data then
        self.m_PortraitTex:LoadNPCPortrait(data.ResName, false)
    end
    self.m_RemarkLabel.text = info.m_Remark

    local guildName=info.m_FromGuildName
    if not guildName or guildName=="" then
        guildName=LocalString.GetString("无")
    end
    FindChild(self.transform,"FromGuildLabel"):GetComponent(typeof(UILabel)).text=guildName
    FindChild(self.transform,"EquipScoreLabel"):GetComponent(typeof(UILabel)).text=tostring(info.m_FromEquipScore)
    FindChild(self.transform,"FromLevelLabel"):GetComponent(typeof(UILabel)).text=SafeStringFormat3( LocalString.GetString("%d级"),info.m_FromGrade)
    
    FindChild(self.transform,"PlayerName"):GetComponent(typeof(UILabel)).text=info.m_FromCharacterName
    FindChild(self.transform,"GameNameLabel"):GetComponent(typeof(UILabel)).text=info.m_Name
    

    local index = 0
    if info.m_GoodAtClass > 0 then
        for i = 0, 14 do
            if (bit.band(info.m_GoodAtClass, (bit.lshift(1, i)))) > 0 then
                self.m_GoodAtClassSprite[index+1].gameObject:SetActive(true)
                self.m_GoodAtClassSprite[index+1].spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), (i + 1)))
                index = index + 1
            end
        end
    end
    do
        local cnt = #self.m_GoodAtClassSprite
        while index < cnt do
            self.m_GoodAtClassSprite[index+1].gameObject:SetActive(false)
            index = index + 1
        end
    end

    if info.m_OnlineTimeId > 0 then
        local time = QuanMinPK_OnlineTime.GetData(info.m_OnlineTimeId)
        if time ~= nil then
            self.m_TimeLabel.text = (time.First .. "  ") .. time.Second
        end
    end
    if info.m_CanCommand > 0 then
        self.m_CommanderLabel.text = LocalString.GetString("是")
    else
        self.m_CommanderLabel.text = LocalString.GetString("否")
    end
    if info.m_ZhanDuiId > 0 then
        self.m_TeamNameLabel.text = LocalString.GetString("有")
    else
        self.m_TeamNameLabel.text = LocalString.GetString("无")
    end
end
function CLuaQMPKPlayerInfoWnd:OnAddBtnClicked( go) 
    Gac2Gas.AddFriend(CLuaQMPKMgr.m_OtherPersonalInfo.m_FromCharacterId, "FromQMPKServer")
end
function CLuaQMPKPlayerInfoWnd:OnSendBtnClicked( go) 
    CChatHelper.ShowFriendWnd(CLuaQMPKMgr.m_OtherPersonalInfo.m_FromCharacterId, CLuaQMPKMgr.m_OtherPersonalInfo.m_FromCharacterName)
end
function CLuaQMPKPlayerInfoWnd:OnPersonalSpaceBtnClicked( go) 
    if CPersonalSpaceMgr.OpenPersonalSpaceEntrance then
        CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(CLuaQMPKMgr.m_OtherPersonalInfo.m_FromCharacterId, 0)
    end
end

return CLuaQMPKPlayerInfoWnd
