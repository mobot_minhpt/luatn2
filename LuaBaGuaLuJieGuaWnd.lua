require("common/common_include")

local UIVerticalLabel = import "UIVerticalLabel"
local QianKunDai_DivineResult = import "L10.Game.QianKunDai_DivineResult"
local CChatLinkMgr = import "CChatLinkMgr"
local UICamera = import "UICamera"

LuaBaGuaLuJieGuaWnd = class()

RegistClassMember(LuaBaGuaLuJieGuaWnd, "Title")
RegistClassMember(LuaBaGuaLuJieGuaWnd, "Content")

RegistClassMember(LuaBaGuaLuJieGuaWnd, "DivineResult")
RegistClassMember(LuaBaGuaLuJieGuaWnd, "ChampionLabel")

function LuaBaGuaLuJieGuaWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaBaGuaLuJieGuaWnd:InitClassMembers()
	self.Title = self.transform:Find("Anchor/Title"):GetComponent(typeof(UIVerticalLabel))
	self.Content = self.transform:Find("Anchor/Content"):GetComponent(typeof(UIVerticalLabel))

	if CommonDefs.IS_VN_CLIENT then
		self.Title.gameObject:SetActive(false)
		self.Content.gameObject:SetActive(false)
		self.Title = self.transform:Find("Anchor/TitleVert"):GetComponent(typeof(UILabel))
		self.Content = self.transform:Find("Anchor/ContentVert"):GetComponent(typeof(UILabel))
	end
	self.Title.gameObject:SetActive(true)
	self.Content.gameObject:SetActive(true)

	self.ChampionLabel = self.transform:Find("Anchor/ChampionLabel"):GetComponent(typeof(UILabel))
end

function LuaBaGuaLuJieGuaWnd:InitValues()
	self.DivineResult = LuaBaGuaLuMgr.m_DivineResult

	local onLabelClick = function (go)
		self:OnLabelClick(go)
	end
	CommonDefs.AddOnClickListener(self.ChampionLabel.gameObject, DelegateFactory.Action_GameObject(onLabelClick), false)

	local result = QianKunDai_DivineResult.GetData(self.DivineResult)
	if result then
		self.Title.text = result.DivineResultName
		self.Content.text = result.Desc

		if CommonDefs.IS_VN_CLIENT then
			self.Content.text = string.gsub(result.Desc, "#r", "\n")
		end

		if LuaBaGuaLuMgr.m_winnerId and LuaBaGuaLuMgr.m_winnerId > 0 then
			local link = CChatLinkMgr.ChatPlayerLink.GenerateLink(LuaBaGuaLuMgr.m_winnerId, LuaBaGuaLuMgr.m_WinnerName)
			self.ChampionLabel.text = SafeStringFormat3(LocalString.GetString("[000000]恭喜[-]%s[000000]获得[-][FF0000]%s[-][000000]银票[-]"), link.displayTag, tostring(LuaBaGuaLuMgr.m_WinnerAward))
		else
			self.ChampionLabel.text = ""
		end
	else
		CUIManager.CloseUI(CLuaUIResources.BaGuaLuJieGuaWnd)
	end
end

function LuaBaGuaLuJieGuaWnd:OnLabelClick(go)
	local url = self.ChampionLabel:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url ~= nil then
    	CChatLinkMgr.ProcessLinkClick(url, nil)
    end
end

return LuaBaGuaLuJieGuaWnd