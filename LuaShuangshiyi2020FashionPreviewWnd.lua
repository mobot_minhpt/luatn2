local CFashionPreviewTextureLoader = import "L10.UI.CFashionPreviewTextureLoader"
local NGUITools = import "NGUITools"
local DelegateFactory = import "DelegateFactory"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local QnButton = import "L10.UI.QnButton"
local Vector3 = import "UnityEngine.Vector3"
local EnumShopMallFashionPreviewType = import "L10.UI.EnumShopMallFashionPreviewType"
local UIEventListener = import "UIEventListener"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local CUIManager = import "L10.UI.CUIManager"
local GestureRecognizer = import "L10.Engine.GestureRecognizer"
local GestureType = import "L10.Engine.GestureType"
local QnRadioBox = import "L10.UI.QnRadioBox"
local ShaderLodParams = import "L10.Game.ShaderLodParams"
local Shader = import "UnityEngine.Shader"
local CRenderObject  = import "L10.Engine.CRenderObject"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local LayerDefine = import "L10.Engine.LayerDefine"
local QualitySettings = import "UnityEngine.QualitySettings"
local CMainCamera = import "L10.Engine.CMainCamera"
local UIPanel = import "UIPanel"
local CFashionPreviewMgr = import "L10.UI.CFashionPreviewMgr"
local EnumPreviewType = import "L10.UI.EnumPreviewType"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

CLuaShuangshiyi2020FashionPreviewWnd = class()

RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"qnModelPreviewer")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"tabs")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"itemID2ShopMallItemMap")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"onPinchInDelegate")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"onPinchOutDelegate")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"localModelScale")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"localModelPos")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"defaultModelLocalPos")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"maxPreviewScale")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"maxPreviewOffsetY")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"bfirstInit")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"shadowCamera")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"shadowCameraRelativePos")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"lightAndFloorAndWall")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"lightAndFloorAndWallRelativePos")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"lastpixelLightCount")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"floor")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"beiguangLight")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"floorInitialLocalPos")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"modelRoot")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"changeXianFanRadioBox")

RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"m_IsSupportFeiSheng")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"m_TableView")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"m_DataList")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"m_CountLookup")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"m_HotLookup")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"m_ZhongCaoButton")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"m_CurCao")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"m_CurCount")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"m_DailyCount")
RegistClassMember(CLuaShuangshiyi2020FashionPreviewWnd,"m_takeOffButton")

function CLuaShuangshiyi2020FashionPreviewWnd:Init()
    -- 防止出现可能存在的 多光源冲突的问题
    if CUIManager.IsLoaded(CUIResources.ShopMallFashionPreviewWnd) then
		CUIManager.CloseUI(CUIResources.ShopMallFashionPreviewWnd)
    end
    
    self.gameObject:GetComponent(typeof(UIPanel)).IgnoreIphoneXMargin = true
    CFashionPreviewMgr.curShopMallPreviewType = EnumShopMallFashionPreviewType.PreviewAllFashion
    CFashionPreviewMgr.curPreviewType = EnumPreviewType.PreviewFashion

    if CLuaShuangshiyi2020Mgr.m_DataList then
        self.m_DataList = CLuaShuangshiyi2020Mgr.m_DataList
    else
        ZhongCao_OldFashion.ForeachKey(function(k, data)
            table.insert( self.m_DataList, data)
        end)
    end

    self.m_HotLookup = CLuaShuangshiyi2020Mgr.m_HotLookup
    self.m_CountLookup = {}
    for i=1,#CLuaShuangshiyi2020Mgr.m_DataList do
        local itemID = CLuaShuangshiyi2020Mgr.m_DataList[i].ItemId
        if CLuaShuangshiyi2020Mgr.m_CountLookup[itemID] then
            self.m_CountLookup[itemID] = CLuaShuangshiyi2020Mgr.m_CountLookup[itemID]
        else
            self.m_CountLookup[itemID] = 0
        end
    end

    self:InitData()
    self.localModelScale = 1
    self.itemID2ShopMallItemMap = {}

    self.qnModelPreviewer = self.transform:Find("Panel/qnModelPreviewer"):GetComponent(typeof(CFashionPreviewTextureLoader))
    self.changeXianFanRadioBox = self.transform:Find("Panel/qnModelPreviewer/Model/changeXianFanRadioBox"):GetComponent(typeof(QnRadioBox))
    self.m_takeOffButton = self.transform:Find("Panel/TakeOffButton"):GetComponent(typeof(QnButton))
    self.m_TableView = self.transform:Find("Panel/ShopRoot"):GetComponent(typeof(QnTableView))
    self.m_ZhongCaoButton = self.transform:Find("Panel/ZhongCao/ZhongCaoButton").gameObject
    self.m_CurCount = self.transform:Find("Panel/ZhongCao/Icon/CurCount"):GetComponent(typeof(UILabel))
    self.m_DailyCount = self.transform:Find("Panel/ZhongCao/Icon/DailyCount"):GetComponent(typeof(UILabel))

    -- 脱下预览
    self.m_takeOffButton.OnClick = DelegateFactory.Action_QnButton(function (btn)
        self:OnTakeOffButtonClick()
        self.m_TableView:SetSelectRow(-1, true)
    end)

    -- reset btn
    UIEventListener.Get(self.qnModelPreviewer.resetButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(self.qnModelPreviewer.resetButton).onClick, DelegateFactory.VoidDelegate(function()
        self:OnReset()
    end), true)

    self:InitModelPreviewer()
    self:InitShopRoot()
    self:InitRadioBox()
end

function CLuaShuangshiyi2020FashionPreviewWnd:OnReset()
    local pos = Vector3(self.defaultModelLocalPos.x, self.defaultModelLocalPos.y, self.defaultModelLocalPos.z)
    self:SetModelPosAndScale(pos, 1)
end


function CLuaShuangshiyi2020FashionPreviewWnd:FixPos(pos)
    return pos.x + 0.56, pos.y, pos.z / 2
end

function CLuaShuangshiyi2020FashionPreviewWnd:InitData()
    local s = Fashion_Setting.GetData("FashionPreview_DefaultModelLocalPos").Value
    local function split( str,reps )
        local resultStrList = {}
        string.gsub(str,'[^'..reps..']+',function ( w )
            table.insert(resultStrList,w)
        end)
        return resultStrList[1], resultStrList[2], resultStrList[3]
    end
    -- 坐标与修正
    self.defaultModelLocalPos = Vector3.zero
    self.defaultModelLocalPos.x, self.defaultModelLocalPos.y, self.defaultModelLocalPos.z = split(s, ',')
    self.defaultModelLocalPos.x, self.defaultModelLocalPos.y, self.defaultModelLocalPos.z = CLuaShuangshiyi2020FashionPreviewWnd:FixPos(self.defaultModelLocalPos)
    -- 根据角色获得偏移与缩放
    if CClientMainPlayer.Inst then
        Fashion_FashionPreview.Foreach(function (key, data)
            local class = CommonDefs.Convert_ToUInt32(CClientMainPlayer.Inst.Class)
            local gender = CommonDefs.Convert_ToUInt32(CClientMainPlayer.Inst.Gender)
            if data.Class == class and data.Gender == gender then
                self.maxPreviewScale = data.MaxPreviewScale
                self.maxPreviewOffsetY = data.MaxPreviewOffsetY
            end
        end)
    end
end

function CLuaShuangshiyi2020FashionPreviewWnd:InitRadioBox()
    local appearData = self.qnModelPreviewer.needShowAppearance
    self.m_IsSupportFeiSheng = CClientMainPlayer.IsFashionSupportFeiSheng(appearData.HeadFashionId, appearData.BodyFashionId, appearData.HideHeadFashionEffect, appearData.HideBodyFashionEffect)
    self.changeXianFanRadioBox:Awake()
    self.changeXianFanRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function (btn, index)
        if index == 1 then
            --仙身
            if self.m_IsSupportFeiSheng then
                appearData.FeiShengAppearanceXianFanStatus = 1
            else
                g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("穿戴凡身拓本时无法预览仙身外观"))
                self.changeXianFanRadioBox:ChangeTo(0, false)
            end
        else
            --凡身
            if CClientMainPlayer.Inst.Is150GhostBody or CClientMainPlayer.Inst.Is150GhostHead then
                g_MessageMgr:ShowMessage("Tip_FanShen_Ghost150")
            end
            appearData.FeiShengAppearanceXianFanStatus = 0
        end
        -- local pos = Vector3(self.defaultModelLocalPos.x, self.defaultModelLocalPos.y, self.defaultModelLocalPos.z)
        -- self:SetModelPosAndScale(pos, 1)
        self:InitModelPreviewer()
    end)
    self.changeXianFanRadioBox.Visible = CClientMainPlayer.Inst.HasFeiSheng
    local isXianShen = appearData.FeiShengAppearanceXianFanStatus > 0 and self.m_IsSupportFeiSheng
    self.changeXianFanRadioBox:ChangeTo( isXianShen and 1 or 0, false)
end

function CLuaShuangshiyi2020FashionPreviewWnd:OnEnable()
    GestureRecognizer.Inst:AddNeedUIPinchWnd(CLuaUIResources.Shuangshiyi2020FashionPreviewWnd)
    self.onPinchInDelegate = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchIn(gesture)end)
    self.onPinchOutDelegate = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchOut(gesture) end)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchIn,  self.onPinchInDelegate)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchOut, self.onPinchOutDelegate)

    g_ScriptEvent:AddListener("MouseScrollWheel",self,"OnMouseScrollWheel")
    g_ScriptEvent:AddListener("PlayerZhongCaoRet2020", self, "OnPlayerZhongCaoRet")
    if CLuaShuangshiyi2020Mgr.m_EnableMultiLight then
        self.lastpixelLightCount = 0
        QualitySettings.pixelLightCount = 3
        if Shader.globalMaximumLOD == ShaderLodParams.SHADER_LOD_HIGH_LEVEL then
            Shader.globalMaximumLOD = ShaderLodParams.SHADER_LOD_MULTILIGHT
        end
    end

end

function CLuaShuangshiyi2020FashionPreviewWnd:OnDisable()
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchIn, self.onPinchInDelegate)
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchOut, self.onPinchOutDelegate)
    g_ScriptEvent:RemoveListener("PlayerZhongCaoRet2020", self, "OnPlayerZhongCaoRet")
    self.onPinchInDelegate = nil
    self.onPinchOutDelegate = nil
    LuaShopMallFashionPreviewMgr:OnClose()
    g_ScriptEvent:RemoveListener("MouseScrollWheel",self,"OnMouseScrollWheel")
    if CLuaShuangshiyi2020Mgr.m_EnableMultiLight and self.lastpixelLightCount then
        QualitySettings.pixelLightCount = self.lastpixelLightCount
        if Shader.globalMaximumLOD == ShaderLodParams.SHADER_LOD_MULTILIGHT then
            Shader.globalMaximumLOD = ShaderLodParams.SHADER_LOD_HIGH_LEVEL
        end
    end
end

function CLuaShuangshiyi2020FashionPreviewWnd:FixInitModelPreviewer()
    local t = self:GetModelRoot()
    t.gameObject:SetActive(false)
    -- 重新加载模型
    self.qnModelPreviewer:LoadModel()
    self.qnModelPreviewer.m_RO:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function (ro)
        local onRoFinshedEvent = function(_ro)
            --更新Ro Visible时刷新一波特效的Visible
            if _ro.m_FXTable then
                CommonDefs.DictIterate(_ro.m_FXTable, DelegateFactory.Action_object_object(function(key,val)
                    val:Refresh()
                end))
            end
        end
        if ro.VehicleRO then
            ro.VehicleRO:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function (_ro)
                local t = self:GetModelRoot()
                t.gameObject:SetActive(true)
                onRoFinshedEvent(ro)
                onRoFinshedEvent(ro.VehicleRO)
            end))
        else
            local t = self:GetModelRoot()
            t.gameObject:SetActive(true)
            onRoFinshedEvent(ro)
        end
    end))
end

function CLuaShuangshiyi2020FashionPreviewWnd:InitModelPreviewer(isNeedResetModelScale)
    LuaShopMallFashionPreviewMgr:InitForLuozhuangSetting()
    if not self.bfirstInit then
        self.qnModelPreviewer:Init(false)
    else
        self:FixInitModelPreviewer()
    end

    LuaModelTextureLoaderMgr:SetModelCameraColor(self.qnModelPreviewer.identifier)

    local d = (self.maxPreviewScale) - 1
    local t = (self.localModelScale - 1) /  d
    t = math.max(math.min(1,t),0)
    local y = math.lerp(0, self.maxPreviewOffsetY, t)
    local pos = Vector3(self.defaultModelLocalPos.x, self.defaultModelLocalPos.y + y, self.defaultModelLocalPos.z)

    self:SetModelPosAndScale(pos, isNeedResetModelScale and 1 or  self.localModelScale)
    self:InitShadowCameraAndLight()
    
    if self.floor then  self.floor.transform.localPosition = self.floorInitialLocalPos end
    if self.beiguangLight then self.beiguangLight.gameObject:SetActive(true) end
end

function CLuaShuangshiyi2020FashionPreviewWnd:SetModelPosAndScale(pos, scale)
    self.localModelScale = scale
    if scale > 0 then
        pos = Vector3(pos.x / scale, pos.y / scale, pos.z /scale)
    end
    self.localModelPos = pos
    local PreviewScale = 1

    CUIManager.SetModelScale(self.qnModelPreviewer.identifier, Vector3(PreviewScale,PreviewScale,PreviewScale))
    CUIManager.SetModelPosition(self.qnModelPreviewer.identifier, pos)

    self:SetShadowCameraAndLightPos(pos)
	CMainCamera.Inst:OnPreRenderShadowmap()
end

function CLuaShuangshiyi2020FashionPreviewWnd:InitShopRoot()
    local maxZhongcaoPerDay = Double11_Setting.GetData().MaxZhongcaoPerDay
    local extraZhongcaoTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eDouble11ZhongCaoAdjustLimit) or 0
    maxZhongcaoPerDay = maxZhongcaoPerDay + extraZhongcaoTimes
    self.m_DailyCount.text = "/" .. maxZhongcaoPerDay

    self.m_CurCao = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eShuangshiyi2020ZhongCao)
    self.m_CurCount.text = maxZhongcaoPerDay - self.m_CurCao
 
     UIEventListener.Get(self.m_ZhongCaoButton).onClick=DelegateFactory.VoidDelegate(function(go)
        local currentSelected = self.m_TableView.currentSelectRow
        if currentSelected>-1 then
            local itemId = self.m_DataList[currentSelected+1].ItemId
            CLuaShuangshiyi2020Mgr.m_ZhongCaoItem = itemId
            Gac2Gas.RequestZhongCao2020(itemId, 1)
        else
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请选择要种草的时装"))
        end
     end)

    self:ReloadItemData()
end

function CLuaShuangshiyi2020FashionPreviewWnd:ReloadItemData()
    self.m_TableView.m_DataSource=DefaultTableViewDataSource.Create(
        function()
            return #self.m_DataList
        end,function(item,index)
            self:InitItem(item.transform,self.m_DataList[index+1])
        end)
    
    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function(row)
        local templateId = self.m_DataList[self.m_TableView.currentSelectRow+1].ItemId
        local fashionId = self.m_DataList[self.m_TableView.currentSelectRow+1].FashionID

        local fashion = Fashion_Fashion.GetData(fashionId)
        local itemId = fashion.ItemID
        self:OnItemClick(fashionId, itemId, templateId)
    end)

    self.m_TableView:ReloadData(false, false)

    if CLuaShuangshiyi2020Mgr.m_PreviewItem~=nil then
        for i=1,#self.m_DataList do
            local templateId = self.m_DataList[i].ItemId
            if templateId == CLuaShuangshiyi2020Mgr.m_PreviewItem then
                local fashionId = self.m_DataList[i].FashionID
                self.m_TableView:SetSelectRow(i-1,true)
                local fashion = Fashion_Fashion.GetData(fashionId)
                local itemId = fashion.ItemID
                self:OnItemClick(fashionId, itemId, templateId)
            end
        end
    end

end

function CLuaShuangshiyi2020FashionPreviewWnd:InitItem(transform,info)
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local priceLabel = transform:Find("PriceLabel"):GetComponent(typeof(UILabel))
    local count = self.m_CountLookup[info.ItemId] or 0
    priceLabel.text=tostring(count)
    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local hotSprite = transform:Find("HotSprite").gameObject
    -- hot就是热标记为
    if self.m_HotLookup[info.ItemId] then
        hotSprite:SetActive(true)
    else
        hotSprite:SetActive(false)
    end

    local template = Item_Item.GetData(info.ItemId)
    if template then
        icon:LoadMaterial(template.Icon)
        nameLabel.text=template.Name .. LocalString.GetString("(30天)")
        UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            local id = template.ID
            CItemInfoMgr.ShowLinkItemTemplateInfo(id, false, nil, AlignType.Default, 0, 0, 0, 0)
        end)
    end
end

-- 点击进行换装
function CLuaShuangshiyi2020FashionPreviewWnd:OnItemClick(fashionId, itemId, templateId)
    if fashionId ~= 0 then
        CFashionPreviewMgr.curShopMallPreviewType = EnumShopMallFashionPreviewType.PreviewAllFashion
        CLuaShuangshiyi2020Mgr.SetFashionId(fashionId)
        self:InitModelPreviewer()
        g_ScriptEvent:BroadcastInLua("ShuangshiyiFashionPreviewWnd_UpdateDescription",{itemId, templateId, LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ShiZhuang })
    end
end

function CLuaShuangshiyi2020FashionPreviewWnd:OnPinchIn(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0),1)
    self:OnPinch(-pinchScale)
end

function CLuaShuangshiyi2020FashionPreviewWnd:OnPinchOut(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0),1)
    self:OnPinch(pinchScale)
end

function CLuaShuangshiyi2020FashionPreviewWnd:OnMouseScrollWheel()
    local v = Input.GetAxis("Mouse ScrollWheel")
    self:OnPinch(v)
end

function CLuaShuangshiyi2020FashionPreviewWnd:OnPinch(delta)
    local d = (self.maxPreviewScale) - 1
    local t = (self.localModelScale - 1) /  d

    t = t + delta

    t = math.max(math.min(1,t),0)
    local s = math.lerp(1, self.maxPreviewScale, t)
    local y = math.lerp(0, self.maxPreviewOffsetY, t)
    local pos = Vector3(self.defaultModelLocalPos.x, self.defaultModelLocalPos.y + y, self.defaultModelLocalPos.z)
    self:SetModelPosAndScale(pos, s)
end

function CLuaShuangshiyi2020FashionPreviewWnd:InitShadowCameraAndLight()
    if self.bfirstInit then return end
    self.bfirstInit = true

    if not CLuaShuangshiyi2020Mgr.m_EnableMultiLight then return end
    local t = CUIManager.instance.transform:Find(self.qnModelPreviewer.identifier)
    if not t then return end
    self.shadowCamera = t:Find("UI Model Shadow Camera")
    self.shadowCameraRelativePos = {
            self.localModelPos.x - self.shadowCamera.localPosition.x,
            self.localModelPos.y - self.shadowCamera.localPosition.y,
            self.localModelPos.z - self.shadowCamera.localPosition.z
    }
    local newRO = CRenderObject.CreateRenderObject(t.gameObject, "LightAndFloorAndWall")
    local fx = CEffectMgr.Inst:AddObjectFX(88801543, newRO ,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero, DelegateFactory.Action_GameObject(function(go)
        NGUITools.SetLayer(newRO .gameObject,LayerDefine.Effect_3D)
        self.floor = go.transform:Find("Anchor/beijing")
        self.floorInitialLocalPos = self.floor.transform.localPosition
        self.beiguangLight = go.transform:Find("Anchor/beiguang")
    end))
    self.lightAndFloorAndWall = newRO.transform
    self.lightAndFloorAndWallRelativePos = {
        self.localModelPos.x - self.lightAndFloorAndWall.localPosition.x,
        self.localModelPos.y - self.lightAndFloorAndWall.localPosition.y,
        self.localModelPos.z - self.lightAndFloorAndWall.localPosition.z
    }
    newRO:AddFX("LightAndFloorAndWall", fx, -1)
end

function CLuaShuangshiyi2020FashionPreviewWnd:SetShadowCameraAndLightPos(pos)
    if not self.shadowCamera then return end
    if (not self.shadowCameraRelativePos) or (#self.shadowCameraRelativePos ~= 3) then return end
    self.shadowCamera.transform.localPosition = Vector3(pos.x - self.shadowCameraRelativePos[1],pos.y - self.shadowCameraRelativePos[2],pos.z - self.shadowCameraRelativePos[3])
    self.lightAndFloorAndWall.transform.localPosition = Vector3(
            pos.x - self.lightAndFloorAndWallRelativePos[1],
            pos.y - self.lightAndFloorAndWallRelativePos[2],
            pos.z - self.lightAndFloorAndWallRelativePos[3])

end

function CLuaShuangshiyi2020FashionPreviewWnd:GetModelRoot()
    if not self.modelRoot then
        local t = CUIManager.instance.transform:Find(self.qnModelPreviewer.identifier)
        if t then
            self.modelRoot = t:Find("ModelRoot")
        end
    end
    return self.modelRoot
end

function CLuaShuangshiyi2020FashionPreviewWnd:OnPlayerZhongCaoRet(mallItemId, count)
    -- 对某个时装 进行种草
    self.m_CountLookup[mallItemId] = (self.m_CountLookup[mallItemId] or 0) + count

    table.sort( self.m_DataList, function(a,b)
        local itemId1 = a.ItemId
        local itemId2 = b.ItemId
        if self.m_CountLookup[itemId1]>self.m_CountLookup[itemId2] then
            return true
        elseif self.m_CountLookup[itemId1]<self.m_CountLookup[itemId2] then
            return false
        else
            return itemId1<itemId2
        end
    end )

    self.m_HotLookup={}
    local max = math.min(5, #self.m_DataList)
    for i=1,max do
        local itemId = self.m_DataList[i].ItemId
        local count = self.m_CountLookup[itemId] or 0
        if count > 500 then
            self.m_HotLookup[itemId] = true
        end
    end

    self.m_TableView:ReloadData(false, false)

     for i=1,#self.m_DataList do
        if self.m_DataList[i].ItemId == mallItemId then
            self.m_TableView:SetSelectRow(i-1,true)
        else
            local itemFx = self.m_TableView:GetItemAtRow(i-1).transform:Find("ItemFx"):GetComponent(typeof(CUIFx))
            itemFx.gameObject:SetActive(false)
        end
    end

    local currentSelected = self.m_TableView.currentSelectRow
    local itemFx = self.m_TableView:GetItemAtRow(currentSelected).transform:Find("ItemFx"):GetComponent(typeof(CUIFx))
    itemFx.gameObject:SetActive(true)
    itemFx:DestroyFx()
    itemFx:LoadFx(CUIFxPaths.GQJCTeamNumChangeFx)
    self.m_CurCount.text = tostring(self.m_CurCount.text - 1)
end

function CLuaShuangshiyi2020FashionPreviewWnd:OnTakeOffButtonClick()
    CFashionPreviewMgr.curShopMallPreviewType = EnumShopMallFashionPreviewType.TakeOffAlllFashion
    CFashionPreviewMgr.HeadFashionID = 0
    CFashionPreviewMgr.BodyFashionID = 0
    CFashionPreviewMgr.ZuoQiFashionID = 0
    CFashionPreviewMgr.UmbrellaFashionID = 0
    self:InitModelPreviewer()
end
