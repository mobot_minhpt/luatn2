-- Auto Generated!!
local CHuluBrothersRankItem = import "L10.UI.CHuluBrothersRankItem"
local LocalString = import "LocalString"
local Profession = import "L10.Game.Profession"
CHuluBrothersRankItem.m_Init_CS2LuaHook = function (this, rank) 
    if rank.m_Rank <= 0 then
        this.m_RankLabel.text = LocalString.GetString("未上榜")
    else
        this.m_RankLabel.text = tostring(rank.m_Rank)
    end
    this.m_ClassSprite.spriteName = Profession.GetIcon(rank.m_Clazz)
    this.m_NameLabel.text = rank.m_Name
    if rank.m_Time <= 0 then
        this.m_TimeLabel.text = LocalString.GetString("无")
    else
        this.m_TimeLabel.text = System.String.Format(LocalString.GetString("{0:00}分{1:00}秒"), math.floor(rank.m_Time / 60), rank.m_Time % 60)
    end
end
