local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"

LuaFightingSpiritGenerateChampionWnd = class()

LuaFightingSpiritGenerateChampionWnd.s_NthMatch = nil
LuaFightingSpiritGenerateChampionWnd.s_Level = nil
LuaFightingSpiritGenerateChampionWnd.s_TeamName = nil
LuaFightingSpiritGenerateChampionWnd.s_LeaderName = nil
LuaFightingSpiritGenerateChampionWnd.s_WinTeamId = nil
LuaFightingSpiritGenerateChampionWnd.s_Time = nil

RegistClassMember(LuaFightingSpiritGenerateChampionWnd, "TitleLabel")
RegistClassMember(LuaFightingSpiritGenerateChampionWnd, "TimeLabel")
RegistClassMember(LuaFightingSpiritGenerateChampionWnd, "ServerLabel")
RegistClassMember(LuaFightingSpiritGenerateChampionWnd, "GroupLabel")
RegistClassMember(LuaFightingSpiritGenerateChampionWnd, "TeamLabel")
RegistClassMember(LuaFightingSpiritGenerateChampionWnd, "TeamBtn")
RegistClassMember(LuaFightingSpiritGenerateChampionWnd, "ShareBtn")
RegistClassMember(LuaFightingSpiritGenerateChampionWnd, "ChongXiaoBg")
RegistClassMember(LuaFightingSpiritGenerateChampionWnd, "OtherBg")
RegistClassMember(LuaFightingSpiritGenerateChampionWnd, "CloseBtn")
RegistClassMember(LuaFightingSpiritGenerateChampionWnd, "Logo")
RegistClassMember(LuaFightingSpiritGenerateChampionWnd, "ClickCheck")

RegistClassMember(LuaFightingSpiritGenerateChampionWnd, "m_IdList")
RegistClassMember(LuaFightingSpiritGenerateChampionWnd, "m_LeaderId")
RegistClassMember(LuaFightingSpiritGenerateChampionWnd, "m_LevelName")
RegistClassMember(LuaFightingSpiritGenerateChampionWnd, "m_ServerName")
RegistClassMember(LuaFightingSpiritGenerateChampionWnd, "m_ChampionID2Lv")
RegistClassMember(LuaFightingSpiritGenerateChampionWnd, "m_ShowCloseBtnTick")

function LuaFightingSpiritGenerateChampionWnd:Awake()
    self.TitleLabel = self.transform:Find("Anchor/Panel/Header/Label"):GetComponent(typeof(UILabel))
    self.TimeLabel = self.transform:Find("Anchor/Panel/Header/Time"):GetComponent(typeof(UILabel))
    self.ServerLabel = self.transform:Find("Anchor/Panel/ServerLabel"):GetComponent(typeof(UILabel))
    self.GroupLabel = self.transform:Find("Anchor/Panel/GroupLabel"):GetComponent(typeof(UILabel))
    self.TeamLabel = self.transform:Find("Anchor/Panel/TeamLabel"):GetComponent(typeof(UILabel))
    self.TeamBtn = self.transform:Find("Anchor/Panel/TeamBtn").gameObject
    self.ShareBtn = self.transform:Find("Anchor/Panel/ShareBtn").gameObject
    self.ChongXiaoBg = self.transform:Find("Anchor/Bg/ChongXiao").gameObject
    self.OtherBg = self.transform:Find("Anchor/Bg/Other").gameObject
    self.CloseBtn = self.transform:Find("Anchor/CloseButton").gameObject
    self.Logo = self.transform:Find("Anchor/Panel/Logo").gameObject
    self.ClickCheck = self.transform:Find("ClickCheck").gameObject
    
    self.CloseBtn:SetActive(false)

    self.TeamBtn:SetActive(false)
    UIEventListener.Get(self.TeamBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_LevelName and self.m_ServerName and self.m_ChampionID2Lv and self.m_IdList and self.m_LeaderId then
            CLuaFightingSpiritMgr.LevelName = self.m_LevelName
            CLuaFightingSpiritMgr.ServerName = self.m_ServerName
            CLuaFightingSpiritMgr.ChampionID2Lv = self.m_ChampionID2Lv
            Gac2Gas.QueryDouHunGroupAppearanceInfo(g_MessagePack.pack(self.m_IdList), self.m_LeaderId)
        end
    end)

    self.ShareBtn:SetActive(not CommonDefs.IS_VN_CLIENT)
    UIEventListener.Get(self.ShareBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self.ShareBtn:SetActive(false)
        self.CloseBtn:SetActive(false)
        --self.TeamBtn:SetActive(false)
        self.Logo:SetActive(true)
        CUIManager.SetUITop(CLuaUIResources.FightingSpiritGenerateChampionWnd)
        CUICommonDef.CaptureScreen("screenshot", true, false, nil, DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            self.ShareBtn:SetActive(true)
            self.CloseBtn:SetActive(true)
            --self.TeamBtn:SetActive(true)
            self.Logo:SetActive(false)
            CUIManager.ResetUITop(CLuaUIResources.FightingSpiritGenerateChampionWnd)
        end))
    end)

    UIEventListener.Get(self.ClickCheck).onClick = DelegateFactory.VoidDelegate(function(go)
        self.ClickCheck:SetActive(false)
        self.CloseBtn:SetActive(true)
        UnRegisterTick(self.m_ShowCloseBtnTick)
        self.m_ShowCloseBtnTick = nil
    end)
end

function LuaFightingSpiritGenerateChampionWnd:Init()
    self.TitleLabel.text = SafeStringFormat3(LocalString.GetString("第%s届斗魂坛"), Extensions.ConvertToChineseString(LuaFightingSpiritGenerateChampionWnd.s_NthMatch))
    local dt = CServerTimeMgr.ConvertTimeStampToZone8Time(LuaFightingSpiritGenerateChampionWnd.s_Time)
    if CommonDefs.IS_VN_CLIENT then
        self.TimeLabel.text = dt.Year..LocalString.GetString("-")..(dt.Month > 9 and dt.Month or "0"..dt.Month)..LocalString.GetString("-")..(dt.Day > 9 and dt.Day or "0"..dt.Day)
    else
        self.TimeLabel.text = dt.Year..LocalString.GetString("年")..(dt.Month > 9 and dt.Month or "0"..dt.Month)..LocalString.GetString("月")..(dt.Day > 9 and dt.Day or "0"..dt.Day)..LocalString.GetString("日")
    end
    self.GroupLabel.text = DouHunCross_Setting.GetData().LevelName[LuaFightingSpiritGenerateChampionWnd.s_Level - 1]
    if LuaFightingSpiritGenerateChampionWnd.s_Level ~= 1 then
        --self.GroupLabel.text = LocalString.GetString("其他组")
        self.GroupLabel.color = NGUIText.ParseColor24("342110", 0)
    end

    if CommonDefs.IS_VN_CLIENT then
        --self.TimeLabel.text = dt.Year..LocalString.GetString("-")..(dt.Month > 9 and dt.Month or "0"..dt.Month)..LocalString.GetString("-")..(dt.Day > 9 and dt.Day or "0"..dt.Day)
        if LocalString.language == "cn" then
            self.TeamLabel.text = LuaFightingSpiritGenerateChampionWnd.s_LeaderName..LocalString.GetString("战队")
        elseif LocalString.language == "vn" or LocalString.language == "en" or LocalString.language == "ina" then
            self.TeamLabel.text = LocalString.GetString("战队") .. " " .. LuaFightingSpiritGenerateChampionWnd.s_LeaderName
        else
            self.TeamLabel.text = LocalString.GetString("战队") .. LuaFightingSpiritGenerateChampionWnd.s_LeaderName
        end
    else
        self.TeamLabel.text = LuaFightingSpiritGenerateChampionWnd.s_LeaderName..LocalString.GetString("战队")
    end
    
    self.ChongXiaoBg:SetActive(LuaFightingSpiritGenerateChampionWnd.s_Level == 1)
    self.OtherBg:SetActive(LuaFightingSpiritGenerateChampionWnd.s_Level ~= 1)

    UnRegisterTick(self.m_ShowCloseBtnTick)
    self.m_ShowCloseBtnTick = RegisterTickOnce(function()
        self.ClickCheck:SetActive(false)
        self.CloseBtn:SetActive(true)
    end, DouHunCross_Setting.GetData().ChampionPage * 1000)

    Gac2Gas.QueryDouHunCrossChampionGroupInfo(LuaFightingSpiritGenerateChampionWnd.s_Level)
end

function LuaFightingSpiritGenerateChampionWnd:OnDestroy()
    UnRegisterTick(self.m_ShowCloseBtnTick)
    self.m_ShowCloseBtnTick = nil
end

function LuaFightingSpiritGenerateChampionWnd:OnEnable()
	g_ScriptEvent:AddListener("QueryDouHunCrossChampionGroupInfoResult", self, "QueryDouHunCrossChampionGroupInfoResult")
end

function LuaFightingSpiritGenerateChampionWnd:OnDisable()
	g_ScriptEvent:RemoveListener("QueryDouHunCrossChampionGroupInfoResult", self, "QueryDouHunCrossChampionGroupInfoResult")
end

function LuaFightingSpiritGenerateChampionWnd:QueryDouHunCrossChampionGroupInfoResult(level, serverName, leaderId, infoList)
    self.ServerLabel.text = serverName

    self.m_LevelName = DouHunCross_Setting.GetData().LevelName[level - 1]
    self.m_LeaderId = leaderId
	self.m_ServerName = serverName

    local id2Lv = {}
    for i=1, #infoList do
        local info = infoList[i]
        id2Lv[info.PlayerId] = info.Grade
    end
    self.m_ChampionID2Lv = id2Lv

    self.m_IdList = {}
    for _, info in ipairs(infoList) do
        table.insert(self.m_IdList, info.PlayerId)
    end
end
