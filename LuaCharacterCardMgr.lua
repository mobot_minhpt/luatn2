LuaCharacterCardMgr = {}
LuaCharacterCardMgr.ShareBigPicId = nil

-- 同步玩家身上的玩法数据,某一个卡槽的状态发生改变
function Gas2Gac.SyncRdcPropSlotValue(slotIdx, newValue)
	-- propertyPlay -> RoleDrawingCardData -> SlotStatus 这个字段的key和value
	-- newValue 的含义参考服务端的 function CRoleDrawingCardMgr:DecodeSlotStatus(value),可以解析为status和count,分别表示是否填充(status为1表示当前卡槽填充了,count表示当前卡槽虚拟卡片的数量)
  local isNew = newValue % 10
  local count = math.floor(newValue/10)

  if not LuaCharacterCardMgr.cardInfo then
    LuaCharacterCardMgr.cardInfo = {}
  end
  LuaCharacterCardMgr.cardInfo[slotIdx] = {isNew = isNew,count = count}

  g_ScriptEvent:BroadcastInLua('UpdateCharacterCardInfo')
end

-- 同步玩家身上的玩法数据的新的过期时间,之前的数据全部过期
function Gas2Gac.SyncRdcPropResetExpire(slotIdx, newValue)
	-- propertyPlay -> RoleDrawingCardData -> ExpireTime 这个字段的最新值设置一下,同时意味着以前的数据全部过期, WholeSetFlag 和 SlotStatus 以及 ShareRewardTime 的值要清空
end

-- 同步玩家身上的玩法数据，上一次分享时间
function Gas2Gac.SyncRdcPropShareTime(shareTime)
	-- propertyPlay -> RoleDrawingCardData -> ShareRewardTime
end

-- 同步玩家身上的玩法数据，获得集齐全家福奖励标记，等于0的时候才可以领取集齐奖励,否则表示已经领取过
function Gas2Gac.SyncRdcPropAllSetReward(newFlag)
	-- propertyPlay -> RoleDrawingCardData -> WholeSetFlag
	if not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.PlayProp then
		return
	end
	CClientMainPlayer.Inst.PlayProp.RoleDrawingCardData.WholeSetFlag = newFlag
  g_ScriptEvent:BroadcastInLua('UpdateCharacterCardInfo')
end

function Gas2Gac.SyncRdcDataUpdate()
  g_ScriptEvent:BroadcastInLua('UpdateCharacterCardInfo')
end

-- 数据是list格式
function Gas2Gac.SyncRdcPropAllSlot(allSlotDataListUd)
  local slotData = MsgPackImpl.unpack(allSlotDataListUd)
  LuaCharacterCardMgr.cardInfo = {}

  if slotData then
    local length = slotData.Count
    for i=0,length-1 do
      local data = slotData[i]
      local isNew = data % 10
      local count = math.floor(data/10)
      table.insert(LuaCharacterCardMgr.cardInfo,{isNew = isNew,count = count})
    end

    if CUIManager.IsLoaded(CLuaUIResources.CharacterCardWnd) then
      g_ScriptEvent:BroadcastInLua('UpdateCharacterCardInfo')
    else
      CUIManager.ShowUI(CLuaUIResources.CharacterCardWnd)
    end
  end
end

-- 批量合成卡片的返回,结构是由idx组成的list
function Gas2Gac.SyncRdcBatchAddCard(cardIdxUd)
  local slotData = MsgPackImpl.unpack(cardIdxUd)
  if slotData then
    local length = slotData.Count
    local newDataTable = {}
    for i=0,length-1 do
      local data = slotData[i]
      table.insert(newDataTable,tonumber(data))
    end
    g_ScriptEvent:BroadcastInLua('CharacterCardHeUpdate',newDataTable)
  end
end
