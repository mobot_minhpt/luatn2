local CCCChatMgr = import "L10.Game.CCCChatMgr"
local HTTPHelper = import "L10.Game.HTTPHelper"
local DelegateFactory = import "DelegateFactory"
local Main = import "L10.Engine.Main"
local CommonDefs = import "L10.Game.CommonDefs"
local MessageWndManager = import "L10.UI.MessageWndManager"
local LocalString = import "LocalString"
local Json = import "L10.Game.Utils.Json"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLoginMgr = import "L10.Game.CLoginMgr"
local WWW = import "UnityEngine.WWW"
local CJoinCCChannelRequestSourceInfo = import "L10.Game.CJoinCCChannelRequestSourceInfo"
local CJoinCCChanelContext = import "L10.Game.CJoinCCChanelContext"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CJingLingWebImageMgr = import "L10.Game.CJingLingWebImageMgr"
local CCMiniAPIMgr = import "L10.Game.CCMiniAPIMgr"

LuaCCChatMgr = class()

LuaCCChatMgr.CC_FOLLOW_MAX_NUM = 20 --服务器有定义
LuaCCChatMgr.QUERY_LIVE_LIST_URL = "http://api.cc.163.com/v1/gamelivelist/qn_check_anchor?ccids="--测试地址为http://api.dev.cc.163.com开头
LuaCCChatMgr.BIND_CC_URL = "http://api.cc.163.com/v1/gamerole/verify_gametoken?"--测试地址为http://api.dev.cc.163.com开头
LuaCCChatMgr.BIND_CC_GAME_TYPE = 30
LuaCCChatMgr.CC_ACTIVITY_URL = "http://api.cc.163.com/v1/gamelivelist/activity/10015?anchor_uid=%d&channel_id=%d" --测试地址为http://api.dev.cc.163.com开头

LuaCCChatMgr.FollowingTbl = {}
function LuaCCChatMgr.ChangeCCFollowing(ccid)

    --取消关注的，直接取关
    if CCCChatMgr.Inst:IsCCFollowing(ccid) then
        Gac2Gas.CcUnfollow(ccid)
        return
    end

    --关注新主播，需要检查关注列表是否已满
	if CCCChatMgr.Inst.m_CCFollowing.Count >= LuaCCChatMgr.CC_FOLLOW_MAX_NUM then

        local url = LuaCCChatMgr.QUERY_LIVE_LIST_URL
        local first = true
        CommonDefs.EnumerableIterate(CCCChatMgr.Inst.m_CCFollowing, DelegateFactory.Action_object(function (id) 
            if not first then
                url = url..","
            end
            first = false
            url = url..tostring(id)
        end))

        Main.Inst:StartCoroutine(HTTPHelper.GetUrl(url, DelegateFactory.Action_bool_string(function (success, result)
            if success then
                LuaCCChatMgr.ProcessQueryFollowingList(result, ccid)
            else
                LuaCCChatMgr.DoChangeCCFollowing(ccid)
            end
        end)))
    else
        LuaCCChatMgr.DoChangeCCFollowing(ccid)
    end
end

function LuaCCChatMgr.ProcessQueryFollowingList(jsonData, origin_ccid)

    local list = Json.Deserialize(jsonData)
    LuaCCChatMgr.FollowingTbl = {}
    if list then
        for i=0,list.Count-1 do
            --包括的项目 followed、ccid、game、server、nickname、anchor_uid、label、priority、del、anchor_tag、purl
            local dict = list[i]
            local data = {}
            data.followed = tonumber(CommonDefs.DictGetValue_LuaCall(dict, "followed") or 0)
            data.ccid = tonumber(CommonDefs.DictGetValue_LuaCall(dict, "ccid") or 0)
            data.nickname = tostring(CommonDefs.DictGetValue_LuaCall(dict, "nickname") or "")
            data.del = tonumber(CommonDefs.DictGetValue_LuaCall(dict, "del") or 0)
            data.anchor_tag = tostring(CommonDefs.DictGetValue_LuaCall(dict, "anchor_tag") or "")
            data.purl = tonumber(CommonDefs.DictGetValue_LuaCall(dict, "purl") or "")
            table.insert(LuaCCChatMgr.FollowingTbl, data)
        end
    end

    local list = CreateFromClass(MakeGenericClass(List, Object))
    
    for __,data in pairs(LuaCCChatMgr.FollowingTbl) do
        if data.del == 1 then
            CommonDefs.ListAdd_LuaCall(list, data.ccid)
        end
    end
    if list.Count > 0 then
        Gac2Gas.CcUnfollowVer2(MsgPackImpl.pack(list), false)
        Gac2Gas.CcFollow(origin_ccid)
    else
        local msg = g_MessageMgr:FormatMessage("CC_CLEAR_EXPIRE_ZHUBO")
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function() 
            CUIManager.ShowUI("CCLiveFollowingListWnd")
        end), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    end

end

function LuaCCChatMgr.DoChangeCCFollowing(ccid)
    if CCCChatMgr.Inst:IsCCFollowing(ccid) then
        Gac2Gas.CcUnfollow(ccid)
    else
        Gac2Gas.CcFollow(ccid)
    end
end

function LuaCCChatMgr.UploadCcCaptureLog(captureTargetStream, captureType, extraInfo)
    --captureTargetStream 对应 EnumCCMiniStreamType 的int值
    --captureType取值0 和 1，分别表示开麦和闭麦
    --extraInfo为字符串
    if captureTargetStream==nil or captureType==nil then return end
    if extraInfo == nil then extraInfo = "" end
    Gac2Gas.CcCaptureLog(captureTargetStream, captureType, extraInfo)
end

function LuaCCChatMgr.RequestCCSpeak(streamName, extraInfo)
    Gac2Gas.RequestCCSpeak(streamName, extraInfo)
end

function LuaCCChatMgr.DoBindCC(token)
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return end
    local gametype = LuaCCChatMgr.BIND_CC_GAME_TYPE --每个游戏gametype都不一样
    local server = mainplayer.Id --游戏角色ID
    local servername = WWW.EscapeURL(mainplayer:GetMyServerName()) --游戏角色所在服务器名字
    local serverhost = mainplayer:GetMyServerId() --游戏角色所有服务器ID
    local level = mainplayer.Level --游戏角色等级
    local name = WWW.EscapeURL(mainplayer.BasicProp.Name) --游戏角色名字,这里直接取，避免变身导致名字变了
    local urs = CLoginMgr.Inst:GetAccountName() --渠道号或者URS
    local url = SafeStringFormat3("%stoken=%s&gametype=%d&server=%s&servername=%s&serverhost=%d&level=%d&name=%s&urs=%s",
        LuaCCChatMgr.BIND_CC_URL,WWW.EscapeURL(token), gametype,server,servername, serverhost,level,name,urs)

    Main.Inst:StartCoroutine(HTTPHelper.GetUrl(url, DelegateFactory.Action_bool_string(function (success, result)
            LuaCCChatMgr.OnBindCCCallback(success, result)
        end)))
end

function LuaCCChatMgr.OnBindCCCallback(success, result)
    local dict = success and Json.Deserialize(result)
    if dict then
        local msg = tostring(CommonDefs.DictGetValue_LuaCall(dict, "msg") or "")
        local code = tostring(CommonDefs.DictGetValue_LuaCall(dict, "code") or "")
        if code == "OK" then
            g_MessageMgr:ShowMessage("Bind_CC_Via_Token_Success")
        elseif code == "ERR" then
            g_MessageMgr:ShowMessage("Bind_CC_Via_Token_Failed_With_Error_Msg", msg)
        else
            g_MessageMgr:ShowMessage("Bind_CC_Via_Token_Failed")
        end
    else
        g_MessageMgr:ShowMessage("Bind_CC_Via_Token_Failed")
    end
end

LuaCCChatMgr.m_CCidOnlineNotifyTbl = nil
function LuaCCChatMgr.CheckShowJoinZhiboComfirm(ccid)
    if not LuaCCChatMgr.m_CCidOnlineNotifyTbl then
        LuaCCChatMgr.m_CCidOnlineNotifyTbl = {}
    end

    local mainplayer = CClientMainPlayer.Inst
    local id = mainplayer and mainplayer.Id or 0
    if id<=0 then return false end
    if not LuaCCChatMgr.m_CCidOnlineNotifyTbl[id] then
        local tbl = {}
        tbl[ccid] = CServerTimeMgr.Inst.timeStamp
        LuaCCChatMgr.m_CCidOnlineNotifyTbl[id] = tbl
        return true
    elseif not LuaCCChatMgr.m_CCidOnlineNotifyTbl[id][ccid] then
        LuaCCChatMgr.m_CCidOnlineNotifyTbl[id][ccid] = CServerTimeMgr.Inst.timeStamp
        return true
    elseif CServerTimeMgr.Inst.timeStamp - LuaCCChatMgr.m_CCidOnlineNotifyTbl[id][ccid] > 60*30 then --超过30分钟
        LuaCCChatMgr.m_CCidOnlineNotifyTbl[id][ccid] = CServerTimeMgr.Inst.timeStamp
        return true
    else
        return false
    end
end

function LuaCCChatMgr:GetCCLiveActivityInfo(anchor_uid, cid)
    Main.Inst:StartCoroutine(HTTPHelper.GetUrl(SafeStringFormat3(self.CC_ACTIVITY_URL, anchor_uid, cid), DelegateFactory.Action_bool_string(function (success, result)
        if success then
            local activityInfo = {}
            activityInfo.anchor_uid = anchor_uid
            activityInfo.cid = cid
            local dict = Json.Deserialize(result)
            local code = tostring(CommonDefs.DictGetValue_LuaCall(dict, "code"))
            if code == "OK" then
                local data = CommonDefs.DictGetValue_LuaCall(dict, "data")
                activityInfo.icon = tostring(CommonDefs.DictGetValue_LuaCall(data, "icon"))
                activityInfo.link = tostring(CommonDefs.DictGetValue_LuaCall(data, "link"))
                activityInfo.starttime = tonumber(CommonDefs.DictGetValue_LuaCall(data, "starttime"))
                activityInfo.endtime = tonumber(CommonDefs.DictGetValue_LuaCall(data, "endtime"))
                activityInfo.available = (activityInfo.starttime and activityInfo.endtime and 
                                            CServerTimeMgr.Inst.timeStamp>=activityInfo.starttime and CServerTimeMgr.Inst.timeStamp<activityInfo.endtime or false)
                if activityInfo.available and not cs_string.IsNullOrEmpty(activityInfo.icon) then
                    local tex = CJingLingWebImageMgr.Inst:LoadImageByUrl(activityInfo.icon)
                    if tex then
                        activityInfo.iconTexture = tex
                        g_ScriptEvent:BroadcastInLua("OnGetCCLiveActivityInfo", activityInfo)
                    else
                        CJingLingWebImageMgr.Inst:DownloadImage(activityInfo.icon, DelegateFactory.Action_Texture2D(function(tex)
                            activityInfo.iconTexture = tex
                            g_ScriptEvent:BroadcastInLua("OnGetCCLiveActivityInfo", activityInfo)
                        end))
                    end
                else
                    activityInfo.iconTexture = nil
                    g_ScriptEvent:BroadcastInLua("OnGetCCLiveActivityInfo", activityInfo)
                end
            else
                g_ScriptEvent:BroadcastInLua("OnGetCCLiveActivityInfo", activityInfo)
            end
        else
        end        
    end)))
end

----------------
--RPC
----------------
function Gas2Gac.CcidOnline(data)
    local list = MsgPackImpl.unpack(data)
    if not list then return end
    local hasShowMsg = false
    for i=0,list.Count-1,3 do
        local ccid = tonumber(list[i])
        if CCCChatMgr.Inst:CheckSpecialZhubo(ccid, CJoinCCChannelRequestSourceInfo.SystemChannel) and LuaCCChatMgr.CheckShowJoinZhiboComfirm(ccid) then
            hasShowMsg = true
            break
        end
    end

    for i=0,list.Count-1,3 do
        local ccid = tonumber(list[i])
        local nickName = tostring(list[i+2])
        local nameUtf8 = CommonDefs.DecodeUnicode(nickName) -- 服务器json解出的中文为unicode字符，需要转义
        if not hasShowMsg and CCCChatMgr.Inst:IsCCFollowing(ccid) and LuaCCChatMgr.CheckShowJoinZhiboComfirm(ccid) then
            local msg = g_MessageMgr:FormatMessage("ZHUBO_SHANGXIAN_GUANZHUMSG", nameUtf8)
            MessageWndManager.ShowOKCancelMessage(msg, 
                DelegateFactory.Action(function ()
                    CCCChatMgr.Inst:JoinCCChannelByCCId(ccid, CJoinCCChanelContext.Video, CJoinCCChannelRequestSourceInfo.SystemChannel)
                end), 
                DelegateFactory.Action(function ()
                    CCCChatMgr.Inst:JoinCCChannelByCCId(ccid, CJoinCCChanelContext.Audio, CJoinCCChannelRequestSourceInfo.SystemChannel)
                end), LocalString.GetString("观看直播"), LocalString.GetString("收听音频"), true)
            hasShowMsg = true
        end

        g_MessageMgr:ShowMessage("ZHUBO_SHANGXIAN_SYSMSG", nameUtf8, nameUtf8, ccid, nameUtf8, ccid)
    end

end

function LuaCCChatMgr:RequestCCSpeakResult(bOpenOrClose, steamName, extraInfo, bForce)
    if bOpenOrClose then
        CCMiniAPIMgr.Inst:StartCCMiniCaptureInternal(steamName, extraInfo, bForce)
    elseif CCMiniAPIMgr.Inst:IsCapturing() then
        --根据浩神的建议，如果bOpenOrClose为false，那么就强制闭麦，此时可能处于管控中
        CCMiniAPIMgr.Inst:StopCCMiniCapture()
    end
end

function Gas2Gac.BroadcastCCNotify(ccid, nickName, keyword)
    local nameUtf8 = CommonDefs.DecodeUnicode(nickName)
    local data = Message_BroadcastCC.GetData(keyword)

    if data then
        local msgName = data.MessageName
        g_MessageMgr:ShowMessage(msgName, nameUtf8, nameUtf8, ccid, nameUtf8, ccid)
    end
end