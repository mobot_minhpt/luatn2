LuaSkilSelectFunctionHolder = {}
local MessageMgr = import "L10.Game.MessageMgr"
local CScene = import "L10.Game.CScene"
local NewYear2019 = import "L10.Game.NewYear2019_Setting"
local CClientMonster = import "L10.Game.CClientMonster"
local CYingLingActiveSkill_TemplateIdx = import "L10.Game.CYingLingActiveSkill_TemplateIdx"
local Skill_YingLingSkill = import "L10.Game.Skill_YingLingSkill"
local UIntUIntKeyValuePair = import "L10.Game.UIntUIntKeyValuePair"
local EnumClass = import "L10.Game.EnumClass"
local CSkillMgr = import "L10.Game.CSkillMgr"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CClientFakePlayer = import "L10.Game.CClientFakePlayer"
local CGameVideoPlayer = import "L10.Game.CGameVideoPlayer"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CPlayerPropertyFight = import "L10.Game.CPlayerPropertyFight"
local NameColor = import "L10.Game.NameColor"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local MsgPackImpl = import "MsgPackImpl"
local CAntiProfessionSkillObject = import "L10.Game.CAntiProfessionSkillObject"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local NGUIText = import "NGUIText"
local VARBINARY = import "L10.Game.Properties.VARBINARY"

function LuaSkilSelectFunctionHolder.Firework2019SelectTarget(mainplayer, objectList)
	if CScene.MainScene.SceneTemplateId ~= NewYear2019.GetData().SceneTempId then return nil end
	local bossNianTempId = NewYear2019.GetData().BossNianTempId

	for i = 0, objectList.Count-1 do
		local obj = objectList[i]
		if TypeIs(obj, typeof(CClientMonster)) and obj.TemplateId == bossNianTempId then
			return obj
		end
	end
	
	g_MessageMgr:DisplayMessage("FIREWORK_SKILL_NO_TARGET")
	return nil
end

--蝶客正面附身
local function PositivePossessCheckSameTeamOrTeamGroup(player, target, sceneTid, bNotShowMsg)
	if CommonDefs.HashSetContains(Skill_Setting.GetData().PossessNoCheckTeam, typeof(UInt32), CScene.MainScene.SceneTemplateId) then
		return true
	end

	--在团队则不判断同队和同团，因为团队成员只有在打开团队界面才同步，客户端的团队信息并不准确，依赖服务端判断
	if CTeamGroupMgr.Instance:InTeamGroup() then
		return true
	end

	if not TypeIs(target, typeof(CClientOtherPlayer)) then
		return true
	end

	if TypeIs(target, typeof(CClientOtherPlayer)) and not player:IsInSameTeam(target) then
		if not bNotShowMsg then
			g_MessageMgr:ShowMessage("DieKe_FuShenFriend_Fail_NotFriend")
		end
		return false, "DieKe_FuShenFriend_Fail_NotFriend"
	end

	return true
end

local NameColorToGroup = {}
NameColorToGroup[NameColor.Blue] = 1
NameColorToGroup[NameColor.Yellow] = 1
NameColorToGroup[NameColor.LightGreen] = 1
NameColorToGroup[NameColor.HeavyGreen] = 1

NameColorToGroup[NameColor.Gray] = 2
NameColorToGroup[NameColor.Red] = 2

local function PositivePossessCheckNameColor(player, target, sceneTid, bNotShowMsg)
	if not PublicMap_PublicMap.GetData(sceneTid).EnablePK then
		return true
	end

	if not TypeIs(target, typeof(CClientOtherPlayer)) then
		return true
	end

	local playerNameColor = player.FightProp:GetPkNameColor()
	local targetNameColor = CPlayerPropertyFight.GetPkNameColor(target.PkValue, target.PkProactive)

	if NameColorToGroup[playerNameColor] == NameColorToGroup[targetNameColor] then
		return true
	end

	local msg
	if NameColorToGroup[playerNameColor] == 1 then
		msg = "DieKe_FuShen_Fail_Target_GreenName"
	else
		msg = "DieKe_FuShen_Fail_Target_RedName"
	end

	if not bNotShowMsg then
		g_MessageMgr:ShowMessage(msg)
	end

	return false, msg
end

--调整需要参照服务端：DieKeSkillMgr.lua CheckPositivePossess
local function CheckPositivePossess(player, target, bNotShowMsg)
	if player == target then
		return false
	end

	if not target then
		if not bNotShowMsg then
			g_MessageMgr:ShowMessage("CAST_SKILL_NEED_TARGET")
		end
		return false, "CAST_SKILL_NEED_TARGET"
	end

	if CSkillMgr.Inst:CheckPlayerIsDieKe(target) then
		if not bNotShowMsg then
			g_MessageMgr:ShowMessage("DieKe_FuShen_Fail_TargetIsDieKe")
		end
		return false, "DieKe_FuShen_Fail_TargetIsDieKe"
	end

	--只对OtherPlayer检查
	if not TypeIs(target, typeof(CClientOtherPlayer)) then
		return true
	end

	local sceneTid = CScene.MainScene.SceneTemplateId

	local ret, msg = PositivePossessCheckSameTeamOrTeamGroup(player, target, sceneTid, bNotShowMsg)
	if not ret then
		return false, msg
	end

	local ret, msg = PositivePossessCheckNameColor(player, target, sceneTid, bNotShowMsg)
	if not ret then
		return false, msg
	end

	return true
end

function LuaSkilSelectFunctionHolder.PositivePossessSelectTarget(mainPlayer, objectList)
	local template = Skill_AllSkills.GetData(91200501)
	local Target = mainPlayer.Target

	local ret, msg
	if Target and mainPlayer:CheckTargetForSingleSkill(template, Target) then
		ret, msg = CheckPositivePossess(mainPlayer, Target, true)
		if ret then
			return Target
		end
	end

	local target = nil
	local targetDist = nil
	
	--优先找附近的队友
	if CTeamMgr.Inst:IsPlayerInTeam(mainPlayer.Id) then
		local teamMembers = CTeamMgr.Inst:GetTeamMembersExceptMe()
		for i = 0, teamMembers.Count - 1 do
			local teamMember = teamMembers[i]
			local obj = CClientPlayerMgr.Inst:GetPlayer(teamMember.m_MemberId)
			if obj and mainPlayer:CheckTargetForSingleSkill(template, obj) and CheckPositivePossess(mainPlayer, obj, true) then
				local dist2 = (obj.Pos.x - mainPlayer.Pos.x) * (obj.Pos.x - mainPlayer.Pos.x) + (obj.Pos.y - mainPlayer.Pos.y) * (obj.Pos.y - mainPlayer.Pos.y)
				if not target or dist2 < targetDist then
					target = obj
					targetDist = dist2
				end
			end
		end
	end

	if not target then
		for i = 0, objectList.Count - 1 do
			local obj = objectList[i]
			if mainPlayer:CheckTargetForSingleSkill(template, obj) and CheckPositivePossess(mainPlayer, obj, true) then
				local dist2 = (obj.Pos.x - mainPlayer.Pos.x) * (obj.Pos.x - mainPlayer.Pos.x) + (obj.Pos.y - mainPlayer.Pos.y) * (obj.Pos.y - mainPlayer.Pos.y)
				if not target or dist2 < targetDist then
					target = obj
					targetDist = dist2
				end
			end
		end
	end

	--把提示消息延后到最后来发，如果当前目标不合法且有提示消息，而且找不到其他合法目标，则提示当前目标不合法的相关消息
	if not target and msg then
		g_MessageMgr:ShowMessage(msg)
	end

	return target
end


local LuaSkillSelectTargetFunction = 
{
	[922458] = LuaSkilSelectFunctionHolder.Firework2019SelectTarget,
	[922459] = LuaSkilSelectFunctionHolder.Firework2019SelectTarget,
	[912005] = LuaSkilSelectFunctionHolder.PositivePossessSelectTarget,
}

local skillFindTargetStrategy = import "L10.Game.CSpecialSkillFindTargetStrategy"

for skillCls, func in pairs(LuaSkillSelectTargetFunction) do
	skillFindTargetStrategy.SetLuaSkillTargetSelectFunction(skillCls, func)
end

LuaSkillSpecialConditionHolder = {}
local Skill_SceneLimit = import "L10.Game.Skill_SceneLimit"
local Skill_ExtraConsume = import "L10.Game.Skill_ExtraConsume"
local DelegateFactory = import "DelegateFactory"
local specialSkillCondition = import "L10.Game.CSpecialSkillCondition"
local CommonDefs = import "L10.Game.CommonDefs"
local Main = import "L10.Engine.Main"
local CItemMgr = import "L10.Game.CItemMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

function LuaSkillSpecialConditionHolder.CastSkillNeedInSceneCheck(skillId)
	local skillCls = math.floor(skillId / 100)
	if not Skill_SceneLimit.Exists(skillCls) then
		return true
	end

	local sceneLimit = Skill_SceneLimit.GetData(skillCls).SceneLimit
	for i = 0, sceneLimit.Length - 1 do
		if sceneLimit[i] == CScene.MainScene.SceneTemplateId then
			return true
		end
	end

	MessageMgr.Inst:ShowMessage("CAST_SKILL_NEED_IN_SPECIFIC_SCENE", {})
	return false
end

function LuaSkillSpecialConditionHolder.CastSkillExtraConsumeCheck(skillId)
	local skillCls = math.floor(skillId / 100)
	if not Skill_ExtraConsume.Exists(skillCls) then
		return true
	end

	local designData = Skill_ExtraConsume.GetData(skillCls)
	if designData.ItemCost then
		local itemCostTbl = {}
		for i = 0, designData.ItemCost.Length - 1, 2 do
			local itemTemplateId = designData.ItemCost[i]
			local itemCount = designData.ItemCost[i + 1]
			itemCostTbl[itemTemplateId] = (itemCostTbl[itemTemplateId] or 0) + itemCount
		end

		local canCost = false
		for itemTemplateId, itemCount in pairs(itemCostTbl) do
			if CItemMgr.Inst:GetItemCount(itemTemplateId) >= itemCount then
				canCost = true
			end
		end

		if not canCost then
			g_MessageMgr:ShowMessage(designData.MessageForNoItem)
			return false
		end
	end

	if designData.SilverCost > 0 then
		if CClientMainPlayer.Inst.ItemProp.Silver < designData.SilverCost then
			g_MessageMgr:ShowMessage("CAST_SKILL_NEED_CONSUME_SPECIFIC_SILVER")
			return false
		end
	end
	
	return true
end

function LuaSkillSpecialConditionHolder.CastSkillBuffCheck(skillId)
	local skillCls = math.floor(skillId / 100)
	if skillCls ~= 960032 then
		return true
	end

	if not CClientMainPlayer.Inst.Target then
		g_MessageMgr:ShowMessage("CAST_SKILL_NEED_TARGET")
		return false
	end
	
	if CommonDefs.DictContains(CClientMainPlayer.Inst.Target.BuffProp.Buffs, typeof(UInt32), 64311901) then
		g_MessageMgr:ShowMessage("CAST_SKILL_BUFF_FORBID")
		return
	end

	return true
end

--调整需要参照服务端：DieKeSkillMgr.lua CheckPositivePossess
function LuaSkillSpecialConditionHolder.CastPositivePossessCheck(skillId)
	local skillCls = math.floor(skillId / 100)
	if skillCls ~= 912005 then
		return true
	end

	local mainPlayer = CClientMainPlayer.Inst
	local target = mainPlayer.Target

	if not CheckPositivePossess(mainPlayer, target) then
		return false
	end

	return true
end

-- 寄·初心
--调整需要参照服务端：SpecialSkillConditions.lua SkillCondition[912019]
function LuaSkillSpecialConditionHolder.CastDieKe125Check(skillId)
	local skillCls = math.floor(skillId / 100)
	if skillCls ~= 912019 then
		return true
	end

	local mainPlayer = CClientMainPlayer.Inst
	local skillProp = mainPlayer.SkillProp

	local skillId
	skillId = skillProp:GetSkillIdWithDeltaByCls(912005, mainPlayer.Level)
	if skillId and skillId > 0 and not mainPlayer:CheckSkillCD(skillId) then
		return true
	end

	skillId = skillProp:GetSkillIdWithDeltaByCls(912007, mainPlayer.Level)
	if skillId and skillId > 0 and not mainPlayer:CheckSkillCD(skillId) then
		return true
	end

	g_MessageMgr:DisplayMessage("DIEKE_125_SKILL_FAILED_ALL_COOLDOWN")

	return false
end

local function luaSkillSpecialConditionInit()
	local LuaSkillSpecialConditionFunc = {}
	-- 增加技能的释放场景检测
	Skill_SceneLimit.ForeachKey(DelegateFactory.Action_object(function(key)
		 LuaSkillSpecialConditionFunc[key] = LuaSkillSpecialConditionHolder.CastSkillNeedInSceneCheck
	end))
	-- 增加技能的物品消耗检测
	Skill_ExtraConsume.ForeachKey(DelegateFactory.Action_object(function(key)
		 LuaSkillSpecialConditionFunc[key] = LuaSkillSpecialConditionHolder.CastSkillExtraConsumeCheck
	end))

	-- 特殊处理 保胎技能 960032
	LuaSkillSpecialConditionFunc[960032] = LuaSkillSpecialConditionHolder.CastSkillBuffCheck

	--不坏金身(他山襄助技能)
	LuaSkillSpecialConditionFunc[922583] = LuaSkillSpecialConditionHolder.CastBuHuaiJinShenCheck

	--蝶客正面附身
	LuaSkillSpecialConditionFunc[912005] = LuaSkillSpecialConditionHolder.CastPositivePossessCheck

	--蝶客125技能
	LuaSkillSpecialConditionFunc[912019] = LuaSkillSpecialConditionHolder.CastDieKe125Check

	for skillCls, func in pairs(LuaSkillSpecialConditionFunc) do
		specialSkillCondition.SetLuaSkillSpecialCondition(skillCls, func)
	end
end

if CommonDefs.IsIOSPlatform() then
	if Main.Inst.EngineVersion < 0 or Main.Inst.EngineVersion > 370344 then
		specialSkillCondition.m_LuaSkillSpecialConditionInit = luaSkillSpecialConditionInit
	end
else
	specialSkillCondition.m_LuaSkillSpecialConditionInit = luaSkillSpecialConditionInit
end

----------------------------------------------------
LuaSkillMgr = {}
function LuaSkillMgr.GetDefaultYingLingState()
	local mainPlayer = CClientMainPlayer.Inst
	if mainPlayer == nil then
		return EnumToInt(EnumYingLingState.eDefault)
	end
	return EnumToInt(mainPlayer.CurYingLingState)
end

function LuaSkillMgr.IsOpenNewSkillSuggestion()
	return true
end

function LuaSkillMgr.PreSkillConditionIsFit(nextSkill)
	if CClientMainPlayer.Inst == nil or nextSkill == nil then
		return false
	end
	--检查前置技能
	local pairs = nextSkill.AdjustedPreSkills
	if pairs == nil or pairs.Length == 0 then
		return true
	end
	local skillProp = CClientMainPlayer.Inst.SkillProp
	local result = true
	do
		local i = 0
		while i < pairs.Length do
			local skillId = CLuaDesignMgr.Skill_AllSkills_GetSkillId(pairs[i][0], pairs[i][1])
			if not skillProp:IsOriginalSkillClsExist(pairs[i][0]) then
				result = false
				break
			else
				if CLuaDesignMgr.Skill_AllSkills_GetLevel(skillProp:GetOriginalSkillIdByCls(pairs[i][0])) < pairs[i][1] then
					result = false
					break
				end
			end
			i = i + 1
		end
	end
	return result
end

function LuaSkillMgr.PreLevelConditionIsFit(nextSkill)
	if CClientMainPlayer.Inst == nil or nextSkill == nil then
		return false
	end
	--检查玩家等级
	local needPlayerLevel = nextSkill.PlayerLevel
	if CClientMainPlayer.Inst.MaxLevel < needPlayerLevel then
		return false
	end
	return true
end

--影灵请求变身
function LuaSkillMgr.RequestYingLingTransform(yingLingState)
	Gac2Gas.RequestYingLingTransform(yingLingState or 0)
end

LuaSkillMgr.YingLingUnUseSkillsCache = nil
LuaSkillMgr.ShareSkillsCache = nil
function LuaSkillMgr.GetYingLingSkillsCache()
	if LuaSkillMgr.YingLingUnUseSkillsCache == nil or LuaSkillMgr.ShareSkillsCache == nil then
		LuaSkillMgr.YingLingUnUseSkillsCache = {}
		LuaSkillMgr.ShareSkillsCache = {}
		for i = 1, 3 do
			local cache = {}
			local YingLingSkill = Skill_YingLingSkill.GetData(i)
			for i = 0, YingLingSkill.Skills.Length - 1 do
				cache[YingLingSkill.Skills[i]] = true
			end
			table.insert(LuaSkillMgr.YingLingUnUseSkillsCache,i, cache)
			local shareSkills = YingLingSkill.ShareSkills
			for j = 0, shareSkills.Length - 1 do
				LuaSkillMgr.ShareSkillsCache[shareSkills[j]] = {state = i,index = j}
			end
		end
	end
	return LuaSkillMgr.YingLingUnUseSkillsCache, LuaSkillMgr.ShareSkillsCache
end

--筛选出符合影灵形态的技能
function LuaSkillMgr.GetYingLingNewProfessionSkillIds(professionSkillIds, state)
	local yingLingUnUseSkillsCache = nil
	local yingLingShareSkillsCache = nil
	yingLingUnUseSkillsCache, yingLingShareSkillsCache = LuaSkillMgr.GetYingLingSkillsCache()
	if yingLingUnUseSkillsCache == nil or yingLingShareSkillsCache == nil then
		return professionSkillIds
	end
	local tempList = {}

	--[[tempSpecialSkillList强制将形态技能抽出来，指定一个顺序
	这是为影灵在切换编辑的形态技能时用以规范SkillPreferenceTopView.itemCells的顺序，防止选中框错位
]]
	local tempSpecialSkillList = {}

	for i = 0,professionSkillIds.Count - 1 do
		local data = Skill_AllSkills.GetData(professionSkillIds[i].Key)
		local shareSkillInfo = yingLingShareSkillsCache[data.SkillClass]
		if not shareSkillInfo then
			table.insert(tempList,professionSkillIds[i])
		else
			if shareSkillInfo.state == state then
				table.insert(tempSpecialSkillList, professionSkillIds[i])
			end
		end
	end
	for i = 1, #tempSpecialSkillList do
		table.insert(tempList, i, tempSpecialSkillList[i])
	end

	return Table2List(tempList,MakeGenericClass(List,UIntUIntKeyValuePair))
end

--检查影灵是否掌握变身技能
function LuaSkillMgr.CheckMainPlayerYingLingBianShenSkill()
	local mainPlayer = CClientMainPlayer.Inst
	if mainPlayer and mainPlayer.Class == EnumClass.YingLing and mainPlayer.SkillProp:IsOriginalSkillClsExist(Skill_Setting.GetData().YingLingBianShenSkillId) then
		return true
	end
	return false
end

--检查影灵技能是否能在当前形态使用
--skillCls 影灵的技能
--state 影灵当前形态
function LuaSkillMgr.IsYingLingSkillCanUse(SkillClass, curstate)
	local res = false
	local yingLingUnUseSkillsCache = nil
	local yingLingShareSkillsCache = nil
	yingLingUnUseSkillsCache, yingLingShareSkillsCache = LuaSkillMgr.GetYingLingSkillsCache()
	local mainPlayer = CClientMainPlayer.Inst
	if mainPlayer and mainPlayer.IsYingLing and yingLingUnUseSkillsCache then
		for i = 1,3 do
			if i ~= curstate then
				if yingLingUnUseSkillsCache[i][SkillClass] then
					res = true
				end
			end
		end
	end
	return res
end

--返回指定形态的技能
--skillCls 影灵任一形态的技能
--state 影灵当前形态
function LuaSkillMgr.GetYingLingRealSkillCls(skillCls,state)
	local _,shareSkillsCache = LuaSkillMgr.GetYingLingSkillsCache()
	local cacheData = shareSkillsCache[skillCls]
	if cacheData and cacheData.state ~=  state then
		for k, v in pairs(shareSkillsCache) do
			if v.index == cacheData.index and v.state == state then
				return k
			end
		end
	end
	return nil
end
----------------------------------------------------
-- BEGIN : 技能图标临时替换功能，用于主界面上的技能释放面板
----------------------------------------------------

LuaSkillMgr.m_OriginSkillCls2ReplaceSkillClsTbl = nil
LuaSkillMgr.m_CheckReplaceSkillIconTick = nil
LuaSkillMgr.m_LoginListenerRegistered = false

function LuaSkillMgr:ReplaceSkillIcon(oriSkillCls, replaceSkillCls, endTime)
	if not self.m_OriginSkillCls2ReplaceSkillClsTbl then
		self.m_OriginSkillCls2ReplaceSkillClsTbl = {}
	end
	self.m_OriginSkillCls2ReplaceSkillClsTbl[oriSkillCls] = {replaceSkillCls = replaceSkillCls, endTime = endTime}
	g_ScriptEvent:BroadcastInLua("OnTempReplaceSkillIconInfoUpdate", oriSkillCls)

	--注册login事件
	if not self.m_LoginListenerRegistered then
		self.m_LoginListenerRegistered = true
		g_ScriptEvent:RemoveListener("PlayerLogin", self, "OnPlayerLogin")
		g_ScriptEvent:AddListener("PlayerLogin", self, "OnPlayerLogin")
	end

	--起一个tick用来检查过期时间
	if not self.m_CheckReplaceSkillIconTick then
		self.m_CheckReplaceSkillIconTick = RegisterTick(function()
			if not self.m_OriginSkillCls2ReplaceSkillClsTbl then return end
			local tbl = {}
			for cls,data in pairs(self.m_OriginSkillCls2ReplaceSkillClsTbl) do
				if data.endTime <= CServerTimeMgr.Inst.timeStamp then
					table.insert(tbl, cls)
				end
			end
			local changed = false
			local changedCls = nil
			for __,cls in pairs(tbl) do
				self.m_OriginSkillCls2ReplaceSkillClsTbl[cls] = nil
				changed = true
				changedCls = cls
			end
			if changed then
				g_ScriptEvent:BroadcastInLua("OnTempReplaceSkillIconInfoUpdate", changedCls)
			end
		end, 1000)
	end 
end

function LuaSkillMgr:ResetSkillIcon(oriSkillCls)
	if self.m_OriginSkillCls2ReplaceSkillClsTbl and self.m_OriginSkillCls2ReplaceSkillClsTbl[oriSkillCls] then
		self.m_OriginSkillCls2ReplaceSkillClsTbl[oriSkillCls] = nil
		g_ScriptEvent:BroadcastInLua("OnTempReplaceSkillIconInfoUpdate", oriSkillCls)
	end
end

--玩家登录游戏时重置临时图标替换信息
function LuaSkillMgr:OnPlayerLogin()
	self.m_OriginSkillCls2ReplaceSkillClsTbl = nil
end

function LuaSkillMgr:GetOriginOrReplaceSkillIcon(skilldata)
	local oriSkillCls = Skill_AllSkills.GetClass(skilldata.ID)
	if self.m_OriginSkillCls2ReplaceSkillClsTbl and self.m_OriginSkillCls2ReplaceSkillClsTbl[oriSkillCls] then
		if self.m_OriginSkillCls2ReplaceSkillClsTbl[oriSkillCls].endTime > CServerTimeMgr.Inst.timeStamp then
			local replaceSkillCls = self.m_OriginSkillCls2ReplaceSkillClsTbl[oriSkillCls].replaceSkillCls
			return Skill_AllSkills.GetData(Skill_AllSkills.GetSkillId(replaceSkillCls,1)).SkillIcon
		else
			self.m_OriginSkillCls2ReplaceSkillClsTbl[oriSkillCls] = nil
		end
	end

	return skilldata.SkillIcon
end

function LuaSkillMgr:DieKeDoPossessOther(engineId, targetEngineId, expiredTime)
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
	if obj == nil then return end
	local co = (CommonDefs.TypeIs(obj, typeof(CClientMainPlayer)) or CommonDefs.TypeIs(obj, typeof(CGameVideoPlayer)) or CommonDefs.TypeIs(obj, typeof(CClientOtherPlayer)) or CommonDefs.TypeIs(obj, typeof(CClientFakePlayer))) and obj or nil
	if co then
		if CClientMainPlayer.Inst and engineId == CClientMainPlayer.Inst.EngineId then
			CClientMainPlayer.Inst.DieKeEndPossessingExpiredTime = expiredTime
			CClientMainPlayer.Inst.DieKePossessingTotalTime = expiredTime - CServerTimeMgr.Inst.timeStamp
		end
		local needBroadcastMsg = co.AppearanceProp.PossessTarget ~= targetEngineId
		co.AppearanceProp.PossessTarget = targetEngineId
		if needBroadcastMsg then
			EventManager.BroadcastInternalForLua(EnumEventType.DieKeDoPossessOther, { engineId, targetEngineId})
		end
	end
	obj.Visible = false
end

----------------------------------------------------
-- END : 技能图标临时替换功能，用于主界面上的技能释放面板
----------------------------------------------------

Skill_AllSkills.m_hookGetAdjustedmoney = function (this)
    if CSkillMgr.Inst:IsColorSystemSkill(this.SkillClass) then
        if Skill_ColorSkill.Exists(this.SkillClass) then
            local cls = Skill_ColorSkill.GetData(this.SkillClass).BaseSkill_ID
            local data = Skill_AllSkills.GetData(Skill_AllSkills.GetSkillId(cls, this.Level))
            if data ~= nil then
                return data.Money
            end
        end
    end

    -- 影灵破云击特殊处理
	local money= nil
	local cls = LuaSkillMgr.GetYingLingRealSkillCls(this.SkillClass,EnumYingLingState.eMale)
	if cls then
		local data = Skill_AllSkills.GetData(Skill_AllSkills.GetSkillId(cls, this.Level))
		if data ~= nil then
			money =  data.Money
		end
	end

	if money then
		return money
	end
    return this.Money
end

Skill_AllSkills.m_hookGetAdjustedexperience = function (this)
    if CSkillMgr.Inst:IsColorSystemSkill(this.SkillClass) then
        if Skill_ColorSkill.Exists(this.SkillClass) then
            local cls = Skill_ColorSkill.GetData(this.SkillClass).BaseSkill_ID
            local data = Skill_AllSkills.GetData(Skill_AllSkills.GetSkillId(cls, this.Level))
            if data ~= nil then
                return data.Experience
            end
        end
    end

    -- 影灵破云击特殊处理
	local exp = nil
	local cls = LuaSkillMgr.GetYingLingRealSkillCls(this.SkillClass,EnumYingLingState.eMale)
	if cls then
		local data = Skill_AllSkills.GetData(Skill_AllSkills.GetSkillId(cls, this.Level))
		if data ~= nil then
			exp = data.Experience
		end
	end

	if exp then
		return exp
	end
    return this.Experience
end
----------------------------------------------------
function Gas2Gac.UpdateExtraYingLingActiveSkills(templateIdx,ud)
	if not CClientMainPlayer.Inst then
		return
	end
	local idx = CreateFromClass(CYingLingActiveSkill_TemplateIdx)
	idx:LoadFromString(ud)
	CommonDefs.DictSet_LuaCall(CClientMainPlayer.Inst.SkillProp.ExtraYingLingActiveSkills,templateIdx,idx)
	g_ScriptEvent:BroadcastInLua("SkillPreferenceViewLoadYingLingSkillIcons",{})
end

function Gas2Gac.DieKeDoPossessOther(engineId, targetEngineId, expiredTime)
	LuaSkillMgr:DieKeDoPossessOther(engineId, targetEngineId, expiredTime)
end

----------------------------------------------------

function Gas2Gac.PassiveSkillSetLevel(SkillType, Index, Level)
	if not CClientMainPlayer.Inst then return end
	CClientMainPlayer.Inst.SkillProp.HpSkillLevel[Index] = Level
	g_ScriptEvent:BroadcastInLua("UpdatePassiveSkillSetLevel", SkillType, Index, Level)
end

----------------------------------------------------
--灵核相关内容

function LuaSkillMgr:SoulCoreUpgrade()
	if not CClientMainPlayer.Inst then return end
	Gac2Gas.SoulCoreUpgrade()
end

function LuaSkillMgr:SoulCoreIsUnlock()
	if not CClientMainPlayer.Inst then return end
	return CClientMainPlayer.Inst.SkillProp.SoulCore.Level > 0
end

function Gas2Gac.SoulCoreSetLevel(lv)
	if not CClientMainPlayer.Inst then return end
	CClientMainPlayer.Inst.SkillProp.SoulCore.Level = lv
	g_ScriptEvent:BroadcastInLua("UpdateMainPlayerSoulCoreLevel")
end

function Gas2Gac.SoulCoreSetMana(mana)
	if not CClientMainPlayer.Inst then return end
	CClientMainPlayer.Inst.SkillProp.SoulCore.Mana = mana
	g_ScriptEvent:BroadcastInLua("UpdateMainPlayerSoulCoreMana")
end

function Gas2Gac.SoulCoreUpdateDisplay(DisplayCoreId, DisplayFxId, DisplayPalletId, DisplayCoreParticles, DisplayCoreEffect, reason)
	if not CClientMainPlayer.Inst then return end

	CClientMainPlayer.Inst.SkillProp.SoulCore.DisplayCoreId = DisplayCoreId
	CClientMainPlayer.Inst.SkillProp.SoulCore.DisplayFxId = DisplayFxId
	CClientMainPlayer.Inst.SkillProp.SoulCore.DisplayPalletId = DisplayPalletId
	-- CClientMainPlayer.Inst.SkillProp.SoulCore.DisplayRandomFactor = DisplayRandomFactor
	CClientMainPlayer.Inst.SkillProp.SoulCore.DisplayCoreParticles = DisplayCoreParticles
	CClientMainPlayer.Inst.SkillProp.SoulCore.DisplayCoreEffect = DisplayCoreEffect

	LuaZongMenMgr:SoulCoreUpdateDisplay(reason)
end

----------------------------------------------------
--职业克符相关内容

function LuaSkillMgr:UnpakExtraAntiProfessionItemStr(str)
	local tbl = {}
	for idx, level in string.gmatch(str, "(%d+),(%d+);") do
		idx = tonumber(idx)
		level = tonumber(level)

		if idx > 0 and idx <= (EnumToInt(EnumClass.Undefined) - 1) and level > 0 and level <= 30 then
			table.insert(tbl, { idx, level })
		else
			return {}
		end
	end
	return tbl
end

function Gas2Gac.AntiProfession_UpgradeSkillResult(bSuccess, Index, Level, FailTimes)
	local skill = CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill[Index]
	skill.CurLevel = Level
	skill.UpgradeFailTimes = FailTimes

	g_ScriptEvent:BroadcastInLua("AntiProfessionUpgradeSkillResult", bSuccess, Index, Level, FailTimes)
end


function Gas2Gac.AntiProfession_UnlockSlotResult(bSuccess, Index, Profession, MaxLevel, FailTimes)
	CClientMainPlayer.Inst.SkillProp.AntiProfessionSlot.CurSlotNum = Index
	CClientMainPlayer.Inst.SkillProp.AntiProfessionSlot.FailTimes = FailTimes

	if bSuccess then
		local newSkill = CreateFromClass(CAntiProfessionSkillObject)
		newSkill.CurLevel = 1
		newSkill.MaxLevel = MaxLevel
		newSkill.Profession = Profession
		newSkill.UpgradeFailTimes = 0
		CommonDefs.DictSet_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill, Index, newSkill)
	end
	
	g_ScriptEvent:BroadcastInLua("AntiProfessionUnlockSlotResult", bSuccess, Index, Profession, MaxLevel, FailTimes)
end

function Gas2Gac.AntiProfession_BreakLimitResult(bSuccess, Index, MaxLevel)
	local skill = CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill[Index]
	skill.MaxLevel = MaxLevel

	g_ScriptEvent:BroadcastInLua("AntiProfessionBreakLimitResult", bSuccess, Index, MaxLevel)
end

function Gas2Gac.AntiProfession_SetAntiProfessionMul(expiredTime, extraStr)
	CClientMainPlayer.Inst.SkillProp.AntiProfessionMulExpiredTime = expiredTime
	local table = LuaSkillMgr:UnpakExtraAntiProfessionItemStr(extraStr)

	CClientMainPlayer.Inst.SkillProp.AntiProfessionMul:Clear()
	for k, v in pairs(table) do
		local profession, level = v[1], v[2]
		CommonDefs.DictSet_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionMul, profession, level)
	end

	g_ScriptEvent:BroadcastInLua("AntiProfessionSetAntiProfessionMul", expiredTime, extraStr)
end

function Gas2Gac.AntiProfession_UpdateSkill(Index, skillObject_U)
	local skill = CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill[Index]
	skill:LoadFromString(skillObject_U)

	g_ScriptEvent:BroadcastInLua("AntiProfessionUpdateSkill", Index)
end

function Gas2Gac.AntiProfession_UpdateScore(Profession, score)
	local oldScore = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionScore, Profession) or 0
	CommonDefs.DictSet_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionScore, Profession, score)

	local change = score - oldScore
	if change > 0 then
		local itemId = SoulCore_Settings.GetData().AntiProfessionItemId[Profession - 1][1]
		local itemData = Item_Item.GetData(itemId)
		g_MessageMgr:ShowMessage("OBTAIN_ANTIPROFESSION", change, NGUIText.EncodeColor(GameSetting_Common_Wapper.Inst:GetColor(itemData.NameColor)), itemData.Name, itemId, score)
	end
	
	g_ScriptEvent:BroadcastInLua("AntiProfessionUpdateScore", Profession, score)
end

function Gas2Gac.AntiProfession_QueryTransferTimeResult(time)
	g_ScriptEvent:BroadcastInLua("AntiProfessionQueryTransferTimeResult", time)
end

function Gas2Gac.AntiProfession_RandomChangeScore(data_U)
	local data = MsgPackImpl.unpack(data_U)
	if not data or data.Count==0 then return end

	local infos = {}
	for i=0,data.Count-1,2 do
		local key = data[i]
		local value = data[i+1]
		infos[key] = value
	end
	LuaZongMenMgr:AntiProfession_RandomChangeScore(infos)
end

function Gas2Gac.AntiProfession_UpdatePersistTransfer(key, strValue)
	if key == EnumPersistPlayDataKey_lua.eTransferAntiProfession then
		if not CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eTransferAntiProfession) then
			CClientMainPlayer.Inst.PlayProp.PersistPlayData[EnumPersistPlayDataKey_lua.eTransferAntiProfession]	= VARBINARY()
		end
		local data = MsgPackImpl.pack(strValue)
		local bufLen = data.Length
		local startIndex = 0
		local default
		default, bufLen, startIndex = CClientMainPlayer.Inst.PlayProp.PersistPlayData[EnumPersistPlayDataKey_lua.eTransferAntiProfession]:LoadFromBytes(data, bufLen, bufLen, startIndex)
		g_ScriptEvent:BroadcastInLua("TransferAntiProfession")
	end
end

----------------------------------------------------
