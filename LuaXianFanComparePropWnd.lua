require("common/common_include")
require("ui/feisheng/LuaXianFanCompareSimplePropView")
require("ui/feisheng/LuaXianFanCompareDetailPropView")
local LuaGameObject=import "LuaGameObject"
local Gac2Gas=import "L10.Game.Gac2Gas"
local LuaUtils=import "LuaUtils"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local NGUITools=import "NGUITools"
--仙凡属性对比界面
CLuaXianFanComparePropWnd=class()
RegistClassMember(CLuaXianFanComparePropWnd,"m_SimplePropTemplate")

RegistClassMember(CLuaXianFanComparePropWnd,"m_SimpleProp1")
RegistClassMember(CLuaXianFanComparePropWnd,"m_SimpleProp2")

RegistClassMember(CLuaXianFanComparePropWnd,"m_DetailProp1")
RegistClassMember(CLuaXianFanComparePropWnd,"m_DetailProp2")



function CLuaXianFanComparePropWnd:Init()
    LuaGameObject.GetChildNoGC(self.transform,"Anchor").gameObject:SetActive(false)
    Gac2Gas.RequestXianFanPropertyCompare()
end

function CLuaXianFanComparePropWnd:OnEnable()
    g_ScriptEvent:AddListener("SendXianFanPropertyCompareResult", self, "OnSendXianFanPropertyCompareResult")
end

function CLuaXianFanComparePropWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendXianFanPropertyCompareResult", self, "OnSendXianFanPropertyCompareResult")
end
function CLuaXianFanComparePropWnd:OnSendXianFanPropertyCompareResult(args)
    local fightProp=args[0]
    if CClientMainPlayer.Inst==nil then return end
    local myFightProp=CClientMainPlayer.Inst.FightProp
    LuaGameObject.GetChildNoGC(self.transform,"Anchor").gameObject:SetActive(true)
    self.m_SimpleProp1=CLuaXianFanCompareSimplePropView:new()
    self.m_SimpleProp2=CLuaXianFanCompareSimplePropView:new()

    self.m_DetailProp1=CLuaXianFanCompareDetailPropView:new()
    self.m_DetailProp2=CLuaXianFanCompareDetailPropView:new()

    local tf1=LuaGameObject.GetChildNoGC(self.transform,"BasicPropertyFrame").transform
    LuaUtils.SetLocalPositionX(tf1.gameObject.transform,-400)
    local go=NGUITools.AddChild(tf1.parent.gameObject,tf1.gameObject)
    LuaUtils.SetLocalPositionX(go.transform,400)

    local tf2=LuaGameObject.GetChildNoGC(self.transform,"PlayerDetailPropertyView").transform
    LuaUtils.SetLocalPositionX(tf2.gameObject.transform,-400)
    local go2=NGUITools.AddChild(tf2.parent.gameObject,tf2.gameObject)
    LuaUtils.SetLocalPositionX(go2.transform,400)
    
    self.m_SimpleProp1:Init(tf1,myFightProp,true)
    self.m_SimpleProp2:Init(go.transform,fightProp,false)

    self.m_DetailProp1:Init(tf2,myFightProp,true)
    self.m_DetailProp2:Init(go2.transform,fightProp,false)
    
    
    LuaGameObject.GetChildNoGC(self.transform,"Table").table:Reposition()
    
end
return CLuaXianFanComparePropWnd
