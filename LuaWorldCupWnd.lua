require("common/common_include")

local UISprite = import "UISprite"
local QnTabView = import "L10.UI.QnTabView"

require("ui/worldcup/LuaWorldCupJingCaiView")
require("ui/worldcup/LuaWorldCupTouZhuRecordView")
require("ui/worldcup/LuaWorldCupHomeTeamView")

CLuaWorldCupWnd = class()
RegistClassMember(CLuaWorldCupWnd, "m_JingCaiView")
RegistClassMember(CLuaWorldCupWnd, "m_TouZhuRecordView")
RegistClassMember(CLuaWorldCupWnd, "m_HomeTeamView")
RegistClassMember(CLuaWorldCupWnd, "m_TabView")

RegistClassMember(CLuaWorldCupWnd, "m_JiangCaiAwardReddot")
RegistClassMember(CLuaWorldCupWnd, "m_HomeTeamAwardReddot")


function CLuaWorldCupWnd:Init()
    self.m_JingCaiView = CLuaWorldCupJingCaiView:new()
    self.m_JingCaiView:Init(FindChild(self.transform, "JingCaiView"))

    self.m_TouZhuRecordView = CLuaWorldCupTouZhuRecordView:new()
    self.m_TouZhuRecordView:Init(FindChild(self.transform, "TouZhuRecordView"))

    self.m_HomeTeamView = CLuaWorldCupHomeTeamView:new()
    self.m_HomeTeamView:Init(FindChild(self.transform, "HomeTeamView"))

    self.m_TabView = FindChild(self.transform, "Tabs"):GetComponent(typeof(QnTabView))

    self.m_TabView.OnSelect = DelegateFactory.Action_QnTabButton_int(function (p1,p2)
        if p2==0 then
            self.m_JingCaiView:OnSelected()
        elseif p2==1 then
            self.m_TouZhuRecordView:OnSelected()
        elseif p2==2 then
            self.m_HomeTeamView:OnSelected()
        end
    end)

    -- 小红点默认关闭
    self.m_JiangCaiAwardReddot = FindChild(self.transform, "JiangCaiAwardReddot"):GetComponent(typeof(UISprite))
    self.m_HomeTeamAwardReddot = FindChild(self.transform, "HomeTeamAwardReddot"):GetComponent(typeof(UISprite))
    self.m_JiangCaiAwardReddot.gameObject:SetActive(false)
    self.m_HomeTeamAwardReddot.gameObject:SetActive(false)

    self.m_TabView:ChangeTo(0)
end

function CLuaWorldCupWnd:OnEnable()
    g_ScriptEvent:AddListener("ShowWorldCupJingCaiAwardReddot", self, "ShowWorldCupJingCaiAwardReddot")
    g_ScriptEvent:AddListener("SendWorldCupTodayJingCaiInfo", self, "SendWorldCupTodayJingCaiInfo")
    g_ScriptEvent:AddListener("ReplyWorldCupTodayJingCaiRecord", self, "ReplyWorldCupTodayJingCaiRecord")
    g_ScriptEvent:AddListener("ReplyWorldCupHomeTeamPlayInfo", self, "ReplyWorldCupHomeTeamPlayInfo")
end

function CLuaWorldCupWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ShowWorldCupJingCaiAwardReddot", self, "ShowWorldCupJingCaiAwardReddot")
    g_ScriptEvent:RemoveListener("SendWorldCupTodayJingCaiInfo", self, "SendWorldCupTodayJingCaiInfo")
    g_ScriptEvent:RemoveListener("ReplyWorldCupTodayJingCaiRecord", self, "ReplyWorldCupTodayJingCaiRecord")
    g_ScriptEvent:RemoveListener("ReplyWorldCupHomeTeamPlayInfo", self, "ReplyWorldCupHomeTeamPlayInfo")
end

function CLuaWorldCupWnd:ShowWorldCupJingCaiAwardReddot()
    if self.m_JiangCaiAwardReddot then
        self.m_JiangCaiAwardReddot.gameObject:SetActive(true)
    end
end

function CLuaWorldCupWnd:SendWorldCupTodayJingCaiInfo(pid, totalNum, resultData, myValue, serverValue, myNum)
    if self.m_JingCaiView then
        self.m_JingCaiView:Refresh(pid, totalNum, resultData, myValue, serverValue, myNum)
    end
end

function CLuaWorldCupWnd:ReplyWorldCupTodayJingCaiRecord(data)
    if self.m_TouZhuRecordView then
        local bAlert = self.m_TouZhuRecordView:Refresh(data)
        if bAlert then
            self.m_JiangCaiAwardReddot.gameObject:SetActive(true)
        else
            self.m_JiangCaiAwardReddot.gameObject:SetActive(false)
        end
    end
end

function CLuaWorldCupWnd:ReplyWorldCupHomeTeamPlayInfo(tid, resultData, bAlert, groupRank, settimes, taotaiStage)
    if bAlert then
        self.m_HomeTeamAwardReddot.gameObject:SetActive(true)
    else
        self.m_HomeTeamAwardReddot.gameObject:SetActive(false)
    end

    if self.m_HomeTeamView then
        self.m_HomeTeamView:ReplyWorldCupHomeTeamPlayInfo(tid, resultData, bAlert, groupRank, settimes, taotaiStage)
    end
end

return CLuaWorldCupWnd
