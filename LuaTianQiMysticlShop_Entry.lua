require("common/common_include")
local Gac2Gas=import "L10.Game.Gac2Gas"
local TianQiMysticalShop_Setting = import "L10.Game.TianQiMysticalShop_Setting"
local DelegateFactory = import "DelegateFactory"
local MessageMgr = import "L10.Game.MessageMgr"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CommonDefs = import "L10.Game.CommonDefs"
local CChatLinkMgr = import "CChatLinkMgr"
local UIScrollView=import "UIScrollView"
local QnButton = import "L10.UI.QnButton"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local CItemMgr=import "L10.Game.CItemMgr"
local CUITexture=import "L10.UI.CUITexture"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"


CLuaTianQiMysticlShop_Entry=class()
RegistChildComponent(CLuaTianQiMysticlShop_Entry,"m_SettingButton","SettingButton", GameObject)
RegistClassMember(CLuaTianQiMysticlShop_Entry,"m_EnterButton")
RegistClassMember(CLuaTianQiMysticlShop_Entry, "m_MoneyCtrl")
RegistClassMember(CLuaTianQiMysticlShop_Entry,"m_TipButton")
RegistClassMember(CLuaTianQiMysticlShop_Entry,"m_ShopWndHead")


RegistClassMember(CLuaTianQiMysticlShop_Entry,"m_RowTemplate")
RegistClassMember(CLuaTianQiMysticlShop_Entry,"m_ItemTemplate")
RegistClassMember(CLuaTianQiMysticlShop_Entry,"m_Grid")
RegistClassMember(CLuaTianQiMysticlShop_Entry,"m_ScrollView")

function CLuaTianQiMysticlShop_Entry:Init()

	self.m_EnterButton = FindChild(self.transform,"EnterButton"):GetComponent(typeof(QnButton))
	self.m_MoneyCtrl = FindChild(self.transform,"QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
	self.m_TipButton = FindChild(self.transform,"TipButton"):GetComponent(typeof(QnButton))
	self.m_ShopWndHead = FindChild(self.transform,"ShopWndHead"):GetComponent(typeof(UILabel))
	UIEventListener.Get(self.m_SettingButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnSettingButtonClicked()
    end)
	self.m_TipButton.gameObject:SetActive(false)

	self.m_MoneyCtrl:SetType(EnumMoneyType.Score, EnumPlayScoreKey.TianQiJiFen, true)

	self.m_Grid = FindChild(self.transform,"ItemGrid"):GetComponent(typeof(UIGrid))

	self.m_RowTemplate = FindChild(self.transform,"RowTemplate").gameObject
	self.m_RowTemplate:SetActive(false)

	self.m_ScrollView = FindChild(self.transform,"ScrollView"):GetComponent(typeof(UIScrollView))

	local cost = TianQiMysticalShop_Setting.GetData().OpenShopCost[0]
	self.m_MoneyCtrl:SetCost(cost)

	--local headTxt = LuaGetGlobal("PreTianQiShopWnd_WndHeadTxt")
	
	self.m_ShopWndHead.text = CChatLinkMgr.TranslateToNGUIText(MessageMgr.Inst:FormatMessage("TIANQI_SHOP_HEAD", {}), false)


	self.m_EnterButton.OnClick = DelegateFactory.Action_QnButton(
		function(button)
			if self.m_MoneyCtrl.moneyEnough then
			 	Gac2Gas.OpenSecretShop(EnumSecretShopType.eTianQiShop)
			else
				MessageMgr.Inst:ShowMessage("TIANQI_JIFEN_NOT_ENOUGH",{})
			end
		end
	)

	self.m_TipButton.OnClick = DelegateFactory.Action_QnButton(
		function(button)
			MessageMgr.Inst:ShowMessage("TIANQI_SHOP_TIP",{})
		end
	)

	self.m_MoneyCtrl.m_AddMoneyButton.gameObject:SetActive(true)


	self.m_MoneyCtrl.m_AddMoneyButton.OnClick = DelegateFactory.Action_QnButton(
		function(button)
			MessageMgr.Inst:ShowMessage("TIANQI_JIFEN_ADD_TIPS",{})
		end
	)

	TianQiMysticalShop_EntryItem.Foreach(function (k,v)
		local go = NGUITools.AddChild(self.m_Grid.gameObject, self.m_RowTemplate)
		go:SetActive(true)
		self:InitRow(go.transform,v)
	end)

	self.m_Grid:Reposition()
		
end

function CLuaTianQiMysticlShop_Entry:OnSettingButtonClicked()
	CUIManager.ShowUI(CLuaUIResources.TianQiMysticalShopSettingWnd)
end

function CLuaTianQiMysticlShop_Entry:InitRow(tf,data)

	self.m_ItemTemplate = FindChild(tf,"ItemTemplate").gameObject
	self.m_ItemTemplate:SetActive(false)

	local descLabel = FindChild(tf,"RowDescLabel"):GetComponent(typeof(UILabel))
	descLabel.text = data.TypeName

	local itemGrid = FindChild(tf,"RowGrid"):GetComponent(typeof(UIGrid))
	
	local TimeLimitItem = TianQiMysticalShop_Setting.GetData().TimeLimitItem

	for itemId in string.gmatch(data.ItemIdList,"(%d+)") do
		local itemGo = NGUITools.AddChild(itemGrid.gameObject, self.m_ItemTemplate)
		itemGo:SetActive(true)

		local numItemId = tonumber(itemId)
		local istimelimit = false
		if TimeLimitItem and CommonDefs.DictContains(TimeLimitItem, typeof(uint), numItemId) then
			istimelimit = true
		end

		self:InitItem(itemGo.transform,numItemId, istimelimit)
	end
end


function CLuaTianQiMysticlShop_Entry:InitItem(tf, itemId, istimelimit)
    local icon = FindChild(tf,"Texture"):GetComponent(typeof(CUITexture))
    if not icon then return end


    local template = CItemMgr.Inst:GetItemTemplate(itemId)
    if template then
        icon:LoadMaterial(template.Icon)
		local QualitySprite = FindChild(tf,"QualitySprite"):GetComponent(typeof(UISprite))
		if QualitySprite then
			QualitySprite.spriteName = CUICommonDef.GetItemCellBorder(template, nil, false)
		end
		if istimelimit then
			FindChild(tf, "TimeLimit"):GetComponent(typeof(UILabel)).gameObject:SetActive(true)
		end
    end

	CommonDefs.AddOnClickListener(icon.gameObject,DelegateFactory.Action_GameObject(function(go)
         CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false,nil,AlignType.ScreenRight,0,0,0,0)
    end),false)

end

function CLuaTianQiMysticlShop_Entry:OnDestroy()
	LuaDelGlobal("PreTianQiShopWnd_ItemData")
end
