local UITabBar = import "L10.UI.UITabBar"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local Collider=import "UnityEngine.Collider"
local CLivingSkillMgr = import "L10.Game.CLivingSkillMgr"
local Vector3 = import "UnityEngine.Vector3"
local CLivingSkillItemTable = import "L10.UI.CLivingSkillItemTable"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CLivingSkillItem = import "L10.UI.CLivingSkillItem"
local CLivingSkillBaseDisplay = import "L10.UI.CLivingSkillBaseDisplay"
local CLivingSkillEffectDisplay = import "L10.UI.CLivingSkillEffectDisplay"
local LifeSkill_SkillType = import "L10.Game.LifeSkill_SkillType"
local CLivingSkillUpgradeCost = import "L10.UI.CLivingSkillUpgradeCost"

LuaLivingSkillWnd =class()
RegistClassMember(LuaLivingSkillWnd,"m_TabBar")
RegistClassMember(LuaLivingSkillWnd,"m_MakeTf")
RegistClassMember(LuaLivingSkillWnd,"m_LearnSkillTf")
RegistClassMember(LuaLivingSkillWnd,"m_CommonSkillTf")
RegistClassMember(LuaLivingSkillWnd,"m_OtherSkillTf")
RegistClassMember(LuaLivingSkillWnd,"m_ItemTable")
RegistClassMember(LuaLivingSkillWnd,"m_EffectDisplay")
RegistClassMember(LuaLivingSkillWnd,"m_SkillTypeTable")
RegistClassMember(LuaLivingSkillWnd,"m_MakeLabel")
RegistClassMember(LuaLivingSkillWnd,"m_UpgradeCost")
RegistClassMember(LuaLivingSkillWnd,"m_CollectTab")
RegistClassMember(LuaLivingSkillWnd,"m_LearnSkillBtn")
RegistClassMember(LuaLivingSkillWnd,"m_CollectBtn")

function LuaLivingSkillWnd:Awake()
    self:InitComponents()
    self:Init()
    CLivingSkillMgr.Inst.selectedItemId = 0
end

function LuaLivingSkillWnd:InitComponents()
    self.m_TabBar = self.transform:Find("TabBar"):GetComponent(typeof(UITabBar))
    self.m_MakeTf = self.transform:Find("Contents/MakeTf")
    self.m_LearnSkillTf = self.transform:Find("Contents/LearnSkillTf")
    self.m_CommonSkillTf = self.transform:Find("Contents/CommonSkillTf")
    self.m_OtherSkillTf = self.transform:Find("Contents/OtherSkillTf")
    self.m_EffectDisplay = self.m_OtherSkillTf:GetComponent(typeof(CLivingSkillEffectDisplay))
    self.m_ItemTable = self.m_MakeTf:Find("ItemTable"):GetComponent(typeof(CLivingSkillItemTable))
    self.m_SkillTypeTable = self.transform:Find("SkillTypeTable"):GetComponent(typeof(QnTableView))
    self.m_MakeLabel = self.transform:Find("TabBar/Tab2/MakeLabel"):GetComponent(typeof(UILabel))
    self.m_UpgradeCost = self.m_LearnSkillTf:Find("UpgradeCost"):GetComponent(typeof(CLivingSkillUpgradeCost))
    self.m_CollectTab = self.transform:Find("TabBar/Tab2").gameObject
    self.m_LearnSkillBtn = self.m_LearnSkillTf:Find("LearnSkillBtn").gameObject
    self.m_CollectBtn = self.m_MakeTf:Find("CollectBtn").gameObject
end

function LuaLivingSkillWnd:Init()
    self.m_TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnTabChange(index)
    end)
    self.m_MakeTf.localPosition = Vector3.zero
end

function LuaLivingSkillWnd:OnEnable()

    --帮贡更新
    --金钱更新
    g_ScriptEvent:AddListener("UpgradeLifeSkillSuccessed", self, "UpgradeLifeSkillSuccessed")

end

function LuaLivingSkillWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpgradeLifeSkillSuccessed", self, "UpgradeLifeSkillSuccessed")
end

function LuaLivingSkillWnd:OnDestroy()
    CLivingSkillMgr.Inst.useItemTemplateId = 0
    CLivingSkillMgr.Inst.targetItemTemplateId = 0
    CLivingSkillMgr.Inst:ClearPosition()
end

function LuaLivingSkillWnd:Start()
    --获取生活技能基本信息
    CLivingSkillMgr.Inst:QueryLifeSkillNeedInfo()

    --初始化
    self.m_SkillTypeTable.m_DataSource = DefaultTableViewDataSource.Create2(
            function()
                return self:NumberOfRows()
            end,
            function(view, row)
                return self:ItemAt(view, row)
            end)

    self.m_SkillTypeTable:ReloadData(false, false)
    self.m_SkillTypeTable.OnSelectAtRow = DelegateFactory.Action_int(function (index)
        self:OnSelectTableView(index)
    end)

    --刷新
    CLivingSkillMgr.Inst:InitPosition()
    self.m_SkillTypeTable:SetSelectRow(CLivingSkillMgr.Inst.entryIndex, true)
    --CLivingSkillMgr.Inst.ClearPosition();

    self:TryShowHouseStuffQuickMakeWnd()
end

function LuaLivingSkillWnd:OnTabChange(index)
    if index == 0 then
        --技能学习
        self.m_LearnSkillTf.gameObject:SetActive(true)
        self.m_CommonSkillTf.gameObject:SetActive(true)
        self.m_MakeTf.gameObject:SetActive(false)
    elseif index == 1 then
        self.m_LearnSkillTf.gameObject:SetActive(false)
        self.m_CommonSkillTf.gameObject:SetActive(false)
        self.m_MakeTf.gameObject:SetActive(true)
        self.m_ItemTable:Init()
    end
end

function LuaLivingSkillWnd:NumberOfRows()
    return CLivingSkillMgr.Inst.lifeSkillTypeCount
end

function LuaLivingSkillWnd:ItemAt(view, row)
    if view == self.m_SkillTypeTable then
        local skillType = CLivingSkillMgr.Inst:GetLifeSkillType(row)
        local item = TypeAs(view:GetFromPool(0), typeof(CLivingSkillItem))
        item:Init(skillType)
        return item
    end
    return nil
end

function LuaLivingSkillWnd:OnSelectTableView(row)
    local skillType = CLivingSkillMgr.Inst:GetLifeSkillType(row)
    CLivingSkillMgr.Inst.selectedSkill = skillType.ID
    self:UpdateSkillInfo()

    if skillType.Type == 0 then
        --面板切换
        self:SwitchDetailView(false, false)
        self.m_EffectDisplay:Refresh()
    else
        local level = CLivingSkillMgr.Inst:GetSkillLevel(CLivingSkillMgr.Inst.selectedSkill)
        self:SwitchDetailView(true, level == 0)
        CommonDefs.GetComponent_Component_Type(self.m_CommonSkillTf, typeof(CLivingSkillBaseDisplay)):Init()
    end
    self:UpdateTab()
end

function LuaLivingSkillWnd:UpdateSkillInfo()
    self.m_UpgradeCost:Refresh()
end

function LuaLivingSkillWnd:SwitchDetailView(isCommon, showLearnTab)
    --显示学习技能面板，tab面板，
    if isCommon then
        self.m_TabBar.gameObject:SetActive(true)
        self.m_OtherSkillTf.gameObject:SetActive(false)
        self.m_LearnSkillTf.gameObject:SetActive(true)

        self.m_TabBar.IgonreRepeatClick = false
        self.m_TabBar:ChangeTab(showLearnTab and 0 or 1, false)
        self.m_TabBar.IgonreRepeatClick = true
    else
        self.m_TabBar.gameObject:SetActive(false)
        self.m_MakeTf.gameObject:SetActive(false)
        self.m_CommonSkillTf.gameObject:SetActive(false)

        self.m_OtherSkillTf.gameObject:SetActive(true)
        self.m_LearnSkillTf.gameObject:SetActive(true)
    end
end

function LuaLivingSkillWnd:UpdateTab()
    --根据选中的类型更新显示
    local skillType = LifeSkill_SkillType.GetData(CLivingSkillMgr.Inst.selectedSkill)
    if skillType.Type == 2 then
        self.m_MakeLabel.text = LocalString.GetString("采集")
    elseif skillType.Type == 1 then
        self.m_MakeLabel.text = LocalString.GetString("钓鱼")
    elseif skillType.Type == 3 then
        self.m_MakeLabel.text = LocalString.GetString("炼药")
    elseif skillType.Type == 4 then
        self.m_MakeLabel.text = LocalString.GetString("烹饪")
    elseif skillType.Type == 5 then
        self.m_MakeLabel.text = LocalString.GetString("工艺")
    end
end

function LuaLivingSkillWnd:TryShowHouseStuffQuickMakeWnd()
    if CLivingSkillMgr.Inst.useItemTemplateId > 0 then
        local data = LifeSkill_SkillType.GetData(CLivingSkillMgr.Inst.selectedSkill)
        if data ~= nil and data.Type == LivingSkillType_lua.Craft then
            CUIManager.ShowUI(CUIResources.HouseStuffQuickMakeWnd)
        end
    end
end

function LuaLivingSkillWnd:UpgradeLifeSkillSuccessed(args)
    --技能升级成功
    --刷新界面
    self.m_ItemTable:UpdateSkillLevel()

    self.m_EffectDisplay:Refresh()
    CommonDefs.GetComponent_Component_Type(self.m_CommonSkillTf, typeof(CLivingSkillBaseDisplay)):Init()
    self:UpdateSkillInfo()
end

function LuaLivingSkillWnd:GetLearnLivingSkillButton()
    if self.m_TabBar.SelectedIndex == 1 then
        CGuideMgr.Inst:TriggerGuide(5)
    else
        if self.m_LearnSkillBtn.activeInHierarchy then
            local collider = self.m_LearnSkillBtn:GetComponent(typeof(Collider))
            if collider.enabled then
                return self.m_LearnSkillBtn
            else
                CGuideMgr.Inst:EndCurrentPhase()
            end
        end
    end
    return nil
end
function LuaLivingSkillWnd:GetCollectButton()
    if self.m_CollectBtn.activeInHierarchy then
        return self.m_CollectBtn
    end
    return nil
end
function LuaLivingSkillWnd:GetCollectTab()
    if self.m_CollectTab.activeInHierarchy then
        return self.m_CollectTab
    end
    return nil
end

function LuaLivingSkillWnd:GoTo(tab, section, row)
    self.m_SkillTypeTable:SetSelectRow(tab, true)
    self.m_ItemTable:GoTo(section, row)
end
