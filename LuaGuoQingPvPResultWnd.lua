local UILabel = import "UILabel"

local GameObject = import "UnityEngine.GameObject"
local EnumPlayTimesKey = import "L10.Game.EnumPlayTimesKey"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local CUITexture = import "L10.UI.CUITexture"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Animator = import "UnityEngine.Animator"

LuaGuoQingPvPResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuoQingPvPResultWnd, "Win", "Win", GameObject)
RegistChildComponent(LuaGuoQingPvPResultWnd, "Fail", "Fail", GameObject)
RegistChildComponent(LuaGuoQingPvPResultWnd, "Team1KillNumber", "Team1KillNumber", GameObject)
RegistChildComponent(LuaGuoQingPvPResultWnd, "Team2KillNumber", "Team2KillNumber", GameObject)
RegistChildComponent(LuaGuoQingPvPResultWnd, "Team1", "Team1", GameObject)
RegistChildComponent(LuaGuoQingPvPResultWnd, "Team2", "Team2", GameObject)
RegistChildComponent(LuaGuoQingPvPResultWnd, "Player", "Player", GameObject)
RegistChildComponent(LuaGuoQingPvPResultWnd, "skillicon", "skillicon", GameObject)
RegistChildComponent(LuaGuoQingPvPResultWnd, "Worditem", "Worditem", GameObject)
RegistChildComponent(LuaGuoQingPvPResultWnd, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaGuoQingPvPResultWnd, "AgainButton", "AgainButton", GameObject)
RegistChildComponent(LuaGuoQingPvPResultWnd, "RewardNumberText", "RewardNumberText", GameObject)
RegistChildComponent(LuaGuoQingPvPResultWnd, "glow1", "glow1", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaGuoQingPvPResultWnd,"m_SkillButtons")
RegistClassMember(LuaGuoQingPvPResultWnd,"awarlimit")
RegistClassMember(LuaGuoQingPvPResultWnd,"mainAnimator")
RegistClassMember(LuaGuoQingPvPResultWnd,"WinorFail")
RegistClassMember(LuaGuoQingPvPResultWnd,"Team_MaxScore")
RegistClassMember(LuaGuoQingPvPResultWnd,"isInited")
function LuaGuoQingPvPResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    UIEventListener.Get(self.AgainButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAgainButtonClick()
	end)
    UIEventListener.Get(self.ShareButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)
    self.m_SkillButtons = {}
    local setting = GuoQing2022_JinLuHunYuanZhan.GetData()
    self.awarlimit = setting.AwardLimit
    self.mainAnimator = self.transform:GetComponent(typeof(Animator))
    self.Win:SetActive(false)
    self.Fail:SetActive(false)
    self.glow1:SetActive(false)
    self.isInited = false
end

function LuaGuoQingPvPResultWnd:Update()
    if CClientMainPlayer.Inst and self.isInited == false then
        self.isInited = true
        self:ShowWndInfo()
    end
end

function LuaGuoQingPvPResultWnd:Init()
    self.Worditem:SetActive(false)
    self.skillicon:SetActive(false)
    self.Player:SetActive(false)
end

function LuaGuoQingPvPResultWnd:OnEnable()
    self.mainAnimator.enabled = false
end

--@region UIEvent

function LuaGuoQingPvPResultWnd:ShowWndInfo()
    self:CheckWF()
    self:ShowInfo()
    self.mainAnimator.enabled = true
    if self.WinorFail == true then
        self.mainAnimator:Play("guoqingpvpresultwnd_slshown", 0, 0)
    else
        self.mainAnimator:Play("guoqingpvpresultwnd_sbshown", 0, 0)
    end
end

function LuaGuoQingPvPResultWnd:OnAgainButtonClick()
    CUIManager.ShowUI(CLuaUIResources.GuoQingPvPWnd)
end
function LuaGuoQingPvPResultWnd:OnShareButtonClick()
    CUICommonDef.CaptureScreenAndShare()
end
function  LuaGuoQingPvPResultWnd:CheckWF()
    self.WinorFail = true
    self.Team_MaxScore = {0,0}
    local myplayerid = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local InfoData = {LuaGuoQingPvPMgr.DefendTeamSkillInfoData,LuaGuoQingPvPMgr.AttackTeamSkillInfoData}
    for t = 1,2 do
        for i = 1,#InfoData[t] do
            local data = InfoData[t][i]
            local playerId = data[1]
            local force = data[5]
            local score = LuaGuoQingPvPMgr.ResultInfoData[playerId][5]

            if score > self.Team_MaxScore[t] then
                self.Team_MaxScore[t] = score
            end
            if playerId == myplayerid then
                if force == LuaGuoQingPvPMgr.WinForce then
                    self.WinorFail = true
                    self.Win:SetActive(true)
                    self.Fail:SetActive(false)
                    self.glow1:SetActive(true)
                else
                    self.WinorFail = false
                    self.Win:SetActive(false)
                    self.Fail:SetActive(true)
                    self.glow1:SetActive(false)
                    local Fengye_Texture1 = self.Fail.transform:Find("Fengye_Texture (7)").gameObject
                    CUICommonDef.SetActive(Fengye_Texture1, true, false)
                    local Fengye_Texture2 = self.transform:Find("Fengye_Texture (3)").gameObject
                    CUICommonDef.SetActive(Fengye_Texture2, true, false)
                    local Fengye_Texture3 = self.transform:Find("Fengye_Texture (5)").gameObject
                    CUICommonDef.SetActive(Fengye_Texture3, true, false)
                end
            end
        end
    end
end
function  LuaGuoQingPvPResultWnd:ShowInfo()
    local InfoData = {LuaGuoQingPvPMgr.DefendTeamSkillInfoData,LuaGuoQingPvPMgr.AttackTeamSkillInfoData}
    local KillNumber = {0,0}
    local KillColor = {"",""}
    local Team = {self.Team1,self.Team2}
    local playtimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eJinLuHunYuanZhanTimes) or 0
    if playtimes > self.awarlimit then
        playtimes = self.awarlimit
    end
    local myplayerid = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    self.RewardNumberText:GetComponent(typeof(UILabel)).text = SafeStringFormat3("%s(%s/%s)",LocalString.GetString("今日已获得奖励次数"),tostring(playtimes),tostring(self.awarlimit))

    local MaxScorePlayer = {nil,nil}

    for t = 1,2 do
        for i = 1,#InfoData[t] do
            local data = InfoData[t][i]
            local playerId = data[1]
            local name =data[2]
            local class = data[3]
            local gender = data[4]
            local force = data[5]
            local playerheadicon = CUICommonDef.GetPortraitName(class, gender)
            
            local color = ""
            if playerId == myplayerid then
                color = "[5bff0a]"
            end
            if force == LuaGuoQingPvPMgr.WinForce then
                KillColor[t] = "[fffe91]"
            end

            local instance = NGUITools.AddChild(Team[t],self.Player)
            if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == playerId then
                local hname = "Highlight_team"..tostring(t)
                local Highlight = FindChild(instance.transform,hname).gameObject
                Highlight:SetActive(true)
            end
            instance:SetActive(true)
    
            local headicon = FindChild(instance.transform:Find("Info").transform,"Icon").gameObject:GetComponent(typeof(CUITexture))
            headicon:LoadNPCPortrait(playerheadicon)
            local nameobj = FindChild(instance.transform,"name").gameObject:GetComponent(typeof(UILabel))
            nameobj.text = color..name
            
            local SkillGird = instance.transform:Find("Skill").gameObject
            for j = 1,7 do
                local skillid = nil
               
                local go=NGUITools.AddChild(SkillGird,self.skillicon)
                
                UIEventListener.Get(go.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                    self:OnSkillIconClick(go)
                end)
    
                local icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
                if data[j+5] ~= 0 then
                    skillid = data[j+5]
                    local skilldata = Skill_AllSkills.GetData(skillid)
                    if skilldata ~= nil then
                        icon:LoadSkillIcon(skilldata.SkillIcon)
                    end 
                else
                    icon:Clear()
                end
                table.insert(self.m_SkillButtons,{go,skillid})
                go:SetActive(true)
            end 
    
            local tdata = LuaGuoQingPvPMgr.ResultInfoData[playerId] 
            local DataText = instance.transform:Find("DataText").gameObject
            for j = 1,5 do
                local text = color..tdata[j]
                if j > 1 and j < 5 then
                    text = text.."%"
                end
                local go= NGUITools.AddChild(DataText,self.Worditem)
                local t = go.gameObject:GetComponent(typeof(UILabel))
                t.text = text
                go:SetActive(true)
            end 
            KillNumber[t] = KillNumber[t] + tdata[1]
            if tdata[5] == self.Team_MaxScore[t] then
                if force == LuaGuoQingPvPMgr.WinForce then
                    MaxScorePlayer[1] = instance
                else
                    MaxScorePlayer[2] = instance
                end

                
            end
            Team[t]:GetComponent(typeof(UIGrid)):Reposition()
        end
    end

    self.Team1KillNumber.transform:Find("NumberLabel"):GetComponent(typeof(UILabel)).text = KillColor[1]..tostring(KillNumber[1])
    self.Team1KillNumber.transform:Find("WordLabel"):GetComponent(typeof(UILabel)).text = KillColor[1]..LocalString.GetString("总击杀")
    
    self.Team2KillNumber.transform:Find("NumberLabel"):GetComponent(typeof(UILabel)).text = KillColor[2]..tostring(KillNumber[2])
    self.Team2KillNumber.transform:Find("WordLabel"):GetComponent(typeof(UILabel)).text = KillColor[2]..LocalString.GetString("总击杀")

    if MaxScorePlayer[1] ~= nil then
        local instance = MaxScorePlayer[1]
        FindChild(instance.transform,"zhanshen").gameObject:SetActive(true)
    end
    if MaxScorePlayer[2] ~= nil then
        local instance = MaxScorePlayer[2]
        FindChild(instance.transform,"yinghun").gameObject:SetActive(true)
    end
end

function LuaGuoQingPvPResultWnd:OnSkillIconClick(go)
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then
        return
    end
    for i = 1,#self.m_SkillButtons do
        if self.m_SkillButtons[i][1] == go then
            local skillId = self.m_SkillButtons[i][2]
            if skillId == nil or skillId == SkillButtonSkillType_lua.NoSkill then
                return
            end
            CSkillInfoMgr.ShowSkillInfoWnd(skillId, true, 0, 0, nil)
            break
        end
    end
end

--@endregion UIEvent

