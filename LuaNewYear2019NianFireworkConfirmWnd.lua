local Item_Item = import "L10.Game.Item_Item"
local CUITexture=import "L10.UI.CUITexture"
CLuaNewYear2019NianFireworkConfirmWnd = class()
RegistClassMember(CLuaNewYear2019NianFireworkConfirmWnd,"m_LeftTime")
RegistClassMember(CLuaNewYear2019NianFireworkConfirmWnd,"m_Tick")
RegistClassMember(CLuaNewYear2019NianFireworkConfirmWnd,"m_CountdownLabel")
function CLuaNewYear2019NianFireworkConfirmWnd:Init()
    self.m_LeftTime=15
    -- 21020279,3;
    local data = Item_Item.GetData(21020279)
    local icon  = FindChild(self.transform,"Icon"):GetComponent(typeof(CUITexture))
    icon:LoadMaterial(data.Icon)
    local numLabel = FindChild(self.transform,"NumLabel"):GetComponent(typeof(UILabel))
    numLabel.text = "3"

    local okButton = FindChild(self.transform,"OKButton").gameObject
    self.m_CountdownLabel = okButton.transform:Find("Label"):GetComponent(typeof(UILabel))
    UIEventListener.Get(okButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI("NewYear2019NianFireworkConfirmWnd")
        Gac2Gas.Request2019FireworkGift()
    end)

    local descLabel = FindChild(self.transform,"DescLabel"):GetComponent(typeof(UILabel))
    descLabel.text= g_MessageMgr:FormatMessage("NewYear2019_NianFirework_Gift")

    self:StartCountdown()

end
function CLuaNewYear2019NianFireworkConfirmWnd:StartCountdown()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
    end
    self.m_LeftTime=15
    self.m_Tick = RegisterTickWithDuration(function ()
        self.m_LeftTime = self.m_LeftTime - 1
        self.m_CountdownLabel.text = SafeStringFormat3(LocalString.GetString("收下(%d)"),self.m_LeftTime)
        if self.m_LeftTime<=0 then
            UnRegisterTick(self.m_Tick)
            self.m_Tick=nil
            CUIManager.CloseUI("NewYear2019NianFireworkConfirmWnd")
            Gac2Gas.Request2019FireworkGift()
        end
    end,1000,15000)
end

function CLuaNewYear2019NianFireworkConfirmWnd:OnDestroy()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
    end
end

function CLuaNewYear2019NianFireworkConfirmWnd:OnEnable()
end

function CLuaNewYear2019NianFireworkConfirmWnd:OnDisable()
end
