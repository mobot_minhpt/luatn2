local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CIMMgr = import "L10.Game.CIMMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CButton = import "L10.UI.CButton"

LuaFSCInviteWnd = class()

LuaFSCInviteWnd.s_List = {}
LuaFSCInviteWnd.s_LastInviteExpiredTime = 0

RegistClassMember(LuaFSCInviteWnd, "m_Table")
RegistClassMember(LuaFSCInviteWnd, "m_Template")
RegistClassMember(LuaFSCInviteWnd, "m_InviteTick")
RegistClassMember(LuaFSCInviteWnd, "m_Index2Obj")

function LuaFSCInviteWnd:Awake()
    self.m_Table = self.transform:Find("Anchor/ScrollView/Table"):GetComponent(typeof(UITable))
    self.m_Template = self.transform:Find("Anchor/Pool/FriendListItem").gameObject
    self.m_Template:SetActive(false)

    self.m_InviteTick = {}
    self.m_Index2Obj = {}
end

function LuaFSCInviteWnd:Init()
    self:ClearAllTick()
    
    self.m_Index2Obj = {}
    Extensions.RemoveAllChildren(self.m_Table.transform)
    for k, info in ipairs(LuaFSCInviteWnd.s_List) do
        table.insert(self.m_Index2Obj, NGUITools.AddChild(self.m_Table.gameObject, self.m_Template))
        self:InitPlayer(k, info, self.m_Index2Obj[k])
    end
    self.transform:Find("Anchor/EmptyLabel").gameObject:SetActive(#LuaFSCInviteWnd.s_List == 0)
    self.m_Table:Reposition()
end

function LuaFSCInviteWnd:OnEnable()
    g_ScriptEvent:AddListener("FSC_InviteSuccess", self, "FSC_InviteSuccess")
    g_ScriptEvent:AddListener("FSC_OnInvitationRejected", self, "FSC_OnInvitationRejected")
end

function LuaFSCInviteWnd:OnDisable()
    g_ScriptEvent:RemoveListener("FSC_InviteSuccess", self, "FSC_InviteSuccess")
    g_ScriptEvent:RemoveListener("FSC_OnInvitationRejected", self, "FSC_OnInvitationRejected")
end

function LuaFSCInviteWnd:OnDestroy()
    self:ClearAllTick()
end

function LuaFSCInviteWnd:ClearAllTick()
   for _, tick in pairs(self.m_InviteTick) do
        UnRegisterTick(tick)
        tick = nil
   end
   self.m_InviteTick = {}
end

function LuaFSCInviteWnd:ClearTick(i)
    UnRegisterTick(self.m_InviteTick[i])
    self.m_InviteTick[i] = nil
end

function LuaFSCInviteWnd:InitPlayer(index, info, obj)
    local playerId = info[1]
    local name = info[2]
    local cls = info[3]
    local gender = info[4]
    local rejectExpiredTime = info[5]
    
    obj:SetActive(true)
    obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = name
    obj.transform:Find("FriendLabel").gameObject:SetActive(CIMMgr.Inst:IsMyFriend(playerId))
    obj.transform:Find("Border/Icon"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender, -1), false)

    local btn = obj.transform:Find("InviteButton"):GetComponent(typeof(CButton))
    local countdownLabel = btn.transform:Find("Label"):GetComponent(typeof(UILabel))
    if LuaFSCInviteWnd.s_LastInviteExpiredTime > CServerTimeMgr.Inst.timeStamp or rejectExpiredTime > CServerTimeMgr.Inst.timeStamp then
        btn.Enabled = false
        local expiredTime = math.max(LuaFSCInviteWnd.s_LastInviteExpiredTime, rejectExpiredTime)
        local count = math.ceil(expiredTime - CServerTimeMgr.Inst.timeStamp)
        countdownLabel.text = "( "..count..LocalString.GetString("秒").." )"
        self:ClearTick(index)
        self.m_InviteTick[index] = RegisterTick(function()
            count = count - 1
            if count <= 0 then
                self:ClearTick(index)
                btn.Enabled = true
                countdownLabel.text = LocalString.GetString("邀请")
            else
                countdownLabel.text = "( "..count..LocalString.GetString("秒").." )"
            end
        end, 1000)
    else
        btn.Enabled = true
        countdownLabel.text = LocalString.GetString("邀请")
    end
    UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function(obj)
        Gac2Gas.Anniv2023FSC_InvitePlayer(playerId)
    end)
end

function LuaFSCInviteWnd:FSC_InviteSuccess()
    self:Init()
end

function LuaFSCInviteWnd:FSC_OnInvitationRejected(playerId, playerName, rejectExpiredTime)
    local index = nil
    for k, info in ipairs(LuaFSCInviteWnd.s_List) do
        if info[1] == playerId then
            info[2] = playerName
            info[5] = rejectExpiredTime
            index = k
            break
        end
    end
    if index then
        self:InitPlayer(index, LuaFSCInviteWnd.s_List[index], self.m_Index2Obj[index])
    end
end

