local CItemMgr=import "L10.Game.CItemMgr"
local CUITexture=import "L10.UI.CUITexture"
local UIInput = import "UIInput"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local EquipmentTemplate_Equip=import "L10.Game.EquipmentTemplate_Equip"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local MessageMgr = import "L10.Game.MessageMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local UITabBar = import "L10.UI.UITabBar"
local QnTabButton = import "L10.UI.QnTabButton"
local Constants = import "L10.Game.Constants"

LuaBQPConfirmCommitWnd=class()

RegistClassMember(LuaBQPConfirmCommitWnd,"EquipTexture")
RegistClassMember(LuaBQPConfirmCommitWnd,"QualitySprite")
RegistClassMember(LuaBQPConfirmCommitWnd,"NameLabel")
RegistClassMember(LuaBQPConfirmCommitWnd,"ScoreLabel")

RegistClassMember(LuaBQPConfirmCommitWnd,"Tags")
RegistClassMember(LuaBQPConfirmCommitWnd,"Tag1")
RegistClassMember(LuaBQPConfirmCommitWnd,"Tag2")

RegistClassMember(LuaBQPConfirmCommitWnd,"DescInput")
RegistClassMember(LuaBQPConfirmCommitWnd,"Cost")
RegistClassMember(LuaBQPConfirmCommitWnd,"CommitBtn")

RegistClassMember(LuaBQPConfirmCommitWnd,"TagIds")
RegistClassMember(LuaBQPConfirmCommitWnd,"TagNames")
RegistClassMember(LuaBQPConfirmCommitWnd,"SelectedTagIds")
RegistClassMember(LuaBQPConfirmCommitWnd,"WordAttr")

function LuaBQPConfirmCommitWnd:Init()
	if not LuaBingQiPuMgr.m_EquipToCommit then
		CUIManager.CloseUI(CLuaUIResources.BQPConfirmCommitWnd)
	end
	self.TagIds = {}
	self.SelectedTagIds = 0

	self:InitComponent()
	self:InitEquipInfo()
	self:InitTags()
end

function LuaBQPConfirmCommitWnd:InitComponent()
	self.EquipTexture = self.transform:Find("Anchor/EquipInfo/Equip/EquipTexture"):GetComponent(typeof(CUITexture))
	self.QualitySprite = self.transform:Find("Anchor/EquipInfo/Equip/QualitySprite"):GetComponent(typeof(UISprite))
	self.NameLabel = self.transform:Find("Anchor/EquipInfo/NameLabel"):GetComponent(typeof(UILabel))
	self.ScoreLabel = self.transform:Find("Anchor/EquipInfo/ScoreLabel"):GetComponent(typeof(UILabel))

	self.Tags = self.transform:Find("Anchor/Tags"):GetComponent(typeof(UITabBar))
	self.Tag1 = self.transform:Find("Anchor/Tags/Tag1").gameObject
	self.Tag2 = self.transform:Find("Anchor/Tags/Tag2").gameObject

	self.DescInput = self.transform:Find("Anchor/Desc/Input"):GetComponent(typeof(UIInput))
	self.Cost = self.transform:Find("Anchor/Bottom/Cost"):GetComponent(typeof(CCurentMoneyCtrl))
	self.CommitBtn = self.transform:Find("Anchor/Bottom/CommitBtn").gameObject

	local setting = BingQiPu_Setting.GetData()
	local cost = setting.SubCost
	self.Cost:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, false)
	self.Cost:SetCost(tonumber(cost))

	local onCommit = function (go)
		self:OnCommitBtnClick(go)
	end
	CommonDefs.AddOnClickListener(self.CommitBtn,DelegateFactory.Action_GameObject(onCommit),false)

	self.Tags.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTagItemSelect(index)
	end)
	
end

function LuaBQPConfirmCommitWnd:InitEquipInfo()
	local item = CItemMgr.Inst:GetById(LuaBingQiPuMgr.m_EquipToCommit.itemId)
	local template = EquipmentTemplate_Equip.GetData(LuaBingQiPuMgr.m_EquipToCommit.templateId)
	if not item or not template then return end
	self.EquipTexture:LoadMaterial(item.Icon)
	self.QualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item.Equip.QualityType)
	local previousAdjusted = item.Equip.IsFeiShengAdjusted
	item.Equip.IsFeiShengAdjusted = 0
	self.NameLabel.text = item.Equip.DisplayName
	self.NameLabel.color = item.Equip.DisplayColor
	self.ScoreLabel.text = SafeStringFormat3(LocalString.GetString("[91FAFF]装备评分: [-][FFFE91]%d[-]"), item.Equip.Score)
	item.Equip.IsFeiShengAdjusted = previousAdjusted
end

function LuaBQPConfirmCommitWnd:InitTags()
	-- 目前随意生成2个TAG
	self.TagIds = {}
	if LuaBingQiPuMgr.m_EquipToCommit.CommitType == 0 then
		BingQiPu_EquipmentTag.Foreach(function (key,data)
			if not data.ComparedRank or data.ComparedRank == "" then
				table.insert(self.TagIds, {ID = key,Score = 0,Name = data.VoteNum})
			else
				local score = self:CaculateWordScore(data)
				if score > 0 then
					if score >= tonumber(data.ComparedRankPara) then
						table.insert(self.TagIds,  {ID = key,Score = score,Name = data.VoteNum})
					end
				end
			end
		end)
	else
		BingQiPu_TreasureTag.Foreach(function (key,data)
			if not data.ComparedRank or data.ComparedRank == "" then
				table.insert(self.TagIds, {ID = key,Score = 0,Name = data.TreasureVoteNum})
			else
				local score = self:CaculateWordScore(data)
				local p = tonumber(data.ComparedRankPara)
				if not p then p = 0 end

				if score >= p then
					table.insert(self.TagIds, {ID = key,Score = score,Name = data.TreasureVoteNum})
				end
			end
		end)
	end
	table.sort(self.TagIds,function(a,b)
		if a.Score == b.Score then 
			return a.ID < b.ID
		else
			return a.Score > b.Score
		end
	end)
	if #self.TagIds >=1 then
		self.Tag1:SetActive(true)
		self:InitTagItem(self.Tag1, self.TagIds[1])
	else
		self.Tag1:SetActive(false)
	end
	if #self.TagIds >=2 then
		self.Tag2:SetActive(true)
		self:InitTagItem(self.Tag2, self.TagIds[2])
	else
		self.Tag2:SetActive(false)
	end
end

function LuaBQPConfirmCommitWnd:GetScoreP(itemWordInfo,wordReferred,tagdata)
	local score = 0
	local ts = {}
	for i = 0, itemWordInfo.Count - 1, 1 do
		local base = math.floor(itemWordInfo[i].Id / 100)
		local level = math.floor(itemWordInfo[i].Id % 100)
		if ts[base] == nil then 
			ts[base] = 1
		else
			ts[base] = ts[base] + 1
		end
	end
	for k,v in pairs(ts) do
		for j = 1, #wordReferred, 1 do
			if k == wordReferred[j] then
				local formula = GetFormula(tagdata.WeightCalFormula)
				if formula then
					score = score + (formula(nil,nil,{v}) or 0 )
				end
			end
		end
	end
	return score
end

function LuaBQPConfirmCommitWnd:GetScoreTopTen(itemWordInfo,wordReferred,tagdata)
	local f = 0
	local formula = GetFormula(tagdata.WeightCalFormula)
	for i = 0, itemWordInfo.Count - 1, 1 do
		local base = math.floor(itemWordInfo[i].Id / 100)
		local level = math.floor(itemWordInfo[i].Id % 100)
		local fdata = BingQiPu_ComparedWordSetting.GetData(base)
		if fdata then
			for j = 1, #wordReferred, 1 do
				if base == wordReferred[j] then
					f = f + fdata.TopTenFactor * level
				end
			end 
		end
	end
	if formula then
	 	return formula(nil,nil,{f}) or 0 
	end
	return 0
end

-- 计算涉及词条的总分
function LuaBQPConfirmCommitWnd:CaculateWordScore(tagdata)
	local rankName = tagdata.ComparedRank
	if rankName == "perfectRateRank" then
		local item = CItemMgr.Inst:GetById(LuaBingQiPuMgr.m_EquipToCommit.itemId)
		if not item then return 0 end
		local perfectRank = item.Equip:GetIntensifyTotalValue() / 100
		return perfectRank
	end

	local rankword = tagdata.Word
	local wordStr = CommonDefs.StringSplit_ArrayChar(rankword, ";")
	local wordReferred = {}
	for i = 0, wordStr.Length - 1, 1 do
		if wordStr[i] then
			if wordStr[i] then
				table.insert(wordReferred, tonumber(wordStr[i]))
			end
		end
	end

	local item = CItemMgr.Inst:GetById(LuaBingQiPuMgr.m_EquipToCommit.itemId)
	local itemWordInfo = item.Equip.WordInfo

	if LuaBingQiPuMgr.m_EquipToCommit.CommitType == 1 then --奇珍的计算方式
		return self:GetScoreP(itemWordInfo,wordReferred,tagdata)
	else
		return self:GetScoreTopTen(itemWordInfo,wordReferred,tagdata)
	end
	return 0
end

function LuaBQPConfirmCommitWnd:InitTagItem(go, tagdata)
	local tf = go.transform
	local btn = tf:GetComponent(typeof(QnTabButton))
	btn.Text = tagdata.Name
end

function LuaBQPConfirmCommitWnd:OnTagItemSelect(index)
	self.SelectedTagIds = self.TagIds[index+1].ID
end

function LuaBQPConfirmCommitWnd:OnCommitBtnClick(go)
	
	if self.SelectedTagIds == 0 then
		MessageMgr.Inst:ShowMessage("BQP_TAG_NOT_ENOUGH", {})
		return
	end

	local content = self.DescInput.value
    if not content or content=="" then
    	MessageMgr.Inst:ShowMessage("BQP_COMMIT_NEED_DESC", {})
        return
    end

    local len = CUICommonDef.GetStrChsLength(content)
    if len > 30 then
    	MessageMgr.Inst:ShowMessage("BQP_COMMIT_DESC_TOOMUCH", {})
        return
    end
    if not CWordFilterMgr.Inst:CheckLinkAndMatch(content) then
    	MessageMgr.Inst:ShowMessage("BQP_COMMIT_DESC_ILLEGAL", {})
        return
    end

    local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(content,true)
    content = ret.msg

    if self.Cost.moneyEnough then
		local item = CItemMgr.Inst:GetById(LuaBingQiPuMgr.m_EquipToCommit.itemId)
		if item.Equip.HasTwoWordSet then
			local msg = MessageMgr.Inst:FormatMessage("BQP_SECOND_WORD", {})
			local okfunc = function()
				self:RequireSubmit(content)
			end
			g_MessageMgr:ShowOkCancelMessage(msg,okfunc,nil,nil,nil,false)
		else
			self:RequireSubmit(content)
		end
    else
    	local msg = MessageMgr.Inst:FormatMessage("NotEnough_Silver_QuickBuy", {})
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(
        	function()
        		CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
        	end
        	), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    end
end

function LuaBQPConfirmCommitWnd:RequireSubmit(content)
	local equipId = LuaBingQiPuMgr.m_EquipToCommit.itemId
    local tag1 = self.SelectedTagIds
    local desc = content
    local bPresiuocRank = LuaBingQiPuMgr.m_EquipToCommit.CommitType==1 --是否评选奇珍榜, true代表奇珍榜，false代表十大兵器
    Gac2Gas.BQPSubmitEquipPrecheck(equipId, tag1, desc, bPresiuocRank)
end
