local MessageWndManager = import "L10.UI.MessageWndManager"
local CAchievementMgr=import "L10.Game.CAchievementMgr"
local CUITexture = import "L10.UI.CUITexture"
local Vector4 = import "UnityEngine.Vector4"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CUIFx = import "L10.UI.CUIFx"

CLuaPlayerAchievementView = class()

RegistClassMember(CLuaPlayerAchievementView,"m_HaoGanDuButton") --好感度按钮
RegistClassMember(CLuaPlayerAchievementView,"m_ZhuXianButton") --主线妖鬼狐按钮
RegistClassMember(CLuaPlayerAchievementView,"m_ZhuXianButtonAlertSprite") --主线妖鬼狐按钮红点
RegistClassMember(CLuaPlayerAchievementView,"m_ZhuXianButtonFx") --特效提示
RegistClassMember(CLuaPlayerAchievementView,"m_AchieveTabsRoot") --成就页签
RegistClassMember(CLuaPlayerAchievementView,"m_RecommendTable") --推荐成就列表
RegistClassMember(CLuaPlayerAchievementView,"m_RecommendTemplate") --推荐成就Item模板
RegistClassMember(CLuaPlayerAchievementView,"m_AchieveCountLabel") -- 达成成就数
RegistClassMember(CLuaPlayerAchievementView,"m_AchieveProgressBar") -- 达成成就进度
RegistClassMember(CLuaPlayerAchievementView,"m_StarCountTexture") --达成成就星星数图标
RegistClassMember(CLuaPlayerAchievementView,"m_StarCountLabel") --达成成就星星数量
RegistClassMember(CLuaPlayerAchievementView,"m_GetAllAwardsButton") --一键领奖
RegistClassMember(CLuaPlayerAchievementView,"m_GetAllAwardsButtonFxGo") --一键领奖特效
RegistClassMember(CLuaPlayerAchievementView,"m_AchieveTabNameTbl") --成就分组灯笼与成就Group TabName对应关系
RegistClassMember(CLuaPlayerAchievementView,"m_SeaFishingBtn")--海钓图鉴的入口
RegistClassMember(CLuaPlayerAchievementView,"m_SeaFishingTuJianAmount")--海钓图鉴的入口
RegistChildComponent(CLuaPlayerAchievementView, "Tab3AlertSprite", "Tab3AlertSprite", GameObject)

function CLuaPlayerAchievementView:Awake()
    self.m_HaoGanDuButton = self.transform:Find("HaoGanDuButton").gameObject
    self.m_ZhuXianButton = self.transform:Find("ZhuXianButton").gameObject
    self.m_ZhuXianButtonFx = self.m_ZhuXianButton.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    self.m_ZhuXianButtonAlertSprite = self.m_ZhuXianButton.transform:Find("AlertSprite")
    self.m_ZhuXianButtonAlertSprite.gameObject:SetActive(false)
    self.m_AchieveTabsRoot = self.transform:Find("AchieveTabs")
    self.m_RecommendTable = self.transform:Find("Overview/RecommendTable"):GetComponent(typeof(UITable))
    self.m_RecommendTemplate = self.transform:Find("Overview/RecommendTemplate").gameObject
    self.m_AchieveCountLabel = self.transform:Find("Overview/StarCountTexture/AchieveCountLabel"):GetComponent(typeof(UILabel))
    self.m_AchieveProgressBar = self.transform:Find("Overview/StarCountTexture/AchieveProgress"):GetComponent(typeof(UITexture))
    self.m_StarCountTexture = self.transform:Find("Overview/StarCountTexture"):GetComponent(typeof(CUITexture))
    self.m_StarCountLabel = self.transform:Find("Overview/StarCountTexture/StarCountLabel"):GetComponent(typeof(UILabel))
    self.m_GetAllAwardsButton =  self.transform:Find("Overview/StarCountTexture/GetAllAwardsButton").gameObject
    self.m_GetAllAwardsButtonFxGo = self.m_GetAllAwardsButton.transform:Find("Fx").gameObject
    self.m_SeaFishingBtn = self.transform:Find("SeaFishingBtn").gameObject
    self:UpdateZhuXianButtonFx()
    self:InitSeaFishingTuJianAmount()
    UIEventListener.Get(self.m_ZhuXianButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnZhuXianButtonClick(go)
    end)

    UIEventListener.Get(self.m_SeaFishingBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSeaFishingBtnClick(go)
    end)
    
    if IsNpcHaoGanDuOpen() then
        local open= false
        if CClientMainPlayer.Inst then
            local taskProp = CClientMainPlayer.Inst.TaskProp
            if taskProp:IsMainPlayerTaskFinished(22203644) then
                open = true
            end
        end
        if open then
            self.m_HaoGanDuButton:SetActive(true)
            UIEventListener.Get(self.m_HaoGanDuButton).onClick = DelegateFactory.VoidDelegate(function(go)
                self:OnHaoGanDuButtonClick(go)
            end)
        else
            self.m_HaoGanDuButton:SetActive(false)
        end
    else
        self.m_HaoGanDuButton:SetActive(false)
    end

    self.m_RecommendTemplate:SetActive(false)
     UIEventListener.Get(self.m_GetAllAwardsButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:GetAllAward(go)
    end)
end

function CLuaPlayerAchievementView:InitSeaFishingTuJianAmount()
    self.m_SeaFishingTuJianAmount = 0
    HouseFish_AllFishes.Foreach(function (itemId, data)
        local fishIdx = data.FishIdx
		if fishIdx > self.m_SeaFishingTuJianAmount then
			self.m_SeaFishingTuJianAmount = fishIdx
		end
    end)
    self:UpdateSeaFishingButtonFx()
end

function CLuaPlayerAchievementView:UpdateSeaFishingButtonFx()
    --收集信息
    local valueLabel = self.m_SeaFishingBtn.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
    local fishbasket = CClientMainPlayer.Inst.ItemProp.FishBasket
	if not fishbasket then return end
	local tujian = fishbasket.TuJian

    local amount = self.m_SeaFishingTuJianAmount
    local gotCount = 0
    for idx=1,self.m_SeaFishingTuJianAmount,1 do
        if tujian:GetBit(idx) then
            gotCount = gotCount + 1
        end
    end
    valueLabel.text = SafeStringFormat3(LocalString.GetString("%d/%d"), gotCount, amount)
end

function CLuaPlayerAchievementView:UpdateZhuXianButtonFx()
    if self:CheckZhuXianAchievementAvailability() then
        local bg = self.m_ZhuXianButton:GetComponent(typeof(UIWidget))
        self.m_ZhuXianButtonFx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
        CUIFx.DoAni(Vector4(-bg.width * 0.5, bg.height*0.5, bg.width, bg.height), false, self.m_ZhuXianButtonFx)
    else
        self.m_ZhuXianButtonFx:DestroyFx()
    end
    --进度信息
    local valueLabel = self.m_ZhuXianButton.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
    local totalCount,finishCount,finishButNotAwardedCount = LuaAchievementMgr:GetAchievementsInfoByTabName(LocalString.GetString("妖鬼狐"))
    valueLabel.text = SafeStringFormat3("%d/%d", finishCount, totalCount)
end

function CLuaPlayerAchievementView:CheckAwardsAvailability()
    self.m_GetAllAwardsButtonFxGo:SetActive(CAchievementMgr.Inst:CheckAchievementAvailable())
end

function CLuaPlayerAchievementView:CheckZhuXianAchievementAvailability() -- 查询主线任务是否有未领取奖励
    local tabName = LocalString.GetString("妖鬼狐")
    local subTabName = tabName
    local data_list = {}
    local available = false
    if CommonDefs.DictContains(CAchievementMgr.Inst.m_TabNameList, typeof(String), tabName) then
        local tabInfo = CommonDefs.DictGetValue(CAchievementMgr.Inst.m_TabNameList, typeof(String), tabName)
        if CommonDefs.DictContains(tabInfo.subTabName, typeof(String), subTabName) then
            local subTabInfo = CommonDefs.DictGetValue(tabInfo.subTabName, typeof(String), subTabName)
            local groupId = subTabInfo.groupId
            CommonDefs.ListIterate(groupId,DelegateFactory.Action_object(function(k)
                table.insert( data_list,k )
            end))
        else
            local groupId = tabInfo.groupId
            CommonDefs.ListIterate(groupId,DelegateFactory.Action_object(function(k)
                table.insert( data_list,k )
            end))
        end
    end
    for k, v in ipairs(data_list) do
        local cellList = CommonDefs.DictGetValue(CAchievementMgr.Inst.m_GroupList, typeof(UInt32), v)
        if(cellList)then
            for i=1,cellList.Count do
                local id = cellList[i - 1]
                local achievement = Achievement_Achievement.GetData(id)
                if (achievement ~= nil and CClientMainPlayer.Inst.PlayProp.Achievement:HasAchievement(id) and not CClientMainPlayer.Inst.PlayProp.Achievement:HasAwardFinished(id)) then --未领取成就
                    available = true
                    break
                end
            end
        end
        if(available)then
            break
        end
    end
    return available
end

function CLuaPlayerAchievementView:OnSeaFishingBtnClick(go)
    CUIManager.ShowUI(CLuaUIResources.SeaFishingTuJianWnd)
end

function CLuaPlayerAchievementView:OnZhuXianButtonClick(go)
    CUIManager.ShowUI(CLuaUIResources.ZhuXianAchievementWnd)
end

function CLuaPlayerAchievementView:OnHaoGanDuButtonClick(go)
    CUIManager.ShowUI(CLuaUIResources.NpcHaoGanDuWnd)
end

function CLuaPlayerAchievementView:OnAchieveTabClick(achieveTabName)
    LuaAchievementMgr:ShowDetailWndByAchievementByTabName(achieveTabName)
end

function CLuaPlayerAchievementView:Init()
    self:InitAchieveButtons()

    -- Init Overview]
    self:InitRecommendation()

    local ownCount, starCount = self:GetAchievementOwnCount()
    local totalCount = CAchievementMgr.Inst.m_GroupList.Count
    self.m_AchieveCountLabel.text =  SafeStringFormat3("%d/%d", ownCount, totalCount)
    self.m_AchieveProgressBar.fillAmount = ownCount/totalCount
    local starTextureName = ""
    if starCount>300 then
        starTextureName = "raw_mainplayerwnd_achievement_star_06.mat"
    elseif starCount>180 then
        starTextureName = "raw_mainplayerwnd_achievement_star_05.mat"
    elseif starCount>100 then
        starTextureName = "raw_mainplayerwnd_achievement_star_04.mat"
    elseif starCount >40 then
        starTextureName = "raw_mainplayerwnd_achievement_star_03.mat"
    elseif starCount >0 then
        starTextureName = "raw_mainplayerwnd_achievement_star_02.mat"
    else
        starTextureName = "raw_mainplayerwnd_achievement_star_01.mat"
    end
    self.m_StarCountTexture:LoadMaterial("UI/Texture/Transparent/Material/"..starTextureName)
    self.m_StarCountLabel.text = tostring(starCount)
end

function CLuaPlayerAchievementView:InitAchieveButtons()
    local tabNames = {}

    if not self.m_AchieveTabNameTbl then
        self.m_AchieveTabNameTbl = {LocalString.GetString("个人成长"),
                    LocalString.GetString("系统玩法"),
                    LocalString.GetString("灵兽日记"),
                    LocalString.GetString("其他成就"),
                    LocalString.GetString("任务剧情"),
                    LocalString.GetString("家园成就"),
                    LocalString.GetString("开宗立派")}
    end

    for __,tabName in pairs(self.m_AchieveTabNameTbl) do
        local element ={}
        element.name = tabName
        if CommonDefs.DictContains_LuaCall(CAchievementMgr.Inst.m_TabNameList, tabName) then
            local totalCount,finishCount,finishButNotAwardedCount = LuaAchievementMgr:GetAchievementsInfoByTabName(tabName)
            element.totalCount = totalCount
            element.finishedCount = finishCount
            element.finishButNotAwardedCount = finishButNotAwardedCount
        else
            local totalCount,finishCount,finishButNotAwardedCount = 0,0,0
            element.totalCount = totalCount
            element.finishedCount = finishCount
            element.finishButNotAwardedCount = finishButNotAwardedCount 
        end
        table.insert(tabNames,element)
    end

    local i = 0
    local n = self.m_AchieveTabsRoot.childCount --这里和策划有个隐含约定，成就分组固定显示前面的n个，妖鬼狐包括后续增加的其他不会显示
    for _,data in pairs(tabNames) do
        if i<n then
            local child = self.m_AchieveTabsRoot:GetChild(i)
            self:InitOneAchieveButton(child, data)
            i = i+1
        else
            break
        end
    end

    while i<n do
        self.m_AchieveTabsRoot:GetChild(i).gameObject:SetActive(false)
        i = i+1
    end
    --刷新一键领奖按钮状态
    self:CheckAwardsAvailability()
    --海钓图鉴按钮
    self.m_SeaFishingBtn:SetActive(LuaSeaFishingMgr.s_SeaFishingSwith)
end

function CLuaPlayerAchievementView:InitOneAchieveButton(childTransform, data)
    if data.name == LocalString.GetString("开宗立派") and not LuaInviteNpcMgr.NeedShowAchievement then
        childTransform.gameObject:SetActive(false)
        self.transform:Find("Sprite (6)").gameObject:SetActive(false)
        return 
    end
    local bg = childTransform:GetComponent(typeof(CUITexture))
    local nameTexture = childTransform:Find("NameLabel"):GetComponent(typeof(UITexture))
    local valueLabel = childTransform:Find("ValueLabel"):GetComponent(typeof(UILabel))
    local fx = childTransform:Find("Fx"):GetComponent(typeof(CUIFx))
    local lightFxGo = childTransform:Find("LightFx").gameObject
    local tabName = data.name
    if data.finishButNotAwardedCount>0 then
        local highlightColor = NGUIText.ParseColor24("fffed7", 0)
        nameTexture.color = highlightColor
        valueLabel.color = highlightColor
        bg:LoadMaterial("UI/Texture/Transparent/Material/raw_mainplayerwnd_achievement_but_01_highlight.mat")
        fx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
        local widget = bg:GetComponent(typeof(UIWidget))
        CUIFx.DoAni(Vector4(-widget.width * 0.5, widget.height * 0.5, widget.width, widget.height), false, fx)
    else
        local normalColor = NGUIText.ParseColor24("ac533b", 0)
        nameTexture.color = normalColor
        valueLabel.color = normalColor
        bg:LoadMaterial("UI/Texture/Transparent/Material/raw_mainplayerwnd_achievement_but_01_normal.mat")
        fx:DestroyFx()
    end
    lightFxGo:SetActive(data.finishedCount>0)
    valueLabel.text = SafeStringFormat3("%d/%d", data.finishedCount, data.totalCount)
    UIEventListener.Get(childTransform.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnAchieveTabClick(tabName)
    end)
end

function CLuaPlayerAchievementView:InitRecommendation()
    Extensions.RemoveAllChildren(self.m_RecommendTable.transform)
    --[[
        1. 尚未达成的成就
        2. 优先级>0的成就
        3. 阈值<=0(不填)表示没有阈值
        4. 满足阈值的成就，不填表示满足
        5. 优先级数值越小越靠前，相同优先级下比较编号大小
        6. 每个Group下只推荐一个
    ]]
    local tbl = {}
    CommonDefs.DictIterate(CAchievementMgr.Inst.m_GroupList, DelegateFactory.Action_object_object(function (groupId, achievements)

        local achievementId, achievement = LuaAchievementMgr:GetCurrentNoneAwardedAchievement(groupId)

        if achievementId>0 and achievement and not CClientMainPlayer.Inst.PlayProp.Achievement:HasAchievement(achievementId) then
            --尚未完成的
            if achievement.Priority>0 and (achievement.Threshold <= 0 or LuaAchievementMgr:GetAchievementProgress(achievementId) >= achievement.Threshold) then
                --优先级大于0并且满足阈值要求
                table.insert(tbl,{id=achievementId, group=groupId, templateData=achievement})
            end
        end
    end))

    table.sort(tbl, function (data1, data2)
        if data1.templateData.Priority<data2.templateData.Priority then
            return true
        elseif data1.templateData.Priority>data2.templateData.Priority then
            return false
        else
            return data1.id<data2.id
        end
    end)

    --选取排序后的前三项
    local i = 0
    for __,data in pairs(tbl) do
        local instance = CUICommonDef.AddChild(self.m_RecommendTable.gameObject, self.m_RecommendTemplate)
        instance:SetActive(true)
        self:InitRecommendationItem(instance, data.id, data.group, data.templateData)
        i = i+1
        if i==3 then break end
    end
    self.m_RecommendTable:Reposition()
end

function CLuaPlayerAchievementView:InitRecommendationItem(go, achievementId, groupId, achievement)
    local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local progressLabel = go.transform:Find("ProgressLabel"):GetComponent(typeof(UILabel))
    local starTable = go.transform:Find("Stars"):GetComponent(typeof(UITable))
    local starTemplate = go.transform:Find("StarTemplate").gameObject
    local awardIcon = go.transform:Find("ProgressBar/AwardIcon"):GetComponent(typeof(UISprite))
    local awardYuanBaoLabel = go.transform:Find("ProgressBar/AwardIcon/YuanBaoLabel"):GetComponent(typeof(UILabel))
    local progressSprite = go.transform:Find("ProgressBar"):GetComponent(typeof(UIWidget))
    local conditionLabel = go.transform:Find("ConditionLabel"):GetComponent(typeof(UILabel))

    starTemplate:SetActive(false)
    CommonDefs.AddOnClickListener(progressSprite.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnAchievementAwardItemClick(go, achievementId) end), false)

    local curLevel = LuaAchievementMgr:GetFinishedAchievementCount(groupId)

    nameLabel.text = achievement.Name
    conditionLabel.text = achievement.RequirementDisplay

    --目前仅奖励元宝或者称号
    awardIcon.gameObject:SetActive(true)
    awardYuanBaoLabel.text = ""
    if achievement.YuanBaoReward>0 then
        if achievement.TitleReward > 0 then
            --奖励元宝和称号
            awardIcon.spriteName = "packagewnd_chenghaoandyuanbao"
        else
            --奖励元宝
            awardIcon.spriteName = "packagewnd_yuanbao"
        end
        awardYuanBaoLabel.text = tostring(achievement.YuanBaoReward)
    elseif achievement.TitleReward > 0 then
        --奖励称号
        awardIcon.spriteName = "packagewnd_chenghao"
    else
        --没有奖励
        awardIcon.gameObject:SetActive(false)
    end

    --成就进度
    local progress = LuaAchievementMgr:GetAchievementProgress(achievementId)
    local totalProgress = 0
    if not System.String.IsNullOrEmpty(achievement.NeedProgress) then
        local strs = CommonDefs.StringSplit_ArrayChar(CommonDefs.StringSplit_ArrayChar(achievement.NeedProgress, ";")[0], ",")
        totalProgress = System.Int32.Parse(strs[strs.Length - 1])
    end

    --此处应该均为未达成的成就

    self:InitProgress(progressLabel, progressSprite, progress, totalProgress)

    self:InitStar(starTable, starTemplate, LuaAchievementMgr:GetGroupAchievements(groupId).Count, curLevel)
end

function CLuaPlayerAchievementView:InitProgress(progressLabel, progressSprite, progress, totalProgress)
    if (math.floor(progress * 10)) % 10 > 0 then --从之前的逻辑拷贝过来，意义未明
        progressLabel.text = System.String.Format("{0}/{1}", NumberComplexToString(progress, typeof(Single), "F1"), totalProgress>0 and totalProgress or 1)
    else
        progressLabel.text = System.String.Format("{0}/{1}", math.floor(progress), totalProgress>0 and totalProgress or 1)
    end
    
    if totalProgress ~= 0 then
        progressSprite.fillAmount = progress / totalProgress
    else
        progressSprite.fillAmount = 0
    end
end

function CLuaPlayerAchievementView:InitStar(starTable, starTemplate, totalCount, finishedCount)
    Extensions.RemoveAllChildren(starTable.transform)
    for i=0, totalCount-1 do
        local instance = CUICommonDef.AddChild(starTable.gameObject,  starTemplate)
        instance:GetComponent(typeof(UISprite)).spriteName = i < finishedCount and AllSpriteNames.achievement_star_highlight or AllSpriteNames.achievement_star_normal
        instance:SetActive(true)
    end
    starTable:Reposition()
end

function CLuaPlayerAchievementView:OnAchievementAwardItemClick(go, achievementId)
    LuaAchievementMgr:ShowAwardsTip(achievementId, true, go.transform.position, 120, 120)
end


function CLuaPlayerAchievementView:GetAchievementOwnCount()
    local ownCount = 0
    local starCount = 0
    CommonDefs.DictIterate(CAchievementMgr.Inst.m_GroupList, DelegateFactory.Action_object_object(function (___key, ___value) 
        local group = {}
        group.Key = ___key
        group.Value = ___value
        local hasAchieved = true
        do
            local j = 0
            while j < group.Value.Count do
                if not CClientMainPlayer.Inst.PlayProp.Achievement:HasAchievement(group.Value[j]) then
                    hasAchieved = false
                else
                    starCount = starCount + 1
                end
                j = j + 1
            end
        end
        if hasAchieved then
            ownCount = ownCount + 1
        end
    end))
    return ownCount, starCount
end


function CLuaPlayerAchievementView:GetAchievementName( key) 
    local a = Achievement_Achievement.GetData(key)
    if a ~= nil and CommonDefs.DictContains(CAchievementMgr.Inst.m_GroupList, typeof(UInt32), a.Group) then
        local cellList = CommonDefs.DictGetValue(CAchievementMgr.Inst.m_GroupList, typeof(UInt32), a.Group)
        local level = CommonDefs.ListIndexOf(cellList, key) + 1
        return SafeStringFormat3(LocalString.GetString("%s(%d星)"), a.Name, level)
    else
        return ""
    end
end

function CLuaPlayerAchievementView:CheckZhuXianAchievementAvailability() -- 查询主线任务是否有未领取奖励
    local tabName = LocalString.GetString("妖鬼狐")
    local subTabName = LocalString.GetString("妖鬼狐")
    local data_list = {}
    local available = false
    if CommonDefs.DictContains(CAchievementMgr.Inst.m_TabNameList, typeof(String), tabName) then
        local tabInfo = CommonDefs.DictGetValue(CAchievementMgr.Inst.m_TabNameList, typeof(String), tabName)
        if CommonDefs.DictContains(tabInfo.subTabName, typeof(String), subTabName) then
            local subTabInfo = CommonDefs.DictGetValue(tabInfo.subTabName, typeof(String), subTabName)
            local groupId = subTabInfo.groupId
            CommonDefs.ListIterate(groupId,DelegateFactory.Action_object(function(k)
                table.insert( data_list,k )
            end))
        else
            local groupId = tabInfo.groupId
            CommonDefs.ListIterate(groupId,DelegateFactory.Action_object(function(k)
                table.insert( data_list,k )
            end))
        end
    end
    for k, v in ipairs(data_list) do
        local cellList = CommonDefs.DictGetValue(CAchievementMgr.Inst.m_GroupList, typeof(UInt32), v)
        if(cellList)then
            for i=1,cellList.Count do
                local id = cellList[i - 1]
                local achievement = Achievement_Achievement.GetData(id)
                if (achievement ~= nil and CClientMainPlayer.Inst.PlayProp.Achievement:HasAchievement(id) and not CClientMainPlayer.Inst.PlayProp.Achievement:HasAwardFinished(id)) then --未领取成就
                    available = true
                    break
                end
            end
        end
        if(available)then
            break
        end
    end
    return available
end

function CLuaPlayerAchievementView:OnEnable()
    self:HideOriginalBg()
    self:Init()
    g_ScriptEvent:AddListener("GetAchievementAwardSuccess",self,"OnAchievementUpdate")  
    g_ScriptEvent:AddListener("OnPlotCommentShowRedAlert",self,"OnPlotCommentShowRedAlert")
    local isEnable = LuaPlotDiscussionMgr:IsOpen()
    if isEnable then
        LuaPlotDiscussionMgr:RequestRedDotInfo()
    end
end
function CLuaPlayerAchievementView:OnDisable()
    self:ShowOriginalBg()
    g_ScriptEvent:RemoveListener("GetAchievementAwardSuccess",self,"OnAchievementUpdate")   
    g_ScriptEvent:RemoveListener("OnPlotCommentShowRedAlert",self,"OnPlotCommentShowRedAlert") 
end

function CLuaPlayerAchievementView:OnPlotCommentShowRedAlert(haoYiXingCardId, yaoGuiHuCardId, isClear)
    if isClear then
        self.m_ZhuXianButtonAlertSprite.gameObject:SetActive(false)
    end
    if yaoGuiHuCardId ~= 0 then
        self.m_ZhuXianButtonAlertSprite.gameObject:SetActive(true)
    end
end

--隐藏原始背景窗口
function CLuaPlayerAchievementView:HideOriginalBg()
    local wndRoot = self.transform.parent.parent
    if wndRoot then
        local wndbg = wndRoot:Find("Wnd_Bg_Primary_Tab"):GetComponent(typeof(UIWidget))
        wndbg.enabled = false
        local title = wndRoot:Find("Wnd_Bg_Primary_Tab/TitleLabel"):GetComponent(typeof(UIWidget))
        title.gameObject:SetActive(false)
    end
end
--显示原始背景窗口
function CLuaPlayerAchievementView:ShowOriginalBg()
    local wndRoot = self.transform.parent.parent
    if wndRoot then
        local wndbg = wndRoot:Find("Wnd_Bg_Primary_Tab"):GetComponent(typeof(UIWidget))
        wndbg.enabled = true
        local title = wndRoot:Find("Wnd_Bg_Primary_Tab/TitleLabel"):GetComponent(typeof(UIWidget))
        title.gameObject:SetActive(true)
    end
end

function CLuaPlayerAchievementView:GetAllAward(go) 
    local list = {}--CreateFromClass(MakeGenericClass(List, UInt32))
    Achievement_Achievement.ForeachKey(function (key) 
        if CClientMainPlayer.Inst.PlayProp.Achievement:HasAchievement(key) and not CClientMainPlayer.Inst.PlayProp.Achievement:HasAwardFinished(key) then
            -- CommonDefs.ListAdd(list, typeof(UInt32), key)
            local data = Achievement_Achievement.GetData(key)
            if data.NotShowMessage ~= 1 then
                table.insert( list,key )
            end
        end
    end)
    if #list == 0 then
        return
    end
    if #list == 1 then
        Gac2Gas.PlayerGetAchievementAward(list[1], "")
        return
    end
    local name1 = self:GetAchievementName(list[1])
    local name2 = self:GetAchievementName(list[2])

    local text = g_MessageMgr:FormatMessage("Achievment_Get_Multiple_Notice", name1, name2, #list)
    MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(function () 
        for i,v in ipairs(list) do
            Gac2Gas.PlayerGetAchievementAward(v, "")
        end
    end), nil, nil, nil, false)
end

function CLuaPlayerAchievementView:OnAchievementUpdate(args)
    self:UpdateZhuXianButtonFx()
    self:InitAchieveButtons()
end
