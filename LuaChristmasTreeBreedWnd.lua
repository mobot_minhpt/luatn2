local UIProgressBar=import "UIProgressBar"

local CBaseWnd = import "L10.UI.CBaseWnd"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local CUIManager = import "L10.UI.CUIManager"
local UICommonDef = import "L10.UI.CUICommonDef"
local Extensions = import "Extensions"
local Color = import "UnityEngine.Color"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local UISprite = import "UISprite"
local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CChatLinkMgr = import "CChatLinkMgr"
local CButton = import "L10.UI.CButton"
local CYaoYiYaoMgr = import "L10.Game.CYaoYiYaoMgr"
local EnumShakeType = import "L10.Game.CYaoYiYaoMgr+EnumShakeType"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local UITexture = import "UITexture"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientNpc = import "L10.Game.CClientNpc"

LuaChristmasTreeBreedWnd = class()

RegistClassMember(LuaChristmasTreeBreedWnd,"m_ChritmasTreeLevelLabel")
RegistClassMember(LuaChristmasTreeBreedWnd,"m_RuleTable")
RegistClassMember(LuaChristmasTreeBreedWnd,"m_TextTemplate")
RegistClassMember(LuaChristmasTreeBreedWnd,"m_ItemTable")
RegistClassMember(LuaChristmasTreeBreedWnd,"m_ItemTemplate")
RegistClassMember(LuaChristmasTreeBreedWnd,"m_ItemTitle")
RegistClassMember(LuaChristmasTreeBreedWnd,"m_CountDown")

RegistClassMember(LuaChristmasTreeBreedWnd,"m_RewardButton")
RegistClassMember(LuaChristmasTreeBreedWnd,"m_CloseButton")

RegistClassMember(LuaChristmasTreeBreedWnd,"m_RuleList")
RegistClassMember(LuaChristmasTreeBreedWnd,"m_ItemList")

--[[
ItemList[i] = {
gameObject,
ItemID, 
current, 
total, 
today, 
NameLabel,
IconObject,
ItemCUITexture,
SubmitNumberLabel, 
ProgressBar, 
ProgressNumberLabel, 
Button
}
--]]

RegistClassMember(LuaChristmasTreeBreedWnd,"m_TopLevel")
RegistClassMember(LuaChristmasTreeBreedWnd,"m_TargetTime")
RegistClassMember(LuaChristmasTreeBreedWnd,"m_CountDownAction")

RegistClassMember(LuaChristmasTreeBreedWnd,"m_CurLevel")    --当前树等级
RegistClassMember(LuaChristmasTreeBreedWnd,"m_ProgressTbl") --当前树道具提交进度
RegistClassMember(LuaChristmasTreeBreedWnd,"m_GiftStatus")  --当前圣诞树对应的领奖状态 0表示无奖,1表示可以领(仍然需要到时间才能领), 2表示今天领取过了
RegistClassMember(LuaChristmasTreeBreedWnd,"m_SubmitTbl")   --当前树道具今天玩家提交进度
RegistClassMember(LuaChristmasTreeBreedWnd,"m_RaiseLevelUpNeedCount") -- 每一级圣诞树升级所需要的3种道具的数量信息

RegistClassMember(LuaChristmasTreeBreedWnd,"m_ModelTextureName")
RegistClassMember(LuaChristmasTreeBreedWnd,"m_ModelTextureLoader")
RegistClassMember(LuaChristmasTreeBreedWnd,"m_ModelTexture")

function LuaChristmasTreeBreedWnd:Init()

    self.m_ChritmasTreeLevelLabel = self.transform:Find("Anchor/XmasTreeView/TreeLable/LevelLabel"):GetComponent(typeof(UILabel))
    self.m_RuleTable = self.transform:Find("Anchor/RuleView/ScrollView/RuleTable").gameObject
    self.m_TextTemplate = self.transform:Find("Anchor/RuleView/ScrollView/TextTemplate").gameObject
    self.m_ItemTable = self.transform:Find("Anchor/UpgradeItem/ItemTable").gameObject
    self.m_ItemTemplate = self.transform:Find("Anchor/UpgradeItem/ItemTemplate").gameObject
    self.m_ItemTitle = self.transform:Find("Anchor/UpgradeItem/Title"):GetComponent(typeof(UILabel))
    self.m_CountDown = self.transform:Find("Anchor/XmasTreeView/CountDown").gameObject
    self.m_ModelTexture = self.transform:Find("Anchor/ModelTexture"):GetComponent(typeof(UITexture))
    self.m_RewardButton = self.transform:Find("Anchor/XmasTreeView/RewardButton"):GetComponent(typeof(CButton))
    self.m_CloseButton = self.transform:Find("Wnd_Bg_Secondary/CloseButton"):GetComponent(typeof(CButton))

    CommonDefs.AddOnClickListener(self.m_CloseButton.gameObject, DelegateFactory.Action_GameObject(
        function(go)
            self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
            CUIManager.CloseUI("ChristmasTreeAddItemWnd")
        end
    ),false)

    CommonDefs.AddOnClickListener(self.m_RewardButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnRewardButtonClick() end), false)

    --初始化圣诞树升级所需道具数量信息
    self.m_RaiseLevelUpNeedCount = {}
    ShengDan_RaiseTree.Foreach(function (level, data)
        self.m_RaiseLevelUpNeedCount[level] = {}
        for needCount in string.gmatch(data.NeedCount, "(%d+)") do
            needCount = tonumber(needCount)
            table.insert(self.m_RaiseLevelUpNeedCount[level], needCount)
        end
    end)

    ---圣诞树的满级（读表）
    self.m_TopLevel = ShengDan_RaiseTree.GetDataCount()

    ---可领奖时间设置为当天19点
    self.m_TargetTime = CServerTimeMgr.Inst:GetTodayTimeStampByHours(ShengDan_Setting.GetData().CanGetRewardFromTreeHour)

    --- 从模板生成规则列表（读策划表）
    self:ClearRuleTable()
    self:InitRuleTable()
    --- 从模板生成道具列表（读策划表）
    self:ClearItemTable()
    self:InitItemTable()
    --- 初始化值
    self.m_CurLevel = 0
    self.m_ProgressTbl = {}
    self.m_GiftStatus = 0
    self.m_SubmitTbl = {}


    --初始化模型
    self.m_ModelTextureName = "__ChristmasTreeBreedWnd__"
    if not self.m_ModelTextureLoader then
        self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
            self:LoadModel(ro)
        end)
    end

    self:UpdateTreeLevel()
    self:UpdateItemProgress()
    self:UpdateCountDown()


    Gac2Gas.QueryChristmasTreeInfo()
end

function LuaChristmasTreeBreedWnd:InitRuleTable()
    self.m_RuleList={}
    for i=1,3 do
        local curObject=UICommonDef.AddChild(self.m_RuleTable,self.m_TextTemplate)
        curObject:SetActive(true)
        self.m_RuleList[i]={}
        self.m_RuleList[i].gameObject=curObject
        self.m_RuleList[i].TitleLabel=curObject.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
        self.m_RuleList[i].ContentLabel=curObject.transform:Find("ContentLabel"):GetComponent(typeof(UILabel))

        CommonDefs.AddOnClickListener(self.m_RuleList[i].ContentLabel.gameObject, DelegateFactory.Action_GameObject(
            function(go)
                local label = go.transform:GetComponent(typeof(UILabel))
                local url = label:GetUrlAtPosition(UICamera.lastWorldPosition)
                if url then
                    CChatLinkMgr.ProcessLinkClick(url,nil)
                end
            end
        ),false)
    end
    self.m_TextTemplate:SetActive(false)
    self.m_RuleTable:GetComponent(typeof(UITable)):Reposition()
end

function LuaChristmasTreeBreedWnd:ClearRuleTable()
    Extensions.RemoveAllChildren(self.m_RuleTable.transform)
    self.m_RuleList={}
end

function LuaChristmasTreeBreedWnd:InitItemTable()
    local itemIDSplited = {}

    for v,k in string.gmatch(ShengDan_Setting.GetData().Item2Idx, "(%d+),(%d+)") do
        itemIDSplited[tonumber(k)]=tonumber(v)
    end

    self.m_ItemList={}
    for i=1,3 do
        local curObject=UICommonDef.AddChild(self.m_ItemTable,self.m_ItemTemplate)
        curObject:SetActive(true)
        self.m_ItemList[i]={}
        self.m_ItemList[i].gameObject=curObject
        self.m_ItemList[i].ItemCUITexture = curObject.transform:Find("Sprite/Item"):GetComponent(typeof(CUITexture))
        self.m_ItemList[i].NameLabel=curObject.transform:Find("Labels/NameLabel"):GetComponent(typeof(UILabel))
        self.m_ItemList[i].IconObject=curObject.transform:Find("Sprite").gameObject
        self.m_ItemList[i].SubmitNumberLabel=curObject.transform:Find("Labels/SubmitNumberLabel"):GetComponent(typeof(UILabel))
        self.m_ItemList[i].ProgressBar=curObject.transform:Find("ProgressBar"):GetComponent(typeof(UIProgressBar))
        self.m_ItemList[i].ProgressNumberLabel=curObject.transform:Find("ProgressBar/ProgressNumberLabel"):GetComponent(typeof(UILabel))
        
        --读表加载升级物品图标和物品名
        self.m_ItemList[i].ItemID = itemIDSplited[i]

        local ItemData = Item_Item.GetData(self.m_ItemList[i].ItemID)
        if ItemData then
            self.m_ItemList[i].ItemCUITexture:LoadMaterial(ItemData.Icon)
            self.m_ItemList[i].NameLabel.text=ItemData.Name
        end


        --注册按钮点击事件
        self.m_ItemList[i].Button=curObject.transform:Find("Button").gameObject
        CommonDefs.AddOnClickListener(self.m_ItemList[i].Button,DelegateFactory.Action_GameObject(
            function()
                --计算窗口的位置
                local butPosition = self.m_ItemList[i].Button.transform.position
                local butWidth = self.m_ItemList[i].Button:GetComponent(typeof(UISprite)).width
                local butHeight = self.m_ItemList[i].Button:GetComponent(typeof(UISprite)).height
                LuaChristmasTreeAddItemMgr.ShowWnd(self.m_ItemList[i].ItemID,butPosition, butWidth, butHeight + 12, AlignType.Top)
            end
        ),false)

        --注册图标点击事件
        CommonDefs.AddOnClickListener(self.m_ItemList[i].IconObject,DelegateFactory.Action_GameObject(
            function()
                CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_ItemList[i].ItemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),false)

    end
    self.m_ItemTemplate:SetActive(false)
    self.m_ItemTable:GetComponent(typeof(UIGrid)):Reposition()
end

function LuaChristmasTreeBreedWnd:ClearItemTable()
    Extensions.RemoveAllChildren(self.m_ItemTable.transform)
    self.m_ItemList={}
end

function LuaChristmasTreeBreedWnd:UpdateTreeLevel()
    self.m_CurLevel = math.min(self.m_CurLevel or 0, self.m_TopLevel)
    self.m_ChritmasTreeLevelLabel.text = "lv."..tostring(self.m_CurLevel)

    if self.m_CurLevel == 0 then return end

    self.m_RuleList[1].TitleLabel.text = LocalString.GetString("摇奖规则")
    self.m_RuleList[1].ContentLabel.text = CChatLinkMgr.TranslateToNGUIText(ShengDan_RaiseTree.GetData(self.m_CurLevel).Message, false)
    self.m_RuleList[2].TitleLabel.text =LocalString.GetString("本级奖励")
    self.m_RuleList[2].ContentLabel.text = CChatLinkMgr.TranslateToNGUIText(ShengDan_RaiseTree.GetData(self.m_CurLevel).RewardMessage, false)
    
    -- 满级的时候特殊处理
    if self.m_CurLevel < self.m_TopLevel then
        self.m_RuleList[3].TitleLabel.text = LocalString.GetString("下级奖励")
        self.m_RuleList[3].ContentLabel.text = CChatLinkMgr.TranslateToNGUIText(ShengDan_RaiseTree.GetData(self.m_CurLevel + 1).RewardMessage, false)
        self.m_RuleList[3].ContentLabel.alpha = 1
        self.m_ItemTitle.text=LocalString.GetString("升级所需道具")
    else
        self.m_RuleList[3].TitleLabel.text = LocalString.GetString("下级奖励")
        self.m_RuleList[3].ContentLabel.text=LocalString.GetString("已满级")
        self.m_RuleList[3].ContentLabel.alpha = 0.5
        self.m_ItemTitle.text=LocalString.GetString("圣诞树已满级")
    end

    self.m_RuleTable:GetComponent(typeof(UITable)):Reposition()

    --更新圣诞树模型
    if not self.m_ModelTextureName then return end
    local tx = CUIManager.CreateModelTexture(self.m_ModelTextureName, self.m_ModelTextureLoader, 180, 0.05, -1, 4.66, false, true, ShengDan_RaiseTree.GetData(self.m_CurLevel).Scale)
    self.m_ModelTexture.mainTexture = tx
end

function LuaChristmasTreeBreedWnd:LoadModel(ro)
    local obj = CClientObjectMgr.Inst:GetObject(LuaChristmasMgr.ChristmasTreeBreedNpcId)
    if obj and TypeIs(obj, typeof(CClientNpc)) then
        CClientNpc.LoadResource(ro, obj.TemplateId, false)
    end
end

function LuaChristmasTreeBreedWnd:UpdateItemProgress()
    local treeLevel = self.m_CurLevel
    local progressTbl = self.m_ProgressTbl
    local submitTbl = self.m_SubmitTbl
    for i=1,3 do
        local progressNum = progressTbl and progressTbl[i] or 0
        local submitNum = submitTbl and submitTbl[i] or 0
        local needNum = self.m_RaiseLevelUpNeedCount and self.m_RaiseLevelUpNeedCount[treeLevel] and self.m_RaiseLevelUpNeedCount[treeLevel][i] or 0
        if needNum == 0 then needNum = 65536 end
        self.m_ItemList[i].SubmitNumberLabel.text = tostring(submitNum)
        self.m_ItemList[i].ProgressBar.value = progressNum/needNum
        self.m_ItemList[i].ProgressNumberLabel.text = SafeStringFormat3("%d/%d", progressNum, needNum)

        if submitNum > 0 then
            self.m_ItemList[i].SubmitNumberLabel.color = Color.white
        else
            self.m_ItemList[i].SubmitNumberLabel.color = Color.red
        end
    end
end

function LuaChristmasTreeBreedWnd:UpdateCountDown()
    --更新倒计时,以及判断可否领取奖励
    if not self.m_TargetTime then return end
    local intervalSeconds = self.m_TargetTime - CServerTimeMgr.Inst.timeStamp
    intervalSeconds=math.max(0,intervalSeconds)

    local hours = math.floor(intervalSeconds / 3600)
	local minutes = math.floor((intervalSeconds - hours * 3600) / 60)
    local seconds = intervalSeconds-hours*3600-minutes*60

    self.m_RewardButton.Text = (self.m_GiftStatus == 2 and LocalString.GetString("今日已领奖")) or LocalString.GetString("摇取奖励")

    if intervalSeconds ~= 0 then
        self.m_CountDown:SetActive(true)
        self.m_CountDown:GetComponent(typeof(UILabel)).text
            = SafeStringFormat3("%02d:%02d:%02d", hours, minutes, seconds)
        self.m_RewardButton.Enabled = false
    else
        self.m_CountDown:SetActive(false)
        if self.m_GiftStatus == 0 then
            -- 没资格领奖的操作
            self.m_RewardButton.Enabled = false
        elseif self.m_GiftStatus == 1 then
            self.m_RewardButton.Enabled = true
        elseif self.m_GiftStatus == 2 then
            self.m_RewardButton.Enabled = false
        else
            self.m_RewardButton.Enabled = false
        end
    end
end

function LuaChristmasTreeBreedWnd:OnRewardButtonClick()
    CUIManager.CloseUI("ChristmasTreeBreedWnd")
    CYaoYiYaoMgr.Inst:ShowWndByType(LocalString.GetString("摇动你的手机，看看有什么宝贝从圣诞树上掉下来吧"), nil, -1, EnumShakeType.YuanDanLottery, DelegateFactory.Action(function ( ... )
        LuaChristmasMgr.GetGiftFromChristmasTree()
    end))
end

function LuaChristmasTreeBreedWnd:StartCountDown()
    self:StopCountDown()
    self:UpdateCountDown()
    self.m_CountDownAction = CTickMgr.Register(DelegateFactory.Action(function () self:UpdateCountDown() end),
        1*1000,ETickType.Loop)
end

function LuaChristmasTreeBreedWnd:StopCountDown()
    if self.m_CountDownAction then
        invoke(self.m_CountDownAction)
        self.m_CountDownAction = nil
    end
end

function LuaChristmasTreeBreedWnd:OnEnable()
    --开始倒计时
    self:StartCountDown()
    --注册事件
    g_ScriptEvent:AddListener("OnSyncChristmasTreeInfo",self,"OnSyncChristmasTreeInfo")
    g_ScriptEvent:AddListener("OnSyncChristmasTreeProgressChanged",self,"OnSyncChristmasTreeProgressChanged")
    g_ScriptEvent:AddListener("OnSyncChristmasTreeLevelUp",self,"OnSyncChristmasTreeLevelUp")
end

function LuaChristmasTreeBreedWnd:OnDisable()
    self:StopCountDown()
    --取消注册事件
    g_ScriptEvent:RemoveListener("OnSyncChristmasTreeInfo",self,"OnSyncChristmasTreeInfo")
    g_ScriptEvent:RemoveListener("OnSyncChristmasTreeProgressChanged",self,"OnSyncChristmasTreeProgressChanged")
    g_ScriptEvent:RemoveListener("OnSyncChristmasTreeLevelUp",self,"OnSyncChristmasTreeLevelUp")
end

function LuaChristmasTreeBreedWnd:OnSyncChristmasTreeInfo(treeLevel, progressTbl, rewardStatus, submitTbl)

    self.m_CurLevel = treeLevel
    self.m_ProgressTbl = progressTbl
    self.m_GiftStatus = rewardStatus
    self.m_SubmitTbl = submitTbl

    self:UpdateTreeLevel()
    self:UpdateItemProgress()
    self:UpdateCountDown() -- 避免初始延迟
end

function LuaChristmasTreeBreedWnd:OnSyncChristmasTreeProgressChanged(level, idx, newProgress, newSubmitTimes, rewardStatus)

    if self.m_CurLevel ~= level then return end
    if self.m_ProgressTbl then
        self.m_ProgressTbl[idx] = newProgress
    end
    if self.m_SubmitTbl then
        self.m_SubmitTbl[idx] = newSubmitTimes
    end
    self.m_GiftStatus = rewardStatus

    self:UpdateItemProgress()
end

function LuaChristmasTreeBreedWnd:OnSyncChristmasTreeLevelUp(oriTreeLevel, newTreeLevel, newNpcEngineId, progressTbl, rewardStatus, submitTbl)

    if self.m_CurLevel~= oriTreeLevel then return end
    self.m_CurLevel = newTreeLevel
    self.m_ProgressTbl = progressTbl
    self.m_GiftStatus = rewardStatus
    self.m_SubmitTbl = submitTbl

    self:UpdateTreeLevel()
    self:UpdateItemProgress()
end

function LuaChristmasTreeBreedWnd:OnDestroy()
    if self.m_ModelTextureName then
        CUIManager.DestroyModelTexture(self.m_ModelTextureName)
    end
end
