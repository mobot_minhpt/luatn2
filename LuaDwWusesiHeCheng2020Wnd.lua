local GameObject				= import "UnityEngine.GameObject"
local CUITexture				= import "L10.UI.CUITexture"
local CButton					= import "L10.UI.CButton"
local CUIFx						= import "L10.UI.CUIFx"
local QnAddSubAndInputButton	= import "L10.UI.QnAddSubAndInputButton"
local UILabel					= import "UILabel"
local DelegateFactory			= import "DelegateFactory"
local Extensions				= import "Extensions"
local CommonDefs				= import "L10.Game.CommonDefs"
local CItemMgr					= import "L10.Game.CItemMgr"
local EnumItemPlace				= import "L10.Game.EnumItemPlace"
local CItemInfoMgr				= import "L10.UI.CItemInfoMgr"
local AlignType					= import "L10.UI.CItemInfoMgr+AlignType"

--五色丝合成界面
CLuaDwWusesiHeCheng2020Wnd = class()

RegistChildComponent(CLuaDwWusesiHeCheng2020Wnd, "Item1",		GameObject)
RegistChildComponent(CLuaDwWusesiHeCheng2020Wnd, "Item2",		GameObject)
RegistChildComponent(CLuaDwWusesiHeCheng2020Wnd, "Item3",		GameObject)
RegistChildComponent(CLuaDwWusesiHeCheng2020Wnd, "Item4",		GameObject)
RegistChildComponent(CLuaDwWusesiHeCheng2020Wnd, "Item5",		GameObject)
RegistChildComponent(CLuaDwWusesiHeCheng2020Wnd, "TargetItem",	CUITexture)
RegistChildComponent(CLuaDwWusesiHeCheng2020Wnd, "Button",		CButton)
RegistChildComponent(CLuaDwWusesiHeCheng2020Wnd, "Fxnode",		CUIFx)
RegistChildComponent(CLuaDwWusesiHeCheng2020Wnd, "Fxnode2",		CUIFx)
RegistChildComponent(CLuaDwWusesiHeCheng2020Wnd, "NumInput",	QnAddSubAndInputButton)

RegistClassMember(CLuaDwWusesiHeCheng2020Wnd,"m_enable")
RegistClassMember(CLuaDwWusesiHeCheng2020Wnd,"m_items")
RegistClassMember(CLuaDwWusesiHeCheng2020Wnd, "delayTick")


function CLuaDwWusesiHeCheng2020Wnd:Init()
	self.m_items = {}
	local dwdata = DuanWu2020_Setting.GetData()
	
	self.m_items[tonumber(dwdata.DangSeSiId[0])] = self.Item1
	self.m_items[tonumber(dwdata.DangSeSiId[1])] = self.Item2
	self.m_items[tonumber(dwdata.DangSeSiId[2])] = self.Item3
	self.m_items[tonumber(dwdata.DangSeSiId[3])] = self.Item4
	self.m_items[tonumber(dwdata.DangSeSiId[4])] = self.Item5
	
	self:Repaint()
end

function CLuaDwWusesiHeCheng2020Wnd:AddItemClickEvent(itemgo,tid)
	local itemclick = function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(tid, false, nil, AlignType.Default, 0, 0, 0, 0) 
	end
	UIEventListener.Get(itemgo).onClick = DelegateFactory.VoidDelegate(itemclick)
end

function CLuaDwWusesiHeCheng2020Wnd:Repaint()
	local dwdata = DuanWu2020_Setting.GetData()
	local have1 = self:InitItem(self.Item1,tonumber(dwdata.DangSeSiId[0])) 
	local have2 = self:InitItem(self.Item2,tonumber(dwdata.DangSeSiId[1]))
	local have3 = self:InitItem(self.Item3,tonumber(dwdata.DangSeSiId[2]))
	local have4 = self:InitItem(self.Item4,tonumber(dwdata.DangSeSiId[3]))
	local have5 = self:InitItem(self.Item5,tonumber(dwdata.DangSeSiId[4]))
	local min = math.min(have1,have2,have3,have4,have5)
	self.m_enable = min > 0

	self.NumInput:SetInputEnabled(self.m_enable)
	self.NumInput:EnableButtons(self.m_enable)

	if self.m_enable then
		self.NumInput:SetMinMax(1,min,1)
		self.NumInput:SetValue(1,true)
	end
	local data = Item_Item.GetData(tonumber(dwdata.WuSeSiTemplateId))
	if data ~= nil then 
		local gray = not self.m_enable
		local trans = self.TargetItem.transform

		Extensions.SetLocalPositionZ(trans,gray and -1 or 0)

	end
end

--初始化一个道具
function CLuaDwWusesiHeCheng2020Wnd:InitItem(go,cid)
	local data = Item_Item.GetData(cid) 
	if data == nil then return 0 end
	
	local iconTex = go.transform:Find("Texture"):GetComponent(typeof(CUITexture))
    local ctTxt = go.transform:Find("Name"):GetComponent(typeof(UILabel))

	iconTex:LoadMaterial(data.Icon)

	local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag,cid)
	local str = tostring(count)
	if count <= 0 then
		str = "[c][ff0000]0[-]/1"
	else
		str = str.."/1"
	end
	ctTxt.text = str
	return count
end


function CLuaDwWusesiHeCheng2020Wnd:OnEnable()
	local submitClick = function(go)
		self:OnSubmitBtnClick()
	end
	CommonDefs.AddOnClickListener(self.Button.gameObject, DelegateFactory.Action_GameObject(submitClick), false)
	g_ScriptEvent:AddListener("SendItem", self, "OnPlayerItemChange")

	local dwdata = DuanWu2020_Setting.GetData()
	self:AddItemClickEvent(self.Item1,tonumber(dwdata.DangSeSiId[0]))
	self:AddItemClickEvent(self.Item2,tonumber(dwdata.DangSeSiId[1]))
	self:AddItemClickEvent(self.Item3,tonumber(dwdata.DangSeSiId[2]))
	self:AddItemClickEvent(self.Item4,tonumber(dwdata.DangSeSiId[3]))
	self:AddItemClickEvent(self.Item5,tonumber(dwdata.DangSeSiId[4]))
end

function CLuaDwWusesiHeCheng2020Wnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "OnPlayerItemChange")
	if self.delayTick then 
		UnRegisterTick(self.delayTick)
		self.delayTick = nil
	end
end

function CLuaDwWusesiHeCheng2020Wnd:OnPlayerItemChange(args)
	self:Repaint()
	--[[
	local item = CItemMgr.Inst:GetById(args[0])
	if not item then return end
	local go = self.m_items[item.TemplateId]
	if go then
		self:InitItem(go,item.TemplateId)
	end
	]]
end


--点击合成按钮
function CLuaDwWusesiHeCheng2020Wnd:OnSubmitBtnClick()

	if self.m_enable then
		self.Fxnode.gameObject:SetActive(false)
		self.Fxnode2.gameObject:SetActive(false)
		--todo：特效
		self.Fxnode:LoadFx("Fx/UI/Prefab/UI_wusesi001.prefab")
		self.Fxnode:Reset()

		if self.delayTick then 
			UnRegisterTick(self.delayTick)
			self.delayTick = nil
		end

		local func = function()
			self.Fxnode2:LoadFx("Fx/UI/Prefab/UI_wusesi002.prefab")
			self.Fxnode2:Reset()
		end
		self.delayTick = RegisterTickOnce(func,2 * 1000)

		local groupid = 17
		local count = self.NumInput:GetValue()
		Gac2Gas.RequestExchangeItems(groupid,count)
	else
		g_MessageMgr:ShowMessage("Wusesi2020")--提示材料不足
	end
end

