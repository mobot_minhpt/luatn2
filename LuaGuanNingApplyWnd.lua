local CButton=import "L10.UI.CButton"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local GuanNing_Setting=import "L10.Game.GuanNing_Setting"
local CScheduleMgr = import "L10.Game.CScheduleMgr"


CLuaGuanNingApplyWnd = class()
RegistClassMember(CLuaGuanNingApplyWnd,"table")
RegistClassMember(CLuaGuanNingApplyWnd,"refreshBtn")
-- RegistClassMember(CLuaGuanNingApplyWnd,"closeBtn")
RegistClassMember(CLuaGuanNingApplyWnd,"descLabel")
RegistClassMember(CLuaGuanNingApplyWnd,"battleDataButton")
RegistClassMember(CLuaGuanNingApplyWnd,"rowCount")

RegistClassMember(CLuaGuanNingApplyWnd,"signTimeList")
RegistClassMember(CLuaGuanNingApplyWnd,"startTimeList")

function CLuaGuanNingApplyWnd:Awake()
    self.table = self.transform:Find("Anchor/ScrollView"):GetComponent(typeof(QnTableView))
    self.refreshBtn = self.transform:Find("Anchor/UpdateBtn").gameObject
    -- self.closeBtn = self.transform:Find("Wnd_Bg_Secondary_1/CloseButton").gameObject
    self.descLabel = self.transform:Find("Anchor/Label"):GetComponent(typeof(UILabel))
    self.battleDataButton = self.transform:Find("Anchor/BattleDataButton").gameObject
--self.rowCount = ?
    self.descLabel.text = g_MessageMgr:FormatMessage("GUANNING_BAOMING_INTERFACE")

    UIEventListener.Get(self.refreshBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        Gac2Gas.QueryGnjcSignUpInfo()
    end)
    -- UIEventListener.Get(self.closeBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
    --     CUIManager.CloseUI(CUIResources.GuanNingApplyWnd)
    -- end)

    UIEventListener.Get(self.battleDataButton).onClick = --MakeDelegateFromCSFunction(self.OnClickBattleDataButton, VoidDelegate, self)
        DelegateFactory.VoidDelegate(function (p) 
            CLuaGuanNingMgr.ShowLastBattleData=false
            CUIManager.ShowUI(CUIResources.GuanNingBattleDataWnd)
        end)
end

function CLuaGuanNingApplyWnd:ConvertToTime(minutes)
    local hour= math.floor(minutes/60)
    local minute=minutes%60
    return SafeStringFormat3("%02d:%02d",hour,minute)
end

function CLuaGuanNingApplyWnd:InitTimeList()
    local setting = GuanNing_Setting.GetData()
    if CLuaGuanNingMgr.m_Hour==0 and CLuaGuanNingMgr.m_Minute==0 then
        local time = CServerTimeMgr.Inst:GetZone8Time()
        local find = nil
        for i=1,setting.StartTimeList.Length do
            local item = setting.StartTimeList[i-1]
            if item.DayOfWeek == EnumToInt(time.DayOfWeek) then
                find=item
                break
            end
        end
        if find then
            CLuaGuanNingMgr.m_Hour = find.Hour
            CLuaGuanNingMgr.m_Minute = find.minute
        end
    end

    local dailyGnjcTime = setting.DailyGNJCTime

    local startTime = CLuaGuanNingMgr.m_Hour*60 + CLuaGuanNingMgr.m_Minute

    local startSignUpDuration = math.floor(setting.StartSignUpDuration/60)
    local signUpDuration =  math.floor(setting.SignUpDuration/60)

    self.signTimeList={}

    local initTimeStr=self:ConvertToTime(startTime)
    local elapsedTime=0
    for i=1,dailyGnjcTime do
        if i==1 then
            table.insert( self.signTimeList, SafeStringFormat3("%s-%s",initTimeStr,self:ConvertToTime(startTime+startSignUpDuration)) )
            elapsedTime = elapsedTime+startSignUpDuration+1
        else
            table.insert( self.signTimeList, SafeStringFormat3("%s-%s",initTimeStr,self:ConvertToTime(startTime+signUpDuration + elapsedTime)) )
            elapsedTime = elapsedTime+startSignUpDuration+1
        end
    end

    self.startTimeList={}
    for i=1,dailyGnjcTime do
        if i==1 then
            startTime = startTime+startSignUpDuration+1--隔了1分钟
        else
            startTime = startTime+signUpDuration+1
        end
        table.insert( self.startTimeList, self:ConvertToTime(startTime) )
    end
end
function CLuaGuanNingApplyWnd:Init( )
    self:InitTimeList()
    
    CLuaGuanNingMgr.signUpState={}
    CLuaGuanNingMgr.signUpCount={}
    -- CGuanNingMgr.Inst:ClearSignUpInfo()
    --    /// 查询报名信息，在报名界面打开时调用
    Gac2Gas.QueryGnjcSignUpInfo()

    self.table.m_DataSource = DefaultTableViewDataSource.Create(
        function() return 5 end,
        function(item,row)
            self:InitItem(item,row)
        end)
    --收到返回之后再实例化
    self.rowCount = 5--CGuanNingMgr.Inst.signTimeList.Count
    self.table:ReloadData(false, false)
end

function CLuaGuanNingApplyWnd:OnEnable( )
    g_ScriptEvent:AddListener("QueryGnjcSignUpInfoResult", self, "GetGnjcSignUpInfoResult")
    -- 
    -- EventManager.AddListener(EnumEventType.GnjcSignUpInfoResult, MakeDelegateFromCSFunction(self.GetGnjcSignUpInfoResult, Action0, self))
    --如果有红点的时候，红点会消失。
    CScheduleMgr.Inst:TryCloseGuanNingAlert()
end
function CLuaGuanNingApplyWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("QueryGnjcSignUpInfoResult", self, "GetGnjcSignUpInfoResult")
    -- EventManager.RemoveListener(EnumEventType.GnjcSignUpInfoResult, MakeDelegateFromCSFunction(self.GetGnjcSignUpInfoResult, Action0, self))
end

function CLuaGuanNingApplyWnd:GetGnjcSignUpInfoResult( )
    self:InitTimeList()

    for i=1,self.rowCount do
        local item = self.table:GetItemAtRow(i-1)
        if item then
            -- item:Refresh()
            self:InitItem(item,i-1)
        end
    end
end

function CLuaGuanNingApplyWnd:InitItem(item,row)
    local transform = item.transform
    local idLabel = transform:Find("IdLabel"):GetComponent(typeof(UILabel))
    local timeLabel = transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local beginLabel = transform:Find("BeginLabel"):GetComponent(typeof(UILabel))
    local applyCountLabel = transform:Find("ApplyCountLabel"):GetComponent(typeof(UILabel))
    local applyBtn = transform:Find("ApplyBtn").gameObject
    local applyBtnLabel = transform:Find("ApplyBtn/Label"):GetComponent(typeof(UILabel))
    local applyBtnSprite = transform:Find("ApplyBtn"):GetComponent(typeof(UISprite))
    local button = transform:Find("ApplyBtn"):GetComponent(typeof(CButton))

    applyCountLabel.text = "0"
    CUICommonDef.SetActive(applyBtn, false, true)

    local playNum = row + 1

    UIEventListener.Get(applyBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        --报名
        if LocalString.GetString("报名") == applyBtnLabel.text then
            Gac2Gas.RequestSignUpGnjc(playNum)
        elseif LocalString.GetString("取消") == applyBtnLabel.text then
            Gac2Gas.RequestCancelSignUpGnjc(playNum)
        end
    end)

    idLabel.text = tostring((row + 1))
    timeLabel.text = self.signTimeList[row+1]
    beginLabel.text = self.startTimeList[row+1]

    -- local index = self.playNum - 1

    if CLuaGuanNingMgr.signUpCount[row+1] then
        applyCountLabel.text = tostring(CLuaGuanNingMgr.signUpCount[row+1])
    else
        applyCountLabel.text = "0"
    end

    --查看有没有报名过

    local signed=false
    for i,v in ipairs(CLuaGuanNingMgr.signUpState) do
        if v==playNum then
            signed=true
            break
        end
    end

    if signed then
        --说明这个场次已经报名过了
        applyBtnLabel.text = LocalString.GetString("取消")
        applyBtnSprite.spriteName = "common_btn_01_blue"-- CGuanNingApplyItem.NormalSprite
        button.normalSprite = "common_btn_01_blue"--CGuanNingApplyItem.NormalSprite
        -- applyBtnLabel.color = Color.white
    else
        applyBtnLabel.text = LocalString.GetString("报名")
        applyBtnSprite.spriteName = "common_btn_01_yellow"--CGuanNingApplyItem.OrangeSprite
        button.normalSprite = "common_btn_01_yellow"--CGuanNingApplyItem.OrangeSprite
        -- applyBtnLabel.color = Color.white
    end

    if CLuaGuanNingMgr.m_CurSignUpIndex > row then
        applyCountLabel.text = "0"
        CUICommonDef.SetActive(applyBtn, false, true)
    else
        CUICommonDef.SetActive(applyBtn, true, true)
    end

end
