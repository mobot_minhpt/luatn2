local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UITable = import "UITable"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local Screen = import "UnityEngine.Screen"
local CBaseWnd = import "L10.UI.CBaseWnd"

LuaSkillLevelDetailInfoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSkillLevelDetailInfoWnd, "Background", "Background", GameObject)
RegistChildComponent(LuaSkillLevelDetailInfoWnd, "ContentScrollView", "ContentScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaSkillLevelDetailInfoWnd, "Table", "Table", UITable)
RegistChildComponent(LuaSkillLevelDetailInfoWnd, "ContentBg", "ContentBg", GameObject)
RegistChildComponent(LuaSkillLevelDetailInfoWnd, "ScrollViewIndicator", "ScrollViewIndicator", UIScrollViewIndicator)
RegistChildComponent(LuaSkillLevelDetailInfoWnd, "TitleTemplate", "TitleTemplate", UILabel)
RegistChildComponent(LuaSkillLevelDetailInfoWnd, "ParagraphTemplate", "ParagraphTemplate", GameObject)
RegistChildComponent(LuaSkillLevelDetailInfoWnd, "Button", "Button", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaSkillLevelDetailInfoWnd, "m_MinHeight")
function LuaSkillLevelDetailInfoWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

end

function LuaSkillLevelDetailInfoWnd:Init()
    if LuaMessageTipMgr.m_ButtonText ~= nil and LuaMessageTipMgr.m_Callback ~= nil then
        self.Button.gameObject:SetActive(true)
        self.Button.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LuaMessageTipMgr.m_ButtonText
    else
        self.Button.gameObject:SetActive(false)
    end
    
    CommonDefs.AddOnClickListener(self.Button.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnButtonClick()
    end), false)

    self.m_MinHeight = 200
    if LuaMessageTipMgr.m_WndPosX == nil then
        self.transform.localPosition = Vector3.zero
    else
        Extensions.SetLocalPositionX(self.transform,LuaMessageTipMgr.m_WndPosX)
    end
    self:InitTable()
    self.TitleTemplate.gameObject:SetActive(false)
    self.ParagraphTemplate.gameObject:SetActive(false)
end

function LuaSkillLevelDetailInfoWnd:InitTable()
    Extensions.RemoveAllChildren(self.Table.transform)
    if LuaMessageTipMgr.m_Title ~= nil then
        local titleGo = CUICommonDef.AddChild(self.Table.gameObject, self.TitleTemplate.gameObject)
        titleGo.transform:GetComponent(typeof(UILabel)).text = LuaMessageTipMgr.m_Title
    end
    if not LuaMessageTipMgr.m_KvTable then return end
    local kv = LuaMessageTipMgr.m_KvTable
    for i = 1,#kv do
        local go = CUICommonDef.AddChild(self.Table.gameObject, self.ParagraphTemplate)
        go.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = kv[i].Key
        go.transform:Find("ValueLabel"):GetComponent(typeof(UILabel)).text = kv[i].Value
        if kv[i].Func then
            kv[i].Func(go)
        end
    end
    self.ContentScrollView:ResetPosition()
    self.Table:Reposition()

    self:LayoutWnd()
end

function LuaSkillLevelDetailInfoWnd:OnButtonClick()
    if LuaMessageTipMgr.m_Callback then
        LuaMessageTipMgr.m_Callback()
    end
    CUIManager.CloseUI(CLuaUIResources.SkillLevelDetailInfoWnd)
end

function LuaSkillLevelDetailInfoWnd:LayoutWnd()
    if LuaMessageTipMgr.m_ButtonText~=nil and LuaMessageTipMgr.m_Callback~=nil then
        self.ContentScrollView.panel.bottomAnchor.absolute = 120
    else
        self.ContentScrollView.panel.bottomAnchor.absolute = 30
    end

    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenWidth = Screen.width * scale * CUIManager.UIMainCamera.rect.width
    local virtualScreenHeight = Screen.height * scale

    local contentTopPadding = math.abs(self.ContentScrollView.panel.topAnchor.absolute)
    local contentBottomPadding = math.abs(self.ContentScrollView.panel.bottomAnchor.absolute)

    --计算高度
    local totalWndHeight = self:TotalHeightOfScrollViewContent() + contentTopPadding + contentBottomPadding + self.ContentScrollView.panel.clipSoftness.y * 2 + 1
    --加1避免由于精度误差导致scrollview刚好显示全table内容时可以滚动的问题
    local displayWndHeight = math.min(math.max(totalWndHeight, self.m_MinHeight), virtualScreenHeight)

    --设置背景高度
    self.Background:GetComponent(typeof(UIWidget)).height = math.ceil(displayWndHeight)

    local rects = CommonDefs.GetComponentsInChildren_Component_Type_Boolean(self.gameObject:GetComponent(typeof(CBaseWnd)), typeof(UIRect), true)
    do
        local i = 0
        while i < rects.Length do
            if rects[i].updateAnchors == UIRect.AnchorUpdate.OnEnable or rects[i].updateAnchors == UIRect.AnchorUpdate.OnStart then
                rects[i]:ResetAndUpdateAnchors()
            end
            i = i + 1
        end
    end
    self.ContentScrollView:ResetPosition()
    self.ScrollViewIndicator:Layout()
end

function LuaSkillLevelDetailInfoWnd:TotalHeightOfScrollViewContent()
    return NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform).size.y + self.Table.padding.y * 2
end
function LuaSkillLevelDetailInfoWnd:OnDisable()
    LuaMessageTipMgr.m_KvTable = nil
    LuaMessageTipMgr.m_Title = nil
    LuaMessageTipMgr.m_ButtonText = nil
    LuaMessageTipMgr.m_Callback = nil
    LuaMessageTipMgr.m_WndPosX = nil
end
--@region UIEvent

--@endregion UIEvent

