local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local QnButton = import "L10.UI.QnButton"
local CUITexture = import "L10.UI.CUITexture"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemMgr = import "L10.Game.CItemMgr"
local Animation = import "UnityEngine.Animation"
local UILabel = import "UILabel"
LuaZYPDResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZYPDResultWnd, "WinStyle", "WinStyle", GameObject)
RegistChildComponent(LuaZYPDResultWnd, "LoseStyle", "LoseStyle", GameObject)
RegistChildComponent(LuaZYPDResultWnd, "Reward", "Reward", GameObject)
RegistChildComponent(LuaZYPDResultWnd, "NoReward", "NoReward", UILabel)
RegistChildComponent(LuaZYPDResultWnd, "ModeLab", "ModeLab", UILabel)
RegistChildComponent(LuaZYPDResultWnd, "RewardBtn", "RewardBtn", QnButton)
RegistChildComponent(LuaZYPDResultWnd, "CloseBtn", "CloseBtn", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaZYPDResultWnd, "m_Animation")
RegistClassMember(LuaZYPDResultWnd, "m_AnimationTick")

function LuaZYPDResultWnd:Awake()
    self.m_Animation = self.transform:GetComponent(typeof(Animation))
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.RewardBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRewardBtnClick()
	end)


    --@endregion EventBind end
end

function LuaZYPDResultWnd:OnDisable()
    if self.m_AnimationTick then
        UnRegisterTick(self.m_AnimationTick)
    end
end

function LuaZYPDResultWnd:Init()
    if LuaZhongYuanJie2023Mgr.isSuccess then
        self:ShowReward()
        if LuaZhongYuanJie2023Mgr.rewardMode == 1 then
            self.m_Animation:Play("zypdresultwnd_normal_win")
        else
            self.m_Animation:Play("zypdresultwnd_hard_win")
        end
    else
        self.CloseBtn:SetActive(false)
        self.transform:Find("_BgMask_").gameObject:SetActive(false)
        self.m_Animation:Play("zypdresultwnd_lose")
        self.m_AnimationTick = RegisterTickOnce(function ()
            if self.m_AnimationTick then
                UnRegisterTick(self.m_AnimationTick)
            end
            CUIManager.CloseUI(CLuaUIResources.ZYPDResultWnd)
        end, self.m_Animation:get_Item("zypdresultwnd_lose").length * 1000)
        
    end
end

function LuaZYPDResultWnd:ShowReward()
    local rewardMode = LuaZhongYuanJie2023Mgr.rewardMode
    if rewardMode == 1 then
        self.ModeLab.text = LocalString.GetString("[acf8ff]普渡古寺普通模式[-]")
        self.NoReward.text = LocalString.GetString("今日普通难度奖励已领取")
    elseif rewardMode == 2 then
        self.ModeLab.text = LocalString.GetString("[d89bff]普渡古寺困难模式[-]")
        self.NoReward.text = LocalString.GetString("今日困难模式奖励已领取")
    end
    if LuaZhongYuanJie2023Mgr:NeedShowRewardNode() then
        self.Reward:SetActive(true)
        self.NoReward.gameObject:SetActive(false)
        
        local itemTex = self.RewardBtn:GetComponent(typeof(CUITexture))
        local itemTemplateId = LuaZhongYuanJie2023Mgr:GetRewardID(rewardMode)

        local item = CItemMgr.Inst:GetItemTemplate(itemTemplateId)
        itemTex:LoadMaterial(item.Icon)
    else
        self.Reward:SetActive(false)
        self.NoReward.gameObject:SetActive(true)
    end
end

--@region UIEvent

function LuaZYPDResultWnd:OnRewardBtnClick()
    local itemTemplateId = LuaZhongYuanJie2023Mgr:GetRewardID(LuaZhongYuanJie2023Mgr.rewardMode)
    CItemInfoMgr.ShowLinkItemTemplateInfo(itemTemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
end


--@endregion UIEvent

