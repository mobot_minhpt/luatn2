local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CSkillButtonInfo = import "L10.UI.CSkillButtonInfo"
local UIGrid = import "UIGrid"
local FXClientInfoReporter = import "L10.Game.FXClientInfoReporter"
local EnumRecordContext = import "L10.Game.EnumRecordContext"
local EChatPanel = import "L10.Game.EChatPanel"
local Vector2 = import "UnityEngine.Vector2"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CRecordingInfoMgr = import "L10.UI.CRecordingInfoMgr"
local CRecordingWnd = import "L10.UI.CRecordingWnd"

LuaRuYiAdditionSkillWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaRuYiAdditionSkillWnd, "AdditionalButton", "AdditionalButton", CSkillButtonInfo)
RegistChildComponent(LuaRuYiAdditionSkillWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaRuYiAdditionSkillWnd, "Tipview", "Tipview", GameObject)
RegistChildComponent(LuaRuYiAdditionSkillWnd, "TipLabel", "TipLabel", UILabel)
RegistChildComponent(LuaRuYiAdditionSkillWnd, "FastSkillBtn", "FastSkillBtn", GameObject)
RegistChildComponent(LuaRuYiAdditionSkillWnd, "CloseTipBtn", "CloseTipBtn", GameObject)

--@endregion RegistChildComponent end

function LuaRuYiAdditionSkillWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.FastSkillBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFastSkillBtnClick()
	end)


	
	UIEventListener.Get(self.CloseTipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseTipBtnClick()
	end)


    --@endregion EventBind end
    self.Tipview:SetActive(false)
end
function LuaRuYiAdditionSkillWnd:OnEnable()
    g_ScriptEvent:AddListener("InitRuYiAdditionalSkill", self, "OnInitRuYiAdditionalSkill")
    g_ScriptEvent:AddListener("UpdateCooldown", self, "OnUpdateCooldown")
    g_ScriptEvent:AddListener("OnVoiceTranslateFinished", self, "OnVoiceTranslateFinished")
    g_ScriptEvent:AddListener("InitAdditionalSkill", self, "OnInitAdditionalSkill")
end

function LuaRuYiAdditionSkillWnd:OnDisable()
    g_ScriptEvent:RemoveListener("InitRuYiAdditionalSkill", self, "OnInitRuYiAdditionalSkill")
    g_ScriptEvent:RemoveListener("UpdateCooldown", self, "OnUpdateCooldown")
    g_ScriptEvent:RemoveListener("OnVoiceTranslateFinished", self, "OnVoiceTranslateFinished")
    g_ScriptEvent:RemoveListener("InitAdditionalSkill", self, "OnInitAdditionalSkill")
end

function LuaRuYiAdditionSkillWnd:Init()
    local setting = HuluBrothers_Setting.GetData()
    local ruyiSkill = setting.RuYiSkill
    self.m_RuYiSkill = ruyiSkill
    local replaceSkill = setting.ReplaceSkill

    self.AdditionalButton.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.Grid.transform)

    local tempSkillArray = CClientMainPlayer.Inst.SkillProp.ActiveTempSkill

    local replaceButton,ruyiButton
    local replaceButtonInfo,ruyiButtonInfo
    for i = 1, tempSkillArray.Length-1 do
        local skillId = tempSkillArray[i]
        if i <=2 and skillId ~= 0 then        
            skillId = CClientMainPlayer.Inst.SkillProp:GetAlternativeSkillIdWithDelta(skillId, CClientMainPlayer.Inst.Level)
            local skill = Skill_AllSkills.GetData(skillId)
            if skillId == ruyiSkill then
                ruyiButton = NGUITools.AddChild(self.Grid.gameObject,self.AdditionalButton.gameObject)
                ruyiButtonInfo = CommonDefs.GetComponent_GameObject_Type(ruyiButton, typeof(CSkillButtonInfo))
                ruyiButtonInfo:LoadIcon(LuaSkillMgr:GetOriginOrReplaceSkillIcon(skill), skillId, false, true)
                ruyiButton:SetActive(true)
            elseif skillId == replaceSkill then
                replaceButton = NGUITools.AddChild(self.Grid.gameObject,self.AdditionalButton.gameObject)
                replaceButtonInfo = CommonDefs.GetComponent_GameObject_Type(replaceButton, typeof(CSkillButtonInfo))
                replaceButtonInfo:LoadIcon(LuaSkillMgr:GetOriginOrReplaceSkillIcon(skill), skillId, false, true)
                replaceButton:SetActive(false)
            end
        end
    end

    if ruyiButton then
        UIEventListener.Get(ruyiButton).onPress  = DelegateFactory.BoolDelegate(function(go,isPressed)
            if replaceButtonInfo.CDLeft ~= 0 then
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("如意技能冷却中，无法释放语音"))
                return
            end
            if isPressed then
                local mainplayer = CClientMainPlayer.Inst
                if mainplayer == nil or mainplayer.IsDie then
                    return
                end
                if ruyiButtonInfo.CDLeft == 0 and not mainplayer.IsCasting then
                    LuaHuLuWa2022Mgr.m_VoiceInputTime = LuaHuLuWa2022Mgr.m_VoiceInputTime + 1
                    if LuaHuLuWa2022Mgr.m_VoiceInputTime >= 3 then
                        --一键释放
                        self:FastCastRuYiSkill()
                        return
                    else
                        mainplayer:TryCastSkill(ruyiSkill, true, Vector2.zero)
                        FXClientInfoReporter.Inst:ReportCastTempSkill(ruyiSkill, "buttonclick")
                        LuaHuLuWa2022Mgr.StartVoicePassword(ruyiButtonInfo.gameObject)
                        return
                    end
                end
                mainplayer:TryCastSkill(ruyiSkill, true, Vector2.zero)
                FXClientInfoReporter.Inst:ReportCastTempSkill(ruyiSkill, "buttonclick")
            else
                CRecordingInfoMgr.CloseRecordingWnd(not go:Equals(CUICommonDef.SelectedUI))
                CRecordingWnd.recordingText = LuaHuLuWa2022Mgr.m_PreRecordingText
            end
        end) 
    end
    
    self.Grid:Reposition()

    CLuaGuideMgr.TryRuYiSkillGuide()
end

function LuaRuYiAdditionSkillWnd:OnVoiceTranslateFinished(args)
	local context = args[0]
	local voiceId = args[1]
	local result = args[2]
	if context == EnumRecordContext.RuYi then
        LuaHuLuWa2022Mgr.CheckVoicePassword(result)
    end
end
function LuaRuYiAdditionSkillWnd:OnUpdateCooldown()
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then
        return
    end
    local cooldownProp = mainplayer.CooldownProp
    local globalRemain = cooldownProp:GetServerRemainTime(1000)
    local globalTotal = cooldownProp:GetServerTotalTime(1000)
    local globalPercent = (globalTotal == 0) and 0 or (globalRemain / globalTotal)
    for i=0,self.Grid.transform.childCount - 1, 1 do 
        local child = self.Grid.transform:GetChild(i)
        local skillButtonInfo = CommonDefs.GetComponent_GameObject_Type(child.gameObject, typeof(CSkillButtonInfo))
        local skillId = skillButtonInfo.skillTemplateID
        local remain = cooldownProp:GetServerRemainTime(skillId)
        local total = cooldownProp:GetServerTotalTime(skillId)
        local percent = total == 0 and 0 or remain/total
        if globalRemain > remain then
            percent = globalPercent
        end
        skillButtonInfo.CDLeft = percent
    end
end

function LuaRuYiAdditionSkillWnd:OnInitRuYiAdditionalSkill()
    self:Init()
end

function LuaRuYiAdditionSkillWnd:OnInitAdditionalSkill()
    self:Init()
end

function LuaRuYiAdditionSkillWnd:FastCastRuYiSkill()
    self.Tipview:SetActive(true)
    local msg = g_MessageMgr:FormatMessage("RuYi_Forget_Password")
    self.TipLabel.text = msg
end

--引导
function LuaRuYiAdditionSkillWnd:GetGuideGo(methodName)
    if methodName == "RuYiBtn" then
        for i=0,self.Grid.transform.childCount - 1, 1 do 
            local child = self.Grid.transform:GetChild(i)
            local skillButtonInfo = CommonDefs.GetComponent_GameObject_Type(child.gameObject, typeof(CSkillButtonInfo))
            local skillId = skillButtonInfo.skillTemplateID
            if skillId == self.m_RuYiSkill then
                return child.gameObject
            end
        end
    end
    return nil
end
--@region UIEvent

function LuaRuYiAdditionSkillWnd:OnFastSkillBtnClick()
    local setting = HuluBrothers_Setting.GetData()
    local replaceSkill = setting.ReplaceSkill

    LuaHuLuWa2022Mgr.m_VoiceInputTime = 0
    g_MessageMgr:ShowMessage("RuYi_Password_Correct")
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil or mainplayer.IsDie then
        return
    end
        
    mainplayer:TryCastSkill(replaceSkill, true, Vector2.zero)
    FXClientInfoReporter.Inst:ReportCastTempSkill(replaceSkill, "buttonclick")
    self.Tipview:SetActive(false)
end

function LuaRuYiAdditionSkillWnd:OnCloseTipBtnClick()
    self.Tipview:SetActive(false)
end


--@endregion UIEvent

