local UIGrid = import "UIGrid"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local UISlider = import "UISlider"
local LocalString = import "LocalString"
local CUIFx = import "L10.UI.CUIFx"
local QnButton = import "L10.UI.QnButton"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CUITexture = import "L10.UI.CUITexture"
local Profession = import "L10.Game.Profession"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumClass = import "L10.Game.EnumClass"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local DelegateFactory = import "DelegateFactory"

CLuaQMHuoYunSubWnd = class()

RegistClassMember(CLuaQMHuoYunSubWnd, "m_CanSubLab")
RegistClassMember(CLuaQMHuoYunSubWnd, "m_TopTipLab")

RegistClassMember(CLuaQMHuoYunSubWnd, "m_TodaySubLab")
RegistClassMember(CLuaQMHuoYunSubWnd, "m_RankLab")
RegistClassMember(CLuaQMHuoYunSubWnd, "m_TotalGetLab")
RegistClassMember(CLuaQMHuoYunSubWnd, "m_TotalFillLab")
RegistClassMember(CLuaQMHuoYunSubWnd, "m_TotalSubLab")
RegistClassMember(CLuaQMHuoYunSubWnd, "m_AllFinAwdBtn")
RegistClassMember(CLuaQMHuoYunSubWnd, "m_AllFinAwdLab")
RegistClassMember(CLuaQMHuoYunSubWnd, "m_AllFinRedTip")
RegistClassMember(CLuaQMHuoYunSubWnd, "m_RemindSubBtn")

RegistClassMember(CLuaQMHuoYunSubWnd, "m_RuleDescBtn")

RegistClassMember(CLuaQMHuoYunSubWnd, "m_ItemGrid")
RegistClassMember(CLuaQMHuoYunSubWnd, "m_ItemTemplate")

RegistClassMember(CLuaQMHuoYunSubWnd, "m_FullTongXinJingChipId")
RegistClassMember(CLuaQMHuoYunSubWnd, "m_HuoYunFullProgress")
RegistClassMember(CLuaQMHuoYunSubWnd, "m_IsSelfComplete")--是否完成负责项职业
RegistClassMember(CLuaQMHuoYunSubWnd, "m_CanSubLabPosXTemp")

RegistClassMember(CLuaQMHuoYunSubWnd, "m_PopStatesBtn")
RegistClassMember(CLuaQMHuoYunSubWnd, "m_Rank2Lab")
RegistClassMember(CLuaQMHuoYunSubWnd, "m_PopArrowIcon")

function CLuaQMHuoYunSubWnd:Awake()
    self:InitComponents()

    self.m_FullTongXinJingChipId = WuYi2020_Setting.GetData().FullTongXinJingChipId
    self.m_HuoYunFullProgress = WuYi2020_Setting.GetData().HuoYunFullProgress
end

function CLuaQMHuoYunSubWnd:InitComponents()
    self.m_CanSubLab    = self.transform:Find("Anchor/Offset/TopRight/SubLabel"):GetComponent(typeof(UILabel))
    self.m_TopTipLab    = self.transform:Find("Anchor/Offset/TopLeft/TipLabel"):GetComponent(typeof(UILabel))

    self.m_TodaySubLab  = self.transform:Find("States/Main/Top/SubLabel"):GetComponent(typeof(UILabel))
    self.m_RankLab      = self.transform:Find("States/Main/Top/RankLabel"):GetComponent(typeof(UILabel))
    self.m_TotalGetLab  = self.transform:Find("States/Main/Bottom/TotalGetLabel"):GetComponent(typeof(UILabel))
    self.m_TotalFillLab = self.transform:Find("States/Main/Bottom/TotalFillLabel"):GetComponent(typeof(UILabel))
    self.m_TotalSubLab  = self.transform:Find("States/Main/Bottom/TotalSubLabel"):GetComponent(typeof(UILabel))
    self.m_AllFinAwdBtn = self.transform:Find("Anchor/Offset/TopRight/AllFinishAwdBtn"):GetComponent(typeof(QnButton))
    self.m_AllFinRedTip = self.transform:Find("Anchor/Offset/TopRight/AllFinishAwdBtn/RedTip"):GetComponent(typeof(UISprite))
    self.m_AllFinAwdLab = self.transform:Find("Anchor/Offset/TopRight/AllFinishAwdBtn/Label"):GetComponent(typeof(UILabel))
    self.m_RemindSubBtn = self.transform:Find("Anchor/Offset/TopRight/RemindSubBtn"):GetComponent(typeof(QnButton))

    self.m_RuleDescBtn  = self.transform:Find("Anchor/Offset/TopRight/RuleDescBtn"):GetComponent(typeof(QnButton))

    self.m_ItemGrid     = self.transform:Find("Anchor/Offset/Main/Grid"):GetComponent(typeof(UIGrid))

    self.m_PopStatesBtn = self.transform:Find("Anchor/Offset/TopLeft/PopStatesBtn"):GetComponent(typeof(QnButton))
    self.m_Rank2Lab     = self.transform:Find("Anchor/Offset/TopLeft/PopStatesBtn/RankLabel"):GetComponent(typeof(UILabel))
    self.m_PopArrowIcon = self.transform:Find("Anchor/Offset/TopLeft/PopStatesBtn/ArrowIcon"):GetComponent(typeof(UISprite))
    self.m_StatesPanel  = self.transform:Find("States"):GetComponent(typeof(UIPanel))
end

function CLuaQMHuoYunSubWnd:Init()
    self.m_IsSelfComplete = false
    self.m_RuleDescBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        g_MessageMgr:ShowMessage("WuYi2020_QuanMinHuoYun_TXJSubmit_Tips")
    end)
    self.m_CanSubLabPosXTemp = self.m_CanSubLab.transform.localPosition.x

    self.m_PopStatesBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        self.m_StatesPanel.gameObject:SetActive(true)
    end)

    self.m_AllFinAwdBtn.OnClick = DelegateFactory.Action_QnButton(function ()
        if CLuaQMHuoYunMgr.bCanGetFullAward then
            Gac2Gas.RequestGetQuanMinHuoYunFullAward(CLuaQMHuoYunMgr.day)
        else
            local msg = g_MessageMgr:FormatMessage("WuYi2020_QuanMinHuoYun_TXJSubmit_SubmitQuest")
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
                local canSub = CItemMgr.Inst:GetItemCount(self.m_FullTongXinJingChipId)
                CLuaNumberInputMgr.ShowNumInputBox(1, canSub, 1, function (num)
                    if num <= 0 then
                        return
                    end
                    Gac2Gas.RequestSubmitTongXinJingChip(CLuaQMHuoYunMgr.day, 0, 0, num, true)
                end, LocalString.GetString("提交碎片"))
            end), nil, nil, nil, false)
        end
    end)

    self.m_RemindSubBtn.OnClick = DelegateFactory.Action_QnButton(function () 
        local canSub = CItemMgr.Inst:GetItemCount(self.m_FullTongXinJingChipId)
        CLuaNumberInputMgr.ShowNumInputBox(1, canSub, 1, function (num)
            if num <= 0 then
                return
            end
            Gac2Gas.RequestSubmitTongXinJingChip(CLuaQMHuoYunMgr.day, 0, 0, num, false)
        end, LocalString.GetString("提交碎片"))
    end)
    self:UpdateComponents()
end

function CLuaQMHuoYunSubWnd:UpdateComponents()
    self:UpdateValues()
    self:UpdateItemList()
end

function CLuaQMHuoYunSubWnd:UpdateValues()
    if self.m_FullTongXinJingChipId then
        self.m_CanSubLab.text = CItemMgr.Inst:GetItemCount(self.m_FullTongXinJingChipId)
    end
    local bShowTopTip = not CLuaQMHuoYunMgr.bAllProgressFull and CLuaQMHuoYunMgr.bAssist and CLuaQMHuoYunMgr.submitClass == 0
    self.m_TopTipLab.gameObject:SetActive(bShowTopTip)
    self.m_PopStatesBtn.gameObject:SetActive(not bShowTopTip)

    self.m_TodaySubLab.text = CLuaQMHuoYunMgr.submitNum
    if CLuaQMHuoYunMgr.rank == 0 then
        self.m_RankLab.text = "-"
    else
        self.m_RankLab.text =  CLuaQMHuoYunMgr.rank
    end
    self.m_Rank2Lab.text = self.m_RankLab.text --与RankLab相同
    self.m_TotalGetLab.text = CLuaQMHuoYunMgr.totalGetNum
    self.m_TotalFillLab.text = CLuaQMHuoYunMgr.totalFillNum
    self.m_TotalSubLab.text = CLuaQMHuoYunMgr.totalSubmitNum

    self.m_AllFinAwdBtn.gameObject:SetActive(CLuaQMHuoYunMgr.bAllProgressFull)

    if CLuaQMHuoYunMgr.bAllProgressFull then --为提交多余，和全部完成按钮腾位子
        self.m_CanSubLab.transform.localPosition = Vector3(self.m_CanSubLabPosXTemp - self.m_AllFinAwdBtn.gameObject:GetComponent(typeof(UISprite)).width,
            self.m_CanSubLab.transform.localPosition.y, self.m_CanSubLab.transform.localPosition.z)
    end

    if CLuaQMHuoYunMgr.bFullAwardGot then
        self.m_AllFinAwdBtn.gameObject:SetActive(false)--现在已领取直接消失，并显示提交多余碎片按钮
    else
        self.m_AllFinRedTip.gameObject:SetActive(CLuaQMHuoYunMgr.bCanGetFullAward)
        self.m_AllFinAwdBtn.Enabled = true
        self.m_AllFinAwdLab.text = LocalString.GetString("全部完成奖励")
    end

    if CLuaQMHuoYunMgr.bAllProgressFull and CLuaQMHuoYunMgr.bFullAwardGot then
        self.m_RemindSubBtn.gameObject:SetActive(true)
    end

end

function CLuaQMHuoYunSubWnd:UpdateItemList()
    local list = CLuaQMHuoYunMgr.list
    if not list then return end
    
    self:CheckSelfFin()
    for i = 1, #list do
        self:UpdateItem(i)
    end
end

function CLuaQMHuoYunSubWnd:CheckSelfFin()
    local list = CLuaQMHuoYunMgr.list
    if not list then return end
    
    self.m_IsSelfComplete = false
    for i = 1, #list do
        local data  = CLuaQMHuoYunMgr.list[i]
        if CLuaQMHuoYunMgr.bAssist and data.class == CLuaQMHuoYunMgr.submitClass or data.class == EnumToInt(CClientMainPlayer.Inst.Class) then --是否是负责项
            if data.progress >= self.m_HuoYunFullProgress then
                self.m_IsSelfComplete = true
                return
            end
        end
    end
end

function CLuaQMHuoYunSubWnd:UpdateItem(place)
    local data      = CLuaQMHuoYunMgr.list[place]
    local go        = self.transform:Find("Anchor/Offset/Main/Grid/ItemCell" .. place).gameObject
    local zhiYeIcon = go.transform:Find("ZhiYeIcon"):GetComponent(typeof(UISprite))
    local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local icon      = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local iconBtn   = go.transform:Find("Icon"):GetComponent(typeof(QnButton))
    local iconLock  = go.transform:Find("Icon/Lock").gameObject
    local iconFx    = go.transform:Find("Icon/Effect"):GetComponent(typeof(CUIFx))
    local slider    = go.transform:Find("Slider"):GetComponent(typeof(UISlider))
    local sliderLab = go.transform:Find("Slider/Label"):GetComponent(typeof(UILabel))
    local chooseBtn = go.transform:Find("ChooseBtn"):GetComponent(typeof(QnButton))
    local submitBtn = go.transform:Find("SubmitBtn"):GetComponent(typeof(QnButton))
    local finTip    = go.transform:Find("FinishedTip").gameObject
    local helpRank  = go.transform:Find("HelpRank"):GetComponent(typeof(UILabel))

    zhiYeIcon.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), data.class))

    nameLabel.text = WuYi2020_HuoYunItem.GetData(place).Name
    icon:LoadMaterial(WuYi2020_HuoYunItem.GetData(place).Icon)
    -- iconBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
    -- end)

    if data.progress >= self.m_HuoYunFullProgress then
        iconLock:SetActive(false)
        iconFx.gameObject:SetActive(true)
        iconFx:LoadFx(CUIFxPaths.ItemRemindYellowFxPath)
    else
        iconLock:SetActive(true)
        iconFx.gameObject:SetActive(false)
    end

    slider.value = data.progress / self.m_HuoYunFullProgress
    sliderLab.text = data.progress .. "/" .. self.m_HuoYunFullProgress

    if data.progress < self.m_HuoYunFullProgress then
        if self.m_IsSelfComplete then --如果负责项完成了,可以任性提交其他
            chooseBtn.gameObject:SetActive(false)
            submitBtn.gameObject:SetActive(true)
            submitBtn.Enabled = true
        else
            if CLuaQMHuoYunMgr.bAssist then
                if CLuaQMHuoYunMgr.submitClass ~= 0 then
                    chooseBtn.gameObject:SetActive(false)
                    submitBtn.gameObject:SetActive(true)
                    submitBtn.Enabled = data.class == CLuaQMHuoYunMgr.submitClass;
                else
                    chooseBtn.gameObject:SetActive(true)
                    submitBtn.gameObject:SetActive(false)
                end
            else
                chooseBtn.gameObject:SetActive(false)
                submitBtn.gameObject:SetActive(true)
                submitBtn.Enabled = data.class == EnumToInt(CClientMainPlayer.Inst.Class);
            end
        end

        finTip:SetActive(false)
    else
        chooseBtn.gameObject:SetActive(false)
        submitBtn.gameObject:SetActive(false)

        finTip:SetActive(true)
    end


    helpRank.text = LocalString.GetString("辅助排名：无")
    if data.assistClass1 ~= 0 then
        helpRank.text = LocalString.GetString("辅助排名：") .. Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), data.assistClass1))
        if data.assistClass2 ~= 0  then
            helpRank.text = helpRank.text .. LocalString.GetString("、") .. Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass),  data.assistClass2))
        end
    end
    
    chooseBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        local msg = g_MessageMgr:FormatMessage("WuYi2020_QuanMinHuoYun_TXJSubmit_ChooseMakeSure",
            Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), data.class)))
        MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
            Gac2Gas.SetQuanMinHuoYunAssistClass(CLuaQMHuoYunMgr.day, place, data.class)
         end), nil, nil, nil, false)
    end)

    submitBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        local canSub = math.min(CItemMgr.Inst:GetItemCount(self.m_FullTongXinJingChipId), self.m_HuoYunFullProgress - data.progress)
        CLuaNumberInputMgr.ShowNumInputBox(1, canSub, 1,function (num)
            if num <= 0 then
                return
            end
            Gac2Gas.RequestSubmitTongXinJingChip(CLuaQMHuoYunMgr.day, place, data.class, num, false)
         end, g_MessageMgr:FormatMessage("WuYi2020_QuanMinHuoYun_TXJSubmit_SelectNum", nameLabel.text))
    end)
end

function CLuaQMHuoYunSubWnd:ResetCanSub()
    self.m_CanSubLab.text = CItemMgr.Inst:GetItemCount(self.m_FullTongXinJingChipId)
end

function CLuaQMHuoYunSubWnd:OnSetQuanMinHuoYunAssistClassSuccess()
    self:UpdateComponents()
end

function CLuaQMHuoYunSubWnd:OnSubmitTongXinJingChipSuccess(place, progress)
    if place == 0 then --提交多余同心镜
        self:UpdateValues()
        return
    end

    if progress >= self.m_HuoYunFullProgress and not self.m_IsSelfComplete then --只有负责项填装完毕的时候才需要刷新其他项状态
        self:UpdateComponents()
    else
        self:UpdateValues()
        self:UpdateItem(place)
    end
end

function CLuaQMHuoYunSubWnd:OnSendQuanMinHuoYunFullAwardSuccess(day)
    if day == CLuaQMHuoYunMgr.day then
        CLuaQMHuoYunMgr.bFullAwardGot = true
        self.m_AllFinAwdBtn.gameObject:SetActive(false)--现在已领取直接消失，并显示提交多余碎片按钮
        self.m_RemindSubBtn.gameObject:SetActive(true)
    end
end

function CLuaQMHuoYunSubWnd:OnQueryQuanMinHuoYunInfoResult()
    self:UpdateComponents()
end

function CLuaQMHuoYunSubWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "ResetCanSub")
    g_ScriptEvent:AddListener("SetItemAt", self, "ResetCanSub")
    g_ScriptEvent:AddListener("SetQuanMinHuoYunAssistClassSuccess", self, "OnSetQuanMinHuoYunAssistClassSuccess")
    g_ScriptEvent:AddListener("SubmitTongXinJingChipSuccess", self, "OnSubmitTongXinJingChipSuccess")
    g_ScriptEvent:AddListener("SendQuanMinHuoYunFullAwardSuccess", self, "OnSendQuanMinHuoYunFullAwardSuccess")
    g_ScriptEvent:AddListener("QueryQuanMinHuoYunInfoResult", self, "OnQueryQuanMinHuoYunInfoResult")
end

function CLuaQMHuoYunSubWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "ResetCanSub")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "ResetCanSub")
    g_ScriptEvent:RemoveListener("SetQuanMinHuoYunAssistClassSuccess", self, "OnSetQuanMinHuoYunAssistClassSuccess")
    g_ScriptEvent:RemoveListener("SubmitTongXinJingChipSuccess", self, "OnSubmitTongXinJingChipSuccess")
    g_ScriptEvent:RemoveListener("SendQuanMinHuoYunFullAwardSuccess", self, "OnSendQuanMinHuoYunFullAwardSuccess")
    g_ScriptEvent:RemoveListener("QueryQuanMinHuoYunInfoResult", self, "OnQueryQuanMinHuoYunInfoResult")
end
