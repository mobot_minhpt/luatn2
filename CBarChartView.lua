-- Auto Generated!!
local CBarChartView = import "L10.UI.CBarChartView"
local CBarChartXAixsItem = import "L10.UI.CBarChartXAixsItem"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Extensions = import "Extensions"
CBarChartView.m_Init_CS2LuaHook = function (this, values, annotations) 
    if values == nil or values.Length == 0 or values.Length ~= annotations.Length then
        return
    end

    local maxVal = values[0]
    do
        local i = 1
        while i < values.Length do
            if maxVal < values[i] then
                maxVal = values[i]
            end
            i = i + 1
        end
    end

    if maxVal <= 0 then
        maxVal = 1
    end

    Extensions.RemoveAllChildren(this.table.transform)
    do
        local i = 0
        while i < values.Length do
            local instance = CUICommonDef.AddChild(this.table.gameObject, this.barTemplate)
            instance:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(instance, typeof(CBarChartXAixsItem)):Init(math.floor((values[i] * 1 / maxVal * this.maxBarHeight)), tostring(values[i]), annotations[i])
            i = i + 1
        end
    end
    this.table:Reposition()
end
