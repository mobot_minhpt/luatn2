local DelegateFactory  = import "DelegateFactory"
local UIScrollView  = import "UIScrollView"
local UITable = import "UITable"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local CUITexture = import "L10.UI.CUITexture"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local NGUIMath = import "NGUIMath"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaGuildLeagueCrossTrainContributionAwardView = class()

RegistClassMember(LuaGuildLeagueCrossTrainContributionAwardView, "m_HeaderTable")
RegistClassMember(LuaGuildLeagueCrossTrainContributionAwardView, "m_HeaderItemTemplate")

RegistClassMember(LuaGuildLeagueCrossTrainContributionAwardView, "m_RoundScrollView")
RegistClassMember(LuaGuildLeagueCrossTrainContributionAwardView, "m_RoundTable")
RegistClassMember(LuaGuildLeagueCrossTrainContributionAwardView, "m_RoundItemTemplate")

RegistClassMember(LuaGuildLeagueCrossTrainContributionAwardView, "m_AwardTimeInfo")
RegistClassMember(LuaGuildLeagueCrossTrainContributionAwardView, "m_RankAwardInfo")

RegistClassMember(LuaGuildLeagueCrossTrainContributionAwardView, "m_CurrentHeaderMarksTbl")

function LuaGuildLeagueCrossTrainContributionAwardView:Awake()
    self.m_HeaderTable = self.transform:Find("Header/Table"):GetComponent(typeof(UITable))
    self.m_HeaderItemTemplate = self.transform:Find("Header/Item").gameObject

    self.m_RoundScrollView = self.transform:Find("ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_RoundTable = self.transform:Find("ScrollView/Table"):GetComponent(typeof(UITable))
    self.m_RoundItemTemplate = self.transform:Find("ScrollView/Item").gameObject

    self.m_HeaderItemTemplate:SetActive(false)
    self.m_RoundItemTemplate:SetActive(false)

    self:LoadDesignData()
end

function LuaGuildLeagueCrossTrainContributionAwardView:Init()
    self:LoadHeaderInfo()
    self:LoadRoundAwardInfo()
    --query rank
    LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossTrainRankRewardInfo()
end

function LuaGuildLeagueCrossTrainContributionAwardView:LoadDesignData()
    self.m_AwardTimeInfo = LuaGuildLeagueCrossMgr:GetTrainRankAwardTimeInfo()
    self.m_RankAwardInfo  = {} -- 每个条目为 {rankStart=1,rankEnd=1, items={{rounditemlist},{rounditemlist},{rounditemlist},{rounditemlist}},
    GuildLeagueCross_TrainRankReward.Foreach(function(key, data)
        local round = math.floor(key % 10)
        local rankStart = math.floor(key/10000)
        local rankEnd = math.floor(key%10000/10)
        local record = nil
        for i=1,#self.m_RankAwardInfo do
            if self.m_RankAwardInfo[i].rankStart == rankStart then
                record = self.m_RankAwardInfo[i]
                break
            end
        end
        if record==nil then
            record = {}
            record.rankStart = rankStart
            record.rankEnd = rankEnd
            record.items = {}
            table.insert(self.m_RankAwardInfo, record)
        end
        local itemTbl = {}
        for i=0,data.Reward.Length-1 do
            table.insert(itemTbl, data.Reward[i][0])
        end
        table.insert(record.items, round, itemTbl)
    end)
    table.sort(self.m_RankAwardInfo, function(a,b)
        return a.rankStart<b.rankStart
    end)
end

function LuaGuildLeagueCrossTrainContributionAwardView:LoadHeaderInfo()
    self.m_CurrentHeaderMarksTbl= {}
    Extensions.RemoveAllChildren(self.m_HeaderTable.transform)
    for i=1, #self.m_RankAwardInfo do
        local child = CUICommonDef.AddChild(self.m_HeaderTable.gameObject, self.m_HeaderItemTemplate)
        child:SetActive(true)
        local info = self.m_RankAwardInfo[i]
        local rankLabel = child.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
        local currentGo = child.transform:Find("Current").gameObject
        rankLabel.text = info.rankStart==info.rankEnd 
                            and SafeStringFormat3(LocalString.GetString("第%d名"), info.rankStart)
                            or SafeStringFormat3(LocalString.GetString("第%d-%d名"), info.rankStart, info.rankEnd)
        currentGo:SetActive(false)
        table.insert(self.m_CurrentHeaderMarksTbl, currentGo)
    end
    self.m_HeaderTable:Reposition()
end

function LuaGuildLeagueCrossTrainContributionAwardView:LoadRoundAwardInfo()
    Extensions.RemoveAllChildren(self.m_RoundTable.transform)
    for i=1, #self.m_AwardTimeInfo do
        local child = CUICommonDef.AddChild(self.m_RoundTable.gameObject, self.m_RoundItemTemplate)
        child:SetActive(true)
        self:InitOneRoundAwardInfo(child.transform, i)
        -- if i % 2 == 1 then
        --     child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.OddBgSpirite)
        -- else
        --     child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.EvenBgSprite)
        -- end
    end
    self.m_RoundTable:Reposition()
    self.m_RoundScrollView:ResetPosition()
end

function LuaGuildLeagueCrossTrainContributionAwardView:InitOneRoundAwardInfo(root, index)
    local timeLabel = root:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local grid = root:Find("Grid"):GetComponent(typeof(UIGrid))
    local template = root:Find("RowTemplate").gameObject
    local highlightBg = root:Find("Highlight"):GetComponent(typeof(UIWidget))
    local bg = root:Find("Bg"):GetComponent(typeof(UIWidget))
    template:SetActive(false)
    highlightBg.gameObject:SetActive(False)
    local curTimeInfo = self.m_AwardTimeInfo[index]
    timeLabel.text = curTimeInfo.timeStr

    local timePassed = false
    if CServerTimeMgr.Inst.timeStamp>=curTimeInfo.timeStamp then
        --已经过去
        timePassed = true
    elseif index==1 or (index>1 and CServerTimeMgr.Inst.timeStamp>=self.m_AwardTimeInfo[index-1].timeStamp) then
        --下一轮即将结算
        highlightBg.gameObject:SetActive(true)
    else
        --下一轮之后的轮次
    end

    Extensions.RemoveAllChildren(grid.transform)
    for i=1, #self.m_RankAwardInfo do
        local child = CUICommonDef.AddChild(grid.gameObject, template)
        child:SetActive(true)
        local items = self.m_RankAwardInfo[i].items[index]
        self:LoadAwardItems(child.transform, items, timePassed, false)
    end
    grid:Reposition()
    --计算grid content width，在此基础上设置背景宽度
    local b = NGUIMath.CalculateRelativeWidgetBounds(grid.transform)
    bg.width = b.size.x + 100
    highlightBg.width = bg.width
end

function LuaGuildLeagueCrossTrainContributionAwardView:LoadAwardItems(root, items, timePassed, awarded)
    local table = root:Find("Table"):GetComponent(typeof(UITable))
    local template = root:Find("ItemCell").gameObject
    template:SetActive(false)
    Extensions.RemoveAllChildren(table.transform)
    for i=1, #items do
        local child = CUICommonDef.AddChild(table.gameObject, template)
        child:SetActive(true)
        local iconTexture = child.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
        local disableSprite = child.transform:Find("DisableSprite").gameObject
        local checkMark = child.transform:Find("Checkmark").gameObject
        local itemDesignData = Item_Item.GetData(items[i])
        iconTexture:LoadMaterial(itemDesignData.Icon)
        disableSprite:SetActive(timePassed)
        checkMark:SetActive(awarded)
        local id = items[i]
        UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function()
            CItemInfoMgr.ShowLinkItemTemplateInfo(id, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
        end)
    end
    table:Reposition()
end


function LuaGuildLeagueCrossTrainContributionAwardView:OnEnable()
    g_ScriptEvent:AddListener("SendGuildLeagueCrossTrainRankRewardInfo", self, "OnSendGuildLeagueCrossTrainRankRewardInfo")
    self:Init()
end

function LuaGuildLeagueCrossTrainContributionAwardView:OnDisable()
    g_ScriptEvent:RemoveListener("SendGuildLeagueCrossTrainRankRewardInfo", self, "OnSendGuildLeagueCrossTrainRankRewardInfo")
end

function LuaGuildLeagueCrossTrainContributionAwardView:OnSendGuildLeagueCrossTrainRankRewardInfo(rankInfoTbl)

    for __,currentGo in pairs(self.m_CurrentHeaderMarksTbl) do
        currentGo:SetActive(false)
    end

    local nearestRewardIndex = rankInfoTbl and #rankInfoTbl or 0
    local curRank = rankInfoTbl and #rankInfoTbl>0 and rankInfoTbl[#rankInfoTbl] or 0
    if curRank>0 then
        for i,record in pairs(self.m_RankAwardInfo) do
            if curRank>=record.rankStart and curRank<=record.rankEnd then
                self.m_CurrentHeaderMarksTbl[i]:SetActive(true)
            end
        end
    end

    for i=1, #self.m_AwardTimeInfo do
        local roundChild = self.m_RoundTable.transform:GetChild(i-1)
        local grid = roundChild:Find("Grid"):GetComponent(typeof(UIGrid))
        for j=0,grid.transform.childCount-1 do
            local items = self.m_RankAwardInfo[j+1].items[i]
            local root = grid.transform:GetChild(j)
            local timePassed = nearestRewardIndex>=i --奖励已经结算
            local curRank = rankInfoTbl and rankInfoTbl[i] or 0 --第i轮次的奖励结算排名
            local awarded = (curRank>=self.m_RankAwardInfo[j+1].rankStart and curRank<=self.m_RankAwardInfo[j+1].rankEnd)
            self:LoadAwardItems(root, items, timePassed, awarded)
        end
    end
end



