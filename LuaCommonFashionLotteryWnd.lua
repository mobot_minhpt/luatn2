local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CItemMgr = import "L10.Game.CItemMgr"
local Animation = import "UnityEngine.Animation"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CCPlayerCtrl = import "L10.Game.CCPlayerCtrl"
local CWinCGMgr = import "L10.Game.CWinCGMgr"
local CLoginMgr = import "L10.Game.CLoginMgr"
local Vector3 = import "UnityEngine.Vector3"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaCommonFashionLotteryWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistClassMember(LuaCommonFashionLotteryWnd, "m_BtnTable")
RegistClassMember(LuaCommonFashionLotteryWnd, "m_LotteryBtn")
RegistClassMember(LuaCommonFashionLotteryWnd, "m_GiftPackBtn")
RegistClassMember(LuaCommonFashionLotteryWnd, "m_ScoreShopBtn")
RegistClassMember(LuaCommonFashionLotteryWnd, "m_RuleBtn")
RegistClassMember(LuaCommonFashionLotteryWnd, "m_FashionView")
RegistClassMember(LuaCommonFashionLotteryWnd, "m_GiftPackView")
RegistClassMember(LuaCommonFashionLotteryWnd, "m_LotteryEnterView")
RegistClassMember(LuaCommonFashionLotteryWnd, "m_LotteryViewRoot")
RegistClassMember(LuaCommonFashionLotteryWnd, "m_LotteryModelRoot")
RegistClassMember(LuaCommonFashionLotteryWnd, "m_LotteryView")
RegistClassMember(LuaCommonFashionLotteryWnd, "m_LotteryResultView")
RegistClassMember(LuaCommonFashionLotteryWnd, "m_LotteryDiscountTip")
RegistClassMember(LuaCommonFashionLotteryWnd, "m_Animation")
RegistClassMember(LuaCommonFashionLotteryWnd, "m_AnimationTick")
RegistClassMember(LuaCommonFashionLotteryWnd, "m_ShowCGViewTick")
RegistClassMember(LuaCommonFashionLotteryWnd, "m_LotterySkipBtn")
RegistClassMember(LuaCommonFashionLotteryWnd, "m_LotteryCGView")
RegistClassMember(LuaCommonFashionLotteryWnd, "m_LotteryCGCtrl")

RegistClassMember(LuaCommonFashionLotteryWnd, "m_LotteryCGAutoStopTime")
RegistClassMember(LuaCommonFashionLotteryWnd, "m_TabBtns")
--@endregion RegistChildComponent end

function LuaCommonFashionLotteryWnd:Awake()
    self.m_Animation = self.transform:GetComponent(typeof(Animation))
    self.m_BtnTable = self.transform:Find("Anchor/Left/TabBtnTable"):GetComponent(typeof(UITable))
    self.m_LotteryBtn = self.transform:Find("Anchor/Left/TabBtnTable/FashionBtn"):GetComponent(typeof(CButton))
    self.m_GiftPackBtn = self.transform:Find("Anchor/Left/TabBtnTable/GiftPackBtn"):GetComponent(typeof(CButton))
    self.m_ScoreShopBtn = self.transform:Find("Anchor/Left/TabBtnTable/ScoreShopBtn"):GetComponent(typeof(CButton))
    self.m_RuleBtn = self.transform:Find("Anchor/Left/RuleBtn").gameObject
    self.m_GiftPackView = self.transform:Find("Anchor/GiftPackView").gameObject

    self.m_LotteryEnterView = self.transform:Find("Anchor/LotteryEnterView").gameObject
    self.m_LotteryDiscountTip = self.m_LotteryEnterView.transform:Find("GetBtn/DiscountTip"):GetComponent(typeof(UILabel))

    self.m_LotteryViewRoot = self.transform:Find("Anchor/LotteryViewRoot").gameObject
    self.m_LotteryView = self.transform:Find("Anchor/LotteryViewRoot/MainView/CommonFashionLotteryView"):GetComponent(typeof(CCommonLuaScript))
    self.m_LotteryResultView = self.transform:Find("Anchor/LotteryViewRoot/MainView/CommonFashionLotteryResultView"):GetComponent(typeof(CCommonLuaScript))
    self.m_LotteryModelRoot = self.transform:Find("Anchor/ModelRoot").gameObject
    self.m_LotterySkipBtn = self.transform:Find("Anchor/LotteryViewRoot/CGView/SkipBtn").gameObject
    self.m_LotteryCGView = self.transform:Find("Anchor/LotteryViewRoot/CGView"):GetComponent(typeof(UIPanel))
    self.m_LotteryCGCtrl = self.transform:Find("Anchor/LotteryViewRoot/CGView/Texture"):GetComponent(typeof(CCPlayerCtrl))

    UIEventListener.Get(self.m_LotterySkipBtn).onClick = DelegateFactory.VoidDelegate(function ()
        self:OnLotterySkipBtnClick()
    end)
    self.m_LotteryCGCtrl.OnBeginPlay = DelegateFactory.Action(function()
        self:OnCCPlayerBeginPlay()
    end)

    self.m_LotteryCGCtrl.OnEndPlay = DelegateFactory.Action(function()
        self:OnCCPlayerEndPlay()
    end)
end

function LuaCommonFashionLotteryWnd:Init()
    self.m_LotterySkipBtn:SetActive(false)
    self.m_LotteryCGView.alpha = 0
    local isFashionLotteryOpen = LuaFashionLotteryMgr:IsFashionLotteryOpen()
    local isDiscountPackOpen = LuaFashionLotteryMgr:IsDiscountPackOpen()
    self:InitLeftButtons(isFashionLotteryOpen, isDiscountPackOpen)
    self:InitFashionView()
    self:InitGiftPackView()
    if LuaFashionLotteryMgr.m_OpenView == "LotteryView" then
        self:OpenLotteryView()
    else
        if isFashionLotteryOpen then
            Gac2Gas:CheckFashionLotteryDiscountInfo()
            self:OnLotteryBtnClick()
        elseif isDiscountPackOpen then
            self:OnGiftPackBtnClick()
        else
            self.m_GiftPackView:SetActive(false)
        end
        self:PlayAnimation("jiuweihufashionlotterywnd_show01", 1)
    end
    local setData = FashionLottery_Settings.GetData()
    self.m_LotteryCGList_Day = setData.CloudVideoName_Day
    self.m_CloudVideoName_Night = setData.CloudVideoName_Night

    local startHour, startMin, endHour, endMin = string.match(setData.CloudVideoTime_Day,"(%d+):(%d+)-(%d+):(%d+)")
    self.CloudVideoTime_Day_StartTime = CServerTimeMgr.Inst:GetTodayTimeStampByTime(startHour,startMin,0)
    self.CloudVideoTime_Day_EndTime = CServerTimeMgr.Inst:GetTodayTimeStampByTime(endHour,endMin,0)

    self.m_LotteryCGAutoStopTime = setData.CloudVideoAutoStopTime
end

function LuaCommonFashionLotteryWnd:InitLeftButtons(isFashionLotteryOpen, isDiscountPackOpen)
    self.m_TabBtnsOpne = {isFashionLotteryOpen, isDiscountPackOpen, true}
    self.m_TabBtns = {self.m_LotteryBtn, self.m_GiftPackBtn, self.m_ScoreShopBtn}
    for i = 1, #self.m_TabBtns do
        self.m_TabBtns[i].gameObject:SetActive(false)
    end
    local wndData = FashionLottery_MainWndInfo.GetData(LuaFashionLotteryMgr.m_ActivityId)
    for i = 0, wndData.WndTabBtnList.Length - 1 do
        local index = tonumber(wndData.WndTabBtnList[i][0])
        self.m_TabBtns[index].gameObject:SetActive(true)
        local str = self.m_TabBtnsOpne[index] and wndData.WndTabBtnList[i][1] or LocalString.GetString("敬请期待")
        self.m_TabBtns[index].transform:Find("Label"):GetComponent(typeof(UILabel)).text = str
    end
    if isFashionLotteryOpen then
        UIEventListener.Get(self.m_LotteryBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnLotteryBtnClick()
        end)
    end
    if isDiscountPackOpen then
        UIEventListener.Get(self.m_GiftPackBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnGiftPackBtnClick()
        end)
    end
    UIEventListener.Get(self.m_ScoreShopBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnScoreShopBtnClick()
    end)
    UIEventListener.Get(self.m_RuleBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        g_MessageMgr:ShowMessage(wndData.WndRuleTipMsg)
    end)
end

function LuaCommonFashionLotteryWnd:InitFashionView()
    self.m_LotteryViewRoot:SetActive(false)
    self.m_LotteryEnterView:SetActive(true)
    self.m_LotteryDiscountTip.gameObject:SetActive(false)
    local fashionLotteryData = FashionLottery_Activity.GetData(LuaFashionLotteryMgr.m_ActivityId)
    if fashionLotteryData then
        self.m_LotteryEnterView.transform:Find("TimeLabel"):GetComponent(typeof(UILabel)).text = fashionLotteryData.TimeStr
        for i = 1, self.m_LotteryModelRoot.transform.childCount - 1 do
            self.m_LotteryModelRoot.transform:GetChild(i).gameObject:SetActive(false)
        end
        local getBtn = self.m_LotteryEnterView.transform:Find("GetBtn").gameObject
        UIEventListener.Get(getBtn).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OpenLotteryView()
        end)
    end
end

function LuaCommonFashionLotteryWnd:UpdateFashionLotteryDiscount(discount)
    if discount < 1 then
        self.m_LotteryDiscountTip.text = SafeStringFormat3(LocalString.GetString("首份%d折"), discount * 10)
        self.m_LotteryDiscountTip.gameObject:SetActive(true)
    else
        self.m_LotteryDiscountTip.gameObject:SetActive(false)
    end
end

function LuaCommonFashionLotteryWnd:InitGiftPackView()
    local discountData = FashionLottery_DiscountPack.GetData(LuaFashionLotteryMgr.m_ActivityId)
    if discountData then
        self.m_GiftPackView.transform:Find("TimeLabel"):GetComponent(typeof(UILabel)).text = discountData.TimeStr
        local itemList = self.m_GiftPackView.transform:Find("ItemList")
        local count = math.min(itemList.transform.childCount, discountData.ShowItemList.Length)
        for i = 0, count - 1 do
            local item = itemList.transform:GetChild(i):GetComponent(typeof(CQnReturnAwardTemplate))
            local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
            local itemTempalte = CItemMgr.Inst:GetItemTemplate(discountData.ShowItemList[i][0])
            item:Init(itemTempalte, discountData.ShowItemList[i][1])
            nameLabel.text = itemTempalte.Name
        end
        local getBtn = self.m_GiftPackView.transform:Find("GetBtn").gameObject
        UIEventListener.Get(getBtn).onClick = DelegateFactory.VoidDelegate(function (go)
            CShopMallMgr.ShowLingyuLiBaoShop()
        end)
    end
end

function LuaCommonFashionLotteryWnd:ChangeTabBtnState(index)
    for i = 1, #self.m_TabBtns do
        self.m_TabBtns[i].Selected = i == index
        local colorStr = i == index and "441A23" or "FFFFFF"
        self.m_TabBtns[i].transform:Find("Label"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24(colorStr, 0)
    end
end

function LuaCommonFashionLotteryWnd:OpenLotteryView()
    self.m_GiftPackView:SetActive(false)
    self.m_LotteryView.transform.localScale = Vector3.one
    local closeSprite = self.m_LotteryView.transform:Find("CloseBtn"):GetComponent(typeof(UISprite))
    closeSprite:ResetAndUpdateAnchors()
    self:PlayAnimation("jiuweihufashionlotterywnd_show02", 1)
end

function LuaCommonFashionLotteryWnd:OnFashionLotteryDrawResult()
    local clipName = #LuaFashionLotteryMgr.m_LotteryResult == 1 and "jiuweihufashionlotterywnd_chouka_once" or "jiuweihufashionlotterywnd_chouka01"
    self:PlayAnimation(clipName, 1)
    local sounds = FashionLottery_Settings.GetData().LotteryResultSound
    local soundEvent = #LuaFashionLotteryMgr.m_LotteryResult == 1 and sounds[0] or sounds[1]
    SoundManager.Inst:PlayOneShot(soundEvent, Vector3.zero, nil, 0)
    local cgName = self:GetCGName()

    if self.m_AnimationTick then UnRegisterTick(self.m_AnimationTick) end
    self.m_AnimationTick = RegisterTickOnce(function ()
        if CCPlayerCtrl.IsSupportted() and cgName and g_DeviceMgr:GetDeviceLevel() > 2 then
            local url = CLoginMgr.Inst:GetCDNAuthUrl(CWinCGMgr.Inst.RemoteBaseUrl .. cgName)
            self.m_LotteryCGCtrl:PlayPureUrl(url, CWinCGMgr.Inst.videoVolume)
            -- 保底关闭
            if self.m_NoCGCloseTick then UnRegisterTick(self.m_NoCGCloseTick) end
            self.m_NoCGCloseTick = RegisterTickOnce(function ()
                self.m_LotteryCGView.alpha = 0
                self.m_LotteryCGCtrl:Stop()
                self:OnCCPlayerEndPlay()
            end, 1500)
            return
        elseif not CCPlayerCtrl.IsSupportted() then
            g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("客户端引擎版本过旧，不能播放此视频，请更新版本后再试"))
        end
        self:OnCCPlayerEndPlay()
    end, 500)
    self.m_LotterySkipBtn:SetActive(not LuaFashionLotteryMgr.m_FirstLottery)
    LuaFashionLotteryMgr.m_FirstLottery = false
end

function LuaCommonFashionLotteryWnd:GetCGName()
    LuaFashionLotteryMgr:InitItemPreciousTbl()
    
    local fashionType = {}
    for i = 1, #LuaFashionLotteryMgr.m_LotteryResult do
        local rewardInfo = LuaFashionLotteryMgr.m_LotteryResult[i]
        if rewardInfo then
            fashionType[rewardInfo.Precious] = true
        end
    end

    local lotteryCGList = self:PlayDayCG() and self.m_LotteryCGList_Day or self.m_CloudVideoName_Night

    if #LuaFashionLotteryMgr.m_LotteryResult == 1 then
        if fashionType[2] then
            return lotteryCGList[2]
        elseif fashionType[1] then
            return lotteryCGList[1]
        else
            return lotteryCGList[0]
        end
    else
        if fashionType[1] and fashionType[2] then
            return lotteryCGList[5]
        elseif fashionType[2] then
            return lotteryCGList[4]
        else
            return lotteryCGList[3]
        end
    end
    return nil
end

function LuaCommonFashionLotteryWnd:PlayDayCG()
    local nowTime = CServerTimeMgr.Inst.timeStamp
    if nowTime >= self.CloudVideoTime_Day_StartTime and nowTime <= self.CloudVideoTime_Day_EndTime then return true end
end

function LuaCommonFashionLotteryWnd:OnCCPlayerBeginPlay()
    if self.m_NoCGCloseTick then UnRegisterTick(self.m_NoCGCloseTick) end
    if self.m_ShowCGViewTick then UnRegisterTick(self.m_ShowCGViewTick) end
    self.m_ShowCGViewTick = RegisterTickOnce(function ()
        self.m_LotteryCGView.alpha = 1
    end, 200)
    local autoStopTime = self.m_LotteryCGAutoStopTime[#LuaFashionLotteryMgr.m_LotteryResult == 1 and 0 or 1]
    -- 保底关闭
    if self.m_PlayCGTick then UnRegisterTick(self.m_PlayCGTick) end
    self.m_PlayCGTick = RegisterTickOnce(function ()
        self.m_LotteryCGView.alpha = 0
        if self.m_LotteryCGCtrl:IsPlaying() then self.m_LotteryCGCtrl:Stop()
        else self:OnCCPlayerEndPlay() end
    end, autoStopTime)
end

function LuaCommonFashionLotteryWnd:OnCCPlayerEndPlay()
    self.m_LotteryCGView.alpha = 0
    self:PlayAnimation("jiuweihufashionlotterywnd_tiaoguo", 1)
    local sounds = FashionLottery_Settings.GetData().LotteryResultSound
    local index = #LuaFashionLotteryMgr.m_LotteryResult > 1 and #LuaFashionLotteryMgr.m_FashionRewardList == 0 and 3 or 2
    if self.m_LotteryResultSound then SoundManager.Inst:StopSound(self.m_LotteryResultSound) end
    self.m_LotteryResultSound = SoundManager.Inst:PlayOneShot(sounds[index], Vector3.zero, nil, 0)
    if self.m_PlayCGTick then UnRegisterTick(self.m_PlayCGTick) end
end

function LuaCommonFashionLotteryWnd:PlayAnimation(clipName, speed)
    if self.m_LotteryResultSound then SoundManager.Inst:StopSound(self.m_LotteryResultSound) end
    local clip = self.m_Animation:get_Item(clipName)
    clip.speed = speed
    clip.time = speed > 0 and 0 or clip.length
    self.m_Animation:Play(clipName)
    return clip.length
end

--@region UIEvent
function LuaCommonFashionLotteryWnd:OnLotteryBtnClick()
    self.m_LotteryModelRoot:SetActive(true)
    self.m_LotteryEnterView:SetActive(true)
    self.m_GiftPackView:SetActive(false)
    local defaltModel = self.m_LotteryModelRoot.transform:GetChild(0)
    defaltModel.gameObject:SetActive(true)
    self:ChangeTabBtnState(1)
end

function LuaCommonFashionLotteryWnd:OnGiftPackBtnClick()
    self.m_LotteryModelRoot:SetActive(false)
    self.m_LotteryEnterView:SetActive(false)
    self.m_GiftPackView:SetActive(true)
    self:ChangeTabBtnState(2)
end

function LuaCommonFashionLotteryWnd:OnScoreShopBtnClick()
    CLuaNPCShopInfoMgr.ShowScoreShop("AppearanceScore")
end

function LuaCommonFashionLotteryWnd:OnLotterySkipBtnClick()
    self.m_LotteryCGView.alpha = 0
    if self.m_LotteryCGCtrl:IsPlaying() then self.m_LotteryCGCtrl:Stop()
    else self:OnCCPlayerEndPlay() end
end
--@endregion UIEvent

function LuaCommonFashionLotteryWnd:OnEnable()
	g_ScriptEvent:AddListener("FashionLotteryDrawResult", self, "OnFashionLotteryDrawResult")
	g_ScriptEvent:AddListener("CheckFashionLotteryDiscountInfoResult", self, "UpdateFashionLotteryDiscount")
	g_ScriptEvent:AddListener("PlayFashionLotteryAnimation", self, "PlayAnimation")
end

function LuaCommonFashionLotteryWnd:OnDisable()
    self.m_Animation:Stop()
    self.m_LotteryCGCtrl:Stop()
    if self.m_AnimationTick then UnRegisterTick(self.m_AnimationTick) end
    if self.m_ShowCGViewTick then UnRegisterTick(self.m_ShowCGViewTick) end
    if self.m_NoCGCloseTick then UnRegisterTick(self.m_NoCGCloseTick) end
    if self.m_PlayCGTick then UnRegisterTick(self.m_PlayCGTick) end
    if self.m_LotteryResultSound then SoundManager.Inst:StopSound(self.m_LotteryResultSound) end
	g_ScriptEvent:RemoveListener("FashionLotteryDrawResult", self, "OnFashionLotteryDrawResult")
    g_ScriptEvent:RemoveListener("CheckFashionLotteryDiscountInfoResult", self, "UpdateFashionLotteryDiscount")
	g_ScriptEvent:RemoveListener("PlayFashionLotteryAnimation", self, "PlayAnimation")
end