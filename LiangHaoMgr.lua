local CFashionMgr = import "L10.Game.CFashionMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EnumClass = import "L10.Game.EnumClass"
local CUIManager = import "L10.UI.CUIManager"
local LocalString = import "LocalString"
local Vector3 = import "UnityEngine.Vector3"
local CIMMgr = import "L10.Game.CIMMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CItemMgr = import "L10.Game.CItemMgr"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"

LuaLiangHaoMgr = class()
LuaLiangHaoMgr.TargetWorldCenter = nil
LuaLiangHaoMgr.TargetSize = nil
LuaLiangHaoMgr.CheckTbl = {}

LuaLiangHaoMgr.RenewPlayerInfo = {} -- id, name, lianghaoId, oldExpireTime, renewPrice, newExpireTime
LuaLiangHaoMgr.PurchaseLiangHaoInfo = {} -- purchaseTbl, auctionTbl, auctionExpireTime
LuaLiangHaoMgr.AuctionLiangHaoInfo = {} -- lianghaoId, topBidderId, topBidderName, topBidderPrice, minPrice, maxPrice
LuaLiangHaoMgr.MyAuctionLiangHaoInfo = {}

LuaLiangHaoMgr.DingZhiItemInfo = {} -- itemId, itemplace, itempos, digitnum, minVal, maxVal
LuaLiangHaoMgr.ReqCustomizeCardInfo = {}
LuaLiangHaoMgr.MyReqCustomizeCardInfo = {}

function LuaLiangHaoMgr.LiangHaoEnabled()
	--C#下面有个同样的判断CIMMgr.Inst.LiangHaoEnabled，由于热更问题这里暂时先不引用C#的
	return true
end

function LuaLiangHaoMgr.GetLiangHaoItemTemplateId()
	return 21050514 --没有转策划表，写死
end

function LuaLiangHaoMgr.RequestExchangeLiangHaoItem(itemId, place, pos)
	local item = CItemMgr.Inst:GetById(itemId)
	if not item then return end
	if item.Item.ExtraVarData ~= nil and item.Item.ExtraVarData.Data ~= nil then
        local raw = item.Item.ExtraVarData.Data
        local list = TypeAs(MsgPackImpl.unpack(raw), typeof(MakeGenericClass(List, Object)))
        if list and list.Count >= 2 then
            local lianghaoId = tonumber(list[0])
            local expiredTime = list[1]
            Gac2Gas.QueryLiangHaoSurplusYuanBaoValue(lianghaoId, SafeStringFormat3("exchangelianghao,%s,%s,%s", itemId, place, pos))
        end
    end	
end

function LuaLiangHaoMgr.ReplyLiangHaoSurplusYuanBaoValue(lianghaoId, context, buyPrice, expiredTime, retYuanBao)
	if string.find(context,"exchangelianghao",1, true)~=nil then
		local msg = g_MessageMgr:FormatMessage("LiangHao_GiveupTOYuanBao_Confirm", lianghaoId, retYuanBao)
	    MessageWndManager.ShowDelayOKCancelMessage(msg, DelegateFactory.Action(function ()
	        LuaLiangHaoMgr.DoExchangeLiangHaoItem(context)
	    end), nil, 10)
	end
end

function LuaLiangHaoMgr.DoExchangeLiangHaoItem(context)
	local itemId, place, pos = string.match(context, "exchangelianghao,(.+),(%d+),(%d+)")
	if itemId and place and pos then
		Gac2Gas.RequestExchangeLiangHaoItem(itemId, tonumber(place), tonumber(pos))
	end
end

--显示外观设置界面，内有靓号入口
function LuaLiangHaoMgr.ShowPlayerAppearanceSettingWnd(anchorTransform)
	local mainplayer = CClientMainPlayer.Inst
	if not mainplayer then return end

	local tbl = {}
	table.insert(tbl, {text=LocalString.GetString("显示头盔"), action = function (checked) LuaLiangHaoMgr.OnTouKuiCheckStatusChanged(checked) end , checked = (mainplayer.AppearanceProp.HideHelmetEffect == 0)})
	
	if LuaLiangHaoMgr.CanHoldDunpaiClass() then
		table.insert(tbl, {text=LocalString.GetString("显示盾牌"), action = function (checked) LuaLiangHaoMgr.OnDunPaiCheckStatusChanged(checked) end , checked = (mainplayer.AppearanceProp.HideDunPai == 0)})
	end
	
	table.insert(tbl, {text=LocalString.GetString("显示神兵外观——衣服"), action = function (checked) LuaLiangHaoMgr.OnShenBingClothCheckStatusChanged(checked) end , checked = (mainplayer.AppearanceProp.HideShenBingClothes == 0)})

	table.insert(tbl, {text=LocalString.GetString("显示神兵外观——帽子"), action = function (checked) LuaLiangHaoMgr.OnShenBingHeadwareCheckStatusChanged(checked) end , checked = (mainplayer.AppearanceProp.HideShenBingHeadwear == 0)})
	
	LuaLiangHaoMgr.CheckTbl = tbl

	if anchorTransform then
		LuaLiangHaoMgr.TargetWorldCenter = anchorTransform.position
		LuaLiangHaoMgr.TargetSize = NGUIMath.CalculateRelativeWidgetBounds(anchorTransform).size
	else
		LuaLiangHaoMgr.TargetWorldCenter = Vector3(0,0,0)
		LuaLiangHaoMgr.TargetSize = Vector3(0,0,0)
	end


	CUIManager.ShowUI("PlayerAppearanceSettingWnd")
end

function LuaLiangHaoMgr.ShowLiangHaoSettingWnd()
	CUIManager.ShowUI("LiangHaoSettingWnd")
end

function LuaLiangHaoMgr.RequestSetLiangHaoPrivilege(tp, val)
	Gac2Gas.RequestSetLiangHaoPrivilege(tp, val)
end

function LuaLiangHaoMgr.RequestGiveUpLiangHao()
	Gac2Gas.RequestGiveUpLiangHao()
end

function LuaLiangHaoMgr.OnTouKuiCheckStatusChanged(checked)
	CFashionMgr.Inst:RequestSetHelmetHide(checked)
end

function LuaLiangHaoMgr.OnDunPaiCheckStatusChanged(checked)
	CFashionMgr.Inst:RequestSetDunPaiHide(checked)
end

function LuaLiangHaoMgr.OnShenBingClothCheckStatusChanged(checked)
	CFashionMgr.Inst:SetPropertyAppearanceHideShenBingClothesInfo(checked)
end

function LuaLiangHaoMgr.OnShenBingHeadwareCheckStatusChanged(checked)
	CFashionMgr.Inst:SetPropertyAppearanceHideShenBingHeadwearInfo(checked)
end


function LuaLiangHaoMgr.CanHoldDunpaiClass()
	local mainplayer = CClientMainPlayer.Inst
	local cls = mainplayer and mainplayer.Class
	if cls then
		return cls == EnumClass.JiaShi or cls == EnumClass.XiaKe or cls == EnumClass.DaoKe or cls == EnumClass.YanShi
	end
	return false
end

-----------------------Begin 续费
function LuaLiangHaoMgr.ShowLiangHaoRenewWnd(targetId, lianghaoId)
	LuaLiangHaoMgr.QueryLiangHaoRenewInfo(targetId, lianghaoId)
end
--查询靓号续费价格
function LuaLiangHaoMgr.QueryLiangHaoRenewInfo(playerId, lianghaoId)
	CUIManager.CloseUI("LiangHaoRenewWnd") --查询之前先关闭
	Gac2Gas.QueryLiangHaoRenewInfo(playerId, lianghaoId)
end

function LuaLiangHaoMgr.ReplyLiangHaoRenewInfo(targetId, lianghaoId, renewPrice, oldExpiredTime, newExpiredTime, addDay)
	if CClientMainPlayer.Inst==nil then return end
	
	LuaLiangHaoMgr.RenewPlayerInfo = {}
	LuaLiangHaoMgr.RenewPlayerInfo.id = targetId
	LuaLiangHaoMgr.RenewPlayerInfo.lianghaoId = lianghaoId
	LuaLiangHaoMgr.RenewPlayerInfo.renewPrice = renewPrice
	LuaLiangHaoMgr.RenewPlayerInfo.oldExpiredTime = oldExpiredTime
	LuaLiangHaoMgr.RenewPlayerInfo.newExpiredTime = newExpiredTime
	LuaLiangHaoMgr.RenewPlayerInfo.addDay = addDay
	if CClientMainPlayer.Inst.Id == targetId then
		LuaLiangHaoMgr.RenewPlayerInfo.name = CClientMainPlayer.Inst.Name
		CUIManager.ShowUI("LiangHaoRenewWnd")
	elseif CIMMgr.Inst:IsMyFriend(targetId) then
		local basicInfo = CIMMgr.Inst:GetBasicInfo(targetId)
		LuaLiangHaoMgr.RenewPlayerInfo.name = basicInfo.Name
		CUIManager.ShowUI("LiangHaoRenewWnd")
	end
end

function LuaLiangHaoMgr.RequestRenewLiangHao(targetId, targetName, lianghaoId, renewPrice)
	Gac2Gas.RequestRenewLiangHao(targetId, targetName, lianghaoId, renewPrice)
end

function LuaLiangHaoMgr.ShowLiangHaoFriendListWnd()
	CUIManager.ShowUI("LiangHaoFriendListWnd")
end
-----------------------End 续费

-----------------------Begin 购买
function LuaLiangHaoMgr.ShowLiangHaoPurchaseWnd()
	Gac2Gas.QueryLiangHaoSellInfo("gac")
end

function LuaLiangHaoMgr.RefreshLiangHaoPurchaseInfo()
	Gac2Gas.QueryLiangHaoSellInfo("refresh")
end

function LuaLiangHaoMgr.ReplyLiangHaoPurchaseInfo(purchaseTbl, auctionTbl, customizeTbl, auctionExpireTime, nextStartTime, tp)
	LuaLiangHaoMgr.PurchaseLiangHaoInfo = {} -- purchaseTbl, auctionTbl, auctionExpireTime
	LuaLiangHaoMgr.PurchaseLiangHaoInfo.purchaseTbl = purchaseTbl or {}
	LuaLiangHaoMgr.PurchaseLiangHaoInfo.auctionTbl = auctionTbl or {}
	LuaLiangHaoMgr.PurchaseLiangHaoInfo.customizeTbl = customizeTbl or {}
	LuaLiangHaoMgr.PurchaseLiangHaoInfo.auctionExpireTime = auctionExpireTime
	LuaLiangHaoMgr.PurchaseLiangHaoInfo.auctionNextStartTime = nextStartTime

	table.sort(LuaLiangHaoMgr.PurchaseLiangHaoInfo.purchaseTbl, function (a, b)
		return a.lianghaoId < b.lianghaoId --升序排列，ID必然不同，如果相同说明有错
	end)

	table.sort(LuaLiangHaoMgr.PurchaseLiangHaoInfo.auctionTbl, function (a, b)
		if a.count~=b.count then
			return b.count < a.count
		end
		return a.lianghaoId < b.lianghaoId --升序排列，ID必然不同，如果相同说明有错
	end)

	if tp == EnumLiangHaoPurchaseInfoReplyType.eGacRequest then
		CUIManager.ShowUI("LiangHaoPurchaseWnd")

	elseif tp == EnumLiangHaoPurchaseInfoReplyType.ePurchase then
		g_ScriptEvent:BroadcastInLua("OnLiangHaoPurchaseInfoReply", tp)

	elseif tp == EnumLiangHaoPurchaseInfoReplyType.eAuction then
		g_ScriptEvent:BroadcastInLua("OnLiangHaoPurchaseInfoReply", tp)

	elseif tp == EnumLiangHaoPurchaseInfoReplyType.eCustomize then
		g_ScriptEvent:BroadcastInLua("OnLiangHaoPurchaseInfoReply", tp)
		
	elseif tp == EnumLiangHaoPurchaseInfoReplyType.eRefresh then
		g_MessageMgr:ShowMessage("Lianghao_Refresh_Tips")
		g_ScriptEvent:BroadcastInLua("OnLiangHaoPurchaseInfoReply", tp)
	end
end

function LuaLiangHaoMgr.RequestBuyLiangHao(lianghaoId, buyPrice)
	Gac2Gas.RequestBuyLiangHao(lianghaoId, buyPrice)
end

function LuaLiangHaoMgr.QueryLiangHaoAuctionInfo(lianghaoId)
	Gac2Gas.QueryLiangHaoJingPaiInfo(lianghaoId)
end

function LuaLiangHaoMgr.RequestAuctionLiangHao(lianghaoId, price)
	Gac2Gas.RequestJingPaiLiangHao(lianghaoId, price)
end

function LuaLiangHaoMgr.ReplyLiangHaoAuctionInfo(lianghaoId, roleId, roleName, serverName, currentPrice, myPrice, minPrice, maxPrice, priceStep)
	local data = {}
	data.lianghaoId = lianghaoId
	data.topBidderId = roleId
	data.myPrice = myPrice
	if roleId == 0 then
		data.topBidderName = ""
	else
		data.topBidderName = roleName..LocalString.GetString("·")..serverName
	end

	data.topBidderPrice = currentPrice
	data.minPrice = minPrice
	data.maxPrice = maxPrice
	data.priceStep = priceStep

	LuaLiangHaoMgr.AuctionLiangHaoInfo = data
	CUIManager.ShowUI("LiangHaoAuctionWnd")
end

function LuaLiangHaoMgr.QueryMyLiangHaoAuctionInfo()
	Gac2Gas.QueryMyLiangHaoJingPaiInfo()
end

function LuaLiangHaoMgr.ReplyMyLiangHaoAuctionInfo(dataTbl)
	LuaLiangHaoMgr.MyAuctionLiangHaoInfo = dataTbl or {}
	CUIManager.ShowUI("LiangHaoMyAuctionWnd")
end
-----------------------End 购买

-----------------------Begin 定制
function LuaLiangHaoMgr.GetCardTitleName(digitNum)
	return SafeStringFormat3(LocalString.GetString("[c][FFFE91]%s位[-][/c]靓号定制卡"), digitNum)
end

function LuaLiangHaoMgr.OpenLiangHaoDingZhiWnd(itemId, place, pos, digitNum, from, to)
	LuaLiangHaoMgr.DingZhiItemInfo = {}
	LuaLiangHaoMgr.DingZhiItemInfo.itemId = itemId
	LuaLiangHaoMgr.DingZhiItemInfo.place = place
	LuaLiangHaoMgr.DingZhiItemInfo.pos = pos
	LuaLiangHaoMgr.DingZhiItemInfo.digitnum = digitNum
	--TODO 换成服务器下发的数值
	LuaLiangHaoMgr.DingZhiItemInfo.minVal = from
	LuaLiangHaoMgr.DingZhiItemInfo.maxVal = to
	CUIManager.ShowUI("LiangHaoCustomizeWnd")
end

function LuaLiangHaoMgr.CloseLiangHaoDingZhiWnd()
	CUIManager.CloseUI("LiangHaoCustomizeWnd")
end

function LuaLiangHaoMgr.RequestCalcLiangHaoPrice(lianghaoId, context)
	Gac2Gas.RequestCalcLiangHaoPrice(lianghaoId, context)
end

function LuaLiangHaoMgr.RequestBuyDingZhiLiangHao(lianghaoId, price)
	Gac2Gas.RequestBuyDingZhiLiangHao(lianghaoId, LuaLiangHaoMgr.DingZhiItemInfo.itemId, 
		LuaLiangHaoMgr.DingZhiItemInfo.place, LuaLiangHaoMgr.DingZhiItemInfo.pos, price)
end

--打开定制卡我的申请页面
function LuaLiangHaoMgr.OpenMyAuctionCustomizeCardWnd()
	Gac2Gas.QueryMyLiangHaoDingZhiApplyInfo()
end
--服务器返回定制卡我的申请信息
function LuaLiangHaoMgr.ReplyMyAuctionCustomizeCardInfo(dataTbl)
	LuaLiangHaoMgr.MyReqCustomizeCardInfo = dataTbl or {}
	CUIManager.ShowUI("LiangHaoMyReqCustomizeCardWnd")
end
--从申请定制卡/提高出价入口打开申请定制卡页面
function LuaLiangHaoMgr.OpenAuctionCustomizeCardWnd(digitNum)
	Gac2Gas.QueryLiangHaoDingZhiInfo(digitNum)
end
--返回定制卡目前的信息
function LuaLiangHaoMgr.ReplyAuctionCustomizeCardInfo(digitNum, toufangNum, playerNum, roleId, roleName, serverName, currentPrice, myPrice, minPrice, maxPrice, priceStep)
	local data = {}
	data.digitNum = digitNum
	data.toufangNum = toufangNum
	data.playerNum = playerNum
	data.roleId = roleId
	if roleId == 0 then
		data.roleName = ""
	else
		data.roleName = roleName..LocalString.GetString("·")..serverName
	end
	data.currentPrice = currentPrice
	data.myPrice = myPrice
	data.minPrice = minPrice
	data.maxPrice = maxPrice
	data.priceStep = priceStep
	LuaLiangHaoMgr.ReqCustomizeCardInfo = data
	CUIManager.ShowUI("LiangHaoReqCustomizeCardWnd")
end
--出价竞拍定制卡
function LuaLiangHaoMgr.DoAuctionCustomizeCard(digitNum, buyPrice)
	Gac2Gas.AuctionLiangHaoDingZhiItem(digitNum, buyPrice)
end

-----------------------End 定制

