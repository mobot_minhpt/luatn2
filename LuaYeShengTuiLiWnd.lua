local CButton = import "L10.UI.CButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UIPanel = import "UIPanel"
local UITexture = import "UITexture"
local Extensions = import "Extensions"
local UILabel = import "UILabel"
local BoxCollider = import "UnityEngine.BoxCollider"
local TouchPhase = import "UnityEngine.TouchPhase"
local Vector3 = import "UnityEngine.Vector3"
local LuaTweenUtils = import "LuaTweenUtils"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Input = import "UnityEngine.Input"
local UISprite = import "UISprite"
local System = import "System"
LuaYeShengTuiLiWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYeShengTuiLiWnd, "StoryNode", "StoryNode", GameObject)
RegistChildComponent(LuaYeShengTuiLiWnd, "StoryTable", "StoryTable", UITable)
RegistChildComponent(LuaYeShengTuiLiWnd, "Clue", "Clue", GameObject)
RegistChildComponent(LuaYeShengTuiLiWnd, "ClueTable", "ClueTable", GameObject)
RegistChildComponent(LuaYeShengTuiLiWnd, "CompleteButton", "CompleteButton", CButton)
RegistChildComponent(LuaYeShengTuiLiWnd, "ShoryTemplateLabel", "ShoryTemplateLabel", GameObject)
RegistChildComponent(LuaYeShengTuiLiWnd, "FillInTemplateLabel", "FillInTemplateLabel", GameObject)
RegistChildComponent(LuaYeShengTuiLiWnd, "ParagraphTemplate", "ParagraphTemplate", GameObject)
RegistChildComponent(LuaYeShengTuiLiWnd, "ClueTemplateLabel", "ClueTemplateLabel", GameObject)
RegistChildComponent(LuaYeShengTuiLiWnd, "ChooseNode", "ChooseNode", GameObject)
RegistChildComponent(LuaYeShengTuiLiWnd, "MalePortrait", "MalePortrait", GameObject)
RegistChildComponent(LuaYeShengTuiLiWnd, "FemalePortrait", "FemalePortrait", GameObject)
RegistChildComponent(LuaYeShengTuiLiWnd, "ClueObjPanel", "ClueObjPanel", GameObject)
RegistChildComponent(LuaYeShengTuiLiWnd, "CloneTemplateLabel", "CloneTemplateLabel", GameObject)
RegistChildComponent(LuaYeShengTuiLiWnd, "ScrollView", "ScrollView", UIScrollView)

--@endregion RegistChildComponent end
RegistClassMember(LuaYeShengTuiLiWnd, "m_ChooseStory")
RegistClassMember(LuaYeShengTuiLiWnd, "m_ClueId")
RegistClassMember(LuaYeShengTuiLiWnd, "m_ClueId2Index")
RegistClassMember(LuaYeShengTuiLiWnd, "m_ClueObj")
RegistClassMember(LuaYeShengTuiLiWnd, "m_ClueIdDic")
RegistClassMember(LuaYeShengTuiLiWnd, "m_ChooseNodePanel")
RegistClassMember(LuaYeShengTuiLiWnd, "m_StoryNodePanel")
RegistClassMember(LuaYeShengTuiLiWnd, "m_TempPortrait")
RegistClassMember(LuaYeShengTuiLiWnd, "m_CurrClueDrag")
RegistClassMember(LuaYeShengTuiLiWnd, "m_OnDragClueObjIndex")
RegistClassMember(LuaYeShengTuiLiWnd, "m_CompleteClue")
RegistClassMember(LuaYeShengTuiLiWnd, "m_CloneClueObj")
RegistClassMember(LuaYeShengTuiLiWnd, "m_LastDragPos")
RegistClassMember(LuaYeShengTuiLiWnd, "m_FingerIndex")
RegistClassMember(LuaYeShengTuiLiWnd, "m_LastClueId")
RegistClassMember(LuaYeShengTuiLiWnd, "m_LastClueInfo")
RegistClassMember(LuaYeShengTuiLiWnd, "m_ParagraphObj")
RegistClassMember(LuaYeShengTuiLiWnd, "m_Tick")
RegistClassMember(LuaYeShengTuiLiWnd, "m_IsInTypeWriterEffect")

EnumYeShengChooseRole = {
	eNone = 100,	
	eMale = 101,	-- 选择男角色
	eFemale = 102,	-- 选择女角色
}
function LuaYeShengTuiLiWnd:Awake()

	self.m_IsInTypeWriterEffect = false
	-- 线索IDlabel
	self.m_ClueId = {}
	-- 线索ID对应的线索框Obj
	self.m_ClueObj = {}
	self.m_ParagraphObj = {}
	-- 当前选择的男女方故事
	self.m_ChooseStory = EnumYeShengChooseRole.eNone
    self.StoryNode.gameObject:SetActive(false)
    self.ChooseNode.gameObject:SetActive(true)
	-- panel初始化
	self.m_ChooseNodePanel = self.ChooseNode.transform:Find("Panel"):GetComponent(typeof(UIPanel))
	self.m_StoryNodePanel = self.StoryNode.transform:Find("Panel"):GetComponent(typeof(UIPanel))
	self.m_StoryNodePanel.alpha = 0
	self.m_ChooseNodePanel.alpha = 1

	self.m_CurrClueDrag = -1		--正在拖拽线索框的线索ID，为-1表示不在拖拽
	self.m_OnDragClueObjIndex = -1 -- 正在拖拽的线索框对应的Obj下标
	self.m_FingerIndex = -1
	self.m_CompleteClue = {}	-- 已经完成的线索
	self.m_LastDragPos = nil	-- 拖拽时的鼠标位置
	self.m_ClueId2Index = {}
	-- 拖拽时的临时Obj
	self.m_CloneClueObj = self.CloneTemplateLabel
	self.m_CloneClueObj.gameObject:SetActive(false)

	self.m_ClueIdDic = {}
	self.m_LastClueInfo = nil
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.CompleteButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCompleteButtonClick()
	end)


    --@endregion EventBind end
end

function LuaYeShengTuiLiWnd:Init()
	self:InitChooseNode()
end
-- 初始化Choose界面
function LuaYeShengTuiLiWnd:InitChooseNode()
	self:InitPortrait(self.MalePortrait)
	self:InitPortrait(self.FemalePortrait)
end

-- 初始化头像和点击事件
function LuaYeShengTuiLiWnd:InitPortrait(go)
	go.transform:GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24("888888",0)
	go.transform:Find("BlueButton").gameObject:SetActive(false)
	UIEventListener.Get(go.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		go.transform:GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24("ffffff",0)
		go.transform:Find("BlueButton").gameObject:SetActive(true)
		if go == self.MalePortrait then
			self.m_ChooseStory = EnumYeShengChooseRole.eMale
			self.FemalePortrait.transform:GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24("888888",0)
			self.FemalePortrait.transform:Find("BlueButton").gameObject:SetActive(false)
		elseif go == self.FemalePortrait then
			self.m_ChooseStory = EnumYeShengChooseRole.eFemale
			self.MalePortrait.transform:GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24("888888",0)
			self.MalePortrait.transform:Find("BlueButton").gameObject:SetActive(false)
		end
	end)
	UIEventListener.Get(go.transform:Find("BlueButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:InitStoryNode()
	end)
end
-- 面板切换动画
function LuaYeShengTuiLiWnd:PlayChangeNodeAnim()
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
	end
	local storyNodeTick = nil 

	local TotalTime = 500	-- 动画总时间0.5s
	local Interval = 20
	local chooseResult = 0
	local storyResult = 1

	-- choose淡出动画
	self.m_Tick = RegisterTickWithDuration(function ()
		if math.abs(self.m_ChooseNodePanel.alpha - chooseResult)< 0.01 then
			self.m_ChooseNodePanel.alpha = chooseResult
			UnRegisterTick(self.m_Tick)
			self.m_Tick = nil
			return
		end
		self.m_ChooseNodePanel.alpha = math.max(self.m_ChooseNodePanel.alpha - Interval/TotalTime,chooseResult)
	end,Interval, TotalTime)

	-- 头像左移动画
	
	if self.m_TempPortrait == nil then return end
	local posxResult = -610
	local ShowTween = LuaTweenUtils.DOLocalMoveX(self.m_TempPortrait.transform,posxResult,0.5,false)
	LuaTweenUtils.SetDelay(ShowTween,0.5)
	LuaTweenUtils.OnComplete(ShowTween, function()
		self.m_StoryNodePanel.gameObject:SetActive(true)
		self.ChooseNode.gameObject:SetActive(false)
	-- story界面淡入动画
		if self.m_Tick then
			UnRegisterTick(self.m_Tick)
		end
		self.m_Tick = RegisterTickWithDuration(function ()
			if math.abs(self.m_StoryNodePanel.alpha - storyResult )< 0.01 then
				UnRegisterTick(self.m_Tick)
				self.m_Tick = nil
				return
			end
			self.m_StoryNodePanel.alpha = math.min(self.m_StoryNodePanel.alpha + Interval/TotalTime,storyResult)
		end,Interval, TotalTime)
	end)
end
-- 初始化Story面板
function LuaYeShengTuiLiWnd:InitStoryNode()
	self.m_TempPortrait = nil
	local StoryId = nil
	if self.m_ChooseStory == EnumYeShengChooseRole.eMale then
		self.MalePortrait.gameObject:SetActive(false)
		self.m_TempPortrait = CUICommonDef.AddChild(self.StoryNode.gameObject,self.MalePortrait.gameObject)
		Extensions.SetLocalPositionX(self.m_TempPortrait.transform,self.MalePortrait.transform.localPosition.x)
		Extensions.SetLocalPositionY(self.m_TempPortrait.transform,self.MalePortrait.transform.localPosition.y)
	elseif self.m_ChooseStory == EnumYeShengChooseRole.eFemale then
		self.FemalePortrait.gameObject:SetActive(false)
		self.m_TempPortrait = CUICommonDef.AddChild(self.StoryNode.gameObject,self.FemalePortrait.gameObject)
		Extensions.SetLocalPositionX(self.m_TempPortrait.transform,self.FemalePortrait.transform.localPosition.x)
		Extensions.SetLocalPositionY(self.m_TempPortrait.transform,self.FemalePortrait.transform.localPosition.y)
	end
	if self.m_TempPortrait ~= nil then
		self.m_TempPortrait.transform:GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24("ffffff",0)
		self.m_TempPortrait.transform:Find("BlueButton").gameObject:SetActive(false)
		self.m_TempPortrait.gameObject:SetActive(true)
	end	
	-- 读表读取故事对应段落Id
	StoryId = g_LuaUtil:StrSplit(ZhuJueJuQing_YeShengGroup.GetData(self.m_ChooseStory).ParagraphID,';')
	-- 初始化Story界面
	self.StoryNode.gameObject:SetActive(true)
	self.m_StoryNodePanel.gameObject:SetActive(false)
	self.StoryNode.transform:Find("TemplateLabel").gameObject:SetActive(false)
	self:InitStoryPanel(StoryId)

	-- Choose界面淡出，头像右移,Story界面淡入
	self:PlayChangeNodeAnim()
end
-- 初始化stroy界面
function LuaYeShengTuiLiWnd:InitStoryPanel(StoryId)
	if StoryId == nil then return end

	Extensions.RemoveAllChildren(self.StoryTable.transform)
	Extensions.RemoveAllChildren(self.ClueTable.transform)
	self.Clue.gameObject:SetActive(false)
	self.CompleteButton.gameObject:SetActive(false)
	for i,v in ipairs(StoryId) do
		if tonumber(v) then
			local ParagraphStr = ZhuJueJuQing_YeShengParagraph.GetData(tonumber(v)).Paragraph
			self:InitParagraph(ParagraphStr,i)
		end 
	end
	self.StoryTable:Reposition()
	self.ScrollView:ResetPosition()
	self:InitClueTable()
end
-- 初始化一个段落
function LuaYeShengTuiLiWnd:InitParagraph(str,index)
	if str == nil then return end
	local SplitStoryList = {}
	-- [url=xxx,xxx]_____[/url]
	local TransUrl = function(str)
		local ClueId = tonumber(string.sub(str,2,string.len(str)-1))
		if not ClueId then return end
		self:AddClueId(ClueId)
		local ClueInfo = self.m_ClueIdDic[ClueId]
		local ClueLen = CommonDefs.IS_VN_CLIENT and CUICommonDef.GetStrByteLength(ClueInfo.Story) or CUICommonDef.GetStrChsLength(ClueInfo.Story) 
		local res = "[url="..tostring(ClueId)..","..index.."]"..
		"[b]"..string.rep("-",ClueLen).."[/b]".."[/url]"
		return res
	end
	local paragraphStr = "        "..string.gsub(str,"({%d+})",function(str) return TransUrl(str) end)
	local paragraph = CUICommonDef.AddChild(self.StoryTable.gameObject,self.ParagraphTemplate)
	local paragraphText = paragraph.gameObject:GetComponent(typeof(UILabel))
	paragraph.gameObject:SetActive(true)
	paragraph.gameObject:GetComponent(typeof(UILabel)).text = paragraphStr
	local collider = paragraph.gameObject:GetComponent(typeof(BoxCollider))
	collider.size = Vector3(paragraphText.localSize.x,paragraphText.localSize.y,0)
	self.m_ParagraphObj[index] = paragraph
	--for clueId in string.gmatch(str,"({%d+})") do
	--	print(clueId)
	--end
end
-- 初始化线索并乱序线索排列
function LuaYeShengTuiLiWnd:InitClueTable()
	if self.m_ClueId == nil then return end
	g_LuaUtil:RandomShuffleArray(self.m_ClueId)
	self:CreateClueObj()
end
-- 创建线索框
function LuaYeShengTuiLiWnd:CreateClueObj()
	for i=1,#self.m_ClueId do
		if self.m_ClueIdDic[self.m_ClueId[i]] then
			self.m_CompleteClue[i] = false
			self.m_ClueId2Index[self.m_ClueId[i]] = i
			local ClueInfo = self.m_ClueIdDic[self.m_ClueId[i]]
			local ClueTempObj = CUICommonDef.AddChild(self.ClueTable.gameObject,self.ClueTemplateLabel.gameObject)
			ClueTempObj:GetComponent(typeof(UILabel)).text = "   "..ClueInfo.title.."   "
			local ClueCollider = ClueTempObj.transform:Find("BG").gameObject
			table.insert(self.m_ClueObj,ClueTempObj)
			ClueTempObj.gameObject:SetActive(true)
			UIEventListener.Get(ClueCollider.gameObject).onDragStart = LuaUtils.VoidDelegate(function(go)
				if self.m_IsInTypeWriterEffect then return end	-- 在打字机效果显示过程中，不允许玩家继续拖拽线索框
				self:OnDragStart(i)
			end)
		end
	end
	self.Clue.gameObject:SetActive(true)
end
-- 添加一个ClueId
function LuaYeShengTuiLiWnd:AddClueId(ClueId)
	for i=1,#self.m_ClueId do
		if self.m_ClueId[i] == ClueId then return end
	end
	table.insert(self.m_ClueId,ClueId)
	local clueInfo = ZhuJueJuQing_YeShengThread.GetData(ClueId)
	self.m_ClueIdDic[ClueId] = {title = clueInfo.Description, Story = clueInfo.ThreadText}
end
-- 开始拖拽线索时，隐藏原线索框，赋值处理
function LuaYeShengTuiLiWnd:OnDragStart(i)
	local clueObj = self.m_ClueObj[i]
	self.m_CurrClueDrag = self.m_ClueId[i]
	self.m_OnDragClueObjIndex = i
	-- cloneObj赋值
	self.m_CloneClueObj:GetComponent(typeof(UILabel)).text = clueObj:GetComponent(typeof(UILabel)).text
	local Bg = clueObj.transform:Find("BG"):GetComponent(typeof(UISprite))
	local CloneBg = self.m_CloneClueObj.transform:Find("BG"):GetComponent(typeof(UISprite))
	
	CloneBg.width = Bg.width
	CloneBg.height = Bg.height
	clueObj:SetActive(false)
	self.m_LastDragPos = Input.mousePosition

	if CommonDefs.IsInMobileDevice() then
		self.m_FingerIndex = Input.GetTouch(0).fingerId
		local pos = Input.GetTouch(0).position
		self.m_LastDragPos = Vector3(pos.x,pos.y,0)
	end
	self.m_CloneClueObj.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_LastDragPos)
	self.m_CloneClueObj:SetActive(true)
end
-- 拖拽以及松开按键时的情况
function LuaYeShengTuiLiWnd:Update()
	if self.m_CurrClueDrag == -1 then return end
	if not self.m_CloneClueObj then return end
	if not self.m_LastDragPos then self.m_LastDragPos = Vector3.zero end
	local isMoving = false
	-- 玩家拖拽时的情况，移动端和pc端
	if CommonDefs.IsInMobileDevice() then
		for i = 0, Input.touchCount-1 do
			local touch = Input.GetTouch(i)
			if touch.fingerId == self.m_FingerIndex then
				if touch.phase ~= TouchPhase.Ended and touch.phase ~= TouchPhase.Canceled then
					if touch.phase ~= TouchPhase.Stationary then
						local pos = touch.position
                        self.m_LastDragPos = Vector3(pos.x, pos.y, 0)
						self.m_CloneClueObj.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_LastDragPos)
					end
					isMoving = true
				end
			else
				break
			end
		end
	else
		if Input.GetMouseButton(0) then
			self.m_CloneClueObj.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
            self.m_LastDragPos = Input.mousePosition
            if not Input.GetMouseButtonUp(0) then
                isMoving = true
            end
		end
	end
	-- 玩家鼠标抬起时或玩家手指从屏幕上移开
	local hoveredObject = CUICommonDef.SelectedUIWithRacast
	if hoveredObject and self.m_CurrClueDrag ~= -1 then
		local url = nil
		if string.find(hoveredObject.name,"ParagraphTemplate") then
			local lb = hoveredObject.gameObject:GetComponent(typeof(UILabel))
			local index = lb:GetCharacterIndexAtPosition(UICamera.lastWorldPosition,false)
			url = lb:GetUrlAtCharacterIndex(index)
		end
		self:OnDragOverUnderLine(url,isMoving)
		if not isMoving then self:CheckClueFillInResult(url) end
	end
	if isMoving then return end
	self.m_CurrClueDrag = -1
	self.m_FingerIndex = -1
	self.m_OnDragClueObjIndex = -1
end
-- 拖拽至下划线处下划线变色
function LuaYeShengTuiLiWnd:OnDragOverUnderLine(Url,isOver)
	if System.String.IsNullOrEmpty(Url) or not isOver then
		if not self.m_LastClueInfo then return end
		-- self.m_LastClueInfo 处下划线变色还原
		self:ChangeFillInColor(false,self.m_LastClueInfo)
		self.m_LastClueInfo = nil
		return 
	end
	if self.m_LastClueInfo == Url then return end	-- 状态没有发生切换
	-- 状态发生切换，下划线变色
	self:ChangeFillInColor(true,Url)
	if self.m_LastClueInfo then
		self:ChangeFillInColor(false,self.m_LastClueInfo)
	end
	self.m_LastClueInfo = Url
end
-- 切换下划线处颜色，isChange为true变色，为false还原
function LuaYeShengTuiLiWnd:ChangeFillInColor(isChange,UrlInfo)
	local urlInfo = g_LuaUtil:StrSplit(UrlInfo,",")
	local ClueId = tonumber(urlInfo[1])
	local ParagraphId = tonumber(urlInfo[2])
	local Paragraph = self.m_ParagraphObj[ParagraphId]
	
	if not Paragraph then return end
	if self.m_CompleteClue[self.m_ClueId2Index[ClueId]] then return end	-- 已经完成还原
	local ParagraphLabel = Paragraph.gameObject:GetComponent(typeof(UILabel))

	if isChange then
		ParagraphLabel.text = string.gsub(ParagraphLabel.text,"%[url="..UrlInfo.."%]%[b%]([-]+)%[/b%]%[/url%]","%[url="..UrlInfo.."%]%[fffc00%]%[b%]%1%[/b%]%[%-%]%[/url%]")
	else
		ParagraphLabel.text = string.gsub(ParagraphLabel.text,"%[url="..UrlInfo.."%]%[fffc00%]%[b%]([-]+)%[/b%]%[%-%]%[/url%]" ,"%[url="..UrlInfo.."%]%[b%]%1%[/b%]%[/url%]")
	end
end
-- 检查填空结果是否正确
function LuaYeShengTuiLiWnd:CheckClueFillInResult(Url)
	if System.String.IsNullOrEmpty(Url) then 
		self.m_CloneClueObj:SetActive(false)
		self.m_ClueObj[self.m_OnDragClueObjIndex].gameObject:SetActive(true)
		return
	end
	local urlInfo = g_LuaUtil:StrSplit(Url,",")
	local clueId = tonumber(urlInfo[1])
	local ParagraphId = tonumber(urlInfo[2])

	local iscomplete = self.m_CompleteClue[self.m_ClueId2Index[clueId]]
	if clueId ~= self.m_CurrClueDrag or iscomplete then
		self.m_CloneClueObj:SetActive(false)
		self.m_ClueObj[self.m_OnDragClueObjIndex].gameObject:SetActive(true)
		if not iscomplete then g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("好像有哪里不太对。")) end
	elseif clueId == self.m_CurrClueDrag then
		self.m_CloneClueObj:SetActive(false)
		self.m_CompleteClue[self.m_OnDragClueObjIndex] = true
		self:FillInLabelComplete(Url,clueId,ParagraphId)
	end
end
-- 填空结果正确，开始填空
function LuaYeShengTuiLiWnd:FillInLabelComplete(Url,clueId,ParagraphId)
	local Paragraph = self.m_ParagraphObj[ParagraphId]
	if not Paragraph then return end
	local ParagraphLabel = Paragraph.gameObject:GetComponent(typeof(UILabel))
	local ClueInfo = nil
	local ClueText = nil
	local ClueLen = 0
	if self.m_ClueIdDic[clueId] then
		ClueInfo = self.m_ClueIdDic[clueId]
		ClueText = ClueInfo.Story
		ClueLen = CommonDefs.IS_VN_CLIENT and CUICommonDef.GetStrByteLength(ClueInfo.Story) or CUICommonDef.GetStrChsLength(ClueInfo.Story) 
	else return 
	end
	local ShowTypeWriterEffect = function(nbegin,nend)
		self.m_IsInTypeWriterEffect = true
		if self.m_Tick then
			UnRegisterTick(self.m_Tick)
		end
		local Interval = 40
		local TotalTime = (ClueLen + 1) * Interval
		local beforeText = string.rep("-",ClueLen)
		local tempNum = 0
		local beginText = string.sub(ParagraphLabel.text,1,nbegin-1)
		local nendText = string.sub(ParagraphLabel.text,nend+1,string.len(ParagraphLabel.text))
		self.m_Tick = RegisterTickWithDuration(function()
			if tempNum >= ClueLen  then
				self:CompleteClueAndCheckAll(clueId)
				UnRegisterTick(self.m_Tick)
				self.m_Tick = nil
				self.m_IsInTypeWriterEffect = false
				return
			end
			tempNum = tempNum + 1
			local res = LuaPersonalSpaceMgrReal.SubStringUTF8(ClueText,1,tempNum).."[b]"..LuaPersonalSpaceMgrReal.SubStringUTF8(beforeText,tempNum+1,ClueLen).."[/b]"
			ParagraphLabel.text = beginText.."[FFFE91]"..res.."[-]"..nendText
		end,Interval, TotalTime)
	end
	local PattenBegin = 0
	local PattenEnd = 0
	PattenBegin,PattenEnd = string.find(ParagraphLabel.text,"%[url="..Url.."%]%[b%]([-]+)%[/b%]%[/url%]")
	ShowTypeWriterEffect(PattenBegin,PattenEnd)
end
-- 记录线索还原情况，检查是否所有线索完成还原
function LuaYeShengTuiLiWnd:CompleteClueAndCheckAll(clueId)
	local hasFalse = false
	if not self.m_ClueId then return end
	for i=1,#self.m_ClueId do
		if self.m_ClueId[i] == clueId then self.m_CompleteClue[i] = true end
		if not self.m_CompleteClue[i] then hasFalse = true end
	end
	if hasFalse then return end
	self.Clue.gameObject:SetActive(false)
	self.CompleteButton.gameObject:SetActive(true)
end
--@region UIEvent
-- 完成还原
function LuaYeShengTuiLiWnd:OnCompleteButtonClick()
	if not self.m_ChooseStory or self.m_ChooseStory == EnumYeShengChooseRole.eNone then return end
	Gac2Gas.YeSheng_FindThreads(self.m_ChooseStory)
	CUIManager.CloseUI(CLuaUIResources.YeShengTuiLiWnd)
end

function LuaYeShengTuiLiWnd:OnDestroy()
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
	end
end
--@endregion UIEvent

