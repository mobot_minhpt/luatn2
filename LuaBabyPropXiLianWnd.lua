require("common/common_include")

local UITabBar = import "L10.UI.UITabBar"
local UIGrid = import "UIGrid"
local CButton = import "L10.UI.CButton"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local Baby_ShaoNian = import "L10.Game.Baby_ShaoNian"
local CUITexture = import "L10.UI.CUITexture"
local Baby_Setting = import "L10.Game.Baby_Setting"
local Item_Item = import "L10.Game.Item_Item"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemAccessTipMgr = import "L10.UI.CItemAccessTipMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local PlayerSettings = import "L10.Game.PlayerSettings"
local Extensions = import "Extensions"
local Mall_LingYuMall = import "L10.Game.Mall_LingYuMall"
local BabyMgr = import "L10.Game.BabyMgr"

LuaBabyPropXiLianWnd = class()

RegistChildComponent(LuaBabyPropXiLianWnd, "TabBar", UITabBar)
RegistChildComponent(LuaBabyPropXiLianWnd, "HintLabel", UILabel)
RegistChildComponent(LuaBabyPropXiLianWnd, "PropGrid", UIGrid)
RegistChildComponent(LuaBabyPropXiLianWnd, "PropTemplate", GameObject)

RegistChildComponent(LuaBabyPropXiLianWnd, "ApplyBtn", CButton)
RegistChildComponent(LuaBabyPropXiLianWnd, "MoreInfo", GameObject)
RegistChildComponent(LuaBabyPropXiLianWnd, "LingYuCost", CCurentMoneyCtrl)
RegistChildComponent(LuaBabyPropXiLianWnd, "LingYuCheckbox", QnCheckBox)
RegistChildComponent(LuaBabyPropXiLianWnd, "ItemNameLabel", UILabel)
RegistChildComponent(LuaBabyPropXiLianWnd, "Need", GameObject)
RegistChildComponent(LuaBabyPropXiLianWnd, "ItemRight", GameObject)
RegistChildComponent(LuaBabyPropXiLianWnd, "Mask", GameObject)

RegistClassMember(LuaBabyPropXiLianWnd, "SelectedBaby")
RegistClassMember(LuaBabyPropXiLianWnd, "SelectedPropId")
RegistClassMember(LuaBabyPropXiLianWnd, "PropItems")

RegistClassMember(LuaBabyPropXiLianWnd, "ItemType")	-- 0-规仪，1-规诫
RegistClassMember(LuaBabyPropXiLianWnd, "NeedItemTId") -- 所需物品ID
RegistClassMember(LuaBabyPropXiLianWnd, "NeedCount") -- 所需物品数量
RegistClassMember(LuaBabyPropXiLianWnd, "TickIdx") -- 展示结果的Idx
RegistClassMember(LuaBabyPropXiLianWnd, "ResultTick") -- 展示结果的tick
RegistClassMember(LuaBabyPropXiLianWnd, "InitTick") --初始化Tick
RegistClassMember(LuaBabyPropXiLianWnd, "ResultCounter")
RegistClassMember(LuaBabyPropXiLianWnd, "IsShowingResult") --是否在展示信息中

--2023
RegistClassMember(LuaBabyPropXiLianWnd, "SkipBtn")
RegistClassMember(LuaBabyPropXiLianWnd, "isSkip")


function LuaBabyPropXiLianWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaBabyPropXiLianWnd:InitClassMembers()
	if not LuaBabyMgr.m_ChosenBaby or  LuaBabyMgr.m_ChosenBaby.Status <= EnumBabyStatus.eChild then
		CUIManager.CloseUI(CLuaUIResources.BabyPropXiLianWnd)
	end
	self.SelectedBaby = LuaBabyMgr.m_ChosenBaby
	self.NeedCount = 1
	self.PropTemplate:SetActive(false)
	self.PropItems = {}
	self.SelectedPropId = 0
	
	self.SkipBtn = self.transform:Find("Anchor/Bottom/SkipBtn")
	self.isSkip = false

	self:UpdateShowResultStatus(true)
	
end

function LuaBabyPropXiLianWnd:InitValues()
	self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabChange(go, index)
	end)

	local onMoreInfoClicked = function (go)
		self:OnMoreInfoClicked(go)
	end
	CommonDefs.AddOnClickListener(self.MoreInfo, DelegateFactory.Action_GameObject(onMoreInfoClicked), false)

	local onApplyBtnClicked = function (go)
		self:OnApplyBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.ApplyBtn.gameObject, DelegateFactory.Action_GameObject(onApplyBtnClicked), false)

	local onLingYuCheckboxClicked = function (value)
		self:OnLingYuCheckboxClicked(value)
	end
	self.LingYuCheckbox.OnValueChanged = DelegateFactory.Action_bool(onLingYuCheckboxClicked)

	if self.SkipBtn then
		local onSkipBtnClicked = function (go)
			self:OnSkipBtnClicked(go)
		end
		CommonDefs.AddOnClickListener(self.SkipBtn.gameObject, DelegateFactory.Action_GameObject(onSkipBtnClicked), false)
	end

	if LuaBabyMgr.m_BabyXiLianType and LuaBabyMgr.m_BabyXiLianType >= 0 and LuaBabyMgr.m_BabyXiLianType < 2 then
		self.ItemType = LuaBabyMgr.m_BabyXiLianType
		self.TabBar:ChangeTab(LuaBabyMgr.m_BabyXiLianType)
	else
		self.ItemType = 0
		self.TabBar:ChangeTab(0)
	end
	
end

function LuaBabyPropXiLianWnd:OnTabChange(go, index)

	-- Step 1 修改hintLabel
	self:DestroyResultTick()
	self:DestroyInitTick()
	self:UpdateShowResultStatus(false)
	self.ItemType = index

	-- Step 2 刷新界面
	self:UpdateProps()

	-- Step 3 切换所需物品
	self.LingYuCheckbox:SetSelected(PlayerSettings.AutoUseLingYuWhenBabyXiLian, false)

end

function LuaBabyPropXiLianWnd:UpdateProps()
	self.SelectedPropId = 0
	CUICommonDef.ClearTransform(self.PropGrid.transform)
	self.PropItems = {}
	Baby_ShaoNian.ForeachKey(DelegateFactory.Action_object(function (key)
		local data = Baby_ShaoNian.GetData(key)
		local go = NGUITools.AddChild(self.PropGrid.gameObject, self.PropTemplate)
		self:InitBabyPropItem(go, data)
		go:SetActive(true)
		table.insert(self.PropItems, go)
	end))
	self.PropGrid:Reposition()
end

function LuaBabyPropXiLianWnd:OnLingYuCheckboxClicked(value)
	PlayerSettings.AutoUseLingYuWhenBabyXiLian = value
	self:UpdateNeedItem()
end

function LuaBabyPropXiLianWnd:InitBabyPropItem(go, data)
	local Logo = go.transform:Find("Logo"):GetComponent(typeof(CUITexture))
	local PropertyLabel = go.transform:Find("PropertyLabel"):GetComponent(typeof(UILabel))
	local ValueLabel = go.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
	local Selected = go.transform:Find("Selected").gameObject
	local RandomSelected = go.transform:Find("RandomSelected").gameObject
	-- 2023
	local BelowStandardTag = go.transform:Find("BelowStandardTag")

	if BelowStandardTag then--是否是迭代后界面
		if LuaBabyMgr.mTargetQiChangId == 0 or LuaBabyMgr.mTargetQiChangId == nil then
			BelowStandardTag.gameObject:SetActive(false)
		else
			local request = LuaBabyMgr.GetQiChangRequestPropByIndex(data.ID, Baby_QiChang.GetData(LuaBabyMgr.mTargetQiChangId))
			local own = self.SelectedBaby.Props.YoungProp[data.ID]
			BelowStandardTag.gameObject:SetActive(own < request)
		end
	end

	Selected:SetActive(false)
	RandomSelected:SetActive(false)
	PropertyLabel.text = data.Name
	ValueLabel.text = self.SelectedBaby.Props.YoungProp[data.ID]
	Logo:LoadMaterial(LuaBabyMgr.GetPropBG(data.YouErName))

	local onPropClicked = function (gameObject)
		self:OnPropClicked(gameObject, data.ID)
	end
	CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(onPropClicked), false)
end

function LuaBabyPropXiLianWnd:SetPropSelected(go, isSelected, index)
	local Selected = go.transform:Find("Selected").gameObject
	local RandomSelected = go.transform:Find("RandomSelected").gameObject
	local ValueLabel = go.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
	if self.ItemType == 0 then
		Selected:SetActive(isSelected)
		RandomSelected:SetActive(false)
	else
		Selected:SetActive(false)
		RandomSelected:SetActive(isSelected)
	end
	if isSelected then
		ValueLabel.text = SafeStringFormat3("%s%s", tostring(self.SelectedBaby.Props.YoungProp[index]), tostring(self:GetDefaultValue())) 
	else
		ValueLabel.text = self.SelectedBaby.Props.YoungProp[index]
	end
end

function LuaBabyPropXiLianWnd:SetPropRandomSelected(go, isSelected)
	local Selected = go.transform:Find("Selected").gameObject
	local RandomSelected = go.transform:Find("RandomSelected").gameObject

	if self.ItemType == 0 then
		Selected:SetActive(false)
		RandomSelected:SetActive(isSelected)
	else
		Selected:SetActive(isSelected)
		RandomSelected:SetActive(false)
	end
end

function LuaBabyPropXiLianWnd:OnPropClicked(go, index)
	self.SelectedPropId = index

	for i = 1, #self.PropItems do
		self:SetPropSelected(self.PropItems[i], self.PropItems[i] == go, i)
	end
end

function LuaBabyPropXiLianWnd:UpdateNeedItem()
	local setting = Baby_Setting.GetData()
	self.NeedItemTId = setting.GuiYiItemId
	self.HintLabel.text = g_MessageMgr:FormatMessage("BABY_XILIAN_GUIYI_TIP")
	if self.ItemType == 1 then
		self.NeedItemTId = setting.GuiJieItemId
		self.HintLabel.text = g_MessageMgr:FormatMessage("BABY_XILIAN_GUIJIE_TIP")
	end

	local item = Item_Item.GetData(self.NeedItemTId)
	if not item then return end
	local lingyuMall = Mall_LingYuMall.GetData(self.NeedItemTId)
	if not lingyuMall then return end

	self.ItemNameLabel.text = item.Name
	local xilianLeft = setting.BabyXiLianCount - self:GetYiXiLianCount()
	self.ApplyBtn.Text = SafeStringFormat3(LocalString.GetString("进行%s(%d/%d)"), item.Name, xilianLeft, setting.BabyXiLianCount)
	self.ApplyBtn.Enabled = not self.IsShowingResult and xilianLeft > 0

	local IconTexture = self.Need.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local DisableSprite = self.Need.transform:Find("DisableSprite").gameObject
	local GetHint = self.Need.transform:Find("GetHint").gameObject
	local CountLabel = self.Need.transform:Find("CountLabel"):GetComponent(typeof(UILabel))

	IconTexture:LoadMaterial(item.Icon)
	local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(self.NeedItemTId)
	local totalCount = bindItemCountInBag + notBindItemCountInBag
	if totalCount < self.NeedCount then
		DisableSprite:SetActive(true)
		GetHint:SetActive(true)
		CountLabel.text = SafeStringFormat3("[FF0000]%d[-]/%d", totalCount, self.NeedCount)

		local onNeedItemClicked = function (go)
			self:OnNeedItemClicked(go, self.NeedItemTId)
		end
		CommonDefs.AddOnClickListener(IconTexture.gameObject, DelegateFactory.Action_GameObject(onNeedItemClicked), false)
	else
		DisableSprite:SetActive(false)
		GetHint:SetActive(false)
		CountLabel.text = SafeStringFormat3("%d/%d", totalCount, self.NeedCount)
	end
	
	if totalCount < self.NeedCount then
		if PlayerSettings.AutoUseLingYuWhenBabyXiLian then
			self.LingYuCost.gameObject:SetActive(true)
			self.Need:SetActive(false)
			self.LingYuCost:SetType(13, 0, false)
			self.LingYuCost:SetCost(lingyuMall.Jade)
			Extensions.SetLocalPositionX(self.ItemRight.transform, -226)
		else
			self.Need:SetActive(true)
			self.LingYuCost.gameObject:SetActive(false)
			Extensions.SetLocalPositionX(self.ItemRight.transform, -485)
		end
	else
		self.Need:SetActive(true)
		self.LingYuCost.gameObject:SetActive(false)
		Extensions.SetLocalPositionX(self.ItemRight.transform, -485)
	end
end

function LuaBabyPropXiLianWnd:GetYiXiLianCount()
	-- 检查宝宝身上的数据是否过期
	if self.SelectedBaby.Props.ExtraData.WashExpireTime < CServerTimeMgr.Inst.timeStamp then
		return 0
	else
		if self.ItemType == 0 then
			return self.SelectedBaby.Props.ExtraData.EnhanceTimes
		else
			return self.SelectedBaby.Props.ExtraData.AdmonishTimes
		end
	end
	return 0
end

function LuaBabyPropXiLianWnd:OnNeedItemClicked(go, itemTId)
	CItemAccessTipMgr.Inst:ShowAccessTip(nil, itemTId, false, go.transform, CTooltipAlignType.Default)
end


function LuaBabyPropXiLianWnd:OnMoreInfoClicked(go)
	g_MessageMgr:ShowMessage("BABY_PROP_XILIAN_TIPS")
end

function LuaBabyPropXiLianWnd:OnApplyBtnClicked(go)
	if self.SelectedPropId == 0 then
		g_MessageMgr:ShowMessage("SELECT_XILIAN_PROP_FIRST")
		return
	end

	local setting = Baby_Setting.GetData()
	if self:GetYiXiLianCount() >= setting.BabyXiLianCount then
		g_MessageMgr:ShowMessage("YANGYU_ADMONISH_TIMES_LIMIT")
		return
	end

	--物品是否充足，或者不充足的情况下灵玉是否充足
	local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(self.NeedItemTId)
	local totalCount = bindItemCountInBag + notBindItemCountInBag

	if totalCount >= self.NeedCount or (totalCount < self.NeedCount and PlayerSettings.AutoUseLingYuWhenBabyXiLian and self.LingYuCost.moneyEnough) then
		LuaBabyMgr.SavePropForSelectedBaby(self.SelectedBaby)
		self:UpdateShowResultStatus(true)
		Gac2Gas.RequestAdmonishBaby(self.SelectedBaby.Id, self.SelectedPropId, self.ItemType == 0, PlayerSettings.AutoUseLingYuWhenBabyXiLian)
	else
		g_MessageMgr:ShowMessage("BABY_XILIAN_ITEM_NOT_ENOUGH")
	end
end

function LuaBabyPropXiLianWnd:OnSkipBtnClicked(go)
	local btnSprite = self.SkipBtn.gameObject.transform:GetComponent(typeof(UISprite))

	self.isSkip = not self.isSkip
	btnSprite.spriteName = self.isSkip and "common_thumb_open" or "common_thumb_close"
end

-- 更新是否在播放动作中
function LuaBabyPropXiLianWnd:UpdateShowResultStatus(status)
	self.IsShowingResult = status
	self.Mask:SetActive(status)
	self.ApplyBtn.Enabled = not status
end

function LuaBabyPropXiLianWnd:GetDefaultValue()
	if self.ItemType == 0 then
		return "[00FF00](+1)[-]";
	else
		return "[FF0000](-1)[-]";
	end
end

function LuaBabyPropXiLianWnd:GetRandomValue()
	if self.ItemType == 1 then
		return "[00FF00](+1)[-]";
	else
		return "[FF0000](-1)[-]";
	end
end

function LuaBabyPropXiLianWnd:ShowResult(babyId, isAdd, chosenPropId, newChosenProp, targetPropId, newTargetProp)
	if self.SelectedBaby.Id ~= babyId then
		return
	end

	self:DestroyResultTick()

	if self.isSkip then
		self:ShowStaticResult(chosenPropId, newChosenProp, targetPropId, newTargetProp)
		
	else
		self.ResultTick = RegisterTick(function ()
			if self.TickIdx == self.SelectedPropId then
				self.TickIdx = self.TickIdx + 1
			end

			if self.TickIdx > #self.PropItems then
				self.TickIdx = 1
			end

			if self.TickIdx == self.SelectedPropId then
				self.TickIdx = self.TickIdx + 1
			end

			for i = 1, #self.PropItems do
				if i ~= self.SelectedPropId then
					self:SetPropRandomSelected(self.PropItems[i], i == self.TickIdx)
				end
			end

			if self.TickIdx == targetPropId and self.ResultCounter > 5 then
				self:DestroyResultTick()
				self:ShowStaticResult(chosenPropId, newChosenProp, targetPropId, newTargetProp)

			else
				self.ResultCounter = self.ResultCounter + 1
				self.TickIdx = self.TickIdx + 1
			end

		end, 300)
	end
end

-- 显示静态结果
function LuaBabyPropXiLianWnd:ShowStaticResult(chosenPropId, newChosenProp, targetPropId, newTargetProp)

	self:SetPropRandomSelected(self.PropItems[targetPropId], true)
	local ValueLabel = self.PropItems[targetPropId].transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
	ValueLabel.text = SafeStringFormat3("%s%s", tostring(self.SelectedBaby.Props.YoungProp[targetPropId]), tostring(self:GetRandomValue()))
	LuaBabyMgr.ShowBabyProDifForSelectedBaby(self.SelectedBaby)
	-- 定时显示下一段
	self:DestroyInitTick()
	self.InitTick = RegisterTickOnce(function ()
		self:UpdateShowResultStatus(false)
		self:UpdateProps()
		self:UpdateNeedItem()
	end, 3500)
end

function LuaBabyPropXiLianWnd:DestroyResultTick()
	self.TickIdx = 1
	self.ResultCounter = 0
	if self.ResultTick ~= nil then
		UnRegisterTick(self.ResultTick)
      	self.ResultTick = nil
	end
end

function LuaBabyPropXiLianWnd:DestroyInitTick()
	if self.InitTick ~= nil then
		UnRegisterTick(self.InitTick)
		self.InitTick = nil
	end
end

function LuaBabyPropXiLianWnd:UpdateAdmonishTimes()
	local setting = Baby_Setting.GetData()
	local item = Item_Item.GetData(self.NeedItemTId)
	if not item then return end

	self.ItemNameLabel.text = item.Name
	local xilianLeft = setting.BabyXiLianCount - self:GetYiXiLianCount()
	self.ApplyBtn.Text = SafeStringFormat3(LocalString.GetString("进行%s(%d/%d)"), item.Name, xilianLeft, setting.BabyXiLianCount)
end

function LuaBabyPropXiLianWnd:OnAdmonishBabyFail()
	self:UpdateShowResultStatus(false)
end

function LuaBabyPropXiLianWnd:OnUpdateBabyData()
	local babyId = self.SelectedBaby.Id
	local dict = BabyMgr.Inst.BabyDictionary
	local baby = dict[babyId]
	self.SelectedBaby=baby
end

function LuaBabyPropXiLianWnd:OnEnable()
	g_ScriptEvent:AddListener("BabySyncAdmonishTimes", self, "UpdateAdmonishTimes")
	g_ScriptEvent:AddListener("SyncAdmonishRes", self, "ShowResult")
	g_ScriptEvent:AddListener("SyncAdmonishBabyFail", self, "OnAdmonishBabyFail")
	g_ScriptEvent:AddListener("SendItem", self, "UpdateNeedItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "UpdateNeedItem")
	g_ScriptEvent:AddListener("UpdateBabyData", self, "OnUpdateBabyData")
	
end

function LuaBabyPropXiLianWnd:OnDisable()
	
	g_ScriptEvent:RemoveListener("SyncAdmonishRes", self, "ShowResult")
	g_ScriptEvent:RemoveListener("SyncAdmonishBabyFail", self, "OnAdmonishBabyFail")
	g_ScriptEvent:RemoveListener("SendItem", self, "UpdateNeedItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "UpdateNeedItem")
	g_ScriptEvent:RemoveListener("BabySyncAdmonishTimes", self, "UpdateAdmonishTimes")
	g_ScriptEvent:RemoveListener("UpdateBabyData", self, "OnUpdateBabyData")
	self:DestroyResultTick()
	self:DestroyInitTick()
end

return LuaBabyPropXiLianWnd
