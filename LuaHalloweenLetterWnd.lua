--import
--local GameObject			= import "UnityEngine.GameObject"

local UIVerticalLabel       = import "UIVerticalLabel"
local LocalString           = import "LocalString"

local CUIManager            = import "L10.UI.CUIManager"

--define 万圣节拯救开发组信内容界面
CLuaHalloweenLetterWnd = class()

--RegistChildComponent
RegistChildComponent(CLuaHalloweenLetterWnd, "Des",		UIVerticalLabel)
RegistChildComponent(CLuaHalloweenLetterWnd, "Des2",	UIVerticalLabel)

--RegistClassMember
--RegistClassMember(CLuaHalloweenLetterWnd, "Name")

CLuaHalloweenLetterWnd.Data=nil

function CLuaHalloweenLetterWnd.Show(content,group)
    CLuaHalloweenLetterWnd.Data={Content = content,Group = group}
    CUIManager.ShowUI("HalloweenLetterWnd")
end

--@region flow function

function CLuaHalloweenLetterWnd:Init()
    local key = CLuaHalloweenLetterWnd.Data.Content
    local msg = g_MessageMgr:FormatMessage(key)
    self.Des.text = msg
    local group = CLuaHalloweenLetterWnd.Data.Group
    if group == 1 then
        self.Des2.text = LocalString.GetString("倩女幽魂手游策划组")
    elseif group == 2 then 
        self.Des2.text = LocalString.GetString("倩女幽魂手游程序组")
    elseif group == 3 then 
        self.Des2.text = LocalString.GetString("倩女幽魂手游美术组")
    elseif group == 4 then 
        self.Des2.text = LocalString.GetString("倩女幽魂手游测试组")
    end
end

function CLuaHalloweenLetterWnd:OnEnable()
end

function CLuaHalloweenLetterWnd:OnDisable()
end

--@endregion