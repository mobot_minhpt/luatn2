local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaOffWorldPlay1MiddleResultWnd = class()

RegistChildComponent(LuaOffWorldPlay1MiddleResultWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaOffWorldPlay1MiddleResultWnd, "title", "title", UILabel)
RegistChildComponent(LuaOffWorldPlay1MiddleResultWnd, "ItemCell", "ItemCell", GameObject)
RegistChildComponent(LuaOffWorldPlay1MiddleResultWnd, "desc", "desc", UILabel)
RegistChildComponent(LuaOffWorldPlay1MiddleResultWnd, "rewardCount", "rewardCount", UILabel)
RegistChildComponent(LuaOffWorldPlay1MiddleResultWnd, "stoneCount", "stoneCount", UILabel)
RegistChildComponent(LuaOffWorldPlay1MiddleResultWnd, "pinfenSp", "pinfenSp", UISprite)
RegistChildComponent(LuaOffWorldPlay1MiddleResultWnd, "pinfenLabel", "pinfenLabel", UILabel)


function LuaOffWorldPlay1MiddleResultWnd:Init()
  local onCloseClick = function(go)
    CUIManager.CloseUI(CLuaUIResources.OffWorldPlay1MiddleResultWnd)
  end
  CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

  --LuaOffWorldPassMgr.MiddleStageData = {infoList[0],infoList[1],infoList[2]}
  local id = tonumber(LuaOffWorldPassMgr.MiddleStageData[1])
  self.title.text = OffWorldPass_JiaoShan.GetData(id).name
  local evaluate = {LocalString.GetString('普通'),LocalString.GetString('优秀'),LocalString.GetString('完美')}
  --
  self.desc.text = LuaOffWorldPassMgr.MiddleStageData[3]..'%'
  if LuaOffWorldPassMgr.m_CurrScore then
    self.stoneCount.text = "+"..tostring(LuaOffWorldPassMgr.m_CurrScore)
  else
    self.stoneCount.text = "+0"
  end

  local gradeColorTable = {'64A6D5','D55DE2','FFE300'}
  OffWorldPass_JiaoShanReward.Foreach(function(k,v)
    if v.stage == tonumber(LuaOffWorldPassMgr.MiddleStageData[1]) and LuaOffWorldPassMgr.MiddleStageData[3] <= v.hpMax and
      LuaOffWorldPassMgr.MiddleStageData[3] >= v.hpMin then
        self.rewardCount.text = LocalString.GetString("×").. tostring(v.dropTime)
        self.pinfenLabel.text = v.result
        self.pinfenSp.color = NGUIText.ParseColor(gradeColorTable[v.dropTime], 0)
    end
  end)


end

--@region UIEvent

--@endregion
