local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"

LuaChildrenDayPlayChooseBall2020Wnd = class()
RegistChildComponent(LuaChildrenDayPlayChooseBall2020Wnd,"CloseButton", GameObject)
RegistChildComponent(LuaChildrenDayPlayChooseBall2020Wnd,"node1", GameObject)
RegistChildComponent(LuaChildrenDayPlayChooseBall2020Wnd,"node2", GameObject)
RegistChildComponent(LuaChildrenDayPlayChooseBall2020Wnd,"node3", GameObject)
RegistChildComponent(LuaChildrenDayPlayChooseBall2020Wnd,"chooseBtn", GameObject)
RegistChildComponent(LuaChildrenDayPlayChooseBall2020Wnd,"chooseLabel", UILabel)

--RegistClassMember(LuaChildrenDayPlayChooseBall2020Wnd, "maxChooseNum")

function LuaChildrenDayPlayChooseBall2020Wnd:Close()
	CUIManager.CloseUI(CLuaUIResources.ChildrenDayPlayChooseBall2020Wnd)
end

function LuaChildrenDayPlayChooseBall2020Wnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateChildrenDayChooseBall", self, "InitTime")
end

function LuaChildrenDayPlayChooseBall2020Wnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateChildrenDayChooseBall", self, "InitTime")
end

function LuaChildrenDayPlayChooseBall2020Wnd:EndWnd()
	local nameTable = {
		LocalString.GetString('水滴子'),
		LocalString.GetString('老虎头'),
		LocalString.GetString('火流星'),
	}
	if not self.chooseIndex then
		self.chooseIndex = math.random(1,3)
		local name = nameTable[self.chooseIndex]
		--g_MessageMgr:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("系统随机为你选择了%s"),name))
	else
		local name = nameTable[self.chooseIndex]
		--g_MessageMgr:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("你选择了%s战斗"),name))
	end

	Gac2Gas.DanZhuSelectDanZhuTypeDone(self.chooseIndex)

	self:Close()
end

function LuaChildrenDayPlayChooseBall2020Wnd:CalTime()
	if self.countTime and self.countTime > 0 then
		self.chooseLabel.text = LocalString.GetString('确认选择') .. '(' .. self.countTime .. ')'
		self.countTime = self.countTime - 1
	else
		self:EndWnd()
	end
end

function LuaChildrenDayPlayChooseBall2020Wnd:InitTime()
	if LuaChildrenDay2020Mgr.gameSelectRemainTime and LuaChildrenDay2020Mgr.gameSelectRemainTime > 0 then
		self.countTime = LuaChildrenDay2020Mgr.gameSelectRemainTime
		self.m_Tick = RegisterTickWithDuration(function ()
			self:CalTime()
		end, 1000, 1000 * 90000)
	else
		self:EndWnd()
	end
end

function LuaChildrenDayPlayChooseBall2020Wnd:InitSingleNode(node,index)
	local btn = node.transform:Find('bg').gameObject
	local title = node.transform:Find('title'):GetComponent(typeof(UILabel))
	local lightBg = node.transform:Find('lightBg').gameObject
	local desc = node.transform:Find('desc'):GetComponent(typeof(UILabel))
	local qiu = node.transform:Find('qiu'):GetComponent(typeof(CUITexture))

	lightBg:SetActive(false)

	local onClick = function(go)
		if self.lightNode then
			self.lightNode:SetActive(false)
		end
		self.lightNode = lightBg
		lightBg:SetActive(true)
		self.chooseIndex = index
	end
	CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(onClick),false)
end

function LuaChildrenDayPlayChooseBall2020Wnd:InitNode()
	self:InitSingleNode(self.node1,1)
	self:InitSingleNode(self.node2,2)
	self:InitSingleNode(self.node3,3)
end

function LuaChildrenDayPlayChooseBall2020Wnd:Init()
	local onCloseClick = function(go)
		self:EndWnd()
	end
	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)

	local onSubmitClick = function(go)
		if not self.chooseIndex then
			g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请先选择一种弹珠"))
		else
			Gac2Gas.DanZhuSelectDanZhuTypeDone(self.chooseIndex)
			self:Close()
		end
	end
	CommonDefs.AddOnClickListener(self.chooseBtn,DelegateFactory.Action_GameObject(onSubmitClick),false)

	self:InitTime()
	self:InitNode()
end

function LuaChildrenDayPlayChooseBall2020Wnd:OnDestroy()
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
	end
end

return LuaChildrenDayPlayChooseBall2020Wnd
