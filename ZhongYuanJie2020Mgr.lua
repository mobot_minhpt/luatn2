local CTaskMgr = import "L10.Game.CTaskMgr"
local CRankData = import "L10.UI.CRankData"
local RankPlayerBaseInfo = import "L10.UI.RankPlayerBaseInfo"
local EnumClass = import "L10.Game.EnumClass"
local EnumRankSheet = import "L10.UI.EnumRankSheet"
local MsgPackImpl = import "MsgPackImpl"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

ZhongYuanJie2020Mgr = {}
ZhongYuanJie2020Mgr.m_HideTaskList = nil
ZhongYuanJie2020Mgr.m_IsFestivalOpen = true
ZhongYuanJie2020Mgr.m_TaskID = 22111031
ZhongYuanJie2020Mgr.m_MirrorNPCEngineID = 0
ZhongYuanJie2020Mgr.m_TipWnd_taskID = 0

function ZhongYuanJie2020Mgr:ShowTip(taskID)
    self.m_TipWnd_taskID = taskID
    CUIManager.ShowUI(CLuaUIResources.XYLPTipWnd)
end

function ZhongYuanJie2020Mgr:IsHideTaskListItem(taskid)
    if not self.m_IsFestivalOpen then return false end
    if not self.m_HideTaskList then
        self.m_HideTaskList = {}
        ZhongYuanJie_XYLPClues.ForeachKey(function (key)
            self.m_HideTaskList[key] = true
        end)
    end
    return self.m_HideTaskList[taskid]
end

function ZhongYuanJie2020Mgr:OpenXYLPWnd()
    if not CClientMainPlayer then return end
    local taskProp = CClientMainPlayer.Inst.TaskProp
    local s = ZhongYuanJie_2020Setting.GetData("XYLP_OpenWndUsedTaskID").Value
    local t = g_LuaUtil:StrSplit(s, ",")
    self.m_TaskID = tonumber(t[1])
    local mapId = tonumber(t[2])
    if taskProp:IsMainPlayerTaskFinished(self.m_TaskID) or taskProp.FinishedTaskIdSet:GetBit(mapId) then
        CUIManager.ShowUI(CLuaUIResources.XYLPWnd)
    elseif CommonDefs.DictContains(taskProp.CurrentTasks, typeof(UInt32), self.m_TaskID) then
        CTaskMgr.Inst:TrackToDoTask(self.m_TaskID)
    else
        Gac2Gas.RequestAutoAcceptTask(self.m_TaskID)
    end
end

function ZhongYuanJie2020Mgr:OpenDesireMirrorWnd()
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then return end
    if not mainPlayer.Target then
        --g_MessageMgr:ShowMessage("DesireMirror_ConfirmSelectTarget")
        return
    end
    self.m_MirrorNPCEngineID = mainPlayer.Target.EngineId
    CUIManager.ShowUI(CLuaUIResources.DesireMirrorWnd)
end

function ZhongYuanJie2020Mgr:XYLPTryRewardResult(groupId, bResult)
    if bResult then
        g_ScriptEvent:BroadcastInLua("XYLPWnd_PassCluesSuccess", groupId)
    end
end

function ZhongYuanJie2020Mgr:XYLPFindThread(threadId)
    if CUIManager.IsLoaded(CLuaUIResources.XYLPWnd) then
        Gac2Gas.XYLPQueryThread()
    end
end

function ZhongYuanJie2020Mgr:ProcessXYLPTask(task)
    local s = ZhongYuanJie_2020Setting.GetData("XYLP_ProcessTaskListItemTaskID").Value
    local t = g_LuaUtil:StrSplit(s, ",")
    local taskID = tonumber(t[1])
    if  taskID  == task.TemplateId then
        ZhongYuanJie2020Mgr:OpenXYLPWnd()
        return true
    end
    return false
end

function ZhongYuanJie2020Mgr:XYLPQueryThreadsResult(data_U)

    local datalist = {}
    --是否获得最终奖励
    local data = MsgPackImpl.unpack(data_U)

    --是否找到某条线索
    local ThreadsDict = data.Threads
    datalist.Threads = {}
    CommonDefs.DictIterate(ThreadsDict, DelegateFactory.Action_object_object(function (key, value)
        datalist.Threads[tonumber(key)] = value
    end))

    --是否完成某组线索
    local RewardsDict = data.Rewards
    datalist.RewardsDict = {}
    CommonDefs.DictIterate(RewardsDict, DelegateFactory.Action_object_object(function (key, value)
        datalist.RewardsDict[tonumber(key)] = value
    end))

    g_ScriptEvent:BroadcastInLua("XYLPWnd_CluesUpdate", datalist)
end

function ZhongYuanJie2020Mgr:YNZJUpdateMirror(monsterId)
    g_ScriptEvent:BroadcastInLua("DesireMirrorWnd_UpdateMirror", monsterId)
end

function ZhongYuanJie2020Mgr:YNZJQueryRankResult(rankInfo_U, selfInfo_U)
    local rankListData = MsgPackImpl.unpack(rankInfo_U)
    local myRankData = MsgPackImpl.unpack(selfInfo_U)
    local count = rankListData.Count
    if myRankData and not CommonDefs.DictContains(myRankData, typeof(String), 'IsEmpty') then
        CRankData.Inst.MainPlayerRankInfo = RankPlayerBaseInfo(0, 0, myRankData.Name, "", 0, 0, CommonDefs.ConvertIntToEnum(typeof(EnumClass), tonumber(myRankData.Class)), 0, tonumber(myRankData.Score), EnumRankSheet.eGuild, "", "", "", "", "", "", 0, 0, "", "", nil, nil, 0, "")
    elseif CClientMainPlayer.Inst then
        CRankData.Inst.MainPlayerRankInfo = RankPlayerBaseInfo(0, 0, CClientMainPlayer.Inst.Name, "", 0, 0, CClientMainPlayer.Inst.Class, 0, 0, EnumRankSheet.eGuild, "", "", "", "", "", "", 0, 0, "", "", nil, nil, 0, "")
    end
    CommonDefs.ListClear(CRankData.Inst.RankList)
    if CClientMainPlayer.Inst then
        CRankData.Inst.MainPlayerRankInfo.PlayerIndex = CClientMainPlayer.Inst.Id
    end
    for i=0,count-1 do
        local data = rankListData[i]
        local tt = {}
        tt.PlayerId = tonumber(data.PlayerId)
        tt.Name = data.Name
        tt.Class = tonumber(data.Class)
        tt.Score = tonumber(data.Score)
        tt.Rank = tonumber(data.Rank)
        if CClientMainPlayer and tt.PlayerId == CClientMainPlayer.Inst.Id then
            CRankData.Inst.MainPlayerRankInfo.Rank = tonumber(tt.Rank and tt.Rank or 0)
        end
        CommonDefs.ListAdd(CRankData.Inst.RankList, typeof(RankPlayerBaseInfo), RankPlayerBaseInfo(
                0, tt.PlayerId, tt.Name, "", 0, tt.Rank, CommonDefs.ConvertIntToEnum(typeof(EnumClass), tt.Class), 0, tt.Score, EnumRankSheet.eGuild, "", "", "", "", "", "", 0, 0, "", "", nil, nil, 0, ""))
    end
    local msg = g_MessageMgr:FormatMessage("DesireMirror_RankWndText")
    LuaCommonRankWndMgr.m_BottomText = CUICommonDef.TranslateToNGUIText(msg)
    LuaCommonRankWndMgr.m_TitleText = LocalString.GetString("欲念净化排行")
    LuaCommonRankWndMgr.m_ThirdHeadText = LocalString.GetString("当前净化积分")
    CUIManager.ShowUI(CLuaUIResources.CommonRankWnd)
end

function ZhongYuanJie2020Mgr:RJPMQueryRankResult(rankInfo_U, selfInfo_U)
    local rankListData = MsgPackImpl.unpack(rankInfo_U)
    local myRankData = MsgPackImpl.unpack(selfInfo_U)
    if myRankData and not CommonDefs.DictContains(myRankData, typeof(String), 'IsEmpty') then
        CRankData.Inst.MainPlayerRankInfo = RankPlayerBaseInfo(0, 0, myRankData.Name, "", 0, 0, CommonDefs.ConvertIntToEnum(typeof(EnumClass), tonumber(myRankData.Class)), 0, tonumber(myRankData.Damage), EnumRankSheet.eGuild, "", "", "", "", "", "", 0, 0, "", "", nil, nil, 0, "")
    elseif CClientMainPlayer.Inst then
        CRankData.Inst.MainPlayerRankInfo = RankPlayerBaseInfo(0, 0, CClientMainPlayer.Inst.Name, "", 0, 0, CClientMainPlayer.Inst.Class, 0, 0, EnumRankSheet.eGuild, "", "", "", "", "", "", 0, 0, "", "", nil, nil, 0, "")
    end
    local count = rankListData.Count
    CommonDefs.ListClear(CRankData.Inst.RankList)
    if CClientMainPlayer.Inst then
        CRankData.Inst.MainPlayerRankInfo.PlayerIndex = CClientMainPlayer.Inst.Id
    end
    for i=0,count-1 do
        local data = rankListData[i]
        local tt = {}
        tt.PlayerId = tonumber(data.PlayerId)
        tt.Name = data.Name
        tt.Class = tonumber(data.Class)
        tt.Score = tonumber(data.Damage)
        tt.Rank = tonumber(data.Rank)
        if CClientMainPlayer and tt.PlayerId == CClientMainPlayer.Inst.Id then
            CRankData.Inst.MainPlayerRankInfo.Rank = tonumber(tt.Rank and tt.Rank or 0)
        end
        CommonDefs.ListAdd(CRankData.Inst.RankList, typeof(RankPlayerBaseInfo), RankPlayerBaseInfo(
                0, tt.PlayerId, tt.Name, "", 0, tt.Rank, CommonDefs.ConvertIntToEnum(typeof(EnumClass), tt.Class), 0, tt.Score, EnumRankSheet.eGuild, "", "", "", "", "", "", 0, 0, "", "", nil, nil, 0, ""))
    end
    local msg = g_MessageMgr:FormatMessage("RJPM_RankWndText")
    LuaCommonRankWndMgr.m_BottomText = CUICommonDef.TranslateToNGUIText(msg)
    LuaCommonRankWndMgr.m_TitleText = LocalString.GetString("入镜破魔排行")
    LuaCommonRankWndMgr.m_ThirdHeadText = LocalString.GetString("队伍伤害平均值")
    LuaCommonRankWndMgr.m_OnBottomTextClick = function(label,index)
        --local s = label.text
        local s = ZhongYuanJie_2020Setting.GetData("RJPM_ZhongKuiItemId").Value
        local t = g_LuaUtil:StrSplit(s,",")
        if #t ~= 6 then return end
        for i = 1, 6, 3 do
            if index >= tonumber(t[i]) and index <= tonumber(t[i+1]) then
                CItemInfoMgr.ShowLinkItemTemplateInfo(tonumber(t[i+2]))
            end
        end
    end
    CUIManager.ShowUI(CLuaUIResources.CommonRankWnd)
end

function ZhongYuanJie2020Mgr:IsRJPMGamePlay(gamePlayId)
    return self.m_IsFestivalOpen and gamePlayId == tonumber(ZhongYuanJie_2020Setting.GetData("RJPM_GamePlayId").Value)
end

