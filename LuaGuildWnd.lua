-- local EventManager = import "EventManager"
-- local EnumEventType = import "EnumEventType"
-- local CLogMgr = import "L10.CLogMgr"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local UIAlertIcon=import "L10.UI.UIAlertIcon"

CLuaGuildWnd = class()
RegistClassMember(CLuaGuildWnd,"m_CloseButton")
RegistClassMember(CLuaGuildWnd,"mTitle")
RegistClassMember(CLuaGuildWnd,"tabBar")
RegistClassMember(CLuaGuildWnd,"m_AlertIcon")
RegistClassMember(CLuaGuildWnd,"joinGuildWnd")
RegistClassMember(CLuaGuildWnd,"joinGuildRegion")
RegistClassMember(CLuaGuildWnd,"guildListRegion")
RegistClassMember(CLuaGuildWnd,"requestCount")
RegistClassMember(CLuaGuildWnd,"triggered")
RegistClassMember(CLuaGuildWnd,"createTab")
RegistClassMember(CLuaGuildWnd,"createInfoRegion")
RegistClassMember(CLuaGuildWnd,"m_GuideTick")

CLuaGuildWnd.applied = false
CLuaGuildWnd.fromChangeGuild=false
function CLuaGuildWnd:Awake()
    self.m_CloseButton = self.transform:Find("Wnd_Bg_Primary_Tab/CloseButton").gameObject
    self.mTitle = self.transform:Find("Wnd_Bg_Primary_Tab/TitleLabel"):GetComponent(typeof(UILabel))
    self.tabBar = self.transform:Find("GuildTab"):GetComponent(typeof(QnTabView))
    self.m_AlertIcon = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs/Tab/Alert"):GetComponent(typeof(UIAlertIcon))

    self.joinGuildWnd = self.transform:Find("GuildTab/JoinGuildWnd")

    self.joinGuildRegion = self.transform:Find("GuildTab/JoinGuildWnd/Offset/JoinGuildRegion").gameObject
    self.guildListRegion = self.transform:Find("GuildTab/JoinGuildWnd/Offset/GuildListRegion").gameObject
    self.requestCount = 0
    self.triggered = false

    local tf =self.transform:Find("Wnd_Bg_Primary_Tab/Tabs")
    self.createTab = tf:GetChild(1).gameObject
    self.createInfoRegion = self.transform:Find("GuildTab/CreateGuildWnd/Offset/CreateInfoRegion").gameObject

end

function CLuaGuildWnd:SetTabViewFromChangeGuild( )
    local tab2 = self.tabBar:GetTabGameObject(1)
    local tab3 = self.tabBar:GetTabGameObject(2)
    tab2:SetActive(false)
    tab3:SetActive(false)
end
function CLuaGuildWnd:Init( )

    if CLuaGuildWnd.fromChangeGuild then
        self:SetTabViewFromChangeGuild()
        CLuaGuildWnd.fromChangeGuild = false
    end

    self.tabBar.OnSelect = DelegateFactory.Action_QnTabButton_int(function (button, index) 
        self.mTitle.text = string.gsub(button.Text, "\n", CommonDefs.IS_VN_CLIENT and " " or "")

        if index==2 then
            self.m_AlertIcon.Visible = false
        end
    end)
end
function CLuaGuildWnd:OnEnable( )
    self.m_AlertIcon.Visible = false
    --请求是否需要显示帮会响应的红点
    Gac2Gas.QueryAllGuildsCreationAlert()
    g_ScriptEvent:AddListener("RequestOperationInGuildSucceed", self, "OnRequestOperationInGuildSucceed")
    g_ScriptEvent:AddListener("SendAllGuildsCreationAlert", self, "OnSendAllGuildsCreationAlert")
    
end

function CLuaGuildWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RequestOperationInGuildSucceed", self, "OnRequestOperationInGuildSucceed")
    g_ScriptEvent:RemoveListener("SendAllGuildsCreationAlert", self, "OnSendAllGuildsCreationAlert")

    --不管有没有帮会，关闭的时候都会停止引导
    if CGuideMgr.Inst:IsInPhase(12) then
        CGuideMgr.Inst:EndCurrentPhase()
    end
end

function CLuaGuildWnd:OnSendAllGuildsCreationAlert(alertStatus)
    self.m_AlertIcon.Visible = alertStatus
end

function CLuaGuildWnd:OnRequestOperationInGuildSucceed( args ) 
    local requestType, paramStr = args[0], args[1]

    if (("AddMember") == requestType) then
        CUIManager.CloseUI(CUIResources.GuildWnd)
    end
end
function CLuaGuildWnd:GetGuildListRegion( )
    if not self.triggered then
        self.triggered = true
        CLuaGuildWnd.applied = false
        self.m_GuideTick = RegisterTickOnce(function()
            local tf = self.joinGuildWnd:Find("Offset/GuildList/GuildTable")
            if tf.childCount==0 then
                CGuideMgr.Inst:TriggerGuide(6)
            end
        end,2000)
    end
    return self.guildListRegion
end

function CLuaGuildWnd:OnDestroy()
    if self.m_GuideTick then
        UnRegisterTick(self.m_GuideTick)
        self.m_GuideTick = nil
    end
end

function CLuaGuildWnd:GetJoinGuildRegion( )
    if self.joinGuildRegion.activeInHierarchy then
        return self.joinGuildRegion
    else
        return nil
    end
end
function CLuaGuildWnd:GetGuideGo( methodName) 
    if methodName == "GetGuildListRegion" then
        return self:GetGuildListRegion()
    elseif methodName == "GetJoinGuildRegion" then
        return self:GetJoinGuildRegion()
    elseif methodName == "GetCreateTab" then
        return self.createTab
    elseif methodName == "GetCreateInfoRegion" then
        return self.createInfoRegion
    else
        return nil
    end
end

