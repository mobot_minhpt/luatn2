local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local QnTableView = import "L10.UI.QnTableView"
local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local AlignType2=import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CButton = import "L10.UI.CButton"

local UITexture = import "UITexture"
local LoadPicMgr = import "L10.Game.LoadPicMgr"
local Texture2D = import "UnityEngine.Texture2D"
local TextureFormat = import "UnityEngine.TextureFormat"
local Screen = import "UnityEngine.Screen"
local NativeHandle = import "NativeHandle"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local HTTPHelper = import "L10.Game.HTTPHelper"
local L10 = import "L10"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local VoiceManager = import "VoiceManager"
local CCoroutineMgr = import "L10.Engine.CCoroutineMgr"
local Main = import "L10.Engine.Main"

LuaCustomZswTemplateEditWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaCustomZswTemplateEditWnd, "Preview", "Preview", CUITexture)
RegistChildComponent(LuaCustomZswTemplateEditWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaCustomZswTemplateEditWnd, "TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaCustomZswTemplateEditWnd, "FastFillBtn", "FastFillBtn", GameObject)
RegistChildComponent(LuaCustomZswTemplateEditWnd, "SaveBtn", "SaveBtn", GameObject)
RegistChildComponent(LuaCustomZswTemplateEditWnd, "PlaceBtn", "PlaceBtn", CButton)
RegistChildComponent(LuaCustomZswTemplateEditWnd, "ZswCountLabel", "ZswCountLabel", UILabel)
RegistChildComponent(LuaCustomZswTemplateEditWnd, "RenameBtn", "RenameBtn", GameObject)
RegistChildComponent(LuaCustomZswTemplateEditWnd, "TemplateNameLabel", "TemplateNameLabel", UILabel)
RegistChildComponent(LuaCustomZswTemplateEditWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaCustomZswTemplateEditWnd, "FastClearBtn", "FastClearBtn", GameObject)
RegistChildComponent(LuaCustomZswTemplateEditWnd, "HintLabel", "HintLabel", UILabel)
RegistChildComponent(LuaCustomZswTemplateEditWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaCustomZswTemplateEditWnd, "ListBtn", "ListBtn", GameObject)
RegistChildComponent(LuaCustomZswTemplateEditWnd, "ShenHeLabel", "ShenHeLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaCustomZswTemplateEditWnd,"m_TemplateIndex")
RegistClassMember(LuaCustomZswTemplateEditWnd,"m_TemplateData")--模板数据
RegistClassMember(LuaCustomZswTemplateEditWnd,"m_FillData")--装填数据
RegistClassMember(LuaCustomZswTemplateEditWnd,"m_TemplateComponents")--模板组件
RegistClassMember(LuaCustomZswTemplateEditWnd,"m_HuaMuTbl")
RegistClassMember(LuaCustomZswTemplateEditWnd,"m_OtherTbl")
RegistClassMember(LuaCustomZswTemplateEditWnd,"m_SelectedTabIndex")
RegistClassMember(LuaCustomZswTemplateEditWnd,"m_IsInAudit")
function LuaCustomZswTemplateEditWnd:Awake()
	self.m_TemplateIndex = CLuaZswTemplateMgr.CurCustomTemplateIdx
	self.m_TemplateData = CLuaZswTemplateMgr.GetCustomTemplateByIndex(self.m_TemplateIndex)
	self.m_TemplateComponents = {}
	if self.m_TemplateData and self.m_TemplateData.components then
		self.m_TemplateComponents = self.m_TemplateData.components
	end
	self.m_HuaMuTbl = {}
	self.m_OtherTbl = {}
	for i,component in ipairs(self.m_TemplateComponents) do
		component.slotIdx = i
		local data = Zhuangshiwu_Zhuangshiwu.GetData(component.id)
		if data then
			if data.Type == EnumFurnitureType_lua.eHuamu then
				table.insert(self.m_HuaMuTbl,component)
			else
				table.insert(self.m_OtherTbl,component)
			end
		end
	end
	CLuaZswTemplateMgr.InitCustomFillingData(self.m_TemplateIndex)

	self.m_Native = CreateFromClass(NativeHandle)
	self.m_PicResolution = 512
	self.ShenHeLabel.gameObject:SetActive(false)
	self.m_IsInAudit = false
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.Preview.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPreviewClick()
	end)


	
	UIEventListener.Get(self.FastFillBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFastFillBtnClick()
	end)


	
	UIEventListener.Get(self.SaveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSaveBtnClick()
	end)


	
	UIEventListener.Get(self.PlaceBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPlaceBtnClick()
	end)


	
	UIEventListener.Get(self.RenameBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRenameBtnClick()
	end)


	
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)


	
	UIEventListener.Get(self.FastClearBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFastClearBtnClick()
	end)


	
	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)


	
	UIEventListener.Get(self.ListBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnListBtnClick()
	end)


    --@endregion EventBind end
	CLuaZswTemplateMgr.RefreshHomeDecorationPicList()
end

function LuaCustomZswTemplateEditWnd:OnEnable()
	g_ScriptEvent:AddListener("RefreshCustomFillingData", self, "OnRefreshCustomFillingData")
	g_ScriptEvent:AddListener("UpdateHomeDecorationPicList", self, "OnUpdateHomeDecorationPicList")
end

function LuaCustomZswTemplateEditWnd:OnDisable()
	g_ScriptEvent:RemoveListener("RefreshCustomFillingData", self, "OnRefreshCustomFillingData")
	g_ScriptEvent:RemoveListener("UpdateHomeDecorationPicList", self, "OnUpdateHomeDecorationPicList")
	VoiceManager.onPicMsg = nil
	CLuaZswTemplateMgr.SaveCustomTemplateToLocal()
end

function LuaCustomZswTemplateEditWnd:Init()
	--清一下上次的填充数据 避免数量冲突
    CLuaZswTemplateMgr.CustomFillingData = {}

	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
			if self.m_SelectedTabIndex == 0 then
				return #self.m_HuaMuTbl
			else
				return #self.m_OtherTbl
			end           
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
		self:OnSelectAtRow(row)
    end)

    self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnTabChange(index)
    end)

	self.m_SelectedTabIndex = 0
    self.TabBar:ChangeTab(0, false)
	
	self.TemplateNameLabel.text = self.m_TemplateData and self.m_TemplateData.name or SafeStringFormat3(LocalString.GetString("自定义装饰物模板%d"),self.m_TemplateIndex)
	self.ZswCountLabel.text = #self.m_TemplateComponents

	self:InitTemplateInfo()
end

function LuaCustomZswTemplateEditWnd:InitTemplateInfo()
	local templateData = CLuaZswTemplateMgr.GetCustomTemplateByIndex(self.m_TemplateIndex)
	self.TemplateNameLabel.text = templateData.name
	self.m_PicSaveName = templateData.name
	local iconUrl = templateData.icon
	if iconUrl then
		L10.Engine.Main.Inst:StartCoroutine(HTTPHelper.NOSDownloadFileData(iconUrl, DelegateFactory.Action_bool_byte_Array(function (sign, bytes) 
			if not sign then
				return
			end
			local _texture = CreateFromClass(Texture2D, 4, 4, TextureFormat.RGBA32, false)
			CommonDefs.LoadImage(_texture, bytes)
			self.Preview.mainTexture = _texture
		end)))
		local auditStatus = templateData.auditStatus
		local resulttxt = LocalString.GetString("[ffff00]审核中[-]")
		if auditStatus == -1 then
			resulttxt = LocalString.GetString("[ff0000]未过审[-]")
			self.ShenHeLabel.gameObject:SetActive(true)
			self.m_IsInAudit = false
		elseif auditStatus == 1 then
			resulttxt = LocalString.GetString("审核通过")
			self.ShenHeLabel.gameObject:SetActive(false)
			self.m_IsInAudit = false
		else
			self.ShenHeLabel.gameObject:SetActive(true)
			self.m_IsInAudit = true
		end
		self.ShenHeLabel.text = resulttxt
	else
		local level = 1
		if CClientHouseMgr.Inst and CClientHouseMgr.Inst.mCurFurnitureProp2 and CClientHouseMgr.Inst.mCurFurnitureProp2.CustomFurnitureTemplateInfo then
			local infoMap = CClientHouseMgr.Inst.mCurFurnitureProp2.CustomFurnitureTemplateInfo
			local playerId = CClientMainPlayer.Inst.Id
			if infoMap and CommonDefs.DictContains_LuaCall(infoMap, playerId) then
				local customInfo = CommonDefs.DictGetValue_LuaCall(infoMap, playerId)
				level = customInfo.LevelInfo[self.m_TemplateIndex]
			end
		end
		local path = Zhuangshiwu_CustomTemplate.GetData(level).DefaultIcon
		self.Preview:LoadMaterial(path)
	end

	self.PlaceBtn.Enabled = CLuaZswTemplateMgr.IsAnySlotFilled(self.m_TemplateIndex)
end

function LuaCustomZswTemplateEditWnd:InitItem(item,row)
	local component = nil
	if self.m_SelectedTabIndex == 0 then
		component = self.m_HuaMuTbl[row + 1]
	else
		component = self.m_OtherTbl[row + 1]
	end

	local Icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local id = component.id
	local data = Zhuangshiwu_Zhuangshiwu.GetData(id)
	if data then
		local itemId = data.ItemId
        local itemData = Item_Item.GetData(itemId)
        if itemData then
            local defaultIconName = itemData.Icon
            Icon:LoadMaterial(defaultIconName)
        end
	end

	local Mask = item.transform:Find("Mask").gameObject
	local slotIdx
	if self.m_SelectedTabIndex == 0 then
		slotIdx = self.m_HuaMuTbl[row+1].slotIdx
	else
		slotIdx = self.m_OtherTbl[row+1].slotIdx
	end
	local isFilled = CLuaZswTemplateMgr.IsSlotFilled(self.m_TemplateIndex,slotIdx)
	Mask:SetActive(not isFilled)
end

function LuaCustomZswTemplateEditWnd:OnSelectAtRow(row)
	local slotIdx
	local zswId = 0
	if self.m_SelectedTabIndex == 0 then
		slotIdx = self.m_HuaMuTbl[row+1].slotIdx
		zswId = self.m_HuaMuTbl[row+1].id
	else
		slotIdx = self.m_OtherTbl[row+1].slotIdx
		zswId = self.m_OtherTbl[row+1].id
	end
	if slotIdx then
		if CLuaZswTemplateMgr.IsSlotFilled(self.m_TemplateIndex,slotIdx) then
			--取出
			local function SelectAction(index)               
				if index==0 then
					CLuaZswTemplateMgr.RevertFillingZsw(self.m_TemplateIndex,zswId,slotIdx,true)
				end       
			end
		
			local selectShareItem=DelegateFactory.Action_int(SelectAction)
			local t={}
			local item=PopupMenuItemData(LocalString.GetString("取出"),selectShareItem,false,nil)
			table.insert(t, item)
		
			local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
			local goTrans = self.TableView:GetItemAtRow(row).transform
			CPopupMenuInfoMgr.ShowPopupMenu(array, goTrans, AlignType2.Top,1,nil,600,true,266)
		else
			--装填
			CLuaZswTemplateMgr.OpenCustomFillingWnd(self.m_TemplateIndex,slotIdx)
		end
	end
end

function LuaCustomZswTemplateEditWnd:OnTabChange(index)
	self.m_SelectedTabIndex = index
	self.TableView:ReloadData(true, false)
	local tbl = {}
	if self.m_SelectedTabIndex == 0 then
		tbl = self.m_HuaMuTbl
		self.HintLabel.text = LocalString.GetString("该装饰物模板没有使用到花木类装饰物")
	else
		tbl = self.m_OtherTbl
		self.HintLabel.text = LocalString.GetString("该装饰物模板没有使用到其他类装饰物")
	end
	self.HintLabel.gameObject:SetActive(#tbl == 0)
end

function LuaCustomZswTemplateEditWnd:OnRefreshCustomFillingData(tindex,zswId,slotIdx)
	if tindex ~= self.m_TemplateIndex then return end
	self.TableView:ReloadData(true, false)
	self.PlaceBtn.Enabled = CLuaZswTemplateMgr.IsAnySlotFilled(self.m_TemplateIndex)
end

function LuaCustomZswTemplateEditWnd:OnUpdateHomeDecorationPicList()
	self:Init()
end

function LuaCustomZswTemplateEditWnd:SettingPicAndGifFromMobile()
	local name = "home_decoration.png"
	VoiceManager.onPicMsg = DelegateFactory.Action_string(function (path)
		Main.Inst:StartCoroutine(LoadPicMgr.LoadMomentTexture(name, DelegateFactory.Action_bytes(function (bytes)
			self:AddPicBack(bytes)
		end)))
	end)

	self.m_Native:SettingAvaterFormMobile("Main/VoiceListener","OnAvaterCallBack",name,DelegateFactory.Action_Texture2D(function (tex)
		self:AddPicWinBack(tex)
	end),2,self.m_PicResolution, self.m_PicResolution)
end
function LuaCustomZswTemplateEditWnd:AddPicBack(bytes)
	local tex = CreateFromClass(Texture2D, 4, 4, TextureFormat.RGBA32, false)
    CommonDefs.LoadImage(tex, bytes)
	self:InitClipPicture(tex)
	self:SendPictureReviewing(tex)
end

function LuaCustomZswTemplateEditWnd:AddPicWinBack(tex)
	self:InitClipPicture(tex)
	self:SendPictureReviewing(tex)
end
--审核
function LuaCustomZswTemplateEditWnd:SendPictureReviewing(tex)
	self:UploadPic(tex)
end
--// auditStatus:-1 审核未通过 0 审核中 1 审核通过
function LuaCustomZswTemplateEditWnd:UploadPic(tex)
	local data = CommonDefs.EncodeToPNG(tex)
    local onFinished = DelegateFactory.Action_string(function (url)
        if url ==nil then
            url = ""
        end

        local templateData = CLuaZswTemplateMgr.GetCustomTemplateByIndex(self.m_TemplateIndex)
        -----审核
		local picIndex = self.m_TemplateIndex
		LuaPersonalSpaceMgrReal.UpdateHomeDecoretionPicWithIndex(url,picIndex,function(cdata)
			local code = cdata.code
			local info = cdata.data
			if info then
				templateData.picId = info.id
				LuaPersonalSpaceMgrReal.GetHomeDecorationPicWithPicIds(tostring(info.id),function(sdata)
					local scode = sdata.code
					local sinfo = sdata.data
					local list = sinfo.list
					if list and list[0] then
						templateData.auditStatus = tonumber(list[0].auditStatus)
						templateData.icon = tostring(list[0].url)
						local auditStatus = list.auditStatus
						local resulttxt = LocalString.GetString("[ffff00]审核中[-]")
						if auditStatus == -1 then
							resulttxt = LocalString.GetString("[ff0000]未过审[-]")
							self.ShenHeLabel.gameObject:SetActive(true)
						elseif auditStatus == 1 then
							resulttxt = LocalString.GetString("审核通过")
							self.ShenHeLabel.gameObject:SetActive(false)
						else
							self.ShenHeLabel.gameObject:SetActive(true)
						end
						self.ShenHeLabel.text = resulttxt
					end
				end)
			end
		end)
    end)
    CPersonalSpaceMgr.Inst:UploadPic("home_decorate_photo", data, onFinished)
end

function LuaCustomZswTemplateEditWnd:GetPicAuditStatus(picId)
end

function LuaCustomZswTemplateEditWnd:InitClipPicture(tex)
    self.m_Picture = tex
    self.Preview.mainTexture = tex
end
--@region UIEvent

function LuaCustomZswTemplateEditWnd:OnPreviewClick()
	if self.m_IsInAudit then
		MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("PictureReplace_Comfire"),
            DelegateFactory.Action(function ()
                self:SettingPicAndGifFromMobile()
            end),
            nil,
        LocalString.GetString("确定"), LocalString.GetString("取消"), false)
	else
		self:SettingPicAndGifFromMobile()
	end
end

function LuaCustomZswTemplateEditWnd:OnFastFillBtnClick()
	CLuaZswTemplateMgr.FastFillCustomTemplate(self.m_TemplateIndex)
end

function LuaCustomZswTemplateEditWnd:OnSaveBtnClick()
end

function LuaCustomZswTemplateEditWnd:OnPlaceBtnClick()
	CLuaZswTemplateMgr.PlaceCustomTemplateInHouse(self.m_TemplateIndex)
end

function LuaCustomZswTemplateEditWnd:OnRenameBtnClick()
	CInputBoxMgr.ShowInputBox(LocalString.GetString("请输入装饰物模板的新名称"), DelegateFactory.Action_string(function (val)
		if CWordFilterMgr.Inst:CheckName(val) then
			local templateData = CLuaZswTemplateMgr.GetCustomTemplateByIndex(self.m_TemplateIndex)
			templateData.name = val
			self.TemplateNameLabel.text = val
		else
			g_MessageMgr:ShowMessage("Name_Violation")
		end
	  end), 14, true, nil, LocalString.GetString("最多14个字符(7个汉字)"))
end

function LuaCustomZswTemplateEditWnd:OnTipBtnClick()
	g_MessageMgr:ShowMessage("CustomZswTemplate_Use_Tip")
end

function LuaCustomZswTemplateEditWnd:OnFastClearBtnClick()
	CLuaZswTemplateMgr.FastClearCustomTemplate(self.m_TemplateIndex)
end

function LuaCustomZswTemplateEditWnd:OnCloseButtonClick()
end

function LuaCustomZswTemplateEditWnd:OnListBtnClick()
	CLuaZswTemplateMgr.OpenCustomListWnd(self.m_TemplateIndex)
end

--@endregion UIEvent

