local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CTrackMgr = import "L10.Game.CTrackMgr"

LuaZhouNianQingFSCDetailWnd = class()
RegistClassMember(LuaZhouNianQingFSCDetailWnd, "m_RankBtn")
RegistClassMember(LuaZhouNianQingFSCDetailWnd, "m_ScoreLabel")
RegistClassMember(LuaZhouNianQingFSCDetailWnd, "m_RankLabel")

function LuaZhouNianQingFSCDetailWnd:Awake()
    self.m_RankBtn = self.transform:Find("Rank/xinxi/RankButton").gameObject
    self.m_ScoreLabel = self.transform:Find("Rank/xinxi/ScoreLabel"):GetComponent(typeof(UILabel))
    self.m_RankLabel = self.transform:Find("Rank/xinxi/RankLabel"):GetComponent(typeof(UILabel))
    UIEventListener.Get(self.m_RankBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.Anniv2023FSC_QueryRankData()
    end)
    UIEventListener.Get(self.transform:Find("Rank/xinxi/Hint/Sprite").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("FSC_OUT_GAME_RULE")
    end)
end

function LuaZhouNianQingFSCDetailWnd:OnEnable()
    g_ScriptEvent:AddListener("FSC_QueryPlayWndDataResult", self, "QueryPlayWndDataResult")

    Gac2Gas.Anniv2023FSC_QueryPlayWndData()
end

function LuaZhouNianQingFSCDetailWnd:OnDisable()
    g_ScriptEvent:RemoveListener("FSC_QueryPlayWndDataResult", self, "QueryPlayWndDataResult")
end

function LuaZhouNianQingFSCDetailWnd:QueryPlayWndDataResult(playData, rankData, selfData, npcData)
    local finishGuiding = playData[1]
    local winNpcTimes = playData[2]
    self.m_ScoreLabel.text = LocalString.GetString("历史高分 ")..(selfData and selfData.score or LocalString.GetString("—"))
    self.m_RankLabel.text = LocalString.GetString("当前排名 ")..(selfData and selfData.rank or LocalString.GetString("未上榜"))

    local guideNpc = nil
    ZhouNianQing2023_PlayWithNpc.Foreach(function(k, v)
        if not guideNpc then guideNpc = {k, v.MapId[0], v.MapId[1], v.MapId[2]} end
        local npc = self.transform:Find("NPC"..v.RewardSpCard)
        if npc then
            if winNpcTimes and winNpcTimes[k] then
                if winNpcTimes[k] > 0 then
                    npc:Find("Mask/Mask1").gameObject:SetActive(winNpcTimes[k] < 1)
                    npc:Find("Mask/Mask2").gameObject:SetActive(winNpcTimes[k] < 2)
                    npc:Find("Mask/Mask3").gameObject:SetActive(winNpcTimes[k] < 3)
                end
                npc:Find("xinxi/Label"):GetComponent(typeof(UILabel)).text = winNpcTimes[k] < 3 and LocalString.GetString("挑战赢牌") or LocalString.GetString("找他玩牌")
            end
            npc:Find("xinxi/Border/Icon"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(NPC_NPC.GetData(k).Portrait, false)
            local x, y, z = v.MapId[0], v.MapId[1], v.MapId[2]
            UIEventListener.Get(npc.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                if not LuaZhouNianQing2023Mgr:IsPassOpen() and k ~= 20040354 then
                    g_MessageMgr:ShowCustomMsg(LocalString.GetString("敬请期待~"))
                    return
                end
                if finishGuiding then
                    CUIManager.CloseUI(CLuaUIResources.ZhouNianQing2023MainWnd)
                    CTrackMgr.Inst:FindNPC(k, x, y, z, nil, nil)
                else
                    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("FSC_GUIDE_HINT"),
                        DelegateFactory.Action(function() 
                            CUIManager.CloseUI(CLuaUIResources.ZhouNianQing2023MainWnd)
                            CTrackMgr.Inst:FindNPC(guideNpc[1], guideNpc[2], guideNpc[3], guideNpc[4], nil, nil)
                        end), nil, nil, nil, false)
                end
            end)
        end
    end)
end