local COpenEntryMgr = import "L10.Game.COpenEntryMgr"
local GameSetting_Common = import "L10.Game.GameSetting_Common"
local Vector4 = import "UnityEngine.Vector4"
local Vector3 = import "UnityEngine.Vector3"
local SoundManager = import "SoundManager"
local PlayerShowDifProType = import "L10.Game.PlayerShowDifProType"
local PathType = import "DG.Tweening.PathType"
local PathMode = import "DG.Tweening.PathMode"
local Object = import "System.Object"
local MessageWndManager = import "L10.UI.MessageWndManager"
local LuaTweenUtils = import "LuaTweenUtils"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local Ease = import "DG.Tweening.Ease"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CPropertyDifMgr = import "L10.UI.CPropertyDifMgr"
local CPreciousItemShareMgr = import "L10.Game.CPreciousItemShareMgr"
local Constants = import "L10.Game.Constants"
local CItemMgr = import "L10.Game.CItemMgr"
local CGetMoneyMgr = import "L10.UI.CGetMoneyMgr"
-- local CEquipWordAutoBaptizeWnd = import "L10.UI.CEquipWordAutoBaptizeWnd"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CEquipmentBaptizeMgr = import "L10.Game.CEquipmentBaptizeMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CAutoBaptizeConditionItem = import "L10.UI.CAutoBaptizeConditionItem"
local CAppStoreReviewBox = import "L10.UI.CAppStoreReviewBox"

local CEquipBaptizeDescTable=import "L10.UI.CEquipBaptizeDescTable"
local CUIFx=import "L10.UI.CUIFx"
local CBaseEquipmentItem=import "L10.UI.CBaseEquipmentItem"
local QnRadioBox=import "L10.UI.QnRadioBox"

EnumEquipWordXiLianRequestState = {
    eNone = 0,
    eNormal = 1,
    eWaitResult=2,
    eResult = 3,
    eWaitDelta=4,
}
CLuaEquipWordAutoBaptizeWnd = class()
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"InitRate")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"RequestRate")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"m_ItemId")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"closeButton")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"equipItem")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"baptizeBtn")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"startBaptizeLabel")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"replaceBtn")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"targetBtn")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"equipBaptizeCost")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"preDescTable")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"postDescTable")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"countLabel")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"count")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"fxRoot")
-- RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"achieveTheGoal")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"accBtn")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"speedLabel")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"lookupDifBtn")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"preScoreLabel")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"postScoreLabel")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"shareBtn")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"shareFx")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"requestCount")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"successOnce")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"pause")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"newWordCount")
--是否支持高级洗炼
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"m_EnableAdvanceXiLian")
RegistClassMember(CLuaEquipWordAutoBaptizeWnd,"m_XiLianRadioBox")



function CLuaEquipWordAutoBaptizeWnd:OnClickTargetButton(go)
    local vipLevel = CClientMainPlayer.Inst.ItemProp.Vip.Level
    local equipData = CEquipmentProcessMgr.Inst.BaptizingEquipment
    local equipType = EquipmentTemplate_Equip.GetData(equipData.TemplateId).Type
    local alertPot = self.targetBtn.transform:Find("Alert").gameObject
    if vipLevel >= GameSetting_Common.GetData().AutoXiLianWordVIP and (equipData.IsGhostEquipment or equipData.IsPurpleEquipment) then
        -- if equipType ~= EnumBodyPosition_lua.Necklace then
            if alertPot.activeSelf then
                alertPot:SetActive(false)
                COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.EquipAutoBaptizeWnd)
            end
        -- end
    end
    CUIManager.ShowUI(CUIResources.EquipWordAutoBaptizeConditionWnd)
end

function CLuaEquipWordAutoBaptizeWnd:Awake()
    CLuaEquipXiLianMgr.ClearOption()
    self.m_RequestState=EnumEquipWordXiLianRequestState.eNone
    self.InitRate = 2
    self.RequestRate = 2
    self.m_ItemId = nil

    self.closeButton = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
    self.equipItem = self.transform:Find("EquipItem"):GetComponent(typeof(CBaseEquipmentItem))
    self.baptizeBtn = self.transform:Find("Anchor/BaptizeBtn").gameObject
    self.startBaptizeLabel = self.transform:Find("Anchor/BaptizeBtn/StartBaptizeLabel"):GetComponent(typeof(UILabel))
    self.replaceBtn = self.transform:Find("Anchor/ReplaceBtn").gameObject

    --洗炼目标
    self.targetBtn = self.transform:Find("Anchor/TargetBtn").gameObject
    local targetTf = self.transform:Find("Target")
    self.m_XiLianRadioBox = targetTf:GetComponent(typeof(QnRadioBox))
    local baseXiLianButton = targetTf:Find("Item1/DescLabel").gameObject
    UIEventListener.Get(baseXiLianButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickTargetButton(go)
    end)
    local advanceXiLianButton = targetTf:Find("Item2/DescLabel").gameObject
    UIEventListener.Get(advanceXiLianButton).onClick = DelegateFactory.VoidDelegate(function(go)
        local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
        local place = equip.place
        local pos = equip.pos
        local equipId = equip.itemId
        Gac2Gas.QueryAutoBaptizeTargetWordCondList(place, pos, equipId)
    end)

    --是否显示高级洗炼
    self.m_EnableAdvanceXiLian = false
    if CClientMainPlayer.Inst then
        local vipLevel = CClientMainPlayer.Inst.ItemProp.Vip.Level
        local equipData = CEquipmentProcessMgr.Inst.BaptizingEquipment
        local equipType = EquipmentTemplate_Equip.GetData(equipData.TemplateId).Type
        if vipLevel >= GameSetting_Common.GetData().AutoXiLianWordVIP and 
        (equipData.IsGhostEquipment or equipData.IsPurpleEquipment or equipData.IsRedEquipment)  then
            self.targetBtn:SetActive(false)
            targetTf.gameObject:SetActive(true)
            self.m_EnableAdvanceXiLian = true
        else
            self.targetBtn:SetActive(true)
            targetTf.gameObject:SetActive(false)
        end
    else
        self.targetBtn:SetActive(true)
        targetTf.gameObject:SetActive(false)
    end

    local script = self.transform:Find("Anchor/Cost"):GetComponent(typeof(CCommonLuaScript))
    script:Init({})
    self.equipBaptizeCost = script.m_LuaSelf
    self.preDescTable = self.transform:Find("Anchor/Desc/Desc"):GetComponent(typeof(CEquipBaptizeDescTable))
    self.postDescTable = self.transform:Find("Anchor/Desc/Desc (2)"):GetComponent(typeof(CEquipBaptizeDescTable))
    self.countLabel = self.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
    self.count = 0
    self.fxRoot = self.transform:Find("Anchor/FxRoot"):GetComponent(typeof(CUIFx))
    -- self.achieveTheGoal = false
    self.accBtn = self.transform:Find("Anchor/AccBtn").gameObject
    self.speedLabel = self.transform:Find("Anchor/AccBtn/SpeedLabel"):GetComponent(typeof(UILabel))
    self.lookupDifBtn = self.transform:Find("Anchor/LookupButton").gameObject
    self.preScoreLabel = self.transform:Find("preScoreLabel"):GetComponent(typeof(UILabel))
    self.postScoreLabel = self.transform:Find("PostScoreLabel"):GetComponent(typeof(UILabel))
    self.shareBtn = self.transform:Find("Anchor/ShareButton").gameObject
    self.shareFx = self.transform:Find("Anchor/ShareButton/Fx"):GetComponent(typeof(CUIFx))
-- self.requesting = false
self.requestCount = 0
self.successOnce = false
self.pause = false
self.newWordCount = 0

    self.shareBtn:SetActive(CPreciousItemShareMgr.Inst:EnableForumShareForEquipBaptize())
    self:HideShareFx()
    UIEventListener.Get(self.shareBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnShareButtonClick(go) end)
    UIEventListener.Get(self.accBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        if self.speedLabel.text == Constants.SpeedLevel1 then
            self.speedLabel.text = Constants.SpeedLevel2
            self.RequestRate = self.InitRate / 2
        elseif self.speedLabel.text == Constants.SpeedLevel2 then
            self.speedLabel.text = Constants.SpeedLevel3
            self.RequestRate = self.InitRate / 4
        elseif self.speedLabel.text == Constants.SpeedLevel3 then
            self.speedLabel.text = Constants.SpeedLevel1
            self.RequestRate = self.InitRate
        end
    end)

    self:UpdateCount()
    UIEventListener.Get(self.closeButton).onClick = DelegateFactory.VoidDelegate(function(go) self:TryClose(go) end)

    UIEventListener.Get(self.targetBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        self:OnClickTargetButton(go)
    end)

    UIEventListener.Get(self.baptizeBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:Baptize(go) end)

    UIEventListener.Get(self.replaceBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        --采用最新词条
        if self.newWordCount > 0 then
            local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
            local commonItem = CItemMgr.Inst:GetById(equip.itemId)
            if commonItem == nil then
                return
            end
            if equip.place == EnumItemPlace.Body and commonItem.Equip.TempGrade > CClientMainPlayer.Inst.FinalMaxLevelForEquip then
                local message = g_MessageMgr:FormatMessage("BAPTIZE_CONFIRM_LEVEL_OVER", commonItem.Equip.ColoredDisplayName, commonItem.Equip.TempGrade)
                -- MessageWndManager.ShowOKCancelMessage(message, < MakeDelegateFromCSFunction >(self.ConfirmWord, Action0, self), nil, nil, nil, false)
                MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () self:ConfirmWord() end), nil, nil, nil, false)
                return
            end
            if commonItem.Equip.TempGrade > commonItem.Equip.Grade then
                local message = g_MessageMgr:FormatMessage("BAPTIZE_CONFIRM", commonItem.Equip.ColoredDisplayName, commonItem.Equip.TempGrade)
                MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () self:ConfirmWord() end), nil, nil, nil, false)
                -- MessageWndManager.ShowOKCancelMessage(message, < MakeDelegateFromCSFunction >(self.ConfirmWord, Action0, self), nil, nil, nil, false)
                return
            else
                self:ConfirmWord()
            end
        else
            --CMiddleNoticeMgr.ShowMiddleNotice("新的词条数量太少，无法替换！");
            g_MessageMgr:ShowMessage("Baptize_WordCount_TooLow")
        end
    end)
    UIEventListener.Get(self.lookupDifBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClickLookupDifButton(go) end)
end

function CLuaEquipWordAutoBaptizeWnd:HasTempWordData( )
    local item = CItemMgr.Inst:GetById(self.m_ItemId)
    if item ~= nil then
        if item.Equip:HasTempWordData() then
            return true
        end
    end
    return false
end
function CLuaEquipWordAutoBaptizeWnd:Init( )
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    if equip ~= nil then
        self.m_ItemId = equip.itemId
    end

    self:SetPreScore(0)
    self:SetPostScore(0)

    self:InitRequestRate()
    --默认装备的洗炼目标是全选
    CEquipmentBaptizeMgr.Inst:InitCondition()

    self:InitInfo()
    self:TryShowLastResult()
    --vip
    local vipLevel = CClientMainPlayer.Inst.ItemProp.Vip.Level
    local equipData = CEquipmentProcessMgr.Inst.BaptizingEquipment
    local equipType = EquipmentTemplate_Equip.GetData(equipData.TemplateId).Type

    -- local alertPot = self.targetBtn.transform:Find("Alert").gameObject

    -- if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.EquipAutoBaptizeWnd) then
    --     alertPot:SetActive(false)
    -- elseif vipLevel >= GameSetting_Common.GetData().AutoXiLianWordVIP and (equipData.IsGhostEquipment or equipData.IsPurpleEquipment) and equipType ~= EnumBodyPosition_lua.Necklace then
    --     alertPot:SetActive(true)
    -- end
    

end
function CLuaEquipWordAutoBaptizeWnd:TryClose( go) 
    if self.m_RequestState~=EnumEquipWordXiLianRequestState.eNone or self.successOnce then
        local tip = g_MessageMgr:FormatMessage("Close_Crafting_Equipment")
        MessageWndManager.ShowOKCancelMessage(tip, DelegateFactory.Action(function () 
            CUIManager.CloseUI(CUIResources.EquipWordAutoBaptizeWnd) 
        end), nil, nil, nil, false)
    else
        CUIManager.CloseUI(CUIResources.EquipWordAutoBaptizeWnd) 
    end
end

function CLuaEquipWordAutoBaptizeWnd:OnClickLookupDifButton( go) 
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local equipListOnBody = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Body)
    local find = false
    do
        local i = 0
        while i < equipListOnBody.Count do
            if equipListOnBody[i].itemId == equip.itemId then
                find = true
                break
            end
            i = i + 1
        end
    end
    if find then
        --是否装备
        Gac2Gas.TryConfirmBaptizeWord(EnumToInt(equip.place), equip.pos, equip.itemId, true)
    else
        g_MessageMgr:ShowMessage("Equip_Compare_NotWear_Tips")
    end
end

function CLuaEquipWordAutoBaptizeWnd:Baptize( go) 
    CLuaEquipMgr:CheckUnsetComposite(self.m_ItemId, function()
        CLuaEquipMgr:CheckUnsetRecreate(self.m_ItemId, function()
            self:GoBaptize(go)
        end
        )
    end) 
end

function CLuaEquipWordAutoBaptizeWnd:GoBaptize( go) 
    --开始洗炼
    if self.m_RequestState==EnumEquipWordXiLianRequestState.eNone then

        --如果选了高级洗炼，并且没有设任何目标的话，就不让玩家洗
        if self.m_EnableAdvanceXiLian and self.m_XiLianRadioBox.CurrentSelectIndex==1 then
            if not CLuaEquipXiLianMgr.IsAdvanceXiLianHaveChoice() then
                g_MessageMgr:ShowMessage("Advance_XiLian_NoTarget")
                return
            end
        end

        local fixedWordList = nil
        local extWordList = nil
    
        local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
        local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
        if equip then
            local item = CItemMgr.Inst:GetById(equip.itemId)
            if item then
                if item.Equip:HasTempWordData() then
                    fixedWordList = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, UInt32)), UInt32, item.Equip:GetTempFixedWordArray())
                    extWordList = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, UInt32)), UInt32, item.Equip:GetTempExtWordArray())
                end
            end
        end
        local hasTemp = fixedWordList and extWordList

        local achieveTheGoal = false
        if hasTemp then
            if self.m_EnableAdvanceXiLian and self.m_XiLianRadioBox.CurrentSelectIndex==1 then
                achieveTheGoal = CLuaEquipXiLianMgr.CheckBaptizeResult(fixedWordList, extWordList,true)
            else
                achieveTheGoal = CLuaEquipXiLianMgr.CheckBaptizeResult(fixedWordList, extWordList)
            end
        end
        if achieveTheGoal then
            local tip = g_MessageMgr:FormatMessage("Already_Reach_Target")
            MessageWndManager.ShowOKCancelMessage(tip, DelegateFactory.Action(function () 
                self:TryBaptize()
            end), nil, nil, nil, false)
        else
            self:TryBaptize()
        end
    else
        self.m_RequestState=EnumEquipWordXiLianRequestState.eNone
        self:EndRequest()
    end
end

function CLuaEquipWordAutoBaptizeWnd:TryBaptize( )
    --材料判断
    if not self.equipBaptizeCost:yinliangEnough() then
        g_MessageMgr:ShowMessage("SILVER_NOT_ENOUGH")
        return
    end
    local startRequest = false
    --净瓶不够 并且不自动消耗灵玉
    if not self.equipBaptizeCost:isJingPingEnough() and not self.equipBaptizeCost:isAutoCostJade() then
        g_MessageMgr:ShowMessage("BAPTIZE_EQUIP_WORD_ITEM_NOT_ENOUGH", List2Params(InitializeList(CreateFromClass(MakeGenericClass(List, Object)), Object, LocalString.GetString("净瓶"))))
    elseif not self.equipBaptizeCost:isJingPingEnough() and self.equipBaptizeCost:isAutoCostJade() then
        local canBaptize = true
        if self.equipBaptizeCost:isAutoCostJade() then
            if not self.equipBaptizeCost:lingyuEnough() then
                canBaptize = false
                CGetMoneyMgr.Inst:GetLingYu(self.equipBaptizeCost:LingYuCost(), self.equipBaptizeCost.TemplateId)
            end
        end
        if canBaptize then
            startRequest = true
        end
    else
        --净瓶够了
        startRequest = true
    end

    if startRequest then
        self:HideShareFx()
        self:BeginRequest()
        self.m_RequestState=EnumEquipWordXiLianRequestState.eNormal
    end
end




function CLuaEquipWordAutoBaptizeWnd:ConfirmWord( )
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    Gac2Gas.BaptizeWordResultConfirm(EnumToInt(equip.place), equip.pos, equip.itemId, true)
    g_MessageMgr:ShowMessage("Baptize_Use_New_Word")
    --点完之后就置灰
    --CUICommonDef.SetActive(keepBtn, false);
    CUICommonDef.SetActive(self.replaceBtn, false, true)

    --needConfirm = false;
    self.successOnce = false
    -- self.achieveTheGoal = false
    self.postDescTable:Clear()
    self:SetPostScore(0)
end
function CLuaEquipWordAutoBaptizeWnd:BeginRequest( )
    g_MessageMgr:ShowMessage("Start_Refinery")
    -- self.achieveTheGoal = false
    self.successOnce = false
    self.requestCount = 0
    --baptizeResult = new CEquipmentBaptizeMgr.BaptizeResult();
    -- self.requesting = true

    CUICommonDef.SetActive(self.replaceBtn, false, true)

    self.startBaptizeLabel.text = LocalString.GetString("停止洗炼")
end
function CLuaEquipWordAutoBaptizeWnd:EndRequest( )
    -- self.requesting = false

    g_MessageMgr:ShowMessage("Stop_Refinery")
    --所有的toggle恢复操作
    local cmps = CommonDefs.GetComponentsInChildren_Component_Type(self.transform, typeof(CAutoBaptizeConditionItem))
    CommonDefs.EnumerableIterate(cmps, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        CUICommonDef.SetActive(item.gameObject, true, true)
    end))

    --成功一次就行了
    if self.successOnce then
        CUICommonDef.SetActive(self.replaceBtn, true, true)
    end

    self.startBaptizeLabel.text = LocalString.GetString("开始洗炼")

    -- self:ClearActions()
end
function CLuaEquipWordAutoBaptizeWnd:InitInfo( )
    self.equipBaptizeCost:SetBaseInfo()

    local equipData = CEquipmentProcessMgr.Inst.BaptizingEquipment

    local fixedWordList= equipData:GetFixedWordList(false)
    local extWordList= equipData:GetExtWordList(false)

    local fixedWordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(self.m_ItemId,List2Table(fixedWordList))
    local fixedWordMaxList = Table2List(fixedWordMaxTable, MakeGenericClass(List, Boolean))

    local extWordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(self.m_ItemId,List2Table(extWordList))
    local extWordMaxList = Table2List(extWordMaxTable, MakeGenericClass(List, Boolean))

    self.preDescTable:Init(fixedWordList, extWordList, nil, nil,fixedWordMaxList,extWordMaxList)
    self:SetPreScore(equipData.Score)

    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    self.equipItem:UpdateData(equipInfo.itemId)
    --TryShowLastResult();

    self:UpdateReplaceBtnState()
end
function CLuaEquipWordAutoBaptizeWnd:UpdateReplaceBtnState( )
    if self:HasTempWordData() then
        CUICommonDef.SetActive(self.replaceBtn, true, true)
    else
        CUICommonDef.SetActive(self.replaceBtn, false, true)
    end
end
function CLuaEquipWordAutoBaptizeWnd:RequestBaptize( )
    --sending = true;
    if not CEquipmentProcessMgr.Inst:CheckOperationConflict(CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize) then
        return
    end

    self:SendAutoXiLianRpc()
end

function CLuaEquipWordAutoBaptizeWnd:SendAutoXiLianRpc()
    local autoExChange = self.equipBaptizeCost:isAutoCostJade()
    CEquipmentProcessMgr.Inst.BaptizeAutoBuyItem = autoExChange
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize

    if self.m_EnableAdvanceXiLian and self.m_XiLianRadioBox.CurrentSelectIndex==1 then
        --高级洗炼
        Gac2Gas.RequestAutoBaptizeWord(EnumToInt(equip.place), equip.pos, 
            equip.itemId, 
            CLuaEquipXiLianMgr.GetAdvanceXiLianCountParam(), 
            "", 
            CLuaEquipXiLianMgr.GetAutoBaptizeParam6(), 
            CLuaEquipXiLianMgr.GetAutoBaptizeParam7(),
            autoExChange,
            true)
    else
        --基础洗炼
        Gac2Gas.RequestAutoBaptizeWord(EnumToInt(equip.place), equip.pos, 
            equip.itemId, 
            CLuaEquipXiLianMgr.GetAutoBaptizeParam4(), 
            CLuaEquipXiLianMgr.GetAutoBaptizeParam5(), 
            "",
            "",
            autoExChange,
            false)
    end
end

function CLuaEquipWordAutoBaptizeWnd:TryShowLastResult( )
    self.postDescTable:Clear()
    self:SetPostScore(0)

    self.newWordCount = 0
    --尝试显示上一次未被确认的洗练结果
    if CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize ~= nil then
        local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
        local item = CItemMgr.Inst:GetById(CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize.itemId)
        if item ~= nil then
            if item.Equip:HasTempWordData() then
                self.successOnce = true
                --请求数据
                Gac2Gas.RequestEquipPropertyChangedWithTempWords(EnumToInt(equip.place), equip.pos, equip.itemId)
            else
                self.successOnce = false
            end
        end
    end
end
function CLuaEquipWordAutoBaptizeWnd:UpdateStartBaptizeBtnState( )
    local selectedCount = 0
    local cmps = CommonDefs.GetComponentsInChildren_Component_Type(self.transform, typeof(CAutoBaptizeConditionItem))
    CommonDefs.EnumerableIterate(cmps, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        if item.Selected then
            selectedCount = selectedCount + 1
        end
    end))
    if selectedCount > 0 then
        CUICommonDef.SetActive(self.baptizeBtn, true, true)
    else
        CUICommonDef.SetActive(self.baptizeBtn, false, true)
    end
end
function CLuaEquipWordAutoBaptizeWnd:OnEnable( )
    g_ScriptEvent:AddListener("BaptizeWordItemNotEnough", self, "OnFail_ItemNotEnough")
    g_ScriptEvent:AddListener("BaptizeWordResult", self, "OnSuccess")
    g_ScriptEvent:AddListener("BaptizeFail", self, "OnFail")
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:AddListener("Guide_ChangeView", self, "ProcessUpdateViewEvent")
    g_ScriptEvent:AddListener("ChangeAutoBaptizeCondition", self, "UpdateReplaceBtnState")
    g_ScriptEvent:AddListener("RequestEquipPropertyChangedWithTempWordsReturn", self, "OnRequestEquipPropertyChangedWithTempWordsReturn")
    g_ScriptEvent:AddListener("TryConfirmBaptizeWordResult", self, "OnTryConfirmBaptizeWordResult")
    g_ScriptEvent:AddListener("ConfirmAdvanceXiLian", self, "OnConfirmAdvanceXiLian")
end
function CLuaEquipWordAutoBaptizeWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("BaptizeWordItemNotEnough", self, "OnFail_ItemNotEnough")
    g_ScriptEvent:RemoveListener("BaptizeWordResult", self, "OnSuccess")
    g_ScriptEvent:RemoveListener("BaptizeFail", self, "OnFail")
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:RemoveListener("Guide_ChangeView", self, "ProcessUpdateViewEvent")
    g_ScriptEvent:RemoveListener("ChangeAutoBaptizeCondition", self, "UpdateReplaceBtnState")
    g_ScriptEvent:RemoveListener("RequestEquipPropertyChangedWithTempWordsReturn", self, "OnRequestEquipPropertyChangedWithTempWordsReturn")
    g_ScriptEvent:RemoveListener("TryConfirmBaptizeWordResult", self, "OnTryConfirmBaptizeWordResult")
    g_ScriptEvent:RemoveListener("ConfirmAdvanceXiLian", self, "OnConfirmAdvanceXiLian")

    LuaAutoBaptizeNecessaryConditionMgr:ClearNecessaryCondition()
end
function CLuaEquipWordAutoBaptizeWnd:OnConfirmAdvanceXiLian()
    self.m_XiLianRadioBox:ChangeTo(1,true)
end

function CLuaEquipWordAutoBaptizeWnd:OnRequestEquipPropertyChangedWithTempWordsReturn( args) 
    local dic = args[0]
    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    --发出请求
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    if equip ~= nil then
        local item = CItemMgr.Inst:GetById(equip.itemId)
        if item ~= nil then
            self.newWordCount = 0
            local tempFixedWords = item.Equip:GetTempFixedWordArray()
            do
                local i = 0
                while i < tempFixedWords.Length do
                    if tempFixedWords[i] > 0 then
                        self.newWordCount = self.newWordCount + 1
                    end
                    i = i + 1
                end
            end
            local tempExtraWords = item.Equip:GetTempFixedWordArray()
            do
                local i = 0
                while i < tempExtraWords.Length do
                    if tempExtraWords[i] > 0 then
                        self.newWordCount = self.newWordCount + 1
                    end
                    i = i + 1
                end
            end


            --如果有未确认的词条
            if item.Equip:HasTempWordData() then

                local fixedWordList = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, UInt32)), UInt32, item.Equip:GetTempFixedWordArray())
                local extWordList = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, UInt32)), UInt32, item.Equip:GetTempExtWordArray())

                local fixedWordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(self.m_ItemId,List2Table(fixedWordList))
                local fixedWordMaxList = Table2List(fixedWordMaxTable, MakeGenericClass(List, Boolean))
            
                local extWordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(self.m_ItemId,List2Table(extWordList))
                local extWordMaxList = Table2List(extWordMaxTable, MakeGenericClass(List, Boolean))

                self.postDescTable:Init(fixedWordList, extWordList, nil, dic,fixedWordMaxList,extWordMaxList)
                -- if CLuaEquipXiLianMgr.CheckBaptizeResult(list1, list2) then
                --     self.achieveTheGoal = true
                -- end

                self:SetPostScore(item.Equip.TempScore)
                CUICommonDef.SetActive(self.replaceBtn, true, true)
                self.successOnce = true
            end
        end
    end
    self:UpdateReplaceBtnState()
end
function CLuaEquipWordAutoBaptizeWnd:ProcessUpdateViewEvent( eventType) 
    if CClientMainPlayer.Inst == nil then
        self.pause = true
        return
    end
    --看一下这个窗口的类型
    local top = CUIManager.instance:GetTopPopUI()
    if top ~= nil then
        if top.Name == CUIResources.EquipWordAutoBaptizeWnd.Name then
            self.pause = false
        else
            self.pause = true
        end
    end
end
function CLuaEquipWordAutoBaptizeWnd:OnSendItem( args ) 
    local itemId = args[0]
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    if equip ~= nil and equip.itemId == itemId then
        --RefreshCurrentEquipWordDescription();
        self:InitInfo()
        self.equipBaptizeCost:SetBaseInfo()
    end
end


function CLuaEquipWordAutoBaptizeWnd:Update()
    if self.pause then
        return
    end

    if CClientMainPlayer.Inst==nil then
        self.m_RequestState=EnumEquipWordXiLianRequestState.eNone
        self:EndRequest()
        return
    end

    if self.m_RequestState==EnumEquipWordXiLianRequestState.eNormal then
        --检查有没有选择高级洗炼目标
        if self.m_EnableAdvanceXiLian and self.m_XiLianRadioBox.CurrentSelectIndex==1 then
            if not CLuaEquipXiLianMgr.IsAdvanceXiLianHaveChoice() then
                g_MessageMgr:ShowMessage("Advance_XiLian_NoTarget")
                self.m_RequestState=EnumEquipWordXiLianRequestState.eNone
                self:EndRequest()
                return
            end
        end

        --材料判断
        if not self.equipBaptizeCost:yinliangEnough() then
            g_MessageMgr:ShowMessage("SILVER_NOT_ENOUGH")
            return
        end
        local startRequest = false
        --净瓶不够 并且不自动消耗灵玉
        if not self.equipBaptizeCost:isJingPingEnough() and not self.equipBaptizeCost:isAutoCostJade() then
            g_MessageMgr:ShowMessage("BAPTIZE_EQUIP_WORD_ITEM_NOT_ENOUGH", List2Params(InitializeList(CreateFromClass(MakeGenericClass(List, Object)), Object, LocalString.GetString("净瓶"))))
        elseif not self.equipBaptizeCost:isJingPingEnough() and self.equipBaptizeCost:isAutoCostJade() then
            local canBaptize = true
            if self.equipBaptizeCost:isAutoCostJade() then
                if not self.equipBaptizeCost:lingyuEnough() then
                    canBaptize = false
                    CGetMoneyMgr.Inst:GetLingYu(self.equipBaptizeCost:LingYuCost(), self.equipBaptizeCost.TemplateId)
                end
            end
            if canBaptize then
                startRequest = true
                -- self.autoBaptizeRoutine = self:StartCoroutine(self:StartAutoBaptizeRoutine())
            end
        else
            --净瓶够了
            -- self.autoBaptizeRoutine = self:StartCoroutine(self:StartAutoBaptizeRoutine())
            startRequest = true
        end



        if startRequest then
            self.equipItem:UpdateFx(false)
            self:SendAutoXiLianRpc()
            self.m_RequestState=EnumEquipWordXiLianRequestState.eWaitResult
            return
        else
            self.m_RequestState=EnumEquipWordXiLianRequestState.eNone
            self:EndRequest()
            return
        end
    elseif self.m_RequestState==EnumEquipWordXiLianRequestState.eResult then
        --看看结果如何
        --继续洗 等待一点时间
        self.m_RequestState=EnumEquipWordXiLianRequestState.eWaitDelta
        self.m_StartWaitTime=Time.time

    elseif self.m_RequestState==EnumEquipWordXiLianRequestState.eWaitDelta then--等待
        if Time.time>self.m_StartWaitTime + self.RequestRate then
            self.m_RequestState=EnumEquipWordXiLianRequestState.eNormal
        end
    end
end
local result = {}
--洗炼结果
function CLuaEquipWordAutoBaptizeWnd:OnFail_ItemNotEnough( )
    self.m_RequestState=EnumEquipWordXiLianRequestState.eNone

    result.success = false
    result["end"] = true
    result.itemNotEnough = true

    self:EndRequest()

end
function CLuaEquipWordAutoBaptizeWnd:OnFail( args) 
    local equipId = args[0]
    self.m_RequestState=EnumEquipWordXiLianRequestState.eNone
    result.success = false
    result["end"] = true
    result.itemNotEnough = false

    -- if self.onFail ~= nil then
    --     GenericDelegateInvoke(self.onFail, Table2ArrayWithCount({equipId}, 1, MakeArrayClass(Object)))
    -- end
    self:EndRequest()
end
function CLuaEquipWordAutoBaptizeWnd:OnSuccess( args ) 
    local fixedWordList, extraWordList, score, changeProperties = args[0],args[1],args[2],args[3]
    if self.m_RequestState==EnumBabyWashRequestState.eWaitResult then
        self.m_RequestState=EnumEquipWordXiLianRequestState.eResult
    end

    result.success = true
    result["end"] = true
    result.fixedWordList = fixedWordList
    result.extraWordList = extraWordList;
    result.changeProperties = changeProperties;
    result.score = score;

    self.newWordCount = 0
    for i=1,fixedWordList.Count do
        if fixedWordList[i-1]>0 then
            self.newWordCount = self.newWordCount+1
        end
    end
    for i=1,extraWordList.Count do
        if extraWordList[i-1]>0 then
            self.newWordCount = self.newWordCount+1
        end
    end
    if self.newWordCount>=7 then
        CAppStoreReviewBox.Show()
    end

--洗炼成功
    self.count=self.count+1
    self.successOnce = true
    self:UpdateCount()

    self.requestCount=self.requestCount+1

    local fixedWordList,extWordList = result.fixedWordList, result.extraWordList
    local fixedWordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(self.m_ItemId,List2Table(fixedWordList))
    local fixedWordMaxList = Table2List(fixedWordMaxTable, MakeGenericClass(List, Boolean))

    local extWordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(self.m_ItemId,List2Table(extWordList))
    local extWordMaxList = Table2List(extWordMaxTable, MakeGenericClass(List, Boolean))

    self.postDescTable:Init(fixedWordList,extWordList,nil,result.changeProperties,fixedWordMaxList,extWordMaxList)
    self:SetPostScore(result.score)
    -- //满足洗练的条件
    local checkResult = false
    if self.m_EnableAdvanceXiLian and self.m_XiLianRadioBox.CurrentSelectIndex==1 then
        checkResult = CLuaEquipXiLianMgr.CheckBaptizeResult(result.fixedWordList, result.extraWordList,true)
    else
        checkResult = CLuaEquipXiLianMgr.CheckBaptizeResult(result.fixedWordList, result.extraWordList)
    end

    if  checkResult then
        -- //达到目标，自动确认
        g_MessageMgr:ShowMessage("Baptize_Achieve_The_Goal")
        -- achieveTheGoal = true


        local autoExChange = self.equipBaptizeCost:isAutoCostJade()
        CEquipmentProcessMgr.Inst.BaptizeAutoBuyItem = autoExChange
        local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    
        if self.m_EnableAdvanceXiLian and self.m_XiLianRadioBox.CurrentSelectIndex==1 then
            --高级洗炼
            Gac2Gas.FinishAutoBaptizeWord(EnumToInt(equip.place), equip.pos, 
                equip.itemId, 
                CLuaEquipXiLianMgr.GetAdvanceXiLianCountParam(), 
                "", 
                CLuaEquipXiLianMgr.GetAutoBaptizeParam6(), 
                CLuaEquipXiLianMgr.GetAutoBaptizeParam7(),
                autoExChange,
                true)
        else
            --基础洗炼
            Gac2Gas.FinishAutoBaptizeWord(EnumToInt(equip.place), equip.pos, 
                equip.itemId, 
                CLuaEquipXiLianMgr.GetAutoBaptizeParam4(), 
                CLuaEquipXiLianMgr.GetAutoBaptizeParam5(), 
                "",
                "",
                autoExChange,
                false)
        end

        -- //EndRequest();
        self.fxRoot:DestroyFx()
        self.fxRoot:LoadFx(CUIFxPaths.EquipmentBaptizeGoalReached)
        self.equipItem:UpdateFx(true)
        SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.MissionDone, Vector3.zero)
        self.requesting = false
        -- //yield break;
        CPreciousItemShareMgr.Inst:RequestTriggerBaptizeShare(self.equipItem.itemId,true)
        self:ShowShareFx()

        --停止
        self.m_RequestState=EnumBabyWashRequestState.eNone
        self:EndRequest()
    end

    -- if self.onSuccess ~= nil then
    --     GenericDelegateInvoke(self.onSuccess, Table2ArrayWithCount({fixedWordList, extraWordList, score, changeProperties}, 4, MakeArrayClass(Object)))
    -- end
end
function CLuaEquipWordAutoBaptizeWnd:InitRequestRate()
    self.RequestRate = GameSetting_Client.GetData().EQUIP_AUTO_XILIAN_LIMIT_TIME
    self.InitRate = self.RequestRate
    self.speedLabel.text = Constants.SpeedLevel1
end

function CLuaEquipWordAutoBaptizeWnd:UpdateCount()
    self.countLabel.text = SafeStringFormat3(LocalString.GetString("已洗炼：%d次"), self.count)
end

function CLuaEquipWordAutoBaptizeWnd:SetPreScore(score)
    CEquipmentBaptizeMgr.Inst.preScore = score
    self.preScoreLabel.text = CEquipmentBaptizeMgr.FormatScore(score)
end
function CLuaEquipWordAutoBaptizeWnd:SetPostScore(score)
    CEquipmentBaptizeMgr.Inst.postScore = score
    self.postScoreLabel.text = CEquipmentBaptizeMgr.FormatScore(score)
end

function CLuaEquipWordAutoBaptizeWnd:OnSetItemAt(args)
    local place,position,oldItemId,newItemId = args[0],args[1],args[2],args[3]
    if oldItemId == nil and newItemId == nil then
        return  
    end
    local itemId = oldItemId and oldItemId or newItemId
    self:OnSendItem(itemId)
end

--属性比较
function CLuaEquipWordAutoBaptizeWnd:OnTryConfirmBaptizeWordResult( args) 
    local obj = args[0]
    local list = TypeAs(obj, typeof(MakeGenericClass(List, PlayerShowDifProType)))
    if list == nil then
        return
    end
    if list.Count == 0 then
        g_MessageMgr:ShowMessage("BaptizeWordResultNoPropDif")
        return
    end

    CPropertyDifMgr.ShowPlayerProDifWithEquip(list)
end
function CLuaEquipWordAutoBaptizeWnd:ShowShareFx( )
    if self.shareBtn.activeSelf then
        self.shareFx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
        LuaTweenUtils.DOKill(self.shareFx.FxRoot, false)
        local background = CommonDefs.GetComponent_GameObject_Type(self.shareBtn, typeof(UISprite))
        local gap = 5
        local bounds = Vector4(- background.width * 0.5 + gap, background.height * 0.5 - gap, background.width - gap * 2, background.height - gap * 2)
        local waypoints = CUICommonDef.GetWayPoints(bounds, 1, true)
        self.shareFx.FxRoot.localPosition = waypoints[0]
        LuaTweenUtils.DOLuaLocalPath(self.shareFx.FxRoot, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
    end
end
function CLuaEquipWordAutoBaptizeWnd:HideShareFx( )
    if self.shareBtn.activeSelf then
        LuaTweenUtils.DOKill(self.shareFx.FxRoot, false)
        self.shareFx:DestroyFx()
    end
end


function CLuaEquipWordAutoBaptizeWnd:OnShareButtonClick(go)
    self:HideShareFx()
    CPreciousItemShareMgr.Inst:ShowEquipBaptizeShareWnd()
end
