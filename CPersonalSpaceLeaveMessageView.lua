-- Auto Generated!!
local AlignType = import "L10.UI.CTooltip+AlignType"
local AlignType1 = import "L10.UI.CItemInfoMgr+AlignType"
local CButton = import "L10.UI.CButton"
local CChatLinkMgr = import "CChatLinkMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CPersonalSpace_GetMessageItem = import "L10.Game.CPersonalSpace_GetMessageItem"
local CPersonalSpace_LeaveMessageCacheData = import "L10.Game.CPersonalSpace_LeaveMessageCacheData"
local CPersonalSpaceDetailMoment = import "L10.UI.CPersonalSpaceDetailMoment"
local CPersonalSpaceLeaveMessageView = import "L10.UI.CPersonalSpaceLeaveMessageView"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CPersonalSpaceWnd = import "L10.UI.CPersonalSpaceWnd"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local GameplayItem_Setting = import "L10.Game.GameplayItem_Setting"
local Item_Item = import "L10.Game.Item_Item"
local L10 = import "L10"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUIText = import "NGUIText"
local NGUITools = import "NGUITools"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local Quaternion = import "UnityEngine.Quaternion"
local String = import "System.String"
local TweenPosition = import "TweenPosition"
local UICamera = import "UICamera"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local UInt64 = import "System.UInt64"
local UIScrollView = import "UIScrollView"
local UISprite = import "UISprite"
local UITable = import "UITable"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CGoogleTranslateLabel = import "L10.UI.CGoogleTranslateLabel"

CPersonalSpaceLeaveMessageView.m_ResetPanel_CS2LuaHook = function (this)
    UIEventListener.Get(this.addRenqiBtn).onClick = nil
    UIEventListener.Get(this.selfLeaveMessageBtn).onClick = nil
    UIEventListener.Get(this.otherLeaveMessageBtn).onClick = nil
    UIEventListener.Get(this.leaveMessagePanel_emotionBtn).onClick = nil
    local barNodeTransform = this.barNode.transform
    if barNodeTransform.localPosition.x ~= 0 then
        CommonDefs.GetComponent_Component_Type(barNodeTransform, typeof(TweenPosition)):ResetToBeginning()
        barNodeTransform.localPosition = Vector3(0, barNodeTransform.localPosition.y, barNodeTransform.localPosition.z)
    end
    CommonDefs.DictClear(this.existingLinks)
    this.ReplyPersonId = 0
    this.ReplyPersonName = ""

    CommonDefs.ListClear(CPersonalSpaceMgr.Inst.leaveMessageCacheList)
    CommonDefs.DictClear(CPersonalSpaceMgr.Inst.leaveMessageCacheDic)
    Extensions.RemoveAllChildren(this.listTable.transform)
end
CPersonalSpaceLeaveMessageView.m_InitMessageList_CS2LuaHook = function (this, playerId)
    this.emptyNode:SetActive(true)
    this:updateLeaveMessageCacheList()

    UIEventListener.Get(this.leaveMessagePanel_emotionBtn).onClick = MakeDelegateFromCSFunction(this.OnEmoticonButtonClick, VoidDelegate, this)

    CPersonalSpaceMgr.Inst:GetMessages(playerId, 0, DelegateFactory.Action_CPersonalSpace_GetMessages_Ret(function (data)
        if not this.listScrollViewIndicator then
            return
        end
        this.listScrollViewIndicator.enableShowNode = true
        if data.code == 0 then
            this.MessageCommentable = data.data.commentable
            this:InitSendMessageNode()
            this:UpdateLeaveMessageScrollView(data, false)
        end
    end), nil)

    local showMoreBtn = this.listScrollViewIndicator.transform:Find("ShowNode").gameObject
    this:SetShowMoreBtn(showMoreBtn, playerId)
end
CPersonalSpaceLeaveMessageView.m_Init_CS2LuaHook = function (this, playerId, playerName, selfPanel)
    this.LeaveMessageOwnerId = playerId
    this:ResetPanel()
    this.templateNode:SetActive(false)
    this.selfPanelSign = selfPanel

    if selfPanel then
        this.selfOpArea:SetActive(true)
        this.otherOpArea:SetActive(false)
        this.selfTopBar:SetActive(true)
        this.otherTopBar:SetActive(false)
    else
        this.selfOpArea:SetActive(false)
        this.otherOpArea:SetActive(true)
        this.selfTopBar:SetActive(false)
        this.otherTopBar:SetActive(true)
    end

    UIEventListener.Get(this.settingBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        if CPersonalSpaceWnd.Instance ~= nil then
            CPersonalSpaceWnd.Instance:InitSettingPanel()
        end
    end)

    CPersonalSpaceMgr.Inst:SetSpaceShareBtn(this.shareBtn, playerId, playerName)

    UIEventListener.Get(this.addRenqiBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        if System.String.IsNullOrEmpty(playerName) then
            if not System.String.IsNullOrEmpty(CPersonalSpaceWnd.Instance.PersonalName) then
                CPersonalSpaceMgr.Inst:RequestAddPopularity(playerId, CPersonalSpaceWnd.Instance.PersonalName)
            end
        else
            CPersonalSpaceMgr.Inst:RequestAddPopularity(playerId, playerName)
        end
    end)

    UIEventListener.Get(this.sendFlowerBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        if System.String.IsNullOrEmpty(playerName) then
            if not System.String.IsNullOrEmpty(CPersonalSpaceWnd.Instance.PersonalName) then
                this:InitSendFlowerPanel(playerId, CPersonalSpaceWnd.Instance.PersonalName, 0, 1)
            end
        else
            this:InitSendFlowerPanel(playerId, playerName, 0, 1)
        end
    end)

    this:InitMessageList(playerId)
end
CPersonalSpaceLeaveMessageView.m_InitLeaveMessageList_CS2LuaHook = function (this, savePosSign)
    local savePos = this.listScrollView.transform.localPosition
    local saveOffsetY = this.listPanel.clipOffset.y

    Extensions.RemoveAllChildren(this.listTable.transform)

    if CPersonalSpaceMgr.Inst.leaveMessageCacheList.Count <= 0 then
        return
    end

    local count = 0
    do
        local i = 0
        while i < CPersonalSpaceMgr.Inst.leaveMessageCacheList.Count do
            local node = CPersonalSpaceMgr.Inst.leaveMessageCacheList[i].node

            if node ~= nil and this.listTable.gameObject ~= nil then
                node:SetActive(false)
                local t = node.transform
                t.parent = this.listTable.gameObject.transform
                node:SetActive(true)
                t.localPosition = Vector3.zero
                t.localRotation = Quaternion.identity
                t.localScale = Vector3.one
                node.layer = this.listTable.gameObject.layer

                if count == 0 then
                    CommonDefs.GetComponent_GameObject_Type(node.transform:Find("divide1").gameObject, typeof(UISprite)).alpha = 0
                else
                    CommonDefs.GetComponent_GameObject_Type(node.transform:Find("divide1").gameObject, typeof(UISprite)).alpha = 1
                end

                CommonDefs.GetComponent_Component_Type(node.transform:Find("bgbutton"), typeof(UISprite)):ResizeCollider()

                count = count + 1
            end
            i = i + 1
        end
    end

    if this.listTable.transform.childCount <= 0 then
        this.emptyNode:SetActive(true)
    else
        this.emptyNode:SetActive(false)
    end

    this.listTable:Reposition()
    this.listScrollView:ResetPosition()

    if savePosSign then
        this.listScrollView.transform.localPosition = savePos
        this.listPanel.clipOffset = Vector2(this.listPanel.clipOffset.x, saveOffsetY)
    end
end
CPersonalSpaceLeaveMessageView.m_UpdateLeaveMessageScrollView_CS2LuaHook = function (this, data, savePos)
    if data.data.list == nil or (data.data.list ~= nil and data.data.list.Length < CPersonalSpaceLeaveMessageView.MaxEveryGetNum) then
        this.listScrollViewIndicator.enableShowNode = false
    end

    if data.code == 0 then
        if data.data.list ~= nil and data.data.list.Length > 0 then
            this:updateLeaveMessageCache(data.data.list)
        end
        this:InitLeaveMessageList(savePos)
    else
        this:InitLeaveMessageList(savePos)
    end
end
CPersonalSpaceLeaveMessageView.m_SetShowMoreBtn_CS2LuaHook = function (this, showMoreBtn, targetid)
    UIEventListener.Get(showMoreBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        showMoreBtn:SetActive(false)
        local lastId = 0
        if CPersonalSpaceMgr.Inst.leaveMessageCacheList.Count > 0 then
            local lastData = CPersonalSpaceMgr.Inst.leaveMessageCacheList[CPersonalSpaceMgr.Inst.leaveMessageCacheList.Count - 1]
            lastId = lastData.data.id
        end
        CPersonalSpaceMgr.Inst:GetMessages(targetid, lastId, DelegateFactory.Action_CPersonalSpace_GetMessages_Ret(function (data)
            if not this.listScrollViewIndicator then
                return
            end
            if data.data.list ~= nil then
                this:updateLeaveMessageCacheList()
                -- save the old node for next show use, this can speed up the init
                this:UpdateLeaveMessageScrollView(data, true)
            else
                this.listScrollViewIndicator.enableShowNode = false
                --TODO show no more
            end
        end), nil)
    end)
end
CPersonalSpaceLeaveMessageView.m_SetInputDefaultValue_CS2LuaHook = function (this, roleid, rolename, node, info)
    if not this.MessageCommentable then
        g_MessageMgr:ShowMessage("CANNOT_COMMENT")
        return
    end

    this.ReplyPersonId = roleid
    this.ReplyPersonName = rolename
    this.leaveMessagePanel_statusInput.value = ""
    this.leaveMessagePanel_statusText.text = CPersonalSpaceDetailMoment.ReplyPrefix .. this.ReplyPersonName
    this.leaveMessagePanel_statusInput.defaultText = CPersonalSpaceDetailMoment.ReplyPrefix .. this.ReplyPersonName
    local barNodeTransform = this.barNode.transform
    CommonDefs.GetComponent_Component_Type(barNodeTransform, typeof(TweenPosition)):PlayForward()
end
CPersonalSpaceLeaveMessageView.m_deleteLeaveMessageCache_CS2LuaHook = function (this, leaveMessageId)
    if not CommonDefs.DictContains(CPersonalSpaceMgr.Inst.leaveMessageCacheDic, typeof(UInt64), leaveMessageId) then
        return
    else
        CommonDefs.DictRemove(CPersonalSpaceMgr.Inst.leaveMessageCacheDic, typeof(UInt64), leaveMessageId)
    end

    local newList = CreateFromClass(MakeGenericClass(List, CPersonalSpace_LeaveMessageCacheData))
    CommonDefs.DictIterate(CPersonalSpaceMgr.Inst.leaveMessageCacheDic, DelegateFactory.Action_object_object(function (___key, ___value)
        local kv = {}
        kv.Key = ___key
        kv.Value = ___value
        local info = kv.Value
        info.node.transform.parent = nil
        --info.node.SetActive(false);
        if newList.Count == 0 then
            CommonDefs.ListAdd(newList, typeof(CPersonalSpace_LeaveMessageCacheData), info)
        else
            local insertSign = false
            do
                local i = 0
                while i < newList.Count do
                    if info.data.id > newList[i].data.id then
                        CommonDefs.ListInsert(newList, i, typeof(CPersonalSpace_LeaveMessageCacheData), info)
                        insertSign = true
                        break
                    end
                    i = i + 1
                end
            end

            if not insertSign then
                CommonDefs.ListAdd(newList, typeof(CPersonalSpace_LeaveMessageCacheData), info)
            end
        end
    end))

    CPersonalSpaceMgr.Inst.leaveMessageCacheList = newList
end
CPersonalSpaceLeaveMessageView.m_DeleteLeaveMessageBack_CS2LuaHook = function (this, data, info)
    if not this.listScrollViewIndicator then
        return
    end
    if data.code == 0 then
        this:deleteLeaveMessageCache(info.id)
        this:InitLeaveMessageList(true)
    end
end
CPersonalSpaceLeaveMessageView.m_GenerateLeaveMessageListNodeFunction_CS2LuaHook = function (this, node, info, totalReNew)
    local statusLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("status"), typeof(UILabel))

    if totalReNew then
        if System.String.IsNullOrEmpty(info.rolename) then
            CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = tostring(info.roleid)
        else
            CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = info.rolename
        end

        local newSignNode = node.transform:Find("new").gameObject
        if info.isnew then
            newSignNode:SetActive(true)
        else
            newSignNode:SetActive(false)
        end

        --node.transform.FindChild("lv").GetComponent<UILabel>().text = info.grade.ToString();
        local levelLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("lv"), typeof(UILabel))
        local default
        if info.xianfanstatus > 0 then
            default = L10.Game.Constants.ColorOfFeiSheng
        else
            default = NGUIText.EncodeColor24(levelLabel.color)
        end
        levelLabel.text = System.String.Format(LocalString.GetString("[c][{0}]{1}[-][/c]"), default, info.grade)

        CommonDefs.GetComponent_Component_Type(node.transform:Find("time"), typeof(UILabel)).text = CPersonalSpaceMgr.GetTimeString(math.floor(info.createtime / 1000))

        --if (!string.IsNullOrEmpty(info.clazz) && !string.IsNullOrEmpty(info.gender))
        --{
        --    uint cls = uint.Parse(info.clazz);
        --    uint gender = uint.Parse(info.gender);
        --    node.transform.FindChild("icon").GetComponent<CUITexture>().LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender));
        --}
        CPersonalSpaceMgr.LoadPortraitPic(info.photo, info.clazz, info.gender, node.transform:Find("icon").gameObject, info.expression_base)

        if info.roleid ~= CPersonalSpaceWnd.Instance.SelfRoleInfo.id then
            CPersonalSpaceMgr.Inst:addPlayerLinkBtn(node.transform:Find("icon/button").gameObject, info.roleid, EnumReportId_lua.eMengDaoMsg, info.text, nil, info.id, 0)
        end

        local showText = string.gsub(string.gsub(info.text, "&lt;", "<"), "&gt;", ">")
        showText = CChatMgr.Inst:FilterYangYangEmotion(showText)

        if info.ownerinfo and  info.ownerinfo.roleid ~= nil and info.ownerinfo.roleid > 0 then
            showText = ((LocalString.GetString("在") .. CPersonalSpaceMgr.GeneratePlayerName(info.ownerinfo.roleid, info.ownerinfo.rolename)) .. LocalString.GetString("的梦岛回复你：")) .. showText
        end

        if info.replyinfo and info.replyinfo.roleid ~= nil and info.replyinfo.roleid > 0 then
            showText = ((LocalString.GetString("对") .. CPersonalSpaceMgr.GeneratePlayerName(info.replyinfo.roleid, info.replyinfo.rolename)) .. LocalString.GetString("说：")) .. showText
        end

        statusLabel.text = CChatLinkMgr.TranslateToNGUIText(showText, false)
        UIEventListener.Get(statusLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
            local url = CommonDefs.GetComponent_GameObject_Type(statusLabel.gameObject, typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
            if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
                return
            else
                this:SetInputDefaultValue(info.roleid, info.rolename, node, info)
            end
        end)

        UIEventListener.Get(node.transform:Find("bgbutton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
            this:SetInputDefaultValue(info.roleid, info.rolename, node, info)
        end)

        local bgbuttonSprite = CommonDefs.GetComponent_Component_Type(node.transform:Find("bgbutton"), typeof(UISprite))
        bgbuttonSprite.height = statusLabel.height + 74

        local deleteBtn = node.transform:Find("deletebutton").gameObject
        if info.roleid == CPersonalSpaceWnd.Instance.SelfRoleInfo.id or this.selfPanelSign then
            deleteBtn:SetActive(true)
            UIEventListener.Get(deleteBtn).onClick = DelegateFactory.VoidDelegate(function (p)
                MessageWndManager.ShowConfirmMessage(CPersonalSpaceLeaveMessageView.DeleteLeaveMessageAlertString, 10, false, DelegateFactory.Action(function ()
                    CPersonalSpaceMgr.Inst:DelMessage(info.id, DelegateFactory.Action_CPersonalSpace_BaseRet(function (data)
                        if not this.listScrollViewIndicator then
                            return
                        end
                        this:DeleteLeaveMessageBack(data, info)
                    end), nil)
                end), nil)
            end)
        else
            deleteBtn:SetActive(false)
        end
        
        local translatePos = {300, 394}
        local translateButton = node.transform:Find("TranslateButton").gameObject
        local highlightObj = translateButton.transform:Find("Highlight").gameObject

        if deleteBtn.activeSelf then
            translateButton.transform.localPosition = Vector3(translatePos[1], translateButton.transform.localPosition.y, translateButton.transform.localPosition.z)
        else    
            translateButton.transform.localPosition = Vector3(translatePos[2], translateButton.transform.localPosition.y, translateButton.transform.localPosition.z)
        end
        statusLabel:GetComponent(typeof(CGoogleTranslateLabel)).isWaitTranslate = false
        UIEventListener.Get(translateButton).onClick = DelegateFactory.VoidDelegate(function (p)
            local highlightIsActive = highlightObj.activeSelf
            if highlightIsActive then
                statusLabel.text = CChatLinkMgr.TranslateToNGUIText(showText, false)
                highlightObj:SetActive(false)
                statusLabel:GetComponent(typeof(CGoogleTranslateLabel)).isWaitTranslate = false
            else    
                highlightObj:SetActive(true)
                LuaSEASdkMgr:QueryGoogleTranslate(showText:gsub('#', ' '))
                statusLabel.text = ''
                statusLabel:GetComponent(typeof(CGoogleTranslateLabel)).isWaitTranslate = true
                statusLabel:GetComponent(typeof(CGoogleTranslateLabel)).sourceString = showText:gsub('#', ' ')
            end
        end)
    end

    return node
end
CPersonalSpaceLeaveMessageView.m_updateLeaveMessageCacheList_CS2LuaHook = function (this)
    local newList = CreateFromClass(MakeGenericClass(List, CPersonalSpace_LeaveMessageCacheData))
    CommonDefs.DictIterate(CPersonalSpaceMgr.Inst.leaveMessageCacheDic, DelegateFactory.Action_object_object(function (___key, ___value)
        local kv = {}
        kv.Key = ___key
        kv.Value = ___value
        local info = kv.Value
        if info.node ~= nil then
            info.node.transform.parent = CPersonalSpaceWnd.Instance.poolNode.transform
            info.node.transform.localPosition = Vector3.zero
            if newList.Count == 0 then
                CommonDefs.ListAdd(newList, typeof(CPersonalSpace_LeaveMessageCacheData), info)
            else
                local insertSign = false
                do
                    local i = 0
                    while i < newList.Count do
                        if info.data.id > newList[i].data.id then
                            CommonDefs.ListInsert(newList, i, typeof(CPersonalSpace_LeaveMessageCacheData), info)
                            insertSign = true
                            break
                        end
                        i = i + 1
                    end
                end

                if not insertSign then
                    CommonDefs.ListAdd(newList, typeof(CPersonalSpace_LeaveMessageCacheData), info)
                end
            end
        end
    end))

    CPersonalSpaceMgr.Inst.leaveMessageCacheList = newList
end
CPersonalSpaceLeaveMessageView.m_SetLeaveMessageShowBtn_CS2LuaHook = function (this, btn)
    UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function (p)
        if not this.MessageCommentable then
            g_MessageMgr:ShowMessage("CANNOT_COMMENT")
            return
        end

        CommonDefs.GetComponent_GameObject_Type(this.barNode, typeof(TweenPosition)):PlayForward()
    end)
end
CPersonalSpaceLeaveMessageView.m_AddLeaveMessageBack_CS2LuaHook = function (this, textAfterFilter, data, replyInfo)
    if not this.listScrollViewIndicator then
        return
    end

    if data.code == 0 then
        local newItem = CreateFromClass(CPersonalSpace_GetMessageItem)
        newItem.clazz = CPersonalSpaceWnd.Instance.SelfRoleInfo.clazz
        newItem.createtime = math.floor((CServerTimeMgr.Inst.timeStamp)) * 1000
        newItem.gender = CPersonalSpaceWnd.Instance.SelfRoleInfo.gender
        newItem.grade = CPersonalSpaceWnd.Instance.SelfRoleInfo.grade
        newItem.id = System.UInt64.Parse(data.data.id)
        newItem.text = textAfterFilter
        newItem.roleid = CPersonalSpaceWnd.Instance.SelfRoleInfo.id
        newItem.rolename = CPersonalSpaceWnd.Instance.SelfRoleInfo.name
        newItem.replyinfo = replyInfo
        --newItem.photo =  TODO
        local updateList = Table2ArrayWithCount({newItem}, 1, MakeArrayClass(CPersonalSpace_GetMessageItem))
        this:updateLeaveMessageCache(updateList)
        this:InitLeaveMessageList(false)
    end
end
CPersonalSpaceLeaveMessageView.m_InitSendMessageNode_CS2LuaHook = function (this)
    this:SetLeaveMessageShowBtn(this.selfLeaveMessageBtn)
    this:SetLeaveMessageShowBtn(this.otherLeaveMessageBtn)

    local backBtn = this.barNode.transform:Find("SendStatus/BackButton").gameObject
    UIEventListener.Get(backBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        CommonDefs.GetComponent_GameObject_Type(this.barNode, typeof(TweenPosition)):PlayReverse()
    end)

    CommonDefs.GetComponent_GameObject_Type(this.leaveMessagePanel_submitBtn, typeof(CButton)).enabled = true
    UIEventListener.Get(this.leaveMessagePanel_submitBtn).onClick = MakeDelegateFromCSFunction(this.OnSubmitBtnClick, VoidDelegate, this)

    this.leaveMessagePanel_statusInput.onReturnKeyPressed = MakeDelegateFromCSFunction(this.OnSubmitBtnClick, MakeGenericClass(Action1, GameObject), this)
end
CPersonalSpaceLeaveMessageView.m_setSendFlowerBtn_CS2LuaHook = function (this, itemId, go, qnAddButton, targetId, targetName)
    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (p)
        local sendNum = qnAddButton:GetValue()
        local itemHaveNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemId)
        if sendNum <= itemHaveNum then
            if sendNum <= 0 then
                g_MessageMgr:ShowMessage("ENTER_NUMBER_TIP")
                return
            end
            CPersonalSpaceMgr.Inst:SendFlower(targetId, targetName, itemId, sendNum, 0)
        else
            CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, false, go.transform, AlignType.Right)
        end
    end)
end
CPersonalSpaceLeaveMessageView.m_setGoToBuyBtn_CS2LuaHook = function (this, itemID, go)
    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (p)
        CItemAccessListMgr.Inst:ShowItemAccessInfo(itemID, true, go.transform, AlignType.Right)
    end)
end
CPersonalSpaceLeaveMessageView.m_OnSendItem_CS2LuaHook = function (this, itemId)
    if CClientMainPlayer.Inst == nil then
        return
    end

    local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(EnumItemPlace.Bag, itemId)

    if pos > 0 then
        local commonItem = CItemMgr.Inst:GetById(itemId)
        if commonItem ~= nil and commonItem.TemplateId == this.SendFlowerNowShowTargetId then
            local itemHaveNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, commonItem.TemplateId)
            local ItemNum = CommonDefs.GetComponent_Component_Type(this.sendFlowerPanel.transform:Find("ItemShow/num"), typeof(UILabel))
            ItemNum.text = tostring(itemHaveNum)

            if this.SendFlowerNowShowPlayerId > 0 then
                this:InitSendFlowerPanel(this.SendFlowerNowShowPlayerId, this.SendFlowerNowShowPlayerName, this.SendFlowerNowShowIndex, this.SendFlowerNowShowInputValue)
            end
        end
    end
end
CPersonalSpaceLeaveMessageView.m_OnSetItemAt_CS2LuaHook = function (this, place, pos, oldId, newId)
    local commonItem = CItemMgr.Inst:GetById(newId)
    if commonItem ~= nil and commonItem.TemplateId == this.SendFlowerNowShowTargetId then
        local itemHaveNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, commonItem.TemplateId)
        local ItemNum = CommonDefs.GetComponent_Component_Type(this.sendFlowerPanel.transform:Find("ItemShow/num"), typeof(UILabel))
        ItemNum.text = tostring(itemHaveNum)
    else
        local ItemNum = CommonDefs.GetComponent_Component_Type(this.sendFlowerPanel.transform:Find("ItemShow/num"), typeof(UILabel))
        ItemNum.text = "0"
    end

    if this.SendFlowerNowShowPlayerId > 0 then
        this:InitSendFlowerPanel(this.SendFlowerNowShowPlayerId, this.SendFlowerNowShowPlayerName, this.SendFlowerNowShowIndex, this.SendFlowerNowShowInputValue)
    end
end
CPersonalSpaceLeaveMessageView.m_InitSendFlowerPanel_CS2LuaHook = function (this, targetId, targetName, chooseIndex, inputValue)
    this.sendFlowerPanel:SetActive(true)
    this.sendFlowerTemplate:SetActive(false)
    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    this.SendFlowerNowShowPlayerId = targetId
    this.SendFlowerNowShowPlayerName = targetName

    UIEventListener.Get(this.sendFlowerPanel.transform:Find("CloseButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        this.sendFlowerPanel:SetActive(false)
        EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
        EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    end)

    local ItemLabel = CommonDefs.GetComponent_Component_Type(this.sendFlowerPanel.transform:Find("ItemShow/name"), typeof(UILabel))
    local ItemNum = CommonDefs.GetComponent_Component_Type(this.sendFlowerPanel.transform:Find("ItemShow/num"), typeof(UILabel))
    local ItemIcon = CommonDefs.GetComponent_Component_Type(this.sendFlowerPanel.transform:Find("ItemShow/icon"), typeof(CUITexture))

    local scrollview = CommonDefs.GetComponent_Component_Type(this.sendFlowerPanel.transform:Find("ScrollView"), typeof(UIScrollView))
    local table = CommonDefs.GetComponent_Component_Type(this.sendFlowerPanel.transform:Find("ScrollView/Table"), typeof(UITable))

    local getMoreBtn = this.sendFlowerPanel.transform:Find("GetMoreButton").gameObject
    local submitBtn = this.sendFlowerPanel.transform:Find("SubmitButton").gameObject
    this:cleanBtn(getMoreBtn)
    this:cleanBtn(submitBtn)

    local qnAddButton = CommonDefs.GetComponent_Component_Type(this.sendFlowerPanel.transform:Find("BuyArea/QnIncreseAndDecreaseButton"), typeof(QnAddSubAndInputButton))
    qnAddButton:SetValue(inputValue, true)
    this:cleanBtn(ItemIcon.gameObject)

    Extensions.RemoveAllChildren(table.transform)
    local flowerArray = GameplayItem_Setting.GetData().PersonalSpace_FlowerItemIds
    local showNode = CreateFromClass(MakeGenericClass(List, GameObject))

    do
        local i = 0
        while i < flowerArray.Length do
            local itemId = flowerArray[i][0]
            local nowIndex = i
            local data = Item_Item.GetData(itemId)
            if data ~= nil then
                local node = NGUITools.AddChild(table.gameObject, this.sendFlowerTemplate)
                node:SetActive(true)
                CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = data.Name
                CommonDefs.GetComponent_Component_Type(node.transform:Find("icon"), typeof(CUITexture)):LoadMaterial(data.Icon)
                local itemHaveNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemId)
                local numTextColor = Color.white
                if itemHaveNum <= 0 then
                    numTextColor = Color.red
                end
                --UILabel
                local numLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("num"), typeof(UILabel))
                numLabel.text = tostring(itemHaveNum)
                numLabel.color = numTextColor

                local showBg = node.transform:Find("bg_light").gameObject
                CommonDefs.ListAdd(showNode, typeof(GameObject), showBg)
                UIEventListener.Get(node).onClick = DelegateFactory.VoidDelegate(function (p)
                    this.SendFlowerNowShowIndex = nowIndex
                    this.SendFlowerNowShowTargetId = itemId
                    ItemIcon:LoadMaterial(data.Icon)
                    ItemLabel.text = data.Name
                    local numTextColor1 = Color.white
                    if itemHaveNum <= 0 then
                        numTextColor1 = Color.red
                    end
                    ItemNum.color = numTextColor1

                    ItemNum.text = tostring(itemHaveNum)
                    this:setGoToBuyBtn(itemId, getMoreBtn)
                    this:setSendFlowerBtn(itemId, submitBtn, qnAddButton, targetId, targetName)

                    UIEventListener.Get(ItemIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (s)
                        CItemInfoMgr.ShowLinkItemTemplateInfo(data.ID, false, nil, AlignType1.Default, 0, 0, 0, 0)
                    end)

                    CommonDefs.ListIterate(showNode, DelegateFactory.Action_object(function (___value)
                        local g = ___value
                        g:SetActive(false)
                    end))

                    showBg:SetActive(true)

                    qnAddButton.onValueChanged = DelegateFactory.Action_uint(function (value)
                        this.SendFlowerNowShowInputValue = value
                        if value > itemHaveNum then
                            ItemNum.color = Color.red
                        else
                            ItemNum.color = Color.white
                        end
                    end)
                    qnAddButton:SetValue(inputValue, true)
                end)

                UIEventListener.Get(node.transform:Find("icon").gameObject).onClick = DelegateFactory.VoidDelegate(function (s)
                    CItemInfoMgr.ShowLinkItemTemplateInfo(data.ID, false, nil, AlignType1.Default, 0, 0, 0, 0)
                end)

                if i == chooseIndex then
                    node:SendMessage("OnClick")
                end
            end
            i = i + 1
        end
    end
    table:Reposition()
    scrollview:ResetPosition()
end
