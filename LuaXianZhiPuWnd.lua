require("common/common_include")

local TweenWidth = import "TweenWidth"
local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CUITexture = import "L10.UI.CUITexture"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"
local Vector3 = import "UnityEngine.Vector3"

LuaXianZhiPuWnd = class()

RegistChildComponent(LuaXianZhiPuWnd, "Content", GameObject)
RegistChildComponent(LuaXianZhiPuWnd, "BGTween", TweenWidth)

RegistChildComponent(LuaXianZhiPuWnd, "ShangXianTableView", QnTableView)
RegistChildComponent(LuaXianZhiPuWnd, "ZhongXianTableView", QnTableView)
RegistChildComponent(LuaXianZhiPuWnd, "XiaoXianTableView", QnTableView)

RegistChildComponent(LuaXianZhiPuWnd, "SanJieIsEmpty", GameObject)
RegistChildComponent(LuaXianZhiPuWnd, "CityIsEmpty", GameObject)
RegistChildComponent(LuaXianZhiPuWnd, "DistrictIsEmpty", GameObject)

RegistClassMember(LuaXianZhiPuWnd, "ShangXianDataSource")
RegistClassMember(LuaXianZhiPuWnd, "ZhongXianDataSource")
RegistClassMember(LuaXianZhiPuWnd, "XiaoXianDataSource")

function LuaXianZhiPuWnd:Init()
	self.Content:SetActive(false)

	CommonDefs.AddEventDelegate(self.BGTween.onFinished, DelegateFactory.Action(function ()
		self:ShowContent()
	end))

	-- 三界
	local initPlayerItem_SanJie = function (item, index)
		self:InitPlayerTemplate(item, index, 1)
	end

	self.ShangXianDataSource = DefaultTableViewDataSource.Create(
		function ()
			return #CLuaXianzhiMgr.m_SanJiePlayerInfos
		end, initPlayerItem_SanJie)
	self.ShangXianTableView.m_DataSource = self.ShangXianDataSource
	self.ShangXianTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
		local info = CLuaXianzhiMgr.m_SanJiePlayerInfos[row+1]
		if CClientMainPlayer.Inst:GetMyServerId() ~= info.serverGroupId then
			g_MessageMgr:ShowMessage("TEAM_GROUP_INVITEE_NOT_IN_SAME_SERVER")
			return
		end
        self:OnSelectAtRow(row, info)
    end)

	-- 城界
	local initPlayerItem_City = function (item, index)
		self:InitPlayerTemplate(item, index, 2)
	end

	self.ZhongXianDataSource = DefaultTableViewDataSource.Create(
		function ()
			return #CLuaXianzhiMgr.m_CityPlayerInfos
	end, initPlayerItem_City)
	self.ZhongXianTableView.m_DataSource = self.ZhongXianDataSource
	self.ZhongXianTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
		local info = CLuaXianzhiMgr.m_CityPlayerInfos[row+1]
        self:OnSelectAtRow(row, info)
    end)

	-- 区界
	local initPlayerItem_District = function (item, index)
		self:InitPlayerTemplate(item, index, 3)
	end

	self.XiaoXianDataSource = DefaultTableViewDataSource.Create(
		function ()
			return #CLuaXianzhiMgr.m_DistrictPlayerInfos
	end, initPlayerItem_District)
	self.XiaoXianTableView.m_DataSource = self.XiaoXianDataSource
	self.XiaoXianTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
		local info = CLuaXianzhiMgr.m_DistrictPlayerInfos[row+1]
        self:OnSelectAtRow(row, info)
    end)

end

function LuaXianZhiPuWnd:ShowContent()
	self.Content:SetActive(true)
	self:UpdatePlayerInfos()
end

function LuaXianZhiPuWnd:UpdatePlayerInfos()
	self.SanJieIsEmpty:SetActive(#CLuaXianzhiMgr.m_SanJiePlayerInfos == 0)
	self.CityIsEmpty:SetActive(#CLuaXianzhiMgr.m_CityPlayerInfos == 0)
	self.DistrictIsEmpty:SetActive(#CLuaXianzhiMgr.m_DistrictPlayerInfos == 0)

	self.ShangXianTableView:ReloadData(true, false)
	self.ZhongXianTableView:ReloadData(true, false)
	self.XiaoXianTableView:ReloadData(true, false)
	--self:ScrollToMyself()
end

function LuaXianZhiPuWnd:InitPlayerTemplate(item, index, xianZhiType)

	local info = nil
	if xianZhiType == 1 then
		info = CLuaXianzhiMgr.m_SanJiePlayerInfos[index+1]
	elseif xianZhiType == 2 then
		info = CLuaXianzhiMgr.m_CityPlayerInfos[index+1]
	elseif xianZhiType == 3 then
		info = CLuaXianzhiMgr.m_DistrictPlayerInfos[index+1]
	end
	if not info then return end

	local BG = item.transform:Find("BG").gameObject
	local Portrait = item.transform:Find("NotEmpty/Player/Portrait"):GetComponent(typeof(CUITexture))
	local Border = item.transform:Find("NotEmpty/Player/Portrait/Border"):GetComponent(typeof(UISprite))
	local PlayerNameLabel = item.transform:Find("NotEmpty/Player/PlayerNameLabel"):GetComponent(typeof(UILabel))

	local XianZhiNamePosLabel = item.transform:Find("NotEmpty/XianZhi/XianZhiNamePosLabel"):GetComponent(typeof(UILabel))
	local XianZhiNameLabel = item.transform:Find("NotEmpty/XianZhi/XianZhiNameLabel"):GetComponent(typeof(UILabel))

	local LevelLabel = item.transform:Find("NotEmpty/Operations/XianZhiSkill/LevelLabel"):GetComponent(typeof(UILabel))
	local XianZhiIcon = item.transform:Find("NotEmpty/Operations/XianZhiSkill/Mask/XianZhiIcon"):GetComponent(typeof(CUITexture))

	local SkillStatus = item.transform:Find("NotEmpty/Operations/SkillStatus").gameObject
	local Minus = item.transform:Find("NotEmpty/Operations/SkillStatus/Minus").gameObject
	local Add = item.transform:Find("NotEmpty/Operations/SkillStatus/Add").gameObject
	local Sealed = item.transform:Find("NotEmpty/Operations/SkillStatus/Sealed").gameObject
	

	local title = XianZhi_Title.GetData(info.titleId)
	local xianZhiTypeId = title.TitleType
	local xianZhiType = XianZhi_TitleType.GetData(xianZhiTypeId)

	PlayerNameLabel.text = info.playerName
	Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(info.playerCls, info.playerGender, -1))

	XianZhiNamePosLabel.text = CLuaXianzhiMgr.GetXianZhiPositionByRegion(info.regionId)
	XianZhiNameLabel.text = title.TitleName

	LevelLabel.text = tostring(info.skillLevel)
	XianZhiIcon:LoadSkillIcon(StringTrim(xianZhiType.Icon))

	SkillStatus:SetActive(true)
	if info.operateType == EnumXianZhiOperateType.eNone then
		SkillStatus:SetActive(false)
	else
		Add:SetActive(info.operateType == EnumXianZhiOperateType.ePromote)
		Minus:SetActive(info.operateType == EnumXianZhiOperateType.eWeaken)
		Sealed:SetActive(info.operateType == EnumXianZhiOperateType.eForbid)
	end

	-- 是否是自身
	local isSelf = CClientMainPlayer.Inst.Id == info.playerId
	BG:SetActive(isSelf)

	if isSelf then
		Portrait.transform:GetComponent(typeof(UITexture)).width = 130
		Portrait.transform:GetComponent(typeof(UITexture)).height = 130
		Border.width = 145
		Border.height = 145
	else
		Portrait.transform:GetComponent(typeof(UITexture)).width = 110
		Portrait.transform:GetComponent(typeof(UITexture)).height = 110
		Border.width = 120
		Border.height = 120
	end
end

function LuaXianZhiPuWnd:OnSelectAtRow(row, info)
	CPlayerInfoMgr.ShowPlayerPopupMenu(info.playerId, EnumPlayerInfoContext.XianZhi, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
end


function LuaXianZhiPuWnd:ScrollToMyself()
	local status = CLuaXianzhiMgr.GetXianZhiStatus()
	if status == EnumXianZhiStatus.CityRegion then
		local myselfIndex = 0
		for i = 1, #CLuaXianzhiMgr.m_CityPlayerInfos do
			if CLuaXianzhiMgr.m_CityPlayerInfos[i].playerId == CClientMainPlayer.Inst.Id then
				myselfIndex = i
			end
		end
		if myselfIndex > 0 then
			self.ZhongXianTableView:ScrollToRow(myselfIndex-1)
		end
		
	elseif status == EnumXianZhiStatus.DistrictRegion then
		local myselfIndex = 0
		for i = 1, #CLuaXianzhiMgr.m_DistrictPlayerInfos do
			if CLuaXianzhiMgr.m_DistrictPlayerInfos[i].playerId == CClientMainPlayer.Inst.Id then
				myselfIndex = i
			end
		end
		if myselfIndex > 0 then
			self.XiaoXianTableView:ScrollToRow(myselfIndex-1)
		end
	end

end

function LuaXianZhiPuWnd:UpdateXianZhiPu()
	self:UpdatePlayerInfos()
end

function LuaXianZhiPuWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateXianZhiPu", self, "UpdateXianZhiPu")
end

function LuaXianZhiPuWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateXianZhiPu", self, "UpdateXianZhiPu")
end

return LuaXianZhiPuWnd
