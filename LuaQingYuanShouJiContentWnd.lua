require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local UIGrid = import "UIGrid"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CTaskMgr = import "L10.Game.CTaskMgr"
local NGUIText = import "NGUIText"
local CChatLinkMgr = import "CChatLinkMgr"
local UICamera = import "UICamera"

CLuaQingYuanShouJiContentWnd = class()
RegistClassMember(CLuaQingYuanShouJiContentWnd,"m_CloseBtn")
RegistClassMember(CLuaQingYuanShouJiContentWnd,"m_PartnerLabel")
RegistClassMember(CLuaQingYuanShouJiContentWnd,"m_Grid")
RegistClassMember(CLuaQingYuanShouJiContentWnd,"m_Template")

function CLuaQingYuanShouJiContentWnd:Awake()
    
end

function CLuaQingYuanShouJiContentWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function CLuaQingYuanShouJiContentWnd:Init()
	self.m_CloseBtn = self.transform:Find("CloseButton").gameObject
	CommonDefs.AddOnClickListener(self.m_CloseBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

	self.m_PartnerLabel = self.transform:Find("Partner/NameLabel"):GetComponent(typeof(UILabel))
	self.m_Grid = self.transform:Find("Grid"):GetComponent(typeof(UIGrid))
	self.m_Template = self.transform:Find("ColumnTemplate").gameObject
	self.m_Template:SetActive(false)
	self.m_PartnerLabel.text = LuaValentine2019Mgr.GetWrapText(LuaValentine2019Mgr.ShouJiPartnerName or "")


	--找到第一个未完成的任务
	local targetTaskIdx = 0
	for i,task in pairs(LuaValentine2019Mgr.ShouJiTaskInfo) do
		if task.taskStatus == EnumQYSJTaskStatus.UNACCEPT or task.taskStatus == EnumQYSJTaskStatus.ACCEPTED then
			targetTaskIdx = i
			break
		end
	end

	Extensions.RemoveAllChildren(self.m_Grid.transform)
	for i=1,3 do
		local instance = CUICommonDef.AddChild(self.m_Grid.gameObject, self.m_Template)
		instance:SetActive(true)
		self:InitColumn(i, instance, targetTaskIdx)
	end
	self.m_Grid:Reposition()

end

function CLuaQingYuanShouJiContentWnd:InitColumn(index, go, targetTaskIdx)
	local button = go.transform:Find("Button"):GetComponent(typeof(CButton))
	local lockFlag = go.transform:Find("Lock").gameObject
	local table = go.transform:Find("Table").gameObject
	local taskIdx = (index-1)*3 + 1

	if taskIdx>=1 and taskIdx+2<=#LuaValentine2019Mgr.ShouJiTaskInfo then

		local firstTask = LuaValentine2019Mgr.ShouJiTaskInfo[taskIdx]
		if CServerTimeMgr.Inst.timeStamp<firstTask.taskUnlockTime then
			--未解锁
			lockFlag:SetActive(true)

			local dt = CServerTimeMgr.ConvertTimeStampToZone8Time(firstTask.taskUnlockTime)
			local timeInfo =  ToStringWrap(dt, LocalString.GetString("MM月dd日"))
			local lockLabel = lockFlag.transform:Find("Label"):GetComponent(typeof(UILabel))
			lockLabel.text = g_MessageMgr:FormatMessage("QYSJ_Unlocked_Notice", timeInfo)

			table:SetActive(false)
			button.gameObject:SetActive(false)
		else
			--已解锁
			lockFlag:SetActive(false)
			table:SetActive(true)

			--初始化任务和按钮
			if taskIdx<=targetTaskIdx and taskIdx+2>=targetTaskIdx then
				button.gameObject:SetActive(false)
				local task = LuaValentine2019Mgr.ShouJiTaskInfo[targetTaskIdx]
				if task.taskStatus == EnumQYSJTaskStatus.UNACCEPT then
					button.Text = LocalString.GetString("领取")
					CommonDefs.AddOnClickListener(button.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnReceiveTaskClick(task.taskId) end), false)
				else
					button.Text = LocalString.GetString("前往")
					CommonDefs.AddOnClickListener(button.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnTrackToDoTaskClick(task.taskId) end), false)
				end

			else
				button.gameObject:SetActive(false)
			end
			button.gameObject:SetActive(taskIdx<=targetTaskIdx and taskIdx+2>=targetTaskIdx)

			for i=0,2 do
				local data =  LuaValentine2019Mgr.ShouJiTaskInfo[taskIdx+i]
				self:InitTaskItem(table.transform:GetChild(i).gameObject, data, taskIdx+i == targetTaskIdx)
			end
		end
	end
end

function CLuaQingYuanShouJiContentWnd:InitTaskItem(go, task, isTargetTask)
	local nameLabel = go.transform:Find("TaskNameLabel"):GetComponent(typeof(UILabel))
	local descLabel = go.transform:Find("TaskDescLabel"):GetComponent(typeof(UILabel))
	local conditionGo = go.transform:Find("TaskConditionLabel").gameObject
	local finishFlag = go.transform:Find("Finish").gameObject
	local selectedGo = go.transform:Find("Selected").gameObject

	local task_task = Task_Task.GetData(task.taskId)
	nameLabel.text = task_task.Display
	local partnerLink = CChatLinkMgr.TranslateToNGUIText(SafeStringFormat3("<link player=%s,%s>", LuaValentine2019Mgr.ShouJiPartnerId, LuaValentine2019Mgr.ShouJiPartnerName))
	descLabel.text = SafeStringFormat3("[c][%s]%s[-][/c]", NGUIText.EncodeColor24(descLabel.color), SafeStringFormat3(Valentine2019_QYSJTask.GetData(task.taskId).Doc, partnerLink))
	local conditionFit = (isTargetTask or task.taskStatus == EnumQYSJTaskStatus.ACCEPTED or task.taskStatus == EnumQYSJTaskStatus.FINISHED)
	nameLabel.gameObject:SetActive(conditionFit)
	descLabel.gameObject:SetActive(conditionFit)
	conditionGo:SetActive(not conditionFit)
	finishFlag:SetActive(task.taskStatus == EnumQYSJTaskStatus.FINISHED)
	selectedGo:SetActive(isTargetTask)
	CommonDefs.AddOnClickListener(descLabel.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnDescLabelClick(descLabel) end), false)
end

function CLuaQingYuanShouJiContentWnd:OnDescLabelClick(label)
	local url = label:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url ~= nil then
        CChatLinkMgr.ProcessLinkClick(url, nil)
    end
end

function CLuaQingYuanShouJiContentWnd:OnReceiveTaskClick(taskId)
	LuaValentine2019Mgr.RequestAcceptValentineQYSJTask()
	self:Close()
end

function CLuaQingYuanShouJiContentWnd:OnTrackToDoTaskClick(taskId)
	CTaskMgr.Inst:TrackToDoTask(taskId)
	self:Close()
end
