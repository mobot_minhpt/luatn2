require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local CTowerDefenseMgr=import "L10.Game.CTowerDefenseMgr"
CLuaBottomRightActionBoard=class()
RegistClassMember(CLuaBottomRightActionBoard,"m_Button1")


function CLuaBottomRightActionBoard:Awake()
    
end


function CLuaBottomRightActionBoard:Init()
    self.m_Button1=LuaGameObject.GetChildNoGC(self.transform,"Button1").gameObject
    if CTowerDefenseMgr.Inst:IsInTowerDefenseScene() then
		if CTowerDefenseMgr.Inst.type == 1 then
			LuaGameObject.GetChildNoGC(self.m_Button1.transform,"Label").label.text=LocalString.GetString("布置守卫")
			UIEventListener.Get(self.m_Button1).onClick = DelegateFactory.VoidDelegate(function(go)
				CTowerDefenseMgr.Inst:EnterSummonTowerMode()
			end)
		elseif CTowerDefenseMgr.Inst.type == 2 then
			LuaGameObject.GetChildNoGC(self.m_Button1.transform,"Label").label.text=LocalString.GetString("布置家具")
			UIEventListener.Get(self.m_Button1).onClick = DelegateFactory.VoidDelegate(function(go)
				if CTowerDefenseMgr.Inst.enablePlaceFurniture then 
					CTowerDefenseMgr.Inst:ShowPlaceFurnitureEditWnd();
				end
			end)
		end
    end

end


function CLuaBottomRightActionBoard:GetGuideGo(methodName)
    if methodName=="GetButton1" then
        return self.m_Button1
    end
    return nil
end
return CLuaBottomRightActionBoard