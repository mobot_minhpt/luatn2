local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local UIGrid = import "UIGrid"
local UITable = import "UITable"
local Screen = import "UnityEngine.Screen"
local UIRoot = import "UIRoot"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"


LuaGuildTerritorialWarsMapTipWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildTerritorialWarsMapTipWnd, "Background", "Background", UISprite)
RegistChildComponent(LuaGuildTerritorialWarsMapTipWnd, "ButtonsBg", "ButtonsBg", UISprite)
RegistChildComponent(LuaGuildTerritorialWarsMapTipWnd, "RightTable", "RightTable", UITable)
RegistChildComponent(LuaGuildTerritorialWarsMapTipWnd, "ButtonTemplate", "ButtonTemplate", UISprite)
RegistChildComponent(LuaGuildTerritorialWarsMapTipWnd, "Grid", "Grid", UIGrid)

--@endregion RegistChildComponent end
RegistClassMember(LuaGuildTerritorialWarsMapTipWnd, "m_BackgroundInitialHeight")
RegistClassMember(LuaGuildTerritorialWarsMapTipWnd, "m_ButtonsBgInitialHeight")
RegistClassMember(LuaGuildTerritorialWarsMapTipWnd, "m_BtnName2ActionName")
RegistClassMember(LuaGuildTerritorialWarsMapTipWnd, "m_TeleportBtn")

function LuaGuildTerritorialWarsMapTipWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	self.ButtonTemplate.gameObject:SetActive(false)
    --@endregion EventBind end
end

function LuaGuildTerritorialWarsMapTipWnd:OnEnable()
end

function LuaGuildTerritorialWarsMapTipWnd:OnDisable()
end

function LuaGuildTerritorialWarsMapTipWnd:Init()
	self.m_BackgroundInitialHeight = self.Background.height
	self.m_ButtonsBgInitialHeight = self.ButtonsBg.height

	self:InitRightView()

	local labelCount = self.Grid.transform.childCount
	self.Grid:Reposition()
	self.Background.height = self.m_BackgroundInitialHeight + self.Grid.cellHeight * (labelCount - 1)

	local btnCount = self.RightTable.transform.childCount
	self.RightTable:Reposition()
	self.ButtonsBg.height = self.m_ButtonsBgInitialHeight + (self.ButtonTemplate.height + self.RightTable.padding.y) * (btnCount - 1)

	self:InitPos()
end

function LuaGuildTerritorialWarsMapTipWnd:InitRightView()
	local t = LuaGuildTerritorialWarsMgr.m_GridTipData
	self.m_BtnName2ActionName = {}
	self.m_BtnName2ActionName[LocalString.GetString("传送")] = "Teleport"
	self.m_BtnName2ActionName[LocalString.GetString("标记")] = "SetFavoriteGrid"
	self.m_BtnName2ActionName[LocalString.GetString("取消标记")] = "SetFavoriteGrid"
	self.m_BtnName2ActionName[LocalString.GetString("申请免战")] = "AvoidWar"
	self.m_BtnName2ActionName[LocalString.GetString("摆脱俘虏")] = "Liberate"
	self.m_BtnName2ActionName[LocalString.GetString("迁城")] = "MigrateCity"
	self.m_BtnName2ActionName[LocalString.GetString("放弃")] = "GiveupGrid"
	self.m_BtnName2ActionName[LocalString.GetString("结盟")] = "MakeAlliance"
	self.m_BtnName2ActionName[LocalString.GetString("取消同盟")] = "BreakAlliance"

	local id = t.id
	local guildId = LuaGuildTerritorialWarsMgr.m_GuildId
	local gridInfo = LuaGuildTerritorialWarsMgr.m_GridInfo[id]
	local guildInfo = LuaGuildTerritorialWarsMgr.m_GuildInfo[guildId]
	local isMyArea = LuaGuildTerritorialWarsMgr:IsMyArea(id)
	local isFriendArea = LuaGuildTerritorialWarsMgr:IsFriendArea(id)
	local isEnemyArea = LuaGuildTerritorialWarsMgr:IsEnemyArea(id)
	local isCity = gridInfo.isCity
	local isMyCity = isMyArea and isCity
	local isFriendCity = isFriendArea and isCity
	local isEnemyCity = isEnemyArea and isCity
	local isStar = LuaGuildTerritorialWarsMgr.m_FavGridIdInfo[id]
	self:AddBtn(LocalString.GetString("传送"))
	if not LuaGuildTerritorialWarsMgr:IsInGuide() then
		self:AddBtn(isStar and LocalString.GetString("取消标记") or LocalString.GetString("标记"))
		if isMyCity then
			self:AddBtn(LocalString.GetString("申请免战"))
			if guildInfo.masterGuildId > 0 then
				self:AddBtn(LocalString.GetString("摆脱俘虏"))
			end
		elseif isMyArea then
			self:AddBtn(LocalString.GetString("迁城"))
			self:AddBtn(LocalString.GetString("放弃"))
		end
	end
end

function LuaGuildTerritorialWarsMapTipWnd:Teleport()
	local t = LuaGuildTerritorialWarsMgr.m_GridTipData
	if LuaGuildTerritorialWarsMgr:IsInGuide() then
		Gac2Gas.RequestEnterGTWGuidePlay()
		return 
	end
	g_ScriptEvent:BroadcastInLua("GuildTerritorialWarsMap_TryToTeleport",t.id)
end

function LuaGuildTerritorialWarsMapTipWnd:AvoidWar()
	Gac2Gas.RequsetTerritoryWarAvoidWar()
end

function LuaGuildTerritorialWarsMapTipWnd:SetFavoriteGrid()
	local t = LuaGuildTerritorialWarsMgr.m_GridTipData
	Gac2Gas.RequestTerritoryWarSetFavoriteGrid(t.id, not LuaGuildTerritorialWarsMgr.m_FavGridIdInfo[t.id])
end

function LuaGuildTerritorialWarsMapTipWnd:GiveupGrid()
	local t = LuaGuildTerritorialWarsMgr.m_GridTipData
	if t.level <= 2 then
		Gac2Gas.RequestTerritoryWarGiveupGrid(t.id)
		return
	end
	local msg = g_MessageMgr:FormatMessage("RequestTerritoryWarGiveupGrid_Confirm",t.x,t.y)
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		Gac2Gas.RequestTerritoryWarGiveupGrid(t.id)
	end),nil,LocalString.GetString("确认"),LocalString.GetString("取消"),false)
end

function LuaGuildTerritorialWarsMapTipWnd:MigrateCity()
	local t = LuaGuildTerritorialWarsMgr.m_GridTipData
	local msg = g_MessageMgr:FormatMessage("RequestTerritoryWarMigrateCity_Confirm")
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		Gac2Gas.RequestTerritoryWarMigrateCity(t.id)
	end),nil,LocalString.GetString("确认"),LocalString.GetString("取消"),false)
end

function LuaGuildTerritorialWarsMapTipWnd:MakeAlliance()
	local t = LuaGuildTerritorialWarsMgr.m_GridTipData
	local gridInfo = LuaGuildTerritorialWarsMgr.m_GridInfo[t.id]
	Gac2Gas.RequestTerritoryWarMakeAlliance(gridInfo.belongGuildId)
end

function LuaGuildTerritorialWarsMapTipWnd:BreakAlliance()
	local t = LuaGuildTerritorialWarsMgr.m_GridTipData
	local gridInfo = LuaGuildTerritorialWarsMgr.m_GridInfo[t.id]
	local guildInfo = LuaGuildTerritorialWarsMgr.m_GuildInfo[gridInfo.belongGuildId]
	local msg = g_MessageMgr:FormatMessage("RequestTerritoryWarBreakAlliance_Confirm",guildInfo.guildName)
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		Gac2Gas.RequestTerritoryWarBreakAlliance(gridInfo.belongGuildId)
	end),nil,LocalString.GetString("确认"),LocalString.GetString("取消"),false)
end

function LuaGuildTerritorialWarsMapTipWnd:Liberate()
	Gac2Gas.RequestTerritoryWarLiberate()
end

function LuaGuildTerritorialWarsMapTipWnd:AddBtn(btnName)
	local obj = NGUITools.AddChild(self.RightTable.gameObject,self.ButtonTemplate.gameObject)
	obj.gameObject:SetActive(true)
	obj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = btnName
	if btnName == LocalString.GetString("传送") then
		self.m_TeleportBtn = obj
	end
	UIEventListener.Get(obj.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		local actionName = self.m_BtnName2ActionName[btnName]
	    if self[actionName] then
			self[actionName]()
			if self.m_TeleportBtn == obj and CGuideMgr.Inst:IsInPhase(EnumGuideKey.GuildTerritorialWarPeripheryPlay) and CGuideMgr.Inst:IsInSubPhase(2)  then
				LuaGuildTerritorialWarsMgr.EnableGuide_GuildTerritorialWarPeripheryPlay2 = true
			end
		end
		CUIManager.CloseUI(CLuaUIResources.GuildTerritorialWarsMapTipWnd)
	end)
end

function LuaGuildTerritorialWarsMapTipWnd:InitPos()
	local halfBgWidth = self.Background.width * 0.5
	local halfBgHeight = self.Background.height * 0.5
	local btnBgWidth = self.ButtonsBg.width

	local localPos = self.Background.transform.parent:InverseTransformPoint(LuaGuildTerritorialWarsMgr.m_GridTipPos)
	localPos = Vector3(localPos.x + halfBgWidth, localPos.y, 0)

	local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
	local virtualScreenWidth = Screen.width * scale * CUIManager.UIMainCamera.rect.width
	local virtualScreenHeight = Screen.height * scale
	local halfVirtualScreenWidth = virtualScreenWidth * 0.5
	local halfVirtualScreenHeight = virtualScreenHeight * 0.5

	local centerX = localPos.x
	local centerY = localPos.y - halfBgHeight
	if centerX - halfBgWidth < -halfVirtualScreenWidth then
		centerX = -halfVirtualScreenWidth + halfBgWidth
	end
	if centerX + halfBgWidth + btnBgWidth > halfVirtualScreenWidth then
		centerX = halfVirtualScreenWidth - halfBgWidth - btnBgWidth
	end
	local maxHeight = math.max(self.Background.height, self.ButtonsBg.height)
	local halfMaxHeight = maxHeight * 0.5
	if centerY - halfMaxHeight < -halfVirtualScreenHeight then
		centerY = -halfVirtualScreenHeight + halfMaxHeight
	end

	self.Background.transform.localPosition = Vector3(centerX, centerY + halfBgHeight, 0);
end

--@region UIEvent

--@endregion UIEvent
function LuaGuildTerritorialWarsMapTipWnd:GetGuideGo(methodName)
	if LuaGuildTerritorialWarsMgr:IsInPlay() then
		if methodName == "GetShareButton" then
			return self.transform:Find("Background/TitleLabel/ShareButton").gameObject
		end
	elseif LuaGuildTerritorialWarsMgr:IsInGuide() or LuaGuildTerritorialWarsMgr:IsPeripheryPlay() then
		if methodName == "GetTeleportBtn" then
			return self.m_TeleportBtn
		end
	end
	return nil
end