-- Auto Generated!!
local CGuildApplyMessageWnd = import "L10.UI.CGuildApplyMessageWnd"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local QnButton = import "L10.UI.QnButton"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CSpeechCtrlMgr = import "L10.Game.CSpeechCtrlMgr"
local CSpeechCtrlType = import "L10.Game.CSpeechCtrlType"
CGuildApplyMessageWnd.m_Init_CS2LuaHook = function (this) 
    this.labels[0].text = g_MessageMgr:FormatMessage("Application_For_Society_One")
    this.labels[1].text = g_MessageMgr:FormatMessage("Application_For_Society_Two")
    this.labels[2].text = g_MessageMgr:FormatMessage("Application_For_Society_Three")
    this.message4Button:SetActive(not CSpeechCtrlMgr.Inst:CheckIfNeedSpeechCtrl(CSpeechCtrlType.Type_Guild_Apply_Message, false))
end
CGuildApplyMessageWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)

    UIEventListener.Get(this.message4Button).onClick = MakeDelegateFromCSFunction(this.OnClickCustomMsgBtn, VoidDelegate, this)
    UIEventListener.Get(this.sendButton).onClick = MakeDelegateFromCSFunction(this.OnClickSendButton, VoidDelegate, this)

    this.radioBox.OnSelect = MakeDelegateFromCSFunction(this.OnClickMessageButton, MakeGenericClass(Action2, QnButton, Int32), this)
end
CGuildApplyMessageWnd.m_OnClickCustomMsgBtn_CS2LuaHook = function (this, go) 
    CInputBoxMgr.ShowInputBox(LocalString.GetString("请输入自定义留言"), MakeDelegateFromCSFunction(this.OnConfirmMessage, MakeGenericClass(Action1, String), this), CGuildMgr.MaxApplyMsgLen * 2, true, CGuildApplyMessageWnd.s_CacheMessage, nil)
end
CGuildApplyMessageWnd.m_OnClickSendButton_CS2LuaHook = function (this, go) 
    --二次检查
    if this:CheckMsg(this.mMessage) then
        if CGuildApplyMessageWnd.ApplyAsMemberAll then
            Gac2Gas.RequestOperationInGuild(CGuildApplyMessageWnd.GuildId, "ApplyAsMemberAll", this.mMessage, "")
        else
            Gac2Gas.RequestOperationInGuild(CGuildApplyMessageWnd.GuildId, "ApplyAsMember", this.mMessage, "")
        end
    end
    CUIManager.CloseUI(CUIResources.GuildApplyMessageWnd)
end
CGuildApplyMessageWnd.m_OnClickMessageButton_CS2LuaHook = function (this, btn, index) 
    if index == 0 then
        this.mMessage = g_MessageMgr:FormatMessage("Application_For_Society_One")
    elseif index == 1 then
        this.mMessage = g_MessageMgr:FormatMessage("Application_For_Society_Two")
    elseif index == 2 then
        this.mMessage = g_MessageMgr:FormatMessage("Application_For_Society_Three")
    end
end
CGuildApplyMessageWnd.m_OnConfirmMessage_CS2LuaHook = function (this, msg) 
    if this:CheckMsg(msg) then
        CGuildApplyMessageWnd.s_CacheMessage = msg
        --字数检查
        if CGuildApplyMessageWnd.ApplyAsMemberAll then
            Gac2Gas.RequestOperationInGuild(CGuildApplyMessageWnd.GuildId, "ApplyAsMemberAll", msg, "")
        else
            Gac2Gas.RequestOperationInGuild(CGuildApplyMessageWnd.GuildId, "ApplyAsMember", msg, "")
        end
    end
    CUIManager.CloseUI(CUIResources.GuildApplyMessageWnd)
end
CGuildApplyMessageWnd.m_CheckMsg_CS2LuaHook = function (this, msg) 
    local length = CUICommonDef.GetStrByteLength(msg)
    local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(msg, true)
    if ret.msg == nil or ret.shouldBeIgnore then
        --message
        g_MessageMgr:ShowMessage("Speech_Violation")
        return false
    end
    return true
end
