-- Auto Generated!!
local CHideAndSeekSkillItem = import "L10.UI.CHideAndSeekSkillItem"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
CHideAndSeekSkillItem.m_Init_CS2LuaHook = function (this, skillId) 
    this.mSkillId = skillId
    local skill = Skill_AllSkills.GetData(skillId)
    this.mSkillIcon:LoadSkillIcon(skill.SkillIcon)
    this.mSkillName.text = skill.Name
end
