local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Vector3 = import "UnityEngine.Vector3"
local CChatLinkMgr = import "CChatLinkMgr"

LuaZuiMengLuWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaZuiMengLuWnd, "LeftTable", "LeftTable", GameObject)
RegistChildComponent(LuaZuiMengLuWnd, "NPC", "NPC", GameObject)
RegistChildComponent(LuaZuiMengLuWnd, "ArchiveTable", "ArchiveTable", GameObject)
RegistChildComponent(LuaZuiMengLuWnd, "Archive", "Archive", GameObject)
RegistChildComponent(LuaZuiMengLuWnd, "ExploreButton", "ExploreButton", GameObject)
RegistChildComponent(LuaZuiMengLuWnd, "PassThreadButton", "PassThreadButton", GameObject)
RegistChildComponent(LuaZuiMengLuWnd, "PassedTex", "PassedTex", GameObject)
RegistChildComponent(LuaZuiMengLuWnd, "InfoText", "InfoText", GameObject)
RegistChildComponent(LuaZuiMengLuWnd, "TipButton", "TipButton", GameObject)

--@endregion RegistChildComponent end

--@endregion UIEvent
RegistClassMember(LuaZuiMengLuWnd, "m_SelectedGroupId") -- 1, 2, 3
RegistClassMember(LuaZuiMengLuWnd, "m_SelectedThreadId") -- 1, 2, 3, 4
RegistClassMember(LuaZuiMengLuWnd, "m_ThreadsInfo")
RegistClassMember(LuaZuiMengLuWnd, "m_NpcDescription")
RegistClassMember(LuaZuiMengLuWnd, "m_NpcName")
RegistClassMember(LuaZuiMengLuWnd, "m_LeftNpcs")
RegistClassMember(LuaZuiMengLuWnd, "m_ArchiveItems")
RegistClassMember(LuaZuiMengLuWnd, "m_ArchiveText")
RegistClassMember(LuaZuiMengLuWnd, "m_ArchiveTitle")
RegistClassMember(LuaZuiMengLuWnd, "m_Tick")


function LuaZuiMengLuWnd:Init()
    -- 获取所有信息
    self.m_ThreadsInfo = {}
    ZhongYuanJie_ZuiMengLu.Foreach(function (groupId, data)
        local t = {}
        local threads = data.Threads
        for i = 0, 2 do
            local thread = ZhongYuanJie_ZuiMengLuThread.GetData(threads[i])
            local readed = CClientMainPlayer.Inst.PlayProp.ZuiMengLuReadSN:GetBit(thread.SN)
            table.insert(t, {ThreadId = threads[i], SN = thread.SN, Title = thread.Title, ArchiveTextLocked = thread.ArchiveTextLocked, ArchiveText = thread.ArchiveText, Readed = readed})
        end
        local thread = ZhongYuanJie_ZuiMengLuThread.GetData(data.FinalThread)
        local readed = CClientMainPlayer.Inst.PlayProp.ZuiMengLuReadSN:GetBit(thread.SN)
        table.insert(t, {ThreadId = data.FinalThread, SN = thread.SN, Title = thread.Title, ArchiveTextLocked = thread.ArchiveTextLocked, ArchiveText = thread.ArchiveText, Readed = readed})
        table.insert(self.m_ThreadsInfo, {Threads = t, GroupId = groupId, SN = data.SN, Portrait = data.Portrait, 
                                            Name = data.Name, NPCDescription = data.NPCDescription,
                                            NPCDescriptionLocked = data.NPCDescriptionLocked, Passed = CClientMainPlayer.Inst.PlayProp.ZuiMengLuRewardSN:GetBit(data.SN)})
    end)
    
    self.m_NpcDescription = self.NPC.transform:Find("Description").gameObject:GetComponent(typeof(UILabel))
    self.m_NoNpcDescription = self.NPC.transform:Find("NoDescription").gameObject:GetComponent(typeof(UILabel))
    self.m_NpcName = self.NPC.transform:Find("Name").gameObject:GetComponent(typeof(UILabel))
    self.m_NoArchiveText = self.transform:Find("Content/NoArchive"):GetComponent(typeof(UILabel))
    self.m_ArchiveText = self.Archive:GetComponent(typeof(UILabel))
    self.m_ArchiveTitle = self.Archive.transform:Find("Title").gameObject:GetComponent(typeof(UILabel))
    self.m_LeftNpcs = {}
    for i = 1, 3 do
        local group = self.LeftTable.transform:Find(SafeStringFormat3("Group%d", i)).gameObject
        group.transform:Find("tex"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(self.m_ThreadsInfo[i].Portrait)
        group.transform:Find("highlight").gameObject:SetActive(false)
        group.transform:Find("highlight/fx"):GetComponent(typeof(CUIFx)):DestroyFx()
        table.insert(self.m_LeftNpcs, group)
    end
    self.m_ArchiveItems = {}
    for i = 1, 4 do
        local archi = self.ArchiveTable.transform:Find(SafeStringFormat3("Archive%d", i)).gameObject
        archi.transform:Find("highlight").gameObject:SetActive(false)
        table.insert(self.m_ArchiveItems, archi)
    end
    self.m_SelectedThreadId = 1
    self.m_SelectedGroupId = 1
    self.ColorNotFinished = self.m_LeftNpcs[1].transform:Find("bar"):GetComponent(typeof(UITexture)).color
    self.ColorFinished = self.m_LeftNpcs[2].transform:Find("bar"):GetComponent(typeof(UITexture)).color
    self.ColorHighlight = self.m_ArchiveItems[1].transform:Find("Label"):GetComponent(typeof(UILabel)).color
    self.ColorNormal = self.m_ArchiveItems[2].transform:Find("Label"):GetComponent(typeof(UILabel)).color
    self.dx = self.m_ArchiveItems[4].transform:Find("Label").localPosition.x
    self.m_ContentPanel = self.transform:Find("Content"):GetComponent(typeof(UIPanel))
    self.m_ContentPanel.alpha = 0

    for i = 1, 3 do
        if not self.m_ThreadsInfo[i].Passed then
            self.m_SelectedGroupId = i
            break
        end
    end

    self.InfoText:GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("ZuiMengLu_RewardDescription")
    UIEventListener.Get(self.TipButton).onClick = DelegateFactory.VoidDelegate(function (go)
        g_MessageMgr:ShowMessage("ZuiMengLu_ReadMe")
    end)

    -- table的OnSelect
    self.ArchiveTable:GetComponent(typeof(QnTabView)).OnSelect = DelegateFactory.Action_QnTabButton_int(function (button, index)
        if index + 1 == self.m_SelectedThreadId then return end
        self.m_ArchiveItems[self.m_SelectedThreadId].transform:Find("highlight").gameObject:SetActive(false)
        self.m_ArchiveItems[self.m_SelectedThreadId].transform:Find("Normal").gameObject:SetActive(true)
        self.m_ArchiveItems[self.m_SelectedThreadId].transform:Find("Label"):GetComponent(typeof(UILabel)).color = self.ColorNormal
        self.m_SelectedThreadId = index + 1
        self:ShowArchiveText()
        self:ShowLeftViewItem(self.m_SelectedGroupId)
    end)
    -- LeftTable
    self.LeftTable:GetComponent(typeof(QnTabView)).OnSelect = DelegateFactory.Action_QnTabButton_int(function (button, index)
        if index + 1 == self.m_SelectedGroupId then return end
        self.m_LeftNpcs[self.m_SelectedGroupId].transform:Find("highlight").gameObject:SetActive(false)
        self.m_LeftNpcs[self.m_SelectedGroupId].transform:Find("highlight/fx"):GetComponent(typeof(CUIFx)):DestroyFx()
        self.m_SelectedGroupId = index + 1
        self:ShowRightView()
        self:ShowLeftViewItem(self.m_SelectedGroupId)
    end)
    -- tick
    if  self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
    local alpha = 0
    self.m_Tick = RegisterTick(function()
        
        if alpha <= 1 then
            local dalpha = (alpha > 0.01) and 0.05 or 0.002
            alpha = alpha + dalpha
            self.m_ContentPanel.alpha = alpha
        else
            UnRegisterTick(self.m_Tick)
            self.m_Tick = nil
        end
    end, 50)
    self:ShowRightView()
    self:ShowLeftView()
end

function LuaZuiMengLuWnd:ShowRightView()
    if not self.m_SelectedGroupId then return end
    local Finished = false
    Finished, self.m_SelectedThreadId = self:IsFinished(self.m_SelectedGroupId)
    local Passed = self:IsPassed(self.m_SelectedGroupId)

    local npc = self.m_ThreadsInfo[self.m_SelectedGroupId]
    self.NPC:GetComponent(typeof(CUITexture)):LoadNPCPortrait(npc.Portrait)
    self.m_NpcName.text = npc.Name
    -- 批注
    if Passed then
        self.m_NpcDescription.text = CChatLinkMgr.TranslateToNGUIText(npc.NPCDescription, false)
    else
        self.m_NoNpcDescription.text = CChatLinkMgr.TranslateToNGUIText(npc.NPCDescriptionLocked, false)
    end
    -- 完成三个就显示传递线索
    if Finished and not Passed then
        UIEventListener.Get(self.PassThreadButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            -- 传递线索
            Gac2Gas.ZuiMengLu_TryReward(npc.GroupId)
        end)
    end
    self.PassThreadButton.gameObject:SetActive(Finished and not Passed)
    self.PassedTex.gameObject:SetActive(Passed)
    self.m_NpcDescription.gameObject:SetActive(Passed)
    self.m_NoNpcDescription.gameObject:SetActive(not Passed)

    -- 设置tab
    for i = 1, 4 do
        local choosed = (i == self.m_SelectedThreadId)
        self.m_ArchiveItems[i].transform:Find("highlight").gameObject:SetActive(choosed)
        self.m_ArchiveItems[i].transform:Find("Normal").gameObject:SetActive(not choosed)
        self.m_ArchiveItems[i].transform:Find("Label"):GetComponent(typeof(UILabel)).color = (choosed and self.ColorHighlight or self.ColorNormal)
        local sn = npc.Threads[i].SN
        local state = CClientMainPlayer.Inst.PlayProp.ZuiMengLuThreadSN:GetBit(sn)
        self.m_ArchiveItems[i].transform:Find("lock").gameObject:SetActive(not state)
        local pos = self.m_ArchiveItems[i].transform:Find("Label").position
        local dx = state and 0 or self.dx
        self.m_ArchiveItems[i].transform:Find("Label").localPosition = Vector3(dx + pos.x, pos.y, pos.z)
        local readed = npc.Threads[i].Readed
        self.m_ArchiveItems[i].transform:Find("Alert").gameObject:SetActive(state and not readed)
    end
    self:ShowArchiveText()
end

function LuaZuiMengLuWnd:ShowArchiveText()
    local thread = self.m_ThreadsInfo[self.m_SelectedGroupId].Threads[self.m_SelectedThreadId]
    local state = CClientMainPlayer.Inst.PlayProp.ZuiMengLuThreadSN:GetBit(thread.SN)
    if state and not thread.Readed then
        thread.Readed = true
        Gac2Gas.ZuiMengLu_ReadThread(thread.ThreadId)
    end
    self.m_ArchiveItems[self.m_SelectedThreadId].transform:Find("Alert").gameObject:SetActive(false)
    self.m_ArchiveItems[self.m_SelectedThreadId].transform:Find("highlight").gameObject:SetActive(true)
    self.m_ArchiveItems[self.m_SelectedThreadId].transform:Find("Normal").gameObject:SetActive(false)
    self.m_ArchiveItems[self.m_SelectedThreadId].transform:Find("Label"):GetComponent(typeof(UILabel)).color = self.ColorHighlight

    self.m_ArchiveText.gameObject:SetActive(state)
    self.m_NoArchiveText.gameObject:SetActive(not state)

    if state then
        self.m_ArchiveTitle.text = thread.Title
        self.m_ArchiveText.text = thread.ArchiveText
        self.ExploreButton:SetActive(false)
    else
        self.m_NoArchiveText.text = thread.ArchiveTextLocked
        if self.m_SelectedThreadId ~= 4 then
            UIEventListener.Get(self.ExploreButton).onClick = DelegateFactory.VoidDelegate(function (go)
                Gac2Gas.ZuiMengLu_TraceThread(thread.ThreadId)
                CUIManager.CloseUI(CLuaUIResources.ZuiMengLuWnd)
            end)
        end
        self.ExploreButton:SetActive(self.m_SelectedThreadId ~= 4)
    end
end

function LuaZuiMengLuWnd:ShowLeftView()
    for i = 1, 3 do
        self:ShowLeftViewItem(i)
    end
end

function LuaZuiMengLuWnd:ShowLeftViewItem(index)
    local obj = self.m_LeftNpcs[index]
    obj.transform:Find("highlight").gameObject:SetActive(index == self.m_SelectedGroupId)
    if index == self.m_SelectedGroupId then
        obj.transform:Find("highlight/fx"):GetComponent(typeof(CUIFx)):LoadFx("fx/ui/prefab/UI_zhongyuanjie_zuimenglu_xuanzhong.prefab")
    end
    local bar = obj.transform:Find("bar").gameObject
    local group_threads = self.m_ThreadsInfo[index].Threads
    local count = 0
    local alert = false
    for i = 1, 4 do
        local unlocked = CClientMainPlayer.Inst.PlayProp.ZuiMengLuThreadSN:GetBit(group_threads[i].SN)
        local readed = group_threads[i].Readed
        count = count + (unlocked and 1 or 0)
        alert = alert or (unlocked and not readed)
    end

    if self:IsPassed(index) then
        self:ShowProgressBar(bar, 5)
        obj.transform:Find("Alert").gameObject:SetActive(alert)
    else
        self:ShowProgressBar(bar, count)
        -- 设置alert
        alert = alert or (count == 4)
        obj.transform:Find("Alert").gameObject:SetActive(alert)
    end
end

function LuaZuiMengLuWnd:ShowProgressBar(bar, state)  -- 1, 2, 3, 4, 5(finished)
    if state == 5 then
        bar.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("完成")
        bar:GetComponent(typeof(UITexture)).color = self.ColorFinished
    else
        bar.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("%d/4", state)
        bar:GetComponent(typeof(UITexture)).color = self.ColorNotFinished
    end
end

function LuaZuiMengLuWnd:OnEnable()
    g_ScriptEvent:AddListener("ZuiMengLu_TryRewardResult", self, "RewardUpdate")
end

function LuaZuiMengLuWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ZuiMengLu_TryRewardResult", self, "RewardUpdate")
end

function LuaZuiMengLuWnd:RewardUpdate(GroupId)
    for index = 1, 3 do
        if self.m_ThreadsInfo[index].GroupId == GroupId then
            self:ShowLeftViewItem(index)
            if index == self.m_SelectedGroupId then
                self:ShowRightView()
            end
            break
        end
    end
end

function LuaZuiMengLuWnd:IsFinished(GroupId)
    local thread_info = self.m_ThreadsInfo[GroupId]
    for i = 1, 3 do
        local sn = thread_info.Threads[i].SN
        local state = CClientMainPlayer.Inst.PlayProp.ZuiMengLuThreadSN:GetBit(sn)
        if not state then
            return false, i
        end
    end
    return true, 1
end

function LuaZuiMengLuWnd:IsPassed(GroupId)
    local group_sn = self.m_ThreadsInfo[GroupId].SN
    return CClientMainPlayer.Inst.PlayProp.ZuiMengLuRewardSN:GetBit(group_sn)
end

function LuaZuiMengLuWnd:OnDestroy()
    if  self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end

end
