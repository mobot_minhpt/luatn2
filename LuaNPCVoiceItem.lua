local UISprite = import "UISprite"
local CUITexture = import "L10.UI.CUITexture"
local SoundManager = import "SoundManager"
local UISpriteAnimation = import "UISpriteAnimation"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local QnButton = import "L10.UI.QnButton"
local CChatHelper = import "L10.UI.CChatHelper"
local UIWidget = import "UIWidget"
local PlayerSettings = import "L10.Game.PlayerSettings"

LuaNPCVoiceItem = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNPCVoiceItem, "VoiceBar", "VoiceBar", GameObject)
RegistChildComponent(LuaNPCVoiceItem, "SenderNameLabel", "SenderNameLabel", UILabel)
RegistChildComponent(LuaNPCVoiceItem, "Portrait", "Portrait", CUITexture)
RegistChildComponent(LuaNPCVoiceItem, "TransformButton", "TransformButton", QnButton)
RegistChildComponent(LuaNPCVoiceItem, "TextLabel", "TextLabel", GameObject)
RegistChildComponent(LuaNPCVoiceItem, "ChatBubble", "ChatBubble", UISprite)
RegistChildComponent(LuaNPCVoiceItem, "ChannelNameLabel", "ChannelNameLabel", UILabel)
RegistChildComponent(LuaNPCVoiceItem, "LoudSpeaker", "LoudSpeaker", UISpriteAnimation)

--@endregion RegistChildComponent end
RegistClassMember(LuaNPCVoiceItem, "m_VoiceTimeLabel")
RegistClassMember(LuaNPCVoiceItem, "m_VoiceTextLabel")
RegistClassMember(LuaNPCVoiceItem, "m_Data")
RegistClassMember(LuaNPCVoiceItem, "m_TransformTextSwitch")
RegistClassMember(LuaNPCVoiceItem, "m_TransformBtnHightlight")
RegistClassMember(LuaNPCVoiceItem, "m_Tick")
function LuaNPCVoiceItem:Awake()
    self.m_VoiceTimeLabel = self.VoiceBar.transform:Find("VoiceBarSprite/VoiceLengthLabel").gameObject:GetComponent(typeof(UILabel))
    self.m_TransformBtnHightlight = self.TransformButton.transform:Find("highlight").gameObject
    self.m_VoiceTextLabel = self.TextLabel.transform:Find("Label").gameObject
    self.m_Tick = nil
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaNPCVoiceItem:Init(data)
    self.m_Data = data
    self.TextLabel.gameObject:SetActive(false)
    self.m_TransformBtnHightlight:SetActive(false)
    self.m_TransformTextSwitch = false
    self.LoudSpeaker:ResetToBeginning()
    self.LoudSpeaker:Pause()
    self:InitSenderInfo()
    self:InitVoiceItem(self.m_Data.text)
    self:LayoutView()
end

function LuaNPCVoiceItem:InitSenderInfo()
    self.ChannelNameLabel.text = SafeStringFormat3("[%s]%s[-]",CChatHelper.GetChannelColor24(self.m_Data.channel), self.m_Data.channelName)
    self.SenderNameLabel.text = self.m_Data.senderName;
    self.Portrait:LoadNPCPortrait(self.m_Data.senderPortrait)
end

function LuaNPCVoiceItem:InitVoiceItem(text)
    local VoiceInfo,Duartion,TextInfo = string.match( text,"audio=(.+),duration=(%d+),text=(.+)" )
    self.m_VoiceTimeLabel.text = SafeStringFormat3(LocalString.GetString("%s秒"),Duartion)
    self.m_VoiceTextLabel:GetComponent(typeof(UILabel)).text = TextInfo
    UIEventListener.Get(self.ChatBubble.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:OnVoicePlay(VoiceInfo,tonumber(Duartion))
    end)

    UIEventListener.Get(self.TransformButton.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:VoiceTextSwitch()
    end)
end

function LuaNPCVoiceItem:VoiceTextSwitch()
    self.m_TransformTextSwitch = not self.m_TransformTextSwitch
    self.TextLabel:SetActive(self.m_TransformTextSwitch)
    self.m_TransformBtnHightlight:SetActive(self.m_TransformTextSwitch)
    self:LayoutView()
end

function LuaNPCVoiceItem:LayoutView()
    local paddingoffset = 19
    local VOICE_BAR_HEIGHT = 50
    local msgLabel = self.m_VoiceTextLabel:GetComponent(typeof(UILabel))
    if self.m_TransformTextSwitch then
        self.ChatBubble.height = VOICE_BAR_HEIGHT + msgLabel.localSize.y + paddingoffset * 2 - msgLabel.spacingY
    else
        self.ChatBubble.height = VOICE_BAR_HEIGHT + paddingoffset * 2
    end
    self.gameObject:GetComponent(typeof(UIWidget)).height = (math.abs(self.ChatBubble.transform.localPosition.y) + self.ChatBubble.height)
    msgLabel:ResizeCollider()
    g_ScriptEvent:BroadcastInLua("UpdateFriendChatListItemReLayout",self.transform)
end
function LuaNPCVoiceItem:OnVoicePlay(VoiceInfo,Duartion)
    if VoiceInfo and Duartion then
        if not PlayerSettings.SoundEnabled then
            local okfunc = function()
                PlayerSettings.SoundEnabled = true
                self:ProcessPlayVoice(VoiceInfo,Duartion)
            end
            local msg = g_MessageMgr:FormatMessage("NPCCHAT_SOUND_OPEN")
            g_MessageMgr:ShowOkCancelMessage(msg,okfunc,nil,nil,nil,false)
        else
            self:ProcessPlayVoice(VoiceInfo,Duartion)
        end
    else
        self.LoudSpeaker:ResetToBeginning()
        self.LoudSpeaker:Pause()
    end
end

function LuaNPCVoiceItem:ProcessPlayVoice(VoiceInfo,Duartion)
    SoundManager.Inst:StartDialogSound(VoiceInfo)
    self.LoudSpeaker:Play()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
    local OnVoiceStop = function()
        self.LoudSpeaker:ResetToBeginning()
        self.LoudSpeaker:Pause()
    end
    self.m_Tick = RegisterTickOnce(OnVoiceStop,Duartion*1000)
end

function LuaNPCVoiceItem:OnDisable()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end
--@region UIEvent

--@endregion UIEvent

