local UIPanel = import "UIPanel"

local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local CUIFx = import "L10.UI.CUIFx"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local EnumShowGreyType = import "L10.UI.CUITexture.EnumShowGreyType"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"

LuaScrollTaskDragPictureGameTaskView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaScrollTaskDragPictureGameTaskView, "LongPicture", "LongPicture", CUITexture)
RegistChildComponent(LuaScrollTaskDragPictureGameTaskView, "UIPanel", "UIPanel", UIPanel)
RegistChildComponent(LuaScrollTaskDragPictureGameTaskView, "GameDesc", "GameDesc", UILabel)
RegistChildComponent(LuaScrollTaskDragPictureGameTaskView, "Fx1", "Fx1", CUIFx)
RegistChildComponent(LuaScrollTaskDragPictureGameTaskView, "Fx2", "Fx2", CUIFx)
RegistChildComponent(LuaScrollTaskDragPictureGameTaskView, "DescPanel", "DescPanel", UIPanel)

--@endregion RegistChildComponent end
RegistClassMember(LuaScrollTaskDragPictureGameTaskView, "m_Ratio")
RegistClassMember(LuaScrollTaskDragPictureGameTaskView, "m_PanelWidth")
RegistClassMember(LuaScrollTaskDragPictureGameTaskView, "m_MaxX")
RegistClassMember(LuaScrollTaskDragPictureGameTaskView, "m_ScrollView")
RegistClassMember(LuaScrollTaskDragPictureGameTaskView, "m_FxPath")
RegistClassMember(LuaScrollTaskDragPictureGameTaskView, "m_Fx")

function LuaScrollTaskDragPictureGameTaskView:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    -- 860
    --
    self.m_Fx           = self.transform:Find("DescPanel/Fx"):GetComponent(typeof(CUIFx))
    self.m_ScrollView   = self.UIPanel:GetComponent(typeof(CUIRestrictScrollView))
    self.m_PanelWidth   = self.transform:GetComponent(typeof(UIPanel)).baseClipRegion.z
end

function LuaScrollTaskDragPictureGameTaskView:Start()
    CommonDefs.AddOnDragListener(self.LongPicture.gameObject, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
		self:OnScreenDrag(go, delta)
    end), false)
    
    UIEventListener.Get(self.LongPicture.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(g)
        self:OnDragStart(g)
    end)

    UIEventListener.Get(self.LongPicture.gameObject).onDragEnd = DelegateFactory.VoidDelegate(function(g)
        self:OnDragEnd(g)
    end)
end

function LuaScrollTaskDragPictureGameTaskView:InitGame(gameId, taskId, callback)
    self:OnScrollTaskGamePanelChange()
    self.m_ScrollView:AlignToEnd()
    self:LoadDesignData(gameId)
    self.GameDesc.gameObject:SetActive(true)
    self.m_Fx:DestroyFx()

    self.m_IsPlaying = true
    self.m_Callback = callback
end

function LuaScrollTaskDragPictureGameTaskView:LoadDesignData(gameId)
    local data = ScrollTask_DragPictureGame.GetData(gameId)
    if data == nil then
        return
    end

    self.GameDesc.text = data.Desc
    self.LongPicture:LoadMaterial(data.PicturePath, EnumShowGreyType.DefaultSetting, DelegateFactory.Action_bool(function(b)
        self.m_Ratio = self.LongPicture.mainTexture.width / self.LongPicture.mainTexture.height
        self.LongPicture.texture.width = self.LongPicture.texture.height * self.m_Ratio 
        self.m_MaxX = self.LongPicture.texture.width - self.m_PanelWidth
    end))

    self.m_FxPath = data.FxPath
end
--@region UIEvent

--@endregion UIEvent

function LuaScrollTaskDragPictureGameTaskView:OnEnable()
    g_ScriptEvent:AddListener("ScrollTaskGamePanelChange", self, "OnScrollTaskGamePanelChange")
end

function LuaScrollTaskDragPictureGameTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("ScrollTaskGamePanelChange", self, "OnScrollTaskGamePanelChange")
end

function LuaScrollTaskDragPictureGameTaskView:OnScrollTaskGamePanelChange()
    self.UIPanel.depth = self.transform:GetComponent(typeof(UIPanel)).depth + 1
    self.DescPanel.depth = self.UIPanel.depth + 1
end

function LuaScrollTaskDragPictureGameTaskView:OnScreenDrag(go, delta)
    if math.abs(self.UIPanel.transform.localPosition.x - self.m_MaxX) < 100 and self.m_IsPlaying then
        self.m_IsPlaying = false
        self:OnSuccess()
    end
end

function LuaScrollTaskDragPictureGameTaskView:OnDragStart(go)
    --local currentPos = UICamera.currentTouch.pos
end

function LuaScrollTaskDragPictureGameTaskView:OnDragEnd(go)
    if math.abs(self.UIPanel.transform.localPosition.x - self.m_MaxX) < 100 and self.m_IsPlaying then
        self.m_IsPlaying = false
        self:OnSuccess()
    end
end

function LuaScrollTaskDragPictureGameTaskView:ProcessProgress()

end

function LuaScrollTaskDragPictureGameTaskView:OnSuccess()
    self.GameDesc.gameObject:SetActive(false)
    if self.m_FxPath then
        self.m_Fx:LoadFx(self.m_FxPath)
    end

    UnRegisterTick(self.m_Tick)
    self.m_Tick = RegisterTickOnce(function()
        self:OnGameFinish()
    end,self.m_FinishDelay or 1000)
end

function LuaScrollTaskDragPictureGameTaskView:OnGameFinish()
    if self.m_Callback then
        self.m_Callback()
    end
end

function LuaScrollTaskDragPictureGameTaskView:OnDestroy()
    UnRegisterTick(self.m_Tick)
end
