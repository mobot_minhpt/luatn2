-- Auto Generated!!
local CHuanLingConsumption = import "L10.UI.CHuanLingConsumption"
local CItemMgr = import "L10.Game.CItemMgr"
local Color = import "UnityEngine.Color"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
CHuanLingConsumption.m_Init_CS2LuaHook = function (this, consumptionId) 
    this.m_ConsumptionId = consumptionId
    this.itemInfoPos.gameObject:SetActive(false)
    this.m_ButtonName = LocalString.GetString("获取")
    this:InitAmount()
end
CHuanLingConsumption.m_InitAmount_CS2LuaHook = function (this) 

    local item = Item_Item.GetData(this.m_ConsumptionId)
    if item ~= nil then
        this.itemTexture:LoadMaterial(item.Icon)

        local bindCount, notbindCount
        bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(this.m_ConsumptionId)
        if bindCount + notbindCount > 0 then
            this.disableSprite.gameObject:SetActive(false)
            this.getLabel:SetActive(false)
            this.countLabel.text = tostring((bindCount + notbindCount))
            this.countLabel.color = Color.white
        else
            this.disableSprite.gameObject:SetActive(true)
            this.getLabel:SetActive(true)
            this.countLabel.text = "0"
            this.countLabel.color = Color.red
        end
    end
end
