local CPlayerInfoMgr=import "CPlayerInfoMgr"
local CRankData=import "L10.UI.CRankData"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CChatHelper = import "L10.UI.CChatHelper"
local CTrackMgr = import "L10.Game.CTrackMgr"

LuaZhouNianQingFSCRankWnd = class()

RegistClassMember(LuaZhouNianQingFSCRankWnd,"m_TableView")
RegistClassMember(LuaZhouNianQingFSCRankWnd,"m_RankList")
RegistClassMember(LuaZhouNianQingFSCRankWnd, "m_MyRank")
RegistClassMember(LuaZhouNianQingFSCRankWnd, "m_EmptyTip")
RegistClassMember(LuaZhouNianQingFSCRankWnd, "m_MainPlayerInfo")
RegistClassMember(LuaZhouNianQingFSCRankWnd, "m_MyRankLabel")
RegistClassMember(LuaZhouNianQingFSCRankWnd, "m_MyRankSprite")
RegistClassMember(LuaZhouNianQingFSCRankWnd, "m_MyNameLabel")
RegistClassMember(LuaZhouNianQingFSCRankWnd, "m_MyScoreLabel")
RegistClassMember(LuaZhouNianQingFSCRankWnd, "m_TableViewDataSource")

RegistClassMember(LuaZhouNianQingFSCRankWnd, "m_NpcMap")


function LuaZhouNianQingFSCRankWnd:Awake()
    self.m_NpcMap = {}
    ZhouNianQing2023_PlayWithNpc.Foreach(function(k, v)
        self.m_NpcMap[k] = v.MapId
    end)

    self.m_MyRank = 0
    self.m_RankList = {}

    self.m_EmptyTip = self.transform:Find("Anchor/EmptyTip").gameObject
    self.m_TableView = self.transform:Find("Anchor/TableView"):GetComponent(typeof(QnTableView))
    self.m_MainPlayerInfo = self.m_TableView.transform:Find("MainPlayerInfo").gameObject
    self.m_MyRankLabel = self.m_MainPlayerInfo.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    self.m_MyRankSprite = self.m_MyRankLabel.transform:Find("RankSprite"):GetComponent(typeof(UISprite))
    self.m_MyNameLabel = self.m_MainPlayerInfo.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    self.m_MyScoreLabel = self.m_MainPlayerInfo.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))

    self.m_MyRankSprite.gameObject:SetActive(false)

    self.m_MainPlayerInfo:SetActive(false)

    self.m_MyNameLabel.text = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name or ""
    self.m_MyRankLabel.text = LocalString.GetString("未上榜")
    self.m_MyScoreLabel.text = LocalString.GetString("—")

    self.m_TableViewDataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RankList
        end,
        function(item, index)
            self:InitItem(item, index, self.m_RankList[index + 1])
        end
    )
    self.m_TableView.m_DataSource = self.m_TableViewDataSource
    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)  
        local data = self.m_RankList[row+1]
        if data then
            if data.IsNPC then
                g_MessageMgr:ShowMessage("FSC_RANK_SELECT_NPC_HINT")
            else
                CPlayerInfoMgr.ShowPlayerPopupMenu(data.ID, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, "", nil, Vector3.zero, AlignType.Default)
            end
        end
    end)

    --self:InitOneItem(self.transform:Find("Anchor/RewardHint/Reward").gameObject, YuanXiao2023_NaoYuanXiao.GetData().RewardTitleItemId)
    self.transform:Find("Anchor/RewardHint"):GetComponent(typeof(UILabel)).text = ZhouNianQing2023_Setting.GetData().ListTips
end

--[[function LuaZhouNianQingFSCRankWnd:InitOneItem(curItem, itemId)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemId)

    if ItemData then 
        texture:LoadMaterial(ItemData.Icon) 
    end

    CommonDefs.AddOnClickListener(
        curItem,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end--]]

function LuaZhouNianQingFSCRankWnd:Init()
    self:OnRankDataReady()
end

function LuaZhouNianQingFSCRankWnd:OnEnable()
    --g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end
function LuaZhouNianQingFSCRankWnd:OnDisable()
    --g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaZhouNianQingFSCRankWnd:InitItem(item, index, info)
    item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

    local tf = item.transform
    local rankLabel = tf:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankSprite = tf:Find("RankLabel/RankSprite"):GetComponent(typeof(UISprite))
    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    local scoreLabel = tf:Find("ScoreLabel"):GetComponent(typeof(UILabel))

    local yp = tf:Find("PlayCard/YuePai").gameObject
    local dp = tf:Find("PlayCard/DaPai").gameObject

    if CClientMainPlayer.Inst and info.ID == CClientMainPlayer.Inst.Id then
        nameLabel.text = info.Name
        yp:SetActive(false)
        dp:SetActive(false)
    elseif info.IsNPC then
        local mapData = PublicMap_PublicMap.GetData(self.m_NpcMap[info.ID] and self.m_NpcMap[info.ID][0] or 0)
        if mapData then 
            nameLabel.text = SafeStringFormat3("[%s]"..info.Name, mapData.Name)
        else
            nameLabel.text = info.Name
        end
        nameLabel.color = NGUIText.ParseColor24("ffec81", 0)
        yp:SetActive(false)
        dp:SetActive(true)
        UIEventListener.Get(dp).onClick = DelegateFactory.VoidDelegate(function(go)
            CUIManager.CloseUI(CLuaUIResources.ZhouNianQingFSCRankWnd)
            CUIManager.CloseUI(CLuaUIResources.ZhouNianQing2023MainWnd)
            CTrackMgr.Inst:FindNPC(info.ID, self.m_NpcMap[info.ID][0], self.m_NpcMap[info.ID][1], self.m_NpcMap[info.ID][2], nil, nil)
        end)
    else
        nameLabel.text = info.Name
        UIEventListener.Get(yp).onClick = DelegateFactory.VoidDelegate(function(go)
            CChatHelper.ShowFriendWnd(info.ID, info.Name)
        end)
    end

    local rank = info.Rank
    if rank == self.m_MyRank then 
        rankLabel.color = Color.green 
        nameLabel.color = Color.green 
        scoreLabel.color = Color.green
    end
    rankSprite.gameObject:SetActive(true)
    rankLabel.text = ""
    if rank == 1 then
        rankSprite.spriteName = "Rank_No.1"
    elseif rank == 2 then
        rankSprite.spriteName = "Rank_No.2png"
    elseif rank == 3 then
        rankSprite.spriteName = "Rank_No.3png"
    else
        rankSprite.gameObject:SetActive(false)
        rankLabel.text = tostring(rank)
    end
    scoreLabel.text = info.Value or LocalString.GetString("—") 
end

function LuaZhouNianQingFSCRankWnd:OnRankDataReady()
    local myInfo = {
        ID = LuaFourSeasonCardMgr.SelfData.playerId,
        Name = LuaFourSeasonCardMgr.SelfData.name,
        Rank = LuaFourSeasonCardMgr.SelfData.rank or 0, 
        Value = LuaFourSeasonCardMgr.SelfData.score
    }
    self.m_MyRank = myInfo.Rank
    if myInfo.Rank > 0 then
        local rank = myInfo.Rank
        self.m_MyRankLabel.text = ""
        self.m_MyRankSprite.gameObject:SetActive(true)
        if rank == 1 then
            self.m_MyRankSprite.spriteName = "Rank_No.1"
        elseif rank == 2 then
            self.m_MyRankSprite.spriteName = "Rank_No.2png"
        elseif rank == 3 then
            self.m_MyRankSprite.spriteName = "Rank_No.3png"
        else
            self.m_MyRankSprite.gameObject:SetActive(false)
            self.m_MyRankLabel.text = tostring(rank)
        end
    else
        self.m_MyRankLabel.text = LocalString.GetString("未上榜")
    end

    self.m_MyNameLabel.text = myInfo.Name or (CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name or LocalString.GetString("—"))
    self.m_MyScoreLabel.text = myInfo.Value or LocalString.GetString("—") 

    self.m_MyScoreLabel.color = Color.green
    self.m_MyRankLabel.color = Color.green
    self.m_MyNameLabel.color = Color.green

    self.m_RankList = LuaFourSeasonCardMgr.RankList

    self.m_MainPlayerInfo:SetActive(#self.m_RankList > 0) 
    self.m_EmptyTip:SetActive(#self.m_RankList == 0)

    self.m_TableView:ReloadData(true, false)
end