local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CJiFenWebMgr = import "L10.Game.CJiFenWebMgr"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Gac2GasC = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local CButton = import "L10.UI.CButton"
local CUITexture = import "L10.UI.CUITexture"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local WWW = import "UnityEngine.WWW"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
LuaOtherWelfareTemplate = class()

RegistClassMember(LuaOtherWelfareTemplate, "m_TitleLabel")
RegistClassMember(LuaOtherWelfareTemplate, "m_DescLabel")
RegistClassMember(LuaOtherWelfareTemplate, "m_Button")
RegistClassMember(LuaOtherWelfareTemplate, "m_ButtonAlert")
RegistClassMember(LuaOtherWelfareTemplate, "m_IconTexture")

RegistClassMember(LuaOtherWelfareTemplate, "m_AwardKey")

function LuaOtherWelfareTemplate:Awake()
    self.m_TitleLabel = self.transform:Find("Title"):GetComponent(typeof(UILabel))
    self.m_DescLabel = self.transform:Find("Description"):GetComponent(typeof(UILabel))
    self.m_Button = self.transform:Find("GetBonusBtn"):GetComponent(typeof(CButton))
    self.m_ButtonAlert = self.transform:Find("GetBonusBtn/Alert").gameObject
    self.m_IconTexture = self.transform:Find("ItemCell/Texture"):GetComponent(typeof(CUITexture))
    CommonDefs.AddOnClickListener(self.m_Button.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnButtonClick(go) end), false)
end

function LuaOtherWelfareTemplate:InitContent(key)
    self.m_AwardKey = key
    self.m_TitleLabel.text = ""
    self.m_DescLabel.text = ""
    self.m_Button.Text = ""
    local data = KaiFuActivity_Others.GetData(key)
    if data ~= nil then
        self.m_TitleLabel.text = data.Title
        self.m_DescLabel.text = data.Desc

        if CommonDefs.IS_HMT_CLIENT and key == "BindPhone" then
            self.m_DescLabel.text = LocalString.GetString("首次关联或确认关联手机可以获得礼包一份！")
        end

        self.m_Button.Text = data.ButtonName
        self.m_IconTexture:LoadMaterial(data.Icon)
        if key == "BindPhone" then
            self.m_ButtonAlert:SetActive(not CSigninMgr.Inst.hasCheckBindPhone and CSigninMgr.Inst:playerFitBindPhone())
        elseif key == "Invitation" then
            self.m_ButtonAlert:SetActive(not CSigninMgr.Inst.hasCheckInviteFriend)
        elseif key == "QiYuJLB" then
            self.m_ButtonAlert:SetActive(CJiFenWebMgr.Inst.m_HasAlert)
        elseif key == "BindDaShen" then
            self:RefreshDashenStatus()
        elseif key == "TeamReverse" then
            Gac2Gas.TeamAppointmentCheckHasReward()
            self.m_ButtonAlert:SetActive(false)
        elseif key == "Unity2018GengXin" then
            self.m_ButtonAlert:SetActive(LuaWelfareMgr:CanShowUnity2018UpgradeAlert())
        else
            self.m_ButtonAlert:SetActive(false)
        end
    end
end

function LuaOtherWelfareTemplate:OnButtonClick(go)

    local default = self.m_AwardKey
    if default == "BindPhone" then
        self:OnBindPhone()
    elseif default == "GiftExchange" then
        self:OnGiftExchange()
    elseif default == "Invitation" then
        self:OnInviteFriend()
    elseif default == "ZhaoHui" then
        self:OnHuiLiu()
    elseif default == "CampusCode" then
        self:OnSchoolInvitation()
    elseif default == "BindAssist" then
        Gac2GasC.TryBindRoleForAppHelper()
    elseif default == "BindDaShen" then
        CUIManager.ShowUI(CUIResources.BindDashenWnd)
    elseif default == "QiYuJLB" then
        CJiFenWebMgr.Inst:OpenUrl()
    elseif default == "SIMCard" then
        self:CheckSIMCard()
    elseif default == "CreditCard" then
        self:CheckCreditCard()
    elseif default == "DXSIMCard" then
        self:CheckDXSIMCard()
    elseif default == "TeamReverse" then
        Gac2Gas.TeamAppointmentPlayerQueryReward()
    elseif default == "ZhaoHuiNew" then
        CUIManager.ShowUI(CLuaUIResources.HuiLiuNewMainWnd)
    elseif default == "Unity2018GengXin" then
        LuaWelfareMgr:OpenUnity2018UpgradeWnd()
    elseif default == "WeChatWelfare" then
        self:OnWeChatWelfareClick()
    elseif default == "AWuWeChatwelfare" then
        self:OnAWuWeChatwelfareClick()
    end
end

function LuaOtherWelfareTemplate:OnBindPhone()
    if CommonDefs.IS_HMT_CLIENT then
        CUIManager.ShowUI(CLuaUIResources.HmtBindWnd)
    else
        Gac2GasC.BindMobileOpenWnd()
        self.m_ButtonAlert:SetActive(false)
        CSigninMgr.Inst.hasCheckBindPhone = true
        EventManager.Broadcast(EnumEventType.OnCheckBindPhone)
    end
end

function LuaOtherWelfareTemplate:OnGiftExchange()
    CUIManager.ShowUI(CUIResources.ExchangeCodeWnd)
end

function LuaOtherWelfareTemplate:OnInviteFriend()
    Gac2GasC.RequestInvitationInfo()
    self.m_ButtonAlert:SetActive(false)
    CSigninMgr.Inst.hasCheckInviteFriend = true
    EventManager.Broadcast(EnumEventType.OnCheckInviteFriend)
    CUIManager.ShowUI(CUIResources.InviteFriendWnd)
end

function LuaOtherWelfareTemplate:OnHuiLiu()
    CUIManager.ShowUI(CUIResources.CallBackFriendWnd)
end

function LuaOtherWelfareTemplate:OnSchoolInvitation()
    Gac2Gas.QueryCampusCodeInfo()
end

function LuaOtherWelfareTemplate:CheckSIMCard()
    CWebBrowserMgr.Inst:OpenUrl(g_MessageMgr:Format(GameSetting_Common.GetData().SIMCardWebSite, WWW.EscapeURL(CLoginMgr.Inst.m_UniSdkSauthInfo.username), CClientMainPlayer.Inst.Id))
    Gac2Gas.OpenSIMCardApplyURL()
end

function LuaOtherWelfareTemplate:CheckCreditCard()
    CWebBrowserMgr.Inst:OpenUrl(GameSetting_Common.GetData().CreditCardWebSite)
end

function LuaOtherWelfareTemplate:CheckDXSIMCard()
    CWebBrowserMgr.Inst:OpenUrl(GameSetting_Common.GetData().DXSIMCardWebSite)
end

function LuaOtherWelfareTemplate:OnWeChatWelfareClick()
    if LuaJingLingMgr.m_WeChatjinglingKey and LuaJingLingMgr.m_WeChatjinglingKey[2] then
        CJingLingMgr.Inst:ShowJingLingWnd(LuaJingLingMgr.m_WeChatjinglingKey[2], "o_push", true, false, nil, false)
    else
        local key = Welfare_Setting.GetData().WelfareWeChatJIngLingDefaultKey
        if key then
            CJingLingMgr.Inst:ShowJingLingWnd(key, "o_push", true, false, nil, false)
        end
    end
end

function LuaOtherWelfareTemplate:OnAWuWeChatwelfareClick()
    CWebBrowserMgr.Inst:OpenUrl("https://crm.u1.netease.com/o/redirect.html?code=GmTpa")
end

function LuaOtherWelfareTemplate:OnEnable()
    g_ScriptEvent:AddListener("JiFenWebInfoResult", self, "UpdateQiYuJLBAlert")
    g_ScriptEvent:AddListener("OnDashenBindStatusChange", self, "RefreshDashenStatus")
    g_ScriptEvent:AddListener("TeamAppointmentCheckHasRewardResult", self, "UpdateInfo")
    g_ScriptEvent:AddListener("OnRefresbUnity2018UpgradeInfo", self, "OnRefresbUnity2018UpgradeInfo")
end

function LuaOtherWelfareTemplate:OnDisable()
    g_ScriptEvent:RemoveListener("JiFenWebInfoResult", self, "UpdateQiYuJLBAlert")
    g_ScriptEvent:RemoveListener("OnDashenBindStatusChange", self, "RefreshDashenStatus")
    g_ScriptEvent:RemoveListener("TeamAppointmentCheckHasRewardResult", self, "UpdateInfo")
    g_ScriptEvent:RemoveListener("OnRefresbUnity2018UpgradeInfo", self, "OnRefresbUnity2018UpgradeInfo")
end

function LuaOtherWelfareTemplate:UpdateQiYuJLBAlert(args)
    if self.m_AwardKey == "QiYuJLB" then
        self.m_ButtonAlert:SetActive(CJiFenWebMgr.Inst.m_HasAlert)
    end
end

function LuaOtherWelfareTemplate:RefreshDashenStatus(args)
    if self.m_AwardKey == "BindDaShen" then
        self.m_ButtonAlert:SetActive(false)
        if System.String.IsNullOrEmpty(CClientMainPlayer.Inst.PlayProp.DaShenAppData.BindedId) then
            self.m_Button.Text = LocalString.GetString("绑定")
        else
            self.m_Button.Text = LocalString.GetString("解绑")
        end
    end
end

function LuaOtherWelfareTemplate:UpdateInfo(sign)
    if self.m_AwardKey == "TeamReverse" then
        self.m_ButtonAlert:SetActive(sign)
    end
end

function LuaOtherWelfareTemplate:OnRefresbUnity2018UpgradeInfo()
    if self.m_AwardKey == "Unity2018GengXin" then
        self.m_ButtonAlert:SetActive(LuaWelfareMgr:CanShowUnity2018UpgradeAlert())
    end
end
