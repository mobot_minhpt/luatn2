local PlayerSettings = import "L10.Game.PlayerSettings"
local DelegateFactory = import "DelegateFactory"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"

LuaVoiceAchievementMgr = {}

LuaVoiceAchievementMgr.AchievementsInfo = {}
LuaVoiceAchievementMgr.LimitContentTime = 100

LuaVoiceAchievementMgr.s_IsOpen = true

LuaVoiceAchievementMgr.SelectAchievementId = 1

function LuaVoiceAchievementMgr.AddListener()
    if not LuaVoiceAchievementMgr.s_IsOpen then
        return
    end

    EventManager.RemoveListenerInternal(EnumEventType.PersonalSpaceAddNewMoment, LuaVoiceAchievementMgr.SendMomentDelegate)
    EventManager.AddListenerInternal(EnumEventType.PersonalSpaceAddNewMoment, LuaVoiceAchievementMgr.SendMomentDelegate)
end

LuaVoiceAchievementMgr.SendMomentDelegate = DelegateFactory.Action_string_CPersonalSpace_AdvRet(function (textAfterFilter, data)
    if CClientMainPlayer.Inst then
        local playerId = CClientMainPlayer.Inst.Id
        --Gac2Gas.OnSendMengDao(playerId,textAfterFilter)
    end 
end)

function LuaVoiceAchievementMgr.OpenAchievementWnd(vid)
    if not LuaVoiceAchievementMgr.s_IsOpen then
        return
    end
    
    LuaVoiceAchievementMgr.SelectAchievementId = vid
    CUIManager.ShowUI(CLuaUIResources.XinBaiVoiceAchivementWnd)
    --Gac2Gas.XinBaiAchv_QueryAchievementInfo()
end

function LuaVoiceAchievementMgr.OnSyncAchievementInfo(achievementInfo,markData)
    if not achievementInfo then return end
    local list = MsgPackImpl.unpack(achievementInfo)
    local marks = MsgPackImpl.unpack(markData)
    
    LuaVoiceAchievementMgr.AchievementsInfo = {}
    local needRedDot = false
    for i=0,list.Count-1,1 do
        local t = {}
        t.ID = tonumber(i+1)
        t.Status = tonumber(list[i])--0 未解锁 1 已分享世界 2未分享世界
        t.IsNew = (marks and marks.Count>i) and marks[i] or false
        if t.IsNew then
            needRedDot = true
        end
        table.insert(LuaVoiceAchievementMgr.AchievementsInfo,t)
    end

    --isloaded
    g_ScriptEvent:BroadcastInLua("SyncVoiceAchievementAlertInfo",needRedDot)
    g_ScriptEvent:BroadcastInLua("SyncVoiceAchievementInfo")
end

function LuaVoiceAchievementMgr.OnPlayerAddAchievement(achievementId)
    LuaVoiceAchievementMgr.UnLockedAchievementId = achievementId
    CUIManager.ShowUI(CLuaUIResources.UnLockVoiceAchievementWnd)
end

function LuaVoiceAchievementMgr:OnAudioBtnClick(id)
    LuaVoiceAchievementMgr:StopAudio()
    local data = VoiceAchievement_VoiceAchievement.GetData(id)
    if data then  
        local path = data.VoicePath
        if not PlayerSettings.SoundEnabled then
            local okfunc = function()
                PlayerSettings.SoundEnabled = true
                LuaVoiceAchievementMgr.m_Sound = SoundManager.Inst:PlayOneShot(path, Vector3.zero, nil, 0, -1)
            end
            local msg = g_MessageMgr:FormatMessage("NPCCHAT_SOUND_OPEN")
            g_MessageMgr:ShowOkCancelMessage(msg,okfunc,nil,nil,nil,false)
        else
            LuaVoiceAchievementMgr.m_Sound = SoundManager.Inst:PlayOneShot(path, Vector3.zero, nil, 0, -1)
        end
    end
end
function LuaVoiceAchievementMgr:StopAudio()
    if LuaVoiceAchievementMgr.m_Sound then
        SoundManager.Inst:StopSound(LuaVoiceAchievementMgr.m_Sound)
        LuaVoiceAchievementMgr.m_Sound = nil
    end
end

function LuaVoiceAchievementMgr:OpenPreviewWnd()
    CUIManager.ShowUI(CLuaUIResources.VoiceAchievemenAwardPreviewWnd)
end
