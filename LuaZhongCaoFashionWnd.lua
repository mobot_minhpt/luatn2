local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local CShopMallMgr=import "L10.UI.CShopMallMgr"
local CFashionPreviewMgr=import "L10.UI.CFashionPreviewMgr"
local EnumPreviewType=import "L10.UI.EnumPreviewType"
local CItemCountUpdate=import "L10.UI.CItemCountUpdate"
local CJingLingMgr=import "L10.Game.CJingLingMgr"
local Item_Item=import "L10.Game.Item_Item"
local CUITexture=import "L10.UI.CUITexture"
local QnAddSubAndInputButton=import "L10.UI.QnAddSubAndInputButton"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local DateTime=import "System.DateTime"
local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"

CLuaZhongCaoFashionWnd=class()
RegistClassMember(CLuaZhongCaoFashionWnd,"m_ZhongCaoButton")
RegistClassMember(CLuaZhongCaoFashionWnd,"m_BaCaoButton")
RegistClassMember(CLuaZhongCaoFashionWnd,"m_TableView")
RegistClassMember(CLuaZhongCaoFashionWnd,"m_DataList")
RegistClassMember(CLuaZhongCaoFashionWnd,"m_NumInput")

RegistClassMember(CLuaZhongCaoFashionWnd,"m_CountLookup")
RegistClassMember(CLuaZhongCaoFashionWnd,"m_CountUpdate")
RegistClassMember(CLuaZhongCaoFashionWnd,"m_HotLookup")
function CLuaZhongCaoFashionWnd:Init()
    self.m_CountLookup={}
    self.m_HotLookup={}

    self.m_TableView=FindChild(self.transform,"GoodsGrid"):GetComponent(typeof(QnTableView))
    self.m_TableView.m_DataSource=DefaultTableViewDataSource.Create(
        function()
            return #self.m_DataList
        end,function(item,index)
            self:InitItem(item.transform,self.m_DataList[index+1])
        end)
    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function(row)
        if self.m_CountUpdate.count>=1 then
            CUICommonDef.SetActive(self.m_ZhongCaoButton,true,true)
        else
            CUICommonDef.SetActive(self.m_ZhongCaoButton,false,true)
        end
    end)

    self.m_DataList={}
    -- for k,v in pairs(Lua_ZhongCao_OldFashion) do
    --     table.insert( self.m_DataList,v)
    -- end
    ZhongCao_OldFashion.ForeachKey(function(k)
        table.insert( self.m_DataList,ZhongCao_OldFashion.GetData(k))
    end)



    local zhongcaoLabelGo = FindChild(self.transform,"ZhongCaoLabel").gameObject
    local bacaoLabelGo = FindChild(self.transform,"BaCaoLabel").gameObject

    local zhaongcao = FindChild(self.transform,"ZhongCao")
    local bacao = FindChild(self.transform,"BaCao")
    self.m_ZhongCaoButton = FindChild(zhaongcao,"ZhongCaoButton").gameObject
    self.m_BaCaoButton = FindChild(bacao,"BaCaoButton").gameObject

    local time=CServerTimeMgr.Inst:GetZone8Time().Ticks
    local zcStartTime = DateTime(2018,11,1,0,0,0).Ticks
    local zcEndTime = DateTime(2018,11,6,22,0,0).Ticks


    local bcStartTime = DateTime(2018,11,7,0,0,0).Ticks
    local bcEndTime = DateTime(2018,11,11,23,59,0).Ticks

    local bacaoTipGo=FindChild(bacao,"TipLabel").gameObject

    if time<zcStartTime then
        --活动没开始
        zhaongcao.gameObject:SetActive(true)
        bacao.gameObject:SetActive(false)
        self:SetColor(zhongcaoLabelGo,false)
        self:SetColor(bacaoLabelGo,false)
    elseif time>=zcStartTime and time<=zcEndTime then
        --种草阶段
        zhaongcao.gameObject:SetActive(true)
        bacao.gameObject:SetActive(false)
        self:SetColor(zhongcaoLabelGo,true)
        self:SetColor(bacaoLabelGo,false)
    elseif time>zcEndTime and time<bcStartTime then
        --种草阶段结束
        zhaongcao.gameObject:SetActive(false)
        bacao.gameObject:SetActive(true)
        self:SetColor(zhongcaoLabelGo,false)
        self:SetColor(bacaoLabelGo,false)
        bacaoTipGo:SetActive(true)
        CUICommonDef.SetActive(self.m_BaCaoButton,false,true)
    elseif time>=bcStartTime and time<=bcEndTime then
        --拔草阶段
        zhaongcao.gameObject:SetActive(false)
        bacao.gameObject:SetActive(true)
        self:SetColor(zhongcaoLabelGo,false)
        self:SetColor(bacaoLabelGo,true)
        bacaoTipGo:SetActive(false)
        CUICommonDef.SetActive(self.m_BaCaoButton,true,true)
    elseif time>bcEndTime then
        --拔草阶段结束
        zhaongcao.gameObject:SetActive(false)
        bacao.gameObject:SetActive(false)
        self:SetColor(zhongcaoLabelGo,false)
        self:SetColor(bacaoLabelGo,false)
    end

    UIEventListener.Get(self.m_ZhongCaoButton).onClick=DelegateFactory.VoidDelegate(function(go)
        if self.m_TableView.currentSelectRow>-1 then
            local itemId = self.m_DataList[self.m_TableView.currentSelectRow+1].ItemId
            Gac2Gas.RequestZhongCao(itemId,self.m_NumInput.m_CurrentValue) 
        end
    end)
    UIEventListener.Get(self.m_BaCaoButton).onClick=DelegateFactory.VoidDelegate(function(go)
        self:OnClickBaCaoButton(go)
    end)

    self.m_NumInput = FindChild(zhaongcao,"QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
    self.m_NumInput:SetMinMax(0,0,1)
    self.m_NumInput:SetValue(0)


    local templateId = ZhongCao_Setting.GetData().CaoItemId
    local icon = FindChild(zhaongcao,"Icon"):GetComponent(typeof(CUITexture))
    local template = Item_Item.GetData(templateId)
    icon:LoadMaterial(template.Icon)
    UIEventListener.Get(icon.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        if self.m_CountUpdate.count>0 then
            CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false,nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
        else
            CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, true, nil, AlignType.Right)
        end
    end)

    self.m_CountUpdate = icon.gameObject:GetComponent(typeof(CItemCountUpdate))
    self.m_CountUpdate.templateId=templateId
    self.m_CountUpdate.format="{0}"
    local mask = icon.transform:Find("Mask").gameObject
    local function OnCountChange(val)
        if val>=1 then
            self.m_NumInput:SetMinMax(1,val,1)
            CUICommonDef.SetActive(self.m_ZhongCaoButton,true,true)
            mask:SetActive(false)
            self.m_CountUpdate.format="{0}"

        else
            self.m_NumInput:SetMinMax(0,0,1)
            CUICommonDef.SetActive(self.m_ZhongCaoButton,false,true)
            mask:SetActive(true)
            self.m_CountUpdate.format="[c][ff0000]{0}[-][/c]"
        end
    end
    self.m_CountUpdate.onChange=DelegateFactory.Action_int(OnCountChange)
    self.m_CountUpdate:UpdateCount()


    Gac2Gas.QueryAllZhongCaoCount()
    CUICommonDef.SetActive(self.m_ZhongCaoButton,false,true)
end

function CLuaZhongCaoFashionWnd:OnClickBaCaoButton(go)
    CShopMallMgr.SelectMallIndex = 0
    CShopMallMgr.SelectCategory = 6
    if self.m_TableView.currentSelectRow>0 then
        local itemId = self.m_DataList[self.m_TableView.currentSelectRow+1].ItemId
        CShopMallMgr.SearchTemplateId = itemId
    else
        CShopMallMgr.SearchTemplateId = 0
    end
    CUIManager.ShowUI(CUIResources.ShoppingMallWnd)
end

function CLuaZhongCaoFashionWnd:SetColor(go, show)
    local label = go:GetComponent(typeof(UILabel))
    local label1 = FindChild(go.transform,"Label1"):GetComponent(typeof(UILabel))
    local label2 = FindChild(go.transform,"Label2"):GetComponent(typeof(UILabel))
    local texture = FindChild(go.transform,"Texture"):GetComponent(typeof(UITexture))
    if show then
        label.color = Color(146/255,1,84/255)
        label1.color = Color(146/255,1,84/255)
        label2.color = Color(1,1,1)
        texture.color = Color(146/255,1,84/255)
    else
        label.color = Color(0.54,0.54,0.54)
        label1.color = Color(0.54,0.54,0.54)
        label2.color = Color(0.54,0.54,0.54)
        texture.color = Color(0.54,0.54,0.54)
    end
end

function CLuaZhongCaoFashionWnd:InitItem(transform,info)
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local priceLabel = transform:Find("PriceLabel"):GetComponent(typeof(UILabel))
    local count = self.m_CountLookup[info.ItemId] or 0
    priceLabel.text=tostring(count)
    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local hotSprite = transform:Find("HotSprite").gameObject
    if self.m_HotLookup[info.ItemId] then
        hotSprite:SetActive(true)
    else
        hotSprite:SetActive(false)
    end

    local template = Item_Item.GetData(info.ItemId)
    if template then
        icon:LoadMaterial(template.Icon)
        nameLabel.text=template.Name
    end


    local button = transform:Find("Button").gameObject
    UIEventListener.Get(button).onClick=DelegateFactory.VoidDelegate(function(go)
        CJingLingMgr.Inst:ShowJingLingWnd(info.KeyWords, "o_gamepage", true,false,nil)
    end)
    UIEventListener.Get(icon.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        local fashionId = info.FashionID
        if fashionId>0 then
            CFashionPreviewMgr.curPreviewType = EnumPreviewType.PreviewFashion;
            CFashionPreviewMgr.ShowFashionPreview(fashionId)
        end
    end)
end

function CLuaZhongCaoFashionWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryAllZhongCaoCountRet", self, "OnQueryAllZhongCaoCountRet")
    g_ScriptEvent:AddListener("PlayerZhongCaoRet", self, "OnPlayerZhongCaoRet")
    
end

function CLuaZhongCaoFashionWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryAllZhongCaoCountRet", self, "OnQueryAllZhongCaoCountRet")
    g_ScriptEvent:RemoveListener("PlayerZhongCaoRet", self, "OnPlayerZhongCaoRet")
end

function CLuaZhongCaoFashionWnd:OnQueryAllZhongCaoCountRet(progress,t)
    self.m_CountLookup=t
    table.sort( self.m_DataList, function(a,b)
        local itemId1 = a.ItemId
        local itemId2 = b.ItemId
        if t[itemId1]>t[itemId2] then
            return true
        elseif t[itemId1]<t[itemId2] then
            return false
        else
            return itemId1<itemId2
        end
    end )

    self.m_HotLookup={}
    local max = math.min(5, #self.m_DataList)
    for i=1,max do
        local itemId = self.m_DataList[i].ItemId
        local count = self.m_CountLookup[itemId] or 0
        if count > 500 then
            self.m_HotLookup[itemId] = true
        end
    end

    --拔草阶段只显示前5个
    if progress==2 then
        local temp={}
        for i=1,max do
            table.insert( temp,self.m_DataList[i] )
        end
        self.m_DataList=temp
    end

    
    self.m_TableView:ReloadData(false, false)

    local zhaongcao = FindChild(self.transform,"ZhongCao")
    local bacao = FindChild(self.transform,"BaCao")
    local zhongcaoLabelGo = FindChild(self.transform,"ZhongCaoLabel").gameObject
    local bacaoLabelGo = FindChild(self.transform,"BaCaoLabel").gameObject
    local bacaoTipGo=FindChild(bacao,"TipLabel").gameObject

    if progress==0 then
        --活动没开始
        zhaongcao.gameObject:SetActive(false)
        bacao.gameObject:SetActive(false)
        self:SetColor(zhongcaoLabelGo,false)
        self:SetColor(bacaoLabelGo,false)
    elseif progress==1 then
        --种草阶段
        zhaongcao.gameObject:SetActive(true)
        bacao.gameObject:SetActive(false)
        self:SetColor(zhongcaoLabelGo,true)
        self:SetColor(bacaoLabelGo,false)
    elseif progress==3 then
        --种草阶段结束
        zhaongcao.gameObject:SetActive(false)
        bacao.gameObject:SetActive(true)
        self:SetColor(zhongcaoLabelGo,false)
        self:SetColor(bacaoLabelGo,false)
        bacaoTipGo:SetActive(true)
        CUICommonDef.SetActive(self.m_BaCaoButton,false,true)
    elseif progress==2 then
        --拔草阶段
        zhaongcao.gameObject:SetActive(false)
        bacao.gameObject:SetActive(true)
        self:SetColor(zhongcaoLabelGo,false)
        self:SetColor(bacaoLabelGo,true)
        bacaoTipGo:SetActive(false)
        CUICommonDef.SetActive(self.m_BaCaoButton,true,true)
    end

end

function CLuaZhongCaoFashionWnd:OnPlayerZhongCaoRet(mallItemId, count)
    self.m_CountLookup[mallItemId] = (self.m_CountLookup[mallItemId] or 0) + count

    for i,v in ipairs(self.m_DataList) do
        if v.ItemId == mallItemId then
            self:InitItem(self.m_TableView:GetItemAtRow(i-1).transform,v)
            break
        end
    end

end