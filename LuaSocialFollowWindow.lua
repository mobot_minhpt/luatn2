local UIGrid = import "UIGrid"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"

LuaSocialFollowWindow = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSocialFollowWindow, "TemplateNode", "TemplateNode", GameObject)
RegistChildComponent(LuaSocialFollowWindow, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaSocialFollowWindow, "AllFollowNode", "AllFollowNode", GameObject)

--@endregion RegistChildComponent end

function LuaSocialFollowWindow:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    Gac2Gas.QueryVNClientAward()

    self.TemplateNode:SetActive(false)
    self:InitWndData()
    self:CreateSocialFollowItem()
end

function LuaSocialFollowWindow:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaSocialFollowWindow:InitWndData()
    self.socialKeyList = {"Facebook_Fanpage_SEA_SOCIAL_INFO", "Facebook_Group_SEA_SOCIAL_INFO", "Youtube_SEA_SOCIAL_INFO", "Discord_SEA_SOCIAL_INFO", "TikTok_SEA_SOCIAL_INFO"}
    self.socialData = {}
    for i = 1, #self.socialKeyList do
        local key = self.socialKeyList[i]
        local data = GameSetting_Client.GetData()[key]
        local dataSplit = g_LuaUtil:StrSplit(data, ";")
        local rewardItem = {}
        local itemSplit = g_LuaUtil:StrSplit(dataSplit[1], ",")
        rewardItem.itemId = tonumber(itemSplit[1])
        rewardItem.itemNum = tonumber(itemSplit[2])
        local socialTex = dataSplit[2]
        local socialName = dataSplit[3]
        local socialUrl = dataSplit[4]
        local awardKey = dataSplit[5]
        table.insert(self.socialData, {rewardItem = rewardItem, socialTex = socialTex, socialName = socialName, socialUrl = socialUrl, awardKey = awardKey})
    end
    
    local followAllData = GameSetting_Client.GetData().Total_SEA_SOCIAL_INFO
    local dataSplit = g_LuaUtil:StrSplit(followAllData, ";")
    local itemSplit = g_LuaUtil:StrSplit(dataSplit[1], ",")
    local rewardItem = {}
    rewardItem.itemId = tonumber(itemSplit[1])
    rewardItem.itemNum = tonumber(itemSplit[2])
    local socialName = dataSplit[2]
    local awardKey = dataSplit[3]
    self.followAllData = {rewardItem = rewardItem, socialName = socialName, awardKey = awardKey}
    
    self.rewardData = {}
    self.socialId2Go = {}
end 

function LuaSocialFollowWindow:OnEnable()
    g_ScriptEvent:AddListener("SyncVNClientAwardResult", self, "OnSyncVNClientAwardResult")
    g_ScriptEvent:AddListener("UpdateVNClientAwardStatus", self, "OnUpdateVNClientAwardStatus")
end 

function LuaSocialFollowWindow:OnDisable()
    g_ScriptEvent:RemoveListener("SyncVNClientAwardResult", self, "OnSyncVNClientAwardResult")
    g_ScriptEvent:RemoveListener("UpdateVNClientAwardStatus", self, "OnUpdateVNClientAwardStatus")
end

function LuaSocialFollowWindow:OnUpdateVNClientAwardStatus(actionType, status)
    self.rewardData[actionType] = status

    if self.socialId2Go then
        for k, v in pairs(self.socialId2Go) do
            local socialData = self.socialData[k]
            local go = v
            self:InitSocialFollowItem(go, socialData, false, false)
        end
        self:InitSocialFollowItem(self.AllFollowNode, self.followAllData, true, false)
    end
end

function LuaSocialFollowWindow:OnSyncVNClientAwardResult(dataU)
    self.rewardData = {}
    local tb = g_MessagePack.unpack(dataU)
    for k,v in pairs(tb) do
        self.rewardData[k] = v
    end
    

    for k, v in pairs(self.socialId2Go) do
        local socialData = self.socialData[k]
        local go = v
        self:InitSocialFollowItem(go, socialData, false, false)
    end
    self:InitSocialFollowItem(self.AllFollowNode, self.followAllData, true, false)
end 

function LuaSocialFollowWindow:CreateSocialFollowItem()
    CUICommonDef.ClearTransform(self.Grid.transform)
    self.socialId2Go = {}
    for i = 1, #self.socialData do
        local socialData = self.socialData[i]
        local go = NGUITools.AddChild(self.Grid.gameObject, self.TemplateNode)
        self:InitSocialFollowItem(go, socialData, false, true)
        self.socialId2Go[i] = go
        go:SetActive(true)
    end
    self.Grid:Reposition()

    self:InitSocialFollowItem(self.AllFollowNode, self.followAllData, true, true)
end

function LuaSocialFollowWindow:IsSocialFollowed(socialKey)
    return self.rewardData[socialKey] and self.rewardData[socialKey] == 2 or false
end

function LuaSocialFollowWindow:IsSocialReceived(socialKey)
    return self.rewardData[socialKey] and self.rewardData[socialKey] == 1 or false
end

function LuaSocialFollowWindow:InitSocialFollowItem(socialItemGo, socialData, isAllFollowItem, createListener)
    self:InitAwardItem(socialItemGo.transform:Find("ItemNode").gameObject, socialData.rewardItem.itemId, socialData.rewardItem.itemNum, self:IsSocialReceived(socialData.awardKey))
    socialItemGo.transform:Find("socialName"):GetComponent(typeof(UILabel)).text = socialData.socialName
    
    if isAllFollowItem then
        local totalSocial = #self.socialData
        local isFollowNumber = 0
        for i = 1, #self.socialData do
            if self:IsSocialFollowed((self.socialData[i].awardKey)) or self:IsSocialReceived((self.socialData[i].awardKey)) then
                isFollowNumber = isFollowNumber + 1
            end
        end

        local followText = socialItemGo.transform:Find("status/FollowText")
        local receiveBtn = socialItemGo.transform:Find("status/ReceiveBtn")
        local receivedText = socialItemGo.transform:Find("status/text")
        followText:GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%d/%d"), isFollowNumber, totalSocial)
        
        local isFollowMoreState = (isFollowNumber < totalSocial)
        local isMeCanReceive = (not self:IsSocialReceived(socialData.awardKey)) and (not isFollowMoreState)
        local isMeReceived = self:IsSocialReceived(socialData.awardKey) and (not isFollowMoreState)
        followText.gameObject:SetActive(isFollowMoreState)
        receiveBtn.gameObject:SetActive(isMeCanReceive)
        receivedText.gameObject:SetActive(isMeReceived)

        if createListener then
            UIEventListener.Get(receiveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                Gac2Gas.RequestVNClientAward(socialData.awardKey)
            end)
        end
    else
        local tex = socialItemGo.transform:Find("socialTex"):GetComponent(typeof(CUITexture))
        tex:LoadMaterial(socialData.socialTex)
        
        local isMeFollowed = self:IsSocialFollowed(socialData.awardKey)
        local isMeReceived = self:IsSocialReceived(socialData.awardKey)
        local isMeCanFollow = (not isMeFollowed) and (not isMeReceived)
        local followBtn = socialItemGo.transform:Find("status/FollowBtn")
        local receiveBtn = socialItemGo.transform:Find("status/ReceiveBtn")
        local receivedText = socialItemGo.transform:Find("status/text")
        followBtn.gameObject:SetActive(isMeCanFollow)
        receiveBtn.gameObject:SetActive(isMeFollowed)
        receivedText.gameObject:SetActive(isMeReceived)

        if createListener then
            UIEventListener.Get(followBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                CommonDefs.OpenURL(socialData.socialUrl)
                Gac2Gas.RequestVNClientFollow(socialData.awardKey)
            end)
            
            UIEventListener.Get(receiveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                Gac2Gas.RequestVNClientAward(socialData.awardKey)
            end)
        end
    end
end

function LuaSocialFollowWindow:InitAwardItem(itemObj, itemId, count, isReceived)
    if not itemObj then return end

    local icon = itemObj.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local amount = itemObj.transform:Find("AmountLable"):GetComponent(typeof(UILabel))
    local quality = itemObj.transform:Find("Quality"):GetComponent(typeof(UISprite))
    local item = Item_Item.GetData(itemId)
    if not item then return end

    icon:LoadMaterial(item.Icon)
    amount.text = tostring(count)
    quality.spriteName = CUICommonDef.GetItemCellBorder(item.NameColor)
    itemObj.transform:Find("CheckIcon").gameObject:SetActive(isReceived)

    CommonDefs.AddOnClickListener(itemObj, DelegateFactory.Action_GameObject(function (gameobject)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
    end), false)
end
