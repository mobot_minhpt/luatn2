local CHousePopupMgr = import "L10.UI.CHousePopupMgr"
local HousePopupMenuItemData = import "L10.UI.HousePopupMenuItemData"
--local MainPlayerDistanceChecker = import "L10.Game.MainPlayerDistanceChecker"
LuaHanJia2024Mgr = {}

function LuaHanJia2024Mgr:OnMainPlayerCreated(gameplayId)

    local cfg = HanJia2024_UpDownWorldPlayInfo.GetData(gameplayId)
    if not cfg then return end

    local showrule = function()
        g_MessageMgr:ShowMessage("UPDOWNWORLD_RULE")
    end
    local gettitle = function()
        return cfg.TaskName
    end
    local getcontent = function()
        return cfg.TaskDesc
    end
    
    LuaCommonGamePlayTaskViewMgr:AppendGameplayInfo(
        gameplayId,
        true,nil,
        false,
        true,gettitle,
        true,getcontent,
        true,nil,nil,
        true,nil,showrule)

    CUIManager.ShowUI(CLuaUIResources.UpDownWorldTopRightWnd)
    print("LuaHanJia2024Mgr.OnMainPlayerCreated",gameplayId)
end

--@region SnowMan
LuaHanJia2024Mgr.SnowManData = {}
LuaHanJia2024Mgr.SnowManData.IsWin = false --是否胜利
LuaHanJia2024Mgr.SnowManData.Type = 1 --难度类型：1普通；2困难
LuaHanJia2024Mgr.SnowManData.Rewarded = nil --是否领取过奖励

function LuaHanJia2024Mgr:OpenSnowManResultWndWithData(isHardMode, isWin, hasRewarded)
    LuaHanJia2024Mgr.SnowManData.IsWin = isWin
    LuaHanJia2024Mgr.SnowManData.Type = isHardMode and 2 or 1
    LuaHanJia2024Mgr.SnowManData.Rewarded = hasRewarded
    CUIManager.ShowUI(CLuaUIResources.SnowManResultWnd)
end

function LuaHanJia2024Mgr:OpenSnowManEnterWnd()
    CUIManager.ShowUI(CLuaUIResources.SnowManEnterWnd)
end

--@endregion

--@region UpDownWorld
LuaHanJia2024Mgr.UpDownWorldData = {}
LuaHanJia2024Mgr.UpDownWorldData.Tasks = {}
LuaHanJia2024Mgr.UpDownWorldData.NmlTasks = {}
LuaHanJia2024Mgr.UpDownWorldData.IsStart = false
LuaHanJia2024Mgr.UpDownWorldData.EndTime = 0

LuaHanJia2024Mgr.UpDownWorldData.Floor = 1
LuaHanJia2024Mgr.UpDownWorldData.Count = 0
LuaHanJia2024Mgr.UpDownWorldData.Total = 0
LuaHanJia2024Mgr.UpDownWorldData.Score = 0

function LuaHanJia2024Mgr:ReqUpDownWorldData()
    Gac2Gas.HanJia2024InvertedWorld_QueryPlayOpenInfo()
end

function LuaHanJia2024Mgr:SyncUpDownWorldNmlTaskData(weeklyMission, dailyMission)
    local datas = {}
    local tbs = {weeklyMission, dailyMission}
    for i = 1, #tbs do
        local tb = tbs[i]
        for id, info in pairs(tb) do
            local taskdata = {}
            taskdata.TaskID = id
            taskdata.Count = info.progress
            taskdata.State = info.rewarded
            table.insert(datas, taskdata)
        end
    end

    LuaHanJia2024Mgr.UpDownWorldData.NmlTasks = datas
    g_ScriptEvent:BroadcastInLua("OnSyncUpDownWorldNmlTaskData")
end

function LuaHanJia2024Mgr:SyncUpDownWorldTaskData(missionData)
    local datas = {}
    for id, info in pairs(missionData) do
        local taskdata = {}
        taskdata.TaskID = id
        taskdata.Count = info.progress
        taskdata.State = info.rewarded
        table.insert(datas, taskdata)
    end
    LuaHanJia2024Mgr.UpDownWorldData.Tasks = datas
    g_ScriptEvent:BroadcastInLua("OnSyncUpDownWorldTaskData")
end

function LuaHanJia2024Mgr:OpenUpDownWorldEnterWndWithData(bStart, endTime)
    LuaHanJia2024Mgr.UpDownWorldData.IsStart = bStart
    LuaHanJia2024Mgr.UpDownWorldData.EndTime = endTime
    CUIManager.ShowUI(CLuaUIResources.UpDownWorldEnterWnd)
end

function LuaHanJia2024Mgr:SyncUpDownWorldBattleInfo(floor, count, total)
    LuaHanJia2024Mgr.UpDownWorldData.Floor = floor
    LuaHanJia2024Mgr.UpDownWorldData.Count = count
    LuaHanJia2024Mgr.UpDownWorldData.Total = total
    g_ScriptEvent:BroadcastInLua("OnSyncUpDownWorldBattleInfo")
end

function LuaHanJia2024Mgr:SyncUpDownWorldBattleScore(value)
    LuaHanJia2024Mgr.UpDownWorldData.Score = value
    g_ScriptEvent:BroadcastInLua("OnSyncUpDownWorldBattleScore")
end

function LuaHanJia2024Mgr:SyncUnitMiniMapPos(posinfo,isnpc)
    print("LuaHanJia2024Mgr:SyncMiniMapPos",isnpc)
    for k,v in pairs(posinfo) do
        print(k,v)
    end

    local engineId = posinfo.engineId
    local tempid = posinfo.id

    local info = {
        x = Utility.GetGridByPixel(posinfo.x), 
        y = Utility.GetGridByPixel(posinfo.y), 
        size = {
            {width = 32, height = 32},
            {width = 75, height = 75},
        }
    }

    local data = nil
    if isnpc then 
        data = HanJia2024_UpDownWorldShowNpcInfo.GetData(tempid)
    else
        data = HanJia2024_UpDownWorldShowMonsterInfo.GetData(tempid)
    end
    if data then
        info.res = data.Icon
    end
    LuaGamePlayMgr:SetMiniMapMarkInfo(engineId, info, false)
    LuaGamePlayMgr:RefreshMiniMapMarks()
end

function LuaHanJia2024Mgr:SyncMiniMapPos(data,isnpc)
    for k,v in pairs(data) do
        self:SyncUnitMiniMapPos(v,isnpc)
    end
end

function LuaHanJia2024Mgr:ShowUnDownWorldFx(UIType)
    g_ScriptEvent:BroadcastInLua("OnSyncFxEvent",UIType)
end

--@endregion
