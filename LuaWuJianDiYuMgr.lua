local CScene = import "L10.Game.CScene"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"

LuaWuJianDiYuMgr = class()
RegistClassMember(LuaWuJianDiYuMgr, "m_PlayerImprismInfoCache")
RegistClassMember(LuaWuJianDiYuMgr, "m_JieTuoGuoInfoCache")
RegistClassMember(LuaWuJianDiYuMgr, "m_TopRightWindow")
LuaWuJianDiYuMgr.m_BossInfoCache = {}
LuaWuJianDiYuMgr.inited = false
LuaWuJianDiYuMgr.gamePlayId = 51101118 --无间地狱玩法id
LuaWuJianDiYuMgr.m_JieTuoGuoNum = 8 --解脱果数量，用于启动合成界面
LuaWuJianDiYuMgr.m_StageJieTuoGuoNum = {}
LuaWuJianDiYuMgr.HellStage = -1
LuaWuJianDiYuMgr.NewLevel = false
LuaWuJianDiYuMgr.CollectedJieTuoGuoNum = 0
LuaWuJianDiYuMgr.m_BossIds = nil
LuaWuJianDiYuMgr.m_HeChengAnimPlayed = false
LuaWuJianDiYuMgr.m_PinDataCache = nil
LuaWuJianDiYuMgr.m_TeamChangeAction = nil

function LuaWuJianDiYuMgr:OnSyncHellJieTuoGuoNum(_num, _totalNum, _level, t, boss_dic)
    LuaWuJianDiYuMgr.CollectedJieTuoGuoNum = _num
    LuaWuJianDiYuMgr.m_JieTuoGuoNum = _totalNum
    if(LuaWuJianDiYuMgr.HellStage == -1)then
        LuaWuJianDiYuMgr.HellStage = _level
        self.m_HeChengAnimPlayed = false
    end
    self.m_PlayerImprismInfoCache = t
    LuaWuJianDiYuMgr.m_BossInfoCache = boss_dic



    --当位于第二层收集满解脱果时（或实时数据已进入第三层但层数数据尚未进入）播放合成动画
    --(self.m_JieTuoGuoInfoCache.num == self.m_JieTuoGuoInfoCache.totalNum and LuaWuJianDiYuMgr.HellStage == 2) or 
    if ( (not self.m_HeChengAnimPlayed) and ((_level == 3 and LuaWuJianDiYuMgr.HellStage == 2))) then
        LuaWuJianDiYuMgr.m_JieTuoGuoNum = 8
        CUIManager.ShowUI(CLuaUIResources.WuJianDiYuHeChengWnd)
        self.m_HeChengAnimPlayed = true
    end

    if(_num < 0 or self.m_HeChengAnimPlayed)then
        _num = 8 
    end
    self.m_JieTuoGuoInfoCache = {num = self:CalcJieTuoGuoCollectNum(_num, LuaWuJianDiYuMgr.HellStage), totalNum = self:GetJieTuoGuoNum(LuaWuJianDiYuMgr.HellStage)}
    CLuaMiniMapPopWnd:NotifyUpdateHellInfos()
    if(self.m_TopRightWindow)then
        self.m_TopRightWindow:NotifyUpdateHellInfos()
    end
end

function LuaWuJianDiYuMgr:OnSyncPinPoints(_pin_data)
    self.m_PinDataCache = _pin_data
    CLuaMiniMapPopWnd:NotifyUpdatePinInfos()
    CLuaMiniMap:UpdateWuJianDiYuPinPoints()
end

function LuaWuJianDiYuMgr:GetJieTuoGuoNum(_stage)
    self:InitSettingData()
    if(_stage < 3)then
        return LuaWuJianDiYuMgr.m_StageJieTuoGuoNum[_stage]
    else
        return 0
    end
end

function LuaWuJianDiYuMgr:CalcJieTuoGuoCollectNum(_num, _stage)
    if(_stage == 2)then -- 目前只有第二层的解脱果数量需要计算
        return _num -  LuaWuJianDiYuMgr:GetJieTuoGuoNum(_stage - 1)
    else
        return _num
    end
end

function LuaWuJianDiYuMgr:InitSettingData()
    if(not self.inited)then
        local settings = UnintermittentHell_Setting.GetData()
        LuaWuJianDiYuMgr.m_StageJieTuoGuoNum = {settings.NumPick[0], settings.NumPick[1], 0}
        LuaWuJianDiYuMgr.m_BossIds = {settings.CatchBossTempId[0], settings.CatchBossTempId[1]}
    end
end

function LuaWuJianDiYuMgr:ReleaseSettingData()
    LuaWuJianDiYuMgr.m_StageJieTuoGuoNum = {}
    LuaWuJianDiYuMgr.m_BossIds = nil
end

function LuaWuJianDiYuMgr:OnMainPlayerCreated()
    local gamePlayId=CScene.MainScene.GamePlayDesignId
    if (gamePlayId == self.gamePlayId) then --无间地狱， 初始化事件及变量
        self.CollectedJieTuoGuoNum = 0
        --self.HellStage = -1
        self.m_HeChengAnimPlayed = false
        self:InitSettingData()
        CLuaMiniMap:InitWuJianDiYu()
        --CLuaGuideMgr.TryTriggerWuJianDiYuGuide()
        self.inited = true;

        if(not self.m_TeamChangeAction)then
            self.m_TeamChangeAction = DelegateFactory.Action(
                function()
                    CLuaMiniMapPopWnd:NotifyUpdatePinInfos()
                    CLuaMiniMap:UpdateWuJianDiYuPinPoints()
                end
            )
        end
        EventManager.AddListener(EnumEventType.TeamInfoChange, self.m_TeamChangeAction)
    end
end

function LuaWuJianDiYuMgr:OnMainPlayerDestroyed()
    if(self.inited)then
        self.inited = false
        if(self.m_TeamChangeAction)then
            EventManager.RemoveListener(EnumEventType.TeamInfoChange, self.m_TeamChangeAction)
            self.m_TeamChangeAction = nil
        end
        CLuaMiniMap:FreePinPoints()
        self.m_PlayerImprismInfoCache = nil
        self.m_JieTuoGuoInfoCache = nil
        self.m_PinDataCache = nil
        self.m_BossInfoCache = {}
        self:ReleaseSettingData()
    end
end

function LuaWuJianDiYuMgr:OnEnterHellLevel(new_level)
    LuaWuJianDiYuMgr.HellStage = new_level
    LuaWuJianDiYuMgr.NewLevel = true
    self.m_HeChengAnimPlayed = false
    if(new_level == 1)then
        local gamePlayId=CScene.MainScene.GamePlayDesignId
        local mCurrentGuidetimes = 0
        if(CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eUnintermittentHellGuideCount))then
            mCurrentGuidetimes = tonumber(CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eUnintermittentHellGuideCount).StringData)
        end
        local settings = UnintermittentHell_Setting.GetData()
        if(tonumber(mCurrentGuidetimes) < tonumber(settings.GuideCount))then
        --if(tonumber(mCurrentGuidetimes) < 30)then
            self:ShowRuleWnd()
        end
    end
    if(self.m_TopRightWindow)then
        self.m_TopRightWindow:NotifyEnterHellStage()
    end
end

function LuaWuJianDiYuMgr:ShowRuleWnd()
    local imagePaths = {
        "UI/Texture/NonTransparent/Material/WuJianDiYu_Rule_1.mat",
        "UI/Texture/NonTransparent/Material/WuJianDiYu_Rule_2.mat",
        "UI/Texture/NonTransparent/Material/WuJianDiYu_Rule_3.mat",
    }
    local msgs = {
        g_MessageMgr:FormatMessage("WuJianDiYu_Rule_1"),
        g_MessageMgr:FormatMessage("WuJianDiYu_Rule_2"),
        g_MessageMgr:FormatMessage("WuJianDiYu_Rule_3"),
    }
    LuaImageRuleMgr:ShowCommonImageRuleWnd(imagePaths, msgs)
end
