
--延迟加载玩法管理器，玩家进入该id的玩法，会主动调用对应playmgr的OnMainPlayerCreated
--离开时会调用OnMainPlayerDestroyed方法
function TryRegisterAllGamePlay()
    if LuaGamePlayMgr.s_PlayId2PlayMgr then return end
    --------------------------------------------------------
    --[[
        Gameplay_Gameplay.HideTopRightMenu字段可以隐藏topright界面
        自定义的topright界面可以在管理器类的OnMainPlayerCreated方法中创建
        不用写到CTopAndRightMenuWnd里面去
    --]]
    local TopAndRightUIRegister_GamePlay = 
    {
        [YuanDan2023_YuanDanQiYuan.GetData().GamePlayId] = "YuanDan2023YuanDanQiYuanTopRightWnd",
        [JiuGeShiHun_Setting.GetData().GamePlayId] = "JiuGeShiHunTopAndRightWnd",
        [Duanwu2021_LongZhouSetting.GetData().GamePlayId] = "DuanWu2021TopRightWnd",
        [LiuYi2021_Setting.GetData().GameplayId] = "LiuYi2021TangGuoPlayingWnd",
        [ShuJia_TravestyRole.GetData().GameplayID] = "DaMoWangPlayingWnd",
        [ShuiGuoPaiDui_Setting.GetData().GamePlayId] = "FruitPartyStateWnd",
        [51102185] = "HMLZPlayingWnd",
        --[51102271] = "LuoCha2021PlayingWnd",
        [ZhaiXing_Setting.GetData().GamePlayId] = "ZhaiXingStateWnd",
        [Double11_Setting.GetData().KanYiDao_GamePlayId] = "KanYiDaoPlayTopRightWnd",
        [YuanDan2022_Setting.GetData().FuDanGameplayId] = "YuanDan2022FuDanTopRightWnd",
        [Christmas2021_XingYu.GetData().GamePlayId] = "SDXYTopRightWnd",
        [LuaHanJiaMgr:GetXueJingKuangHuanPlayId()] = "XueJingKuangHuanStateWnd",
        [51102393] = "RunningWildTangYuanTopRightWnd",
        [QingMing2022_PVESetting.GetData().GameplayId] = "YinHunPoZhiTopRightWnd",
        [51102651] = "WuYi2022TopRightWnd",
        [DuanWu_Setting.GetData().BubbleGameplayId] = "LiuYi2022PopoPlayingWnd",
        [CuJu_Setting.GetData().GamePlayId] = "CuJuMinimap",
        [Double11_MQHX.GetData().PlayDesignId] = "MengQuanHengXingTopRightWnd",
        [Double11_SZTB.GetData().PlayDesignId] = "ShenZhaiTanBaoTopRightWnd",
        [Halloween2022_Setting.GetData().TrafficLightPlayId] = "HwMuTouRenTopRightWnd",
        [LuaShiTuMgr:GetFruitPartyTaskPlayId()] = "ShiTuFruitPartyTaskStateWnd",
        [LuaShiTuMgr:GetFruitPartyNormalPlayId()] = "ShiTuFruitPartyNormalStateWnd",
        [51102271] = {ui = "LuoCha2021GameplayBtnWnd", showContentNode = true},    -- 要求显示原topright界面可以采用这种写法
        [51102268] = {ui = "LuoCha2021GameplayBtnWnd", showContentNode = true},
        [DaFuWeng_Setting.GetData().GamePlayId] = "DaFuWongMainPlayWnd",
        [LuaQingMing2023Mgr:GetPVPGamePlayId()] = "QingMing2023PVPTopRightWnd",
        [Duanwu2023_PVPVESetting.GetData().GamePlayId] = "DuanWu2023PVPVETopRightWnd",
        [WuYi2023_FenYongZhenXian.GetData().GamePlayId] = "WuYi2023FYZXTopRightWnd",
        [LuaLiuYi2023Mgr:GetDDXGGamePlayId()] = "LiuYi2023DaoDanXiaoGuiStateWnd",
        [LuaCarnival2023Mgr:GetPVEBossGamePlayId()] = "Carnival2023PVEPlayerCountWnd",
        [LuaZhongYuanJie2023Mgr:GetGamePlayId(1)] = "ZYPDStateWnd",
        [LuaZhongYuanJie2023Mgr:GetGamePlayId(2)] = "ZYPDStateWnd",
        [LuaYaoYeManJuanMgr:GetGamePlayId()] = "YaoYeManJuanStateWnd",
        [Double11_MQLD.GetData().GamePlayId] = "MengQuan2023TopRightWnd",
        [Double11_BYDLY.GetData().GamePlayId] = "BaoYuDuoYu2023TopRightWnd",
        [Double11_BYDLY.GetData().HeroGamePlayId] = "BaoYuDuoYu2023TopRightWnd",
        [HengYuDangKou_Setting.GetData().GamePlayId] = "HengYuDangKouPlayingWnd",
        [Christmas2023_BingXuePlay.GetData().GameplayId] = "Christmas2023BingXuePlayPlayingWnd",
        [YuanDan2024_Setting.GetData().XNCGGamePlayId] = "YuanDan2024PVEGridWnd",
    }
    LuaTopAndRightMenuWndMgr.s_PlayId2TopAndRightWnd = TopAndRightUIRegister_GamePlay
    --------------------------------------------------------
    -- 通用任务栏逻辑
    --------------------------------------------------------

    local mgr = {
        [51100871] = "CLuaLianLianKanMgr",
        [51103057] = "LuaGaoChangJiDouMgr",
        [YuanXiao2023_NaoYuanXiao.GetData().GameplayId] = "LuaYuanXiaoDefenseMgr",
        [Christmas2022_Setting.GetData().GamePlayId] = "LuaChristmas2022Mgr",
        [ChunJie_TTSYSetting.GetData().PlayDesignId] = "LuaChunJie2023Mgr",
        [ChunJie_SLTTSetting.GetData().PlayDesignId] = "LuaChunJie2023Mgr",
        [GuildOccupationWar_Setting.GetData().GamePlayID] = "LuaJuDianBattleMgr",
        [GuildOccupationWar_DailySetting.GetData().GamePlayId] = "LuaJuDianBattleMgr",
        [DaFuWeng_Setting.GetData().GamePlayId] = "LuaDaFuWongMgr",
        [QingMing2023_PVPSetting.GetData().GamePlayId] = "LuaQingMing2023Mgr",
 		[QingMing2023_PVESetting.GetData().GameplayId] = "LuaQingMing2023Mgr",
        [WuYi2023_FenYongZhenXian.GetData().GamePlayId] = "LuaWuYi2023Mgr",
        [51103222] = "LuaArenaJingYuanMgr",
        [LuaLiuYi2023Mgr:GetDDXGGamePlayId()] = "LuaLiuYi2023Mgr",
        [LuaLiuYi2023Mgr:GetBossGamePlayId()] = "LuaLiuYi2023Mgr",
        [LuaGuoQing2023Mgr:GetJingYuanPlayGamePlayId()] = "LuaGuoQing2023Mgr",
        [LuaCarnival2023Mgr:GetPVENormalGamePlayId()] = "LuaCarnival2023Mgr",
        [LuaCarnival2023Mgr:GetPVEBossGamePlayId()] = "LuaCarnival2023Mgr",
        [LuaYaoYeManJuanMgr:GetGamePlayId()] = "LuaYaoYeManJuanMgr",
        [HengYuDangKou_Setting.GetData().GamePlayId] = "LuaHengYuDangKouMgr",
        [51103325] = "LuaHalloween2023Mgr",
        [Double11_BYDLY.GetData().GamePlayId] = "LuaShuangshiyi2023Mgr",
        [Double11_BYDLY.GetData().HeroGamePlayId] = "LuaShuangshiyi2023Mgr",
    }

    HanJia2024_UpDownWorldPlayInfo.Foreach(function(k,v)
        mgr[k] = "LuaHanJia2024Mgr"
    end)

    HanJia2023_XuePoLiXianLevel.Foreach(function(_, v)
        mgr[v.GameplayId] = "LuaHanJia2023Mgr"
    end)
    HanJia2023_BingTianShiLian.Foreach(function(_, v)
        mgr[v.GameplayId] = "LuaHanJia2023Mgr"
    end)

    LiYuZhangDaiFood_Ingredients.Foreach(function(_, v)
        mgr[v.CopyId] = "LuaYiRenSiMgr"
    end)
    

    --一个玩法管理器可能对应多个玩法，比如海战玩法，包括船上场景、海上场景
    local d = NavalWar_Setting.GetData()
    mgr[d.GamePlayId] = "LuaHaiZhanMgr"
    mgr[d.CabinGamePlayId] = "LuaHaiZhanMgr"
    mgr[d.TrainingSeaPlayId] = "LuaHaiZhanMgr"
    mgr[d.TrainingCabinPlayId] = "LuaHaiZhanMgr"
    mgr[d.TrainingPrePlayId] = "LuaHaiZhanMgr"
    mgr[d.PvpSeaPlayId] = "LuaHaiZhanMgr"
    mgr[d.PvpCabinPlayId] = "LuaHaiZhanMgr"
    LuaGamePlayMgr.s_PlayId2PlayMgr = mgr
end

-- 基于公共场景的玩法
function TryRegisterAllGamePlay_CommonScene()
    if LuaGamePlayMgr.s_SceneTemplateId2PlayMgr then return end
    local TopAndRightUIRegister_Scene =
    {
    }
    LuaTopAndRightMenuWndMgr.s_SceneTemplateId2TopAndRightWnd = TopAndRightUIRegister_Scene

    local mgr = {
        [16000348] = "LuaMeiXiangLouMgr",
        [16000197] = "CLuaXuanZhuanMuMaMgr",
        [16102358] = "LuaJuDianBattleMgr",
        [Valentine2023_YWQS_Setting.GetData().PlayMapId] = "LuaValentine2023Mgr"
    }
    LuaGamePlayMgr.s_SceneTemplateId2PlayMgr = mgr

    local SelectionWndUIRegister_GamePlay = 
    {
        [NavalWar_Setting.GetData().TrainingSeaPlayId] = "HaiZhanPosSelectionWnd",
        [NavalWar_Setting.GetData().PveSeaPlayId] = "HaiZhanPosSelectionWnd",
        [NavalWar_Setting.GetData().PvpSeaPlayId] = "HaiZhanPosSelectionWnd",
    }
    --进入玩法的选择界面
    LuaGamePlayMgr.s_PlayId2SelectionWnd = SelectionWndUIRegister_GamePlay
end