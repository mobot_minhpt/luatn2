local QnRadioBox = import "L10.UI.QnRadioBox"
local UIGrid = import "UIGrid"
local LocalString = import "LocalString"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local NGUITools = import "NGUITools"
local GameLauncher = import "L10.Engine.GameLauncher"
local DelegateFactory = import "DelegateFactory"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CLoginMgr = import "L10.Game.CLoginMgr"
local EBloomEffectMode = import "L10.Engine.EBloomEffectMode"
local CPostEffectMgr = import "L10.Engine.CPostEffectMgr"
local CPostProcessingMgr = import "L10.Engine.PostProcessing.CPostProcessingMgr"
local String = import "System.String"
local QnCheckBoxGroup = import "L10.UI.QnCheckBoxGroup"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CTooltip = import "L10.UI.CTooltip"
local Setting = import "L10.Engine.Setting"

LuaSettingWnd_RenderSetting = class()

RegistClassMember(LuaSettingWnd_RenderSetting, "m_BloomSettingsRadioBox")
RegistClassMember(LuaSettingWnd_RenderSetting, "m_BloomSettingsGrid")
RegistClassMember(LuaSettingWnd_RenderSetting, "m_QnSelectableButtonTel")
RegistClassMember(LuaSettingWnd_RenderSetting, "m_GameplayOptimizationButton")
RegistClassMember(LuaSettingWnd_RenderSetting, "m_RenderDetailSettingsCheckBoxGroup")
RegistClassMember(LuaSettingWnd_RenderSetting, "m_EBloomEffectModes")
RegistClassMember(LuaSettingWnd_RenderSetting, "m_DetailSettings")

function LuaSettingWnd_RenderSetting:Awake()
    local t = {
        {title = LocalString.GetString("水面反射")      ,propertyName = "WaterReflectionEnabled"},
        {title = LocalString.GetString("人物扰动草")    ,propertyName = "PlayerTouchGrassEnabled"},
        {title = LocalString.GetString("高精度贴图")    ,propertyName = "HighResTextureEnabled"},
        {title = LocalString.GetString("高帧率模式")    ,propertyName = "HighFrameRateEnabled"},
        {title = LocalString.GetString("屏蔽NPC")       ,propertyName = "HideHpcEnabled"},
        {title = LocalString.GetString("屏蔽灵兽召唤物"),propertyName = "HidePetEnabled"},
        {title = LocalString.GetString("屏蔽名字")      ,propertyName = "HideHeadInfoEnabled"},
        {title = LocalString.GetString("屏蔽技能特效")  ,propertyName = "HideSkillEffect"},
        {title = LocalString.GetString("屏蔽神兵特效")  ,propertyName = "HideShenbingEffect"},
        {title = LocalString.GetString("屏蔽角色转头")  ,propertyName = "HideIKLookAtCamera"},
        {title = LocalString.GetString("屏蔽校色")      ,propertyName = "ColorGradingDisabled", msg = "Setting_Tip_Color_Grading"},
        {title = LocalString.GetString("关闭模型法线")   ,propertyName = "ModelNormalMapDisabled", msg = "Setting_Tip_Model_Normal_Map"},
    }

    -- 开启新登录界面后，启用“非队友玩家精简外观”的设置
    if Setting.sDengLu_Level ~= "dengzhongshijie" then
        t = {
            {title = LocalString.GetString("水面反射")      ,propertyName = "WaterReflectionEnabled"},
            {title = LocalString.GetString("人物扰动草")    ,propertyName = "PlayerTouchGrassEnabled"},
            {title = LocalString.GetString("高精度贴图")    ,propertyName = "HighResTextureEnabled"},
            {title = LocalString.GetString("高帧率模式")    ,propertyName = "HighFrameRateEnabled"},
            {title = LocalString.GetString("非队友玩家精简面容")    ,propertyName = "SimplifyOtherPlayerFacialEnabled"},
            {title = LocalString.GetString("屏蔽NPC")       ,propertyName = "HideHpcEnabled"},
            {title = LocalString.GetString("屏蔽灵兽召唤物"),propertyName = "HidePetEnabled"},
            {title = LocalString.GetString("屏蔽名字")      ,propertyName = "HideHeadInfoEnabled"},
            {title = LocalString.GetString("屏蔽技能特效")  ,propertyName = "HideSkillEffect"},
            {title = LocalString.GetString("屏蔽神兵特效")  ,propertyName = "HideShenbingEffect"},
            {title = LocalString.GetString("屏蔽角色转头")  ,propertyName = "HideIKLookAtCamera"},
            {title = LocalString.GetString("屏蔽校色")      ,propertyName = "ColorGradingDisabled", msg = "Setting_Tip_Color_Grading"},
            {title = LocalString.GetString("关闭模型法线")   ,propertyName = "ModelNormalMapDisabled", msg = "Setting_Tip_Model_Normal_Map"},
        }
    end

    self.m_DetailSettings = {}
    for i,v in ipairs(t) do
        table.insert( self.m_DetailSettings,v )
    end
    self.m_EBloomEffectModes ={[0] = EBloomEffectMode.eOff, [1] = EBloomEffectMode.eMediun, [2] = EBloomEffectMode.eStrong, [3] = EBloomEffectMode.eDelicate}
    self:InitComponents()
    self:Init()
end

function LuaSettingWnd_RenderSetting:OnEnable()
    self:LoadData()
    g_ScriptEvent:AddListener("ReloadSettings", self, "LoadData")
end

function LuaSettingWnd_RenderSetting:OnDisable()
    g_ScriptEvent:RemoveListener("ReloadSettings", self, "LoadData")
end


function LuaSettingWnd_RenderSetting:InitComponents()
    local BloomSettings = {LocalString.GetString("默认"),LocalString.GetString("柔和"),LocalString.GetString("鲜艳"),LocalString.GetString("精致")}
    self.m_BloomSettingsRadioBox = self.transform:Find("RenderSettings/BloomSettings"):GetComponent(typeof(QnRadioBox))
    self.m_BloomSettingsGrid = self.m_BloomSettingsRadioBox.transform:Find("Grid"):GetComponent(typeof(UIGrid))
    self.m_QnSelectableButtonTel = self.m_BloomSettingsGrid.transform:Find("QnSelectableButtonTel").gameObject
    self.m_QnSelectableButtonTel:SetActive(false)
    for i = 1,3 do
        local button = NGUITools.AddChild(self.m_BloomSettingsGrid.gameObject, self.m_QnSelectableButtonTel)
        button:SetActive(true)
        button.transform:GetComponent(typeof(QnSelectableButton)).Text = BloomSettings[i]
    end
    if GameLauncher.HDRSupport then
        local button = NGUITools.AddChild(self.m_BloomSettingsGrid.gameObject, self.m_QnSelectableButtonTel)
        button:SetActive(true)
        button.transform:GetComponent(typeof(QnSelectableButton)).Text  = BloomSettings[4]
    end

    self.m_GameplayOptimizationButton = self.transform:Find("RenderSettings/GameplaySettings/GameplayOptimizationButton"):GetComponent(typeof(QnSelectableButton))

    self.m_RenderDetailSettingsCheckBoxGroup = self.transform:Find("RenderDetailSettings/QnCheckBoxGroup"):GetComponent(typeof(QnCheckBoxGroup))

    UIEventListener.Get(self.transform:Find("RenderSettings/GameplaySettings/TitleLabel/InfoButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go) 
        CTooltip.ShowAtTop(g_MessageMgr:FormatMessage("Setting_Tip_Gameplay_Optimization"), go.transform)
    end)

    UIEventListener.Get(self.transform:Find("RenderDetailSettings/TitleLabel/InfoButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go) 
        CTooltip.ShowAtTop(g_MessageMgr:FormatMessage("Setting_Tip_Render_Detail_Setting"), go.transform)
    end)
end

function LuaSettingWnd_RenderSetting:Init()
    self.m_BloomSettingsGrid:Reposition()

    --避免打开页面就回调接口
    self.m_BloomSettingsRadioBox:Awake()
    self.m_BloomSettingsRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSelectBloomSettingsRadioBox(btn, index)
    end)

    self.m_GameplayOptimizationButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnGameplayOptimizationClicked(btn)
    end)

    local detailtitles_buf = {}
    for i,v in ipairs(self.m_DetailSettings) do
        table.insert(detailtitles_buf, v.title)
    end
    local detailtitles = Table2Array(detailtitles_buf, MakeArrayClass(String))
    self.m_RenderDetailSettingsCheckBoxGroup:InitWithOptions(detailtitles, true, false)

    self.m_RenderDetailSettingsCheckBoxGroup.OnSelect = DelegateFactory.Action_QnCheckBox_int(function(checkbox,level)
        self:OnRenderDetailSettingsSelect(level)
    end)

    self:LoadData()
end


function LuaSettingWnd_RenderSetting:OnSelectBloomSettingsRadioBox(btn,index)
    
    --低端设备上禁止开启选项
    if PlayerSettings.IsMonitoredProperty("BloomSetting") and PlayerSettings.IsLowPerformanceDevice then
        if index~=0 then
            PlayerSettings.ShowForbiddenSettingMessage()
        end
        return
    end
    
    PlayerSettings.BloomSetting = index
    CLoginMgr.Inst:LogBloomSettingsInfo(false)
end

function LuaSettingWnd_RenderSetting:OnGameplayOptimizationClicked(btn)
    PlayerSettings.GameplayOptimizationEnabled = self.m_GameplayOptimizationButton:isSeleted()
end

function LuaSettingWnd_RenderSetting:LoadData()
    self.m_BloomSettingsRadioBox:ChangeTo(CLuaPlayerSettings.GetValueForSettingWnd("BloomSetting"), false)
    self.m_GameplayOptimizationButton:SetSelected(PlayerSettings.GameplayOptimizationEnabled, false)

    for i = 1,#self.m_DetailSettings do
        local option = self.m_DetailSettings[i]
        local checkbox = self.m_RenderDetailSettingsCheckBoxGroup[i - 1]
        checkbox.Selected = CLuaPlayerSettings.GetValueForSettingWnd(option.propertyName)
        local infoButton = checkbox.transform:Find("InfoButton")
        if infoButton then
            infoButton.gameObject:SetActive(option.msg~=nil)
            if option.msg then
                UIEventListener.Get(infoButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                    CTooltip.ShowAtTop(g_MessageMgr:FormatMessage(option.msg), go.transform)
                end)
            end
        end
    end
end

function LuaSettingWnd_RenderSetting:OnRenderDetailSettingsSelect(level)
    local option = self.m_DetailSettings[level + 1]
    local enable = self.m_RenderDetailSettingsCheckBoxGroup[level].Selected
    if option.propertyName == "HighResTextureEnabled" then
        self:ConfirmLowResTextureSetting(self.m_RenderDetailSettingsCheckBoxGroup[level], option.propertyName, enable)
    elseif option.propertyName == "HighFrameRateEnabled" then
        self:ConfirmHighFrameRateSetting(self.m_RenderDetailSettingsCheckBoxGroup[level], option.propertyName, enable)
     elseif PlayerSettings.IsMonitoredProperty(option.propertyName) and PlayerSettings.IsLowPerformanceDevice then
        --先处理校色，其他新增监控也类似处理一下
        if option.propertyName == "ColorGradingDisabled" and not enable then
            PlayerSettings.ShowForbiddenSettingMessage()
        end
    else
        PlayerSettings[option.propertyName] = enable
        if option.propertyName == "HideHeadInfoEnabled" then
            EventManager.BroadcastInternalForLua(EnumEventType.OnHeadInfoOptionChangeBySetting, {})
        end
    end
end

function LuaSettingWnd_RenderSetting:ConfirmLowResTextureSetting(checkbox, propertyName, enabled)
    local msg = nil

    if enabled then
        msg = LocalString.GetString("高精度贴图模式将占用大量系统资源，部分机型有可能造成闪退。设置后需要重启游戏才能生效。是否确认修改？")
    else
        msg = LocalString.GetString("取消高精度贴图模式需要重启游戏才能生效。是否确认修改？")
    end

    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
        PlayerSettings[propertyName] = enabled
    end), DelegateFactory.Action(function ()
        checkbox:SetSelected(not enabled, true)
    end), nil, nil, false)
end

function LuaSettingWnd_RenderSetting:ConfirmHighFrameRateSetting(checkbox, propertyName, enabled)
    local msg = nil

    if enabled then
        msg = g_MessageMgr:FormatMessage("Open_High_Frame_Rate_Mode")
    else
        msg = g_MessageMgr:FormatMessage("Close_High_Frame_Rate_Mode")
    end

    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
        PlayerSettings[propertyName] = enabled
    end), DelegateFactory.Action(function ()
        checkbox:SetSelected(not enabled, true)
    end), nil, nil, false)
end

