local CPackageMoneyInfoView = import "L10.UI.CPackageMoneyInfoView"
local CTooltip              = import "L10.UI.CTooltip"

CPackageMoneyInfoView.m_hookOnYinPiaoArrowClick = function(this, go)
    this.yinpiaoArrow.flip = UIBasicSprite.Flip.Nothing
    local moneyTypeList = {EnumMoneyType_lua.YinPiao, EnumMoneyType_lua.SkillPrivateSilver}
    LuaOwnMoneyListMgr:ShowOwnMoneyListWnd(moneyTypeList, go, CTooltip.AlignType.Top, function()
        if this and this.yinpiaoArrow then
            this.yinpiaoArrow.flip = UIBasicSprite.Flip.Vertically
        end
    end)
end

CPackageMoneyInfoView.m_UpdateMoneyDisplay_CS2LuaHook = function(this)
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer then
        this.yinliangLabel.text = tostring(mainPlayer.Silver)
        this.yinpiaoLabel.text = tostring(mainPlayer.FreeSilver)
        this.yuanbaoLabel.text = tostring(mainPlayer.ItemProp.YuanBao)
        local bindLingYu = mainPlayer.BindJade
        this.bindLinYuSprite:SetActive(bindLingYu > 0)
        this.lingyuLabel.text = tostring(mainPlayer.Jade + bindLingYu)
        this.yinpiaoArrow.gameObject:SetActive(mainPlayer.SkillPrivateSilver > 0)
        if mainPlayer.SkillPrivateSilver > 0 then
            CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.SkillPrivateSilverGuide)
        end
    end
end
