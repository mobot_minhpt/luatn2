local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local GameObject = import "UnityEngine.GameObject"

LuaStarBiWuGiftRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaStarBiWuGiftRankWnd, "BottomLabel", "BottomLabel", UILabel)
RegistChildComponent(LuaStarBiWuGiftRankWnd, "TableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaStarBiWuGiftRankWnd, "TipButton", "TipButton", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaStarBiWuGiftRankWnd,"m_List")
RegistClassMember(LuaStarBiWuGiftRankWnd,"m_Index")

function LuaStarBiWuGiftRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

    --@endregion EventBind end
end

function LuaStarBiWuGiftRankWnd:Init()
	self.BottomLabel.text = g_MessageMgr:FormatMessage("StarBiWuGiftRankWnd_BottomText")
	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_List
        end,
        function(item, index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

            self:InitItem(item, index)
        end
    )
	self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
		self:OnSelectAtRow(row)
    end)
	Gac2Gas.QueryStarBiwuZhanduiRenqiRank()
end

function LuaStarBiWuGiftRankWnd:InitItem(item, index)
	local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
	local rankImage = item.transform:Find("RankLabel/RankImage"):GetComponent(typeof(UISprite))
	local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local popularityLabel = item.transform:Find("PopularityLabel"):GetComponent(typeof(UILabel))
	local rank1NameLabel = item.transform:Find("Rank1NameLabel"):GetComponent(typeof(UILabel))
	local rank2NameLabel = item.transform:Find("Rank2NameLabel"):GetComponent(typeof(UILabel))
	local leiGuSprite = item.transform:Find("LeiGuSprite").gameObject
	local rankBtnLabel = item.transform:Find("RankBtnLabel").gameObject
	leiGuSprite.gameObject:SetActive(false)

	local t = self.m_List[index + 1]
	local rank = t.rank

	rankImage.spriteName= ""
	rankLabel.text = ""
	if rank==1 then
		rankImage.spriteName="Rank_No.1"
	elseif rank==2 then
		rankImage.spriteName="Rank_No.2png"
	elseif rank==3 then
		rankImage.spriteName="Rank_No.3png"
	else
		if rank > 0 then
			rankLabel.text = rank
		else
			rankLabel.text = "-"
		end
	end
	nameLabel.text = t.zhanduiName
	popularityLabel.text = t.renqi
	rank1NameLabel.text = t.name1
	rank2NameLabel.text = t.name2
	local id = t.zhanduiId
	leiGuSprite.gameObject:SetActive(CLuaStarBiwuMgr.m_ZhanduiRenqiRankInfo.giftRankSet and CLuaStarBiwuMgr.m_ZhanduiRenqiRankInfo.giftRankSet[id])

	UIEventListener.Get(rankBtnLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		local i = index
	    self:OnRankBtnLabelClick(i)
	end)
end

function LuaStarBiWuGiftRankWnd:OnEnable()
	g_ScriptEvent:AddListener("OnSyncStarBiwuZhanduiRenqiRank",self,"OnSyncStarBiwuZhanduiRenqiRank")
end

function LuaStarBiWuGiftRankWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSyncStarBiwuZhanduiRenqiRank",self,"OnSyncStarBiwuZhanduiRenqiRank")
end

function LuaStarBiWuGiftRankWnd:OnSyncStarBiwuZhanduiRenqiRank()
	self.m_List = CLuaStarBiwuMgr.m_ZhanduiRenqiRankInfo.rankList
	table.sort(self.m_List,function (a,b)
		return a.rank < b.rank
	end)
	self.TableView:ReloadData(true, false)
end

--@region UIEvent

function LuaStarBiWuGiftRankWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("StarBiWuGiftRankWnd_ReadMe")
end

function LuaStarBiWuGiftRankWnd:OnRankBtnLabelClick(index)
	local t = self.m_List[index + 1] 
	CLuaStarBiwuMgr.m_GiftContributionRankWndZhanDuiId = t.zhanduiId
	CUIManager.ShowUI(CLuaUIResources.StarBiWuGiftContributionRankWnd)
end

function LuaStarBiWuGiftRankWnd:OnSelectAtRow(row)
	self.m_Index = row
end
--@endregion UIEvent

