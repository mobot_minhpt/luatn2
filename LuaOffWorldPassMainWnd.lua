local UISprite = import "UISprite"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local Vector2 = import "UnityEngine.Vector2"
local Quaternion = import "UnityEngine.Quaternion"
local CUIFx = import "L10.UI.CUIFx"
local CUITexture = import "L10.UI.CUITexture"
local UITexture = import "UITexture"
local UIScrollView = import "UIScrollView"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local QnTabButton = import "L10.UI.QnTabButton"
local Vector3 = import "UnityEngine.Vector3"
local Vector4 = import "UnityEngine.Vector4"
local UILabel = import "UILabel"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local PathType = import "DG.Tweening.PathType"
local UIPanel = import "UIPanel"
local PathMode = import "DG.Tweening.PathMode"
local Ease = import "DG.Tweening.Ease"

LuaOffWorldPassMainWnd=class()
RegistChildComponent(LuaOffWorldPassMainWnd,"CloseBtn", GameObject)
RegistChildComponent(LuaOffWorldPassMainWnd,"panel1", GameObject)
RegistChildComponent(LuaOffWorldPassMainWnd,"panel2", GameObject)
RegistChildComponent(LuaOffWorldPassMainWnd,"RewardBtn", GameObject)
RegistChildComponent(LuaOffWorldPassMainWnd,"TaskBtn", GameObject)
RegistChildComponent(LuaOffWorldPassMainWnd,"timeLabel", UILabel)
RegistChildComponent(LuaOffWorldPassMainWnd,"GetMoreBtn", GameObject)
RegistChildComponent(LuaOffWorldPassMainWnd,"ExpBg", UISlider)
RegistChildComponent(LuaOffWorldPassMainWnd,"NeedExpLb", UILabel)
RegistChildComponent(LuaOffWorldPassMainWnd,"LvLb", UILabel)
RegistChildComponent(LuaOffWorldPassMainWnd,"TipBtn", GameObject)
RegistChildComponent(LuaOffWorldPassMainWnd,"unlockBtn1", GameObject)
RegistChildComponent(LuaOffWorldPassMainWnd,"unlockBtn2", GameObject)
RegistChildComponent(LuaOffWorldPassMainWnd,"unlockNode1", GameObject)
RegistChildComponent(LuaOffWorldPassMainWnd,"unlockNode2", GameObject)
RegistChildComponent(LuaOffWorldPassMainWnd,"unlock1Fx", CUIFx)
RegistChildComponent(LuaOffWorldPassMainWnd,"unlock2Fx", CUIFx)
RegistChildComponent(LuaOffWorldPassMainWnd,"Number", UILabel)

--RegistClassMember(LuaOffWorldPassMainWnd, "cameraNode")
RegistClassMember(LuaOffWorldPassMainWnd,"m_ModelTextureLoader")
RegistClassMember(LuaOffWorldPassMainWnd,"m_ModelName")
RegistClassMember(LuaOffWorldPassMainWnd,"m_Tick")
RegistClassMember(LuaOffWorldPassMainWnd,"m_FxTick")
RegistClassMember(LuaOffWorldPassMainWnd,"m_BonusItemNodeTable")
RegistClassMember(LuaOffWorldPassMainWnd,"weaponRo")
RegistClassMember(LuaOffWorldPassMainWnd, "swShowArgs")
RegistClassMember(LuaOffWorldPassMainWnd, "m_PassRedDotMark")
RegistClassMember(LuaOffWorldPassMainWnd, "m_PlayRedDotMark")

function LuaOffWorldPassMainWnd:Awake()
end

function LuaOffWorldPassMainWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateBuyOffWorldPassResult", self, "UpdateBonusInfo")
	g_ScriptEvent:AddListener("UpdateSingleBuyOffWorldPassResult", self, "UpdateSingleBonusInfo")
	g_ScriptEvent:AddListener("UpdateOffWorldPlayBonusType", self, "UpdateBonusGet")
	g_ScriptEvent:AddListener("UpdateOffWorldPlayGetSw", self, "UpdateGetSW")
	g_ScriptEvent:AddListener("OffWorldPassNeedRedDot", self, "UpdateRedDot")
	--g_ScriptEvent:AddListener("OnScreenChange", self, "UpdateScreen")
end

function LuaOffWorldPassMainWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateBuyOffWorldPassResult", self, "UpdateBonusInfo")
	g_ScriptEvent:RemoveListener("UpdateSingleBuyOffWorldPassResult", self, "UpdateSingleBonusInfo")
	g_ScriptEvent:RemoveListener("UpdateOffWorldPlayBonusType", self, "UpdateBonusGet")
	g_ScriptEvent:RemoveListener("UpdateOffWorldPlayGetSw", self, "UpdateGetSW")
  g_ScriptEvent:RemoveListener("OffWorldPassNeedRedDot", self, "UpdateRedDot")
	--g_ScriptEvent:RemoveListener("OnScreenChange", self, "UpdateScreen")
end

function LuaOffWorldPassMainWnd:UpdateSingleBonusInfo(level,passType)
  if self.m_BonusItemNodeTable == nil then
    self:InitPanel1()
  end
  
	local i = level
	local data = OffWorldPass_PassReward.GetData(i)
	local go = self.m_BonusItemNodeTable[i]

	local item1 = go.transform:Find('ItemCell1').gameObject
	local item2 = go.transform:Find('ItemCell2').gameObject
	local item3 = go.transform:Find('ItemCell3').gameObject

  local itemAndCount = {0,0,0,0,0,0}
  local dataTbl = {data.basic, data.luxury, data.epic}
  for i=1,3 do
    if dataTbl[i] and dataTbl[i].Length > 1 then
      itemAndCount[i*2-1] = dataTbl[i][0]
      itemAndCount[i*2] = dataTbl[i][1]
    end
  end

    local TableBody = self.panel1.transform:Find('ScrollView'):GetComponent(typeof(UIScrollView))
	local open1 = self:InitItemNode(item1,itemAndCount[1], itemAndCount[2],i,LuaOffWorldPassMgr.page1data.pass1,TableBody,1,1)
	local open2 = self:InitItemNode(item2,itemAndCount[3], itemAndCount[4],i,LuaOffWorldPassMgr.page1data.pass2,TableBody,2,LuaOffWorldPassMgr.page1data.pass2enable)
	local open3 = self:InitItemNode(item3,itemAndCount[5], itemAndCount[6],i,LuaOffWorldPassMgr.page1data.pass3,TableBody,3,LuaOffWorldPassMgr.page1data.pass3enable)
	if open1 or open2 or open3 then
		go.transform:Find('label/lock').gameObject:SetActive(false)
		go.transform:Find('label/open').gameObject:SetActive(true)
	else
		go.transform:Find('label/lock').gameObject:SetActive(true)
		go.transform:Find('label/open').gameObject:SetActive(false)
	end
  Gac2Gas.GetOffWorldRedDotInfo()
end

-- mark：nil全更新 1更新通行证 2更新挑战
function LuaOffWorldPassMainWnd:UpdateRedDot(bPassRedDot, bPlayRedDot, mark)
  local alertPass = self.transform:Find("TabBtnsPanel/RewardBtn/AlertSpriteReward").gameObject
  local alertPlay = self.transform:Find("TabBtnsPanel/TaskBtn/AlertSpriteTask").gameObject

  if mark == nil or mark == 1 then
    alertPass:SetActive(bPassRedDot)
  end

  if mark == nil or mark == 2 then
    alertPlay:SetActive(bPlayRedDot)
  end
end

function LuaOffWorldPassMainWnd:InitRedDot()
  local passEnable = {1, LuaOffWorldPassMgr.page1data.pass3enable, LuaOffWorldPassMgr.page1data.pass3enable}
  local passData = {LuaOffWorldPassMgr.page1data.pass1, LuaOffWorldPassMgr.page1data.pass2, LuaOffWorldPassMgr.page1data.pass3}
  local count = OffWorldPass_PassReward.GetDataCount()
  local level = LuaOffWorldPassMgr.page1data.level
  local passMark = false
  for i=1,count-1 do
    for j=1, 3 do
      if passEnable[j] then
        local open = i <= level
        local get = bit.band(passData[j], bit.lshift(1, level-1))
        if open and (get == nil or get <= 0) then
          passMark = true
        end
      end
    end
  end
  self:UpdateRedDot(passMark, false, 1)
end

function LuaOffWorldPassMainWnd:UpdateBonusInfo()
  if self.m_BonusItemNodeTable == nil then
    self:InitPanel1()
  end

  if LuaOffWorldPassMgr.page1data.pass2enable > 0 then
    self.unlockBtn1:SetActive(false)
		self.unlockNode1:SetActive(true)
		self.unlock1Fx:LoadFx("fx/ui/prefab/UI_shenwutongxingzheng_jiesuojihuo.prefab")
  else
    self.unlockBtn1:SetActive(true)
		self.unlockNode1:SetActive(false)
  end
  if LuaOffWorldPassMgr.page1data.pass3enable > 0 then
    self.unlockBtn2:SetActive(false)
		self.unlockNode2:SetActive(true)
		self.unlock2Fx:LoadFx("fx/ui/prefab/UI_shenwutongxingzheng_jiesuojihuo.prefab")
  else
    self.unlockBtn2:SetActive(true)
		self.unlockNode2:SetActive(false)
  end

  local count = OffWorldPass_PassReward.GetDataCount()
  for i=1,count-1 do
    local data = OffWorldPass_PassReward.GetData(i)
    local go = self.m_BonusItemNodeTable[i]

    local item1 = go.transform:Find('ItemCell1').gameObject
    local item2 = go.transform:Find('ItemCell2').gameObject
    local item3 = go.transform:Find('ItemCell3').gameObject

    local itemAndCount = {0,0,0,0,0,0}
    local dataTbl = {data.basic, data.luxury, data.epic}
    for i=1,3 do
      if dataTbl[i] and dataTbl[i].Length > 1 then
        itemAndCount[i*2-1] = dataTbl[i][0]
        itemAndCount[i*2] = dataTbl[i][1]
      end
    end
    local TableBody = self.panel1.transform:Find('ScrollView'):GetComponent(typeof(UIScrollView))
    local open1 = self:InitItemNode(item1,itemAndCount[1], itemAndCount[2],i,LuaOffWorldPassMgr.page1data.pass1,TableBody,1,1)
    local open2 = self:InitItemNode(item2,itemAndCount[3], itemAndCount[4],i,LuaOffWorldPassMgr.page1data.pass2,TableBody,2,LuaOffWorldPassMgr.page1data.pass2enable)
    local open3 = self:InitItemNode(item3,itemAndCount[5], itemAndCount[6],i,LuaOffWorldPassMgr.page1data.pass3,TableBody,3,LuaOffWorldPassMgr.page1data.pass3enable)
		if open1 or open2 or open3 then
			go.transform:Find('label/lock').gameObject:SetActive(false)
			go.transform:Find('label/open').gameObject:SetActive(true)
		else
			go.transform:Find('label/lock').gameObject:SetActive(true)
			go.transform:Find('label/open').gameObject:SetActive(false)
		end

		if open1 then
			go.transform:Find('bg1'):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor("98B3A0", 0)
		else
			go.transform:Find('bg1'):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor("7E8E83", 0)
		end
		if open2 and LuaOffWorldPassMgr.page1data.pass2enable > 0 then
			go.transform:Find('bg2'):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor("BB999D", 0)
		else
			go.transform:Find('bg2'):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor("877678", 0)
		end
		if open3 and LuaOffWorldPassMgr.page1data.pass3enable > 0 then
			go.transform:Find('bg3'):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor("A68CA9", 0)
		else
			go.transform:Find('bg3'):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor("7B6B7D", 0)
		end
  end
  Gac2Gas.GetOffWorldRedDotInfo()
end

function LuaOffWorldPassMainWnd:CloseWnd(go)
  CUIManager.CloseUI(CLuaUIResources.OffWorldPassMainWnd)
end

function LuaOffWorldPassMainWnd:InitItemNode(node,itemId, count, level,rewardBitmap,m_ScrollView,bonusType,openSign)
  if itemId > 0 then
    local item = Item_Item.GetData(itemId)
    if item then
      node.transform:Find('IconTexture'):GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
      local countText = ""
      if count > 1 then
        countText = tostring(count)
      end
      node.transform:Find("AmountLabel"):GetComponent(typeof(UILabel)).text = countText
      local lock = node.transform:Find('lock').gameObject
      local getNode = node.transform:Find('get').gameObject
      getNode:SetActive(false)
      local fx = node.transform:Find('Fx'):GetComponent(typeof(CUIFx))

      local trans = node.transform
      local open = level <= LuaOffWorldPassMgr.page1data.level
      local get = bit.band(rewardBitmap, bit.lshift(1, level-1))

			if openSign <= 0 then
        lock:SetActive(true)
        UIEventListener.Get(node).onClick = DelegateFactory.VoidDelegate(function()
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end)
				return
			end

      if open then
        lock:SetActive(false)
        if get and get > 0 then
          fx:DestroyFx()
          LuaTweenUtils.DOKill(fx.transform, false)
          fx.gameObject:SetActive(false)
          getNode:SetActive(true)
					UIEventListener.Get(node).onClick = DelegateFactory.VoidDelegate(function()
						CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
					end)
        else
          fx.gameObject:SetActive(true)
          fx.ScrollView = m_ScrollView
          LuaTweenUtils.DOKill(fx.transform, false)
          local b = NGUIMath.CalculateRelativeWidgetBounds(trans)
          local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, false)
          fx.transform.localPosition = waypoints[0]
          fx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
          LuaTweenUtils.DOLuaLocalPath(fx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
					UIEventListener.Get(node).onClick = DelegateFactory.VoidDelegate(function()
						Gac2Gas.PlayerCollectOffWorldPassReward(level,bonusType)
					end)
        end
				return true
      else
        lock:SetActive(true)
        UIEventListener.Get(node).onClick = DelegateFactory.VoidDelegate(function()
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end)
      end
    else
      node:SetActive(false)
    end
  else
    node:SetActive(false)
		local open = level <= LuaOffWorldPassMgr.page1data.level
		return open
  end
end

function LuaOffWorldPassMainWnd:LoadModel(ro, prefabname)
    ro:LoadMain(prefabname, nil, false, false)
		-- ro.transform.localRotation = Quaternion.Euler(0, 90, -90)
		ro.transform.localRotation = Quaternion.Euler(self.swShowArgs[5],self.swShowArgs[6],self.swShowArgs[7])
		self.weaponRo = ro
end

function LuaOffWorldPassMainWnd:InitSWModel(data)
  local equipmentId = nil
  local weaponTexture = nil
  OffWorldPass_Fashion.Foreach(function(i,v)
    if v.type == data.weaponType and v.wuxing == data.wuxing then
      equipmentId = v.equipmentId
      weaponTexture = v.weaponTexture
    end
  end)

  if not equipmentId or not weaponTexture then
    return
  end

  local equipData = EquipmentTemplate_Equip.GetData(equipmentId)
  
	local weaponNode = self.panel1.transform:Find('weaponShowNode/weapon').gameObject
  local m_FakeModel = weaponNode:GetComponent(typeof(CUITexture))
  m_FakeModel:LoadMaterial(weaponTexture)

  if data.weaponType == 1 or data.weaponType == 3 then
    m_FakeModel.transform.localPosition = Vector3(-2, 30, 0)
    Extensions.SetLocalRotationZ(m_FakeModel.transform, 10)
  elseif data.weaponType == 2  then
    m_FakeModel.transform.localPosition = Vector3(-2, 40, 0)
    Extensions.SetLocalRotationZ(m_FakeModel.transform, 15)
  else
    m_FakeModel.transform.localPosition = Vector3(-2, 11, 0)
    Extensions.SetLocalRotationZ(m_FakeModel.transform, 0)
  end

  -- 模型加载代码
  -- self.swShowArgs = LuaOffWorldPassMgr.GetWeaponShowArgs(data)
  -- self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
  --   self:LoadModel(ro, equipData.Prefab)
  -- end)
  -- m_FakeModel.mainTexture = CUIManager.CreateModelTexture(self.m_ModelName, self.m_ModelTextureLoader,90,0,-0.3,3.04,false,true,1,true)
  -- m_FakeModel.mainTexture = CUIManager.CreateModelTexture(self.m_ModelName, self.m_ModelTextureLoader,self.swShowArgs[1],self.swShowArgs[2],self.swShowArgs[3],self.swShowArgs[4],false,true,1,true)
	-- local onDrag = function(go,delta)
	-- 	self.weaponRo.transform:Rotate(Vector3.right,(delta.x+delta.y+delta.z))
	-- end
	-- CommonDefs.AddOnDragListener(weaponNode,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
end

function LuaOffWorldPassMainWnd:UpdateGetSW()
  Gac2Gas.GetOffWorldRedDotInfo()
  local desLaebl = self.panel1.transform:Find('weaponShowNode/desLabel').gameObject
  local getBtn = self.panel1.transform:Find('weaponShowNode/getBtn').gameObject
  local alGetNode = self.panel1.transform:Find('weaponShowNode/alGet').gameObject
	desLaebl:SetActive(false)
	getBtn:SetActive(false)
	alGetNode:SetActive(true)
end

function LuaOffWorldPassMainWnd:InitSW()
  --LuaOffWorldPassMgr.page3data = {swstage = swstage,weaponType = weaponType, wuxing = wuxing, name = name, status = status, fashionId = fashionId}
  local desLaebl = self.panel1.transform:Find('weaponShowNode/desLabel').gameObject
  local getBtn = self.panel1.transform:Find('weaponShowNode/getBtn').gameObject
  local alGetNode = self.panel1.transform:Find('weaponShowNode/alGet').gameObject
  local alGetNode = self.panel1.transform:Find('weaponShowNode/alGet').gameObject
  local nameNode = self.panel1.transform:Find('weaponShowNode/Label').gameObject
  local desLabel2 = self.panel1.transform:Find('weaponShowNode/desLabel/Label').gameObject

  desLabel2:SetActive(true)
  nameNode:SetActive(true)
  desLaebl.transform:GetComponent(typeof(UILabel)).text = LocalString.GetString("神武淬炼中")

  if LuaOffWorldPassMgr.page3data.swstage == 1 then
    --
    if LuaOffWorldPassMgr.page1data.level < OffWorldPass_PassReward.GetDataCount() - 1 then
      desLaebl:SetActive(true)
      alGetNode:SetActive(false)
      getBtn:SetActive(false)
      self:InitSWModel({weaponType = LuaOffWorldPassMgr.page3data.weaponType, wuxing = 0})
    else
      desLaebl:SetActive(false)
      alGetNode:SetActive(false)
      getBtn:SetActive(true)
      UIEventListener.Get(getBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
        --Gac2Gas_SubmitSelfEditedOffWorldFashion
        --玩家请求获得自定义神武(虚态的)
        --    param:  weaponType  神武类型
        --            wuxing      神武五行
        --            name        神武名字
        Gac2Gas.RequestRealOffWorldFashion()
      end)
      self:InitSWModel({weaponType = LuaOffWorldPassMgr.page3data.weaponType, wuxing = LuaOffWorldPassMgr.page3data.wuxing})
    end
  elseif LuaOffWorldPassMgr.page3data.swstage == 2 then
    desLaebl:SetActive(false)
    alGetNode:SetActive(true)
    getBtn:SetActive(false)
    self:InitSWModel({weaponType = LuaOffWorldPassMgr.page3data.weaponType, wuxing = LuaOffWorldPassMgr.page3data.wuxing})
  elseif LuaOffWorldPassMgr.page3data.swstage == 0 then
    desLaebl:SetActive(true)
    alGetNode:SetActive(false)
    getBtn:SetActive(false)
    nameNode:SetActive(false)
    desLabel2:SetActive(false)
    desLaebl.transform:GetComponent(typeof(UILabel)).text = LocalString.GetString("完成任务[00FF00][异世红尘]择神刃[-]后可获得虚态神武")
    --self:InitSWModel({weaponType = LuaOffWorldPassMgr.page3data.weaponType, wuxing = LuaOffWorldPassMgr.page3data.wuxing})
  end

  if LuaOffWorldPassMgr.page3data.name then
    self.panel1.transform:Find('weaponShowNode/Label'):GetComponent(typeof(UILabel)).text = LuaOffWorldPassMgr.page3data.name
  end

end

function LuaOffWorldPassMainWnd:InitPanel1()
  local TableBody = self.panel1.transform:Find('ScrollView'):GetComponent(typeof(UIScrollView))
  local Grid = self.panel1.transform:Find('ScrollView/Grid'):GetComponent(typeof(UIGrid))
  local template = self.panel1.transform:Find('Template').gameObject
  template:SetActive(false)

	self.m_BonusItemNodeTable = {}

	Extensions.RemoveAllChildren(Grid.transform)

	local maxOpenIndex = 1
  local count = OffWorldPass_PassReward.GetDataCount()
	local maxNode
  for i=1,count-1 do
    local data = OffWorldPass_PassReward.GetData(i)
    local go = NGUITools.AddChild(Grid.gameObject, template)
    go:SetActive(true)
    local item1 = go.transform:Find('ItemCell1').gameObject
    local item2 = go.transform:Find('ItemCell2').gameObject
    local item3 = go.transform:Find('ItemCell3').gameObject
    local label = go.transform:Find('label'):GetComponent(typeof(UILabel))
    label.text = i..LocalString.GetString('级')

    local itemAndCount = {0,0,0,0,0,0}
    local dataTbl = {data.basic, data.luxury, data.epic}
    
    for i=1,3 do
      if dataTbl[i] and dataTbl[i].Length > 1 then
        itemAndCount[i*2-1] = dataTbl[i][0]
        itemAndCount[i*2] = dataTbl[i][1]
      end
    end

    local open1 = self:InitItemNode(item1,itemAndCount[1], itemAndCount[2],i,LuaOffWorldPassMgr.page1data.pass1,TableBody,1,1)
    local open2 = self:InitItemNode(item2,itemAndCount[3], itemAndCount[4], i,LuaOffWorldPassMgr.page1data.pass2,TableBody,2,LuaOffWorldPassMgr.page1data.pass2enable)
    local open3 = self:InitItemNode(item3,itemAndCount[5], itemAndCount[6],i,LuaOffWorldPassMgr.page1data.pass3,TableBody,3,LuaOffWorldPassMgr.page1data.pass3enable)
		self.m_BonusItemNodeTable[i] = go
		if open1 or open2 or open3 then
			maxOpenIndex = i
			maxNode = go
			go.transform:Find('label/lock').gameObject:SetActive(false)
			go.transform:Find('label/open').gameObject:SetActive(true)
		else
			go.transform:Find('label/lock').gameObject:SetActive(true)
			go.transform:Find('label/open').gameObject:SetActive(false)
		end
		if open1 then
			go.transform:Find('bg1'):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor("98B3A0", 0)
		else
			go.transform:Find('bg1'):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor("7E8E83", 0)
		end
		if open2 and LuaOffWorldPassMgr.page1data.pass2enable > 0 then
			go.transform:Find('bg2'):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor("BB999D", 0)
		else
			go.transform:Find('bg2'):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor("877678", 0)
		end
		if open3 and LuaOffWorldPassMgr.page1data.pass3enable > 0 then
			go.transform:Find('bg3'):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor("A68CA9", 0)
		else
			go.transform:Find('bg3'):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor("7B6B7D", 0)
		end
  end

	if maxNode then
		maxNode.transform:Find('Selected').gameObject:SetActive(true)
	end

	Grid:Reposition()
	TableBody:ResetPosition()
	if maxOpenIndex > 5 then
		local gridWidth = 162
    TableBody.transform.localPosition = Vector3(18.62 - (maxOpenIndex - 6) * gridWidth,4,0)
		local _panel = TableBody.transform:GetComponent(typeof(UIPanel))
    _panel.clipOffset = Vector2(71.3 + (maxOpenIndex - 6) * gridWidth, 0)
	end

  self:InitSW()
end

function LuaOffWorldPassMainWnd:IsJiaoShanOpen()
  return false
end

function LuaOffWorldPassMainWnd:InitPanel2()
  self.panel2.transform:Find('label'):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage('OffWorldPassPlayTipText')

  -- 副本挑战次数
  self.Number.text = tostring( OffWorldPass_Setting.GetData().CaiDieWeekLimitTime - LuaOffWorldPassMgr.page2data.fubenCount)

  -- 分数
  self.panel2.transform:Find('infoNode/label2'):GetComponent(typeof(UILabel)).text = LuaOffWorldPassMgr.page2data.fuben2coin .. '/' .. OffWorldPass_Setting.GetData().MaxScorePerWeekCaiDie
	local percent2 = LuaOffWorldPassMgr.page2data.fuben2coin / OffWorldPass_Setting.GetData().MaxScorePerWeekCaiDie
	self.panel2.transform:Find('infoNode/ExpBg'):GetComponent(typeof(UITexture)).fillAmount = percent2

  local fuben1Btn = self.panel2.transform:Find('node1/btn').gameObject
  local fuben1CloseNode = self.panel2.transform:Find('node1Close').gameObject
  local fuben2Btn = self.panel2.transform:Find('node2/btn').gameObject

  -- 目前第一个副本没开放
  UIEventListener.Get(fuben1Btn).onClick = LuaUtils.VoidDelegate(function ( ... )
		Gac2Gas.EnterJiaoShanGamePlay()
  end)

  fuben1Btn:SetActive(self:IsJiaoShanOpen())
  fuben1CloseNode:SetActive(not self:IsJiaoShanOpen())
  fuben2Btn:SetActive(true)

  UIEventListener.Get(fuben2Btn).onClick = LuaUtils.VoidDelegate(function ( ... )
    Gac2Gas.CaiDieSignUp()
  end)
  --LuaOffWorldPassMgr.page2data = {passtime = passtime,fuben1coin = fuben1coin,fuben2coin= fuben2coin,fuben1get=fuben1get,fuben2get=fuben2get}
  local fuben2BonusBtn = self.panel2.transform:Find('infoNode/Reward').gameObject
  local received = self.panel2.transform:Find('infoNode/received').gameObject

  if LuaOffWorldPassMgr.page2data.fuben2coin >= OffWorldPass_Setting.GetData().MaxScorePerWeekCaiDie and LuaOffWorldPassMgr.page2data.fuben2get == 0 then
    fuben2BonusBtn:SetActive(true)
    received:SetActive(false)
		fuben2BonusBtn.transform:Find('fx'):GetComponent(typeof(CUIFx)):LoadFx('fx/ui/prefab/UI_shenwutongxingzheng_libaojiesuo.prefab')
    UIEventListener.Get(fuben2BonusBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
			self.panel2.transform:Find('fx'):GetComponent(typeof(CUIFx)):LoadFx('fx/ui/prefab/UI_shenwutongxingzheng_libaodakai.prefab')
      fuben2BonusBtn.transform:Find('fx').gameObject:SetActive(false)
			if self.m_FxTick then
				UnRegisterTick(self.m_FxTick)
				self.m_FxTick = nil
			end
			self.m_FxTick = RegisterTickWithDuration(function ()
				self.panel2.transform:Find('fx'):GetComponent(typeof(CUIFx)):DestroyFx()
				Gac2Gas.PlayerCollectOffWorldWeekReward()
			end, 2000, 2000)
    end)
	elseif LuaOffWorldPassMgr.page2data.fuben2get > 0 then
    received:SetActive(true)
    fuben2BonusBtn:SetActive(true)
		fuben2BonusBtn.transform.localPosition = Vector3(fuben2BonusBtn.transform.localPosition.x,fuben2BonusBtn.transform.localPosition.y,-1)
		fuben2BonusBtn.transform:Find('fx').gameObject:SetActive(false)
    UIEventListener.Get(fuben2BonusBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
    end)
  else
    received:SetActive(false)
    CUICommonDef.SetActive(fuben2BonusBtn, false, true)
  end
end

function LuaOffWorldPassMainWnd:UpdateBonusGet()
  Gac2Gas.GetOffWorldRedDotInfo()
  LuaOffWorldPassMgr.page2data.fuben2get = 1
	local fubenBonusBtn = self.panel2.transform:Find('infoNode/Reward').gameObject
  local received = self.panel2.transform:Find('infoNode/received').gameObject

  fubenBonusBtn:SetActive(true)
  received:SetActive(true)
  fubenBonusBtn.transform.localPosition = Vector3(fubenBonusBtn.transform.localPosition.x,fubenBonusBtn.transform.localPosition.y,-1)
  fubenBonusBtn.transform:Find('fx').gameObject:SetActive(false)
  UIEventListener.Get(fubenBonusBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
  end)
end

function LuaOffWorldPassMainWnd:OnSelectTab(tab)
  if self.CurTabIndex == tab then return end
  self.CurTabIndex = tab
  self.panel1:SetActive(self.CurTabIndex == 0)
  self.panel2:SetActive(self.CurTabIndex == 1)

  if self.CurTabIndex == 0 then
    self:InitPanel1()
  else
    self:InitPanel2()
  end

  local rewardbtn = CommonDefs.GetComponent_GameObject_Type(self.RewardBtn,typeof(QnTabButton))
  local taskbtn = CommonDefs.GetComponent_GameObject_Type(self.TaskBtn,typeof(QnTabButton))
  rewardbtn.Selected = self.CurTabIndex == 0
  taskbtn.Selected = self.CurTabIndex == 1
end

function LuaOffWorldPassMainWnd:InitTopInfo()
  local nowLevel = LuaOffWorldPassMgr.page1data.level
  self.LvLb.text = nowLevel
	local levelInfo = OffWorldPass_PassReward.GetData(nowLevel)
  local beforeLevelCount = 0
  if nowLevel > 0 then
    beforeLevelCount = OffWorldPass_PassReward.GetData(nowLevel-1).scoreToNext
  end
  

	if nowLevel == OffWorldPass_PassReward.GetDataCount() - 1 then
		self.NeedExpLb.text = LocalString.GetString('已满级')
    self.GetMoreBtn:SetActive(false)
		self.ExpBg.gameObject:SetActive(false)
		self.ExpBg.transform.parent:Find('ExpIcon').gameObject:SetActive(false)
	else
    self.GetMoreBtn:SetActive(true)
		if levelInfo then
			local rest = levelInfo.scoreToNext - LuaOffWorldPassMgr.page1data.coin
			if rest < 0 then
				rest = 0
			end
			self.NeedExpLb.text = SafeStringFormat3(LocalString.GetString('升至下级还需%s'),rest)
			local percent = (LuaOffWorldPassMgr.page1data.coin-beforeLevelCount) / (levelInfo.scoreToNext-beforeLevelCount)
			if percent > 1 then
				percent = 1
			end
			self.ExpBg.value = percent
		end
	end

  local onTipClick = function(go)
    g_MessageMgr:ShowMessage('OffWorldPassTip')
  end

  CommonDefs.AddOnClickListener(self.TipBtn,DelegateFactory.Action_GameObject(onTipClick),false)

  UIEventListener.Get(self.GetMoreBtn).onClick = DelegateFactory.VoidDelegate(function(go)
		--CItemAccessListMgr.Inst:ShowItemAccessInfo(resId, true, go.transform, CTooltipAlignType.Left)
		g_MessageMgr:ShowMessage('OffWorldPassPlayGetMore')
  end)

	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
	self.m_Tick = RegisterTickWithDuration(function ()
		self:CalTime()
	end, 1000, 1000 * 90000)
	self:CalTime()
end

function LuaOffWorldPassMainWnd:CalTime()
	local now = CServerTimeMgr.Inst.timeStamp
  local expireTime = string.gsub(OffWorldPass_Setting.GetData().PlayDataExpireTime, ",", " ")
	local restTime = CServerTimeMgr.Inst:GetTimeStampByStr(expireTime) - now
	if restTime <= 0 then
		self.timeLabel.text = LocalString.GetString('异世神武活动已结束')
	else
		local day = math.floor(restTime / 3600 / 24)
		if day > 0 then
			self.timeLabel.text = SafeStringFormat3(LocalString.GetString('异世神武截止倒计时：%s天'),day)
		else
			local hour = math.floor(restTime / 3600)
			if hour > 0 then
				self.timeLabel.text = SafeStringFormat3(LocalString.GetString('异世神武截止倒计时：%s小时'),hour)
			else
				local min = math.floor(restTime / 60) - hour*60
				if min == 0 then
					min = 1
				end
				if min < 10 then
					min = '0' ..  min
				end
				local sec = math.floor(restTime % 60)
				if sec < 10 then
					sec = '0' .. sec
				end

				self.timeLabel.text = SafeStringFormat3(LocalString.GetString('异世神武截止倒计时：%s:%s'),min,sec)
			end
		end
	end
end


--LuaOffWorldPassMgr.page1data = {level = level, coin = coin,pass1 = pass1,pass2 = pass2,pass3 = pass3,pass2enable = pass2enable,pass3enable = pass3enable}
--LuaOffWorldPassMgr.page2data = {passtime = passtime,fuben1coin = fuben1coin,fuben2coin= fuben2coin,fuben1get=fuben1get,fuben2get=fuben2get}
function LuaOffWorldPassMainWnd:UpdateAward()
  self:OnSelectTab(0)

  if LuaOffWorldPassMgr.page1data.pass2enable > 0 then
    self.unlockBtn1:SetActive(false)
		self.unlockNode1:SetActive(true)
  else
    self.unlockBtn1:SetActive(true)
		self.unlockNode1:SetActive(false)
    UIEventListener.Get(self.unlockBtn1).onClick = DelegateFactory.VoidDelegate(function(go)
      CUIManager.ShowUI(CLuaUIResources.OffWorldPassDetailWnd)
    end)
  end
  if LuaOffWorldPassMgr.page1data.pass3enable > 0 then
    self.unlockBtn2:SetActive(false)
		self.unlockNode2:SetActive(true)
  else
    self.unlockBtn2:SetActive(true)
		self.unlockNode2:SetActive(false)
    UIEventListener.Get(self.unlockBtn2).onClick = DelegateFactory.VoidDelegate(function(go)
      CUIManager.ShowUI(CLuaUIResources.OffWorldPassDetailWnd)
    end)
  end
end

function LuaOffWorldPassMainWnd:OnDestroy()
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end

	if self.m_FxTick then
		UnRegisterTick(self.m_FxTick)
		self.m_FxTick = nil
	end
	CUIManager.DestroyModelTexture(self.m_ModelName)
  LuaOffWorldPassMgr.OpenTabIndex = nil
end

function LuaOffWorldPassMainWnd:Init()
  Gac2Gas.GetOffWorldRedDotInfo()
  self.m_ModelName = "_OffWorldPassWeaponPreview_"

  local onCloseClick = function(go)
    self:CloseWnd()
  end

  CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

  self:InitTopInfo()

  UIEventListener.Get(self.RewardBtn).onClick = DelegateFactory.VoidDelegate(function(go)
    self:OnSelectTab(0)
  end)

  UIEventListener.Get(self.TaskBtn).onClick = DelegateFactory.VoidDelegate(function(go)
    self:OnSelectTab(1)
  end)
  -- 初始化位置
  if not LuaOffWorldPassMgr.OpenTabIndex or LuaOffWorldPassMgr.OpenTabIndex == 0 then
    self:OnSelectTab(0)
  else
    self:OnSelectTab(1)
  end


  if LuaOffWorldPassMgr.page1data.pass2enable > 0 then
    self.unlockBtn1:SetActive(false)
		self.unlockNode1:SetActive(true)
  else
    self.unlockBtn1:SetActive(true)
		self.unlockNode1:SetActive(false)
    UIEventListener.Get(self.unlockBtn1).onClick = DelegateFactory.VoidDelegate(function(go)
      CUIManager.ShowUI(CLuaUIResources.OffWorldPassDetailWnd)
    end)
  end
  if LuaOffWorldPassMgr.page1data.pass3enable > 0 then
    self.unlockBtn2:SetActive(false)
		self.unlockNode2:SetActive(true)
  else
    self.unlockBtn2:SetActive(true)
		self.unlockNode2:SetActive(false)
    UIEventListener.Get(self.unlockBtn2).onClick = DelegateFactory.VoidDelegate(function(go)
      CUIManager.ShowUI(CLuaUIResources.OffWorldPassDetailWnd)
			--fuben2BonusBtn.transform:Find('fx'):GetComponent(typeof(CUIFx)):LoadFx('fx/ui/prefab/UI_shenwutongxingzheng_libaojiesuo.prefab')
    end)
  end
end
