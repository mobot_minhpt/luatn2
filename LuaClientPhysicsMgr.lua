LuaClientPhysicsMgr = {}

-- 等于nil表示没有初始化
-- 等于false表示没有对应的action
LuaClientPhysicsMgr.s_OnEnterSteerFunc = {}
LuaClientPhysicsMgr.s_OnLeaveSteerFunc = {}


function LuaClientPhysicsMgr.GetOnEnterSteerFunc(id)
    return LuaClientPhysicsMgr.GetSteerFunc(true,id)
end
function LuaClientPhysicsMgr.GetOnLeaveSteerFunc(id)
    return LuaClientPhysicsMgr.GetSteerFunc(false,id)
end
function LuaClientPhysicsMgr.GetSteerFunc(bEnter,id)
    local funcs = bEnter and LuaClientPhysicsMgr.s_OnEnterSteerFunc or LuaClientPhysicsMgr.s_OnLeaveSteerFunc

    if funcs[id]==false then return nil end
    if funcs[id] then return funcs[id] end

    local designData = Physics_AI.GetData(id)
    if designData then
        local raw = nil
        if bEnter then
            raw = designData.OnEnterSteer
        else
            raw = designData.OnLeaveSteer
        end
        -- print(raw)
        if raw and raw~="" then
            funcs[id] = {}
            for EventStr in string.gmatch(raw, "[^;]+") do
                local EventName, EventParam = EventStr, {}
                -- print(EventStr)
                if( string.find(EventStr,"%(") ) then
                    local ParamsStr
                    EventName, ParamsStr = string.match(EventStr, "([^%(]*)%(([^%)]*)%)")
                    EventParam = { loadstring("return "..ParamsStr)() }
                end
                if EventName then
                    local FuncName = SteerEnterLeaveFunNameMap[EventName]
                    if FuncName then
                        table.insert(funcs[id], { FuncName, EventParam })
                    end
                end
            end
            return funcs[id]
        else
            funcs[id]=false
        end
    else
        funcs[id]=false
    end
    return nil
end
