-- Auto Generated!!
local BoolDelegate = import "UIEventListener+BoolDelegate"
local CommonDefs = import "L10.Game.CommonDefs"
local CQueQiaoMgr = import "L10.Game.CQueQiaoMgr"
local CQueQiaoPoemWnd = import "L10.UI.CQueQiaoPoemWnd"
local CRecordingInfoMgr = import "L10.UI.CRecordingInfoMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumRecordContext = import "L10.Game.EnumRecordContext"
local EnumVoicePlatform = import "L10.Game.EnumVoicePlatform"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
--鹊桥对诗
CQueQiaoPoemWnd.m_Init_CS2LuaHook = function (this) 
    this.questionLabel.text = CQueQiaoMgr.Inst.poemQuestion
    this.aliveDuration = CQueQiaoMgr.Inst.poemDuration
    this:ShowCountDownValue()
    this.lastUpdateTime = 0
end
CQueQiaoPoemWnd.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.voiceBtn).onPress = CommonDefs.CombineListner_BoolDelegate(UIEventListener.Get(this.voiceBtn).onPress, MakeDelegateFromCSFunction(this.OnVoiceButtonPress, BoolDelegate, this), true)
    UIEventListener.Get(this.sendBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.sendBtn).onClick, MakeDelegateFromCSFunction(this.OnSendButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    this.closeBtn:SetActive(false)
end
CQueQiaoPoemWnd.m_ShowCountDownValue_CS2LuaHook = function (this) 
    local color = "EEBA26"
    if this.aliveDuration < this.CountDownColorThreshold then
        color = "FF0000"
    end
    this.countDownLabel.text = System.String.Format("[{0}]{1}s[-]", color, math.floor(this.aliveDuration))
end
CQueQiaoPoemWnd.m_OnVoiceButtonPress_CS2LuaHook = function (this, go, pressed) 
    if pressed then
        CRecordingInfoMgr.ShowRecordingWnd(EnumRecordContext.QueQiao, EChatPanel.Undefined, go, EnumVoicePlatform.Default, false)
    else
        CRecordingInfoMgr.CloseRecordingWnd(not go:Equals(CUICommonDef.SelectedUI))
    end
end
CQueQiaoPoemWnd.m_OnSendButtonClick_CS2LuaHook = function (this, go) 
    local val = StringTrim(this.input.value)
    if System.String.IsNullOrEmpty(val) then
        return
    end
    Gac2Gas.SubmitQueQiaoPoemAnswer(val)
end

