local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local Time = import "UnityEngine.Time"
local CChatLinkMgr = import "CChatLinkMgr"
local Quaternion = import "UnityEngine.Quaternion"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"

LuaVoiceAchievemenAwardPreviewWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaVoiceAchievemenAwardPreviewWnd, "Previewer", "Previewer", CUITexture)
RegistChildComponent(LuaVoiceAchievemenAwardPreviewWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaVoiceAchievemenAwardPreviewWnd, "RightBtn", "RightBtn", GameObject)
RegistChildComponent(LuaVoiceAchievemenAwardPreviewWnd, "LeftBtn", "LeftBtn", GameObject)
RegistChildComponent(LuaVoiceAchievemenAwardPreviewWnd, "DescLabel", "DescLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaVoiceAchievemenAwardPreviewWnd, "m_ModelTextureLoader")
RegistClassMember(LuaVoiceAchievemenAwardPreviewWnd, "m_ModelTexture")
RegistClassMember(LuaVoiceAchievemenAwardPreviewWnd, "Rotation")

function LuaVoiceAchievemenAwardPreviewWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
	UIEventListener.Get(self.LeftBtn).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        self.m_LeftPressed = isPressed
    end)
    UIEventListener.Get(self.RightBtn).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        self.m_RightPressed = isPressed
    end)
	self.Rotation = 0
	self.m_StartPressTime = 0
    self.m_DeltaTime = 0.1
	self.m_DeltaRot = -10

    local texture = self.Previewer.transform:GetComponent(typeof(UITexture))
    local shader = ShaderEx.Find("Unlit/Premultiplied Colored")
    if shader then
        texture.shader = shader
    end
end

function LuaVoiceAchievemenAwardPreviewWnd:Init()
	
	self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadModel(ro)
    end)
	self.Previewer.mainTexture=CUIManager.CreateModelTexture("__VoiceAchiement__", self.m_ModelTextureLoader,self.Rotation,0,-0.45,3,false,true,0.8)
end
function LuaVoiceAchievemenAwardPreviewWnd:Update()
    if self.m_LeftPressed and Time.realtimeSinceStartup - self.m_StartPressTime > self.m_DeltaTime then
        self.Rotation = self.Rotation + self.m_DeltaRot
        self.m_StartPressTime = Time.realtimeSinceStartup
        CUIManager.SetModelRotation("__VoiceAchiement__", self.Rotation)
    elseif self.m_RightPressed and Time.realtimeSinceStartup - self.m_StartPressTime > self.m_DeltaTime then
        self.Rotation = self.Rotation - self.m_DeltaRot
        self.m_StartPressTime = Time.realtimeSinceStartup
        CUIManager.SetModelRotation("__VoiceAchiement__", self.Rotation)
    end
end
function LuaVoiceAchievemenAwardPreviewWnd:LoadModel(ro)
    
	local setting = VoiceAchievement_Setting.GetData()
	local itemid = setting.FinalRewardItemID
	local data = Item_Item.GetData(itemid)
	local path = "Item/AniWeapon/wp_umbrella_15/Prefab/wp_umbrella_15_01.prefab"--data.Model 
    local prefabname = "Assets/Res/"..path  

	--self.DescLabel.text = CChatLinkMgr.TranslateToNGUIText(data.Description, false)
    ro:LoadMain(prefabname)
	local fx = CEffectMgr.Inst:AddObjectFX(88803418,ro,0, 1, 1, nil, false, EnumWarnFXType.None,Vector3.zero,Vector3.zero,nil)
	ro.transform.localRotation = Quaternion.Euler(Vector3(0,-114,-33))
	ro:DoAni("umbrella01_loop", true, 0, 1, 0, true, 1)

	
end

--@region UIEvent

--@endregion UIEvent
function LuaVoiceAchievemenAwardPreviewWnd:OnDestroy()
    self.Previewer.mainTexture=nil
    CUIManager.DestroyModelTexture("__VoiceAchiement__")
end
