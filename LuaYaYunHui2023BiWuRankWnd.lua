local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CRankData = import "L10.UI.CRankData"
local QnTableView = import "L10.UI.QnTableView"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CPlayerInfoMgrAlignType = import "CPlayerInfoMgr+AlignType"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local Profession = import "L10.Game.Profession"
local CButton = import "L10.UI.CButton"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Animation = import "UnityEngine.Animation"
local DefaultUISimpleTableViewDataSource = import "L10.UI.DefaultUISimpleTableViewDataSource"
local UISimpleTableView = import "L10.UI.UISimpleTableView"
local UITableTween = import "L10.UI.UITableTween"
LuaYaYunHui2023BiWuRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaYaYunHui2023BiWuRankWnd, "MyTemplate", "MyTemplate", GameObject)
RegistChildComponent(LuaYaYunHui2023BiWuRankWnd, "NoneRankLabel", "NoneRankLabel", UILabel)
RegistChildComponent(LuaYaYunHui2023BiWuRankWnd, "TableView", "TableView", UISimpleTableView)
RegistChildComponent(LuaYaYunHui2023BiWuRankWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaYaYunHui2023BiWuRankWnd, "FormLabel", "FormLabel", UILabel)
RegistChildComponent(LuaYaYunHui2023BiWuRankWnd, "LevelLabel", "LevelLabel", UILabel)
RegistChildComponent(LuaYaYunHui2023BiWuRankWnd, "Desc", "Desc", UILabel)
RegistChildComponent(LuaYaYunHui2023BiWuRankWnd, "ItemCell1", "ItemCell1", GameObject)
RegistChildComponent(LuaYaYunHui2023BiWuRankWnd, "ItemCell2", "ItemCell2", GameObject)
RegistChildComponent(LuaYaYunHui2023BiWuRankWnd, "GoButton", "GoButton", CButton)
RegistChildComponent(LuaYaYunHui2023BiWuRankWnd, "RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaYaYunHui2023BiWuRankWnd, "AwardLabel", "AwardLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaYaYunHui2023BiWuRankWnd, "m_RankID")
RegistClassMember(LuaYaYunHui2023BiWuRankWnd, "m_Ani")
RegistClassMember(LuaYaYunHui2023BiWuRankWnd, "m_TimeList")
RegistClassMember(LuaYaYunHui2023BiWuRankWnd, "m_RankTable")
RegistClassMember(LuaYaYunHui2023BiWuRankWnd, "m_ShowDelayTick")
RegistClassMember(LuaYaYunHui2023BiWuRankWnd, "m_DataSource")
RegistClassMember(LuaYaYunHui2023BiWuRankWnd, "m_Template")
function LuaYaYunHui2023BiWuRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    g_MessageMgr:ShowMessage("YaYunHui2023_JueDouRuleTip")
	end)

    UIEventListener.Get(self.GoButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGoButtonClick()
	end)
    --@endregion EventBind end
    self.m_TimeList = nil
    self.m_DataSource = nil
    self.m_RankTable = {}
    self.m_ShowDelayTick = nil
    self.m_Template = self.TableView.transform:Find("Pool/ItemTemplate").gameObject
    self.m_Template.gameObject:SetActive(false)
    self.gameObject:GetComponent(typeof(UIPanel)).alpha = 0
    self.m_Ani = self.transform:GetComponent(typeof(Animation))
    self.m_RankID = AsianGames2023_Setting.GetData().JueDouRankId

end

function LuaYaYunHui2023BiWuRankWnd:Init()
    if not self.m_DataSource then
        self.m_DataSource = DefaultUISimpleTableViewDataSource.Create(
            function()
                return #self.m_RankTable
            end,
            function(index)
                return self:InitItem(index)
            end
        )
    end

    self.TableView:Clear()
    self.TableView.dataSource = self.m_DataSource

    self.TableView.gameObject:SetActive(false)
    self.NoneRankLabel.gameObject:SetActive(true)
    self:InitMyRank()
    self:InitRight()
    Gac2Gas.QueryRank(self.m_RankID) 
end

function LuaYaYunHui2023BiWuRankWnd:OnRankDataReady()

    if CLuaRankData.m_CurRankId ~= self.m_RankID then return end 
    self.m_RankTable = {}
    if CRankData.Inst.RankList and CRankData.Inst.RankList.Count > 0 then
        self.MyTemplate.gameObject:SetActive(true)
        self.TableView.gameObject:SetActive(true)
        self.NoneRankLabel.gameObject:SetActive(false)
        for i = 1, CRankData.Inst.RankList.Count do
            table.insert(self.m_RankTable, CRankData.Inst.RankList[i - 1])
        end

        if CRankData.Inst.MainPlayerRankInfo then self:InitMyRank(CRankData.Inst.MainPlayerRankInfo) end

    else
        self.TableView.gameObject:SetActive(false)
        self.NoneRankLabel.gameObject:SetActive(true)
	end
    self.TableView:LoadData(0, false)
    self.TableView.table.transform:GetComponent(typeof(UITableTween)):PlayAnim()
    self.m_Ani:Play("yayunhui2023biwurankwnd_dakai")

end

function LuaYaYunHui2023BiWuRankWnd:InitMyRank(data)
    if not CClientMainPlayer.Inst then self.MyTemplate.gameObject:SetActive(false) return end
    self.MyTemplate.gameObject:SetActive(true)
    local rankLabel = self.MyTemplate.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankImage = rankLabel.transform:Find("RankImage"):GetComponent(typeof(UISprite))
    rankLabel.text = data and data.Rank > 0 and tostring(data.Rank) or LocalString.GetString("未上榜")
    if data and data.Rank <= 3 and data.Rank > 0 then
        rankLabel.text = ""
        rankImage.gameObject:SetActive(true)
        if data.Rank == 1 then
            rankImage.spriteName = "Rank_No.1"
        elseif data.Rank == 2 then
            rankImage.spriteName = "Rank_No.2png"
        elseif data.Rank == 3 then
            rankImage.spriteName = "Rank_No.3png"
        end
    else
        rankImage.gameObject:SetActive(false)
    end
    
    self.MyTemplate.transform:Find("CareerIcon"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIcon(CClientMainPlayer.Inst.Class)
    self.MyTemplate.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Name
    local lvLabel = self.MyTemplate.transform:Find("lvLabel"):GetComponent(typeof(UILabel))
    if CClientMainPlayer.Inst.HasFeiSheng then
        lvLabel.text = SafeStringFormat3("lv.%d",CClientMainPlayer.Inst.XianShenLevel)
        lvLabel.color = NGUIText.ParseColor24("ff7900", 0)
    else
        lvLabel.text = SafeStringFormat3("lv.%d",CClientMainPlayer.Inst.Level)
        lvLabel.color = NGUIText.ParseColor24("512E16", 0)
    end
    self.MyTemplate.transform:Find("countabel"):GetComponent(typeof(UILabel)).text = data and tostring(data.Value) or "0"
    local rateLabel = self.MyTemplate.transform:Find("rateLabel"):GetComponent(typeof(UILabel))
    rateLabel.text = "--"
    if data then
        local extraData = g_MessagePack.unpack(data.extraData)
        if extraData and #extraData >= 2 then 
            rateLabel.text = SafeStringFormat3("%d%%",extraData[2] / 10) 
        end
    end
end

function LuaYaYunHui2023BiWuRankWnd:InitItem(index)
    local data = self.m_RankTable[index + 1]
    if not data then return end
    local item = self.TableView:DequeueReusableCellWithIdentifier("ItemTemplate")
    if item == nil then
        item = self.TableView:AllocNewCellWithIdentifier(self.m_Template,"ItemTemplate")
    end
    item.gameObject:SetActive(true)
    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankImage = rankLabel.transform:Find("RankImage"):GetComponent(typeof(UISprite))
    local NameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local lvLabel = item.transform:Find("lvLabel"):GetComponent(typeof(UILabel))
    local CareerIcon = item.transform:Find("CareerIcon"):GetComponent(typeof(UISprite))
    local countabel = item.transform:Find("countabel"):GetComponent(typeof(UILabel))
    local rateLabel = item.transform:Find("rateLabel"):GetComponent(typeof(UILabel))

    local Bg01 = item.transform:Find("Bg01").gameObject
    local Bg02 = item.transform:Find("Bg02").gameObject
    Bg01:SetActive(index%2 == 0)
    Bg02:SetActive(index%2 ~= 0)

    rankLabel.text = ""
    local rank = data.Rank
    if rank <= 0 then
        self:InitMyRank()
        return      
    end
    if rank == 1 then
        rankImage.gameObject:SetActive(true)
        rankImage.spriteName = "Rank_No.1"
    elseif rank == 2 then
        rankImage.gameObject:SetActive(true)
        rankImage.spriteName = "Rank_No.2png"
    elseif rank == 3 then
        rankImage.gameObject:SetActive(true)
        rankImage.spriteName = "Rank_No.3png"

    else
        rankImage.gameObject:SetActive(false)
        rankLabel.text = tostring(rank)
    end
    NameLabel.text = data.Name
    lvLabel.text = SafeStringFormat3("lv.%d",data.Level)
    CareerIcon.spriteName = Profession.GetIcon(data.Job)
    countabel.text = tostring(data.Value)
    local extraData = g_MessagePack.unpack(data.extraData)
    if extraData and #extraData >= 2 then 
        rateLabel.text = SafeStringFormat3("%d%%",extraData[2] / 10) 
        if extraData[1] == 1 then
            lvLabel.color = NGUIText.ParseColor24("ff7900", 0)
        else
            lvLabel.color = NGUIText.ParseColor24("512E16", 0)
        end
    end
    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,CPlayerInfoMgrAlignType.Default)
	end)
    return item
end

function LuaYaYunHui2023BiWuRankWnd:InitRight()
    self.TimeLabel.text = AsianGames2023_Setting.GetData().JueDouPlayTimeShowText
    self.FormLabel.text = AsianGames2023_Setting.GetData().JueDouFormShowText
    self.LevelLabel.text = SafeStringFormat3(LocalString.GetString("%d级"),AsianGames2023_Setting.GetData().JueDouLevelLimit) 
    self.Desc.text = SafeStringFormat3("[381F00]%s[-]",g_MessageMgr:FormatMessage("YaYunHui2023_JueDouTaskDesc")) 


    local dailyItemId = AsianGames2023_Setting.GetData().JueDouDailyAwardTemplateId
    local rankItemId = AsianGames2023_Setting.GetData().JueDouRankAwardTemplateId

    local dailyItemData = Item_Item.GetData(dailyItemId)
    local rankItemData = Item_Item.GetData(rankItemId)

    self.ItemCell1.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(rankItemData.Icon)
    self.ItemCell2.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(dailyItemData.Icon)

    UIEventListener.Get(self.ItemCell1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CItemInfoMgr.ShowLinkItemTemplateInfo(rankItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
	end)

    UIEventListener.Get(self.ItemCell2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CItemInfoMgr.ShowLinkItemTemplateInfo(dailyItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
	end)

    self:SetBtnStatus()

    local awardTimes = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eAsianGames2023JueDouDailyAwardTimes) or 0
    local awardLimit = AsianGames2023_Setting.GetData().JueDouDailyAwardLimit
    if awardTimes >= awardLimit then
        self.AwardLabel.text = SafeStringFormat3(LocalString.GetString("[381F00]每日参与奖 %d/%d[-]"),awardTimes,awardLimit)
    else
        self.AwardLabel.text = SafeStringFormat3(LocalString.GetString("[381F00]每日参与奖 [D84434]%d[-]/%d[-]"),awardTimes,awardLimit)
    end
    
end

function LuaYaYunHui2023BiWuRankWnd:SetBtnStatus()
    local timeList = AsianGames2023_Setting.GetData().JueDouPeriods
    local timeData = CServerTimeMgr.Inst:GetZone8Time()
    local curTime = timeData.Hour * 60 + timeData.Minute
    if not self.m_TimeList then
        self.m_TimeList = {}
        for i = 0,timeList.Length - 1 do
            if not System.String.IsNullOrEmpty(timeList[i]) then
                
                local startHour,startMinute,endHour,endMinute = string.match(timeList[i], "(%d+):(%d+)-(%d+):(%d+)")
                table.insert(self.m_TimeList,{
                    startTime = startHour * 60 + startMinute,
                    endTime = endHour * 60 + endMinute,
                    timeStr = timeList[i],
                })
            end
        end
    end
    local cnt = 0
    for i = 1,#self.m_TimeList do
        local val = self.m_TimeList[i]
        if curTime >= val.startTime and curTime <= val.endTime then
            self.GoButton.Enabled = true
            self.GoButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("前往杭州城")
            return
        elseif curTime < val.startTime then
            self.GoButton.Enabled = false
            self.GoButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = val.timeStr
            return
        end
    end
    local timeEndList = AsianGames2023_Setting.GetData().JueDouLastTimeDate
    local now = CServerTimeMgr.Inst:GetZone8Time()
    for i = 0,timeEndList.Length - 1 do
        if not System.String.IsNullOrEmpty(timeEndList[i]) then
            local time = CServerTimeMgr.ConvertTimeStampToZone8Time(CServerTimeMgr.Inst:GetTimeStampByStr(timeEndList[i]))
            if now.Year == time.Year and now.Month == time.Month and now.Day == time.Day then
                self.GoButton.Enabled = false
                self.GoButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("今日已结束")
                return
            end
        end
    end
    self.GoButton.Enabled = false
    self.GoButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("明日 %s"),self.m_TimeList[1].timeStr)
end

function LuaYaYunHui2023BiWuRankWnd:OnGoButtonClick()
    local sceneId = AsianGames2023_Setting.GetData().JueDouSceneTemplateIds[0]
    Gac2Gas.LevelTeleport(sceneId)
end

function LuaYaYunHui2023BiWuRankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaYaYunHui2023BiWuRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
    if self.m_ShowDelayTick then UnRegisterTick(self.m_ShowDelayTick) self.m_ShowDelayTick = nil end
end

--@region UIEvent

--@endregion UIEvent

