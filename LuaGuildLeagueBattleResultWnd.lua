local PlayerSettings = import "L10.Game.PlayerSettings"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local UIEventListener = import "UIEventListener"
local LocalString = import "LocalString"
local Int32 = import "System.Int32"
local GuildLeagueViceBattleResultInfo = import "L10.Game.GuildLeagueViceBattleResultInfo"
local GuildLeagueMainBattleResultInfo = import "L10.Game.GuildLeagueMainBattleResultInfo"
local CGuildLeagueBattleResultInfoPane=import "L10.UI.CGuildLeagueBattleResultInfoPane"
local UITabBar=import "L10.UI.UITabBar"
local QnCheckBox=import "L10.UI.QnCheckBox"

local GameObject = import "UnityEngine.GameObject"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CGuildLeagueMgr = import "L10.Game.CGuildLeagueMgr"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local AlignType = import  "L10.UI.CPopupMenuInfoMgr+AlignType"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"


LuaGuildLeagueBattleResultWnd = class()
RegistClassMember(LuaGuildLeagueBattleResultWnd,"titleLabel")
RegistClassMember(LuaGuildLeagueBattleResultWnd,"leftPane")
RegistClassMember(LuaGuildLeagueBattleResultWnd,"rightPane")
RegistClassMember(LuaGuildLeagueBattleResultWnd,"tabBar")
RegistClassMember(LuaGuildLeagueBattleResultWnd,"mainRequested")
RegistClassMember(LuaGuildLeagueBattleResultWnd,"viceRequested")
RegistClassMember(LuaGuildLeagueBattleResultWnd,"switchButton")
RegistClassMember(LuaGuildLeagueBattleResultWnd,"hideNoneEnmeyCheckbox")
RegistClassMember(LuaGuildLeagueBattleResultWnd,"showJiangJunWei")
RegistClassMember(LuaGuildLeagueBattleResultWnd,"infoLabel")
RegistClassMember(LuaGuildLeagueBattleResultWnd,"mainInfo1")
RegistClassMember(LuaGuildLeagueBattleResultWnd,"mainInfo2")
RegistClassMember(LuaGuildLeagueBattleResultWnd,"viceInfo1")
RegistClassMember(LuaGuildLeagueBattleResultWnd,"viceInfo2")

RegistChildComponent(LuaGuildLeagueBattleResultWnd, "ShenShouButton", "ShenShouButton", GameObject)
RegistChildComponent(LuaGuildLeagueBattleResultWnd, "ShenShouLabel", "ShenShouLabel", UILabel)
RegistChildComponent(LuaGuildLeagueBattleResultWnd, "ShenShouSprite", "ShenShouSprite", GameObject)
RegistChildComponent(LuaGuildLeagueBattleResultWnd, "ShenShouIcon", "ShenShouIcon", CUITexture)
RegistChildComponent(LuaGuildLeagueBattleResultWnd, "ShenShouCountLabel", "ShenShouCountLabel", UILabel)



RegistClassMember(LuaGuildLeagueBattleResultWnd,"m_ShenShouInfo")


function LuaGuildLeagueBattleResultWnd:Awake()
    self.titleLabel = self.transform:Find("Wnd_Bg_Primary_Tab/TitleLabel"):GetComponent(typeof(UILabel))
    self.leftPane = self.transform:Find("Anchor/Left"):GetComponent(typeof(CGuildLeagueBattleResultInfoPane))
    self.rightPane = self.transform:Find("Anchor/Right"):GetComponent(typeof(CGuildLeagueBattleResultInfoPane))
    self.tabBar = self.transform:GetComponent(typeof(UITabBar))
    self.mainRequested = false
    self.viceRequested = false
    self.switchButton = self.transform:Find("Anchor/SwitchButton").gameObject
    self.hideNoneEnmeyCheckbox = self.transform:Find("Anchor/Checkbox"):GetComponent(typeof(QnCheckBox))
    self.showJiangJunWei = true
    self.infoLabel = self.transform:Find("Anchor/Label"):GetComponent(typeof(UILabel))
    self.mainInfo1 = nil
    self.mainInfo2 = nil
    self.viceInfo1 = nil
    self.viceInfo2 = nil

    self.ShenShouButton:SetActive(false)

    self.leftPane.gameObject:SetActive(true)
    self.rightPane.gameObject:SetActive(true)
    --1是将军威 0是玄霄怒
    if CGuildLeagueMgr.Inst:GetMyForce() == 0 then
        self.showJiangJunWei = false
    else
        self.showJiangJunWei = true
    end

    self.titleLabel.text = ""
    UIEventListener.Get(self.switchButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickSwitchButton(go)
    end)

    if self.showJiangJunWei then
        self.infoLabel.text = LocalString.GetString("[c][c43347]显示玄霄怒统计信息[-][/c]")
    else
        self.infoLabel.text = LocalString.GetString("[c][60c8fd]显示将军威统计信息[-][/c]")
    end

    self.tabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function(go,index)
        self:OnTabChange(go,index)
    end)
end

function LuaGuildLeagueBattleResultWnd:Init()
    self.hideNoneEnmeyCheckbox:SetSelected(PlayerSettings.HideNoneEnemyInGuildLeague, true)
    self.hideNoneEnmeyCheckbox.gameObject:SetActive(not LuaGuildLeagueMgr.m_CurrentSituationForWatch) --联赛观战时不显示该选项
end

function LuaGuildLeagueBattleResultWnd:Start()
    Gac2Gas.QueryGuildLeagueInfo()
    self.hideNoneEnmeyCheckbox.OnValueChanged = DelegateFactory.Action_bool(function(val)
        PlayerSettings.HideNoneEnemyInGuildLeague = val
    end)
end
function LuaGuildLeagueBattleResultWnd:OnEnable()
    g_ScriptEvent:AddListener("GetGuildLeagueInfoResult",self,"GetGuildLeagueInfoResult")
    g_ScriptEvent:AddListener("RemoteSummonGuildLeagueMainPlayBossSuccess",self,"OnRemoteSummonGuildLeagueMainPlayBossSuccess")
end
function LuaGuildLeagueBattleResultWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GetGuildLeagueInfoResult",self,"GetGuildLeagueInfoResult")
    g_ScriptEvent:RemoveListener("RemoteSummonGuildLeagueMainPlayBossSuccess",self,"OnRemoteSummonGuildLeagueMainPlayBossSuccess")
end
function LuaGuildLeagueBattleResultWnd:OnRemoteSummonGuildLeagueMainPlayBossSuccess(t)
    self.m_ShenShouInfo = t
    self:RefreshShenShouStatus()
end
function LuaGuildLeagueBattleResultWnd:RefreshShenShouStatus()
    if self.tabBar.SelectedIndex~=0 then 
        self.ShenShouButton:SetActive(false)
        return
    end
    --副战场不显示
    if not CGuildLeagueMgr.Inst.inGuildLeagueMainScene then
        self.ShenShouButton:SetActive(false)
        return
    end

    local myForce = CGuildLeagueMgr.Inst:GetMyForce()

    local show = false
    if self.showJiangJunWei and myForce==1 then
        show = true
    elseif not self.showJiangJunWei and myForce==0 then
        show = true
    end
    self.ShenShouButton:SetActive(show)

    if show then
        if self.m_ShenShouInfo and #self.m_ShenShouInfo>0 then
            CUICommonDef.SetActive(self.ShenShouButton, true, true)
            self.ShenShouSprite.gameObject:SetActive(false)
            self.ShenShouIcon.gameObject:SetActive(true)
            self.ShenShouLabel.gameObject:SetActive(true)
            local templateId = self.m_ShenShouInfo[1]
            if #self.m_ShenShouInfo>1 then
                self.ShenShouCountLabel.gameObject:SetActive(true)
                self.ShenShouCountLabel.text = tostring(#self.m_ShenShouInfo)
            else
                self.ShenShouCountLabel.gameObject:SetActive(false)
            end
            local designData = Monster_Monster.GetData(templateId)
            local shenShouName = ""
            if designData then
                self.ShenShouIcon:LoadNPCPortrait(designData.HeadIcon)
                shenShouName = designData.Name
            end

            UIEventListener.Get(self.ShenShouButton).onClick = DelegateFactory.VoidDelegate(function(go)
                local tbl = {}
                table.insert(tbl, PopupMenuItemData(SafeStringFormat3(LocalString.GetString("上路召唤%s"),shenShouName),
                    DelegateFactory.Action_int(function (index)
                        self:DoSummon(1,shenShouName)
                    end), false, nil))
                table.insert(tbl, PopupMenuItemData(SafeStringFormat3(LocalString.GetString("中路召唤%s"),shenShouName),
                    DelegateFactory.Action_int(function (index)
                        self:DoSummon(2,shenShouName)
                    end), false, nil))
                table.insert(tbl, PopupMenuItemData(SafeStringFormat3(LocalString.GetString("下路召唤%s"),shenShouName),
                    DelegateFactory.Action_int(function (index)
                        self:DoSummon(3,shenShouName)
                    end), false, nil))
                
                local array = Table2Array(tbl, MakeArrayClass(PopupMenuItemData))
                CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType.Top)
            end)
        else
            CUICommonDef.SetActive(self.ShenShouButton, false, true)
            self.ShenShouSprite.gameObject:SetActive(true)
            self.ShenShouIcon.gameObject:SetActive(false)
            self.ShenShouLabel.gameObject:SetActive(false)
        end
    end
end
function LuaGuildLeagueBattleResultWnd:DoSummon(road,shenShouName)
    local roadstr = ""
    if road==1 then
        roadstr = LocalString.GetString("上路")
    elseif road==2 then
        roadstr = LocalString.GetString("中路")
    elseif road==3 then
        roadstr = LocalString.GetString("下路")
    end
    local msg = g_MessageMgr:FormatMessage("GUILD_LEAGUE_CONFIRM_MOTIVE_SUMMON_MONSTER",shenShouName,GuildLeague_Setting.GetData().RemoteSummonBossDelay,roadstr)
    g_MessageMgr:ShowOkCancelMessage(msg, function()
        Gac2Gas.RequestSummonGuildLeagueMainPlayBoss(road)
    end, nil,nil,nil,false)
end

function LuaGuildLeagueBattleResultWnd:OnClickSwitchButton( go)
    if self.showJiangJunWei then
        EventManager.BroadcastInternalForLua(EnumEventType.GuildLeagueSwitchDisplay, {false})
        self.showJiangJunWei = false
        self.infoLabel.text = LocalString.GetString("[c][60c8fd]显示将军威统计信息[-][/c]")
    else
        EventManager.BroadcastInternalForLua(EnumEventType.GuildLeagueSwitchDisplay, {true})
        self.showJiangJunWei = true
        self.infoLabel.text = LocalString.GetString("[c][c43347]显示玄霄怒统计信息[-][/c]")
    end
    self:RefreshShenShouStatus()
end
function LuaGuildLeagueBattleResultWnd:OnTabChange( go, index)
    if index == 0 then
        self.titleLabel.text = LocalString.GetString("主战场战况查询")
        if not self.mainRequested then
            Gac2Gas.QueryGuildLeagueMainInfo()
        else
            self.leftPane:Init(self.mainInfo1, true)
            self.rightPane:Init(self.mainInfo2, false)
        end
    else
        self.titleLabel.text = LocalString.GetString("副战场战况查询")
        if not self.viceRequested then
            Gac2Gas.QueryGuildLeagueViceInfo()
        else
            self.leftPane:Init(self.viceInfo1, true)
            self.rightPane:Init(self.viceInfo2, false)
        end
    end
    self:RefreshShenShouStatus()
end
function LuaGuildLeagueBattleResultWnd:GetGuildLeagueInfoResult( isMain, info1, info2)
    if isMain then
        self.mainInfo1 = TypeAs(info1, typeof(GuildLeagueMainBattleResultInfo))
        self.mainInfo2 = TypeAs(info2, typeof(GuildLeagueMainBattleResultInfo))
        self.mainRequested = true
        self.tabBar:ChangeTab(0, false)

        self.leftPane:Init(self.mainInfo1, true)
        self.rightPane:Init(self.mainInfo2, false)
    else
        self.viceInfo1 = TypeAs(info1, typeof(GuildLeagueViceBattleResultInfo))
        self.viceInfo2 = TypeAs(info2, typeof(GuildLeagueViceBattleResultInfo))
        self.viceRequested = true
        self.tabBar:ChangeTab(1, false)
        self.leftPane:Init(self.viceInfo1, true)
        self.rightPane:Init(self.viceInfo2, false)
    end
    EventManager.BroadcastInternalForLua(EnumEventType.GuildLeagueSwitchDisplay, {self.showJiangJunWei})
end

