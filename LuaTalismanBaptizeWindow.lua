local EnumTalismanTabWnd = import "L10.UI.EnumTalismanTabWnd"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CTalismanBaptizeDetailView=import "L10.UI.CTalismanBaptizeDetailView"
local UITabBar=import "L10.UI.UITabBar"
local CCommonLuaScript=import "L10.UI.CCommonLuaScript"
local CTalismanWndMgr=import "L10.UI.CTalismanWndMgr"

CLuaTalismanBaptizeWindow = class()
RegistClassMember(CLuaTalismanBaptizeWindow,"itemList")
RegistClassMember(CLuaTalismanBaptizeWindow,"detailView")
RegistClassMember(CLuaTalismanBaptizeWindow,"inlayDetailView")
RegistClassMember(CLuaTalismanBaptizeWindow,"tabBar")
RegistClassMember(CLuaTalismanBaptizeWindow,"m_CurItemInfo")

function CLuaTalismanBaptizeWindow:Awake()
    local script = self.transform:Find("TaslimanList"):GetComponent(typeof(CCommonLuaScript))
    script:Awake()
    self.itemList =script.m_LuaSelf

    self.detailView = self.transform:Find("BaptizeDetailView"):GetComponent(typeof(CTalismanBaptizeDetailView))
    local script = self.transform:Find("InlayStoneDetailView"):GetComponent(typeof(CCommonLuaScript))
    script:Awake()
    self.inlayDetailView =script.m_LuaSelf


    self.tabBar = self.transform:Find("TabBar"):GetComponent(typeof(UITabBar))
    self.m_CurItemInfo = nil

end

function CLuaTalismanBaptizeWindow:Init()
    self.tabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function(go,index)
        self:OnTabBarChanged(go,index)
    end)
    self.itemList.OnTalismanSelect = function(info)
         self:OnTalismanSelect(info)
    end
    self.tabBar:ChangeTab(CTalismanWndMgr.baptizeTabIndex)
end

function CLuaTalismanBaptizeWindow:OnTalismanSelect( info) 
    self.m_CurItemInfo = info
    CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize = self.m_CurItemInfo
    CEquipmentProcessMgr.Inst.SelectEquipment = self.m_CurItemInfo
    if self.tabBar.SelectedIndex > 0 then
        self.detailView:OnTalismanSelect(self.m_CurItemInfo)
    else
        local default = info and info.itemId or ""
        self.inlayDetailView:Init(default, true)
    end
end
function CLuaTalismanBaptizeWindow:OnTabBarChanged( go, index) 
    if index == 0 then
        self.detailView.gameObject:SetActive(false)
        self.inlayDetailView.gameObject:SetActive(true)
        self.itemList:Init(EnumTalismanTabWnd.inlay, true)
        if self.m_CurItemInfo ~= nil then
            self.inlayDetailView:Init(self.m_CurItemInfo.itemId, true)
        end
    elseif index == 1 then
        self.detailView.gameObject:SetActive(true)
        self.inlayDetailView.gameObject:SetActive(false)
        self.detailView:Init(index - 1)
        self.itemList:Init(EnumTalismanTabWnd.baptize, true)
    elseif index == 2 then
        self.detailView.gameObject:SetActive(true)
        self.inlayDetailView.gameObject:SetActive(false)
        self.detailView:Init(index - 1)
        self.itemList:Init(EnumTalismanTabWnd.baptize, false)
    end
end

