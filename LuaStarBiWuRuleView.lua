local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CChatLinkMgr = import "CChatLinkMgr"
local CLoginMgr = import "L10.Game.CLoginMgr"
LuaStarBiWuRuleView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaStarBiWuRuleView, "Item", "Item", GameObject)
RegistChildComponent(LuaStarBiWuRuleView, "RuleItemTable", "RuleItemTable", GameObject)
RegistChildComponent(LuaStarBiWuRuleView, "CompareRule", "CompareRule", GameObject)

--@endregion RegistChildComponent end

function LuaStarBiWuRuleView:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.Item:SetActive(false)
    self.CompareRule:SetActive(false)
    self:InitView()
end

function LuaStarBiWuRuleView:InitView()
    local isQuDao = not CLuaStarBiwuMgr:GetPlayerIsGuanFu()
    Extensions.RemoveAllChildren(self.RuleItemTable.transform)
    if CLuaStarBiwuMgr.ShowSaiZhiCompare then
        self:InitCompareRule()
    end

    StarBiWuShow_SaiZhiRule.Foreach(function(k,v)
        local ruleText = isQuDao and v.QuDaoFu or v.GuanFu
        if not System.String.IsNullOrEmpty(ruleText) then 
            local ruleItem = NGUITools.AddChild(self.RuleItemTable.gameObject, self.Item)
            ruleItem.gameObject:SetActive(true)
            ruleItem.transform:Find("Content"):GetComponent(typeof(UILabel)).text = CChatLinkMgr.TranslateToNGUIText(ruleText, false)
            ruleItem.transform:Find("Title"):GetComponent(typeof(UILabel)).text= CChatLinkMgr.TranslateToNGUIText(v.Title, false)
        end
    end)
    self.RuleItemTable:GetComponent(typeof(UITable)):Reposition()
end

function LuaStarBiWuRuleView:InitCompareRule()
    local CompareList = StarBiWuShow_Setting.GetData().SaiZhiRuleCompare
    if not CompareList or CompareList.Length < 2 then return end
    local count = 0
    local ruleTableItem = NGUITools.AddChild(self.RuleItemTable.gameObject, self.CompareRule)
    ruleTableItem.gameObject:SetActive(true)
    local ruleTable = ruleTableItem.transform:Find("CompareRuleTable")
    for i = 0, CompareList.Length - 1 do
        for j = 0, CompareList[i].Length - 1 do
            local tipText = CompareList[i][j]
            local labelItem = ruleTable.transform:GetChild(count)
            if labelItem then
                labelItem:GetComponent(typeof(UILabel)).text = tipText
            end
            count = count + 1
        end
    end
    ruleTable:GetComponent(typeof(UITable)):Reposition()
end

function LuaStarBiWuRuleView:OnEnable()
    g_ScriptEvent:BroadcastInLua("OnStarBiWuDetailInfoWndChange",LocalString.GetString("规则"))
end

--@region UIEvent

--@endregion UIEvent

