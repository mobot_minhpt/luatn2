local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UITable = import "UITable"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"
local CGameReplayMgr = import "L10.Game.CGameReplayMgr"
local Extensions = import "Extensions"

LuaFightingSpiritRoundRaceRoot = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFightingSpiritRoundRaceRoot, "JumpBtn", "JumpBtn", GameObject)
RegistChildComponent(LuaFightingSpiritRoundRaceRoot, "WildCardRaceTeamplate", "WildCardRaceTeamplate", GameObject)
RegistChildComponent(LuaFightingSpiritRoundRaceRoot, "WildCardRankTeamplate", "WildCardRankTeamplate", GameObject)
RegistChildComponent(LuaFightingSpiritRoundRaceRoot, "WildCardScrollview", "WildCardScrollview", CUIRestrictScrollView)
RegistChildComponent(LuaFightingSpiritRoundRaceRoot, "WildCardTable", "WildCardTable", UITable)
RegistChildComponent(LuaFightingSpiritRoundRaceRoot, "DeleteBtn", "DeleteBtn", GameObject)

--@endregion RegistChildComponent end

function LuaFightingSpiritRoundRaceRoot:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.JumpBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJumpBtnClick()
	end)


	
	UIEventListener.Get(self.DeleteBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDeleteBtnClick()
	end)


    --@endregion EventBind end

    self.JumpBtnLabel = self.JumpBtn.transform:Find("JumpBtnLabel"):GetComponent(typeof(UILabel))
end

-- 初始化主赛事数据
-- 输入参数是List<CFightingSpiritMatchRecord>
function LuaFightingSpiritRoundRaceRoot:ShowData(args)
	self.WildCardRaceTeamplate:SetActive(false)
	self.WildCardRankTeamplate:SetActive(false)

    local matchList = args[0]
    self.m_MatchRecordData = matchList
	self.m_WildCardRankDataMap = {}
	self.m_WildCardRankDataList = {}

	local map1 = {1,5,2,6,3,7,4,8}
    local map2 = {1,4,2,5,3,6}
    local map3 = {1,3,2}
    local mapList = {map1, map1, map1, map2, map3}
    local statIndex = {0,8,16,24,30}

    self.m_MatchMap = {}

    local firstCount = 0
    for k, map in ipairs(mapList) do
        local reverseList = {}
        for i, index in ipairs(map) do
            reverseList[index]= i
        end

        for i, index in ipairs(reverseList) do
            table.insert(self.m_MatchMap, index + firstCount)
        end
        firstCount = firstCount + #map
    end


	self.m_MapList = mapList
	self.m_WildCardRaceData = {}

	for i=1, 5 do
		local race = {}

		-- 显示数据的时候 需要按 1 5 2 6这样塞进去
		local map = mapList[i]
		for ii, j in ipairs(map) do
			local index = statIndex[i] + j

			if matchList.Count >= index then
				local matchData = matchList[index-1]
				table.insert(race, matchData)

				-- 更新排名数据
				if matchData.WinnerID >0 then
					self:UpdateRankData(matchData.ServerName1, i, matchData.ServerName2, 1 == matchData.WinnerID)
					self:UpdateRankData(matchData.ServerName2, i, matchData.ServerName1, 2 == matchData.WinnerID)
				end
			end
		end

		self.m_WildCardRaceData[i] = race
	end

	-- 排个名
	for k, v in pairs(self.m_WildCardRankDataMap) do
        table.insert(self.m_WildCardRankDataList, v)
    end

    table.sort(self.m_WildCardRankDataList, function(a, b)
        if a.winCount == b.winCount then
            if a.loseCount  ~= b.loseCount then
                return a.loseCount < b.loseCount
            else
                return CommonDefs.StringCompareTo(a.name, b.name) > 0
            end
        else
            return a.winCount > b.winCount
        end
    end)

    self:InitWildCardShow()
end

function LuaFightingSpiritRoundRaceRoot:UpdateRankData(name, round, opponent, hasWin)
    local data = self.m_WildCardRankDataMap[name]

    if data == nil then
        data = {}
        data.name = name
        data.winCount = 0
        data.loseCount = 0
        data.opponentList = {}
        data.winMarkList = {}
        self.m_WildCardRankDataMap[name] = data
    end

    if hasWin then
        data.winCount = data.winCount + 1
    else
        data.loseCount = data.loseCount + 1
    end

    data.opponentList[round] = opponent
    data.winMarkList[round] = hasWin
end

function LuaFightingSpiritRoundRaceRoot:InitWildCardShow()
    Extensions.RemoveAllChildren(self.WildCardTable.transform)
    self.m_RaceItem = {}

    -- 设置对阵
    if self.m_WildCardRaceData then
        self:InitWildCardRace(self.m_WildCardRaceData)
    end
    
    -- 设置排名
    if self.m_WildCardRankDataList then
        self:InitWildCardRank(self.m_WildCardRankDataList)
    end
    
    self.WildCardTable:Reposition()
end

-- 根据数据设置排名
function LuaFightingSpiritRoundRaceRoot:InitWildCardRank(rankList)
    local rankItem = NGUITools.AddChild(self.WildCardTable.gameObject, self.WildCardRankTeamplate)
    rankItem:SetActive(true)

    local label =  rankItem.transform:Find("Title/1/TitleLabel"):GetComponent(typeof(UILabel))
    label.text = LocalString.GetString("队伍名称")

    self.m_JumpItem = rankItem
    self.m_RealJumpItem = rankItem.transform:Find("Title").gameObject
    
    local table = rankItem.transform:Find("TeamInfoTable"):GetComponent(typeof(UITable))
    local template = rankItem.transform:Find("TeamInfoTemplate").gameObject

    template:SetActive(false)
    table.gameObject:SetActive(true)

    Extensions.RemoveAllChildren(table.transform)

    for i=1, #rankList do
        local g = NGUITools.AddChild(table.gameObject, template)
        g.gameObject:SetActive(true)

        if i <= 8 then
            self.m_RealJumpItem = g.gameObject
        end

        local numLabel = g.transform:Find("Num"):GetComponent(typeof(UILabel))
        local teamNameLabel = g.transform:Find("TeamName"):GetComponent(typeof(UILabel))
        local score = g.transform:Find("Score"):GetComponent(typeof(UILabel))
        local encounter = g.transform:Find("Table")

		-- 对阵数据
        local rankData = rankList[i]

        numLabel.text = ""
        numLabel.gameObject:SetActive(false)
        teamNameLabel.text = rankData.name
        score.text = SafeStringFormat3(LocalString.GetString("[00FF00]%s[-]胜[FF0000]%s[-]败"), tostring(rankData.winCount), 
			tostring(rankData.loseCount)) 
        
        if rankData then
            local opList = rankData.opponentList
            local winList = rankData.winMarkList

            for j=1,5 do
                local item = encounter:Find(tostring(j))

                local nameLabel = item:GetComponent(typeof(UILabel))
                local bg = item:Find("Bg"):GetComponent(typeof(UISprite))

                -- 对阵名称
                if opList[j] then
                    item.gameObject:SetActive(true)
                    nameLabel.text = opList[j]

                    -- 判断胜负
                    if winList[j] then
                        bg.color = Color(77/255,128/255,214/255,50/255)
                    else
                        bg.color = Color(77/255,128/255,214/255,0/255)
                    end
                else
                    item.gameObject:SetActive(false)
                end
            end
        else
            for j=1,5 do
                local item = encounter:Find(tostring(j))
                item.gameObject:SetActive(false)
            end
        end
    end

    table:Reposition()

    local bg = rankItem.transform:Find("Bg"):GetComponent(typeof(UISprite))
    bg.gameObject:SetActive(true)
    local box = NGUIMath.CalculateRelativeWidgetBounds(rankItem.transform)
    bg.height = box.size.y
end

-- 初始化对战节点 包含若干队伍
function LuaFightingSpiritRoundRaceRoot:InitWildCardRace(wildCardData)
    

    for i=1, #wildCardData do
        local item = NGUITools.AddChild(self.WildCardTable.gameObject, self.WildCardRaceTeamplate)
        item:SetActive(true)
        local raceData = wildCardData[i]

        local titleLabel = item.transform:Find("TitleBg/TitleLabel"):GetComponent(typeof(UILabel))
        local voidLabel = item.transform:Find("VoidLabel"):GetComponent(typeof(UILabel))
        local teamTable = item.transform:Find("TeamInfoTable"):GetComponent(typeof(UITable))
        local template = item.transform:Find("TeamInfoTemplate").gameObject

        template:SetActive(false)
        titleLabel.text = SafeStringFormat3(LocalString.GetString("第%s轮"), Extensions.ToChinese(i))

        Extensions.RemoveAllChildren(teamTable.transform)
        
        for j=1, #raceData do
            local g = NGUITools.AddChild(teamTable.gameObject, template)
            g:SetActive(true)
            table.insert(self.m_RaceItem, g)
            self:InitWildCardTeamItem(g, raceData[j],j,i)
        end

        teamTable:Reposition()
    end
    self.WildCardTable:Reposition()
end

-- 初始化队伍信息
function LuaFightingSpiritRoundRaceRoot:InitWildCardTeamItem(item, teamData, index, round)
    local numLabel = item.transform:Find("Num"):GetComponent(typeof(UILabel))
    local teamNameLabel1 = item.transform:Find("TeamName1"):GetComponent(typeof(UILabel))
    local teamNameLabel2 = item.transform:Find("TeamName2"):GetComponent(typeof(UILabel))
    local winMark1 = item.transform:Find("TeamName1/WinMark").gameObject
    local winMark2 = item.transform:Find("TeamName2/WinMark").gameObject
    local vs = item.transform:Find("Vs").gameObject
    local scoreLabel = item.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))

    local bg = item.transform:Find("Bg"):GetComponent(typeof(UISprite))
    if math.floor(index/2)%2==0 then
        bg.color = Color(1,1,1,35/255)
    else
        bg.color = Color(0,0,0,35/255)
    end

    teamNameLabel1.text = teamData.ServerName1
    teamNameLabel2.text = teamData.ServerName2
    scoreLabel.text = ""
    vs:SetActive(true)
	
    numLabel.text = tostring(self.m_MapList[round][index])

    winMark1:SetActive(teamData.WinnerID == 1)
    winMark2:SetActive(teamData.WinnerID == 2)

	self:RefreshItemIcon(item, teamData)
end

-- 刷新跳转按钮设置
function LuaFightingSpiritRoundRaceRoot:Update()
    if self.m_JumpItem then
        local jumpItemY = self.m_JumpItem.transform.localPosition.y
        local scrollY = self.WildCardScrollview.transform.localPosition.y

        if jumpItemY + scrollY > -500 then
            self.m_JumpToTop = true
            self.JumpBtnLabel.text = LocalString.GetString("轮次")
        else
            self.m_JumpToTop = false
            self.JumpBtnLabel.text = LocalString.GetString("对阵")
        end
    end
end


-- 更新图标状态
function LuaFightingSpiritRoundRaceRoot:RefreshItemIcon(item, matchData)
    local viewBtn = item.transform:Find("ViewBtn").gameObject
    local cannotWatchMark = item.transform:Find("CannotWatchMark").gameObject
    local downloadingMark = item.transform:Find("DownloadingMark").gameObject
    local downloadBtn = item.transform:Find("DownloadBtn").gameObject

    cannotWatchMark:SetActive(true)
    viewBtn:SetActive(false)
    downloadingMark:SetActive(false)
    downloadBtn:SetActive(false)
    
    if CFightingSpiritMgr.Instance:CanPlayThisGame(matchData) and CFightingSpiritMgr.Instance:IsGameWatchReplay(matchData.GameIndex) then
        local info = CFightingSpiritMgr.Instance:GetRecordCCPlayInfo(matchData)
        if info then
            cannotWatchMark:SetActive(false)
            viewBtn:SetActive(CGameReplayMgr.Instance:IsFileDownloaded(info.FileName))
            downloadingMark:SetActive(CGameReplayMgr.Instance:IsFileDownloading(info.FileName))
            downloadBtn:SetActive((not CGameReplayMgr.Instance:IsFileDownloading(info.FileName)) and (not CGameReplayMgr.Instance:IsFileDownloaded(info.FileName)))
        end
    end

    UIEventListener.Get(viewBtn).onClick = DelegateFactory.VoidDelegate(function()
        -- 播放函数
        CFightingSpiritMgr.Instance:RequestPlayVideo(matchData)
    end)

    -- 下载和播放 调用相同的接口
    UIEventListener.Get(downloadBtn).onClick = DelegateFactory.VoidDelegate(function()
        -- 播放函数
        CFightingSpiritMgr.Instance:RequestPlayVideo(matchData)
    end)
end

-- 根据文件名来判断是否要更新录像信息
function LuaFightingSpiritRoundRaceRoot:OnVideoFilenameReceive(filename)
    local count = math.min(self.m_MatchRecordData.Count-1, 32)
    if count > 0 then
        for i =0, count do
            local matchData = self.m_MatchRecordData[i]
            local info = CFightingSpiritMgr.Instance:GetRecordCCPlayInfo(matchData)
            if info then
                if filename == info.FileName then
                    local item = self.m_RaceItem[self.m_MatchMap[i+1]]
                    self:RefreshItemIcon(item, matchData)
                end
            end
        end
    end
end

function LuaFightingSpiritRoundRaceRoot:OnEnable()
    if self.m_UrlDownloadedAction == nil then
        self.m_UrlDownloadedAction = DelegateFactory.Action_uint(function(index)
            local count = math.min(self.m_MatchRecordData.Count-1, 32)
            if count >0 then
                for i =0, count do
                    local matchData = self.m_MatchRecordData[i]
                    local item = self.m_RaceItem[self.m_MatchMap[i+1]]
                    self:RefreshItemIcon(item, matchData)
                end
            end
        end)
    end

    if self.m_VideoDownloadedAction == nil then
        self.m_VideoDownloadedAction = DelegateFactory.Action_bool_string(function(flag, filename)
            self:OnVideoFilenameReceive(filename)
        end)
    end

    if self.m_VideoDownloadStartAction == nil then
        self.m_VideoDownloadStartAction = DelegateFactory.Action_string(function(filename)
            self:OnVideoFilenameReceive(filename)
        end)
    end

    if self.m_VideoRemovedAction == nil then
        self.m_VideoRemovedAction = DelegateFactory.Action_string(function(filename)
            self:OnVideoFilenameReceive(filename)
        end)
    end

    EventManager.AddListenerInternal(EnumEventType.FightingSpiritMatchRecordURLDownloadComplete, self.m_UrlDownloadedAction)
    EventManager.AddListenerInternal(EnumEventType.GameVideoDownloaded, self.m_VideoDownloadedAction)
    EventManager.AddListenerInternal(EnumEventType.GameVideoDownloadStart, self.m_VideoDownloadStartAction)
    EventManager.AddListenerInternal(EnumEventType.GameVideoRemoved, self.m_VideoRemovedAction)
end

function LuaFightingSpiritRoundRaceRoot:OnDisable()
    EventManager.RemoveListenerInternal(EnumEventType.FightingSpiritMatchRecordURLDownloadComplete, self.m_UrlDownloadedAction)
    EventManager.RemoveListenerInternal(EnumEventType.GameVideoDownloaded, self.m_VideoDownloadedAction)
    EventManager.RemoveListenerInternal(EnumEventType.GameVideoDownloadStart, self.m_VideoDownloadStartAction)
    EventManager.RemoveListenerInternal(EnumEventType.GameVideoRemoved, self.m_VideoRemovedAction)
end

--@region UIEvent

function LuaFightingSpiritRoundRaceRoot:OnJumpBtnClick()
	if self.m_JumpItem then
        if self.m_JumpToTop then
            self.WildCardScrollview:ResetPosition()
        else
            if self.m_RealJumpItem then
                CUICommonDef.SetFullyVisible(self.m_RealJumpItem.gameObject, self.WildCardScrollview)
            end
        end
    end
end

function LuaFightingSpiritRoundRaceRoot:OnDeleteBtnClick()
    if self.m_MatchRecordData then
        for i =0, self.m_MatchRecordData.Count-1 do
            local matchData = self.m_MatchRecordData[i]
            CFightingSpiritMgr.Instance:DeleteReplayFile(matchData)
        end
    end
end


--@endregion UIEvent

