-- Auto Generated!!
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local CTipTitleItem = import "CTipTitleItem"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CYuanDanMgr = import "L10.Game.CYuanDanMgr"
local CYuanDanQiangDanStartWnd = import "L10.UI.CYuanDanQiangDanStartWnd"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CYuanDanQiangDanStartWnd.m_Init_CS2LuaHook = function (this) 
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(g_MessageMgr:FormatMessage("YUANDAN_QMQD_TIP"))
    Extensions.RemoveAllChildren(this.contentTable.transform)
    if info ~= nil then
        if info.titleVisible then
            local titleGo = CUICommonDef.AddChild(this.contentTable.gameObject, this.titleTemplate)
            titleGo:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(titleGo, typeof(CTipTitleItem)):Init(info.title)
        end
        do
            local i = 0
            while i < info.paragraphs.Count do
                local paragraphGo = CUICommonDef.AddChild(this.contentTable.gameObject, this.paragraphTemplate)
                paragraphGo:SetActive(true)
                CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem)):Init(info.paragraphs[i], 4294967295)
                i = i + 1
            end
        end
        this.contentTable:Reposition()
        this.scrollView:ResetPosition()
    end
    this.joinBtn:SetActive(not CYuanDanMgr.Inst.SignedUp)
    this.cancelBtn:SetActive(CYuanDanMgr.Inst.SignedUp)
    this.joinHint:SetActive(CYuanDanMgr.Inst.SignedUp)
    this.remainTimesLabel.text = System.String.Format(LocalString.GetString("剩余次数: {0}/{1}"), CYuanDanMgr.Inst.DailyTimes, CYuanDanMgr.Inst.MaxDailyTimes)
end
CYuanDanQiangDanStartWnd.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.joinBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.joinBtn).onClick, MakeDelegateFromCSFunction(this.OnJoinButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.cancelBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.cancelBtn).onClick, MakeDelegateFromCSFunction(this.OnCancelButtonClick, VoidDelegate, this), true)
    this.titleTemplate:SetActive(false)
    this.paragraphTemplate:SetActive(false)
end
