local DelegateFactory = import "DelegateFactory"
local CMainCamera     = import "L10.Engine.CMainCamera"
local UIRoot          = import "UIRoot"
local Screen          = import "UnityEngine.Screen"

LuaOverheadButtonWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaOverheadButtonWnd, "anchor")
RegistClassMember(LuaOverheadButtonWnd, "button")
RegistClassMember(LuaOverheadButtonWnd, "buttonLabel")

RegistClassMember(LuaOverheadButtonWnd, "cachedPos")

function LuaOverheadButtonWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
end

-- 初始化UI组件
function LuaOverheadButtonWnd:InitUIComponents()
    self.anchor = self.transform:Find("Anchor")
    self.button = self.anchor:Find("Button").gameObject
    self.buttonLabel = self.button.transform:Find("Label"):GetComponent(typeof(UILabel))
end

function LuaOverheadButtonWnd:InitEventListener()
    UIEventListener.Get(self.button).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)
end


function LuaOverheadButtonWnd:Init()
    self.cachedPos = Vector3(1000, 1000, 1000)
    self:LayoutWnd()
end

function LuaOverheadButtonWnd:Update()
    self:LayoutWnd()
end

function LuaOverheadButtonWnd:LayoutWnd()
    if CommonDefs.IsUnityObjectNull(LuaOverheadButtonMgr.infos.target) then
        CUIManager.CloseUI(CLuaUIResources.OverheadButtonWnd)
        return
    end

    local screenPos = CMainCamera.Main:WorldToScreenPoint(LuaOverheadButtonMgr.infos.target.position)
    screenPos.z = 0
    local nguiWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
    if self.cachedPos == nguiWorldPos then return end

    self.cachedPos = nguiWorldPos
    self.anchor.position = nguiWorldPos

    local localPos = self.anchor.localPosition
    local bounds = NGUIMath.CalculateRelativeWidgetBounds(self.anchor)
    local centerX = localPos.x
    local centerY = localPos.y

    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenWidth = Screen.width * CUIManager.UIMainCamera.rect.width * scale
    local virtualScreenHeight = Screen.height * scale

    -- 左右不超出屏幕范围
    if centerX - bounds.size.x * 0.5 < -virtualScreenWidth * 0.5 then
        centerX = -virtualScreenWidth * 0.5 + bounds.size.x * 0.5
    end
    if centerX + bounds.size.x * 0.5 > virtualScreenWidth * 0.5 then
        centerX = virtualScreenWidth * 0.5 - bounds.size.x * 0.5
    end

    -- 上下不超出屏幕范围
    if centerY + bounds.size.y * 0.5 > virtualScreenHeight * 0.5 then
        centerY = virtualScreenHeight * 0.5 - bounds.size.y * 0.5
    end
    if centerY - bounds.size.y * 0.5 < -virtualScreenHeight * 0.5 then
        centerY = -virtualScreenHeight * 0.5 + bounds.size.y * 0.5
    end

    self.anchor.localPosition = Vector3(centerX, centerY, 0)
end

--@region UIEvent

function LuaOverheadButtonWnd:OnButtonClick()
    if LuaOverheadButtonMgr.infos.clickFunc then
        LuaOverheadButtonMgr.infos.clickFunc()
    end
end

--@endregion UIEvent

LuaOverheadButtonMgr = {}

LuaOverheadButtonMgr.infos = {}

function LuaOverheadButtonMgr:ShowOverheadButton(buttonText, target, clickFunc)
    self.infos = {}
    self.infos.buttonText = buttonText and buttonText or ""
    self.infos.target = target
    self.infos.clickFunc = clickFunc
    CUIManager.ShowUI(CLuaUIResources.OverheadButtonWnd)
end
