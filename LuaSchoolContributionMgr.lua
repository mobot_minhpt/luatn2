local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"

CLuaSchoolContributionMgr={}
g_LuaSchoolContributionMgr = CLuaSchoolContributionMgr

function CLuaSchoolContributionMgr:OnMainPlayerCreated()
    local prop = g_LuaSchoolContributionMgr:GetSchoolContributionProp()
    if not prop then return end

    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp then
        CClientMainPlayer.Inst.PlayProp:SetScoreByKey(EnumPlayScoreKey.SchoolContribution, prop.CurrentContribution);
		EventManager.Broadcast(EnumEventType.MainPlayerPlayPropUpdate)
    end
end

function CLuaSchoolContributionMgr:GetSchoolContributionProp()
    if (CClientMainPlayer.Inst and CClientMainPlayer.Inst.TaskProp) then
        return CClientMainPlayer.Inst.TaskProp.SchoolContributionData
    end
end
