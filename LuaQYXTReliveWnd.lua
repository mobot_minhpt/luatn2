local UILabel = import "UILabel"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local CStatusMgr = import "L10.Game.CStatusMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EPropStatus = import "L10.Game.EPropStatus"

CLuaQYXTReliveWnd = class()

RegistClassMember(CLuaQYXTReliveWnd,"m_ReliveLabel")
RegistClassMember(CLuaQYXTReliveWnd,"m_StartTime")
RegistClassMember(CLuaQYXTReliveWnd,"m_Duration")
RegistClassMember(CLuaQYXTReliveWnd,"m_CloseWhenAlive")


function CLuaQYXTReliveWnd:Awake()
    self.m_StartTime = -1
    self.m_Duration = 10
    local delayTime = Valentine2020_Setting.GetData().DelayReliveDuration
    if delayTime then
        self.m_Duration = delayTime
    end
    self.m_ReliveLabel = self.transform:Find("Anchor/ContentRoot/CountDownLabel"):GetComponent(typeof(UILabel))
    self.m_CloseWhenAlive = true
end

function CLuaQYXTReliveWnd:Init()
    self.m_StartTime = CServerTimeMgr.Inst.timeStamp
end

function CLuaQYXTReliveWnd:Update()
    if self.m_CloseWhenAlive and CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("Death")) == 0 then
        self:CountdownOver()
    end
    if self.m_Duration <= 0 then
        self:CountdownOver()
    end
    if CServerTimeMgr.Inst.timeStamp - self.m_StartTime <= self.m_Duration then
        local seconds = math.floor(self.m_Duration - (CServerTimeMgr.Inst.timeStamp - self.m_StartTime) + 0.5)
        seconds = seconds < 0 and 0 or seconds
        self.m_ReliveLabel.text = SafeStringFormat3(LocalString.GetString("%d秒"), seconds)
    else
        self:CountdownOver()
    end
end

function CLuaQYXTReliveWnd:CountdownOver()
    CUIManager.CloseUI(CLuaUIResources.QYXTReliveWnd)
end