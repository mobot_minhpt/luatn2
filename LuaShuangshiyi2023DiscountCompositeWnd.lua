local UILabel = import "UILabel"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaShuangshiyi2023DiscountCompositeWnd = class()
LuaShuangshiyi2023DiscountCompositeWnd.sourceCouponId = nil
LuaShuangshiyi2023DiscountCompositeWnd.targetCouponId = nil

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShuangshiyi2023DiscountCompositeWnd, "SourceCouponsItem", "SourceCouponsItem", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountCompositeWnd, "TargetCouponsItem", "TargetCouponsItem", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountCompositeWnd, "CompositeButton", "CompositeButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountCompositeWnd, "CompositeNeedNumber", "CompositeNeedNumber", UILabel)
RegistChildComponent(LuaShuangshiyi2023DiscountCompositeWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaShuangshiyi2023DiscountCompositeWnd, "DiscountCompositeText", "DiscountCompositeText", UILabel)

--@endregion RegistChildComponent end

function LuaShuangshiyi2023DiscountCompositeWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:InitUIEvent()
    self:RefreshConstUI()
end

function LuaShuangshiyi2023DiscountCompositeWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaShuangshiyi2023DiscountCompositeWnd:InitWndData()
    self.couponData = {}
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer and CommonDefs.DictContains_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11ExteriorDiscountCouponsData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11ExteriorDiscountCouponsData)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        self.couponData = list[2]
    end

    self.couponTypeIconList = {}
    local rawData = Double11_DiscountCouponsSetting.GetData().CouponsTypeIcon
    local rawDataList = g_LuaUtil:StrSplit(rawData, ";")
    for i = 1, #rawDataList do
        if rawDataList[i] ~= "" then
            table.insert(self.couponTypeIconList, rawDataList[i])
        end
    end

    self.couponsList = {}
    local rawCouponsData = Double11_DiscountCouponsSetting.GetData().BigIconList
    local couponsList = g_LuaUtil:StrSplit(rawCouponsData, ";")
    for i = 1, #couponsList do
        if couponsList[i] ~= "" then
            local couponsData = g_LuaUtil:StrSplit(couponsList[i], ",")
            local coupons = {}
            coupons.name = couponsData[1]
            coupons.icon = couponsData[2]
            table.insert(self.couponsList, coupons)
        end
    end
    
    self.compositeRatio = Double11_ExteriorDiscountCoupons.GetData(LuaShuangshiyi2023DiscountCompositeWnd.sourceCouponId).LevelUpNum
end

function LuaShuangshiyi2023DiscountCompositeWnd:RefreshConstUI()
    self.DiscountCompositeText.text = g_MessageMgr:FormatMessage("Double11_2023Discount_Composite_Tips", self.compositeRatio)
    
    local couponObjList = {self.SourceCouponsItem, self.TargetCouponsItem}
    local couponIdList = {LuaShuangshiyi2023DiscountCompositeWnd.sourceCouponId, LuaShuangshiyi2023DiscountCompositeWnd.targetCouponId}
    self.sourceCouponNumber = nil
    for i = 1, #couponObjList do
        local couponObj = couponObjList[i]
        local couponId = couponIdList[i]
        if couponId then
            if i == 1 then
                self.sourceCouponNumber = self.couponData[couponId]
            end
            
            local couponCfgData = Double11_ExteriorDiscountCoupons.GetData(couponId)

            local quality = couponCfgData.Quality
            local icon = self.couponsList[quality].icon
            local textureComp = couponObj.transform:Find("Card"):GetComponent(typeof(CUITexture))
            textureComp:LoadMaterial(icon)

            local name = couponCfgData.Name
            couponObj.transform:Find("Name"):GetComponent(typeof(UILabel)).text = name

            local couponType = couponCfgData.Type
            couponObj.transform:Find("TypeIcon/san").gameObject:SetActive(couponType == 1)
            couponObj.transform:Find("TypeIcon/zuoji").gameObject:SetActive(couponType == 2)
            couponObj.transform:Find("TypeIcon/beishi").gameObject:SetActive(couponType == 3)
            couponObj.transform:Find("TypeIcon/san"):GetComponent(typeof(UITexture)).color = LuaShuangshiyi2023Mgr.DiscountQualityColor[quality]
            couponObj.transform:Find("TypeIcon/zuoji"):GetComponent(typeof(UITexture)).color = LuaShuangshiyi2023Mgr.DiscountQualityColor[quality]
            couponObj.transform:Find("TypeIcon/beishi"):GetComponent(typeof(UITexture)).color = LuaShuangshiyi2023Mgr.DiscountQualityColor[quality]
        end
    end
    
    local minValue = 1
    local maxValue = self.sourceCouponNumber >= self.compositeRatio and math.floor(self.sourceCouponNumber / self.compositeRatio) or 1
    
    self.QnIncreseAndDecreaseButton:SetMinMax(minValue, maxValue, 1)
    self.QnIncreseAndDecreaseButton:SetValue(minValue, true)
    self.QnIncreseAndDecreaseButton:SetInputEnabled(not (minValue == maxValue and minValue == 1))
    self.QnIncreseAndDecreaseButton:EnableButtons(not (minValue == maxValue and minValue == 1))
end

function LuaShuangshiyi2023DiscountCompositeWnd:InitUIEvent()
    self.QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function(value)
        if self.sourceCouponNumber < self.compositeRatio*value then
            self.CompositeNeedNumber.text = CUICommonDef.TranslateToNGUIText("[c][ff5050]" .. self.sourceCouponNumber .. LocalString.GetString("[-][/c]/") .. self.compositeRatio)
        else
            self.CompositeNeedNumber.text = value * self.compositeRatio .. "/" .. self.compositeRatio
        end
    end)

    UIEventListener.Get(self.CompositeButton).onClick = DelegateFactory.VoidDelegate(function (_)
        local compositeNumber = self.QnIncreseAndDecreaseButton:GetValue()
        if compositeNumber > 0 then
            Gac2Gas.RequestCompositing2023Double11Coupons(LuaShuangshiyi2023DiscountCompositeWnd.sourceCouponId, compositeNumber)
            CUIManager.CloseUI(CLuaUIResources.Shuangshiyi2023DiscountCompositeWnd)
        else
            g_MessageMgr:ShowMessage("Double11_2023Discount_Composite_InvalidNumber")
        end
    end)
end 
