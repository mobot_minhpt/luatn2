local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CTaskListItem = import "L10.UI.CTaskListItem"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local GameSetting_Client = import "L10.Game.GameSetting_Client"
local LocalString = import "LocalString"
local Object = import "System.Object"
local UInt32 = import "System.UInt32"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local GuideDefine = import "L10.Game.Guide.GuideDefine" 
local UIPanel = import "UIPanel"
local UIScrollView = import "UIScrollView"
local UITable = import "UITable"
local UISprite = import "UISprite"
local NGUIMath = import "NGUIMath"
local CUIFx = import "L10.UI.CUIFx"
local Vector4 = import "UnityEngine.Vector4"
local CSituationWndMgr = import "L10.UI.CSituationWndMgr"
local EnumSituationType = import "L10.UI.EnumSituationType"
local CGuildQueenMgr = import "L10.Game.CGuildQueenMgr"
local CWeekendGameplayMgr = import "L10.Game.CWeekendGameplayMgr"
local EnumWeekendGameplayStatus = import "L10.Game.EnumWeekendGameplayStatus"
local CActivityMgr = import "L10.Game.CActivityMgr"
local GameSetting_Client_Wapper = import "L10.Game.GameSetting_Client_Wapper"
local Extensions = import "Extensions"
local CItemMgr = import "L10.Game.CItemMgr"

LuaTaskListView = class()

RegistClassMember(LuaTaskListView, "m_Table")               --布局用的table
RegistClassMember(LuaTaskListView, "m_TaskTemplate")        --任务条目模板
RegistClassMember(LuaTaskListView, "m_ScrollView")          --布局用的滚动视图
RegistClassMember(LuaTaskListView, "m_TaskListRoot")        --任务列表根节点 

RegistClassMember(LuaTaskListView, "m_WeekendPlayButton")   --周末玩法面板
RegistClassMember(LuaTaskListView, "m_BanghuaDataButton")   --帮花玩法面板
RegistClassMember(LuaTaskListView, "m_WeekendPlayAlert")    --周末玩法提醒特效

RegistClassMember(LuaTaskListView, "m_TaskIds")
RegistClassMember(LuaTaskListView, "m_Items")
RegistClassMember(LuaTaskListView, "m_OnReposition")
RegistClassMember(LuaTaskListView, "m_EnableUpdate")
RegistClassMember(LuaTaskListView, "m_TickAction")
RegistClassMember(LuaTaskListView, "m_Binded")
RegistClassMember(LuaTaskListView, "m_TaskListViewChildsList") 

function LuaTaskListView:BindComponents()
    if self.m_Binded then return end
    
    self.m_Table = self.transform:Find("TaskListRoot/TaskScrollView/Table"):GetComponent(typeof(UITable))
    self.m_TaskTemplate = self.transform:Find("TaskListRoot/TaskScrollView/TaskItem").gameObject
    self.m_ScrollView = self.transform:Find("TaskListRoot/TaskScrollView"):GetComponent(typeof(UIScrollView))
    self.m_TaskListRoot = self.transform:Find("TaskListRoot")
    self.m_WeekendPlayButton = self.transform:Find("WeekendPlay").gameObject
    self.m_BanghuaDataButton = self.transform:Find("BangHuaDataButton").gameObject
    self:RegisterTaskListViewChildsList()
    self.m_WeekendPlayAlert = self.transform:Find("WeekendPlay/Fx"):GetComponent(typeof(CUIFx))
    self.m_TaskIds = {}
    self.m_Items = {}
    self.m_OnReposition = nil
    self.m_EnableUpdate = false
    self.m_TickAction = nil
    --为了避免有人编辑的时候误隐藏节点，代码主动打开一下
    self.m_TaskListRoot.gameObject:SetActive(true)
    self.m_ScrollView.gameObject:SetActive(true)
    
    CommonDefs.AddOnClickListener(self.m_WeekendPlayButton, DelegateFactory.Action_GameObject(function(go) self:OnWeekendPlayButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_BanghuaDataButton, DelegateFactory.Action_GameObject(function(go) self:OnBangHuaDataButtonClick(go) end), false)

    self.m_Binded = true
end
--@region TaskListViewChilds
--在此注册TaskListView中的Child，点击事件和点击效果自己处理,child的显示和隐藏以及高度自适应由通用机制完成
function LuaTaskListView:RegisterTaskListViewChildsList() 
    self.m_TaskListViewChildsList = {}
    self:InitTaskListViewChild("BangHuaDataButton",function() 
        return CGuildQueenMgr.Inst.ShowBangHuaZhangKuanButton
    end)
    self:InitTaskListViewChild("WeekendPlay",function() 
        return CWeekendGameplayMgr.Inst.status ~= EnumWeekendGameplayStatus.NotOpen
    end,function(isOpen) 
        self:UpdateWeekendPlayButtonVisibility(isOpen)
    end)
    self:InitTaskListViewChild("GuiMenGuanStatusView",function() 
        return CLuaGuiMenGuanMgr.s_ShowStatusView and CLuaGuiMenGuanMgr.s_ShowStatusView or false
    end)
    self:InitTaskListViewChild("ShiMenRuQinTaskView",function() 
        return LuaZongMenMgr.m_SMSW_PlayState ~= EnumSMSWPlayState.Idle and LuaZongMenMgr.m_SMSW_PlayState ~= EnumSMSWPlayState.Prepare
    end)
    self:InitTaskListViewChild("OlympicGamesView",function() 
        return LuaOlympicGamesMgr:IsInPlay()
    end)
    self:InitTaskListViewChild("GuildTerritorialWarsView",function() 
        return LuaGuildTerritorialWarsMgr:IsPlayOpen()
    end)
    self:InitTaskListViewChild("GuildCustomBuildPreTaskView",function() 
        return LuaGuildCustomBuildMgr.IsNeedShowPreBuildTaskView()
    end)
    self:InitTaskListViewChild("ShuJia2023WorldEventTaskView",function() 
        return LuaShuJia2023Mgr:IsNeedShowWorldEvent()
    end, nil, true) 
end

function LuaTaskListView:InitTaskListViewChild(rootPath, visibilityFunc, onShowFunc, bShowTogether) -- showTogether: 需要和其它View一起显示
    local go = self.transform:Find(rootPath).gameObject
    local widget = go:GetComponent(typeof(UIWidget))
    table.insert(self.m_TaskListViewChildsList,{
        widget = widget,
        visibilityFunc = visibilityFunc,
        onShowFunc = onShowFunc, 
        bShowTogether = bShowTogether,
        defaultPosY = widget.transform.localPosition.y,
    })
end

function LuaTaskListView:UpdateTaskListViewChildsPos()
    local hasOffset,offset = false, 0
    for index,child in pairs(self.m_TaskListViewChildsList) do
        if child.widget.gameObject.activeSelf then
            hasOffset = true
            Extensions.SetLocalPositionY(child.widget.transform, child.defaultPosY + offset)
            offset = offset - child.widget.height
        end
    end
    return hasOffset,offset
end

function LuaTaskListView:UpdateTaskListViewChildsVisibility()
    local visibleIndex = -1
    local findShowChild = false
    local showTogether = {}
    for index,child in pairs(self.m_TaskListViewChildsList) do
        if child.bShowTogether then
            if child.visibilityFunc() then
                showTogether[index] = true
            end
        elseif not findShowChild and child.visibilityFunc() then
            findShowChild = true
            visibleIndex = index
        end
    end
    for index,child in pairs(self.m_TaskListViewChildsList) do
        local isShow = (index == visibleIndex) or showTogether[index]
        if child.onShowFunc then
            child.onShowFunc(isShow)
        else
            child.widget.gameObject:SetActive(isShow)
        end
    end
end
--@endregion TaskListViewChilds

function LuaTaskListView:OnEnable()
    self:BindComponents()
    self.m_EnableUpdate = true
    self:ReloadTaskList()
    self:RegisterEvents()
    self:UpdatePlayViewVisibility()
end

function LuaTaskListView:OnDisable()
    self:CancelTick()
    self:UnRegisterEvents()
end

function LuaTaskListView:RegisterEvents()
    g_ScriptEvent:AddListener("MainPlayerCreated", self, "OnMainPlayerCreated") -- C# definination
    g_ScriptEvent:AddListener("UpdateCurrentTask", self, "UpdateTaskInfo") -- C# definination
    g_ScriptEvent:AddListener("UpdateYiTiaoLongTaskFinishedTimes", self, "OnUpdateYiTiaoLongTaskFinishedTimes")
    g_ScriptEvent:AddListener("UpdateShiTuQingTaskFinishedTimes", self, "OnUpdateShiTuQingTaskFinishedTimes")
    g_ScriptEvent:AddListener("TopRightWeekendPlayOpen", self, "OnWeekendPlayOpen") -- C# definination
    g_ScriptEvent:AddListener("OnActivityShownInTaskUpdate", self, "UpdateActivityInfo") -- C# definination
    g_ScriptEvent:AddListener("OnShowBangHuaZhangKuan", self, "OnShowBangHuaZhangKuan") -- C# definination
    g_ScriptEvent:AddListener("RefreshTaskStatus", self, "OnRefreshTaskStatus") -- C# definination
    g_ScriptEvent:AddListener("RefreshGuiMenGuanVisibleStatus", self, "RefreshGuiMenGuanVisibleStatus") -- Lua definination
    g_ScriptEvent:AddListener("RefreshShiMenRuQinTaskViewVisibleStatus", self, "RefreshShiMenRuQinTaskViewVisibleStatus") -- Lua definination
    g_ScriptEvent:AddListener("SyncOlympicBossStatus", self, "OnSyncOlympicBossStatus") 
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem") -- Lua definination
    g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:AddListener("OnUpdateTempHiddenTasksInTrackPanel", self, "OnUpdateTempHiddenTasksInTrackPanel")
    g_ScriptEvent:AddListener("ShuJia2023WorldEventsSyncStageTwoStatus", self, "UpdatePlayViewVisibility")
    g_ScriptEvent:AddListener("UpdateTaskPlayViewVisibility", self, "UpdatePlayViewVisibility")
    self.m_ScrollView:GetComponent(typeof(UIPanel)).onClipMove = LuaUtils.OnClippingMoved(function(panel) --这里不等价于+=，后续如果有多播需求需要C#再提供封装
        self:OnClipMove(panel)
    end)
end

function LuaTaskListView:UnRegisterEvents()
    g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "OnMainPlayerCreated") -- C# definination
    g_ScriptEvent:RemoveListener("UpdateCurrentTask", self, "UpdateTaskInfo") -- C# definination
    g_ScriptEvent:RemoveListener("UpdateYiTiaoLongTaskFinishedTimes", self, "OnUpdateYiTiaoLongTaskFinishedTimes")
    g_ScriptEvent:RemoveListener("UpdateShiTuQingTaskFinishedTimes", self, "OnUpdateShiTuQingTaskFinishedTimes")
    g_ScriptEvent:RemoveListener("TopRightWeekendPlayOpen", self, "OnWeekendPlayOpen") -- C# definination
    g_ScriptEvent:RemoveListener("OnActivityShownInTaskUpdate", self, "UpdateActivityInfo") -- C# definination
    g_ScriptEvent:RemoveListener("OnShowBangHuaZhangKuan", self, "OnShowBangHuaZhangKuan") -- C# definination
    g_ScriptEvent:RemoveListener("RefreshTaskStatus", self, "OnRefreshTaskStatus") -- C# definination
    g_ScriptEvent:RemoveListener("RefreshGuiMenGuanVisibleStatus", self, "RefreshGuiMenGuanVisibleStatus") -- Lua definination
    g_ScriptEvent:RemoveListener("RefreshShiMenRuQinTaskViewVisibleStatus", self, "RefreshShiMenRuQinTaskViewVisibleStatus") -- Lua definination
    g_ScriptEvent:RemoveListener("SyncOlympicBossStatus", self, "OnSyncOlympicBossStatus") 
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:RemoveListener("OnUpdateTempHiddenTasksInTrackPanel", self, "OnUpdateTempHiddenTasksInTrackPanel")
    g_ScriptEvent:RemoveListener("ShuJia2023WorldEventsSyncStageTwoStatus", self, "UpdatePlayViewVisibility")
    g_ScriptEvent:RemoveListener("UpdateTaskPlayViewVisibility", self, "UpdatePlayViewVisibility")
    self.m_ScrollView:GetComponent(typeof(UIPanel)).onClipMove = nil
end


function LuaTaskListView:OnWeekendPlayButtonClick(go)
    CUIManager.ShowUI(CUIResources.ShishijingWnd)
end

function LuaTaskListView:OnBangHuaDataButtonClick(go)
    CSituationWndMgr.OpenSituationWnd(EnumSituationType.BangHua)
end

------------------------------------------------
--  EventListener消息处理方法
------------------------------------------------
function LuaTaskListView:OnMainPlayerCreated()
    self:UpdatePlayViewVisibility()
    self:ReloadTaskList()
end

function LuaTaskListView:UpdateTaskInfo(args) --args[0] taskId
    self:ReloadTaskList() -- 任务信息的更新可能引起排序变化，因此这里需要重新加载
end

function LuaTaskListView:OnUpdateYiTiaoLongTaskFinishedTimes(taskId, round, subTimes, todayTimes)    
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil then return end
    local taskProp = mainplayer.TaskProp
    if CommonDefs.DictContains_LuaCall(taskProp.CurrentTasks, taskId) then
        local task = CommonDefs.DictGetValue_LuaCall(taskProp.CurrentTasks, taskId)
        local mainTaskId = task.MainTaskId > 0 and task.MainTaskId or task.TemplateId
        local mainTask = Task_Task.GetData(mainTaskId)
        if mainTask.GamePlay == "YiTiaoLong" then
            if subTimes >= mainTask.subTimes then
                self.m_EnableUpdate = false
               self:RegisterDelayRefreshTick()
            end
        end
    end

    for __, item in pairs(self.m_Items) do
        if item.TaskId == taskId then
            item:OnUpdateYiTiaoLongTaskFinishedTimes(taskId, round, subTimes, todayTimes)
            break
        end
    end
end

function LuaTaskListView:OnUpdateShiTuQingTaskFinishedTimes(taskId, subTimes)    
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil then return end
    local taskProp = mainplayer.TaskProp
    if CommonDefs.DictContains_LuaCall(taskProp.CurrentTasks, taskId) then
        local task = CommonDefs.DictGetValue_LuaCall(taskProp.CurrentTasks, taskId)
        local mainTaskId = task.MainTaskId > 0 and task.MainTaskId or task.TemplateId
        local mainTask = Task_Task.GetData(mainTaskId)
        if mainTask.GamePlay == "ShiTuQing" then
            if subTimes >= mainTask.subTimes then
                self.m_EnableUpdate = false
               self:RegisterDelayRefreshTick() --复用一条龙的延迟刷新逻辑
            end
        end
    end

    for __, item in pairs(self.m_Items) do
        if item.TaskId == taskId then
            item:OnUpdateShiTuQingTaskFinishedTimes(taskId, subTimes)
            break
        end
    end
end

function LuaTaskListView:OnWeekendPlayOpen(args) --args[0] isOpen
    self:UpdatePlayViewVisibility()
end

function LuaTaskListView:UpdateActivityInfo()
    self:ReloadTaskList()
end

function LuaTaskListView:OnShowBangHuaZhangKuan(args)
    self:UpdatePlayViewVisibility()
end

function LuaTaskListView:OnRefreshTaskStatus(args)
    self:ReloadTaskList()
end

function LuaTaskListView:RefreshGuiMenGuanVisibleStatus()
    self:UpdatePlayViewVisibility()
end

function LuaTaskListView:RefreshShiMenRuQinTaskViewVisibleStatus()
    self:UpdatePlayViewVisibility()    
end

function LuaTaskListView:OnSyncOlympicBossStatus()
    self:UpdatePlayViewVisibility()    
end

function LuaTaskListView:OnSendItem(args)
    local itemId = args[0]
	local item = CItemMgr.Inst:GetById(itemId)
	if item and item.IsItem and item.TemplateId == SectInviteNpc_Setting.GetData().FuLingItemId then
		self:ReloadTaskList()
	end
end

function LuaTaskListView:OnSetItemAt(args)
    local place, pos, oldItemId, newItemId = args[0],args[1],args[2],args[3]
    local item = CItemMgr.Inst:GetById(newItemId)
    if item then
        local templateId = item.TemplateId
        if templateId == SectInviteNpc_Setting.GetData().FuLingItemId then
            self:ReloadTaskList()
        end
    end
end

function LuaTaskListView:OnUpdateTempHiddenTasksInTrackPanel()
    self:ReloadTaskList()
end
------------------------------------------------
--  置顶玩法窗口可见性刷新处理方法
------------------------------------------------
function LuaTaskListView:UpdatePlayViewVisibility()
    self:UpdateTaskListViewChildsVisibility()
    self:UpdateTaskListPos()
end

function LuaTaskListView:UpdateWeekendPlayButtonVisibility(isOpen)
    self.m_WeekendPlayButton:SetActive(isOpen)
    if not isOpen then return end
    if isOpen and CWeekendGameplayMgr.Inst.status == EnumWeekendGameplayStatus.BossOpen then

        self.m_WeekendPlayAlert:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
        local gap = 5
        local background = self.m_WeekendPlayButton:GetComponent(typeof(UISprite))
        CUIFx.DoAni(Vector4(-background.width * 0.5 + gap,
                    background.height * 0.5 - gap,
                    background.width - gap * 2,
                    background.height - gap * 2
                    ), false, self.m_WeekendPlayAlert)
    else
        self.m_WeekendPlayAlert:DestroyFx()
    end
end

function LuaTaskListView:RegisterDelayRefreshTick()
    self:CancelTick()
    self.m_TickAction = RegisterTickOnce(function()
        self.m_EnableUpdate = true
        self:ReloadTaskList()
    end,GameSetting_Client_Wapper.Inst.YiTiaoLongShowConfirmBoxDelay)
end

function LuaTaskListView:CancelTick()
    if self.m_TickAction then
        self.m_TickAction:Invoke()
        self.m_TickAction = nil
    end
end

function LuaTaskListView:ReloadTaskList()
    if not self.m_EnableUpdate then
        return
    end
    self.m_TaskIds = {}
    self.m_Items = {}
    Extensions.RemoveAllChildren(self.m_Table.transform)
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer then
        self:LoadActivity()
        local hiddentaskIds = CLuaPlayerSettings.GetHiddenInTaskTrackPanelIds()
        local taskProperty = mainplayer.TaskProp
        for i=0,taskProperty.CurrentTaskList.Count-1 do
            local taskId = taskProperty.CurrentTaskList[i]
            if not CLuaTaskMgr.IsHideTaskListItem(taskId) and not CLuaTaskMgr.HiddenInTaskTrackPanel(taskId, hiddentaskIds) then
                local data = {}
                data.taskId = taskId
                data.updateTime = CommonDefs.DictGetValue_LuaCall(taskProperty.CurrentTasks, taskId).UpdateTime
                table.insert(self.m_TaskIds, data)
            end
        end

        table.sort(self.m_TaskIds, function(task1, task2)
            if task1.updateTime~=task2.updateTime then
                return task1.updateTime > task2.updateTime
            else
                return task1.taskId < task2.taskId
            end
        end)

        for __,taskData in pairs(self.m_TaskIds) do
            local taskId = taskData.taskId
            local task = CommonDefs.DictGetValue(taskProperty.CurrentTasks, typeof(UInt32), taskId)

            local taskTemplate = Task_Task.GetData(taskId)
            local taskname = taskTemplate.Display
            local completed = false
            local taskinfo = nil

            local normalStatus = true
            if task.OverGrade > 0 then
                local minGrade = taskTemplate.GradeCheck[0]
                if mainplayer.HasFeiSheng then
                    minGrade = taskTemplate.GradeCheckFS1[0]
                end
                if mainplayer.MaxLevel >= 30 then
                    taskinfo = g_MessageMgr:FormatMessage("Task_Need_Level", minGrade)
                else
                    taskinfo = g_MessageMgr:FormatMessage("Task_Need_Level_Below_30", minGrade)
                end

                normalStatus = false
            elseif CTaskMgr.Inst:IsBagSpaceNotEnough(task) then
                taskinfo = CTaskMgr.Inst:GetBagSpaceNotEnoughDescription(task)
                normalStatus = false
            end

            if CTaskMgr.Inst:IsFestivalTaskTimeOut(task) then
                if taskTemplate.ShowTimeoutTips == 2 then
                    local setting = GameSetting_Client.GetData()
                    taskinfo = setting.MainTaskReplaceTips
                else
                    taskinfo = LocalString.GetString("该任务已过期，请放弃")
                end

                normalStatus = false
            end

            if normalStatus then
                completed = task.CanSubmit == 1
                if completed and taskTemplate.SubmitNPC == 0 then
                    if taskId == SectInviteNpc_Setting.GetData().VisitTaskId then
                        taskinfo = CTaskMgr.Inst:GetTaskDescriptionSimple(task.TemplateId)
                    elseif LuaInviteNpcMgr.IsFriendlinessTask(taskId) then
                        taskinfo = CTaskMgr.Inst:GetTaskDescriptionSimple(task.TemplateId)
                        local extraStr = task.ExtraData.StringData
                        if extraStr and extraStr ~= "" then
                            local npcId = string.match(extraStr,"(%d+);")
                            local npcName = NPC_NPC.GetData(npcId).Name
                            taskinfo = SafeStringFormat3(taskinfo,npcName)
                        end
                    elseif taskTemplate.TaskLocations ~= nil and taskTemplate.TaskLocations.Length >= 1 then
                        taskinfo = taskTemplate.TaskLocations[taskTemplate.TaskLocations.Length - 1].trackInfo
                        taskinfo = taskinfo .. LocalString.GetString("\n(点击提交)")
                    end
                else
                    if task.IsFailed then
                        taskinfo = CTaskMgr.Inst:GetTaskTrackDescription(task)
                    else
                        local local_ = CTaskMgr.Inst:GetTaskTrackPos(task)
                        if local_ ~= nil then
                            if taskId == SectInviteNpc_Setting.GetData().VisitTaskId then
                                taskinfo = CTaskMgr.Inst:GetTaskDescriptionSimple(task.TemplateId)
                            elseif LuaInviteNpcMgr.IsFriendlinessTask(taskId) then
                                taskinfo = CTaskMgr.Inst:GetTaskDescriptionSimple(task.TemplateId)
                                local extraStr = task.ExtraData.StringData
                                if extraStr and extraStr ~= "" then
                                    local npcId = string.match(extraStr,"(%d+);")
                                    local npcName = NPC_NPC.GetData(npcId).Name
                                    taskinfo = SafeStringFormat3(taskinfo,npcName)
                                end
                            else
                                taskinfo = local_.trackInfo .. CTaskMgr.Inst:GetTaskTrackDescription(task)
                            end
                        else
                            taskinfo = CTaskMgr.Inst:GetTaskDescriptionSimple(task.TemplateId)
                            if LuaInviteNpcMgr.IsFriendlinessTask(taskId) then
                                local extraStr = task.ExtraData.StringData
                                if extraStr and extraStr ~= "" then
                                    local npcId = string.match(extraStr,"(%d+);")
                                    local npcName = NPC_NPC.GetData(npcId).Name
                                    taskinfo = SafeStringFormat3(taskinfo,npcName)
                                end
                            end
                        end
                    end

                    if CTaskMgr.Inst:NeedCustomizeTrackInfo(task.TemplateId) then
                        taskinfo = CTaskMgr.Inst:GetCustomizedTrackInfo(task.TemplateId)
                    end
                end
            end

            if task.MainTaskId > 0 and CommonDefs.DictContains(taskProperty.RepeatableTasks, typeof(UInt32), task.MainTaskId) then
                local mainTask = Task_Task.GetData(task.MainTaskId)
                local repeatableTaskInfo = CommonDefs.DictGetValue(taskProperty.RepeatableTasks, typeof(UInt32), task.MainTaskId)
                if mainTask.subTimes > 1 then
                    taskname = taskname .. System.String.Format("({0}/{1})", repeatableTaskInfo.FinishedSubTimes + 1, mainTask.subTimes)
                end
            end

            --if (taskTemplate.Status == (int)EnumDesignDataStatus.eDisable)
            --{
            --    taskname += "  [ff0000](已关闭)[-]";
            --}else
            if task.IsFailed then
                taskname = taskname .. LocalString.GetString("  [ff0000](失败)[-]")
            elseif completed then
                taskname = taskname .. LocalString.GetString("  [14ff0e]完成[-]")
            end


            local instance = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_TaskTemplate)
            local item = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CTaskListItem))
            instance:SetActive(true)

            item:Init(task, taskname, taskinfo, completed, self.m_ScrollView)
            item:Resize()
            table.insert(self.m_Items, item)
        end

        self.m_Table:Reposition()
        self.m_ScrollView:ResetPosition()
        self:OnClipMove(self.m_ScrollView:GetComponent(typeof(UIPanel)))
        self:UpdateTaskListPos()
    end
end
-- svn revision 206782
function LuaTaskListView:LoadActivity()
    local list =  CActivityMgr.Inst.activityList
    if list == nil then return end
    CommonDefs.DictIterate(CActivityMgr.Inst.activityList, DelegateFactory.Action_object_object(function(key, act)
        local instance = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_TaskTemplate)
        local item = instance:GetComponent(typeof(CTaskListItem))
        instance:SetActive(true)
        item:InitActivity(act, self.m_ScrollView)
        item:Resize()
    end))
end

function LuaTaskListView:UpdateTaskListPos()
    local hasOffset,offset = self:UpdateTaskListViewChildsPos()

    if hasOffset then
        Extensions.SetLocalPositionY(self.m_TaskListRoot, offset -5)
    else
        Extensions.SetLocalPositionY(self.m_TaskListRoot, 0)
    end
end


function LuaTaskListView:OnClipMove(panel)
    if self.m_OnReposition then
        GenericDelegateInvoke(self.m_OnReposition, Table2ArrayWithCount({self:GetBottomWorldPos()}, 1, MakeArrayClass(Object)))
    end
end

function LuaTaskListView:GetBottomWorldPos()
    local panel = self.m_ScrollView.panel == nil and self.m_ScrollView:GetComponent(typeof(UIPanel)) or self.m_ScrollView.panel
    local worldCorners = panel.worldCorners
    local minY = worldCorners[0].y --底部
    local maxY = worldCorners[1].y --顶部

    if self.m_Table.transform.childCount > 0 then
        local lastChild = self.m_Table.transform:GetChild(self.m_Table.transform.childCount - 1)
        local b = NGUIMath.CalculateAbsoluteWidgetBounds(lastChild)
        if b.min.y > maxY then
            return maxY
        elseif b.min.y > minY then
            return b.min.y
        else
            return minY
        end
    else
        return maxY
    end
end

-- for guide
function LuaTaskListView:GetFirstTaskGO()
    if self.m_Items then
        if CGuideMgr.Inst:IsInPhase(EnumGuideKey.TaskGuide) then
            for i, item in pairs(self.m_Items) do
                if self.m_TaskIds[i].taskId == GameSetting_Client_Wapper.Inst.NewPlayerFirstTaskId then
                    CUICommonDef.SetFullyVisible(item.gameObject, self.m_ScrollView)
                    return item.gameObject
                end
            end
        end
    else
        return nil
    end
end
