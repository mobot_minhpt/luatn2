require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local Color = import "UnityEngine.Color"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUIFx = import "L10.UI.CUIFx"
local UITabBar = import "L10.UI.UITabBar"
--local ShenBingChuShi_PlayContent = import "L10.Game.ShenBingChuShi_PlayContent"
local CButton = import "L10.UI.CButton"

LuaShenbingEnterWnd = class()
RegistClassMember(LuaShenbingEnterWnd,"CloseBtn")
RegistClassMember(LuaShenbingEnterWnd,"RestNumLabel")
RegistClassMember(LuaShenbingEnterWnd,"NeedLvLabel")
RegistClassMember(LuaShenbingEnterWnd,"TabNode")
RegistClassMember(LuaShenbingEnterWnd,"NormalPanel")
RegistClassMember(LuaShenbingEnterWnd,"HardPanel")
RegistClassMember(LuaShenbingEnterWnd,"EnterBtn")
RegistClassMember(LuaShenbingEnterWnd,"TipBtn")

function LuaShenbingEnterWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaShenbingEnterWnd:EnterFight()
	if self.chooseEntrance and self.chooseMode and self.chooseEntrance >= 1 and self.chooseMode >= 1 then
		if self.keyTable then
			for i,v in pairs(self.keyTable) do
				local data = ShenBingChuShi_PlayContent.GetData(v)
				if data then
					if data.IsEntrance == self.chooseEntrance and data.Mode == self.chooseMode then
						Gac2Gas.RequestEnterShenBing(data.ID)
						if self.formerFxNode then
							self.formerFxNode:DestroyFx()
							self.formerFxNode = nil
						end
						return
					end
				end
			end
		end
	else
		g_MessageMgr:ShowMessage("SBCS_ENTRANCE_NO_SELECT")
	end
end

function LuaShenbingEnterWnd:ChooseEnterState(entrance,mode,go)
	if self.chooseNodeSign then
		self.chooseNodeSign:SetActive(false)
	end
	self.chooseNodeSign = go.transform:Find("clickSign").gameObject
	self.chooseNodeSign:SetActive(true)
	self.chooseEntrance = entrance
	self.chooseMode = mode

	local fxNode = go.transform:Find("fxNode"):GetComponent(typeof(CUIFx))
	if self.formerFxNode and self.formerFxNode ~= fxNode then
		self.formerFxNode:DestroyFx()
	end
	self.formerFxNode = fxNode
	if mode == 2 then
		if entrance == 1 then
			fxNode:LoadFx("fx/ui/prefab/UI_shenbingchushi01.prefab",8)
		elseif entrance == 2 then
			fxNode:LoadFx("fx/ui/prefab/UI_shenbingchushi02.prefab",8)
		end
	end
end

function LuaShenbingEnterWnd:OnTabChange(go, index)
	if index == 0 then
		self.NormalPanel:SetActive(true)
		self.HardPanel:SetActive(false)

	elseif index == 1 then
		self.NormalPanel:SetActive(false)
		self.HardPanel:SetActive(true)

	end
end

function LuaShenbingEnterWnd:InitTabStatus()
	self.keyTable = {}
	ShenBingChuShi_PlayContent.ForeachKey(function (key)
		table.insert(self.keyTable,key)
	end)
	local normalStatus = 0
	for i,v in pairs(self.keyTable) do
		local data = ShenBingChuShi_PlayContent.GetData(v)
		if data then
			for j,b in pairs(LuaShenbingMgr.ShenbingEnterWndFinishTask) do
				if data.ID == b and data.Mode == 1 and data.GamePlayPass == 1 then
					normalStatus = normalStatus + 1
				end
			end
		end
	end

	local onNormal1Click = function(go)
		self:ChooseEnterState(1,1,go)
	end
	local onNormal2Click = function(go)
		self:ChooseEnterState(2,1,go)
	end
	CommonDefs.AddOnClickListener(self.NormalPanel.transform:Find("btn1").gameObject,DelegateFactory.Action_GameObject(onNormal1Click),false)
	CommonDefs.AddOnClickListener(self.NormalPanel.transform:Find("btn2").gameObject,DelegateFactory.Action_GameObject(onNormal2Click),false)
	self.NormalPanel.transform:Find("btn1/introText"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("SBCS_ENTRANCE_INTRO1")
	self.NormalPanel.transform:Find("btn2/introText"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("SBCS_ENTRANCE_INTRO2")

	local hardBtn = self.TabNode.transform:Find("Hard"):GetComponent(typeof(CButton))
	local hardBtnLabel = self.TabNode.transform:Find("Hard/Label"):GetComponent(typeof(UILabel))
	if normalStatus >= 2 then
		hardBtn.Enabled = true
		hardBtnLabel.text = LocalString.GetString("英雄")

		local onHard1Click = function(go)
			self:ChooseEnterState(1,2,go)
		end
		local onHard2Click = function(go)
			self:ChooseEnterState(2,2,go)
		end
		CommonDefs.AddOnClickListener(self.HardPanel.transform:Find("btn1").gameObject,DelegateFactory.Action_GameObject(onHard1Click),false)
		CommonDefs.AddOnClickListener(self.HardPanel.transform:Find("btn2").gameObject,DelegateFactory.Action_GameObject(onHard2Click),false)

		self.HardPanel.transform:Find("btn1/introText"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("SBCS_ENTRANCE_INTRO1")
		self.HardPanel.transform:Find("btn2/introText"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("SBCS_ENTRANCE_INTRO2")
	else
		hardBtn.Enabled = false
		hardBtnLabel.text = LocalString.GetString("未解锁")
	end
end

function LuaShenbingEnterWnd:Init()

	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	local onTipNodeClick = function(go)
		g_MessageMgr:ShowMessage("SBCS_ENTRANCE_TIPS")
	end
	CommonDefs.AddOnClickListener(self.TipBtn,DelegateFactory.Action_GameObject(onTipNodeClick),false)

	local onEnterNodeClick = function(go)
		self:EnterFight()
	end
	CommonDefs.AddOnClickListener(self.EnterBtn,DelegateFactory.Action_GameObject(onEnterNodeClick),false)
	self.NeedLvLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(ShenBingChuShi_Setting.GetData().GradeLimit))
	self.RestNumLabel.text = LuaShenbingMgr.ShenbingRemainTimes
	if LuaShenbingMgr.ShenbingRemainTimes > 0 then
		self.RestNumLabel.color = Color.green
	else
		self.RestNumLabel.color = Color.grey
	end
	self:InitTabStatus()
	local tabBar = self.TabNode:GetComponent(typeof(UITabBar))
	tabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index) self:OnTabChange(go, index) end)
	tabBar:ChangeTab(0,false)
end

return LuaShenbingEnterWnd
