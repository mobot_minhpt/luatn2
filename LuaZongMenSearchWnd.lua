local CButton = import "L10.UI.CButton"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local QnTableView = import "L10.UI.QnTableView"
local QnButton = import "L10.UI.QnButton"

LuaZongMenSearchWnd = class()

RegistChildComponent(LuaZongMenSearchWnd,"m_Input","Input", UIInput)
RegistChildComponent(LuaZongMenSearchWnd,"m_TipButton","TipButton", GameObject)
RegistChildComponent(LuaZongMenSearchWnd,"m_SearchButton","SearchButton", GameObject)
RegistChildComponent(LuaZongMenSearchWnd,"m_ZongMenInfoView","ZongMenInfoView", GameObject)
RegistChildComponent(LuaZongMenSearchWnd,"m_NameLabel","NameLabel", UILabel)
RegistChildComponent(LuaZongMenSearchWnd,"m_RuMenBiaoZhunValueLabel","RuMenBiaoZhunValueLabel", UILabel)
RegistChildComponent(LuaZongMenSearchWnd,"m_ZongMenGuiZeValueLabel","ZongMenGuiZeValueLabel", UILabel)
RegistChildComponent(LuaZongMenSearchWnd,"m_RuMenBiaoZhunDetailBtn","RuMenBiaoZhunDetailBtn", GameObject)
RegistChildComponent(LuaZongMenSearchWnd,"m_ZongMenGuiZeDetailBtn","ZongMenGuiZeDetailBtn", GameObject)
RegistChildComponent(LuaZongMenSearchWnd,"m_Grid","Grid", UIGrid)
RegistChildComponent(LuaZongMenSearchWnd,"m_InfoTemplate","InfoTemplate", GameObject)
RegistChildComponent(LuaZongMenSearchWnd,"m_LianXiButton","LianXiButton", GameObject)
RegistChildComponent(LuaZongMenSearchWnd,"m_JoinButton","JoinButton", GameObject)
RegistChildComponent(LuaZongMenSearchWnd,"m_RecommendButton","RecommendButton", GameObject)
RegistChildComponent(LuaZongMenSearchWnd,"m_RecommendView","RecommendView", GameObject)
RegistChildComponent(LuaZongMenSearchWnd,"m_DetailButton","DetailButton", GameObject)
RegistChildComponent(LuaZongMenSearchWnd,"m_BottomButtons","BottomButtons", GameObject)
RegistChildComponent(LuaZongMenSearchWnd,"m_QnTableView","QnTableView", QnTableView)

RegistClassMember(LuaZongMenSearchWnd, "m_InfoLabels")
RegistClassMember(LuaZongMenSearchWnd, "m_ZongMenName")
RegistClassMember(LuaZongMenSearchWnd, "m_ZongMenid")
RegistClassMember(LuaZongMenSearchWnd, "m_SelectIndex")
RegistClassMember(LuaZongMenSearchWnd, "m_RecommendList")

function LuaZongMenSearchWnd:Init()
    UIEventListener.Get(self.m_SearchButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnSearchButtonClicked()
    end)
    UIEventListener.Get(self.m_LianXiButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnLianXiButtonClicked()
    end)
    UIEventListener.Get(self.m_TipButton).onClick = DelegateFactory.VoidDelegate(function (p)
        g_MessageMgr:ShowMessage("ZongMenSearchWnd_ReadMe")
    end)
    UIEventListener.Get(self.m_JoinButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnJoinButtonClicked()
    end)
    UIEventListener.Get(self.m_RecommendButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnRecommendButtonClicked()
    end)
    UIEventListener.Get(self.m_DetailButton).onClick = DelegateFactory.VoidDelegate(function (p)
        if not self.m_SelectIndex or not self.m_RecommendList[self.m_SelectIndex + 1] then 
            g_MessageMgr:ShowMessage("NoneSelect_JoinSect")
            return 
        end
        local data = self.m_RecommendList[self.m_SelectIndex + 1]
        self.m_ZongMenName = data and data.Name
        self.m_ZongMenid = data.SectId
        self:OnJoinButtonClicked()
    end)
    for i = 0, self.m_BottomButtons.transform.childCount - 1 do
        local child = self.m_BottomButtons.transform:GetChild(i)
        local btn = child:GetComponent(typeof(CButton))
        btn.gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
        local text = btn.Text
        UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
            CJingLingMgr.Inst:ShowJingLingWnd(text, "o_push", true, false, nil, false)
        end)
    end
    self.m_ZongMenInfoView:SetActive(false)
    self.m_RecommendView:SetActive(true)
    self:InitQnTableView()
    self:IniInfoLabels()
    self:InitReportButton()
    if CClientMainPlayer.Inst then
        Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId,"recommend",0)
    end
end

function LuaZongMenSearchWnd:InitQnTableView()
    self.m_QnTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self.m_RecommendList and #self.m_RecommendList or 0
        end,
        function(item, index)
            self:ItemAt(item,index)
        end)
    self.m_QnTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
    self.m_QnTableView:ReloadData(true, true)
end

function LuaZongMenSearchWnd:ItemAt(item,index)
    if not self.m_RecommendList then return end
    local data = self.m_RecommendList[index + 1]
    -- local leaderWuXingName = SoulCore_WuXing.GetData(data.LeaderWuXing).Name
    local wuxing = SoulCore_WuXing.GetData(data.FaZhenWuXing ~= 0 and data.FaZhenWuXing or 1)
    local curName = wuxing.Name 
    local downName =  SoulCore_WuXing.GetData(data.AvoidWuxing ~= 0 and data.AvoidWuxing or 1).Name
    --local leaderNameText = data.LeaderName .. self:GetWuXingColorWord(SafeStringFormat3("(%s[ffffff])",leaderWuXingName))
    local wuXingText = CUICommonDef.TranslateToNGUIText(self:GetWuXingColorWord(SafeStringFormat3(LocalString.GetString("当前:%s"),curName)))
    local avoidWuXingText = CUICommonDef.TranslateToNGUIText(self:GetWuXingColorWord(SafeStringFormat3(LocalString.GetString("规避:%s"),downName)))
    item.transform:Find("SectNameLabel"):GetComponent(typeof(UILabel)).text = data.Name
    -- item.transform:Find("LeaderNameLabel"):GetComponent(typeof(UILabel)).text = leaderNameText
    item.transform:Find("NumLabel"):GetComponent(typeof(UILabel)).text = data.MemberCount
    item.transform:Find("MiShuLabel"):GetComponent(typeof(UILabel)).text = data.MishuLevel
    item.transform:Find("WuXingLabel"):GetComponent(typeof(UILabel)).text = data.FaZhenWuXing ~= 0 and wuXingText or ""
    item.transform:Find("AvioidWuXingLabel"):GetComponent(typeof(UILabel)).text = data.AvoidWuxing ~= 0 and avoidWuXingText or ""
    item.transform:GetComponent(typeof(QnButton)):SetBackgroundTexture(index % 2 == 0 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
    UIEventListener.Get(item.transform:Find("DetailLabel").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnDetailButtonClicked()
    end)
end

function LuaZongMenSearchWnd:OnSelectAtRow(row)
    self.m_SelectIndex = row
end

function LuaZongMenSearchWnd:IniInfoLabels()
    Extensions.RemoveAllChildren(self.m_Grid.transform)
    self.m_InfoTemplate:SetActive(false)
    self.m_InfoLabels = {}
    local list = {LocalString.GetString("门主"),LocalString.GetString("宗派等级"),LocalString.GetString("宗派人数"),LocalString.GetString("秘术等级"),
    LocalString.GetString("炼化五行"),LocalString.GetString("当前流派"),LocalString.GetString("活跃度"),LocalString.GetString("宗派入口")}
    for i,text in pairs(list) do
        local go = NGUITools.AddChild(self.m_Grid.gameObject, self.m_InfoTemplate)
        go:SetActive(true)
        go.transform:Find("InfoLabel"):GetComponent(typeof(UILabel)).text = text
        local levelspite =  go.transform:Find("ValueLabel/LevelSprite"):GetComponent(typeof(UISprite))
        levelspite.gameObject:SetActive(false)
        table.insert(self.m_InfoLabels, go.transform:Find("ValueLabel"):GetComponent(typeof(UILabel)))
    end
    self.m_Grid:Reposition()
end

function LuaZongMenSearchWnd:OnEnable()
    g_ScriptEvent:AddListener("OnZongMenSearchResult", self, "OnZongMenSearchResult")
    g_ScriptEvent:AddListener("OnSectRecommendInfoResult", self, "OnSectRecommendInfoResult")
end

function LuaZongMenSearchWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnZongMenSearchResult", self, "OnZongMenSearchResult")
    g_ScriptEvent:RemoveListener("OnSectRecommendInfoResult", self, "OnSectRecommendInfoResult")
end

function LuaZongMenSearchWnd:OnSectRecommendInfoResult(data)
    self.m_RecommendList = data
    self.m_QnTableView:ReloadData(true, true)
end

function LuaZongMenSearchWnd:OnZongMenSearchResult(data)
    self.m_ZongMenid = data.Id
    self.m_ZongMenName = data.Name.StringData
    self.m_NameLabel.text = data.Name.StringData
    local joinStandardData = Menpai_JoinStandard.GetData(data.JoinStandard[0])
    local ruleData = Menpai_Rules.GetData(data.Instruction[0])
    for i = 0,data.JoinStandard.Length - 1 do
        local t = Menpai_JoinStandard.GetData(data.JoinStandard[i])
        if t and t.Description then
            joinStandardData = t 
            break
        end
    end
    for i = 0,data.Instruction.Length - 1 do
        local t = Menpai_Rules.GetData(data.Instruction[i])
        if t and t.Description then
            ruleData = t 
            break
        end
    end
    self.m_RuMenBiaoZhunValueLabel.text = SafeStringFormat3(LocalString.GetString("1、%s 2、..."), joinStandardData and joinStandardData.Description or "---")
    self.m_ZongMenGuiZeValueLabel.text = SafeStringFormat3(LocalString.GetString("1、%s 2、..."), ruleData and ruleData.Description or "---")
    local leaderWuXingName = SoulCore_WuXing.GetData(data.LeaderWuXing).Name
    self.m_InfoLabels[1].text = data.LeaderName .. self:GetWuXingColorWord(SafeStringFormat3("(%s[ffffff])",leaderWuXingName))
    self.m_InfoLabels[2].enabled = false
    local levelspite = self.m_InfoLabels[2].transform:Find("LevelSprite"):GetComponent(typeof(UISprite))
    levelspite.spriteName = SafeStringFormat3("guildwnd_grade_0%d", data.Level)
    levelspite.gameObject:SetActive(true)
    self.m_InfoLabels[3].text = SafeStringFormat3("%d/30", data.DiziCount)
    self.m_InfoLabels[4].text = data.MiShuLevel
    local wuxing = SoulCore_WuXing.GetData(data.FaZhenWuXing ~= 0 and data.FaZhenWuXing or 1)
    if wuxing then
        local curName = wuxing.Name 
        local downName =  SoulCore_WuXing.GetData(data.AvoidWuxing ~= 0 and data.AvoidWuxing or 1).Name
        local s = self:GetWuXingColorWord(SafeStringFormat3(LocalString.GetString("当前:%s  [ffffff]规避:%s"),curName, downName))
        if data.AvoidWuxing == 0 then
            s = self:GetWuXingColorWord(SafeStringFormat3(LocalString.GetString("当前:%s"),curName))
        end
        self.m_InfoLabels[5].text = CUICommonDef.TranslateToNGUIText(s)
    end
    self.m_InfoLabels[6].text = data.IsXiePai == 1 and LocalString.GetString("邪派") or LocalString.GetString("正派")
    self.m_InfoLabels[7].text = data.Activity
    self.m_InfoLabels[8].text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3("[04fa21][%s,%d,%d]",PublicMap_PublicMap.GetData(data.EntranceMapId).Name,data.EntrancePosX,data.EntrancePosY))
    UIEventListener.Get(self.m_RuMenBiaoZhunDetailBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        LuaZongMenMgr:OnZongMenGuiZeWnd(LocalString.GetString("入派标准"),data.JoinStandard, false, data.RaidMapId)
    end)
    UIEventListener.Get(self.m_ZongMenGuiZeDetailBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        LuaZongMenMgr:OnZongMenGuiZeWnd(LocalString.GetString("宗派规则"),data.Instruction, true, data.RaidMapId)
    end)
    self.m_ZongMenInfoView:SetActive(true)
    self.m_RecommendView:SetActive(false)
end

function LuaZongMenSearchWnd:GetWuXingColorWord(str)
    local colorDict = {
        [LocalString.GetString("金")] = LocalString.GetString("[ffc800]金"),
        [LocalString.GetString("木")] = LocalString.GetString("[04fa21]木"),
        [LocalString.GetString("水")] = LocalString.GetString("[64ffef]水"),
        [LocalString.GetString("火")] = LocalString.GetString("[ff0024]火"),
        [LocalString.GetString("土")] = LocalString.GetString("[bfb794]土")
    }
    local word = str
    for key, val in pairs(colorDict) do
        word = word:gsub(key, val)
    end
    return word 
end

function LuaZongMenSearchWnd:OnSearchButtonClicked()
    local str = tostring(self.m_Input.value)
    if System.String.IsNullOrEmpty(str) then 
        Gac2Gas.QueryToJoinSectInfo(0)
        return 
    end
    Gac2Gas.QueryToJoinSectInfo(tonumber(str))
end

function LuaZongMenSearchWnd:OnLianXiButtonClicked()
    Gac2Gas.RequestContactSectManager(self.m_ZongMenid)
end

function LuaZongMenSearchWnd:OnJoinButtonClicked()
    if not self.m_SelectIndex and not self.m_ZongMenid then 
        g_MessageMgr:ShowMessage("NoneSelect_JoinSect")
        return 
    end
    local msg =  g_MessageMgr:FormatMessage("Join_ZongMen_Confirm", self.m_ZongMenName)
    MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
        LuaZongMenMgr:JoinZongMen(self.m_ZongMenid, false, nil, false)
    end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
end

function LuaZongMenSearchWnd:OnRecommendButtonClicked()
    self.m_ZongMenInfoView:SetActive(false)
    self.m_RecommendView:SetActive(true)
end

function LuaZongMenSearchWnd:OnDetailButtonClicked()
    if not self.m_SelectIndex then 
        g_MessageMgr:ShowMessage("NoneSelect_SearchRecommendSect")
        return 
    end
    local data = self.m_RecommendList[self.m_SelectIndex + 1]
    Gac2Gas.QueryToJoinSectInfo(data.SectId)
end

-- 初始化宗门举报按钮
function LuaZongMenSearchWnd:InitReportButton()
    local reportButton = self.transform:Find("Anchor/ReportButton").gameObject

    UIEventListener.Get(reportButton).onClick = DelegateFactory.VoidDelegate(function (p)
        if self.m_RecommendView.active and self.m_SelectIndex and self.m_RecommendList then
            local data = self.m_RecommendList[self.m_SelectIndex + 1]
            self.m_ZongMenName = data and data.Name
            self.m_ZongMenid = data and data.SectId
        end

        if self.m_ZongMenid then
            LuaPlayerReportMgr:ShowGuildReportWnd(self.m_ZongMenid, self.m_ZongMenName, "zongpai",
                LocalString.GetString("宗派举报"), LocalString.GetString("举报宗派"))
        else
            g_MessageMgr:ShowMessage("Sect_Report_Need_Choose_Sect")
        end
    end)
end
