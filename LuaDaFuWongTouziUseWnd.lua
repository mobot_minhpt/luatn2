local GameObject = import "UnityEngine.GameObject"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local CUITexture = import "L10.UI.CUITexture"
local Quaternion = import "UnityEngine.Quaternion"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local Space = import "UnityEngine.Space"
local Quaternion = import "UnityEngine.Quaternion"
local CClientObjectRoot = import "L10.Game.CClientObjectRoot"
local CRenderObject = import "L10.Engine.CRenderObject"
LuaDaFuWongTouziUseWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaFuWongTouziUseWnd, "TitleName", "TitleName", UILabel)
RegistChildComponent(LuaDaFuWongTouziUseWnd, "Desc", "Desc", UILabel)
RegistChildComponent(LuaDaFuWongTouziUseWnd, "OKBtn", "OKBtn", CButton)
RegistChildComponent(LuaDaFuWongTouziUseWnd, "CancelButton", "CancelButton", CButton)
RegistChildComponent(LuaDaFuWongTouziUseWnd, "TouZiTexture", "TouZiTexture", CUITexture)
RegistChildComponent(LuaDaFuWongTouziUseWnd, "Preview", "Preview", GameObject)
RegistChildComponent(LuaDaFuWongTouziUseWnd, "TouZiDot", "TouZiDot", GameObject)
RegistChildComponent(LuaDaFuWongTouziUseWnd, "Dot1", "Dot1", GameObject)
RegistChildComponent(LuaDaFuWongTouziUseWnd, "Dot2", "Dot2", GameObject)
RegistChildComponent(LuaDaFuWongTouziUseWnd, "Dot3", "Dot3", GameObject)
RegistChildComponent(LuaDaFuWongTouziUseWnd, "Dot5", "Dot5", GameObject)
RegistChildComponent(LuaDaFuWongTouziUseWnd, "Dot4", "Dot4", GameObject)
RegistChildComponent(LuaDaFuWongTouziUseWnd, "Dot6", "Dot6", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongTouziUseWnd, "m_ModelTextureLoader")
RegistClassMember(LuaDaFuWongTouziUseWnd, "m_SelectDot")
RegistClassMember(LuaDaFuWongTouziUseWnd, "m_TouZiRo")
RegistClassMember(LuaDaFuWongTouziUseWnd, "m_CurTouZiRotation")
RegistClassMember(LuaDaFuWongTouziUseWnd, "m_IsDrag")
RegistClassMember(LuaDaFuWongTouziUseWnd, "m_RoleTick")
RegistClassMember(LuaDaFuWongTouziUseWnd, "m_DotList")
RegistClassMember(LuaDaFuWongTouziUseWnd, "m_ColorList")
function LuaDaFuWongTouziUseWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.OKBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOKBtnClick()
	end)


	
	UIEventListener.Get(self.CancelButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelButtonClick()
	end)


    --@endregion EventBind end
	self.m_SelectDot = 1	-- 当前选择的点数
	self.m_ModelTextureLoader = nil
	self.m_TouZiRo = nil
	self.m_CurTouZiRotation = {90,0,0}
	self.m_IsDrag = false
	self.m_RoleTick = nil
	self.m_FakeTouZiRo = nil
	self.m_ColorList = {"c2a003","db4646","981acd","16a857"}
	self.m_ColorList[0] = "483931"
end
function LuaDaFuWongTouziUseWnd:GetType()
	return 2 
end
function LuaDaFuWongTouziUseWnd:Init()
	self.m_ItemId = LuaDaFuWongMgr.CurItemId
	local data = DaFuWeng_Card.GetData(self.m_ItemId)
	if not data then return end

	self.m_CountDown = DaFuWeng_Countdown.GetData(EnumDaFuWengStage.RoundCard).Time
	self:SetCountDownTick()
	self:InitTitle(data.Name,data.SelectDesc)
	self:InitTouZi()
end

function LuaDaFuWongTouziUseWnd:SetCountDownTick()
	local countDown = self.m_CountDown
	local endFunc = function() self:OnCancelButtonClick() end
	local durationFunc = function(time) if self.CancelButton then self.CancelButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("取消(%d)"),time)  end end
	LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.CardUse,countDown,durationFunc,endFunc)
end
function LuaDaFuWongTouziUseWnd:InitTitle(title, desc)
	self.TitleName.text = title
	self.Desc.text = SafeStringFormat3("[672700]%s[-]",g_MessageMgr:Format(desc))
end
function LuaDaFuWongTouziUseWnd:InitTouZi()
	if self:GetType() == 1 then
		self.Preview.gameObject:SetActive(true)
		self.TouZiDot.gameObject:SetActive(false)
		local touzipath = "assets/res/character/npc/lnpc999/prefab/lnpc999_01.prefab"
		if not self.m_TouZiRo then
				self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function(ro)
				self.m_TouZiRo = ro
				ro:LoadMain(touzipath)
				ro:DoAni("stand02", false, 0, 1, 0, true, 1)
				self.m_TouZiRo.transform.localRotation = Quaternion.Euler(self.m_CurTouZiRotation[1],self.m_CurTouZiRotation[2],self.m_CurTouZiRotation[3])
			end)
		else
			self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Load(self.m_TouZiRo)
			self.m_TouZiRo:DoAni("stand02", false, 0, 1, 0, true, 1)
			self.m_TouZiRo.transform.localRotation = Quaternion.Euler(self.m_CurTouZiRotation[1],self.m_CurTouZiRotation[2],self.m_CurTouZiRotation[3])
		end
		if not self.m_FakeTouZiRo then
			local root = CClientObjectRoot.Inst.Other.gameObject
			self.m_FakeTouZiRo = CRenderObject.CreateRenderObject(root, "FakeTouZiRo")
			self.m_FakeTouZiRo.transform.rotation = Quaternion.Euler(self.m_CurTouZiRotation[1],self.m_CurTouZiRotation[2],self.m_CurTouZiRotation[3])
		end
		self.TouZiTexture.mainTexture = CUIManager.CreateModelTexture("__DaFuWongTouZiCard__",self.m_ModelTextureLoader,0,0,0,3,false,true,1.5)
		UIEventListener.Get(self.TouZiTexture.gameObject).onDrag = DelegateFactory.VectorDelegate(function (go, delta)
			self:OnDragTouZi(go, delta)
		end)
		UIEventListener.Get(self.TouZiTexture.gameObject).onDragStart = DelegateFactory.VoidDelegate(function (go)
			self:OnDragStartOrEnd(go,true)
		end)
		UIEventListener.Get(self.TouZiTexture.gameObject).onDragEnd = DelegateFactory.VoidDelegate(function (go)
			self:OnDragStartOrEnd(go,false)
		end)
	else
		self.Preview.gameObject:SetActive(false)
		self.TouZiDot.gameObject:SetActive(true)
		self.m_SelectDot = 0
		self:InitClickTouZi()
		self:OnTouZiClick(1)
	end
end

function LuaDaFuWongTouziUseWnd:InitClickTouZi()
	self.m_DotList = {}
	local dot = {self.Dot1,self.Dot2,self.Dot3,self.Dot4,self.Dot5,self.Dot6}
	for i = 1,#dot do
		self.m_DotList[i] = self:InitOneTouZi(dot[i],i)
	end
end


function LuaDaFuWongTouziUseWnd:InitOneTouZi(go,index)
	local res = nil
	if not go then return res end
	local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
	local Select = go.transform:Find("Select").gameObject
	Select.gameObject:SetActive(false)
	label.gameObject:SetActive(false)
	UIEventListener.Get(go.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTouZiClick(index)
	end)
	res = {go = go,label = label,Select = Select}
	return res
end

function LuaDaFuWongTouziUseWnd:OnTouZiClick(index)
	if self.m_SelectDot == index then return end
	local lastView = self.m_DotList[self.m_SelectDot]
	if lastView then 
		lastView.Select.gameObject:SetActive(false) 
		lastView.label.gameObject:SetActive(false)
	end
	self.m_SelectDot = index
	local view = self.m_DotList[index]
	if view then 
		view.Select.gameObject:SetActive(true) 
		local landInfo = self:GetFinalLand(index)
		if landInfo then 
			view.label.gameObject:SetActive(true)
			view.label.text = landInfo.name
			view.label.color = NGUIText.ParseColor24(self.m_ColorList[landInfo.owner], 0)
		end
	end

end

function LuaDaFuWongTouziUseWnd:GetFinalLand(dot)
	local res = nil
	local id = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
	local curPlayerPos = LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[id] and LuaDaFuWongMgr.PlayerInfo[id].Pos or -1
	local curLandInfo = LuaDaFuWongMgr.LandInfo and LuaDaFuWongMgr.LandInfo[curPlayerPos]
	if curLandInfo then
		local resultPos = curPlayerPos
		for i = 1,dot do
			resultPos = LuaDaFuWongMgr.LandInfo[resultPos].next
		end
		local name = DaFuWeng_Land.GetData(resultPos).Name
		local owner = LuaDaFuWongMgr.LandInfo[resultPos].Owner
		if LuaDaFuWongMgr.PlayerInfo[owner] then 
			owner = LuaDaFuWongMgr.PlayerInfo[owner].round
		else
			owner = 0
		end
		res = {name = name,owner = owner} 
	end
	return res
end

function LuaDaFuWongTouziUseWnd:OnDragTouZi(go, delta)
	if self.m_IsDrag then return end
	if math.abs(delta.y) > math.abs(delta.x) then
		if delta.y >= 10 then
			self:StartRole(1,0)
		elseif delta.y <= -10 then
			self:StartRole(-1,0)
		end
	else
		if delta.x >= 10 then
			self:StartRole(0,-1)
		elseif delta.x <= -10 then
			self:StartRole(0,1)
		end
	end
end

function LuaDaFuWongTouziUseWnd:StartRole(x,y)
	self.m_IsDrag = true
	local CurInfo = self.m_CurTouZiRotation
	local ResInfo = self:GetResRotationInfo(x,y)
	local timeLen = 0.2
	local deltaTime = 0.05
	local times = timeLen / deltaTime
	local curtime = 1

	local fakedeltaX = x * 90 / times
	local fakedeltaY = y * 90 / times
	local fakedeltaZ = 0

	if self.m_RoleTick then UnRegisterTick(self.m_RoleTick) self.m_RoleTick = nil end
	self.m_TouZiTick = RegisterTickWithDuration(function()
		if curtime > times then
			self.m_CurTouZiRotation = ResInfo
			self.m_IsDrag = false
			return
		end

		if self.m_TouZiRo then
			self.m_FakeTouZiRo.transform:Rotate(Vector3(fakedeltaX,fakedeltaY,fakedeltaZ),Space.World)
			self.m_TouZiRo.transform.localRotation = self.m_FakeTouZiRo.transform.rotation
		end

		curtime = curtime + 1
	end,deltaTime * 1000,(timeLen + deltaTime) * 1000)
end


function LuaDaFuWongTouziUseWnd:GetResRotationInfo(xValue,yValue)
	local curInfo = self.m_CurTouZiRotation
	local resX,resY,rexZ = xValue * 90,yValue * 90,0
	local resInfo = {curInfo[1] + resX ,curInfo[2] + resY,curInfo[3] + rexZ}
	if resInfo[1] >= 360 or resInfo[1] <= -360  then
		resInfo[1] = resInfo[1] % 360
	elseif resInfo[2] >= 360 or resInfo[2] <= -360 then
		resInfo[2] = resInfo[2] % 360
	elseif resInfo[3] >= 360 or resInfo[3] <= -360 then
		resInfo[3] = resInfo[3] % 360
	end

	return resInfo
end

function  LuaDaFuWongTouziUseWnd:OnDragStartOrEnd(go,isStart)
	--print("OnDragStartOrEnd",isStart)
	--self.m_IsDrag = isStart
end

function LuaDaFuWongTouziUseWnd:GetTouZiDot()
	if self.m_FakeTouZiRo then
		local upAngle = Vector3.Angle(Vector3.forward,self.m_FakeTouZiRo.transform.up)
		local forwardAngle = Vector3.Angle(Vector3.forward,self.m_FakeTouZiRo.transform.forward)
		local rightAngle = Vector3.Angle(Vector3.forward,self.m_FakeTouZiRo.transform.right)

		if math.floor(upAngle + 0.5) == 0 then return 1 end
		if math.floor(upAngle + 0.5) == 180 then return 6 end
		if math.floor(forwardAngle + 0.5) == 0 then return 3 end
		if math.floor(forwardAngle + 0.5) == 180 then return 4 end
		if math.floor(rightAngle + 0.5) == 0 then return 5 end
		if math.floor(rightAngle + 0.5) == 180 then return 2 end
	end
	return 0
end

--@region UIEvent

function LuaDaFuWongTouziUseWnd:OnOKBtnClick()
	if self:GetType() == 1 then
		self.m_SelectDot = self:GetTouZiDot()
	end
	Gac2Gas.DaFuWengUseCard(self.m_ItemId,self.m_SelectDot)
	CUIManager.CloseUI(CLuaUIResources.DaFuWongTouziUseWnd)
end

function LuaDaFuWongTouziUseWnd:OnCancelButtonClick()
	CUIManager.CloseUI(CLuaUIResources.DaFuWongTouziUseWnd)
end

--@endregion UIEvent

function LuaDaFuWongTouziUseWnd:OnDisable()
	LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.CardUse)
	self.TouZiTexture.mainTexture = nil
	if self.m_TouZiRo then
		self.m_TouZiRo:Destroy()
		self.m_TouZiRo = nil
	end
	if self.m_FakeTouZiRo then
		self.m_FakeTouZiRo:Destroy()
		self.m_FakeTouZiRo = nil
	end
	if self.m_RoleTick then UnRegisterTick(self.m_RoleTick) self.m_RoleTick = nil end
	
	CUIManager.DestroyModelTexture("__DaFuWongTouZiCard__")
end
