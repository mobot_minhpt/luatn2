local Extensions = import "Extensions"
local EnumGuildFreightStatus = import "L10.Game.EnumGuildFreightStatus"
local CommonDefs = import "L10.Game.CommonDefs"
local CFreightMgr = import "L10.Game.CFreightMgr"
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"
-- local CFreightCargoList = import "L10.UI.CFreightCargoList"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCargoListItemCell = import "L10.UI.CCargoListItemCell"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"


CLuaFreightCargoList = class()
RegistClassMember(CLuaFreightCargoList,"itemcell")
RegistClassMember(CLuaFreightCargoList,"table")
RegistClassMember(CLuaFreightCargoList,"itemList")
RegistClassMember(CLuaFreightCargoList,"OnCargoClicked")

function CLuaFreightCargoList:Awake()
    self.itemcell = self.transform:Find("ItemCell").gameObject
    self.table = self.transform:Find("Table"):GetComponent(typeof(UITable))
self.itemList = nil
self.OnCargoClicked = nil

end

function CLuaFreightCargoList:Init( )
    -- CommonDefs.ListClear(self.itemList)
    self.itemList = {}
    self.itemcell:SetActive(false)
    Extensions.RemoveAllChildren(self.table.transform)
    if CFreightMgr.Inst.cargoList.Count == 0 then
        return
    end
    do
        local i = 0
        while i < CFreightMgr.Inst.cargoList.Count do
            local templateId = CFreightMgr.Inst.cargoList[i].templateId
            local item = NGUITools.AddChild(self.table.gameObject, self.itemcell)
            CommonDefs.GetComponent_GameObject_Type(item, typeof(CCargoListItemCell)):Init(i, CClientMainPlayer.Inst.Id ~= CFreightMgr.Inst.playerId)
            item:SetActive(true)
            table.insert( self.itemList,item )
            UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function(go)
                self:OnItemClicked(go)
            end)
            i = i + 1
        end
    end
    self.table:Reposition()

    if CClientMainPlayer.Inst.Id ~= CFreightMgr.Inst.playerId then
        do
            local i = 0
            while i < CFreightMgr.Inst.cargoList.Count do
                if CFreightMgr.Inst.cargoList[i].status == EnumGuildFreightStatus.eNotFilledNeedHelp then
                    self:OnItemClicked(self.itemList[i+1])
                    return
                end
                i = i + 1
            end
        end
        if self.OnCargoClicked ~= nil then
            self.OnCargoClicked(-1)
        end
    else
        self:UpdateSelectTarget(0)
    end
end
function CLuaFreightCargoList:OnItemClicked( go) 
    for i,v in ipairs(self.itemList) do
        local cmp = v:GetComponent(typeof(CCargoListItemCell))
        if go == v then
            if self.OnCargoClicked ~= nil then
                self.OnCargoClicked(cmp.cargoIndex)
            end
            cmp.Selected = true
            CFreightEquipSubmitMgr.curSelectIndex = i-1
        else
            cmp.Selected = false
        end
    end
end
function CLuaFreightCargoList:UpdateSelectTarget( index) 
    if CFreightMgr.Inst.playerId == CClientMainPlayer.Inst.Id then

        for i,v in ipairs(self.itemList) do
            local cmp = v:GetComponent(typeof(CCargoListItemCell))
            if cmp.isFillAvailable and not cmp.Filled then
                self:OnItemClicked(self.itemList[i])
                return
            end
        end

        for i,v in ipairs(self.itemList) do
            local cmp = v:GetComponent(typeof(CCargoListItemCell))
            if not cmp.Filled then
                self:OnItemClicked(self.itemList[i])
                return
            end
        end

        if self.OnCargoClicked ~= nil then
            self.OnCargoClicked(-1)
        end
    else
        for i,v in ipairs(self.itemList) do
            local cmp = v:GetComponent(typeof(CCargoListItemCell))
            if CFreightMgr.Inst.cargoList[cmp.cargoIndex].status == EnumGuildFreightStatus.eNotFilledNeedHelp then
                self:OnItemClicked(self.itemList[i])
                return
            end
        end

        if self.OnCargoClicked ~= nil then
            self.OnCargoClicked(-1)
        end
    end
end

function CLuaFreightCargoList:OnEnable()
    g_ScriptEvent:AddListener("OnFillCargoSuccess", self, "FillCargoSuccess")
    g_ScriptEvent:AddListener("OnRequestHelpSuccess", self, "RequestHelpSuccess")
    g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")

end
function CLuaFreightCargoList:OnDisable()
    g_ScriptEvent:RemoveListener("OnFillCargoSuccess", self, "FillCargoSuccess")
    g_ScriptEvent:RemoveListener("OnRequestHelpSuccess", self, "RequestHelpSuccess")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
end

function CLuaFreightCargoList:FillCargoSuccess(args)
    local index=args[0]
    CItemInfoMgr.CloseItemInfoWnd()
    self.itemList[index+1]:GetComponent(typeof(CCargoListItemCell)):FillCargo()
    self:UpdateSelectTarget(index)
end
function CLuaFreightCargoList:RequestHelpSuccess(args)
    local index=args[0]

end
function CLuaFreightCargoList:OnSetItemAt(args)
    self:Refresh()
end
function CLuaFreightCargoList:OnSendItem(args)
    self:Refresh()
end
function CLuaFreightCargoList:Refresh()
    local id1 = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local id2 = CFreightMgr.Inst.playerId
    local b = id1~=id2
    for i,v in ipairs(self.itemList) do
        local cmp = v:GetComponent(typeof(CCargoListItemCell))
        local selected = cmp.Selected
        cmp:Init(i-1,b)
        cmp.Selected = selected
    end
    if self.OnCargoClicked ~= nil then
        self.OnCargoClicked(CFreightEquipSubmitMgr.curSelectIndex)
    end

end
