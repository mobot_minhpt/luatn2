-- Auto Generated!!
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"
local CFreightEquipSubmitWnd = import "L10.UI.CFreightEquipSubmitWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local LocalString = import "LocalString"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CFreightEquipSubmitWnd.m_Init_CS2LuaHook = function (this) 
    if CFreightEquipSubmitMgr.title ~= "" then
        this.titleLabel.text = CFreightEquipSubmitMgr.title
    else
        this.titleLabel.text = LocalString.GetString("选择装填的装备")
    end
    this.equipments:Init(CFreightEquipSubmitMgr.curSelectIndex)
    if not System.String.IsNullOrEmpty(CFreightEquipSubmitMgr.bottomBtnLabel) and CFreightEquipSubmitMgr.bottomBtnAction ~= nil then
        this.bottomBtn:SetActive(true)
        this.buttonLabel.text = CFreightEquipSubmitMgr.bottomBtnLabel
        this.background.height = this.defaultheight + 100
    else
        this.bottomBtn:SetActive(false)
        this.background.height = this.defaultheight
    end
end
CFreightEquipSubmitWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseBtnClick, VoidDelegate, this), true)
    --UIEventListener.Get(submitBtn).onClick += this.OnSubmitBtnClick;
    UIEventListener.Get(this.bottomBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.bottomBtn).onClick, MakeDelegateFromCSFunction(this.OnBottomButtonClick, VoidDelegate, this), true)

    this.equipments.OnEquipSelected = MakeDelegateFromCSFunction(this.SetCurSelectEquip, MakeGenericClass(Action1, String), this)
end

CFreightEquipSubmitMgr.m_OpenEquipmentChooseWnd_CS2LuaHook = function (key, dic, titleStr, select, confirm, button, bottmButtonLabel, bottomButtonAction, emptyStr) 
    if nil == titleStr then
        titleStr = LocalString.GetString("选择装填的装备")
    end
    if nil == button then
        button = LocalString.GetString("装填")
    end

    CFreightEquipSubmitMgr.managerBroadcastKey = key
    CFreightEquipSubmitMgr.curSelectIndex = select
    CFreightEquipSubmitMgr.submitEquipList = dic
    CFreightEquipSubmitMgr.title = titleStr
    CFreightEquipSubmitMgr.showConfirm = confirm
    CFreightEquipSubmitMgr.buttonLabel = button
    CFreightEquipSubmitMgr.bottomBtnLabel = bottmButtonLabel
    CFreightEquipSubmitMgr.bottomBtnAction = bottomButtonAction
    CFreightEquipSubmitMgr.emptyLabel = emptyStr
    CUIManager.ShowUI(CUIResources.FreightEquipSubmitWnd)
end
