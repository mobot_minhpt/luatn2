local CClientFurniture=import "L10.Game.CClientFurniture"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local Vector3 = import "UnityEngine.Vector3"
local UILabel = import "UILabel"
local CUITexture=import "L10.UI.CUITexture"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local MessageWndManager = import "L10.UI.MessageWndManager"

CLuaWinterHouseUnlockPreviewWnd = class()

RegistClassMember(CLuaWinterHouseUnlockPreviewWnd, "m_ModelTextureLoader")
RegistClassMember(CLuaWinterHouseUnlockPreviewWnd, "m_ModelTexture")
RegistClassMember(CLuaWinterHouseUnlockPreviewWnd, "m_ConfirmLabel")
RegistClassMember(CLuaWinterHouseUnlockPreviewWnd, "m_UnlockButton")
RegistClassMember(CLuaWinterHouseUnlockPreviewWnd, "m_TemplateId")
RegistClassMember(CLuaWinterHouseUnlockPreviewWnd, "m_FurnitureId")
RegistChildComponent(CLuaWinterHouseUnlockPreviewWnd,"m_MoneyCtrl", CCurentMoneyCtrl)

function CLuaWinterHouseUnlockPreviewWnd:Awake()
    self.m_ConfirmLabel = self.transform:Find("Anchor/ConfirmLabel"):GetComponent(typeof(UILabel))
    self.m_ModelTexture = self.transform:Find("Anchor/Preview"):GetComponent(typeof(CUITexture))
    self.m_UnlockButton = self.transform:Find("Anchor/UnlockButton").gameObject

    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadModel(ro)
    end)

    UIEventListener.Get(self.m_UnlockButton).onClick  =DelegateFactory.VoidDelegate(function(go)
        MessageWndManager.ShowOKCancelMessage(CLuaWinterHouseUnlockPreviewMgr.ConfirmMsg, DelegateFactory.Action(function () 
            Gac2Gas.RequestEnableWinterFunitureAtHome(self.m_FurnitureId)
            CUIManager.CloseUI(CLuaUIResources.WinterHouseUnlockPreviewWnd)
        end), nil, nil, nil, false)
    end)
end

function CLuaWinterHouseUnlockPreviewWnd:Init()
    if not CLuaWinterHouseUnlockPreviewMgr.TemplateId or not CLuaWinterHouseUnlockPreviewMgr.FurnitureId then
        return
    end
    self.m_FurnitureId = CLuaWinterHouseUnlockPreviewMgr.FurnitureId
    self.m_TemplateId = CLuaWinterHouseUnlockPreviewMgr.TemplateId
    self.m_MoneyCtrl:SetType(CLuaWinterHouseUnlockPreviewMgr.MoneyType)
    self.m_MoneyCtrl:SetCost(CLuaWinterHouseUnlockPreviewMgr.Cost)
    self.m_ConfirmLabel.text = CLuaWinterHouseUnlockPreviewMgr.ConfirmMsg

    self.m_ModelTexture.mainTexture=CUIManager.CreateModelTexture("__SnowSceneUnlockPreview__", self.m_ModelTextureLoader,180,0.05,-1,4.66,false,true,1)
end

function CLuaWinterHouseUnlockPreviewWnd:LoadModel(ro)
    local data = Zhuangshiwu_Zhuangshiwu.GetData(self.m_TemplateId)
    local resName = data.ResNameXue
    -- if resName and resName~="" then
    local resid = SafeStringFormat3("_%02d",data.ResIdXue)
    local prefabname = "Assets/Res/Character/Jiayuan/" .. data.ResNameXue .. "/Prefab/" .. data.ResNameXue .. resid .. ".prefab"
    if data.ProgramSnow>0 then
        resid = SafeStringFormat3("_%02d",data.ResId)
        prefabname = "Assets/Res/Character/Jiayuan/" .. data.ResName .. "/Prefab/" .. data.ResName .. resid .. ".prefab"
        ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(renderObject)
            CClientFurniture.SetSnowLevel(ro,true,self.m_TemplateId)
        end))
    end
    ro:LoadMain(prefabname)
    -- end

    if data.FXXue>0 then
        local fx = CEffectMgr.Inst:AddObjectFX(data.FXXue, ro, 0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
        ro:AddFX("SelfFx", fx)
    end
    
    local whdata = WinterHouse_Zhuangshiwu.GetData(self.m_TemplateId)
    ro.Scale = whdata.ModelScale
end

function CLuaWinterHouseUnlockPreviewWnd:OnDestroy()
    CLuaWinterHouseUnlockPreviewMgr.ResetParms()
    self.m_ModelTexture.mainTexture=nil
    CUIManager.DestroyModelTexture("__SnowSceneUnlockPreview__")
end
