local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local Color = import "UnityEngine.Color"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local MessageWndManager = import "L10.UI.MessageWndManager"
local QnRadioBox = import "L10.UI.QnRadioBox"
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local QnTipButton = import "L10.UI.QnTipButton"
local CPlayerShopPopupMenuInfoMgr = import "L10.UI.CPlayerShopPopupMenuInfoMgr"
local String = import "System.String"
local SearchOption = import "L10.UI.SearchOption"
local UIScrollView = import "UIScrollView"
local UITable = import "UITable"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CPlayerShop_Shelf = import "L10.UI.CPlayerShop_Shelf"
local BoxCollider = import "UnityEngine.BoxCollider"
local CButton = import "L10.UI.CButton"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UIGrid = import "UIGrid"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"

LuaQiugouWnd = class()
RegistChildComponent(LuaQiugouWnd,"CloseButton", GameObject)
RegistChildComponent(LuaQiugouWnd,"QnRadioBox", GameObject)
RegistChildComponent(LuaQiugouWnd,"BuyNode", GameObject)
RegistChildComponent(LuaQiugouWnd,"SellNode", GameObject)

RegistClassMember(LuaQiugouWnd, "cButtonSelect")
RegistClassMember(LuaQiugouWnd, "cButtonSelectName")
RegistClassMember(LuaQiugouWnd, "cButtonSelectType")
RegistClassMember(LuaQiugouWnd, "cButtonSelectTabs")

function LuaQiugouWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.QiugouWnd)
end

function LuaQiugouWnd:OnEnable()
	g_ScriptEvent:AddListener("QueryItemPriceBack", self, "SetItemPrice")
	g_ScriptEvent:AddListener("UpdateBuyOrderItems", self, "InitBuyNodeRightByData")
	g_ScriptEvent:AddListener("UpdateBuyOrderTotalItems", self, "InitBuyNodeLeftTotalData")
	g_ScriptEvent:AddListener("UpdateBuyOrderListItems", self, "InitBuyNodeLeftListData")
	g_ScriptEvent:AddListener("UpdateSellItemInfo", self, "UpdateSellItemTable")
	g_ScriptEvent:AddListener("UpdateBuyOrderTabNums", self, "UpdateBuyOrderTabNums")
end

function LuaQiugouWnd:OnDisable()
	g_ScriptEvent:RemoveListener("QueryItemPriceBack", self, "SetItemPrice")
	g_ScriptEvent:RemoveListener("UpdateBuyOrderItems", self, "InitBuyNodeRightByData")
	g_ScriptEvent:RemoveListener("UpdateBuyOrderTotalItems", self, "InitBuyNodeLeftTotalData")
	g_ScriptEvent:RemoveListener("UpdateBuyOrderListItems", self, "InitBuyNodeLeftListData")
	g_ScriptEvent:RemoveListener("UpdateSellItemInfo", self, "UpdateSellItemTable")
	g_ScriptEvent:RemoveListener("UpdateBuyOrderTabNums", self, "UpdateBuyOrderTabNums")
end

function LuaQiugouWnd:NumValueChange(value)
		self:PriceValueChange()
end

function LuaQiugouWnd:PriceValueChange(value)
	local price = self.m_SingleItemPriceInput:GetValue()
	self:SetPriceTip()
	local count = self.m_SingleItemNumInput:GetValue()
	local totalPrice = price * count
	if totalPrice > CPlayerShop_Shelf.TotalPriceLimit then
		self.m_TotalPriceInput.text = LocalString.GetString("超过10亿")
		self.m_TaxInput.text = tostring((CPlayerShopMgr.Inst:GetTax(price, count)))
	else
		self.m_TotalPriceInput.text = tostring(totalPrice)
		self.m_TaxInput.text = tostring((CPlayerShopMgr.Inst:GetTax(price, count)))
	end
end

function LuaQiugouWnd:GetPricePercent(basicPrice, currentPrice)
    local t = 0
		if basicPrice and currentPrice then
			if basicPrice > 0 then
				t = math.floor(100 * currentPrice / basicPrice + 0.5)
			end
		end
    return t
end

function LuaQiugouWnd:SetPriceTip()
	if not self.m_PriceTipLabel.gameObject.activeSelf then
		return
	end
	local t = self:GetPricePercent(self.m_BasicPrice, self.m_SingleItemPriceInput:GetValue())
	self.m_PriceTipLabel.color = t > 100 and Color.red or Color.green
	if t > 100 then
		self.m_PriceTipLabel.text = System.String.Format(LocalString.GetString("推荐价格：+{0}%"), t - 100)
	else
		self.m_PriceTipLabel.text = System.String.Format(LocalString.GetString("推荐价格：{0}%"), t - 100)
	end
end

function LuaQiugouWnd:SetItemPrice(basisPrice, minPrice, maxPrice)
	self.m_TotalPriceInput = self.BuyNode.transform:Find('Left/TotalPrice/Sprite/Label'):GetComponent(typeof(UILabel))
	self.m_TaxInput = self.BuyNode.transform:Find('Left/ExtraPrice/Sprite/Label'):GetComponent(typeof(UILabel))

	self.m_SingleItemNumInput:SetMinMax(1, 20, 1)
	self.m_SingleItemNumInput:SetValue(1, true)

	self.m_MaxPrice = maxPrice
	self.m_MinPrice = minPrice
	self.m_BasicPrice = basisPrice
	self.m_SingleItemPriceInput:SetButtonType(QnAddSubAndInputButton.ButtonType.OnlyAllowButtons)

	local step = math.floor(tonumber((self.m_MaxPrice - self.m_MinPrice) * CPlayerShopMgr.PriceParameters or 0))
	if step < 1 then
		step = 1
	end
	self.m_SingleItemPriceInput:SetMinMax(self.m_MinPrice, self.m_MaxPrice, step)
	self.m_SingleItemPriceInput:SetValue(self.m_BasicPrice, true)
	self.m_PriceTipLabel.gameObject:SetActive(true)
	self:SetPriceTip()
end

function LuaQiugouWnd:ReloadFurnitureData()
	if self.SelectLevel >= 0 and self.SelectLevel < self.m_LevelActions.Count and self.SelectType >= 0 and self.SelectType < self.m_TypeActions.Count then
		local furnitures = CPlayerShopMgr.Inst:SearchFurniture(self.m_LevelActions[self.SelectLevel].text, self.m_TypeActions[self.SelectType].text)
		if not self.m_SearchTemplateIds then
			self.m_SearchTemplateIds = CreateFromClass(MakeGenericClass(List, UInt32))
		end
		if not self.m_SearchTemplateColors then
			self.m_SearchTemplateColors = CreateFromClass(MakeGenericClass(List, Color))
		end

		if not self.m_SearchNames then
			self.m_SearchNames = CreateFromClass(MakeGenericClass(List, String))
		end

		CommonDefs.ListClear(self.m_SearchTemplateIds)
		CommonDefs.ListClear(self.m_SearchTemplateColors)
		CommonDefs.ListClear(self.m_SearchNames)

		local colorDic = CreateFromClass(MakeGenericClass(Dictionary, String, UInt32))
		CommonDefs.DictAdd(colorDic, typeof(String), "COLOR_ORANGE", typeof(UInt32), 1)
		CommonDefs.DictAdd(colorDic, typeof(String), "COLOR_BLUE", typeof(UInt32), 2)
		CommonDefs.DictAdd(colorDic, typeof(String), "COLOR_RED", typeof(UInt32), 3)
		CommonDefs.DictAdd(colorDic, typeof(String), "COLOR_PURPLE", typeof(UInt32), 4)

		CommonDefs.DictIterate(furnitures, DelegateFactory.Action_object_object(function(key,val)
			local item = Item_Item.GetData(key)
			if item then
				if item.PlayerShopPrice ~= 0 or item.ValuablesFloorPrice ~= 0 then
					CommonDefs.ListAdd(self.m_SearchTemplateIds,typeof(UInt32), key)
				end
			end
		end))

		CommonDefs.ListSort1(self.m_SearchTemplateIds, typeof(UInt32), DelegateFactory.Comparison_uint(function (a, b)
			local data1 = Item_Item.GetData(a)
			local data2 = Item_Item.GetData(b)
			local name1 = data1.Name
			local name2 = data2.Name
			if (name1 == name2) then
				local colorIdx1 = 1
				(function ()
					local __try_get_result
					__try_get_result, colorIdx1 = CommonDefs.DictTryGet(colorDic, typeof(String), data1.NameColor, typeof(UInt32))
					return __try_get_result
				end)()
				local colorIdx2 = 1
				(function ()
					local __try_get_result
					__try_get_result, colorIdx2 = CommonDefs.DictTryGet(colorDic, typeof(String), data2.NameColor, typeof(UInt32))
					return __try_get_result
				end)()
				return NumberCompareTo(colorIdx1, colorIdx2)
			end
			return CommonDefs.StringCompareTo(name1, name2)
		end))
		self.m_SearchTemplateColors = CPlayerShopMgr.Inst:GetColors(self.m_SearchTemplateIds)


		do
			local i = 0
			while i < self.m_SearchTemplateIds.Count do
				CommonDefs.ListAdd(self.m_SearchNames, typeof(String), CommonDefs.DictGetValue(furnitures, typeof(UInt32), self.m_SearchTemplateIds[i]))
				i = i + 1
			end
		end

		self.m_FurnitureNameActions = CPlayerShopMgr.Inst:ConvertOptions(self.m_SearchNames,DelegateFactory.Action_int(function(row)
			if self.m_FurnitureNameButton ~= nil and self.m_FurnitureNameActions ~= nil and self.m_FurnitureNameActions.Count > row then
				self.SelectFurniture = row
				if self.m_FocusButton ~= nil then -- FonusButton need change to price show
					local focus = CPlayerShopMgr.Inst:IsFocusTemplate(self.m_SearchTemplateIds[self.SelectFurniture])
					local default
					if focus then
						default = LocalString.GetString("取消关注")
					else
						default = LocalString.GetString("关注此类")
					end
					self.m_FocusButton.Text = default
				end
				self.m_FurnitureNameButton.Text = self.m_FurnitureNameActions[row].text
				CommonDefs.GetComponentInChildren_Component_Type(self.m_FurnitureNameButton, typeof(UILabel)).color = self.m_SearchTemplateColors[row]
				Gac2Gas.QueryRequestBuyItemPrice(self.m_SearchTemplateIds[self.SelectFurniture])
			end
		end))

		if not self.m_FurnitureNameButton then
			self.m_FurnitureNameButton = self.BuyNode.transform:Find('Left/Name/QnButton'):GetComponent(typeof(QnTipButton))

			local onNameClick = function(button)
				CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(self.m_FurnitureNameActions), button.transform, AlignType.Top, self:GetColumnCount(self.m_FurnitureNameActions.Count), self.m_SearchTemplateColors, CreateFromClass(ItemSearchParams, self.m_SearchTemplateIds, false, SearchOption.All), DelegateFactory.Action(function ()
					self.m_FurnitureNameButton:SetTipStatus(false)
				end), 600,420)
			end
			CommonDefs.AddOnClickListener(self.m_FurnitureNameButton.gameObject,DelegateFactory.Action_GameObject(onNameClick),false)
		end
		if self.m_FurnitureNameActions.Count > 0 then
			local sfIndex = 0
			if self.RequestBuyItemData then
				local count = self.m_FurnitureNameActions.Count
				for i=0,count-1 do
					if self.m_FurnitureNameActions[i].text == self.RequestBuyItemData.Name then
						sfIndex = i
						break
					end
				end
			end

			self.RequestBuyItemData = nil
			self.SelectFurniture = sfIndex
			self.m_FurnitureNameButton.Text = self.m_FurnitureNameActions[sfIndex].text
			CommonDefs.GetComponentInChildren_Component_Type(self.m_FurnitureNameButton, typeof(UILabel)).color = self.m_SearchTemplateColors[sfIndex]
			--self:QuerySellCount(self.m_SearchTemplateIds)
			Gac2Gas.QueryRequestBuyItemPrice(self.m_SearchTemplateIds[self.SelectFurniture])
		else
			self.m_FurnitureNameButton.Text = LocalString.GetString("空")
			MessageWndManager.ShowOKMessage(LocalString.GetString("当前你选择的分类中没有任何装饰物,请重新选择！"), nil)
		end
	else
		self.m_FurnitureNameButton.Text = LocalString.GetString("空")
		MessageWndManager.ShowOKMessage(LocalString.GetString("当前你选择的分类中没有任何装饰物,请重新选择！"), nil)
	end
end

function LuaQiugouWnd:ReloadTypeData()
	self.m_TypeActions = CPlayerShopMgr.Inst:ConvertOptions(CPlayerShopMgr.Inst:GetFurnitureTypeGroupNames(),DelegateFactory.Action_int(function(row)
		if self.m_TypeButton ~= nil and self.m_TypeActions ~= nil and self.m_TypeActions.Count > row then
			self.SelectType = row
			self.m_TypeButton.Text = self.m_TypeActions[self.SelectType].text
			self:ReloadFurnitureData()
		end
	end))

	if not self.SelectType then
		self.SelectType = -1
	end
	if self.m_TypeActions.Count > 0 then
		if self.RequestBuyItemData then
			local count = self.m_TypeActions.Count
			for i=0,count-1 do
				local name = self.m_TypeActions[i].text
				if name == self.RequestBuyItemData.TypeName then
					self.SelectType = i
					break
				end
			end
		else
			self.SelectType = math.min(math.max(self.SelectType, 0), self.m_TypeActions.Count - 1)
		end
		self.m_TypeButton.Text = self.m_TypeActions[self.SelectType].text
	end
end

function LuaQiugouWnd:GetColumnCount(count)
    local columns = 1
    if count >= 6 then
        columns = 2
    end
    return columns
end

function LuaQiugouWnd:InitBuyLevelData()
  local m_LevelActions = CPlayerShopMgr.Inst:ConvertOptions(CPlayerShopMgr.Inst:GetFurnitureLevelGroupNames(), DelegateFactory.Action_int(function(row)
    if self.m_LevelButton ~= nil and self.m_LevelActions ~= nil and self.m_LevelActions.Count > row then
        self.SelectLevel = row
        self.m_LevelButton.Text = self.m_LevelActions[self.SelectLevel].text
        self:ReloadTypeData(self.m_LevelActions[self.SelectLevel].text)
        self:ReloadFurnitureData()
    end
	end))
  self.m_LevelActions = m_LevelActions
	if not self.SelectLevel then
		self.SelectLevel = -1
	end
	if m_LevelActions.Count > 0 then
		if self.RequestBuyItemData then
			self.SelectLevel = self.RequestBuyItemData.Grade
		else
			self.SelectLevel = math.min(math.max(self.SelectLevel, 0), self.m_LevelActions.Count - 1)
		end
    self.m_LevelButton.Text = self.m_LevelActions[self.SelectLevel].text
  end

	local onLevelClick = function(button)
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(self.m_LevelActions), button.transform, AlignType.Bottom, self:GetColumnCount(self.m_LevelActions.Count), nil, nil, DelegateFactory.Action(function ()
        self.m_LevelButton:SetTipStatus(false)
    end), 600,420)
	end
	CommonDefs.AddOnClickListener(self.m_LevelButton.gameObject,DelegateFactory.Action_GameObject(onLevelClick),false)
end

function LuaQiugouWnd:InitBuyNodeClickButton()
	if not self.m_LevelButton then
		self.m_LevelButton = self.BuyNode.transform:Find('Left/Level/QnButton'):GetComponent(typeof(QnTipButton))
		local onLevelClick = function(button)
			CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(self.m_LevelActions), button.transform, AlignType.Bottom, self:GetColumnCount(self.m_LevelActions.Count), nil, nil, DelegateFactory.Action(function ()
				self.m_LevelButton:SetTipStatus(false)
			end), 600,420)
		end
		CommonDefs.AddOnClickListener(self.m_LevelButton.gameObject,DelegateFactory.Action_GameObject(onLevelClick),false)
	end
	if not self.m_TypeButton then
		self.m_TypeButton = self.BuyNode.transform:Find('Left/FType/QnButton'):GetComponent(typeof(QnTipButton))

		local onTypeClick = function(button)
			CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(self.m_TypeActions), button.transform, AlignType.Bottom, self:GetColumnCount(self.m_TypeActions.Count), nil, nil, DelegateFactory.Action(function ()
				self.m_TypeButton:SetTipStatus(false)
			end), 600,420)
		end
		CommonDefs.AddOnClickListener(self.m_TypeButton.gameObject,DelegateFactory.Action_GameObject(onTypeClick),false)
	end
end

function LuaQiugouWnd:LevelAction(row)
	if self.m_LevelButton and self.m_LevelActions and self.m_LevelActions.Count > 0 then
		self.SelectLevel = row
		self.m_LevelButton.Text = self.m_LevelActions[self.SelectLevel].text
		self:ReloadTypeData(self.m_LevelActions[self.SelectLevel].text)
		self:ReloadFurnitureData()
	end
end

function LuaQiugouWnd:RgbToHex(color)
	local rgb = {color.r*255,color.g*255,color.b*255}
	local hexadecimal = ''

	for key, value in pairs(rgb) do
		local hex = ''

		while(value > 0)do
			local index = math.fmod(value, 16) + 1
			value = math.floor(value / 16)
			hex = string.sub('0123456789ABCDEF', index, index) .. hex
		end

		if(string.len(hex) == 0)then
			hex = '00'

		elseif(string.len(hex) == 1)then
			hex = '0' .. hex
		end

		hexadecimal = hexadecimal .. hex
	end

	return hexadecimal
end

function LuaQiugouWnd:CheckSubmitButton()
	if self.uploadItemsFull then
		local submitBtn = self.BuyNode.transform:Find('Left/SubmitBtn').gameObject
		submitBtn:GetComponent(typeof(QnButton)).Enabled = false
	else
		local onSubmitClick = function(go)
			local price = self.m_SingleItemPriceInput:GetValue()
			local count = self.m_SingleItemNumInput:GetValue()

			local item = Item_Item.GetData(self.m_SearchTemplateIds[self.SelectFurniture])
			local color = CPlayerShopMgr.Inst:GetColor(self.m_SearchTemplateIds[self.SelectFurniture])
			local str = LocalString.GetString('以#287[c][%s]%s[-]的价格求购[c][FFD400]%s[-]个[c][%s]%s[-]，总价#287[c][%s]%s[-]。是否确认？\n（系统将先行扣除银两，下架时返还）')
			local moneyColor1 = self:RgbToHex(UILabel.GetMoneyColor(price))
			local moneyColor2 = self:RgbToHex(UILabel.GetMoneyColor(price*count))
			str = SafeStringFormat3(str,moneyColor1,price,count,self:RgbToHex(color),item.Name,moneyColor2,price*count)
			MessageWndManager.ShowOKCancelMessage(str, DelegateFactory.Action(function ()
				Gac2Gas.CreateRequestBuyOrder(self.m_SearchTemplateIds[self.SelectFurniture],count,price,false)
			end), nil, nil, nil, false)
		end
		local submitBtn = self.BuyNode.transform:Find('Left/SubmitBtn').gameObject
		submitBtn:GetComponent(typeof(QnButton)).Enabled = true
		CommonDefs.AddOnClickListener(submitBtn,DelegateFactory.Action_GameObject(onSubmitClick),false)
	end
end

function LuaQiugouWnd:InitBuyNodeLeft()
	self:InitBuyNodeClickButton()
  self:InitBuyLevelData()

  self:LevelAction(self.SelectLevel)
	self:CheckSubmitButton()

	local tipBtn = self.BuyNode.transform:Find('TipBtn').gameObject
	local onTipClick = function(go)
		g_MessageMgr:ShowMessage('QiugouTip')
	end
	CommonDefs.AddOnClickListener(tipBtn,DelegateFactory.Action_GameObject(onTipClick),false)
end

function LuaQiugouWnd:InitBuyNodeRightSingleNode(node,data)
  if data then
		node.gameObject:SetActive(true)
    node:Find('Info').gameObject:SetActive(true)
    node:Find('Tip').gameObject:SetActive(false)
    node:Find('Sprite/wu').gameObject:SetActive(false)

    node:Find('Info/PriceLabel'):GetComponent(typeof(UILabel)).text = data[3]
    node:Find('Info/BuyNum'):GetComponent(typeof(UILabel)).text = LocalString.GetString('求购数 ') .. data[4]
		local item = Item_Item.GetData(tonumber(data[2]))
    node:Find('Sprite/ItemTexture').gameObject:SetActive(true)
    node:Find('Sprite/ItemTexture'):GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
    node:Find('Info/Name'):GetComponent(typeof(UILabel)).text = item.Name
		local color = CPlayerShopMgr.Inst:GetColor(tonumber(data[2]))
		node:Find('Info/Name'):GetComponent(typeof(UILabel)).color = color

		node:GetComponent(typeof(BoxCollider)).enabled = true
		local onItemClick = function(go)
			LuaQiugouMgr.QiugouBuyItem = data
			CUIManager.ShowUI(CLuaUIResources.QiugouBuyWnd)
		end
		CommonDefs.AddOnClickListener(node.gameObject, DelegateFactory.Action_GameObject(onItemClick), false)

		local status1 = node:Find('status1').gameObject
		local status2 = node:Find('status2').gameObject
		local expireTime = tonumber(data[6])
		if CServerTimeMgr.Inst.timeStamp - expireTime > 0 then
			status1:SetActive(false)
			status2:SetActive(true)
		else
			status1:SetActive(true)
			status2:SetActive(false)
		end
  else
    --node:Find('Info').gameObject:SetActive(false)
    --node:Find('Tip').gameObject:SetActive(true)
    --node:Find('Sprite/wu').gameObject:SetActive(true)
    --node:Find('Sprite/ItemTexture').gameObject:SetActive(false)
		--node:GetComponent(typeof(BoxCollider)).enabled = false
		node.gameObject:SetActive(false)
  end
end

function LuaQiugouWnd:InitBuyNodeRightByData()
	local exist = false
	for i=1,4 do
		local node = self.BuyNode.transform:Find('Right/BuyNode'..i)
		if LuaQiugouMgr.BuyOrderTable[i] then
			exist = true
			self:InitBuyNodeRightSingleNode(node,LuaQiugouMgr.BuyOrderTable[i])
		else
			self:InitBuyNodeRightSingleNode(node,nil)
		end
	end

	if exist then
		self.BuyNode.transform:Find('Right/Tip1').gameObject:SetActive(false)
		self.BuyNode.transform:Find('Right/Tip2').gameObject:SetActive(true)
	else
		self.BuyNode.transform:Find('Right/Tip1').gameObject:SetActive(true)
		self.BuyNode.transform:Find('Right/Tip2').gameObject:SetActive(false)
	end
end

function LuaQiugouWnd:InitBuyNodeRight()
  self:InitBuyNodeRightSingleNode(self.BuyNode.transform:Find('Right/BuyNode1'),nil)
  self:InitBuyNodeRightSingleNode(self.BuyNode.transform:Find('Right/BuyNode2'),nil)
  self:InitBuyNodeRightSingleNode(self.BuyNode.transform:Find('Right/BuyNode3'),nil)
  self:InitBuyNodeRightSingleNode(self.BuyNode.transform:Find('Right/BuyNode4'),nil)
	self.BuyNode.transform:Find('Right/Tip1').gameObject:SetActive(false)
	self.BuyNode.transform:Find('Right/Tip2').gameObject:SetActive(false)
	Gac2Gas.QueryMyRequestBuyOrder()
end

function LuaQiugouWnd:InitBuyNode()
  self:InitBuyNodeLeft()
  self:InitBuyNodeRight()
end

function LuaQiugouWnd:InitRequestBuyItem()
	local zData = {}
	Zhuangshiwu_Zhuangshiwu.Foreach(function(key,value)
		if value.ItemId == LuaQiugouMgr.RequestBuyItemId then
				zData.Grade = value.Grade - 1
				zData.Type = value.Type - 1
				zData.TypeName = CClientFurnitureMgr.GetTypeNameByType(value.Type)
				zData.Name = value.Name
		end
	end)
	self.RequestBuyItemData = zData
	LuaQiugouMgr.RequestBuyItemId = nil
end

-- MID
function LuaQiugouWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)
	self.m_SingleItemPriceInput = self.BuyNode.transform:Find('Left/Price/QnIncreseAndDecreaseButton'):GetComponent(typeof(QnAddSubAndInputButton))
	self.m_PriceTipLabel = self.BuyNode.transform:Find('Left/Price/PriceTipLabel'):GetComponent(typeof(UILabel))
	self.m_SingleItemNumInput = self.BuyNode.transform:Find('Left/Num/QnIncreseAndDecreaseButton'):GetComponent(typeof(QnAddSubAndInputButton))
	self.m_SingleItemPriceInput.onValueChanged = DelegateFactory.Action_uint(function(value)
		self:PriceValueChange(value)
	end)
	self.m_SingleItemNumInput.onValueChanged = DelegateFactory.Action_uint(function(value)
		self:NumValueChange(value)
	end)

  local qnRadioBoxNode = self.QnRadioBox:GetComponent(typeof(QnRadioBox))
  qnRadioBoxNode.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
    self.BuyNode:SetActive(index == 1)
    self.SellNode:SetActive(index == 0)
    if index == 0 then
      self:InitSellNode()
    elseif index == 1 then
      self:InitBuyNode()
    end
  end)
	if LuaQiugouMgr.RequestBuyItemId and LuaQiugouMgr.RequestBuyItemId > 0 then
		self:InitRequestBuyItem()
		qnRadioBoxNode:ChangeTo(1,true)
	else
		qnRadioBoxNode:ChangeTo(0,true)
	end
end

function LuaQiugouWnd:InitBuyNodeLeftScrollview(m_LevelButton,total)
	local showTable = {}
	if total then
		local levelGroupArray = CPlayerShopMgr.Inst:GetFurnitureLevelGroupNames()
		local count = levelGroupArray.Count
		for i=1,count do
			local furnitures = CPlayerShopMgr.Inst:SearchFurniture(levelGroupArray[i-1], self.cButtonSelectName)
			CommonDefs.DictIterate(furnitures, DelegateFactory.Action_object_object(function(key,val)
				local item = Item_Item.GetData(key)
				if item then
					if item.PlayerShopPrice ~= 0 or item.ValuablesFloorPrice ~= 0 then
						table.insert(showTable,key)
					end
				end
			end))
		end
	else
		local furnitures = CPlayerShopMgr.Inst:SearchFurniture(m_LevelButton.Text, self.cButtonSelectName)
		CommonDefs.DictIterate(furnitures, DelegateFactory.Action_object_object(function(key,val)
			local item = Item_Item.GetData(key)
			if item then
				if item.PlayerShopPrice ~= 0 or item.ValuablesFloorPrice ~= 0 then
					table.insert(showTable,key)
				end
			end
		end))
	end

	local scrollView = self.SellNode.transform:Find('TypeTableView/ScrollView'):GetComponent(typeof(CUIRestrictScrollView))
	local grid = self.SellNode.transform:Find('TypeTableView/ScrollView/Grid'):GetComponent(typeof(UIGrid))
	local emptyNode = self.SellNode.transform:Find('TypeTableView/EmptyLabel').gameObject
	Extensions.RemoveAllChildren(grid.transform)
	local exist = false
	for i,v in ipairs(showTable) do
		local itemTemplateId = v
		local num = 0
		if LuaQiugouMgr.BuyOrderTypeTotalTable[itemTemplateId] then
		 num = LuaQiugouMgr.BuyOrderTypeTotalTable[itemTemplateId]
		end
		if num > 0 then
			exist = true
			local node = NGUITools.AddChild(grid.gameObject,self.SellTemplateNode1)
			node:SetActive(true)
			local item = Item_Item.GetData(itemTemplateId)
			node.transform:Find('Sprite/ItemTexture'):GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
			local color = CPlayerShopMgr.Inst:GetColor(itemTemplateId)
			node.transform:Find('NameLabel'):GetComponent(typeof(UILabel)).color = color
			node.transform:Find('NameLabel'):GetComponent(typeof(UILabel)).text = item.Name
			node.transform:Find('CountLabel'):GetComponent(typeof(UILabel)).text = LocalString.GetString('求购：')..num

			local onClick = function(button)
				Gac2Gas.QueryRequestBuyOrderList(itemTemplateId,1)
			end
			CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(onClick),false)
		end
	end

	if exist then
		emptyNode:SetActive(false)
	else
		emptyNode:SetActive(true)
	end

	scrollView:ResetPosition()
	grid:Reposition()
end

function LuaQiugouWnd:InitBuyNodeLeftListData()
	self.SellTotalNode:SetActive(false)
	self.SellDetailNode:SetActive(true)

	local count = #LuaQiugouMgr.BuyOrderListItemTable

	local scrollView = self.SellNode.transform:Find('DetailTableView/ScrollView'):GetComponent(typeof(CUIRestrictScrollView))
	local grid = self.SellNode.transform:Find('DetailTableView/ScrollView/Grid'):GetComponent(typeof(UIGrid))
	Extensions.RemoveAllChildren(grid.transform)
	local emptyLabel = self.SellNode.transform:Find('DetailTableView/EmptyLabel').gameObject
	if count > 0 then
		emptyLabel:SetActive(false)
		local item = Item_Item.GetData(LuaQiugouMgr.BuyOrderListItemId)
		local color = CPlayerShopMgr.Inst:GetColor(LuaQiugouMgr.BuyOrderListItemId)

		for i,v in ipairs(LuaQiugouMgr.BuyOrderListItemTable) do
			local node = NGUITools.AddChild(grid.gameObject,self.SellTemplateNode2)
			node:SetActive(true)

			node.transform:Find('Sprite/ItemTexture'):GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
			node.transform:Find('Sprite/num'):GetComponent(typeof(UILabel)).text = v[3]
			node.transform:Find('NameLabel'):GetComponent(typeof(UILabel)).color = color
			node.transform:Find('NameLabel'):GetComponent(typeof(UILabel)).text = item.Name
			node.transform:Find('Label/Sprite/Label'):GetComponent(typeof(UILabel)).text = v[4]

			local onClick = function(button)
				LuaQiugouMgr.BuyOrderListSingleItemData = v
				CUIManager.ShowUI(CLuaUIResources.QiugouSellWnd)
			end
			CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(onClick),false)
		end
		scrollView:ResetPosition()
		grid:Reposition()
	else
		emptyLabel:SetActive(true)
	end

	self.m_SellListPageButton = self.SellNode.transform:Find('DetailTableView/Page/QnIncreseAndDecreaseButton'):GetComponent(typeof(QnAddSubAndInputButton))

	self.m_SellListPageButton:SetMinMax(1, LuaQiugouMgr.BuyOrderListTotalPage, 1)
	self.m_SellListPageButton.onValueChanged = DelegateFactory.Action_uint(function(value)
	end)
	self.m_SellListPageButton:SetValue(LuaQiugouMgr.BuyOrderListPage, true)
	self.m_SellListPageButton:OverrideText(LuaQiugouMgr.BuyOrderListPage..'/'..LuaQiugouMgr.BuyOrderListTotalPage)
	self.m_SellListPageButton.onValueChanged = DelegateFactory.Action_uint(function(value)
		self:PageValueChange(value)
	end)
end

function LuaQiugouWnd:PageValueChange(value)
	self.m_SellListPageButton:OverrideText(value..'/'..LuaQiugouMgr.BuyOrderListTotalPage)
	Gac2Gas.QueryRequestBuyOrderList(LuaQiugouMgr.BuyOrderListItemId,value)
end

function LuaQiugouWnd:InitBuyNodeLeftTotalData()
	self.SellTotalNode:SetActive(true)
	self.SellDetailNode:SetActive(false)

--	if LuaQiugouMgr.BuyOrderTypeTotalCount > 0 and self.cButtonSelect then
--		local showCount = LuaQiugouMgr.BuyOrderTypeTotalCount
--		if showCount > 99 then
--			showCount = '99+'
--		end
--		self.cButtonSelect.transform:Find('Label'):GetComponent(typeof(UILabel)).text = self.cButtonSelectName .. '(' .. showCount .. ')'
--	end

	local m_LevelButton = self.SellNode.transform:Find('TypeTableView/Level/QnButton'):GetComponent(typeof(QnTipButton))
	local levelGroupArray = CPlayerShopMgr.Inst:GetFurnitureLevelGroupNames()
	local totalName = LocalString.GetString('全部')
	levelGroupArray:Insert(0,totalName)
  local m_LevelActions = CPlayerShopMgr.Inst:ConvertOptions(levelGroupArray, DelegateFactory.Action_int(function(row)
    if m_LevelButton ~= nil and self.m_SellLevelActions ~= nil and self.m_SellLevelActions.Count > row then
        --self.SelectLevel = row
        m_LevelButton.Text = self.m_SellLevelActions[row].text
				if row == 0 then
					self:InitBuyNodeLeftScrollview(m_LevelButton,true)
				else
					self:InitBuyNodeLeftScrollview(m_LevelButton,false)
				end
    end
	end))
	self.m_SellLevelActions = m_LevelActions
	local onLevelClick = function(button)
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(m_LevelActions), button.transform, AlignType.Bottom, self:GetColumnCount(m_LevelActions.Count), nil, nil, DelegateFactory.Action(function ()
        m_LevelButton:SetTipStatus(false)
    end), 600,420)
	end
	CommonDefs.AddOnClickListener(m_LevelButton.gameObject,DelegateFactory.Action_GameObject(onLevelClick),false)

	m_LevelButton.Text = totalName
	self:InitBuyNodeLeftScrollview(m_LevelButton,true)
end

function LuaQiugouWnd:InitSellBar()
	self.SellRowTemplate = self.SellNode.transform:Find('SellTableView/SellRowTemplate').gameObject
	self.SellTemplateNode1 = self.SellNode.transform:Find('TypeTableView/Pool/PlayerShopTemplateItem').gameObject
	self.SellTemplateNode1:SetActive(false)
	self.SellTemplateNode2 = self.SellNode.transform:Find('DetailTableView/Pool/PlayerShopTemplateItem').gameObject
	self.SellTemplateNode2:SetActive(false)
	self.SellTotalNode = self.SellNode.transform:Find('TypeTableView').gameObject
	self.SellDetailNode = self.SellNode.transform:Find('DetailTableView').gameObject

	self.SellRowTemplate:SetActive(false)
	local scrollView = self.SellNode.transform:Find('SellTableView/Scrollview'):GetComponent(typeof(UIScrollView))
	local _table = self.SellNode.transform:Find('SellTableView/Scrollview/Table'):GetComponent(typeof(UITable))

	local typeTable = {}
	Zhuangshiwu_TypeIcon.Foreach(function(key,data)
		local d = {data.ID,data.Description}
		if data.QiuGou == 1 then
			table.insert(typeTable,d)
		end
	end)
	table.sort(typeTable,function(a,b)
		return a[1] < b[1]
	end)

	Extensions.RemoveAllChildren(_table.transform)

	self.cButtonSelectTabs = {}
	for i,v in ipairs(typeTable) do
		local zswData = v
		local node = NGUITools.AddChild(_table.gameObject,self.SellRowTemplate)

		node:SetActive(true)
		--
		node.transform:Find('Label'):GetComponent(typeof(UILabel)).text = zswData[2]

		local cButton = node:GetComponent(typeof(CButton))
		local clickFunction = function(go)
			if not self.cButtonSelect then
				self.cButtonSelect = cButton
			elseif self.cButtonSelect then
				self.cButtonSelect.Selected = false
			end
			cButton.Selected = true
			self.cButtonSelect = cButton
			self.cButtonSelectName = zswData[2]
			self.cButtonSelectType = zswData[1]

			local grid = self.SellNode.transform:Find('TypeTableView/ScrollView/Grid'):GetComponent(typeof(UIGrid))
			Extensions.RemoveAllChildren(grid.transform)

			Gac2Gas.QueryRequestBuyOrderCountByType(zswData[1])
			Gac2Gas.QueryRequestBuyOrderCount()
		end

		self.cButtonSelectTabs[zswData[1]] = {cButton.transform:Find('Label'):GetComponent(typeof(UILabel)), zswData[2]}

		CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(clickFunction),false)

		if i == 1 then
			clickFunction()
		end
	end

	_table:Reposition()
	scrollView:ResetPosition()
end

function LuaQiugouWnd:InitSellNode()
	self:InitSellBar()
end

function LuaQiugouWnd:UpdateSellItemTable()
	if self.cButtonSelectType then
		Gac2Gas.QueryRequestBuyOrderCountByType(self.cButtonSelectType)
	end
end

function LuaQiugouWnd:UpdateBuyOrderTabNums()
	if LuaQiugouMgr.BuyOrderTabNumTable then
		for i,v in pairs(self.cButtonSelectTabs) do
			if LuaQiugouMgr.BuyOrderTabNumTable[i] and LuaQiugouMgr.BuyOrderTabNumTable[i] > 0 then
				local count = LuaQiugouMgr.BuyOrderTabNumTable[i]
				if count > 99 then
					count = '99+'
				end
				v[1].text = v[2] .. '(' .. count .. ')'
			else
				v[1].text = v[2]
			end
		end
	end
end


return LuaQiugouWnd
