local EChatPanel = import "L10.Game.EChatPanel"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UIProgressBar = import "UIProgressBar"
local UISprite = import "UISprite"
local UILabel = import "UILabel"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CButton = import "L10.UI.CButton"
local QnCheckBox = import "L10.UI.QnCheckBox"
local StringActionKeyValuePair = import "L10.Game.StringActionKeyValuePair"
local Object = import "System.Object"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local StringStringKeyValuePair = import "L10.Game.StringStringKeyValuePair"
local QualityColor = import "L10.Game.QualityColor"

LuaZhuoYaoWarehouseView = class()

RegistChildComponent(LuaZhuoYaoWarehouseView, "m_TipButton", "TipButton", GameObject)
RegistChildComponent(LuaZhuoYaoWarehouseView, "m_SelectSorts", "SelectSorts", QnSelectableButton)
RegistChildComponent(LuaZhuoYaoWarehouseView, "m_SelectSortsArrowSprite", "SelectSortsArrowSprite", GameObject)
RegistChildComponent(LuaZhuoYaoWarehouseView, "m_DefaultBottomView", "DefaultBottomView", GameObject)
RegistChildComponent(LuaZhuoYaoWarehouseView, "m_LianHuaButton", "LianHuaButton", GameObject)
RegistChildComponent(LuaZhuoYaoWarehouseView, "m_Progress", "Progress", UIProgressBar)
RegistChildComponent(LuaZhuoYaoWarehouseView, "m_GradeLabel", "GradeLabel", UILabel)
RegistChildComponent(LuaZhuoYaoWarehouseView, "m_ChangeViewButton", "ChangeViewButton", CButton)
RegistChildComponent(LuaZhuoYaoWarehouseView, "m_DiscardBottomView", "DiscardBottomView", GameObject)
RegistChildComponent(LuaZhuoYaoWarehouseView, "m_ProgressLabel", "ProgressLabel", UILabel)
RegistChildComponent(LuaZhuoYaoWarehouseView, "m_DiscardButton", "DiscardButton", GameObject)
RegistChildComponent(LuaZhuoYaoWarehouseView, "m_RebackButton", "RebackButton", GameObject)
RegistChildComponent(LuaZhuoYaoWarehouseView, "m_QnTableView", "QnTableView", QnAdvanceGridView)

RegistClassMember(LuaZhuoYaoWarehouseView, "m_SelectSortIndex")
RegistClassMember(LuaZhuoYaoWarehouseView, "m_IsDiscardView")
RegistClassMember(LuaZhuoYaoWarehouseView, "m_SelectIds")
RegistClassMember(LuaZhuoYaoWarehouseView, "m_MaxBagSize")
RegistClassMember(LuaZhuoYaoWarehouseView, "m_YaoGuaiList")
RegistClassMember(LuaZhuoYaoWarehouseView, "m_CurBagSize")

function LuaZhuoYaoWarehouseView:Start()

    UIEventListener.Get(self.m_TipButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnTipButtonClick()
    end)

    self.m_SelectSorts.OnButtonSelected = DelegateFactory.Action_bool(function (selected )
        self:OnSelectSortsClick(selected)
    end)

    UIEventListener.Get(self.m_LianHuaButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnLianHuaButtonClick()
    end)

    UIEventListener.Get(self.m_ChangeViewButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnChangeViewButtonClick()
    end)
    self.m_IsDiscardView = true
    self:OnChangeViewButtonClick()
    UIEventListener.Get(self.m_DiscardButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnDiscardItemButtonClick()
    end)
    UIEventListener.Get(self.m_RebackButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnChangeViewButtonClick()
    end)
    self:Init()
end

function LuaZhuoYaoWarehouseView:Init()
    self.m_SelectIds = {}
    self.m_MaxBagSize = tonumber(ZhuoYao_Setting.GetData("BagMaxSize").Value)
    self.m_CurBagSize = tonumber(ZhuoYao_Setting.GetData("BagInitSize").Value)
    self.m_YaoGuaiList = {}
    self:OnChooseSortClicked(1)
    self.m_IsDiscardView = false
    self.m_QnTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self.m_MaxBagSize
        end,
        function(item, index)
            self:ItemAt(item,index)
        end)
    self.m_QnTableView:SetSelectRow(0, true)
    self.m_QnTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
    self:OnSyncZhuoYaoLevelAndExp(LuaZhuoYaoMgr.m_Level, LuaZhuoYaoMgr.m_Exp, LuaZhuoYaoMgr.m_BagSize)
    self:OnWarehouseYaoGuaiListUpdate()
end

function LuaZhuoYaoWarehouseView:OnEnable()
    self.m_QnTableView:ReloadData(true, true)
    g_ScriptEvent:AddListener("OnSyncZhuoYaoLevelAndExp", self, "OnSyncZhuoYaoLevelAndExp")
    g_ScriptEvent:AddListener("OnWarehouseYaoGuaiListUpdate", self, "OnWarehouseYaoGuaiListUpdate")
    g_ScriptEvent:AddListener("OnIncYaoGuaiBagSucc", self, "OnIncYaoGuaiBagSucc")
    self:OnSyncZhuoYaoLevelAndExp(LuaZhuoYaoMgr.m_Level, LuaZhuoYaoMgr.m_Exp, LuaZhuoYaoMgr.m_BagSize)
    self:OnWarehouseYaoGuaiListUpdate()
end

function LuaZhuoYaoWarehouseView:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncZhuoYaoLevelAndExp", self, "OnSyncZhuoYaoLevelAndExp")
    g_ScriptEvent:RemoveListener("OnWarehouseYaoGuaiListUpdate", self, "OnWarehouseYaoGuaiListUpdate")
    g_ScriptEvent:RemoveListener("OnIncYaoGuaiBagSucc", self, "OnIncYaoGuaiBagSucc")
end

function LuaZhuoYaoWarehouseView:OnSyncZhuoYaoLevelAndExp(level, exp, bagSize)
    self.m_CurBagSize = bagSize
    self.m_Progress.value = exp / ZhuoYao_Exp.GetData(level).ExpFull
    self.m_GradeLabel.text = level
    self.m_ProgressLabel.text = SafeStringFormat3("%d/%d",exp,ZhuoYao_Exp.GetData(level).ExpFull)
end

function LuaZhuoYaoWarehouseView:OnIncYaoGuaiBagSucc( bagSize)
    self.m_CurBagSize = bagSize
    self.m_QnTableView:ReloadData(true, true)
end

function LuaZhuoYaoWarehouseView:OnSelectAtRow(row)
    if row >= self.m_CurBagSize then
        local message = g_MessageMgr:FormatMessage("YaoGuai_Warehouse_Expand_Confirm", self:BindRepoUnlockPrice(self.m_CurBagSize))
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
            Gac2Gas.RequestIncYaoGuaiBag(1)
        end), nil, LocalString.GetString("开启"), nil, false)
        return
    end

    if not self.m_IsDiscardView then
        local data = (#self.m_YaoGuaiList >= (row + 1)) and self.m_YaoGuaiList[row + 1] or nil
        if data then
            local time = CServerTimeMgr.ConvertTimeStampToZone8Time(data.CatchTime)
            local labels = {}
            table.insert(labels, CreateFromClass(StringStringKeyValuePair, LocalString.GetString("评级"), tostring(ZhuoYao_TuJian.GetData(data.TemplateId).Grade)))
            table.insert(labels, CreateFromClass(StringStringKeyValuePair, LocalString.GetString("怪物品质"), tostring(data.Quality)))
            table.insert(labels, CreateFromClass(StringStringKeyValuePair, LocalString.GetString("捕获时间"), ToStringWrap(time, "yyyy-MM-dd")))
            local btns = {}
            table.insert(btns, CreateFromClass(StringActionKeyValuePair, LocalString.GetString("分享"), DelegateFactory.Action(function()
                LuaZhuoYaoWarehouseView:Share(data)
            end)))
            LuaZhuoYaoMgr:ShowTip(data, labels, btns)
        end
    end
end

function LuaZhuoYaoWarehouseView:Share(data)
    CUIManager.CloseUI(CLuaUIResources.YaoGuaiInfoWnd)
    local tujianId = data.TemplateId
    local tujiandata = ZhuoYao_TuJian.GetData(tujianId)
    local quality = data.Quality
    local time = CServerTimeMgr.ConvertTimeStampToZone8Time(data.CatchTime)
    local borderSpriteName,colorEnum = LuaZhuoYaoMgr:GetYaoGuaiQualityBorderColor(tujiandata, quality)
    local name = SafeStringFormat3("[c][%s][%s][-][/c]", NGUIText.EncodeColor24(QualityColor.GetRGBValue(colorEnum)), tujiandata.Name)
    local time = ToStringWrap(time, "yyyy-MM-dd")
    local msg = SafeStringFormat3(LocalString.GetString("<link button=%s,YaoGuaiInfo,%s,%s,%s,%s>"),tujiandata.Name, tujianId, quality, time, data.Level)
    CLuaShareMgr:ShowCommonShareBox({EChatPanel.Friend,EChatPanel.Sect,EChatPanel.Guild}, name, msg)
end

function LuaZhuoYaoWarehouseView:SortYaoGuaiList()
    if self.m_SelectSortIndex == 1 then
        table.sort(self.m_YaoGuaiList,function(a, b)
            return a.Quality > b.Quality 
        end)
    elseif self.m_SelectSortIndex == 2 then
        table.sort(self.m_YaoGuaiList,function(a, b)
            return a.Level > b.Level
        end)
    elseif self.m_SelectSortIndex == 3 then
        table.sort(self.m_YaoGuaiList,function(a, b)
            return a.CatchTime > b.CatchTime
        end)
    end
end

function LuaZhuoYaoWarehouseView:ItemAt(item,index)
    item.gameObject.name = tostring(index)
    local lockedSprite = item.transform:Find("LockedSprite").gameObject
    local noneItemBg = item.transform:Find("NoneItemBg").gameObject
    local unLockRoot = item.transform:Find("UnLockRoot").gameObject
    
    local data = (#self.m_YaoGuaiList >= (index + 1)) and self.m_YaoGuaiList[index + 1] or nil
    lockedSprite:SetActive(index >= self.m_CurBagSize)
    noneItemBg:SetActive(data == nil)
    unLockRoot:SetActive(data ~= nil)
    if not data then return end

    self:InitMonterItem(item, data)
end

function LuaZhuoYaoWarehouseView:InitMonterItem(item, data)
    local unLockRoot = item.transform:Find("UnLockRoot").gameObject
    local portrait = unLockRoot.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
    local qualitySprite = portrait.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
    local nameLabel = unLockRoot.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local qualityLabel = unLockRoot.transform:Find("QualityLabel"):GetComponent(typeof(UILabel))
    local levelLabel = unLockRoot.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local qnCheckBox = unLockRoot.transform:Find("QnCheckBox"):GetComponent(typeof(QnCheckBox))

    local tujianId = data.TemplateId
    local tujiandata = ZhuoYao_TuJian.GetData(tujianId)
    local name = tujiandata.Name
    local quality = data.Quality
    local borderSpriteName = LuaZhuoYaoMgr:GetYaoGuaiQualityBorderColor(tujiandata, quality)
    local level = data.Level
    local Id = data.Id
    local icon = tujiandata.Icon

    portrait:LoadNPCPortrait(icon)
    qualitySprite.spriteName = borderSpriteName
    nameLabel.text = name 
    qualityLabel.text = quality
    levelLabel.text = SafeStringFormat3(LocalString.GetString("%d级"),level) 
    qnCheckBox.gameObject:SetActive(self.m_IsDiscardView)
    local isHighQuality = LuaZhuoYaoMgr:CheckYaoGuaiIsHighQuality(tujiandata, quality)
    qnCheckBox.Selected = false

    if self.m_IsDiscardView then
        qnCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (selected )
            self:OnQnCheckBoxSelected(selected, Id, isHighQuality, name)
        end)
    end
end

function LuaZhuoYaoWarehouseView:OnWarehouseYaoGuaiListUpdate()
    self.m_YaoGuaiList = {}
    for _,info in pairs(LuaZhuoYaoMgr.m_WarehouseYaoGuaiList) do
        table.insert(self.m_YaoGuaiList,{Quality = info.Quality, Level = info.Level,CatchTime = info.CatchTime, TemplateId = info.TemplateId,Id = info.Id})
    end
    self:SortYaoGuaiList()
    self.m_QnTableView:ReloadData(true, true)
    self.m_QnTableView.m_ScrollView:ResetPosition()
end

function LuaZhuoYaoWarehouseView:OnQnCheckBoxSelected(selected, Id, isHighQuality, name)
    self.m_SelectIds[Id] = {selected = selected, isHighQuality = isHighQuality, name = name}
end

function LuaZhuoYaoWarehouseView:BindRepoUnlockPrice(usableSize)
    local price = ""
    local bagUnlockFormulaId = tonumber(ZhuoYao_Setting.GetData("BagUnlockFormulaId").Value)
    local price_formula = AllFormulas.Action_Formula[bagUnlockFormulaId] and AllFormulas.Action_Formula[bagUnlockFormulaId].Formula or nil
    if price_formula then
        price = price_formula(nil, nil, {usableSize + 1})
    end
    return price
end

--@region UIEvent

function LuaZhuoYaoWarehouseView:OnTipButtonClick()
    g_MessageMgr:ShowMessage("ZhuoYao_Warehouse_ReadMe")
end

function LuaZhuoYaoWarehouseView:OnSelectSortsClick(selected)
    local textArray = {LocalString.GetString("妖怪品质"),LocalString.GetString("等级"),LocalString.GetString("捕获时间")}
    Extensions.SetLocalRotationZ(self.m_SelectSortsArrowSprite.transform, selected and 180 or 0)
    if not selected then return end
    local t = {}
    for k,text in pairs(textArray) do
        local item=PopupMenuItemData(text,DelegateFactory.Action_int(function (idx)
            self.m_SelectSorts.Text = textArray[idx + 1]
            self:OnChooseSortClicked(idx + 1)
        end),false,nil)
        table.insert(t, item)
    end
    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, self.m_SelectSorts.m_Label.transform, AlignType.Bottom,1,DelegateFactory.Action(function()
        Extensions.SetLocalRotationZ(self.m_SelectSortsArrowSprite.transform, 0)
    end),600,true,296)
end

function LuaZhuoYaoWarehouseView:OnChooseSortClicked(index)
    self.m_SelectSortIndex = index
    self:SortYaoGuaiList()
    self.m_QnTableView:ReloadData(true, true)
end

function LuaZhuoYaoWarehouseView:OnLianHuaButtonClick()
    Gac2Gas.RequestTrackToSectEntrance()
end

function LuaZhuoYaoWarehouseView:OnChangeViewButtonClick()
    self.m_IsDiscardView = not self.m_IsDiscardView
    self.m_SelectIds = {}
    self.m_DefaultBottomView:SetActive(not self.m_IsDiscardView)
    self.m_DiscardBottomView:SetActive(self.m_IsDiscardView)
    self.m_ChangeViewButton.Text = self.m_IsDiscardView and LocalString.GetString("丢弃中") or LocalString.GetString("丢弃")
    self.m_QnTableView:ReloadData(true, true)
end

function LuaZhuoYaoWarehouseView:OnDiscardItemButtonClick()
    local hasHighQuality = false
    if self.m_SelectIds then
        local list = {}
        local hasHighQualityYaoGuaiName = ""
        for id, t in pairs(self.m_SelectIds) do
            if t.selected then
                table.insert(list, id)
                hasHighQuality = hasHighQuality or t.isHighQuality
                hasHighQualityYaoGuaiName = t.name
            end
        end
        if #list == 0 then
            g_MessageMgr:ShowMessage("NoneSelect_DeleteYaoGuai")
        end
        if hasHighQuality then
            local msg = g_MessageMgr:FormatMessage("Discard_High_Quality_YaoGuai", hasHighQualityYaoGuaiName)
            local okAction = DelegateFactory.Action(function ()
                local List_Object = MakeGenericClass(List, Object)
                Gac2Gas.RequestDeleteYaoGuai(MsgPackImpl.pack(Table2List(list, List_Object)))
            end)
            MessageWndManager.ShowOKCancelMessage(msg,okAction,nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
            return
        end
        
        local List_String = MakeGenericClass(List,Object)
        local cslist = Table2List(list, List_String)
        local u = MsgPackImpl.pack(cslist)
        Gac2Gas.RequestDeleteYaoGuai(u)
    end
    self.m_SelectIds = {}
end

--@endregion

