-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CHouseStuffMakeWnd = import "L10.UI.CHouseStuffMakeWnd"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CLivingMakeConsumeItem = import "L10.UI.CLivingMakeConsumeItem"
local CLivingSkillQuickMakeMgr = import "L10.UI.CLivingSkillQuickMakeMgr"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local Object = import "System.Object"
CLivingMakeConsumeItem.m_SetMakeCount_CS2LuaHook = function (this, count) 
    this.needCount = (count * this.unitCost)
    if this.needCount <= this.countUpdate.count then
        this.countUpdate.format = "{0}/" .. tostring(this.needCount)
    else
        this.countUpdate.format = "[ff0000]{0}[-]/" .. tostring(this.needCount)
    end
    -- +"/[ff0000]{0}[-]";
    this.countUpdate:Reformat()
end
CLivingMakeConsumeItem.m_Init_CS2LuaHook = function (this, templateId, needCount, cost) 

    this.needCount = needCount
    this.templateId = templateId
    this.unitCost = cost
    this.countUpdate.templateId = templateId
    this.countUpdate.onChange = MakeDelegateFromCSFunction(this.OnChange, MakeGenericClass(Action1, Int32), this)
    this.countUpdate:UpdateCount()

    local template = CItemMgr.Inst:GetItemTemplate(templateId)
    this.icon:LoadMaterial(template.Icon)

    this:StartRequestYuanbaoCost()
end
CLivingMakeConsumeItem.m_OnChange_CS2LuaHook = function (this, count) 

    if this.OnChangeAction ~= nil then
        GenericDelegateInvoke(this.OnChangeAction, Table2ArrayWithCount({this}, 1, MakeArrayClass(Object)))
    end

    if this.needCount <= count then
        this.countUpdate.format = "{0}/" .. tostring(this.needCount)
        --+ "/{0}";
        this.getNode:SetActive(false)
    else
        this.countUpdate.format = "[ff0000]{0}[-]/" .. tostring(this.needCount)
        -- +"/[ff0000]{0}[-]";
        this.getNode:SetActive(true)
    end

    this:UpdateYuanbaoCost()
end
CLivingMakeConsumeItem.m_OnClick_CS2LuaHook = function (this) 
    if this.countUpdate.count >= this.needCount then
        CItemInfoMgr.ShowLinkItemTemplateInfo(this.templateId, false, nil, AlignType.Default, 0, 0, 0, 0)
    else
        CLivingSkillQuickMakeMgr.Inst:ShowMakeTipWnd(this.templateId)
    end
end
CLivingMakeConsumeItem.m_StartRequestYuanbaoCost_CS2LuaHook = function (this) 
    --制作朱砂笔
    if CHouseStuffMakeWnd.IsMakeZhuShaBi then
        this:RequestPrice()
        this:InvokeRepeating("RequestPrice", 15, 15)
    end
end
CLivingMakeConsumeItem.m_OnPreSellDone_CS2LuaHook = function (this, itemId, maxCount, price) 
    if this.templateId == itemId then
        if this.mPrice ~= price then
            this.mPrice = price
            this:UpdateYuanbaoCost()
        end
    end
end
CLivingMakeConsumeItem.m_UpdateYuanbaoCost_CS2LuaHook = function (this) 
    if CHouseStuffMakeWnd.IsMakeZhuShaBi then
        if this.countUpdate.count < this.needCount then
            this.costYuanbao = (this.mPrice * (this.needCount - this.countUpdate.count))
        else
            this.costYuanbao = 0
        end
        EventManager.Broadcast(EnumEventType.UpdateChuanjiabaoYuanbaoCost)
    end
end
