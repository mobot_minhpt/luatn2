local UILabel = import "UILabel"
local UISprite = import "UISprite"
local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DelegateFactory  = import "DelegateFactory"

LuaHengYuDangKouSelectDifficultyBox = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHengYuDangKouSelectDifficultyBox, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaHengYuDangKouSelectDifficultyBox, "DifficultySprite", "DifficultySprite", UISprite)
RegistChildComponent(LuaHengYuDangKouSelectDifficultyBox, "TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaHengYuDangKouSelectDifficultyBox, "OKButton", "OKButton", GameObject)

--@endregion RegistChildComponent end

function LuaHengYuDangKouSelectDifficultyBox:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.OKButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOKButtonClick()
	end)


    --@endregion EventBind end
end

function LuaHengYuDangKouSelectDifficultyBox:Init()
	-- 设置难度
	self.TabBar:ChangeTab(0, false)
	self:SetCur(1)
	self.DescLabel.text = SafeStringFormat3(LocalString.GetString("每%d秒可调整一次，后续Boss将继承难度"), HengYuDangKou_Setting.GetData().ResetDifficultyCD)
	
	if LuaHengYuDangKouMgr.m_SceneInfo then
		self:SyncHengYuDangKouSceneInfo(LuaHengYuDangKouMgr.m_SceneInfo)
	end

	self.m_Tick = RegisterTick(function()
		if LuaHengYuDangKouMgr.m_SceneInfo == nil or LuaHengYuDangKouMgr.m_SceneInfo.lastResetDifficultyTs == nil then
			return
		end

		local lastResetDifficultyTs = LuaHengYuDangKouMgr.m_SceneInfo.lastResetDifficultyTs
		local diff = HengYuDangKou_Setting.GetData().ResetDifficultyCD - (CServerTimeMgr.Inst.timeStamp - lastResetDifficultyTs)
		if diff <= 0 then
			CUICommonDef.SetActive(self.OKButton.gameObject, true, true)
			self.OKButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("变更难度")
		else
			CUICommonDef.SetActive(self.OKButton.gameObject, false, true)
			self.OKButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("变更难度(%d)"), diff)
		end
	end, 1000)
end

function LuaHengYuDangKouSelectDifficultyBox:SyncHengYuDangKouSceneInfo(sceneInfo)
	-- 设置难度
	local curDifficulty = sceneInfo.curDifficulty or 1
	self.TabBar:ChangeTab(curDifficulty -1, false)
	self:SetCur(curDifficulty)
end

function LuaHengYuDangKouSelectDifficultyBox:SetCur(difficulty)
	for i = 1, 3 do
		local sp = self.transform:Find(SafeStringFormat3("Anchor/DifficultySprite/Level_%d", i)).gameObject:SetActive(difficulty == i)
	end
end

function LuaHengYuDangKouSelectDifficultyBox:OnEnable()
	g_ScriptEvent:AddListener("SyncHengYuDangKouSceneInfo", self, "SyncHengYuDangKouSceneInfo")
end

function LuaHengYuDangKouSelectDifficultyBox:OnDisable()
	UnRegisterTick(self.m_Tick)
	g_ScriptEvent:RemoveListener("SyncHengYuDangKouSceneInfo", self, "SyncHengYuDangKouSceneInfo")
end

--@region UIEvent

function LuaHengYuDangKouSelectDifficultyBox:OnOKButtonClick()
	-- 选择难度
	g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("HengYuDangKou_SetDifficultyTips", tostring(self.TabBar.selectedIndex + 1)), function()
		Gac2Gas.HengYuDangKouResetDifficulty(self.TabBar.selectedIndex + 1)
		CUIManager.CloseUI(CLuaUIResources.HengYuDangKouSelectDifficultyBox)
	end, nil, nil, nil, false)
end


--@endregion UIEvent

