local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Animation = import "UnityEngine.Animation"
local CJingLingMgr = import "L10.Game.CJingLingMgr"

LuaSnowAdventureResultWnd = class()
LuaSnowAdventureResultWnd.bResult = nil
LuaSnowAdventureResultWnd.curLevelId = nil
LuaSnowAdventureResultWnd.costTime = nil

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSnowAdventureResultWnd, "winPattern", "winPattern", GameObject)
RegistChildComponent(LuaSnowAdventureResultWnd, "losePattern", "losePattern", GameObject)
RegistChildComponent(LuaSnowAdventureResultWnd, "StageName", "StageName", UILabel)
RegistChildComponent(LuaSnowAdventureResultWnd, "ConsumeTime", "ConsumeTime", UILabel)
RegistChildComponent(LuaSnowAdventureResultWnd, "StageType", "StageType", CUITexture)

--@endregion RegistChildComponent end

function LuaSnowAdventureResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:RefreshConstUI()
    self:InitUIEvent()
end

function LuaSnowAdventureResultWnd:Init()

end

function LuaSnowAdventureResultWnd:InitUIEvent()
    self.trainSnowmanBtn = self.losePattern.transform:Find("xinxi/fail_peiyangxueren")
    self.askBtn = self.losePattern.transform:Find("xinxi/fail_liaojiejineng")
    self.closeBtn = self.transform:Find("CloseButton")

    UIEventListener.Get(self.closeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        CUIManager.CloseUI(CLuaUIResources.SnowAdventureResultWnd)
        if LuaSnowAdventureResultWnd.bResult then
            if LuaSnowAdventureResultWnd.curLevelId < 400 then
                --单人模式
                CUIManager.CloseUI(CLuaUIResources.SnowAdventureResultWnd)
                LuaSnowAdventureSingleStageMainWnd.FirstOpenStageId = LuaSnowAdventureResultWnd.curLevelId
                CUIManager.ShowUI(CLuaUIResources.SnowAdventureSingleStageMainWnd)

            else
                --多人模式
                CUIManager.CloseUI(CLuaUIResources.SnowAdventureResultWnd)
                LuaSnowAdventureMultipleStageMainWnd.FirstOpenStageId = true
                CUIManager.ShowUI(CLuaUIResources.SnowAdventureMultipleStageMainWnd)

            end
        end
    end)
    
    UIEventListener.Get(self.trainSnowmanBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        if LuaSnowAdventureResultWnd.curLevelId < 400 then
            --单人模式
            CUIManager.CloseUI(CLuaUIResources.SnowAdventureResultWnd)
            LuaSnowAdventureSingleStageMainWnd.FirstOpenStageId = LuaSnowAdventureResultWnd.curLevelId
            LuaSnowAdventureSnowManWnd.ShowCountdownPattern = false
            CUIManager.ShowUI(CLuaUIResources.SnowAdventureSnowManWnd)
        else    
            --多人模式
            CUIManager.CloseUI(CLuaUIResources.SnowAdventureResultWnd)
            LuaSnowAdventureMultipleStageMainWnd.FirstOpenStageId = true
            LuaSnowAdventureSnowManWnd.ShowCountdownPattern = false
            CUIManager.ShowUI(CLuaUIResources.SnowAdventureSnowManWnd)
        end
    end)

    UIEventListener.Get(self.askBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        CJingLingMgr.Inst:ShowJingLingWnd(LocalString.GetString("雪魄历险-雪人技能"), "o_push", true, false, nil, false)
    end)
end

function LuaSnowAdventureResultWnd:RefreshConstUI()
    self.winPattern:SetActive(LuaSnowAdventureResultWnd.bResult)
    self.losePattern:SetActive(not LuaSnowAdventureResultWnd.bResult)
    local stageType = math.floor(LuaSnowAdventureResultWnd.curLevelId/100)
    local difficultyColor = LuaHanJia2023Mgr.easyColor
    if stageType == 1 then
        --简单
        difficultyColor = LuaHanJia2023Mgr.easyColor
        self.StageType:LoadMaterial("UI/Texture/FestivalActivity/Festival_HanJia/HanJia2023/Material/snowadventuresinglestagemainwnd_right_chuji.mat")
    elseif stageType == 2 then
        --普通
        self.StageType:LoadMaterial("UI/Texture/FestivalActivity/Festival_HanJia/HanJia2023/Material/snowadventuresinglestagemainwnd_right_zhongji.mat")
    elseif stageType == 3 then
        --困难
        self.StageType:LoadMaterial("UI/Texture/FestivalActivity/Festival_HanJia/HanJia2023/Material/snowadventuresinglestagemainwnd_right_kunnan.mat")
    elseif stageType == 4 then
        --多人
        difficultyColor = LuaHanJia2023Mgr.multipleColor
        self.StageType:LoadMaterial("UI/Texture/FestivalActivity/Festival_HanJia/HanJia2023/Material/snowadventuresinglestagemainwnd_right_tongxing.mat")
    end
    self.StageType.transform:Find("Texture"):GetComponent(typeof(UITexture)).color = difficultyColor

    
    local stageConfigData = HanJia2023_XuePoLiXianLevel.GetData(LuaSnowAdventureResultWnd.curLevelId)
    self.StageName.text = stageConfigData.Title
    self.ConsumeTime.text = SafeStringFormat3(LocalString.GetString("通关耗时: %02d:%02d"),
            math.floor(LuaSnowAdventureResultWnd.costTime/60), LuaSnowAdventureResultWnd.costTime%60)

    self.wndAnimation = self.transform:GetComponent(typeof(Animation))
    if LuaSnowAdventureResultWnd.bResult then
        self.wndAnimation:Play("snowadventureresultwnd_sl")
    else
        self.wndAnimation:Play("snowadventureresultwnd_sb")
    end
end

--@region UIEvent

--@endregion UIEvent

