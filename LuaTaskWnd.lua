local UInt32 = import "System.UInt32"
local Task_Task = import "L10.Game.Task_Task"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CTaskInfoMgr = import "L10.UI.CTaskInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local UITabBar = import "L10.UI.UITabBar"
local CTaskMasterView = import "L10.UI.CTaskMasterView"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local DelegateFactory = import "DelegateFactory"
local CBaseWnd = import "L10.UI.CBaseWnd"


CLuaTaskWnd = class()
RegistClassMember(CLuaTaskWnd,"tabController")
RegistClassMember(CLuaTaskWnd,"closeButton")
RegistClassMember(CLuaTaskWnd,"masterView")
RegistClassMember(CLuaTaskWnd,"detailView")
RegistClassMember(CLuaTaskWnd,"m_DefaultSelectedTaskId")

-- Auto Generated!!
function CLuaTaskWnd:Init( )

    self.tabController = self.transform:GetComponent(typeof(UITabBar))
    self.closeButton = self.transform:Find("Wnd_Bg_Primary_Tab/CloseButton").gameObject
    self.masterView = self.transform:Find("Anchor/MasterView"):GetComponent(typeof(CTaskMasterView))
    self.detailView = self.transform:Find("Anchor/DetailView"):GetComponent(typeof(CCommonLuaScript))
    self.m_DefaultSelectedTaskId = 0

    CommonDefs.AddOnClickListener(self.closeButton, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

    self.masterView.OnTaskItemSelected = DelegateFactory.Action_uint(function (templateId)
        self:OnTaskItemSelected(templateId)
    end)
    self.tabController.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnTabChange(go, index)
    end)

    self.m_DefaultSelectedTaskId = CTaskInfoMgr.SelectedTaskId

    if CTaskInfoMgr.TabIndex >= 0 and CTaskInfoMgr.TabIndex <= 1 then
        self.tabController:ChangeTab(CTaskInfoMgr.TabIndex, false)
        CTaskInfoMgr.TabIndex = - 1
        return
    end

    local existFailedTask = false
    if CClientMainPlayer.Inst ~= nil then
        local current = CClientMainPlayer.Inst.TaskProp.CurrentTaskList
        do
            local i = 0
            while i < current.Count do
                local task = CommonDefs.DictGetValue(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), current[i])
                if task.IsFailed then
                    existFailedTask = true
                    self.tabController:ChangeTab(0, false)
                    break
                end
                i = i + 1
            end
        end
    end
    if not existFailedTask then
        if CTaskMgr.Inst.HasNewAcceptableTask then
            self.tabController:ChangeTab(1, false)
        else
            self.tabController:ChangeTab(0, false)
        end
    end
end

function CLuaTaskWnd:Close()
    self.masterView = nil
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function CLuaTaskWnd:OnTabChange( go, index) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    if index == 0 then
        local selectedTaskId = self.m_DefaultSelectedTaskId
        if CClientMainPlayer.Inst ~= nil then
            local taskProp = CClientMainPlayer.Inst.TaskProp
            if CommonDefs.ListContains(taskProp.CurrentTaskList, typeof(UInt32), selectedTaskId) then
                self.masterView:LoadTaskList(true, selectedTaskId)
            else
                do
                    local i = 0
                    while i < taskProp.CurrentTaskList.Count do
                        local task = CommonDefs.DictGetValue(taskProp.CurrentTasks, typeof(UInt32), taskProp.CurrentTaskList[i])
                        if task.IsFailed then
                            selectedTaskId = task.TemplateId
                            break
                        end
                        i = i + 1
                    end
                end
            end
        end
        self.masterView:LoadTaskList(true, selectedTaskId)
    elseif index == 1 then
        self.masterView:LoadTaskList(false, self.m_DefaultSelectedTaskId)
    end
    self.m_DefaultSelectedTaskId = 0
    --默认值使用一次就重置
end

function CLuaTaskWnd:OnTaskItemSelected(templateId)
    self.detailView.m_LuaSelf:Init(templateId)
end

CTaskInfoMgr.m_ShowReceivedZhuXianTask_CS2LuaHook = function () 
    CTaskInfoMgr.Inst.selectedTaskId = 0
    if CClientMainPlayer.Inst ~= nil then
        local tasks = CClientMainPlayer.Inst.TaskProp.CurrentTaskList
        do
            local i = 0
            while i < tasks.Count do
                local template = Task_Task.GetData(tasks[i])
                if template ~= nil and template.DisplayNode == 1 then
                    CTaskInfoMgr.Inst.selectedTaskId = template.ID
                    break
                end
                i = i + 1
            end
        end
    end
    CTaskInfoMgr.ShowTaskWnd(CTaskInfoMgr.Inst.selectedTaskId, 0)
end
CTaskInfoMgr.m_ShowAcceptableZhiXianTask_CS2LuaHook = function () 
    CTaskInfoMgr.Inst.selectedTaskId = 0
    if CClientMainPlayer.Inst ~= nil then
        local tasks = CClientMainPlayer.Inst.TaskProp.AcceptableTaskList
        do
            local i = 0
            while i < tasks.Count do
                local template = Task_Task.GetData(tasks[i])
                if template ~= nil and template.DisplayNode == 3 then
                    CTaskInfoMgr.Inst.selectedTaskId = template.ID
                    break
                end
                i = i + 1
            end
        end
    end
    CTaskInfoMgr.ShowTaskWnd(CTaskInfoMgr.Inst.selectedTaskId, 1)
end

