-- Auto Generated!!
local CGCSituationGuildDetailView = import "L10.UI.CGCSituationGuildDetailView"
local CGCSituationPlayerTemplate = import "L10.UI.CGCSituationPlayerTemplate"
local CGuildChallengeMgr = import "L10.Game.CGuildChallengeMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGCSituationGuildDetailView.m_Init_CS2LuaHook = function (this, index) 
    this.guildIndex = index
    if index == 1 then
        this.guildName.text = CGuildChallengeMgr.Inst.guildName1
        this.totalPoint.text = tostring((CGuildChallengeMgr.Inst.killScore1 + CGuildChallengeMgr.Inst.occupyScore1))
        this.killScore.text = tostring(CGuildChallengeMgr.Inst.killScore1)
        this.occupyScore.text = tostring(CGuildChallengeMgr.Inst.occupyScore1)
        this.occupyCount.text = System.String.Format(LocalString.GetString("{0}个"), CGuildChallengeMgr.Inst.occupyCount1)
    else
        this.guildName.text = CGuildChallengeMgr.Inst.guildName2
        this.totalPoint.text = tostring((CGuildChallengeMgr.Inst.killScore2 + CGuildChallengeMgr.Inst.occupyScore2))
        this.killScore.text = tostring(CGuildChallengeMgr.Inst.killScore2)
        this.occupyScore.text = tostring(CGuildChallengeMgr.Inst.occupyScore2)
        this.occupyCount.text = System.String.Format(LocalString.GetString("{0}个"), CGuildChallengeMgr.Inst.occupyCount2)
    end

    this:InitArrow()

    this.playerList.m_DataSource = this
    this.playerList:ReloadData(true, true)
end
CGCSituationGuildDetailView.m_InitArrow_CS2LuaHook = function (this) 
    this.levelArrow.enabled = false
    this.killNumArrow.enabled = false
    this.killedNumArrow.enabled = false
    this.killPointArrow.enabled = false
end
CGCSituationGuildDetailView.m_NumberOfRows_CS2LuaHook = function (this, view) 
    if this.guildIndex == 1 then
        return CGuildChallengeMgr.Inst.playerList1.Count
    else
        return CGuildChallengeMgr.Inst.playerList2.Count
    end
end
CGCSituationGuildDetailView.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local item = TypeAs(this.playerList:GetFromPool(0), typeof(CGCSituationPlayerTemplate))
    if this.guildIndex == 1 then
        item:UpdateData(CGuildChallengeMgr.Inst.playerList1[row], row, true)
    else
        item:UpdateData(CGuildChallengeMgr.Inst.playerList2[row], row, false)
    end
    return item
end
CGCSituationGuildDetailView.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.levelBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.levelBtn).onClick, MakeDelegateFromCSFunction(this.OnLevelButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.killNumBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.killNumBtn).onClick, MakeDelegateFromCSFunction(this.OnKillNumBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.killedNumBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.killedNumBtn).onClick, MakeDelegateFromCSFunction(this.OnKilledNumBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.killPointBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.killPointBtn).onClick, MakeDelegateFromCSFunction(this.OnKillPointBtnClick, VoidDelegate, this), true)
end
CGCSituationGuildDetailView.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.levelBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.levelBtn).onClick, MakeDelegateFromCSFunction(this.OnLevelButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.killNumBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.killNumBtn).onClick, MakeDelegateFromCSFunction(this.OnKillNumBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.killedNumBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.killedNumBtn).onClick, MakeDelegateFromCSFunction(this.OnKilledNumBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.killPointBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.killPointBtn).onClick, MakeDelegateFromCSFunction(this.OnKillPointBtnClick, VoidDelegate, this), false)
end
CGCSituationGuildDetailView.m_OnLevelButtonClick_CS2LuaHook = function (this, go) 
    this.killNumArrow.enabled = false
    this.killedNumArrow.enabled = false
    this.killPointArrow.enabled = false
    if this.levelArrow.enabled then
        Extensions.SetLocalRotationZ(this.levelArrow.transform, this.levelArrow.transform.localRotation.z == 0 and 180 or 0)
    else
        this.levelArrow.enabled = true
    end
    if this.guildIndex == 1 then
        CGuildChallengeMgr.Inst:SortPlayerListByLevel(this.levelArrow.transform.localRotation.z == 0, CGuildChallengeMgr.Inst.playerList1)
    else
        CGuildChallengeMgr.Inst:SortPlayerListByLevel(this.levelArrow.transform.localRotation.z == 0, CGuildChallengeMgr.Inst.playerList2)
    end
    this.playerList:ReloadData(true, true)
end
CGCSituationGuildDetailView.m_OnKillNumBtnClick_CS2LuaHook = function (this, go) 
    this.levelArrow.enabled = false
    this.killedNumArrow.enabled = false
    this.killPointArrow.enabled = false
    if this.killNumArrow.enabled then
        Extensions.SetLocalRotationZ(this.killNumArrow.transform, this.killNumArrow.transform.localRotation.z == 0 and 180 or 0)
    else
        this.killNumArrow.enabled = true
    end
    if this.guildIndex == 1 then
        CGuildChallengeMgr.Inst:SortPlayerListByKillNum(this.killNumArrow.transform.localRotation.z == 0, CGuildChallengeMgr.Inst.playerList1)
    else
        CGuildChallengeMgr.Inst:SortPlayerListByKillNum(this.killNumArrow.transform.localRotation.z == 0, CGuildChallengeMgr.Inst.playerList2)
    end
    this.playerList:ReloadData(true, true)
end
CGCSituationGuildDetailView.m_OnKilledNumBtnClick_CS2LuaHook = function (this, go) 
    this.levelArrow.enabled = false
    this.killNumArrow.enabled = false
    this.killPointArrow.enabled = false
    if this.killedNumArrow.enabled then
        Extensions.SetLocalRotationZ(this.killedNumArrow.transform, this.killedNumArrow.transform.localRotation.z == 0 and 180 or 0)
    else
        this.killedNumArrow.enabled = true
    end
    if this.guildIndex == 1 then
        CGuildChallengeMgr.Inst:SortPlayerListByKilledNum(this.killedNumArrow.transform.localRotation.z == 0, CGuildChallengeMgr.Inst.playerList1)
    else
        CGuildChallengeMgr.Inst:SortPlayerListByKilledNum(this.killedNumArrow.transform.localRotation.z == 0, CGuildChallengeMgr.Inst.playerList2)
    end
    this.playerList:ReloadData(true, true)
end
CGCSituationGuildDetailView.m_OnKillPointBtnClick_CS2LuaHook = function (this, go) 
    this.levelArrow.enabled = false
    this.killNumArrow.enabled = false
    this.killedNumArrow.enabled = false
    if this.killPointArrow.enabled then
        Extensions.SetLocalRotationZ(this.killPointArrow.transform, this.killPointArrow.transform.localRotation.z == 0 and 180 or 0)
    else
        this.killPointArrow.enabled = true
    end
    if this.guildIndex == 1 then
        CGuildChallengeMgr.Inst:SortPlayerListByKillScore(this.killPointArrow.transform.localRotation.z == 0, CGuildChallengeMgr.Inst.playerList1)
    else
        CGuildChallengeMgr.Inst:SortPlayerListByKillScore(this.killPointArrow.transform.localRotation.z == 0, CGuildChallengeMgr.Inst.playerList2)
    end
    this.playerList:ReloadData(true, true)
end
