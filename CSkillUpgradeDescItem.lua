-- Auto Generated!!
local CSkillUpgradeDescItem = import "L10.UI.CSkillUpgradeDescItem"
local LocalString = import "LocalString"
local NGUIText = import "NGUIText"
CSkillUpgradeDescItem.m_Init_CS2LuaHook = function (this, desc, preSkill, needLevel) 
    this.descLabel.text = System.String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor24(this.descLabel.color), desc)
    if this.preSkillLabel ~= nil then
        this.preSkillLabel.text = preSkill
    end
    if this.needLevelLabel ~= nil then
        this.needLevelLabel.text = needLevel
    end
    if this.nextLevelLabel ~= nil then
        this.nextLevelLabel.text = LocalString.GetString("下一级")
    end
end
CSkillUpgradeDescItem.m_Init_V2_CS2LuaHook = function (this, level, desc, preSkill, needLevel) 
    this.descLabel.text = System.String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor24(this.descLabel.color), desc)
    if this.preSkillLabel ~= nil then
        this.preSkillLabel.text = preSkill
    end
    if this.needLevelLabel ~= nil then
        this.needLevelLabel.text = needLevel
    end
    if this.nextLevelLabel ~= nil then
        if level > 0 then
            this.nextLevelLabel.text = System.String.Format(LocalString.GetString("下一级({0}级)"), level)
        else
            this.nextLevelLabel.text = LocalString.GetString("下一级")
        end
    end
end
