local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UISprite = import "UISprite"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CUITexture = import "L10.UI.CUITexture"
local CPaintTextureDLN = import "L10.UI.CPaintTextureDLN"

LuaPaintPicWnd = class()
RegistChildComponent(LuaPaintPicWnd,"closeBtn", GameObject)
RegistChildComponent(LuaPaintPicWnd,"bgTexture", CUITexture)
RegistChildComponent(LuaPaintPicWnd,"paintNode", GameObject)
RegistChildComponent(LuaPaintPicWnd,"upTexture", CUITexture)

RegistClassMember(LuaPaintPicWnd, "taskID")
RegistClassMember(LuaPaintPicWnd, "taskData")

function LuaPaintPicWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.PaintPicWnd)
end

function LuaPaintPicWnd:Finish()
	Gac2Gas.FinishTaskPainting(self.taskID)
	RegisterTickWithDuration(function ()
		self:Close()
	end,2000,2000)
end

function LuaPaintPicWnd:SetStateStart(state)
	if self.taskID then
		local count = ZhuJueJuQing_Painting.GetDataCount()
		for i=1,count do
			local data = ZhuJueJuQing_Painting.GetData(i)
			if data and data.TaskID == self.taskID then
				self.taskData = data
				break
			end
		end
	end

	if self.taskData then
		--self.bgTexture:LoadMaterial("UI/Texture/Transparent/Material/" .. self.taskData.BackgroundText .. ".mat",false)
		--self.upTexture:LoadTexture("UI/Texture/Transparent/" .. self.taskData.CoverText .. ".png",false)
		self.bgTexture.gameObject:SetActive(false)
		local paintTexture = self.upTexture.transform.parent:GetComponent(typeof(CPaintTextureDLN))
		--self.upTexture.gameObject:SetActive(false)
		RegisterTickWithDuration(function ()
			self.bgTexture.gameObject:SetActive(true)
			--self.upTexture.gameObject:SetActive(true)
			if paintTexture then
				paintTexture:Awake()
			end
		end,400,400)
	end
end

function LuaPaintPicWnd:SetStateEnd(stateIndex)
  self:Finish()
end

function LuaPaintPicWnd:SetPercent(percent)
	if self.ProgressNodeTable[LuaShenbingMgr.DLNState] and percent[0] then
		self.ProgressNodeTable[LuaShenbingMgr.DLNState].transform:Find("bg"):GetComponent(typeof(UISprite)).fillAmount = percent[0]
	end
end

function LuaPaintPicWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateDLNDraw", self, "SetStateEnd")
	--g_ScriptEvent:AddListener("UpdateDLNState", self, "SetPercent")
end

function LuaPaintPicWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateDLNDraw", self, "SetStateEnd")
	--g_ScriptEvent:RemoveListener("UpdateDLNState", self, "SetPercent")
end

function LuaPaintPicWnd:Init()
	local onCloseClick = function(go)
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Paint_Not_Done"), DelegateFactory.Action(function ()
      self:Close()
    end), nil, nil, nil, false)
	end

	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	self.taskID = tonumber(LuaPaintPicMgr.ShowArgs)
	self:SetStateStart(1)
end

return LuaPaintPicWnd
