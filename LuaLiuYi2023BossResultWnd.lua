local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UIGrid = import "UIGrid"
local QnButton = import "L10.UI.QnButton"
local CButton = import "L10.UI.CButton"
local Extensions = import "Extensions"
local MessageMgr = import "L10.Game.MessageMgr"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CUITexture = import "L10.UI.CUITexture"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemMgr = import "L10.Game.CItemMgr"
local Animation = import "UnityEngine.Animation"

LuaLiuYi2023BossResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaLiuYi2023BossResultWnd, "WinStyle", "WinStyle", GameObject)
RegistChildComponent(LuaLiuYi2023BossResultWnd, "LoseStyle", "LoseStyle", GameObject)
RegistChildComponent(LuaLiuYi2023BossResultWnd, "TimeCostLab", "TimeCostLab", UILabel)
RegistChildComponent(LuaLiuYi2023BossResultWnd, "HpLab", "HpLab", UILabel)
RegistChildComponent(LuaLiuYi2023BossResultWnd, "RewardTip", "RewardTip", GameObject)
RegistChildComponent(LuaLiuYi2023BossResultWnd, "RewardsGrid", "RewardsGrid", UIGrid)
RegistChildComponent(LuaLiuYi2023BossResultWnd, "Reward1", "Reward1", QnButton)
RegistChildComponent(LuaLiuYi2023BossResultWnd, "Reward2", "Reward2", QnButton)
RegistChildComponent(LuaLiuYi2023BossResultWnd, "LoseTipLab", "LoseTipLab", UILabel)
RegistChildComponent(LuaLiuYi2023BossResultWnd, "PlayAgainBtn", "PlayAgainBtn", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaLiuYi2023BossResultWnd, "m_Animation")

function LuaLiuYi2023BossResultWnd:Awake()
    self.m_Animation = self.transform:GetComponent(typeof(Animation))
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    UIEventListener.Get(self.PlayAgainBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go) 
        CUIManager.CloseUI(CLuaUIResources.LiuYi2023BossResultWnd)
        CUIManager.ShowUI(CLuaUIResources.LiuYi2023BossSignWnd)
    end)
end

function LuaLiuYi2023BossResultWnd:Init()
    local isWin = LuaLiuYi2023Mgr.m_BossIsSuccess
    local costTime = LuaLiuYi2023Mgr.m_BossUseTime or 0
    local percent = LuaLiuYi2023Mgr.m_BossPassNpcHp
    local rewards = LuaLiuYi2023Mgr.m_BossRewards

    if isWin then
        self.TimeCostLab.text = self:GetRemainTimeText(costTime)
        self.HpLab.text = SafeStringFormat3("%.f%%", percent)
        self.Reward1.gameObject:SetActive(#rewards >= 1)
        if #rewards >= 1 then
            self:InitReward(self.Reward1, rewards[1])
        end
        self.Reward2.gameObject:SetActive(#rewards >= 2)
        
        if #rewards >= 2 then
            self:InitReward(self.Reward2, rewards[2])
        end
        self.RewardsGrid:Reposition()
        self.RewardTip.gameObject:SetActive(#rewards == 0)
    end
    -- self.WinStyle:SetActive(isWin)
    -- self.LoseStyle:SetActive(not isWin)
    if isWin then
        self.m_Animation:Play("common_result_win")
    else
        self.m_Animation:Play("common_result_lose")
    end
end

--@region UIEvent

--@endregion UIEvent

function LuaLiuYi2023BossResultWnd:InitReward(rewardBtn, itemTemplateId)
    local itemTex   = rewardBtn:GetComponent(typeof(CUITexture))
    
    local item = CItemMgr.Inst:GetItemTemplate(itemTemplateId)
    itemTex:LoadMaterial(item.Icon)
    rewardBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemTemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)
end

function LuaLiuYi2023BossResultWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds <= 1 then
        return ""
    end
    local time
    if totalSeconds >= 3600 then
        time = SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        time = SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
    return time
end

function LuaLiuYi2023BossResultWnd:OnDestroy()
    LuaLiuYi2023Mgr.m_BossRewards = {}
    LuaLiuYi2023Mgr.m_ImagePaths = nil
    LuaLiuYi2023Mgr.m_Msgs = nil
end