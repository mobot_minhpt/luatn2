require("common/common_include")

local CButton = import "L10.UI.CButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CBaseWnd = import "L10.UI.CBaseWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Extensions = import "Extensions"
local UILabel = import "UILabel"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local CTooltip=import "L10.UI.CTooltip"

LuaLiangHaoCustomizeWnd = class()

RegistClassMember(LuaLiangHaoCustomizeWnd, "m_TitleLabel")
RegistClassMember(LuaLiangHaoCustomizeWnd, "m_InputLabel")
RegistClassMember(LuaLiangHaoCustomizeWnd, "m_RangeLabel")
RegistClassMember(LuaLiangHaoCustomizeWnd, "m_DigitTable")
RegistClassMember(LuaLiangHaoCustomizeWnd, "m_DigitTemplate")
RegistClassMember(LuaLiangHaoCustomizeWnd, "m_CostPriceLabel")

RegistClassMember(LuaLiangHaoCustomizeWnd, "m_PriceButton")
RegistClassMember(LuaLiangHaoCustomizeWnd, "m_PurchaseButton")

RegistClassMember(LuaLiangHaoCustomizeWnd, "m_LiangHaoId")
RegistClassMember(LuaLiangHaoCustomizeWnd, "m_LastLiangHaoId")
RegistClassMember(LuaLiangHaoCustomizeWnd, "m_LastLiangHaoPrice")

function LuaLiangHaoCustomizeWnd:Init()
	self.m_TitleLabel = self.transform:Find("Wnd_Bg_Secondary_2/TitleLabel"):GetComponent(typeof(UILabel))
	self.m_InputLabel = self.transform:Find("Anchor/InputLabel"):GetComponent(typeof(UILabel))
	self.m_RangeLabel = self.transform:Find("Anchor/RangeLabel"):GetComponent(typeof(UILabel))

	self.m_DigitTemplate = self.transform:Find("Anchor/LabelTemplate").gameObject
	self.m_DigitTable = self.transform:Find("Anchor/Table"):GetComponent(typeof(UITable))

	self.m_CostPriceLabel = self.transform:Find("Anchor/Cost/Value"):GetComponent(typeof(UILabel))

	self.m_PriceButton = self.transform:Find("Anchor/PriceButton").gameObject
	self.m_PurchaseButton = self.transform:Find("Anchor/PurchaseButton"):GetComponent(typeof(CButton))
	
	CommonDefs.AddOnClickListener(self.m_PriceButton, DelegateFactory.Action_GameObject(function(go) self:OnPriceButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_PurchaseButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnPurchaseButtonClick() end), false)

	self.m_DigitTemplate:SetActive(false)
	self:InitContent()
end

function LuaLiangHaoCustomizeWnd:InitContent()
	self.m_TitleLabel.text = SafeStringFormat3(LocalString.GetString("定制%d位靓号"),LuaLiangHaoMgr.DingZhiItemInfo.digitnum)
	self.m_InputLabel.text = SafeStringFormat3(LocalString.GetString("点击输入%d位靓号"),LuaLiangHaoMgr.DingZhiItemInfo.digitnum)
	self.m_RangeLabel.text = SafeStringFormat3(LocalString.GetString("%d<=靓号数字<=%d"),LuaLiangHaoMgr.DingZhiItemInfo.minVal, LuaLiangHaoMgr.DingZhiItemInfo.maxVal)
	self.m_CostPriceLabel.text = LocalString.GetString("尚未计算")

	self.m_LiangHaoId = nil
	self.m_LastLiangHaoId = nil
	self.m_LastLiangHaoPrice = nil
	self.m_PurchaseButton.Enabled = false
	Extensions.RemoveAllChildren(self.m_DigitTable.transform)
    for i=1,LuaLiangHaoMgr.DingZhiItemInfo.digitnum do
    	local go = CUICommonDef.AddChild(self.m_DigitTable.gameObject, self.m_DigitTemplate)
    	go:SetActive(true)
    	go:GetComponent(typeof(UILabel)).text = ""
    	CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function(go) self:OnLiangHaoDigitClick() end), false)
    end
    self.m_DigitTable:Reposition()

end

function LuaLiangHaoCustomizeWnd:OnEnable()
	g_ScriptEvent:AddListener("ReplyCalcLiangHaoPriceResult", self, "ReplyCalcLiangHaoPriceResult")
end

function LuaLiangHaoCustomizeWnd:OnDisable()
	g_ScriptEvent:RemoveListener("ReplyCalcLiangHaoPriceResult", self, "ReplyCalcLiangHaoPriceResult")
end

function LuaLiangHaoCustomizeWnd:ReplyCalcLiangHaoPriceResult(lianghaoId, price, context)
	if context == "lianghaodingzhi" and self.m_LiangHaoId == lianghaoId then
		self.m_LastLiangHaoId = lianghaoId
		self.m_LastLiangHaoPrice = price

		if price>0 then
			self.m_CostPriceLabel.text = tostring(price)
			self.m_PurchaseButton.Enabled = true
		else
			self.m_CostPriceLabel.text = LocalString.GetString("尚未计算")
			self.m_PurchaseButton.Enabled = false
		end
	end
end

function LuaLiangHaoCustomizeWnd:ShowLiangHao(id)
	self.m_LiangHaoId = id
	if not id then return end
	local numberStr = id > 0 and tostring(id) or ""

	local childCount = self.m_DigitTable.transform.childCount
	for i=0,childCount-1 do
		local child = self.m_DigitTable.transform:GetChild(i)
		if i<#numberStr then
			child:GetComponent(typeof(UILabel)).text = string.sub(numberStr,i+1,i+1)
		else
			child:GetComponent(typeof(UILabel)).text = ""
		end
	end
	if self.m_LiangHaoId and self.m_LiangHaoId == self.m_LastLiangHaoId and self.m_LastLiangHaoPrice>0 then
		self.m_CostPriceLabel.text = tostring(self.m_LastLiangHaoPrice)
		self.m_PurchaseButton.Enabled = true
	else
		self.m_CostPriceLabel.text = LocalString.GetString("尚未计算")
		self.m_PurchaseButton.Enabled = false
	end
end


function LuaLiangHaoCustomizeWnd:OnPriceButtonClick()
	if self.m_LiangHaoId and self.m_LiangHaoId >= LuaLiangHaoMgr.DingZhiItemInfo.minVal and self.m_LiangHaoId <= LuaLiangHaoMgr.DingZhiItemInfo.maxVal then
		LuaLiangHaoMgr.RequestCalcLiangHaoPrice(self.m_LiangHaoId, "lianghaodingzhi")
	else
		g_MessageMgr:ShowMessage("Lianghao_PriceCalc_Error_Format")
	end
end

function LuaLiangHaoCustomizeWnd:OnPurchaseButtonClick()
	if self.m_LiangHaoId and self.m_LiangHaoId == self.m_LastLiangHaoId then 
		MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Lianghao_Buy_Confirm",self.m_LastLiangHaoPrice, self.m_LiangHaoId), 
			DelegateFactory.Action(function() 
				LuaLiangHaoMgr.RequestBuyDingZhiLiangHao(self.m_LiangHaoId, self.m_LastLiangHaoPrice)
			end),nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
	end
end

function LuaLiangHaoCustomizeWnd:OnLiangHaoDigitClick()
	local childCount = self.m_DigitTable.transform.childCount
	local rightLabel = self.m_DigitTable.transform:GetChild(childCount-1):GetComponent(typeof(UILabel))
	CNumberKeyBoardMgr.ShowNumberKeyBoardWithLength(0, LuaLiangHaoMgr.DingZhiItemInfo.maxVal, self.m_LiangHaoId or 0, DelegateFactory.Action_int(function (val)
        self:ShowLiangHao(val)
    end),
    DelegateFactory.Action_int(function (val)
        self:ShowLiangHao(val)
    end), rightLabel, CTooltip.AlignType.Bottom, true)
end

function LuaLiangHaoCustomizeWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end
