-- Auto Generated!!
local AlignType = import "CPlayerInfoMgr+AlignType"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CGuildWelfareModel = import "L10.UI.CGuildWelfareModel"
local CGuildWelfareWnd = import "L10.UI.CGuildWelfareWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local Int32 = import "System.Int32"
local MessageWndManager = import "L10.UI.MessageWndManager"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local UInt64 = import "System.UInt64"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildWelfareModel.m_SetMemberWelfareLabel_CS2LuaHook = function (guildMemberInfoRpc, playerId, memberInfo, title, zlIdx) 

    local idx = 0

    if not CommonDefs.DictContains(CGuildWelfareModel.Office2Idx, typeof(Int32), title) then
        return zlIdx
    end

    if title == 3 then
        idx = zlIdx
        zlIdx = zlIdx + 1
    else
        idx = CommonDefs.DictGetValue(CGuildWelfareModel.Office2Idx, typeof(Int32), title)
    end

    CGuildWelfareModel.AllocedGongXuns[idx] = memberInfo.MiscData.AllocExtraGongXunWeek
    local memberDynamicInfo = CommonDefs.DictGetValue(guildMemberInfoRpc.DynamicInfos, typeof(UInt64), playerId)
    local name = ""
    local id = 0
    if memberDynamicInfo ~= nil then
        name = memberDynamicInfo.Name
        id = memberDynamicInfo.PlayerId
    end
    CGuildWelfareModel.Names[idx] = name
    CGuildWelfareModel.ids[idx] = id
    return zlIdx
end
CGuildWelfareModel.m_SetWelfareLabel_CS2LuaHook = function (guildMemberInfoRpc) 

    CGuildWelfareModel.Names = Table2ArrayWithCount({
        "", 
        "", 
        "", 
        "", 
        "", 
        "", 
        "", 
        "", 
        "", 
        "", 
        "", 
        "", 
        "", 
        "", 
        ""
    }, 15, MakeArrayClass(System.String))
    CGuildWelfareModel.ids = Table2ArrayWithCount({
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0
    }, 15, MakeArrayClass(System.UInt64))
    if guildMemberInfoRpc == nil then
        return
    end

    local zlIdx = CommonDefs.DictGetValue(CGuildWelfareModel.Office2Idx, typeof(Int32), 3)
    CommonDefs.DictIterate(guildMemberInfoRpc.Infos, DelegateFactory.Action_object_object(function (___key, ___value) 
        local entry = {}
        entry.Key = ___key
        entry.Value = ___value
        local playerId = entry.Key
        local memberInfo = entry.Value
        local title = memberInfo.Title
        zlIdx = CGuildWelfareModel.SetMemberWelfareLabel(guildMemberInfoRpc, playerId, memberInfo, title, zlIdx)
        if memberInfo.MiscData.IsNeiWuFuOfficer == 1 then
            zlIdx = CGuildWelfareModel.SetMemberWelfareLabel(guildMemberInfoRpc, playerId, memberInfo, - 1, zlIdx)
        end
    end))

    CGuildWelfareModel.RemainGongXun = CGuildMgr.Inst.m_GuildInfo.MiscData.ExtraGongXun
    CGuildWelfareModel.WarningAllocGongXun = math.floor(math.ceil(CGuildWelfareModel.RemainGongXun * 0.6))
end

CGuildWelfareWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeBtn).onClick = MakeDelegateFromCSFunction(this.OnClickCloseBtn, VoidDelegate, this)
    UIEventListener.Get(this.ApplyButton.gameObject).onClick = MakeDelegateFromCSFunction(this.OnClickApplyButton, VoidDelegate, this)
    UIEventListener.Get(this.RandomButton.gameObject).onClick = MakeDelegateFromCSFunction(this.OnClickRandomButton, VoidDelegate, this)
    UIEventListener.Get(this.moreInfo).onClick = MakeDelegateFromCSFunction(this.OnClickMoreInfo, VoidDelegate, this)
    do
        local i = 0
        while i < this.rects.Length do
            UIEventListener.Get(this.rects[i]).onClick = MakeDelegateFromCSFunction(this.OnClickItem, VoidDelegate, this)
            i = i + 1
        end
    end
end
CGuildWelfareWnd.m_Init_CS2LuaHook = function (this) 
    CGuildWelfareModel.RandomForm = false
    CGuildWelfareModel.SetWelfareLabel(CGuildMgr.Inst.m_GuildMemberInfo)
    this:InitNames()
    do
        local i = 0
        while i < CGuildWelfareWnd.GUILD_RANK_COUNT do
            this.inputs[i]:SetButtonType(QnAddSubAndInputButton.ButtonType.AllowInputAndButtons)
            local idx = i
            this.inputs[i].onValueChanged = DelegateFactory.Action_uint(function (value) 
                this:OnGongXunAllcTryChange(idx, value)
            end)
            i = i + 1
        end
    end
    this:UpdateGongXuns()
    Gac2Gas.GetAllocExtraGongXunStatus()
end
CGuildWelfareWnd.m_UpdateGongXuns_CS2LuaHook = function (this) 

    this.GongXunsLeft.text = tostring(CGuildWelfareModel.RemainGongXun)
    do
        local i = 0
        while i < CGuildWelfareWnd.GUILD_RANK_COUNT do
            this.inputs[i]:SetValue(CGuildWelfareModel.AllocedGongXuns[i], true)
            i = i + 1
        end
    end
end
CGuildWelfareWnd.m_UpdateButtonStatus_CS2LuaHook = function (this) 
    local canAlloc = CGuildWelfareModel.CanAlloc
    this.ApplyButton.Enabled = canAlloc
    this.RandomButton.Enabled = canAlloc
end
CGuildWelfareWnd.m_UpdateInputStatus_CS2LuaHook = function (this) 

    if CGuildWelfareModel.CanAlloc then
        local tryToAlloc = 0
        local allocMap = CreateFromClass(MakeGenericClass(Dictionary, UInt64, UInt32))
        do
            local i = 0
            while i < CGuildWelfareWnd.GUILD_RANK_COUNT do
                CommonDefs.DictSet(allocMap, typeof(UInt64), CGuildWelfareModel.ids[i], typeof(UInt32), this.inputs[i]:GetValue())
                i = i + 1
            end
        end
        CommonDefs.DictIterate(allocMap, DelegateFactory.Action_object_object(function (___key, ___value) 
            local pair = {}
            pair.Key = ___key
            pair.Value = ___value
            tryToAlloc = tryToAlloc + pair.Value
        end))

        local tryLeft = 0
        if CGuildWelfareModel.RemainGongXun - tryToAlloc > 0 then
            tryLeft = CGuildWelfareModel.RemainGongXun - tryToAlloc
        end

        this.GongXunsLeft.text = tostring(tryLeft)

        do
            local i = 0
            while i < CGuildWelfareWnd.GUILD_RANK_COUNT do
                if System.String.IsNullOrEmpty(CGuildWelfareModel.Names[i]) then
                    this.inputs[i]:EnableButtons(false)
                    this.inputs[i]:SetInputEnabled(false)
                else
                    this.inputs[i]:EnableButtons(true)
                    this.inputs[i]:SetInputEnabled(true)
                    local maxValue = math.min(this.inputs[i]:GetValue() + tryLeft, CGuildWelfareModel.WarningAllocGongXun)
                    this.inputs[i]:SetMinMax(0, maxValue, 1)

                    if CGuildWelfareModel.RandomForm then
                        this.inputs[i]:EnableButtons(false)
                        this.inputs[i]:SetInputEnabled(false)
                    end
                end
                i = i + 1
            end
        end
    else
        do
            local i = 0
            while i < CGuildWelfareWnd.GUILD_RANK_COUNT do
                this.inputs[i]:EnableButtons(false)
                i = i + 1
            end
        end
    end
end
CGuildWelfareWnd.m_ClearInputStatus_CS2LuaHook = function (this) 

    do
        local i = 0
        while i < CGuildWelfareWnd.GUILD_RANK_COUNT do
            this.inputs[i]:SetValue(0, true)
            this.inputs[i]:SetMinMax(0, CGuildWelfareWnd.GUILD_MAX_GONGXUN_ALLOC, 1)
            i = i + 1
        end
    end
end
CGuildWelfareWnd.m_OnGongXunAllcTryChange_CS2LuaHook = function (this, idx, value) 
    if CGuildWelfareModel.CanAlloc and not CGuildWelfareModel.RandomForm then
        if CGuildWelfareModel.WarningAllocGongXun ~= 0 and value >= CGuildWelfareModel.WarningAllocGongXun then
            local message = g_MessageMgr:FormatMessage("Allocate_Gongxun_Beyond_Limit")
            MessageWndManager.ShowOKMessage(message, nil)
        end
    end

    -- 因为内务府职位可以兼任，所以如果要保证可能存在的两个同一玩家的按钮状态的一致
    do
        local i = 0
        while i < CGuildWelfareWnd.GUILD_RANK_COUNT do
            if i ~= idx and CGuildWelfareModel.ids[idx] == CGuildWelfareModel.ids[i] then
                this.inputs[i]:SetValue(value, false)
            end
            i = i + 1
        end
    end
    this:UpdateInputStatus()
end
CGuildWelfareWnd.m_OnAllocExtraGongXunSuccess_CS2LuaHook = function (this) 
    this:ClearInputStatus()
    this:UpdateGongXuns()
    this:UpdateInputStatus()
    this.scrollView:ResetPosition()
end
CGuildWelfareWnd.m_OnClickItem_CS2LuaHook = function (this, go) 
    local index = CommonDefs.ArrayFindIndex_GameObject_Array(this.rects, DelegateFactory.Predicate_GameObject(function (p2) 
        return p2 == go
    end))
    if index > - 1 then
        local playerId = CGuildWelfareModel.ids[index]
        if playerId > 0 then
            CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
        end
    end
end
CGuildWelfareWnd.m_OnClickApplyButton_CS2LuaHook = function (this, go) 
    local message = g_MessageMgr:FormatMessage("Confirm_Allocate_Gongxun")
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
        -- 处理职位兼任的情况
        local allocMap = CreateFromClass(MakeGenericClass(Dictionary, UInt64, UInt32))
        do
            local i = 0
            while i < CGuildWelfareWnd.GUILD_RANK_COUNT do
                if CGuildWelfareModel.ids[i] ~= 0 then
                    CommonDefs.DictSet(allocMap, typeof(UInt64), CGuildWelfareModel.ids[i], typeof(UInt32), this.inputs[i]:GetValue())
                end
                i = i + 1
            end
        end

        local infos = CreateFromClass(MakeGenericClass(List, Object))
        CommonDefs.DictIterate(allocMap, DelegateFactory.Action_object_object(function (___key, ___value) 
            local kvp = {}
            kvp.Key = ___key
            kvp.Value = ___value
            CommonDefs.ListAdd(infos, typeof(UInt64), kvp.Key)
            CommonDefs.ListAdd(infos, typeof(UInt32), kvp.Value)
        end))
        Gac2Gas.RequestAllocExtraGongXun(false, MsgPackImpl.pack(infos))
    end), nil, nil, nil, false)
end
CGuildWelfareWnd.m_OnClickRandomButton_CS2LuaHook = function (this, go) 
    local message = g_MessageMgr:FormatMessage("Confirm_Random_Allocate_Gongxun")
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
        Gac2Gas.RequestAllocExtraGongXun(true, MsgPackImpl.pack(CreateFromClass(MakeGenericClass(List, Object))))
        --随机分配
    end), nil, nil, nil, false)
end
CGuildWelfareWnd.m_OnClickCloseBtn_CS2LuaHook = function (this, go) 
    if CGuildWelfareModel.CanAlloc and this:hasAllocGongxun() then
        local message = g_MessageMgr:FormatMessage("GONGXUN_NOT_STORE")
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
            local infos = CreateFromClass(MakeGenericClass(List, Object))
            do
                local i = 0
                while i < CGuildWelfareWnd.GUILD_RANK_COUNT do
                    if CGuildWelfareModel.ids[i] ~= 0 then
                        CommonDefs.ListAdd(infos, typeof(UInt64), CGuildWelfareModel.ids[i])
                        CommonDefs.ListAdd(infos, typeof(UInt32), this.inputs[i]:GetValue())
                    end
                    i = i + 1
                end
            end
            Gac2Gas.RequestAllocExtraGongXun(false, MsgPackImpl.pack(infos))
            this:Close()
        end), DelegateFactory.Action(function () 
            this:Close()
        end), nil, nil, false)
    else
        this:Close()
    end
end
CGuildWelfareWnd.m_hasAllocGongxun_CS2LuaHook = function (this) 
    local result = false
    do
        local i = 0
        while i < CGuildWelfareWnd.GUILD_RANK_COUNT do
            if this.inputs[i]:GetValue() ~= 0 then
                return true
            end
            i = i + 1
        end
    end
    return result
end
