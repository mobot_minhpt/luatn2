-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CTalismanBaptizeListTemplate = import "L10.UI.CTalismanBaptizeListTemplate"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUITexture = import "L10.UI.CUITexture"
local EnumEventType = import "EnumEventType"
local EnumQualityType = import "L10.Game.EnumQualityType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local IdPartition = import "L10.Game.IdPartition"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTalismanBaptizeListTemplate.m_Init_CS2LuaHook = function (this, itemId) 

    this.nameLabel.text = ""
    this.iconTexture.material = nil
    this.levelLabel.text = ""
    this.bindSprite.enabled = false
    Extensions.RemoveAllChildren(this.stoneTable.transform)
    this.stoneTemplate:SetActive(false)
    this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)

    this.talisman = CItemMgr.Inst:GetById(itemId)
    if this.talisman ~= nil and this.talisman.IsEquip and IdPartition.IdIsTalisman(this.talisman.TemplateId) then
        this.nameLabel.text = this.talisman.Name
        this.nameLabel.color = this.talisman.Equip.DisplayColor
        this.iconTexture:LoadMaterial(this.talisman.Icon)
        this.bindSprite.enabled = true
        this.bindSprite.spriteName = this.talisman.BindOrEquipCornerMark
        this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(this.talisman.Equip.QualityType)
        UIEventListener.Get(this.qualitySprite.gameObject).onClick = MakeDelegateFromCSFunction(this.OnIconClicked, VoidDelegate, this)

        this:InitStoneList()
    end
end
CTalismanBaptizeListTemplate.m_InitStoneList_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.stoneTable.transform)

    local item = CItemMgr.Inst:GetById(this.talisman.Id)
    local maxHole = CItemMgr.Inst:GetEquipmentMaxHole(this.talisman.TemplateId)
    local Hole = item.Equip.Hole
    do
        local i = 1
        while i <= Hole do
            local stone = CItemMgr.Inst:GetItemTemplate(item.Equip.HoleItems[i])
            if stone ~= nil then
                local stoneGo = NGUITools.AddChild(this.stoneTable.gameObject, this.stoneTemplate)
                stoneGo:SetActive(true)
                CommonDefs.GetComponent_GameObject_Type(stoneGo, typeof(CUITexture)):LoadMaterial(stone.Icon)
            else
                local stoneGo = NGUITools.AddChild(this.stoneTable.gameObject, this.stoneTemplate)
                stoneGo:SetActive(true)
                CommonDefs.GetComponent_GameObject_Type(stoneGo, typeof(CUITexture)):Clear()
            end
            i = i + 1
        end
    end
    this.stoneTable:Reposition()
end
CTalismanBaptizeListTemplate.m_OnEnable_CS2LuaHook = function (this) 
    if this.needUpdate then
        this:Refresh()
        this.needUpdate = false
    end
    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
end
CTalismanBaptizeListTemplate.m_Refresh_CS2LuaHook = function (this) 
    if this.talisman ~= nil then
        this.nameLabel.text = this.talisman.Equip.DisplayName
        this.nameLabel.color = this.talisman.Equip.DisplayColor
        -- QualityColor.GetRGBValue(item.Equip.QualityType, item.Equip.WordsCount);

        this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(this.talisman.Equip.QualityType)
        this.levelLabel.text = tostring(this.talisman.Equip.NeedLevel)
        this.bindSprite.spriteName = this.talisman.BindOrEquipCornerMark
        --宝石刷新
        this:InitStoneList()
    end
end
CTalismanBaptizeListTemplate.m_OnIconClicked_CS2LuaHook = function (this, go) 

    if this.talisman ~= nil then
        CItemInfoMgr.ShowLinkItemInfo(this.talisman, false, nil, AlignType.Default, 0, 0, 0, 0, 0)
        if this.onIconClick ~= nil then
            GenericDelegateInvoke(this.onIconClick, Table2ArrayWithCount({this.gameObject}, 1, MakeArrayClass(Object)))
        end
    end
end
