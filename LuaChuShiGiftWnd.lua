local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local QnCheckBoxGroup = import "L10.UI.QnCheckBoxGroup"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaChuShiGiftWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChuShiGiftWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaChuShiGiftWnd, "Portrait", "Portrait", CUITexture)
RegistChildComponent(LuaChuShiGiftWnd, "LvLabel", "LvLabel", UILabel)
RegistChildComponent(LuaChuShiGiftWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaChuShiGiftWnd, "ReadMeLabel", "ReadMeLabel", UILabel)
RegistChildComponent(LuaChuShiGiftWnd, "DesLabel", "DesLabel", UILabel)
RegistChildComponent(LuaChuShiGiftWnd, "UnSetLabel", "UnSetLabel", UILabel)
RegistChildComponent(LuaChuShiGiftWnd, "FixLabel", "FixLabel", UILabel)
RegistChildComponent(LuaChuShiGiftWnd, "QnCheckBoxGroup", "QnCheckBoxGroup", QnCheckBoxGroup)
RegistChildComponent(LuaChuShiGiftWnd, "QnCheckBoxGroupTable", "QnCheckBoxGroupTable", UITable)
RegistChildComponent(LuaChuShiGiftWnd, "QnCheckBoxTel", "QnCheckBoxTel", GameObject)
RegistChildComponent(LuaChuShiGiftWnd, "GiveGiftButton", "GiveGiftButton", GameObject)
RegistChildComponent(LuaChuShiGiftWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaChuShiGiftWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaChuShiGiftWnd, "Best", "Best", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaChuShiGiftWnd, "m_SelectId")
RegistClassMember(LuaChuShiGiftWnd, "m_Data")
RegistClassMember(LuaChuShiGiftWnd, "m_JiyuId")

function LuaChuShiGiftWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.FixLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFixLabelClick()
	end)


	
	UIEventListener.Get(self.GiveGiftButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGiveGiftButtonClick()
	end)


	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


    --@endregion EventBind end
end

function LuaChuShiGiftWnd:OnEnable()
	g_ScriptEvent:AddListener("ModifyChuShiJiYuSucc", self, "OnModifyChuShiJiYuSucc")
end

function LuaChuShiGiftWnd:OnDisable()
	LuaShiTuMgr.m_ChuShiGiftInfo = {}
	g_ScriptEvent:RemoveListener("ModifyChuShiJiYuSucc", self, "OnModifyChuShiJiYuSucc")
end

function LuaChuShiGiftWnd:OnModifyChuShiJiYuSucc(tudiId, jiyuId)
	if LuaShiTuMgr.m_ChuShiGiftInfo.tudiId == tudiId then
		local jiyuData = ShiTu_ChuShiJiYu.GetData(jiyuId)
		self.m_JiyuId = jiyuId
		self.DesLabel.gameObject:SetActive(jiyuData)
		if jiyuData then
			self.DesLabel.text = jiyuData.Content
		end
		self.UnSetLabel.gameObject:SetActive(not jiyuData)
		self.FixLabel.gameObject:SetActive(true)
	end
end

function LuaChuShiGiftWnd:Init()
	self.TitleLabel.gameObject:SetActive(false)
	self.ReadMeLabel.text = g_MessageMgr:FormatMessage("ChuShiGiftWnd_ReadMe")
	local info = LuaShiTuMgr.m_ChuShiGiftInfo
	self.Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(info.class, info.gender, -1), false)
	self.LvLabel.text = SafeStringFormat3("lv.%d", info.level)
	self.NameLabel.text = info.tudiName
	local jiyuData = ShiTu_ChuShiJiYu.GetData(info.jiyuId)
	self.DesLabel.gameObject:SetActive(jiyuData)
	self.FixLabel.gameObject:SetActive(true)
	if jiyuData then
		self.DesLabel.text = jiyuData.Content
	end
	self.UnSetLabel.gameObject:SetActive(not jiyuData)
	self:InitQnCheckBoxGroup()
	self:OnModifyChuShiJiYuSucc(info.tudiId, info.jiyuId)
end

function LuaChuShiGiftWnd:InitQnCheckBoxGroup()
	local info = LuaShiTuMgr.m_ChuShiGiftInfo
	local minLevel = 0
	ShiTu_ChuShiLi.Foreach(function (k,v)
		if v.ChuShiLevel <= info.level and minLevel <= v.ChuShiLevel then
			minLevel = math.max(v.ChuShiLevel,minLevel)
		end
	end)
	local arr = {}
	local nameArr = {}
	ShiTu_ChuShiLi.Foreach(function (k,v)
		if v.ChuShiLevel == minLevel then
			local itemData = Item_Item.GetData(v.GiftItemId)
			table.insert(nameArr,itemData.Name)
			table.insert(arr,ShiTu_ChuShiLi.GetData(k))
		end
	end)
	self.m_Data = arr
	self.QnCheckBoxGroup:InitWithOptions(Table2Array(nameArr, MakeArrayClass(String)), false, false)
    self.QnCheckBoxGroup.OnSelect = DelegateFactory.Action_QnCheckBox_int(function(checkbox,level)
        self:OnSelect(checkbox, level)
    end)
	self.QnCheckBoxGroup:SetSelect(0,false,false)

	for i = 1, #arr do
		local obj = self.QnCheckBoxGroup.m_CheckBoxes[i - 1]
		local cell = obj.transform:Find("ItemCell")
		local iconTexture = obj.transform:Find("ItemCell/IconTexture"):GetComponent(typeof(CUITexture))
		local data = arr[i]
		local itemData = Item_Item.GetData(data.GiftItemId)
		iconTexture:LoadMaterial(itemData.Icon)
		UIEventListener.Get(cell.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			CItemInfoMgr.ShowLinkItemTemplateInfo(data.GiftItemId)
		end)
	end
end

--@region UIEvent

function LuaChuShiGiftWnd:OnFixLabelClick()
	LuaShiTuMgr.m_ShiTuChooseJiYuWnd_TuDiId = LuaShiTuMgr.m_ChuShiGiftInfo.tudiId
	CUIManager.ShowUI(CLuaUIResources.ShiTuChooseJiYuWnd)
end

function LuaChuShiGiftWnd:OnGiveGiftButtonClick()
	Gac2Gas.SendChuShiGift(LuaShiTuMgr.m_ChuShiGiftInfo.tudiId, self.m_SelectId,self.m_JiyuId and self.m_JiyuId or 0)
	CUIManager.CloseUI(CLuaUIResources.ChuShiGiftWnd)
end

function LuaChuShiGiftWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("ChuShiGiftWnd_Tip")
end


function LuaChuShiGiftWnd:OnSelect(checkbox, level)
	local data = self.m_Data[level + 1]
	self.m_SelectId = data.ID
	self.QnCostAndOwnMoney:SetType(data.PrizeType == 1 and EnumMoneyType.YinLiang or EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
	self.QnCostAndOwnMoney:SetCost(data.Prize)
end
--@endregion UIEvent

