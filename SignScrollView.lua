-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CSignItemCell = import "L10.UI.CSignItemCell"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local NGUITools = import "NGUITools"
local SignScrollView = import "L10.UI.SignScrollView"
local System = import "System"
SignScrollView.m_Init_CS2LuaHook = function (this) 

    CommonDefs.ListClear(this.list)
    Extensions.RemoveAllChildren(this.table.transform)
    this.itemCellTpl:SetActive(false)
    local day = System.DateTime.DaysInMonth(CSigninMgr.Inst.year, CSigninMgr.Inst.month)
    do
        local i = 0
        while i < day do
            local itemcell = NGUITools.AddChild(this.table.gameObject, this.itemCellTpl)
            CommonDefs.GetComponent_GameObject_Type(itemcell, typeof(CSignItemCell)):Init(i + 1, this.scrollview)
            itemcell:SetActive(true)
            CommonDefs.ListAdd(this.list, typeof(GameObject), itemcell)
            i = i + 1
        end
    end
    this.table:Reposition()
    this.scrollview:ResetPosition()
    local signedDaysExceptToday = CSigninMgr.Inst.hasSignedToday and CSigninMgr.Inst.signedDays - 1 or CSigninMgr.Inst.signedDays
    local row = math.floor(signedDaysExceptToday / this.table.maxPerLine)
    local totalrows = math.ceil(day / this.table.maxPerLine)
    if this.table ~= nil and this.scrollview.panel ~= nil then
        local offset = math.min(row * this.table.cellHeight / (totalrows * this.table.cellHeight - this.scrollview.panel.height), 1)
        this.scrollview:SetDragAmount(0, offset, false)
    end

    if not CSigninMgr.Inst.hasSignedToday and CSigninMgr.Inst.signedDays < this.list.Count then
        CommonDefs.GetComponent_GameObject_Type(this.list[CSigninMgr.Inst.signedDays], typeof(CSignItemCell)):LoadFx()
    end
end
