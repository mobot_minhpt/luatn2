local DelegateFactory = import "DelegateFactory"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"
local CGamePlayMgr    = import "L10.Game.CGamePlayMgr"

LuaWorldCup2022GJZLView = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWorldCup2022GJZLView, "taskName")
RegistClassMember(LuaWorldCup2022GJZLView, "desc")
RegistClassMember(LuaWorldCup2022GJZLView, "bg")
RegistClassMember(LuaWorldCup2022GJZLView, "progress")
RegistClassMember(LuaWorldCup2022GJZLView, "time")
RegistClassMember(LuaWorldCup2022GJZLView, "slider")

RegistClassMember(LuaWorldCup2022GJZLView, "tick")

function LuaWorldCup2022GJZLView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self:InitUIComponents()
    self:InitEventListener()
end

-- 初始化UI组件
function LuaWorldCup2022GJZLView:InitUIComponents()
    self.taskName = self.transform:Find("TaskName"):GetComponent(typeof(UILabel))
    self.desc = self.transform:Find("Desc"):GetComponent(typeof(UILabel))
    self.bg = self.transform:Find("Bg"):GetComponent(typeof(UIWidget))
    self.progress = self.transform:Find("Bg/Progress"):GetComponent(typeof(UILabel))
    self.time = self.transform:Find("Bg/Time"):GetComponent(typeof(UILabel))
    self.slider = self.transform:Find("Bg/Slider"):GetComponent(typeof(UISlider))
end

function LuaWorldCup2022GJZLView:InitEventListener()
    local leaveButton = self.transform:Find("Bg/LeaveButton").gameObject
    local ruleButton = self.transform:Find("Bg/RuleButton").gameObject

    UIEventListener.Get(leaveButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveButtonClick()
	end)

    UIEventListener.Get(ruleButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleButtonClick()
	end)
end

function LuaWorldCup2022GJZLView:OnEnable()
    g_ScriptEvent:AddListener("GJZL_SyncStage", self, "OnSyncStage")
    g_ScriptEvent:AddListener("GJZL_SyncProgress", self, "OnSyncProgress")
    g_ScriptEvent:AddListener("GJZL_SelectPositionSuccess", self, "OnSelectPositionSuccess")
end

function LuaWorldCup2022GJZLView:OnDisable()
    g_ScriptEvent:RemoveListener("GJZL_SyncStage", self, "OnSyncStage")
    g_ScriptEvent:RemoveListener("GJZL_SyncProgress", self, "OnSyncProgress")
    g_ScriptEvent:RemoveListener("GJZL_SelectPositionSuccess", self, "OnSelectPositionSuccess")
    self:ClearTick()
end

function LuaWorldCup2022GJZLView:OnSyncStage()
    self:UpdateStage()
end

function LuaWorldCup2022GJZLView:OnSyncProgress()
    self:UpdateProgress()
end

function LuaWorldCup2022GJZLView:OnSelectPositionSuccess()
    if LuaWorldCup2022Mgr.GJZLInfo.stage then
        self:UpdateDesc()
        self.bg:ResetAndUpdateAnchors()
    end
end


function LuaWorldCup2022GJZLView:Init()
    self:UpdateStage()
end

-- 更新阶段信息
function LuaWorldCup2022GJZLView:UpdateStage()
    local info = LuaWorldCup2022Mgr.GJZLInfo
    if not info.stage then
        self.taskName.text = LocalString.GetString("冠军之路")
        self.desc.text = ""
        self.progress.text = ""
        self.time.text = ""
        self.slider.gameObject:SetActive(false)
        return
    end

    self:UpdateDesc()
    self.taskName.text = (info.stage == 0 or info.stage == 6) and LocalString.GetString("冠军之路") or SafeStringFormat3(LocalString.GetString("冠军之路 (阶段%d)"), info.stage)
    self.bg:ResetAndUpdateAnchors()
    self:UpdateProgress()

    self:ClearTick()
    self:UpdateTime()
    self.tick = RegisterTick(function()
        self:UpdateTime()
    end, 1000)
end

-- 更新描述
function LuaWorldCup2022GJZLView:UpdateDesc()
    local info = LuaWorldCup2022Mgr.GJZLInfo
    if info.stage == 4 then
        local str = ""
        if info.myPos and info.myPos > 0 then
            if info.myPos == 1 then
                str = "WORLDCUP2022_GUANJUNZHILU_DESC_%d_ZHONGFENG"
            elseif info.myPos == 2 or info.myPos == 3 then
                str = "WORLDCUP2022_GUANJUNZHILU_DESC_%d_BIANFENG"
            else
                str = "WORLDCUP2022_GUANJUNZHILU_DESC_%d_ZHONGCHANG"
            end
        else
            str = "WORLDCUP2022_GUANJUNZHILU_DESC_%d"
        end
        self.desc.text = g_MessageMgr:FormatMessage(SafeStringFormat3(str, info.stage))
    else
        self.desc.text = g_MessageMgr:FormatMessage(SafeStringFormat3("WORLDCUP2022_GUANJUNZHILU_DESC_%d", info.stage))
    end
end

-- 更新时间
function LuaWorldCup2022GJZLView:UpdateTime()
    local info = LuaWorldCup2022Mgr.GJZLInfo

    if not info.countDownTime then return end
    local leftTime = math.floor(info.countDownTime - CServerTimeMgr.Inst.timeStamp)
    if leftTime < 0 then
        self.time.text = "00:00"
        self:ClearTick()
        return
    end

    local m = math.floor(leftTime / 60)
    local s = leftTime % 60
    self.time.text = SafeStringFormat3("%02d:%02d", m, s)
end

-- 更新进度
function LuaWorldCup2022GJZLView:UpdateProgress()
    local info = LuaWorldCup2022Mgr.GJZLInfo
    local stage = info.stage
    if not stage then return end

    if info.stage == 0 or info.stage == 6 then
        self.slider.gameObject:SetActive(false)
        self.progress.text = ""
        return
    end

    local baseStr = ""
    if stage == 1 then
        baseStr = LocalString.GetString("正面硬币")
    elseif stage == 2 then
        baseStr = LocalString.GetString("红牌")
    elseif stage == 3 then
        baseStr = LocalString.GetString("球门")
    elseif stage == 4 then
        baseStr = LocalString.GetString("进球")
    elseif stage == 5 then
        baseStr = LocalString.GetString("奖杯")
    end

    if stage == 1 or stage == 2 or stage == 4 then
        self.slider.gameObject:SetActive(false)

        if info.progress then
            self.progress.text = SafeStringFormat3("%s (%d/%d)", baseStr, info.progress, info.fullProgress)
        else
            self.progress.text = baseStr
        end
    else
        if info.progress then
            self.slider.gameObject:SetActive(true)
            local percent = info.fullProgress > 0 and info.progress / info.fullProgress or 0
            self.slider.value = percent
        else
            self.slider.gameObject:SetActive(false)
        end

        self.progress.text = baseStr
    end
end

function LuaWorldCup2022GJZLView:ClearTick()
    if self.tick then
        UnRegisterTick(self.tick)
    end
end

function LuaWorldCup2022GJZLView:OnDestroy()
    LuaWorldCup2022Mgr.GJZLInfo = {}
end

--@region UIEvent

function LuaWorldCup2022GJZLView:OnLeaveButtonClick()
    CGamePlayMgr.Inst:LeavePlay()
end

function LuaWorldCup2022GJZLView:OnRuleButtonClick()
    local info = LuaWorldCup2022Mgr.GJZLInfo
    if not info.stage then return end

    if info.stage == 0 then
        g_MessageMgr:ShowMessage("WORLDCUP2022_GUANJUNZHILU_RULE")
        return
    end

    g_MessageMgr:ShowMessage(SafeStringFormat3("WORLDCUP2022_GUANJUNZHILU_RULE_%d", info.stage))
end

--@endregion UIEvent
