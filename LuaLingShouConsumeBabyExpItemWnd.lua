require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local Gac2Gas=import "L10.Game.Gac2Gas"
local CLingShouBaseMgr=import "L10.Game.CLingShouBaseMgr"
local CItemMgr=import "L10.Game.CItemMgr"
local Constants=import "L10.Game.Constants"
local EnumItemPlace=import "L10.Game.EnumItemPlace"

CLuaLingShouConsumeBabyExpItemWnd=class()
RegistClassMember(CLuaLingShouConsumeBabyExpItemWnd,"m_NeedExpLabel")
RegistClassMember(CLuaLingShouConsumeBabyExpItemWnd,"m_ParentExpLabel")
RegistClassMember(CLuaLingShouConsumeBabyExpItemWnd,"m_TitleLabel")

RegistClassMember(CLuaLingShouConsumeBabyExpItemWnd,"m_FeedBaby")

RegistClassMember(CLuaLingShouConsumeBabyExpItemWnd,"m_UseExpItemId")
RegistClassMember(CLuaLingShouConsumeBabyExpItemWnd,"m_UseExpPos")
RegistClassMember(CLuaLingShouConsumeBabyExpItemWnd,"m_UseExp")


function CLuaLingShouConsumeBabyExpItemWnd:Init()
    self.m_UseExpPos=0
    self.m_UseExpItemId=nil
    self.m_UseExp=0
    if CUIManager.IsLoaded(CUIResources.LingShouBabyWnd) then
        self.m_FeedBaby=true
    else
        self.m_FeedBaby=false
    end
    local g = LuaGameObject.GetChildNoGC(self.transform,"OkButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        --注入经验
        if self.m_UseExpItemId then
            if self.m_FeedBaby then
                Gac2Gas.RequestLingShouBabyUseExpItem(CLingShouMgr.Inst.selectedLingShou,1,self.m_UseExpPos, self.m_UseExpItemId)
            else
                Gac2Gas.RequestFeedLingShou(CLingShouMgr.Inst.selectedLingShou, 1, self.m_UseExpPos, self.m_UseExpItemId)
            end
        end
    end)
    local g = LuaGameObject.GetChildNoGC(self.transform,"CancleButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        --注入经验
        CUIManager.CloseUI(CLuaUIResources.LingShouConsumeBabyExpItemWnd)
    end)

    self.m_TitleLabel=LuaGameObject.GetChildNoGC(self.transform,"TitleLable").label
    self.m_TitleLabel.text=LocalString.GetString("注入经验快速升级")

    if self.m_FeedBaby then
        LuaGameObject.GetChildNoGC(self.transform,"Label1").label.text=LocalString.GetString("宝宝升级所需经验")
    else
        LuaGameObject.GetChildNoGC(self.transform,"Label1").label.text=LocalString.GetString("灵兽升级所需经验")
    end
    LuaGameObject.GetChildNoGC(self.transform,"Label2").label.text=LocalString.GetString("九转黄金丹总经验")
    
    self.m_NeedExpLabel=LuaGameObject.GetChildNoGC(self.transform,"NeedExpLabel").label
    self.m_NeedExpLabel.text="0"
    self.m_ParentExpLabel=LuaGameObject.GetChildNoGC(self.transform,"ParentExpLabel").label
    self.m_ParentExpLabel.text="0"


    self:InitData()


end
function CLuaLingShouConsumeBabyExpItemWnd:InitData()
    -- self.m_UseExpItem=nil
    self.m_UseExpItemId=nil
    --先选经验丹
    local bindItemCountInBag,notBindItemCountInBag=CItemMgr.Inst:CalcItemCountInBagByTemplateId(Constants.BabyFangShengExpItemId)
    local count=bindItemCountInBag+notBindItemCountInBag
    --没有经验丹 就关掉界面
    if count==0 then
        CUIManager.CloseUI(CLuaUIResources.LingShouConsumeBabyExpItemWnd)
        return
    end

    local list=CItemMgr.Inst:GetPlayerItemsAtPlaceByTemplateId(EnumItemPlace.Bag,Constants.BabyFangShengExpItemId)
    self.m_UseExpItemId=nil
    self.m_UseExp=18446744073709551615
    self.m_UseExpPos=0
    for i=1,list.Count do
        local item=list[i-1]
        local commonItem=CItemMgr.Inst:GetById(item.itemId)
        if commonItem then
            local exp=CLingShouMgr.GetBabyFangShengItemExp(commonItem)
            if self.m_UseExp>exp then
                self.m_UseExp=exp
                self.m_UseExpItemId=item.itemId
                self.m_UseExpPos=item.pos
            end
        end
    end
    if not self.m_UseExpItemId then
        CUIManager.CloseUI(CLuaUIResources.LingShouConsumeBabyExpItemWnd)
        return
    end

    -- self.m_UseExpItem,self.m_UseExp,self.m_UseExpPos=CItemMgr.Inst:GetMinExpBabyExpItem()

    local lingShouId=CLingShouMgr.Inst.selectedLingShou
    if lingShouId~=nil or lingShouId~="" then
        local details=CLingShouMgr.Inst:GetLingShouDetails(lingShouId)
        if details~=nil then
            if self.m_FeedBaby then
                --喂宝宝
                --经验丹的经验值
                self.m_ParentExpLabel.text=tostring(self.m_UseExp)
                local babyInfo = details.data.Props.Baby
                if babyInfo.BornTime>0 then
                    local needExp=LingShouBaby_BabyExp.Exists(babyInfo.Level) and LingShouBaby_BabyExp.GetData(babyInfo.Level).ExpFull or 0
                    needExp=math.max(needExp-babyInfo.Exp,0)
                    self.m_NeedExpLabel.text=tostring(needExp)
                end
            else
                --经验丹的经验值
                self.m_ParentExpLabel.text=tostring(self.m_UseExp)
                local needExp = CLingShouBaseMgr.GetLevelRequireExp(details.data.Level)
                needExp=math.max(needExp-details.data.Exp,0)
                self.m_NeedExpLabel.text=tostring(needExp)
            end
        end
    end
end

function CLuaLingShouConsumeBabyExpItemWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
    
end

function CLuaLingShouConsumeBabyExpItemWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
end

function CLuaLingShouConsumeBabyExpItemWnd:OnSendItem(args)
    local itemId=args[0]
    if self.m_UseExpItemId==itemId then
        self:InitData()
    end
end

return CLuaLingShouConsumeBabyExpItemWnd