g_sprites = {
    OddBgSpirite = "common_bg_mission_background_n",
    EvenBgSprite = "common_bg_mission_background_s",
    HighlightBgSprite = "common_bg_mission_background_highlight",
    MailNotAwardedGiftIcon = "friendwnd_weilingqu",
    MailAwardedGiftIcon = "friendwnd_yilingqu",
    GreenHandGiftWnd_BlueButtonName = "common_btn_01_blue",
    GreenHandGiftWnd_OrangeButtonName = "common_btn_01_yellow",
    RankFirstSpriteName = "Rank_No.1",
    RankSecondSpriteName = "Rank_No.2png",
    RankThirdSpriteName = "Rank_No.3png",
    CommonWinSpriteName = "common_fight_result_win",
    CommonLoseSpriteName = "common_fight_result_lose",
    Common_Huangguan_01 = "common_huangguan_01",
    Common_Huangguan_02 = "common_huangguan_02",
    Common_Huangguan_03 = "common_huangguan_03",
    TeamChannelSprite = "thumbnailchat_teamchannel_bg",
    WorldChannelSprite = "thumbnailchat_worldchannel_bg",
    TransparentSprite = "common_bg_transparent",
    BlueProgressSprite = "common_mainplayerwnd_blue",
    RedProgressSprite = "common_mainplayerwnd_red",
    YellowProgressSprite = "common_mainplayerwnd_yellow",
    CommonHPSprite = "common_hpandmp_hp",
	CommonMPSprite = "common_hpandmp_mp",
    MinimapCujuBlueSprite = "minimap_cuju_blue",
    MinimapCujuRedSprite = "minimap_cuju_red",
    MinimapCujuBlueSelfSprite = "minimap_cuju_player_blue",
    MinimapCujuRedSelfSpriteSprite = "minimap_cuju_player_red",
    CommonBlueButtonBg = "common_btn_01_blue",
    CommonOrangeButtonBg = "common_btn_01_yellow",
    AppearanceCommonBlueButtonBg = "appearancewnd_btn_01",
    AppearanceCommonOrangeButtonBg = "appearancewnd_btn_02",
}