-- Auto Generated!!
local CJingLingChatView = import "L10.UI.CJingLingChatView"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local IJingLingChatItem = import "L10.UI.IJingLingChatItem"
local JingLingMessageType = import "L10.Game.JingLingMessageType"
CJingLingChatView.m_Init_CS2LuaHook = function (this) 

    this.tableView:Clear()
    this.tableView.dataSource = this
    if CJingLingMgr.Inst.Msgs.Count == 0 then
        CJingLingMgr.Inst:AddIntroduction()
    end
    this:UpdateChatMessages()
end
CJingLingChatView.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 

    local msgs = CJingLingMgr.Inst.Msgs
    if index < 0 and index >= msgs.Count then
        return nil
    end
    local cellIdentifier = nil
    local template = nil
    if msgs[index].msgType == JingLingMessageType.TimeSplit then
        cellIdentifier = "TimeSplitCell"
        template = this.timesplitTemplate
    elseif msgs[index].msgType == JingLingMessageType.Answer then
        cellIdentifier = "AnswerTemplate"
        template = this.answerTemplate
    elseif msgs[index].msgType == JingLingMessageType.Question then
        cellIdentifier = "QuestionTemplate"
        template = this.questionTemplate
    end
    if cellIdentifier == nil then
        return nil
    end

    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(template, cellIdentifier)
    end

    local chatItem = CommonDefs.GetComponent_GameObject_Type(cell, typeof(IJingLingChatItem))
    CJingLingMgr.Inst:MarkAsRead(index)
    chatItem:Init(msgs[index])
    return cell
end

