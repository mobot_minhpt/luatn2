-- Auto Generated!!

local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local CIMMgr = import "L10.Game.CIMMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DelegateFactory = import "DelegateFactory"
local Double = import "System.Double"
local ExpressionHead_Expression = import "L10.Game.ExpressionHead_Expression"
local Int32 = import "System.Int32"
local CPersonalSpace_ExpressionBase = import "L10.Game.CPersonalSpace_ExpressionBase"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"

CExpressionMgr.m_OnMainPlayerDestroyed_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.validExpressionList)
    CommonDefs.ListClear(this.validExpressionTxtList)
    CommonDefs.ListClear(this.validStickerList)
    CommonDefs.ListClear(this.validProfileFrameList)

    LuaExpressionMgr:UnRegistCheckExpired()
    LuaExpressionMgr.m_ValidLimitExpressionTxtList = {}
end
CExpressionMgr.m_OnMainPlayerCreated_CS2LuaHook = function (this) 
    Gac2Gas.QueryValidExpression()
    Gac2Gas.QueryValidExpressionTxt()
    Gac2Gas.QueryValidSticker()
    Gac2Gas.QueryValidProfileFrame()
end
CExpressionMgr.m_AddExpression_CS2LuaHook = function (this, id) 
    if not CommonDefs.ListContains(this.validExpressionList, typeof(Int32), id) then
        CommonDefs.ListAdd(this.validExpressionList, typeof(Int32), id)
    end
end
CExpressionMgr.m_AddSticker_CS2LuaHook = function (this, id) 
    if not CommonDefs.ListContains(this.validStickerList, typeof(Int32), id) then
        CommonDefs.ListAdd(this.validStickerList, typeof(Int32), id)
    end
end
CExpressionMgr.m_RemoveProfileFrame_CS2LuaHook = function (this, id) 
    local list = CExpressionMgr.Inst.validProfileFrameList
    for i=0,list.Count-1 do
        if math.floor(list[i][0])==id then
            CommonDefs.ListRemove(this.validProfileFrameList, typeof(MakeArrayClass(Double)), list[i])
            break
        end
    end
end
CExpressionMgr.m_AddProfileFrame_CS2LuaHook = function (this, id, time) 
    local exist = false
    CommonDefs.ListIterate(this.validProfileFrameList, DelegateFactory.Action_object(function (___value) 
        local data = ___value
        if data[0] == id then
            data[1] = time
            exist = true
        end
    end))

    if not exist then
        CommonDefs.ListAdd(this.validProfileFrameList, typeof(MakeArrayClass(Double)), Table2ArrayWithCount({id, time}, 2, MakeArrayClass(System.Double)))
    end
end
CExpressionMgr.m_HasGetExpression_CS2LuaHook = function (this, key) 
    if key == CExpressionMgr.DefaultExpressionId then
        return true
    end
    if CommonDefs.ListContains(this.validExpressionList, typeof(Int32), key) then
        return true
    else
        return false
    end
end
CExpressionMgr.m_HasGetExpressionTemplateId_CS2LuaHook = function (this, templateId) 
    if CClientMainPlayer.Inst == nil then
        return false
    end
    do
        local i = 0
        while i < this.validExpressionList.Count do
            local expressionId = CIMMgr.Inst:Expression2ExpressionId(this.validExpressionList[i], EnumToInt(CClientMainPlayer.Inst.Class), EnumToInt(CClientMainPlayer.Inst.Gender))
            local data = ExpressionHead_Expression.GetData(expressionId)
            if data ~= nil then
                if data.ItemID == templateId then
                    return true
                end
            end
            i = i + 1
        end
    end
    return false
end
CExpressionMgr.m_GetValidExpressionCount_CS2LuaHook = function (this) 
    if CommonDefs.ListContains(this.validExpressionList, typeof(Int32), 0) then
        return this.validExpressionList.Count
    end
    return this.validExpressionList.Count + 1
end
CExpressionMgr.m_HasGetExpressionTxt_CS2LuaHook = function (this, key) 
    if CommonDefs.ListContains(this.validExpressionTxtList, typeof(Int32), key) or LuaExpressionMgr:GetLimitExpressionTxt(key)  then
        return true
    else
        return false
    end
end
CExpressionMgr.m_HasGetSticker_CS2LuaHook = function (this, key) 
    if CommonDefs.ListContains(this.validStickerList, typeof(Int32), key) then
        return true
    else
        return false
    end
end
--新功能建议使用LuaExpressionMgr:GetProfieFrameExpireTime取代该方法，该方法会过滤掉过期的头像框
CExpressionMgr.m_HasGetProfileFrame_CS2LuaHook = function (this, key) 
    do
        local __itr_is_triger_return = nil
        local __itr_result = nil
        CommonDefs.ListIterateWithRet(this.validProfileFrameList, DelegateFactory.SingleValIterFunc(function (___value) 
            local data = ___value
            if data[0] == key then
                --小于0为永久头像框，否则为带有效期的头像框
                if data[1]<0 or data[1] > CServerTimeMgr.Inst.timeStamp then
                    __itr_is_triger_return = true
                    __itr_result = data[1]
                    return true
                end
            end
        end))
        if __itr_is_triger_return == true then
            return __itr_result
        end
    end
    return 0
end

LuaExpressionMgr = {}

LuaExpressionMgr.m_WaitForMainPlayerTick = nil

-- 检查限时贴纸是否过期的tick
LuaExpressionMgr.m_CheckExpiredTick = nil

-- 已拥有的未过期的限时ExpressionTxt
LuaExpressionMgr.m_ValidLimitExpressionTxtList = {}

if rawget(_G, "LuaExpressionMgr") then
    g_ScriptEvent:RemoveListener("UpdateMainPlayerExpressionTxt", LuaExpressionMgr, "OnUpdateMainPlayerExpressionTxt")
end

g_ScriptEvent:AddListener("UpdateMainPlayerExpressionTxt", LuaExpressionMgr, "OnUpdateMainPlayerExpressionTxt")

function LuaExpressionMgr:WaitForMainPlayerCreated(func)
    UnRegisterTick(self.m_WaitForMainPlayerTick)
    self.m_WaitForMainPlayerTick = RegisterTick(function()
        if CClientMainPlayer.Inst then 
            func()
            UnRegisterTick(self.m_WaitForMainPlayerTick)
        end
    end, 1000)
end

function LuaExpressionMgr:OnUpdateMainPlayerExpressionTxt(args)
    if not CClientMainPlayer.Inst then
        self:WaitForMainPlayerCreated(function()
            CClientMainPlayer.Inst.BasicProp.ExpressionTxt = args[0]
            --CPersonalSpaceMgr.Inst:PushRoleInfo()
        end)
    else
        --CPersonalSpaceMgr.Inst:PushRoleInfo()
    end
end

function LuaExpressionMgr:RegistCheckExpired()
    UnRegisterTick(self.m_CheckExpiredTick)
    self.m_CheckExpiredTick = RegisterTick(function()
        if not CClientMainPlayer.Inst then return end
        local refresh = false
        for k, _ in pairs(self.m_ValidLimitExpressionTxtList) do
            local _refresh = self:UpdateLimitExpressionTxt(k)
            if _refresh then
                refresh = true
                if CClientMainPlayer.Inst.BasicProp.ExpressionTxt == k then 
                    CClientMainPlayer.Inst.BasicProp.ExpressionTxt = 0
                    --CPersonalSpaceMgr.Inst:PushRoleInfo()
                end
            end
        end
        if CClientMainPlayer.Inst.BasicProp.ExpressionTxt > 0 and not self.m_ValidLimitExpressionTxtList[CClientMainPlayer.Inst.BasicProp.ExpressionTxt] then
            CClientMainPlayer.Inst.BasicProp.ExpressionTxt = 0
            refresh = true
        end
        if refresh then
            EventManager.BroadcastInternalForLua(EnumEventType.QueryValidExpressionTxtResult, {})
        end
    end, 1000)
end

function LuaExpressionMgr:UnRegistCheckExpired()
    UnRegisterTick(self.m_CheckExpiredTick)
    self.m_CheckExpiredTick = nil
end

function LuaExpressionMgr:AddLimitExpressionTxt(id, expiredTime)
    if expiredTime == -1 or expiredTime > CServerTimeMgr.Inst.timeStamp then
        self.m_ValidLimitExpressionTxtList[id] = expiredTime
    end
end

function LuaExpressionMgr:UpdateLimitExpressionTxt(id)
    local expiredTime = self.m_ValidLimitExpressionTxtList[id]
    
    if expiredTime and expiredTime ~= -1 and expiredTime <= CServerTimeMgr.Inst.timeStamp then
        self.m_ValidLimitExpressionTxtList[id] = nil
        return true
    end
    return false
end

function LuaExpressionMgr:GetLimitExpressionTxt(id)
    self:UpdateLimitExpressionTxt(id)
    return self.m_ValidLimitExpressionTxtList[id]
end

LuaExpressionMgr.m_RenewalEquipApperanceId = 0
LuaExpressionMgr.m_CommonExpressionSelectionWndInfo = {}
-- includeFxExpression 表示是否包含伏羲表情动作
-- includeDoublePlayerExpression 表情是否包含双人动作
-- callback 返回参数是个table， 包含两个参数：isRegular 和 info
-- isRegular 为true时 info类型为C#的ExpressionActionInfo
-- isRegular 为false时 info类型为Lua table，定义在LuaFuxiAniMgr
function LuaExpressionMgr:ShowCommonExpressionSelectionWnd(title, includeFxExpression, includeDoublePlayerExpression, callback)
    self.m_CommonExpressionSelectionWndInfo = {}
    self.m_CommonExpressionSelectionWndInfo.title = title
    self.m_CommonExpressionSelectionWndInfo.includeFxExpression = includeFxExpression
    self.m_CommonExpressionSelectionWndInfo.includeDoublePlayerExpression = includeDoublePlayerExpression
    self.m_CommonExpressionSelectionWndInfo.callback = callback
    CUIManager.ShowUI("CommonExpressionSelectionWnd")
end

function LuaExpressionMgr:OnCommonExpressionSelectionWndCallback(data)
    if self.m_CommonExpressionSelectionWndInfo and self.m_CommonExpressionSelectionWndInfo.callback then
        self.m_CommonExpressionSelectionWndInfo.callback(data)
    end
end

function LuaExpressionMgr:GetValidityPeriodStr(data)
    local showLabel = false
    local str = ""
    if data and data.AvailableTime > 0 then
        local mainplayer = CClientMainPlayer.Inst
        if mainplayer then
            local dict = mainplayer.PlayProp.ExpressionExpireInfo
            if dict then
                local __try_get_result, time = CommonDefs.DictTryGet(dict, typeof(Byte), data.LockGroup, typeof(UInt32))
                if __try_get_result then
                    local now = CServerTimeMgr.Inst.timeStamp
                    time = time - now
                    showLabel = time > 0
                    local day, hour = math.floor(time / 86400), math.floor((time % 86400) / 3600)
                    if day > 0 then
                        str = SafeStringFormat3(LocalString.GetString("余%d天"),day) 
                    else
                        str = SafeStringFormat3(LocalString.GetString("余%d小时"),hour) 
                    end
                end
            end
        end
    end
    return str, showLabel
end

--HasGetProfileFrame的意义不太清晰，并且会过滤掉过期的头像框，为了不改变原有逻辑但更好的处理衣橱功能，增加GetProfileFrameExpireTime
function LuaExpressionMgr:GetProfieFrameExpireTime(id)
    local list = CExpressionMgr.Inst.validProfileFrameList
    for i=0,list.Count-1 do
        if math.floor(list[i][0])==id then
            return list[i][1]
        end
    end
    return 0
end
