local CBiWuDaHuiMgr=import "L10.Game.CBiWuDaHuiMgr"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local QnTableView=import "L10.UI.QnTableView"
local CClientFormula=import "L10.Game.CClientFormula"
local QnTableItem = import "L10.UI.QnTableItem"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EnumPlayScoreKey=import "L10.Game.EnumPlayScoreKey"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

CLuaPlayerScoreView = class()
RegistClassMember(CLuaPlayerScoreView,"tableView")
RegistClassMember(CLuaPlayerScoreView,"allData")
RegistClassMember(CLuaPlayerScoreView,"selectedKey")
RegistClassMember(CLuaPlayerScoreView,"m_CacheParam1")
RegistClassMember(CLuaPlayerScoreView,"m_CacheParam2")

function CLuaPlayerScoreView:Awake()
    self.tableView = self.transform:GetComponent(typeof(QnTableView))
    self.allData = {}
    --盛放这里处理的没有每次更新，UI迭代先把初始化到awake
    self.selectedKey = nil
    self:Init()
end

function CLuaPlayerScoreView:Init( )
    self.allData = {}
    --TODO 从配表读取积分信息

    local t = {}
    Shop_PlayScore.ForeachKey(function (id)
        local playScore = Shop_PlayScore.GetData(id)
        if playScore.ForbidShow == 0 and self:IsShopOpen(playScore) then
            if self:CheckDeleteTime(playScore.DeleteTime) then
                table.insert( self.allData,{
                  id = id,
                  stringKey = playScore.Key,
                  icon = playScore.ScoreIcon,
                  displayName = playScore.ScoreName,
                  TipMessageName = playScore.TipMessage,
                  showButton = playScore.ShowButton > 0,
                  deleteTime = playScore.DeleteTime,
                  canShop = playScore.CanShop>0,
                  order = playScore.Order,
                } )
            end
        end
    end)

    -- local qita = {
    --     stringKey = "Other",
    --     icon = CUIMaterialPaths.OtherScoreIcon,
    --     displayName = LocalString.GetString("其他积分"),
    --     TipMessageName = "",
    --     showButton = true
    -- }

    -- table.insert( self.allData, qita )
    table.sort(self.allData, function (a, b)
        if a.order < b.order then return true end
        if a.order > b.order then return false end
        return a.id < b.id
    end)

    self.tableView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.allData
    end,
    function(item, index)
        local canShop = self.allData[index + 1].canShop
        if canShop then
            self:InitItem(item.transform, self.allData[index + 1])
        else
            self:InitItem2(item.transform, self.allData[index + 1])
        end
    end)
    self.tableView:ReloadData(true, false)
end

--客户端提供一个临时屏蔽积分商店入口的开关，用于特殊需求的临时屏蔽处理
function CLuaPlayerScoreView:IsShopOpen(designData)
    if designData.Key == "AppearanceScore" then
        return LuaAppearancePreviewMgr.m_EnableNewAppearanceWnd or false
    elseif designData.Key == "StarBiWuScore" then
        return CommonDefs.IS_CN_CLIENT and CClientMainPlayer.Inst.ItemProp.Vip.Level >= Shop_Setting.GetData().StarBiWuScoreShopVipLimit or false
    elseif designData.Key == "ClubHouseSYLX" then
        return CommonDefs.IS_CN_CLIENT
    end
    return true
end

function CLuaPlayerScoreView:CheckDeleteTime(timeString)
  if timeString and timeString ~= '' then
    local splits = g_LuaUtil:StrSplit(timeString,"-")
    local year = tonumber(splits[1])
    local month = tonumber(splits[2])
    local day = tonumber(splits[3])

    local nowTime = CServerTimeMgr.Inst.timeStamp
    local nowTimeDate = os.date('*t',nowTime)

    local targetTimeStamp = os.time({year=year,month=month,day=day,hour=24})
    local nowTimeStamp = os.time({year=nowTimeDate.year,month=nowTimeDate.month,day=nowTimeDate.day,hour=nowTimeDate.hour})

    local difTimeStamp = targetTimeStamp - nowTimeStamp
    if difTimeStamp > 0 then
      local restDay = math.ceil(difTimeStamp/24/3600)
      return restDay
    else
      return false
    end
  else
    return true
  end
end


function CLuaPlayerScoreView:InitItem(transform,data)
    local iconTexture = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local displayNameLabel = transform:Find("DisplayNameLabel"):GetComponent(typeof(UILabel))
    local valueLabel = transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
    local m_TipButton = transform:Find("TipButton").gameObject
    local timeLabel = transform:Find('TimeLabel'):GetComponent(typeof(UILabel))
    local xianshiNode = transform:Find('Icon/xianshi').gameObject
    local tableItem = transform:GetComponent(typeof(QnTableItem))

    iconTexture:LoadMaterial(data.icon)
    displayNameLabel.text = data.displayName

    local setValue = function(val)
        valueLabel.text = tostring(math.floor(val or 0))
    end

    if data.stringKey == "BIWU" or data.stringKey == "GIFT" then--不考虑比武 功勋 赠送积分
    elseif data.stringKey == "GXZ" then
        setValue(CClientMainPlayer.Inst.PlayProp.GuildData.GuildGongXun)
    else
        local val = CClientMainPlayer.Inst.PlayProp.Scores[LuaEnumPlayScoreKey[data.stringKey]]
        setValue(val)
    end

    tableItem.OnSelected = DelegateFactory.Action_GameObject(function (go)
        self:OnItemSelected(data)
    end)

    m_TipButton:SetActive(not System.String.IsNullOrEmpty(data.TipMessageName))

    UIEventListener.Get(m_TipButton).onClick = DelegateFactory.VoidDelegate(function(go)
        if data.stringKey == "QYJF" then
            local playerLevel = 1
            if CClientMainPlayer.Inst ~= nil then
                playerLevel = CClientMainPlayer.Inst.MaxLevel
            end
            g_MessageMgr:ShowMessage(data.TipMessageName, CClientFormula.Inst:Formula_205(playerLevel))
        else
            g_MessageMgr:ShowMessage(data.TipMessageName)
        end
    end)

    if data.deleteTime and data.deleteTime ~= '' then
      timeLabel.gameObject:SetActive(true)
      local restDay = self:CheckDeleteTime(data.deleteTime)
      timeLabel.text = SafeStringFormat3(LocalString.GetString('(剩%s天)'),restDay)
      xianshiNode:SetActive(true)
    else
      timeLabel.gameObject:SetActive(false)
      xianshiNode:SetActive(false)
    end
end


function CLuaPlayerScoreView:InitItem2(transform,data)
    local iconTexture = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local displayNameLabel = transform:Find("DisplayNameLabel"):GetComponent(typeof(UILabel))
    local valueLabel = transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
    local m_TipButton = transform:Find("TipButton").gameObject
    -- local timeLabel = transform:Find('TimeLabel'):GetComponent(typeof(UILabel))
    local tableItem = transform:GetComponent(typeof(QnTableItem))
    local alert = transform:Find("Alert").gameObject

    iconTexture:LoadMaterial(data.icon)

    if data.stringKey=="BIWU" then
        displayNameLabel.text = SafeStringFormat3(LocalString.GetString("比武大会%s"),CBiWuDaHuiMgr.Inst:GetMyStageDesc())
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level > 69 then
            local record = PlayerPrefs.GetInt("mainplayerwnd_biwushop_alert")
            if record and record == 1 then
                alert:SetActive(false)
            else
                alert:SetActive(true)
            end
        end
    else
        displayNameLabel.text=data.displayName
    end


    valueLabel.text = "0"
    if data.stringKey=="BIWU" then
        Gac2Gas.QueryBiWuScore()
    elseif data.stringKey=="GIFT" then
        Gac2Gas.RequestGiftLimit()
    elseif data.stringKey=="XZS" then
        if CClientMainPlayer.Inst then
            local score = CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.XZS)
            valueLabel.text = tostring(math.floor(score or 0))
        end
    end

    tableItem.OnSelected = DelegateFactory.Action_GameObject(function (go)
        self:OnItemSelected(data)
    end)

    if data.stringKey=="BIWU" then
        m_TipButton:SetActive(true)
        UIEventListener.Get(m_TipButton).onClick = DelegateFactory.VoidDelegate(function(go)
            g_MessageMgr:ShowMessage(data.TipMessageName)
        end)
    else
        m_TipButton:SetActive(false)
    end
end

function CLuaPlayerScoreView:OnItemSelected(data)
    self.selectedKey = data.stringKey
    if self.selectedKey == nil or self.selectedKey == "" then
        return
    end

    if self.selectedKey=="GIFT" then
        g_MessageMgr:ShowMessage(data.TipMessageName, self.m_CacheParam1, self.m_CacheParam2)
        return
    elseif self.selectedKey=="XZS" then
        g_MessageMgr:ShowMessage(data.TipMessageName)
        return
    end

    self:OnExchangeButtonClick()
end

function CLuaPlayerScoreView:OnExchangeButtonClick()
    if self.selectedKey=="BIWU" then
        CUIManager.ShowUI(CUIResources.BWDHScoreWnd)
        local record = PlayerPrefs.GetInt("mainplayerwnd_biwushop_alert")
        if (not record) or record == 0 then 
            PlayerPrefs.SetInt("mainplayerwnd_biwushop_alert", 1)
            g_ScriptEvent:BroadcastInLua("OnBiWuShopAlertUpdate", false)
            self.tableView:ReloadData(true, false)
        end
        return
    end
    if self.selectedKey == "Other" then
        CUIManager.ShowUI(CUIResources.OtherScoreWnd)
    else
        Shop_PlayScore.ForeachKey(function (key)
            local playScore = Shop_PlayScore.GetData(key)
            if playScore.Key == self.selectedKey then
                CLuaNPCShopInfoMgr.ShowScoreShopById(key)
                return
            end
        end)
    end
end

function CLuaPlayerScoreView:OnEnable()
    Gac2Gas.QueryBiWuScore()
    Gac2Gas.RequestGiftLimit()
    g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate", self, "ShowScoreValue")
    g_ScriptEvent:AddListener("QueryPlayerCurrentBiWuScore", self, "OnQueryPlayerCurrentBiWuScore")
    g_ScriptEvent:AddListener("RequestGiftLimitResult", self, "OnRequestGiftLimitResult")
end
function CLuaPlayerScoreView:OnDisable()
    g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate", self, "ShowScoreValue")
    g_ScriptEvent:RemoveListener("QueryPlayerCurrentBiWuScore", self, "OnQueryPlayerCurrentBiWuScore")
    g_ScriptEvent:RemoveListener("RequestGiftLimitResult", self, "OnRequestGiftLimitResult")

end


function CLuaPlayerScoreView:OnQueryPlayerCurrentBiWuScore(args)
    for i,v in ipairs(self.allData) do
        if v.stringKey =="BIWU" then
            local item = self.tableView:GetItemAtRow(i-1)
            -- if item then
                local valueLabel = item.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
                valueLabel.text = tostring(args[0])
            -- end
            break
        end
    end
end

function CLuaPlayerScoreView:OnRequestGiftLimitResult(args)
    for i,v in ipairs(self.allData) do
        if v.stringKey =="GIFT" then
            local item = self.tableView:GetItemAtRow(i-1)
            -- if item then
                self.m_CacheParam1=args[1]
                self.m_CacheParam2=args[2]

                local valueLabel = item.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
                valueLabel.text = tostring(args[0])
            -- end
            break
        end
    end
end

function CLuaPlayerScoreView:ShowScoreValue()
    for i=1,self.tableView.m_ItemList.Count do
        local item = self.tableView.m_ItemList[i-1]
        local valueLabel = item.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))

        local data = self.allData[i]

        local setValue = function(val)
            valueLabel.text = tostring(math.floor(val))
        end

        if data.stringKey == "GIFT" then--不考虑 功勋 赠送积分
        elseif data.stringKey == "BIWU" then
            local stage = CBiWuDaHuiMgr.Inst:GetMyStage()
            local key = "BiWuStage"..stage
            local val = CClientMainPlayer.Inst.PlayProp.Scores[LuaEnumPlayScoreKey[key]] or 0
            valueLabel.text = tostring(math.floor(val))
        elseif data.stringKey == "GXZ" then
            local val = CClientMainPlayer.Inst.PlayProp.GuildData.GuildGongXun
            valueLabel.text = tostring(math.floor(val))
        else
            local val = CClientMainPlayer.Inst.PlayProp.Scores[LuaEnumPlayScoreKey[data.stringKey]]
            valueLabel.text = tostring(math.floor(val))
        end
    end
end
