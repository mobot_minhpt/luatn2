local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CShiTuMgr = import "L10.Game.CShiTuMgr"
local MsgPackImpl = import "MsgPackImpl"
local CFamilyTreeMgr = import "L10.Game.CFamilyTreeMgr"
local CIMMgr = import "L10.Game.CIMMgr"

LuaShiTuTrainingHandbookMgr = class()
LuaShiTuTrainingHandbookMgr.m_IsShowForTeacher = false
LuaShiTuTrainingHandbookMgr.wndtype = {
    TrainingPlan = 1,WeeklyHomework = 2,ZunShiZhongDao = 3
}
LuaShiTuTrainingHandbookMgr.m_ScoreDict = nil
LuaShiTuTrainingHandbookMgr.m_ShifuPlayerId = 0
LuaShiTuTrainingHandbookMgr.m_TudiPlayerId = 0
LuaShiTuTrainingHandbookMgr.m_TudiSortedList = {}

function LuaShiTuTrainingHandbookMgr:OpenWnd(wnd, data)
    local list = MsgPackImpl.unpack(data)
    if list.Count ~= 1 then return end
    local tudiPlayerId = list[0]
    if not CShiTuMgr.Inst:IsCurrentTuDi(tudiPlayerId) then return end
    self.m_IsShowForTeacher = true
    self.m_ShifuPlayerId = CClientMainPlayer.Inst.Id
    if LuaShiTuMgr.m_OpenNewSystem then
        self.m_TudiPlayerId = 0
        if self:IsWaiMen() then
            g_MessageMgr:ShowMessage("ShiTuView_WaiMenDiZi_Forbid_ShowWenDaoView")
            return
        end
        CUIManager.CloseUI(CLuaUIResources.ShiTuNewTrainingHandbookWnd)
        CUIManager.ShowUI(CLuaUIResources.ShiTuNewTrainingHandbookWnd)
        return
    end

    self.m_TudiPlayerId = tudiPlayerId
    CUIManager.ShowUI(CLuaUIResources.ShiTuTrainingHandbookWnd)
end

LuaShiTuTrainingHandbookMgr.m_TuDiPlayerListWndBtnText = ""
LuaShiTuTrainingHandbookMgr.m_TuDiPlayerListWndBtnAction = nil
function LuaShiTuTrainingHandbookMgr:ShowTuDiPlayerListWnd(btnText, btnAction)
    self.m_TuDiPlayerListWndBtnText = btnText
    self.m_TuDiPlayerListWndBtnAction = btnAction
    CUIManager.ShowUI(CLuaUIResources.TuDiPlayerListWnd)
end

function LuaShiTuTrainingHandbookMgr:ShowWndForTeacher(tudiPlayerId)
    if not CClientMainPlayer.Inst then return end
    if CClientMainPlayer.Inst.IsCrossServerPlayer then
        g_MessageMgr:ShowMessage("CROSS_SERVER_NOT_USE_THE_FEATURE")
        return
    end

    if not LuaShiTuMgr.m_OpenNewSystem  then
        Gac2Gas.ShiTuCultivateShiFuChooseTudi(tudiPlayerId)
        return
    end

    self.m_IsShowForTeacher = true
    self.m_ShifuPlayerId = CClientMainPlayer.Inst.Id
    self.m_TudiPlayerId = 0
    if self:IsWaiMen() then
        g_MessageMgr:ShowMessage("ShiTuView_WaiMenDiZi_Forbid_ShowWenDaoView")
        return
    end
    CUIManager.CloseUI(CLuaUIResources.ShiTuNewTrainingHandbookWnd)
    CUIManager.ShowUI(CLuaUIResources.ShiTuNewTrainingHandbookWnd)
end

function LuaShiTuTrainingHandbookMgr:ShowWndForStudent(shifuPlayerId)
    if not CClientMainPlayer.Inst then return end
    if CClientMainPlayer.Inst.IsCrossServerPlayer then
        g_MessageMgr:ShowMessage("CROSS_SERVER_NOT_USE_THE_FEATURE")
        return
    end
    if shifuPlayerId == nil then
        shifuPlayerId = CShiTuMgr.Inst:GetCurrentShiFuId()
    end
    if CShiTuMgr.Inst:IsCurrentShiFu(shifuPlayerId) then
        self.m_IsShowForTeacher = false
        self.m_ShifuPlayerId = shifuPlayerId
        if LuaShiTuMgr.m_OpenNewSystem  then
            self.m_TudiPlayerId = 0
            if self:IsWaiMen() then
                g_MessageMgr:ShowMessage("ShiTuView_WaiMenDiZi_Forbid_ShowWenDaoView")
                return
            end
            CUIManager.CloseUI(CLuaUIResources.ShiTuNewTrainingHandbookWnd)
            CUIManager.ShowUI(CLuaUIResources.ShiTuNewTrainingHandbookWnd)
        else
            self.m_TudiPlayerId = CClientMainPlayer.Inst.Id
            CUIManager.ShowUI(CLuaUIResources.ShiTuTrainingHandbookWnd)
        end
    end
end

function LuaShiTuTrainingHandbookMgr:GetShifuPlayerId()
    return self.m_ShifuPlayerId
end

function LuaShiTuTrainingHandbookMgr:GetTudiPlayerId()
    return self.m_TudiPlayerId
end

function LuaShiTuTrainingHandbookMgr:IsTudiRuShi()
    local info = self:GetShiTuInfo()
    return info and info.Extra:GetBit(1) or false
end

function LuaShiTuTrainingHandbookMgr:IsWaiMen()
    local info = self:GetShiTuInfo()
    return info and info.Extra:GetBit(2) or false
end

function LuaShiTuTrainingHandbookMgr:GetPartnerPlayerId()
    local playerId = self.m_IsShowForTeacher and self.m_TudiPlayerId or self.m_ShifuPlayerId
    if playerId == 0 and CClientMainPlayer.Inst then
        if self.m_IsShowForTeacher and CClientMainPlayer.Inst.RelationshipProp.TuDi.Count > 0 then
            playerId = 0
            local find = false
            CommonDefs.DictIterate(CClientMainPlayer.Inst.RelationshipProp.TuDi, DelegateFactory.Action_object_object(function(key,val)
                if not val.Extra:GetBit(2) and not find and val.Finished == 0 then
                    find = true
                    playerId = key
                end
            end))
        elseif CClientMainPlayer.Inst.RelationshipProp.ShiFu.Count > 0 then
            playerId = CShiTuMgr.Inst:GetCurrentShiFuId()
        end 
    end
    return playerId
end

function LuaShiTuTrainingHandbookMgr:IsShowForTeacher()
    return self.m_IsShowForTeacher
end

function LuaShiTuTrainingHandbookMgr:GetPlayScore(scoreKey)
    if self.m_ScoreDict == nil then
        self.m_ScoreDict = {}
        Shop_PlayScore.ForeachKey(function (key)
            local playScore = Shop_PlayScore.GetData(key)
            if (playScore.Key == ToStringWrap(EnumPlayScoreKey.GIFT)) or (playScore.Key == ToStringWrap(EnumPlayScoreKey.QYJF)) then
                self.m_ScoreDict[playScore.Key] = playScore
            end
        end)
    end
    return self.m_ScoreDict[ToStringWrap(scoreKey)]
end

function LuaShiTuTrainingHandbookMgr:OnWndClose()
    self.m_ScoreDict = nil
    self.m_TudiSortedList = {}
end

function LuaShiTuTrainingHandbookMgr:GetShiTuInfo()
    if CClientMainPlayer.Inst then
        local relationshipProp = CClientMainPlayer.Inst.RelationshipProp
        local infoDic = LuaShiTuTrainingHandbookMgr:IsShowForTeacher() and relationshipProp.TuDi or relationshipProp.ShiFu
        local info = CommonDefs.DictGetValue(infoDic, typeof(UInt64), self:GetPartnerPlayerId())
        return info
    end
end

LuaShiTuTrainingHandbookMgr.m_WenDaoRewardWndLevel = 0
LuaShiTuTrainingHandbookMgr.m_WenDaoRewardWndCurrentStage = 0
function LuaShiTuTrainingHandbookMgr:ShowWenDaoRewardWnd(level, currentStage)
    self.m_WenDaoRewardWndLevel = level
    self.m_WenDaoRewardWndCurrentStage = currentStage
    local hasData = false
    ShiTuCultivate_CultivateNewStage.Foreach(function (k,v)
		if v.Level == level and v.StageId == self.m_WenDaoStage then
			hasData = true
		end
	end)
    if not hasData then
        return
    end
    CUIManager.ShowUI(CLuaUIResources.ShiTuWenDaoRewardWnd)
end

LuaShiTuTrainingHandbookMgr.m_WenDaoInfo = {}
LuaShiTuTrainingHandbookMgr.m_WenDaoStage = 1
function LuaShiTuTrainingHandbookMgr:SendNewShiTuWenDaoInfo(isShiFu, otherPlayerId, currentStage, tudiInfoUd, wendaoInfoUd)
    LuaShiTuTrainingHandbookMgr.m_WenDaoInfo = {}
    LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.isShiFu = isShiFu
    LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.otherPlayerId = otherPlayerId
    LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.currentStage = currentStage
    local tudiInfo = tudiInfoUd and g_MessagePack.unpack(tudiInfoUd) or {}
    self.m_TudiSortedList = {}
    for tudiId, info in pairs(tudiInfo) do
        local tudiName = info.playerName
        local tudiTime = info.tudiTime
        local gender = info.gender
        local class = info.class
        local level = info.level
        local isFeiSheng = info.isFeiSheng
        local isBest = info.isBest
        local canSelect = info.canSelect
        table.insert(self.m_TudiSortedList, {
            tudiName = tudiName, tudiTime = tudiTime, gender = gender, class = class,
            level = level, isFeiSheng = isFeiSheng, isBest = isBest, tudiId = tudiId,
            canSelect = canSelect
        })
    end
    table.sort(self.m_TudiSortedList,function(a,b)
        return a.tudiTime < b.tudiTime
    end)

    local info = wendaoInfoUd and g_MessagePack.unpack(wendaoInfoUd) or {}
    LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.wendaoInfo = {}
    for stage, stageInfo in pairs(info) do
        local data = ShiTuCultivate_CultivateNewStage.GetData(stage)
        LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.wendaoInfo[data.StageId] = stageInfo
    end
    g_ScriptEvent:BroadcastInLua("SendNewShiTuWenDaoInfo")
end