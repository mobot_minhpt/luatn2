local DelegateFactory  = import "DelegateFactory"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CQnSymbolParser = import "CQnSymbolParser"


LuaMengQuanHengXingSignUpWnd = class()
LuaMengQuanHengXingSignUpWnd.s_OpenWndWithInfo = nil

function LuaMengQuanHengXingSignUpWnd:Awake()
    self:initGameplayData()
    self:InitUI()
    self:InitSendMengQuanHengXingPlayTimes()
end

function LuaMengQuanHengXingSignUpWnd:initGameplayData()
    self.MQHXConfigData = Double11_MQHX.GetData()
    self.gameplayId = self.MQHXConfigData.PlayDesignId
    self.scoreRewardLimit = self.MQHXConfigData.ScoreRewardTimes
    self.joinRewardLimit = self.MQHXConfigData.JoinRewardTimes
    self.isMatching = false

    if self.gameplayId and CClientMainPlayer.Inst then
        Gac2Gas.GlobalMatch_RequestCheckSignUp(self.gameplayId, CClientMainPlayer.Inst.Id)
    end
end

function LuaMengQuanHengXingSignUpWnd:InitUI()
    self.ruleButton = self.transform:Find("Anchor/BossView/Bottom/GameObject/RuleButton")
    self.playRewardTimeText = self.transform:Find("Anchor/BossView/Bottom/GameObject/Count/Cnt1"):GetComponent(typeof(UILabel))
    self.challengeRewardTimeText = self.transform:Find("Anchor/BossView/Bottom/GameObject/Count/Cnt2"):GetComponent(typeof(UILabel))
    self.hintText = self.transform:Find("Anchor/BossView/Bottom/GameObject/Hint"):GetComponent(typeof(UILabel))
    self.dogeTexture = self.transform:Find("Anchor/BossView/DogeTexture"):GetComponent(typeof(UITexture))
    self.timeInfoText = self.transform:Find("Anchor/InfoView/Content/Time/Info"):GetComponent(typeof(UILabel))
    self.requireLevelText = self.transform:Find("Anchor/InfoView/Content/Need/Info"):GetComponent(typeof(UILabel))
    self.taskDescribeText = self.transform:Find("Anchor/InfoView/Content/Desc/Info"):GetComponent(typeof(UILabel))
    self.rewardItem1 = self.transform:Find("Anchor/InfoView/Content/Reward/Item1")
    self.rewardItem2 = self.transform:Find("Anchor/InfoView/Content/Reward/Item2")
    self.rewardItem3 = self.transform:Find("Anchor/InfoView/Content/Reward/Item3")
    self.enterButton = self.transform:Find("Anchor/InfoView/Content/EnterButton")
    self.matchingMark = self.transform:Find("Anchor/InfoView/Content/EnterButton/MatchingMark")
    self.enterButtonLabel = self.transform:Find("Anchor/InfoView/Content/EnterButton/Label"):GetComponent(typeof(UILabel))
    
    self.hintText.text = LocalString.GetString(self.MQHXConfigData.MQHXHint)
    self.taskDescribeText.text = LocalString.GetString(self.MQHXConfigData.MQHXTaskDisc)
    self.timeInfoText.text = CQnSymbolParser.ConvertQnTextToNGUIText(LocalString.GetString(self.MQHXConfigData.MQHXTime))
    self.requireLevelText.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(self.MQHXConfigData.LevelLimit))
    
    self:InitOneItem(self.rewardItem1.gameObject, self.MQHXConfigData.JoinRewardItemId[0])
    self:InitOneItem(self.rewardItem2.gameObject, self.MQHXConfigData.JoinRewardItemId[1])
    self:InitOneItem(self.rewardItem3.gameObject, self.MQHXConfigData.ScoreRewardItemId)
    
    UIEventListener.Get(self.enterButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        if self.isMatching then
            Gac2Gas.GlobalMatch_RequestCancelSignUp(self.gameplayId)
        else
            Gac2Gas.GlobalMatch_RequestSignUp(self.gameplayId)
        end
    end)

    UIEventListener.Get(self.ruleButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("Double11_MengQuanHengXing_Tips")
    end)
end

function LuaMengQuanHengXingSignUpWnd:Init()
    g_ScriptEvent:BroadcastInLua("Double11MQHX_OpenSignUpWnd")
end

function LuaMengQuanHengXingSignUpWnd:Start()

end

function LuaMengQuanHengXingSignUpWnd:OnEnable()
    g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResultWithInfo", self, "OnGlobalMatch_CheckInMatchingResultWithInfo")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
    g_ScriptEvent:AddListener("SyncMengQuanHengXingRankInfo", self, "OnCloseSignUpWnd")
end

function LuaMengQuanHengXingSignUpWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResultWithInfo", self, "OnGlobalMatch_CheckInMatchingResultWithInfo")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
    g_ScriptEvent:RemoveListener("SyncMengQuanHengXingRankInfo", self, "OnCloseSignUpWnd")
end

function LuaMengQuanHengXingSignUpWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end
    UIEventListener.Get(curItem).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
end

function LuaMengQuanHengXingSignUpWnd:InitSendMengQuanHengXingPlayTimes()
    local joinPlayTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eDouble11MQHXJoinRewardTimes)
    local scoreRewardTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eDouble11MQHXScoreRewardTimes)

    self.playRewardTimeText.text = self.joinRewardLimit - joinPlayTimes .. LocalString.GetString("次")
    self.challengeRewardTimeText.text = self.scoreRewardLimit - scoreRewardTimes .. LocalString.GetString("次")
    self.playRewardTimeText.color = NGUIText.ParseColor((self.joinRewardLimit - joinPlayTimes) > 0 and "ffffff" or "ff5050", 0)
    self.challengeRewardTimeText.color = NGUIText.ParseColor((self.scoreRewardLimit - scoreRewardTimes) > 0 and "ffffff" or "ff5050", 0)
end

function LuaMengQuanHengXingSignUpWnd:OnState(isMatching)
    self.isMatching = isMatching
    self.matchingMark.gameObject:SetActive(isMatching)

    local btnSp = self.enterButton:GetComponent(typeof(UISprite))
    if not isMatching then
        self.enterButtonLabel.text = LocalString.GetString("报名匹配")
        btnSp.spriteName = "common_btn_01_yellow"
    else
        self.enterButtonLabel.text = LocalString.GetString("取消匹配")
        btnSp.spriteName = "common_btn_01_blue"
    end
end

function LuaMengQuanHengXingSignUpWnd:OnGlobalMatch_CheckInMatchingResultWithInfo(playerId, playId, isInMatching, resultStr, info)
    if CClientMainPlayer.Inst == nil or playerId ~= CClientMainPlayer.Inst.Id or playId ~= self.gameplayId then
        return
    end

    self:OnState(isInMatching)
end

function LuaMengQuanHengXingSignUpWnd:OnGlobalMatch_SignUpPlayResult(playId, success)
    if success and playId == self.gameplayId then
        self:OnState(true)
    end
end

function LuaMengQuanHengXingSignUpWnd:OnGlobalMatch_CancelSignUpResult(playId, success)
    if success and playId == self.gameplayId then
        self:OnState(false)
    end
end

function LuaMengQuanHengXingSignUpWnd:OnCloseSignUpWnd()
    CUIManager.CloseUI(CLuaUIResources.MengQuanHengXingSignUpWnd)
    CUIManager.CloseUI(CLuaUIResources.Shuangshiyi2022MainWnd)
end 