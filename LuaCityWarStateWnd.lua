require("common/common_include")

local UILabel = import "UILabel"
local UISprite = import "UISprite"
local UISlider = import "UISlider"
local CCityWarMgr = import "L10.Game.CCityWarMgr"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local Extensions = import "Extensions"
local EUIModuleGroup = import "L10.UI.CUIManager+EUIModuleGroup"

CLuaCityWarStateWnd = class()
CLuaCityWarStateWnd.Path = "ui/citywar/LuaCityWarStateWnd"

RegistClassMember(CLuaCityWarStateWnd, "m_MyForce")
RegistClassMember(CLuaCityWarStateWnd, "m_MyProgressSpriteTable")
RegistClassMember(CLuaCityWarStateWnd, "m_MyProgressSliderTable")
RegistClassMember(CLuaCityWarStateWnd, "m_OtherProgressSpriteTable")
RegistClassMember(CLuaCityWarStateWnd, "m_OtherProgressSliderTable")
RegistClassMember(CLuaCityWarStateWnd, "m_MyProgressForeSpriteTable")
RegistClassMember(CLuaCityWarStateWnd, "m_OtherProgressForeSpriteTable")
RegistClassMember(CLuaCityWarStateWnd, "m_FullProgress")
RegistClassMember(CLuaCityWarStateWnd, "m_ConstructBtn")
RegistClassMember(CLuaCityWarStateWnd, "m_ExpandButtonTrans")
RegistClassMember(CLuaCityWarStateWnd, "m_WarRootObjTable")

function CLuaCityWarStateWnd:Awake( ... )
	self.m_MyProgressSliderTable = {}
	self.m_MyProgressSpriteTable = {}
	self.m_OtherProgressSliderTable = {}
	self.m_OtherProgressSpriteTable = {}
	self.m_MyProgressForeSpriteTable = {}
	self.m_OtherProgressForeSpriteTable = {}
	self.m_WarRootObjTable = {}
	local transNameTable = {"First", "Second", "Third"}
	for i = 1, 3 do
		table.insert(self.m_WarRootObjTable, self.transform:Find("Anchor/"..transNameTable[i]).gameObject)
		local myTrans = self.transform:Find("Anchor/"..transNameTable[i].."/MyProgress")
		local otherTrans = self.transform:Find("Anchor/"..transNameTable[i].."/OtherProgress")
		local curSceneObj = self.transform:Find("Anchor/"..transNameTable[i].."/CurSceneFlag").gameObject
		local otherSceneObj = self.transform:Find("Anchor/"..transNameTable[i].."/OtherSceneFlag").gameObject
		table.insert(self.m_MyProgressSpriteTable, myTrans:GetComponent(typeof(UISprite)))
		table.insert(self.m_MyProgressSliderTable, myTrans:GetComponent(typeof(UISlider)))
		table.insert(self.m_MyProgressForeSpriteTable, myTrans:Find("Foreground"):GetComponent(typeof(UISprite)))
		table.insert(self.m_OtherProgressSpriteTable, otherTrans:GetComponent(typeof(UISprite)))
		table.insert(self.m_OtherProgressSliderTable, otherTrans:GetComponent(typeof(UISlider)))
		table.insert(self.m_OtherProgressForeSpriteTable, otherTrans:Find("Foreground"):GetComponent(typeof(UISprite)))
		curSceneObj:SetActive(CLuaCityWarMgr.WarIndex == i)
		otherSceneObj:SetActive(CLuaCityWarMgr.WarIndex ~= i)
		self.m_WarRootObjTable[i]:SetActive(false)
	end

	self.m_ConstructBtn = self.transform:Find("Anchor/ConstructionBtn").gameObject

	--需求有变，原来是自己是蓝方，现在改成进攻方是蓝方
	--self.m_MyForce = CForcesMgr.Inst:GetPlayerForce(CClientMainPlayer.Inst.Id)
	self.m_MyForce = 1
	self.m_FullProgress = CLuaCityWarMgr.MirrorWarHuFuFullProgress

	self:UpdateMirrorWarForceInfo()
	self:InitOccupyInfo()

	self.m_ExpandButtonTrans = self.transform:Find("Anchor/ExpandButton")
	UIEventListener.Get(self.m_ExpandButtonTrans.gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		Extensions.SetLocalRotationZ(self.m_ExpandButtonTrans, 180)
		CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
	end)
	UIEventListener.Get(self.m_ConstructBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
		if CLuaCityWarMgr:IsInWatchScene() then
			g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("观战中无法进行此操作"))
			return
		end
		CCityWarMgr.Inst.PlaceMode = true

		CameraFollow.Inst:SetMaximumPinchIn(false,false)

		-- CameraFollow.Inst:CityWarAdjustCombatVehicleCamera()

		local excepts = {"MiddleNoticeCenter","GuideWnd", "PlaceCityUnitWnd"}
		local List_String = MakeGenericClass(List,cs_string)
		CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
		CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)
		CUIManager.ShowUI(CLuaUIResources.PlaceCityUnitWnd)

		local bIsInCopyScene = CCityWarMgr.Inst:IsInCityWarCopyScene()
		if bIsInCopyScene then
			Gac2Gas.CheckMyRights("RebornPoint", 0)
		else
			Gac2Gas.CheckMyRights("CityBuild", 0)
		end
	end)
end

function CLuaCityWarStateWnd:InitOccupyInfo( ... )
	for k, v in pairs(CLuaCityWarMgr.WarOccupyInfo) do
		self:UpdateMirrorWarOccupyInfo(math.floor(k / 10), k % 10, v.Activated, v.Progress, v.OccupyForce)
	end
end

function CLuaCityWarStateWnd:UpdateMirrorWarForceInfo( ... )
	if #CLuaCityWarMgr.WarForceInfo <= 0 then return end
	local warNum = CLuaCityWarMgr:GetCityWarCopyNum(CLuaCityWarMgr.WarForceInfo[1].MapId, CLuaCityWarMgr.WarForceInfo[2].MapId)
	local myGuildName = self.m_MyForce == CLuaCityWarMgr.WarForceInfo[1].Force and CLuaCityWarMgr.WarForceInfo[1].GuildName or CLuaCityWarMgr.WarForceInfo[2].GuildName
	local otherGuildName = self.m_MyForce ~= CLuaCityWarMgr.WarForceInfo[1].Force and CLuaCityWarMgr.WarForceInfo[1].GuildName or CLuaCityWarMgr.WarForceInfo[2].GuildName
	self.transform:Find("Anchor/Title/MyGuildName"):GetComponent(typeof(UILabel)).text = myGuildName
	self.transform:Find("Anchor/Title/OtherGuildName"):GetComponent(typeof(UILabel)).text = otherGuildName
	local transNameTable = {"First", "Second", "Third"}
	for i = 1, 3 do
		local curSceneObj = self.transform:Find("Anchor/"..transNameTable[i].."/CurSceneFlag").gameObject
		local otherSceneObj = self.transform:Find("Anchor/"..transNameTable[i].."/OtherSceneFlag").gameObject
		curSceneObj:SetActive(CLuaCityWarMgr.WarIndex == i)
		otherSceneObj:SetActive(CLuaCityWarMgr.WarIndex ~= i)
		self.m_WarRootObjTable[i]:SetActive(i <= warNum)
	end
end

function CLuaCityWarStateWnd:UpdateMirrorWarOccupyInfo(warIndex, force, activated, progress, occupyForce)
	if force == self.m_MyForce then
		self.m_MyProgressSpriteTable[warIndex].alpha = activated and 1.0 or 0.5
		self.m_MyProgressSliderTable[warIndex].value = progress / self.m_FullProgress
		self.m_MyProgressForeSpriteTable[warIndex].spriteName = occupyForce == self.m_MyForce and "common_hpandmp_mp" or "common_hpandmp_hp"
	else
		self.m_OtherProgressSliderTable[warIndex].value = progress / self.m_FullProgress
		self.m_OtherProgressSliderTable[warIndex].alpha = activated and 1.0 or 0.5
		self.m_OtherProgressForeSpriteTable[warIndex].spriteName = occupyForce == self.m_MyForce and "common_hpandmp_mp" or "common_hpandmp_hp"
	end
end

function CLuaCityWarStateWnd:HideTopAndRightTipWnd( ... )
	Extensions.SetLocalRotationZ(self.m_ExpandButtonTrans, 0)
end

function CLuaCityWarStateWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("UpdateMirrorWarOccupyInfo", self, "UpdateMirrorWarOccupyInfo")
	g_ScriptEvent:AddListener("UpdateMirrorWarForceInfo", self, "UpdateMirrorWarForceInfo")
	g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "HideTopAndRightTipWnd")
end

function CLuaCityWarStateWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("UpdateMirrorWarOccupyInfo", self, "UpdateMirrorWarOccupyInfo")
	g_ScriptEvent:RemoveListener("UpdateMirrorWarForceInfo", self, "UpdateMirrorWarForceInfo")
	g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "HideTopAndRightTipWnd")
end
