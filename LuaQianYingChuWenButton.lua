local DelegateFactory = import "DelegateFactory"
local Vector4         = import "UnityEngine.Vector4"
local CUIFxPaths      = import "L10.UI.CUIFxPaths"

LuaQianYingChuWenButton = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaQianYingChuWenButton, "fx")
RegistClassMember(LuaQianYingChuWenButton, "icon")

function LuaQianYingChuWenButton:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self.icon = self.transform:Find("Icon"):GetComponent(typeof(UIWidget))

    UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClick()
    end)
end

function LuaQianYingChuWenButton:OnEnable()
    g_ScriptEvent:AddListener("SyncBeginnerGuideInfo", self, "OnSyncBeginnerGuideInfo")
end

function LuaQianYingChuWenButton:OnDisable()
    g_ScriptEvent:RemoveListener("SyncBeginnerGuideInfo", self, "OnSyncBeginnerGuideInfo")
end

function LuaQianYingChuWenButton:OnSyncBeginnerGuideInfo()
    self:Init()
end


function LuaQianYingChuWenButton:Init(args)
    if args then
        self.fx = args[0]
    end
    if not self.fx then return end

    if self:HasTaskCanSubmit() then
        if not self.fx.FxGo then
            self.fx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)

            local gap = 5
            CUIFx.DoAni(Vector4(- self.icon.width * 0.5 + gap, self.icon.height * 0.5 - gap,
                self.icon.width - gap * 2, self.icon.height - gap * 2), true, self.fx)
        end
    else
        self.fx:DestroyFx()
    end
end

function LuaQianYingChuWenButton:Update()
    if not self.fx then return end

    self.fx.transform.position = self.transform.position
end

-- 有奖励可以领取
function LuaQianYingChuWenButton:HasTaskCanSubmit()
    if LuaQianYingChuWenMgr:IsAllTaskFinish() then
        return true
    end

    local day2Finish = LuaQianYingChuWenMgr:GetDay2Finish()
    for i = 1, 7 do
        if day2Finish[i] == EnumBeginnerGuideDayStatus.eAccept then
            local firsttaskInfo = LuaQianYingChuWenMgr:GetTaskInfo(i)[1]
            if firsttaskInfo and firsttaskInfo.status == EnumBeginnerGuideTaskStatus.eCanSubmit then
                return true
            end
        end
    end
    return false
end

--@region UIEvent

function LuaQianYingChuWenButton:OnClick()
    LuaQianYingChuWenMgr:OpenQianYingChuWenWnd()
end

--@endregion UIEvent
