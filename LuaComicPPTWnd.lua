local CUIFx = import "L10.UI.CUIFx"


local TweenAlpha = import "TweenAlpha"
local CUITexture = import "L10.UI.CUITexture"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"

LuaComicPPTWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaComicPPTWnd, "Pic2", "Pic2", TweenAlpha)
RegistChildComponent(LuaComicPPTWnd, "Pic1", "Pic1", TweenAlpha)
RegistChildComponent(LuaComicPPTWnd, "UnderFxNode", "UnderFxNode", CUIFx)

--@endregion RegistChildComponent end
RegistClassMember(LuaComicPPTWnd, "m_ComicId")
RegistClassMember(LuaComicPPTWnd, "m_ComicDuration")
RegistClassMember(LuaComicPPTWnd, "m_CloseEnable")
RegistClassMember(LuaComicPPTWnd, "m_DurationTick")
RegistClassMember(LuaComicPPTWnd, "m_CurPicName")
RegistClassMember(LuaComicPPTWnd, "m_NextPicName")
RegistClassMember(LuaComicPPTWnd, "m_Texture1")
RegistClassMember(LuaComicPPTWnd, "m_Texture2")
RegistClassMember(LuaComicPPTWnd, "m_RootPath")
RegistClassMember(LuaComicPPTWnd, "m_CurTextureIndex")
RegistClassMember(LuaComicPPTWnd, "m_PicNames")
RegistClassMember(LuaComicPPTWnd, "m_UIFxList")
function LuaComicPPTWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_CloseEnable = false
    self.m_CurTextureIndex = 0
end

function LuaComicPPTWnd:Init()
    self.m_ComicId = LuaSeaFishingMgr.m_PlayCommicId
    self.m_ComicDuration = LuaSeaFishingMgr.m_ComicDuration and LuaSeaFishingMgr.m_ComicDuration or 0
    local data = ShowPicture_ShowPicture.GetData(self.m_ComicId)
    local setting = ShowPicture_Setting.GetData()
    if not data or not setting then
        self.m_CloseEnable = true
        return
    end
    self.m_RootPath = setting.PictureRootPath
    self.m_PicNames = data.Prefab
    self.m_UIFxList = data.UIFx
    local count = self.m_PicNames.Length
    local interval = self.m_ComicDuration / count
    if self.m_DurationTick ~= nil then
        UnRegisterTick(self.m_DurationTick)
        self.m_DurationTick = nil
    end

    self.m_Texture1 = self.Pic1.transform:GetComponent(typeof(CUITexture))
    self.m_Texture2 = self.Pic2.transform:GetComponent(typeof(CUITexture))
    self:ShowNextPic()

    self.m_DurationTick = RegisterTickWithDuration(function ()
        self:ShowNextPic()
    end,interval*1000,self.m_ComicDuration*1000)
end

function LuaComicPPTWnd:ShowNextPic()
    if self.m_CurTextureIndex >= self.m_PicNames.Length then
        self.m_CloseEnable = true
        self:CheckAndAutoClose()
        return
    end

    local tex = self.m_PicNames[self.m_CurTextureIndex]
    local path = self.m_RootPath..tex..".mat"

    local duration = self.m_ComicDuration / self.m_PicNames.Length
    local fxId = self.m_UIFxList[self.m_CurTextureIndex]
    if fxId ~= 0 then
        --这个特效特殊处理一下 要放在图片的下面
        if fxId == 89000101 and self.m_ComicId == 2 then
            self.UnderFxNode:LoadFx("fx/ui/prefab/UI_haidiao_yuguang.prefab")
        else
            CCommonUIFxWnd.AddUIFx(fxId, duration, true, false)
        end
    end
    if self.m_CurTextureIndex == 0 then--播放第一张
        self.m_Texture1:LoadMaterial(path)
        self.Pic1.from = 0
        self.Pic1.to = 1

        self.Pic1:ResetToBeginning()
        self.Pic1:PlayForward()
    else
        local topPic,bottomPic
        if self.m_Texture1.alpha == 0 then
            topPic = 2
            bottomPic = 1
        else
            topPic = 1
            bottomPic = 2
        end
        local topTween = self["Pic"..topPic]
        local bottomTween = self["Pic"..bottomPic]
        local topTexture = self["m_Texture"..topPic]
        local bottomTexture = self["m_Texture"..bottomPic]
        bottomTexture:LoadMaterial(path)
        bottomTween.from = 0
        bottomTween.to = 1
        --bottomTween.duration = duration
        bottomTween:ResetToBeginning()
        bottomTween:PlayForward()

        topTween.from = 1
        topTween.to = 0
        --topTween.duration = duration
        topTween:ResetToBeginning()
        topTween:PlayForward()
    end
    self.m_CurTextureIndex = self.m_CurTextureIndex + 1
end

function LuaComicPPTWnd:Update()
    self:ClickThroughToClose()
end

function LuaComicPPTWnd:OnDisable()
    if self.m_DurationTick ~= nil then
        UnRegisterTick(self.m_DurationTick)
        self.m_DurationTick = nil
    end
end

function LuaComicPPTWnd:ClickThroughToClose()
    if Input.GetMouseButtonDown(0) and self.m_CloseEnable then    
        CUIManager.CloseUI(CLuaUIResources.ComicPPTWnd)
    end
end

function LuaComicPPTWnd:CheckAndAutoClose()
    local data = ShowPicture_ShowPicture.GetData(self.m_ComicId)
    if data and data.IsAutoClose == 1 then
        local duration = self.m_ComicDuration / self.m_PicNames.Length
        if self.m_CloseTick then
            UnRegisterTick(self.m_CloseTick)
            self.m_CloseTick = nil
        end
        self.m_CloseTick = RegisterTickOnce(function ()
            CUIManager.CloseUI(CLuaUIResources.ComicPPTWnd)
        end,duration*1000)
    end
end

--@region UIEvent

--@endregion UIEvent

