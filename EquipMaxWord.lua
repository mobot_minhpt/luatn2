EnumEquipType2TCType = {
    NormalEquip = 1,
    Talisman = 2,
    BackPendant = 3,
    HiddenWeapon = 4,
    Beaded = 5,
}


function GetEquipWordMaxTC(equipType,equipTC,UpdeviationList,colorDeviation)
    local WordTC
    local TCType = EnumEquipType2TCType[equipType]
    if not TCType then return 1 end

    if TCType == 2 then
        WordTC = equipTC * ((100 + UpdeviationList[TCType]) / 100)
    elseif TCType == 3 then
        WordTC = equipTC * ((100 + UpdeviationList[TCType]) / 100)
    elseif TCType == 4 then
        local maxDeviation = (100 + colorDeviation) / 100
        WordTC = equipTC * ((100 + UpdeviationList[TCType]) * maxDeviation / 100)
    elseif TCType == 5 then
        local maxDeviation = (100 + colorDeviation) / 100
        WordTC = equipTC * ((100 + UpdeviationList[TCType]) * maxDeviation / 100)
    else
        WordTC = equipTC * ((100 + UpdeviationList[TCType]) / 100)
    end
    WordTC = math.max(1,math.floor(WordTC))

    return WordTC
end
