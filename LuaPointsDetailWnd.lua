require("common/common_include")

local GameObject  = import "UnityEngine.GameObject"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Vector3 = import "UnityEngine.Vector3"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Input = import "UnityEngine.Input"
local Screen = import "UnityEngine.Screen"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local CRankData=import "L10.UI.CRankData"
local LuaGameObject=import "LuaGameObject"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CUIManager = import "L10.UI.CUIManager"
local CGuildMgr = import "L10.Game.CGuildMgr"
local System = import "System"

--local CLuaMergeBattlePopupWnd = import "L10.UI.CLuaMergeBattlePopupWnd"
--local LuaMergeBattlePopupWnd = import "L10.UI.LuaMergeBattlePopupWnd"

CLuaPointsDetailWnd=class()

--RegistChildComponent(CLuaPointsDetailWnd, "InfoTable", CUIRestrictScrollView)
--RegistChildComponent(CLuaPointsDetailWnd, "Pool", CUIGameObjectPool)
--RegistChildComponent(CLuaPointsDetailWnd, "Table", UIGrid)
RegistChildComponent(CLuaPointsDetailWnd, "PopupMenu", GameObject)
RegistChildComponent(CLuaPointsDetailWnd, "SelfItem", GameObject)

RegistClassMember(CLuaPointsDetailWnd,"m_MyRankLabel")
RegistClassMember(CLuaPointsDetailWnd,"m_MyNameLabel")
RegistClassMember(CLuaPointsDetailWnd,"m_MyGuildLabel")
RegistClassMember(CLuaPointsDetailWnd,"m_MyGasLabel")
RegistClassMember(CLuaPointsDetailWnd,"m_MyScoreLabel")
RegistClassMember(CLuaPointsDetailWnd,"m_MyClassSprite")
RegistClassMember(CLuaPointsDetailWnd,"m_MyRankSprite")

RegistClassMember(CLuaPointsDetailWnd,"m_TableViewDataSource")
RegistClassMember(CLuaPointsDetailWnd,"m_TableView")
RegistClassMember(CLuaPointsDetailWnd,"m_RankList")



function CLuaPointsDetailWnd:Awake()
    CLuaPointsDetailWnd.m_extraData={}
    self:InitSelfItem()
    local tf = LuaGameObject.GetChildNoGC(self.transform,"Anchor").transform
    self.m_TableView=LuaGameObject.GetChildNoGC(tf,"TableView").tableView

    local function initItem(item,index)
        self:InitItem(item,index)
    end

    self.m_TableViewDataSource=DefaultTableViewDataSource.CreateByCount(0,initItem)
    self.m_TableView.m_DataSource=self.m_TableViewDataSource
    --self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        -- print("select ",row)
    --end)
    
end


    

function CLuaPointsDetailWnd:Init()
	
end

function CLuaPointsDetailWnd:InitSelfItem()
	self.m_MyRankLabel=LuaGameObject.GetChildNoGC(self.SelfItem.transform,"MyRankLabel").label
    self.m_MyRankLabel.text=""
    self.m_MyNameLabel=LuaGameObject.GetChildNoGC(self.SelfItem.transform,"MyNameLabel").label
    self.m_MyNameLabel.text=""
    self.m_MyGuildLabel=LuaGameObject.GetChildNoGC(self.SelfItem.transform,"MyGuildLabel").label
    self.m_MyGuildLabel.text=""
    self.m_MyScoreLabel=LuaGameObject.GetChildNoGC(self.SelfItem.transform,"MyScoreLabel").label
    self.m_MyScoreLabel.text=""
    self.m_MyGasLabel=LuaGameObject.GetChildNoGC(self.SelfItem.transform,"MyGasLabel").label
    self.m_MyGasLabel.text=""
    self.m_MyClassSprite=LuaGameObject.GetChildNoGC(self.SelfItem.transform,"MyClassSprite").sprite
    self.m_MyClassSprite.spriteName=""
    self.m_MyRankSprite=LuaGameObject.GetChildNoGC(self.m_MyRankLabel.transform,"spe").sprite
    self.m_MyRankSprite.spriteName=""
    
    

    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text= CClientMainPlayer.Inst.Name
        --self.m_MyGasLabel.text=CClientMainPlayer.Inst.GetMyServerName()
		self.m_MyClassSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), (EnumToInt(CClientMainPlayer.Inst.Class))))
		if CClientMainPlayer.Inst:IsInGuild() then
	        self.m_MyGuildLabel.text=CGuildMgr.Inst.m_GuildName
	    else
	    	self.m_MyGuildLabel.text="-"
		end
    end

    --self.m_MyRankLabel.text=LocalString.GetString("未上榜")
end




function CLuaPointsDetailWnd:InitItem(item,index)
    if index % 2 == 1 then
        item:SetBackgroundTexture("common_bg_mission_background_n")
    else
        item:SetBackgroundTexture("common_bg_mission_background_s")
    end

    local tf=item.transform
    local scoreLabel=LuaGameObject.GetChildNoGC(tf,"ScoreLabel").label
    scoreLabel.text=""
    local nameLabel=LuaGameObject.GetChildNoGC(tf,"NameLabel").label
    nameLabel.text=" "
    local gasLabel=LuaGameObject.GetChildNoGC(tf,"GasLabel").label
    gasLabel.text=""
    local guildLabel=LuaGameObject.GetChildNoGC(tf,"GuildLabel").label
    guildLabel.text=""
    local rankLabel=LuaGameObject.GetChildNoGC(tf,"RankLabel").label
    rankLabel.text=""
    local rankSprite=LuaGameObject.GetChildNoGC(rankLabel.transform,"RankImage").sprite
    rankSprite.spriteName=""
    local classSprite=LuaGameObject.GetChildNoGC(tf,"ClassSprite").sprite
    classSprite.spriteName=""
    
    local info=self.m_RankList[index]
    local rank=info.Rank
    if rank==1 then
        rankSprite.spriteName="Rank_No.1"
    elseif rank==2 then
        rankSprite.spriteName="Rank_No.2png"
    elseif rank==3 then
        rankSprite.spriteName="Rank_No.3png"
    else
        rankLabel.text=tostring(rank)
    end

    nameLabel.text=info.Name
    classSprite.spriteName=Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), (EnumToInt(info.Job))))
    guildLabel.text=info.Guild_Name
    scoreLabel.text=System.String.Format(LocalString.GetString("{0}分"), info.Value)

    local extraList = MsgPackImpl.unpack(info.extraData)
    if extraList then
        for i=0,5 do
    	   CLuaPointsDetailWnd.m_extraData[(index)*6+i]=extraList[i]
        end
		for i = 0, extraList.Count - 1, 6 do
			if extraList[i]==0 then
				gasLabel.text=CLuaMergeBattleMgr.m_serverName1
			else
				gasLabel.text=CLuaMergeBattleMgr.m_serverName2
			end

		end
	end

	CommonDefs.AddOnClickListener(item.gameObject, DelegateFactory.Action_GameObject(function (go)
		if self.m_MyRankLabel.text~=LuaGameObject.GetChildNoGC(tf,"RankLabel").label.text and self.m_MyNameLabel.text~=LuaGameObject.GetChildNoGC(tf,"NameLabel").label.text then
        	CLuaPointsDetailWnd.m_targetData=item.gameObject
            CLuaPointsDetailWnd.m_targetId=info.PlayerIndex
            CLuaPointsDetailWnd.m_targetIndex=index+1
            local mousePosition = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
            self.PopupMenu.transform.localPosition=Vector3(self.PopupMenu.transform.localPosition.x,mousePosition.y*Screen.height,0)
            self.PopupMenu:SetActive(true)
        end
	end), false)

end

function CLuaPointsDetailWnd:Update()

	--self:ClickThroughToClose()
	
end


function CLuaPointsDetailWnd:OnRankDataReady(args)
    if CLuaRankData.m_CurRankId ~= 41000160 then return end
    local myInfo=CRankData.Inst.MainPlayerRankInfo
    
    self.m_MyNameLabel.text= myInfo.Name
    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text= CClientMainPlayer.Inst.Name
    end
    self.m_MyScoreLabel.text=System.String.Format(LocalString.GetString("{0}分"), myInfo.Value)
    self.m_MyRankSprite.spriteName=""
    local rank=myInfo.Rank
    if rank==1 then
        self.m_MyRankSprite.spriteName="Rank_No.1"
    elseif rank==2 then
        self.m_MyRankSprite.spriteName="Rank_No.2png"
    elseif rank==3 then
        self.m_MyRankSprite.spriteName="Rank_No.3png"
    else
        self.m_MyRankLabel.text=tostring(rank)
    end
    
    if myInfo.Rank>0 then
        self.m_MyRankLabel.text=myInfo.Rank
    else
        self.m_MyRankLabel.text=LocalString.GetString("未上榜")
    end
    local extraList = MsgPackImpl.unpack(myInfo.extraData)
    
    if extraList[0]==0 then
        self.m_MyGasLabel.text=CLuaMergeBattleMgr.m_serverName1
    else
        self.m_MyGasLabel.text=CLuaMergeBattleMgr.m_serverName2
    end
    self.m_RankList=CRankData.Inst.RankList
    self.m_TableViewDataSource.count=self.m_RankList.Count
    self.m_TableView:ReloadData(true,false)
end



function CLuaPointsDetailWnd:OnEnable()
    self.PopupMenu.gameObject:SetActive(false)
	g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
	Gac2Gas.QueryRank(41000160)--Lua_MergeBattle_Setting["ScoreRankId"]
end

function CLuaPointsDetailWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end


