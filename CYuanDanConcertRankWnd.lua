-- Auto Generated!!
local AlignType = import "CPlayerInfoMgr+AlignType"
local CCommonSendFlowerMgr = import "L10.UI.CCommonSendFlowerMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CYuanDanConcertRankItem = import "L10.UI.CYuanDanConcertRankItem"
local CYuanDanConcertRankWnd = import "L10.UI.CYuanDanConcertRankWnd"
local CYuanDanMgr = import "L10.Game.CYuanDanMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local UIEventListener = import "UIEventListener"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CYuanDanConcertRankWnd.m_Init_CS2LuaHook = function (this) 
    this.tableView:Clear()
    this.tableView.dataSource = this
    this.tableView.eventDelegate = this
    this.tableView:LoadData(0, true)

    local default
    if (CYuanDanMgr.Inst.ConcertPlayerRankInfoList.Count == 0) then
        default = g_MessageMgr:FormatMessage("NewYearConcert_Rank_Hint_Tip")
    else
        default = nil
    end
    this.hintLabel.text = default

    this.enterConcertBtn:SetActive(not CYuanDanMgr.Inst.IsInConcertPlay)
    this.sendFlowerBtn:SetActive(CYuanDanMgr.Inst.IsInConcertPlay)
end
CYuanDanConcertRankWnd.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 
    local cellIdentifier = "RowCell"

    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(this.template, cellIdentifier)
    end
    if index >= 0 and index < CYuanDanMgr.Inst.ConcertPlayerRankInfoList.Count then
        local data = CYuanDanMgr.Inst.ConcertPlayerRankInfoList[index]
        CommonDefs.GetComponentInChildren_GameObject_Type(cell, typeof(CYuanDanConcertRankItem)):Init(data, data.rank % 2 ~= 0)
    end

    return cell
end
CYuanDanConcertRankWnd.m_OnRowSelected_CS2LuaHook = function (this, index) 

    if index >= 0 and index < CYuanDanMgr.Inst.ConcertPlayerRankInfoList.Count then
        this.selectedIndex = index
        if not this.firstSelected then
            CPlayerInfoMgr.ShowPlayerPopupMenu(CYuanDanMgr.Inst.ConcertPlayerRankInfoList[index].playerId, EnumPlayerInfoContext.RankList, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
        end
        this.firstSelected = false
    end
end
CYuanDanConcertRankWnd.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeBtn).onClick = MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.sendFlowerBtn).onClick = MakeDelegateFromCSFunction(this.OnSendFlowerButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.enterConcertBtn).onClick = MakeDelegateFromCSFunction(this.OnEnterConcertButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.infoBtn).onClick = MakeDelegateFromCSFunction(this.OnInfoButtonClick, VoidDelegate, this)
end
CYuanDanConcertRankWnd.m_OnSendFlowerButtonClick_CS2LuaHook = function (this, go) 
    if this.selectedIndex >= 0 and this.selectedIndex < CYuanDanMgr.Inst.ConcertPlayerRankInfoList.Count then
        local data = CYuanDanMgr.Inst.ConcertPlayerRankInfoList[this.selectedIndex]
        CCommonSendFlowerMgr.Inst:ShowSendFlowerWnd(data.playerId, data.playerName, CCommonSendFlowerMgr.EnumSendFlowerType.YuanDanConcert, 0)
    else
        g_MessageMgr:ShowMessage("NewYearConcert_Pick_One_Player")
    end
end
