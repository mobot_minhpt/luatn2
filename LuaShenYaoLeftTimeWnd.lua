local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local CPos = import "L10.Engine.CPos"
local Color = import "UnityEngine.Color"
local CScene = import "L10.Game.CScene"
local Utility = import "L10.Engine.Utility"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CTeamMatchMgr = import "L10.Game.CTeamMatchMgr"
local CTeamQuickJoinInfoMgr = import "L10.UI.CTeamQuickJoinInfoMgr"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CStatusMgr = import "L10.Game.CStatusMgr"
local EPropStatus = import "L10.Game.EPropStatus"
local Ease = import "DG.Tweening.Ease"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local EnumObjectType = import "L10.Game.EnumObjectType"

LuaShenYaoLeftTimeWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShenYaoLeftTimeWnd, "InfoTable", "InfoTable", QnTableView)
RegistChildComponent(LuaShenYaoLeftTimeWnd, "UnlockLvLabel", "UnlockLvLabel", UILabel)
RegistChildComponent(LuaShenYaoLeftTimeWnd, "RefreshBtn", "RefreshBtn", GameObject)
RegistChildComponent(LuaShenYaoLeftTimeWnd, "CreateTeamBtn", "CreateTeamBtn", GameObject)
RegistChildComponent(LuaShenYaoLeftTimeWnd, "JoinTeamBtn", "JoinTeamBtn", GameObject)
RegistChildComponent(LuaShenYaoLeftTimeWnd, "TipButton", "TipButton", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaShenYaoLeftTimeWnd, "m_inited")
RegistClassMember(LuaShenYaoLeftTimeWnd, "m_InfoList") -- 原始数据
RegistClassMember(LuaShenYaoLeftTimeWnd, "m_UniqueInfoList") -- 去重数据
RegistClassMember(LuaShenYaoLeftTimeWnd, "m_Level2NpcList") -- 阶次->蜃妖npc
RegistClassMember(LuaShenYaoLeftTimeWnd, "m_maxLevel") -- 蜃妖的最高等级
RegistClassMember(LuaShenYaoLeftTimeWnd, "m_unlockLevel") -- 打败过的蜃妖的最高等级

function LuaShenYaoLeftTimeWnd:Awake()
    self.m_maxLevel = 0
    XinBaiLianDong_ShenYaoNpc.Foreach(function(k,v)
        self.m_maxLevel = math.max(self.m_maxLevel, k)
    end)

    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


    --@endregion EventBind end
    self.m_inited = false
    self.UnlockLvLabel.text = ""
    self.m_InfoList = {}
    self.m_UniqueInfoList = {}
    self.m_Level2NpcList = {}
    
    self.InfoTable.m_DataSource = DefaultTableViewDataSource.Create(function() 
        return #self.m_InfoList
    end, function(item, index)
        self:InitItem(item.transform, index)
    end)

    UIEventListener.Get(self.RefreshBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        Gac2Gas.ShenYao_QuerySceneCountTbl()
    end)

    UIEventListener.Get(self.CreateTeamBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        if not CTeamMgr.Inst:TeamExists() then
            local findId = self:GetTeamMatchId()
            CTeamMatchMgr.Inst:CreateTeam(findId)
        end
        if not CUIManager.IsLoaded(CUIResources.TeamWnd) then
            CUIManager.ShowUI(CUIResources.TeamWnd)
        end
    end)

    UIEventListener.Get(self.JoinTeamBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        --快捷组队
        local findId = self:GetTeamMatchId()
        CTeamQuickJoinInfoMgr.Inst:ShowTeamQuickJoinWndWithActivityId(findId)
    end)

    UIEventListener.Get(self.transform:Find("SettingButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSettingButtonClick()
	end)
end

function LuaShenYaoLeftTimeWnd:Init()
    Gac2Gas.ShenYao_QuerySceneCountTbl()
end

function LuaShenYaoLeftTimeWnd:OnEnable()
    g_ScriptEvent:AddListener("ShenYao_QuerySceneCountTblResult", self, "ShenYao_QuerySceneCountTblResult")
end

function LuaShenYaoLeftTimeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ShenYao_QuerySceneCountTblResult", self, "ShenYao_QuerySceneCountTblResult")
end

function LuaShenYaoLeftTimeWnd:OnDestroy()
    self:ClearTweeners()
    CUIManager.CloseUI(CLuaUIResources.ShenYaoSettingWnd)
end

--@region UIEvent

function LuaShenYaoLeftTimeWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("SHENYAO_TIP_RULE")
end


--@endregion UIEvent

function LuaShenYaoLeftTimeWnd:OnSettingButtonClick()
    Gac2Gas.ShenYao_QueryRejectLevel()
end

function LuaShenYaoLeftTimeWnd:InitItem(tf, index)
    local info = self.m_UniqueInfoList[index + 1]
    local nameLabel = tf:Find("MapLabel"):GetComponent(typeof(UILabel))
    local map = PublicMap_PublicMap.GetData(info[1].sceneTemplateId)
    nameLabel.text = map and map.Name or ""

    local btnObj = {}
    local fengyinBtnObj = {}
    local leftCount = {}

    for i = 0, 2 do
        table.insert(btnObj, tf:Find("GotoBtn_"..i).gameObject)
        table.insert(fengyinBtnObj, tf:Find("FengyinBtn_"..i).gameObject)
        table.insert(leftCount, tf:Find("LeftTimeLabel_"..i):GetComponent(typeof(UILabel)))
    end
    
    if #info < 3 then
        btnObj[3]:SetActive(false)
        leftCount[3].gameObject:SetActive(false)
    end

    if #info < 2 then
        btnObj[2]:SetActive(false)
        leftCount[2].gameObject:SetActive(false)
    end

    for i = 1, #info do
        leftCount[i].text = SafeStringFormat3(LocalString.GetString("%d阶 剩余%d"), info[i].level, info[i].leftCount)
        if info[i].leftCount == 0 then
            leftCount[i].color = Color.white
            if info[i].playingCount == 0 then
                btnObj[i]:SetActive(false)
                fengyinBtnObj[i]:GetComponent(typeof(CButton)).enabled = false
                fengyinBtnObj[i]:SetActive(true)
            else
                btnObj[i]:SetActive(true)
                btnObj[i]:GetComponent(typeof(CButton)).Enabled = false
                fengyinBtnObj[i]:SetActive(false)
            end
        else
            leftCount[i].color = NGUIText.ParseColor24("80FC63", 0)
            btnObj[i]:SetActive(true)
            btnObj[i]:GetComponent(typeof(CButton)).Enabled = true
            fengyinBtnObj[i]:SetActive(false)
            UIEventListener.Get(btnObj[i]).onClick = DelegateFactory.VoidDelegate(function (p) 
                LuaXinBaiLianDongMgr:TrackShenYaoNpc(self.m_Level2NpcList[info[i].level])
            end)
        end
    end
end

function LuaShenYaoLeftTimeWnd:GetTeamMatchId()
    --[[
    local findId = 0
    local data = Task_QueryNpc.GetData(10) -- 蜃妖ActivityTypeId
    if data ~= nil then
        TeamMatch_Activities.Foreach(function (key, val) 
            if val.LinkedScheduleID == data.ScheduleID then
                findId = key
            end
        end)
    end
    return findId
    --]]
    return 94 -- 默认1~2阶蜃妖
end

function LuaShenYaoLeftTimeWnd:ShenYao_QuerySceneCountTblResult(sceneCountTbl, unlockLv)
    -- 做一个类似F5的效果
    self:ClearTweeners()
    local panel = self.InfoTable:GetComponent(typeof(UIPanel))
    local tweener = LuaTweenUtils.TweenFloat(0, 1, 0.1, function(val)
        panel.alpha = val
    end)
    LuaTweenUtils.SetEase(tweener, Ease.Linear)
    table.insert(self.m_Tweeners, tweener)

    local data = g_MessagePack.unpack(sceneCountTbl)
    self.m_InfoList = {}
    self.m_UniqueInfoList = {}
    self.m_Level2NpcList = {}
    self.m_unlockLevel = unlockLv

    for key, val in pairs(data) do
        local mapItem = {}
        for k = 1, #val do
            local v = val[k]
            local npcItem = {
                instId = v.NpcInfo.InstId,
                sceneTemplateId = tonumber(key),
                level = v.NpcInfo.Level,
                npcId = v.NpcInfo.NpcId,
                times = v.NpcInfo.Times,
                targetSceneTemplateId = v.NpcInfo.Pos.mapId,
                targetPosX = v.NpcInfo.Pos.x,
                targetPosY = v.NpcInfo.Pos.y,
                leftCount = v.LeftCount,
                playingCount = v.PlayingCount
            }
            table.insert(mapItem, npcItem)
            if not self.m_Level2NpcList[npcItem.level] then
                self.m_Level2NpcList[npcItem.level] = {}
            end
            table.insert(self.m_Level2NpcList[npcItem.level], npcItem)
        end
        table.insert(self.m_InfoList, mapItem)
    end

    for _, mapItem in ipairs(self.m_InfoList) do -- 对每张地图的NPC去重
        table.sort(mapItem, function(a, b) 
            return a.level < b.level
        end)
        local uniqueMapItem = {}
        for __, npcItem in ipairs(mapItem) do
            if npcItem.level and npcItem.level > 0 and npcItem.npcId then
                if #uniqueMapItem == 0 or uniqueMapItem[#uniqueMapItem].level ~= npcItem.level then
                    local copyNpc = {}
                    for k, v in pairs(npcItem) do
                        copyNpc[k] = v
                    end
                    table.insert(uniqueMapItem, copyNpc)
                else
                    uniqueMapItem[#uniqueMapItem].leftCount = uniqueMapItem[#uniqueMapItem].leftCount + npcItem.leftCount
                    uniqueMapItem[#uniqueMapItem].playingCount = uniqueMapItem[#uniqueMapItem].playingCount + npcItem.playingCount
                end
            end
        end
        table.insert(self.m_UniqueInfoList, uniqueMapItem)
    end

    table.sort(self.m_UniqueInfoList, function(a, b)
        return a[1].level < b[1].level
    end)

    self.InfoTable:ReloadData(true, false)
    if unlockLv >= 8 then
        self.InfoTable:ScrollToEnd()
    end

    if CommonDefs.IS_VN_CLIENT then
        self.UnlockLvLabel.text = LocalString.GetString("阶") .. (unlockLv >= self.m_maxLevel and unlockLv or unlockLv + 1)
    else
        self.UnlockLvLabel.text = (unlockLv >= self.m_maxLevel and unlockLv or unlockLv + 1)..LocalString.GetString("阶")
    end

    self.m_inited = true
end

RegistClassMember(LuaShenYaoLeftTimeWnd, "m_Tweeners")
function LuaShenYaoLeftTimeWnd:ClearTweeners()
    if self.m_Tweeners then
        for i = 1, #self.m_Tweeners do
            LuaTweenUtils.Kill(self.m_Tweeners[i], false)
        end
    end
    self.m_Tweeners = {}
end
