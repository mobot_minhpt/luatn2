local UIPanel = import "UIPanel"
local UICommonDef = import "L10.UI.CUICommonDef"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"

LuaMakeClothesResultView = class()

RegistChildComponent(LuaMakeClothesResultView, "m_ClothesSurface","ClothesSurface", UIPanel)
RegistChildComponent(LuaMakeClothesResultView, "m_ClothesInside","ClothesInside", UIPanel)
RegistChildComponent(LuaMakeClothesResultView, "m_ClothesSurface2","ClothesSurface2", UIPanel)
RegistChildComponent(LuaMakeClothesResultView, "m_ClothesInside2","ClothesInside2", UIPanel)
RegistChildComponent(LuaMakeClothesResultView, "m_Template","Template", CCommonLuaScript)
RegistChildComponent(LuaMakeClothesResultView, "m_ShareButton","ShareButton", GameObject)
RegistChildComponent(LuaMakeClothesResultView, "m_Clothes","Clothes", CUITexture)
RegistChildComponent(LuaMakeClothesResultView, "m_CloseButton","CloseButton", GameObject)

function LuaMakeClothesResultView:Start()
    self.m_Template.gameObject:SetActive(false)
    self.m_ClothesSurface.gameObject:SetActive(LuaMakeClothesMgr.m_WndType == 1)
    self.m_ClothesInside.gameObject:SetActive(LuaMakeClothesMgr.m_WndType == 1)
    self.m_ClothesSurface2.gameObject:SetActive(LuaMakeClothesMgr.m_WndType ~= 1)
    self.m_ClothesInside2.gameObject:SetActive(LuaMakeClothesMgr.m_WndType ~= 1)
    local surface = LuaMakeClothesMgr.m_WndType == 1 and self.m_ClothesSurface or self.m_ClothesSurface2
    local inside = LuaMakeClothesMgr.m_WndType == 1 and self.m_ClothesInside or self.m_ClothesInside2
    for index,viewData in pairs(LuaMakeClothesMgr.m_PatternsListOnClothes) do
        local instance = NGUITools.AddChild(viewData.isInSide and inside.gameObject or surface.gameObject, self.m_Template.gameObject)
        instance.transform.localPosition = viewData.pos
        instance.transform.localEulerAngles = viewData.rot
        local tex = instance:GetComponent(typeof(UITexture))
        tex.flip = viewData.flip
        local patternView = instance:GetComponent(typeof(CCommonLuaScript))
        patternView:Awake()
        tex.width = viewData.scale * 600
        tex.height = viewData.scale * 600
        patternView.m_LuaSelf:Init({[0] = viewData.info})
        instance:SetActive(true)
    end
    UIEventListener.Get(self.m_ShareButton).onClick = DelegateFactory.VoidDelegate(function()
        self:Share()
    end)
    self.m_Clothes:LoadMaterial(LuaMakeClothesMgr:GetClothesMat(LuaMakeClothesMgr.m_SelectMaterial))
end

function LuaMakeClothesResultView:Share()
    self.m_ShareButton:SetActive(false)
    self.m_CloseButton:SetActive(false)
    UICommonDef.CaptureScreen(
            "screenshot",
            true,
            false,
            nil,
            DelegateFactory.Action_string_bytes(
                    function(fullPath, jpgBytes)
                        ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
                        self.m_ShareButton:SetActive(true)
                        self.m_CloseButton:SetActive(true)
                    end
            ),
            false
    )
end