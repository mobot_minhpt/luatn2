local CServerTimeMgr    = import "L10.Game.CServerTimeMgr"
local CTaskMgr          = import "L10.Game.CTaskMgr"
local CTaskInfoMgr      = import "L10.UI.CTaskInfoMgr"
local CScheduleMgr      = import "L10.Game.CScheduleMgr"
local CActivityAlertMgr = import "L10.UI.CActivityAlertMgr"
local CWelfareBonusMgr  = import "L10.Game.CWelfareBonusMgr"

if rawget(_G, "LuaQianYingChuWenMgr") then
    g_ScriptEvent:RemoveListener("GasDisconnect", LuaQianYingChuWenMgr, "OnDisconnect")
end

LuaQianYingChuWenMgr = {}
LuaQianYingChuWenMgr.dayInfo = {}
LuaQianYingChuWenMgr.cachedCycleWords = {}

g_ScriptEvent:AddListener("GasDisconnect", LuaQianYingChuWenMgr, "OnDisconnect")

-- 切换角色清除数据
function LuaQianYingChuWenMgr:OnDisconnect()
    self.dayInfo = {}
    self.cachedCycleWords = {}
end

-- 打开倩影初闻界面
function LuaQianYingChuWenMgr:OpenQianYingChuWenWnd()
    if self:IsQianYingChuWenOpen() then
        CUIManager.ShowUI(CLuaUIResources.QianYingChuWenWnd)
    end
end

-- 倩影初闻系统是否开放
function LuaQianYingChuWenMgr:IsQianYingChuWenOpen()
    if CClientMainPlayer.Inst then
        local stage = CClientMainPlayer.Inst.PlayProp.BeginnerGuideInfo.Stage
        if stage == EnumBeginnerGuideStage.eOpen or stage == EnumBeginnerGuideStage.eFinalRewarding then
            return true
        end
    end
    return false
end

-- 获取登录时间
function LuaQianYingChuWenMgr:GetTotalLoginDays()
    local key = EnumPersistPlayDataKey_lua.eTotalLoginDaysInfo

    local data = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), key)
    if not data then return 0 end

    return g_MessagePack.unpack(data.Data)[2]
end


--#region Gas2Gac

-- 同步 beginnerGuideInfo
function LuaQianYingChuWenMgr:SyncBeginnerGuideInfo(beginnerGuideInfoU)
    local data = CBeginnerGuide()
    data:LoadFromString(beginnerGuideInfoU)
    CClientMainPlayer.Inst.PlayProp.BeginnerGuideInfo = data
    self:UpdateAcceptDayInfo()

    g_ScriptEvent:BroadcastInLua("SyncBeginnerGuideInfo")
    if CClientMainPlayer.Inst.PlayProp.BeginnerGuideInfo.Stage == EnumBeginnerGuideStage.eFinish then
        self.dayInfo = {}
        EventManager.Broadcast(EnumEventType.SyncDynamicActivitySimpleInfo)
    end
end

-- 玩法开放给玩家
function LuaQianYingChuWenMgr:OpenBeginnerGuideInfoToPlayer()
    EventManager.Broadcast(EnumEventType.SyncDynamicActivitySimpleInfo)
end

-- 领取任务奖励成功
function LuaQianYingChuWenMgr:RequestBeginnerGuideTaskRewardSuccess(taskId, postTaskId, todayAllFinish)
    g_ScriptEvent:BroadcastInLua("RequestBeginnerGuideTaskRewardSuccess", taskId, postTaskId, todayAllFinish)
end

-- 领取最终奖励成功
function LuaQianYingChuWenMgr:RequestBeginnerGuideFinalRewardSuccess(rewardId)
end

-- 一键领取任务奖励成功
function LuaQianYingChuWenMgr:RequestBeginnerGuideBatchTaskRewardSuccess(tasks, todayAllFinishT)
    g_ScriptEvent:BroadcastInLua("RequestBeginnerGuideBatchTaskRewardSuccess", tasks, todayAllFinishT)
end

-- 关闭七日签到奖励
function LuaQianYingChuWenMgr:CloseLoginDaysAward()
    CWelfareBonusMgr.Inst.LoginAwardId = 0
    EventManager.Broadcast(EnumEventType.LoginDaysAwardReceived)

    if CUIManager.IsLoaded(CUIResources.GreenHandGiftWnd) then
        CUIManager.CloseUI(CUIResources.GreenHandGiftWnd)
    end
end

-- 更新任务
function LuaQianYingChuWenMgr:RequestFixBeginnerGuideDay2FinishSuccess(day)
    g_ScriptEvent:BroadcastInLua("RequestFixBeginnerGuideDay2FinishSuccess", day)
end

--#endregion

-- 更新七天任务的完成情况
function LuaQianYingChuWenMgr:UpdateAcceptDayInfo()
    local day2Finish = self:GetDay2Finish()
    for i = 1, 7 do
        if day2Finish[i] == EnumBeginnerGuideDayStatus.eAccept then
            self:SetAcceptDayInfo(i)
        end
    end
end

-- 获取某天的任务信息
function LuaQianYingChuWenMgr:GetTaskInfo(day)
    local day2Finish = self:GetDay2Finish()
    if day2Finish[day] == EnumBeginnerGuideDayStatus.eFinish then
        if not self.dayInfo[day] or self.dayInfo[day].status ~= EnumBeginnerGuideDayStatus.eFinish then
            self:SetFinishedDayInfo(day)
        end
    elseif day2Finish[day] == EnumBeginnerGuideDayStatus.eAccept then
        if not self.dayInfo[day] or self.dayInfo[day].status ~= EnumBeginnerGuideDayStatus.eAccept then
            self:SetAcceptDayInfo(day)
        end
    elseif day2Finish[day] == EnumBeginnerGuideDayStatus.eEmpty then
        if not self.dayInfo[day] or self.dayInfo[day].status ~= EnumBeginnerGuideDayStatus.eEmpty then
            self:SetEmptyDayInfo(day)
        end
    end
    return self.dayInfo[day].task
end

-- 获取每天任务的完成情况
function LuaQianYingChuWenMgr:GetDay2Finish()
    return CClientMainPlayer.Inst.PlayProp.BeginnerGuideInfo.Day2Finish
end

-- 设置正在进行中的某天的任务信息
function LuaQianYingChuWenMgr:SetAcceptDayInfo(day)
    local tasksDict = CClientMainPlayer.Inst.PlayProp.BeginnerGuideInfo.Tasks

    -- 需要去除的TaskId（因为多个任务存在前后关系，当前任务没完成，不应该显示后续任务）
    local excludeTaskDict = {}
    CommonDefs.DictIterate(tasksDict, DelegateFactory.Action_object_object(function(key, value)
        local postTaskId = BeginnerGuide_Task.GetData(tonumber(key)).PostTask
        if postTaskId > 0 then
            excludeTaskDict[postTaskId] = true
        end
    end))

    local taskInfo = {}
    CommonDefs.DictIterate(tasksDict, DelegateFactory.Action_object_object(function(key, value)
        local id = tonumber(key)
        if excludeTaskDict[id] then return end

        local status = tonumber(value)
        local data = BeginnerGuide_Task.GetData(id)
        if data.OpenDay ~= day or data.Status > 0 then return end

        local accumulate = nil
        if status ~= EnumBeginnerGuideTaskStatus.eFinish then
            accumulate = CommonDefs.DictGetValue_LuaCall(tasksDict, id) or 0
            if accumulate >= data.Target then
                status = EnumBeginnerGuideTaskStatus.eCanSubmit
            end
        end

        local condition = nil
        if status < EnumBeginnerGuideTaskStatus.eCanSubmit then
            condition = self:GetNeedShowCondition(data)
        end

        table.insert(taskInfo, {id = id, status = status, accumulate = accumulate, condition = condition})
    end))

    self:SortTaskInfo(taskInfo)

    self.dayInfo[day] = {}
    self.dayInfo[day].task = taskInfo
    self.dayInfo[day].status = EnumBeginnerGuideDayStatus.eAccept
end

-- 设置已经完成的某天的任务信息
function LuaQianYingChuWenMgr:SetFinishedDayInfo(day)
    local taskInfo = {}
    BeginnerGuide_Task.Foreach(function(id, data)
        if data.OpenDay == day and data.PostTask == 0 and data.Status == 0 then
            table.insert(taskInfo, {id = id, status = EnumBeginnerGuideTaskStatus.eFinish})
        end
    end)

    self:SortTaskInfo(taskInfo)

    self.dayInfo[day] = {}
    self.dayInfo[day].task = taskInfo
    self.dayInfo[day].status = EnumBeginnerGuideDayStatus.eFinish
end

-- 设置未开始的某天的任务信息
function LuaQianYingChuWenMgr:SetEmptyDayInfo(day)
    local taskInfo = {}
    local postTasks = {}
    BeginnerGuide_Task.Foreach(function(id, data)
        if data.OpenDay == day and data.PostTask > 0 then
            postTasks[data.PostTask] = true
        end
    end)

    BeginnerGuide_Task.Foreach(function(id, data)
        if data.OpenDay == day and not postTasks[id] and data.Status == 0 then
            table.insert(taskInfo, {id = id, status = EnumBeginnerGuideTaskStatus.eNew, accumulate = 0})
        end
    end)

    self:SortTaskInfo(taskInfo)

    self.dayInfo[day] = {}
    self.dayInfo[day].task = taskInfo
    self.dayInfo[day].status = EnumBeginnerGuideDayStatus.eEmpty
end

-- 对某天的任务信息排序
function LuaQianYingChuWenMgr:SortTaskInfo(taskInfo)
    table.sort(taskInfo, function(a, b)
        if a.status ~= b.status then
            if a.status == EnumBeginnerGuideTaskStatus.eCanSubmit or b.status == EnumBeginnerGuideTaskStatus.eFinish then
                return true
            end
            if b.status == EnumBeginnerGuideTaskStatus.eCanSubmit or a.status == EnumBeginnerGuideTaskStatus.eFinish then
                return false
            end
        end

        if not a.condition and b.condition then
            return true
        end
        if a.condition and not b.condition then
            return false
        end
        return a.id < b.id
    end)
end


-- 是否需要显示前置条件
function LuaQianYingChuWenMgr:GetNeedShowCondition(data)
    if data.LevelCheck > 0 then
        if CClientMainPlayer.Inst.MaxLevel < data.LevelCheck then
            return SafeStringFormat3(LocalString.GetString("需达到%d级"), data.LevelCheck)
        end
    end

    if data.GuildCheck > 0 then
        if CClientMainPlayer.Inst.BasicProp.GuildId == 0 then
            return LocalString.GetString("需加入帮会")
        end
    end

    if data.PreTaskCheck > 0 then
        if not CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(data.PreTaskCheck) then
            return data.ConditionDesc
        end
    end

    if data.DayOfWeekCheck and data.DayOfWeekCheck.Length > 0 then
        local dayOfWeek = EnumToInt(CServerTimeMgr.Inst:GetZone8Time().DayOfWeek)

        for i = 0, data.DayOfWeekCheck.Length - 1 do
            if dayOfWeek == data.DayOfWeekCheck[i] then
                return nil
            end
        end
        return data.ConditionDesc
    end

    -- 一些活动特殊处理
    if data.Event == "GuildDrink" then
        local scheduleInfo = self:GetGuildDrinkScheduleInfo()
        if not scheduleInfo then
            return g_MessageMgr:FormatMessage("GUILDDRINK_IS_CLOSE_TODAY")
        end
    elseif data.Event == "GNJC" then
        local scheduleInfo = self:GetScheduleInfo(data.GoToParams)
        if not scheduleInfo then
            return g_MessageMgr:FormatMessage("GUANNING_IS_CLOSE_TODAY")
        end
    elseif data.Event == "GaoChang" then
        local scheduleInfo = self:GetScheduleInfo(data.GoToParams)
        if not scheduleInfo then
            return g_MessageMgr:FormatMessage("GAOCHANG_IS_CLOSE_TODAY")
        end
    elseif data.Event == "GuildLeague" then
        local scheduleInfo = self:GetScheduleInfo(data.GoToParams)
        if not scheduleInfo then
            return g_MessageMgr:FormatMessage("GUILDLEAGUE_IS_CLOSE_TODAY")
        end
    end

    return nil
end

-- 获取任务都完成的天数
function LuaQianYingChuWenMgr:GetFinishedDayCount()
    local day2Finish = self:GetDay2Finish()
    local count = 0
    for i = 1, 7 do
        if day2Finish[i] == EnumBeginnerGuideDayStatus.eFinish then
            count = count + 1
        end
    end
    return count
end

-- 是否所有的任务都完成了
function LuaQianYingChuWenMgr:IsAllTaskFinish()
    return self:GetFinishedDayCount() == 7 or CClientMainPlayer.Inst.PlayProp.BeginnerGuideInfo.Stage == EnumBeginnerGuideStage.eFinalRewarding
end

--#region 前往逻辑

-- Event-->前往函数名
EnumBeginnerGuideGoToActionFunc = {
	SubmitTask = "GoTo_SubmitTask",
    YTL = "GoTo_YTL",
    GuildFreight = "GoTo_GuildFreight",
    ShouCai = "GoTo_ShouCai",
    CaiWei = "GoTo_CaiWei",
    JiXing = "GoTo_JiXing",
    GuildShouWei = "GoTo_GuildShouWei",
    GuildDrink = "GoTo_GuildDrink",
    ZhanLong = "GoTo_ZhanLong",
    SchoolTask = "GoTo_SchoolTask",
    SchoolScore = "GoTo_SchoolScore",
    GNJC = "GoTo_GNJC",
    BaoTuTask = "GoTo_BaoTuTask",
    GaoChang = "GoTo_GaoChang",
    GuildLeague = "GoTo_GuildLeague",
    PaoShang = "GoTo_PaoShang",
    QingYuan = "GoTo_QingYuan",
    ShenYao = "GoTo_ShenYao",
}

-- 完成任务
function LuaQianYingChuWenMgr:GoTo_SubmitTask(taskIds)
    local taskProp = CClientMainPlayer.Inst.TaskProp

    for i = 0, taskIds.Length - 1 do
        if CommonDefs.ListContains_LuaCall(taskProp.CurrentTaskList, taskIds[i]) then
            self:TrackToDoTask(taskIds[i])
            self:CloseWnd()
            return
        end

        if CommonDefs.ListContains_LuaCall(taskProp.AcceptableTaskList, taskIds[i]) then
            CTaskInfoMgr.ShowTaskWnd(taskIds[i], 1)
            self:CloseWnd()
            return
        end
    end

    self:ShowMessage("QIANYINGCHUWEN_TASK_CANNOT_ACCEPT", 1)
end

-- 一条龙
function LuaQianYingChuWenMgr:GoTo_YTL(activityIds)
    local scheduleInfo = self:GetScheduleInfo(activityIds)
    if not scheduleInfo then return end

    if scheduleInfo:IsRegardAsFinished() then
        self:ShowMessage("YITIAOLONG_IS_FINISHED")
    else
        if CLuaScheduleMgr.IsInProgress(scheduleInfo.taskId) then
            self:TrackToDoTask(CClientMainPlayer.Inst.PlayProp.YiTiaoLongCurrentTaskId)
        else
            self:DoScheduleTask(scheduleInfo)
        end
        self:CloseWnd()
    end
end

-- 师门任务
function LuaQianYingChuWenMgr:GoTo_SchoolTask(activityIds)
    local curTaskId = self:GetCurSchoolTaskId()
    if curTaskId then
        self:TrackToDoTask(curTaskId)
        self:CloseWnd()
        return
    end

    local scheduleInfo = CLuaScheduleMgr.m_SchoolTaskOpenStatus and self:GetScheduleInfo(activityIds) or self:GetNewbieSchoolScheduleInfo()
    if not scheduleInfo then return end

    if scheduleInfo:IsRegardAsFinished() then
        self:ShowMessage("SCHOOLTASK_IS_FINISHED")
    else
        self:DoScheduleTask(scheduleInfo)
        self:CloseWnd()
    end
end

-- 师门课业学分
function LuaQianYingChuWenMgr:GoTo_SchoolScore()
    local curTaskId = self:GetCurSchoolTaskId()
    if curTaskId then
        self:TrackToDoTask(curTaskId)
        self:CloseWnd()
        return
    end

    local scheduleInfo = self:GetNewbieSchoolScheduleInfo()
    if not scheduleInfo then return end

    if scheduleInfo:IsRegardAsFinished() then
        self:ShowMessage("SCHOOLSCORE_IS_FINISHED")
    else
        self:DoScheduleTask(scheduleInfo)
        self:CloseWnd()
    end
end

-- 获取当前正在进行中的师门课业或师门学分任务
function LuaQianYingChuWenMgr:GetCurSchoolTaskId()
    local taskProp = CClientMainPlayer.Inst.TaskProp
    for i = 0, taskProp.CurrentTaskList.Count - 1 do
        local taskId = taskProp.CurrentTaskList[i]
        local gamePlay = Task_Task.GetData(taskId).GamePlay
        if gamePlay == "NewbieSchoolTask" or gamePlay == "SchoolTask" then
            return taskId
        else
            -- 主任务为师门任务
            local mainTaskId = CommonDefs.DictGetValue_LuaCall(taskProp.CurrentTasks, taskId).MainTaskId
            if mainTaskId > 0 and CLuaScheduleMgr.IsInProgress(mainTaskId) then
                local mainGamePlay =  Task_Task.GetData(mainTaskId).GamePlay
                if mainGamePlay == "NewbieSchoolTask" or mainGamePlay == "SchoolTask" then
                    return taskId
                end
            end
        end

        -- 入门仪式和出师仪式特殊处理
        local taskList = BeginnerGuide_Setting.GetData().NiebieSchoolTaskList
        for j = 0, taskList.Length - 1 do
            if taskId == taskList[j] then
                return taskId
            end
        end
    end
    return nil
end

-- 货运任务
function LuaQianYingChuWenMgr:GoTo_GuildFreight(activityIds)
    local scheduleInfo = self:GetScheduleInfo(activityIds)
    if not scheduleInfo then return end

    if scheduleInfo:IsRegardAsFinished() then
        self:ShowMessage("GUILDFREIGHT_IS_FINISHED")
    else
        if CLuaScheduleMgr.IsInProgress(scheduleInfo.taskId) then
            self:TrackToDoTask(scheduleInfo.taskId)
            self:CloseWnd()
        else
            local ret = self:DoScheduleTask(scheduleInfo)
            if ret == "Close" then
                self:ShowMessage("GUILDFREIGHT_IS_CLOSE", 1)
            else
                self:CloseWnd()
            end
        end
    end
end

-- 关宁
function LuaQianYingChuWenMgr:GoTo_GNJC(activityIds)
    local scheduleInfo = self:GetScheduleInfo(activityIds)
    if not scheduleInfo then return end

    if scheduleInfo:IsRegardAsFinished() then
        self:ShowMessage("GUANNING_IS_FINISHED")
    else
        local ret = self:DoScheduleTask(scheduleInfo)
        if ret == "Close" then
            self:ShowMessage("GUANNING_IS_CLOSE", 1)
        else
            self:CloseWnd()
        end
    end
end

-- 守财
function LuaQianYingChuWenMgr:GoTo_ShouCai(activityIds)
    local scheduleInfo = self:GetScheduleInfo(activityIds)
    if not scheduleInfo then return end

    if scheduleInfo:IsRegardAsFinished() then
        self:ShowMessage("SHOUCAI_IS_FINISHED")
    else
        local ret = self:DoScheduleTask(scheduleInfo, 4)
        if ret == "Close" then
            self:ShowMessage("SHOUCAI_IS_CLOSE", 1)
        elseif ret == "NoLeft" then
            self:ShowMessage("SHOUCAI_NO_LEFT", 1)
        elseif ret == "Do" then
            self:CloseWnd()
        end
    end
end

-- 采薇
function LuaQianYingChuWenMgr:GoTo_CaiWei(activityIds)
    local scheduleInfo = self:GetScheduleInfo(activityIds)
    if not scheduleInfo then return end

    if scheduleInfo:IsRegardAsFinished() then
        self:ShowMessage("CAIWEI_IS_FINISHED")
    else
        local ret = self:DoScheduleTask(scheduleInfo, 1)
        if ret == "Close" then
            self:ShowMessage("CAIWEI_IS_CLOSE", 1)
        elseif ret == "NoLeft" then
            self:ShowMessage("CAIWEI_NO_LEFT", 1)
        elseif ret == "Do" then
            self:CloseWnd()
        end
    end
end

-- 吉星
function LuaQianYingChuWenMgr:GoTo_JiXing(activityIds)
    local scheduleInfo = self:GetScheduleInfo(activityIds)
    if not scheduleInfo then return end

    if scheduleInfo:IsRegardAsFinished() then
        self:ShowMessage("JIXING_IS_FINISHED")
    else
        local ret = self:DoScheduleTask(scheduleInfo, 2)
        if ret == "Close" then
            self:ShowMessage("JIXING_IS_CLOSE", 1)
        elseif ret == "NoLeft" then
            self:ShowMessage("JIXING_NO_LEFT", 1)
        elseif ret == "Do" then
            self:CloseWnd()
        end
    end
end

-- 帮会大盗
function LuaQianYingChuWenMgr:GoTo_GuildShouWei(activityIds)
    local scheduleInfo = self:GetScheduleInfo(activityIds)
    if not scheduleInfo then return end

    if scheduleInfo:IsRegardAsFinished() then
        self:ShowMessage("GUILDSHOUWEI_IS_FINISHED")
    else
        local ret = self:DoScheduleTask(scheduleInfo)
        if ret == "Close" then
            self:ShowMessage("GUILDSHOUWEI_IS_CLOSE", 1)
        elseif ret == "Do" then
            self:CloseWnd()
        end
    end
end

-- 行酒令
function LuaQianYingChuWenMgr:GoTo_GuildDrink()
    local scheduleInfo = self:GetGuildDrinkScheduleInfo()
    if not scheduleInfo then return end

    if scheduleInfo:IsRegardAsFinished() then
        self:ShowMessage("GUILDDRINK_IS_FINISHED")
    else
        local ret = self:DoScheduleTask(scheduleInfo)
        if ret == "Close" then
            self:ShowMessage("GUILDDRINK_IS_CLOSE", 1)
        else
            self:CloseWnd()
        end
    end
end

-- 战龙堂
function LuaQianYingChuWenMgr:GoTo_ZhanLong(activityIds)
    local scheduleInfo = self:GetScheduleInfo(activityIds)
    if not scheduleInfo then
        self:ShowMessage("ZHANLONG_IS_CLOSE_TODAY")
        return
    end

    if scheduleInfo:IsRegardAsFinished() then
        self:ShowMessage("ZHANLONG_IS_FINISHED")
    else
        if CLuaScheduleMgr.IsInProgress(scheduleInfo.taskId) then
            local curTaskId = nil
            local taskProp = CClientMainPlayer.Inst.TaskProp
            for i = 0, taskProp.CurrentTaskList.Count - 1 do
                local taskId = taskProp.CurrentTaskList[i]
                if Task_Task.GetData(taskId).GamePlay == "ZhanLong" then
                    curTaskId = taskId
                    break
                end
            end

            self:TrackToDoTask(curTaskId)
        else
            self:DoScheduleTask(scheduleInfo)
        end
        self:CloseWnd()
    end
end

-- 宝图任务
function LuaQianYingChuWenMgr:GoTo_BaoTuTask(activityIds)
    local scheduleInfo = self:GetScheduleInfo(activityIds)
    if not scheduleInfo then return end

    if scheduleInfo:IsRegardAsFinished() then
        self:ShowMessage("BAOTUTASK_IS_FINISHED")
    else
        if CLuaScheduleMgr.IsInProgress(scheduleInfo.taskId) then
            local curTaskId = nil
            local taskProp = CClientMainPlayer.Inst.TaskProp
            for i = 0, taskProp.CurrentTaskList.Count - 1 do
                local taskId = taskProp.CurrentTaskList[i]
                local mainTaskId = CommonDefs.DictGetValue_LuaCall(taskProp.CurrentTasks, taskId).MainTaskId
                if taskId == scheduleInfo.taskId or mainTaskId == scheduleInfo.taskId then
                    curTaskId = taskId
                    break
                end
            end

            self:TrackToDoTask(curTaskId)
        else
            self:DoScheduleTask(scheduleInfo)
        end
        self:CloseWnd()
    end
end

-- 高昌
function LuaQianYingChuWenMgr:GoTo_GaoChang(activityIds)
    local scheduleInfo = self:GetScheduleInfo(activityIds)
    if not scheduleInfo then return end

    if scheduleInfo:IsRegardAsFinished() then
        self:ShowMessage("GAOCHANG_IS_FINISHED")
    else
        local ret = self:DoScheduleTask(scheduleInfo)
        if ret == "Close" then
            self:ShowMessage("GAOCHANG_IS_CLOSE", 1)
        else
            self:CloseWnd()
        end
    end
end

-- 帮会联赛
function LuaQianYingChuWenMgr:GoTo_GuildLeague(activityIds)
    local scheduleInfo = self:GetScheduleInfo(activityIds)
    if not scheduleInfo then return end

    if scheduleInfo:IsRegardAsFinished() then
        self:ShowMessage("GUILDLEAGUE_IS_FINISHED")
    else
        local ret = self:DoScheduleTask(scheduleInfo)
        if ret == "Close" then
            self:ShowMessage("GUILDLEAGUE_IS_CLOSE", 1)
        else
            self:CloseWnd()
        end
    end
end

-- 跑商
function LuaQianYingChuWenMgr:GoTo_PaoShang(activityIds)
    local scheduleInfo = self:GetScheduleInfo(activityIds)
    if not scheduleInfo then
        self:ShowMessage("PAOSHANG_IS_CLOSE_TODAY")
        return
    end

    if scheduleInfo:IsRegardAsFinished() then
        self:ShowMessage("PAOSHANG_IS_FINISHED")
    else
        if CLuaScheduleMgr.IsInProgress(scheduleInfo.taskId) then
            g_MessageMgr:ShowMessage("PAOSHANG_IS_GOING")
            self:CloseWnd()
        else
            local ret = self:DoScheduleTask(scheduleInfo)
            if ret == "Close" then
                self:ShowMessage("PAOSHANG_IS_CLOSE", 1)
            else
                self:CloseWnd()
            end
        end
    end
end

-- 情缘任务
function LuaQianYingChuWenMgr:GoTo_QingYuan(activityIds)
    local scheduleInfo = self:GetScheduleInfo(activityIds)
    if not scheduleInfo then return end

    if scheduleInfo:IsRegardAsFinished() then
        self:ShowMessage("QINGYUAN_IS_FINISHED")
    else
        if CLuaScheduleMgr.IsInProgress(scheduleInfo.taskId) then
            local curTaskId = nil
            local taskProp = CClientMainPlayer.Inst.TaskProp
            for i = 0, taskProp.CurrentTaskList.Count - 1 do
                local taskId = taskProp.CurrentTaskList[i]
                local mainTaskId = CommonDefs.DictGetValue_LuaCall(taskProp.CurrentTasks, taskId).MainTaskId
                if taskId == scheduleInfo.taskId or mainTaskId == scheduleInfo.taskId then
                    curTaskId = taskId
                    break
                end
            end

            self:TrackToDoTask(curTaskId)
        else
            self:DoScheduleTask(scheduleInfo)
        end
        self:CloseWnd()
    end
end

-- 蜃妖
function LuaQianYingChuWenMgr:GoTo_ShenYao(activityIds)
    local scheduleInfo = self:GetScheduleInfo(activityIds)
    if not scheduleInfo then return end

    if not scheduleInfo:IsRegardAsFinished() then
        local ret = self:DoScheduleTask(scheduleInfo)
        if ret == "Close" then
            self:ShowMessage("SHENYAO_IS_CLOSE", 1)
        else
            self:CloseWnd()
        end
    end
end

-- 点击任务后的行为，如果任务失败或过期打开放弃任务界面
function LuaQianYingChuWenMgr:TrackToDoTask(taskId)
    if not taskId then return end

    local taskProp = CClientMainPlayer.Inst.TaskProp

    local task = CommonDefs.DictGetValue_LuaCall(taskProp.CurrentTasks, taskId)
    if not task then return end

    if task.IsFailed then
        CTaskInfoMgr.ShowTaskWnd(taskId, 0)
        g_MessageMgr:ShowMessage("TASK_FAILURE_NEED_GIVE_UP")
        return
    end

    if CTaskMgr.Inst:IsFestivalTaskTimeOut(task) then
        CTaskInfoMgr.ShowTaskWnd(taskId, 0)
        return
    end

    if CTaskMgr.Inst:IsTrackingToDoTask(taskId) then
        return
    end
    CTaskMgr.Inst:TrackToDoTask(taskId)
end

-- 点击参加活动后的反应 限时活动判断一下时间
function LuaQianYingChuWenMgr:DoScheduleTask(scheduleInfo, activityType)
    if self:CheckActivityTime(scheduleInfo) ~= "Open" then
        return "Close"
    end

    -- 用于吉星、采薇和守财，判断剩余数量是否为0
    if activityType then
        local count = 0
        CommonDefs.ListIterate(CActivityAlertMgr.Inst.infoList, DelegateFactory.Action_object(function (value)
            if value.activityType == activityType then
                count = value.count
            end
        end))
        if count == 0 then
            return "NoLeft"
        end
    end

    CLuaScheduleMgr.TrackSchedule(scheduleInfo)
    return "Do"
end

-- 显示活动时间检查
function LuaQianYingChuWenMgr:CheckActivityTime(scheduleInfo)
    if scheduleInfo.lastingTime > 0 then
        local now = CServerTimeMgr.Inst:GetZone8Time()
        local nowTime = (now.Hour * 60 + now.Minute) * 60 + now.Second
        local startTime = (scheduleInfo.hour * 60 + scheduleInfo.minute) * 60 + CScheduleMgr.DelayTriggerSecond
        local endTime = (scheduleInfo.hour * 60 + scheduleInfo.minute) * 60 + scheduleInfo.lastingTime * 60
        if nowTime <= startTime then
            return "NotOpen"
        elseif nowTime > endTime then
            return "End"
        else
            return "Open"
        end
    end
    return "Open"
end

-- 获取活动信息
function LuaQianYingChuWenMgr:GetScheduleInfo(activityIds)
    local infos = CScheduleMgr.Inst:GetCanJoinSchedules()

    local tbl = {}
    for i = 0, infos.Count - 1 do
        for j = 0, activityIds.Length - 1 do
            if infos[i].activityId == activityIds[j] then
                table.insert(tbl, infos[i])
            end
        end
    end

    if #tbl == 0 then return nil end
    if #tbl == 1 then return tbl[1] end

    -- 多个活动，优先进行中，其次未开始
    local state = {}
    for i, scheduleInfo in pairs(tbl) do
        state[i] = self:CheckActivityTime(scheduleInfo)
    end

    for i = 1, #tbl do
        if state[i] == "Open" then
            return tbl[i]
        end
    end

    for i = 1, #tbl do
        if state[i] == "NotOpen" then
            return tbl[i]
        end
    end
    return tbl[1]
end

-- 获取师门课业信息
function LuaQianYingChuWenMgr:GetNewbieSchoolScheduleInfo()
    local infos = CScheduleMgr.Inst:GetCanJoinSchedules()

    for i = 0, infos.Count - 1 do
        local scheduleData = Task_Schedule.GetData(infos[i].activityId)
        if scheduleData and scheduleData.Type and scheduleData.Type == "NewBieSchoolTask" then
            return infos[i]
        end
    end
    return nil
end

-- 获取行酒令信息
function LuaQianYingChuWenMgr:GetGuildDrinkScheduleInfo()
    local infos = CScheduleMgr.Inst:GetCanJoinSchedules()

    for i = 0, infos.Count - 1 do
        local scheduleData = Task_Schedule.GetData(infos[i].activityId)
        if scheduleData and scheduleData.Type and scheduleData.Type == "XingJiuLing" then
            return infos[i]
        end
    end
    return nil
end

--#endregion

--#region 推荐词条

-- 获取一个新词条
function LuaQianYingChuWenMgr:GetNewWord()
    if #self.cachedCycleWords == 0 then
        self:SetCycleWords()
    end

    if #self.cachedCycleWords == 0 then
        return 1
    end

    local id = table.remove(self.cachedCycleWords, 1)
    return id
end

-- 生成一个循环周期的所有词条
function LuaQianYingChuWenMgr:SetCycleWords()
    local formulaId = BeginnerGuide_Setting.GetData().ProbabilityFormulaId
    local formula = AllFormulas.Action_Formula[formulaId]
    if not formula then return end

    -- 计算每个词条出现的概率 以及解析“第几天第几条必现”字段
    local probability = {}
    local dayOrder = {}
    local property = self:GetPropertyParams()
    BeginnerGuide_AChuWord.Foreach(function(id, data)
        local tonality = {}
        for i = 0, data.Tonality.Length - 1 do
            table.insert(tonality, data.Tonality[i])
        end

        probability[id] = math.floor(formula.Formula(nil, nil, {property, tonality}))

        if not System.String.IsNullOrEmpty(data.DayOrderToShow) then
            for day, order in string.gmatch(data.DayOrderToShow, "(%d+),(%d+)") do
                day = tonumber(day)
				order = tonumber(order)
                if not dayOrder[day] then
                    dayOrder[day] = {}
                end
                dayOrder[day][order] = id
            end
        end
    end)

    -- 开始洗牌
    self.cachedCycleWords = {}
    local fix = {}
    local openDays = self:GetTotalLoginDays() - CClientMainPlayer.Inst.PlayProp.BeginnerGuideInfo.AlreadyLoginDays + 1
    if dayOrder[openDays] and #dayOrder[openDays] > 0 then
        for i = 1, #dayOrder[openDays] do
            table.insert(self.cachedCycleWords, dayOrder[openDays][i])
            fix[dayOrder[openDays][i]] = true
        end
    end

    local totalCount = BeginnerGuide_AChuWord.GetDataCount()
    local num = math.min(totalCount, BeginnerGuide_Setting.GetData().CycleWordNum) - #self.cachedCycleWords
    if num <= 0 then return end

    local tbl = {}
    for i = 1, totalCount do
        if not fix[i] then
            table.insert(tbl, i)
        end
    end

    -- 从大到小排列
    table.sort(tbl, function(a, b)
        if probability[a] > probability[b] then
            return true
        else
            return false
        end
    end)

    for i = 1, num do
        table.insert(self.cachedCycleWords, tbl[i])
    end
end

-- 获取属性参数
function LuaQianYingChuWenMgr:GetPropertyParams()
    local tbl = {}
    -- 性别
    tbl[1] = EnumToInt(CClientMainPlayer.Inst.Gender)

    -- 等级
    tbl[2] = CClientMainPlayer.Inst.MaxLevel

    -- 是否有师徒关系
    tbl[3] = CClientMainPlayer.Inst.RelationshipProp.ShiFu.Count + CClientMainPlayer.Inst.RelationshipProp.TuDi.Count

    -- 创建天数
    local createTime = CClientMainPlayer.Inst.BasicProp.CreateTime
    tbl[4] =  math.floor((CServerTimeMgr.Inst.timeStamp - createTime) / 86400)

    -- 在线天数
    tbl[5] = self:GetTotalLoginDays()

    -- 银两数
    tbl[6] = CClientMainPlayer.Inst.Silver

    --银票数
    tbl[7] = CClientMainPlayer.Inst.FreeSilver

    -- 历史充值
    tbl[8] = CClientMainPlayer.Inst.ItemProp.Vip.TotalCharge

    -- Vip等级
    tbl[9] = CClientMainPlayer.Inst.ItemProp.Vip.Level

    -- 是否有帮会
    tbl[10] = CClientMainPlayer.Inst.BasicProp.GuildId == 0 and 0 or 1

    -- 好友数量
    tbl[11] = CClientMainPlayer.Inst.RelationshipProp.Friends.Count

    -- 元宝
    tbl[12] = CClientMainPlayer.Inst.YuanBao

    -- 修为
    tbl[13] = CClientMainPlayer.Inst.BasicProp.XiuWeiGrade

    -- 修炼
    tbl[14] = CClientMainPlayer.Inst.BasicProp.XiuLian

    -- 帮贡
    tbl[15] = CClientMainPlayer.Inst.PlayProp.GuildData.Contribution

    -- 本周有没有开科举
    tbl[16] = self:IsKeJuOpen()

    -- 周几
    tbl[17] = EnumToInt(CServerTimeMgr.Inst:GetZone8Time().DayOfWeek)

    -- 本月几号
    tbl[18] = CServerTimeMgr.Inst:GetZone8Time().Day

    -- 几月
    tbl[19] = CServerTimeMgr.Inst:GetZone8Time().Month

    -- 是否拥有家园
    tbl[20] = System.String.IsNullOrEmpty(CClientMainPlayer.Inst.ItemProp.HouseId) and 0 or 1

    return tbl
end

-- 科举是否开放，开了返回1，没开返回0
function LuaQianYingChuWenMgr:IsKeJuOpen()
    local weekBegin = self:GetWeekBegin(CServerTimeMgr.Inst.timeStamp)

    if math.floor(weekBegin / 24 / 3600) % 2 == 1 then
		return 1
    else
        return 0
	end
end

function LuaQianYingChuWenMgr:GetWeekBegin(t)
	local d = os.date("*t", t)
	local day = os.time({year = d.year, month = d.month, day = d.day, hour = 0, min = 0, sec = 0, isdst = false})
	return day - ((d.wday + 5) % 7) * 86400
end

--#endregion

-- 界面存在的情况下，由阿初进行消息提示
function LuaQianYingChuWenMgr:ShowMessage(param1, param2, param3)
    if CUIManager.IsLoaded(CLuaUIResources.QianYingChuWenWnd) then
        g_ScriptEvent:BroadcastInLua("QianYingChuWenShowMessage", param1, param2, param3)
    end
end

-- 关闭窗口
function LuaQianYingChuWenMgr:CloseWnd()
    CUIManager.CloseUI(CLuaUIResources.QianYingChuWenWnd)
end
