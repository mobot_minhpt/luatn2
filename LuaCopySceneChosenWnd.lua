require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local UICommonDef = import "L10.UI.CUICommonDef"
local CButton = import "L10.UI.CButton"
local UISprite = import "UISprite"
local UITable = import "UITable"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local Extensions = import "Extensions"
local CGameVideoMgr = import "L10.Game.CGameVideoMgr"
local CScene = import "L10.Game.CScene"
local CTeamMgr = import "L10.Game.CTeamMgr"

LuaCopySceneChosenWnd = class()

RegistClassMember(LuaCopySceneChosenWnd, "m_itemTemplate")
RegistClassMember(LuaCopySceneChosenWnd, "m_EnterRandomSceneBtn")
RegistClassMember(LuaCopySceneChosenWnd, "m_TitleLabel")
RegistClassMember(LuaCopySceneChosenWnd, "m_Table")
RegistClassMember(LuaCopySceneChosenWnd, "m_ScrollView")

RegistClassMember(LuaCopySceneChosenWnd, "m_SceneList")

function LuaCopySceneChosenWnd:Awake()
    self.m_itemTemplate = self.transform:Find("Anchor/Scroll View/Template").gameObject
    self.m_EnterRandomSceneBtn = self.transform:Find("Anchor/EnterBtn"):GetComponent(typeof(CButton))
    self.m_TitleLabel = self.transform:Find("Wnd_Bg_Secondary_1/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_Table = self.transform:Find("Anchor/Scroll View/Table"):GetComponent(typeof(UITable))
    self.m_ScrollView = self.transform:Find("Anchor/Scroll View"):GetComponent(typeof(CUIRestrictScrollView))
end

function LuaCopySceneChosenWnd:Start()
    self.m_itemTemplate:SetActive(false)

    UIEventListener.Get(self.m_EnterRandomSceneBtn.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnEnterRandomSceneButtonClick(go)
        end
    )
end

function LuaCopySceneChosenWnd:Init()
    self.m_SceneList = LuaCopySceneChosenMgr.CandidateScenes
    if not self.m_SceneList or #self.m_SceneList == 0 then
        self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
        return
    end
    self.m_TitleLabel.text = LocalString.GetString("进入") .. tostring(self.m_SceneList[1].sceneName)
    self.m_EnterRandomSceneBtn.Enabled = LuaCopySceneChosenMgr.ShowRandomButton
    self:LoadItems()
end

function LuaCopySceneChosenWnd:LoadItems()
    Extensions.RemoveAllChildren(self.m_Table.transform)

    for _,scene in pairs(self.m_SceneList) do
        local cur = UICommonDef.AddChild(self.m_Table.gameObject, self.m_itemTemplate)
        cur:SetActive(true)
        self:InitSingleItem(
            cur,
            LocalString.GetString("分线") .. tostring(scene.index),
            scene.playerNum,
            scene.maxPlayerNum,
            scene.sceneId
        )

        UIEventListener.Get(cur).onClick =
            DelegateFactory.VoidDelegate(
            function(go)
                self:OnItemClick(go, scene)
            end
        )
    end

    self.m_Table:Reposition()
    self.m_ScrollView:ResetPosition()
end

function LuaCopySceneChosenWnd:InitSingleItem(go, branchName, playerNumber, maxPlayerNumber, sceneId)
    local branchNameLabel = go.transform:Find("BranchNameLabel"):GetComponent(typeof(UILabel))
    local playerNumberLabel = go.transform:Find("PlayerNumberLabel"):GetComponent(typeof(UILabel))
    local statusIcon = go.transform:Find("Icon"):GetComponent(typeof(UISprite))
    local canEnterLabel = go.transform:Find("CanEnterLabel"):GetComponent(typeof(UILabel))
    local currentFlag = go.transform:Find("CurrentFlag").gameObject
    local teamMemberIcon = go.transform:Find("TeamMemberIcon").gameObject

    local baomanIconSpriteName = "loginwnd_severstate_baoman"
    local busyIconSpriteName = "loginwnd_severstate_busy"
    local normalIconSpriteName = "loginwnd_severstate_smooth"

    branchNameLabel.text = branchName
    playerNumberLabel.text = tostring(playerNumber) .. LocalString.GetString("人")

    if playerNumber >= maxPlayerNumber then
        statusIcon.spriteName = baomanIconSpriteName
    elseif playerNumber >= 80 then
        statusIcon.spriteName = busyIconSpriteName
    else
        statusIcon.spriteName = normalIconSpriteName
    end

    teamMemberIcon:SetActive(self:CheckTeamMember(sceneId))

    Extensions.SetLocalPositionZ(canEnterLabel.transform, playerNumber >= maxPlayerNumber and -1 or 0)

    currentFlag:SetActive(CScene.MainScene and sceneId == CScene.MainScene.SceneId)
end

function LuaCopySceneChosenWnd:OnItemClick(go, scene)

    if CGameVideoMgr.Inst:IsInGameVideoScene() then
        self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
        g_MessageMgr:ShowMessage("STATUS_CONFLICT", LocalString.GetString("观战"))
    else
        if scene.sceneId == CScene.MainScene.SceneId then
            g_MessageMgr:ShowMessage("Message_ChuanSong_Current_Scene_Tips")
        elseif scene.playerNum >= scene.maxPlayerNum then
            g_MessageMgr:ShowMessage("Scene_Player_Count_Max")
        else
            self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
            LuaCopySceneChosenMgr.OnSceneSelected(scene.sceneId)
        end
    end
    
end

function LuaCopySceneChosenWnd:CheckTeamMember(sceneId)
    if CTeamMgr.Inst:TeamExists() then
        local memberList = CTeamMgr.Inst.Members
        for i=0, memberList.Count-1 do
            if memberList[i].m_SceneId == sceneId then
                return true
            end
        end
    end
    return false
end

function LuaCopySceneChosenWnd:OnEnable()
end

function LuaCopySceneChosenWnd:OnDisable()
end

function LuaCopySceneChosenWnd:OnEnterRandomSceneButtonClick(go)
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
    LuaCopySceneChosenMgr.OnSceneSelected("")
end

function LuaCopySceneChosenWnd:OnDestroy()
end
