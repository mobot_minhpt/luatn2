-- Auto Generated!!
local EnumEventType		= import "EnumEventType"
local EventManager		= import "EventManager"
local MsgPackImpl		= import "MsgPackImpl"
local Object			= import "System.Object"
local CPos				= import "L10.Engine.CPos"
local CClientObjectMgr	= import "L10.Game.CClientObjectMgr"
local CWorldEventMgr	= import "L10.Game.CWorldEventMgr"
local CScene			= import "L10.Game.CScene"
local CEffectMgr		= import "L10.Game.CEffectMgr"
local EnumWarnFXType	= import "L10.Game.EnumWarnFXType"
local CUIManager        = import "L10.UI.CUIManager"

local function split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

CLuaWorldEventMgr = {}
CLuaWorldEventMgr.isShowRedPoint = false--信息是否未读，未读红点
CLuaWorldEventMgr.cronMsg = nil			--地裂特效表数据
CLuaWorldEventMgr.Efts = {}				--地裂特效实例临时存储

function CLuaWorldEventMgr:OnDisconnect()
	CLuaWorldEventMgr.isShowRedPoint = false
	CLuaWorldEventMgr.cronMsg = nil
	CLuaWorldEventMgr.Efts = {}
end
g_ScriptEvent:AddListener("GasDisconnect", CLuaWorldEventMgr, "OnDisconnect")

function CLuaWorldEventMgr:OnRenderSceneInit()
	CLuaWorldEventMgr.ProcessWorldFx()
end
g_ScriptEvent:AddListener("RenderSceneInit", CLuaWorldEventMgr, "OnRenderSceneInit")

function CLuaWorldEventMgr.OpenWorldEventHisWnd()
	CLuaWorldEventMgr.isShowRedPoint = false;
	CUIManager.ShowUI(CUIResources.WorldEventHisWnd)
	Gac2Gas.UpdateWorldEvent2019ViewedMessageIdx()
	EventManager.Broadcast(EnumEventType.UpdateWorldEvent)
end


function CLuaWorldEventMgr.ProcessWorldFx(eftid)
	for i = 1,#CLuaWorldEventMgr.Efts do
		local eft = CLuaWorldEventMgr.Efts[i]
		if eft ~= nil then
			eft:Destroy()
		end
	end
	CLuaWorldEventMgr.Efts = {}
	if eftid ~= 0 and eftid ~= nil then
		local MainScene = CScene.MainScene
		if MainScene == nil then return end
		local curscene = tostring(MainScene.SceneTemplateId)
		local data = CLuaWorldEventMgr.cronMsg
		if data == nil then return end
		--local fxid = data.WorldEffect
		local strpos = data.WorldEffectPos
		if strpos == nil or strpos == "" then return end
		local scenes = split(strpos, ";")
		local index = 1
		for i = 1,#scenes do
			local args = split(scenes[i],",")
			
			if args[1] == curscene then
				local pixpos = CPos(args[2],args[3])
				local rot = args[4]
				local fx = CLuaWorldEventMgr.AddWorldFx(eftid,pixpos,rot)
				CLuaWorldEventMgr.Efts[index] = fx
				index = index + 1
			end
		end
	end
end

--增加一个地裂
function CLuaWorldEventMgr.AddWorldFx(fxid,pixpos,dir)
	local fx = CEffectMgr.Inst:AddWorldPositionFX(fxid,pixpos,dir,0,1.0,-1,EnumWarnFXType.None,nil,0,0, nil)
	return fx
end


CWorldEventMgr.m_PlayAnimation_CS2LuaHook = function (engineId, aniName) 
    local cObject = CClientObjectMgr.Inst:GetObject(engineId)
    if cObject ~= nil and cObject.RO ~= nil then
        cObject.RO:DoAni(aniName, false, 0, 1, 0.15, true, 1)
    end
end

-- 老版的世界事件消息，已弃用
--Gas2Gac.SyncWorldEventMessageIdx = function (messageIdx, isPlayFx, isShowRedPoint) 
--    --CWorldEventMgr.Inst.worldMessageIdx = messageIdx;
--    CWorldEventMgr.Inst.worldMessageIdx = messageIdx;
--    CWorldEventMgr.Inst.needPlayFx = isPlayFx
--	CLuaWorldEventMgr.isShowRedPoint = isShowRedPoint
	
--    EventManager.Broadcast(EnumEventType.UpdateWorldEvent)
--end

-- 新版的世界事件消息，华灯新世界 专用
Gas2Gac.SyncWorldEvent2019MessageLieFengInfo = function(serverMessageIdx,viewedMessageIdx, bShowUIEffect, liefengEffectId)
	CWorldEventMgr.Inst.needPlayFx = true
	CWorldEventMgr.Inst.worldMessageIdx = serverMessageIdx
	CWorldEventMgr.Inst.worldReadMessageIdx = viewedMessageIdx
	CLuaWorldEventMgr.isShowRedPoint = serverMessageIdx > viewedMessageIdx
	CLuaWorldEventMgr.cronMsg = ShiJieShiJian_CronMessage2019.GetData(serverMessageIdx)
	--CLuaWorldEventMgr.ShowUIEffect = bShowUIEffect

	if bShowUIEffect then
		local uieft = CLuaWorldEventMgr.cronMsg.UIEffect
		EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFxAssignedPath, {uieft, 2});
	end
	--if liefengEffectId ~= 0 then
		CLuaWorldEventMgr.ProcessWorldFx(liefengEffectId)
	--end
	EventManager.Broadcast(EnumEventType.UpdateWorldEvent)
end

-- 奇异花裂缝修复事件
function Gas2Gac.ReplyLieFengRepairProgressInfo(value, dataUD)
  	g_ScriptEvent:BroadcastInLua("OnQiyishuValueChange",value,dataUD)
end

Gas2Gac.KickNextTempleGroup = function (engineIdTbl, animation) 
    if engineIdTbl ~= nil then
        local data = TypeAs(MsgPackImpl.unpack(engineIdTbl), typeof(MakeGenericClass(List, Object)))
        if data ~= nil then
            do
                local i = 0
                while i < data.Count do
                    local engineId = math.floor(tonumber(data[i] or 0))
                    CWorldEventMgr.PlayAnimation(engineId, animation)
                    i = i + 1
                end
            end
        end
    end
end
