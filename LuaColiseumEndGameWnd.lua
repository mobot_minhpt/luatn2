local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UIProgressBar = import "UIProgressBar"
local UIPanel = import "UIPanel"
local UITexture = import "UITexture"
local Ease = import "DG.Tweening.Ease"
local CUITexture = import "L10.UI.CUITexture"
local CUIFx = import "L10.UI.CUIFx"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CTeamMgr = import "L10.Game.CTeamMgr"

LuaColiseumEndGameWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaColiseumEndGameWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaColiseumEndGameWnd, "DanLabel", "DanLabel", UILabel)
RegistChildComponent(LuaColiseumEndGameWnd, "DanSprites", "DanSprites", GameObject)
RegistChildComponent(LuaColiseumEndGameWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaColiseumEndGameWnd, "Progress", "Progress", UIProgressBar)
RegistChildComponent(LuaColiseumEndGameWnd, "MainInfoView", "MainInfoView", UIPanel)
RegistChildComponent(LuaColiseumEndGameWnd, "FlagBg", "FlagBg", UITexture)
RegistChildComponent(LuaColiseumEndGameWnd, "DataBg1", "DataBg1", UITexture)
RegistChildComponent(LuaColiseumEndGameWnd, "DataBg2", "DataBg2", UITexture)
RegistChildComponent(LuaColiseumEndGameWnd, "DetailDataViewHeaders", "DetailDataViewHeaders", UIPanel)
RegistChildComponent(LuaColiseumEndGameWnd, "Tag1", "Tag1", CUITexture)
RegistChildComponent(LuaColiseumEndGameWnd, "Tag2", "Tag2", CUITexture)
RegistChildComponent(LuaColiseumEndGameWnd, "DetailDataGridView", "DetailDataGridView", UIPanel)
RegistChildComponent(LuaColiseumEndGameWnd, "RankButton", "RankButton", GameObject)
RegistChildComponent(LuaColiseumEndGameWnd, "ShopButton", "ShopButton", GameObject)
RegistChildComponent(LuaColiseumEndGameWnd, "RankLabel", "RankLabel", UILabel)
RegistChildComponent(LuaColiseumEndGameWnd, "ShopScoreLabel", "ShopScoreLabel", UILabel)
RegistChildComponent(LuaColiseumEndGameWnd, "ButtonView", "ButtonView", UIPanel)
RegistChildComponent(LuaColiseumEndGameWnd, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaColiseumEndGameWnd, "ReStartButton", "ReStartButton", GameObject)
RegistChildComponent(LuaColiseumEndGameWnd, "ProcessDiffLabel", "ProcessDiffLabel", UILabel)
RegistChildComponent(LuaColiseumEndGameWnd, "DanUpdateFx", "DanUpdateFx", CUIFx)
RegistChildComponent(LuaColiseumEndGameWnd, "NewGetScoreLabel", "NewGetScoreLabel", UILabel)
RegistChildComponent(LuaColiseumEndGameWnd, "Bg", "Bg", CUITexture)
RegistChildComponent(LuaColiseumEndGameWnd, "DataGrid1", "DataGrid1", UIGrid)
RegistChildComponent(LuaColiseumEndGameWnd, "DataGrid2", "DataGrid2", UIGrid)
RegistChildComponent(LuaColiseumEndGameWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaColiseumEndGameWnd, "DetailDataView", "DetailDataView", GameObject)
RegistChildComponent(LuaColiseumEndGameWnd, "DanTex", "DanTex", CUITexture)
RegistChildComponent(LuaColiseumEndGameWnd, "ProgressLabel", "ProgressLabel", UILabel)
RegistChildComponent(LuaColiseumEndGameWnd, "MainInfoViewFx", "MainInfoViewFx", CUIFx)
RegistChildComponent(LuaColiseumEndGameWnd, "GuangShuFx", "GuangShuFx", CUIFx)
RegistChildComponent(LuaColiseumEndGameWnd, "FenWeiFx", "FenWeiFx", CUIFx)
RegistChildComponent(LuaColiseumEndGameWnd, "LevelNameLabel", "LevelNameLabel", UILabel)
--@endregion RegistChildComponent end
RegistClassMember(LuaColiseumEndGameWnd,"m_ShowInitialFxTick")
RegistClassMember(LuaColiseumEndGameWnd,"m_IsLeader")

function LuaColiseumEndGameWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.RankButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankButtonClick()
	end)

	UIEventListener.Get(self.ShopButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShopButtonClick()
	end)

	UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)

	UIEventListener.Get(self.ReStartButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReStartButtonClick()
	end)

    --@endregion EventBind end
end

function LuaColiseumEndGameWnd:OnEnable()
	g_ScriptEvent:AddListener("TeamInfoChange",self,"OnTeamInfoChange")
end

function LuaColiseumEndGameWnd:OnDisable()
	g_ScriptEvent:RemoveListener("TeamInfoChange",self,"OnTeamInfoChange")
	self:CancelShowInitialFxTick()
	LuaTweenUtils.DOKill(self.transform, false)
end

function LuaColiseumEndGameWnd:OnTeamInfoChange()
	if LuaColiseumMgr.m_LastGameType == 1 then
		return
	end
	self.m_IsLeader = CClientMainPlayer.Inst and CTeamMgr.Inst:IsTeamLeader(CClientMainPlayer.Inst.Id)
	self.ReStartButton.gameObject:SetActive(self.m_IsLeader or not CTeamMgr.Inst:TeamExists())
end

function LuaColiseumEndGameWnd:Init()
	self:InitLevelNameLabel()
	self.Template.gameObject:SetActive(false)
	self.ProcessDiffLabel.gameObject:SetActive(false)
	self.NewGetScoreLabel.gameObject:SetActive(false)
	for i = 0, self.DetailDataView.transform.childCount - 1 do
		self.DetailDataView.transform:GetChild(i).gameObject:SetActive(false)
	end
	self.DetailDataView.gameObject:SetActive(true)
	local shopScore = LuaColiseumMgr.m_ArenaPlayScoreBeforeBattle
	self.ShopScoreLabel.text = shopScore
	self:InitDetailDataView()
	self:InitMainInfoView(LuaColiseumMgr.m_ScoreBeforeBattle)
	self:ShowInitialFx()
	self:OnTeamInfoChange()
	self.ShareButton.gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
end

function LuaColiseumEndGameWnd:InitLevelNameLabel()
    self.LevelNameLabel.text = LuaColiseumMgr:GetLevelName()
end

function LuaColiseumEndGameWnd:InitMainInfoView(score)
	local titleTextArray = {LocalString.GetString("竞技场一对一"),LocalString.GetString("竞技场二对二"),LocalString.GetString("竞技场三对三"),LocalString.GetString("竞技场四对四"),LocalString.GetString("竞技场五对五")}
	local titleText = titleTextArray[LuaColiseumMgr.m_LastGameType]
	self.TitleLabel.text = titleText

	--段位
    local dan = LuaColiseumMgr:GetDan(score)
	local danInfoData = Arena_DanInfo.GetData(dan)

	--段位名称文字和颜色
    local danLabelText, danLabelColor = danInfoData.DanName,danInfoData.DanColor

	 --段位->大段位图标 
	local danTextPaths = {
        "UI/Texture/Transparent/Material/coliseummainwnd_dan_01.mat",
        "UI/Texture/Transparent/Material/coliseummainwnd_dan_02.mat",
        "UI/Texture/Transparent/Material/coliseummainwnd_dan_03.mat",
        "UI/Texture/Transparent/Material/coliseummainwnd_dan_04.mat",
        "UI/Texture/Transparent/Material/coliseummainwnd_dan_05.mat"
    }
	--段位->小段位文字 映射
	local smallDanTexts = {LocalString.GetString("一段"),LocalString.GetString("二段"),LocalString.GetString("三段"),LocalString.GetString("四段"),LocalString.GetString("五段")}
    --进度条数值, 小段位点数
	local processValue, showDanSpritesNum = LuaColiseumMgr:GetProcessValueAndDanSpritesNum(score)
	local smallDan = smallDanTexts[showDanSpritesNum] and showDanSpritesNum or 1
	self.ScoreLabel.text = score
	self.DanLabel.color = NGUIText.ParseColor24(danLabelColor, 0)

	self.DanTex:LoadMaterial(danTextPaths[dan])

	
    self.DanLabel.text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3(LocalString.GetString("%s·%s"),danLabelText,smallDanTexts[smallDan]))
	local childCount = self.DanSprites.transform.childCount
	for i = 0, childCount - 1 do
        local child = self.DanSprites.transform:GetChild(i)
        child.transform:Find("Sprite"):GetComponent(typeof(UITexture)).enabled = i < showDanSpritesNum
    end
    self.Progress.value = processValue / 100
    self.ProgressLabel.text = SafeStringFormat3("%d/%d",processValue, 100)
end

function LuaColiseumEndGameWnd:ShowInitialFx()
	self.MainInfoView.gameObject:SetActive(true)
	self:CancelShowInitialFxTick()
	self.m_ShowInitialFxTick = RegisterTickOnce(function ()
		self:InitScoreChangeAnimation()
		self:CancelShowInitialFxTick()
		self.m_ShowInitialFxTick = RegisterTickOnce(function ()
			local data = LuaColiseumMgr.m_ArenaPlayMyResultData
			if data.playerWinType == 0 then
				self.MainInfoViewFx:LoadFx("fx/ui/prefab/UI_jinjichang_qizhi.prefab")
			end
			self.GuangShuFx:LoadFx("fx/ui/prefab/UI_jianianhuashoupiao_guangshu.prefab")
			self.FenWeiFx:LoadFx("fx/ui/prefab/UI_kuafujingjichang_fenwei.prefab")
			self:OnScoreChangeAnimationEnd()
			self.DetailDataViewHeaders.gameObject:SetActive(true)
			self.DataBg1.gameObject:SetActive(true)
			self.DataBg2.gameObject:SetActive(true)
			self.DetailDataGridView.gameObject:SetActive(true)
			self:CancelShowInitialFxTick()
			self.m_ShowInitialFxTick = RegisterTickOnce(function ()
				self.Tag1.gameObject:SetActive(true)
				self.Tag2.gameObject:SetActive(true)
				self.ButtonView.gameObject:SetActive(true)
			end,1000)
		end,500)
	end,200)
end

function LuaColiseumEndGameWnd:InitScoreChangeAnimation()
	local processValueBeforeBattle,danSpritesNumBeforeBattle,danBeforeBattle = LuaColiseumMgr:GetProcessValueAndDanSpritesNum(LuaColiseumMgr.m_ScoreBeforeBattle)
	local processValueAfterBattle,danSpritesNumAfterBattle,danAfterBattle = LuaColiseumMgr:GetProcessValueAndDanSpritesNum(LuaColiseumMgr.m_ScoreAfterBattle)
	local tweener = LuaTweenUtils.TweenFloat(processValueBeforeBattle,processValueAfterBattle, 0.5,function ( val )
		self.Progress.value = val / 100
    	self.ProgressLabel.text = SafeStringFormat3("%d/%d",val, 100)
	end)
	LuaTweenUtils.SetEase(tweener, Ease.OutCubic)
	LuaTweenUtils.SetTarget(tweener, self.transform)
	local tweener2 = LuaTweenUtils.TweenFloat(LuaColiseumMgr.m_ScoreBeforeBattle,LuaColiseumMgr.m_ScoreAfterBattle, 0.5,function ( val )
		self.ScoreLabel.text = math.ceil(val)
	end)
	LuaTweenUtils.SetEase(tweener2, Ease.OutCubic)
	LuaTweenUtils.SetTarget(tweener2, self.transform)
end

function LuaColiseumEndGameWnd:OnScoreChangeAnimationEnd()
	local scoreChangeValue = LuaColiseumMgr.m_ScoreAfterBattle - LuaColiseumMgr.m_ScoreBeforeBattle
	self.ProcessDiffLabel.color = (scoreChangeValue > 0) and NGUIText.ParseColor24("00ff60", 0) or NGUIText.ParseColor24("ff5050", 0)
	self.ProcessDiffLabel.text = SafeStringFormat3(scoreChangeValue > 0 and "(+%d)" or "(%d)",scoreChangeValue)
	self:InitMainInfoView(LuaColiseumMgr.m_ScoreAfterBattle)
	local shopScore = LuaColiseumMgr.m_ArenaPlayScoreAfterBattle
	self.ShopScoreLabel.text = shopScore
	self.NewGetScoreLabel.text = SafeStringFormat3("+%d",LuaColiseumMgr.m_ArenaPlayScoreAfterBattle - LuaColiseumMgr.m_ArenaPlayScoreBeforeBattle)
	self.ProcessDiffLabel.gameObject:SetActive(true)
	self.NewGetScoreLabel.gameObject:SetActive(LuaColiseumMgr:IsInCurrentSeason())
	local processValueAfterBattle,danSpritesNumAfterBattle,danAfterBattle = LuaColiseumMgr:GetProcessValueAndDanSpritesNum(LuaColiseumMgr.m_ScoreAfterBattle)
	local processValueBeforeBattle,danSpritesNumBeforeBattle,danBeforeBattle = LuaColiseumMgr:GetProcessValueAndDanSpritesNum(LuaColiseumMgr.m_ScoreBeforeBattle)
	local childCount = self.DanSprites.transform.childCount
	for i = 1, childCount do
        local child = self.DanSprites.transform:GetChild(i - 1)
		local fx = child.transform:Find("Fx"):GetComponent(typeof(CUIFx))
        if i > danSpritesNumBeforeBattle and i <= danSpritesNumAfterBattle then
		
			fx:LoadFx("fx/ui/prefab/UI_jinjichang_jiangduanxiao.prefab")
		elseif i <= danSpritesNumBeforeBattle and i > danSpritesNumAfterBattle then
	
			fx:LoadFx("fx/ui/prefab/UI_jinjichang_shengduanxiao.prefab")
		end
    end
	if danAfterBattle ~= danBeforeBattle then
		self.DanUpdateFx:LoadFx((danAfterBattle > danBeforeBattle) and "fx/ui/prefab/UI_jinjichang_shengduanda.prefab" or "fx/ui/prefab/UI_jinjichang_jiangduanda.prefab")
	end
end


function LuaColiseumEndGameWnd:CancelShowInitialFxTick()
	if self.m_ShowInitialFxTick then
		UnRegisterTick(self.m_ShowInitialFxTick)
		self.m_ShowInitialFxTick = nil
	end
end

function LuaColiseumEndGameWnd:InitDetailDataView()
	local winResult1 = LuaColiseumMgr.m_ArenaPlayForce1WinType
	local winResult2 = LuaColiseumMgr.m_ArenaPlayForce2WinType
	local tagSprites = {
		[0] = "UI/Texture/Transparent/Material/liuyipengpengche_sheng.mat",
		[1] = "UI/Texture/Transparent/Material/liuyipengpengche_bai.mat",
		[2] = "UI/Texture/Transparent/Material/liuyipengpengche_ping.mat"
	}
	self.Tag1:LoadMaterial(tagSprites[LuaColiseumMgr.m_ArenaPlayForce1WinType])  
	self.Tag2:LoadMaterial(tagSprites[LuaColiseumMgr.m_ArenaPlayForce2WinType])   
	if #LuaColiseumMgr.m_ArenaPlayForce1ResultData > 0 then
		table.sort(LuaColiseumMgr.m_ArenaPlayForce1ResultData,function (a, b)
			if a.damage == b.damage then
				return a.playerId < b.playerId
			end
			return a.damage > b.damage
		end)
	end
	if #LuaColiseumMgr.m_ArenaPlayForce2ResultData > 0 then
		table.sort(LuaColiseumMgr.m_ArenaPlayForce2ResultData,function (a, b)
			if a.damage == b.damage then
				return a.playerId < b.playerId
			end
			return a.damage > b.damage
		end)
	end
	Extensions.RemoveAllChildren(self.DataGrid1.transform)
	for i =1, #LuaColiseumMgr.m_ArenaPlayForce1ResultData  do
		local t = LuaColiseumMgr.m_ArenaPlayForce1ResultData[i]
		local obj = NGUITools.AddChild(self.DataGrid1.gameObject, self.Template.gameObject)
		self:InitItem(obj, t)
	end
	self.DataGrid1:Reposition()
	Extensions.RemoveAllChildren(self.DataGrid2.transform)
	for i =1, #LuaColiseumMgr.m_ArenaPlayForce2ResultData  do
		local t = LuaColiseumMgr.m_ArenaPlayForce2ResultData[i]
		local obj = NGUITools.AddChild(self.DataGrid2.gameObject, self.Template.gameObject)
		self:InitItem(obj, t)
	end
	self.DataGrid1:Reposition()
end

function LuaColiseumEndGameWnd:InitItem(obj, t)
	local classSprite = obj.transform:Find("ClassSprite"):GetComponent(typeof(UISprite))
	classSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), t.class))
	local labelRootNames = {"Label1","Label2","Label3","Label4","Label5","Label6","Label7","Label8"}
	local dataKeys = {"name","killNum","dieTimes","controllNum","decontrollNum","resurgenceTimes","damage","curativeDose"}
	for i,name in pairs(labelRootNames) do
		local label = obj.transform:Find(name):GetComponent(typeof(UILabel))
		local key = dataKeys[i]
		local val = t[dataKeys[i]]
		
		label.text = (type(val) == "number") and math.floor(tonumber(val)) or val
		if CClientMainPlayer.Inst then
			label.color = (CClientMainPlayer.Inst.Id == t.playerId) and NGUIText.ParseColor24("00ff60", 0) or Color.white
		end
	end
	UIEventListener.Get(classSprite.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CPlayerInfoMgr.ShowPlayerPopupMenu(t.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
	end)
	obj:SetActive(true)
end
--@region UIEvent

function LuaColiseumEndGameWnd:OnRankButtonClick()
	LuaColiseumMgr.m_RankWndType = LuaColiseumMgr.m_LastGameType
    CUIManager.ShowUI(CLuaUIResources.ColiseumRankWnd)
end

function LuaColiseumEndGameWnd:OnShopButtonClick()
	CUIManager.CloseUI(CLuaUIResources.ColiseumShopWnd)
	CUIManager.ShowUI(CLuaUIResources.ColiseumShopWnd)
end

function LuaColiseumEndGameWnd:OnShareButtonClick()
	CUIManager.ShowUI(CLuaUIResources.ColiseumShareWnd)
end

function LuaColiseumEndGameWnd:OnReStartButtonClick()
	Gac2Gas.RequestSignupArena(LuaColiseumMgr.m_LastGameType)
	CUIManager.CloseUI(CLuaUIResources.ColiseumEndGameWnd)
	CUIManager.ShowUI(CLuaUIResources.CrossServerColiseumMainWnd)
end

--@endregion UIEvent

