local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UIInput = import "UIInput"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CIMMgr = import "L10.Game.CIMMgr"
local CFamilyTreeMgr = import "L10.Game.CFamilyTreeMgr"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"
local DefaultChatInputListener = import "L10.UI.DefaultChatInputListener"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CTradeSendMgr = import "L10.Game.CTradeSendMgr"
local Constants = import "L10.Game.Constants"
local CQnSymbolParser = import "CQnSymbolParser"
local CChatInputMgrEParentType = import "L10.UI.CChatInputMgr+EParentType"
local CChatInputLink = import "CChatInputLink"
local CChatInputMgr = import "L10.UI.CChatInputMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CChatLinkMgr = import "CChatLinkMgr"

LuaShiTuGiftGivingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShiTuGiftGivingWnd, "Portrait", "Portrait", CUITexture)
RegistChildComponent(LuaShiTuGiftGivingWnd, "LvLabel", "LvLabel", UILabel)
RegistChildComponent(LuaShiTuGiftGivingWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaShiTuGiftGivingWnd, "OtherPlayerWishItem", "OtherPlayerWishItem", GameObject)
RegistChildComponent(LuaShiTuGiftGivingWnd, "MyWishItem", "MyWishItem", GameObject)
RegistChildComponent(LuaShiTuGiftGivingWnd, "OtherPlayerWishItemLabel", "OtherPlayerWishItemLabel", UILabel)
RegistChildComponent(LuaShiTuGiftGivingWnd, "LeftTitleLabel", "LeftTitleLabel", UILabel)
RegistChildComponent(LuaShiTuGiftGivingWnd, "RightTitleLabel", "RightTitleLabel", UILabel)
RegistChildComponent(LuaShiTuGiftGivingWnd, "GiftGivingItem", "GiftGivingItem", GameObject)
RegistChildComponent(LuaShiTuGiftGivingWnd, "EmotionButton", "EmotionButton", GameObject)
RegistChildComponent(LuaShiTuGiftGivingWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaShiTuGiftGivingWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaShiTuGiftGivingWnd, "GiveGiftButton", "GiveGiftButton", GameObject)
RegistChildComponent(LuaShiTuGiftGivingWnd, "QnCheckBox", "QnCheckBox", QnCheckBox)
RegistChildComponent(LuaShiTuGiftGivingWnd, "UseItemLabel1", "UseItemLabel1", UILabel)
RegistChildComponent(LuaShiTuGiftGivingWnd, "UseItemLabel2", "UseItemLabel2", UILabel)
RegistChildComponent(LuaShiTuGiftGivingWnd, "LimitMoneyLabel", "LimitMoneyLabel", UILabel)
RegistChildComponent(LuaShiTuGiftGivingWnd, "UseItemTexture", "UseItemTexture", CUITexture)
RegistChildComponent(LuaShiTuGiftGivingWnd, "Best", "Best", GameObject)
RegistChildComponent(LuaShiTuGiftGivingWnd, "StatusText", "StatusText", UIInput)
RegistChildComponent(LuaShiTuGiftGivingWnd, "TitleLabel", "TitleLabel", UILabel)
--@endregion RegistChildComponent end
RegistClassMember(LuaShiTuGiftGivingWnd,"m_DefaultChatInputListener")
RegistClassMember(LuaShiTuGiftGivingWnd,"m_ExistingLinks")
RegistClassMember(LuaShiTuGiftGivingWnd,"m_AddGift")
RegistClassMember(LuaShiTuGiftGivingWnd,"m_Cost")
RegistClassMember(LuaShiTuGiftGivingWnd,"m_IsOnline")
RegistClassMember(LuaShiTuGiftGivingWnd,"m_IsUseItem")
RegistClassMember(LuaShiTuGiftGivingWnd,"m_IsZengSongItem")

function LuaShiTuGiftGivingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPortraitClick()
	end)


	
	UIEventListener.Get(self.OtherPlayerWishItem.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOtherPlayerWishItemClick()
	end)


	
	UIEventListener.Get(self.MyWishItem.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMyWishItemClick()
	end)


	
	UIEventListener.Get(self.GiftGivingItem.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGiftGivingItemClick()
	end)


	
	UIEventListener.Get(self.EmotionButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEmotionButtonClick()
	end)


	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.GiveGiftButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGiveGiftButtonClick()
	end)


	
	UIEventListener.Get(self.UseItemTexture.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUseItemTextureClick()
	end)


	UIEventListener.Get(self.UseItemLabel2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUseItemLabel2Click()
	end)

    --@endregion EventBind end
end

function LuaShiTuGiftGivingWnd:Init()
	self.TitleLabel.text = LuaShiTuMgr.s_SendGiftToShiFu and LocalString.GetString("礼馈师恩") or LocalString.GetString("礼赠弟子")
	self:InitRightView()
	self:InitLeftView()
end

function LuaShiTuGiftGivingWnd:OnEnable()
	g_ScriptEvent:AddListener("OnShopMallItemSelectWndSelectItem", self, "OnSelectShopMallItemItem")
	g_ScriptEvent:AddListener("OnSetShiTuGiftWishSucc", self, "OnSetShiTuGiftWishSucc")
end

function LuaShiTuGiftGivingWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnShopMallItemSelectWndSelectItem", self, "OnSelectShopMallItemItem")
	g_ScriptEvent:RemoveListener("OnSetShiTuGiftWishSucc", self, "OnSetShiTuGiftWishSucc")
	CLuaShopMallMgr.m_ShopMallSelectItem = nil
end

function LuaShiTuGiftGivingWnd:OnSetShiTuGiftWishSucc(isShifu, otherPlayerId, winshItemId, num, content)
	-- if otherPlayerId == LuaShiTuMgr.m_PlayerIdReceivedGift or isShifu then
		local itemId2 = winshItemId
		local itemData2 = Item_Item.GetData(itemId2)
		local numLabel2 = self.MyWishItem.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
		numLabel2.gameObject:SetActive(num > 1)
		numLabel2.text = num
		self.MyWishItem.transform:Find("AddItemBtn").gameObject:SetActive(itemData2 == nil)
		if itemData2 then
			self.MyWishItem.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(itemData2.Icon)
		end
	-- end
end

function LuaShiTuGiftGivingWnd:OnSelectShopMallItemItem()
	local itemData = Item_Item.GetData(CLuaShopMallMgr.m_ShopMallSelectItem.itemId)
	local num = CLuaShopMallMgr.m_ShopMallSelectItem.num
	if LuaShiTuMgr.m_SelectShopItemType == 0 then
		self.m_AddGift = CLuaShopMallMgr.m_ShopMallSelectItem
		self.GiftGivingItem.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)
		local numLabel = self.GiftGivingItem.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
		numLabel.gameObject:SetActive(num > 1)
		numLabel.text = num
		self.GiftGivingItem.transform:Find("AddItemBtn").gameObject:SetActive(false)
		local shiTuGiftSpeakerItemId = ShiTu_Setting.GetData().ShiTuGiftSpeakerItemId
		local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, shiTuGiftSpeakerItemId)
		local mallData = Mall_LingYuMall.GetData(shiTuGiftSpeakerItemId)
		self.m_Cost = CLuaShopMallMgr.m_ShopMallSelectItem.price
		local price = CLuaShopMallMgr.m_ShopMallSelectItem.price + ((not default and self.m_IsUseItem) and mallData.Jade or 0)
		self.QnCostAndOwnMoney:SetCost(price) 
	end
end

function LuaShiTuGiftGivingWnd:InitLeftView()
	self:InitWishItems()
	local info = CIMMgr.Inst:GetBasicInfo(LuaShiTuMgr.m_PlayerIdReceivedGift)
	if not info then
		CUIManager.CloseUI(CLuaUIResources.ShiTuGiftGivingWnd)
		return
	end
	self.Best.gameObject:SetActive(false)
	self.Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(info.Class, info.Gender, -1), false)
	self.LvLabel.text = info.Level
	self.LvLabel.gameObject:SetActive(false)
	self.LvLabel.color = NGUIText.ParseColor24(info.XianFanStatus > 0 and "fe7900" or "6E5038", 0) 
	self.NameLabel.text = info.Name
	local titleName = LocalString.GetString("吾师")
	local mainPlayer = CClientMainPlayer.Inst
	if not LuaShiTuMgr.s_SendGiftToShiFu and mainPlayer then
		local tudiDict = mainPlayer.RelationshipProp.TuDi
		local t = {}
		CommonDefs.DictIterate(tudiDict, DelegateFactory.Action_object_object(function(key,val)
			if val.Finished == 0 then
				table.insert(t,val)
			end
		end))
		table.sort( t,function(a,b)
			if CFamilyTreeMgr.Instance:CheckRuShiPlayer(a.PlayerId) then
				return true
			end
			if CFamilyTreeMgr.Instance:CheckRuShiPlayer(b.PlayerId) then
				return false
			end
			return a.Time < b.Time
		end )
		local hasRuShi = CFamilyTreeMgr.Instance:CheckRuShiPlayer(t[1].PlayerId)
		local j = 1
		for i = 1, #t do
			local indexText = ShiTu_Setting.GetData().MemberSerialNumber[j - 1]
			local isWaiMen = t[i] and (t[i].Extra:GetBit(2)) or false
			if not isWaiMen then
				j = j + 1
			end
			local text = hasRuShi and LocalString.GetString("入室弟子") or (isWaiMen and LocalString.GetString("外门弟子") or SafeStringFormat3(LocalString.GetString("%s徒弟"),indexText))
			if t[i].PlayerId == LuaShiTuMgr.m_PlayerIdReceivedGift then
				titleName = text
			end
		end
	end
	self.LeftTitleLabel.text = titleName
	for i,playerInfo in pairs(LuaShiTuMgr.m_ShiTuShiMenPlayerInfo) do
		if playerInfo.playerId == LuaShiTuMgr.m_PlayerIdReceivedGift then
			self.m_IsOnline = playerInfo.isOnline
			Extensions.SetLocalPositionZ(self.Portrait.transform, playerInfo.isOnline and 0 or - 1)
		end
	end
	for i,playerInfo in pairs(LuaShiTuMgr.m_WaiMenDiZiInfo) do
		if playerInfo.playerId == LuaShiTuMgr.m_PlayerIdReceivedGift then
			self.m_IsOnline = playerInfo.isOnline
			Extensions.SetLocalPositionZ(self.Portrait.transform, playerInfo.isOnline and 0 or - 1)
		end
	end
end

function LuaShiTuGiftGivingWnd:InitRightView()
	self:InitDefaultChatInputListener()
	self:InitQnCostAndOwnMoney()
	self.QnCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (value)
        self:OnQnCheckBoxChanged(value)
    end)
	local shiTuGiftSpeakerItemId = ShiTu_Setting.GetData().ShiTuGiftSpeakerItemId
	local itemData = Item_Item.GetData(shiTuGiftSpeakerItemId)
	local mallData = Mall_LingYuMall.GetData(shiTuGiftSpeakerItemId)
	local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, shiTuGiftSpeakerItemId)
	self.UseItemTexture.gameObject:SetActive(true)
	self.UseItemTexture:LoadMaterial(itemData.Icon)
	self.UseItemLabel1.text = ""--SafeStringFormat3(LocalString.GetString("消耗%d"), mallData.Jade)
	self.UseItemLabel2.text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3(LocalString.GetString("使用<link button=%s,ItemInfo,%s>发送或消耗%d"),itemData.Name, shiTuGiftSpeakerItemId,mallData.Jade))
	self.LimitMoneyLabel.text = LuaShiTuMgr.m_PrepareGiftRet.remainJadeLimit
	self.RightTitleLabel.text = LuaShiTuMgr.s_SendGiftToShiFu and LocalString.GetString("赠予师父") or LocalString.GetString("赠予徒弟")
	self.QnCheckBox:SetSelected(false, false)
	self.QnCostAndOwnMoney:SetCost(0) 
	local numLabel = self.GiftGivingItem.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
	numLabel.gameObject:SetActive(false)
	numLabel.text = ""
end

function LuaShiTuGiftGivingWnd:OnQnCheckBoxChanged(value)
	self.m_IsZengSongItem = false
	self.m_IsUseItem = value
	local shiTuGiftSpeakerItemId = ShiTu_Setting.GetData().ShiTuGiftSpeakerItemId
	local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, shiTuGiftSpeakerItemId)
	local mallData = Mall_LingYuMall.GetData(shiTuGiftSpeakerItemId)
	local cost = self.m_Cost and self.m_Cost or 0
	local price = cost + ((not default and self.m_IsUseItem) and mallData.Jade or 0)
	if default or (not value) then
		self.QnCostAndOwnMoney:SetCost(price) 
	else
		self.QnCheckBox:SetSelected(not value, true)
		self.m_IsUseItem = false
		local msg = g_MessageMgr:FormatMessage("ShiTuGiftGivingWnd_UseShiTuGiftSpeakerItemId_Confirm")
		MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
			self.m_IsZengSongItem = true
			self.m_IsUseItem = true
			self.QnCostAndOwnMoney:SetCost(price) 
			self.QnCheckBox:SetSelected(value, true)
		end),nil,LocalString.GetString("确认"),LocalString.GetString("取消"),false)
	end
end

function LuaShiTuGiftGivingWnd:InitQnCostAndOwnMoney()
	self.QnCostAndOwnMoney:SetType(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
	self.QnCheckBox:SetSelected(false, false)
end

function LuaShiTuGiftGivingWnd:InitWishItems()
	self.OtherPlayerWishItemLabel.text = LuaShiTuMgr.s_SendGiftToShiFu and LocalString.GetString("师父心愿") or LocalString.GetString("徒弟心愿")
	local itemId1 = LuaShiTuMgr.m_PrepareGiftRet.targetWinshItemId
	local itemData1 = Item_Item.GetData(itemId1)
	local numLabel1 = self.OtherPlayerWishItem.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
	numLabel1.gameObject:SetActive(LuaShiTuMgr.m_PrepareGiftRet.targetWishItemCount > 1)
	numLabel1.text = LuaShiTuMgr.m_PrepareGiftRet.targetWishItemCount
	self.OtherPlayerWishItem.transform:Find("AddItemBtn").gameObject:SetActive(false)
	self.OtherPlayerWishItem.transform:Find("NoneItemLabel").gameObject:SetActive(itemData1 == nil)
	if itemData1 then
		self.OtherPlayerWishItem.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(itemData1.Icon)
	end
	local itemId2 = LuaShiTuMgr.m_PrepareGiftRet.myWinshItemId
	local itemData2 = Item_Item.GetData(itemId2)
	local numLabel2 = self.MyWishItem.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
	numLabel2.gameObject:SetActive(LuaShiTuMgr.m_PrepareGiftRet.myWishItemCount > 1)
	numLabel2.text = LuaShiTuMgr.m_PrepareGiftRet.myWishItemCount
	self.MyWishItem.transform:Find("AddItemBtn").gameObject:SetActive(itemData2 == nil)
	if itemData2 then
		self.MyWishItem.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(itemData2.Icon)
	end
end

--@region UIEvent

function LuaShiTuGiftGivingWnd:OnPortraitClick()
	CPlayerInfoMgr.ShowPlayerPopupMenu(LuaShiTuMgr.m_PlayerIdReceivedGift, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, Vector3.zero, AlignType.Default)
end

function LuaShiTuGiftGivingWnd:OnOtherPlayerWishItemClick()
	if LuaShiTuMgr.m_PrepareGiftRet.targetWinshItemId > 0 then
		LuaShiTuMgr.m_WishGifType = 0
		CUIManager.ShowUI(CLuaUIResources.ShiTuWishGiftWnd)
	else
		g_MessageMgr:ShowMessage(LuaShiTuMgr.s_SendGiftToShiFu and "ShiTuGiftGivingWnd_ShiFuUnAddWishItem" or "ShiTuGiftGivingWnd_TuDiUnAddWishItem")
	end
end

function LuaShiTuGiftGivingWnd:OnMyWishItemClick()
	LuaShiTuMgr.m_WishGifType = 1
	CUIManager.ShowUI(CLuaUIResources.ShiTuWishGiftWnd)
end

function LuaShiTuGiftGivingWnd:OnGiftGivingItemClick()
	LuaShiTuMgr.m_SelectShopItemType = 0
	CUIManager.ShowUI(CLuaUIResources.ShopMallItemSelectWnd)
end

function LuaShiTuGiftGivingWnd:OnEmotionButtonClick()
	CChatInputMgr.ShowChatInputWnd(CChatInputMgrEParentType.PersonalSpace, self.m_DefaultChatInputListener, self.EmotionButton.gameObject, EChatPanel.Undefined, 0, 0)
end

function LuaShiTuGiftGivingWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("ShiTuGiftGivingWnd_ReadMe")
end

function LuaShiTuGiftGivingWnd:OnGiveGiftButtonClick()
	if not self.m_AddGift then
		g_MessageMgr:ShowMessage("ShiTuGiftGivingWnd_NotAddGift")
		return
	end
	local text = self:GetSendMsg()
	if text == nil then
		return
	end
	local cost = self.m_Cost and self.m_Cost or 0
	if LuaShiTuMgr.m_PrepareGiftRet.remainJadeLimit < cost then
		g_MessageMgr:ShowMessage("Zengsong_Quota_Not_Enough", math.floor(LuaShiTuMgr.m_PrepareGiftRet.remainJadeLimit))
		return
	end
	if not self.m_IsOnline then
		g_MessageMgr:ShowMessage("ShiTuGiftGivingWnd_Not_Online")
		return
	end

	local shiTuGiftSpeakerItemId = ShiTu_Setting.GetData().ShiTuGiftSpeakerItemId
	local itemData = Item_Item.GetData(shiTuGiftSpeakerItemId)
	local mallData = Mall_LingYuMall.GetData(shiTuGiftSpeakerItemId)
	local msg = g_MessageMgr:FormatMessage("ShiTuGiftGivingWnd_UseShiTuGiftSpeakerItemId_Confirm2", mallData.Jade,itemData.Name)
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		Gac2Gas.RequestSendShiTuGift(LuaShiTuMgr.m_PlayerIdReceivedGift, self.m_IsUseItem, text, self.m_AddGift.itemId, self.m_AddGift.num)
	end),nil,LocalString.GetString("确认"),LocalString.GetString("取消"),false)
end

function LuaShiTuGiftGivingWnd:OnUseItemTextureClick()
	CItemInfoMgr.ShowLinkItemTemplateInfo(ShiTu_Setting.GetData().ShiTuGiftSpeakerItemId)
end

function LuaShiTuGiftGivingWnd:OnUseItemLabel2Click()
	local url = self.UseItemLabel2:GetUrlAtPosition(UICamera.lastWorldPosition)
	if url ~= nil then
		CChatLinkMgr.ProcessLinkClick(url, nil)
	end
end
--@endregion UIEvent

function LuaShiTuGiftGivingWnd:GetSendMsg()
	local input = self.StatusText
	local sendInfo = input.value
	if System.String.IsNullOrEmpty(sendInfo) then
        g_MessageMgr:ShowMessage("ENTER_TEXT_TIP")
        return nil
    end
	input.value = CQnSymbolParser.FilterExceededEmoticons(StringTrim(sendInfo), 25)
	local msg = input.value
	local sendText = msg
	sendText = CWordFilterMgr.Inst:DoFilterOnSendPersonalSpace(sendText, true)
	return sendText
end

function LuaShiTuGiftGivingWnd:InitDefaultChatInputListener()
	self.m_ExistingLinks =  CreateFromClass(MakeGenericClass(Dictionary, Int32, CChatInputLink))
	self.m_DefaultChatInputListener = DefaultChatInputListener()
	self.m_DefaultChatInputListener:SetOnInputEmoticonFunc(function (prefix)
		self:OnInputEmoticon(prefix)
	end)
	self.m_DefaultChatInputListener:SetOnInputItemFunc(function (item)
		self:OnInputItemLink(item)
	end)
	self.m_DefaultChatInputListener:SetOnInputBabyFunc(function (babyId, babyName)
		self:OnInputBabyLink(babyId, babyName)
	end)
	self.m_DefaultChatInputListener:SetOnLingShouFunc(function (lingshouId, lingshouName)
		self:OnInputLingShouLink(lingshouId, lingshouName)
	end)
	self.m_DefaultChatInputListener:SetOnInputWeddingItemFunc(function (item)
		self:OnInputWeddingItemLink(item)
	end)
	self.m_DefaultChatInputListener:SetOnInputAchievementFunc(function (achievementId)
		self:OnInputAchievement(achievementId)
	end)
end

function LuaShiTuGiftGivingWnd:OnInputEmoticon(prefix)
	local input = self.StatusText
	self.StatusText.label.EnableEmotion = false
	input.text = SafeStringFormat3("%s#%s",input.text, prefix) 
end

function LuaShiTuGiftGivingWnd:OnInputItemLink(item)
	local text = nil
	local input = self.StatusText
	text = CChatInputLink.AppendItemLink(input.value, item, self.m_ExistingLinks)
	self.StatusText.label.EnableEmotion = false
	input.text = text
end

function LuaShiTuGiftGivingWnd:OnInputBabyLink(babyId, babyName)
	local text = nil
	local input = self.StatusText
	text = CChatInputLink.AppendBabyLink(input.value, babyId, babyName, self.m_ExistingLinks)
	self.StatusText.label.EnableEmotion = false
	input.text = text
end

function LuaShiTuGiftGivingWnd:OnInputLingShouLink(lingshouId, lingshouName)
	local text = nil
	local input = self.StatusText
	text = CChatInputLink.AppendLingShouLink(input.value, lingshouId, lingshouName, self.m_ExistingLinks)
	self.StatusText.label.EnableEmotion = false
	input.text = text
end

function LuaShiTuGiftGivingWnd:OnInputWeddingItemLink(item)
	local text = nil
	local input = self.StatusText
	text = CChatInputLink.AppendWeddingItemLink(input.value, item, self.m_ExistingLinks)
	self.StatusText.label.EnableEmotion = false
	input.text = text
end

function LuaShiTuGiftGivingWnd:OnInputAchievement(achievementId)
	local text = nil
	local input = self.StatusText
	text = CChatInputLink.AppendAchievementLink(input.value, achievementId, self.m_ExistingLinks)
	self.StatusText.label.EnableEmotion = false
	input.text = text
end
