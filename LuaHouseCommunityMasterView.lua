local CHouseCommunityMasterView = import "L10.UI.CHouseCommunityMasterView"
local CCommunityMgr = import "L10.UI.CCommunityMgr"
local CellIndexType = import "L10.UI.UITableView+CellIndexType"
local CellIndex = import "L10.UI.UITableView+CellIndex"

function CHouseCommunityMasterView.m_OnCollectHouseUpdate_CS2LuaHook(this)
    local curSelectedIndex = this.SelectedIndex
    if not curSelectedIndex then
        curSelectedIndex = CellIndex(CCommunityMgr.Inst.CurCommunitySection,CCommunityMgr.Inst.CurCommunityRow, CCommunityMgr.Inst.CurIndexType)
    end
    curSelectedIndex.section = math.max(0,math.min(curSelectedIndex.section,this:NumberOfSectionsInTableView()-1))
    curSelectedIndex.row = math.max(0,math.min(curSelectedIndex.row,this:NumberOfRowsInSection(curSelectedIndex.section)-1))

    if curSelectedIndex.section ~= (this:NumberOfSectionsInTableView() - 1) then
        return
    end

    this.mbNeedQueryHouse = false
    this:LoadData(curSelectedIndex, true)
    this.mbNeedQueryHouse = true
end
function CHouseCommunityMasterView.m_hookOnSectionClicked(this, index, expanded)

    if CCommunityMgr.Inst.CurCommunitySection ~= 0 then
        this:UpdateSelection(CellIndex(CCommunityMgr.Inst.CurCommunitySection, CCommunityMgr.Inst.CurCommunityRow, CellIndexType.Section))
    end

    CCommunityMgr.Inst.CurCommunitySection = index.section
    CCommunityMgr.Inst.CurCommunityRow = index.row
    CCommunityMgr.Inst.CurIndexType = CellIndexType.Section

    if index.section == 0 then
        CCommunityMgr.Inst.CurIsPublicCommunityExpanded = not CCommunityMgr.Inst.CurIsPublicCommunityExpanded
        this:LoadData(CellIndex(0, 0, CellIndexType.Section), false);
    end

    if CCommunityMgr.Inst.CurIsPublicCommunityExpanded and this:NumberOfRowsInSection(index.section) > 0 then
        if (index.section ~= 0) and (index.section ~= CCommunityMgr.Inst.sections.Count - 1) then
            this:OnRowSelected(CellIndex(index.section, 0, CellIndexType.Row))
            if expanded then
                this:SetRowSelected(CellIndex(index.section, 0, CellIndexType.Row))
            end
        end
    end
end