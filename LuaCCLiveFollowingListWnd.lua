require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"


LuaCCLiveFollowingListWnd = class()
RegistClassMember(LuaCCLiveFollowingListWnd,"m_CloseBtn")
RegistClassMember(LuaCCLiveFollowingListWnd,"m_TableViewDataSource")
RegistClassMember(LuaCCLiveFollowingListWnd,"m_TableView")
RegistClassMember(LuaCCLiveFollowingListWnd,"m_DisplayResult")


function LuaCCLiveFollowingListWnd:Awake()
    
end

function LuaCCLiveFollowingListWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaCCLiveFollowingListWnd:Init()
	self.m_CloseBtn = self.transform:Find("Wnd_Bg_Secondary_3/CloseButton").gameObject
	CommonDefs.AddOnClickListener(self.m_CloseBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

	self.m_TableView = self.transform:Find("TableView"):GetComponent(typeof(QnTableView))
	self.m_TableViewDataSource=DefaultTableViewDataSource.Create(function() 
			return self:NumberOfItems()  
		end, function(item, index)
			self:InitItem(item, index)
		end)
    self.m_TableView.m_DataSource=self.m_TableViewDataSource

    self:LoadData()
end

function LuaCCLiveFollowingListWnd:LoadData()
	--获取关注的主播
	self.m_DisplayResult = {}
	for i=1,#LuaCCChatMgr.FollowingTbl do
		local data = LuaCCChatMgr.FollowingTbl[i]
		table.insert(self.m_DisplayResult, data)
	end
	--根据主播被关注的数量从大到小排序，如果关注数量相同，用ccid来进行唯一性排序，不考虑在线和优先级
	table.sort(self.m_DisplayResult, function (a, b)
		local followANum = a.followed
		local followBNum = b.followed
		if followANum == followBNum then
			return a.ccid < b.ccid
		else
			return followBNum < followANum
		end
	end)

	self.m_TableView:ReloadData(true,false)
end

function LuaCCLiveFollowingListWnd:NumberOfItems()
	return #self.m_DisplayResult
end

function LuaCCLiveFollowingListWnd:InitItem(item, index)
	local data = self.m_DisplayResult[index+1]
	if not data then return end
	local script = item.gameObject:GetComponent(typeof(CCommonLuaScript))
	script.m_LuaSelf:InitByZhuboInfo(data)
end

function LuaCCLiveFollowingListWnd:OnDestroy()
	
end
