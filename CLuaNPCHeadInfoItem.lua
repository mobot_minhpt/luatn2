local CClientNpc = import "L10.Game.CClientNpc"
local CNPCHeadInfoItem = import "L10.UI.CNPCHeadInfoItem"
local Constants = import "L10.Game.Constants"

CNPCHeadInfoItem.m_hookSetNPCTaskStatusIcon = function (this, npc)
    if npc ~= nil then
        local npc_npc = NPC_NPC.GetData(npc.TemplateId)
        if npc_npc and npc_npc.NotShowTaskStatus == 1 then
            this:HideStatusSprite()
            return
        end
        repeat
            local default = npc.TaskStatus
            if default == CClientNpc.NPCTaskStatus.TaskNotFound then
                this:HideStatusSprite()
                break
            elseif default == CClientNpc.NPCTaskStatus.SingleTaskFinished then
                if npc.TaskOverGrade then
                    this:ShowStatusSprite(Constants.NPCTaskIcon_Period_Red)
                else
                    this:ShowStatusSprite(Constants.NPCTaskIcon_Period_Green)
                end
                break
            elseif default == CClientNpc.NPCTaskStatus.RepeatableTaskFinished then
                if npc.TaskOverGrade then
                    this:ShowStatusSprite(Constants.NPCTaskIcon_Period_Red)
                else
                    this:ShowStatusSprite(Constants.NPCTaskIcon_Period_Blue)
                end
                break
            elseif default == CClientNpc.NPCTaskStatus.SingleTaskReceived then
                this:ShowStatusSprite(Constants.NPCTaskIcon_Period_Red)
                break
            elseif default == CClientNpc.NPCTaskStatus.RepeatableTaskReceived then
                this:ShowStatusSprite(Constants.NPCTaskIcon_Period_Red)
                break
            elseif default == CClientNpc.NPCTaskStatus.SingleTaskAcceptable then
                if npc.TaskOverGrade then
                    this:ShowStatusSprite(Constants.NPCTaskIcon_QuestionMark_Red)
                else
                    this:ShowStatusSprite(Constants.NPCTaskIcon_QuestionMark_Green)
                end
                break
            elseif default == CClientNpc.NPCTaskStatus.RepeatableTaskAcceptable then
                if npc.TaskOverGrade then
                    this:ShowStatusSprite(Constants.NPCTaskIcon_QuestionMark_Red)
                else
                    this:ShowStatusSprite(Constants.NPCTaskIcon_QuestionMark_Blue)
                end
                break
            elseif default == CClientNpc.NPCTaskStatus.SingleTaskNotFitCurrent then
                this:ShowStatusSprite(Constants.NPCTaskIcon_QuestionMark_Red)
                break
            elseif default == CClientNpc.NPCTaskStatus.RepeatableTaskNotFitCurrent then
                this:ShowStatusSprite(Constants.NPCTaskIcon_QuestionMark_Red)
                break
            elseif default == CClientNpc.NPCTaskStatus.SingleTaskFindNPC then
                if npc.TaskOverGrade then
                    this:ShowStatusSprite(Constants.NPCTaskIcon_Period_Red)
                else
                    this:ShowStatusSprite(Constants.NPCTaskIcon_Period_Green)
                end
                break
            elseif default == CClientNpc.NPCTaskStatus.RepeatableTaskFindNPC then
                if npc.TaskOverGrade then
                    this:ShowStatusSprite(Constants.NPCTaskIcon_Period_Red)
                else
                    this:ShowStatusSprite(Constants.NPCTaskIcon_Period_Blue)
                end
                break
            elseif default == CClientNpc.NPCTaskStatus.SingleTaskFindNPCSubmitEquipNotFinished then
                this:ShowStatusSprite(Constants.NPCTaskIcon_Period_Red)
                break
            elseif default == CClientNpc.NPCTaskStatus.RepeatableTaskFindNPCSubmitEquipNotFinished then
                this:ShowStatusSprite(Constants.NPCTaskIcon_Period_Red)
                break
            elseif default == CClientNpc.NPCTaskStatus.SingleTaskFindNPCSubmitEquipFinished then
                this:ShowStatusSprite(Constants.NPCTaskIcon_Period_Green)
                break
            elseif default == CClientNpc.NPCTaskStatus.RepeatableTaskFindNPCSubmitEquipFinished then
                this:ShowStatusSprite(Constants.NPCTaskIcon_Period_Blue)
                break
            else
                this:HideStatusSprite()
                break
            end
        until 1
    end
end

CNPCHeadInfoItem.m_hookOnUpdateSpeIcon = function (this)
    if not CNPCHeadInfoItem.SubUIAtlas then CNPCHeadInfoItem.SubUIAtlas = this.speIconSprite.atlas end
    if not CNPCHeadInfoItem.MainUIAtlas then CNPCHeadInfoItem.MainUIAtlas = this.nameBoard.bg.atlas end

    local info = LuaGamePlayMgr:GetNpcHeadSpeIconInfo(this.objId)
    if info then
        this.speIconContainer.gameObject:SetActive(true)
        this.speIconSprite.atlas = (info.atlas and info.atlas == "MainUI") and CNPCHeadInfoItem.MainUIAtlas or CNPCHeadInfoItem.SubUIAtlas
        this.speIconSprite.spriteName = info.spriteName
        this.speIconSprite:MakePixelPerfect()

        this.speIconLabel.text = info.label and info.label or ""
        this.speIconLabel.color = info.labelColor and info.labelColor or Color.white
    else
        this.speIconContainer.gameObject:SetActive(false)
    end
end
