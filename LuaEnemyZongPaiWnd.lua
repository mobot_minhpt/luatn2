local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"

LuaEnemyZongPaiWnd = class()
RegistClassMember(LuaEnemyZongPaiWnd,"m_TableViewDataSource")
RegistClassMember(LuaEnemyZongPaiWnd,"m_TableView")
RegistClassMember(LuaEnemyZongPaiWnd,"m_Info")
RegistClassMember(LuaEnemyZongPaiWnd,"m_VoidLabel")
RegistClassMember(LuaEnemyZongPaiWnd,"m_QuestBtn")
RegistClassMember(LuaEnemyZongPaiWnd,"m_ConfirmBtn")
RegistClassMember(LuaEnemyZongPaiWnd,"m_CurrentSect")
RegistClassMember(LuaEnemyZongPaiWnd,"m_CurrentSectName")
RegistClassMember(LuaEnemyZongPaiWnd,"m_Desc1")
RegistClassMember(LuaEnemyZongPaiWnd,"m_Desc2")
RegistClassMember(LuaEnemyZongPaiWnd,"m_Action")

function LuaEnemyZongPaiWnd:Init()
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId ~= 0 then
        Gac2Gas.QueryEnemySectOverview(CClientMainPlayer.Inst.BasicProp.SectId)
    end

    local taskId = 22111270
    if CClientMainPlayer.Inst and CommonDefs.ListContains(CClientMainPlayer.Inst.TaskProp.CurrentTaskList, typeof(UInt32), taskId) then
        CGuideMgr.Inst:StartGuide(140, 0)
    end

    self.m_TableView = self.transform:Find("Anchor/TableView"):GetComponent(typeof(QnTableView))
    self.m_VoidLabel = self.transform:Find("Anchor/VoidLabel").gameObject
    self.m_QuestBtn = self.transform:Find("Anchor/Bottom/QuestBtn").gameObject
    self.m_ConfirmBtn = self.transform:Find("Anchor/Bottom/ConfirmBtn").gameObject

    self.m_Desc1 = self.transform:Find("Anchor/Title/1").gameObject
    self.m_Desc2 = self.transform:Find("Anchor/Title/2").gameObject

    local function initItem(item,index)
        if index % 2 == 1 then
            item:SetBackgroundTexture("common_bg_mission_background_n")
        else
            item:SetBackgroundTexture("common_bg_mission_background_s")
        end
        self:InitItem(item,index)
    end

    self.m_TableViewDataSource = DefaultTableViewDataSource.CreateByCount(0,initItem)
    self.m_TableView.m_DataSource = self.m_TableViewDataSource
    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)

    UIEventListener.Get(self.m_QuestBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        g_MessageMgr:ShowMessage("Enemy_Sect_Introduction")
    end)

    UIEventListener.Get(self.m_ConfirmBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        if self.m_CurrentSect == nil then
            g_MessageMgr:ShowMessage("ENEMY_SECT_NOT_CHOOSE")
        else
            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("CONFIRM_GO_ENEMY_SECT", self.m_CurrentSectName), function()
                Gac2Gas.RequestTrackEnemySect(self.m_CurrentSect)
            end, nil, nil, nil, false)
        end
    end)

    self.m_Info = nil
    self.m_CurrentSect = nil
    self.m_CurrentSectName = ""

    --self:test()
end

function LuaEnemyZongPaiWnd:OnSelectAtRow(row)
    self.m_CurrentSect = self.m_Info[row][0]
    self.m_CurrentSectName = self.m_Info[row][1]
end

function LuaEnemyZongPaiWnd:InitItem(item,index)
    -- 节点
    local tf = item.transform
    local nameLabel = FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel))
    local content = FindChild(tf,"Content"):GetComponent(typeof(UILabel))
    local detailBtn = FindChild(tf,"DetailBtn").gameObject
    
    -- 数据
    local thisSect = self.m_Info[index]
    local sectId = thisSect[0]
    local sectName = thisSect[1]

    local nameStr = ""
    if thisSect.Count > 2 then
        nameStr = thisSect[2]
        for j = 3, thisSect.Count-1 do
            local peopleName = thisSect[j] -- 被抓的人
            nameStr = nameStr .. LocalString.GetString("、") .. peopleName
        end

        if string.len(nameStr) > 38 then
            nameStr = string.sub(nameStr, 1, 34)
            nameStr = nameStr .. SafeStringFormat3(LocalString.GetString("等%d位弟子"), thisSect.Count -2)
        end
    end

    nameLabel.text = sectName
    content.text = nameStr

    detailBtn:SetActive(thisSect.Count -2> 0)

    UIEventListener.Get(detailBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        LuaZongMenMgr:ShowArrestListWnd(sectId)
    end)
end

function LuaEnemyZongPaiWnd:InitInfo(info)
    self.m_Info = info

    if info.Count > 0 then
        self.m_VoidLabel:SetActive(false)
        self.m_TableViewDataSource.count= info.Count
        self.m_TableView:ReloadData(true,false)
    else
        self.m_VoidLabel:SetActive(true)
        self.m_TableViewDataSource.count= 0
        self.m_TableView:ReloadData(true,false)
    end
end

function LuaEnemyZongPaiWnd:OnEnable()
    self.m_Action = DelegateFactory.Action_uint(function(guideId)
        if guideId == 140 then
            -- 发个rpc通知任务完成
            Gac2Gas.FinishEventTask(22111270, "FinishEnemyZongPaiGuide")
        end
    end)
    EventManager.AddListenerInternal(EnumEventType.GuideEnd, self.m_Action)
	g_ScriptEvent:AddListener("SyncEnemySectOverview", self, "InitInfo")
end

function LuaEnemyZongPaiWnd:OnDisable()
    EventManager.RemoveListenerInternal(EnumEventType.GuideEnd, self.m_Action)
	g_ScriptEvent:RemoveListener("SyncEnemySectOverview", self, "InitInfo")
end


-- 用于处理引导
function LuaEnemyZongPaiWnd:GetGuideGo(methodName)
    if methodName == "GetDesc1" then
        return self.m_Desc1
    elseif methodName == "GetDesc2" then
        return self.m_Desc2
    elseif methodName == "GetBtn" then
        return self.m_ConfirmBtn
    end
    return nil
end
