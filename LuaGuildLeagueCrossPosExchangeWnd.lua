local DelegateFactory  = import "DelegateFactory"
local UIScrollView  = import "UIScrollView"
local UITable = import "UITable"
local CButton = import "L10.UI.CButton"
local CChatHelper = import "L10.UI.CChatHelper"
local UITabBar = import "L10.UI.UITabBar"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaGuildLeagueCrossPosExchangeWnd = class()
RegistClassMember(LuaGuildLeagueCrossPosExchangeWnd, "m_TabBar")
RegistClassMember(LuaGuildLeagueCrossPosExchangeWnd, "m_MainView")
RegistClassMember(LuaGuildLeagueCrossPosExchangeWnd, "m_RecordView")
RegistClassMember(LuaGuildLeagueCrossPosExchangeWnd, "m_GuildTemplate")
RegistClassMember(LuaGuildLeagueCrossPosExchangeWnd, "m_MainScrollview")
RegistClassMember(LuaGuildLeagueCrossPosExchangeWnd, "m_MainTable")
RegistClassMember(LuaGuildLeagueCrossPosExchangeWnd, "m_ApplyPosExchangeButton")
RegistClassMember(LuaGuildLeagueCrossPosExchangeWnd, "m_ApplyListButton")
RegistClassMember(LuaGuildLeagueCrossPosExchangeWnd, "m_InfoButton")
RegistClassMember(LuaGuildLeagueCrossPosExchangeWnd, "m_DescLabel")
RegistClassMember(LuaGuildLeagueCrossPosExchangeWnd, "m_ApplyListAlert")

RegistClassMember(LuaGuildLeagueCrossPosExchangeWnd, "m_CurrentSelectedInfo")
RegistClassMember(LuaGuildLeagueCrossPosExchangeWnd, "m_AlertAlreadyShow")

RegistClassMember(LuaGuildLeagueCrossPosExchangeWnd, "m_RecordTemplate")
RegistClassMember(LuaGuildLeagueCrossPosExchangeWnd, "m_RecordScrollview")
RegistClassMember(LuaGuildLeagueCrossPosExchangeWnd, "m_RecordTable")


function LuaGuildLeagueCrossPosExchangeWnd:Awake()
    self.m_TabBar = self.gameObject:GetComponent(typeof(UITabBar))
    self.m_MainView = self.transform:Find("Anchor/MainView")
    self.m_RecordView = self.transform:Find("Anchor/RecordView")
    self.m_GuildTemplate = self.transform:Find("Anchor/MainView/ScrollView/Item").gameObject
    self.m_MainScrollview = self.transform:Find("Anchor/MainView/ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_MainTable = self.transform:Find("Anchor/MainView/ScrollView/Table"):GetComponent(typeof(UITable))
    self.m_ApplyPosExchangeButton = self.transform:Find("Anchor/MainView/ApplyPosExchangeButton"):GetComponent(typeof(CButton))
    self.m_ApplyListButton = self.transform:Find("Anchor/MainView/ApplyListButton"):GetComponent(typeof(CButton))
    self.m_InfoButton = self.transform:Find("Anchor/MainView/InfoButton").gameObject
    self.m_DescLabel = self.transform:Find("Anchor/MainView/DescLabel"):GetComponent(typeof(UILabel))
    self.m_ApplyListAlert = self.transform:Find("Anchor/MainView/ApplyListButton/Alert").gameObject
    self.m_RecordTemplate = self.transform:Find("Anchor/RecordView/ScrollView/Item").gameObject
    self.m_RecordScrollview = self.transform:Find("Anchor/RecordView/ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_RecordTable = self.transform:Find("Anchor/RecordView/ScrollView/Table"):GetComponent(typeof(UITable))
    self.m_GuildTemplate:SetActive(false)
    self.m_RecordTemplate:SetActive(false)
    self.m_TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(
        function(go, index)
            self:OnTabChange(go, index)
        end
    )

    UIEventListener.Get(self.m_ApplyPosExchangeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnApplyPosExchangeButtonClick() end)
    UIEventListener.Get(self.m_ApplyListButton.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnApplyListButtonClick() end)
    UIEventListener.Get(self.m_InfoButton.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnInfoButtonClick() end)
end

function LuaGuildLeagueCrossPosExchangeWnd:Init()
    self.m_TabBar:ChangeTab(0, false)
end

function LuaGuildLeagueCrossPosExchangeWnd:OnTabChange(go, index)
    if index == 0 then
        self.m_MainView.gameObject:SetActive(true)
        self.m_RecordView.gameObject:SetActive(false)
        self:ShowMainView()
    elseif index == 1 then
        self.m_MainView.gameObject:SetActive(false)
        self.m_RecordView.gameObject:SetActive(true)
        self:ShowRecordView()
    end
end

function LuaGuildLeagueCrossPosExchangeWnd:ShowMainView()
    self.m_DescLabel.text = g_MessageMgr:FormatMessage("GLC_EXCHANGE_POS_DESCRIPTION_FOR_SHORT")
    self:LoadData()
    self:CheckIfShowApplyListAlert()
end

function LuaGuildLeagueCrossPosExchangeWnd:ShowRecordView()
    self:LoadRecordData({})
    LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossExchangePosRecord()
end

function LuaGuildLeagueCrossPosExchangeWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRequestGLCExchangeGuildPosSuccess", self, "OnRequestGLCExchangeGuildPosSuccess")
    g_ScriptEvent:AddListener("OnCancelGLCExchangeGuildPosSuccess", self, "OnCancelGLCExchangeGuildPosSuccess")
    g_ScriptEvent:AddListener("OnSendGuildLeagueCrossExchangePosRecord", self, "OnSendGuildLeagueCrossExchangePosRecord")
end

function LuaGuildLeagueCrossPosExchangeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRequestGLCExchangeGuildPosSuccess", self, "OnRequestGLCExchangeGuildPosSuccess")
    g_ScriptEvent:RemoveListener("OnCancelGLCExchangeGuildPosSuccess", self, "OnCancelGLCExchangeGuildPosSuccess")
    g_ScriptEvent:RemoveListener("OnSendGuildLeagueCrossExchangePosRecord", self, "OnSendGuildLeagueCrossExchangePosRecord")
end

function LuaGuildLeagueCrossPosExchangeWnd:CheckIfShowApplyListAlert()
    if self.m_AlertAlreadyShow then
        self.m_ApplyListAlert:SetActive(false)
    else
        self.m_ApplyListAlert:SetActive(#LuaGuildLeagueCrossMgr.m_GuildPosExchangeInfo.requestInfoTbl>0)
    end
end

function LuaGuildLeagueCrossPosExchangeWnd:LoadData()
    local data = LuaGuildLeagueCrossMgr.m_GuildPosExchangeInfo.guildInfoTbl
    self.m_CurrentSelectedInfo = nil
    self.m_ApplyPosExchangeButton.Enabled = true
    self.m_ApplyPosExchangeButton.Text = LocalString.GetString("申请互换出战")

    local n = self.m_MainTable.transform.childCount
    for i=0,n-1 do
        self.m_MainTable.transform:GetChild(i).gameObject:SetActive(false)
    end
    local idx = 0
    for i=1, #data do
        local child = nil
		if idx<n then
			child = self.m_MainTable.transform:GetChild(idx).gameObject
		else
			child = CUICommonDef.AddChild(self.m_MainTable.gameObject, self.m_GuildTemplate)
		end
		idx = idx+1
        child:SetActive(true)
        local info = data[i]
        self:InitOneItem(child.transform, i, info.guildId, info.guildName, info.leaderId, info.leaderName, info.rankStr, info.equipScore, info.hasApply)
        if i % 2 == 1 then
            child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.OddBgSpirite)
        else
            child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.EvenBgSprite)
        end
        local button = child:GetComponent(typeof(CButton))
        button.Selected = false
        UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnGuildItemClick(child.gameObject, info) end)
    end
    self.m_MainTable:Reposition()
    self.m_MainScrollview:ResetPosition()

end

function LuaGuildLeagueCrossPosExchangeWnd:OnRequestGLCExchangeGuildPosSuccess()
    local data = LuaGuildLeagueCrossMgr.m_GuildPosExchangeInfo.guildInfoTbl
    local n = self.m_MainTable.transform.childCount
    for i=0,n-1 do
        if i<#data then
            local child = self.m_MainTable.transform:GetChild(i)
            child:Find("ApplyLabel").gameObject:SetActive(data[i+1].hasApply)
        end
    end
    self:UpdateApplyPosExchangeButton()
end

function LuaGuildLeagueCrossPosExchangeWnd:OnCancelGLCExchangeGuildPosSuccess()
    self:OnRequestGLCExchangeGuildPosSuccess()
end

function LuaGuildLeagueCrossPosExchangeWnd:InitOneItem(transRoot, index, guildId, guildName, leaderId, leaderName, rankStr, equipScore, hasApply)
    local indexLabel = transRoot:Find("IndexLabel"):GetComponent(typeof(UILabel))
    local guildNameLabel = transRoot:Find("GuildNameLabel"):GetComponent(typeof(UILabel))
    local ownerNameLabel = transRoot:Find("OwnerNameLabel"):GetComponent(typeof(UILabel))
    local chatButton = transRoot:Find("OwnerNameLabel/ChatButton"):GetComponent(typeof(CButton))
    local rankLabel = transRoot:Find("RankLabel"):GetComponent(typeof(UILabel))
    local equipScoreLabel = transRoot:Find("EquipScoreLabel"):GetComponent(typeof(UILabel))
    local applyLabel = transRoot:Find("ApplyLabel").gameObject
    local isMyGuild = LuaGuildLeagueCrossMgr:IsMyGuild(guildId)
    local lblColor = isMyGuild and NGUIText.ParseColor24("00ff60", 0) or NGUIText.ParseColor24("ffffff", 0)
    indexLabel.text = tostring(index)
    indexLabel.color = lblColor
    guildNameLabel.text = guildName
    guildNameLabel.color = lblColor
    ownerNameLabel.text = leaderName
    ownerNameLabel.color = lblColor
    chatButton.Enabled = not isMyGuild
    rankLabel.text = rankStr
    rankLabel.color = lblColor
    equipScoreLabel.text = tostring(equipScore)
    equipScoreLabel.color = lblColor
    applyLabel:SetActive(hasApply)

    UIEventListener.Get(chatButton.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnChatButtonClick(chatButton.gameObject, leaderId, leaderName) end)
end

function LuaGuildLeagueCrossPosExchangeWnd:OnGuildItemClick(go, info)
    local n = self.m_MainTable.transform.childCount
    for i=0,n-1 do
        local child = self.m_MainTable.transform:GetChild(i)
        local button = child:GetComponent(typeof(CButton))
        if child.gameObject == go then
            button.Selected = true
            self.m_CurrentSelectedInfo = info
            self:UpdateApplyPosExchangeButton()
        else
            button.Selected = false
        end
    end
end

function LuaGuildLeagueCrossPosExchangeWnd:UpdateApplyPosExchangeButton()
    local info = self.m_CurrentSelectedInfo
    if info then
        self.m_ApplyPosExchangeButton.Enabled = not LuaGuildLeagueCrossMgr:IsMyGuild(info.guildId)
        self.m_ApplyPosExchangeButton.Text = info.hasApply and LocalString.GetString("撤回换位申请") or LocalString.GetString("申请互换出战")
    else
        self.m_ApplyPosExchangeButton.Enabled = true
        self.m_ApplyPosExchangeButton.Text = LocalString.GetString("申请互换出战")
    end
end

function LuaGuildLeagueCrossPosExchangeWnd:OnChatButtonClick(go, leaderId, leaderName)
    CChatHelper.ShowFriendWnd(leaderId, leaderName)
end

function LuaGuildLeagueCrossPosExchangeWnd:OnApplyPosExchangeButtonClick()
    if self.m_CurrentSelectedInfo then
        if not self.m_CurrentSelectedInfo.hasApply then
            local existOtherApply = false
            for __,info in pairs(LuaGuildLeagueCrossMgr.m_GuildPosExchangeInfo.guildInfoTbl) do
                if info~=self.m_CurrentSelectedInfo and info.hasApply then
                    existOtherApply = true
                    break
                end
            end
            if existOtherApply then
                g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("GLC_EXCHANGE_POS_EXIST_REQUEST_CONFRIM"), function()
                    LuaGuildLeagueCrossMgr:RequestGLCExchangeGuildPos(self.m_CurrentSelectedInfo.guildId)
                end, nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
            else
                LuaGuildLeagueCrossMgr:RequestGLCExchangeGuildPos(self.m_CurrentSelectedInfo.guildId)
            end
        else
            LuaGuildLeagueCrossMgr:CancelGLCExchangeGuildPos(self.m_CurrentSelectedInfo.guildId)
        end
    else
        g_MessageMgr:ShowMessage("GLC_EXCHANGE_POS_NEED_SELECT_GUILD")
    end
end

function LuaGuildLeagueCrossPosExchangeWnd:OnApplyListButtonClick()
    CUIManager.ShowUI("GuildLeagueCrossPosExchangeApplyWnd")
    if #LuaGuildLeagueCrossMgr.m_GuildPosExchangeInfo.requestInfoTbl>0 then
        self.m_AlertAlreadyShow = true --如果有数据，那么点击后不再显示红点
    end
    self:CheckIfShowApplyListAlert()
end

function LuaGuildLeagueCrossPosExchangeWnd:OnInfoButtonClick()
    g_MessageMgr:ShowMessage("GLC_EXCHANGE_POS_DESCRIPTION")
end

function LuaGuildLeagueCrossPosExchangeWnd:OnSendGuildLeagueCrossExchangePosRecord(recordTbl)
    if self.m_RecordView.gameObject.activeSelf then
        self:LoadRecordData(recordTbl)
    end
end

function LuaGuildLeagueCrossPosExchangeWnd:LoadRecordData(recordTbl)
    local n = self.m_RecordTable.transform.childCount
    for i=0,n-1 do
        self.m_RecordTable.transform:GetChild(i).gameObject:SetActive(false)
    end
    if recordTbl==nil then recordTbl = {} end
    local idx = 0
    for i=1, #recordTbl do
        local child = nil
		if idx<n then
			child = self.m_RecordTable.transform:GetChild(idx).gameObject
		else
			child = CUICommonDef.AddChild(self.m_RecordTable.gameObject, self.m_RecordTemplate)
		end
		idx = idx+1
        child:SetActive(true)
        local timeLabel = child.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
        local guild1Label = child.transform:Find("Guild1Label"):GetComponent(typeof(UILabel))
        local guild2Label = child.transform:Find("Guild2Label"):GetComponent(typeof(UILabel))
        local info = recordTbl[i]
        local recordTime =  CServerTimeMgr.ConvertTimeStampToZone8Time(info.time)
        timeLabel.text = ToStringWrap(recordTime, LocalString.GetString("MM月dd日 HH:mm"))
        local srcIsMyGuild = LuaGuildLeagueCrossMgr:IsMyGuild(info.srcGuildId)
        guild1Label.text = SafeStringFormat3(LocalString.GetString("（联赛排名[fffe91]第%d[-]，出战[fffe91]序号%d[-]）[%s]%s[-]"), info.srcRank, info.srcIndex, self:GetRecordGuildNameColor(info.srcGuildId), info.srcGuildName)
        guild2Label.text = SafeStringFormat3(LocalString.GetString("[%s]%s[-]（联赛排名[fffe91]第%d[-]，出战[fffe91]序号%d[-]）"),  self:GetRecordGuildNameColor(info.dstGuildId), info.dstGuildName, info.dstRank, info.dstIndex)
    end
    self.m_RecordTable:Reposition()
    self.m_RecordScrollview:ResetPosition()
end

function LuaGuildLeagueCrossPosExchangeWnd:GetRecordGuildNameColor(guildId)
    local isMyGuild = LuaGuildLeagueCrossMgr:IsMyGuild(guildId)
    return isMyGuild and "00ff60" or "aea0ff"
end

