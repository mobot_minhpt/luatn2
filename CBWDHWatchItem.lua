-- Auto Generated!!
local CBWDHWatchItem = import "L10.UI.CBWDHWatchItem"
local Constants = import "L10.Game.Constants"
local LocalString = import "LocalString"
CBWDHWatchItem.m_Init_CS2LuaHook = function (this, info, index) 

    if System.String.IsNullOrEmpty(info.team1Name) then
        this.team1Label.text = LocalString.GetString("轮空")
    else
        this.team1Label.text = this:TeamInfoLabel(info.team1Name)
    end

    if System.String.IsNullOrEmpty(info.team2Name) then
        this.team2Label.text = LocalString.GetString("轮空")
    else
        this.team2Label.text = this:TeamInfoLabel(info.team2Name)
    end

    if index % 2 == 0 then
        this:SetBackgroundTexture(Constants.NewEvenBgSprite)
    else
        this:SetBackgroundTexture(Constants.NewOddBgSprite)
    end
end
CBWDHWatchItem.m_TeamInfoLabel_CS2LuaHook = function (this, teamName) 
    --[[if (CXialvPKMgr.Inst.WatchXialvPK_Local || CXialvPKMgr.Inst.WatchXialvPK_Cross)
            {
                return CXialvPKMgr.Inst.GetXialvPKMatchName(teamName);
            }]]
    return teamName
end
