local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local Time = import "UnityEngine.Time"
local CBaseWnd = import "L10.UI.CBaseWnd"

EnumRelatePhoneCodeType = {
    CodeTypeBind = 0, --绑定
    CodeTypeChange = 1, --更换手机
    CodeTypeEnsure = 2, --验证手机
}

LuaRelatePhoneWnd = class()
RegistClassMember(LuaRelatePhoneWnd, "m_TitleLabel")
RegistClassMember(LuaRelatePhoneWnd, "m_EmptyPanel")
RegistClassMember(LuaRelatePhoneWnd, "m_DoRelatePanel")
RegistClassMember(LuaRelatePhoneWnd, "m_RelateSuccessPanel")
RegistClassMember(LuaRelatePhoneWnd, "m_RelatedPanel")
RegistClassMember(LuaRelatePhoneWnd, "m_ChangePhonePanel")
RegistClassMember(LuaRelatePhoneWnd, "m_EnsurePhonePanel")

RegistClassMember(LuaRelatePhoneWnd, "m_DoRelatePhonePanelCodeButton")
RegistClassMember(LuaRelatePhoneWnd, "m_DoRelatePhonePanelRelateButton")
RegistClassMember(LuaRelatePhoneWnd, "m_DoRelatePhonePanelCodeTipLabel")

RegistClassMember(LuaRelatePhoneWnd, "m_RelateSuccessPanelButton")

RegistClassMember(LuaRelatePhoneWnd, "m_RelatedPanelPhoneNumLabel")
RegistClassMember(LuaRelatePhoneWnd, "m_RelatedPanelRelateLabel")
RegistClassMember(LuaRelatePhoneWnd, "m_RelatedPanelEnsureRelateButton")
RegistClassMember(LuaRelatePhoneWnd, "m_RelatedPanelChangePhoneButton")

RegistClassMember(LuaRelatePhoneWnd, "m_ChangePhonePanelCodeButton")
RegistClassMember(LuaRelatePhoneWnd, "m_ChangePhonePanelCodeTipLabel")
RegistClassMember(LuaRelatePhoneWnd, "m_ChangePhonePanelUnbindButton")

RegistClassMember(LuaRelatePhoneWnd, "m_EnsurePhonePanelCodeButton")
RegistClassMember(LuaRelatePhoneWnd, "m_EnsurePhonePanelEnsureButton")
RegistClassMember(LuaRelatePhoneWnd, "m_EnsurePhonePanelCodeTipLabel")

RegistClassMember(LuaRelatePhoneWnd, "m_RecoverCodeButtonTick")

function LuaRelatePhoneWnd:Awake()
    self.m_TitleLabel = self.transform:Find("Wnd_Bg_Secondary_2/TitleLabel"):GetComponent(typeof(UILabel))

    self.m_EmptyPanel = self.transform:Find("Anchor/EmptyPanel").gameObject
    CommonDefs.AddOnClickListener(self.m_EmptyPanel.transform:Find("Button").gameObject, DelegateFactory.Action_GameObject(function(go) self:OnEmptyPanelButtonClick(go) end), false)

    self.m_DoRelatePanel = self.transform:Find("Anchor/DoRelatePanel").gameObject
    self.m_DoRelatePhonePanelCodeButton =  self.m_DoRelatePanel.transform:Find("CodeButton"):GetComponent(typeof(CButton))
    self.m_DoRelatePhonePanelRelateButton =  self.m_DoRelatePanel.transform:Find("RelateButton"):GetComponent(typeof(CButton))
    self.m_DoRelatePanel.transform:Find("Phone/PreInput"):GetComponent(typeof(UILabel)).text = "86"
    self.m_DoRelatePhonePanelCodeTipLabel = self.m_DoRelatePanel.transform:Find("Code/CodeTipLabel").gameObject
    self.m_DoRelatePhonePanelCodeTipLabel:SetActive(false)
    CommonDefs.AddOnClickListener(self.m_DoRelatePhonePanelCodeButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnDoRelatePanelCodeButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_DoRelatePhonePanelRelateButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnDoRelatePanelRelateButtonClick(go) end), false)
    
    self.m_RelateSuccessPanel = self.transform:Find("Anchor/RelateSuccessPanel").gameObject
    self.m_RelateSuccessPanelButton = self.m_RelateSuccessPanel.transform:Find("Button").gameObject
    CommonDefs.AddOnClickListener(self.m_RelateSuccessPanelButton, DelegateFactory.Action_GameObject(function(go) self:OnRelateSuccessPanelButtonClick(go) end), false)


    self.m_RelatedPanel = self.transform:Find("Anchor/RelatedPanel").gameObject
    self.m_RelatedPanelPhoneNumLabel = self.m_RelatedPanel.transform:Find("PhoneNum"):GetComponent(typeof(UILabel))
    self.m_RelatedPanelRelateLabel = self.m_RelatedPanel.transform:Find("RelateLabel"):GetComponent(typeof(UILabel))
    self.m_RelatedPanelEnsureRelateButton = self.m_RelatedPanel.transform:Find("EnsureRelateButton").gameObject
    self.m_RelatedPanelChangePhoneButton = self.m_RelatedPanel.transform:Find("ChangePhoneButton").gameObject
    CommonDefs.AddOnClickListener(self.m_RelatedPanelEnsureRelateButton, DelegateFactory.Action_GameObject(function(go) self:OnRelatedPanelEnsureRelateButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_RelatedPanelChangePhoneButton, DelegateFactory.Action_GameObject(function(go) self:OnRelatedPanelChangePhoneButtonClick(go) end), false)

    self.m_ChangePhonePanel = self.transform:Find("Anchor/ChangePhonePanel").gameObject
    self.m_ChangePhonePanelCodeButton =  self.m_ChangePhonePanel.transform:Find("CodeButton"):GetComponent(typeof(CButton))
    self.m_ChangePhonePanelUnbindButton = self.m_ChangePhonePanel.transform:Find("Button").gameObject
    self.m_ChangePhonePanelCodeTipLabel = self.m_ChangePhonePanel.transform:Find("Code/CodeTipLabel").gameObject
    self.m_ChangePhonePanelCodeTipLabel:SetActive(false)
    CommonDefs.AddOnClickListener(self.m_ChangePhonePanelCodeButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnChangePhonePanelCodeButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_ChangePhonePanelUnbindButton, DelegateFactory.Action_GameObject(function(go) self:OnChangePhonePanelUnbindButtonClick(go) end), false)

    self.m_EnsurePhonePanel = self.transform:Find("Anchor/EnsurePhonePanel").gameObject
    self.m_EnsurePhonePanelCodeButton =  self.m_EnsurePhonePanel.transform:Find("CodeButton"):GetComponent(typeof(CButton))
    self.m_EnsurePhonePanelEnsureButton =  self.m_EnsurePhonePanel.transform:Find("EnsureButton"):GetComponent(typeof(CButton))
    self.m_EnsurePhonePanelCodeTipLabel = self.m_EnsurePhonePanel.transform:Find("Code/CodeTipLabel").gameObject
    self.m_EnsurePhonePanelCodeTipLabel:SetActive(false)
    CommonDefs.AddOnClickListener(self.m_EnsurePhonePanelCodeButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnEnsurePhonePanelCodeButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_EnsurePhonePanelEnsureButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnEnsurePhonePanelEnsureButtonClick(go) end), false)

    self.m_EmptyPanel:SetActive(false)
    self.m_DoRelatePanel:SetActive(false)
    self.m_RelateSuccessPanel:SetActive(false)
    self.m_RelatedPanel:SetActive(false)
    self.m_ChangePhonePanel:SetActive(false)
    self.m_EnsurePhonePanel:SetActive(false)

    self.m_RecoverCodeButtonTick = {}
end



function LuaRelatePhoneWnd:Init()

    local relatePhoneInfo = LuaWelfareMgr.m_RelatePhoneInfo
    self.m_TitleLabel.text = LocalString.GetString("关联手机")
    if (relatePhoneInfo.m_PhoneNum == nil or relatePhoneInfo.m_PhoneNum == "") then
        self.m_EmptyPanel:SetActive(true)
    else
        self.m_RelatedPanel:SetActive(true)
        self.m_RelatedPanelPhoneNumLabel.text = self:GetPhoneNum()

        if relatePhoneInfo.m_Changyongshebei then
            self.m_RelatedPanelRelateLabel.text = ""
        else
            if relatePhoneInfo.m_MonthCheck then
                self.m_RelatedPanelRelateLabel.text = LocalString.GetString("本月已验证手机")
            else
                self.m_RelatedPanelRelateLabel.text = LocalString.GetString("本月未验证手机")
            end
        end

        if relatePhoneInfo.m_IsChangePhone then
            relatePhoneInfo.m_IsChangePhone = false
            self.m_RelatedPanel:SetActive(false)
            self.m_ChangePhonePanel:SetActive(true)
            self:InitChangePhonePanel()
        end
    end
end

function LuaRelatePhoneWnd:OnEnable()
    g_ScriptEvent:AddListener("BindMobileDisbindPhoneSuccess", self, "OnBindMobileDisbindPhoneSuccess")
    g_ScriptEvent:AddListener("BindMobileVeryfiCaptchaSuccess", self, "OnBindMobileVeryfiCaptchaSuccess")
end

function LuaRelatePhoneWnd:OnDisable()
    g_ScriptEvent:RemoveListener("BindMobileDisbindPhoneSuccess", self, "OnBindMobileDisbindPhoneSuccess")
    g_ScriptEvent:RemoveListener("BindMobileVeryfiCaptchaSuccess", self, "OnBindMobileVeryfiCaptchaSuccess")
    self:StopRecoverCodeButtonTick(EnumRelatePhoneCodeType.CodeTypeBind)
    self:StopRecoverCodeButtonTick(EnumRelatePhoneCodeType.CodeTypeChange)
    self:StopRecoverCodeButtonTick(EnumRelatePhoneCodeType.CodeTypeEnsure)
end
--copy from server side code BindMobileMgr.lua
function LuaRelatePhoneWnd:GetMaskPhoneNumber(phoneNumber)
	return string.sub(phoneNumber, 1, 3) .. "****" .. string.sub(phoneNumber, 8)
end

function LuaRelatePhoneWnd:GetPhoneNum()
    local relatePhoneInfo = LuaWelfareMgr.m_RelatePhoneInfo
    local areaCode = relatePhoneInfo.m_AreaCode
    local phoneNumber = relatePhoneInfo.m_PhoneNum
    if phoneNumber and phoneNumber~="" then
        phoneNumber = self:GetMaskPhoneNumber(phoneNumber)
    end
    if areaCode and areaCode ~= "" then
        return areaCode .. '-' .. phoneNumber
    else
        return phoneNumber
    end
end

function LuaRelatePhoneWnd:OnEmptyPanelButtonClick(go)
    self.m_EmptyPanel:SetActive(false)
    self.m_DoRelatePanel:SetActive(true)
end

function LuaRelatePhoneWnd:OnDoRelatePanelCodeButtonClick(go)
    local inputPhoneNum = self.m_DoRelatePanel.transform:Find("Phone/Input"):GetComponent(typeof(UILabel)).text
    local preInputPhoneNum = self.m_DoRelatePanel.transform:Find("Phone/PreInput"):GetComponent(typeof(UILabel)).text
    if CommonDefs.StringLength(inputPhoneNum) == 11 then
        self:StartRecoverCodeButtonTick(EnumRelatePhoneCodeType.CodeTypeBind, self.m_DoRelatePhonePanelCodeButton, self.m_DoRelatePhonePanelCodeTipLabel)
        Gac2Gas.BindMobileRequestCaptcha_Bind_Lua(preInputPhoneNum,inputPhoneNum)
    else
        Gac2Gas.BindMobileRequestCaptcha_Bind_Lua(preInputPhoneNum,inputPhoneNum)
    end
end

function LuaRelatePhoneWnd:OnDoRelatePanelRelateButtonClick(go)
    local inputPhoneNum = self.m_DoRelatePanel.transform:Find("Phone/Input"):GetComponent(typeof(UILabel)).text
    local preInputPhoneNum = self.m_DoRelatePanel.transform:Find("Phone/PreInput"):GetComponent(typeof(UILabel)).text
    if inputPhoneNum == "" or preInputPhoneNum == "" then
        g_MessageMgr:ShowMessage("BM_PHONE_NUMBER_EMPTY")
        return
    end
    local captcha = self.m_DoRelatePanel.transform:Find("Code/Input"):GetComponent(typeof(UILabel)).text
    Gac2Gas.BindMobileVerifyCaptcha_Lua(preInputPhoneNum, inputPhoneNum, captcha)
end

function LuaRelatePhoneWnd:OnRelateSuccessPanelButtonClick(go)
    self:Close()
end

function LuaRelatePhoneWnd:OnRelatedPanelEnsureRelateButtonClick(go)
    self.m_RelatedPanel:SetActive(false)
    self.m_EnsurePhonePanel:SetActive(true)
    self:InitEnsurePhonePanel()
end

function LuaRelatePhoneWnd:OnRelatedPanelChangePhoneButtonClick(go)
    self.m_RelatedPanel:SetActive(false)
    self.m_ChangePhonePanel:SetActive(true)
    self:InitChangePhonePanel()
end

function LuaRelatePhoneWnd:OnChangePhonePanelCodeButtonClick(go)
    Gac2Gas.BindMobileRequestCaptchaDisbind()
    self:StartRecoverCodeButtonTick(EnumRelatePhoneCodeType.CodeTypeChange, self.m_ChangePhonePanelCodeButton, self.m_ChangePhonePanelCodeTipLabel)
end

function LuaRelatePhoneWnd:OnChangePhonePanelUnbindButtonClick(go)
    local captcha = self.m_ChangePhonePanel.transform:Find("Code/Input"):GetComponent(typeof(UILabel)).text
    Gac2Gas.BindMobileDisbind_Lua(captcha)
end

function LuaRelatePhoneWnd:OnEnsurePhonePanelCodeButtonClick(go)
    local relatePhoneInfo = LuaWelfareMgr.m_RelatePhoneInfo
    if relatePhoneInfo.m_Changyongshebei then
        Gac2Gas.GetVerifyCommonDeviceCode_Lua()
    else
        Gac2Gas.BindMobileRequestCaptcha_Renew_Lua()
    end
    self:StartRecoverCodeButtonTick(EnumRelatePhoneCodeType.CodeTypeEnsure, self.m_EnsurePhonePanelCodeButton, self.m_EnsurePhonePanelCodeTipLabel)
end

function LuaRelatePhoneWnd:OnEnsurePhonePanelEnsureButtonClick(go)
    local captcha = self.m_EnsurePhonePanel.transform:Find("Code/Input"):GetComponent(typeof(UILabel)).text
    local relatePhoneInfo = LuaWelfareMgr.m_RelatePhoneInfo
    if relatePhoneInfo.m_Changyongshebei then
        Gac2Gas.RequestVerifyCommonDevice_Lua(captcha)
    else
        Gac2Gas.BindMobileVerifyCaptcha_Lua(relatePhoneInfo.m_AreaCode, relatePhoneInfo.m_PhoneNum, captcha)
    end
end

function LuaRelatePhoneWnd:StartRecoverCodeButtonTick(type, codebutton, codetip)
    self:StopRecoverCodeButtonTick(type)
    local endTime = Time.realtimeSinceStartup + 60
    codebutton.Enabled = false
    codetip:SetActive(true)
    self.m_RecoverCodeButtonTick[type] = RegisterTick(function()
        if Time.realtimeSinceStartup>endTime then
            self:StopRecoverCodeButtonTick()
            codebutton.Enabled = true
            codebutton.Text = LocalString.GetString("获取验证码")
        else
            codebutton.Text = SafeStringFormat3(LocalString.GetString("剩%d秒"), math.ceil(endTime-Time.realtimeSinceStartup))
        end
    end, 100)
end

function LuaRelatePhoneWnd:StopRecoverCodeButtonTick(type)
    if type and self.m_RecoverCodeButtonTick[type] then
        UnRegisterTick(self.m_RecoverCodeButtonTick[type])
        self.m_RecoverCodeButtonTick[type] = nil
    end
end

function LuaRelatePhoneWnd:InitEnsurePhonePanel()
    self:StopRecoverCodeButtonTick(EnumRelatePhoneCodeType.CodeTypeEnsure)
    self.m_EnsurePhonePanelCodeButton.Enabled = true
    self.m_EnsurePhonePanelCodeButton.Text = LocalString.GetString("获取验证码")
    self.m_EnsurePhonePanelCodeTipLabel:SetActive(false)

    self.m_TitleLabel.text = LocalString.GetString("验证手机")
end

function LuaRelatePhoneWnd:InitChangePhonePanel()
    self:StopRecoverCodeButtonTick(EnumRelatePhoneCodeType.CodeTypeChange)
    self.m_ChangePhonePanel.transform:Find("RelatedPhone/PhoneNum"):GetComponent(typeof(UILabel)).text = self:GetPhoneNum()
    self.m_ChangePhonePanelCodeButton.Enabled = true
    self.m_ChangePhonePanelCodeButton.Text = LocalString.GetString("获取验证码")
    self.m_ChangePhonePanelCodeTipLabel:SetActive(false)

    self.m_TitleLabel.text = LocalString.GetString("更换手机")
end

function LuaRelatePhoneWnd:OnBindMobileDisbindPhoneSuccess()
    self.m_ChangePhonePanel:SetActive(false)
    self.m_DoRelatePanel:SetActive(true)

    self.m_TitleLabel.text = LocalString.GetString("关联手机")
end

function LuaRelatePhoneWnd:OnBindMobileVeryfiCaptchaSuccess(phoneNumber)
    if self.m_EnsurePhonePanel.activeSelf then
        self:Close()
    elseif self.m_DoRelatePanel.activeSelf then
        self.m_DoRelatePanel:SetActive(false)
        self.m_RelateSuccessPanel:SetActive(true)
        self.m_RelateSuccessPanel.transform:Find("PhoneNum"):GetComponent(typeof(UILabel)).text = phoneNumber
        self.m_TitleLabel.text = LocalString.GetString("关联手机")
    end
end

function LuaRelatePhoneWnd:Close()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end


