-- Auto Generated!!
local CHousePopupMenu = import "L10.UI.CHousePopupMenu"
local CHousePopupMenuItem = import "L10.UI.CHousePopupMenuItem"
local CHousePopupMgr = import "L10.UI.CHousePopupMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local GameObject = import "UnityEngine.GameObject"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CHousePopupMenu.m_LoadItems_CS2LuaHook = function (this) 
    local items = CHousePopupMgr.Items
    if items == nil or items.Length == 0 then
        this:Close()
        return
    end
    if this.buttons ~= nil then
        do
            local i = 0
            while i < this.buttons.Length do
                GameObject.Destroy(this.buttons[i])
                i = i + 1
            end
        end
    end
    this.buttons = CreateFromClass(MakeArrayClass(GameObject), items.Length)
    do
        local i = 0
        while i < this.buttons.Length do
            local data = items[i]
            local button = TypeAs(CommonDefs.Object_Instantiate(this.imageButtonTemplate), typeof(GameObject))
            button.transform.parent = this.anchor.transform
            this.buttons[i] = button
            button:SetActive(true)
            local menuItem = CommonDefs.GetComponent_GameObject_Type(button, typeof(CHousePopupMenuItem))
            menuItem:Init(data.text, data.imageName, data.bCanClick, data.CDTime)

            UIEventListener.Get(button).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(button).onClick, MakeDelegateFromCSFunction(this.OnMenuitemClicked, VoidDelegate, this), true)
            i = i + 1
        end
    end

    this:RepositionButtons()

    this.cachedPos = CommonDefs.op_Multiply_Vector3_Single(Vector3.one, 1000)
    this:LayoutWnd()
    --
end
CHousePopupMenu.m_RepositionButtons_CS2LuaHook = function (this) 
    -- 比较土的办法算这个弧形的位置，根据具体的按钮数分别算各个按钮的位置
    --   |<------ ButtonBoardWidth ----->|  ____
    --                  b5                    ^
    --              b4      b6                  
    --          b3              b7       ButtonBoardHeight
    --      b2                      b8        ~
    --   b1                            b9   ____
    if this.buttons ~= nil and this.buttons.Length ~= 0 then
        local indices = CreateFromClass(MakeArrayClass(System.Int32), this.buttons.Length)
        local width = 8 local height = 4 local centerIdx = 5
        local len = this.buttons.Length
        if len == 1 then
            indices[0] = 5
        elseif len == 2 then
            indices[0] = 4
            indices[1] = 6
        elseif len == 3 then
            indices[0] = 3
            indices[1] = 5
            indices[2] = 7
        elseif len == 4 then
            indices[0] = 2
            indices[1] = 4
            indices[2] = 6
            indices[3] = 8
        elseif len == 5 then
            indices[0] = 1
            indices[1] = 3
            indices[2] = 5
            indices[3] = 7
            indices[4] = 9
        elseif len == 6 then
            indices[0] = 1
            indices[1] = 2
            indices[2] = 3
            indices[3] = 4
            indices[4] = 5
            indices[5] = 6
            width = 5
            height = 2
            centerIdx = 3.5
        elseif len == 7 then
            indices[0] = 1
            indices[1] = 2
            indices[2] = 3
            indices[3] = 4
            indices[4] = 5
            indices[5] = 6
            indices[6] = 7
            width = 6
            height = 3
            centerIdx = 4
        elseif len == 8 then
            do
                local i = 0
                while i < len do
                    indices[i] = i + 1
                    i = i + 1
                end
            end
            width = 6
            height = 2
            centerIdx = 4.5
        elseif len == 9 then
            do
                local i = 0
                while i < len do
                    indices[i] = i + 1
                    i = i + 1
                end
            end
            width = 6
            height = 3
            centerIdx = 5
        end
        if len<7 then
            for i=1,len do
                local idx = indices[i-1]
                local xoff, zoff
                xoff = CHousePopupMenu.ButtonBoardWidth/width*(idx-centerIdx)
                zoff = CHousePopupMenu.ButtonBoardHeight/height*(height - math.abs(idx-centerIdx))
                -- print(xoff,zoff,height - math.abs(idx-centerIdx))
                this.buttons[i-1].transform.localPosition = CreateFromClass(Vector3, xoff, zoff, 0)
            end
        else
            local remainder = len%2
            local middle = math.floor(len/2) + (remainder==1 and 1 or 0.5)
            local hudu = 120+30*(len-7)--弧度
            local delta = 3.14*hudu/180/(len-1) -- 
            local radius1 = 600/2+20--+(len-7)*50
            local radius2 = CHousePopupMenu.ButtonBoardHeight/2+20
            for i=1,len do
                local idx = indices[i-1]-middle-- -3 -2 -1 0 1 2 3
                local xoff = radius1*(math.sin(delta*idx))
                local zoff = radius2-radius2*4*(1-math.cos(delta*idx))
                this.buttons[i-1].transform.localPosition = CreateFromClass(Vector3, xoff, zoff, 0)
            end
        end
    end
end
CHousePopupMenu.m_OnMenuitemClicked_CS2LuaHook = function (this, go) 
    CItemInfoMgr.CloseItemInfoWnd()
    do
        local i = 0
        while i < this.buttons.Length do
            if this.buttons[i]:Equals(go) then
                if CHousePopupMgr.Items[i].action ~= nil then
                    GenericDelegateInvoke(CHousePopupMgr.Items[i].action, Table2ArrayWithCount({i}, 1, MakeArrayClass(Object)))
                end
                if CHousePopupMgr.Items[i].bCloseWnd and CHousePopupMgr.Items[i].bCanClick then
                    CUIManager.CloseUI(CUIResources.HousePopupMenu)
                    CUIManager.CloseUI(CUIResources.FurnitureSlider)
                end
                break
            end
            i = i + 1
        end
    end
end

