local CLoginMgr=import "L10.Game.CLoginMgr"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"

local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"

local DelegateFactory  = import "DelegateFactory"

LuaShengXiaoCardResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShengXiaoCardResultWnd, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaShengXiaoCardResultWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaShengXiaoCardResultWnd, "ServerLabel", "ServerLabel", UILabel)
RegistChildComponent(LuaShengXiaoCardResultWnd, "ID", "ID", UILabel)
RegistChildComponent(LuaShengXiaoCardResultWnd, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaShengXiaoCardResultWnd, "RankButton", "RankButton", GameObject)
RegistChildComponent(LuaShengXiaoCardResultWnd, "PlayerItem", "PlayerItem", GameObject)
RegistChildComponent(LuaShengXiaoCardResultWnd, "Lose", "Lose", GameObject)
RegistChildComponent(LuaShengXiaoCardResultWnd, "Win", "Win", GameObject)
RegistChildComponent(LuaShengXiaoCardResultWnd, "Grid", "Grid", GameObject)

--@endregion RegistChildComponent end

LuaShengXiaoCardResultWnd.s_PlayerInfos = nil

function LuaShengXiaoCardResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	if CommonDefs.IS_VN_CLIENT then
		self.ShareButton:SetActive(false)
	end
	UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)


	
	UIEventListener.Get(self.RankButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankButtonClick()
	end)

    --@endregion EventBind end
	self.PlayerItem:SetActive(false)
end

function LuaShengXiaoCardResultWnd:Init()
	self.NameLabel.text = CClientMainPlayer.Inst.Name
	local myserver = CLoginMgr.Inst:GetSelectedGameServer()
	self.ServerLabel.text = myserver.name
	self.ID.text = tostring(CClientMainPlayer.Inst.Id)

	self.Icon:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName,false)

	if CClientMainPlayer.Inst.Id==LuaShengXiaoCardMgr.m_LoserId then
		self.Win:SetActive(false)
		self.Lose:SetActive(true)
	else
		self.Win:SetActive(true)
		self.Lose:SetActive(false)
	end

	Extensions.RemoveAllChildren(self.Grid.transform)
    for k,v in pairs(LuaShengXiaoCardResultWnd.s_PlayerInfos) do
		local go  = NGUITools.AddChild(self.Grid,self.PlayerItem)
		go:SetActive(true)

		local portrait = go.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
		local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
		local scoreLabel = go.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
		nameLabel.text = v.Name
		local portraitName = CUICommonDef.GetPortraitName(v.Class, v.Gender, -1)
		portrait:LoadNPCPortrait(portraitName,false)

		UIEventListener.Get(portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
			CPlayerInfoMgr.ShowPlayerPopupMenu(k, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, "", nil, Vector3.zero, AlignType.Default)
		end)
		scoreLabel.text = nil

		--积分赛
		if LuaShengXiaoCardMgr.m_IsReportRank then
			if v.GameInfo and v.GameInfo.Report then
				if v.GameInfo.Escape then
					scoreLabel.text = self:FormatScore(ShengXiaoCard_Setting.GetData().EscapeScore)
				elseif k==LuaShengXiaoCardMgr.m_LoserId then 
					scoreLabel.text = self:FormatScore(ShengXiaoCard_Setting.GetData().LoseScore)
				else
					scoreLabel.text = self:FormatScore(ShengXiaoCard_Setting.GetData().WinScore)
				end
			end
		end

		local win = go.transform:Find("WinIcon").gameObject
		local lose = go.transform:Find("LoseIcon").gameObject

		if k==LuaShengXiaoCardMgr.m_LoserId then 
			win:SetActive(false)
			lose:SetActive(true)
		else
			win:SetActive(true)
			lose:SetActive(false)
		end
	end

	self.Grid:GetComponent(typeof(UIGrid)):Reposition()
end
function LuaShengXiaoCardResultWnd:FormatScore(v)
	if v>0 then
		return "+"..tostring(v)
	else
		return tostring(v)
	end
end
--@region UIEvent
local tick = nil
function LuaShengXiaoCardResultWnd:OnShareButtonClick()
	local closeButton = self.transform:Find("CloseButton").gameObject
	local bottomRight = self.transform:Find("BottomRight").gameObject
	closeButton:SetActive(false)
	bottomRight:SetActive(false)
	CUICommonDef.CaptureScreenAndShare()

	tick = RegisterTickOnce(function()
		tick = nil
		closeButton:SetActive(true)
		bottomRight:SetActive(true)
	end,500)
end

function LuaShengXiaoCardResultWnd:OnRankButtonClick()
    CUIManager.ShowUI(CLuaUIResources.ShengXiaoCardRankWnd)
end

--@endregion UIEvent

function LuaShengXiaoCardResultWnd:OnDestroy()
    if tick then UnRegisterTick(tick) tick=nil end
end
