local Profession = import "L10.Game.Profession"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local EnumClass = import "L10.Game.EnumClass"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CQMPKTitleTemplate=import "L10.UI.CQMPKTitleTemplate"


CLuaQMPKSearchRecruitWnd = class()
RegistClassMember(CLuaQMPKSearchRecruitWnd,"m_ConfirmBtn")
RegistClassMember(CLuaQMPKSearchRecruitWnd,"m_CancelBtn")
-- RegistClassMember(CLuaQMPKSearchRecruitWnd,"m_CloseBtn")
RegistClassMember(CLuaQMPKSearchRecruitWnd,"m_ClassTitle")
RegistClassMember(CLuaQMPKSearchRecruitWnd,"m_CanCommandTitle")
RegistClassMember(CLuaQMPKSearchRecruitWnd,"m_CanCommandStrList")
RegistClassMember(CLuaQMPKSearchRecruitWnd,"m_CanCommandPopupList")
RegistClassMember(CLuaQMPKSearchRecruitWnd,"m_ClassPopupList")
RegistClassMember(CLuaQMPKSearchRecruitWnd,"m_ClassStrList")
RegistClassMember(CLuaQMPKSearchRecruitWnd,"m_MaxClass")
RegistClassMember(CLuaQMPKSearchRecruitWnd,"m_SelectedClassIndex")
RegistClassMember(CLuaQMPKSearchRecruitWnd,"m_CommandIndex")

function CLuaQMPKSearchRecruitWnd:Awake()
    self.m_ConfirmBtn = self.transform:Find("Anchor/Bottom/SaveBtn").gameObject
    self.m_CancelBtn = self.transform:Find("Anchor/Bottom/SendBtn").gameObject
    self.m_ClassTitle = self.transform:Find("Anchor/Class/TitleTemplate"):GetComponent(typeof(CQMPKTitleTemplate))
    self.m_CanCommandTitle = self.transform:Find("Anchor/Director/TitleTemplate"):GetComponent(typeof(CQMPKTitleTemplate))
self.m_CanCommandStrList = {LocalString.GetString("不限"), LocalString.GetString("是")}
self.m_CanCommandPopupList = {}
self.m_ClassPopupList = {}
self.m_ClassStrList = {}
--self.m_MaxClass = ?
self.m_SelectedClassIndex = 0
self.m_CommandIndex = 0

end

-- Auto Generated!!
function CLuaQMPKSearchRecruitWnd:OnEnable( )
    --UIEventListener.Get(m_CloseBtn).onClick = OnClickCloseButton;
    UIEventListener.Get(self.m_ConfirmBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnConfirmBtnClicked(go) end)
    UIEventListener.Get(self.m_CancelBtn).onClick =  DelegateFactory.VoidDelegate(function(go) CUIManager.CloseUI(CLuaUIResources.QMPKSearchRecruitWnd) end)
    UIEventListener.Get(self.m_ClassTitle.gameObject).onClick =  DelegateFactory.VoidDelegate(function(go) self:OnClassClicked(go) end)
    UIEventListener.Get(self.m_CanCommandTitle.gameObject).onClick =DelegateFactory.VoidDelegate(function(go) self:OnCommandClicked(go) end)
end
function CLuaQMPKSearchRecruitWnd:Init( )
    -- self.m_CanCommandPopupList = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    -- self.m_ClassPopupList = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    -- self.m_ClassStrList = CreateFromClass(MakeGenericClass(List, String))

    -- CommonDefs.ListClear(self.m_ClassPopupList)
    -- CommonDefs.ListClear(self.m_ClassStrList)
    -- CommonDefs.EnumerableIterate(Enum.GetValues(typeof(EnumClass)), DelegateFactory.Action_object(function (___value) 
    --     clazz = ___value
    --     local continue
    --     repeat
    --         if EnumToInt(clazz) >= EnumToInt(self.m_MaxClass) then
    --             continue = true
    --             break
    --         end
    --         local name = Profession.GetFullName(clazz)
    --         local data = PopupMenuItemData(name, DelegateFactory.Action_int(function(index) self:OnClassSelected(index) end), false, nil, EnumPopupMenuItemStyle.Light)
    --         -- CommonDefs.ListAdd(self.m_ClassStrList, typeof(String), name)
    --         table.insert( self.m_ClassStrList,name )
    --         -- CommonDefs.ListAdd(self.m_ClassPopupList, typeof(PopupMenuItemData), data)
    --         table.insert( self.m_ClassPopupList,data )
    --         continue = true
    --     until 1
    --     if not continue then
    --         return
    --     end
    -- end))
    local classes={
        EnumClass.SheShou,
        EnumClass.JiaShi,
        EnumClass.DaoKe,
        EnumClass.XiaKe,
        EnumClass.FangShi,
        EnumClass.YiShi,
        EnumClass.MeiZhe,
        EnumClass.YiRen,
        EnumClass.YanShi,
        EnumClass.HuaHun,
        EnumClass.YingLing,
        EnumClass.DieKe,
    }
    for i,v in ipairs(classes) do
        local name = Profession.GetFullName(v)
        local data = PopupMenuItemData(name, DelegateFactory.Action_int(function(index) self:OnClassSelected(index) end), false, nil, EnumPopupMenuItemStyle.Light)
        table.insert( self.m_ClassStrList,name )
        table.insert( self.m_ClassPopupList,data )
    end


    if nil == CClientMainPlayer.Inst then
        self.m_SelectedClassIndex = 0
    else
        self.m_SelectedClassIndex = EnumToInt(CClientMainPlayer.Inst.Class) - 1
    end
    self.m_ClassTitle:Init(self.m_ClassStrList[self.m_SelectedClassIndex+1])


    -- CommonDefs.ListClear(self.m_CanCommandPopupList)
    -- do
    --     local i = 0 local cnt = self.m_CanCommandStrList.Count
    --     while i < cnt do
    --         local data = PopupMenuItemData(self.m_CanCommandStrList[i], MakeDelegateFromCSFunction(self.OnCommandSelected, MakeGenericClass(Action1, Int32), self), false, nil, EnumPopupMenuItemStyle.Light)
    --         CommonDefs.ListAdd(self.m_CanCommandPopupList, typeof(PopupMenuItemData), data)
    --         i = i + 1
    --     end
    -- end
    for i,v in ipairs(self.m_CanCommandStrList) do
        local data = PopupMenuItemData(v, DelegateFactory.Action_int(function(index) self:OnCommandSelected(index) end), false, nil, EnumPopupMenuItemStyle.Light)
        table.insert( self.m_CanCommandPopupList,data )
    end

    self.m_CanCommandTitle:Init(self.m_CanCommandStrList[self.m_CommandIndex])
end
function CLuaQMPKSearchRecruitWnd:OnClassSelected( index) 
    self.m_ClassTitle:Init(self.m_ClassStrList[index+1])
    self.m_SelectedClassIndex = index
end
function CLuaQMPKSearchRecruitWnd:OnCommandSelected( index) 
    self.m_CanCommandTitle:Init(self.m_CanCommandStrList[index+1])
    self.m_CommandIndex = index
end
function CLuaQMPKSearchRecruitWnd:OnConfirmBtnClicked( go) 
    CQuanMinPKMgr.Inst.m_SearchZhanduiClass = self.m_SelectedClassIndex + 1
    CQuanMinPKMgr.Inst.m_SearchZhanduiCommandIndex = self.m_CommandIndex
    Gac2Gas.SearchQmpkZhanDuiByClassAndZhiHui(self.m_SelectedClassIndex + 1, self.m_CommandIndex, 1)
    -- self:Close()
    CUIManager.CloseUI(CLuaUIResources.QMPKSearchRecruitWnd)

end
function CLuaQMPKSearchRecruitWnd:OnClassClicked( go) 
    self.m_ClassTitle:Expand(true)
    local array=Table2Array(self.m_ClassPopupList,MakeArrayClass(PopupMenuItemData))
    
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, CPopupMenuInfoMgr.AlignType.Bottom, #self.m_ClassPopupList >= 8 and 2 or 1, DelegateFactory.Action(function () 
        self.m_ClassTitle:Expand(false)
    end), 600, true, 296)
end
function CLuaQMPKSearchRecruitWnd:OnCommandClicked( go) 
    self.m_CanCommandTitle:Expand(true)
    local array=Table2Array(self.m_CanCommandPopupList,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, DelegateFactory.Action(function () 
        self.m_CanCommandTitle:Expand(false)
    end), 600, true, 296)
end

return CLuaQMPKSearchRecruitWnd
