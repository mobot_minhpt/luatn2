local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CWelfareBonusAwardTemplate = import "L10.UI.CWelfareBonusAwardTemplate"
local CItemMgr = import "L10.Game.CItemMgr"

LuaSpringFestivalBonusTemplate = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaSpringFestivalBonusTemplate, "BianKuang", GameObject)
RegistChildComponent(LuaSpringFestivalBonusTemplate, "BuQianSprite", GameObject)
RegistChildComponent(LuaSpringFestivalBonusTemplate, "ItemCell1", GameObject)
RegistChildComponent(LuaSpringFestivalBonusTemplate, "ItemCell2", GameObject)

--@endregion RegistChildComponent end

function LuaSpringFestivalBonusTemplate:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaSpringFestivalBonusTemplate:InitBonus(key)
    local holiday = QianDao_Holiday.GetData(key)
    self:InitAward(holiday.Item, holiday.Display, nil)
    
    if holiday.NeedTimes == CSigninMgr.Inst.holidayInfo.totalTimes + 1 and not CSigninMgr.Inst.holidayInfo.hasSignGift and CSigninMgr.Inst.holidayInfo.todayInfo.Open == 1 then
        -- 领取
        self.BuQianSprite:SetActive(false)
        self.BianKuang:SetActive(true)
        UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            Gac2Gas.RequestHolidayQianDao(CSigninMgr.Inst.holidayInfo.batchId);
        end)
    elseif holiday.NeedTimes == CSigninMgr.Inst.holidayInfo.totalTimes + 1 and CSigninMgr.Inst.holidayInfo.hasSignGift and CSigninMgr.Inst.holidayInfo.todayInfo.Open == 1 and CSigninMgr.Inst.holidayInfo.validTimes >= holiday.NeedTimes then
        -- 补签
        self.BuQianSprite:SetActive(true)
        self.BianKuang:SetActive(true)
        UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            Gac2Gas.RequestHolidayQianDao(CSigninMgr.Inst.holidayInfo.batchId);
        end)
    elseif holiday.NeedTimes > CSigninMgr.Inst.holidayInfo.totalTimes or CSigninMgr.Inst.holidayInfo.todayInfo.Open == 0 then
        -- 未到领取时间
        self.BuQianSprite:SetActive(false)
        self.BianKuang:SetActive(false)
        UIEventListener.Get(self.gameObject).onClick = nil
    else
        -- 已签
        self.BuQianSprite:SetActive(false)
        self.BianKuang:SetActive(false)
        self.ItemCell1.transform:Find("Sprite").gameObject:SetActive(true)
        self.ItemCell2.transform:Find("Sprite").gameObject:SetActive(true)
        UIEventListener.Get(self.gameObject).onClick = nil
    end
end

function LuaSpringFestivalBonusTemplate:InitAward(award, display, scrollView)
    local awardString = CommonDefs.StringSplit_ArrayChar(award, ";")
    local fxAward = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, String)), String, CommonDefs.StringSplit_ArrayChar(display, ";"))
    self:InitOneAward(self.ItemCell1, CommonDefs.StringSplit_ArrayChar(awardString[0], ","), fxAward, scrollView)
    self:InitOneAward(self.ItemCell2, CommonDefs.StringSplit_ArrayChar(awardString[1], ","), fxAward, scrollView)
end

function LuaSpringFestivalBonusTemplate:InitOneAward(itemCell, itemInfo, fxAward, scrollView)
    if itemInfo ~= nil and itemInfo.Length >= 2 then
        local default, templateId = System.UInt32.TryParse(itemInfo[0])
        if default then
            local item = CItemMgr.Inst:GetItemTemplate(templateId)
            local template = CommonDefs.GetComponent_GameObject_Type(itemCell, typeof(CWelfareBonusAwardTemplate))
            if item ~= nil and template ~= nil then
                template:Init(item, itemInfo[1], CommonDefs.ListContains(fxAward, typeof(String), tostring(templateId)), scrollView)
                itemCell.transform:Find("Label").gameObject:SetActive(itemInfo[1] ~= "1")
            end
        end
        itemCell.transform:Find("Sprite").gameObject:SetActive(false)
    end
end

--@region UIEvent

--@endregion UIEvent

