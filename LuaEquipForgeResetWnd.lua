local CPackageItemCell = import "L10.UI.CPackageItemCell"
local CItemMgr=import "L10.Game.CItemMgr"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local UITable = import "UITable"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CBaseEquipmentItem = import "L10.UI.CBaseEquipmentItem"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local Extensions = import "Extensions"
local CItemMgr = import "L10.Game.CItemMgr"

LuaEquipForgeResetWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaEquipForgeResetWnd, "CommitBtn", "CommitBtn", GameObject)
RegistChildComponent(LuaEquipForgeResetWnd, "Cost", "Cost", CCurentMoneyCtrl)
RegistChildComponent(LuaEquipForgeResetWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaEquipForgeResetWnd, "ForgeLevelLabel", "ForgeLevelLabel", UILabel)
RegistChildComponent(LuaEquipForgeResetWnd, "Table", "Table", UITable)
RegistChildComponent(LuaEquipForgeResetWnd, "MainEquip", "MainEquip", CBaseEquipmentItem)

--@endregion RegistChildComponent end

function LuaEquipForgeResetWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.CommitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCommitBtnClick()
	end)


    --@endregion EventBind end
end

function LuaEquipForgeResetWnd:Init()
    local data = CLuaEquipMgr.m_ForgeResetCostData

    -- 文字
    local label = self.transform:Find("Anchor/Label"):GetComponent(typeof(UILabel))
    label.text = g_MessageMgr:FormatMessage("Equip_Forge_Reset_Wnd_Desc")

    -- 设置得到物品
    local itemList = data.itemList
    Extensions.RemoveAllChildren(self.Table.transform)
    self.ItemTemplate:SetActive(false)

    -- 物品全是绑定的
    for i=1, #itemList, 3 do
        local g = NGUITools.AddChild(self.Table.gameObject, self.ItemTemplate)
        g:SetActive(true)
        
        local icon = g.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
        local numLabel = g.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
        local bindSprite = g.transform:Find("BindSprite").gameObject

        local itemId = itemList[i]
        local count = itemList[i+1]
        local isbind = itemList[i+2]

        local itemData = Item_Item.GetData(itemId)
        icon:LoadMaterial(itemData.Icon)

        UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function (go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType2.Default, 0, 0, 0, 0)
        end)
        numLabel.text = tostring(count)
    end
    self.Table:Reposition()

    -- 设置消耗
    local moneyCost = data.costYuanBao
    self.Cost:SetCost(moneyCost)

    -- 分解的装备
    local equipId = CLuaEquipMgr.m_ForgeResetItemId
    self.MainEquip:UpdateData(equipId)
    local item = CItemMgr.Inst:GetById(equipId)

    if item.IsEquip then
        self.ForgeLevelLabel.text = "lv." .. tostring(item.Equip.ForgeLevel)
    end
end

function LuaEquipForgeResetWnd:OnDisable()
	CLuaEquipMgr.m_ForgeResetItemId = ""
end

--@region UIEvent

function LuaEquipForgeResetWnd:OnCommitBtnClick()
    local itemId = CLuaEquipMgr.m_ForgeResetItemId
    local equipItem = CItemMgr.Inst:GetById(itemId)

    local msg = g_MessageMgr:FormatMessage("Extra_Equip_Reset_Confirm", equipItem.Name)

    g_MessageMgr:ShowOkCancelMessage(msg, function()
        local itemId = CLuaEquipMgr.m_ForgeResetItemId
        local place, pos = CLuaEquipMgr:GetItemPlaceAndPos(itemId)
        Gac2Gas.RequestForgeResetExtraEquip(place, pos, itemId)
        CUIManager.CloseUI(CLuaUIResources.EquipForgeResetWnd)
    end, nil, nil, nil, false)
end


--@endregion UIEvent

