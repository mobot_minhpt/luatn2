local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local UISprite = import "UISprite"
local QnButtonState = import "L10.UI.QnButton+QnButtonState"

LuaHuLuWaLianDanShouCeWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHuLuWaLianDanShouCeWnd, "DayTableView", "DayTableView", QnTableView)
RegistChildComponent(LuaHuLuWaLianDanShouCeWnd, "WeekTableView", "WeekTableView", QnTableView)
RegistChildComponent(LuaHuLuWaLianDanShouCeWnd, "TargetLevelLabel", "TargetLevelLabel", UILabel)
RegistChildComponent(LuaHuLuWaLianDanShouCeWnd, "NeedFireCountLabel", "NeedFireCountLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaHuLuWaLianDanShouCeWnd,"m_CurLevel")
RegistClassMember(LuaHuLuWaLianDanShouCeWnd,"m_TotalProgress")

function LuaHuLuWaLianDanShouCeWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.DayTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #LuaHuLuWa2022Mgr.m_DailyTask
        end,
        function(item,row)
            self:InitItem(item,row,true)
        end)
	
	self.WeekTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #LuaHuLuWa2022Mgr.m_WeekTask
        end,
        function(item,row)
            self:InitItem(item,row,false)
        end)
    self.DayTableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnSelectAtRow(row,true)
    end)
    self.WeekTableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnSelectAtRow(row,false)
    end)

    self.m_CurLevel,self.m_TotalProgress = LuaHuLuWa2022Mgr.GetCurZhanLingLevel()

    local nextLevel = self.m_CurLevel+1
    self.TargetLevelLabel.text = nextLevel
    local targetProgress = LuaHuLuWa2022Mgr.m_Level2NeedProgress[nextLevel]

    if targetProgress then
        self.NeedFireCountLabel.text = targetProgress - self.m_TotalProgress
    else
        self.TargetLevelLabel.gameObject:SetActive(false)
        self.NeedFireCountLabel.gameObject:SetActive(false)
    end
end

function LuaHuLuWaLianDanShouCeWnd:Init()
    --已完成的任务放在最后
    local sortfuc = function(a,b)
        if a.progress == 1 and b.progress ~= 1 then
            return false
        elseif a.progress ~= 1 and b.progress == 1 then 
            return true
        else
            return a.taskId < b.taskId
        end  
    end
    
    table.sort(LuaHuLuWa2022Mgr.m_DailyTask,sortfuc)
    table.sort(LuaHuLuWa2022Mgr.m_WeekTask,sortfuc)
    self.DayTableView:ReloadData(false,false)
    self.WeekTableView:ReloadData(false,false)
    self:InitFinishTask()
end

function LuaHuLuWaLianDanShouCeWnd:InitItem(item,row,isDaily)
    local goBtn = item.transform:Find("GoBtn")
    local taskProgress = item.transform:Find("Progress/TaskProgress"):GetComponent(typeof(UISprite))
    local progressLabel = item.transform:Find("Progress/ProgressLabel"):GetComponent(typeof(UILabel))
    local descLabel = item.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
    local rewardLabel = item.transform:Find("RewardLabel"):GetComponent(typeof(UILabel))
    local speedLabel = item.transform:Find("SpeedLabel"):GetComponent(typeof(UILabel))
    local finishTag = item.transform:Find("FinishedTag")

    local data
    if isDaily then
        data = LuaHuLuWa2022Mgr.m_DailyTask[row+1]
    else
        data = LuaHuLuWa2022Mgr.m_WeekTask[row+1]
    end
    if data then
        local id = data.taskId
        local finishCount = data.finishCount
        local taskData = ZhanLing_Task.GetData(id)
        local progress = data.progress
        local target = taskData.Target

        taskProgress.fillAmount = progress
        progressLabel.text = SafeStringFormat3("%d/%d",finishCount,target)
        descLabel.text = taskData.Desc
        finishTag.gameObject:SetActive(progress >= 1)
        goBtn.gameObject:SetActive(progress < 1)
        if progress < 1 or not progress then
            UIEventListener.Get(goBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                LuaHuLuWa2022Mgr.OpenWnd(id)
            end)
        end
        rewardLabel.text = taskData.ProgressReward
        local rate = ZhanLing_Setting.GetData().Vip2AddRate
        if LuaHuLuWa2022Mgr.m_IsVip2 then
            speedLabel.text = SafeStringFormat3("x%.1f",rate)
        else
            speedLabel.text = nil
        end
    end
end
function LuaHuLuWaLianDanShouCeWnd:OnSelectAtRow(row,isDaily)
    self:InitFinishTask()
end

function LuaHuLuWaLianDanShouCeWnd:InitFinishTask()
    for i=0,self.DayTableView.m_ItemList.Count-1,1 do
        local p = LuaHuLuWa2022Mgr.m_DailyTask[i+1].progress
        local item = self.DayTableView.m_ItemList[i]
        local pos = item.transform.localPosition
        if p >= 1 then
            pos.z = -1
        else
            pos.z = 0
        end
        item.transform.localPosition = pos
    end
    for ii=0,self.WeekTableView.m_ItemList.Count-1,1 do
        local p = LuaHuLuWa2022Mgr.m_WeekTask[ii+1].progress
        local item = self.WeekTableView.m_ItemList[ii]
        local pos = item.transform.localPosition
        if p >= 1 then
            pos.z = -1
        else
            pos.z = 0
        end
        item.transform.localPosition = pos
    end

end

function LuaHuLuWaLianDanShouCeWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncLianDanLuPlayData", self, "OnSyncLianDanLuPlayDate")
    Gac2Gas.QueryLianDanLuPlayData()
end
function LuaHuLuWaLianDanShouCeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncLianDanLuPlayData", self, "OnSyncLianDanLuPlayDate")
    Gac2Gas.QueryLianDanLuPlayData()
end

function LuaHuLuWaLianDanShouCeWnd:OnSyncLianDanLuPlayDate()
    self.m_CurLevel,self.m_TotalProgress = LuaHuLuWa2022Mgr.GetCurZhanLingLevel()

    local nextLevel = self.m_CurLevel+1
    self.TargetLevelLabel.text = nextLevel
    local targetProgress = LuaHuLuWa2022Mgr.m_Level2NeedProgress[nextLevel]

    if targetProgress then
        self.NeedFireCountLabel.text = targetProgress - self.m_TotalProgress
    else
        self.TargetLevelLabel.gameObject:SetActive(false)
        self.NeedFireCountLabel.gameObject:SetActive(false)
    end
end
--@region UIEvent

--@endregion UIEvent

