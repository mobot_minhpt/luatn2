local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CScene=import "L10.Game.CScene"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local Menpai_SkyBoxKind = import "L10.Game.Menpai_SkyBoxKind"
local Constants = import "L10.Game.Constants"

LuaZongMenCreationWnd = class()

RegistChildComponent(LuaZongMenCreationWnd,"m_Input","Input", UIInput)
RegistChildComponent(LuaZongMenCreationWnd,"m_Blank","Blank", GameObject)
RegistChildComponent(LuaZongMenCreationWnd,"m_SelectNameSuffixButton","SelectNameSuffixButton", QnSelectableButton)
RegistChildComponent(LuaZongMenCreationWnd,"m_NameSuffixLabel","NameSuffixLabel", UILabel)
RegistChildComponent(LuaZongMenCreationWnd,"m_NameSuffixList","NameSuffixList", GameObject)
RegistChildComponent(LuaZongMenCreationWnd,"m_SceneNameLabel","SceneNameLabel", UILabel)
RegistChildComponent(LuaZongMenCreationWnd,"m_ShowScenesListViewButton","ShowScenesListViewButton", GameObject)
RegistChildComponent(LuaZongMenCreationWnd,"m_ScenesListView","ScenesListView", GameObject)
RegistChildComponent(LuaZongMenCreationWnd,"m_ScrollView","ScrollView", UIScrollView)
RegistChildComponent(LuaZongMenCreationWnd,"m_ScenesListViewGrid","ScenesListViewGrid", UIGrid)
RegistChildComponent(LuaZongMenCreationWnd,"m_SceneTexture","SceneTexture", CUITexture)
RegistChildComponent(LuaZongMenCreationWnd,"m_CreateButton","CreateButton", GameObject)
RegistChildComponent(LuaZongMenCreationWnd,"m_SelectNameSuffixButtonTemplate","SelectNameSuffixButtonTemplate", GameObject)
RegistChildComponent(LuaZongMenCreationWnd,"m_NameSuffixGrid","NameSuffixGrid", UIGrid)
RegistChildComponent(LuaZongMenCreationWnd,"m_TextureTemplate","TextureTemplate", GameObject)

RegistClassMember(LuaZongMenCreationWnd, "m_ScenesListViewQnRadioBox")
RegistClassMember(LuaZongMenCreationWnd, "m_PosX")
RegistClassMember(LuaZongMenCreationWnd, "m_PosY")
RegistClassMember(LuaZongMenCreationWnd, "m_EntranceMapId")
RegistClassMember(LuaZongMenCreationWnd, "m_SelectSceneId")
RegistClassMember(LuaZongMenCreationWnd, "m_NameSuffixQnRadioBox")
RegistClassMember(LuaZongMenCreationWnd, "m_SkyBoxKinds")

function LuaZongMenCreationWnd:Init()
    if CommonDefs.IS_VN_CLIENT then
        self.m_Input.characterLimit = 30
    end

    self.m_SceneTexture.material = nil
    self:InitNameSuffixQnRadioBox()
    UIEventListener.Get(self.m_ShowScenesListViewButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnShowScenesListViewButtonClicked()
    end)
    UIEventListener.Get(self.m_CreateButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnCreateButtonClicked()
    end)
    if not CClientMainPlayer.Inst then 
        CUIManager.CloseUI(CLuaUIResources.ZongMenCreationWnd)
        return
    end
    if not CScene.MainScene or not CClientMainPlayer.Inst then
        CUIManager.CloseUI(CLuaUIResources.ZongMenCreationWnd)
    end
    local pos = Utility.PixelPos2GridPos(CClientMainPlayer.Inst.Pos)
    self.m_PosX, self.m_PosY, self.m_EntranceMapId = pos.x,pos.y,CScene.MainScene.SceneTemplateId
    self.m_SceneNameLabel.text =  SafeStringFormat3("%s(%d,%d)",CScene.MainScene.SceneName, pos.x, pos.y)
    self:InitScenesListView()
end

function LuaZongMenCreationWnd:OnInputChange()
end

function LuaZongMenCreationWnd:InitNameSuffixQnRadioBox()
    self.m_SelectNameSuffixButtonTemplate:SetActive(false)
    local list = g_LuaUtil:StrSplit(Menpai_Setting.GetData("NameSuffixList").Value, ",")
    Extensions.RemoveAllChildren(self.m_NameSuffixGrid.transform)
    self.m_NameSuffixQnRadioBox = self.m_NameSuffixList:AddComponent(typeof(QnRadioBox))
    self.m_NameSuffixQnRadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), #list)
    for i,text in pairs(list) do
        local go = NGUITools.AddChild(self.m_NameSuffixGrid.gameObject, self.m_SelectNameSuffixButtonTemplate)
        go:SetActive(true)
        go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = text
        self.m_NameSuffixQnRadioBox.m_RadioButtons[i - 1] = go:GetComponent(typeof(QnSelectableButton))
        self.m_NameSuffixQnRadioBox.m_RadioButtons[i - 1].OnClick = MakeDelegateFromCSFunction(self.m_NameSuffixQnRadioBox.On_Click, MakeGenericClass(Action1, QnButton), self.m_NameSuffixQnRadioBox)
    end
    self.m_NameSuffixGrid:Reposition()
    UIEventListener.Get(self.m_Blank).onClick = DelegateFactory.VoidDelegate(function (p)
        self.m_SelectNameSuffixButton:SetSelected(false)
    end)
    self.m_SelectNameSuffixButton.OnButtonSelected = DelegateFactory.Action_bool(function (selected )
        self.m_NameSuffixQnRadioBox.gameObject:SetActive(selected)
    end)
    self.m_SelectNameSuffixButton:SetSelected(false)

    self.m_NameSuffixQnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSelectNameSuffixRadioBox(index)
    end)
    self.m_NameSuffixQnRadioBox:ChangeTo(0, true)
end

function LuaZongMenCreationWnd:OnSelectNameSuffixRadioBox(index)
    self.m_NameSuffixLabel.text = self.m_NameSuffixQnRadioBox.m_RadioButtons[index].Text
end

function LuaZongMenCreationWnd:OnShowScenesListViewButtonClicked()
    self.m_ScenesListView:SetActive(true)
    self.m_ScenesListViewGrid:Reposition()
    self.m_ScrollView:ResetPosition()
end

function LuaZongMenCreationWnd:InitScenesListView()
    self.m_ScenesListView:SetActive(false)
    Extensions.RemoveAllChildren(self.m_ScenesListViewGrid.transform)
    self.m_TextureTemplate:SetActive(false)
    local obj = NGUITools.AddChild(self.m_ScenesListViewGrid.gameObject, self.m_TextureTemplate)
    obj:GetComponent(typeof(CUITexture)):LoadMaterial("")
    obj:SetActive(false)
    self.m_SkyBoxKinds = {}
    Menpai_SkyBoxKind.Foreach(DelegateFactory.Action_object_object(function(key,data) 
        if data.MenPaiKind == 1 then
            local obj = NGUITools.AddChild(self.m_ScenesListViewGrid.gameObject, self.m_TextureTemplate)
            obj:GetComponent(typeof(CUITexture)):LoadMaterial(data.Preview)
            obj:SetActive(true)
            local k = key
            table.insert(self.m_SkyBoxKinds,k)
        end
    end))
    self.m_ScenesListViewGrid:Reposition()
    self.m_ScrollView:ResetPosition()
    self.m_ScenesListViewQnRadioBox = self.m_ScrollView.gameObject:AddComponent(typeof(QnRadioBox))
    self.m_ScenesListViewQnRadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), self.m_ScenesListViewGrid.transform.childCount)
    for i = 0, self.m_ScenesListViewGrid.transform.childCount - 1 do
        local go = self.m_ScenesListViewGrid.transform:GetChild(i).gameObject
        self.m_ScenesListViewQnRadioBox.m_RadioButtons[i] = go:GetComponent(typeof(QnSelectableButton))
        self.m_ScenesListViewQnRadioBox.m_RadioButtons[i].OnClick = MakeDelegateFromCSFunction(self.m_ScenesListViewQnRadioBox.On_Click, MakeGenericClass(Action1, QnButton), self.m_ScenesListViewQnRadioBox)
    end
    self.m_ScenesListViewQnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSelectSceneTexture(index)
    end)
end

function LuaZongMenCreationWnd:OnSelectSceneTexture(index)
    self.m_SelectSceneId = self.m_SkyBoxKinds[index]
    local mat = self.m_ScenesListViewQnRadioBox.m_RadioButtons[index].gameObject:GetComponent(typeof(CUITexture)).material
    self.m_SceneTexture.material = mat
    self.m_ScenesListView:SetActive(false)
end

function LuaZongMenCreationWnd:OnCreateButtonClicked()
    local str = tostring(self.m_Input.value)
    str = string.gsub(str, "%s+", "")
    local error = false
    if cs_string.IsNullOrEmpty(str) then
        error = true
    end
    if CommonDefs.IsSeaMultiLang() then
        local len = CommonDefs.StringLength(str)
        -- 宗派名VARBINARY(40)
        if not error and (len < Constants.SeaMinCharLimitForSectName or len > Constants.SeaMaxCharLimitForSectName) then
            error = true
        end
    else
        local len = CUICommonDef.GetStrByteLength(str)
        if not error and len< 4 then
            error = true
        end
    end
    if not CommonDefs.IS_VN_CLIENT then
        if not error then
            if not System.Text.RegularExpressions.Regex.IsMatch(str, "^[\\u4e00-\\u9fbb]+$") then
                error = true
            end
        end
    end
    if error then
        if CommonDefs.IsSeaMultiLang() then
            g_MessageMgr:ShowMessage("Sea_MenPaiCreate_NameConfirm", Constants.SeaMinCharLimitForSectName, Constants.SeaMaxCharLimitForSectName)
        else
            g_MessageMgr:ShowMessage("MenPaiCreate_NameConfirm")
        end
        return
    end
    if not self.m_SelectSceneId then 
        g_MessageMgr:ShowMessage("MenPaiCreate_NoneSelectSceneId")
        return 
    end

    local text = tostring(self.m_Input.value) .. self.m_NameSuffixLabel.text
    local ret = CWordFilterMgr.Inst:CheckName(text, LocalString.GetString("宗派名称"))
    if not ret then
        g_MessageMgr:ShowMessage("FENZU_VIOLATION")
        return
    end
    local default, itemPos, itemId,itemPlace = LuaZongMenMgr:GetXingWuItemInfo()
    if not itemPos then
        g_MessageMgr:ShowMessage("XinWu_Item_Not_Found")
        return
    end
    -- todo
    -- 伏羲动作迁移,加两个参数 expressionId 和 fuXiDanceMotionId
    local expressionId = LuaZongMenMgr.m_CreateSectExpressionId
    local fuXiDanceMotionId = LuaZongMenMgr.m_CreateSectFuXiDanceMotionId
    Gac2Gas.RequestCreateSect(self.m_EntranceMapId, self.m_PosX, self.m_PosY, self.m_SelectSceneId, text, itemPlace, itemPos, itemId, expressionId, fuXiDanceMotionId)
end