-- Auto Generated!!
local AlignType = import "CPlayerInfoMgr+AlignType"
local CBWZQAttendChooseWnd = import "L10.UI.CBWZQAttendChooseWnd"
local CBWZQMgr = import "L10.Game.CBWZQMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumClass = import "L10.Game.EnumClass"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Profession = import "L10.Game.Profession"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local Vector3 = import "UnityEngine.Vector3"
CBWZQAttendChooseWnd.m_GenerateNode_CS2LuaHook = function (this, data) 
    local node = NGUITools.AddChild(this.table.gameObject, this.template)
    node:SetActive(true)

    CommonDefs.GetComponent_Component_Type(node.transform:Find("Name/text"), typeof(UILabel)).text = data.name
    if System.String.IsNullOrEmpty(data.guildName) or System.String.IsNullOrEmpty(data.guildTitleText) then
        CommonDefs.GetComponent_Component_Type(node.transform:Find("Guild/text"), typeof(UILabel)).text = ""
    else
        CommonDefs.GetComponent_Component_Type(node.transform:Find("Guild/text"), typeof(UILabel)).text = (data.guildName .. LocalString.GetString("的")) .. data.guildTitleText
    end
    CommonDefs.GetComponent_Component_Type(node.transform:Find("Name/JobIcon"), typeof(UISprite)).spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), (data.clazz)))
    CommonDefs.GetComponent_Component_Type(node.transform:Find("lv"), typeof(UILabel)).text = "lv." .. data.level
    local iconbutton = node.transform:Find("icon/button").gameObject
    local button = node.transform:Find("button").gameObject

    UIEventListener.Get(iconbutton).onClick = DelegateFactory.VoidDelegate(function (p) 
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.ChatLink, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
    end)

    UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function (p) 
        Gac2Gas.GetOneZhaoQinCallerInfo(data.playerId)
        this:Close()
    end)

    CommonDefs.GetComponent_Component_Type(node.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(data.clazz, data.gender, -1), false)
end
CBWZQAttendChooseWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)

    this.template:SetActive(false)

    local playerInfoList = CBWZQMgr.Inst.risePlayerInfoList
    Extensions.RemoveAllChildren(this.table.transform)

    if playerInfoList.Count > 0 then
        do
            local i = 0
            while i < playerInfoList.Count do
                local info = playerInfoList[i]
                this:GenerateNode(info)
                i = i + 1
            end
        end
        this.table:Reposition()
        this.scrollView:ResetPosition()
    else
        this:Close()
    end
end
