local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local File = import "System.IO.File"
local Path = import "System.IO.Path"
local Utility = import "L10.Engine.Utility"
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
--
-- Device_开头的表内容是根据一个Device.xlsx生成的，利用了策划数据转表工具
-- 这个表的维护不依赖于策划，所以不准备放到design目录下面，可以方便qa和程序更新
-- 对应的Device.xlsx
--

if not rawget(_G, "Device_DeviceModel") then Device_DeviceModel = {} end
Device_DeviceModel["iPhone1,1"] = {DeviceModel=[=[iPhone1,1]=],Level=1,}
Device_DeviceModel["iPhone1,2"] = {DeviceModel=[=[iPhone1,2]=],Level=1,}
Device_DeviceModel["iPhone2,1"] = {DeviceModel=[=[iPhone2,1]=],Level=1,}
Device_DeviceModel["iPhone3,1"] = {DeviceModel=[=[iPhone3,1]=],Level=1,}
Device_DeviceModel["iPhone3,2"] = {DeviceModel=[=[iPhone3,2]=],Level=1,}
Device_DeviceModel["iPhone3,3"] = {DeviceModel=[=[iPhone3,3]=],Level=1,}
Device_DeviceModel["iPhone4,1"] = {DeviceModel=[=[iPhone4,1]=],Level=1,}
Device_DeviceModel["iPhone5,1"] = {DeviceModel=[=[iPhone5,1]=],Level=1,}
Device_DeviceModel["iPhone5,2"] = {DeviceModel=[=[iPhone5,2]=],Level=1,}
Device_DeviceModel["iPhone5,3"] = {DeviceModel=[=[iPhone5,3]=],Level=1,}
Device_DeviceModel["iPhone5,4"] = {DeviceModel=[=[iPhone5,4]=],Level=1,}
Device_DeviceModel["iPhone6,1"] = {DeviceModel=[=[iPhone6,1]=],Level=1,}
Device_DeviceModel["iPhone6,2"] = {DeviceModel=[=[iPhone6,2]=],Level=1,}
Device_DeviceModel["iPhone7,2"] = {DeviceModel=[=[iPhone7,2]=],Level=1,}
Device_DeviceModel["iPhone7,1"] = {DeviceModel=[=[iPhone7,1]=],Level=1,}
Device_DeviceModel["iPhone8,1"] = {DeviceModel=[=[iPhone8,1]=],Level=1,}
Device_DeviceModel["iPhone8,2"] = {DeviceModel=[=[iPhone8,2]=],Level=1,}
Device_DeviceModel["iPhone8,4"] = {DeviceModel=[=[iPhone8,4]=],Level=1,}
Device_DeviceModel["iPhone9,1"] = {DeviceModel=[=[iPhone9,1]=],Level=2,}
Device_DeviceModel["iPhone9,3"] = {DeviceModel=[=[iPhone9,3]=],Level=2,}
Device_DeviceModel["iPhone9,2"] = {DeviceModel=[=[iPhone9,2]=],Level=2,}
Device_DeviceModel["iPhone9,4"] = {DeviceModel=[=[iPhone9,4]=],Level=2,}
Device_DeviceModel["iPhone10,1"] = {DeviceModel=[=[iPhone10,1]=],Level=2,}
Device_DeviceModel["iPhone10,4"] = {DeviceModel=[=[iPhone10,4]=],Level=2,}
Device_DeviceModel["iPhone10,2"] = {DeviceModel=[=[iPhone10,2]=],Level=2,}
Device_DeviceModel["iPhone10,5"] = {DeviceModel=[=[iPhone10,5]=],Level=2,}
Device_DeviceModel["iPhone10,3"] = {DeviceModel=[=[iPhone10,3]=],Level=3,}
Device_DeviceModel["iPhone10,6"] = {DeviceModel=[=[iPhone10,6]=],Level=3,}
Device_DeviceModel["iPhone11,2"] = {DeviceModel=[=[iPhone11,2]=],Level=3,}
Device_DeviceModel["iPhone11,6"] = {DeviceModel=[=[iPhone11,6]=],Level=3,}
Device_DeviceModel["iPhone11,8"] = {DeviceModel=[=[iPhone11,8]=],Level=3,}
Device_DeviceModel["iPhone12,1"] = {DeviceModel=[=[iPhone12,1]=],Level=4,}
Device_DeviceModel["iPhone12,3"] = {DeviceModel=[=[iPhone12,3]=],Level=4,}
Device_DeviceModel["iPhone12,5"] = {DeviceModel=[=[iPhone12,5]=],Level=4,}
Device_DeviceModel["iPhone12,8"] = {DeviceModel=[=[iPhone12,8]=],Level=4,}
Device_DeviceModel["iPhone13,1"] = {DeviceModel=[=[iPhone13,1]=],Level=4,}
Device_DeviceModel["iPhone13,2"] = {DeviceModel=[=[iPhone13,2]=],Level=4,}
Device_DeviceModel["iPhone13,3"] = {DeviceModel=[=[iPhone13,3]=],Level=4,}
Device_DeviceModel["iPhone13,4"] = {DeviceModel=[=[iPhone13,4]=],Level=4,}
Device_DeviceModel["iPhone14,4"] = {DeviceModel=[=[iPhone14,4]=],Level=5,}
Device_DeviceModel["iPhone14,5"] = {DeviceModel=[=[iPhone14,5]=],Level=5,}
Device_DeviceModel["iPhone14,2"] = {DeviceModel=[=[iPhone14,2]=],Level=5,}
Device_DeviceModel["iPhone14,3"] = {DeviceModel=[=[iPhone14,3]=],Level=5,}
Device_DeviceModel["iPhone14,6"] = {DeviceModel=[=[iPhone14,6]=],Level=5,}
Device_DeviceModel["iPhone14,7"] = {DeviceModel=[=[iPhone14,7]=],Level=5,}
Device_DeviceModel["iPhone14,8"] = {DeviceModel=[=[iPhone14,8]=],Level=5,}
Device_DeviceModel["iPhone15,2"] = {DeviceModel=[=[iPhone15,2]=],Level=5,}
Device_DeviceModel["iPhone15,3"] = {DeviceModel=[=[iPhone15,3]=],Level=5,}
Device_DeviceModel["iPhone16,1"] = {DeviceModel=[=[iPhone16,1]=],Level=5,}
Device_DeviceModel["iPhone16,2"] = {DeviceModel=[=[iPhone16,2]=],Level=5,}

Device_DeviceModel["iPad1,1"] = {DeviceModel=[=[iPad1,1]=],Level=1,}
Device_DeviceModel["iPad2,1"] = {DeviceModel=[=[iPad2,1]=],Level=1,}
Device_DeviceModel["iPad2,2"] = {DeviceModel=[=[iPad2,2]=],Level=1,}
Device_DeviceModel["iPad2,3"] = {DeviceModel=[=[iPad2,3]=],Level=1,}
Device_DeviceModel["iPad2,4"] = {DeviceModel=[=[iPad2,4]=],Level=1,}
Device_DeviceModel["iPad3,1"] = {DeviceModel=[=[iPad3,1]=],Level=1,}
Device_DeviceModel["iPad3,2"] = {DeviceModel=[=[iPad3,2]=],Level=1,}
Device_DeviceModel["iPad3,3"] = {DeviceModel=[=[iPad3,3]=],Level=1,}
Device_DeviceModel["iPad3,4"] = {DeviceModel=[=[iPad3,4]=],Level=1,}
Device_DeviceModel["iPad3,6"] = {DeviceModel=[=[iPad3,6]=],Level=1,}
Device_DeviceModel["iPad3,5"] = {DeviceModel=[=[iPad3,5]=],Level=1,}
Device_DeviceModel["iPad6,11"] = {DeviceModel=[=[iPad6,11]=],Level=1,}
Device_DeviceModel["iPad6,12"] = {DeviceModel=[=[iPad6,12]=],Level=1,}
Device_DeviceModel["iPad7,5"] = {DeviceModel=[=[iPad7,5]=],Level=1,}
Device_DeviceModel["iPad7,6"] = {DeviceModel=[=[iPad7,6]=],Level=1,}
Device_DeviceModel["iPad7,11"] = {DeviceModel=[=[iPad7,11]=],Level=2,}
Device_DeviceModel["iPad7,12"] = {DeviceModel=[=[iPad7,12]=],Level=2,}
Device_DeviceModel["iPad11,6"] = {DeviceModel=[=[iPad11,6]=],Level=3,}
Device_DeviceModel["iPad11,7"] = {DeviceModel=[=[iPad11,7]=],Level=3,}
Device_DeviceModel["iPad12,1"] = {DeviceModel=[=[iPad12,1]=],Level=4,}
Device_DeviceModel["iPad12,2"] = {DeviceModel=[=[iPad12,2]=],Level=4,}
Device_DeviceModel["iPad4,1"] = {DeviceModel=[=[iPad4,1]=],Level=1,}
Device_DeviceModel["iPad4,2"] = {DeviceModel=[=[iPad4,2]=],Level=1,}
Device_DeviceModel["iPad4,3"] = {DeviceModel=[=[iPad4,3]=],Level=1,}
Device_DeviceModel["iPad5,3"] = {DeviceModel=[=[iPad5,3]=],Level=1,}
Device_DeviceModel["iPad5,4"] = {DeviceModel=[=[iPad5,4]=],Level=1,}
Device_DeviceModel["iPad11,3"] = {DeviceModel=[=[iPad11,3]=],Level=3,}
Device_DeviceModel["iPad11,4"] = {DeviceModel=[=[iPad11,4]=],Level=3,}
Device_DeviceModel["iPad13,1"] = {DeviceModel=[=[iPad13,1]=],Level=4,}
Device_DeviceModel["iPad13,2"] = {DeviceModel=[=[iPad13,2]=],Level=4,}
Device_DeviceModel["iPad13,16"] = {DeviceModel=[=[iPad13,16]=],Level=5,}
Device_DeviceModel["iPad13,17"] = {DeviceModel=[=[iPad13,17]=],Level=5,}
Device_DeviceModel["iPad6,7"] = {DeviceModel=[=[iPad6,7]=],Level=1,}
Device_DeviceModel["iPad6,8"] = {DeviceModel=[=[iPad6,8]=],Level=1,}
Device_DeviceModel["iPad6,3"] = {DeviceModel=[=[iPad6,3]=],Level=1,}
Device_DeviceModel["iPad6,4"] = {DeviceModel=[=[iPad6,4]=],Level=1,}
Device_DeviceModel["iPad7,1"] = {DeviceModel=[=[iPad7,1]=],Level=2,}
Device_DeviceModel["iPad7,2"] = {DeviceModel=[=[iPad7,2]=],Level=2,}
Device_DeviceModel["iPad7,3"] = {DeviceModel=[=[iPad7,3]=],Level=2,}
Device_DeviceModel["iPad7,4"] = {DeviceModel=[=[iPad7,4]=],Level=2,}
Device_DeviceModel["iPad8,1"] = {DeviceModel=[=[iPad8,1]=],Level=3,}
Device_DeviceModel["iPad8,2"] = {DeviceModel=[=[iPad8,2]=],Level=3,}
Device_DeviceModel["iPad8,3"] = {DeviceModel=[=[iPad8,3]=],Level=3,}
Device_DeviceModel["iPad8,4"] = {DeviceModel=[=[iPad8,4]=],Level=3,}
Device_DeviceModel["iPad8,5"] = {DeviceModel=[=[iPad8,5]=],Level=3,}
Device_DeviceModel["iPad8,6"] = {DeviceModel=[=[iPad8,6]=],Level=3,}
Device_DeviceModel["iPad8,7"] = {DeviceModel=[=[iPad8,7]=],Level=3,}
Device_DeviceModel["iPad8,8"] = {DeviceModel=[=[iPad8,8]=],Level=3,}
Device_DeviceModel["iPad8,9"] = {DeviceModel=[=[iPad8,9]=],Level=4,}
Device_DeviceModel["iPad8,10"] = {DeviceModel=[=[iPad8,10]=],Level=4,}
Device_DeviceModel["iPad8,11"] = {DeviceModel=[=[iPad8,11]=],Level=4,}
Device_DeviceModel["iPad8,12"] = {DeviceModel=[=[iPad8,12]=],Level=4,}
Device_DeviceModel["iPad13,4"] = {DeviceModel=[=[iPad13,4]=],Level=5,}
Device_DeviceModel["iPad13,5"] = {DeviceModel=[=[iPad13,5]=],Level=5,}
Device_DeviceModel["iPad13,6"] = {DeviceModel=[=[iPad13,6]=],Level=5,}
Device_DeviceModel["iPad13,7"] = {DeviceModel=[=[iPad13,7]=],Level=5,}
Device_DeviceModel["iPad13,8"] = {DeviceModel=[=[iPad13,8]=],Level=5,}
Device_DeviceModel["iPad13,9"] = {DeviceModel=[=[iPad13,9]=],Level=5,}
Device_DeviceModel["iPad13,10"] = {DeviceModel=[=[iPad13,10]=],Level=5,}
Device_DeviceModel["iPad13,11"] = {DeviceModel=[=[iPad13,11]=],Level=5,}
Device_DeviceModel["iPad2,5"] = {DeviceModel=[=[iPad2,5]=],Level=1,}
Device_DeviceModel["iPad2,6"] = {DeviceModel=[=[iPad2,6]=],Level=1,}
Device_DeviceModel["iPad2,7"] = {DeviceModel=[=[iPad2,7]=],Level=1,}
Device_DeviceModel["iPad4,4"] = {DeviceModel=[=[iPad4,4]=],Level=1,}
Device_DeviceModel["iPad4,5"] = {DeviceModel=[=[iPad4,5]=],Level=1,}
Device_DeviceModel["iPad4,6"] = {DeviceModel=[=[iPad4,6]=],Level=1,}
Device_DeviceModel["iPad4,7"] = {DeviceModel=[=[iPad4,7]=],Level=1,}
Device_DeviceModel["iPad4,8"] = {DeviceModel=[=[iPad4,8]=],Level=1,}
Device_DeviceModel["iPad4,9"] = {DeviceModel=[=[iPad4,9]=],Level=1,}
Device_DeviceModel["iPad5,1"] = {DeviceModel=[=[iPad5,1]=],Level=1,}
Device_DeviceModel["iPad5,2"] = {DeviceModel=[=[iPad5,2]=],Level=1,}
Device_DeviceModel["iPad11,1"] = {DeviceModel=[=[iPad11,1]=],Level=3,}
Device_DeviceModel["iPad11,2"] = {DeviceModel=[=[iPad11,2]=],Level=3,}
Device_DeviceModel["iPad14,1"] = {DeviceModel=[=[iPad14,1]=],Level=5,}
Device_DeviceModel["iPad14,2"] = {DeviceModel=[=[iPad14,2]=],Level=5,}
Device_DeviceModel["iPod1,1"] = {DeviceModel=[=[iPod1,1]=],Level=1,}
Device_DeviceModel["iPod2,1"] = {DeviceModel=[=[iPod2,1]=],Level=1,}
Device_DeviceModel["iPod3,1"] = {DeviceModel=[=[iPod3,1]=],Level=1,}
Device_DeviceModel["iPod4,1"] = {DeviceModel=[=[iPod4,1]=],Level=1,}
Device_DeviceModel["iPod5,1"] = {DeviceModel=[=[iPod5,1]=],Level=1,}
Device_DeviceModel["iPod7,1"] = {DeviceModel=[=[iPod7,1]=],Level=1,}
Device_DeviceModel["iPod9,1"] = {DeviceModel=[=[iPod9,1]=],Level=2,}

if not rawget(_G, "Device_AndroidCPU") then Device_AndroidCPU = {} end
Device_AndroidCPU["kirin955"] = {CPUName=[=[kirin955]=],Level=1,}
Device_AndroidCPU["kirin960"] = {CPUName=[=[kirin960]=],Level=1,}
Device_AndroidCPU["hi3660"] = {CPUName=[=[hi3660]=],Level=1,}
Device_AndroidCPU["kirin970"] = {CPUName=[=[kirin970]=],Level=2,}
Device_AndroidCPU["hi3670"] = {CPUName=[=[hi3670]=],Level=2,}
Device_AndroidCPU["hisilicon kirin970"] = {CPUName=[=[hisilicon kirin970]=],Level=2,}
Device_AndroidCPU["vendor kirin810"] = {CPUName=[=[vendor kirin810]=],Level=2,}
Device_AndroidCPU["kirin980"] = {CPUName=[=[kirin980]=],Level=3,}
Device_AndroidCPU["hisilicon kirin980"] = {CPUName=[=[hisilicon kirin980]=],Level=3,}
Device_AndroidCPU["vendor kirin820"] = {CPUName=[=[vendor kirin820]=],Level=3,}
Device_AndroidCPU["kirin985"] = {CPUName=[=[kirin985]=],Level=3,}
Device_AndroidCPU["hisilicon kirin990"] = {CPUName=[=[hisilicon kirin990]=],Level=5,}
Device_AndroidCPU["kirin990"] = {CPUName=[=[kirin990]=],Level=5,}
Device_AndroidCPU["vendor kirin990"] = {CPUName=[=[vendor kirin990]=],Level=5,}
Device_AndroidCPU["kirin9000"] = {CPUName=[=[kirin9000]=],Level=5,}
Device_AndroidCPU["kirin9000s"] = {CPUName=[=[kirin9000s]=],Level=5,}
Device_AndroidCPU["kirin710"] = {CPUName=[=[kirin710]=],Level=1,}
Device_AndroidCPU["MT6769V/CZ"] = {CPUName=[=[MT6769V/CZ]=],Level=2,}
Device_AndroidCPU["MT6785V/CC"] = {CPUName=[=[MT6785V/CC]=],Level=2,}
Device_AndroidCPU["MT6853V/ZA"] = {CPUName=[=[MT6853V/ZA]=],Level=3,}
Device_AndroidCPU["MT6853V/NZA"] = {CPUName=[=[MT6853V/NZA]=],Level=2,}
Device_AndroidCPU["MT6833V/ZA "] = {CPUName=[=[MT6833V/ZA ]=],Level=3,}
Device_AndroidCPU["MT6873"] = {CPUName=[=[MT6873]=],Level=3,}
Device_AndroidCPU["MT6873V "] = {CPUName=[=[MT6873V ]=],Level=3,}
Device_AndroidCPU["MT6853V"] = {CPUName=[=[MT6853V]=],Level=3,}
Device_AndroidCPU["MT6833V"] = {CPUName=[=[MT6833V]=],Level=3,}
Device_AndroidCPU["PNZA MT6833P"] = {CPUName=[=[PNZA MT6833P]=],Level=3,}
Device_AndroidCPU["MT6853T"] = {CPUName=[=[MT6853T]=],Level=3,}
Device_AndroidCPU["MT6853V/TNZA"] = {CPUName=[=[MT6853V/TNZA]=],Level=3,}
Device_AndroidCPU["MT6875"] = {CPUName=[=[MT6875]=],Level=3,}
Device_AndroidCPU["MT6877V/ZA"] = {CPUName=[=[MT6877V/ZA]=],Level=3,}
Device_AndroidCPU["MT6877"] = {CPUName=[=[MT6877]=],Level=3,}
Device_AndroidCPU["MT6877T"] = {CPUName=[=[MT6877T]=],Level=3,}
Device_AndroidCPU["MT6855"] = {CPUName=[=[MT6855]=],Level=3,}
Device_AndroidCPU["MT6889"] = {CPUName=[=[MT6889]=],Level=3,}
Device_AndroidCPU["MT6889V"] = {CPUName=[=[MT6889V]=],Level=3,}
Device_AndroidCPU["MT6889Z/CZA"] = {CPUName=[=[MT6889Z/CZA]=],Level=5,}
Device_AndroidCPU["MT6889Z"] = {CPUName=[=[MT6889Z]=],Level=5,}
Device_AndroidCPU["MT6883Z/CZA"] = {CPUName=[=[MT6883Z/CZA]=],Level=3,}
Device_AndroidCPU["MT6883"] = {CPUName=[=[MT6883]=],Level=3,}
Device_AndroidCPU["MT6885Z"] = {CPUName=[=[MT6885Z]=],Level=5,}
Device_AndroidCPU["MT6885Z/CZA"] = {CPUName=[=[MT6885Z/CZA]=],Level=5,}
Device_AndroidCPU["MT6885"] = {CPUName=[=[MT6885]=],Level=5,}
Device_AndroidCPU["MT6891Z"] = {CPUName=[=[MT6891Z]=],Level=5,}
Device_AndroidCPU["MT6891Z/CZA"] = {CPUName=[=[MT6891Z/CZA]=],Level=5,}
Device_AndroidCPU["MT6891"] = {CPUName=[=[MT6891]=],Level=5,}
Device_AndroidCPU["MT6893"] = {CPUName=[=[MT6893]=],Level=5,}
Device_AndroidCPU["MT6893Z_A/CZA"] = {CPUName=[=[MT6893Z_A/CZA]=],Level=5,}
Device_AndroidCPU["MT6893Z"] = {CPUName=[=[MT6893Z]=],Level=5,}
Device_AndroidCPU["MT6833"] = {CPUName=[=[MT6833]=],Level=2,}
Device_AndroidCPU["MT6833V/ZA"] = {CPUName=[=[MT6833V/ZA]=],Level=2,}
Device_AndroidCPU["MT6833GP"] = {CPUName=[=[MT6833GP]=],Level=3,}
Device_AndroidCPU["MT6895Z/CZA"] = {CPUName=[=[MT6895Z/CZA]=],Level=5,}
Device_AndroidCPU["MT6895"] = {CPUName=[=[MT6895]=],Level=5,}
Device_AndroidCPU["MT6895Z"] = {CPUName=[=[MT6895Z]=],Level=5,}
Device_AndroidCPU["MT6896Z"] = {CPUName=[=[MT6896Z]=],Level=5,}
Device_AndroidCPU["MT6983"] = {CPUName=[=[MT6983]=],Level=5,}
Device_AndroidCPU["DX-1"] = {CPUName=[=[DX-1]=],Level=5,}
Device_AndroidCPU["MT6985"] = {CPUName=[=[MT6985]=],Level=5,}
Device_AndroidCPU["DX-2"] = {CPUName=[=[DX-2]=],Level=5,}
Device_AndroidCPU["Exynos 2200"] = {CPUName=[=[Exynos 2200]=],Level=5,}
Device_AndroidCPU["Exynos 2100"] = {CPUName=[=[Exynos 2100]=],Level=5,}
Device_AndroidCPU["Exynos 1380"] = {CPUName=[=[Exynos 1380]=],Level=3,}
Device_AndroidCPU["Exynos 1080"] = {CPUName=[=[Exynos 1080]=],Level=5,}
Device_AndroidCPU["Exynos 990"] = {CPUName=[=[Exynos 990]=],Level=4,}
Device_AndroidCPU["Exynos 9825"] = {CPUName=[=[Exynos 9825]=],Level=3,}
Device_AndroidCPU["Exynos 9820"] = {CPUName=[=[Exynos 9820]=],Level=2,}
Device_AndroidCPU["Exynos 9810"] = {CPUName=[=[Exynos 9810]=],Level=2,}
Device_AndroidCPU["Exynos 8895"] = {CPUName=[=[Exynos 8895]=],Level=2,}
Device_AndroidCPU["Exynos 8890"] = {CPUName=[=[Exynos 8890]=],Level=1,}
Device_AndroidCPU["Exynos 1280"] = {CPUName=[=[Exynos 1280]=],Level=1,}
Device_AndroidCPU["Exynos 1330"] = {CPUName=[=[Exynos 1330]=],Level=1,}
Device_AndroidCPU["Exynos 980"] = {CPUName=[=[Exynos 980]=],Level=1,}
Device_AndroidCPU["Exynos 880"] = {CPUName=[=[Exynos 880]=],Level=1,}
Device_AndroidCPU["Exynos 9611"] = {CPUName=[=[Exynos 9611]=],Level=1,}
Device_AndroidCPU["Exynos 9610"] = {CPUName=[=[Exynos 9610]=],Level=1,}
Device_AndroidCPU["Exynos 909"] = {CPUName=[=[Exynos 909]=],Level=1,}
Device_AndroidCPU["Exynos 850"] = {CPUName=[=[Exynos 850]=],Level=1,}
Device_AndroidCPU["Exynos 7885"] = {CPUName=[=[Exynos 7885]=],Level=1,}
Device_AndroidCPU["Exynos 7904"] = {CPUName=[=[Exynos 7904]=],Level=1,}
Device_AndroidCPU["Exynos 7884"] = {CPUName=[=[Exynos 7884]=],Level=1,}
Device_AndroidCPU["MSM8917"] = {CPUName=[=[MSM8917]=],Level=1,}
Device_AndroidCPU["SDM429"] = {CPUName=[=[SDM429]=],Level=1,}
Device_AndroidCPU["MSM8937"] = {CPUName=[=[MSM8937]=],Level=1,}
Device_AndroidCPU["MSM8940"] = {CPUName=[=[MSM8940]=],Level=1,}
Device_AndroidCPU["SDM439"] = {CPUName=[=[SDM439]=],Level=1,}
Device_AndroidCPU["SDM450"] = {CPUName=[=[SDM450]=],Level=1,}
Device_AndroidCPU["SM4250-AA"] = {CPUName=[=[SM4250-AA]=],Level=1,}
Device_AndroidCPU["SM4350"] = {CPUName=[=[SM4350]=],Level=1,}
Device_AndroidCPU["MSM8939"] = {CPUName=[=[MSM8939]=],Level=1,}
Device_AndroidCPU["MSM8952"] = {CPUName=[=[MSM8952]=],Level=1,}
Device_AndroidCPU["MSM8953"] = {CPUName=[=[MSM8953]=],Level=1,}
Device_AndroidCPU["SDM630"] = {CPUName=[=[SDM630]=],Level=1,}
Device_AndroidCPU["SDM632"] = {CPUName=[=[SDM632]=],Level=1,}
Device_AndroidCPU["SDM636"] = {CPUName=[=[SDM636]=],Level=1,}
Device_AndroidCPU["MSM8956"] = {CPUName=[=[MSM8956]=],Level=1,}
Device_AndroidCPU["MSM8976"] = {CPUName=[=[MSM8976]=],Level=1,}
Device_AndroidCPU["SDM660"] = {CPUName=[=[SDM660]=],Level=1,}
Device_AndroidCPU["SDM660 AIE"] = {CPUName=[=[SDM660]=],Level=1,}
Device_AndroidCPU["SM6115"] = {CPUName=[=[SM6115]=],Level=1,}
Device_AndroidCPU["SM6125"] = {CPUName=[=[SM6125]=],Level=1,}
Device_AndroidCPU["SM6150"] = {CPUName=[=[SM6150]=],Level=1,}
Device_AndroidCPU["SM6150-AC"] = {CPUName=[=[SM6150-AC]=],Level=1,}
Device_AndroidCPU["SDM670"] = {CPUName=[=[SDM670]=],Level=1,}
Device_AndroidCPU["SM6225"] = {CPUName=[=[SM6225]=],Level=2,}
Device_AndroidCPU["SM6350"] = {CPUName=[=[SM6350]=],Level=3,}
Device_AndroidCPU["SM6375"] = {CPUName=[=[SM6375]=],Level=3,}
Device_AndroidCPU["SM6450"] = {CPUName=[=[SM6450]=],Level=3,}
Device_AndroidCPU["SDM710"] = {CPUName=[=[SDM710]=],Level=1,}
Device_AndroidCPU["SDM712"] = {CPUName=[=[SDM712]=],Level=1,}
Device_AndroidCPU["SM7125"] = {CPUName=[=[SM7125]=],Level=1,}
Device_AndroidCPU["SM7150-AA"] = {CPUName=[=[SM7150-AA]=],Level=2,}
Device_AndroidCPU["SDM730"] = {CPUName=[=[SDM730]=],Level=2,}
Device_AndroidCPU["SM7150-AB"] = {CPUName=[=[SM7150-AB]=],Level=2,}
Device_AndroidCPU["SDM730G"] = {CPUName=[=[SDM730G]=],Level=2,}
Device_AndroidCPU["SDM730G AIE"] = {CPUName=[=[SDM730G]=],Level=2,}
Device_AndroidCPU["SM7225"] = {CPUName=[=[SM7225]=],Level=2,}
Device_AndroidCPU["SM7250-AA"] = {CPUName=[=[SM7250-AA]=],Level=3,}
Device_AndroidCPU["SM7250-AB"] = {CPUName=[=[SM7250-AB]=],Level=3,}
Device_AndroidCPU["SM7250-AC"] = {CPUName=[=[SM7250-AC]=],Level=3,}
Device_AndroidCPU["SDM765G"] = {CPUName=[=[SM7250-AC]=],Level=3,}
Device_AndroidCPU["SDM765G AIE"] = {CPUName=[=[SM7250-AC]=],Level=3,}
Device_AndroidCPU["SM7325"] = {CPUName=[=[SM7325]=],Level=4,}
Device_AndroidCPU["SM7325-AE"] = {CPUName=[=[SM7325-AE]=],Level=4,}
Device_AndroidCPU["SM7350"] = {CPUName=[=[SM7350]=],Level=4,}
Device_AndroidCPU["SM7450-AB"] = {CPUName=[=[SM7450-AB]=],Level=4,}
Device_AndroidCPU["MSM8996"] = {CPUName=[=[MSM8996]=],Level=1,}
Device_AndroidCPU["MSM8996AB"] = {CPUName=[=[MSM8996AB]=],Level=1,}
Device_AndroidCPU["MSM8998"] = {CPUName=[=[MSM8998]=],Level=2,}
Device_AndroidCPU["SDM845"] = {CPUName=[=[SDM845]=],Level=3,}
Device_AndroidCPU["SDM845 AIE"] = {CPUName=[=[SDM845]=],Level=3,}
Device_AndroidCPU["SDM850"] = {CPUName=[=[SDM850]=],Level=4,}
Device_AndroidCPU["sdm855"] = {CPUName=[=[sdm855]=],Level=5,}
Device_AndroidCPU["SM8150"] = {CPUName=[=[SM8150]=],Level=5,}
Device_AndroidCPU["sdm855+"] = {CPUName=[=[sdm855+]=],Level=5,}
Device_AndroidCPU["SM8150-AC"] = {CPUName=[=[SM8150-AC]=],Level=5,}
Device_AndroidCPU["SM8250"] = {CPUName=[=[SM8250]=],Level=5,}
Device_AndroidCPU["SM8250-AB"] = {CPUName=[=[SM8250-AB]=],Level=5,}
Device_AndroidCPU["SM8250-AC"] = {CPUName=[=[SM8250-AC]=],Level=5,}
Device_AndroidCPU["SM8350"] = {CPUName=[=[SM8350]=],Level=5,}
Device_AndroidCPU["SM8350-AB"] = {CPUName=[=[SM8350-AB]=],Level=5,}
Device_AndroidCPU["SM8450"] = {CPUName=[=[SM8450]=],Level=5,}
Device_AndroidCPU["SM8475"] = {CPUName=[=[SM8475]=],Level=5,}
Device_AndroidCPU["SM8550-AB"] = {CPUName=[=[SM8550-AB]=],Level=5,}
Device_AndroidCPU["SM8550-AC"] = {CPUName=[=[SM8550-AC]=],Level=5,}


if not rawget(_G, "Device_Memory") then Device_Memory = {} end
Device_Memory[1] = {Level=1,AndroidMemorySize=0,IOSMemorySize=0,}
Device_Memory[2] = {Level=2,AndroidMemorySize=2700,IOSMemorySize=800,}
Device_Memory[3] = {Level=3,AndroidMemorySize=3700,IOSMemorySize=1800,}
Device_Memory[4] = {Level=4,AndroidMemorySize=5600,IOSMemorySize=2800,}
Device_Memory[5] = {Level=5,AndroidMemorySize=7500,IOSMemorySize=3700,}

if not rawget(_G, "Device_SpecialList") then Device_SpecialList = {} end
Device_SpecialList["HUAWEI LND-AL30"] = {DeviceModel=[=[HUAWEI LND-AL30]=],Level=2,}
Device_SpecialList["Google Pixel 4 XL"] = {DeviceModel=[=[Google Pixel 4 XL]=],Level=3,}
Device_SpecialList["vivo V1838A"] = {DeviceModel=[=[vivo V1838A]=],Level=3,}
Device_SpecialList["vivo vivo X20A"] = {DeviceModel=[=[vivo vivo X20A]=],Level=2,}
Device_SpecialList["Meizu M1852"] = {DeviceModel=[=[Meizu M1852]=],Level=4,}
Device_SpecialList["HUAWEI HUAWEI RIO-AL00"] = {DeviceModel=[=[HUAWEI HUAWEI RIO-AL00]=],Level=1,}
Device_SpecialList["OnePlus ONEPLUS A6010"] = {DeviceModel=[=[OnePlus ONEPLUS A6010]=],Level=3,}



--圆角屏设备，刘海屏不要填入下表
if not rawget(_G, "Device_RoundedScreen") then Device_RoundedScreen = {} end
Device_RoundedScreen["Meizu 16th"] = {Margin=40,} --魅族 16th
Device_RoundedScreen["Meizu 16th Plus"] = {Margin=40,} --魅族 16th Plus
Device_RoundedScreen["samsung SM-G965N"] = {Margin=40,} --三星 Galaxy S9+
Device_RoundedScreen["OPPO PAFM00"] = {Margin=40,} --OPPO Find X
Device_RoundedScreen["vivo vivo NEX A"] = {Margin=40,} --vivo NEX
Device_RoundedScreen["vivo vivo NEX S"] = {Margin=40,} --vivo NEX 旗舰版
Device_RoundedScreen["samsung SM-N9500"] = {Margin=40,} --三星 note 8
Device_RoundedScreen["Xiaomi MIX 3"] = {Margin=40,} --小米 MIX 3
Device_RoundedScreen["OPPO PBBM00"] = {Margin=40,} --OPPO A7x
Device_RoundedScreen["OPPO PCAM10"] = {Margin=40,} --OPPO A9
Device_RoundedScreen["Realme RMX1851"] = {Margin=40,} --realme X 青春版
Device_RoundedScreen["vivo V1916A"] = {Margin=40,} --vivo iQOO Pro(5G)
Device_RoundedScreen["vivo V1809A"] = {Margin=40,} --vivo X23
Device_RoundedScreen["vivo  V1901A"] = {Margin=40,} --vivo Y3
Device_RoundedScreen["vivo V1818CT"] = {Margin=40,} --vivo Y93 标准版
Device_RoundedScreen["vivo V1813A"] = {Margin=40,} --vivo Y97
Device_RoundedScreen["OnePlus GM1900"] = {Margin=40,} --一加 7
Device_RoundedScreen["samsung SM-N9760"] = {Margin=40,} --三星 GALAXY Note 10+(5G)
Device_RoundedScreen["Samsung SM-A7050"] = {Margin=40,} --三星Galaxy A70(SM-A7050)
Device_RoundedScreen["Samsung SM-G9730"] = {Margin=40,} --三星Galaxy S10(SM-G9730)
Device_RoundedScreen["HUAWEI VER-AN00"] = {Margin=40,} --华为 Mate 20 X(5G)
Device_RoundedScreen["HUAWEI VCE-AL00"] = {Margin=40,} --华为 nova 4
Device_RoundedScreen["HUAWEI SEA-AL10"] = {Margin=40,} --华为 nova 5 Pro(5G)
Device_RoundedScreen["HUAWEIEML-AL00"] = {Margin=40,} --华为 P20
Device_RoundedScreen["HUAWEI VOG-AL10"] = {Margin=40,} --华为 P30 Pro
Device_RoundedScreen["HUAWEI DUB-AL20"] = {Margin=40,} --华为 畅享 9
Device_RoundedScreen["HUAWEI COL-AL10"] = {Margin=40,} --华为 荣耀 10
Device_RoundedScreen["HUAWEI HRY-AL00a"] = {Margin=40,} --华为 荣耀 10 青春版
Device_RoundedScreen["Xiaomi Redmi Note 7"] = {Margin=40,} --红米 Note 7
Device_RoundedScreen["Xiaomi Redmi Note 8"] = {Margin=40,} --红米 Note 8
Device_RoundedScreen["HUAWEI JSN-AL00a"] = {Margin=40,} --荣耀 8X

--部分刘海屏
Device_RoundedScreen["OPPO CPH1851"] = {Margin=40,} --OPPO AX5台版
Device_RoundedScreen["vivo vivo X21A"] = {Margin=40,} --vivo X21
Device_RoundedScreen["vivo vivo X21iA"] = {Margin=40,} --vivo X21i
Device_RoundedScreen["VIVO VIVO X21UD A"] = {Margin=40,} --vivo X21UD
Device_RoundedScreen["vivo vivo Y83"] = {Margin=40,} --vivo Y83
Device_RoundedScreen["vivo vivo Y85A"] = {Margin=40,} --vivo Y85
Device_RoundedScreen["vivo vivo Z1"] = {Margin=40,} --vivo Z1
Device_RoundedScreen["OnePlus ONEPLUS A6000"] = {Margin=40,} --一加6
Device_RoundedScreen["HUAWEI LYA-AL000"] = {Margin=40,} --华为 Mate 20 Pro
Device_RoundedScreen["HUAWEI TAS-AL00"] = {Margin=40,} --华为 Mate 30
Device_RoundedScreen["HUAWEI ANE-AL00"] = {Margin=40,} --华为 Nova 3e
Device_RoundedScreen["HUAWEIINE-AL00"] = {Margin=40,} --华为 Nova 3i
Device_RoundedScreen["Xiaomi MI 8 SE"] = {Margin=40,} --小米 8 SE
Device_RoundedScreen["Xiaomi MI 8 Lite"] = {Margin=40,} --小米 8 SE
Device_RoundedScreen["samsung SM-F907N"] = {Margin=80,} --三星 Galaxy Fold 5G
Device_RoundedScreen["HUAWEI MRX-W09"] = {Margin=80,} --华为MatePadPro
Device_RoundedScreen["OPPO PEUM00"] = {Margin=50,} --OPPO Find N

EnumDevicePerformanceLevel = {
	VeryLow = 1, --超低配置
	Low = 2, --低配置
	Mid = 3, --中等配置
	High = 4, --高配置
	VeryHigh = 5, --超高配置
}

g_DeviceMgr = {}

local CommonDefs = import "L10.Game.CommonDefs"
local SystemInfo = import "UnityEngine.SystemInfo"
local NativeTools = import "L10.Engine.NativeTools"
local Main = import "L10.Engine.Main"
local SdkU3d = import "NtUniSdk.Unity3d.SdkU3d"

function g_DeviceMgr:GetDeviceLevel()
	if LuaCloudGameFaceMgr:IsCloudGameFace() then return EnumDevicePerformanceLevel.VeryHigh end
	if NativeTools.IsMuMuDevice() then return EnumDevicePerformanceLevel.High end
	if CommonDefs.IsPCGameMode() then
		--PC目前默认按最高配置来处理
		return EnumDevicePerformanceLevel.VeryHigh
	end
	if SdkU3d.IsCloudGameCloudClient() then
		--unisdk的云端包默认按最高配置处理
		return EnumDevicePerformanceLevel.VeryHigh
	end

	local deviceModel = SystemInfo.deviceModel

	if Device_SpecialList[deviceModel] then return Device_SpecialList[deviceModel].Level end

	if Device_DeviceModel[deviceModel] then return Device_DeviceModel[deviceModel].Level end

	if CommonDefs.IsAndroidPlatform() then
        --依次检查cpu名称，cpu名称的小写，cpu名称小写去掉空格
		local cpuName = NativeTools.GetAndroidCPUName()
		if cpuName~=nil and Device_AndroidCPU[cpuName] then return Device_AndroidCPU[cpuName].Level end
        local lowerCpuName = cpuName and string.lower(cpuName) or nil
        if cpuName~=lowerCpuName and Device_AndroidCPU[lowerCpuName] then return Device_AndroidCPU[lowerCpuName].Level end
        local lastBlankCharIndex = lowerCpuName~=nil and string.find(lowerCpuName, "%s[^%s]+$") or nil
        if lastBlankCharIndex then
            local lastComponent = string.sub(lowerCpuName, lastBlankCharIndex+1)
            if Device_AndroidCPU[lastComponent] then return Device_AndroidCPU[lastComponent].Level end
        end
		local hardware = NativeTools.GetAndroidHardwareName()
		if hardware~=nil and Device_AndroidCPU[hardware] then return Device_AndroidCPU[hardware].Level end
	end

	local key = CommonDefs.IsAndroidPlatform() and "AndroidMemorySize" or "IOSMemorySize"
	local fitMinRequirementForHighPerformance = g_DeviceMgr:CheckMinRequirementForHighPerformance()
	local systemMemorySize = NativeTools.GetNativeSystemMemorySize()
	if fitMinRequirementForHighPerformance and Device_Memory[EnumDevicePerformanceLevel.VeryHigh] and systemMemorySize >= Device_Memory[EnumDevicePerformanceLevel.VeryHigh][key] then
		return EnumDevicePerformanceLevel.VeryHigh
	elseif fitMinRequirementForHighPerformance and Device_Memory[EnumDevicePerformanceLevel.High] and systemMemorySize >= Device_Memory[EnumDevicePerformanceLevel.High][key] then
		return EnumDevicePerformanceLevel.High
	elseif Device_Memory[EnumDevicePerformanceLevel.Mid] and systemMemorySize >= Device_Memory[EnumDevicePerformanceLevel.Mid][key] then
		return EnumDevicePerformanceLevel.Mid
	elseif Device_Memory[EnumDevicePerformanceLevel.Low] and systemMemorySize >= Device_Memory[EnumDevicePerformanceLevel.Low][key] then
		return EnumDevicePerformanceLevel.Low
	else
		return EnumDevicePerformanceLevel.VeryLow
	end
end

function g_DeviceMgr:CheckMinRequirementForHighPerformance()
	if not CommonDefs.IsInMobileDevice() then
		--PC目前默认按最高配置来处理
		return true
	end
	--根据2016年发布的系统版本来初步筛选高配与超高配设备
	--iOS Verion >= 10 (iOS 10, iPhone7 iPhone7P)
	--Android API Level >= 24 (Android 7.0, HuaWei Mate 9/XiaoMi 5/OPPO R9/vivo X9) 注除了华为其他都是基于Android6.0和Android5.0
	if  CommonDefs.IsAndroidPlatform() then
		return NativeTools.GetAndroidAPILevel() >=24
	elseif CommonDefs.IsIOSPlatform() then
		if Main.Inst.EngineVersion <0 or Main.Inst.EngineVersion >369396  then --这个引擎版本判断在下次iOS更新后去掉即可，为了避免C#不更新的情况
			return NativeTools.GetIOSSystemVersion() >=10.0
		else
			return true
		end
	else
		return true
	end
end

function g_DeviceMgr:RequestLogPCloginExtraInfo(cpuName, osName, systemMemorySize, graphicsDeviceName, graphicsMemorySize)
	Gac2Gas.RequestLogPCloginExtraInfo(cpuName, osName, tostring(systemMemorySize), graphicsDeviceName, tostring(graphicsMemorySize))
end

--获取圆角屏的边距值，用于处理UI过于靠近四角导致遮挡的问题
--注意，只检查非刘海屏的设备，刘海屏用其他逻辑检查
function g_DeviceMgr.GetRoundScreenMagrin(devicelModel, deviceModelToLower)
	if not (CommonDefs.IsInMobileDevice() and devicelModel and deviceModelToLower) then
		return 0
	end

	-- 精确匹配
	if Device_RoundedScreen[devicelModel] then
		return Device_RoundedScreen[devicelModel].Margin
	end

	-- 模糊匹配
	-- 魅族16th系列，只考虑常规
	-- 魅族 16th手机的四个圆角角度很大，导致四角UI遮挡，该平台没有提供相应接口，这里做个特殊处理利用刘海屏代码进行一下适配
    -- 已经知会交互，新设计避免往四角放置UI，留出必要空隙
    -- 该方案不具有普适应，将来新设备仍要继续适配
	if string.find(deviceModelToLower, "meizu", 1, true) then
		if string.find(deviceModelToLower, "16th", 1, true) then
			return 40
		end
	end

	return 0
end

local checked=false
function g_DeviceMgr.TryCheckMuMu()
	if CGuideMgr.Inst:IsNewbiePlayId() then return end
	if not NativeTools.IsMuMuDevice() then return end

	local CWinSocialWndMgr=import "L10.UI.CWinSocialWndMgr"
	--获取本地设置的变量
	local isopen = PlayerPrefs.GetInt("mumu_outchat_open")
	if isopen==1 then
		if not CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
			CWinSocialWndMgr.Inst:OpenOrCloseWinSocialWnd()
		end
	elseif isopen==-1 then
		--关闭状态，不用处理
	else
		if checked then return end
		checked = true
		if not CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
			g_DeviceMgr.SetMuMuChatOpen()
			CWinSocialWndMgr.Inst:OpenOrCloseWinSocialWnd()
		end
	end
end
function g_DeviceMgr.SetMuMuChatOpen()
	PlayerPrefs.SetInt("mumu_outchat_open",1)
end
function g_DeviceMgr.SetMuMuChatClose()
	PlayerPrefs.SetInt("mumu_outchat_open",-1)
end

local removed=false
function g_DeviceMgr.TryRemoveTempApkFile()
	if removed then return end
	local savePath = Utility.persistentDataPath
	savePath = savePath .. "/qnmobile.apk"
	if File.Exists(savePath) then
		File.Delete(savePath)
	end
	removed = true
end
