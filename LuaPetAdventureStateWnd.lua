local CButton = import "L10.UI.CButton"
local CUITexture = import "L10.UI.CUITexture"

LuaPetAdventureStateWnd = class()

RegistChildComponent(LuaPetAdventureStateWnd, "ExpandButton", CButton)
RegistChildComponent(LuaPetAdventureStateWnd, "Tip", GameObject)

RegistChildComponent(LuaPetAdventureStateWnd, "PlayerGrid", UIGrid)
RegistChildComponent(LuaPetAdventureStateWnd, "Self", GameObject)
RegistChildComponent(LuaPetAdventureStateWnd, "PlayerInfo", GameObject)

RegistClassMember(LuaPetAdventureStateWnd, "PlayerInfoList")
RegistClassMember(LuaPetAdventureStateWnd, "RankMatBasePath")

function LuaPetAdventureStateWnd:Init()
    self.PlayerInfo:SetActive(false)
    self.RankMatBasePath = "UI/Texture/Transparent/Material/grabchristmasgiftrankwnd_huangguang_0%d.mat"
    CommonDefs.AddOnClickListener(self.ExpandButton.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnExpandButtonClicked(go)
    end), false)
    self.PlayerInfoList = {}
    self:InitPlayerInfos()
end


function LuaPetAdventureStateWnd:OnExpandButtonClicked(go)
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

--@desc 初始化，没有数据时的显示
function LuaPetAdventureStateWnd:InitPlayerInfos()
    self:InitPlayerItem(self.Self, nil, true)
    CUICommonDef.ClearTransform(self.PlayerGrid.transform)

    for i = 1, 4 do
        local go = NGUITools.AddChild(self.PlayerGrid.gameObject, self.PlayerInfo)
        self:InitPlayerItem(go, nil, false)
        go:SetActive(true)
    end

    self.PlayerGrid:Reposition()
end

--[[
rank,
playerId,
hpPercent,
aliveTime,
playerName,
isDead = isDead,
]]--
function LuaPetAdventureStateWnd:InitPlayerItem(go, info, isSelf)
    local rankTexture = go.transform:Find("RankTexture"):GetComponent(typeof(CUITexture))
    local rankLabel = go.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local playerNameLabel = go.transform:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
    local playerScoreLabel = go.transform:Find("PlayerScoreLabel"):GetComponent(typeof(UILabel))

    rankTexture:Clear()
    rankLabel.text = "-"
    playerScoreLabel.text = "-"
    
    if isSelf then
        playerNameLabel.text = CClientMainPlayer.Inst.Name
    else
        playerNameLabel.text = LocalString.GetString("暂无")
    end

    if not info and not isSelf then
        rankLabel.alpha = 0.5
        playerNameLabel.alpha = 0.5
        playerScoreLabel.alpha = 0.5
    else
        rankLabel.alpha = 1
        playerNameLabel.alpha = 1
        playerScoreLabel.alpha = 1
    end

    if not info then return end

    if tonumber(info.rank) < 4 then
        rankTexture:LoadMaterial(SafeStringFormat3(self.RankMatBasePath, info.rank))
        rankTexture.gameObject:SetActive(true)
        rankLabel.text = nil
    else
        rankTexture:Clear()
        rankTexture.gameObject:SetActive(false)
        rankLabel.text = tostring(info.rank)
    end
    
    playerNameLabel.text = info.playerName
    playerScoreLabel.text = info.hpPercent
end

function LuaPetAdventureStateWnd:UpdatePlayerInfos()
    
    if not LuaHanJiaMgr.PetAdventureSelfInfo then return end
    self:InitPlayerItem(self.Self, LuaHanJiaMgr.PetAdventureSelfInfo, true)

    if not LuaHanJiaMgr.PetAdventureInfoList then return end

    CUICommonDef.ClearTransform(self.PlayerGrid.transform)
    for i = 1, 4 do
        local go = NGUITools.AddChild(self.PlayerGrid.gameObject, self.PlayerInfo)
        local info = LuaHanJiaMgr.PetAdventureInfoList[i]
        self:InitPlayerItem(go, info, false)
        go:SetActive(true)
    end

    self.PlayerGrid:Reposition()
end

function LuaPetAdventureStateWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncHanJiaAdventurePlayInfo", self, "UpdatePlayerInfos")
end

function LuaPetAdventureStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncHanJiaAdventurePlayInfo", self, "UpdatePlayerInfos")
end