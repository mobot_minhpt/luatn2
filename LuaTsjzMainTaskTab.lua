local QnTableView               = import "L10.UI.QnTableView"
local DefaultTableViewDataSource= import "L10.UI.DefaultTableViewDataSource"
local ClientAction              = import "L10.UI.ClientAction"
local UILabel                   = import "UILabel"
local DelegateFactory		    = import "DelegateFactory"
local UIEventListener		    = import "UIEventListener"
local LocalString               = import "LocalString"
local LuaGameObject             = import "LuaGameObject"
local CServerTimeMgr            = import "L10.Game.CServerTimeMgr"
local CUIManager                = import "L10.UI.CUIManager"
local UITabBar                  = import "L10.UI.UITabBar"
local QnTabButton               = import "L10.UI.QnTabButton"

--天成酒壶任务页签
LuaTsjzMainTaskTab = class()

RegistChildComponent(LuaTsjzMainTaskTab, "TaskTitleLB1",		    UILabel)
RegistChildComponent(LuaTsjzMainTaskTab, "TaskTitleLB2",		    UILabel)
RegistChildComponent(LuaTsjzMainTaskTab, "QnTableView1",		    QnTableView)
RegistChildComponent(LuaTsjzMainTaskTab, "QnTableView2",		    QnTableView)
RegistChildComponent(LuaTsjzMainTaskTab, "WeekBtn1",		        QnTabButton)
RegistChildComponent(LuaTsjzMainTaskTab, "WeekBtn2",		        QnTabButton)
RegistChildComponent(LuaTsjzMainTaskTab, "WeekBtn3",		        QnTabButton)
RegistChildComponent(LuaTsjzMainTaskTab, "WeekBtn4",		        QnTabButton)
RegistChildComponent(LuaTsjzMainTaskTab, "WeekBtn5",		        QnTabButton)
RegistChildComponent(LuaTsjzMainTaskTab, "TabBar",		            UITabBar)


RegistClassMember(LuaTsjzMainTaskTab,  "DailyTasks")
RegistClassMember(LuaTsjzMainTaskTab,  "WeekTasks")
RegistClassMember(LuaTsjzMainTaskTab,  "CurWeekIndex")
RegistClassMember(LuaTsjzMainTaskTab,  "Btns")
RegistClassMember(LuaTsjzMainTaskTab,  "Isdirty")

function LuaTsjzMainTaskTab:Awake()
    self.CurWeekIndex = 0
    self.Btns = {self.WeekBtn1,self.WeekBtn2,self.WeekBtn3,self.WeekBtn4,self.WeekBtn5}

    g_ScriptEvent:AddListener("OnTsjzDataChange", self, "OnTsjzDataChange")
    self:OnTsjzDataChange()
end


function LuaTsjzMainTaskTab:OnDestroy()
    g_ScriptEvent:RemoveListener("OnTsjzDataChange", self, "OnTsjzDataChange")
end

function LuaTsjzMainTaskTab:OnEnable()
    if self.Isdirty then
        self:OnTsjzDataChange()
    end
end

function LuaTsjzMainTaskTab:OnTsjzDataChange()
    if not self.gameObject.activeSelf then
        self.Isdirty = true
        return 
    end
    self.Isdirty = false
    local data = LuaHanJiaMgr.TcjhMainData
    local taskinfos = data.TaskData

    self.DailyTasks = {}
    self.WeekTasks = {}
    for i = 1,5 do
        self.WeekTasks[i] = {}
    end

    local alertWeeks = {}
    local weektaskcount = 0
    local weekrewardcount = 0
    local dailytaskcount = 0
    local dailyrewardcount =0

    for i = 1,#taskinfos do
        local task = taskinfos[i]
        if task.TaskID < 20000 then
            dailytaskcount = dailytaskcount + 1
            if task.IsRewarded then
                dailyrewardcount = dailyrewardcount + 1
            end
            self.DailyTasks[#self.DailyTasks + 1] = task
        else
            weektaskcount = weektaskcount + 1
            if task.IsRewarded then 
                weekrewardcount = weekrewardcount + 1
            end
            local week = math.floor((task.TaskID - 20000) / 1000)
            local weektask = self.WeekTasks[week]
            weektask[#weektask + 1] = task
            local canreward = self:CanRewarded(task)
            if canreward then
                alertWeeks[week] = 1
            end
        end
    end

    self:SortTable(self.DailyTasks)
    for i = 1,5 do
        self:SortTable(self.WeekTasks[i])
    end

    self.TaskTitleLB1.text = SafeStringFormat3(LocalString.GetString("今日任务  %d/%d"), dailyrewardcount,dailytaskcount)
    self.TaskTitleLB2.text = SafeStringFormat3(LocalString.GetString("挑战任务  %d/%d"), weekrewardcount,weektaskcount)
    
    local len = #self.DailyTasks
    local initfunc = function (item,index)
        self:FillItem(item,self.DailyTasks[index+1])
    end
    self.QnTableView1.m_DataSource = DefaultTableViewDataSource.CreateByCount(len,initfunc)
    self.QnTableView1:ReloadData(true, true)

    self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabChange(index+1)
    end)
    self.TabBar.validator = LuaUtils.UITabBar_Validate(function(index)
        return self:OnTabChangeValidate(index+1)
    end)
    local curweek = 1
    for i=1,#self.Btns do
        local btn = self.Btns[i]
        if #self.WeekTasks[i] <= 0 then
            LuaGameObject.GetChildNoGC(btn.transform,"Label").gameObject:SetActive(false)
            LuaGameObject.GetChildNoGC(btn.transform,"LockLabel").gameObject:SetActive(true)
            LuaGameObject.GetChildNoGC(btn.transform,"AlertSprite").gameObject:SetActive(false)
        else
            curweek = i
            LuaGameObject.GetChildNoGC(btn.transform,"AlertSprite").gameObject:SetActive(alertWeeks[i] and alertWeeks[i] == 1)
        end
    end
    if self.CurWeekIndex > 0 then 
        self.TabBar:ChangeTab(self.CurWeekIndex-1)
    else
        self.TabBar:ChangeTab(curweek-1)
    end
end

function LuaTsjzMainTaskTab:OnTabChangeValidate(index)
    return #self.WeekTasks[index] > 0
end

function LuaTsjzMainTaskTab:CanRewarded(task)
    local taskcfg = HanJia2022_TianShuoTask.GetData(task.TaskID)
    local isOutDate = self:IsTaskOutDate(task)
    return (task.Progress >= taskcfg.Target or taskcfg.Target == 0) and not task.IsRewarded and not isOutDate
end

function LuaTsjzMainTaskTab:SortTable(value)
    if #value < 2 then return end
    table.sort(value, function(a, b)
        local isaout = self:IsTaskOutDate(a)
        local isbout = self:IsTaskOutDate(b)
        if not isaout and isbout then
            return true
        elseif isaout and not isbout then
            return false
        else
            if not a.IsRewarded and  b.IsRewarded then
                return true
            elseif a.IsRewarded and not b.IsRewarded then
                return false
            else
                local ar = self:IsRewarding(a)
                local br = self:IsRewarding(b)
                if ar ~= br then 
                    return ar > br
                else
                    return a.TaskID < b.TaskID
                end
            end
        end
    end)
end

function LuaTsjzMainTaskTab:IsRewarding(task)
    local taskcfg = HanJia2022_TianShuoTask.GetData(task.TaskID)
    if task.Progress >= taskcfg.Target or taskcfg.Target == 0 then 
        return 1
    else
        return 0
    end
end

function LuaTsjzMainTaskTab:IsTaskOutDate(task)
    local taskcfg = HanJia2022_TianShuoTask.GetData(task.TaskID)
    local curtime = CServerTimeMgr.Inst.timeStamp
    local taskendtime = CServerTimeMgr.Inst:GetTimeStampByStr(taskcfg.ExpireTime)
    return curtime > taskendtime
end

function LuaTsjzMainTaskTab:FillItem(item,task)

    local isnormal = task.TaskID < 20000
    local taskcfg = HanJia2022_TianShuoTask.GetData(task.TaskID)

	local exp = taskcfg.JingCuiReward
	local passtype = LuaHanJiaMgr.TcjhMainData.PassType
	if passtype == 2 or passtype == 3 then
		local setting = HanJia2022_TianShuoSetting.GetData()
		local fac = setting.Vip2JingCuiAddRate
		exp = math.floor(taskcfg.JingCuiReward * fac)
	end

    local isOutDate = self:IsTaskOutDate(task)

    local transform = item.transform
    local bg1 = LuaGameObject.GetChildNoGC(transform,"Bg1").gameObject
    local bg2 = LuaGameObject.GetChildNoGC(transform,"Bg2").gameObject
    local deslb = LuaGameObject.GetChildNoGC(transform,"DesLabel").label
    local explb = LuaGameObject.GetChildNoGC(transform,"ExpLabel").label
    local countlb = LuaGameObject.GetChildNoGC(transform,"CountLabel").label
    local cmpgo = LuaGameObject.GetChildNoGC(transform,"CompleteSprite").gameObject
    local odgo = LuaGameObject.GetChildNoGC(transform,"OutDateSprite").gameObject
    local gotobtn = LuaGameObject.GetChildNoGC(transform,"GotoTaskBtn").gameObject
    local rewardbtn = LuaGameObject.GetChildNoGC(transform,"RewardBtn").gameObject
    local extgo = LuaGameObject.GetChildNoGC(transform,"ExtSprite").gameObject

    bg1:SetActive(isnormal)
    bg2:SetActive(not isnormal)

    deslb.text = taskcfg.Desc
    explb.text = "+"..exp
    countlb.text = task.Progress.."/"..taskcfg.Target

    gotobtn:SetActive(task.Progress < taskcfg.Target and not isOutDate)
    rewardbtn:SetActive((task.Progress >= taskcfg.Target or taskcfg.Target == 0) and not task.IsRewarded and not isOutDate)
    cmpgo:SetActive(task.IsRewarded)
    odgo:SetActive(isOutDate)
    extgo:SetActive(passtype == 2 or passtype == 3)

    UIEventListener.Get(gotobtn).onClick = DelegateFactory.VoidDelegate(function(go)
        ClientAction.DoAction(taskcfg.Action);
        CUIManager.CloseUI(CLuaUIResources.TsjzMainWnd)
    end)

    UIEventListener.Get(rewardbtn).onClick = DelegateFactory.VoidDelegate(function(go)
        LuaHanJiaMgr.RequireTsjzTaskReward(task.TaskID)
    end)
end

--@region UIEvent

function LuaTsjzMainTaskTab:OnTabChange(week)
    self:OnWeekBtnBtnClick(week)
end

function LuaTsjzMainTaskTab:OnWeekBtnBtnClick(week)

    local weektasks = self.WeekTasks[week]
    if weektasks == nil or #weektasks <= 0 then 
        g_MessageMgr:ShowMessage("TCJH_WEEK_Task_LOCK",week)--Msg:TCJH_WEEK_Task_LOCK
        --return
    end

    --if self.CurWeekIndex ~= week then
    --    if self.CurWeekIndex > 0 then
    --        LuaGameObject.GetChildNoGC(self.Btns[self.CurWeekIndex].transform,"SelectSprite").gameObject:SetActive(false)
    --    end
    --    self.CurWeekIndex = week
    --    LuaGameObject.GetChildNoGC(self.Btns[self.CurWeekIndex].transform,"SelectSprite").gameObject:SetActive(true)
    --end
    
    local len = #weektasks
    local initfunc = function(item,index)
        local task = weektasks[index+1]
        self:FillItem(item,task)
    end
    self.QnTableView2.m_DataSource = DefaultTableViewDataSource.CreateByCount(len,initfunc)
    self.QnTableView2:ReloadData(true, true)
end

--@endregion
