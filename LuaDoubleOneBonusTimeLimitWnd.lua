local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUIFx = import "L10.UI.CUIFx"
local CFirstChargeGiftCell = import "L10.UI.CFirstChargeGiftCell"
local Mall_LingYuMallLimit=import "L10.Game.Mall_LingYuMallLimit"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CPayMgr = import "L10.Game.CPayMgr"

LuaDoubleOneBonusTimeLimitWnd = class()
RegistChildComponent(LuaDoubleOneBonusTimeLimitWnd,"closeBtn", GameObject)
RegistChildComponent(LuaDoubleOneBonusTimeLimitWnd,"gift1", CFirstChargeGiftCell)
RegistChildComponent(LuaDoubleOneBonusTimeLimitWnd,"gift2", CFirstChargeGiftCell)
RegistChildComponent(LuaDoubleOneBonusTimeLimitWnd,"gift3", CFirstChargeGiftCell)
RegistChildComponent(LuaDoubleOneBonusTimeLimitWnd,"oriValue", UILabel)
RegistChildComponent(LuaDoubleOneBonusTimeLimitWnd,"realValue", UILabel)
RegistChildComponent(LuaDoubleOneBonusTimeLimitWnd,"chargeBtn", GameObject)
RegistChildComponent(LuaDoubleOneBonusTimeLimitWnd,"leftTimeLabel", UILabel)
RegistChildComponent(LuaDoubleOneBonusTimeLimitWnd,"oriValue1", UILabel)
RegistChildComponent(LuaDoubleOneBonusTimeLimitWnd,"realValue1", UILabel)
RegistChildComponent(LuaDoubleOneBonusTimeLimitWnd,"pro1Node", GameObject)
RegistChildComponent(LuaDoubleOneBonusTimeLimitWnd,"pro2Node", GameObject)

RegistClassMember(LuaDoubleOneBonusTimeLimitWnd, "leftTime")
RegistClassMember(LuaDoubleOneBonusTimeLimitWnd, "m_CountDownTick")
RegistClassMember(LuaDoubleOneBonusTimeLimitWnd, "m_TimeStamp")

function LuaDoubleOneBonusTimeLimitWnd:Close()
	CUIManager.CloseUI("DoubleOneBonusTimeLimitWnd")
end

function LuaDoubleOneBonusTimeLimitWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaDoubleOneBonusTimeLimitWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaDoubleOneBonusTimeLimitWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	if not LuaDoubleOne2019Mgr.RMBPackageId then
		self:Close()
		return
	end

	local itemData = SinglesDay2019_FlashSale.GetData(LuaDoubleOne2019Mgr.RMBPackageId)
	if not itemData then
		return
	end

	local itemDataTable = g_LuaUtil:StrSplit(itemData.ItemInfo,";")
	local nodeTable = {self.gift1,self.gift2,self.gift3}
	for i,v in ipairs(itemDataTable) do
		local vTable = g_LuaUtil:StrSplit(v,",")
		nodeTable[i]:Init(vTable[1],vTable[2])
		nodeTable[i].transform:Find('fx'):GetComponent(typeof(CUIFx)):LoadFx("Fx/UI/Prefab/UI_meirichoujiang_liuguang.prefab")
	end

  local realValueNum = itemData.DiscountPrice
  local oriValueNum = itemData.OriginalPrice

	if itemData.IsYuanItem == 1 then
		self.pro1Node:SetActive(true)
		self.pro2Node:SetActive(false)

		self.oriValue.text = oriValueNum .. LocalString.GetString('元')
		self.realValue.text = realValueNum

		local pid = Mall_LingYuMallLimit.GetData(LuaDoubleOne2019Mgr.RMBPackageId).RmbPID
		local onChargeClick = function(go)
			CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(pid),0)
		end
		CommonDefs.AddOnClickListener(self.chargeBtn,DelegateFactory.Action_GameObject(onChargeClick),false)
	else
		self.pro1Node:SetActive(false)
		self.pro2Node:SetActive(true)

		self.oriValue1.text = oriValueNum
		self.realValue1.text = realValueNum

		local onChargeClick = function(go)
			Gac2Gas.BuyMallItem(EShopMallRegion_lua.ELingyuMallLimit, LuaDoubleOne2019Mgr.RMBPackageId, 1)
		end
		CommonDefs.AddOnClickListener(self.chargeBtn,DelegateFactory.Action_GameObject(onChargeClick),false)
	end

  self.leftTime = CLuaActivityAlert.s_DoubleOneBonusTimeRemain
  self.m_TimeStamp = CServerTimeMgr.Inst.timeStamp
  self:UpdateCountDown()
  if self.m_CountDownTick then
    UnRegisterTick(self.m_CountDownTick)
    self.m_CountDownTick = nil
  end
  self.m_CountDownTick = RegisterTick(function() self:UpdateCountDown() end, 1000)
end

function LuaDoubleOneBonusTimeLimitWnd:UpdateCountDown()
	local deltaTime = CServerTimeMgr.Inst.timeStamp - self.m_TimeStamp
  --local leftTime = math.floor(self.leftTime - deltaTime)
  local leftTime = CLuaActivityAlert.s_DoubleOneBonusTimeRemain
  if leftTime and leftTime > 0 then
    local hour = math.floor(leftTime / 3600)
		local fen = math.floor((leftTime - hour * 3600) / 60)
    local second = leftTime - hour * 3600 - fen * 60
    if fen < 10 then
      fen = '0' .. fen
    end
    if second < 10 then
      second = '0' .. second
    end
    self.leftTimeLabel.text = hour .. ':' .. fen .. ':' .. second
  else
    self.leftTimeLabel.text = '0:00:00'
    self:Close()
  end
end

function LuaDoubleOneBonusTimeLimitWnd:OnDestroy()
  if self.m_CountDownTick then
    UnRegisterTick(self.m_CountDownTick)
    self.m_CountDownTick = nil
  end
end

return LuaDoubleOneBonusTimeLimitWnd
