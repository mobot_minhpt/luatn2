-- Auto Generated!!
local CCommonDPSInfoDisplayMgr = import "L10.UI.CCommonDPSInfoDisplayMgr"
local CDaDiZiStateWnd = import "L10.UI.CDaDiZiStateWnd"
local CTopAndRightTipWnd = import "L10.UI.CTopAndRightTipWnd"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local EnumDPSInfoDisplayType = import "L10.UI.EnumDPSInfoDisplayType"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Vector3 = import "UnityEngine.Vector3"
CDaDiZiStateWnd.m_OnClickExpandButton_CS2LuaHook = function (this, go) 
    this.expandButton.transform.localEulerAngles = Vector3(0, 0, 0)
    CTopAndRightTipWnd.showPackage = false
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end
CDaDiZiStateWnd.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListener(EnumEventType.OnCommonGameplayDPSInfoUpate, MakeDelegateFromCSFunction(this.OnCommonGameplayDPSInfoUpate, Action0, this))
    EventManager.RemoveListener(EnumEventType.HideTopAndRightTipWnd, MakeDelegateFromCSFunction(this.OnHideTopAndRightTipWnd, Action0, this))

    --这个界面不显示的话，那么tip界面肯定也不显示
    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end
end
CDaDiZiStateWnd.m_Refresh_CS2LuaHook = function (this) 
    this.nameLabel1.text = CCommonDPSInfoDisplayMgr.Inst.Player1Name
    this.nameLabel2.text = CCommonDPSInfoDisplayMgr.Inst.Player2Name
    this.valueLabel1.text = tostring(CCommonDPSInfoDisplayMgr.Inst.Player1DPS)
    this.valueLabel2.text = tostring(CCommonDPSInfoDisplayMgr.Inst.Player2DPS)
end


CCommonDPSInfoDisplayMgr.m_Clear_CS2LuaHook = function (this) 
    this.InfoType = EnumDPSInfoDisplayType.Undefined
    this.Player1Name = nil
    this.Player1DPS = 0
    this.Player2Name = nil
    this.Player2DPS = 0
end
CCommonDPSInfoDisplayMgr.m_UpdateInfo_CS2LuaHook = function (this, type, player1Name, player1DPS, player2Name, player2DPS) 
    this.InfoType = type
    this.Player1Name = player1Name
    this.Player1DPS = player1DPS
    this.Player2Name = player2Name
    this.Player2DPS = player2DPS
    EventManager.Broadcast(EnumEventType.OnCommonGameplayDPSInfoUpate)
end
