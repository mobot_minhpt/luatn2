local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"

LuaMJSYInfoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaMJSYInfoWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaMJSYInfoWnd, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaMJSYInfoWnd, "Desc", "Desc", UILabel)
RegistChildComponent(LuaMJSYInfoWnd, "Background", "Background", UISprite)

--@endregion RegistChildComponent end

function LuaMJSYInfoWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaMJSYInfoWnd:Init()
    local idx = LuaChristmas2021Mgr.m_XianSuoIdxForTip
    if not idx then 
        CUIManager.CloseUI(CLuaUIResources.MJSYInfoWnd)
        return 
    end
    local data = Christmas2021_WishInDream.GetData(idx)
    self.NameLabel.text = data.Name
    self.Icon:LoadMaterial(data.Icon)
    self.Desc.text = data.Describe
end

--@region UIEvent

--@endregion UIEvent

