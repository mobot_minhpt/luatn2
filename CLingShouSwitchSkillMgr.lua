-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CLingShouSwitchSkillMgr = import "L10.Game.CLingShouSwitchSkillMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EventManager = import "EventManager"
local UInt32 = import "System.UInt32"
CLingShouSwitchSkillMgr.m_CancleLock_CS2LuaHook = function (this) 
    CommonDefs.ListIterate(this.lockedSkillIdList, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        EventManager.BroadcastInternalForLua(EnumEventType.LingShouSwitchSkill_LockSkill, {item, false})
    end))
    CommonDefs.ListClear(this.lockedSkillIdList)
end
CLingShouSwitchSkillMgr.m_CanLock_CS2LuaHook = function (this, skillId) 
    if this.LockedSkillCount >= this.CanLockCount then
        return false
    end
    return true
end
CLingShouSwitchSkillMgr.m_AddLockedSkill_CS2LuaHook = function (this, skillId) 
    if not CommonDefs.ListContains(this.lockedSkillIdList, typeof(UInt32), skillId) then
        CommonDefs.ListAdd(this.lockedSkillIdList, typeof(UInt32), skillId)
    end
end
CLingShouSwitchSkillMgr.m_RemoveLockedSkill_CS2LuaHook = function (this, skillid) 
    if CommonDefs.ListContains(this.lockedSkillIdList, typeof(UInt32), skillid) then
        CommonDefs.ListRemove(this.lockedSkillIdList, typeof(UInt32), skillid)
    end
end
CLingShouSwitchSkillMgr.m_ClearData_CS2LuaHook = function (this) 
    this.srcLingShouId = ""
    this.destLingShouId = ""
    this.AdvanceSkillToSwitch = 0
    this.LingShouSkills1 = nil
    this.LingShouSkills2 = nil
end
CLingShouSwitchSkillMgr.m_GetLingShouPos_CS2LuaHook = function (this, lingshouId) 
    if CClientMainPlayer.Inst == nil then
        return 0
    end
    local items = CClientMainPlayer.Inst.ItemProp:GetItemAtPlace(EnumItemPlace.LingShou)
    do
        local i = 0
        while i < items.Length do
            if items[i] == lingshouId then
                return i
            end
            i = i + 1
        end
    end
    return 0
end
CLingShouSwitchSkillMgr.m_RequestJiuZhuanQianKun_CS2LuaHook = function (this) 
    if this.srcSkillPos <= 0 then
        return
    end
    Gac2Gas.RequestJiuZhuanQianKun(EnumToInt(this.place), this.pos, this.itemId, this:GetLingShouPos(this.mSrcLingShouId), this.mSrcLingShouId, this.srcSkillPos, this:GetLingShouPos(this.mDestLingShouId), this.mDestLingShouId)
end
CLingShouSwitchSkillMgr.m_RequestYiXingHuanDou_CS2LuaHook = function (this) 
    local state = 0
    if this.LingShouSkills2 ~= nil then
        CommonDefs.ListIterate(this.lockedSkillIdList, DelegateFactory.Action_object(function (___value) 
            local item = ___value
            local index = CommonDefs.Array_IndexOf_uint(this.LingShouSkills2, item)
            if index >= 0 then
                state = bit.bor(state, (bit.lshift(1, index)))
            end
        end))
    end
    Gac2Gas.RequestYiXingHuanDou(EnumToInt(this.place), this.pos, this.itemId, this:GetLingShouPos(this.mSrcLingShouId), this.mSrcLingShouId, this:GetLingShouPos(this.mDestLingShouId), this.mDestLingShouId, state)
end
CLingShouSwitchSkillMgr.m_CheckLingShouNature_CS2LuaHook = function (this) 
    if System.String.IsNullOrEmpty(this.srcLingShouId) or System.String.IsNullOrEmpty(this.destLingShouId) then
        return false
    end
    local data1 = CLingShouMgr.Inst:GetLingShouOverview(this.srcLingShouId)
    local data2 = CLingShouMgr.Inst:GetLingShouOverview(this.destLingShouId)
    if data1 == nil or data2 == nil then
        return false
    end

    if data1.nature == data2.nature then
        return true
    end

    return false
end
