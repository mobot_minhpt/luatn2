local DelegateFactory = import "DelegateFactory"
local CScheduleMgr    = import "L10.Game.CScheduleMgr"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"
local CTaskMgr        = import "L10.Game.CTaskMgr"
local Ease            = import "DG.Tweening.Ease"

LuaXinBaiPage = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaXinBaiPage, "taskProgress")
RegistClassMember(LuaXinBaiPage, "taskProgressSlider")
RegistClassMember(LuaXinBaiPage, "taskProgressScrollView")
RegistClassMember(LuaXinBaiPage, "taskProgressGrid")
RegistClassMember(LuaXinBaiPage, "taskProgressTip")
RegistClassMember(LuaXinBaiPage, "taskTemplate")
RegistClassMember(LuaXinBaiPage, "grid")
RegistClassMember(LuaXinBaiPage, "smjsItem")
RegistClassMember(LuaXinBaiPage, "smjsTexture")
RegistClassMember(LuaXinBaiPage, "tableView")
RegistClassMember(LuaXinBaiPage, "region")
RegistClassMember(LuaXinBaiPage, "content")
RegistClassMember(LuaXinBaiPage, "gradient")
RegistClassMember(LuaXinBaiPage, "arrow")

RegistClassMember(LuaXinBaiPage, "firstTaskWidth")
RegistClassMember(LuaXinBaiPage, "needPlayTween")
RegistClassMember(LuaXinBaiPage, "dataList")
RegistClassMember(LuaXinBaiPage, "smjsActivityId")

function LuaXinBaiPage:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
    self:InitActive()
    self:InitTableView()
    self.smjsActivityId = XinBaiLianDong_Setting.GetData().XinBaiProgress_ShuiManJinShanActivityId
    self.needPlayTween = true

    self:UpdateTaskProgress(true)
    self:PlayTween()
end

-- 初始化UI组件
function LuaXinBaiPage:InitUIComponents()
    self.taskProgress = self.transform:Find("TaskProgress").gameObject
    self.taskProgressScrollView = self.transform:Find("TaskProgress/ScrollView"):GetComponent(typeof(UIScrollView))
    self.taskProgressSlider = self.transform:Find("TaskProgress/ScrollView/Slider"):GetComponent(typeof(UISlider))
    self.taskProgressGrid = self.transform:Find("TaskProgress/ScrollView/Grid"):GetComponent(typeof(UIGrid))
    self.taskProgressTip = self.transform:Find("TaskProgress/Tip"):GetComponent(typeof(UILabel))
    self.taskTemplate = self.transform:Find("TaskProgress/TaskTemplate").gameObject

    self.gradient = self.transform:Find("TaskProgress/Panel/GradientSprite"):GetComponent(typeof(UIWidget))
    self.arrow = self.transform:Find("TaskProgress/Panel/GradientSprite/ArrowSprite").gameObject

    self.grid = self.transform:Find("Content/Grid"):GetComponent(typeof(UIGrid))
    self.smjsItem = self.grid.transform:Find("ShuiManJinShanItem")
    self.smjsTexture = self.smjsItem:Find("Texture"):GetComponent(typeof(UITexture))
    self.tableView = self.grid.transform:Find("Offset/Grid"):GetComponent(typeof(QnTableView))
    self.region = self.transform:Find("Region"):GetComponent(typeof(UIWidget))
    self.content = self.transform:Find("Content")
end

function LuaXinBaiPage:InitEventListener()
    local tipButton = self.transform:Find("TaskProgress/Tip/TipButton").gameObject

    UIEventListener.Get(tipButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnTipButtonClick()
    end)
end

function LuaXinBaiPage:InitActive()
    self.smjsItem.gameObject:SetActive(false)
    self.taskTemplate:SetActive(false)
    self.taskProgress:SetActive(false)
end

function LuaXinBaiPage:InitTableView()
    self.tableView.m_DataSource = DefaultTableViewDataSource.Create(function()
            return #self.dataList
        end, function(item, index)
            self:InitItem(item, self.dataList[index + 1])
    end)
end

-- 加载图标
function LuaXinBaiPage:InitTaskProgress()
    Extensions.RemoveAllChildren(self.taskProgressGrid.transform)
    local totalCount = XinBaiLianDong_Progress.GetDataCount()

    for i = 1, totalCount do
        local child = NGUITools.AddChild(self.taskProgressGrid.gameObject, self.taskTemplate)
        child:SetActive(true)

        local icon = child.transform:Find("Tween/Normal/Icon"):GetComponent(typeof(CUITexture))
        icon:LoadMaterial(XinBaiLianDong_Progress.GetData(i).Icon)

        UIEventListener.Get(child.transform:Find("Tween/Normal").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnTaskIconClick(i)
        end)
    end
    self.firstTaskWidth = 54
    local totalWidth = self.taskProgressGrid.cellWidth * (totalCount - 1) + self.firstTaskWidth
    self.taskProgressSlider.foregroundWidget.width = totalWidth
    self.taskProgressSlider.backgroundWidget.width = totalWidth

    self.taskProgressGrid:Reposition()
end

-- 播放动效
function LuaXinBaiPage:PlayTween()
    if not self.needPlayTween then return end
    self.needPlayTween = false

    local totalCount = self.taskProgressGrid.transform.childCount
    local yDiff = 75
    local dutation = 0.8

    for i = 1, totalCount do
        local widget = self.taskProgressGrid.transform:GetChild(i - 1):Find("Tween"):GetComponent(typeof(UIWidget))
        LuaTweenUtils.TweenAlpha(widget, 0, 1, dutation)
        local y = widget.transform.localPosition.y
        LuaUtils.SetLocalPositionY(widget.transform, y - yDiff)
        LuaTweenUtils.SetEase(LuaTweenUtils.TweenPositionY(widget.transform, y, dutation), Ease.OutExpo)
    end
end

function LuaXinBaiPage:OnEnable()
    g_ScriptEvent:AddListener("XinBaiProgressQueryResult", self, "OnXinBaiProgressQueryResult")
    g_ScriptEvent:AddListener("XinBaiProgressQueryRewardResult", self, "OnXinBaiProgressQueryRewardResult")
end

function LuaXinBaiPage:OnDisable()
    g_ScriptEvent:RemoveListener("XinBaiProgressQueryResult", self, "OnXinBaiProgressQueryResult")
    g_ScriptEvent:RemoveListener("XinBaiProgressQueryRewardResult", self, "OnXinBaiProgressQueryRewardResult")
end

function LuaXinBaiPage:OnXinBaiProgressQueryResult()
    self:UpdateTaskProgress()
    self:UpdateShuiManJinShan()
end

function LuaXinBaiPage:OnXinBaiProgressQueryRewardResult()
    self:UpdateTaskProgress()
end

function LuaXinBaiPage:Init()
    self:InitTaskList()
    self:UpdateShuiManJinShan()

    Gac2Gas.XinBaiProgress_Query()
    Gac2Gas.XinBaiProgress_QueryReward()
end

function LuaXinBaiPage:InitTaskList()
    local list = {}
    list = CLuaScheduleMgr.m_TableTypeList[XinBaiLianDong_Setting.GetData().XinBaiProgress_TabType]

    self.smjsItem.gameObject:SetActive(false)
    self.dataList = {}
    for i, info in ipairs(list) do
        if info.activityId == self.smjsActivityId then -- 水漫金山有没有开
            self.smjsItem.gameObject:SetActive(true)
            self:InitItem(self.smjsItem, info)
        else
            table.insert(self.dataList, info)
        end
    end

    self.tableView:ReloadData(true, false)
    self.grid:Reposition()
end

function LuaXinBaiPage:InitItem(item, info)
    local trans = item.transform

    local schedule = Task_Schedule.GetData(info.activityId)
    if not schedule then return end

    UIEventListener.Get(trans.gameObject).onClick = DelegateFactory.VoidDelegate(function(p)
        CLuaScheduleMgr.selectedScheduleInfo = info
        CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
    end)

    -- 次数
    local countLabel = trans:Find("Content/CountLabel"):GetComponent(typeof(UILabel))
    local finishedTimes = info.FinishedTimes -- 每日完成次数
    local totalTimes = CLuaScheduleMgr.GetDailyTimes(schedule) -- 每日次数上限

    if totalTimes == 0 then
        countLabel.enabled = false
    else
        countLabel.enabled = true
        countLabel.text =SafeStringFormat3(LocalString.GetString("次数 %d/%d"), finishedTimes, totalTimes)
    end

    self:InitButton(trans, info, schedule)

    -- 活力
    local huoLiLabel = trans:Find("Content/HuoLiLabel"):GetComponent(typeof(UILabel))
    if not CLuaScheduleMgr.NeedShowHuoli(schedule) then
        huoLiLabel.text = nil
    else
        local curHuoli = CLuaScheduleMgr.GetHuoli(schedule.HuoLi)
        local huoli = math.min(totalTimes, finishedTimes) * curHuoli
        huoLiLabel.text = SafeStringFormat3(LocalString.GetString("活力 %d/%d"), huoli, CLuaScheduleMgr.GetDailyTimes(schedule) * curHuoli)
    end

    if info.activityId ~= self.smjsActivityId then
        local nameLabel = trans:Find("Content/NameLabel"):GetComponent(typeof(UILabel))
        nameLabel.text = schedule.TaskName

        -- 图标
        local icon = trans:Find("Icon"):GetComponent(typeof(CUITexture))
        icon.material = nil
        if schedule.Reward and schedule.Reward.Length > 0 then
            local id = schedule.Reward[0]
            local template = Item_Item.GetData(id)
            if template then
                icon:LoadMaterial(template.Icon)
            else
                local equipTemplate = EquipmentTemplate_Equip.GetData(id)
                if equipTemplate then
                    icon:LoadMaterial(equipTemplate.Icon)
                end
            end
        end
    end
end

-- 按键
function LuaXinBaiPage:InitButton(trans, info, schedule)
    local finishedTimes = info.FinishedTimes -- 每日完成次数
    local totalTimes = CLuaScheduleMgr.GetDailyTimes(schedule) -- 每日次数上限
    local restrictTimes = CScheduleMgr.GetRestrictTimes(info.activityId, info.taskId) -- 最大次数

    -- 按键文字显示
    local buttonLabel = trans:Find("Content/Btn/Label"):GetComponent(typeof(UILabel))
    if schedule.ButtonType == 0 then
        buttonLabel.text = LocalString.GetString("参加")
    elseif schedule.ButtonType == 1 then
        buttonLabel.text = LocalString.GetString("领取")
    elseif schedule.ButtonType == 2 then
        buttonLabel.text = LocalString.GetString("查看")
    end

    -- 红点提示
    local alertSprite = trans:Find("Content/Alert"):GetComponent(typeof(UISprite))
    --alertSprite.enabled = false
    --if CScheduleMgr.Inst:GetAlertState(info.activityId) == CScheduleMgr.EnumAlertState.Show
    --    and not CTaskMgr.Inst:IsInProgress(info.taskId) and finishedTimes == 0 then
    --    alertSprite.enabled = true
    --end
    alertSprite.enabled = LuaActivityRedDotMgr:IsRedDot(info.activityId, LuaEnumRedDotType.Schedule)

    local contentNode = trans:Find("Content").gameObject
    local button = trans:Find("Content/Btn").gameObject
    local levelRequireLabel = trans:Find("Content/LevelRequireLabel"):GetComponent(typeof(UILabel))
    local finishSprite = trans:Find("Content/FinishSprite"):GetComponent(typeof(UISprite))

    CUICommonDef.SetActive(contentNode, true, true)
    button:SetActive(true)
    levelRequireLabel.gameObject:SetActive(false)
    finishSprite.gameObject:SetActive(false)

    if totalTimes > 0 and finishedTimes >= restrictTimes then
        CUICommonDef.SetActive(contentNode, false, true)
        finishSprite.gameObject:SetActive(true)
        button:SetActive(false)
        return
    end

    local taskData = Task_Task.GetData(info.taskId)
    if not CLuaScheduleMgr.IsLevelEnough(taskData) then
        levelRequireLabel.text = SafeStringFormat3(LocalString.GetString("%d级开启"), taskData.Level)
        levelRequireLabel.gameObject:SetActive(true)
        button:SetActive(false)
        return
    end

    if CLuaScheduleMgr.IsInProgress(info.taskId) then
        levelRequireLabel.text = LocalString.GetString("已接")
        levelRequireLabel.gameObject:SetActive(true)
        button:SetActive(false)
        return
    end

    if schedule.NoJoinButton > 0 then
        levelRequireLabel.gameObject:SetActive(false)
        button:SetActive(false)
        return
    end

    if info.lastingTime > 0 then -- 限时
        local now = CServerTimeMgr.Inst:GetZone8Time()
        local nowTime = (now.Hour * 60 + now.Minute) * 60 + now.Second
        local startTime = (info.hour * 60 + info.minute) * 60 + CScheduleMgr.DelayTriggerSecond
        local endTime = (info.hour * 60 + info.minute) * 60 + info.lastingTime * 60
        if nowTime > startTime and nowTime <= endTime then
            levelRequireLabel.gameObject:SetActive(false)
        elseif nowTime < startTime then -- 没开始
            levelRequireLabel.gameObject:SetActive(true)
            levelRequireLabel.text = SafeStringFormat3("%02d:%02d", info.hour, info.minute)
            button:SetActive(false)
        elseif nowTime > endTime then -- 已结束
            CUICommonDef.SetActive(contentNode, false, true)
            levelRequireLabel.gameObject:SetActive(true)
            levelRequireLabel.text = LocalString.GetString("已结束")
            button:SetActive(false)
        end
    end

    UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickCanJiaButton(trans, info)
    end)
end

-- 进度条是否显示
function LuaXinBaiPage:UpdateTaskProgressAnchor()
    local panel = self.content:GetComponent(typeof(UIPanel))
    local scrollView = self.content:GetComponent(typeof(UIScrollView))
    if LuaXinBaiLianDongMgr.ProgressInfo.isOpen then
        if not self.taskProgress.active then
            self:InitTaskProgress()
        end

        self.taskProgress:SetActive(true)
        self.region.height = 466
        panel:ResetAndUpdateAnchors()
        scrollView:ResetPosition()
    else
        self.taskProgress:SetActive(false)
        self.region.height = 800
        panel:ResetAndUpdateAnchors()
        scrollView:ResetPosition()
    end
end

-- 更新解锁进度显示
function LuaXinBaiPage:UpdateTaskProgress(needAutoScroll)
    self:UpdateTaskProgressAnchor()
    if not LuaXinBaiLianDongMgr.ProgressInfo.isOpen then return end

    local tbl = LuaXinBaiLianDongMgr.ProgressInfo.progressTbl or {}
    local flagTbl = LuaXinBaiLianDongMgr.ProgressInfo.flagTbl

    local progressLength = 0
    local interval = self.taskProgressGrid.cellWidth

    local beforeProgressIsReached = true
    local curTask = 0

    local canReceiveNode = nil
    local totalCount = self.taskProgressGrid.transform.childCount
    for i = 1, totalCount do
        local child = self.taskProgressGrid.transform:GetChild(i - 1)

        local icon = child:Find("Tween/Normal/Icon")
        local fx = child:Find("Tween/Fx"):GetComponent(typeof(CUIFx))
        local num = child:Find("Tween/Num"):GetComponent(typeof(UILabel))
        local redDot = child:Find("Tween/Normal/RedDot").gameObject
        local mark = child:Find("Tween/Normal/Mark").gameObject

        local progress = tbl[i] or 0
        local maxProgress = XinBaiLianDong_Progress.GetData(i).MaxProgress

        if maxProgress > 0 then
            local percent = math.min(maxProgress, progress) / maxProgress
            num.text = SafeStringFormat3("%d%%", math.floor(percent * 100))
            progressLength = progressLength + interval * percent
            num.gameObject:SetActive(true)
        else
            num.gameObject:SetActive(false)
        end

        local curTaskIsFinished = self:IsTasksFinished(i)
        local isFirstTaskAccepted = self:IsFirstTaskAccepted(i)
        local isReward = (flagTbl and flagTbl[i]) and true or false

        fx:DestroyFx()

        if beforeProgressIsReached then
            local isGradeOk = self:CheckTaskAcceptGrade(self:GetFirstTaskId(i))
            local isTaskStatusOpen = self:CheckTaskStatus(self:GetFirstTaskId(i))
            local canAccept = not isFirstTaskAccepted and self:IsPreTaskFinished(i) and isGradeOk and isTaskStatusOpen
            redDot:SetActive(canAccept)
            if needAutoScroll and canAccept then canReceiveNode = child end
        else
            redDot:SetActive(false)
        end
        if needAutoScroll and not canReceiveNode and isFirstTaskAccepted and not curTaskIsFinished then canReceiveNode = child end

        if progress < maxProgress then
            if beforeProgressIsReached then
                curTask = i
            end
            beforeProgressIsReached = false
        else
            beforeProgressIsReached = true

            local canReward = not isReward and curTaskIsFinished
            if canReward then fx:LoadFx(g_UIFxPaths.XinBaiProgressFxPath) end
        end

        LuaUtils.SetLocalPositionZ(icon, isFirstTaskAccepted and 0 or -1)
        mark:SetActive(isReward)
    end

    if needAutoScroll and canReceiveNode then
        local item, scrollView = canReceiveNode, self.taskProgressScrollView
        local bound = NGUIMath.CalculateAbsoluteWidgetBounds(item)
        local localmax = scrollView.transform:InverseTransformPoint(bound.max)

        local bound2 = NGUIMath.CalculateRelativeWidgetBounds(scrollView.transform, scrollView.transform)
        local contentMin = bound2.min
        local contentMax = bound2.max

        local clipLength = scrollView.panel.finalClipRegion.z - 2 * scrollView.panel.clipSoftness.x
        local delta = (localmax.x - contentMin.x - clipLength) / (contentMax.x - contentMin.x - clipLength)
        delta = Mathf.Clamp01(delta)
        scrollView:SetDragAmount(delta, 0, false)
    end

    local width = self.taskProgressSlider.foregroundWidget.width
    self.taskProgressSlider.value = math.min((progressLength + self.firstTaskWidth) / width, 1)

    if curTask == 0 then
        self.taskProgressTip.text = ""
        return
    end

    local taskIds = {}
    for taskId in string.gmatch(XinBaiLianDong_Progress.GetData(curTask).TaskIds, "(%d+)") do
        table.insert(taskIds, tonumber(taskId))
    end
    if #taskIds == 0 then return end
    local lastTask = taskIds[#taskIds]
    local taskName = Task_Task.GetData(lastTask).Display
    self.taskProgressTip.text = SafeStringFormat3(LocalString.GetString("完成[FFFF00]任务%s[-]可为当前节点增加助力"), taskName)
end

-- 是否完成任务
function LuaXinBaiPage:IsTasksFinished(progressId)
    for taskId in string.gmatch(XinBaiLianDong_Progress.GetData(progressId).TaskIds, "(%d+)") do
        if not CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(tonumber(taskId)) then
            return false
        end
    end

    return true
end

-- 前置任务是否完成
function LuaXinBaiPage:IsPreTaskFinished(progressId)
    local preCheckTaskId = XinBaiLianDong_Progress.GetData(progressId).PreCheckTaskId
    if preCheckTaskId > 0 then
        if not CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(preCheckTaskId) then
            return false
        end
    end

    return true
end

-- 获取第一个任务
function LuaXinBaiPage:GetFirstTaskId(progressId)
    for taskId in string.gmatch(XinBaiLianDong_Progress.GetData(progressId).TaskIds, "(%d+)") do
        return tonumber(taskId)
    end
    return 0
end

-- 第一个任务正在进行中或者已完成
function LuaXinBaiPage:IsFirstTaskAccepted(progressId)
    local taskProp = CClientMainPlayer.Inst.TaskProp
    local taskId = self:GetFirstTaskId(progressId)
    if CommonDefs.ListContains_LuaCall(taskProp.CurrentTaskList, taskId) or taskProp:IsMainPlayerTaskFinished(taskId) then
        return true
    else
        return false
    end
end

-- 更新水漫金山显示
function LuaXinBaiPage:UpdateShuiManJinShan()
    local parent = self.smjsItem:Find("Num")
    for i = 1, 9 do
        parent:GetChild(i - 1).gameObject:SetActive(false)
    end

    local tbl = LuaXinBaiLianDongMgr.ProgressInfo.smjsTbl
    if not tbl then return end
    local leftNum, rightNum = tbl[1001], tbl[1002]
    if not leftNum or not rightNum then return end

    local totalNum = leftNum + rightNum
    local leftPercent = math.floor(leftNum / totalNum * 100 + 0.5)
    if leftPercent <= 0 and leftNum > 0 then
        leftPercent = 1
    elseif leftPercent >= 100 and rightNum > 0 then
        leftPercent = 99
    end
    local rightPercent = 100 - leftPercent

    local num = math.ceil(rightPercent / 10 - 0.5)
    if num < 1 then num = 1 end
    if num > 9 then num = 9 end
    local rect = self.smjsTexture.uvRect
    rect.y = 0.11111 * (num - 1)
    self.smjsTexture.uvRect = rect

    local child = parent:Find(tostring(num))
    child.gameObject:SetActive(true)

    local left = child:Find("Left"):GetComponent(typeof(UILabel))
    local right = child:Find("Right"):GetComponent(typeof(UILabel))
    left.text = SafeStringFormat3(LocalString.GetString("%d%%"), leftPercent)
    right.text = SafeStringFormat3(LocalString.GetString("%d%%"), rightPercent)
end

-- 更新箭头显示
function LuaXinBaiPage:Update()
    local isEnd, isStart = self.taskProgressScrollView:GetReachState()
    self.gradient.alpha = isEnd and 0 or 1
    self.arrow:SetActive(not isEnd)
end

-- 是否达到任务接受等级
function LuaXinBaiPage:CheckTaskAcceptGrade(taskId)
    local data = Task_Task.GetData(taskId)

    local minGrade = CClientMainPlayer.Inst.HasFeiSheng and data.GradeCheckFS1[0] or data.GradeCheck[0]
    if CClientMainPlayer.Inst.Level < minGrade then
        return false, minGrade
    else
        return true
    end
end

-- 检查任务状态
function LuaXinBaiPage:CheckTaskStatus(taskId)
    local statusTbl = LuaXinBaiLianDongMgr.ProgressInfo.statusTbl
    if not statusTbl or not statusTbl[taskId] then return false end

    return statusTbl[taskId] == 0
end

--@region UIEvent

function LuaXinBaiPage:OnClickCanJiaButton(trans, info)
    local activityId = info.activityId
    --跨服
    if not CLuaScheduleMgr.m_CrossServerActivityIds[activityId] and CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsCrossServerPlayer then
        g_MessageMgr:ShowMessage("CROSS_SERVER_NOT_USE_THE_FEATURE")
        return
    end
    local schedule = Task_Schedule.GetData(activityId)
    if not schedule then return end

    local isXianShi = schedule.TabType == 2

    --限时活动要检查
    if isXianShi and CLuaScheduleMgr.m_TaskCheck and not CTaskMgr.Inst:CheckTaskTime(info.taskId) then
        g_MessageMgr:ShowMessage("GAMEPLAY_NOT_OPEN")
        return
    end

    CLuaScheduleMgr.TrackSchedule(info)

    if trans then
        local alertSprite = trans:Find("Content/Alert"):GetComponent(typeof(UISprite))
        alertSprite.enabled = false
    end

    CScheduleMgr.Inst:SetAlertState(activityId, CScheduleMgr.EnumAlertState.Clicked, false)
    LuaActivityRedDotMgr:OnRedDotClicked(activityId, LuaEnumRedDotType.Schedule)
    EventManager.Broadcast(EnumEventType.UpdateScheduleAlert)
    CUIManager.CloseUI(CLuaUIResources.ScheduleWnd)
end

function LuaXinBaiPage:OnTaskIconClick(id)
    local tbl = LuaXinBaiLianDongMgr.ProgressInfo.progressTbl or {}

    if id > 1 then
        local progress = tbl[id - 1] or 0
        local maxProgress = XinBaiLianDong_Progress.GetData(id - 1).MaxProgress

        if progress < maxProgress then
            g_MessageMgr:ShowMessage("XINBAIPROGRESS_PROGRESS_IS_LOCKED")
            return
        end
    end

    self.taskProgressGrid.transform:GetChild(id - 1):Find("Tween/Normal/RedDot").gameObject:SetActive(false)
    local isFirstTaskAccepted = self:IsFirstTaskAccepted(id)
    if not isFirstTaskAccepted then
        local preTaskIsFinished = self:IsPreTaskFinished(id)
        if preTaskIsFinished then
            local taskId = self:GetFirstTaskId(id)
            if taskId > 0 then
                local isGradeOk, minGrade = self:CheckTaskAcceptGrade(taskId)
                local isTaskStatusOpen = self:CheckTaskStatus(taskId)
                if not isTaskStatusOpen then
                    g_MessageMgr:ShowMessage("XINBAIPROGRESS_TASK_IS_NOT_OPEN")
                elseif not isGradeOk then
                    g_MessageMgr:ShowMessage("XINBAIPROGRESS_LEVEL_NOT_ENOUGH", minGrade)
                else
                    Gac2Gas.RequestAutoAcceptTask(taskId)
                end
            end
        else
            g_MessageMgr:ShowMessage(id == 1 and "XINBAIPROGRESS_FIRST_PRE_TASK_NOT_FINISHED" or "XINBAIPROGRESS_BEFORE_TASK_IS_NOT_FINISHED")
        end
        return
    end

    local flagTbl = LuaXinBaiLianDongMgr.ProgressInfo.flagTbl
    local curTaskIsFinished = self:IsTasksFinished(id)
    local isReward = (flagTbl and flagTbl[id]) and true or false

    if isReward then
        g_MessageMgr:ShowMessage("XINBAIPROGRESS_HAVE_RECEIVE_AWARD")
        return
    end

    if not curTaskIsFinished then
        g_MessageMgr:ShowMessage("XINBAIPROGRESS_TASK_IS_GOING")
        return
    end

    Gac2Gas.XinBaiProgress_TryReward(id)
end

function LuaXinBaiPage:OnTipButtonClick()
    g_MessageMgr:ShowMessage("XINBAIPROGRESS_TIP")
end

--@endregion UIEvent
