local CTooltip = import "L10.UI.CTooltip"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CModelAutoRotate = import "L10.UI.CModelAutoRotate"
local Screen = import "UnityEngine.Screen"
local CRenderObject  = import "L10.Engine.CRenderObject"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local NGUITools = import "NGUITools"
local LayerDefine = import "L10.Engine.LayerDefine"
local ParticleSystem = import "UnityEngine.ParticleSystem"
local Renderer = import "UnityEngine.Renderer"
local MaterialPropertyBlock = import "UnityEngine.MaterialPropertyBlock"
local QualitySettings = import "UnityEngine.QualitySettings"

CLuaOtherSoulCoreWnd = class()

RegistClassMember(CLuaOtherSoulCoreWnd, "m_ModelTex")
RegistClassMember(CLuaOtherSoulCoreWnd, "m_SoulCoreName")
RegistClassMember(CLuaOtherSoulCoreWnd, "m_ClassTex")
RegistClassMember(CLuaOtherSoulCoreWnd, "m_LevelLab")
RegistClassMember(CLuaOtherSoulCoreWnd, "m_LevelSufLab")
RegistClassMember(CLuaOtherSoulCoreWnd, "m_UpLab")
RegistClassMember(CLuaOtherSoulCoreWnd, "m_DownLab")
RegistClassMember(CLuaOtherSoulCoreWnd, "m_EfficientLab")
RegistClassMember(CLuaOtherSoulCoreWnd, "m_PowerLab")
RegistClassMember(CLuaOtherSoulCoreWnd, "m_NorColor")
RegistClassMember(CLuaOtherSoulCoreWnd, "m_FixColor")
RegistClassMember(CLuaOtherSoulCoreWnd, "m_AutoRotate")
RegistClassMember(CLuaOtherSoulCoreWnd, "m_ModelTextureName")
RegistClassMember(CLuaOtherSoulCoreWnd, "m_AutoRotSpeed")
RegistClassMember(CLuaOtherSoulCoreWnd, "m_Rotation")
RegistClassMember(CLuaOtherSoulCoreWnd, "m_ClassToName")

function CLuaOtherSoulCoreWnd:Awake()
    self:InitComponents()
    self.m_NorColor     = NGUIText.ParseColor24("FFFFFF", 0)
    self.m_FixColor     = NGUIText.ParseColor24("FF790B", 0)

    self.m_ModelTextureName = "__OtherSoulCoreWndModelCamera__"
    self.m_AutoRotSpeed = SoulCore_Settings.GetData().SoulCoreCarveRotSpeed or 12
    self.m_Rotation = 180
end

function CLuaOtherSoulCoreWnd:InitComponents()
    self.m_ModelTex     = self.transform:Find("Anchor/Left/SoulCore/ModelTexture"):GetComponent(typeof(UITexture))
    self.m_SoulCoreName = self.transform:Find("Anchor/Left/SoulCoreName/Label"):GetComponent(typeof(UILabel))

    self.m_ClassTex     = self.transform:Find("Anchor/Right/WuXing"):GetComponent(typeof(CUITexture))
    self.m_LevelLab     = self.transform:Find("Anchor/Right/Level/Label"):GetComponent(typeof(UILabel))
    self.m_LevelSufLab  = self.transform:Find("Anchor/Right/Level/Suffix"):GetComponent(typeof(UILabel))

    self.m_UpLab        = self.transform:Find("Anchor/Right/Grid/Up/Label"):GetComponent(typeof(UILabel))
    self.m_DownLab      = self.transform:Find("Anchor/Right/Grid/Down/Label"):GetComponent(typeof(UILabel))
    self.m_EfficientLab = self.transform:Find("Anchor/Right/Grid/Efficient/Label"):GetComponent(typeof(UILabel))
    self.m_PowerLab     = self.transform:Find("Anchor/Right/Grid/Power/Label"):GetComponent(typeof(UILabel))

    self.m_AutoRotate   = self.transform:Find("Anchor/Left/SoulCore/ModelTexture"):GetComponent(typeof(CModelAutoRotate))
end

function CLuaOtherSoulCoreWnd:Init()
    self:InitModelTexCallback()

    self.m_ClassToName = {}

    for i=1, SoulCore_WuXing.GetDataCount() do
        self.m_ClassToName[i] = SoulCore_WuXing.GetData(i).Name
    end
    
    self:InitTip()

    self.m_SoulCoreName.text = CPlayerInfoMgr.PlayerInfo.name .. LocalString.GetString("灵核")

    local soulCoreProp = CPlayerInfoMgr.PlayerInfo.skillProp.SoulCore
    local wuXing = CPlayerInfoMgr.PlayerInfo.mingGe
    local fanShenLevel = CPlayerInfoMgr.PlayerInfo.feishengLevel
    local xianShenLevel = CPlayerInfoMgr.PlayerInfo.xianshenLevel
    local appearData = {DisplayCoreId = soulCoreProp.DisplayCoreId,
        DisplayFxId = soulCoreProp.DisplayFxId,
        DisplayPalletId = soulCoreProp.DisplayPalletId,
        DisplayCoreParticles = soulCoreProp.DisplayCoreParticles,
        DisplayCoreEffect = soulCoreProp.DisplayCoreEffect,
        Level = soulCoreProp.Level}
    
    self:LoadModel(appearData, wuXing)
    self:LoadCurrentProperty(wuXing, soulCoreProp.Level, fanShenLevel, xianShenLevel)
    self:LoadPower(CPlayerInfoMgr.PlayerInfo.skillProp.AntiProfessionSkill)
end

function CLuaOtherSoulCoreWnd:OnEnable()
    if LuaShopMallFashionPreviewMgr.enableMultiLight then
        QualitySettings.pixelLightCount = 3
    end
end

function CLuaOtherSoulCoreWnd:OnDisable()
    if LuaShopMallFashionPreviewMgr.enableMultiLight then
        QualitySettings.pixelLightCount = 0
    end
end


function CLuaOtherSoulCoreWnd:InitModelTexCallback()
    CommonDefs.AddOnDragListener(self.m_ModelTex.gameObject, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
		self:OnScreenDrag(go, delta)
    end), false)
    
    UIEventListener.Get(self.m_ModelTex.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(g)
        self.m_IsDraging = true
        self:CheckIfNeedRotate()
    end)

    UIEventListener.Get(self.m_ModelTex.gameObject).onDragEnd = DelegateFactory.VoidDelegate(function(g)
        self.m_IsDraging = false
        self:CheckIfNeedRotate()
    end)
end

function CLuaOtherSoulCoreWnd:OnScreenDrag(go, delta)
    local rot = -delta.x / Screen.width * 360
	self.m_AutoRotate:Rotate(rot)
end

function CLuaOtherSoulCoreWnd:OnClick(go)
    
end

function CLuaOtherSoulCoreWnd:CheckIfNeedRotate()
    local IsNeedRotate =  not self.m_IsDraging
    self.m_AutoRotate.IsRotating = IsNeedRotate
end

function CLuaOtherSoulCoreWnd:InitTip()
    local titles = {}
    local m_UpTitleLab        = self.transform:Find("Anchor/Right/Grid/Up/Title"):GetComponent(typeof(UILabel))
    local m_DownTitleLab      = self.transform:Find("Anchor/Right/Grid/Down/Title"):GetComponent(typeof(UILabel))
    local m_EfficientTitleLab = self.transform:Find("Anchor/Right/Grid/Efficient/Title"):GetComponent(typeof(UILabel))

    table.insert(titles, {label = m_UpTitleLab, tip = "SoulCore_Tips_Up_Message"})
    table.insert(titles, {label = m_DownTitleLab, tip = "SoulCore_Tips_Down_Message"})
    table.insert(titles, {label = m_EfficientTitleLab, tip = "SoulCore_Tips_BaseRate_Message"})

    for i = 1, #titles do
        local title = titles[i]
        UIEventListener.Get(title.label.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
            CTooltip.Show(g_MessageMgr:FormatMessage(title.tip), title.label.transform, CTooltip.AlignType.Top)
        end)
    end
end

function CLuaOtherSoulCoreWnd:LoadModel(appearData, wuXing)
    self.m_ModelTex.mainTexture = CUIManager.CreateModelTexture(self.m_ModelTextureName, LuaDefaultModelTextureLoader.Create(function (ro)
        LuaZongMenMgr:CreateSoulCoreGameObject(ro.gameObject, appearData, wuXing)
        self.m_AutoRotate:Init(self.m_ModelTextureName, 180, self.m_AutoRotSpeed)
        self:InitShadowCameraAndLight()
    end) , 180, 0, -0.32, 4.66, false, true, 1)
end

function CLuaOtherSoulCoreWnd:LoadCurrentProperty(class, oriLevel, fanShenLevel, xianShenLevel)
    --五行属性
    self:LoadWuXingProp(class)

    --当前属性
    self:LoadCurLvProp(oriLevel, fanShenLevel, xianShenLevel)
end

function CLuaOtherSoulCoreWnd:LoadPower(antiProfessionSkill)
    local calLevelTable = {}
    CommonDefs.DictIterate(antiProfessionSkill, DelegateFactory.Action_object_object(function (idx, AntiProfessionSkillObject)
        table.insert(calLevelTable, {idx, AntiProfessionSkillObject.CurLevel})
    end))

    self.m_PowerLab.text = SafeStringFormat3("%.1f", LuaZongMenMgr:CalAntiProfessionLevel(calLevelTable))
end

function CLuaOtherSoulCoreWnd:LoadWuXingProp(class)
    local classData = SoulCore_WuXing.GetData(class)

    if classData ~= nil then
        for i = 0, classData.Up.Length - 1 do
            if i == 0 then
                self.m_UpLab.text = SafeStringFormat3("%s%s", self.m_ClassToName[classData.Up[0]], self:FormatNumber(classData.UpValue[0]))
            else
                self.m_UpLab.text = self.m_UpLab.text .. LocalString.GetString("、") .. SafeStringFormat3("%s%s", self.m_ClassToName[classData.Up[i]], self:FormatNumber(classData.UpValue[i]))
            end
        end
        
        for i = 0, classData.Down.Length - 1 do
            if i == 0 then
                self.m_DownLab.text = SafeStringFormat3("%s%s", self.m_ClassToName[classData.Down[0]], self:FormatNumber(classData.DownValue[0]))
            else
                self.m_DownLab.text = self.m_DownLab.text .. LocalString.GetString("、") .. SafeStringFormat3("%s%s", self.m_ClassToName[classData.Down[i]], self:FormatNumber(classData.DownValue[i]))
            end
        end

        self.m_ClassTex:LoadMaterial(LuaZongMenMgr:GetMingGeMatPath(class))
    else
        self.m_UpLab.text = "-"
        self.m_DownLab.text = "-"
    end
end

function CLuaOtherSoulCoreWnd:LoadCurLvProp(oriLevel, fanShenLevel, xianShenLevel)
    local hasFeiSheng = fanShenLevel ~= 0 and xianShenLevel ~= 0
    local curLevel = hasFeiSheng and LuaZongMenMgr:GetAdjustSoulCoreLevel(fanShenLevel, xianShenLevel, oriLevel) or oriLevel 
    local curData = SoulCore_SoulCore.GetData(curLevel)
    if curData ~= nil then
        self.m_LevelLab.text = curLevel
        self.m_EfficientLab.text = SafeStringFormat3(LocalString.GetString("%s灵力/分钟"), self:FormatNumber(curData.BaseRate))
        local showFeiSheng = hasFeiSheng and oriLevel ~= curLevel
        if showFeiSheng then
            self.m_LevelLab.color = self.m_FixColor
            self.m_LevelSufLab.text = SafeStringFormat3("/%d%s", oriLevel, LocalString.GetString("级"))
            self.m_EfficientLab.color = self.m_FixColor
        else
            self.m_LevelLab.color = self.m_NorColor
            self.m_LevelSufLab.text = LocalString.GetString("级")
            self.m_EfficientLab.color = self.m_NorColor
        end
    else
        self.m_LevelLab.text = curLevel
        self.m_EfficientLab.text = "-"
    end
end

function CLuaOtherSoulCoreWnd:FormatNumber(num)
    return tonumber(SafeStringFormat3("%.2f", num))
end

function CLuaOtherSoulCoreWnd:OnDestroy()
    self.m_ModelTex.mainTexture = nil
    CUIManager.DestroyModelTexture(self.m_ModelTextureName)
end

function CLuaOtherSoulCoreWnd:InitShadowCameraAndLight()
    local t = CUIManager.instance.transform:Find(self.m_ModelTextureName)
    if not t then return end
    if t:Find("LightAndFloorAndWall") ~= nil then
        return
    end
    local newRO = CRenderObject.CreateRenderObject(t.gameObject, "LightAndFloorAndWall")
    local fx = CEffectMgr.Inst:AddObjectFX(88801543, newRO ,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero, DelegateFactory.Action_GameObject(function(go)
        NGUITools.SetLayer(newRO.gameObject,LayerDefine.Effect_3D)
        local beijingRenderer = go.transform:Find("Anchor/beijing"):GetComponent(typeof(Renderer))
        local spotLight = go.transform:Find("Anchor/Spotlight")
        local particle = go.transform:Find("Anchor/PretendParticleEffect"):GetComponent(typeof(ParticleSystem))

        LuaUtils.SetLocalPositionY(beijingRenderer.transform, -15)
        local mpb = CreateFromClass(MaterialPropertyBlock)
        beijingRenderer:GetPropertyBlock(mpb)
        mpb:SetColor("_Color", NGUIText.ParseColor24("00030EFF", 0))
        beijingRenderer:SetPropertyBlock(mpb)
        spotLight.gameObject:SetActive(false)
        LuaUtils.SetLocalPosition(particle.transform, 4.04, -7.29, -3.66)
        particle.maxParticles = 15
    end))
    newRO:AddFX("LightAndFloorAndWall", fx, -1)
end
