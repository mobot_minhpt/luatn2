require("3rdParty/ScriptEvent")
require("common/common_include")

local CButton = import "L10.UI.CButton"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UIGrid = import "UIGrid"
local CPackageViewItemResource = import "L10.UI.CPackageViewItemResource"
local Vector3 = import "UnityEngine.Vector3"
local Quaternion = import "UnityEngine.Quaternion"
local CResourceMgr = import "L10.Engine.CResourceMgr"
local CUITexture = import "L10.UI.CUITexture"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Item_Item = import "L10.Game.Item_Item"
local ChiJi_Item = import "L10.Game.ChiJi_Item"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EnumQualityType = import "L10.Game.EnumQualityType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local DefaultItemActionDataSource = import "L10.UI.DefaultItemActionDataSource"

LuaPUBGSelfPackageView = class()

RegistChildComponent(LuaPUBGSelfPackageView, "ClearUpBtn", CButton)
RegistChildComponent(LuaPUBGSelfPackageView, "ArrangeBtn", CButton)
RegistChildComponent(LuaPUBGSelfPackageView, "DiscardBtn", CButton)
RegistChildComponent(LuaPUBGSelfPackageView, "CancelBtn", CButton)

RegistChildComponent(LuaPUBGSelfPackageView, "ItemCell", GameObject)
RegistChildComponent(LuaPUBGSelfPackageView, "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaPUBGSelfPackageView, "Grid", UIGrid)

RegistChildComponent(LuaPUBGSelfPackageView, "NormalBtnRoot", GameObject)
RegistChildComponent(LuaPUBGSelfPackageView, "ClearUpBtnRoot", GameObject)

RegistClassMember(LuaPUBGSelfPackageView, "BAGSIZE")
RegistClassMember(LuaPUBGSelfPackageView, "PackageItemList")
RegistClassMember(LuaPUBGSelfPackageView, "PackageInfos")
RegistClassMember(LuaPUBGSelfPackageView, "IsClearUpState")
RegistClassMember(LuaPUBGSelfPackageView, "WaitingToClearUpList")
RegistClassMember(LuaPUBGSelfPackageView, "SelectedItemIndex")

function LuaPUBGSelfPackageView:Awake()
    self.ItemCell:SetActive(false)
    self.BAGSIZE = 25
    self.PackageItemList = {}
    self.PackageInfos = {}
    self.WaitingToClearUpList = {}
    self.IsClearUpState = false
    self.SelectedItemIndex = -1
    CPackageViewItemResource.Inst.Resource = self.ItemCell
end

function LuaPUBGSelfPackageView:Init()
    self:ShowNormalBtnRoot()
    CommonDefs.AddOnClickListener(self.ClearUpBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnClearUpBtnClicked(go)
    end), false)
    
    CommonDefs.AddOnClickListener(self.ArrangeBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnArrangeBtnClicked(go)
    end), false)
    
    CommonDefs.AddOnClickListener(self.DiscardBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnDiscardBtnClicked(go)
    end), false)
    
    CommonDefs.AddOnClickListener(self.CancelBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnCancelBtnClicked(go)
    end), false)
    
    -- 等确认了存储方案再写
    self:InitPUBGPackage()
end

-- 将packageInfo补全为25个
function LuaPUBGSelfPackageView:ProcessPackageInfos()
    self.PackageInfos = {}

    if not LuaPUBGMgr.m_PackageInfos then LuaPUBGMgr.m_PackageInfos = {} end
    for i = 1, self.BAGSIZE do
        if i <= #LuaPUBGMgr.m_PackageInfos then
            table.insert(self.PackageInfos, LuaPUBGMgr.m_PackageInfos[i])
        else
            table.insert(self.PackageInfos, {itemId = 0, count = 0, enable = 0, id = 0})
        end
    end
end


function LuaPUBGSelfPackageView:InitPUBGPackage()
    self:ProcessPackageInfos()

    Extensions.RemoveAllChildren(self.Grid.transform)
    self.PackageItemList = {}

    for i = 1, self.BAGSIZE do
        local instance = CResourceMgr.Inst:Instantiate(CPackageViewItemResource.Inst, false, 1)
        instance.transform.parent = self.Grid.transform
        instance.transform.localPosition = Vector3.zero
        instance.transform.localRotation = Quaternion.identity
        instance.transform.localScale = Vector3.one

        self:InitPackageItem(instance, self.PackageInfos[i], i)
        instance:SetActive(true)
        
        table.insert(self.PackageItemList, instance)

    end
    self.Grid:Reposition()
    self.ScrollView:ResetPosition()
end

function LuaPUBGSelfPackageView:InitPackageItem(item, info, index)
    if not item then return end

    local IconTexture = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    IconTexture:Clear()

    local LockedSprite = item.transform:Find("LockedSprite").gameObject
    LockedSprite:SetActive(false)

    local CooldownSprite = item.transform:Find("CooldownSprite"):GetComponent(typeof(UISprite))
    CooldownSprite.gameObject:SetActive(false)

    local CannotDiscardSprite = item.transform:Find("CannotDiscardSprite").gameObject
    CannotDiscardSprite:SetActive(false)

    local DisableSprite = item.transform:Find("DisableSprite").gameObject
    DisableSprite:SetActive(false)

    local QualitySprite = item.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
    QualitySprite.spriteName = nil
    
    local ClearupCheckbox = item.transform:Find("ClearupCheckbox"):GetComponent(typeof(QnCheckBox))
    ClearupCheckbox.gameObject:SetActive(false)

    local AmountLabel = item.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
    AmountLabel.text = ""

    local TextLabel = item.transform:Find("TextLabel"):GetComponent(typeof(UILabel))
    TextLabel.text = ""

    self:SetPackageItemSelected(item, false, index)

    if not info then return end

    local chijiItem = ChiJi_Item.GetData(info.itemId)
    if not chijiItem then return end

    local actsource = nil

    local useAction = function ()
        -- 使用
        Gac2Gas.RequestUseChiJiItem(index, info.id)
        CItemInfoMgr.CloseItemInfoWnd()
    end

    local dropAction = function ()
        -- 丢弃
        Gac2Gas.RequestDropChiJiItem(index, info.id)
        CItemInfoMgr.CloseItemInfoWnd()
    end

    local slipAction = function ()
        CLuaNumberInputMgr.ShowNumInputBox(1, info.count, 1, function (count) 
            Gac2Gas.RequestSplitChiJiItem(index, info.id, count)
        end, LocalString.GetString("请输入需要拆分的数量"), -1)
        CItemInfoMgr.CloseItemInfoWnd()
    end

    if LuaPUBGMgr.IsEquip(info.itemId) then
        local equip = EquipmentTemplate_Equip.GetData(chijiItem.ItemId)
        if equip then
            IconTexture:LoadMaterial(equip.Icon)
            AmountLabel.text = tostring(info.count)
            QualitySprite.spriteName = CUICommonDef.GetItemCellBorder(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), equip.Color))
        end
        local t_action = {}
        t_action[1] = dropAction
        local t_name = {}
        t_name[1] = LocalString.GetString("丢弃")
        actsource = DefaultItemActionDataSource.Create(1, t_action,t_name)
    else
        local item = Item_Item.GetData(chijiItem.ItemId)
        if item then
            IconTexture:LoadMaterial(item.Icon)
            AmountLabel.text = tostring(info.count)
            QualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item, nil, false);
        end

        if LuaPUBGMgr.IsSkill(info.itemId) or (LuaPUBGMgr.IsItem(info.itemId) and info.count <= 1 )then
            local t_action = {}
            t_action[1] = useAction
            t_action[2] = dropAction
            local t_name = {}
            t_name[1] = LocalString.GetString("使用")
            t_name[2] = LocalString.GetString("丢弃")
            actsource = DefaultItemActionDataSource.Create(2, t_action,t_name)
        else
            local t_action = {}
            t_action[1] = useAction
            t_action[2] = slipAction
            t_action[3] = dropAction
            local t_name = {}
            t_name[1] = LocalString.GetString("使用")
            t_name[2] = LocalString.GetString("拆分")
            t_name[3] = LocalString.GetString("丢弃")
            actsource = DefaultItemActionDataSource.Create(3, t_action,t_name)
        end
    end

    CommonDefs.AddOnClickListener(item, DelegateFactory.Action_GameObject(function (go) 
        self:OnPackageItemSelected(go, info, index)
        if not self.IsClearUpState then
            CItemInfoMgr.ShowLinkItemTemplateInfo(chijiItem.ItemId, false, actsource, AlignType.ScreenLeft, 0, 0, 0, 0)
        end
    end),false)
end

function LuaPUBGSelfPackageView:OnPackageItemSelected(item, info, index)
    if not item then return end
    if self.IsClearUpState then
        if not info or info.itemId == 0 then return end
        local ClearupCheckbox = item.transform:Find("ClearupCheckbox"):GetComponent(typeof(QnCheckBox))
        ClearupCheckbox.Selected = not ClearupCheckbox.Selected

        if ClearupCheckbox.Selected and not self:IsAlreadyWaitToClear(index) then
            table.insert(self.WaitingToClearUpList, index)
        elseif not ClearupCheckbox.Selected and self:IsAlreadyWaitToClear(index) then
            self:RemoveFromWaitingList(index)
        end
    else
        -- todo 加一点选中操作
        self.SelectedItemIndex = index
        for i = 1, #self.PackageItemList do
            self:SetPackageItemSelected(self.PackageItemList[i], (self.PackageItemList[i] == item), i)
        end
    end
end

function LuaPUBGSelfPackageView:IsAlreadyWaitToClear(index)
    if not self.WaitingToClearUpList then return false end

    for i = 1, #self.WaitingToClearUpList do
        if self.WaitingToClearUpList[i] == index then
            return true
        end
    end
    return false
end

function LuaPUBGSelfPackageView:RemoveFromWaitingList(index)
    if not self.WaitingToClearUpList then return end
    for i = #self.WaitingToClearUpList, 1, -1 do
        if self.WaitingToClearUpList[i] == index then
            table.remove(self.WaitingToClearUpList, i)
        end
    end
end

function LuaPUBGSelfPackageView:SetPackageItemSelected(item, isSelected, index)
    if not item then return end

    -- 该位置没有物品则无法点击
    if isSelected then
        if not self.PackageInfos[index] or self.PackageInfos[index].itemId == 0 then return end
    end

    local SelectedSprite = item.transform:Find("SelectedSprite").gameObject
    SelectedSprite:SetActive(isSelected)
end

function LuaPUBGSelfPackageView:UpdateClearUpState(item, isClearUp, index)
    if not item then return end

    -- 该位置没有物品则无法出现checkbox
    if isClearUp then
        if not self.PackageInfos[index] or self.PackageInfos[index].itemId == 0 then return end
    end

    local ClearupCheckbox = item.transform:Find("ClearupCheckbox"):GetComponent(typeof(QnCheckBox))
    ClearupCheckbox.gameObject:SetActive(isClearUp)
    ClearupCheckbox.Selected = false
end

function LuaPUBGSelfPackageView:ShowNormalBtnRoot()
    self.NormalBtnRoot:SetActive(true)
    self.ClearUpBtnRoot:SetActive(false)
    self.IsClearUpState = false
    self:ClearSelectionUnderClearUp()
    -- 显示没有选中态
    for i = 1, #self.PackageItemList do
        self:UpdateClearUpState(self.PackageItemList[i], false, i)
    end
end

function LuaPUBGSelfPackageView:ShowClearUpBtnRoot()
    self.NormalBtnRoot:SetActive(false)
    self.ClearUpBtnRoot:SetActive(true)
    self.IsClearUpState = true
    self:ClearSelectionUnderNonClearUp()
    for i = 1, #self.PackageItemList do
        self:UpdateClearUpState(self.PackageItemList[i], true, i)
    end
end

function LuaPUBGSelfPackageView:ClearSelectionUnderNonClearUp()
    if self.SelectedItemIndex ~= -1 then
        self.SelectedItemIndex = -1
        for i = 1, #self.PackageItemList do
            self:SetPackageItemSelected(self.PackageItemList[i], false, i)
        end
    end
end

function LuaPUBGSelfPackageView:ClearSelectionUnderClearUp()
    self.WaitingToClearUpList = {}
end

function LuaPUBGSelfPackageView:OnClearUpBtnClicked(go)
    self:ShowClearUpBtnRoot()
end

function LuaPUBGSelfPackageView:OnArrangeBtnClicked(go)
    Gac2Gas.RequestMergeChiJiBag()
end

function LuaPUBGSelfPackageView:OnDiscardBtnClicked(go)

    for i = 1, #self.WaitingToClearUpList do
        local info = self.PackageInfos[self.WaitingToClearUpList[i]]
        Gac2Gas.RequestDropChiJiItem(self.WaitingToClearUpList[i], info.id)
    end
    self:ClearSelectionUnderClearUp()
    -- 显示没有选中态
    for i = 1, #self.PackageItemList do
        self:UpdateClearUpState(self.PackageItemList[i], true, i)
    end
end

function LuaPUBGSelfPackageView:OnCancelBtnClicked(go)
    self:ShowNormalBtnRoot()
end

function LuaPUBGSelfPackageView:UpdatePackageItem(pos, info)
    if pos > self.BAGSIZE then return end

    self.PackageInfos[pos] = info
    self:ClearSelectionUnderNonClearUp()
    self:InitPackageItem(self.PackageItemList[pos], info, pos)
end

function LuaPUBGSelfPackageView:OnEnable()
    g_ScriptEvent:AddListener("UpdateWholePUBGPackageInfos", self, "InitPUBGPackage")
    g_ScriptEvent:AddListener("UpdateSinglePUBGPackageInfo", self, "UpdatePackageItem")
end

function LuaPUBGSelfPackageView:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateWholePUBGPackageInfos", self, "InitPUBGPackage")
    g_ScriptEvent:RemoveListener("UpdateSinglePUBGPackageInfo", self, "UpdatePackageItem")
end

return LuaPUBGSelfPackageView