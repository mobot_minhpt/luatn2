local CGaoChangEnterWnd = import "L10.UI.CGaoChangEnterWnd"
local CActivityEnterMgr = import "L10.UI.CActivityEnterMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CScene            = import "L10.Game.CScene"

LuaGaoChangCrossMgr = {}

-- Gas2Gac

function LuaGaoChangCrossMgr:GaoChangCrossApplySuccess()
    if CGaoChangEnterWnd.Instance then
        self:ShowGaochangCrossEnterWnd(false)
    end
end

function LuaGaoChangCrossMgr:GaoChangCrossInviteToStage1()
    local msg = g_MessageMgr:FormatMessage("GAOCHANGCROSS_OPEN_INTERFACE_TIP")
    MessageWndManager.ShowConfirmMessage(msg, math.random(15, 30), true, DelegateFactory.Action(function ()
        Gac2Gas.GaoChangCrossEnterStage1()
    end), nil)
end

function LuaGaoChangCrossMgr:GaoChangCrossInviteToApply()
    local msg = g_MessageMgr:FormatMessage("GAOCHANGCROSS_OPEN_INFORM")
    MessageWndManager.ShowOKCancelMessageWithTimelimitAndPriority(msg, 300, 0, DelegateFactory.Action(function()
        Gac2Gas.GaoChangCrossQueryApplyInfo()
    end), nil, nil, nil, false)
end

function LuaGaoChangCrossMgr:GaoChangCrossQueryApplyInfoResult(bApplied)
    self:ShowGaochangCrossEnterWnd(not bApplied)
end

-- 打开跨服高昌进入界面
function LuaGaoChangCrossMgr:ShowGaochangCrossEnterWnd(canApply)
    local wndTitle = LocalString.GetString("高昌迷宫")
    local msg = g_MessageMgr:FormatMessage("GAOCHANGCROSS_INTERFACE_TIP")
    local okLabel = canApply and LocalString.GetString("我要报名") or LocalString.GetString("已报名")
    local leftLabel = LocalString.GetString("进入高昌")

    local OnApplyButtonClick = DelegateFactory.Action(function()
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("GAOCHANGCROSS_APPLY_INTERFACE_TIP"), DelegateFactory.Action(function()
			Gac2Gas.GaoChangCrossApply()
		end), nil, nil, nil, false)
    end)

    local OnEnterButtonClick = DelegateFactory.Action(function()
        Gac2Gas.GaoChangCrossEnterStage1()
        CUIManager.CloseUI(CUIResources.GaoChangEnterWnd)
    end)

    CActivityEnterMgr.ShowActivityEnterWnd(wndTitle, msg, okLabel, leftLabel, canApply, true, OnApplyButtonClick,
        OnEnterButtonClick, true, "", nil)
end

function LuaGaoChangCrossMgr:ShowTopRightWnd(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == GaoChangCross_Setting.GetData().GameplayId then
        wnd.contentNode:SetActive(true)
        CUIManager.ShowUI(CLuaUIResources.GaoChangTopRightWnd)
    else
        CUIManager.CloseUI(CLuaUIResources.GaoChangTopRightWnd)
    end
end

-- 是否在场景中
function LuaGaoChangCrossMgr:IsInScene()
    if not CScene.MainScene then return false end

    local bResult = false
    GaoChangCross_Stage.Foreach(function(id, data)
        if data.NewMapId == CScene.MainScene.SceneTemplateId then
            bResult = true
        end
    end)

    return bResult
end

-- 请求报名信息
function LuaGaoChangCrossMgr:QueryApplyInfo()
    Gac2Gas.GaoChangCrossQueryApplyInfo()
end
