-- Auto Generated!!
local CEquipBaptizeAttributeItem = import "L10.UI.CEquipBaptizeAttributeItem"
local UIWidget = import "UIWidget"
CEquipBaptizeAttributeItem.m_Init_CS2LuaHook = function (this, record) 
    if CommonDefs.StringLength(record.name) > 4 then
        this.nameLabel.pivot = UIWidget.Pivot.Right
    else
        this.nameLabel.pivot = UIWidget.Pivot.Center
    end

    this.nameLabel.text = record.name
    if record.valuePairs.Length == 1 then
        this.slider1.gameObject:SetActive(true)
        this.slider1.value = record.valuePairs[0].value
        --
        this.slider2.gameObject:SetActive(false)
        this.slider3.gameObject:SetActive(false)
        if record.valuePairs[0].type == typeof(System.Single) then
            local val = record.valuePairs[0]
            if val.showPercent then
                this.val1Label.text = System.String.Format("{0:f3}%/{1:f3}%", val.current, val.max)
            else
                this.val1Label.text = System.String.Format("{0:f3}/{1:f3}", val.current, val.max)
            end
        else
            local val = record.valuePairs[0]
            this.val1Label.text = System.String.Format("{0}/{1}", val.current, val.max)
        end
        this.splitTf.gameObject:SetActive(false)
    elseif record.valuePairs.Length == 2 then
        this.slider1.gameObject:SetActive(false)
        this.slider2.gameObject:SetActive(true)
        this.slider2.value = record.valuePairs[0].value
        this.slider3.gameObject:SetActive(true)
        this.slider3.value = record.valuePairs[1].value

        if record.valuePairs[0].type == typeof(System.Single) then
            local val1 = record.valuePairs[0]
            local val2 = record.valuePairs[1]
            if val1.showPercent then
                this.val2Label.text = System.String.Format("{0:f3}%/{1:f3}%", val1.current, val1.max)
            else
                this.val2Label.text = System.String.Format("{0:f3}/{1:f3}", val1.current, val1.max)
            end

            if val2.showPercent then
                this.val3Label.text = System.String.Format("{0:f3}%/{1:f3}%", val2.current, val2.max)
            else
                this.val3Label.text = System.String.Format("{0:f3}/{1:f3}", val2.current, val2.max)
            end
        else
            local val1 = record.valuePairs[0]
            local val2 = record.valuePairs[1]
            this.val2Label.text = System.String.Format("{0}/{1}", val1.current, val1.max)
            this.val3Label.text = System.String.Format("{0}/{1}", val2.current, val2.max)
        end
        this.splitTf.gameObject:SetActive(true)
    end
end
