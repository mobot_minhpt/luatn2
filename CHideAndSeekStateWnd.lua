-- Auto Generated!!
local Boolean = import "System.Boolean"
local BulletInfo = import "L10.UI.CHideAndSeekStateWnd+BulletInfo"
local Callback = import "EventDelegate+Callback"
local CChatLinkMgr = import "CChatLinkMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CDuoMaoMaoMgr = import "L10.Game.CDuoMaoMaoMgr"
local CHideAndSeekStateWnd = import "L10.UI.CHideAndSeekStateWnd"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CScene = import "L10.Game.CScene"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CVoiceMsg = import "L10.Game.CVoiceMsg"
local DelegateFactory = import "DelegateFactory"
local Ease = import "DG.Tweening.Ease"
local EnumEventType = import "EnumEventType"
local EnumHideAndSeekDanmuForces = import "L10.UI.CHideAndSeekStateWnd+EnumHideAndSeekDanmuForces"
local EventDelegate = import "EventDelegate"
local EventManager = import "EventManager"
local GameSetting_Common = import "L10.Game.GameSetting_Common"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local NGUIText = import "NGUIText"
local Object = import "UnityEngine.Object"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CSpeechCtrlMgr = import "L10.Game.CSpeechCtrlMgr"
local CSpeechCtrlType = import "L10.Game.CSpeechCtrlType"
CHideAndSeekStateWnd.m_Init_CS2LuaHook = function (this) 
    if CDuoMaoMaoMgr.Inst.HideLeftNum ~= - 1 and CDuoMaoMaoMgr.Inst.HideTotalNum ~= - 1 and CDuoMaoMaoMgr.Inst.SeekLeftNum ~= - 1 and CDuoMaoMaoMgr.Inst.SeekTotalNum ~= - 1 then
        this.hiderNumber.text = (tostring(CDuoMaoMaoMgr.Inst.HideLeftNum) .. "/") .. tostring(CDuoMaoMaoMgr.Inst.HideTotalNum)
        this.seekerNumber.text = (tostring(CDuoMaoMaoMgr.Inst.SeekLeftNum) .. "/") .. tostring(CDuoMaoMaoMgr.Inst.SeekTotalNum)
    else
        this.hiderNumber.text = ""
        this.seekerNumber.text = ""
    end
    this.MaxDanMuLength = GameSetting_Common.GetData().MaxDanMuLength
    if CSpeechCtrlMgr.Inst:CheckIfNeedSpeechCtrl(CSpeechCtrlType.Type_DuoMaoMao_DanMu, false) then
        this:CloseDanMu()
        this.zhankaiTf.gameObject:SetActive(false)
	    this.shousuoTf.gameObject:SetActive(false)
    else
        this:OpenDanMu()
    end
    CommonDefs.DictClear(this.voiceIdList)
end
CHideAndSeekStateWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.expandButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        this.expandButton.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
    UIEventListener.Get(this.leaveButton).onClick = MakeDelegateFromCSFunction(this.OnLeaveButtonClick, VoidDelegate, this)

    this.chatInput.characterLimit = this.MaxDanMuLength
    this.zhankaiTf.gameObject:SetActive(false)
    this.shousuoTf.gameObject:SetActive(true)

    this.settingBtn:SetSelected(true, false)
    UIEventListener.Get(this.closeBtn).onClick = MakeDelegateFromCSFunction(this.OnClickHideButton, VoidDelegate, this)
    UIEventListener.Get(this.zhankaiBtn).onClick = MakeDelegateFromCSFunction(this.OnClickZhanKaiButton, VoidDelegate, this)

    this.settingBtn.OnButtonSelected = MakeDelegateFromCSFunction(this.OnSettingButtonSelected, MakeGenericClass(Action1, Boolean), this)
    UIEventListener.Get(this.sendBtn).onClick = MakeDelegateFromCSFunction(this.OnSendButtonClicked, VoidDelegate, this)
    EventDelegate.Add(this.chatInput.onChange, MakeDelegateFromCSFunction(this.OnInputValueChange, Callback, this))
end
CHideAndSeekStateWnd.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.UpdatePlayerNumLeft, MakeDelegateFromCSFunction(this.UpdatePlayerNum, MakeGenericClass(Action4, UInt32, UInt32, UInt32, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.SceneRemainTimeUpdate, MakeDelegateFromCSFunction(this.OnRemainTimeUpdate, MakeGenericClass(Action1, Int32), this))
    EventManager.RemoveListenerInternal(EnumEventType.RecvChatMsg, MakeDelegateFromCSFunction(this.OnRecMsg, MakeGenericClass(Action1, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.NewVoiceTextReady, MakeDelegateFromCSFunction(this.VoiceMsgRec, MakeGenericClass(Action2, String, String), this))
    --这个界面不显示的话，那么tip界面肯定也不显示
    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end
end
CHideAndSeekStateWnd.m_OnRemainTimeUpdate_CS2LuaHook = function (this, leftTime) 
    if CScene.MainScene ~= nil then
        this.remainingTime.text = this:GetRemainTimeText(CScene.MainScene.ShowTime)
    else
        this.remainingTime.text = ""
    end
end
CHideAndSeekStateWnd.m_GetRemainTimeText_CS2LuaHook = function (this, totalSeconds) 
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return System.String.Format("[ACF9FF]{0:00}:{1:00}:{2:00}[-]", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return System.String.Format("[ACF9FF]{0:00}:{1:00}[-]", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end
CHideAndSeekStateWnd.m_OnSettingButtonSelected_CS2LuaHook = function (this, open) 
    if open then
        this:OpenDanMu()
    else
        this:CloseDanMu()
    end
end
CHideAndSeekStateWnd.m_OnInputValueChange_CS2LuaHook = function (this) 
    local str = this.chatInput.value
    local len = this:GetLength(str)
    if len > this.MaxDanMuLength then
        repeat
            str = CommonDefs.StringSubstring2(str, 0, CommonDefs.StringLength(str) - 1)
            len = this:GetLength(str)
        until not (len > this.MaxDanMuLength)
        this.chatInput.value = str
    end
end
CHideAndSeekStateWnd.m_OnSendButtonClicked_CS2LuaHook = function (this, go) 
    if System.String.IsNullOrEmpty(this.chatInput.value) then
        return
    end
    local chatContent = this.chatInput.value
    CChatMgr.Inst:SendChatMsg(CChatMgr.CHANNEL_NAME_PLAY, chatContent, false)
    this.chatInput.value = ""
end
CHideAndSeekStateWnd.m_GetDanMuColor_CS2LuaHook = function (this, forces) 
    repeat
        local default = forces
        if default == EnumHideAndSeekDanmuForces.Self then
            return NGUIText.ParseColor24(CHideAndSeekStateWnd.SelfColor, 0)
        elseif default == EnumHideAndSeekDanmuForces.Hider then
            return NGUIText.ParseColor24(CHideAndSeekStateWnd.HiderColor, 0)
        elseif default == EnumHideAndSeekDanmuForces.Seeker then
            return NGUIText.ParseColor24(CHideAndSeekStateWnd.SeekerColor, 0)
        elseif default == EnumHideAndSeekDanmuForces.None then
            return NGUIText.ParseColor24(CHideAndSeekStateWnd.NoneColor, 0)
        end
    until 1
    return Color.white
end
CHideAndSeekStateWnd.m_GetHideAndSeekForce_CS2LuaHook = function (this, id) 
    if id == CClientMainPlayer.Inst.Id then
        return EnumHideAndSeekDanmuForces.Self
    elseif CDuoMaoMaoMgr.Inst:isHider(id) then
        return EnumHideAndSeekDanmuForces.Hider
    elseif CDuoMaoMaoMgr.Inst:IsSeeker(id) then
        return EnumHideAndSeekDanmuForces.Seeker
    end
    return EnumHideAndSeekDanmuForces.None
end
CHideAndSeekStateWnd.m_OnRecMsg_CS2LuaHook = function (this, id) 
    local chatMsg = CChatMgr.Inst:GetLatestMsg()
    if chatMsg ~= nil and chatMsg.channelName == LocalString.GetString("友方") and chatMsg.fromUserId > 0 then
        local voiceMsg = CVoiceMsg.Parse(chatMsg.message)
        local info = CreateFromClass(BulletInfo, this:GetHideAndSeekForce(chatMsg.fromUserId), chatMsg.fromUserName, chatMsg.fromUserId)
        if voiceMsg == nil then
            this:OnAddBullet(chatMsg.message, info)
        else
            CommonDefs.DictAdd(this.voiceIdList, typeof(String), voiceMsg.VoiceId, typeof(BulletInfo), info)
        end
    end
end
CHideAndSeekStateWnd.m_VoiceMsgRec_CS2LuaHook = function (this, id, text) 
    if CommonDefs.DictContains(this.voiceIdList, typeof(String), id) then
        this:OnAddBullet(text, CommonDefs.DictGetValue(this.voiceIdList, typeof(String), id))
        CommonDefs.DictRemove(this.voiceIdList, typeof(String), id)
    end
end
CHideAndSeekStateWnd.m_OnAddBullet_CS2LuaHook = function (this, content, info) 
    if System.String.IsNullOrEmpty(content) then
        return
    end
    local label = this.pool:GetFromPool(0)
    label.transform.parent = this.bulletScreen.transform
    label.transform.localScale = Vector3.one
    label:SetActive(true)
    local bullet = CommonDefs.GetComponent_GameObject_Type(label, typeof(UILabel))
    if bullet == nil or info == nil then
        return
    end
    bullet.text = CChatLinkMgr.TranslateToNGUIText(this:GenerateBulletText(content, info), false)
    bullet.color = this:GetDanMuColor(info.force)
    bullet.fontSize = UnityEngine_Random(38, 52)
    bullet.transform.localPosition = Vector3(math.floor(this.bulletScreen.width / 2), UnityEngine_Random(math.floor(- this.bulletScreen.height / 2), math.floor(this.bulletScreen.height / 2)), 0)
    CommonDefs.OnComplete_Tweener(CommonDefs.SetEase_Tweener(LuaTweenUtils.DOLocalMoveX(bullet.transform, math.floor(- this.bulletScreen.width / 2) - bullet.width, UnityEngine_Random(6, 12), false), Ease.Linear), DelegateFactory.TweenCallback(function () 
        label.transform.parent = nil
        Object.Destroy(label)
    end))
end
CHideAndSeekStateWnd.m_GenerateBulletText_CS2LuaHook = function (this, content, info) 
    if info == nil then
        return nil
    end
    if CClientMainPlayer.Inst.Id == info.fromUserId then
        return (LocalString.GetString("我") .. ":") .. content
    end
    return (info.fromUserName .. ":") .. content
end
