require("3rdParty/ScriptEvent")
require("common/common_include")
local MessageMgr=import "L10.Game.MessageMgr"
local Time=import "UnityEngine.Time"
local LuaGameObject=import "LuaGameObject"
local CTowerDefenseMgr = import "L10.Game.CTowerDefenseMgr"

CLuaTowerDefenseStateWnd=class()
RegistClassMember(CLuaTowerDefenseStateWnd,"m_ExpandButton")
RegistClassMember(CLuaTowerDefenseStateWnd,"m_CountDownTweener")
RegistClassMember(CLuaTowerDefenseStateWnd,"m_CountdownLabel")
RegistClassMember(CLuaTowerDefenseStateWnd,"m_GoldLabel")
RegistClassMember(CLuaTowerDefenseStateWnd,"m_HpBar")
RegistClassMember(CLuaTowerDefenseStateWnd,"m_FormatFunc")
RegistClassMember(CLuaTowerDefenseStateWnd,"m_Start")
RegistClassMember(CLuaTowerDefenseStateWnd,"m_LastTickTime")
RegistClassMember(CLuaTowerDefenseStateWnd,"m_Countdown")
RegistClassMember(CLuaTowerDefenseStateWnd,"m_RoundIndex")
RegistClassMember(CLuaTowerDefenseStateWnd,"m_MaxRoundIndex")

function CLuaTowerDefenseStateWnd:Init()
    self.m_ExpandButton=LuaGameObject.GetChildNoGC(self.transform,"ExpandButton").gameObject
    UIEventListener.Get(self.m_ExpandButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
        self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
    end)

	if CTowerDefenseMgr.Inst.type == 1 then
		self.m_CountdownLabel=LuaGameObject.GetChildNoGC(self.transform,"CountdownLabel").label
		self.m_GoldLabel=LuaGameObject.GetChildNoGC(self.transform,"GoldLabel").label
		self.m_HpBar=LuaGameObject.GetChildNoGC(self.transform,"HpBar").slider
		self.m_HpBar.value=CTowerDefenseMgr.Inst.HouseHp/CTowerDefenseMgr.Inst.HouseFullHp
		local gameStatus=CTowerDefenseMgr.Inst.gameStatus
		local countdown=CTowerDefenseMgr.Inst.countdown-math.floor(Time.time-CTowerDefenseMgr.Inst.beginTime)
		if countdown<=0 then
			countdown=0
		end
		local roundIndex=CTowerDefenseMgr.Inst.roundIndex
		local maxRoundIndex=CTowerDefenseMgr.Inst.maxRoundIndex
		self.m_Start=false
		self:RefreshPlayStatus(gameStatus,countdown,roundIndex,maxRoundIndex)
	elseif CTowerDefenseMgr.Inst.type == 2 then 
		LuaGameObject.GetChildNoGC(self.transform, "Left").gameObject:SetActive(false)
	end
end

function CLuaTowerDefenseStateWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
	if CTowerDefenseMgr.Inst.type == 1 then
		g_ScriptEvent:AddListener("TowerDefenseSyncPlayStatusInfo", self, "OnTowerDefenseSyncPlayStatusInfo")
		g_ScriptEvent:AddListener("TowerDefenseSyncMyPlayPlayerInfo", self, "OnTowerDefenseSyncMyPlayPlayerInfo")
		g_ScriptEvent:AddListener("TowerDefenseSyncHouseHp", self, "OnTowerDefenseSyncHouseHp")
	end

    
end

function CLuaTowerDefenseStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
	if CTowerDefenseMgr.Inst.type == 1 then 
		g_ScriptEvent:RemoveListener("TowerDefenseSyncPlayStatusInfo", self, "OnTowerDefenseSyncPlayStatusInfo")
		g_ScriptEvent:RemoveListener("TowerDefenseSyncMyPlayPlayerInfo", self, "OnTowerDefenseSyncMyPlayPlayerInfo")
		g_ScriptEvent:RemoveListener("TowerDefenseSyncHouseHp", self, "OnTowerDefenseSyncHouseHp")
	end
end

function CLuaTowerDefenseStateWnd:OnTowerDefenseSyncHouseHp(args)
    if args and args.Length==2 then
        self.m_HpBar.value=args[0]/args[1]
    end
end

function CLuaTowerDefenseStateWnd:OnHideTopAndRightTipWnd()
    self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180);
end

function CLuaTowerDefenseStateWnd:OnTowerDefenseSyncPlayStatusInfo(args)
    if self.m_CountDownTweener then
        LuaTweenUtils.Kill(self.m_CountDownTweener,true)
        self.m_CountDownTweener=nil
    end
    self:RefreshPlayStatus(args[0],args[1],args[2],args[3])
end
function CLuaTowerDefenseStateWnd:RefreshPlayStatus(gameStatus,countdown,roundIndex,maxRoundIndex)
    self.m_RoundIndex=roundIndex
    self.m_MaxRoundIndex=maxRoundIndex
    if gameStatus==EnumTowerDefenseGameStaus.ePreparing then
        self.m_FormatFunc=function(countdown)
            self.m_CountdownLabel.text=MessageMgr.Inst:FormatMessage("Beginning_Msg",{self:FormatCountdown(countdown)}) or " "
        end
        self.m_Start=true
        self.m_LastTickTime=Time.time
        self.m_Countdown=countdown
        self.m_FormatFunc(countdown)
    elseif gameStatus==EnumTowerDefenseGameStaus.eRunning then
        if roundIndex < maxRoundIndex then
            self.m_FormatFunc=function(countdown)
                self.m_CountdownLabel.text=MessageMgr.Inst:FormatMessage("NextRound_Msg",{tostring(self.m_RoundIndex),tostring(self.m_MaxRoundIndex),self:FormatCountdown(countdown)}) or " "
            end
        else
            self.m_FormatFunc=nil
            self.m_CountdownLabel.text=SafeStringFormat3(LocalString.GetString("第%d/%d波"),self.m_RoundIndex,self.m_MaxRoundIndex)
        end
        self.m_Start=true
        self.m_LastTickTime=Time.time
        self.m_Countdown=countdown
        if self.m_FormatFunc then
            self.m_FormatFunc(countdown)
        end
    elseif gameStatus==EnumTowerDefenseGameStaus.eEnd then
        self.m_Start=false
        self.m_FormatFunc=nil
        self.m_CountdownLabel.text=MessageMgr.Inst:FormatMessage("Final_msg",{self.m_RoundIndex}) or " "
    else
        self.m_FormatFunc=nil
        self.m_Start=false
        self.m_CountdownLabel.text=" "
    end
    self.m_GoldLabel.text=tostring(CTowerDefenseMgr.Inst.Gold)
    
end


function CLuaTowerDefenseStateWnd:Update()
    if self.m_Start then
        if Time.time>self.m_LastTickTime+1 then
            self.m_LastTickTime = Time.time;
            self.m_Countdown = self.m_Countdown - 1;
            if self.m_Countdown<=0 then
                self.m_Start = false
                self.m_Countdown=0
                if self.m_FormatFunc then
                    self.m_FormatFunc(0)
                end
                return
            end
            if self.m_FormatFunc then
                self.m_FormatFunc(self.m_Countdown)
            end
        end
    end
end
function CLuaTowerDefenseStateWnd:FormatCountdown(countdown)
    return SafeStringFormat3("%02d:%02d",math.floor(countdown/60),countdown%60)
end
function CLuaTowerDefenseStateWnd:OnTowerDefenseSyncMyPlayPlayerInfo()
    self.m_GoldLabel.text=tostring(CTowerDefenseMgr.Inst.Gold)
end

function CLuaTowerDefenseStateWnd:OnDestroy()
    if self.m_CountDownTweener then
        LuaTweenUtils.Kill(self.m_CountDownTweener,true)
        self.m_CountDownTweener=nil
    end
end
return CLuaTowerDefenseStateWnd
