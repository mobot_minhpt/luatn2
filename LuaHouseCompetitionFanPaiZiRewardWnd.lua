local CUIFx = import "L10.UI.CUIFx"
local CUITexture = import "L10.UI.CUITexture"
local QnButton = import "L10.UI.QnButton"
local DelegateFactory = import "DelegateFactory"
local CBaseWnd = import "L10.UI.CBaseWnd"

CLuaHouseCompetitionFanPaiZiRewardWnd = class()

RegistClassMember(CLuaHouseCompetitionFanPaiZiRewardWnd,"m_RootGo")
RegistClassMember(CLuaHouseCompetitionFanPaiZiRewardWnd,"m_Fx")
RegistClassMember(CLuaHouseCompetitionFanPaiZiRewardWnd,"m_RewardIcon")
RegistClassMember(CLuaHouseCompetitionFanPaiZiRewardWnd,"m_ReceiveRewardButton")
RegistClassMember(CLuaHouseCompetitionFanPaiZiRewardWnd,"m_ShowRootTick")

function CLuaHouseCompetitionFanPaiZiRewardWnd:Awake()
    self:InitComponents()
end

function CLuaHouseCompetitionFanPaiZiRewardWnd:Init()
    self.m_ReceiveRewardButton.OnClick = DelegateFactory.Action_QnButton(function (btn) 
        self.transform:GetComponent(typeof(CBaseWnd)):Close()
    end)

    self.m_RootGo:SetActive(false)
    self.m_Fx:DestroyFx()
    self.m_Fx:LoadFx("fx/ui/prefab/UI_xiaozhu_huodejiangli.prefab")
    self.m_ShowRootTick = RegisterTickOnce(function()
        self.m_RootGo:SetActive(true)
    end, 400)
    
    self.m_RewardIcon:LoadMaterial(HouseCompetition_Setting.GetData().FanPaiZi_Zhuangshiwu_Icon)
end

function CLuaHouseCompetitionFanPaiZiRewardWnd:InitComponents()
    self.m_RootGo = self.transform:Find("Root").gameObject
    self.m_Fx = self.transform:Find("FxOffset/Fx"):GetComponent(typeof(CUIFx))
    self.m_RewardIcon = self.transform:Find("Root/RewardIcon"):GetComponent(typeof(CUITexture))
    self.m_ReceiveRewardButton = self.transform:Find("Root/ReceiveRewardButton"):GetComponent(typeof(QnButton))
end

function CLuaHouseCompetitionFanPaiZiRewardWnd:CancelShowRootTick()
    if self.m_ShowRootTick then
        UnRegisterTick(self.m_ShowRootTick)
        self.m_ShowRootTick = nil
    end
end

function CLuaHouseCompetitionFanPaiZiRewardWnd:OnDestroy()
    self:CancelShowRootTick()
end