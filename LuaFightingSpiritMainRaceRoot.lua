local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"
local CGameReplayMgr = import "L10.Game.CGameReplayMgr"

LuaFightingSpiritMainRaceRoot = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFightingSpiritMainRaceRoot, "MainRaceTemplate", "MainRaceTemplate", GameObject)
RegistChildComponent(LuaFightingSpiritMainRaceRoot, "MainRacePositionGrid", "MainRacePositionGrid", GameObject)
RegistChildComponent(LuaFightingSpiritMainRaceRoot, "LabelRoot", "LabelRoot", GameObject)
RegistChildComponent(LuaFightingSpiritMainRaceRoot, "DeleteBtn", "DeleteBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaFightingSpiritMainRaceRoot, "m_MainRaceData")
RegistClassMember(LuaFightingSpiritMainRaceRoot, "m_MatchRecordData")
RegistClassMember(LuaFightingSpiritMainRaceRoot, "m_MainRaceItem")
RegistClassMember(LuaFightingSpiritMainRaceRoot, "m_MainRaceTeamPositionGrid")
RegistClassMember(LuaFightingSpiritMainRaceRoot, "m_HasInited")
RegistClassMember(LuaFightingSpiritMainRaceRoot, "m_MatchMap")
RegistClassMember(LuaFightingSpiritMainRaceRoot, "m_InverseMatchMap")
RegistClassMember(LuaFightingSpiritMainRaceRoot, "m_MatchTypeMap")
RegistClassMember(LuaFightingSpiritMainRaceRoot, "m_UrlDownloadedAction")
RegistClassMember(LuaFightingSpiritMainRaceRoot, "m_VideoDownloadedAction")
RegistClassMember(LuaFightingSpiritMainRaceRoot, "m_VideoDownloadStartAction")
RegistClassMember(LuaFightingSpiritMainRaceRoot, "m_VideoRemovedAction")

function LuaFightingSpiritMainRaceRoot:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.DeleteBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDeleteBtnClick()
	end)


    --@endregion EventBind end
end

function LuaFightingSpiritMainRaceRoot:Init()
    self.m_HasInited = true

    -- 获取节点位置
    self.m_MainRaceTeamPositionGrid = {}
    for i=1, 14 do
        local g = self.MainRacePositionGrid.transform:Find(tostring(i)).gameObject
        table.insert(self.m_MainRaceTeamPositionGrid, g)
    end

    -- 服务器下发顺序与对应显示场次
    self.m_MatchMap = {
        1, 3, 4, 2, 7, 8, 5, 6, 9, 10, 12, 11, 13, 14 
    }

    self.m_InverseMatchMap = {}
    for i = 1, 14 do
        self.m_InverseMatchMap[self.m_MatchMap[i]] = i
    end

    -- 胜者败者组标志
    -- 胜者1 败者2 胜败3
    self.m_MatchTypeMap = {
        1,1,1,1,2,2,1,1,2,2,2,1,2,3
    }

    self.MainRaceTemplate:SetActive(false)
end

-- 初始化主赛事数据
-- 输入参数是List<CFightingSpiritMatchRecord>
function LuaFightingSpiritMainRaceRoot:ShowData(args)
    if not self.m_HasInited then
        self:Init()
    end
    local matchList = args[0]
    local index = args[1]
    self.m_MatchRecordData = matchList
    self.m_MainRaceData = {}

    -- 14场比赛都会上传
    local length = math.min(matchList.Count-1, 13)
    if length >0 then
        for i= 0, length do
            local matchData = matchList[i]
            local realIndex = self.m_MatchMap[i+1]
    
            local data = {}
            data.num = realIndex
            self.m_MainRaceData[realIndex] = data
            -- 本场赛事信息
            data.name1 = matchData.ServerName1
            data.name2 = matchData.ServerName2
            data.server1 = matchData.ServerID1
            data.server2 = matchData.ServerID2

            if index == 19 then
                data.winnerIs1st = matchData.WinnerID == data.server1
            else
                print(matchData.WinnerID)
                data.winnerIs1st = matchData.WinnerID == 1
            end
    
            if data.winnerIs1st then
                data.score1 = matchData.WinScore
                data.score2 = matchData.LoseScore
            else
                data.score1 = matchData.LoseScore
                data.score2 = matchData.WinScore
            end
            data.type = self.m_MatchTypeMap[realIndex]
        end
    end
    
    self:InitMainRaceRoot(self.m_MainRaceData)
end

function LuaFightingSpiritMainRaceRoot:InitMainRaceRoot(mainRaceData)
    self.m_MainRaceItem = {}
    for i=1, #mainRaceData do
        local root = self.m_MainRaceTeamPositionGrid[i]
        Extensions.RemoveAllChildren(root.transform)
        local g = NGUITools.AddChild(root, self.MainRaceTemplate)
        table.insert(self.m_MainRaceItem, g)
        g:SetActive(true)
        self:InitMaineamItem(g, mainRaceData[i], i)
    end
end

function LuaFightingSpiritMainRaceRoot:InitMaineamItem(item, teamData, index)
    local numLabel = item.transform:Find("HeadSprite/Num"):GetComponent(typeof(UILabel))
    local headSprite = item.transform:Find("HeadSprite"):GetComponent(typeof(UISprite))
    local teamNameLabel1 = item.transform:Find("TeamName1"):GetComponent(typeof(UILabel))
    local teamNameLabel2 = item.transform:Find("TeamName2"):GetComponent(typeof(UILabel))
    local winMark1 = item.transform:Find("TeamName1/WinMark"):GetComponent(typeof(UISprite))
    local winMark2 = item.transform:Find("TeamName2/WinMark"):GetComponent(typeof(UISprite))
    local scoreLabel1 = item.transform:Find("TeamName1/Score"):GetComponent(typeof(UILabel))
    local scoreLabel2 = item.transform:Find("TeamName2/Score"):GetComponent(typeof(UILabel))
    local sp1 = item.transform:Find("HeadSprite/Sprite01"):GetComponent(typeof(UISprite))
    local sp2 = item.transform:Find("HeadSprite/Sprite02"):GetComponent(typeof(UISprite))

    local matchData = self.m_MatchRecordData[self.m_InverseMatchMap[index]-1]
    -- 播放标志
    local viewBtn = item.transform:Find("ViewBtn").gameObject
    UIEventListener.Get(viewBtn).onClick = DelegateFactory.VoidDelegate(function()
        -- 播放函数
        CFightingSpiritMgr.Instance:RequestPlayVideo(matchData)
    end)

    local downloadBtn = item.transform:Find("DownloadBtn").gameObject
    -- 下载和播放 调用相同的接口
    UIEventListener.Get(downloadBtn).onClick = DelegateFactory.VoidDelegate(function()
        -- 播放函数
        CFightingSpiritMgr.Instance:RequestPlayVideo(matchData)
    end)

    -- 更新图标
    self:RefreshItemIcon(item, matchData)

    teamNameLabel1.text = teamData.name1
    teamNameLabel2.text = teamData.name2
    numLabel.text = tostring(teamData.num)

    local type = teamData.type
    if type == 1 then
        sp1.color = Color(126/255,172/255,1,150/255)
        sp2.color = Color(126/255,172/255,1,150/255)
    elseif type == 2 then
        sp2.color = Color(227/255,105/255,105/255,160/255)
        sp1.color = Color(227/255,105/255,105/255,160/255)
    else
        sp1.color = Color(126/255,172/255,1,150/255)
        sp2.color = Color(227/255,105/255,105/255,160/255)
    end 

    if teamData.winnerIs1st then
        winMark1.spriteName = "common_fight_result_win"
        winMark2.spriteName = "common_fight_result_lose"
        scoreLabel1.text = SafeStringFormat3("[7EACFF]%s[-]", tostring(teamData.score1))
        scoreLabel2.text = SafeStringFormat3("[E36969]%s[-]",  tostring(teamData.score2))
    else
        winMark1.spriteName = "common_fight_result_lose"
        winMark2.spriteName = "common_fight_result_win"
        scoreLabel1.text = SafeStringFormat3("[E36969]%s[-]", tostring(teamData.score1))
        scoreLabel2.text = SafeStringFormat3("[7EACFF]%s[-]", tostring(teamData.score2))
    end

    scoreLabel1.text = ""
    scoreLabel2.text = ""
end

-- 更新图标状态
function LuaFightingSpiritMainRaceRoot:RefreshItemIcon(item, matchData)
    local viewBtn = item.transform:Find("ViewBtn").gameObject
    local cannotWatchMark = item.transform:Find("CannotWatchMark").gameObject
    local downloadingMark = item.transform:Find("DownloadingMark").gameObject
    local downloadBtn = item.transform:Find("DownloadBtn").gameObject

    cannotWatchMark:SetActive(true)
    viewBtn:SetActive(false)
    downloadingMark:SetActive(false)
    downloadBtn:SetActive(false)
    
    if CFightingSpiritMgr.Instance:CanPlayThisGame(matchData) and CFightingSpiritMgr.Instance:IsGameWatchReplay(matchData.GameIndex) then
        local info = CFightingSpiritMgr.Instance:GetRecordCCPlayInfo(matchData)
        if info then
            cannotWatchMark:SetActive(false)
            viewBtn:SetActive(CGameReplayMgr.Instance:IsFileDownloaded(info.FileName))
            downloadingMark:SetActive(CGameReplayMgr.Instance:IsFileDownloading(info.FileName))
            downloadBtn:SetActive((not CGameReplayMgr.Instance:IsFileDownloading(info.FileName)) and (not CGameReplayMgr.Instance:IsFileDownloaded(info.FileName)))
        end
    end
end

-- 根据文件名来判断是否要更新录像信息
function LuaFightingSpiritMainRaceRoot:OnVideoFilenameReceive(filename)
    local count = math.min(self.m_MatchRecordData.Count-1, 13)
    if count > 0 then
        for i =0, count do
            local matchData = self.m_MatchRecordData[i]
            local info = CFightingSpiritMgr.Instance:GetRecordCCPlayInfo(matchData)
            if info then
                if filename == info.FileName then
                    local item = self.m_MainRaceItem[self.m_MatchMap[i+1]]
                    self:RefreshItemIcon(item, matchData)
                end
            end
        end
    end
end

function LuaFightingSpiritMainRaceRoot:OnEnable()
    if self.m_UrlDownloadedAction == nil then
        self.m_UrlDownloadedAction = DelegateFactory.Action_uint(function(index)
            local count = math.min(self.m_MatchRecordData.Count-1, 13)
            if count >0 then
                for i =0, count do
                    local matchData = self.m_MatchRecordData[i]
                    local item = self.m_MainRaceItem[self.m_MatchMap[i+1]]
                    self:RefreshItemIcon(item, matchData)
                end
            end
        end)
    end

    if self.m_VideoDownloadedAction == nil then
        self.m_VideoDownloadedAction = DelegateFactory.Action_bool_string(function(flag, filename)
            self:OnVideoFilenameReceive(filename)
        end)
    end

    if self.m_VideoDownloadStartAction == nil then
        self.m_VideoDownloadStartAction = DelegateFactory.Action_string(function(filename)
            self:OnVideoFilenameReceive(filename)
        end)
    end

    if self.m_VideoRemovedAction == nil then
        self.m_VideoRemovedAction = DelegateFactory.Action_string(function(filename)
            self:OnVideoFilenameReceive(filename)
        end)
    end

    EventManager.AddListenerInternal(EnumEventType.FightingSpiritMatchRecordURLDownloadComplete, self.m_UrlDownloadedAction)
    EventManager.AddListenerInternal(EnumEventType.GameVideoDownloaded, self.m_VideoDownloadedAction)
    EventManager.AddListenerInternal(EnumEventType.GameVideoDownloadStart, self.m_VideoDownloadStartAction)
    EventManager.AddListenerInternal(EnumEventType.GameVideoRemoved, self.m_VideoRemovedAction)
end

function LuaFightingSpiritMainRaceRoot:OnDisable()
    EventManager.RemoveListenerInternal(EnumEventType.FightingSpiritMatchRecordURLDownloadComplete, self.m_UrlDownloadedAction)
    EventManager.RemoveListenerInternal(EnumEventType.GameVideoDownloaded, self.m_VideoDownloadedAction)
    EventManager.RemoveListenerInternal(EnumEventType.GameVideoDownloadStart, self.m_VideoDownloadStartAction)
    EventManager.RemoveListenerInternal(EnumEventType.GameVideoRemoved, self.m_VideoRemovedAction)
end

--@region UIEvent

function LuaFightingSpiritMainRaceRoot:OnDeleteBtnClick()
    if self.m_MatchRecordData then
        for i =0, self.m_MatchRecordData.Count-1 do
            local matchData = self.m_MatchRecordData[i]
            CFightingSpiritMgr.Instance:DeleteReplayFile(matchData)
        end
    end
end

--@endregion UIEvent

