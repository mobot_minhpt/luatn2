require("common/common_include")
require("ui/lingshou/LuaLingShouBabySkillSlot")
require("ui/lingshou/LuaLingShouMgr")

local LuaGameObject=import "LuaGameObject"
local ArrowUpSprite= "common_fight_rank_up"
local ArrowDownSprite = "common_fight_rank_down"
local Skill_AllSkills=import "L10.Game.Skill_AllSkills"
local MessageMgr=import "L10.Game.MessageMgr"
local Flip=import "UIBasicSprite+Flip"

CLuaLingShouBabyWashResultView =class()
RegistClassMember(CLuaLingShouBabyWashResultView,"m_PinzhiSprite")
RegistClassMember(CLuaLingShouBabyWashResultView,"m_ModelTexture")
-- RegistClassMember(CLuaLingShouBabyWashResultView,"m_ZhandouliLabel")
RegistClassMember(CLuaLingShouBabyWashResultView,"m_SkillSlot1")
RegistClassMember(CLuaLingShouBabyWashResultView,"m_SkillSlot2")
-- RegistClassMember(CLuaLingShouBabyWashResultView,"m_MarkSprite")--提升还是降低

RegistClassMember(CLuaLingShouBabyWashResultView,"m_SkillDescLabel1")
RegistClassMember(CLuaLingShouBabyWashResultView,"m_SkillDescLabel2")
-- RegistClassMember(CLuaLingShouBabyWashResultView,"m_GameObject")
RegistClassMember(CLuaLingShouBabyWashResultView,"m_TitleLabel")

RegistClassMember(CLuaLingShouBabyWashResultView,"m_Display")
RegistClassMember(CLuaLingShouBabyWashResultView,"m_AdjHpLabel")
RegistClassMember(CLuaLingShouBabyWashResultView,"m_ArrowSprite")

function CLuaLingShouBabyWashResultView:ctor()
end

function CLuaLingShouBabyWashResultView:Init( tf )
    -- self.m_GameObject=tf.gameObject
    self.m_TitleLabel=LuaGameObject.GetChildNoGC(tf,"TitleLabel").label
    self.m_Display=LuaGameObject.GetChildNoGC(tf,"Display").gameObject
    self.m_PinzhiSprite=LuaGameObject.GetChildNoGC(tf,"PinZhi").sprite
    self.m_ModelTexture=LuaGameObject.GetChildNoGC(tf,"ModelTexture").lingShouBabyTextureLoader
    -- self.m_ZhandouliLabel=LuaGameObject.GetChildNoGC(tf,"ZhandouliLabel").label
    self.m_AdjHpLabel=LuaGameObject.GetChildNoGC(tf,"AdjHpLabel").label

    self.m_SkillSlot1=CLuaLingShouBabySkillSlot:new()
    self.m_SkillSlot1:Init(LuaGameObject.GetChildNoGC(tf,"Skill1").transform)
    self.m_SkillSlot2=CLuaLingShouBabySkillSlot:new()
    self.m_SkillSlot2:Init(LuaGameObject.GetChildNoGC(tf,"Skill2").transform)

    self.m_SkillDescLabel1=LuaGameObject.GetChildNoGC(tf,"SkillDescLabel1").label
    self.m_SkillDescLabel2=LuaGameObject.GetChildNoGC(tf,"SkillDescLabel2").label

    self.m_ArrowSprite=LuaGameObject.GetChildNoGC(tf,"Arrow1").sprite
    self.m_ArrowSprite.enabled=false
end
function CLuaLingShouBabyWashResultView:SetTitle(title)
    self.m_TitleLabel.text=title
end
function CLuaLingShouBabyWashResultView:InitData(babyInfo)
    if babyInfo and babyInfo.BornTime>0 then
        self.m_Display:SetActive(true)

        local skillId1=CLuaLingShouMgr.ProcessSkillId(babyInfo.Quality,babyInfo.SkillList[1])
        local skillId2=CLuaLingShouMgr.ProcessSkillId(babyInfo.Quality,babyInfo.SkillList[2])
        -- local skillId1=babyInfo.SkillList[1]
        -- local skillId2=babyInfo.SkillList[2]
        --有灵兽
        self:InitSkillSlot1(skillId1)
        self:InitSkillSlot2(skillId2)
        
        self.m_AdjHpLabel.text="+"..tostring(CLuaLingShouMgr.GetLSBBAdjHp(babyInfo.Level,babyInfo.Quality))
        -- self.m_ZhandouliLabel.text=tostring(CLuaLingShouMgr.BabyZhandouliFormula(babyInfo.Level,babyInfo.Quality,babyInfo.SkillList))
        self.m_PinzhiSprite.spriteName=CLuaLingShouMgr.GetBabyQualitySprite(babyInfo.Quality)
        self.m_ModelTexture:Init(babyInfo.TemplateId)
    else
        self.m_Display:SetActive(false)
        self.m_SkillDescLabel1.text=MessageMgr.Inst:FormatMessage("LingShouBaby_Skill1_Condition",{})-- LocalString.GetString("品质[ffd739]>=丁[-]时自动领悟")
        self.m_SkillDescLabel2.text=MessageMgr.Inst:FormatMessage("LingShouBaby_Skill2_Condition",{})--LocalString.GetString("品质[ffd739]>=乙[-]时自动领悟")
        self.m_SkillSlot1:InitData(0)
        self.m_SkillSlot2:InitData(0)
        -- self.m_ZhandouliLabel.text=" "
        self.m_PinzhiSprite.spriteName="lingshou_ji"
        self.m_ModelTexture:InitNull()
        self.m_ArrowSprite.enabled=false
        self.m_AdjHpLabel.text=""
    end    
end
function CLuaLingShouBabyWashResultView:InitSkillSlot1(skillId1)
    if skillId1>0 then
        self.m_SkillSlot1:InitData(skillId1)
        local templateData = Skill_AllSkills.GetData(skillId1)
        if templateData then
            self.m_SkillDescLabel1.text=templateData.Name
        else
            self.m_SkillDescLabel1.text=" "
        end
    else
        self.m_SkillSlot1:InitData(0)
        self.m_SkillDescLabel1.text=MessageMgr.Inst:FormatMessage("LingShouBaby_Skill1_Condition",{})--LocalString.GetString("品质[ffd739]>=乙[-]时自动领悟")
    end
end
function CLuaLingShouBabyWashResultView:InitSkillSlot2(skillId2)
    if skillId2>0 then
        self.m_SkillSlot2:InitData(skillId2)
        local templateData = Skill_AllSkills.GetData(skillId2)
        if templateData then
            self.m_SkillDescLabel2.text=templateData.Name
        else
            self.m_SkillDescLabel2.text=" "
        end
    else
        self.m_SkillSlot2:InitData(0)
        self.m_SkillDescLabel2.text=MessageMgr.Inst:FormatMessage("LingShouBaby_Skill2_Condition",{})--LocalString.GetString("等级[ffd739]>=100[-]时自动领悟")
    end
end

function CLuaLingShouBabyWashResultView:InitResult(babyInfo,babyQuality,babySkillCls1,babySkillCls2)
    -- self:InitData(babyInfo)
    self.m_Display:SetActive(true)

    local skillId1=0
    local level1=1
    if babyInfo.SkillList[1]>0 then
        level1=babyInfo.SkillList[1]%100
    end
    level1=math.max(level1, babyInfo.SavedSkillLv1)

    if babySkillCls1>0 then
        skillId1=level1+babySkillCls1*100
    end
    local skillId2=0
    local level2=1
    if babyInfo.SkillList[2]>0 then
        level2=babyInfo.SkillList[2]%100
    end
    level2=math.max(level2, babyInfo.SavedSkillLv2)
    

    if babySkillCls2>0 then
        skillId2=level2+babySkillCls2*100
    end

    skillId1=CLuaLingShouMgr.ProcessSkillId(babyQuality,skillId1)
    skillId2=CLuaLingShouMgr.ProcessSkillId(babyQuality,skillId2)

    self:InitSkillSlot1(skillId1)
    self:InitSkillSlot2(skillId2)

    local oldVal=CLuaLingShouMgr.GetLSBBAdjHp(babyInfo.Level,babyInfo.Quality)
    local newVal=CLuaLingShouMgr.GetLSBBAdjHp(babyInfo.Level,babyQuality)
    if oldVal>newVal then
        self.m_ArrowSprite.enabled=true
        self.m_ArrowSprite.spriteName=ArrowDownSprite
        self.m_ArrowSprite.flip=Flip.Vertically
    elseif oldVal<newVal then
        self.m_ArrowSprite.enabled=true
        self.m_ArrowSprite.spriteName=ArrowUpSprite
        self.m_ArrowSprite.flip=Flip.Nothing
    else
        self.m_ArrowSprite.enabled=false
    end
    
    self.m_AdjHpLabel.text="+"..tostring(CLuaLingShouMgr.GetLSBBAdjHp(babyInfo.Level,babyQuality))
    self.m_PinzhiSprite.spriteName=CLuaLingShouMgr.GetBabyQualitySprite(babyQuality)
    self.m_ModelTexture:Init(babyInfo.TemplateId)
end
return CLuaLingShouBabyWashResultView