local UILabel = import "UILabel"
local LocalString = import "LocalString"
local CUIManager = import "L10.UI.CUIManager"
local QnButton = import "L10.UI.QnButton"
local DelegateFactory = import "DelegateFactory"
local CHouseCompetitionMgr = import "L10.Game.CHouseCompetitionMgr"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local EnumHouseCompetitionStage = import "L10.Game.EnumHouseCompetitionStage"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Color = import "UnityEngine.Color"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

CLuaHouseCompetitionFanPaiZiAddChancesWnd = class()

RegistClassMember(CLuaHouseCompetitionFanPaiZiAddChancesWnd,"m_DescLab")
RegistClassMember(CLuaHouseCompetitionFanPaiZiAddChancesWnd,"m_ToVoteBtn")
RegistClassMember(CLuaHouseCompetitionFanPaiZiAddChancesWnd,"m_Grid")
RegistClassMember(CLuaHouseCompetitionFanPaiZiAddChancesWnd,"m_TasksGetChancesMax")
RegistClassMember(CLuaHouseCompetitionFanPaiZiAddChancesWnd,"m_TasksProgressMax")
RegistClassMember(CLuaHouseCompetitionFanPaiZiAddChancesWnd,"m_HuoliProgress")
RegistClassMember(CLuaHouseCompetitionFanPaiZiAddChancesWnd,"m_VisitProgress")
RegistClassMember(CLuaHouseCompetitionFanPaiZiAddChancesWnd,"m_JoinProgress")

function CLuaHouseCompetitionFanPaiZiAddChancesWnd:Awake()
    self:InitComponents()
end

function CLuaHouseCompetitionFanPaiZiAddChancesWnd:InitComponents()
    self.m_DescLab = self.transform:Find("Anchor/Offset/Top/TopLeft/LeftLab2"):GetComponent(typeof(UILabel))
    self.m_ToVoteBtn = self.transform:Find("Anchor/Offset/Top/TopRight/ToVoteBtn"):GetComponent(typeof(QnButton))
    self.m_Grid = self.transform:Find("Anchor/Offset/Bottom/Content/Grid").gameObject

    self.m_TasksGetChancesMax = {}
    self.m_TasksProgressMax = {}
    self.m_HuoliProgress = 0
    self.m_VisitProgress = 0
    self.m_JoinProgress = 0
end

function CLuaHouseCompetitionFanPaiZiAddChancesWnd:Init()
    self.m_DescLab.text = g_MessageMgr:FormatMessage("HOUSE_COMPETITION_FANPAIZI_ADDCHOICE_DESC")
    self.m_ToVoteBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        CLuaWebBrowserMgr.OpenHouseActivityUrl("JiaYuan2022List")--投票入口
    end)

    self:UpdateHuoliProgress()
    self:UpdateVisitHouseProgress()
    --复赛则不查询是否参赛
    local stage = CHouseCompetitionMgr.Inst:GetCurrentStage()
    if stage == EnumHouseCompetitionStage.eChusai or stage == EnumHouseCompetitionStage.eUnknown then
        self:UpdateJoinProgress()
    end

    self:InitTasksInfo()
    self:UpdateTasks()
end

function CLuaHouseCompetitionFanPaiZiAddChancesWnd:UpdateTasks()
    local toScheduleAction = DelegateFactory.Action_QnButton(function (btn)
        CUIManager.ShowUI(CLuaUIResources.ScheduleWnd)

        --活动还得慢慢做,帮玩家关一下界面
        self:TryClose()
    end)

    local toHousesAction = DelegateFactory.Action_QnButton(function (btn)
        Gac2Gas.RequestOpenHouseCompetitionWnd()

        --拜访家园可能需要多做几次,也帮玩家关一下界面
        self:TryClose()
    end)

    local toRuleAction = DelegateFactory.Action_QnButton(function (btn)
        CLuaWebBrowserMgr.OpenHouseActivityUrl("JiaYuan2022Rule")--更多入口
    end)

    local toSignUpAction = DelegateFactory.Action_QnButton(function (btn)
        CLuaWebBrowserMgr.OpenHouseActivityUrl("JiaYuan2022Apply")--报名入口
    end)
    for index = 1, 5 do
        local item = self.m_Grid.transform:GetChild(index-1)

        local nameLab = item.transform:Find("NameLab"):GetComponent(typeof(UILabel))
        local limitLab = item.transform:Find("LimitLab"):GetComponent(typeof(UILabel))
        local progressLab = item.transform:Find("ProgressLab"):GetComponent(typeof(UILabel))
        local btn = item.transform:Find("Btn"):GetComponent(typeof(QnButton))

        local progress = 0
        if index == 1 then
            nameLab.text = System.String.Format(LocalString.GetString("游戏活力满{0}"), self.m_TasksProgressMax[index])
            limitLab.text = System.String.Format(LocalString.GetString("{0}票/天"), self.m_TasksGetChancesMax[index])
            btn.OnClick = toScheduleAction
            progress = self.m_HuoliProgress
        elseif index == 2 then
            nameLab.text = System.String.Format(LocalString.GetString("游戏活力满{0}"), self.m_TasksProgressMax[index])
            limitLab.text = System.String.Format(LocalString.GetString("{0}票/天"), self.m_TasksGetChancesMax[index])
            btn.OnClick = toScheduleAction
            progress = self.m_HuoliProgress
        elseif index == 3 then
            limitLab.text = System.String.Format(LocalString.GetString("{0}票/天"), self.m_TasksGetChancesMax[index])
            btn.OnClick = toHousesAction
            progress = self.m_VisitProgress
        elseif index == 4 then
            limitLab.text = System.String.Format(LocalString.GetString("{0}票"), self.m_TasksGetChancesMax[index])
            btn.OnClick = toSignUpAction
            progress = self.m_JoinProgress
        else
            limitLab.text = LocalString.GetString("—")
            progressLab.text = LocalString.GetString("—")
            btn.OnClick = toRuleAction
        end

        local ProgressMax = self.m_TasksProgressMax[index]
        if type(progress) == "boolean" then
            if progress == true then
                progress = 1
            else
                progress = 0
            end
        end
        if ProgressMax ~= nil then
            if progress >= ProgressMax then
                progressLab.text = ProgressMax .. "/" .. ProgressMax
                progressLab.color = Color.green
                btn.Enabled = false
                btn.Text = LocalString.GetString("已获得")
            else
                progressLab.text = progress .. "/" .. ProgressMax
                progressLab.color = Color.white
                btn.Enabled = true
                btn.Text = LocalString.GetString("前 往")
            end
        end

        --非复赛置灰参观复赛家园
        if index == 3 and  CHouseCompetitionMgr.Inst:GetCurrentStage() ~= EnumHouseCompetitionStage.eFusai then
            btn.Enabled = false
        end

        --复赛置灰参加比赛并返回(因为进度失效需要完全初始化)
        if index == 4 and CHouseCompetitionMgr.Inst:GetCurrentStage() == EnumHouseCompetitionStage.eFusai then
            progressLab.text = LocalString.GetString("—")
            progressLab.color = Color.white
            btn.Enabled = false
            btn.Text = LocalString.GetString("前 往")
        end
    end
end

function CLuaHouseCompetitionFanPaiZiAddChancesWnd:InitTasksInfo()
    local huoliVoteThresh = HouseCompetition_Setting.GetData().HuoliVoteThresh

    local task1Info = huoliVoteThresh[0]
    self.m_TasksProgressMax[1] = task1Info[1]
    self.m_TasksGetChancesMax[1] = task1Info[2]

    local task2Info = huoliVoteThresh[1]
    self.m_TasksProgressMax[2] = task2Info[1]
    self.m_TasksGetChancesMax[2] = task2Info[2]

    self.m_TasksProgressMax[3] = 1
    self.m_TasksGetChancesMax[3] = HouseCompetition_Setting.GetData().FanPaiZi_VisitHome_Votes

    self.m_TasksProgressMax[4] = 1
    self.m_TasksGetChancesMax[4] = HouseCompetition_Setting.GetData().FanPaiZi_Signup_Votes
end

function CLuaHouseCompetitionFanPaiZiAddChancesWnd:TryClose()
    if CUIManager.IsLoaded(CLuaUIResources.HouseCompetitionFanPaiZiWnd) then
        CUIManager.CloseUI(CLuaUIResources.HouseCompetitionFanPaiZiWnd)
    end
    if CUIManager.IsLoaded(CLuaUIResources.HouseCompetitionFanPaiZiAddChancesWnd) then
        CUIManager.CloseUI(CLuaUIResources.HouseCompetitionFanPaiZiAddChancesWnd)
    end
end

function CLuaHouseCompetitionFanPaiZiAddChancesWnd:UpdateHuoliProgress()
    if not CClientMainPlayer.Inst then
        return
    end
    local dayHuoli = CClientMainPlayer.Inst.PlayProp.DayHuoLi
    local expireTime = CClientMainPlayer.Inst.PlayProp.DayHuoLiExpireTime
    if (CServerTimeMgr.Inst.timeStamp < expireTime) then
        self.m_HuoliProgress = dayHuoli
    else
        self.m_HuoliProgress = 0
    end
end

function CLuaHouseCompetitionFanPaiZiAddChancesWnd:UpdateVisitHouseProgress()
    self.m_VisitProgress = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(CLuaHouseCompetitionMgr.eHouseCompetitionEnterPlayGetVoteTimes)
end

function CLuaHouseCompetitionFanPaiZiAddChancesWnd:UpdateJoinProgress()
    Gac2Gas.QueryHasRegisteredChusai()
end

function CLuaHouseCompetitionFanPaiZiAddChancesWnd:OnSendHouseCompetitionData()
    self.m_JoinProgress = CHouseCompetitionMgr.Inst.RegisteredChusai
    self:RefreshTaskState(4, self.m_JoinProgress)
end

function CLuaHouseCompetitionFanPaiZiAddChancesWnd:OnUpdateTempPlayTimesWithKey(args)
    local key = args[0]
    if key == CLuaHouseCompetitionMgr.eHouseCompetitionEnterPlayGetVoteTimes then
        self.m_VisitProgress = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(CLuaHouseCompetitionMgr.eHouseCompetitionEnterPlayGetVoteTimes)
        self:RefreshTaskState(3, self.m_VisitProgress)
    end
end

function CLuaHouseCompetitionFanPaiZiAddChancesWnd:RefreshTaskState(index, progress)
    local item = self.m_Grid.transform:GetChild(index - 1)
    local progressLab = item.transform:Find("ProgressLab"):GetComponent(typeof(UILabel))
    local btn = item.transform:Find("Btn"):GetComponent(typeof(QnButton))

    local ProgressMax = self.m_TasksProgressMax[index]
    if type(progress) == "boolean" then
        if progress == true then
            progress = 1
        else
            progress = 0
        end
    end

    if ProgressMax ~= nil then
        if progress >= ProgressMax then
            progressLab.text = ProgressMax .. "/" .. ProgressMax
            progressLab.color = Color.green
            btn.Enabled = false
            btn.Text = LocalString.GetString("已获得")
        else
            progressLab.text = progress .. "/" .. ProgressMax
            progressLab.color = Color.white
            btn.Enabled = true
            btn.Text = LocalString.GetString("前 往")
        end
    end
end

function CLuaHouseCompetitionFanPaiZiAddChancesWnd:UpdateDayHuoli()
    self:UpdateHuoliProgress()
    self:RefreshTaskState(1, self.m_HuoliProgress)
    self:RefreshTaskState(2, self.m_HuoliProgress)
end

function CLuaHouseCompetitionFanPaiZiAddChancesWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSendHouseCompetitionData", self, "OnSendHouseCompetitionData")
    g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
    g_ScriptEvent:AddListener("PlayerUpdateDayHuoLi", self, "UpdateDayHuoli")
end

function CLuaHouseCompetitionFanPaiZiAddChancesWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSendHouseCompetitionData", self, "OnSendHouseCompetitionData")
    g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
    g_ScriptEvent:RemoveListener("PlayerUpdateDayHuoLi", self, "UpdateDayHuoli")
end
