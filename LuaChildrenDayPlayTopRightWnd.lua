local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CUIResources = import "L10.UI.CUIResources"
local CTopAndRightTipWnd=import "L10.UI.CTopAndRightTipWnd"


LuaChildrenDayPlayTopRightWnd = class()
RegistChildComponent(LuaChildrenDayPlayTopRightWnd,"rightBtn", GameObject)
RegistChildComponent(LuaChildrenDayPlayTopRightWnd,"info", GameObject)
RegistChildComponent(LuaChildrenDayPlayTopRightWnd,"info1", GameObject)

RegistClassMember(LuaChildrenDayPlayTopRightWnd, "team1Label")
RegistClassMember(LuaChildrenDayPlayTopRightWnd, "team2Label")

function LuaChildrenDayPlayTopRightWnd:Close()
	--CUIManager.CloseUI(CLuaUIResources.)
end

function LuaChildrenDayPlayTopRightWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateChildrenDayPlayForceScoreInfo", self, "UpdateInfo")
end

function LuaChildrenDayPlayTopRightWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateChildrenDayPlayForceScoreInfo", self, "UpdateInfo")
end

function LuaChildrenDayPlayTopRightWnd:Init()
	UIEventListener.Get(self.rightBtn).onClick=LuaUtils.VoidDelegate(function(go)
		LuaUtils.SetLocalRotation(self.rightBtn.transform,0,0,0)
		CTopAndRightTipWnd.showPackage = false
		CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
	end)
	--LuaChildrenDay2019Mgr.PengPengCheForceScoreInfo = {force1 = force1Score,force2= force2Score}
  --g_ScriptEvent:BroadcastInLua('UpdateChildrenDayPlayForceScoreInfo')

	self.team1Label = self.info.transform:Find('alive'):GetComponent(typeof(UILabel))
	self.team2Label = self.info1.transform:Find('alive'):GetComponent(typeof(UILabel))
	self.team1Label.text = '0'
	self.team2Label.text = '0'

	self.info.transform:Find('pNode').gameObject:SetActive(false)
	self.info1.transform:Find('pNode').gameObject:SetActive(false)
end

function LuaChildrenDayPlayTopRightWnd:UpdateInfo()
 if LuaChildrenDay2019Mgr.PengPengCheForceScoreInfo then
	 self.team1Label.text = LuaChildrenDay2019Mgr.PengPengCheForceScoreInfo.force1
	 self.team2Label.text = LuaChildrenDay2019Mgr.PengPengCheForceScoreInfo.force2
	 if LuaChildrenDay2019Mgr.PengPengCheForceScoreInfo.selfForce == 1 then
		 self.info.transform:Find('pNode').gameObject:SetActive(true)
		 self.info1.transform:Find('pNode').gameObject:SetActive(false)
	 elseif LuaChildrenDay2019Mgr.PengPengCheForceScoreInfo.selfForce == 2 then
		 self.info.transform:Find('pNode').gameObject:SetActive(false)
		 self.info1.transform:Find('pNode').gameObject:SetActive(true)
	 end
 end
end

return LuaChildrenDayPlayTopRightWnd
