LuaAccountTransferMgr = {}
LuaAccountTransferMgr.m_IsAccountTransfer = false
LuaAccountTransferMgr.m_Tick = nil

function LuaAccountTransferMgr:ShowMessageOnServerWnd()
    self:CancelTick()
    self.m_IsAccountTransfer = true
    self.m_Tick = RegisterTick(function ()
        if CUIManager.IsLoaded(CUIResources.ServerWnd) then
            self:CancelTick()
            g_MessageMgr:ShowMessage("AccountTransfer_SuccessTip")
        end
    end,1000)
end

function LuaAccountTransferMgr:CancelTick()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end

function LuaAccountTransferMgr:OnMainPlayerCreated()
    self.m_IsAccountTransfer = false
    self:CancelTick()
end