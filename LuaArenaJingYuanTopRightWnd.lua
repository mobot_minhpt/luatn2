local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CScene = import "L10.Game.CScene"

LuaArenaJingYuanTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaArenaJingYuanTopRightWnd, "rightBtn", GameObject)
RegistChildComponent(LuaArenaJingYuanTopRightWnd, "RemainTimeLabel", UILabel)

RegistClassMember(LuaArenaJingYuanTopRightWnd, "m_TeamInfo")
RegistClassMember(LuaArenaJingYuanTopRightWnd, "m_WinJingYuanCount")

--@endregion RegistChildComponent end

function LuaArenaJingYuanTopRightWnd:Awake()
    UIEventListener.Get(self.rightBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightBtnClick()
	end)
end

function LuaArenaJingYuanTopRightWnd:Init()
    self.m_WinJingYuanCount = Arena_JingYuanPlaySetting.GetData().WinJingYuanCount
    self.m_TeamInfo = {}
    for i = 1, 4 do
        local teamNode = self.transform:Find("Anchor/tipPanel/Table/Team" .. tostring(i))
        local teamInfo = {}
        teamInfo.CountLabel = teamNode:Find("CountLabel"):GetComponent(typeof(UILabel))
        teamInfo.CountSlider = teamNode:Find("CountSlider"):GetComponent(typeof(UISlider))
        table.insert(self.m_TeamInfo, teamInfo)
    end
    self:OnSyncArenaTeamJingYuanInfo()
end

function LuaArenaJingYuanTopRightWnd:OnSyncArenaTeamJingYuanInfo()
    local infoTbl = LuaArenaJingYuanMgr.m_TeamJingYuanInfo
    for i = 1, 4 do
        local teamInfo = self.m_TeamInfo[i]
        local jingyuanCount = infoTbl and infoTbl[i] and infoTbl[i].jingyuanCount or 0
        teamInfo.CountLabel.text = tostring(jingyuanCount)
        teamInfo.CountSlider.value = jingyuanCount / self.m_WinJingYuanCount
    end
end

function LuaArenaJingYuanTopRightWnd:OnRemainTimeUpdate()
    local mainScene = CScene.MainScene
    if mainScene then
        self.RemainTimeLabel.text = LuaGamePlayMgr:GetRemainTimeText(mainScene.ShowTime)
    end
end

--@region UIEvent
function LuaArenaJingYuanTopRightWnd:OnRightBtnClick()
    self.rightBtn.transform.localEulerAngles = Vector3(0, 0, 0)
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaArenaJingYuanTopRightWnd:OnHideTopAndRightTipWnd()
    self.rightBtn.transform.localEulerAngles = Vector3(0, 0, -180)
end

--@endregion UIEvent

function LuaArenaJingYuanTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SyncArenaTeamJingYuanInfo",self,"OnSyncArenaTeamJingYuanInfo")
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
end

function LuaArenaJingYuanTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncArenaTeamJingYuanInfo",self,"OnSyncArenaTeamJingYuanInfo")
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
end