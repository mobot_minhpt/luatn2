local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnCheckBox = import "L10.UI.QnCheckBox"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local Extensions = import "Extensions"
local CBaseWnd = import "L10.UI.CBaseWnd"
local NGUIMath = import "NGUIMath"
local UIRect = import "UIRect"
local UIWidget = import "UIWidget"

LuaCheckboxPopupMenu = class()

RegistClassMember(LuaCheckboxPopupMenu,"m_Bg")
RegistClassMember(LuaCheckboxPopupMenu,"m_ScrollView")
RegistClassMember(LuaCheckboxPopupMenu,"m_Table")
RegistClassMember(LuaCheckboxPopupMenu,"m_ScrollViewIndicator")
RegistClassMember(LuaCheckboxPopupMenu,"m_TitleTemplate")
RegistClassMember(LuaCheckboxPopupMenu,"m_CheckboxTemplate")
RegistClassMember(LuaCheckboxPopupMenu,"m_MinHeight")

function LuaCheckboxPopupMenu:Awake()
    self.m_Bg = self.transform:Find("Anchor/Bg"):GetComponent(typeof(UIWidget))
    self.m_ScrollView = self.transform:Find("Anchor/Bg/ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_Table = self.transform:Find("Anchor/Bg/ScrollView/Table"):GetComponent(typeof(UITable))
    self.m_ScrollViewIndicator = self.transform:Find("Anchor/Bg/ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator))
    self.m_TitleTemplate = self.transform:Find("Anchor/Bg/Templates/TitleTemplate").gameObject
    self.m_CheckboxTemplate = self.transform:Find("Anchor/Bg/Templates/Checkbox").gameObject
    self.m_TitleTemplate:SetActive(false)
    self.m_CheckboxTemplate:SetActive(false)
    self.m_MinHeight = 200
end

function LuaCheckboxPopupMenu:Init()
    local info = g_PopupMenuMgr.m_CheckboxPopupMenuInfo
    Extensions.RemoveAllChildren(self.m_Table.transform)
    if info.title ~= nil then
        local titleGo = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_TitleTemplate)
        titleGo:SetActive(true)
        titleGo:GetComponent(typeof(UILabel)).text = info.title
    end

    for __, item in pairs(info.itemTbl) do
        local checkboxGo = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_CheckboxTemplate)
        checkboxGo:SetActive(true)
        local cb = checkboxGo:GetComponent(typeof(QnCheckBox))
        cb.Text = item.text
        local action = item.action
        cb.OnValueChanged = DelegateFactory.Action_bool(function(selected)
            if action then
                action(selected)
            end
        end)
        cb:SetSelected(item.defaultSelected, true)
    end
    self.m_Table:Reposition()
    self:LayoutContent(info)
    self:AdjustPosition(info)
end

function LuaCheckboxPopupMenu:LayoutContent(info)
    local size = CUICommonDef.GetVirtualScreenSize()
    local virtualScreenWidth, virtualScreenHeight = size.x, size.y
    local contentHeight = NGUIMath.CalculateRelativeWidgetBounds(self.m_Table.transform).size.y + self.m_Table.padding.y * 2
    local contentPadding = math.abs(self.m_ScrollView.panel.topAnchor.absolute) + math.abs(self.m_ScrollView.panel.bottomAnchor.absolute) + self.m_ScrollView.panel.clipSoftness.y * 2 + 1
    local displayWndHeight =  math.min(math.max(contentHeight + contentPadding, self.m_MinHeight), virtualScreenHeight) 
    self.m_Bg.width = info.menuWidth --预览扩展，将来有需要改变宽度再处理
    self.m_Bg.height = math.ceil(displayWndHeight)

    local rects = CommonDefs.GetComponentsInChildren_Component_Type_Boolean(self.transform, typeof(UIRect), true)
    for i=0, rects.Length-1 do
        if rects[i].updateAnchors == UIRect.AnchorUpdate.OnEnable or rects[i].updateAnchors == UIRect.AnchorUpdate.OnStart then
            rects[i]:ResetAndUpdateAnchors()
        end
    end
    self.m_ScrollView:ResetPosition()
    self.m_ScrollViewIndicator:Layout()
end

function LuaCheckboxPopupMenu:AdjustPosition(info)
    local size = CUICommonDef.GetVirtualScreenSize()
    local virtualScreenWidth, virtualScreenHeight = size.x, size.y
    local bgHalfWidth, bgHalfHeight = self.m_Bg.localSize.x*0.5, self.m_Bg.localSize.y*0.5
    --注意Bg中心为枢点
    local localPos = self.m_Bg.transform.parent:InverseTransformPoint(info.nguiWorldPos)
    local targetPos = localPos
    if info.alignType == EnumCheckboxPopupMenuAlginType.Left then
        targetPos = Vector3(localPos.x - bgHalfWidth, localPos.y, targetPos.z)
    elseif info.alignType == EnumCheckboxPopupMenuAlginType.Right then
        targetPos = Vector3(localPos.x + bgHalfWidth, localPos.y, targetPos.z)
    elseif info.alignType == EnumCheckboxPopupMenuAlginType.Top then
        targetPos = Vector3(localPos.x, localPos.y + bgHalfHeight, targetPos.z)
    elseif info.alignType == EnumCheckboxPopupMenuAlginType.Bottom then
        targetPos = Vector3(localPos.x, localPos.y - bgHalfHeight, targetPos.z)
    end

    local xMax, xMin = virtualScreenWidth * 0.5 - bgHalfWidth, -virtualScreenWidth * 0.5 + bgHalfWidth
    local yMax, yMin = virtualScreenHeight * 0.5 - bgHalfHeight, -virtualScreenHeight * 0.5 + bgHalfHeight
    
    --左右不超出屏幕范围
    if targetPos.x < xMin then targetPos.x = xMin end
    if targetPos.x > xMax then  targetPos.x = xMax end
    --上下不超出屏幕范围
    if targetPos.y < yMin then targetPos.y = yMin end
    if targetPos.y > yMax then targetPos.y = yMax end 
    self.m_Bg.transform.localPosition = targetPos
end

function LuaCheckboxPopupMenu:Update()
    self.gameObject:GetComponent(typeof(CBaseWnd)):ClickThroughToClose()
end

