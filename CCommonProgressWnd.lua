-- Auto Generated!!
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonProgressInfoMgr = import "L10.UI.CCommonProgressInfoMgr"
local CCommonProgressWnd = import "L10.UI.CCommonProgressWnd"
local ChuanjiabaoModel_Setting = import "L10.Game.ChuanjiabaoModel_Setting"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CThumbnailChatWnd = import "L10.UI.CThumbnailChatWnd"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CYuanDanMgr = import "L10.Game.CYuanDanMgr"
local GameSetting_Client = import "L10.Game.GameSetting_Client"
local GameSetting_Common = import "L10.Game.GameSetting_Common"
local House_Setting = import "L10.Game.House_Setting"
local ProgressType = import "L10.UI.CCommonProgressInfoMgr+ProgressType"
local SoundManager = import "SoundManager"
local Time = import "UnityEngine.Time"
local UIRoot = import "UIRoot"
local UISprite = import "UISprite"
local UIWidget = import "UIWidget"
local ZuoQi_Setting = import "L10.Game.ZuoQi_Setting"
LuaCommonProgressWndMgr ={}
LuaCommonProgressWndMgr.m_ShowCloseBtn = false
LuaCommonProgressWndMgr.m_OnCloseButtonClickAction = nil
CCommonProgressWnd.m_hookOnDisable  = function (this) 
    LuaCommonProgressWndMgr.m_OnCloseButtonClickAction = nil
    LuaCommonProgressWndMgr.m_ShowCloseBtn = false
end
CCommonProgressWnd.m_Init_CS2LuaHook = function (this) 

    this.textLabel.text = nil
    this.progressBar.value = 0
    this.closeBtn:SetActive(LuaCommonProgressWndMgr.m_ShowCloseBtn)
    this.showInverseProgress = CCommonProgressInfoMgr.m_ShowInverseProgress
    local sprite = TypeAs(this.progressBar.foregroundWidget, typeof(UISprite))
    local default
    if this.showInverseProgress then
        default = this.InverseProgressSpriteName
    else
        default = this.ProgressSpriteName
    end
    sprite.spriteName = default
    sprite.width = this.showInverseProgress and this.InverseProgressSpriteWidth or this.ProgressSpriteWidth
    repeat
        local extern = CCommonProgressInfoMgr.CurProgressType
        if extern == CCommonProgressInfoMgr.ProgressType.YuanDanHatching then
            this.textLabel.text = CCommonProgressInfoMgr.m_DisplayName
            this.closeBtn:SetActive(true)
            break
        elseif extern == CCommonProgressInfoMgr.ProgressType.Undefined then
            this.duration = - 1
            break
        else
            this.startTime = Time.realtimeSinceStartup
            this.duration = CCommonProgressInfoMgr.m_DisplayTime
            this.textLabel.text = CCommonProgressInfoMgr.m_DisplayName
            break
        end
    until 1
    if not (CCommonProgressInfoMgr.CurProgressType == CCommonProgressInfoMgr.ProgressType.DuZouCasting) and not (CCommonProgressInfoMgr.CurProgressType == CCommonProgressInfoMgr.ProgressType.HeZouCasting) then
        SoundManager.Inst:StartProgressSound()
    end
end
CCommonProgressWnd.m_IPhoneXAdaptation_CS2LuaHook = function (this) 
    if UIRoot.EnableIPhoneXAdaptation then
        local w = CommonDefs.GetComponent_Component_Type(this.transform:Find("Anchor"), typeof(UIWidget))
        if w == nil then
            return
        end
        w.bottomAnchor.absolute = 490 + CThumbnailChatWnd.GetHeightOffsetForIphoneX()
        w.topAnchor.absolute = 590 + CThumbnailChatWnd.GetHeightOffsetForIphoneX()
        w:ResetAndUpdateAnchors()
    end
end
CCommonProgressWnd.m_Update_CS2LuaHook = function (this) 

    if CCommonProgressInfoMgr.m_UseServerTime then
        this:UpdateProgressByServerTime()
        return
    end

    if this.duration > 0 then
        if this.showInverseProgress then
            this.progressBar.value = math.max(0, 1 - (Time.realtimeSinceStartup - this.startTime) / this.duration)
        else
            this.progressBar.value = (Time.realtimeSinceStartup - this.startTime) / this.duration
        end
        if Time.realtimeSinceStartup - this.startTime >= this.duration then
            this.duration = - 1
        else
            return
        end
    else
        CCommonProgressInfoMgr.CloseProgressWnd()
    end
end
CCommonProgressWnd.m_UpdateProgressByServerTime_CS2LuaHook = function (this) 
    local totalTime = CCommonProgressInfoMgr.m_ServerEndTime - CCommonProgressInfoMgr.m_ServerStartTime
    local progress = CServerTimeMgr.Inst.timeStamp - CCommonProgressInfoMgr.m_ServerStartTime
    if progress < 0 then
        progress = 0
    end
    if totalTime > progress then
        if this.showInverseProgress then
            this.progressBar.value = math.max(0, 1 - (progress / totalTime))
        else
            this.progressBar.value = (progress / totalTime)
        end
    else
        CCommonProgressInfoMgr.CloseProgressWnd()
    end
end
CCommonProgressWnd.m_OnCloseButtonClick_CS2LuaHook = function (this, go)
    if LuaCommonProgressWndMgr.m_OnCloseButtonClickAction and LuaCommonProgressWndMgr.m_ShowCloseBtn then
        LuaCommonProgressWndMgr.m_OnCloseButtonClickAction()
    end 
    if CCommonProgressInfoMgr.CurProgressType == CCommonProgressInfoMgr.ProgressType.YuanDanHatching then
        CYuanDanMgr.Inst:RequestGiveUpHatching()
    else
        CCommonProgressInfoMgr.CloseCommonProgressWndByType(CCommonProgressInfoMgr.CurProgressType)
    end
end

CCommonProgressInfoMgr.m_ShowProgressWnd_Item_Item_CS2LuaHook = function (item) 
    CCommonProgressInfoMgr.ShowCommonProgressWnd(item.CastingDesc, item.Casting, ProgressType.ItemCasting)
    if item.Type == EnumItemType_lua.Map or item.Type == EnumItemType_lua.PuzzleMap then
        CUIManager.CloseUI(CUIResources.DigTreasureWnd)
    end
end
CCommonProgressInfoMgr.m_ShowChuanjiabaoShaozhiProgressWnd_CS2LuaHook = function () 
    local duration = ChuanjiabaoModel_Setting.GetData().ShaoZhiDuration
    local name = CClientHouseMgr.ShaozhiChuanjiabaoTxt
    CCommonProgressInfoMgr.ShowCommonProgressWnd(name, duration, ProgressType.ChuanjiabaoShaozhi)
end
CCommonProgressInfoMgr.m_ShowSummonLingShouProgressWnd_CS2LuaHook = function () 
    local duration = CLingShouMgr.Inst.castingDuration
    local name = CLingShouMgr.Inst.castingText
    CCommonProgressInfoMgr.ShowCommonProgressWnd(name, duration, ProgressType.LingShouCasting)
end
CCommonProgressInfoMgr.m_TeleportShowProgressWnd_CS2LuaHook = function () 
    local setting_common = GameSetting_Common.GetData()
    local duration = setting_common.TELEPORTING_CASTING_TIME
    local setting_Client = GameSetting_Client.GetData()
    local name = setting_Client.TELEPORTING_CASTING_DESC
    CCommonProgressInfoMgr.ShowCommonProgressWnd(name, duration, ProgressType.TeleportCasting)
end
CCommonProgressInfoMgr.m_StealCropShowProgressWnd_CS2LuaHook = function () 
    local duration = House_Setting.GetData().Steal_Spell_Time
    local name = House_Setting.GetData().StealCropTxt
    CCommonProgressInfoMgr.ShowCommonProgressWnd(name, duration, ProgressType.StealCrop)
end
CCommonProgressInfoMgr.m_FenXianTeleportShowProgressWnd_CS2LuaHook = function () 
    local setting_common = GameSetting_Common.GetData()
    local duration = setting_common.FenXian_CASTING_TIME
    local setting_Client = GameSetting_Client.GetData()
    local name = setting_Client.TELEPORTING_CASTING_DESC
    CCommonProgressInfoMgr.ShowCommonProgressWnd(name, duration, ProgressType.FenXianTeleportCasting)
end
CCommonProgressInfoMgr.m_FastTeleportShowProgressWnd_CS2LuaHook = function () 
    local setting_common = GameSetting_Common.GetData()
    local duration = setting_common.FastTeleport_CASTING_TIME
    local setting_Client = GameSetting_Client.GetData()
    local name = setting_Client.TELEPORTING_CASTING_DESC
    CCommonProgressInfoMgr.ShowCommonProgressWnd(name, duration, ProgressType.FastTeleportCasting)
end
CCommonProgressInfoMgr.m_ZuoQiShowProgressWnd_CS2LuaHook = function () 
    local duration = ZuoQi_Setting.GetData().RideOn_Cast_Duration
    local name = ZuoQi_Setting.GetData().RideOn_Cast_Desc
    CCommonProgressInfoMgr.ShowCommonProgressWnd(name, duration, ProgressType.ZuoQiCasting)
end
CCommonProgressInfoMgr.m_CloseProgressWnd_CS2LuaHook = function () 
    CCommonProgressInfoMgr.CurProgressType = ProgressType.Undefined
    CUIManager.CloseUI(CIndirectUIResources.CommonProgressWnd)
    if CClientMainPlayer.Inst ~= nil then
        CClientMainPlayer.Inst:RmTeleportFx()
    end
end
CCommonProgressInfoMgr.m_ShowCommonProgressWnd_CS2LuaHook = function (displayName, displayTime, type) 
    CCommonProgressInfoMgr.CurProgressType = type
    CCommonProgressInfoMgr.m_DisplayName = displayName
    CCommonProgressInfoMgr.m_DisplayTime = displayTime
    CCommonProgressInfoMgr.m_UseServerTime = false
    CCommonProgressInfoMgr.m_ShowInverseProgress = (type == ProgressType.SkillLeading)
    CUIManager.ShowUI(CIndirectUIResources.CommonProgressWnd)
end
CCommonProgressInfoMgr.m_ShowCommonProgressWndByServerTime_CS2LuaHook = function (displayName, startTime, endTime, type) 
    CCommonProgressInfoMgr.CurProgressType = type
    CCommonProgressInfoMgr.m_DisplayName = displayName
    CCommonProgressInfoMgr.m_ServerStartTime = startTime
    CCommonProgressInfoMgr.m_ServerEndTime = endTime
    if CCommonProgressInfoMgr.m_ServerEndTime <= CCommonProgressInfoMgr.m_ServerStartTime then
        return
    end
    CCommonProgressInfoMgr.m_UseServerTime = true
    CCommonProgressInfoMgr.m_ShowInverseProgress = (type == ProgressType.SkillLeading)
    CUIManager.ShowUI(CIndirectUIResources.CommonProgressWnd)
end
CCommonProgressInfoMgr.m_CloseCommonProgressWndByType_CS2LuaHook = function (type) 
    if CCommonProgressInfoMgr.CurProgressType == type then
        CCommonProgressInfoMgr.CloseProgressWnd()
    end
end

CCommonProgressInfoMgr.m_hookShowProgressWnd1 = function(skill)
    local casting = skill.Casting
    local lifeSkillType = skill.Type
    casting = CLuaXianzhiMgr.GetXianZhiLifeSkillCastTime(casting, lifeSkillType)
    CCommonProgressInfoMgr.ShowCommonProgressWnd(skill.Name, casting, ProgressType.LifeSkillCasting);
end
