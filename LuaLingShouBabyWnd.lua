require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local LingShouBaby_BabyTemplate=import "L10.Game.LingShouBaby_BabyTemplate"
require("ui/lingshou/baby/LuaLingShouBabyPropertyDetailView")
require("ui/lingshou/baby/LuaLingShouBabySkillDetailView")
require("ui/lingshou/baby/LuaLingShouBabyWashDetailView")
require("ui/lingshou/LuaLingShouMgr")

CLuaLingShouBabyWnd=class()
RegistClassMember(CLuaLingShouBabyWnd,"preBtn")
RegistClassMember(CLuaLingShouBabyWnd,"nextBtn")
RegistClassMember(CLuaLingShouBabyWnd,"m_ModelTexture")
RegistClassMember(CLuaLingShouBabyWnd,"m_FatherTexture")
RegistClassMember(CLuaLingShouBabyWnd,"m_MotherTexture")
RegistClassMember(CLuaLingShouBabyWnd,"m_PinzhiSprite")
RegistClassMember(CLuaLingShouBabyWnd,"m_ZhandouliLabel")
RegistClassMember(CLuaLingShouBabyWnd,"propertyDetailView")
RegistClassMember(CLuaLingShouBabyWnd,"skillDetailView")
RegistClassMember(CLuaLingShouBabyWnd,"washDetailView")

RegistClassMember(CLuaLingShouBabyWnd,"m_FatherTemplateId")
RegistClassMember(CLuaLingShouBabyWnd,"m_MotherTemplateId")
RegistClassMember(CLuaLingShouBabyWnd,"m_IsFollowFather")
RegistClassMember(CLuaLingShouBabyWnd,"m_NameLabel")
RegistClassMember(CLuaLingShouBabyWnd,"m_LevelLabel")
RegistClassMember(CLuaLingShouBabyWnd,"m_BabyInfo")
RegistClassMember(CLuaLingShouBabyWnd,"m_LingShouId")


-- function CLuaLingShouBabyWnd:Awake()
--     print("CLuaLingShouBabyWnd:Awake")
-- end


function CLuaLingShouBabyWnd:Init()
    self.preBtn=LuaGameObject.GetChildNoGC(self.transform,"PreBtn").gameObject
    self.nextBtn=LuaGameObject.GetChildNoGC(self.transform,"NextBtn").gameObject
    self.preBtn:SetActive(false)
    self.nextBtn:SetActive(false)

    local g = LuaGameObject.GetChildNoGC(self.transform,"CloseButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CUIResources.LingShouBabyWnd)
    end)

    self.m_ModelTexture=LuaGameObject.GetChildNoGC(self.transform,"ModelTexture").lingShouBabyTextureLoader
    self.m_ModelTexture:InitNull()
    self.m_FatherTexture=LuaGameObject.GetChildNoGC(self.transform,"FatherTexture").lingShouTextureLoader
    self.m_FatherTexture:InitNull()
    self.m_MotherTexture=LuaGameObject.GetChildNoGC(self.transform,"MotherTexture").lingShouTextureLoader
    self.m_MotherTexture:InitNull()
    self.m_NameLabel=LuaGameObject.GetChildNoGC(self.transform,"NameLabel").label
    self.m_LevelLabel=LuaGameObject.GetChildNoGC(self.transform,"LevelLabel").label


    --品质
    self.m_PinzhiSprite=LuaGameObject.GetChildNoGC(self.transform,"PinZhiSprite").sprite
    self.m_PinzhiSprite.spriteName="lingshou_ji"
    --战斗力
    self.m_ZhandouliLabel=LuaGameObject.GetChildNoGC(self.transform,"ZhandouliLabel").label
    self.m_ZhandouliLabel.text=" "


    self.propertyDetailView=CLuaLingShouBabyPropertyDetailView:new()
    self.propertyDetailView:Init(LuaGameObject.GetChildNoGC(self.transform,"Property").transform)
    self.skillDetailView=CLuaLingShouBabySkillDetailView:new()
    self.skillDetailView:Init(LuaGameObject.GetChildNoGC(self.transform,"Skill").transform)
    self.washDetailView=CLuaLingShouBabyWashDetailView:new()
    self.washDetailView:Init(LuaGameObject.GetChildNoGC(self.transform,"Wash").transform)

    self.m_LingShouId=CLingShouMgr.Inst.selectedLingShou
    self:InitBabyInfo()
end

function CLuaLingShouBabyWnd:InitBabyInfo()
    local lingShouId=self.m_LingShouId
    if lingShouId~=nil or lingShouId~="" then
        local details=CLingShouMgr.Inst:GetLingShouDetails(lingShouId)
        if details~=nil then
            local marryInfo=details.data.Props.MarryInfo
            if marryInfo==nil then
                CUIManager.CloseUI(CUIResources.LingShouBabyWnd)
                return
            end
            
            if marryInfo.IsFather>0 then
                self.m_FatherTemplateId=details.data.TemplateId
                self.m_MotherTemplateId=marryInfo.PartnerTemplateId
                self.m_IsFollowFather=true
            else
                self.m_FatherTemplateId=marryInfo.PartnerTemplateId
                self.m_MotherTemplateId=details.data.TemplateId
                self.m_IsFollowFather=false
            end

            local babyInfo = details.data.Props.Baby
            if babyInfo.BornTime>0 then
                local templateData=LingShouBaby_BabyTemplate.GetData(babyInfo.TemplateId)
                if templateData then
                    self.m_NameLabel.text=templateData.Name
                end

                self.m_ModelTexture:Init(babyInfo.TemplateId)
                -- self.m_NameLabel.text=
                self.m_FatherTexture:Init(self.m_FatherTemplateId,0,0)
                self.m_MotherTexture:Init(self.m_MotherTemplateId,0,0)
                -- print("level",babyInfo.Level)
                self.m_LevelLabel.text=SafeStringFormat3("Lv.%d",babyInfo.Level) or " "
                self.propertyDetailView:InitData(self.m_FatherTemplateId,self.m_MotherTemplateId,self.m_IsFollowFather,babyInfo,details.fightProperty)
                self.washDetailView:InitData(babyInfo,details.fightProperty)
                self.skillDetailView:InitData(babyInfo)
                self.m_PinzhiSprite.spriteName=CLuaLingShouMgr.GetBabyQualitySprite(babyInfo.Quality)

                self.m_ZhandouliLabel.text=tostring(CLuaLingShouMgr.BabyZhandouliFormula(babyInfo.Level,babyInfo.Quality,babyInfo.SkillList))
            else
                --没有宝宝了
                CUIManager.CloseUI(CUIResources.LingShouBabyWnd)
                return
            end
        end
    end
end

function CLuaLingShouBabyWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateLingShouBabyInfo", self, "OnUpdateLingShouBabyInfo")
    --加经验的时候  也会引起技能变化

    --更新洗炼积分
    g_ScriptEvent:AddListener("UpdateLingShouBabyLastWashResult", self, "OnUpdateLingShouBabyLastWashResult")

end

function CLuaLingShouBabyWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateLingShouBabyInfo", self, "OnUpdateLingShouBabyInfo")
    g_ScriptEvent:RemoveListener("UpdateLingShouBabyLastWashResult", self, "OnUpdateLingShouBabyLastWashResult")

end
function CLuaLingShouBabyWnd:OnUpdateLingShouBabyInfo(args)
    local lingshouId=args[0]
    if self.m_LingShouId==lingshouId then
        self:InitBabyInfo()
    end
end
function CLuaLingShouBabyWnd:OnUpdateLingShouBabyLastWashResult(args)
    local lingShouId=args[0]
    local babyQuality=args[1]
    local babySkillCls1=args[2]
    local babySkillCls2=args[3]
    local washCount=args[4]
    if self.m_LingShouId==lingShouId then
        self.washDetailView:OnUpdateLingShouBabyLastWashResult(lingShouId,babyQuality,babySkillCls1,babySkillCls2,washCount)
    end
end

return CLuaLingShouBabyWnd
