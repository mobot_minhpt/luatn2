local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CQMPKScoreItem = import "L10.UI.CQMPKScoreItem"

CLuaStarBiwuScoreWnd = class()
CLuaStarBiwuScoreWnd.Path = "ui/starbiwu/LuaStarBiwuScoreWnd"
RegistClassMember(CLuaStarBiwuScoreWnd,"m_AdvGridView")
RegistClassMember(CLuaStarBiwuScoreWnd,"m_CurrentRow")

function CLuaStarBiwuScoreWnd:Awake()
    self.m_AdvGridView = self.transform:Find("Anchor/ZhanDuiList/ListView"):GetComponent(typeof(QnTableView))
    self.m_CurrentRow = -1

    self.transform:Find("Anchor/ZhanDuiList/ListView/Scrollview/Pool/MyScoreItem").gameObject:SetActive(false)
    self.transform:Find("Anchor/ZhanDuiList/ListView/Scrollview/Pool/ScoreItem").gameObject:SetActive(false)
end

function CLuaStarBiwuScoreWnd:Init( )
    local getNumFunc = function() return #CLuaStarBiwuMgr.m_ScoreRankList end
    local createItemFunc = function(view, index)
        if index < 0 and index >= #CLuaStarBiwuMgr.m_ScoreRankList then
            return nil
        end

        local item
        if index == 0 and CLuaStarBiwuMgr.m_HasMyScore then
            item = TypeAs(view:GetFromPool(0), typeof(CQMPKScoreItem))
        else
            item = TypeAs(view:GetFromPool(1), typeof(CQMPKScoreItem))
        end
        if not item then
            return nil
        end
        item:Init(CLuaStarBiwuMgr.m_ScoreRankList[index + 1], index, index == 0 and CLuaStarBiwuMgr.m_HasMyScore)
        item:SetBackgroundTexture(index % 2 == 0 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
        return item
    end

    self.m_AdvGridView.m_DataSource= DefaultTableViewDataSource.Create2(getNumFunc, createItemFunc)
    self.m_AdvGridView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self.m_CurrentRow = row
        local zhanduiId = 0
        if CLuaStarBiwuMgr.m_ScoreRankList and (row < #CLuaStarBiwuMgr.m_ScoreRankList) then
            zhanduiId = CLuaStarBiwuMgr.m_ScoreRankList[row + 1].m_Id
        end
        CLuaStarBiwuMgr:ShowZhanDuiMemberWnd(row + 1, zhanduiId, 2)
    end)
    self.m_AdvGridView:ReloadData(true,false)
end
