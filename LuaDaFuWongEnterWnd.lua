local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local QnButton = import "L10.UI.QnButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUITexture = import "L10.UI.CUITexture"
local PlayerSettings = import "L10.Game.PlayerSettings"
LuaDaFuWongEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaFuWongEnterWnd, "Table", "Table", UITable)
RegistChildComponent(LuaDaFuWongEnterWnd, "TaskTemplateItem", "TaskTemplateItem", GameObject)
RegistChildComponent(LuaDaFuWongEnterWnd, "StartBtn", "StartBtn", QnButton)
RegistChildComponent(LuaDaFuWongEnterWnd, "RuleBtn", "RuleBtn", QnButton)
RegistChildComponent(LuaDaFuWongEnterWnd, "TaskBtn", "TaskBtn", QnButton)
RegistChildComponent(LuaDaFuWongEnterWnd, "settingBtn", "settingBtn", QnButton)
RegistChildComponent(LuaDaFuWongEnterWnd, "voiceBtn", "voiceBtn", QnButton)
RegistChildComponent(LuaDaFuWongEnterWnd, "VolumeSetting", "VolumeSetting", GameObject)
RegistChildComponent(LuaDaFuWongEnterWnd, "bgMask", "bgMask", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongEnterWnd, "m_MatchStatus")
RegistClassMember(LuaDaFuWongEnterWnd, "m_TongXingZhengAlert")
RegistClassMember(LuaDaFuWongEnterWnd, "m_MatchTimeTick")
RegistClassMember(LuaDaFuWongEnterWnd, "m_MaxDailyTask")
function LuaDaFuWongEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.StartBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnStartBtnClick()
	end)


	
	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)


	
	UIEventListener.Get(self.TaskBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTaskBtnClick()
	end)


	
	UIEventListener.Get(self.settingBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnsettingBtnClick()
	end)


	
	UIEventListener.Get(self.voiceBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnvoiceBtnClick()
	end)


	UIEventListener.Get(self.bgMask.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnbgMaskClick()
	end)

    --@endregion EventBind end
    self.m_TongXingZhengAlert = self.TaskBtn.transform:Find("alert").gameObject
    self.m_TongXingZhengAlert:SetActive(false)
    self.m_MatchTimeTick = nil
    self.m_MaxDailyTask = DaFuWeng_Setting.GetData().MaxTongXingZhengTaskNum
    self.VolumeSetting.gameObject:SetActive(false)
end

function LuaDaFuWongEnterWnd:Init()
    -- 请求数据 匹配情况，当天任务及任务完成情况，通行证信息
    Gac2Gas.OpenDaFuWengSignUpWnd()
    if not LuaDaFuWongMgr.TongXingZhengInfo then 
        Gac2Gas.RequestDaFuWengPlayData()
    else
        self:UpdateVoiceLabel()
    end
    self.TaskTemplateItem:SetActive(false)
    self:UpdateStartBtn()
    self:UpdateTongXingZhengInfo()
    self:UpdateTaskListInfo()
    self:InitDate()
    self:OnUpdateMatchStatus(LuaDaFuWongMgr.MatchStatus)
    self:SetMatchTick()
    --self:UpdateVoiceLabel()
end
function LuaDaFuWongEnterWnd:SetMatchTick()
    if LuaDaFuWongMgr.MatchStatus and LuaDaFuWongMgr.MatchTime ~= 0 then
        if self.m_MatchTimeTick then UnRegisterTick(self.m_MatchTimeTick) self.m_MatchTimeTick = nil end
        self.m_MatchTimeTick = RegisterTick(function ()
            local matchTimeLabel = self.StartBtn.transform:Find("Canel/CanelLabel/Label"):GetComponent(typeof(UILabel))
            local totalSeconds = CServerTimeMgr.Inst.timeStamp - LuaDaFuWongMgr.MatchTime
            local timeText = ""
            if totalSeconds >= 3600 then
                timeText = SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
            else
                timeText =  SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
            end
            matchTimeLabel.text = timeText
        end,500)
    else
        if self.m_MatchTimeTick then UnRegisterTick(self.m_MatchTimeTick) self.m_MatchTimeTick = nil end
    end
end
function LuaDaFuWongEnterWnd:InitDate()
    local nowtime = CServerTimeMgr.Inst:GetZone8Time()
    local year = self.transform:Find("Content/riqi"):GetComponent(typeof(UILabel))
    local month = year.transform:Find("month"):GetComponent(typeof(UILabel))
    local day = year.transform:Find("day"):GetComponent(typeof(UILabel))
    year.text = nowtime.Year
    month.text = nowtime.Month
    day.text = nowtime.Day

    self.transform:Find("Content/Center/Label/DesLabel"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("DaFuWeng_EnterWnd_Title")
end

function LuaDaFuWongEnterWnd:UpdateStartBtn()
    self.StartBtn.transform:Find("Start").gameObject:SetActive(not self.m_MatchStatus)
    self.StartBtn.transform:Find("Canel").gameObject:SetActive(self.m_MatchStatus)
    self.StartBtn.transform:Find("Canel/CanelLabel/Label"):GetComponent(typeof(UILabel)).text = "00:00"
end


function LuaDaFuWongEnterWnd:OnTongXingZhengUpdate()
    self:UpdateTongXingZhengInfo()
    self:UpdateTaskListInfo()
    self:UpdateVoiceLabel()
end

function LuaDaFuWongEnterWnd:UpdateTongXingZhengInfo()
    if not LuaDaFuWongMgr.TongXingZhengInfo then 
        self.TaskBtn.transform:Find("Content").gameObject:SetActive(false) 
    else
        local scoreInfo = LuaDaFuWongMgr.TongXingZhengInfo
        local maxExp = LuaDaFuWongMgr.TongXingZengLevelToExp[math.min(scoreInfo.lv + 1,DaFuWeng_Setting.GetData().MaxBuyLevel)].progress
        self.TaskBtn.transform:Find("Content").gameObject:SetActive(true) 
        self.TaskBtn.transform:Find("Content/Level"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%d级"),scoreInfo.lv)
        self.TaskBtn.transform:Find("Content/Score"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("%d/%d",scoreInfo.curExp,maxExp)
        if not LuaDaFuWongMgr.TongXingZengLevelToExp[scoreInfo.lv + 1] then 
            self.TaskBtn.transform:Find("Content/Score"):GetComponent(typeof(UILabel)).text = LocalString.GetString("已满级")
        end
        self.m_TongXingZhengAlert:SetActive(LuaDaFuWongMgr:TongXingZhengReward())
    end
end

function LuaDaFuWongEnterWnd:UpdateTaskListInfo()
    Extensions.RemoveAllChildren(self.Table.transform)
    if not LuaDaFuWongMgr.DailyTaskInfo then return end
    local dailyTaskInfo = LuaDaFuWongMgr.DailyTaskInfo
    local task = {}
    for k,v in pairs(dailyTaskInfo) do
        local data = {id = k,p = v.p,r = v.r}
        table.insert(task,data)
    end
    table.sort(task,function(a,b) 
        return a.r and not b.r
    end)
    for i = 1,self.m_MaxDailyTask do
        local go = CUICommonDef.AddChild(self.Table.gameObject,self.TaskTemplateItem)
        go.gameObject:SetActive(true)
        if task[i] and not task[i].r then
            go.transform:Find("EmptyLabel").gameObject:SetActive(false)
            go.transform:Find("Content").gameObject:SetActive(true)
            local id = task[i].id
            local taskInfo = DaFuWeng_TongXingZhengTask.GetData(id)
            go.transform:Find("Content/Panel/Score"):GetComponent(typeof(UILabel)).text = taskInfo.ProgressReward
            go.transform:Find("Content/IconPath/Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(taskInfo.IconPath)
            go.transform:Find("Content/Progress"):GetComponent(typeof(UISlider)).value = task[i].p/taskInfo.Target
            go.transform:Find("Content/Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(taskInfo.Describe,task[i].p,taskInfo.Target)
        else
            go.transform:Find("EmptyLabel").gameObject:SetActive(true)
            go.transform:Find("Content").gameObject:SetActive(false)
        end
    end
    self.Table:Reposition()
end

function LuaDaFuWongEnterWnd:UpdateVoiceLabel()
    local musicLabel = self.settingBtn.transform:Find("MusicLabel"):GetComponent(typeof(UILabel))
    local voiceLabel = self.settingBtn.transform:Find("VoiceLabel"):GetComponent(typeof(UILabel))
    voiceLabel.transform:Find("On").gameObject:SetActive(PlayerSettings.SoundEnabled)
    voiceLabel.transform:Find("Off").gameObject:SetActive(not PlayerSettings.SoundEnabled)


    musicLabel.transform:Find("On").gameObject:SetActive(PlayerSettings.MusicEnabled)
    musicLabel.transform:Find("Off").gameObject:SetActive(not PlayerSettings.MusicEnabled)

    self:CheckAndUpdateVoiceName()
end

function LuaDaFuWongEnterWnd:OnUpdateMatchStatus(matchStatus)
    self.m_MatchStatus = matchStatus
    self:SetMatchTick()
    self:UpdateStartBtn()
end

function LuaDaFuWongEnterWnd:CheckAndUpdateVoiceName()
    local voicePackageName = self.voiceBtn.transform:Find("MusicLabel"):GetComponent(typeof(UILabel))
    voicePackageName.text = nil
    if LuaDaFuWongMgr.VoicePackage then
        local use = LuaDaFuWongMgr.VoicePackage.use
        local data = DaFuWeng_VoicePackage.GetData(use)
        if data then
            voicePackageName.text = data.Title
            CUICommonDef.SetActive(voicePackageName.gameObject, true, false)
        end
        if LuaDaFuWongMgr.VoicePackage.bag[use] then
           if CServerTimeMgr.Inst.timeStamp >= LuaDaFuWongMgr.VoicePackage.bag[use] then
                voicePackageName.text = LocalString.GetString("语音包已过期")
                CUICommonDef.SetActive(voicePackageName.gameObject, false, false)
                g_MessageMgr:ShowMessage("CUSTOM_STRING1", g_MessageMgr:FormatMessage("DaFuWeng_VoicePackage_OutOfDate"))

           end
        end
    end
end

function LuaDaFuWongEnterWnd:OnGlobalMatch_SignUpPlayResult(playId, success)
    if playId == DaFuWeng_Setting.GetData().GamePlayId and success then
        LuaDaFuWongMgr:OnMatchStatusChange(true)
    end
end

function LuaDaFuWongEnterWnd:OnGlobalMatch_CancelSignUpResult(playId, success)
    if playId == DaFuWeng_Setting.GetData().GamePlayId and success then
        LuaDaFuWongMgr:OnMatchStatusChange(false)
    end
end

--@region UIEvent

function LuaDaFuWongEnterWnd:OnStartBtnClick()
    if not self.m_MatchStatus then
        -- 请求匹配
        --print("match")
        Gac2Gas.RequestSignUpDaFuWeng()
    else
        -- 取消匹配
        --print("cancel match")
        Gac2Gas.RequestCancelSignUpDaFuWeng()
    end
end

function LuaDaFuWongEnterWnd:OnRuleBtnClick()
    CUIManager.ShowUI(CLuaUIResources.DaFuWongRuleWnd)
end

function LuaDaFuWongEnterWnd:OnTaskBtnClick()
    CUIManager.ShowUI(CLuaUIResources.DaFuWongTongXingZhengWnd)
end
function LuaDaFuWongEnterWnd:OnsettingBtnClick()
    self.VolumeSetting.gameObject:SetActive(true)
    local closeVolumeSetting = self.VolumeSetting.transform:Find("Background/CloseButton").gameObject
    UIEventListener.Get(closeVolumeSetting.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self.VolumeSetting.gameObject:SetActive(false)
        self:UpdateVoiceLabel()
	end)
end

function LuaDaFuWongEnterWnd:OnvoiceBtnClick()
    CUIManager.ShowUI(CLuaUIResources.DaFuWongVoicePackageWnd)
end


function LuaDaFuWongEnterWnd:OnbgMaskClick()
    self.VolumeSetting.gameObject:SetActive(false)
    self:UpdateVoiceLabel()
end


--@endregion UIEvent

function LuaDaFuWongEnterWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateDaFuWongMatchStatus",self,"OnUpdateMatchStatus")
    g_ScriptEvent:AddListener("UpdateDaFuWongTongXingZhengInfo",self,"UpdateTongXingZhengInfo")
    g_ScriptEvent:AddListener("UpdateDaFuWongTaskListInfo",self,"UpdateTaskListInfo")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
    g_ScriptEvent:AddListener("OnTongXingZhengDataUpdate", self, "OnTongXingZhengUpdate")
    g_ScriptEvent:AddListener("OnSoundStatusUpdate", self, "UpdateVoiceLabel")
end

function LuaDaFuWongEnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateDaFuWongMatchStatus",self,"OnUpdateMatchStatus")
    g_ScriptEvent:RemoveListener("UpdateDaFuWongTongXingZhengInfo",self,"UpdateTongXingZhengInfo")
    g_ScriptEvent:RemoveListener("UpdateDaFuWongTaskListInfo",self,"UpdateTaskListInfo")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
    g_ScriptEvent:RemoveListener("OnTongXingZhengDataUpdate", self, "OnTongXingZhengUpdate")
    g_ScriptEvent:RemoveListener("OnSoundStatusUpdate", self, "UpdateVoiceLabel")
    if self.m_MatchTimeTick then UnRegisterTick(self.m_MatchTimeTick) self.m_MatchTimeTick = nil end
end
