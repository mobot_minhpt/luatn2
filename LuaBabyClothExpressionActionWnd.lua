require("common/common_include")

local UIGrid = import "UIGrid"
local CUITexture = import "L10.UI.CUITexture"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltip = import "L10.UI.CTooltip"
local BabyMgr = import "L10.Game.BabyMgr"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local CUIManager = import "L10.UI.CUIManager"
local CUIRes = import "L10.UI.CUIResources"

LuaBabyClothExpressionActionWnd = class()

RegistClassMember(LuaBabyClothExpressionActionWnd, "BornBabyRoot")
RegistClassMember(LuaBabyClothExpressionActionWnd, "OtherBabyRoot")

RegistClassMember(LuaBabyClothExpressionActionWnd, "ExpressionGrid_Born")
RegistClassMember(LuaBabyClothExpressionActionWnd, "ExpressionGrid_Other")
RegistClassMember(LuaBabyClothExpressionActionWnd, "ExpressionItem")

-- data
RegistClassMember(LuaBabyClothExpressionActionWnd, "ExpressionBabyEngineId")
RegistClassMember(LuaBabyClothExpressionActionWnd, "ExpressionInfoList")

RegistClassMember(LuaBabyClothExpressionActionWnd, "ExpressionItems")

function LuaBabyClothExpressionActionWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaBabyClothExpressionActionWnd:InitClassMembers()

	self.BornBabyRoot = self.transform:Find("Anchor/BornBabyRoot").gameObject
	self.OtherBabyRoot = self.transform:Find("Anchor/OtherBabyRoot").gameObject
	self.ExpressionItem = self.transform:Find("Anchor/ExpressionItem").gameObject
	self.ExpressionItem:SetActive(false)

	self.ExpressionGrid_Born = self.transform:Find("Anchor/BornBabyRoot/ExpressionGrid_Born"):GetComponent(typeof(UIGrid))
	self.ExpressionGrid_Other = self.transform:Find("Anchor/OtherBabyRoot/ExpressionGrid_Other"):GetComponent(typeof(UIGrid))

end

function LuaBabyClothExpressionActionWnd:ClickThroughToClose()
    if Input.GetMouseButtonDown(0) then
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform) and
            CUICommonDef.GetNearestPanelDepth(self.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
            CUIManager.CloseUI(CUIRes.BabyClothExpressionActionWnd)
        end
    end
end

function LuaBabyClothExpressionActionWnd:Update()
	self:ClickThroughToClose()
end

function LuaBabyClothExpressionActionWnd:InitValues()
	self:UpdateExpressions()
end

function LuaBabyClothExpressionActionWnd:UpdateExpressions()
	--self.ExpressionBabyEngineId = LuaBabyMgr.m_ExpressionBabyEngineId
  local unlockGroupData = BabyMgr.Inst:GetBabyExpressionInfoSameAsServer(BabyMgr.Inst.ShowBabyInfo)
  local babyExpressionList = BabyMgr.Inst:GetBabyExpressionsListInfo(BabyMgr.Inst.ShowBabyInfo.Props.ExtraData.ShowStatus, unlockGroupData)
  self.ExpressionInfoList = {}

	for i = 0, babyExpressionList.Count-1 do
		table.insert(self.ExpressionInfoList, babyExpressionList[i])
	end
	babyExpressionList = nil

	local function compare(a, b)
		if a.IsLock and not b.IsLock then
			return false
		elseif not a.IsLock and b.IsLock then
			return true
		else
			return a:GetOrder() < b:GetOrder()
		end
	end
	table.sort(self.ExpressionInfoList, compare)

	self.ExpressionItems = {}
	if #self.ExpressionInfoList <= 3 then
		self:ShowBornExpressions()
	else
		self:ShowOtherExpressions()
	end
end

function LuaBabyClothExpressionActionWnd:ShowBornExpressions()
	self.BornBabyRoot:SetActive(true)
	self.OtherBabyRoot:SetActive(false)

	CUICommonDef.ClearTransform(self.ExpressionGrid_Born.transform)
	CUICommonDef.ClearTransform(self.ExpressionGrid_Other.transform)

	for i = 1, #self.ExpressionInfoList do
		local go = NGUITools.AddChild(self.ExpressionGrid_Born.gameObject, self.ExpressionItem)
		self:InitExpressionItem(go, self.ExpressionInfoList[i])
		go:SetActive(true)
		table.insert(self.ExpressionItems, go)
	end
	self.ExpressionGrid_Born:Reposition()
end

function LuaBabyClothExpressionActionWnd:ShowOtherExpressions()
	self.BornBabyRoot:SetActive(false)
	self.OtherBabyRoot:SetActive(true)

	CUICommonDef.ClearTransform(self.ExpressionGrid_Born.transform)
	CUICommonDef.ClearTransform(self.ExpressionGrid_Other.transform)

	for i = 1, #self.ExpressionInfoList do
		local go = NGUITools.AddChild(self.ExpressionGrid_Other.gameObject, self.ExpressionItem)
		self:InitExpressionItem(go, self.ExpressionInfoList[i])
		go:SetActive(true)
		table.insert(self.ExpressionItems, go)
	end
	self.ExpressionGrid_Other:Reposition()
end

function LuaBabyClothExpressionActionWnd:InitExpressionItem(go, info)
	local Icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local DisableBg = go.transform:Find("Icon/DisableBg").gameObject
	local Access = go.transform:Find("Icon/Access").gameObject
	local Name = go.transform:Find("Name"):GetComponent(typeof(UILabel))

	Icon:LoadMaterial(info:GetIcon())
	Name.text = info:GetName()
	DisableBg:SetActive(info.IsLock)
	Access:SetActive(info.IsLock)

	local onExpressionItemClicked = function (go)
		self:OnExpressionItemClicked(go, info)
	end
	CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(onExpressionItemClicked), false)
end

function LuaBabyClothExpressionActionWnd:OnExpressionItemClicked(go, info)
	if info.IsLock then
		CItemAccessListMgr.Inst:ShowItemAccessInfo(info:GetItemID(), true, go.transform, CTooltip.AlignType.Right)
	else
    --[[
		if info:GetShowType() == 1 then
			-- 单人动作
			Gac2Gas.RequestBabyDoExpression(self.ExpressionBabyEngineId, info:GetID(), 0)
		elseif info:GetShowType() == 2 then
			-- todo 弹出附近的人的界面
			local hint = SafeStringFormat3(LocalString.GetString("请选择目标宝宝进行%s动作"), info:GetName())
			CNearbyPlayerWndMgr.ShowWnd(hint, info:GetName(), DelegateFactory.Action_ulong(function (otherEngineId)
				Gac2Gas.RequestBabyDoExpression(self.ExpressionBabyEngineId, info:GetID(), otherEngineId)
			end), info:GetShowType())
		end
    --]]
    EventManager.BroadcastInternalForLua(EnumEventType.BabyInfoUpdate, {info:GetID()})
	end
end


function LuaBabyClothExpressionActionWnd:UpdateExpressionItem(go, info)
	local Icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local Name = go.transform:Find("Name"):GetComponent(typeof(UILabel))
	Icon:LoadMaterial(info:GetIcon())
	Name.text = info:GetName()
end

function LuaBabyClothExpressionActionWnd:OnBabyInfoDelete()
	CUIManager.CloseUI(CUIRes.BabyClothExpressionActionWnd)
end

function LuaBabyClothExpressionActionWnd:OnEnable()
	g_ScriptEvent:AddListener("BabyInfoDelete", self, "OnBabyInfoDelete")
end

function LuaBabyClothExpressionActionWnd:OnDisable()
	g_ScriptEvent:RemoveListener("BabyInfoDelete", self, "OnBabyInfoDelete")
end

return LuaBabyClothExpressionActionWnd
