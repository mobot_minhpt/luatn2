local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local QnRadioBox=import "L10.UI.QnRadioBox"
local CUIGameObjectPool=import "L10.UI.CUIGameObjectPool"
local QnTableView=import "L10.UI.QnTableView"
local CGuanNingMgr=import "L10.Game.CGuanNingMgr"
local Profession=import "L10.Game.Profession"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local CUIFx = import "L10.UI.CUIFx"
local AlignType=import "CPlayerInfoMgr+AlignType"

CLuaGuanNingResultPane = class()
RegistClassMember(CLuaGuanNingResultPane,"sortFlags")
RegistClassMember(CLuaGuanNingResultPane,"crossServerTitle")
RegistClassMember(CLuaGuanNingResultPane,"normalTitle")
RegistClassMember(CLuaGuanNingResultPane,"radioBox1")
RegistClassMember(CLuaGuanNingResultPane,"radioBox2")
RegistClassMember(CLuaGuanNingResultPane,"tableView")
RegistClassMember(CLuaGuanNingResultPane,"playInfoList")
RegistClassMember(CLuaGuanNingResultPane,"isCrossServer")
RegistClassMember(CLuaGuanNingResultPane, "id2Item")
RegistClassMember(CLuaGuanNingResultPane, "allThumbUpBtn") -- Reposition前需要先关掉所有thumbUp and DestoryFx
RegistClassMember(CLuaGuanNingResultPane, "LastTUpPlayerList")
RegistClassMember(CLuaGuanNingResultPane, "Camp")
RegistClassMember(CLuaGuanNingResultPane, "bIsMyForce") --缓存阵营信息，避免跨场景后无法获取

function CLuaGuanNingResultPane:Init( transform )
    self.Camp = transform.name == "Left" and 0 or 1
    self.bIsMyForce = self.Camp == CLuaGuanNingMgr:GetMyForce()

    self.sortFlags = { 1,1,1 }
    self.crossServerTitle = transform:Find("CrossServerTitle").gameObject
    self.normalTitle = transform:Find("NormalTitle").gameObject
    self.radioBox1 = transform:Find("NormalTitle/RadioBox1"):GetComponent(typeof(QnRadioBox))
    self.radioBox2 = transform:Find("CrossServerTitle/RadioBox2"):GetComponent(typeof(QnRadioBox))
    self.tableView = transform:Find("Content/ScrollView"):GetComponent(typeof(QnTableView))

    self.id2Item = {}
    self.playInfoList = {}
    self.allThumbUpBtn = {}
    self.LastTUpPlayerList = {}

    -- cache一下,mgr里这个属性有些混乱
    self.isCrossServer = CGuanNingMgr.Inst.IsCrossServer
    if self.isCrossServer then
        self.crossServerTitle:SetActive(true)
        self.crossServerTitle.transform.localPosition = Vector3.zero
        self.normalTitle:SetActive(false)
        self.normalTitle.transform.localPosition = Vector3.zero
    else
        self.crossServerTitle:SetActive(false)
        self.crossServerTitle.transform.localPosition = Vector3.zero
        self.normalTitle:SetActive(true)
        self.normalTitle.transform.localPosition = Vector3.zero
    end

    local callback=DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnRadioBoxSelect(btn,index)
    end)

    self.radioBox1.OnSelect = callback
    self.radioBox2.OnSelect = callback

    --默认排序
    self.radioBox1:ChangeTo(- 1, true)
    self.radioBox2:ChangeTo(- 1, true)
    --Sort(playInfoList, ESortType.AllScore);

    self.tableView.m_DataSource = DefaultTableViewDataSource.Create2(
        function()
            return #self.playInfoList
        end,
        function(view,row)
            local key=self.isCrossServer and 1 or 0
            local item=view:GetFromPool(key)
            self:InitItem(item,self.playInfoList[row+1],row)
            return item
        end)
    
    --tableView.ReloadData(true);
end
function CLuaGuanNingResultPane:InitData( list) 
    self.id2Item = {}
    self.playInfoList = list
    self.radioBox1:ChangeTo(- 1, true)
    self.radioBox2:ChangeTo(- 1, true)

    -- self.sortFlags[3] = 1
    self:Sort(self.playInfoList, 3)--1,2,3

    if self.bIsMyForce then
        for _, v in ipairs(self.allThumbUpBtn) do
            v.transform:Find("ThumbUpFx"):GetComponent(typeof(CUIFx)):DestroyFx()
            v:SetActive(false)
        end
    end

    self.tableView:ReloadData(true, false)

    self:InitThumbUp(self.LastTUpPlayerList)
end
function CLuaGuanNingResultPane:Scroll2MyRow( )
    if CClientMainPlayer.Inst == nil then
        return
    end
    local myName=CClientMainPlayer.Inst.Name
    local findIndex=0
    for i,v in ipairs(self.playInfoList) do
        if v.name==myName then
            findIndex=i-1
        end
    end
    self.tableView:ScrollToRow(findIndex)
end
function CLuaGuanNingResultPane:OnRadioBoxSelect( btn, index) 
    -- local sortButton = TypeAs(btn, typeof(CSortButton))
    self.sortFlags[index+1] = - self.sortFlags[index+1]

    for i,v in ipairs(self.sortFlags) do
        if i-1~=index then
            self.sortFlags[i] = -1
        end
    end
    btn:SetSortTipStatus(self.sortFlags[index+1] < 0)
    self:Sort(self.playInfoList, index)

    if self.bIsMyForce then
        for _, v in ipairs(self.allThumbUpBtn) do
            v.transform:Find("ThumbUpFx"):GetComponent(typeof(CUIFx)):DestroyFx()
            v:SetActive(false)
        end
    end

    self.tableView:ReloadData(true, false)

    self:InitThumbUp(self.LastTUpPlayerList)
end
function CLuaGuanNingResultPane:Sort( list, type) 
    local flag = self.sortFlags[type+1]
    if type==0 then
        table.sort( list, function(a,b)
            if a.fightScore>b.fightScore then
                return -flag<0
            elseif a.fightScore<b.fightScore then
                return flag<0
            else
                return a.playerId<b.playerId
            end
        end )
    elseif type==1 then
        table.sort( list, function(a,b)
            if a.occupyScore > b.occupyScore then
                return -flag<0
            elseif a.occupyScore < b.occupyScore then
                return flag<0
            else
                return a.playerId<b.playerId
            end
        end)
    elseif type==2 then
        table.sort( list, function(a,b)
            if a.score > b.score then
                return -flag<0
            elseif a.score < b.score then
                return flag<0
            else
                return a.playerId<b.playerId
            end
        end)
    end
end

function CLuaGuanNingResultPane:OnEnable()
    --g_ScriptEvent:AddListener("DianZanGnjcOtherPlayerSuccess", self, "OnDianZanGnjcOtherPlayerSuccess") 
    --g_ScriptEvent:AddListener("CancelDianZanGnjcOtherPlayerSuccess", self, "OnCancelDianZanGnjcOtherPlayerSuccess") 
end

function CLuaGuanNingResultPane:OnDisable()
    --g_ScriptEvent:RemoveListener("DianZanGnjcOtherPlayerSuccess", self, "OnDianZanGnjcOtherPlayerSuccess") 
    --g_ScriptEvent:RemoveListener("CancelDianZanGnjcOtherPlayerSuccess", self, "OnCancelDianZanGnjcOtherPlayerSuccess") 
end

--[[
function CLuaGuanNingResultPane:OnDianZanGnjcOtherPlayerSuccess(playId, serverGroupId, targetId)
    if self.bIsMyForce and playId == CLuaGuanNingMgr.m_PlayId and serverGroupId == CLuaGuanNingMgr.m_ServerGroupId then
        CLuaGuanNingMgr.m_ThumbUpCnt = CLuaGuanNingMgr.m_ThumbUpCnt + 1
        local tUp, fxPath
        if self.Camp == 0 then
            tUp = self.id2Item[targetId]:Find("LThumbUp")
            fxPath = "fx/ui/prefab/UI_Guanning_dzblue.prefab"
        else
            tUp = self.id2Item[targetId]:Find("RThumbUp")
            fxPath = "fx/ui/prefab/UI_Guanning_dzred.prefab"
        end
        local selected = tUp:Find("Selected").gameObject
        tUp:Find("ThumbUpFx"):GetComponent(typeof(CUIFx)):LoadFx(fxPath)
        selected:SetActive(true)
        table.insert(self.LastTUpPlayerList, targetId)
    end
end

function CLuaGuanNingResultPane:OnCancelDianZanGnjcOtherPlayerSuccess(playId, serverGroupId, targetId)
    if self.bIsMyForce and playId == CLuaGuanNingMgr.m_PlayId and serverGroupId == CLuaGuanNingMgr.m_ServerGroupId then
        CLuaGuanNingMgr.m_ThumbUpCnt = CLuaGuanNingMgr.m_ThumbUpCnt - 1
        local tUp
        if self.Camp == 0 then
            tUp = self.id2Item[targetId]:Find("LThumbUp")
        else
            tUp = self.id2Item[targetId]:Find("RThumbUp")
        end
        local selected = tUp:Find("Selected").gameObject
        tUp:Find("ThumbUpFx"):GetComponent(typeof(CUIFx)):DestroyFx()
        selected:SetActive(false)
        for k, id in ipairs(self.LastTUpPlayerList) do
            if id == targetId then
                table.remove(self.LastTUpPlayerList, k)
                break
            end
        end
    end
end
--]]

function CLuaGuanNingResultPane:RefreshThumbUp(tUpPlayerList)
    if self.bIsMyForce then
        self.LastTUpPlayerList = tUpPlayerList
        for _, v in ipairs(self.playInfoList) do
            local tUp, fxPath
            if self.id2Item and v and v.playerId and self.id2Item[v.playerId] then
                if self.Camp == 0 then
                    tUp = self.id2Item[v.playerId]:Find("LThumbUp")
                    fxPath = "fx/ui/prefab/UI_Guanning_dzblue.prefab"
                else
                    tUp = self.id2Item[v.playerId]:Find("RThumbUp")
                    fxPath = "fx/ui/prefab/UI_Guanning_dzred.prefab"
                end
                if tUp then
                    if CClientMainPlayer.Inst and v.playerId ~= CClientMainPlayer.Inst.Id then
                        tUp.gameObject:SetActive(true)
                        local selected = tUp:Find("Selected").gameObject
                        local fnd = false
                        for _, id in ipairs(tUpPlayerList) do 
                            if v.playerId == id then
                                fnd = true
                                break
                            end
                        end
                        if fnd then
                            if not selected.activeSelf then
                                tUp:Find("ThumbUpFx"):GetComponent(typeof(CUIFx)):LoadFx(fxPath)
                                selected:SetActive(true)
                            end
                        else
                            if selected.activeSelf then
                                tUp:Find("ThumbUpFx"):GetComponent(typeof(CUIFx)):DestroyFx()
                                selected:SetActive(false)
                            end
                        end
                    else
                        tUp.gameObject:SetActive(false)
                    end
                end
            end
        end
    end
end

function CLuaGuanNingResultPane:InitThumbUp(tUpPlayerList) --Call After ReloadData
    if self.bIsMyForce and CLuaGuanNingMgr.m_PlayId and CLuaGuanNingMgr.m_ServerGroupId then
        self.LastTUpPlayerList = tUpPlayerList
        self.allThumbUpBtn = {}
        for _, v in ipairs(self.playInfoList) do
            local tUp
            if self.id2Item and v and v.playerId and self.id2Item[v.playerId] then 
                if self.Camp == 0 then
                    tUp = self.id2Item[v.playerId]:Find("LThumbUp")
                else
                    tUp = self.id2Item[v.playerId]:Find("RThumbUp")
                end
                if tUp then
                    table.insert(self.allThumbUpBtn, tUp.gameObject)
                    if CClientMainPlayer.Inst and v.playerId ~= CClientMainPlayer.Inst.Id then
                        tUp.gameObject:SetActive(true)
                        local selected = tUp:Find("Selected").gameObject
                        local fnd = false
                        for _, id in ipairs(tUpPlayerList) do 
                            if v.playerId == id then
                                fnd = true
                                break
                            end
                        end
                        if fnd then
                            selected:SetActive(true)
                        else
                            selected:SetActive(false)
                        end
                        UIEventListener.Get(tUp:Find("Texture").gameObject).onClick = DelegateFactory.VoidDelegate(
                            function(go) 
                                if selected.activeSelf then
                                    Gac2Gas.RequestCancelDianZanGnjcOtherPlayer(CLuaGuanNingMgr.m_PlayId, CLuaGuanNingMgr.m_ServerGroupId, v.playerId)
                                    Gac2Gas.RequestGnjcDianZanData(CLuaGuanNingMgr.m_PlayId, CLuaGuanNingMgr.m_ServerGroupId)
                                else
                                    --if CLuaGuanNingMgr.m_ThumbUpCnt >= CLuaGuanNingMgr.m_ThumbUpLimit then
                                    --    g_MessageMgr:ShowMessage("GuanNing_ThumbUp_Limit", CLuaGuanNingMgr.m_ThumbUpLimit)
                                    --    return
                                    --end
                                    Gac2Gas.RequestDianZanGnjcOtherPlayer(CLuaGuanNingMgr.m_PlayId, CLuaGuanNingMgr.m_ServerGroupId, v.playerId)
                                    Gac2Gas.RequestGnjcDianZanData(CLuaGuanNingMgr.m_PlayId, CLuaGuanNingMgr.m_ServerGroupId)
                                end
                            end)
                    else
                        tUp.gameObject:SetActive(false)
                    end
                end
            end
        end
    end
end

function CLuaGuanNingResultPane:InitItem(item,info,row)
    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
        CPlayerInfoMgr.ShowPlayerPopupMenu(info.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgr.AlignType.Default)
    end)

    local transform=item.transform
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local clsSprite = transform:Find("Sprite"):GetComponent(typeof(UISprite))
    local fightScoreLabel = transform:Find("FightScoreLabel"):GetComponent(typeof(UILabel))
    local occupySocreLabel = transform:Find("OccupySocreLabel"):GetComponent(typeof(UILabel))
    local scoreLabel = transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    local bgSprite = transform:Find("Bg"):GetComponent(typeof(UISprite))

    local tf=transform:Find("ServerNameLabel")
    local serverNameLabel = nil
    if tf then
        serverNameLabel = tf:GetComponent(typeof(UILabel))
    end

    local markGrid = transform:Find("Grid"):GetComponent(typeof(UIGrid))

    local goPool = FindChild(self.tableView.transform.parent.parent.parent,"ItemPool"):GetComponent(typeof(CUIGameObjectPool))

    local clickRegion = transform:Find("Container").gameObject

    local m_PlayerId = info.playerId
    if row % 2 == 0 then
        bgSprite.alpha = 0
    else
        bgSprite.alpha = 1
    end

    for i = markGrid.transform.childCount - 1, 0, - 1 do
        goPool:Recycle(markGrid.transform:GetChild(i).gameObject)
    end

    nameLabel.text = info.name
    --Debug.Log(info.role);
    --roleLabel.text = info.role;
    fightScoreLabel.text = tostring(info.fightScore)
    occupySocreLabel.text = tostring(info.occupyScore)
    scoreLabel.text = tostring(info.score)
    clsSprite.spriteName = Profession.GetIconByNumber(info.cls)

    -- 是本人
    if info.name == CClientMainPlayer.Inst.Name then
        nameLabel.color = Color.green
        --roleLabel.color = Color.green;
        fightScoreLabel.color = Color.green
        occupySocreLabel.color = Color.green
        scoreLabel.color = Color.green
        if serverNameLabel ~= nil then
            serverNameLabel.color = Color.green
        end
    else
        nameLabel.color = Color.white
        --roleLabel.color = Color.white;
        fightScoreLabel.color = Color.white
        occupySocreLabel.color = Color.white
        scoreLabel.color = Color.white
        if serverNameLabel ~= nil then
            serverNameLabel.color = Color.white
        end
    end

    if serverNameLabel ~= nil then
        if CLuaGuanNingMgr.m_PlayerServerNameCache[info.playerId] then
            local serverName = CLuaGuanNingMgr.m_PlayerServerNameCache[info.playerId]
            serverNameLabel.text = serverName
        elseif CommonDefs.DictContains(CGuanNingMgr.Inst.PlayerServerName, typeof(UInt64), info.playerId) then
            local serverName = CommonDefs.DictGetValue(CGuanNingMgr.Inst.PlayerServerName, typeof(UInt64), info.playerId)
            CLuaGuanNingMgr.m_PlayerServerNameCache[info.playerId] = serverName
            serverNameLabel.text = serverName
        else
            serverNameLabel.text = nil
        end
    end

    if markGrid.transform.childCount > 0 then
        for i = markGrid.transform.childCount, 1, - 1 do
            goPool:Recycle(markGrid.transform:GetChild(i - 1).gameObject)
        end
    end
    if clickRegion ~= nil then
        UIEventListener.Get(clickRegion).onClick = nil
    end

    local hasRecord = false
    local score = CLuaGuanNingMgr.GetPlayerScoreRecord(info.playerId)
    hasRecord = score ~= nil and #score > 0
    if score ~= nil then
        do
            local i = 0
            while i < #score do
                local mark = goPool:GetFromPool(0)
                mark:SetActive(true)
                mark.transform.parent = markGrid.transform
                mark.transform.localScale = Vector3.one
                local sprite = mark.transform:Find("Sprite"):GetComponent(typeof(UISprite))
                sprite.spriteName = CLuaGuanNingMgr.GetScoreRecordSpriteName(score[i+1])
                sprite:ParentHasChanged()
                i = i + 1
            end
        end
        if clickRegion ~= nil and hasRecord then
            UIEventListener.Get(clickRegion).onClick = DelegateFactory.VoidDelegate(function(go)
                CLuaGuanNingMgr.m_SelectedPlayerId=m_PlayerId
                CUIManager.ShowUI(CUIResources.GuanNingPlayerAchievementWnd)
            end)
        end
    end
    markGrid:Reposition()
    -- end
    if hasRecord then
        Extensions.SetLocalPositionY(nameLabel.transform, 20)
    else
        Extensions.SetLocalPositionY(nameLabel.transform, 0)
    end

    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if m_PlayerId <= 10000000 then
            --fakeplayer
            g_MessageMgr:ShowMessage("GuanNing_FakePlayer_NoInfo")
        else
            CPlayerInfoMgr.ShowPlayerPopupMenu(m_PlayerId, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
        end
    end)

    self.id2Item[m_PlayerId] = transform
end

return CLuaGuanNingResultPane
