local CMainCamera = import "L10.Engine.CMainCamera"
local Color = import "UnityEngine.Color"
local UICamera = import "UICamera"
local Physics = import "UnityEngine.Physics"
local CUIFx = import "L10.UI.CUIFx"
local UITexture = import "UITexture"
local Extensions = import "Extensions"
local MessageWndManager = import "L10.UI.MessageWndManager"
local RenderTexture = import "UnityEngine.RenderTexture"
local Screen = import "UnityEngine.Screen"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local TextureWrapMode = import "UnityEngine.TextureWrapMode"
local FilterMode = import "UnityEngine.FilterMode"
local Camera = import "UnityEngine.Camera"
local GameObject = import "UnityEngine.GameObject"
local CameraClearFlags = import "UnityEngine.CameraClearFlags"
local LayerDefine = import "L10.Engine.LayerDefine"
local Quaternion = import "UnityEngine.Quaternion"
local Vector3 = import "UnityEngine.Vector3"
local CPreDrawMgr = import "L10.Engine.CPreDrawMgr"
local Object=import "System.Object"
local MsgPackImpl=import "MsgPackImpl"

LuaDrawDripWnd = class()

RegistChildComponent(LuaDrawDripWnd,"CloseBtn",GameObject)
RegistChildComponent(LuaDrawDripWnd,"ShowTexture",UITexture)
RegistChildComponent(LuaDrawDripWnd,"StarTemplate",GameObject)
RegistChildComponent(LuaDrawDripWnd,"PosNodeFather",GameObject)
RegistChildComponent(LuaDrawDripWnd,"DragMoveNode",GameObject)
RegistChildComponent(LuaDrawDripWnd,"TipTextNode",GameObject)
RegistChildComponent(LuaDrawDripWnd,"LineNodeFather",GameObject)
RegistChildComponent(LuaDrawDripWnd,"DrawFinishFxNode",GameObject)

local function split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

function LuaDrawDripWnd:SetFailed()
  self.DragMoveNode:SetActive(false)

	local lineTrans = self.LineNodeFather.transform
	for i = 1, lineTrans.childCount do
		local lineParent = lineTrans:GetChild(i - 1)
		if lineParent then
			lineParent.gameObject:SetActive(false)
		end
	end

	self.SaveFormerNode = nil

  self:InitStarNode()
end

function LuaDrawDripWnd:startDraw(node)
	self.DragMoveNode:SetActive(true)
	self.DragMoveNode.transform.position = UICamera.currentTouch.pos
	local _trans = self.DragMoveNode.transform
	local currentPos = UICamera.currentTouch.pos
	_trans.position = UICamera.currentCamera:ScreenToWorldPoint(Vector3(currentPos.x,currentPos.y,0))
  self:SetColor(node,Color.blue)
end

function LuaDrawDripWnd:CheckSucc()
	local lineTrans = self.LineNodeFather.transform
	for i = 1, lineTrans.childCount do
		local lineParent = lineTrans:GetChild(i - 1)
		if lineParent then
			if not lineParent.gameObject.activeSelf then
				return
			end
		end
	end

--   self.DrawFinishFxNode:GetComponent(typeof(CUIFx)):LoadFx(LuaWorldEventMgr.DrawFinishFx)

  --Gac2Gas.FinishLianXingXingTask(CWorldEventMgr.Inst.xinxinTaskId)
  local empty = CreateFromClass(MakeGenericClass(List, Object))
  empty = MsgPackImpl.pack(empty)
  Gac2Gas.FinishClientTaskEvent(LuaWorldEventMgr.DrawTaskId or 0, LuaWorldEventMgr.DrawEventName or "",empty)

	self.transform:Find("YuDi").gameObject:SetActive(true)
	self.DragMoveNode:SetActive(false)

  self.transform:Find("Fx").gameObject:SetActive(true)
  self.TipTextNode:SetActive(false)
  self.LineNodeFather:SetActive(false)
  self.transform:Find("PosNodeFather").gameObject:SetActive(false)

  RegisterTickOnce(function ()
    CUIManager.CloseUI(CLuaUIResources.DrawDripWnd)
  end,3000)
end

function LuaDrawDripWnd:SetColor(node,m_color)
  -- node.transform:Find("node"):GetComponent(typeof(UISprite)).color = m_color
  -- node.transform:Find("node/Sprite"):GetComponent(typeof(UISprite)).color = m_color
end

function LuaDrawDripWnd:judgePass(node)
	if not self.SaveFormerNode then
		return
	end

	if node and node.transform.parent then
		local nodeNum = tonumber(node.transform.parent.name)
		if not nodeNum then
			return
		end
		if self.SaveFormerNode == nodeNum then
			return
		end
		local lineId = nil
        for i, line in ipairs(self.MatchTable) do
            if line[1] == nodeNum and line[2] == self.SaveFormerNode then
                lineId = line[3]
                break
            end
        end
		if lineId then
            self:SetColor(node,Color.blue)
			local node = self.LineNodeFather.transform:GetChild(lineId)
			if node then
				if node.gameObject.activeSelf then
					self:SetFailed()
				else
					self.SaveFormerNode = nodeNum
					node.gameObject:SetActive(true)
					self:CheckSucc()
				end
			end
		else
			self:SetFailed()
		end
	end
end

function LuaDrawDripWnd:endDraw(node)
	self.DragMoveNode:SetActive(false)

	local lineTrans = self.LineNodeFather.transform
	for i = 1, lineTrans.childCount do
		local lineParent = lineTrans:GetChild(i - 1)
		if lineParent then
			lineParent.gameObject:SetActive(false)
		end
	end

  self:InitStarNode()
end

function LuaDrawDripWnd:setStarNode(dragNode,index)
	local onDrag = function(go,delta)
		local _trans = self.DragMoveNode.transform
		local currentPos = UICamera.currentTouch.pos
		_trans.position = UICamera.currentCamera:ScreenToWorldPoint(Vector3(currentPos.x,currentPos.y,0))

		local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
		local hits = Physics.RaycastAll(ray, 99999,  UICamera.currentCamera.cullingMask)

		for i =0,hits.Length - 1 do
			if hits[i].collider.gameObject.transform.parent then
				self:judgePass(hits[i].collider.gameObject)
			end
		end
	end

	local onPress = function(go,flag)
		if flag then
			local startNum = tonumber(go.transform.parent.name)
			if startNum then
				self:startDraw(go)
				self.SaveFormerNode = startNum
			end
		else
			self:endDraw(go)
		end
	end
	CommonDefs.AddOnDragListener(dragNode,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnPressListener(dragNode,DelegateFactory.Action_GameObject_bool(onPress),false)
end


function LuaDrawDripWnd:GenerateStarNode(parentNodeTrans)
	local go = CUICommonDef.AddChild(parentNodeTrans.gameObject,self.StarTemplate)
	go:SetActive(true)
	self:setStarNode(go,parentNodeTrans.name)
end

function LuaDrawDripWnd:InitStarNode()
	local trans = self.PosNodeFather.transform
	for i =1, trans.childCount do
		local parent = trans:GetChild(i-1)
		if parent then
			Extensions.RemoveAllChildren(parent)
			self:GenerateStarNode(parent)
		end
	end


end

local cameraNode = nil

function LuaDrawDripWnd:Awake()
	self.transform:Find("YuDi").gameObject:SetActive(false)
end

function LuaDrawDripWnd:Init()
	local onCloseClick = function(go)
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("CUSTOM_STRING2",LocalString.GetString("确认退出吗?")), DelegateFactory.Action(function ()
      CUIManager.CloseUI(CLuaUIResources.DrawDripWnd)
    end), nil, nil, nil, false)
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	self.StarTemplate:SetActive(false)
	self.DragMoveNode:SetActive(false)

  CPreDrawMgr.m_bEnableRectPreDraw = false

	if 1920 / Screen.width > 1080 / Screen.height then
		self.ShowTexture.width = 1920--Screen.width
		self.ShowTexture.height = Screen.height * 1920 / Screen.width--Screen.height
	else
		self.ShowTexture.width = Screen.width * 1080 / Screen.height--Screen.width
		self.ShowTexture.height = 1080--Screen.height
	end

	local renderTex = RenderTexture(self.ShowTexture.width,self.ShowTexture.height,32,RenderTextureFormat.ARGB32,RenderTextureReadWrite.Default)
	renderTex.wrapMode = TextureWrapMode.Clamp
	renderTex.filterMode = FilterMode.Bilinear
	renderTex.anisoLevel = 2
	self.ShowTexture.mainTexture = renderTex

	cameraNode = GameObject("__WorldEventDrawLineCamera__")
	local cameraScript = CommonDefs.AddCameraComponent(cameraNode)
    CMainCamera.Inst:SetCameraEnableStatus(false,"use_another_camera", false)
	cameraScript.clearFlags = CameraClearFlags.Skybox
  --cameraScript.backgroundColor = Color(49 / 255, 77 / 255, 121 / 255, 5 / 255)
    cameraScript.cullingMask = 2^LayerDefine.ModelForNGUI_3D + 2^LayerDefine.Effect_3D + 2^LayerDefine.Default + 2^LayerDefine.Water + 2^LayerDefine.WaterReflection + 2^LayerDefine.Terrain
    cameraScript.orthographic = false
    cameraScript.fieldOfView = 60
    cameraScript.farClipPlane = 140
    cameraScript.nearClipPlane = 0.1
    cameraScript.depth = -10
    cameraScript.transform.parent = CUIManager.instance.transform

    cameraNode.transform.position = Vector3(15,25,10)
    cameraNode.transform.rotation = Quaternion.Euler(-7.1,0,0)
  --cameraScript.transform.rotation = Quaternion.LookRotation(Vector3(selfData.kitePos[1] - selfData.playerPos[1],selfData.kitePos[2] - selfData.playerPos[2],selfData.kitePos[3] - selfData.playerPos[3]))

	cameraScript.targetTexture = renderTex
    self.DragMoveNode.transform:Find("FxNode"):GetComponent(typeof(CUIFx)):LoadFx(LuaWorldEventMgr.DrawMoveNodeFx)
	--
	self:InitStarNode()

    -- 初始化线段
    self.MatchTable = {}
    for i = 0, self.LineNodeFather.transform.childCount-1 do
        local line = self.LineNodeFather.transform:GetChild(i)
        line.gameObject:SetActive(false)
        local fun = string.gmatch(line.name, "(%d)[_-](%d)")
        local a, b = fun()
        a = tonumber(a)
        b = tonumber(b)
        table.insert(self.MatchTable, {a, b, i})
        table.insert(self.MatchTable, {b, a, i})
    end

end

function LuaDrawDripWnd:OnDestroy()
    CMainCamera.Inst:SetCameraEnableStatus(true,"use_another_camera", false)
	CPreDrawMgr.m_bEnableRectPreDraw = true
	if cameraNode then
		GameObject.Destroy(cameraNode)
	end
end

