-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CMyDuobaoItem = import "L10.UI.CMyDuobaoItem"
local DelegateFactory = import "DelegateFactory"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
CMyDuobaoItem.m_UpdateData_CS2LuaHook = function (this, status, info) 
    this.m_YiKouJiaObj:SetActive(info.Need_FenShu == 1)
    this.m_TemplateId = info.ItemID
    this.m_ItemNameLabel.text = info.ItemName
    this.m_HasInvestLabel.text = System.String.Format(LocalString.GetString("已投入{0}份"), info.MyInvestInFenShu)

    local data = Item_Item.GetData(info.ItemID)
    if data ~= nil then
        this.m_Texture:LoadMaterial(data.Icon)
    end
    if info.IsPublished then
        if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id == info.WinnerId then
            this.m_StatusLabel.text = LocalString.GetString("[00ff00]已获得[-]")
        else
            this.m_StatusLabel.text = LocalString.GetString("[949392]夺宝失败[-]")
        end
    else
        this.m_StatusLabel.text = LocalString.GetString("[ffff00]等待揭晓[-]")
    end
    this.m_CrossServerObj.gameObject:SetActive(info.IsCrossServer ~= 0)
end
CMyDuobaoItem.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_Texture.gameObject).onClick = DelegateFactory.VoidDelegate(function (go) 
        local item = CItemMgr.Inst:GetItemTemplate(this.m_TemplateId)
        CItemInfoMgr.ShowLinkItemTemplateInfo(this.m_TemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
        if this.OnClickItemTexture ~= nil then
            invoke(this.OnClickItemTexture)
        end
    end)
end
