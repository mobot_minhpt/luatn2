local EnumQMPKPlayStage = import "L10.Game.EnumQMPKPlayStage"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local QnTableView=import "L10.UI.QnTableView"
local QnButton=import "L10.UI.QnButton"
local CCommonSelector=import "L10.UI.CCommonSelector"
local EnumQMPKZhanduiMemberWndType=import "L10.Game.EnumQMPKZhanduiMemberWndType"

CLuaQMPKTaoTaiSaiMatchRecordWnd = class()
RegistClassMember(CLuaQMPKTaoTaiSaiMatchRecordWnd,"m_WatchBtn")
RegistClassMember(CLuaQMPKTaoTaiSaiMatchRecordWnd,"m_AdvGridView")
RegistClassMember(CLuaQMPKTaoTaiSaiMatchRecordWnd,"m_TitleLabel")
RegistClassMember(CLuaQMPKTaoTaiSaiMatchRecordWnd,"m_TimeLabel")
RegistClassMember(CLuaQMPKTaoTaiSaiMatchRecordWnd,"m_Selector")
RegistClassMember(CLuaQMPKTaoTaiSaiMatchRecordWnd,"m_DescLab")
RegistClassMember(CLuaQMPKTaoTaiSaiMatchRecordWnd,"m_JueSaiIndex")
-- RegistClassMember(CLuaQMPKTaoTaiSaiMatchRecordWnd,"m_RefreshTick")
RegistClassMember(CLuaQMPKTaoTaiSaiMatchRecordWnd,"m_WndType")
RegistClassMember(CLuaQMPKTaoTaiSaiMatchRecordWnd,"m_CurrentRow")

RegistClassMember(CLuaQMPKTaoTaiSaiMatchRecordWnd,"m_DataSource")
RegistClassMember(CLuaQMPKTaoTaiSaiMatchRecordWnd,"m_TaoTaiSaiTimeStr")
RegistClassMember(CLuaQMPKTaoTaiSaiMatchRecordWnd,"m_ZongJueSaiTimeStr")
RegistClassMember(CLuaQMPKTaoTaiSaiMatchRecordWnd,"m_StageNameList")
RegistClassMember(CLuaQMPKTaoTaiSaiMatchRecordWnd,"m_JueSaiNameList")
RegistClassMember(CLuaQMPKTaoTaiSaiMatchRecordWnd,"m_CombineNameList")
RegistClassMember(CLuaQMPKTaoTaiSaiMatchRecordWnd,"m_MatchNum")
RegistClassMember(CLuaQMPKTaoTaiSaiMatchRecordWnd,"m_MyZhanDuiId")
RegistClassMember(CLuaQMPKTaoTaiSaiMatchRecordWnd,"m_SelectIndex")

function CLuaQMPKTaoTaiSaiMatchRecordWnd:Awake()
    self.m_WatchBtn = self.transform:Find("ShowArea/ViewButton"):GetComponent(typeof(QnButton))
    self.m_AdvGridView = self.transform:Find("ShowArea/QnAdvView"):GetComponent(typeof(QnTableView))
    self.m_TitleLabel = self.transform:Find("Wnd_Bg_Primary_Normal/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_TimeLabel = self.transform:Find("ShowArea/TopArea/TimeLabel"):GetComponent(typeof(UILabel))
    self.m_Selector = self.transform:Find("ShowArea/TopArea/Selector"):GetComponent(typeof(CCommonSelector))
    self.m_DescLab = self.transform:Find("ShowArea/DescLab"):GetComponent(typeof(UILabel))
    self.m_SelectIndex = 0
    --self.m_RefreshTick = ?
    --self.m_WndType = ?
    self.m_CurrentRow = -1

    self.m_StageNameList={
        LocalString.GetString("64进32"),
        LocalString.GetString("32进16"),
        LocalString.GetString("16进8"),
        LocalString.GetString("8进4"),
    }
    self.m_JueSaiNameList={
        LocalString.GetString("半决赛第一场"),
        LocalString.GetString("半决赛第二场"),
        LocalString.GetString("季军赛"),
        LocalString.GetString("总决赛第一场"),
        LocalString.GetString("总决赛第二场"),
        LocalString.GetString("总决赛第三场")
    }
    self.m_MatchNum={
        32,
        16,
        8,
        4,
        1,
        1,
        1,
        1,
        1,
        1,
    }
    self.m_CombineNameList = {}
    for i = 1, #self.m_StageNameList do
        table.insert(self.m_CombineNameList, self.m_StageNameList[i])
    end
    for i = 1, #self.m_JueSaiNameList do
        table.insert(self.m_CombineNameList, self.m_JueSaiNameList[i])
    end
    self.m_TaoTaiSaiTimeStr = QuanMinPK_Setting.GetData().TaoTaiSaiTime
    self.m_ZongJueSaiTimeStr = QuanMinPK_Setting.GetData().ZongJueSaiTime
    self.m_MyZhanDuiId = CQuanMinPKMgr.Inst.m_MyPersonalInfo and CQuanMinPKMgr.Inst.m_MyPersonalInfo.m_ZhanDuiId or 0
    self.m_DescLab.text = ""

    local getNumFunc=function() return #CLuaQMPKMgr.m_TaoTaiSaiRecordList end
    local initItemFunc=function(item,index)
        item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
        self:InitItem(item,index,CLuaQMPKMgr.m_TaoTaiSaiRecordList[index+1])
    end

    self.m_DataSource=DefaultTableViewDataSource.Create(getNumFunc,initItemFunc)
    self.m_AdvGridView.m_DataSource=self.m_DataSource
    self.m_AdvGridView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self.m_CurrentRow = row
        if CLuaQMPKMgr.m_TaoTaiSaiRecordList[self.m_CurrentRow+1].m_Invalid ~= true 
        and CLuaQMPKMgr.m_TaoTaiSaiRecordList[self.m_CurrentRow+1].m_Win <= 0
            and CLuaQMPKMgr.m_TaoTaiSaiRecordList[self.m_CurrentRow+1].m_IsMatching then
            self.m_WatchBtn.Enabled = true
        else
            self.m_WatchBtn.Enabled = false
        end
    end)

    local refreshButton=FindChild(self.transform,"RefreshButton").gameObject
    UIEventListener.Get(refreshButton).onClick=DelegateFactory.VoidDelegate(function(go)
        self:RequestQmpkWatchList(self.m_SelectIndex)
    end)
end

function CLuaQMPKTaoTaiSaiMatchRecordWnd:OnEnable( )
    UIEventListener.Get(self.m_WatchBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnWatchBtnClicked(go) end)
    g_ScriptEvent:AddListener("ReplyQmpkTaoTaiSaiWatchList", self, "OnReplyQmpkTaoTaiSaiWatchList")
    g_ScriptEvent:AddListener("QMPKTaoTaiSaiStatusResult", self, "RealInit")
end
function CLuaQMPKTaoTaiSaiMatchRecordWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("ReplyQmpkTaoTaiSaiWatchList", self, "OnReplyQmpkTaoTaiSaiWatchList")
    g_ScriptEvent:RemoveListener("QMPKTaoTaiSaiStatusResult", self, "RealInit")
end

function CLuaQMPKTaoTaiSaiMatchRecordWnd:OnReplyQmpkTaoTaiSaiWatchList(stage, isTaoTaiSai)
    if isTaoTaiSai == true then
        self.m_SelectIndex = stage
        self.m_TitleLabel.text = LocalString.GetString("淘汰赛观战")
        self.m_TimeLabel.text = SafeStringFormat3("[c][ffffff]%s[-][/c]", LocalString.GetString("比赛时间：")) .. self.m_TaoTaiSaiTimeStr
    else
        self.m_TitleLabel.text = LocalString.GetString("总决赛观战")
        self.m_TimeLabel.text = SafeStringFormat3("[c][ffffff]%s[-][/c]", LocalString.GetString("比赛时间：")) .. self.m_ZongJueSaiTimeStr
        self.m_SelectIndex = stage + #self.m_StageNameList
    end
    -- 淘汰赛未产生双方信息,或者未公布对手
    if isTaoTaiSai and (#CLuaQMPKMgr.m_TaoTaiSaiRecordList == 0 or CLuaQMPKMgr.m_TaoTaiSaiRecordList[1].m_Zhandui2 == -1) then
        local month, time
        local days = {}
        for a, b, c, d, e, f in string.gmatch(self.m_TaoTaiSaiTimeStr, "(%d+).-(%d+).-(%d+).-(%d+).-(%d+).+ (%d+:%d+)") do --8月4、6、11、13日 19:00-20:30
            month = a
            days[1] = b
            days[2] = c
            days[3] = d
            days[4] = e
            time = f
        end
        self.m_DescLab.text = SafeStringFormat3(LocalString.GetString("双方对阵信息将于%s月%s日%s公布"), month, days[self.m_SelectIndex], time)
    else 
        self.m_DescLab.text = ""
    end
    -- 未产生双方信息,插入假数据
    if #CLuaQMPKMgr.m_TaoTaiSaiRecordList == 0 then
        for i=1, self.m_MatchNum[self.m_SelectIndex] do
            table.insert(CLuaQMPKMgr.m_TaoTaiSaiRecordList, {m_Invalid = true})
        end
    end
    self.m_Selector:SetName(self.m_CombineNameList[self.m_SelectIndex])
    self.m_WatchBtn.Enabled = false
    self.m_AdvGridView:ReloadData(false, false)
end
function CLuaQMPKTaoTaiSaiMatchRecordWnd:Init( )
    self.m_WndType = CQuanMinPKMgr.Inst.m_WatchListWndType

    if self.m_WndType == EnumQMPKPlayStage.eTaoTaiSai then
        -- 淘汰赛查询一下当前淘汰赛stage
        Gac2Gas.QueryQmpkTaoTaiSaiOverView(0)
    elseif self.m_WndType == EnumQMPKPlayStage.eZongJueSai then
        -- 总决赛
        self:RealInit()
    end
end

function CLuaQMPKTaoTaiSaiMatchRecordWnd:RealInit()
    self.m_SelectIndex = 1

    self.m_WatchBtn.Enabled = false
    if self.m_WndType == EnumQMPKPlayStage.eTaoTaiSai then
        self.m_TitleLabel.text = LocalString.GetString("淘汰赛观战")
        self.m_TimeLabel.text = SafeStringFormat3("[c][ffffff]%s[-][/c]", LocalString.GetString("比赛时间：")) .. self.m_TaoTaiSaiTimeStr
        if CQuanMinPKMgr.Inst.m_CurrentTaoTaiSaiStage > 0
            and CQuanMinPKMgr.Inst.m_CurrentTaoTaiSaiStage <= #self.m_StageNameList then
            self.m_SelectIndex = CQuanMinPKMgr.Inst.m_CurrentTaoTaiSaiStage
        else 
            self.m_SelectIndex = 1
        end
    elseif self.m_WndType == EnumQMPKPlayStage.eZongJueSai then
        self.m_TitleLabel.text = LocalString.GetString("总决赛观战")
        self.m_TimeLabel.text = SafeStringFormat3("[c][ffffff]%s[-][/c]", LocalString.GetString("比赛时间：")) .. self.m_ZongJueSaiTimeStr
        self.m_SelectIndex = #self.m_StageNameList + 99
    end
    self:RequestQmpkWatchList(self.m_SelectIndex)

    local List_String = MakeGenericClass(List,cs_string)
    self.m_Selector:Init(Table2List(self.m_CombineNameList,List_String),
            DelegateFactory.Action_int(function(index) self:OnStageClick(index) end))

    self.m_Selector:SetName("")
end

function CLuaQMPKTaoTaiSaiMatchRecordWnd:OnStageClick(index)
    self:RequestQmpkWatchList(index + 1)
end

function CLuaQMPKTaoTaiSaiMatchRecordWnd:RequestQmpkWatchList(index)
    if index <= #self.m_StageNameList then
        Gac2Gas.RequestQmpkTaoTaiSaiWatchList(index)

    else 
        Gac2Gas.RequestQmpkZongJueSaiWatchList(index - #self.m_StageNameList)

    end
end

function CLuaQMPKTaoTaiSaiMatchRecordWnd:OnWatchBtnClicked( go)
    self:RequestWatchQmpk(self.m_SelectIndex)
end

function CLuaQMPKTaoTaiSaiMatchRecordWnd:RequestWatchQmpk(index)
    if index <= #self.m_StageNameList then
        local rank = CLuaQMPKMgr.m_TaoTaiSaiRecordList[self.m_CurrentRow+1].m_Rank
        Gac2Gas.RequestWatchQmpkTaoTaoSai(index, rank)

    else 
        Gac2Gas.RequestWatchQmpkZongJueSai(index - #self.m_StageNameList)

    end
end

function CLuaQMPKTaoTaiSaiMatchRecordWnd:InitItem(item,index,record)
    local transform=item.transform
    local m_NameLabel={}
    m_NameLabel[0]=FindChild(transform,"NameLabel1"):GetComponent(typeof(UILabel))
    m_NameLabel[1]=FindChild(transform,"NameLabel2"):GetComponent(typeof(UILabel))
    local m_DescLabel={}
    m_DescLabel[0]=FindChild(transform,"Desc1"):GetComponent(typeof(UILabel))
    m_DescLabel[1]=FindChild(transform,"Desc2"):GetComponent(typeof(UILabel))
    local m_ResultSprite={}
    m_ResultSprite[0]=FindChild(transform,"ResultSprite1"):GetComponent(typeof(UISprite))
    m_ResultSprite[1]=FindChild(transform,"ResultSprite2"):GetComponent(typeof(UISprite))
    local m_ScoreLabel={}
    m_ScoreLabel[0]=FindChild(transform,"ScoreLabel1"):GetComponent(typeof(UILabel))
    m_ScoreLabel[1]=FindChild(transform,"ScoreLabel2"):GetComponent(typeof(UILabel))

    local m_MatchingLabel = transform:Find("Label"):GetComponent(typeof(UILabel))
    local m_VSSprite = transform:Find("VSSprite"):GetComponent(typeof(UISprite))
    local m_FightingSpite = transform:Find("FightingSprite"):GetComponent(typeof(UISprite))

    UIEventListener.Get(m_NameLabel[0].gameObject).onClick=DelegateFactory.VoidDelegate(function(p)
        CQuanMinPKMgr.Inst:ShowZhanDuiMemberWnd(0, EnumQMPKZhanduiMemberWndType.eOneZhandui, record.m_Zhandui1,true)
    end)
    UIEventListener.Get(m_NameLabel[1].gameObject).onClick=DelegateFactory.VoidDelegate(function(p)
        CQuanMinPKMgr.Inst:ShowZhanDuiMemberWnd(0, EnumQMPKZhanduiMemberWndType.eOneZhandui, record.m_Zhandui2,true)
    end)

    local teamName1 = record.m_TeamName1 and record.m_TeamName1 or ""
    local teamName2 = record.m_TeamName2 and record.m_TeamName2 or ""
    m_NameLabel[0].text = (record.m_Zhandui1 and record.m_Zhandui1 == self.m_MyZhanDuiId) and SafeStringFormat3("[c][00ff60]%s[-][/c]", teamName1) or teamName1
    m_NameLabel[1].text = (record.m_Zhandui2 and record.m_Zhandui2 == self.m_MyZhanDuiId) and SafeStringFormat3("[c][00ff60]%s[-][/c]", teamName2) or teamName2

    if record.m_Invalid == true or record.m_Zhandui2 == -1 then
        m_ResultSprite[0].gameObject:SetActive(false)
        m_ResultSprite[1].gameObject:SetActive(false)
        m_VSSprite.gameObject:SetActive(true)
        m_MatchingLabel.gameObject:SetActive(false)
        m_FightingSpite.gameObject:SetActive(false)
        m_ScoreLabel[0].text = ""
        m_ScoreLabel[1].text = ""
        -- 未产生双方信息
        if record.m_Invalid == true then
            m_NameLabel[0].gameObject:SetActive(false)
            m_NameLabel[1].gameObject:SetActive(false)
            m_DescLabel[0].gameObject:SetActive(true)
            m_DescLabel[1].gameObject:SetActive(true)
            m_DescLabel[0].text = LocalString.GetString("暂未产生")
            m_DescLabel[1].text = LocalString.GetString("暂未产生")
        end
        -- 未公布对手信息
        if record.m_Zhandui2 == -1 then
            m_NameLabel[0].gameObject:SetActive(true)
            m_NameLabel[1].gameObject:SetActive(false)
            m_DescLabel[0].gameObject:SetActive(false)
            m_DescLabel[1].gameObject:SetActive(true)
            m_DescLabel[1].text = LocalString.GetString("暂未公布")
        end

        return
    end
    m_NameLabel[0].gameObject:SetActive(true)
    m_NameLabel[1].gameObject:SetActive(true)

    m_DescLabel[0].gameObject:SetActive(false)
    m_DescLabel[1].gameObject:SetActive(false)

    m_ScoreLabel[0].text = record.m_Score1 and record.m_Score1 or ""
    m_ScoreLabel[1].text = record.m_Score2 and record.m_Score2 or ""

    if record.m_Win >= 0 then
        m_ResultSprite[0].gameObject:SetActive(true)
        m_ResultSprite[1].gameObject:SetActive(true)
        m_VSSprite.gameObject:SetActive(true)
        m_MatchingLabel.gameObject:SetActive(false)
        m_FightingSpite.gameObject:SetActive(false)

        local default
        if record.m_Win == record.m_Zhandui1 then
            default = "common_fight_result_win"
        else
            default = "common_fight_result_lose"
        end
        m_ResultSprite[0].spriteName = default

        local extern
        if record.m_Win == record.m_Zhandui2 then
            extern = "common_fight_result_win"
        else
            extern = "common_fight_result_lose"
        end
        m_ResultSprite[1].spriteName = extern
    else
        m_ResultSprite[0].gameObject:SetActive(false)
        m_ResultSprite[1].gameObject:SetActive(false)
        m_VSSprite.gameObject:SetActive(false)
        if record.m_IsMatching then
            m_MatchingLabel.gameObject:SetActive(false)
            m_FightingSpite.gameObject:SetActive(true)
        else
            m_MatchingLabel.gameObject:SetActive(true)
            m_FightingSpite.gameObject:SetActive(false)
        end
    end
end
return CLuaQMPKTaoTaiSaiMatchRecordWnd
