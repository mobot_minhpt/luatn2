local CAuctionMgr = import "L10.Game.CAuctionMgr"
local CAuctionItemInfo = import "L10.Game.CAuctionItemInfo"

LuaAuctionMgr = {}

--@desc 是否需要显示起拍价
function LuaAuctionMgr.NeedShowStartPrice(itemId)
    local setting = Paimai_Setting.GetData()
    local index = CommonDefs.Array_IndexOf_uint(setting.BiddableItemId, itemId)
    return index >= 0
end

--@desc 获得物品的起拍价(计算方式与服务器同步)
function LuaAuctionMgr:GetItemStartPrice(item)
    if not item then return 0 end

    local silverPrice = nil
    if not item.IsPrecious then
        silverPrice = item.PlayerShopPrice
    else
        silverPrice = item.ValuablesFloorPrice
    end
    local setting = Paimai_Setting.GetData()

    return silverPrice and math.ceil((silverPrice/setting.LingyuExchangeSilver) / 2)
end

LuaAuctionMgr.m_PaimaiMySystemItems = {}
LuaAuctionMgr.m_CurSelectItem = nil
LuaAuctionMgr.m_TakeOffPriceItemAuctionID = ""

LuaAuctionMgr.KouDaoGamePlayId = 51100170        -- 寇岛玩法ID
LuaAuctionMgr.HengYuDangKouGamePlayId = 51103268 -- 横屿荡寇玩法ID

function LuaAuctionMgr:IsEnableNewKouDaoAuction()
    return CommonDefs.GetPlayConfigValue("Paimai") == 2
end

-- 当前服务器是否开启横屿荡寇活动拍卖
function LuaAuctionMgr:IsEnableHengYuDangKouAuction()
    return LuaHengYuDangKouMgr:IsHengYuDangKouEnable()
end

-- 查询 我的竞拍 的结果
-- Gac2Gas.QueryPaimaiMySystemItems
-- 收到这个之后，接下来开始循环收到 SendPaimaiMySystemItems
function LuaAuctionMgr:SendPaimaiMySystemItemsBegin()
	print("SendPaimaiMySystemItemsBegin")
    self.m_PaimaiMySystemItems = {}
end

-- 玩家可能会投入很多组，这个RPC每次返回一部分, 直到收到 SendPaimaiMySystemItemsEnd 代表接收完毕
function LuaAuctionMgr:SendPaimaiMySystemItems(infoRpcUd)
	print("SendPaimaiMySystemItems")
	local infoData = CSystemPaimaiMyPaimaiRPC()
    infoData:LoadFromString(infoRpcUd)
    self.m_PaimaiMySystemItems = self.m_PaimaiMySystemItems or {}
    CommonDefs.DictIterate(infoData.Infos, DelegateFactory.Action_object_object(function (___key, ___value)
		-- print(___value.TemplateId)      -- 物品模板Id
		-- print(___value.ItemCount)       -- 库存
		-- print(___value.Price)           -- 当前价格
        local TemplateId = ___value.TemplateId
        if not self.m_PaimaiMySystemItems[TemplateId] then
            self.m_PaimaiMySystemItems[TemplateId]= {}
        end
        table.insert(self.m_PaimaiMySystemItems[TemplateId],  ___value)
	end))
end

function LuaAuctionMgr:SendPaimaiMySystemItemsEnd()
	print("SendPaimaiMySystemItemsEnd")
    if CUIManager.IsLoaded(CLuaUIResources.MyAuctionWnd) then
        g_ScriptEvent:BroadcastInLua("OnSendPaimaiMySystemItemsEnd")
        return 
    end
    CUIManager.ShowUI(CLuaUIResources.MyAuctionWnd)
end

-- 根据价格查询库存
-- Gac2Gas.QueryPaimaiSystemItemByPrice
function LuaAuctionMgr:SendPaimaiSystemItemByPrice(itemTemplateId, oneShootPrice, currentlowestPrice, takeoffPrice, price, hasCount)
    print("SendPaimaiSystemItemByPrice", itemTemplateId, oneShootPrice, currentlowestPrice, takeoffPrice, price, hasCount)
    g_ScriptEvent:BroadcastInLua("OnSendPaimaiSystemItemByPrice", itemTemplateId, oneShootPrice, currentlowestPrice, takeoffPrice, price, hasCount, nil)
    --EventManager.BroadcastInternalForLua(EnumEventType.OnAuctionItemPriceRefresh, {self.m_CurSelectItem and self.m_CurSelectItem.AuctionID or "",price, price})
end

-- 根据价格查询库存（GamePlay版）
-- Gac2Gas.QueryGamePlayPaiMaiItemByPrice
-- Gas2Gac.SendGamePlayPaiMaiItemInfoByPrice
function LuaAuctionMgr:SendGamePlayPaiMaiItemInfoByPrice(itemTemplateId, oneShootPrice, currentLowestPrice, takeoffPrice, price, hasCount, extraValue)
    g_ScriptEvent:BroadcastInLua("OnSendPaimaiSystemItemByPrice", itemTemplateId, oneShootPrice, currentLowestPrice, takeoffPrice, price, hasCount, extraValue)
end

-- Gac2Gas.QueryPaimaiSystemOnShelfItem -> 
-- 如果每页展示8个，那么第一页的RPC参数为
-- beginIndex = 1, endIndex = 8, pageNum = 1, itemTemplateId = 模板Id
-- validTotalCount: 总数(用以计算总页数)
-- pageNum: 当前第几页
-- itemTemplateId: 物品模板Id
function LuaAuctionMgr:SendSystemOnShelfItem(validTotalCount, infoRpcUd, pageNum, itemTemplateId, currentLowestPrice)
    CAuctionMgr.Inst.PageIndex = pageNum
    CommonDefs.ListClear(CAuctionMgr.Inst.CurrentItemInfoList)
    local infoData = CSystemPaimaiOnshelfRPC()
    infoData:LoadFromString(infoRpcUd)
    local list = {}
    self.m_TakeOffPriceItemAuctionID = ""
    CommonDefs.DictIterate(infoData.Infos, DelegateFactory.Action_object_object(function(___key, ___value)
        -- print(___value.TemplateId)      -- 物品模板Id
        -- print(___value.ItemCount)       -- 库存
        -- print(___value.EndTime)         -- 结束时间戳
        -- print(___value.Price)           -- 当前价格
        -- print(___value.TakeOffPrice)    -- 起拍价
        -- print(___value.OneShootPrice)   --一口价
        -- print(___value.HasBet)          -- 玩家自己是否参与竞拍
        -- print(___value.PlayId)          -- 扩展用，后面策划会设计其他玩法的拍卖，这个对应的就是GamePlay 表中的id
        local auctionID, instanceID = "SystemOnShelfItem" .. tostring(___key), ""
        local templateID = CommonDefs.Convert_ToUInt32(___value.TemplateId)
        local price = CommonDefs.Convert_ToUInt32(___value.Price)
        local fixPrice = CommonDefs.Convert_ToUInt32(___value.OneShootPrice)
        local startPrice = CommonDefs.Convert_ToUInt32(___value.TakeOffPrice)
        local selfPrice = 0
        local endTime = CommonDefs.Convert_ToUInt32(___value.EndTime)
        -- type==3 使用新拍卖界面 NewAuctionBidWnd
        local type, count = 3, CommonDefs.Convert_ToUInt32(___value.ItemCount)
        local item = CAuctionItemInfo(auctionID, templateID, instanceID, price, fixPrice, startPrice, selfPrice, endTime, "", type, count, 0, self.KouDaoGamePlayId, currentLowestPrice)
        table.insert(list, item)
        --CommonDefs.ListAdd(CAuctionMgr.Inst.CurrentItemInfoList, typeof(CAuctionItemInfo), item)
        if startPrice == ___value.Price then
            self.m_TakeOffPriceItemAuctionID = auctionID
        end
    end))
    table.sort(list, function(a, b)
        return a.Price < b.Price
    end)
    CAuctionMgr.Inst.TotalCount = validTotalCount -- #list
    CAuctionMgr.Inst.CurrentItemInfoList = Table2List(list, MakeGenericClass(List, CAuctionItemInfo))
    g_ScriptEvent:BroadcastInLua("OnSendSystemOnShelfItem", validTotalCount, pageNum, itemTemplateId, currentLowestPrice)
end

-- 请求玩法拍卖上架商品（GamePlay版）
-- Gac2Gas.QueryGamePlayPaiMaiOnShelfItem
-- Gas2Gac.SendGamePlayPaiMaiOnShelfItem
function LuaAuctionMgr:SendGamePlayPaiMaiOnShelfItem(targetType, playId, validTotalCount, infoRpcUd, pageNum, itemTemplateId, onShelfCurCount, onShelfTotalCount)
    CAuctionMgr.Inst.PageIndex = pageNum
    CommonDefs.ListClear(CAuctionMgr.Inst.CurrentItemInfoList)
    local infoData = CGamePlayPaiMaiOnShelfRPC()
    infoData:LoadFromString(infoRpcUd)
    local list = {}
    self.m_TakeOffPriceItemAuctionID = ""
    CommonDefs.DictIterate(infoData.Infos, DelegateFactory.Action_object_object(function(___key, ___value)
        -- print(___value.TemplateId)         -- 物品模板Id
        -- print(___value.ItemCount)          -- 库存
        -- print(___value.EndTime)            -- 结束时间戳
        -- print(___value.Price)              -- 当前价格
        -- print(___value.TakeOffPrice)       -- 起拍价
        -- print(___value.OneShootPrice)      --一口价
        -- print(___value.HasBet)             -- 玩家自己是否参与竞拍
        -- print(___value.PlayId)             -- 扩展用，后面策划会设计其他玩法的拍卖，这个对应的就是GamePlay 表中的id
        -- print(___value.CurrentLowestPrice) -- 当前最低价
        -- print(___value.PaiMaiId)           -- 拍卖Id，用于后续拍卖接口
        -- print(___value.InstanceId)         -- 实例Id
        local auctionID, instanceID = ___value.PaiMaiId, ___value.InstanceId
        local templateID = CommonDefs.Convert_ToUInt32(___value.TemplateId)
        local price = CommonDefs.Convert_ToUInt32(___value.Price)
        local fixPrice = CommonDefs.Convert_ToUInt32(___value.OneShootPrice)
        local startPrice = CommonDefs.Convert_ToUInt32(___value.TakeOffPrice)
        local selfPrice = 0
        local endTime = CommonDefs.Convert_ToUInt32(___value.EndTime)
        -- type==3 使用新拍卖界面 NewAuctionBidWnd
        local type, count = 3, CommonDefs.Convert_ToUInt32(___value.ItemCount)
        local currentLowestPrice = CommonDefs.Convert_ToUInt32(___value.CurrentLowestPrice)
        local item = CAuctionItemInfo(auctionID, templateID, instanceID, price, fixPrice, startPrice, selfPrice, endTime, "", type, count, 0, playId, currentLowestPrice)
        table.insert(list, item)
        if startPrice == ___value.Price then
            self.m_TakeOffPriceItemAuctionID = auctionID
        end
    end))
    CAuctionMgr.Inst.TotalCount = validTotalCount
    CAuctionMgr.Inst.CurrentItemInfoList = Table2List(list, MakeGenericClass(List, CAuctionItemInfo))
    g_ScriptEvent:BroadcastInLua("OnSendGamePlayPaiMaiOnShelfItem", targetType, playId, validTotalCount, infoRpcUd, pageNum, itemTemplateId, onShelfCurCount, onShelfTotalCount)
end

-- Gac2Gas.RequestPaimaiBetSystemItem ->
-- 请求购买的结果, bOk = true 表示成功
function LuaAuctionMgr:PlayerRequestBetSystemResult(bOk, itemTemplateId, price, targetType, betCount)
	print("PlayerRequestBetSystemResult", bOk, itemTemplateId, price, targetType, betCount)
    g_ScriptEvent:BroadcastInLua("OnPlayerRequestBetSystemResult", bOk, itemTemplateId, price, targetType, betCount)
end

-- 请求购买的结果（GamePlay版）
-- Gac2Gas.RequestGamePlayPaiMaiBet
-- Gas2Gac.RequestGamePlayPaiMaiBetResult
function LuaAuctionMgr:RequestGamePlayPaiMaiBetResult(bOk, itemTemplateId, price, targetType, betCount)
    g_ScriptEvent:BroadcastInLua("OnRequestGamePlayPaiMaiBetResult", bOk, itemTemplateId, price, targetType, betCount)
end

function LuaAuctionMgr:SyncSystemPaimaiDelay(playId)
	if CClientMainPlayer.Inst and playId == CClientMainPlayer.Inst.Id then
        g_ScriptEvent:BroadcastInLua("OnSyncSystemPaimaiDelay")
    end
end