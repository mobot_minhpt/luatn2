local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CSortButton = import "L10.UI.CSortButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local QnRadioBox=import "L10.UI.QnRadioBox"
local QnTableView=import "L10.UI.QnTableView"
local Profession = import "L10.Game.Profession"
local Double = import "System.Double"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType1 = import "CPlayerInfoMgr+AlignType"

--根据交互稿仿照LuaBangHuiJingYingSettingWnd实现

LuaGuildLeagueAppointHeroDetailWnd = class()

RegistClassMember(LuaGuildLeagueAppointHeroDetailWnd,"sortRadioBox")
RegistClassMember(LuaGuildLeagueAppointHeroDetailWnd,"tableView")
RegistClassMember(LuaGuildLeagueAppointHeroDetailWnd,"refreshBtn")
RegistClassMember(LuaGuildLeagueAppointHeroDetailWnd,"cancleAllBtn")
RegistClassMember(LuaGuildLeagueAppointHeroDetailWnd,"jingyingNumLabel")
RegistClassMember(LuaGuildLeagueAppointHeroDetailWnd,"mGuildMemberInfoList")
RegistClassMember(LuaGuildLeagueAppointHeroDetailWnd,"m_SortFlags")
RegistClassMember(LuaGuildLeagueAppointHeroDetailWnd,"m_HasRight")
RegistClassMember(LuaGuildLeagueAppointHeroDetailWnd,"leagueButton")
RegistClassMember(LuaGuildLeagueAppointHeroDetailWnd,"tipButton")

function LuaGuildLeagueAppointHeroDetailWnd:Awake()
    self.m_SortFlags={-1,-1,-1,-1,-1,-1,-1,-1}
    self.m_HasRight = false
    
    self.sortRadioBox = self.transform:Find("Anchor/TableView/Header"):GetComponent(typeof(QnRadioBox))
    self.tableView = self.transform:Find("Anchor/TableView"):GetComponent(typeof(QnTableView))
    self.refreshBtn = self.transform:Find("Anchor/TableView/Buttons/RefreshBtn").gameObject
    self.cancleAllBtn = self.transform:Find("Anchor/TableView/Buttons/CancelAllBtn").gameObject
    self.leagueButton = self.transform:Find("Anchor/TableView/Buttons/LeagueButton").gameObject
    self.tipButton = self.transform:Find("Anchor/TableView/Buttons/TipButton").gameObject
    self.jingyingNumLabel = self.transform:Find("Anchor/TableView/Buttons/JingYingLabel"):GetComponent(typeof(UILabel))
    self.mGuildMemberInfoList = {}

    UIEventListener.Get(self.refreshBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickRefreshButton(go)
    end)
    UIEventListener.Get(self.cancleAllBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickCancleAllButton(go)
    end)

    self.sortRadioBox.OnSelect =DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnRadioBoxSelect(btn,index)
    end)

    UIEventListener.Get(self.leagueButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.GuildLeagueWeeklyInfoWnd)
    end)

    UIEventListener.Get(self.tipButton).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("Guild_League_Hero_Appointment_Rule")
    end)

end

function LuaGuildLeagueAppointHeroDetailWnd:OnRadioBoxSelect( btn, index) 
    local sortButton = TypeAs(btn, typeof(CSortButton))

    self.m_SortFlags[index+1] = - self.m_SortFlags[index+1]
    for i,v in ipairs(self.m_SortFlags) do
        if i~=index+1 then
            self.m_SortFlags[i] = -1
        end
    end

    sortButton:SetSortTipStatus(self.m_SortFlags[index+1] < 0)
    self:Sort(self.mGuildMemberInfoList, index)
    self.tableView:ReloadData(true, false)
end

function LuaGuildLeagueAppointHeroDetailWnd:OnClickCancleAllButton( go) 
    local msg = g_MessageMgr:FormatMessage("Guild_League_Hero_Appointment_Cancel_All_Confirm", nil)
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
        --服务器没有提供批量接口，改为逐个取消
        for k,v in pairs(self.mGuildMemberInfoList) do
            if v.bHero then
                LuaGuildLeagueMgr:RequestAppointGuildLeagueHero(v.id, false)
            end
        end
    end), nil, nil, nil, false)
end

function LuaGuildLeagueAppointHeroDetailWnd:OnClickRefreshButton( go) 
    --刷新这里就相当于重新打开一下界面
    LuaGuildLeagueMgr:RequestOpenGuildLeagueAppointHeroDetailWnd()
end

function LuaGuildLeagueAppointHeroDetailWnd:Init( )

    local function initItem(item,row)
        item:SetBackgroundTexture(row % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
        self:InitItem(item.transform,self.mGuildMemberInfoList[row+1],row)
    end

    self.tableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.mGuildMemberInfoList
        end,
        initItem)
    
    self.tableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        local playerId = self.mGuildMemberInfoList[row+1].id
        local pos = Vector3(540, 0)
        pos = self.transform:TransformPoint(pos)
        CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, pos, AlignType1.Default)
    end)

    self.mGuildMemberInfoList = LuaGuildLeagueMgr.m_AppointHeroMemberInfoTbl
    --默认排序
    self.sortRadioBox:ChangeTo(-1, true)
    self:Sort(self.mGuildMemberInfoList, -1)

    self.tableView:ReloadData(true, false)

    self:UpdateHeroNum()
end

function LuaGuildLeagueAppointHeroDetailWnd:OnEnable()
    g_ScriptEvent:AddListener("AppointGuildLeagueHeroSuccess", self, "OnAppointGuildLeagueHeroSuccess")
end
function LuaGuildLeagueAppointHeroDetailWnd:OnDisable()
    g_ScriptEvent:RemoveListener("AppointGuildLeagueHeroSuccess", self, "OnAppointGuildLeagueHeroSuccess")
end

function LuaGuildLeagueAppointHeroDetailWnd:OnAppointGuildLeagueHeroSuccess(destPlayerId, bHero) 
    local parent=self.tableView.m_Grid.transform         
    for i,v in ipairs(self.mGuildMemberInfoList) do
        if v.id==destPlayerId then
            v.bHero = bHero
            self:SetItemHero(parent:GetChild(i-1),i-1,bHero)
            break
        end
    end
    self:UpdateHeroNum()
end

function LuaGuildLeagueAppointHeroDetailWnd:UpdateHeroNum( )
    if self.mGuildMemberInfoList ~= nil then
        local num = 0
        for i,v in ipairs(self.mGuildMemberInfoList) do
            if v.bHero then
                num=num+1
            end
        end
        self.jingyingNumLabel.text = SafeStringFormat3( LocalString.GetString("%d/%d"), num, GuildLeague_Setting.GetData().MaxHeroCount )
    end
end

function LuaGuildLeagueAppointHeroDetailWnd:Sort(t,sortIndex)
    local function defaultSort(a,b)
        if a.duration > b.duration then
            return true
        elseif a.duration < b.duration then
            return false
        else
            return a.id<b.id
        end
    end
    local flag=self.m_SortFlags[sortIndex+1]
    if sortIndex==0 then--cls name
        table.sort( t, function(a,b)
            local ret=flag
            if a.class<b.class then
                ret = -flag
            elseif a.class>b.class then
                ret=flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==1 then--level
        table.sort( t, function(a,b)
            local ret=flag
            if a.level<b.level then
                ret = flag
            elseif a.level>b.level then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )

    elseif sortIndex==2 then--xiuwei
        table.sort( t, function(a,b)

            local ret=flag
            if a.xiuweiGrade<b.xiuweiGrade then
                ret = flag
            elseif a.xiuweiGrade>b.xiuweiGrade then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==3 then--xiulian
        table.sort( t, function(a,b)
            local ret=flag
            if a.xiulianGrade<b.xiulianGrade then
                ret = flag
            elseif a.xiulianGrade>b.xiulianGrade then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==4 then--equip score
        table.sort( t, function(a,b)
            local ret=flag
            if a.equipScore<b.equipScore then
                ret = flag
            elseif a.equipScore>b.equipScore then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==5 then--liansai count
        table.sort( t, function(a,b)
            local ret=flag
            if a.times<b.times then
                ret = flag
            elseif a.times>b.times then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==6 then--liansai time default
        table.sort( t, function(a,b)
            local ret=flag
            if a.duration < b.duration then
                ret = flag
            elseif a.duration > b.duration then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==7 then--hero
        --hero靠前显示
        table.sort( t, function(a,b)
            local ret=flag
            if a.bHero and not b.bHero then
                ret = flag
            elseif (not a.bHero) and b.bHero then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    else
        table.sort( t, function(a,b)
                return defaultSort(a,b)
        end )
    end
end

function LuaGuildLeagueAppointHeroDetailWnd:InitItem(transform,info,row)
    local clsSprite = transform:Find("ClassSprite"):GetComponent(typeof(UISprite))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local levelLabel = transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local xiuweiLabel = transform:Find("XiuweiLabel"):GetComponent(typeof(UILabel))
    local xiulianLabel = transform:Find("XiulianLabel"):GetComponent(typeof(UILabel))
    local equipScoreLabel = transform:Find("EquipScoreLabel"):GetComponent(typeof(UILabel))
    local lianSaiCountLabel = transform:Find("LianSaiCountLabel"):GetComponent(typeof(UILabel))
    local lianSaiTimeLabel = transform:Find("LianSaiTimeLabel"):GetComponent(typeof(UILabel))
    local tickSprite = transform:Find("TickSprite"):GetComponent(typeof(UISprite))

    local playerId=info.id
    local playerClass = info.class
    local myId = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id or 0

    if playerId == myId then
        nameLabel.color = Color.green
        levelLabel.color = Color.green
        xiuweiLabel.color = Color.green
        xiulianLabel.color = Color.green
        equipScoreLabel.color = Color.green
        lianSaiCountLabel.color = Color.green
        lianSaiTimeLabel.color = Color.green
    else
        nameLabel.color = Color.white
        levelLabel.color = Color.white
        xiuweiLabel.color = Color.white
        xiulianLabel.color = Color.white
        equipScoreLabel.color = Color.white
        lianSaiCountLabel.color = Color.white
        lianSaiTimeLabel.color = Color.white
    end


    nameLabel.text = info.name
    clsSprite.spriteName = Profession.GetIconByNumber(info.class)

    CUICommonDef.SetActive(clsSprite.gameObject, info.online, true)

    local default
    if info.isInXianShenStatus then
        default=SafeStringFormat3( "[c][ff7900]Lv.%d[-][/c]",info.level )
    else
        default=SafeStringFormat3( "Lv.%d",info.level )
    end
    levelLabel.text = default

    xiuweiLabel.text = NumberComplexToString(NumberTruncate(info.xiuweiGrade, 1), typeof(Double), "F1")
    --一位小数显示
    xiulianLabel.text = tostring(info.xiulianGrade)

    equipScoreLabel.text = tostring(info.equipScore)
    --联赛次数
    lianSaiCountLabel.text = info.times
    --联赛时间
    local minute = (math.floor(info.duration / 60))
    local hour = math.floor(minute / 60)
    if hour > 0 then
        lianSaiTimeLabel.text =  SafeStringFormat3(LocalString.GetString("%d小时"), hour)
    else
        lianSaiTimeLabel.text =  SafeStringFormat3(LocalString.GetString("%d分钟"), minute)
    end

    self:SetItemHero(transform,row,info.bHero)

    UIEventListener.Get(tickSprite.transform:GetChild(0).gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if tickSprite.enabled then
            --撤销
            LuaGuildLeagueMgr:RequestAppointGuildLeagueHero(playerId, false)
        elseif self:CheckAppointValid(playerId, playerClass) then
            LuaGuildLeagueMgr:RequestAppointGuildLeagueHero(playerId, true)
        end
    end)
end

function LuaGuildLeagueAppointHeroDetailWnd:SetItemHero(transform,row,isHero)
    local tickSprite = transform:Find("TickSprite"):GetComponent(typeof(UISprite))
    tickSprite.enabled = isHero
end

--检查任命英雄是否合法，服务器不提供消息提示，所以纯客户端做一下判断提示，主要检查两点：1. 同一职业只能任命一个 2. 任命数最多不超过上限
function LuaGuildLeagueAppointHeroDetailWnd:CheckAppointValid(playerId, playerClass)
    local heroCount = 0
    for k,v in pairs(self.mGuildMemberInfoList) do
        if v.bHero then
            heroCount = heroCount+1
            if v.class==playerClass then
                g_MessageMgr:ShowMessage("Guild_League_Hero_Appointment_Already_Appoint_Same_Class_Player")
                return false
            end
        end
    end
    if heroCount >= GuildLeague_Setting.GetData().MaxHeroCount then
        g_MessageMgr:ShowMessage("Guild_League_Hero_Appointment_Reach_Max_Hero_Limit")
        return false
    end
    return true
end
