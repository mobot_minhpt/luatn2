local UITable = import "UITable"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemCountUpdate = import "L10.UI.CItemCountUpdate"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CEquipBaptizeDescTable = import "L10.UI.CEquipBaptizeDescTable"
local UIToggle = import "UIToggle"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local AlignType2 = import "L10.UI.CTooltip+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local GameObject = import "UnityEngine.GameObject"
local CBaseEquipmentItem = import "L10.UI.CBaseEquipmentItem"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CEquipmentBaptizeMgr = import "L10.Game.CEquipmentBaptizeMgr"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"

LuaEquipCompositeWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaEquipCompositeWnd, "ChooseMaterialBtn", "ChooseMaterialBtn", GameObject)
RegistChildComponent(LuaEquipCompositeWnd, "MaterialEquip", "MaterialEquip", CBaseEquipmentItem)
RegistChildComponent(LuaEquipCompositeWnd, "MainEquip", "MainEquip", CBaseEquipmentItem)
RegistChildComponent(LuaEquipCompositeWnd, "GetNode", "GetNode", GameObject)
RegistChildComponent(LuaEquipCompositeWnd, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaEquipCompositeWnd, "CostNumLabel", "CostNumLabel", UILabel)
RegistChildComponent(LuaEquipCompositeWnd, "ItemNameLabel", "ItemNameLabel", UILabel)
RegistChildComponent(LuaEquipCompositeWnd, "LingYuCost", "LingYuCost", CCurentMoneyCtrl)
RegistChildComponent(LuaEquipCompositeWnd, "YinLiangCost", "YinLiangCost", CCurentMoneyCtrl)
RegistChildComponent(LuaEquipCompositeWnd, "LockDescLabel", "LockDescLabel", UILabel)
RegistChildComponent(LuaEquipCompositeWnd, "PostScoreLabel", "PostScoreLabel", UILabel)
RegistChildComponent(LuaEquipCompositeWnd, "ItemCostNode", "ItemCostNode", GameObject)
RegistChildComponent(LuaEquipCompositeWnd, "AutoCostJadeCheckBox", "AutoCostJadeCheckBox", QnCheckBox)
RegistChildComponent(LuaEquipCompositeWnd, "WordTable2", "WordTable2", UITable)
RegistChildComponent(LuaEquipCompositeWnd, "RongLianBtn", "RongLianBtn", GameObject)
RegistChildComponent(LuaEquipCompositeWnd, "DescBtn", "DescBtn", GameObject)
RegistChildComponent(LuaEquipCompositeWnd, "DescItem", "DescItem", GameObject)
RegistChildComponent(LuaEquipCompositeWnd, "PreScoreLabel", "PreScoreLabel", UILabel)
RegistChildComponent(LuaEquipCompositeWnd, "WordTable1", "WordTable1", UITable)

--@endregion RegistChildComponent end

function LuaEquipCompositeWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ChooseMaterialBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChooseMaterialBtnClick()
	end)


	
	UIEventListener.Get(self.RongLianBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRongLianBtnClick()
	end)


	
	UIEventListener.Get(self.DescBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDescBtnClick()
	end)


    --@endregion EventBind end

    self.LingYuCost:SetType(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
end

function LuaEquipCompositeWnd:Init()

    -- 首次进入弹出提示
    if PlayerPrefs.GetInt("equip_composite_wnd_first_open", 0) ~= 20220714 then
        LuaCommonTextImageRuleMgr:ShowOnlyImageWnd(4)
        PlayerPrefs.SetInt("equip_composite_wnd_first_open",20220714)
    end

    -- 是否可以锁词条 只有物品品质和tc相同 才可以锁
    self.m_CanLock = false
    -- 当前已锁词条 list of {itemId, id, isFixed}
    self.m_LockWordList = {}
    
    self.DescItem:SetActive(false)

    -- 设置灵玉消耗状态
    self.AutoCostJadeCheckBox.OnValueChanged = DelegateFactory.Action_bool(function(val)
        self:SetAutoCost(val)
    end)
    self.AutoCostJadeCheckBox:SetSelected(false, true)

    -- 设置词条显示
    self:InitShow()
end

-- 设置显示
function LuaEquipCompositeWnd:InitShow()
    -- 设置词条显示
    self:InitWordsData()
    self:InitItem()
end

-- 切换材料装备需要刷新界面
function LuaEquipCompositeWnd:OnSelectMaterial(itemId)
    self.m_LockWordList = {}
    if itemId and itemId ~= "" then
		CLuaEquipMgr.m_MaterialEquipItemId = itemId
	end
    self:InitShow()
end

-- 显示词条
function LuaEquipCompositeWnd:InitWordsData()
    local testLabel = self.DescItem.transform:Find("DescLabel"):GetComponent(typeof(UILabel))

    local matId = CLuaEquipMgr.m_MaterialEquipItemId
    local mainId = CLuaEquipMgr.m_MainEquipItemId

	local mainItem = CItemMgr.Inst:GetById(mainId)
	local matItem = CItemMgr.Inst:GetById(matId)

	local mainEquip = mainItem.Equip
	local matEquip = matItem.Equip

    local mainData = EquipmentTemplate_Equip.GetData(mainItem.TemplateId)
    local matData = EquipmentTemplate_Equip.GetData(matItem.TemplateId)

    if mainEquip.Color == matEquip.Color and mainData.TC == matData.TC and mainEquip.IsBinded and matEquip.IsBinded then
        self.m_CanLock = true
    else
        self.m_CanLock = false
    end

    local canFixedLock = true
    local canExtraLock = true

    for i, data in ipairs(self.m_LockWordList) do
        if data.isFixed then
            canFixedLock = false
        else
            canExtraLock = false
        end
    end

    -- 需要展示的main和mat数据
    local mainWordList = {}
    local matWordList = {}

    local minWordCount = math.min(mainEquip.WordsCount, matEquip.WordsCount)

    local heightList = {}

    -- 遍历
    -- 主装备固定词条
    local mainIndex = 1
    local mainFixedList = mainEquip:GetFixedWordList(false)
    for i =0, mainFixedList.Count -1 do
        local id = mainFixedList[i]

        local data = {}
        data.isFixed = true
        data.id = id
        data.index = i + 1
        
        -- 状态有三种 一种是不可锁 一种是可锁 一种是已锁
        local state = CLuaEquipMgr.EnumCompositWordTipState.CantLock
        if self.m_CanLock and canFixedLock then
            state = CLuaEquipMgr.EnumCompositWordTipState.CanLock
        end

        for j, lock in ipairs(self.m_LockWordList) do
            if lock.itemId == mainId and lock.id == id and lock.isFixed and lock.index == i+1 then
                state = CLuaEquipMgr.EnumCompositWordTipState.Locked
            end
        end
        data.state = state

        -- 设置高
        local word = Word_Word.GetData(data.id)
        testLabel.text = word.Description
        testLabel:UpdateNGUIText()
        table.insert(heightList, testLabel.height)
        
        table.insert(mainWordList, data)
        mainIndex = mainIndex + 1
    end

    -- 主装备额外词条
    local mainExtraList = mainEquip:GetExtWordList(false)
    for i =0, mainExtraList.Count -1 do
        local id = mainExtraList[i]

        local data = {}
        data.itemId = mainId
        data.isFixed = false
        data.id = id
        data.index = i + 1
        -- 状态有三种 一种是不可锁 一种是可锁 一种是已锁
        local state = CLuaEquipMgr.EnumCompositWordTipState.CantLock
        if self.m_CanLock and canExtraLock and mainIndex <= minWordCount then
            state = CLuaEquipMgr.EnumCompositWordTipState.CanLock
        end

        for j, lock in ipairs(self.m_LockWordList) do
            if lock.itemId == mainId and lock.id == id and not lock.isFixed and lock.index == i+1 then
                state = CLuaEquipMgr.EnumCompositWordTipState.Locked
            end
        end
        data.state = state

        local word = Word_Word.GetData(data.id)
        testLabel.text = word.Description
        testLabel:UpdateNGUIText()
        mainIndex = mainIndex + 1

        table.insert(heightList, testLabel.height)

        table.insert(mainWordList, data)
    end


    local matIndex = 1
    -- 材料固定词条
    local matFixedList = matEquip:GetFixedWordList(false)
    for i =0, matFixedList.Count -1 do
        local id = matFixedList[i]

        local data = {}
        data.itemId = matId
        data.isFixed = true
        data.id = id
        data.index = i + 1
        -- 状态有三种 一种是不可锁 一种是可锁 一种是已锁
        local state = CLuaEquipMgr.EnumCompositWordTipState.CantLock
        if self.m_CanLock and canFixedLock then
            state = CLuaEquipMgr.EnumCompositWordTipState.CanLock
        end

        for j, lock in ipairs(self.m_LockWordList) do
            if lock.itemId == matId and lock.id == id and lock.isFixed and lock.index == i+1  then
                state = CLuaEquipMgr.EnumCompositWordTipState.Locked
            end
        end
        data.state = state

        local word = Word_Word.GetData(data.id)
        testLabel.text = word.Description
        testLabel:UpdateNGUIText()

        local height = testLabel.height
        if matIndex <= #heightList and height > heightList[matIndex] then
            heightList[matIndex] = height
        end
        matIndex = matIndex + 1

        table.insert(matWordList, data)
    end

    -- 材料装备额外词条
    local matExtraList = matEquip:GetExtWordList(false)
    for i =0, matExtraList.Count -1 do
        local id = matExtraList[i]

        local data = {}
        data.itemId = matId
        data.isFixed = false
        data.id = id
        data.index = i + 1
        -- 状态有三种 一种是不可锁 一种是可锁 一种是已锁
        local state = CLuaEquipMgr.EnumCompositWordTipState.CantLock
        if self.m_CanLock and canExtraLock and matIndex <= minWordCount then
            state = CLuaEquipMgr.EnumCompositWordTipState.CanLock
        end

        for j, lock in ipairs(self.m_LockWordList) do
            if lock.itemId == matId and lock.id == id and not lock.isFixed and lock.index == i+1 then
                state = CLuaEquipMgr.EnumCompositWordTipState.Locked
            end
        end
        data.state = state

        local word = Word_Word.GetData(data.id)
        testLabel.text = word.Description
        testLabel:UpdateNGUIText()

        local height = testLabel.height
        if matIndex <= #heightList and height > heightList[matIndex] then
            heightList[matIndex] = height
        else
            table.insert(heightList, height)
        end
        matIndex = matIndex + 1

        table.insert(matWordList, data)
    end

    -- 处理显示和回调
    CLuaEquipMgr:InitWord(mainId, self.WordTable1, self.DescItem, mainWordList, heightList,function(data)
        self:OnLockChange(data)
    end)

    CLuaEquipMgr:InitWord(matId, self.WordTable2, self.DescItem, matWordList, heightList, function(data)
        self:OnLockChange(data)
    end)

    self.WordTable1.transform.parent:GetComponent(typeof(CUIRestrictScrollView)):ResetPosition()
    self.WordTable2.transform.parent:GetComponent(typeof(CUIRestrictScrollView)):ResetPosition()

    -- 显示词条锁定状态
    if self.m_CanLock then
        self.LockDescLabel.text = SafeStringFormat3(LocalString.GetString("词条已锁定 %s/2"), tostring(#self.m_LockWordList))
    else
        self.LockDescLabel.text = g_MessageMgr:FormatMessage("Equip_Composite_Cant_Lock_Word_Tip")
    end

    self.PreScoreLabel.text = tostring(CEquipmentBaptizeMgr.FormatScore(mainEquip.Score))
    self.PostScoreLabel.text = tostring(CEquipmentBaptizeMgr.FormatScore(matEquip.Score))
end

-- 锁定信息变动
function LuaEquipCompositeWnd:OnLockChange(data)
    if data.state == CLuaEquipMgr.EnumCompositWordTipState.Locked then
        local newList = {}
        for i, lock in ipairs(self.m_LockWordList) do
            if lock.id ~= data.id or lock.itemId ~= data.itemId or lock.isFixed ~= data.isFixed then
                table.insert(newList, lock)
            end
        end

        self.m_LockWordList = newList
    elseif data.state == CLuaEquipMgr.EnumCompositWordTipState.CanLock then
        table.insert(self.m_LockWordList, data)
    end
    -- 锁定信息变了 全部显示都要刷新
    self:InitShow()
end

function LuaEquipCompositeWnd:SetAutoCost(isAutoCost)
    self.ItemCostNode:SetActive(not isAutoCost)
    self.LingYuCost.gameObject:SetActive(isAutoCost)
end

-- 显示物品与消耗信息
function LuaEquipCompositeWnd:InitItem()
	local matId = CLuaEquipMgr.m_MaterialEquipItemId
    local mainId = CLuaEquipMgr.m_MainEquipItemId

    self.MainEquip:UpdateData(mainId)
    self.MaterialEquip:UpdateData(matId)

	local mainItem = CItemMgr.Inst:GetById(mainId)
	local matItem = CItemMgr.Inst:GetById(matId)

    if mainItem == nil or matItem == nil then
        return 
    end
	
    -- 获取消耗数据
    local costData, yinLiangCost = CLuaEquipMgr:GetCompositeCostData(mainItem, matItem, #self.m_LockWordList)
    
    local itemId = costData[0]
    local needCount = costData[1]

    local bindCount, notBindCount
    bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)
    local count = bindCount + notBindCount

    -- 数量显示
    local isEnough = needCount <= count
    self.GetNode:SetActive(not isEnough)
    if not isEnough then
        self.CostNumLabel.text = SafeStringFormat3("[FF0000]%s[-]/%s", tostring(count),tostring(needCount))
    else
        self.CostNumLabel.text = SafeStringFormat3("[00FF00]%s[-]/%s", tostring(count),tostring(needCount))
    end

    -- 物品显示
    local itemData = Item_Item.GetData(itemId)
    self.Icon:LoadMaterial(itemData.Icon)
    self.ItemNameLabel.text = itemData.Name

    -- 点击事件
    UIEventListener.Get(self.Icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    if not isEnough then
            -- 获取方式
            CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, false, nil, AlignType2.Right)
        else
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end
	end)

    -- 显示银两与灵玉消耗
    local mallData = Mall_LingYuMall.GetData(itemId)
    local jadeCost = mallData and mallData.Jade or 0
    local lackCount =  needCount - count
    if lackCount < 0 then
        lackCount = 0
    end
    jadeCost = jadeCost * lackCount
    self.YinLiangCost:SetCost(yinLiangCost)
    self.LingYuCost:SetCost(jadeCost)

end

function LuaEquipCompositeWnd:OnItemUpdate()
    self:UpdateItem()
	self:InitShow()
end

function LuaEquipCompositeWnd:UpdateItem()
    -- 新装备融炼相关
    if CLuaEquipMgr.m_MaterialEquipItemId == nil or CLuaEquipMgr.m_MaterialEquipItemId == "" or
        CItemMgr.Inst:GetById(CLuaEquipMgr.m_MaterialEquipItemId) == nil then
        CLuaEquipMgr.m_MaterialEquipItemId = ""
    end
    if CLuaEquipMgr.m_MainEquipItemId == nil or CLuaEquipMgr.m_MainEquipItemId == "" or
        CItemMgr.Inst:GetById(CLuaEquipMgr.m_MainEquipItemId) == nil then
        CLuaEquipMgr.m_MainEquipItemId = ""
    end
    if CLuaEquipMgr.m_ResultEquipItemId == nil or CLuaEquipMgr.m_ResultEquipItemId == "" or
        CItemMgr.Inst:GetById(CLuaEquipMgr.m_ResultEquipItemId) == nil then
        CLuaEquipMgr.m_ResultEquipItemId = ""
    end
end

function LuaEquipCompositeWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "OnItemUpdate")
	g_ScriptEvent:AddListener("SetItemAt", self, "OnItemUpdate")
	g_ScriptEvent:AddListener("Equip_Composite_Select_Material_Equip", self, "OnSelectMaterial")
    g_ScriptEvent:AddListener("SendItem", self, "InitItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "InitItem")
end

function LuaEquipCompositeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "OnItemUpdate")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "OnItemUpdate")
	g_ScriptEvent:RemoveListener("Equip_Composite_Select_Material_Equip", self, "OnSelectMaterial")
    g_ScriptEvent:RemoveListener("SendItem", self, "InitItem")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "InitItem")
end

--@region UIEvent

function LuaEquipCompositeWnd:OnChooseMaterialBtnClick()
    CLuaEquipMgr:SelectMaterialEquip()
end

function LuaEquipCompositeWnd:OnRongLianBtnClick()
    local isAutoJade = self.AutoCostJadeCheckBox.Selected

	local mainItem = CItemMgr.Inst:GetById(CLuaEquipMgr.m_MainEquipItemId)
	local matItem = CItemMgr.Inst:GetById(CLuaEquipMgr.m_MaterialEquipItemId)
	
    -- 获取消耗数据
    local costData, yinLiangCost = CLuaEquipMgr:GetCompositeCostData(mainItem, matItem, #self.m_LockWordList)
    local itemId = costData[0]
    local needCount = costData[1]
    local bindCount, notBindCount
    bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)
    local count = bindCount + notBindCount
    -- 数量显示
    local isEnough = needCount <= count


    if isAutoJade and not self.LingYuCost.moneyEnough then
        -- 灵玉不足
        self.LingYuCost:AddMoney(nil)
    elseif not self.YinLiangCost.moneyEnough then
        -- 银两不足
        self.YinLiangCost:AddMoney(nil)
    elseif not isEnough and not isAutoJade then
        -- 物品不足弹消息
        g_MessageMgr:ShowMessage("Equip_Composite_Item_Not_Enough")
    else
        local mainPlace, mainPos = CLuaEquipMgr:GetItemPlaceAndPos(CLuaEquipMgr.m_MainEquipItemId)
        local matPlace, matPos = CLuaEquipMgr:GetItemPlaceAndPos(CLuaEquipMgr.m_MaterialEquipItemId)

        -- 可以合成
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Equip_Composite_Confirm_Tip",needCount, mainItem.ColoredName, matItem.ColoredName), function()
            local index1, id1, isMat1 =0,0,0
            local index2, id2, isMat2 =0,0,0


            for i, data in ipairs(self.m_LockWordList) do
                if data.isFixed then
                    index1 = data.index
                    id1 = data.id
                    isMat1 = data.itemId == CLuaEquipMgr.m_MaterialEquipItemId and 1 or 0
                else
                    index2 = data.index
                    id2 = data.id
                    isMat2 = data.itemId == CLuaEquipMgr.m_MaterialEquipItemId and 1 or 0
                end
            end

            -- 发送合成RPC
            Gac2Gas.RequestCompositeExtraEquipWord(mainPlace, mainPos, CLuaEquipMgr.m_MainEquipItemId, matPlace, matPos, CLuaEquipMgr.m_MaterialEquipItemId,
            index1, isMat1, id1, index2, isMat2, id2, isAutoJade)
            -- 关闭本界面
            CUIManager.CloseUI(CLuaUIResources.EquipCompositeWnd)

            g_ScriptEvent:BroadcastInLua("Equip_Composite_Select_Material_Equip", "")

        end, nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    end
end

function LuaEquipCompositeWnd:OnDescBtnClick()
    LuaCommonTextImageRuleMgr:ShowOnlyImageWnd(4)
end


--@endregion UIEvent

