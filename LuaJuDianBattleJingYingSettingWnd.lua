local UILabel = import "UILabel"
local QnTableView = import "L10.UI.QnTableView"
local GameObject = import "UnityEngine.GameObject"
local QnRadioBox = import "L10.UI.QnRadioBox"
local CSortButton = import "L10.UI.CSortButton"
local DelegateFactory  = import "DelegateFactory"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"
local Profession = import "L10.Game.Profession"
local QnTableItem=import "L10.UI.QnTableItem"

LuaJuDianBattleJingYingSettingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJuDianBattleJingYingSettingWnd, "HeaderRadioBox", "HeaderRadioBox", QnRadioBox)
RegistChildComponent(LuaJuDianBattleJingYingSettingWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaJuDianBattleJingYingSettingWnd, "CancelAllBtn", "CancelAllBtn", GameObject)
RegistChildComponent(LuaJuDianBattleJingYingSettingWnd, "ContributionButton", "ContributionButton", GameObject)
RegistChildComponent(LuaJuDianBattleJingYingSettingWnd, "RefreshBtn", "RefreshBtn", GameObject)
RegistChildComponent(LuaJuDianBattleJingYingSettingWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaJuDianBattleJingYingSettingWnd, "JingYingNumLabel", "JingYingNumLabel", UILabel)
RegistChildComponent(LuaJuDianBattleJingYingSettingWnd, "OnlineNumLabel", "OnlineNumLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaJuDianBattleJingYingSettingWnd,"m_SortFlags")
RegistClassMember(LuaJuDianBattleJingYingSettingWnd,"m_SortIndex")
RegistClassMember(LuaJuDianBattleJingYingSettingWnd,"m_List")
RegistClassMember(LuaJuDianBattleJingYingSettingWnd,"m_MorePanel")

function LuaJuDianBattleJingYingSettingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	self.HeaderRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
	    self:OnHeaderRadioBoxSelected(btn, index)
	end)

	UIEventListener.Get(self.CancelAllBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelAllBtnClick()
	end)

	UIEventListener.Get(self.ContributionButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnContributionButtonClick()
	end)

	UIEventListener.Get(self.RefreshBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRefreshBtnClick()
	end)

	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

    --@endregion EventBind end

	self.m_MorePanel = FindChild(self.transform, "MorePanel").gameObject
    self.m_MorePanel:SetActive(false)
    local configButton = self.m_MorePanel.transform:Find("OptionBtn").gameObject
    UIEventListener.Get(configButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickConfigButton(go)
    end)
    local moreButton = FindChild(self.transform,"MoreBtn").gameObject
    UIEventListener.Get(moreButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self.m_MorePanel:SetActive(not self.m_MorePanel.activeSelf)
    end)
end

function LuaJuDianBattleJingYingSettingWnd:Init()
	self.m_SortFlags = {-1,-1,-1,-1,-1,-1,-1}
	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_List
        end,
        function(item,row)
            self:InitItem(item, row)
        end)
	self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
		self:OnSelectAtRow(row)
	end)
	self.m_List = {}
	self:UpdateMemberNum()
end

function LuaJuDianBattleJingYingSettingWnd:UpdateMemberInfo()
	local t = LuaJuDianBattleMgr.m_ChallengeMemberInfo
	local onlineMemberCount = t.onlineMemberCount and t.onlineMemberCount or 0
	local totalMemberCount = t.totalMemberCount and t.totalMemberCount or 0
    self.OnlineNumLabel.text = SafeStringFormat3(LocalString.GetString("帮会在线：[00ff00]%d[-]/%d"), onlineMemberCount, totalMemberCount)
	local onlineJingYingNum, curJingYingNum, limitJingYingNum = t.onlineEliteCount and t.onlineEliteCount or 0, t.totalEliteCount and t.totalEliteCount or 0, GuildTerritoryWar_RelatedPlaySetting.GetData().MaxNumWarElite
	self.JingYingNumLabel.text = SafeStringFormat3(LocalString.GetString("棋局精英：[00ff00]%d[-]/%d/%d"), onlineJingYingNum, curJingYingNum, limitJingYingNum)
end

function LuaJuDianBattleJingYingSettingWnd:UpdateMemberNum()
	self:UpdateMemberInfo()
	local t = LuaJuDianBattleMgr.m_ChallengeMemberInfo
	self.m_List = {}
	local list = t.list and t.list or {}
	for i = 1, #list do
		if not LuaJuDianBattleMgr.m_FilterOnlineAndJingYing or list[i].isOnline or list[i].isElite then
			table.insert(self.m_List, list[i])
		end
	end
	self:Sort(self.m_SortIndex)
	self.TableView:ReloadData(true, false)
end

function LuaJuDianBattleJingYingSettingWnd:InitItem(item, row)
	local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local waiYuanTex = item.transform:Find("NameLabel/WaiYuanTex")
	local xiuweiLabel = item.transform:Find("XiuweiLabel"):GetComponent(typeof(UILabel))
	local levelLabel = item.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
	local xiulianLabel = item.transform:Find("XiulianLabel"):GetComponent(typeof(UILabel))
	local equipScoreLabel = item.transform:Find("EquipScoreLabel"):GetComponent(typeof(UILabel))
	local classSprite = item.transform:Find("ClassSprite"):GetComponent(typeof(UISprite)) --TODO
	local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
	local scoreLabel = item.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
	local tickSprite = item.transform:Find("TickSprite"):GetComponent(typeof(UISprite))
	local isEnter = item.transform:Find("IsEnter"):GetComponent(typeof(UILabel))

    local bg = item.transform:GetComponent(typeof(QnTableItem))
    if row % 2 == 0 then
        bg.m_NormalSpirte = "common_bg_mission_background_s"
    else
        bg.m_NormalSpirte = "common_bg_mission_background_n"
    end

	local data = self.m_List[row + 1]

	local isElite = data.isElite
	local isOnLine = data.isOnline
	self:SetItemElite(item.transform,row,isElite)
	nameLabel.text = data.playername
	levelLabel.text = SafeStringFormat3("Lv.%d",data.level) 
	levelLabel.color = (CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == data.playerId) and Color.green or ((data.feisheng > 0) and NGUIText.ParseColor24("fe7900", 0) or Color.white)
	xiuweiLabel.text = data.xiuwei
	xiulianLabel.text = data.xiulian
	equipScoreLabel.text = data.equipScore
	local timeStr = ""
	if math.floor(data.playtime / 3600) > 0 then
		timeStr = timeStr .. (math.floor(data.playtime / 3600) .. LocalString.GetString("小时"))
	end
	if math.floor((data.playtime % 3600) / 60) > 0 then
		timeStr = timeStr .. (math.floor((data.playtime % 3600) / 60) .. LocalString.GetString("分钟"))
	end
	timeLabel.text = timeStr
	scoreLabel.text = data.score
	waiYuanTex.gameObject:SetActive(data.isForeignAid)
	classSprite.spriteName = Profession.GetIconByNumber(data.class)
	UIEventListener.Get(tickSprite.transform:GetChild(0).gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
		Gac2Gas.RequestSetGuildJuDianChallengeElite(data.playerId, not tickSprite.enabled)
    end)
	Extensions.SetLocalPositionZ(classSprite.transform, isOnLine and 0 or -1)
	local labelColor = Color.white
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == data.playerId then
		labelColor = Color.green
	end
	nameLabel.color = labelColor
	xiuweiLabel.color = labelColor
	xiulianLabel.color = labelColor
	equipScoreLabel.color = labelColor
	timeLabel.color = labelColor
	scoreLabel.color = labelColor

	isEnter.text = LocalString.GetString(not LuaJuDianBattleMgr.m_IsChallenging and "—" or (data.isInPlay and "是" or "否"))
	isEnter.color = LuaJuDianBattleMgr.m_IsChallenging and not data.isInPlay and isElite and NGUIText.ParseColor24("ff5050", 0) or Color.white
end

function LuaJuDianBattleJingYingSettingWnd:SetItemElite(transform,row,isElite)
    local tickSprite = transform:Find("TickSprite"):GetComponent(typeof(UISprite))
    tickSprite.enabled = isElite
end

function LuaJuDianBattleJingYingSettingWnd:Sort(sortIndex)
	self.m_SortIndex = sortIndex
	local flag = self.m_SortFlags[sortIndex]
	local function defaultSort(a,b)
		if a.equipScore ~= b.equipScore then
			return a.equipScore > b.equipScore
		end
        return a.playerId < b.playerId
    end
	local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
	table.sort( self.m_List, function(a,b)
		if myId == a.playerId then
			return true
		elseif myId == b.playerId then
			return false
		end
		if a.isOnline ~= b.isOnline then
			return a.isOnline
		end
		if flag then
			if sortIndex == 1 then
				return flag * (a.level - b.level) > 0
			elseif sortIndex == 2 then
				return flag * (a.xiuwei - b.xiuwei) > 0
			elseif sortIndex == 3 then
				return flag * (a.xiulian - b.xiulian) > 0
			elseif sortIndex == 4 then
				return flag * (a.equipScore - b.equipScore) > 0
			elseif sortIndex == 5 then
				return flag * (a.playtime - b.playtime) > 0
			elseif sortIndex == 6 then
				return flag * (a.score - b.score) > 0
			elseif sortIndex == 7 and LuaJuDianBattleMgr.m_IsChallenging then
				local aIsInPlay = a.isInPlay and 1 or 0
				local bIsInPlay = b.isInPlay and 1 or 0
				local aIsElite = a.isElite and 1 or 0
				local bIsElite = b.isElite and 1 or 0
				if aIsInPlay ~= bIsInPlay then
					return aIsInPlay < bIsInPlay
				end
				return aIsElite > bIsElite
			end
		end
		return defaultSort(a,b)
	end )
end

function LuaJuDianBattleJingYingSettingWnd:OnEnable()
	g_ScriptEvent:AddListener("SendGuildJuDianChallengeMemberInfo", self, "OnSendGuildJuDianChallengeMemberInfo")
	g_ScriptEvent:AddListener("SendSetGuildJuDianChallengeEliteResult", self, "OnSendSetGuildJuDianChallengeEliteResult")
end

function LuaJuDianBattleJingYingSettingWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendGuildJuDianChallengeMemberInfo", self, "OnSendGuildJuDianChallengeMemberInfo")
	g_ScriptEvent:RemoveListener("SendSetGuildJuDianChallengeEliteResult", self, "OnSendSetGuildJuDianChallengeEliteResult")
end

function LuaJuDianBattleJingYingSettingWnd:OnSendGuildJuDianChallengeMemberInfo(onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount, hasPermission, list)
	self:UpdateMemberNum()
end

function LuaJuDianBattleJingYingSettingWnd:OnSendSetGuildJuDianChallengeEliteResult(targetPlayerId, isElite, onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount)
	self:UpdateMemberInfo()
	for i,v in ipairs(self.m_List) do
		if v.playerId == targetPlayerId then
			self:SetItemElite(self.TableView.m_Grid.transform:GetChild(i - 1),i - 1,v.isElite)
			break
		end
	end
end

--@region UIEvent

function LuaJuDianBattleJingYingSettingWnd:OnCancelAllBtnClick()
	MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("GTW_DISMISS_CONFIRM"),DelegateFactory.Action(function()
		Gac2Gas.RequestClearAllGuildJuDianChallengeElite()
	end),nil,LocalString.GetString("确认"),LocalString.GetString("取消"),false)
end

function LuaJuDianBattleJingYingSettingWnd:OnContributionButtonClick()
	CUIManager.ShowUI(CLuaUIResources.JuDianBattleContributionWnd)
end

function LuaJuDianBattleJingYingSettingWnd:OnRefreshBtnClick()
	Gac2Gas.RequestGuildJuDianChallengeMemberInfo(true)
end


function LuaJuDianBattleJingYingSettingWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("GUILD_JUDIAN_QJTZ_APPOINT_TIPS")
end


function LuaJuDianBattleJingYingSettingWnd:OnHeaderRadioBoxSelected(btn, index)
	local sortButton = TypeAs(btn, typeof(CSortButton))

	local sortIndex = index + 1
	if self.m_SortFlags == nil then
		return
	end
    self.m_SortFlags[sortIndex] = - self.m_SortFlags[sortIndex]
    for i,v in ipairs(self.m_SortFlags) do
        if i ~= sortIndex then
            self.m_SortFlags[i] = -1
        end
    end

    sortButton:SetSortTipStatus(self.m_SortFlags[sortIndex] < 0)
    self:Sort(sortIndex)
    self.TableView:ReloadData(true, false)
end

function LuaJuDianBattleJingYingSettingWnd:OnSelectAtRow(row)
	local data = self.m_List[row + 1]
	CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, "", nil, Vector3.zero, AlignType.Default)
end
--@endregion UIEvent

function LuaJuDianBattleJingYingSettingWnd:OnClickConfigButton(go)
	CLuaOptionMgr.ShowDlg(
        function() return 2 end,
        function(index) 
            if index==1 then
                return LocalString.GetString("只显示在线成员和棋局精英") 
            else
                return LocalString.GetString("显示全部成员") 
            end
        end,
        function(index) 
            if index==1 then 
                return LuaJuDianBattleMgr.m_FilterOnlineAndJingYing
            else
                return not LuaJuDianBattleMgr.m_FilterOnlineAndJingYing
            end
        end,
        function(index,val) 
            if index==1 then
                if val then
                    LuaJuDianBattleMgr.m_FilterOnlineAndJingYing = true
                    self:UpdateMemberNum()
                end
            else
                if val then
                    LuaJuDianBattleMgr.m_FilterOnlineAndJingYing = false
                    self:UpdateMemberNum()
                end
            end
        end,
        self.transform:TransformPoint(Vector3(600,-200,0)),
        true
    )
end