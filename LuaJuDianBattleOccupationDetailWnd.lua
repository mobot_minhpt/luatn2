local UILabel = import "UILabel"
local QnTableView = import "L10.UI.QnTableView"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaJuDianBattleOccupationDetailWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJuDianBattleOccupationDetailWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaJuDianBattleOccupationDetailWnd, "TableView", "TableView", QnTableView)

--@endregion RegistChildComponent end

function LuaJuDianBattleOccupationDetailWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaJuDianBattleOccupationDetailWnd:Init()
    self.m_ItemList = {}
    
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.m_ItemList
    end, function(item, index)
        self:InitItem(item, index)
    end)

    self.TitleLabel.text = SafeStringFormat3(LocalString.GetString("%s占领详情"), LuaJuDianBattleMgr.DetailWndGuildName)
    Gac2Gas.GuildJuDianQueryGuildFlagAndJuDian(LuaJuDianBattleMgr.DetailWndGuildId)
end

function LuaJuDianBattleOccupationDetailWnd:OnData(mapList)
    self.m_ItemList = {}
    for k, v in pairs(mapList) do
        local data = {}
        data.sceneIdx = k % 100
        data.mapId = math.floor(k / 100)
        data.itemList = v
        table.insert(self.m_ItemList, data)
    end
    self.TableView:ReloadData(true, false)
end

function LuaJuDianBattleOccupationDetailWnd:InitItem(item, index)
    local mapLabel = item.transform:Find("MapLabel"):GetComponent(typeof(UILabel))
    local sceneIdLabel = item.transform:Find("FenXianLabel"):GetComponent(typeof(UILabel))
    local detailLabel = item.transform:Find("DetailLabel"):GetComponent(typeof(UILabel))

    local data = self.m_ItemList[index+1]
    local mapdata = PublicMap_PublicMap.GetData(data.mapId)

    mapLabel.text = mapdata.Name
    sceneIdLabel.text = ""
    --SafeStringFormat3(LocalString.GetString("分线%s"), tostring(data.sceneIdx))
    
    local detailInfo = ""
    for i=1, #data.itemList do
        local itemMark = data.itemList[i]

        if i > 1 then
            detailInfo = detailInfo .. LocalString.GetString("、")
        end

        if itemMark == 0 then
            detailInfo = detailInfo .. LocalString.GetString("[EBAEFF]据点旗[-]")
        else
            detailInfo = detailInfo .. SafeStringFormat3(LocalString.GetString("[71BEFF]据点%s[-]"), tostring(itemMark))
        end
    end
    detailLabel.text = detailInfo
end

function LuaJuDianBattleOccupationDetailWnd:OnEnable()
    g_ScriptEvent:AddListener("GuildJuDianSyncGuildFlagAndJuDian", self, "OnData")
end

function LuaJuDianBattleOccupationDetailWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GuildJuDianSyncGuildFlagAndJuDian", self, "OnData")
end

--@region UIEvent

--@endregion UIEvent

