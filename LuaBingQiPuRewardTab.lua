local UISlider = import "UISlider"

local UITabBar = import "L10.UI.UITabBar"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CItemInfoMgr				= import "L10.UI.CItemInfoMgr"
local AlignType					= import "L10.UI.CItemInfoMgr+AlignType"
local DefaultTableViewDataSource= import "L10.UI.DefaultTableViewDataSource"
local LuaGameObject             = import "LuaGameObject"
local CChatLinkMgr = import "CChatLinkMgr"

LuaBingQiPuRewardTab = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBingQiPuRewardTab, "SubTabs", "SubTabs", UITabBar)
RegistChildComponent(LuaBingQiPuRewardTab, "BQPReward1", "BQPReward1", GameObject)
RegistChildComponent(LuaBingQiPuRewardTab, "BQPReward2", "BQPReward2", GameObject)
RegistChildComponent(LuaBingQiPuRewardTab, "LabelC11", "LabelC11", UILabel)
RegistChildComponent(LuaBingQiPuRewardTab, "LabelC21", "LabelC21", UILabel)
RegistChildComponent(LuaBingQiPuRewardTab, "LabelC31", "LabelC31", UILabel)
RegistChildComponent(LuaBingQiPuRewardTab, "TableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaBingQiPuRewardTab, "LabelC1", "LabelC1", UILabel)
RegistChildComponent(LuaBingQiPuRewardTab, "LabelC2", "LabelC2", UILabel)
RegistChildComponent(LuaBingQiPuRewardTab, "LabelC3", "LabelC3", UILabel)
RegistChildComponent(LuaBingQiPuRewardTab, "CurCountLabel", "CurCountLabel", UILabel)
RegistChildComponent(LuaBingQiPuRewardTab, "Item1", "Item1", GameObject)
RegistChildComponent(LuaBingQiPuRewardTab, "Item2", "Item2", GameObject)
RegistChildComponent(LuaBingQiPuRewardTab, "Item3", "Item3", GameObject)
RegistChildComponent(LuaBingQiPuRewardTab, "Item4", "Item4", GameObject)
RegistChildComponent(LuaBingQiPuRewardTab, "Slider01", "Slider01", UISlider)
RegistChildComponent(LuaBingQiPuRewardTab, "Slider02", "Slider02", UISlider)

--@endregion RegistChildComponent end

RegistClassMember(LuaBingQiPuRewardTab,"m_Inited")
RegistClassMember(LuaBingQiPuRewardTab,"m_TotleVoteData")

function LuaBingQiPuRewardTab:Awake()
    --@region EventBind: Dont Modify Manually!

	self.SubTabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnSubTabsTabChange(index)
	end)

    --@endregion EventBind end

	self.m_Inited = false
end

function LuaBingQiPuRewardTab:OnEnable()
	g_ScriptEvent:AddListener("UpdateBQPStatus",self, "UpdateBQPStatus")
end

function LuaBingQiPuRewardTab:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateBQPStatus",self, "UpdateBQPStatus")
end

function LuaBingQiPuRewardTab:UpdateBQPStatus()
	local count = LuaBingQiPuMgr.m_TotleLikes
	self.CurCountLabel.text = tostring(count)
	local f3 = self.m_TotleVoteData[3].Count
	local f4 = self.m_TotleVoteData[4].Count
	if count < f3 then
		self.Slider01.value = count / f3
		self.Slider02.value = 0
	else
		self.Slider01.value = 1
		self.Slider02.value = (count - f3) / (f4 - f3) 
	end

	for i=1,#self.m_TotleVoteData do
		local data = self.m_TotleVoteData[i]
		self:FillItemExtDot(data.Ctrl,data.Count<=count)
	end
end

function LuaBingQiPuRewardTab:Init()
	if self.m_Inited then return end
	local ctrl2={
		{self.LabelC1,self.LabelC11},
		{self.LabelC2,self.LabelC21},
		{self.LabelC3,self.LabelC31},
	}
	local cfg1 = {}
	local type2Index = 1
	BingQiPu_Reward.Foreach(function (id, data) 
        if data.Type == 0 then
			local d = {
				Name = CChatLinkMgr.TranslateToNGUIText(data.Name),
				Items = {}
			}
			for i=0,data.Items.Length-1,2 do
				local cfg = {
					ID =data.Items[i],
					Count = data.Items[i+1]
				}
				table.insert(d.Items, cfg)
			end
			table.insert(cfg1,d)
		else
			ctrl2[type2Index][1].text = CChatLinkMgr.TranslateToNGUIText(data.Name)
			ctrl2[type2Index][2].text = CChatLinkMgr.TranslateToNGUIText(data.Des)
			type2Index = type2Index + 1
		end
    end)

	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(function()   
		return #cfg1
	end,
	function(item, index)
		self:InitItem(item.gameObject, cfg1[index+1])
	end)
	self.TableView:ReloadData(true, false)

	self:InitTotleReward()

	self.m_Inited = true

	LuaBingQiPuMgr.QueryBQPPraiseCount()
end

function LuaBingQiPuRewardTab:InitTotleReward()
	local setting = BingQiPu_Setting.GetData()
	local totalVoteNum = setting.TotalVoteNum
	local totalVoteGift = setting.TotalVoteGift

	local nums = g_LuaUtil:StrSplitAdv(totalVoteNum,";")
	local gifts = g_LuaUtil:StrSplitAdv(totalVoteGift,";")

	local items = {self.Item1,self.Item2,self.Item3,self.Item4}
	self.m_TotleVoteData = {}
	for i=1,#nums do
		local giftstr = gifts[i]
		local data = {
			Index = i,
			Count = tonumber(nums[i]),
			ItemID = tonumber(giftstr),
			ItemCount = 1,
			Ctrl = items[i]
		}
		self.m_TotleVoteData[i] = data
		self:FillTotleReward(data)
	end
end

function LuaBingQiPuRewardTab:FillTotleReward(data)
	local item = data.Ctrl
	self:FillItem(item,data.ItemID,data.ItemCount,data.Index > 2)
	local extlb = item.transform:Find("ExtLabel"):GetComponent(typeof(UILabel))
	extlb.text = tostring(data.Count)
end

function LuaBingQiPuRewardTab:FillItemExtDot(item,tf)
	local go = item.transform:Find("DotHighlight").gameObject
	go:SetActive(tf)
	local loggo = item.transform:Find("Log").gameObject
	loggo:SetActive(tf)
end

function LuaBingQiPuRewardTab:InitItem(go,data)
	local trans = go.transform
	local label = LuaGameObject.GetChildNoGC(trans,"Label").label
	label.text = data.Name

	local count = #data.Items
	for i=1,5 do
		local cell = LuaGameObject.GetChildNoGC(trans,"ItemCell"..i).gameObject
		if i <= 0 or i > count then
			cell:SetActive(false)
		else
			cell:SetActive(true)
			
			local id = data.Items[i].ID
			local count = data.Items[i].Count
			self:FillItem(cell,id,count,i<=2)
		end
	end
end

function LuaBingQiPuRewardTab:FillItem(item,itemid,itemcount,isleft)
    local itemcfg = Item_Item.GetData(itemid)
    local iconTex = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local ctTxt = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
    if itemcfg then
        iconTex:LoadMaterial(itemcfg.Icon)
    end

    if tonumber(itemcount) <= 1 then
        ctTxt.text = ""
    else
        ctTxt.text = itemcount
    end

    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        local atype
        if isleft then
            atype = AlignType.ScreenRight
        else
            atype = AlignType.ScreenLeft
        end
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, atype, 0, 0, 0, 0)
    end)
end

--@region UIEvent

function LuaBingQiPuRewardTab:OnSubTabsTabChange(index)
	self.BQPReward1:SetActive(index == 0)
	self.BQPReward2:SetActive(index == 1)
end

--@endregion UIEvent

