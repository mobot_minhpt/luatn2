require("common/common_include")

local UIGrid = import "UIGrid"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CUITexture = import "L10.UI.CUITexture"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemAccessTipMgr = import "L10.UI.CItemAccessTipMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local Item_Item = import "L10.Game.Item_Item"
local MessageMgr = import "L10.Game.MessageMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local Money = import "L10.Game.Money"

-- 延长任期公用一个界面
CLuaXianZhiExtendWnd = class()

RegistClassMember(CLuaXianZhiExtendWnd, "m_DescLabel")
RegistClassMember(CLuaXianZhiExtendWnd, "m_NeedItemGrid")
RegistClassMember(CLuaXianZhiExtendWnd, "m_ItemTemplate")
RegistClassMember(CLuaXianZhiExtendWnd, "m_ExtendButton")
RegistClassMember(CLuaXianZhiExtendWnd, "m_TongTianxiCheckBox")
RegistClassMember(CLuaXianZhiExtendWnd, "m_ExpCheckBox")
RegistClassMember(CLuaXianZhiExtendWnd, "m_ExpOwnLabel")
RegistClassMember(CLuaXianZhiExtendWnd, "m_ExpCostLabel")
RegistClassMember(CLuaXianZhiExtendWnd, "m_ExpCostSprite")
RegistClassMember(CLuaXianZhiExtendWnd, "m_ExpOwnSprite")
RegistClassMember(CLuaXianZhiExtendWnd, "m_CostHint")
RegistClassMember(CLuaXianZhiExtendWnd, "m_NeedItems")
RegistClassMember(CLuaXianZhiExtendWnd, "m_IsUseExp")
RegistClassMember(CLuaXianZhiExtendWnd, "m_IsExpEnough")

function CLuaXianZhiExtendWnd:InitComponets()
	self.m_DescLabel = self.transform:Find("Anchor/Desc/Label"):GetComponent(typeof(UILabel))
    self.m_NeedItemGrid = self.transform:Find("Anchor/TongTianXi/NeedItemGrid"):GetComponent(typeof(UIGrid))
    self.m_ItemTemplate = self.transform:Find("Anchor/TongTianXi/ItemTemplate").gameObject
    self.m_ExtendButton = self.transform:Find("Anchor/ExtendBtn").gameObject
	self.m_TongTianxiCheckBox = self.transform:Find("Anchor/TongTianXi/CheckBox"):GetComponent(typeof(QnCheckBox))
	self.m_ExpCheckBox = self.transform:Find("Anchor/Exp/CheckBox"):GetComponent(typeof(QnCheckBox))
	self.m_ExpOwnLabel = self.transform:Find("Anchor/Exp/ExpOwnLabel"):GetComponent(typeof(UILabel))
	self.m_ExpCostLabel = self.transform:Find("Anchor/Exp/ExpCostLabel"):GetComponent(typeof(UILabel))

	self.m_ExpOwnSprite = self.transform:Find("Anchor/Exp/ExpOwnLabel/ExpOwnSprite"):GetComponent(typeof(UISprite))
	self.m_ExpCostSprite = self.transform:Find("Anchor/Exp/ExpCostLabel/ExpCostSprite"):GetComponent(typeof(UISprite))
	self.m_CostHint = self.transform:Find("Anchor/Exp/CostHint"):GetComponent(typeof(UILabel))
end

function CLuaXianZhiExtendWnd:Init()
	self:InitComponets()

	self.m_ItemTemplate:SetActive(false)
	self.m_TongTianxiCheckBox.gameObject:SetActive(true)
	self.m_ExtendButton.gameObject:SetActive(true)

	self:UpdateNeedItemListAndExp()

	self.m_ExpCheckBox.OnValueChanged = DelegateFactory.Action_bool(
		function(select)
			self.m_IsUseExp = select
			self.m_TongTianxiCheckBox:SetSelected(not select, true)
        end
	)
	
	self.m_TongTianxiCheckBox.OnValueChanged = DelegateFactory.Action_bool(
		function(select)
			self.m_IsUseExp = not select
			self.m_ExpCheckBox:SetSelected(not select, true)
        end
	)
	self.m_ExpCheckBox:SetSelected(true, false)

	CommonDefs.AddOnClickListener(self.m_ExtendButton.gameObject, DelegateFactory.Action_GameObject(function (go)
		self:OnExtendButtonClicked(go)
	end), false)
end

function CLuaXianZhiExtendWnd:UpdateNeedItemListAndExp()
	self.m_NeedItems = {}
	local status = CLuaXianzhiMgr.GetXianZhiStatus()

	if status == EnumXianZhiStatus.NotFengXian then
		CUIManager.CloseUI(CLuaUIResources.XianZhiExtendWnd)
		return
	end

	local setting = XianZhi_Setting.GetData()
	local items = nil
	local expFormulaId = nil
	if true then
		-- 延长任期
		if status == EnumXianZhiStatus.DistrictRegion then
			items = setting.DistrictExtendItems
			expFormulaId = setting.DistrictExtendExpFormulaId
		elseif status == EnumXianZhiStatus.CityRegion then
			items = setting.CityExtendItems
			expFormulaId = setting.CityExtendExpFormulaId
		elseif status == EnumXianZhiStatus.SanJie then
			items = setting.SanJieExtendItems
			expFormulaId = setting.SanJieExtendExpFormulaId
		end
	end

	if not items then
		CUIManager.CloseUI(CLuaUIResources.XianZhiExtendWnd)
		return
	end

	for i = 0, items.Length-1, 2 do
		local itemId = items[i]
		local count = items[i+1]
		table.insert(self.m_NeedItems, {
			itemId = itemId,
			count = count,
			})
	end

	local xianZhiData = CClientMainPlayer.Inst.PlayProp.XianZhiData
	local level = XianZhi_Level.GetData(xianZhiData.XianZhiLevel)
	local e = {xianZhiData.XianZhiLevel}
	local needExpCount = GetFormula(expFormulaId)(nil,nil,e)
	self:UpdateSkillExp(needExpCount)
	
	self:UpdateNeedItems()
	self:UpdateExtendDesc()
end

function CLuaXianZhiExtendWnd:UpdateSkillExp(needExp)
	local skillPrivateExp = CClientMainPlayer.Inst.SkillProp.SkillPrivateExp
    local exp = CClientMainPlayer.Inst.PlayProp.Exp
    local qianshiExp = CClientMainPlayer.Inst.QianShiExp

	self.m_CostHint.text = nil

	self.m_IsExpEnough = (skillPrivateExp + exp + qianshiExp) >= needExp
	if self.m_IsExpEnough then
		-- 优先消耗专有经验
		if CClientMainPlayer.Inst.SkillProp.SkillPrivateExp > 0 then
			self.m_ExpOwnSprite.spriteName = Money.GetIconName(EnumMoneyType.PrivateExp, EnumPlayScoreKey.NONE)
			self.m_ExpCostSprite.spriteName = self.m_ExpOwnSprite.spriteName
			self.m_ExpOwnLabel.text = tostring(skillPrivateExp)
		elseif CClientMainPlayer.Inst.QianShiExp > 0 then
			self.m_ExpOwnSprite.spriteName = Money.GetIconName(EnumMoneyType.QianShiExp, EnumPlayScoreKey.NONE)
			self.m_ExpCostSprite.spriteName = self.m_ExpOwnSprite.spriteName
			self.m_ExpOwnLabel.text = tostring(qianshiExp)
		else
			self.m_ExpOwnSprite.spriteName = Money.GetIconName(EnumMoneyType.Exp, EnumPlayScoreKey.NONE)
			self.m_ExpCostSprite.spriteName = self.m_ExpOwnSprite.spriteName
			self.m_ExpOwnLabel.text = tostring(exp)
		end

		local icons = ""
		if skillPrivateExp == 0 then
			if qianshiExp == 0 then
				-- 角色
			else
				-- 前世+角色
				if qianshiExp < needExp then
					icons = icons.."#290"
				end
			end
		else
			if skillPrivateExp >= needExp then
				-- 专用
			elseif qianshiExp == 0 then
				-- 专用+角色
				icons = icons .. "#290"
			else
				if (skillPrivateExp + qianshiExp) < needExp then
					-- 专用+前世+角色
					icons = icons .. "#292"
					icons = icons .. "#290"
				else
					-- 专用+前世
					icons = icons .. "#292"
				end
			end
		end
		if not System.String.IsNullOrEmpty(icons) then
			self.m_CostHint.text = System.String.Format(LocalString.GetString("不足的部分将从{0}中补足"), icons)
		else
			self.m_CostHint.text = nil
		end
	else
		self.m_ExpOwnSprite.spriteName = Money.GetIconName(EnumMoneyType.Exp, EnumPlayScoreKey.NONE)
		self.m_ExpCostSprite.spriteName = self.m_ExpOwnSprite.spriteName
		self.m_ExpOwnLabel.text = tostring(exp)
		self.m_CostHint.text = nil
	end
	self.m_ExpCostLabel.text = tostring(needExp)
end


-- 延长界面的描述
function CLuaXianZhiExtendWnd:UpdateExtendDesc()
	local now = CServerTimeMgr.Inst.timeStamp

	local daysRemain = math.floor((CLuaXianzhiMgr.m_RemainXianZhiTime - now) / 3600 / 24)
	if daysRemain < 0 then
		daysRemain = 0
	end

	local needItemName = ""
	local extendTime = 864000

	for i = 1, #self.m_NeedItems do
		local item = Item_Item.GetData(self.m_NeedItems[i].itemId)
		needItemName = SafeStringFormat3("%s%s,", needItemName, item.Name)
	end

	local status = CLuaXianzhiMgr.GetXianZhiStatus()
	local setting = XianZhi_Setting.GetData()
	if status == EnumXianZhiStatus.DistrictRegion then
		extendTime = setting.DistrictExtendTime
	elseif status == EnumXianZhiStatus.CityRegion then
		extendTime = setting.CityExtendTime
	elseif status == EnumXianZhiStatus.SanJie then
		extendTime = setting.SanJieExtendTime
	end
	local extendDays = math.floor(extendTime/ 3600 / 24)

	local msg = g_MessageMgr:FormatMessage("XianZhi_Extend_Confirm", tostring(daysRemain), string.sub(needItemName, 1, -2), tostring(extendDays))
	self.m_DescLabel.text = msg
end

-- 更新所需物品
function CLuaXianZhiExtendWnd:UpdateNeedItems()
	CUICommonDef.ClearTransform(self.m_NeedItemGrid.transform)

	for i = 1, #self.m_NeedItems do
		local go = NGUITools.AddChild(self.m_NeedItemGrid.gameObject, self.m_ItemTemplate)
		self:InitItemTemplate(go, self.m_NeedItems[i])
		go:SetActive(true)
	end
	self.m_NeedItemGrid:Reposition()
end

function CLuaXianZhiExtendWnd:InitItemTemplate(go, info)
	local Icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local CountLabel = go.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
	local DisableIcon = go.transform:Find("DisableIcon").gameObject
	local itemId = info.itemId
	local count = info.count

	local item = Item_Item.GetData(tonumber(itemId))
	Icon:LoadMaterial(item.Icon)
	local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)
	local totalCount = bindItemCountInBag + notBindItemCountInBag
	CountLabel.text = SafeStringFormat3("%d/%d", totalCount, count)
	if totalCount < count then
		DisableIcon:SetActive(true)
	else
		DisableIcon:SetActive(false)
	end

	CommonDefs.AddOnClickListener(Icon.gameObject, DelegateFactory.Action_GameObject(function (gameObject)
			self:OnItemClicked(gameObject, itemId, totalCount >= count)
		end), false)

end

function CLuaXianZhiExtendWnd:OnItemClicked(go, itemId, isEnough)
	if not isEnough then
		CItemAccessTipMgr.Inst:ShowAccessTip(nil, itemId, false, go.transform, CTooltipAlignType.Top)
	else
		CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
	end
end

function CLuaXianZhiExtendWnd:OnExtendButtonClicked(go)

	if self.m_IsUseExp then
		if self.m_IsExpEnough then
			Gac2Gas.RequestExtendXianZhi(true)
		else
			MessageMgr.Inst:ShowMessage("XianZhi_Extend_Exp_Not_Enough", {})
		end
	else
		if not self:IsNeedItemsEnough() then
			MessageMgr.Inst:ShowMessage("XianZhi_Extend_Items_Not_Enough", {})
		else
			Gac2Gas.RequestExtendXianZhi(false)
		end
	end
end

function CLuaXianZhiExtendWnd:IsNeedItemsEnough()
	for i = 1, #self.m_NeedItems do
		local needCount = self.m_NeedItems[i].count
		local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(self.m_NeedItems[i].itemId)
		local totalCount = bindItemCountInBag + notBindItemCountInBag
		if totalCount < needCount then
			return false
		end
	end
		
	return true
end

function CLuaXianZhiExtendWnd:SendItem(args)
	self:UpdateNeedItems()
end

function CLuaXianZhiExtendWnd:SetItemAt(args)
	self:UpdateNeedItems()
end

function CLuaXianZhiExtendWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateRemainXianZhiTime", self, "UpdateExtendDesc")
	g_ScriptEvent:AddListener("SendItem", self, "SendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "SetItemAt")
end

function CLuaXianZhiExtendWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateRemainXianZhiTime", self, "UpdateExtendDesc")
	g_ScriptEvent:RemoveListener("SendItem", self, "SendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "SetItemAt")
end
