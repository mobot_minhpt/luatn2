local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CUICommonDef = import "L10.UI.CUICommonDef"

LuaDuanWu2022WuduResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDuanWu2022WuduResultWnd, "RestartBtn", "RestartBtn", GameObject)
RegistChildComponent(LuaDuanWu2022WuduResultWnd, "RankBtn", "RankBtn", GameObject)
RegistChildComponent(LuaDuanWu2022WuduResultWnd, "IconRoot", "IconRoot", GameObject)
RegistChildComponent(LuaDuanWu2022WuduResultWnd, "LevelRoot", "LevelRoot", GameObject)
RegistChildComponent(LuaDuanWu2022WuduResultWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaDuanWu2022WuduResultWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaDuanWu2022WuduResultWnd, "NewRecord", "NewRecord", GameObject)
RegistChildComponent(LuaDuanWu2022WuduResultWnd, "Title1", "Title1", GameObject)
RegistChildComponent(LuaDuanWu2022WuduResultWnd, "Title2", "Title2", GameObject)

--@endregion RegistChildComponent end

function LuaDuanWu2022WuduResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.RestartBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRestartBtnClick()
	end)


	
	UIEventListener.Get(self.RankBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankBtnClick()
	end)


    --@endregion EventBind end
end

--difficulty1, difficulty2, difficulty3, floor, usedTime, score, bBreakRecord
function LuaDuanWu2022WuduResultWnd:Init()
	local data = LuaDuanWu2022Mgr.resultData

	self.NewRecord:SetActive(data.bBreakRecord)
	self.ScoreLabel.text = tostring(data.score)

	local sec = data.usedTime%60
	local min = math.floor(data.usedTime/60)
	self.TimeLabel.text = min .. ":" .. sec

	local setting = DuanWu_Setting.GetData()
	local nameList = setting.QuWuDuDifficultyName

	-- 设置等级
	local levelCount = {data.difficulty1, data.difficulty2, data.difficulty3}
	for i = 1, 3 do
		local label = self.LevelRoot.transform:Find(i .. "/LevelLabel"):GetComponent(typeof(UILabel))
		local name = self.LevelRoot.transform:Find(i .. "/Label"):GetComponent(typeof(UILabel))

		name.text = nameList[i-1]
		label.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(levelCount[i]))
	end

	-- 设置图标
	for i = 1, 5 do
		local icon = self.IconRoot.transform:Find(i).gameObject
		CUICommonDef.SetActive(icon, i<=data.floor, true)
	end

	local win = (data.floor == 5)
	self.Title1:SetActive(win)
	self.Title2:SetActive(not win)
end

--@region UIEvent

function LuaDuanWu2022WuduResultWnd:OnRestartBtnClick()
	CUIManager.CloseUI(CLuaUIResources.DuanWu2022WuduResultWnd)
	CUIManager.ShowUI(CLuaUIResources.DuanWu2022WuduEnterWnd)
end

function LuaDuanWu2022WuduResultWnd:OnRankBtnClick()
	CUIManager.ShowUI(CLuaUIResources.DuanWu2022WuduRankWnd)
end

--@endregion UIEvent

