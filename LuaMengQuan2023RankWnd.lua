local GameObject = import "UnityEngine.GameObject"

local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local AlignType = import "CPlayerInfoMgr+AlignType"
local Object = import "System.Object"
local CRankData = import "L10.UI.CRankData"
local Profession = import "L10.Game.Profession"
local Vector3 = import "UnityEngine.Vector3"

LuaMengQuan2023RankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaMengQuan2023RankWnd, "TableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaMengQuan2023RankWnd, "BottomLabel", "BottomLabel", UILabel)
RegistChildComponent(LuaMengQuan2023RankWnd, "MyRankItem", "MyRankItem", GameObject)

--@endregion RegistChildComponent end

function LuaMengQuan2023RankWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.MQHXConfigData = Double11_MQLD.GetData()
    self.m_RankList = {}
    if CLuaRankData.m_CurRankId ~= self.MQHXConfigData.RankId then CUIManager.CloseUI(CLuaUIResources.MengQuan2023RankWnd) end
end

function LuaMengQuan2023RankWnd:Init()
    self.BottomLabel.text = g_MessageMgr:FormatMessage("MengQuan2023_Rank_Bottom_Tip")
    -- 设置排名显示
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.m_RankList
    end, function(item, index)
        item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
        self:InitItem(item, index + 1, self.m_RankList[index + 1])
    end)
    -- 设置选中状态，显示玩家信息
    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function (index)
        local data = self.m_RankList[index + 1]
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined,
                "", nil, Vector3.zero, AlignType.Default)
    end)
    self:InitRankData()
end

function LuaMengQuan2023RankWnd:InitRankData()
    local myInfo = CRankData.Inst.MainPlayerRankInfo
    local rank = myInfo.Rank
    self:InitItem(self.MyRankItem, rank, myInfo)

    self.m_RankList = {}
    for i = 1, CRankData.Inst.RankList.Count do
        table.insert(self.m_RankList, CRankData.Inst.RankList[i - 1])
    end
    self.TableView:ReloadData(true, false)
end

function LuaMengQuan2023RankWnd:InitItem(item, rank, info)
    -- 排名
    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankImage = item.transform:Find("RankImage"):GetComponent(typeof(UISprite))
    local rankImgList = {g_sprites.RankFirstSpriteName, g_sprites.RankSecondSpriteName, g_sprites.RankThirdSpriteName}
    rankImage.spriteName = rankImgList[rank]
    rankLabel.text = rank > 0 and tostring(rank) or LocalString.GetString("未上榜")
    
    -- 其他信息
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local valueLabel = item.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
    local careerSprite = item.transform:Find("ProfessionIcon"):GetComponent(typeof(UISprite))
    local isMe = (CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == info.PlayerIndex or false)
    nameLabel.color = isMe and Color.green or Color.white
    valueLabel.color = isMe and Color.green or Color.white

    nameLabel.text = (isMe and CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name) or info.Name
    valueLabel.text = tostring(info.Value)
    item.transform:Find("My").gameObject:SetActive(isMe)
    careerSprite.spriteName = Profession.GetIcon(info.Job)
end

--@region UIEvent

--@endregion UIEvent

