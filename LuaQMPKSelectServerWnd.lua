local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local CButton = import "L10.UI.CButton"
local EServerStatus = import "L10.UI.EServerStatus"
local CGameServer = import "L10.UI.CGameServer"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"

LuaQMPKSelectServerWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQMPKSelectServerWnd, "ServerTab", "ServerTab", QnTableView)
RegistChildComponent(LuaQMPKSelectServerWnd, "OpBtn", "OpBtn", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaQMPKSelectServerWnd, "m_ServerInfos")
RegistClassMember(LuaQMPKSelectServerWnd, "m_CurServerInx")

function LuaQMPKSelectServerWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.OpBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        local curServerInfo = self.m_ServerInfos[self.m_CurServerInx]
        if curServerInfo == nil then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请先选择一个比赛服"))
            return 
        end
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("QMPK_FASTSWITCHSERVER_MAKESURE"),DelegateFactory.Action(function()
            local gs = CLoginMgr.Inst:GetMainlandServerById(curServerInfo.id)
            if gs then
                CLoginMgr.Inst:FastSwitchGameServer(gs, 0)
            end
        end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
	end)

    self.ServerTab.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_ServerInfos
        end,
        function(item, index)
            self:InitItem(item, index + 1, self.m_ServerInfos[index + 1])
        end
    )
    self.ServerTab.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self.m_CurServerInx = row + 1
    end)

end

function LuaQMPKSelectServerWnd:Init()
    self.m_ServerInfos = {}
    if CLuaQMPKMgr.m_ServerInfos then
        for i = 1, #CLuaQMPKMgr.m_ServerInfos do
            table.insert(self.m_ServerInfos, CLuaQMPKMgr.m_ServerInfos[i])
        end
    end
    self.ServerTab:ReloadData(true, false)
end

--@region UIEvent

--@endregion UIEvent

function LuaQMPKSelectServerWnd:InitItem(item, index, info)
    local serverNameLab     = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local serverStateSpr    = item.transform:Find("severstate"):GetComponent(typeof(UISprite))

    serverNameLab.text = LocalString.GetString("全民争霸服").. tostring(index)
    local status = EServerStatus.Idle
    if info.onlineNum > CQuanMinPKMgr.Inst.m_ServerLimitRed then
        status = EServerStatus.VeryBusy
    elseif info.onlineNum > CQuanMinPKMgr.Inst.m_ServerLimitYellow then
        status = EServerStatus.Busy
    end

    serverStateSpr.spriteName = CGameServer.GetStatusSprite(status)
end

function LuaQMPKSelectServerWnd:OnDestroy()
    CLuaQMPKMgr.ClearFastEnterServerInfo()
end
