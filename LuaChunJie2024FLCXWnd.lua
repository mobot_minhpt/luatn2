
LuaChunJie2024FLCXWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2024FLCXWnd, "tipButton", "TipButton", GameObject)
RegistChildComponent(LuaChunJie2024FLCXWnd, "template", "Template", GameObject)
RegistChildComponent(LuaChunJie2024FLCXWnd, "grid", "Grid", UIGrid)
RegistChildComponent(LuaChunJie2024FLCXWnd, "expireTime", "ExpireTime", UILabel)
--@endregion RegistChildComponent end

function LuaChunJie2024FLCXWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.tipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)	    self:OnTipButtonClick()	end)

    --@endregion EventBind end
end

function LuaChunJie2024FLCXWnd:Init()

end

--@region UIEvent

function LuaChunJie2024FLCXWnd:OnTipButtonClick()
end

--@endregion UIEvent
