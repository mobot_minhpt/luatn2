local GameObject = import "UnityEngine.GameObject"
local AMPlayer = import "L10.Game.CutScene.AMPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local PlayerSettings = import "L10.Game.PlayerSettings"
local QnNewSlider = import "L10.UI.QnNewSlider"
local BoxCollider = import "UnityEngine.BoxCollider"
local CUITexture = import "L10.UI.CUITexture"

LuaDaFuWongVolumeSettingsWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaFuWongVolumeSettingsWnd, "MusicSetting", "MusicSetting", QnSelectableButton)
RegistChildComponent(LuaDaFuWongVolumeSettingsWnd, "SoundSetting", "SoundSetting", QnSelectableButton)
RegistChildComponent(LuaDaFuWongVolumeSettingsWnd, "VolumeSlider", "VolumeSlider", QnNewSlider)
RegistChildComponent(LuaDaFuWongVolumeSettingsWnd, "Background", "Background", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongVolumeSettingsWnd, "m_VolumeSlider_UISlider")
RegistClassMember(LuaDaFuWongVolumeSettingsWnd, "m_MusicSetting_CUITexture")
RegistClassMember(LuaDaFuWongVolumeSettingsWnd, "m_SoundSetting_CUITexture")
function LuaDaFuWongVolumeSettingsWnd:Awake()
    self.m_VolumeSlider_UISlider = self.VolumeSlider:GetComponent(typeof(UISlider))
    self.m_MusicSetting_CUITexture = self.MusicSetting:GetComponent(typeof(CUITexture))
    self.m_SoundSetting_CUITexture = self.SoundSetting:GetComponent(typeof(CUITexture))
    self.VolumeSlider.OnValueChanged = DelegateFactory.Action_float(function(volume)
        self:OnSetVolume(volume)
    end)
    self.MusicSetting.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnSetMusicEnabled()
    end)

    self.SoundSetting.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnSetSoundEnabled()
    end)
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaDaFuWongVolumeSettingsWnd:Init()
    self:LoadData()
end
function LuaDaFuWongVolumeSettingsWnd:OnSetVolume(volume)
    PlayerSettings.VolumeSetting = volume
end

function LuaDaFuWongVolumeSettingsWnd:OnSetMusicEnabled()

    local result = not PlayerSettings.MusicEnabled
    PlayerSettings.MusicEnabled = result -- self.MusicSetting:isSeleted()
    self.m_MusicSetting_CUITexture:LoadMaterial(self:GetTexturePage(result))
end

function LuaDaFuWongVolumeSettingsWnd:OnSetSoundEnabled()

    local result = not PlayerSettings.SoundEnabled
    PlayerSettings.SoundEnabled = result
    if AMPlayer.Inst and AMPlayer.Inst.IsPlaying then
        AMPlayer.Inst:SetMSoundVolume(result and 1 or 0)
    end
    self.m_SoundSetting_CUITexture:LoadMaterial(self:GetTexturePage(result))
end
function LuaDaFuWongVolumeSettingsWnd:LoadData()

    -- self.MusicSetting:SetSelected(PlayerSettings.MusicEnabled)
    -- self.SoundSetting:SetSelected(PlayerSettings.SoundEnabled)
    self.m_MusicSetting_CUITexture:LoadMaterial(self:GetTexturePage(PlayerSettings.MusicEnabled))
    self.m_SoundSetting_CUITexture:LoadMaterial(self:GetTexturePage(PlayerSettings.SoundEnabled))
    self.m_VolumeSlider_UISlider.value = PlayerSettings.VolumeSetting
end

function LuaDaFuWongVolumeSettingsWnd:GetTexturePage(isOpen)
    if isOpen then return "UI/Texture/FestivalActivity/Festival_DaFuWong/Material/dafuwongvoicepackagewnd_open.mat"
    else return  "UI/Texture/FestivalActivity/Festival_DaFuWong/Material/dafuwongvoicepackagewnd_close.mat" end
end

function LuaDaFuWongVolumeSettingsWnd:OnEnable()
    self:LoadData()
    g_ScriptEvent:AddListener("ReloadSettings", self, "LoadData")
end

function LuaDaFuWongVolumeSettingsWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReloadSettings", self, "LoadData")
end

--@region UIEvent

--@endregion UIEvent

