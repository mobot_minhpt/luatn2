local UICamera = import "UICamera"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CChatLinkMgr = import "CChatLinkMgr"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local QnTableView=import "L10.UI.QnTableView"


CLuaGuildHistoryWnd = class()
RegistClassMember(CLuaGuildHistoryWnd,"gridView")
RegistClassMember(CLuaGuildHistoryWnd,"textList")
RegistClassMember(CLuaGuildHistoryWnd,"m_AllGuildHistoryInfo")

function CLuaGuildHistoryWnd:Awake()
    self.closeBtn = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
    self.gridView = self.transform:Find("Anchor/Static/QnTableView"):GetComponent(typeof(QnTableView))
end

function CLuaGuildHistoryWnd:Init( )
    self.gridView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.textList
        end,
        function(item, index)
            self:InitItem(item,index)
        end)
    self.textList={}
end

function CLuaGuildHistoryWnd:OnSendGuildInfo_GuildHistoryInfo() 
    if CLuaGuildMgr.m_GuildHistoryInfo then
        self.m_AllGuildHistoryInfo = {}

        for k,v in pairs(CLuaGuildMgr.m_GuildHistoryInfo) do
            table.insert( self.m_AllGuildHistoryInfo,v )
        end

        table.sort( self.m_AllGuildHistoryInfo, function(a,b)
            return a.Time > b.Time
        end )

        local count = #self.m_AllGuildHistoryInfo
        do
            local i = 0
            while i < count do
                local date = CServerTimeMgr.ConvertTimeStampToZone8Time(self.m_AllGuildHistoryInfo[i+1].Time)
                local content = ToStringWrap(date, "yyyy-MM-dd") .. "       " .. self.m_AllGuildHistoryInfo[i+1].Event
                table.insert( self.textList, CChatLinkMgr.TranslateToNGUIText(content, false) )
                i = i + 1
            end
        end
    end
    self.gridView:ReloadData(false, false)
end

function CLuaGuildHistoryWnd:InitItem(item,row)
    item.m_Label.text = self.textList[row+1]
    UIEventListener.Get(item.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        local url = item.m_Label:GetUrlAtPosition(UICamera.lastWorldPosition)
        if url ~= nil then
            CChatLinkMgr.ProcessLinkClick(url, nil)
        end
    end)
end

function CLuaGuildHistoryWnd:OnEnable()
    g_ScriptEvent:AddListener("SendGuildInfo_GuildHistoryInfo", self, "OnSendGuildInfo_GuildHistoryInfo")
    
    CLuaGuildMgr.GetGuildHistoryInfo()
    self.m_AllGuildHistoryInfo = nil
    self.textList={}
end
function CLuaGuildHistoryWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendGuildInfo_GuildHistoryInfo", self, "OnSendGuildInfo_GuildHistoryInfo")

end
