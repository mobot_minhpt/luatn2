local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UITable = import "UITable"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local Extensions = import "Extensions"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"

LuaHalloween2021GamePlayWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHalloween2021GamePlayWnd, "SingleMatchButton", "SingleMatchButton", GameObject)
RegistChildComponent(LuaHalloween2021GamePlayWnd, "TeamMatchButton", "TeamMatchButton", GameObject)
RegistChildComponent(LuaHalloween2021GamePlayWnd, "MatchNumLabel", "MatchNumLabel", UILabel)
RegistChildComponent(LuaHalloween2021GamePlayWnd, "CancelMatchButton", "CancelMatchButton", GameObject)
RegistChildComponent(LuaHalloween2021GamePlayWnd, "TemplateNode", "TemplateNode", GameObject)
RegistChildComponent(LuaHalloween2021GamePlayWnd, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaHalloween2021GamePlayWnd, "Table", "Table", UITable)

--@endregion RegistChildComponent end

function LuaHalloween2021GamePlayWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.SingleMatchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSingleMatchButtonClick()
	end)


	
	UIEventListener.Get(self.TeamMatchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTeamMatchButtonClick()
	end)


	
	UIEventListener.Get(self.CancelMatchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelMatchButtonClick()
	end)


    --@endregion EventBind end
end

function LuaHalloween2021GamePlayWnd:Init()
	self.TemplateNode:SetActive(false)
	self.MatchNumLabel.text = SafeStringFormat3(LocalString.GetString("今日参与次数%d/%d"),LuaHalloween2021Mgr.todayMatchNum ,1)
	Extensions.RemoveAllChildren(self.Table.transform)
	local msg = g_MessageMgr:FormatMessage("Halloween2021GamePlay_ReadMe")
	if System.String.IsNullOrEmpty(msg) then
        return
    end
	local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then
        return
    end
	for i = 0,info.paragraphs.Count - 1 do
		local paragraphGo = CUICommonDef.AddChild(self.Table.gameObject, self.TemplateNode)
		paragraphGo:SetActive(true)
		CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem)):Init(info.paragraphs[i], 4294967295)
	end
	self.ScrollView:ResetPosition()
    self.Table:Reposition()
	self.SingleMatchButton.gameObject:SetActive(true)
	self.TeamMatchButton.gameObject:SetActive(true)
	self.CancelMatchButton.gameObject:SetActive(false)
	self.MatchNumLabel.text = ""
	Gac2Gas.OpenJieLiangYuanSignUpWnd()
end

function LuaHalloween2021GamePlayWnd:OnEnable()
	g_ScriptEvent:AddListener("OnOpenJieLiangYuanSignUpWnd", self, "OnOpenJieLiangYuanSignUpWnd")
	g_ScriptEvent:AddListener("OnSyncJieLiangYuanSignUpResult", self, "OnSyncJieLiangYuanSignUpResult")
end

function LuaHalloween2021GamePlayWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnOpenJieLiangYuanSignUpWnd", self, "OnOpenJieLiangYuanSignUpWnd")
	g_ScriptEvent:RemoveListener("OnSyncJieLiangYuanSignUpResult", self, "OnSyncJieLiangYuanSignUpResult")
end

function LuaHalloween2021GamePlayWnd:OnOpenJieLiangYuanSignUpWnd(bSignUp, dailyAwardTimes)
	self:OnSyncJieLiangYuanSignUpResult(bSignUp)
	self.MatchNumLabel.text = SafeStringFormat3(LocalString.GetString("今日奖励次数%d/%d"),dailyAwardTimes,Halloween2021_JieLiangYuan.GetData().PlayAwardLimit)
end

function LuaHalloween2021GamePlayWnd:OnSyncJieLiangYuanSignUpResult(bSignUp)
	self.SingleMatchButton.gameObject:SetActive(not bSignUp)
	self.TeamMatchButton.gameObject:SetActive(not bSignUp)
	self.CancelMatchButton.gameObject:SetActive(bSignUp)
end
--@region UIEvent

function LuaHalloween2021GamePlayWnd:OnSingleMatchButtonClick()
	Gac2Gas.RequestSignUpJieLiangYuan()
end

function LuaHalloween2021GamePlayWnd:OnTeamMatchButtonClick()
	Gac2Gas.TeamRequestEnterJieLiangYuan()
end

function LuaHalloween2021GamePlayWnd:OnCancelMatchButtonClick()
	Gac2Gas.RequestCancelSignUpJieLiangYuan()
end

--@endregion UIEvent

