local CUIFx = import "L10.UI.CUIFx"

local QnNewSlider = import "L10.UI.QnNewSlider"

local CUITexture = import "L10.UI.CUITexture"

local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

LuaFishGetAndRecordWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFishGetAndRecordWnd, "FishIcon", "FishIcon", CUITexture)
RegistChildComponent(LuaFishGetAndRecordWnd, "FishNameLbel", "FishNameLbel", UILabel)
RegistChildComponent(LuaFishGetAndRecordWnd, "GuanShangTag", "GuanShangTag", GameObject)
RegistChildComponent(LuaFishGetAndRecordWnd, "FirstGet", "FirstGet", GameObject)
RegistChildComponent(LuaFishGetAndRecordWnd, "WeightNode", "WeightNode", GameObject)
RegistChildComponent(LuaFishGetAndRecordWnd, "Rule", "Rule", QnNewSlider)
RegistChildComponent(LuaFishGetAndRecordWnd, "WeightLabel", "WeightLabel", UILabel)
RegistChildComponent(LuaFishGetAndRecordWnd, "FxNode", "FxNode", CUIFx)
RegistChildComponent(LuaFishGetAndRecordWnd, "FxTextTexture", "FxTextTexture", CUITexture)

--@endregion RegistChildComponent end
RegistClassMember(LuaFishGetAndRecordWnd, "m_MinWeight")
RegistClassMember(LuaFishGetAndRecordWnd, "m_MaxWeight")
RegistClassMember(LuaFishGetAndRecordWnd, "m_ProcessWeight")
RegistClassMember(LuaFishGetAndRecordWnd, "m_MiddleWeight")

function LuaFishGetAndRecordWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    if not LuaSeaFishingMgr.m_HarvestFishItemId then
        CUIManager.CloseUI(CLuaUIResources.FishGetAndRecordWnd)
        return
    end
    local fishData = HouseFish_AllFishes.GetData(LuaSeaFishingMgr.m_HarvestFishItemId)
    local data = Item_Item.GetData(LuaSeaFishingMgr.m_HarvestFishItemId)

    if fishData then
        local weightStr = fishData.Weight
        self.m_MinWeight,self.m_MaxWeight = string.match(weightStr,"(%d+)~(%d+)")
        local isGuanShang = fishData.IsGuanShang == 1 and true or false
        self.GuanShangTag:SetActive(isGuanShang)
        self.m_ProcessWeight = self.m_MinWeight
        self.m_MiddleWeight = (self.m_MinWeight + self.m_MaxWeight) / 2
    end

end

function LuaFishGetAndRecordWnd:Init()
    local weight = LuaSeaFishingMgr.m_FishWeight
    self.WeightLabel.text = weight.."g"
    local itemData = Item_Item.GetData(LuaSeaFishingMgr.m_HarvestFishItemId)
    self.FishIcon:LoadMaterial(itemData.Icon)
    self.FishNameLbel.text = itemData.Name

    local value = (weight - self.m_MinWeight) / (self.m_MaxWeight - self.m_MinWeight)
    self.Rule.m_Value = value
end

function LuaFishGetAndRecordWnd:Update()
    if not self.m_ProcessWeight then return end
    local speed = LuaSeaFishingMgr.m_WeightSpeed * math.max(math.abs(self.m_ProcessWeight - self.m_MiddleWeight)/self.m_MiddleWeight,0.1)
    self.m_ProcessWeight = self.m_ProcessWeight + speed
    if self.m_ProcessWeight >= LuaSeaFishingMgr.m_FishWeight then
        self.m_ProcessWeight = LuaSeaFishingMgr.m_FishWeight
    end 
    local value = (self.m_ProcessWeight - self.m_MinWeight) / (self.m_MaxWeight - self.m_MinWeight)
    self.Rule.m_Value = value
    self.WeightLabel.text = SafeStringFormat3(LocalString.GetString("%.2f两"),self.m_ProcessWeight)
    if self.m_ProcessWeight >= LuaSeaFishingMgr.m_FishWeight then
        self.m_ProcessWeight = nil
        --self.FxNode:LoadFx("fx/ui/prefab/UI_gerenxinjilu.prefab")
        self:InitTitle()
    end 
end

function LuaFishGetAndRecordWnd:InitTitle()
    local isFirstGet,isNewRecord,isHightQuality,isNormal = false,false,false,false
    local fishbasket = CClientMainPlayer.Inst.ItemProp.FishBasket
    if not fishbasket then
        return
    end
    local fxpath = "fx/ui/prefab/UI_haidiao_shouhuo_beijing.prefab"
    local texturePath
    --是否首次获得
    local fishData = HouseFish_AllFishes.GetData(LuaSeaFishingMgr.m_HarvestFishItemId)
    local idx = fishData.FishIdx
    local tujianCounts = fishbasket.TuJianCount
    local count = tujianCounts[idx]
    if count == 1 then
        isFirstGet = true
        texturePath = "UI/Texture/Transparent/Material/haidiao_shoucihuode.mat"
    end
    --是否个人新纪录
    if not isFirstGet then
        local tujianWeights = fishbasket.FishWeight
        local weight = tujianWeights[idx]
        if LuaSeaFishingMgr.m_FishWeight >= weight then
            isNewRecord = true
            fxpath = "fx/ui/prefab/UI_gerenxinjilu.prefab"
        end
        --是否高质量
        if not isNewRecord then
            if fishData.Level >= 3 then
                isHightQuality = true
                texturePath = "UI/Texture/Transparent/Material/haidiao_gaopinzhi.mat"
            end
            --恭喜获得
            if not isHightQuality then
                isNormal = true
                texturePath = "UI/Texture/Transparent/Material/haidiao_gongxihuode.mat"
            end
        end
    end
    self.FxTextTexture.gameObject:SetActive(texturePath)
    if texturePath then
        self.FxTextTexture:LoadMaterial(texturePath)
    end
    self.FxNode:LoadFx(fxpath)
end

--@region UIEvent

--@endregion UIEvent

