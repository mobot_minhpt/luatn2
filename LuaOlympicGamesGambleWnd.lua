
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local DelegateFactory  = import "DelegateFactory"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"

LuaOlympicGamesGambleWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaOlympicGamesGambleWnd, "RewardPoolCount", "RewardPoolCount", UILabel)
RegistChildComponent(LuaOlympicGamesGambleWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaOlympicGamesGambleWnd, "GambleDesc", "GambleDesc", UILabel)
RegistChildComponent(LuaOlympicGamesGambleWnd, "OptionRoot", "OptionRoot", GameObject)
RegistChildComponent(LuaOlympicGamesGambleWnd, "NeedMoneyCount", "NeedMoneyCount", UILabel)
RegistChildComponent(LuaOlympicGamesGambleWnd, "OwnMoneyCount", "OwnMoneyCount", UILabel)
RegistChildComponent(LuaOlympicGamesGambleWnd, "CountInput", "CountInput", QnAddSubAndInputButton)
RegistChildComponent(LuaOlympicGamesGambleWnd, "CommitBtn", "CommitBtn", GameObject)
RegistChildComponent(LuaOlympicGamesGambleWnd, "RuleDescBtn", "RuleDescBtn", GameObject)
RegistChildComponent(LuaOlympicGamesGambleWnd, "ShareBtn", "ShareBtn", GameObject)
RegistChildComponent(LuaOlympicGamesGambleWnd, "CostRoot", "CostRoot", CCurentMoneyCtrl)
RegistChildComponent(LuaOlympicGamesGambleWnd, "BetLabel", "BetLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaOlympicGamesGambleWnd,"m_ChoiceSelectBtn")
RegistClassMember(LuaOlympicGamesGambleWnd,"m_ChoiceLabel")
RegistClassMember(LuaOlympicGamesGambleWnd,"m_ChoicePercent")
RegistClassMember(LuaOlympicGamesGambleWnd,"m_ChoiceSlider")
RegistClassMember(LuaOlympicGamesGambleWnd,"m_CurSeleted")
RegistClassMember(LuaOlympicGamesGambleWnd,"m_IsFirstTime")
RegistClassMember(LuaOlympicGamesGambleWnd,"m_HasInited")
RegistClassMember(LuaOlympicGamesGambleWnd,"m_GambleId")

function LuaOlympicGamesGambleWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.CommitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCommitBtnClick()
	end)


	
	UIEventListener.Get(self.RuleDescBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleDescBtnClick()
	end)


	
	UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)


    --@endregion EventBind end
end

function LuaOlympicGamesGambleWnd:Init()
	self.m_HasInited = false
	self.m_GambleId = nil

	self.m_ChoiceSelectBtn = {}
	self.m_ChoiceLabel = {}
	self.m_ChoicePercent = {}
	self.m_ChoiceSlider = {}

	for i=1,4 do
		table.insert(self.m_ChoiceSelectBtn, self.OptionRoot.transform:Find(tostring(i)):GetComponent(typeof(QnSelectableButton)))
		table.insert(self.m_ChoiceLabel, self.OptionRoot.transform:Find(tostring(i).."/Choice"):GetComponent(typeof(UILabel)))
		table.insert(self.m_ChoicePercent, self.OptionRoot.transform:Find(tostring(i).."/Percent"):GetComponent(typeof(UILabel)))
		table.insert(self.m_ChoiceSlider, self.OptionRoot.transform:Find(tostring(i).."/Slider"):GetComponent(typeof(UISlider)))
	end

	if LuaOlympicGamesMgr.id ~= nil then
		self:InitStatus(LuaOlympicGamesMgr.id, LuaOlympicGamesMgr.myChoice, LuaOlympicGamesMgr.betCount, LuaOlympicGamesMgr.totalBet, LuaOlympicGamesMgr.gambleData)
	end
end

function LuaOlympicGamesGambleWnd:InitStatus(id, myChoice, betCount, totalBet, gambleData)
	self.m_HasInited = true
	self.m_GambleId = id
	local data = OlympicGameActivity_Gamble.GetData(id)
	self.RewardPoolCount.text = tostring(totalBet)


	self.TimeLabel.text = data.EndTime
	self.GambleDesc.text = data.DialogContent
	local descStr = {data.DialogChoice1, data.DialogChoice2, data.DialogChoice3, data.DialogChoice4}

	for i = 1,4 do
		local percent = gambleData[i*2-1]
		local id = gambleData[i*2-2]
		self.m_ChoiceSlider[id].value = percent
		self.m_ChoicePercent[id].text = tostring(percent * 100) .. "%"
		self.m_ChoiceLabel[id].text = descStr[i]

		self.m_ChoiceSelectBtn[i].OnButtonSelected = DelegateFactory.Action_bool(function(b)
			if b then
				for j=1,4 do
					if i~=j then
						self.m_ChoiceSelectBtn[j]:SetSelected(false, false)
					end
				end
				self.m_CurSeleted = i
			end
		end)
	end

	-- 已经有选项时 锁定高亮
	if myChoice>=1 and myChoice<=4 then
		for i=1,4 do
			CUICommonDef.SetColliderActive(self.m_ChoiceSelectBtn[i].gameObject, false)
		end
		self.m_ChoiceSelectBtn[myChoice]:SetSelected(true , false)
		self.m_CurSeleted = myChoice
		if betCount >0 then
			self.BetLabel.text = SafeStringFormat3(LocalString.GetString("已投注%d份"), betCount)
		end
		self.m_IsFirstTime = false
	else
		self.BetLabel.text = ""
		self.m_CurSeleted = nil
		self.m_IsFirstTime = true
	end

	-- 设置消耗
	self.CostRoot:SetCost(data.MoneyConsume)
	self.CountInput:SetMinMax(1, OlympicGameActivity_Setting.GetData().MaxGambleCountLimit, 1)
	self.CountInput.onValueChanged = DelegateFactory.Action_uint(function (value) 
		self.CostRoot:SetCost(data.MoneyConsume * value)
	end)

	self.CostRoot:SetCost(data.MoneyConsume * self.CountInput.m_CurrentValue)
end

function LuaOlympicGamesGambleWnd:OnEnable()
    g_ScriptEvent:AddListener("SendOlympicGambleData2021", self, "InitStatus")
end

function LuaOlympicGamesGambleWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendOlympicGambleData2021", self, "InitStatus")
end

--@region UIEvent

function LuaOlympicGamesGambleWnd:OnCommitBtnClick()
	if self.m_HasInited then
		if self.m_GambleId and self.m_CurSeleted then
			g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Olympic_Gamble_2021_Confirm"), function()
				Gac2Gas.DoOlympicGamble(self.m_GambleId, self.m_CurSeleted, self.CountInput.m_CurrentValue)
			end, nil, nil, nil, false)
		else
			g_MessageMgr:ShowMessage("Olympic_Gamble_2021_NotChoose")
		end
	end
end

function LuaOlympicGamesGambleWnd:OnRuleDescBtnClick()
	g_MessageMgr:ShowMessage("Olympic_Gamble_2021_Rule")
end

function LuaOlympicGamesGambleWnd:OnShareBtnClick()
	if self.m_IsFirstTime or true then
		local shareText = g_MessageMgr:FormatMessage("Olympic_Gamble_2021_Share_Content")
		LuaInGameShareMgr.Share(EnumInGameShareChannelDefine.eXingGuanShare, function()
			return shareText
		end, self.ShareBtn.transform, CPopupMenuInfoMgr.AlignType.Top, nil)
	else
		g_MessageMgr:ShowMessage("Olympic_Gamble_2021_Share_Condition")
	end
end

--@endregion UIEvent
