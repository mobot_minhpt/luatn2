-- Auto Generated!!
local CExpressionActionItem = import "L10.UI.CExpressionActionItem"
local CExpressionActionMgr = import "L10.Game.CExpressionActionMgr"
local DelegateFactory = import "DelegateFactory"
local Expression_Show = import "L10.Game.Expression_Show"
local UILongPressButton = import "L10.UI.UILongPressButton"
CExpressionActionItem.m_Init_CS2LuaHook = function (this, info, isInNeed) 
    this.actionInfo = info
    local expressionAction = Expression_Show.GetData(this.actionInfo:GetID())
    if expressionAction ~= nil then
        local validityPeriodLabel = this.transform:Find("Icon/ValidityPeriodLabel"):GetComponent(typeof(UILabel))
        local text, isShowLabel = LuaExpressionMgr:GetValidityPeriodStr(expressionAction)
        validityPeriodLabel.text = text
        validityPeriodLabel.gameObject:SetActive(isShowLabel)
        this.icon:LoadMaterial(this.actionInfo:GetIcon())
        this.nameLabel.text = this.actionInfo:GetName()
        CommonDefs.GetComponent_Component_Type(this, typeof(UILongPressButton)).OnLongPressDelegate = DelegateFactory.Action(function () 
            if this.OnItemLongPressed ~= nil then
                invoke(this.OnItemLongPressed)
            end
            CExpressionActionMgr.ShowExpressionActionPreview(this.actionInfo:GetID())
        end)

        if this.actionInfo:GetIsLock() then
            this.disableSprite:SetActive(true)
            this.lockSprite:SetActive(true)
            this.alertSprite:SetActive(false)
        else
            this.disableSprite:SetActive(false)
            this.lockSprite:SetActive(false)
        end

        if isInNeed then
            this.needSprite.alpha = 1
        else
            this.needSprite.alpha = 0
        end

        this:RefreshAlert()
    end
end
