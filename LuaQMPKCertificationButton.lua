local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUIFx = import "L10.UI.CUIFx"
local GameObject = import "UnityEngine.GameObject"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Ease = import "DG.Tweening.Ease"
local Vector4 = import "UnityEngine.Vector4"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIFxPaths = import "L10.UI.CUIFxPaths"

LuaQMPKCertificationButton = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQMPKCertificationButton, "Fx", "Fx", CUIFx)
RegistChildComponent(LuaQMPKCertificationButton, "Alert", "Alert", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaQMPKCertificationButton, "m_Name")
RegistClassMember(LuaQMPKCertificationButton, "m_Id")

function LuaQMPKCertificationButton:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaQMPKCertificationButton:OnEnable()
    g_ScriptEvent:AddListener("SubmitQmpkCertificationInfoSuccess", self, "OnSubmitQmpkCertificationInfoSuccess")
    self:RefreshState()
end

function LuaQMPKCertificationButton:OnDisable()
    g_ScriptEvent:RemoveListener("SubmitQmpkCertificationInfoSuccess", self, "OnSubmitQmpkCertificationInfoSuccess")
end

--@region UIEvent

--@endregion UIEvent

function LuaQMPKCertificationButton:RefreshState()
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eQmpkPlayData) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eQmpkPlayData)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            self.m_Name = data.m_ShiMing_Name
            self.m_Id = data.m_ShiMing_Id
        end
    end
    local showAlert = not (self.m_Name and self.m_Id)
    if showAlert then
        self.Fx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
        local b = NGUIMath.CalculateRelativeWidgetBounds(self.transform:Find("Texture"))
        local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
        self.Fx.transform.localPosition = waypoints[0]
        LuaTweenUtils.DOLuaLocalPath(self.Fx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
    else
        LuaTweenUtils.DOKill(self.Fx.transform, true)
    end

    if CLuaActivityAlert.s_QMPKCertificationClicked or (not showAlert) then
        self.Alert:SetActive(false)
    end
end

function LuaQMPKCertificationButton:OnSubmitQmpkCertificationInfoSuccess()
    self:RefreshState()
end