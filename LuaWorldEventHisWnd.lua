require("common/common_include")

local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local UITable = import "UITable"
local CWorldEventMgr = import "L10.Game.CWorldEventMgr"
local ShiJieShiJian_CronMessage2019 = import "L10.Game.ShiJieShiJian_CronMessage2019"
local UIVerticalLabel = import "UIVerticalLabel"
local CUIManager = import "L10.UI.CUIManager"
local CUIRes = import "L10.UI.CUIResources"
local UIPanel = import "UIPanel"
local UIWidget = import "UIWidget"
local UITexture = import "UITexture"
local Vector2 = import "UnityEngine.Vector2"
local TweenPosition = import "TweenPosition"

LuaWorldEventHisWnd = class()

RegistClassMember(LuaWorldEventHisWnd,"closeBtn")
RegistClassMember(LuaWorldEventHisWnd,"template")
RegistClassMember(LuaWorldEventHisWnd,"dragTable")
RegistClassMember(LuaWorldEventHisWnd,"bg")
RegistClassMember(LuaWorldEventHisWnd,"firstLeftNode")
RegistClassMember(LuaWorldEventHisWnd,"firstRightNode")
RegistClassMember(LuaWorldEventHisWnd,"secondNode")

local function split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

local numString = {LocalString.GetString("一"),LocalString.GetString("二"),LocalString.GetString("三"),LocalString.GetString("四"),LocalString.GetString("五"),
LocalString.GetString("六"),LocalString.GetString("七"),LocalString.GetString("八"),LocalString.GetString("九"),LocalString.GetString("十"),}

local GetNumString = function(num)
	num = tonumber(num)
	if num > 0 and num <= 10 then
		return numString[num]
	elseif num > 10 and num <= 19 then
		return numString[10] .. numString[num%10]
	elseif num >= 20 then
		if num%10 == 0  then
			return numString[math.floor(num/10)] .. numString[10]
		else
			return numString[math.floor(num/10)] .. numString[10] .. numString[num%10]
		end
	end
	return LocalString.GetString("零")
end

local GetDataString = function(numString)
 	local dataTable = split(numString,' ')
	if #dataTable > 4 then
		return SafeStringFormat3(LocalString.GetString("%s月%s日"),GetNumString(dataTable[4]),GetNumString(dataTable[3]))
	end
end

local GetTimestampString = function(timestamp)
  local time = os.date("*t",timestamp)
  return SafeStringFormat3(LocalString.GetString("%s月%s日"),GetNumString(time.month),GetNumString(time.day))
end


local offsetx = 0
local rePos = false
local bgWidth = 320
local tablePadding = -5

function LuaWorldEventHisWnd:InitData()
  local tableParent = self.dragTable
	Extensions.RemoveAllChildren(tableParent.transform)

	local readIndex = CWorldEventMgr.Inst.worldReadMessageIdx
	local worldEventIndex = CWorldEventMgr.Inst.worldMessageIdx

  offsetx = 0
  rePos = false

	for i = 1,worldEventIndex do
		local data = ShiJieShiJian_CronMessage2019.GetData(i)
		if data then
			if data.Journal ~= nil and data.Journal ~= '' then
        offsetx = offsetx + bgWidth + tablePadding * 2
			end
		end
	end

  local width,height
  local screenWidth = CommonDefs.GameScreenWidth
  local screenHeight = CommonDefs.GameScreenHeight

  if 1920 / screenWidth > 1080 / screenHeight then
		width = 1920--Screen.width
		height = screenHeight * 1920 / screenWidth--Screen.height
	else
		width = screenWidth * 1080 / screenHeight--Screen.width
		height = 1080--Screen.height
	end

  if offsetx < width - 350 then
    rePos = false
    self.firstLeftNode:GetComponent(typeof(TweenPosition)).to = Vector3(-offsetx/2 - 15,0,0)
    self.firstRightNode:GetComponent(typeof(TweenPosition)).to = Vector3(offsetx/2 - 15,0,0)
  else
    rePos = true
    self.firstLeftNode:GetComponent(typeof(TweenPosition)).to = Vector3(-width/2 + 160,0,0)
    self.firstRightNode:GetComponent(typeof(TweenPosition)).to = Vector3(width/2 - 190,0,0)
  end
end

function LuaWorldEventHisWnd:InitDataEnd()
  local readIndex = CWorldEventMgr.Inst.worldReadMessageIdx
	local worldEventIndex = CWorldEventMgr.Inst.worldMessageIdx

	for i = worldEventIndex,1,-1 do
		local data = ShiJieShiJian_CronMessage2019.GetData(i)
		if data then
			if data.Journal ~= nil and data.Journal ~= '' then
				local go = CUICommonDef.AddChild(self.dragTable,self.template)
				go:SetActive(true)
				local titleLabel = go.transform:Find("node/title"):GetComponent(typeof(UIVerticalLabel))
        titleLabel.fontColor = Color(1,1,1,1)
        if LuaWorldEventMgr.EventTimeTable[data.ID] then
          titleLabel.text = GetTimestampString(LuaWorldEventMgr.EventTimeTable[data.ID])
        else
          titleLabel.text = GetDataString(data.Time)
        end
				local infoLabel = go.transform:Find("node/info"):GetComponent(typeof(UIVerticalLabel))
        infoLabel.mVerOldDir = false
        infoLabel.fontColor = Color(0.52156,0.74509,0.9333,1)
				infoLabel.text = g_MessageMgr:FormatMessage(data.Journal)
        go.transform:Find("node/bg"):GetComponent(typeof(UITexture)).width = 320
        if i == worldEventIndex and CWorldEventMgr.Inst.worldMessageIdx > CWorldEventMgr.Inst.worldReadMessageIdx then
          infoLabel:StartAppearAni(0.1)
        end
			end
		end
	end
	self.dragTable:GetComponent(typeof(UITable)).padding = Vector2(0,tablePadding)
	self.dragTable:GetComponent(typeof(UITable)):Reposition()
	self.dragTable.transform.parent:GetComponent(typeof(UIScrollView)):ResetPosition()

	local bgUISprite = self.bg:GetComponent(typeof(UIWidget))
	local off = -15
	if rePos then
		off = (offsetx - bgUISprite.width)/2 - 3
	end
	self.dragTable.transform.parent:GetComponent(typeof(UIPanel)).clipOffset  = Vector2(self.dragTable.transform.parent:GetComponent(typeof(UIPanel)).clipOffset.x - off,0)
	self.dragTable.transform.parent.localPosition = Vector3(self.dragTable.transform.parent.localPosition.x + off,
	self.dragTable.transform.parent.localPosition.y,self.dragTable.transform.parent.localPosition.z)
end

function LuaWorldEventHisWnd:Init()
	local onCloseClick = function(go)
		CUIManager.CloseUI(CUIRes.WorldEventHisWnd)
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
  self.secondNode:SetActive(false)
	self.template:SetActive(false)

  self:InitData()

  local tp = self.firstLeftNode:GetComponent(typeof(TweenPosition))
  if tp then
    local tweenFinish = function()
      self.secondNode:SetActive(true)
      self:InitDataEnd()
    end
    CommonDefs.AddEventDelegate(tp.onFinished, DelegateFactory.Action(tweenFinish))
  end
  --]]
end

return LuaWorldEventHisWnd
