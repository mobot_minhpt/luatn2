local QnSelectableButton=import "L10.UI.QnSelectableButton"
local QnButton=import "L10.UI.QnButton"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CEquipBaptizeDescTable=import "L10.UI.CEquipBaptizeDescTable"
local CommonDefs = import "L10.Game.CommonDefs"
local Word_AutoXiLianWord = import "L10.Game.Word_AutoXiLianWord"
local MessageWndManager = import "L10.UI.MessageWndManager"

CLuaEquipWordAutoBaptizeTargetWnd = class()
RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_PreDescTable")
-- RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_ConditionTable")
RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_RevertBtn")
RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_SubmitBtn")
RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_TipBtn")
-- RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_ConditionBar")
RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_PredicWordsList")
RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_ButtonDataList")
-- RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_ConditionView")
-- RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_ConditionButtonList")
-- RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_PreNecessaryList")
RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_LeftConditionTable")
RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_ConditionTab1")
RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_ConditionTab2")
RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_CloseBtn")
RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_ConditionItem")
RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_LeftConditionItem")
RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_PreCheckSpeical")
-- RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_ScoreConditionCmp")
-- RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_PreCounts")
-- RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_PreWords")
-- RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_PreNecessary")
-- RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_PreNecessaryList")
-- RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_PreCheckScore")
-- RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_PreCheckSpeical")
-- RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_PreNecessaryList")

RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_CountOptionCmps")
RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_CountOptions")

RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_ChoiceButtons")
RegistClassMember(CLuaEquipWordAutoBaptizeTargetWnd, "m_ChooseChoiceButtons")

function CLuaEquipWordAutoBaptizeTargetWnd:Awake()
    self.m_PredicWordsList = {}
    self.m_ButtonDataList = {}
    self.m_PreNecessaryList = {-1,-1}
    self.m_PreDescTable = self.transform:Find("Anchor/Desc"):GetComponent(typeof(CEquipBaptizeDescTable))
    -- self.m_ConditionTable = self.transform:Find("Anchor/Conditions/ConditionTf"):GetComponent(typeof(UITable))
    self.m_LeftConditionTable = self.transform:Find("Anchor/LeftConditions/ConditionTf")
    -- self.m_ConditionBar = self.transform:Find("Anchor/ConditionBar"):GetComponent(typeof(QnTabView))
    -- self.m_ConditionTab1 = self.transform:Find("Anchor/ConditionBar/Condition1Tab")
    -- self.m_ConditionTab2 = self.transform:Find("Anchor/ConditionBar/Condition2Tab")
    --self.m_ConditionTab3 = self.transform:Find("Anchor/ConditionBar/Condition3Tab")
    --self.m_ConditionTab4 = self.transform:Find("Anchor/ConditionBar/Condition4Tab")
    self.m_RevertBtn = self.transform:Find("Anchor/RevertBtn"):GetComponent(typeof(QnButton)).gameObject
    self.m_SubmitBtn = self.transform:Find("Anchor/SubmitBtn"):GetComponent(typeof(QnButton)).gameObject
    self.m_TipBtn = self.transform:Find("Anchor/TipButton"):GetComponent(typeof(QnButton)).gameObject
    self.m_CloseBtn = self.transform:Find("Wnd_Bg_Secondary_1/CloseButton").gameObject
    self.m_ConditionItem = self.transform:Find("Anchor/Conditions/AutoBaptizeConditionItem").gameObject
    self.m_ConditionItem:SetActive(false)
    self.m_LeftConditionItem = self.transform:Find("Anchor/LeftConditions/AutoBaptizeConditionItem").gameObject
    self.m_LeftConditionItem:SetActive(false)
    -- self.m_ConditionButtonList = {self.m_ConditionTab1,self.m_ConditionTab2}


    local button1 = self.transform:Find("Anchor/ConditionBar/Condition1Tab").gameObject
    local button2 = self.transform:Find("Anchor/ConditionBar/Condition2Tab").gameObject
    UIEventListener.Get(button1).onClick = DelegateFactory.VoidDelegate(function(go)
        CLuaEquipWordBaptizeChooseWordWnd.s_OptionIndex = 1
        CUIManager.ShowUI(CLuaUIResources.EquipWordBaptizeChooseWordWnd)
    end)
    UIEventListener.Get(button2).onClick = DelegateFactory.VoidDelegate(function(go)
        CLuaEquipWordBaptizeChooseWordWnd.s_OptionIndex = 2
        CUIManager.ShowUI(CLuaUIResources.EquipWordBaptizeChooseWordWnd)
    end)
    self.m_ChooseChoiceButtons = {button1,button2}

    UIEventListener.Get(self.m_TipBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("Auto_XiLian_Introduction")
    end)
    self.m_ChoiceButtons = {}
    self.m_ChoiceButtons[1] = self.transform:Find("Anchor/ConditionBar/Choice1"):GetComponent(typeof(QnSelectableButton))
    self.m_ChoiceButtons[2] = self.transform:Find("Anchor/ConditionBar/Choice2"):GetComponent(typeof(QnSelectableButton))
    for i,v in ipairs(self.m_ChoiceButtons) do
        v.OnButtonSelected_02 = DelegateFactory.Action_QnSelectableButton_bool(function(btn,b)
            for j,button in ipairs(self.m_ChoiceButtons) do
                if btn==button then
                    CUICommonDef.SetActive(self.m_ChooseChoiceButtons[j].gameObject,b,true)
                    break
                end
            end
        end)
        v:SetSelected(CLuaEquipXiLianMgr.m_OtherOptionStates[i],false)
    end
    
    --还原目标
    UIEventListener.Get(self.m_RevertBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CLuaEquipXiLianMgr.ClearOption()
        self:OnUpdateEquipWordBaptizeOption(1)
        self:OnUpdateEquipWordBaptizeOption(2)
        for i,v in ipairs(self.m_ChoiceButtons) do
            v:SetSelected(false,false)
        end
        for i,v in ipairs(self.m_CountOptionCmps) do
            if i==1 then
                v:SetSelected(true,false)
            else
                v:SetSelected(false,false)
            end
        end
    end)

    UIEventListener.Get(self.m_SubmitBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickConfirmButton(go)
    end)

    UIEventListener.Get(self.m_CloseBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        if self:CheckChooseChanged() then
            local tip = g_MessageMgr:FormatMessage("Auto_XiLian_Alert")
            MessageWndManager.ShowOKCancelMessage(tip, DelegateFactory.Action(function () 
                -- self:RevertChange()
                CUIManager.CloseUI(CLuaUIResources.EquipWordAutoBaptizeTargetWnd)
            end), nil, nil, nil, false)
        else
            CUIManager.CloseUI(CLuaUIResources.EquipWordAutoBaptizeTargetWnd)
        end
    end)

    self:OnUpdateEquipWordBaptizeOption(1)
    self:OnUpdateEquipWordBaptizeOption(2)
end

function CLuaEquipWordAutoBaptizeTargetWnd:CheckChooseChanged()
    local idx = 1
    for i,v in ipairs(self.m_CountOptionCmps) do
        if v.m_IsSelected then
            idx = i
            break
        end
    end
    if self.m_CountOptions[idx]~=CLuaEquipXiLianMgr.m_countOption then
        return true
    end

    -- for i,v in ipairs(CLuaEquipXiLianMgr.m_OtherOptions) do
    --     if self.m_ConditionOptions[i] then
    --         -- CLuaEquipXiLianMgr.m_OtherOptions[i] = self.m_ConditionOptions[i]
    --         local tmp1,tmp2 = {},{}
    --         for k,b in pairs(self.m_ConditionOptions[i]) do
    --             if b then
    --                 table.insert( tmp1,k )
    --             end
    --         end
    --         for k,b in pairs(CLuaEquipXiLianMgr.m_OtherOptions[i]) do
    --             if b then
    --                 table.insert( tmp2,k )
    --             end
    --         end
    --         local raw1 = table.concat( tmp1, ",")
    --         print(raw1)
    --         local raw2 = table.concat( tmp2, ",")
    --         print(raw2)

    --     else
    --         if #CLuaEquipXiLianMgr.m_OtherOptions[i]>0 then
    --             return true
    --         end
    --     end
    -- end

    local count = 0
    for i,v in ipairs(self.m_ChoiceButtons) do
        if v.m_IsSelected then
            if next(CLuaEquipXiLianMgr.m_OtherOptions[i]) then
                count= count+1
            end
            if CLuaEquipXiLianMgr.m_OtherOptionStates[i]==false then
                return true
            end
        else
            if CLuaEquipXiLianMgr.m_OtherOptionStates[i]==true then
                return true
            end
        end
    end
    return false
end

function CLuaEquipWordAutoBaptizeTargetWnd:OnClickConfirmButton(go)
    local idx = 1
    for i,v in ipairs(self.m_CountOptionCmps) do
        if v.m_IsSelected then
            idx = i
            break
        end
    end
    CLuaEquipXiLianMgr.m_countOption = self.m_CountOptions[idx]

    -- for i,v in ipairs(CLuaEquipXiLianMgr.m_OtherOptions) do
    --     if self.m_ConditionOptions[i] then
    --         CLuaEquipXiLianMgr.m_OtherOptions[i] = self.m_ConditionOptions[i]
    --     else
    --         CLuaEquipXiLianMgr.m_OtherOptions[i] = {}
    --     end
    -- end

    local count = 0
    for i,v in ipairs(self.m_ChoiceButtons) do
        if v.m_IsSelected then
            if next(CLuaEquipXiLianMgr.m_OtherOptions[i]) then
                count= count+1
            end
            CLuaEquipXiLianMgr.m_OtherOptionStates[i] = true
        else
            CLuaEquipXiLianMgr.m_OtherOptionStates[i] = false
        end
    end

	g_ScriptEvent:BroadcastInLua("ConfirmAdvanceXiLian")

    --选择1个条件：非固定词条中，需出现所选词条中的一条才会停止洗炼，是否确认？
    if count==1 then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("AdvanceXiLian_1Choice_Confirm"), DelegateFactory.Action(function () 
            -- self:Submit()
            CUIManager.CloseUI(CLuaUIResources.EquipWordAutoBaptizeTargetWnd)
        end), nil, nil, nil, false)
    --选择2个条件：非固定词条中，其中一个词条需为条件一所选词条中的一条，另一个词条需为条件二所选词条中的一条才会停止洗炼，是否确认？
    elseif count==2 then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("AdvanceXiLian_2Choice_Confirm"), DelegateFactory.Action(function () 
            -- self:Submit()
            CUIManager.CloseUI(CLuaUIResources.EquipWordAutoBaptizeTargetWnd)
        end), nil, nil, nil, false)
    else
        -- self:Submit()
        CUIManager.CloseUI(CLuaUIResources.EquipWordAutoBaptizeTargetWnd)
    end
end

function CLuaEquipWordAutoBaptizeTargetWnd:Init()
    local equipData = CEquipmentProcessMgr.Inst.BaptizingEquipment

    local fixedWordList,extWordList= equipData:GetFixedWordList(false), equipData:GetExtWordList(false)

    local fixedWordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(equipData.Id,List2Table(fixedWordList))
    local fixedWordMaxList = Table2List(fixedWordMaxTable, MakeGenericClass(List, Boolean))

    local extWordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(equipData.Id,List2Table(extWordList))
    local extWordMaxList = Table2List(extWordMaxTable, MakeGenericClass(List, Boolean))

    self.m_PreDescTable:Init(equipData:GetFixedWordList(false), equipData:GetExtWordList(false), nil, nil,fixedWordMaxList,extWordMaxList)
    self:InitBaseConditions(false)
end

-- function CLuaEquipWordAutoBaptizeTargetWnd:InitNecessaryConditionView()
--     for i=1,2,1 do
--         local wordId = LuaAutoBaptizeNecessaryConditionMgr.m_necessaryConditionList[i]
--         if wordId ~= -1 then
--             local desc = self.m_PredicWordsList[wordId].WordDescription
--             self.m_ConditionButtonList[i]:Find("Label"):GetComponent(typeof(UILabel)).text = desc
--         end
--     end
-- end

function CLuaEquipWordAutoBaptizeTargetWnd:InitCountOption(tf,count)
    local label = tf:Find("DescLabel"):GetComponent(typeof(UILabel))
    if count==0 then
        label.text = LocalString.GetString("无数量要求")
    else
        label.text = tostring(count)..LocalString.GetString("条或以上")
    end

    local cmp = tf:GetComponent(typeof(QnSelectableButton))
    cmp:SetSelected(CLuaEquipXiLianMgr.m_countOption==count,false)

    table.insert( self.m_CountOptionCmps,cmp )
    cmp.OnClick = DelegateFactory.Action_QnButton(function(btn)
        for i,v in ipairs(self.m_CountOptionCmps) do
            if v==btn then
                v:SetSelected(true,false)
            else
                v:SetSelected(false,false)
            end
        end
    end)
end

function CLuaEquipWordAutoBaptizeTargetWnd:InitBaseConditions(isRevert)
    local equipData = CEquipmentProcessMgr.Inst.BaptizingEquipment
    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local equipTemplate = CEquipmentProcessMgr.Inst.BaptizingEquipmentTemplate
    local i = 0
    local j = 0
    if equipTemplate ~= nil then
        local countOption = EquipBaptize_AdvWordCountOption.GetData(EnumToInt(equipData.Color))
        if countOption ~= nil then
            self.m_CountOptionCmps = {}
            self.m_CountOptions = {0}--0表示无数量要求
            CommonDefs.EnumerableIterate(countOption.WordCount, DelegateFactory.Action_object(function (count) 
                table.insert( self.m_CountOptions,count )
            end))
            for k,count in ipairs(self.m_CountOptions) do
                local conditionTable = self.m_LeftConditionTable.gameObject
                local go
                if isRevert then
                    go = conditionTable.transform:GetChild(i).gameObject
                else
                    go = NGUITools.AddChild(conditionTable, self.m_LeftConditionItem)
                end
                go:SetActive(true)
                self:InitCountOption(go.transform,count)
                i = i+1
            end
        end
    end
end

-- function CLuaEquipWordAutoBaptizeTargetWnd:CheckChooseChanged()
--     self:Submit()
--     local changed = false
--     local curCounts = CEquipmentBaptizeMgr.Inst:GetAutoBaptizeParam4()
--     local curWords = CEquipmentBaptizeMgr.Inst:GetAutoBaptizeParam5()
--     local curNecessary = CLuaEquipXiLianMgr.GetAutoBaptizeParam6()
--     local curCheckScore = CEquipmentBaptizeMgr.Inst.checkScore
--     local curCheckSpeical = CEquipmentBaptizeMgr.Inst:ContainSpecialWordOption(CEquipmentBaptizeMgr.Word_QingHong_8)

--     local CheckChangeFun = function(str1,str2)
--         local tab1 = loadstring("return ".."{"..str1.."}")()
--         local tab2 = loadstring("return ".."{"..str2.."}")()
--         table.sort(tab1)
--         table.sort(tab2)
--         local changed = false
--         if (#tab1 ~= #tab2) then
--             changed = true
--         else
--             for i=1,#tab1,1 do
--                 if tab1[i] ~= tab2[i] then
--                     changed = true
--                     break
--                 end
--             end
--         end
--         return changed
--     end
--     changed = CheckChangeFun(curCounts,self.m_PreCounts) or CheckChangeFun(curWords,self.m_PreWords) or CheckChangeFun(curNecessary,self.m_PreNecessary) or (self.m_PreCheckScore~=curCheckScore) or (self.m_PreCheckSpeical~=curCheckSpeical)
--     return changed
-- end

function CLuaEquipWordAutoBaptizeTargetWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateEquipWordBaptizeOption", self, "OnUpdateEquipWordBaptizeOption")
end
function CLuaEquipWordAutoBaptizeTargetWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateEquipWordBaptizeOption", self, "OnUpdateEquipWordBaptizeOption")
end
function CLuaEquipWordAutoBaptizeTargetWnd:OnUpdateEquipWordBaptizeOption(idx)
    local options = nil
    -- self.m_ConditionOptions[idx] = options
    local label = nil
    if idx==1 then
        options = CLuaEquipXiLianMgr.m_OtherOptions[1]
        label = self.transform:Find("Anchor/ConditionBar/Condition1Tab/Label"):GetComponent(typeof(UILabel))
    else
        options = CLuaEquipXiLianMgr.m_OtherOptions[2]
        label = self.transform:Find("Anchor/ConditionBar/Condition2Tab/Label"):GetComponent(typeof(UILabel))
    end
    if options and label then
        local count = 0
        local key = next(options)
        -- for k,v in pairs(options) do
        --     print(k,v)
        -- end
        -- print(key)
        for k,v in pairs(options) do
            count = count+1
        end
        if key and key>0 then
            local designData = Word_AutoXiLianWord.GetData(key)
            if count>1 then
                label.text = SafeStringFormat3(LocalString.GetString("%s等%s个目标之一"), designData.WordDescription,tostring(count))
            else
                label.text = designData.WordDescription
            end
        else
            label.text = LocalString.GetString("无")
        end
    end
end
