-- Auto Generated!!
local CJingLingWebImageMgr = import "L10.Game.CJingLingWebImageMgr"
local CQuickAskImageItem = import "L10.UI.CQuickAskImageItem"
CQuickAskImageItem.m_Init_CS2LuaHook = function (this, askText, isHot, imageUrl) 
    this.textLabel.text = ""  --图标可能是半透明的，这里取消文字显示
    this.hotspotIcon.gameObject:SetActive(isHot)
    this.Text = askText
    this.url = imageUrl
    local tex = CJingLingWebImageMgr.Inst:LoadImageByUrl(imageUrl)
    if tex == nil then
        CJingLingWebImageMgr.Inst:DownloadImage(imageUrl, nil)
    else
        this:LoadImage(tex)
    end
end
CQuickAskImageItem.m_OnImageReady_CS2LuaHook = function (this, url) 
    if this.url == url then
        local tex = CJingLingWebImageMgr.Inst:LoadImageByUrl(url)
        this:LoadImage(tex)
    end
end
