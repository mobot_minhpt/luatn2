local CUIFx = import "L10.UI.CUIFx"

local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local UITabBar = import "L10.UI.UITabBar"

local DelegateFactory  = import "DelegateFactory"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"

LuaMJSYPlayWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaMJSYPlayWnd, "BigPortrait", "BigPortrait", CUITexture)
RegistChildComponent(LuaMJSYPlayWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaMJSYPlayWnd, "Description", "Description", UILabel)
RegistChildComponent(LuaMJSYPlayWnd, "FinishTag", "FinishTag", GameObject)
RegistChildComponent(LuaMJSYPlayWnd, "XianSuoView", "XianSuoView", GameObject)
RegistChildComponent(LuaMJSYPlayWnd, "JieXiView", "JieXiView", GameObject)
RegistChildComponent(LuaMJSYPlayWnd, "XianSuoGrid", "XianSuoGrid", UIGrid)
RegistChildComponent(LuaMJSYPlayWnd, "XianSuoItem", "XianSuoItem", GameObject)
RegistChildComponent(LuaMJSYPlayWnd, "Answer", "Answer", UILabel)
RegistChildComponent(LuaMJSYPlayWnd, "Recomand", "Recomand", GameObject)
RegistChildComponent(LuaMJSYPlayWnd, "RecomandGift", "RecomandGift", GameObject)
RegistChildComponent(LuaMJSYPlayWnd, "GoPlayBtn", "GoPlayBtn", GameObject)
RegistChildComponent(LuaMJSYPlayWnd, "GetRecomandBtn", "GetRecomandBtn", GameObject)
RegistChildComponent(LuaMJSYPlayWnd, "Npc1", "Npc1", GameObject)
RegistChildComponent(LuaMJSYPlayWnd, "Npc2", "Npc2", GameObject)
RegistChildComponent(LuaMJSYPlayWnd, "Npc3", "Npc3", GameObject)
RegistChildComponent(LuaMJSYPlayWnd, "Npc4", "Npc4", GameObject)
RegistChildComponent(LuaMJSYPlayWnd, "NpcTab", "NpcTab", UITabBar)
RegistChildComponent(LuaMJSYPlayWnd, "ActiveFxNode", "ActiveFxNode", CUIFx)

--@endregion RegistChildComponent end

function LuaMJSYPlayWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.GoPlayBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGoPlayBtnClick()
	end)


	
	UIEventListener.Get(self.GetRecomandBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGetRecomandBtnClick()
	end)


    --@endregion EventBind end
end

function LuaMJSYPlayWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "UpdateTempPlayTimesWithKey")
end

function LuaMJSYPlayWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "UpdateTempPlayTimesWithKey")
end

function LuaMJSYPlayWnd:UpdateTempPlayTimesWithKey(args)
	--print("UpdateTempPlayTimesWithKey")
	if CUIManager.IsLoaded(CLuaUIResources.QuestionAndAnswerWnd) then
		CUIManager.CloseUI(CLuaUIResources.QuestionAndAnswerWnd)			
	end

	self:InitNpcDetail(self.m_CurNpcIndex)
end


function LuaMJSYPlayWnd:Init()
	self.XianSuoItem:SetActive(false)
	self:InitNpcList()
	self.NpcTab.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self.m_CurNpcIndex = index
        self:InitNpcDetail(index)
    end)
	self.NpcTab:ChangeTab(0,false)
end

function LuaMJSYPlayWnd:IsDreamWishActive(wishIdx)
	local v = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eDreamWish)
	return v and bit.band(v,bit.lshift(1,wishIdx)) > 0
end

function LuaMJSYPlayWnd:IsDreamWishAwarded(npcId)
	local npcIdx = self.m_NpcId2Idx[npcId]
	local v = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eDreamWishAward)
	return v and bit.band(v, bit.lshift(1, npcIdx)) > 0
end

function LuaMJSYPlayWnd:IsFindAllXianSuoByNpcId(npcId)
	local xiansuos = LuaChristmas2021Mgr.m_NPCXianSuoTbl[npcId]
	local firstIdx = xiansuos[1]
	local isFindAll = true
	for i,idx in ipairs(xiansuos) do 
		if not self:IsDreamWishActive(idx) then
			isFindAll = false
			return false
		end
	end
	return isFindAll
end

function LuaMJSYPlayWnd:InitNpcList()
	self.m_Idx2NpcId = {}
	local index = 0
	self.m_NpcOrder = {20023804,20023842,20023862,20023876}
	self.m_NpcId2Idx = {}
	for i,npc in ipairs(self.m_NpcOrder) do 
		self.m_NpcId2Idx[npc] = i
	end

	for _,npcId in ipairs(self.m_NpcOrder) do 
		local npcItem = self.NpcTab:GetTabGoByIndex(index)
		self.m_Idx2NpcId[index+1] = npcId
		local icon = npcItem.transform:GetComponent(typeof(CUITexture))
		local npcdata = NPC_NPC.GetData(npcId)
		if npcdata then
			icon:LoadNPCPortrait(npcdata.Portrait)
		end
		index = index + 1
	end
end

function LuaMJSYPlayWnd:InitNpcDetail(index)
	self.GetRecomandBtn:SetActive(true)
	self.Recomand:SetActive(false)

	local npcId = self.m_Idx2NpcId[index+1]
	self.m_SelectNpcId = npcId
	if not npcId then return end 
	local npcdata = NPC_NPC.GetData(npcId)
	if not npcdata then return end

	self.BigPortrait:LoadNPCPortrait(npcdata.Portrait)
	self.NameLabel.text = npcdata.Name

	local xiansuos = LuaChristmas2021Mgr.m_NPCXianSuoTbl[npcId]
	if not xiansuos then return end
	local firstIdx = xiansuos[1]
	local data = Christmas2021_WishInDream.GetData(firstIdx)
	if data then
		self.Description.text = data.NPCStory
	end

	--显示线索
	self.XianSuoView:SetActive(true)
	Extensions.RemoveAllChildren(self.XianSuoGrid.transform)
	for i,idx in ipairs(xiansuos) do
		local item = NGUITools.AddChild(self.XianSuoGrid.gameObject,self.XianSuoItem)
        item:SetActive(true)
		local lockNodex = item.transform:Find("LockNode").gameObject
		local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
		local iconpath = Christmas2021_WishInDream.GetData(idx).Icon
		--todo线索是否已经获取
		if self:IsDreamWishActive(idx) then
			lockNodex:SetActive(false)
			icon:LoadMaterial(iconpath)
			UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function (go)
				LuaChristmas2021Mgr.OpenXianSuoTipInfoWnd(idx)
			end)
			self.ActiveFxNode:DestroyFx()
			self.ActiveFxNode:LoadFx("fx/ui/prefab/UI_zbsjf_goumaianniu.prefab")
		else
			self.ActiveFxNode:DestroyFx()
			lockNodex:SetActive(true)
			icon:LoadMaterial(nil)
			UIEventListener.Get(item).onClick = nil
		end
	end
	self.XianSuoGrid:Reposition()

	--显示解析
	local isAward = self:IsDreamWishAwarded(npcId)
	self.JieXiView:SetActive(isAward)
	self.XianSuoView:SetActive(not isAward)
	if data then
		self.Answer.text = data.Explanation
		local ridx = LuaChristmas2021Mgr.m_NPC2CorrectIdx[self.m_SelectNpcId]
		local rdata = Christmas2021_WishInDream.GetData(ridx)
		self.RecomandGift.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(rdata.Icon)
		UIEventListener.Get(self.RecomandGift).onClick = DelegateFactory.VoidDelegate(function (go)
			LuaChristmas2021Mgr.OpenXianSuoTipInfoWnd(ridx)
		end)
	end

	self.GetRecomandBtn:SetActive(not isAward)
	self.Recomand:SetActive(isAward)
	self.FinishTag:SetActive(isAward)
end
--@region UIEvent

function LuaMJSYPlayWnd:OnGoPlayBtnClick()
	if self.m_SelectNpcId then
		local xiansuos = LuaChristmas2021Mgr.m_NPCXianSuoTbl[self.m_SelectNpcId]
		local firstIdx = xiansuos[1]
		local data = Christmas2021_WishInDream.GetData(firstIdx)
		if data then
			Gac2Gas.RequestEnterDreamWishPlay(data.GamePlayId)
		end
		
	end
end

function LuaMJSYPlayWnd:OnGetRecomandBtnClick()
	local isFindAll = self:IsFindAllXianSuoByNpcId(self.m_SelectNpcId)
	if not isFindAll then
		g_MessageMgr:ShowMessage("Christmas2021_MJSY_Collect_NotFinish")
		return
	end

	local xiansuos = LuaChristmas2021Mgr.m_NPCXianSuoTbl[self.m_SelectNpcId]
	local firstIdx = xiansuos[1]
	local data = Christmas2021_WishInDream.GetData(firstIdx)

	local questionId = data.QuestionID

	local correctDelegate = DelegateFactory.VoidDelegate(function (go)
		Gac2Gas.RequestDreamWishAward(self.m_SelectNpcId)
	end)
	local mistakeDelegate = DelegateFactory.VoidDelegate(function (go)
		g_MessageMgr:ShowMessage("Christmas2021_MJSY_Answer_NotCorrect")
	end)
	LuaChristmas2021Mgr.OpenQuestionWnd(LocalString.GetString("梦境识愿"),questionId,correctDelegate,mistakeDelegate)


	-- self.GetRecomandBtn:SetActive(false)
	-- self.Recomand:SetActive(true)

	-- if self.m_SelectNpcId then
	-- 	local ridx = LuaChristmas2021Mgr.m_NPC2CorrectIdx[self.m_SelectNpcId]
	-- 	local data = Christmas2021_WishInDream.GetData(ridx)
	-- 	if data then
	-- 		self.RecomandGift.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(data.Icon)
	-- 	end 
	-- end
end


--@endregion UIEvent

