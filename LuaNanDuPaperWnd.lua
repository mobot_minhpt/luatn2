local UIVerticalLabel = import "UIVerticalLabel"

local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Animation = import "UnityEngine.Animation"

LuaNanDuPaperWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNanDuPaperWnd, "Btns", "Btns", GameObject)
RegistChildComponent(LuaNanDuPaperWnd, "LeftArrowBtn", "LeftArrowBtn", GameObject)
RegistChildComponent(LuaNanDuPaperWnd, "RightArrowBtn", "RightArrowBtn", GameObject)
RegistChildComponent(LuaNanDuPaperWnd, "NewsBtn1", "NewsBtn1", GameObject)
RegistChildComponent(LuaNanDuPaperWnd, "NewsBtn2", "NewsBtn2", GameObject)
RegistChildComponent(LuaNanDuPaperWnd, "NewsBtn3", "NewsBtn3", GameObject)
RegistChildComponent(LuaNanDuPaperWnd, "NewsBtn4", "NewsBtn4", GameObject)
RegistChildComponent(LuaNanDuPaperWnd, "OtherReadBtn1", "OtherReadBtn1", GameObject)
RegistChildComponent(LuaNanDuPaperWnd, "OtherReadBtn2", "OtherReadBtn2", GameObject)
RegistChildComponent(LuaNanDuPaperWnd, "OtherReadBtn3", "OtherReadBtn3", GameObject)
RegistChildComponent(LuaNanDuPaperWnd, "OtherReadBtn4", "OtherReadBtn4", GameObject)
RegistChildComponent(LuaNanDuPaperWnd, "SpotTemplate", "SpotTemplate", GameObject)
RegistChildComponent(LuaNanDuPaperWnd, "SpotContent", "SpotContent", GameObject)
RegistChildComponent(LuaNanDuPaperWnd, "CloseBtn", "CloseBtn", GameObject)
RegistChildComponent(LuaNanDuPaperWnd, "PaperDate", "PaperDate", UILabel)
RegistChildComponent(LuaNanDuPaperWnd, "NewsTitle1", "NewsTitle1", UIVerticalLabel)
RegistChildComponent(LuaNanDuPaperWnd, "NewsTitle2", "NewsTitle2", UIVerticalLabel)
RegistChildComponent(LuaNanDuPaperWnd, "NewsTitle3", "NewsTitle3", UIVerticalLabel)
RegistChildComponent(LuaNanDuPaperWnd, "NewsTitle4", "NewsTitle4", UIVerticalLabel)

--@endregion RegistChildComponent end
LuaNanDuPaperWnd.paperIdList = {}
LuaNanDuPaperWnd.firstOpenPaperId = nil
function LuaNanDuPaperWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:RefreshVariableUI(LuaNanDuPaperWnd.firstOpenPaperId)
    self:InitUIEvent()
end

function LuaNanDuPaperWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaNanDuPaperWnd:InitWndData()
    self.curPaperId = LuaNanDuPaperWnd.firstOpenPaperId
    self.paperLength = #LuaNanDuPaperWnd.paperIdList
    
    self.needShowGray = self.paperLength == 1
    self.clickNewsList = {}
    self.clickNewsNumber = 0
end

function LuaNanDuPaperWnd:FindElementInTable(id, list)
    for i = 1, #list do
        if list[i] == id then
            return i
        end
    end
    return 0
end

function LuaNanDuPaperWnd:RefreshVariableUI(paperId)
    local elementType2Title = {[101] = self.NewsTitle1, [102] = self.NewsTitle2, [103] = self.NewsTitle3, [104] = self.NewsTitle4}
    NanDuFanHua_NanDuPaper.Foreach(function(key, data)
        if data.PaperID == paperId then
            self.PaperDate.text = data.PaperDate
            if elementType2Title[data.ElementType] then
                elementType2Title[data.ElementType].fontColor = NGUIText.ParseColor24("D8C8B1", 0)
                elementType2Title[data.ElementType].text = data.NewsTitle
            end
        end
    end)
    
    local inTableIndex = self:FindElementInTable(paperId, LuaNanDuPaperWnd.paperIdList)
    
    self.LeftArrowBtn:SetActive(self.paperLength ~= 1 and inTableIndex ~= 1)
    self.RightArrowBtn:SetActive(self.paperLength ~= 1 and inTableIndex ~= self.paperLength)
    
    self.spotList = {}
    Extensions.RemoveAllChildren(self.SpotContent.transform)
    if self.paperLength > 1 then
        for subId = 1, self.paperLength do
            local subTrans = CommonDefs.Object_Instantiate(self.SpotTemplate.transform)
            subTrans.gameObject:SetActive(true)
            subTrans:SetParent(self.SpotContent.transform)
            subTrans.localScale = Vector3.one
            subTrans.localPosition = Vector3.zero
            subTrans:GetComponent(typeof(UISprite)).alpha = LuaNanDuPaperWnd.paperIdList[subId] == paperId and 0.8 or 0.3
            table.insert(self.spotList, subTrans.gameObject)
        end

        self.SpotContent.transform:GetComponent(typeof(UIGrid)):Reposition()
    end
end

function LuaNanDuPaperWnd:InitUIEvent()
    UIEventListener.Get(self.LeftArrowBtn).onClick = DelegateFactory.VoidDelegate(function ()
        --Click Left
        local inTableIndex = self:FindElementInTable(self.curPaperId, LuaNanDuPaperWnd.paperIdList)
        local leftPaperId = LuaNanDuPaperWnd.paperIdList[inTableIndex-1]
        if leftPaperId then
            self.curPaperId = leftPaperId
            self:RefreshVariableUI(self.curPaperId)
            self.transform:GetComponent(typeof(Animation)):Stop()
            self.transform:GetComponent(typeof(Animation)):Play("nandupaperwnd_qiehuan")
        end
    end)

    UIEventListener.Get(self.RightArrowBtn).onClick = DelegateFactory.VoidDelegate(function ()
        --Click Right
        local inTableIndex = self:FindElementInTable(self.curPaperId, LuaNanDuPaperWnd.paperIdList)
        local rightPaperId = LuaNanDuPaperWnd.paperIdList[inTableIndex+1]
        if rightPaperId then
            self.curPaperId = rightPaperId
            self:RefreshVariableUI(self.curPaperId)
            self.transform:GetComponent(typeof(Animation)):Stop()
            self.transform:GetComponent(typeof(Animation)):Play("nandupaperwnd_qiehuan")
        end
    end)
    
    local newsBtnList = {self.NewsBtn1, self.NewsBtn2, self.NewsBtn3, self.NewsBtn4}
    local newsElementList = {101, 102, 103, 104}
    for i = 1, #newsBtnList do
        UIEventListener.Get(newsBtnList[i]).onClick = DelegateFactory.VoidDelegate(function ()
            --Click NewsBtn i
            LuaNanDuPaperGlassWnd.paperId = self.curPaperId
            LuaNanDuPaperGlassWnd.elementType = newsElementList[i]
            CUIManager.ShowUI(CLuaUIResources.NanDuPaperGlassWnd)
            if self.clickNewsList[i] == nil then
                self.clickNewsList[i] = true
                self.clickNewsNumber = self.clickNewsNumber + 1
            end
            self:HideSomeUI()

            if self.needShowGray then
                Extensions.SetLocalPositionZ(newsBtnList[i].transform, -1)
                newsBtnList[i].transform:Find("CUIFx_huang").gameObject:SetActive(false)
            end
        end)
    end
    
    local otherReadBtnList = {self.OtherReadBtn1, self.OtherReadBtn2, self.OtherReadBtn3, self.OtherReadBtn4}
    local otherReadElementList = {201, 202, 203, 301}
    for i = 1, #otherReadBtnList do
        UIEventListener.Get(otherReadBtnList[i]).onClick = DelegateFactory.VoidDelegate(function ()
            --Click otherReadBtnList i
            LuaNanDuPaperGlassWnd.paperId = self.curPaperId
            LuaNanDuPaperGlassWnd.elementType = otherReadElementList[i]
            CUIManager.ShowUI(CLuaUIResources.NanDuPaperGlassWnd)
            self:HideSomeUI()
        end)
    end
end


function LuaNanDuPaperWnd:OnEnable()
    g_ScriptEvent:AddListener("ClosePaperGlassWnd", self, "ShowSomeUI")
end

function LuaNanDuPaperWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ClosePaperGlassWnd", self, "ShowSomeUI")
end

--放大镜界面时需要关闭一些元素
function LuaNanDuPaperWnd:HideSomeUI()
    self.Btns:SetActive(false)
    self.CloseBtn:SetActive(false)
end 

function LuaNanDuPaperWnd:ShowSomeUI()
    self.Btns:SetActive(true)
    self.CloseBtn:SetActive(true)

    if self.clickNewsNumber == 4 and self.needShowGray then
        Gac2Gas.NanDuPaperAddPaper(LuaNanDuPaperGlassWnd.paperId)
    end
end 
