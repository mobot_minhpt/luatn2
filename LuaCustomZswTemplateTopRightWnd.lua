local UISprite = import "UISprite"
local QnTableView = import "L10.UI.QnTableView"
local UITable = import "UITable"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local CUITexture = import "L10.UI.CUITexture"
local Texture2D = import "UnityEngine.Texture2D"
local TextureFormat = import "UnityEngine.TextureFormat"
local Screen = import "UnityEngine.Screen"
local NativeHandle = import "NativeHandle"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local HTTPHelper = import "L10.Game.HTTPHelper"
local L10 = import "L10"

LuaCustomZswTemplateTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCustomZswTemplateTopRightWnd, "Background", "Background", UISprite)
RegistChildComponent(LuaCustomZswTemplateTopRightWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaCustomZswTemplateTopRightWnd, "Table", "Table", UITable)
RegistChildComponent(LuaCustomZswTemplateTopRightWnd, "TipBtn", "TipBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaCustomZswTemplateTopRightWnd,"m_DefaultHeight")
RegistClassMember(LuaCustomZswTemplateTopRightWnd,"m_TemplateUnlockCount")
RegistClassMember(LuaCustomZswTemplateTopRightWnd,"m_TemplateLimitCount")
RegistClassMember(LuaCustomZswTemplateTopRightWnd,"m_DefaultTemplateCount")
RegistClassMember(LuaCustomZswTemplateTopRightWnd,"m_LevelInfoArr")
RegistClassMember(LuaCustomZswTemplateTopRightWnd,"m_SortIndexTable")
function LuaCustomZswTemplateTopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)


    --@endregion EventBind end
	self.m_DefaultHeight = self.Background.height
	self.m_TemplateUnlockCount = 0
	self.m_TemplateLimitCount = 10
	self.m_LevelInfoArr = {1,1}
	self.m_FreeTemplateCount = 0
	self.m_SortIndexTable = {}
	--self.m_DefaultTemplateCount = 4
	CLuaZswTemplateMgr.RefreshHomeDecorationPicList()
end

function LuaCustomZswTemplateTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncCustomFurnitureTemplateNum", self, "OnSyncCustomFurnitureTemplateNum")
	g_ScriptEvent:AddListener("SyncCustomFurnitureTemplateLv", self, "OnSyncCustomFurnitureTemplateLv")
	g_ScriptEvent:AddListener("UpdateHomeDecorationPicList", self, "OnUpdateHomeDecorationPicList")

end
function LuaCustomZswTemplateTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncCustomFurnitureTemplateNum", self, "OnSyncCustomFurnitureTemplateNum")
	g_ScriptEvent:RemoveListener("SyncCustomFurnitureTemplateLv", self, "OnSyncCustomFurnitureTemplateLv")
	g_ScriptEvent:RemoveListener("UpdateHomeDecorationPicList", self, "OnUpdateHomeDecorationPicList")
end
function LuaCustomZswTemplateTopRightWnd:Init()
	self.m_SortIndexTable = {}
	self.m_TemplateUnlockCount = 0
	local CustomTemplateSlotUnlockCost = Zhuangshiwu_Setting.GetData().CustomTemplateSlotUnlockCost
	for i=0,CustomTemplateSlotUnlockCost.Length-1,1 do
		if CustomTemplateSlotUnlockCost[i] == 0 then
			self.m_TemplateUnlockCount = self.m_TemplateUnlockCount + 1
		end
	end
	self.m_FreeTemplateCount = self.m_TemplateUnlockCount
	if CClientHouseMgr.Inst and CClientHouseMgr.Inst.mCurFurnitureProp2 and CClientHouseMgr.Inst.mCurFurnitureProp2.CustomFurnitureTemplateInfo then
		local infoMap = CClientHouseMgr.Inst.mCurFurnitureProp2.CustomFurnitureTemplateInfo
		local playerId = CClientMainPlayer.Inst.Id
		if infoMap and CommonDefs.DictContains_LuaCall(infoMap, playerId) then
			local customInfo = CommonDefs.DictGetValue_LuaCall(infoMap, playerId)
			self.m_TemplateUnlockCount = customInfo.Num
			self.m_LevelInfoArr = customInfo.LevelInfo
			--print(customInfo.Num,customInfo.LevelInfo[1],customInfo.LevelInfo[2],customInfo.LevelInfo[3])
		end		
	end
	
	local limitCount = Zhuangshiwu_Setting.GetData().MaxCustomTemplateSlotNum
	local count = math.min(self.m_TemplateUnlockCount+1,limitCount)

	--make sort list
	for i=0,count,1 do
		local t = {}	
		t.slot = i+1
		t.level = self.m_LevelInfoArr[i+1]
		if not self.m_LevelInfoArr[i+1] then
			if i+1 <= self.m_FreeTemplateCount then
				t.level = 1
			else
				t.level = 0
			end
		end
		local fillData = CLuaZswTemplateMgr.GetCustomTemplateByIndex(i+1)
		local isFill = 0
		if fillData and fillData.name then
			isFill = 1
		end
		t.isFill = isFill
		table.insert(self.m_SortIndexTable,t)
	end

	table.sort(self.m_SortIndexTable,function(a,b)		
		if a.isFill == b.isFill then
			if a.level == b.level then
				return a.slot < b.slot
			else
				return a.level > b.level
			end
		else
			return a.isFill > b.isFill
		end
	end)

    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return count
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

    self.TableView:ReloadData(false, false)

    local tableHeight = NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform).size.y
    local templateHeight = tableHeight / count

    if count == 0 then--1
        self.Background.height = self.m_DefaultHeight - 176
    elseif count <= 3 then--小于三个的时候自适应高度显示
        self.Background.height = self.m_DefaultHeight + tableHeight - templateHeight
    
    else--大于三个的时候固定高度 滑动显示
        self.Background.height = self.m_DefaultHeight +  2.5*templateHeight
    end
end

function LuaCustomZswTemplateTopRightWnd:InitItem(item,row)
	local Locked = item.transform:Find("Locked")
	local Detail = item.transform:Find("Detail")
	local LimitLabel = Detail:Find("LimitLabel"):GetComponent(typeof(UILabel))
	local LabelName = Detail:Find("LabelName"):GetComponent(typeof(UILabel))
	local Icon = Detail:Find("Icon"):GetComponent(typeof(CUITexture))
	local DeleteBtn = Detail:Find("DeleteBtn")
	local UpgradeBtn = Detail:Find("UpgradeBtn")
	local EditBtn = Detail:Find("EditBtn")
	local EditBtnLabel = EditBtn:Find("Label"):GetComponent(typeof(UILabel))
	local shenHeLabel = Icon.transform:Find("ShenHeLabel"):GetComponent(typeof(UILabel))
	local color = LimitLabel.color
	color.a = 85 / 255
	LimitLabel.color = color

	local slot = self.m_SortIndexTable[row+1].slot
	row = slot - 1
	local isLocked = true
	local level = 1
	if self.m_LevelInfoArr and self.m_LevelInfoArr[slot] then
		level = self.m_LevelInfoArr[slot]
		isLocked = level == 0
	elseif row < self.m_FreeTemplateCount then
		isLocked = false
	end
	UpgradeBtn.gameObject:SetActive(level < 3)
	if isLocked then                       --lock
		Locked.gameObject:SetActive(true)
		Detail.gameObject:SetActive(false)
		UIEventListener.Get(Locked.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnLockedIconClick()
		end)
		Icon:LoadMaterial(nil)
	else			                       -- unlock
		Locked.gameObject:SetActive(false)
		Detail.gameObject:SetActive(true)

		local data = Zhuangshiwu_CustomTemplate.GetData(level)
		if data then
			LimitLabel.text = SafeStringFormat3(LocalString.GetString("上限%d"),data.FurNum)
			Icon:LoadMaterial(data.DefaultIcon)
		end
		UIEventListener.Get(DeleteBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnClickDeleteBtn(slot-1)
		end)
		UIEventListener.Get(UpgradeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnClickUpgradeBtn(slot-1)
		end)
		self:InitEditBtn(slot-1,EditBtn)
		local fillData = CLuaZswTemplateMgr.GetCustomTemplateByIndex(slot)
		if fillData and fillData.name then
			LabelName.text = fillData.name
			LabelName.alpha = 1
			Icon.alpha = 1
			DeleteBtn.gameObject:SetActive(true)
		else
			LabelName.text = LocalString.GetString("模板待添加")
			LabelName.alpha = 85 / 255
			Icon.alpha = 0.3
			DeleteBtn.gameObject:SetActive(false)
		end
		shenHeLabel.gameObject:SetActive(false)
		if fillData and fillData.icon then
			local auditStatus = fillData.auditStatus
			local resulttxt = LocalString.GetString("[ffff00]审核中[-]")
			if auditStatus == -1 then
				resulttxt = LocalString.GetString("[ff0000]未过审[-]")
				shenHeLabel.gameObject:SetActive(true)
			elseif auditStatus == 1 then
				resulttxt = LocalString.GetString("审核通过")
				shenHeLabel.gameObject:SetActive(false)
			else
				shenHeLabel.gameObject:SetActive(true)
			end
			shenHeLabel.text = resulttxt
			L10.Engine.Main.Inst:StartCoroutine(HTTPHelper.NOSDownloadFileData(fillData.icon, DelegateFactory.Action_bool_byte_Array(function (sign, bytes) 
				if sign then
					local _texture = CreateFromClass(Texture2D, 4, 4, TextureFormat.RGBA32, false)
					CommonDefs.LoadImage(_texture, bytes)
					Icon.mainTexture = _texture
				end
			end)))
		end
	end
end

function LuaCustomZswTemplateTopRightWnd:OnLockedIconClick()
	CUIManager.ShowUI(CLuaUIResources.UnlockCustomZswTemplateWnd)
end

function LuaCustomZswTemplateTopRightWnd:OnClickDeleteBtn(row)
	MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Delete_CustomTemplate_Data_Comfire"), DelegateFactory.Action(function ()            
		CLuaZswTemplateMgr.DeleteCustomTemplateWithIndex(row+1)--index
		self.TableView:ReloadData(false, false)
	end), nil, nil, nil, false)
end

function LuaCustomZswTemplateTopRightWnd:OnClickUpgradeBtn(row)
	local level = 1
	if self.m_LevelInfoArr and self.m_LevelInfoArr[row+1] then
		level = self.m_LevelInfoArr[row + 1]
	end
	CLuaZswTemplateMgr.CurCustomTemplateLevel = level
	CLuaZswTemplateMgr.CurCustomTemplateIdx = row+1
	CUIManager.ShowUI(CLuaUIResources.CustomZswTemplateUpgradeWnd)
end

function LuaCustomZswTemplateTopRightWnd:OnClickEditBtn(row)
	CLuaZswTemplateMgr.OpenCustomEditWndWithIndex(row+1)
end

--进入批量操作模式
function LuaCustomZswTemplateTopRightWnd:OnClickAddTemplateBtn(row)
	MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Open_MultipleFurniture_Comfire"), DelegateFactory.Action(function ()            
		CUIManager.CloseUI(CLuaUIResources.CustomZswTemplateTopRightWnd)
		local limitCount = 0
		local level = 1
		if self.m_LevelInfoArr and self.m_LevelInfoArr[row+1] then
			level = self.m_LevelInfoArr[row + 1]
		end
		local data = Zhuangshiwu_CustomTemplate.GetData(level)
		if data then
			limitCount = data.FurNum
		end
		CLuaZswTemplateMgr.EnterCustomTemplateEditingMode(row+1,limitCount)--index
	end), nil, nil, nil, false)
end

function LuaCustomZswTemplateTopRightWnd:InitEditBtn(row,btn)
	local fillData = CLuaZswTemplateMgr.GetCustomTemplateByIndex(row+1)
	local isEmpty = not fillData or (type(fillData)=="table" and not next(fillData))
	local EditBtnLabel = btn:Find("Label"):GetComponent(typeof(UILabel))
	EditBtnLabel.text = isEmpty and LocalString.GetString("添加") or LocalString.GetString("使用")
	UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		if isEmpty then
			self:OnClickAddTemplateBtn(row)
		else
			self:OnClickEditBtn(row)
		end
	end)
end

function LuaCustomZswTemplateTopRightWnd:OnSyncCustomFurnitureTemplateLv(houseId, playerId, slotIdx, lv)
	self.m_LevelInfoArr[slotIdx] = lv
	self:Init()
end

function LuaCustomZswTemplateTopRightWnd:OnSyncCustomFurnitureTemplateNum(houseId, playerId, num)
	self:Init()
end

function LuaCustomZswTemplateTopRightWnd:OnUpdateHomeDecorationPicList()
	self:Init()
end

--@region UIEvent

function LuaCustomZswTemplateTopRightWnd:OnTipBtnClick()
	g_MessageMgr:ShowMessage("CustomZswTemplate_Use_Tip")
end


--@endregion UIEvent

