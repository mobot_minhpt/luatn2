-- Auto Generated!!
local CFadeEffect = import "L10.UI.CFadeEffect"
local Time = import "UnityEngine.Time"
local UIWidget = import "UIWidget"
CFadeEffect.m_Init_CS2LuaHook = function (this) 
    this.m_StartTime = Time.realtimeSinceStartup
    this.m_IsDisappear = CFadeEffect.IsDisappear
    this.m_AliveDuration = CFadeEffect.AliveDuration
    CommonDefs.GetComponent_Component_Type(this, typeof(UIWidget)).alpha = this.m_IsDisappear and 1 or 0
    this.m_IsInited = true
end
