local CTooltip=import "L10.UI.CTooltip"
local CItemBagMgr = import "L10.Game.CItemBagMgr"
local Time = import "UnityEngine.Time"
local System = import "System"
local QualityColor = import "L10.Game.QualityColor"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local EnumQualityType = import "L10.Game.EnumQualityType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumGuildFreightStatus = import "L10.Game.EnumGuildFreightStatus"
local EnumFreightEquipmentType = import "L10.Game.EnumFreightEquipmentType"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CFreightMgr = import "L10.Game.CFreightMgr"
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"
-- local CFreightCargoInfo = import "L10.UI.CFreightCargoInfo"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CButton = import "L10.UI.CButton"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"


CLuaFreightCargoInfo = class()
RegistClassMember(CLuaFreightCargoInfo,"itemCell")
RegistClassMember(CLuaFreightCargoInfo,"cargoIcon")
RegistClassMember(CLuaFreightCargoInfo,"cargoAmount")
RegistClassMember(CLuaFreightCargoInfo,"cargoNameLabel")
RegistClassMember(CLuaFreightCargoInfo,"cargoQuality")
RegistClassMember(CLuaFreightCargoInfo,"acquireGo")
RegistClassMember(CLuaFreightCargoInfo,"fillupBtn")
RegistClassMember(CLuaFreightCargoInfo,"fillupBtnLabel")
RegistClassMember(CLuaFreightCargoInfo,"awardLabel")
RegistClassMember(CLuaFreightCargoInfo,"awardBigLabel")
RegistClassMember(CLuaFreightCargoInfo,"requestHelpBtn")
RegistClassMember(CLuaFreightCargoInfo,"requestHelpLabel")
RegistClassMember(CLuaFreightCargoInfo,"tips")
RegistClassMember(CLuaFreightCargoInfo,"filledLabel")
RegistClassMember(CLuaFreightCargoInfo,"helpFillButton")
RegistClassMember(CLuaFreightCargoInfo,"bangGongFx")
RegistClassMember(CLuaFreightCargoInfo,"item")
RegistClassMember(CLuaFreightCargoInfo,"equip")
RegistClassMember(CLuaFreightCargoInfo,"index")
RegistClassMember(CLuaFreightCargoInfo,"showFxBanggong")
RegistClassMember(CLuaFreightCargoInfo,"RequestBuyCargoTime")
RegistClassMember(CLuaFreightCargoInfo,"requestTime")

function CLuaFreightCargoInfo:Awake()
    self.itemCell = self.transform:Find("ItemCell").gameObject
    self.cargoIcon = self.transform:Find("ItemCell/Icon"):GetComponent(typeof(L10.UI.CUITexture))
    self.cargoAmount = self.transform:Find("ItemCell/Amount"):GetComponent(typeof(UILabel))
    self.cargoNameLabel = self.transform:Find("ItemCell/Name"):GetComponent(typeof(UILabel))
    self.cargoQuality = self.transform:Find("ItemCell/Quality"):GetComponent(typeof(UISprite))
    self.acquireGo = self.transform:Find("ItemCell/Acquire").gameObject
    self.fillupBtn = self.transform:Find("FillUpBtn"):GetComponent(typeof(L10.UI.CButton))
    self.fillupBtnLabel = self.transform:Find("FillUpBtn/Label"):GetComponent(typeof(UILabel))
    self.awardLabel = self.transform:Find("AwardLabel/Banggong"):GetComponent(typeof(UILabel))
    self.awardBigLabel = self.transform:Find("AwardLabel/Banggong/BanggongBig"):GetComponent(typeof(UILabel))
    self.requestHelpBtn = self.transform:Find("RequestHelp"):GetComponent(typeof(L10.UI.CButton))
    self.requestHelpLabel = self.transform:Find("RequestHelp/Label"):GetComponent(typeof(UILabel))
    self.tips = self.transform:Find("Tips").gameObject
    self.filledLabel = self.transform:Find("FilledLabel"):GetComponent(typeof(UILabel))
    self.helpFillButton = self.transform:Find("HelpFillButton"):GetComponent(typeof(L10.UI.CButton))
    self.bangGongFx = self.transform:Find("AwardLabel/Banggong/fx"):GetComponent(typeof(L10.UI.CUIFx))
self.item = nil
self.equip = nil
self.index = 0
self.showFxBanggong = 40
self.RequestBuyCargoTime = 0
self.requestTime = 0


UIEventListener.Get(self.itemCell).onClick = DelegateFactory.VoidDelegate(function(go)
    self:OnItemClicked(go)
end)
-- CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(self.itemCell).onClick, < MakeDelegateFromCSFunction >(self.OnItemClicked, VoidDelegate, self), true)
UIEventListener.Get(self.fillupBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
    self:FillCargo(go)
end)
-- CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(self.fillupBtn.gameObject).onClick, < MakeDelegateFromCSFunction >(self.FillCargo, VoidDelegate, self), true)
UIEventListener.Get(self.requestHelpBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
    self:RequestHelp(go)
end)
-- CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(self.requestHelpBtn.gameObject).onClick, < MakeDelegateFromCSFunction >(self.RequestHelp, VoidDelegate, self), true)
UIEventListener.Get(self.tips).onClick = DelegateFactory.VoidDelegate(function(go)
    self:OnTipsClicked(go)
end)
-- CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(self.tips).onClick, < MakeDelegateFromCSFunction >(self.OnTipsClicked, VoidDelegate, self), true)
UIEventListener.Get(self.helpFillButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
    self:HelpFillCargo(go)
end)
-- CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(self.helpFillButton.gameObject).onClick, < MakeDelegateFromCSFunction >(self.HelpFillCargo, VoidDelegate, self), true)

end

function CLuaFreightCargoInfo:Init( index) 
    if index < 0 or index >= CFreightMgr.Inst.cargoList.Count then
        self.gameObject:SetActive(false)
        return
    end
    self:InitEmpty()
    self.index = index
    local templateId = CFreightMgr.Inst.cargoList[index].templateId

    self.item = CItemMgr.Inst:GetItemTemplate(templateId)
    if self.item == nil then
        self.equip = GuildFreight_Equipments.GetData(templateId)
        if self.equip == nil then
            return
        end
    end
    local cargoInfo = CFreightMgr.Inst.cargoList[index]
    local itemOwnAmount = 0
    if self.item ~= nil then
        itemOwnAmount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, cargoInfo.templateId)
        if CFreightMgr.Inst:IsItemSkillBook(templateId) then
            itemOwnAmount = itemOwnAmount + CFreightMgr.Inst:GetSkillBookAmountInItemBag(templateId)
        elseif self.item.Type == EnumItemType_lua.StatusMedichine then
            local bindCount, unbindCount = CItemBagMgr.Inst:GetItemCountByTemplateIdInItemBag(templateId)
            itemOwnAmount = itemOwnAmount + bindCount + unbindCount
        end
    elseif CommonDefs.DictContains(CFreightEquipSubmitMgr.submitEquipList, typeof(Int32), index) then
        itemOwnAmount = CommonDefs.DictGetValue(CFreightEquipSubmitMgr.submitEquipList, typeof(Int32), index).Count
    end
    if CClientMainPlayer.Inst.Id == CFreightMgr.Inst.playerId then
        self:UpdateLabel(cargoInfo.guildAward)
        if cargoInfo.status == EnumGuildFreightStatus.eNotFilled or cargoInfo.status == EnumGuildFreightStatus.eNotFilledNeedHelp then
            self:InitSelfUnFilled(itemOwnAmount, cargoInfo)
        else
            self:InitSelfFilled(cargoInfo)
        end
    else
        self:InitOtherPlayer(itemOwnAmount, cargoInfo)
    end
    self:InitCargoItemInfo()
    self:TaskOverTime()
end
function CLuaFreightCargoInfo:InitEmpty( )
    self.gameObject:SetActive(true)
    self.cargoAmount.text = ""
    self.acquireGo:SetActive(false)
    self.awardLabel.text = ""
    self.requestHelpLabel.text = LocalString.GetString("求助")
    self.fillupBtn.gameObject:SetActive(true)
    self.requestHelpBtn.gameObject:SetActive(true)
    self.helpFillButton.gameObject:SetActive(false)
    self.tips:SetActive(true)
    self.filledLabel.text = ""
end
function CLuaFreightCargoInfo:InitSelfUnFilled( itemOwnAmount, cargoInfo) 
    if itemOwnAmount >= cargoInfo.amount then
        self.cargoAmount.text = System.String.Format("{0}/{1}", itemOwnAmount, cargoInfo.amount)
        CUIManager.CloseUI(CUIResources.ItemAccessListWnd)
        CItemInfoMgr.CloseItemInfoWnd()
    else
        self.cargoAmount.text = System.String.Format("[c][FF0000]{0}[-]/{1}", itemOwnAmount, cargoInfo.amount)
    end
    self:UpdateSelfFilledUpBtn(itemOwnAmount >= cargoInfo.amount)
    self.acquireGo:SetActive(itemOwnAmount < cargoInfo.amount)
    --awardLabel.text = cargoInfo.guildAward.ToString();
    if CFreightMgr.Inst:FilledCount() >= GuildFreight_GameSetting.GetData().FillBoxCountCanAskForHelp then
        if CFreightMgr.Inst.requestHelpCoolDownDic[self.index] <= 0 then
            self.requestHelpBtn.Enabled = CFreightMgr.Inst:CanHelp(cargoInfo.templateId)
            self.requestHelpLabel.text = System.String.Format(LocalString.GetString("求助 {0}/3"), CFreightMgr.Inst:FilledByHelpCount())
        else
            self.requestHelpBtn.Enabled = false
            self.requestHelpLabel.text = System.String.Format(LocalString.GetString("{0}秒"), CFreightMgr.Inst.requestHelpCoolDownDic[self.index])
        end
    else
        self.requestHelpBtn.Enabled = false
        self.requestHelpLabel.text = LocalString.GetString("求助 0/3")
    end
    if cargoInfo.status == EnumGuildFreightStatus.eNotFilledNeedHelp then
        self.filledLabel.text = LocalString.GetString("求助中")
    end
    if not CFreightMgr.Inst:CanHelp(cargoInfo.templateId) then
        self.filledLabel.text = LocalString.GetString("[c][FF0000]该物品无法进行求助[-]")
    end
end
function CLuaFreightCargoInfo:UpdateSelfFilledUpBtn( countEnough) 
    self.fillupBtn.Enabled = true
    if countEnough then
        self.fillupBtnLabel.text = LocalString.GetString("装  填")
    else
        self.fillupBtnLabel.text = LocalString.GetString("购买并装填")
    end
end
function CLuaFreightCargoInfo:InitSelfFilled( cargoInfo) 
    self.cargoAmount.text = ""
    self.fillupBtn.gameObject:SetActive(false)
    --awardLabel.text = "";
    self.requestHelpBtn.gameObject:SetActive(false)
    self.tips:SetActive(false)
    self.filledLabel.text = LocalString.GetString("货物已装满")
    if cargoInfo.status == EnumGuildFreightStatus.eFilledByHelp then
        CFreightMgr.Inst:QueryHelperNames(cargoInfo.helperId)
    end
end
function CLuaFreightCargoInfo:InitOtherPlayer( itemOwnAmount, cargoInfo) 
    self.requestHelpBtn.gameObject:SetActive(false)
    self.fillupBtn.gameObject:SetActive(false)
    self.helpFillButton.gameObject:SetActive(true)
    self:UpdateLabel(cargoInfo.guildAward)
    self:QueryHelpTimes()
    if cargoInfo.status == EnumGuildFreightStatus.eNotFilledNeedHelp then
        if itemOwnAmount >= cargoInfo.amount then
            self.cargoAmount.text = System.String.Format("{0}/{1}", itemOwnAmount, cargoInfo.amount)
        else
            self.cargoAmount.text = System.String.Format("[c][FF0000]{0}[-]/{1}", itemOwnAmount, cargoInfo.amount)
        end
        self.acquireGo:SetActive(itemOwnAmount < cargoInfo.amount)
        self.helpFillButton.Enabled = itemOwnAmount >= cargoInfo.amount
        self.filledLabel.text = LocalString.GetString("求助中")
    elseif cargoInfo.status ~= EnumGuildFreightStatus.eNotFilled then
        self.cargoAmount.text = ""
        --awardLabel.text = "";
        self.tips:SetActive(false)
        self.filledLabel.text = LocalString.GetString("货物已装满")
        self.helpFillButton.Enabled = false
    else
        self.cargoAmount.text = ""
        self.helpFillButton.Enabled = false
        --awardLabel.text = "";
    end
end
function CLuaFreightCargoInfo:InitCargoItemInfo( )
    if self.item ~= nil then
        self.cargoIcon:LoadMaterial(self.item.Icon)
        self.cargoQuality.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
        self.cargoNameLabel.text = self.item.Name
        self.cargoNameLabel.color = GameSetting_Common_Wapper.Inst:GetColor(self.item.NameColor)
    elseif self.equip ~= nil then
        self.cargoIcon:LoadMaterial(self.equip.Icon)
        self.cargoQuality.spriteName = CUICommonDef.GetItemCellBorder(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), self.equip.Color))
        local default
        if self.equip.Color == 5 then
            default = LocalString.GetString("红色")
        else
            default = LocalString.GetString("紫色")
        end
        self.cargoNameLabel.text = CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("{0}-{1}级{2}{3}"), {self.equip.MinGrade, self.equip.MaxGrade, default, EnumFreightEquipmentType.GetEquipmentType(System.Int32.Parse(CommonDefs.StringSplit_ArrayChar(self.equip.Type, ",")[0]))})
        self.cargoNameLabel.color = self.equip.Color == 5 and QualityColor.GetRGBValue(EnumQualityType.Red) or QualityColor.GetRGBValue(EnumQualityType.Purple)
    end
end
function CLuaFreightCargoInfo:TaskOverTime( )
    if CFreightMgr.Inst.endTime < CFreightMgr.Inst.currentTime then
        self.fillupBtn.Enabled = false
        self.requestHelpBtn.Enabled = false
        self.helpFillButton.Enabled = false
    end
end
function CLuaFreightCargoInfo:OnFreightHelpTimesGet( args) 
    local count=args[0]
    if CClientMainPlayer.Inst.Id ~= CFreightMgr.Inst.playerId then
        if count == 0 then
            self.filledLabel.text = LocalString.GetString("[c][FFFF00]今日帮助装填次数已满")
        elseif not CFreightMgr.Inst:CanHelp(self.item.ID) then
            self.filledLabel.text = LocalString.GetString("[c][FF0000]该物品无法进行求助[-]")
        else
            local totalHelpTimesPerDay = 3
            self.filledLabel.text = System.String.Format(LocalString.GetString("[c][FFFFFF]今日已帮助装填{0}/{1}"), totalHelpTimesPerDay - count, totalHelpTimesPerDay)
        end
        if self.helpFillButton.Enabled then
            self.helpFillButton.Enabled = count > 0
        end
    end
end
function CLuaFreightCargoInfo:UpdateLabel( guildAward) 
    if guildAward >= self.showFxBanggong then
        self.bangGongFx:LoadFx(CUIFxPaths.FreightBangGongFx)
        self.awardLabel.text = ""
        self.awardBigLabel.text = tostring(guildAward)
    else
        self.bangGongFx:DestroyFx()
        self.awardBigLabel.text = ""
        self.awardLabel.text = tostring(guildAward)
    end
end
function CLuaFreightCargoInfo:OnEnable( )
    g_ScriptEvent:AddListener("OnFillCargoSuccess", self, "FillCargoSuccess")
    g_ScriptEvent:AddListener("OnRequestHelpSuccess", self, "RequestHelpSuccess")
    g_ScriptEvent:AddListener("OnRequestHelperNameSuccess", self, "OnQueryHelperNameSuccess")
    g_ScriptEvent:AddListener("OnFreightSubmitEquipSelected", self, "FillEquipment")
    g_ScriptEvent:AddListener("FreightCargoRequestHelpCd", self, "OnFreightCargoRequestHelpCd")
    g_ScriptEvent:AddListener("FreightHelpTimesGet", self, "OnFreightHelpTimesGet")
    g_ScriptEvent:AddListener("PlayerBuyMallItemSuccess", self, "OnPlayerBuyMallItemSuccess")
    g_ScriptEvent:AddListener("OnQuickBuyItemFromPlayerShopSuccess", self, "OnQuickBuyItemFromPlayerShopSuccess")
    g_ScriptEvent:AddListener("OnQuickBuyItemFromPlayerShopFail", self, "OnQuickBuyItemFromPlayerShopFail")
end
function CLuaFreightCargoInfo:OnDisable( )
    g_ScriptEvent:RemoveListener("OnFillCargoSuccess", self, "FillCargoSuccess")
    g_ScriptEvent:RemoveListener("OnRequestHelpSuccess", self, "RequestHelpSuccess")
    g_ScriptEvent:RemoveListener("OnRequestHelperNameSuccess", self, "OnQueryHelperNameSuccess")
    g_ScriptEvent:RemoveListener("OnFreightSubmitEquipSelected", self, "FillEquipment")
    g_ScriptEvent:RemoveListener("FreightCargoRequestHelpCd", self, "RequestHelpCdUpdate")
    g_ScriptEvent:RemoveListener("FreightHelpTimesGet", self, "OnFreightHelpTimesGet")
    g_ScriptEvent:RemoveListener("PlayerBuyMallItemSuccess", self, "OnPlayerBuyMallItemSuccess")
    g_ScriptEvent:RemoveListener("OnQuickBuyItemFromPlayerShopSuccess", self, "OnQuickBuyItemFromPlayerShopSuccess")
    g_ScriptEvent:RemoveListener("OnQuickBuyItemFromPlayerShopFail", self, "OnQuickBuyItemFromPlayerShopFail")
end
function CLuaFreightCargoInfo:OnTipsClicked(go)
    CTooltip.Show(GuildFreight_GameSetting.GetData().Tipsone, self.tips.transform, CTooltip.AlignType.Top)
end
function CLuaFreightCargoInfo:HelpFillCargo(go)
    CFreightMgr.Inst:UpdateFreightBoxStatus(CFreightMgr.Inst.playerId, self.index + 1, EnumGuildFreightStatus_lua.eFilledByHelp,"")
end
function CLuaFreightCargoInfo:QueryHelpTimes()
    Gac2Gas.QueryGuildFreightGiveHelpCount()
end

function CLuaFreightCargoInfo:OnPlayerBuyMallItemSuccess( args) 
    local regionId, templateId, itemCount, reason=args[0],args[1],args[2],args[3]
    if reason == "GuildFreight" and regionId == 4 and templateId == self.item.ID then
        self:DoFillCargo()
    end
end
function CLuaFreightCargoInfo:OnQuickBuyItemFromPlayerShopSuccess( args) 
    local price, itemName, itemTemplateId, num=args[0],args[1],args[2],args[3]
    if itemTemplateId == self.item.ID then
        g_MessageMgr:ShowMessage("QUICK_BUY_ITEM_FROM_PLAYERSHOP_SUCCESS", price, num, itemName)
        self:DoFillCargo()
    end
end
function CLuaFreightCargoInfo:OnQuickBuyItemFromPlayerShopFail( args) 
    local itemTemplateId, num=args[0],args[1]
    if itemTemplateId ~= self.item.ID then
        return
    end
    local access = ItemGet_item.GetData(self.item.ID)
    if access ~= nil and access.NPCPath ~= nil and access.NPCPath.Length == 2 then
        g_MessageMgr:ShowMessage("BUY_FAIL_TO_PATH_FINDING")
        CTrackMgr.Inst:FindNPC(access.NPCPath[0], access.NPCPath[1], 0, 0, nil, nil)
    else
        g_MessageMgr:ShowMessage("QUICK_BUY_ITEM_FROM_PLAYERSHOP_FAIL")
    end
end
function CLuaFreightCargoInfo:OnFreightCargoRequestHelpCd( args) 
    self:RequestHelpCdUpdate(args[0])
end
function CLuaFreightCargoInfo:RequestHelpCdUpdate( index) 
    -- local index=args[0]
    if self.index == index then
        if CFreightMgr.Inst.requestHelpCoolDownDic[index] > 0 then
            CommonDefs.GetComponent_Component_Type(self.requestHelpBtn, typeof(CButton)).Enabled = false
            self.requestHelpLabel.text = System.String.Format(LocalString.GetString("{0}秒"), CFreightMgr.Inst.requestHelpCoolDownDic[index])
        else
            CommonDefs.GetComponent_Component_Type(self.requestHelpBtn, typeof(CButton)).Enabled = true
            self.requestHelpLabel.text = System.String.Format(LocalString.GetString("求助 {0}/3"), CFreightMgr.Inst:FilledByHelpCount())
        end
    end
end
function CLuaFreightCargoInfo:OnQueryHelperNameSuccess( args) 
    local id=args[0]
    if CFreightMgr.Inst.cargoList[self.index].helperId == id and CommonDefs.DictContains(CFreightMgr.Inst.helperNames, typeof(UInt64), id) then
        self.filledLabel.text = CommonDefs.DictGetValue(CFreightMgr.Inst.helperNames, typeof(UInt64), id) .. LocalString.GetString("帮助装满")
    end
end
function CLuaFreightCargoInfo:OnItemClicked( go) 
    local t_name={}
	t_name[1]=LocalString.GetString("获取途径") 
	local t_action={}
	t_action[1] = function ()
		CItemAccessListMgr.Inst:ShowItemAccessInfo(self.item.ID, true, self.transform,CTooltip.AlignType.Right)
    end
    
    local actionSource=DefaultItemActionDataSource.Create(1,t_action,t_name)

    if self.item ~= nil then
        CItemInfoMgr.ShowLinkItemTemplateInfo(self.item.ID, false, actionSource, AlignType.Default, 0, 0, 0, 0)

    elseif self.equip ~= nil then
        CItemInfoMgr.ShowFreightEquipInfo(self.equip, actionSource)
    end
end
function CLuaFreightCargoInfo:FillCargo( go) 
    local cargoInfo = CFreightMgr.Inst.cargoList[self.index]
    local amount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, cargoInfo.templateId)
    if CFreightMgr.Inst:IsItemSkillBook(cargoInfo.templateId) then
        amount = amount + CFreightMgr.Inst:GetSkillBookAmountInItemBag(cargoInfo.templateId)
    elseif self.item.Type == EnumItemType_lua.StatusMedichine then
        local bindCount, unbindCount = CItemBagMgr.Inst:GetItemCountByTemplateIdInItemBag(cargoInfo.templateId)
        amount = amount + bindCount + unbindCount
    end
    if amount >= cargoInfo.amount then
        self:DoFillCargo()
    else
        self:DoBuyCargo(cargoInfo.amount - amount)
    end
end
function CLuaFreightCargoInfo:DoFillCargo( )
    --装填货物
    if self.item ~= nil and CClientMainPlayer.Inst.Id == CFreightMgr.Inst.playerId then
        CFreightMgr.Inst:UpdateFreightBoxStatus(CFreightMgr.Inst.playerId, self.index + 1, EnumGuildFreightStatus_lua.eFilled, "")
        self.RequestBuyCargoTime = Time.realtimeSinceStartup
    elseif self.item == nil and self.equip ~= nil then
        CFreightEquipSubmitMgr.OpenEquipmentChooseWnd("", CFreightEquipSubmitMgr.submitEquipList, nil, CFreightEquipSubmitMgr.curSelectIndex, true, nil, "", nil, "")
    end
end
function CLuaFreightCargoInfo:DoBuyCargo( count) 
    if Time.realtimeSinceStartup - self.RequestBuyCargoTime <= GuildFreight_GameSetting.GetData().BuyColdTime then
        g_MessageMgr:ShowMessage("RPC_TOO_FREQUENT")
        return
    end
    self.RequestBuyCargoTime = Time.realtimeSinceStartup
    local itemGet = ItemGet_item.GetData(self.item.ID)
    if itemGet ~= nil then
        if CommonDefs.Array_IndexOf_int(itemGet.GetPath, EnumItemAccessType_lua.PlayerShop) >= 0 then
            --玩家商店购买
            Gac2Gas.RequestQuickBuyItemFromPlayerShop(self.item.ID, count, true)
        elseif CommonDefs.Array_IndexOf_int(itemGet.GetPath, EnumItemAccessType_lua.Yishi) >= 0 then
            --易市购买
            Gac2Gas.BuyMallItemWithReason(EShopMallRegion_lua.EYuanbaoMarket, self.item.ID, count, "GuildFreight")
        end
    end
end
function CLuaFreightCargoInfo:FillEquipment( args) 
    local equipmentId, key=args[0],args[1]
    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, equipmentId)
    local place = EnumItemPlace_lua.Bag
    local context = System.String.Format("{0},{1},{2}", place, pos, equipmentId)
    if CClientMainPlayer.Inst.Id == CFreightMgr.Inst.playerId then
        CFreightMgr.Inst:UpdateFreightBoxStatus(CFreightMgr.Inst.playerId, self.index + 1, EnumGuildFreightStatus_lua.eFilled, context)
        self.RequestBuyCargoTime = Time.realtimeSinceStartup
    else
        CFreightMgr.Inst:UpdateFreightBoxStatus(CFreightMgr.Inst.playerId, self.index + 1, EnumGuildFreightStatus_lua.eFilledByHelp, context)
    end
end
function CLuaFreightCargoInfo:RequestHelp( go) 
    if Time.realtimeSinceStartup - self.requestTime <= 1 then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请稍等"))
        return
    end
    self.requestTime = Time.realtimeSinceStartup
    if CFreightMgr.Inst.requestHelpCoolDownDic[self.index] <= 0 then
        CFreightMgr.Inst.requestHelpCoolDownDic[self.index] = CFreightMgr.Inst.requestHelpCDTime
        CFreightMgr.Inst:StartCountDown()
        self:RequestHelpCdUpdate(self.index)
        --请求帮助
        if CFreightMgr.Inst.cargoList[self.index].status == EnumGuildFreightStatus.eNotFilled then
            CFreightMgr.Inst:UpdateFreightBoxStatus(CClientMainPlayer.Inst.Id, self.index + 1, EnumGuildFreightStatus_lua.eNotFilledNeedHelp, "")
        else
            Gac2Gas.RequestBroadcastFreightHelp(self.index + 1)
        end
    else
        g_MessageMgr:ShowMessage("SeekHelp_Cooling")
    end
end
function CLuaFreightCargoInfo:FillCargoSuccess( args) 
    local index=args[0]
    if self.index == index then
        self:Init(index)
    end
end
function CLuaFreightCargoInfo:RequestHelpSuccess( args) 
    local index=args[0]
    if self.index == index then
        Gac2Gas.RequestBroadcastFreightHelp(index + 1)
        self:Init(index)
    end
end
-- function CLuaFreightCargoInfo:GetActionPairs( itemId, templateId) 
--     local putinTrade = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("获取途径"), DelegateFactory.Action(function () 
--         CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, true, self.transform, AlignType1.Right)
--     end))
--     local actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
--     CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), putinTrade)
--     return actionPairs
-- end

