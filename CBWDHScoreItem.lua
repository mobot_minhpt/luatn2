-- Auto Generated!!
local CBiWuDaHuiMgr = import "L10.Game.CBiWuDaHuiMgr"
local CBWDHScoreItem = import "L10.UI.CBWDHScoreItem"
CBWDHScoreItem.m_Init_CS2LuaHook = function(this, group, score, row, shopid)
    if row % 2 == 0 then
        this:SetBackgroundTexture(g_sprites.EvenBgSprite)
    else
        this:SetBackgroundTexture(g_sprites.OddBgSpirite)
    end

    this.group = group
    this.nameLabel.text = CBiWuDaHuiMgr.Inst:GetStageFullDesc(group)
    this.valueLabel.text = tostring(score)

    if this.Btn then
        UIEventListener.Get(this.Btn).onClick =
            DelegateFactory.VoidDelegate(
            function(go)
                CUIManager.CloseUI(CUIResources.BWDHScoreWnd)
                CLuaNPCShopInfoMgr.ShowScoreShopById(shopid)
            end
        )
    end
end
