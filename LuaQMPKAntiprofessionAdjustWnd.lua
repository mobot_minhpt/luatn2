local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local QnTableView = import "L10.UI.QnTableView"
local QnButton = import "L10.UI.QnButton"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local EnumClass = import "L10.Game.EnumClass"
local Profession = import "L10.Game.Profession"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"

LuaQMPKAntiprofessionAdjustWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQMPKAntiprofessionAdjustWnd, "LeftLvLab", "LeftLvLab", UILabel)
RegistChildComponent(LuaQMPKAntiprofessionAdjustWnd, "Table", "Table", QnTableView)
RegistChildComponent(LuaQMPKAntiprofessionAdjustWnd, "ClearBtn", "ClearBtn", QnButton)
RegistChildComponent(LuaQMPKAntiprofessionAdjustWnd, "ResetBtn", "ResetBtn", QnButton)
RegistChildComponent(LuaQMPKAntiprofessionAdjustWnd, "ConfirmBtn", "ConfirmBtn", QnButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaQMPKAntiprofessionAdjustWnd, "m_AntiInfo")
RegistClassMember(LuaQMPKAntiprofessionAdjustWnd, "m_LvMax")
RegistClassMember(LuaQMPKAntiprofessionAdjustWnd, "m_TotallMaxLv")
RegistClassMember(LuaQMPKAntiprofessionAdjustWnd, "m_DecreaseTick")
RegistClassMember(LuaQMPKAntiprofessionAdjustWnd, "m_IncreaseTick")
RegistClassMember(LuaQMPKAntiprofessionAdjustWnd, "m_CurItem")
RegistClassMember(LuaQMPKAntiprofessionAdjustWnd, "m_CurInfo")
RegistClassMember(LuaQMPKAntiprofessionAdjustWnd, "m_InputLab")
RegistClassMember(LuaQMPKAntiprofessionAdjustWnd, "m_IncreaseButton")
RegistClassMember(LuaQMPKAntiprofessionAdjustWnd, "m_DecreaseButton")
RegistClassMember(LuaQMPKAntiprofessionAdjustWnd, "m_InputNum")
RegistClassMember(LuaQMPKAntiprofessionAdjustWnd, "m_InputNumMax")
RegistClassMember(LuaQMPKAntiprofessionAdjustWnd, "m_LeftAdjustNum")

function LuaQMPKAntiprofessionAdjustWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self.ClearBtn.OnClick = DelegateFactory.Action_QnButton(function (go)
        self:OnClearBtnClick()
    end)

    self.ResetBtn.OnClick = DelegateFactory.Action_QnButton(function (go)
        self:OnResetBtnClick()
    end)

    self.ConfirmBtn.OnClick = DelegateFactory.Action_QnButton(function (go)
        self:OnConfirmBtnClick()
    end)

    self.Table.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_AntiInfo 
        end,
        function(item, index)
            self:InitItem(item, index + 1, self.m_AntiInfo[index + 1])
        end
    )

    self.m_AntiInfo = {}
    for i = 1, (EnumToInt(EnumClass.Undefined) - 2) do
        local info = CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill, i) and CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill[i] or nil
        if info then
            self.m_AntiInfo[i] = {CurLevel = info.CurLevel, Profession = info.Profession}
        end
    end

    self.m_LvMax = QuanMinPK_Setting.GetData().MaxAntiProfessionLevel
    self.m_TotallMaxLv = QuanMinPK_Setting.GetData().TotalAntiProfessionPointNum
end

function LuaQMPKAntiprofessionAdjustWnd:Init()
    self:Refresh()
    self.ResetBtn.Enabled = false
    self.ConfirmBtn.Enabled = false
end

--@region UIEvent

--@endregion UIEvent

function LuaQMPKAntiprofessionAdjustWnd:InitItem(item, index, info)
    local NameLab   = item.transform:Find("NameLab"):GetComponent(typeof(UILabel))
    local SeqLab    = item.transform:Find("AntiIcon/SeqLab"):GetComponent(typeof(UILabel))
    local Icon      = item.transform:Find("AntiIcon/Icon"):GetComponent(typeof(CUITexture))
    local InputLab  = item.transform:Find("IncAndDec/InputLab"):GetComponent(typeof(UILabel))
    local DecBtn    = item.transform:Find("IncAndDec/DecreaseButton"):GetComponent(typeof(QnButton))
    local IncBtn    = item.transform:Find("IncAndDec/IncreaseButton"):GetComponent(typeof(QnButton))

    UIEventListener.Get(DecBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:PrepareAdjustAntiLv(item, InputLab, IncBtn, DecBtn, info)
	    self:OnDecreaseButtonClick()
	end)

	UIEventListener.Get(IncBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:PrepareAdjustAntiLv(item, InputLab, IncBtn, DecBtn, info)
	    self:OnIncreaseButtonClick()
	end)

	UIEventListener.Get(InputLab.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:PrepareAdjustAntiLv(item, InputLab, IncBtn, DecBtn, info)
	    self:OnInputLabClick()
	end)

    UIEventListener.Get(IncBtn.gameObject).onPress = DelegateFactory.BoolDelegate(function (go, state)
        self:PrepareAdjustAntiLv(item, InputLab, IncBtn, DecBtn, info)
        self:OnIncreaseButtonPress(go,state)
    end)

	UIEventListener.Get(DecBtn.gameObject).onPress = DelegateFactory.BoolDelegate(function (go, state)
        self:PrepareAdjustAntiLv(item, InputLab, IncBtn, DecBtn, info)
        self:OnDecreaseButtonPress(go,state)
    end)

    local profession = info.Profession
    local proClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), profession)
    local proName = Profession.GetName(proClass)
    NameLab.text = LocalString.GetString("克") .. proName
    SeqLab.text = index
    Icon:LoadMaterial(Profession.GetLargeIcon(proClass))

    self:PrepareAdjustAntiLv(item, InputLab, IncBtn, DecBtn, info)
    self:SetInputNum(self.m_InputNum, true)
end

function LuaQMPKAntiprofessionAdjustWnd:RefreshAllIncBtn()
    local count = 0
    for i = 1, #self.m_AntiInfo do
        count = count + self.m_AntiInfo[i].CurLevel
    end

    local list = self.Table.m_ItemList
    for i = 0, list.Count - 1 do 
        local item = list[i]
        local info = self.m_AntiInfo[i+1]
        local incBtn = item.transform:Find("IncAndDec/IncreaseButton"):GetComponent(typeof(QnButton))
        local inputNumMax = math.min(self.m_TotallMaxLv - count + info.CurLevel, self.m_LvMax)
        incBtn.Enabled = inputNumMax > info.CurLevel
    end
end

function LuaQMPKAntiprofessionAdjustWnd:RefreshItemLvAndDesc(item, info)
    local DescLab   = item.transform:Find("EffectLab"):GetComponent(typeof(UILabel))
    local InputLab  = item.transform:Find("IncAndDec/InputLab"):GetComponent(typeof(UILabel))
    
    local profession = info.Profession
    local proClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), profession)
    local curLevel = info.CurLevel
    InputLab.text = "Lv." .. curLevel
    local oriData = SoulCore_AntiProfessionLevel.GetData(info.CurLevel)
    local oriMul =  oriData and oriData.AntiProfessionMul or 0
    local effectStr = SafeStringFormat3("+%.1f%%", oriMul * 100)
    DescLab.text = SafeStringFormat3(LocalString.GetString("对%s伤害 %s"), Profession.GetFullName(proClass) , effectStr)
end

function LuaQMPKAntiprofessionAdjustWnd:PrepareAdjustAntiLv(item, InputLab, IncBtn, DecBtn, info)
    self.m_CurItem = item
    self.m_InputLab = InputLab
    self.m_IncreaseButton = IncBtn
    self.m_DecreaseButton = DecBtn
    self.m_CurInfo = info
    self.m_InputNum = info.CurLevel

    local count = 0
    for i = 1, #self.m_AntiInfo do
        count = count + self.m_AntiInfo[i].CurLevel
    end

    self.m_InputNumMax = math.min(self.m_TotallMaxLv - count + info.CurLevel, self.m_LvMax)
end

function LuaQMPKAntiprofessionAdjustWnd:OnDecreaseButtonClick()
    self:SetInputNum(self.m_InputNum - 1)
end

function LuaQMPKAntiprofessionAdjustWnd:OnIncreaseButtonClick()
    self:SetInputNum(self.m_InputNum + 1)
end

function LuaQMPKAntiprofessionAdjustWnd:OnDecreaseButtonPress(go,isPressed)
	if isPressed then
		if self.m_DecreaseTick == nil then
			self.m_DecreaseTick = RegisterTick(function()
				self:SetInputNum(self.m_InputNum - 1)
			end, 100)
		end
	else
		if self.m_DecreaseTick ~= nil then
			UnRegisterTick(self.m_DecreaseTick)
			self.m_DecreaseTick = nil
		end
	end
end

function LuaQMPKAntiprofessionAdjustWnd:OnIncreaseButtonPress(go,isPressed)
	if isPressed then
		if self.m_IncreaseTick == nil then
			self.m_IncreaseTick = RegisterTick(function()
				self:SetInputNum(self.m_InputNum + 1)
			end, 100)
		end
	else
		if self.m_IncreaseTick ~= nil then
			UnRegisterTick(self.m_IncreaseTick)
			self.m_IncreaseTick = nil
		end
	end
end

function LuaQMPKAntiprofessionAdjustWnd:OnInputLabClick()
    CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(0, self.m_InputNumMax, self.m_InputNum, 100, DelegateFactory.Action_int(function (val) 
		self.m_InputLab.text = "Lv." .. val
    end), DelegateFactory.Action_int(function (val) 
        self:SetInputNum(val)
    end), self.m_InputLab, AlignType.Right, true)
end

function LuaQMPKAntiprofessionAdjustWnd:SetInputNum(num, noRefreshState)
	if num < 1 then
		num = 1
	end

    if num > self.m_InputNumMax then
		num = self.m_InputNumMax
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("没有剩余可供分配的点数！"))
	end

	self.m_InputNum = num
    self.m_CurInfo.CurLevel = self.m_InputNum

	self.m_IncreaseButton.Enabled = self.m_InputNumMax - num > 0
	self.m_DecreaseButton.Enabled = num > 1
	if self.m_InputNumMax - num <= 0 then
		if self.m_IncreaseTick ~= nil then
			UnRegisterTick(self.m_IncreaseTick)
			self.m_IncreaseTick = nil
		end
	end

	if num <= 1 then
		if self.m_DecreaseTick ~= nil then
			UnRegisterTick(self.m_DecreaseTick)
			self.m_DecreaseTick = nil
		end
	end

    if not noRefreshState then
        self:RefreshLeftLv()
    end
    self:RefreshItemLvAndDesc(self.m_CurItem, self.m_CurInfo)
    self:RefreshAllIncBtn()
end

function LuaQMPKAntiprofessionAdjustWnd:OnClearBtnClick()
    for i = 1, #self.m_AntiInfo do
        self.m_AntiInfo[i].CurLevel = 1
    end

    self:Refresh()
end

function LuaQMPKAntiprofessionAdjustWnd:OnResetBtnClick()
    self.m_AntiInfo = {}
    for i = 1, (EnumToInt(EnumClass.Undefined) - 2) do
        local info = CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill, i) and CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill[i] or nil
        if info then
            self.m_AntiInfo[i] = {CurLevel = info.CurLevel, MaxLevel = info.MaxLevel, Profession = info.Profession}
        end
    end

    self:Refresh()
    self.ResetBtn.Enabled = false
    self.ConfirmBtn.Enabled = false
end

function LuaQMPKAntiprofessionAdjustWnd:OnConfirmBtnClick()
    local requestTab = {}
    for i = 1, #self.m_AntiInfo do
        local info = self.m_AntiInfo[i]
        table.insert(requestTab, info.CurLevel)
    end
    local requestStr = table.concat(requestTab, ",")
    Gac2Gas.RequestSetQmpkAntiProfessionData(requestStr)

    CUIManager.CloseUI(CLuaUIResources.QMPKAntiprofessionAdjustWnd)
end

function LuaQMPKAntiprofessionAdjustWnd:Refresh()
    self.Table:ReloadData(true, false)
    self:RefreshLeftLv()
end

function LuaQMPKAntiprofessionAdjustWnd:RefreshLeftLv()
    local count = 0
    for i = 1, #self.m_AntiInfo do
        count = count + self.m_AntiInfo[i].CurLevel
    end
    self.m_LeftAdjustNum = self.m_TotallMaxLv - count
    self.LeftLvLab.text = self.m_LeftAdjustNum
    self.ResetBtn.Enabled = true
    self.ConfirmBtn.Enabled = true
end

function LuaQMPKAntiprofessionAdjustWnd:OnDestroy()
    if self.m_IncreaseTick ~= nil then
        UnRegisterTick(self.m_IncreaseTick)
        self.m_IncreaseTick = nil
    end

    if self.m_DecreaseTick ~= nil then
        UnRegisterTick(self.m_DecreaseTick)
        self.m_DecreaseTick = nil
    end
end