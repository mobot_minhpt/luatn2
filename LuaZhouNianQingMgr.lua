require("common/common_include")

local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local CCommonPlayerDisplayData = import "L10.Game.CCommonPlayerDisplayData"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local EnumCommonPlayerListUpdateType = import "L10.Game.EnumCommonPlayerListUpdateType"

CLuaZhouNianQingMgr={}
CLuaZhouNianQingMgr.m_ShopInfo=nil
CLuaZhouNianQingMgr.m_ShopRemainTime=0

function CLuaZhouNianQingMgr.GetPackagePintuPos(itemTemplateId)
	local itemInfos = CItemMgr.Inst:GetPlayerItemsAtPlaceByTemplateId(EnumItemPlace.Bag, itemTemplateId)
	if itemInfos.Count > 0 then
		return itemInfos[0].pos
	else
		return 0
	end
end

-- 好友召回处理
function Gas2Gac.QueryHuiLiuDanInvitePlayersResult(resultTbl_U)
	local list = MsgPackImpl.unpack(resultTbl_U)
	if not list then return end

	if not CClientMainPlayer.Inst then return end

	CCommonPlayerListMgr.Inst.WndTitle = LocalString.GetString("选择召回对象")
	CCommonPlayerListMgr.Inst.ButtonText = LocalString.GetString("召回")
	CCommonPlayerListMgr.Inst.UpdateType = EnumCommonPlayerListUpdateType.Default
	CommonDefs.ListClear(CCommonPlayerListMgr.Inst.allData)

	for i = 0, list.Count - 1, 5 do
		local id = list[i]
		local name = list[i + 1]
		local class = list[i + 2]
		local gender = list[i + 3]
		local grade = list[i + 4]

		local data = CreateFromClass(CCommonPlayerDisplayData, id, name, grade, class, gender, -1, true)
		CommonDefs.ListAdd(CCommonPlayerListMgr.Inst.allData, typeof(CCommonPlayerDisplayData), data)
	end
	
	local callback = function (playerId)
		local itemId = LuaGetGlobal("ZNQHuiLiu_ItemId")
		local pos = LuaGetGlobal("ZNQHuiLiu_Pos")
		local place = LuaGetGlobal("ZNQHuiLiu_Place")
		Gac2Gas.RequestInvitePlayerWithHuiLiuDan(playerId, place, pos, itemId)
	end

	CCommonPlayerListMgr.Inst.OnPlayerSelected = DelegateFactory.Action_ulong(callback)
	CUIManager.ShowUI(CIndirectUIResources.CommonPlayerListWnd)
end

function Gas2Gac.InvitePlayerWithHuiLiuDanSuccess()
	CUIManager.CloseUI(CIndirectUIResources.CommonPlayerListWnd)
end

function Gas2Gac.ZhouNianShopQueryPackageResult(data, remainSeconds)
	local list = MsgPackImpl.unpack(data)
	if not list then return end

	CLuaZhouNianQingMgr.m_ShopInfo = {}
	CLuaZhouNianQingMgr.m_ShopRemainTime = remainSeconds
	for i = 0, list.Count - 1 do
		table.insert(CLuaZhouNianQingMgr.m_ShopInfo, {
			packageId = list[i][0],
			remainBuyTimes = list[i][1],
		})
	end
	table.sort(CLuaZhouNianQingMgr.m_ShopInfo, function (a, b)
		local data1 = ZhouNianQing_LimitedShop.GetData(a.packageId)
		local data2 = ZhouNianQing_LimitedShop.GetData(b.packageId)
		return data1.PosType < data2.PosType
	end)
	g_ScriptEvent:BroadcastInLua("ZhouNianShopQueryPackageResult")
end
