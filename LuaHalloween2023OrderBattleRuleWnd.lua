local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CUITexture = import "L10.UI.CUITexture"
local Animation = import "UnityEngine.Animation"
local CButton = import "L10.UI.CButton"

LuaHalloween2023OrderBattleRuleWnd = class()

RegistClassMember(LuaHalloween2023OrderBattleRuleWnd, "m_TypeLabel")
RegistClassMember(LuaHalloween2023OrderBattleRuleWnd, "m_Portrait")
RegistClassMember(LuaHalloween2023OrderBattleRuleWnd, "m_NameLabel")
RegistClassMember(LuaHalloween2023OrderBattleRuleWnd, "m_SkillTable")
RegistClassMember(LuaHalloween2023OrderBattleRuleWnd, "m_SkillTemplate")
RegistClassMember(LuaHalloween2023OrderBattleRuleWnd, "m_SkillTemplate")
RegistClassMember(LuaHalloween2023OrderBattleRuleWnd, "m_SkillTemplate")
--坐骑信息图标
RegistClassMember(LuaHalloween2023OrderBattleRuleWnd, "m_Player1Tip")
RegistClassMember(LuaHalloween2023OrderBattleRuleWnd, "m_Player2Tip")
RegistClassMember(LuaHalloween2023OrderBattleRuleWnd, "m_CarForceInfo")
RegistClassMember(LuaHalloween2023OrderBattleRuleWnd, "m_CarDefaultInfo")

RegistClassMember(LuaHalloween2023OrderBattleRuleWnd, "m_Animation")
RegistClassMember(LuaHalloween2023OrderBattleRuleWnd, "m_ShowRoleId")

function LuaHalloween2023OrderBattleRuleWnd:Awake()
    self.m_Animation = self.transform:GetComponent(typeof(Animation))
    self.m_TypeLabel = self.transform:Find("Backgroud/SkillView/Title/TypeLabel"):GetComponent(typeof(UILabel))
    self.m_Portrait = self.transform:Find("Backgroud/SkillView/Title/Portrait"):GetComponent(typeof(CUITexture))
    self.m_NameLabel = self.transform:Find("Backgroud/SkillView/Title/NameLabel"):GetComponent(typeof(UILabel))
    self.m_SkillTable = self.transform:Find("Backgroud/SkillView/SkillTable"):GetComponent(typeof(UITable))
    self.m_SkillTemplate = self.transform:Find("Backgroud/SkillView/SkillTemplate").gameObject

    self.m_Player1Tip = self.transform:Find("Car/Player1").gameObject
    self.m_Player2Tip = self.transform:Find("Car/Player2").gameObject
    self.m_CarForceInfo = self.transform:Find("Car/Force").gameObject
    self.m_CarDefaultInfo = self.transform:Find("Car/Default").gameObject

    self.m_ChangeButton = self.transform:Find("Backgroud/ChangeButton"):GetComponent(typeof(CButton))
    self.m_RuleButton = self.transform:Find("Backgroud/RuleButton").gameObject
    UIEventListener.Get(self.m_ChangeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:OnChangeBtnClick()
    end)
    UIEventListener.Get(self.m_RuleButton).onClick = DelegateFactory.VoidDelegate(function()
        self:OnRuleBtnClick()
    end)
end

function LuaHalloween2023OrderBattleRuleWnd:Init()
    if LuaHalloween2023Mgr.m_ShowOrderBattleRuleWndType == "GameBegin" then
        self.m_Animation:Play("halloween2023orderbattlerulewnd_readyshow")
    end
    self.m_SkillTemplate:SetActive(false)
    if CClientMainPlayer.Inst and LuaHalloween2023Mgr.m_MyTeamRoleInfo then
        self.m_ShowRoleId = CClientMainPlayer.Inst.Id == LuaHalloween2023Mgr.m_MyTeamRoleInfo.driverPlayerId and 1 or 2
    else
        self.m_ShowRoleId = 1
    end
    self:OnShow()
end

function LuaHalloween2023OrderBattleRuleWnd:OnShow()
    self:InitCarIcon()
    self:InitTitle()
    self:InitSkills()
end

function LuaHalloween2023OrderBattleRuleWnd:InitCarIcon()
    if LuaHalloween2023Mgr.m_MyTeamRoleInfo and LuaHalloween2023Mgr.m_MyTeamRoleInfo.zuoQiId then
        self.m_CarForceInfo:SetActive(true)
        self.m_CarDefaultInfo:SetActive(false)
        if LuaHalloween2023Mgr.m_CandyDeliveryGameSetData == nil then LuaHalloween2023Mgr:InitCandyDeliveryGameSetData() end
        local zuoQiData = LuaHalloween2023Mgr.m_CandyDeliveryGameSetData.candyForceSet[LuaHalloween2023Mgr.m_MyTeamRoleInfo.zuoQiId]
        self.m_CarForceInfo.transform:Find("ForceBg"):GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24(zuoQiData.Color,0)
        self.m_CarForceInfo.transform:GetComponent(typeof(UILabel)).text = zuoQiData.Name
        self.m_CarForceInfo.transform:Find("ForceIcon"):GetComponent(typeof(CUITexture)):LoadMaterial(zuoQiData.ZuiQiTexture)
    else
        self.m_CarForceInfo:SetActive(false)
        self.m_CarDefaultInfo:SetActive(true)
    end
end

function LuaHalloween2023OrderBattleRuleWnd:InitTitle()
    self.m_Player1Tip:SetActive(self.m_ShowRoleId == 1)
    self.m_Player2Tip:SetActive(self.m_ShowRoleId == 2)
    self.m_TypeLabel.text = self.m_ShowRoleId == 1 and LocalString.GetString("骑手技能") or LocalString.GetString("抢单技能")
    self.m_ChangeButton.Text = self.m_ShowRoleId == 1 and LocalString.GetString("查看抢单技能") or LocalString.GetString("查看骑手技能")
    local playerId = 0
    if LuaHalloween2023Mgr.m_MyTeamRoleInfo then
        playerId = self.m_ShowRoleId == 1 and LuaHalloween2023Mgr.m_MyTeamRoleInfo.driverPlayerId or LuaHalloween2023Mgr.m_MyTeamRoleInfo.passengerPlayerId
    end
    local member = CTeamMgr.Inst:GetMemberById(playerId or 0)
    if member then
        self.m_Portrait.gameObject:SetActive(true)
        self.m_NameLabel.gameObject:SetActive(true)
        self.m_NameLabel.text = member.m_MemberName
        self.m_NameLabel.color = self.m_ShowRoleId == 1 and NGUIText.ParseColor24("3579e3",0) or NGUIText.ParseColor24("8d4909",0)
        self.m_NameLabel.transform:Find("Label"):GetComponent(typeof(UILabel)):ResetAndUpdateAnchors()
        self.m_Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(member.m_MemberClass, member.m_MemberGender, member.m_Expression))
        local bgColor = self.m_ShowRoleId == 1 and NGUIText.ParseColor24("6ab3f1",0) or NGUIText.ParseColor24("f1b06a",0)
        self.m_Portrait.transform:Find("Bg"):GetComponent(typeof(UITexture)).color = bgColor
    else
        self.m_Portrait.gameObject:SetActive(false)
        self.m_NameLabel.gameObject:SetActive(false)
    end
end

function LuaHalloween2023OrderBattleRuleWnd:InitSkills()
    local skillList = nil
    if self.m_ShowRoleId == 1 then
        skillList = Halloween2023_Setting.GetData().CandyDeliveryDriverSkills
    elseif self.m_ShowRoleId == 2 then
        skillList = Halloween2023_Setting.GetData().CandyDeliveryPassengerSkills
    end
    if skillList == nil then return end
    
	Extensions.RemoveAllChildren(self.m_SkillTable.transform)
    for i = 0, skillList.Length - 1 do
		local item = NGUITools.AddChild(self.m_SkillTable.gameObject, self.m_SkillTemplate)
        item.gameObject:SetActive(true)
        self:InitOneSkill(item, skillList[i])
    end
    self.m_SkillTable.padding = self.m_ShowRoleId == 1 and Vector2(0, 18) or Vector2(0, 0)
    local pos = self.m_SkillTable.transform.localPosition
    pos.y = self.m_ShowRoleId == 1 and -8 or -31
    self.m_SkillTable.transform.localPosition = pos
    self.m_SkillTable:Reposition()
end

function LuaHalloween2023OrderBattleRuleWnd:InitOneSkill(item, skillId)
    local skillData = Skill_AllSkills.GetData(skillId)
    local skillIcon = item.transform:Find("SkillIcon"):GetComponent(typeof(CUITexture))
    skillIcon:LoadSkillIcon(skillData.SkillIcon)
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text = skillData.Name
    nameLabel.color = self.m_ShowRoleId == 1 and NGUIText.ParseColor24("3077e2",0) or NGUIText.ParseColor24("e28b2e",0)
    local descLabel = item.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
    descLabel.text = skillData.Display
    descLabel.color = self.m_ShowRoleId == 1 and NGUIText.ParseColor24("3a47ad",0) or NGUIText.ParseColor24("a06424",0)
    local otherColor = self.m_ShowRoleId == 1 and NGUIText.ParseColor24("4D5CD0",0) or NGUIText.ParseColor24("a06424",0)
    skillIcon.transform:Find("Line"):GetComponent(typeof(UITexture)).color = otherColor
    skillIcon.transform:Find("ColoredFrame"):GetComponent(typeof(UITexture)).color = otherColor
end

--@region UIEvent
function LuaHalloween2023OrderBattleRuleWnd:OnChangeBtnClick()
    self.m_ShowRoleId = self.m_ShowRoleId == 1 and 2 or 1
    self:OnShow()
end

function LuaHalloween2023OrderBattleRuleWnd:OnRuleBtnClick()
    g_MessageMgr:ShowMessage("2023Halloween_Takeout_rules")
end
--@endregion UIEvent

