local Item_Type = import "L10.Game.Item_Type"

local CItemMgr = import "L10.Game.CItemMgr"
local CItem = import "L10.Game.CItem"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChatLinkMgr = import "CChatLinkMgr"

local QnTableView=import "L10.UI.QnTableView"
local CUITexture=import "L10.UI.CUITexture"
local CButton=import "L10.UI.CButton"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"


CLuaCityWarZiCaiShopWnd = class()
RegistClassMember(CLuaCityWarZiCaiShopWnd,"goodsTable")
RegistClassMember(CLuaCityWarZiCaiShopWnd,"nameLabel")
RegistClassMember(CLuaCityWarZiCaiShopWnd,"levelLabel")
RegistClassMember(CLuaCityWarZiCaiShopWnd,"typeLabel")
RegistClassMember(CLuaCityWarZiCaiShopWnd,"descLabel")
RegistClassMember(CLuaCityWarZiCaiShopWnd,"descScrollView")
RegistClassMember(CLuaCityWarZiCaiShopWnd,"icon")
RegistClassMember(CLuaCityWarZiCaiShopWnd,"buyBtn")
RegistClassMember(CLuaCityWarZiCaiShopWnd,"selectedItemIndex")

RegistClassMember(CLuaCityWarZiCaiShopWnd,"m_ZiCaiShopItemTbl")
RegistClassMember(CLuaCityWarZiCaiShopWnd,"m_PreItemsStr")

RegistClassMember(CLuaCityWarZiCaiShopWnd,"m_CostLabel")
RegistClassMember(CLuaCityWarZiCaiShopWnd,"m_ZiCaiLabel")


function CLuaCityWarZiCaiShopWnd:Awake()
    self.m_ZiCaiShopItemTbl={}
    self.m_PreItemsStr=nil
    self.goodsTable = self.transform:Find("GoodsGrid"):GetComponent(typeof(QnTableView))
    self.nameLabel = self.transform:Find("Detail/Desc/NameLabel"):GetComponent(typeof(UILabel))
    self.levelLabel = self.transform:Find("Detail/Desc/LevelLabel"):GetComponent(typeof(UILabel))
    self.typeLabel = self.transform:Find("Detail/Desc/Type/TypeLabel"):GetComponent(typeof(UILabel))
    self.descLabel = self.transform:Find("Detail/Desc/DescRegion/DescScrollView/DescLabel"):GetComponent(typeof(UILabel))
    self.descScrollView = self.transform:Find("Detail/Desc/DescRegion/DescScrollView"):GetComponent(typeof(UIScrollView))
    self.icon = self.transform:Find("Detail/Desc/Icon (1)"):GetComponent(typeof(CUITexture))
    self.buyBtn = self.transform:Find("Detail/BuyBtn"):GetComponent(typeof(CButton))
    self.selectedItemIndex = 0

    self.m_CostLabel = self.transform:Find("Detail/Desc/GameObject/QnCostAndOwnMoney/Cost"):GetComponent(typeof(UILabel))
    self.m_ZiCaiLabel = self.transform:Find("Detail/Desc/GameObject/QnCostAndOwnMoney/Own"):GetComponent(typeof(UILabel))
    self.m_CostLabel.text="0"
    self.m_ZiCaiLabel.text="0"


    UIEventListener.Get(self.buyBtn.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.BuyCityWarZiCaiShopItem(self.m_ZiCaiShopItemTbl[self.selectedItemIndex+1].templateId)
    end)

end

function CLuaCityWarZiCaiShopWnd:Init( )
    self.goodsTable.m_DataSource=DefaultTableViewDataSource.Create(
        function()
            return #self.m_ZiCaiShopItemTbl
        end,function(item,index)
            self:InitItem(item.transform,index,self.m_ZiCaiShopItemTbl[index+1])
        end)

    self.goodsTable.OnSelectAtRow=DelegateFactory.Action_int(function(row)
        self.selectedItemIndex = row
        self:UpdateDescription(row)
    end)

    self.goodsTable:ReloadData(false, false)
    self.goodsTable:SetSelectRow(0, true)
    Gac2Gas.OpenCityWarZiCaiShop()
end

function CLuaCityWarZiCaiShopWnd:UpdateDescription( row )
    local name = ""
    local level = 0
    local desc = ""
    local itemType = ""
    local price = ""

    local info = self.m_ZiCaiShopItemTbl[row+1]
    self.m_CostLabel.text=tostring(info.price)
    local templateId = info.templateId
        local item = CItemMgr.Inst:GetItemTemplate(templateId)
        name = item.Name
        level = CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(item)
        desc = CItem.GetItemDescription(item.ID,true)
        self.icon:LoadMaterial(item.Icon)
        local data = Item_Type.GetData(item.Type)
        if data ~= nil then
            itemType = data.Name
        end


    self.nameLabel.text = name
    local default
    if level > 0 then
        default = "Lv." .. tostring(level)
    else
        default = nil
    end
    local levelStr = default
    if CClientMainPlayer.Inst ~= nil then
        if CClientMainPlayer.Inst.Level < level then
            levelStr = System.String.Format("[c][ff0000]{0}[-][/c]", levelStr)
        end
    end
    self.levelLabel.text = levelStr
    self.typeLabel.text = itemType
    self.descLabel.text = CChatLinkMgr.TranslateToNGUIText(desc, false)
    self.descScrollView:ResetPosition()
end

function CLuaCityWarZiCaiShopWnd:InitItem(transform,row,info)
    local templateId = info.templateId

    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local priceLabel = transform:Find("PriceLabel"):GetComponent(typeof(UILabel))
    -- local moneySprite = transform:Find("MoneySprite"):GetComponent(typeof(UISprite))
    local disableSprite = transform:Find("Icon/DisableSprite"):GetComponent(typeof(UISprite))
    local levelLabel = transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local numLimitLabel = transform:Find("NumLimitLabel"):GetComponent(typeof(UILabel))

    nameLabel.text = nil
    levelLabel.text = nil
    icon:Clear()
    -- moneySprite.spriteName = iconName

    disableSprite.enabled = false
    local gradeRequire = 0
        local item = CItemMgr.Inst:GetItemTemplate(templateId)
        if item ~= nil then
            nameLabel.text = item.Name
            gradeRequire = CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(item)
            icon:LoadMaterial(item.Icon)
            disableSprite.enabled = not CItem.GetMainPlayerIsFit(templateId, true)
        end

    local default
    if gradeRequire > 0 then
        default = "Lv." .. tostring(gradeRequire)
    else
        default = nil
    end
    local gradeStr = default
    --等级不够 标红
    if gradeStr then
        if CClientMainPlayer.Inst ~= nil then
            if CClientMainPlayer.Inst.Level < gradeRequire then
                gradeStr = SafeStringFormat3("[c][ff0000]%s[-][/c]", gradeStr)
            end
        end
    end
    levelLabel.text = gradeStr

    priceLabel.text = tostring(info.price)
end


function CLuaCityWarZiCaiShopWnd:OnEnable()
	g_ScriptEvent:AddListener("SendCityWarZiCaiShopInfo", self, "OnSendCityWarZiCaiShopInfo")
    
end
function CLuaCityWarZiCaiShopWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendCityWarZiCaiShopInfo", self, "OnSendCityWarZiCaiShopInfo")
end

function CLuaCityWarZiCaiShopWnd:OnSendCityWarZiCaiShopInfo(zicai,items)
    self.m_ZiCaiLabel.text=tostring(zicai)
    if self.m_PreItemsStr~=items then
        self.m_PreItemsStr=items
        self.m_ZiCaiShopItemTbl = {}
        for itemId, price, bind in string.gmatch(items, "(%d+),(%d+),(%d+);") do
            itemId = tonumber(itemId)
            price = tonumber(price)
            table.insert( self.m_ZiCaiShopItemTbl, {
                templateId = itemId,
                price = price,
            } )
        end
        self.goodsTable:ReloadData(false, false)
        self.goodsTable:SetSelectRow(0, true)
    end
end
