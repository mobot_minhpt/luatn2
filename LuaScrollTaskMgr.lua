local CScreenCaptureWnd = import "L10.UI.CScreenCaptureWnd"
local Screen = import "UnityEngine.Screen"
local GLTextManager = import "GLTextManager"
local RenderTexture = import "UnityEngine.RenderTexture"
local Rect = import "UnityEngine.Rect"
local CMainCamera = import "L10.Engine.CMainCamera"
local CScene = import "L10.Game.CScene"
local TextureFormat = import "UnityEngine.TextureFormat"
local CameraClearFlags = import "UnityEngine.CameraClearFlags"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local Texture2D = import "UnityEngine.Texture2D"
local NGUIText = import "NGUIText"
local CSharpResourceLoader = import "L10.Game.CSharpResourceLoader"
local CUITexture = import "L10.UI.CUITexture"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local TextureWrapMode = import "UnityEngine.TextureWrapMode"
local FilterMode = import "UnityEngine.FilterMode"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local Application = import "UnityEngine.Application"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local File = import "System.IO.File"
local CZhuJueJuQingMgr = import "L10.Game.CZhuJueJuQingMgr"
local CHeadInfoWnd = import "L10.UI.CHeadInfoWnd"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local Constants = import "L10.Game.Constants"

LuaScrollTaskMgr = class()

------------------------------------------
-- listener
------------------------------------------
------------------------------------------
-- listener end
------------------------------------------


------------------------------------------
-- RPC
------------------------------------------
LuaScrollTaskMgr.nextLoadingPicToScroll = false
LuaScrollTaskMgr.startFromScene = false
LuaScrollTaskMgr.waitForSubtitleId = nil

function LuaScrollTaskMgr:OpenScrollTaskWnd(openType, subtitleId, pictureKey, layout)
    if openType == 2 then
        layout = layout or 1
        self:ShowScrollWithStartFromScene(layout, subtitleId, pictureKey)
        return
    end

    CUIManager.ShowUI(CLuaUIResources.ScrollTaskWnd)
end

function LuaScrollTaskMgr:ShowScrollWithStartFromScene(layout, subtitleId, pictureKey)
    self:CaptureAndCacheScreen("SceneScreenshot")
    local info = {op = "ShowScrollWithStartFromScene", formatId = layout or 1, subtitleId = subtitleId, pictureKey = pictureKey or "SceneScreenshot"}
    self:EnqueueScorllInfo(info)
    LuaScrollTaskMgr.startFromScene = true
    CUIManager.ShowUI(CLuaUIResources.ScrollTaskWnd)
end

function LuaScrollTaskMgr:CloseScrollTaskWnd(closeType)
    if closeType == 2 then
        self:CloseWithEndToScene()
        return
    end
    g_ScriptEvent:BroadcastInLua("CloseScrollTask")
end

function LuaScrollTaskMgr:CloseWithEndToScene()
    local info = {op = "CloseWithEndToScene"}
    self:EnqueueScorllInfo(info)
    g_ScriptEvent:BroadcastInLua("ScrollTaskDoNextScroll")
end

function LuaScrollTaskMgr:ShowScrollTaskSubTitle(subtitleId, pictureKey, layout)
    local info = {op = "ShowScroll", formatId = layout, subtitleId = subtitleId, pictureKey = pictureKey}
    self:EnqueueScorllInfo(info)
    g_ScriptEvent:BroadcastInLua("ScrollTaskDoNextScroll")
end

function LuaScrollTaskMgr:HideScrollTaskSubTitle(subtitleId, pictureKey)
    local info = {op = "HideScroll", subtitleId = subtitleId, pictureKey = pictureKey}
    self:EnqueueScorllInfo(info)
    g_ScriptEvent:BroadcastInLua("ScrollTaskDoNextScroll")    
end

function LuaScrollTaskMgr:SetNextLoadingPicToCurrentScroll()
    self.nextLoadingPicToScroll = true
end

function LuaScrollTaskMgr:SetNextLoadingPicToScroll(subtitleId, pictureKey, layout)
    local info = {op = "ShowScroll", formatId = layout, subtitleId = subtitleId, pictureKey = pictureKey}
    self:EnqueueScorllInfo(info)
    CUIManager.ShowUI(CLuaUIResources.ScrollTaskWnd)
    self.nextLoadingPicToScroll = true
end

function LuaScrollTaskMgr:CancelSetNextLoadingPicToScroll()
    CUIManager.CloseUI(CLuaUIResources.ScrollTaskWnd)
    self.nextLoadingPicToScroll = false
end

function LuaScrollTaskMgr:ScrollTaskCaptureScreen(pictureKey)
    self:CaptureAndCacheScreen(pictureKey)
end

function LuaScrollTaskMgr:ScrollTaskChangeLayout(subtitleId, pictureKey, layout)
    local info = {op = "ChangeLayout", formatId = layout, subtitleId = subtitleId, pictureKey = pictureKey}
    self:EnqueueScorllInfo(info)
    g_ScriptEvent:BroadcastInLua("ScrollTaskDoNextScroll")
end

function LuaScrollTaskMgr:WaitForShowScrollTaskSubTitle(subtitleId)
    LuaScrollTaskMgr.waitForSubtitleId = subtitleId
    g_ScriptEvent:BroadcastInLua("WaitForShowScrollTaskSubTitle")
end

function LuaScrollTaskMgr:SwitchToGameView(eventName, taskId, pictureKey, layout)
    local info = {op = "SwitchToGameView", formatId = layout or 1, pictureKey = pictureKey, eventName = eventName, taskId = taskId}
    self:EnqueueScorllInfo(info)
    g_ScriptEvent:BroadcastInLua("ScrollTaskDoNextScroll")
end

function LuaScrollTaskMgr:PlayScrollTaskQTE(qteId, duration)
    -- body
end

function LuaScrollTaskMgr:StartScrollTaskGameEvent(taskId, eventName)
    self:SwitchToGameView(eventName, taskId)
end
------------------------------------------
-- RPC end
------------------------------------------

------------------------------------------
-- 卷轴队列
------------------------------------------
LuaScrollTaskMgr.m_ScorllInfoQueue = {}

function LuaScrollTaskMgr:EnqueueScorllInfo(info)
    table.insert(self.m_ScorllInfoQueue, info)
end

function LuaScrollTaskMgr:DequeueScorllInfo()
    if #self.m_ScorllInfoQueue > 0 then
        return table.remove(self.m_ScorllInfoQueue, 1)
    end

    return nil
end

function LuaScrollTaskMgr:ClearScrollQueue()
    self.m_ScorllInfoQueue = {}
end

function LuaScrollTaskMgr:FindScorllInfoBySubtitleId(subtitleId)
    for i = 1, #LuaScrollTaskMgr.m_ScorllInfoQueue do
        local info = LuaScrollTaskMgr.m_ScorllInfoQueue[i]
        if info.subtitleId == subtitleId then
            return info
        end
    end

    return nil
end
------------------------------------------
-- 卷轴队列 end
------------------------------------------


------------------------------------------
-- 截图相关
------------------------------------------
LuaScrollTaskMgr.m_Screenshots = {}
LuaScrollTaskMgr.m_ScreenShotsByLoad = {}

function LuaScrollTaskMgr:CaptureAndCacheScreen(screenshotKey)
    local action = function ()
        self.m_Screenshots[screenshotKey] = self:CaptureScreen()
	    Gac2Gas.ScrollTaskCaptureScreenDone(screenshotKey)
    end
    local ro = CClientMainPlayer.Inst and CClientMainPlayer.Inst.RO 
    if ro and not ro:IsAllFinished() then
        ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function (renderObject)
            action()
        end))
    else 
        action()
    end
end

function LuaScrollTaskMgr:TryInitGameTexture()
    if self.m_Screenshots["GameScreen"] ~= nil or CMainCamera.CaptureCamera == nil then
        return
    end

    local width = Screen.width
    local height = Screen.height
    local percent = 1
    if CommonDefs.Is_PC_PLATFORM() then
        if CWinSocialWndMgr == nil then
            CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
        end
        if CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
            percent = (1 - Constants.WinSocialWndRatio)
        end
    end
    local rt = CreateFromClass(RenderTexture, width * percent, height, 24 , RenderTextureFormat.ARGB32, RenderTextureReadWrite.Default)
    rt.wrapMode = TextureWrapMode.Clamp
    rt.filterMode = FilterMode.Bilinear
    rt.anisoLevel = 2
    rt.name = "ScrollTask_GameRT"

    if not self.m_OldHeight then
        self.m_OldHeight = CMainCamera.Inst.m_Height
    end
    CMainCamera.Inst.m_Height = CMainCamera.Inst.m_OriScreenHeight
    CMainCamera.CaptureCamera.targetTexture = rt
    CMainCamera.CaptureCamera.rect = CreateFromClass(Rect, 0, 0, 1, 1)
    --CMainCamera.Main.near = 14.5
    self.m_Screenshots["GameScreen"] = rt
end


function LuaScrollTaskMgr:CaptureScreen(isSave)
    CScreenCaptureWnd.IsCapturing = true
    local rect = CreateFromClass(Rect, 0, 0, isSave and 1920 or Screen.width, isSave and 1080 or Screen.height)

    GLTextManager.Visible = false
    local rt = CreateFromClass(RenderTexture, math.floor(rect.width), math.floor(rect.height), 24)
    local screenShot = CreateFromClass(Texture2D, math.floor(rect.width), math.floor(rect.height), TextureFormat.RGB24, false)
    local tmpFlags = CMainCamera.CaptureCamera.clearFlags
    if CScene.MainScene ~= nil and CScene.MainScene.SkyBoxEnabled then
        CMainCamera.CaptureCamera.clearFlags = CameraClearFlags.Skybox
    else
        CMainCamera.CaptureCamera.clearFlags = CameraClearFlags.Color
    end

    CMainCamera.CaptureCamera.targetTexture = rt
    local oldr = CMainCamera.CaptureCamera.rect
    CMainCamera.CaptureCamera.rect = CreateFromClass(Rect, 0, 0, 1, 1)
    CMainCamera.CaptureCamera:Render()
    CMainCamera.CaptureCamera.rect = oldr

    local old = RenderTexture.active
    RenderTexture.active = rt
    screenShot:ReadPixels(rect, 0, 0)
    screenShot:Apply()

    CMainCamera.CaptureCamera.targetTexture = nil
    RenderTexture.active = old
    GameObject.Destroy(rt)

    CScreenCaptureWnd.IsCapturing = false
    GLTextManager.Visible = true

    if isSave then
        if Application.platform == RuntimePlatform.WindowsPlayer or Application.platform == RuntimePlatform.WindowsEditor then
            local bytes = CommonDefs.EncodeToJPG(screenShot)
            local CWinUtility = import "L10.UI.CWinUtility"
            local fileName
            local default
            default, fileName = CWinUtility.Inst:GetSaveFileName("jpg")
            if default then
                File.WriteAllBytes(fileName, bytes)
            end
        end
    end

    return screenShot
end

function LuaScrollTaskMgr:GetScreenshot(screenshotKey)
    if screenshotKey == "GameScreen" then 
        self:TryInitGameTexture()
        if CScene.MainScene and CScene.MainScene.GamePlayDesignId == 51102280 then 
            self:ShowOrHideHeadInfo(true)
        end
    else 
        if self.m_Screenshots["GameScreen"] and CMainCamera.CaptureCamera then
            if self.m_OldHeight then
                CMainCamera.Inst.m_Height = self.m_OldHeight
                self.m_OldHeight = nil
            end
            CMainCamera.CaptureCamera.targetTexture = nil
            local percent = 1
            if CommonDefs.Is_PC_PLATFORM() then
                if CWinSocialWndMgr == nil then
                    CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
                end
                if CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
                    percent = (1 - Constants.WinSocialWndRatio)
                end
            end
            CMainCamera.CaptureCamera.rect = CreateFromClass(Rect, 0, 0, percent, 1)
            local go = self.m_Screenshots["GameScreen"]
            GameObject.Destroy(go)
            self.m_Screenshots["GameScreen"] = nil
        end
        self:ShowOrHideHeadInfo(false)
    end

    if self.m_Screenshots[screenshotKey] then
        return self.m_Screenshots[screenshotKey]
    end

    if self.m_ScreenShotsByLoad[screenshotKey] then
        return self.m_ScreenShotsByLoad[screenshotKey], true
    end
end
LuaScrollTaskMgr.m_HeadInfoPanelShowDepth = -2
function LuaScrollTaskMgr:ShowOrHideHeadInfo(bShow)
    if CHeadInfoWnd.Instance and CHeadInfoWnd.Instance.panel then
        CHeadInfoWnd.Instance.panel.depth = bShow and self.m_HeadInfoPanelShowDepth or -2 
        if CClientMainPlayer.Inst then
            CClientMainPlayer.Inst.m_IsHeadInfoVisible = not bShow
            EventManager.BroadcastInternalForLua(EnumEventType.UpdatePlayerForceInfo, {})
        end
    end
end

function LuaScrollTaskMgr:PreLoadPicture(screenshotKey)
    CSharpResourceLoader.Inst:LoadTexture2D("UI/Texture/ScrollTask/" .. screenshotKey .. ".jpg", DelegateFactory.Action_Texture2D(function(texture)
        self.m_ScreenShotsByLoad[screenshotKey] = texture
        g_ScriptEvent:BroadcastInLua("ScrollTaskPreLoadPictureFinished")
    end))
end

function LuaScrollTaskMgr:ClearScreenshot()
    if CMainCamera.CaptureCamera then
        if self.m_OldHeight then
            CMainCamera.Inst.m_Height = self.m_OldHeight
            self.m_OldHeight = nil
        end
        CMainCamera.CaptureCamera.targetTexture = nil
        CMainCamera.CaptureCamera.backgroundColor = Color.black
        local percent = 1
        if CommonDefs.Is_PC_PLATFORM() then
            if CWinSocialWndMgr == nil then
                CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
            end
            if CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
                percent = (1 - Constants.WinSocialWndRatio)
            end
        end
        CMainCamera.CaptureCamera.rect = CreateFromClass(Rect, 0, 0, percent, 1)
    end
    for k, v in pairs(self.m_Screenshots) do
        GameObject.Destroy(v)
    end
    self.m_Screenshots = {}
    self.m_ScreenShotsByLoad = {}
end
------------------------------------------
-- 截图相关 end
------------------------------------------

------------------------------------------
-- 梳篦相关
------------------------------------------

LuaScrollTaskMgr.CombShapePropertys = {
    {525, 273, "scrolltaskwnd_shubi_01"},
    {510, 355, "scrolltaskwnd_shubi_02"},
    {489, 459, "scrolltaskwnd_shubi_03"}
}

LuaScrollTaskMgr.CombMaterialPropertys = {
    {"A2AECAFF", "3B3D5206", "scrolltaskwnd_shubi_yu"},
    {"B38711FF", "5F330B06", "scrolltaskwnd_shubi_yu"},
    {"CCB579FF", "2E1C0B06", "scrolltaskwnd_shubi_mu"},
    {"97B772FF", "1F721905", "scrolltaskwnd_shubi_yu"},
}

LuaScrollTaskMgr.CombDecoratePropertys = {
    {606, 411, 80, "scrolltaskwnd_shubi_fanhua"},
    {651, 387, 80, "scrolltaskwnd_shubi_caidie"},
    {688, 603, 0, "scrolltaskwnd_shubi_diaozhui"},
    {736, 318, 80, "scrolltaskwnd_shubi_feicui"},
}

function LuaScrollTaskMgr:GenCombTex(uiTexture, combInfo)
    if uiTexture.material == nil then
        
    end
    local shapeProperty = self.CombShapePropertys[combInfo.Shape]
    local materialProperty = self.CombMaterialPropertys[combInfo.Material]
    local decorateProperty = self.CombDecoratePropertys[combInfo.Decorate]

    local shapeTexName = shapeProperty[3]
    local MaterialTexName = materialProperty[3]

    CSharpResourceLoader.Inst:LoadMaterial("UI/Texture/Transparent/Material/" .. shapeTexName .. ".mat", DelegateFactory.Action_Material(function(mat)
        uiTexture.material = mat
        CSharpResourceLoader.Inst:LoadMaterial("UI/Texture/NonTransparent/Material/" .. MaterialTexName .. ".mat", DelegateFactory.Action_Material(function(mat)
            uiTexture.material:SetTexture("_MainTex", mat:GetTexture("_MainTex"))
            self:SetMaterialProperty(uiTexture.material, materialProperty)
            if uiTexture.panel then
                uiTexture.panel:Refresh()
            end
        end))
    end))

    uiTexture.width = shapeProperty[1]
    uiTexture.height = shapeProperty[2]
    self:AddDecorate(uiTexture, decorateProperty)
end

function LuaScrollTaskMgr:SetMaterialProperty(mat, combMaterialProperty)
    mat:SetColor("_TintColor", NGUIText.ParseColor32(combMaterialProperty[1], 0))

    mat:SetColor("_MaskColor1", NGUIText.ParseColor32(combMaterialProperty[2], 0))
end

function LuaScrollTaskMgr:AddDecorate(uiTexture, decorateProperty)
    local DecorateMatPath = "UI/Texture/Transparent/Material/" .. decorateProperty[4] .. ".mat"
    local decoreate = uiTexture.transform:Find("Decoreate")
    if not decoreate then
        decoreate = NGUITools.AddChild(uiTexture.gameObject)
        decoreate.name = "Decoreate"
        CommonDefs.AddComponent_GameObject_Type(decoreate, typeof(UITexture))
        CommonDefs.AddComponent_GameObject_Type(decoreate, typeof(CUITexture))
    end
    
    local decoreateUITexture = decoreate:GetComponent(typeof(CUITexture))
    decoreateUITexture:LoadMaterial(DecorateMatPath)
    decoreateUITexture.texture.width = decorateProperty[1]
    decoreateUITexture.texture.height = decorateProperty[2]
    decoreateUITexture.texture.depth = uiTexture.depth + 1
    Extensions.SetLocalPositionY(decoreateUITexture.transform, decorateProperty[3])
end

function LuaScrollTaskMgr:SaveCombDataSuccess(taskId, material, decoration, shape)

    g_ScriptEvent:BroadcastInLua("OnSaveCombDataSuccess")
end

LuaScrollTaskMgr.m_PictureCombData = nil

function LuaScrollTaskMgr:PlayCombPicture(taskId, material, decoration, shape, duration, width, height)
    self.m_PictureCombData = {Shape = shape, Material = material, Decorate = decoration}
    CZhuJueJuQingMgr.Instance.PicTexturePath = ""
    CZhuJueJuQingMgr.Instance.PicDuduration = duration
    CLuaJuqingPicWnd.s_Width=width
    CLuaJuqingPicWnd.s_Height=height
    CUIManager.ShowUI(CUIResources.JuqingPicWnd)
end

------------------------------------------
-- 梳篦相关 end
------------------------------------------

------------------------------------------
-- 音游相关
------------------------------------------

function LuaScrollTaskMgr:StartMusicRun(musicId)
    self:SetMusicRunCamera()
    self:StartScrollTaskGameEvent(0, "MusicGame_"..musicId)
end

function LuaScrollTaskMgr:StopMusicRun(musicId)
    self:ResetMusicRunCamera()
    g_ScriptEvent:BroadcastInLua("ScrollTaskMusicGameStopMusicRun", musicId)
end


function LuaScrollTaskMgr:SetMusicRunCamera()
    local percent = 1
    if CommonDefs.Is_PC_PLATFORM() then
        if CWinSocialWndMgr == nil then
            CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
        end
        if CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
            percent = (1 - Constants.WinSocialWndRatio)
        end
    end

    local r = 90
    local z = 5
    local y = 0
    local vec3 = Vector3(r, z, y)
    CameraFollow.Inst.targetRZY = vec3
    CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
    CameraFollow.Inst.BasePosOffset = Vector3(0, 0, 1.67 * percent)
    CameraFollow.Inst.m_CameraBindEnable = true
    CameraFollow.Inst.m_CameraBindDirectionOffset = -90
    CameraFollow.Inst.m_ForbiddDragOrZoom = true
end

function LuaScrollTaskMgr:ResetMusicRunCamera()
    local r = 90
    local z = 12
    local y = 9.5
    local vec3 = Vector3(r, z, y)
    CameraFollow.Inst.targetRZY = vec3
    CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
    CameraFollow.Inst.BasePosOffset = Vector3(0, 0, 0)
    CameraFollow.Inst.m_CameraBindEnable = false
    CameraFollow.Inst.m_CameraBindDirectionOffset = 0
    CameraFollow.Inst.m_ForbiddDragOrZoom = false
end
------------------------------------------
-- 音游相关 end
------------------------------------------
