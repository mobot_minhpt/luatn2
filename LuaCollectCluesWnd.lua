local UIGrid = import "UIGrid"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local UIPanel = import "UIPanel"
local Animation = import "UnityEngine.Animation"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"

LuaCollectCluesWnd = class()

--@region RegistChildComponent: Dont Modify Manually!


--@endregion RegistChildComponent end

function LuaCollectCluesWnd:Awake()
    self.UnlockLabel = self.transform:Find("reelBgPanel/yirensiwnd_reel_bg/UnlockLabel"):GetComponent(typeof(UILabel))
    self.TotalLabel = self.UnlockLabel.transform:Find("TotalLabel"):GetComponent(typeof(UILabel))
    self.Content = self.transform:Find("QnTableView1/ScrollView/Content").gameObject
    self.ItemCell = self.Content.transform:Find("ItemCell").gameObject

    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:RefreshConstUI()
    self:RefreshVariableUI()
    
    local scrollComp = self.transform:Find("QnTableView1/ScrollView"):GetComponent(typeof(CUIRestrictScrollView))
    scrollComp.enabled = false
    RegisterTickOnce(function ()
        if not CommonDefs.IsNull(scrollComp) then
            scrollComp.enabled = true
        end
    end, 1000)
end

function LuaCollectCluesWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaCollectCluesWnd:InitWndData()
    self.prefixPath = ""

    --cluesTable是聚合clueType之后的数组, 以clueType为KEY！
    self.cluesTable = {}
    self.cluesObjList = {}
    NanDuFanHua_YiRenSiClues.Foreach(function (k, v)
        if self.cluesTable[v.CluesType] then
            table.insert(self.cluesTable[v.CluesType].IdList, v.Id)
            table.insert(self.cluesTable[v.CluesType].DescribeList, v.Describe)
        else    
            local srcName = v.CluesName
            local e = string.find(srcName,'%(')
            if e then
                srcName = string.sub(srcName,1,e-1)
            end

            local tbl = {IdList = {v.Id}, Name = srcName, CluesMaterial = v.CluesMaterial, DescribeList = {v.Describe}}
            self.cluesTable[v.CluesType] = tbl
        end
    end)
end

function LuaCollectCluesWnd:RefreshConstUI()
    self.ItemCell:SetActive(false)
    
    for i = 1, #self.cluesTable do
        local go = CommonDefs.Object_Instantiate(self.ItemCell)
        go.transform.parent = self.Content.transform:Find("Root" .. tostring(i))
        
        go.transform.localScale = Vector3.one
        go.transform.localPosition = Vector3.zero
        go:SetActive(true)
        go.transform:Find("Mask/Tex").gameObject:SetActive(false)
        go.transform:Find("Panel/LockedSprite").gameObject:SetActive(false)
        go.transform:Find("Panel/LockMask").gameObject:SetActive(false)
        go.transform:Find("Panel/RedDot").gameObject:SetActive(false)
        go.transform:Find("Name"):GetComponent(typeof(UILabel)).text = self.cluesTable[i].Name
        go.transform:Find("Mask/Tex"):GetComponent(typeof(CUITexture)):LoadMaterial(self.prefixPath .. self.cluesTable[i].CluesMaterial)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function ()
            self:OnClickClues(i)
        end)
        self.cluesObjList[i] = go
    end
end

function LuaCollectCluesWnd:RefreshVariableUI()
    local readClues = LuaYiRenSiMgr.ReadClues and LuaYiRenSiMgr.ReadClues or 0
    local findClues = LuaYiRenSiMgr.FindClues and LuaYiRenSiMgr.FindClues or 0
    local playAnimationClues = LuaYiRenSiMgr.PlayAnimationClues and LuaYiRenSiMgr.PlayAnimationClues or 0
    local findFullNumber = 0
    local aniCluesPlayList = {}
    for i = 1, #self.cluesTable do
        local clueObj = self.cluesObjList[i]
        if #self.cluesTable[i].IdList > 1 then
            --特殊线索
            local clueListLen = #self.cluesTable[i].IdList
            local findNumber = 0
            local showRedDot = false
            for t = 1, clueListLen do
                local cluesId = self.cluesTable[i].IdList[t]
                local isFind = (bit.band(findClues, bit.lshift(1, cluesId - 1)) ~= 0)
                local isRead = (bit.band(readClues, bit.lshift(1, cluesId - 1)) ~= 0)
                findNumber = findNumber + (isFind and 1 or 0)
                showRedDot = showRedDot or (isFind and (not isRead))
            end
            clueObj.transform:Find("Mask/Tex").gameObject:SetActive(true)
            clueObj.transform:Find("Panel/LockedSprite").gameObject:SetActive(findNumber < clueListLen)
            clueObj.transform:Find("Panel/LockMask").gameObject:SetActive(findNumber < clueListLen)
            clueObj.transform:Find("Panel/LockMask"):GetComponent(typeof(UITexture)).fillAmount = 1 - (findNumber/clueListLen)
            clueObj.transform:Find("Panel/RedDot").gameObject:SetActive(showRedDot)
            clueObj.transform:Find("Name"):GetComponent(typeof(UILabel)).alpha = (findNumber >= clueListLen and 1 or 0.5)

            findFullNumber = findFullNumber + (findNumber >= clueListLen and 1 or 0)
        else
            --普通线索
            local cluesId = self.cluesTable[i].IdList[1]
            local isFind = (bit.band(findClues, bit.lshift(1, cluesId - 1)) ~= 0)
            local isRead = (bit.band(readClues, bit.lshift(1, cluesId - 1)) ~= 0)
            local isPlayAnimation = (bit.band(playAnimationClues, bit.lshift(1, cluesId - 1)) ~= 0)

            clueObj.transform:Find("Mask/Tex").gameObject:SetActive(true)
            if isFind then
                --找到过这条线索
                if isPlayAnimation then
                    --播放过这条线索动画
                    clueObj.transform:Find("Panel/LockedSprite").gameObject:SetActive(not isFind)
                    clueObj.transform:Find("Panel/LockMask").gameObject:SetActive(not isFind)
                    clueObj.transform:Find("Panel/RedDot").gameObject:SetActive(isFind and (not isRead))
                    clueObj.transform:Find("Name"):GetComponent(typeof(UILabel)).alpha = (isFind and 1 or 0.5)
                else
                    --没有播放过这条线索动画
                    clueObj.transform:Find("Panel/LockedSprite").gameObject:SetActive(true)
                    clueObj.transform:Find("Panel/LockMask").gameObject:SetActive(true)
                    clueObj.transform:Find("Panel/RedDot").gameObject:SetActive(false)
                    clueObj.transform:Find("Name"):GetComponent(typeof(UILabel)).alpha = 0.5
                    self:PlayUnlockAnimation(i, 1000)
                    table.insert(aniCluesPlayList, cluesId)
                end
            else
                --没有找到过这条线索
                clueObj.transform:Find("Panel/LockedSprite").gameObject:SetActive(not isFind)
                clueObj.transform:Find("Panel/LockMask").gameObject:SetActive(not isFind)
                clueObj.transform:Find("Panel/RedDot").gameObject:SetActive(isFind and (not isRead))
                clueObj.transform:Find("Name"):GetComponent(typeof(UILabel)).alpha = (isFind and 1 or 0.5)
            end
            findFullNumber = findFullNumber + (isFind and 1 or 0)
        end
    end

    RegisterTickOnce(function ()
        if aniCluesPlayList then
            if #aniCluesPlayList > 0 then
                Gac2Gas.YiRenSiPlayAniClues(g_MessagePack.pack(aniCluesPlayList))
            end
        end
    end, 2000)


    self.UnlockLabel.text = findFullNumber
    self.TotalLabel.text = #self.cluesTable
end

function LuaCollectCluesWnd:OnClickClues(cluesType)
    local findClues = LuaYiRenSiMgr.FindClues and LuaYiRenSiMgr.FindClues or 0
    local readClues = LuaYiRenSiMgr.ReadClues and LuaYiRenSiMgr.ReadClues or 0

    if #self.cluesTable[cluesType].IdList > 1 then
        --特殊线索
        local clueListLen = #self.cluesTable[cluesType].IdList
        local findNumber = 0
        local firstOpenClueId = nil
        for t = 1, clueListLen do
            local cluesId = self.cluesTable[cluesType].IdList[t]
            local isFind = (bit.band(findClues, bit.lshift(1, cluesId - 1)) ~= 0)
            findNumber = findNumber + (isFind and 1 or 0)
            if isFind and findNumber == 1 then
                firstOpenClueId = cluesId
            end
        end
        if findNumber > 0 then
            LuaCluesDetailWnd.OpenClueId = firstOpenClueId
            CUIManager.ShowUI(CLuaUIResources.CluesDetailWnd)
        else
            g_MessageMgr:ShowMessage('YIRENSI_NO_CLUE_YET')
        end
    else
        --普通线索
        local cluesId = self.cluesTable[cluesType].IdList[1]
        local isFind = (bit.band(findClues, bit.lshift(1, cluesId - 1)) ~= 0)
        local isRead = (bit.band(readClues, bit.lshift(1, cluesId - 1)) ~= 0)
        if isFind then
            if not isRead then
                Gac2Gas.YiRenSiReadClues(cluesId)
            end
            LuaCluesDetailWnd.OpenClueId = cluesId
            CUIManager.ShowUI(CLuaUIResources.CluesDetailWnd)
        else
            g_MessageMgr:ShowMessage('YIRENSI_NO_CLUE_YET')
        end
    end
end

function LuaCollectCluesWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncYiRenSiFindCluesData", self, "OnSyncYiRenSiFindCluesData")
    Gac2Gas.RequestYiRenSiFindCluesData()
end

function LuaCollectCluesWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncYiRenSiFindCluesData", self, "OnSyncYiRenSiFindCluesData")
end 

function LuaCollectCluesWnd:OnSyncYiRenSiFindCluesData()
    self:RefreshVariableUI()
end

function LuaCollectCluesWnd:PlayUnlockAnimation(clueType, delay)
    RegisterTickOnce(function ()
        local go = self.cluesObjList[clueType]
        if go then
            go.transform:Find("Vfx").gameObject:SetActive(true)
            go.transform:GetComponent(typeof(Animation)):Play("nandufanhua_unlock")
        end
    end, delay)
end

