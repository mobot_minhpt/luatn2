require("3rdParty/ScriptEvent")
require("common/common_include")

local UISprite = import "UISprite"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIRes = import "L10.UI.CUIResources"
local Extensions = import "Extensions"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CUIManager = import "L10.UI.CUIManager"
local UIGrid = import "UIGrid"
local LocalString = import "LocalString"
local Profession = import "L10.Game.Profession"

CLuaInjusticeWnd=class()
RegistClassMember(CLuaInjusticeWnd,"CloseBtn")
RegistClassMember(CLuaInjusticeWnd,"RankTemplate")
RegistClassMember(CLuaInjusticeWnd,"MyRankItem")
RegistClassMember(CLuaInjusticeWnd,"RankGrid")

function CLuaInjusticeWnd:Init()
	local onCloseClick = function(go)
		CUIManager.CloseUI(CUIRes.InjusticeWnd)
	end
	Extensions.RemoveAllChildren(self.RankGrid.transform)
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
	self.RankTemplate:SetActive(false)
	local selfRank = 0
	for i,v in ipairs(LuaKitePlayMgr.InjusticeRankTable) do
		local node = NGUITools.AddChild(self.RankGrid,self.RankTemplate)
		node:SetActive(true)
		node.transform:Find("rank"):GetComponent(typeof(UILabel)).text = i
		node.transform:Find("icon"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(v.playerClass)
		node.transform:Find("name"):GetComponent(typeof(UILabel)).text = v.playerName
		node.transform:Find("time"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%s分%s秒"),math.floor(v.score/60),v.score%60)
		if v.playerId == CClientMainPlayer.Inst.Id then
			selfRank = i
		end
	end
	if selfRank > 0 then
		self.MyRankItem.transform:Find("rank"):GetComponent(typeof(UILabel)).text = selfRank
	else
		self.MyRankItem.transform:Find("rank"):GetComponent(typeof(UILabel)).text = LocalString.GetString("未上榜")
	end
	self.MyRankItem.transform:Find("icon"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIcon(CClientMainPlayer.Inst.Class)
	self.MyRankItem.transform:Find("name"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Name
	self.MyRankItem.transform:Find("time"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%s分%s秒"),math.floor(LuaKitePlayMgr.InjusticeSelfSocre/60),LuaKitePlayMgr.InjusticeSelfSocre%60)

	self.RankGrid:GetComponent(typeof(UIGrid)):Reposition()
end

return CLuaInjusticeWnd
