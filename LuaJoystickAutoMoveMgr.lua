local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local PathFindDefs = import "L10.Engine.PathFinding.PathFindDefs"
local EMoveType = import "L10.Engine.EMoveType"
local EMoveToResult = import "L10.Engine.EMoveToResult"
local Setting = import "L10.Engine.Setting"
local Vector3 = import "UnityEngine.Vector3"
local Mathf = import "UnityEngine.Mathf"
local CPos = import "L10.Engine.CPos"

CLuaJoystickAutoMoveMgr = class()
CLuaJoystickAutoMoveMgr.m_MoveDir = nil

CLuaJoystickAutoMoveMgr.m_ChangeDirTickInterval = 100
CLuaJoystickAutoMoveMgr.m_ChangeDirRadianPerTick = Deg2Rad * 30
CLuaJoystickAutoMoveMgr.m_ChangeDirCosPerTick = math.cos(CLuaJoystickAutoMoveMgr.m_ChangeDirRadianPerTick)
CLuaJoystickAutoMoveMgr.m_ChangeDirSinPerTick = math.sin(CLuaJoystickAutoMoveMgr.m_ChangeDirRadianPerTick)

CLuaJoystickAutoMoveMgr.m_DragDir = nil
CLuaJoystickAutoMoveMgr.m_DragDot = 0
CLuaJoystickAutoMoveMgr.m_Threshold = 0.0001

function CLuaJoystickAutoMoveMgr:AddListener()
	g_ScriptEvent:RemoveListener("MainPlayerMoveEnded", self, "OnMainPlayerMoveEnded")
	g_ScriptEvent:RemoveListener("PlayerDragJoyStick", self, "OnPlayerDragJoyStick")
	g_ScriptEvent:RemoveListener("PlayerDragJoyStickComplete", self, "OnPlayerDragJoyStickComplete")
	g_ScriptEvent:AddListener("MainPlayerMoveEnded", self, "OnMainPlayerMoveEnded")
	g_ScriptEvent:AddListener("PlayerDragJoyStick", self, "OnPlayerDragJoyStick")
	g_ScriptEvent:AddListener("PlayerDragJoyStickComplete", self, "OnPlayerDragJoyStickComplete")
end
CLuaJoystickAutoMoveMgr:AddListener()

function CLuaJoystickAutoMoveMgr:DoMove(bMoveEnd)
	if not (CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsInJoystickAutoMove) then return end

	local speed = CClientMainPlayer.Inst.MaxSpeed
	local pixelSpeed = CClientMainPlayer.Inst.MaxPixelSpeed

	local deltaVec = CommonDefs.op_Multiply_Single_Vector3(2 * speed, self.m_MoveDir)

	local posX = CClientMainPlayer.Inst.WorldPos.x + deltaVec.x
	local posY = CClientMainPlayer.Inst.WorldPos.z + deltaVec.z
	posX = math.max(math.min(posX, CRenderScene.Inst.MapWidth - 1), 1)
	posY = math.max(math.min(posY, CRenderScene.Inst.MapHeight - 1), 1)

	local target = CPos(posX * Setting.eGridSpan, posY * Setting.eGridSpan)
	local ret = CClientMainPlayer.Inst:MoveTo(target, pixelSpeed, 0,0,0, PathFindDefs.EFindPathType.eFPT_HypoLine, CClientMainPlayer.Inst.BarrierType, false, EMoveType.Normal, PathFindDefs.MAX_REGION_LIMIT)

	if ret ~= EMoveToResult.Success then
		if ret ~= EMoveToResult.NotAllowed and ret ~= EMoveToResult.SuperPosition and self.m_DragDot ~= 0 then
			ret = CClientMainPlayer.Inst:MoveTo(target, pixelSpeed, 0,0,0, PathFindDefs.EFindPathType.eFPT_HypoLineHelper, CClientMainPlayer.Inst.BarrierType, false, EMoveType.Normal, PathFindDefs.MAX_REGION_LIMIT)
		end
		if ret ~= EMoveToResult.Success then
			if self.m_RetryMoveTick ~= nil then
				UnRegisterTick(self.m_RetryMoveTick)
				self.m_RetryMoveTick = nil
			end
			self.m_RetryMoveTick = RegisterTickOnce(function()
				self:DoMove()
			end, 100)
		end
	end
end

function CLuaJoystickAutoMoveMgr:DoChangeMoveDir()
	if not (CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsInJoystickAutoMove) then return end

	local dragDir = self.m_DragDir
	local moveDir = self.m_MoveDir

	if dragDir.x == 0 and dragDir.z == 0 then
		return
	end

	dragDir = dragDir.normalized
	if Vector3.Dot(dragDir, moveDir) >= self.m_ChangeDirCosPerTick then
		self.m_MoveDir.x = dragDir.x
		self.m_MoveDir.z = dragDir.z
	else
		local cosVal = self.m_ChangeDirCosPerTick 
		local sinVal = self.m_ChangeDirSinPerTick 
		if moveDir.x * dragDir.z - moveDir.z * dragDir.x < 0 then
			sinVal = -sinVal
		end
		local newDirX = moveDir.x * cosVal - moveDir.z * sinVal
		local newDirZ = moveDir.z * cosVal + moveDir.x * sinVal

		self.m_MoveDir.x = newDirX
		self.m_MoveDir.z = newDirZ
	end

	self:DoMove()
end

function CLuaJoystickAutoMoveMgr:ChangeMoveDir(dir, bForce)
	if not (CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsInJoystickAutoMove) then return end

	local dragDir = self.m_DragDir
	if bForce or math.abs(dragDir.x - dir.x) > self.m_Threshold or math.abs(dragDir.z - dir.z) > self.m_Threshold then
		self.m_DragDir = Vector3(dir.x, 0, dir.z)

		local dragDot = Vector3.Dot(self.m_DragDir, self.m_MoveDir)
		self.m_DragDot = dragDot

		if self.m_ChangeDirTick ~= nil then
			return
		end
		self.m_ChangeDirTick = RegisterTick(function()
			if not (CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsInJoystickAutoMove) then
				UnRegisterTick(self.m_ChangeDirTick)
				self.m_ChangeDirTick = nil
				return
			end

			if (self.m_DragDir.x == 0 and self.m_DragDir.z == 0) or (self.m_MoveDir.x == self.m_DragDir.x and self.m_MoveDir.z == self.m_DragDir.z) then
				UnRegisterTick(self.m_ChangeDirTick)
				self.m_ChangeDirTick = nil
				return
			end

			self:DoChangeMoveDir()
		end, self.m_ChangeDirTickInterval)

		self:DoChangeMoveDir()
	end
end

function CLuaJoystickAutoMoveMgr:OnMainPlayerMoveEnded()
	self:DoMove(true)
end

function CLuaJoystickAutoMoveMgr:OnPlayerDragJoyStick(args)
	self:ChangeMoveDir(args[0])
end

function CLuaJoystickAutoMoveMgr:OnPlayerDragJoyStickComplete()
	self:ChangeMoveDir(Vector3(0, 0, 0), true)
end

function Gas2Gac.StartJoystickAutoMove(changeDirSpeed, changeDirInterval)
	if not CClientMainPlayer.Inst then return end

	local dir = CClientMainPlayer.Inst.Dir

	local radian = Deg2Rad * dir
	local dirX = Mathf.Cos(radian)
	local dirZ = Mathf.Sin(radian)
	CLuaJoystickAutoMoveMgr.m_MoveDir = Vector3(dirX, 0, dirZ)

	CLuaJoystickAutoMoveMgr.m_ChangeDirTickInterval = math.max(changeDirInterval, 30)
	CLuaJoystickAutoMoveMgr.m_ChangeDirRadianPerTick = Deg2Rad * (changeDirSpeed * changeDirInterval / 1000)
	CLuaJoystickAutoMoveMgr.m_ChangeDirCosPerTick = math.cos(CLuaJoystickAutoMoveMgr.m_ChangeDirRadianPerTick)
	CLuaJoystickAutoMoveMgr.m_ChangeDirSinPerTick = math.sin(CLuaJoystickAutoMoveMgr.m_ChangeDirRadianPerTick)

	CLuaJoystickAutoMoveMgr.m_DragDir = Vector3(0, 0, 0)
	CLuaJoystickAutoMoveMgr.m_DragDot = 0

	CClientMainPlayer.Inst.IsInJoystickAutoMove = true
	CLuaJoystickAutoMoveMgr:DoMove()
end

function Gas2Gac.EndJoystickAutoMove()
	if not CClientMainPlayer.Inst then return end

	CClientMainPlayer.Inst.IsInJoystickAutoMove = false
	CClientMainPlayer.Inst:StopMove()
end
