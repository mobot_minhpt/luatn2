local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"

LuaNpcInvitedEnchantAbilityInfoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaNpcInvitedEnchantAbilityInfoWnd, "BuffIcon", "BuffIcon", CUITexture)
RegistChildComponent(LuaNpcInvitedEnchantAbilityInfoWnd, "BuffNameLabel", "BuffNameLabel", UILabel)
RegistChildComponent(LuaNpcInvitedEnchantAbilityInfoWnd, "BuffTypeLabel", "BuffTypeLabel", UILabel)
RegistChildComponent(LuaNpcInvitedEnchantAbilityInfoWnd, "EnchantAbilityDescription", "EnchantAbilityDescription", UILabel)

--@endregion RegistChildComponent end

function LuaNpcInvitedEnchantAbilityInfoWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaNpcInvitedEnchantAbilityInfoWnd:Init()
    local data = SectInviteNpc_EnchantAbility.GetData(LuaInviteNpcMgr.m_EnchatId)
    self.BuffIcon:LoadMaterial(data.EnchantIcon)
    self.BuffNameLabel.text = data.EnchantEffectName
    self.BuffTypeLabel.text = data.EnchantTypeName
    self.EnchantAbilityDescription.text = data.EnchantEffectDescribe
end

--@region UIEvent

--@endregion UIEvent

