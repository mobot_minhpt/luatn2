-- Auto Generated!!
local CKeJuGambleTemplate = import "L10.UI.CKeJuGambleTemplate"
local CKeJuGambleWnd = import "L10.UI.CKeJuGambleWnd"
local CKeJuMgr = import "L10.Game.CKeJuMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Engine = import "L10.Engine"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local KeJu_Setting = import "L10.Game.KeJu_Setting"
local L10 = import "L10"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local MsgPackImpl = import "MsgPackImpl"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local UInt64 = import "System.UInt64"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CKeJuGambleWnd.m_Init_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.playerList)
    CommonDefs.ListClear(this.chooseList)
    this.titleLabel.text = g_MessageMgr:FormatMessage("KEJU_GAMBLE")
    this.titleLabel2.text = g_MessageMgr:FormatMessage("KEJU_GAMBLE2")
    this:InitChooseCount()
    Extensions.RemoveAllChildren(this.table.transform)
    this.playerTemplate:SetActive(false)
    do
        local i = 0
        while i < CKeJuMgr.Inst.gamblePlayerInfo.Count do
            local instance = NGUITools.AddChild(this.table.gameObject, this.playerTemplate)
            instance:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(instance, typeof(CKeJuGambleTemplate)):Init(CKeJuMgr.Inst.gamblePlayerInfo[i].playerName, CKeJuMgr.Inst.gamblePlayerInfo[i].playerId)
            UIEventListener.Get(instance).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(instance).onClick, MakeDelegateFromCSFunction(this.OnPlayerClick, VoidDelegate, this), true)
            CommonDefs.ListAdd(this.playerList, typeof(GameObject), instance)
            i = i + 1
        end
    end
    this.table:Reposition()
    this.scrollview:ResetPosition()
    if this.cancel ~= nil then
        invoke(this.cancel)
    end
    this.countDown = KeJu_Setting.GetData().DianShiCountDownDuration
    this.okButtonLabel.text = tostring(this.countDown)
    this.cancel = L10.Engine.CTickMgr.Register(MakeDelegateFromCSFunction(this.CountDown, Action0, this), 1000, Engine.ETickType.Loop)
end
CKeJuGambleWnd.m_CountDown_CS2LuaHook = function (this) 

    this.countDown = this.countDown - 1
    if this.countDown > 0 then
        this.okButtonLabel.text = tostring(this.countDown)
    else
        invoke(this.cancel)
        this:Close()
    end
end
CKeJuGambleWnd.m_OnPlayerClick_CS2LuaHook = function (this, go) 

    do
        local i = 0
        while i < this.playerList.Count do
            local continue
            repeat
                if go == this.playerList[i] then
                    local template = CommonDefs.GetComponent_GameObject_Type(this.playerList[i], typeof(CKeJuGambleTemplate))
                    if template == nil then
                        continue = true
                        break
                    end
                    template:SetSelect(false)
                    if not CommonDefs.ListContains(this.chooseList, typeof(UInt64), template.playerId) then
                        if this.chooseList.Count >= 3 then
                            g_MessageMgr:ShowMessage("KEJU_GAMBLE_CHOOSE_EXCESS")
                            return
                        else
                            CommonDefs.ListAdd(this.chooseList, typeof(UInt64), template.playerId)
                            template:SetSelect(true)
                        end
                    else
                        CommonDefs.ListRemove(this.chooseList, typeof(UInt64), template.playerId)
                    end
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
    this:InitChooseCount()
end
CKeJuGambleWnd.m_InitChooseCount_CS2LuaHook = function (this) 
    local msg = ""
    if this.chooseList.Count == 0 then
        msg = LocalString.GetString("最多可选3人,已选[c][FF0000]0[-]/3")
    else
        msg = System.String.Format(LocalString.GetString("最多可选3人,已选{0}/3"), this.chooseList.Count)
    end
    this.countLabel.text = msg
end
CKeJuGambleWnd.m_OnOkButtonClick_CS2LuaHook = function (this, go) 

    if this.chooseList.Count == 3 then
        --Gac2Gas.SubmitDianShiGamblingPlayerId(this.selectPlayerid);
        this:VotePlayer()
        this:Close()
    elseif this.chooseList.Count > 0 then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("KEJU_GAMBLE_CHOOSE_LESS3"), MakeDelegateFromCSFunction(this.VotePlayer, Action0, this), nil, nil, nil, false)
    else
        g_MessageMgr:ShowMessage("KEJU_GAMBLE_CHOOSE_EMPTY")
    end
end
CKeJuGambleWnd.m_VotePlayer_CS2LuaHook = function (this) 
    local obj = CreateFromClass(MakeGenericClass(List, Object))
    do
        local i = 0
        while i < this.chooseList.Count do
            CommonDefs.ListAdd(obj, typeof(UInt64), this.chooseList[i])
            i = i + 1
        end
    end
    local data = MsgPackImpl.pack(obj)
    Gac2Gas.SubmitDianShiGamblingPlayerId(data)
    this:Close()
end
