local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CSkillButtonInfo = import "L10.UI.CSkillButtonInfo"
local UISlider = import "UISlider"
local GameObject = import "UnityEngine.GameObject"
local SoundManager = import "SoundManager"
local Vector2 = import "UnityEngine.Vector2"
local CMoveScalingEffect = import "L10.Game.CMoveScalingEffect"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CPos = import "L10.Engine.CPos"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CUIResources = import "L10.UI.CUIResources"
local Utility = import "L10.Engine.Utility"
local CForcesMgr = import "L10.Game.CForcesMgr"
local Animation = import "UnityEngine.Animation"
local CClientBullet = import "L10.Game.CClientBullet"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CStatusMgr = import "L10.Game.CStatusMgr"
local EPropStatus = import "L10.Game.EPropStatus"

LuaTurkeyMatchSkillWnd = class()

-- @region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaTurkeyMatchSkillWnd, "Button", "Button", CSkillButtonInfo)
RegistChildComponent(LuaTurkeyMatchSkillWnd, "AdditionButton", "AdditionButton", CSkillButtonInfo)
RegistChildComponent(LuaTurkeyMatchSkillWnd, "ProgressBar", "ProgressBar", UISlider)
RegistChildComponent(LuaTurkeyMatchSkillWnd, "Stage3", "Stage3", GameObject)
RegistChildComponent(LuaTurkeyMatchSkillWnd, "FilledTip", "FilledTip", GameObject)

-- @endregion RegistChildComponent end
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_Progress")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_MakeSkillId")
--RegistClassMember(LuaTurkeyMatchSkillWnd, "m_ReplaceSkillId")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_UpSpeedSkillId")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_SkillButtons")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_Tick")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_MaxTime")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_DamageProgress")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_Tweener")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_SnowBallFxId")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_CurProgress")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_SliderTex")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_SkillTex")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_SkillTexPaths")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_LastIndex")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_JiasuSkillTex")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_FillFxWid")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_Animator")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_IsShowSkill")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_ProgressWidget")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_SliderTexPaths")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_MyForce")
RegistClassMember(LuaTurkeyMatchSkillWnd, "m_CachedPos")

function LuaTurkeyMatchSkillWnd:Awake()
    self.m_FillFxWid = self.transform:Find("GameObject"):GetComponent(typeof(UIWidget))
    self.m_ProgressWidget = self.transform:Find("kgdjindu"):GetComponent(typeof(UIWidget))
    self.m_Animator = self.transform:GetComponent(typeof(Animation))
    self.m_SliderTex = self.ProgressBar.transform:Find("Foreground"):GetComponent(typeof(CUITexture))
    self.m_SkillTex = self.Button:GetComponent(typeof(CUITexture))
    self.m_SliderTexPaths = {}
    local format1 = "UI/Texture/FestivalActivity/Festival_Christmas/Christmas2022/Material/turkeymatchskillwnd_christmas2022_jindu0%d.mat"
    for i = 1, 2 do
        table.insert(self.m_SliderTexPaths, SafeStringFormat3(format1, i))
    end
    self.m_SkillTexPaths = {}
    local format2 = "UI/Texture/FestivalActivity/Festival_Christmas/Christmas2022/Material/turkeymatchskillwnd_christmas2022_jineng0%d.mat"
    for i = 1, 3 do
        table.insert(self.m_SkillTexPaths, SafeStringFormat3(format2, i))
    end
    -- @region EventBind: Dont Modify Manually!
    -- @endregion EventBind end
    UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSkillButtonClicked(go)
    end)
    UIEventListener.Get(self.AdditionButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSkillButtonClicked(go)
    end)

    self.m_Progress = 0
    local tempSkills = Christmas2022_Setting.GetData().TempSkillIds
    self.m_MakeSkillId = tempSkills[0]
    self.m_UpSpeedSkillId = tempSkills[1]
    self.m_SkillButtons = {self.Button, self.AdditionButton}
    self.m_SnowBallFxId = 88804278
    self.m_MaxTime = Christmas2022_Setting.GetData().SnowballFullTime or 5
    self.m_DamageProgress = Christmas2022_Setting.GetData().SnowballDamageTime /self.m_MaxTime
    self.m_CurProgress = 0
    self.m_LastIndex = 1
    self.m_IsShowSkill = false
    self.m_ProgressWidget.alpha = 0
    self.m_FillFxWid.gameObject:SetActive(false)
    self.m_CachedPos = {x=0,z=0}
end

function LuaTurkeyMatchSkillWnd:Init()
    CUIManager.SetUIVisibility(CUIResources.SkillButtonBoard, false, "TurkeyMatch")
    CUIManager.SetUIVisibility(CUIResources.CurrentTarget, false, "TurkeyMatch")

    self:InitSkill()
    UnRegisterTick(self.m_Tick)
    self.m_Tick = RegisterTick(function()
        self:OnTick()
    end, 100)
end

function LuaTurkeyMatchSkillWnd:InitSkill()
    self.m_SkillTex:LoadMaterial(self.m_SkillTexPaths[1])

    self.m_MyForce = CForcesMgr.Inst:GetPlayerForce(CClientMainPlayer.Inst.Id)

    local skill1 = self.m_MakeSkillId
    local data1 = Skill_AllSkills.GetData(skill1)
    self.Button:LoadIcon("", skill1, false, true)

    local skill2 = self.m_UpSpeedSkillId
    local data2 = Skill_AllSkills.GetData(skill2)
    self.AdditionButton:LoadIcon(SafeStringFormat3("UI/Texture/Skill/Material/%s.mat", self.m_MyForce == 1 and "skill_90_20" or "skill_90_21") , skill2, true)
end
function LuaTurkeyMatchSkillWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateCooldown", self, "OnUpdateCooldown")
    g_ScriptEvent:AddListener("MainPlayerSkillPropUpdate", self, "OnMainPlayerSkillPropUpdate")
end

function LuaTurkeyMatchSkillWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateCooldown", self, "OnUpdateCooldown")
    g_ScriptEvent:RemoveListener("MainPlayerSkillPropUpdate", self, "OnMainPlayerSkillPropUpdate")
end

-- @region UIEvent

-- @endregion UIEvent

function LuaTurkeyMatchSkillWnd:UpdateTurkeyMatchSkillProgress(time)
    local progress = time > self.m_MaxTime and 1 or time / self.m_MaxTime
    if self.m_Tweener then
        LuaTweenUtils.Kill(self.m_Tweener, false)
    end
    if not self.m_IsShowSkill then
        self.m_Animator:Play("turkeymatchskillwnd_show")
    end
    self.m_IsShowSkill = true
    if self.ProgressBar.value <= 1 then
        local time = math.abs(progress - self.m_CurProgress) * 4
        self.m_Tweener = LuaTweenUtils.TweenFloat(self.m_CurProgress, progress, time, function ( val )
            self.m_CurProgress = val
            self.ProgressBar.value = val * 0.54 + 0.44
            self:RefreshSkillTex(val)
        end)
    end
    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst:EnableMainPlayerRotateSpeed(360)
    end
end

function LuaTurkeyMatchSkillWnd:HideTurkeyMatchSkillProgress(time)
    local progress = 0
    if self.m_Tweener then
        LuaTweenUtils.Kill(self.m_Tweener, false)
    end
    if self.m_IsShowSkill then
        self.m_Animator:Play("turkeymatchskillwnd_close")
    end
    self.m_IsShowSkill = false
    self.m_CurProgress = 0
    self.ProgressBar.value = 0
    self:RefreshSkillTex(-1)
    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst:DisableMainPlayerRotateSpeed()
    end
end

function LuaTurkeyMatchSkillWnd:RefreshSkillTex(progress)
    local index 
    if progress < 0 then
        index = 1
    elseif progress < self.m_DamageProgress then
        index = 2
    elseif progress < 1 then
        index = 3
    else
        index = 4
    end
    if index ~= self.m_LastIndex then
        local sliderPath = self.m_SliderTexPaths[index >= 3 and 2 or 1]
        local path = self.m_SkillTexPaths[index == 4 and 3 or index]
        self.Stage3:SetActive(index >= 3)
        self.FilledTip:SetActive(index == 4)
        self.m_FillFxWid.gameObject:SetActive(index == 4)
        self.m_SliderTex:LoadMaterial(sliderPath)
        self.m_SkillTex:LoadMaterial(path)
        if index == 4 then
            self.m_FillFxWid.alpha = 1
        end
    end
    self.m_LastIndex = index
end

function LuaTurkeyMatchSkillWnd:OnUpdateCooldown()
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then
        return
    end
    local cooldownProp = mainplayer.CooldownProp
    local globalRemain = cooldownProp:GetServerRemainTime(1000)
    local globalTotal = cooldownProp:GetServerTotalTime(1000)
    local globalPercent = (globalTotal == 0) and 0 or (globalRemain / globalTotal)
    for i = 1, #self.m_SkillButtons do
        local skillButtonInfo = self.m_SkillButtons[i]
        local skillId = skillButtonInfo.skillTemplateID
        local remain = cooldownProp:GetServerRemainTime(skillId)
        local total = cooldownProp:GetServerTotalTime(skillId)
        local percent = total == 0 and 0 or remain / total
        if globalRemain > remain then
            percent = globalPercent
        end
        skillButtonInfo.CDLeft = percent
    end
end

function LuaTurkeyMatchSkillWnd:OnMainPlayerSkillPropUpdate()
    self:OnTick()
end

function LuaTurkeyMatchSkillWnd:OnSkillButtonClicked(go)
    local mainplayer = CClientMainPlayer.Inst

    if mainplayer == nil or mainplayer.IsDie then
        return
    end

    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.SkillButton, Vector3.zero, nil, 0)
    local skillButtonInfo = CommonDefs.GetComponent_GameObject_Type(go, typeof(CSkillButtonInfo))
    local id = skillButtonInfo.skillTemplateID
    self:CastSkill(id, true, Vector2.zero)
end

function LuaTurkeyMatchSkillWnd:CastSkill(skillId, autoTarget, pos)
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then
        return
    end
    local realSkillId = mainplayer.SkillProp:GetAlternativeSkillIdWithDelta(skillId, mainplayer.Level)
    mainplayer:TryCastSkill(realSkillId, autoTarget, pos)
end

function LuaTurkeyMatchSkillWnd:OnTick()
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then
        return
    end
    CUIManager.SetUIVisibility(CUIResources.SkillButtonBoard, false, "TurkeyMatch")
    local snowBallFx = (mainplayer.RO:GetFxById(self.m_SnowBallFxId) or {}).m_FX
    if snowBallFx then
        local effect = CommonDefs.GetComponentInChildren_GameObject_Type(snowBallFx.gameObject, typeof(CMoveScalingEffect))
        if effect then
            self:UpdateTurkeyMatchSkillProgress(effect.m_Time)
            local centerPos = effect.transform:Find("xueqiugaodu/xue")
            local playerId, pixelDis = self:GetNearestPlayerIdAndPixelDistance(Utility.WorldPos2PixelPos(centerPos and centerPos.transform.position or effect.transform.position))
            local curR = effect.m_Scale
            if playerId and curR * 64 > pixelDis then
                Gac2Gas.TurkeyMatchSnowballHitPlayer(playerId)
            end
        else
            self:HideTurkeyMatchSkillProgress()
        end
    else
        self:HideTurkeyMatchSkillProgress()
    end
end

function LuaTurkeyMatchSkillWnd:OnDestroy()
    UnRegisterTick(self.m_Tick)
    if self.m_Tweener then
        LuaTweenUtils.Kill(self.m_Tweener, false)
    end
    CUIManager.SetUIVisibility(CUIResources.SkillButtonBoard, true, "TurkeyMatch")
    CUIManager.SetUIVisibility(CUIResources.CurrentTarget, true, "TurkeyMatch")

    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst:DisableMainPlayerRotateSpeed()
    end
end

function LuaTurkeyMatchSkillWnd:GetNearestPlayerIdAndPixelDistance(snowBallPos)
    local allFightObject = CClientObjectMgr.Inst:GetAllCanFightObjects()
    local nearestPlayerId = nil
    local nearestPixelDis = 99999999
    for i = 0, allFightObject.Count - 1 do
        local obj = allFightObject[i]
        if TypeIs(obj, typeof(CClientOtherPlayer)) then
            local player = TypeAs(obj, typeof(CClientOtherPlayer))
            if self.m_MyForce ~= CForcesMgr.Inst:GetPlayerForce(player.PlayerId) then 
                local pixelDis = CPos.Distance_XY(snowBallPos, obj.Pos)
                if pixelDis < nearestPixelDis then
                    nearestPlayerId = player.PlayerId
                    nearestPixelDis = pixelDis
                end

                -- 雪球
                local fx = (player.RO:GetFxById(self.m_SnowBallFxId) or {}).m_FX
                if fx then
                    local effect = CommonDefs.GetComponentInChildren_GameObject_Type(fx.gameObject, typeof(CMoveScalingEffect))
                    local centerPos = effect.transform:Find("xueqiugaodu/xue")
                    local fxPos = Utility.WorldPos2PixelPos(centerPos and centerPos.transform.position or effect.transform.position)
                    local otherCurR = effect.m_Scale * 64
                    pixelDis = CPos.Distance_XY(snowBallPos, fxPos) - otherCurR
                    if pixelDis < nearestPixelDis then
                        nearestPlayerId = player.PlayerId
                        nearestPixelDis = pixelDis
                    end
                end
            end
        end
    end
    return nearestPlayerId, nearestPixelDis
end