local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"
local CMonsterPropertyFight=import "L10.Game.CMonsterPropertyFight"

CLuaHouseWoodPileMgr = {}
CLuaHouseWoodPileMgr.m_NumLimit = 20

CLuaHouseWoodPileMgr.m_FightStatus = nil
CLuaHouseWoodPileMgr.m_Damages = nil
CLuaHouseWoodPileMgr.m_IsHeal = false--是否是治疗木桩
CLuaHouseWoodPileMgr.m_StartFightTick = nil

function CLuaHouseWoodPileMgr.IsWoodPile(tempid)
    if tempid==9127 or tempid==9128 then
        return true
    end
    return false
end
function CLuaHouseWoodPileMgr.IsHealWoodPile(tempid)
    if tempid==9128 then
        return true
    end
    return false
end

function CLuaHouseWoodPileMgr.StartFightWithWoodPile(engineid,tempalteid)
	CLuaHouseWoodPileMgr.m_Damages = {}
end

function CLuaHouseWoodPileMgr.IsFightingWithSelf(engineId)
	if CLuaHouseWoodPileMgr.m_FightStatus then
		local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
		if CLuaHouseWoodPileMgr.m_FightStatus[engineId]==myId then
			return true
		end
	end
	return false
end
function CLuaHouseWoodPileMgr.IsFighting(engineId)
	if CLuaHouseWoodPileMgr.m_FightStatus then
		if CLuaHouseWoodPileMgr.m_FightStatus[engineId] then
			return true
		end
	end
	return false
end

function Gas2Gac.SyncHouseWoodPileFightStatus(fightStatusU)
    local list = MsgPackImpl.unpack(fightStatusU)
	CLuaHouseWoodPileMgr.m_FightStatus = {}
	local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
	local count= 0
	for i=1,list.Count,2 do
		local id = tonumber(list[i])
		CLuaHouseWoodPileMgr.m_FightStatus[tonumber(list[i-1])] = id--engineId playerId
		if id==myId then
			count=count+1
		end
    end
	g_ScriptEvent:BroadcastInLua("SyncHouseWoodPileFightStatus")
	if count==0 then
		CUIManager.CloseUI(CLuaUIResources.HouseWoodPileStatusWnd)
	end
end

function Gas2Gac.SyncPileFightProgressInfo(pileId,damageInfoU)
	local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
	local isMyInfo = false
	local fur = CommonDefs.DictGetValue_LuaCall(CClientFurnitureMgr.Inst.WoodPileFurnitureList,pileId)
	local engineId = fur and fur.EngineId or 0

	if CLuaHouseWoodPileMgr.m_FightStatus then
		for k,v in pairs(CLuaHouseWoodPileMgr.m_FightStatus) do
			if v==myId and k==engineId then
				isMyInfo = true
				break
			end
		end
	end
	if not isMyInfo then
		return
	end
	local list = MsgPackImpl.unpack(damageInfoU)
	
	local damageInfo = {
		startTime = tonumber(list[0]),
		petDamage = tonumber(list[1]),
		lingshouDamage = tonumber(list[2]),
		totalDamage = tonumber(list[3]),
		otherPetDamage = tonumber(list[6])
	}
	damageInfo.skillDamageInfo = {}
	local list2 = list[4]
	for i=1,list2.Count,2 do
		damageInfo.skillDamageInfo[tonumber(list2[i-1])] = tonumber(list2[i])
	end

	damageInfo.specialPetDamageInfo = {}
	local list3 = list[5]
	for i=1,list3.Count,2 do
		damageInfo.specialPetDamageInfo[tonumber(list3[i-1])] = tonumber(list3[i])
	end

	local fur = CommonDefs.DictGetValue_LuaCall(CClientFurnitureMgr.Inst.WoodPileFurnitureList,pileId)
	local tempalteid = fur and fur.TemplateId or 0
	CLuaHouseWoodPileMgr.m_IsHeal = CLuaHouseWoodPileMgr.IsHealWoodPile(tempalteid)
	if not CUIManager.IsLoaded(CLuaUIResources.HouseWoodPileStatusWnd) then
		CUIManager.ShowUI(CLuaUIResources.HouseWoodPileStatusWnd)
	end
	if not CLuaHouseWoodPileMgr.m_Damages then
		CLuaHouseWoodPileMgr.m_Damages = {}
	end
    CLuaHouseWoodPileMgr.m_Damages[pileId] = damageInfo

    g_ScriptEvent:BroadcastInLua("SyncPileFightProgressInfo", pileId,damageInfo)
end

function Gas2Gac.AttachWoodPileToMonster(monsterEngineId, pileId,templateId)
	CClientFurnitureMgr.Inst:AttachWoodPileToMonster(monsterEngineId,pileId,templateId)
    g_ScriptEvent:BroadcastInLua("AttachWoodPileToMonster", monsterEngineId,pileId,templateId)
end


function Gas2Gac.SendWoodPileFightProp(engineId, fightPropU)
	local fightProp= CMonsterPropertyFight()
	fightProp:LoadFromString(fightPropU)
    g_ScriptEvent:BroadcastInLua("SendWoodPileFightProp", engineId,fightProp)
end
