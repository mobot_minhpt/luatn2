local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CRankData = import "L10.UI.CRankData"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"
local Profession = import "L10.Game.Profession"

LuaTurkeyMatchRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaTurkeyMatchRankWnd, "MyRankLabel", "MyRankLabel", UILabel)
RegistChildComponent(LuaTurkeyMatchRankWnd, "MyNameLabel", "MyNameLabel", UILabel)
RegistChildComponent(LuaTurkeyMatchRankWnd, "TableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaTurkeyMatchRankWnd, "MyFristScoreLabel", "MyFristScoreLabel", UILabel)
RegistChildComponent(LuaTurkeyMatchRankWnd, "MySecondScoreLabel", "MySecondScoreLabel", UILabel)
RegistChildComponent(LuaTurkeyMatchRankWnd, "MyTotalScoreLabel", "MyTotalScoreLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaTurkeyMatchRankWnd, "m_MyRankImage")
RegistClassMember(LuaTurkeyMatchRankWnd, "m_RankList")

function LuaTurkeyMatchRankWnd:Awake()
    self.m_MyRankImage = self.MyRankLabel.transform:Find("RankImage"):GetComponent(typeof(UISprite))
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    if CClientMainPlayer.Inst then
        self.MyNameLabel.text = CClientMainPlayer.Inst.Name
    end
    self.m_MyRankImage.spriteName=""
    self.MyRankLabel.text = LocalString.GetString("未上榜")
    self.MyFristScoreLabel.text = LocalString.GetString("暂无")
    self.MySecondScoreLabel.text = LocalString.GetString("暂无")
    self.MyTotalScoreLabel.text = LocalString.GetString("暂无")

    self.m_RankList={}
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RankList
        end,
        function(item,index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

            self:InitItem(item,index,self.m_RankList[index+1])
        end
    )
    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local data = self.m_RankList[row+1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
        end
    end)
end

function LuaTurkeyMatchRankWnd:Init()
    Gac2Gas.QueryTurkeyMatchRankData()
end

--@region UIEvent

--@endregion UIEvent


function LuaTurkeyMatchRankWnd:InitItem(item,index,info)
    local tf=item.transform
    local fristScoreLabel=tf:Find("FristScoreLabel"):GetComponent(typeof(UILabel))
    fristScoreLabel.text=""
    local secondScoreLabel=tf:Find("SecondScoreLabel"):GetComponent(typeof(UILabel))
    secondScoreLabel.text=""
    local totalScoreLabel=tf:Find("TotalScoreLabel"):GetComponent(typeof(UILabel))
    totalScoreLabel.text=""
    local nameLabel=tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text=""
    local rankLabel=tf:Find("RankLabel"):GetComponent(typeof(UILabel))
    rankLabel.text=""
    local rankSprite=tf:Find("RankLabel/RankImage"):GetComponent(typeof(UISprite))
    rankSprite.spriteName=""
    local rank=info.rankPos
    if rank==1 then
        rankSprite.spriteName="Rank_No.1"
    elseif rank==2 then
        rankSprite.spriteName="Rank_No.2png"
    elseif rank==3 then
        rankSprite.spriteName="Rank_No.3png"
    else
        rankLabel.text=tostring(rank)
    end
    
    nameLabel.text = info.name
    fristScoreLabel.text = info.firstStageScore
    secondScoreLabel.text = self:GetRemainTimeText(info.secondStageScore)
end

function LuaTurkeyMatchRankWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryTurkeyMatchRankData_Result", self, "OnQueryTurkeyMatchRankData_Result")
end

function LuaTurkeyMatchRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryTurkeyMatchRankData_Result", self, "OnQueryTurkeyMatchRankData_Result")
end

function LuaTurkeyMatchRankWnd:OnQueryTurkeyMatchRankData_Result(rankPos, firstStageScore, secondStageScore, infos)
    self.MyRankLabel.text = rankPos > 0 and rankPos or LocalString.GetString("未上榜")
    if CClientMainPlayer.Inst then
        self.MyNameLabel.text = CClientMainPlayer.Inst.Name
    end
    
    self.MyFristScoreLabel.text = firstStageScore
    self.MySecondScoreLabel.text = self:GetRemainTimeText(secondStageScore)
    
    self.m_RankList = infos
    self.TableView:ReloadData(true,false)
end

function LuaTurkeyMatchRankWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        return LocalString.GetString("暂无")
    elseif totalSeconds >= 3600 then
        return SafeStringFormat3(LocalString.GetString("%02d:%02d:%02d"), math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3(LocalString.GetString("%02d:%02d"), math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end
