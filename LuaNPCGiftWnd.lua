local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
LuaNPCGiftWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaNPCGiftWnd, "Gift", "Gift", GameObject)
RegistChildComponent(LuaNPCGiftWnd, "RedPackage", "RedPackage", GameObject)
RegistChildComponent(LuaNPCGiftWnd, "ItemCell", "ItemCell", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaNPCGiftWnd, "m_NPCId")
RegistClassMember(LuaNPCGiftWnd, "m_EventId")
RegistClassMember(LuaNPCGiftWnd, "m_Type")
RegistClassMember(LuaNPCGiftWnd, "m_ItemId")        -- 奖励道具ID
RegistClassMember(LuaNPCGiftWnd, "m_ItemNum")       -- 奖励道具数量
RegistClassMember(LuaNPCGiftWnd, "m_IsBind")        -- 奖励道具是否绑定
RegistClassMember(LuaNPCGiftWnd, "m_AwardCount")    -- 奖励数值
RegistClassMember(LuaNPCGiftWnd, "m_TimeStr") -- 时间Label
RegistClassMember(LuaNPCGiftWnd, "m_Fx")

RegistClassMember(LuaNPCGiftWnd, "m_delayTick")
RegistClassMember(LuaNPCGiftWnd, "m_animDelay")

function LuaNPCGiftWnd:Awake()
    self.m_animDelay = 500
    self.Gift.gameObject:SetActive(false)
    self.RedPackage.gameObject:SetActive(false)
    self.m_Fx = self.transform:Find("Anchor/fx"):GetComponent(typeof(CUIFx))
    self.m_Fx:DestroyFx()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end
-- MsgId:对应的消息类型
-- NPCID:发送方NPC的ID
-- IsAccept:是否已经领取过
function LuaNPCGiftWnd:Init()
    local data = LuaNPCChatMgr.OpenNPCGiftWndData
    self.m_NPCId = data.NPCID
    self.m_EventId = data.EventID
    self.m_Type = data.Type
    local MainPlayer = CClientMainPlayer.Inst
    if not MainPlayer then return end
    local isAccept = MainPlayer.RelationshipProp.FinishNpcChatEventSet:GetBit(NpcChat_ChatEvent.GetData(data.EventID).SN)
    if isAccept then
        local TimeStamp = LuaNPCChatMgr:GetEventTime(self.m_EventId)
        if TimeStamp and tonumber(TimeStamp) then
            local date = os.date("*t",tonumber(TimeStamp))
            self.m_TimeStr = SafeStringFormat3("%d-%02d-%02d %02d:%02d:%02d",date.year,date.month,date.day,date.hour,date.min,date.sec)
        else
            self.m_TimeStr = ""
        end
    else
        self.m_TimeStr = ""
    end
    if self.m_Type == EnumNPCChatEventType.Gift then
        self.m_ItemId = data.Args.ItemID
        self.m_ItemNum = data.Args.ItemNum
        self.m_IsBind = data.Args.isBind
        if isAccept then
            self:InitGiftHasReceive(false)
        else
            self:InitGiftWithoutReceive()
        end
    elseif self.m_Type == EnumNPCChatEventType.RedPackage then
        self.m_AwardCount = data.Args.AwardAmount
        if isAccept then
            self:InitRedPackageHasReceive(false)
        else
            self:InitRedPackageWithoutReceive()
        end
    end
end
-- 打开礼物动画
function LuaNPCGiftWnd:DoOpenGiftAnim()
    self.m_Fx:LoadFx("fx/ui/prefab/UI_liaotian_liwudakai.prefab")
    self:InitGiftHasReceive(true)
end
-- 已经领取过礼物
function LuaNPCGiftWnd:InitGiftHasReceive(needanim)
    self.Gift:SetActive(true)
    self.RedPackage:SetActive(false)
    local data = NPC_NPC.GetData(self.m_NPCId)
    self.Gift.transform:Find("UnReceive").gameObject:SetActive(false)
    local Receive = self.Gift.transform:Find("Receive")
    Receive.transform:Find("NPCPortrait").gameObject:SetActive(false)
    Receive.transform:Find("TimeLabel"):GetComponent(typeof(UILabel)).text = self.m_TimeStr
    Receive.transform:Find("NPCPortrait").gameObject:SetActive(false)
    --local Portrait = Receive.transform:Find("NPCPortrait"):GetComponent(typeof(CUITexture))
    --Portrait:LoadPortrait(data.Portrait, false)
    if needanim then
        if self.m_delayTick then
            UnRegisterTick(self.m_delayTick)
            self.m_delayTick = nil
        end
        self.m_delayTick = RegisterTickOnce(function()
            self:ShowItemCell(self.m_ItemId,self.m_ItemNum)
        end, self.m_animDelay)
    else
        self:ShowItemCell(self.m_ItemId,self.m_ItemNum)
    end
    Receive.gameObject:SetActive(true)
end
-- 还未领取过礼物
function LuaNPCGiftWnd:InitGiftWithoutReceive()
    self.Gift:SetActive(true)
    self.RedPackage:SetActive(false)
    local data = NPC_NPC.GetData(self.m_NPCId)
    self.ItemCell:SetActive(false)
    self.Gift.transform:Find("Receive").gameObject:SetActive(false)
    local UnReceive = self.Gift.transform:Find("UnReceive")
    local Portrait = UnReceive.transform:Find("NPCPortrait"):GetComponent(typeof(CUITexture))
    Portrait:LoadPortrait(data.Portrait, false)
    Portrait.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.StrH2V(SafeStringFormat3(LocalString.GetString("来自%s的礼物"),data.Name),false)
    UIEventListener.Get(UnReceive.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        Gac2Gas.RequestFinishNpcChatEvent(self.m_NPCId,self.m_EventId)
    end)
    UnReceive.gameObject:SetActive(true)
end

function LuaNPCGiftWnd:ShowItemCell(ItemID,Num)
    local Item = Item_Item.GetData(ItemID)
    if not Item then return end
    local Icon = self.ItemCell.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local NumLabel = self.ItemCell.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
    local BindSprite = self.ItemCell.transform:Find("BindSprite")
    local ItemQuality = self.ItemCell.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
    BindSprite.gameObject:SetActive(self.m_IsBind)
    Icon:LoadMaterial(Item.Icon)
    ItemQuality.spriteName = CUICommonDef.GetItemCellBorder(Item, nil, false)
    if Num == 1 then
        NumLabel.gameObject:SetActive(false)
    else
        NumLabel.gameObject:SetActive(true)
        NumLabel.text = tostring(Num)
    end
    UIEventListener.Get(self.ItemCell).onClick = DelegateFactory.VoidDelegate(function (go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(ItemID)
    end)
    self.ItemCell:SetActive(true)
end
-- 打开红包动画
function LuaNPCGiftWnd:DoOpenRedPackageAnim()
    self.m_Fx:LoadFx("fx/ui/prefab/UI_liaotian_hongbaodakai.prefab")
    self:InitRedPackageHasReceive(true)
end
-- 已经领取过红包
function LuaNPCGiftWnd:InitRedPackageHasReceive(needanim)
    self.Gift:SetActive(false)
    self.RedPackage:SetActive(true)
    local data = NPC_NPC.GetData(self.m_NPCId)
    self.RedPackage.transform:Find("UnReceive").gameObject:SetActive(false)
    local Receive = self.RedPackage.transform:Find("Receive")
    Receive.transform:Find("TimeLabel"):GetComponent(typeof(UILabel)).text = self.m_TimeStr
    Receive.transform:Find("NPCPortraitLeft").gameObject:SetActive(false)
    local Portrait = Receive.transform:Find("NPCPortrait"):GetComponent(typeof(CUITexture))
    Portrait:LoadPortrait(data.Portrait, false)
    local NameLabel = Receive.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local MoneyNum = Receive.transform:Find("MoneyNum"):GetComponent(typeof(UILabel))
    NameLabel.text = SafeStringFormat3(LocalString.GetString("来自%s的红包"),data.Name)
    MoneyNum.text = tostring(self.m_AwardCount)
    if needanim then
        NameLabel.gameObject:SetActive(false)
        MoneyNum.gameObject:SetActive(false)
        if self.m_delayTick then
            UnRegisterTick(self.m_delayTick)
            self.m_delayTick = nil
        end
        self.m_delayTick = RegisterTickOnce(function()
            NameLabel.gameObject:SetActive(true)
            MoneyNum.gameObject:SetActive(true)
        end, self.m_animDelay)
    end
    Receive.gameObject:SetActive(true)
end
-- 还未领取过红包
function LuaNPCGiftWnd:InitRedPackageWithoutReceive()
    self.Gift:SetActive(false)
    self.RedPackage:SetActive(true)
    local data = NPC_NPC.GetData(self.m_NPCId)
    self.RedPackage.transform:Find("Receive").gameObject:SetActive(false)
    local UnReceive = self.RedPackage.transform:Find("UnReceive")
    local Portrait = UnReceive.transform:Find("NPCPortrait"):GetComponent(typeof(CUITexture))
    Portrait:LoadPortrait(data.Portrait, false)
    Portrait.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = LocalString.StrH2V(SafeStringFormat3(LocalString.GetString("来自%s的红包"),data.Name),false)
    UIEventListener.Get(UnReceive.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        Gac2Gas.RequestFinishNpcChatEvent(self.m_NPCId,self.m_EventId)
    end)
    UnReceive.gameObject:SetActive(true)
end
function LuaNPCGiftWnd:OnSetNpcChatEventFinished(NPCID,EventID)
    if EventID == self.m_EventId and self.m_NPCId == NPCID then
        local TimeStamp = LuaNPCChatMgr:GetEventTime(self.m_EventId)
        if TimeStamp and tonumber(TimeStamp) then
            local date = os.date("*t",tonumber(TimeStamp))
            self.m_TimeStr = SafeStringFormat3("%d-%02d-%02d %02d:%02d:%02d",date.year,date.month,date.day,date.hour,date.min,date.sec)
        else
            self.m_TimeStr = ""
        end
        if self.m_Type == EnumNPCChatEventType.Gift then
            self:DoOpenGiftAnim()
        elseif self.m_Type == EnumNPCChatEventType.RedPackage then
            self:DoOpenRedPackageAnim()
        end
    end
end

function LuaNPCGiftWnd:OnEnable()
    g_ScriptEvent:AddListener("SetNpcChatEventFinished",self,"OnSetNpcChatEventFinished")
end

function LuaNPCGiftWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SetNpcChatEventFinished",self,"OnSetNpcChatEventFinished")
    if self.m_delayTick then
        UnRegisterTick(self.m_delayTick)
        self.m_delayTick = nil
    end
end
    --@region UIEvent

--@endregion UIEvent

