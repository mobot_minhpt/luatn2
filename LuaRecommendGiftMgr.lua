
LuaRecommendGiftMgr = class()
LuaRecommendGiftMgr.CurItemId = nil
LuaRecommendGiftMgr.ExpireTime = 0
LuaRecommendGiftMgr.RemainTime = 0

function LuaRecommendGiftMgr.OnPlayerLogin()
    LuaRecommendGiftMgr.CurItemId = nil
	LuaRecommendGiftMgr.ExpireTime = 0
	LuaRecommendGiftMgr.NowTime = 0
	LuaRecommendGiftMgr.Purchased = false
	LuaRecommendGiftMgr.ShowAlert = false
    LuaRecommendGiftMgr.CountDownEndTime = 0
    CLuaActivityAlert.s_RecommendGiftStatus = false
end