

local QnCheckBoxGroup = import "L10.UI.QnCheckBoxGroup"

local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

LuaQiXi2021ConfirmWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQiXi2021ConfirmWnd, "OkButton", "OkButton", GameObject)
RegistChildComponent(LuaQiXi2021ConfirmWnd, "CancelButton", "CancelButton", GameObject)
RegistChildComponent(LuaQiXi2021ConfirmWnd, "LbCost1", "LbCost1", UILabel)
RegistChildComponent(LuaQiXi2021ConfirmWnd, "LbCost2", "LbCost2", UILabel)
RegistChildComponent(LuaQiXi2021ConfirmWnd, "CheckGroup", "CheckGroup", QnCheckBoxGroup)
RegistChildComponent(LuaQiXi2021ConfirmWnd, "LbCost3", "LbCost3", UILabel)

--@endregion RegistChildComponent end

function LuaQiXi2021ConfirmWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick(go)
	end)

	UIEventListener.Get(self.CancelButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelButtonClick(go)
	end)

	self.CheckGroup.OnSelect = DelegateFactory.Action_QnCheckBox_int(function (checkbox,value)
	    self:OnCheckGroupSelect(checkbox,value)
	end)

    --@endregion EventBind end
end

function LuaQiXi2021ConfirmWnd:Init()
	self.m_type = 0
	local data = QiXi_FireWork2021.GetData()
	self.LbCost1.text = tostring(data.LowPrice)
	self.LbCost2.text = tostring(data.HighPrice)
end

--@region UIEvent

function LuaQiXi2021ConfirmWnd:OnOkButtonClick(go)
	if self.m_type == 1 then
		local data = QiXi_FireWork2021.GetData()
		local msg = g_MessageMgr:FormatMessage("2021QiXi_Firework_OpenAgain",data.HighPrice)
		g_MessageMgr:ShowOkCancelMessage(msg,function()
			Gac2Gas.RequestStartFireWorkInYanHuaPlay(self.m_type)
			CUIManager.CloseUI(CLuaUIResources.QiXi2021ConfirmWnd)
		end,nil,nil,nil,false)
	else
		Gac2Gas.RequestStartFireWorkInYanHuaPlay(self.m_type)
	end
end

function LuaQiXi2021ConfirmWnd:OnCancelButtonClick(go)
end

function LuaQiXi2021ConfirmWnd:OnCheckGroupSelect(checkbox,value)
	self.m_type = value
end


--@endregion UIEvent

