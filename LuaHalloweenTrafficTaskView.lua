local CScene = import "L10.Game.CScene"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CChatLinkMgr=import "CChatLinkMgr"
local UICamera = import "UICamera"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"
local CButton = import "L10.UI.CButton"
local String = import "System.String"

LuaHalloweenTrafficTaskView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHalloweenTrafficTaskView, "TaskName", "TaskName", GameObject)
RegistChildComponent(LuaHalloweenTrafficTaskView, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaHalloweenTrafficTaskView, "LeaveBtn", "LeaveBtn", CButton)
RegistChildComponent(LuaHalloweenTrafficTaskView, "RuleBtn", "RuleBtn", CButton)
RegistChildComponent(LuaHalloweenTrafficTaskView, "DescLab", "DescLab", UILabel)

--@endregion RegistChildComponent end

function LuaHalloweenTrafficTaskView:Awake()
    --@region EventBind: Dont Modify Manually!
    UIEventListener.Get(self.LeaveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveBtnClick(go)
	end)

    UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick(go)
	end)
    --@endregion EventBind end
end

function LuaHalloweenTrafficTaskView:Init()
    self.TaskName.gameObject:GetComponent(typeof(UILabel)).text = LocalString.GetString("[幻灵夜]不动如山")
    self.DescLab.text = g_MessageMgr:FormatMessage("Halloween2022_TrafficPlay_Tips")
end
function LuaHalloweenTrafficTaskView:OnSceneRemainTimeUpdate(args)
    if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("[c][aaf8ff]剩余[-][/c] %s"), self:GetRemainTimeText(CScene.MainScene.ShowTime))
        else
            self.CountDownLabel.text = nil
        end
    else
        self.CountDownLabel.text = nil
    end
end

function LuaHalloweenTrafficTaskView:GetRemainTimeText(totalSeconds)
    if totalSeconds <= 1 then
        return ""
    end
    local time
    if totalSeconds >= 3600 then
        time = SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        time = SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
    return time
end
--@region UIEvent
function LuaHalloweenTrafficTaskView:OnLeaveBtnClick(go)
    CGamePlayMgr.Inst:LeavePlay()
end

function LuaHalloweenTrafficTaskView:OnRuleBtnClick(go)
    g_MessageMgr:ShowMessage("Halloween2022_Trafficplay_TaskDes")
end
--@endregion UIEvent
function LuaHalloweenTrafficTaskView:OnEnable()
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
end

function LuaHalloweenTrafficTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
end
