local CButton = import "L10.UI.CButton"
local CShiTuMgr = import "L10.Game.CShiTuMgr"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"

LuaShiTuMainWndEmptyView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShiTuMainWndEmptyView, "ReadMeLabel", "ReadMeLabel", UILabel)
RegistChildComponent(LuaShiTuMainWndEmptyView, "LefBtn", "LefBtn", CButton)
RegistChildComponent(LuaShiTuMainWndEmptyView, "RightBtn", "RightBtn", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaShiTuMainWndEmptyView, "m_IsTudiView")

function LuaShiTuMainWndEmptyView:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.LefBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLefBtnClick()
	end)


	
	UIEventListener.Get(self.RightBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightBtnClick()
	end)


    --@endregion EventBind end
end

function LuaShiTuMainWndEmptyView:OnEnable()
	self.m_IsTudiView = LuaShiTuMgr.m_ShiTuMainWndTabIndex == 0
	self.ReadMeLabel.text = g_MessageMgr:FormatMessage(self.m_IsTudiView and "ShiTuMainWnd_NoneShiFu_ReadMe" or "ShiTuMainWnd_NoneTuDi_ReadMe")
	self.LefBtn.Text = self.m_IsTudiView and LocalString.GetString("看谁寻徒") or LocalString.GetString("看谁求师")
	self.RightBtn.Text = self.m_IsTudiView and LocalString.GetString("我要求师") or LocalString.GetString("我要寻徒")
end

--@region UIEvent

function LuaShiTuMainWndEmptyView:OnLefBtnClick()
	local issuccess = LuaShiTuMgr:GetShiTuResultWndCacheData(self.m_IsTudiView and CShiTuMgr.ShiTuChooseType.TU or CShiTuMgr.ShiTuChooseType.SHI)
	if not issuccess then
		local msg = g_MessageMgr:FormatMessage(self.m_IsTudiView and "ShiTuRecommendWnd_DengJi_QiuShi_Confirm" or "ShiTuRecommendWnd_DengJi_ShouTu_Confirm") 
		MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
			self:OnRightBtnClick()
		end),nil,LocalString.GetString("登记"),LocalString.GetString("取消"),false)
		return
	end
	
	CShiTuMgr.Inst.CurrentEnterType = self.m_IsTudiView and CShiTuMgr.ShiTuChooseType.TU or CShiTuMgr.ShiTuChooseType.SHI
	--CUIManager.ShowUI(CLuaUIResources.ShiTuRecommendWnd)
	if self.m_IsTudiView then
		local isWaiMen = CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel >= ShiTu_Setting.GetData().WaiMenTuDiPlayerLevelLimit
		Gac2Gas.RequestRecommendShiFu(isWaiMen)
	else
		Gac2Gas.RequestRecommendTuDi(false)
	end
end

function LuaShiTuMainWndEmptyView:OnRightBtnClick()
	if not self.m_IsTudiView then
		if CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel < ShiTu_Setting.GetData().CanShouTuLevel then
			g_MessageMgr:ShowMessage("CanShouTu_LevelLimit")
			return
		end
	end
	CShiTuMgr.Inst.SaveResultValue:Clear()
	CShiTuMgr.Inst.CurrentEnterType = self.m_IsTudiView and CShiTuMgr.ShiTuChooseType.TU or CShiTuMgr.ShiTuChooseType.SHI
	CUIManager.ShowUI(CUIResources.ShiTuChooseWnd)
end

--@endregion UIEvent

