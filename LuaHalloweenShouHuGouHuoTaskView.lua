local CScene = import "L10.Game.CScene"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CChatLinkMgr=import "CChatLinkMgr"
local UICamera = import "UICamera"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"
local CButton = import "L10.UI.CButton"
local String = import "System.String"

LuaHalloweenShouHuGouHuoTaskView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHalloweenShouHuGouHuoTaskView, "TaskName", "TaskName", UILabel)
RegistChildComponent(LuaHalloweenShouHuGouHuoTaskView, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaHalloweenShouHuGouHuoTaskView, "LeaveBtn", "LeaveBtn", CButton)
RegistChildComponent(LuaHalloweenShouHuGouHuoTaskView, "RuleBtn", "RuleBtn", CButton)
RegistChildComponent(LuaHalloweenShouHuGouHuoTaskView, "DescLab", "DescLab", UILabel)

--@endregion RegistChildComponent end

function LuaHalloweenShouHuGouHuoTaskView:Awake()
    --@region EventBind: Dont Modify Manually!
    UIEventListener.Get(self.LeaveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveBtnClick(go)
	end)

    UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick(go)
	end)
    --@endregion EventBind end
end

function LuaHalloweenShouHuGouHuoTaskView:Init()
    self.TaskName.text = LocalString.GetString("[幻灵夜]暗夜篝火战")
    self:UpdateTaskText()
end

function LuaHalloweenShouHuGouHuoTaskView:OnSceneRemainTimeUpdate(args)
    if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("剩余 [c][00ff00]%s[-][/c]"), self:GetRemainTimeText(CScene.MainScene.ShowTime))
        else
            self.CountDownLabel.text = nil
        end
    else
        self.CountDownLabel.text = nil
    end
end

function LuaHalloweenShouHuGouHuoTaskView:GetRemainTimeText(totalSeconds)
    if totalSeconds <= 1 then
        return ""
    end
    local time
    if totalSeconds >= 3600 then
        time = SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        time = SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
    return time
end

function LuaHalloweenShouHuGouHuoTaskView:UpdateTaskText()
    local fullMonsterTimes = Halloween2022_Setting.GetData().CampfirePlayMonsterTimes
    local curMonsterTimes = LuaHalloween2022Mgr.m_FireDeferenceMonsterWave
    self.DescLab.text = g_MessageMgr:FormatMessage("Halloween2022_ProtectCampfire_Tips")
                         .."\n"..SafeStringFormat3(LocalString.GetString("已出现(%d/%d)波怪物"),curMonsterTimes,fullMonsterTimes)
end
--@region UIEvent
function LuaHalloweenShouHuGouHuoTaskView:OnLeaveBtnClick(go)
    CGamePlayMgr.Inst:LeavePlay()
end

function LuaHalloweenShouHuGouHuoTaskView:OnRuleBtnClick(go)
    g_MessageMgr:ShowMessage("Halloween2022_ProtectCampfire_TaskDes")
end
--@endregion UIEvent
function LuaHalloweenShouHuGouHuoTaskView:OnEnable()
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:AddListener("SyncFireDefenceMonsterTimes", self, "UpdateTaskText")
end

function LuaHalloweenShouHuGouHuoTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("SyncFireDefenceMonsterTimes", self, "UpdateTaskText")
end



