local LingShou_Setting=import "L10.Game.LingShou_Setting"
local CItemConsumeWndMgr=import "L10.UI.CItemConsumeWndMgr"
local Item_Item=import "L10.Game.Item_Item"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CScene = import "L10.Game.CScene"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local QnTableView = import "L10.UI.QnTableView"
local LingShouOverview = import "L10.Game.CLingShouBaseMgr+LingShouOverview"
local EnumLingShouGender = import "L10.Game.EnumLingShouGender"
local CLingShouBaseMgr = import "L10.Game.CLingShouBaseMgr"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

CLuaLingShouMasterView = class()
RegistClassMember(CLuaLingShouMasterView,"lingShouTable")
RegistClassMember(CLuaLingShouMasterView,"lingShouSlotNum")
RegistClassMember(CLuaLingShouMasterView,"overviewButton")
RegistClassMember(CLuaLingShouMasterView,"m_IsUsedExtraLingShouSlotItem")

RegistClassMember(CLuaLingShouMasterView,"m_IsOtherPlayer")
RegistClassMember(CLuaLingShouMasterView,"m_OtherPlayerInfo")
RegistClassMember(CLuaLingShouMasterView,"m_OtherLingShouInfo")

function CLuaLingShouMasterView:Awake()
    local isQMPKWnd = false 
    local parentScript = self.transform.parent:GetComponent(typeof(CCommonLuaScript))
    if parentScript then
        isQMPKWnd = parentScript.m_LuaWndClassName == "CLuaQMPKLingShouConfig"
    end
    if isQMPKWnd then
        self.m_IsOtherPlayer = CLuaQMPKMgr.s_IsOtherPlayer
        self.m_OtherPlayerInfo = CLuaQMPKMgr.s_PlayerInfo
    end
    self.lingShouTable = self.transform:GetComponent(typeof(QnTableView))
    if self.m_IsOtherPlayer then
        self.lingShouSlotNum = #self.m_OtherPlayerInfo.LingShou
    else
        self.lingShouSlotNum = CClientMainPlayer.Inst and  CClientMainPlayer.Inst.FightProp.LingShouNumber or 0
    end

    local tf = self.transform:Find("OverviewButton")
    if tf then
        self.overviewButton = self.transform:Find("OverviewButton").gameObject
        UIEventListener.Get(self.overviewButton).onClick = DelegateFactory.VoidDelegate(function(p)
            CUIManager.ShowUI(CUIResources.LingshouOverviewWnd)
        end)
    end
end

function CLuaLingShouMasterView:Start( )
    if self.m_IsOtherPlayer then 
        self:InitOtherPlayerLingShou()
    else
        self:InitSelfLingShou()
    end
end

function CLuaLingShouMasterView:InitSelfLingShou()
    self.lingShouTable.m_DataSource = DefaultTableViewDataSource.Create2(        function()
        if CLingShouMgr.Inst.fromNPC then
            return CLingShouMgr.Inst:GetLingShouOverviewList().Count
        else
            if self.m_IsUsedExtraLingShouSlotItem then
                return math.min(self.lingShouSlotNum + 1, 10)
            else
                return math.min(self.lingShouSlotNum + 1, 9)
            end
        end
    end,
    function(view,row)
        local list = CLingShouMgr.Inst:GetLingShouOverviewList()

        if row < list.Count then
            local item = view:GetFromPool(0)--, typeof(CLingShouItem))
            if item ~= nil then
                item:Init(list[row])
                return item
            end
        else
            if row < self.lingShouSlotNum then
                --这是空槽
                local item = view:GetFromPool(0)-- typeof(CLingShouItem))
                if item ~= nil then
                    item:Init(nil)
                    return item
                end
            else
                local ret = view:GetFromPool(1)-- typeof(CLingShouAddItem))
                UIEventListener.Get(ret.gameObject).onClick = DelegateFactory.VoidDelegate(function(p)
                    self:OnClickAddItem(p)
                end)

                return ret
            end
            return nil
        end
    end)

    self.lingShouTable.OnSelectAtRow = DelegateFactory.Action_int(function (row) 
        local list = CLingShouMgr.Inst:GetLingShouOverviewList()
        if row < list.Count then
            CLingShouMgr.Inst.selectedLingShou = list[row].id
        end
	    g_ScriptEvent:BroadcastInLua("SelectLingShouAtRow",row)
    end)

    --请求数据
    CLingShouMgr.Inst:RequestLingShouList()
end
function CLuaLingShouMasterView:InitOtherPlayerLingShou()
    self:InitOtherPlayerLingShouInfo()
    self.lingShouTable.m_DataSource = DefaultTableViewDataSource.Create2(function()
        return #self.m_OtherLingShouInfo
    end,
    function(view,row)
        local item = view:GetFromPool(1)
        if item ~= nil then
            self:InitOtherPlayeLingShouItem(item, self.m_OtherLingShouInfo[row+1])
            return item
        end
    end)

    self.lingShouTable.OnSelectAtRow = DelegateFactory.Action_int(function (row) 
	    g_ScriptEvent:BroadcastInLua("SelectOtherPlayerLingShouAtRow",row)
    end)
    
    self.lingShouTable:ReloadData(false, false)
    self.lingShouTable:SetSelectRow(0, true)
end

function CLuaLingShouMasterView:OnClickAddItem(go)
    local item = Item_Item.GetData(CLingShouMgr.Inst.SheJiWuItem)
    if item ~= nil then
        local slotIndex = CClientMainPlayer.Inst.FightProp.LingShouNumber
        --现有的数量
        local nums = LingShou_Setting.GetData().SheJiWuNum
        if nums ~= nil then
            --下一个需要的数量
            local cost= 0
            if self.m_IsUsedExtraLingShouSlotItem then
                cost = nums[slotIndex-1]
            else
                cost = nums[slotIndex]
            end
            CItemConsumeWndMgr.ShowItemConsumeWnd(CLingShouMgr.Inst.SheJiWuItem, cost, 
                SafeStringFormat3(LocalString.GetString("解锁需要消耗%s"), item.Name), 
                LocalString.GetString("解锁"), 
                DelegateFactory.Action(function () Gac2Gas.AddNewLingShouMaxNumber() end), 
                DelegateFactory.Action(function () CUIManager.ShowUI(CUIResources.LingShouPurchaseWnd) end), 
                DelegateFactory.Action(function () Gac2Gas.AddNewLingShouMaxNumber() end))
        end
    end
end

function CLuaLingShouMasterView:OnEnable( )
    if not self.m_IsOtherPlayer then
        g_ScriptEvent:AddListener("GetAllLingShouOverview", self, "GetAllLingShouOverview")
        g_ScriptEvent:AddListener("DeleteLingShou", self, "DeleteLingShou")

        g_ScriptEvent:AddListener("AddLingShou", self, "AddLingShou")
        g_ScriptEvent:AddListener("UpdatePlayerLingShouMaxNumber", self, "UpdateLingShouMaxNumber")
    end
end
function CLuaLingShouMasterView:OnDisable( )
    if not self.m_IsOtherPlayer then
        g_ScriptEvent:RemoveListener("GetAllLingShouOverview", self, "GetAllLingShouOverview")
        g_ScriptEvent:RemoveListener("DeleteLingShou", self, "DeleteLingShou")

        g_ScriptEvent:RemoveListener("AddLingShou", self, "AddLingShou")
        g_ScriptEvent:RemoveListener("UpdatePlayerLingShouMaxNumber", self, "UpdateLingShouMaxNumber")
    end
end
function CLuaLingShouMasterView:GetAllLingShouOverview( )

    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.IsUsedExtraLingShouSlotItem>0 then
        self.m_IsUsedExtraLingShouSlotItem=true
    else
        self.m_IsUsedExtraLingShouSlotItem=false
    end

    local list = CLingShouMgr.Inst:GetLingShouOverviewList()
    --初始化列表
    self.lingShouTable:ReloadData(false, false)

    if CScene.MainScene ~= nil and CScene.MainScene.LingShouMode == 1 and CSwitchMgr.EnableLingShouHuaLing then
        self.lingShouTable:SetSelectRow(CLingShouMgr.Inst:GetHuaLingIndex(), true)
    else
        local selectedLingShou = CLingShouMgr.Inst.selectedLingShou
        local idx = 0
        if selectedLingShou and selectedLingShou~="" then
            for i=1,list.Count do
                local item = list[i-1]
                if item.id == selectedLingShou then
                    idx = i-1
                    break
                end
            end
            self.lingShouTable:SetSelectRow(idx, true)
        else
            self.lingShouTable:SetSelectRow(CLingShouMgr.Inst:GetBattleIndex(), true)
        end
    end


    if self.lingShouSlotNum < list.Count then
        -- CLogMgr.LogError(((LocalString.GetString("灵兽槽数目不对 slot num:") .. self.lingShouSlotNum) .. " has:") .. list.Count)
    end
end
function CLuaLingShouMasterView:DeleteLingShou( lingshouId) 
    self.lingShouTable:ReloadData(false, false)
    self.lingShouTable:SetSelectRow(0, true)
    --如果没有灵兽了
    -- if self:NumberOfRows(self.lingShouTable) == 0 then
        --contentTf.gameObject.SetActive(false);
        --detailsDisplay.Fill(null);
    -- end
end

function CLuaLingShouMasterView:UpdateLingShouMaxNumber(maxlingshouNumber,isUsedExtraLingShouSlotItem)
    self.lingShouSlotNum = maxlingshouNumber
    if isUsedExtraLingShouSlotItem>0 then
        self.m_IsUsedExtraLingShouSlotItem=true
    else
        self.m_IsUsedExtraLingShouSlotItem=false
    end
    self.lingShouTable:ReloadData()
end

function CLuaLingShouMasterView:AddLingShou(lingshouId)
    self.lingShouTable:ReloadData()
end

function CLuaLingShouMasterView:InitOtherPlayerLingShouInfo()
    self.m_OtherLingShouInfo = {}

    for i = 1, #self.m_OtherPlayerInfo.LingShou do 
        local lingShouData = self.m_OtherPlayerInfo.LingShou[i]
        local overview = {}
        overview.evolveGrade = lingShouData.Props.EvolveGrade
        overview.level = lingShouData.Level
        overview.name = lingShouData.Name
        overview.id = lingShouData.Id
        overview.templateId = lingShouData.TemplateId
        overview.gender = CommonDefs.ConvertIntToEnum(typeof(EnumLingShouGender), tonumber(lingShouData.Props.MarryInfo.Gender))
        table.insert(self.m_OtherLingShouInfo, overview)
    end
end

function CLuaLingShouMasterView:InitOtherPlayeLingShouItem(item, overviewData)
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local levelLabel = item.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local evolveGradeLabel = item.transform:Find("EvolveGradeLabel"):GetComponent(typeof(UILabel))
    local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local addSprite = item.transform:Find("AddSprite"):GetComponent(typeof(UISprite))
    local battleSprite = item.transform:Find("Icon/BattleSprite"):GetComponent(typeof(UISprite))

    nameLabel.text = overviewData.name
    levelLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), overviewData.level)
    evolveGradeLabel.text = CLingShouBaseMgr.GetEvolveGradeDesc(overviewData.evolveGrade)
    icon:LoadNPCPortrait(CLingShouBaseMgr.GetLingShouIcon(overviewData.templateId))
    addSprite.gameObject:SetActive(false)

    if overviewData.id == self.m_OtherPlayerInfo.OtherInfo[1] then
        battleSprite.enabled = true
        battleSprite.spriteName = "common_fight"
    elseif overviewData.id == self.m_OtherPlayerInfo.OtherInfo[2] then
        battleSprite.enabled = true
        battleSprite.spriteName = "common_support"
    elseif overviewData.id == self.m_OtherPlayerInfo.OtherInfo[3] then
        battleSprite.enabled = true
        battleSprite.spriteName = "common_ban"
    else
        battleSprite.enabled = false
    end
end