local CScene = import "L10.Game.CScene"
local CPos = import "L10.Engine.CPos"
local Utility = import "L10.Engine.Utility"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"

LuaDuanWu2021Mgr = {}
LuaDuanWu2021Mgr.m_DragonBoatResultData = {}

function LuaDuanWu2021Mgr:IsInPlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == Duanwu2021_LongZhouSetting.GetData().GamePlayId then
           return true
        end
    end
    return false
end

function LuaDuanWu2021Mgr:ShowLongZhouPlayResultWnd(rank, time, temples, isWin)

	local list = MsgPackImpl.unpack(temples)
    local t = {0,0,0,0}
    for i=0, list.Count-1, 2 do
        local templeId = list[i]
        local count = list[i+1]
        if templeId == 39000114 then
            t[1] = t[1] + count
        elseif templeId == 39000115  then
            t[2] = t[2] + count
        elseif templeId == 39000118  then
            t[4] = t[4] + count
        else
            t[3] = t[3] + count
        end

    end
    self.m_DragonBoatResultData = {}
    self.m_DragonBoatResultData.rank = rank
    self.m_DragonBoatResultData.time = time
    self.m_DragonBoatResultData.pickupDataList = t
    self.m_DragonBoatResultData.isSuccess = isWin
    CUIManager.ShowUI(CLuaUIResources.DragonBoatResultWnd)
end

function LuaDuanWu2021Mgr:SyncLongZhouPlayPlayInfo(myRank, infoUD)

    local list = MsgPackImpl.unpack(infoUD)
    local t = {}
    for i=0, list.Count-1, 4 do
        local rank = list[i]
        local playerId = list[i+1]
        local playerName = list[i+2]
        local isArrived = list[i+3]
        table.insert(t,{rank = rank, playerId = playerId, playerName = playerName, isArrived = isArrived})

    end
    table.sort(t,function(a,b) 
        if a.rank == b.rank then return a.playerId < b.playerId end
        return a.rank < b.rank
    end)
    g_ScriptEvent:BroadcastInLua("OnSyncLongZhouPlayPlayInfo", t, myRank)
end

function LuaDuanWu2021Mgr:SyncLongZhouOperateResult(resultUD,isRoundOver)

	local list = MsgPackImpl.unpack(resultUD)
    local myTimeMS, friendTimeMS
    if CClientMainPlayer.Inst then
        for i=0, list.Count-1, 2 do
            local playerId = list[i]
            local useTimeMS = list[i+1]
            if playerId == CClientMainPlayer.Inst.Id then
                myTimeMS = useTimeMS
            else
                friendTimeMS = useTimeMS
            end

        end
        g_ScriptEvent:BroadcastInLua("OnSyncLongZhouOperateResult",myTimeMS, friendTimeMS,isRoundOver)
    end
end

function LuaDuanWu2021Mgr:ShowOffWorldPlay2TipWnd()
    local imagePaths = {
        "UI/Texture/NonTransparent/Material/dragonboat_yindao_01.mat",
        "UI/Texture/NonTransparent/Material/dragonboat_yindao_02.mat",
        "UI/Texture/NonTransparent/Material/dragonboat_yindao_03.mat",
    }
    local msgs = {
        "",
        "",
        "",
    }
    LuaImageRuleMgr:ShowCommonImageRuleWnd(imagePaths, msgs)
end

function LuaDuanWu2021Mgr:SyncLongZhouOnBoatStatus(onBoat)
    if onBoat then
        CUIManager.CloseUI(CLuaUIResources.DragonBoatControllerWnd)
        CUIManager.ShowUI(CLuaUIResources.DragonBoatControllerWnd)
        self:AddArrowListFx()
    end
end

function LuaDuanWu2021Mgr:AddArrowListFx()
    local data = Duanwu2021_LongZhouSetting.GetData().ArrowPosList
    for i = 0, data.Length - 1 do
        local x,y,angle = data[i][0],data[i][1],data[i][2]
        CEffectMgr.Inst:AddWorldPositionFX(88802351, Utility.GridPos2PixelPos(CPos(x,y)),angle,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
    end
end