local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CItemCountUpdate=import "L10.UI.CItemCountUpdate"
local UIGrid=import "UIGrid"
local CUITexture=import "L10.UI.CUITexture"
CLuaNewYear2019NianRewardExchangeWnd = class()
RegistClassMember(CLuaNewYear2019NianRewardExchangeWnd, "m_Items")
RegistClassMember(CLuaNewYear2019NianRewardExchangeWnd, "m_SelectedIndex")
RegistClassMember(CLuaNewYear2019NianRewardExchangeWnd, "m_HonorExchangeIdx")
RegistClassMember(CLuaNewYear2019NianRewardExchangeWnd, "m_HonorCoinExchangeMap")
RegistClassMember(CLuaNewYear2019NianRewardExchangeWnd, "m_XunZhangLabel")


function CLuaNewYear2019NianRewardExchangeWnd:Init()
    self.m_Items = {}
    self.m_SelectedIndex = 0
    local modelTexture = FindChild(self.transform,"ModelTexture"):GetComponent(typeof(CUITexture))
    local modelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        local prefabname = "Assets/Res/Character/Npc/lnpc534/Prefab/lnpc534_01.prefab"
        ro:LoadMain(prefabname)
        ro.Scale = 0.8
    end)
    modelTexture.mainTexture=CUIManager.CreateModelTexture("__Pig__", modelTextureLoader,210,0.05,-1,4.66,false,true,1)

    local itemTemplate = FindChild(self.transform,"ItemTemplate").gameObject
    itemTemplate:SetActive(false)

    local exchangeButton = FindChild(self.transform,"ExchangeButton").gameObject
    UIEventListener.Get(exchangeButton).onClick = DelegateFactory.VoidDelegate(function(go)
        -- Gac2Gas
        -- RequestExchangeHonorCoin
        -- RequestExchangeHonorCoinMeterial
        if self.m_SelectedIndex==0 then 
            g_MessageMgr:ShowMessage("NewYear2019_Exchange_NoSelect")
            return 
        end
        local info = self.m_HonorCoinExchangeMap[self.m_SelectedIndex]

        if info.Idx>self.m_HonorExchangeIdx+1 then--买不了
            g_MessageMgr:ShowMessage("NewYear2019_Cannot_Exchange")
            return
        end
        
        if info.Coin then
            local bought = info.Idx <= self.m_HonorExchangeIdx
            if not bought then
                Gac2Gas.RequestExchangeHonorCoin()
            else
            g_MessageMgr:ShowMessage("NewYear2019_Already_Exchange")
            end
        else
            -- print(self.m_HonorCoinExchangeMap[self.m_SelectedIndex].Key)
            local bought = self:Is2019NewYearHonorCoinMeterialExchanged(info.Key)
            if not bought then
                Gac2Gas.RequestExchangeHonorCoinMeterial(self.m_HonorCoinExchangeMap[self.m_SelectedIndex].Key)
            else
            g_MessageMgr:ShowMessage("NewYear2019_Already_Exchange")
            end
        end
    end)

    local values = FindChild(self.transform,"Values")
    local value1 = values:Find("Value1")
    local value2 = values:Find("Value2")
    local value3 = values:Find("Value3")
    local value4 = values:Find("Value4")

    self.m_XunZhangLabel = value1:GetComponent(typeof(UILabel))
    self.m_XunZhangLabel.text = CClientMainPlayer.Inst and tostring(CClientMainPlayer.Inst.PlayProp.HonorCoinCount) or "0"

    value2:GetComponent(typeof(CItemCountUpdate)):UpdateCount()
    value3:GetComponent(typeof(CItemCountUpdate)):UpdateCount()
    value4:GetComponent(typeof(CItemCountUpdate)):UpdateCount()

    local matT = {}
    NewYear2019_HonorCoinMeterialExchange.Foreach(function(k,v)
        -- v.Require
        local requireTbl = {}
        for i=1,v.Cost.Length,2 do
            local itemTempId,num = tonumber(v.Cost[i-1]),tonumber(v.Cost[i])
            requireTbl[itemTempId] = num
        end
        matT[v.Require] = { Key = k, Mat = requireTbl,Reward = v.Reward[0] }
    end)

    self.m_HonorCoinExchangeMap = {}
    NewYear2019_HonorCoinExchange.Foreach(function(k,info)
        table.insert( self.m_HonorCoinExchangeMap, { Idx = k,Coin = info.Cost, Reward = info.Reward[0]})
        if matT[k] then
            matT[k].Idx = k+1
            table.insert( self.m_HonorCoinExchangeMap, matT[k])
        end
    end)



    self.m_HonorExchangeIdx = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.HonorExchangeIdx or 0


    local parent = FindChild(self.transform,"Grid").gameObject
    for i,v in ipairs(self.m_HonorCoinExchangeMap) do
        local go = NGUITools.AddChild(parent,itemTemplate)
        go:SetActive(true)
        self:InitItem(go.transform,v)
        table.insert( self.m_Items,go )
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
            self:OnSelected(g)
        end)
    end
    parent:GetComponent(typeof(UIGrid)):Reposition()



end

function CLuaNewYear2019NianRewardExchangeWnd:OnEnable()
    g_ScriptEvent:AddListener("Sync2019HonorCoinInfo", self, "OnSync2019HonorCoinInfo")
end

function CLuaNewYear2019NianRewardExchangeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("Sync2019HonorCoinInfo", self, "OnSync2019HonorCoinInfo")
end

function CLuaNewYear2019NianRewardExchangeWnd:OnSync2019HonorCoinInfo(honorCoinCount, honorExchageIdx)
    -- print(honorCoinCount, honorExchageIdx)
    self.m_HonorExchangeIdx = honorExchageIdx
    self.m_XunZhangLabel.text = tostring(honorCoinCount)

    for i,v in ipairs(self.m_Items) do
        self:InitItem(v.transform,self.m_HonorCoinExchangeMap[i])
    end

end

function CLuaNewYear2019NianRewardExchangeWnd:OnSelected(go)
    for i,v in ipairs(self.m_Items) do
        local highlight = v.transform:Find("Highlight").gameObject
        if v==go then
            highlight:SetActive(true)
            self.m_SelectedIndex = i
        else
            highlight:SetActive(false)
        end
    end
end

function CLuaNewYear2019NianRewardExchangeWnd:OnDestroy()
    -- self.m_ModelTexture.mainTexture=nil
    CUIManager.DestroyModelTexture("__Pig__")
end

function CLuaNewYear2019NianRewardExchangeWnd:InitItem(transform,info)
    local get = transform:Find("Get").gameObject
    get:SetActive(false)
    local bg = transform:GetComponent(typeof(CUITexture))

    if info.Idx>self.m_HonorExchangeIdx+1 then
        bg.texture.alpha = 0.3
    else
        bg.texture.alpha = 1
    end

    local cost1 = transform:Find("Cost1").gameObject
    local cost2 = transform:Find("Cost2").gameObject
    local cost3 = transform:Find("Cost3").gameObject
    local cost4 = transform:Find("Cost4").gameObject

    local get = transform:Find("Get").gameObject
    if info.Coin then
        cost1:GetComponent(typeof(UILabel)).text = tostring(info.Coin)
        cost2:SetActive(false)
        cost3:SetActive(false)
        cost4:SetActive(false)
        bg:LoadMaterial("UI/Texture/Transparent/Material/newyear2019_duihuan_hong.mat")
        local bought = info.Idx <= self.m_HonorExchangeIdx
        if bought then
            get:SetActive(true)
            cost1:SetActive(false)
        else
            get:SetActive(false)
            cost1:SetActive(true)
        end

    else
        cost1:SetActive(false)
        cost2:SetActive(true)
        cost2:GetComponent(typeof(UILabel)).text = tostring(info.Mat[21020283])
        cost3:SetActive(true)
        cost3:GetComponent(typeof(UILabel)).text = tostring(info.Mat[21020284])
        cost4:SetActive(true)
        cost4:GetComponent(typeof(UILabel)).text = tostring(info.Mat[21020285])
        bg:LoadMaterial("UI/Texture/Transparent/Material/newyear2019_duihuan_huang.mat")
        local bought = self:Is2019NewYearHonorCoinMeterialExchanged(info.Key)
        if bought then
            get:SetActive(true)
            cost2:SetActive(false)
            cost3:SetActive(false)
            cost4:SetActive(false)
        else
            get:SetActive(false)
            cost2:SetActive(true)
            cost3:SetActive(true)
            cost4:SetActive(true)
        end
    end

    local icon = transform:Find("ItemCell/Icon"):GetComponent(typeof(CUITexture))
    local id = info.Reward
    local itemData = Item_Item.GetData(id)
    icon:LoadMaterial(itemData.Icon)
    local nameLabel  = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text = itemData.Name

    UIEventListener.Get(transform:Find("ItemCell").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(id, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)

end

function CLuaNewYear2019NianRewardExchangeWnd:Is2019NewYearHonorCoinMeterialExchanged(idx)
    if not CClientMainPlayer.Inst then return false end
    local v = CClientMainPlayer.Inst.PlayProp.HonorMeterialRewardSet
	return bit.band(v, bit.lshift(1, idx-1)) ~= 0
end
