local UIScrollView = import "UIScrollView"
local UIGrid = import "UIGrid"
local UITabBar = import "L10.UI.UITabBar"
local QnCheckBox = import "L10.UI.QnCheckBox"
local QnExpandListBox = import "L10.UI.QnExpandListBox"
local UIInput = import "UIInput"
local UILabel = import "UILabel"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local UITexture = import "UITexture"
local GameObject = import "UnityEngine.GameObject"
local Camera = import "UnityEngine.Camera"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local RenderTexture = import "UnityEngine.RenderTexture"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local CameraClearFlags = import "UnityEngine.CameraClearFlags"
local CMainCamera = import "L10.Engine.CMainCamera"
local CSharpResourceLoader = import "L10.Game.CSharpResourceLoader"
local Quaternion = import "UnityEngine.Quaternion"
local CWorldPositionFX = import "L10.Game.CWorldPositionFX"
local CFX = import "L10.Game.CEffectMgr+CFX"
local CPostProcessingMgr = import "L10.Engine.PostProcessing.CPostProcessingMgr"
local EPostCameraType = import "L10.Engine.PostProcessing.EPostCameraType"
local TextureWrapMode = import "UnityEngine.TextureWrapMode"
local FilterMode = import "UnityEngine.FilterMode"
local PlayerSettings = import "L10.Game.PlayerSettings"
local Screen = import "UnityEngine.Screen"
local NormalCamera = import "L10.Engine.CameraControl.NormalCamera"
local CPinchFaceCinemachineCtrlMgr = import "L10.Game.CPinchFaceCinemachineCtrlMgr"
local CCinemachineCtrl_LoginMgr = import "L10.Game.CCinemachineCtrl_LoginMgr"
local CPinchFaceHubMgr = import "L10.Game.CPinchFaceHubMgr"
local CFacialMgr = import "L10.Game.CFacialMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Texture2D = import "UnityEngine.Texture2D"
local BoxCollider = import "UnityEngine.BoxCollider"
local TextureFormat = import "UnityEngine.TextureFormat"
local UITableTween = import "L10.UI.UITableTween"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CFuxiFaceDNAMgr = import "L10.Game.CFuxiFaceDNAMgr"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CButton = import "L10.UI.CButton"
local CUIModule = import "L10.UI.CUIModule"

LuaPinchFaceHubWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPinchFaceHubWnd, "MainPreviewTexture", "MainPreviewTexture", UITexture)
RegistChildComponent(LuaPinchFaceHubWnd, "Bottom", "Bottom", GameObject)
RegistChildComponent(LuaPinchFaceHubWnd, "Right", "Right", GameObject)
RegistChildComponent(LuaPinchFaceHubWnd, "RedoBtn", "RedoBtn", GameObject)
RegistChildComponent(LuaPinchFaceHubWnd, "UndoBtn", "UndoBtn", GameObject)
RegistChildComponent(LuaPinchFaceHubWnd, "RevertBtn", "RevertBtn", GameObject)
RegistChildComponent(LuaPinchFaceHubWnd, "ZhuShiBtn", "ZhuShiBtn", GameObject)
RegistChildComponent(LuaPinchFaceHubWnd, "ShuFaBtn", "ShuFaBtn", GameObject)
RegistChildComponent(LuaPinchFaceHubWnd, "CollectBtn", "CollectBtn", GameObject)
RegistChildComponent(LuaPinchFaceHubWnd, "UncollectBtn", "UncollectBtn", GameObject)
RegistChildComponent(LuaPinchFaceHubWnd, "ApplyBtn", "ApplyBtn", GameObject)
RegistChildComponent(LuaPinchFaceHubWnd, "Content", "Content", GameObject)
RegistChildComponent(LuaPinchFaceHubWnd, "ScrollView", "ScrollView", UIScrollView)
RegistChildComponent(LuaPinchFaceHubWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaPinchFaceHubWnd, "TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaPinchFaceHubWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaPinchFaceHubWnd, "ProfessionCheckBox", "ProfessionCheckBox", QnCheckBox)
RegistChildComponent(LuaPinchFaceHubWnd, "SelectBox", "SelectBox", QnExpandListBox)
RegistChildComponent(LuaPinchFaceHubWnd, "Input", "Input", UIInput)
RegistChildComponent(LuaPinchFaceHubWnd, "SeachBtn", "SeachBtn", GameObject)
RegistChildComponent(LuaPinchFaceHubWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaPinchFaceHubWnd, "PlayerLabel", "PlayerLabel", UILabel)
RegistChildComponent(LuaPinchFaceHubWnd, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaPinchFaceHubWnd, "VoidLabel", "VoidLabel", UILabel)
RegistChildComponent(LuaPinchFaceHubWnd, "RunOutLabel", "RunOutLabel", GameObject)
RegistChildComponent(LuaPinchFaceHubWnd, "WorkCountLabel", "WorkCountLabel", GameObject)
RegistChildComponent(LuaPinchFaceHubWnd, "DeleteBtn", "DeleteBtn", GameObject)
RegistChildComponent(LuaPinchFaceHubWnd, "ReportBtn", "ReportBtn", GameObject)
RegistChildComponent(LuaPinchFaceHubWnd, "CloseBtn", "CloseBtn", GameObject)

--@endregion RegistChildComponent end

function LuaPinchFaceHubWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.RedoBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRedoBtnClick()
	end)


	
	UIEventListener.Get(self.UndoBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUndoBtnClick()
	end)


	
	UIEventListener.Get(self.RevertBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRevertBtnClick()
	end)


	
	UIEventListener.Get(self.ZhuShiBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnZhuShiBtnClick()
	end)


	
	UIEventListener.Get(self.ShuFaBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShuFaBtnClick()
	end)


	
	UIEventListener.Get(self.CollectBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCollectBtnClick()
	end)


	
	UIEventListener.Get(self.UncollectBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUncollectBtnClick()
	end)


	
	UIEventListener.Get(self.ApplyBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnApplyBtnClick()
	end)


	
	UIEventListener.Get(self.SeachBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSeachBtnClick()
	end)


	
	UIEventListener.Get(self.DeleteBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDeleteBtnClick()
	end)


	
	UIEventListener.Get(self.ReportBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReportBtnClick()
	end)


	
	UIEventListener.Get(self.CloseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseBtnClick()
	end)


    --@endregion EventBind end

    UIEventListener.Get(self.MainPreviewTexture.gameObject).onDrag = LuaUtils.VectorDelegate(function(go, delta)
		self:OnSwipe(delta)
	end)

	self.VisibilityTabBar = self.Right.transform:Find("Offset/VisibilityTabBar"):GetComponent(typeof(UITabBar))

	self.DifferentLabel = self.Right.transform:Find("Offset/DifferentLabel"):GetComponent(typeof(UILabel))
	self.DifferentLabel.text = LocalString.GetString("该数据与当前职业不同，应用后妆容将略有差异。")
end

function LuaPinchFaceHubWnd:Init()
	self.m_InitData = CFacialMgr.GenerateHubData(LuaPinchFaceMgr.m_RO, EnumToInt(LuaPinchFaceMgr.m_CurEnumClass), EnumToInt(LuaPinchFaceMgr.m_CurEnumGender),
		LuaPinchFaceMgr.m_HairId, LuaPinchFaceMgr.m_HeadId)

	-- id索引到具体的item
	self.m_Id2ItemDict = {}

	self.Template:SetActive(false)
	self:InitCamera()

	-- 设置数据
	self.m_PublicTypeList = {"hot", "weakHot", "new"}
	self.m_PageSize = 10

	self:SetSelectState(0)

	self.m_ApplyIdList = {}
	self.m_ApplyDataTbl = {}
	self.m_ApplyIndex = 0

	self:RefreshUndoBtn()

	self.ProfessionCheckBox.transform:Find("DescLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("仅显示此体型")

	-- 设置选项
	self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnTabChange(index)
    end)
    self.TabBar:ChangeTab(0, false)
	self.m_LastRequest = CServerTimeMgr.Inst.timeStamp

	UnRegisterTick(self.m_Tick)
	self.m_Tick = RegisterTick(function()
		--self.Grid:Reposition()
		local cur = CServerTimeMgr.Inst.timeStamp
		if self.ScrollView and self.ScrollView.ReachBottom and cur - self.m_LastRequest > 2 then
			self.m_LastRequest = CServerTimeMgr.Inst.timeStamp
			self:DoRequest()
		end

		-- 刷新贴图
		self:UpdateTexture()
	end, 300)

	self.SelectBox.transform:Find("BG"):GetComponent(typeof(UISprite)).height = 215
end

function LuaPinchFaceHubWnd:DoRequest(clear)
	-- 清除同时代表了加载
	if clear then
		self:OnPinchFaceHubWorkListUpdate(clear)
	end

	if self.m_TabIndex == 0 then
		if self.m_SearchKey and self.m_SearchKey ~= "" then
			CPinchFaceHubMgr.Inst:SearchWork(self.m_SearchKey, LuaPinchFaceMgr.m_CurEnumGender, LuaPinchFaceMgr.m_CurEnumClass, self.m_IsAllJob)
		else
			CPinchFaceHubMgr.Inst:GetPublicList(LuaPinchFaceMgr.m_CurEnumGender, LuaPinchFaceMgr.m_CurEnumClass, self.m_IsAllJob, 
			self.m_PublicTypeList[self.m_PublicTypeIndex])
		end
	elseif self.m_TabIndex == 1 then
		CPinchFaceHubMgr.Inst:GetCollectList()
	else
		CPinchFaceHubMgr.Inst:GetSelfList()
	end
end

function LuaPinchFaceHubWnd:OnTabChange(index)
	self:ClearCurList()
	-- 刷一下动画
	self.Grid.transform:GetComponent(typeof(UITableTween)):PlayAnim()

	self.m_TabIndex = index

	self:HideItem(index)
	self:SetSelectState(0)

	if index == 0 then
		self:InitPublic()
	elseif index == 1 then
		self:InitCollect()
	else
		self:InitSelf()
	end
end

function LuaPinchFaceHubWnd:HideItem(index)
	self.ProfessionCheckBox.gameObject:SetActive(true)
	self.SelectBox.gameObject:SetActive(index == 0)
	self.Input.gameObject:SetActive(index == 0)

	self.WorkCountLabel.gameObject:SetActive(index == 2 or index == 1)
	--self.VoidLabel.gameObject:SetActive(false)
	self.RunOutLabel.gameObject:SetActive(false)

	self.CollectBtn.gameObject:SetActive(index == 0)
	self.UncollectBtn.gameObject:SetActive(index == 1)
	self.DeleteBtn.gameObject:SetActive(index == 2)
	self.ApplyBtn.gameObject:SetActive(true)

	self.ReportBtn.gameObject:SetActive(index ~= 2)
end

function LuaPinchFaceHubWnd:SetSelectState(id)
	local showBtn = id ~= 0
	self.m_CurrentSelectWorkId = id

	local index = self.m_TabIndex
	self.CollectBtn.gameObject:SetActive(index == 0 and showBtn)
	self.UncollectBtn.gameObject:SetActive(index == 1 and showBtn)
	self.DeleteBtn.gameObject:SetActive(index == 2 and showBtn)
	self.ApplyBtn.gameObject:SetActive(showBtn)
end

function LuaPinchFaceHubWnd:Update()
	if self.m_LastView and self.m_ZhuShiBtn and self.m_ShuFaBtn then
		self.m_ZhuShiBtn.Selected = self.m_LastView.ZhuShiButton.Selected
		self.m_ShuFaBtn.Selected = self.m_LastView.ShuFaButton.Selected
	end
end

-- 获取当前需要显示的列表
function LuaPinchFaceHubWnd:GetCurrentShowList()
	local index = self.TabBar.SelectedIndex
	local isSearch = false
	if self.m_SearchKey and self.m_SearchKey ~= "" then
		isSearch = true
	end

	if index == 0 and not isSearch then
		return CPinchFaceHubMgr.Inst.m_PublicIdList
	elseif index == 0 and isSearch then
		return CPinchFaceHubMgr.Inst.m_SearchIdList
	elseif index== 1 then
		return CPinchFaceHubMgr.Inst.m_CollectIdList
	else
		return CPinchFaceHubMgr.Inst.m_SelfIdList
	end
end

-- 获取当前需要显示的列表
function LuaPinchFaceHubWnd:GetCurrentItemNum()
	local i = 0
	if self.m_Id2ItemDict then
		for k,v in pairs(self.m_Id2ItemDict) do
			i = i + 1
		end
	end
	return i
end

-- CPinchFaceWork:wordData,picData,pinchFaceData
--  ulong id;
--  string shareId;
--  ulong createTime;
--  int hot;
--  int likeCount;
--  int collectCount;
--  int applyCount;
--  string dataUrl;
--  string image;
--  string title;
--  string desc;
--  string roleName;
--  int gender;
--  int visibility;
--  int workType;
--  int jobId;
--  bool isLiked;
--  bool isCollected;
--  bool onceApplied;
function LuaPinchFaceHubWnd:InitItem(id, item, work)
	local texture = item.transform:Find("Tetxure"):GetComponent(typeof(UITexture))
	local highlight = item.transform:Find("Highlight").gameObject
	local numLabel = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
	local titleLabel = item.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
	local collectMark = item.transform:Find("CollectMark/Collect").gameObject

	if texture.mainTexture == nil then
		local tex = Texture2D(228, 171, TextureFormat.RGB24, false)
		tex.wrapMode = TextureWrapMode.Clamp
		texture.mainTexture = tex
	end

	-- 设置图片
	if work.picData then
		CommonDefs.LoadImage(texture.mainTexture, work.picData)
		texture.mainTexture.wrapMode = TextureWrapMode.Clamp
	else
		-- 没有图片信息的话 需要请求
		work:RequestPicData()
	end
	
	local workData = work.workData
	titleLabel.text = workData.title
	numLabel.text = tostring(workData.hot)
	collectMark:SetActive(workData.isCollected)

	-- 设置高亮
	if self.m_CurrentSelectWorkId == id then
		self:OnDisHightlight()
		highlight:SetActive(true)
		self.m_LastSelectItem = highlight

		-- 直接调用点击函数
		self:OnItemClick(id, item, work)
	else
		highlight:SetActive(false)
	end

	UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnItemClick(id, item, work)
	end)
end

function LuaPinchFaceHubWnd:UpdateTexture()
	-- 用来更新
	if self.m_Id2ItemDict then
		for id, item in pairs(self.m_Id2ItemDict) do
			local texture = item.transform:Find("Tetxure"):GetComponent(typeof(UITexture))
			if texture.isVisible and texture.mainTexture == nil then
				-- 重新初始化
				local work = CPinchFaceHubMgr.Inst.m_IdWorkDict[id]
				if work then
					self:InitItem(id, item, work)
				end
			elseif (not texture.isVisible) and texture.mainTexture ~= nil then
				GameObject.Destroy(texture.mainTexture)
				texture.mainTexture = nil
			end
		end
	end
end

function LuaPinchFaceHubWnd:OnItemClick(id, item, work)
	self:OnDisHightlight()

	local highlight = item.transform:Find("Highlight").gameObject
	-- 设置高亮
	highlight:SetActive(true)
	self.m_LastSelectItem = highlight

	-- 当前选中
	self:SetSelectState(id)
	self:ShowWorkDesc(id)

	if work.pinchFaceData == nil then
		-- 没有数据的话 需要请求
		work:RequestPinchFaceData(nil)
	end

	if self.TabBar.SelectedIndex == 0 then
		if work.workData.isCollected then
			self.CollectBtn.gameObject:SetActive(false)
			self.UncollectBtn.gameObject:SetActive(true)
		else
			self.CollectBtn.gameObject:SetActive(true)
			self.UncollectBtn.gameObject:SetActive(false)
		end
	end

	self:PreviewWork()
end

function LuaPinchFaceHubWnd:ShowWorkDesc(id)
	local work = nil
	if id and id > 0 then
		work = CPinchFaceHubMgr.Inst.m_IdWorkDict[id]
	end

	self.VisibilityTabBar.gameObject:SetActive(false)

	if work then
		self.Right:SetActive(true)

		-- 显示描述
		self.TitleLabel.text = work.workData.title
		if work.workData.roleName and work.workData.roleName ~= "" then
			self.PlayerLabel.text = SafeStringFormat3(LocalString.GetString("作者：%s"), work.workData.roleName)
		else
			self.PlayerLabel.text = ""
		end
		self.DescLabel.text = work.workData.desc

		if self.m_TabIndex == 2 then
			self.VisibilityTabBar.gameObject:SetActive(true)
			-- 0公开 1私有 正好对应tab
			self.VisibilityTabBar:ChangeTab(work.workData.visibility, true)
			self.VisibilityTabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
				self:SetWorkVisibility(id, index)
			end)
		end


		local workData = work.workData
		-- 职业是否相同
		local diff = self:CheckIsMatchCurData(workData.jobId, workData.gender) and not self:CheckIsSameJob(workData.jobId, workData.gender)
		self.DifferentLabel.gameObject:SetActive(diff)
	else
		self.Right:SetActive(false)
	end
end

function LuaPinchFaceHubWnd:SetWorkVisibility(id, index)
	local work = nil
	if id and id > 0 then
		work = CPinchFaceHubMgr.Inst.m_IdWorkDict[id]
	end

	if work then
		CPinchFaceHubMgr.Inst:UpdateVisibility(id, index, 
			DelegateFactory.Action(function()
				CPinchFaceHubMgr.Inst:ClearPublic()
				g_MessageMgr:ShowMessage("PinchFaceHub_UpdateVisibility_Success")
		end), DelegateFactory.Action(function()
			g_MessageMgr:ShowMessage("PinchFaceHub_UpdateVisibility_Fail")
		end))
	end
end

function LuaPinchFaceHubWnd:OnDisHightlight()
	if self.m_LastSelectItem then
		self.m_LastSelectItem:SetActive(false)
		self.m_LastSelectItem = nil
	end
end

function LuaPinchFaceHubWnd:InitPublic()
	CPinchFaceHubMgr.Inst:ClearPublic()
	
	-- 设置标题
	local selectNames = CreateFromClass(MakeGenericClass(List, String))
	CommonDefs.ListAdd(selectNames, typeof(String), LocalString.GetString("总热度"))
	CommonDefs.ListAdd(selectNames, typeof(String), LocalString.GetString("周热度"))
	CommonDefs.ListAdd(selectNames, typeof(String), LocalString.GetString("最新发布"))

	self.m_PublicTypeIndex = 1
    -- 设置下拉菜单
    self.SelectBox:SetItems(selectNames)
    self.SelectBox.ClickCallback = DelegateFactory.Action_int(function(index)
		self.m_PublicTypeIndex = index + 1
		self:SetSelectState(0)
		CPinchFaceHubMgr.Inst:ClearPublic()
		self:DoRequest(true)
    end)
	self.SelectBox:SetIndex(0, false)

	self.m_IsAllJob = false
	-- 设置职业
	self.ProfessionCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (value)
		self.m_IsAllJob = not value
		self:SetSelectState(0)
		CPinchFaceHubMgr.Inst:ClearPublic()
		self:DoRequest(true)
	end)
	self.ProfessionCheckBox:SetSelected(true, true)

	-- 前面两处就不trigger了 这里手动发一下请求
	self:ClearCurList()
	self:DoRequest(true)
end

function LuaPinchFaceHubWnd:InitSelf()
	self.m_IsAllJob = false
	-- 设置职业
	self.ProfessionCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (value)
		self.m_IsAllJob = not value
		CPinchFaceHubMgr.Inst:ClearSelf()
		self:DoRequest(true)
	end)
	self.ProfessionCheckBox:SetSelected(true, true)

	self:DoRequest(true)
end

function LuaPinchFaceHubWnd:InitCollect()
	self.m_IsAllJob = false
	-- 设置职业
	self.ProfessionCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (value)
		self.m_IsAllJob = not value
		CPinchFaceHubMgr.Inst:ClearCollect()
		self:DoRequest(true)
	end)
	self.ProfessionCheckBox:SetSelected(true, true)

	self:DoRequest(true)
end

function LuaPinchFaceHubWnd:InitCamera()
	self.m_Camera = GameObject.Find("__PinchFaceModel__/PinchFaceWndCCinemachineCtrl"):GetComponent(typeof(Camera))
	self:UpdateModelTexture()

	self.transform:Find("_BgMask_").gameObject:SetActive(false)
end

function LuaPinchFaceHubWnd:OnScreenChange()
	self:UpdateModelTexture()
end

-- 用来更新RT的尺寸
function LuaPinchFaceHubWnd:UpdateModelTexture()
	self.MainPreviewTexture.mainTexture = self.m_Camera.targetTexture
	self.MainPreviewTexture.gameObject.transform.localPosition = Vector3(0, 0, 0)
	self.MainPreviewTexture.depth = 1
end

function LuaPinchFaceHubWnd:OnSwipe(delta)
	local deltaVal= - delta.x / Screen.width * 360
	self:RotateRoleModel(deltaVal)
end

-- 旋转模型
function LuaPinchFaceHubWnd:RotateRoleModel(degree)
	if not LuaPinchFaceMgr.m_RO then return end
    local t = LuaPinchFaceMgr.m_RO.transform
    if t and t.transform.gameObject.activeSelf then
		local newEulerAngleY = (t.transform.localEulerAngles.y + degree + 180) % 360 - 180
		if newEulerAngleY ~= t.transform.localEulerAngles.y then
			t.transform.localEulerAngles = Vector3(0, newEulerAngleY, 0)
			self:OnSetPinchFaceIKLookAtTo()
		end
    end
end

-- 调用IK
function LuaPinchFaceHubWnd:OnSetPinchFaceIKLookAtTo()
	if not LuaPinchFaceMgr.m_RO then return end
	local cameraObj = self.m_Camera
	local t = LuaPinchFaceMgr.m_IsUseIKLookAt and self.m_IKTarget or nil
	if not self:CheckCameraPosForIK() then
		t = nil
	end
	if LuaPinchFaceMgr.m_IsUseIKLookAt and self.m_Target == t then
		return
	end
	LuaPinchFaceMgr.m_RO:SetLookAtIKTarget(t, true, false)
	self.m_Target = t
end

function LuaPinchFaceHubWnd:CheckCameraPosForIK()
	if not LuaPinchFaceMgr.m_IsUseIKLookAt or nil == LuaPinchFaceMgr.m_RO then
		return false
	end
	local t = LuaPinchFaceMgr.m_RO.gameObject
    if t ~= nil and t.gameObject.activeSelf then
		local yAngle = t.transform.localEulerAngles.y
		local cameraObj = self.m_Camera
		local targetDir = CommonDefs.op_Subtraction_Vector3_Vector3(cameraObj.transform.position, LuaPinchFaceMgr.m_RO.transform.position)
		local conbined = (Vector3.Lerp(LuaPinchFaceMgr.m_RO:GetSlotTransform("Bip001 Pelvis").up, LuaPinchFaceMgr.m_RO.transform.forward, NormalCamera.m_lerpFactor)).normalized
		local pTocAngle = Vector3.Angle(targetDir, conbined)
		return pTocAngle <= 120 and (yAngle >= 90 and yAngle <= 270)
    end
	return false
end

function LuaPinchFaceHubWnd:ClearCurList()
	for k, item in pairs(self.m_Id2ItemDict) do
		local texture = item.transform:Find("Tetxure"):GetComponent(typeof(UITexture))
		if texture.mainTexture then
			GameObject.Destroy(texture.mainTexture)
			texture.mainTexture = nil
		end
	end
	Extensions.RemoveAllChildren(self.Grid.transform)
	self.m_Id2ItemDict = {}
	self.m_LastSelectItem = nil
	self.ScrollView:ResetPosition()
	self.Grid:Reposition()
	self:ShowWorkDesc(nil)
end

function LuaPinchFaceHubWnd:OnPinchFaceHubWorkListUpdate(bForceClear)
	local cur = self:GetCurrentItemNum()
	local dict = self:GetCurrentShowList()

	-- 列表更新了
	if bForceClear or cur > dict.Count or cur == 0 then
		self:ClearCurList()
		bForceClear = true
	end

	CommonDefs.ListIterate(dict, DelegateFactory.Action_object(function (id)
		if self.m_Id2ItemDict[id] then
			return
		end

		local work = CPinchFaceHubMgr.Inst.m_IdWorkDict[id]

		-- 过滤掉不符合条件的
		local check = true
		if (not self.m_IsAllJob) and work then
			if not self:CheckIsMatchCurData(work.workData.jobId, work.workData.gender) then
				check = false
			end
		end

		if work and check then
			local g = NGUITools.AddChild(self.Grid.gameObject, self.Template)
			g:SetActive(true)
			self.m_Id2ItemDict[id] = g

			self:InitItem(id, g, work)
		end
	end))

	-- 显示数量 可能是自己的或者是收藏
	local usage = CPinchFaceHubMgr.Inst.m_SelfUsage
	if self.m_TabIndex == 1 then
		usage = CPinchFaceHubMgr.Inst.m_CollectUsage
	end
	self.WorkCountLabel:GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("数量 %d/%d"), usage.current, usage.max)

	self.Grid:Reposition()
	if bForceClear then
		self.ScrollView:ResetPosition()
	end

	self.VoidLabel.enabled = self:GetCurrentItemNum() == 0
end

function LuaPinchFaceHubWnd:RefreshId(id)
	-- 作品更新了
	local item = self.m_Id2ItemDict[id]
	if item then
		self:InitItem(id, item, CPinchFaceHubMgr.Inst.m_IdWorkDict[id])
	end
end

function LuaPinchFaceHubWnd:OnPinchFaceHubWorkUpdate(args)
	-- 作品更新了
	local id = args[0]
	self:RefreshId(id)
end

function LuaPinchFaceHubWnd:OnPinchFaceHubWorkPicLoadFinished(args)
	-- 图片加载完成了
	local id = args[0]
	self:RefreshId(id)
end

function LuaPinchFaceHubWnd:ApplyPinchFaceData(data, isRecord)
	local data = CFacialMgr.ParseHubData(data)
	LuaPinchFaceMgr:ReadPinchFaceHubData(data, isRecord)
end

function LuaPinchFaceHubWnd:OnPinchFaceHubWorkDataLoadFinished(args)
	-- 数据加载完成了
	local id = args[0]
	if id == self.m_ApplyingWordId then
		self.m_ApplyingWordId = 0
		self:ApplyPinchFaceData(CPinchFaceHubMgr.Inst.m_IdWorkDict[id].pinchFaceData)
	end
end

function LuaPinchFaceHubWnd:RefreshUndoBtn()
	-- 数据加载完成了
	-- 前进
	CUICommonDef.SetActive(self.RedoBtn.gameObject, self.m_ApplyIndex >=0  and self.m_ApplyIndex < #self.m_ApplyIdList, true)
	-- 撤销
	CUICommonDef.SetActive(self.UndoBtn.gameObject, self.m_ApplyIndex >=2 or (self.m_ApplyIndex == -1 and #self.m_ApplyIdList >0), true)
end

function LuaPinchFaceHubWnd:OnSetPinchFaceBlurTexVisible(visible)
	self:UpdateModelTexture()
end

function LuaPinchFaceHubWnd:UpdateApplyIndex(index, isRecord)
	local id = self.m_ApplyIdList[index]

	if index >= 0 then
		self.m_ApplyIndex = index
		self.m_IsPreview = false
	else
		self.m_IsPreview = true
	end

	if id and id ~= 0 then
		local data = self.m_ApplyDataTbl[id]
		self:ApplyPinchFaceData(data, isRecord)
	elseif index == 0 and self.m_InitData then
		self:ApplyPinchFaceData(self.m_InitData, isRecord)
	end
	self:RefreshUndoBtn()
end

function LuaPinchFaceHubWnd:CheckIsMatchCurData(job, gender)
    local initializationData = LuaPinchFaceMgr:GetInitializationData()
    local newIData = Initialization_Init.GetData(job * 100 + gender)
    if newIData then
        if not initializationData then return false end
        if initializationData.StdModelName ~= newIData.StdModelName then return false end
    end
    return true
end

function LuaPinchFaceHubWnd:CheckIsSameJob(job, gender)
    return EnumToInt(LuaPinchFaceMgr.m_CurEnumGender) == gender and  EnumToInt(LuaPinchFaceMgr.m_CurEnumClass) == job
end

function LuaPinchFaceHubWnd:PreviewWork()
	self:UpdateApplyIndex(-1)

	local work = nil
	if self.m_CurrentSelectWorkId > 0 then
		work = CPinchFaceHubMgr.Inst.m_IdWorkDict[self.m_CurrentSelectWorkId]
	end

	if work then
		local workData = work.workData
		if self:CheckIsMatchCurData(workData.jobId, workData.gender) then
			if work.pinchFaceData ~= nil then
				-- 应用数据
				self:ApplyPinchFaceData(work.pinchFaceData)
			else
				work:RequestPinchFaceData(nil)
				self.m_ApplyingWordId = self.m_CurrentSelectWorkId
			end
		else
			g_MessageMgr:ShowMessage("PinchFaceHub_Apply_Not_Match")
		end
	end
end

function LuaPinchFaceHubWnd:OnEnable()
	CPinchFaceHubMgr.Inst:Clear()
	local pinchWnd = CUIManager.instance.loadedUIs[CLuaUIResources.PinchFaceWnd]
	if pinchWnd then
		pinchWnd:GetComponent(typeof(UIPanel)).alpha = 0
	end

	self.m_LastView = pinchWnd.transform:Find("PinchFaceMainView"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
	self.m_ZhuShiBtn = self.ZhuShiBtn:GetComponent(typeof(CButton))
	self.m_ShuFaBtn = self.ShuFaBtn:GetComponent(typeof(CButton))

	self.m_ZhuShiBtn.Selected = self.m_LastView.ZhuShiButton.Selected
	self.m_ShuFaBtn.Selected = self.m_LastView.ShuFaButton.Selected

	self.ZhuShiBtn:GetComponent(typeof(UISprite)).depth = 3
	self.ShuFaBtn:GetComponent(typeof(UISprite)).depth = 3

	if CPinchFaceCinemachineCtrlMgr.Inst then
		CPinchFaceCinemachineCtrlMgr.Inst:SetScreenOffsetXOffset(0.1)
	end
	self.m_Cache = LuaPinchFaceMgr.m_CanCacheOfflineData
	LuaPinchFaceMgr.m_CanCacheOfflineData = false

	g_ScriptEvent:AddListener("OnScreenChange", self, "OnScreenChange")
	g_ScriptEvent:AddListener("PinchFaceHubWorkListUpdate", self, "OnPinchFaceHubWorkListUpdate")
	g_ScriptEvent:AddListener("PinchFaceHubWorkUpdate", self, "OnPinchFaceHubWorkUpdate")
	g_ScriptEvent:AddListener("PinchFaceHubWorkPicLoadFinished", self, "OnPinchFaceHubWorkPicLoadFinished")
	g_ScriptEvent:AddListener("PinchFaceHubWorkDataLoadFinished", self, "OnPinchFaceHubWorkDataLoadFinished")
	g_ScriptEvent:AddListener("OnSetPinchFaceBlurTexVisible", self, "OnSetPinchFaceBlurTexVisible")
end

function LuaPinchFaceHubWnd:OnDisable()
	if CommonDefs.DictContains(CUIManager.instance.loadedUIs, typeof(CUIModule), CLuaUIResources.PinchFaceWnd) then
		local pinchWnd = CUIManager.instance.loadedUIs[CLuaUIResources.PinchFaceWnd]
		if pinchWnd then
			pinchWnd:GetComponent(typeof(UIPanel)).alpha = 1
		end
	end

    --CPinchFaceCinemachineCtrlMgr.Inst:ShowCam(self.m_LastCameraIndex)
	self:ClearCurList()
	CPinchFaceHubMgr.Inst:Clear()

	-- 恢复了设置 或者没有应用过数据
	if self.m_ApplyIndex == 0 or #self.m_ApplyIdList == 0 then
		self:UpdateApplyIndex(0)
	elseif self.m_ApplyIndex == -1 then
		-- 预览的话 使用最后一个应用的数据
		self:UpdateApplyIndex(#self.m_ApplyIdList)
	else
		self:UpdateApplyIndex(self.m_ApplyIndex)
	end

	LuaPinchFaceMgr.m_CanCacheOfflineData = self.m_Cache

	UnRegisterTick(self.m_Tick)
	UnRegisterTick(self.m_DelayTick)
	if CPinchFaceCinemachineCtrlMgr.Inst then
		CPinchFaceCinemachineCtrlMgr.Inst:SetScreenOffsetXOffset(-0.1)
	end
	g_ScriptEvent:RemoveListener("OnScreenChange", self, "OnScreenChange")
	g_ScriptEvent:RemoveListener("PinchFaceHubWorkListUpdate", self, "OnPinchFaceHubWorkListUpdate")
	g_ScriptEvent:RemoveListener("PinchFaceHubWorkUpdate", self, "OnPinchFaceHubWorkUpdate")
	g_ScriptEvent:RemoveListener("PinchFaceHubWorkPicLoadFinished", self, "OnPinchFaceHubWorkPicLoadFinished")
	g_ScriptEvent:RemoveListener("PinchFaceHubWorkDataLoadFinished", self, "OnPinchFaceHubWorkDataLoadFinished")
	g_ScriptEvent:RemoveListener("OnSetPinchFaceBlurTexVisible", self, "OnSetPinchFaceBlurTexVisible")
end

function LuaPinchFaceHubWnd:CacheLastApplyData()
	local lastDataIndex = 0
	if self.m_ApplyIndex == 0 or #self.m_ApplyIdList == 0 then
		lastDataIndex = 0 
	elseif self.m_ApplyIndex == -1 then
		-- 预览的话 使用最后一个应用的数据
		lastDataIndex = #self.m_ApplyIdList
	else
		lastDataIndex = self.m_ApplyIndex
	end
	local id = self.m_ApplyIdList[lastDataIndex]
	local data = self.m_InitData
	if id and id ~= 0 then
		data = self.m_ApplyDataTbl[id]
	end
	print("CacheLastApplyData", lastDataIndex, id, data)
	local cachedata = CFacialMgr.ParseHubData(data)
	LuaPinchFaceMgr.m_CurFacialData = CFacialMgr.GetCustomFacialData(cachedata.FaceDnaData, cachedata.PartDatas)
	print("CacheLastApplyData", cachedata.FaceDnaData, cachedata.PartDatas, LuaPinchFaceMgr.m_CurFacialData)
end
--@region UIEvent

function LuaPinchFaceHubWnd:OnRedoBtnClick()
	-- 前进 使用下一个应用的数据
	if self.m_ApplyIndex >=0  and self.m_ApplyIndex < #self.m_ApplyIdList then
		self:UpdateApplyIndex(self.m_ApplyIndex + 1)
	end
end

function LuaPinchFaceHubWnd:OnUndoBtnClick()
	-- 撤销
	-- 在预览未应用妆容时 撤销到最后一个应用的妆容
	-- 在预览应用妆容时 撤销到上一个应用的妆容
	local id =0
	if self.m_ApplyIndex == -1 then
		-- 应用最后一个
		self:UpdateApplyIndex(#self.m_ApplyIdList)
	elseif self.m_ApplyIndex >= 2 then
		-- 应用上一个
		self:UpdateApplyIndex(self.m_ApplyIndex-1)
	end
end

function LuaPinchFaceHubWnd:OnRevertBtnClick()
	self:UpdateApplyIndex(0)
end

function LuaPinchFaceHubWnd:OnZhuShiBtnClick()
	self.m_LastView:OnZhuShiButtonClick()
	self.ZhuShiBtn:GetComponent(typeof(CButton)).Selected = self.m_LastView.ZhuShiButton.Selected
	self.ShuFaBtn:GetComponent(typeof(CButton)).Selected = self.m_LastView.ShuFaButton.Selected
end

function LuaPinchFaceHubWnd:OnShuFaBtnClick()
	self.m_LastView:OnShuFaButtonClick()
	self.ZhuShiBtn:GetComponent(typeof(CButton)).Selected = self.m_LastView.ZhuShiButton.Selected
	self.ShuFaBtn:GetComponent(typeof(CButton)).Selected = self.m_LastView.ShuFaButton.Selected
end

function LuaPinchFaceHubWnd:OnCollectBtnClick()
	if self.m_CurrentSelectWorkId > 0 then
		local usage = CPinchFaceHubMgr.Inst.m_CollectUsage
		if usage.current ~= 0 and usage.max ~= 0 and usage.max <= usage.current then
			g_MessageMgr:ShowMessage("PinchFaceHub_Collect_Usage_RunOut")
		else
			CPinchFaceHubMgr.Inst:ClearCollect()
			CPinchFaceHubMgr.Inst:CollectWork(self.m_CurrentSelectWorkId, nil, nil)
		end
	end
end

function LuaPinchFaceHubWnd:OnUncollectBtnClick()
	if self.m_CurrentSelectWorkId > 0 then
		CPinchFaceHubMgr.Inst:ClearCollect()
		CPinchFaceHubMgr.Inst:CancelCollectWork(self.m_CurrentSelectWorkId, nil, nil)
	end
end

function LuaPinchFaceHubWnd:OnApplyBtnClick()
	local id = self.m_CurrentSelectWorkId
	local work = nil
	if self.m_CurrentSelectWorkId > 0 then
		work = CPinchFaceHubMgr.Inst.m_IdWorkDict[self.m_CurrentSelectWorkId]
	end

	if work then
		local workData = work.workData
		if self:CheckIsMatchCurData(workData.jobId, workData.gender) then
			if work.pinchFaceData ~= nil then
				self:CacheLastApplyData()
				CFuxiFaceDNAMgr.Inst:ClearFuxiLogState()
				-- 发送我应用了一次的消息
				CPinchFaceHubMgr.Inst:ApplyWork(self.m_CurrentSelectWorkId, nil, nil)
				-- 数据加载完成了
				table.insert(self.m_ApplyIdList, id)
				self.m_ApplyDataTbl[id] = work.pinchFaceData
				self:UpdateApplyIndex(#self.m_ApplyIdList, true)
				g_MessageMgr:ShowMessage("PinchFaceHub_Apply_Success")
			else
				g_MessageMgr:ShowMessage("PinchFaceHub_Apply_Data_Not_Load_Finished")
			end
		else
			g_MessageMgr:ShowMessage("PinchFaceHub_Apply_Not_Match")
		end
	end
end

function LuaPinchFaceHubWnd:OnSeachBtnClick()
	-- 搜索
	if self.m_SearchKey == self.Input.value then
		return
	end

	local input = self.Input.value
	if input == "" or #input <= 0 then
		self.m_SearchKey = ""
		CPinchFaceHubMgr.Inst:ClearSearch()
		self:DoRequest(true)
		return
	end

	if CommonDefs.StringLength(input) < 2 then
		g_MessageMgr:ShowMessage("PinchFaceHub_Apply_Search_Limit")
		return
	end

	local check = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(self.Input.value, false)
    if check.msg == nil or check.shouldBeIgnore then 
        g_MessageMgr:ShowMessage("Speech_Violation")
        return
    end

	self.m_SearchKey = self.Input.value
	CPinchFaceHubMgr.Inst:ClearSearch()
	self:DoRequest(true)
end

function LuaPinchFaceHubWnd:OnDeleteBtnClick()
	if self.m_CurrentSelectWorkId > 0 then
		g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("PinchFaceHub_Delete_Confrim"), function()
			CPinchFaceHubMgr.Inst:DeleteWork(self.m_CurrentSelectWorkId, DelegateFactory.Action(function()
				g_MessageMgr:ShowMessage("PinchFaceHub_Delete_Success")
				-- 主动清一下
				self:OnPinchFaceHubWorkListUpdate(true)
			end), nil)
		end, nil, nil, nil, false)
	end
end

function LuaPinchFaceHubWnd:OnReportBtnClick()
	local work = nil
	if self.m_CurrentSelectWorkId > 0 then
		work = CPinchFaceHubMgr.Inst.m_IdWorkDict[self.m_CurrentSelectWorkId]
	end

	if work ~= nil and work.workData ~= nil then
		LuaPinchFaceMgr:ShowReportWnd(work.workData.roleId, work.workData.title, work.workData.shareId)
	end
end

function LuaPinchFaceHubWnd:OnCloseBtnClick()
	if self.m_IsPreview and #self.m_ApplyIdList == 0 then
		g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("PinchFaceHub_Exit_Makesure"), function()
			CUIManager.CloseUI(CLuaUIResources.PinchFaceHubWnd)
		end, nil, nil, nil, false)
	else
		CUIManager.CloseUI(CLuaUIResources.PinchFaceHubWnd)
	end
end


--@endregion UIEvent

