local CWelfareBonusMgr = import "L10.Game.CWelfareBonusMgr"
local CSigninMgr=import "L10.Game.CSigninMgr"
local EventManager = import "EventManager"
local EnumEventType=import "EnumEventType"
local Time = import "UnityEngine.Time"

if rawget(_G, "CLuaEntryUIMgr") then
    g_ScriptEvent:RemoveListener("ShowUIPreDraw", CLuaEntryUIMgr, "OnChangeView")
    g_ScriptEvent:RemoveListener("GasDisconnect", CLuaEntryUIMgr, "OnGasDisconnect")
    g_ScriptEvent:RemoveListener("MainPlayerDestroyed", CLuaEntryUIMgr, "OnMainPlayerDestroyed")
end
CLuaEntryUIMgr = {}
CLuaEntryUIMgr.m_UIs = {}
CLuaEntryUIMgr.m_LastLoginPlayerId = 0
CLuaEntryUIMgr.m_LastOpenGreenHandGiftWndTime = 0
CLuaEntryUIMgr.m_ShowGreenHandGiftWndTimeInterval = 600
g_ScriptEvent:AddListener("ShowUIPreDraw", CLuaEntryUIMgr, "OnChangeView")
g_ScriptEvent:AddListener("GasDisconnect", CLuaEntryUIMgr, "OnGasDisconnect")
g_ScriptEvent:AddListener("MainPlayerDestroyed", CLuaEntryUIMgr, "OnMainPlayerDestroyed")

function CLuaEntryUIMgr.OnChangeView(self,args)
    local show,name=args[0],args[1]
    CLuaEntryUIMgr.m_UIs[name] = nil--保证不会重复打开一个界面
    if not show and CLuaEntryUIMgr.m_Priority[name] then--关闭界面的时候
        --这里可能并不是真的关掉界面 调用CUIManager.CloseUI的时候就会调过来
        RegisterTickOnce(function()
            CLuaEntryUIMgr.PopUI()
        end,30)
    end
end
function CLuaEntryUIMgr.OnMainPlayerDestroyed(self,args)
    CLuaEntryUIMgr.m_UIs = {}--清空
end
function CLuaEntryUIMgr.OnGasDisconnect(self,args)
    CLuaEntryUIMgr.m_UIs = {}--清空
end

CLuaEntryUIMgr.m_Priority = {
    ["HuiliuWnd"] = 1,--回流豪礼界面
    ["GreenHandGiftWnd"] = 2,--新手好礼界面
    ["WelfareWnd"] = 3,
    ["NewActivityWnd"] = 4,
    ["HouseChristmasGiftWnd"] = 5,--家园圣诞掉落界面
}

function CLuaEntryUIMgr.PushUI(module)
    -- print("push ui",module.Name)
    CLuaEntryUIMgr.m_UIs[module.Name] = module
end

function CLuaEntryUIMgr.PopUI()
    -- print("pop ui")
    local priority = 999
    local module = nil
    for k,v in pairs(CLuaEntryUIMgr.m_UIs) do
        if CLuaEntryUIMgr.m_Priority[k]<priority then
            priority=CLuaEntryUIMgr.m_Priority[k]
            module = v
        end
    end
    if module then
        CUIManager.ShowUI(module)
        CLuaEntryUIMgr.m_UIs[module.Name] = nil
    end
end

function CLuaEntryUIMgr.RemoveUI(module)
    CLuaEntryUIMgr.m_UIs[module.Name] = nil
end

--检查是否当前运行显示新手礼包窗口
function CLuaEntryUIMgr.CheckCanShowGreenHandGiftWnd()
    if CClientMainPlayer.Inst then
        if CClientMainPlayer.Inst.Id~=CLuaEntryUIMgr.m_LastLoginPlayerId then --切换了角色，可以显示
            return true
        else --超过了限定的时间间隔，可以显示
            return Time.realtimeSinceStartup - CLuaEntryUIMgr.m_LastOpenGreenHandGiftWndTime > CLuaEntryUIMgr.m_ShowGreenHandGiftWndTimeInterval
        end
    else
        return false
    end
end

function CLuaEntryUIMgr.UpdateCanShowGreenHandGiftWndCondition()
    if CClientMainPlayer.Inst then
        CLuaEntryUIMgr.m_LastLoginPlayerId = CClientMainPlayer.Inst.Id
    end
    CLuaEntryUIMgr.m_LastOpenGreenHandGiftWndTime = Time.realtimeSinceStartup
end

--OnMainPlayerCreated之后就尝试显示
function CLuaEntryUIMgr.OnMainPlayerCreated()
    -- print("OnMainPlayerCreated")
    --栈中有界面就显示
    if CWelfareBonusMgr.Inst.showGreenHandGiftWnd and CLuaEntryUIMgr.CheckCanShowGreenHandGiftWnd() then
        CWelfareBonusMgr.Inst.showGreenHandGiftWnd = false

        if CSigninMgr.Inst.huiliuAwardGetToday 
        and CClientMainPlayer.Inst ~= nil 
        and CClientMainPlayer.Inst.Level >= 3 
        and CWelfareBonusMgr.inst.LoginAwardId > 0 -- 用于倩影初闻录开放期间不显示登录七日签到界面
        and (CWelfareBonusMgr.Inst:CheckLoginDaysAward(CWelfareBonusMgr.inst.LoginAwardId, CWelfareBonusMgr.inst.LoginDays) 
                or CWelfareBonusMgr.Inst:CheckOnlineTimeAward(CWelfareBonusMgr.inst.OnlineAwardId, CWelfareBonusMgr.inst.OnlineAwardLeftTime)
            ) then

            CLuaEntryUIMgr.RemoveUI(CUIResources.WelfareWnd)
            -- CUIManager.CloseUI(CUIResources.WelfareWnd)
            CLuaEntryUIMgr.PushUI(CUIResources.GreenHandGiftWnd)
            CLuaEntryUIMgr.UpdateCanShowGreenHandGiftWndCondition()
        end
    else
        if CClientMainPlayer.Inst ~= nil 
            and not CSigninMgr.Inst.hasSignedToday 
            and CClientMainPlayer.Inst.Level >= 3 
            and CClientMainPlayer.Inst.IsAlive 
            and not (CSigninMgr.Inst.huiliuDay == 1 
            and not CSigninMgr.Inst.huiliuAwardGetToday) 
            and not CUIManager.IsLoaded(CUIResources.GreenHandGiftWnd) 
            and not CUIManager.IsLoading(CUIResources.GreenHandGiftWnd) then

            -- CLuaEntryUIMgr.PushUI(CUIResources.WelfareWnd)
        else--不满足条件 移除
            CLuaEntryUIMgr.RemoveUI(CUIResources.WelfareWnd)
        end
    end

    CLuaEntryUIMgr.PopUI()
end


--level, day:今天是第几天, flag: 1领过了，0没领过)
function Gas2Gac.SendHuiLiuData(level, day, flag, query)
    CSigninMgr.Inst.huiliuLevel = level
    CSigninMgr.Inst.huiliuDay = day
    CSigninMgr.Inst.huiliuAwardGetToday = (flag == 1) or not Huiliu_Item.Exists(level)

    if day == 1 and flag == 0 and not query then
        CLuaEntryUIMgr.PushUI(CUIResources.HuiliuWnd)
    end

    if query then
        EventManager.Broadcast(EnumEventType.OnHuiliuInfoUpdate)
    end
end

function Gas2Gac.ShowPlayUpdateAlert()
    CLuaEntryUIMgr.PushUI(CLuaUIResources.NewActivityWnd)
end

--同步签到天数奖励信息
function Gas2Gac.UpdateQianDaoDaysAward(qianDaoAwardId, qianDaoDays) 
    CWelfareBonusMgr.Inst.QianDaoAwardId = qianDaoAwardId
    CWelfareBonusMgr.Inst.QianDaoDays = qianDaoDays
    EventManager.Broadcast(EnumEventType.QianDaoDaysAwardReceived)

    if CWelfareBonusMgr.Inst:CheckQianDaoDaysAward(qianDaoAwardId, qianDaoDays) then
        CWelfareBonusMgr.Inst.showGreenHandGiftWnd = true
    else
        if not CUIManager.IsLoaded(CUIResources.WelfareWnd) then
            CLuaEntryUIMgr.PushUI(CUIResources.WelfareWnd)
        end
    end
end

--同步登录天数奖励信息
function Gas2Gac.UpdateLoginDaysAward(loginAwardId, loginDays) 
    CWelfareBonusMgr.Inst.LoginAwardId = loginAwardId
    CWelfareBonusMgr.Inst.LoginDays = loginDays
    EventManager.Broadcast(EnumEventType.LoginDaysAwardReceived)

    if CWelfareBonusMgr.Inst:CheckLoginDaysAward(loginAwardId, loginDays) then
        CWelfareBonusMgr.Inst.showGreenHandGiftWnd = true
    else
        if not CUIManager.IsLoaded(CUIResources.WelfareWnd) then
            CLuaEntryUIMgr.PushUI(CUIResources.WelfareWnd)
        end
        
    end
end

function Gas2Gac.NotifyPlayerChooseChristmasPickOnEnterHouse()
	CLuaEntryUIMgr.PushUI(CLuaUIResources.HouseChristmasGiftWnd)
end
