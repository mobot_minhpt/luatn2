require("common/common_include")

local UIGrid = import "UIGrid"
local Extensions = import "Extensions"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local QnTabView = import "L10.UI.QnTabView"
local CForcesMgr = import "L10.Game.CForcesMgr"
local UISlider = import "UISlider"
local NGUIText = import "NGUIText"
local UIEventListener = import "UIEventListener"
local LuaUtils = import "LuaUtils"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local Profession = import "L10.Game.Profession"
local QnButton = import "L10.UI.QnButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

CLuaCityWarResultWnd = class()
CLuaCityWarResultWnd.Path = "ui/citywar/LuaCityWarResultWnd"

RegistClassMember(CLuaCityWarResultWnd, "m_CurrentTabIndex")
RegistClassMember(CLuaCityWarResultWnd, "m_TabInitedTable")
RegistClassMember(CLuaCityWarResultWnd, "m_FullProgress")
RegistClassMember(CLuaCityWarResultWnd, "m_WinSpriteName")
RegistClassMember(CLuaCityWarResultWnd, "m_LoseSpriteName")
RegistClassMember(CLuaCityWarResultWnd, "m_TieSpriteName")

-- Switch Button
RegistClassMember(CLuaCityWarResultWnd, "m_SwitchBtnColorTable")
RegistClassMember(CLuaCityWarResultWnd, "m_SwitchBtnTextTable")
RegistClassMember(CLuaCityWarResultWnd, "m_SwitchBtnIndex")
RegistClassMember(CLuaCityWarResultWnd, "m_SwitchLabel")
RegistClassMember(CLuaCityWarResultWnd, "m_SwitchRootObj")

RegistClassMember(CLuaCityWarResultWnd, "m_MainPlayerId")

--{{{minelist}, {otherlist}}...}
RegistClassMember(CLuaCityWarResultWnd, "m_BattlePlayerDataTable")
RegistClassMember(CLuaCityWarResultWnd, "m_BattlePlayerTableViewTable")

-- Sort
RegistClassMember(CLuaCityWarResultWnd, "m_LastSortBtnObj")
RegistClassMember(CLuaCityWarResultWnd, "m_SortStatusTable")

RegistClassMember(CLuaCityWarResultWnd, "m_WarNumber")

-- Multi Page
RegistClassMember(CLuaCityWarResultWnd, "m_CurrentPageIndex")

-- Invite Button
RegistClassMember(CLuaCityWarResultWnd, "m_InviteButtonLabel")
RegistClassMember(CLuaCityWarResultWnd, "m_InviteButton")

function CLuaCityWarResultWnd:ChangeSortBtnLabel(obj, bClear, bInc)
	local label = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
	local str = string.gsub(label.text, LocalString.GetString("▲"), "")
	str = string.gsub(str, LocalString.GetString("▼"), "")
	if bClear then label.text = str return end
	str = str..(bInc and LocalString.GetString("▲") or LocalString.GetString("▼"))
	label.text = str
end

function CLuaCityWarResultWnd:ChooseSortBtn(obj, index)
	if self.m_LastSortBtnObj then
		 self:ChangeSortBtnLabel(self.m_LastSortBtnObj, true, true)
	end
	if self.m_SortStatusTable[index] == nil then
		self.m_SortStatusTable[index] = false
	else
		self.m_SortStatusTable[index] = not self.m_SortStatusTable[index]
	end
	self:ChangeSortBtnLabel(obj, false, self.m_SortStatusTable[index])
	self:RefreshData(index, self.m_SortStatusTable[index])
	self.m_LastSortBtnObj = obj
end

function CLuaCityWarResultWnd:InitSortBtn( ... )
	self.m_SortStatusTable = {}
	for j = 1, self.m_WarNumber do
		local rootTrans = self.transform:Find("Battle"..j)
		local transTbl = {"Left", "Right"}
		local curTbl = {}
		for i = 1, #transTbl do
			local headerTrans = rootTrans:Find(transTbl[i].."/List/Header")
			for k = 2, 12 do
				UIEventListener.Get(headerTrans:Find((k <= 7 and "First/" or "Second/")..tostring(k)).gameObject).onClick = LuaUtils.VoidDelegate(function ( go )
					self:ChooseSortBtn(go, k - 1)
				end)
			end
		end
	end
end

function CLuaCityWarResultWnd:Init( ... )
	if CLuaCityWarMgr.HistoryResultFlag then
		self.m_InviteButton.gameObject:SetActive(false)
		for i = 1, 3 do
			self.transform:Find("Wnd_Bg_Primary_Tab/Tabs/Tab"..i).gameObject:SetActive(false)
		end
		self:QueryMirrorWarSummaryResult(true, CLuaCityWarMgr.HistoryWinForce, CLuaCityWarMgr.HistoryPlayResultUD, CLuaCityWarMgr.HistoryForceInfoUD, CLuaCityWarMgr.HistorySummaryInfoUD)
	end
end

function CLuaCityWarResultWnd:Awake( ... )
	self.m_TabInitedTable = {}
	self.m_FullProgress = CLuaCityWarMgr.MirrorWarHuFuFullProgress
	self.m_WinSpriteName = "common_fight_result_win"
	self.m_LoseSpriteName = "common_fight_result_lose"
	self.m_TieSpriteName = "common_fight_result_tie"

	self.transform:Find("Wnd_Bg_Primary_Tab/Tabs"):GetComponent(typeof(QnTabView)).OnSelect = DelegateFactory.Action_QnTabButton_int(function (btn, index)
		self:ChangeTab(index)
	end)

	self.m_SwitchBtnColorTable = {NGUIText.ParseColor24("FF4C4C", 0), NGUIText.ParseColor24("6B9BFB", 0)}
	self.m_SwitchBtnTextTable = {LocalString.GetString("查看敌方成员数据"), LocalString.GetString("查看我方成员数据")}
	self.m_SwitchBtnIndex = 1

	self.m_SwitchRootObj = self.transform:Find("CommonBattleRoot").gameObject
	self.m_SwitchLabel = self.m_SwitchRootObj.transform:Find("SwitchLabel"):GetComponent(typeof(UILabel))
	UIEventListener.Get(self.m_SwitchRootObj.transform:Find("SwitchButton").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnSwitchButtonClick()
	end)

	self.m_InviteButton = self.transform:Find("Overview/InviteBtn"):GetComponent(typeof(QnButton))
	self.m_InviteButtonLabel = self.m_InviteButton.transform:Find("Label"):GetComponent(typeof(UILabel))
	UIEventListener.Get(self.m_InviteButton.gameObject).onClick = LuaUtils.VoidDelegate(function ()
		CLuaCityWarMgr.InviteTickTime = CServerTimeMgr.Inst.timeStamp
		Gac2Gas.RequestSummonGuildMemberToMirrorWarPlay()
	end)

	self.m_BattlePlayerDataTable = {}
	self.m_MainPlayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
	self.m_MainPlayerForce = CForcesMgr.Inst:GetPlayerForce(self.m_MainPlayerId)
	self:InitTableView()
	self:InitWarNum()
	self:InitSortBtn()
	self:InitPageBtn()
end

function CLuaCityWarResultWnd:InitWarNum()
	if #CLuaCityWarMgr.WarForceInfo <= 0 then
		self.m_WarNumber = 0
		return
	end
	self.m_WarNumber = CLuaCityWarMgr:GetCityWarCopyNum(CLuaCityWarMgr.WarForceInfo[1].MapId, CLuaCityWarMgr.WarForceInfo[2].MapId)
	for i = 1, 2 do
		self.transform:Find("Wnd_Bg_Primary_Tab/Tabs/Tab"..i).gameObject:SetActive(i <= self.m_WarNumber)
		self.transform:Find("Overview/Battle"..i).gameObject:SetActive(i <= self.m_WarNumber)
	end
end

function CLuaCityWarResultWnd:InitPageBtn()
	local changePageTrans = self.transform:Find("CommonBattleRoot/ChangePage/ChangePageButton")
	UIEventListener.Get(changePageTrans.gameObject).onClick = LuaUtils.VoidDelegate(function()
		local index = self.m_CurrentPageIndex == 1 and 2 or 1
		self:InitPageIndex(index)
		Extensions.SetLocalRotationZ(changePageTrans, index == 1 and 0 or 180)
		self:ChangeTableView(index)
	end)
	self:InitPageIndex(1)
end

function CLuaCityWarResultWnd:InitPageIndex(index)
	self.m_CurrentPageIndex = index

	for j = 1, self.m_WarNumber do
		local rootTrans = self.transform:Find("Battle"..j)
		local transTbl = {"Left", "Right"}
		local curTbl = {}
		for i = 1, #transTbl do
			local headerTrans = rootTrans:Find(transTbl[i].."/List/Header")
			headerTrans:Find("First").gameObject:SetActive(index == 1)
			headerTrans:Find("Second").gameObject:SetActive(index == 2)
		end
	end
end

function CLuaCityWarResultWnd:ChangeTableView(index)
	local tableview = self.m_BattlePlayerTableViewTable[self.m_CurrentTabIndex][self.m_SwitchBtnIndex]
	for i = 0, tableview.Count - 1 do
		local item = tableview:GetItemAtRow(i)
		if item then
			item.transform:Find("First").gameObject:SetActive(index == 1)
			item.transform:Find("Second").gameObject:SetActive(index == 2)
		end
	end
end


function CLuaCityWarResultWnd:InitTableView( ... )
	self.m_BattlePlayerTableViewTable = {}
	local dataSource = DefaultTableViewDataSource.Create(function()
			local battle = self.m_BattlePlayerDataTable[self.m_CurrentTabIndex]
			if battle then
				return #battle[self.m_SwitchBtnIndex]
			end
			return 0
		end, function(item, index)
			self:InitItem(item.gameObject, index + 1)
		end)

	for j = 1, 3 do
		local rootTrans = self.transform:Find("Battle"..j)
		local transTbl = {"Left", "Right"}
		local curTbl = {}
		for i = 1, #transTbl do
			local tableView = rootTrans:Find(transTbl[i].."/List/ScrollView"):GetComponent(typeof(QnAdvanceGridView))
			tableView.m_DataSource = dataSource
			table.insert(curTbl, tableView)
		end
		table.insert(self.m_BattlePlayerTableViewTable, curTbl)
	end
end

function CLuaCityWarResultWnd:ChangeTab(index)
	self.m_CurrentTabIndex = index
	self:InitSwitchButton(self.m_MainPlayerForce == 1 and 1 or 2)

	if self.m_TabInitedTable[index] then
		if index ~= 0 then
			self:ChangeTableView(self.m_CurrentPageIndex)
		end
		return
	end
	if index == 0 then
		Gac2Gas.QueryMirrorWarSummary()
	else
		Gac2Gas.QueryMirrorWarPlayInfo(index)
	end
end

function CLuaCityWarResultWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("QueryMirrorWarSummaryResult", self, "QueryMirrorWarSummaryResult")
	g_ScriptEvent:AddListener("QueryMirrorWarPlayInfoResult", self, "QueryMirrorWarPlayInfoResult")
end

function CLuaCityWarResultWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("QueryMirrorWarSummaryResult", self, "QueryMirrorWarSummaryResult")
	g_ScriptEvent:RemoveListener("QueryMirrorWarPlayInfoResult", self, "QueryMirrorWarPlayInfoResult")
end


function CLuaCityWarResultWnd:QueryMirrorWarSummaryResult(finish, winForce, playResultUD, forceInfoUD, summaryInfoUD)
	if self.m_CurrentTabIndex ~= 0 then return end
	local rootTrans = self.transform:Find("Overview")
	local topTrans = rootTrans:Find("TopInfo")
	--需求有变，原来是自己是蓝方，现在改成进攻方是蓝方
	--local myForce = CForcesMgr.Inst:GetPlayerForce(CClientMainPlayer.Inst.Id)
	local myForce = 1

	local myScore = 0
	local otherScore = 0

	for i = 1, 2 do
		local resultObj = topTrans:Find((i == 1 and "MyInfo" or "OtherInfo").."/ResultSprite").gameObject
		local thisForce = i == 1 and 1 or 0
		resultObj:SetActive(finish)
		if finish then
			local spriteName = ""
			if winForce == 2 then
				spriteName = self.m_TieSpriteName
			elseif winForce == thisForce then
				spriteName = self.m_WinSpriteName
			else
				spriteName = self.m_LoseSpriteName
			end
			resultObj:GetComponent(typeof(UISprite)).spriteName = spriteName
		end
	end

	local list1 = MsgPackImpl.unpack(playResultUD)
	if not list1 then return end
	for i = 0, list1.Count - 1, 3 do
		local warIndex = list1[i]
		local finish = list1[i + 1]
		local winForce = list1[i + 2]

		if CLuaCityWarMgr.WarIndex == warIndex then
			rootTrans:Find("Battle"..warIndex):GetComponent(typeof(UISprite)).spriteName = "main_taskandteam_background_s"
		end
		rootTrans:Find("Battle"..warIndex.."/CurSceneTex").gameObject:SetActive(CLuaCityWarMgr.WarIndex == warIndex)
		rootTrans:Find("Battle"..warIndex.."/OtherSceneTex").gameObject:SetActive(CLuaCityWarMgr.WarIndex ~= warIndex)
		rootTrans:Find("Battle"..warIndex.."/CurSceneFlag").gameObject:SetActive(CLuaCityWarMgr.WarIndex == warIndex)


		rootTrans:Find("Battle"..warIndex.."/InfoRoot/MyResult/ResultSprite").gameObject:SetActive(finish)
		rootTrans:Find("Battle"..warIndex.."/InfoRoot/OtherResult/ResultSprite").gameObject:SetActive(finish)

		if finish then
			local mySpriteName = ""
			local otherSpriteName = ""
			if myForce == winForce then
				myScore = myScore + 1
				mySpriteName = self.m_WinSpriteName
				otherSpriteName = self.m_LoseSpriteName
			elseif winForce == 2 then
				myScore = myScore + 1
				otherScore = otherScore + 1
				--mySpriteName = self.m_TieSpriteName
				--otherSpriteName = self.m_TieSpriteName
				mySpriteName = ""
				otherSpriteName = ""
			else
				otherScore = otherScore + 1
				mySpriteName = self.m_LoseSpriteName
				otherSpriteName = self.m_WinSpriteName
			end
			rootTrans:Find("Battle"..warIndex.."/InfoRoot/MyResult/ResultSprite"):GetComponent(typeof(UISprite)).spriteName = mySpriteName
			rootTrans:Find("Battle"..warIndex.."/InfoRoot/OtherResult/ResultSprite"):GetComponent(typeof(UISprite)).spriteName = otherSpriteName
		end

		rootTrans:Find("Battle"..warIndex.."/BattleStatusLabel"):GetComponent(typeof(UILabel)).text = finish and LocalString.GetString("已结束") or LocalString.GetString("进行中")
	end
	--topTrans:Find("MyInfo/MyScore"):GetComponent(typeof(UILabel)).text = myScore
	--topTrans:Find("OtherInfo/MyScore"):GetComponent(typeof(UILabel)).text = otherScore


	local list2 = MsgPackImpl.unpack(forceInfoUD)
	if not list2 then return end
	for i = 0, list2.Count - 1, 3 do
		local force = list2[i]
		local guildId = list2[i + 1]
		local guildName = list2[i + 2]

		if force == myForce then
			topTrans:Find("MyInfo/GuildName"):GetComponent(typeof(UILabel)).text = guildName
		else
			topTrans:Find("OtherInfo/GuildName"):GetComponent(typeof(UILabel)).text = guildName
		end
	end

	local transNameTable = {"CityLevel", "BuildingCount", "ReliveCount", "MemberCount", "KillCount"}
	local myValueTable = {0, 0, 0, 0, 0}
	local otherValueTable = {0, 0, 0, 0, 0}


	local list3 = MsgPackImpl.unpack(summaryInfoUD)
	if not list3 then return end
	for i = 0, list3.Count - 1, 10 do
		local warIndex = list3[i]
		local force = list3[i + 1]
		local cityGrade = list3[i + 2]
		local unitNum = list3[i + 3]
		local rebornPointNum = list3[i + 4]
		local playerNum = list3[i + 5]
		local killNum = list3[i + 6]
		local activated = list3[i + 7]
		local progress = list3[i + 8]
		local occupyForce = list3[i + 9]

		local resultTrans
		if force == myForce then
			myValueTable[1] = cityGrade
			for j = 2, 5 do
				myValueTable[j] = myValueTable[j] + list3[i + j + 1]
			end
			resultTrans = rootTrans:Find("Battle"..warIndex.."/InfoRoot/MyResult")
		else
			otherValueTable[1] = cityGrade
			for j = 2, 5 do
				otherValueTable[j] = otherValueTable[j] + list3[i + j + 1]
			end
			resultTrans = rootTrans:Find("Battle"..warIndex.."/InfoRoot/OtherResult")
		end
		resultTrans:Find("MemberCount"):GetComponent(typeof(UILabel)).text = playerNum
		resultTrans:Find("HufuLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("%s%%", math.floor(progress/self.m_FullProgress * 100))
		resultTrans:Find("HufuProgress"):GetComponent(typeof(UISlider)).value = progress / self.m_FullProgress
		resultTrans:Find("HufuProgress/Foreground"):GetComponent(typeof(UISprite)).spriteName = occupyForce == myForce and "guildleaguewnd_blue_02" or "guildleaguewnd_red_02"
	end

	for i = 1, 2 do
		local rootName = i == 1 and "MyInfo" or "OtherInfo"
		local valueTable = i == 1 and myValueTable or otherValueTable
		for j = 1, #transNameTable do
			topTrans:Find(rootName.."/"..transNameTable[j]):GetComponent(typeof(UILabel)).text = valueTable[j]
		end
	end

	self.m_TabInitedTable[self.m_CurrentTabIndex] = true
end

function CLuaCityWarResultWnd:QueryMirrorWarPlayInfoResult(warIndex, finish, winForce, forceInfoUD, summaryInfoUD, playerInfoUD)

	if self.m_CurrentTabIndex ~= warIndex then return end
	local rootTrans = self.transform:Find("Battle"..warIndex)
	local topTrans = rootTrans:Find("TopInfo")
	--local myForce = CForcesMgr.Inst:GetPlayerForce(CClientMainPlayer.Inst.Id)
	local myForce = 1

	for i = 1, 2 do
		local resultObj = topTrans:Find((i == 1 and "MyInfo" or "OtherInfo").."/ResultSprite").gameObject
		local thisForce = i == 1 and 1 or 0
		resultObj:SetActive(finish)
		if finish then
			local spriteName = ""
			if winForce == 2 then
				spriteName = self.m_TieSpriteName
			elseif winForce == thisForce then
				spriteName = self.m_WinSpriteName
			else
				spriteName = self.m_LoseSpriteName
			end
			resultObj:GetComponent(typeof(UISprite)).spriteName = spriteName
		end
	end

	local list1 = MsgPackImpl.unpack(forceInfoUD)
	if not list1 then return end
	for i = 0, list1.Count - 1, 3 do
		local force = list1[i]
		local guildId = list1[i + 1]
		local guildName = list1[i + 2]

		if force == myForce then
			topTrans:Find("MyInfo/GuildName"):GetComponent(typeof(UILabel)).text = guildName
		else
			topTrans:Find("OtherInfo/GuildName"):GetComponent(typeof(UILabel)).text = guildName
		end
	end

	local list2 = MsgPackImpl.unpack(summaryInfoUD)
	if not list2 then return end
	for i = 0, list2.Count - 1, 6 do
		local force = list2[i]
		local playerNum = list2[i + 1]
		local killNum = list2[i + 2]
		local activated = list2[i + 3]
		local progress = list2[i + 4]
		local occupyForce = list2[i + 5]

		local infoTransName = force == myForce and "MyInfo" or "OtherInfo"

		topTrans:Find(infoTransName.."/MemberCount"):GetComponent(typeof(UILabel)).text = playerNum
		topTrans:Find(infoTransName.."/KillCount"):GetComponent(typeof(UILabel)).text = killNum
		topTrans:Find(infoTransName.."/HufuLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("%s%%", math.floor(progress/self.m_FullProgress * 100))
		topTrans:Find(infoTransName.."/HufuProgress"):GetComponent(typeof(UISlider)).value = progress / self.m_FullProgress
		topTrans:Find(infoTransName.."/HufuProgress/Foreground"):GetComponent(typeof(UISprite)).spriteName = occupyForce == myForce and "guildleaguewnd_blue_02" or "guildleaguewnd_red_02"
	end

	local myUITable = rootTrans:Find("Left/List/ScrollView/Table"):GetComponent(typeof(UIGrid))
	local otherUITable = rootTrans:Find("Right/List/ScrollView/Table"):GetComponent(typeof(UIGrid))
	local playerTemplateObj = rootTrans:Find("Pool/PlayerItem").gameObject
	playerTemplateObj:SetActive(false)

	local curTbl = {{}, {}}
	self.m_BattlePlayerDataTable[warIndex] = curTbl

	local list3 = MsgPackImpl.unpack(playerInfoUD)
	if not list3 then return end
	for i = 0, list3.Count - 1, 17 do
		local id = list3[i]
		local name = list3[i + 1]
		local role = list3[i + 2]
		local level = list3[i + 3]
		local force = list3[i + 4]
		local killNum = list3[i + 5]
		local dieNum = list3[i + 6]
		local helpNum = list3[i + 7]
		local baoShiNum = list3[i + 8]
		local reliveNum = list3[i + 9]
		local ctrlNum = list3[i + 10]
		local rmCtrlNum = list3[i + 11]
		local occupyTime = list3[i + 12]
		local dpsNum = list3[i + 13]
		local healNum = list3[i + 14]
		local underDamage = list3[i + 15]

		local componentNum = list3[i + 16]

		local idx = force == myForce and 1 or 2

		table.insert(curTbl[idx], {id, role, name, killNum, dieNum, helpNum, baoShiNum, reliveNum, ctrlNum, dpsNum, underDamage, healNum, occupyTime, componentNum})
	end

	self:RefreshData(0)

	self.m_TabInitedTable[self.m_CurrentTabIndex] = true
end

function CLuaCityWarResultWnd:InitItem(obj, index)
	local transNameTbl = {"NameLabel", "KillNumLabel", "KilledNumLabel", "HelpLabel", "BaoshiLabel", "RebornLabel", "ControlLabel", "DpsLabel", "UnderDamageLabel", "HealNumLabel", "HufuLabel", "ComponentLabel"}
	local battle = self.m_BattlePlayerDataTable[self.m_CurrentTabIndex]
	if not battle then return end
	local valueTbl = battle[self.m_SwitchBtnIndex][index]
	if not valueTbl then return end
	for i = 1, #transNameTbl do
		local val = valueTbl[i + 2]
		if i == 11 then
			val = SafeStringFormat3(LocalString.GetString("00:%02d:%02d"), math.floor(val / 60), val % 60)
		elseif i >= 8 and i <= 10 then
			val = math.floor(val)
			if val > 99999 then
				val = SafeStringFormat3(LocalString.GetString("%d万"), math.floor(val / 10000))
			end
		end
		local transName = transNameTbl[i]
		if i >= 2 and i <= 7 then
			transName = "First/"..transName
		elseif i > 7 then
			transName = "Second/"..transName
		end
		local label = obj.transform:Find(transName):GetComponent(typeof(UILabel))
		label.text = val
		label.color = valueTbl[1] == self.m_MainPlayerId and Color.green or Color.white
	end
	obj.transform:Find("ClazzSprite"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(valueTbl[2])
	obj.transform:Find("Bg"):GetComponent(typeof(UISprite)).spriteName = index % 2 == 0 and "common_textbg_01_light" or ""

	obj.transform:Find("First").gameObject:SetActive(self.m_CurrentPageIndex == 1)
	obj.transform:Find("Second").gameObject:SetActive(self.m_CurrentPageIndex == 2)
end

function CLuaCityWarResultWnd:RefreshData(sortType, bInc)
	-- sort
	local data = self.m_BattlePlayerDataTable[self.m_CurrentTabIndex] and self.m_BattlePlayerDataTable[self.m_CurrentTabIndex][self.m_SwitchBtnIndex]
	if not data then return end
	local sortFunc = function (a, b)
		-- mainplayer awalys first
		if a[1] == self.m_MainPlayerId then return true end
		if b[1] == self.m_MainPlayerId then return false end
		if sortType == 0 then return false end

		if bInc then
			return a[sortType + 3] < b[sortType + 3]
		else
			return a[sortType + 3] > b[sortType + 3]
		end
	end

	table.sort(data, sortFunc)
	self.m_BattlePlayerTableViewTable[self.m_CurrentTabIndex][self.m_SwitchBtnIndex]:ReloadData(true, false)
end

function CLuaCityWarResultWnd:InitSwitchButton(index)
	self.m_SwitchRootObj:SetActive(self.m_CurrentTabIndex ~= 0)

	if self.m_CurrentTabIndex == 0 then return end

	self.m_SwitchBtnIndex = index
	local rootTrans = self.transform:Find("Battle"..self.m_CurrentTabIndex)
	--local isLeft = (self.m_MainPlayerForce == 1 and index == 1) or (self.m_MainPlayerForce == 0 and index == 2)
	rootTrans:Find("Left").gameObject:SetActive(index == 1)
	rootTrans:Find("Right").gameObject:SetActive(index == 2)

	local isMyData = (self.m_MainPlayerForce == 1 and index == 1) or (self.m_MainPlayerForce == 0 and index == 2)
	local idx = isMyData and 1 or 2
	self.m_SwitchLabel.text = self.m_SwitchBtnTextTable[idx]
	--self.m_SwitchLabel.color = self.m_SwitchBtnColorTable[index]
	self.m_SortStatusTable = {}

	self:RefreshData(0)
end

function CLuaCityWarResultWnd:OnSwitchButtonClick()
	self:InitSwitchButton(self.m_SwitchBtnIndex == 1 and 2 or 1)
	self.m_SortStatusTable = {}
end


function CLuaCityWarResultWnd:OnDestroy( ... )
	-- clear flag
	CLuaCityWarMgr.HistoryResultFlag = nil
end

function CLuaCityWarResultWnd:Update()
	local interval = math.floor(CServerTimeMgr.Inst.timeStamp - CLuaCityWarMgr.InviteTickTime)
	self.m_InviteButton.Enabled = interval > 60
	self.m_InviteButtonLabel.text = SafeStringFormat3(LocalString.GetString("邀请成员进战场 %s"), interval > 60 and "" or tostring(60 - interval).."s")
end
