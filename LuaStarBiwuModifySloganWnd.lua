local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"


CLuaStarBiwuModifySloganWnd=class()
-- RegistClassMember(CLuaStarBiwuModifySloganWnd,"ModifySloganRoot")
RegistClassMember(CLuaStarBiwuModifySloganWnd,"ModifySloganButton")
RegistClassMember(CLuaStarBiwuModifySloganWnd,"NameInput")
RegistClassMember(CLuaStarBiwuModifySloganWnd,"SloganInput")
RegistClassMember(CLuaStarBiwuModifySloganWnd,"ModifyCancelButton")
RegistClassMember(CLuaStarBiwuModifySloganWnd,"ModifyConfirmButton")

function CLuaStarBiwuModifySloganWnd:Init()
    -- self.ModifySloganRoot = self.transform:Find("ModifySlogan").gameObject
    self.NameInput = self.transform:Find("ShowArea/NameInput"):GetComponent(typeof(UIInput))
    self.SloganInput = self.transform:Find("ShowArea/SloganInput"):GetComponent(typeof(UIInput))
    self.ModifyCancelButton = self.transform:Find("ShowArea/CancelButton").gameObject
    self.ModifyConfirmButton = self.transform:Find("ShowArea/CreateButton").gameObject


    self.NameInput.value = CLuaStarBiwuMgr.m_CurrentZhanduiName
    self.SloganInput.value = CLuaStarBiwuMgr.m_CurrentZhanduiSlogan


    -- UIEventListener.Get(self.ModifySloganButton).onClick = DelegateFactory.VoidDelegate(function(go) self:onModifySloganClick(go) end)
    UIEventListener.Get(self.ModifyConfirmButton).onClick = DelegateFactory.VoidDelegate(function(go) self:onModifySloganConfirm(go) end)
    UIEventListener.Get(self.ModifyCancelButton).onClick = DelegateFactory.VoidDelegate(function(go) self:onCancelModify(go) end)

end
function CLuaStarBiwuModifySloganWnd:onModifySloganConfirm( go)
    local text=self.NameInput.value
    if not text or text=="" then
        g_MessageMgr:ShowMessage("QMPK_Please_Input_Zhandui_Name")
        return
    end
    local nameStr = string.gsub(text, " ", "")
    if #nameStr == 0 then
        g_MessageMgr:ShowMessage("QMPK_Please_Input_Zhandui_Name")
        return
    end
    if CUICommonDef.GetStrByteLength(nameStr) > CQuanMinPKMgr.m_MaxNameLength then
        g_MessageMgr:ShowMessage("QMPK_Zhandui_Name_Too_Long")
        return
    end
    if CUICommonDef.GetStrByteLength(self.SloganInput.value) > CQuanMinPKMgr.m_MaxSloganLength then
        g_MessageMgr:ShowMessage("QMPK_Slogan_Too_Long")
        return
    end
    if not CWordFilterMgr.Inst:CheckName(nameStr, LocalString.GetString("明星赛战队名")) or
        not CWordFilterMgr.Inst:CheckName(self.SloganInput.value, LocalString.GetString("明星赛战队口号")) then
        g_MessageMgr:ShowMessage("QMPK_Name_Slogan_Violation")
        return
    end
    Gac2Gas.RequestUpdateStarBiwuZhanDuiTitleAndKouHao(nameStr, self.SloganInput.value)
    CUIManager.CloseUI(CLuaUIResources.StarBiwuModifySloganWnd)
end
function CLuaStarBiwuModifySloganWnd:onCancelModify( go)
    -- self.ModifySloganRoot:SetActive(false)
    CUIManager.CloseUI(CLuaUIResources.StarBiwuModifySloganWnd)
end
