local DelegateFactory        = import "DelegateFactory"
local CCurentMoneyCtrl       = import "L10.UI.CCurentMoneyCtrl"
local TweenScale             = import "TweenScale"
local TweenAlpha             = import "TweenAlpha"
local CServerTimeMgr         = import "L10.Game.CServerTimeMgr"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local Item_Item              = import "L10.Game.Item_Item"
local CShopMallMgr           = import "L10.UI.CShopMallMgr"
local SkeletonAnimation      = import "Spine.Unity.SkeletonAnimation"

LuaLingyuGiftRecommendWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaLingyuGiftRecommendWnd, "title")
RegistClassMember(LuaLingyuGiftRecommendWnd, "time")
RegistClassMember(LuaLingyuGiftRecommendWnd, "grid")
RegistClassMember(LuaLingyuGiftRecommendWnd, "template")
RegistClassMember(LuaLingyuGiftRecommendWnd, "originCost")
RegistClassMember(LuaLingyuGiftRecommendWnd, "discount")
RegistClassMember(LuaLingyuGiftRecommendWnd, "moneyCtrl")
RegistClassMember(LuaLingyuGiftRecommendWnd, "buyButton")
RegistClassMember(LuaLingyuGiftRecommendWnd, "bubbleTweenWidget")
RegistClassMember(LuaLingyuGiftRecommendWnd, "bubbleTweenAlpha")
RegistClassMember(LuaLingyuGiftRecommendWnd, "bubbleTweenScale")
RegistClassMember(LuaLingyuGiftRecommendWnd, "word")
RegistClassMember(LuaLingyuGiftRecommendWnd, "bubble")
RegistClassMember(LuaLingyuGiftRecommendWnd, "aChu")

RegistClassMember(LuaLingyuGiftRecommendWnd, "tick")
RegistClassMember(LuaLingyuGiftRecommendWnd, "bubbleTick")
RegistClassMember(LuaLingyuGiftRecommendWnd, "bubblePlayTick") -- 气泡延迟一下再出现

function LuaLingyuGiftRecommendWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self:InitUIComponents()
    self.template:SetActive(false)

    UIEventListener.Get(self.buyButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuyButtonClick()
	end)
end

-- 初始化UI组件
function LuaLingyuGiftRecommendWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.title = anchor:Find("Title"):GetComponent(typeof(UILabel))
    self.time = anchor:Find("Time"):GetComponent(typeof(UILabel))
    self.template = anchor:Find("Props/Template").gameObject
    self.grid = anchor:Find("Props/Grid"):GetComponent(typeof(UIGrid))
    self.originCost = anchor:Find("Cost/OriginCost"):GetComponent(typeof(UILabel))
    self.discount = anchor:Find("Cost/Discount"):GetComponent(typeof(UILabel))
    self.moneyCtrl = anchor:Find("Cost/QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
    self.buyButton = anchor:Find("BuyButton"):GetComponent(typeof(QnButton))
    self.bubbleTweenWidget = self.transform:Find("Tween"):GetComponent(typeof(UIWidget))
    self.bubbleTweenScale = self.transform:Find("Tween"):GetComponent(typeof(TweenScale))
    self.bubbleTweenAlpha = self.transform:Find("Tween"):GetComponent(typeof(TweenAlpha))
    self.word = self.transform:Find("Tween/Word"):GetComponent(typeof(UILabel))
    self.bubble = self.transform:Find("Tween/Word/Bubble"):GetComponent(typeof(UIWidget))
    self.aChu = self.transform:Find("AChuWidget/AChu")
end

function LuaLingyuGiftRecommendWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncUXDynamicShopShangJiaGiftItems", self, "OnSyncUXDynamicShopShangJiaGiftItems")
    g_ScriptEvent:AddListener("MainPlayerUpdateMoney", self, "OnMainPlayerUpdateMoney")
end

function LuaLingyuGiftRecommendWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncUXDynamicShopShangJiaGiftItems", self, "OnSyncUXDynamicShopShangJiaGiftItems")
    g_ScriptEvent:RemoveListener("MainPlayerUpdateMoney", self, "OnMainPlayerUpdateMoney")
end

function LuaLingyuGiftRecommendWnd:OnSyncUXDynamicShopShangJiaGiftItems()
    self:StartTick()
    self:UpdateCost()
    self:UpdateItems()
end

function LuaLingyuGiftRecommendWnd:OnMainPlayerUpdateMoney()
    self.moneyCtrl.m_OwnLabel.color = self.moneyCtrl.moneyEnough and NGUIText.ParseColor24("FFFFFF", 0) or NGUIText.ParseColor24("FF0000", 0)
end


function LuaLingyuGiftRecommendWnd:Init()
    self:ShowBubble()
    self:StartTick()
    self:UpdateCost()
    self:UpdateItems()
end

function LuaLingyuGiftRecommendWnd:StartTick()
    self:ClearTick(self.tick)
    self:UpdateTime()
    self.tick = RegisterTick(function()
        self:UpdateTime()
    end, 1000)
end

function LuaLingyuGiftRecommendWnd:UpdateTime()
    local expireTime = LuaLingyuGiftRecommendMgr.info.expireTime
    local leftTime = expireTime and math.floor(expireTime - CServerTimeMgr.Inst.timeStamp) or -1
    if leftTime <= 0 then
        g_MessageMgr:ShowMessage("UXDYNAMICGIFT_NO_LEFT_TIME")
        CUIManager.CloseUI(CLuaUIResources.LingyuGiftRecommendWnd)
        return
    end

    local color = leftTime < tonumber(Mall_Setting.GetData("UXDynamicGift_RedDisplayTime").Value) and "D20F03" or "894729"
    local h = math.floor(leftTime / 3600)
    local m = math.floor(leftTime % 3600 / 60)
    local s = leftTime % 60
    self.time.text = SafeStringFormat3("%02d:%02d:%02d", h, m, s)
    self.time.color = NGUIText.ParseColor24(color, 0)
end

-- 显示气泡
function LuaLingyuGiftRecommendWnd:ShowBubble()
    self:ClearTick(self.bubbleTick)
    self:ClearTick(self.bubblePlayTick)
    self.bubbleTweenWidget.alpha = 0
    self.bubblePlayTick = RegisterTickOnce(function()
        local anim = self.aChu:GetComponentInChildren(typeof(SkeletonAnimation))
        if anim then
            anim.state:SetAnimation(0, "wave", false)
            anim.state:AddAnimation(0, "idle", true, 0)
        end

        self.bubbleTweenWidget.alpha = 1
        self.bubbleTweenScale:ResetToBeginning()
        self.bubbleTweenScale:PlayForward()

        self.bubbleTick = RegisterTickOnce(function()
            self.bubbleTweenScale:PlayReverse()
            self.bubbleTweenAlpha:ResetToBeginning()
            self.bubbleTweenAlpha:PlayForward()
            self:ClearTick(self.bubbleTick)
        end, 6000)
    end, 600)
end

-- 更新价格
function LuaLingyuGiftRecommendWnd:UpdateCost()
    local info = LuaLingyuGiftRecommendMgr.info
    if not info.expireTime then return end

    local active = info.discount < 10
    self.originCost.gameObject:SetActive(active)
    self.discount.gameObject:SetActive(active)
    self.originCost.text = SafeStringFormat3(LocalString.GetString("%d"), info.oriTotalJade)
    self.moneyCtrl:SetCost(info.totalJade)
    self.discount.text = SafeStringFormat3(LocalString.GetString("%.1f折"), info.discount)
    self.moneyCtrl.m_OwnLabel.color = self.moneyCtrl.moneyEnough and NGUIText.ParseColor24("FFFFFF", 0) or NGUIText.ParseColor24("FF0000", 0)
end

-- 更新礼包列表
function LuaLingyuGiftRecommendWnd:UpdateItems()
    Extensions.RemoveAllChildren(self.grid.transform)

    local info = LuaLingyuGiftRecommendMgr.info
    if not info.expireTime then return end

    for i = 1, #info.itemInfo, 2 do
        local child = NGUITools.AddChild(self.grid.gameObject, self.template)
        child:SetActive(true)
        local qnReturnAward = child.transform:GetComponent(typeof(CQnReturnAwardTemplate))
        qnReturnAward:Init(Item_Item.GetData(info.itemInfo[i]), info.itemInfo[i + 1])
    end
    self.grid:Reposition()
end

function LuaLingyuGiftRecommendWnd:ClearTick(tick)
    if tick then
        UnRegisterTick(tick)
    end
    return nil
end

function LuaLingyuGiftRecommendWnd:OnDestroy()
    self:ClearTick(self.tick)
    self:ClearTick(self.bubbleTick)
    self:ClearTick(self.bubblePlayTick)
end

--@region UIEvent

function LuaLingyuGiftRecommendWnd:OnBuyButtonClick()
    local info = LuaLingyuGiftRecommendMgr.info
    if not info.expireTime then return end

    if not self.moneyCtrl.moneyEnough then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("JADE_PAY_NOTICE"), function ()
            CShopMallMgr.ShowChargeWnd()
        end, nil, LocalString.GetString("充值"), nil, false)
        return
    end

    Gac2Gas.BuyUXDynamicGiftItems(info.event, info.traceId)
end

--@endregion UIEvent

