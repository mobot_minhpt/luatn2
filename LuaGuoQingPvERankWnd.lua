local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"

LuaGuoQingPvERankWnd = class()


RegistChildComponent(LuaGuoQingPvERankWnd, "MyRankLabel", "MyRankLabel", UILabel)
RegistChildComponent(LuaGuoQingPvERankWnd, "MyNameLabel", "MyNameLabel", UILabel)
RegistChildComponent(LuaGuoQingPvERankWnd, "MyFinishTimeLabel", "MyFinishTimeLabel", UILabel)
RegistChildComponent(LuaGuoQingPvERankWnd, "MyRankImage", "MyRankImage", UISprite)
RegistChildComponent(LuaGuoQingPvERankWnd, "TableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaGuoQingPvERankWnd, "NoneRankLabel", "NoneRankLabel", GameObject)

RegistClassMember(LuaGuoQingPvERankWnd, "m_RankData")
RegistClassMember(LuaGuoQingPvERankWnd, "m_MyRank")
RegistClassMember(LuaGuoQingPvERankWnd, "m_RankSpriteName")
RegistClassMember(LuaGuoQingPvERankWnd, "m_ClassNameTable")
RegistClassMember(LuaGuoQingPvERankWnd, "m_TabIndex")
RegistClassMember(LuaGuoQingPvERankWnd, "isUpdateTabled")
function LuaGuoQingPvERankWnd:Awake()
	--@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self:NumberOfRows()
        end,
        function(item, index)
            return self:ItemAt(item,index)
        end)
    local callback = DelegateFactory.Action_int(function(row) self:OnSelectAtRow(row) end)
	self.TableView.OnSelectAtRow = callback
	self.isUpdateTabled = false
end
function LuaGuoQingPvERankWnd:Update()
	if self.isUpdateTabled == false and CClientMainPlayer.Inst ~= nil then
		self:UpdateTable()
	end
end
function LuaGuoQingPvERankWnd:Init()
	self:UpdateTable()
	
end

--@region UIEvent

function LuaGuoQingPvERankWnd:TimeChange(timedata)
	local number = tonumber(timedata)
	local minutes = math.floor(number / 60)
	local seconds = number % 60
	return SafeStringFormat3("%s%s%s%s",tostring(minutes),LocalString.GetString("分"),tostring(seconds),LocalString.GetString("秒"))
	
end

function LuaGuoQingPvERankWnd:UpdateTable()
    self.m_RankSpriteName = {"Rank_No.1","Rank_No.2png","Rank_No.3png"}
	self.m_RankData = LuaGuoQingPvEMgr.RankData
	self.NoneRankLabel:SetActive(self:NumberOfRows() == 0)
	self.TableView:ReloadData(true, true)
	self.m_MyRank = nil
	if CClientMainPlayer.Inst then
		self.isUpdateTabled = true
	end
	for _,data in pairs(self.m_RankData) do
		if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == data["playerId"] then
			self.m_MyRank = data
			break
		end
	end
	if self.m_MyRank == nil then
		self.MyRankLabel.text = LocalString.GetString("未上榜")
		self.MyRankImage.spriteName = nil
		self.MyFinishTimeLabel.text = self:TimeChange(LuaGuoQingPvEMgr.SelfFinishTime)
		if LuaGuoQingPvEMgr.SelfFinishTime == 0 then
			self.MyFinishTimeLabel.text = LocalString.GetString("未通关")
		end
	else
		if tonumber(self.m_MyRank["rank"]) <= 3 then
			self.MyRankImage.spriteName = self.m_RankSpriteName[tonumber(self.m_MyRank["rank"])]
		else
			self.MyRankImage.spriteName = nil
		end
		self.MyRankLabel.text = self.m_MyRank["rank"]
		self.MyFinishTimeLabel.text = self:TimeChange(self.m_MyRank["finishTime"])
		if self.m_MyRank["finishTime"] == 0 then
			self.MyFinishTimeLabel.text = LocalString.GetString("未通关")
		end
	end
	
	self.MyNameLabel.text = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name or " "
	
	
end

function LuaGuoQingPvERankWnd:NumberOfRows()
	return self.m_RankData and #self.m_RankData or 0
end

function LuaGuoQingPvERankWnd:ItemAt(item,index)
	if not self.m_RankData then return end
	local info = self.m_RankData[index+1]
	self:InitItem(item.transform,info)
	local default = index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite
    item:SetBackgroundTexture(default)
end

function LuaGuoQingPvERankWnd:OnSelectAtRow(row)

end



function LuaGuoQingPvERankWnd:InitItem(transform, info)
	FindChild(transform,"RankLabel"):GetComponent(typeof(UILabel)).text = info["rank"]
	if tonumber(info["rank"]) <= 3 then
		FindChild(transform,"RankImage"):GetComponent(typeof(UISprite)).spriteName = self.m_RankSpriteName[tonumber(info["rank"])]
	else
		FindChild(transform,"RankImage"):GetComponent(typeof(UISprite)).spriteName = nil
	end
	FindChild(transform,"NameLabel"):GetComponent(typeof(UILabel)).text = info["playerName"]
	if info["finishTime"] == 0 then
		FindChild(transform,"TimeLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("未通关")
	else
		FindChild(transform,"TimeLabel"):GetComponent(typeof(UILabel)).text = self:TimeChange(info["finishTime"])
	end

end
--@endregion





