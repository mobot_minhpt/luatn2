-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuildDevelopDetailView_House = import "L10.UI.CGuildDevelopDetailView_House"
local CGuildMgr = import "L10.Game.CGuildMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local LocalString = import "LocalString"
CGuildDevelopDetailView_House.m_OnClickStartBuildButton_CS2LuaHook = function (this, go) 
    -- TODO zzm 建设房屋
    -- TODO zzm 建设房屋
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.BasicProp.GuildId ~= 0 then
        Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "WingRoom", "", "")
    end
end
CGuildDevelopDetailView_House.m_OnRequestOperationInGuildSucceed_CS2LuaHook = function (this, requestType, paramStr) 
    -- 房屋 建设成功 
    -- 房屋 建设成功 
    if (("WingRoom") == requestType) then
        CGuildMgr.Inst.canUpdateWingRoom = false

        local updateTime = CGuildMgr.Inst:GetUpdateTime(CGuildDevelopDetailView_House.UPDATE_TYPE)
        local nowTime = CServerTimeMgr.Inst:GetZone8Time()
        local d = nowTime:AddSeconds(updateTime - 1)
        local duration = CommonDefs.op_Subtraction_DateTime_DateTime(d, nowTime)

        this.buildTimeLabel.text = System.String.Format(CGuildDevelopDetailView_House.TimeFormat, duration.Days, duration.Hours, duration.Minutes)
        this.startBuildBtn.Enabled = false
        this.startBuildBtn.label.text = LocalString.GetString("正在建设")

        if CGuildMgr.Inst.m_GuildInfo ~= nil then
            CGuildMgr.Inst.m_GuildInfo.CurBuild = requestType
            CGuildMgr.Inst.m_GuildInfo.CurBuildStartTime = math.floor(CServerTimeMgr.Inst.timeStamp)
            CGuildMgr.Inst:GetMyGuildInfo()
        end
    end
end
CGuildDevelopDetailView_House.m_Init_CS2LuaHook = function (this) 
    if CGuildMgr.Inst.m_GuildInfo == nil then
        return
    end

    this.startBuildBtn.Enabled = true

    -- 等级变量
    this.buildNumLabel.text = System.String.Format(LocalString.GetString("房屋数量{0}/{1}"), CGuildMgr.Inst.m_GuildInfo.WingRoomNum, CGuildMgr.Inst:GetMaxBuilding())
    this.limitLabel.text = System.String.Format(LocalString.GetString("提升人口上限{0}人"), CGuildMgr.Inst:GetWingRoomSizeLimit())

    -- 升级资金
    local costSilver = CGuildMgr.Inst:GetUpdateCost(CGuildDevelopDetailView_House.UPDATE_TYPE)
    if CGuildMgr.Inst:GetSilver() >= costSilver then
        this.startBuildBtn.Enabled = true
        this.costLabel.text = System.String.Format(LocalString.GetString("帮会资金[00ff00]{0}[-]"), costSilver)
    else
        this.startBuildBtn.Enabled = false
        this.costLabel.text = System.String.Format(LocalString.GetString("帮会资金[ff0000]{0}[-]"), costSilver)
    end

    CGuildMgr.Inst.canUpdateWingRoom = true
    -- 升级条件1, 帮会资金
    local totalSilver = costSilver + 2 * CGuildMgr.Inst:GetMaintainCost()
    local satisfy = true
    if CGuildMgr.Inst:GetSilver() < totalSilver then
        satisfy = false
    end

    this.needLabel.text = System.String.Format("{0}/{1}", CGuildMgr.Inst:GetSilver(), totalSilver)
    this.needSlider.value = (CGuildMgr.Inst:GetSilver() / totalSilver)

    -- 帮会等级
    local sizeLevelneedMin = CGuildMgr.Inst:GetMinSizeLevelForBuildingNum(CGuildMgr.Inst.m_GuildInfo.WingRoomNum + 1)
    local sizeLevelneed = CGuildMgr.Inst.m_GuildInfo.Scale
    if CGuildMgr.Inst.m_GuildInfo.WingRoomNum >= CGuildMgr.Inst:GetMaxBuilding() then
        this.startBuildBtn.Enabled = false
        sizeLevelneed = sizeLevelneed + 1
        --c = Color.red;
        CGuildMgr.Inst.canUpdateWingRoom = false
        sizeLevelneedMin = sizeLevelneed

        satisfy = false
    end

    if not satisfy then
        this.startBuildBtn.Enabled = false
        --requireLabel.color = Color.red;
        if CGuildMgr.Inst ~= nil then
            CGuildMgr.Inst.canUpdateSizeLevel = false
        end
    else
        --requireLabel.color = Color.green;
    end
    --只判断帮会等级
    if sizeLevelneedMin > CGuildMgr.Inst.m_GuildInfo.Scale then
        this.requireLabel.color = Color.red
    else
        this.requireLabel.color = Color.green
    end

    this.requireLabel.text = System.String.Format(LocalString.GetString("帮会等级达到{1}级"), totalSilver, sizeLevelneedMin)


    -- 升级时间
    local updateTime = CGuildMgr.Inst:GetUpdateTime(CGuildDevelopDetailView_House.UPDATE_TYPE)

    if (CGuildMgr.Inst.m_GuildInfo.CurBuild == CGuildMgr.BUILD_TYPE_WingRoom) then
        local startTime = CServerTimeMgr.ConvertTimeStampToZone8Time(CGuildMgr.Inst.m_GuildInfo.CurBuildStartTime)
        local nowTime = CServerTimeMgr.Inst:GetZone8Time()
        local duration = CommonDefs.op_Subtraction_DateTime_DateTime(startTime:AddSeconds(updateTime), nowTime)
        local totalSeconds = duration.TotalSeconds

        this.startBuildBtn.label.text = LocalString.GetString("开始建设")
        if totalSeconds > 0 then
            CGuildMgr.Inst.canUpdateWingRoom = false
            --float rate = 1 - (float)totalSeconds / updateTime;
            --_UpdateSilder(duration, startBuildBtn, "正在建设", rate);
            this.buildTimeLabel.text = System.String.Format(CGuildDevelopDetailView_House.TimeFormat, duration.Days, duration.Hours, duration.Minutes)
        end
    else
        local timeStr = LocalString.GetString("需要")
        if math.floor(updateTime / 3600) > 0 then
            if math.floor((updateTime % 3600) / 60) > 0 then
                timeStr = SafeStringFormat3(LocalString.GetString("需要%s小时%s分钟"), tostring(math.floor(updateTime / 3600)), tostring((math.floor((updateTime % 3600) / 60))))
            else
                timeStr = SafeStringFormat3(LocalString.GetString("需要%s小时"), tostring(math.floor(updateTime / 3600)))
            end
        end

        this.buildTimeLabel.text = timeStr
    end

    -- 升级按钮是否可按
    local curBuild = CGuildMgr.Inst.m_GuildInfo.CurBuild
    if not CGuildMgr.Inst.canUpdateWingRoom or not ((curBuild=="" or curBuild==nil) or (CGuildMgr.BUILD_TYPE_WingRoom == curBuild)) then
        this.startBuildBtn.Enabled = false
    end

    -- 判断满级的情况
    local stringLimitMax = LocalString.GetString("已满级")
    if CGuildMgr.Inst.m_GuildInfo.WingRoomNum >= CGuildMgr.Inst:GetMaxBuilding_All() then
        this.requireLabel.text = stringLimitMax
        this.requireLabel.color = Color.green
        --requireLabel2.gameObject.SetActive(false);
        this.limitLabel.text = stringLimitMax
        this.startBuildBtn.Enabled = false
    end
end
