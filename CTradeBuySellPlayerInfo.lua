-- Auto Generated!!
local CTradeBuySellPlayerInfo = import "L10.UI.CTradeBuySellPlayerInfo"
CTradeBuySellPlayerInfo.m_Init_CS2LuaHook = function (this, name, level, race, friend, color) 
    this.nameLabel.text = name
    this.levelLabel.text = level
    this.raceLabel.text = race
    this.friendLabel.text = friend
    this.nameLabel.color = color
    this.levelLabel.color = color
    this.raceLabel.color = color
    this.friendLabel.color = color
end
