local NGUIMath=import "NGUIMath"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local COpenEntryMgr=import "L10.Game.COpenEntryMgr"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local CUIFx=import "L10.UI.CUIFx"
local UICenterOnChild = import "UICenterOnChild"
local CUICenterOnChild = import "L10.UI.CUICenterOnChild"
local UIGrid = import "UIGrid"

local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local Constants = import "L10.Game.Constants"
local Vector3 = import "UnityEngine.Vector3"
local UISprite = import "UISprite"
local String = import "System.String"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local UITable = import "UITable"
local UILabel = import "UILabel"
local QnSelectableButton=import "L10.UI.QnSelectableButton"
local QnRadioBox=import "L10.UI.QnRadioBox"
--local DefaultPopupMenuListener = import "L10.UI.DefaultPopupMenuListener"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CScene = import "L10.Game.CScene"
local CTrackMgr = import "L10.Game.CTrackMgr"

CLuaWarehouseView = class()
--绑仓
RegistChildComponent(CLuaWarehouseView, "SwitchButton", GameObject)
-- RegistChildComponent(CLuaWarehouseView, "SwitchLabel", UILabel)
RegistChildComponent(CLuaWarehouseView, "PutAllBtn", GameObject)
RegistChildComponent(CLuaWarehouseView, "BindTipButton", GameObject)
RegistChildComponent(CLuaWarehouseView, "ChangeViewBtn", GameObject)


RegistChildComponent(CLuaWarehouseView, "RemainTimesLabel", GameObject)
RegistChildComponent(CLuaWarehouseView, "RemainTimesLabel2", GameObject)
RegistClassMember(CLuaWarehouseView,"m_ScrollView")
RegistClassMember(CLuaWarehouseView,"m_CenterOnChild")
RegistClassMember(CLuaWarehouseView,"m_Grid")
RegistClassMember(CLuaWarehouseView,"m_WarehouseTemplate")
RegistClassMember(CLuaWarehouseView,"m_ViewFrozenSilverBtn")
RegistClassMember(CLuaWarehouseView,"m_ArrangeBtn")
RegistClassMember(CLuaWarehouseView,"m_PageIndicatorTable")
RegistClassMember(CLuaWarehouseView,"m_PageIndicatorTemplate")
RegistClassMember(CLuaWarehouseView,"m_CurWarehouseBtn")
RegistClassMember(CLuaWarehouseView,"m_Arrow")
RegistClassMember(CLuaWarehouseView,"m_RemainTimesLabel")
RegistClassMember(CLuaWarehouseView,"m_RemainTimesLabel2")
RegistClassMember(CLuaWarehouseView,"m_ShowRemainTimesLabel")
RegistClassMember(CLuaWarehouseView,"m_Pages")
RegistClassMember(CLuaWarehouseView,"m_Indicators")
RegistClassMember(CLuaWarehouseView,"m_OnMoveItemFromWarehouseToBagAction")
RegistClassMember(CLuaWarehouseView,"m_CurIndex")
RegistClassMember(CLuaWarehouseView,"m_CurBangCangIndex")
RegistClassMember(CLuaWarehouseView,"m_OnCenterCallback")
RegistClassMember(CLuaWarehouseView,"m_OnFinishCallback")
RegistClassMember(CLuaWarehouseView,"m_CurrentView") --0:普通仓库 --1：绑定仓库/行囊
RegistClassMember(CLuaWarehouseView,"m_RefreshPackageView") --刷新包裹mask状态的接口
RegistClassMember(CLuaWarehouseView,"m_Inited")

RegistClassMember(CLuaWarehouseView,"m_BindRepoGuideFx")
RegistClassMember(CLuaWarehouseView,"m_FindNpcBtn")
RegistClassMember(CLuaWarehouseView,"m_ConstructingTag")


CLuaWarehouseView.HighlightedIndicatorSpriteName = "common_page_icon_highlight"
CLuaWarehouseView.NormalIndicatorSpriteName = "common_page_icon_normal"
CLuaWarehouseView.s_EnableFirstIndexDefaultSelected = false
CLuaWarehouseView.m_XingNangNum = 1
CLuaWarehouseView.m_MaxXingNangNum = 2
CLuaWarehouseView.m_XinNangStartId = 3
CLuaWarehouseView.m_BangCangStartId = 5
CLuaWarehouseView.m_BangCangNum = 1

function CLuaWarehouseView:GetCurRepoPageIdx()
    if(self.m_CurIndex >= 0 and self.m_CurIndex < #self.m_Pages)then
        return self.m_Pages[self.m_CurIndex + 1].m_LuaSelf.warehouseIndex
    end
end

function CLuaWarehouseView:InitWarehouse(_forced)
    if(self.m_Inited and not _forced)then
        return
    end

    self.m_Inited = true
    Extensions.RemoveAllChildren(self.m_Grid.transform)
    Extensions.RemoveAllChildren(self.m_PageIndicatorTable.transform)
    self.m_Pages = {}
    self.m_Indicators = {}
    local initial_centeron_index = 1
    local normalPageCount = 0
    if(CClientMainPlayer.Inst)then
            local availableNum = math.min(CClientMainPlayer.Inst.ItemProp.RepertoryAlreadyOpenNum, Constants.TotalWarehouseNum)
            for i = 1, availableNum, 1 do
                local inst = NGUITools.AddChild(self.m_Grid.gameObject, self.m_WarehouseTemplate)
                inst:SetActive(true)
                local page = inst:GetComponent(typeof(CCommonLuaScript))
                page.m_LuaSelf:InitWarehouse(i - 1)
                page.m_LuaSelf.OnRequestMoveItem = function(repoIdx, itemId)
                    self:OnRequestMoveItem(repoIdx, itemId)
                end
                table.insert(self.m_Pages, page)
                normalPageCount = normalPageCount + 1

                --添加指示器
                local indicator = NGUITools.AddChild(self.m_PageIndicatorTable.gameObject, self.m_PageIndicatorTemplate)
                indicator:SetActive(true)
                local sprite = indicator:GetComponent(typeof(UISprite))
                table.insert(self.m_Indicators, sprite)
            end

            --初始化行囊页，目前数量为2
            if(CSwitchMgr.IsEnableRiderPackage)then
                for i = 0, (CLuaWarehouseView.m_XingNangNum - 1), 1 do
                    local inst = NGUITools.AddChild(self.m_Grid.gameObject, self.m_WarehouseTemplate)
                    inst:SetActive(true)
                    local page = inst:GetComponent(typeof(CCommonLuaScript))
                    page.m_LuaSelf:InitWarehouse(CLuaWarehouseView.m_XinNangStartId + i)
                    page.m_LuaSelf.OnRequestMoveItem = function(repoIdx, itemId)
                        self:OnRequestMoveItem(repoIdx, itemId)
                    end
                    table.insert(self.m_Pages, page)
                    normalPageCount = normalPageCount + 1

                    --添加指示器
                    --[[local indicator = NGUITools.AddChild(self.m_PageIndicatorTable.gameObject, self.m_PageIndicatorTemplate)
                    indicator:SetActive(true)
                    local sprite = indicator:GetComponent(typeof(UISprite))
                    table.insert(self.m_Indicators, sprite)--]]
                end
            end
            
            if(availableNum > 1 and not self.s_EnableFirstIndexDefaultSelected)then
                local existSparseSpace = false
                for i = 1, availableNum, 1 do
                    self.m_CurIndex = i - 1
                    for j = 1, Constants.SingeWarehouseSize, 1 do
                        if(not CLuaWarehousePage.IsRepertoryPosValid( (i-1) * Constants.SingeWarehouseSize + j ) or String.IsNullOrEmpty(CClientMainPlayer.Inst.RepertoryProp:GetItemAt((i-1) * Constants.SingeWarehouseSize + j)) )then
                            existSparseSpace = true
                            break
                        end
                    end
                    if(existSparseSpace)then
                        break
                    end
                end
            else
                self.m_CurIndex = 0
            end

            --初始化绑仓页，目前数量为2
            for i = 0, (CLuaWarehouseView.m_BangCangNum - 1), 1 do
                local inst = NGUITools.AddChild(self.m_Grid.gameObject, self.m_WarehouseTemplate)
                inst:SetActive(true)
                local page = inst:GetComponent(typeof(CCommonLuaScript))
                page.m_LuaSelf:InitWarehouse(CLuaWarehouseView.m_BangCangStartId + i)
                page.m_LuaSelf.OnRequestMoveItem = function(repoIdx, itemId)
                    self:OnRequestMoveItem(repoIdx, itemId)
                end
                table.insert(self.m_Pages, page)

                --添加指示器
                local indicator = NGUITools.AddChild(self.m_PageIndicatorTable.gameObject, self.m_PageIndicatorTemplate)
                indicator:SetActive(true)
                local sprite = indicator:GetComponent(typeof(UISprite))
                table.insert(self.m_Indicators, sprite)
            end
            
        if(self.m_CurrentView == 0)then
            initial_centeron_index = self.m_CurIndex + 1
        else
            initial_centeron_index = normalPageCount + 1
        end

        self.m_Grid:Reposition()
        self.m_PageIndicatorTable.gameObject:SetActive(#self.m_Indicators > 1)
        if(self.m_PageIndicatorTable.gameObject.activeSelf)then
            self.m_PageIndicatorTable:Reposition()
        end
        self.m_ScrollView:ResetPosition()
        self.m_CenterOnChild:CenterOnInstant(self.m_Pages[initial_centeron_index].transform)
        
        self.m_Arrow.flip = UIBasicSprite.Flip.Nothing
        if(self.m_RemainTimesLabel ~= nil and self.m_RemainTimesLabel2 ~= nil)then
            self.m_RemainTimesLabel.enabled = self.m_ShowRemainTimesLabel
            local str = String.Format(LocalString.GetString("支持双击移动\n本周随身仓库还可打开[c][FFB800]{0}[-][/c]次"), CClientMainPlayer.Inst:GetLeftRepertoryOpenTimes())
            self.m_RemainTimesLabel.text = str
            self.m_RemainTimesLabel2.text = str
        end
    end
end

function CLuaWarehouseView:Awake()

    local bindrepo_setting = BindRepo_Setting.GetData()
    CLuaWarehouseView.m_BangCangNum = math.ceil(bindrepo_setting.MaxItemNum / Constants.SingeWarehouseSize)
    self.m_ShowRemainTimesLabel = false
    self.m_CurIndex = 0
    self.m_CurBangCangIndex = 0
    self.m_CurrentView = 0
    self.m_OnMoveItemFromWarehouseToBagAction = nil
    self.m_ScrollView = self.transform:Find("Offset/ScrollView"):GetComponent(typeof(CUIRestrictScrollView))
    local grid = self.transform:Find("Offset/ScrollView/Grid").gameObject
    self.m_Grid = grid:GetComponent(typeof(UIGrid))
    self.m_CenterOnChild = grid:GetComponent(typeof(CUICenterOnChild))
    self.m_WarehouseTemplate = self.transform:Find("Offset/ScrollView/WarehouseItem").gameObject
    self.m_PageIndicatorTemplate = self.transform:Find("Offset/IndicatorTemplate").gameObject
    self.m_ViewFrozenSilverBtn = self.transform:Find("Offset/FrozenBtn").gameObject
    self.m_ArrangeBtn = self.transform:Find("Offset/ArrangeBtn").gameObject
    self.m_CurWarehouseBtn = self.transform:Find("Offset/Title/CurWarehouseBtn"):GetComponent(typeof(QnSelectableButton))
    self.m_Arrow = self.transform:Find("Offset/Title/CurWarehouseBtn/Arrow"):GetComponent(typeof(UISprite))
    self.m_PageIndicatorTable = self.transform:Find("Offset/PageIndicatorTable"):GetComponent(typeof(UITable))
    self.m_FindNpcBtn = self.transform:Find("Offset/FindNpcBtn")
    self.m_ConstructingTag = self.transform:Find("Offset/FindNpcBtn/Constructing")
    self.m_RemainTimesLabel = nil
    self.m_RemainTimesLabel2 = nil
    if(self.RemainTimesLabel and self.RemainTimesLabel2)then
        self.m_ShowRemainTimesLabel = true
        self.m_RemainTimesLabel = self.RemainTimesLabel:GetComponent(typeof(UILabel))
        self.m_RemainTimesLabel2 = self.RemainTimesLabel2:GetComponent(typeof(UILabel))
    end

    self.m_WarehouseTemplate:SetActive(false)
    self.m_PageIndicatorTemplate:SetActive(false)
    UIEventListener.Get(self.BindTipButton).onClick = DelegateFactory.VoidDelegate(
        function(go)
            --TODO 换成仓库的填表
            g_MessageMgr:ShowMessage("BDCK_TIPS")
        end
    )
    UIEventListener.Get(self.m_ViewFrozenSilverBtn).onClick = DelegateFactory.VoidDelegate(
        function(go)
            self:OnViewFrozenSilverButtonClick(go)
        end
    )
    UIEventListener.Get(self.m_ArrangeBtn).onClick = DelegateFactory.VoidDelegate(
        function(go)
            self:OnArrangeButtonClick(go)
        end
    )
    
    if self.ChangeViewBtn then
        self.ChangeViewBtn:SetActive(IsOpenStoreRoom() and LuaStoreRoomMgr.IsAllowOpenStoreRoomFromRepo)
        UIEventListener.Get(self.ChangeViewBtn).onClick = DelegateFactory.VoidDelegate(
        function(go)
            if LuaStoreRoomMgr.IsAllowOpenStoreRoomFromRepo then
                Gac2Gas.RequestOpenStoreRoomFromRepoWnd()
            end
        end
        )
    end
    



    local levelMatch = CLuaBindRepoMgr.IsLevelMatch()
    self.SwitchButton:SetActive(levelMatch and IsBindRepoDisplayOpen())

    if levelMatch and IsBindRepoDisplayOpen() then
        if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.BindRepo)then
            COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.BindRepo)
            self.m_BindRepoGuideFx = self.SwitchButton.transform:Find("Fx"):GetComponent(typeof(CUIFx))
            self.m_BindRepoGuideFx:DestroyFx()
            self.m_BindRepoGuideFx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
            local path={Vector3(110,40,0),Vector3(110,-40,0),Vector3(-110,-40,0),Vector3(-110,40,0),Vector3(110,40,0)}
            local array = Table2Array(path, MakeArrayClass(Vector3))
            -- LuaTweenUtils.DOKill(self.m_BindRepoGuideFx.FxRoot, true)
            LuaUtils.SetLocalPosition(self.m_BindRepoGuideFx.FxRoot,110,40,0)
            LuaTweenUtils.DODefaultLocalPath(self.m_BindRepoGuideFx.FxRoot,array,2)
        end
    end

    UIEventListener.Get(self.PutAllBtn).onClick = DelegateFactory.VoidDelegate(
        function(go)
            Gac2Gas.MoveAllNormalItemToSimpleItems()
        end
    )
    --TODO 服务器更新后取消注释
    --Gac2Gas.RequestOpenBindRepoWnd()
end
function CLuaWarehouseView:Start()
    self.m_RadioBox = self.transform:Find("Offset/Title"):GetComponent(typeof(QnRadioBox))
    self.m_RadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        if index==0 then
            self:OnCurWarehouseButtonClick()
        elseif index==1 then
            self:OnSwitchWarehouse()
    
            if self.m_BindRepoGuideFx then
                self.m_BindRepoGuideFx:DestroyFx()
            end
        end
    end)
end

function CLuaWarehouseView:OnChangeTab(index)
    self.m_CurrentView = index
    self.m_RadioBox:ChangeTo(index,false)
    self:RefreshWarehouseViewWidget()
    --鞍囊不显示Indicator
    self.m_PageIndicatorTable.gameObject:SetActive(self.m_CurrentView == 0)
end

function CLuaWarehouseView:OnEnable()

    self:RegisterEvents()
    self:InitWarehouse(false)
    self:InitStoreRoomNpcBtn()
    self:RefreshWarehouseViewWidget()
end

function CLuaWarehouseView:OnDisable()
    --TODO
    CLuaWarehouseMenuMgr:CloseMenu()
    self:UnRegisterEvents()
    LuaStoreRoomMgr.IsAllowOpenStoreRoomFromRepo = false
end

function CLuaWarehouseView:OnDestroy()

    self.m_OnMoveItemFromWarehouseToBagAction = nil
    self.m_RefreshPackageView = nil
end

function CLuaWarehouseView:RegisterEvents()
    if(not self.m_OnCenterCallback)then
        self.m_OnCenterCallback = LuaUtils.OnCenterCallback(
            function(go)
                self:OnCenter(go)
            end
        )
    end
    self.m_CenterOnChild.onCenter = self.m_OnCenterCallback

    if(not self.m_OnFinishCallback)then
        self.m_OnFinishCallback = LuaUtils.OnFinished(
            function()
                self:OnFinished()
            end
        )
    end
    --CommonDefs.AddEventDelegate(self.m_CenterOnChild.onFinished, self.m_OnFinishCallback)
    self.m_CenterOnChild.onFinished = self.m_OnFinishCallback

    g_ScriptEvent:AddListener("OnWarehouseNumUpdate", self, "UpdateWarehouse")
    g_ScriptEvent:AddListener("SetIsAllowOpenStoreRoomFromRepo", self, "OnSetIsAllowOpenStoreRoomFromRepo")
end

function CLuaWarehouseView:UnRegisterEvents()
    g_ScriptEvent:RemoveListener("OnWarehouseNumUpdate", self, "UpdateWarehouse")
    g_ScriptEvent:RemoveListener("SetIsAllowOpenStoreRoomFromRepo", self, "OnSetIsAllowOpenStoreRoomFromRepo")
end

function CLuaWarehouseView:OnSetIsAllowOpenStoreRoomFromRepo(isAllow)
    if self.ChangeViewBtn then
        self.ChangeViewBtn:SetActive(isAllow)
    end
end

function CLuaWarehouseView:InitStoreRoomNpcBtn()
    if not self.m_FindNpcBtn or not self.m_ConstructingTag then
        return
    end
    local active = IsOpenStoreRoom()
    self.m_FindNpcBtn.gameObject:SetActive(active)
    self.m_SceneId2NpcInfo = {}
    self.m_DefaultNpcInfo ={
        sceneId = 16000003,
        npcId = 20001059,
        x = 93,
        y = 30
    }

    local storeRoomNpcId = StoreRoom_Setting.GetData().StoreRoomNpcId
    for i=0,storeRoomNpcId.Length-1,4 do
        local npc = {
            sceneId = storeRoomNpcId[i+1],
            npcId = storeRoomNpcId[i],
            x = storeRoomNpcId[i+2],
            y= storeRoomNpcId[i+3]
        }
        self.m_SceneId2NpcInfo[npc.sceneId] = npc
    end
    
    local unlockTaskId = StoreRoom_Setting.GetData().UnlockTaskTempId
    self.isConstructFinish = CClientMainPlayer.Inst and CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(unlockTaskId)
    if active  and CClientMainPlayer.Inst then
        local label = self.m_ConstructingTag:GetComponent(typeof(UILabel))
        if self.isConstructFinish then
            label.gameObject:SetActive(false)
        else
            label.gameObject:SetActive(true)
            if not CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eStoreRoomTaskProgress) then
                label.text = LocalString.GetString("待建设")
            else
                local data = CClientMainPlayer.Inst.PlayProp.PersistPlayData[EnumPersistPlayDataKey_lua.eStoreRoomTaskProgress].Data
                local p = data and MsgPackImpl.unpack(data) or 0
                if p == 0 then
                    label.text = LocalString.GetString("待建设")
                else
                    label.text = LocalString.GetString("建设中")
                end
            end
        end
    end

    UIEventListener.Get(self.m_FindNpcBtn.gameObject).onClick = DelegateFactory.VoidDelegate(
        function(go)
            self:OnClickFindNpcBtn()
        end
    )
end

function CLuaWarehouseView:OnClickFindNpcBtn()
    local curentSceneId = CScene.MainScene and CScene.MainScene.SceneTemplateId or 0
    local info = self.isConstructFinish and self.m_SceneId2NpcInfo[curentSceneId] or self.m_DefaultNpcInfo
    if not info then
        info = self.m_DefaultNpcInfo
    end
    local sceneName = PublicMap_PublicMap.GetData(info.sceneId).Name
    local npcName = NPC_NPC.GetData(info.npcId).Name

    g_MessageMgr:ShowMessage("StoreRoom_FindNpc_Msg",SafeStringFormat3("%s%s",sceneName,npcName))
    CTrackMgr.Inst:FindNPC(info.npcId, info.sceneId, info.x, info.y, nil, nil)
    CUIManager.CloseUI(CUIResources.PackageWnd)
end

function CLuaWarehouseView:RefreshWarehouseViewWidget()
    local normal_warehouse = (self.m_CurrentView == 0)
    self.PutAllBtn:SetActive(not normal_warehouse)
    self.BindTipButton:SetActive(not normal_warehouse)
    self.m_ViewFrozenSilverBtn:SetActive(normal_warehouse)
    -- self.SwitchLabel.text = normal_warehouse and LocalString.GetString("绑仓") or LocalString.GetString("仓库")
    if(self.m_ShowRemainTimesLabel and self.RemainTimesLabel and self.RemainTimesLabel2)then
        self.RemainTimesLabel:SetActive(self.m_CurrentView == 0)
        self.RemainTimesLabel2:SetActive(self.m_CurrentView == 1)
    end
    CLuaWarehouseMenuMgr:CloseMenu()
end

function CLuaWarehouseView:OnSwitchWarehouse()
    --切换仓库/绑仓
    self.m_CurrentView = 1
    self:RefreshWarehouseViewWidget()
    self:InitWarehouse(true)
end

function CLuaWarehouseView:OnCenter(go)
    local normalPageCount = 0
    for i = 1, #self.m_Pages, 1 do
        local isCentered = (go == self.m_Pages[i].gameObject)
        if self.m_Indicators[i] then
            self.m_Indicators[i].spriteName = isCentered and CLuaWarehouseView.HighlightedIndicatorSpriteName or CLuaWarehouseView.NormalIndicatorSpriteName
        end
        
        if (isCentered)then
            self.m_Pages[i].m_LuaSelf:LoadItems()
            local cur_repo_idx = self.m_Pages[i].m_LuaSelf.warehouseIndex

            if cur_repo_idx >= CLuaWarehouseView.m_BangCangStartId and self.m_CurrentView ~= 1 then
                self:OnChangeTab(1)
            elseif cur_repo_idx < CLuaWarehouseView.m_BangCangStartId and self.m_CurrentView ~= 0 then
                self:OnChangeTab(0)
            end
            --更新名字
            if self.m_CurrentView==0 then
                self.m_CurWarehouseBtn.Text = self:GetWareHouseDisplayName(cur_repo_idx)
            else
                self.m_CurWarehouseBtn.Text = self:GetWareHouseDisplayName(0)
            end

            if(cur_repo_idx >= CLuaWarehouseView.m_BangCangStartId and cur_repo_idx < CLuaWarehouseView.m_BangCangStartId + CLuaWarehouseView.m_BangCangNum)then
                self.m_CurBangCangIndex = i - 1
                self.m_CurIndex = i - 1
                if(self.m_RefreshPackageView)then
                    self.m_RefreshPackageView(true)
                end
            else
                self.m_CurIndex = i - 1
                if(self.m_RefreshPackageView)then
                    self.m_RefreshPackageView(false)
                end
                normalPageCount = normalPageCount + 1
            end
        end
    end
end

function CLuaWarehouseView:OnFinished()
   --新版的仓库使用label代替了弹出式提示
    -- if(CClientMainPlayer.Inst)then
    --     local availableNum = math.min(CClientMainPlayer.Inst.ItemProp.RepertoryAlreadyOpenNum, Constants.TotalWarehouseNum)
    --     local cur_repo_idx = self:GetCurRepoPageIdx()
    --     if (cur_repo_idx >= Constants.TotalWarehouseNum and cur_repo_idx < Constants.TotalWarehouseNum + CLuaWarehouseView.m_XingNangNum)then
    --         local carry_size = nil
    --         local has_zuoqi = false
    --         carry_size, has_zuoqi = CLuaWarehousePage.TryGetMaxZuoQiCarrySize()
    --         if(not has_zuoqi)then
    --             g_MessageMgr:ShowMessage("Set_Defult_ZuoQi")
    --         end
    --     end
    -- end
end

function CLuaWarehouseView:GetWareHouseDisplayName(_index)
    if not CClientMainPlayer.Inst then return end
    local availableNum = math.min(CClientMainPlayer.Inst.ItemProp.RepertoryAlreadyOpenNum, Constants.TotalWarehouseNum)
    local str = LocalString.GetString("仓库")
    local id = _index
    if(_index < Constants.TotalWarehouseNum)then
        id = _index + 1
    elseif(_index < Constants.TotalWarehouseNum + CLuaWarehouseView.m_XingNangNum)then
        str = LocalString.GetString("行囊")
        if(CLuaWarehouseView.m_XingNangNum > 1)then
            id = _index - Constants.TotalWarehouseNum + 1
        else
            id = ""
        end
    end
    if(CommonDefs.IS_VN_CLIENT and (id ~= ""))then
        return str.." "..id
    else
        return str..id
    end
end


function CLuaWarehouseView:OnViewFrozenSilverButtonClick(go)
    CUIManager.ShowUI(CUIResources.FrozenSilverWnd)
end

function CLuaWarehouseView:OnArrangeButtonClick(go)
    if(self.m_CurrentView == 0)then
        Gac2Gas.RepertoryReorder()
    elseif(self.m_CurrentView == 1)then
        Gac2Gas.SimpleItemRepoReorder()
    end
end

function CLuaWarehouseView:OnCurWarehouseButtonClick()
    if not CClientMainPlayer.Inst then
        return
    end
    if self.m_CurrentView==1 then
        self.m_CurrentView = 0
        self:RefreshWarehouseViewWidget()
        self:InitWarehouse(true)
    else--选择仓库
        local b = NGUIMath.CalculateRelativeWidgetBounds(self.m_CurWarehouseBtn.transform)
        local menus = {}

        local availableNum = math.min(CClientMainPlayer.Inst.ItemProp.RepertoryAlreadyOpenNum, Constants.TotalWarehouseNum)
        local show_lock = math.min(Constants.TotalWarehouseNum, availableNum + 1) > availableNum
        local canAddZuoQi = CSwitchMgr.IsEnableRiderPackage
        for i = 1, availableNum, 1 do
            table.insert(menus, {text = self:GetWareHouseDisplayName(i - 1), locked = false} )
        end

        if(canAddZuoQi)then
            for i = 0, (CLuaWarehouseView.m_XingNangNum - 1), 1 do
                table.insert(menus,{ text = self:GetWareHouseDisplayName(Constants.TotalWarehouseNum + i), locked = false})
            end
        end

        if(show_lock)then
            table.insert(menus, { text = self:GetWareHouseDisplayName(availableNum + 1), locked = true })
        end
        CLuaWarehouseMenuMgr:ShowOrCloseMenu(self.m_CurIndex, menus, self, self.m_CurWarehouseBtn.transform:TransformPoint(Vector3(b.center.x, b.center.y - b.extents.y, b.center.z) )  )
    end
end

function CLuaWarehouseView:UpdateWarehouse()
    self:InitWarehouse(true)
end

function CLuaWarehouseView:OnMenuAppear()
    self.m_Arrow.flip = UIBasicSprite.Flip.Vertically
end

function CLuaWarehouseView:OnMenuDisappear()
    self.m_Arrow.flip = UIBasicSprite.Flip.Nothing
end

function CLuaWarehouseView:OnMenuItemClick(selectedIndex)
    if(CClientMainPlayer.Inst)then
        local availableNum = math.min(CClientMainPlayer.Inst.ItemProp.RepertoryAlreadyOpenNum, Constants.TotalWarehouseNum)
        if(selectedIndex < availableNum)then
            --切换仓库
            if(selectedIndex >= 0 and selectedIndex < #self.m_Pages)then
                if ENABLE_XLUA then
                    cast(self.m_CenterOnChild, typeof(UICenterOnChild))
					self.m_CenterOnChild:CenterOn(self.m_Pages[selectedIndex + 1].transform)
					cast(self.m_CenterOnChild, typeof(CUICenterOnChild))
                else
                    self.m_CenterOnChild:CenterOn(self.m_Pages[selectedIndex + 1].transform)
                end
            end
        elseif(selectedIndex >= availableNum and selectedIndex < availableNum + CLuaWarehouseView.m_XingNangNum)then
            --切换到坐骑口袋
            if(selectedIndex >= 0 and CSwitchMgr.IsEnableRiderPackage)then
                if ENABLE_XLUA then
                    cast(self.m_CenterOnChild, typeof(UICenterOnChild))
					self.m_CenterOnChild:CenterOn(self.m_Pages[selectedIndex + 1].transform)
					cast(self.m_CenterOnChild, typeof(CUICenterOnChild))
                else
                    self.m_CenterOnChild:CenterOn(self.m_Pages[selectedIndex + 1].transform)
                end
            end
        else
            local pageIndex = CSwitchMgr.IsEnableRiderPackage and (selectedIndex - CLuaWarehouseView.m_XingNangNum) or selectedIndex
            local setting = GameSetting_Common.GetData()
            local prices = setting.RepertoryBuyPrice
            if(prices ~= nil and prices.Length >= pageIndex and pageIndex > 0)then
                MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("OPEN_REPERTORY_WITH_MONEY", prices[pageIndex - 1]), DelegateFactory.Action(function()
                    Gac2Gas.OpenNewRepertory()
                end), nil, nil, nil, false)
            end
        end
    end
    CUIManager.CloseUI(CUIResources.WarehouseMenu)
end

function CLuaWarehouseView:OnRequestMoveItem(repoIdx, itemId)
    if(self.m_OnMoveItemFromWarehouseToBagAction ~= nil)then
        self.m_OnMoveItemFromWarehouseToBagAction(repoIdx, itemId)
    end
end
