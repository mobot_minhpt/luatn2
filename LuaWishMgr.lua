local CUIResources = import "L10.UI.CUIResources"
local CShopMallMgr = import "L10.UI.CShopMallMgr"

LuaWishMgr = {}

function LuaWishMgr:OpenWishShop()
  LuaWishMgr.WishShop = true
  CShopMallMgr.SelectMallIndex = 0
  CShopMallMgr.SelectCategory = 0
  CUIManager.ShowUI(CUIResources.ShoppingMallWnd)
end

-------------------------------------

function Gas2Gac.Wish_AddWishNotifyUI(wishId, bSuccess)
    if bSuccess then
      g_ScriptEvent:BroadcastInLua("WishAddUpdate",wishId)
    end
end

function Gas2Gac.Wish_DelWishNotifyUI(wishId, bSuccess)
    if bSuccess then
      g_ScriptEvent:BroadcastInLua("WishDelUpdate",wishId)
      --g_MessageMgr:ShowMessage('CUSTOM_STRING3',LocalString.GetString('该心愿像流星一样消失了...'))
    end
end

function Gas2Gac.Wish_AddHelpNotifyUI(wishId, bSuccess)
    if bSuccess then
      g_ScriptEvent:BroadcastInLua("WishAddHelpUpdate",wishId)
    end
end

local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

function Gas2Gac.Wish_QueryWishLimitResult(bSuccess, num, nextTime)
    if bSuccess then
      if num > 0 then
        LuaWishMgr.RestWishNum = num
        CUIManager.ShowUI(CLuaUIResources.WishAddWnd)
      else
        local restTime = nextTime - CServerTimeMgr.Inst.timeStamp
        local restHour = math.floor(restTime/3600)
        local restMin = math.floor((restTime - restHour * 3600)/60)
        local restSec = math.floor(restTime % 60)
        local restDay = math.floor(restHour/24)
        if restDay > 0 then
          g_MessageMgr:ShowMessage("CUSTOM_STRING2",SafeStringFormat3(LocalString.GetString('%s天后可再次许愿'),restDay))
        else
          if restHour > 0 then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2",SafeStringFormat3(LocalString.GetString('%s小时后可再次许愿'),restHour))
          elseif restMin > 0 then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2",SafeStringFormat3(LocalString.GetString('%s分钟后可再次许愿'),restMin))
          else
            g_MessageMgr:ShowMessage("CUSTOM_STRING2",SafeStringFormat3(LocalString.GetString('%s秒后可再次许愿'),restSec))
          end
        end
      end
    end
end

function Gas2Gac.LuaQueryMonthPresentLimitResult(used, vipLimit, adjustLimit, context)
    local rest = vipLimit + adjustLimit - used
    LuaWishMgr.MonthSendRest = rest
    CUIManager.ShowUI(CLuaUIResources.WishItemWnd)
end
