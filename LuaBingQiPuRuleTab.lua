local UITable               = import "UITable"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local Extensions			= import "Extensions"
local CommonDefs            = import "L10.Game.CommonDefs"
local GameObject            = import "UnityEngine.GameObject"
local CMessageTipMgr		= import "L10.UI.CMessageTipMgr"
local CTipParagraphItem		= import "L10.UI.CTipParagraphItem"
local MessageMgr			= import "L10.Game.MessageMgr"

LuaBingQiPuRuleTab = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBingQiPuRuleTab, "Table", "Table", UITable)
RegistChildComponent(LuaBingQiPuRuleTab, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaBingQiPuRuleTab, "ParagraphTemplate", "ParagraphTemplate", GameObject)
RegistChildComponent(LuaBingQiPuRuleTab, "Indicator", "Indicator", UIScrollViewIndicator)

--@endregion RegistChildComponent end

function LuaBingQiPuRuleTab:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaBingQiPuRuleTab:Init()
    self:ParseRuleText()
end

function LuaBingQiPuRuleTab:ParseRuleText()
    Extensions.RemoveAllChildren(self.Table.transform)

    local msg =	MessageMgr.Inst:FormatMessage("BQP_RULE",{});
    if System.String.IsNullOrEmpty(msg) then
        return
    end

    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then return end

    --if info.titleVisible then
    --    local titleGo = CUICommonDef.AddChild(self.Table.gameObject, self.TitleTemplate)
    --    titleGo:SetActive(true)
    --    CommonDefs.GetComponent_GameObject_Type(titleGo, typeof(CTipTitleItem)):Init(info.title)
    --end

    do
        local i = 0
        while i < info.paragraphs.Count do
            local paragraphGo = CUICommonDef.AddChild(self.Table.gameObject, self.ParagraphTemplate)
            paragraphGo:SetActive(true)
			local tip = CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem))
            tip:Init(info.paragraphs[i], 4294967295)
            i = i + 1
        end
    end

    self.ScrollView:ResetPosition()
    self.Indicator:Layout()
    self.Table:Reposition()
end

--@region UIEvent

--@endregion UIEvent

