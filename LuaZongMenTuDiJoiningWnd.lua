local CShiTuMgr = import "L10.Game.CShiTuMgr"
local CIMMgr = import "L10.Game.CIMMgr"
local QnCheckBox = import "L10.UI.QnCheckBox"
local Object = import "System.Object"

LuaZongMenTuDiJoiningWnd = class()

RegistChildComponent(LuaZongMenTuDiJoiningWnd,"m_IntroductionLabel","IntroductionLabel", UILabel)
RegistChildComponent(LuaZongMenTuDiJoiningWnd,"m_ScrollView","ScrollView", UIScrollView)
RegistChildComponent(LuaZongMenTuDiJoiningWnd,"m_Grid1","Grid1", UIGrid)
RegistChildComponent(LuaZongMenTuDiJoiningWnd,"m_SubmitButton","SubmitButton",GameObject)
RegistChildComponent(LuaZongMenTuDiJoiningWnd,"m_Template","Template",GameObject)
RegistChildComponent(LuaZongMenTuDiJoiningWnd,"m_Grid2","Grid2", UIGrid)
RegistChildComponent(LuaZongMenTuDiJoiningWnd,"m_UnChooseBtn","UnChooseBtn",GameObject)

RegistClassMember(LuaZongMenTuDiJoiningWnd, "m_SelectedTudiDict")
RegistClassMember(LuaZongMenTuDiJoiningWnd, "m_TudiSectAndMingGeDict")

function LuaZongMenTuDiJoiningWnd:Init()
    self.m_Template:SetActive(false)
    self.m_IntroductionLabel.text = g_MessageMgr:FormatMessage("MenPai_TuDiJoining_ReadMe")
    UIEventListener.Get(self.m_UnChooseBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.CloseUI(CLuaUIResources.ZongMenTuDiJoiningWnd)
    end)
    UIEventListener.Get(self.m_SubmitButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnSubmitButtonClicked()
    end)
    self.m_SelectedTudiDict = {}
    self.m_Template:SetActive(false)
    local tudiList = CShiTuMgr.Inst:GetAllCurrentTuDi()
    local objectList = CreateFromClass(MakeGenericClass(List, Object))
    for i = 0, tudiList.Count - 1 do
        CommonDefs.ListAdd_LuaCall(objectList,tudiList[i])
    end
    local u = MsgPackImpl.pack(objectList)
    Gac2Gas.QueryTudiSectAndMingGe(u)
    self:UpdateBottomBtn()
end

function LuaZongMenTuDiJoiningWnd:InitTemplate(go, id)
    local info  = CIMMgr.Inst:GetBasicInfo(id)
    local isHasSect =  self.m_TudiSectAndMingGeDict[id].sectId ~= 0
    local isJianDingMinGe = self.m_TudiSectAndMingGeDict[id].mingGe ~= 0
    local canSelect = not isHasSect and isJianDingMinGe
    local name =info.Name
    local portraitName = CUICommonDef.GetPortraitName(info.Class, info.Gender, -1)
    go.transform:Find("Portrait").gameObject:GetComponent(typeof(CUITexture)):LoadNPCPortrait(portraitName, false)
    go.transform:Find("NameLabel").gameObject:GetComponent(typeof(UILabel)).text = name
    local infoLabel = go.transform:Find("InfoLabel").gameObject:GetComponent(typeof(UILabel))
    infoLabel.text = isHasSect and LocalString.GetString("已加入宗派") or infoLabel.text
    infoLabel.text = (not isJianDingMinGe) and LocalString.GetString("未鉴定命格") or infoLabel.text
    local checkbox = go.transform:Find("QnCheckBoxTel").gameObject:GetComponent(typeof(QnCheckBox))
    checkbox.gameObject:SetActive(canSelect)
    go.transform:Find("CannotSelectedSprite").gameObject:SetActive(not canSelect)
    infoLabel.gameObject:SetActive(not canSelect)
    checkbox.OnValueChanged = DelegateFactory.Action_bool(
        function(select)
            self:OnSelect(select, id)
        end
    )
    checkbox:SetSelected(false, true)
    go:SetActive(true)
end

function LuaZongMenTuDiJoiningWnd:OnSelect(select, id)
    self.m_SelectedTudiDict[id] = select
    self:UpdateBottomBtn()
end

function LuaZongMenTuDiJoiningWnd:UpdateBottomBtn()
    local isChooseAny = false
    for id, select in pairs(self.m_SelectedTudiDict) do
        isChooseAny = isChooseAny or select 
    end
    self.m_SubmitButton:SetActive(isChooseAny)
    self.m_UnChooseBtn:SetActive(not isChooseAny)
end

function LuaZongMenTuDiJoiningWnd:OnSubmitButtonClicked()
    local list = {}
    for id, select in pairs(self.m_SelectedTudiDict) do
        if select then
            table.insert(list, id)
        end
    end
    Gac2Gas.RequestInviteTudiGoinSect(MsgPackImpl.pack(Table2List(list, MakeGenericClass(List, Object))))
end

function LuaZongMenTuDiJoiningWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSyncTudiSectAndMingGe", self, "OnSyncTudiSectAndMingGe")
end

function LuaZongMenTuDiJoiningWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncTudiSectAndMingGe", self, "OnSyncTudiSectAndMingGe")
end

function LuaZongMenTuDiJoiningWnd:OnSyncTudiSectAndMingGe(dict)
    self.m_TudiSectAndMingGeDict = dict
    local tudiList = CShiTuMgr.Inst:GetAllCurrentTuDi()
    local len = tudiList.Count
    Extensions.RemoveAllChildren(self.m_Grid2.transform)
    Extensions.RemoveAllChildren(self.m_Grid1.transform)
    local relationshipProp = CClientMainPlayer.Inst.RelationshipProp
    local infoDic = relationshipProp.TuDi
    
    if len == 1 then
        local info = CommonDefs.DictGetValue(infoDic, typeof(UInt64), tudiList[0])
        local isWaiMen = info and info.Extra:GetBit(2) or false
        if not isWaiMen then
            self:InitTemplate(self.m_Template, tudiList[0])
        end
    else
        local grid = len == 2 and self.m_Grid2 or self.m_Grid1
        for i = 0, len - 1 do
            local info = CommonDefs.DictGetValue(infoDic, typeof(UInt64), tudiList[i])
            local isWaiMen = info and info.Extra:GetBit(2) or false
            if not isWaiMen then
                local go = NGUITools.AddChild(grid.gameObject, self.m_Template)
                self:InitTemplate(go, tudiList[i])
            end
        end
        grid:Reposition()
    end
end