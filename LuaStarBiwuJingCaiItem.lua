local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local QnRadioBox = import "L10.UI.QnRadioBox"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local AlignType = import "L10.UI.CTooltip.AlignType"
local WndType = import "L10.UI.WndType"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local BoxCollider = import "UnityEngine.BoxCollider"
LuaStarBiwuJingCaiItem=class()

RegistClassMember(LuaStarBiwuJingCaiItem,"RadioBoxYLabel")
RegistClassMember(LuaStarBiwuJingCaiItem,"RadioBoxNLabel")
RegistClassMember(LuaStarBiwuJingCaiItem,"RadioBoxRoot")

RegistClassMember(LuaStarBiwuJingCaiItem,"m_ItemIndex")
RegistClassMember(LuaStarBiwuJingCaiItem,"m_Choice")
RegistClassMember(LuaStarBiwuJingCaiItem,"m_Callback")

RegistClassMember(LuaStarBiwuJingCaiItem,"RadioBoxY")
RegistClassMember(LuaStarBiwuJingCaiItem,"RadioBoxN")

function LuaStarBiwuJingCaiItem:Awake()
    self.m_Choice = -1
end

function LuaStarBiwuJingCaiItem:Init(Root,Obj,gridList,Index,SingleChoiceList,name1,name2,resultList,shutdownStatus,rightChice)
    self.m_ItemIndex = Index

    local jingcaiText = StarBiWuShow_JingCai.GetData(gridList[Index]) 
    local go=NGUITools.AddChild(Root,Obj)
    go:SetActive(true)   
    local tipBtn = go.transform:Find("DetailBtn").gameObject
    tipBtn.gameObject:SetActive(false)
    local resultTip = go.transform:Find("ResultLabel"):GetComponent(typeof(UILabel))
    resultTip.gameObject:SetActive(false)
    local defaultValue = jingcaiText.DefaultValue
    if System.String.IsNullOrEmpty(name1) then
        name1 = defaultValue and defaultValue.Length >=1 and  defaultValue[0] or ""
    end
    if System.String.IsNullOrEmpty(name2) then
        name2 = defaultValue and defaultValue.Length >=2 and  defaultValue[1] or ""
    end
    if jingcaiText.ID > 24 then
        go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString(jingcaiText.Message), name1,name2)
    elseif not System.String.IsNullOrEmpty(jingcaiText.Threshold) then
        go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString(jingcaiText.Message), name1,tostring(jingcaiText.Threshold))
    else
        go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString(jingcaiText.Message), name1)
    end

    self.RadioBoxRoot = go.transform:Find("RadioBox"):GetComponent(typeof(QnRadioBox))
    self.RadioBoxYLabel = go.transform:Find("RadioBox/RadioBoxY/Label"):GetComponent(typeof(UILabel))
    self.RadioBoxNLabel = go.transform:Find("RadioBox/RadioBoxN/Label"):GetComponent(typeof(UILabel))

    local CorrectPercentage,ErrorPercentage
    if SingleChoiceList then
        CorrectPercentage = math.floor(SingleChoiceList[Index] * 100 + 0.5)
        ErrorPercentage = 100 - CorrectPercentage
    end
    self.RadioBoxYLabel.text = CorrectPercentage and SafeStringFormat3(LocalString.GetString("是(%d%s)"),CorrectPercentage, "%") or LocalString.GetString("是")
    self.RadioBoxNLabel.text = ErrorPercentage and SafeStringFormat3(LocalString.GetString("否(%d%s)"),ErrorPercentage, "%") or LocalString.GetString("否")

    if not System.String.IsNullOrEmpty(jingcaiText.Tip) then
        tipBtn.gameObject:SetActive(true)
        UIEventListener.Get(tipBtn).onClick = DelegateFactory.VoidDelegate(function(go)
            self:ShowTipMessage(go,jingcaiText.Tip)
        end)
    end

    -- if not System.String.IsNullOrEmpty(jingcaiText.ShowResult) and resultList and resultList[gridList[Index]] then
    --     if gridList[Index] == 4  or gridList[Index] == 2 or gridList[Index] == 6  then resultList[gridList[Index]] = tonumber(resultList[gridList[Index]]) * 100 end
    --     resultTip.gameObject:SetActive(true)
    --     resultTip.text = self:GetFormatResultStr(jingcaiText.ShowResult,tonumber(resultList[gridList[Index]]))
    -- end

    local callback = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnRadioBoxSelect(btn,index)
    end)

    self.RadioBoxRoot:Awake()
    self.RadioBoxRoot.m_RadioButtons[0]:SetSelected(false)
    self.RadioBoxRoot.OnSelect = callback
    if shutdownStatus == 3 and rightChice >= 0 then
        local rightChoiceList = {}
        local tmp = rightChice
        for i = 1, 4 do
            rightChoiceList[5 - i] = math.floor(tmp % 10) 
            tmp = tmp / 10
        end
        self.RadioBoxY = go.transform:Find("RadioBox/RadioBoxY"):GetComponent(typeof(QnSelectableButton))
        self.RadioBoxN = go.transform:Find("RadioBox/RadioBoxN"):GetComponent(typeof(QnSelectableButton))
        self.RadioBoxY:SetSelected(rightChoiceList[Index + 1] > 0)
        self.RadioBoxN:SetSelected(rightChoiceList[Index + 1] <= 0)
        self.RadioBoxY.gameObject:GetComponent(typeof(BoxCollider)).enabled = false
        self.RadioBoxN.gameObject:GetComponent(typeof(BoxCollider)).enabled = false
        self.RadioBoxYLabel.text = LocalString.GetString("是")
        self.RadioBoxNLabel.text = LocalString.GetString("否")
        -- if gridList[Index] > 9 then
        --     resultTip.gameObject:SetActive(true)
        --     resultTip.text = rightChoiceList[Index + 1] > 0 and LocalString.GetString("是") or LocalString.GetString("否")
        -- end
    end
end
function LuaStarBiwuJingCaiItem:InitHistoryItem(Root,Obj,gridList,rightChoiceList,Index,name1,name2,resultList)
    self.m_ItemIndex = Index
    local jingcaiText = StarBiWuShow_JingCai.GetData(gridList[Index]) 
    local go=NGUITools.AddChild(Root,Obj)
    go:SetActive(true)   
    
    local tipBtn = go.transform:Find("DetailBtn").gameObject
    tipBtn.gameObject:SetActive(false)
    local resultTip = go.transform:Find("ResultLabel"):GetComponent(typeof(UILabel))
    resultTip.gameObject:SetActive(false)
    local defaultValue = jingcaiText.DefaultValue
    if System.String.IsNullOrEmpty(name1) then
        name1 = defaultValue and defaultValue.Length >=1 and  defaultValue[0] or ""
    end
    if System.String.IsNullOrEmpty(name2) then
        name2 = defaultValue and defaultValue.Length >=2 and  defaultValue[1] or ""
    end
    if jingcaiText.ID > 24 then
        go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString(jingcaiText.Message), name1,name2)
    elseif not System.String.IsNullOrEmpty(jingcaiText.Threshold) then
        go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString(jingcaiText.Message), name1,jingcaiText.Threshold)
    else
        go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString(jingcaiText.Message), name1)
    end

    local hasJingCai = resultList and resultList[1] or false

    self.RadioBoxRoot = go.transform:Find("RadioBox"):GetComponent(typeof(QnRadioBox))
    self.RadioBoxYLabel = go.transform:Find("RadioBox/RadioBoxY/Label"):GetComponent(typeof(UILabel))
    self.RadioBoxNLabel = go.transform:Find("RadioBox/RadioBoxN/Label"):GetComponent(typeof(UILabel))
    self.RadioBoxY = go.transform:Find("RadioBox/RadioBoxY"):GetComponent(typeof(QnSelectableButton))
    self.RadioBoxN = go.transform:Find("RadioBox/RadioBoxN"):GetComponent(typeof(QnSelectableButton))
    self.RadioBoxY:SetSelected(rightChoiceList[Index] > 0 and hasJingCai)
    self.RadioBoxN:SetSelected(rightChoiceList[Index] <= 0 and hasJingCai)
    self.RadioBoxY.gameObject:GetComponent(typeof(BoxCollider)).enabled = false
    self.RadioBoxN.gameObject:GetComponent(typeof(BoxCollider)).enabled = false
    -- local CorrectPercentage,ErrorPercentage
    -- if SingleChoiceList then
    --     CorrectPercentage = math.floor(SingleChoiceList[Index] * 100 + 0.5)
    --     ErrorPercentage = 100 - CorrectPercentage
    -- end
    self.RadioBoxYLabel.text = LocalString.GetString("是")
    self.RadioBoxNLabel.text = LocalString.GetString("否")

    self.RadioBoxRoot.enabled = false
    self.RadioBoxRoot.enabled = false
    if not System.String.IsNullOrEmpty(jingcaiText.Tip) then
        tipBtn.gameObject:SetActive(true)
        UIEventListener.Get(tipBtn).onClick = DelegateFactory.VoidDelegate(function(go)
            self:ShowTipMessage(go,jingcaiText.Tip)
        end)
    end

    -- if not System.String.IsNullOrEmpty(jingcaiText.ShowResult) and resultList and resultList[gridList[Index]] then
    --     if gridList[Index] == 4  or gridList[Index] == 2 or gridList[Index] == 6  then resultList[gridList[Index]] = tonumber(resultList[gridList[Index]]) * 100 end
    --     resultTip.gameObject:SetActive(true)
    --     resultTip.text = self:GetFormatResultStr(jingcaiText.ShowResult,tonumber(resultList[gridList[Index]]))
    -- end
    -- if gridList[Index] > 9 then
    --     resultTip.gameObject:SetActive(true)
    --     resultTip.text = rightChoiceList[Index] > 0 and LocalString.GetString("是") or LocalString.GetString("否")
    -- end
    -- local callback = DelegateFactory.Action_QnButton_int(function(btn,index)
    --     self:OnRadioBoxSelect(btn,index)
    -- end)

    -- self.RadioBoxRoot:Awake()
    -- self.RadioBoxRoot.m_RadioButtons[0]:SetSelected(false)
    -- self.RadioBoxRoot.OnSelect = callback
end
function LuaStarBiwuJingCaiItem:OnRadioBoxSelect(btn, index) 
    self.m_Choice = index
    self:UpdateChiceBtn()
end

function LuaStarBiwuJingCaiItem:ShowTipMessage(btn,msg)
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info then
        CMessageTipMgr.Inst:Display(WndType.Tooltip, false, "", info.paragraphs, 640, btn.transform, AlignType.Top, 4294967295)
    end    
end

function LuaStarBiwuJingCaiItem:UpdateChiceBtn()
    if self.m_Callback then
        self.m_Callback(self.m_ItemIndex,self.m_Choice)
    end
end

function LuaStarBiwuJingCaiItem:GetFormatResultStr(resultFormat,Value)
    if resultFormat == "TenThousand" then
        if Value >= 10000 then
            local val = Value / 10000
            val = math.ceil(val * 10) / 10
            return SafeStringFormat3(LocalString.GetString("%.1f万"),val)
        else
            return tostring(Value)
        end 
    elseif resultFormat == "Time" then
        local sceonds = Value
        if sceonds >= 60 then
            return SafeStringFormat3(LocalString.GetString("%d分%d秒"),math.floor(sceonds / 60),math.ceil(sceonds % 60))
        else
            return SafeStringFormat3(LocalString.GetString("%d秒"),math.ceil(sceonds))
        end
    else
        Value = math.ceil(Value * 10) / 10
        Value = Value - Value % 0.01
        return SafeStringFormat3(resultFormat,tostring(Value))
    end
end

