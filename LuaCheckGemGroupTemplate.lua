local DefaultItemActionDataSource=import "L10.UI.DefaultItemActionDataSource"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local AlignType1 = import "L10.UI.CTooltip+AlignType"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CUITexture=import "L10.UI.CUITexture"
local Alignment=import "NGUIText.Alignment"

CLuaCheckGemGroupTemplate = class()
RegistClassMember(CLuaCheckGemGroupTemplate,"nameLabel")
RegistClassMember(CLuaCheckGemGroupTemplate,"currentTag")
RegistClassMember(CLuaCheckGemGroupTemplate,"table")
RegistClassMember(CLuaCheckGemGroupTemplate,"gemTemplate")
RegistClassMember(CLuaCheckGemGroupTemplate,"checkButton")
RegistClassMember(CLuaCheckGemGroupTemplate,"gemGroup")
RegistClassMember(CLuaCheckGemGroupTemplate,"gemList")
RegistClassMember(CLuaCheckGemGroupTemplate,"gemIdList")
RegistClassMember(CLuaCheckGemGroupTemplate,"OnGemGroupSelected")
RegistClassMember(CLuaCheckGemGroupTemplate,"OnRequestInlayGem")
RegistClassMember(CLuaCheckGemGroupTemplate,"m_Equipment")
RegistClassMember(CLuaCheckGemGroupTemplate,"m_GemItemPool")

function CLuaCheckGemGroupTemplate:Awake()
    self.nameLabel = self.transform:Find("Name"):GetComponent(typeof(UILabel))
    self.currentTag = self.transform:Find("Name/Current").gameObject
    self.table = self.transform:Find("Table"):GetComponent(typeof(UITable))
    self.checkButton = self.transform:Find("Name").gameObject
    self.gemGroup = nil
    self.gemList = {}
    self.gemIdList = {}
    self.OnGemGroupSelected = nil
    self.OnRequestInlayGem = nil

    UIEventListener.Get(self.checkButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnCheckButtonClick(go)
    end)
end

function CLuaCheckGemGroupTemplate:Init( id) 
    self.m_Equipment = CItemMgr.Inst:GetById(CEquipmentProcessMgr.Inst.SelectEquipment.itemId).Equip

    self.gemList={}
    self.gemIdList={}

    self.nameLabel.text = ""
    for i = self.table.transform.childCount - 1, 0, - 1 do
        self.m_GemItemPool:Recycle(self.table.transform:GetChild(i).gameObject)
    end

    self.gemGroup = GemGroup_GemGroup.GetData(id)
    if self.gemGroup == nil then
        return
    end
    self:InitNameLabelColor(self.gemGroup)

    self.nameLabel.text = self.gemGroup.Name

    for i=1,self.gemGroup.Consist.Length do
        local instance = self.m_GemItemPool:GetFromPool(0)
        instance:SetActive(true)
        instance.transform.parent = self.table.transform
        instance.transform.localScale = Vector3.one
        local item = Item_Item.GetData(self.gemGroup.Consist[i-1])
        if item ~= nil then
            table.insert( self.gemList, instance )
            table.insert( self.gemIdList,item.ID )
        end
    end
    self.table:Reposition()
    for i,v in ipairs(self.gemList) do
        self:InitItem(v.transform,self.gemIdList[i])
    end
end

function CLuaCheckGemGroupTemplate:InitNameLabelColor( gemGroup) 
    self.currentTag:SetActive(false)
    local equip = CItemMgr.Inst:GetById(CEquipmentProcessMgr.Inst.SelectEquipment.itemId)
    local equipSetIndex =  equip.Equip.ActiveHoleSetIdx==2 and 2 or 1
    if equipSetIndex~=CLuaCheckGemGroupWnd.m_HoleSetIndex then
        return
    end
    if equip ~= nil then
        local showTag = true
        local gemInEquip = {}

        local holeCount = equip.Equip.Hole
        local holeItems = {}

        if equip.Equip.ActiveHoleSetIdx<=1 then
            for i=1,holeCount do
                table.insert( holeItems, equip.Equip.HoleItems[i])
            end
        else
            for i=1,holeCount do
                local id = equip.Equip.SecondaryHoleItems[i]
                if id>0 and id<100 then
                    table.insert( holeItems, equip.Equip.HoleItems[id])
                else
                    table.insert( holeItems, equip.Equip.SecondaryHoleItems[i])
                end
            end
        end

        --查看当前套
        for i=1,holeCount do
            local jewelInHole = Jewel_Jewel.GetData(holeItems[i])
            if jewelInHole ~= nil then
                gemInEquip[jewelInHole.JewelKind] = true
            end
        end

        for i=1,gemGroup.Consist.Length do
            local jewel = Jewel_Jewel.GetData(gemGroup.Consist[i-1])
            if jewel ~= nil and not gemInEquip[jewel.JewelKind] then
                showTag = false
                break
            end
        end

        local count = 0
        for k,v in pairs(gemInEquip) do
            count = count+1
        end
        if showTag and gemGroup.Consist.Length == count then
            self.currentTag:SetActive(true)
        end
    end
end

--是否需要显示镶嵌按钮
function CLuaCheckGemGroupTemplate:NeedShowInlayButton( templateId ) 
    local itemInfo = CEquipmentProcessMgr.Inst.SelectEquipment
    local commonItem = CItemMgr.Inst:GetById(itemInfo.itemId)
    if not commonItem then return false end
    local equip = commonItem.Equip

    local have = false
    local holeItems = {}
    local jewel = Jewel_Jewel.GetData(templateId)

    if CLuaCheckGemGroupWnd.m_HoleSetIndex==2 then
        for i=1,equip.Hole do
            table.insert( holeItems, equip.SecondaryHoleItems[i])
        end
    else
        for i=1,equip.Hole do
            table.insert( holeItems, equip.HoleItems[i])
        end
    end

    for i=1,equip.Hole do--看我自己有没有
        if holeItems[i]>0 then
            if  holeItems[i]<100 then--引用
                local jewelInHole = Jewel_Jewel.GetData(equip.HoleItems[holeItems[i]])
                if jewelInHole and jewel.JewelKind == jewelInHole.JewelKind then
                    have=true
                    break
                end
            else
                local jewelInHole = Jewel_Jewel.GetData(holeItems[i])
                if jewelInHole and jewel.JewelKind == jewelInHole.JewelKind then
                    have=true
                    break
                end
            end
        end
    end
    if have then--如果我有了，则镶嵌的时候用的是包裹里的
        return false
    else--如果我没有，则看另一套有没有
        if CLuaCheckGemGroupWnd.m_HoleSetIndex==2 then--查第一套有没有
            for i=1,equip.Hole do
                local jewelInHole = Jewel_Jewel.GetData(equip.HoleItems[i])
                if jewelInHole and jewel.JewelKind == jewelInHole.JewelKind  then
                    have=true
                    return true
                end
            end
        else
            if equip.HoleSetCount==2 then--查第二套有没有
                for i=1,equip.Hole do
                    local tempId = equip.SecondaryHoleItems[i]
                    if tempId>100 then
                        local jewelInHole = Jewel_Jewel.GetData(tempId)
                        if jewelInHole and jewel.JewelKind == jewelInHole.JewelKind then
                            have=true
                            return true
                        end
                    end
                end
            end
        end
                
        if not have then--如果对方也没有，则看包裹有没有
            local amount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, templateId)
            return amount>0
        end
    end

    return false

end
function CLuaCheckGemGroupTemplate:OnGemClick( go) 
    if self.OnGemGroupSelected ~= nil then
        self.OnGemGroupSelected(self.gameObject)
    end
    if CEquipmentProcessMgr.Inst.SelectEquipment == nil then
        return
    end



    for i,v in ipairs(self.gemList) do
        if go==v then
            local templateId = self.gemIdList[i]
            local names={}
            local actions={}

            local needShow = self:NeedShowInlayButton(templateId)
            if needShow then
                table.insert( names,LocalString.GetString("镶嵌"))
                table.insert( actions,function()
                    if self.OnRequestInlayGem then
                        self.OnRequestInlayGem(templateId)
                    end
                end)
            end
            table.insert( names,LocalString.GetString("获取途径"))
            table.insert( actions,function()
                CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, true, self.transform, AlignType1.Right)
            end)
            local actionSource=DefaultItemActionDataSource.Create(#names,actions,names)
            CLuaItemInfoWnd.s_FromCheckGemGroupWnd = true
            CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, actionSource, AlignType.Default, 0, 0, 0, 0)
        end
    end
end

function CLuaCheckGemGroupTemplate:OnCheckButtonClick( go) 
    CLuaGemGroupInfoWnd.GemGroupId = self.gemGroup.ID
    CUIManager.ShowUI(CUIResources.GemGroupInfoWnd)
    if self.OnGemGroupSelected ~= nil then
        self.OnGemGroupSelected(self.gameObject)
    end
end

function CLuaCheckGemGroupTemplate:OnEnable()
    g_ScriptEvent:AddListener("SetItemAt",self,"OnSetItemAt")
    g_ScriptEvent:AddListener("SendItem",self,"OnSendItem")
end
function CLuaCheckGemGroupTemplate:OnDisable()
    g_ScriptEvent:RemoveListener("SetItemAt",self,"OnSetItemAt")
    g_ScriptEvent:RemoveListener("SendItem",self,"OnSendItem")
end

function CLuaCheckGemGroupTemplate:OnSetItemAt(args)
    local place, pos, oldItemId, newItemId = args[0],args[1],args[2],args[2]
    self:InitNameLabelColor(self.gemGroup)

    for i,v in ipairs(self.gemList) do
        self:InitItem(v.transform,self.gemIdList[i])
    end
end

function CLuaCheckGemGroupTemplate:OnSendItem(args)
    local itemId = args[0]
    self:InitNameLabelColor(self.gemGroup)

    for i,v in ipairs(self.gemList) do
        self:InitItem(v.transform,self.gemIdList[i])
    end
end

function CLuaCheckGemGroupTemplate:InitItem(transform,id)
    local iconTexture = transform:Find("bg/Texture"):GetComponent(typeof(CUITexture))
    local selectSprite = transform:Find("select"):GetComponent(typeof(UISprite))
    local countLabel = transform:Find("bg/Count"):GetComponent(typeof(UILabel))
    local colorTint = transform:Find("bg"):GetComponent(typeof(UISprite))

    local equip = CItemMgr.Inst:GetById(CEquipmentProcessMgr.Inst.SelectEquipment.itemId).Equip

    iconTexture.material = nil
    selectSprite.enabled = false
    -- countLabel.text = ""

    -- self.gemId = id
    local gem = Item_Item.GetData(id)
    local jewel = Jewel_Jewel.GetData(id)
    if gem == nil then
        return
    end
    iconTexture:LoadMaterial(gem.Icon)

    local bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(id)
    local ownCount = bindCount + notBindCount
    --还要看有没有公用



    countLabel.alignment = Alignment.Right
    countLabel.text = tostring(ownCount)
    countLabel.color = ownCount > 0 and Color.white or Color.red

    colorTint.color = Color(1, 1, 1, 0.3)

    local holeItems = {}
    if CLuaCheckGemGroupWnd.m_HoleSetIndex<=1 then
        for i=1,equip.Hole do
            table.insert( holeItems, equip.HoleItems[i])
        end
    else
        for i=1,equip.Hole do
            table.insert( holeItems, equip.SecondaryHoleItems[i])
        end
    end

    local have = false
    for i=1,equip.Hole do--看有没有
        if holeItems[i]>0 then
            if  holeItems[i]<100 then--引用
                local jewelInHole = Jewel_Jewel.GetData(equip.HoleItems[holeItems[i]])
                if jewelInHole and jewel.JewelKind == jewelInHole.JewelKind then
                    colorTint.color = Color.white
                    have=true
                    break
                end
            else
                local jewelInHole = Jewel_Jewel.GetData(holeItems[i])
                if jewelInHole and jewel.JewelKind == jewelInHole.JewelKind then
                    colorTint.color = Color.white
                    have=true
                    break
                end
            end
        end
    end
    if not have then
        --如果当前是第二套，则需要查看第一套
        local JewelKind = Jewel_Jewel.GetData(id).JewelKind
        if CLuaCheckGemGroupWnd.m_HoleSetIndex==2 then
            for i=1,equip.Hole do
                local tempId = equip.HoleItems[i]
                if tempId>0 then
                    local jewelInfo1 = Jewel_Jewel.GetData(tempId)
                    if jewelInfo1.JewelKind==JewelKind then
                        countLabel.alignment = Alignment.Left
                        countLabel.text = "lv."..jewelInfo1.JewelLevel
                        countLabel.color = Color.white
                        break
                    end
                end
            end
        else
            --第一套也需要查看第二套
            if equip.HoleSetCount==2 then
                for i=1,equip.Hole do
                    local tempId = equip.SecondaryHoleItems[i]
                    if tempId>100 then
                        local jewelInfo1 = Jewel_Jewel.GetData(tempId)
                        if jewelInfo1.JewelKind==JewelKind then
                            countLabel.alignment = Alignment.Left
                            countLabel.text = "lv."..jewelInfo1.JewelLevel
                            countLabel.color = Color.white
                            break
                        end
                    end
                end
            end
        end
    end

    UIEventListener.Get(transform.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnGemClick(go)
    end)
end
