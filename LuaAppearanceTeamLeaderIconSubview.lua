local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local Profession = import "L10.Game"
local EnumGender = import "L10.Game.EnumGender"
local UISprite = import "UISprite"
local LuaTweenUtils = import "LuaTweenUtils"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaAppearanceTeamLeaderIconSubview = class()

RegistChildComponent(LuaAppearanceTeamLeaderIconSubview,"m_TeamLeaderIconItem", "CommonItemCell", GameObject)
RegistChildComponent(LuaAppearanceTeamLeaderIconSubview,"m_ItemDisplay", "AppearanceCommonItemDisplayView", CCommonLuaScript)
RegistChildComponent(LuaAppearanceTeamLeaderIconSubview,"m_TeamLeaderDisplaySubview", "AppearanceTeamLeaderDisplaySubview", CCommonLuaScript)
RegistChildComponent(LuaAppearanceTeamLeaderIconSubview,"m_ContentGrid", "ContentGrid", UIGrid)
RegistChildComponent(LuaAppearanceTeamLeaderIconSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)

RegistClassMember(LuaAppearanceTeamLeaderIconSubview, "m_AllIcons")
RegistClassMember(LuaAppearanceTeamLeaderIconSubview, "m_SelectedDataId")

function LuaAppearanceTeamLeaderIconSubview:Awake()
end

function LuaAppearanceTeamLeaderIconSubview:Init()
    self:PlayAppearAnimation()
    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceTeamLeaderIconSubview:PlayAppearAnimation()
    self.m_TeamLeaderDisplaySubview.gameObject:SetActive(true)
    LuaTweenUtils.TweenAlpha(self.m_TeamLeaderDisplaySubview.transform:GetComponent(typeof(UIWidget)), 0, 1, 0.8)
    self.m_TeamLeaderDisplaySubview:Init()
end

function LuaAppearanceTeamLeaderIconSubview:LoadData()
    self.m_AllIcons = LuaAppearancePreviewMgr:GetAllTeamLeaderIconInfo(false)

    Extensions.RemoveAllChildren(self.m_ContentGrid.transform)
    self.m_ContentGrid.gameObject:SetActive(true)

    for i = 1, # self.m_AllIcons do
        local child = CUICommonDef.AddChild(self.m_ContentGrid.gameObject, self.m_TeamLeaderIconItem)
        child:SetActive(true)
        self:InitItem(child, self.m_AllIcons[i])
    end
    self.m_ContentGrid:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceTeamLeaderIconSubview:SetDefaultSelection()
    self.m_SelectedDataId = LuaAppearancePreviewMgr:GetCurrentInUseTeamLeaderIcon()
end

function LuaAppearanceTeamLeaderIconSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end

    local childCount = self.m_ContentGrid.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        local appearanceData = self.m_AllIcons[i+1]
        if appearanceData.id == self.m_SelectedDataId then
            self:OnItemClick(childGo)
            break
        end
    end
end

function LuaAppearanceTeamLeaderIconSubview:InitItem(itemGo, appearanceData)
    local iconTexture = itemGo.transform:GetComponent(typeof(CUITexture))
    local disabledGo = itemGo.transform:Find("Disabled").gameObject
    local lockedGo = itemGo.transform:Find("Locked").gameObject
    local cornerGo = itemGo.transform:Find("Corner").gameObject
    local selectedGo = itemGo.transform:Find("Selected").gameObject
    iconTexture:LoadMaterial(appearanceData.icon)
    
    if CClientMainPlayer.Inst then
        lockedGo:SetActive(not LuaAppearancePreviewMgr:MainPlayerHasTeamLeaderIcon(appearanceData.id))
        cornerGo:SetActive(LuaAppearancePreviewMgr:IsCurrentInUseTeamLeaderIcon(appearanceData.id))
    else
        lockedGo:SetActive(false)
        cornerGo:SetActive(false)
    end
    selectedGo:SetActive(self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id)
    disabledGo:SetActive(lockedGo.activeSelf)
    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go)
    end)
end

function LuaAppearanceTeamLeaderIconSubview:OnItemClick(go)
    local childCount = self.m_ContentGrid.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:Find("Selected").gameObject:SetActive(true)
            self.m_SelectedDataId = self.m_AllIcons[i+1].id
        else
            childGo.transform:Find("Selected").gameObject:SetActive(false)
        end
    end
    self:UpdateItemDisplay()
end

function LuaAppearanceTeamLeaderIconSubview:UpdateItemDisplay()
    self.m_ItemDisplay.gameObject:SetActive(true)
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    if id==0 then
        self.m_TeamLeaderDisplaySubview:SetLeaderIcon("")
        self.m_TeamLeaderDisplaySubview:SetItemName("")
        self.m_ItemDisplay:Clear()
        return
    end

    local appearanceData = nil
    for i=1,#self.m_AllIcons do
        if self.m_AllIcons[i].id == id then
            appearanceData = self.m_AllIcons[i]
            break
        end
    end
    self.m_TeamLeaderDisplaySubview:SetLeaderIcon(appearanceData.sprite)
    self.m_TeamLeaderDisplaySubview:SetItemName(appearanceData.name)
    
    local icon = appearanceData.icon
    local disabled = true
    local locked = true
    local buttonTbl = {}
    local exist = LuaAppearancePreviewMgr:MainPlayerHasTeamLeaderIcon(appearanceData.id)
    local inUse = LuaAppearancePreviewMgr:IsCurrentInUseTeamLeaderIcon(appearanceData.id)
    locked = not exist
    disabled = not exist

    if exist then
        if inUse then
            table.insert(buttonTbl, {text=LocalString.GetString("脱下"), isYellow=false, action=function(go) self:OnRemoveButtonClick() end})
        else
            table.insert(buttonTbl, {text=LocalString.GetString("更换"), isYellow=false, action=function(go) self:OnApplyButtonClick() end})
        end
    else
        table.insert(buttonTbl, {text=LocalString.GetString("获取"), isYellow=true, action=function(go) self:OnMoreButtonClick(go) end})
    end

    self.m_ItemDisplay:Init(icon, appearanceData.name, SafeStringFormat3(LocalString.GetString("风尚度+%d"), appearanceData.fashionability), LuaAppearancePreviewMgr:GetTeamLeaderIconConditionText(appearanceData.id), true, disabled, locked, buttonTbl)
end

function LuaAppearanceTeamLeaderIconSubview:OnApplyButtonClick()
    local exist = LuaAppearancePreviewMgr:MainPlayerHasTeamLeaderIcon(self.m_SelectedDataId)
    if exist then
        LuaAppearancePreviewMgr:RequestSetTeamLeaderIcon(self.m_SelectedDataId)
    end
end

function LuaAppearanceTeamLeaderIconSubview:OnRemoveButtonClick()
    LuaAppearancePreviewMgr:RequestSetTeamLeaderIcon(0)
end

function LuaAppearanceTeamLeaderIconSubview:OnMoreButtonClick(go)
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    local appearanceData = nil
    for i=1,#self.m_AllIcons do
        if self.m_AllIcons[i].id == id then
            appearanceData = self.m_AllIcons[i]
            break
        end
    end
    if appearanceData then
        LuaItemAccessListMgr:ShowItemAccessInfoAtLeft(appearanceData.itemGetId, true, go.transform)
    end
end

function  LuaAppearanceTeamLeaderIconSubview:OnEnable()
    g_ScriptEvent:AddListener("OnUnLockTeamLeaderIconSuccess", self, "OnUnLockTeamLeaderIconSuccess")
    g_ScriptEvent:AddListener("OnUseTeamLeaderIconSuccess", self, "OnUseTeamLeaderIconSuccess")
end

function  LuaAppearanceTeamLeaderIconSubview:OnDisable()
    g_ScriptEvent:RemoveListener("OnUnLockTeamLeaderIconSuccess", self, "OnUnLockTeamLeaderIconSuccess")
    g_ScriptEvent:RemoveListener("OnUseTeamLeaderIconSuccess", self, "OnUseTeamLeaderIconSuccess")
end

function LuaAppearanceTeamLeaderIconSubview:OnUnLockTeamLeaderIconSuccess(id)
    self:LoadData()
end

function LuaAppearanceTeamLeaderIconSubview:OnUseTeamLeaderIconSuccess(id)
    self:SetDefaultSelection()
    self:LoadData()
end
