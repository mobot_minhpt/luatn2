
local YuanDan2020_Setting           = import "L10.Game.YuanDan2020_Setting"
local CWordFilterMgr                = import "L10.Game.CWordFilterMgr"
local CUICommonDef                  = import "L10.UI.CUICommonDef"

LuaGuildVoteThanksWnd = class()

--------RegistChildComponent-------
RegistChildComponent(LuaGuildVoteThanksWnd,           "InputContent",           UIInput)
RegistChildComponent(LuaGuildVoteThanksWnd,           "VoteBtn",                GameObject)

---------RegistClassMember-------
RegistClassMember(LuaGuildVoteThanksWnd,              "m_ContentMaxInput")
function LuaGuildVoteThanksWnd:Init()
    self.InputContent.value = YuanDan2020_Setting.GetData().ThankContent
    self.m_ContentMaxInput = YuanDan2020_Setting.GetData().ThankContentMaxLength
    UIEventListener.Get(self.VoteBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        if self:CheckInputValue() then
            Gac2Gas.RequestYuanDanGuildVoteThankSpeech(luaGuildVoteMgr.QuestionId,self.InputContent.value)
            CUIManager.CloseUI(CLuaUIResources.GuildVoteThanksWnd)
        end
    end)
end

function LuaGuildVoteThanksWnd:CheckInputValue()
    local ThanksStr = string.gsub(self.InputContent.value, " ", "")
	if CommonDefs.IS_VN_CLIENT then
		ThanksStr = self.InputContent.value
	end
    self.InputContent.value = ThanksStr

    local ContentLength = CUICommonDef.GetStrByteLength(ThanksStr)
    if ContentLength <= 0 or ContentLength > self.m_ContentMaxInput then
        g_MessageMgr:ShowMessage("Guild_Vote_Content_Length_Not_Fit")
        return false
    end

    if not CWordFilterMgr.Inst:CheckName(ThanksStr) then
        g_MessageMgr:ShowMessage("Guild_Vote_VIOLATION")
        return false
    end
    return true
end