local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnRadioBox = import "L10.UI.QnRadioBox"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local CButton = import "L10.UI.CButton"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local TouchPhase = import "UnityEngine.TouchPhase"
local Ease = import "DG.Tweening.Ease"
local TweenScale = import "TweenScale"
local Object = import "System.Object"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local DefaultItemActionDataSource = import "L10.UI.DefaultItemActionDataSource"
local AlignType2 = import "L10.UI.CTooltip+AlignType"

LuaCookBookWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCookBookWnd, "QnRadioBox", "QnRadioBox", QnRadioBox)
RegistChildComponent(LuaCookBookWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaCookBookWnd, "MainTexture", "MainTexture", CUITexture)
RegistChildComponent(LuaCookBookWnd, "UnFinishedRoot", "UnFinishedRoot", GameObject)
RegistChildComponent(LuaCookBookWnd, "UnFinishedLabel", "UnFinishedLabel", UILabel)
RegistChildComponent(LuaCookBookWnd, "FoodItem", "FoodItem", GameObject)
RegistChildComponent(LuaCookBookWnd, "FoodRoot", "FoodRoot", UIGrid)
RegistChildComponent(LuaCookBookWnd, "DragItem", "DragItem", CUITexture)
RegistChildComponent(LuaCookBookWnd, "CookButton", "CookButton", CButton)
RegistChildComponent(LuaCookBookWnd, "FinishedRoot", "FinishedRoot", GameObject)
RegistChildComponent(LuaCookBookWnd, "ReadMeLabel", "ReadMeLabel", UILabel)
RegistChildComponent(LuaCookBookWnd, "Stars", "Stars", GameObject)
RegistChildComponent(LuaCookBookWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaCookBookWnd, "FoodButton", "FoodButton", GameObject)
RegistChildComponent(LuaCookBookWnd, "OpenGiftButton", "OpenGiftButton", GameObject)
RegistChildComponent(LuaCookBookWnd, "GiftNumLabel", "GiftNumLabel", UILabel)
RegistChildComponent(LuaCookBookWnd, "CookMethodRoot", "CookMethodRoot", UIGrid)
RegistChildComponent(LuaCookBookWnd, "OpenGiftFx", "OpenGiftFx", CUIFx)
RegistChildComponent(LuaCookBookWnd, "OpenGiftResultRoot", "OpenGiftResultRoot", UIPanel)
RegistChildComponent(LuaCookBookWnd, "OpenGiftResultTable", "OpenGiftResultTable", UITable)
RegistChildComponent(LuaCookBookWnd, "OpenGiftResultItem", "OpenGiftResultItem", GameObject)
RegistChildComponent(LuaCookBookWnd, "CookButtonFx", "CookButtonFx", CUIFx)
RegistChildComponent(LuaCookBookWnd, "GuangFx", "GuangFx", CUIFx)
--@endregion RegistChildComponent end
RegistClassMember(LuaCookBookWnd,"m_CookBookData")
RegistClassMember(LuaCookBookWnd,"m_FoodRootRadioBox")
RegistClassMember(LuaCookBookWnd,"m_CookMethodRootRadioBox")
RegistClassMember(LuaCookBookWnd,"m_FoodRootRadioBoxIndex")
RegistClassMember(LuaCookBookWnd,"m_CookMethodRootRadioBoxIndex")
RegistClassMember(LuaCookBookWnd,"m_SelectItemList")
RegistClassMember(LuaCookBookWnd,"m_SelectMethodList")
RegistClassMember(LuaCookBookWnd,"m_LastSelectTabIndex")
RegistClassMember(LuaCookBookWnd,"m_IsDragging")
RegistClassMember(LuaCookBookWnd,"m_LastDragPos")
RegistClassMember(LuaCookBookWnd,"m_FingerIndex")
RegistClassMember(LuaCookBookWnd,"m_DragItemIndex")
RegistClassMember(LuaCookBookWnd,"m_DragItemIsMethod")
RegistClassMember(LuaCookBookWnd,"m_AddonFiveStarWeightFoodDict")

function LuaCookBookWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.MainTexture.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMainTextureClick()
	end)
	
	UIEventListener.Get(self.CookButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCookButtonClick()
	end)

	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

	UIEventListener.Get(self.FoodButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFoodButtonClick()
	end)

	UIEventListener.Get(self.OpenGiftButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOpenGiftButtonClick()
	end)

    --@endregion EventBind end
end

function LuaCookBookWnd:Init()
	self.m_SelectItemList = {}
	self.m_SelectMethodList = {}
	self.GiftNumLabel.text = SafeStringFormat3("x%d",LuaCookBookMgr.m_IngredientPackCount)
	self.UnFinishedLabel.text = CUICommonDef.TranslateToNGUIText(g_MessageMgr:FormatMessage("CookBookWnd_Undeveloped"))
	self.FoodItem.gameObject:SetActive(false)
	self.FoodItem.transform:Find("Highlighted").gameObject:SetActive(false)
	self.DragItem.gameObject:SetActive(false)
	self.OpenGiftResultItem.gameObject:SetActive(false)
	self.OpenGiftResultRoot.alpha = 0
	self.GuangFx:LoadFx("fx/ui/prefab/UI_caipu_guang.prefab")
	self:InitTab()
	self:InitAddonFiveStarWeightFoodDict()
end

function LuaCookBookWnd:InitAddonFiveStarWeightFoodDict()
	self.m_AddonFiveStarWeightFoodDict = {}
	ShangShi_Ingredient.Foreach(function (key,val)
		if string.find(val.Name,LocalString.GetString("特级")) then
			self.m_AddonFiveStarWeightFoodDict[val.Group] = val.ID
		end
	end)
end

function LuaCookBookWnd:Update()
	if nil == self.m_LastDragPos then
        self.m_LastDragPos = Vector3.zero
    end
    if not self.m_IsDragging then
        return
    end

	if CommonDefs.IsInMobileDevice() then
        for i = 0,Input.touchCount - 1 do
            local touch = Input.GetTouch(i)
            if touch.fingerId == self.m_FingerIndex then
                if touch.phase ~= TouchPhase.Ended and touch.phase ~= TouchPhase.Canceled then
                    if touch.phase ~= TouchPhase.Stationary then
						local pos = touch.position
                        self.m_LastDragPos = Vector3(pos.x, pos.y, 0)
                        self.DragItem.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_LastDragPos)
                    end
                    return
                else
                    break
                end
            end
        end
    else
        if Input.GetMouseButton(0) then
            self.DragItem.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
            self.m_LastDragPos = Input.mousePosition
            if not Input.GetMouseButtonUp(0) then
                return
            end
        end
    end
    self.DragItem.gameObject:SetActive(false)
    local hoveredObject = CUICommonDef.SelectedUIWithRacast
    if hoveredObject then
		self:OnDragEnd(hoveredObject)
    end
    self.m_IsDragging = false
    self.m_FingerIndex = -1
	self.m_DragItemIndex = -1
end

function LuaCookBookWnd:OnDragEnd(hoveredObject)
	local isMethod = self.m_DragItemIsMethod
	local newIndex = - 1
	if hoveredObject.transform.parent == self.FoodRoot.transform and self.m_FoodRootRadioBox then
		for i = 1,self.m_FoodRootRadioBox.m_RadioButtons.Length do
			local btn = self.m_FoodRootRadioBox.m_RadioButtons[i - 1]
			if btn.gameObject == hoveredObject.gameObject then
				if not isMethod and i ~= self.m_DragItemIndex then
					local temp = self.m_SelectItemList[i]
					self.m_SelectItemList[i] = self.m_SelectItemList[self.m_DragItemIndex]
					self.m_SelectItemList[self.m_DragItemIndex] = temp
					newIndex = i
					self.m_FoodRootRadioBox:ChangeTo(newIndex - 1,false)
					break
				elseif isMethod then
					g_MessageMgr:ShowMessage("CookBookWnd_DragEnd_TypeError")
					break
				end
			end
		end	
	elseif hoveredObject.transform.parent == self.CookMethodRoot.transform and self.m_CookMethodRootRadioBox then
		for i = 1,self.m_CookMethodRootRadioBox.m_RadioButtons.Length do
			local btn = self.m_CookMethodRootRadioBox.m_RadioButtons[i - 1]
			if btn.gameObject == hoveredObject.gameObject then
				if isMethod and i ~= self.m_DragItemIndex then
					local temp = self.m_SelectMethodList[i]
					self.m_SelectMethodList[i] = self.m_SelectMethodList[self.m_DragItemIndex]
					self.m_SelectMethodList[self.m_DragItemIndex] = temp
					newIndex = i
					self.m_CookMethodRootRadioBox:ChangeTo(newIndex - 1,false)
					break
				elseif not isMethod then
					g_MessageMgr:ShowMessage("CookBookWnd_DragEnd_TypeError")
					break
				end
			end
		end	
	end
	if newIndex < 0 then return end
	self:ReInitFoodItem(newIndex - 1, isMethod)
	self:ReInitFoodItem(self.m_DragItemIndex - 1, isMethod)
end

function LuaCookBookWnd:OnEnable()
	g_ScriptEvent:AddListener("OnCookBookSelectIngredientId", self, "OnCookBookSelectIngredientId")
	g_ScriptEvent:AddListener("OnShangShiSendOpenIngredientPackResult", self, "OnShangShiSendOpenIngredientPackResult")
	g_ScriptEvent:AddListener("OnOpenIngredientPackResultWndClose", self, "OnOpenIngredientPackResultWndClose")
	g_ScriptEvent:AddListener("OnShangShiSendMyFoodIngredient", self, "OnShangShiSendMyFoodIngredient")
	g_ScriptEvent:AddListener("OnShangShiSendCookbookData", self, "OnShangShiSendCookbookData")
	g_ScriptEvent:AddListener("OnShangShiSendCookingResult", self, "OnShangShiSendCookingResult")
end

function LuaCookBookWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnCookBookSelectIngredientId", self, "OnCookBookSelectIngredientId")
	g_ScriptEvent:RemoveListener("OnShangShiSendOpenIngredientPackResult", self, "OnShangShiSendOpenIngredientPackResult")
	g_ScriptEvent:RemoveListener("OnOpenIngredientPackResultWndClose", self, "OnOpenIngredientPackResultWndClose")
	g_ScriptEvent:RemoveListener("OnShangShiSendMyFoodIngredient", self, "OnShangShiSendMyFoodIngredient")
	g_ScriptEvent:RemoveListener("OnShangShiSendCookbookData", self, "OnShangShiSendCookbookData")
	g_ScriptEvent:RemoveListener("OnShangShiSendCookingResult", self, "OnShangShiSendCookingResult")
	LuaCookBookMgr.m_DefaultCookbookId = 0
	self:CancelOpenGiftFxTick()
	LuaTweenUtils.DOKill(self.transform, false)
end

function LuaCookBookWnd:OnShangShiSendCookingResult()
	-- local key = self.m_LastSelectTabIndex + 1
	-- local data = self.m_CookBookData[key]
	-- self.m_SelectMethodList = {}
	-- self.m_SelectItemList = {}
	-- for i = 1,data.Method.Length do
	-- 	self:ReInitFoodItem(i - 1, true)
	-- end
	-- for i = 1,data.IngredientGroup.Length do
	-- 	self:ReInitFoodItem(i - 1, false)
	-- end
	self.QnRadioBox:ChangeTo(self.m_LastSelectTabIndex, true)
	self:ShowCookButtonFx()
end

function LuaCookBookWnd:OnShangShiSendCookbookData()
	self.GiftNumLabel.text = SafeStringFormat3("x%d",LuaCookBookMgr.m_IngredientPackCount)
	self.QnRadioBox:ChangeTo(self.m_LastSelectTabIndex, true)
end

function LuaCookBookWnd:OnShangShiSendMyFoodIngredient()
	local key = self.m_LastSelectTabIndex + 1
	local data = self.m_CookBookData[key]
	for i = 1,data.Method.Length do
		local t = self.m_SelectMethodList[i]
		if t then
			local id = t.id
			self.m_SelectMethodList[i].count = LuaCookBookMgr.m_MyFoodIngredientData[id].count
			self:ReInitFoodItem(i - 1, true)
		end
	end
	for i = 1,data.IngredientGroup.Length do
		local t = self.m_SelectItemList[i]
		if t then
			local id = t.id
			self.m_SelectItemList[i].count = LuaCookBookMgr.m_MyFoodIngredientData[id].count
			self:ReInitFoodItem(i - 1, false)
		end
	end
	self.GiftNumLabel.text = SafeStringFormat3("x%d",LuaCookBookMgr.m_IngredientPackCount)
end

function LuaCookBookWnd:OnShangShiSendOpenIngredientPackResult()
	LuaCookBookMgr:InitMyFoodIngredientData()
	Extensions.RemoveAllChildren(self.OpenGiftResultTable.transform)
	for i = 1,#LuaCookBookMgr.m_OpenIngredientPackResult do
		local t = LuaCookBookMgr.m_OpenIngredientPackResult[i]
		local data = LuaCookBookMgr.m_MyFoodIngredientData[t.ingredientId]
		local obj = NGUITools.AddChild(self.OpenGiftResultTable.gameObject, self.OpenGiftResultItem.gameObject)
		obj.gameObject:SetActive(true)
		local iconPath = SafeStringFormat3("UI/Texture/Item_Other/Material/%s.mat",data.data.Icon)
		obj.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(iconPath)
		obj.transform:Find("NumLabel"):GetComponent(typeof(UILabel)).text = data.data.Name
		obj.transform:Find("New").gameObject:SetActive(t.isNew)
	end
	self.OpenGiftResultTable:Reposition()
end

function LuaCookBookWnd:OnOpenIngredientPackResultWndClose()
	local posArray = {}
	local itemArray = {}
	self.OpenGiftResultRoot.alpha = 1
	for i = 0,self.OpenGiftResultTable.transform.childCount - 1 do
		local tweenerScale = self.OpenGiftResultTable.transform:GetChild(i):GetComponent(typeof(TweenScale))
		tweenerScale.enabled = true
		table.insert(posArray,tweenerScale.transform.position)
		table.insert(itemArray,tweenerScale.transform)
	end
	local tweener = LuaTweenUtils.TweenFloat(0, 1, 1, function(val)
        self.OpenGiftResultRoot.alpha = 1 - val
		local btnPos = self.FoodButton.transform.position
		for i = 1,self.OpenGiftResultTable.transform.childCount do
			local oldPos = posArray[i]
			local item = itemArray[i]
			local x = math.lerp(oldPos.x, btnPos.x, val)
			local y = math.lerp(oldPos.y, btnPos.y, val)
			item.transform.position = Vector3(x, y, item.transform.position.z)
		end
    end)
    LuaTweenUtils.SetEase(tweener, Ease.InOutQuint)
	LuaTweenUtils.SetTarget(tweener,self.transform)
	LuaTweenUtils.OnComplete(tweener, function ()
		local tween = self.FoodButton:GetComponent(typeof(TweenScale))
		tween.enabled = true
		tween.transform.localScale = Vector3(1, 1, 1)
		tween.from = Vector3(1, 1, 1)
		tween.to = Vector3(1.08, 1.08, 1)
		tween:PlayForward()
		Extensions.RemoveAllChildren(self.OpenGiftResultTable.transform)
	end)
	self:ShowCookButtonFx()
end

function LuaCookBookWnd:ShowCookButtonFx()
	local key = self.m_LastSelectTabIndex + 1
	local data = self.m_CookBookData[key]
	local t = LuaCookBookMgr.m_UnlockedCookbookData[key]
	local isFinished = not t.isCookSucc
	if isFinished then
		for i = 1,data.Method.Length do
			local t = self.m_SelectMethodList[i]
			if t then
				if t.count == 0 then
					isFinished = false
					break
				end
			else
				isFinished = false
				break
			end
		end
	end
	if isFinished then
		for i = 1,data.IngredientGroup.Length do
			local t = self.m_SelectItemList[i]
			if t then
				if t.count == 0 then
					isFinished = false
					break
				end
			else
				isFinished = false
				break
			end
		end
	end
	if isFinished then
		self.CookButtonFx:DestroyFx()
		self.CookButtonFx.OnLoadFxFinish = DelegateFactory.Action(function()
			self.CookButtonFx.fx.transform.localPosition = Vector3(-5,-2,0)
			self.CookButtonFx.fx.transform.localScale = Vector3(0.7,1,1)
		end)
		self.CookButtonFx:LoadFx("fx/ui/prefab/UI_zbsjf_goumaianniu.prefab")
	else
		self.CookButtonFx:DestroyFx()
	end
end

function LuaCookBookWnd:OnCookBookSelectIngredientId(selectId, count)
	local selectData = LuaCookBookMgr.m_MyFoodIngredientData[selectId]

	local selectItem = {id = selectId, count = count, data = selectData.data}
	local key = self.m_LastSelectTabIndex + 1
	local data = self.m_CookBookData[key]
	local isMethod = selectData.data.IsMethod == 1
	local len = isMethod and data.Method.Length or data.IngredientGroup.Length
	for i = 1,len do
		local item = self.m_SelectItemList[i]
		if isMethod then
			item = self.m_SelectMethodList[i]
		end
		if item and item.id == selectId then
			if isMethod then
				self.m_SelectMethodList[i] = nil
			else
				self.m_SelectItemList[i] = nil
			end
			self:ReInitFoodItem(i - 1, isMethod)
		end
	end
	local index = self.m_FoodRootRadioBoxIndex
	if isMethod then
		index = self.m_CookMethodRootRadioBoxIndex
		self.m_SelectMethodList[index + 1] = selectItem
	else
		self.m_SelectItemList[index + 1] = selectItem
	end
	self:ReInitFoodItem(index, isMethod)
	self:ShowCookButtonFx()
end

function LuaCookBookWnd:ReInitFoodItem(index, isMethod)
	local key = self.m_LastSelectTabIndex + 1
	local data = self.m_CookBookData[key]
	local t = LuaCookBookMgr.m_UnlockedCookbookData[key]
	local go = (isMethod and self.CookMethodRoot or self.FoodRoot).transform:GetChild(index).gameObject
	local info = isMethod and data.Method[index] or data.IngredientGroup[index]
	local curSelectItem = self.m_SelectItemList[index + 1]
	if isMethod then
		curSelectItem = self.m_SelectMethodList[index + 1] 
	end
	self:InitFoodItem(go, info, t.isCookSucc,curSelectItem)
end

function LuaCookBookWnd:InitTab()
	self.m_CookBookData = {}
	ShangShi_Cookbook.ForeachKey(function(key)
		local data = ShangShi_Cookbook.GetData(key)
		self.m_CookBookData[key] = data
		local btn = self.QnRadioBox.m_RadioButtons[key - 1]
		btn.Text = data.Name
		--self.QnRadioBox.m_RadioButtons[key - 1].transform:Find("Selected/SelectedLabel"):GetComponent(typeof(UILabel)).text = data.Name
		local t = LuaCookBookMgr.m_UnlockedCookbookData[key]
		btn.transform:Find("LockSprite").gameObject:SetActive(t == nil)
		btn.m_Label.gameObject:SetActive(t ~= nil)
	end)
	self.QnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
		self:OnSelectTab(btn,index)
	end)
	self.QnRadioBox:ChangeTo(self:GetDefaultSelectTabIndex(), true)
end

function LuaCookBookWnd:GetDefaultSelectTabIndex()
	local list = {}
	local defaultIndex = -1
	for key, _ in pairs(LuaCookBookMgr.m_UnlockedCookbookData) do
		if key == LuaCookBookMgr.m_DefaultCookbookId then
			defaultIndex = key - 1
		end
		table.insert(list,LuaCookBookMgr.m_UnlockedCookbookData[key])
	end
	table.sort(list, function (a, b)
		if a.isCookSucc and not b.isCookSucc then 
			return false 
		elseif b.isCookSucc and not a.isCookSucc then
			return true
		elseif a.star == 5 and b.star ~= 5 then 
			return false
		elseif a.star ~= 5 and b.star == 5 then 
			return true
		end
		return a.cookbookId < b.cookbookId
	end)
	return (defaultIndex >= 0) and (defaultIndex) or (#list > 0 and list[1].cookbookId - 1 or 0)
end

--@region UIEvent

function LuaCookBookWnd:OnCookButtonClick()
	local key = self.m_LastSelectTabIndex + 1
	local data = self.m_CookBookData[key]
	local t = LuaCookBookMgr.m_UnlockedCookbookData[key]
	if t and t.star == 5 then
		g_MessageMgr:ShowMessage("SHANGSHI_COOKBOOK_ALREADY_FIVE_STAR")
		return 
	end
	local isFinished = true
	local hasAddonFiveStarWeightItem = false
	local list = {}
	for i = 1,data.IngredientGroup.Length do
		local curSelectItem = self.m_SelectItemList[i]
		if not curSelectItem then
			isFinished = false
			break
		end
		if not hasAddonFiveStarWeightItem then
			local groupId = tonumber(data.IngredientGroup[i - 1][0]) 
			local addonFiveStarWeightId = self.m_AddonFiveStarWeightFoodDict[groupId]
			if addonFiveStarWeightId then
				if LuaCookBookMgr.m_MyFoodIngredientData[addonFiveStarWeightId].count > 0 and addonFiveStarWeightId ~= curSelectItem.data.ID then
					hasAddonFiveStarWeightItem = true
				end
			end
		end
		table.insert(list,curSelectItem.data.ID)
		table.insert(list,1)
	end
	if isFinished then
		for i = 1,data.Method.Length do
			local curSelectItem = self.m_SelectMethodList[i]
			if not curSelectItem then
				isFinished = false
				break
			end
			table.insert(list,curSelectItem.data.ID)
			table.insert(list,1)
		end
	end
	if not isFinished then
		g_MessageMgr:ShowMessage("CookBookWnd_UnFinished")
		return
	end
	local msg = g_MessageMgr:FormatMessage(hasAddonFiveStarWeightItem and "SHANGSHI_COOK_TOP_INGREDIENT_UNSELECT" or "CookBookWnd_CookConfirm")
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		Gac2Gas.ShangShiStartCooking(data.ID,MsgPackImpl.pack(Table2List(list, MakeGenericClass(List, Object))))
	end),nil,nil,nil,false)
end

function LuaCookBookWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("CookBookWnd_ReadMe")
end

function LuaCookBookWnd:OnFoodButtonClick()
	CUIManager.ShowUI(CLuaUIResources.MyShangShiIngredientWnd)
end

function LuaCookBookWnd:OnOpenGiftButtonClick()
	if LuaCookBookMgr.m_IngredientPackCount == 0 then
		g_MessageMgr:ShowMessage("CookBookWnd_NoneIngredientPack")
		return
	end
	local key = self.m_LastSelectTabIndex + 1
	local data = self.m_CookBookData[key]
	self:CancelOpenGiftFxTick()
	self.OpenGiftResultRoot.alpha = 1
	self.OpenGiftFx:DestroyFx()
	self.OpenGiftFx:LoadFx("fx/ui/prefab/UI_caipu_shicaibao.prefab")
	self.m_CancelOpenGiftFxTick = RegisterTickOnce(function ()
		Gac2Gas.ShangShiOpenIngredientPack(data.ID)
	end,200)
end

--@endregion UIEvent
function LuaCookBookWnd:OnMainTextureClick()
	local key = self.m_LastSelectTabIndex + 1
	local data = self.m_CookBookData[key]
	local t = LuaCookBookMgr.m_UnlockedCookbookData[key]
	if t.star == 0 then return end
	LuaCookBookMgr.m_ShangShiSendCookingResult = {cookbookId = data.ID, isSucc = true, star = t.star}
    CUIManager.ShowUI(CLuaUIResources.CookingResultWnd)
end

function LuaCookBookWnd:CancelOpenGiftFxTick()
	if self.m_CancelOpenGiftFxTick then
		UnRegisterTick(self.m_CancelOpenGiftFxTick)
		self.m_CancelOpenGiftFxTick = nil
	end
end

function LuaCookBookWnd:OnSelectTab(btn,index)

	local key = index + 1
	local data = self.m_CookBookData[key]
	local t = LuaCookBookMgr.m_UnlockedCookbookData[key]

	if t == nil then
		g_MessageMgr:ShowMessage("CookBookWnd_Select_LockedTab")
		if self.m_LastSelectTabIndex then
			self.QnRadioBox:ChangeTo(self.m_LastSelectTabIndex, false)
		end
		return
	end
	for i = 0,self.QnRadioBox.m_RadioButtons.Length - 1 do
		local t = LuaCookBookMgr.m_UnlockedCookbookData[i + 1]
		self.QnRadioBox.m_RadioButtons[i].m_Label.gameObject:SetActive(t ~= nil and i ~= index)
		self.QnRadioBox.m_RadioButtons[i].transform:Find("NormalSprite").gameObject:SetActive(i ~= index)
	end
	self.m_LastSelectTabIndex = index

	self.NameLabel.text = data.Name
	self.ReadMeLabel.text = CUICommonDef.TranslateToNGUIText(data.Desc)
	self.MainTexture:LoadMaterial(data.Icon)
	for i = 1, 5 do
		self.Stars.transform:GetChild(i - 1):GetComponent(typeof(UISprite)).enabled = i <= t.star
	end
	Extensions.SetLocalPositionZ(self.MainTexture.transform, t.isCookSucc and 0 or -1)
	self.MainTexture.color = t.isCookSucc and Color.white or NGUIText.ParseColor32("6868683C", 0)
	self.UnFinishedRoot.gameObject:SetActive(not t.isCookSucc)
	self.FinishedRoot.gameObject:SetActive(t.isCookSucc)
	self.m_SelectItemList = {}
	self.m_SelectMethodList = {}
	if t.isCookSucc then
		for i = 1,data.Method.Length do
			local selectId = ShangShi_Ingredient.GetDataBySubKey("Group",data.Method[i  - 1][0]).ID
			local selectData = LuaCookBookMgr.m_MyFoodIngredientData[selectId]
			self.m_SelectMethodList[i] = {id = selectId, count = selectData.count, data = selectData.data}
		end
		for i = 1,data.IngredientGroup.Length do
			local selectId = ShangShi_Ingredient.GetDataBySubKey("Group",data.IngredientGroup[i  - 1][0]).ID
			local selectData = LuaCookBookMgr.m_MyFoodIngredientData[selectId]
			self.m_SelectItemList[i] = {id = selectId, count = selectData.count, data = selectData.data}
		end
	end
	self.CookButton.Text = LocalString.GetString("烹饪")
	self:InitRightView(data,t)
	self:ShowCookButtonFx()
end

function LuaCookBookWnd:InitRightView(data,t)
	if not self.m_FoodRootRadioBox then
		self.m_FoodRootRadioBox = self.FoodRoot.gameObject:AddComponent(typeof(QnRadioBox))
	end
	Extensions.RemoveAllChildren(self.FoodRoot.transform)
	self.m_FoodRootRadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), data.IngredientGroup.Length)
	for i = 1,data.IngredientGroup.Length do
		local go = NGUITools.AddChild(self.FoodRoot.gameObject, self.FoodItem.gameObject)
		go.gameObject:SetActive(true)
		local curSelectItem = self.m_SelectItemList[i]
		self:InitFoodItem(go, data.IngredientGroup[i - 1], t.isCookSucc,curSelectItem)
		self.m_FoodRootRadioBox.m_RadioButtons[i - 1] = go:GetComponent(typeof(QnSelectableButton))
		self.m_FoodRootRadioBox.m_RadioButtons[i - 1].OnClick = MakeDelegateFromCSFunction(self.m_FoodRootRadioBox.On_Click, MakeGenericClass(Action1, QnButton), self.m_FoodRootRadioBox)
		UIEventListener.Get(go).onDragStart = DelegateFactory.VoidDelegate(function (go)
			self:OnDragIconStart(go,i,false)
		end)
	end
	self.m_FoodRootRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnFoodRootRadioBoxSelected(btn,index)
    end)
	self.FoodRoot:Reposition()

	if not self.m_CookMethodRootRadioBox then
		self.m_CookMethodRootRadioBox = self.CookMethodRoot.gameObject:AddComponent(typeof(QnRadioBox))
	end
	Extensions.RemoveAllChildren(self.CookMethodRoot.transform)
	self.m_CookMethodRootRadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), data.Method.Length)
	for i = 1,data.Method.Length do
		local go = NGUITools.AddChild(self.CookMethodRoot.gameObject, self.FoodItem.gameObject)
		go.gameObject:SetActive(true)
		local curSelectItem = self.m_SelectMethodList[i]
		self:InitFoodItem(go, data.Method[i - 1], t.isCookSucc,curSelectItem)
		self.m_CookMethodRootRadioBox.m_RadioButtons[i - 1] = go:GetComponent(typeof(QnSelectableButton))
		self.m_CookMethodRootRadioBox.m_RadioButtons[i - 1].OnClick = MakeDelegateFromCSFunction(self.m_CookMethodRootRadioBox.On_Click, MakeGenericClass(Action1, QnButton), self.m_CookMethodRootRadioBox)
		UIEventListener.Get(go).onDragStart = DelegateFactory.VoidDelegate(function (go)
			self:OnDragIconStart(go,i,true)
		end)
	end
	self.m_CookMethodRootRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnCookMethodRadioBoxSelected(btn,index)
    end)
	self.CookMethodRoot:Reposition()
end

function LuaCookBookWnd:OnDragIconStart(go,index,isMethod)
	local curSelectItem = self.m_SelectItemList[index]
	if isMethod then
		curSelectItem = self.m_SelectMethodList[index]
	end
	if curSelectItem then
		self.DragItem.gameObject:SetActive(true)
		self.DragItem:LoadMaterial(SafeStringFormat3("UI/Texture/Item_Other/Material/%s.mat",curSelectItem.data.Icon))
		self.m_IsDragging = true
		self.m_LastDragPos = Input.mousePosition
		if CommonDefs.IsInMobileDevice() then
			self.m_FingerIndex = Input.GetTouch(0).fingerId
			self.m_LastDragPos = Input.GetTouch(0).position
		end
		self.m_DragItemIndex = index
		self.m_DragItemIsMethod = isMethod
	end
end

function LuaCookBookWnd:OnFoodRootRadioBoxSelected(btn,index)
	self.m_FoodRootRadioBoxIndex = index
	local curSelectItem = self.m_SelectItemList[index + 1]
	self:ShowItemSelecrWnd(1, curSelectItem)
end

function LuaCookBookWnd:OnCookMethodRadioBoxSelected(btn,index)
	self.m_CookMethodRootRadioBoxIndex = index
	local curSelectItem = self.m_SelectMethodList[index + 1]
	self:ShowItemSelecrWnd(2, curSelectItem)
end

function LuaCookBookWnd:ShowItemSelecrWnd(type, curSelectItem)
	LuaCookBookMgr.m_ItemSelectWndType = type
	LuaCookBookMgr.m_ItemSelectWndCurSelectItem = curSelectItem
	CUIManager.ShowUI(CLuaUIResources.CookBookItemSelectWnd)
end

function LuaCookBookWnd:ShowItemTip(templateId)
    local names = {}
    local actions = {}

    table.insert(names,LocalString.GetString("获取"))

    table.insert(actions,function()
		CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, true, nil, AlignType2.Right)
    end)
    local actionSource = DefaultItemActionDataSource.Create(#names,actions,names)
    CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, actionSource, AlignType.Default, 0, 0, 0, 0)
end

function LuaCookBookWnd:InitFoodItem(go, info, isCookSucc, curSelectItem)
	local groupId, num = info[0],info[1]

	local item = go.transform:Find("Item"):GetComponent(typeof(CUITexture))
	local emptyGo = go.transform:Find("Empty").gameObject
	local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local numLabel = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
	Extensions.SetLocalPositionZ(item.transform,(curSelectItem and curSelectItem.count == 0) and - 1 or 0)
	Extensions.SetLocalPositionZ(numLabel.transform,(curSelectItem and curSelectItem.count == 0) and 1 or 0)
	emptyGo.gameObject:SetActive(not isCookSucc and curSelectItem == nil)
	item.gameObject:SetActive(isCookSucc or curSelectItem)
	if curSelectItem then
		item:LoadMaterial(SafeStringFormat3("UI/Texture/Item_Other/Material/%s.mat",curSelectItem.data.Icon))
		nameLabel.text = curSelectItem.data.Name
		numLabel.text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3(curSelectItem.count == 0 and "[ff0000]%d[4E2F2F]/1" or "[4E2F2F]%d/1",curSelectItem.count))
	end
end
