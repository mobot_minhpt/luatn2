local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Animation = import "UnityEngine.Animation"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local Profession = import "L10.Game.Profession"

LuaBaoYuDuoYu2023ResultWnd = class()
LuaBaoYuDuoYu2023ResultWnd.m_IsHero = false
LuaBaoYuDuoYu2023ResultWnd.m_IsWin = false
LuaBaoYuDuoYu2023ResultWnd.m_IsGetDailyAward = false
LuaBaoYuDuoYu2023ResultWnd.m_IsGetHeroAward = false
LuaBaoYuDuoYu2023ResultWnd.m_IsGetEliteAward = false
LuaBaoYuDuoYu2023ResultWnd.m_UsedTime = nil
LuaBaoYuDuoYu2023ResultWnd.m_ExtraInfo = nil

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBaoYuDuoYu2023ResultWnd, "Btn01", "Btn01", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023ResultWnd, "Btn02", "Btn02", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023ResultWnd, "TeamInfo", "TeamInfo", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023ResultWnd, "ConsumeTime", "ConsumeTime", UILabel)
RegistChildComponent(LuaBaoYuDuoYu2023ResultWnd, "TeammateNode", "TeammateNode", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023ResultWnd, "Teammate1", "Teammate1", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023ResultWnd, "Teammate2", "Teammate2", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023ResultWnd, "Teammate3", "Teammate3", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023ResultWnd, "Teammate4", "Teammate4", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023ResultWnd, "Teammate5", "Teammate5", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023ResultWnd, "RewardInfo", "RewardInfo", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023ResultWnd, "RewardNode", "RewardNode", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023ResultWnd, "NoRewardReminder", "NoRewardReminder", UILabel)
RegistChildComponent(LuaBaoYuDuoYu2023ResultWnd, "Item2", "Item2", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023ResultWnd, "Item1", "Item1", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023ResultWnd, "PlayInfo", "PlayInfo", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023ResultWnd, "Tex", "Tex", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023ResultWnd, "PlayTitle", "PlayTitle", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023ResultWnd, "Difficulty", "Difficulty", UILabel)

--@endregion RegistChildComponent end

function LuaBaoYuDuoYu2023ResultWnd:Awake()
    self.Btn02:SetActive(not CommonDefs.IS_VN_CLIENT)
    
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:RefreshVariableUI()
    self:InitUIEvent()
end

function LuaBaoYuDuoYu2023ResultWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaBaoYuDuoYu2023ResultWnd:InitWndData()
    self.teammateList = {self.Teammate1, self.Teammate2, self.Teammate3, self.Teammate4, self.Teammate5}
end 

function LuaBaoYuDuoYu2023ResultWnd:RefreshVariableUI()
    if LuaBaoYuDuoYu2023ResultWnd.m_IsHero then
        --设置成英雄难度
        self.transform:Find("Info/TeamInfo/RankLabel/baoyuduoyu2023resultwnd_chendi"):GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/FestivalActivity/Festival_Double11/Shuangshiyi2023/Material/baoyuduoyu2023resultwnd_chendi_02.mat")
        self.transform:Find("Info/RewardInfo/RewardLabel/baoyuduoyu2023resultwnd_chendi"):GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/FestivalActivity/Festival_Double11/Shuangshiyi2023/Material/baoyuduoyu2023resultwnd_chendi_02.mat")
        self.Difficulty.text = LocalString.GetString("英雄模式")
        self.Difficulty.color = NGUIText.ParseColor24("eee5ff", 0)
        self.ConsumeTime.color = NGUIText.ParseColor24("613f5f", 0)
        self.transform:Find("Info/TeamInfo/RankLabel"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24("613f5f", 0)
        self.transform:Find("Info/Sprite"):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor24("453da6", 0)
        self.transform:Find("Info/Sprite"):GetComponent(typeof(UISprite)).alpha = 0.15
        self.NoRewardReminder.color = NGUIText.ParseColor24("613f5f", 0)
    else
        --设置成普通难度
        self.transform:Find("Info/TeamInfo/RankLabel/baoyuduoyu2023resultwnd_chendi"):GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/FestivalActivity/Festival_Double11/Shuangshiyi2023/Material/baoyuduoyu2023resultwnd_chendi.mat")
        self.transform:Find("Info/RewardInfo/RewardLabel/baoyuduoyu2023resultwnd_chendi"):GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/FestivalActivity/Festival_Double11/Shuangshiyi2023/Material/baoyuduoyu2023resultwnd_chendi.mat")
        self.Difficulty.text = LocalString.GetString("普通模式")
        self.Difficulty.color = NGUIText.ParseColor24("FFFFFF", 0)
        self.ConsumeTime.color = NGUIText.ParseColor24("7D4E24", 0)
        self.transform:Find("Info/TeamInfo/RankLabel"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24("7D4E24", 0)
        self.transform:Find("Info/Sprite"):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor24("A6773D", 0)
        self.transform:Find("Info/Sprite"):GetComponent(typeof(UISprite)).alpha = 0.15
        self.NoRewardReminder.color = NGUIText.ParseColor24("7D4E24", 0)
    end
    
    if LuaBaoYuDuoYu2023ResultWnd.m_IsWin then
        self.transform:GetComponent(typeof(Animation)):Play("baseresultwnd_2_win")
    else
        self.transform:GetComponent(typeof(Animation)):Play("baseresultwnd_2_lose")
    end
    
    local finishTime = LuaBaoYuDuoYu2023ResultWnd.m_UsedTime
    self.ConsumeTime.text = SafeStringFormat3("%02d:%02d", math.floor(finishTime / 60), finishTime % 60)

    --refresh teaminfo
    for i = 1, #self.teammateList do
        local teammateObj = self.teammateList[i]
        --m_ExtraInfo
        local playerid = LuaBaoYuDuoYu2023ResultWnd.m_ExtraInfo[5*(i-1)+1]
        local name = LuaBaoYuDuoYu2023ResultWnd.m_ExtraInfo[5*(i-1)+2]
        local level = LuaBaoYuDuoYu2023ResultWnd.m_ExtraInfo[5*(i-1)+3]
        local career = LuaBaoYuDuoYu2023ResultWnd.m_ExtraInfo[5*(i-1)+4]
        local isFeiSheng = LuaBaoYuDuoYu2023ResultWnd.m_ExtraInfo[5*(i-1)+5] == 1
        if playerid then
            teammateObj:SetActive(true)
            teammateObj.transform:Find("Level"):GetComponent(typeof(UILabel)).text = "Lv." .. level
            teammateObj.transform:Find("PlayerName"):GetComponent(typeof(UILabel)).text = name
            teammateObj.transform:Find("careerSprite"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(career)
        else    
            teammateObj:SetActive(false)
        end

        if CClientMainPlayer.Inst and (CClientMainPlayer.Inst.Id == playerid) or false then
            --是自己
            self.teammateList[i].transform:Find("PlayerName"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24("269156", 0)
            self.teammateList[i].transform:Find("Level"):GetComponent(typeof(UILabel)).color = isFeiSheng and NGUIText.ParseColor24("C87900", 0) or NGUIText.ParseColor24("676767", 0)
        else    
            --是别人
            self.teammateList[i].transform:Find("PlayerName"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24("676767", 0)
            self.teammateList[i].transform:Find("Level"):GetComponent(typeof(UILabel)).color = isFeiSheng and NGUIText.ParseColor24("C87900", 0) or NGUIText.ParseColor24("676767", 0)
        end
    end
    
    --refresh rewardnode
    if LuaBaoYuDuoYu2023ResultWnd.m_IsHero then
        local itemIndex = 1
        local itemList = {self.Item1, self.Item2}
        for i = 1, #itemList do
            itemList[i]:SetActive(false)
        end
        if LuaBaoYuDuoYu2023ResultWnd.m_IsGetHeroAward then
            local itemObj = itemList[itemIndex]
            itemObj:SetActive(true)
            self:InitRewardItem(itemObj, Double11_BYDLY.GetData().MainWndRewardItems[1])
            itemIndex = itemIndex + 1
        end
        if LuaBaoYuDuoYu2023ResultWnd.m_IsGetEliteAward then
            local itemObj = itemList[itemIndex]
            itemObj:SetActive(true)
            self:InitRewardItem(itemObj, Double11_BYDLY.GetData().MainWndRewardItems[2])
            itemIndex = itemIndex + 1
        end
        self.NoRewardReminder.gameObject:SetActive(itemIndex == 1)
        if itemIndex == 1 then
            if LuaBaoYuDuoYu2023ResultWnd.m_IsWin then
                self.NoRewardReminder.text = LocalString.GetString("奖励次数已达上限")
            else
                self.NoRewardReminder.text = LocalString.GetString("无")
            end
        end
    else
        self.Item1:SetActive(LuaBaoYuDuoYu2023ResultWnd.m_IsGetDailyAward)
        self.NoRewardReminder.gameObject:SetActive(not LuaBaoYuDuoYu2023ResultWnd.m_IsGetDailyAward)
        self:InitRewardItem(self.Item1, Double11_BYDLY.GetData().MainWndRewardItems[0])
        self.Item2:SetActive(false)

        if not LuaBaoYuDuoYu2023ResultWnd.m_IsGetDailyAward then
            if LuaBaoYuDuoYu2023ResultWnd.m_IsWin then
                self.NoRewardReminder.text = LocalString.GetString("奖励次数已达上限")
            else
                self.NoRewardReminder.text = LocalString.GetString("无")
            end
        end
    end
end

function LuaBaoYuDuoYu2023ResultWnd:InitUIEvent()
    UIEventListener.Get(self.Btn01).onClick = DelegateFactory.VoidDelegate(function (_)
        CUIManager.ShowUI(CLuaUIResources.BaoYuDuoYu2023SignUpWnd)
        CUIManager.CloseUI(CLuaUIResources.BaoYuDuoYu2023ResultWnd)
    end)

    UIEventListener.Get(self.Btn02).onClick = DelegateFactory.VoidDelegate(function (_)
        CUICommonDef.CaptureScreen("screenshot", true, false, nil, DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
        end))
    end)
end

function LuaBaoYuDuoYu2023ResultWnd:InitRewardItem(rewardItemObj, templateId)
    local texture = rewardItemObj.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(templateId)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end
    UIEventListener.Get(rewardItemObj).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
end 