-- Auto Generated!!
local AlignType = import "CPlayerInfoMgr+AlignType"
local AlignType1 = import "L10.UI.CTooltip+AlignType"
local Callback = import "EventDelegate+Callback"
local CButton = import "L10.UI.CButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CJieBaiMgr = import "L10.Game.CJieBaiMgr"
local CJieBaiWnd = import "L10.UI.CJieBaiWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CTooltip = import "L10.UI.CTooltip"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIDatePickerMgr = import "L10.UI.CUIDatePickerMgr"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumJieBaiSessionState = import "L10.Game.EnumJieBaiSessionState"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EventDelegate = import "EventDelegate"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local UIInput = import "UIInput"
local UILabel = import "UILabel"
local UInt64 = import "System.UInt64"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local Wedding_Setting = import "L10.Game.Wedding_Setting"
CJieBaiWnd.m_CleanNode_CS2LuaHook = function (this, nodeArray) 
    CommonDefs.EnumerableIterate(nodeArray, DelegateFactory.Action_object(function (___value) 
        local node = ___value
        node.transform:Find("show").gameObject:SetActive(false)
        node.transform:Find("empty").gameObject:SetActive(true)
        node.transform:Find("wait").gameObject:SetActive(false)
    end))
end
CJieBaiWnd.m_SetNodeDefault_CS2LuaHook = function (this, nodeArray, count) 
    do
        local i = 0
        while i < count do
            if i >= nodeArray.Length then
                break
            end

            local node = nodeArray[i]
            node.transform:Find("show").gameObject:SetActive(false)
            node.transform:Find("empty").gameObject:SetActive(false)
            node.transform:Find("wait").gameObject:SetActive(true)
            i = i + 1
        end
    end
end
CJieBaiWnd.m_DisableBirthInput_CS2LuaHook = function (this, playerId) 
    if CClientMainPlayer.Inst ~= nil then
        if playerId == CClientMainPlayer.Inst.Id then
            CommonDefs.GetComponent_Component_Type(this.setBirthNode.transform:Find("SubmitButton"), typeof(CButton)).Enabled = false
            this.setBirthNode.transform:Find("ClickMask").gameObject:SetActive(true)
            this.setBirthNode.transform:Find("SubmitButton").gameObject:SetActive(false)
        end
    end
end
CJieBaiWnd.m_UpdatePlayerInfo_CS2LuaHook = function (this) 
    local nodeArray = this.personalInfoArray
    this:SetNodeDefault(nodeArray, nodeArray.Length)
    --sessionData.sessionPlayerId2Info[info.Id] = info;
    --sessionData.sessionPlayerId2Index[info.Id] = index;
    local selfIndex = 1000
    if CClientMainPlayer.Inst ~= nil then
        if CommonDefs.DictContains(CJieBaiMgr.Inst.sessionData.sessionPlayerId2Index, typeof(UInt64), CClientMainPlayer.Inst.Id) then
            selfIndex = CommonDefs.DictGetValue(CJieBaiMgr.Inst.sessionData.sessionPlayerId2Index, typeof(UInt64), CClientMainPlayer.Inst.Id)
        end
    end

    CommonDefs.DictIterate(CJieBaiMgr.Inst.sessionData.sessionPlayerId2Info, DelegateFactory.Action_object_object(function (___key, ___value) 
        local pair = {}
        pair.Key = ___key
        pair.Value = ___value
        local playerId = pair.Key
        local info = pair.Value
        if CommonDefs.DictContains(CJieBaiMgr.Inst.sessionData.sessionPlayerId2Index, typeof(UInt64), playerId) then
            local index = CommonDefs.DictGetValue(CJieBaiMgr.Inst.sessionData.sessionPlayerId2Index, typeof(UInt64), playerId)
            local node = nodeArray[index - 1]
            node.transform:Find("show").gameObject:SetActive(true)
            node.transform:Find("empty").gameObject:SetActive(false)
            node.transform:Find("wait").gameObject:SetActive(false)
            CommonDefs.GetComponent_Component_Type(node.transform:Find("show/name"), typeof(UILabel)).text = info.Name

            CommonDefs.GetComponent_Component_Type(node.transform:Find("show"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(info.Cls, info.Gender, -1), false)

            if index ~= selfIndex then
                local clickNode = node.transform:Find("show/button").gameObject
                UIEventListener.Get(clickNode).onClick = DelegateFactory.VoidDelegate(function (p) 
                    CPlayerInfoMgr.ShowPlayerPopupMenu(info.Id, EnumPlayerInfoContext.ChatLink, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
                end)

                CommonDefs.GetComponent_Component_Type(node.transform:Find("show/title"), typeof(UILabel)).text = CJieBaiMgr.Inst:GetJieBaiRelationByID(playerId, true)
            else
                CommonDefs.GetComponent_Component_Type(node.transform:Find("show/title"), typeof(UILabel)).text = ""
            end

            if info.signForJieBai then
                node.transform:Find("show/sign").gameObject:SetActive(true)
                if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id == playerId then
                    local submitBtn = this.confirmNode.transform:Find("SubmitButton").gameObject
                    CommonDefs.GetComponent_GameObject_Type(submitBtn, typeof(CButton)).Enabled = false
                end
            else
                node.transform:Find("show/sign").gameObject:SetActive(false)
            end

            this:DisableBirthInput(playerId)
        end
    end))
end
CJieBaiWnd.m_InitSetBirth_CS2LuaHook = function (this) 
    this.setBirthNode:SetActive(true)
    this.setNameNode:SetActive(false)
    this.confirmNode:SetActive(false)
    --CTeamMatchLevelPickerItemResource.Inst.Resource = timeTemplate;
    CommonDefs.GetComponent_Component_Type(this.setBirthNode.transform:Find("SubmitButton"), typeof(CButton)).Enabled = true
    this.setBirthNode.transform:Find("ClickMask").gameObject:SetActive(false)

    this.today = CServerTimeMgr.Inst:GetZone8Time()
    this.maxYear = this.today.Year
    this.minYear = Wedding_Setting.GetData().BaziMinYear

    this.yearInput:Awake()
    this.yearInput.AlignType = CTooltip.AlignType.Right
    this.yearInput.OnKeyboardClosed = MakeDelegateFromCSFunction(this.OnKeyBoardClosed, Action0, this)
    this.yearInput.OnMaxValueButtonClick = MakeDelegateFromCSFunction(this.OnMaxButtonClick, Action0, this)
    this.yearInput:ForceSetText("", false)
    this.yearTips.text = System.String.Format(LocalString.GetString("输入{0}-{1}之间的年份"), this.minYear, this.maxYear)
    this.yearTips.gameObject:SetActive(true)

    UIEventListener.Get(this.monthDayButton).onClick = MakeDelegateFromCSFunction(this.OnMonthDayButtonClick, VoidDelegate, this)

    local submitBtn = this.setBirthNode.transform:Find("SubmitButton").gameObject
    UIEventListener.Get(submitBtn).onClick = MakeDelegateFromCSFunction(this.OnSubmitBirth, VoidDelegate, this)
end
CJieBaiWnd.m_OnSubmitBirth_CS2LuaHook = function (this, go) 
    if this.chooseYear <= 0 or this.chooseMonth <= 0 or this.chooseDay <= 0 then
        g_MessageMgr:ShowMessage("ENTER_COMPLETE_IFO")
    else
        local sendBirthData = this.chooseYear * 10000 + this.chooseMonth * 100 + this.chooseDay
        Gac2Gas.WriteJinLanPu(CJieBaiMgr.Inst.sessionData.sessionId, sendBirthData)
    end
end
CJieBaiWnd.m_OnKeyBoardClosed_CS2LuaHook = function (this) 
    local default
    default, this.chooseYear = System.Int32.TryParse(this.yearInput.Text)
    if default and this.chooseYear >= this.minYear and this.chooseYear <= this.maxYear then
        this.yearTips.gameObject:SetActive(false)
        this:InitMonthDay()
        return
    else
        this.yearTips.gameObject:SetActive(true)
        g_MessageMgr:ShowMessage("ENTER_RIGHT_YEAR")
        this.yearInput:ForceSetText("", false)
        this.chooseYear = 0
    end
end
CJieBaiWnd.m_InitMonthDay_CS2LuaHook = function (this) 
    this.chooseMonth = 0
    this.chooseDay = 0
    this.monthDayLabel.text = ""
    this.monthDayTips:SetActive(true)
    Extensions.SetLocalRotationZ(this.monthDayArrow.transform, 0)
end
CJieBaiWnd.m_OnMonthDayButtonClick_CS2LuaHook = function (this, go) 
    if this.chooseYear >= this.minYear and this.chooseYear <= this.maxYear then
        Extensions.SetLocalRotationZ(this.monthDayArrow.transform, 180)
        CUIDatePickerMgr.ShowDoublePickerWnd(this.chooseYear, math.max(this.chooseMonth, 1), math.max(this.chooseDay, 1), go.transform, AlignType1.Right)
    else
        g_MessageMgr:ShowMessage("ENTER_RIGHT_BIRTHYEAR")
    end
end
CJieBaiWnd.m_OnDoublePickerSelect_CS2LuaHook = function (this, index1, index2) 
    this.chooseMonth = index1
    this.chooseDay = index2
    this.monthDayTips:SetActive(false)
    this.monthDayLabel.text = System.String.Format("{0}/{1}", this.chooseMonth, this.chooseDay)
    Extensions.SetLocalRotationZ(this.monthDayArrow.transform, 0)
end
CJieBaiWnd.m_SetShowName_CS2LuaHook = function (this, title1, title2) 
    if not this:CheckTeamLeader() then
        local title1Label = CommonDefs.GetComponent_Component_Type(this.BeginNameInputNode.transform:Find("Label"), typeof(UILabel))
        local title2Label = CommonDefs.GetComponent_Component_Type(this.LastNameInputNode.transform:Find("Label"), typeof(UILabel))
        title1Label.text = title1
        title2Label.text = title2
    end
end
CJieBaiWnd.m_CheckTeamLeader_CS2LuaHook = function (this) 
    if CTeamMgr.Inst:TeamExists() then
        return CTeamMgr.Inst:MainPlayerIsTeamLeader()
    end
    return false
end
CJieBaiWnd.m_SubmitTitle_CS2LuaHook = function (this) 
    local title1Label = CommonDefs.GetComponent_Component_Type(this.BeginNameInputNode.transform:Find("Label"), typeof(UILabel))
    local title2Label = CommonDefs.GetComponent_Component_Type(this.LastNameInputNode.transform:Find("Label"), typeof(UILabel))
    if System.String.IsNullOrEmpty(title1Label.text) or System.String.IsNullOrEmpty(title2Label.text) then
        g_MessageMgr:ShowMessage("ENTER_COMPLETE_IFO")
    else
        if CommonDefs.StringLength(title1Label.text) > CJieBaiWnd.MaxTitle1Length then
            g_MessageMgr:ShowMessage("CHUANJIABAO_NAME_LIMIT", LocalString.GetString("结拜称号前缀"), CJieBaiWnd.MaxTitle1Length)
            return
        end
        if CommonDefs.StringLength(title2Label.text) > CJieBaiWnd.MinTitle2Length then
            g_MessageMgr:ShowMessage("CHUANJIABAO_NAME_LIMIT", LocalString.GetString("结拜称号后缀"), CJieBaiWnd.MinTitle2Length)
            return
        end

        local totalName = (title1Label.text .. CJieBaiWnd.TitleMidName[CJieBaiMgr.Inst.sessionData.memberCount - 1]) .. title2Label.text
        if CJieBaiMgr.Inst:CheckJiebaiTitleNameBySelfName(totalName) then
            Gac2Gas.ConfirmSessionJieBaiTitle(CJieBaiMgr.Inst.sessionData.sessionId, totalName)
        end
    end
end
CJieBaiWnd.m_InitSetName_CS2LuaHook = function (this, memberCount) 
    this.setBirthNode:SetActive(false)
    this.setNameNode:SetActive(true)
    this.confirmNode:SetActive(false)
    this.MidNameLabel.text = CJieBaiWnd.TitleMidName[memberCount - 1]

    if this:CheckTeamLeader() then
        this.SetNameTipNode:SetActive(false)
        this.SetNameOpArea:SetActive(true)
        local submitBtn = this.SetNameOpArea.transform:Find("SubmitButton").gameObject
        UIEventListener.Get(submitBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
            this:SubmitTitle()
        end)

        local title1LabelInput = CommonDefs.GetComponent_GameObject_Type(this.BeginNameInputNode, typeof(UIInput))
        local title2LabelInput = CommonDefs.GetComponent_GameObject_Type(this.LastNameInputNode, typeof(UIInput))
        EventDelegate.Add(title1LabelInput.onChange, MakeDelegateFromCSFunction(this.TitleValueChange, Callback, this))
        --
        EventDelegate.Add(title2LabelInput.onChange, MakeDelegateFromCSFunction(this.TitleValueChange, Callback, this))
        this.setNameNode.transform:Find("ClickMask").gameObject:SetActive(false)
    else
        this.SetNameTipNode:SetActive(true)
        this.SetNameOpArea:SetActive(false)
        this.setNameNode.transform:Find("ClickMask").gameObject:SetActive(true)
    end
end
CJieBaiWnd.m_InitConfirm_CS2LuaHook = function (this, title) 
    this.setBirthNode:SetActive(false)
    this.setNameNode:SetActive(false)
    this.confirmNode:SetActive(true)
    local titleLabel = CommonDefs.GetComponent_Component_Type(this.confirmNode.transform:Find("title"), typeof(UILabel))
    local showTitle = ""
    do
        if CommonDefs.IsSeaMultiLang() then
            showTitle = title
        else
            local i = 0
            while i < CommonDefs.StringLength(title) do
                if i < CommonDefs.StringLength(title) - 1 then
                    showTitle = showTitle .. (StringAt(title, i + 1) .. "\n")
                else
                    showTitle = showTitle .. StringAt(title, i + 1)
                end
                i = i + 1
            end
        end
    end
    titleLabel.text = showTitle
    local submitBtn = this.confirmNode.transform:Find("SubmitButton").gameObject
    UIEventListener.Get(submitBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        this:SubmitJiebai()
    end)
end
CJieBaiWnd.m_UpdateState_CS2LuaHook = function (this, state, membercount, title1, title2) 
    if state == EnumToInt((EnumJieBaiSessionState.eDefault)) then
        this:InitSetBirth()
    elseif state == EnumToInt((EnumJieBaiSessionState.eJinLanPuDone)) then
        this:InitSetName(membercount)
    elseif state == EnumToInt((EnumJieBaiSessionState.eTitleDone)) then
        local title = (title1 .. CJieBaiWnd.TitleMidName[membercount - 1]) .. title2
        this:InitConfirm(title)
    elseif state == EnumToInt((EnumJieBaiSessionState.eSignDone)) then
        this:Close()
    end
end
CJieBaiWnd.m_Init_CS2LuaHook = function (this) 
    this:CleanNode(this.personalInfoArray)
    this:UpdatePlayerInfo()

    UIEventListener.Get(this.closeBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        this:ClosePanel()
    end)

    UIEventListener.Get(this.ChatBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        CSocialWndMgr.ShowChatWnd(EChatPanel.Team)
    end)

    this:UpdateState(EnumToInt((CJieBaiMgr.Inst.sessionData.state)), CJieBaiMgr.Inst.sessionData.memberCount, CJieBaiMgr.Inst.sessionData.title1, CJieBaiMgr.Inst.sessionData.title2)
end
