local UILabel = import "UILabel"
local UIGrid = import "UIGrid"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local TouchPhase = import "UnityEngine.TouchPhase"
local UISprite = import "UISprite"
local CButton = import "L10.UI.CButton"
local CUIFx = import "L10.UI.CUIFx"
local TweenScale = import "TweenScale"
local TweenPosition = import "TweenPosition"
local TweenAlpha = import "TweenAlpha"
local COpenEntryMgr = import "L10.Game.COpenEntryMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Input = import "UnityEngine.Input"
local Vector3 = import "UnityEngine.Vector3"
local Object = import "System.Object"

CLuaTiaoXiangPlayWnd = class()

RegistChildComponent(CLuaTiaoXiangPlayWnd, "WorkView", GameObject)
RegistChildComponent(CLuaTiaoXiangPlayWnd, "ResultView", GameObject)
RegistChildComponent(CLuaTiaoXiangPlayWnd, "MsgLabel", UILabel)
RegistChildComponent(CLuaTiaoXiangPlayWnd, "Ingredient2", CUITexture)
RegistChildComponent(CLuaTiaoXiangPlayWnd, "Ingredient1", CUITexture)
RegistChildComponent(CLuaTiaoXiangPlayWnd, "Ingredient3", CUITexture)
RegistChildComponent(CLuaTiaoXiangPlayWnd, "Ingredient4", CUITexture)
RegistChildComponent(CLuaTiaoXiangPlayWnd, "Grid", UIGrid)
RegistChildComponent(CLuaTiaoXiangPlayWnd, "ColorItem", GameObject)
RegistChildComponent(CLuaTiaoXiangPlayWnd, "ResetBtn", CButton)
RegistChildComponent(CLuaTiaoXiangPlayWnd, "ComiteBtn", CButton)
RegistChildComponent(CLuaTiaoXiangPlayWnd, "Bottle", GameObject)
RegistChildComponent(CLuaTiaoXiangPlayWnd, "CloneItem", CUITexture)
RegistChildComponent(CLuaTiaoXiangPlayWnd, "SucceedInfo", GameObject)
RegistChildComponent(CLuaTiaoXiangPlayWnd, "ResultFxNode", CUIFx)
RegistChildComponent(CLuaTiaoXiangPlayWnd, "ResultTextFxNode", CUIFx)
RegistChildComponent(CLuaTiaoXiangPlayWnd, "GuideFinger", GameObject)
RegistChildComponent(CLuaTiaoXiangPlayWnd, "FailInfo", GameObject)
RegistChildComponent(CLuaTiaoXiangPlayWnd, "GridFxNode", CUIFx)
RegistChildComponent(CLuaTiaoXiangPlayWnd, "TiaoZhiFxNode", CUIFx)
RegistClassMember(CLuaTiaoXiangPlayWnd, "RePlayBtn")
RegistClassMember(CLuaTiaoXiangPlayWnd, "TweenScale")
RegistClassMember(CLuaTiaoXiangPlayWnd, "TweenPosition")

RegistClassMember(CLuaTiaoXiangPlayWnd, "m_Textures")
RegistClassMember(CLuaTiaoXiangPlayWnd, "m_Colors")
RegistClassMember(CLuaTiaoXiangPlayWnd, "m_DragingIdx")

RegistClassMember(CLuaTiaoXiangPlayWnd, "m_Mixture")
RegistClassMember(CLuaTiaoXiangPlayWnd, "m_cookBook")
RegistClassMember(CLuaTiaoXiangPlayWnd, "m_Idx2Name")
RegistClassMember(CLuaTiaoXiangPlayWnd, "m_Result")
RegistClassMember(CLuaTiaoXiangPlayWnd, "m_LastDragPos")
RegistClassMember(CLuaTiaoXiangPlayWnd, "m_IsDragging")
RegistClassMember(CLuaTiaoXiangPlayWnd, "m_DragingIdx")
RegistClassMember(CLuaTiaoXiangPlayWnd, "m_FxPath")
RegistClassMember(CLuaTiaoXiangPlayWnd, "m_CloseTick")
RegistClassMember(CLuaTiaoXiangPlayWnd, "m_NeedCount")
RegistClassMember(CLuaTiaoXiangPlayWnd, "m_CloseBtn")

function CLuaTiaoXiangPlayWnd:Awake()
    self.m_CloseTick = nil
    self.ResultView:SetActive(false)
    self.WorkView:SetActive(true)
    self.CloneItem.gameObject:SetActive(false)
    self.ComiteBtn.Enabled = false
    --self.m_Result = false
    self.GuideFinger:SetActive(false)
    self.ColorItem:SetActive(false)
    self.m_CloseBtn = self.transform:Find("closeButton").gameObject

    local path = "Fx/UI/Prefab/UI_tiaoxiangwanfa_"
    self.m_FxPath = {
        ["jidian"] = path.."jidian.prefab",
        ["liuguang"] = path.."liuguang.prefab",
        ["tiaozhi"] = path.."tiaozhi.prefab",
        ["chenggong"] = path.."chenggong.prefab",
        ["shibai"] = path.."shibai.prefab",
        ["chenggong02"] = path.."chenggong02.prefab",
        ["shibai02"] = path.."shibai02.prefab",
    }

    self.RePlayBtn = self.ResultView.transform:Find("RePlayBtn").gameObject
    self.TweenScale = self.Bottle.transform:GetComponent(typeof(TweenScale))
    self.TweenPosition = self.Bottle.transform:GetComponent(typeof(TweenPosition))

    UIEventListener.Get(self.Ingredient2.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(go)
        self:OnDragSkillIconStart(2)
    end)
    UIEventListener.Get(self.Ingredient1.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(go)
        self:OnDragSkillIconStart(1)
    end)
    UIEventListener.Get(self.Ingredient3.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(go)
        self:OnDragSkillIconStart(3)
    end)
    UIEventListener.Get(self.Ingredient4.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(go)
        self:OnDragSkillIconStart(4)
    end)

    UIEventListener.Get(self.ResetBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnReplay()
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("瓷瓶已清空，可以重新调制了哦"))
    end)
    UIEventListener.Get(self.ComiteBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnComite()
    end)
    UIEventListener.Get(self.RePlayBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        --self.TweenScale:ResetToBeginning()
        --self.TweenPosition:ResetToBeginning()
        self.ResultView:SetActive(false)
        self.WorkView:SetActive(true)
        self:OnReplay()
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("瓷瓶已清空，可以重新调制了哦"))
    end)

    self.fingerTweenPos = self.GuideFinger.transform:GetComponent(typeof(TweenPosition))
    self.fingerTweenScale = self.GuideFinger.transform:GetComponent(typeof(TweenScale))
    self.fingerTweenAlpha = self.GuideFinger.transform:GetComponent(typeof(TweenAlpha))
end

function CLuaTiaoXiangPlayWnd:Init()
    self:InitCookView()
    self:OnReplay()

    if LuaTiaoXiangPlayMgr.m_GuideKey and not COpenEntryMgr.Inst:GetGuideRecord(LuaTiaoXiangPlayMgr.m_GuideKey) then
        self.GuideFinger:SetActive(true)
    end
end

function CLuaTiaoXiangPlayWnd:InitCookView()
    for i,iconPath in ipairs(LuaTiaoXiangPlayMgr.m_Textures) do
        local key = "Ingredient"..i
        local ingredient = self[key]
        if iconPath == "" then
            local key = "Ingredient"..i
            ingredient.gameObject:SetActive(false)
        else
            local name = ingredient.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
            name.text = LuaTiaoXiangPlayMgr.m_Idx2Name[i]
            ingredient:LoadMaterial(iconPath)
            name.color = NGUIText.ParseColor(LuaTiaoXiangPlayMgr.m_Colors[i], 0)
        end
    end
    self.WorkView.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = LuaTiaoXiangPlayMgr.m_ResultName
    self.MsgLabel.text = LuaTiaoXiangPlayMgr.m_MsgTxt

    Extensions.RemoveAllChildren(self.Grid.transform)
    local gridCount = 0
    for i,count in ipairs(LuaTiaoXiangPlayMgr.m_cookBook) do
        gridCount = gridCount + count
    end
    self.m_NeedCount = gridCount
    for i=1,gridCount,1 do
        local colorItem = NGUITools.AddChild(self.Grid.gameObject,self.ColorItem)
        colorItem:SetActive(true)
    end
    self.Grid:Reposition()
end

function CLuaTiaoXiangPlayWnd:Update()
    if nil == self.m_LastDragPos then
        self.m_LastDragPos = Vector3.zero
    end
    if not self.m_IsDragging then
        return
    end

    if CommonDefs.IsInMobileDevice() then
        for i = 0,Input.touchCount - 1 do
            local touch = Input.GetTouch(i)
            if touch.fingerId == self.m_FingerIndex then
                if touch.phase ~= TouchPhase.Ended and touch.phase ~= TouchPhase.Canceled then
                    if touch.phase ~= TouchPhase.Stationary then
                        local pos = touch.position
                        self.m_LastDragPos = Vector3(pos.x, pos.y, 0)
                        self.CloneItem.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_LastDragPos)
                    end
                    return
                else
                    break
                end
            end
        end
    else
        if Input.GetMouseButton(0) then
            self.CloneItem.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
            self.m_LastDragPos = Input.mousePosition

            if not Input.GetMouseButtonUp(0) then
                return
            end
        end
    end
    self.CloneItem.gameObject:SetActive(false)
    local hoveredObject = CUICommonDef.SelectedUIWithRacast
    if hoveredObject and hoveredObject.name == "Bottle" and self.m_DragingIdx ~= -1 then
        self:PutItemInBottle(self.m_DragingIdx)
    else
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请将材料拖入瓷瓶"))
    end
    self.m_IsDragging = false
    self.m_FingerIndex = -1
    self.m_DragingIdx = -1
end

function CLuaTiaoXiangPlayWnd:OnDragSkillIconStart(idx)
    self.m_DragingIdx = idx
    self.CloneItem:LoadMaterial(LuaTiaoXiangPlayMgr.m_Textures[idx])
    self.CloneItem.gameObject:SetActive(true)
    self.m_IsDragging = true
    self.m_LastDragPos = Input.mousePosition

    if CommonDefs.IsInMobileDevice() then
        self.m_FingerIndex = Input.GetTouch(0).fingerId
        local pos = Input.GetTouch(0).position
        self.m_LastDragPos = Vector3(pos.x, pos.y, 0)
    end
end

function CLuaTiaoXiangPlayWnd:PutItemInBottle(idx)
    if #self.m_Mixture >= self.m_NeedCount then return end--10
    if LuaTiaoXiangPlayMgr.m_GuideKey and not COpenEntryMgr.Inst:GetGuideRecord(LuaTiaoXiangPlayMgr.m_GuideKey) then
        self.GuideFinger:SetActive(false)
        COpenEntryMgr.Inst:SetGuideRecord(LuaTiaoXiangPlayMgr.m_GuideKey)
    end

    g_MessageMgr:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("添加了一味%s"),LuaTiaoXiangPlayMgr.m_Idx2Name[idx]))
    table.insert(self.m_Mixture,idx)
    local count = #self.m_Mixture
    local colorSprite = self.Grid.transform:GetChild(count-1):GetComponent(typeof(UISprite))
    colorSprite.color = NGUIText.ParseColor(LuaTiaoXiangPlayMgr.m_Colors[idx], 0)
    local fxNode = colorSprite.transform:Find("FxNode"):GetComponent(typeof(CUIFx))
    fxNode:DestroyFx()
    fxNode:LoadFx(self.m_FxPath.jidian)
    if count == self.m_NeedCount then
        self.ComiteBtn.Enabled = true
        --self.GridFxNode:DestroyFx()
        self.GridFxNode:LoadFx(self.m_FxPath.liuguang)
    end
end

function CLuaTiaoXiangPlayWnd:OnReplay()
    for i=0,self.Grid.transform.childCount - 1 do
        self.Grid.transform:GetChild(i):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor(LuaTiaoXiangPlayMgr.m_Colors[5], 0)
    end
    self.m_Mixture = {}
    self.SucceedInfo:SetActive(false)
    self.RePlayBtn:SetActive(false)
    self.FailInfo:SetActive(false)
    self.ComiteBtn.Enabled = false
    self.m_Result = false
    Extensions.SetLocalPositionZ(self.Bottle.transform, 0)
    
    self.ResultFxNode:DestroyFx()
    self.ResultTextFxNode:DestroyFx()
    self.TiaoZhiFxNode:DestroyFx()
    self.GridFxNode:DestroyFx()
    for i=0,self.Grid.transform.childCount-1,1 do
        self.Grid.transform:GetChild(i):Find("FxNode"):GetComponent(typeof(CUIFx)):DestroyFx()
    end
end

function CLuaTiaoXiangPlayWnd:OnComite()
    local meiGui,taoHua,baiHe,qiangWeiLu = 0,0,0,0
    for i,ingredient in ipairs(self.m_Mixture) do
        if ingredient == 1 then 
            meiGui = meiGui + 1 
        elseif ingredient == 2 then
            taoHua = taoHua + 1
        elseif ingredient == 3 then
            baiHe = baiHe + 1
        elseif ingredient == 4 then
            qiangWeiLu = qiangWeiLu + 1
        end
    end

    if meiGui == LuaTiaoXiangPlayMgr.m_cookBook[1] and taoHua == LuaTiaoXiangPlayMgr.m_cookBook[2] and baiHe == LuaTiaoXiangPlayMgr.m_cookBook[3] and qiangWeiLu == LuaTiaoXiangPlayMgr.m_cookBook[4] then
        self.m_Result = true
    end
    
    self.WorkView:SetActive(false)
    self.ResultView:SetActive(true)

    self:WaitShowResultFx(self.m_Result)
end

function CLuaTiaoXiangPlayWnd:WaitShowResultFx(result)
    self.m_CloseBtn:SetActive(false)
    self.TiaoZhiFxNode:DestroyFx()
    self.TiaoZhiFxNode:LoadFx(self.m_FxPath.tiaozhi)
    if self.m_CloseTick then
        UnRegisterTick(self.m_CloseTick)
        self.m_CloseTick = nil
    end
    self.m_CloseTick = RegisterTickOnce(function ( ... )
        if not self.ResultFxNode or self.m_Result == nil then
            return 
        end
        self.ResultFxNode:DestroyFx()
        self.ResultTextFxNode:DestroyFx()
        self.SucceedInfo:SetActive(result)
        self.FailInfo:SetActive(not result)
        self.RePlayBtn:SetActive(not result)
        if result then
            self.ResultFxNode:LoadFx(self.m_FxPath.chenggong02)
            self.ResultTextFxNode:LoadFx(self.m_FxPath.chenggong)
            Extensions.SetLocalPositionZ(self.Bottle.transform, 0)
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("此香集百花之香，水润香腻，是为上品"))
            local empty = CreateFromClass(MakeGenericClass(List, Object))
            self:Wait2FinishTask()
            ---Gac2Gas.FinishClientTaskEvent(CLuaTaskMgr.m_TiaoXiangPlayWnd_TaskId,"TiaoXiangPlay",MsgPackImpl.pack(empty))
        else
            self.m_CloseBtn:SetActive(true)
            self.ResultFxNode:LoadFx(self.m_FxPath.shibai02)
            self.ResultTextFxNode:LoadFx(self.m_FxPath.shibai)
            Extensions.SetLocalPositionZ(self.Bottle.transform, - 1)
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("单味花香不宜过量，是为次品"))
        end
		end, 2500)
end

function CLuaTiaoXiangPlayWnd:Wait2FinishTask()
    if self.m_FinishTick then
        UnRegisterTick(self.m_FinishTick)
        self.m_FinishTick = nil
    end

    self.m_FinishTick = RegisterTickOnce(function ( ... )
        local empty = CreateFromClass(MakeGenericClass(List, Object))
        Gac2Gas.FinishClientTaskEvent(CLuaTaskMgr.m_TiaoXiangPlayWnd_TaskId,"TiaoXiangPlay",MsgPackImpl.pack(empty))
        CUIManager.CloseUI(CLuaUIResources.TiaoXiangPlayWnd)
	end, 3000)
end
