local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Profession = import "L10.Game.Profession"
local CServerTimeMgr   = import "L10.Game.CServerTimeMgr"
local NativeTools = import "L10.Engine.NativeTools"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"

LuaSnowAdventureMultipleTeamWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSnowAdventureMultipleTeamWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaSnowAdventureMultipleTeamWnd, "SnowAdventureStageDetailWnd", "SnowAdventureStageDetailWnd", GameObject)
RegistChildComponent(LuaSnowAdventureMultipleTeamWnd, "SnowManInfo", "SnowManInfo", GameObject)
RegistChildComponent(LuaSnowAdventureMultipleTeamWnd, "SnowMan1", "SnowMan1", GameObject)
RegistChildComponent(LuaSnowAdventureMultipleTeamWnd, "SnowMan2", "SnowMan2", GameObject)
RegistChildComponent(LuaSnowAdventureMultipleTeamWnd, "SnowMan3", "SnowMan3", GameObject)
RegistChildComponent(LuaSnowAdventureMultipleTeamWnd, "SnowMan4", "SnowMan4", GameObject)
RegistChildComponent(LuaSnowAdventureMultipleTeamWnd, "SnowMan5", "SnowMan5", GameObject)
RegistChildComponent(LuaSnowAdventureMultipleTeamWnd, "SnowManInteract", "SnowManInteract", GameObject)
RegistChildComponent(LuaSnowAdventureMultipleTeamWnd, "ChangeSnowManButton", "ChangeSnowManButton", GameObject)
RegistChildComponent(LuaSnowAdventureMultipleTeamWnd, "OpenDialogButton", "OpenDialogButton", GameObject)
RegistChildComponent(LuaSnowAdventureMultipleTeamWnd, "RemainTimeLabel", "RemainTimeLabel", UILabel)
RegistChildComponent(LuaSnowAdventureMultipleTeamWnd, "TimeProgressBarBg", "TimeProgressBarBg", GameObject)
RegistChildComponent(LuaSnowAdventureMultipleTeamWnd, "TimeProgressBar", "TimeProgressBar", UISprite)
RegistChildComponent(LuaSnowAdventureMultipleTeamWnd, "ReadyButton", "ReadyButton", GameObject)

--@endregion RegistChildComponent end

function LuaSnowAdventureMultipleTeamWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:OnRefreshVariableUI()
    self:InitUIEvent()
end

function LuaSnowAdventureMultipleTeamWnd:Init()
    Gac2Gas.RequestXuePoLiXianData()
end

function LuaSnowAdventureMultipleTeamWnd:InitWndData()
    self.prepareDuration = HanJia2023_XuePoLiXianSetting.GetData().PrepareDuration

    self.stageDetailScript = LuaSnowAdventureStageDetailWnd:new()
    self.stageDetailScript:Init(self.SnowAdventureStageDetailWnd)
    
    self.TimeProgressBarWidget = self.TimeProgressBar.transform:GetComponent(typeof(UIWidget))

    self.initEventOnce = false
end

function LuaSnowAdventureMultipleTeamWnd:InitUIEvent()
    if LuaHanJia2023Mgr.snowAdventureData == nil then
        return
    end
    self.initEventOnce = true
    
    UIEventListener.Get(self.ChangeSnowManButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        if self.isMeReady then
            g_MessageMgr:ShowMessage("SnowAdventure_multipleTeamCancelReadyReminder")
        else
            LuaSnowAdventureSnowManWnd.ShowCountdownPattern = true
            CUIManager.ShowUI(CLuaUIResources.SnowAdventureSnowManWnd)
        end
    end)

    UIEventListener.Get(self.OpenDialogButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        CSocialWndMgr.ShowChatWnd(EChatPanel.Team)
    end)

    UIEventListener.Get(self.ReadyButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        Gac2Gas.XuePoLiXianUpdatePrepareStatus(not self.isMeReady)
    end)

    UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("SnowAdventure_RejectMultipleTeam"), DelegateFactory.Action(
                function()
                    --主动退出的话,直接把面板关了吧... 防止有啥不同步的情况, 导致队伍信息不对而面板关不掉了
                    CUIManager.CloseUI(CLuaUIResources.SnowAdventureMultipleTeamWnd)
                    Gac2Gas.XuePoLiXianRejectPrepare()
                end
        ), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    end)
end

function LuaSnowAdventureMultipleTeamWnd:OnEnable()
    self.OnReceiveChatMsg = DelegateFactory.Action_uint(function(channelDef)
        --800 Team Channel
        if channelDef == 800 then
            local chatMsg = CChatMgr.Inst:GetLatestMsg(channelDef)
            self:RefreshSnowmanBubble(chatMsg.fromUserId, chatMsg.message)
        end
    end)
    g_ScriptEvent:AddListener("HanJia2023_SyncMultipleTeamInfo", self, "OnRefreshVariableUI")
    g_ScriptEvent:AddListener("HanJia2023_XuePoLiXianStopPrepare", self, "OnStopPrepare")
    g_ScriptEvent:AddListener("HanJia2023_SyncXuePoLiXianData", self, "OnRefreshVariableUI")

    EventManager.AddListenerInternal(EnumEventType.RecvChatMsg, self.OnReceiveChatMsg)
end

function LuaSnowAdventureMultipleTeamWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HanJia2023_SyncMultipleTeamInfo", self, "OnRefreshVariableUI")
    g_ScriptEvent:RemoveListener("HanJia2023_XuePoLiXianStopPrepare", self, "OnStopPrepare")
    g_ScriptEvent:RemoveListener("HanJia2023_SyncXuePoLiXianData", self, "OnRefreshVariableUI")

    EventManager.RemoveListenerInternal(EnumEventType.RecvChatMsg, self.OnReceiveChatMsg)

    if self.countdownTick then
        UnRegisterTick(self.countdownTick)
        self.countdownTick = nil
    end
end

function LuaSnowAdventureMultipleTeamWnd:OnRefreshVariableUI()
    if self.initEventOnce == false then
        self:InitUIEvent()
    end

    if LuaHanJia2023Mgr.snowAdventureData == nil then
        return
    end
    
    self.stageDetailScript:RefreshData(LuaHanJia2023Mgr.multipleTeamInfo.curLevelId)
    self.isMeReady = true
    if CClientMainPlayer.Inst then
        local mainPlayerId = CClientMainPlayer.Inst.Id
        for k, v in pairs(LuaHanJia2023Mgr.multipleTeamInfo.memberInfo) do
            if k == mainPlayerId then
                self.isMeReady = v.isOk
                break
            end
        end
    end
    self:RefreshReadyButton(self.isMeReady)
    self:RefreshTeamInfo(LuaHanJia2023Mgr.multipleTeamInfo.memberInfo)
    self:RefreshCountdown(LuaHanJia2023Mgr.multipleTeamInfo.startTs, self.prepareDuration)
end

function LuaSnowAdventureMultipleTeamWnd:RefreshReadyButton(isMeReady)
    local buttonSprite = self.ReadyButton.transform:GetComponent(typeof(UISprite))
    local buttonLabel = self.ReadyButton.transform:Find("Label"):GetComponent(typeof(UILabel))
    if isMeReady then
        buttonSprite.spriteName = "common_btn_01_blue"
        buttonLabel.text = LocalString.GetString("取消准备")
    else
        buttonSprite.spriteName = "common_btn_01_yellow"
        buttonLabel.text = LocalString.GetString("准备好了")
    end
end 

function LuaSnowAdventureMultipleTeamWnd:RefreshTeamInfo(memberInfo)
    --TODO JUNYI, 把memberInfo重新排序一下, 按照自己>队长>原队伍位置
    local snowmanList = { self.SnowMan1, self.SnowMan2, self.SnowMan3, self.SnowMan4, self.SnowMan5}
    local snowmanIdx = 1
    self.playerId2SnowmanId = {}

    local sourceTable = {}
    for k, v in pairs(memberInfo) do
        table.insert(sourceTable, {pid = k, pdata = v})
    end
    table.sort(sourceTable, function(a, b)
        local isALeader = CClientMainPlayer.Inst and CTeamMgr.Inst:IsTeamLeader(a.pid) or false
        local isBLeader = CClientMainPlayer.Inst and CTeamMgr.Inst:IsTeamLeader(b.pid) or false
        if (isALeader and isBLeader) or ((not isALeader) and (not isBLeader)) then
            --都是队长 或 都不是队长
            local isAMe = CClientMainPlayer.Inst and (CClientMainPlayer.Inst.Id == a.pid) or false
            local isBMe = CClientMainPlayer.Inst and (CClientMainPlayer.Inst.Id == b.pid) or false
            if isAMe then
                return true
            elseif isBMe then
                return false
            else
                return a.pid > b.pid
            end
        else
            --有一个队长
            if isALeader then
                return true
            elseif isBLeader then
                return false
            else
                return a.pid > b.pid
            end
        end
    end)
    for i = 1, #sourceTable do
        snowmanList[snowmanIdx]:SetActive(true)
        local info = sourceTable[i]
        self:RefreshSnowMan(snowmanList[snowmanIdx], info.pid, info.pdata.isOk, info.pdata.lastSnowmanType, info.pdata.lastSnowmanLv)
        self.playerId2SnowmanId[info.pid] = snowmanIdx
        snowmanIdx = snowmanIdx + 1
    end
    for i = snowmanIdx, #snowmanList do
        snowmanList[i]:SetActive(false)
    end
end 

function LuaSnowAdventureMultipleTeamWnd:RefreshSnowMan(snowmanObj, playerId, isReady, snowmanType, snowmanLevel)
    local snowmanConfig = HanJia2023_XuePoLiXianSnowman.GetData(snowmanType)
    local snowmanTexture = snowmanObj.transform:Find("SnowManTexture"):GetComponent(typeof(CUITexture))
    snowmanTexture:LoadMaterial(snowmanConfig.Pic)
    local snowmanLevelLabel = snowmanObj.transform:Find("SnowManLevel"):GetComponent(typeof(UILabel))
    snowmanLevelLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(snowmanLevel))
    local proIcon = snowmanObj.transform:Find("SnowManProIcon"):GetComponent(typeof(UISprite))
    proIcon.spriteName = Profession.GetIconByNumber(snowmanConfig.Career)
    local isReadyTransform = snowmanObj.transform:Find("IsReady")
    isReadyTransform.gameObject:SetActive(isReady)

    --是否是队长
    local isLeader = CClientMainPlayer.Inst and CTeamMgr.Inst:IsTeamLeader(playerId) or false
    local isLeaderTransform = snowmanObj.transform:Find("IsTeamLeader")
    isLeaderTransform.gameObject:SetActive(isLeader)
    
    --获得playerId人的名字
    if CClientMainPlayer.Inst then
        local mainPlayerId = CClientMainPlayer.Inst.Id
        local labelComp = snowmanObj.transform:Find("PlayerName"):GetComponent(typeof(UILabel))
        if playerId == mainPlayerId then
            --是我自己
            labelComp.text = CClientMainPlayer.Inst.Name
            labelComp.color = NGUIText.ParseColor24("06883A",0)
        else    
            --其他成员
            local info = CTeamMgr.Inst:GetMemberById(playerId)
            if info then
                labelComp.text = info.m_MemberName
                labelComp.color = NGUIText.ParseColor24("FFFFFF",0)
            end
        end
    end
end 

function LuaSnowAdventureMultipleTeamWnd:RefreshSnowmanBubble(talkerPlayerId, talkerMsg)
    local snowmanId = self.playerId2SnowmanId[talkerPlayerId]
    local snowmanList = { self.SnowMan1, self.SnowMan2, self.SnowMan3, self.SnowMan4, self.SnowMan5}
    if snowmanId then
        local snowmanObj = snowmanList[snowmanId]
        snowmanObj.transform:Find("DialogBg").gameObject:SetActive(true)
        snowmanObj.transform:Find("DialogBg/DialogText"):GetComponent(typeof(UILabel)).text = talkerMsg
        --@TODO JUNYI 交互想要在这里搞一个动态改sortingOrder的事情
        --@TODO JUNYI 这个地方应该还需要挂一个计时器, 结束的时候隐藏一下dialog
    end
end

function LuaSnowAdventureMultipleTeamWnd:RefreshCountdown(startTs, duration)
    if self.countdownTick then
        UnRegisterTick(self.countdownTick)
        self.countdownTick = nil
    end
    local nowTs = CServerTimeMgr.Inst.timeStamp
    local tickDuration = 0
    local remainValue = startTs+duration-nowTs
    if remainValue > 0 then
        tickDuration = (remainValue)*1000
    end
    local commonColor = NGUIText.ParseColor24("274882",0)
    local warningColor = NGUIText.ParseColor24("B52B14",0)
    self:ChangeCountdown(remainValue, duration, commonColor, warningColor)
    self.countdownTick = RegisterTickWithDuration(function ()
        local nowTs = CServerTimeMgr.Inst.timeStamp
        remainValue = startTs+duration-nowTs
        self:ChangeCountdown(remainValue, duration, commonColor, warningColor)
    end, 30, tickDuration)
end 

function LuaSnowAdventureMultipleTeamWnd:ChangeCountdown(remainValue, duration, commonColor, warningColor)
    local changeColorTime = 10

    self.RemainTimeLabel.text = g_MessageMgr:FormatMessage("SnowAdventure_CountdownReminder", math.floor(remainValue))
    self.TimeProgressBarWidget.width = remainValue / duration * self.TimeProgressBarBg.transform:GetComponent(typeof(UIWidget)).width

    if remainValue <= changeColorTime then
        --变红
        self.RemainTimeLabel.color = warningColor
        self.TimeProgressBar.spriteName = "common_hpandmp_hp"
    else    
        --普通
        self.RemainTimeLabel.color = commonColor
        self.TimeProgressBar.spriteName = "common_hpandmp_mp"
    end
end


function LuaSnowAdventureMultipleTeamWnd:OnStopPrepare()
    CUIManager.CloseUI(CLuaUIResources.SnowAdventureMultipleTeamWnd)
end 