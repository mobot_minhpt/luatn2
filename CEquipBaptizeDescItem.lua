-- Auto Generated!!
local CEquipBaptizeDescItem = import "L10.UI.CEquipBaptizeDescItem"
local CPropertyDifMgr = import "L10.UI.CPropertyDifMgr"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local Vector3 = import "UnityEngine.Vector3"
local Word_Word = import "L10.Game.Word_Word"
CEquipBaptizeDescItem.m_Init_UInt32_CS2LuaHook = function (this, id,isMax) 
    this.lookupButton:SetActive(false)
    local data = Word_Word.GetData(id)
    if data ~= nil then
        this.nameLabel.text = System.String.Format("[{0}]", data.Name)
        this.descLabel.text = data.Description
        this.mDescOrigStr = this.descLabel.text
        if isMax then
            this.descLabel.text = CLuaEquipMgr.WordMaxMark..this.descLabel.text
        end
    else
        this.nameLabel.text = ""
        this.descLabel.text = ""
    end
    --this.contentLabel.text = content;
    this.descLabel:UpdateNGUIText()
    this.bgWidget.height = math.floor(math.max((this.descLabel.localSize.y+20), (this.nameLabel.localSize.y+20)))
end
CEquipBaptizeDescItem.m_Init_String_CS2LuaHook = function (this, str) 
    this.lookupButton:SetActive(true)
    this.nameLabel.text = ""
    this.descLabel.transform.localPosition = Vector3(- 245, - 5)
    this.descLabel.text = str
    this.descLabel.width = 490
    this.descLabel:UpdateNGUIText()
    this.bgWidget.height = math.floor(math.max((this.descLabel.localSize.y+20), (this.nameLabel.localSize.y+20)))
end
CEquipBaptizeDescItem.m_Init_List_CS2LuaHook = function (this, list) 
    do
        local i = 0
        while i < list.Count do
            if list[i].oldValue > list[i].nowValue then
                this.descLabel.text = LocalString.GetString("[c][ff2525]基础属性降低[-][/c]")
                break
            else
                this.descLabel.text = LocalString.GetString("[c][00ff00]基础属性提升[-][/c]")
            end
            i = i + 1
        end
    end
    this.lookupButton:SetActive(true)
    this.nameLabel.text = ""
    this.descLabel.transform.localPosition = Vector3(- 245, - 5)
    --descLabel.text = str;
    this.descLabel.width = 490
    this.descLabel:UpdateNGUIText()
    this.bgWidget.height = math.floor(math.max((this.descLabel.localSize.y+20), (this.nameLabel.localSize.y+20)))
    UIEventListener.Get(this.lookupButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        CPropertyDifMgr.ShowEquipDifWnd(list)
    end)
end
