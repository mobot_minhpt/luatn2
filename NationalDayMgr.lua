local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CUIManager = import "L10.UI.CUIManager"
local MsgPackImpl = import "MsgPackImpl"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local Utility = import "L10.Engine.Utility"
local CPos = import "L10.Engine.CPos"

LuaNationalDayMgr = class()

LuaNationalDayMgr.m_TXZRResultScore = 0 --天选之人得分
LuaNationalDayMgr.m_JCYWResultInfo = nil --校场演武结果

function LuaNationalDayMgr.OpenTXZREnterWnd()
	CUIManager.ShowUI("NationalDayTXZREnterWnd")
end

function LuaNationalDayMgr.OpenTXZRResultWnd()
	CUIManager.ShowUI("NationalDayTXZRResultWnd")
end

function LuaNationalDayMgr.OpenJCYWResultWnd()
	CUIManager.ShowUI("NationalDayJCYWResultWnd")
end

function LuaNationalDayMgr.IsInTXZRGameplay(gameplayId)
	return gameplayId == 51101189
end

function LuaNationalDayMgr.IsInJCYWGameplay(gameplayId)
	return gameplayId == 51101190
end

--报名参加天选之人玩法
function LuaNationalDayMgr.TXZRSignUpPlay()
	Gac2Gas.NationalDayTXZRSignUpPlay()
end

--取消报名天选之人玩法
function LuaNationalDayMgr.TXZRCancelSignUp()
	Gac2Gas.NationalDayTXZRCancelSignUp()
end

--查询是否处于天选之人玩法匹配状态
function LuaNationalDayMgr.TXZRCheckMatching()
	Gac2Gas.NationalDayTXZRCheckInMatching()
end

--
function LuaNationalDayMgr.OnTXZRSignUpPlayResult(bSuccess)
	g_ScriptEvent:BroadcastInLua("OnTXZRSignUpPlayResult", bSuccess)
end

function LuaNationalDayMgr.OnTXZRCancelSignUpResult(bSuccess)
	g_ScriptEvent:BroadcastInLua("OnTXZRCancelSignUpResult", bSuccess)
end

function LuaNationalDayMgr.OnTXZRCheckInMatchingResult(bSuccess)
	g_ScriptEvent:BroadcastInLua("OnTXZRCheckInMatchingResult", bSuccess)
end

function LuaNationalDayMgr.TXZRPlayAoeFx(aoeId)
	if not GuoQingXianLi_TXZRAoe.Exists(aoeId) then return end

	local data = GuoQingXianLi_TXZRAoe.GetData(aoeId)
	for i=0, data.FxPos.Length-1 do
		local pixelPos = Utility.GridPos2PixelPos(CPos(data.FxPos[i][0], data.FxPos[i][1])) --特效区域的左下角格子
		local worldPos = Utility.PixelPos2WorldPos(pixelPos)
		local xScale = data.FxPos[i][2] --水平格子数
		local zScale = data.FxPos[i][3] --垂直格子数
		worldPos.x = worldPos.x + (xScale/2 - 0.5)
		worldPos.z = worldPos.z + (zScale/2 - 0.5)

		CEffectMgr.Inst:AddWorldPositionFX(88801258,worldPos,0,0,1.0,-1,EnumWarnFXType.None,nil,xScale,zScale, nil)
	end
end

function LuaNationalDayMgr.OnTXZRPlayEnd(extraData_U)
	local extraDataList = MsgPackImpl.unpack(extraData_U)
	if not extraDataList or extraDataList.Count==0 then return end
	local score = tonumber(extraDataList[0])
	LuaNationalDayMgr.m_TXZRResultScore = score
	LuaNationalDayMgr.OpenTXZRResultWnd()
end

function LuaNationalDayMgr.OnJCYWPlayEnd(validPlayer_U, achieveGoals_U, score, usedTime)
	-- validPlayer_U:m_CharacterID,m_Name,m_Grade,m_Class,m_Gender,m_XiuWeiGrade,m_XiuLianGrade,m_Expression,m_ExpressionTxt,m_Sticker,m_XianFanStatus,m_HasFeiSheng,m_ProfileInfo_U,m_IsLeader

	local validPlayers = MsgPackImpl.unpack(validPlayer_U)
	local achieveGoals = MsgPackImpl.unpack(achieveGoals_U)
	if not validPlayers or validPlayers.Count == 0 then
		return
	end
	if not achieveGoals or achieveGoals.Count == 0 then
		return
	end

	local playerNum = validPlayers.Count
	local tbl = {}
	local myself = nil
	for i=0,playerNum-1 do
		local playerInfoDict = validPlayers[i]
		local data = {}
		data.playerId = tonumber(CommonDefs.DictGetValue_LuaCall(playerInfoDict, "m_CharacterID"))
		data.playerName = tostring(CommonDefs.DictGetValue_LuaCall(playerInfoDict, "m_Name"))
		data.playerLevel = tonumber(CommonDefs.DictGetValue_LuaCall(playerInfoDict, "m_Grade"))
		data.class = tonumber(CommonDefs.DictGetValue_LuaCall(playerInfoDict, "m_Class"))
		data.gender = tonumber(CommonDefs.DictGetValue_LuaCall(playerInfoDict, "m_Gender"))
		data.xianfanStaus = tonumber(CommonDefs.DictGetValue_LuaCall(playerInfoDict, "m_XianFanStatus"))
		data.isLeader = CommonDefs.DictGetValue_LuaCall(playerInfoDict, "m_IsLeader")

		if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == data.playerId then
			myself = data
		else
			table.insert(tbl, data)
		end
	end
	--把自己放第一位
	local teammembers = nil
	if myself then
		teammembers = {}
		table.insert(teammembers, myself)
		for __,data in pairs(tbl) do
			table.insert(teammembers, data)
		end
	else
		teammembers = tbl
	end

	local goals = {}

	CommonDefs.DictIterate(achieveGoals, DelegateFactory.Action_object_object(function(key,val)
		goals[tonumber(key)] = true
	end))

	LuaNationalDayMgr.m_JCYWResultInfo = {}
	LuaNationalDayMgr.m_JCYWResultInfo.m_Members = teammembers
	LuaNationalDayMgr.m_JCYWResultInfo.m_Goals = goals
	LuaNationalDayMgr.m_JCYWResultInfo.m_Score = score
	LuaNationalDayMgr.m_JCYWResultInfo.m_UsedTime = usedTime

	LuaNationalDayMgr.OpenJCYWResultWnd()
end
