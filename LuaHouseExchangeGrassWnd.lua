local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"

local GameObject = import "UnityEngine.GameObject"

local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local QnTableView = import "L10.UI.QnTableView"

LuaHouseExchangeGrassWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHouseExchangeGrassWnd, "CountLabel", "CountLabel", UILabel)
RegistChildComponent(LuaHouseExchangeGrassWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaHouseExchangeGrassWnd, "PreviewTexture", "PreviewTexture", CUITexture)
RegistChildComponent(LuaHouseExchangeGrassWnd, "CountInput", "CountInput", QnAddSubAndInputButton)
RegistChildComponent(LuaHouseExchangeGrassWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaHouseExchangeGrassWnd, "AddGrassButton", "AddGrassButton", GameObject)
RegistChildComponent(LuaHouseExchangeGrassWnd, "CostLabel", "CostLabel", UILabel)
RegistChildComponent(LuaHouseExchangeGrassWnd, "OwnLabel", "OwnLabel", UILabel)
RegistChildComponent(LuaHouseExchangeGrassWnd, "TitleLabel", "TitleLabel", UILabel)

--@endregion RegistChildComponent end

RegistClassMember(LuaHouseExchangeGrassWnd,"m_Keys")
RegistClassMember(LuaHouseExchangeGrassWnd,"m_Lookup")


LuaHouseExchangeGrassWnd.s_SelectedGrassType = 0
function LuaHouseExchangeGrassWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)


	
	UIEventListener.Get(self.AddGrassButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddGrassButtonClick()
	end)


    --@endregion EventBind end

    
    self.m_Keys = {}
    House_AllGrass.ForeachKey(function(key)
        if key>0 then
            table.insert(self.m_Keys,key)
        end
    end)

    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_Keys
        end,
        function(item,row)
            self:InitItem(item,self.m_Keys[row+1])
        end)

    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(index)
        self:OnSelectAtRow(index)
    end)


    self.CountInput.onValueChanged = DelegateFactory.Action_uint(function(value)
        local key = self.m_Keys[self.TableView.currentSelectRow+1]
        local data = House_AllGrass.GetData(key)
        self.CostLabel.text = tostring(data.Price*value)
    end)
end

function LuaHouseExchangeGrassWnd:Init()
    local wood = 0
    if CClientHouseMgr.Inst.mConstructProp then
        wood = CClientHouseMgr.Inst.mConstructProp.Wood
    end
    self.OwnLabel.text = tostring(wood)


    self.m_Lookup = LuaHouseTerrainMgr.GetGrassUsedCount()
    -- local grass = LuaHouseTerrainMgr.GetGrassUsedCount()

    self.TableView:ReloadData(true,false)
    local grassType = LuaHouseExchangeGrassWnd.s_SelectedGrassType
    local selectedIndex = 0
    for i,v in ipairs(self.m_Keys) do
        if v==grassType then
            selectedIndex = i-1
        end
    end
    self.TableView:SetSelectRow(selectedIndex,true)
end

--@region UIEvent

function LuaHouseExchangeGrassWnd:OnButtonClick()
    local count = self.CountInput:GetValue()

    local key = self.m_Keys[self.TableView.currentSelectRow+1]
    local data = House_AllGrass.GetData(key)
    local cost = data.Price*count
    local wood = 0
    if CClientHouseMgr.Inst.mConstructProp then
        wood = CClientHouseMgr.Inst.mConstructProp.Wood
    end
    if cost>wood then
        g_MessageMgr:ShowMessage("HouseGrass_Unlock_NoWood")
        return
    end

    if LuaHouseTerrainMgr.GetGrassMaxNum(key)>=LuaHouseTerrainMgr.GetGrassLimitNum(key) then
        g_MessageMgr:ShowMessage("HouseGrass_Unlock_Exceed",data.Name)
        return
    end
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("House_Buy_Grass_Confirm",cost,data.Name,LuaHouseTerrainMgr.GetGrassMaxNum(key)+count),
        function()
            local type = self.TableView.currentSelectRow+1
            Gac2Gas.RequestBuyHouseGrassGrid(type,count)
        end, nil, nil, nil, false)
end


function LuaHouseExchangeGrassWnd:OnAddGrassButtonClick()
    CShopMallMgr.ShowLinyuShoppingMall(21003558)
end


--@endregion UIEvent


function LuaHouseExchangeGrassWnd:InitItem(item,key)
    local data = House_AllGrass.GetData(key)
    local tf = item.transform
    local icon = tf:Find("Icon"):GetComponent(typeof(CUITexture))

    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text = tostring(data.Name)
    icon:LoadMaterial(data.Icon
)
    local countLabel=  tf:Find("AmountLabel"):GetComponent(typeof(UILabel))
    
	local max = LuaHouseTerrainMgr.GetGrassMaxNum(key)
    countLabel.text = SafeStringFormat3("%d/%d", self.m_Lookup[key] or 0,max)
end
function LuaHouseExchangeGrassWnd:OnSelectAtRow(row)
    local key = self.m_Keys[row+1]
    local data = House_AllGrass.GetData(key)

    local limitNum = LuaHouseTerrainMgr.GetGrassLimitNum(key)
    local max = LuaHouseTerrainMgr.GetGrassMaxNum(key)
    if max>=limitNum then
        self.CountLabel.text = SafeStringFormat3(LocalString.GetString("上限%d格，已达上限"),limitNum)
    else
        self.CountLabel.text = SafeStringFormat3(LocalString.GetString("上限%d格，已拥有%d格"),limitNum,max)
    end
    self.CostLabel.text = tostring(data.Price*self.CountInput:GetValue())

    self.TitleLabel.text = tostring(data.Name)
    self.PreviewTexture:LoadMaterial(data.PreviewPic)

    self.CountInput:SetMinMax(1,math.max(1,limitNum-max),1)
    self.CountInput:OnValueChange()
end



function LuaHouseExchangeGrassWnd:OnEnable()
    g_ScriptEvent:AddListener("OnUpdateHouseResource", self, "OnUpdateHouseResource")
    g_ScriptEvent:AddListener("SyncHouseGrassGridNum", self, "OnSyncHouseGrassGridNum")
    
end
function LuaHouseExchangeGrassWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnUpdateHouseResource", self, "OnUpdateHouseResource")
    g_ScriptEvent:RemoveListener("SyncHouseGrassGridNum", self, "OnSyncHouseGrassGridNum")
end
function LuaHouseExchangeGrassWnd:OnDestroy()
    LuaHouseExchangeGrassWnd.s_SelectedGrassType = 0
end

function LuaHouseExchangeGrassWnd:OnUpdateHouseResource()
    local wood = 0
    if CClientHouseMgr.Inst.mConstructProp then
        wood = CClientHouseMgr.Inst.mConstructProp.Wood
    end
    self.OwnLabel.text = tostring(wood)
end
function LuaHouseExchangeGrassWnd:OnSyncHouseGrassGridNum(grassType, count)
    local idx = 0
    for i,v in ipairs(self.m_Keys) do
        if v==grassType then
            idx=i
            break
        end
    end
    local item = self.TableView:GetItemAtRow(idx-1)
    self:InitItem(item,grassType)
    if idx-1==self.TableView.currentSelectRow then
        self:OnSelectAtRow(idx-1)
        local limitNum = LuaHouseTerrainMgr.GetGrassLimitNum(grassType)

        self.CountInput:SetMinMax(1,math.max(1,limitNum-count),1)
        self.CountInput:OnValueChange()
    end
end
