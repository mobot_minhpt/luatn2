local MessageWndManager = import "L10.UI.MessageWndManager"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EServerStatus=import "L10.UI.EServerStatus"
local CGameServer=import "L10.UI.CGameServer"
local QnSelectableButton=import "L10.UI.QnSelectableButton"

CLuaQMPKServerChosenWnd = class()
RegistClassMember(CLuaQMPKServerChosenWnd,"m_ChangeButon")
RegistClassMember(CLuaQMPKServerChosenWnd,"m_CurServerIndex")


function CLuaQMPKServerChosenWnd:Init( )
    self.m_CurServerIndex = -1
    self.m_ChangeButon = self.transform:Find("Anchor/ChangeButton").gameObject

    if nil == CClientMainPlayer.Inst then
        return
    end
  
    UIEventListener.Get(self.m_ChangeButon).onClick = DelegateFactory.VoidDelegate(function(go) self:OnChangeButtonClick(go) end)

    local currentServerId=CClientMainPlayer.Inst:GetCurrentServerId()
    local myServerId=CClientMainPlayer.Inst:GetMyServerId()
  
    local serverNameLabel1=FindChild(self.transform,"ServerNameLabel1"):GetComponent(typeof(UILabel))
    serverNameLabel1.text=nil
    local serverNameLabel2=FindChild(self.transform,"ServerNameLabel2"):GetComponent(typeof(UILabel))
    local itemTemplate=FindChild(self.transform,"ServerItem").gameObject
    itemTemplate:SetActive(false)
    local parent=FindChild(self.transform,"Grid")
    for i,v in ipairs(CLuaQMPKMgr.m_ServerList) do
        if v.m_Id==currentServerId then
            serverNameLabel2.text=LocalString.GetString("全民争霸服")..tostring(i)
        end
        if v.m_Id==myServerId then
            serverNameLabel1.text=LocalString.GetString("全民争霸服")..tostring(i)
        end
        local go=NGUITools.AddChild(parent.gameObject,itemTemplate)
        go:SetActive(true)
        self:InitItem(go.transform,i,v.m_OnlinePlayerNum)
        UIEventListener.Get(go).onClick=DelegateFactory.VoidDelegate(function(p)
            local cnt=parent.childCount
            for i=1,cnt do
                local g=parent:GetChild(i-1).gameObject
                local cmp=g:GetComponent(typeof(QnSelectableButton))
                if g==p then
                    cmp:SetSelected(true,false)
                else
                    cmp:SetSelected(false,false)
                end
            end
            self.m_CurServerIndex = i
        end)
    end 
end
function CLuaQMPKServerChosenWnd:OnSelectAtRow( index) 
    self.m_CurServerIndex = index
end
function CLuaQMPKServerChosenWnd:OnChangeButtonClick( go) 
    if self.m_CurServerIndex < 1 or self.m_CurServerIndex > #CLuaQMPKMgr.m_ServerList then
        return
    end

    Gac2Gas.RequestTeleportToOtherQmpkServer(CLuaQMPKMgr.m_ServerList[self.m_CurServerIndex].m_Id)
end

function CLuaQMPKServerChosenWnd:InitItem(transform,index, onlinePlayerNum)
    local m_NameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local m_StateSprite = transform:Find("StateSprite"):GetComponent(typeof(UISprite))

    m_NameLabel.text = LocalString.GetString("全民争霸服").. tostring(index)
    local status = EServerStatus.Idle
    if onlinePlayerNum > CQuanMinPKMgr.Inst.m_ServerLimitRed then
        status = EServerStatus.VeryBusy
    elseif onlinePlayerNum > CQuanMinPKMgr.Inst.m_ServerLimitYellow then
        status = EServerStatus.Busy
    end

    m_StateSprite.spriteName = CGameServer.GetStatusSprite(status)
end

return CLuaQMPKServerChosenWnd
