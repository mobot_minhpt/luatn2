
------------------------------ HEADER ------------------------------

--加足够多的中文让enca能识别出来是utf-8的
local random = math.random
local max = math.max
local min = math.min
local abs = math.abs
local ceil = math.ceil
local floor = math.floor
local log = math.log
local cos = math.cos
local sin = math.sin
local pi = math.pi
local round = function (num, n)
    if n > 0 then
        local scale = math.pow(10, n-1)
        return math.floor(num / scale + 0.5) * scale
    elseif n < 0 then
        local scale = math.pow(10, n)
        return math.floor(num / scale + 0.5) * scale
    elseif n == 0 then
        return num
    end
end

local power = function(a, b) return a^b end
local TRUE, FALSE = true, false
local True, False = true, false

local function GetIdBySkillCls(character, skillCls)
	local skillProp = character:SkillProp()
	return skillProp:GetSkillCls2Id_At(skillCls)
end

local function GetGradeBySkillCls(character, skillCls)
	local skillId = g_SkillMgr:GetSkillIdWithDeltaByCls(character, skillCls)
	if not skillId then return 0 end
	return skillId % 100
end

local function GetOriGradeBySkillCls(character, skillCls)
	local skillId = GetIdBySkillCls(character, skillCls)
	if not skillId then return 0 end
	return skillId % 100
end

local function GetGradeByBuffCls(character, buffCls)
	return g_BuffMgr:GetBuffLvByCls(character, buffCls) or 0
end

local function ConcatId(cls, grade)
	return cls * 100 + grade
end

local function IsGhostEquipObject(obj)
	if obj and obj:TypeIs(EnumObjectType.Item) then
		return obj:IsGhostEquipObject()
	end
	return false
end

local function IsPurpleEquipObject(obj)
	if obj and obj:TypeIs(EnumObjectType.Item) then
		return obj:IsPurpleEquipObject()
	end
	return false
end

local function SceneEvent(obj, eventName, ...)
	AIEvent.BroadcastEventInScene(obj, eventName, ...)
end

local function IsCharacterDeath(d)
 return g_StatusMgr:IsCharacterDeath(d)
end

local function IsCharacterDizzy(d)
 return g_StatusMgr:IsCharacterDizzy(d)
end

local function IsCharacterSleep(d)
 return g_StatusMgr:IsCharacterSleep(d)
end

local function IsCharacterChaos(d)
 return g_StatusMgr:IsCharacterChaos(d)
end

local function IsCharacterBind(d)
 return g_StatusMgr:IsCharacterBind(d)
end

local function IsCharacterSilence(d)
 return g_StatusMgr:IsCharacterSilence(d)
end

local function IsCharacterBlind(d)
 return g_StatusMgr:IsCharacterBlind(d)
end

local function IsCharacterPetrify(d)
 return g_StatusMgr:IsCharacterPetrify(d)
end

local function IsCharacterTie(d)
 return g_StatusMgr:IsCharacterTie(d)
end


-- 这个方法不够通用,考虑放在其他地方
local function IsIdInEventTipsIdSet(tipId, toCheckId)
	if not Message_EventTips[tipId] then
		return false
	end
	
	return g_GasMessageMgr:IsIdInEventTipsIdSet(tipId, toCheckId)
end

local function PlayerTaskEvent(player, event, isShare, eventTimes)
	if not player:TypeIs(EnumObjectType.Player) then return end
	g_TaskMgr:OnPlayerTaskEvent(player, event, isShare, eventTimes)
end

local function GetPlayerClass(player)
	if player and player:TypeIs(EnumObjectType.Player) then
		return player:GetClass()
	end
end

local function GetPlayerGender(player)
	if player and player:TypeIs(EnumObjectType.Player) then
		return player:GetGender()
	end
end

local function IsSceneTemplateId(self, ...)
	local scene = self and self.GetScene and self:GetScene()
	local sceneTempId = scene and scene:GetTemplateId()

    local count = select("#", ...)
	for i = 1, count do
        local tempId = select(i, ...)
		if tempId == sceneTempId then
			return 1
		end
	end
	return 0
end

local function GetBuffClsCount(character, ...)
	return g_BuffMgr:GetBuffClsCount(character, ...)
end

local function GetForwardPixelPosByRange(character, range)
	if not character then
		LogCallContext_lua()
		return
	end

	local __RayInterceptByDir = function(vecFrom,dir,dist)
		local dx = math.cos(dir) * dist
		local dy = math.sin(dir) * dist
		if(dx > 0) then	dx = math.floor(dx)
		else dx = math.ceil(dx) end
		if(dy > 0) then dy = math.floor(dy)
		else dy = math.ceil(dy) end
		return { x = vecFrom.x + dx, y = vecFrom.y + dy}
	end

	if not range then
		range = 1
	end

	local pixelPos = {}
	pixelPos.x, pixelPos.y = character.m_EngineObject:GetPixelPosv()

	local direction = character:GetDirection()
	local radian = DegreeToRadian(direction)
	
    local rangeInPixel = CastGridValueToPixelValue_V2(range * 0.98);

    local retPixelPos = __RayInterceptByDir(pixelPos, radian, rangeInPixel)
    return retPixelPos.x, retPixelPos.y
end

local __STATIC_TABLE = {}
local __STATIC_TABLE_MT = {
    __newindex = function(t,k,v) error("Attempt to modify a readonly table.") end,
}

local function MAKE_STATIC_TABLE(tbl)
	if not rawget(_G, "PUB_RELEASE") then
        for _, v in pairs(tbl) do
            if type(v) == "table" then
               setmetatable(v, __STATIC_TABLE_MT)
            else
            	error("v is not table")
            end
        end
		setmetatable(tbl, __STATIC_TABLE_MT)
	end
end

local function IsInSameTeam(a, d)
	if not a or not a:TypeIs(EnumObjectType.Player) then return false end
	if not d or not d:TypeIs(EnumObjectType.Player) then return false end
	local playerId1 = a.m_Id
	local playerId2 = d.m_Id
	return g_ShadowTeamMgr:IsTwoPlayerInSameTeam(playerId1, playerId2) and true or false
end

local function IsInSameTeamGroup(a, d)
	if not a or not a:TypeIs(EnumObjectType.Player) then return false end
	if not d or not d:TypeIs(EnumObjectType.Player) then return false end
	local playerId1 = a.m_Id
	local playerId2 = d.m_Id
	return g_GasTeamGroupMgr:IsTwoPlayerInSameTeamGroup(playerId1, playerId2) and true or false
end

local function IsInSameGuild(a, d)
	if not a or not a:TypeIs(EnumObjectType.Player) then return false end
	if not d or not d:TypeIs(EnumObjectType.Player) then return false end
	local guildId1 = a:BasicProp():GetGuildId()
	local guildId2 = d:BasicProp():GetGuildId()
	return guildId1 ~= 0 and guildId1 == guildId2
end

------------------------------ HEADER ------------------------------


AllFormulas.Action_Formula = AllFormulas.Action_Formula or {}
AllFormulas.Action_Formula[173] = AllFormulas.Action_Formula[173] or {  }
AllFormulas.Action_Formula[173].Formula = function(a, d, e)
    local ret = nil
    ret = floor(e[1] * 100 + (e[2] - 0.85) * 20000 + (e[3] - 0.8) * 28000 + e[4] * 1200 + e[5] * 150 + e[6] * 2000 + e[7] * 8500 + e[8] * 5500 + e[9] * 15000 + e[10] * 100)
    return ret
end


AllFormulas.Action_Formula[284] = AllFormulas.Action_Formula[284] or {  }
AllFormulas.Action_Formula[284].Formula = function(a, d, e)
    local ret = nil
    ret = floor(((0.8 + a:GetRealLevel() * (0.1 + 0.045 * min(1, max(0, a:GetRealLevel() - 69)) + 0.04 * min(1, max(0, a:GetRealLevel() - 89)) + 0.005 * min(1, max(0, a:GetRealLevel() - 109)))) * (0.2 * a:GetRealLevel() + 2) - 0.9) * 10 * e[1] * min(1, max(0, a:GetRealLevel() / 20 * 0.35 - 0.1)))
    return ret
end


AllFormulas.Action_Formula[373] = AllFormulas.Action_Formula[373] or {  }
AllFormulas.Action_Formula[373].Formula = function(a, d, e)
    local ret = nil
    ret = floor((1 + max(0, min(1, e[1] - 69)) + 2 * max(0, min(1, e[1] - 89)) + 4 * max(0, min(1, e[1] - 109)) + 8 * max(0, min(1, e[1] - 129))) * (max(0, min(1, e[2])) + max(0, min(1, e[2] - 1)) * 5 + max(0, min(1, e[2] - 2)) * 14 + max(0, min(1, e[2] - 3)) * 140 + max(0, min(1, e[2] - 4)) * 160 + max(0, min(1, e[2] - 5)) * 330 + max(0, min(1, e[2] - 6)) * 670))
    return ret
end


AllFormulas.Action_Formula[450] = AllFormulas.Action_Formula[450] or {  }
AllFormulas.Action_Formula[450].Formula = function(a, d, e)
    local ret = nil
    ret = 1 + min(1, max(0, e[1] - 3)) * 2 + min(1, max(0, e[1] - 4)) * 47 + min(1, max(0, e[1] - 5)) * 450 + min(1, max(0, e[1] - 6)) * 2000 + min(1, max(0, e[1] - 7)) * 10000 + (min(1, max(0, e[3] - 5)) * 50 + min(1, max(0, e[3] - 6)) * 250 + min(1, max(0, e[3] - 7)) * 700) * floor(e[2] / 45)
    return ret
end


AllFormulas.Action_Formula[465] = AllFormulas.Action_Formula[465] or {  }
AllFormulas.Action_Formula[465].Formula = function(a, d, e)
    local ret = nil
    ret = floor((e[1] - 1) * 5 * (1 + 0.5 * ((e[2] - 1) + max(0, e[2] - 4) * 2)) + min(e[3], 1 / 50) * 25000 + max(0, e[3] - 1 / 50) * 30000 + min(e[4], 1 / 50) * 25000 + max(0, e[4] - 1 / 50) * 15000)
    return ret
end


AllFormulas.Action_Formula[490] = AllFormulas.Action_Formula[490] or {  }
AllFormulas.Action_Formula[490].Formula = function(a, d, e)
    local ret = nil
    ret = e[1] * 8 + max(0, e[1] - 12) * 20
    return ret
end


AllFormulas.Action_Formula[544] = AllFormulas.Action_Formula[544] or {  }
AllFormulas.Action_Formula[544].Formula = function(a, d, e)
    local ret = nil
    ret = min(1, e[2] / (e[1] + max(0, min(1, e[1] - e[2] - 7 - max(0, e[1] - 149) * max(0, 151 - e[1]) * 13)) * e[1] * 0.12 + max(0, min(1, e[1] - e[2] - 7)) * max(0, e[1] - 136) * max(0, 138 - e[1]) * 14 + max(0, min(1, e[1] - e[2] - 20)) * max(0, e[1] - 149) * max(0, 151 - e[1]) * 11))
    return ret
end


AllFormulas.Action_Formula[545] = AllFormulas.Action_Formula[545] or {  }
AllFormulas.Action_Formula[545].Formula = AllFormulas.Action_Formula[544].Formula


AllFormulas.Action_Formula[546] = AllFormulas.Action_Formula[546] or {  }
AllFormulas.Action_Formula[546].Formula = AllFormulas.Action_Formula[544].Formula


AllFormulas.Action_Formula[547] = AllFormulas.Action_Formula[547] or {  }
AllFormulas.Action_Formula[547].Formula = AllFormulas.Action_Formula[544].Formula


AllFormulas.Action_Formula[548] = AllFormulas.Action_Formula[548] or {  }
AllFormulas.Action_Formula[548].Formula = AllFormulas.Action_Formula[544].Formula


AllFormulas.Action_Formula[549] = AllFormulas.Action_Formula[549] or {  }
AllFormulas.Action_Formula[549].Formula = AllFormulas.Action_Formula[544].Formula


AllFormulas.Action_Formula[550] = AllFormulas.Action_Formula[550] or {  }
AllFormulas.Action_Formula[550].Formula = AllFormulas.Action_Formula[544].Formula


AllFormulas.Action_Formula[551] = AllFormulas.Action_Formula[551] or {  }
AllFormulas.Action_Formula[551].Formula = AllFormulas.Action_Formula[544].Formula


AllFormulas.Action_Formula[552] = AllFormulas.Action_Formula[552] or {  }
AllFormulas.Action_Formula[552].Formula = function(a, d, e)
    local ret = nil
    ret = min(1, e[2] / (e[1] + max(0, min(1, e[1] - e[2] - 7 - max(0, e[1] - 149) * max(0, 151 - e[1]) * 13)) * e[1] * 0.12 + max(0, min(1, e[1] - e[2] - 7)) * max(0, e[1] - 136) * max(0, 138 - e[1]) * 14 + (max(0, min(1, e[1] - e[2] - 20)) + max(0, min(0.1, e[1] - e[2] - 12))) * max(0, e[1] - 149) * max(0, 151 - e[1]) * 15))
    return ret
end


AllFormulas.Action_Formula[553] = AllFormulas.Action_Formula[553] or {  }
AllFormulas.Action_Formula[553].Formula = AllFormulas.Action_Formula[552].Formula


AllFormulas.Action_Formula[554] = AllFormulas.Action_Formula[554] or {  }
AllFormulas.Action_Formula[554].Formula = AllFormulas.Action_Formula[552].Formula


AllFormulas.Action_Formula[555] = AllFormulas.Action_Formula[555] or {  }
AllFormulas.Action_Formula[555].Formula = AllFormulas.Action_Formula[552].Formula


AllFormulas.Action_Formula[556] = AllFormulas.Action_Formula[556] or {  }
AllFormulas.Action_Formula[556].Formula = function(a, d, e)
    local ret = nil
    ret = min(1, e[2] / (e[1] + max(0, min(1, e[1] - e[2] - 7 - max(0, e[1] - 149) * max(0, 151 - e[1]) * 13)) * e[1] * 0.12 + max(0, min(1, e[1] - e[2] - 7)) * max(0, e[1] - 136) * max(0, 138 - e[1]) * 14 + (1.5 * max(0, min(1, 130 - e[2])) - 0.5 * min(max(0, (150 - e[2]) / 13), 1)) * max(0, e[1] - 149) * max(0, 151 - e[1]) * 8.8))
    return ret
end


AllFormulas.Action_Formula[560] = AllFormulas.Action_Formula[560] or {  }
AllFormulas.Action_Formula[560].Formula = function(a, d, e)
    local ret = nil
    ret = 1500000 + (e[1] - 1) * 1000000
    return ret
end


AllFormulas.Action_Formula[564] = AllFormulas.Action_Formula[564] or {  }
AllFormulas.Action_Formula[564].Formula = function(a, d, e)
    local ret = nil
    ret = floor(power(e[1], 2) * 0.18)
    return ret
end


AllFormulas.Action_Formula[565] = AllFormulas.Action_Formula[565] or {  }
AllFormulas.Action_Formula[565].Formula = function(a, d, e)
    local ret = nil
    ret = floor(power(e[1], 2) * 0.0003)
    return ret
end


AllFormulas.Action_Formula[571] = AllFormulas.Action_Formula[571] or {  }
AllFormulas.Action_Formula[571].Formula = function(a, d, e)
    local ret = nil
    ret = 950000000 + min(e[1], 39) * 15000000 + min(1, max(e[1] - 39, 0)) * min(e[1] - 39, 40) * 11000000 + min(1, max(e[1] - 79, 0)) * min(e[1] - 79, 40) * 7000000 + min(1, max(e[1] - 119, 0)) * min(e[1] - 119, 40) * 5000000
    return ret
end


AllFormulas.Action_Formula[578] = AllFormulas.Action_Formula[578] or {  }
AllFormulas.Action_Formula[578].Formula = function(a, d, e)
    local ret = nil
    ret = e[1] * 5
    return ret
end


AllFormulas.Action_Formula[586] = AllFormulas.Action_Formula[586] or {  }
AllFormulas.Action_Formula[586].Formula = function(a, d, e)
    local ret = nil
    ret = min(400, e[1]) / 200 + max(0, e[1] - 400) / 50
    return ret
end


AllFormulas.Action_Formula[587] = AllFormulas.Action_Formula[587] or {  }
AllFormulas.Action_Formula[587].Formula = function(a, d, e)
    local ret = nil
    ret = min(4000, min(200, e[1]) / 100 + min(100, max(0, e[1] - 200)) + max(0, e[1] - 300) * 30) / 3200
    return ret
end


AllFormulas.Action_Formula[588] = AllFormulas.Action_Formula[588] or {  }
AllFormulas.Action_Formula[588].Formula = function(a, d, e)
    local ret = nil
    ret = min(2.5, 1 + min(100, max(0, e[1] - 300)) * 0.002 + max(0, e[1] - 400) * 0.005) - 1
    return ret
end


AllFormulas.Action_Formula[615] = AllFormulas.Action_Formula[615] or {  }
AllFormulas.Action_Formula[615].Formula = function(a, d, e)
    local ret = nil
    ret = 30000
    return ret
end


AllFormulas.Action_Formula[617] = AllFormulas.Action_Formula[617] or {  }
AllFormulas.Action_Formula[617].Formula = function(a, d, e)
    local ret = nil
    ret = 3 + min(1, max(e[1] - 69, 0)) * 2 + min(1, max(e[1] - 109, 0)) * 2
    return ret
end


AllFormulas.Action_Formula[631] = AllFormulas.Action_Formula[631] or {  }
AllFormulas.Action_Formula[631].Formula = function(a, d, e)
    local ret = nil
    ret = 2 * e[1]
    return ret
end


AllFormulas.Action_Formula[632] = AllFormulas.Action_Formula[632] or {  }
AllFormulas.Action_Formula[632].Formula = function(a, d, e)
    local ret = nil
    ret = floor(power(1.8, e[1]) / 370) * 300
    return ret
end


AllFormulas.Action_Formula[633] = AllFormulas.Action_Formula[633] or {  }
AllFormulas.Action_Formula[633].Formula = function(a, d, e)
    local ret = nil
    ret = 2000
    return ret
end


AllFormulas.Action_Formula[634] = AllFormulas.Action_Formula[634] or {  }
AllFormulas.Action_Formula[634].Formula = function(a, d, e)
    local ret = nil
    ret = 3
    return ret
end


AllFormulas.Action_Formula[638] = AllFormulas.Action_Formula[638] or {  }
AllFormulas.Action_Formula[638].Formula = function(a, d, e)
    local ret = nil
    ret = 750000 + (e[1] - 1) * 500000
    return ret
end


AllFormulas.Action_Formula[639] = AllFormulas.Action_Formula[639] or {  }
AllFormulas.Action_Formula[639].Formula = AllFormulas.Action_Formula[638].Formula


AllFormulas.Action_Formula[641] = AllFormulas.Action_Formula[641] or {  }
AllFormulas.Action_Formula[641].Formula = function(a, d, e)
    local ret = nil
    ret = ((0.8 + e[2] * (0.1 + 0.05 * min(1, max(0, e[2] - 69)) + 0.03 * min(1, max(0, e[2] - 89)) + 0.01 * min(1, max(0, e[2] - 109)))) * (0.2 * e[2] + 2) - 0.9) * 120 * e[1]
    return ret
end


AllFormulas.Action_Formula[642] = AllFormulas.Action_Formula[642] or {  }
AllFormulas.Action_Formula[642].Formula = function(a, d, e)
    local ret = nil
    ret = 0
    return ret
end


AllFormulas.Action_Formula[644] = AllFormulas.Action_Formula[644] or {  }
AllFormulas.Action_Formula[644].Formula = function(a, d, e)
    local ret = nil
    ret = 0.5 + e[1]
    return ret
end


AllFormulas.Action_Formula[646] = AllFormulas.Action_Formula[646] or {  }
AllFormulas.Action_Formula[646].Formula = function(a, d, e)
    local ret = nil
        if e[1] == 0 then
        ret = 60 + min(60, max(0, e[2] - 0) * 12 + max(0, e[3] - 0) * 5 + max(0, e[4] - 0) * 6)
    elseif e[1] == 2 then
        ret = 30 + min(60, max(0, e[2] - 0) * 12 + max(0, e[3] - 0) * 5 + max(0, e[4] - 0) * 6)
    else
        ret = 30 + min(30, max(0, e[2] - 0) * 12 + max(0, e[3] - 0) * 5 + max(0, e[4] - 0) * 6)
    end

    return ret
end


AllFormulas.Action_Formula[647] = AllFormulas.Action_Formula[647] or {  }
AllFormulas.Action_Formula[647].Formula = function(a, d, e)
    local ret = nil
    ret = (e[1] == 1 or e[1] % 20 == 0) and 1 or 0
    return ret
end


AllFormulas.Action_Formula[648] = AllFormulas.Action_Formula[648] or {  }
AllFormulas.Action_Formula[648].Formula = function(a, d, e)
    local ret = nil
    local eq_lv = GetGradeByBuffCls(a, 640822)
    local s_lv = GetGradeBySkillCls(a, 903017)
    local reduce_du = 0.01 * a:FightProp():GetParamIgnoreAntiPoison() / max(5, 13 - s_lv * 0.2)
    local reduce_max = 0.01 * min(35, (s_lv * 2 + 75) / max(5, 13 - s_lv * 0.2))
    local Skill_cd = e[1]
    if eq_lv > 0 then
        Skill_cd = e[1] * (1 - 0.04 - 0.04 * eq_lv + max(0, 2 - eq_lv) * 0.04)
    end

    if s_lv > 0 then
        Skill_cd = Skill_cd * max(1 - reduce_du, 1 - reduce_max)
    end

    ret = Skill_cd
    return ret
end


AllFormulas.Action_Formula[656] = AllFormulas.Action_Formula[656] or {  }
AllFormulas.Action_Formula[656].Formula = function(a, d, e)
    local ret = nil
    local new_cd = e[1]
    local subType, bothHand = g_SkillMgr:GetWeaponParam(a)
    if bothHand == 2 then
        new_cd = new_cd * 0.75
    end

    ret = new_cd
    return ret
end


AllFormulas.Action_Formula[657] = AllFormulas.Action_Formula[657] or {  }
AllFormulas.Action_Formula[657].Formula = function(a, d, e)
    local ret = nil
    ret = e[1] * 0.6
    return ret
end


AllFormulas.Action_Formula[658] = AllFormulas.Action_Formula[658] or {  }
AllFormulas.Action_Formula[658].Formula = function(a, d, e)
    local ret = nil
    ret = e[1] * 1.25
    return ret
end


AllFormulas.Action_Formula[659] = AllFormulas.Action_Formula[659] or {  }
AllFormulas.Action_Formula[659].Formula = function(a, d, e)
    local ret = nil
    ret = e[1]
    return ret
end


AllFormulas.Action_Formula[713] = AllFormulas.Action_Formula[713] or {  }
AllFormulas.Action_Formula[713].Formula = function(a, d, e)
    local ret = nil
    ret = floor(((0.8 + e[2] * (0.1 + 0.05 * min(1, max(0, e[2] - 69)) + 0.05 * min(1, max(0, e[2] - 89)) + 0.05 * min(1, max(0, e[2] - 109)))) * (0.2 * e[2] + 2) - 0.9) * 60 * e[1])
    return ret
end


AllFormulas.Action_Formula[714] = AllFormulas.Action_Formula[714] or {  }
AllFormulas.Action_Formula[714].Formula = function(a, d, e)
    local ret = nil
    ret = floor(e[1] * 3.5)
    return ret
end


AllFormulas.Action_Formula[715] = AllFormulas.Action_Formula[715] or {  }
AllFormulas.Action_Formula[715].Formula = function(a, d, e)
    local ret = nil
    ret = floor(e[1] / 20000) + 10
    return ret
end


AllFormulas.Action_Formula[739] = AllFormulas.Action_Formula[739] or {  }
AllFormulas.Action_Formula[739].Formula = function(a, d, e)
    local ret = nil
    local qc = 0
    local pt_num_ret = 200
    local pt_beilv_ret = 600
    local zy_ret = 3600
    local zy_fen = 0
    local pt_fen = 0
    if e[8] > 0 then
        local qc_jc = { 0.1, 0.2, 0.32, 0.46, 0.62, 0.8 }
        qc = qc_jc[e[8]]
    end

    local ratio = max(0, e[1] - 0.5) * 3 + max(0, e[1] - 1.75) * 4.5
    local qinmi = e[3] * e[3] * 0.03 + e[3] * 0.1
    local zizhi_max = max(0, 1.55 * e[2] - 500) * 0.0015 + max(0, 1.55 * e[2] - 4000) * 0.00225
    local zizhi_mid = max(0, 1.25 * e[2] - 500) * 0.0015 + max(0, 1.25 * e[2] - 4000) * 0.00225
    local zizhi_min = max(0, 0.8 * e[2] - 500) * 0.0015 + max(0, 0.8 * e[2] - 4000) * 0.00225
    local zizhi = 0.2 * (zizhi_max * 2 + zizhi_mid * 2 + zizhi_min)
    local beilv = max(1, (0.5 + qinmi + ratio + zizhi + e[5] * 0.2) * (0.4 + 0.6 * e[5] / 30))
    if e[4] > 0 then
        pt_fen = pt_beilv_ret * beilv * e[4] + pt_num_ret * e[4] * e[4]
    end

    if e[6] > 0 then
        zy_fen = (e[7] * 0.1 + 0.3) * e[6] * zy_ret * (1 + qc)
    end

    ret = max(0, floor(pt_fen + zy_fen))
    return ret
end


AllFormulas.Action_Formula[740] = AllFormulas.Action_Formula[740] or {  }
AllFormulas.Action_Formula[740].Formula = function(a, d, e)
    local ret = nil
    ret = min(2700, 600 + floor(21 * e[1] * e[1] / 576))
    return ret
end


AllFormulas.Action_Formula[806] = AllFormulas.Action_Formula[806] or {  }
AllFormulas.Action_Formula[806].Formula = function(a, d, e)
    local ret = nil
    local libaoTbl = e[1]
    local libaoCount_1 = libaoTbl[21020337] or 0
    local libaoCount_2 = libaoTbl[21020338] or 0
    local libaoCount_3 = libaoTbl[21020339] or 0
    local libaoCount_4 = libaoTbl[21020340] or 0
    local libaoCount_5 = libaoTbl[21020341] or 0
    local jackpot = 10 * libaoCount_1 + 10 * libaoCount_2 + 30 * libaoCount_3 + 110 * libaoCount_4 + 220 * libaoCount_5 + 100000
    ret = jackpot
    return ret
end


AllFormulas.Action_Formula[807] = AllFormulas.Action_Formula[807] or {  }
AllFormulas.Action_Formula[807].Formula = function(a, d, e)
    local ret = nil
    ret = floor(82 * e[1] + power(e[1] - 1, 2.4998) - 12 + max(0, 20 - e[1] * 10)) * 9
    return ret
end


AllFormulas.Action_Formula[822] = AllFormulas.Action_Formula[822] or {  }
AllFormulas.Action_Formula[822].Formula = function(a, d, e)
    local ret = nil
    ret = floor(power(e[1], 1.8) / 100) * 100 + 200
    return ret
end


AllFormulas.Action_Formula[823] = AllFormulas.Action_Formula[823] or {  }
AllFormulas.Action_Formula[823].Formula = function(a, d, e)
    local ret = nil
    ret = floor((e[1] - 15) * max(e[3] - 5, 0) * max(1, (e[2] / 5)) * 0.3125) / 1000
    return ret
end


AllFormulas.Action_Formula[826] = AllFormulas.Action_Formula[826] or {  }
AllFormulas.Action_Formula[826].Formula = function(a, d, e)
    local ret = nil
    ret = 5
    if (e[1] > 0 and e[1] < 6) then
        ret = floor(5 + 2 * e[1])
    end

    if (e[1] > 5 and e[1] < 16) then
        ret = floor(10 + e[1])
    end

    return ret
end


AllFormulas.Action_Formula[827] = AllFormulas.Action_Formula[827] or {  }
AllFormulas.Action_Formula[827].Formula = function(a, d, e)
    local ret = nil
    ret = 100000 + 150000 * e[1]
    return ret
end


AllFormulas.Action_Formula[842] = AllFormulas.Action_Formula[842] or {  }
AllFormulas.Action_Formula[842].Formula = function(a, d, e)
    local ret = nil
    ret = ((0.8 + e[1] * (0.1 + 0.05 * min(1, max(0, e[1] - 69)) + 0.05 * min(1, max(0, e[1] - 89)) + 0.05 * min(1, max(0, e[1] - 109)))) * (0.2 * e[1] + 2) - 0.9) * 1200
    return ret
end


AllFormulas.Action_Formula[856] = AllFormulas.Action_Formula[856] or {  }
AllFormulas.Action_Formula[856].Formula = function(a, d, e)
    local ret = nil
    ret = min(max((e[1] - 1), 0), 15) / 10 + 1
    return ret
end


AllFormulas.Action_Formula[901] = AllFormulas.Action_Formula[901] or {  }
AllFormulas.Action_Formula[901].Formula = function(a, d, e)
    local ret = nil
    ret = e[1] * 30000000 + 30000000
    return ret
end


AllFormulas.Action_Formula[902] = AllFormulas.Action_Formula[902] or {  }
AllFormulas.Action_Formula[902].Formula = AllFormulas.Action_Formula[901].Formula


AllFormulas.Action_Formula[903] = AllFormulas.Action_Formula[903] or {  }
AllFormulas.Action_Formula[903].Formula = function(a, d, e)
    local ret = nil
    ret = e[1] * 20000000 + e[1] * e[1] * 40000000
    return ret
end


AllFormulas.Action_Formula[931] = AllFormulas.Action_Formula[931] or {  }
AllFormulas.Action_Formula[931].Formula = function(a, d, e)
    local ret = nil
    ret = 69 + (max(min(e[2] - 69, 1), 0) * 20) + (max(min(e[2] - 89, 1), 0) * 20) + (max(min(e[2] - 109, 1), 0) * max(e[2] - 109, 0))
    return ret
end


AllFormulas.Action_Formula[938] = AllFormulas.Action_Formula[938] or {  }
AllFormulas.Action_Formula[938].Formula = function(a, d, e)
    local ret = nil
    ret = (e[1] - 10) * 50000
    return ret
end


AllFormulas.Action_Formula[939] = AllFormulas.Action_Formula[939] or {  }
AllFormulas.Action_Formula[939].Formula = function(a, d, e)
    local ret = nil
    local catch_baseprop = { 1, 1, 1, 0.8, 0.5 }
    local grade_baseprop = { 0.75, 0.75, 0.6, 0.3, 0.2, 0.1, 0.1, 0.075, 0.075, 0.05 }
    local leveldiff = e[3] - e[1]
    local skilldiff = e[4] - e[2]
    local monstergrade = e[4]
    if leveldiff > 20 or skilldiff > 2 then
        ret = 0
    else
        local i = 3 + min(2, max(-2, e[4] - e[2]))
        local catch_ret = min(1, max(-0.1, min(leveldiff / (-200), 0.1)) + catch_baseprop[i]) * grade_baseprop[monstergrade]
        ret = max(0, catch_ret)
    end

    return ret
end


AllFormulas.Action_Formula[942] = AllFormulas.Action_Formula[942] or {  }
AllFormulas.Action_Formula[942].Formula = function(a, d, e)
    local ret = nil
    ret = min(5 * e[1], 330)
    return ret
end


AllFormulas.Action_Formula[944] = AllFormulas.Action_Formula[944] or {  }
AllFormulas.Action_Formula[944].Formula = function(a, d, e)
    local ret = nil
    local v = 0
                if e[1] >= 400 then
        v = 15
    elseif e[1] >= 350 then
        v = 10
    elseif e[1] >= 200 then
        v = 6
    elseif e[1] >= 100 then
        v = 3
    else
        v = 1
    end

    ret = v
    return ret
end


AllFormulas.Action_Formula[945] = AllFormulas.Action_Formula[945] or {  }
AllFormulas.Action_Formula[945].Formula = function(a, d, e)
    local ret = nil
    ret = e[1] * (1 - e[2]) * e[3] * e[4]
    return ret
end


AllFormulas.Action_Formula[946] = AllFormulas.Action_Formula[946] or {  }
AllFormulas.Action_Formula[946].Formula = function(a, d, e)
    local ret = nil
    ret = (e[1] * 0.016 + 1) / 300 * 500 / 9 * (e[2] * 1 + 1) * e[3] * min(e[1], 1)
    return ret
end


AllFormulas.Action_Formula[947] = AllFormulas.Action_Formula[947] or {  }
AllFormulas.Action_Formula[947].Formula = function(a, d, e)
    local ret = nil
    ret = e[1]
    return ret
end


AllFormulas.Action_Formula[948] = AllFormulas.Action_Formula[948] or {  }
AllFormulas.Action_Formula[948].Formula = function(a, d, e)
    local ret = nil
    ret = 0
    return ret
end


AllFormulas.Action_Formula[949] = AllFormulas.Action_Formula[949] or {  }
AllFormulas.Action_Formula[949].Formula = function(a, d, e)
    local ret = nil
    ret = (e[1] + e[2]) * e[3] * (e[5] + e[6]) * e[7]
    return ret
end


AllFormulas.Action_Formula[950] = AllFormulas.Action_Formula[950] or {  }
AllFormulas.Action_Formula[950].Formula = function(a, d, e)
    local ret = nil
    ret = floor(((0.8 + a:GetRealLevel() * (0.1 + 0.05 * min(1, max(0, a:GetRealLevel() - 69)) + 0.05 * min(1, max(0, a:GetRealLevel() - 89)) + 0.02 * min(1, max(0, a:GetRealLevel() - 109)))) * (0.2 * a:GetRealLevel() + 2) - 0.9) * 10 * e[1] * min(1, max(0, a:GetRealLevel() / 20 * 0.35 - 0.1))) + 50
    return ret
end


AllFormulas.Action_Formula[952] = AllFormulas.Action_Formula[952] or {  }
AllFormulas.Action_Formula[952].Formula = function(a, d, e)
    local ret = nil
    local v = 0
        if e[1] == 1 then
        v = 0
    elseif e[2] == 1 then
        v = 500 * 2 ^ (e[1] - 2)
    else
        v = 400 * e[2] * 2 ^ (e[1] - 2)
    end

    ret = v
    return ret
end


AllFormulas.Action_Formula[955] = AllFormulas.Action_Formula[955] or {  }
AllFormulas.Action_Formula[955].Formula = function(a, d, e)
    local ret = nil
    local wqx_extra_hp = 0
    if e[1] >= e[2] then
        wqx_extra_hp = floor(0.3 * (64 * e[2] + 2.073 * e[2] ^ 2 + 0.001326 * e[2] ^ 3))
    else
        wqx_extra_hp = floor(max(0, 0.3 * (64 * e[1] + 2.073 * e[1] ^ 2 + 0.001326 * e[1] ^ 3)))
    end

    ret = wqx_extra_hp
    return ret
end


AllFormulas.Action_Formula[956] = AllFormulas.Action_Formula[956] or {  }
AllFormulas.Action_Formula[956].Formula = function(a, d, e)
    local ret = nil
    ret = min((e[1] / 10000), 0.2)
    return ret
end


AllFormulas.Action_Formula[964] = AllFormulas.Action_Formula[964] or {  }
AllFormulas.Action_Formula[964].Formula = function(a, d, e)
    local ret = nil
    local e1_ret = e[1] or 10
    local b_lv = GetGradeByBuffCls(d, 642458)
    if b_lv > 0 then
        e1_ret = e1_ret + 3 * max(0.02, 0.19 + 0.027 * b_lv - max(0, 2 - b_lv))
    end

    ret = e1_ret
    return ret
end


AllFormulas.Action_Formula[965] = AllFormulas.Action_Formula[965] or {  }
AllFormulas.Action_Formula[965].Formula = function(a, d, e)
    local ret = nil
    local e1_ret = e[1] or 0.4
    local e2_ret = e[2] or 0.15
    local b_lv = GetGradeByBuffCls(d, 642458)
    if b_lv > 0 then
        e1_ret = e1_ret + e2_ret * max(0.02, 0.19 + 0.027 * b_lv - max(0, 2 - b_lv))
    end

    ret = e1_ret
    return ret
end


AllFormulas.Action_Formula[966] = AllFormulas.Action_Formula[966] or {  }
AllFormulas.Action_Formula[966].Formula = function(a, d, e)
    local ret = nil
    local e1_ret = e[1]
    local b_lv = GetGradeByBuffCls(d, 642458)
    if b_lv > 0 then
        e1_ret = e1_ret - e1_ret * max(0.02, 0.19 + 0.027 * b_lv - max(0, 2 - b_lv))
    end

    ret = max(0, e1_ret)
    return ret
end


AllFormulas.Action_Formula[980] = AllFormulas.Action_Formula[980] or {  }
AllFormulas.Action_Formula[980].Formula = function(a, d, e)
    local ret = nil
    ret = e[1] * (1 + e[2] + e[4]) * e[3] * 20
    return ret
end


AllFormulas.Action_Formula[985] = AllFormulas.Action_Formula[985] or {  }
AllFormulas.Action_Formula[985].Formula = function(a, d, e)
    local ret = nil
    ret = 0.5 * (a:GetMaxLevel() * 200 * a:GetMaxLevel() + 5000 * a:GetMaxLevel())
    return ret
end


AllFormulas.Action_Formula[993] = AllFormulas.Action_Formula[993] or {  }
AllFormulas.Action_Formula[993].Formula = function(a, d, e)
    local ret = nil
    ret = 5 + e[1] * 5 / 300
    return ret
end


AllFormulas.Action_Formula[994] = AllFormulas.Action_Formula[994] or {  }
AllFormulas.Action_Formula[994].Formula = function(a, d, e)
    local ret = nil
    ret = 3 + max(0, (min(1, e[1] - 1))) + max(0, (min(1, e[1] - 2))) + max(0, (min(1, e[1] - 3)))
    return ret
end


AllFormulas.Action_Formula[1000] = AllFormulas.Action_Formula[1000] or {  }
AllFormulas.Action_Formula[1000].Formula = function(a, d, e)
    local ret = nil
    ret = floor(e[1] / 3500)
    return ret
end


AllFormulas.Action_Formula[1007] = AllFormulas.Action_Formula[1007] or {  }
AllFormulas.Action_Formula[1007].Formula = function(a, d, e)
    local ret = nil
    ret = e[1] * 1000
    return ret
end


AllFormulas.Action_Formula[1008] = AllFormulas.Action_Formula[1008] or {  }
AllFormulas.Action_Formula[1008].Formula = function(a, d, e)
    local ret = nil
    ret = floor(e[1] / 2)
    return ret
end


AllFormulas.Action_Formula[1009] = AllFormulas.Action_Formula[1009] or {  }
AllFormulas.Action_Formula[1009].Formula = function(a, d, e)
    local ret = nil
    ret = 100 * floor(100 * e[1] * e[2] / 4800)
    return ret
end


AllFormulas.Action_Formula[1020] = AllFormulas.Action_Formula[1020] or {  }
AllFormulas.Action_Formula[1020].Formula = function(a, d, e)
    local ret = nil
    ret = min(1, 1 - (min(125, e[1]) - 50) * 0.01 * min(1, max(0, e[2] - 260) / 300))
    return ret
end


AllFormulas.Action_Formula[1032] = AllFormulas.Action_Formula[1032] or {  }
AllFormulas.Action_Formula[1032].Formula = function(a, d, e)
    local ret = nil
    ret = 13 + 0.1 * (e[1] - 2 * e[2])
    return ret
end


AllFormulas.Action_Formula[1033] = AllFormulas.Action_Formula[1033] or {  }
AllFormulas.Action_Formula[1033].Formula = function(a, d, e)
    local ret = nil
    ret = 30 + 0.2 * (e[2] - e[1])
    return ret
end


AllFormulas.Action_Formula[1053] = AllFormulas.Action_Formula[1053] or {  }
AllFormulas.Action_Formula[1053].Formula = function(a, d, e)
    local ret = nil
            if e[1] <= 50 then
        ret = 1
    elseif e[1] <= 100 then
        ret = 2
    elseif e[1] <= 150 then
        ret = 3
    else
        ret = 4
    end

    return ret
end


AllFormulas.Action_Formula[1057] = AllFormulas.Action_Formula[1057] or {  }
AllFormulas.Action_Formula[1057].Formula = function(a, d, e)
    local ret = nil
    if e[1] <= 0 then
        ret = 0
    else
        ret = max(e[1] * 0.5, min(100000, e[1]))
    end

    return ret
end


AllFormulas.Action_Formula[1058] = AllFormulas.Action_Formula[1058] or {  }
AllFormulas.Action_Formula[1058].Formula = function(a, d, e)
    local ret = nil
    ret = 1 + 0.2 * (e[2] / e[1])
    return ret
end


AllFormulas.Action_Formula[1066] = AllFormulas.Action_Formula[1066] or {  }
AllFormulas.Action_Formula[1066].Formula = function(a, d, e)
    local ret = nil
                    if e[4] > 1609430400 then
        ret = 5
    elseif e[4] > 1577808000 then
        ret = 4
    elseif e[4] > 1546272000 then
        ret = 3
    elseif e[4] > 1514736000 then
        ret = 2
    elseif e[4] > 1474041600 then
        ret = 1
    else
        ret = 0
    end

    return ret
end


AllFormulas.Action_Formula[1070] = AllFormulas.Action_Formula[1070] or {  }
AllFormulas.Action_Formula[1070].Formula = function(a, d, e)
    local ret = nil
    ret = 20
    return ret
end


AllFormulas.Action_Formula[1071] = AllFormulas.Action_Formula[1071] or {  }
AllFormulas.Action_Formula[1071].Formula = function(a, d, e)
    local ret = nil
    local ret1 = 0
    local ret2 = 1
            if e[1] == 90 then
        if e[2] <= 7 then
            ret1 = 1
        else
            ret1 = 1 / (e[2] - 6)
        end

    elseif e[1] == 60 then
                        if e[2] <= 1 then
            ret1 = 1
        elseif e[2] == 2 then
            ret1 = 0.25
        elseif e[2] == 3 then
            ret1 = 1 / 6
        else
            ret1 = 0.1 / (2 ^ (e[2] - 4))
        end

    elseif e[1] == 30 then
                if e[2] >= 3 then
            ret1 = 0
        elseif e[2] == 2 then
            ret1 = 0.007
        else
            ret1 = 0.2 / 2 ^ e[2]
        end

    else
        ret1 = 0
    end

    ret = { ret1, ret2 }
    return ret
end


AllFormulas.Action_Formula[1072] = AllFormulas.Action_Formula[1072] or {  }
AllFormulas.Action_Formula[1072].Formula = function(a, d, e)
    local ret = nil
    ret = 40 + e[1] * 2
    return ret
end


AllFormulas.Action_Formula[1091] = AllFormulas.Action_Formula[1091] or {  }
AllFormulas.Action_Formula[1091].Formula = function(a, d, e)
    local ret = nil
    ret = min(35, floor(max(0, e[1] - 50) / 20) * 5 + floor(max(0, e[1] - 100) / 20) * 5)
    return ret
end


AllFormulas.Action_Formula[1093] = AllFormulas.Action_Formula[1093] or {  }
AllFormulas.Action_Formula[1093].Formula = function(a, d, e)
    local ret = nil
    ret = floor(((0.8 + a:GetRealLevel() * (0.1 + 0.05 * min(1, max(0, a:GetRealLevel() - 69)) + 0.045 * min(1, max(0, a:GetRealLevel() - 89)) + 0.005 * min(1, max(0, a:GetRealLevel() - 109)))) * (0.2 * a:GetRealLevel() + 2) - 0.9) * 10 * e[1] * min(1, max(0, a:GetRealLevel() / 20 * 0.35 - 0.1)))
    return ret
end


AllFormulas.Action_Formula[1094] = AllFormulas.Action_Formula[1094] or {  }
AllFormulas.Action_Formula[1094].Formula = function(a, d, e)
    local ret = nil
    ret = max(0, floor((12 - e[5]) / 3))
    return ret
end


AllFormulas.Action_Formula[1095] = AllFormulas.Action_Formula[1095] or {  }
AllFormulas.Action_Formula[1095].Formula = function(a, d, e)
    local ret = nil
    ret = max(0, floor((13 - e[5]) / 3))
    return ret
end


AllFormulas.Action_Formula[1096] = AllFormulas.Action_Formula[1096] or {  }
AllFormulas.Action_Formula[1096].Formula = function(a, d, e)
    local ret = nil
    ret = max(0, floor((14 - e[5]) / 3))
    return ret
end


AllFormulas.Action_Formula[1097] = AllFormulas.Action_Formula[1097] or {  }
AllFormulas.Action_Formula[1097].Formula = function(a, d, e)
    local ret = nil
    if e[1] <= 3 then
        ret = e[1] + 1
    else
        ret = e[1] * 2 - 2
    end

    return ret
end


AllFormulas.Action_Formula[1100] = AllFormulas.Action_Formula[1100] or {  }
AllFormulas.Action_Formula[1100].Formula = function(a, d, e)
    local ret = nil
    ret = floor(e[1] * e[2] * e[3] * e[4])
    return ret
end


AllFormulas.Action_Formula[1101] = AllFormulas.Action_Formula[1101] or {  }
AllFormulas.Action_Formula[1101].Formula = function(a, d, e)
    local ret = nil
    ret = floor(e[1] * e[2] * e[3])
    return ret
end


AllFormulas.Action_Formula[1102] = AllFormulas.Action_Formula[1102] or {  }
AllFormulas.Action_Formula[1102].Formula = function(a, d, e)
    local ret = nil
    ret = min(max(floor(e[1] / 25), 1), 2)
    return ret
end


AllFormulas.Action_Formula[1103] = AllFormulas.Action_Formula[1103] or {  }
AllFormulas.Action_Formula[1103].Formula = function(a, d, e)
    local ret = nil
    ret = max(5 ^ e[1] - 1, 0)
    return ret
end


AllFormulas.Action_Formula[1104] = AllFormulas.Action_Formula[1104] or {  }
AllFormulas.Action_Formula[1104].Formula = function(a, d, e)
    local ret = nil
    ret = max((e[1] - 6) * 3, 0)
    return ret
end


AllFormulas.Action_Formula[1105] = AllFormulas.Action_Formula[1105] or {  }
AllFormulas.Action_Formula[1105].Formula = function(a, d, e)
    local ret = nil
    ret = max((e[1] - 12) * 2, 0)
    return ret
end


AllFormulas.Action_Formula[1106] = AllFormulas.Action_Formula[1106] or {  }
AllFormulas.Action_Formula[1106].Formula = AllFormulas.Action_Formula[1105].Formula


AllFormulas.Action_Formula[1107] = AllFormulas.Action_Formula[1107] or {  }
AllFormulas.Action_Formula[1107].Formula = function(a, d, e)
    local ret = nil
    ret = ceil(max((e[1] - 20) * 0.8, 0))
    return ret
end


AllFormulas.Action_Formula[1108] = AllFormulas.Action_Formula[1108] or {  }
AllFormulas.Action_Formula[1108].Formula = function(a, d, e)
    local ret = nil
    ret = ceil(max((e[1] * 5 / 14 - 20) * 0.8, 0))
    return ret
end


AllFormulas.Action_Formula[1109] = AllFormulas.Action_Formula[1109] or {  }
AllFormulas.Action_Formula[1109].Formula = function(a, d, e)
    local ret = nil
    ret = max((e[1] - 2) * 3, 0)
    return ret
end


AllFormulas.Action_Formula[1110] = AllFormulas.Action_Formula[1110] or {  }
AllFormulas.Action_Formula[1110].Formula = function(a, d, e)
    local ret = nil
    ret = ceil(max((e[1] - 5) * 2, 0))
    return ret
end


AllFormulas.Action_Formula[1111] = AllFormulas.Action_Formula[1111] or {  }
AllFormulas.Action_Formula[1111].Formula = function(a, d, e)
    local ret = nil
    ret = ceil(max(e[1] * 0.25 - 4, 0))
    return ret
end


AllFormulas.Action_Formula[1112] = AllFormulas.Action_Formula[1112] or {  }
AllFormulas.Action_Formula[1112].Formula = function(a, d, e)
    local ret = nil
    ret = max(4 ^ (e[1] - 2), 1)
    return ret
end


AllFormulas.Action_Formula[1113] = AllFormulas.Action_Formula[1113] or {  }
AllFormulas.Action_Formula[1113].Formula = function(a, d, e)
    local ret = nil
    local i = 0
    local j = 0
                    if e[1] <= 69 then
        i = 2000
    elseif e[1] >= 70 and e[1] <= 89 then
        i = 2400
    elseif e[1] >= 90 and e[1] <= 109 then
        i = 2800
    elseif e[1] >= 110 and e[1] <= 129 then
        i = 3200
    elseif e[1] >= 130 then
        i = 3500
    end

                    if e[2] <= 1499 then
        j = 100
    elseif e[2] >= 1500 and e[2] <= 1999 then
        j = 200
    elseif e[2] >= 2000 and e[2] <= 2499 then
        j = 300
    elseif e[2] >= 2500 and e[2] <= 2999 then
        j = 400
    elseif e[2] >= 3000 then
        j = 500
    end

    ret = i + j
    return ret
end


AllFormulas.Action_Formula[1123] = AllFormulas.Action_Formula[1123] or {  }
AllFormulas.Action_Formula[1123].Formula = function(a, d, e)
    local ret = nil
    if e[1] == 0 then
        ret = 0
    else
        ret = max(ceil((e[2] / e[1] * 0.9 - 0.9) * 100) / 100, 0)
    end

    return ret
end


AllFormulas.Action_Formula[1141] = AllFormulas.Action_Formula[1141] or {  }
AllFormulas.Action_Formula[1141].Formula = function(a, d, e)
    local ret = nil
    ret = a:FightProp():GetParamGrade() * 150
    return ret
end


AllFormulas.Action_Formula[1142] = AllFormulas.Action_Formula[1142] or {  }
AllFormulas.Action_Formula[1142].Formula = function(a, d, e)
    local ret = nil
    ret = a:FightProp():GetParamGrade() * 50
    return ret
end


AllFormulas.Action_Formula[1143] = AllFormulas.Action_Formula[1143] or {  }
AllFormulas.Action_Formula[1143].Formula = function(a, d, e)
    local ret = nil
    ret = a:FightProp():GetParamGrade() * 30
    return ret
end


AllFormulas.Action_Formula[1144] = AllFormulas.Action_Formula[1144] or {  }
AllFormulas.Action_Formula[1144].Formula = function(a, d, e)
    local ret = nil
    ret = 50 * e[1]
    return ret
end


AllFormulas.Action_Formula[1145] = AllFormulas.Action_Formula[1145] or {  }
AllFormulas.Action_Formula[1145].Formula = function(a, d, e)
    local ret = nil
    ret = -0.3
    return ret
end


AllFormulas.Action_Formula[1146] = AllFormulas.Action_Formula[1146] or {  }
AllFormulas.Action_Formula[1146].Formula = function(a, d, e)
    local ret = nil
    ret = 0.2
    return ret
end


AllFormulas.Action_Formula[1147] = AllFormulas.Action_Formula[1147] or {  }
AllFormulas.Action_Formula[1147].Formula = function(a, d, e)
    local ret = nil
    ret = a:FightProp():GetParamGrade() * 3
    return ret
end


AllFormulas.Action_Formula[1148] = AllFormulas.Action_Formula[1148] or {  }
AllFormulas.Action_Formula[1148].Formula = function(a, d, e)
    local ret = nil
    ret = a:FightProp():GetParamGrade() * 0.8
    return ret
end


AllFormulas.Action_Formula[1149] = AllFormulas.Action_Formula[1149] or {  }
AllFormulas.Action_Formula[1149].Formula = function(a, d, e)
    local ret = nil
    ret = a:FightProp():GetParamGrade() * 80 * e[1]
    return ret
end


AllFormulas.Action_Formula[1150] = AllFormulas.Action_Formula[1150] or {  }
AllFormulas.Action_Formula[1150].Formula = AllFormulas.Action_Formula[1149].Formula


AllFormulas.Action_Formula[1151] = AllFormulas.Action_Formula[1151] or {  }
AllFormulas.Action_Formula[1151].Formula = function(a, d, e)
    local ret = nil
    ret = a:FightProp():GetParamGrade() * 0.5
    return ret
end


AllFormulas.Action_Formula[1152] = AllFormulas.Action_Formula[1152] or {  }
AllFormulas.Action_Formula[1152].Formula = function(a, d, e)
    local ret = nil
    ret = floor(e[1] ^ 2 * ((e[3] + 29) ^ 1.5 + (e[4] + 29) ^ 1.5 + (e[5] + 29) ^ 1.5 + ((e[3] + 29) * (e[4] + 29) * (e[5] + 29)) ^ 0.5) * max(0.01, min(1, (10000 - e[2]) / 10000)))
    return ret
end


AllFormulas.Action_Formula[1153] = AllFormulas.Action_Formula[1153] or {  }
AllFormulas.Action_Formula[1153].Formula = function(a, d, e)
    local ret = nil
    if e[2] < 20 then
        ret = 0
    else
        ret = floor((0.7 + 0.3 * e[1]) * e[2] + 100)
    end

    return ret
end


AllFormulas.Action_Formula[1154] = AllFormulas.Action_Formula[1154] or {  }
AllFormulas.Action_Formula[1154].Formula = function(a, d, e)
    local ret = nil
    ret = 1000 + 30 * e[1] + 2 * e[1] * e[1]
    return ret
end


AllFormulas.Action_Formula[1155] = AllFormulas.Action_Formula[1155] or {  }
AllFormulas.Action_Formula[1155].Formula = function(a, d, e)
    local ret = nil
    ret = floor(1600 + 48 * e[1] + 3.2 * e[1] * e[1])
    return ret
end


AllFormulas.Action_Formula[1156] = AllFormulas.Action_Formula[1156] or {  }
AllFormulas.Action_Formula[1156].Formula = function(a, d, e)
    local ret = nil
    ret = e[1]
    return ret
end


AllFormulas.Action_Formula[1157] = AllFormulas.Action_Formula[1157] or {  }
AllFormulas.Action_Formula[1157].Formula = function(a, d, e)
    local ret = nil
                            if e[1] == 0 then
        ret = 0
    elseif e[1] > 0 and e[1] <= 20 then
        ret = 0.5
    elseif e[1] > 20 and e[1] <= 40 then
        ret = 0.6
    elseif e[1] > 40 and e[1] <= 60 then
        ret = 0.7
    elseif e[1] > 60 and e[1] <= 80 then
        ret = 0.8
    elseif e[1] > 80 and e[1] <= 100 then
        ret = 0.9
    elseif e[1] > 100 then
        ret = 1
    end

    return ret
end


AllFormulas.Action_Formula[1169] = AllFormulas.Action_Formula[1169] or {  }
AllFormulas.Action_Formula[1169].Formula = function(a, d, e)
    local ret = nil
    ret = max(1, floor(0.9 * e[1] / e[2] * 100) / 100)
    return ret
end


AllFormulas.Action_Formula[1170] = AllFormulas.Action_Formula[1170] or {  }
AllFormulas.Action_Formula[1170].Formula = function(a, d, e)
    local ret = nil
    ret = floor((10 * e[1])) / 100
    return ret
end


AllFormulas.Action_Formula[1171] = AllFormulas.Action_Formula[1171] or {  }
AllFormulas.Action_Formula[1171].Formula = AllFormulas.Action_Formula[1170].Formula


AllFormulas.Action_Formula[1172] = AllFormulas.Action_Formula[1172] or {  }
AllFormulas.Action_Formula[1172].Formula = function(a, d, e)
    local ret = nil
    ret = floor((4 * e[1])) / 100
    return ret
end


AllFormulas.Action_Formula[1173] = AllFormulas.Action_Formula[1173] or {  }
AllFormulas.Action_Formula[1173].Formula = function(a, d, e)
    local ret = nil
    ret = floor((2 * e[1])) / 100
    return ret
end


AllFormulas.Action_Formula[1174] = AllFormulas.Action_Formula[1174] or {  }
AllFormulas.Action_Formula[1174].Formula = function(a, d, e)
    local ret = nil
    ret = floor(((e[1] + 10) * (e[2] + 10) * (e[3] + 10)) ^ 0.5) / 100
    return ret
end


AllFormulas.Action_Formula[1176] = AllFormulas.Action_Formula[1176] or {  }
AllFormulas.Action_Formula[1176].Formula = function(a, d, e)
    local ret = nil
                if e[1] < 1658937600 then
        ret = min(2100, ((e[1] - 1658332800) / 86400) * 300)
    elseif e[1] < 1659542400 then
        ret = 2100 + min(2800, ((e[1] - 1658937600) / 86400) * 400)
    elseif e[1] < 1660147200 then
        ret = 4900 + min(2100, ((e[1] - 1659542400) / 86400) * 300)
    elseif e[1] < 1660752000 then
        ret = 7000 + min(3500, ((e[1] - 1660147200) / 86400) * 500)
    else
        ret = 13300 + min(2100, ((e[1] - 1660752000) / 86400) * 300)
    end

    return ret
end


AllFormulas.Action_Formula[1177] = AllFormulas.Action_Formula[1177] or {  }
AllFormulas.Action_Formula[1177].Formula = function(a, d, e)
    local ret = nil
    ret = floor(60 + 5 * (e[1] - e[2] * 2) / (abs(5 * (e[1] - e[2] * 2)) * 0.016 + 1))
    return ret
end


AllFormulas.Action_Formula[1178] = AllFormulas.Action_Formula[1178] or {  }
AllFormulas.Action_Formula[1178].Formula = function(a, d, e)
    local ret = nil
    ret = 5 * e[1]
    return ret
end


AllFormulas.Action_Formula[1192] = AllFormulas.Action_Formula[1192] or {  }
AllFormulas.Action_Formula[1192].Formula = function(a, d, e)
    local ret = nil
    ret = e[1] * 0.01 + 0.3
    return ret
end


AllFormulas.Action_Formula[1193] = AllFormulas.Action_Formula[1193] or {  }
AllFormulas.Action_Formula[1193].Formula = AllFormulas.Action_Formula[1192].Formula


AllFormulas.Action_Formula[1194] = AllFormulas.Action_Formula[1194] or {  }
AllFormulas.Action_Formula[1194].Formula = AllFormulas.Action_Formula[1192].Formula


AllFormulas.Action_Formula[1195] = AllFormulas.Action_Formula[1195] or {  }
AllFormulas.Action_Formula[1195].Formula = AllFormulas.Action_Formula[1192].Formula


AllFormulas.Action_Formula[1196] = AllFormulas.Action_Formula[1196] or {  }
AllFormulas.Action_Formula[1196].Formula = AllFormulas.Action_Formula[1192].Formula


AllFormulas.Action_Formula[1197] = AllFormulas.Action_Formula[1197] or {  }
AllFormulas.Action_Formula[1197].Formula = AllFormulas.Action_Formula[1192].Formula


AllFormulas.Action_Formula[1198] = AllFormulas.Action_Formula[1198] or {  }
AllFormulas.Action_Formula[1198].Formula = AllFormulas.Action_Formula[1192].Formula


AllFormulas.Action_Formula[1199] = AllFormulas.Action_Formula[1199] or {  }
AllFormulas.Action_Formula[1199].Formula = AllFormulas.Action_Formula[1192].Formula


AllFormulas.Action_Formula[1200] = AllFormulas.Action_Formula[1200] or {  }
AllFormulas.Action_Formula[1200].Formula = AllFormulas.Action_Formula[1192].Formula


AllFormulas.Action_Formula[1204] = AllFormulas.Action_Formula[1204] or {  }
AllFormulas.Action_Formula[1204].Formula = function(a, d, e)
    local ret = nil
    ret = 1
    local ret1 = 0
    local ret2 = 0
    local ret3 = 0
    local ret4 = 0
    if e[1][2] < e[2][1] or e[1][2] > e[2][2] then
        ret1 = 0
    else
        ret1 = 5
    end

    if e[1][1] == 1 then
        ret2 = e[2][3]
    else
        ret2 = 10 - e[2][3]
    end

    local t1 = {  }
    for i = 6, 0, -1 do
        t1[#t1 + 1] = math.floor(e[2][4] / 2 ^ i)
        e[2][4] = e[2][4] % 2 ^ i
    end

    if e[1][17] == 0 then
        if t1[e[1][17] + 7] == 1 then
            ret3 = 1
        else
            ret = 0
        end

    else
        if t1[e[1][17]] == 1 then
            ret3 = 1
        else
            ret = 0
        end

    end

    local t2 = {  }
    for i = 4, 0, -1 do
        t2[#t2 + 1] = math.floor(e[2][5] / 2 ^ i)
        e[2][5] = e[2][5] % 2 ^ i
    end

    if t2[e[1][17]] == 1 then
        ret4 = 1
    else
        ret = 0
    end

        if e[1][16] == 0 and e[2][6] == 1 then
        ret = 0
    elseif e[1][16] == 1 and e[2][6] == 2 then
        ret = 0
    end

    if e[1][20] == 0 and e[2][8] == 1 then
        ret = 0
    end

    if e[1][10] == 1 and e[2][7] == 1 then
        ret = 0
    end

    ret = (ret1 + ret2) * ret
    return ret
end


AllFormulas.Action_Formula[1208] = AllFormulas.Action_Formula[1208] or {  }
AllFormulas.Action_Formula[1208].Formula = function(a, d, e)
    local ret = nil
    ret = min(1, e[2] / (e[1] + max(0, min(1, e[1] - e[2] - 7 - max(0, e[1] - 149) * max(0, 151 - e[1]) * 13)) * e[1] * 0.12 + max(0, min(1, e[1] - e[2] - 7)) * max(0, e[1] - 136) * max(0, 138 - e[1]) * 14 + max(0, min(1, e[1] - e[2] - 20)) * max(0, e[1] - 149) * max(0, 151 - e[1]) * 11))
    return ret
end


AllFormulas.Action_Formula[1210] = AllFormulas.Action_Formula[1210] or {  }
AllFormulas.Action_Formula[1210].Formula = function(a, d, e)
    local ret = nil
    local base = 0.6 * e[2] ^ 2 - 32 * e[2] + 1280
                if e[1] == 18007720 then
        base = base * 0.6
    elseif e[1] == 18007721 or e[1] == 18007724 then
        base = base * 0.7
    elseif e[1] == 18007722 then
        base = base * 0.85
    elseif e[1] == 18007723 then
        base = base * 0.9
    end

    if e[1] == 18007720 or e[1] == 18007721 or e[1] == 18007723 then
        base = base * 0.3
    end

    ret = base
    return ret
end


AllFormulas.Action_Formula[1211] = AllFormulas.Action_Formula[1211] or {  }
AllFormulas.Action_Formula[1211].Formula = function(a, d, e)
    local ret = nil
    local base = 0.9 * e[2] ^ 2 - 48 * e[2] + 1920
                if e[1] == 18007720 then
        base = base * 0.6
    elseif e[1] == 18007721 or e[1] == 18007724 then
        base = base * 0.7
    elseif e[1] == 18007722 then
        base = base * 0.85
    elseif e[1] == 18007723 then
        base = base * 0.9
    end

    if e[1] == 18007720 or e[1] == 18007721 or e[1] == 18007723 then
        base = base * 0.3
    end

    ret = base
    return ret
end


AllFormulas.Action_Formula[1212] = AllFormulas.Action_Formula[1212] or {  }
AllFormulas.Action_Formula[1212].Formula = function(a, d, e)
    local ret = nil
    local base = 1.5 * e[2] ^ 2 - 48 * e[2] + 2000
        if e[1] == 18007720 then
        base = base * 1.2
    elseif e[1] == 18007724 then
        base = base * 1.5
    end

    ret = base
    return ret
end


AllFormulas.Action_Formula[1213] = AllFormulas.Action_Formula[1213] or {  }
AllFormulas.Action_Formula[1213].Formula = function(a, d, e)
    local ret = nil
    local base = 50 * e[2] - 1000
        if e[1] == 18007724 then
        base = base * 1.5
    elseif e[1] == 18007721 or e[1] == 18007723 or e[1] == 18007720 or e[1] == 18007722 then
        base = base * 1.2
    end

    ret = base
    return ret
end


AllFormulas.Action_Formula[1214] = AllFormulas.Action_Formula[1214] or {  }
AllFormulas.Action_Formula[1214].Formula = function(a, d, e)
    local ret = nil
    local base = 0.4 * e[2] + 10
    if e[1] == 18007720 or e[1] == 18007724 then
        base = base * 0.5
    end

    if e[1] == 18007720 or e[1] == 18007721 or e[1] == 18007723 then
        base = base * 0.3
    end

    ret = base
    return ret
end


AllFormulas.Action_Formula[1215] = AllFormulas.Action_Formula[1215] or {  }
AllFormulas.Action_Formula[1215].Formula = function(a, d, e)
    local ret = nil
    ret = 0
    return ret
end


AllFormulas.Action_Formula[1216] = AllFormulas.Action_Formula[1216] or {  }
AllFormulas.Action_Formula[1216].Formula = AllFormulas.Action_Formula[1215].Formula


AllFormulas.Action_Formula[1217] = AllFormulas.Action_Formula[1217] or {  }
AllFormulas.Action_Formula[1217].Formula = function(a, d, e)
    local ret = nil
    if e[1] < 20 then
        ret = 0
    else
        ret = ceil(0.4 * e[1])
    end

    return ret
end


AllFormulas.Action_Formula[1218] = AllFormulas.Action_Formula[1218] or {  }
AllFormulas.Action_Formula[1218].Formula = function(a, d, e)
    local ret = nil
    ret = floor(max(50, 0.2 * e[1]))
    return ret
end


AllFormulas.Action_Formula[1222] = AllFormulas.Action_Formula[1222] or {  }
AllFormulas.Action_Formula[1222].Formula = function(a, d, e)
    local ret = nil
    ret = floor(e[1] * e[2] / 100)
    return ret
end


AllFormulas.Action_Formula[1227] = AllFormulas.Action_Formula[1227] or {  }
AllFormulas.Action_Formula[1227].Formula = function(a, d, e)
    local ret = nil
    ret = 10000 * e[1]
    return ret
end


AllFormulas.Action_Formula[1233] = AllFormulas.Action_Formula[1233] or {  }
AllFormulas.Action_Formula[1233].Formula = function(a, d, e)
    local ret = nil
    local base = 0.6 * e[2] ^ 2 - 32 * e[2] + 1280
                if e[1] == 18007720 then
        base = base * 0.6
    elseif e[1] == 18007721 or e[1] == 18007724 then
        base = base * 0.7
    elseif e[1] == 18007722 then
        base = base * 0.85
    elseif e[1] == 18007723 then
        base = base * 0.9
    end

    if e[1] == 18007719 or e[1] == 18007722 or e[1] == 18007724 then
        base = base * 0.3
    end

    ret = base
    return ret
end


AllFormulas.Action_Formula[1234] = AllFormulas.Action_Formula[1234] or {  }
AllFormulas.Action_Formula[1234].Formula = function(a, d, e)
    local ret = nil
    local base = 0.9 * e[2] ^ 2 - 48 * e[2] + 1920
                if e[1] == 18007720 then
        base = base * 0.6
    elseif e[1] == 18007721 or e[1] == 18007724 then
        base = base * 0.7
    elseif e[1] == 18007722 then
        base = base * 0.85
    elseif e[1] == 18007723 then
        base = base * 0.9
    end

    if e[1] == 18007719 or e[1] == 18007722 or e[1] == 18007724 then
        base = base * 0.3
    end

    ret = base
    return ret
end


AllFormulas.Action_Formula[1235] = AllFormulas.Action_Formula[1235] or {  }
AllFormulas.Action_Formula[1235].Formula = function(a, d, e)
    local ret = nil
    local base = 3 * e[2] + 50
    if e[1] == 18007721 then
        base = base * 1.5
    end

    if e[1] == 18007720 or e[1] == 18007721 or e[1] == 18007723 then
        base = base * 0.3
    end

    ret = base
    return ret
end


AllFormulas.Action_Formula[1236] = AllFormulas.Action_Formula[1236] or {  }
AllFormulas.Action_Formula[1236].Formula = function(a, d, e)
    local ret = nil
    local base = 3 * e[2] + 50
    if e[1] == 18007721 then
        base = base * 1.5
    end

    if e[1] == 18007719 or e[1] == 18007722 or e[1] == 18007724 then
        base = base * 0.3
    end

    ret = base
    return ret
end


AllFormulas.Action_Formula[1237] = AllFormulas.Action_Formula[1237] or {  }
AllFormulas.Action_Formula[1237].Formula = function(a, d, e)
    local ret = nil
    local base = 0.4 * e[2] + 10
    if e[1] == 18007720 or e[1] == 18007724 then
        base = base * 0.5
    end

    if e[1] == 18007719 or e[1] == 18007722 or e[1] == 18007724 then
        base = base * 0.3
    end

    ret = base
    return ret
end


AllFormulas.Action_Formula[1238] = AllFormulas.Action_Formula[1238] or {  }
AllFormulas.Action_Formula[1238].Formula = function(a, d, e)
    local ret = nil
    ret = e[1] - 20
    return ret
end


AllFormulas.Action_Formula[1240] = AllFormulas.Action_Formula[1240] or {  }
AllFormulas.Action_Formula[1240].Formula = function(a, d, e)
    local ret = nil
    local base = e[1]
    if e[2] >= 5 then
        base = base
    end

    if e[2] < 5 then
        base = base * 0.9
    end

    ret = floor(base)
    return ret
end


AllFormulas.Action_Formula[1242] = AllFormulas.Action_Formula[1242] or {  }
AllFormulas.Action_Formula[1242].Formula = function(a, d, e)
    local ret = nil
    ret = {  }
    ret[1] = 100 * (0.02 + (e[1] - 1) * 0.005)
    ret[2] = 100 * (0.08 + (e[1] - 1) * 0.02)
    ret[3] = 100 * (0.12 + (e[1] - 1) * 0.03)
    return ret
end


AllFormulas.Action_Formula[1243] = AllFormulas.Action_Formula[1243] or {  }
AllFormulas.Action_Formula[1243].Formula = function(a, d, e)
    local ret = nil
    ret = {  }
    ret[1] = 100 * (0.03 + (e[1] - 1) * 0.0075)
    ret[2] = 100 * (0.12 + (e[1] - 1) * 0.03)
    ret[3] = 100 * (0.18 + (e[1] - 1) * 0.045)
    return ret
end


AllFormulas.Action_Formula[1244] = AllFormulas.Action_Formula[1244] or {  }
AllFormulas.Action_Formula[1244].Formula = function(a, d, e)
    local ret = nil
    ret = 0.2 * (e[1] - 1) + 0.8
    return ret
end


AllFormulas.Action_Formula[1245] = AllFormulas.Action_Formula[1245] or {  }
AllFormulas.Action_Formula[1245].Formula = function(a, d, e)
    local ret = nil
    ret = (floor(((e[1] + 20) ^ 3 + (e[2] + 20) ^ 3 + (e[3] + 20) ^ 3 + 3 * ((e[1] + 20) * (e[2] + 20) * (e[3] + 20)) ^ (4 / 3)) * (1 - min(e[4], 1500) ^ 0.1 / 3000 ^ 0.1) * (1 + 0.005 * e[5] ^ 2) ^ e[5] / 200 + e[5] * 500)) * min(1, e[5])
    return ret
end


AllFormulas.Action_Formula[1246] = AllFormulas.Action_Formula[1246] or {  }
AllFormulas.Action_Formula[1246].Formula = function(a, d, e)
    local ret = nil
    local levelBase = 1
        if e[4] == 4 then
        levelBase = 3
    elseif e[4] == 9 then
        levelBase = 5
    end

    ret = floor(levelBase * (e[1] + e[2] + e[3] + 150) ^ 2 / 1000)
    return ret
end


AllFormulas.Action_Formula[1247] = AllFormulas.Action_Formula[1247] or {  }
AllFormulas.Action_Formula[1247].Formula = function(a, d, e)
    local ret = nil
    ret = min(e[1], 10) * 60480 + min(max(e[1] - 10, 0), 50) * 15120 + min(max(e[1] - 50, 0), 120) * 8640 + min(max(e[1] - 120, 0), 220) * 6048 + min(max(e[1] - 220, 0), 360) * 4320 + min(max(e[1] - 360, 0), 560) * 3024 + min(max(e[1] - 560, 0), 1200) * 945 + min(max(e[1] - 1200, 0), 9999) * 68
    return ret
end


AllFormulas.Action_Formula[1248] = AllFormulas.Action_Formula[1248] or {  }
AllFormulas.Action_Formula[1248].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(2.695 * lv * lv + (0.77 * lv) + (-8559.9))
    return ret
end


AllFormulas.Action_Formula[1249] = AllFormulas.Action_Formula[1249] or {  }
AllFormulas.Action_Formula[1249].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(3.85 * lv * lv + (1.1 * lv) + (-12657))
    return ret
end


AllFormulas.Action_Formula[1250] = AllFormulas.Action_Formula[1250] or {  }
AllFormulas.Action_Formula[1250].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(64 * lv * lv + (-7040 * lv) + (195998.4))
    return ret
end


AllFormulas.Action_Formula[1251] = AllFormulas.Action_Formula[1251] or {  }
AllFormulas.Action_Formula[1251].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(0 * lv * lv + (18 * lv) + (-600))
    return ret
end


AllFormulas.Action_Formula[1252] = AllFormulas.Action_Formula[1252] or {  }
AllFormulas.Action_Formula[1252].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(0 * lv * lv + (3 * lv) + (-180))
    return ret
end


AllFormulas.Action_Formula[1253] = AllFormulas.Action_Formula[1253] or {  }
AllFormulas.Action_Formula[1253].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(0 * lv * lv + (238.85 * lv) + (-14166.1))
    return ret
end


AllFormulas.Action_Formula[1254] = AllFormulas.Action_Formula[1254] or {  }
AllFormulas.Action_Formula[1254].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(0 * lv * lv + (18 * lv) + (-900))
    return ret
end


AllFormulas.Action_Formula[1255] = AllFormulas.Action_Formula[1255] or {  }
AllFormulas.Action_Formula[1255].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(2.156 * lv * lv + (0.616 * lv) + (-6647.92))
    return ret
end


AllFormulas.Action_Formula[1256] = AllFormulas.Action_Formula[1256] or {  }
AllFormulas.Action_Formula[1256].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(3.08 * lv * lv + (0.88 * lv) + (-9925.6))
    return ret
end


AllFormulas.Action_Formula[1257] = AllFormulas.Action_Formula[1257] or {  }
AllFormulas.Action_Formula[1257].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(80 * lv * lv + (-8800 * lv) + (244998))
    return ret
end


AllFormulas.Action_Formula[1258] = AllFormulas.Action_Formula[1258] or {  }
AllFormulas.Action_Formula[1258].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(0 * lv * lv + (16.2 * lv) + (-540))
    return ret
end


AllFormulas.Action_Formula[1259] = AllFormulas.Action_Formula[1259] or {  }
AllFormulas.Action_Formula[1259].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(0 * lv * lv + (2.4 * lv) + (-144))
    return ret
end


AllFormulas.Action_Formula[1260] = AllFormulas.Action_Formula[1260] or {  }
AllFormulas.Action_Formula[1260].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(0 * lv * lv + (281 * lv) + (-16666))
    return ret
end


AllFormulas.Action_Formula[1261] = AllFormulas.Action_Formula[1261] or {  }
AllFormulas.Action_Formula[1261].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(0 * lv * lv + (18 * lv) + (-600))
    return ret
end


AllFormulas.Action_Formula[1262] = AllFormulas.Action_Formula[1262] or {  }
AllFormulas.Action_Formula[1262].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(2.695 * lv * lv + (0.77 * lv) + (-9559.9))
    return ret
end


AllFormulas.Action_Formula[1263] = AllFormulas.Action_Formula[1263] or {  }
AllFormulas.Action_Formula[1263].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(3.85 * lv * lv + (1.1 * lv) + (-13657))
    return ret
end


AllFormulas.Action_Formula[1264] = AllFormulas.Action_Formula[1264] or {  }
AllFormulas.Action_Formula[1264].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(80 * lv * lv + (-8800 * lv) + (244998))
    return ret
end


AllFormulas.Action_Formula[1265] = AllFormulas.Action_Formula[1265] or {  }
AllFormulas.Action_Formula[1265].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(0 * lv * lv + (18 * lv) + (-600))
    return ret
end


AllFormulas.Action_Formula[1266] = AllFormulas.Action_Formula[1266] or {  }
AllFormulas.Action_Formula[1266].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(0 * lv * lv + (3 * lv) + (-180))
    return ret
end


AllFormulas.Action_Formula[1267] = AllFormulas.Action_Formula[1267] or {  }
AllFormulas.Action_Formula[1267].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(0 * lv * lv + (252.9 * lv) + (-14999.4))
    return ret
end


AllFormulas.Action_Formula[1268] = AllFormulas.Action_Formula[1268] or {  }
AllFormulas.Action_Formula[1268].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(0 * lv * lv + (18 * lv) + (-600))
    return ret
end


AllFormulas.Action_Formula[1269] = AllFormulas.Action_Formula[1269] or {  }
AllFormulas.Action_Formula[1269].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(2.695 * lv * lv + (0.77 * lv) + (-9559.9))
    return ret
end


AllFormulas.Action_Formula[1270] = AllFormulas.Action_Formula[1270] or {  }
AllFormulas.Action_Formula[1270].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(3.85 * lv * lv + (1.1 * lv) + (-13657))
    return ret
end


AllFormulas.Action_Formula[1271] = AllFormulas.Action_Formula[1271] or {  }
AllFormulas.Action_Formula[1271].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(80 * lv * lv + (-8800 * lv) + (244998))
    return ret
end


AllFormulas.Action_Formula[1272] = AllFormulas.Action_Formula[1272] or {  }
AllFormulas.Action_Formula[1272].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(0 * lv * lv + (18 * lv) + (-600))
    return ret
end


AllFormulas.Action_Formula[1273] = AllFormulas.Action_Formula[1273] or {  }
AllFormulas.Action_Formula[1273].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(0 * lv * lv + (3 * lv) + (-180))
    return ret
end


AllFormulas.Action_Formula[1274] = AllFormulas.Action_Formula[1274] or {  }
AllFormulas.Action_Formula[1274].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(0 * lv * lv + (252.9 * lv) + (-14999.4))
    return ret
end


AllFormulas.Action_Formula[1275] = AllFormulas.Action_Formula[1275] or {  }
AllFormulas.Action_Formula[1275].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(0 * lv * lv + (18 * lv) + (-600))
    return ret
end


AllFormulas.Action_Formula[1276] = AllFormulas.Action_Formula[1276] or {  }
AllFormulas.Action_Formula[1276].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(2.695 * lv * lv + (0.77 * lv) + (-9559.9))
    return ret
end


AllFormulas.Action_Formula[1277] = AllFormulas.Action_Formula[1277] or {  }
AllFormulas.Action_Formula[1277].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(3.85 * lv * lv + (1.1 * lv) + (-12657))
    return ret
end


AllFormulas.Action_Formula[1278] = AllFormulas.Action_Formula[1278] or {  }
AllFormulas.Action_Formula[1278].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(80 * lv * lv + (-8800 * lv) + (244998))
    return ret
end


AllFormulas.Action_Formula[1279] = AllFormulas.Action_Formula[1279] or {  }
AllFormulas.Action_Formula[1279].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(0 * lv * lv + (281 * lv) + (-16666))
    return ret
end


AllFormulas.Action_Formula[1280] = AllFormulas.Action_Formula[1280] or {  }
AllFormulas.Action_Formula[1280].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(2.29075 * lv * lv + (0.6545 * lv) + (-8125.915))
    return ret
end


AllFormulas.Action_Formula[1281] = AllFormulas.Action_Formula[1281] or {  }
AllFormulas.Action_Formula[1281].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1] + 59
    ret = floor(3.2725 * lv * lv + (0.935 * lv) + (-11608.45))
    return ret
end


AllFormulas.Action_Formula[1282] = AllFormulas.Action_Formula[1282] or {  }
AllFormulas.Action_Formula[1282].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpAttMin2())
    else
        ret = floor(-3.6 * e[1] * e[1] + 958.8 * e[1] + 237.6 + a:FightProp():GetParamAdjpAttMin2())
    end

    return ret
end


AllFormulas.Action_Formula[1283] = AllFormulas.Action_Formula[1283] or {  }
AllFormulas.Action_Formula[1283].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpAttMax2())
    else
        ret = floor(-7.2 * e[1] * e[1] + 1678.8 * e[1] + 357.6 + a:FightProp():GetParamAdjpAttMax2())
    end

    return ret
end


AllFormulas.Action_Formula[1284] = AllFormulas.Action_Formula[1284] or {  }
AllFormulas.Action_Formula[1284].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjHpFull2())
    else
        ret = floor(-2.4 * e[1] * e[1] + 1559.2 * e[1] + 2240 + a:FightProp():GetParamAdjHpFull2())
    end

    return ret
end


AllFormulas.Action_Formula[1285] = AllFormulas.Action_Formula[1285] or {  }
AllFormulas.Action_Formula[1285].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpHit2())
    else
        ret = floor(-0.14 * e[1] * e[1] + 38 * e[1] + 70 + a:FightProp():GetParamAdjpHit2())
    end

    return ret
end


AllFormulas.Action_Formula[1286] = AllFormulas.Action_Formula[1286] or {  }
AllFormulas.Action_Formula[1286].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpFatal2())
    else
        ret = floor(0 * e[1] * e[1] + 4 * e[1] + 25 + a:FightProp():GetParamAdjpFatal2())
    end

    return ret
end


AllFormulas.Action_Formula[1287] = AllFormulas.Action_Formula[1287] or {  }
AllFormulas.Action_Formula[1287].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpDef2())
    else
        ret = floor(-2.7 * e[1] * e[1] + 674.1 * e[1] + 88.2 + a:FightProp():GetParamAdjpDef2())
    end

    return ret
end


AllFormulas.Action_Formula[1288] = AllFormulas.Action_Formula[1288] or {  }
AllFormulas.Action_Formula[1288].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmDef2())
    else
        ret = floor(-2.7 * e[1] * e[1] + 674.1 * e[1] + 88.2 + a:FightProp():GetParamAdjmDef2())
    end

    return ret
end


AllFormulas.Action_Formula[1289] = AllFormulas.Action_Formula[1289] or {  }
AllFormulas.Action_Formula[1289].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpMiss2())
    else
        ret = floor(-0.168 * e[1] * e[1] + 43.2 * e[1] + 72 + a:FightProp():GetParamAdjpMiss2())
    end

    return ret
end


AllFormulas.Action_Formula[1290] = AllFormulas.Action_Formula[1290] or {  }
AllFormulas.Action_Formula[1290].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmMiss2())
    else
        ret = floor(-0.168 * e[1] * e[1] + 43.2 * e[1] + 72 + a:FightProp():GetParamAdjmMiss2())
    end

    return ret
end


AllFormulas.Action_Formula[1291] = AllFormulas.Action_Formula[1291] or {  }
AllFormulas.Action_Formula[1291].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmAttMin2())
    else
        ret = floor(-3 * e[1] * e[1] + 799 * e[1] + 198 + a:FightProp():GetParamAdjmAttMin2())
    end

    return ret
end


AllFormulas.Action_Formula[1292] = AllFormulas.Action_Formula[1292] or {  }
AllFormulas.Action_Formula[1292].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmAttMax2())
    else
        ret = floor(-6 * e[1] * e[1] + 1399 * e[1] + 298 + a:FightProp():GetParamAdjmAttMax2())
    end

    return ret
end


AllFormulas.Action_Formula[1293] = AllFormulas.Action_Formula[1293] or {  }
AllFormulas.Action_Formula[1293].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjHpFull2())
    else
        ret = floor(-3 * e[1] * e[1] + 1949 * e[1] + 2800 + a:FightProp():GetParamAdjHpFull2())
    end

    return ret
end


AllFormulas.Action_Formula[1294] = AllFormulas.Action_Formula[1294] or {  }
AllFormulas.Action_Formula[1294].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmHit2())
    else
        ret = floor(-0.14 * e[1] * e[1] + 38 * e[1] + 70 + a:FightProp():GetParamAdjmHit2())
    end

    return ret
end


AllFormulas.Action_Formula[1295] = AllFormulas.Action_Formula[1295] or {  }
AllFormulas.Action_Formula[1295].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmFatal2())
    else
        ret = floor(0 * e[1] * e[1] + 4 * e[1] + 25 + a:FightProp():GetParamAdjmFatal2())
    end

    return ret
end


AllFormulas.Action_Formula[1296] = AllFormulas.Action_Formula[1296] or {  }
AllFormulas.Action_Formula[1296].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpDef2())
    else
        ret = floor(-3 * e[1] * e[1] + 749 * e[1] + 98 + a:FightProp():GetParamAdjpDef2())
    end

    return ret
end


AllFormulas.Action_Formula[1297] = AllFormulas.Action_Formula[1297] or {  }
AllFormulas.Action_Formula[1297].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmDef2())
    else
        ret = floor(-3 * e[1] * e[1] + 749 * e[1] + 98 + a:FightProp():GetParamAdjmDef2())
    end

    return ret
end


AllFormulas.Action_Formula[1298] = AllFormulas.Action_Formula[1298] or {  }
AllFormulas.Action_Formula[1298].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpMiss2())
    else
        ret = floor(-0.168 * e[1] * e[1] + 43.2 * e[1] + 72 + a:FightProp():GetParamAdjpMiss2())
    end

    return ret
end


AllFormulas.Action_Formula[1299] = AllFormulas.Action_Formula[1299] or {  }
AllFormulas.Action_Formula[1299].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmMiss2())
    else
        ret = floor(-0.168 * e[1] * e[1] + 43.2 * e[1] + 72 + a:FightProp():GetParamAdjmMiss2())
    end

    return ret
end


AllFormulas.Action_Formula[1300] = AllFormulas.Action_Formula[1300] or {  }
AllFormulas.Action_Formula[1300].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpAttMin2())
    else
        ret = floor(-3.3 * e[1] * e[1] + 878.9 * e[1] + 217.8 + a:FightProp():GetParamAdjpAttMin2())
    end

    return ret
end


AllFormulas.Action_Formula[1301] = AllFormulas.Action_Formula[1301] or {  }
AllFormulas.Action_Formula[1301].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpAttMax2())
    else
        ret = floor(-6.6 * e[1] * e[1] + 1538.9 * e[1] + 327.8 + a:FightProp():GetParamAdjpAttMax2())
    end

    return ret
end


AllFormulas.Action_Formula[1302] = AllFormulas.Action_Formula[1302] or {  }
AllFormulas.Action_Formula[1302].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjHpFull2())
    else
        ret = floor(-3.3 * e[1] * e[1] + 2143.9 * e[1] + 3080 + a:FightProp():GetParamAdjHpFull2())
    end

    return ret
end


AllFormulas.Action_Formula[1303] = AllFormulas.Action_Formula[1303] or {  }
AllFormulas.Action_Formula[1303].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpHit2())
    else
        ret = floor(-0.14 * e[1] * e[1] + 38 * e[1] + 70 + a:FightProp():GetParamAdjpHit2())
    end

    return ret
end


AllFormulas.Action_Formula[1304] = AllFormulas.Action_Formula[1304] or {  }
AllFormulas.Action_Formula[1304].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpFatal2())
    else
        ret = floor(0 * e[1] * e[1] + 4 * e[1] + 25 + a:FightProp():GetParamAdjpFatal2())
    end

    return ret
end


AllFormulas.Action_Formula[1305] = AllFormulas.Action_Formula[1305] or {  }
AllFormulas.Action_Formula[1305].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpDef2())
    else
        ret = floor(-3.3 * e[1] * e[1] + 823.9 * e[1] + 107.8 + a:FightProp():GetParamAdjpDef2())
    end

    return ret
end


AllFormulas.Action_Formula[1306] = AllFormulas.Action_Formula[1306] or {  }
AllFormulas.Action_Formula[1306].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmDef2())
    else
        ret = floor(-3.3 * e[1] * e[1] + 823.9 * e[1] + 107.8 + a:FightProp():GetParamAdjmDef2())
    end

    return ret
end


AllFormulas.Action_Formula[1307] = AllFormulas.Action_Formula[1307] or {  }
AllFormulas.Action_Formula[1307].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpMiss2())
    else
        ret = floor(-0.14 * e[1] * e[1] + 36 * e[1] + 60 + a:FightProp():GetParamAdjpMiss2())
    end

    return ret
end


AllFormulas.Action_Formula[1308] = AllFormulas.Action_Formula[1308] or {  }
AllFormulas.Action_Formula[1308].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmMiss2())
    else
        ret = floor(-0.14 * e[1] * e[1] + 36 * e[1] + 60 + a:FightProp():GetParamAdjmMiss2())
    end

    return ret
end


AllFormulas.Action_Formula[1309] = AllFormulas.Action_Formula[1309] or {  }
AllFormulas.Action_Formula[1309].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmAttMin2())
    else
        ret = floor(-2.7 * e[1] * e[1] + 719.1 * e[1] + 178.2 + a:FightProp():GetParamAdjmAttMin2())
    end

    return ret
end


AllFormulas.Action_Formula[1310] = AllFormulas.Action_Formula[1310] or {  }
AllFormulas.Action_Formula[1310].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmAttMax2())
    else
        ret = floor(-5.4 * e[1] * e[1] + 1259.1 * e[1] + 268.2 + a:FightProp():GetParamAdjmAttMax2())
    end

    return ret
end


AllFormulas.Action_Formula[1311] = AllFormulas.Action_Formula[1311] or {  }
AllFormulas.Action_Formula[1311].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjHpFull2())
    else
        ret = floor(-3 * e[1] * e[1] + 1949 * e[1] + 2800 + a:FightProp():GetParamAdjHpFull2())
    end

    return ret
end


AllFormulas.Action_Formula[1312] = AllFormulas.Action_Formula[1312] or {  }
AllFormulas.Action_Formula[1312].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmHit2())
    else
        ret = floor(-0.14 * e[1] * e[1] + 38 * e[1] + 70 + a:FightProp():GetParamAdjmHit2())
    end

    return ret
end


AllFormulas.Action_Formula[1313] = AllFormulas.Action_Formula[1313] or {  }
AllFormulas.Action_Formula[1313].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmFatal2())
    else
        ret = floor(0 * e[1] * e[1] + 4 * e[1] + 25 + a:FightProp():GetParamAdjmFatal2())
    end

    return ret
end


AllFormulas.Action_Formula[1314] = AllFormulas.Action_Formula[1314] or {  }
AllFormulas.Action_Formula[1314].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpDef2())
    else
        ret = floor(-3 * e[1] * e[1] + 749 * e[1] + 98 + a:FightProp():GetParamAdjpDef2())
    end

    return ret
end


AllFormulas.Action_Formula[1315] = AllFormulas.Action_Formula[1315] or {  }
AllFormulas.Action_Formula[1315].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmDef2())
    else
        ret = floor(-3 * e[1] * e[1] + 749 * e[1] + 98 + a:FightProp():GetParamAdjmDef2())
    end

    return ret
end


AllFormulas.Action_Formula[1316] = AllFormulas.Action_Formula[1316] or {  }
AllFormulas.Action_Formula[1316].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpMiss2())
    else
        ret = floor(-0.14 * e[1] * e[1] + 36 * e[1] + 60 + a:FightProp():GetParamAdjpMiss2())
    end

    return ret
end


AllFormulas.Action_Formula[1317] = AllFormulas.Action_Formula[1317] or {  }
AllFormulas.Action_Formula[1317].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmMiss2())
    else
        ret = floor(-0.14 * e[1] * e[1] + 36 * e[1] + 60 + a:FightProp():GetParamAdjmMiss2())
    end

    return ret
end


AllFormulas.Action_Formula[1318] = AllFormulas.Action_Formula[1318] or {  }
AllFormulas.Action_Formula[1318].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpAttMin2())
    else
        ret = floor(-3.3 * e[1] * e[1] + 878.9 * e[1] + 217.8 + a:FightProp():GetParamAdjpAttMin2())
    end

    return ret
end


AllFormulas.Action_Formula[1319] = AllFormulas.Action_Formula[1319] or {  }
AllFormulas.Action_Formula[1319].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpAttMax2())
    else
        ret = floor(-6.6 * e[1] * e[1] + 1538.9 * e[1] + 327.8 + a:FightProp():GetParamAdjpAttMax2())
    end

    return ret
end


AllFormulas.Action_Formula[1320] = AllFormulas.Action_Formula[1320] or {  }
AllFormulas.Action_Formula[1320].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjHpFull2())
    else
        ret = floor(-3 * e[1] * e[1] + 1949 * e[1] + 2800 + a:FightProp():GetParamAdjHpFull2())
    end

    return ret
end


AllFormulas.Action_Formula[1321] = AllFormulas.Action_Formula[1321] or {  }
AllFormulas.Action_Formula[1321].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpHit2())
    else
        ret = floor(-0.14 * e[1] * e[1] + 38 * e[1] + 70 + a:FightProp():GetParamAdjpHit2())
    end

    return ret
end


AllFormulas.Action_Formula[1322] = AllFormulas.Action_Formula[1322] or {  }
AllFormulas.Action_Formula[1322].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpFatal2())
    else
        ret = floor(0 * e[1] * e[1] + 4 * e[1] + 25 + a:FightProp():GetParamAdjpFatal2())
    end

    return ret
end


AllFormulas.Action_Formula[1323] = AllFormulas.Action_Formula[1323] or {  }
AllFormulas.Action_Formula[1323].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpDef2())
    else
        ret = floor(-3 * e[1] * e[1] + 749 * e[1] + 98 + a:FightProp():GetParamAdjpDef2())
    end

    return ret
end


AllFormulas.Action_Formula[1324] = AllFormulas.Action_Formula[1324] or {  }
AllFormulas.Action_Formula[1324].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmDef2())
    else
        ret = floor(-3 * e[1] * e[1] + 749 * e[1] + 98 + a:FightProp():GetParamAdjmDef2())
    end

    return ret
end


AllFormulas.Action_Formula[1325] = AllFormulas.Action_Formula[1325] or {  }
AllFormulas.Action_Formula[1325].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjpMiss2())
    else
        ret = floor(-0.14 * e[1] * e[1] + 36 * e[1] + 60 + a:FightProp():GetParamAdjpMiss2())
    end

    return ret
end


AllFormulas.Action_Formula[1326] = AllFormulas.Action_Formula[1326] or {  }
AllFormulas.Action_Formula[1326].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmMiss2())
    else
        ret = floor(-0.14 * e[1] * e[1] + 36 * e[1] + 60 + a:FightProp():GetParamAdjmMiss2())
    end

    return ret
end


AllFormulas.Action_Formula[1327] = AllFormulas.Action_Formula[1327] or {  }
AllFormulas.Action_Formula[1327].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmAttMin2())
    else
        ret = floor(-2.4 * e[1] * e[1] + 639.2 * e[1] + 158.4 + a:FightProp():GetParamAdjmAttMin2())
    end

    return ret
end


AllFormulas.Action_Formula[1328] = AllFormulas.Action_Formula[1328] or {  }
AllFormulas.Action_Formula[1328].Formula = function(a, d, e)
    local ret = nil
    local lv = e[1]
    if lv == 0 then
        ret = floor(a:FightProp():GetParamAdjmAttMax2())
    else
        ret = floor(-4.8 * e[1] * e[1] + 1119.2 * e[1] + 238.4 + a:FightProp():GetParamAdjmAttMax2())
    end

    return ret
end


AllFormulas.Action_Formula[1329] = AllFormulas.Action_Formula[1329] or {  }
AllFormulas.Action_Formula[1329].Formula = function(a, d, e)
    local ret = nil
    ret = min(max(5000, 10 * e[1]), 50000)
    return ret
end


AllFormulas.Action_Formula[1331] = AllFormulas.Action_Formula[1331] or {  }
AllFormulas.Action_Formula[1331].Formula = function(a, d, e)
    local ret = nil
    local Num = e[1]
            if Num < 2 then
        ret = 0
    elseif Num <= 5 and Num >= 2 then
        ret = 1
    elseif Num > 5 and Num <= 10 then
        ret = 2
    else
        ret = 3
    end

    return ret
end


AllFormulas.Action_Formula[1332] = AllFormulas.Action_Formula[1332] or {  }
AllFormulas.Action_Formula[1332].Formula = function(a, d, e)
    local ret = nil
    ret = 0
    return ret
end


AllFormulas.Action_Formula[1333] = AllFormulas.Action_Formula[1333] or {  }
AllFormulas.Action_Formula[1333].Formula = AllFormulas.Action_Formula[1332].Formula


AllFormulas.Action_Formula[1334] = AllFormulas.Action_Formula[1334] or {  }
AllFormulas.Action_Formula[1334].Formula = function(a, d, e)
    local ret = nil
    ret = (0.1 * e[2] + 0.2 + 0.7 * min(1, floor(e[2] / 10)) + 0.9 * floor(max(0, min(100, e[2]) - 50) / 20) + max(e[1] - 11, e[2]) * 0.05 * floor(min(100, e[2]) / 20)) * 160
    return ret
end


AllFormulas.Action_Formula[1335] = AllFormulas.Action_Formula[1335] or {  }
AllFormulas.Action_Formula[1335].Formula = function(a, d, e)
    local ret = nil
    ret = 0
    return ret
end


AllFormulas.Action_Formula[1336] = AllFormulas.Action_Formula[1336] or {  }
AllFormulas.Action_Formula[1336].Formula = function(a, d, e)
    local ret = nil
    local buffLv = GetGradeByBuffCls(a, 690199)
    ret = e[1] * (1 + buffLv * 0.003)
    return ret
end


AllFormulas.Action_Formula[1337] = AllFormulas.Action_Formula[1337] or {  }
AllFormulas.Action_Formula[1337].Formula = function(a, d, e)
    local ret = nil
    local buffLv = GetGradeByBuffCls(a, 690199)
    ret = d:FightProp():GetParamGrade() * e[1] * (1 + buffLv * 0.003)
    return ret
end


AllFormulas.Action_Formula[1338] = AllFormulas.Action_Formula[1338] or {  }
AllFormulas.Action_Formula[1338].Formula = function(a, d, e)
    local ret = nil
    local buffLv = GetGradeByBuffCls(a, 690199)
    ret = d:FightProp():GetParamGrade() ^ 2 * e[1] * (1 + buffLv * 0.003)
    return ret
end


AllFormulas.Action_Formula[1339] = AllFormulas.Action_Formula[1339] or {  }
AllFormulas.Action_Formula[1339].Formula = function(a, d, e)
    local ret = nil
    local unlockLv = floor((e[1] + e[2] + e[3]) / 3)
            if e[4] <= 300 then
        unlockLv = unlockLv + 15
    elseif e[4] <= 600 then
        unlockLv = unlockLv + 10
    elseif e[4] <= 10000 then
        unlockLv = unlockLv + 5
    end

    ret = min(70, unlockLv)
    return ret
end


AllFormulas.Action_Formula[1340] = AllFormulas.Action_Formula[1340] or {  }
AllFormulas.Action_Formula[1340].Formula = function(a, d, e)
    local ret = nil
    local buffLv = GetGradeByBuffCls(a, 690199)
    ret = (e[1] + d:FightProp():GetParamGrade() * e[2] + d:FightProp():GetParamGrade() ^ 2 * e[3]) * (1 + buffLv * 0.02)
    return ret
end


AllFormulas.Action_Formula[1341] = AllFormulas.Action_Formula[1341] or {  }
AllFormulas.Action_Formula[1341].Formula = function(a, d, e)
    local ret = nil
    local baseRet = floor((0.32 * e[1] + 5) * 1)
            if e[1] == 0 then
        ret = 0
    elseif e[1] < 30 then
        ret = baseRet
    elseif e[1] < 50 then
        ret = 2 * baseRet
    else
        ret = 4 * baseRet
    end

    return ret
end


AllFormulas.Action_Formula[1342] = AllFormulas.Action_Formula[1342] or {  }
AllFormulas.Action_Formula[1342].Formula = function(a, d, e)
    local ret = nil
    local baseRet = floor((0.32 * e[1] + 5) * 2)
            if e[1] == 0 then
        ret = 0
    elseif e[1] < 30 then
        ret = baseRet
    elseif e[1] < 50 then
        ret = 2 * baseRet
    else
        ret = 4 * baseRet
    end

    return ret
end


AllFormulas.Action_Formula[1343] = AllFormulas.Action_Formula[1343] or {  }
AllFormulas.Action_Formula[1343].Formula = function(a, d, e)
    local ret = nil
    local baseRet = floor((0.32 * e[1] + 5) * 3)
            if e[1] == 0 then
        ret = 0
    elseif e[1] < 30 then
        ret = baseRet
    elseif e[1] < 50 then
        ret = 2 * baseRet
    else
        ret = 4 * baseRet
    end

    return ret
end


AllFormulas.Action_Formula[1344] = AllFormulas.Action_Formula[1344] or {  }
AllFormulas.Action_Formula[1344].Formula = function(a, d, e)
    local ret = nil
    local baseRet = floor((0.32 * e[1] + 5) * 4)
            if e[1] == 0 then
        ret = 0
    elseif e[1] < 30 then
        ret = baseRet
    elseif e[1] < 50 then
        ret = 2 * baseRet
    else
        ret = 4 * baseRet
    end

    return ret
end


AllFormulas.Action_Formula[1345] = AllFormulas.Action_Formula[1345] or {  }
AllFormulas.Action_Formula[1345].Formula = function(a, d, e)
    local ret = nil
    local baseRet = floor((0.32 * e[1] + 5) * 5)
            if e[1] == 0 then
        ret = 0
    elseif e[1] < 30 then
        ret = baseRet
    elseif e[1] < 50 then
        ret = 2 * baseRet
    else
        ret = 4 * baseRet
    end

    return ret
end


AllFormulas.Action_Formula[1346] = AllFormulas.Action_Formula[1346] or {  }
AllFormulas.Action_Formula[1346].Formula = function(a, d, e)
    local ret = nil
    local baseRet = floor((0.32 * e[1] + 5) * 6)
            if e[1] == 0 then
        ret = 0
    elseif e[1] < 30 then
        ret = baseRet
    elseif e[1] < 50 then
        ret = 2 * baseRet
    else
        ret = 4 * baseRet
    end

    return ret
end


AllFormulas.Action_Formula[1347] = AllFormulas.Action_Formula[1347] or {  }
AllFormulas.Action_Formula[1347].Formula = function(a, d, e)
    local ret = nil
    local baseRet = floor((0.32 * e[1] + 5) * 7)
            if e[1] == 0 then
        ret = 0
    elseif e[1] < 30 then
        ret = baseRet
    elseif e[1] < 50 then
        ret = 2 * baseRet
    else
        ret = 4 * baseRet
    end

    return ret
end


AllFormulas.Action_Formula[1348] = AllFormulas.Action_Formula[1348] or {  }
AllFormulas.Action_Formula[1348].Formula = function(a, d, e)
    local ret = nil
    local baseRet = floor((0.32 * e[1] + 5) * 8)
            if e[1] == 0 then
        ret = 0
    elseif e[1] < 30 then
        ret = baseRet
    elseif e[1] < 50 then
        ret = 2 * baseRet
    else
        ret = 4 * baseRet
    end

    return ret
end


AllFormulas.Action_Formula[1349] = AllFormulas.Action_Formula[1349] or {  }
AllFormulas.Action_Formula[1349].Formula = function(a, d, e)
    local ret = nil
    local baseRet = floor((0.32 * e[1] + 5) * 9)
            if e[1] == 0 then
        ret = 0
    elseif e[1] < 30 then
        ret = baseRet
    elseif e[1] < 50 then
        ret = 2 * baseRet
    else
        ret = 4 * baseRet
    end

    return ret
end


AllFormulas.Action_Formula[1350] = AllFormulas.Action_Formula[1350] or {  }
AllFormulas.Action_Formula[1350].Formula = function(a, d, e)
    local ret = nil
    local buffLv = GetGradeByBuffCls(a, 690240)
    ret = (e[1] + d:FightProp():GetParamGrade() * e[2] + d:FightProp():GetParamGrade() ^ 2 * e[3]) * (1 + buffLv * 0.3)
    return ret
end


AllFormulas.Action_Formula[1351] = AllFormulas.Action_Formula[1351] or {  }
AllFormulas.Action_Formula[1351].Formula = function(a, d, e)
    local ret = nil
    local buffLv = GetGradeByBuffCls(a, 690240)
    ret = e[1] * (1 + buffLv * 0.3)
    return ret
end


AllFormulas.Action_Formula[1352] = AllFormulas.Action_Formula[1352] or {  }
AllFormulas.Action_Formula[1352].Formula = function(a, d, e)
    local ret = nil
    local buffLv = GetGradeByBuffCls(a, 690240)
    ret = d:FightProp():GetParamGrade() * e[1] * (1 + buffLv * 0.3)
    return ret
end


AllFormulas.Action_Formula[1353] = AllFormulas.Action_Formula[1353] or {  }
AllFormulas.Action_Formula[1353].Formula = function(a, d, e)
    local ret = nil
    local buffLv = GetGradeByBuffCls(a, 690240)
    ret = d:FightProp():GetParamGrade() ^ 2 * e[1] * (1 + buffLv * 0.3)
    return ret
end


AllFormulas.Action_Formula[1354] = AllFormulas.Action_Formula[1354] or {  }
AllFormulas.Action_Formula[1354].Formula = function(a, d, e)
    local ret = nil
    local buffLv = GetGradeByBuffCls(a, 690240)
    local buffLv2 = GetGradeByBuffCls(a, 690279)
    ret = e[1] * (1 + buffLv * 0.3 + buffLv2 * 0.015)
    return ret
end


AllFormulas.Action_Formula[1355] = AllFormulas.Action_Formula[1355] or {  }
AllFormulas.Action_Formula[1355].Formula = function(a, d, e)
    local ret = nil
    local buffLv = GetGradeByBuffCls(a, 690240)
    local buffLv2 = GetGradeByBuffCls(a, 690279)
    ret = d:FightProp():GetParamGrade() * e[1] * (1 + buffLv * 0.3 + buffLv2 * 0.015)
    return ret
end


AllFormulas.Action_Formula[1356] = AllFormulas.Action_Formula[1356] or {  }
AllFormulas.Action_Formula[1356].Formula = function(a, d, e)
    local ret = nil
    local buffLv = GetGradeByBuffCls(a, 690240)
    local buffLv2 = GetGradeByBuffCls(a, 690279)
    ret = d:FightProp():GetParamGrade() ^ 2 * e[1] * (1 + buffLv * 0.3 + buffLv2 * 0.015)
    return ret
end


AllFormulas.Action_Formula[1357] = AllFormulas.Action_Formula[1357] or {  }
AllFormulas.Action_Formula[1357].Formula = function(a, d, e)
    local ret = nil
    ret = e[1]
    return ret
end


AllFormulas.Action_Formula[1358] = AllFormulas.Action_Formula[1358] or {  }
AllFormulas.Action_Formula[1358].Formula = function(a, d, e)
    local ret = nil
    ret = floor(((min(300, d:FightProp():GetParamAdjAntiFire()) + min(300, d:FightProp():GetParamAdjAntiThunder()) + min(300, d:FightProp():GetParamAdjAntiIce()) + min(300, d:FightProp():GetParamAdjAntiPoison()) + min(300, d:FightProp():GetParamAdjAntiWind()) + min(300, d:FightProp():GetParamAdjAntiLight()) + min(300, d:FightProp():GetParamAdjAntiIllusion()) + min(300, d:FightProp():GetParamAdjAntiWater())) + (min(300, d:FightProp():GetParamEnhanceDizzy()) + min(300, d:FightProp():GetParamEnhanceChaos()) + min(300, d:FightProp():GetParamEnhanceSilence()) + min(300, d:FightProp():GetParamEnhanceSleep()) + min(300, d:FightProp():GetParamEnhanceTie()) + min(300, d:FightProp():GetParamEnhanceBind())) + (min(300, d:FightProp():GetParamAntiDizzy()) + min(300, d:FightProp():GetParamAntiChaos()) + min(300, d:FightProp():GetParamAntiSilence()) + min(300, d:FightProp():GetParamAntiSleep()) + min(300, d:FightProp():GetParamAntiTie()) + min(300, d:FightProp():GetParamAntiBind()))) * e[1])
    return ret
end


AllFormulas.Action_Formula[1359] = AllFormulas.Action_Formula[1359] or {  }
AllFormulas.Action_Formula[1359].Formula = function(a, d, e)
    local ret = nil
    ret = floor((max(d:FightProp():GetParampAttMin(), d:FightProp():GetParammAttMin()) + max(d:FightProp():GetParampAttMax(), d:FightProp():GetParammAttMax())) * e[1])
    return ret
end


AllFormulas.Action_Formula[1360] = AllFormulas.Action_Formula[1360] or {  }
AllFormulas.Action_Formula[1360].Formula = AllFormulas.Action_Formula[1359].Formula


AllFormulas.Action_Formula[1361] = AllFormulas.Action_Formula[1361] or {  }
AllFormulas.Action_Formula[1361].Formula = function(a, d, e)
    local ret = nil
    ret = floor((1 - d:GetHp() / d:FightProp():GetParamHpFull()) * 100) / 100 * e[1]
    return ret
end


AllFormulas.Action_Formula[1362] = AllFormulas.Action_Formula[1362] or {  }
AllFormulas.Action_Formula[1362].Formula = function(a, d, e)
    local ret = nil
    local buffLv = GetGradeByBuffCls(a, 690292)
    ret = e[1] * 100 * (0.5 + buffLv * 0.003)
    return ret
end


AllFormulas.Action_Formula[1391] = AllFormulas.Action_Formula[1391] or {  }
AllFormulas.Action_Formula[1391].Formula = function(a, d, e)
    local ret = nil
    ret = floor((e[1] + e[2] + e[3] + 150) ^ 2 / 1000) / floor(153 ^ 2 / 1000) - 1
    return ret
end


AllFormulas.Action_Formula[1392] = AllFormulas.Action_Formula[1392] or {  }
AllFormulas.Action_Formula[1392].Formula = function(a, d, e)
    local ret = nil
    ret = -10 * max(0, e[1] - 20) + 7 * e[1] ^ 2
    return ret
end


AllFormulas.Action_Formula[1393] = AllFormulas.Action_Formula[1393] or {  }
AllFormulas.Action_Formula[1393].Formula = function(a, d, e)
    local ret = nil
    ret = 0.03 * e[1]
    return ret
end


AllFormulas.Action_Formula[1394] = AllFormulas.Action_Formula[1394] or {  }
AllFormulas.Action_Formula[1394].Formula = function(a, d, e)
    local ret = nil
    ret = 2 * e[1]
    return ret
end


AllFormulas.Action_Formula[1395] = AllFormulas.Action_Formula[1395] or {  }
AllFormulas.Action_Formula[1395].Formula = function(a, d, e)
    local ret = nil
    ret = 10 * e[1]
    return ret
end


AllFormulas.Action_Formula[1396] = AllFormulas.Action_Formula[1396] or {  }
AllFormulas.Action_Formula[1396].Formula = function(a, d, e)
    local ret = nil
    ret = 0.06 * e[1]
    return ret
end


AllFormulas.Action_Formula[1397] = AllFormulas.Action_Formula[1397] or {  }
AllFormulas.Action_Formula[1397].Formula = function(a, d, e)
    local ret = nil
    ret = 0.01 * e[1]
    return ret
end


AllFormulas.Action_Formula[1398] = AllFormulas.Action_Formula[1398] or {  }
AllFormulas.Action_Formula[1398].Formula = function(a, d, e)
    local ret = nil
    ret = 12 * e[1]
    return ret
end


AllFormulas.Action_Formula[1399] = AllFormulas.Action_Formula[1399] or {  }
AllFormulas.Action_Formula[1399].Formula = function(a, d, e)
    local ret = nil
    ret = e[1]
    return ret
end


AllFormulas.Action_Formula[1400] = AllFormulas.Action_Formula[1400] or {  }
AllFormulas.Action_Formula[1400].Formula = function(a, d, e)
    local ret = nil
    ret = floor(e[1] * 10 / 7) / 100
    return ret
end


AllFormulas.Action_Formula[1401] = AllFormulas.Action_Formula[1401] or {  }
AllFormulas.Action_Formula[1401].Formula = function(a, d, e)
    local ret = nil
    ret = min(max((e[1] * 0.3), 0.25), 1.25)
    return ret
end


AllFormulas.Action_Formula[1402] = AllFormulas.Action_Formula[1402] or {  }
AllFormulas.Action_Formula[1402].Formula = function(a, d, e)
    local ret = nil
                    if e[1] < 2 then
        ret = 0
    elseif e[1] < 3 and e[1] >= 2 then
        ret = e[1] * 100
    elseif e[1] < 4 and e[1] >= 3 then
        ret = e[1] * 150
    elseif e[1] < 5 and e[1] >= 4 then
        ret = e[1] * 200 - 1
    elseif e[1] >= 5 then
        ret = 999
    end

    return ret
end


AllFormulas.Action_Formula[1403] = AllFormulas.Action_Formula[1403] or {  }
AllFormulas.Action_Formula[1403].Formula = function(a, d, e)
    local ret = nil
    ret = e[1]
    return ret
end


AllFormulas.Action_Formula[1404] = AllFormulas.Action_Formula[1404] or {  }
AllFormulas.Action_Formula[1404].Formula = function(a, d, e)
    local ret = nil
    ret = e[1] / 1000
    return ret
end


AllFormulas.Action_Formula[1405] = AllFormulas.Action_Formula[1405] or {  }
AllFormulas.Action_Formula[1405].Formula = function(a, d, e)
    local ret = nil
    if e[2] * e[3] < 10 then
        ret = 1
    else
        ret = 0.1
    end

    return ret
end


AllFormulas.Action_Formula[1406] = AllFormulas.Action_Formula[1406] or {  }
AllFormulas.Action_Formula[1406].Formula = function(a, d, e)
    local ret = nil
    ret = max(100, floor(0.95 * e[1] / e[2] * 100))
    return ret
end


AllFormulas.Action_Formula[1462] = AllFormulas.Action_Formula[1462] or {  }
AllFormulas.Action_Formula[1462].Formula = function(a, d, e)
    local ret = nil
    ret = max(floor(e[1] / 1.4) - 1, 1)
    return ret
end


AllFormulas.Action_Formula[1466] = AllFormulas.Action_Formula[1466] or {  }
AllFormulas.Action_Formula[1466].Formula = function(a, d, e)
    local ret = nil
    ret = min(80, 8 * e[1]) + min(40, 4 * e[2]) + min(30, 15 * e[3]) + min(50, floor(e[4] / 50)) + min(50, floor(e[5] / 50))
    return ret
end


AllFormulas.Action_Formula[1467] = AllFormulas.Action_Formula[1467] or {  }
AllFormulas.Action_Formula[1467].Formula = function(a, d, e)
    local ret = nil
    if e[2] < 20 then
        ret = min(300, e[2] * (2 + e[1]))
    else
        ret = min(300, e[2] * (5 + e[1]))
    end

    return ret
end


AllFormulas.Action_Formula[1509] = AllFormulas.Action_Formula[1509] or {  }
AllFormulas.Action_Formula[1509].Formula = function(a, d, e)
    local ret = nil
    local e1_ret = e[1] or 15
    local tf_xun = GetGradeBySkillCls(d, 913621)
    local touzhi_lv = GetGradeByBuffCls(d, 642514) + GetGradeByBuffCls(a, 642512)
    local fight_lv = GetGradeByBuffCls(d, 642490) + GetGradeByBuffCls(d, 642497) + GetGradeByBuffCls(d, 642520)
    if tf_xun > 0 and touzhi_lv <= 0 and fight_lv > 0 then
        e1_ret = 2.5
    end

    ret = e1_ret
    return ret
end


AllFormulas.Action_Formula[1513] = AllFormulas.Action_Formula[1513] or {  }
AllFormulas.Action_Formula[1513].Formula = function(a, d, e)
    local ret = nil
    local e_ret = e[1]
    local b_lv = GetGradeByBuffCls(d, e[2])
    if b_lv > 0 then
        e_ret = min(1, e_ret + b_lv * e[3])
    end

    ret = e_ret
    return ret
end


AllFormulas.Action_Formula[1525] = AllFormulas.Action_Formula[1525] or {  }
AllFormulas.Action_Formula[1525].Formula = function(a, d, e)
    local ret = nil
    ret = floor((e[1] / e[2] * e[3] - e[1]) * 0.9) + e[1]
    return ret
end


AllFormulas.Action_Formula[1526] = AllFormulas.Action_Formula[1526] or {  }
AllFormulas.Action_Formula[1526].Formula = function(a, d, e)
    local ret = nil
    ret = floor(e[2] * 100 / e[1] - 100) * 0.009
    return ret
end


AllFormulas.Action_Formula[1559] = AllFormulas.Action_Formula[1559] or {  }
AllFormulas.Action_Formula[1559].Formula = function(a, d, e)
    local ret = nil
    ret = floor(((0.8 + a:GetRealLevel() * (0.1 + 0.045 * min(1, max(0, a:GetRealLevel() - 69)) + 0.045 * min(1, max(0, a:GetRealLevel() - 89)))) * (0.2 * a:GetRealLevel() + 2) - 0.9) * 10 * e[1])
    return ret
end


AllFormulas.Action_Formula[1561] = AllFormulas.Action_Formula[1561] or {  }
AllFormulas.Action_Formula[1561].Formula = function(a, d, e)
    local ret = nil
    ret = (e[1] - e[2]) * (e[1] - e[2]) * e[3]
    return ret
end


AllFormulas.Action_Formula[1562] = AllFormulas.Action_Formula[1562] or {  }
AllFormulas.Action_Formula[1562].Formula = function(a, d, e)
    local ret = nil
    local b = math.sqrt(e[1])
    ret = min(ceil(b / 1.5 + 0.001), 12)
    return ret
end


AllFormulas.Action_Formula[1563] = AllFormulas.Action_Formula[1563] or {  }
AllFormulas.Action_Formula[1563].Formula = function(a, d, e)
    local ret = nil
    ret = e[1] * max((1 - e[2] * 2), 0.1) + e[3]
    return ret
end


AllFormulas.Action_Formula[1570] = AllFormulas.Action_Formula[1570] or {  }
AllFormulas.Action_Formula[1570].Formula = function(a, d, e)
    local ret = nil
    ret = max(floor(power((1 + min(1, max(0, e[2] - 2)) * 2 + min(1, max(0, e[2] - 5)) * 2 + min(1, max(0, e[2] - 6)) * 0.2 + min(1, max(0, e[2] - 8)) * 0.8), (1.4 + (max(0, min(144, e[1]) - 120)) * 0.015)) + power(max(0, min(155, e[1]) - 119), 1.55) * 0.03 + (1 + min(1, max(0, e[2] - 2)) * 2 + min(1, max(0, e[2] - 5)) * 2) * 1), floor(power((1 + min(1, max(0, e[2] - 2)) * 2 + min(1, max(0, e[2] - 5)) * 2 + min(1, max(0, e[2] - 6)) * 2 + min(1, max(0, e[2] - 8)) * 2 + min(1, max(0, e[2] - 11)) * 3 + min(1, max(0, e[2] - 13)) * 2 + min(1, max(0, e[2] - 16)) * 3 + min(1, max(0, e[2] - 18)) * 3), 1.24 + (e[1] - 129) * 0.0067) + power(e[1] - 109, 1.5) * 0.02 + 1))
    return ret
end

