local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UITexture = import "UITexture"
local DelegateFactory = import "DelegateFactory"
local UISprite = import "UISprite"
local LocalString = import "LocalString"
local Vector3 = import "UnityEngine.Vector3"
local CCPlayerCtrl = import "L10.Game.CCPlayerCtrl"
local BoxCollider = import "UnityEngine.BoxCollider"
local Quaternion = import "UnityEngine.Quaternion"
local Application = import "UnityEngine.Application"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local SoundManager = import "SoundManager"
local Main = import "L10.Engine.Main"

LuaDetailMovieShowWnd = class()
RegistChildComponent(LuaDetailMovieShowWnd,"rotateNode", GameObject)
RegistChildComponent(LuaDetailMovieShowWnd,"movieTexture", CCPlayerCtrl)
RegistChildComponent(LuaDetailMovieShowWnd,"replayBtn", GameObject)
RegistChildComponent(LuaDetailMovieShowWnd,"pauseBtn", GameObject)
RegistChildComponent(LuaDetailMovieShowWnd,"resumeBtn", GameObject)

RegistClassMember(LuaDetailMovieShowWnd, "bg1")
RegistClassMember(LuaDetailMovieShowWnd, "bg2")
RegistClassMember(LuaDetailMovieShowWnd, "playUrl")
RegistClassMember(LuaDetailMovieShowWnd, "playNeedRotate")

function LuaDetailMovieShowWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.DetailMovieShowWnd)
end

function LuaDetailMovieShowWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaDetailMovieShowWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaDetailMovieShowWnd:GetWidthAndHeight(width,height)
  local minWidth = 500
  local minHeight = 500
  local maxWidth = 1600
  local maxHeight = 1000
	if self.playNeedRotate then
		local save = maxWidth
		maxWidth = maxHeight
		maxHeight = save
	end

  local ratio = width / height
  if width >= minWidth and width <= maxWidth and height >= minHeight and height <= maxHeight then
    return width,height
  elseif (width == maxWidth and height < maxHeight) or (width < maxWidth and height == maxHeight) then
    return width,height
  elseif width < minWidth then
    return self:GetWidthAndHeight(minWidth,minWidth / ratio)
  elseif width > maxWidth then
    return self:GetWidthAndHeight(maxWidth,maxWidth / ratio)
  elseif height < minHeight then
    return self:GetWidthAndHeight(minHeight * ratio,minHeight)
  elseif height > maxHeight then
    return self:GetWidthAndHeight(maxHeight * ratio,maxHeight)
  end
end

function LuaDetailMovieShowWnd:SetSpriteSize(node,width,height)
  local sp = node:GetComponent(typeof(UISprite))
  if sp then
    sp.width = width
    sp.height = height
    local collider = node:GetComponent(typeof(BoxCollider))
    if collider then
      collider.size = Vector3(width,height,0)
    end
  end
end

function LuaDetailMovieShowWnd:SetPlayScreen()
  local width = self.movieTexture:GetPlayWidth()
  local height = self.movieTexture:GetPlayHeight()

  local texture = self.movieTexture.transform:GetComponent(typeof(UITexture))
  if texture then
    if width > 0 and height > 0 then
      local setWidth,setHeight = self:GetWidthAndHeight(width,height)
      if setWidth > 0 and setHeight > 0 then
        texture.width = setWidth
        texture.height = setHeight
      end
    end
    self.bg1:SetActive(true)
    self.bg2:SetActive(true)
		if self.playNeedRotate then
			self:SetSpriteSize(self.replayBtn,texture.height,texture.width)
			self:SetSpriteSize(self.pauseBtn,texture.height,texture.width)
			self:SetSpriteSize(self.resumeBtn,texture.height,texture.width)
		else
			self:SetSpriteSize(self.replayBtn,texture.width,texture.height)
			self:SetSpriteSize(self.pauseBtn,texture.width,texture.height)
			self:SetSpriteSize(self.resumeBtn,texture.width,texture.height)
		end
    self.pauseBtn:SetActive(true)
  end
end

function LuaDetailMovieShowWnd:MovieInfoBack(data)
	if data and data.GetVideoInfo and data.GetVideoInfo.VideoInfo then
      local rotate = data.GetVideoInfo.VideoInfo.Rotate

      if rotate ~= 0 then
          self.playNeedRotate = true
          if CommonDefs.IsIOSPlatform() then
              if rotate == 90 then
                  self.rotateNode.transform.localRotation = Quaternion.Euler(0,0,0)
              elseif rotate == 180 then
                  self.rotateNode.transform.localRotation = Quaternion.Euler(0,0,-rotate)
              elseif rotate == 270 then
                  self.rotateNode.transform.localRotation = Quaternion.Euler(0,0,0)
              else
                --这种情况应该不存在的
                  self.rotateNode.transform.localRotation = Quaternion.Euler(0,0,0)
              end
          else
              if CommonDefs.IsAndroidPlatform() and (Main.Inst.EngineVersion < 0 or Main.Inst.EngineVersion > 484993) then
                  self.playNeedRotate = false
              --啥也不做
              else
                  self.rotateNode.transform.localRotation = Quaternion.Euler(0,0,-rotate)
              end
          end
      else
          self.playNeedRotate = false
      end

      self.movieTexture:PlayPureUrl(self.playUrl, 1)
	end
end

function LuaDetailMovieShowWnd:PlayMovie(url)
  if CCPlayerCtrl.IsSupportted() then
    if CommonDefs.IS_HMT_CLIENT then
      self.movieTexture:PlayPureUrl(self.playUrl, 1)
    else
		  LuaPersonalSpaceMgrReal.GetNosVideoInfo(url,function(data)
		  	self:MovieInfoBack(data)
      end)
    end
	else
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("暂不支持此功能,请下载最新版本客户端!"))
		self:Close()
	end
end

function LuaDetailMovieShowWnd:Init()
	SoundManager.Inst:StopBGMusic()

  local url = LuaPersonalSpaceMgrReal.MoviePlayUrl
	url = string.gsub(url,'https://','http://')
	self.playUrl = url

  self.bg1 = self.movieTexture.transform:Find('bg1').gameObject
  self.bg2 = self.movieTexture.transform:Find('bg2').gameObject
  self.bg1:SetActive(false)
  self.bg2:SetActive(false)

	local onReplayClick = function(go)
    self:PlayMovie(url)
    self.replayBtn:SetActive(false)
    self.pauseBtn:SetActive(true)
    self.resumeBtn:SetActive(false)
	end
	CommonDefs.AddOnClickListener(self.replayBtn,DelegateFactory.Action_GameObject(onReplayClick),false)
	local onPauseClick = function(go)
    self.movieTexture:Pause()
    self.replayBtn:SetActive(false)
    self.pauseBtn:SetActive(false)
    self.resumeBtn:SetActive(true)
	end
	CommonDefs.AddOnClickListener(self.pauseBtn,DelegateFactory.Action_GameObject(onPauseClick),false)
	local onResumeClick = function(go)
    self.movieTexture:Resume()
    self.replayBtn:SetActive(false)
    self.pauseBtn:SetActive(true)
    self.resumeBtn:SetActive(false)
	end
	CommonDefs.AddOnClickListener(self.resumeBtn,DelegateFactory.Action_GameObject(onResumeClick),false)
  self.replayBtn:SetActive(false)
  self.pauseBtn:SetActive(false)
  self.resumeBtn:SetActive(false)

  local beginPlay = function()
    self:SetPlayScreen()
  end
  self.movieTexture.OnBeginPlay = DelegateFactory.Action(beginPlay)
  local endPlay = function()
    self.replayBtn:SetActive(true)
    self.pauseBtn:SetActive(false)
    self.resumeBtn:SetActive(false)
    self.movieTexture:Stop()
  end
  self.movieTexture.OnEndPlay = DelegateFactory.Action(endPlay)

  self:PlayMovie(url)
end

function LuaDetailMovieShowWnd:OnDestroy()
	SoundManager.Inst:StartBGMusic()
end

return LuaDetailMovieShowWnd
