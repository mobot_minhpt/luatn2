local CLingShouBaseMgr=import "L10.Game.CLingShouBaseMgr"
local MessageWndManager=import "L10.UI.MessageWndManager"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local CUIFxPaths=import "L10.UI.CUIFxPaths"
local LuaTweenUtils=import "LuaTweenUtils"
local COpenEntryMgr=import "L10.Game.COpenEntryMgr"
local TriggerOnceGuideType=import "L10.Game.Guide.TriggerOnceGuideType"

CLuaLingShouMarriageMatchWnd=class()
RegistClassMember(CLuaLingShouMarriageMatchWnd,"m_MatchButton")
RegistClassMember(CLuaLingShouMarriageMatchWnd,"m_CostLabel")
RegistClassMember(CLuaLingShouMarriageMatchWnd,"m_ShouMingLabel")
RegistClassMember(CLuaLingShouMarriageMatchWnd,"m_Cost")
RegistClassMember(CLuaLingShouMarriageMatchWnd,"m_AddButton")
RegistClassMember(CLuaLingShouMarriageMatchWnd,"m_AddButtonDisplay")
RegistClassMember(CLuaLingShouMarriageMatchWnd,"m_Icon1")
RegistClassMember(CLuaLingShouMarriageMatchWnd,"m_Icon2")
RegistClassMember(CLuaLingShouMarriageMatchWnd,"m_Title")

RegistClassMember(CLuaLingShouMarriageMatchWnd,"m_ZhaoQinPlayerId")
RegistClassMember(CLuaLingShouMarriageMatchWnd,"m_ZhaoQinLingShouId")
RegistClassMember(CLuaLingShouMarriageMatchWnd,"m_LingShouId")
RegistClassMember(CLuaLingShouMarriageMatchWnd,"m_LingShouShouMing")

RegistClassMember(CLuaLingShouMarriageMatchWnd,"m_UIFx")

RegistClassMember(CLuaLingShouMarriageMatchWnd,"m_PlayerName")


function CLuaLingShouMarriageMatchWnd:Init()
    self.m_LingShouShouMing=0

    self.m_PlayerName = ""

    self.m_MatchButton=self.transform:Find("Anchor/MatchButton").gameObject
    UIEventListener.Get(self.m_MatchButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickMatchButton(go)
    end)


    local item1=self.transform:Find("Anchor/Item1").gameObject
    UIEventListener.Get(item1).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickItem1(go)
    end)

    self.m_AddButtonDisplay=self.transform:Find("Anchor/Item2/Button").gameObject
    self.m_AddButton=self.transform:Find("Anchor/Item2/AddButton").gameObject
    UIEventListener.Get(self.m_AddButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickAddButton(go)
    end)

    local tipButton=self.transform:Find("Anchor/TipButton").gameObject
    UIEventListener.Get(tipButton).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("LingShou_HeBaZi_Tip")
    end)


    self.m_Title=self.transform:Find("Anchor/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_Title.text=SafeStringFormat3( LocalString.GetString("[c][ffffff]%s[-][/c]正在招亲"),CLuaLingShouMgr.ZhaoQinLingShouName )

    self.m_Icon1=self.transform:Find("Anchor/Item1/Icon1"):GetComponent(typeof(CUITexture))
    self.m_Icon1:LoadNPCPortrait(CLingShouBaseMgr.GetLingShouIcon(CLuaLingShouMgr.ZhaoQinLingShouTemplateId))

    self.m_ZhaoQinLingShouId=CLuaLingShouMgr.ZhaoQinLingShouId
    self.m_ZhaoQinPlayerId=CLuaLingShouMgr.ZhaoQinPlayerId
    self.m_LingShouId=nil

    self.m_Icon2=self.transform:Find("Anchor/Item2/Icon2"):GetComponent(typeof(CUITexture))

    self.m_Cost=LingShouBaby_Setting.GetData().BaZiLifespanCost

    self.m_ShouMingLabel=self.transform:Find("Anchor/ShouMingLabel"):GetComponent(typeof(UILabel))
    self.m_CostLabel=self.transform:Find("Anchor/CostLabel"):GetComponent(typeof(UILabel))
    self.m_CostLabel.text=tostring(self.m_Cost)
    self.m_ShouMingLabel.text="[c][ff0000]0[-][/c]"--没选灵兽的时候 寿命显示为0

    self.m_UIFx=self.transform:Find("Anchor/Fx"):GetComponent(typeof(CUIFx))
    self.m_UIFx:DestroyFx()
    self.m_UIFx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)

    if not COpenEntryMgr.Inst:GetTriggeredState(TriggerOnceGuideType.LingShouMarriageClickGuide) then
        local path={Vector3(110,0,0),Vector3(0,-110,0),Vector3(-110,0,0),Vector3(0,110,0),Vector3(110,0,0)}
        local array = Table2Array(path, MakeArrayClass(Vector3))
        LuaUtils.SetLocalPosition(self.m_UIFx.FxRoot,110,0,0)
        LuaTweenUtils.DODefaultLocalPath(self.m_UIFx.FxRoot,array,2)
    end

    Gac2Gas.RequestPlayerName(CLuaLingShouMgr.ZhaoQinPlayerId)

end
function CLuaLingShouMarriageMatchWnd:OnClickMatchButton(go)
    if self.m_LingShouId then
        Gac2Gas.RequestLingShouHeBaZi(self.m_LingShouId, self.m_ZhaoQinPlayerId, self.m_ZhaoQinLingShouId)
    end
end
function CLuaLingShouMarriageMatchWnd:OnClickAddButton(go)
    CUIManager.ShowUI(CLuaUIResources.LingShouMarriageMatchSelectWnd)
end

function CLuaLingShouMarriageMatchWnd:OnClickItem1(go)
    self.m_UIFx:DestroyFx()
    if not COpenEntryMgr.Inst:GetTriggeredState(TriggerOnceGuideType.LingShouMarriageClickGuide) then
        COpenEntryMgr.Inst:SetTriggeredState(TriggerOnceGuideType.LingShouMarriageClickGuide)
    end

    CLuaLingShouOtherMgr.lingShouId = CLuaLingShouMgr.ZhaoQinLingShouId
    CLuaLingShouOtherMgr.playerId = CLuaLingShouMgr.ZhaoQinPlayerId
    CLuaLingShouOtherMgr.playerName = self.m_PlayerName
    CLuaLingShouOtherMgr.requestType = LuaEnumOtherLingShouRequestType.SingleLingShou
    CLuaLingShouOtherMgr.ClearLingShouData()
    Gac2Gas.QueryAllPlayerLingShou(CLuaLingShouOtherMgr.playerId, 0)--主要是为了请求结伴信息
    Gac2Gas.QueryLingShouInfo(CLuaLingShouOtherMgr.playerId, CLuaLingShouOtherMgr.lingShouId, 0)
end

function CLuaLingShouMarriageMatchWnd:OnEnable()
    g_ScriptEvent:AddListener("LingShouMarriageSelectDone", self, "OnLingShouMarriageSelectDone")
    g_ScriptEvent:AddListener("LingShouHeBaZiResult", self, "OnLingShouHeBaZiResult")
    --获取寿命值
    g_ScriptEvent:AddListener("GetLingShouDetails", self, "OnGetLingShouDetails")
    g_ScriptEvent:AddListener("SendPlayerName", self, "OnSendPlayerName")
    
end
function CLuaLingShouMarriageMatchWnd:OnDisable()
    g_ScriptEvent:RemoveListener("LingShouMarriageSelectDone", self, "OnLingShouMarriageSelectDone")
    g_ScriptEvent:RemoveListener("LingShouHeBaZiResult", self, "OnLingShouHeBaZiResult")
    g_ScriptEvent:RemoveListener("GetLingShouDetails", self, "OnGetLingShouDetails")
    g_ScriptEvent:RemoveListener("SendPlayerName", self, "OnSendPlayerName")
end
function CLuaLingShouMarriageMatchWnd:OnSendPlayerName(playerId,name)
    if playerId == CLuaLingShouMgr.ZhaoQinPlayerId then
        self.m_PlayerName=name
    end
end

function CLuaLingShouMarriageMatchWnd:OnGetLingShouDetails(args)
    local lingShouId=args[0]
    local data=args[1]
    if lingShouId==self.m_LingShouId then
        self:UpdateShouMing(data.data.Props.Shouming)
    end
end
function CLuaLingShouMarriageMatchWnd:UpdateShouMing(shouming)
    self.m_LingShouShouMing=shouming

    if shouming>=self.m_Cost then
        self.m_ShouMingLabel.text=tostring(shouming)
    else
        self.m_ShouMingLabel.text=SafeStringFormat3("[c][ff0000]%d[-][/c]",shouming)
    end
end

function CLuaLingShouMarriageMatchWnd:OnLingShouHeBaZiResult(args)
    if args[0]==true then
        local msg=g_MessageMgr:FormatMessage("LingShou_HeBaZi_Success",{})
        local function onOk()
            --判断寿命
            if self.m_LingShouShouMing>=self.m_Cost then
                Gac2Gas.RequestEngageWithTargetLingShou(self.m_LingShouId, self.m_ZhaoQinPlayerId, self.m_ZhaoQinLingShouId)
            else
                g_MessageMgr:ShowMessage("LingShou_Engage_Need_ShouMing", {})
            end
        end
        MessageWndManager.ShowRedStyleOKCancelMessage(msg,DelegateFactory.Action(onOk),nil,LocalString.GetString("前往求亲"),LocalString.GetString("暂不求亲"),false)
    end
end

function CLuaLingShouMarriageMatchWnd:OnLingShouMarriageSelectDone(templateId,id)
    local icon=CLingShouBaseMgr.GetLingShouIcon(templateId)
    self.m_Icon2:LoadNPCPortrait(icon)
    self.m_AddButtonDisplay:SetActive(false)
    
    self.m_LingShouId=id
    --请求寿命数据
    CLingShouMgr.Inst:RequestLingShouDetails(self.m_LingShouId)
end

