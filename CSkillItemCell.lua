-- Auto Generated!!
local BoolDelegate = import "UIEventListener+BoolDelegate"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CSkillItemCell = import "L10.UI.CSkillItemCell"
local CSkillMgr = import "L10.Game.CSkillMgr"
local Object = import "System.Object"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local SkillCategory = import "L10.Game.SkillCategory"
local SkillKind = import "L10.Game.SkillKind"
local UIEventListener = import "UIEventListener"
local UIWidget = import "UIWidget"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CSkillItemCell.m_Init_CS2LuaHook = function (this, classId, originLevel, deltaLevel, showDelta, iconName, showDisableSprite, canLearnText) 

    this:UpdateLevel(classId, originLevel, deltaLevel, showDelta, showDisableSprite, canLearnText)
    if iconName == nil then
        this.skillIcon:Clear()
    else
        this.skillIcon:LoadSkillIcon(iconName)
    end
    if this.disableSprite ~= nil then
        this.disableSprite.enabled = showDisableSprite
    end

    this.Selected = false

    local data = Skill_AllSkills.GetData(CLuaDesignMgr.Skill_AllSkills_GetSkillId(classId, 1))
    this.ShowPassiveIcon = (data ~= nil and data.ECategory == SkillCategory.Passive)
    this.Kind = data ~= nil and data.EKind or SkillKind.CommonSkill

    if this.colorSystemIcon ~= nil then
        if CSkillMgr.Inst:IsColorSystemSkill(classId) or CSkillMgr.Inst:IsColorSystemBaseSkill(classId) then
            this.colorSystemIcon.gameObject:SetActive(true)
            this.colorSystemIcon.spriteName = CSkillMgr.Inst:GetColorSystemSkillIcon(classId)
        else
            this.colorSystemIcon.gameObject:SetActive(false)
        end
    end
end
CSkillItemCell.m_UpdateColorSystemSkillIcon_CS2LuaHook = function (this, cls) 
    local data = Skill_AllSkills.GetData(CLuaDesignMgr.Skill_AllSkills_GetSkillId(cls, 1))
    if data == nil then
        return
    end
    this.skillIcon:LoadSkillIcon(data.SkillIcon)
    if this.colorSystemIcon ~= nil then
        if CSkillMgr.Inst:IsColorSystemSkill(cls) or CSkillMgr.Inst:IsColorSystemBaseSkill(cls) then
            this.colorSystemIcon.gameObject:SetActive(true)
            this.colorSystemIcon.spriteName = CSkillMgr.Inst:GetColorSystemSkillIcon(cls)
        else
            this.colorSystemIcon.gameObject:SetActive(false)
        end
    end
end
CSkillItemCell.m_UpdateLevel_CS2LuaHook = function (this, classId, originLevel, deltaLevel, showDelta, showDisableSprite, canLearnText) 

    this.classId = classId
    this.originLevel = originLevel
    this.deltaLevel = deltaLevel

    if this.levelLabel ~= nil then
        if not showDelta or deltaLevel == 0 then
            if this.m_FeiSheng and this.m_FeiShengLevel ~= originLevel then
                local default
                if this.m_FeiShengLevel > 0 then
                    default = System.String.Format("[c][{0}]{1}[-][/c]", Constants.ColorOfFeiSheng, this.m_FeiShengLevel)
                else
                    default = nil
                end
                this.levelLabel.text = default
            else
                this.levelLabel.text = originLevel > 0 and tostring(originLevel) or nil
            end
        else
            if this.m_FeiSheng and this.m_FeiShengLevel ~= originLevel then
                this.levelLabel.text = System.String.Format("[c][{0}]{1}(+{2})[-][/c]", Constants.ColorOfFeiSheng, this.m_FeiShengLevel, deltaLevel)
            else
                this.levelLabel.text = System.String.Format("{0}(+{1})", originLevel, deltaLevel)
            end
        end

        this.levelLabel.gameObject:SetActive(originLevel > 0)
    end
    if this.disableSprite ~= nil then
        this.disableSprite.enabled = showDisableSprite
    end

    if this.levelLabel ~= nil then
        if this.levelLabel.gameObject.activeSelf then
            local w = CommonDefs.GetComponentInChildren_Component_Type(this.levelLabel, typeof(UIWidget))
            w:ResetAndUpdateAnchors()
        end
    end

    if this.canLearnLabel ~= nil then
        this.canLearnLabel.text = canLearnText
    end
end
CSkillItemCell.m_InitFeiSheng_CS2LuaHook = function (this, active, level) 
    this.m_FeiSheng = active
    this.m_FeiShengLevel = level
end
CSkillItemCell.m_Start_CS2LuaHook = function (this) 

    UIEventListener.Get(this.gameObject).onDragStart = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.gameObject).onDragStart, MakeDelegateFromCSFunction(this.OnDragIconStart, VoidDelegate, this), true)
    UIEventListener.Get(this.gameObject).onPress = CommonDefs.CombineListner_BoolDelegate(UIEventListener.Get(this.gameObject).onPress, MakeDelegateFromCSFunction(this.OnPressIcon, BoolDelegate, this), true)
end
CSkillItemCell.m_OnDragIconStart_CS2LuaHook = function (this, go) 

    if this.OnDragStart ~= nil then
        GenericDelegateInvoke(this.OnDragStart, Table2ArrayWithCount({go, this.classId}, 2, MakeArrayClass(Object)))
    end
end
CSkillItemCell.m_OnPressIcon_CS2LuaHook = function (this, go, pressed) 

    if this.OnPress ~= nil then
        GenericDelegateInvoke(this.OnPress, Table2ArrayWithCount({go, pressed, this.classId}, 3, MakeArrayClass(Object)))
    end
end
CSkillItemCell.m_GetDragFromGo_CS2LuaHook = function (this) 

    return this.gameObject
end
