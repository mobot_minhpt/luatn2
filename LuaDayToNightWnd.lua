local Object=import "System.Object"
local Animator          = import "UnityEngine.Animator"
local UILabel           = import "UILabel"

LuaDayToNightWnd = class()

--------RegistChildComponent-------
RegistChildComponent(LuaDayToNightWnd,  "LongPressBtn",     GameObject)         --  长按按钮
RegistChildComponent(LuaDayToNightWnd,  "TipsLabel",        UILabel)            --  按钮提示信息
RegistChildComponent(LuaDayToNightWnd,  "Tips",             GameObject)         --  按钮和按钮提示信息
RegistChildComponent(LuaDayToNightWnd,  "AnimatorFX",       GameObject)         --  动画特效

---------RegistClassMember-------
RegistClassMember(LuaDayToNightWnd,     "m_Animator")
RegistClassMember(LuaDayToNightWnd,     "m_TimeTick")
RegistClassMember(LuaDayToNightWnd,     "m_Step")

function LuaDayToNightWnd:Init()
    self.TipsLabel.text = LocalString.GetString("长按太阳，感受日月交替")
    self.Tips:SetActive(false)
    self.m_Step = 1

    self.m_TimeTick = RegisterTickOnce(function ()
        self.Tips:SetActive(true)
    end, 1 * 1000)

    UIEventListener.Get(self.LongPressBtn).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        self:OnPress(go, isPressed)
    end)
end

function LuaDayToNightWnd:OnDisable()
    UnRegisterTick(self.m_TimeTick)
end
LuaDayToNightWnd.taskId = 0

function LuaDayToNightWnd:OnPress(go,isPressed)
    UnRegisterTick(self.m_TimeTick)
    if isPressed then
        self.m_TimeTick = RegisterTickOnce(function ()
            if not self.m_Animator then
                self.m_Animator = self.AnimatorFX.transform:Find("__FxRoot__"):GetComponentInChildren(typeof(Animator))
            end
            if self.m_Animator then
                if self.m_Step == 1 then
                    self.m_Animator:CrossFade("UI_riyuezhuanhuan001_step002",0.5,0,0);
                    self.TipsLabel.text = LocalString.GetString("长按月亮，感受潮起潮落")
                else
                    self.m_Animator:CrossFade("UI_riyuezhuanhuan001_step004",0.5,0,0);
                end
            end
            
            self.Tips:SetActive(false)
            UnRegisterTick(self.m_TimeTick)
            if self.m_Step == 1 then
                self.m_TimeTick = RegisterTickOnce(function ()
                    self.Tips:SetActive(true)
                end, 9 * 1000)
            else 
                self.m_TimeTick = RegisterTickOnce(function ()
                    CUIManager.CloseUI(CLuaUIResources.DayToNightWnd)
                    local empty = CreateFromClass(MakeGenericClass(List, Object))
                    Gac2Gas.FinishClientTaskEvent(LuaDayToNightWnd.taskId,"DayToNight",MsgPackImpl.pack(empty))
                end, 5 * 1000)
            end
            self.m_Step = 2
        end, 1 * 1000)
    end
end
