-- Auto Generated!!
local CSecondPwdVerifyWnd = import "L10.UI.CSecondPwdVerifyWnd"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CSecondPwdVerifyWnd.m_Awake_CS2LuaHook = function (this) 
    this.getVerifyButton:SetActive(true)
    this.countDownBtn:SetActive(false)

    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.getVerifyButton).onClick = MakeDelegateFromCSFunction(this.OnClickGetVerifyButton, VoidDelegate, this)
    UIEventListener.Get(this.verifyButton).onClick = MakeDelegateFromCSFunction(this.OnClickVerifyButton, VoidDelegate, this)
end
CSecondPwdVerifyWnd.m_OnClickGetVerifyButton_CS2LuaHook = function (this, go) 
    Gac2Gas.RequestSecondaryPasswordCaptcha(CSecondPwdVerifyWnd.type)
    --开始倒计时
    this.showTime = 60
    this.getVerifyButton:SetActive(false)
    this.countDownBtn:SetActive(true)
end
