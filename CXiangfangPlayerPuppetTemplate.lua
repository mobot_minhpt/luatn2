-- Auto Generated!!
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CXiangfangPlayerPuppetTemplate = import "L10.UI.CXiangfangPlayerPuppetTemplate"
local EnumChineseDigits = import "L10.Game.EnumChineseDigits"
local House_XiangfangUpgrade = import "L10.Game.House_XiangfangUpgrade"
CXiangfangPlayerPuppetTemplate.m_OnInviteBtnClick_CS2LuaHook = function (this, go) 

    --Gac2Gas.QueryFriendsFitPuppetRequirement();
    --CUIManager.ShowUI(CUIResources.PlayerPuppetInviteWnd);
    g_MessageMgr:ShowMessage("OPERATION_NEED_GO_TO_CSC")
end
CXiangfangPlayerPuppetTemplate.m_OnNotOpenSpriteClick_CS2LuaHook = function (this, go) 
    for i = 1, 20 do
        local data = House_XiangfangUpgrade.GetData(i)
        if data ~= nil and data.MaxPuppetNum > this.Index then
            g_MessageMgr:ShowMessage("Roomer_Num_Need_Xiangfang_Grade", i)
            return
        end
    end
end
CXiangfangPlayerPuppetTemplate.m_Init_CS2LuaHook = function (this, index, info) 
    this.Index = index

    if info ~= nil and info:IsInUse() then
        this.placedNode:SetActive(true)
    else
        this.placedNode:SetActive(false)
    end

    if CClientHouseMgr.Inst.mConstructProp ~= nil then
        this.IdLabel.text = EnumChineseDigits.GetDigit(index + 1)
        local xiangfangGrade = CClientHouseMgr.Inst.mConstructProp.XiangfangGrade
        local data = House_XiangfangUpgrade.GetData(xiangfangGrade)
        if data ~= nil then
            local maxRoomCurGrade = data.MaxPuppetNum

            if index >= maxRoomCurGrade then
                this.NotOpenGO:SetActive(true)
                this.PlayerInfoGO:SetActive(false)
                this.NoPlayerGO:SetActive(false)
            else
                if info == nil then
                    this.NotOpenGO:SetActive(false)
                    this.PlayerInfoGO:SetActive(false)
                    this.NoPlayerGO:SetActive(true)
                else
                    this.NotOpenGO:SetActive(false)
                    this.PlayerInfoGO:SetActive(true)
                    this.NoPlayerGO:SetActive(false)

                    local portrait = CUICommonDef.GetPortraitName(info.Class, info.Gender, -1)
                    local name = info.AssignName
                    if System.String.IsNullOrEmpty(name) then
                        name = info.Name
                    end

                    this.Portrait:LoadNPCPortrait(portrait, false)
                    this.Expression.gameObject:SetActive(false)
                    this.NameLabel.text = name
                end
            end
        end
    end
end
