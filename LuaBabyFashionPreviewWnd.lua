require("common/common_include")

local CUITexture = import "L10.UI.CUITexture"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CClientObject = import "L10.Game.CClientObject"
local Baby_Fashion = import "L10.Game.Baby_Fashion"
local BabyMgr = import "L10.Game.BabyMgr"
local Time = import "UnityEngine.Time"
local Baby_Setting = import "L10.Game.Baby_Setting"
local Screen = import "UnityEngine.Screen"

LuaBabyFashionPreviewWnd = class()

RegistChildComponent(LuaBabyFashionPreviewWnd, "RotateLeftButton", GameObject)
RegistChildComponent(LuaBabyFashionPreviewWnd, "RotateRightButton", GameObject)
RegistChildComponent(LuaBabyFashionPreviewWnd, "Texture", CUITexture)
RegistChildComponent(LuaBabyFashionPreviewWnd, "ResetButton", GameObject)
RegistChildComponent(LuaBabyFashionPreviewWnd, "SuitPreivew", QnSelectableButton)
RegistChildComponent(LuaBabyFashionPreviewWnd, "QnModelPreviewer", GameObject)

-- ModelTextureLoader
RegistClassMember(LuaBabyFashionPreviewWnd, "ModelTextureName")
RegistClassMember(LuaBabyFashionPreviewWnd, "ModelTextureLoader")
RegistClassMember(LuaBabyFashionPreviewWnd, "SelectedBaby")
RegistClassMember(LuaBabyFashionPreviewWnd, "SelectedFashionId")

-- 旋转相关
RegistClassMember(LuaBabyFashionPreviewWnd, "Rotation")
RegistClassMember(LuaBabyFashionPreviewWnd, "DefaultRotation")
RegistClassMember(LuaBabyFashionPreviewWnd, "LeftPressed")
RegistClassMember(LuaBabyFashionPreviewWnd, "RightPressed")
RegistClassMember(LuaBabyFashionPreviewWnd, "StartPressTime")
RegistClassMember(LuaBabyFashionPreviewWnd, "DeltaTime")
RegistClassMember(LuaBabyFashionPreviewWnd, "DeltaRot")

function LuaBabyFashionPreviewWnd:Init()
	self:InitValues()
	self:InitClassMembers()
end

function LuaBabyFashionPreviewWnd:InitValues()
	if not LuaBabyMgr.m_PreviewFashionBaby then CUIManager.CloseUI(CLuaUIResources.BabyFashionPreviewWnd) return end
	self.SelectedBaby = LuaBabyMgr.m_PreviewFashionBaby
	self.SelectedFashionId = LuaBabyMgr.m_PreviewFashinId
	LuaBabyMgr.m_PreviewFashionBaby = nil
	self.ModelTextureName = SafeStringFormat3("__%s__", tostring(self.gameObject:GetInstanceID()))

	local setting = Baby_Setting.GetData()
	self.Rotation = setting.FashionPreviewDefaultRotation
	self.DefaultRotation = setting.FashionPreviewDefaultRotation
	self.LeftPressed = false
	self.RightPressed = false
	self.StartPressTime = 0
	self.DeltaTime = 0.025
	self.DeltaRot = -5

end

function LuaBabyFashionPreviewWnd:InitClassMembers()

	self.ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
		self:LoadModel(ro)
	end)

	self.SuitPreivew.OnButtonSelected = DelegateFactory.Action_bool(function (isSelected)
		self:UpdateFashion()
	end)

	CommonDefs.AddOnPressListener(self.RotateLeftButton, DelegateFactory.Action_GameObject_bool(function (go, isPressed)
		self:OnRotateLeftButtonPressed(go, isPressed)
	end), false)

	CommonDefs.AddOnPressListener(self.RotateRightButton, DelegateFactory.Action_GameObject_bool(function (go, isPressed)
		self:OnRotateRightButtonPressed(go, isPressed)
	end), false)

	CommonDefs.AddOnClickListener(self.ResetButton, DelegateFactory.Action_GameObject(function (go)
		self:OnResetButtonClicked(go)
	end), false)

	CommonDefs.AddOnDragListener(self.QnModelPreviewer, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
		self:OnScreenDrag(go, delta)
	end), false)
	self:UpdateFashion()
end

function LuaBabyFashionPreviewWnd:OnRotateLeftButtonPressed(go, isPressed)
	self.LeftPressed = isPressed
end

function LuaBabyFashionPreviewWnd:OnRotateRightButtonPressed(go, isPressed)
	self.RightPressed = isPressed
end

function LuaBabyFashionPreviewWnd:OnResetButtonClicked(go)
	self.Rotation = self.DefaultRotation
	CUIManager.SetModelRotation(self.ModelTextureName, self.Rotation)
end

function LuaBabyFashionPreviewWnd:OnScreenDrag(go, delta)
	local rot = -delta.x / Screen.width * 360
	self.Rotation = self.Rotation + rot
	CUIManager.SetModelRotation(self.ModelTextureName, self.Rotation)
end

function LuaBabyFashionPreviewWnd:UpdateFashion()
	local fashion = Baby_Fashion.GetData(self.SelectedFashionId)
	local rootPosY = -1
	local rootPosZ = 4.66

	local setting = Baby_Setting.GetData()
	local posList = setting.FashionPreviewPos
	if self.SelectedBaby.Props.ExtraData.ShowStatus == EnumBabyStatus.eChild then
		posList = setting.FashionPreviewPosChild
	end
	for i = 0, posList.Length-1, 3 do
		if math.floor(posList[i]) == fashion.Position then
			rootPosY = posList[i+1]
			rootPosZ = posList[i+2]
		end
	end

	local tx = CUIManager.CreateModelTexture(self.ModelTextureName, self.ModelTextureLoader, self.Rotation, 0, rootPosY , rootPosZ, false, true, 1.0)
	self.Texture.mainTexture = tx
end

function LuaBabyFashionPreviewWnd:GetBabyPreviewAppearance()
	local realAppearance = BabyMgr.Inst:ToBabyAppearance(self.SelectedBaby)
	local fashion = Baby_Fashion.GetData(self.SelectedFashionId)

	if fashion.Position == 0 then -- 头发
		realAppearance.FashionHeadId = self.SelectedFashionId
	else
		realAppearance.FashionBodyId = self.SelectedFashionId
		realAppearance.ColorId = 0 -- 清除染色信息
	end

	-- 设置同款预览
	if self.SuitPreivew:isSeleted() then
		if fashion.Position == 0 then
			realAppearance.FashionBodyId = fashion.PreviewSuit
			realAppearance.ColorId = 0 -- 清除染色信息
		else
			realAppearance.FashionHeadId = fashion.PreviewSuit
		end
	end
	realAppearance.BodyWeight = 8
	return realAppearance
end

function LuaBabyFashionPreviewWnd:LoadModel(ro)
	local appear = self:GetBabyPreviewAppearance()
	CClientObject.LoadBabyRenderObject(ro, appear, true, 0, 0)
end


function LuaBabyFashionPreviewWnd:Update()
	if self.LeftPressed and Time.realtimeSinceStartup - self.StartPressTime > self.DeltaTime then
		self.Rotation = self.Rotation + self.DeltaRot
		CUIManager.SetModelRotation(self.ModelTextureName, self.Rotation)
		self.StartPressTime = Time.realtimeSinceStartup
	elseif self.RightPressed and Time.realtimeSinceStartup - self.StartPressTime > self.DeltaTime then
		self.Rotation = self.Rotation - self.DeltaRot
		CUIManager.SetModelRotation(self.ModelTextureName, self.Rotation)
		self.StartPressTime = Time.realtimeSinceStartup
	end
end


function LuaBabyFashionPreviewWnd:OnEnable()

end

function LuaBabyFashionPreviewWnd:OnDisable()
	CUIManager.DestroyModelTexture(self.ModelTextureName)
end

return LuaBabyFashionPreviewWnd
