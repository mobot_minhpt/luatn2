local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"

LuaYuanDan2023MainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYuanDan2023MainWnd, "TimeLb", "TimeLb", UILabel)
RegistChildComponent(LuaYuanDan2023MainWnd, "Act1Btn", "Act1Btn", CButton)
RegistChildComponent(LuaYuanDan2023MainWnd, "Act2Btn", "Act2Btn", CButton)
RegistChildComponent(LuaYuanDan2023MainWnd, "Act3Btn", "Act3Btn", CButton)
RegistChildComponent(LuaYuanDan2023MainWnd, "Act4Btn", "Act4Btn", CButton)
RegistChildComponent(LuaYuanDan2023MainWnd, "Act5Btn", "Act5Btn", CButton)

--@endregion RegistChildComponent end

function LuaYuanDan2023MainWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Act1Btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAct1BtnClick()
	end)


	
	UIEventListener.Get(self.Act2Btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAct2BtnClick()
	end)


	
	UIEventListener.Get(self.Act3Btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAct3BtnClick()
	end)


	
	UIEventListener.Get(self.Act4Btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAct4BtnClick()
	end)


	
	UIEventListener.Get(self.Act5Btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAct5BtnClick()
	end)


    --@endregion EventBind end
end

function LuaYuanDan2023MainWnd:Init()
	self.TimeLb.text = YuanDan2023_Setting.GetData().YuanDanPlayTimeStr
end

--@region UIEvent
-- 元旦奇缘
function LuaYuanDan2023MainWnd:OnAct1BtnClick()
	CUIManager.ShowUI("YuanDan2023YuanDanQiYuanEnterWnd")
end
-- 心愿必达
function LuaYuanDan2023MainWnd:OnAct2BtnClick()
	CUIManager.ShowUI("YuanDan2023XinYuanBiDaWnd")
end
-- 万象更新
function LuaYuanDan2023MainWnd:OnAct3BtnClick()
	local thisactivityId = YuanDan2023_Setting.GetData().WanXiangGengXinActivityID
    local schedule = Task_Schedule.GetData(thisactivityId)
	if not schedule then return end
    local thistaskiId = schedule.TaskID[0]
    CLuaScheduleMgr.selectedScheduleInfo={
        activityId = thisactivityId,
        taskId = thistaskiId,

        TotalTimes = 0,--不显示次数
        DailyTimes = 0,--不显示活力
        FinishedTimes = 0,
        
        ActivityTime = schedule.ExternTip,
        ShowJoinBtn = false 
    }
    CLuaScheduleInfoWnd.s_UseCustomInfo = true
    CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
end
-- 天降宝箱
function LuaYuanDan2023MainWnd:OnAct4BtnClick()
	local thisactivityId = YuanDan2023_Setting.GetData().TianJiangBaoXiangActivityID
    local schedule = Task_Schedule.GetData(thisactivityId)
	if not schedule then return end
    local thistaskiId = schedule.TaskID[0]
    CLuaScheduleMgr.selectedScheduleInfo={
        activityId = thisactivityId,
        taskId = thistaskiId,

        TotalTimes = 0,--不显示次数
        DailyTimes = 0,--不显示活力
        FinishedTimes = 0,
        
        ActivityTime = schedule.ExternTip,
        ShowJoinBtn = false 
    }
    CLuaScheduleInfoWnd.s_UseCustomInfo = true
    CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
end
-- 跨年狂欢
function LuaYuanDan2023MainWnd:OnAct5BtnClick()
	local thisactivityId = YuanDan2023_Setting.GetData().KuaNianKuangHuanActivityID
    local schedule = Task_Schedule.GetData(thisactivityId)
	if not schedule then return end
    local thistaskiId = schedule.TaskID[0]
    CLuaScheduleMgr.selectedScheduleInfo={
        activityId = thisactivityId,
        taskId = thistaskiId,

        TotalTimes = 0,--不显示次数
        DailyTimes = 0,--不显示活力
        FinishedTimes = 0,
        
        ActivityTime = schedule.ExternTip,
        ShowJoinBtn = false 
    }
    CLuaScheduleInfoWnd.s_UseCustomInfo = true
    CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
end


--@endregion UIEvent

