local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local CButton = import "L10.UI.CButton"
local CItemConsumeWndMgr=import "L10.UI.CItemConsumeWndMgr"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local QnTableView = import "L10.UI.QnTableView"
local LingShouOverview = import "L10.Game.CLingShouBaseMgr+LingShouOverview"
local EnumLingShouGender = import "L10.Game.EnumLingShouGender"

LuaQMPKLingShouLearnWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQMPKLingShouLearnWnd, "LingShouTable", "LingShouTable", QnTableView)
RegistChildComponent(LuaQMPKLingShouLearnWnd, "OpBtn", "OpBtn", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaQMPKLingShouLearnWnd, "m_SelectLingShouId")

function LuaQMPKLingShouLearnWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.OpBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_SelectLingShouId == nil then
            g_MessageMgr:ShowMessage("QMPK_LINGSHOULEARNWND_NOSELECT")
            return
        end

        g_ScriptEvent:BroadcastInLua("QMPKLingShouLearn", self.m_SelectLingShouId)
        CUIManager.CloseUI(CLuaUIResources.QMPKLingShouLearnWnd)
    end)
end

function LuaQMPKLingShouLearnWnd:Init()
    self:InitSelfLingShou()
end

function LuaQMPKLingShouLearnWnd:OnEnable()
    g_ScriptEvent:AddListener("GetAllLingShouOverview", self, "GetAllLingShouOverview")
end
function LuaQMPKLingShouLearnWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GetAllLingShouOverview", self, "GetAllLingShouOverview")
end
--@region UIEvent

--@endregion UIEvent


function LuaQMPKLingShouLearnWnd:InitSelfLingShou()
    self.LingShouTable.m_DataSource = DefaultTableViewDataSource.Create2(function()
            return CLingShouMgr.Inst:GetLingShouOverviewList().Count
    end,
    function(view,row)
        local list = CLingShouMgr.Inst:GetLingShouOverviewList()
        if row < list.Count then
            local item = view:GetFromPool(0)--typeof(CLingShouItem))
            if item ~= nil then
                item:Init(list[row])
                return item
            end
        end
    end)

    self.LingShouTable.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local list = CLingShouMgr.Inst:GetLingShouOverviewList()
        if row < list.Count then
            self.m_SelectLingShouId = list[row].id
        end
    end)

    --请求数据
    CLingShouMgr.Inst:RequestLingShouList()
end

function LuaQMPKLingShouLearnWnd:GetAllLingShouOverview()
    self.LingShouTable:ReloadData(false, false)
end