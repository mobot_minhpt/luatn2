local CHouseStuffMakeWnd=import "L10.UI.CHouseStuffMakeWnd"
local CItemCountUpdate=import "L10.UI.CItemCountUpdate"
local CLivingSkillMgr = import "L10.Game.CLivingSkillMgr"
local CItemMgr=import "L10.Game.CItemMgr"
local CLivingSkillQuickMakeMgr=import "L10.UI.CLivingSkillQuickMakeMgr"

CLuaHouseStuffMakeUseWnd = class()
RegistClassMember(CLuaHouseStuffMakeUseWnd,"itemPrefab")
RegistClassMember(CLuaHouseStuffMakeUseWnd,"grid")
RegistClassMember(CLuaHouseStuffMakeUseWnd,"node")

function CLuaHouseStuffMakeUseWnd:Awake()
    self.itemPrefab = self.transform:Find("Node/Item").gameObject
    self.itemPrefab:SetActive(false)
    self.grid = self.transform:Find("Node/ScrollView/Grid"):GetComponent(typeof(UIGrid))
    self.node = self.transform:Find("Node")
end

function CLuaHouseStuffMakeUseWnd:Init( )
    local keys={}
    LifeSkill_ZhuShaBiNormalMat.ForeachKey(function(key)
        table.insert(keys,key)        
    end)
    for i,v in ipairs(keys) do
        if v == CLivingSkillMgr.Inst.selectedItemId then
            local data = LifeSkill_ZhuShaBiNormalMat.GetData(v)
            local obj = NGUITools.AddChild(self.grid.gameObject,self.itemPrefab)
            obj:SetActive(true)
            self:InitItem(obj.transform,data.Item,data.ItemCount)
            break
        end
    end

    local keys={}
    LifeSkill_Extra.ForeachKey( function(key)
        table.insert(keys,key)        
    end)
    for i,v in ipairs(keys) do
        local obj = NGUITools.AddChild(self.grid.gameObject,self.itemPrefab)
        obj:SetActive(true)
        self:InitItem(obj.transform,v,1)
    end

    self.grid:Reposition()
end

function CLuaHouseStuffMakeUseWnd:InitItem( transform, templateId,needCount )
    local icon = transform:Find("Sprite/Icon"):GetComponent(typeof(CUITexture))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local expLabel = transform:Find("ExpLabel"):GetComponent(typeof(UILabel))
    local countUpdate = transform:GetComponent(typeof(CItemCountUpdate))
    local getNode = transform:Find("Sprite/GetNode")

    expLabel.text = ""
    templateId = templateId
    local template = CItemMgr.Inst:GetItemTemplate(templateId)
    if template ~= nil then
        icon:LoadMaterial(template.Icon)
        nameLabel.text = template.Name
    else
        nameLabel.text = ""
    end

    countUpdate.templateId = templateId
    countUpdate.onChange = DelegateFactory.Action_int(function (val) 
        if val <needCount then
            countUpdate.format = "[ff0000]{0}[-]/"..tostring(needCount)
            getNode.gameObject:SetActive(true)
        else
            countUpdate.format = "{0}/"..tostring(needCount)
            getNode.gameObject:SetActive(false)
        end
    end)
    countUpdate:UpdateCount()

    UIEventListener.Get(transform.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        if countUpdate.count<needCount then
            CLivingSkillQuickMakeMgr.Inst:ShowMakeTipWnd(templateId)
        else
            CHouseStuffMakeWnd.ZhuShaBiUpgradeMat = templateId
            CUIManager.CloseUI(CUIResources.HouseStuffMakeUseWnd)
            g_ScriptEvent:BroadcastInLua("SetZhuShaBiUpgradeMat",templateId)
        end
    end)

end
