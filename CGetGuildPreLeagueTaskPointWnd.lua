-- Auto Generated!!
local BoxCollider = import "UnityEngine.BoxCollider"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGetGuildPreLeagueTaskPointWnd = import "L10.UI.CGetGuildPreLeagueTaskPointWnd"
local CGuildPreLeagueTaskPointMgr = import "L10.UI.CGuildPreLeagueTaskPointMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CTaskMgr = import "L10.Game.CTaskMgr"
local DelegateFactory = import "DelegateFactory"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
CGetGuildPreLeagueTaskPointWnd.m_Init_CS2LuaHook = function (this) 

    this.scoreLabel.text = "0"

    this.TaskAwardPointLabel.text = System.String.Format(LocalString.GetString("+{0}点/次"), CGuildPreLeagueTaskPointMgr.TaskAwardPoint)
    this.MenPaiAwardPointLabel.text = System.String.Format(LocalString.GetString("+{0}点"), CGuildPreLeagueTaskPointMgr.MenPaiAwardPoint)
    this.ZhanLongAwardPointLabel.text = System.String.Format(LocalString.GetString("+{0}点"), CGuildPreLeagueTaskPointMgr.ZhanLongAwardPoint)

    --门派挑战
    --查看备战任务是否领取
    --查看战龙堂任务是否领取
    if CClientMainPlayer.Inst ~= nil then
        local taskProperty = CClientMainPlayer.Inst.TaskProp
        --已经领取
        if CommonDefs.DictContains(taskProperty.CurrentTasks, typeof(UInt32), CGuildPreLeagueTaskPointMgr.BeiZhanTaskId) then
            this:ShowAccept(this.TaskAwardPointItem)
            CommonDefs.GetComponent_GameObject_Type(this.TaskAwardPointItem, typeof(BoxCollider)).enabled = false
        end
        if CommonDefs.DictContains(taskProperty.CurrentTasks, typeof(UInt32), CGuildPreLeagueTaskPointMgr.MenPaiTaskId) then
            this:ShowAccept(this.MenPaiAwardPointItem)
            CommonDefs.GetComponent_GameObject_Type(this.MenPaiAwardPointItem, typeof(BoxCollider)).enabled = false
        end
        if CommonDefs.DictContains(taskProperty.CurrentTasks, typeof(UInt32), CGuildPreLeagueTaskPointMgr.ZhanLongTaskId) then
            this:ShowAccept(this.ZhanLongAwardPointItem)
            CommonDefs.GetComponent_GameObject_Type(this.ZhanLongAwardPointItem, typeof(BoxCollider)).enabled = false
        end
    end
    Gac2Gas.QueryGuildPreLeagueTaskPersonalPoint()
end
CGetGuildPreLeagueTaskPointWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.TaskAwardPointItem).onClick = DelegateFactory.VoidDelegate(function (p) 
        CTaskMgr.Inst:TrackToDoTask(CGuildPreLeagueTaskPointMgr.BeiZhanTaskId)
        this:Close()
    end)
    UIEventListener.Get(this.MenPaiAwardPointItem).onClick = DelegateFactory.VoidDelegate(function (p) 
        CTaskMgr.Inst:TrackToDoTask(CGuildPreLeagueTaskPointMgr.MenPaiTaskId)
        --CUIManager.ShowUI(CUIResources.PVPWnd);
        this:Close()
    end)
    UIEventListener.Get(this.ZhanLongAwardPointItem).onClick = DelegateFactory.VoidDelegate(function (p) 
        CTaskMgr.Inst:TrackToDoTask(CGuildPreLeagueTaskPointMgr.ZhanLongTaskId)
        this:Close()
    end)
    UIEventListener.Get(this.BeiZhanRMBItem).onClick = DelegateFactory.VoidDelegate(function (p) 
        --CYuanbaoMarketMgr.ShowPlayerShop(CGuildPreLeagueTaskPointMgr.BeiZhanRMBItemId);
        CShopMallMgr.ShowLinyuShoppingMall(CGuildPreLeagueTaskPointMgr.BeiZhanRMBItemId)
        this:Close()
    end)

    this:ShowGoto(this.TaskAwardPointItem)
    this:ShowGoto(this.MenPaiAwardPointItem)
    this:ShowGoto(this.ZhanLongAwardPointItem)
end
