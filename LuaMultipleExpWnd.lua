local QnCheckBox = import "L10.UI.QnCheckBox"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CPropertyItem = import "L10.Game.CPropertyItem"
local Physics = import "UnityEngine.Physics"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local Object = import "System.Object"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltip = import "L10.UI.CTooltip"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CWelfareAlertCtl = import "L10.UI.CWelfareAlertCtl"

LuaMultipleExpWnd = class()

RegistClassMember(LuaMultipleExpWnd,"doubleExp")
RegistClassMember(LuaMultipleExpWnd,"multiExp")
RegistClassMember(LuaMultipleExpWnd,"tipsButton")
RegistClassMember(LuaMultipleExpWnd,"settingBtn")
RegistClassMember(LuaMultipleExpWnd,"settingPanel")
RegistClassMember(LuaMultipleExpWnd,"Table")
RegistClassMember(LuaMultipleExpWnd,"CheckBox1")
RegistClassMember(LuaMultipleExpWnd,"CheckBox2")

RegistClassMember(LuaMultipleExpWnd, "DoubleBtn")
RegistClassMember(LuaMultipleExpWnd, "TripleBtn")
RegistClassMember(LuaMultipleExpWnd, "DoubleWnd")
RegistClassMember(LuaMultipleExpWnd, "TripleWnd")

RegistClassMember(LuaMultipleExpWnd, "CurBtn")

RegistClassMember(LuaMultipleExpWnd, "BaoLingDanBtn")
RegistClassMember(LuaMultipleExpWnd, "NiLiuPingPanel")

function LuaMultipleExpWnd:Awake()
    self.doubleExp = self.transform:Find("MultipleExp/Anchor/doubleExp").gameObject
    self.multiExp = self.transform:Find("MultipleExp/Anchor/multiExp").gameObject
    self.tipsButton = self.transform:Find("MultipleExp/Anchor/tipsButton").gameObject
    self.settingBtn = self.transform:Find("MultipleExp/Anchor/settingBtn").gameObject
    self.settingPanel = self.transform:Find("MultipleExp/Anchor/settingPanel").gameObject
    self.Table = self.transform:Find("MultipleExp/Anchor/settingPanel/node/Table"):GetComponent(typeof(UITable))
    self.CheckBox1 = self.transform:Find("MultipleExp/Anchor/settingPanel/node/Table/CheckBox1"):GetComponent(typeof(QnCheckBox))
    self.CheckBox2 = self.transform:Find("MultipleExp/Anchor/settingPanel/node/Table/CheckBox2"):GetComponent(typeof(QnCheckBox))

    self.DoubleBtn = self.transform:Find("MultipleExp/Anchor/multiExp/DoubleBtn")
    self.TripleBtn = self.transform:Find("MultipleExp/Anchor/multiExp/TripleBtn")
    self.DoubleWnd = self.transform:Find("MultipleExp/Anchor/multiExp/Double")
    self.TripleWnd = self.transform:Find("MultipleExp/Anchor/multiExp/Triple")

    self.BaoLingDanBtn = self.transform:Find("AnswerExp/ExpItem").gameObject
    self.NiLiuPingPanel = self.transform:Find("AnswerExp/Panel").gameObject
end

function LuaMultipleExpWnd:OnEnable()
    g_ScriptEvent:AddListener("OnQueryDoubleExpInfoReturn", self, "UpdateAlert")
    g_ScriptEvent:AddListener("SendItem", self, "InitAnswerExp")
	g_ScriptEvent:AddListener("SetItemAt", self, "InitAnswerExp")

    UIEventListener.Get(self.tipsButton).onClick =  DelegateFactory.VoidDelegate(function(go) 
        if self.CurBtn == self.DoubleBtn then
            g_MessageMgr:ShowMessage("DoubleExp_Tip") 
        elseif self.CurBtn == self.TripleBtn then
            g_MessageMgr:ShowMessage("TripleExp_Tips") 
        end
    end)
    UIEventListener.Get(self.settingBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:InitSettingPanel(go) end)

    if CClientMainPlayer.Inst == nil then
        return
    end
    self.multiExp:SetActive(CClientMainPlayer.Inst.HasFeiSheng)
    self.doubleExp:SetActive(not CClientMainPlayer.Inst.HasFeiSheng)
    self.CheckBox1.OnValueChanged = DelegateFactory.Action_bool(function(check) self:OnUseTripleChanged(check) end)
    self.CheckBox2.OnValueChanged = DelegateFactory.Action_bool(function(check) self:OnUseDoubleChanged(check) end)
    --Gac2Gas.RequestMultipleExpInfo()

    if self.DoubleBtn then
        self.DoubleBtn:GetComponent(typeof(QnSelectableButton)).OnClick = DelegateFactory.Action_QnButton(function(btn) self:OnTabClicked(btn.transform) end)
    end

    if self.TripleBtn then
        self.TripleBtn:GetComponent(typeof(QnSelectableButton)).OnClick = DelegateFactory.Action_QnButton(function(btn) self:OnTabClicked(btn.transform) end)
    end

    local getFreeDoubleExp = CPropertyItem.IsSamePeriod(CSigninMgr.Inst.doubleExpReceiveTime, "w")
    --local tripleItemTId = MultipleExp_GameSetting.GetData().Triple_Item_TemplateId
    --local haveTripleExpCount = self:GetTripleItemCount()
    if self.CurBtn then
        self:OnTabClicked(self.CurBtn)
    else
        local tripleBuff = CSigninMgr.Inst:IsPlayerUsingMultipleExp(MultipleExp_GameSetting.GetData().Triple_Show_BuffId)
        if not getFreeDoubleExp then
            if self.DoubleBtn then
                self:OnTabClicked(self.DoubleBtn)
            end
        elseif tripleBuff then
            if self.TripleBtn then
                self:OnTabClicked(self.TripleBtn)
            end
        else
            if self.DoubleBtn then
                self:OnTabClicked(self.DoubleBtn)
            end
        end
    end

    self:InitAnswerExp()
end

function LuaMultipleExpWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnQueryDoubleExpInfoReturn", self, "UpdateAlert")
    g_ScriptEvent:RemoveListener("SendItem", self, "InitAnswerExp")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "InitAnswerExp")
end

function LuaMultipleExpWnd:UpdateAlert()
    if self.CurBtn then
        self:OnTabClicked(self.CurBtn)
    end
    if not CommonDefs.IsNull(LuaWelfareMgr.MultipleExpTabBtn) then
        CommonDefs.GetComponent_GameObject_Type(LuaWelfareMgr.MultipleExpTabBtn.gameObject, typeof(CWelfareAlertCtl)):ShowAlert(LuaWelfareMgr:CheckMultipleExpAlert())
    end
end

function LuaMultipleExpWnd:Update()
    if (self.settingPanel.activeSelf or self.NiLiuPingPanel.activeSelf) and Input.GetMouseButtonDown(0) then
        local cam = UICamera.currentCamera
        local hits = Physics.RaycastAll(cam:ScreenPointToRay(Input.mousePosition), cam.farClipPlane, cam.cullingMask)
        if hits then
            local clickSetting, clickNiLiuPing = false, false
            for i = 0, hits.Length - 1 do
                if not clickSetting and NGUITools.IsChild(self.settingPanel.transform, hits[i].collider.transform) then
                    clickSetting = true
                end
                if not clickNiLiuPing and NGUITools.IsChild(self.NiLiuPingPanel.transform, hits[i].collider.transform) then
                    clickNiLiuPing = true
                end
            end
            if not clickSetting then
                self.settingPanel:SetActive(false)
            end
            if not clickNiLiuPing then
                self.NiLiuPingPanel:SetActive(false)
            end
        end
    end
end

function LuaMultipleExpWnd:OnTabClicked(go)
    self.CurBtn = go

    local isDouble = go.gameObject == self.DoubleBtn.gameObject
    local isTriple = go.gameObject == self.TripleBtn.gameObject
    self.DoubleWnd.gameObject:SetActive(isDouble)
    self.TripleWnd.gameObject:SetActive(isTriple)

    self.DoubleBtn:GetComponent(typeof(QnSelectableButton)):SetSelected(isDouble, false)
    self.TripleBtn:GetComponent(typeof(QnSelectableButton)):SetSelected(isTriple, false)
end

function LuaMultipleExpWnd:OnUseTripleChanged(check)
    local arg = check and "1" or "0"
    Gac2Gas.SetGameSettingInfo(EnumGameSetting_lua.eTripleExpAutoUseForYiTiaoLong, arg)
    if check then
        self.CheckBox2:SetSelected(not check, false)
    end
end

function LuaMultipleExpWnd:OnUseDoubleChanged(check)
    local arg = check and "1" or "0"
    Gac2Gas.SetGameSettingInfo(EnumGameSetting_lua.eMultipleExpAutoUseForYiTiaoLong, arg)
    if check then
        self.CheckBox1:SetSelected(not check, false)
    end
end

function LuaMultipleExpWnd:InitSettingPanel( go)
    self.settingPanel:SetActive(true)

    self.CheckBox1.Visible = CClientMainPlayer.Inst.HasFeiSheng
    self.CheckBox2.Visible = true
    self.Table:Reposition()

    local gameSetting = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eCommonGameSetting)
    local settingDic = nil 
    if gameSetting then
        settingDic = TypeAs(MsgPackImpl.unpack(gameSetting.Data), typeof(MakeGenericClass(Dictionary, String, Object)))
    end

    --双倍
    local skey = tostring((EnumGameSetting_lua.eMultipleExpAutoUseForYiTiaoLong))
    if settingDic and CommonDefs.DictContains(settingDic, typeof(String), skey) then
        local key = CommonDefs.DictGetValue(settingDic, typeof(String), skey)
        local value =  (System.Int32.Parse(ToStringWrap(key)) == 1)
        self.CheckBox2:SetSelected(value, true)
        if value then
            self.CheckBox1:SetSelected(false, true)
        end
    else
        self.CheckBox2:SetSelected(false, true)
    end

    --三倍
    skey = tostring((EnumGameSetting_lua.eTripleExpAutoUseForYiTiaoLong))
    if settingDic and CommonDefs.DictContains(settingDic, typeof(String), skey) then
        local key = CommonDefs.DictGetValue(settingDic, typeof(String), skey)
        local value = (System.Int32.Parse(ToStringWrap(key)) == 1)
        self.CheckBox1:SetSelected(value, true)
        if value then
            self.CheckBox2:SetSelected(false, true)
        end
    else
        self.CheckBox1:SetSelected(false, true)
    end
end

function LuaMultipleExpWnd:InitAnswerExp()
    if not CClientMainPlayer.Inst then return end
    local baoLingDanItemId, niLiuPingItemId = CClientMainPlayer.Inst.HasFeiSheng and 21004564 or 21000709, 21000716
    local baoLingDan, niLiuPing = Item_Item.GetData(baoLingDanItemId), Item_Item.GetData(niLiuPingItemId)
    local baoLingDanUseTimes, niLiuPingUseTimes = 
        CClientMainPlayer.Inst.ItemProp:GetItemUseTimes(baoLingDan.TimesGroup, baoLingDan.ItemPeriod), 
        CClientMainPlayer.Inst.ItemProp:GetItemUseTimes(niLiuPing.TimesGroup, niLiuPing.ItemPeriod)
    -- 服务器增加使用次数上限的方式是减少当前使用次数（可能减到负值）
    baoLingDanUseTimes = baoLingDanUseTimes + niLiuPingUseTimes
    local baoLingDanCount, niLiuPingCount = CItemMgr.Inst:GetItemCount(baoLingDanItemId), CItemMgr.Inst:GetItemCount(niLiuPingItemId)
    
    self:InitBLD(baoLingDanItemId, baoLingDanUseTimes, baoLingDan.TimesPerDay + niLiuPingUseTimes, niLiuPingUseTimes)
    self:InitNLP(niLiuPingItemId, niLiuPingCount)

    UIEventListener.Get(self.transform:Find("AnswerExp/TipBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function(go) g_MessageMgr:ShowMessage("Welfare_AnswerExp_Tip") end)
    self.transform:Find("AnswerExp/Hint"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("Welfare_AnswerExp_ItemInstruction")
    
    local useBtn = self.transform:Find("AnswerExp/UseBtn").gameObject
    local limitLb = self.transform:Find("AnswerExp/LimitLabel"):GetComponent(typeof(UILabel))
    useBtn:SetActive(baoLingDanUseTimes < 3)
    if CommonDefs.IsSeaMultiLang() then
        useBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("使用") .. " " ..baoLingDan.Name
    else
        useBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("使用")..baoLingDan.Name
    end
    limitLb.gameObject:SetActive(baoLingDanUseTimes >= 3)
    UIEventListener.Get(useBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        if baoLingDanCount > 0 then
            if baoLingDanUseTimes >= baoLingDan.TimesPerDay + niLiuPingUseTimes then
                g_MessageMgr:ShowOkCancelMessage(LocalString.GetString("是否使用逆流瓶增加次数？"), function()
                    if niLiuPingCount == 0 then
                        CItemAccessListMgr.Inst:ShowItemAccessInfo(niLiuPingItemId, false, nil, CTooltip.AlignType.Right)
                    else
                        local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, niLiuPingItemId)
                        if default and pos and itemId then
                            Gac2Gas.RequestUseItem(EnumItemPlace.Bag, pos, itemId, "")
                        end
                    end
                end, nil, LocalString.GetString("确认"), LocalString.GetString("取消"), false)
            else
                local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, baoLingDanItemId)
                if default and pos and itemId then
                    Gac2Gas.RequestUseItem(EnumItemPlace.Bag, pos, itemId, "")
                end
            end
        else
            g_MessageMgr:ShowCustomMsg(baoLingDan.Name..LocalString.GetString("不足"))
            CItemAccessListMgr.Inst:ShowItemAccessInfo(baoLingDanItemId, false, nil, CTooltip.AlignType.Right)
        end
    end)
end

function LuaMultipleExpWnd:InitBLD(itemID, useTimes, limitTimes, addTimes)
    local curItem = self.BaoLingDanBtn
    local texture = curItem.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local amountLb = curItem.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
    local count = CItemMgr.Inst:GetItemCount(itemID)
    amountLb.text = count
    amountLb.color = count == 0 and Color.red or Color.white
    curItem.transform:Find("Got").gameObject:SetActive(count == 0)
    local ItemData = Item_Item.GetData(itemID)
    if ItemData then 
        texture:LoadMaterial(ItemData.Icon) 
        curItem.transform:Find("Name"):GetComponent(typeof(UILabel)).text = ItemData.Name
        if CClientMainPlayer.Inst then
            local useTimesLb = curItem.transform:Find("UseTimes/Label"):GetComponent(typeof(UILabel))
            useTimesLb.color = Color.white
            useTimesLb.text = "[0E3254]"..(useTimes == limitTimes and "[FF0000]" or "")..useTimes.."[0E3254]/"..limitTimes
            local incBtn = curItem.transform:Find("UseTimes/IncreaseButton").gameObject
            if addTimes < 2 then
                CUICommonDef.SetActive(incBtn, true, true)
                UIEventListener.Get(incBtn).onClick = DelegateFactory.VoidDelegate(function(go)
                    self.NiLiuPingPanel:SetActive(true)
                end)
            else
                CUICommonDef.SetActive(incBtn, false, true)
            end
        end
    end
    UIEventListener.Get(curItem).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, count == 0 and DefaultItemActionDataSource.Create(1, {function ( ... )
            CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
            CItemAccessListMgr.Inst:ShowItemAccessInfo(itemID, false, nil, CTooltip.AlignType.Right)
        end}, {LocalString.GetString("获取")}) or nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
end

function LuaMultipleExpWnd:InitNLP(itemID, count)
    local curItem = self.NiLiuPingPanel.transform
    curItem:Find("Bg/Hint"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("Welfare_UseNiLiuPing_Instruction")
    local item = curItem:Find("Bg/ClickArea/Item")
    local texture = item:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local amountLb = item:Find("AmountLabel"):GetComponent(typeof(UILabel))
    amountLb.text = count
    amountLb.color = count == 0 and Color.red or Color.white
    item:Find("Got").gameObject:SetActive(count == 0)
    local ItemData = Item_Item.GetData(itemID)
    if ItemData then 
        texture:LoadMaterial(ItemData.Icon) 
        item:Find("Name"):GetComponent(typeof(UILabel)).text = ItemData.Name
    end
    UIEventListener.Get(curItem:Find("Bg/ClickArea").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if count > 0 then
            local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, itemID)
            if default and pos and itemId then
                Gac2Gas.RequestUseItem(EnumItemPlace.Bag, pos, itemId, "")
            end
        else
            CItemAccessListMgr.Inst:ShowItemAccessInfo(itemID, false, nil, CTooltip.AlignType.Right)
        end
    end)
end
