require("3rdParty/ScriptEvent")
require("common/common_include")

local Extensions = import "Extensions"
local UITable = import "UITable"
local NGUITools = import "NGUITools"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local Profession = import "L10.Game.Profession"
local Constants = import "L10.Game.Constants"
local UIEventListener = import "UIEventListener"
local LuaUtils = import "LuaUtils"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"

CLuaCrossSXDDZChallengerListWnd = class()
RegistClassMember(CLuaCrossSXDDZChallengerListWnd, "m_Item2PlayerIdTable")
RegistClassMember(CLuaCrossSXDDZChallengerListWnd, "m_Item2SpriteNameTable")

function CLuaCrossSXDDZChallengerListWnd:Init( ... )
	if not CLuaCrossSXDDZWnd.ChallengerList then
		CUIManager.CloseUI(CLuaUIResources.CrossSXDDZChallengerListWnd)
		return
	end

	local table = self.transform:Find("Scroll View/Table"):GetComponent(typeof(UITable))
	local templateObj = self.transform:Find("Template").gameObject
	templateObj:SetActive(false)
	Extensions.RemoveAllChildren(table.transform)
	self.m_Item2PlayerIdTable = {}
	self.m_Item2SpriteNameTable = {}
	for i = 0, CLuaCrossSXDDZWnd.ChallengerList.Count - 1 do
		-- champion
		if CLuaCrossSXDDZWnd.ChallengerList[i][6] ~= 2 then
			local candidate = CLuaCrossSXDDZWnd.ChallengerList[i]
			local obj = NGUITools.AddChild(table.gameObject, templateObj)
			local spriteName = i % 2 == 0 and Constants.GreyItemBgSpriteName or Constants.WhiteItemBgSpriteName
			obj:GetComponent(typeof(UISprite)).spriteName = spriteName
			obj:SetActive(true)
			obj.transform:Find("Name"):GetComponent(typeof(UILabel)).text = CLuaCrossSXDDZWnd.ChallengerList[i][1]
			obj.transform:Find("Level"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("[c][%s]Lv.%s[-][/c]", CLuaCrossSXDDZWnd.ChallengerList[i][5] > 0 and Constants.ColorOfFeiSheng or "131838", CLuaCrossSXDDZWnd.ChallengerList[i][2])
			obj.transform:Find("Sprite"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(CLuaCrossSXDDZWnd.ChallengerList[i][4])
			obj.transform:Find("Server"):GetComponent(typeof(UILabel)).text = CLuaCrossSXDDZWnd.ChallengerList[i][3]
			obj.transform:Find("OutFlag").gameObject:SetActive(CLuaCrossSXDDZWnd.ChallengerList[i][6] ~= 1)
			UIEventListener.Get(obj).onClick = LuaUtils.VoidDelegate(function (go)
				self:OnClick(go)
			end)
			self.m_Item2PlayerIdTable[obj] = CLuaCrossSXDDZWnd.ChallengerList[i][0]
			self.m_Item2SpriteNameTable[obj] = spriteName
		end
	end
	table:Reposition()
end

function CLuaCrossSXDDZChallengerListWnd:OnClick(go)
	for k, v in pairs(self.m_Item2PlayerIdTable) do
		if k == go then
			k:GetComponent(typeof(UISprite)).spriteName = Constants.ChosenItemBgSpriteName
			CPlayerInfoMgr.ShowPlayerPopupMenu(v, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, Vector3.zero, AlignType.Default)
		else
			k:GetComponent(typeof(UISprite)).spriteName = self.m_Item2SpriteNameTable[k]
		end
	end
end

return CLuaCrossSXDDZChallengerListWnd