
LuaFruitPartyMgr = {}

LuaFruitPartyMgr.m_MaxLianJi = 0
LuaFruitPartyMgr.m_Score = 0
LuaFruitPartyMgr.m_MaxScore = 0
LuaFruitPartyMgr.m_Rewards = {}
LuaFruitPartyMgr.m_GotRewards = {}

function LuaFruitPartyMgr:ShowShuiGuoPaiDuiResultWnd(maxLianJi, score, rewards, gotRewards)
    self.m_MaxLianJi = maxLianJi
    self.m_Score = score
    self.m_Rewards = rewards
    self.m_GotRewards = gotRewards
    CUIManager.ShowUI(CLuaUIResources.FruitPartyResultWnd)
end

function LuaFruitPartyMgr:SyncShuiGuoPaiDuiScore(score)
    if CClientMainPlayer.Inst then
        local deltaScore = score - LuaFruitPartyMgr.m_Score
        if deltaScore > 0 then
            CClientMainPlayer.Inst.RO:ShowScoreTextWithSize(deltaScore, Color.green, 0.6)
        end
    end
    LuaFruitPartyMgr.m_Score = score
	g_ScriptEvent:BroadcastInLua("SyncShuiGuoPaiDuiScore", score)
end
--师徒玩法复用水果派对后新增接口
function LuaFruitPartyMgr:InitShuiGuoPaiDuiScore(score, maxScore)
    self.m_Score = score
    self.m_MaxScore = maxScore
    g_ScriptEvent:BroadcastInLua("InitShuiGuoPaiDuiScore", score, maxScore)
end

function LuaFruitPartyMgr.IsInFruitParty()
	if not CClientMainPlayer.Inst then return false end
	local setting = ShuiGuoPaiDui_Setting.GetData()
	return CClientMainPlayer.Inst.PlayProp.PlayId == setting.GamePlayId
end