local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local CActivityLeftTimeWnd = import "L10.UI.CActivityLeftTimeWnd"

LuaActivityAlertItem = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistClassMember(LuaActivityAlertItem, "m_IconSpriteBg")
RegistClassMember(LuaActivityAlertItem, "m_IconSprite")
RegistClassMember(LuaActivityAlertItem, "m_IconTexture")
RegistClassMember(LuaActivityAlertItem, "m_CountLabel")

RegistClassMember(LuaActivityAlertItem, "bDisableInCrossServer")
RegistClassMember(LuaActivityAlertItem, "m_ActivityType")
RegistClassMember(LuaActivityAlertItem, "m_OpenWnd")

--@endregion RegistChildComponent end

function LuaActivityAlertItem:Awake()
    self.m_IconSpriteBg = self.transform:Find("Sprite").gameObject
    self.m_IconSprite = self.transform:Find("Sprite/Icon"):GetComponent(typeof(UISprite))
    self.m_IconTexture = self.transform:Find("Texture"):GetComponent(typeof(CUITexture))
    self.m_CountLabel = self.transform:Find("Label"):GetComponent(typeof(UILabel))

    self.bDisableInCrossServer = true
    UIEventListener.Get(self.gameObject).onClick  =DelegateFactory.VoidDelegate(function(go)
        if self.bDisableInCrossServer then
            CUICommonDef.CallMethodConsiderCrossServer(LuaUtils.Action(function()
                self:DoClick()
            end),{})
        else
            self:DoClick()
        end
    end)
end

function LuaActivityAlertItem:Init(info)
    local data = Task_QueryNpc.GetData(info.activityType)
    if data then
        if not System.String.IsNullOrEmpty(data.Icon) then
            self.m_IconSpriteBg:SetActive(true)
            self.m_IconTexture.gameObject:SetActive(false)
            self.m_IconSprite.spriteName = data.Icon
        elseif not System.String.IsNullOrEmpty(data.IconTexture) then
            self.m_IconSpriteBg:SetActive(false)
            self.m_IconTexture.gameObject:SetActive(true)
            self.m_IconTexture:LoadMaterial(data.IconTexture)
        end
        if data.HideNumbers == 1 then
            self.m_CountLabel.gameObject:SetActive(false)
        else
            self.m_CountLabel.gameObject:SetActive(true)
            self.m_CountLabel.text = string.format(LocalString.GetString("余%d"), info.count)
        end
        self.m_ActivityType = info.activityType
        self.m_OpenWnd = data.OpenWnd
    end
end

--@region UIEvent
function LuaActivityAlertItem:DoClick()
    if self.m_OpenWnd then
        CActivityLeftTimeWnd.activityType = self.m_ActivityType
        CUIManager.ShowUI(self.m_OpenWnd)
    end
end
--@endregion UIEvent

