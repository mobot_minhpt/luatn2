local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UIScrollView  = import "UIScrollView"
local UITable = import "UITable"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local UITabBar = import "L10.UI.UITabBar"

LuaGuildLeagueCrossGambleServerInfoWnd = class()
RegistChildComponent(LuaGuildLeagueCrossGambleServerInfoWnd, "m_TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaGuildLeagueCrossGambleServerInfoWnd, "m_TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaGuildLeagueCrossGambleServerInfoWnd, "m_HeaderForServer", "HeaderForServer", GameObject)
RegistChildComponent(LuaGuildLeagueCrossGambleServerInfoWnd, "m_HeaderForScore", "HeaderForScore", GameObject)
RegistChildComponent(LuaGuildLeagueCrossGambleServerInfoWnd, "m_ItemForServerTemplate", "ItemForServer", GameObject)
RegistChildComponent(LuaGuildLeagueCrossGambleServerInfoWnd, "m_ItemForScoreTemplate", "ItemForScore", GameObject)
RegistChildComponent(LuaGuildLeagueCrossGambleServerInfoWnd, "m_ScrollView", "ScrollView", UIScrollView)
RegistChildComponent(LuaGuildLeagueCrossGambleServerInfoWnd, "m_Table", "Table", UITable)

RegistClassMember(LuaGuildLeagueCrossGambleServerInfoWnd, "m_ServerGuildInfo") --记录下，来本次打开界面不再刷新数据
RegistClassMember(LuaGuildLeagueCrossGambleServerInfoWnd, "m_ServerScoreInfo") --记录下，来本次打开界面不再刷新数据

function LuaGuildLeagueCrossGambleServerInfoWnd:Awake()
    self.m_ItemForServerTemplate:SetActive(false)
    self.m_ItemForScoreTemplate:SetActive(false)

    self.m_TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(
        function(go, index)
            self:OnTabChange(go, index)
        end
    )
end

function LuaGuildLeagueCrossGambleServerInfoWnd:Init()
    self.m_ServerGuildInfo = LuaGuildLeagueCrossMgr.m_ServerGuildInfo
    self.m_TitleLabel.text = self.m_ServerGuildInfo.serverName
    self.m_TabBar:ChangeTab(0, false)
end

function LuaGuildLeagueCrossGambleServerInfoWnd:OnTabChange(go, index)
    if index == 0 then
        self.m_HeaderForServer:SetActive(true)
        self.m_HeaderForScore:SetActive(false)
        self:ShowServerView()
    elseif index == 1 then
        self.m_HeaderForServer:SetActive(false)
        self.m_HeaderForScore:SetActive(true)
        self:ShowScoreView()
    end
end

function LuaGuildLeagueCrossGambleServerInfoWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSendGuildLeagueCrossServerMatchRecord", self, "OnSendGuildLeagueCrossServerMatchRecord")
end

function LuaGuildLeagueCrossGambleServerInfoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSendGuildLeagueCrossServerMatchRecord", self, "OnSendGuildLeagueCrossServerMatchRecord")
end

function LuaGuildLeagueCrossGambleServerInfoWnd:OnSendGuildLeagueCrossServerMatchRecord(recordTbl)
    self.m_ServerScoreInfo = recordTbl
    if self.m_TabBar.SelectedIndex == 1 then
        self:ShowScoreView()
    end
end

function LuaGuildLeagueCrossGambleServerInfoWnd:ShowServerView()
    local data = self.m_ServerGuildInfo.detailInfo
    Extensions.RemoveAllChildren(self.m_Table.transform)

    for i=1, #data do
        local child = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_ItemForServerTemplate)
        child:SetActive(true)
        local info = data[i]
        self:InitOneGuildInfo(child.transform, info.guildName, info.ownerName, info.professionIcon, info.memberCount, info.maxMemberCount, info.rankStr, info.score)
        if i % 2 == 1 then
            child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.OddBgSpirite)
        else
            child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.EvenBgSprite)
        end
    end
    self.m_Table:Reposition()
    self.m_ScrollView:ResetPosition()
end

function LuaGuildLeagueCrossGambleServerInfoWnd:InitOneGuildInfo(transRoot, guildName, ownerName, professionIcon, memberCount, maxMemberCount, rankStr, score)
    transRoot:Find("GuildNameLabel"):GetComponent(typeof(UILabel)).text = guildName
    transRoot:Find("OwnerNameLabel"):GetComponent(typeof(UILabel)).text = ownerName
    transRoot:Find("OwnerNameLabel/ProfessionIcon"):GetComponent(typeof(UISprite)).spriteName = professionIcon
    transRoot:Find("MemberCountLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("%d/%d", memberCount, maxMemberCount)
    transRoot:Find("RankLabel"):GetComponent(typeof(UILabel)).text = rankStr
    transRoot:Find("EquipScoreLabel"):GetComponent(typeof(UILabel)).text = tostring(score)
end

function LuaGuildLeagueCrossGambleServerInfoWnd:ShowScoreView()
    Extensions.RemoveAllChildren(self.m_Table.transform)
    if self.m_ServerScoreInfo == nil then
        LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossServerMatchRecord(self.m_ServerGuildInfo.serverId)
        return 
    end

    for i,record in pairs(self.m_ServerScoreInfo) do
        local child = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_ItemForScoreTemplate)
        child:SetActive(true)
        
        local resultIcon = child.transform:Find("ResultIcon"):GetComponent(typeof(UISprite))
        local roundNameLabel = child.transform:Find("RoundNameLabel"):GetComponent(typeof(UILabel))
        local name1Label = child.transform:Find("Name1Label"):GetComponent(typeof(UILabel))
        local score1Label = child.transform:Find("Score1Label"):GetComponent(typeof(UILabel))
        local name2Label = child.transform:Find("Name2Label"):GetComponent(typeof(UILabel))
        local score2Label = child.transform:Find("Score2Label"):GetComponent(typeof(UILabel))
        local detailButton = child.transform:Find("DetailButton").gameObject
        local isWin = (record.winServerId == record.serverId1)
        resultIcon.spriteName =  isWin and g_sprites.CommonWinSpriteName or g_sprites.CommonLoseSpriteName
        CUICommonDef.SetGrey(resultIcon.gameObject, not isWin)
        roundNameLabel.text = self:GetRoundName(record.round)
        name1Label.text = record.serverName1
        score1Label.text = tostring(record.serverScore1)
        name2Label.text = record.serverName2
        score2Label.text = tostring(record.serverScore2)
        UIEventListener.Get(detailButton).onClick = DelegateFactory.VoidDelegate(function() 
            LuaGuildLeagueCrossMgr:ShowServerMatchDetailInfoWnd(record.matchIndex)
        end)      

        if i % 2 == 1 then
            child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.OddBgSpirite)
        else
            child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.EvenBgSprite)
        end
    end
    self.m_Table:Reposition()
    self.m_ScrollView:ResetPosition()
end

function LuaGuildLeagueCrossGambleServerInfoWnd:GetRoundName(round)
    if round==1 then return LocalString.GetString("小组赛第一轮") end
    if round==2 then return LocalString.GetString("小组赛第二轮") end
    if round==3 then return LocalString.GetString("小组赛第三轮") end
    if round==4 then return LocalString.GetString("排位赛第一轮") end
    if round==5 then return LocalString.GetString("排位赛第二轮") end
    if round==6 then return LocalString.GetString("排位赛第三轮") end
    if round==7 then return LocalString.GetString("决赛") end
    return ""
end

