local LuaGameObject=import "LuaGameObject"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CCommonItemSelectCellData = import "L10.UI.CCommonItemSelectCellData"
local CItemMgr=import "L10.Game.CItemMgr"
local MessageMgr = import "L10.Game.MessageMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local Constants = import "L10.Game.Constants"

LuaBQPMySponsorWnd=class()

RegistClassMember(LuaBQPMySponsorWnd,"ItemToAdd")
RegistClassMember(LuaBQPMySponsorWnd,"ItemTemplate")
RegistClassMember(LuaBQPMySponsorWnd,"InputButton")
RegistClassMember(LuaBQPMySponsorWnd,"MoneyCtrl")
RegistClassMember(LuaBQPMySponsorWnd,"SponsorBtn")
RegistClassMember(LuaBQPMySponsorWnd,"CancelBtn")

RegistClassMember(LuaBQPMySponsorWnd,"SelectedItemId")
RegistClassMember(LuaBQPMySponsorWnd,"SelectedTemplateId")


function LuaBQPMySponsorWnd:Init()
	self.SelectedItemId = nil
	self.SelectedTemplateId = nil

	self.ItemToAdd = self.transform:Find("Anchor/Middle/ItemToAdd").gameObject
	self.ItemTemplate = self.transform:Find("Anchor/Middle/Item").gameObject
	self.InputButton = self.transform:Find("Anchor/Middle/QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
	self.MoneyCtrl = self.transform:Find("Anchor/Middle/QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
	self.SponsorBtn = self.transform:Find("Anchor/Bottom/SponsorBtn").gameObject
	self.CancelBtn = self.transform:Find("Anchor/Bottom/CancelBtn").gameObject

	self.MoneyCtrl:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, false)

	local onInputChanged = function (num)
		self.MoneyCtrl:SetCost(num * 10000)
	end
	self.InputButton.onValueChanged = DelegateFactory.Action_uint(onInputChanged)

	local max = math.min(math.floor(CClientMainPlayer.Inst.Silver / 10000), 200000)
	self.InputButton:SetMinMax(0, max, 1)
	self.InputButton:SetValue(0)


	local onSponsor = function (go)
		self:OnSponsorBtnClick(go)
	end
	CommonDefs.AddOnClickListener(self.SponsorBtn,DelegateFactory.Action_GameObject(onSponsor),false)

	local onCancel = function (go)
		self:OnCancelBtnClick(go)
	end
	CommonDefs.AddOnClickListener(self.CancelBtn,DelegateFactory.Action_GameObject(onCancel),false)

	local onAdd = function (go)
		self:OnAddItemClick(go)
	end
	CommonDefs.AddOnClickListener(self.ItemToAdd,DelegateFactory.Action_GameObject(onAdd),false)
	CommonDefs.AddOnClickListener(self.ItemTemplate,DelegateFactory.Action_GameObject(onAdd),false)

	if LuaBingQiPuMgr.m_SelectSponsorItem then
		self:OnSelectEquip(LuaBingQiPuMgr.m_SelectSponsorItem.ID,LuaBingQiPuMgr.m_SelectSponsorItem.CfgID)
	end

	Gac2Gas.QueryMyBQPForSubmit()
end

function LuaBQPMySponsorWnd:OnSponsorBtnClick(go)
	if self.InputButton:GetValue() == 0 then
		MessageMgr.Inst:ShowMessage("BQP_ZANZHU_NOT_ZERO", {})
		return
	end
	if self.MoneyCtrl.moneyEnough then
		if not self.SelectedItemId then
			MessageMgr.Inst:ShowMessage("BQP_SPONSOR_NO_EQUIP", {})
		else
			local msg = g_MessageMgr:FormatMessage("BQP_SPONSOR_CONFIRM",tostring(self.InputButton:GetValue() * 10000))
			MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(
				function()
					Gac2Gas.BQPInvestForSponsorRank(self.SelectedItemId, self.InputButton:GetValue() * 10000)
				end
       		), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
			
		end
	else
		local msg = MessageMgr.Inst:FormatMessage("NotEnough_Silver_QuickBuy", {})
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(
            function()
                CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
            end
        ), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
	end
end

function LuaBQPMySponsorWnd:OnCancelBtnClick(go)
	CUIManager.CloseUI(CLuaUIResources.BQPMySponsorWnd)
end

function LuaBQPMySponsorWnd:OnAddItemClick(go)
	local initfunc = function()
		local datas = CreateFromClass(MakeGenericClass(List, CCommonItemSelectCellData))
		for i=1,#LuaBingQiPuMgr.m_EquipsCommited do
			local dt = LuaBingQiPuMgr.m_EquipsCommited[i]
			if dt.Rank > 0 then
				local v = dt.Item
				local data = CreateFromClass(CCommonItemSelectCellData, v.Id, v.TemplateId, v.IsBinded, v.Amount)
				CommonDefs.ListAdd(datas,typeof(CCommonItemSelectCellData),data)
			end
		end
		return datas
	end

	local selectfunc = function(itemId, templateId)
		self:OnSelectEquip(itemId, templateId)
	end

	local title = LocalString.GetString("选择要赞助的兵器")
	LuaCommonItemSelectMgr.ShowSelectWnd(title,nil,initfunc,selectfunc)
end

function LuaBQPMySponsorWnd:OnSelectEquip(itemId, templateId)
	self.SelectedItemId = itemId
	self.SelectedTemplateId = templateId
	self:InitItemTemplate()
end

-- 初始化待赞助装备
function LuaBQPMySponsorWnd:InitItemTemplate()

	self.ItemTemplate:SetActive(true)
	self.ItemToAdd:SetActive(false)
	local tf=self.ItemTemplate.transform
	local iconTexture = LuaGameObject.GetChildNoGC(tf,"IconTexture").cTexture
	local qualitySprite = LuaGameObject.GetChildNoGC(tf,"QualitySprite").sprite
	local bindSprite = LuaGameObject.GetChildNoGC(tf,"BindSprite").sprite
	local amountLabel = LuaGameObject.GetChildNoGC(tf,"AmountLabel").label
	local textLabel = LuaGameObject.GetChildNoGC(tf,"TextLabel").label

	iconTexture:Clear()
	qualitySprite.spriteName = CUICommonDef.GetItemCellBorder()
	amountLabel.text = ""
	textLabel.text = ""

	local item = CItemMgr.Inst:GetById(self.SelectedItemId)
	if not item then return end
	bindSprite.spriteName = item.BindOrEquipCornerMark
	iconTexture:LoadMaterial(item.Icon)
	qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item.Equip.QualityType)
	textLabel.text = item.Equip.LostSoulOrLostDurationText
end

function LuaBQPMySponsorWnd:OnDisable()
	LuaCommonItemSelectMgr.CloseSelectWnd()
end
