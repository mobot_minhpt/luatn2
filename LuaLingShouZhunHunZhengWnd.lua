require("3rdParty/ScriptEvent")
require("common/common_include")
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local Time=import "UnityEngine.Time"
local LuaGameObject=import "LuaGameObject"
local LuaTweenUtils=import "LuaTweenUtils"
local LingShou_LingShou=import "L10.Game.LingShou_LingShou"

CLuaLingShouZhunHunZhengWnd=class()
RegistClassMember(CLuaLingShouZhunHunZhengWnd,"m_TypewriteTexture")
RegistClassMember(CLuaLingShouZhunHunZhengWnd,"m_Footprint1")
RegistClassMember(CLuaLingShouZhunHunZhengWnd,"m_Footprint2")
RegistClassMember(CLuaLingShouZhunHunZhengWnd,"m_Anchor")
RegistClassMember(CLuaLingShouZhunHunZhengWnd,"m_Tick1")
RegistClassMember(CLuaLingShouZhunHunZhengWnd,"m_Tick2")

RegistClassMember(CLuaLingShouZhunHunZhengWnd,"m_Panel")

RegistClassMember(CLuaLingShouZhunHunZhengWnd,"m_Tweener1")
RegistClassMember(CLuaLingShouZhunHunZhengWnd,"m_Tweener2")
RegistClassMember(CLuaLingShouZhunHunZhengWnd,"m_Tweener3")
RegistClassMember(CLuaLingShouZhunHunZhengWnd,"m_Tweener3_1")
RegistClassMember(CLuaLingShouZhunHunZhengWnd,"m_Tweener4")
RegistClassMember(CLuaLingShouZhunHunZhengWnd,"m_Tweener5")
RegistClassMember(CLuaLingShouZhunHunZhengWnd,"m_Tweener6")

RegistClassMember(CLuaLingShouZhunHunZhengWnd,"m_UIFx")

RegistClassMember(CLuaLingShouZhunHunZhengWnd,"m_FootMat")

function CLuaLingShouZhunHunZhengWnd:Awake()
    local g = LuaGameObject.GetChildNoGC(self.transform,"CloseButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CUIResources.LingShouZhunHunZhengWnd)
    end)
    self.m_FootMat={"UI/Texture/Transparent/Material/lingshouzhunhunzheng_zhuayin_01.mat",
                    "UI/Texture/Transparent/Material/lingshouzhunhunzheng_zhuayin_02.mat",
                    "UI/Texture/Transparent/Material/lingshouzhunhunzheng_zhuayin_03.mat",
                    "UI/Texture/Transparent/Material/lingshouzhunhunzheng_zhuayin_04.mat"}


    self.m_TypewriteTexture=LuaGameObject.GetChildNoGC(self.transform,"Typewrite").texture
    self.m_TypewriteTexture.gameObject:SetActive(false)

    self.m_Footprint1=LuaGameObject.GetChildNoGC(self.transform,"Footprint1").texture
    self.m_Footprint1.alpha=0
    self.m_Footprint2=LuaGameObject.GetChildNoGC(self.transform,"Footprint2").texture
    self.m_Footprint2.alpha=0

    self.m_Anchor=LuaGameObject.GetChildNoGC(self.transform,"Anchor").transform

    self.m_Panel=LuaGameObject.GetChildNoGC(self.transform,"Anchor").panel
    self.m_UIFx=LuaGameObject.GetChildNoGC(self.transform,"Fx").uiFx
end

function CLuaLingShouZhunHunZhengWnd:PlayTypewrite()
    self.m_TypewriteTexture.material:SetFloat("_StartTime", Time.timeSinceLevelLoad+1.5)
    self.m_TypewriteTexture.gameObject:SetActive(true)
end

function CLuaLingShouZhunHunZhengWnd:Init()
    --初始化爪子
    local t1=LuaGameObject.GetChildNoGC(self.transform,"Footprint1").cTexture
    if t1 then
        local data=LingShou_LingShou.GetData(CLingShouMgr.Lua_MarryLingShouTemplateId1 )
        if data then
            t1:LoadMaterial(self.m_FootMat[data.Foot],false)
            local scale=data.FootScale
            LuaUtils.SetLocalScale(t1.transform,scale,scale,1)
        end
    end
    local t2=LuaGameObject.GetChildNoGC(self.transform,"Footprint2").cTexture
    if t2 then
        -- t2:LoadMaterial("UI/Texture/Transparent/Material/lingshouzhunhunzheng_zhuayin_02.mat",false)
        local data=LingShou_LingShou.GetData(CLingShouMgr.Lua_MarryLingShouTemplateId2)
        if data then
            t2:LoadMaterial(self.m_FootMat[data.Foot],false)
            local scale=data.FootScale
            LuaUtils.SetLocalScale(t2.transform,scale,scale,1)
        end
    end

    self:PlayTypewrite()

    --放大
    self.m_Tweener1=LuaTweenUtils.TweenVector3(Vector3(1,1,1),Vector3(2,2,1),2,function(val) self.m_Anchor.localScale=val end)
    LuaTweenUtils.SetDelay(self.m_Tweener1,8+5)
    --移到中间
    self.m_Tweener5=LuaTweenUtils.TweenVector3(Vector3(0,0,0),Vector3(330,46,1),2,function(val) self.m_Anchor.localPosition=val end)
    LuaTweenUtils.SetDelay(self.m_Tweener5,8+5)
    
    --缩小
    self.m_Tweener2=LuaTweenUtils.TweenVector3(Vector3(2,2,1),Vector3(1,1,1),2,function(val) self.m_Anchor.localScale=val end)
    LuaTweenUtils.SetDelay(self.m_Tweener2,11+8)
    self.m_Tweener6=LuaTweenUtils.TweenVector3(Vector3(330,46,1),Vector3(0,0,0),2,function(val) self.m_Anchor.localPosition=val end)
    LuaTweenUtils.SetDelay(self.m_Tweener6,11+8)
    --脚印
    self.m_Tweener3=LuaTweenUtils.TweenFloat(0,1,2,
                function(val)
                    self.m_Footprint1.alpha=val
                end)
    self.m_Tweener3_1=LuaTweenUtils.TweenFloat(0,1,2,
                function(val)
                    self.m_Footprint2.alpha=val
                end)
    LuaTweenUtils.SetDelay(self.m_Tweener3,10+math.random( 4,7))
    LuaTweenUtils.SetDelay(self.m_Tweener3_1,10+math.random( 4,7))
    

    --消失
    self.m_Tweener4=LuaTweenUtils.TweenFloat(1,0,1,function(val)self.m_Panel.alpha=val end)
    LuaTweenUtils.SetDelay(self.m_Tweener4,13+7)


    --播放特效
    self.m_Tick1=RegisterTickOnce(function()
        self.m_UIFx:LoadFx(CLuaUIFxPaths.LingShouZhunHunZhengFx)
    end,(13+8)*1000)

    self.m_Tick2=RegisterTickOnce(function()
        CUIManager.CloseUI(CUIResources.LingShouZhunHunZhengWnd)
    end,(15+8)*1000)
end
function CLuaLingShouZhunHunZhengWnd:OnDestroy()
   if self.m_Tick1~=nil then
        UnRegisterTick(self.m_Tick1)
        self.m_Tick1=nil
   end
   if self.m_Tick2~=nil then
        UnRegisterTick(self.m_Tick2)
        self.m_Tick2=nil
   end
   if self.m_Tweener1 then
        LuaTweenUtils.Kill(self.m_Tweener1,false)
   end
    if self.m_Tweener2 then
        LuaTweenUtils.Kill(self.m_Tweener2,false)
   end
    if self.m_Tweener3 then
        LuaTweenUtils.Kill(self.m_Tweener3,false)
   end
    if self.m_Tweener3_1 then
        LuaTweenUtils.Kill(self.m_Tweener3_1,false)
   end
    if self.m_Tweener4 then
        LuaTweenUtils.Kill(self.m_Tweener4,false)
   end
    if self.m_Tweener5 then
        LuaTweenUtils.Kill(self.m_Tweener5,false)
   end
    if self.m_Tweener6 then
        LuaTweenUtils.Kill(self.m_Tweener6,false)
   end
end


return CLuaLingShouZhunHunZhengWnd