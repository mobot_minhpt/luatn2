local UIScrollView = import "UIScrollView"
local UITable = import "UITable"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CBaseWnd = import "L10.UI.CBaseWnd"
local Screen = import "UnityEngine.Screen"
local CCommonItem = import "L10.Game.CCommonItem"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
LuaPropertyCompareTip = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPropertyCompareTip, "ItemNameLabel", "ItemNameLabel", UILabel)
RegistChildComponent(LuaPropertyCompareTip, "MyIcon", "MyIcon", GameObject)
RegistChildComponent(LuaPropertyCompareTip, "ContentScrollView", "ContentScrollView", UIScrollView)
RegistChildComponent(LuaPropertyCompareTip, "Table", "Table", UITable)
RegistChildComponent(LuaPropertyCompareTip, "FirstTitleTemplate", "FirstTitleTemplate", GameObject)
RegistChildComponent(LuaPropertyCompareTip, "SecondTitleTemplate", "SecondTitleTemplate", GameObject)
RegistChildComponent(LuaPropertyCompareTip, "EquipTemplate", "EquipTemplate", GameObject)
RegistChildComponent(LuaPropertyCompareTip, "ItemTemplate", "ItemTemplate", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaPropertyCompareTip, "m_MinHeight")
RegistClassMember(LuaPropertyCompareTip, "m_EmptyEquipmentList")
RegistClassMember(LuaPropertyCompareTip, "m_Empty")
RegistClassMember(LuaPropertyCompareTip, "m_Title2Paragraph")
RegistClassMember(LuaPropertyCompareTip, "m_SelectInfo")
RegistClassMember(LuaPropertyCompareTip, "m_ListBtn")
function LuaPropertyCompareTip:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.ItemTemplate.gameObject:SetActive(false)
    self.FirstTitleTemplate.gameObject:SetActive(false)
    self.SecondTitleTemplate.gameObject:SetActive(false)
    self.EquipTemplate.gameObject:SetActive(false)
    self.m_MinHeight = 200
    self.m_EmptyEquipmentList = {}
    self.m_Title2Paragraph = {}
    self.m_SelectInfo = {}
    self.m_Empty = self.transform:Find("Empty").gameObject
end

function LuaPropertyCompareTip:InitEmpty(headerTable)
    self:InitHeader(headerTable)
    Extensions.RemoveAllChildren(self.Table.transform)
    self.m_Empty.gameObject:SetActive(true)
end

function LuaPropertyCompareTip:Init(DataTable)
    self.m_ListBtn = {}
    self.m_Empty.gameObject:SetActive(false)
    self:InitHeader(DataTable.Header)
    self:InitContent(DataTable.Content)
    for k,v in pairs(self.m_ListBtn) do
        self:OnBtnClick(v,k)
    end
end

function LuaPropertyCompareTip:InitHeader(header)
    if not header then return end
    self.ItemNameLabel.text = header.Name
    self.MyIcon.gameObject:SetActive(not header.IsOpposite)
    if not header.IsOpposite then
        self.ItemNameLabel.color = NGUIText.ParseColor24("00FF60", 0)
    else
        self.ItemNameLabel.color = NGUIText.ParseColor24("FFFFFF", 0)
    end
end

function LuaPropertyCompareTip:InitContent(content)
    Extensions.RemoveAllChildren(self.Table.transform)
    self.m_EmptyEquipmentList = {}
    self.m_Title2Paragraph = {}
    self.m_SelectInfo = {}
    for i = 1 ,#content do
        local go = self:AddParagraphGo(content[i])
    end
    local empty = self.transform:Find("Templates/EmptyTemplate").gameObject
    CUICommonDef.AddChild(self.Table.gameObject,empty)
    self.Table:Reposition()
    self.ContentScrollView:ResetPosition()
    --self:LayoutWnd()
end
function LuaPropertyCompareTip:AddParagraphGo(data)
    if data.type == 1 then
        return self:InitFirstParagraph(data)
    elseif data.type == 2 then
        return self:InitSecondParagraph(data)
    elseif data.type == 3 then
        return self:InitItemParagraph(data)
    elseif data.type == 4 then
        return self:InitEquipParagraph(data)
    elseif data.type == 5 then
        local newData = {type = data.type,Value = LocalString.GetString("[该装备无法查询]")}
        local go = self:InitEquipParagraph(newData)
        if not self.m_EmptyEquipmentList[data.Value] then
            self.m_EmptyEquipmentList[data.Value] = {}
            self.m_EmptyEquipmentList[data.Value].view = {}
        end
        table.insert(self.m_EmptyEquipmentList[data.Value].view,go)
        if data.index then
            if not self.m_Title2Paragraph[data.index] then self.m_Title2Paragraph[data.index] = {} end
            table.insert(self.m_Title2Paragraph[data.index],go)
        end
        return go
    end
end
function LuaPropertyCompareTip:InitFirstParagraph(data)
    local go = CUICommonDef.AddChild(self.Table.gameObject,self.FirstTitleTemplate)
    go.gameObject:SetActive(true)
    go.gameObject.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = data.Name
    go.gameObject.transform:Find("ValueLabel"):GetComponent(typeof(UILabel)).text = tostring(math.floor(data.Value))
    go.gameObject.transform:Find("DesLabel"):GetComponent(typeof(UILabel)).text = data.Des
    self:SetUpOrDownSprite(go,data.UpOrDown)
    return go
end 
function LuaPropertyCompareTip:InitSecondParagraph(data)
    local go = CUICommonDef.AddChild(self.Table.gameObject,self.SecondTitleTemplate)
    go.gameObject:SetActive(true)
    go.gameObject.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = data.Name
    if data.Value >= 0 then
        go.gameObject.transform:Find("ValueLabel"):GetComponent(typeof(UILabel)).text = "+" .. tostring(math.floor(math.abs(data.Value)))
    else
        go.gameObject.transform:Find("ValueLabel"):GetComponent(typeof(UILabel)).text = "-" .. tostring(math.floor(math.abs(data.Value)))
    end
    self:SetUpOrDownSprite(go,data.UpOrDown)
    local btn = go.gameObject.transform:Find("Btn").gameObject
    if data.index then self.m_SelectInfo[data.index] = true end
    
    UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtnClick(go,data.index)
	end)
    self.m_ListBtn[data.index] = btn.gameObject
    return go
end

function LuaPropertyCompareTip:InitItemParagraph(data)
    local go = CUICommonDef.AddChild(self.Table.gameObject,self.ItemTemplate)
    go.gameObject:SetActive(true)
    local label = go.gameObject.transform:Find("Label"):GetComponent(typeof(UILabel))
    label.text = data.Value.desc
   
    self:SetUpOrDownSprite(go,data.UpOrDown)
    local label = go.gameObject.transform:Find("Label"):GetComponent(typeof(UILabel))
    if math.floor(math.abs(data.Value.val)) == 0 then
        go:GetComponent(typeof(UIWidget)).alpha = 0.3
    else
        go:GetComponent(typeof(UIWidget)).alpha = 1
    end
    if data.wordId and data.wordId == 26 and data.Value.val > 0 then
        go:GetComponent(typeof(UIWidget)).alpha = 1
    end
    UIEventListener.Get(label.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        local lablel = go:GetComponent(typeof(UILabel))
        local url = label:GetUrlAtPosition(UICamera.lastWorldPosition)

        if url then CChatLinkMgr.ProcessLinkClick(url,nil) end
    end)
    if data.index then
        if not self.m_Title2Paragraph[data.index] then self.m_Title2Paragraph[data.index] = {} end
        table.insert(self.m_Title2Paragraph[data.index],go)
    end
    return go
end
function LuaPropertyCompareTip:InitEquipParagraph(data)
    local go = CUICommonDef.AddChild(self.Table.gameObject,self.EquipTemplate)
    go.gameObject:SetActive(true)
    local label = go.gameObject.transform:Find("Label"):GetComponent(typeof(UILabel))
    label.text = data.Value
    UIEventListener.Get(label.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        local lablel = go:GetComponent(typeof(UILabel))
        local url = label:GetUrlAtPosition(UICamera.lastWorldPosition)

        if url then CChatLinkMgr.ProcessLinkClick(url,nil) 
        end
    end)
    if data.index then
        if not self.m_Title2Paragraph[data.index] then self.m_Title2Paragraph[data.index] = {} end
        table.insert(self.m_Title2Paragraph[data.index],go)
    end
    return go
end
function LuaPropertyCompareTip:SetUpOrDownSprite(go,upOrDown)
    if not go then return end
    local ArrowName = {"common_arrow_07_green","common_arrow_07_red"}
    local sprite = go.transform:Find("UpOrDown"):GetComponent(typeof(UISprite))
    sprite.gameObject:SetActive(false)
    if upOrDown >= 1 then
        sprite.gameObject:SetActive(true)
        sprite.spriteName = ArrowName[1]
    elseif upOrDown <= -1 then
        sprite.gameObject:SetActive(true)
        sprite.spriteName = ArrowName[2]
    end
end
function LuaPropertyCompareTip:LayoutWnd()
    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenWidth = Screen.width * scale * CUIManager.UIMainCamera.rect.width
    local virtualScreenHeight = Screen.height * scale

    local contentTopPadding = math.abs(self.ContentScrollView.panel.topAnchor.absolute)
    local contentBottomPadding = math.abs(self.ContentScrollView.panel.bottomAnchor.absolute)

    --计算高度
    local totalWndHeight = self:TotalHeightOfScrollViewContent() + contentTopPadding + contentBottomPadding + self.ContentScrollView.panel.clipSoftness.y * 2 + 1
    --加1避免由于精度误差导致scrollview刚好显示全table内容时可以滚动的问题
    local displayWndHeight = math.min(math.max(totalWndHeight, self.m_MinHeight), virtualScreenHeight)

    --设置背景高度
    self.transform:GetComponent(typeof(UIWidget)).height = math.ceil(displayWndHeight)

    local rects = CommonDefs.GetComponentsInChildren_Component_Type_Boolean(self.gameObject:GetComponent(typeof(CBaseWnd)), typeof(UIRect), true)
    do
        local i = 0
        while i < rects.Length do
            if rects[i].updateAnchors == UIRect.AnchorUpdate.OnEnable or rects[i].updateAnchors == UIRect.AnchorUpdate.OnStart then
                rects[i]:ResetAndUpdateAnchors()
            end
            i = i + 1
        end
    end
    self.ContentScrollView:ResetPosition()
    self.transform:Find("ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator)):Layout()
    --self.m_Background.transform.localPosition = Vector3.zero
end

function LuaPropertyCompareTip:TotalHeightOfScrollViewContent()
    return NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform).size.y + self.Table.padding.y * 2
end

function LuaPropertyCompareTip:UpdateEquipmentData(id)
    local go = self.m_EmptyEquipmentList[id] and self.m_EmptyEquipmentList[id].view
    if not go then return end

    local item = LuaPlayerPropertyMgr.EquipmentData[id]
    local res = ""
    if item then
        local sign = ""
        local existingLinks = CreateFromClass(MakeGenericClass(Dictionary, Int32, CChatInputLink))
        local text = CChatInputLink.AppendItemLink(sign,item,existingLinks)
        local itemLink = CChatInputLink.Encapsulate(text, existingLinks)
        res = CUICommonDef.TranslateToNGUIText(itemLink)
    end
    for i = 1,#go do
        local label = go[i].gameObject.transform:Find("Label"):GetComponent(typeof(UILabel))
        label.text = res
    end
end

function LuaPropertyCompareTip:OnBtnClick(go,index)
    if not index or not go then return end
    local goList = self.m_Title2Paragraph[index]
    local curStatus =  not self.m_SelectInfo[index]
    go:GetComponent(typeof(UISprite)).flip = curStatus and  UIBasicSprite.Flip.Vertically or UIBasicSprite.Flip.Nothing
    --go.transform.localEulerAngles = curStatus and Vector3(0, 0, 0) or Vector3(0, 0, 180)
    if goList then
        for i = 1,#goList do
            goList[i].gameObject:SetActive(curStatus)
        end
    end
    self.m_SelectInfo[index] = curStatus
    self.Table:Reposition()
    self.transform:Find("ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator)):Layout()
end

function LuaPropertyCompareTip:GetGuideBtn()
    if self.m_ListBtn then return self.m_ListBtn[1] end
    return nil
end

function LuaPropertyCompareTip:OnEnable()
    g_ScriptEvent:AddListener("OnGetPlayerPropertyEquipmentInfo",self,"UpdateEquipmentData")
end

function LuaPropertyCompareTip:OnDisable()
    g_ScriptEvent:RemoveListener("OnGetPlayerPropertyEquipmentInfo",self,"UpdateEquipmentData")
end
--@region UIEvent

--@endregion UIEvent

