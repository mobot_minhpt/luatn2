-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuideMessager = import "L10.Game.Guide.CGuideMessager"
local CLogMgr = import "L10.CLogMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local CTeamMatchMgr = import "L10.Game.CTeamMatchMgr"
local CTeamMatchWnd = import "L10.UI.CTeamMatchWnd"
local EnumEventType = import "EnumEventType"
local EnumShowType = import "L10.UI.CTeamMatchWnd+EnumShowType"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local Object = import "System.Object"
local TeamMatch_Activities = import "L10.Game.TeamMatch_Activities"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local YiTiaoLong_Setting = import "L10.Game.YiTiaoLong_Setting"
CTeamMatchWnd.m_Init_CS2LuaHook = function (this) 

    local showLeftLevelLabel = CommonDefs.IS_VN_CLIENT
    this.leftLevelLabel:SetActive(showLeftLevelLabel)
    this.rightlevelLabel:SetActive(not showLeftLevelLabel)


    this.checkbox.Selected = CTeamMatchMgr.Inst.IncludeNewbie

    this.qingyiSettingRoot:SetActive(false)

    this.minLevelPicker:Clear()
    this.maxLevelPicker:Clear()
    this.minLevelPicker.OnValueChanged = MakeDelegateFromCSFunction(this.OnMinValueChanged, MakeGenericClass(Action1, Int32), this)
    this.maxLevelPicker.OnValueChanged = MakeDelegateFromCSFunction(this.OnMaxValueChanged, MakeGenericClass(Action1, Int32), this)

    repeat
        local default = CTeamMatchWnd.ShowType
        if default == EnumShowType.EnumTeamMatch then
            this.curActivityId = CTeamMatchMgr.Inst.selectedActivityId
            this.curMinLevel = CTeamMatchMgr.Inst.MinLevel
            this.curMaxLevel = CTeamMatchMgr.Inst.MaxLevel
            this.tableView.OnActivitySelected = MakeDelegateFromCSFunction(this.OnActivitySelected, MakeGenericClass(Action1, UInt32), this)
            CTeamMatchMgr.Inst:FillMatchActivitySections(this.tableView.MatchActivitySections)
            break
        elseif default == EnumShowType.EnumTeamGroupMatach then
            this.curActivityId = CTeamGroupMgr.Instance.selectedActivityId
            this.curMinLevel = CTeamGroupMgr.Instance.MinLevel
            this.curMaxLevel = CTeamGroupMgr.Instance.MaxLevel
            this.tableView.OnActivitySelected = MakeDelegateFromCSFunction(this.OnTeamGroupActivitySelected, MakeGenericClass(Action1, UInt32), this)
            CTeamGroupMgr.Instance:FillMatchActivitySections(this.tableView.MatchActivitySections)
            break
        end
    until 1
    this.tableView:Init()
end

CTeamMatchWnd.m_hookOnTeamGroupActivitySelected_CS2LuaHook = function(this, id)
    this:InitPickers(1, 160, CTeamGroupMgr.Instance.MinLevel, CTeamGroupMgr.Instance.MaxLevel)
    CTeamGroupMgr.Instance.selectedActivityId = id
    EventManager.Broadcast(EnumEventType.TeamGroupTargetChange)
end

CTeamMatchWnd.m_OnActivitySelected_CS2LuaHook = function (this, activityId) 
    this.curActivityId = activityId
    this.minLevelPicker:Clear()
    this.maxLevelPicker:Clear()
    local activity = TeamMatch_Activities.GetData(activityId)
    if activity ~= nil then
        local min = activity.MinPlayerLevel
        local max = activity.MaxPlayerLevel <= 0 and CTeamMatchMgr.Inst:GetCurrentPlayerMaxGrade() or math.floor(math.min(activity.MaxPlayerLevel, CTeamMatchMgr.Inst:GetCurrentPlayerMaxGrade()))

        local leftVal = min
        local rightVal = max
        if CTeamMatchMgr.Inst.selectedActivityId == activityId then
            leftVal = CTeamMatchMgr.Inst.MinLevel
            rightVal = CTeamMatchMgr.Inst.MaxLevel
        elseif CClientMainPlayer.Inst ~= nil then
            leftVal = math.floor(math.max(leftVal, CClientMainPlayer.Inst.Level - activity.MinDefLevel))
            rightVal = math.floor(math.min(rightVal, CClientMainPlayer.Inst.Level + activity.MaxDefLevel))
        end
        if leftVal > rightVal then
            local temp = leftVal
            leftVal = rightVal
            rightVal = temp
        end
        this:InitPickers(min, max, leftVal, rightVal)
    end
    this.qingyiSettingRoot:SetActive(this:NeedShowIncludeNewbieOption(activityId))
end
CTeamMatchWnd.m_Start_CS2LuaHook = function (this) 

    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.matchButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.matchButton).onClick, MakeDelegateFromCSFunction(this.OnMatchButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.infoBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.infoBtn).onClick, MakeDelegateFromCSFunction(this.OnInfoButtonClick, VoidDelegate, this), true)
end
CTeamMatchWnd.m_OnCloseButtonClick_CS2LuaHook = function (this, go) 
    this:Close()
    local cmp = CommonDefs.GetComponent_GameObject_Type(this.matchButton, typeof(CGuideMessager))
    if cmp ~= nil then
        cmp:Click()
    end
end
CTeamMatchWnd.m_OnMatchButtonClick_CS2LuaHook = function (this, go) 
    repeat
        local default = CTeamMatchWnd.ShowType
        if default == EnumShowType.EnumTeamMatch then
            if this.curActivityId == 0 or TeamMatch_Activities.GetData(this.curActivityId) == nil then
                g_MessageMgr:ShowMessage("Choose_Target")
            else
                repeat
                    local extern = CTeamMatchWnd.ShowType
                    if extern == EnumShowType.EnumTeamMatch then
                        CTeamMatchMgr.Inst:AskForTeamMatchingForLeader(this.curActivityId, this.curMinLevel > this.curMaxLevel and this.curMaxLevel or this.curMinLevel, this.curMinLevel > this.curMaxLevel and this.curMinLevel or this.curMaxLevel, this:NeedShowIncludeNewbieOption(this.curActivityId) and this.checkbox.Selected)
                        break
                    elseif extern == EnumShowType.EnumTeamGroupMatach then
                        break
                    end
                until 1
                this:Close()
            end
            break
        elseif default == EnumShowType.EnumTeamGroupMatach then
            CTeamGroupMgr.Instance.MaxLevel = this.curMinLevel > this.curMaxLevel and this.curMinLevel or this.curMaxLevel
            CTeamGroupMgr.Instance.MinLevel = this.curMinLevel > this.curMaxLevel and this.curMaxLevel or this.curMinLevel
            EventManager.Broadcast(EnumEventType.TeamGroupTargetChange)
            CTeamGroupMgr.Instance:StartQuickJoin()
            this:Close()
            break
        end
    until 1
end
CTeamMatchWnd.m_NeedShowIncludeNewbieOption_CS2LuaHook = function (this, activityId) 

    local activity = TeamMatch_Activities.GetData(activityId)
    if activity.Newbie == 1 and CClientMainPlayer.Inst ~= nil then
        return CClientMainPlayer.Inst.Level >= YiTiaoLong_Setting.GetData().QingYiGrade
    end
    return false
end
CTeamMatchWnd.m_OnInfoButtonClick_CS2LuaHook = function (this, go) 

    local activity = TeamMatch_Activities.GetData(this.curActivityId)
    if activity.Newbie == 1 and CClientMainPlayer.Inst ~= nil then
        local formula = CommonDefs.GetFormulaObject(205, typeof(Formula2))
        if formula ~= nil then
            local level = GenericDelegateInvoke(formula, Table2ArrayWithCount({CClientMainPlayer.Inst.Level}, 1, MakeArrayClass(Object)))
            if level >= activity.MinPlayerLevel then
                g_MessageMgr:ShowMessage("HELP_NEWBEE_TIP", level)
            end
        end
    end
end
CTeamMatchWnd.m_GetGuideGo_CS2LuaHook = function (this, methodName) 
    if methodName == "GetTeamMatchButton" then
        return this:GetTeamMatchButton()
    else
        CLogMgr.LogError(LocalString.GetString("引导数据表填写错误"))
        return nil
    end
end

