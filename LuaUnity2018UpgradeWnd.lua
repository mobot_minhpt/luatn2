local CommonDefs = import "L10.Game.CommonDefs"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local DelegateFactory = import "DelegateFactory"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CButton = import "L10.UI.CButton"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr.AlignType"
local CLoginMgr = import "L10.Game.CLoginMgr"

LuaUnity2018UpgradeWnd = class()

RegistClassMember(LuaUnity2018UpgradeWnd, "m_AwardTable")
RegistClassMember(LuaUnity2018UpgradeWnd, "m_HandleButton")
RegistClassMember(LuaUnity2018UpgradeWnd, "m_HandleButtonFx")
RegistClassMember(LuaUnity2018UpgradeWnd, "m_InfoButton")

RegistClassMember(LuaUnity2018UpgradeWnd, "m_Key2ItemTbl")

function LuaUnity2018UpgradeWnd:Awake()
    self.m_AwardTable = self.transform:Find("AwardTable"):GetComponent(typeof(UITable))
    self.m_HandleButton = self.transform:Find("HandleButton"):GetComponent(typeof(CButton))
    self.m_HandleButtonFx = self.transform:Find("HandleButton/HandleButtonFx").gameObject
    self.m_InfoButton = self.transform:Find("InfoButton").gameObject

    self.m_Key2ItemTbl = nil

    CommonDefs.AddOnClickListener(self.m_HandleButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnHandleButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_InfoButton, DelegateFactory.Action_GameObject(function(go) self:OnInfoButtonClick() end), false)
end

function LuaUnity2018UpgradeWnd:Init()
    local upgradeInfo = LuaWelfareMgr.m_Unity2018UpgradeInfo
    self.m_Key2ItemTbl = {}
    local childCount = self.m_AwardTable.transform.childCount
    local i = 0
    Version_Unity2018Award.ForeachKey(function(key)
        if i<childCount then
            local instance = self.m_AwardTable.transform:GetChild(i).gameObject
            instance:SetActive(true)
            self.m_Key2ItemTbl[key] = instance
            i = i+1
        end
    end)
    self:InitAwardItems()
    self.m_AwardTable:Reposition()
end

function LuaUnity2018UpgradeWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRefresbUnity2018UpgradeInfo", self, "OnRefresbUnity2018UpgradeInfo")
end

function LuaUnity2018UpgradeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRefresbUnity2018UpgradeInfo", self, "OnRefresbUnity2018UpgradeInfo")
end

function LuaUnity2018UpgradeWnd:OnRefresbUnity2018UpgradeInfo()
    self:InitAwardItems()
end

function LuaUnity2018UpgradeWnd:InitAwardItems()

    local upgradeInfo = LuaWelfareMgr.m_Unity2018UpgradeInfo

    for key, instance in pairs(self.m_Key2ItemTbl) do
        --由于交互要求每个图标大小不一，这里硬编码处理，每个instance只会有style1或style2
        --并且前两个目前必然是style1，后两个必然是style2，否则报错
        local style1 = instance.transform:Find("Item_Style1")
        local style2 = instance.transform:Find("Item_Style2")

        local data = Version_Unity2018Award.GetData(key)
        local endTime = cs_string.IsNullOrEmpty(data.EndTime) and 0 or CServerTimeMgr.Inst:GetTimeStampByStr(data.EndTime)
        local isExpired = false
        if endTime>0 then
            if upgradeInfo.finished then
                isExpired = upgradeInfo.finishTime>endTime --已经更新过，但是更新时间滞后于奖励截止时间
            elseif CServerTimeMgr.Inst.timeStamp>endTime then
                isExpired = true --未更新过，超出了奖励截止时间
            end
        end
        CUICommonDef.SetGrey(instance, isExpired)
        local showAwardItemFx = not (upgradeInfo.needUpgrade or isExpired) and upgradeInfo.finished and not upgradeInfo.awarded

        if data.Items.Length==1 then

            local nameLabel = style1:Find("NameLabel"):GetComponent(typeof(UILabel))
            local iconTexture = style1:Find("Icon").gameObject
            local fxGo = style1:Find("Fx").gameObject
            local designData = Item_Item.GetData(data.Items[0][0])
            nameLabel.text = designData.Name
            fxGo:SetActive(showAwardItemFx)
            local itemId = designData.ID
            CommonDefs.AddOnClickListener(iconTexture, DelegateFactory.Action_GameObject(function(go) self:OnItemClick(go, itemId) end), false)
        elseif data.Items.Length==2 then

            for i=0,1 do
                local child = style2:GetChild(i)
                local nameLabel = child:Find("NameLabel"):GetComponent(typeof(UILabel))
                local iconTexture = child:Find("Icon").gameObject
                local fxGo = child:Find("Fx").gameObject
                local designData = Item_Item.GetData(data.Items[i][0])
                nameLabel.text = designData.Name
                fxGo:SetActive(showAwardItemFx)
                local itemId = designData.ID
                CommonDefs.AddOnClickListener(iconTexture, DelegateFactory.Action_GameObject(function(go) self:OnItemClick(go, itemId) end), false)
            end
        end

        local dateLabel = instance.transform:Find("DateLabel"):GetComponent(typeof(UILabel))
        local awardLabel = instance.transform:Find("AwardLabel"):GetComponent(typeof(UILabel))
        
        if isExpired then
            dateLabel.gameObject:SetActive(true)
            awardLabel.gameObject:SetActive(false)
            if endTime<=0 then
                dateLabel.text = LocalString.GetString("更新可领")
            else
                dateLabel.text = ToStringWrap(CServerTimeMgr.ConvertTimeStampToZone8Time(endTime), LocalString.GetString("MM月dd日前更新可领"))
            end
        elseif upgradeInfo.awarded then
            dateLabel.gameObject:SetActive(false)
            awardLabel.gameObject:SetActive(true)
            awardLabel.text = LocalString.GetString("已领")
        elseif upgradeInfo.needUpgrade then
            dateLabel.gameObject:SetActive(true)
            awardLabel.gameObject:SetActive(false)
            if endTime<=0 then
                dateLabel.text = LocalString.GetString("更新可领")
            else
                dateLabel.text = ToStringWrap(CServerTimeMgr.ConvertTimeStampToZone8Time(endTime), LocalString.GetString("MM月dd日前更新可领"))
            end
        elseif upgradeInfo.finished then
            dateLabel.gameObject:SetActive(false)
            awardLabel.gameObject:SetActive(true)
            -- upgradeInfo.awarded is false
            awardLabel.text = LocalString.GetString("可领")
        else
            dateLabel.gameObject:SetActive(false)
            awardLabel.gameObject:SetActive(false)
        end
    end
    self:InitHandleButton(upgradeInfo)
end


function LuaUnity2018UpgradeWnd:InitHandleButton(upgradeInfo)
    self.m_HandleButton.gameObject:SetActive(true)
    self.m_HandleButton.Enabled = true
    if upgradeInfo.needUpgrade then
        self.m_HandleButton.Text = LocalString.GetString("立即更新")
        self.m_HandleButtonFx:SetActive(true)
		if CommonDefs.IsAndroidPlatform() then
			g_MessageMgr:ShowMessage("UNITY2018_UPGRADE_DESCRIPTION")
		end
    elseif upgradeInfo.finished then
        if upgradeInfo.awarded then
            self.m_HandleButton.Enabled = false
            self.m_HandleButton.Text = LocalString.GetString("已领取")
            self.m_HandleButtonFx:SetActive(false)
        else
            self.m_HandleButton.Text = LocalString.GetString("一键领取")
            self.m_HandleButtonFx:SetActive(true)
        end
    else
        --一般情况不会执行到这里
        self.m_HandleButton.gameObject:SetActive(false)
        self.m_HandleButtonFx:SetActive(false)
    end
end

function LuaUnity2018UpgradeWnd:OnHandleButtonClick()
    local upgradeInfo = LuaWelfareMgr.m_Unity2018UpgradeInfo
    if upgradeInfo.needUpgrade then
        if cs_string.IsNullOrEmpty(upgradeInfo.upgradeUrl) then
            g_MessageMgr:ShowMessage("UNITY2018_UPGRADE_NO_URL")
        else
            local message = g_MessageMgr:FormatMessage("UNITY2018_UPGRADE_CONFIRM", upgradeInfo.packageSize)
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
                CWebBrowserMgr.Inst:OpenExtenalUrl(CLoginMgr.CdnUrlEncrypt(upgradeInfo.upgradeUrl))
            end), nil, nil, nil, false)
        end
    elseif upgradeInfo.finished and not upgradeInfo.awarded then
        LuaWelfareMgr:RequestUnity2018UpgradeAward()
    end
end

function LuaUnity2018UpgradeWnd:OnInfoButtonClick()
    g_MessageMgr:ShowMessage("UNITY2018_UPGRADE_DESCRIPTION")
end

function LuaUnity2018UpgradeWnd:OnItemClick(go, itemId)
    CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
end


