-- Auto Generated!!
local CLivingSkillItem = import "L10.UI.CLivingSkillItem"
local CLivingSkillMgr = import "L10.Game.CLivingSkillMgr"
CLivingSkillItem.m_Init_CS2LuaHook = function (this, skillType) 
    if skillType == nil then
        this.icon:Clear()
        this.levelLabel.text = nil
        this.nameLabel.text = nil
        return
    end
    this.skillId = skillType.ID
    this.nameLabel.text = skillType.Name
    this.icon:LoadSkillIcon(skillType.SkillIcon)


    --查一下这个技能的等级
    this.levelLabel.text = tostring(CLivingSkillMgr.Inst:GetSkillLevel(skillType.ID))
end
CLivingSkillItem.m_UpgradeLifeSkillSuccessed_CS2LuaHook = function (this, skillId, skillLv) 
    if this.skillId == skillId then
        this.levelLabel.text = tostring(skillLv)
    end
end
