-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CItemMgr = import "L10.Game.CItemMgr"
local CLivingSkillCountInputWnd = import "L10.UI.CLivingSkillCountInputWnd"
local CLivingSkillMgr = import "L10.Game.CLivingSkillMgr"
local CLivingSkillUseWnd = import "L10.UI.CLivingSkillUseWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CPos = import "L10.Engine.CPos"
local CScene = import "L10.Game.CScene"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local LifeSkill_Item = import "L10.Game.LifeSkill_Item"
local LifeSkill_SkillType = import "L10.Game.LifeSkill_SkillType"
local LocalString = import "LocalString"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
CLivingSkillCountInputWnd.m_Init_CS2LuaHook = function (this) 
    this.itemData = LifeSkill_Item.GetData(CLivingSkillMgr.Inst.selectedItemId)
    if this.itemData ~= nil then
        this.HuoliCost = this.itemData.HuoLi
        this.huoliLabel.text = tostring(this.itemData.HuoLi)

        local template = CItemMgr.Inst:GetItemTemplate(this.itemData.ID)
        if template ~= nil then
            this.iconTexture:LoadMaterial(template.Icon)
        else
            this.iconTexture.material = nil
        end
        this:UpdateHuoli()
    end
    this:InitLabel()
    this.inputButton.onValueChanged = CommonDefs.CombineListner_Action_uint(this.inputButton.onValueChanged, MakeDelegateFromCSFunction(this.UpdateCost, MakeGenericClass(Action1, UInt32), this), true)
end
CLivingSkillCountInputWnd.m_InitLabel_CS2LuaHook = function (this) 
    local data = LifeSkill_SkillType.GetData(CLivingSkillMgr.Inst.selectedSkill)
    repeat
        local default = data.Type
        if default == 1 then
            this.titleLabel.text = LocalString.GetString("钓鱼")
            CommonDefs.GetComponent_Component_Type(this.button.transform:Find("Label"), typeof(UILabel)).text = LocalString.GetString("钓鱼")
            this.descLabel.text = LocalString.GetString("请输入钓鱼数量")
            break
        elseif default == 2 then
            this.titleLabel.text = LocalString.GetString("采集")
            CommonDefs.GetComponent_Component_Type(this.button.transform:Find("Label"), typeof(UILabel)).text = LocalString.GetString("采集")
            this.descLabel.text = LocalString.GetString("请输入采集数量")
            break
        end
    until 1
end
CLivingSkillCountInputWnd.m_UpdateInputRestrict_CS2LuaHook = function (this) 
    local count = math.floor(CClientMainPlayer.Inst.PlayProp.HuoLi / this.HuoliCost)
    count = math.max(count, 1)
    this.inputButton:SetMinMax(1, count, 1)
end
CLivingSkillCountInputWnd.m_OnClickButton_CS2LuaHook = function (this, go) 
    CLivingSkillMgr.Inst.ContinuousTimes = this.inputButton:GetValue()
    this:Close()
    this:TryCollect(nil)
end
CLivingSkillCountInputWnd.m_TryCollect_CS2LuaHook = function (this, go) 
    if CLivingSkillCountInputWnd.sbDisableCollectCrossServer then
        CUICommonDef.CallMethodConsiderCrossServer(MakeDelegateFromCSFunction(this.Collect, Action0, this), {})
    else
        this:Collect()
    end
end
CLivingSkillCountInputWnd.m_Collect_CS2LuaHook = function (this) 
    local skillTypeData = LifeSkill_SkillType.GetData(CLivingSkillMgr.Inst.selectedSkill)
    local skillType = skillTypeData.Type
    --先检查一下能不能钓鱼
    local data = LifeSkill_Item.GetData(CLivingSkillMgr.Inst.selectedItemId)
    if not CLivingSkillMgr.Inst:CheckHuoli(data.ID) then
        return
    end

    if not CLivingSkillMgr.Inst:CheckPackage(data.ID) then
        return
    end

    --//寻路
    if data ~= nil then
        local mainPlayer = CClientMainPlayer.Inst
        local needPath = true
        if skillType == 1 then
            if data.SceneId == CScene.MainScene.SceneTemplateId and CScene.MainScene:IsInFishingRegion(mainPlayer) then
                --直接关闭生活技能面板，并开始采集
                needPath = false
            end
        elseif skillType == 2 then
            if data.SceneId == CScene.MainScene.SceneTemplateId and CScene.MainScene:IsInPlantingRegion(mainPlayer) then
                --直接关闭生活技能面板，并开始采集
                CTrackMgr.Inst:Stop()
                needPath = false
            end
        else
            needPath = false
        end

        --不需要寻路，直接进行
        if not needPath then
            --有可能从帮会那里过来
            CUIManager.CloseUI(CUIResources.GuildMainWnd)
            CUIManager.CloseUI(CUIResources.SkillWnd)
            CUIManager.CloseUI(CIndirectUIResources.LivingSkillUseWnd)
            --采集
            if skillType == 1 then
                CLivingSkillMgr.Inst:Fish(CLivingSkillMgr.Inst.selectedItemId)
            else
                CLivingSkillMgr.Inst:Caiji(CLivingSkillMgr.Inst.selectedItemId)
            end
            return
        else
            --需要寻路到指定位置，然后使用技能
            local pos = CreateFromClass(CPos)
            if data.Location.Length >= 2 then
                local count = math.floor(data.Location.Length / 2)
                local index = UnityEngine_Random(0, count - 1)
                pos = CreateFromClass(CPos, data.Location[index * 2], data.Location[index * 2 + 1])
            end
            CUIManager.CloseUI(CUIResources.GuildMainWnd)
            CUIManager.CloseUI(CUIResources.SkillWnd)

            CLivingSkillUseWnd.itemId = CLivingSkillMgr.Inst.selectedItemId
            --CLivingSkillUseWnd.skillTypeId = CLivingSkillMgr.Inst.selectedSkill;
            --直接寻路
            CTrackMgr.Inst:Track("", data.SceneId, CreateFromClass(CPos, pos.x * 64, pos.y * 64), 0, DelegateFactory.Action(function () 
                --出现一个窗口提示 是否使用
                CUIManager.ShowUI(CIndirectUIResources.LivingSkillUseWnd)
            end), nil, nil, nil, true)
        end
    end
end
CLivingSkillCountInputWnd.m_UpdateCost_CS2LuaHook = function (this, count) 
    this.huoliLabel.text = tostring((this.itemData.HuoLi * count))
    if count > 0 then
        CUICommonDef.SetActive(this.button, true, true)
    else
        CUICommonDef.SetActive(this.button, false, true)
    end

    --再检查一下等级
    if this.itemData.SkillLev > CLivingSkillMgr.Inst:GetSkillLevel(CLivingSkillMgr.Inst.selectedSkill) then
        CUICommonDef.SetActive(this.button, false, true)
    end

    local makeCount = this.inputButton:GetValue()
    if makeCount == 0 then
        makeCount = 1
    end
end
CLivingSkillCountInputWnd.m_UpdateHuoli_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    local curHuoli = CClientMainPlayer.Inst.PlayProp.HuoLi
    this.allHuoliLabel.text = curHuoli >= this.HuoliCost and tostring(curHuoli) or System.String.Format("[c][ff0000]{0}[-][/c]", curHuoli)
    this:UpdateInputRestrict()
end
