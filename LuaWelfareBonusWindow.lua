
require("3rdParty/ScriptEvent")
require("common/common_include")

local CFirstChargeGiftCell = import "L10.UI.CFirstChargeGiftCell"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CWelfareBonusMgr = import "L10.Game.CWelfareBonusMgr"
local CWelfareBonusTemplate = import "L10.UI.CWelfareBonusTemplate"
local EnumWelfareBonusCategory = import "L10.Game.EnumWelfareBonusCategory"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local UISprite = import "UISprite"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UITable = import "UITable"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaWelfareBonusWindow = class()

RegistChildComponent(LuaWelfareBonusWindow, "InstructionLabel", UILabel)
RegistChildComponent(LuaWelfareBonusWindow, "EndTips", UILabel)
RegistChildComponent(LuaWelfareBonusWindow, "BonusScrollview", CUIRestrictScrollView)
RegistChildComponent(LuaWelfareBonusWindow, "Table", UITable)
RegistChildComponent(LuaWelfareBonusWindow, "BonusTemplate", GameObject)
RegistChildComponent(LuaWelfareBonusWindow, "BonusTemplate2", GameObject)
RegistChildComponent(LuaWelfareBonusWindow, "ItemDisplay", GameObject)
RegistChildComponent(LuaWelfareBonusWindow, "ItemDisplayTable", UITable)
RegistChildComponent(LuaWelfareBonusWindow, "ItemDisplayCell", GameObject)
RegistChildComponent(LuaWelfareBonusWindow, "Texture", GameObject)
RegistChildComponent(LuaWelfareBonusWindow, "SpringFestivalSignInWindow", GameObject)

RegistClassMember(LuaWelfareBonusWindow, "ChargeInfo")
RegistClassMember(LuaWelfareBonusWindow, "Category")

-- common lua script 调用
function LuaWelfareBonusWindow:Init(args)
	if not args or args.Length <= 0 then
		return
	end

    local category = args[0]
    self.Category = category
    self:InitInLua(category)
end

-- lua 内部调用
function LuaWelfareBonusWindow:InitInLua(category)
    if self:InitSpringFestivalWindow(category) then return end

    self.EndTips.gameObject:SetActive(false)
    self.BonusTemplate:SetActive(false)
    self.BonusTemplate2:SetActive(false)

    self:InitScrollviewWithoutItemDisplay()


    if category == EnumWelfareBonusCategory.OnlineTime then
        self:OnInitOnlineTime()
    elseif category == EnumWelfareBonusCategory.HolidayOnline then
        self:OnInitHolidayOnlineTime()
    elseif category == EnumWelfareBonusCategory.LvUp then
        self:OnInitLvUp()
    elseif category == EnumWelfareBonusCategory.LoginDays then
        self:OnInitLoginDays()
    elseif category == EnumWelfareBonusCategory.Holiday then
        self:OnInitHoliday()
    elseif category == EnumWelfareBonusCategory.Huiliu then
        self:OnInitHuiliu()
    elseif category == EnumWelfareBonusCategory.QianDaoDays then
        self:OnInitQianDaoDays()
    elseif category == EnumWelfareBonusCategory.DesignCompetiton then
        self:OnInitDesignCompetition()
    else
        self.InstructionLabel.text = ""
    end
end


function LuaWelfareBonusWindow:OnInitOnlineTime()
	if KaiFuActivity_OnlineTime.Exists(CWelfareBonusMgr.Inst.OnlineAwardId) then
		self.InstructionLabel.text = LocalString.GetString("在线累计一定时间获得丰厚奖励")
		Extensions.RemoveAllChildren(self.Table.transform)
		CWelfareBonusMgr.Inst.isUpdateOnlineTimeTroughOpenWelfareWnd = true
		Gac2Gas.QueryOnlineTimeAward()
	--else
	--	g_ScriptEvent:BroadcastInLua("OnWelfareBonusEmpty", EnumWelfareBonusCategory.OnlineTime)
	end
end

function LuaWelfareBonusWindow:OnInitHolidayOnlineTime()
	if HolidayOnlineTime_Award.Exists(CWelfareBonusMgr.Inst.holidayOnlineAwardId) then
        self.InstructionLabel.text = LocalString.GetString("节日期间在线累计一定时间获得丰厚奖励")
        Extensions.RemoveAllChildren(self.Table.transform)
        Gac2Gas.QueryHolidayOnlineTimeInfo()
    end
end

function LuaWelfareBonusWindow:OnInitLvUp()
	if KaiFuActivity_LvUp.Exists(CWelfareBonusMgr.Inst.LvUpAwardId) then
		self.InstructionLabel.text = LocalString.GetString("升级可获得丰厚奖励")
		Extensions.RemoveAllChildren(self.Table.transform)
		KaiFuActivity_LvUp.Foreach(function (key, data)
			self:InitLvUp(key, data)
		end)
		self.Table:Reposition()
        self.BonusScrollview:ResetPosition()
	--else
	--	g_ScriptEvent:BroadcastInLua("OnWelfareBonusEmpty",EnumWelfareBonusCategory.LvUp)
	end
end

function LuaWelfareBonusWindow:InitLvUp(key, data)
	if key >= CWelfareBonusMgr.Inst.LvUpAwardId then
        local go = NGUITools.AddChild(self.Table.gameObject, self.BonusTemplate)
        go:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(go, typeof(CWelfareBonusTemplate)):InitLvUp(key, self.BonusScrollview)
    end
end



function LuaWelfareBonusWindow:OnInitLoginDays()
	CWelfareBonusMgr.Inst.isUpdateOnlineTimeTroughOpenWelfareWnd = true

	if KaiFuActivity_LoginDays.Exists(CWelfareBonusMgr.Inst.LoginAwardId) then
		self.InstructionLabel.text = LocalString.GetString("登录7天可获得丰厚奖励")
		Extensions.RemoveAllChildren(self.Table.transform)
		KaiFuActivity_LoginDays.Foreach(function (key, data)
			self:InitLoginDays(key, data)
		end)
		self.Table:Reposition()
        self.BonusScrollview:ResetPosition()
	--else
	--	g_ScriptEvent:BroadcastInLua("OnWelfareBonusEmpty",EnumWelfareBonusCategory.LoginDays)
	end
end

function LuaWelfareBonusWindow:InitLoginDays(key, data)
	if key >= CWelfareBonusMgr.Inst.LoginAwardId then
        local time = NGUITools.AddChild(self.Table.gameObject, self.BonusTemplate2)
        time:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(time, typeof(CWelfareBonusTemplate)):InitLoginDays(key, self.BonusScrollview)
    end
end


function LuaWelfareBonusWindow:OnInitHoliday()
	Extensions.RemoveAllChildren(self.Table.transform)
	Gac2Gas.QueryHolidayQianDaoInfo(CSigninMgr.Inst.holidayInfo.batchId)
end

function LuaWelfareBonusWindow:OnInitDesignCompetition()
    Extensions.RemoveAllChildren(self.Table.transform)
	Gac2Gas.QueryHolidayQianDaoInfo(CSigninMgr.Inst.designCompetitionInfo.batchId)
end

function LuaWelfareBonusWindow:OnInitHuiliu()
	if Huiliu_Item.Exists(CSigninMgr.Inst.huiliuLevel) then
        self.InstructionLabel.text = LocalString.GetString("欢迎回来！更多奖励等着你！")
        Extensions.RemoveAllChildren(self.Table.transform)
        local data = Huiliu_Item.GetData(CSigninMgr.Inst.huiliuLevel)
        local awardList = CreateFromClass(MakeArrayClass(System.String), 7)
        awardList[0] = data.Day1
        awardList[1] = data.Day2
        awardList[2] = data.Day3
        awardList[3] = data.Day4
        awardList[4] = data.Day5
        awardList[5] = data.Day6
        awardList[6] = data.Day7
        for i = 0, data.DayNum-1 do
        	self:InitHuiliu(i, awardList[i])
        end
        self.Table:Reposition()
        self.BonusScrollview:ResetPosition()
    end
end

function LuaWelfareBonusWindow:InitHuiliu(index, award)
	local instance = NGUITools.AddChild(self.Table.gameObject, self.BonusTemplate)
    instance:SetActive(true)
    local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CWelfareBonusTemplate))
    if template ~= nil then
        template:InitHuiliu(index, award, self.BonusScrollview)
    end
end

function LuaWelfareBonusWindow:OnInitQianDaoDays()
	 CWelfareBonusMgr.Inst.isUpdateOnlineTimeTroughOpenWelfareWnd = true

    if KaiFuActivity_QianDaoDays.Exists(CWelfareBonusMgr.Inst.QianDaoAwardId) then
       	self.InstructionLabel.text = LocalString.GetString("签到5天可获得丰厚奖励")
        Extensions.RemoveAllChildren(self.Table.transform)
        KaiFuActivity_QianDaoDays.Foreach(function (key, data)
        	self:InitQianDaoDays(key, data)
        end)
        self.Table:Reposition()
        self.BonusScrollview:ResetPosition()
    --else
    --    g_ScriptEvent:BroadcastInLua("OnWelfareBonusEmpty",EnumWelfareBonusCategory.QianDaoDays)
    end
end

function LuaWelfareBonusWindow:InitQianDaoDays(key, data)
	if key >= CWelfareBonusMgr.Inst.QianDaoAwardId then
        local time = NGUITools.AddChild(self.Table.gameObject, self.BonusTemplate2)
        time:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(time, typeof(CWelfareBonusTemplate)):InitQianDaoDays(key, self.BonusScrollview)
    end
end

function LuaWelfareBonusWindow:OnEnable()
	g_ScriptEvent:AddListener("OnlineTimeAwardReceived", self, "InitOnlineTimeAward")
	g_ScriptEvent:AddListener("LoginDaysAwardReceived", self, "OnLogininAwardReceived")
	g_ScriptEvent:AddListener("QianDaoDaysAwardReceived", self, "OnQianDaoDaysAwardReceived")
	g_ScriptEvent:AddListener("LvUpAwardReceived", self, "OnLvUpAwardReceived")
	g_ScriptEvent:AddListener("OnHolidaySigninInfoUpdate", self, "InitHolidayAward")
	g_ScriptEvent:AddListener("HolidayOnlineTimeAwardReceived", self, "InitHolidayOnlineTimeAward")
	g_ScriptEvent:AddListener("HolidayOnlineTimeEnd", self, "OnHolidayOnlineTimeEnd")
	g_ScriptEvent:AddListener("OnHuiliuAwardGet", self, "OnHuiliuAwardGet")
end

function LuaWelfareBonusWindow:OnDisable()
	g_ScriptEvent:RemoveListener("OnlineTimeAwardReceived", self, "InitOnlineTimeAward")
	g_ScriptEvent:RemoveListener("LoginDaysAwardReceived", self, "OnLogininAwardReceived")
	g_ScriptEvent:RemoveListener("QianDaoDaysAwardReceived", self, "OnQianDaoDaysAwardReceived")
	g_ScriptEvent:RemoveListener("LvUpAwardReceived", self, "OnLvUpAwardReceived")
	g_ScriptEvent:RemoveListener("OnHolidaySigninInfoUpdate", self, "InitHolidayAward")
	g_ScriptEvent:RemoveListener("HolidayOnlineTimeAwardReceived", self, "InitHolidayOnlineTimeAward")
	g_ScriptEvent:RemoveListener("HolidayOnlineTimeEnd", self, "OnHolidayOnlineTimeEnd")
	g_ScriptEvent:RemoveListener("OnHuiliuAwardGet", self, "OnHuiliuAwardGet")
end

function LuaWelfareBonusWindow:InitOnlineTimeAward()
	Extensions.RemoveAllChildren(self.Table.transform)
    self.BonusTemplate:SetActive(false)
    self.BonusTemplate2:SetActive(false)
    KaiFuActivity_OnlineTime.Foreach(function (key, data)
    	self:InitOnlineTime(key, data)
    end)
    self.Table:Reposition()
    self.BonusScrollview:ResetPosition()
end

function LuaWelfareBonusWindow:InitOnlineTime(key, data)
	if key >= CWelfareBonusMgr.Inst.OnlineAwardId then
        local time = NGUITools.AddChild(self.Table.gameObject, self.BonusTemplate2)
        time:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(time, typeof(CWelfareBonusTemplate)):InitOnlineTime(key, self.BonusScrollview)
    end
end


function LuaWelfareBonusWindow:OnLogininAwardReceived()
	self:InitInLua(EnumWelfareBonusCategory.LoginDays)
end

function LuaWelfareBonusWindow:OnQianDaoDaysAwardReceived()
	self:InitInLua(EnumWelfareBonusCategory.QianDaoDays)
end

function LuaWelfareBonusWindow:OnLvUpAwardReceived()
	self:InitInLua(EnumWelfareBonusCategory.LvUp)
end

function LuaWelfareBonusWindow:OnHuiliuAwardGet()
	self:InitInLua(EnumWelfareBonusCategory.Huiliu)
end

function LuaWelfareBonusWindow:InitHolidayAward()
    if self:InitSpringFestivalReward() then return end
	self:InitItemDisplay()

    Extensions.RemoveAllChildren(self.Table.transform)
    self.BonusTemplate:SetActive(false)
    self.BonusTemplate2:SetActive(false)

    QianDao_Holiday.Foreach(function (key, data)
    	self:InitHolidaySignin(key, data)
    end)
    self.Table:Reposition()
    self.BonusScrollview:ResetPosition()

    --直接跳转到可以领取的那一天去
    local holidayInfo = CSigninMgr.Inst.holidayInfo
    if self.Category == EnumWelfareBonusCategory.DesignCompetiton then
        holidayInfo = CSigninMgr.Inst.designCompetitionInfo
    end

    local signedDaysExceptToday = holidayInfo.hasSignGift and holidayInfo.totalTimes - 1 or holidayInfo.totalTimes
    local bg = CommonDefs.GetComponent_GameObject_Type(self.BonusTemplate2, typeof(UISprite))
    if self.Table ~= nil and self.BonusScrollview.panel ~= nil and bg ~= nil then
        local height = bg.height + self.Table.padding.y
        local offset = math.min(1, signedDaysExceptToday * height / (self.Table.transform.childCount * height - self.BonusScrollview.panel.height))
        offset = math.max(0, offset)
        self.BonusScrollview:SetDragAmount(0, offset, false)
    end
end

function LuaWelfareBonusWindow:InitHolidaySignin(key, data)
	if data.Open == 0 then
        return
    end
    --if (data.NeedTimes < CSigninMgr.Inst.holidayTotalTimes) return;
    local batchId = nil
    if self.Category == EnumWelfareBonusCategory.DesignCompetiton then
        batchId = CSigninMgr.Inst.designCompetitionInfo.batchId
    else
        batchId = CSigninMgr.Inst.holidayInfo.batchId
    end
    if data.BatchId ~= batchId then
        return
    end
    self.InstructionLabel.text = data.SubTitle
    local instance = NGUITools.AddChild(self.Table.gameObject, self.BonusTemplate)
    instance:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(CWelfareBonusTemplate)):InitHoliday(self.Category, key, self.BonusScrollview)
end

function LuaWelfareBonusWindow:InitItemDisplay()
    local today = nil
    if self.Category == EnumWelfareBonusCategory.DesignCompetiton then
        today = CSigninMgr.Inst.designCompetitionInfo.todayInfo
    else
        today = CSigninMgr.Inst.holidayInfo.todayInfo
    end
    if today ~= nil and today.DisplayOnFirstPage ~= nil and today.DisplayOnFirstPage.Length > 0 then
        self:InitScrollviewWithItemDisplay()

        for i = 0, today.DisplayOnFirstPage.Length-1 do
        	local item = Item_Item.GetData(today.DisplayOnFirstPage[i])
            if item ~= nil then
                local instance = NGUITools.AddChild(self.ItemDisplayTable.gameObject, self.ItemDisplayCell)
                instance:SetActive(true)
                CommonDefs.GetComponent_GameObject_Type(instance, typeof(CFirstChargeGiftCell)):Init(item.ID, 0)
            end
        end
        self.ItemDisplayTable:Reposition()
    else
        self:InitScrollviewWithoutItemDisplay()
    end
end

function LuaWelfareBonusWindow:InitHolidayOnlineTimeAward()
	Extensions.RemoveAllChildren(self.Table.transform)
    self.BonusTemplate:SetActive(false)
    self.BonusTemplate2:SetActive(false)
    HolidayOnlineTime_Award.Foreach(function (key, data)
    	self:InitHolidayOnlineTime(key, data)
    end)
    self.Table:Reposition()
    self.BonusScrollview:ResetPosition()
end

function LuaWelfareBonusWindow:InitHolidayOnlineTime(key, data)
	local todayAward = HolidayOnlineTime_Award.GetData(CWelfareBonusMgr.Inst.holidayOnlineAwardId)
    if key >= CWelfareBonusMgr.Inst.holidayOnlineAwardId and todayAward.BatchId == data.BatchId then
        local time = NGUITools.AddChild(self.Table.gameObject, self.BonusTemplate2)
        time:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(time, typeof(CWelfareBonusTemplate)):InitHolidayOnline(key, self.BonusScrollview)
    end
end

function LuaWelfareBonusWindow:OnHolidayOnlineTimeEnd()
	self.EndTips.text = LocalString.GetString("该活动已结束")
    self.EndTips.gameObject:SetActive(true)
    Extensions.RemoveAllChildren(self.Table.transform)
    self.BonusTemplate:SetActive(false)
    self.BonusTemplate2:SetActive(false)
end

function LuaWelfareBonusWindow:InitScrollviewWithItemDisplay()
	self.ItemDisplay:SetActive(true)
    Extensions.RemoveAllChildren(self.ItemDisplayTable.transform)
   	self.ItemDisplayCell:SetActive(false)
    self.BonusScrollview.panel:SetRect(0, -40, 1234, 608)
    self.BonusScrollview.panel.clipOffset = Vector2.zero
    self.BonusScrollview.transform.localPosition = Vector3.zero
end

function LuaWelfareBonusWindow:InitScrollviewWithoutItemDisplay()
	self.ItemDisplay:SetActive(false)
	self.BonusScrollview.panel:SetRect(0, 22, 1234, 760)
	self.BonusScrollview.panel.clipOffset = Vector2.zero
	self.BonusScrollview.transform.localPosition = Vector3(0, 16, 0)
end


---------------------------------
--- 春节2023 ---
---------------------------------
function LuaWelfareBonusWindow:InitSpringFestivalWindow(category)
    -- 2023春节特殊处理，防止春节签到露出通用界面背景（看具体动效这里可先显示背景）
    if category == EnumWelfareBonusCategory.Holiday and CSigninMgr.Inst.holidayInfo.batchId == 75 then
        for i = 0, self.transform.childCount - 1 do
            self.transform:GetChild(i).gameObject:SetActive(false)
        end
        self:OnInitHoliday()
        return true
    else
        for i = 0, self.transform.childCount - 1 do
            self.transform:GetChild(i).gameObject:SetActive(true)
        end
        self.SpringFestivalSignInWindow:SetActive(false)
        return false
    end
end

function LuaWelfareBonusWindow:InitSpringFestivalReward()
    if self.Category == EnumWelfareBonusCategory.Holiday and CSigninMgr.Inst.holidayInfo.batchId == 75 then
        -- 2023春节特殊处理， 有延迟获得签到数据后再显示春节界面防止露出默认值
        self.SpringFestivalSignInWindow:SetActive(true)
        self.SpringFestivalSignInWindow:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init()
        return true
    end
    return false
end