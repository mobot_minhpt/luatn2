local CUITexture = import "L10.UI.CUITexture"
local CSkillButtonInfo = import "L10.UI.CSkillButtonInfo"
local Vector2 = import "UnityEngine.Vector2"
local DelegateFactory  = import "DelegateFactory"
local Animation = import "UnityEngine.Animation"

LuaPengDaoPlayWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPengDaoPlayWnd, "AdditionalButton1", "AdditionalButton1", CSkillButtonInfo)
RegistChildComponent(LuaPengDaoPlayWnd, "AdditionalButton2", "AdditionalButton2", CSkillButtonInfo)
RegistChildComponent(LuaPengDaoPlayWnd, "GetSkillIcon", "GetSkillIcon", CUITexture)
RegistChildComponent(LuaPengDaoPlayWnd, "GetBuffIcon", "GetBuffIcon", CUITexture)
RegistChildComponent(LuaPengDaoPlayWnd, "GetSkill", "GetSkill", GameObject)
RegistChildComponent(LuaPengDaoPlayWnd, "GetBuff", "GetBuff", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaPengDaoPlayWnd,"m_ShowTaskViewTick")
RegistClassMember(LuaPengDaoPlayWnd,"m_ReplaceSkillTick")
RegistClassMember(LuaPengDaoPlayWnd,"m_Ani")
RegistClassMember(LuaPengDaoPlayWnd,"m_AdditionalButtonSkillIdArr")

function LuaPengDaoPlayWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    if CommonDefs.IS_VN_CLIENT then
        self.GetSkill.transform:Find("Texture (1)").transform.localPosition = Vector3(-253, -2, 0)
        self.GetSkill.transform:Find("Texture (2)").transform.localPosition = Vector3(-104, -2, 0)
        self.GetSkill.transform:Find("Texture (3)").transform.localPosition = Vector3(29, -2, 0)
        self.GetSkill.transform:Find("Texture (4)").transform.localPosition = Vector3(137, -2, 0)

        self.GetBuff.transform:Find("Texture1").transform.localPosition = Vector3(-253, -2, 0)
        self.GetBuff.transform:Find("Texture2").transform.localPosition = Vector3(-104, -2, 0)
        self.GetBuff.transform:Find("Texture3").transform.localPosition = Vector3(29, -2, 0)
        self.GetBuff.transform:Find("Texture4").transform.localPosition = Vector3(137, -2, 0)
    end
end

function LuaPengDaoPlayWnd:Init()
    self.m_Ani = self.gameObject:GetComponent(typeof(Animation))
    self.AdditionalButton1.gameObject:SetActive(false)
    self.AdditionalButton2.gameObject:SetActive(false)
    self.GetSkill.gameObject:SetActive(false)
    self.GetBuff.gameObject:SetActive(false)
    self:CancelShowTaskViewTick()
    self.m_ShowTaskViewTick = RegisterTick(function ()
        local desc = g_MessageMgr:FormatMessage("PengDaoPlay_TaskView", "%s")
        LuaCommonCountDownViewMgr:SetInfo(desc, true, true, nil, LocalString.GetString("规则"), nil, function ()
            g_MessageMgr:ShowMessage("PengDaoPlay_Rule")
        end,nil)
    end,500)
    --Gac2Gas.PDFY_QueryTempSkill()
end

function LuaPengDaoPlayWnd:OnEnable()
    g_ScriptEvent:AddListener("PDFY_SelectBlessing", self, "OnSelectBlessing")
    g_ScriptEvent:AddListener("PDFY_QueryTempSkillResult", self, "OnQueryTempSkillResult")
    --g_ScriptEvent:AddListener("UpdateCooldown", self, "OnUpdateCooldown")
end

function LuaPengDaoPlayWnd:OnDisable()
    self:CancelShowTaskViewTick()
    self:CancelReplaceSkillTick()
    g_ScriptEvent:RemoveListener("PDFY_SelectBlessing", self, "OnSelectBlessing")
    g_ScriptEvent:RemoveListener("PDFY_QueryTempSkillResult", self, "OnQueryTempSkillResult")
    --g_ScriptEvent:RemoveListener("UpdateCooldown", self, "OnUpdateCooldown")
end

function LuaPengDaoPlayWnd:OnSelectBlessing(blessingId, replaceType, replaceId)
    print(blessingId, replaceType, replaceId)
    self.GetSkill.gameObject:SetActive(false)
    self.GetBuff.gameObject:SetActive(false)
    local data = PengDaoFuYao_InsiderGain.GetData(blessingId)
    if data.Type == 1 then
        local buffData = Buff_Buff.GetData(data.GainId)
        self.GetBuffIcon:LoadMaterial(buffData.Icon)
        self.m_Ani:Play("pengdaoplaywnd_buff")
    elseif data.Type == 2 then
        local skillId = data.GainId
        local skillData = Skill_AllSkills.GetData(skillId)
        self.GetSkillIcon:LoadSkillIcon(skillData.SkillIcon)
        self.m_Ani:Play("pengdaoplaywnd_skill")
    end
    self:CancelReplaceSkillTick()
    self.m_ReplaceSkillTick = RegisterTickOnce(function ()
        self.GetSkill.gameObject:SetActive(false)
        self.GetBuff.gameObject:SetActive(false)
        Gac2Gas.PDFY_QueryTempSkill()
    end,1300)
end

function LuaPengDaoPlayWnd:OnQueryTempSkillResult()
    -- self.AdditionalButton1.gameObject:SetActive(false)
    -- self.AdditionalButton2.gameObject:SetActive(false)
    -- local btnArr = {self.AdditionalButton1, self.AdditionalButton2}
    -- self.m_AdditionalButtonSkillIdArr = {}
    -- if LuaPengDaoMgr.m_TempSkillResultInfo then
    --     for i = 1,#LuaPengDaoMgr.m_TempSkillResultInfo do
    --         local info = LuaPengDaoMgr.m_TempSkillResultInfo[i]
    --         local type, id = info[1], info[2]
    --         local skillId = 0
    --         if type == 1 then
    --             local data = PengDaoFuYao_InsiderGain.GetData(id)
    --             skillId = data.GainId
    --         else
    --             local data = PengDaoFuYao_OutsiderSkill.GetData(id)
    --             skillId = data.SkillId * 100 + 1
    --         end
    --         self.m_AdditionalButtonSkillIdArr[i] = skillId
    --         local skillData = Skill_AllSkills.GetData(skillId)
    --         local btn = btnArr[i]
    --         btn.gameObject:SetActive(true)
    --         btn:LoadIcon(skillData.SkillIcon, skillId, false)
    --         UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
    --             self:OnSkillButtonClick(btn.gameObject,skillId)
    --         end)
    --     end
    -- end
end

function LuaPengDaoPlayWnd:OnUpdateCooldown()
    if self.m_AdditionalButtonSkillIdArr then
        local skillId1, skillId2 = self.m_AdditionalButtonSkillIdArr[1], self.m_AdditionalButtonSkillIdArr[2]
        if skillId1 then
            self:OnUpdateBtnCooldown(skillId1, self.AdditionalButton1)
        end
        if skillId1 then
            self:OnUpdateBtnCooldown(skillId2, self.AdditionalButton2)
        end
    end
end

function LuaPengDaoPlayWnd:OnUpdateBtnCooldown(skillId, skillBtnInfo)
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return end
    local cooldownProp = mainplayer.CooldownProp
    local remain = cooldownProp:GetServerRemainTime(skillId)
    local total = cooldownProp:GetServerTotalTime(skillId)
    local percent = (total == 0) and 0 or (remain / total)
    local globalRemain = cooldownProp:GetServerRemainTime(1000)
    local globalTotal = cooldownProp:GetServerTotalTime(1000)
    local globalPercent = (globalTotal == 0) and 0 or (globalRemain / globalTotal)
    if (globalRemain > remain) then
        percent = globalPercent
    end
    skillBtnInfo.CDLeft = percent
end

function LuaPengDaoPlayWnd:CancelShowTaskViewTick()
    if self.m_ShowTaskViewTick then
        UnRegisterTick(self.m_ShowTaskViewTick)
        self.m_ShowTaskViewTick = nil
    end
end


function LuaPengDaoPlayWnd:CancelReplaceSkillTick()
    if self.m_ReplaceSkillTick then
        UnRegisterTick(self.m_ReplaceSkillTick)
        self.m_ReplaceSkillTick = nil
    end
end

function LuaPengDaoPlayWnd:OnSkillButtonClick(go, skillId)
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil or mainplayer.IsDie then
        return
    end

    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.SkillButton, Vector3.zero, nil, 0)
    mainplayer:TryCastSkill(skillId, true, Vector2.zero)
end

--@region UIEvent

--@endregion UIEvent

