local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local Profession=import "L10.Game.Profession"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"


CLuaQingRenJie2021RankWnd = class()
RegistClassMember(CLuaQingRenJie2021RankWnd, "m_TableView")
RegistClassMember(CLuaQingRenJie2021RankWnd,"m_MyRankLabel")
RegistClassMember(CLuaQingRenJie2021RankWnd,"m_MyNameLabel")
RegistClassMember(CLuaQingRenJie2021RankWnd,"m_MyCareer")


function CLuaQingRenJie2021RankWnd:Init()

    local tipButton = self.transform:Find("Anchor/RankView/TipButton").gameObject
    UIEventListener.Get(tipButton).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("Valentine_YiAiZhiMing_Tip")
    end)

    self.m_MyRankLabel = self.transform:Find("Anchor/RankView/MainPlayerInfo/MyRankLabel"):GetComponent(typeof(UILabel))
    self.m_MyNameLabel = self.transform:Find("Anchor/RankView/MainPlayerInfo/MyNameLabel"):GetComponent(typeof(UILabel))
    self.m_MyCareer =  self.transform:Find("Anchor/RankView/MainPlayerInfo/MyCareer"):GetComponent(typeof(UISprite))

    local deadlineLabel = self.transform:Find("DeadlineLabel"):GetComponent(typeof(UILabel))
    deadlineLabel.text = Valentine_YiAiZhiMing.GetData().YiAiZhiMingDeadline

    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text= CClientMainPlayer.Inst.Name
        self.m_MyCareer.spriteName = Profession.GetIcon(CClientMainPlayer.Inst.Class)
    end
    self.m_MyRankLabel.text=LocalString.GetString("未上榜")

    self.m_TableView = self.transform:Find("Anchor/RankView/TableView"):GetComponent(typeof(QnTableView))

    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return CLuaQingRenJie2021Mgr.m_OthersInfo and #CLuaQingRenJie2021Mgr.m_OthersInfo or 0
        end,
        function(item, index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

            local info = CLuaQingRenJie2021Mgr.m_OthersInfo and CLuaQingRenJie2021Mgr.m_OthersInfo[index + 1]
            self:InitItem(item,index,info)
        end)
    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        local info = CLuaQingRenJie2021Mgr.m_OthersInfo and CLuaQingRenJie2021Mgr.m_OthersInfo[row + 1] or nil
        if info then
            CPlayerInfoMgr.ShowPlayerPopupMenu(info.playerId, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
        end
    end)

    self:OnRankDataReady()
    self:InitRewards()
end

function CLuaQingRenJie2021RankWnd:InitItem(item,index,info)
    local tf=item.transform
    local nameLabel=tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text=" "
    local rankLabel=tf:Find("RankLabel"):GetComponent(typeof(UILabel))
    rankLabel.text=""
    local rankSprite=tf:Find("RankLabel/RankSprite"):GetComponent(typeof(UISprite))
    rankSprite.spriteName=""

    local clsSprite = tf:Find("Career"):GetComponent(typeof(UISprite))

    local rank=info.rank
    if rank==1 then
        rankSprite.spriteName="Rank_No.1"
    elseif rank==2 then
        rankSprite.spriteName="Rank_No.2png"
    elseif rank==3 then
        rankSprite.spriteName="Rank_No.3png"
    else
        rankLabel.text=tostring(rank)
    end
    if rank>3 then
        rankSprite.gameObject:SetActive(false)
    else
        rankSprite.gameObject:SetActive(true)
        rankSprite:MakePixelPerfect()
    end

    nameLabel.text = info.playerName
    clsSprite.spriteName = Profession.GetIconByNumber(info.playerClass)

    local labels = tf:Find("Labels")
    for i=1,4 do
        local label = labels:GetChild(i-1):GetComponent(typeof(UILabel))
        label.text = tostring(info.num[i])
    end

end

function CLuaQingRenJie2021RankWnd:OnRankDataReady()
    if not CLuaQingRenJie2021Mgr.m_SelfInfo.rank then
        self.m_MyRankLabel.text=LocalString.GetString("未上榜")
    else
        self.m_MyRankLabel.text = tostring(CLuaQingRenJie2021Mgr.m_SelfInfo.rank)
    end

    self.m_MyNameLabel.text = nil
    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text = CClientMainPlayer.Inst.Name
        self.m_MyCareer.spriteName = Profession.GetIcon(CClientMainPlayer.Inst.Class)
    end

    local parent = self.transform:Find("Anchor/RankView/MainPlayerInfo")
    local labels = parent:Find("Labels")
    for i=1,4 do
        local label = labels:GetChild(i-1):GetComponent(typeof(UILabel))
        label.text = tostring(CLuaQingRenJie2021Mgr.m_SelfInfo.num[i])
    end

    self.m_TableView:ReloadData(true,false)
end

function CLuaQingRenJie2021RankWnd:InitRewards()

    local tf = self.transform:Find("Anchor/RewardView")
    local grid = self.transform:Find("Anchor/RewardView/ScrollView/Grid").gameObject

    local item1 = tf:Find("Item1").gameObject
    item1:SetActive(false)
    local item2 = tf:Find("Item2").gameObject
    item2:SetActive(false)

    local mails = Valentine_YiAiZhiMing.GetData().RankAwardMailId
    local t = {}
    for from, to, mailId in string.gmatch(mails, "(%d+),(%d+),(%d+);") do
        from = tonumber(from)
        to = tonumber(to)
        mailId = tonumber(mailId)
        t[from] = mailId
    end
    table.insert( t, Valentine_YiAiZhiMing.GetData().SpecialJoinAwardMailId)
    table.insert( t, Valentine_YiAiZhiMing.GetData().CommonJoinAwardMailId)
    for i,v in ipairs(t) do
        local raw = Mail_Mail.GetData(v).Items

        local go = NGUITools.AddChild(grid,item1)
        go:SetActive(true)
        local grid2 = go.transform:Find("Grid").gameObject

        local label = go.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
        local label2 = go.transform:Find("Label2"):GetComponent(typeof(UILabel))
        if i==1 then
            label.text = LocalString.GetString("榜首大礼")
            label2.text = LocalString.GetString("（第1名）")
        elseif i==2 then
            label.text = LocalString.GetString("前茅大礼")
            label2.text = LocalString.GetString("（第2-5名）")
        elseif i==3 then
            label.text = LocalString.GetString("参与好礼")
            label2.text = LocalString.GetString("（赠送以爱或知己之名）")
        else
            label.text = LocalString.GetString("参与奖")
            label2.text = LocalString.GetString("（赠送玫瑰或绚丽玫瑰）")
        end

        for templateId, count, _ in string.gmatch(raw, "(%d+),(%d+),(%d+);") do
            templateId = tonumber(templateId)
            count = tonumber(count)
            
            local go2 = NGUITools.AddChild(grid2,item2)
            go2:SetActive(true)
            local icon = go2.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
            local itemData=Item_Item.GetData(templateId)
            if itemData then
                icon:LoadMaterial(itemData.Icon)
            else
                local equipData=EquipmentTemplate_Equip.GetData(templateId)
                if equipData then
                    icon:LoadMaterial(equipData.Icon)
                end
            end
            UIEventListener.Get(go2).onClick=DelegateFactory.VoidDelegate(function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(templateId,false, nil, AlignType2.Default, 0, 0, 0, 0)
            end)
        end
        grid2:GetComponent(typeof(UIGrid)):Reposition()

    end
    grid:GetComponent(typeof(UIGrid)):Reposition()

end
