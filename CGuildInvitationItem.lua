-- Auto Generated!!
local CGuildInvitationItem = import "L10.UI.CGuildInvitationItem"
local CUICommonDef = import "L10.UI.CUICommonDef"
local LocalString = import "LocalString"
local Object = import "System.Object"
local StringBuilder = import "System.Text.StringBuilder"
CGuildInvitationItem.m_Init_CS2LuaHook = function (this, playerId, playerName, portraitName, level) 
    this.PlayerId = playerId
    this.nameLabel.text = playerName
    this.portrait:LoadNPCPortrait(portraitName, false)
    this.levelLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(level))
end
CGuildInvitationItem.m_OnOpButtonClick_CS2LuaHook = function (this, go) 
    if this.OnOpButtonClickDelegate ~= nil then
        GenericDelegateInvoke(this.OnOpButtonClickDelegate, Table2ArrayWithCount({this.PlayerId}, 1, MakeArrayClass(Object)))

        CUICommonDef.SetActive(this.opButton, false, true)
    end
end
