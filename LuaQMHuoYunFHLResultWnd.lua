local UIPanel = import "UIPanel"
local UISlider = import "UISlider"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local LuaTweenUtils = import "LuaTweenUtils"
local Ease = import "DG.Tweening.Ease"

CLuaQMHuoYunFHLResultWnd = class()

RegistClassMember(CLuaQMHuoYunFHLResultWnd, "m_Panel")
RegistClassMember(CLuaQMHuoYunFHLResultWnd, "m_Slider")
RegistClassMember(CLuaQMHuoYunFHLResultWnd, "m_SliderLabel")
RegistClassMember(CLuaQMHuoYunFHLResultWnd, "m_Icon")
RegistClassMember(CLuaQMHuoYunFHLResultWnd, "m_NumLabel")
RegistClassMember(CLuaQMHuoYunFHLResultWnd, "m_TimeTick")
RegistClassMember(CLuaQMHuoYunFHLResultWnd, "m_Tweener")

function CLuaQMHuoYunFHLResultWnd:Awake()
    self:InitComponents()
end

function CLuaQMHuoYunFHLResultWnd:InitComponents()
    self.m_Panel        = self.transform:GetComponent(typeof(UIPanel))
    self.m_Slider       = self.transform:Find("Anchor/Main/Top/Slider"):GetComponent(typeof(UISlider))
    self.m_SliderLabel  = self.transform:Find("Anchor/Main/Top/Slider/Label"):GetComponent(typeof(UILabel))
    self.m_Icon         = self.transform:Find("Anchor/Main/Bottom/TXJ/Icon"):GetComponent(typeof(CUITexture))
    self.m_NumLabel     = self.transform:Find("Anchor/Main/Bottom/NumLabel"):GetComponent(typeof(UILabel))
end

function CLuaQMHuoYunFHLResultWnd:Init()
    self.m_Icon:LoadMaterial((Item_Item.GetData(WuYi2020_Setting.GetData().FullTongXinJingChipId).Icon))
    local bgMask = self.transform:Find("_BgMask_")
    if bgMask ~= nil then
        bgMask.gameObject:SetActive(false)
    end

    local class = CLuaQMHuoYunMgr.resultClass
    local value = CLuaQMHuoYunMgr.resultValue
    local maxValue = CLuaQMHuoYunMgr.resultMaxValue
    local fillNum = CLuaQMHuoYunMgr.resultFillNum

    self.m_Slider.value = maxValue == 0 and 0 or value / maxValue
    self.m_SliderLabel.text = math.floor(value) .. "/" .. maxValue
    self.m_NumLabel.text = 0

    self:TweenAlpha(self.m_Panel, 0, 1, 0.5)
    self.m_TimeTick = RegisterTickOnce(function ()
        local valueTemp = self.m_Slider.value
        local duration = (valueTemp + 0.1) * 6.18
        self.m_Tweener = LuaTweenUtils.TweenFloat(0, 1, duration, function ( val )
            self.m_Slider.value = valueTemp * (1-val)
            self.m_SliderLabel.text = math.floor(value * (1-val)) .. "/" .. maxValue
            self.m_NumLabel.text = math.floor(fillNum * val)

            if val == 1 then
                bgMask.gameObject:SetActive(true)
            end
        end)
        LuaTweenUtils.SetEase(self.m_Tweener, Ease.OutQuad)
    end, 1000)
end

function CLuaQMHuoYunFHLResultWnd:TweenAlpha(widget, startVal, endVal, time)
	local tweener = LuaTweenUtils.TweenFloat(startVal, endVal, time, function ( val )
		widget.alpha = val
	end)
	LuaTweenUtils.SetEase(tweener, Ease.InOutQuint)
end

function CLuaQMHuoYunFHLResultWnd:OnDisable()
    if self.m_TimeTick then
        UnRegisterTick(self.m_TimeTick)
    end
end

function CLuaQMHuoYunFHLResultWnd:OnDestroy()
    if self.m_Tweener then
        LuaTweenUtils.Kill(self.m_Tweener,false)
    end
end