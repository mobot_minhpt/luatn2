local UIGrid = import "UIGrid"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local Quaternion = import "UnityEngine.Quaternion"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local UILabel = import "UILabel"
local UIProgressBar = import "UIProgressBar"
local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Gac2Gas2  = import "L10.Game.Gac2Gas"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Overflow = import "UILabel+Overflow"

--可竞猜0 可加倍1 等待中2 已结束未猜3 已结束已猜中4 已结束未猜中5
EnumFightingSpiritGambleState = {
    eCanGamble = 0,
    eCanDouble = 1,
    eWaiting = 2,
    eEndWithoutGamble = 3,
    eWin = 4,
    eFail = 5,
}

LuaFightingSpiritGambleWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFightingSpiritGambleWnd, "ChangeViewBtn", "ChangeViewBtn", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "ChangeViewAlert", "ChangeViewAlert", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "ChangeViewLabel", "ChangeViewLabel", UILabel)
RegistChildComponent(LuaFightingSpiritGambleWnd, "MainRaceSumView", "MainRaceSumView", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "MainRaceGroupTab", "MainRaceGroupTab", UITabBar)
RegistChildComponent(LuaFightingSpiritGambleWnd, "MainRaceGroupView", "MainRaceGroupView", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "InfoView", "InfoView", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "PayBackCountLabel", "PayBackCountLabel", UILabel)
RegistChildComponent(LuaFightingSpiritGambleWnd, "AddSubBtn", "AddSubBtn", QnAddSubAndInputButton)
RegistChildComponent(LuaFightingSpiritGambleWnd, "AlreadyGamble", "AlreadyGamble", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "NotTakeIn", "NotTakeIn", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "DoubleCommitBtn", "DoubleCommitBtn", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "CostRoot", "CostRoot", CCurentMoneyCtrl)
RegistChildComponent(LuaFightingSpiritGambleWnd, "PayBack", "PayBack", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "CommitBtn", "CommitBtn", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "WaitingResult", "WaitingResult", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "ResultFail", "ResultFail", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "ResultSuccess", "ResultSuccess", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "Cake", "Cake", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "ChooseTeamLabel", "ChooseTeamLabel", UILabel)
RegistChildComponent(LuaFightingSpiritGambleWnd, "SameChoosePercentLabel", "SameChoosePercentLabel", UILabel)
RegistChildComponent(LuaFightingSpiritGambleWnd, "CakeIndicator", "CakeIndicator", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "AlreadyGambleCountLabel", "AlreadyGambleCountLabel", UILabel)
RegistChildComponent(LuaFightingSpiritGambleWnd, "ResultWinCount", "ResultWinCount", UILabel)
RegistChildComponent(LuaFightingSpiritGambleWnd, "MainRaceSumTitleLabel", "MainRaceSumTitleLabel", UILabel)
RegistChildComponent(LuaFightingSpiritGambleWnd, "MainRaceSumGrid", "MainRaceSumGrid", UIGrid)
RegistChildComponent(LuaFightingSpiritGambleWnd, "MainRaceSumNotTakeIn", "MainRaceSumNotTakeIn", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "MainRaceSumNotStart", "MainRaceSumNotStart", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "MainRaceSumAlert", "MainRaceSumAlert", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "ResultFailReasonLabel", "ResultFailReasonLabel", UILabel)
RegistChildComponent(LuaFightingSpiritGambleWnd, "RemainTimeLabel", "RemainTimeLabel", UILabel)
RegistChildComponent(LuaFightingSpiritGambleWnd, "MainRaceMoneyPoolCount", "MainRaceMoneyPoolCount", UILabel)
RegistChildComponent(LuaFightingSpiritGambleWnd, "TipLabel", "TipLabel", UILabel)
RegistChildComponent(LuaFightingSpiritGambleWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "FightingSpiritQuizRoot", "FightingSpiritQuizRoot", GameObject)
RegistChildComponent(LuaFightingSpiritGambleWnd, "MainRaceRoot", "MainRaceRoot", GameObject)

--@endregion RegistChildComponent end
-- 是否是结算页面
RegistClassMember(LuaFightingSpiritGambleWnd, "m_PageIndex")
RegistClassMember(LuaFightingSpiritGambleWnd, "m_MainRaceData")
-- 当前信息
RegistClassMember(LuaFightingSpiritGambleWnd, "m_CurrentGambleData")
RegistClassMember(LuaFightingSpiritGambleWnd, "m_CurrentOpenDoubleBet")
RegistClassMember(LuaFightingSpiritGambleWnd, "m_CurrentBetData")
RegistClassMember(LuaFightingSpiritGambleWnd, "m_CurrentGambleType")
RegistClassMember(LuaFightingSpiritGambleWnd, "m_CurrentState")
-- 全部信息
RegistClassMember(LuaFightingSpiritGambleWnd, "m_GambleDataList")
RegistClassMember(LuaFightingSpiritGambleWnd, "m_BetDataList")
-- 外卡赛竞猜信息 1-4每页记一次
RegistClassMember(LuaFightingSpiritGambleWnd, "m_WildCardChooseInfo")
-- 主赛事竞猜信息 1-6 6页总共一个
RegistClassMember(LuaFightingSpiritGambleWnd, "m_MainChooseInfo")
RegistClassMember(LuaFightingSpiritGambleWnd, "m_MainChooseServerId")
-- 主赛事竞猜信息
RegistClassMember(LuaFightingSpiritGambleWnd, "m_ColorList")
RegistClassMember(LuaFightingSpiritGambleWnd, "m_CakeData")

function LuaFightingSpiritGambleWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)


    --@endregion EventBind end
end

function LuaFightingSpiritGambleWnd:Init()
    -- 记录所有竞猜信息
    self.m_GambleDataList = {}

    -- 主赛事6-11
    self.m_PageIndex = 0
    self.ChangeViewAlert:SetActive(false)
    
    self.m_BetDataList = {}

    -- 主赛事选择信息
    -- 记录所有页卡的选择 默认选中1
    self.m_MainChooseInfo = {}
    self.m_MainChooseServerId = {}
    for i=1, 6 do
        self.m_MainChooseInfo[i] = -1
        self.m_MainChooseServerId[i] = 0
    end

    -- 外卡1-5 主赛事6-11
    self.MainRaceGroupTab.OnTabChange = DelegateFactory.Action_GameObject_int(function(obj, index)
        if index <= 5 then
            if CLuaFightingSpiritMgr.m_MainRaceGambleOpen then
                self.m_PageIndex = index+6
                Gac2Gas.QueryDouHunCrossGambleInfo(index+6)
            end
        else
            self:SetMainRaceSum()
        end
    end)

    -- 节点
    self.MainRaceSumView:SetActive(false)
    self.MainRaceGroupTab.gameObject:SetActive(false)
    self.MainRaceGroupView:SetActive(false)

    if CLuaFightingSpiritMgr.m_GambleWndPageIndex and CLuaFightingSpiritMgr.m_GambleWndPageIndex == 1 then
        -- 设置问答
        self:OnChangeView(true)
    else
        -- 设置主赛事 默认显示
        self:OnChangeView(false)
    end

    -- 饼状图颜色
    self.m_ColorList = {}
    table.insert(self.m_ColorList, Color(1, 166/255, 116/255,1))
    table.insert(self.m_ColorList, Color(1, 115/255, 134/255,1))
    table.insert(self.m_ColorList, Color(212/255, 115/255, 1,1))
    table.insert(self.m_ColorList, Color(1, 231/255, 127/255,1))
    table.insert(self.m_ColorList, Color(108/255, 134/255, 1,1))
    table.insert(self.m_ColorList, Color(151/255, 229/255, 211/255,1))
    table.insert(self.m_ColorList, Color(184/255, 229/255, 152/255,1))
    table.insert(self.m_ColorList, Color(152/255, 211/255, 1,1))

    self.MainRaceSumAlert.transform.localPosition = Vector3(90, 20, 0)
end

-- 点击之后设置选择信息
-- index：1-4
-- mark：-1,0,1
function LuaFightingSpiritGambleWnd:SetMainRaceSum()
    -- TODO 判断一下是否可以请求
    if self.m_CurrentState == EnumFightingSpiritGambleState.eCanGamble then
        for i=1,6 do
            if self.m_MainChooseInfo[i] <=0 then
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请先选出全部六组的第一名"))
                return
            end
        end
    end
    self.m_PageIndex = 12

    local initIndex = 0
    for i = 6, 11 do
        if self.m_GambleDataList[i] == nil then
            if CLuaFightingSpiritMgr.m_MainRaceGambleOpen then
                Gac2Gas.QueryDouHunCrossGambleInfo(i)
            end
        else
            initIndex = i
        end
    end
    -- 如果本地有一些数据 那么就先初始化一下
    if initIndex > 0 and self.m_GambleDataList[initIndex] and self.m_BetDataList then
        self:OnGambleData(initIndex, self.m_CurrentOpenDoubleBet, self.m_GambleDataList[initIndex], self.m_BetDataList)
    end
end

-- 请求红点信息
function LuaFightingSpiritGambleWnd:QueryRedDotInfo()
    for i = 6, 11 do
        if self.m_GambleDataList[i] == nil then
            if CLuaFightingSpiritMgr.m_MainRaceGambleOpen then
                Gac2Gas.QueryDouHunCrossGambleInfo(i)
            end
        end
    end
end

-- 显示默认的主赛事 如果未接到rpc返回的主赛事显示
function LuaFightingSpiritGambleWnd:ShowDefalutView(isMainRace)
    self.MainRaceSumNotStart:SetActive(true)
    self.MainRaceSumView:SetActive(true)

    -- 关闭一些节点的显示
    self.MainRaceSumGrid.gameObject:SetActive(false)
    self.MainRaceSumTitleLabel.gameObject:SetActive(false)
    self.MainRaceSumNotTakeIn.gameObject:SetActive(false)
    self.MainRaceGroupTab.gameObject:SetActive(false)
    self.InfoView:SetActive(false)
    self.MainRaceGroupView:SetActive(false)

    self.MainRaceMoneyPoolCount.gameObject:SetActive(false)
    self.RemainTimeLabel.text = ""

    local label = self.MainRaceSumNotStart.transform:GetComponent(typeof(UILabel))
    if isMainRace then
        label.text = LocalString.GetString("淘汰赛竞猜尚未开始")
        self.TipLabel.text = g_MessageMgr:FormatMessage("DouHun_Gamble_Tip_MainRace")
    else
        label.text = LocalString.GetString("外卡赛竞猜尚未开始")
        self.TipLabel.text = g_MessageMgr:FormatMessage("DouHun_Gamble_Tip_Waika")
    end
end

-- 根据已有的信息初始化
-- 需要gambleData里面的betInfo计算汇率，serverData显示名称（不会失效）
-- 需要gambleData里面的result来显示猜对是否（揭晓结果的时候可能会失效，但也只是一瞬间，忽略）
-- 可竞猜0 可加倍1 等待中2 已结束未猜3 已结束已猜中4 已结束未猜中5
function LuaFightingSpiritGambleWnd:UpdateMainRaceSumView()
    local state = self.m_CurrentState

    self.MainRaceSumNotTakeIn:SetActive(state == 3)
    self.MainRaceSumGrid.gameObject:SetActive(not (state == 3))
    self.MainRaceSumTitleLabel.gameObject:SetActive(not (state == 3))
    self.MainRaceSumNotStart.gameObject:SetActive(false)

    for i = 6,11 do
        local betData = self.m_BetDataList[i]
        local gambleData = self.m_GambleDataList[i]
        if betData and gambleData then
            if betData.hasGamble and gambleData.bSetResult and 
                    gambleData.result == betData.choice and not betData.hasGotReward then
                Gac2Gas.RequestGetDouHunCrossGambleReward(i)
            end
        end
    end
    if state ~= 3 then
        for i=1, 6 do
            local item = self.MainRaceSumGrid.transform:Find(tostring(i)).gameObject

            local teamLabel = item.transform:Find("TeamLabel"):GetComponent(typeof(UILabel))
            local percentLabel = item.transform:Find("PercentLabel"):GetComponent(typeof(UILabel))
            local resultLabel = item.transform:Find("ResultLabel"):GetComponent(typeof(UILabel))
            local jumpBtnNotPay = item.transform:Find("JumpBtnNotPay").gameObject
            local jumpBtnAlreadyPay = item.transform:Find("JumpBtnAlreadyPay").gameObject

            -- 默认初始化
            teamLabel.text = ""
            percentLabel.text = ""
            resultLabel.text = ""
            jumpBtnNotPay:SetActive(false)
            jumpBtnAlreadyPay:SetActive(false)

            local gambleData = self.m_GambleDataList[i + 5]
            local betData = self.m_BetDataList[i + 5]

            if gambleData and betData then
                -- 表示可以处理了
                if state == 0 then
                    -- 未猜
                    jumpBtnNotPay:SetActive(true)
                    UIEventListener.Get(jumpBtnNotPay).onClick = DelegateFactory.VoidDelegate(function()
                        self.MainRaceGroupTab:ChangeTab(i-1, false)
                    end)
                    -- 未竞猜
                    local serverData = gambleData.serverData[self.m_MainChooseInfo[i]]
                    --print("gambleData.serverData[betData.choice]", serverData, self.m_MainChooseInfo[i])
                    if serverData then
                        teamLabel.text = serverData.ServerName
                        percentLabel.text = SafeStringFormat3(LocalString.GetString("（%.1f%%竞猜支持率）"), serverData.Percent * 100)
                    else
                        teamLabel.text = ""
                        percentLabel.text = ""
                    end 
                elseif state == 1 or state == 2 then
                    -- 未结果
                    jumpBtnAlreadyPay:SetActive(true)
                    UIEventListener.Get(jumpBtnAlreadyPay).onClick = DelegateFactory.VoidDelegate(function()
                        self.MainRaceGroupTab:ChangeTab(i-1, false)
                    end)

                    local serverData = gambleData.serverData[betData.choice]
                    --print("gambleData.serverData[betData.choice]", serverData, betData.choice)
                    if serverData then
                        teamLabel.text = serverData.ServerName
                        percentLabel.text = SafeStringFormat3(LocalString.GetString("（%.1f%%竞猜支持率）"), serverData.Percent* 100)
                    else
                        teamLabel.text = ""
                        percentLabel.text = ""
                    end
                else
                    -- 有结果
                    local hasWin = gambleData.result == betData.choice
                    resultLabel.color = Color(1,1,1,1)
                    if hasWin then
                        resultLabel.text = LocalString.GetString("[7DEA53]猜对")
                    else
                        resultLabel.text = LocalString.GetString("[FF1D1F]猜错")
                    end

                    local serverData = gambleData.serverData[betData.choice]
                    if serverData then
                        teamLabel.text = serverData.ServerName
                        percentLabel.text = SafeStringFormat3(LocalString.GetString("（%.1f%%竞猜支持率）"), serverData.Percent* 100)
                    else
                        teamLabel.text = ""
                        percentLabel.text = ""
                    end
                end
            end
        end
    end
end

-- 点击之后设置选择信息
-- index：1-6
-- mark：-1,0,1
function LuaFightingSpiritGambleWnd:OnMainRaceChoose(index, chooseIndex, serverID)
    self.m_MainChooseInfo[index] = chooseIndex
    self.m_MainChooseServerId[index] = serverID
    self:SetCake(chooseIndex)
    -- 设置当前界面高亮
    for j=1, 8 do
        local mark = self.MainRaceGroupView.transform:Find(tostring(j).."/HighLightMark").gameObject
        mark:SetActive(j==chooseIndex)
    end
end


function LuaFightingSpiritGambleWnd:InitMainRaceTab(gambleType, gambleData, betDataList)
    local bSetResult = gambleData.bSetResult
    self.MainRaceSumAlert:SetActive(false)
    for i=1,6 do
        local betData = betDataList[i+5]
        local item = self.MainRaceGroupTab.transform:Find(tostring(i)).gameObject
        local mark = item.transform:Find("Mark"):GetComponent(typeof(CUITexture))

        -- 是否已猜 本地有数据 或者远程有数据都可以
        if betData.silver > 0 or self.m_MainChooseInfo[i]>0 then
            -- 已做答
            mark:LoadMaterial("UI/Texture/Transparent/Material/bagualuwnd_button_shanggua.mat")
        else
            mark:LoadMaterial("UI/Texture/Transparent/Material/bagualuwnd_button_shanggua_none.mat")
        end

        local curGambleData = self.m_GambleDataList[i+5]
        if curGambleData and betData.hasGamble and curGambleData.bSetResult and 
            curGambleData.result == betData.choice and not betData.hasGotReward then
            self.MainRaceSumAlert:SetActive(true)
        end
    end
end


-- 初始化主赛事界面
function LuaFightingSpiritGambleWnd:InitMainRaceView(gambleType, bOpenDoubleBet, gambleData, betData)
    local sumSilver = 0
    for i=1, 8 do
        sumSilver = sumSilver + gambleData.serverData[i].ServerSilver
    end

    -- 胜负关系
    local hasResult = gambleData.bSetResult
    -- 是否已猜
    local hasGamble = betData.silver > 0
    local index = gambleType - 5

    -- 饼状图数据
    self.m_CakeData = {}
    local cakeIndex = 0
    self.ChooseTeamLabel.text = ""

    self.SameChoosePercentLabel.gameObject:SetActive(false)
    for i=1, 8 do
        local item = self.MainRaceGroupView.transform:Find(tostring(i)).gameObject
    
        local itemTexture = item:GetComponent(typeof(CUITexture))
        local btnLabel = item.transform:Find("BtnLabel"):GetComponent(typeof(CUITexture))
        local hightMarkTexture = item.transform:Find("HighLightMark"):GetComponent(typeof(CUITexture))
        local highMarkLabel= item.transform:Find("HighLightMark/BtnLabel (1)"):GetComponent(typeof(CUITexture))

        local teamLabel = item.transform:Find("TeamLabel"):GetComponent(typeof(UILabel))
        local countLabel = item.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
        local serverData = gambleData.serverData[i]
        
        -- 显示
        teamLabel.text = serverData.ServerName
        --print("name$id", serverData.ServerName, serverData.Id)
        UIEventListener.Get(teamLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            --print("Gac2Gas.QueryDouHunCrossServerGroupInfo(serverData.Id)", serverData.Id)
            Gac2Gas.QueryDouHunCrossServerGroupInfo(serverData.Id)
        end)

        local count = math.min(100, math.floor(serverData.Percent* 100))
        countLabel.text = tostring(count).. "%"

        -- 饼状图数据
        if sumSilver > 0 then
            self.m_CakeData[i] = serverData.Percent
        else
            self.m_CakeData[i] = 1/8
        end

        --print("result&gamble", gambleData.result, betData.choice, serverData.Id)
        -- 设置不同状态
        if hasResult then
            btnLabel.gameObject:SetActive(true)
            -- 已完
            if serverData.Id == gambleData.result then
                --print("setWinner", serverData.Id, gambleData.result )
                btnLabel:LoadMaterial("UI/Texture/Transparent/Material/fightingspiritgamblewnd_wenzi_sheng.mat")
                highMarkLabel:LoadMaterial("UI/Texture/Transparent/Material/fightingspiritgamblewnd_wenzi_sheng.mat")
                hightMarkTexture:LoadMaterial("UI/Texture/Transparent/Material/fightingspiritgamblewnd_xuanzhong_caidui.mat")
                CUICommonDef.SetActive(item, true, true)
                CUICommonDef.SetActive(hightMarkTexture.gameObject, true, true)
            else
                btnLabel:LoadMaterial("UI/Texture/Transparent/Material/fightingspiritgamblewnd_wenzi_ya_normal.mat")
                highMarkLabel:LoadMaterial("UI/Texture/Transparent/Material/fightingspiritgamblewnd_wenzi_ya_normal.mat")
                hightMarkTexture:LoadMaterial("UI/Texture/Transparent/Material/fightingspiritgamblewnd_xuanzhong_caicuo.mat")
                CUICommonDef.SetActive(item, false, false)
                CUICommonDef.SetActive(hightMarkTexture.gameObject, false, false)
            end

            -- 是否已猜
            if hasGamble then
                if betData.choice == serverData.Id then
                    CUICommonDef.SetActive(item, true, false)
                    CUICommonDef.SetActive(hightMarkTexture.gameObject, true, false)
                    cakeIndex = i
                    self.ChooseTeamLabel.text = serverData.ServerName
                    self.SameChoosePercentLabel.text = tostring(count).. "%"
                    self.SameChoosePercentLabel.gameObject:SetActive(true)
                    hightMarkTexture.gameObject:SetActive(true)

                    btnLabel.gameObject:SetActive(false)
                else
                    hightMarkTexture.gameObject:SetActive(false)
                end
            else
                hightMarkTexture.gameObject:SetActive(false)
            end
        elseif not hasResult and not hasGamble then
            -- 未猜未完
            CUICommonDef.SetActive(item, true, true)
            CUICommonDef.SetActive(hightMarkTexture.gameObject, true, true)

            -- 设置正确的材质
            btnLabel:LoadMaterial("UI/Texture/Transparent/Material/fightingspiritgamblewnd_wenzi_ya_normal.mat")
            highMarkLabel:LoadMaterial("UI/Texture/Transparent/Material/fightingspiritgamblewnd_wenzi_ya_highlight.mat")
            hightMarkTexture:LoadMaterial("UI/Texture/Transparent/Material/fightingspiritgamblewnd_ya_highlight.mat")
            itemTexture:LoadMaterial("UI/Texture/Transparent/Material/fightingspiritgamblewnd_ya_normal.mat")

            -- 按照存储的记录来设置
            if i == self.m_MainChooseInfo[index] then
                self.ChooseTeamLabel.text = serverData.ServerName
                self.SameChoosePercentLabel.text = tostring(count).. "%"
                self.SameChoosePercentLabel.gameObject:SetActive(true)
                cakeIndex = 1
                hightMarkTexture.gameObject:SetActive(true)
            else
                hightMarkTexture.gameObject:SetActive(false)
            end

            UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function()
                self.ChooseTeamLabel.text = serverData.ServerName
                self.SameChoosePercentLabel.text = tostring(count).. "%"
                self.SameChoosePercentLabel.gameObject:SetActive(true)
                self:OnMainRaceChoose(index, i, serverData.Id)
            end)
        else
            -- 已猜未完
            CUICommonDef.SetActive(item, false, false)
            CUICommonDef.SetActive(hightMarkTexture.gameObject, false, true)

            -- 设置正确的材质
            btnLabel:LoadMaterial("UI/Texture/Transparent/Material/fightingspiritgamblewnd_wenzi_ya_normal.mat")
            highMarkLabel:LoadMaterial("UI/Texture/Transparent/Material/fightingspiritgamblewnd_wenzi_ya_highlight.mat")
            hightMarkTexture:LoadMaterial("UI/Texture/Transparent/Material/fightingspiritgamblewnd_ya_highlight.mat")
            itemTexture:LoadMaterial("UI/Texture/Transparent/Material/fightingspiritgamblewnd_ya_normal.mat")

            if betData.choice == serverData.Id then
                hightMarkTexture.gameObject:SetActive(true)
                self.ChooseTeamLabel.text = serverData.ServerName
                self.SameChoosePercentLabel.text = tostring(count).. "%"
                self.SameChoosePercentLabel.gameObject:SetActive(true)
                cakeIndex = i
            else
                hightMarkTexture.gameObject:SetActive(false)
            end
            UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function()
            end)
        end

        CUICommonDef.SetActive(teamLabel.gameObject, true, false)
    end

    self:SetCake(cakeIndex)
end

function LuaFightingSpiritGambleWnd:SetCake(highlightIndex)
    local itemList = {}
    self.CakeIndicator:SetActive(false)

    for i=1, 7 do
        table.insert(itemList, self.Cake.transform:Find(tostring(i)):GetComponent(typeof(UITexture)))
    end

    local highLight = self.Cake.transform:Find("HighLight"):GetComponent(typeof(UITexture))
    local right = self.Cake.transform:Find("RightBar"):GetComponent(typeof(UITexture))
    local left = self.Cake.transform:Find("LeftBar"):GetComponent(typeof(UITexture))
    local inside = self.Cake.transform:Find("P1/Inside"):GetComponent(typeof(UITexture))
    local out = self.Cake.transform:Find("P2/Out"):GetComponent(typeof(UITexture))

    highLight.depth = 21
    right.depth = 20
    left.depth = 22

    local itemIndex = 1
    local sum = 0
    for i= 1, 8 do
        local color = self.m_ColorList[i]
        local count = self.m_CakeData[i]

        -- 防止除零错误
        if count < 0 then
            count = 1/8
        end

        if i == highlightIndex then
            -- 设置边缘
            left.fillAmount = 0.008
            inside.fillAmount = count + 0.008
            out.fillAmount = count + 0.008
            right.fillAmount = count + 0.008

            highLight.fillAmount = count
            highLight.color = color

            local radius = 170
            local x = math.cos((math.pi/2) - (math.pi * count)) * radius
            local y = math.sin((math.pi/2) - (math.pi * count)) * radius

            if x and y and x>-200 and x<200 and y>-200 and y<200 then
                self.CakeIndicator:SetActive(true)
                self.CakeIndicator.transform.localPosition = Vector3(x,y,0)
                self.CakeIndicator.transform.localRotation = Quaternion.Euler(0, 0, 60 - 180 * count)
            else
                self.CakeIndicator:SetActive(false)
            end
        else
            local item = itemList[itemIndex]
            itemIndex = itemIndex + 1
            if item then
                sum = sum + count
                item.fillAmount = sum
                item.color = color
            else
                -- 说明超出了item
            end
        end
    end

    if  highlightIndex == 0 then
        -- 设置边缘
        left.fillAmount = 0
        inside.fillAmount = 0
        out.fillAmount = 0
        right.fillAmount = 0

        highLight.fillAmount = 1 - sum
        highLight.color = self.m_ColorList[8]
        self.CakeIndicator:SetActive(false)
    end
end

function LuaFightingSpiritGambleWnd:UpdateTitle(gambleType, totalSilver, expiredTime)
    self.MainRaceMoneyPoolCount.gameObject:SetActive(gambleType > 5)

    if gambleType <= 5 then
        self.TipLabel.text = g_MessageMgr:FormatMessage("DouHun_Gamble_Tip_Waika")
    else
        self.MainRaceMoneyPoolCount.text = tostring(totalSilver * 6)
        self.TipLabel.text = g_MessageMgr:FormatMessage("DouHun_Gamble_Tip_MainRace")
    end

    if expiredTime > CServerTimeMgr.Inst.timeStamp then
        self.RemainTimeLabel.gameObject:SetActive(true)
        local timeStamp = expiredTime - CServerTimeMgr.Inst.timeStamp
        local day = math.floor(timeStamp/(24 * 3600))
        local hour = math.floor((timeStamp - (day*24*3600)) / 3600)
        if day > 0 then
            self.RemainTimeLabel.text = SafeStringFormat3(LocalString.GetString("%s天%s时"), tostring(day), tostring(hour))
        else
            self.RemainTimeLabel.text = SafeStringFormat3(LocalString.GetString("%s时"), tostring(hour))
        end
    else
        self.RemainTimeLabel.gameObject:SetActive(false)
    end
end

-- 当数据返回时才刷新界面
-- 包括按钮
function  LuaFightingSpiritGambleWnd:OnGambleData(gambleType, bOpenDoubleBet, gambleData, betDataList)
    -- 当前下发信息
    self.m_CurrentGambleData = gambleData
    self.m_CurrentBetData = betDataList[gambleType]
    self.m_CurrentGambleType = gambleType

    -- 全局信息
    self.m_GambleDataList[gambleType] = gambleData
    self.m_BetDataList = betDataList
    self.m_CurrentOpenDoubleBet = bOpenDoubleBet

    if gambleType >5 then
        if self.m_PageIndex > 5 then
            self:InitMainRaceTab(gambleType, gambleData, betDataList)
        end

        if gambleType == self.m_PageIndex or self.m_PageIndex == 12 then
            -- 主赛事
            self.MainRaceGroupTab.gameObject:SetActive(true)
            self.InfoView:SetActive(false)
            self.MainRaceSumView:SetActive(false)
            self.MainRaceGroupView:SetActive(false)

            self:UpdateTitle(gambleType, gambleData.totalSilver, gambleData.closeTime)

            local betData = betDataList[gambleType]

            -- 主赛事设置的状态有所区别
            self.m_CurrentState = self:GetMainRaceState(bOpenDoubleBet, gambleData, betData)

            if self.m_PageIndex == 12 then
                -- 结算界面初始化
                -- 尝试领奖 
                self.MainRaceSumView:SetActive(true)
                self:UpdateMainRaceSumView()
                self:UpdateMainRaceInfoView()
            elseif gambleType == self.m_PageIndex then
                -- 当前竞猜页面
                self.MainRaceGroupView:SetActive(true)
                self:InitMainRaceView(gambleType, bOpenDoubleBet, gambleData, betData)
            end

            -- 按钮
            UIEventListener.Get(self.ChangeViewBtn).onClick = DelegateFactory.VoidDelegate(function()
                self:OnChangeView(true)
            end)
        end
    end

    -- 更新红点
    if (self.m_PageIndex <=5) ~= (gambleType <=5) then
        local betData = betDataList[gambleType]
        if gambleData and gambleData.bSetResult and gambleData.result == betData.choice and 
            betData.silver>0 and (not betData.hasGotReward) then
            self.ChangeViewAlert:SetActive(true)
        end
    end
end

function LuaFightingSpiritGambleWnd:OnChangeView(isToShowWildCard)
    -- 先判断一下 是否可以执行切换逻辑
    if isToShowWildCard and not CLuaFightingSpiritMgr:CanOpenGambleWnd(true) then
        isToShowWildCard = false
    end

    if not isToShowWildCard and not CLuaFightingSpiritMgr:CanOpenGambleWnd(false) then
        isToShowWildCard = true
    end

    if isToShowWildCard then
        self.FightingSpiritQuizRoot:SetActive(true)
        self.MainRaceRoot:SetActive(false)
    else
        self.FightingSpiritQuizRoot:SetActive(false)
        self.MainRaceRoot:SetActive(true)

        if CLuaFightingSpiritMgr.m_MainRaceGambleOpen then
            self:ShowDefalutView(true)
            self.ChangeViewAlert:SetActive(false)
            self.MainRaceGroupTab:ChangeTab(0, false)
            self:QueryRedDotInfo()
            for i = 1, 5 do
                local gambleData = self.m_GambleDataList[i]
                local betData = self.m_BetDataList[i]
                if gambleData and betData then
                    if gambleData.bSetResult and gambleData.result == betData.choice and 
                        betData.silver>0 and (not betData.hasGotReward) then
                        self.ChangeViewAlert:SetActive(true)
                    end
                end
            end
        else
            CLuaFightingSpiritMgr:ShowGambleNotOpen(true)
        end
    end

    -- 切换外卡赛按钮
    if not isToShowWildCard then
        UIEventListener.Get(self.ChangeViewBtn).onClick = DelegateFactory.VoidDelegate(function()
            self:OnChangeView(true)
        end)
        self.ChangeViewLabel.text = LocalString.GetString("切换至问答")
    else
        UIEventListener.Get(self.ChangeViewBtn).onClick = DelegateFactory.VoidDelegate(function()
            self:OnChangeView(false)
        end)
        self.ChangeViewLabel.text = LocalString.GetString("切换至竞猜")
    end
end

-- 获取状态
-- 可竞猜0 可加倍1 等待中2 已结束未猜3 已结束已猜中4 已结束未猜中5
function LuaFightingSpiritGambleWnd:GetState(bOpenDoubleBet, gambleData, betData)
    -- 胜负关系
    local hasResult = gambleData.bSetResult
    -- 是否已猜
    local hasGamble = betData.hasGamble
    -- 是否已加倍
    local hasDouble = betData.hasDouble

    if hasResult then
        if hasGamble then
            if gambleData.result == betData.choice then
                return 4
            else
                return 5
            end
        else
            return 3
        end
    else
        if hasGamble then
            if bOpenDoubleBet and not hasDouble then
                return 1
            end
            return 2
        else
            return 0
        end
    end
end
-- 获取状态
-- 可竞猜0 可加倍1 等待中2 已结束未猜3 已结束已猜中4 已结束未猜中5
function LuaFightingSpiritGambleWnd:GetMainRaceState(bOpenDoubleBet, gambleData, betData)
    -- 胜负关系
    local hasResult = gambleData.bSetResult
    -- 是否已猜
    local hasGamble = betData.hasGamble
    -- 是否已加倍
    local hasDouble = betData.hasDouble

    if hasResult then
        if hasGamble then
            for i=6,11 do
               local gambleDataSelf = self.m_GambleDataList[i]
               local betData = self.m_BetDataList[i]
               if gambleDataSelf and betData and gambleDataSelf.result == betData.choice then
                    return EnumFightingSpiritGambleState.eWin
                end
            end
            return EnumFightingSpiritGambleState.eFail
        else
            return EnumFightingSpiritGambleState.eEndWithoutGamble
        end
    else
        if hasGamble then
            if not hasDouble then
                return EnumFightingSpiritGambleState.eCanDouble
            end
            return EnumFightingSpiritGambleState.eWaiting
        else
            return EnumFightingSpiritGambleState.eCanGamble
        end
    end
end


-- 主赛事的预期回报显示
function LuaFightingSpiritGambleWnd:UpdateMainRacePayBackCount(useResult)
    local sum = 0

    local currentList = {}
    for i=6,11 do
        local gambleData = self.m_GambleDataList[i]
        local betData = self.m_BetDataList[i]
        if gambleData and betData then
            -- 计算预期的回报倍数
            local choice = 0
            if useResult then
                choice = gambleData.result
            else
                choice = betData.choice
            end
            local curChocie = self.m_MainChooseServerId[i-5]
            for k,v in pairs(gambleData.betInfo) do
                if k == choice or k == curChocie then
                    table.insert(currentList, v)
                end
                sum = sum + v
            end
        end
    end

	if self.m_WenDaSilver > 0 then
        sum = self.m_WenDaSilver
    end

    local payback = 0
    for _, current in ipairs(currentList) do
        payback = payback + (GetFormula(1123)(nil, nil, {current, sum/6})/6)
    end
    
	payback = math.floor(payback*100)/100

    self.PayBackCountLabel.text = tostring(payback)
end

-- 刷新info
-- 可竞猜0 可加倍1 等待中2 已结束未猜3 已结束已猜中4 已结束未猜中5
function LuaFightingSpiritGambleWnd:UpdateMainRaceInfoView()
    
    local state = self.m_CurrentState
    self.InfoView:SetActive(true)
    -- 节点显示
    self.PayBack:SetActive(state == 0 or state == 1 or state == 2 or state == 4 or state == 5)
    self.CostRoot.gameObject:SetActive(state == 0 or state == 1)
    self.AlreadyGamble:SetActive(state == 1 or state == 2 or state == 4 or state == 5)
    self.AddSubBtn.gameObject:SetActive(state == 0)
    self.CommitBtn:SetActive(state == 0)
    self.DoubleCommitBtn:SetActive(state == 1)
    self.WaitingResult:SetActive(state == 2)
    self.NotTakeIn:SetActive(state == 3)
    self.ResultSuccess:SetActive(state == 4 or state == 4)
    self.ResultFail:SetActive(state == 5)

    -- 回报倍数
    self.ResultWinCount.gameObject:SetActive(false)
    self.PayBackCountLabel.text = 0
    if state > EnumFightingSpiritGambleState.eWaiting then
        self.PayBack.transform:GetComponent(typeof(UILabel)).text = LocalString.GetString("正确选项回报倍数")
        self:UpdateMainRacePayBackCount(true)

        local sumRewardSilver = 0
        for i=6, 11 do
            local betData = self.m_BetDataList[i]
            if betData then
                sumRewardSilver = sumRewardSilver + betData.rewardSilver
            end
        end

        if sumRewardSilver > 0 then
            self.ResultWinCount.gameObject:SetActive(true)
            self.ResultWinCount.text = tostring(sumRewardSilver)
        end
    else
        self.PayBack.transform:GetComponent(typeof(UILabel)).text = LocalString.GetString("最高竞猜成功回报倍数")
        self:UpdateMainRacePayBackCount(false)
    end

    self.ResultFailReasonLabel.text = LocalString.GetString("没有猜中")

    -- 计算总押注
    local silver = 0
    for i=6,11 do
        silver = silver + self.m_BetDataList[i].silver
    end
    self.AlreadyGambleCountLabel.text = tostring(silver)

    local setting = DouHunCross_Setting.GetData()

    -- 主赛事的押注金额*6
    local peiceCount = setting.GambleMainRaceSimplePiece * 6
    local maxCount = setting.MainBetMaxSilver*6/peiceCount

    -- 设置金额消耗
    self.CostRoot:SetCost(peiceCount)

    
    CUICommonDef.SetActive(self.PayBackCountLabel.gameObject, true, true)

    -- 设置添加份数的按钮
	self.AddSubBtn.onValueChanged = DelegateFactory.Action_uint(function (value)
		self.CostRoot:SetCost(peiceCount * value)
	end)
    self.AddSubBtn:SetValue(1, true)
    self.AddSubBtn:SetMinMax(1, maxCount, 1)

    if state == EnumFightingSpiritGambleState.eCanGamble then
        --可竞猜
        UIEventListener.Get(self.CommitBtn).onClick = DelegateFactory.VoidDelegate(function()
            if self.CostRoot.moneyEnough then
                local content = SafeStringFormat3(LocalString.GetString("斗魂坛不可多次竞猜，确定要竞猜%s份吗？将消耗#287%s。"), tostring(self.AddSubBtn:GetValue()),tostring(self.CostRoot:GetCost()))
                g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("CUSTOM_STRING2", content), function()
                    for i = 6, 11 do
                        local result = self.m_MainChooseServerId[i-5]
                        -- 押注的时候 每份数/6
                        --print("Gac2Gas.RequestBetDouHunCrossGamble", i, result, self.CostRoot:GetCost()/6)
                        Gac2Gas.RequestBetDouHunCrossGamble(i, result, self.CostRoot:GetCost()/6)
                    end
                end, nil, nil, nil, false)
            else
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("银两不足"))
            end
        end)
    elseif state == EnumFightingSpiritGambleState.eCanDouble then
        -- 可翻倍
        self.CostRoot:SetCost(silver)
        UIEventListener.Get(self.DoubleCommitBtn).onClick = DelegateFactory.VoidDelegate(function()
            if not self.m_CurrentOpenDoubleBet then
                g_MessageMgr:ShowMessage("DouHun_Gamble_Not_Double_Commit_Time")
                return
            end

            if self.CostRoot.moneyEnough then
                local cost = self.CostRoot:GetCost()
                local peice = cost / peiceCount
                local content = SafeStringFormat3(LocalString.GetString("将消耗#287%s加倍至%s份，是否确认？"), tostring(cost), tostring(peice * 2))
                g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("CUSTOM_STRING2", content), function()
                    for i = 6, 11 do
                        local result = self.m_MainChooseInfo[i-5]
                        -- 押注的时候 每份数/6
                        Gac2Gas.RequestDoubleBetDouHunCrossGamble(i)
                    end
                end, nil, nil, nil, false)
            else
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("银两不足"))
            end
        end)
    elseif state == EnumFightingSpiritGambleState.eWaiting then
        -- 等待
    elseif state == EnumFightingSpiritGambleState.eEndWithoutGamble then
        -- 已结束未猜
        CUICommonDef.SetActive(self.PayBackCountLabel.gameObject, false, true)
    elseif state == EnumFightingSpiritGambleState.eFail then
        -- 一个都没猜对
        CUICommonDef.SetActive(self.PayBackCountLabel.gameObject, false, true)
    elseif state == EnumFightingSpiritGambleState.eWin then
        -- 至少猜对了一个
    end
end

function LuaFightingSpiritGambleWnd:OnFail(gambleType, choice, silver)
    -- 失败不需要处理
end

function LuaFightingSpiritGambleWnd:OnBetSuccess(gambleType, choice, silver)
    local betData =  self.m_BetDataList[gambleType]
    if betData then
        betData.choice = choice
        betData.hasGamble = true
        betData.silver = silver
    end

    -- 刷新界面
    self:OnGambleData(gambleType, self.m_CurrentOpenDoubleBet, self.m_GambleDataList[gambleType], self.m_BetDataList)
    --print("QueryDouHunCrossGambleInfo(gambleType)", gambleType)
    Gac2Gas.QueryDouHunCrossGambleInfo(gambleType)
end

function LuaFightingSpiritGambleWnd:OnDoubleBetSuccess(gambleType, choice, silver)
    local betData =  self.m_BetDataList[gambleType]
    if betData then
        betData.choice = choice
        betData.silver = silver
        betData.hasDouble = true
    end
    -- 刷新界面
    self:OnGambleData(gambleType, self.m_CurrentOpenDoubleBet, self.m_GambleDataList[gambleType], self.m_BetDataList)
end

-- 请求打开队伍界面
function LuaFightingSpiritGambleWnd:OnGroupId(groupId)
    --Gac2Gas.QueryDouHunChlgTeamInfoById(groupId)
end

-- 收到奖励金额
function LuaFightingSpiritGambleWnd:OnRward(gambleType, silver)
    self.ResultWinCount.gameObject:SetActive(true)

    local sumRewardSilver = 0
    local betData = self.m_BetDataList[gambleType]
    betData.rewardSilver = silver
    betData.hasGotReward = true

    -- 主赛事特殊处理 在这里进行累加
    if gambleType > 5 then
        for i=6, 11 do
            local betData = self.m_BetDataList[i]
            if betData then
                sumRewardSilver = sumRewardSilver + betData.rewardSilver
            end
        end
        self.MainRaceSumAlert:SetActive(false)
    else
        sumRewardSilver = silver
    end

    self.ResultWinCount.text = tostring(sumRewardSilver)
    local multi = 0
    local des = LocalString.GetString("[fff880]竞猜成功！[-]")
    -- 外卡赛需要显示名称
    if self.m_CurrentGambleType <=5 then
        des = SafeStringFormat3(LocalString.GetString("[a21111]外卡赛%s[-]   "), DouHunCross_Setting.GetData().LevelName[self.m_CurrentGambleType-1]) .. des
    end
    CLuaFightingSpiritMgr:OpenGambleRewardWnd(sumRewardSilver, des, multi)
end

function LuaFightingSpiritGambleWnd:OnWenDaData(data)
    self.m_WenDaSilver = data[1]
    self:UpdateMainRacePayBackCount()
end

function LuaFightingSpiritGambleWnd:OnEnable()
    self.m_WenDaSilver = 0
    Gac2Gas.OpenDouHunWenDaWnd()
    g_ScriptEvent:AddListener("FightingSpiritWenDa", self, "OnWenDaData")
	g_ScriptEvent:AddListener("QueryDouHunCrossGambleInfoResult", self, "OnGambleData")
	g_ScriptEvent:AddListener("BetDouHunCrossGambleFailed", self, "OnFail")
	g_ScriptEvent:AddListener("BetDouHunCrossGambleSuccess", self, "OnBetSuccess")
	g_ScriptEvent:AddListener("DoubleBetDouHunCrossGambleFailed", self, "OnFail")
	g_ScriptEvent:AddListener("DoubleBetDouHunCrossGambleSuccess", self, "OnDoubleBetSuccess")
	g_ScriptEvent:AddListener("RequestGetDouHunCrossGambleRewardSuccess", self, "OnRward")
	g_ScriptEvent:AddListener("QueryDouHunCrossServerGroupInfoResult", self, "OnGroupId")
end

function LuaFightingSpiritGambleWnd:OnDisable()
    g_ScriptEvent:RemoveListener("FightingSpiritWenDa", self, "OnWenDaData")
	g_ScriptEvent:RemoveListener("QueryDouHunCrossGambleInfoResult", self, "OnGambleData")
    g_ScriptEvent:RemoveListener("BetDouHunCrossGambleFailed", self, "OnFail")
	g_ScriptEvent:RemoveListener("BetDouHunCrossGambleSuccess", self, "OnBetSuccess")
	g_ScriptEvent:RemoveListener("DoubleBetDouHunCrossGambleFailed", self, "OnFail")
	g_ScriptEvent:RemoveListener("DoubleBetDouHunCrossGambleSuccess", self, "OnDoubleBetSuccess")
	g_ScriptEvent:RemoveListener("RequestGetDouHunCrossGambleRewardSuccess", self, "OnRward")
	g_ScriptEvent:RemoveListener("QueryDouHunCrossServerGroupInfoResult", self, "OnGroupId")
end

--@region UIEvent

function LuaFightingSpiritGambleWnd:OnTipBtnClick()
    g_MessageMgr:ShowMessage("DouHun_Gamble_Rule")
end


--@endregion UIEvent

