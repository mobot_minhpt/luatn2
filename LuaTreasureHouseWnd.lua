require("3rdParty/ScriptEvent")
require("common/common_include")

local UITabBar = import "L10.UI.UITabBar"
local CButton = import "L10.UI.CButton"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
--local Initialization_Init = import "L10.Game.Initialization_Init"
local Profession = import "L10.Game.Profession"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local EnumClass = import "L10.Game.EnumClass"
local Cangbaoge_Setting = import "L10.Game.Cangbaoge_Setting"
local CUITexture = import "L10.UI.CUITexture"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local CPlayerInfoMgrAlignType = import "CPlayerInfoMgr+AlignType"
local Constants = import "L10.Game.Constants"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"

-- 将待售区信息放在藏宝阁界面里处理
LuaTreasureHouseWnd=class()

-------  藏宝阁总界面信息  -------
RegistClassMember(LuaTreasureHouseWnd,"TabBar")
RegistClassMember(LuaTreasureHouseWnd,"RoleForSaleView")
RegistClassMember(LuaTreasureHouseWnd,"MyRoleView")

-------  待售区信息  -------
RegistClassMember(LuaTreasureHouseWnd,"ClassSelectBtn") -- 选择职业
RegistClassMember(LuaTreasureHouseWnd,"GradeSelectBtn") -- 选择等级
RegistClassMember(LuaTreasureHouseWnd,"ScoreSelectBtn") -- 选择装评
RegistClassMember(LuaTreasureHouseWnd,"HiddenTreasureBtn") -- 进入藏宝阁按钮

RegistClassMember(LuaTreasureHouseWnd,"TableViewDataSource")
RegistClassMember(LuaTreasureHouseWnd,"TableView")
RegistClassMember(LuaTreasureHouseWnd,"NoResultHint") -- 无结果提示
RegistClassMember(LuaTreasureHouseWnd,"GoodsInfo") -- 商品信息(所有)
RegistClassMember(LuaTreasureHouseWnd,"FilteredGoodsInfo") -- 过滤过的商品信息(部分)


-------  用于过滤的信息  -------
RegistClassMember(LuaTreasureHouseWnd,"ClassList") -- 职业list
RegistClassMember(LuaTreasureHouseWnd,"GradeList") -- 等级list(low, high)
RegistClassMember(LuaTreasureHouseWnd,"EquipScoreList") -- 装评list(low, high)
RegistClassMember(LuaTreasureHouseWnd,"Min_Grade") -- 最小的等级
RegistClassMember(LuaTreasureHouseWnd,"Max_Grade") -- 最大的等级
RegistClassMember(LuaTreasureHouseWnd,"Min_EquipScore") -- 最小的装评
RegistClassMember(LuaTreasureHouseWnd,"Max_EquipScore") -- 最大的装评
RegistClassMember(LuaTreasureHouseWnd,"SelectedClass") -- 选中的职业(0表示不限)
RegistClassMember(LuaTreasureHouseWnd,"SelectedGrade") -- 选中的等级区间(0表示不限)
RegistClassMember(LuaTreasureHouseWnd,"SelectedEquipScore") -- 选中的装评区间(0表示不限)


function LuaTreasureHouseWnd:Init()
	self.GoodsInfo = {}
	self.FilteredGoodsInfo = {}

	self:InitTreasureHouseMember()
	self:InitRoleForSaleMember()
end

-- 初始化藏宝阁总界面成员变量
function LuaTreasureHouseWnd:InitTreasureHouseMember()
	self.TabBar = self.gameObject:GetComponent(typeof(UITabBar))
	self.RoleForSaleView = self.transform:Find("RoleForSaleView").gameObject
	self.MyRoleView = self.transform:Find("MyRoleView").gameObject

	self.RoleForSaleView:SetActive(false)
	self.MyRoleView:SetActive(false)

	self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabChange(go, index)
	end)

	self.TabBar:ChangeTab(0)
end

-- 初始化待售区成员变量
function LuaTreasureHouseWnd:InitRoleForSaleMember()

	self.ClassSelectBtn = self.transform:Find("RoleForSaleView/SelectionRoot/ClassSelectBtn"):GetComponent(typeof(CButton))
	self.GradeSelectBtn = self.transform:Find("RoleForSaleView/SelectionRoot/GradeSelectBtn"):GetComponent(typeof(CButton))
	self.ScoreSelectBtn = self.transform:Find("RoleForSaleView/SelectionRoot/ScoreSelectBtn"):GetComponent(typeof(CButton))
	self.HiddenTreasureBtn = self.transform:Find("RoleForSaleView/SelectionRoot/HiddenTreasureBtn"):GetComponent(typeof(CButton))

	self:InitClassList()
    self:InitGradeList()
    self:InitEquipScoreList()

	-- 选择职业
	local onClassBtnClicked = function (go)
		self:OnClassBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.ClassSelectBtn.gameObject,DelegateFactory.Action_GameObject(onClassBtnClicked),false)

	-- 选择等级
	local onGradeBtnClicked = function (go)
		self:OnGradeBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.GradeSelectBtn.gameObject,DelegateFactory.Action_GameObject(onGradeBtnClicked),false)

	-- 选择装评
	local onScoreBtnClicked = function (go)
		self:OnScoreBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.ScoreSelectBtn.gameObject,DelegateFactory.Action_GameObject(onScoreBtnClicked),false)

	-- 进入藏宝阁
	local onHiddenTreasureBtnClicked = function (go)
		self:OnHiddenTreasureBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.HiddenTreasureBtn.gameObject,DelegateFactory.Action_GameObject(onHiddenTreasureBtnClicked),false)

	self.NoResultHint = self.transform:Find("RoleForSaleView/Content/NoResultHint").gameObject

	local function initItem(item,index)
    	self:InitItem(item, index)
    end
    self.TableViewDataSource=DefaultTableViewDataSource.CreateByCount(#self.GoodsInfo, initItem)
    self.TableView.m_DataSource=self.TableViewDataSource
    self.TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
    
    Gac2Gas.CBG_RequestAllGameOnShelfInfos(CClientMainPlayer.Inst.Id, "")

end

-- 初始化ClassList
function LuaTreasureHouseWnd:InitClassList()
	self.SelectedClass = 0
    self.ClassList = CreateFromClass(MakeGenericClass(List, UInt32))

	Initialization_Init.ForeachKey(function (key) 
        local data = Initialization_Init.GetData(key)
        if data.Status == 0 and not self.ClassList:Contains(data.Class) then
        	self.ClassList:Add(data.Class)
        end
    end)
end

--  初始化GradeList
function LuaTreasureHouseWnd:InitGradeList()
	self.SelectedGrade = 0
	self.GradeList = {}
	self.Min_Grade = 0
	self.Max_Grade = 160

	local setting = Cangbaoge_Setting.GetData()
	for i = 0, setting.LevelLimited.Length, 1 do
		if i == 0 then
			table.insert(self.GradeList, {
				low = self.Min_Grade,
				high = setting.LevelLimited[i],
				})
		elseif i == setting.LevelLimited.Length then
			table.insert(self.GradeList, {
				low = setting.LevelLimited[i-1]+1,
				high = self.Max_Grade,
				})
		else
			table.insert(self.GradeList, {
				low = setting.LevelLimited[i-1]+1,
				high = setting.LevelLimited[i],
				})
		end
	end
end

-- 初始化EquipScoreList
function LuaTreasureHouseWnd:InitEquipScoreList()
	self.SelectedEquipScore = 0
	self.EquipScoreList = {}
	self.Min_EquipScore = 0
	self.Max_EquipScore = 1000000
	local setting = Cangbaoge_Setting.GetData()
	for i = 0, setting.EquipmentScoreLimited.Length, 1 do
		if i == 0 then
			table.insert(self.EquipScoreList, {
				low = self.Min_EquipScore,
				high = setting.EquipmentScoreLimited[i],
				})
		elseif i == setting.EquipmentScoreLimited.Length then
			table.insert(self.EquipScoreList, {
				low = setting.EquipmentScoreLimited[i-1]+1,
				high = self.Max_EquipScore,
				})
		else
			table.insert(self.EquipScoreList, {
				low = setting.EquipmentScoreLimited[i-1]+1,
				high = setting.EquipmentScoreLimited[i],
				})
		end
	end
end

-- 切换tab
function LuaTreasureHouseWnd:OnTabChange(gameObject, index)
	if index == 0 then
		self.RoleForSaleView:SetActive(true)
		self.MyRoleView:SetActive(false)
		-- 请求数据
    	if not CClientMainPlayer.Inst then 
    		CUIManager.CloseUI(CLuaUIResources.TreasureHouseWnd)
    		return
    	end
    	Gac2Gas.CBG_RequestAllGameOnShelfInfos(CClientMainPlayer.Inst.Id, "")
	elseif index == 1 then
		self.RoleForSaleView:SetActive(false)
		self.MyRoleView:SetActive(true)
		local luaScript = CommonDefs.GetComponent_GameObject_Type(self.MyRoleView, typeof(CCommonLuaScript))
		luaScript:Init({})
	end
end

function LuaTreasureHouseWnd:OnEnable()
	g_ScriptEvent:AddListener("CBGSendAllPlayerInfo", self, "UpdatePlayerInfo")
	g_ScriptEvent:AddListener("UnisdkGetUserTickResult", self, "UnisdkGetUserTickResult")
end

function LuaTreasureHouseWnd:OnDisable()
	g_ScriptEvent:RemoveListener("CBGSendAllPlayerInfo", self, "UpdatePlayerInfo")
	g_ScriptEvent:RemoveListener("UnisdkGetUserTickResult", self, "UnisdkGetUserTickResult")
end

-- 点击选择职业过滤
function  LuaTreasureHouseWnd:OnClassBtnClicked(go)
	local menuList = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))

	-- 职业不限
	CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("职业不限"), DelegateFactory.Action_int(function (idx) 
                self:OnClassSelected(idx)
            end), false, nil, EnumPopupMenuItemStyle.Light))

	-- 职业
    for i = 0, self.ClassList.Count - 1 do
    	CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), self.ClassList[i])), DelegateFactory.Action_int(function (idx) 
                self:OnClassSelected(idx)
            end), false, nil, EnumPopupMenuItemStyle.Light))
    end
    CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(menuList), go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, nil, 350, true, 300)
end

-- 确认选择某个职业过滤
function LuaTreasureHouseWnd:OnClassSelected(index)
	if self.SelectedClass == index then return end
	if index == 0 then
		self.ClassSelectBtn.Text = LocalString.GetString("职业不限")
	else
		local cls = self.ClassList[index-1]
		self.ClassSelectBtn.Text = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), cls))
	end
	self.SelectedClass = index
	self:DoFilterGoodsInfo()
 end 

-- 点击选择等级过滤
function LuaTreasureHouseWnd:OnGradeBtnClicked(go)
	local menuList = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))

 	-- 职业不限
	CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("等级不限"), DelegateFactory.Action_int(function (idx) 
                self:OnGradeSelected(idx)
            end), false, nil, EnumPopupMenuItemStyle.Light))

	for i = 1, #self.GradeList, 1 do
		CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(SafeStringFormat3("%s ~ %s", tostring(self.GradeList[i].low), tostring(self.GradeList[i].high)), DelegateFactory.Action_int(function (idx) 
                self:OnGradeSelected(idx)
            end), false, nil, EnumPopupMenuItemStyle.Light))
	end
	CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(menuList), go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, nil, 350, true, 300)
end

-- 确认选择某个等级过滤
function LuaTreasureHouseWnd:OnGradeSelected(index)
	if self.SelectedGrade == index then return end 
	if index == 0 then
		self.GradeSelectBtn.Text = LocalString.GetString("等级不限")
	else
		local gradeSection = self.GradeList[index]
		self.GradeSelectBtn.Text = SafeStringFormat3("%s ~ %s", tostring(gradeSection.low), tostring(gradeSection.high))
	end
	self.SelectedGrade = index
	self:DoFilterGoodsInfo()
end

-- 进入藏宝阁链接(移动端自动登录)
function LuaTreasureHouseWnd:OnHiddenTreasureBtnClicked(go)
	--local setting = Cangbaoge_Setting.GetData()
	--CWebBrowserMgr.Inst:OpenUrl(setting.CangBaoGeURL)
--[[	if CommonDefs.DEVICE_PLATFORM == "pc" then
		local setting = Cangbaoge_Setting.GetData()
		CWebBrowserMgr.Inst:OpenUrl(setting.CangBaoGeURL)
	else
		CTreasureHouseMgr.Inst:GetUserTicket()
	end--]]
end

function LuaTreasureHouseWnd:UnisdkGetUserTickResult(argv)
	local url = SafeStringFormat3("https://reg.163.com/services/ticketlogin?ticket=%s&url=%s", argv[0], Cangbaoge_Setting.GetData().CangBaoGeURL)
	CWebBrowserMgr.Inst:OpenUrl(url)
end

-- 点击选择装评过滤
function LuaTreasureHouseWnd:OnScoreBtnClicked(go)
	local menuList = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))

	CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("装评不限"), DelegateFactory.Action_int(function (idx) 
                self:OnScoreSelected(idx)
            end), false, nil, EnumPopupMenuItemStyle.Light))

	for i = 1, #self.EquipScoreList, 1 do
		CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(SafeStringFormat3("%s ~ %s", tostring(self.EquipScoreList[i].low), tostring(self.EquipScoreList[i].high)), DelegateFactory.Action_int(function (idx) 
                self:OnScoreSelected(idx)
            end), false, nil, EnumPopupMenuItemStyle.Light))
	end
	CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(menuList), go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, nil, 350, true, 380)
end


-- 确认选择装评过滤
function LuaTreasureHouseWnd:OnScoreSelected(index)
	if self.SelectedEquipScore == index then return end
	if index == 0 then
		self.ScoreSelectBtn.Text = LocalString.GetString("装评不限")
	else
		local equipScoreSection = self.EquipScoreList[index]
		self.ScoreSelectBtn.Text = SafeStringFormat3("%s ~ %s", tostring(equipScoreSection.low), tostring(equipScoreSection.high))
	end
	self.SelectedEquipScore = index
	self:DoFilterGoodsInfo()
end

-- 根据选项对商品进行过滤
function LuaTreasureHouseWnd:DoFilterGoodsInfo()

	self.FilteredGoodsInfo = {}
	for i = 1, #self.GoodsInfo, 1 do
		local info = self.GoodsInfo[i]
		if self:CheckClass(info) and self:CheckGrade(info) and self:CheckEquipScore(info) then
			table.insert(self.FilteredGoodsInfo, self.GoodsInfo[i])
		end
	end
	self.TableViewDataSource.count=#self.FilteredGoodsInfo
	self.TableView:ReloadData(true,false)
	self.NoResultHint:SetActive(#self.FilteredGoodsInfo == 0)
end

-- 过滤职业信息
function LuaTreasureHouseWnd:CheckClass(info)
	if self.SelectedClass ~= 0 then
		-- 过滤职业
		local cls = self.ClassList[self.SelectedClass-1]
		return info.class == cls
	else
		return true
	end
end

-- 过滤等级信息
function LuaTreasureHouseWnd:CheckGrade(info)
	if self.SelectedGrade ~= 0 then
		local gradeSection = self.GradeList[self.SelectedGrade]
		return info.grade >= gradeSection.low and info.grade <= gradeSection.high
	else
		return true
	end
end

-- 过滤装评信息
function LuaTreasureHouseWnd:CheckEquipScore(info)
	if self.SelectedEquipScore ~= 0 then
		local equipScoreSection = self.EquipScoreList[self.SelectedEquipScore]
		return info.equipScore >= equipScoreSection.low and info.equipScore <= equipScoreSection.high
	else
		return true
	end
end


-- 初始化商品（账号）信息
function LuaTreasureHouseWnd:InitItem(item, index)
	-- 从过滤信息中读取
	local info = self.FilteredGoodsInfo[index + 1]
	if not info then return end
	local tf=item.transform

	local icon = tf:Find("PlayerIcon/Icon"):GetComponent(typeof(CUITexture))
	icon:Clear()
	local levelLabel = tf:Find("PlayerIcon/LevelLabel"):GetComponent(typeof(UILabel))
	local playerNameLabel = tf:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
	local playerScoreLabel = tf:Find("PlayerScoreLabel"):GetComponent(typeof(UILabel))
	local playerPriceLabel = tf:Find("PlayerPriceLabel"):GetComponent(typeof(UILabel))
	local playerAdvertiseLabel = tf:Find("PlayerAdvertiseLabel"):GetComponent(typeof(UILabel))

	local portraitName = CUICommonDef.GetPortraitName(info.class, info.gender, -1)
	icon:LoadNPCPortrait(portraitName,false)

	if info.xianfan == 1 then
		levelLabel.text = SafeStringFormat3("[%s]%s[-]", Constants.ColorOfFeiSheng, tostring(info.grade))
	else
		levelLabel.text = tostring(info.grade)
	end
	
	if info.id == CClientMainPlayer.Inst.Id then
		playerNameLabel.text = SafeStringFormat3("[00FF00]%s[-]", tostring(info.name)) 
	else
		playerNameLabel.text = tostring(info.name)
	end
	playerScoreLabel.text = SafeStringFormat3(LocalString.GetString("[91FAFF]装评[-] %s"), tostring(info.equipScore))
	playerPriceLabel.text = SafeStringFormat3(LocalString.GetString("[91FAFF]期望售价[-][FFFF00]￥%s[-]"), tostring(info.price))
	if not info.desc or info.desc == "" then
		playerAdvertiseLabel.text = LocalString.GetString("无")
	else
		playerAdvertiseLabel.text = SafeStringFormat3(LocalString.GetString("“%s”"), info.desc)
	end

	--local playerClicked = function (go)
		--CPlayerInfoMgr.ShowPlayerPopupMenu(info.id, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgrAlignType.Default)
	--end
	--CommonDefs.AddOnClickListener(icon.gameObject,DelegateFactory.Action_GameObject(playerClicked),false)

end

-- 商品点击效果
function LuaTreasureHouseWnd:OnSelectAtRow(row)
	local info = self.FilteredGoodsInfo[row + 1]
	if not info then return end
	CPlayerInfoMgr.ShowPlayerPopupMenu(info.id, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgrAlignType.Default)
end

function LuaTreasureHouseWnd:UpdatePlayerInfo(infos)
	self.GoodsInfo = infos
	self:DoFilterGoodsInfo()
end

return LuaTreasureHouseWnd
