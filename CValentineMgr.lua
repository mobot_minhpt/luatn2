-- Auto Generated!!
local CTrackMgr = import "L10.Game.CTrackMgr"
local DelegateFactory = import "DelegateFactory"


Gas2Gac.PreCheckGiveHelpValentineResult = function (targetPlayerId, itemTemplateId, result, sceneTemplateId, sceneId, x, y) 
    if not result then
        return
    end
    CTrackMgr.Inst:FindLocation(sceneId, sceneTemplateId, math.floor(x), math.floor(y), DelegateFactory.Action(function () 
        Gac2Gas.TryGiveHelpValentine(targetPlayerId, itemTemplateId)
    end), nil, nil, 0)
end
