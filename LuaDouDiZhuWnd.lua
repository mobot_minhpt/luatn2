local EChatPanel=import "L10.Game.EChatPanel"
local CSocialWndMgr=import "L10.UI.CSocialWndMgr"

local SoundManager=import "SoundManager"
local PlayerSettings=import "L10.Game.PlayerSettings"
local CCountdownController=import "L10.UI.CCountdownController"

local CIMMgr=import "L10.Game.CIMMgr"
local CJingLingMgr=import "L10.Game.CJingLingMgr"
local CGroupIMMgr=import "L10.Game.CGroupIMMgr"
local CMainCamera = import "L10.Engine.CMainCamera"

CLuaDouDiZhuWnd=class()
RegistClassMember(CLuaDouDiZhuWnd,"m_LeaveButton")
RegistClassMember(CLuaDouDiZhuWnd,"m_QuickChatButton")
RegistClassMember(CLuaDouDiZhuWnd,"m_TuoGuanButton")
RegistClassMember(CLuaDouDiZhuWnd,"m_ResetButton")
RegistClassMember(CLuaDouDiZhuWnd,"m_RankButton")
RegistClassMember(CLuaDouDiZhuWnd,"m_ResizeButton")


RegistClassMember(CLuaDouDiZhuWnd,"m_HandCardTfs")
RegistClassMember(CLuaDouDiZhuWnd,"m_PlayerViews")
RegistClassMember(CLuaDouDiZhuWnd,"m_DuZhuLabel")

RegistClassMember(CLuaDouDiZhuWnd,"m_TimeLabel")
RegistClassMember(CLuaDouDiZhuWnd,"m_CountdownCtl")

RegistClassMember(CLuaDouDiZhuWnd,"m_DiZhuMao")
RegistClassMember(CLuaDouDiZhuWnd,"m_Music")
RegistClassMember(CLuaDouDiZhuWnd,"m_Alert")



function CLuaDouDiZhuWnd:Init()
    if CommonDefs.IS_VN_CLIENT then
        local tuoGuanButton=FindChild(self.transform,"TuoGuanButton").gameObject
        tuoGuanButton.transform:Find("Label"):GetComponent(typeof(UILabel)).spacingX = 0
    end
    
    CLuaDouDiZhuMgr.m_CardTemplate=FindChild(self.transform,"CardTemplate").gameObject
    CLuaDouDiZhuMgr.m_CardTemplate:SetActive(false)


    self.m_DiZhuMao=FindChild(self.transform,"DiZhuMao").gameObject
    self.m_DiZhuMao:SetActive(false)

    self.m_TimeLabel=FindChild(self.transform,"TimeLabel"):GetComponent(typeof(UILabel))
    self.m_TimeLabel.text=nil
    self.m_CountdownCtl=self.m_TimeLabel:GetComponent(typeof(CCountdownController))
    self.m_CountdownCtl.OnTickEnd=DelegateFactory.Action(function()
        self.m_TimeLabel.text=nil
        -- print("time end")
    end)
    self.m_CountdownCtl.OnTick=DelegateFactory.Action_int(function(count)
        -- self:FormatTime(count)
        self.m_TimeLabel.text = self:FormatTime(count)
        -- print(count)
    end)

    self.m_DuZhuLabel=FindChild(self.transform,"DuZhuLabel"):GetComponent(typeof(UILabel))
    self:OnSyncDuZhuScore(CLuaDouDiZhuMgr.m_Score,CLuaDouDiZhuMgr.m_Power)
    -- self.m_DuZhuLabel.text=SafeStringFormat3( LocalString.GetString("底分 %d    倍率 %d"),CLuaDouDiZhuMgr.m_Score,CLuaDouDiZhuMgr.m_Power )

    self.m_LeaveButton=FindChild(self.transform,"LeaveButton").gameObject
    self.m_QuickChatButton=FindChild(self.transform,"QuickChatButton").gameObject
    self.m_ResetButton = self.transform:Find("Anchor/TopRight/Table/ResetButton").gameObject
    self.m_ResizeButton = self.transform:Find("Anchor/TopRight/Table/ResizeButton").gameObject
    self.m_RankButton = self.transform:Find("Anchor/TopRight/Table/RankButton").gameObject

    UIEventListener.Get(self.m_ResizeButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CUIResources.DouDiZhuWnd)
        CUIManager.ShowUI(CLuaUIResources.MiniDouDiZhuWnd)
    end)


    UIEventListener.Get(self.m_LeaveButton).onClick=LuaUtils.VoidDelegate(function(go)
        --离开场景
        local msgKey="DouDiZhu_Leave_Game"
        if CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eGuildFightPre or CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eGuildFightFinal then--家园
            msgKey="DouDiZhu_Fight_Leave_Game"
        elseif CLuaDouDiZhuMgr.m_Type == EnumDouDiZhuType.eRemoteQiangXiangZi then
            msgKey = "DouDiZhu_QiangXiangZi_Leave_Game"
        end
        if CLuaDouDiZhuMgr.m_IsWatch then
            msgKey="DouDiZhu_Watch_Leave_Game"
        end
        local msg=g_MessageMgr:FormatMessage(msgKey,{})
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
            Gac2Gas.RequestLeaveDouDiZhu()
        end), nil, nil, nil, false)
    end)

    UIEventListener.Get(self.m_QuickChatButton).onClick=LuaUtils.VoidDelegate(function(go)
        CLuaDouDiZhuMgr:ShowQuickChatWnd(self.m_QuickChatButton.transform.position, "doudizhu")
    end)

    self.m_TuoGuanButton=FindChild(self.transform,"TuoGuanButton").gameObject
    UIEventListener.Get(self.m_TuoGuanButton).onClick=LuaUtils.VoidDelegate(function(go)
        Gac2Gas.RequestDouDiZhuTuoGuan(true)
    end)    
    local chatButton=FindChild(self.transform,"ChatButton").gameObject
    UIEventListener.Get(chatButton).onClick=LuaUtils.VoidDelegate(function(go)
        if CUIManager.IsLoaded(CUIResources.SocialWnd) then
            CUIManager.CloseUI(CUIResources.SocialWnd)
        else
            CSocialWndMgr.ShowChatWnd(EChatPanel.Current)
        end
    end)    
    self.m_Alert=FindChild(chatButton.transform,"Alert"):GetComponent(typeof(UISprite))
    self:OnIMShouldShowAlertValueChange(nil)
    
    self.m_HandCardTfs={}
    table.insert( self.m_HandCardTfs, FindChild(self.transform,"HandCards1"))
    table.insert( self.m_HandCardTfs, FindChild(self.transform,"HandCards2"))
    table.insert( self.m_HandCardTfs, FindChild(self.transform,"HandCards3"))


    self.m_PlayerViews={}
    for i=1,3 do
        local playerView=CLuaDouDiZhuPlayerView:new()
        local node=FindChild(self.transform,"Player"..tostring(i))
        playerView:Init(node,i)
        table.insert( self.m_PlayerViews, playerView)
    end

    for k,v in pairs(CLuaDouDiZhuMgr.m_Play.m_IndexInfo) do
        local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(k)
        if self.m_PlayerViews[viewIndex] then
            self.m_PlayerViews[viewIndex]:OnSyncDouDiZhuPlayerInfo(v)
            if viewIndex==1 then
                if v.tuoGuan then
                    --托管状态
                    self:SetTuoGuanButtonActive(false)
                else
                    self:SetTuoGuanButtonActive(true)
                end
            end
        else
        end
    end


    -- local dipai=FindChild(self.transform,"DiPai")
    -- local cardTemplate=FindChild(dipai,"Card").gameObject
    -- cardTemplate:SetActive(false)

    if CLuaDouDiZhuMgr.m_Play then
        self:OnSyncDiZhuReserveCards()
    end

    -- local toprightbtn=FindChild(self.transform,"TopRightButton").gameObject
    -- local label=FindChild(toprightbtn.transform,"Label"):GetComponent(typeof(UILabel))

    if CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eRemoteQiangXiangZi then
        self.m_ResetButton:SetActive(false)
        self.m_RankButton:SetActive(false)

    elseif CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eJiaYuanPlay or
		CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eRemoteJiaYuanPlay or
		CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eGuildPlay then--家园
        self.m_ResetButton:SetActive(true)
        self.m_RankButton:SetActive(false)

        -- label.text=LocalString.GetString("重置积分")
        UIEventListener.Get(self.m_ResetButton).onClick=LuaUtils.VoidDelegate(function(go)
            local msg=g_MessageMgr:FormatMessage("DouDiZhu_ResetScore_Confirm",{})
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
                --请求重置
                Gac2Gas.RequestResetDouDiZhuScore()
            end), nil, nil, nil, false)
        end)

        if CLuaDouDiZhuMgr.m_IsWatch then
            self.m_ResetButton:SetActive(false)
        end
    elseif CLuaDouDiZhuMgr.m_Type == EnumDouDiZhuType.eCrossPlay then--跨服斗地主
        self.m_ResetButton:SetActive(false)
        self.m_RankButton:SetActive(true)

        -- label.text=LocalString.GetString("积分排行")
        UIEventListener.Get(self.m_RankButton).onClick=LuaUtils.VoidDelegate(function(go)
            CLuaCrossDouDiZhuMgr.OpenInGameRankWnd()
        end)
    else--帮会相关
        -- label.text=LocalString.GetString("积分排行")
        self.m_ResetButton:SetActive(false)
        self.m_RankButton:SetActive(true)
        UIEventListener.Get(self.m_RankButton).onClick=LuaUtils.VoidDelegate(function(go)
            Gac2Gas.QueryGuildDouDiZhuFightRank()
        end)
    end

    --观战玩家 
    if CLuaDouDiZhuMgr.m_IsWatch then
        self.m_QuickChatButton:SetActive(false)
        self.m_TuoGuanButton:SetActive(false)
    end
    local chat=FindChild(self.transform,"Chat").gameObject
    --比赛中不显示
    local isFight = CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eGuildFightPre or CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eGuildFightFinal
    if isFight then
        self.m_TuoGuanButton:SetActive(false)
        if not CLuaDouDiZhuMgr.m_IsWatch then--比赛非观战
            chat:SetActive(false)
        end
    end


    --table reposition
    self.m_ResizeButton:SetActive(CLuaDouDiZhuMgr.m_IsRemote)
    if CLuaDouDiZhuMgr.m_IsRemote then
        CLuaGuideMgr.TryTriggerRemoteDouDiZhuGuide()
    end
    local table = self.transform:Find("Anchor/TopRight/Table"):GetComponent(typeof(UITable))
    if table then
        table:Reposition()
    end


    CUIManager.CloseUI(CUIResources.SocialWnd)

    self:OnShowDouDiZhuRoundLeftTime(CLuaDouDiZhuMgr.m_ShowRoundLeftTime, CLuaDouDiZhuMgr.m_RoundLeftTime, CLuaDouDiZhuMgr.m_RoundTime)
end

function CLuaDouDiZhuWnd:SetTuoGuanButtonActive(active)
    if active then
        local isFight = CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eGuildFightPre or CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eGuildFightFinal
        if CLuaDouDiZhuMgr.m_IsWatch or isFight then
            self.m_TuoGuanButton:SetActive(false)
        else
            self.m_TuoGuanButton:SetActive(active)
        end
    else
        self.m_TuoGuanButton:SetActive(active)
    end
    local table = self.transform:Find("Anchor/TopRight/Table"):GetComponent(typeof(UITable))
    if table then
        table:Reposition()
    end
end





function CLuaDouDiZhuWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncDouDiZhuCardsInfo", self, "OnSyncDouDiZhuCardsInfo")
    g_ScriptEvent:AddListener("PlayerDouDiZhuReady", self, "OnPlayerDouDiZhuReady")
    g_ScriptEvent:AddListener("SyncDouDiZhuPlayerInfo", self, "OnSyncDouDiZhuPlayerInfo")
    g_ScriptEvent:AddListener("SyncDiZhuReserveCards", self, "OnSyncDiZhuReserveCards")
    g_ScriptEvent:AddListener("PlayerDouDiZhuTuoGuan", self, "OnPlayerDouDiZhuTuoGuan")
    g_ScriptEvent:AddListener("PlayerMingPai", self, "OnPlayerMingPai")
    g_ScriptEvent:AddListener("SyncQiangDiZhuProgress", self, "OnSyncQiangDiZhuProgress")
    g_ScriptEvent:AddListener("SyncChuPaiProgress", self, "OnSyncChuPaiProgress")
    g_ScriptEvent:AddListener("PlayerChuPai", self, "OnPlayerChuPai")
    g_ScriptEvent:AddListener("DouDiZhuStartRound", self, "OnDouDiZhuStartRound")
    g_ScriptEvent:AddListener("PlayerQiangDiZhu", self, "OnPlayerQiangDiZhu")
    g_ScriptEvent:AddListener("SyncDuZhuScore", self, "OnSyncDuZhuScore")
    g_ScriptEvent:AddListener("DiZhuAddReserveCards", self, "OnDiZhuAddReserveCards")
    g_ScriptEvent:AddListener("PlayerLeaveDouDiZhu", self, "OnPlayerLeaveDouDiZhu")
    g_ScriptEvent:AddListener("DouDiZhu_ClearMsg", self, "OnDouDiZhu_ClearMsg")
    g_ScriptEvent:AddListener("ShowPlayerDouDiZhuShortMessage", self, "OnShowPlayerDouDiZhuShortMessage")
    g_ScriptEvent:AddListener("ResetDouDiZhuRound", self, "OnResetDouDiZhuRound")
    g_ScriptEvent:AddListener("UpdateDouDiZhuPlayerScore", self, "OnUpdateDouDiZhuPlayerScore")
    g_ScriptEvent:AddListener("ShowDouDiZhuRoundLeftTime", self, "OnShowDouDiZhuRoundLeftTime")
    g_ScriptEvent:AddListener("DouDiZhuEndRound", self, "OnDouDiZhuEndRound")

    g_ScriptEvent:AddListener("IMShouldShowAlertValueChange", self, "OnIMShouldShowAlertValueChange")

    g_ScriptEvent:AddListener("ApplyJoinRemoteDouDiZhuPlayerListAlert", self, "OnApplyJoinRemoteDouDiZhuPlayerListAlert")

    g_ScriptEvent:AddListener("SyncQiangXiangZiRoundResult", self, "OnSyncQiangXiangZiRoundResult")

    if not CLuaDouDiZhuMgr.m_IsRemote then
        CMainCamera.Inst:SetCameraEnableStatus(false, "doudizhuwnd", false)
    end
end

function CLuaDouDiZhuWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncDouDiZhuCardsInfo", self, "OnSyncDouDiZhuCardsInfo")
    g_ScriptEvent:RemoveListener("PlayerDouDiZhuReady", self, "OnPlayerDouDiZhuReady")
    g_ScriptEvent:RemoveListener("SyncDouDiZhuPlayerInfo", self, "OnSyncDouDiZhuPlayerInfo")
    g_ScriptEvent:RemoveListener("SyncDiZhuReserveCards", self, "OnSyncDiZhuReserveCards")
    g_ScriptEvent:RemoveListener("PlayerDouDiZhuTuoGuan", self, "OnPlayerDouDiZhuTuoGuan")
    g_ScriptEvent:RemoveListener("PlayerMingPai", self, "OnPlayerMingPai")
    g_ScriptEvent:RemoveListener("SyncQiangDiZhuProgress", self, "OnSyncQiangDiZhuProgress")
    g_ScriptEvent:RemoveListener("SyncChuPaiProgress", self, "OnSyncChuPaiProgress")
    g_ScriptEvent:RemoveListener("PlayerChuPai", self, "OnPlayerChuPai")
    g_ScriptEvent:RemoveListener("DouDiZhuStartRound", self, "OnDouDiZhuStartRound")
    g_ScriptEvent:RemoveListener("PlayerQiangDiZhu", self, "OnPlayerQiangDiZhu")
    g_ScriptEvent:RemoveListener("SyncDuZhuScore", self, "OnSyncDuZhuScore")
    g_ScriptEvent:RemoveListener("DiZhuAddReserveCards", self, "OnDiZhuAddReserveCards")
    g_ScriptEvent:RemoveListener("PlayerLeaveDouDiZhu", self, "OnPlayerLeaveDouDiZhu")
    g_ScriptEvent:RemoveListener("DouDiZhu_ClearMsg", self, "OnDouDiZhu_ClearMsg")
    g_ScriptEvent:RemoveListener("ShowPlayerDouDiZhuShortMessage", self, "OnShowPlayerDouDiZhuShortMessage")
    g_ScriptEvent:RemoveListener("ResetDouDiZhuRound", self, "OnResetDouDiZhuRound")
    g_ScriptEvent:RemoveListener("UpdateDouDiZhuPlayerScore", self, "OnUpdateDouDiZhuPlayerScore")
    g_ScriptEvent:RemoveListener("ShowDouDiZhuRoundLeftTime", self, "OnShowDouDiZhuRoundLeftTime")
    g_ScriptEvent:RemoveListener("DouDiZhuEndRound", self, "OnDouDiZhuEndRound")

    g_ScriptEvent:RemoveListener("IMShouldShowAlertValueChange", self, "OnIMShouldShowAlertValueChange")
    g_ScriptEvent:RemoveListener("ApplyJoinRemoteDouDiZhuPlayerListAlert", self, "OnApplyJoinRemoteDouDiZhuPlayerListAlert")
    g_ScriptEvent:RemoveListener("SyncQiangXiangZiRoundResult", self, "OnSyncQiangXiangZiRoundResult")

    if not CLuaDouDiZhuMgr.m_IsRemote then
        CMainCamera.Inst:SetCameraEnableStatus(true, "doudizhuwnd", false)
    end
end

function CLuaDouDiZhuWnd:OnSyncQiangXiangZiRoundResult(isWin, isPlayOver, chestCount)
    self:RefreshRemoteQiangXiangZiView()
end

function CLuaDouDiZhuWnd:OnApplyJoinRemoteDouDiZhuPlayerListAlert()
    for i,v in ipairs(self.m_PlayerViews) do
        v:OnApplyJoinRemoteDouDiZhuPlayerListAlert()
    end
end
function CLuaDouDiZhuWnd:OnDouDiZhuEndRound( winnerIndex )
    --展示底牌
    for i,v in ipairs(self.m_PlayerViews) do
        v:OnDouDiZhuEndRound(winnerIndex)
    end
end
function CLuaDouDiZhuWnd:OnShowDouDiZhuRoundLeftTime(bShow, leftTime, time)
    if bShow then
        self.m_CountdownCtl:Init(leftTime,time)
    else
        self.m_CountdownCtl.start=false
    end
end

function CLuaDouDiZhuWnd:FormatTime(time)
    local minute=math.floor(time/60)
    local second=time%60
    return SafeStringFormat3("%02d:%02d",minute,second)
end

function CLuaDouDiZhuWnd:OnUpdateDouDiZhuPlayerScore(index,score)
    -- print("update score ",index,score)
    local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
    if self.m_PlayerViews[viewIndex] then
        self.m_PlayerViews[viewIndex]:OnUpdateDouDiZhuPlayerScore(score)
    end
end

function CLuaDouDiZhuWnd:OnResetDouDiZhuRound()
    for i,v in ipairs(self.m_PlayerViews) do
        v:OnResetDouDiZhuRound()
    end
    --重新显示托管按钮
    self:SetTuoGuanButtonActive(true)

    --底牌清掉
    self:OnSyncDiZhuReserveCards()
end

function CLuaDouDiZhuWnd:OnShowPlayerDouDiZhuShortMessage(index,id)
    local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
    if self.m_PlayerViews[viewIndex] then
        self.m_PlayerViews[viewIndex]:OnShowPlayerDouDiZhuShortMessage(id)
    end
end

function CLuaDouDiZhuWnd:OnPlayerLeaveDouDiZhu(index)
    local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
    -- print("OnPlayerLeaveDouDiZhu",viewIndex)
    if self.m_PlayerViews[viewIndex] then
        self.m_PlayerViews[viewIndex]:OnPlayerLeaveDouDiZhu()
    end
end

function CLuaDouDiZhuWnd:OnSyncDuZhuScore(score,power)
    if CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eRemoteQiangXiangZi then
        self.m_DuZhuLabel.transform.parent.gameObject:SetActive(false)
    elseif CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eGuildFightPre then
        self.m_DuZhuLabel.text=SafeStringFormat3( LocalString.GetString("[c][ffff61]突围赛 第%d局[-][/c] 底分 %d 倍率 %d"),CLuaDouDiZhuMgr.m_Round,score,power )
    elseif CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eGuildFightFinal then
        self.m_DuZhuLabel.text=SafeStringFormat3( LocalString.GetString("[c][ffff61]排位赛 第%d局[-][/c] 底分 %d 倍率 %d"),CLuaDouDiZhuMgr.m_Round,score,power )
    elseif CLuaDouDiZhuMgr.m_Type == EnumDouDiZhuType.eCrossPlay then
        self.m_DuZhuLabel.text=SafeStringFormat3( LocalString.GetString("[c][ffff61]%s[-][/c] 底分 %d    倍率 %d"),CLuaDouDiZhuMgr.m_RoundName,score,power )
    else
        self.m_DuZhuLabel.text=SafeStringFormat3( LocalString.GetString("底分 %d    倍率 %d"),score,power )
    end
end

function CLuaDouDiZhuWnd:RefreshRemoteQiangXiangZiView()
    if CLuaDouDiZhuMgr.m_Play then
        CLuaDouDiZhuMgr.m_Play:ResetRoundData()
    end

    for i, v in pairs(self.m_PlayerViews) do
        v:OnResetDouDiZhuRound()
    end
end

function CLuaDouDiZhuWnd:OnDiZhuAddReserveCards(index,info)
    local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
    if self.m_PlayerViews[viewIndex] then
        self.m_PlayerViews[viewIndex]:OnDiZhuAddReserveCards(info)
    end

    --做地主帽动画
end

function CLuaDouDiZhuWnd:OnDouDiZhuStartRound(winnerIndex, winnerPlayerId)
    for i,v in ipairs(self.m_PlayerViews) do
        v:OnDouDiZhuStartRound(winnerIndex)
    end
end

function CLuaDouDiZhuWnd:OnDouDiZhu_ClearMsg()
    for i,v in ipairs(self.m_PlayerViews) do
        v:ClearMsg()
    end
end

function CLuaDouDiZhuWnd:OnPlayerQiangDiZhu(index,status, bEnd, diZhuIndex)
    local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
    if self.m_PlayerViews[viewIndex] then
        self.m_PlayerViews[viewIndex]:OnPlayerQiangDiZhu(status)
    end
    if bEnd then
        viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(diZhuIndex)
        if self.m_PlayerViews[viewIndex] then
            self.m_PlayerViews[viewIndex]:SetDiZhu()
        end
        self.m_DiZhuMao:SetActive(false)
    else
        self.m_DiZhuMao:SetActive(true)
    end
end

function CLuaDouDiZhuWnd:OnDouDiZhuStartRound()
    for i,v in ipairs(self.m_PlayerViews) do
        v:OnDouDiZhuStartRound()
    end
end

function CLuaDouDiZhuWnd:OnSyncDouDiZhuCardsInfo(index,info,bFaPai)
    local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
    if self.m_PlayerViews[viewIndex] then
        self.m_PlayerViews[viewIndex]:OnSyncDouDiZhuCardsInfo(info,bFaPai)
    end
end
function CLuaDouDiZhuWnd:OnPlayerDouDiZhuReady(index, playerId, ready)
    local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
    if CLuaDouDiZhuMgr.m_Play and  CLuaDouDiZhuMgr.m_Play:IsAllReady() then
        for i,v in ipairs(self.m_PlayerViews) do
            v:OnPlayerDouDiZhuReady(true)
        end
    else
        self.m_PlayerViews[viewIndex]:OnPlayerDouDiZhuReady(ready)
    end
end
function CLuaDouDiZhuWnd:OnSyncDouDiZhuPlayerInfo(index,info)
    local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
    self.m_PlayerViews[viewIndex]:OnSyncDouDiZhuPlayerInfo(info)
end

function CLuaDouDiZhuWnd:OnSyncDiZhuReserveCards()
    local cards=CLuaDouDiZhuMgr.m_Play.m_ReserveCards
    local dipai=FindChild(self.transform,"DiPai")
    local grid=FindChild(dipai,"Grid").gameObject
    CUICommonDef.ClearTransform(grid.transform)
    if cards then
        for i,v in ipairs(cards) do
            -- print("card",i,v)
            local card = NGUITools.AddChild(grid,CLuaDouDiZhuMgr.m_CardTemplate)
            card:SetActive(true)
            CLuaDouDiZhuMgr.SetCard(card.transform,v,i)
        end
        grid:GetComponent(typeof(UIGrid)):Reposition()
    end
end

function CLuaDouDiZhuWnd:OnPlayerDouDiZhuTuoGuan(index,playerId,bTuoGuan)
    local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
    if self.m_PlayerViews[viewIndex] then
        self.m_PlayerViews[viewIndex]:OnPlayerDouDiZhuTuoGuan(bTuoGuan)
    end
    if viewIndex==1 then
        if bTuoGuan then
            --托管状态
            self:SetTuoGuanButtonActive(false)
        else
            self:SetTuoGuanButtonActive(true)
        end
    end
end

function CLuaDouDiZhuWnd:OnPlayerMingPai(index)
    local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
    if self.m_PlayerViews[viewIndex] then
        local info=CLuaDouDiZhuMgr.m_Play:GetIndexInfo(index)
        self.m_PlayerViews[viewIndex]:OnPlayerMingPai(info)
    end
end

function CLuaDouDiZhuWnd:OnSyncQiangDiZhuProgress(index,bIsJiaoDiZhu, leftTime)
    local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
    if self.m_PlayerViews[viewIndex] then
        self.m_PlayerViews[viewIndex]:OnSyncQiangDiZhuProgress(bIsJiaoDiZhu, leftTime)
    end
    self.m_DiZhuMao:SetActive(true)
end

function CLuaDouDiZhuWnd:OnSyncChuPaiProgress(index,leftTime, maxCardIndex)
    local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
    if self.m_PlayerViews[viewIndex] then
        self.m_PlayerViews[viewIndex]:OnSyncChuPaiProgress(leftTime, maxCardIndex)
    end
end

function CLuaDouDiZhuWnd:OnPlayerChuPai(index)
    local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
    if self.m_PlayerViews[viewIndex] then
        self.m_PlayerViews[viewIndex]:OnPlayerChuPai()
    end
end
function CLuaDouDiZhuWnd:Awake()
    if not CLuaDouDiZhuMgr.m_IsRemote then
        SoundManager.s_BlockBgMusic=true
        SoundManager.Inst:StopBGMusic()
        --播放斗地主背景音乐
        if PlayerSettings.MusicEnabled then
            self.m_Music=SoundManager.Inst:PlaySound("event:/L10/L10_music/MUS_JinLing",Vector3.zero, 1, nil, 0)
        end
    end

    self.transform:Find("Bg/Background").gameObject:SetActive(not CLuaDouDiZhuMgr.m_IsRemote)
end

function CLuaDouDiZhuWnd:OnIMShouldShowAlertValueChange(args)
    if CLuaDouDiZhuMgr.m_Type == EnumDouDiZhuType.eRemoteQiangXiangZi then
        self.m_Alert.enabled = false
        return
    end

    local immgrInst=CIMMgr.Inst
    self.m_Alert.enabled=immgrInst.ShowIMAlert_ContainSystemMsg 
                        or CJingLingMgr.Inst.ExistsUnreadMsgs 
                        or immgrInst.ShouldShowFriendRequestAlert
                        or CGroupIMMgr.Inst.ShowGroupIMAlert_NotContainIgnore
end

function CLuaDouDiZhuWnd:OnDestroy()
    if not CLuaDouDiZhuMgr.m_IsRemote then
        SoundManager.s_BlockBgMusic=false
        if self.m_Music then
            SoundManager.Inst:StopSound(self.m_Music)
            self.m_Music=nil
        end
        SoundManager.Inst:StartBGMusic()
    end
    for i,v in ipairs(self.m_PlayerViews) do
        v:OnDestroy()
    end 
    self.m_TimeLabel=nil
    self.m_CountdownCtl=nil
end


function CLuaDouDiZhuWnd:GetGuideGo(methodName)
    if methodName == "GetResizeButton" then
        return self.m_ResizeButton
    end
    return nil
end