local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local CButton = import "L10.UI.CButton"
local CPackageItemCell = import "L10.UI.CPackageItemCell"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaAntiProfessionConvertResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaAntiProfessionConvertResultWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaAntiProfessionConvertResultWnd, "CommitBtn", "CommitBtn", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaAntiProfessionConvertResultWnd, "m_Infos")

function LuaAntiProfessionConvertResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaAntiProfessionConvertResultWnd:Init()
    if LuaZongMenMgr.m_AntiProfessionConvertResult == nil then
        CUIManager.CloseUI(CLuaUIResources.AntiProfessionConvertResultWnd)
    end

    UIEventListener.Get(self.CommitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CUIManager.CloseUI(CLuaUIResources.AntiProfessionConvertResultWnd)
	end)

    self:InitTable()
end

--@region UIEvent

--@endregion UIEvent

function LuaAntiProfessionConvertResultWnd:InitTable()
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_Infos
        end,
        function(item, index)
            self:InitItem(item, self.m_Infos[index + 1])
        end
    )

    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local info = self.m_Infos[row+1]
        local tableItem = self.TableView:GetItemAtRow(row)
        local selectedItemCell = tableItem.transform:GetComponent(typeof(CPackageItemCell))

        for i=1,#self.m_Infos do
            local tableItem = self.TableView:GetItemAtRow(i-1)
            local itemCell = tableItem.transform:GetComponent(typeof(CPackageItemCell))
            if itemCell ~= nil and itemCell ~= selectedItemCell then
                itemCell.Selected = false
            end
        end

        selectedItemCell.Selected = true

        local itemId = SoulCore_Settings.GetData().AntiProfessionItemId[info.Profession - 1][1]
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)

    self:RefreshData()
end

function LuaAntiProfessionConvertResultWnd:RefreshData()
    self.m_Infos = {}

    for k, v in pairs(LuaZongMenMgr.m_AntiProfessionConvertResult) do
        table.insert(self.m_Infos, {Profession = k, Num = v})
    end

    table.sort(self.m_Infos, function (a, b)
        if a.Num > b.Num then
            return true
        elseif a.Num == b.Num then
            return a.Profession < b.Profession
        end
        return false
    end)
    self.TableView:ReloadData(true, false) 
end

function LuaAntiProfessionConvertResultWnd:InitItem(item, info)
    local itemCell = item:GetComponent(typeof(CPackageItemCell))
    itemCell:Init("", false, false, false, nil)
    itemCell.Selected = false
    itemCell.iconTexture:LoadMaterial(LuaZongMenMgr:GetProfessionScoreIconPath(info.Profession))
    if info.Num == 1 then
        itemCell.amountLabel.text = ""
    else
        itemCell.amountLabel.text = info.Num 
    end
end

function LuaAntiProfessionConvertResultWnd:OnDestroy()
    LuaZongMenMgr.m_AntiProfessionConvertResult = nil
end
