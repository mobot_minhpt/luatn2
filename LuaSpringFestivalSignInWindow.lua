local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaSpringFestivalSignInWindow = class()

RegistChildComponent(LuaSpringFestivalSignInWindow, "Label", UILabel)
RegistChildComponent(LuaSpringFestivalSignInWindow, "BonusTemplate1", GameObject)
RegistChildComponent(LuaSpringFestivalSignInWindow, "BonusTemplate2", GameObject)
RegistChildComponent(LuaSpringFestivalSignInWindow, "BonusTemplate3", GameObject)
RegistChildComponent(LuaSpringFestivalSignInWindow, "BonusTemplate4", GameObject)
RegistChildComponent(LuaSpringFestivalSignInWindow, "BonusTemplate5", GameObject)
RegistChildComponent(LuaSpringFestivalSignInWindow, "BonusTemplate6", GameObject)
RegistChildComponent(LuaSpringFestivalSignInWindow, "BonusTemplate7", GameObject)
RegistChildComponent(LuaSpringFestivalSignInWindow, "BonusTemplate8", GameObject)
RegistChildComponent(LuaSpringFestivalSignInWindow, "BonusTemplate9", GameObject)

RegistClassMember(LuaSpringFestivalSignInWindow, "m_SpringFestivalBatchId")
RegistClassMember(LuaSpringFestivalSignInWindow, "m_BonusList")

--@region RegistChildComponent: Dont Modify Manually!


--@endregion RegistChildComponent end

function LuaSpringFestivalSignInWindow:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaSpringFestivalSignInWindow:Init()
    self.m_SpringFestivalBatchId = 75
    self.m_BonusList = {}
    table.insert(self.m_BonusList, self.BonusTemplate1)
    table.insert(self.m_BonusList, self.BonusTemplate2)
    table.insert(self.m_BonusList, self.BonusTemplate3)
    table.insert(self.m_BonusList, self.BonusTemplate4)
    table.insert(self.m_BonusList, self.BonusTemplate5)
    table.insert(self.m_BonusList, self.BonusTemplate6)
    table.insert(self.m_BonusList, self.BonusTemplate7)
    table.insert(self.m_BonusList, self.BonusTemplate8)
    table.insert(self.m_BonusList, self.BonusTemplate9)
    QianDao_Holiday.Foreach(function (key, data)
        self:InitSpringFestivalSignin(key, data)
    end)
end

function LuaSpringFestivalSignInWindow:InitSpringFestivalSignin(key, data)
	if data.Open == 0 or data.BatchId ~= self.m_SpringFestivalBatchId then return end
    local instance = self.m_BonusList[data.NeedTimes]
    instance:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:InitBonus(key)
    self.Label.text = data.SubTitle
end
--@region UIEvent

--@endregion UIEvent

