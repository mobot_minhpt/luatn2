-- Auto Generated!!
local Callback = import "EventDelegate+Callback"
local CDuoMaoMaoMgr = import "L10.Game.CDuoMaoMaoMgr"
local CHideAndSeekSmallTypeWnd = import "L10.UI.CHideAndSeekSmallTypeWnd"
local EventDelegate = import "EventDelegate"
local TweenPosition = import "TweenPosition"
local Vector3 = import "UnityEngine.Vector3"
CHideAndSeekSmallTypeWnd.m_Awake_CS2LuaHook = function (this) 
    local tweener = CommonDefs.GetComponent_GameObject_Type(this.TweenGO, typeof(TweenPosition))
    if tweener ~= nil then
        EventDelegate.Add(tweener.onFinished, MakeDelegateFromCSFunction(this.onTweenFinished, Callback, this))
        tweener.enabled = false
    end
end
CHideAndSeekSmallTypeWnd.m_onTweenFinished_CS2LuaHook = function (this) 
    local tweener = CommonDefs.GetComponent_GameObject_Type(this.TweenGO, typeof(TweenPosition))
    if tweener ~= nil then
        tweener.enabled = false
    end

    if not this.showAllButtonSelectedSprite.activeSelf then
        this.AllPartGO:SetActive(false)
        this.SingleRowPartGO:SetActive(true)
        this.expandBtn:SetActive(true)

        local zhuangshiwuId = CDuoMaoMaoMgr.Inst:GetCurrentZhuangshiwuId()
        do
            local i = 0
            while i < this.SmallItemList.Count do
                this.SmallItemList[i]:Reinit()
                if this.SmallItemList[i].TemplateId == zhuangshiwuId then
                    this.SmallItemList[i]:SetSelected(true)
                end
                i = i + 1
            end
        end
    end
end
CHideAndSeekSmallTypeWnd.m_OnExpandBtnClicked_CS2LuaHook = function (this, go) 
    if not this.showAllButtonSelectedSprite.activeSelf then
        local rotation = this.expandBtn.transform.eulerAngles
        this.expandBtn.transform.eulerAngles = Vector3(rotation.x, rotation.y, 180 - rotation.z)

        this.SingleRowPartGO:SetActive(not this.SingleRowPartGO.activeSelf)
    end
end
CHideAndSeekSmallTypeWnd.m_OnShowAllButtonClicked_CS2LuaHook = function (this, go) 
    local bShowAll = not this.showAllButtonSelectedSprite.activeSelf
    this.showAllButtonSelectedSprite:SetActive(bShowAll)

    if bShowAll then
        this.AllPartGO:SetActive(true)
        this.SingleRowPartGO:SetActive(false)
        this.expandBtn:SetActive(false)

        local zhuangshiwuId = CDuoMaoMaoMgr.Inst:GetCurrentZhuangshiwuId()
        do
            local i = 0
            while i < this.AllSmallItemList.Count do
                this.AllSmallItemList[i]:Reinit()
                if this.AllSmallItemList[i].TemplateId == zhuangshiwuId then
                    this.AllSmallItemList[i]:SetSelected(true)
                end
                i = i + 1
            end
        end
    end

    this:PlayPositionTweener(bShowAll)

    this.table:Reposition()
    this.scrollView:ResetPosition()
    this.allTable:Reposition()
    this.allScrollView:ResetPosition()
end
CHideAndSeekSmallTypeWnd.m_PlayPositionTweener_CS2LuaHook = function (this, bShowAll) 
    local tweener = CommonDefs.GetComponent_GameObject_Type(this.TweenGO, typeof(TweenPosition))
    if tweener ~= nil then
        tweener.enabled = true
        if bShowAll then
            tweener:PlayForward()
        else
            tweener:PlayReverse()
        end
    end
end
CHideAndSeekSmallTypeWnd.m_OnSmallItemClicked_CS2LuaHook = function (this, go) 
    if this.showAllButtonSelectedSprite.activeSelf then
        do
            local i = 0
            while i < this.AllSmallItemList.Count do
                if this.AllSmallItemList[i].gameObject == go then
                    this.AllSmallItemList[i]:OnClicked()
                else
                    this.AllSmallItemList[i]:SetSelected(false)
                end
                i = i + 1
            end
        end
    else
        do
            local i = 0
            while i < this.SmallItemList.Count do
                if this.SmallItemList[i].gameObject == go then
                    this.SmallItemList[i]:OnClicked()
                else
                    this.SmallItemList[i]:SetSelected(false)
                end
                i = i + 1
            end
        end
    end
end
