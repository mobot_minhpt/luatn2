LuaGuildExternalAidMgr = {}

EnumGuildForeignAidApplyInfoType = { --和服务器定义保持一致
	eWaitGuildAccept = 1,
	eWaitAidAccept = 2,
	eNormal = 3,
	eElite = 4,
	eMax = 4,
}

EnumExternalAidGamePlay ={
    eGuildTerritoryWar = 0,
    eGuildLeagueCross = 1,
}

function LuaGuildExternalAidMgr:OnGuildExternalAidWndClose()
    --这里的实现有点Hack，在关闭AidWnd的时候，如果征召界面打开，尝试刷新一下, 相当于点击界面中的手动刷新
    if CUIManager.IsLoaded("GuildLeagueConveneWnd") then
        Gac2Gas.RequestOpenGuildCallUpWnd()
    end
end

LuaGuildExternalAidMgr.m_DefaultPlayerIdForAidWnd = 0
function LuaGuildExternalAidMgr:ShowGuildExternalAidWnd(gameplayContext, targetPlayerId)
    self.m_DefaultPlayerIdForAidWnd = targetPlayerId and targetPlayerId or 0
    if gameplayContext==nil then
        gameplayContext = self:GetGameplayContextByTime()
    end
    if gameplayContext==nil or gameplayContext == EnumExternalAidGamePlay.eGuildTerritoryWar then
        self:RequestGuildForeignAidInfo(EnumExternalAidGamePlay.eGuildTerritoryWar)
    elseif gameplayContext == EnumExternalAidGamePlay.eGuildLeagueCross then
        self:RequestGuildForeignAidInfo(EnumExternalAidGamePlay.eGuildLeagueCross)
    end
end

function LuaGuildExternalAidMgr:ShowGuildExternalAidCardWnd(gameplayContext)
    if gameplayContext==nil then
        gameplayContext = self:GetGameplayContextByTime()
    end
    if gameplayContext==nil or gameplayContext == EnumExternalAidGamePlay.eGuildTerritoryWar then
        self:RequestGuildForeignAidPersonalInfo(EnumExternalAidGamePlay.eGuildTerritoryWar)
    elseif gameplayContext == EnumExternalAidGamePlay.eGuildLeagueCross then
        self:RequestGuildForeignAidPersonalInfo(EnumExternalAidGamePlay.eGuildLeagueCross)
    end
end
--由于一开始外援功能的通用处理没有考虑到不同玩法，一些地方缺乏上下文，如果context缺失，提供一个临时补救措施，根据玩法开放时间来判断是什么上下文
--基于的前提是使用通用外援功能的玩法开放时间不能重叠
function LuaGuildExternalAidMgr:GetGameplayContextByTime()
    if LuaGuildLeagueCrossMgr:IsDuringOpenTime() then
        return EnumExternalAidGamePlay.eGuildLeagueCross
    end
    return nil
end

function LuaGuildExternalAidMgr:NeedShowEliteOnAidWnd(gameplayContext)
    return gameplayContext == EnumExternalAidGamePlay.eGuildLeagueCross
end

function LuaGuildExternalAidMgr:GetNoAidCardDesc(gameplayContext)
    if gameplayContext == EnumExternalAidGamePlay.eGuildTerritoryWar then
        return g_MessageMgr:FormatMessage("GuildExternalAidCardWnd_NoneCard")
    elseif gameplayContext == EnumExternalAidGamePlay.eGuildLeagueCross then
        return g_MessageMgr:FormatMessage("GuildExternalAidCardWnd_NoneCard_GuildLeagueCross")
    else
        return ""
    end
end

function LuaGuildExternalAidMgr:GetAidCardTipMessageName(gameplayContext)
    if gameplayContext == EnumExternalAidGamePlay.eGuildTerritoryWar then
        return "GuildExternalAidCardWnd_ReadMe"
    elseif gameplayContext == EnumExternalAidGamePlay.eGuildLeagueCross then
        return "GuildExternalAidCardWnd_ReadMe_GuildLeagueCross"
    else
        return ""
    end
end
------------------------
---Gac2Gas
------------------------
function LuaGuildExternalAidMgr:RequestGuildForeignAidInfo(gameplayContext)
	Gac2Gas.RequestGuildForeignAidInfo(gameplayContext)
end
function LuaGuildExternalAidMgr:RequestGuildForeignAidApplyInfo() --洪磊感觉添加context涉及方法太多，这里暂时没有context
	Gac2Gas.RequestGuildForeignAidApplyInfo()
end
function LuaGuildExternalAidMgr:GuildForeignAidInvitePlayer(playerId)
	Gac2Gas.GuildForeignAidInvitePlayer(playerId)
end
function LuaGuildExternalAidMgr:GuildForeignAidAcceptPlayer(playerId)
	Gac2Gas.GuildForeignAidAcceptPlayer(playerId)
end
function LuaGuildExternalAidMgr:GuildForeignAidRefusePlayer(playerId)
	Gac2Gas.GuildForeignAidRefusePlayer(playerId)
end
function LuaGuildExternalAidMgr:GuildForeignAidDeletePlayer(playerId, gameplayContext)
	Gac2Gas.GuildForeignAidDeletePlayer(playerId, gameplayContext)
end
function LuaGuildExternalAidMgr:GuildForeignAidRefuseAll()
	Gac2Gas.GuildForeignAidRefuseAll()
end
function LuaGuildExternalAidMgr:RequestGuildForeignAidPersonalInfo(gameplayContext)
	Gac2Gas.RequestGuildForeignAidPersonalInfo(gameplayContext)
end
function LuaGuildExternalAidMgr:RequestGuildForeignAidInviteInfo(gameplayContext)
	Gac2Gas.RequestGuildForeignAidInviteInfo(gameplayContext)
end
function LuaGuildExternalAidMgr:GuildForeignAidApply(guildId, declaration)
	Gac2Gas.GuildForeignAidApply(guildId, declaration)
end
function LuaGuildExternalAidMgr:GuildForeignAidCancelApply(guildId)
	Gac2Gas.GuildForeignAidCancelApply(guildId)
end
function LuaGuildExternalAidMgr:RequestSetGuildForeignAidType(playerId, isElite)
    Gac2Gas.RequestSetGuildForeignAidType(playerId, 
        isElite and EnumGuildForeignAidApplyInfoType.eElite or EnumGuildForeignAidApplyInfoType.eNormal)
end
-------------------------

--帮会通用外援
LuaGuildExternalAidMgr.m_ForeignAidInfo = {}
function LuaGuildExternalAidMgr:SendGuildForeignAidInfo(hasApplication, canApply, maxAidCount, maxEliteAidCount, aidInfoUd, gameplayContext)
    local list = aidInfoUd and MsgPackImpl.unpack(aidInfoUd)
    self.m_ForeignAidInfo = {}
    self.m_ForeignAidInfo.m_DataTbl = {}
    self.m_ForeignAidInfo.m_CanApply = canApply
    self.m_ForeignAidInfo.m_AidOnlineNum = 0
    self.m_ForeignAidInfo.m_MaxAidOnlineCount = maxAidCount
    self.m_ForeignAidInfo.m_EliteAidOnlineNum = 0
    self.m_ForeignAidInfo.m_MaxEliteAidOnlineCount = maxEliteAidCount
    self.m_ForeignAidInfo.m_Context = gameplayContext
    for i= 0, list.Count-1, 14 do
        local playerId = list[i]
        local playerName = list[i+1]
        local level = list[i+2]
        local class = list[i+3]
        local hasFeiSheng = list[i+4]
        local xiuwei = list[i+5]
        local xiulian = list[i+6]
        local zhanli = list[i+7]
        local guildId = list[i+8]
        local guildName = list[i+9]
        local canFightTime = list[i+10]
        local isOnline = list[i+11]
        local equipScore = list[i+12]
        local isElite = (list[i+13] == EnumGuildForeignAidApplyInfoType.eElite)
        if isOnline then
            self.m_ForeignAidInfo.m_AidOnlineNum = self.m_ForeignAidInfo.m_AidOnlineNum + 1
            if isElite then
                self.m_ForeignAidInfo.m_EliteAidOnlineNum = self.m_ForeignAidInfo.m_EliteAidOnlineNum + 1
            end
        end
        table.insert(self.m_ForeignAidInfo.m_DataTbl, {
            playerId = playerId, playerName = playerName, level = level, class = class,hasFeiSheng = hasFeiSheng,
            xiuwei = xiuwei, xiulian = xiulian, zhanli = zhanli, guildId = guildId, guildName = guildName,
            canFightTime = canFightTime, isOnline = isOnline, equipScore = equipScore, isElite = isElite,
        })
    end
    if CUIManager.IsLoaded(CLuaUIResources.GuildExternalAidWnd) then
        g_ScriptEvent:BroadcastInLua("OnSendGuildForeignAidInfo",hasApplication)
        return 
    end
   CUIManager.ShowUI(CLuaUIResources.GuildExternalAidWnd)
end

function LuaGuildExternalAidMgr:SendGuildForeignAidApplicationStatus(canApply)
    g_ScriptEvent:BroadcastInLua("OnSendGuildForeignAidApplicationStatus",canApply)
end

LuaGuildExternalAidMgr.m_ApplyInfo = {}
function LuaGuildExternalAidMgr:SendGuildForeignAidApplyInfo(canApply, gameplayContext,applyInfoUd)
    self.m_ForeignAidInfo.m_CanApply = canApply
    self.m_ForeignAidInfo.m_Context = gameplayContext
    local list = applyInfoUd and MsgPackImpl.unpack(applyInfoUd)
    self.m_ApplyInfo = {}
    for i= 0, list.Count-1, 12 do
        local playerId = list[i]
        local playerName = list[i+1]
        local level = list[i+2]
        local class = list[i+3]
        local hasFeiSheng = list[i+4]
        local xiuwei = list[i+5]
        local xiulian = list[i+6]
        local zhanli = list[i+7]
        local guildId = list[i+8]
        local guildName = list[i+9]
        local declaration = list[i+10]
        local equipScore = list[i+11]
        table.insert(self.m_ApplyInfo, {
            playerId = playerId, playerName = playerName, level = level, class = class,hasFeiSheng = hasFeiSheng,
            xiuwei = xiuwei, xiulian = xiulian, zhanli = zhanli, guildId = guildId, guildName = guildName,
            declaration = declaration, equipScore = equipScore
        })
    end
    g_ScriptEvent:BroadcastInLua("OnSendGuildForeignAidApplyInfo")
end

LuaGuildExternalAidMgr.m_PersonalInfo = {}
LuaGuildExternalAidMgr.m_CardInfo = {}
function LuaGuildExternalAidMgr:SendGuildForeignAidPersonalInfo(guildId, guildName, aidType, canFightTime, hasInvite, personalInfoUd, gameplayContext)

    local list = personalInfoUd and MsgPackImpl.unpack(personalInfoUd)
    self.m_PersonalInfo = {}
    for i= 0, list.Count-1, 7 do
        local guildId = list[i]
        local guildName = list[i+1]
        local leagueLevel = list[i+2]
        local leagueRank = list[i+3]
        local leaderId = list[i+4]
        local leaderName = list[i+5]
        local applyState = list[i+6] --1,已申请;2,已同意,3关闭申请
        -- if leagueLevel ~= 0 and leagueRank ~= 0 then
            table.insert(self.m_PersonalInfo,{
                guildId = guildId, guildName = guildName, leagueLevel = leagueLevel, leagueRank = leagueRank,
                leaderId = leaderId, leaderName = leaderName, applyState = applyState
            })
        -- end
    end
    self.m_CardInfo = {guildId = guildId, guildName = guildName, aidType = aidType, canFightTime = canFightTime, hasInvite = hasInvite, context = gameplayContext}
    if CUIManager.IsLoaded(CLuaUIResources.GuildExternalAidCardWnd) then
        g_ScriptEvent:BroadcastInLua("OnSendGuildForeignAidPersonalInfo")
        return 
    end
    CUIManager.ShowUI(CLuaUIResources.GuildExternalAidCardWnd)
end

LuaGuildExternalAidMgr.m_InviteInfo = {}
function LuaGuildExternalAidMgr:SendGuildForeignAidInviteInfo(inviteInfoUd, gameplayContext)
    --gameplayContext上下文暂时用不到，后面有需要再添加逻辑
    self.m_InviteInfo = {}
    local list = inviteInfoUd and MsgPackImpl.unpack(inviteInfoUd)
    for i= 0, list.Count-1, 7 do
        local guildId = list[i]
        local guildName = list[i+1]
        local leagueLevel = list[i+2]
        local leagueRank = list[i+3]
        local leaderId = list[i+4]
        local leaderName = list[i+5]
        local expireTime = list[i+6]
        table.insert(self.m_InviteInfo,{
            guildId = guildId, guildName = guildName, leagueLevel = leagueLevel, leagueRank = leagueRank,
            leaderId = leaderId, leaderName = leaderName,expireTime = expireTime
        })
    end
    if #self.m_InviteInfo == 0 then 
        CUIManager.CloseUI(CLuaUIResources.GuildExternalAidInvitationWnd)
        return 
    end
    if CUIManager.IsLoaded(CLuaUIResources.GuildExternalAidInvitationWnd) then
        g_ScriptEvent:BroadcastInLua("OnSendGuildForeignAidInviteInfo")
        return 
    end
    CUIManager.ShowUI(CLuaUIResources.GuildExternalAidInvitationWnd)
end

function LuaGuildExternalAidMgr:SendGuildForeignAidApplyStatusChange(guildId, isApply)
    if not CUIManager.IsLoaded(CLuaUIResources.GuildExternalAidCardWnd) then
        return 
    end
    self:RequestGuildForeignAidPersonalInfo(self.m_CardInfo.context)
    if CUIManager.IsLoaded(CLuaUIResources.GuildExternalAidInvitationWnd) then
        self:RequestGuildForeignAidInviteInfo(self.m_CardInfo.context)
    end
end

function LuaGuildExternalAidMgr:SendGuildForeignAidPlayerBeAid(guildId)
    if not CUIManager.IsLoaded(CLuaUIResources.GuildExternalAidCardWnd) then
        return 
    end
    self:RequestGuildForeignAidPersonalInfo(self.m_CardInfo.context)
    CUIManager.CloseUI(CLuaUIResources.GuildExternalAidInvitationWnd)
end

function LuaGuildExternalAidMgr:SetGuildForeignAidTypeSuccess(targetPlayerId, aidType)
    if not self.m_ForeignAidInfo.m_DataTbl and not CUIManager.IsLoaded(CLuaUIResources.GuildExternalAidWnd) then return end
    --影响精英在线人数
    self.m_ForeignAidInfo.m_EliteAidOnlineNum = 0
    for __,data in pairs(self.m_ForeignAidInfo.m_DataTbl) do
        if data.playerId == targetPlayerId then
            data.isElite = (aidType == EnumGuildForeignAidApplyInfoType.eElite)
        end
        
        if data.isOnline and data.isElite then
            self.m_ForeignAidInfo.m_EliteAidOnlineNum = self.m_ForeignAidInfo.m_EliteAidOnlineNum + 1
        end
    end

    g_ScriptEvent:BroadcastInLua("OnSetGuildForeignAidTypeSuccess",targetPlayerId)
end