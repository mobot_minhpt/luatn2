local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CScene = import "L10.Game.CScene"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CPos = import "L10.Engine.CPos"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CTaskInfoMgr = import "L10.UI.CTaskInfoMgr"
local CTaskListItem = import "L10.UI.CTaskListItem"
local Lua = import "L10.Engine.Lua"
local EnumObjectType = import "L10.Game.EnumObjectType"
local COpenEntryMgr = import "L10.Game.COpenEntryMgr"
local EnumMenuItem = import "L10.Game.EnumMenuItem"
local CStatusMgr = import "L10.Game.CStatusMgr"
local EPropStatus = import "L10.Game.EPropStatus"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"

if rawget(_G, "LuaXinBaiLianDongMgr") then
    g_ScriptEvent:RemoveListener("GasDisconnect", LuaXinBaiLianDongMgr, "OnDisconnect")
end

LuaXinBaiLianDongMgr = {}

g_ScriptEvent:AddListener("GasDisconnect", LuaXinBaiLianDongMgr, "OnDisconnect")

function LuaXinBaiLianDongMgr:OnDisconnect()
    LuaXinBaiLianDongMgr.ProgressInfo = {}
    
    LuaXinBaiLianDongMgr.IsShenYaoOpen = false
    LuaXinBaiLianDongMgr.IsShenYaoStart = false
end

--#region 善妖结缘

LuaXinBaiLianDongMgr.SYJYInfo = {}
LuaXinBaiLianDongMgr.SYJYStoryMsg = ""

function LuaXinBaiLianDongMgr:ShowShanYaoJieYuanWnd(data)
    self.SYJYInfo.followId = nil
    self.SYJYInfo.score = 0
    self.SYJYInfo.unlock = {}

    local rawData = g_MessagePack.unpack(data)["args"]
    if rawData[1] then
        for k, v in pairs(rawData[1]) do
            if k == "Enable" then
                self.SYJYInfo.followId = v
            elseif k == "Score" then
                self.SYJYInfo.score = v
            elseif k == "Unlock" then
                for _k, _v in pairs(v) do
                    self.SYJYInfo.unlock[_k] = _v
                end
            end
        end
    end
    self.SYJYInfo.defaultSelectId = rawData[2]

    CUIManager.ShowUI(CLuaUIResources.ShanYaoJieYuanWnd)
end

-- 查看故事
function LuaXinBaiLianDongMgr:ShowShanYaoJieYuanStoryWnd(msg)
    self.SYJYStoryMsg = msg
    CUIManager.ShowUI(CLuaUIResources.ShanYaoJieYuanStoryWnd)
end

-- 妖伴解锁结果
function LuaXinBaiLianDongMgr:YaoBanUnlock(yaobanId, time)
    if not self.SYJYInfo.unlock then
        self.SYJYInfo.unlock = {[yaobanId] = {Time = time}}
    else
        self.SYJYInfo.unlock[yaobanId] = {Time = time}
    end
    g_ScriptEvent:BroadcastInLua("ShanYaoJieYuanYaoBanUnlock", yaobanId)
end

-- 妖伴跟随结果
function LuaXinBaiLianDongMgr:YaoBanEnable(yaobanId)
    self.SYJYInfo.followId = yaobanId
    g_ScriptEvent:BroadcastInLua("ShanYaoJieYuanYaoBanEnableOrDisable")
end

-- 妖伴取消跟随结果
function LuaXinBaiLianDongMgr:YaoBanDisable()
    self.SYJYInfo.followId = nil
    g_ScriptEvent:BroadcastInLua("ShanYaoJieYuanYaoBanEnableOrDisable")
end

-- 更新积分
function LuaXinBaiLianDongMgr:YaoBanUpdateScore(score)
    self.SYJYInfo.score = score
    g_ScriptEvent:BroadcastInLua("ShanYaoJieYuanYaoBanUpdateScore")
end

-- 打开分享界面
function LuaXinBaiLianDongMgr:OpenShanYaoJieYuanShareWnd(args)
    if not args or args.Length < 1 then
		return
	end

    self.SYJYInfo.shareBuddyId = tonumber(args[0])
    CUIManager.ShowUI(CLuaUIResources.ShanYaoJieYuanShareWnd)
end

--#endregion

--#region 观妖笺

LuaXinBaiLianDongMgr.GYJUnlock = {}
LuaXinBaiLianDongMgr.GYJIsReward = nil

function LuaXinBaiLianDongMgr:ShowGuanYaoJianWnd(data)
    self.GYJUnlock = {}

    local rawData = g_MessagePack.unpack(data)["args"]
    local unlockData = rawData[1]
    if unlockData then
        for k, v in pairs(unlockData) do
            if k == "Unlock" then
                for _k, _v in pairs(v) do
                    self.GYJUnlock[_k] = _v
                end
            elseif k == "IsReward" then
                self.GYJIsReward = v
            end
        end
    end

    CUIManager.ShowUI(CLuaUIResources.GuanYaoJianWnd)
end

function LuaXinBaiLianDongMgr:GuanYaoJianUnlock(id)
    self.GYJUnlock[id] = true

    g_ScriptEvent:BroadcastInLua("GuanYaoJianUnlock", id)
end

function LuaXinBaiLianDongMgr:GuanYaoJianRewardSuccess()
    self.GYJIsReward = true
    g_ScriptEvent:BroadcastInLua("GuanYaoJianRewardSuccess")
end

-- 是否所有的都被解锁了
function LuaXinBaiLianDongMgr:IsGYJAllUnlock()
    local ret = true
    XinBaiLianDong_GuanYaoJian.Foreach(function(id, data)
        if not self.GYJUnlock[id] then
            ret = false
        end
    end)
    return ret
end

--#endregion


--#region 白蛇变人

LuaXinBaiLianDongMgr.BSBRGameplayId = nil
LuaXinBaiLianDongMgr.BSBRInfo = {}

function LuaXinBaiLianDongMgr:ShowTopRightWnd(wnd)
    local found = false
    ZhuJueJuQing_BaiSheBianRenGameplay.Foreach(function(key, value)
        if key == LuaTopAndRightMenuWndMgr.m_GamePlayId then
            wnd.contentNode:SetActive(true)
            self.BSBRGameplayId = key
            CUIManager.ShowUI(CLuaUIResources.BaiSheBianRenTopRightWnd)
            found = true
        end
    end)

    if not found then
        CUIManager.CloseUI(CLuaUIResources.BaiSheBianRenTopRightWnd)
    end
end

function LuaXinBaiLianDongMgr:BaiSheBianRenSyncPlayState(playState, expiredTime, score, maxScore)
    self.BSBRInfo.score = score
    self.BSBRInfo.maxScore = maxScore

    g_ScriptEvent:BroadcastInLua("BaiSheBianRenSyncPlayState")
end

-- 模型缩放
function LuaXinBaiLianDongMgr:ChangeObjectScale(engineId, scale, isScaleAtOnce)
    local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if not obj then return end

    if isScaleAtOnce then
        obj.RO.Scale = scale * obj.m_InitScale
    else
        obj:ScaleTo(scale * obj.m_InitScale)
    end
end

--#endregion


--#region 蜃妖相关

function LuaXinBaiLianDongMgr:OnMainPlayerDestroyed()
    LuaShenYaoTaskView.m_TaskStageGamePlayId = nil
    LuaShenYaoTaskView.m_TaskSategTitle = nil
    LuaShenYaoTaskView.m_TaskContent = nil
end

function LuaXinBaiLianDongMgr:IsInShenYaoPlay()
    if not CClientMainPlayer.Inst then return false end
    return CClientMainPlayer.Inst.PlayProp.PlayId >= 51102640 and CClientMainPlayer.Inst.PlayProp.PlayId <= 51102649
end

LuaXinBaiLianDongMgr.m_ShenYaoRewardTimes = 0
LuaXinBaiLianDongMgr.m_ShenYaoMaxRewardTimes = 0
LuaXinBaiLianDongMgr.m_ShenYaoJoinTimes = 0
LuaXinBaiLianDongMgr.m_ShenYaoMaxJoinTimes = 0
function LuaXinBaiLianDongMgr:IsShenYaoFinished()
    return self.m_ShenYaoRewardTimes >= self.m_ShenYaoMaxRewardTimes or self.m_ShenYaoJoinTimes >= self.m_ShenYaoMaxJoinTimes
end

function LuaXinBaiLianDongMgr:ShenYao_OpenSelectNpcWnd(itemId)
    local item = CItemMgr.Inst:GetById(itemId)
    if not item then return end

    local level, npcStr = string.match(item.Item.ExtraData.Data.varbinary.StringData, "([^:]+):([^:]+)")
    if not level or not npcStr then return end
    level = tonumber(level)

    local randNpcTbl = {}
    for npcId in string.gmatch(npcStr, "(%d+)") do
        table.insert(randNpcTbl, tonumber(npcId))
    end
    
    LuaShenYaoSelectNpcWnd.s_Level = level
    LuaShenYaoSelectNpcWnd.s_NpcTbl = randNpcTbl
    LuaShenYaoSelectNpcWnd.s_ItemId = itemId
    if #randNpcTbl == 2 then
        CUIManager.ShowUI(CLuaUIResources.ShenYaoSelectOneOfTwoWnd)
    elseif #randNpcTbl == 3 then
        CUIManager.ShowUI(CLuaUIResources.ShenYaoSelectOneOfThreeWnd)
    else
        CUIManager.ShowUI(CLuaUIResources.ShenYaoSelectOneOfFourWnd)
    end
end

function LuaXinBaiLianDongMgr:ShenYao_OpenTeamShenYaoInfoWnd(tbl_U)
    print("ShenYao_OpenTeamShenYaoInfoWnd", tbl_U)
    local info = tbl_U.Length == 0 and {} or g_MessagePack.unpack(tbl_U)
    LuaShenYaoTeamInfoWnd.s_Info = info
    CUIManager.ShowUI(CLuaUIResources.ShenYaoTeamInfoWnd)
end

function LuaXinBaiLianDongMgr:ShenYao_QueryRejectLevelResult(level)
    print("ShenYao_QueryRejectLevelResult", level)
    LuaShenYaoSettingWnd.s_CurLv = level
    CUIManager.ShowUI(CLuaUIResources.ShenYaoSettingWnd)
end

function LuaXinBaiLianDongMgr:ShenYao_OpenPersonalNpcWnd(consumeItemId, tbl_U)
    print("ShenYao_OpenPersonalNpcWnd", consumeItemId, tbl_U)

    local info = tbl_U.Length == 0 and {} or g_MessagePack.unpack(tbl_U)

    LuaShenYaoExclusiveWnd.s_ItemId = consumeItemId
    LuaShenYaoExclusiveWnd.s_Info = info
	CUIManager.ShowUI(CLuaUIResources.ShenYaoExclusiveWnd)
end

LuaXinBaiLianDongMgr.ShenYao_CurTrackNpc = nil
function LuaXinBaiLianDongMgr:TrackShenYaoNpc(list) -- 相同level的npclist
    if not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.RO or not CScene.MainScene then
        return
    end
    if CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("TeamFollow")) == 1 then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("在组队跟随状态下不能这样做"))
        return
    end

    local minDisNpc = {npc=nil, dis=999999}
    local maxLeftCountNpc = nil

    for _, npcItem in ipairs(list) do
        if npcItem and npcItem.targetSceneTemplateId and npcItem.leftCount then
            if npcItem.leftCount > 0 and npcItem.targetSceneTemplateId == CScene.MainScene.SceneTemplateId then
                local pos = Utility.PixelPos2GridPos(CClientMainPlayer.Inst.Pos)
                local dis = Vector3.Distance(Vector3(pos.x, 0, pos.y), Vector3(npcItem.targetPosX, 0, npcItem.targetPosY))
                if not minDisNpc.npc or minDisNpc.dis > dis then
                    minDisNpc.npc = npcItem
                    minDisNpc.dis = dis
                end
            end
            if not maxLeftCountNpc or npcItem.leftCount and maxLeftCountNpc.leftCount < npcItem.leftCount then
                maxLeftCountNpc = npcItem
            end
        end
    end

    local npc = nil
    if minDisNpc.npc then
        npc = minDisNpc.npc
    elseif maxLeftCountNpc and maxLeftCountNpc.leftCount > 0 then
        local tmpList = {}
        for _, npcItem in ipairs(list) do
            if maxLeftCountNpc.leftCount == npcItem.leftCount then
                table.insert(tmpList, npcItem)
            end
        end
        npc = tmpList[math.random(#tmpList)]
    end
    
    if npc then
        CUIManager.CloseUI(CLuaUIResources.ShenYaoLeftTimeWnd)
        CTrackMgr.Inst:FindLocation(nil, npc.targetSceneTemplateId, npc.targetPosX, npc.targetPosY, DelegateFactory.Action(function() 
            self.ShenYao_CurTrackNpc = { npcId = npc.npcId, instId = npc.instId, level = npc.level } 
            Gac2Gas.ShenYao_QuerySceneCountTbl()
        end), nil, nil, 0)
    end
end

function LuaXinBaiLianDongMgr:ShenYao_QuerySceneCountTblResult(sceneCountTbl, unlockLv)
    print("ShenYao_QuerySceneCountTblResult", sceneCountTbl, unlockLv)
    if self.ShenYao_CurTrackNpc then
        local data = g_MessagePack.unpack(sceneCountTbl)
        local npcList = {}
        local noLeftover = false

        for key, val in pairs(data) do
            for k = 1, #val do
                local v = val[k]
                local npcItem = {
                    instId = v.NpcInfo.InstId,
                    sceneTemplateId = tonumber(key),
                    level = v.NpcInfo.Level,
                    npcId = v.NpcInfo.NpcId,
                    times = v.NpcInfo.Times,
                    targetSceneTemplateId = v.NpcInfo.Pos.mapId,
                    targetPosX = v.NpcInfo.Pos.x,
                    targetPosY = v.NpcInfo.Pos.y,
                    leftCount = v.LeftCount,
                    playingCount = v.PlayingCount
                }
                if npcItem.npcId == self.ShenYao_CurTrackNpc.npcId and npcItem.instId == self.ShenYao_CurTrackNpc.instId then 
                    noLeftover = (npcItem.leftCount == 0)
                end
                if npcItem.level == self.ShenYao_CurTrackNpc.level then
                    table.insert(npcList, npcItem)
                end
            end
        end

        if noLeftover then
            self:TrackShenYaoNpc(npcList)
        else
            Gac2Gas.ShenYao_QueryNpcEngineId(self.ShenYao_CurTrackNpc.instId)
        end

        self.ShenYao_CurTrackNpc = nil
    end
    g_ScriptEvent:BroadcastInLua("ShenYao_QuerySceneCountTblResult", sceneCountTbl, unlockLv)
end

function LuaXinBaiLianDongMgr:ShenYao_QueryTeamPassResult(Level, NpcId, playerInfos_U)
    print("ShenYao_QueryTeamPassResult", Level, NpcId, playerInfos_U)
    local info = playerInfos_U.Length == 0 and {} or g_MessagePack.unpack(playerInfos_U)
    if CUIManager.IsLoaded(CLuaUIResources.ShenYaoPassingRecordWnd) then
        g_ScriptEvent:BroadcastInLua("ShenYao_QueryTeamPassResult", Level, NpcId, info)
    else
        LuaShenYaoPassingRecordWnd.s_TeamInfo = info
        LuaShenYaoPassingRecordWnd.s_Level = Level
        LuaShenYaoPassingRecordWnd.s_NpcId = NpcId
        CUIManager.ShowUI(CLuaUIResources.ShenYaoPassingRecordWnd)
    end
end

LuaXinBaiLianDongMgr.IsShenYaoOpen = false
LuaXinBaiLianDongMgr.IsShenYaoStart = false
LuaXinBaiLianDongMgr.ShenYaoPlayStartTime = 0
function LuaXinBaiLianDongMgr:ShenYao_SyncPlayState(bIsPlayOpen, bIsPlayStart, playStartTime)
    print("ShenYao_SyncPlayState", bIsPlayOpen, bIsPlayStart, playStartTime)
    self.IsShenYaoOpen = bIsPlayOpen
    self.IsShenYaoStart = bIsPlayStart
    self.ShenYaoPlayStartTime = playStartTime
    g_ScriptEvent:BroadcastInLua("ShenYao_SyncPlayState", bIsPlayOpen, bIsPlayStart, playStartTime)
end

LuaXinBaiLianDongMgr.ExclusiveShenYao_TaskTick = nil
function LuaXinBaiLianDongMgr:CancelExclusiveShenYaoTaskTick() 
    if self.ExclusiveShenYao_TaskTick then
        invoke(self.ExclusiveShenYao_TaskTick)
        self.ExclusiveShenYao_TaskTick = nil
    end
end

function LuaXinBaiLianDongMgr.ShowTime(totalSeconds)
	if totalSeconds < 0 then
        return LocalString.GetString("已过期")
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaXinBaiLianDongMgr:ExclusiveShenYaoTaskTickFunc(init)
    local remainTime = math.floor(CServerTimeMgr.ConvertTimeStampToZone8Time(self.ExclusiveShenYao_TaskEndTime):Subtract(CServerTimeMgr.Inst:GetZone8Time()).TotalSeconds)

    local timeOut = false
    if (not CClientMainPlayer.Inst or CommonDefs.DictContains(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), self.ExclusiveShenYao_TaskId)) and remainTime >= 0 then
        timeOut = false
    else
        timeOut = true
    end    

    local txt 
    if self.ExclusiveShenYao_TaskDescription and self.ExclusiveShenYao_NpcInfo.NpcId then
        local title = XinBaiLianDong_ShenYaoNpc.GetData(self.ExclusiveShenYao_NpcInfo.Level).Title..NPC_NPC.GetData(self.ExclusiveShenYao_NpcInfo.NpcId).Name
        local pos = PublicMap_PublicMap.GetData(self.ExclusiveShenYao_NpcInfo.SceneTid).Name.."("..self.ExclusiveShenYao_NpcInfo.X..","..self.ExclusiveShenYao_NpcInfo.Y..")"
        txt = CUICommonDef.TranslateToNGUIText(SafeStringFormat3(self.ExclusiveShenYao_TaskDescription, title, pos, self.ShowTime(remainTime)))
    end

    if not CommonDefs.IsNull(self.ExclusiveShenYao_TaskDetailLabel) then
        self.ExclusiveShenYao_TaskDetailLabel.text = txt or ""
    end

    if timeOut then
        if not CommonDefs.IsNull(self.ExclusiveShenYao_TaskInfoLabel) then
            self.ExclusiveShenYao_TaskInfoLabel.text = LocalString.GetString("该任务已过期，请放弃")
        end
        if not init then -- 只需要在tick里刷新任务栏和任务详情，初始化时不用
            self:CancelExclusiveShenYaoTaskTick()
            EventManager.BroadcastInternalForLua(EnumEventType.RefreshTaskStatus, {})
        end
    else
        if not CommonDefs.IsNull(self.ExclusiveShenYao_TaskInfoLabel) then
            self.ExclusiveShenYao_TaskInfoLabel.text = txt or ""
        end
    end

    return not timeOut
end

LuaXinBaiLianDongMgr.ExclusiveShenYao_TaskId = nil
LuaXinBaiLianDongMgr.ExclusiveShenYao_TaskInfoLabel = nil
LuaXinBaiLianDongMgr.ExclusiveShenYao_TaskDetailLabel = nil
LuaXinBaiLianDongMgr.ExclusiveShenYao_TaskDescription = nil
LuaXinBaiLianDongMgr.ExclusiveShenYao_TaskEndTime = 0
LuaXinBaiLianDongMgr.ExclusiveShenYao_NpcInfo = {}
function LuaXinBaiLianDongMgr:InitExclusiveShenYaoTask(task, taskInfoLabel, taskDescription)
    self:CancelExclusiveShenYaoTaskTick()
    self.ExclusiveShenYao_TaskId = task.TemplateId
    self.ExclusiveShenYao_TaskInfoLabel = taskInfoLabel
    self.ExclusiveShenYao_TaskDescription = taskDescription
    if task.ExtraData and task.ExtraData.Data then
        local data = task.ExtraData.Data
        local list = data.Length == 0 and {} or g_MessagePack.unpack(data)
        if #list >= 6 then
            self.ExclusiveShenYao_NpcInfo.Level = list[1]
            self.ExclusiveShenYao_NpcInfo.NpcId = list[2]
            self.ExclusiveShenYao_NpcInfo.SceneId = list[3]
            self.ExclusiveShenYao_NpcInfo.SceneTid = list[4]
            self.ExclusiveShenYao_NpcInfo.X = list[5]
            self.ExclusiveShenYao_NpcInfo.Y = list[6]
            self.ExclusiveShenYao_NpcInfo.CreateTime = list[7]
            self.ExclusiveShenYao_NpcInfo.EngineId = list[8]

            self.ExclusiveShenYao_TaskEndTime = task.AcceptTime + XinBaiLianDong_Setting.GetData().ShenYao_NpcLifeTime

            self:ExclusiveShenYaoTaskTickFunc(true)
            local remainTime = math.floor(CServerTimeMgr.ConvertTimeStampToZone8Time(self.ExclusiveShenYao_TaskEndTime):Subtract(CServerTimeMgr.Inst:GetZone8Time()).TotalSeconds)
            if remainTime >= 0 then
                self.ExclusiveShenYao_TaskTick = RegisterTick(function()
                    self:ExclusiveShenYaoTaskTickFunc()
                end, 1000)
            end
        end
    end
end

function LuaXinBaiLianDongMgr:ProcessExclusiveShenYaoTask()    
    if not CClientMainPlayer.Inst then 
        return
    end
    if CClientMainPlayer.Inst.IsCrossServerPlayer then
        g_MessageMgr:ShowMessage("SHENYAO_PATHFIND_FAIL")
        return
    end
    --Gac2Gas.ShenYao_QueryPersonalNpcInfo()
    local remainTime = math.floor(CServerTimeMgr.ConvertTimeStampToZone8Time(self.ExclusiveShenYao_TaskEndTime):Subtract(CServerTimeMgr.Inst:GetZone8Time()).TotalSeconds)
    if remainTime >= 0 then
        if self.ExclusiveShenYao_NpcInfo.NpcId then
            -- 可能存在同NpcId不同坐标的情况，FindNPC可能找到错误的Npc，要用EngineId判断
            -- CTrackMgr.Inst:FindNPC(self.ExclusiveShenYao_NpcInfo.NpcId, self.ExclusiveShenYao_NpcInfo.SceneTid, self.ExclusiveShenYao_NpcInfo.X, self.ExclusiveShenYao_NpcInfo.Y)
            CTrackMgr.Inst:FindLocation(self.ExclusiveShenYao_NpcInfo.SceneId, self.ExclusiveShenYao_NpcInfo.SceneTid, self.ExclusiveShenYao_NpcInfo.X, self.ExclusiveShenYao_NpcInfo.Y, DelegateFactory.Action(function() 
                local target = nil
                CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
                    if obj and obj.ObjectType == EnumObjectType.NPC and obj.EngineId == self.ExclusiveShenYao_NpcInfo.EngineId then
                        target = obj
                    end
                end))
                if target then
                    target:OnSelected()
                end
            end), nil, nil, 0)
        end
    else
        CTaskInfoMgr.ShowTaskWnd(self.ExclusiveShenYao_TaskId, 0)
    end
end

function LuaXinBaiLianDongMgr:ShenYao_QueryPersonalNpcInfoResult(info_U)
    print("ShenYao_QueryPersonalNpcInfoResult", info_U)
    local data = info_U.Length == 0 and {} or g_MessagePack.unpack(info_U)
    for k, v in pairs(data) do
        print(k, v)
    end
end

function LuaXinBaiLianDongMgr:ShenYao_QueryNpcEngineIdResult(sceneId, instId, engineId)
    print("ShenYao_QueryNpcEngineIdResult", sceneId, instId, engineId)
    local target = nil
    CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
        if  obj and obj.ObjectType == EnumObjectType.NPC and obj.EngineId == engineId then
            target = obj
        end
    end))
    if target then
        target:OnSelected()
    end
end

--#endregion


--#region 新白进度条

LuaXinBaiLianDongMgr.ProgressInfo = {}

-- 分开使用两个RPC是因为QueryResult的数据是放在Master上并且需要广播，而QueryRewardResult的数据是在Gas上
function LuaXinBaiLianDongMgr:XinBaiProgressQueryResult(bOpen, ProgressTbl_U, ShuiManJinShan_U, statusTbl_U)
    self.ProgressInfo.isOpen = bOpen
    self.ProgressInfo.progressTbl = g_MessagePack.unpack(ProgressTbl_U)
    self.ProgressInfo.smjsTbl = g_MessagePack.unpack(ShuiManJinShan_U)
    self.ProgressInfo.statusTbl = g_MessagePack.unpack(statusTbl_U)

    g_ScriptEvent:BroadcastInLua("XinBaiProgressQueryResult")
    EventManager.Broadcast(EnumEventType.SyncDynamicActivitySimpleInfo)
end

function LuaXinBaiLianDongMgr:XinBaiProgressQueryRewardResult(flagTbl_U)
    local tbl = g_MessagePack.unpack(flagTbl_U)
    self.ProgressInfo.flagTbl = tbl[1]
    self.ProgressInfo.statusTbl = tbl[2]
    g_ScriptEvent:BroadcastInLua("XinBaiProgressQueryRewardResult")
end

-- 新白联动按钮是否显示
function LuaXinBaiLianDongMgr:CanShowXinBaiButton()
    if not LuaXinBaiLianDongMgr.ProgressInfo.isOpen then return false end
    return COpenEntryMgr.Inst:IsMenuItemVisible(EnumMenuItem.Activity)
end

--#endregion

--#region 流程图相关

-- 技能高亮图标显示
function LuaXinBaiLianDongMgr:UpdateSkillHintFx(skillId, isHint)
    g_ScriptEvent:BroadcastInLua("XinBaiLianDongUpdateSkillHintFx", skillId, isHint)
end

--#endregion
