require("common/common_include")
local Gac2Gas=import "L10.Game.Gac2Gas"
local CQiXiMgr = import "L10.UI.CQiXiMgr"
local CUIResources = import "L10.UI.CUIResources"
local LocalString = import "LocalString"
local UILabel = import "UILabel"
local System = import "System"
local MessageMgr = import "L10.Game.MessageMgr"
local CMiddleNoticeMgr = import "L10.UI.CMiddleNoticeMgr"


CLuaQiXiDaTiWnd=class()
RegistClassMember(CLuaQiXiDaTiWnd,"m_QuestionLabel")
RegistClassMember(CLuaQiXiDaTiWnd,"m_CountDownProgressBar")
RegistClassMember(CLuaQiXiDaTiWnd,"m_AnswerAButton")
RegistClassMember(CLuaQiXiDaTiWnd,"m_AnswerBButton")
RegistClassMember(CLuaQiXiDaTiWnd,"m_AnswerCButton")
RegistClassMember(CLuaQiXiDaTiWnd,"m_AnswerDButton")
RegistClassMember(CLuaQiXiDaTiWnd,"m_RightTag")
RegistClassMember(CLuaQiXiDaTiWnd,"m_CloseButton")

RegistClassMember(CLuaQiXiDaTiWnd,"m_TotalDuration")
RegistClassMember(CLuaQiXiDaTiWnd,"aliveDuration")
RegistClassMember(CLuaQiXiDaTiWnd,"lastUpdateTime")
RegistClassMember(CLuaQiXiDaTiWnd,"updateInterval")
RegistClassMember(CLuaQiXiDaTiWnd,"m_SelfAnswer")
RegistClassMember(CLuaQiXiDaTiWnd,"m_OtherAnswer")
RegistClassMember(CLuaQiXiDaTiWnd,"m_OtherPlayerName")


function CLuaQiXiDaTiWnd:Init()
	-- 2021七夕鹊桥仙曲复用界面
	if LuaQiXi2021Mgr.IsQueQiaoXianQuPlay() then
		self:InitQueQiaoXianQuDaTi()
		return 
	end
	self.m_RightTag:SetActive(false)
	self.m_AnswerAButton.Selectable = true 
	self.m_AnswerBButton.Selectable = true
	self.m_AnswerCButton.Selectable = true
	self.m_AnswerDButton.Selectable = true
	self.m_AnswerAButton:SetSelected(false, false)
	self.m_AnswerBButton:SetSelected(false, false)
	self.m_AnswerCButton:SetSelected(false, false)
	self.m_AnswerDButton:SetSelected(false, false)
	self.m_TotalDuration = QiXi_LoveQingQiu.GetData().QuestionDuration
	self.aliveDuration = self.m_TotalDuration
	self.lastUpdateTime = 0
	self.updateInterval = 0.5
	self.m_SelfAnswer = -1
	self.m_OtherAnswer = -1
	self.m_OtherPlayerName = ""

	self.m_QuestionLabel.text = CQiXiMgr.s_Question
	if CQiXiMgr.s_Answer.Count >= 4 then
		self.m_AnswerAButton.Text = "A. " .. CQiXiMgr.s_Answer[0]
		self.m_AnswerBButton.Text = "B. " .. CQiXiMgr.s_Answer[1]
		self.m_AnswerCButton.Text = "C. " .. CQiXiMgr.s_Answer[2]
		self.m_AnswerDButton.Text = "D. " .. CQiXiMgr.s_Answer[3]
	end
	local onSelectAnswer = function(button)
		self.m_AnswerAButton.Selectable = false 
		self.m_AnswerBButton.Selectable = false
		self.m_AnswerCButton.Selectable = false
		self.m_AnswerDButton.Selectable = false
		--选过一次答案之后就不能再选了
		if self.m_SelfAnswer < 0 then
			self.m_SelfAnswer = 0
			if button == self.m_AnswerAButton then
				self.m_SelfAnswer = 0
			elseif button == self.m_AnswerBButton then
				self.m_SelfAnswer = 1
			elseif button == self.m_AnswerCButton then
				self.m_SelfAnswer = 2
			elseif button == self.m_AnswerDButton then
				self.m_SelfAnswer = 3
			end
			Gac2Gas.LoveQingQiuAnswerQuestion(CQiXiMgr.s_Answer[self.m_SelfAnswer])
			self:CheckAnswer()
		end
	end
	
	self.m_AnswerAButton.OnClick = DelegateFactory.Action_QnButton(onSelectAnswer)
	self.m_AnswerBButton.OnClick = DelegateFactory.Action_QnButton(onSelectAnswer)
	self.m_AnswerCButton.OnClick = DelegateFactory.Action_QnButton(onSelectAnswer)
	self.m_AnswerDButton.OnClick = DelegateFactory.Action_QnButton(onSelectAnswer)
	
	local closeFunc = function(go)
		if self.aliveDuration > 0 and (self.m_SelfAnswer <0 or self.m_OtherAnswer <0) then
			CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("对方还没有作答呢，请再等等！"))
		else
			CUIManager.CloseUI("QiXiDaTiWnd")
		end
	end
	CommonDefs.AddOnClickListener(self.m_CloseButton,DelegateFactory.Action_GameObject(closeFunc),false)
	self:ShowCountDownValue()
end

function CLuaQiXiDaTiWnd:OnEnable()
	g_ScriptEvent:AddListener("LoveQingQiuAnswerQuestion", self, "OnLoveQingQiuAnswerQuestion")
	g_ScriptEvent:AddListener("QueQiaoXianQuQuestionResult",self,"OnQueQiaoXianQuQuestionResult")
end
function CLuaQiXiDaTiWnd:OnDisable()
	g_ScriptEvent:RemoveListener("LoveQingQiuAnswerQuestion", self, "OnLoveQingQiuAnswerQuestion")
	g_ScriptEvent:RemoveListener("QueQiaoXianQuQuestionResult",self,"OnQueQiaoXianQuQuestionResult")
end

function CLuaQiXiDaTiWnd:Update()
	if Time.realtimeSinceStartup - self.lastUpdateTime >= self.updateInterval then
        self.lastUpdateTime = Time.realtimeSinceStartup
        self:ShowCountDownValue()
	end
	if (self.aliveDuration > 0) then
		self.aliveDuration = self.aliveDuration - Time.deltaTime
	end
	if LuaQiXi2021Mgr.IsQueQiaoXianQuPlay() then
		if (self.aliveDuration <= 0) then
			CUIManager.CloseUI("QiXiDaTiWnd")
		else
			if not self.m_CloseButton then 
				self.m_CloseButton = self.transform:Find("Wnd_Bg_Secondary_2/CloseButton").gameObject 
			end
			self.m_CloseButton.gameObject:SetActive(false)
		end
	end
end

function CLuaQiXiDaTiWnd:ShowCountDownValue()
	local color = "EEBA26";
	if (self.aliveDuration < 5) then
		color = "FF0000"
	end
	self.m_CountDownProgressBar:SetProgress(self.aliveDuration / self.m_TotalDuration)
	local str = SafeStringFormat3(LocalString.GetString("[%s]还剩%d秒[-]"), color, self.aliveDuration)
	self.m_CountDownProgressBar:SetText(str)
end

function CLuaQiXiDaTiWnd:OnLoveQingQiuAnswerQuestion(args)
	local playerName = tostring(args[0])
	local answerContent = tostring(args[1])
	self.m_OtherPlayerName = playerName
	for i=0,3 do
		if CQiXiMgr.s_Answer[i] == answerContent then
			self.m_OtherAnswer = i
			break
		end
	end
	self:CheckAnswer()
end

function CLuaQiXiDaTiWnd:CheckAnswer()
	if self.m_OtherAnswer >=0 and self.m_SelfAnswer >= 0 then	
		if self.m_OtherAnswer == self.m_SelfAnswer then
			local button1 = self:GetButtonByIndex(self.m_SelfAnswer)
			self.m_RightTag.transform.position = button1.transform.position
			self.m_RightTag:SetActive(true)
			MessageMgr.Inst:ShowMessage("QYQQ_QUESTION_RIGHT", {self.m_OtherPlayerName, CQiXiMgr.s_Answer[self.m_OtherAnswer]})
		else
			local button1 = self:GetButtonByIndex(self.m_SelfAnswer)
			local button2 = self:GetButtonByIndex(self.m_OtherAnswer)
			self.m_RightTag.transform.position = button2.transform.position
			self.m_RightTag:SetActive(true)
			MessageMgr.Inst:ShowMessage("QYQQ_QUESTION_WRONG", {self.m_OtherPlayerName, CQiXiMgr.s_Answer[self.m_OtherAnswer]})
		end
	end
end

function CLuaQiXiDaTiWnd:GetButtonByIndex(index)
	local button1 = self.m_AnswerAButton
	if index == 0 then
		button1 = self.m_AnswerAButton
	elseif index == 1 then
		button1 = self.m_AnswerBButton
	elseif index == 2 then
		button1 = self.m_AnswerCButton
	elseif index == 3 then
		button1 = self.m_AnswerDButton
	end
	return button1
end

function CLuaQiXiDaTiWnd:OnQueQiaoXianQuQuestionResult(result, timeout)
	if self.aliveDuration <= 0 then return end
	self.aliveDuration = 0
end

function CLuaQiXiDaTiWnd:InitQueQiaoXianQuDaTi()
	self.transform:Find("Wnd_Bg_Secondary_2/TitleLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("鹊桥仙曲")

	self.m_RightTag:SetActive(false)
	self.m_AnswerAButton.Selectable = true 
	self.m_AnswerBButton.Selectable = true
	self.m_AnswerCButton.Selectable = true
	self.m_AnswerDButton.Selectable = true
	self.m_AnswerAButton:SetSelected(false, false)
	self.m_AnswerBButton:SetSelected(false, false)
	self.m_AnswerCButton:SetSelected(false, false)
	self.m_AnswerDButton:SetSelected(false, false)
	self.m_TotalDuration = QiXi_QiXi2021.GetData().QuestionDuration
	self.aliveDuration = LuaQiXi2021Mgr.daTiTimeLeft
	self.lastUpdateTime = 0
	self.updateInterval = 0.5
	self.m_SelfAnswer = -1
	self.m_OtherAnswer = -1
	self.m_OtherPlayerName = ""

	self.m_QuestionLabel.text = LuaQiXi2021Mgr.questionContent
	if #LuaQiXi2021Mgr.questionAnswers >= 4 then
		self.m_AnswerAButton.Text = "A. " .. LuaQiXi2021Mgr.questionAnswers[1]
		self.m_AnswerBButton.Text = "B. " .. LuaQiXi2021Mgr.questionAnswers[2]
		self.m_AnswerCButton.Text = "C. " .. LuaQiXi2021Mgr.questionAnswers[3]
		self.m_AnswerDButton.Text = "D. " .. LuaQiXi2021Mgr.questionAnswers[4]
	end

	if not System.String.IsNullOrEmpty(LuaQiXi2021Mgr.answerContent) then
		self.m_AnswerAButton.Selectable = false 
		self.m_AnswerBButton.Selectable = false
		self.m_AnswerCButton.Selectable = false
		self.m_AnswerDButton.Selectable = false
		if LuaQiXi2021Mgr.answerContent == LuaQiXi2021Mgr.questionAnswers[1] then
			self.m_SelfAnswer = 0
			self.m_AnswerAButton:SetSelected(true)
		elseif LuaQiXi2021Mgr.answerContent == LuaQiXi2021Mgr.questionAnswers[2] then
			self.m_SelfAnswer = 1
			self.m_AnswerBButton:SetSelected(true)
		elseif LuaQiXi2021Mgr.answerContent == LuaQiXi2021Mgr.questionAnswers[3] then
			self.m_SelfAnswer = 2
			self.m_AnswerCButton:SetSelected(true)
		elseif LuaQiXi2021Mgr.answerContent == LuaQiXi2021Mgr.questionAnswers[4] then
			self.m_SelfAnswer = 3
			self.m_AnswerDButton:SetSelected(true)
		end
	end
	local onSelectAnswer = function(button)
		self.m_AnswerAButton.Selectable = false 
		self.m_AnswerBButton.Selectable = false
		self.m_AnswerCButton.Selectable = false
		self.m_AnswerDButton.Selectable = false
		--选过一次答案之后就不能再选了
		if self.m_SelfAnswer < 0 then
			self.m_SelfAnswer = 0
			if button == self.m_AnswerAButton then
				self.m_SelfAnswer = 0
			elseif button == self.m_AnswerBButton then
				self.m_SelfAnswer = 1
			elseif button == self.m_AnswerCButton then
				self.m_SelfAnswer = 2
			elseif button == self.m_AnswerDButton then
				self.m_SelfAnswer = 3
			end
			LuaQiXi2021Mgr.SendQueQiaoXianQuAnswerQuestion(self.m_SelfAnswer + 1)
		end
	end
	
	self.m_AnswerAButton.OnClick = DelegateFactory.Action_QnButton(onSelectAnswer)
	self.m_AnswerBButton.OnClick = DelegateFactory.Action_QnButton(onSelectAnswer)
	self.m_AnswerCButton.OnClick = DelegateFactory.Action_QnButton(onSelectAnswer)
	self.m_AnswerDButton.OnClick = DelegateFactory.Action_QnButton(onSelectAnswer)
	
	self:ShowCountDownValue()
end

return CLuaQiXiDaTiWnd
