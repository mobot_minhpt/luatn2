local CUITexture = import "L10.UI.CUITexture"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

LuaNanDuFanHuaLordCiTiaoDetailWnd = class()
LuaNanDuFanHuaLordCiTiaoDetailWnd.citiaoIndex = nil
--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNanDuFanHuaLordCiTiaoDetailWnd, "DescribeLabel", "DescribeLabel", UILabel)
RegistChildComponent(LuaNanDuFanHuaLordCiTiaoDetailWnd, "Tex", "Tex", CUITexture)

--@endregion RegistChildComponent end

function LuaNanDuFanHuaLordCiTiaoDetailWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaNanDuFanHuaLordCiTiaoDetailWnd:Init()
    local cfg = NanDuFanHua_JuBaoPen.GetData(LuaNanDuFanHuaLordCiTiaoDetailWnd.citiaoIndex)
    self.Tex:LoadMaterial(cfg.Icon)
    self.DescribeLabel.text = cfg.Content
    self.transform:Find("Title"):GetComponent(typeof(UILabel)).text = cfg.Name

    if self.autoHideWndTicker then
        UnRegisterTick(self.autoHideWndTicker)
    end
    self.autoHideWndTicker = RegisterTickOnce(function()
        CUIManager.CloseUI(CLuaUIResources.NanDuFanHuaLordCiTiaoDetailWnd)
    end, 5000)
end

function LuaNanDuFanHuaLordCiTiaoDetailWnd:OnDestroy()
    if self.autoHideWndTicker then
        UnRegisterTick(self.autoHideWndTicker)
    end
end
--@region UIEvent

--@endregion UIEvent

