
require("common/common_include")

local MsgPackImpl = import "MsgPackImpl"
local CommonDefs = import "L10.Game.CommonDefs"
local CIMMgr = import "L10.Game.CIMMgr"
local CGroupIMMgr = import "L10.Game.CGroupIMMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CVoiceMgr = import "L10.Game.CVoiceMgr"
local Utility = import "L10.Engine.Utility"
local EnumClass = import "L10.Game.EnumClass"
local EnumGender = import "L10.Game.EnumGender"
local CPlayerDataMgr = import "L10.Game.CPlayerDataMgr"
CLuaIMMgr = class()

function CLuaIMMgr.SendIMMsgForGM(channel, targetId, msg)
	-- 跟GM聊天，所有过滤都跳过
	local shouldBeIgnore = false
	local isVoiceMsg = CVoiceMgr.Inst:IsVoice(msg)
	local msgArray = Utility.BreakString(msg, 4, 255, nil)
	Gac2Gas.SendLongIMMsgForGM(targetId, shouldBeIgnore, isVoiceMsg, msgArray[0], msgArray[1], msgArray[2], msgArray[3])
end

function CLuaIMMgr.GetNPCFriendAnswer(NPCID)
	local NPC = NpcChat_ChatNpc.GetData(NPCID)
	local AnswerList = NPC.DefaultAnswerID
	local RandomNum = UnityEngine_Random(0,AnswerList.Length)
	local AnswerInfo = NpcChat_DefaultAnswer.GetData(AnswerList[RandomNum])
	if AnswerInfo then
		return AnswerInfo.Answers
	end
	return ""
end

function CLuaIMMgr.SetID2InteractMsgInfo(ID,status)
	if not LuaNPCChatMgr:NeedAlert(ID) then
		status = false
	end

	if CIMMgr.Inst.m_ID2NeedInteractMsg:ContainsKey(ID) then
		--if CIMMgr.Inst.m_ID2NeedInteractMsg[ID] == status then return end
		CIMMgr.Inst.m_ID2NeedInteractMsg[ID] = status
	else
		CIMMgr.Inst.m_ID2NeedInteractMsg:Add(ID,status)
	end
		
	CIMMgr.Inst:UpdateIMAlert()

	EventManager.BroadcastInternalForLua(EnumEventType.OnIMMsgReadStatusChanged, {ID})

end

function CLuaIMMgr:QueryOnlineReverseFriends(context_int)
    Gac2Gas.QueryOnlineReverseFriends(context_int)    
end
--------------
-- 聊天数据备份与恢复功能,仅限国服使用
--------------
EnumIMSyncImRecordStatus = {
    eNotStarted = 0,
    eStarted = 1,
    eSuccess = 2,
    eFailed = 3,
}

CLuaIMMgr.m_UploadImRecordContext = "_UploadImRecord"
CLuaIMMgr.m_DownloadImRecordContext = "_DownloadImRecord"

CLuaIMMgr.m_DownloadImRecordStatus = EnumIMSyncImRecordStatus.eNotStarted
CLuaIMMgr.m_UploadImRecordStatus = EnumIMSyncImRecordStatus.eNotStarted
CLuaIMMgr.m_DownloadImRecordProgress = 0
CLuaIMMgr.m_UploadImRecordProgress = 0

function CLuaIMMgr:IsEnableSyncImRecord()
    return false
end

function CLuaIMMgr:TryUploadImRecord()
    if not self:IsEnableSyncImRecord() then return end
    if not LuaWelfareMgr:HasBigMonthCard() then
        g_MessageMgr:ShowMessage("Sync_IM_Record_Need_Big_Month_Card")
        return
    end
    if self.m_UploadImRecordStatus == EnumIMSyncImRecordStatus.eStarted then
        g_MessageMgr:ShowMessage("Sync_IM_Record_Upload_In_Progress")
        return
    end
    --先下载，再本地合并，再上传
    self:RequestDownloadImRecord(self.m_UploadImRecordContext)
end

function CLuaIMMgr:TryDownloadImRecord()
    if not self:IsEnableSyncImRecord() then return end
    if not LuaWelfareMgr:HasBigMonthCard() then
        g_MessageMgr:ShowMessage("Sync_IM_Record_Need_Big_Month_Card")
        return
    end
    if self.m_DownloadImRecordStatus == EnumIMSyncImRecordStatus.eStarted then
        g_MessageMgr:ShowMessage("Sync_IM_Record_Upload_In_Progress")
        return
    end
    self:RequestDownloadImRecord(self.m_DownloadImRecordContext)
end

function CLuaIMMgr:RequestUploadImRecord()
    Gac2Gas.RequestUploadImRecord(self.m_UploadImRecordContext)
end

function CLuaIMMgr:RequestDownloadImRecord(context)
    Gac2Gas.RequestDownloadImRecord(context)
end

function CLuaIMMgr:UploadImRecord(url, token, context)
    print("UploadImRecord", url, token, context)
    if context~=self.m_UploadImRecordContext then return end
    self.m_UploadImRecordStatus = EnumIMSyncImRecordStatus.eStarted
    self.m_UploadImRecordProgress = 0
    CPlayerDataMgr.Inst:DoUploadCompressedMsgFile(url, token, DelegateFactory.Action_float(function(progress)
        self.m_UploadImRecordProgress = progress
        print("upload progress", progress)
    end), DelegateFactory.Action_bool_string(function(bSuccess, fileId)
        print("upload result", bSuccess, fileId)
        self.m_UploadImRecordProgress = 1
        if bSuccess then
            Gac2Gas.UploadImRecordSuccess(fileId, context)
            self.m_UploadImRecordStatus = EnumIMSyncImRecordStatus.eSuccess
        else
            self.m_UploadImRecordStatus = EnumIMSyncImRecordStatus.eFailed
        end
    end))
end

function CLuaIMMgr:DownloadImRecord(url, token, context)
    print("DownloadImRecord", url, token, context)
    if context==self.m_UploadImRecordContext then
        self.m_UploadImRecordStatus = EnumIMSyncImRecordStatus.eStarted
        CPlayerDataMgr.Inst:PrepareCompressedMsgFileToUpload(url, token)
    elseif context == self.m_DownloadImRecordContext then
        self.m_DownloadImRecordStatus = EnumIMSyncImRecordStatus.eStarted
        CPlayerDataMgr.Inst:DownloadAndReplaceLocalMsgs(url, DelegateFactory.Action_float(function(progress)
            self.m_DownloadImRecordProgress = progress
            print("download progress", progress)
        end), DelegateFactory.Action_bool(function(bSuccess)
            print("download result", bSuccess)
            self.m_DownloadImRecordProgress = 1
            if bSuccess then
                self.m_DownloadImRecordStatus = EnumIMSyncImRecordStatus.eSuccess
            else
                self.m_DownloadImRecordStatus = EnumIMSyncImRecordStatus.eFailed
            end
        end))
    end
end

function CLuaIMMgr:SetUploadImRecordResult(bSuccess)
    self.m_UploadImRecordStatus = bSuccess and EnumIMSyncImRecordStatus.eSuccess or EnumIMSyncImRecordStatus.eFailed
end

function CLuaIMMgr:SetDownloadImRecordResult(bSuccess)
    self.m_DownloadImRecordStatus = bSuccess and EnumIMSyncImRecordStatus.eSuccess or EnumIMSyncImRecordStatus.eFailed
end

function Gas2Gac.QueryCloseFriendInfoResult(retOtherServerPlayerInfoData, playerId2LastOfflineTimeData)
	local retOtherServerPlayerInfo = retOtherServerPlayerInfoData and MsgPackImpl.unpack(retOtherServerPlayerInfoData)
	local playerId2LastOfflineTime = playerId2LastOfflineTimeData and MsgPackImpl.unpack(playerId2LastOfflineTimeData)

	if not retOtherServerPlayerInfo or not playerId2LastOfflineTime then return end

	local playerInfo = {}

	if CommonDefs.IsDic(playerId2LastOfflineTime) then

		CommonDefs.DictIterate(playerId2LastOfflineTime, DelegateFactory.Action_object_object(function(key, offlineTime)

			local basicInfo = CIMMgr.Inst:GetBasicInfo(key)

			local isOnline = true
			if offlineTime == 0 then
				isOnline = true
			else
				isOnline = false
			end

			--离线小时数计算
			local offlineHours = 0
			if offlineTime > 0 then
				local endTime = CServerTimeMgr.ConvertTimeStampToZone8Time(offlineTime)
				local now = CServerTimeMgr.Inst:GetZone8Time()
				local span = now:Subtract(endTime)
				if span.TotalHours > 0 then
					offlineHours = math.ceil(span.TotalHours)
				end
			end

			--本服玩家用主角服务器名
			local server = ""
			local mainplayer = CClientMainPlayer.Inst
			if mainplayer then server = mainplayer:GetMyServerName() end
			playerInfo[key] = { 
				playerId = key, 
				name = basicInfo.Name, 
				displayName = basicInfo.Name, 
				gender = basicInfo.Gender, 
				class = basicInfo.Class, 
				level = basicInfo.Level, 
				xianfanStatus = basicInfo.XianFanStatus, 
				isOnline = isOnline, 
				hours = offlineHours, 
				serverName = server,
				isSameServer = true
			}

		end))

	end

	if CommonDefs.IsDic(retOtherServerPlayerInfo) then
		
		--过滤同一同行证玩家
		local tbl = {}

		CommonDefs.DictIterate(retOtherServerPlayerInfo, DelegateFactory.Action_object_object(function(key, val)
			-- val : [playerId] = {"ServerId":"17","ServerName":"test","PlayerId":12512000017,"Account":"","Uid":"aebfqur45ij5zpwi","GuestUid":"","Name":"'èª“è¨€ä½³äºº'","Gender":1,"Profession":10,"Grade":30,"XianFanStatus":0,"IsLatest":true}

			local sourcePlayerInfo
			if playerInfo[key] then
				sourcePlayerInfo = playerInfo[key]
			end

			local sourcePlayerName = ""
			if sourcePlayerInfo and playerInfo[key].name then
				sourcePlayerName = " ("..playerInfo[key].name..")"
			end

			local sourcePlayerIsOnline = false
			if sourcePlayerInfo and playerInfo[key].isOnline then
				sourcePlayerIsOnline = true
			end

			local valPlayerId = 0
			local valName = nil
			local valGender = 0
			local valProfession = 0
			local valGrade = 0
			local valXianFanStatus = 0
			local valServerName = ""

			if CommonDefs.DictContains_LuaCall(val, "PlayerId") then valPlayerId = CommonDefs.DictGetValue_LuaCall(val, "PlayerId") end
			if CommonDefs.DictContains_LuaCall(val, "Name") then valName = CommonDefs.DictGetValue_LuaCall(val, "Name") end
			if CommonDefs.DictContains_LuaCall(val, "Gender") then valGender = CommonDefs.DictGetValue_LuaCall(val, "Gender") end
			if CommonDefs.DictContains_LuaCall(val, "Profession") then valProfession = CommonDefs.DictGetValue_LuaCall(val, "Profession") end
			if CommonDefs.DictContains_LuaCall(val, "Grade") then valGrade = CommonDefs.DictGetValue_LuaCall(val, "Grade") end
			if CommonDefs.DictContains_LuaCall(val, "XianFanStatus") then valXianFanStatus = CommonDefs.DictGetValue_LuaCall(val, "XianFanStatus") end
			if CommonDefs.DictContains_LuaCall(val, "ServerName") then valServerName = CommonDefs.DictGetValue_LuaCall(val, "ServerName") end

			--过滤同一同行证玩家
			local bSkip = false
			if tbl[valPlayerId] then
				local _key = tbl[valPlayerId]
				local existInfo = playerInfo[_key]
				if existInfo and existInfo.sourcePlayerXianfanStatus and existInfo.sourcePlayerLevel and 
					sourcePlayerInfo and sourcePlayerInfo.xianfanStatus and sourcePlayerInfo.level then

					if existInfo.sourcePlayerXianfanStatus > sourcePlayerInfo.xianfanStatus or 
						existInfo.sourcePlayerXianfanStatus == sourcePlayerInfo.xianfanStatus and existInfo.sourcePlayerLevel > sourcePlayerInfo.level then
						bSkip = true
					end

				end
			end

			if not bSkip then
				tbl[valPlayerId] = key

				playerInfo[key] = {
					playerId = valPlayerId,
					sourcePlayerId = key,
					name = valName,
					displayName = string.gsub(valName, "^%'(.+)%'$", "%1")..sourcePlayerName, 
					gender = valGender, 
					class = valProfession, 
					level = valGrade, 
					xianfanStatus = valXianFanStatus, 
					isOnline = true, 
					hours = 0, 
					serverName = valServerName,
					isSameServer = false,

					sourcePlayerXianfanStatus = sourcePlayerInfo and sourcePlayerInfo.xianfanStatus or nil,
					sourcePlayerLevel = sourcePlayerInfo and sourcePlayerInfo.level or nil,
					sourcePlayerIsOnline = sourcePlayerIsOnline,
				}
			end

		end))

	end

	local dataTbl = {}
	for k, v in pairs(playerInfo) do
		table.insert(dataTbl, v)
	end

	table.sort(dataTbl, function(a, b)
		--优先考虑在线
		if a.isOnline == b.isOnline then
			--其次考虑本服
			if a.isSameServer == b.isSameServer then
				--再次考虑离线时间短的
				if a.hours == b.hours then
					return a.playerId < b.playerId --最后id小的排前面
				else
					return a.hours < b.hours
				end
			elseif a.isSameServer then
				return true
			else
				return false
			end
		else
			return a.isOnline --在线的排前面(isOnline
		end
	end)

	-- playerId, name, gender, class, level, xianfanStatus, isOnline, hours, serverName

	-- table.insert(dataTbl, {playerId = 1001, name = "ZhangSan", gender = 1, class = 2, level = 109, xianfanStatus = 0, isOnline = 1, hours = 10, serverName = "Server1"})
	-- table.insert(dataTbl, {playerId = 1002, name = "LiSi", gender = 0, class = 5, level = 129, xianfanStatus = 0, isOnline = 0, hours = 8, serverName = "Server2"})
	g_ScriptEvent:BroadcastInLua("OnReceiveCloseFriendInfoResult", dataTbl)
end

CIMMgr.m_hookTalkWithNPC = function(this,recverId, senderId, engineId, senderName, senderClass, senderGender, message)
	if message == nil then	return end
	this.Inst:RecvMsg(recverId, senderId, engineId, senderName, senderClass, senderGender, message, CServerTimeMgr.Inst.timeStamp)
	local NPC = NPC_NPC.GetData(recverId)
	if not NPC then return end
	local FreeChatProbability = NpcChat_Setting.GetData().FreeChatProbability -- 读表
	local RandomNum = UnityEngine_Random(0,100)
	if RandomNum > FreeChatProbability * 100 then return end
	this.Inst:RecvMsg(senderId, recverId, 0, NPC.Name, EnumToInt(EnumClass.Undefined), EnumToInt(EnumGender.Undefined), CLuaIMMgr.GetNPCFriendAnswer(recverId), CServerTimeMgr.Inst.timeStamp)
end