-- require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local CWeddingMgr=import "L10.Game.CWeddingMgr"
local CItemMgr=import "L10.Game.CItemMgr"
local Wedding_Setting=import "L10.Game.Wedding_Setting"
local Item_Item=import "L10.Game.Item_Item"
local CItemChooseMgr=import "L10.UI.CItemChooseMgr"
local EquipmentTemplate_Equip=import "L10.Game.EquipmentTemplate_Equip"
local MessageMgr=import "L10.Game.MessageMgr"
local Convert=import "System.Convert"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
-- local Gac2Gas=import "L10.Game.Gac2Gas"
local CWordFilterMgr=import "L10.Game.CWordFilterMgr"

CLuaJieZhiKeZiWnd=class()
RegistClassMember(CLuaJieZhiKeZiWnd,"m_CloseButton")
RegistClassMember(CLuaJieZhiKeZiWnd,"m_TemplateId")
RegistClassMember(CLuaJieZhiKeZiWnd,"m_KeDaoCountUpdate")
RegistClassMember(CLuaJieZhiKeZiWnd,"m_RingTemplateId")
RegistClassMember(CLuaJieZhiKeZiWnd,"m_Input")

function CLuaJieZhiKeZiWnd:Init()
    self.m_TemplateId=21020005
    self.m_RingTemplateId=Wedding_Setting.GetData().MarryRingItemId
    self.m_Input=LuaGameObject.GetChildNoGC(self.transform,"Input").input
    local g = LuaGameObject.GetChildNoGC(self.transform,"CloseButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CUIResources.JieZhiKeZiWnd)
    end)
    local g = LuaGameObject.GetChildNoGC(self.transform,"CancleButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CUIResources.JieZhiKeZiWnd)
    end)
    local g = LuaGameObject.GetChildNoGC(self.transform,"OkButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        --是否有戒指
        --是否输入了内容
        self:DoRequestKeZi()
    end)
    local g = LuaGameObject.GetChildNoGC(self.transform,"RingItem").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemChooseMgr.ShowWndWithMarriageRing()
    end)

    local kedaoIcon=LuaGameObject.GetChildNoGC(self.transform,"EquipIcon").cTexture
    local item = Item_Item.GetData(self.m_TemplateId)
    if item then
        kedaoIcon:LoadMaterial(item.Icon)
    end
    self.m_KeDaoCountUpdate=LuaGameObject.GetChildNoGC(self.transform,"EquipItem").itemCountUpdate
    self.m_KeDaoCountUpdate.templateId=self.m_TemplateId
    self.m_KeDaoCountUpdate.format="{0}/1"
    local function OnCountChange(val)
        if val==0 then
            self.m_KeDaoCountUpdate.format="[ff0000]{0}[-]/1"
        else
            self.m_KeDaoCountUpdate.format="{0}/1"
        end
    end
    self.m_KeDaoCountUpdate.onChange=DelegateFactory.Action_int(OnCountChange)
    self.m_KeDaoCountUpdate:UpdateCount()

    local ringIcon=LuaGameObject.GetChildNoGC(self.transform,"RingIcon").cTexture
    local itemId=CWeddingMgr.Inst.RingItemIdForKeZi
    local ringItem = EquipmentTemplate_Equip.GetData(self.m_RingTemplateId)
    if itemId then
        ringIcon:LoadMaterial(ringItem.Icon)
    else
        ringIcon:Clear()
    end
end
function CLuaJieZhiKeZiWnd:OnEnable()
    g_ScriptEvent:AddListener("SculptureWeddingRingSuccess", self, "OnSculptureWeddingRingSuccess")
end

function CLuaJieZhiKeZiWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SculptureWeddingRingSuccess", self, "OnSculptureWeddingRingSuccess")

    CWeddingMgr.Inst:ResetKeZiItemId()
end
function CLuaJieZhiKeZiWnd:DoRequestKeZi()
    local itemId=CWeddingMgr.Inst.RingItemIdForKeZi
    if not itemId then
        MessageMgr.Inst:ShowMessage("Ring_Sculpture_No_Ring",{})
        return
    end
    local content=self.m_Input.value
    if not content or content=="" then
        MessageMgr.Inst:ShowMessage("Ring_Sculpture_No_Content",{})
        return
    end
    local len=CUICommonDef.GetStrByteLength(content)
    if len>30 then
        MessageMgr.Inst:ShowMessage("Ring_Sculpture_Content_ToMuch",{})
        return
    end
    if not CWordFilterMgr.Inst:CheckLinkAndMatch(content) then
        MessageMgr.Inst:ShowMessage("Ring_Sculpture_Content_Illegal",{})
        return
    end

    local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(content,true)
    content = ret.msg
    local itemInfo=CItemMgr.Inst:GetItemInfo(itemId)
    local place,pos=Convert.ToUInt32(itemInfo.place),itemInfo.pos
    if content then
        local kedaoItemId=CWeddingMgr.Inst.KeDaoItemIdForKeZi
        local kedaopos=CWeddingMgr.Inst.KeDaoPosForKeZi--CItemMgr.Inst:FindItemPosition(EnumItemPlace.Body,kedaoItemId)
        local kedaoplace=CWeddingMgr.Inst.KeDaoPlaceForKeZi--Convert.ToUInt32(EnumItemPlace.Body)
        Gac2Gas.RequestSculptureWeddingRing(itemId,place,pos,content,kedaoItemId,kedaoplace,kedaopos)
    end
end

function CLuaJieZhiKeZiWnd:OnSculptureWeddingRingSuccess()
    --显示预览界面
    local itemId=CWeddingMgr.Inst.RingItemIdForKeZi
    if itemId then
        CItemInfoMgr.ShowLinkItemInfo(itemId, false, nil, AlignType.Default, 0,0,0,0)
    end
    MessageMgr.Inst:ShowMessage("Ring_Sculpture_Success",{})
    CUIManager.CloseUI(CUIResources.JieZhiKeZiWnd)
end

return CLuaJieZhiKeZiWnd
