-- Auto Generated!!
local CHongLvTemple = import "L10.UI.CHongLvTemple"
local Temple_Temple = import "L10.Game.Temple_Temple"
CHongLvTemple.m_Init_CS2LuaHook = function (this, templeId) 
    local data = Temple_Temple.GetData(templeId)
    if data ~= nil then
        this.m_TempleName.text = data.Name
        this.m_TempleDesp.text = data.Description
    end
end
