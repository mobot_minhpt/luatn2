local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnTableView = import "L10.UI.QnTableView"
local UISprite = import "UISprite"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local AlignType2=import "L10.UI.CPopupMenuInfoMgr+AlignType"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CRankData=import "L10.UI.CRankData"

LuaRuYiDongRankView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaRuYiDongRankView, "EmptyTipLabel", "EmptyTipLabel", UILabel)
RegistChildComponent(LuaRuYiDongRankView, "MyRankItem", "MyRankItem", GameObject)
RegistChildComponent(LuaRuYiDongRankView, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaRuYiDongRankView, "Arrow", "Arrow", UISprite)
RegistChildComponent(LuaRuYiDongRankView, "DifficultyLabel", "DifficultyLabel", UILabel)
RegistChildComponent(LuaRuYiDongRankView, "Difficulty", "Difficulty", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaRuYiDongRankView, "m_SelectDifficulty")
RegistClassMember(LuaRuYiDongRankView, "m_RankData")
function LuaRuYiDongRankView:Awake()
    self.m_SelectDifficulty = 0
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Difficulty.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDifficultyClick(go)
	end)


    --@endregion EventBind end
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RankData
        end,
        function(item,row)
            self:InitItem(item,row)
        end)
    self.m_RankData = {}
    self:RefreshDataView()
    --
    local setting = HuluBrothers_Setting.GetData()
	if LuaHuLuWa2022Mgr.m_OpenRankFromGamePlayId == setting.NormalPlayId then
		self.m_SelectDifficulty  = 0
	elseif LuaHuLuWa2022Mgr.m_OpenRankFromGamePlayId == setting.HardPlayId then
		self.m_SelectDifficulty = 1
	elseif LuaHuLuWa2022Mgr.m_OpenRankFromGamePlayId == setting.HellPlayId then
		self.m_SelectDifficulty = 2
	end  
    self:RefreshDifficulty()
end

function LuaRuYiDongRankView:OnEnable( ... )
	g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaRuYiDongRankView:OnDisable( ... )
	g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaRuYiDongRankView:InitRankData(difficulty)
    if not difficulty then
        difficulty = self.m_SelectDifficulty
    end
    local rankId = 41000258+difficulty
    Gac2Gas.QueryRank(rankId)
end

function LuaRuYiDongRankView:InitItem(item,row)
    local bg1 = item.transform:Find("bg1")
    local bg2 = item.transform:Find("bg2")
    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankSprite = item.transform:Find("RankLabel/RankSprite"):GetComponent(typeof(UISprite))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))

    bg1.gameObject:SetActive(row%2==0)
    bg2.gameObject:SetActive(row%2~=0)
    local data = self.m_RankData[row + 1]
    if data then
        local rank = tonumber(data.rank)
        rankLabel.text = rank
        if rank <= 3 then
            rankSprite.gameObject:SetActive(true)
            rankSprite.spriteName = "Rank_No."..rank
        else
            rankSprite.gameObject:SetActive(false)
        end
        local time = data.time
        local min = math.floor(time/60)
        local sec = time - min*60
        timeLabel.text = SafeStringFormat3(LocalString.GetString("%d分%d秒"),min,sec)
        nameLabel.text = data.name
    end
end

function LuaRuYiDongRankView:InitMyRank()
    local item = self.MyRankItem
    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankSprite = item.transform:Find("RankLabel/RankSprite"):GetComponent(typeof(UISprite))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    rankSprite.gameObject:SetActive(false)
    local myRankInfo = CRankData.Inst.MainPlayerRankInfo
    if CClientMainPlayer.Inst then
        nameLabel.text = CClientMainPlayer.Inst.RealName
    else
        nameLabel.text = nil
    end
    
    if not myRankInfo or myRankInfo.Rank == 0  then
        rankLabel.text = LocalString.GetString("未上榜")
    else
        rankLabel.text = myRankInfo.Rank
    end
    if myRankInfo and myRankInfo.Value~=0 then
        local time = myRankInfo.Value
        local min = math.floor(time/60)
        local sec = time - min*60
        timeLabel.text = SafeStringFormat3(LocalString.GetString("%d分%d秒"),min,sec)
    else
        timeLabel.text = LocalString.GetString("无")
    end
end

function LuaRuYiDongRankView:RefreshDifficulty()
    self:InitRankData(self.m_SelectDifficulty)
    local names = {
        [1] = LocalString.GetString("普通"),
        [2] = LocalString.GetString("英雄"),
        [3] = LocalString.GetString("地狱"),
    }
    self.DifficultyLabel.text = names[self.m_SelectDifficulty+1]--SafeStringFormat3(LocalString.GetString("难度%d"),self.m_SelectDifficulty+1)
end

function LuaRuYiDongRankView:OnRankDataReady( ... )
	self.m_RankData = {}
    --print(CRankData.Inst.RankList.Count)
	if CRankData.Inst.RankList and CRankData.Inst.RankList.Count > 0 then
		do
			local i = 0
			while i < CRankData.Inst.RankList.Count do
				local info = CRankData.Inst.RankList[i]
        		local extraList = MsgPackImpl.unpack(info.extraData)
				local data = {name = info.Name,rank = info.Rank,time = info.Value}
                table.insert(self.m_RankData,data)
				i = i + 1
			end
			if i == 0 then
				self.EmptyTipLabel.gameObject:SetActive(true)
			else
				self.EmptyTipLabel.gameObject:SetActive(false)
			end
		end
    else
    	self.EmptyTipLabel.gameObject:SetActive(true)
	end

	self:RefreshDataView()
end

function LuaRuYiDongRankView:RefreshDataView()
    self.TableView:ReloadData(false,false)
    self:InitMyRank()
end
--@region UIEvent

function LuaRuYiDongRankView:OnDifficultyClick(go)
    local function SelectAction(index)        
        self.m_SelectDifficulty = index
        self:RefreshDifficulty()
    end

    local selectShareItem=DelegateFactory.Action_int(SelectAction)

    local t={}
    local item=PopupMenuItemData(LocalString.GetString("普通"),selectShareItem,false,nil)
    table.insert(t, item)
    local item=PopupMenuItemData(LocalString.GetString("英雄"),selectShareItem,false,nil)
    table.insert(t, item)
    local item=PopupMenuItemData(LocalString.GetString("地狱"),selectShareItem,false,nil)
    table.insert(t, item)

    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))

    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType2.Bottom, 1, DelegateFactory.Action(function () 
        self.Arrow.transform.localEulerAngles = Vector3(0,0,0)
    end), 600, true, 266)

    self.Arrow.transform.localEulerAngles = Vector3(0,0,180)
end


--@endregion UIEvent

