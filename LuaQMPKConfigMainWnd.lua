local QnTabView = import "L10.UI.QnTabView"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
CLuaQMPKConfigMainWnd = class()

RegistClassMember(CLuaQMPKConfigMainWnd, "m_TitleLab")
RegistClassMember(CLuaQMPKConfigMainWnd, "m_NameLab")
RegistClassMember(CLuaQMPKConfigMainWnd, "m_TableView")
RegistClassMember(CLuaQMPKConfigMainWnd, "m_CloseBtn")
RegistClassMember(CLuaQMPKConfigMainWnd, "m_CurTab")
function CLuaQMPKConfigMainWnd:Awake()
    self.m_TitleLab = self.transform:Find("Wnd_Bg_Primary_Tab/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_NameLab = self.transform:Find("Wnd_Bg_Primary_Tab/NameLab"):GetComponent(typeof(UILabel))
    self.m_TableView = self.transform:Find("Anchor/QnTabView"):GetComponent(typeof(QnTabView))
    self.m_CloseBtn = self.transform:Find("Wnd_Bg_Primary_Tab/CloseButton").gameObject
    self.m_CurTab = 0
    self.m_TableView.OnSelect = DelegateFactory.Action_QnTabButton_int(function (button, index)
        local page = self.m_TableView.transform:GetChild(self.m_CurTab):GetComponent(typeof(CCommonLuaScript))
        if page and page.m_LuaSelf then
            page.m_LuaSelf:OnPageCloseOrChange(nil)
        end
        self.m_CurTab = index
    end)
    UIEventListener.Get(self.m_CloseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCloseBtnClick(go)
    end)
end

function CLuaQMPKConfigMainWnd:Init()
    if CLuaQMPKMgr.s_IsOtherPlayer then
        self.m_TitleLab.text = LocalString.GetString("他人配置")
        self.m_NameLab.text = CLuaQMPKMgr.s_PlayerName
    else
        self.m_NameLab.text = ""
    end
end

function CLuaQMPKConfigMainWnd:OnDestroy()
    CLuaQMPKMgr.s_IsOtherPlayer = false
    CLuaQMPKMgr.s_PlayerId = nil
    CLuaQMPKMgr.s_PlayerName = nil
    CLuaQMPKMgr.s_PlayerCls = nil
    CLuaQMPKMgr.s_PlayerInfo = nil
end

function CLuaQMPKConfigMainWnd:OnCloseBtnClick()
    local curIndex = self.m_TableView.CurrentSelectTab
    local page = self.m_TableView.transform:GetChild(curIndex):GetComponent(typeof(CCommonLuaScript))
    if page and page.m_LuaSelf then
        local finalFunc = function() CUIManager.CloseUI(CLuaUIResources.QMPKConfigMainWnd) end
        page.m_LuaSelf:OnPageCloseOrChange(finalFunc)
    end
end


return CLuaQMPKConfigMainWnd