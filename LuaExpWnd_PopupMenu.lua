local UITweener=import "UITweener"

local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local MessageMgr = import "L10.Game.MessageMgr"

CLuaExpWnd_PopupMenu=class()
RegistClassMember(CLuaExpWnd_PopupMenu,"m_MainButton")
RegistClassMember(CLuaExpWnd_PopupMenu,"m_IsRecordingMark")
RegistClassMember(CLuaExpWnd_PopupMenu,"m_Tweener")
RegistClassMember(CLuaExpWnd_PopupMenu,"m_CaptureButton")
RegistClassMember(CLuaExpWnd_PopupMenu,"m_ReplayKitButton")
RegistClassMember(CLuaExpWnd_PopupMenu,"m_ForumButton")
RegistClassMember(CLuaExpWnd_PopupMenu,"m_AddSprite")
RegistClassMember(CLuaExpWnd_PopupMenu,"m_IsRecordingSprite")
RegistClassMember(CLuaExpWnd_PopupMenu,"m_IsRecordingSprite2")
RegistClassMember(CLuaExpWnd_PopupMenu,"m_RedDotAlertObj1")
RegistClassMember(CLuaExpWnd_PopupMenu,"m_RedDotAlertObj2")


RegistClassMember(CLuaExpWnd_PopupMenu,"m_IsFold")
RegistClassMember(CLuaExpWnd_PopupMenu,"m_LastTime")
RegistClassMember(CLuaExpWnd_PopupMenu,"m_LastStatus")

function CLuaExpWnd_PopupMenu:Awake()
	--nothing to do
    self.m_IsFold = true
    self.m_LastTime = 0
    self.m_LastStatus = false
end

function CLuaExpWnd_PopupMenu:Init()
	self.m_IsFold = true;
    self.m_Tweener.transform.localScale = Vector3.zero
    self.m_ReplayKitButton:SetActive(Application.platform == RuntimePlatform.IPhonePlayer)

    --UI点击事件
    local function OnMainButtonClick(go)
        self:ShowSelf(self.m_IsFold)
    end

    local function OnCaptureButtonClick(go)
        self:ShowSelf(false)
        CUIManager.ShowUI(CUIResources.ScreenCaptureWnd)
    end

    local function OnReplayKitButtonClick(go)
        self:ShowSelf(false)
        if Application.platform == RuntimePlatform.IPhonePlayer then
            local ReplayManager = import "ReplayManager"  
            if (not ReplayManager.IsSupportReplay()) then
                MessageMgr.Inst:ShowMessage("ReplayKit_Cannot_Begin");
                return
            end
            if ReplayManager.IsRecording() then
                ReplayManager.StopRecording(true)
            else
                ReplayManager.StartRecording()
            end
        end
    end

    local function OnForumButtonClick(go)
        CWebBrowserMgr.Inst:OpenUrl(CWebBrowserMgr.s_ForumUrl)
        self:ShowSelf(false);
    end


    CommonDefs.AddOnClickListener(self.m_MainButton, DelegateFactory.Action_GameObject(OnMainButtonClick), false)
    CommonDefs.AddOnClickListener(self.m_CaptureButton, DelegateFactory.Action_GameObject(OnCaptureButtonClick), false)
    CommonDefs.AddOnClickListener(self.m_ReplayKitButton, DelegateFactory.Action_GameObject(OnReplayKitButtonClick), false)
    CommonDefs.AddOnClickListener(self.m_ForumButton, DelegateFactory.Action_GameObject(OnForumButtonClick), false)

    if(Application.platform == RuntimePlatform.IPhonePlayer) then    
        local Device = import "UnityEngine.iOS.Device"
        local DeviceGeneration = import "UnityEngine.iOS.DeviceGeneration"
        local ReplayManager = import "ReplayManager"    
        if (Device.generation == DeviceGeneration.iPhone5 or Device.generation == DeviceGeneration.iPhone5C) then
            self.m_ReplayKitButton:SetActive(false)
        end
        self.m_IsRecordingSprite:SetActive(ReplayManager.IsRecording())
        self.m_IsRecordingSprite2:SetActive(ReplayManager.IsRecording())
        self.m_AddSprite:SetActive(not ReplayManager.IsRecording())
    end
end

function CLuaExpWnd_PopupMenu:OnEnable()
    g_ScriptEvent:AddListener("GasDisconnect", self, "OnDisconnectGas")
    if(self:IsForumEnable()) then
        self.m_ForumButton:SetActive(true)
    else
        self.m_ForumButton:SetActive(false)
    end
end

function CLuaExpWnd_PopupMenu:OnDisable()
    g_ScriptEvent:RemoveListener("GasDisconnect", self, "OnDisconnectGas")
end

function CLuaExpWnd_PopupMenu:Update()
    self:ClickThroughToClose()
    if (Time.realtimeSinceStartup - self.m_LastTime > 0.2) then
        self.m_LastTime = Time.realtimeSinceStartup
        if Application.platform == RuntimePlatform.IPhonePlayer then
            local ReplayManager = import "ReplayManager"     
            local isRecording = ReplayManager.IsRecording()
            if (self.m_LastStatus ~= isRecording) then
                self.m_LastStatus = isRecording
                self.m_IsRecordingSprite:SetActive(isRecording)
                self.m_IsRecordingSprite2:SetActive(isRecording)
                self.m_AddSprite:SetActive(not isRecording)
                if (isRecording) then
                    MessageMgr.Inst:ShowMessage("ReplayKit_Begin")
                end
            end
        end
    end
end

function CLuaExpWnd_PopupMenu:OnDestroy()
    CLuaExpWnd_PopupMenu = nil
end

function CLuaExpWnd_PopupMenu:ShowSelf(show)
    if (show and self.m_IsFold) then
        self.m_IsFold = false
        self.m_Tweener:PlayForward()
        self.m_AddSprite:GetComponent(typeof(UITweener)):PlayForward()
    elseif (not show and not self.m_IsFold) then
            self.m_IsFold = true
            self.m_Tweener:PlayReverse()
            self.m_AddSprite:GetComponent(typeof(UITweener)):PlayReverse()
    end
end

function CLuaExpWnd_PopupMenu:OnDisconnectGas()
    if(Application.platform == RuntimePlatform.IPhonePlayer) then  
        local ReplayManager = import "ReplayManager"    
        if (ReplayManager.IsRecording()) then
            ReplayManager.StopRecording(false)
        end
    end
end

function CLuaExpWnd_PopupMenu:IsForumEnable()
    return CommonDefs.IsPlayEnabled("InnerLuntan")
end

function CLuaExpWnd_PopupMenu:ClickThroughToClose()
    if Input.GetMouseButtonDown(0) then       
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform) and
            CUICommonDef.GetNearestPanelDepth(self.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
            self:ShowSelf(false)
        end
    end
end

return CLuaExpWnd_PopupMenu