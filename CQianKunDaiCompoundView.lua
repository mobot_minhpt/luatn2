-- Auto Generated!!
local BoolDelegate = import "UIEventListener+BoolDelegate"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CEquipment = import "L10.Game.CEquipment"
local CItem = import "L10.Game.CItem"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CQianKunDaiCompoundView = import "L10.UI.CQianKunDaiCompoundView"
local CQianKunDaiMgr = import "L10.Game.CQianKunDaiMgr"
local CQianKunDaiSelectionItem = import "L10.UI.CQianKunDaiSelectionItem"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local Debug = import "UnityEngine.Debug"
local DelegateFactory = import "DelegateFactory"
local Ease = import "DG.Tweening.Ease"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local IdPartition = import "L10.Game.IdPartition"
local Int32 = import "System.Int32"
local Item_Item = import "L10.Game.Item_Item"
local ItemInBagItemInfo = import "L10.Game.ItemInBagItemInfo"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local MsgPackImpl = import "MsgPackImpl"
local NGUIText = import "NGUIText"
local Object = import "System.Object"
local QianKunDai_Formula = import "L10.Game.QianKunDai_Formula"
local String = import "System.String"
local StringActionKeyValuePair = import "L10.Game.StringActionKeyValuePair"
local StringBuilder = import "System.Text.StringBuilder"
local TweenPosition = import "TweenPosition"
local TweenScale = import "TweenScale"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local QianKunDai_Setting = import "L10.Game.QianKunDai_Setting"
local QianKunDai_Item = import "L10.Game.QianKunDai_Item"
CQianKunDaiCompoundView.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_CompoundBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.OnCompoundBtnClicked, VoidDelegate, this)
    UIEventListener.Get(this.m_CompoundBtn.gameObject).onPress = CommonDefs.CombineListner_BoolDelegate(UIEventListener.Get(this.m_CompoundBtn.gameObject).onPress, MakeDelegateFromCSFunction(this.OnCompoundBtnPress, BoolDelegate, this), true)
    UIEventListener.Get(this.m_FormulaBg).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_FormulaBg).onClick, MakeDelegateFromCSFunction(this.OnFormulaBgClicked, VoidDelegate, this), true)
end
CQianKunDaiCompoundView.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.QianKunDaiPutInItem, MakeDelegateFromCSFunction(this.OnPutInItem, MakeGenericClass(Action2, UInt32, String), this))
    EventManager.AddListener(EnumEventType.QianKunDaiClearAllSelection, MakeDelegateFromCSFunction(this.ClearAllSelections, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.QianKunDaiHeChengSuccess, MakeDelegateFromCSFunction(this.OnHeChengItemSuccess, MakeGenericClass(Action2, String, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.QianKunDaiUpdateFormula, MakeDelegateFromCSFunction(this.UnpdateFormula, MakeGenericClass(Action1, UInt32), this))
    EventManager.AddListener(EnumEventType.QianKunDaiShakeFinish, MakeDelegateFromCSFunction(this.QianKunDaiShakeFinish, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
end
CQianKunDaiCompoundView.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.QianKunDaiPutInItem, MakeDelegateFromCSFunction(this.OnPutInItem, MakeGenericClass(Action2, UInt32, String), this))
    EventManager.RemoveListener(EnumEventType.QianKunDaiClearAllSelection, MakeDelegateFromCSFunction(this.ClearAllSelections, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.QianKunDaiHeChengSuccess, MakeDelegateFromCSFunction(this.OnHeChengItemSuccess, MakeGenericClass(Action2, String, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.QianKunDaiUpdateFormula, MakeDelegateFromCSFunction(this.UnpdateFormula, MakeGenericClass(Action1, UInt32), this))
    EventManager.RemoveListener(EnumEventType.QianKunDaiShakeFinish, MakeDelegateFromCSFunction(this.QianKunDaiShakeFinish, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
end
CQianKunDaiCompoundView.m_OnDestroy_CS2LuaHook = function (this) 
    if this.m_Tick ~= nil then
        invoke(this.m_Tick)
        this.m_Tick = nil
    end
    CQianKunDaiCompoundView.InBagAnimation = false
end
CQianKunDaiCompoundView.m_Init_CS2LuaHook = function (this, recipeId) 
    this.RecipeId = recipeId
    this:ClearAllSelectedItems()
    this:UpdateSelectedItems()

    this:UpdateFormulaInfo()
    this:UpdateJiFen(true)

    local p = CommonDefs.GetComponent_Component_Type(this.m_CompoundBtn, typeof(TweenPosition))
    if p ~= nil then
        p.enabled = false
    end
    local s = CommonDefs.GetComponent_GameObject_Type(this.m_BagTexture, typeof(TweenScale))
    if s ~= nil then
        s.enabled = false
    end

    this:SamleAni(this.m_BagAnimation, this.BagClipName, 0)
    this.m_Result.gameObject:SetActive(false)
    this.m_Mask:SetActive(false)
    CQianKunDaiCompoundView.InBagAnimation = false
end
CQianKunDaiCompoundView.m_UnpdateFormula_CS2LuaHook = function (this, recipeId) 
    if this.RecipeId == recipeId then
        return
    end

    this:Init(recipeId)
end
CQianKunDaiCompoundView.m_ClearAllSelections_CS2LuaHook = function (this) 
    this:ClearAllSelectedItems()
    this:UpdateSelectedItems()
end
CQianKunDaiCompoundView.m_UpdateSelectedItems_CS2LuaHook = function (this) 
    for i=0,this.SelectedItems.Length-1,1 do
        local id = this.SelectedItems[i]
        if id==nil or id=="" then
            this.SelectedItems[i] = nil
        else
            if not ItemInBagItemInfo.ItemIdIsFakedBagItem(id) then
                local item = CItemMgr.Inst:GetById(id)
                if item == nil then
                    this.SelectedItems[i] = nil
                elseif item.IsEquip and item.IsEquiped then
                    this.SelectedItems[i] = nil
                end
            end
        end
    end

    CQianKunDaiCompoundView.InBagAnimation = false
    do
        local i = 0
        while i < this.m_CompoundSelections.Length do
            this.m_CompoundSelections[i].gameObject:SetActive(true)
            if System.String.IsNullOrEmpty(this.SelectedItems[i]) then
                this.m_CompoundSelections[i]:Init(i)
                this.m_CompoundSelections[i].OnItemClickDelegate = nil
                this.m_CompoundSelections[i].OnItemDoubleClickDelegate = nil
            else
                this.m_CompoundSelections[i]:Init(this.SelectedItems[i], i)
                this.m_CompoundSelections[i].OnItemClickDelegate = MakeDelegateFromCSFunction(this.OnItemClick, MakeGenericClass(Action1, CQianKunDaiSelectionItem), this)
                this.m_CompoundSelections[i].OnItemDoubleClickDelegate = MakeDelegateFromCSFunction(this.OnItemDoubleClick, MakeGenericClass(Action1, CQianKunDaiSelectionItem), this)
            end
            i = i + 1
        end
    end
end
CQianKunDaiCompoundView.m_UpdateFormulaInfo_CS2LuaHook = function (this) 
    local formula = QianKunDai_Formula.GetData(this.RecipeId)
    if formula ~= nil then
        this.m_FormulaInput.text = formula.MeterialDesc
        this.m_FormulaInput:UpdateNGUIText()
        NGUIText.regionWidth = 1000000
        local inputSize = NGUIText.CalculatePrintedSize(formula.MeterialDesc)
        this.m_FormulaInput.width = inputSize.x > 334 and 334 or inputSize.x

        this.m_FormulaOutput.text = formula.ProductDesc
        this.m_FormulaOutput:UpdateNGUIText()
        NGUIText.regionWidth = 1000000
        local outputSize = NGUIText.CalculatePrintedSize(formula.ProductDesc)
        this.m_FormulaOutput.width = outputSize.x > 236 and 236 or outputSize.x

        local totalSize = this.m_FormulaInput.width + 20 + 60 + 20 + this.m_FormulaOutput.width

        local inputX = - totalSize / 2
        local arrowX = inputX + this.m_FormulaInput.width + 20 + 60
        local outputX = arrowX + 20

        Extensions.SetLocalPositionX(this.m_FormulaInput.transform, inputX)
        Extensions.SetLocalPositionX(this.m_Arrow.transform, arrowX)
        Extensions.SetLocalPositionX(this.m_FormulaOutput.transform, outputX)
    end
end
CQianKunDaiCompoundView.m_OnCompoundBtnPress_CS2LuaHook = function (this, go, state) 

    if this.m_CompoundBtn.Selected then
        return
    end

    if state then
        this.m_NormalText:SetActive(false)
        this.m_HightlightBG:SetActive(true)
        this.m_HightlightText:SetActive(true)
    else
        this.m_NormalText:SetActive(true)
        this.m_HightlightBG:SetActive(false)
        this.m_HightlightText:SetActive(false)
    end
end
CQianKunDaiCompoundView.m_OnFormulaBgClicked_CS2LuaHook = function (this, go) 
    CQianKunDaiMgr.Inst:ShowFormulaDetail(this.RecipeId)
end
CQianKunDaiCompoundView.m_GetSelectedItemCount_CS2LuaHook = function (this) 
    local count = 0
    do
        local i = 0
        while i < this.SelectedItems.Length do
            if not System.String.IsNullOrEmpty(this.SelectedItems[i]) then
                count = count + 1
            end
            i = i + 1
        end
    end
    return count
end
CQianKunDaiCompoundView.m_GetFirstSelectedItem_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.SelectedItems.Length do
            if not System.String.IsNullOrEmpty(this.SelectedItems[i]) then
                return this.SelectedItems[i]
            end
            i = i + 1
        end
    end
    return nil
end
CQianKunDaiCompoundView.m_GetSelectedItemList_CS2LuaHook = function (this) 
    for i=0,this.SelectedItems.Length-1,1 do
        if System.String.IsNullOrEmpty(this.SelectedItems[i]) then
            this.SelectedItems[i] = nil
        else
            if not ItemInBagItemInfo.ItemIdIsFakedBagItem(this.SelectedItems[i]) then
                local item = CItemMgr.Inst:GetById(this.SelectedItems[i])
                if item == nil then
                    this.SelectedItems[i] = nil
                end
            end
        end
    end
    local list = CreateFromClass(MakeGenericClass(List, String))
    do
        local i = 0
        while i < this.SelectedItems.Length do
            if not System.String.IsNullOrEmpty(this.SelectedItems[i]) then
                CommonDefs.ListAdd(list, typeof(String), this.SelectedItems[i])
            end
            i = i + 1
        end
    end
    return list
end
CQianKunDaiCompoundView.m_ClearAllSelectedItems_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.SelectedItems.Length do
            this.SelectedItems[i] = nil
            i = i + 1
        end
    end
end
CQianKunDaiCompoundView.m_GetFirstEmptyItemPos_CS2LuaHook = function (this) 
    local counter = 0
    do
        counter = 0
        while counter < this.SelectedItems.Length do
            if System.String.IsNullOrEmpty(this.SelectedItems[counter]) then
                return counter
            end
            counter = counter + 1
        end
    end
    return counter
end
CQianKunDaiCompoundView.m_OnCompoundBtnClicked_CS2LuaHook = function (this, go) 

    -- 检查包裹是否已满
    if not CClientMainPlayer.Inst then
        return
    end
    if CClientMainPlayer.Inst.ItemProp:GetEmptyPosCount(EnumItemPlace.Bag) < 1 then
        g_MessageMgr:ShowMessage("QKD_NOT_ENOUGH_SPACE")
        return
    end

    -- 是否有选中的物品
    if this:GetSelectedItemCount() == 0 then
        g_MessageMgr:ShowMessage("QIANKUNDAI_SELECT_NOTHING")
        return
    end

    -- 是否满足合成公式
    if not CQianKunDaiMgr.Inst:MatchFormula(this.RecipeId, this:GetSelectedItemList()) then
        if this.RecipeId == CQianKunDaiMgr.Inst.JiFenFormulaId then
            local itemId =  this:GetSelectedItemList()[0]
            local item = CItemMgr.Inst:GetById(itemId)
            local qItem = QianKunDai_Item.GetData(item.TemplateId)
            g_MessageMgr:ShowMessage("BAGUALU_LINGLI_EXCHANGE_NUM", qItem.MinNum, item.Name)
        else
            g_MessageMgr:ShowMessage("MATERIAL_NOT_SATISFY_FORMULA")
        end
        
        return
    end

    if this.RecipeId == CQianKunDaiMgr.Inst.JiNengShuFormulaId then
        this:Compound_JiNengShu()
    elseif this.RecipeId == CQianKunDaiMgr.Inst.WenShiFormulaId then
        this:Compound_WenShi()
    else
        this:Compound_Default()
    end
end
CQianKunDaiCompoundView.m_Compound_Default_CS2LuaHook = function (this) 
    -- 检查绑定是否一致

    local formula = QianKunDai_Formula.GetData(this.RecipeId)
    if formula ~= nil and formula.IsBangDing == 1 then
        local sameBinded = this:CheckSelectedItemsSameBinded()
        if not sameBinded then
            g_MessageMgr:ShowMessage("QIANKUNDAI_MATERIAL_NEED_SAME_BIND")
            return
        end
    end

    if formula ~= nil and formula.IsBangDing == 0 then
        local sameBinded = this:CheckSelectedItemsSameBinded()
        if not sameBinded then
            local title = g_MessageMgr:FormatMessage("QIANKUNDAI_MATERIAL_HECHENG_BIND")
            MessageWndManager.ShowOKCancelMessage(title, DelegateFactory.Action(function () 
                this:Compound_Default_Request()
            end), nil, nil, nil, false)
            return
        end
    end
    this:Compound_Default_Request()
end
CQianKunDaiCompoundView.m_Compound_Default_Request_CS2LuaHook = function (this) 
    CQianKunDaiMgr.Inst:ShowQianKunDaiCost(this.RecipeId, DelegateFactory.Action(function () 
        local hasPrevious = false
        local previousNames = nil
        local posList = CreateFromClass(MakeGenericClass(List, Object))
        if not CClientMainPlayer.Inst then return end
        do
            local i = 0
            while i < this.SelectedItems.Length do
                local continue
                repeat
                    if System.String.IsNullOrEmpty(this.SelectedItems[i]) then
                        continue = true
                        break
                    end

                    local item = CItemMgr.Inst:GetById(this.SelectedItems[i])
                    if item.IsPrecious then
                        hasPrevious = true
                        if previousNames == nil then
                            previousNames = NewStringBuilderWraper(StringBuilder, item.ColoredName)
                        else
                            previousNames:Append(", " .. item.ColoredName)
                        end
                    end
                    local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(EnumItemPlace.Bag, this.SelectedItems[i])
                    CommonDefs.ListAdd(posList, typeof(UInt32), pos)
                    continue = true
                until 1
                if not continue then
                    break
                end
                i = i + 1
            end
        end

        if hasPrevious then
            local message = g_MessageMgr:FormatMessage("QIANKUNDAI_PRECIOUS_HINT", ToStringWrap(previousNames))
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
                local setting = QianKunDai_Setting.GetData()
                
                if this.RecipeId == setting.ScoreFormulaId then
                    Gac2Gas.RequestBaGuaLuHeChengScore(MsgPackImpl.pack(posList))
                else
                    Gac2Gas.RequestHeChengItem(this.RecipeId, MsgPackImpl.pack(posList))
                end
                CQianKunDaiCompoundView.InBagAnimation = true
            end), nil, nil, nil, false)
        else
            local setting = QianKunDai_Setting.GetData()
            if this.RecipeId == setting.ScoreFormulaId then
                Gac2Gas.RequestBaGuaLuHeChengScore(MsgPackImpl.pack(posList))
            else
                Gac2Gas.RequestHeChengItem(this.RecipeId, MsgPackImpl.pack(posList))
            end
            CQianKunDaiCompoundView.InBagAnimation = true
        end
    end), this:GetSelectedItemList())
end
CQianKunDaiCompoundView.m_Compound_JiNengShu_CS2LuaHook = function (this) 
    -- 检查绑定是否一致，如果有小书箱中的物品，则表示不绑定

    local formula = QianKunDai_Formula.GetData(this.RecipeId)
    if formula ~= nil and formula.IsBangDing == 1 then
        local sameBinded = this:CheckSelectedItemsSameBinded()
        if not sameBinded then
            g_MessageMgr:ShowMessage("QIANKUNDAI_MATERIAL_NEED_SAME_BIND")
            return
        end
    end

    if formula ~= nil and formula.IsBangDing == 0 then
        local sameBinded = this:CheckSelectedItemsSameBinded()
        if not sameBinded then
            local title = g_MessageMgr:FormatMessage("QIANKUNDAI_MATERIAL_HECHENG_BIND")
            MessageWndManager.ShowOKCancelMessage(title, DelegateFactory.Action(function () 
                this:Compound_JiNengShu_Request()
            end), nil, nil, nil, false)
            return
        end
    end
    this:Compound_JiNengShu_Request()
end
CQianKunDaiCompoundView.m_Compound_JiNengShu_Request_CS2LuaHook = function (this) 
    CQianKunDaiMgr.Inst:ShowQianKunDaiCost(this.RecipeId, DelegateFactory.Action(function () 
        local posList = CreateFromClass(MakeGenericClass(List, Object))
        if not CClientMainPlayer.Inst then
            return
        end
        do
            local i = 0
            while i < this.SelectedItems.Length do
                local continue
                repeat
                    if System.String.IsNullOrEmpty(this.SelectedItems[i]) then
                        continue = true
                        break
                    end

                    if this:ItemIdIsFakedBagItem(this.SelectedItems[i]) then
                        local info = ItemInBagItemInfo.ParseStringToInfo(this.SelectedItems[i])
                        local detailPos = CreateFromClass(MakeGenericClass(List, Object))
                        CommonDefs.ListAdd(detailPos, typeof(UInt32), info.BagItemPos)
                        CommonDefs.ListAdd(detailPos, typeof(UInt32), info.PosInBagItem + 1)
                        CommonDefs.ListAdd(posList, typeof(MakeGenericClass(List, Object)), detailPos)
                    else
                        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(EnumItemPlace.Bag, this.SelectedItems[i])
                        local detailPos = CreateFromClass(MakeGenericClass(List, Object))
                        CommonDefs.ListAdd(detailPos, typeof(UInt32), pos)
                        CommonDefs.ListAdd(detailPos, typeof(Int32), 0)
                        CommonDefs.ListAdd(posList, typeof(MakeGenericClass(List, Object)), detailPos)
                    end
                    continue = true
                until 1
                if not continue then
                    break
                end
                i = i + 1
            end
        end
        Gac2Gas.RequestHeChengJiNengShu(this.RecipeId, MsgPackImpl.pack(posList))
        CQianKunDaiCompoundView.InBagAnimation = true
    end), this:GetSelectedItemList())
end
CQianKunDaiCompoundView.m_Compound_WenShi_CS2LuaHook = function (this) 
    local formula = QianKunDai_Formula.GetData(this.RecipeId)
    if formula ~= nil and formula.IsBangDing == 1 then
        local sameBinded = this:CheckSelectedItemsSameBinded()
        if not sameBinded then
            g_MessageMgr:ShowMessage("QIANKUNDAI_MATERIAL_NEED_SAME_BIND")
            return
        end
    end

    if formula ~= nil and formula.IsBangDing == 0 then
        local sameBinded = this:CheckSelectedItemsSameBinded()
        if not sameBinded then
            local title = g_MessageMgr:FormatMessage("QIANKUNDAI_MATERIAL_HECHENG_BIND")
            MessageWndManager.ShowOKCancelMessage(title, DelegateFactory.Action(function () 
                this:Compound_WenShi_Request()
            end), nil, nil, nil, false)
            return
        end
    end

    this:Compound_WenShi_Request()
end
CQianKunDaiCompoundView.m_Compound_WenShi_Request_CS2LuaHook = function (this) 
    CQianKunDaiMgr.Inst:ShowQianKunDaiCost(this.RecipeId, DelegateFactory.Action(function () 
        local posList = CreateFromClass(MakeGenericClass(List, Object))
        if not CClientMainPlayer.Inst then
            return
        end
        do
            local i = 0
            while i < this.SelectedItems.Length do
                local continue
                repeat
                    if System.String.IsNullOrEmpty(this.SelectedItems[i]) then
                        continue = true
                        break
                    end

                    if this:ItemIdIsFakedBagItem(this.SelectedItems[i]) then
                        local info = ItemInBagItemInfo.ParseStringToInfo(this.SelectedItems[i])
                        local detailPos = CreateFromClass(MakeGenericClass(List, Object))
                        CommonDefs.ListAdd(detailPos, typeof(UInt32), info.BagItemPos)
                        CommonDefs.ListAdd(detailPos, typeof(UInt32), info.PosInBagItem + 1)
                        CommonDefs.ListAdd(posList, typeof(MakeGenericClass(List, Object)), detailPos)
                    else
                        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(EnumItemPlace.Bag, this.SelectedItems[i])
                        local detailPos = CreateFromClass(MakeGenericClass(List, Object))
                        CommonDefs.ListAdd(detailPos, typeof(UInt32), pos)
                        CommonDefs.ListAdd(detailPos, typeof(Int32), 0)
                        CommonDefs.ListAdd(posList, typeof(MakeGenericClass(List, Object)), detailPos)
                    end
                    continue = true
                until 1
                if not continue then
                    break
                end
                i = i + 1
            end
        end
        Gac2Gas.RequestHeChengWenShi(this.RecipeId, MsgPackImpl.pack(posList))
        CQianKunDaiCompoundView.InBagAnimation = true
    end), this:GetSelectedItemList())
end
CQianKunDaiCompoundView.m_CheckSelectedItemsSameBinded_CS2LuaHook = function (this) 
    -- 检查绑定是否一致
    local firstSelectedItem = this:GetFirstSelectedItem()

    if System.String.IsNullOrEmpty(firstSelectedItem) then
        return true
    end

    local notSameIsBind = false
    local firstBinded = false
    if not ItemInBagItemInfo.ItemIdIsFakedBagItem(firstSelectedItem) then
        local first = CItemMgr.Inst:GetById(firstSelectedItem)
        notSameIsBind = first.IsBinded
        firstBinded = first.IsBinded
    else
        local info = ItemInBagItemInfo.ParseStringToInfo(firstSelectedItem)
        notSameIsBind = info.IsBind
        firstBinded = info.IsBind
    end


    do
        local i = 1
        while i < this.SelectedItems.Length do
            local continue
            repeat
                if System.String.IsNullOrEmpty(this.SelectedItems[i]) then
                    continue = true
                    break
                end

                if not ItemInBagItemInfo.ItemIdIsFakedBagItem(this.SelectedItems[i]) then
                    local item = CItemMgr.Inst:GetById(this.SelectedItems[i])
                    if item == nil then
                        continue = true
                        break
                    end
                    notSameIsBind = firstBinded ~= item.IsBinded
                    -- 若有绑定不一致，则notSameIsBind为true
                else
                    local info = ItemInBagItemInfo.ParseStringToInfo(this.SelectedItems[i])
                    if info == nil then
                        continue = true
                        break
                    end
                    notSameIsBind = firstBinded ~= info.IsBind
                end

                if notSameIsBind then
                    return false
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
    return true
end
CQianKunDaiCompoundView.m_QianKunDaiShakeFinish_CS2LuaHook = function (this) 
    -- 包裹动画停止
    this:SamleAni(this.m_BagAnimation, this.BagClipName, 0)

    -- 包裹复位
    CommonDefs.SetEase_Tweener(LuaTweenUtils.DOLocalMoveY(this.m_CompoundBtn.transform, - 380, 0.2, false), Ease.Linear)

    -- 显示result，转换积分的话，不需要显示result
    if not System.String.IsNullOrEmpty(this.m_Result.m_ItemId) and not (this.m_Result.m_ItemId == "0") then
        this.m_Result.gameObject:SetActive(true)
        this:DisplayObtainItemMessage(this.m_Result.m_ItemId)
        local position = CommonDefs.GetComponent_Component_Type(this.m_Result, typeof(TweenPosition))
        if position ~= nil then
            CommonDefs.ListClear(position.onFinished)
            position:SetOnFinished(DelegateFactory.Callback(function () 
                this.m_Result.transform.localPosition = position.from
                this.m_Result.gameObject:SetActive(false)
                position:ResetToBeginning()

                -- 重新显示九宫格
                this:ReShowSelectedItems()
                this.m_Mask:SetActive(false)
                CQianKunDaiCompoundView.InBagAnimation = false
            end))
            position:PlayForward()
        end
    else
        local position = CommonDefs.GetComponent_Component_Type(this.m_Result, typeof(TweenPosition))
        this:DisplayObtainItemMessage(this.m_Result.m_ItemId)
        if position ~= nil then
            position:ResetToBeginning()
        end
        this:ReShowSelectedItems()
        this.m_Mask:SetActive(false)
        CQianKunDaiCompoundView.InBagAnimation = false
    end
end
CQianKunDaiCompoundView.m_OnSetItemAt_CS2LuaHook = function (this, place, position, oldItemId, newItemId) 

    if place ~= EnumItemPlace.Bag or CClientMainPlayer.Inst == nil or position == 0 then
        return
    end
    -- 动画期间不做处理
    if CQianKunDaiCompoundView.InBagAnimation then
        return
    end

    this:UpdateSelectedItems()
end
CQianKunDaiCompoundView.m_OnSendItem_CS2LuaHook = function (this, itemId) 

    if System.String.IsNullOrEmpty(itemId) then
        return
    end

    -- 动画期间不做处理
    if CQianKunDaiCompoundView.InBagAnimation then
        return
    end

    do
        local i = 0
        while i < this.SelectedItems.Length do
            if not System.String.IsNullOrEmpty(this.SelectedItems[i]) and (itemId == this.SelectedItems[i]) then
                this.m_CompoundSelections[i]:Init(itemId, i)
                break
            end
            i = i + 1
        end
    end
end
CQianKunDaiCompoundView.m_DisplayObtainItemMessage_CS2LuaHook = function (this, compoundItemId) 

    if System.String.IsNullOrEmpty(compoundItemId) or (compoundItemId == "0") then
        g_MessageMgr:ShowMessage("QIANKUNDAI_OBTAIN_SCORE", tostring(this.ScoreGot))
    else
        local commonItem = CItemMgr.Inst:GetById(compoundItemId)
        if IdPartition.IdIsItem(commonItem.TemplateId) then
            local item = Item_Item.GetData(commonItem.TemplateId)
            if item ~= nil then
                g_MessageMgr:ShowMessage("QIANKUNDAI_OBTAIN_ITEM", CItem.GetColorString(commonItem.TemplateId), commonItem.Name, tostring(this.ScoreGot))
            end
        else
            local equip = EquipmentTemplate_Equip.GetData(commonItem.TemplateId)
            if equip ~= nil then
                g_MessageMgr:ShowMessage("QIANKUNDAI_OBTAIN_ITEM", NGUIText.EncodeColor24(CEquipment.GetColor(commonItem.TemplateId)), commonItem.Name, tostring(this.ScoreGot))
            end
        end
    end
end
CQianKunDaiCompoundView.m_ReShowSelectedItems_CS2LuaHook = function (this) 

    do
        local i = 0
        while i < this.SelectedItems.Length do
            local continue
            repeat
                if System.String.IsNullOrEmpty(this.SelectedItems[i]) then
                    continue = true
                    break
                end

                local itemId = this.SelectedItems[i]

                if this:ItemIdIsFakedBagItem(itemId) then
                    -- 此处检查小书箱中是否还有该物品，及对应pos的templateId是否一致
                    -- 不一致证明已经没有该物品，需要去掉
                    -- 一致需要更新数量
                    local itemBagInfo = ItemInBagItemInfo.ParseStringToInfo(itemId)
                    if itemBagInfo ~= nil then
                        local bagItem = CItemMgr.Inst:GetPackageItemAtPos(itemBagInfo.BagItemPos)
                        if bagItem ~= nil and bagItem.IsItem and (bagItem.Item:IsXiaoShuXiang() or bagItem.Item:IsWenShiNang()) then
                            local allItemsInBag = bagItem.Item:GetAllItemsInItem()
                            local foundSameTemplate = false
                            do
                                local j = 0
                                while j < allItemsInBag.Count do
                                    if allItemsInBag[j].itemTemplateId == itemBagInfo.TemplateId and allItemsInBag[j].isBind == itemBagInfo.IsBind then
                                        local updateInfo = (function () 
                                            local __localValue = CreateFromClass(ItemInBagItemInfo)
                                            __localValue.BagItemPos = itemBagInfo.BagItemPos
                                            __localValue.PosInBagItem = j
                                            __localValue.TemplateId = itemBagInfo.TemplateId
                                            __localValue.Count = allItemsInBag[j].itemCount
                                            __localValue.IsBind = allItemsInBag[j].isBind
                                            return __localValue
                                        end)()
                                        this.SelectedItems[i] = updateInfo:GetItemBagInfoString()
                                        local itemT = Item_Item.GetData(itemBagInfo.TemplateId)
                                        Debug.Log(System.String.Format("selected item {0}, {1}, {2}", i, itemT.Name, updateInfo.Count))
                                        foundSameTemplate = true
                                        break
                                    end
                                    j = j + 1
                                end
                            end
                            if not foundSameTemplate then
                                this.SelectedItems[i] = nil
                            end
                        end
                    else
                        this.SelectedItems[i] = nil
                    end
                else
                    local item = CItemMgr.Inst:GetById(itemId)
                    if item == nil then
                        this.SelectedItems[i] = nil
                    end
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end

    -- 放入生成的物品，积分不需要
    local newItem = CItemMgr.Inst:GetById(this.m_Result.m_ItemId)
    if newItem ~= nil then
        if this.SelectedItems[4] == nil then
            this.SelectedItems[4] = this.m_Result.m_ItemId
        else
            local count = this:GetSelectedItemCount()
            if count < CQianKunDaiCompoundView.MAX_SELECTION_COUNT then
                local middleItem = this.SelectedItems[4]
                local emptyPos = this:GetFirstEmptyItemPos()

                if emptyPos < this.SelectedItems.Length then
                    this.SelectedItems[emptyPos] = middleItem
                    this.SelectedItems[4] = this.m_Result.m_ItemId
                end
            end
        end
    end

    EventManager.BroadcastInternalForLua(EnumEventType.QianKunDaiSyncSelectItems, {this:GetSelectedItemList()})

    do
        local i = 0
        while i < this.m_CompoundSelections.Length do
            local position = CommonDefs.GetComponent_Component_Type(this.m_CompoundSelections[i], typeof(TweenPosition))
            if position ~= nil then
                position:ResetToBeginning()
            end
            i = i + 1
        end
    end
    this:UpdateSelectedItems()
    this:UpdateJiFen(false)

    do
        local i = 0
        while i < this.SelectedItems.Length do
            if this.SelectedItems[i] == this.m_Result.m_ItemId and this.m_CompoundSelections[i] ~= nil then
                this:OnItemClick(this.m_CompoundSelections[i])
            end
            i = i + 1
        end
    end
end
CQianKunDaiCompoundView.m_UpdateJiFen_CS2LuaHook = function (this, isInit) 
    local jiFen = 0
    if CClientMainPlayer.Inst ~= nil then
        jiFen = CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.QianKunDai)
    end

    if not isInit then
        this.m_JiFenFx:LoadFx(CUIFxPaths.ExchangePlantWordFx)
        this.m_JiFenFx.gameObject:SetActive(true)
    else
        this.m_JiFenFx.gameObject:SetActive(false)
    end
    this.m_JiFenLabel.text = tostring(jiFen)
    this.m_JiFenLabel.gameObject:SetActive(true)
end
CQianKunDaiCompoundView.m_OnPutInItem_CS2LuaHook = function (this, pos, itemId) 
    local emptyPos
    do
        emptyPos = 0
        while emptyPos < this.SelectedItems.Length do
            if this.SelectedItems[emptyPos] == nil then
                break
            end
            emptyPos = emptyPos + 1
        end
    end
    if emptyPos <= this.SelectedItems.Length then
        this.SelectedItems[emptyPos] = itemId
    else
        g_MessageMgr:ShowMessage("MAX_HECHENG_NUM")
        return
    end
    this:UpdateSelectedItems()
end
CQianKunDaiCompoundView.m_OnTakeOut_CS2LuaHook = function (this) 

    if this:ItemIdIsFakedBagItem(this.SelectedItemId) then
        local bagItemInfo = ItemInBagItemInfo.ParseStringToInfo(this.SelectedItemId)
        if bagItemInfo ~= nil then
            this.SelectedItems[this.SelectedBagPos] = nil
            EventManager.BroadcastInternalForLua(EnumEventType.QianKunDaiTakeOutItem, {this.SelectedItemId})
            this:UpdateSelectedItems()
            CItemInfoMgr.CloseItemInfoWnd()
        end
    else
        local item = CItemMgr.Inst:GetById(this.SelectedItemId)
        if item ~= nil and CClientMainPlayer.Inst ~= nil then
            this.SelectedItems[this.SelectedBagPos] = nil
            EventManager.BroadcastInternalForLua(EnumEventType.QianKunDaiTakeOutItem, {this.SelectedItemId})
            this:UpdateSelectedItems()
            CItemInfoMgr.CloseItemInfoWnd()
        end
    end
end
CQianKunDaiCompoundView.m_OnItemClick_CS2LuaHook = function (this, cell) 

    do
        local i = 0
        while i < this.m_CompoundSelections.Length do
            this.m_CompoundSelections[i].Selected = (this.m_CompoundSelections[i] == cell)
            if this.m_CompoundSelections[i] == cell then
                this.SelectedBagPos = i
            end
            i = i + 1
        end
    end

    local itemId = cell.ItemId
    if System.String.IsNullOrEmpty(itemId) then
        return
    end

    if (string.find(itemId, ":", 1, true) ~= nil) then
        local bagItemInfo = ItemInBagItemInfo.ParseStringToInfo(itemId)
        if bagItemInfo == nil then
            return
        end
        this.SelectedItemId = itemId
        CItemInfoMgr.ShowLinkItemTemplateInfo(bagItemInfo.TemplateId, false, this, CItemInfoMgr.AlignType.ScreenRight, 0, 0, 0, 0)
    else
        if not CClientMainPlayer.Inst then
            return
        end
        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(EnumItemPlace.Bag, itemId)
        if pos == 0 then
            return
        end
        local item = CItemMgr.Inst:GetById(itemId)
        if item == nil then
            return
        end
        this.SelectedItemId = itemId
        CItemInfoMgr.ShowQianKunDaiItem(itemId, false, this, CItemInfoMgr.AlignType.ScreenRight)
    end
end
CQianKunDaiCompoundView.m_OnItemDoubleClick_CS2LuaHook = function (this, cell) 
    do
        local i = 0
        while i < this.m_CompoundSelections.Length do
            this.m_CompoundSelections[i].Selected = (this.m_CompoundSelections[i] == cell)
            if this.m_CompoundSelections[i] == cell then
                this.SelectedBagPos = i
            end
            i = i + 1
        end
    end

    local itemId = cell.ItemId
    if System.String.IsNullOrEmpty(itemId) then
        return
    end

    if (string.find(itemId, ":", 1, true) ~= nil) then
        local bagItemInfo = ItemInBagItemInfo.ParseStringToInfo(itemId)
        if bagItemInfo == nil then
            return
        end
        this.SelectedItemId = itemId
        this:OnTakeOut()
    else
        if not CClientMainPlayer.Inst then
            return
        end
        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(EnumItemPlace.Bag, itemId)
        if pos == 0 then
            return
        end
        local item = CItemMgr.Inst:GetById(itemId)
        if item == nil then
            return
        end

        this.SelectedItemId = itemId
        this:OnTakeOut()
    end
end
CQianKunDaiCompoundView.m_GetActionPairs_CS2LuaHook = function (this, itemId, templateId) 
    if CClientMainPlayer.Inst == nil or System.String.IsNullOrEmpty(this.SelectedItemId) then
        return nil
    end

    if this:ItemIdIsFakedBagItem(this.SelectedItemId) then
        local bagItemInfo = ItemInBagItemInfo.ParseStringToInfo(this.SelectedItemId)
        if bagItemInfo == nil then
            return nil
        end

        local item = CItemMgr.Inst:GetPackageItemAtPos(bagItemInfo.BagItemPos)
        if item ~= nil and item.IsItem and (item.Item:IsXiaoShuXiang() or item.Item:IsWenShiNang()) then
            local itemInfos = item.Item:GetAllItemsInItem()
            if itemInfos ~= nil and itemInfos[bagItemInfo.PosInBagItem].itemTemplateId == bagItemInfo.TemplateId then
                local putInAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("取出"), MakeDelegateFromCSFunction(this.OnTakeOut, Action0, this))
                local actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
                CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), putInAction)
                return actionPairs
            end
        end
    else
        local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, this.SelectedItemId)
        local item = CItemMgr.Inst:GetById(this.SelectedItemId)
        if pos == 0 or item == nil then
            return nil
        end
        local putInAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("取出"), MakeDelegateFromCSFunction(this.OnTakeOut, Action0, this))
        local actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
        CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), putInAction)
        return actionPairs
    end
    return nil
end
CQianKunDaiCompoundView.m_ItemIdIsFakedBagItem_CS2LuaHook = function (this, itemId) 
    if System.String.IsNullOrEmpty(itemId) then
        return false
    end
    return (string.find(itemId, ":", 1, true) ~= nil)
end
