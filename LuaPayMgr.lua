local SdkU3d = import "NtUniSdk.Unity3d.SdkU3d"
local SkuDetailsInfo = import "NtUniSdk.Unity3d.SkuDetailsInfo"
local ConstProp = import "NtUniSdk.Unity3d.ConstProp"
local NtOrderInfo = import "NtUniSdk.Unity3d.NtOrderInfo"
local OrderStatus = import "NtUniSdk.Unity3d.OrderStatus"
local AdvertU3d = import "NtUniSdk.Unity3d.AdvertU3d"
local AdvertConstProp = import "NtUniSdk.Unity3d.AdvertConstProp"
local String = import "System.String"
local SystemInfo = import "UnityEngine.SystemInfo"
local ETickType = import "L10.Engine.ETickType"
local ChannelDefine = import "L10.Game.ChannelDefine"
local Gac2Login = import "L10.Game.Gac2Login"
local Object = import "System.Object"

local CPayMgr = import "L10.Game.CPayMgr"
local ProductInfo = import "L10.Game.CPayMgr+ProductInfo"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local JsonMapper = import "JsonMapper"
local CMiddleNoticeMgr = import "L10.UI.CMiddleNoticeMgr"
local CLogMgr = import "L10.CLogMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local DRPFMgr = import "L10.Game.DRPFMgr"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local UniSdk_ConstProp = import "L10.Game.UniSdk_ConstProp"
local JSON = require "3rdParty/json/JSON"
local NativeTools = import "L10.Engine.NativeTools"

EnumCheckOrderDataType_GAS3 = {
	eRequestCheckOrder = 1,
	eCheckOrderDone = 2,
	eConfirmCheckOrder = 3,
}

g_PcUniSdkPayChannelsPlatform = {
	["netease"] = {ios = false, ad = false},
}

LuaPayMgr = class()

-----------------------------------------------------------------------------------------------
-----------------------------------------pcpay-------------------------------------------------
-----------------------------------------------------------------------------------------------
LuaPayMgr.m_hookTryGetClientPaidOrders = function(this, sessionId, extra)
	if CLoginMgr.Inst:GetAppChannel() == ChannelDefine.APPSTORE then
		this:AppStoreCheckOrdersInQrCodeLogin(sessionId, extra)
	elseif CLoginMgr.Inst:GetAppChannel() == ChannelDefine.HUAWEI then
		this.m_TryGetClientOrdersTimes = 0
		if this.m_TryGetClientOrdersTick == nil then
			this.m_TryGetClientOrdersTick = CTickMgr.Register(DelegateFactory.Action(function()
				this.m_TryGetClientOrdersTimes = this.m_TryGetClientOrdersTimes + 1
				if this.m_TryGetClientOrdersTimes > this.m_TryGetClientOrdersMaxTimes or this.m_CheckedAndRestoredOrders.Count > 0 then
					this:HuaWeiCheckOrdersInQrCodeLogin(sessionId, extra)
					invoke(this.m_TryGetClientOrdersTick)
					this.m_TryGetClientOrdersTick = nil
				end
			end), 1000, ETickType.Loop)
		end
	else
		Gac2Login.RequestQueryOrderForPcInLoginDone(sessionId, extra)
	end
end
CPayMgr.m_hookTryGetClientPaidOrders = LuaPayMgr.m_hookTryGetClientPaidOrders

function LuaPayMgr.SendQueryOrderReceiptDataAndSignInQrCodeLogin(thisReceiptData, thisReceiptSign, sessionId)
	-- receipt_data
	Gac2Login.SendQueryOrderSubReceiptDataBeginInLogin(sessionId)
	local segments = CommonDefs.StringSplit_Segment(thisReceiptData, 200, 250)
	for k = 0, segments.Length - 1 do
		Gac2Login.SendQueryOrderSubReceiptDataInLogin(sessionId, segments[k])
	end
	
	-- receipt_sign
	Gac2Login.SendQueryOrderSubReceiptSignBeginInLogin(sessionId)
	segments = CommonDefs.StringSplit_Segment(thisReceiptSign, 200, 250)
	for k = 0, segments.Length - 1 do
		Gac2Login.SendQueryOrderSubReceiptSignInLogin(sessionId, segments[k])
	end
end

LuaPayMgr.m_hookAppStoreCheckOrdersInQrCodeLogin = function(this, sessionId, extra)
	local channel_name = SdkU3d.getChannel()
	local app_channel = CLoginMgr.Inst:GetAppChannel()
	local platform = SdkU3d.getPlatform()
	local udid = SdkU3d.getUdid()

	local uid = SdkU3d.getPropStr(ConstProp.UID) or "0"
	if uid == "" then uid = "0" end
	local username
	if CommonDefs.IS_KR_CLIENT or CommonDefs.IS_HMT_CLIENT then
		username = uid .. "@" .. channel_name .. ".win.163.com"
	else
		username = uid .. "@" .. platform .. "." .. channel_name .. ".win.163.com"
	end
	local userip = Extensions.GetLocalIp()
	local devname = SystemInfo.deviceModel
	local currency = CPayMgr.Inst.m_Currency

	local orders = SdkU3d.getCheckedOrders()
	if not orders then return end

	for i = 0, orders.Length - 1 do
		local thisOrder = orders[i]
		if thisOrder.orderStatus == OrderStatus.OS_SDK_CHECK_OK then
			if this:CanHandleOrderInQrCodeLogin(thisOrder) then
				local thisReceiptData = thisOrder.transactionReceipt or ""
				local thisReceiptSign = thisOrder.signature or ""

				local thisPid = thisOrder.productId
				local thisPayChannel = thisOrder.orderChannel
				if System.String.IsNullOrEmpty(thisPayChannel) then
					thisPayChannel = app_channel
				end
				local thisGoodsId = thisPid
				local thisGoodsInfo = CPayMgr.Inst:GetGoodsInfoByPid(thisPid)
				local thisGoodsCount = thisOrder.productCount
				local thisSn = thisOrder.orderId

				local thisCurrency = CPayMgr.Inst.m_Currency
				if not System.String.IsNullOrEmpty(thisOrder.currencyForLog) then
					thisCurrency = thisOrder.currencyForLog
				end

				if not CommonDefs.DictContains(CPayMgr.Inst.m_AlreadyCheckedOrders, typeof(String), thisSn) then
					-- receipt_data, receipt_sign
					LuaPayMgr.SendQueryOrderReceiptDataAndSignInQrCodeLogin(thisReceiptData, thisReceiptSign, sessionId)

					-- 透传这个订单的playerid到服务器
					local orderPlayerId = not System.String.IsNullOrEmpty(thisOrder.userData) and thisOrder.userData or "0"
					local ip = userip .. "_" .. orderPlayerId

					-- ip, pid, ch, pf, app_channel, udid, devname, goodsid, goodsinfo, goodscount, currency, sn
					Gac2Login.RequestQueryOrderForPcInLogin(sessionId, tonumber(orderPlayerId), extra, ip, thisPid, thisPayChannel, platform, app_channel, udid, devname, thisGoodsId, thisGoodsInfo, thisGoodsCount, thisCurrency, thisSn)
				end
			end
		end
	end

	Gac2Login.RequestQueryOrderForPcInLoginDone(sessionId, extra)
end
CPayMgr.m_hookAppStoreCheckOrdersInQrCodeLogin = LuaPayMgr.m_hookAppStoreCheckOrdersInQrCodeLogin

LuaPayMgr.m_hookHuaWeiCheckOrdersInQrCodeLogin = function(this, sessionId, extra)
	local channel_name = SdkU3d.getChannel()
	local app_channel = CLoginMgr.Inst:GetAppChannel()
	local platform = SdkU3d.getPlatform()
	local udid = SdkU3d.getUdid()

	local uid = SdkU3d.getPropStr(ConstProp.UID)
	uid = not System.String.IsNullOrEmpty(uid) and uid or "0"
	if uid == "" then uid = "0" end
	local username
	if CommonDefs.IS_KR_CLIENT or CommonDefs.IS_HMT_CLIENT then
		username = uid .. "@" .. channel_name .. ".win.163.com"
	else
		username = uid .. "@" .. platform .. "." .. channel_name .. ".win.163.com"
	end
	local userip = Extensions.GetLocalIp()
	local devname = SystemInfo.deviceModel
	local currency = CPayMgr.Inst.m_Currency

	CommonDefs.DictIterate(CPayMgr.Inst.m_CheckedAndRestoredOrders, DelegateFactory.Action_object_object(function (___key, ___value)
		local thisOrder = ___value
		if thisOrder.orderStatus == OrderStatus.OS_SDK_CHECK_OK or thisOrder.orderStatus == OrderStatus.OS_SDK_CHECKING or thisOrder.orderStatus == OrderStatus.OS_SDK_CHECK_RESTORE_OK then
			if this:CanHandleOrderInQrCodeLogin(thisOrder) then
				local thisReceiptData = thisOrder.transactionReceipt or ""
				local thisReceiptSign = thisOrder.signature or ""

				local thisPid = thisOrder.productId
				local thisPayChannel = thisOrder.orderChannel
				if System.String.IsNullOrEmpty(thisPayChannel) then
					thisPayChannel = app_channel
				end
				local thisGoodsId = thisPid
				local thisGoodsInfo = CPayMgr.Inst:GetGoodsInfoByPid(thisPid)
				local thisGoodsCount = thisOrder.productCount
				local thisSn = thisOrder.orderId

				local thisCurrency = CPayMgr.Inst.m_Currency

				if not CommonDefs.DictContains(CPayMgr.Inst.m_AlreadyCheckedOrders, typeof(String), thisSn) then
					-- receipt_data, receipt_sign
					LuaPayMgr.SendQueryOrderReceiptDataAndSignInQrCodeLogin(thisReceiptData, thisReceiptSign, sessionId)

					-- 透传这个订单的playerid到服务器
					local orderPlayerId = CPayMgr.Inst:HuaWeiGetOrderPlayerId(thisOrder)
					orderPlayerId = not System.String.IsNullOrEmpty(orderPlayerId) and orderPlayerId or "0"
					local ip = userip .. "_" .. orderPlayerId

					-- ip, pid, ch, pf, app_channel, udid, devname, goodsid, goodsinfo, goodscount, currency, sn
					Gac2Login.RequestQueryOrderForPcInLogin(sessionId, tonumber(orderPlayerId), extra, ip, thisPid, thisPayChannel, platform, app_channel, udid, devname, thisGoodsId, thisGoodsInfo, thisGoodsCount, thisCurrency, thisSn)
				end
			end
		end
    end))

	Gac2Login.RequestQueryOrderForPcInLoginDone(sessionId, extra)
end
CPayMgr.m_hookHuaWeiCheckOrdersInQrCodeLogin = LuaPayMgr.m_hookHuaWeiCheckOrdersInQrCodeLogin

-- gas3 pcplatform
LuaPayMgr.m_hookSendPlayerBasicInfoForPcPay_GAS3 = function(this, basic_infoUD)
	if not CSwitchMgr.EnablePcPay then
		return
	end

	local basic_info = TypeAs(MsgPackImpl.unpack(basic_infoUD), typeof(MakeGenericClass(Dictionary, String, Object)))
	if not basic_info then return end
	local playerId = tonumber(CommonDefs.DictGetValue(basic_info, typeof(String), "playerId"))
	local playerName = tostring(CommonDefs.DictGetValue(basic_info, typeof(String), "playerName"))
	local grade = tonumber(CommonDefs.DictGetValue(basic_info, typeof(String), "grade"))
	local playerClass = tonumber(CommonDefs.DictGetValue(basic_info, typeof(String), "class"))
	local equipScore = tonumber(CommonDefs.DictGetValue(basic_info, typeof(String), "equipScore"))
	local vipLevel = tonumber(CommonDefs.DictGetValue(basic_info, typeof(String), "vipLevel"))
	local playerGuildId = tonumber(CommonDefs.DictGetValue(basic_info, typeof(String), "playerGuildId"))
	local createTime = tonumber(CommonDefs.DictGetValue(basic_info, typeof(String), "createTime"))

	if not (playerId and playerName and grade and playerClass and equipScore and vipLevel and playerGuildId and createTime) then
		return
	end

	if CLoginMgr.Inst:UniSdkHasLogin() then
		-- 调用ntUpLoadUserInfo前需要设置玩家的信息，如果缺少某些字段可以不传
		SdkU3d.setPropStr(ConstProp.USERINFO_UID, tostring(playerId)) -- 角色id
		SdkU3d.setPropStr(ConstProp.USERINFO_NAME, playerName) -- 角色昵称
		SdkU3d.setPropStr(ConstProp.USERINFO_GRADE, tostring(grade)) -- 角色等级

		SdkU3d.setPropStr(ConstProp.USERINFO_MENPAI_ID, tostring(playerClass)) -- 职业ID
		SdkU3d.setPropStr(ConstProp.USERINFO_MENPAI_NAME, Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), playerClass))) -- 职业名称
		SdkU3d.setPropStr(ConstProp.USERINFO_CAPABILITY, tostring(equipScore)) -- 战斗力（装备评分）
		SdkU3d.setPropStr(ConstProp.USERINFO_VIP, tostring(vipLevel)) -- 角色vip等级
		SdkU3d.setPropStr(ConstProp.USERINFO_ORG, tostring(playerGuildId)) -- 角色的帮派id

		CLoginMgr.Inst:UniSdkUploadUserInfoCurrentTimeProp(UniSdk_ConstProp.USERINFO_ROLE_CTIME, createTime)

		SdkU3d.ntUpLoadUserInfo()
	end
end
CPayMgr.m_hookSendPlayerBasicInfoForPcPay_GAS3 = LuaPayMgr.m_hookSendPlayerBasicInfoForPcPay_GAS3

LuaPayMgr.m_hookRequestCheckOrderForPc_GAS3 = function(this)
	if not CSwitchMgr.EnablePcPay then
		return
	end
	if not SdkU3d.s_SupportGas3 then
		return
	end

	local pid = this.PcPayPid
	if System.String.IsNullOrEmpty(pid) then
		return
	end
	this:BuyProduct(pid)
end
CPayMgr.m_hookRequestCheckOrderForPc_GAS3 = LuaPayMgr.m_hookRequestCheckOrderForPc_GAS3

LuaPayMgr.m_hookRequestConfirmCheckOrderForPcUniSdk_GAS3 = function(this)
	if not CSwitchMgr.EnablePcPay then
		return
	end
	if not SdkU3d.s_SupportGas3 then
		return
	end

	local pid = this.PcPayPid
	if System.String.IsNullOrEmpty(pid) then
		return
	end
	this:BuyProduct(pid)
end
CPayMgr.m_hookRequestConfirmCheckOrderForPcUniSdk_GAS3 = LuaPayMgr.m_hookRequestConfirmCheckOrderForPcUniSdk_GAS3

-----------------------------------------------------------------------------------------------
------------------------------------------mobile-----------------------------------------------
-----------------------------------------------------------------------------------------------
function LuaPayMgr.BuyDynamicRmbPackage(pid, targetPlayerId, itemId)
	Gac2Gas.SendCheckOrderRmbPackageItemId(itemId)
	CPayMgr.Inst:BuyProduct(pid, targetPlayerId)
end

LuaPayMgr.m_hookBuyProduct = function(this, pid, targetPlayerId)

	if SdkU3d.s_SupportGas3 then
		this:BuyProduct_GAS3(pid, targetPlayerId)
		return
	end

	if CommonDefs.IS_HMT_CLIENT and CommonDefs.Is_PC_PLATFORM() and CommonDefs.Is_UNITY_STANDALONE_WIN() then
		if CPayMgr.s_UseHmtChargeUrl then
			CommonDefs.OpenURL(CPayMgr.s_HmtChargeUrl)
            return
		end
	end
	if CommonDefs.IS_VN_CLIENT and CommonDefs.Is_PC_PLATFORM() and CommonDefs.Is_UNITY_STANDALONE_WIN() then
		if CPayMgr.s_UseVnChargeUrl then
			CommonDefs.OpenURL(CPayMgr.s_VnChargeUrl)
			return
		end
	end

	local isCheckOrderForPc = false
	if CSwitchMgr.EnablePcPay then
		local pcPay_IsCheckOrder = this.IsCheckOrder
		if pcPay_IsCheckOrder then
			isCheckOrderForPc = true
		end
	end

	local tokenMoneyOption = false
	if CSwitchMgr.EnableTokenMoney then
		tokenMoneyOption = true
		local need_IgnoreTokenMoney = this.IgnoreTokenMoney
		if need_IgnoreTokenMoney then
			tokenMoneyOption = false
		end
	end

	local delayRequestCheckOrderAction = DelegateFactory.Action(function()
		if SdkU3d.s_SupportGas3 then
			return
		end
		local channel_name = SdkU3d.getChannel()

		local pay_channel = SdkU3d.getPayChannelByPid(pid)
		local app_channel = CLoginMgr.Inst:GetAppChannel()
		local platform = SdkU3d.getPlatform()
		local udid = SdkU3d.getUdid()

		local uid = SdkU3d.getPropStr(ConstProp.UID) or "0"
		if uid == "" then uid = "0" end
		local username
		if CommonDefs.IS_KR_CLIENT or CommonDefs.IS_HMT_CLIENT then
			username = uid .. "@" .. channel_name .. ".win.163.com"
		else
			username = uid .. "@" .. platform .. "." .. channel_name .. ".win.163.com"
		end
		local userip = Extensions.GetLocalIp()
		local devname = SystemInfo.deviceModel
		local goodsid = pid
		local goodsinfo = this:GetGoodsInfoByPid(pid)
		local goodscount = 1
		local currency = this.m_Currency

		local finalCurrency = currency
		if this:NeedForbidInvalidCurrencies() then
			if not System.String.IsNullOrEmpty(this.m_ClientCurrency) then
				finalCurrency = finalCurrency .. "_" .. this.m_ClientCurrency
			end
		end

		local extra = ""
		local sessionid = SdkU3d.getPropStr(ConstProp.SESSION) or ""
        local sdk_version = SdkU3d.getSDKVersion(SdkU3d.getChannel()) or ""
		if app_channel == ChannelDefine.YIXIN then
			if not System.String.IsNullOrEmpty(sessionid) then
				-- 易信渠道生成订单的时候需要sessionid和sdkVersion
				extra = sessionid .. "__l10__" .. sdk_version
			end
        elseif app_channel == ChannelDefine.QUICK then
			-- quick渠道获取子渠道信息
			local subChannelType = SdkU3d.getPropStr("extra_data")
			if not System.String.IsNullOrEmpty(subChannelType) then
				extra = subChannelType
			end
		elseif app_channel == ChannelDefine.COOLPAD then
			-- coolpad获取子渠道信息
			local subChannelType = CLoginMgr.Inst:GetAppMetadata_PL_GRAPH()
			if not System.String.IsNullOrEmpty(subChannelType) then
				extra = subChannelType
			end
		elseif app_channel == ChannelDefine.KUAIFA then
			-- kuaifa获取子渠道信息
			local subChannelType = CLoginMgr.Inst:GetAppMetadata_PL_GRAPH()
			if not System.String.IsNullOrEmpty(subChannelType) then
				extra = subChannelType
			end
		elseif  app_channel == ChannelDefine.UC or app_channel == ChannelDefine.WANDOUJIA then
			-- uc_platform获取回调地址，用于服务器签名区分
			local payCBUrl = SdkU3d.getPropStr("PAY_CB_URL")
			if not System.String.IsNullOrEmpty(payCBUrl) then
				extra = payCBUrl
			end
		end

		-- ip, pid, ch, pf, app_channel, udid, devname, goodsid, goodsinfo, goodscount, currency, extra
		if isCheckOrderForPc then
			local pcPayTicket = this.PcPayTicket or ""
			Gac2Login.RequestCheckOrderForPcPay(pcPayTicket, userip, pid, pay_channel, platform, app_channel, udid, devname, goodsid, goodsinfo, goodscount, finalCurrency, extra)
		else
			if targetPlayerId > 0 then
				Gac2Gas.RequestCheckOrderAsPresent(targetPlayerId, userip, pid, pay_channel, platform, app_channel, udid, devname, goodsid, goodsinfo, goodscount, finalCurrency, extra, tokenMoneyOption)
			else
				Gac2Gas.RequestCheckOrder(userip, pid, pay_channel, platform, app_channel, udid, devname, goodsid, goodsinfo, goodscount, finalCurrency, extra, tokenMoneyOption)
			end
		end

		-- ios充值前要求一定上传一次玩家的uid
		CLoginMgr.Inst:UniSdkUploadUserInfo()
		if not CommonDefs.IS_HMT_CLIENT then
			SAFE_CALL(function()
				local __try_get_result, productInfo = CommonDefs.DictTryGet(this.m_ProductId2Info, typeof(String), goodsid, typeof(ProductInfo))
				if __try_get_result then
					local jsonStr = SafeStringFormat3("{\"goodsinfo\":\"%s\",\"goodscount\":%d,\"price\":%f,\"currency\":\"%s\"}", goodsinfo, goodscount, productInfo.m_Price * goodscount, currency)
					local param = JsonMapper.ToObject(jsonStr)
					AdvertU3d.trackEvent(AdvertConstProp.AD_SDK_L10_PURCHASE_INITIATED, param)
				end
			end)
		end
	end)

	if this:NeedForbidInvalidCurrencies() then
		this:AddDelayRequestCheckOrderAction(pid, delayRequestCheckOrderAction)
		this:ExtendFuncQuerySkuDetails(pid)
		return
	end

	invoke(delayRequestCheckOrderAction)
end
CPayMgr.m_hookBuyProduct = LuaPayMgr.m_hookBuyProduct

-- gas3
function LuaPayMgr.SendCheckOrderData_GAS3(isCheckOrderForPc, checkOrderData, dataType)
	checkOrderData.dataType = dataType
	local jsonStr = JSON:encode(checkOrderData)

	if isCheckOrderForPc then
		Gac2Login.SendCheckOrderDataForPcPayBegin_GAS3()
	else
		Gac2Gas.SendCheckOrderDataBegin_GAS3()
	end

	local segments = CommonDefs.StringSplit_Segment(jsonStr, 200, 250)
	for k = 0, segments.Length - 1 do
		if isCheckOrderForPc then
			Gac2Login.SendCheckOrderDataForPcPay_GAS3(segments[k])
		else
			Gac2Gas.SendCheckOrderData_GAS3(segments[k])
		end
	end

	if isCheckOrderForPc then
		Gac2Login.SendCheckOrderDataForPcPayEnd_GAS3()
	else
		Gac2Gas.SendCheckOrderDataEnd_GAS3()
	end
end

function LuaPayMgr.CheckCanUsePcUniSdkPay()
	if not (CLoginMgr.IsPcPlatform() and SdkU3d.s_EnablePcUniSdk and CLoginMgr.Inst.UniSdk_LoginDone) then
		return
	end

	local config = g_PcUniSdkPayChannelsPlatform 
	local channel = SdkU3d.getChannel() or ""
	local platform = SdkU3d.getPlatform() or ""

	if not config[channel] then
		return
	end

	if not config[channel][platform] then
		return
	end

	return true
end

LuaPayMgr.m_hookBuyProduct_GAS3 = function(this, pid, targetPlayerId)
	if not SdkU3d.s_SupportGas3 then
		return
	end

	if CommonDefs.IS_HMT_CLIENT and CommonDefs.Is_PC_PLATFORM() and CommonDefs.Is_UNITY_STANDALONE_WIN() then
		if CPayMgr.s_UseHmtChargeUrl then
			CommonDefs.OpenURL(CPayMgr.s_HmtChargeUrl)
			return
		end
	end
	if CommonDefs.IS_VN_CLIENT and CommonDefs.Is_PC_PLATFORM() and CommonDefs.Is_UNITY_STANDALONE_WIN() then
		if CPayMgr.s_UseVnChargeUrl then
			CommonDefs.OpenURL(CPayMgr.s_VnChargeUrl)
			return
		end
	end

	local bUsingPcUniSdk = false
	local bPcUniSdkPayConfirmed = false
	if CLoginMgr.IsPcPlatform() and LuaPayMgr.CheckCanUsePcUniSdkPay() then
		bUsingPcUniSdk = true
		local pcUniSdkPay_IsCheckOrderConfirmed = this.IsCheckOrderConfirmed
		if pcUniSdkPay_IsCheckOrderConfirmed then
			bPcUniSdkPayConfirmed  = true
		end
	end
	local isConfirmCheckOrderForPcUniSdk = false
	if CSwitchMgr.EnablePcPay then
		local pcUniSdkPay_IsConfirmCheckOrder = this.IsConfirmCheckOrder
		if pcUniSdkPay_IsConfirmCheckOrder then
			isConfirmCheckOrderForPcUniSdk  = true
		end
	end

	local isCheckOrderForPc = false
	if CSwitchMgr.EnablePcPay then
		local pcPay_IsCheckOrder = this.IsCheckOrder
		if pcPay_IsCheckOrder then
			isCheckOrderForPc = true
		end
	end

	local tokenMoneyOption = false
	if CSwitchMgr.EnableTokenMoney then
		tokenMoneyOption = true
		local need_IgnoreTokenMoney = this.IgnoreTokenMoney
		if need_IgnoreTokenMoney then
			tokenMoneyOption = false
		end
	end

	local delayRequestCheckOrderAction = DelegateFactory.Action(function()
		if not SdkU3d.s_SupportGas3 then
			return
		end

		local channel_name = SdkU3d.getChannel()

		local pay_channel = SdkU3d.getPayChannelByPid(pid)
		local app_channel = CLoginMgr.Inst:GetAppChannel()
		local platform = SdkU3d.getPlatform()
		local udid = SdkU3d.getUdid()

		local uid = SdkU3d.getPropStr(ConstProp.UID) or "0"
		if uid == "" then uid = "0" end
		local username
		if CommonDefs.IS_KR_CLIENT or CommonDefs.IS_HMT_CLIENT then
			username = uid .. "@" .. channel_name .. ".win.163.com"
		else
			username = uid .. "@" .. platform .. "." .. channel_name .. ".win.163.com"
		end
		local userip = Extensions.GetLocalIp()
		local devname = SystemInfo.deviceModel
		local goodsid = pid
		local goodsinfo = this:GetGoodsInfoByPid(pid)
		local goodscount = 1
		local currency = this.m_Currency
		local clientcurrency = this.m_ClientCurrency

		local extraPayData = {}
		if app_channel == ChannelDefine.QUICK then
			-- quick渠道获取子渠道信息
			local subChannelType = SdkU3d.getPropStr("extra_data")
			if not System.String.IsNullOrEmpty(subChannelType) then
				extraPayData.sub_app_channel = subChannelType
			end
		elseif app_channel == ChannelDefine.COOLPAD then
			-- coolpad渠道获取子渠道信息
			local subChannelType = CLoginMgr.Inst:GetAppMetadata_PL_GRAPH()
			if not System.String.IsNullOrEmpty(subChannelType) then
				extraPayData.sub_app_channel = subChannelType
			end
		elseif app_channel == ChannelDefine.KUAIFA then
			-- kuaifa渠道获取子渠道信息
			local subChannelType = CLoginMgr.Inst:GetAppMetadata_PL_GRAPH()
			if not System.String.IsNullOrEmpty(subChannelType) then
				extraPayData.sub_app_channel = subChannelType
			end
		end

		local pcPayTicket
		if isConfirmCheckOrderForPcUniSdk then
			pcPayTicket = this.PcPayTicket or ""
		elseif isCheckOrderForPc then
			pcPayTicket = this.PcPayTicket or ""
		end

		local checkOrderData = {
			uid = uid,
			ip = userip,
			pid = pid,
			pay_channel = pay_channel,
			platform = platform,
			app_channel = app_channel,
			udid = udid,
			devname = devname,
			goodsid = goodsid,
			goodsinfo = goodsinfo,
			goodscount = goodscount,
			currency = currency,
			clientcurrency = clientcurrency,
			tokenMoneyOption = tokenMoneyOption,
			targetPlayerId = tostring(targetPlayerId or 0),
			extraPayData = extraPayData,
			pcPayTicket = pcPayTicket,
			bUsingPcUniSdk = bUsingPcUniSdk,
			bPcUniSdkPayConfirmed = bPcUniSdkPayConfirmed,
		}

		if isConfirmCheckOrderForPcUniSdk then
			LuaPayMgr.SendCheckOrderData_GAS3(true, checkOrderData, EnumCheckOrderDataType_GAS3.eConfirmCheckOrder)
		elseif isCheckOrderForPc then
			LuaPayMgr.SendCheckOrderData_GAS3(true, checkOrderData, EnumCheckOrderDataType_GAS3.eRequestCheckOrder)
		else
			LuaPayMgr.SendCheckOrderData_GAS3(false, checkOrderData, EnumCheckOrderDataType_GAS3.eRequestCheckOrder)
		end

		-- ios充值前要求一定上传一次玩家的uid
		CLoginMgr.Inst:UniSdkUploadUserInfo()
		if not CommonDefs.IS_HMT_CLIENT then
			SAFE_CALL(function()
				local __try_get_result, productInfo = CommonDefs.DictTryGet(this.m_ProductId2Info, typeof(String), goodsid, typeof(ProductInfo))
				if __try_get_result then
					local jsonStr = SafeStringFormat3("{\"goodsinfo\":\"%s\",\"goodscount\":%d,\"price\":%f,\"currency\":\"%s\"}", goodsinfo, goodscount, productInfo.m_Price * goodscount, currency)
					local param = JsonMapper.ToObject(jsonStr)
					AdvertU3d.trackEvent(AdvertConstProp.AD_SDK_L10_PURCHASE_INITIATED, param)
				end
			end)
		end
	end)

	-- pcunisdk支付确认无需币种限制
	if not isConfirmCheckOrderForPcUniSdk and this:NeedForbidInvalidCurrencies() then
		this:AddDelayRequestCheckOrderAction(pid, delayRequestCheckOrderAction)
		this:ExtendFuncQuerySkuDetails(pid)
		return
	end

	invoke(delayRequestCheckOrderAction)
end
CPayMgr.m_hookBuyProduct_GAS3 = LuaPayMgr.m_hookBuyProduct_GAS3

LuaPayMgr.m_hookCheckOrder = function(this, pid, sn, create_time, extra_data)
	if SdkU3d.s_SupportGas3 then
		return
	end

	-- mumu全渠道不应该走到这里
	if NativeTools.IsAllChannelApk() then
		CLogMgr.Log("[CPayMgr] CheckOrder, AllChannelApkPayLimit")
		return
	end

	-- pc上gas2不应该走到这里
	if CLoginMgr.IsPcPlatform() then
		CLogMgr.Log("[CPayMgr] CheckOrder, PcPayLimit")
		return
	end

	CLogMgr.Log("[CPayMgr] CheckOrder, begin, " .. sn)

	local checkedPlayerId = 0
	if CSwitchMgr.EnablePcPay then
		if this.PcPayPlayerId <= 0 then
			if CClientMainPlayer.Inst == nil then
				return
			end
			checkedPlayerId = CClientMainPlayer.Inst.Id
		else
			checkedPlayerId = this.PcPayPlayerId
		end
	else
		if CClientMainPlayer.Inst == nil then
			return
		end
		checkedPlayerId = CClientMainPlayer.Inst.Id
	end

	if checkedPlayerId <= 0 then
		return
	end

	local order = CreateFromClass(NtOrderInfo) --只能传入在步骤1注册过的商品

	order.orderId = sn
	order.productId = pid
	order.orderDesc = this:GetGoodsInfoByPid(pid)
	if CSwitchMgr.EnableQuickChannelGoodsNameCountConvert and CLoginMgr.Inst:GetAppChannel() == ChannelDefine.QUICK then
		order.orderDesc = this:GetGoodsInfoByPidAndChannel(pid, ChannelDefine.QUICK)
		order.productCount = this:GetGoodsCountByPidAndChannel(pid, ChannelDefine.QUICK)
	end
	order.userData = ToStringWrap(checkedPlayerId)
	order.orderEtc = ""

	if CLoginMgr.Inst:GetAppChannel() == ChannelDefine.GIONEE then
		local ret = create_time

		-- 兼容旧版本
		local login_channel = SdkU3d.getChannel() or ""
		local sdk_version = SdkU3d.getSDKVersion(login_channel) or ""
		local sdk_version_changed = "4.0.6.zn"
		if not System.String.IsNullOrEmpty(create_time) and not System.String.IsNullOrEmpty(sdk_version) and CommonDefs.CompareVersionString(sdk_version, sdk_version_changed) < 0 then
			local json = JsonMapper.ToObject(create_time)
			if json ~= nil then
				local submit_time = ToStringWrap(CommonDefs.IDictGet(json, "submit_time") or "")
				if not System.String.IsNullOrEmpty(submit_time) then
					ret = submit_time
				end
			end
		end

		order.orderEtc = ret
	elseif CLoginMgr.Inst:GetAppChannel() == ChannelDefine.VIVO then
		-- sdk4.5.0.1改用extra_data，旧版本还需用老方式
		local login_channel = SdkU3d.getChannel() or ""
		local sdk_version = SdkU3d.getSDKVersion(login_channel) or ""
		local sdk_version_changed = "4.5.0.1"
		local extra_list = TypeAs(MsgPackImpl.unpack(extra_data), typeof(MakeGenericClass(List, Object)))
		if extra_list ~= nil and extra_list.Count >= 1 and not System.String.IsNullOrEmpty(sdk_version) and CommonDefs.CompareVersionString(sdk_version, sdk_version_changed) >= 0 then
			order.orderEtc = ToStringWrap(extra_list[0])
		else
			order.orderEtc = create_time
		end
		CLogMgr.Log("[CPayMgr] VIVO, set orderEtc, " .. order.orderEtc)
	elseif CLoginMgr.Inst:GetAppChannel() == ChannelDefine.APPSTORE then
		if CommonDefs.IsIOSPlatform() then
			-- app_store要限制币种
			if CSwitchMgr.AppStoreCurrencyLimit and order.arrPriceLocaleId ~= nil and CPayMgr.m_AvailableCurrencies ~= nil then
				for i = 0, CPayMgr.m_AvailableCurrencies.Length - 1 do
					local availableCurrency = CPayMgr.m_AvailableCurrencies[i]
					CommonDefs.ListAdd(order.arrPriceLocaleId, typeof(String), availableCurrency)
				end
			end
		end
	elseif CLoginMgr.Inst:GetAppChannel() == ChannelDefine.YIXIN then
		local extra_list = TypeAs(MsgPackImpl.unpack(extra_data), typeof(MakeGenericClass(List, Object)))
		if extra_list ~= nil and extra_list.Count >= 2 then
			order.sdkOrderId = ToStringWrap(extra_list[0]) -- trade_serialid
			order.orderEtc = ToStringWrap(extra_list[1]) -- pay_url
		end
	elseif CLoginMgr.Inst:GetAppChannel() == ChannelDefine.SOGOU or CLoginMgr.Inst:GetAppChannel() == ChannelDefine.WAN_37 then
		SdkU3d.setPropStr(ConstProp.CURRENCY, LocalString.GetString("灵玉")) -- 游戏内货币名称
	elseif CLoginMgr.Inst:GetAppChannel() == ChannelDefine.UC or CLoginMgr.Inst:GetAppChannel() == ChannelDefine.WANDOUJIA then
		local extra_list = TypeAs(MsgPackImpl.unpack(extra_data), typeof(MakeGenericClass(List, Object)))
		CLogMgr.Log("[CPayMgr] uc_wandoujia")
		if extra_list ~= nil and extra_list.Count >= 1 then
			order.orderEtc = ToStringWrap(extra_list[0]) -- extra sign
			CLogMgr.Log("[CPayMgr] uc_wandoujia, set orderEtc, " .. order.orderEtc)
		end
	elseif CLoginMgr.Inst:GetAppChannel() == "dangle" then
		local extra_list = TypeAs(MsgPackImpl.unpack(extra_data), typeof(MakeGenericClass(List, Object)))
		CLogMgr.Log("[CPayMgr] dangle")
		if extra_list ~= nil and extra_list.Count >= 1 then
			local pay_channel = SdkU3d.getPayChannelByPid(pid)
			local ch = ToStringWrap(extra_list[0])
			if ch ~= pay_channel then
				CLogMgr.Log("[CPayMgr] dangle, " .. ch .. " " .. pay_channel)
				return
			end
			if ch == "dangle" then
				order.orderEtc = ToStringWrap(extra_list[1]) -- extra sign
				CLogMgr.Log("[CPayMgr] dangle, set orderEtc, " .. order.orderEtc)
			elseif ch == "allysdk" then
				CLogMgr.Log("[CPayMgr] dangle, allysdk")
			end
		end
	elseif CLoginMgr.Inst:GetAppChannel() == ChannelDefine.YIWAN then
		local extra_list = TypeAs(MsgPackImpl.unpack(extra_data), typeof(MakeGenericClass(List, Object)))
		CLogMgr.Log("[CPayMgr] iaround")
		if extra_list ~= nil and extra_list.Count >= 1 then
			local pay_channel = SdkU3d.getPayChannelByPid(pid)
			local ch = ToStringWrap(extra_list[0])
			if ch ~= pay_channel then
				CLogMgr.Log("[CPayMgr] iaround, " .. ch .. " " .. pay_channel)
				return
			end
			if ch == "iaround" then
				order.orderEtc = ToStringWrap(extra_list[1]) -- extra sign
				CLogMgr.Log("[CPayMgr] iaround, set orderEtc, " .. order.orderEtc)
			elseif ch == "allysdk" then
				CLogMgr.Log("[CPayMgr] iaround, allysdk")
			end
		end
	elseif CLoginMgr.Inst:GetAppChannel() == ChannelDefine.Bilibili then
		local extra_list = TypeAs(MsgPackImpl.unpack(extra_data), typeof(MakeGenericClass(List, Object)))
		CLogMgr.Log("[CPayMgr] Bilibili")
		if extra_list ~= nil and extra_list.Count >= 1 then
			SdkU3d.setPropStr("GAME_MONEY", "1")
			SdkU3d.setPropStr(ConstProp.USERINFO_NAME, CClientMainPlayer.Inst.BasicProp.Name)
			order.signature = ToStringWrap(extra_list[0])
			CLogMgr.Log("[CPayMgr] Bilibili, set signature, " .. order.signature)
		end
	end

	-- hmt android 官网包,第三方支付STANDALONE=1
	if CommonDefs.IS_HMT_CLIENT and CommonDefs.IsAndroidPlatform() and not CommonDefs.IS_GOOGLEPLAY_CLIENT then
		SdkU3d.setPropStr("STANDALONE", "1")
	end

	CLoginMgr.Inst:StartLoadingWnd(LocalString.GetString("请稍候"))
    this:StartInChargeActionCountdown()
    CLogMgr.Log("[CPayMgr] CheckOrder, before SdkU3d.ntCheckOrder, " .. sn)
    CLogMgr.Log("[CPayMgr] order.orderEtc, " .. order.orderEtc)
    SdkU3d.ntCheckOrder(order)

	CTickMgr.Register(DelegateFactory.Action(function()
		CLoginMgr.Inst:StopLoadingWnd()
	end), 8000, ETickType.Once)
end
CPayMgr.m_hookCheckOrder = LuaPayMgr.m_hookCheckOrder

-- gas3
LuaPayMgr.m_hookCheckOrder_GAS3 = function(this, pid, gamesn, info_dataUD)
	if not SdkU3d.s_SupportGas3 then
		return
	end

	-- mumu全渠道不应该走到这里
	if NativeTools.IsAllChannelApk() then
		local login_channel = SdkU3d.getChannel() or ""
		local platform = SdkU3d.getPlatform() or ""
		CLogMgr.Log("[CPayMgr] CheckOrder_GAS3, AllChannelApkPayLimit, " .. login_channel .. " " .. platform)
		return
	end

	-- pc上没有开启pcunisdk支付不应该走到这里
	if CLoginMgr.IsPcPlatform() and not LuaPayMgr.CheckCanUsePcUniSdkPay() then
		local login_channel = SdkU3d.getChannel() or ""
		local platform = SdkU3d.getPlatform() or ""
		CLogMgr.Log("[CPayMgr] CheckOrder_GAS3, PcUniSdkPayLimit, " .. login_channel .. " " .. platform)
		return
	end

	CLogMgr.Log("[CPayMgr] CheckOrder_GAS3, begin, " .. gamesn)

	local info_data = TypeAs(MsgPackImpl.unpack(info_dataUD), typeof(MakeGenericClass(Dictionary, String, Object)))
	if not info_data then return end

	-- 测试服和正式服必须去计费对应测试和正式环境下单，否则无法发货！
	local gas3url = SdkU3d.getPropStr("UNISDK_JF_GAS3_URL")
	local test_env
	if CommonDefs.DictContains(info_data, typeof(String), "test_env") then
		test_env = CommonDefs.DictGetValue(info_data, typeof(String), "test_env")
	end
	if test_env and test_env == 1 then
		if CommonDefs.IS_VN_CLIENT then
			if gas3url ~= "https://mgbsdksgtest.matrix.easebar.com/l10vn/sdk/" then
				error("Never checkorder with release gas3url in test server!!!")
				return
			end
		else
			if gas3url ~= "https://mgbsdktest.matrix.netease.com/l10/sdk/" then
				error("Never checkorder with release gas3url in test server!!!")
				return
			end
		end
	else
		if CommonDefs.IS_VN_CLIENT then
			if gas3url ~= "https://mgbsdksg.matrix.easebar.com/l10vn/sdk/" then
				error("Never checkorder with test gas3url in release server!!!")
				return
			end
		else
			if gas3url ~= "https://mgbsdk.matrix.netease.com/l10/sdk/" then
				error("Never checkorder with test gas3url in release server!!!")
				return
			end
		end
	end

	local roleid = tonumber(CommonDefs.DictGetValue(info_data, typeof(String), "roleid"))
	local hostid = tonumber(CommonDefs.DictGetValue(info_data, typeof(String), "hostid"))
	local aid = tostring(CommonDefs.DictGetValue(info_data, typeof(String), "aid"))
	local sdkUid = tostring(CommonDefs.DictGetValue(info_data, typeof(String), "sdkUid"))
	local ext_info = tostring(CommonDefs.DictGetValue(info_data, typeof(String), "ext_info"))

	if not (roleid and hostid and aid and sdkUid and ext_info) then return end

	local checkedPlayerId = 0
	if CSwitchMgr.EnablePcPay then
		if this.PcPayPlayerId <= 0 then
			if CClientMainPlayer.Inst == nil then
				return
			end
			checkedPlayerId = CClientMainPlayer.Inst.Id
		else
			checkedPlayerId = this.PcPayPlayerId
		end
	else
		if CClientMainPlayer.Inst == nil then
			return
		end
		checkedPlayerId = CClientMainPlayer.Inst.Id
	end

	if checkedPlayerId <= 0 or checkedPlayerId ~= roleid then
		return
	end

	-- unisdk在拿不到order内的数据时，会尝试用这里设置的全局数据(uid\aid\hostid\roleid\jfextinfo)
	SdkU3d.setPropStr(ConstProp.USERINFO_UID, tostring(roleid))
	SdkU3d.setPropInt(ConstProp.USERINFO_HOSTID, hostid)
	SdkU3d.setPropStr(ConstProp.USERINFO_AID, aid)
	SdkU3d.setPropStr(ConstProp.UID, sdkUid)
	-- 全局透传字段，连续快速下单有可能覆盖，推荐使用order.jfExtInfo，pcunisdk没有order.jfExtInfo
	if CLoginMgr.Inst:GetAppChannel() == ChannelDefine.UC or CLoginMgr.Inst:GetAppChannel() == ChannelDefine.WANDOUJIA then
		-- 九游不能设全局透传参数否则unisdk会塞进callbackInfo，签名会不对
	else
		SdkU3d.setPropStr(ConstProp.UNISDK_EXT_INFO, ext_info)
	end

	if CLoginMgr.Inst:GetAppChannel() == ChannelDefine.Bilibili then
		SdkU3d.setPropStr("GAME_MONEY", "1")
		SdkU3d.setPropStr(ConstProp.USERINFO_NAME, CClientMainPlayer.Inst.BasicProp.Name)
	end

	local order = CreateFromClass(NtOrderInfo)
	order.productId = pid
	order.orderDesc = this:GetGoodsInfoByPid(pid)
	order.productCount = 1
	if CSwitchMgr.EnableQuickChannelGoodsNameCountConvert and CLoginMgr.Inst:GetAppChannel() == ChannelDefine.QUICK then
		order.orderDesc = this:GetGoodsInfoByPidAndChannel(pid, ChannelDefine.QUICK)
		order.productCount = this:GetGoodsCountByPidAndChannel(pid, ChannelDefine.QUICK)
	end
	order.userData = ToStringWrap(checkedPlayerId)
	order.orderEtc = ""

	if CLoginMgr.Inst:GetAppChannel() == ChannelDefine.APPSTORE then
		if CommonDefs.IsIOSPlatform() then
			-- app_store要限制币种
			if CSwitchMgr.AppStoreCurrencyLimit and order.arrPriceLocaleId ~= nil and CPayMgr.m_AvailableCurrencies ~= nil then
				for i = 0, CPayMgr.m_AvailableCurrencies.Length - 1 do
					local availableCurrency = CPayMgr.m_AvailableCurrencies[i]
					CommonDefs.ListAdd(order.arrPriceLocaleId, typeof(String), availableCurrency)
				end
			end

			-- app_store支持计费后台配置限制币种
			if CSwitchMgr.AppStoreJFEnableCheckCurrency then
				SdkU3d.setPropStr("ENABLE_CHECK_CURRENCY", "1")
			end

			-- app_store优先使用缓存下单支付，避免每次去苹果后台请求商品列表
			-- 注意该功能与计费币种限制冲突，币种限制需要每次下单前去苹果后台拿最新的币种回来上报给计费（unisdk那边时序上ENABLE_CHECK_CURRENCY先生效，所以两个都打开相当于没开缓存功能）
			-- 经过对比测试，开优化和不开优化没有明显差别
			if CSwitchMgr.AppStoreUseSKProductCache then
				SdkU3d.setPropInt("USE_SKPRODUCT_CACHE", 1)
			end
		end
	end

	order.userName = tostring(roleid)
	order.serverId = tostring(hostid)
	order.uid = sdkUid
	order.aid = aid
	order.jfExtInfo = ext_info
	
	-- 开启pcunisdk支付，app_store需要额外设置
	if CLoginMgr.IsPcPlatform() and CLoginMgr.Inst:GetAppChannel() == ChannelDefine.APPSTORE then
		SdkU3d.setPropStr("NT_PAY_IOS_CHECKSTAND", "1")
	end

	-- hmt android 官网包,第三方支付STANDALONE=1
	if CommonDefs.IS_HMT_CLIENT and CommonDefs.IsAndroidPlatform() and not CommonDefs.IS_GOOGLEPLAY_CLIENT then
		SdkU3d.setPropStr("STANDALONE", "1")
	end

	CLoginMgr.Inst:StartLoadingWnd(LocalString.GetString("请稍候"))
	this:StartInChargeActionCountdown()
	CLogMgr.Log("[CPayMgr] CheckOrder_GAS3, before SdkU3d.ntCheckOrder, " .. gamesn)
	CLogMgr.Log("[CPayMgr] order.orderEtc, " .. order.orderEtc)
	SdkU3d.ntCheckOrder(order)

	CTickMgr.Register(DelegateFactory.Action(function()
		CLoginMgr.Inst:StopLoadingWnd()
	end), 8000, ETickType.Once)
end
CPayMgr.m_hookCheckOrder_GAS3 = LuaPayMgr.m_hookCheckOrder_GAS3

function LuaPayMgr.SendQueryOrderReceiptDataAndSign(thisReceiptData, thisReceiptSign, isPcPayQueryOrder)
	-- receipt_data
	if isPcPayQueryOrder then
		Gac2Login.SendQueryOrderSubReceiptDataBeginForPc()
	else
		Gac2Gas.SendQueryOrderSubReceiptDataBegin()
	end

	local segments = CommonDefs.StringSplit_Segment(thisReceiptData, 200, 250)
	for k = 0, segments.Length - 1 do
		if isPcPayQueryOrder then
			Gac2Login.SendQueryOrderSubReceiptDataForPc(segments[k])
		else
			Gac2Gas.SendQueryOrderSubReceiptData(segments[k])
		end
	end
	
	-- receipt_sign
	if isPcPayQueryOrder then
		Gac2Login.SendQueryOrderSubReceiptSignBeginForPc()
	else
		Gac2Gas.SendQueryOrderSubReceiptSignBegin()
	end

	segments = CommonDefs.StringSplit_Segment(thisReceiptSign, 200, 250)
	for k = 0, segments.Length - 1 do
		if isPcPayQueryOrder then
			Gac2Login.SendQueryOrderSubReceiptSignForPc(segments[k])
		else
			Gac2Gas.SendQueryOrderSubReceiptSign(segments[k])
		end
	end
end

LuaPayMgr.m_hookAppStoreCheckOrders = function(this, isPcPayQueryOrder)
	local channel_name = SdkU3d.getChannel()
	local app_channel = CLoginMgr.Inst:GetAppChannel()
	local platform = SdkU3d.getPlatform()
	local udid = SdkU3d.getUdid()

	local uid = SdkU3d.getPropStr(ConstProp.UID) or "0"
	if uid == "" then uid = "0" end
	local username
	if CommonDefs.IS_KR_CLIENT or CommonDefs.IS_HMT_CLIENT then
		username = uid .. "@" .. channel_name .. ".win.163.com"
	else
		username = uid .. "@" .. platform .. "." .. channel_name .. ".win.163.com"
	end
	local userip = Extensions.GetLocalIp()
	local devname = SystemInfo.deviceModel
	local currency = this.m_Currency
	
	local allOrdersFail = true
	local orders = SdkU3d.getCheckedOrders()
	if not orders then return end

	for i = 0, orders.Length - 1 do
		local thisOrder = orders[i]
		if thisOrder.orderStatus == OrderStatus.OS_SDK_CHECK_OK then
			if this:CanHandleOrder(thisOrder, isPcPayQueryOrder) then
				allOrdersFail = false

				local thisReceiptData = thisOrder.transactionReceipt or ""
				local thisReceiptSign = thisOrder.signature or ""

				local thisPid = thisOrder.productId
				local thisPayChannel = thisOrder.orderChannel
				if System.String.IsNullOrEmpty(thisPayChannel) then
					thisPayChannel = app_channel
				end
				local thisGoodsId = thisPid
				local thisGoodsInfo = CPayMgr.Inst:GetGoodsInfoByPid(thisPid)
				local thisGoodsCount = thisOrder.productCount
				local thisSn = thisOrder.orderId

				local thisCurrency = this.m_Currency
				if not System.String.IsNullOrEmpty(thisOrder.currencyForLog) then
					thisCurrency = thisOrder.currencyForLog
				end

				local delayQueryOrder = DelegateFactory.Action(function()
					if not this:CanHandleOrder(thisOrder, isPcPayQueryOrder) then
						return
					end
					if CommonDefs.DictContains(this.m_AlreadyCheckedOrders, typeof(String), thisSn) then
						return
					end
					if this:HandlePromoCodeOrder(thisOrder, isPcPayQueryOrder) then
						return
					end
					-- receipt_data, receipt_sign
					LuaPayMgr.SendQueryOrderReceiptDataAndSign(thisReceiptData, thisReceiptSign, isPcPayQueryOrder)

					-- 透传这个订单的playerid到服务器
					local orderPlayerId = not System.String.IsNullOrEmpty(thisOrder.userData) and thisOrder.userData or ""
					local ip = userip .. "_" .. orderPlayerId

					-- ip, pid, ch, pf, app_channel, udid, devname, goodsid, goodsinfo, goodscount, currency, sn
					if isPcPayQueryOrder then
						Gac2Login.RequestQueryOrderForPc(ip, thisPid, thisPayChannel, platform, app_channel, udid, devname, thisGoodsId, thisGoodsInfo, thisGoodsCount, thisCurrency, thisSn)
					else
						Gac2Gas.RequestQueryOrder(ip, thisPid, thisPayChannel, platform, app_channel, udid, devname, thisGoodsId, thisGoodsInfo, thisGoodsCount, thisCurrency, thisSn)
					end
				end)
				invoke(delayQueryOrder)
				this:AddDelayQueryOrderAction(delayQueryOrder)
			else
				-- 有回调但无法轮询，上报日志给服务器，会重复打日志，但只有少数玩家会遇到，用于补单参考
				-- unisdk在匹配不到订单时会新生成一个假订单，userData为空，打到该日志说明订单匹配完成，不会重新匹配其他订单号导致重复发货，可以补单
				-- 另一种情况是同设备回调其他角色的订单，玩家可能搞错角色，可以用这个日志检查订单实际角色
				SAFE_CALL(function()
					local receipt = thisOrder.transactionReceipt
					local unique_identifier = ""
					local transaction_id = ""
					if not System.String.IsNullOrEmpty(receipt) and not this:IsJFGas3Order(thisOrder) then
						local receiptDict = TypeAs(this:ParseAppStoreReceiptNoRecurive(receipt), typeof(MakeGenericClass(Dictionary, String, Object)))
						local paramName = "purchase-info"
						if receiptDict ~= nil and CommonDefs.DictContains(receiptDict, typeof(String), paramName) then
							local purchase_info = TypeAs(CommonDefs.DictGetValue(receiptDict, typeof(String), paramName), typeof(String))
							local infoDict = TypeAs(this:ParseAppStoreReceiptNoRecurive(purchase_info), typeof(MakeGenericClass(Dictionary, String, Object)))
							paramName = "unique-identifier"
							unique_identifier = infoDict and CommonDefs.DictContains(infoDict, typeof(String), paramName) and CommonDefs.DictGetValue(infoDict, typeof(String), paramName) or ""
							paramName = "transaction-id"
							transaction_id = infoDict and CommonDefs.DictContains(infoDict, typeof(String), paramName) and CommonDefs.DictGetValue(infoDict, typeof(String), paramName) or ""
						end
					end
					local orderPlayerId = not System.String.IsNullOrEmpty(thisOrder.userData) and thisOrder.userData or ""
					local thisPid = thisOrder.productId
					local thisSn = thisOrder.orderId
					local reason = "other_orderPlayerId"
					if orderPlayerId == "" then
						reason = "empty_orderPlayerId"
					elseif  orderPlayerId == "0" then
						reason = "zero_orderPlayerId"
					end
					if this:IsJFGas3Order(thisOrder) then
						reason = "gas3_order"
					end

					if isPcPayQueryOrder then
						local pcPayTicket = CPayMgr.Inst.PcPayTicket or ""
						Gac2Login.LogCanNotHandleOrderInfo(pcPayTicket, orderPlayerId, thisPid, thisSn, reason, unique_identifier, transaction_id)
					else
						Gac2Gas.LogCanNotHandleOrderInfo(orderPlayerId, thisPid, thisSn, reason, unique_identifier, transaction_id)
					end
				end)
			end
		end
	end

	if CSwitchMgr.EnablePcPay and isPcPayQueryOrder and allOrdersFail then
        CLoginMgr.Inst:Disconnect()
	end
end
CPayMgr.m_hookAppStoreCheckOrders = LuaPayMgr.m_hookAppStoreCheckOrders

LuaPayMgr.m_hookGooglePlayCheckOrders = function(this, isPcPayQueryOrder)
	local channel_name = SdkU3d.getChannel()
	local app_channel = CLoginMgr.Inst:GetAppChannel()
	local platform = SdkU3d.getPlatform()
	local udid = SdkU3d.getUdid()

	local uid = SdkU3d.getPropStr(ConstProp.UID) or "0"
	if uid == "" then uid = "0" end
	local username
	if CommonDefs.IS_KR_CLIENT or CommonDefs.IS_HMT_CLIENT then
		username = uid .. "@" .. channel_name .. ".win.163.com"
	else
		username = uid .. "@" .. platform .. "." .. channel_name .. ".win.163.com"
	end
	local userip = Extensions.GetLocalIp()
	local devname = SystemInfo.deviceModel
	local currency = this.m_Currency

	local allOrdersFail = true
	CommonDefs.DictIterate(this.m_CheckedAndRestoredOrders, DelegateFactory.Action_object_object(function (___key, ___value)
		local thisOrder = ___value
		if (thisOrder.orderStatus == OrderStatus.OS_SDK_CHECK_OK or thisOrder.orderStatus == OrderStatus.OS_SDK_CHECK_RESTORE_OK) and this:CanHandleOrder(thisOrder, isPcPayQueryOrder) then
			allOrdersFail = false

			local thisReceiptData = CommonDefs.Convert_ToBase64String(CommonDefs.UTF8Encoding_GetBytes(thisOrder.orderEtc or ""))
            local thisReceiptSign = thisOrder.signature or ""

			local thisPid = thisOrder.productId
			local thisPayChannel = thisOrder.orderChannel
			if System.String.IsNullOrEmpty(thisPayChannel) then
				thisPayChannel = app_channel
			end
			local thisGoodsId = thisPid
			local thisGoodsInfo = CPayMgr.Inst:GetGoodsInfoByPid(thisPid)
			local thisGoodsCount = thisOrder.productCount
			local thisSn = thisOrder.orderId

			local thisCurrency = this.m_Currency
			if CPayMgr.s_bGooglePlayUseCurrencyForLog then
				if not System.String.IsNullOrEmpty(thisOrder.currencyForLog) then
					thisCurrency = thisOrder.currencyForLog
				end
			else
				local skuInfo = SkuDetailsInfo.getSkuDetail(thisOrder.productId)
				if skuInfo and not System.String.IsNullOrEmpty(skuInfo.priceCurrencyCode) then
					thisCurrency = skuInfo.priceCurrencyCode
				end
			end

			local delayQueryOrder = DelegateFactory.Action(function()
					if not this:CanHandleOrder(thisOrder, isPcPayQueryOrder) then
						return
					end
					if CommonDefs.DictContains(this.m_AlreadyCheckedOrders, typeof(String), thisSn) then
						return
					end
					if this:HandlePromoCodeOrder(thisOrder, isPcPayQueryOrder) then
						return
					end
					-- receipt_data, receipt_sign
					LuaPayMgr.SendQueryOrderReceiptDataAndSign(thisReceiptData, thisReceiptSign, isPcPayQueryOrder)

					-- 透传这个订单的playerid到服务器
					local orderPlayerId = this:GooglePlayGetOrderPlayerId(thisOrder)
					local ip = userip .. "_" .. orderPlayerId

					-- ip, pid, ch, pf, app_channel, udid, devname, goodsid, goodsinfo, goodscount, currency, sn
					if isPcPayQueryOrder then
						Gac2Login.RequestQueryOrderForPc(ip, thisPid, thisPayChannel, platform, app_channel, udid, devname, thisGoodsId, thisGoodsInfo, thisGoodsCount, thisCurrency, thisSn)
					else
						Gac2Gas.RequestQueryOrder(ip, thisPid, thisPayChannel, platform, app_channel, udid, devname, thisGoodsId, thisGoodsInfo, thisGoodsCount, thisCurrency, thisSn)
					end
				end)
				invoke(delayQueryOrder)
				this:AddDelayQueryOrderAction(delayQueryOrder)
		end
	end))

	if CSwitchMgr.EnablePcPay and isPcPayQueryOrder and allOrdersFail then
        CLoginMgr.Inst:Disconnect()
	end
end
CPayMgr.m_hookGooglePlayCheckOrders = LuaPayMgr.m_hookGooglePlayCheckOrders

LuaPayMgr.m_hookHuaWeiCheckOrders = function(this, isPcPayQueryOrder)
	local channel_name = SdkU3d.getChannel()
	local app_channel = CLoginMgr.Inst:GetAppChannel()
	local platform = SdkU3d.getPlatform()
	local udid = SdkU3d.getUdid()

	local uid = SdkU3d.getPropStr(ConstProp.UID) or "0"
	if uid == "" then uid = "0" end
	local username
	if CommonDefs.IS_KR_CLIENT or CommonDefs.IS_HMT_CLIENT then
		username = uid .. "@" .. channel_name .. ".win.163.com"
	else
		username = uid .. "@" .. platform .. "." .. channel_name .. ".win.163.com"
	end
	local userip = Extensions.GetLocalIp()
	local devname = SystemInfo.deviceModel
	local currency = this.m_Currency

	local allOrdersFail = true

	CommonDefs.DictIterate(this.m_CheckedAndRestoredOrders, DelegateFactory.Action_object_object(function (___key, ___value)
		local thisOrder = ___value
		if (thisOrder.orderStatus == OrderStatus.OS_SDK_CHECK_OK or thisOrder.orderStatus == OrderStatus.OS_SDK_CHECKING or thisOrder.orderStatus == OrderStatus.OS_SDK_CHECK_RESTORE_OK) and this:CanHandleOrder(thisOrder, isPcPayQueryOrder) then
			allOrdersFail = false
			
			local thisReceiptData = thisOrder.transactionReceipt or ""
			local thisReceiptSign = thisOrder.signature or ""
			
			local thisPid = thisOrder.productId
			local thisPayChannel = thisOrder.orderChannel
			if System.String.IsNullOrEmpty(thisPayChannel) then
				thisPayChannel = app_channel
			end
			local thisGoodsId = thisPid
			local thisGoodsInfo = CPayMgr.Inst:GetGoodsInfoByPid(thisPid)
			local thisGoodsCount = thisOrder.productCount
			local thisSn = thisOrder.orderId

			local thisCurrency = this.m_Currency
			
			local delayQueryOrder = DelegateFactory.Action(function()
				if not this:CanHandleOrder(thisOrder, isPcPayQueryOrder) then
					return
				end
				if CommonDefs.DictContains(this.m_AlreadyCheckedOrders, typeof(String), thisSn) then
					return
				end
				if this:HandlePromoCodeOrder(thisOrder, isPcPayQueryOrder) then
					return
				end
				-- receipt_data, receipt_sign
				LuaPayMgr.SendQueryOrderReceiptDataAndSign(thisReceiptData, thisReceiptSign, isPcPayQueryOrder)
				
				-- 透传这个订单的playerid到服务器
				local orderPlayerId = this:HuaWeiGetOrderPlayerId(thisOrder)
				local ip = userip .. "_" .. orderPlayerId
			
				-- ip, pid, ch, pf, app_channel, udid, devname, goodsid, goodsinfo, goodscount, currency, sn
				if isPcPayQueryOrder then
					Gac2Login.RequestQueryOrderForPc(ip, thisPid, thisPayChannel, platform, app_channel, udid, devname, thisGoodsId, thisGoodsInfo, thisGoodsCount, thisCurrency, thisSn)
				else
					Gac2Gas.RequestQueryOrder(ip, thisPid, thisPayChannel, platform, app_channel, udid, devname, thisGoodsId, thisGoodsInfo, thisGoodsCount, thisCurrency, thisSn)
				end
			end)
			invoke(delayQueryOrder)
			this:AddDelayQueryOrderAction(delayQueryOrder)
		end
	end))

	if CSwitchMgr.EnablePcPay and isPcPayQueryOrder and allOrdersFail then
        CLoginMgr.Inst:Disconnect()
	end
end
CPayMgr.m_hookHuaWeiCheckOrders = LuaPayMgr.m_hookHuaWeiCheckOrders

LuaPayMgr.m_hookOnCheckOrderDone = function(this, order)
	if SdkU3d.s_SupportGas3 then
		this:OnCheckOrderDone_GAS3(order)
		return
	end

	-- gas3订单不要轮询
	local isGas3Order = this:IsJFGas3Order(order)

	local isPcPayQueryOrder = false
	if CSwitchMgr.EnablePcPay then
		isPcPayQueryOrder = this.IsQueryOrder
	end

	local channel_name = SdkU3d.getChannel()

	local pid = order.productId
	local pay_channel = order.orderChannel
	local app_channel = CLoginMgr.Inst:GetAppChannel()
	local platform = SdkU3d.getPlatform()
	local udid = SdkU3d.getUdid()

	local uid = SdkU3d.getPropStr(ConstProp.UID) or "0"
	if uid == "" then uid = "0" end
	local username
	if CommonDefs.IS_KR_CLIENT or CommonDefs.IS_HMT_CLIENT then
		username = uid .. "@" .. channel_name .. ".win.163.com"
	else
		username = uid .. "@" .. platform .. "." .. channel_name .. ".win.163.com"
	end
	local userip = Extensions.GetLocalIp()
	local devname = SystemInfo.deviceModel
	local goodsid = pid
	local goodsinfo = this:GetGoodsInfoByPid(pid)
	local goodscount = order.productCount
	local currency = this.m_Currency

	local sn = order.orderId
	local ext_info = order.jfExtInfo
	if System.String.IsNullOrEmpty(ext_info) and CLoginMgr.IsPcPlatform() then
		ext_info = SdkU3d.getPropStr(ConstProp.UNISDK_EXT_INFO)
	end

	SAFE_CALL(function() this:ShowOrderResult(order, isPcPayQueryOrder) end)

	local isClientChargeSuccess = false
	if SdkU3d.getPayChannel() == ChannelDefine.GOOGLE then
		if order.orderStatus == OrderStatus.OS_SDK_CHECK_OK or order.orderStatus == OrderStatus.OS_SDK_CHECK_RESTORE_OK then
			isClientChargeSuccess = true
		end
	elseif pay_channel == ChannelDefine.HUAWEI then
		if order.orderStatus == OrderStatus.OS_SDK_CHECK_OK or order.orderStatus == OrderStatus.OS_SDK_CHECKING or order.orderStatus == OrderStatus.OS_SDK_CHECK_RESTORE_OK then
			isClientChargeSuccess = true
		end
	else
		if order.orderStatus == OrderStatus.OS_SDK_CHECK_OK or order.orderStatus == OrderStatus.OS_SDK_CHECKING then
			isClientChargeSuccess = true
		end
	end
	if not isClientChargeSuccess and not isPcPayQueryOrder then
		-- gas3用gamesn上报
		if isGas3Order and not System.String.IsNullOrEmpty(ext_info) then
			SAFE_CALL(function()
				local ext_data = JsonMapper.ToObject(ext_info)
				if ext_data ~= nil then
					local gamesn = ToStringWrap(CommonDefs.IDictGet(ext_data, "gamesn") or "")
					Gac2Gas.NotifyFailedOrderInfo(pid, gamesn, order.orderErrReason)
				end
			end)
		else
			Gac2Gas.NotifyFailedOrderInfo(pid, sn, order.orderErrReason)
		end
	end

	-- app_store, google_play, huawei
    local receipt_data = order.transactionReceipt or ""
    local receipt_sign = order.signature or ""

	if System.String.IsNullOrEmpty(pay_channel) then
		pay_channel = app_channel
	end

	if this:IsPromoCodeGoodsByPid(pid) or pay_channel == ChannelDefine.APPSTORE then
		this:AppStoreCheckOrders(isPcPayQueryOrder)
		CLoginMgr.Inst:StopLoadingWnd()
        this:StopInChargeActionCountdown()
        return
	elseif SdkU3d.getPayChannel() == ChannelDefine.GOOGLE then
		if isClientChargeSuccess and not isGas3Order then
			if not CommonDefs.DictContains(this.m_CheckedAndRestoredOrders, typeof(String), order.orderId) then
				CommonDefs.DictAdd(this.m_CheckedAndRestoredOrders, typeof(String), order.orderId, typeof(NtOrderInfo), order)
			end
		end
		this:GooglePlayCheckOrders(isPcPayQueryOrder)
		CLoginMgr.Inst:StopLoadingWnd()
		this:StopInChargeActionCountdown()
		return
	elseif pay_channel == ChannelDefine.HUAWEI then
		if isClientChargeSuccess and not isGas3Order then
			if not CommonDefs.DictContains(this.m_CheckedAndRestoredOrders, typeof(String), order.orderId) then
				CommonDefs.DictAdd(this.m_CheckedAndRestoredOrders, typeof(String), order.orderId, typeof(NtOrderInfo), order)
			end
		end
		this:HuaWeiCheckOrders(isPcPayQueryOrder)
		CLoginMgr.Inst:StopLoadingWnd()
		this:StopInChargeActionCountdown()
		return
	end

	if isClientChargeSuccess then
		if not isGas3Order then
			local delayQueryOrder = DelegateFactory.Action(function()
				if CommonDefs.DictContains(this.m_AlreadyCheckedOrders, typeof(String), sn) then
					return
				end
				-- receipt_data, receipt_sign
				LuaPayMgr.SendQueryOrderReceiptDataAndSign(receipt_data, receipt_sign, isPcPayQueryOrder)

				-- 透传这个订单的playerid到服务器
				local orderPlayerId = not System.String.IsNullOrEmpty(order.userData) and order.userData or ""
				local ip = userip .. "_" .. orderPlayerId

				-- ip, pid, ch, pf, app_channel, udid, devname, goodsid, goodsinfo, goodscount, currency, sn
				if isPcPayQueryOrder then
					Gac2Login.RequestQueryOrderForPc(ip, pid, pay_channel, platform, app_channel, udid, devname, goodsid, goodsinfo, goodscount, currency, sn)
				else
					Gac2Gas.RequestQueryOrder(ip, pid, pay_channel, platform, app_channel, udid, devname, goodsid, goodsinfo, goodscount, currency, sn)
				end

				-- 上传用户信息
				CLoginMgr.Inst:OnPlayerPaySuccess()
			end)
			invoke(delayQueryOrder)
			this:AddDelayQueryOrderAction(delayQueryOrder)
		end
	else
		-- 充值失败的情况，要告诉服务器
		if CSwitchMgr.EnablePcPay and isPcPayQueryOrder then
			CLoginMgr.Inst:Disconnect()
		end
	end

	CLoginMgr.Inst:StopLoadingWnd()
	this:StopInChargeActionCountdown()
end
CPayMgr.m_hookOnCheckOrderDone = LuaPayMgr.m_hookOnCheckOrderDone

LuaPayMgr.m_hookOnCheckOrderDone_GAS3 = function(this, order)

	-- gas3订单不要轮询
	local isGas3Order = this:IsJFGas3Order(order)

	local isPcPayQueryOrder = false
	if CSwitchMgr.EnablePcPay then
		isPcPayQueryOrder = this.IsQueryOrder
	end

	local channel_name = SdkU3d.getChannel()

	local pid = order.productId
	local pay_channel = order.orderChannel
	local app_channel = CLoginMgr.Inst:GetAppChannel()
	local platform = SdkU3d.getPlatform()
	local udid = SdkU3d.getUdid()

	local uid = SdkU3d.getPropStr(ConstProp.UID) or "0"
	if uid == "" then uid = "0" end
	local username
	if CommonDefs.IS_KR_CLIENT or CommonDefs.IS_HMT_CLIENT then
		username = uid .. "@" .. channel_name .. ".win.163.com"
	else
		username = uid .. "@" .. platform .. "." .. channel_name .. ".win.163.com"
	end
	local userip = Extensions.GetLocalIp()
	local devname = SystemInfo.deviceModel
	local goodsid = pid
	local goodsinfo = this:GetGoodsInfoByPid(pid)
	local goodscount = order.productCount
	local currency = this.m_Currency

	local sn = order.orderId
	local ext_info = order.jfExtInfo
	if System.String.IsNullOrEmpty(ext_info) and CLoginMgr.IsPcPlatform() then
		ext_info = SdkU3d.getPropStr(ConstProp.UNISDK_EXT_INFO)
	end

	SAFE_CALL(function() this:ShowOrderResult(order, isPcPayQueryOrder) end)

	local isClientChargeSuccess = false
	if SdkU3d.getPayChannel() == ChannelDefine.GOOGLE then
		if order.orderStatus == OrderStatus.OS_SDK_CHECK_OK or order.orderStatus == OrderStatus.OS_SDK_CHECK_RESTORE_OK then
			isClientChargeSuccess = true
		end
	elseif pay_channel == ChannelDefine.HUAWEI then
		if order.orderStatus == OrderStatus.OS_SDK_CHECK_OK or order.orderStatus == OrderStatus.OS_SDK_CHECKING or order.orderStatus == OrderStatus.OS_SDK_CHECK_RESTORE_OK then
			isClientChargeSuccess = true
		end
	else
		if order.orderStatus == OrderStatus.OS_SDK_CHECK_OK or order.orderStatus == OrderStatus.OS_SDK_CHECKING then
			isClientChargeSuccess = true
		end
	end

	if isClientChargeSuccess then
		-- gas3 上报 checkorderdone
		if isGas3Order and this:CanHandleGas3Order(order, isPcPayQueryOrder) then
			local checkOrderDoneData = {
				uid = uid,
				ip = userip,
				pid = pid,
				pay_channel = pay_channel,
				platform = platform,
				app_channel = app_channel,
				udid = udid,
				devname = devname,
				goodsid = goodsid,
				goodsinfo = goodsinfo,
				goodscount = goodscount,
				currency = currency,
				sn = sn,
				jfCode = order.jfCode,
				jfSubCode = order.jfSubCode,
				jfMessage = order.jfMessage,
				jfExtInfo = ext_info,
				orderStatus = EnumToInt(order.orderStatus),
				isPcPay = isPcPayQueryOrder,
			}
			LuaPayMgr.SendCheckOrderData_GAS3(isPcPayQueryOrder, checkOrderDoneData, EnumCheckOrderDataType_GAS3.eCheckOrderDone)
		end
	end

	if not isClientChargeSuccess and not isPcPayQueryOrder then
		-- gas3用gamesn上报
		if isGas3Order and not System.String.IsNullOrEmpty(ext_info) then
			SAFE_CALL(function()
				local ext_data = JsonMapper.ToObject(ext_info)
				if ext_data ~= nil then
					local gamesn = ToStringWrap(CommonDefs.IDictGet(ext_data, "gamesn") or "")
					Gac2Gas.NotifyFailedOrderInfo(pid, gamesn, order.orderErrReason)
				end
			end)
		else
			Gac2Gas.NotifyFailedOrderInfo(pid, sn, order.orderErrReason)
		end
	end

	-- app_store, google_play, huawei
	local receipt_data = order.transactionReceipt or ""
	local receipt_sign = order.signature or ""

	if System.String.IsNullOrEmpty(pay_channel) then
		pay_channel = app_channel
	end

	if this:IsPromoCodeGoodsByPid(pid) or pay_channel == ChannelDefine.APPSTORE then
		this:AppStoreCheckOrders(isPcPayQueryOrder)
		CLoginMgr.Inst:StopLoadingWnd()
		this:StopInChargeActionCountdown()
		return
	elseif SdkU3d.getPayChannel() == ChannelDefine.GOOGLE then
		if isClientChargeSuccess and not isGas3Order then
			if not CommonDefs.DictContains(this.m_CheckedAndRestoredOrders, typeof(String), order.orderId) then
				CommonDefs.DictAdd(this.m_CheckedAndRestoredOrders, typeof(String), order.orderId, typeof(NtOrderInfo), order)
			end
		end
		this:GooglePlayCheckOrders(isPcPayQueryOrder)
		CLoginMgr.Inst:StopLoadingWnd()
		this:StopInChargeActionCountdown()
		return
	elseif pay_channel == ChannelDefine.HUAWEI then
		if isClientChargeSuccess and not isGas3Order then
			if not CommonDefs.DictContains(this.m_CheckedAndRestoredOrders, typeof(String), order.orderId) then
				CommonDefs.DictAdd(this.m_CheckedAndRestoredOrders, typeof(String), order.orderId, typeof(NtOrderInfo), order)
			end
		end
		this:HuaWeiCheckOrders(isPcPayQueryOrder)
		CLoginMgr.Inst:StopLoadingWnd()
		this:StopInChargeActionCountdown()
		return
	end

	if isClientChargeSuccess then
		if not isGas3Order then
			local delayQueryOrder = DelegateFactory.Action(function()
				if CommonDefs.DictContains(this.m_AlreadyCheckedOrders, typeof(String), sn) then
					return
				end
				-- receipt_data, receipt_sign
				LuaPayMgr.SendQueryOrderReceiptDataAndSign(receipt_data, receipt_sign, isPcPayQueryOrder)

				-- 透传这个订单的playerid到服务器
				local orderPlayerId = not System.String.IsNullOrEmpty(order.userData) and order.userData or ""
				local ip = userip .. "_" .. orderPlayerId

				-- ip, pid, ch, pf, app_channel, udid, devname, goodsid, goodsinfo, goodscount, currency, sn
				if isPcPayQueryOrder then
					Gac2Login.RequestQueryOrderForPc(ip, pid, pay_channel, platform, app_channel, udid, devname, goodsid, goodsinfo, goodscount, currency, sn)
				else
					Gac2Gas.RequestQueryOrder(ip, pid, pay_channel, platform, app_channel, udid, devname, goodsid, goodsinfo, goodscount, currency, sn)
				end

				-- 上传用户信息
				CLoginMgr.Inst:OnPlayerPaySuccess()
			end)
			invoke(delayQueryOrder)
			this:AddDelayQueryOrderAction(delayQueryOrder)
		end
	else
		-- 充值失败的情况，要告诉服务器
		if CSwitchMgr.EnablePcPay and isPcPayQueryOrder then
			CLoginMgr.Inst:Disconnect()
		end
	end

	CLoginMgr.Inst:StopLoadingWnd()
	this:StopInChargeActionCountdown()
end
CPayMgr.m_hookOnCheckOrderDone_GAS3 = LuaPayMgr.m_hookOnCheckOrderDone_GAS3

LuaPayMgr.m_hookShowOrderResult = function(this, order, isPcPayQuery)
	this:UpdateDebugInfo(order)

    -- Google Play restore 的订单不显示内容
	if SdkU3d.getPayChannel() == ChannelDefine.GOOGLE and (order.orderStatus == OrderStatus.OS_SDK_CHECK_OK or order.orderStatus == OrderStatus.OS_SDK_CHECK_RESTORE_OK) then
		return
	end
	if order.orderChannel == ChannelDefine.HUAWEI and (order.orderStatus == OrderStatus.OS_SDK_CHECKING or order.orderStatus == OrderStatus.OS_SDK_CHECK_OK or order.orderStatus == OrderStatus.OS_SDK_CHECK_RESTORE_OK) then
		return
	end

	-- 正确支付的订单不显示内容
	if order.orderStatus == OrderStatus.OS_SDK_CHECK_OK then
		return
	end

	-- 只有app_store显示支付失败原因
	local errReason = order.orderErrReason or ""
	if not CommonDefs.IS_HMT_CLIENT then
		SAFE_CALL(function()
			local __try_get_result, productInfo = CommonDefs.DictTryGet(this.m_ProductId2Info, typeof(String), order.productId, typeof(ProductInfo))
			if __try_get_result then
				local jsonStr = SafeStringFormat3("{\"goodsinfo\":\"%s\",\"goodscount\":%d,\"price\":%f,\"currency\":\"%s\",\"reason\":\"%s\"}", productInfo.m_GoodsInfo, order.productCount, order.productCurrentPrice * order.productCount, order.currencyForLog, errReason)
				local param = JsonMapper.ToObject(jsonStr)
				AdvertU3d.trackEvent(AdvertConstProp.AD_SDK_L10_PURCHASE_CANCELLED, param)
			end
		end)
	end

	SAFE_CALL(function()
		-- 订单支付失败的rpc
		local receipt = order.transactionReceipt
		local isSend = false
		-- gas3订单升级到ios7收据，不再解析(计费有订单失败的日志)
		if CLoginMgr.Inst:GetAppChannel() == ChannelDefine.APPSTORE and CommonDefs.IsIOSPlatform() and not System.String.IsNullOrEmpty(receipt) and not this:IsJFGas3Order(order) then
			local receiptDict = TypeAs(this:ParseAppStoreReceiptNoRecurive(receipt), typeof(MakeGenericClass(Dictionary, String, Object)))
			local paramName = "purchase-info"
			if receiptDict ~= nil and CommonDefs.DictContains(receiptDict, typeof(String), paramName) then
				local purchase_info = TypeAs(CommonDefs.DictGetValue(receiptDict, typeof(String), paramName), typeof(String))
				local infoDict = TypeAs(this:ParseAppStoreReceiptNoRecurive(purchase_info), typeof(MakeGenericClass(Dictionary, String, Object)))
				paramName = "unique-identifier"
				local unique_identifier = infoDict and CommonDefs.DictContains(infoDict, typeof(String), paramName) and CommonDefs.DictGetValue(infoDict, typeof(String), paramName) or ""
				paramName = "transaction-id"
				local transaction_id = infoDict and CommonDefs.DictContains(infoDict, typeof(String), paramName) and CommonDefs.DictGetValue(infoDict, typeof(String), paramName) or ""
				if CSwitchMgr.EnablePcPay and isPcPayQuery then
					local pcPayTicket = CPayMgr.Inst.PcPayTicket or ""
					Gac2Login.LogFailedOrderInfo(pcPayTicket, order.productId, order.orderId, errReason, unique_identifier, transaction_id)
				else
					Gac2Gas.LogFailedOrderInfo(order.productId, order.orderId, errReason, unique_identifier, transaction_id)
				end
				isSend = true
			end
		end 
		if not isSend then
			if CSwitchMgr.EnablePcPay and isPcPayQuery then
				local pcPayTicket = CPayMgr.Inst.PcPayTicket or ""
				Gac2Login.LogFailedOrderInfo(pcPayTicket, order.productId, order.orderId, errReason, "", "")
			else
				Gac2Gas.LogFailedOrderInfo(order.productId, order.orderId, errReason, "", "")
			end
		end
	end)

	if CLoginMgr.Inst:GetAppChannel() == ChannelDefine.APPSTORE and CommonDefs.IsIOSPlatform() then
		if order.orderStatus == OrderStatus.OS_SDK_INVALIDE_PRICE_LOCALE_ID or order.orderStatus == OrderStatus.OS_SDK_INVALID_CURRENCY then
			-- 提示用户使用指定币种支付
			CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("不支持使用此币种支付！"), 4)
			return
		end

		if string.find(errReason, "Another_order_is_checking") then
			CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("支付正在进行中，已提交给苹果支付系统，请稍候"), 4)
		elseif string.find(errReason, "Can_not_make_payments") or string.find(errReason, "Can_not_make_IAP") then
			CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("这个设备和系统不支持IAP支付"), 4)
		elseif string.find(errReason, "Receipt_not_exist") then
			CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("收费凭证为空"), 4)
		elseif string.find(errReason, "Illegal_currency") then
			CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("不支持使用此币种支付！"), 4)
		elseif string.find(errReason, "Invalid_product_id") then
			CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("网络不稳定，获取商品信息失败"), 4)
		elseif string.find(errReason, "SKPayment_transaction_failed") then
			errReason = string.gsub(errReason, "SKPayment_fransaction_failed", "")
			CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("您取消了支付或者暂时无法连接到iTunes Store"), 4)
		elseif string.find(errReason, "Generate_requestData_failed") then
			CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("生成苹果服务端验证的收费凭证数据失败"), 4)
		elseif string.find(errReason, "Verify_connection_failed") then
			CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("去苹果服务端验证收费凭证时连接失败"), 4)
		elseif string.find(errReason, "Not_json_response") then
			CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("苹果服务端验证订单后返回数据非json格式"), 4)
		elseif string.find(errReason, "Receipt_verify_failed") then
			errReason = string.gsub(errReason, "Receipt_verify_failed status", "")
			CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("验证失败，") .. errReason, 4)
		elseif string.find(errReason, "Query_product_from_jf_failed") then
			CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("网络不稳定，请稍后再试"), 4)
		elseif string.find(errReason, "Nt_Create_order_id_failed") then
			if order.jfCode == 460 and order.jfSubCode == 3 then
				-- sdk会弹框提示用户，需要自己弹框要关掉sdk的再弹，参考unisdk苹果内购支付文档
			elseif order.jfCode == 460 and order.jfSubCode == 7 then
				CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("不支持使用此币种支付"), 4)
			end
		end
	end
end
CPayMgr.m_hookShowOrderResult = LuaPayMgr.m_hookShowOrderResult

LuaPayMgr.m_hookOnGetQueryOrderResult = function (this, pid, sn, result) 
    -- google_play ntConsume, order 只用到了orderID 和orderChannel
    if SdkU3d.getPayChannel() == ChannelDefine.GOOGLE then
        local order = CreateFromClass(NtOrderInfo)
        order.orderId = sn
        order.orderChannel = SdkU3d.getPayChannel()
        SdkU3d.ntConsume(order)

        if CommonDefs.DictContains(this.m_CheckedAndRestoredOrders, typeof(String), sn) then
            CommonDefs.DictRemove(this.m_CheckedAndRestoredOrders, typeof(String), sn)
        end
    end
    if SdkU3d.getPayChannelByPid(pid) == ChannelDefine.HUAWEI then
        local order = CreateFromClass(NtOrderInfo)
        order.orderId = sn
        SdkU3d.ntConsume(order)

        if CommonDefs.DictContains(this.m_CheckedAndRestoredOrders, typeof(String), sn) then
            CommonDefs.DictRemove(this.m_CheckedAndRestoredOrders, typeof(String), sn)
        end
    end

    SdkU3d.removeCheckedOrders(sn)
    if sn ~= "" and not CommonDefs.DictContains(this.m_AlreadyCheckedOrders, typeof(String), sn) then
        CommonDefs.DictAdd(this.m_AlreadyCheckedOrders, typeof(String), sn, typeof(Boolean), true)
    end
    Gac2Gas.SendRemoveCheckedOrderDone(pid, sn)

    if CommonDefs.DictContains(this.m_AlreadyNotifiedPromoCodeOrders, typeof(String), sn) then
        CommonDefs.DictRemove(this.m_AlreadyNotifiedPromoCodeOrders, typeof(String), sn)
    end
    if CommonDefs.DictContains(this.m_PromoCodeQueryOrderActions, typeof(String), sn) then
        CommonDefs.DictRemove(this.m_PromoCodeQueryOrderActions, typeof(String), sn)
    end

    if this:NeedReportInvalidCurrencies() then
        local delayReportAction = DelegateFactory.Action(function () 
            DRPFMgr.Inst:QueryOrderDone(pid, sn, result, this.m_ClientCurrency)
        end)
        this:AddDelayReportQueryOrderDoneAction(pid, delayReportAction)
        this:ExtendFuncQuerySkuDetails(pid)
    end

    EventManager.BroadcastInternalForLua(EnumEventType.OnGetQueryOrderResult, {pid, sn, result})
end
CPayMgr.m_hookOnGetQueryOrderResult = LuaPayMgr.m_hookOnGetQueryOrderResult 

LuaPayMgr.m_hookParseAppStoreReceiptNoRecurive = function(this, receipt)
	local result = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
	if System.String.IsNullOrEmpty(receipt) then
		return result
	end
	
	local base64EncodedBytes = CommonDefs.Convert_FromBase64String(receipt)
	if base64EncodedBytes ~= nil then
		receipt = CommonDefs.UTF8Encoding_GetString(base64EncodedBytes)
		for key, value in string.gmatch(receipt, "\"([^\"]+)\"%s*=%s*\"([^\"]+)\";") do
			if key and value then
				CommonDefs.DictAdd(result, typeof(String), key, typeof(Object), value)
			end
		end
	end
	return result
end
CPayMgr.m_hookParseAppStoreReceiptNoRecurive = LuaPayMgr.m_hookParseAppStoreReceiptNoRecurive
