
CLuaChunJieSeriesTaskDescWnd = class()

CLuaChunJieSeriesTaskDescWnd.m_TaskId = 0

function CLuaChunJieSeriesTaskDescWnd:Init()
    local descLabel  = self.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
    descLabel.text = nil
    local taskId = CLuaChunJieSeriesTaskDescWnd.m_TaskId
    if taskId>0 then
        local task = Task_Task.GetData(taskId)
        descLabel.text = CUICommonDef.TranslateToNGUIText(task.TaskDescription)

        local taskProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.TaskProp or nil
        local got = taskProp and (CommonDefs.DictContains_LuaCall(taskProp.CurrentTasks,taskId) or taskProp:IsMainPlayerTaskFinished(taskId)) or false
        local button = self.transform:Find("Button").gameObject
        if got then
            button:SetActive(false)
        else
            UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function(g)
                CUIManager.CloseUI(CLuaUIResources.ChunJieSeriesTaskDescWnd)
                CUIManager.CloseUI(CLuaUIResources.ChunJieSeriesTaskWnd)
                Gac2Gas.RequestAutoAcceptTask(CLuaChunJieSeriesTaskDescWnd.m_TaskId)
            end)
        end
    end
end
