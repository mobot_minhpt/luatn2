local CMainCamera = import "L10.Engine.CMainCamera"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local Ease = import "DG.Tweening.Ease"

local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local GameObject			= import "UnityEngine.GameObject"
local Vector3               = import "UnityEngine.Vector3"

--define
CLuaZQYueShenDianTopRightWnd = class()

--RegistChildComponent
RegistChildComponent(CLuaZQYueShenDianTopRightWnd, "ExpandButton",	GameObject)
RegistChildComponent(CLuaZQYueShenDianTopRightWnd, "CountLabel",	UILabel)
RegistChildComponent(CLuaZQYueShenDianTopRightWnd, "Items",		    GameObject)
RegistChildComponent(CLuaZQYueShenDianTopRightWnd, "Content",		GameObject)
RegistChildComponent(CLuaZQYueShenDianTopRightWnd, "m_Fx","Fx",CUIFx)

--RegistClassMember
RegistClassMember(CLuaZQYueShenDianTopRightWnd, "m_count")
RegistClassMember(CLuaZQYueShenDianTopRightWnd, "m_TotalNum")
RegistClassMember(CLuaZQYueShenDianTopRightWnd, "m_itemStates")
RegistClassMember(CLuaZQYueShenDianTopRightWnd, "m_curLv")--当前层
RegistClassMember(CLuaZQYueShenDianTopRightWnd, "m_IsDoAni")
RegistClassMember(CLuaZQYueShenDianTopRightWnd, "m_Tick")

CLuaZQYueShenDianTopRightWnd.Level = 0
--@region flow function

function CLuaZQYueShenDianTopRightWnd:Awake()
    self.m_count = 0 
end

function CLuaZQYueShenDianTopRightWnd:Init()
    self.m_count = 0 
    self.m_curLv = 1
    self.m_itemStates={2,2,2,2,2}--0:隐藏；1:正常显示；2:加锁显示
    self:UpdateCount(0,8)
    self.m_IsDoAni = false
end

function CLuaZQYueShenDianTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("OnSyncYueShenTempleInfo", self, "OnSyncYueShenTempleInfo")
    g_ScriptEvent:AddListener("PlayerGetPickInYueShenTemple", self, "OnPlayerGetPickInYueShenTemple")
    local btnclick = function(go)
        self:OnExpandButtonClick()
    end

    CommonDefs.AddOnClickListener(self.ExpandButton, DelegateFactory.Action_GameObject(btnclick), false)
end

function CLuaZQYueShenDianTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("OnSyncYueShenTempleInfo", self, "OnSyncYueShenTempleInfo")
    g_ScriptEvent:RemoveListener("PlayerGetPickInYueShenTemple", self, "OnPlayerGetPickInYueShenTemple")
end

--@endregion

--@region 界面接口

function CLuaZQYueShenDianTopRightWnd:UpdateCount(count,max)
    if CLuaZQYueShenDianTopRightWnd.Level==nil or CLuaZQYueShenDianTopRightWnd.Level<=1 then
        max = 3
    else
        max = 8 
    end
    self.CountLabel.text = count.."/"..max
    --@todo PlayAnim
end

function CLuaZQYueShenDianTopRightWnd:UpdateTeamState(teaminfo)
    local count = #teaminfo
    for i=1,5 do
        local item = self.Items.transform:Find("Item"..i).gameObject
        if i > count then
            if self.m_itemStates[i] ~= 0 then
                self.m_itemStates[i] = 0
                item:SetActive(false)
            end
        else
            if self.m_itemStates[i] == 0 then --控件处于隐藏状态
                item:SetActive(true)
            end
            self.m_itemStates[i] = teaminfo[i].isHaveBuff and 1 or 2
            item.transform:Find("Lock").gameObject:SetActive(self.m_itemStates[i]==1)
        end
    end
end

--@endregion

--@region 界面事件

function CLuaZQYueShenDianTopRightWnd:OnExpandButtonClick()
	self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
	self.Content:SetActive(false)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function CLuaZQYueShenDianTopRightWnd:OnHideTopAndRightTipWnd()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
    if self.m_curLv == 3 then
        return
    end
	self.Content:SetActive(true)
end

function CLuaZQYueShenDianTopRightWnd:OnSyncYueShenTempleInfo(num,totalNum,teaminfo,currentLevel)
    if self.m_curLv ~= 3 and currentLevel == 3 then
        self.m_curLv = currentLevel
        self.Content:SetActive(false)
        -- CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
        return
    end
    
    if self.m_count ~= num then 
        if not self.m_IsDoAni then
            self:UpdateCount(num,totalNum)
        end
        self.m_count = num
        self.m_TotalNum = totalNum
    end
    self:UpdateTeamState(teaminfo)
end


function CLuaZQYueShenDianTopRightWnd:OnDestroy()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end
function CLuaZQYueShenDianTopRightWnd:OnPlayerGetPickInYueShenTemple(engineId,count)
    self.m_IsDoAni = false

    local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if obj then
        local screenPos = CMainCamera.Main:WorldToScreenPoint(obj.WorldPos)
        screenPos.z = 0
        if self.m_Tick then
            UnRegisterTick(self.m_Tick)
            self.m_Tick = nil
            self:UpdateCount(self.m_count,self.m_TotalNum)
        end
    
        self.m_Tick = RegisterTickOnce(function()
            if count>self.m_count then
                self.m_count = count
            end
            self:UpdateCount(self.m_count,self.m_TotalNum)
            self.m_IsDoAni = false
            self.m_Tick = nil
            self.m_Fx:DestroyFx()
        end,1000)
    
        self.m_IsDoAni = true

        local fromNGUIworldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
        local fromNGUIrelativePos = self.m_Fx.transform:InverseTransformPoint(fromNGUIworldPos)
        fromNGUIrelativePos.z=0
    
        local targetPos = self.CountLabel.transform.position
        local targetLocalPos = self.m_Fx.transform:InverseTransformPoint(targetPos)
        targetLocalPos.z=0
        
        LuaTweenUtils.DOKill(self.m_Fx.FxRoot, false)
        
        local path = {fromNGUIrelativePos, targetLocalPos}
        local array = Table2Array(path, MakeArrayClass(Vector3))
        LuaUtils.SetLocalPosition(self.m_Fx.FxRoot,fromNGUIrelativePos.x, fromNGUIrelativePos.y, 0)
    
        LuaTweenUtils.DOLocalPathOnce(self.m_Fx.FxRoot, array, 1, Ease.Linear)
    
        self.m_Fx:DestroyFx()
        self.m_Fx:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
    end
end

