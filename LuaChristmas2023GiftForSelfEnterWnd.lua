local Transform = import "UnityEngine.Transform"

local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaChristmas2023GiftForSelfEnterWnd = class()
RegistClassMember(LuaChristmas2023GiftForSelfEnterWnd, "FinalGetGift")  -- 最终礼物对象
RegistClassMember(LuaChristmas2023GiftForSelfEnterWnd, "FinalGiftFx")   -- 最终礼物动效
RegistClassMember(LuaChristmas2023GiftForSelfEnterWnd, "m_FinalId")     -- 最终礼物的编号
RegistClassMember(LuaChristmas2023GiftForSelfEnterWnd, "m_GiftCount")   -- 礼物列表
RegistClassMember(LuaChristmas2023GiftForSelfEnterWnd, "m_GiftList")    -- 礼物列表
RegistClassMember(LuaChristmas2023GiftForSelfEnterWnd, "m_LightList")   -- 灯列表
RegistClassMember(LuaChristmas2023GiftForSelfEnterWnd, "m_getGiftList") -- 获得礼物列表
RegistClassMember(LuaChristmas2023GiftForSelfEnterWnd, "m_getCardList") -- 获得卡片列表
RegistClassMember(LuaChristmas2023GiftForSelfEnterWnd, "m_UpdateTick")  -- 延迟更新定时任务

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChristmas2023GiftForSelfEnterWnd, "MakeButton", "MakeButton", GameObject)
RegistChildComponent(LuaChristmas2023GiftForSelfEnterWnd, "GiftList", "GiftList", Transform)
RegistChildComponent(LuaChristmas2023GiftForSelfEnterWnd, "LightList", "LightList", Transform)
RegistChildComponent(LuaChristmas2023GiftForSelfEnterWnd, "FinalNoGift", "FinalNoGift", GameObject)
RegistChildComponent(LuaChristmas2023GiftForSelfEnterWnd, "FinalHasGift", "FinalHasGift", GameObject)
RegistChildComponent(LuaChristmas2023GiftForSelfEnterWnd, "TipsButton", "TipsButton", GameObject)
RegistChildComponent(LuaChristmas2023GiftForSelfEnterWnd, "FinalHasGot", "FinalHasGot", GameObject)

--@endregion RegistChildComponent end

function LuaChristmas2023GiftForSelfEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self.FinalGetGift = self.FinalHasGift.transform:GetChild(0).gameObject
    self.FinalGiftFx = self.FinalNoGift.transform:Find("CUIFx").gameObject
    self.FinalGiftFx:SetActive(false)
    self.m_GiftCount = 6
    self.m_FinalId = 7
    UIEventListener.Get(self.MakeButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.Christmas2023GiftForSelfMakeGiftWnd)
    end)
    UIEventListener.Get(self.TipsButton).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("Christmas2023_CarftFormula_TIPS")
    end)
end
-- 加载数据
function LuaChristmas2023GiftForSelfEnterWnd:LoadData()
    self.m_getGiftList = {}
    self.m_getCardList = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eChristmas2023GiftData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eChristmas2023GiftData)
        local tbl = g_MessagePack.unpack(playDataU.Data.Data)
        if tbl then
            if tbl.getGiftList then self.m_getGiftList = tbl.getGiftList end
            if tbl.getCardList then self.m_getCardList = tbl.getCardList end
        end
    end
end
-- 初始化礼物列表
function LuaChristmas2023GiftForSelfEnterWnd:InitGiftList()
    self.m_GiftList = {}
    for i = 0, self.m_GiftCount - 1 do
        local tf = self.GiftList:GetChild(i):GetChild(0)
        table.insert(self.m_GiftList, {
            picture = tf:Find("Picture").gameObject,
            overlay = tf:Find("Picture/Overlay").gameObject,
            nogift = tf:Find("Status/NoGift").gameObject,
            hasgot = tf:Find("Status/NoGift/HasGot").gameObject,
            hasgift = tf:Find("Status/HasGift").gameObject,
            getgift = tf:Find("Status/HasGift"):GetChild(0).gameObject,
        })
        -- 编号方便区分
        self.m_GiftList[i + 1].picture.name = tostring(i + 1)
        self.m_GiftList[i + 1].nogift.name = tostring(i + 1)
        self.m_GiftList[i + 1].getgift.name = tostring(i + 1)
    end
end
-- 初始化灯列表
function LuaChristmas2023GiftForSelfEnterWnd:InitLightList()
    self.m_LightList = {}
    for i = 0, self.m_GiftCount - 1 do
        local tf = self.LightList:GetChild(i)
        table.insert(self.m_LightList, {
            onimage = tf:Find("OnImage").gameObject,
            offimage = tf:Find("OffImage").gameObject,
        })
    end
end
-- 更新第i个礼物信息
function LuaChristmas2023GiftForSelfEnterWnd:UpdateInfoByIndex(i)
    if i ~= self.m_FinalId and (i < 1 or i > self.m_GiftCount) then return end
    if i == self.m_FinalId then
        if self.m_getCardList[i] and self.m_getCardList[i] > 0 then
            -- 有最终礼物
            if self.m_getGiftList[i] and self.m_getGiftList[i] > 0 then
                -- 领取过礼物
                self.FinalNoGift:SetActive(true)
                self.FinalHasGot:SetActive(true)
                self.FinalHasGift:SetActive(false)
                UIEventListener.Get(self.FinalNoGift).onClick = nil
            else
                -- 没领取过礼物
                self.FinalNoGift:SetActive(false)
                self.FinalHasGift:SetActive(true)
                UIEventListener.Get(self.FinalGetGift).onClick = DelegateFactory.VoidDelegate(function(go)
                    Gac2Gas.Christmas2023GetCardGift(self.m_FinalId)
                end)
            end
        else
            -- 没有最终礼物
            self.FinalNoGift:SetActive(true)
            self.FinalHasGot:SetActive(false)
            self.FinalHasGift:SetActive(false)
            UIEventListener.Get(self.FinalNoGift).onClick = DelegateFactory.VoidDelegate(function(go)
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("通过制作，解锁所有6个礼物，即可领取"))
            end)
        end
        return
    end
    if self.m_getCardList[i] and self.m_getCardList[i] > 0 then
        -- 有卡片
        self.m_GiftList[i].overlay:SetActive(false)
        if self.m_getGiftList[i] and self.m_getGiftList[i] > 0 then
            -- 领取过礼物
            self.m_GiftList[i].nogift:SetActive(true)
            self.m_GiftList[i].hasgot:SetActive(true)
            self.m_GiftList[i].hasgift:SetActive(false)
            UIEventListener.Get(self.m_GiftList[i].picture).onClick = nil
            UIEventListener.Get(self.m_GiftList[i].nogift).onClick = nil
        else
            -- 没领取过礼物
            self.m_GiftList[i].nogift:SetActive(false)
            self.m_GiftList[i].hasgift:SetActive(true)
            UIEventListener.Get(self.m_GiftList[i].getgift).onClick = DelegateFactory.VoidDelegate(function(go)
                Gac2Gas.Christmas2023GetCardGift(tonumber(go.name))
            end)
        end
        UIEventListener.Get(self.m_GiftList[i].picture).onClick = nil
    else
        -- 没有卡片
        self.m_GiftList[i].overlay:SetActive(true)
        self.m_GiftList[i].nogift:SetActive(true)
        self.m_GiftList[i].hasgot:SetActive(false)
        self.m_GiftList[i].hasgift:SetActive(false)
        UIEventListener.Get(self.m_GiftList[i].picture).onClick = DelegateFactory.VoidDelegate(function(go)
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("制作礼物成功后才会解锁哦"))
        end)
        UIEventListener.Get(self.m_GiftList[i].nogift).onClick = DelegateFactory.VoidDelegate(function(go)
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("制作礼物成功后才会解锁哦"))
        end)
    end
end
-- 显示礼物信息
function LuaChristmas2023GiftForSelfEnterWnd:ShowGift()
    for i = 1, self.m_GiftCount do
        self:UpdateInfoByIndex(i)
    end
    self:UpdateInfoByIndex(self.m_FinalId)
end
-- 显示灯
function LuaChristmas2023GiftForSelfEnterWnd:ShowLight()
    -- 不在主界面先不显示灯
    if LuaChristmas2023Mgr.m_UpWndCount > 0 then return end
    local count = 0
    for i = 1, self.m_GiftCount do
        if self.m_getCardList[i] and self.m_getCardList[i] > 0 then
            count = count + 1
        end
    end
    for i = 1, self.m_GiftCount do
        self.m_LightList[i].onimage:SetActive(i <= count)
        self.m_LightList[i].offimage:SetActive(i > count)
    end
end
function LuaChristmas2023GiftForSelfEnterWnd:Init()
    self:LoadData()
    self:InitGiftList()
    self:InitLightList()
    self:ShowGift()
    self:ShowLight()
end
-- 更新数据
function LuaChristmas2023GiftForSelfEnterWnd:OnUpdateTempPlayDataWithKey(args)
    local key = (args and args[0]) and args[0] or -1
    if key == EnumTempPlayDataKey_lua.eChristmas2023GiftData then
        self:LoadData()
        self:ShowGift()
        self:ShowLight()
    end
end
-- 更新数据
function LuaChristmas2023GiftForSelfEnterWnd:OnChristmas2023GetCardGiftSuccess(cardId)
    self:UpdateInfoByIndex(cardId)
    -- 领取最终礼物的时候播放动效
    if cardId == self.m_FinalId then
        self.FinalGiftFx:SetActive(true)
    end
end
-- 更新灯数据
function LuaChristmas2023GiftForSelfEnterWnd:OnChristmas2023UpdateLightInfo()
    LuaChristmas2023Mgr.m_UpWndCount = 0
    if self.m_UpdateTick then
        UnRegisterTick(self.m_UpdateTick)
        self.m_UpdateTick = nil
    end
    -- 延迟一会更新
    self.m_UpdateTick = RegisterTickOnce(function()
        self:ShowLight()
    end, 150)
end
function LuaChristmas2023GiftForSelfEnterWnd:OnEnable()
    LuaChristmas2023Mgr.m_UpWndCount = 0
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey", self, "OnUpdateTempPlayDataWithKey")
    g_ScriptEvent:AddListener("OnChristmas2023GetCardGiftSuccess", self, "OnChristmas2023GetCardGiftSuccess")
    g_ScriptEvent:AddListener("OnChristmas2023UpdateLightInfo", self, "OnChristmas2023UpdateLightInfo")
end
function LuaChristmas2023GiftForSelfEnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey", self, "OnUpdateTempPlayDataWithKey")
    g_ScriptEvent:RemoveListener("OnChristmas2023GetCardGiftSuccess", self, "OnChristmas2023GetCardGiftSuccess")
    g_ScriptEvent:RemoveListener("OnChristmas2023UpdateLightInfo", self, "OnChristmas2023UpdateLightInfo")
    if self.m_UpdateTick then
        UnRegisterTick(self.m_UpdateTick)
        self.m_UpdateTick = nil
    end
end

--@region UIEvent

--@endregion UIEvent

