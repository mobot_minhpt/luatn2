local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local QnTableView = import "L10.UI.QnTableView"
local QnTableItemPool = import "L10.UI.QnTableItemPool"
local CButton = import "L10.UI.CButton"
local QnTabView = import "L10.UI.QnTabView"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local QnButton = import "L10.UI.QnButton"
local CFeiShengMgr = import "L10.Game.CFeiShengMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CConstellationWnd = import "L10.UI.CConstellationWnd"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CXingGuanWnd = import "L10.UI.CXingGuanWnd"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local Screen = import "UnityEngine.Screen"

LuaXingGuanOverview = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaXingGuanOverview, "ShareBtn", "ShareBtn", GameObject)
RegistChildComponent(LuaXingGuanOverview, "Overview", "Overview", QnTableView)
RegistChildComponent(LuaXingGuanOverview, "Pool", "Pool", QnTableItemPool)
RegistChildComponent(LuaXingGuanOverview, "DescTemplate", "DescTemplate", GameObject)
RegistChildComponent(LuaXingGuanOverview, "Own", "Own", GameObject)
RegistChildComponent(LuaXingGuanOverview, "Tabs", "Tabs", GameObject)
RegistChildComponent(LuaXingGuanOverview, "HideButton", "HideButton", CButton)
RegistChildComponent(LuaXingGuanOverview, "EditButton", "EditButton", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaXingGuanOverview, "m_TotalNum")
RegistClassMember(LuaXingGuanOverview, "m_TabView")
RegistClassMember(LuaXingGuanOverview, "m_XingMangNumLab")
RegistClassMember(LuaXingGuanOverview, "m_XingMangAddBtn")
RegistClassMember(LuaXingGuanOverview, "m_ConstellationWnd")
RegistClassMember(LuaXingGuanOverview, "m_OverviewShowBtn")
RegistClassMember(LuaXingGuanOverview, "m_OverviewShowBtnArrow")
RegistClassMember(LuaXingGuanOverview, "m_DataList")
RegistClassMember(LuaXingGuanOverview, "m_XingGuanWnd")
RegistClassMember(LuaXingGuanOverview, "m_ScoreLab")
RegistClassMember(LuaXingGuanOverview, "m_NullLab")
RegistClassMember(LuaXingGuanOverview, "m_ScrollViewIndicator")

function LuaXingGuanOverview:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_TabView = self.Tabs:GetComponent(typeof(QnTabView))
    self.m_XingMangNumLab = self.Own:GetComponent(typeof(UILabel))
    self.m_XingMangAddBtn = self.Own.transform:Find("AddBtn"):GetComponent(typeof(QnButton))
    self.m_ConstellationWnd = self.transform.parent:Find("Constellation"):GetComponent(typeof(CConstellationWnd))
    self.m_XingGuanWnd = self.transform.parent:GetComponent(typeof(CXingGuanWnd))
    self.m_OverviewShowBtn = self.transform.parent:Find("XingGuanOverviewShowButton"):GetComponent(typeof(QnButton))
    self.m_OverviewShowBtnArrow = self.transform.parent:Find("XingGuanOverviewShowButton/sprite").gameObject
    self.m_ScoreLab = self.transform:Find("Anchor/Middle/Score"):GetComponent(typeof(UILabel))
    self.m_NullLab = self.transform:Find("Anchor/Middle/NullLab"):GetComponent(typeof(UILabel))
    self.m_ScrollViewIndicator = self.transform:Find("Anchor/Middle/ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator))

    self.m_TotalNum = Xingguan_Setting.GetData().XingguanTempletNum

    
    self.DescTemplate.gameObject:SetActive(false)
end

function LuaXingGuanOverview:Start()
    self:InitTemplate()

    self.m_XingMangAddBtn.OnClick = DelegateFactory.Action_QnButton(function(q)
        CUIManager.ShowUI(CUIResources.XingGuanShengJiWnd)
    end)

    UIEventListener.Get(self.ShareBtn).onClick = DelegateFactory.VoidDelegate(function ()
        self:OnShareBtnClick()
    end)
    self.ShareBtn.gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
    UIEventListener.Get(self.HideButton.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
        self.gameObject:SetActive(false)
        self.m_OverviewShowBtn.gameObject:SetActive(true)
        CLuaXingGuanWnd:OnCloseOverview()
    end)

    UIEventListener.Get(self.EditButton.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
        --self.m_XingGuanWnd:FastEnterInside(self.EditButton.gameObject)
        self.m_ConstellationWnd.gameObject:SetActive(true)
        self.m_ConstellationWnd:Init()
        self.m_XingGuanWnd.m_CanOperate = false
        CLuaXingGuanWnd:OnCloseOverview()
    end)

    self.Overview.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnSelectAtRow(row)
    end)

    self.m_DataList = {}
    self.m_Category2Props = {}
    Xingguan_Category.Foreach(function(key, val)
        table.insert(self.m_DataList, val.Category)
        self.m_Category2Props[key] = {}
        -- body
    end)
    self.Overview.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_DataList
        end,
        
        function(item, index)
            self:InitItem(item, self.m_DataList[index + 1], self.m_Category2Props[index + 1])
    end)

    self.m_ScoreLab.text = SafeStringFormat3(LocalString.GetString("（总评分 %s）"), CLuaPlayerCapacityMgr.GetValue("GrowthValue_XingGuan"))
    self.Overview:ReloadData(true, true)
    self.m_NullLab.gameObject:SetActive(self:IsNullEffect())
    self:Refresh()
end

function LuaXingGuanOverview:IsNullEffect()
    if self.m_Category2Props then
        for i = 1, #self.m_Category2Props do
            local prop = self.m_Category2Props[i]
            if #prop > 0 then
                return false
            end
        end
    end
    return true
end

function LuaXingGuanOverview:OnEnable()
    g_ScriptEvent:AddListener("SwitchXingguanTemplateSuccess", self, "OnSwitchXingguanTemplateSuccess")
    g_ScriptEvent:AddListener("SyncXingguanEffectOverview", self, "OnSyncXingguanEffectOverview")
    g_ScriptEvent:AddListener("SyncXingguanProp", self, "Refresh")
    g_ScriptEvent:AddListener("XingguanConstellationWndClose", self, "OnXingguanConstellationWndClose")
    g_ScriptEvent:AddListener("UpdatePlayerCapacity", self, "UpdatePlayerCapacity")
    self:RefreshTemplate()
    self:QueryOverview()
end

function LuaXingGuanOverview:OnDisable()
    g_ScriptEvent:RemoveListener("SwitchXingguanTemplateSuccess", self, "OnSwitchXingguanTemplateSuccess")
    g_ScriptEvent:RemoveListener("SyncXingguanEffectOverview", self, "OnSyncXingguanEffectOverview")
    g_ScriptEvent:RemoveListener("SyncXingguanProp", self, "Refresh")
    g_ScriptEvent:RemoveListener("XingguanConstellationWndClose", self, "OnXingguanConstellationWndClose")
    g_ScriptEvent:RemoveListener("UpdatePlayerCapacity", self, "UpdatePlayerCapacity")
end


--@region UIEvent

--@endregion UIEvent

function LuaXingGuanOverview:UpdatePlayerCapacity()
    self.m_ScoreLab.text = SafeStringFormat3(LocalString.GetString("（总评分 %s）"), CLuaPlayerCapacityMgr.GetValue("GrowthValue_XingGuan"))
end

function LuaXingGuanOverview:InitTemplate()
    self.m_TabView.OnSelect = DelegateFactory.Action_QnTabButton_int(function (btn, index) 
        self:OnClickTemplate(index)
    end)
end

function LuaXingGuanOverview:RefreshTemplate()
    if CClientMainPlayer.Inst == nil then
        return 
    end
    for i = 0, self.m_TotalNum - 1 do
        local tab = self.Tabs.transform:GetChild(i)
        local labGo = tab:Find("Label").gameObject
        local lockGo = tab:Find("Lock").gameObject
        local selectedGo = tab:Find("Texture_Highlight").gameObject
        local unlocked = i == 0 and true or CClientMainPlayer.Inst.PlayProp.XingguanProperty.TemplateUnlocked:GetBit(i)
        
        labGo:SetActive(unlocked)
        lockGo:SetActive(not unlocked)
        selectedGo:SetActive(i == CClientMainPlayer.Inst.PlayProp.XingguanProperty.ActiveTemplateId)
    end
end

function LuaXingGuanOverview:OnClickTemplate(index)
    if CClientMainPlayer.Inst == nil then
        return 
    end
    local playerId = CClientMainPlayer.Inst.Id
    local unlocked = index == 0 and true or CClientMainPlayer.Inst.PlayProp.XingguanProperty.TemplateUnlocked:GetBit(index)
    if unlocked then
        Gac2Gas.Xingguan_PlayerRequestSwitchTemplate(playerId, index, "")
    else 
        local titleStr = g_MessageMgr:FormatMessage("XingGuan_Unlock_Template_Msg")
        QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.LingYu, titleStr, Xingguan_Setting.GetData().TempletReleaseCost[index], DelegateFactory.Action(function () 
            Gac2Gas.Xingguan_PlayerRequestUnlockTemplate(CClientMainPlayer.Inst.Id, index, "")
        end), nil, false, true, EnumPlayScoreKey.NONE, false)
    end
end

function LuaXingGuanOverview:OnSwitchXingguanTemplateSuccess(templateId)
    self:QueryOverview()
end

function LuaXingGuanOverview:QueryOverview()
    if CClientMainPlayer.Inst == nil then
        return 
    end
    local playerId = CClientMainPlayer.Inst.Id
    Gac2Gas.Xingguan_PlayerRequestGetEffectOverview(playerId, "")
end

function LuaXingGuanOverview:OnSyncXingguanEffectOverview(activeTemplateId, allPropTable, statusPropTable, paramSumTable, xingguanIdTable)
    for i = 1, #self.m_Category2Props do
        self.m_Category2Props[i] = {}
    end

    for i = 1, #allPropTable do
        local prop = allPropTable[i]
        local data = Xingguan_PlayerPropEffect.GetData(prop.propIdx)
        if data then
            prop.name = data.Desc
            prop.val = (0 < prop.val and prop.val < 1) and (math.floor(prop.val * 100) .. "%") or prop.val
            prop.order = data.Order
            prop.categoryId = data.CategoryId
            prop.show = data.Show
            
            table.insert(self.m_Category2Props[prop.categoryId], prop)
        end
    end

    for i = 1, #statusPropTable do
        local status = statusPropTable[i]
        local data = self:GetStatusPropEffectDataByStatusIdxAndPropIdx(status.statusIdx, status.propIdx)
        if data then
            status.name = data.Desc
            status.val = status.val
            status.order = data.Order
            status.categoryId = data.CategoryId
            status.show = data.Show
            table.insert(self.m_Category2Props[status.categoryId], status)
        end
    end

    for i = 1, #paramSumTable do
        local paramSum = paramSumTable[i]
        if paramSum.gameEventBonus == "Equipment_SpecialLevelLimit" then
            paramSum.name = LocalString.GetString("装备可穿戴等级")
            paramSum.val = "+" .. paramSum.param
            paramSum.order = 1
            paramSum.categoryId = 5
            paramSum.show = 1
            table.insert(self.m_Category2Props[paramSum.categoryId], paramSum)
        end
    end

    for i = 1, #xingguanIdTable do
        local xingguanId = xingguanIdTable[i]
        local data = Xingguan_StarGroup.GetData(xingguanId)
        if data then
            local xingguanEffect = {}
            local splits = g_LuaUtil:StrSplit(data.OverviewEffectDesc, ';')
            xingguanEffect.xingguanId = xingguanId
            xingguanEffect.name = splits[1] or "" 
            xingguanEffect.val = splits[2] and splits[2] or "" 
            xingguanEffect.order = data.OverviewEffectOrder
            xingguanEffect.categoryId = data.CategoryId
            xingguanEffect.show = data.Show
            table.insert(self.m_Category2Props[xingguanEffect.categoryId], xingguanEffect)
        end
    end

    for i = 1, #self.m_Category2Props do
        local t = self.m_Category2Props[i]
        table.sort(t, function(a, b)
            if a.show == b.show then
                return a.order < b.order 
            else
                return a.show > b.show
            end
        end)
    end

    self.Overview:ReloadData(true, true)
    self.m_NullLab.gameObject:SetActive(self:IsNullEffect())
    self.m_ScoreLab.text = SafeStringFormat3(LocalString.GetString("（总评分 %s）"), CLuaPlayerCapacityMgr.GetValue("GrowthValue_XingGuan"))
    self:Refresh()
    self:Layout()
end

function LuaXingGuanOverview:RefreshXingMang()
    self.m_XingMangNumLab.text = (CFeiShengMgr.Inst.m_TotalXingmangCount - CFeiShengMgr.Inst.m_UsedXingmangCount .. "/") .. CFeiShengMgr.Inst.m_TotalXingmangCount
end

function LuaXingGuanOverview:Refresh()
    self:RefreshTemplate()
    self:RefreshXingMang()
end


function LuaXingGuanOverview:OnShareBtnClick()
    CUICommonDef.CaptureScreen(
        "screenshot",
        true,
        false,
        nil,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            end
        ),
        false
    )
end

function LuaXingGuanOverview:InitItem(item, info, Prop)
    local titleLab = item.transform:Find("Title/MessageLabel"):GetComponent(typeof(UILabel))
    local NoLab = item.transform:Find("NOLabel"):GetComponent(typeof(UILabel))
    local grid = item.transform:Find("Table"):GetComponent(typeof(UIGrid))
    titleLab.text = info

    NGUITools.DestroyChildren(grid.transform)
    local noEffect = false
    if #Prop > 0 then
        for i = 1, #Prop do
            local prop = Prop[i]
            if prop.show == 1 and grid.transform.childCount % 2 == 1 then
                local blankGo = NGUITools.AddChild(grid.gameObject, self.DescTemplate)
                blankGo:SetActive(true)
                blankGo.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = ""
                blankGo.transform:Find("ValueLabel"):GetComponent(typeof(UILabel)).text = ""
            end 
            local go = NGUITools.AddChild(grid.gameObject, self.DescTemplate)
            go:SetActive(true)
            local nameLab = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
            local valLab = go.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
            nameLab.text = prop.name
            valLab.text = prop.val
        end
        grid.maxPerLine = 2
    else 
        noEffect = true
    end
    NoLab.gameObject:SetActive(noEffect)
    item.gameObject:SetActive(not noEffect)

    grid:Reposition()
end

function LuaXingGuanOverview:GetStatusPropEffectDataByStatusIdxAndPropIdx(StatusIdx, PropIdx)
    for i = 1, Xingguan_StatusPropEffect.GetDataCount() do
        local data = Xingguan_StatusPropEffect.GetData(i)
        if data and data.StatusIdx == StatusIdx and data.PropIdx == PropIdx then
            return data
        end
    end
    return nil
end

function LuaXingGuanOverview:OnXingguanConstellationWndClose()
    self:Refresh()
    self:QueryOverview()
end

function LuaXingGuanOverview:Layout()
    self.m_Height = Screen.height
    self.m_Width = Screen.width
    self.m_ScrollViewIndicator:Layout()
end

function LuaXingGuanOverview:Update()
    if CommonDefs.Is_PC_PLATFORM() then
        if self.m_Height ~= Screen.height or self.m_Width ~= Screen.width then
            self:Layout()
        end
    end
end
