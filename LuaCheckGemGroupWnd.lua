local MessageWndManager = import "L10.UI.MessageWndManager"
local Extensions = import "Extensions"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local UITable=import "UITable"
local CCommonLuaScript=import "L10.UI.CCommonLuaScript"
local AlignType1 = import "L10.UI.CItemInfoMgr+AlignType"

local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local Color = import "UnityEngine.Color"
local CUITexture=import "L10.UI.CUITexture"
local QnTabView=import "L10.UI.QnTabView"

local EnumItemType = import "L10.Game.EnumItemType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"

local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CUIGameObjectPool = import "L10.UI.CUIGameObjectPool"



CLuaCheckGemGroupWnd = class()
RegistClassMember(CLuaCheckGemGroupWnd,"inlayGemTemplateId")
RegistClassMember(CLuaCheckGemGroupWnd,"isRequestInlayHere")

RegistClassMember(CLuaCheckGemGroupWnd,"m_CommonItem")
RegistClassMember(CLuaCheckGemGroupWnd,"m_HoleList")
RegistClassMember(CLuaCheckGemGroupWnd,"m_HoleInfoList")
RegistClassMember(CLuaCheckGemGroupWnd,"m_TabView")
RegistClassMember(CLuaCheckGemGroupWnd,"m_Mark")
RegistClassMember(CLuaCheckGemGroupWnd,"m_SrcHoleIndex")
RegistClassMember(CLuaCheckGemGroupWnd,"m_SrcHoleItemId")
RegistClassMember(CLuaCheckGemGroupWnd,"m_GemItemPool")



CLuaCheckGemGroupWnd.m_HoleSetIndex= 0
function CLuaCheckGemGroupWnd:Awake()
    if CommonDefs.IS_VN_CLIENT then
        self.transform:Find("Anchor/TableViewPool/GemGroupTemplate/Name/Current").transform.localPosition = Vector3(-69, 40, 0)
    end
    
    self.m_SrcHoleIndex = 0
    self.m_SrcHoleItemId = 0
    
    self.m_CommonItem=nil
    self.m_HoleList={}
    self.m_HoleInfoList={}

    self.gemGroupList = {}
    self.inlayGemTemplateId = 0
    self.isRequestInlayHere = false

    local switchButtons = FindChild(self.transform,"SwitchButtons")
    local switchButton1 = switchButtons:GetChild(0)
    local switchButton2 = switchButtons:GetChild(1)
    local switchSprite1 = switchButton1:GetComponent(typeof(UISprite))
    local switchSprite2 = switchButton2:GetComponent(typeof(UISprite))
    self.m_TabView = switchButtons:GetComponent(typeof(QnTabView))
    self.m_TabView.OnSelect=DelegateFactory.Action_QnTabButton_int(function (go, index)
        if index == 0 then--第一套
            LuaUtils.SetLocalScale(switchButton1,1.2,1.2,1.2)
            LuaUtils.SetLocalScale(switchButton2,1,1,1)
            switchSprite1.depth=2
            switchSprite2.depth=3
        elseif index == 1 then
            LuaUtils.SetLocalScale(switchButton1,1,1,1)
            LuaUtils.SetLocalScale(switchButton2,1.2,1.2,1.2)
            switchSprite1.depth=3
            switchSprite2.depth=2
        end
        CLuaCheckGemGroupWnd.m_HoleSetIndex=index+1
        self:InitEquipmentInfo()
        -- self:RefreshGemGroupList()
        self:UpdateSortDisplay()
        self:InitGemGroupList( )
    end)

    self.m_Mark = FindChild(self.transform,"MarkSprite").gameObject
    self.m_Mark:SetActive(false)


	self.m_GemItemPool = FindChild(self.transform,"GemItemPool"):GetComponent(typeof(CUIGameObjectPool))
    self.TableView = FindChild(self.transform,"Scrollview"):GetComponent(typeof(QnTableView))
    self.TableViewDataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.gemGroupList
        end,
        function(item,row) 
            local instance = item.gameObject
            local template = instance:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
            if template ~= nil then
                template.OnGemGroupSelected = function(go)
                    self.TableView:SetSelectRow(row,true)
                end
                template.m_GemItemPool = self.m_GemItemPool
                template.OnRequestInlayGem = function(templateId)
                    self:OnRequestInlayGem(templateId)
                end
                template:Init(self.gemGroupList[row+1])
            end

        end
    )
    self.TableView.m_DataSource = self.TableViewDataSource
    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        -- self:OnGemGroupClicked(go)
    end)

end

function CLuaCheckGemGroupWnd:Init( )
    self.gemGroupList={}
    if CEquipmentProcessMgr.Inst.SelectEquipment == nil then
        return
    end
    
    local itemInfo = CEquipmentProcessMgr.Inst.SelectEquipment
    local equip = EquipmentTemplate_Equip.GetData(itemInfo.templateId)

    local commonItem = CItemMgr.Inst:GetById(itemInfo.itemId)
    if commonItem.Equip.ActiveHoleSetIdx<=1 then
        self.m_TabView:ChangeTo(0)
    else
        self.m_TabView:ChangeTo(1)
    end
    if not commonItem.Equip.HasTwoWordSet then
        self.m_TabView.gameObject:SetActive(false)
    end


    GemGroup_GemGroup.Foreach(function (key, data) 
        if data.Level == 1 and data.Available == 1 then
            local fit = false
            if data.BothHand == 1 and equip.BothHand ~= 1 then
                fit = false
            else
                local type = g_LuaUtil:StrSplit(data.Equipment,";")
                for i,v in ipairs(type) do
                    local subType = g_LuaUtil:StrSplit(v,",")
                    if #subType>=2 then
                        if equip.Type == tonumber(subType[1]) and equip.SubType == tonumber(subType[2]) then
                            fit = true
                            break
                        end
                    end
                end
            end
            if fit then
                table.insert( self.gemGroupList, key )
            end
        end
    end)

    self:UpdateSortDisplay()
end
function CLuaCheckGemGroupWnd:UpdateSortDisplay( )
    local info = CEquipmentProcessMgr.Inst.SelectEquipment
    local equip = CItemMgr.Inst:GetById(info.itemId)
    local hasGem = false

    local is2set = CLuaCheckGemGroupWnd.m_HoleSetIndex==2
    for i=1,equip.Equip.Hole do
        local b = is2set and equip.Equip.SecondaryHoleItems[i]>0 or equip.Equip.HoleItems[i]>0
        if b then hasGem=true break end
    end

    local gemDesignLookup = {}
    for i,v in ipairs(self.gemGroupList) do
        gemDesignLookup[v] = GemGroup_GemGroup.GetData(v)
    end
    if hasGem then
        table.sort( self.gemGroupList,function(Id1,Id2)
            local gemGroup1 = gemDesignLookup[Id1]
            local gemGroup2 = gemDesignLookup[Id2]
            local count1 = 0
            local count2 = 0
            for i=1,equip.Equip.Hole do
                local tmpId = 0
                if is2set then
                    tmpId = equip.Equip.SecondaryHoleItems[i]
                    if tmpId>0 and tmpId<100 then
                        tmpId = equip.Equip.HoleItems[tmpId]
                    end
                else
                    tmpId = equip.Equip.HoleItems[i]
                end

                local jewelInHole = Jewel_Jewel.GetData(tmpId)
                if jewelInHole then
                    for j=1,gemGroup1.Consist.Length do
                        local jewel = Jewel_Jewel.GetData(gemGroup1.Consist[j-1])
                        if jewel.JewelKind == jewelInHole.JewelKind then
                            count1 = count1 + 1
                        end
                    end

                    for j=1,gemGroup2.Consist.Length do
                        local jewel = Jewel_Jewel.GetData(gemGroup2.Consist[j-1])
                        if jewel.JewelKind == jewelInHole.JewelKind then
                            count2 = count2 + 1
                        end
                    end
                end
            end
            if count1 > count2 then
                return true
            elseif count1 < count2 then
                return false
            elseif gemGroup1.Consist.Length > gemGroup2.Consist.Length then
                return false
            elseif gemGroup1.Consist.Length < gemGroup2.Consist.Length then
                return true
            else
                return Id1<Id2
            end
        end)
    else
        table.sort( self.gemGroupList,function(a,b)
            local gemGroup1 = gemDesignLookup[a]
            local gemGroup2 = gemDesignLookup[b]
            if gemGroup1.Consist.Length > gemGroup2.Consist.Length then
                return false
            elseif gemGroup1.Consist.Length < gemGroup2.Consist.Length then
                return true
            else
                return a<b
            end
        end )
    end
    self:InitGemGroupList()
end

function CLuaCheckGemGroupWnd:InitGemGroupList( )
    self.TableView:ReloadData(true, false)
end
--如果是第二套的逻辑 优先镶嵌第一套上面的
--第二套也需要使用这条规则
function CLuaCheckGemGroupWnd:OnRequestInlayGem( templateId) 
    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipment
    if equipInfo == nil then
        return
    end
    self.inlayGemTemplateId = templateId
    CItemInfoMgr.CloseItemInfoWnd()

    local equip = CItemMgr.Inst:GetById(equipInfo.itemId)
    local inlayed = false
    if equip.Equip.HoleSetCount == 2 then
        local jewel = Jewel_Jewel.GetData(templateId)
        if CLuaCheckGemGroupWnd.m_HoleSetIndex==2 then--看第一套有没有
            for i=1,equip.Equip.Hole do
                local jewelInHole = Jewel_Jewel.GetData(equip.Equip.HoleItems[i])
                if jewelInHole and jewel.JewelKind == jewelInHole.JewelKind then
                    self.m_SrcHoleIndex = i
                    self.m_SrcHoleItemId = equip.Equip.HoleItems[i]
                    inlayed = true
                    break
                end
            end
        else--看第二套有没有
            for i=1,equip.Equip.Hole do
                local tempId = equip.Equip.SecondaryHoleItems[i]
                if tempId>100 then
                    local jewelInHole = Jewel_Jewel.GetData(tempId)
                    if jewelInHole and jewel.JewelKind == jewelInHole.JewelKind then
                        self.m_SrcHoleIndex = i
                        self.m_SrcHoleItemId = tempId
                        inlayed = true
                        break
                    end
                end
            end
        end
    end
    if inlayed then
        self.isRequestInlayHere = true
        Gac2Gas.RequestCanReferenceStoneOnEquipment(
            EnumToInt(equipInfo.place), 
            equipInfo.pos, 
            equipInfo.itemId, 
            self.m_SrcHoleIndex,
            self.m_SrcHoleItemId,
            CLuaCheckGemGroupWnd.m_HoleSetIndex==2 and 1 or 2)
        return
    end

    local find, requestInlayGemPos, requestInlayGemId = CItemMgr.Inst:FindItemByTemplateIdPriorToBinded(EnumItemPlace.Bag, templateId)
    if not find then
        return
    end

    local item = CItemMgr.Inst:GetById(requestInlayGemId)
    
    if item and not item.IsBinded and equip.IsBinded then--没绑定
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("INLAY_STONE_BIND_CONFIRM"), DelegateFactory.Action(function () 
            self.isRequestInlayHere = true
            Gac2Gas.RequestCanInlayStoneOnEquipment(
                EnumToInt(equipInfo.place), 
                equipInfo.pos, 
                equipInfo.itemId, 
                EnumItemPlace_lua.Bag, 
                requestInlayGemPos, 
                requestInlayGemId, 
                CLuaCheckGemGroupWnd.m_HoleSetIndex)
        end), nil, nil, nil, false)
    else
        self.isRequestInlayHere = true
        Gac2Gas.RequestCanInlayStoneOnEquipment(
            EnumToInt(equipInfo.place), 
            equipInfo.pos, 
            equipInfo.itemId, 
            EnumItemPlace_lua.Bag, 
            requestInlayGemPos, 
            requestInlayGemId, 
            CLuaCheckGemGroupWnd.m_HoleSetIndex)
    end
end

function CLuaCheckGemGroupWnd:OnRequestCanInlayStoneOnEquipmentResult( can, equipId, equipNewGrade, setIndex) 
    if not self.isRequestInlayHere then return end
    self.isRequestInlayHere=false
    if not can then
        return
    end
    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipment
    if equipInfo == nil or equipInfo.itemId ~= equipId then
        return
    end
    if not CEquipmentProcessMgr.Inst:CheckOperationConflict(equipInfo) then
        return
    end
    CItemInfoMgr.CloseItemInfoWnd()

    local default, requestInlayGemPos, requestInlayGemId = CItemMgr.Inst:FindItemByTemplateIdPriorToBinded(EnumItemPlace.Bag, self.inlayGemTemplateId)
    if requestInlayGemId==nil or requestInlayGemId=="" then
        return
    end

    local equipItem = CItemMgr.Inst:GetById(equipInfo.itemId)
    if not equipItem.Equip.IsBinded then
        local message = g_MessageMgr:FormatMessage("Equipment_Inlay_Stone_Confirm")
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
            Gac2Gas.InlayStoneOnEquipment(EnumToInt(equipInfo.place), equipInfo.pos, equipInfo.itemId, EnumItemPlace_lua.Bag, requestInlayGemPos, requestInlayGemId, setIndex)
        end), nil, nil, nil, false)
    else
        Gac2Gas.InlayStoneOnEquipment(EnumToInt(equipInfo.place), equipInfo.pos, equipInfo.itemId, EnumItemPlace_lua.Bag, requestInlayGemPos, requestInlayGemId, setIndex)
    end
end

function CLuaCheckGemGroupWnd:OnRequestCanReferenceStoneOnEquipmentResult(can, equipId, equipNewGrade)
    if not self.isRequestInlayHere then return end
    self.isRequestInlayHere = false
    if not can then return end
    if CClientMainPlayer.Inst == nil then return end

    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipment
    if equipInfo == nil then return end

    Gac2Gas.ReferenceStoneOnEquipment(
        EnumToInt(equipInfo.place), 
        equipInfo.pos, 
        equipInfo.itemId, 
        self.m_SrcHoleIndex,
        self.m_SrcHoleItemId,
		CLuaCheckGemGroupWnd.m_HoleSetIndex==2 and 1 or 2)
    CUIManager.CloseUI(CUIResources.InlayGemListWnd)
end

function CLuaCheckGemGroupWnd:OnGemGroupClicked( go) 
    -- for i,v in ipairs(self.gemGroupGoList) do
    --     v:GetComponent(typeof(QnSelectableButton)):SetSelected(go == v, false)
    -- end
end

function CLuaCheckGemGroupWnd:OnEnable()
    g_ScriptEvent:AddListener("RequestCanInlayStoneOnEquipmentResult",self,"OnRequestCanInlayStoneOnEquipmentResult")
    g_ScriptEvent:AddListener("SendItem",self,"OnSendItem")
    g_ScriptEvent:AddListener("SetItemAt",self,"OnSetItemAt")
    g_ScriptEvent:AddListener("RequestCanReferenceStoneOnEquipmentResult",self,"OnRequestCanReferenceStoneOnEquipmentResult")
    g_ScriptEvent:AddListener("ReplyEquipInfoInAttrGroup",self,"OnReplyEquipInfoInAttrGroup")

end
function CLuaCheckGemGroupWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RequestCanInlayStoneOnEquipmentResult",self,"OnRequestCanInlayStoneOnEquipmentResult")
    g_ScriptEvent:RemoveListener("SendItem",self,"OnSendItem")
    g_ScriptEvent:RemoveListener("SetItemAt",self,"OnSetItemAt")
    g_ScriptEvent:RemoveListener("RequestCanReferenceStoneOnEquipmentResult",self,"OnRequestCanReferenceStoneOnEquipmentResult")
    g_ScriptEvent:RemoveListener("ReplyEquipInfoInAttrGroup",self,"OnReplyEquipInfoInAttrGroup")
end

function CLuaCheckGemGroupWnd:OnSendItem(args)
    self:InitEquipmentInfo()

end
function CLuaCheckGemGroupWnd:OnSetItemAt(args)
    self:InitEquipmentInfo()
end

function CLuaCheckGemGroupWnd:InitEquipmentInfo( )
    local transform = self.transform:Find("Anchor/EquipmentInfo")
    local equipIcon = transform:Find("Equipment/Icon"):GetComponent(typeof(CUITexture))
    local equipQuality = transform:Find("Equipment/Quality"):GetComponent(typeof(UISprite))
    local gemgroupLabel = transform:Find("Equipment/Gemgroup"):GetComponent(typeof(UILabel))

    UIEventListener.Get(equipIcon.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemInfo(CEquipmentProcessMgr.Inst.SelectEquipment.itemId, false, nil, AlignType1.Default, 0, 0, 0, 0)
    end)

    CItemInfoMgr.CloseItemInfoWnd()
    CUIManager.CloseUI(CUIResources.GemInfoWnd)

    local equip = CItemMgr.Inst:GetById(CEquipmentProcessMgr.Inst.SelectEquipment.itemId)
    gemgroupLabel.text = ""
    if equip == nil then
        return
    end

    equipIcon:LoadMaterial(equip.Icon)
    equipQuality.spriteName = CUICommonDef.GetItemCellBorder(equip.Equip.QualityType)
    
    gemgroupLabel.text = nil
    local itemId = CEquipmentProcessMgr.Inst.SelectEquipment.itemId
    Gac2Gas.RequestEquipInfoInAttrGroup(itemId,equip.Equip.ActiveWordSetIdx,CLuaCheckGemGroupWnd.m_HoleSetIndex)

    self:InitEquipHoleList(transform,equip)
end

function CLuaCheckGemGroupWnd:InitEquipHoleList(transform,item)
    local holeTemplate = transform:Find("HoleTemplate").gameObject
    holeTemplate:SetActive(false)

    local tableCmp = transform:Find("Table"):GetComponent(typeof(UITable))
    Extensions.RemoveAllChildren(tableCmp.transform)

    self.m_CommonItem = item

    local hasStone = false
    self.m_HoleList = {}
    self.m_HoleInfoList = {}

    if item == nil then
        return false
    end

    local gemGroup = GemGroup_GemGroup.GetData(item.Equip.GemGroupId)
    local gemGroupId = {}
    if gemGroup then
        for i=1,gemGroup.ConsistType.Length do
            gemGroupId[gemGroup.ConsistType[i-1]] = true
        end
    end

    local maxHole = CItemMgr.Inst:GetEquipmentMaxHole(item.TemplateId)
    
    local equipment = item.Equip

    local holeItems = {}
    local referenceIndex = {}
    for i=1,equipment.Hole do
        local jewelId = item.Equip.SecondaryHoleItems[i]
        if jewelId>0 and jewelId<100 then
            referenceIndex[item.Equip.HoleItems[jewelId]] = true
            referenceIndex[jewelId] = true
        end
    end

    local items = CLuaCheckGemGroupWnd.m_HoleSetIndex==1 and item.Equip.HoleItems or item.Equip.SecondaryHoleItems
    for i=1,items.Length-1 do
        table.insert(holeItems,items[i])
    end

    if item.Equip.HasTwoWordSet and item.Equip.ActiveHoleSetIdx == 0 and CLuaCheckGemGroupWnd.m_HoleSetIndex == 1 then
        self.m_Mark:SetActive(true)
    else
        self.m_Mark:SetActive(CLuaCheckGemGroupWnd.m_HoleSetIndex==item.Equip.ActiveHoleSetIdx)
    end

    for i=1,maxHole do
        local stone = nil
        local isReference = false
        if holeItems[i] then
            isReference = referenceIndex[holeItems[i]]
            if holeItems[i]>0 and holeItems[i]<100 then
                stone = CItemMgr.Inst:GetItemTemplate(item.Equip.HoleItems[holeItems[i]])
            else
                stone = CItemMgr.Inst:GetItemTemplate(holeItems[i])
            end
        end
        local hole = NGUITools.AddChild(tableCmp.gameObject, holeTemplate)
        hole:SetActive(true)

        local holeInfo = {
            holeIndex = i,
            stoneItemId = i <= equipment.Hole and holeItems[i] or 0,
            isReference = isReference
        }

        table.insert( self.m_HoleInfoList, holeInfo )
        table.insert( self.m_HoleList, hole )
        if i <= equipment.Hole then
            if stone ~= nil then
                if gemGroup ~= nil then
                    local jewel = Jewel_Jewel.GetData(stone.ID)
                    self:InitHoleItem(hole.transform,LuaEnumHoleStatus.availableWithGem, holeInfo, stone, gemGroupId[jewel.JewelKind],isReference)
                else
                    self:InitHoleItem(hole.transform,LuaEnumHoleStatus.availableWithGem, holeInfo, stone, false,isReference)
                end
                UIEventListener.Get(hole).onClick = DelegateFactory.VoidDelegate(function(go)
                    self:OnGemSelect(go)
                end)
                hasStone = true
            else
                self:InitHoleItem(hole.transform,LuaEnumHoleStatus.availableWithoutGem, holeInfo, nil, false)
                UIEventListener.Get(hole).onClick = DelegateFactory.VoidDelegate(function(go)
                    self:OnInlayGemClicked(go)
                end)
            end
        else
            self:InitHoleItem(hole.transform,LuaEnumHoleStatus.disable, holeInfo, nil, false)
            UIEventListener.Get(hole).onClick = DelegateFactory.VoidDelegate(function(go)
                self:OnOpenHoleClicked(go)
            end)
        end
    end
    tableCmp:Reposition()
end

function CLuaCheckGemGroupWnd:OnGemSelect( go) 
    for i,v in ipairs(self.m_HoleList) do
        self:SetHoleSelected(v.transform,go == v)

        if go == v then
            CLuaEquipMgr.SelectedEquipmentHoleInfo = self.m_HoleInfoList[i]
            CLuaGemInfoWnd.GemId = self.m_HoleInfoList[i].stoneItemId
            CLuaGemInfoWnd.IsReference = self.m_HoleInfoList[i].isReference
            if CLuaGemInfoWnd.GemId>0 and CLuaGemInfoWnd.GemId<100 then
                local commonItem = CItemMgr.Inst:GetById(CEquipmentProcessMgr.Inst.SelectEquipment.itemId)
                CLuaGemInfoWnd.GemId = commonItem.Equip.HoleItems[CLuaGemInfoWnd.GemId]
                -- CLuaGemInfoWnd.IsReference = true
            end

            CLuaEquipMgr.ShowGemInfoWnd(true, 
                function()--升级
                    CItemInfoMgr.CloseItemInfoWnd()
                    CLuaEquipMgr.m_GemUpdateSelectedEquipment = self.m_CommonItem
                    CLuaGemUpdateScrollView.SelectHoleInfo = CLuaEquipMgr.SelectedEquipmentHoleInfo
                    CLuaGemUpgradeWnd.m_HoleSetIndex = CLuaCheckGemGroupWnd.m_HoleSetIndex
                    CUIManager.ShowUI(CUIResources.GemUpgradeWnd)
                end, 
                function()--卸载
                    CItemInfoMgr.CloseItemInfoWnd()
                    local holeInfo = CLuaEquipMgr.SelectedEquipmentHoleInfo
                    local info = CEquipmentProcessMgr.Inst.SelectEquipment
                    if not CEquipmentProcessMgr.Inst:CheckOperationConflict(info) then
                        return
                    end
                    Gac2Gas.RequestCanRemoveStoneOnEquipment(EnumToInt(info.place), info.pos, info.itemId, holeInfo.holeIndex, CLuaCheckGemGroupWnd.m_HoleSetIndex)
                end)
        end
    end
end

function CLuaCheckGemGroupWnd:OnInlayGemClicked( go) 
    for i,v in ipairs(self.m_HoleList) do
        if go == v then
            self:SetHoleSelected(v.transform,true)
            --查看另一套的宝石
            local count = 0
            local itemList = CItemMgr.Inst:GetPlayerItemAtPlaceByType(EnumItemPlace.Bag, EnumItemType.Gem)
            if itemList ~= nil and itemList.Count > 0 then
                count = itemList.Count
            else
                --查看共享的宝石
                local equip = self.m_CommonItem.Equip
                if CLuaCheckGemGroupWnd.m_HoleSetIndex==2 then
                    local refLookup = {}
                    for i=1,equip.Hole do
                        local jewelId = equip.SecondaryHoleItems[i]
                        if jewelId>0 and jewelId<100 then
                            refLookup[equip.HoleItems[jewelId]] = i--这个宝石已经被引用过了
                        end
                    end
                    --显示公用宝石
                    for i=1,equip.Hole do
                        local jewelId = equip.HoleItems[i]
                        if jewelId>0 and not refLookup[jewelId] then
                            count=count+1
                        end
                    end
                else
                    for i=1,equip.Hole do
                        local jewelId = equip.SecondaryHoleItems[i]
                        if jewelId>100 then
                            count=count+1
                        end
                    end
                end
            end

            if count>0 then
                CLuaEquipMgr.SelectedEquipmentHoleInfo = self.m_HoleInfoList[i]
                CLuaEquipMgr.m_GemInlaySelectedEquipment = self.m_CommonItem
                CLuaInlayGemListWnd.m_HoleSetIndex = CLuaCheckGemGroupWnd.m_HoleSetIndex

                CUIManager.ShowUI(CUIResources.InlayGemListWnd)
            else
                if self.m_CommonItem and self.m_CommonItem.IsEquip and self.m_CommonItem.Equip.IsFaBao then
                    CItemAccessListMgr.Inst:ShowItemAccessInfo(38, false, go.transform)
                else
                    CItemAccessListMgr.Inst:ShowItemAccessInfo(37, false, go.transform)
                end
            end
        else
            self:SetHoleSelected(v.transform,false)
        end
    end
end

function CLuaCheckGemGroupWnd:OnOpenHoleClicked( go) 
    for i,v in ipairs(self.m_HoleList) do
        self:SetHoleSelected(v.transform,go==v)
        if go == v then
            CLuaEquipMgr.SelectedEquipmentHoleInfo = self.m_HoleInfoList[i]
            CUIManager.ShowUI(CUIResources.MakeEquipHoleWnd)
        end
    end
end

function CLuaCheckGemGroupWnd:InitHoleItem(transform, status, holeInfo, item, isGemGroup,isReference)
    local holeAvailabel = transform:Find("available"):GetComponent(typeof(UISprite))
    local holeDisable = transform:Find("disable").gameObject
    local iconTexture = transform:Find("icon"):GetComponent(typeof(CUITexture))
    local qualitySprite = transform:GetComponent(typeof(UISprite))
    local selectSprite = transform:Find("Select").gameObject
    local levelLabel = transform:Find("Level"):GetComponent(typeof(UILabel))

    local shareMark = transform:Find("ShareMarkSprite").gameObject
    shareMark:SetActive(false)

    if isReference then--引用类型
        shareMark:SetActive(true)
    else
        shareMark:SetActive(false)
    end

    levelLabel.text = ""
    holeAvailabel.enabled = status == LuaEnumHoleStatus.availableWithoutGem
    holeDisable:SetActive(status == LuaEnumHoleStatus.disable)
    local jewel = nil
    if item ~= nil then
        iconTexture:LoadMaterial(item.Icon)
        jewel = Jewel_Jewel.GetData(item.ID)
        levelLabel.text = SafeStringFormat3("lv.%d", jewel.JewelLevel)
    end
    qualitySprite.spriteName = isGemGroup and "common_select_01" or "common_itemcell_border"
    selectSprite:SetActive(false)
end

function CLuaCheckGemGroupWnd:SetHoleSelected(transform,select)
    local selectSprite = transform:Find("Select").gameObject
    selectSprite:SetActive(select)
end

function CLuaCheckGemGroupWnd:OnDestroy()
    CLuaCheckGemGroupWnd.m_HoleSetIndex = 0
end

function CLuaCheckGemGroupWnd:OnReplyEquipInfoInAttrGroup(itemId, wordSetIdx, holeSetIdx, gemGroupId, feiShengAdjust,item)
    local transform = self.transform:Find("Anchor/EquipmentInfo")
    local gemgroupLabel = transform:Find("Equipment/Gemgroup"):GetComponent(typeof(UILabel))
    local gemgroup = GemGroup_GemGroup.GetData(gemGroupId)
        if gemgroup ~= nil then
            gemgroupLabel.text = SafeStringFormat3(LocalString.GetString("%s(%d级)"), gemgroup.Name, gemgroup.Level)
            gemgroupLabel.color = GameSetting_Common_Wapper.Inst:GetColor(gemgroup.NameColor)
        else
            gemgroupLabel.text = LocalString.GetString("石之灵(无)")
            gemgroupLabel.color = Color.white
        end
end
