local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CEquipment = import "L10.Game.CEquipment"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local MessageWndManager = import "L10.UI.MessageWndManager"
local SoundManager = import "SoundManager"

CLuaEquipRemindWnd = class()
RegistChildComponent(CLuaEquipRemindWnd, "eupipCell", "ItemCell", GameObject)
RegistChildComponent(CLuaEquipRemindWnd, "iconTexture", "IconTexture", CUITexture)
RegistChildComponent(CLuaEquipRemindWnd, "nameLabel", "Label", UILabel)
RegistChildComponent(CLuaEquipRemindWnd, "applyButton", "ApplyButton", GameObject)
RegistChildComponent(CLuaEquipRemindWnd, "closeButton", "CloseButton", GameObject)

RegistClassMember(CLuaEquipRemindWnd, "info")
RegistClassMember(CLuaEquipRemindWnd, "item")
RegistClassMember(CLuaEquipRemindWnd, "m_SoundEvent")
RegistClassMember(CLuaEquipRemindWnd, "m_OnEquipRemindUpdateAction")

function CLuaEquipRemindWnd:Init()
	self:UpdateData()
	self:PlaySound()
end

function CLuaEquipRemindWnd:OnDestroy()
	CommonDefs.ListClear(CItemInfoMgr.EquipRemindInfoList)
	self:DestroySoundEvent()
end

function CLuaEquipRemindWnd:UpdateData()
	self.iconTexture:Clear()
	self.nameLabel.text = ""

	if CItemInfoMgr.EquipRemindInfoList == nil or CItemInfoMgr.EquipRemindInfoList.Count <= 0 then
		--Debug.LogError("item not found!");
		CUIManager.CloseUI(CIndirectUIResources.EquipRemindWnd)
		return
	end

	self.info = CItemInfoMgr.EquipRemindInfoList[CItemInfoMgr.EquipRemindInfoList.Count - 1]
	self.item = CItemMgr.Inst:GetById(self.info.EquipId)
	if self.item ~= nil then
		local iconPath = self.item.Icon

		if not System.String.IsNullOrEmpty(iconPath) then
			self.iconTexture:LoadMaterial(iconPath)
		end
		self.nameLabel.text = self.item.Equip.ColoredDisplayName
	else
		CommonDefs.ListRemoveAt(CItemInfoMgr.EquipRemindInfoList, CItemInfoMgr.EquipRemindInfoList.Count - 1)

		if CItemInfoMgr.EquipRemindInfoList.Count <= 0 then
			CUIManager.CloseUI(CIndirectUIResources.EquipRemindWnd)
			return
		end
		self:UpdateData()
		return
	end
end

function CLuaEquipRemindWnd:OnEnable()
	CommonDefs.AddOnClickListener(self.applyButton, DelegateFactory.Action_GameObject(function(go) self:OnApplyButtonClick(go) end), false)
	CommonDefs.AddOnClickListener(self.closeButton, DelegateFactory.Action_GameObject(function(go) self:OnCloseButtonClick(go) end), false)
	CommonDefs.AddOnClickListener(self.eupipCell, DelegateFactory.Action_GameObject(function(go) self:OnEquipCellClick(go) end), false)

	g_ScriptEvent:AddListener("EquipSuccess", self, "OnEquipSuccess")
	g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "OnItemUpdate")
	g_ScriptEvent:AddListener("EquipRemindUpdate", self, "UpdateData")
end

function CLuaEquipRemindWnd:OnDisable()
	g_ScriptEvent:RemoveListener("EquipSuccess", self, "OnEquipSuccess")
	g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "OnItemUpdate")
	g_ScriptEvent:RemoveListener("EquipRemindUpdate", self, "UpdateData")
end

function CLuaEquipRemindWnd:OnApplyButtonClick(go)
	if CClientMainPlayer.Inst == nil then
		return
	end
	if self.info == nil then
		return
	end
	local place = EnumItemPlace.Bag
	local pos = self.info.Pos
	if pos > 0 and self.item ~= nil then
		local item_pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(place, self.item.Id)
		if item_pos == 0 then
			--TODO 给出提示
			self:OnEquipSuccessImpl(pos, self.item.Id)
			return
		end

		pos = item_pos
		if self.item.IsEquip and not self.item.IsBinded and self.item.Equip:IsPrecious() and CEquipment.GetMainPlayerIsFit(self.item.TemplateId, false) then
			local message = g_MessageMgr:FormatMessage("PRECIOUS_EQUIP_CONFIRM", self.item.ColoredName)
			MessageWndManager.ShowConfirmMessage(message, 5, false, DelegateFactory.Action(function () 
				Gac2Gas.RequestEquipItemReminded(EnumToInt(place), pos, self.item.Id)
			end), nil)
		else
			Gac2Gas.RequestEquipItemReminded(EnumToInt(place), pos, self.item.Id)
		end
	else
		--TODO ERROR
		CUIManager.CloseUI(CIndirectUIResources.EquipRemindWnd)
	end
end

function CLuaEquipRemindWnd:OnCloseButtonClick(go)
	if CItemInfoMgr.EquipRemindInfoList.Count > 0 then
		CommonDefs.ListRemoveAt(CItemInfoMgr.EquipRemindInfoList, CItemInfoMgr.EquipRemindInfoList.Count - 1)
	end
	self:UpdateData()
end

function CLuaEquipRemindWnd:OnEquipSuccess(args)
	local pos = args[0]
	local equipId = args[1]
	self:OnEquipSuccessImpl(pos, equipId)
end

function CLuaEquipRemindWnd:OnEquipSuccessImpl(pos, equipId)
	if CItemInfoMgr.EquipRemindInfoList.Count > 0 then
		CommonDefs.ListRemoveAt(CItemInfoMgr.EquipRemindInfoList, CItemInfoMgr.EquipRemindInfoList.Count - 1)
	end

	if CItemInfoMgr.EquipRemindInfoList.Count <= 0 then
		CUIManager.CloseUI(CIndirectUIResources.EquipRemindWnd)
	else
		self:UpdateData()
	end
end

function CLuaEquipRemindWnd:OnEquipCellClick()
	CItemInfoMgr.ShowLinkItemInfo(self.info.EquipId, true, nil, AlignType.Default, 0, 0, 0, 0)
end

function CLuaEquipRemindWnd:OnSendItem(args)
	local itemId = args[0]
	-- 刷新一下物品数量
	self:OnItemUpdate()

	if self.info == nil or self.info.EquipId ~= itemId then
		return
	end

	self.item = CItemMgr.Inst:GetById(self.info.EquipId)
	if self.item ~= nil then
		local iconPath = self.item.Icon

		if not System.String.IsNullOrEmpty(iconPath) then
			self.iconTexture:LoadMaterial(iconPath)
		end
		self.nameLabel.text = self.item.Equip.ColoredDisplayName
	end
end

function CLuaEquipRemindWnd:OnItemUpdate(args)
	local needUpdate = false
	-- 判断是不是需要显示的装备
	for i = CItemInfoMgr.EquipRemindInfoList.Count -1 ,0, -1 do
		local itemId = CItemInfoMgr.EquipRemindInfoList[i].EquipId
		if CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId) <= 0  then
			CommonDefs.ListRemoveAt(CItemInfoMgr.EquipRemindInfoList, i)
			needUpdate = true
		end
	end

	if needUpdate then
		self:UpdateData()
	end
end

function CLuaEquipRemindWnd:PlaySound()
	self:DestroySoundEvent()
	self.m_SoundEvent = SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.NewItem, Vector3.zero)
end

function CLuaEquipRemindWnd:DestroySoundEvent()
	if self.m_SoundEvent then
		SoundManager.Inst:StopSound(self.m_SoundEvent)
		self.m_SoundEvent = nil
	end
end
