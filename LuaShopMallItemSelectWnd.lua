local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnRadioBox = import "L10.UI.QnRadioBox"
local QnTableView = import "L10.UI.QnTableView"
local CShopMallItemPreviewer = import "L10.UI.CShopMallItemPreviewer"
local GameObject = import "UnityEngine.GameObject"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local ShopMallTemlate = import "L10.UI.ShopMallTemlate"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"

LuaShopMallItemSelectWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaShopMallItemSelectWnd, "QnRadioBox", "QnRadioBox", QnRadioBox)
RegistChildComponent(LuaShopMallItemSelectWnd, "QnTableView", "QnTableView", QnTableView)
RegistChildComponent(LuaShopMallItemSelectWnd, "ItemPreviewer", "ItemPreviewer", CShopMallItemPreviewer)
RegistChildComponent(LuaShopMallItemSelectWnd, "QnButton", "QnButton", GameObject)
RegistChildComponent(LuaShopMallItemSelectWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaShopMallItemSelectWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)

--@endregion RegistChildComponent end
RegistClassMember(LuaShopMallItemSelectWnd,"m_LimitPurchaseQuantityItemIds")
RegistClassMember(LuaShopMallItemSelectWnd,"m_DynamicOnShelfItemsSetData")
RegistClassMember(LuaShopMallItemSelectWnd,"m_CurrentItems")
RegistClassMember(LuaShopMallItemSelectWnd,"m_SelectRow")

function LuaShopMallItemSelectWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.QnButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnQnButtonClick()
	end)
    --@endregion EventBind end
end

function LuaShopMallItemSelectWnd:Init()
	self.m_DynamicOnShelfItemsSetData = {}
	self.QnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
	    self:OnQnRadioBoxSelected(btn, index)
	end)
	self.m_LimitPurchaseQuantityItemIds = g_LuaUtil:StrSplit(Mall_Setting.GetData("LimitPurchaseQuantityItemIds").Value,";")
	for _,itemId in pairs(CLuaShopMallMgr.m_DynamicOnShelfItems) do
		local mallData = Mall_LingYuMall.GetData(itemId)
		if mallData then
			if not self.m_DynamicOnShelfItemsSetData[mallData.SubCategory] then
				self.m_DynamicOnShelfItemsSetData[mallData.SubCategory] = {}
			end
			table.insert(self.m_DynamicOnShelfItemsSetData[mallData.SubCategory],mallData)
		end
	end
	self.QnCostAndOwnMoney:SetCost(0)
	self.QnTableView.m_DataSource=DefaultTableViewDataSource.Create(function()
        return self.m_CurrentItems and self.m_CurrentItems.Count or 0
    end, function(item, index)
        self:InitItem(item, index)
    end)
	self.QnTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
	self.QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function (value) 
		self:OnQnIncreseAndDecreaseButtonValueChanged(value)
	end)
	self.QnRadioBox:ChangeTo(0, true)
end

function LuaShopMallItemSelectWnd:InitItem(item, index)
	if not self.m_CurrentItems then
		item.gameObject:SetActive(false)
		return
	end
	item:UpdateData(self.m_CurrentItems[index], EnumMoneyType.LingYu, false, false)
	item.OnClickItemTexture = DelegateFactory.Action(function ()
		self.QnTableView:SetSelectRow(index, true)
	end)
end

function  LuaShopMallItemSelectWnd:OnSelectAtRow(row)
	self.m_SelectRow = row
	self:UpdateTotalPrice()
	self.ItemPreviewer:UpdateData(nil, 0, "")
	local template = self.m_CurrentItems[row]
	self.QnCostAndOwnMoney:SetType(template.CanUseBindJade > 0 and EnumMoneyType.LingYu_WithBind or EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
	local maxCount = 999
	local itemData = Item_Item.GetData(template.ItemId)
	if itemData then
		maxCount = math.min(maxCount,itemData.Overlap)
	end
	if template.AvalibleCount >= 0 then
		local avaliableCount = math.max(1, template.AvalibleCount)
		maxCount = math.min(maxCount, avaliableCount)
	end
	if template.SubCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ShiZhuang then
		maxCount = 1
	elseif template.SubCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ZuoQi then
		maxCount = 1
	elseif template.SubCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.BackPendant then
		maxCount = 1
	end
	for _,itemId in pairs(self.m_LimitPurchaseQuantityItemIds) do
		if itemId == tostring(template.ItemId) then
			maxCount = 1
		end
	end
	self.QnIncreseAndDecreaseButton:SetMinMax(1, maxCount, 1)
	self.ItemPreviewer:UpdateData(template, 0, "")
end

function LuaShopMallItemSelectWnd:UpdateTotalPrice()
	local totalprice = 0
    if self.m_CurrentItems and self.m_SelectRow and self.m_CurrentItems.Count > self.m_SelectRow and self.m_SelectRow >= 0 then
        totalprice = self.m_CurrentItems[self.m_SelectRow].Price * self.QnIncreseAndDecreaseButton:GetValue()
    end
    self.QnCostAndOwnMoney:SetCost(totalprice)
end

function LuaShopMallItemSelectWnd:OnQnIncreseAndDecreaseButtonValueChanged(value)
	self:UpdateTotalPrice()
end

function LuaShopMallItemSelectWnd:OnEnable()
	CShopMallMgr.Init()
end

function LuaShopMallItemSelectWnd:OnDisable()
end

--@region UIEvent

function  LuaShopMallItemSelectWnd:OnQnButtonClick()
	CLuaShopMallMgr.m_ShopMallSelectItem = {
		itemId = self.m_CurrentItems[self.m_SelectRow].ItemId,
		num = self.QnIncreseAndDecreaseButton:GetValue(),
		price = self.QnCostAndOwnMoney:GetCost()
	}
	g_ScriptEvent:BroadcastInLua("OnShopMallItemSelectWndSelectItem")
	CUIManager.CloseUI(CLuaUIResources.ShopMallItemSelectWnd)
end

function  LuaShopMallItemSelectWnd:OnQnRadioBoxSelected(btn, index)
	self.QnIncreseAndDecreaseButton:SetValue(1)
	self.m_CurrentItems = CShopMallMgr.GetLingYuMallInfo(index, 0)
	local list = self.m_DynamicOnShelfItemsSetData[index + 1]
	if list then
		for _,v in pairs(list) do
			local template = ShopMallTemlate()
			template.ItemId = v.ID
			template.Price = v.Jade
			template.Category = v.Category
			template.SubCategory = v.SubCategory
			template.PreSell = false
			template.Status = v.Status
			template.Index = v.Index
			template.FeiShengIndex = v.FeiShengIndex
			template.Discount = v.Discount
			template.AvalibleCount = -1
			template.CanZengSong = v.CanZengSong
			template.DeleteTime = v.DeleteTime
			template.CanUseBindJade = v.CanUseBindJade
			CommonDefs.ListAdd(self.m_CurrentItems, typeof(ShopMallTemlate), template)
		end
	end
	for i = self.m_CurrentItems.Count - 1, 0, -1 do
		local template = self.m_CurrentItems[i]
		local mallData = Mall_LingYuMall.GetData(template.ItemId)
		local canZengSongSectItem = mallData and (mallData.CanZengSongSectItem > 0)
		if (template.CanZengSong == 0) and not canZengSongSectItem then
			CommonDefs.ListRemoveAt(self.m_CurrentItems, i)
		end
	end
	self.QnTableView:ReloadData(true,true)
	self.QnTableView:SetSelectRow(0, true)
end

--@endregion UIEvent

