local CChatLinkMgr=import "CChatLinkMgr"

local CBaseWnd = import "L10.UI.CBaseWnd"
local UIPanel = import "UIPanel"
local CUIFx = import "L10.UI.CUIFx"
local Vector2 = import "UnityEngine.Vector2"
local UIEventListener = import "UIEventListener"
local CTickMgr = import "L10.Engine.CTickMgr"
local ETickType = import "L10.Engine.ETickType"
local TweenAlpha = import "TweenAlpha"
local UILabel = import "UILabel"
local Camera = import "UnityEngine.Camera"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Subtitle_Subtitle = import "L10.Game.Subtitle_Subtitle"
local Time = import "UnityEngine.Time"
local ShiJieShiJian_Setting = import "L10.Game.ShiJieShiJian_Setting"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CommonDefs = import "L10.Game.CommonDefs"

LuaWipeLightEffectWnd = class()
RegistClassMember(LuaWipeLightEffectWnd,"closeBtn")
RegistClassMember(LuaWipeLightEffectWnd,"lightFx")
RegistClassMember(LuaWipeLightEffectWnd,"bg")
RegistClassMember(LuaWipeLightEffectWnd,"displayCamera")
RegistClassMember(LuaWipeLightEffectWnd,"subtitleLabel")
RegistClassMember(LuaWipeLightEffectWnd,"descLabel")
RegistClassMember(LuaWipeLightEffectWnd,"effectTweenAlpha")
RegistClassMember(LuaWipeLightEffectWnd,"smallLightFxPath")
RegistClassMember(LuaWipeLightEffectWnd,"middleLightFxPath")
RegistClassMember(LuaWipeLightEffectWnd,"largeLightFxPath")
RegistClassMember(LuaWipeLightEffectWnd,"wipeTimesToShowMiddleLight")
RegistClassMember(LuaWipeLightEffectWnd,"wipeTimesToShowLargeLight")
RegistClassMember(LuaWipeLightEffectWnd,"currentWipeTimes")
RegistClassMember(LuaWipeLightEffectWnd,"lastDragDir")
RegistClassMember(LuaWipeLightEffectWnd,"closeTick")
RegistClassMember(LuaWipeLightEffectWnd,"fxPathToPlay")

--字幕
RegistClassMember(LuaWipeLightEffectWnd,"nextSubtitleId")
RegistClassMember(LuaWipeLightEffectWnd,"aliveDuration")
RegistClassMember(LuaWipeLightEffectWnd,"fadeInTime")
RegistClassMember(LuaWipeLightEffectWnd,"fadeOutTime")
RegistClassMember(LuaWipeLightEffectWnd,"startTime")
--完成进度标记
RegistClassMember(LuaWipeLightEffectWnd,"wipeComplete")
RegistClassMember(LuaWipeLightEffectWnd,"subtitleComplete")
RegistClassMember(LuaWipeLightEffectWnd,"needClose")

function LuaWipeLightEffectWnd:Awake()
    self.gameObject:GetComponent(typeof(UIPanel)).IgnoreIphoneXMargin = true
end

function LuaWipeLightEffectWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaWipeLightEffectWnd:Init()
	self.closeBtn = self.transform:Find("Camera/Panel/CloseButton").gameObject
	self.lightFx = self.transform:Find("Camera/Panel/TweenAlpha/LightFx"):GetComponent(typeof(CUIFx))
	self.bg = self.transform:Find("Camera/Panel/BlackBg").gameObject
	self.displayCamera = self.transform:Find("Camera"):GetComponent(typeof(Camera))
	self.displayCamera.rect = CUICommonDef.GetCurrentCameraRect()
	self.subtitleLabel = self.transform:Find("Camera/Panel/SubtitleLabel"):GetComponent(typeof(UILabel))
	self.descLabel = self.transform:Find("Camera/Panel/DescLabel"):GetComponent(typeof(UILabel))
	self.effectTweenAlpha = self.transform:Find("Camera/Panel/TweenAlpha"):GetComponent(typeof(TweenAlpha))
	self.effectTweenAlpha.enabled = false
	self.effectTweenAlpha:SetOnFinished(DelegateFactory.Callback(function()
			self:OnTeenAlphaFinished()
		end))

	if CTaskMgr.Inst.m_WipeLightEffectShowType == 1 then
		--从小灯到大灯
		self.smallLightFxPath = "Fx/UI/Prefab/UI_heiping_deng01.prefab"
		self.middleLightFxPath = "Fx/UI/Prefab/UI_heiping_deng02.prefab"
		self.largeLightFxPath = "Fx/UI/Prefab/UI_heiping_deng03.prefab"
		self.wipeTimesToShowMiddleLight = ShiJieShiJian_Setting.GetData().WipeLightOneToTwo
		self.wipeTimesToShowLargeLight = ShiJieShiJian_Setting.GetData().WipeLightTwoToThree
	elseif CTaskMgr.Inst.m_WipeLightEffectShowType == 2 then
		--从大灯到小灯
		self.smallLightFxPath = "Fx/UI/Prefab/UI_heiping_deng03.prefab"
		self.middleLightFxPath = "Fx/UI/Prefab/UI_heiping_deng02.prefab"
		self.largeLightFxPath = "Fx/UI/Prefab/UI_heiping_deng01.prefab"
		self.wipeTimesToShowMiddleLight = ShiJieShiJian_Setting.GetData().WipeLightTwoToThree
		self.wipeTimesToShowLargeLight = ShiJieShiJian_Setting.GetData().WipeLightOneToTwo
	else
		--其他值先按1处理
		self.smallLightFxPath = "Fx/UI/Prefab/UI_heiping_deng01.prefab"
		self.middleLightFxPath = "Fx/UI/Prefab/UI_heiping_deng02.prefab"
		self.largeLightFxPath = "Fx/UI/Prefab/UI_heiping_deng03.prefab"
		self.wipeTimesToShowMiddleLight = ShiJieShiJian_Setting.GetData().WipeLightOneToTwo
		self.wipeTimesToShowLargeLight = ShiJieShiJian_Setting.GetData().WipeLightTwoToThree
	end

	self.currentWipeTimes = 0
	self.lastDragDir = Vector2.zero
	self.fxPathToPlay = nil
	self.descLabel.text = g_MessageMgr:FormatMessage(ShiJieShiJian_Setting.GetData().WipeLightMessageTip)

	self.lightFx:LoadFx(self.smallLightFxPath)

	CommonDefs.AddOnClickListener(self.closeBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

	UIEventListener.Get(self.bg).onDragStart = LuaUtils.VoidDelegate(function(go)
        self:OnDragStart()
    end)
	UIEventListener.Get(self.bg).onDrag = LuaUtils.VectorDelegate(function(go,vec)
        self:OnDrag(vec)
    end)
    UIEventListener.Get(self.bg).onDragEnd = LuaUtils.VoidDelegate(function(go)
        self:OnDragEnd()
    end)

    self:CancelTick()

    self.wipeComplete = false
    self.subtitleComplete = false
    self.needClose = false

	self.aliveDuration = 0
	self.fadeInTime = 0
	self.fadeOutTime = 0
	self.subtitleLabel.text = nil
	self.subtitleLabel.alpha = 1
	self.nextSubtitleId = CTaskMgr.Inst.m_WipeLightEffectSubtitleId
	if not Subtitle_Subtitle.Exists(self.nextSubtitleId) then
		self.subtitleComplete = true
	end
	self.startTime = Time.realtimeSinceStartup
	self:ShowSubtitle()

end

function LuaWipeLightEffectWnd:OnDragStart()
	self.lastDragDir = Vector2.zero
end

function LuaWipeLightEffectWnd:OnDrag(delta)
	if (self.lastDragDir.x == 0 and self.lastDragDir.y == 0) or (Vector2.Dot(self.lastDragDir, delta) < 0) then
		self.currentWipeTimes = self.currentWipeTimes + 1
		if self.currentWipeTimes == self.wipeTimesToShowMiddleLight then
			self:DoTweenAlpha(self.middleLightFxPath)
		elseif self.currentWipeTimes == self.wipeTimesToShowMiddleLight + self.wipeTimesToShowLargeLight then
			self:DoTweenAlpha(self.largeLightFxPath)
			self.wipeComplete = true
		elseif self.currentWipeTimes > self.wipeTimesToShowMiddleLight + self.wipeTimesToShowLargeLight then
			self.wipeComplete = true
		end
	end
	self.lastDragDir = delta.normalized
end

function LuaWipeLightEffectWnd:OnDragEnd()
	-- body
end

function LuaWipeLightEffectWnd:DoTweenAlpha(newFxPath)
	if self.effectTweenAlpha then
		self.fxPathToPlay = newFxPath
		TweenAlpha.Begin(self.effectTweenAlpha.gameObject, 0.5, 0)
	end
end

function LuaWipeLightEffectWnd:OnTeenAlphaFinished()
	if self.effectTweenAlpha.value < 0.01 then
		self.lightFx:LoadFx(self.fxPathToPlay)
		self.fxPathToPlay = nil
		TweenAlpha.Begin(self.effectTweenAlpha.gameObject, 1, 1)
	end
end

function LuaWipeLightEffectWnd:WaitForSecondsAndClose()
	self:CancelTick()
	self.closeTick = CTickMgr.Register(DelegateFactory.Action(function ()
		self.closeTick = nil
		CTaskMgr.Inst:FinishWipeLightEffectTask()
		self:Close()
	end), ShiJieShiJian_Setting.GetData().WipeLightDuration * 1000, ETickType.Once)
end

function LuaWipeLightEffectWnd:CompleteAndClose()
	self:CancelTick()
	CTaskMgr.Inst:FinishWipeLightEffectTask()
	self:Close()
end

function LuaWipeLightEffectWnd:CancelTick()
	if self.closeTick then
		invoke(self.closeTick)
		self.closeTick = nil
	end
end

function LuaWipeLightEffectWnd:OnDisable()
	self:CancelTick()
	self.needClose = false
end

function LuaWipeLightEffectWnd:Update()

	if self.wipeComplete and self.subtitleComplete then
		if not self.needClose then
			self.needClose = true
			self:WaitForSecondsAndClose()
		end
		return
	end

	if self.subtitleComplete then
		return
	end

	local time = Time.realtimeSinceStartup - self.startTime
	if time <= self.fadeInTime then
		-- fade in
		self.subtitleLabel.alpha = time / self.fadeInTime
	elseif self.aliveDuration - time <= self.fadeOutTime and time <= self.aliveDuration then
		-- fade out
		self.subtitleLabel.alpha = (self.aliveDuration - time) / self.fadeOutTime 
	elseif time > self.aliveDuration or time < 0 then
		if Subtitle_Subtitle.Exists(self.nextSubtitleId) then
			self:ShowSubtitle()
		else
			self.subtitleComplete = true
			if self.wipeComplete then
				--如果字幕结束的时候，擦屏也结束了，就直接关闭窗口
				self:CompleteAndClose()
			end
		end
	else
		self.subtitleLabel.alpha = 1
	end
end

function LuaWipeLightEffectWnd:ShowSubtitle()
	if Subtitle_Subtitle.Exists(self.nextSubtitleId) then
		local subtitle = Subtitle_Subtitle.GetData(self.nextSubtitleId)
		self.aliveDuration = subtitle.AliveDuration
		self.fadeInTime = subtitle.FadeInTime
		self.fadeOutTime = subtitle.FadeOutTime
		self.subtitleLabel.text = CChatLinkMgr.TranslateToNGUIText(subtitle.Content, false)
		self.subtitleLabel.alpha = 0
		self.nextSubtitleId = subtitle.Next
		self.startTime = Time.realtimeSinceStartup
	end
end

return LuaWipeLightEffectWnd