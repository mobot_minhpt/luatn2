
local CPlayerHeadInfoItem = import "L10.UI.CPlayerHeadInfoItem"
local CPetHeadInfoItem = import "L10.UI.CPetHeadInfoItem"
local CScene=import "L10.Game.CScene"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CForcesMgr = import "L10.Game.CForcesMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local QualityColor = import "L10.Game.QualityColor"
local EnumQualityType = import "L10.Game.EnumQualityType"
local Color = import "UnityEngine.Color"
local CClientPet = import "L10.Game.CClientPet"
local Time = import "UnityEngine.Time"

CPlayerHeadInfoItem.m_UpdateTitleBoardAction=function(headInfoItem, objId)
    if CScene.MainScene then
        local gamePlayId=CScene.MainScene.GamePlayDesignId

        if gamePlayId == 51100803 or 51100804 == gamePlayId then
            local obj = CClientObjectMgr.Inst:GetObject(objId)
            if obj then
                local pid = nil
                if TypeAs(obj, typeof(CClientMainPlayer)) then
                    pid = obj.Id
                elseif TypeAs(obj, typeof(CClientOtherPlayer)) then
                    pid = obj.PlayerId
                end
                if pid then
                    local force = CForcesMgr.Inst:GetPlayerForce(pid)
                    if force > 0 then
                        local force2name = {[1] = LocalString.GetString("红队"), [2] = LocalString.GetString("黄队"), [3] = LocalString.GetString("蓝队"), [4] = LocalString.GetString("绿队"), }
                        headInfoItem.titleBoard.gameObject:SetActive(true)
                        headInfoItem.titleBoard.displayName = force2name[force] or ""
                        headInfoItem.titleBoard.fontSize = headInfoItem.defaultTitleFontSize
                        headInfoItem.titleBoard.fontColor = QualityColor.GetRGBValue((EnumQualityType.White))
                        headInfoItem.titleBoard.bgVisible = false

                        local force2name = {[1] = Color.red, [2] = Color.yellow, [3] = Color.blue, [4] = Color.green, }
                        headInfoItem.titleBoard.fontColor = force2name[force] or Color.white
                    end
                end
            end
        elseif gamePlayId == 51100444 then  -- 全民pk
            headInfoItem.titleBoard.gameObject:SetActive(false)
        elseif LuaShiTuMgr:IsInJianLaJiPlay() then
            --师徒捡垃圾玩法中根据buffid来显示不同称号信息
            local clientObj = CClientObjectMgr.Inst:GetObject(headInfoItem:FollowId())
            local buffTitleTbl = LuaShiTuMgr:GetBuffTitleTbl()
            if clientObj and buffTitleTbl then
                for buffId, buffName in pairs(buffTitleTbl) do
                    if CommonDefs.DictContains_LuaCall(clientObj.BuffProp.Buffs, buffId) then
                        headInfoItem.titleBoard.gameObject:SetActive(true)
                        headInfoItem.titleBoard.displayName = buffName
                        headInfoItem.titleBoard.fontSize = headInfoItem.defaultTitleFontSize
                        headInfoItem.titleBoard.fontColor = QualityColor.GetRGBValue((EnumQualityType.White))
                        headInfoItem.titleBoard.bgVisible = false
                        break
                    end
                end
            end
        else -- 通用玩法管理
            local obj = CClientObjectMgr.Inst:GetObject(objId)
            -- 处理分身的一种特殊情况
            if TypeIs(obj, typeof(CClientPet)) then
                objId = obj.m_OwnerEngineId
            end
            local playerId = obj and obj.BasicProp and obj.BasicProp.Id
            local info = LuaGamePlayMgr:GetNameBoardInfo(objId) or LuaGamePlayMgr:GetPlayerNameBoardInfo(playerId)
            if info then
                headInfoItem.nameBoard.gameObject:SetActive(info.show == nil and true or info.show) 
                headInfoItem.nameBoard.displayName = info.displayName or ""
                headInfoItem.nameBoard.fontSize = info.fontSize or headInfoItem.defaultNameFontSize
                headInfoItem.nameBoard.fontColor = info.fontColor or obj.NameDisplayColor
                if info.bgName then headInfoItem.nameBoard:SetBg(info.bgName) end
                headInfoItem.nameBoard.bgVisible = info.bgVisible or false
            end
            info = LuaGamePlayMgr:GetTitleBoardInfo(objId) or LuaGamePlayMgr:GetPlayerTitleBoardInfo(playerId)
            if info then
                headInfoItem.titleBoard.gameObject:SetActive(info.show == nil and true or info.show) 
                headInfoItem.titleBoard.displayName = info.displayName or ""
                headInfoItem.titleBoard.fontSize = info.fontSize or headInfoItem.defaultTitleFontSize
                headInfoItem.titleBoard.fontColor = info.fontColor or NGUIText.ParseColor24("ffffff", 0)
                if info.bgName then headInfoItem.titleBoard:SetBg(info.bgName) end
                headInfoItem.titleBoard.bgVisible = info.bgVisible or false
            end
        end
        -- 这两个在Nameboard下面的适配是OnEnable，需要处理下
        headInfoItem.PKIcon:UpdateAnchorsInternal(Time.frameCount)
        headInfoItem.GuanNingEquipRankIcon:UpdateAnchorsInternal(Time.frameCount)
    end
end

CPetHeadInfoItem.m_UpdateLingShouHeadInfoAction = function(headInfoItem, lingshou)
    local defaultNameBg = "headinfownd_lingshou_attack_namebg"

    local ownerId = lingshou.m_OwnerEngineId
    local ownerInfo = LuaGamePlayMgr:GetNameBoardInfo(ownerId)
    local info = LuaGamePlayMgr:GetNameBoardInfo(lingshou.EngineId) or (ownerInfo and ownerInfo.lingShouInfo)
    if info then
        headInfoItem.nameBoard.gameObject:SetActive(info.show == nil and true or info.show) 
        headInfoItem.nameBoard.displayName = info.displayName or ""
        headInfoItem.nameBoard.fontSize = info.fontSize or headInfoItem.defaultNameFontSize
        headInfoItem.nameBoard.fontColor = info.fontColor or lingshou.NameDisplayColor
        if info.bgName then headInfoItem.nameBoard:SetBg(info.bgName) end
        headInfoItem.nameBoard.bgVisible = info.bgVisible or false
    end
    ownerInfo = LuaGamePlayMgr:GetTitleBoardInfo(ownerId)
    info = LuaGamePlayMgr:GetTitleBoardInfo(lingshou.EngineId) or (ownerInfo and ownerInfo.lingShouInfo)
    if info then
        headInfoItem.titleBoard.gameObject:SetActive(info.show == nil and true or info.show) 
        headInfoItem.titleBoard.displayName = info.displayName or ""
        headInfoItem.titleBoard.fontSize = info.fontSize or headInfoItem.defaultTitleFontSize
        headInfoItem.titleBoard.fontColor = info.fontColor or NGUIText.ParseColor24("ffffff", 0)
        if info.bgName then headInfoItem.titleBoard:SetBg(info.bgName) end
        headInfoItem.titleBoard.bgVisible = info.bgVisible or false
    end
end

CPetHeadInfoItem.m_UpdatePetHeadInfoAction = function(headInfoItem, pet)
    local defaultNameBg = "headinfownd_lingshou_attack_namebg"

    local ownerId = pet.m_OwnerEngineId
    local ownerInfo = LuaGamePlayMgr:GetNameBoardInfo(ownerId)
    local info = LuaGamePlayMgr:GetNameBoardInfo(pet.EngineId) or (ownerInfo and ownerInfo.petInfo)
    if info then
        headInfoItem.nameBoard.gameObject:SetActive(info.show == nil and true or info.show) 
        headInfoItem.nameBoard.displayName = info.displayName or ""
        headInfoItem.nameBoard.fontSize = info.fontSize or headInfoItem.defaultNameFontSize
        headInfoItem.nameBoard.fontColor = info.fontColor or pet.NameDisplayColor
        if info.bgName then headInfoItem.nameBoard:SetBg(info.bgName) end
        headInfoItem.nameBoard.bgVisible = info.bgVisible or false
    end
    if pet.IsShadow and ownerInfo and ownerInfo.hideShadowPetName then
        headInfoItem.nameBoard.gameObject:SetActive(false)
    end
    ownerInfo = LuaGamePlayMgr:GetTitleBoardInfo(ownerId)
    info = LuaGamePlayMgr:GetTitleBoardInfo(pet.EngineId) or (ownerInfo and ownerInfo.petInfo)
    if info then
        headInfoItem.titleBoard.gameObject:SetActive(info.show == nil and true or info.show) 
        headInfoItem.titleBoard.displayName = info.displayName or ""
        headInfoItem.titleBoard.fontSize = info.fontSize or headInfoItem.defaultTitleFontSize
        headInfoItem.titleBoard.fontColor = info.fontColor or NGUIText.ParseColor24("ffffff", 0)
        if info.bgName then headInfoItem.titleBoard:SetBg(info.bgName) end
        headInfoItem.titleBoard.bgVisible = info.bgVisible or false
    end
end
