local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CItemMgr = import "L10.Game.CItemMgr"
local Animation = import "UnityEngine.Animation"

LuaQingMing2023PVPResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQingMing2023PVPResultWnd, "RankLabel", UILabel)
RegistChildComponent(LuaQingMing2023PVPResultWnd, "InfoTable", GameObject)
RegistChildComponent(LuaQingMing2023PVPResultWnd, "ItemCell1", CQnReturnAwardTemplate)
RegistChildComponent(LuaQingMing2023PVPResultWnd, "ItemCell2", CQnReturnAwardTemplate)
RegistChildComponent(LuaQingMing2023PVPResultWnd, "NoRewardLabel", UILabel)
RegistChildComponent(LuaQingMing2023PVPResultWnd, "PlayerInfo", GameObject)
RegistChildComponent(LuaQingMing2023PVPResultWnd, "ShareBtn", GameObject)
RegistChildComponent(LuaQingMing2023PVPResultWnd, "Win", GameObject)
RegistChildComponent(LuaQingMing2023PVPResultWnd, "Fail", GameObject)

RegistClassMember(LuaQingMing2023PVPResultWnd, "m_ShareTick")
--@endregion RegistChildComponent end

function LuaQingMing2023PVPResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)
end

function LuaQingMing2023PVPResultWnd:Init()
    self.PlayerInfo:SetActive(false)
    local animation = self.transform:GetComponent(typeof(Animation))
    if LuaQingMing2023Mgr.m_PVP_Result.rank == 1 then
        animation:Play("conmmon_resultwnd_half_win_1")
    else
        animation:Play("conmmon_resultwnd_half_loss_1")
    end
    self:InitInfo()
    self:InitReward()
end

function LuaQingMing2023PVPResultWnd:InitInfo()
    self.RankLabel.text = tostring(LuaQingMing2023Mgr.m_PVP_Result.rank)
    if LuaQingMing2023Mgr.m_PVP_Result.subPlayInfo and #LuaQingMing2023Mgr.m_PVP_Result.subPlayInfo == 3 then
        self.InfoTable.transform:Find("Game1"):GetComponent(typeof(UILabel)).text = LuaQingMing2023Mgr.m_PVP_Result.subPlayInfo[1].Name
        self.InfoTable.transform:Find("Game2"):GetComponent(typeof(UILabel)).text = LuaQingMing2023Mgr.m_PVP_Result.subPlayInfo[2].Name
        self.InfoTable.transform:Find("Game3"):GetComponent(typeof(UILabel)).text = LuaQingMing2023Mgr.m_PVP_Result.subPlayInfo[3].Name
        self.InfoTable.transform:Find("Game1/ScoreLabel"):GetComponent(typeof(UILabel)).text = tostring(LuaQingMing2023Mgr.m_PVP_Result.subPlayInfo[1].Score)
        self.InfoTable.transform:Find("Game2/ScoreLabel"):GetComponent(typeof(UILabel)).text = tostring(LuaQingMing2023Mgr.m_PVP_Result.subPlayInfo[2].Score)
        self.InfoTable.transform:Find("Game3/ScoreLabel"):GetComponent(typeof(UILabel)).text = tostring(LuaQingMing2023Mgr.m_PVP_Result.subPlayInfo[3].Score)
        self.InfoTable.transform:Find("Total/ScoreLabel"):GetComponent(typeof(UILabel)).text = tostring(LuaQingMing2023Mgr.m_PVP_Result.totalScore)
    end
end

function LuaQingMing2023PVPResultWnd:InitReward()
    local setData = QingMing2023_PVPSetting.GetData()
    local rewardList = {}
    if LuaQingMing2023Mgr.m_PVP_Result.winnerAward == 1 then table.insert(rewardList, setData.WinAwardItemId) end
    if LuaQingMing2023Mgr.m_PVP_Result.engageAward == 1 then table.insert(rewardList, setData.EngageAwardItemId) end

    self.NoRewardLabel.gameObject:SetActive(#rewardList == 0)
    self.ItemCell1.gameObject:SetActive(#rewardList > 0)
    self.ItemCell2.gameObject:SetActive(#rewardList > 1)
    if #rewardList == 0 then
        if LuaQingMing2023Mgr.m_PVP_Result.winnerAward == 2 and LuaQingMing2023Mgr.m_PVP_Result.engageAward == 2 then
            self.NoRewardLabel.text = g_MessageMgr:FormatMessage("QINGMING2023_PVP_NOAWARD_FULL")
        elseif LuaQingMing2023Mgr.m_PVP_Result.winnerAward == 0 and LuaQingMing2023Mgr.m_PVP_Result.engageAward == 2 then
            self.NoRewardLabel.text = g_MessageMgr:FormatMessage("QINGMING2023_PVP_NOAWARD_RANK_LIMIT")
        else
            self.NoRewardLabel.text = g_MessageMgr:FormatMessage("QINGMING2023_PVP_NOAWARD_SCORE_LIMIT")
        end
    end
    if rewardList[1] then self.ItemCell1:Init(CItemMgr.Inst:GetItemTemplate(rewardList[1]), 1) end
    if rewardList[2] then self.ItemCell2:Init(CItemMgr.Inst:GetItemTemplate(rewardList[2]), 1) end
end

--@region UIEvent
function LuaQingMing2023PVPResultWnd:OnShareBtnClick()
    self.PlayerInfo:SetActive(true)
	CUICommonDef.CaptureScreenUIAndShare(CLuaUIResources.QingMing2023PVPResultWnd, self.ShareBtn)
end
--@endregion UIEvent
