local CPlayerInfoMgr=import "CPlayerInfoMgr"
local CRankData=import "L10.UI.CRankData"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaYuanXiaoDefenseRankWnd = class()

RegistClassMember(LuaYuanXiaoDefenseRankWnd,"m_TableView")
RegistClassMember(LuaYuanXiaoDefenseRankWnd,"m_RankList")
RegistClassMember(LuaYuanXiaoDefenseRankWnd, "m_MyRank")
RegistClassMember(LuaYuanXiaoDefenseRankWnd, "m_EmptyTip")
RegistClassMember(LuaYuanXiaoDefenseRankWnd, "m_MainPlayerInfo")
RegistClassMember(LuaYuanXiaoDefenseRankWnd, "m_MyRankLabel")
RegistClassMember(LuaYuanXiaoDefenseRankWnd, "m_MyRankSprite")
RegistClassMember(LuaYuanXiaoDefenseRankWnd, "m_MyNameLabel")
--RegistClassMember(LuaYuanXiaoDefenseRankWnd, "m_MyTeamScoreLabel")
RegistClassMember(LuaYuanXiaoDefenseRankWnd, "m_MyScoreLabel")
RegistClassMember(LuaYuanXiaoDefenseRankWnd, "m_TableViewDataSource")

function LuaYuanXiaoDefenseRankWnd:Awake()
    self.m_MyRank = 0
    self.m_RankList = {}

    self.m_EmptyTip = self.transform:Find("Anchor/EmptyTip").gameObject
    self.m_TableView = self.transform:Find("Anchor/TableView"):GetComponent(typeof(QnTableView))
    self.m_MainPlayerInfo = self.m_TableView.transform:Find("MainPlayerInfo").gameObject
    self.m_MyRankLabel = self.m_MainPlayerInfo.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    self.m_MyRankSprite = self.m_MyRankLabel.transform:Find("RankSprite"):GetComponent(typeof(UISprite))
    self.m_MyNameLabel = self.m_MainPlayerInfo.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    --self.m_MyTeamScoreLabel = self.m_MainPlayerInfo.transform:Find("TeamScoreLabel"):GetComponent(typeof(UILabel))
    self.m_MyScoreLabel = self.m_MainPlayerInfo.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))

    self.m_MyRankSprite.gameObject:SetActive(false)

    self.m_MainPlayerInfo:SetActive(false)

    self.m_MyNameLabel.text = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name or ""
    self.m_MyRankLabel.text = LocalString.GetString("未上榜")
    --self.m_MyTeamScoreLabel.text = LocalString.GetString("—")
    self.m_MyScoreLabel.text = LocalString.GetString("—")

    self.m_TableViewDataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RankList
        end,
        function(item, index)
            self:InitItem(item, index, self.m_RankList[index + 1])
        end
    )
    self.m_TableView.m_DataSource = self.m_TableViewDataSource
    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)  
        local data = self.m_RankList[row+1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, "", nil, Vector3.zero, AlignType.Default)
        end
    end)

    self:InitOneItem(self.transform:Find("Anchor/RewardHint/Reward").gameObject, YuanXiao2023_NaoYuanXiao.GetData().RewardTitleItemId)

    Gac2Gas.QueryRank(YuanXiao2023_NaoYuanXiao.GetData().RankId)
end

function LuaYuanXiaoDefenseRankWnd:InitOneItem(curItem, itemId)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemId)

    if ItemData then 
        texture:LoadMaterial(ItemData.Icon) 
    end

    CommonDefs.AddOnClickListener(
        curItem,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end

function LuaYuanXiaoDefenseRankWnd:Init()

end

function LuaYuanXiaoDefenseRankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
    
end
function LuaYuanXiaoDefenseRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaYuanXiaoDefenseRankWnd:InitItem(item, index, info)
    item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

    local tf = item.transform
    --local numLabel = tf:Find("NumLabel"):GetComponent(typeof(UILabel))
    --numLabel.text = ""
    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text = ""
    local rankLabel = tf:Find("RankLabel"):GetComponent(typeof(UILabel))
    rankLabel.text = ""
    local rankSprite = tf:Find("RankLabel/RankSprite"):GetComponent(typeof(UISprite))
    rankSprite.spriteName = ""

    local rank = info.Rank

    --local teamScoreLabel = tf:Find("TeamScoreLabel"):GetComponent(typeof(UILabel))
    local scoreLabel = tf:Find("ScoreLabel"):GetComponent(typeof(UILabel))

    --local extraUD = info.extraData
	--local extra = extraUD and MsgPackImpl.unpack(extraUD)
	--local teamScore = info.Value
	--local score = extra and extra.Count > 0 and extra[0]
    --if teamScore then teamScoreLabel.text = teamScore
    --else teamScoreLabel.text = LocalString.GetString("—") end
    if info.Value then scoreLabel.text = info.Value
    else scoreLabel.text = LocalString.GetString("—") end

    nameLabel.text = info.Name

    if rank == self.m_MyRank then 
        --numLabel.color = Color.green 
        nameLabel.color = Color.green 
        rankLabel.color = Color.green 
        --teamScoreLabel.color = Color.green
        scoreLabel.color = Color.green
    end

    rankSprite.gameObject:SetActive(true)
    if rank == 1 then
        rankSprite.spriteName = "Rank_No.1"
    elseif rank == 2 then
        rankSprite.spriteName = "Rank_No.2png"
    elseif rank == 3 then
        rankSprite.spriteName = "Rank_No.3png"
    else
        rankSprite.gameObject:SetActive(false)
        rankLabel.text = tostring(rank)
    end
end

function LuaYuanXiaoDefenseRankWnd:OnRankDataReady()
    local myInfo = CRankData.Inst.MainPlayerRankInfo
    self.m_MyRank = myInfo.Rank
    if myInfo.Rank > 0 then
        local rank = myInfo.Rank
        self.m_MyRankLabel.text = ""
        self.m_MyRankSprite.gameObject:SetActive(true)
        if rank == 1 then
            self.m_MyRankSprite.spriteName = "Rank_No.1"
        elseif rank == 2 then
            self.m_MyRankSprite.spriteName = "Rank_No.2png"
        elseif rank == 3 then
            self.m_MyRankSprite.spriteName = "Rank_No.3png"
        else
            self.m_MyRankSprite.gameObject:SetActive(false)
            self.m_MyRankLabel.text = tostring(rank)
        end
    else
        self.m_MyRankLabel.text = LocalString.GetString("未上榜")
    end

    self.m_MyNameLabel.text = myInfo.Name
    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text = CClientMainPlayer.Inst.Name
    end

    --local extraUD = myInfo.extraData
	--local extra = extraUD and MsgPackImpl.unpack(extraUD)
	--local teamScore = myInfo.Value
	--local score = extra and extra.Count > 0 and extra[0]

    --if teamScore then self.m_MyTeamScoreLabel.text = teamScore
    --else self.m_MyTeamScoreLabel.text = LocalString.GetString("—") end
        
    --if score then self.m_MyScoreLabel.text = score
    --else self.m_MyScoreLabel.text = LocalString.GetString("—") end

    if myInfo.Value then self.m_MyScoreLabel.text = myInfo.Value
    else self.m_MyScoreLabel.text = LocalString.GetString("—") end

    --self.m_MyTeamScoreLabel.color = Color.green
    self.m_MyScoreLabel.color = Color.green
    self.m_MyRankLabel.color = Color.green
    self.m_MyNameLabel.color = Color.green

    self.m_RankList = {}
    for i = 1, CRankData.Inst.RankList.Count do
        table.insert(self.m_RankList, CRankData.Inst.RankList[i - 1])
    end

    self.m_MainPlayerInfo:SetActive(#self.m_RankList > 0) 
    self.m_EmptyTip:SetActive(#self.m_RankList == 0)

    self.m_TableView:ReloadData(true, false)
end