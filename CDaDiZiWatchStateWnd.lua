-- Auto Generated!!
local CDaDiZiWatchStateWnd = import "L10.UI.CDaDiZiWatchStateWnd"
local CScene = import "L10.Game.CScene"
local CUIManager = import "L10.UI.CUIManager"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local String = import "System.String"
CDaDiZiWatchStateWnd.m_OnEnable_CS2LuaHook = function (this) 
    CUIManager.instance:HideUI(InitializeList(CreateFromClass(MakeGenericClass(List, String)), String, "CurrentTarget", "MainPlayerFrame", "TaskAndTeam", "Joystick", "RightMenuWnd", "SkillButtonBoard"))

    EventManager.AddListenerInternal(EnumEventType.SceneRemainTimeUpdate, MakeDelegateFromCSFunction(this.OnRemainTimeUpdate, MakeGenericClass(Action1, Int32), this))
    EventManager.AddListener(EnumEventType.ShowTimeCountDown, MakeDelegateFromCSFunction(this.OnShowTimeCountDown, Action0, this))
    EventManager.AddListener(EnumEventType.SendShouXiDaDiZiBiWuWatchInfo, MakeDelegateFromCSFunction(this.OnSendShouXiDaDiZiBiWuWatchInfo, Action0, this))
end
CDaDiZiWatchStateWnd.m_OnDisable_CS2LuaHook = function (this) 
    CUIManager.instance:ShowUI(InitializeList(CreateFromClass(MakeGenericClass(List, String)), String, "CurrentTarget", "MainPlayerFrame", "TaskAndTeam", "Joystick", "RightMenuWnd", "SkillButtonBoard"))

    EventManager.RemoveListenerInternal(EnumEventType.SceneRemainTimeUpdate, MakeDelegateFromCSFunction(this.OnRemainTimeUpdate, MakeGenericClass(Action1, Int32), this))
    EventManager.RemoveListener(EnumEventType.ShowTimeCountDown, MakeDelegateFromCSFunction(this.OnShowTimeCountDown, Action0, this))
    EventManager.RemoveListener(EnumEventType.SendShouXiDaDiZiBiWuWatchInfo, MakeDelegateFromCSFunction(this.OnSendShouXiDaDiZiBiWuWatchInfo, Action0, this))
end
CDaDiZiWatchStateWnd.m_OnRemainTimeUpdate_CS2LuaHook = function (this, leftTime) 
    if CScene.MainScene ~= nil then
        if CScene.MainScene.ShowTimeCountDown then
            this.remainTimeLabel.text = this:GetRemainTimeText(CScene.MainScene.ShowTime)
        else
            this.remainTimeLabel.text = nil
        end
    else
        this.remainTimeLabel.text = nil
    end
end
CDaDiZiWatchStateWnd.m_GetRemainTimeText_CS2LuaHook = function (this, totalSeconds) 
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return System.String.Format("[ACF9FF]{0:00}:{1:00}:{2:00}[-]", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return System.String.Format("[ACF9FF]{0:00}:{1:00}[-]", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end
