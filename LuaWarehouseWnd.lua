local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CItemMgr=import "L10.Game.CItemMgr"
local Constants = import "L10.Game.Constants"

CLuaWarehouseWnd = class()
RegistClassMember(CLuaWarehouseWnd,"closeBtn")
RegistClassMember(CLuaWarehouseWnd,"warehouseView")
RegistClassMember(CLuaWarehouseWnd,"packageView")
RegistClassMember(CLuaWarehouseWnd,"m_BangCangEnabled")

function CLuaWarehouseWnd:Awake()

    self.closeBtn = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
    self.warehouseView = self.transform:Find("WarehouseView"):GetComponent(typeof(CCommonLuaScript))
    self.packageView = self.transform:Find("WarehousePackageView"):GetComponent(typeof(CCommonLuaScript))
    self.m_BangCangEnabled = false

    if CUIManager.IsLoading(CLuaUIResources.StoreRoomWnd) or CUIManager.IsLoaded(CLuaUIResources.StoreRoomWnd) then
		CUIManager.CloseUI(CLuaUIResources.StoreRoomWnd)
	end
end


function CLuaWarehouseWnd:Init()

    self.packageView.m_LuaSelf.OnPackageItemClick = function(packagePos, itemId)
        self:OnPackageItemClick(packagePos, itemId)
    end
    self.packageView.m_LuaSelf:Init()
    self.warehouseView.m_LuaSelf.m_OnMoveItemFromWarehouseToBagAction = function(repoIndex, itemId)
        self:OnMoveItemFromWarehouseToBag(repoIndex, itemId)
    end
    self.warehouseView.m_LuaSelf.m_RefreshPackageView = function(mask_unbinded)
        self.m_BangCangEnabled = mask_unbinded
        self.packageView.m_LuaSelf:OnRefreshPackageView(mask_unbinded)
    end
end

function CLuaWarehouseWnd:OnPackageItemClick(packagePos, itemId)
    if(self.m_BangCangEnabled)then
        Gac2Gas.MoveNormalItemToSimpleItems(packagePos, itemId)
    else
        --检查是否可以放入绑仓
        if IsBindRepoDisplayOpen() and CLuaBindRepoMgr.CanPlaceInBindRepo(itemId) then
            Gac2Gas.MoveNormalItemToSimpleItems(packagePos, itemId)
            local commonItem = CItemMgr.Inst:GetById(itemId)
            if commonItem then
                g_MessageMgr:ShowMessage("ANNANG_WARNING",commonItem.Name)
            end
        else
            Gac2Gas.MoveItemToRepertory(packagePos, itemId, self.warehouseView.m_LuaSelf:GetCurRepoPageIdx() * Constants.SingeWarehouseSize + 1)
        end
    end
end

function CLuaWarehouseWnd:OnMoveItemFromWarehouseToBag(repoIndex, itemId)
    Gac2Gas.MoveItemToBag(repoIndex, itemId)
end

function CLuaWarehouseWnd:OnEnable()
    g_ScriptEvent:AddListener("RefreshRpcFromFastOverlap", self, "OnRefreshRpcFromFastOverlap")
end

function CLuaWarehouseWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RefreshRpcFromFastOverlap", self, "OnRefreshRpcFromFastOverlap")
end

function CLuaWarehouseWnd:OnRefreshRpcFromFastOverlap()
    Gac2Gas.RequestMergeOverlapItemToRepo()
end