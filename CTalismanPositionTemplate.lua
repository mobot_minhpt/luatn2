-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemMgr = import "L10.Game.CItemMgr"
local Color = import "UnityEngine.Color"
local CTalismanPositionTemplate = import "L10.UI.CTalismanPositionTemplate"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EquipmentTemplate_Type = import "L10.Game.EquipmentTemplate_Type"
local IdPartition = import "L10.Game.IdPartition"
CTalismanPositionTemplate.m_Init_CS2LuaHook = function (this, position) 

    this.position = position
    local type = EquipmentTemplate_Type.GetData(position)
    this.positionLabel.text = ""
    this.iconTexture.material = nil
    this.qualitySprite.color = Color.white

    if type ~= nil then
        this.positionLabel.text = type.Name
    else
        type = EquipmentTemplate_Type.GetData(position - 8)
        if type ~= nil then
            this.positionLabel.text = type.Name
        end
    end

    if CClientMainPlayer.Inst == nil then
        return
    end
    local itemProp = CClientMainPlayer.Inst.ItemProp
    local itemId = itemProp:GetItemAt(EnumItemPlace.Body, position)
    this.mItemId = itemId

    this:InitTalisman(this.mItemId)
end
CTalismanPositionTemplate.m_InitTalisman_CS2LuaHook = function (this, itemId) 
    local talisman = CItemMgr.Inst:GetById(itemId)
    if talisman ~= nil and IdPartition.IdIsTalisman(talisman.TemplateId) then
        this.iconTexture:LoadMaterial(talisman.Icon)
        this.qualitySprite.color = talisman.Equip.DisplayColor

        this.disableSprite.gameObject:SetActive(talisman.Equip.Disable > 0)
    else
        this.disableSprite.gameObject:SetActive(false)
    end
end
