--[[
	若功能中需要改动c#，并在Lua下访问此C#改动请在下方进行登记，详细注意事项参见
	https://confluence.leihuo.netease.com/pages/viewpage.action?pageId=47937723


	1.  CChatLinkMgr.cs文件改动，JoinCCChannel反射方法搬到lua下，同时封装了两个Env相关方法供lua使用
	2.  CHouseCommunityMasterView.cs CCommunityWnd.cs CCommunityHouseItem.cs文件改动，涉及一个家园代言人功能改动，时间关系直接在C#中进行处理
	3.  EnumPlayScoreKey增加一个字段GlobalSpokesmanScore，热更新的话能进游戏，但是当铺积分商店功能不正常。
	4.	RenderObject的SetKickOutStatus方法增加一个延迟参数，如果热更新的话，需要patch下lua的这个方法调用
	5.  ACHelperMgr.cs文件改动 增加一段IEnumerator, 在pc端 提高反外挂sdk 的检测频率
	6.	新时装增加headalpha，不影响热更新，但是热更此功能不支持。

	chenguangzhong 2020/06/29
	SVN 419083 CWeatherMgr客户端支持复合型天气
	M /trunk/L10/Development/QnMobile/Assets/Scripts/Game/Scene/CWeatherMgr.cs

	SVN 420406 不影响热更 refs #program_default 主干删除静态渲染相关的所有的代码，该功能基本没用，QA只需留意下高分辨率和低分辨率设置不报错就ok
	SVN 420959  refs #program_default 将美术目录的批量特效延迟脚本导入游戏工程， 对热更的影响是部分特效如果挂了该脚本会丢失，目前看应该还没有。

	
]]

--[[
	chenguangzhong 2020/07/21
	M CameraFollow.cs 
	增加相机自适应高度修正值,在七夕节会用到。CLuaQiXiMgr.ProcessCamera中会设置该值，cs代码没有更新将无法修改或读取该值而报错
	
	M CLightMgr.cs
	M CRenderScene.cs
	增加了有关场景头顶灯的控制和场景亮度控制，无法热更cs代码可能导致RPC函数或者Lua下调用不存在的函数而报错
	如Gas2Gac.AttachLightToCharacter

	A CSceneBrightnessCurveEffect.cs
	A CSceneBrightnessMgr.cs
	新增控制场景亮度的特效脚本，无法热更cs代码将导致特效上CSceneBrightnessCurveEffect脚本丢失而报错
]]

--[[
r421366
refs #program_default bundleloader 更新，将外网打过测试的2018的代码patch提交到主干，尝试修复patch目录文件过多时客户端内存会慢慢暴增的问题
不影响IOS热更
refs #program_default main.cs 更新，修改快速GC的时间间隔，不影响热更，cs文件跟随本文件修改一起提交
]]


--shengfang #140373 蛛丝洞轻鸿玉佩引导优化
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local Lua=import "L10.Engine.Lua"
CGuideMgr.m_hookProcessUpdateViewEvent = function(this,eventType)
	if eventType=="QingHongFlyWnd" then
		Lua.s_IsHook=true
	else
		Lua.s_IsHook=false
	end
end
