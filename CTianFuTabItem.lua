-- Auto Generated!!
local BoolDelegate = import "UIEventListener+BoolDelegate"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CTianFuTabItem = import "L10.UI.CTianFuTabItem"
local LocalString = import "LocalString"
local Object = import "System.Object"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local Time = import "UnityEngine.Time"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
CTianFuTabItem.m_Init_CS2LuaHook = function (this, classId, level, maxLevel, needXiuWeiLevel, locked) 

    if this.lastClassId ~= classId then
        this.incBtnPressed = false
        this.decBtnPressed = false
    end
    this.lastClassId = classId

    this.existSkill = Skill_AllSkills.Exists(CLuaDesignMgr.Skill_AllSkills_GetSkillId(classId, level > 0 and level or 1))
    this.levelLabel.text = (level > 0 and not locked) and tostring(level) or nil
    this.levelLabel.gameObject:SetActive(not System.String.IsNullOrEmpty(this.levelLabel.text))
    this.isLocked = locked
    if locked then
        this.lockOrAddSprite.spriteName = CTianFuTabItem.LockSpriteName
        this.lockOrAddSprite:MakePixelPerfect()
    elseif not this.existSkill then
        this.lockOrAddSprite.spriteName = this.AddSpriteName
        this.lockOrAddSprite:MakePixelPerfect()
    else
        this.lockOrAddSprite.spriteName = nil
    end
    this.unlockLabel.gameObject:SetActive(locked)
    this.unlockLabel.text = tostring(needXiuWeiLevel) .. LocalString.GetString("修为开启")
    this.pointsLabel.text = System.String.Format("{0}/{1}", level, maxLevel)
    local data = Skill_AllSkills.GetData(CLuaDesignMgr.Skill_AllSkills_GetSkillId(classId, level > 0 and level or 1))
    if data == nil or locked then
        this.skillIcon:Clear()
        this.skillNameLabel.text = nil
    else
        this.skillIcon:LoadSkillIcon(data.SkillIcon)
        this.skillNameLabel.text = data.Name
    end

    if this.m_FeiSheng and this.m_FeiShengLevel ~= level then
        local modifiedLevel = this.m_FeiShengLevel
        local originWithDeltaLevel = CClientMainPlayer.Inst and CClientMainPlayer.Inst.SkillProp:GetTianFuSkilleltaLevel(data.SkillClass, CClientMainPlayer.Inst.Level)
        if modifiedLevel > 0 and originWithDeltaLevel > 0 then
            modifiedLevel = (originWithDeltaLevel + modifiedLevel)
            if modifiedLevel > data._Before_Extend_Level then
                modifiedLevel = data._Before_Extend_Level
            end
        end
        this.levelLabel.text = System.String.Format("[c][{0}]{1}[-][/c]", Constants.ColorOfFeiSheng, modifiedLevel)
    end
end
CTianFuTabItem.m_InitFeiSheng_CS2LuaHook = function (this, active, feishengLevel) 
    this.m_FeiSheng = active
    this.m_FeiShengLevel = feishengLevel
end
CTianFuTabItem.m_Update_CS2LuaHook = function (this) 

    if this.incBtnPressed and Time.realtimeSinceStartup - this.lastIncTime >= 0.1 then
        this.lastIncTime = Time.realtimeSinceStartup
        this:OnIncPoint()
    end
    if this.decBtnPressed and Time.realtimeSinceStartup - this.lastDecTime >= 0.1 then
        this.lastDecTime = Time.realtimeSinceStartup
        this:OnDecPoint()
    end
end
CTianFuTabItem.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.IncPointBtn).onPress = CommonDefs.CombineListner_BoolDelegate(UIEventListener.Get(this.IncPointBtn).onPress, MakeDelegateFromCSFunction(this.OnIncreaseButtonPress, BoolDelegate, this), true)
    UIEventListener.Get(this.decPointBtn).onPress = CommonDefs.CombineListner_BoolDelegate(UIEventListener.Get(this.decPointBtn).onPress, MakeDelegateFromCSFunction(this.OnDecreaseButtonPress, BoolDelegate, this), true)
    UIEventListener.Get(this.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.gameObject).onClick, MakeDelegateFromCSFunction(this.OnTabItemClick, VoidDelegate, this), true)
    UIEventListener.Get(this.pointsLabel.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.pointsLabel.gameObject).onClick, MakeDelegateFromCSFunction(this.OnNumberInputClick, VoidDelegate, this), true)
end
CTianFuTabItem.m_OnDisable_CS2LuaHook = function (this) 

    this.incBtnPressed = false
    this.decBtnPressed = false
end
CTianFuTabItem.m_OnIncreaseButtonPress_CS2LuaHook = function (this, go, pressed) 

    this.incBtnPressed = pressed
end
CTianFuTabItem.m_OnDecreaseButtonPress_CS2LuaHook = function (this, go, pressed) 

    this.decBtnPressed = pressed
end
CTianFuTabItem.m_OnIncPoint_CS2LuaHook = function (this) 

    if this.OnIncPointBtnClick ~= nil then
        GenericDelegateInvoke(this.OnIncPointBtnClick, nil)
    end
end
CTianFuTabItem.m_OnDecPoint_CS2LuaHook = function (this) 

    if this.OnDecPointBtnClick ~= nil then
        GenericDelegateInvoke(this.OnDecPointBtnClick, nil)
    end
end
CTianFuTabItem.m_OnTabItemClick_CS2LuaHook = function (this, go) 

    if this.OnItemClick ~= nil then
        GenericDelegateInvoke(this.OnItemClick, Table2ArrayWithCount({this.gameObject}, 1, MakeArrayClass(Object)))
    end
end
CTianFuTabItem.m_OnNumberInputClick_CS2LuaHook = function (this, go) 
    if this.OnShowNumberKeyboard ~= nil then
        GenericDelegateInvoke(this.OnShowNumberKeyboard, Table2ArrayWithCount({this.gameObject}, 1, MakeArrayClass(Object)))
    end
end
CTianFuTabItem.m_GetIncButton_CS2LuaHook = function (this) 

    return this.IncPointBtn
end
