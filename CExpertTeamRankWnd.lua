-- Auto Generated!!
local CExpertTeamMgr = import "L10.Game.CExpertTeamMgr"
local CExpertTeamRankWnd = import "L10.UI.CExpertTeamRankWnd"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
CExpertTeamRankWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.backBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        this:Close()
    end)

    this.templateNode:SetActive(false)
    CExpertTeamMgr.Inst:SortRankExpertInfo()
    this:InitNewExpertList()
    this:InitExpertList()
    this:InitWeekExpertList()
end
CExpertTeamRankWnd.m_InitNode_CS2LuaHook = function (this, info, index, table, tempNode) 
    local node = NGUITools.AddChild(table.gameObject, this.templateNode)
    node:SetActive(true)
    if index == 0 then
    elseif index == 1 then
    elseif index == 2 then
    end

    local iconNode = node.transform:Find("icon").gameObject
    CExpertTeamMgr.Inst:SetPlayerIcon(info.rankPlayerId, info.avatar, iconNode, nil, "", 0, 0)

    CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = info.rankPlayerName
    CommonDefs.GetComponent_Component_Type(node.transform:Find("num1"), typeof(UILabel)).text = tostring(info.score)
    CommonDefs.GetComponent_Component_Type(node.transform:Find("num2"), typeof(UILabel)).text = tostring(info.tAcceptTimes)
end
CExpertTeamRankWnd.m_InitNewExpertList_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.newexpert_table.transform)
    local list = CExpertTeamMgr.Inst.newExpertList

    local scrollview = this.newexpert_scrollview
    local table = this.newexpert_table
    if list.Count > 0 then
        do
            local i = 0
            while i < list.Count do
                this:InitNode(list[i], i, table, this.templateNode)
                i = i + 1
            end
        end

        table:Reposition()
        scrollview:ResetPosition()
    end
end
CExpertTeamRankWnd.m_InitExpertList_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.expert_table.transform)
    local list = CExpertTeamMgr.Inst.expertList

    local scrollview = this.expert_scrollview
    local table = this.expert_table
    if list.Count > 0 then
        do
            local i = 0
            while i < list.Count do
                this:InitNode(list[i], i, table, this.templateNode)
                i = i + 1
            end
        end

        table:Reposition()
        scrollview:ResetPosition()
    end
end
CExpertTeamRankWnd.m_InitWeekExpertList_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.thisweek_table.transform)
    local list = CExpertTeamMgr.Inst.weekExpertList

    local scrollview = this.thisweek_scrollview
    local table = this.thisweek_table
    if list.Count > 0 then
        do
            local i = 0
            while i < list.Count do
                this:InitNode(list[i], i, table, this.templateNode)
                i = i + 1
            end
        end

        table:Reposition()
        scrollview:ResetPosition()
    end
end
