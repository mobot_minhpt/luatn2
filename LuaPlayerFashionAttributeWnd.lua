local UILabel = import "UILabel"
local UIWidget = import "UIWidget"
local NGUIMath = import "NGUIMath"
local UITable = import "UITable"

LuaPlayerFashionAttributeWnd = class()

RegistChildComponent(LuaPlayerFashionAttributeWnd,"AtrributeTable", UITable)
RegistChildComponent(LuaPlayerFashionAttributeWnd,"AttributeItem", GameObject)
RegistChildComponent(LuaPlayerFashionAttributeWnd,"Background", UIWidget)
RegistChildComponent(LuaPlayerFashionAttributeWnd,"TitleLabel", UIWidget)

function LuaPlayerFashionAttributeWnd:Init()
    Extensions.RemoveAllChildren(self.AtrributeTable.transform)
    for _,attrDesc in ipairs(LuaAppearancePreviewMgr.AttributeList) do
        local go = NGUITools.AddChild(self.AtrributeTable.gameObject, self.AttributeItem)
        go:SetActive(true)
        local value = string.match(attrDesc,"%d+")
        local symbolIndex = string.find(attrDesc,LocalString.GetString("增加"))
        if symbolIndex and value then
            local attr = string.sub(attrDesc,1,symbolIndex-1)
            go:GetComponent(typeof(UILabel)).text = SafeStringFormat3("%s[02FF67]+%d[-]",attr,value)
        elseif value then
            go:GetComponent(typeof(UILabel)).text = string.gsub(attrDesc,value,SafeStringFormat3("[02FF67]%s[-]",value))
        else
            go:GetComponent(typeof(UILabel)).text = attrDesc
        end
    end
    self.AtrributeTable:Reposition()
    local tableheight = NGUIMath.CalculateRelativeWidgetBounds(self.AtrributeTable.transform).size.y
    local titleHeight = self.TitleLabel.height
    self.Background.height = tableheight + titleHeight + 60
end