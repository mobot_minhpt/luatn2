local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local UISprite = import "UISprite"
local UISlider = import "UISlider"
local UIBasicSprite = import "UIBasicSprite"

LuaAppearanceCommonFashionDisplayView = class()
RegistChildComponent(LuaAppearanceCommonFashionDisplayView,"m_NoneLabel", "None", UILabel)
RegistChildComponent(LuaAppearanceCommonFashionDisplayView,"m_ContentRoot", "Content", GameObject)
RegistChildComponent(LuaAppearanceCommonFashionDisplayView,"m_IconTexture", "Item", CUITexture)
RegistChildComponent(LuaAppearanceCommonFashionDisplayView,"m_DisabledGo", "Disabled", GameObject)
RegistChildComponent(LuaAppearanceCommonFashionDisplayView,"m_LockedGo", "Locked", GameObject)
RegistChildComponent(LuaAppearanceCommonFashionDisplayView,"m_NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaAppearanceCommonFashionDisplayView,"m_QualityBg", "QualityBg", UIBasicSprite)
RegistChildComponent(LuaAppearanceCommonFashionDisplayView,"m_QualityLabel", "QualityLabel", UILabel)
RegistChildComponent(LuaAppearanceCommonFashionDisplayView,"m_ProgressLabel", "ProgressLabel", UILabel)
RegistChildComponent(LuaAppearanceCommonFashionDisplayView,"m_ProgressBar", "ProgressBar", UISlider)
RegistChildComponent(LuaAppearanceCommonFashionDisplayView,"m_RegularButtonRoot", "RegularButtonRoot", Transform)
RegistChildComponent(LuaAppearanceCommonFashionDisplayView,"m_BigButton", "BigButton", CButton) --显示一个按钮时，使用该按钮
RegistChildComponent(LuaAppearanceCommonFashionDisplayView,"m_LeftButton", "LeftButton", CButton) --显示两个按钮时，左边使用该按钮
RegistChildComponent(LuaAppearanceCommonFashionDisplayView,"m_RightButton", "RightButton", CButton) --显示两个按钮时，右边使用该按钮

function LuaAppearanceCommonFashionDisplayView:Awake()
end

function LuaAppearanceCommonFashionDisplayView:Clear()
    self.m_NoneLabel.gameObject:SetActive(true)
    self.m_ContentRoot:SetActive(false)
end

-- buttonTbl每个元素的格式 {text=xxx, isYellow=xxx, action=xxx}
function LuaAppearanceCommonFashionDisplayView:Init(icon, name, qualityBgColor, qualityText, bShowProgress, curVal, needVal, disabled, locked, buttonTbl)
    self.m_NoneLabel.gameObject:SetActive(false)
    self.m_ContentRoot:SetActive(true)
    self.m_IconTexture:LoadMaterial(icon)
    --根据最新策划需求，不显示disable和lock状态
    self.m_DisabledGo:SetActive(false)
    self.m_LockedGo:SetActive(false)
    self.m_NameLabel.text = name
    self.m_QualityBg.color = qualityBgColor
    self.m_QualityLabel.text = qualityText
    if bShowProgress then
        self.m_ProgressLabel.text = SafeStringFormat3(LocalString.GetString("解锁永久进度 %d/%d"), curVal, needVal)
        self.m_ProgressBar.value = curVal/needVal
        self.m_ProgressBar.gameObject:SetActive(true)
    else
        self.m_ProgressLabel.text = ""                                      
        self.m_ProgressBar.gameObject:SetActive(false)
    end
    self.m_BigButton.gameObject:SetActive(false)
    self.m_LeftButton.gameObject:SetActive(false)
    self.m_RightButton.gameObject:SetActive(false)
    local tbl = {}
    --最少显示0个按钮，最多显示2个按钮
    if buttonTbl and #buttonTbl>0 and #buttonTbl<3 then
        if #buttonTbl==1 then
            table.insert(tbl, self.m_BigButton)
        else
            table.insert(tbl, self.m_LeftButton)
            table.insert(tbl, self.m_RightButton)
        end
    end

    for i=1,#buttonTbl do
        local btn = tbl[i]
        local info = buttonTbl[i]
        btn.gameObject:SetActive(true)
        btn.Text = info.text
        btn:SetBackgroundSprite(info.isYellow and g_sprites.AppearanceCommonOrangeButtonBg or g_sprites.AppearanceCommonBlueButtonBg)
        btn:SetFontTextColor(info.isYellow and NGUIText.ParseColor24("754923", 0) or NGUIText.ParseColor24("1C4165", 0))
        UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            if info.action then
                info.action(go)
            end
        end)
    end
end

