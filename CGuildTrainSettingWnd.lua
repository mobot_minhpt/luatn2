-- Auto Generated!!
local CGuildTrainMgr = import "L10.Game.CGuildTrainMgr"
local CGuildTrainSettingWnd = import "L10.UI.CGuildTrainSettingWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Guild_Setting = import "L10.Game.Guild_Setting"
local MessageWndManager = import "L10.UI.MessageWndManager"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildTrainSettingWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseBtnClicked, VoidDelegate, this), true)
    UIEventListener.Get(this.confirmBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.confirmBtn).onClick, MakeDelegateFromCSFunction(this.OnConfirmBtnClicked, VoidDelegate, this), true)

    EventManager.AddListener(EnumEventType.GuildTrainInfoInit, MakeDelegateFromCSFunction(this.categoryList.Init, Action0, this.categoryList))
end
CGuildTrainSettingWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseBtnClicked, VoidDelegate, this), false)
    UIEventListener.Get(this.confirmBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.confirmBtn).onClick, MakeDelegateFromCSFunction(this.OnConfirmBtnClicked, VoidDelegate, this), false)

    EventManager.RemoveListener(EnumEventType.GuildTrainInfoInit, MakeDelegateFromCSFunction(this.categoryList.Init, Action0, this.categoryList))
end
CGuildTrainSettingWnd.m_OnConfirmBtnClicked_CS2LuaHook = function (this, go) 
    local changeCount = 0
    local hasChanged = false
    do
        local i = 0
        while i < CGuildTrainMgr.Inst.trainCategoryList.Count do
            do
                local j = 0
                while j < CGuildTrainMgr.Inst.trainCategoryList[i].trainItemList.Count do
                    if not CGuildTrainMgr.Inst.oldTrainSelectList[i].trainItemList[j].hasSelected and CGuildTrainMgr.Inst.trainCategoryList[i].trainItemList[j].hasSelected then
                        changeCount = changeCount + 1
                    elseif CGuildTrainMgr.Inst.oldTrainSelectList[i].trainItemList[j].hasSelected and not CGuildTrainMgr.Inst.trainCategoryList[i].trainItemList[j].hasSelected then
                        hasChanged = true
                    end
                    j = j + 1
                end
            end
            i = i + 1
        end
    end
    this:SetTrainCategory(changeCount, hasChanged)
end
CGuildTrainSettingWnd.m_SetTrainCategory_CS2LuaHook = function (this, count, hasChanged) 
    local msg = ""
    if count > 0 then
        msg = g_MessageMgr:FormatMessage("GUILD_XIULIAN_CHOOSE__CONFIRM", Guild_Setting.GetData().Practice_Spent_Guild_Money * count)
    elseif hasChanged then
        msg = g_MessageMgr:FormatMessage("GUILD_XIULIAN_CHOOSE_DIFF_CONFIRM")
    end
    if msg ~= "" then
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
            CGuildTrainMgr.Inst:SetGuildPracticeAvailable()
            this:Close()
        end), nil, nil, nil, false)
    else
        this:Close()
    end
end
