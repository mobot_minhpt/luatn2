local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CBaseWnd = import "L10.UI.CBaseWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local CCharacterModelTextureLoader=import "L10.UI.CCharacterModelTextureLoader"
local Constants = import "L10.Game.Constants"
local Color = import "UnityEngine.Color"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local UIWidget = import "UIWidget"
local UITable = import "UITable"
local LuaTweenUtils = import "LuaTweenUtils"
local Ease = import "DG.Tweening.Ease"
local Vector3 = import "UnityEngine.Vector3"
local GameObject = import "UnityEngine.GameObject"
local UITexture = import "UITexture"
local UIPanel = import "UIPanel"
local NGUIText = import "NGUIText"
LuaNationalDayJCYWResultWnd = class()

RegistClassMember(LuaNationalDayJCYWResultWnd, "m_CloseButton")
RegistClassMember(LuaNationalDayJCYWResultWnd, "m_CostTimeLabel")
RegistClassMember(LuaNationalDayJCYWResultWnd, "m_ScoreLabel")
RegistClassMember(LuaNationalDayJCYWResultWnd, "m_ShareButton")
RegistClassMember(LuaNationalDayJCYWResultWnd, "m_EvaluateIcon")
RegistClassMember(LuaNationalDayJCYWResultWnd, "m_TargetsTable")
RegistClassMember(LuaNationalDayJCYWResultWnd, "m_FlagBg")

function LuaNationalDayJCYWResultWnd:Awake()
    self.gameObject:GetComponent(typeof(UIPanel)).IgnoreIphoneXMargin = true
end

function LuaNationalDayJCYWResultWnd:Init()

	self.m_CloseButton = self.transform:Find("Anchor/CloseButton").gameObject
	self.m_ShareButton = self.transform:Find("Anchor/Right/Bg/ShareButton").gameObject
	self.m_CostTimeLabel = self.transform:Find("Anchor/Right/Bg/CostTimeLabel"):GetComponent(typeof(UILabel))
	self.m_ScoreLabel = self.transform:Find("Anchor/Right/Bg/ScoreLabel"):GetComponent(typeof(UILabel))
	self.m_EvaluateIcon = self.transform:Find("Anchor/Right/Bg/EvaluateIcon"):GetComponent(typeof(CUITexture))
	self.m_TargetsTable = self.transform:Find("Anchor/Right/Bg/Table"):GetComponent(typeof(UITable))
	self.m_FlagBg = self.transform:Find("Anchor/Right/Bg"):GetComponent(typeof(UIWidget))

	CommonDefs.AddOnClickListener(self.m_CloseButton, DelegateFactory.Action_GameObject(function(go) self:OnCloseButtonClick() end), false)
	if CommonDefs.IS_VN_CLIENT then 
		self.m_ShareButton:SetActive(false)
	else
		CommonDefs.AddOnClickListener(self.m_ShareButton, DelegateFactory.Action_GameObject(function(go) self:OnShareButtonClick() end), false)
	end
	--角色
	self:ShowPlayerModels()

	local time = LuaNationalDayMgr.m_JCYWResultInfo.m_UsedTime
	self.m_CostTimeLabel.text = SafeStringFormat3("%d:%02d", math.floor(time/60), math.floor(math.fmod(time,60)))
	self.m_ScoreLabel.text = tostring(LuaNationalDayMgr.m_JCYWResultInfo.m_Score)

	self:DoFlagBgAnimation()
	self:InitEvaluateIcon(0.5)
	self:InitTargets(0.7)

end

function LuaNationalDayJCYWResultWnd:ShowPlayerModels()
	local playersRoot = self.transform:Find("Anchor/Players")
	--第一个节点为主角，其余左右排列
	local firstPlayer = playersRoot:GetChild(0)
	while playersRoot.childCount>1 do
		local child = playersRoot:GetChild(1)
		child.transform.parent = nil
		GameObject.Destroy(child.gameObject)
	end

	local playerPosTbl = {}
	local firstPos = firstPlayer.transform.localPosition
	local firstScale = Vector3.one
	playerPosTbl[1] = {pos = firstPos, scale = firstScale, depth = 6}
	playerPosTbl[2] = {pos = Vector3(firstPos.x - 260, firstPos.y + 20, 0), scale = Vector3(0.9,0.9,0.9), depth = 5}
	playerPosTbl[3] = {pos = Vector3(firstPos.x + 260, firstPos.y + 20, 0), scale = Vector3(0.9,0.9,0.9), depth = 5}
	playerPosTbl[4] = {pos = Vector3(firstPos.x - 250 * 2, firstPos.y + 20 * 2, 0), scale = Vector3(0.81,0.81,0.81), depth = 4}
	playerPosTbl[5] = {pos = Vector3(firstPos.x + 250 * 2, firstPos.y + 20 * 2, 0), scale = Vector3(0.81,0.81,0.81), depth = 4}

	local membersCount = #LuaNationalDayMgr.m_JCYWResultInfo.m_Members
	for i=2,membersCount do
		if i>5 then break end
		CUICommonDef.AddChild(playersRoot.gameObject, firstPlayer.gameObject)
	end
	local i = 0
	for __,data in pairs(LuaNationalDayMgr.m_JCYWResultInfo.m_Members) do
		if i>=5 then break end
		local child = playersRoot:GetChild(i)
		child.gameObject:SetActive(true)
		child.transform.localPosition = playerPosTbl[i+1].pos
		local modelloader = child:Find("ModelTexture"):GetComponent(typeof(CCharacterModelTextureLoader))
		modelloader:Init(data.playerId,data.class,data.gender)
		modelloader.transform.localScale = playerPosTbl[i+1].scale
		modelloader.transform:GetComponent(typeof(UITexture)).depth = playerPosTbl[i+1].depth
		child:Find("LeaderIcon").gameObject:SetActive(data.isLeader)
		child:Find("NameLabel"):GetComponent(typeof(UILabel)).text = data.playerName
		local levelLabel = child:Find("LevelLabel"):GetComponent(typeof(UILabel))
		levelLabel.text = SafeStringFormat3("lv.%d", data.playerLevel)
		if data.xianfanStaus>0 then
			levelLabel.color = NGUIText.ParseColor24(Constants.ColorOfFeiSheng, 0)
		else
			levelLabel.color = Color.white
		end
		i = i+1
	end
end

function LuaNationalDayJCYWResultWnd:DoFlagBgAnimation()
	--animation
	LuaTweenUtils.DOKill(self.m_FlagBg.transform, false)
	self.m_FlagBg.alpha = 0 -- 先隐藏
	local v = self.m_FlagBg.transform.localPosition
	local endY = v.y
	v.y = v.y + 500
	self.m_FlagBg.transform.localPosition = v
	-- 0.2s显现
	local tweener1 = LuaTweenUtils.TweenPositionY(self.m_FlagBg.transform, endY, 0.3)
	local tweener2 = LuaTweenUtils.TweenAlpha(self.m_FlagBg, 0, 1, 0.3)
	LuaTweenUtils.SetDelay(LuaTweenUtils.SetEase(tweener1, Ease.OutBack), 0)
	LuaTweenUtils.SetDelay(LuaTweenUtils.SetEase(tweener2, Ease.OutBack), 0)
end

function LuaNationalDayJCYWResultWnd:InitEvaluateIcon(delay)
	local targetTbl = {}
	targetTbl[1] = "UI/Texture/Transparent/Material/JCYW_pingjia_chaqiangrenyi.mat"--差强人意
	targetTbl[2] = "UI/Texture/Transparent/Material/JCYW_pingjia_chaqiangrenyi.mat"--差强人意
	targetTbl[3] = "UI/Texture/Transparent/Material/JCYW_pingjia_jianrujiajing.mat"--渐入佳境
	targetTbl[4] = "UI/Texture/Transparent/Material/JCYW_pingjia_nanfengdishou.mat"--难逢敌手
	targetTbl[5] = "UI/Texture/Transparent/Material/JCYW_pingjia_jushiwushuang.mat"--绝世无双

	local tbl = LuaNationalDayMgr.m_JCYWResultInfo.m_Goals or {}

	--不连续key的表不能用#tbl获取数量
	local count = 0
	for key,val in pairs(tbl) do
		count = count + 1
	end

	if targetTbl[count] then
		self.m_EvaluateIcon:LoadMaterial(targetTbl[count])
	else
		self.m_EvaluateIcon:Clear()
	end

	--animation
	LuaTweenUtils.DOKill(self.m_EvaluateIcon.transform, false)
	self.m_EvaluateIcon.texture.alpha = 0 -- 先隐藏
	-- 0.2s显现
	local tweener1 = LuaTweenUtils.TweenScale(self.m_EvaluateIcon.transform, Vector3(2,2,2), Vector3.one, 0.2)
	local tweener2 = LuaTweenUtils.TweenAlpha(self.m_EvaluateIcon.texture, 0, 1, 0.2)
	LuaTweenUtils.SetDelay(LuaTweenUtils.SetEase(tweener1, Ease.OutBack), delay)
	LuaTweenUtils.SetDelay(LuaTweenUtils.SetEase(tweener2, Ease.OutBack), delay)
end

function LuaNationalDayJCYWResultWnd:InitTargets(delay)
	local n = self.m_TargetsTable.transform.childCount
	local tbl = LuaNationalDayMgr.m_JCYWResultInfo.m_Goals or {}
	self.m_TargetsTable:Reposition()
	self.m_TargetsTable.enabled = false
	local itemDelayIdx = 0
	for i=0,n-1 do
		local child = self.m_TargetsTable.transform:GetChild(i)
		local bgTexture = child:GetComponent(typeof(UIWidget))
		local lblTexture = child:Find("Texture"):GetComponent(typeof(UIWidget))
		local fit = (tbl[i+1]~=nil)
		if fit then
			bgTexture.color = NGUIText.ParseColor24("DFA64D", 0)
			lblTexture.alpha = 1
		else
			bgTexture.color = NGUIText.ParseColor24("90573B", 0)
			lblTexture.alpha = 0.4
		end
		CommonDefs.AddOnClickListener(child.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnTargetButtonClick(go) end), false)
		if fit then
			--animation
			LuaTweenUtils.DOKill(child, false)
			bgTexture.alpha = 0 -- 先隐藏
			-- 0.2s显现
			local tweener1 = LuaTweenUtils.TweenScale(child, Vector3(2,2,2), Vector3.one, 0.2)
			local tweener2 = LuaTweenUtils.TweenAlpha(bgTexture, 0, 1, 0.2)
			--延迟i*0.1s开始
			LuaTweenUtils.SetDelay(LuaTweenUtils.SetEase(tweener1, Ease.OutBack), itemDelayIdx * 0.3 + delay)
			LuaTweenUtils.SetDelay(LuaTweenUtils.SetEase(tweener2, Ease.OutBack), itemDelayIdx * 0.3 + delay)
			itemDelayIdx = itemDelayIdx + 1
		end
	end
end

function LuaNationalDayJCYWResultWnd:OnCloseButtonClick()
	self:Close()
end

function LuaNationalDayJCYWResultWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaNationalDayJCYWResultWnd:OnShareButtonClick()
	CUICommonDef.CaptureScreen(
        "screenshot",
        true,
        false,
        self.m_ShareButton,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            end
        ),
        false
    )
end

function LuaNationalDayJCYWResultWnd:OnTargetButtonClick(go)
	g_MessageMgr:ShowMessage("NationalDay_JCYW_Target_Desc")
end
