local CScene = import "L10.Game.CScene"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CTaskAndTeamWnd = import "L10.UI.CTaskAndTeamWnd"
local CSkillMgr = import "L10.Game.CSkillMgr"
local PlayerSettings = import "L10.Game.PlayerSettings"

LuaLiuYi2023Mgr = class()

LuaLiuYi2023Mgr.m_DDXGGamePlayId = nil
LuaLiuYi2023Mgr.m_DDXGIsSucess = false
LuaLiuYi2023Mgr.m_DDXGPassTime = 0
LuaLiuYi2023Mgr.m_DDXGPlayerInfos = {}
LuaLiuYi2023Mgr.m_DDXGResultPlayerInfos = {}
LuaLiuYi2023Mgr.m_DDXGRewards = {}

LuaLiuYi2023Mgr.m_BossGamePlayId = nil
LuaLiuYi2023Mgr.m_BossIsSuccess = false
LuaLiuYi2023Mgr.m_BossPassNpcHp = 0
LuaLiuYi2023Mgr.m_BossUseTime = 0
LuaLiuYi2023Mgr.m_BossRewards = {}
LuaLiuYi2023Mgr.m_BossNpcLeftHp = 0
LuaLiuYi2023Mgr.m_BossTaskName = ""
LuaLiuYi2023Mgr.m_BossDesc = ""

LuaLiuYi2023Mgr.m_ImagePaths = nil
LuaLiuYi2023Mgr.m_Msgs = nil
function LuaLiuYi2023Mgr:GetCommonImageRuleData()
    if self.m_ImagePaths == nil then
        self.m_ImagePaths = {}

        local skillStr = LiuYi2023_BossFight.GetData("RuleImagePaths").Value
        local strs = g_LuaUtil:StrSplit(skillStr,",")
        for i = 1, #strs do
            table.insert(self.m_ImagePaths, strs[i])
        end
    end
    if self.m_Msgs == nil then
        self.m_Msgs = {}

        local skillStr = LiuYi2023_BossFight.GetData("RuleMsgs").Value
        local strs = g_LuaUtil:StrSplit(skillStr,";")
        for i = 1, #strs do
            table.insert(self.m_Msgs, g_MessageMgr:FormatMessage(strs[i]))
        end
    end
    return self.m_ImagePaths, self.m_Msgs
end

function LuaLiuYi2023Mgr:ShowCommonImageRuleWnd()
    local imagePaths, msgs = self:GetCommonImageRuleData()
    LuaImageRuleMgr:ShowCommonImageRuleWnd(imagePaths, msgs)
end

function LuaLiuYi2023Mgr:OnMainPlayerCreated(playId)
    if playId == self:GetDDXGGamePlayId() then
        LuaCommonGamePlayTaskViewMgr:AppendGameplayInfo(playId, 
        true, nil,
        false,
        true, function() return Gameplay_Gameplay.GetData(playId).Name end,
        true, function() return g_MessageMgr:FormatMessage("LIUYI2023_DAODANXIAOGUI_TASKDESC") end,
        true, nil, nil,
        true, nil, function() return self:ShowCommonImageRuleWnd() end)
    end
    if CTaskAndTeamWnd.Instance then
        CTaskAndTeamWnd.Instance:ChangeToTaskTab()
    end
    PlayerSettings.IsUseTempSetting = true
    PlayerSettings.VisiblePlayerLimit = 35
    g_ScriptEvent:AddListener("UpdateGamePlayStageInfo", self, "OnUpdateGamePlayStageInfo")
end

function LuaLiuYi2023Mgr:OnMainPlayerDestroyed()
    PlayerSettings.IsUseTempSetting = false
    PlayerSettings.VisiblePlayerLimit = PlayerSettings.VisiblePlayerLimit
    g_ScriptEvent:RemoveListener("UpdateGamePlayStageInfo", self, "OnUpdateGamePlayStageInfo")
end

function LuaLiuYi2023Mgr:OnUpdateGamePlayStageInfo(gameplayId, title, content)
	if gameplayId == self:GetBossGamePlayId() then
		self.m_BossTaskName = title
        self.m_BossDesc = content
        g_ScriptEvent:BroadcastInLua("UpdateLiuYi2023BossStageInfo")
    end
end

function LuaLiuYi2023Mgr:GetBossGamePlayId()
    if self.m_BossGamePlayId == nil then
        self.m_BossGamePlayId = tonumber(LiuYi2023_BossFight.GetData("GameplayId").Value)
    end
    return self.m_BossGamePlayId
end

function LuaLiuYi2023Mgr:IsInBossPlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == self:GetBossGamePlayId() then
            return true
        end
    end
    return false
end

function LuaLiuYi2023Mgr:PlayerQueryTangGuoGuiDataResult(baseRewardTime, winRewardTime, joinTime)
    g_ScriptEvent:BroadcastInLua("PlayerQueryTangGuoGuiDataResult", baseRewardTime, winRewardTime, joinTime)
end

function LuaLiuYi2023Mgr:SyncTangGuoGuiNpcHp(NpcLeftHp)
    self.m_BossNpcLeftHp = NpcLeftHp
    g_ScriptEvent:BroadcastInLua("SyncTangGuoGuiNpcHp", NpcLeftHp)
end

function LuaLiuYi2023Mgr:TangGuoGuiPlayResult(isSuccess, passNpcHp, useTime, rewards)
    self.m_BossIsSuccess = isSuccess
    self.m_BossPassNpcHp = passNpcHp
    self.m_BossUseTime = useTime
    self.m_BossRewards = rewards

    CUIManager.ShowUI(CLuaUIResources.LiuYi2023BossResultWnd)
end

function LuaLiuYi2023Mgr:OpenLiuYi2023DaoDanXiaoGuiRules()
    self:ShowCommonImageRuleWnd()
end

function LuaLiuYi2023Mgr:GetDDXGGamePlayId()
    if self.m_DDXGGamePlayId == nil then
        self.m_DDXGGamePlayId = tonumber(LiuYi2023_DaoDanXiaoGui.GetData("GameplayId").Value)
    end
    return self.m_DDXGGamePlayId
end

function LuaLiuYi2023Mgr:IsInDDXGPlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == self:GetDDXGGamePlayId() then
            return true
        end
    end
    return false
end

function LuaLiuYi2023Mgr:PlayerQueryDaoDanXiaoGuiDataResult(todayJoinTime, todayRewardTime, passTime)
    g_ScriptEvent:BroadcastInLua("PlayerQueryDaoDanXiaoGuiDataResult", todayJoinTime, todayRewardTime, passTime)
end

function LuaLiuYi2023Mgr:SyncDaoDanXiaoGuiPlayInfo(playerInfos)
    local mainPlayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local find = -1
    for i = 1, #playerInfos do
        if playerInfos[i].playerId == mainPlayerId then 
            find = i 
            break
        end
    end
    if find > 0 then
        table.insert(playerInfos, 1, playerInfos[find])
    end
    self.m_DDXGPlayerInfos = playerInfos
    g_ScriptEvent:BroadcastInLua("SyncDaoDanXiaoGuiPlayInfo", playerInfos)
end

function LuaLiuYi2023Mgr:DaoDanXiaoGuiPlayResult(isSuccess, passTime, playerInfos, rewards)
    self.m_DDXGIsSucess = isSuccess
    self.m_DDXGPassTime = passTime
    self.m_DDXGResultPlayerInfos = playerInfos
    self.m_DDXGRewards = rewards

    CUIManager.ShowUI(CLuaUIResources.LiuYi2023DaoDanXiaoGuiResultWnd)
end

function LuaLiuYi2023Mgr:DaoDanXiaoGuiScanResult(isSuccess)
    local skillStr = LiuYi2023_DaoDanXiaoGui.GetData("TemSkillFx").Value
    local strs = g_LuaUtil:StrSplit(skillStr,";")
    local fxId = isSuccess and tonumber(strs[1]) or tonumber(strs[3])

    local ro = (CSkillMgr.Inst:GetPossessTarget(CClientMainPlayer.Inst) or {}).RO or CClientMainPlayer.Inst.RO
    local fx = CEffectMgr.Inst:AddObjectFX(fxId,ro,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
    ro:AddFX(fxId, fx, -1)
end

function LuaLiuYi2023Mgr:DaoDanXiaoGuiSeeThroughResult(isSuccess)
    local skillStr = LiuYi2023_DaoDanXiaoGui.GetData("TemSkillFx").Value
    local strs = g_LuaUtil:StrSplit(skillStr,";")
    local fxId = isSuccess and tonumber(strs[2]) or tonumber(strs[3])

    local ro = (CSkillMgr.Inst:GetPossessTarget(CClientMainPlayer.Inst) or {}).RO or CClientMainPlayer.Inst.RO
    local fx = CEffectMgr.Inst:AddObjectFX(fxId,ro,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
    ro:AddFX(fxId, fx, -1)
end

function LuaLiuYi2023Mgr:LiuYi2023FindDaoDanXiaoGui(playerId, class, gender)
    g_ScriptEvent:BroadcastInLua("LiuYi2023FindDaoDanXiaoGui", playerId, class, gender)
end