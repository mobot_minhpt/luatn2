require("3rdParty/ScriptEvent")
require("common/common_include")

local CHouseCompetitionMgr = import "L10.Game.CHouseCompetitionMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local Gac2Gas = import "L10.Game.Gac2Gas"
local DelegateFactory = import "DelegateFactory"
local MessageMgr = import "L10.Game.MessageMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local LocalString = import "LocalString"

CHouseCompetitionVictoryWnd=class()
RegistClassMember(CHouseCompetitionVictoryWnd,"m_HouseList")

function CHouseCompetitionVictoryWnd:Awake()
	self.m_HouseList = {}
	for i = 0, 5 do
		local go = self.transform:Find(SafeStringFormat3("%dthItem", i+1)).gameObject
		if go then
			self.m_HouseList[i] = go

			local index = i
			CommonDefs.AddOnClickListener(go,DelegateFactory.Action_GameObject(function(go)
		            self:OnHouseClicked(index)
		        end),false)
		end
	end


end

function CHouseCompetitionVictoryWnd:OnHouseClicked(index)
	local houseInfo = CHouseCompetitionMgr.Inst.VictoryHouseList[index]
	if not houseInfo then return end
	local ownerId = houseInfo.OwnerId
	local text = MessageMgr.Inst:FormatMessage("House_Competition_Enter_Confirm",{houseInfo.OwnerName.. LocalString.GetString("的家")})
    MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(function () 
		Gac2Gas.RequestEnterHouseCompetitionYard(ownerId)
        CUIManager.CloseUI(CUIResources.CHouseCompetitionVictoryWnd)
    end), nil, nil, nil, false)
end


function CHouseCompetitionVictoryWnd:InitSingleHouseInfo(go, houseInfo)
	local classTextureGO = go.transform:Find("ClassTexture").gameObject
	local classTexture = CommonDefs.GetComponent_GameObject_Type(classTextureGO, typeof(CUITexture))
	local portraitName = CUICommonDef.GetPortraitName(houseInfo.Class, houseInfo.Gender, houseInfo.Expression)
	classTexture:LoadNPCPortrait(portraitName)

	local nameLabelGO = go.transform:Find("Label").gameObject
	local nameLabel = CommonDefs.GetComponent_GameObject_Type(nameLabelGO, typeof(UILabel))
	nameLabel.text = houseInfo.OwnerName
end

function CHouseCompetitionVictoryWnd:OnDestroy()

end

function CHouseCompetitionVictoryWnd:OnDisable()

end

function CHouseCompetitionVictoryWnd:Init()
	for i = 0, 5 do
		local houseInfo = CHouseCompetitionMgr.Inst.VictoryHouseList[i]
		local go = self.m_HouseList[i]
		if houseInfo and go then
			self:InitSingleHouseInfo(go, houseInfo)
		end
	end
end

return CHouseCompetitionVictoryWnd








