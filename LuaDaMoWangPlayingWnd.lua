local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaDaMoWangPlayingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaMoWangPlayingWnd, "ExpandButton", "ExpandButton", CButton)
RegistChildComponent(LuaDaMoWangPlayingWnd, "TemplateTip1", "TemplateTip1", GameObject)
RegistChildComponent(LuaDaMoWangPlayingWnd, "TemplateTip2", "TemplateTip2", GameObject)
RegistChildComponent(LuaDaMoWangPlayingWnd, "TemplateTip3", "TemplateTip3", GameObject)
RegistChildComponent(LuaDaMoWangPlayingWnd, "Line1", "Line1", GameObject)
RegistChildComponent(LuaDaMoWangPlayingWnd, "Line2", "Line2", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaMoWangPlayingWnd, "m_BossTipTextTable")
RegistClassMember(LuaDaMoWangPlayingWnd, "m_ChallengerTipTextTable")
--RegistClassMember(LuaDaMoWangPlayingWnd, "m_HPUpdateListener")
RegistClassMember(LuaDaMoWangPlayingWnd, "m_BossDropAward")
RegistClassMember(LuaDaMoWangPlayingWnd, "m_PlayerKillAward")
function LuaDaMoWangPlayingWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)

    --@endregion EventBind end
end

function LuaDaMoWangPlayingWnd:Init()
	-- 读表，魔王和挑战者双方任务
	LuaShuJia2021Mgr.InitInfo()
	self:InitAwardTable()
end

-- 初始化奖励列表
function LuaDaMoWangPlayingWnd:InitAwardTable()
	-- 读表奖励物品信息
	local awardIdTable = LuaShuJia2021Mgr.awardIdTable
	
	self.m_BossDropAward = {false, false, false}
	self.m_PlayerKillAward = {false,false,false}
	-- 初始化奖励图标
	self.TemplateTip1.transform:Find("AwardItemCell"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init(awardIdTable[1])
	self.TemplateTip2.transform:Find("AwardItemCell"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init(awardIdTable[2])
	self.TemplateTip3.transform:Find("AwardItemCell"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init(awardIdTable[3])

	self:SetAwardComplete(self.TemplateTip1,1,false,false)
	self:SetAwardComplete(self.TemplateTip2,2,false,false)
	self:SetAwardComplete(self.TemplateTip3,3,false,false)
	self:SetLineActive(self.Line1,false)
	self:SetLineActive(self.Line2,false)

end

-- 根据进度更新显示
function LuaDaMoWangPlayingWnd:UpdateInfo()
	local achieveTask = {}
	local m_needShowFx = LuaShuJia2021Mgr.needShowFx
	self:CheckBossHp()
	if LuaShuJia2021Mgr.isBoss then
		achieveTask = self.m_PlayerKillAward
		self:SetAwardComplete(self.TemplateTip1,1,achieveTask[1],m_needShowFx[1])
		self:SetAwardComplete(self.TemplateTip2,2,achieveTask[2],m_needShowFx[2])
		self:SetAwardComplete(self.TemplateTip3,3,achieveTask[3],m_needShowFx[3])
		self:SetLineActive(self.Line1,achieveTask[2])
		self:SetLineActive(self.Line2,achieveTask[3])
	else
		achieveTask = self.m_BossDropAward
		self:SetAwardComplete(self.TemplateTip1,1,achieveTask[1],m_needShowFx[1])
		self:SetAwardComplete(self.TemplateTip2,2,achieveTask[2],m_needShowFx[2])
		self:SetAwardComplete(self.TemplateTip3,3,achieveTask[3],m_needShowFx[3])
		self:SetLineActive(self.Line1,achieveTask[2])
		self:SetLineActive(self.Line2,achieveTask[3])
	end
end
-- 奖励图标显示
function LuaDaMoWangPlayingWnd:SetAwardComplete(go,rank,Complete,ShowFx)
	if go == nil then return end
	if not rank or rank <= 0 or rank > 3 then return end
	
	local awardItemCell = go.transform:Find("AwardItemCell"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
	local tipLabel = go.transform:Find("TipLabel"):GetComponent(typeof(UILabel))

	
	if ShowFx and Complete then
		LuaShuJia2021Mgr.ShowFuBenFx(go)
		
	end
	awardItemCell:SetAwardComplete(Complete)	-- 图标显示更新
	-- label显示更新
	if LuaShuJia2021Mgr.isBoss then 
		tipLabel.text = LuaShuJia2021Mgr.bossTipTextTable[rank]
	else
		tipLabel.text = LuaShuJia2021Mgr.challengerTipTextTable[rank]
	end

	if Complete then
		tipLabel.text = "[c][00ff60]"..tipLabel.text .. "[-][c]"
	else
		tipLabel.text = "[c][ffffff]"..tipLabel.text .. "[-][c]"
	end
end
-- 连线是否变亮
function LuaDaMoWangPlayingWnd:SetLineActive(go, Active)
	if go == nil then return end
	local activeGo = go.transform:Find("ActiveLine").gameObject
	activeGo:SetActive(Active)
end

function LuaDaMoWangPlayingWnd:CheckBossHp()
	for i=1,#LuaShuJia2021Mgr.ChallengerTipHpPrecentTable do
		local complete = LuaShuJia2021Mgr.bossHpPrecent >= LuaShuJia2021Mgr.ChallengerTipHpPrecentTable[i]
		self.m_BossDropAward[i] = complete
	end
	local LossTimes =LuaShuJia2021Mgr.maxChallengeTimes - LuaShuJia2021Mgr.leftChallengeTimes
	local KillTimes = LuaShuJia2021Mgr.killTimes
	for i=1,#LuaShuJia2021Mgr.lostTimeToReward do
		local completeLoss = false
		local completeKill = false
		if tonumber(LuaShuJia2021Mgr.lostTimeToReward[i]) ~= -1 then
			completeLoss = LossTimes >= tonumber(LuaShuJia2021Mgr.lostTimeToReward[i])
		end
		if tonumber(LuaShuJia2021Mgr.KillPlayerTimeToReward[i]) ~= -1 then
			completeKill = KillTimes >= tonumber(LuaShuJia2021Mgr.KillPlayerTimeToReward[i])
		end
		self.m_PlayerKillAward[i] = completeLoss or completeKill
	end
end

function LuaDaMoWangPlayingWnd:OnEnable()
	g_ScriptEvent:AddListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
	g_ScriptEvent:AddListener("UpdateDaMoWangPlayInfo",self, "UpdateInfo")
end

function LuaDaMoWangPlayingWnd:OnDisable()
	g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
	g_ScriptEvent:RemoveListener("UpdateDaMoWangPlayInfo",self, "UpdateInfo")
	if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
		CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
	end

end
--@region UIEvent
-- 按钮方向反转，显示原UI
function LuaDaMoWangPlayingWnd:OnExpandButtonClick()
	self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaDaMoWangPlayingWnd:OnHideTopAndRightTipWnd()
	self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

--@endregion UIEvent

