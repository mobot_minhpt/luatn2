require("common/common_include")

local CUITexture = import "L10.UI.CUITexture"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"

LuaXianZhiTypeDescWnd = class()

RegistChildComponent(LuaXianZhiTypeDescWnd, "XianZhiTypeIcon", CUITexture)
RegistChildComponent(LuaXianZhiTypeDescWnd, "DescLabel", UILabel)
RegistChildComponent(LuaXianZhiTypeDescWnd, "DescPanel", CUIRestrictScrollView)

function LuaXianZhiTypeDescWnd:Init()
	local xianZhiType = XianZhi_TitleType.GetData(CLuaXianzhiMgr.m_XianZhiTypeId)
	if not xianZhiType then
		CUIManager.CloseUI(CLuaUIResources.XianZhiTypeDescWnd)
		return
	end

	self.XianZhiTypeIcon:LoadSkillIcon(StringTrim(xianZhiType.Icon))
	self.DescLabel.text = xianZhiType.Des
	self.DescPanel:ResetPosition()
end

function LuaXianZhiTypeDescWnd:OnEnable()
	-- g_ScriptEvent:AddListener("AcceptTaskFromBabySuccess", self, "UpdateDiaryAlert")
end

function LuaXianZhiTypeDescWnd:OnDisable()
	-- g_ScriptEvent:RemoveListener("AcceptTaskFromBabySuccess", self, "UpdateDiaryAlert")
end

return LuaXianZhiTypeDescWnd
