local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local Constants=import "L10.Game.Constants"

--有2个行囊、绑仓
CLuaWarehouseMgr = {}


--行囊中物品的数量
function CLuaWarehouseMgr.GetXingNangItemCount()
    if not CClientMainPlayer.Inst then return 0 end
    local repo = CClientMainPlayer.Inst.RepertoryProp

    local count=0
    for i=Constants.SingeWarehouseSize * 3 + 1,Constants.SingeWarehouseSize * (3 + CLuaWarehouseView.m_XingNangNum) do
        local itemId = CClientMainPlayer.Inst.RepertoryProp:GetItemAt(i)
        if itemId~=nil and itemId~="" then
            count=count+1
        end
    end
    return count
end
