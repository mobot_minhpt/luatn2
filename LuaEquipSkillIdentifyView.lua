require("common/common_include")

local CUITexture = import "L10.UI.CUITexture"
local CItemMgr = import "L10.Game.CItemMgr"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local UIScrollView = import "UIScrollView"
local CCurrentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CButton = import "L10.UI.CButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local Item_Item = import "L10.Game.Item_Item"
local CCommonItemSelectInfoMgr = import "L10.UI.CCommonItemSelectInfoMgr"
local IdentifySkill_IdentifySkill = import "L10.Game.IdentifySkill_IdentifySkill"
local MessageWndManager = import "L10.UI.MessageWndManager"
local LocalString = import "LocalString"
local CUIFx = import "L10.UI.CUIFx"
local LuaTweenUtils = import "LuaTweenUtils"
local Ease = import "DG.Tweening.Ease"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaEquipSkillIdentifyView = class()
RegistClassMember(LuaEquipSkillIdentifyView,"inited")
RegistClassMember(LuaEquipSkillIdentifyView,"gameObject")
RegistClassMember(LuaEquipSkillIdentifyView,"transform")

--选择的装备
RegistClassMember(LuaEquipSkillIdentifyView,"m_CurEquipIcon")
RegistClassMember(LuaEquipSkillIdentifyView,"m_CurEquipQualityFx")
RegistClassMember(LuaEquipSkillIdentifyView,"m_ChangeEquipButton")
RegistClassMember(LuaEquipSkillIdentifyView,"m_SelectEquipButton")
RegistClassMember(LuaEquipSkillIdentifyView,"m_CurEquipSkillRoot")
RegistClassMember(LuaEquipSkillIdentifyView,"m_CurEquipSkillIcon")

--已有的技能
RegistClassMember(LuaEquipSkillIdentifyView,"m_CurSkillRoot")
RegistClassMember(LuaEquipSkillIdentifyView,"m_CurSkillIcon")
RegistClassMember(LuaEquipSkillIdentifyView,"m_CurSkillLevelLabel")
RegistClassMember(LuaEquipSkillIdentifyView,"m_CurSkillNameLabel")
RegistClassMember(LuaEquipSkillIdentifyView,"m_CurSkillScrollView")
RegistClassMember(LuaEquipSkillIdentifyView,"m_CurSkillDescLabel")
RegistClassMember(LuaEquipSkillIdentifyView,"m_CurSkillFx")

--洗出的技能
RegistClassMember(LuaEquipSkillIdentifyView,"m_TmpSkillRoot")
RegistClassMember(LuaEquipSkillIdentifyView,"m_TmpSkillIcon")
RegistClassMember(LuaEquipSkillIdentifyView,"m_TmpSkillLevelLabel")
RegistClassMember(LuaEquipSkillIdentifyView,"m_TmpSkillNameLabel")
RegistClassMember(LuaEquipSkillIdentifyView,"m_TmpSkillScrollView")
RegistClassMember(LuaEquipSkillIdentifyView,"m_TmpSkillDescLabel")


RegistClassMember(LuaEquipSkillIdentifyView,"m_ReplaceArrow")

RegistClassMember(LuaEquipSkillIdentifyView,"m_NeedItemRoot")
RegistClassMember(LuaEquipSkillIdentifyView,"m_NeedItemIcon")
RegistClassMember(LuaEquipSkillIdentifyView,"m_NeedItemAmountLabel")
RegistClassMember(LuaEquipSkillIdentifyView,"m_NeedItemQualitySprite")
RegistClassMember(LuaEquipSkillIdentifyView,"m_NeedItemDisableSprite")
RegistClassMember(LuaEquipSkillIdentifyView,"m_NeedItemAddSprite")
RegistClassMember(LuaEquipSkillIdentifyView,"m_NeedItemSelectSprite")
RegistClassMember(LuaEquipSkillIdentifyView,"m_CurrentMoneyCtrl")

RegistClassMember(LuaEquipSkillIdentifyView,"m_ReplaceButton")
RegistClassMember(LuaEquipSkillIdentifyView,"m_IdentifyButton")




RegistClassMember(LuaEquipSkillIdentifyView,"m_ItemId")
RegistClassMember(LuaEquipSkillIdentifyView,"m_CurWashItemType")
RegistClassMember(LuaEquipSkillIdentifyView,"m_CandidateItemsTbl")
RegistClassMember(LuaEquipSkillIdentifyView,"m_SelectableCostItem")

function LuaEquipSkillIdentifyView:InitWithGameObject(viewRootGo)
	if self.inited then
		return
	end
	self.gameObject = viewRootGo
	self.transform = viewRootGo.transform

	self.m_CurEquipIcon = self.transform:Find("CurEquip/Bg/EquipIcon"):GetComponent(typeof(CUITexture))
	self.m_CurEquipQualityFx = self.transform:Find("CurEquip/Bg/EquipIcon/QualityFx"):GetComponent(typeof(CUIFx))
	self.m_ChangeEquipButton = self.transform:Find("CurEquip/Bg/ChangeButton").gameObject
	self.m_SelectEquipButton = self.transform:Find("CurEquip/Bg/SelectButton").gameObject
	self.m_CurEquipSkillRoot = self.transform:Find("CurEquip/Bg/Skill").gameObject
	self.m_CurEquipSkillIcon = self.transform:Find("CurEquip/Bg/Skill/SkillIcon"):GetComponent(typeof(CUITexture))

	self.m_CurSkillRoot = self.transform:Find("CurEquip/CurSkill").gameObject
	self.m_CurSkillIcon = self.transform:Find("CurEquip/CurSkill/Bg/IconTexture"):GetComponent(typeof(CUITexture))
	self.m_CurSkillLevelLabel = self.transform:Find("CurEquip/CurSkill/Bg/LevelLabel"):GetComponent(typeof(UILabel))
	self.m_CurSkillNameLabel = self.transform:Find("CurEquip/CurSkill/Bg/NameLabel"):GetComponent(typeof(UILabel))
	self.m_CurSkillScrollView = self.transform:Find("CurEquip/CurSkill/ScrollView"):GetComponent(typeof(UIScrollView))
	self.m_CurSkillDescLabel = self.transform:Find("CurEquip/CurSkill/ScrollView/Label"):GetComponent(typeof(UILabel))
	self.m_CurSkillFx = self.transform:Find("CurEquip/CurSkill/Bg/SkillFx"):GetComponent(typeof(CUIFx))

	self.m_TmpSkillRoot = self.transform:Find("CurEquip/TmpSkill").gameObject
	self.m_TmpSkillIcon = self.transform:Find("CurEquip/TmpSkill/Bg/IconTexture"):GetComponent(typeof(CUITexture))
	self.m_TmpSkillLevelLabel = self.transform:Find("CurEquip/TmpSkill/Bg/LevelLabel"):GetComponent(typeof(UILabel))
	self.m_TmpSkillNameLabel = self.transform:Find("CurEquip/TmpSkill/Bg/NameLabel"):GetComponent(typeof(UILabel))
	self.m_TmpSkillScrollView = self.transform:Find("CurEquip/TmpSkill/ScrollView"):GetComponent(typeof(UIScrollView))
	self.m_TmpSkillDescLabel = self.transform:Find("CurEquip/TmpSkill/ScrollView/Label"):GetComponent(typeof(UILabel))

	self.m_ReplaceArrow = self.transform:Find("CurEquip/Arrow").gameObject

	self.m_NeedItemRoot = self.transform:Find("CostRoot/ItemCell").gameObject
	self.m_NeedItemIcon = self.transform:Find("CostRoot/ItemCell/IconTexture"):GetComponent(typeof(CUITexture))
	self.m_NeedItemAmountLabel = self.transform:Find("CostRoot/ItemCell/AmountLabel"):GetComponent(typeof(UILabel))
	self.m_NeedItemQualitySprite = self.transform:Find("CostRoot/ItemCell/QualitySprite"):GetComponent(typeof(UISprite))
	self.m_NeedItemDisableSprite = self.transform:Find("CostRoot/ItemCell/DisableSprite"):GetComponent(typeof(UISprite))
	self.m_NeedItemAddSprite = self.transform:Find("CostRoot/ItemCell/AddSprite"):GetComponent(typeof(UISprite))
	self.m_CurrentMoneyCtrl = self.transform:Find("CostRoot/YinLiangCost"):GetComponent(typeof(CCurrentMoneyCtrl))
	self.m_NeedItemSelectSprite = self.transform:Find("CostRoot/ItemCell/SelectSprite"):GetComponent(typeof(UISprite))

	self.m_ReplaceButton = self.transform:Find("ReplaceButton"):GetComponent(typeof(CButton))
	self.m_IdentifyButton = self.transform:Find("IdentifyButton"):GetComponent(typeof(CButton))

	CommonDefs.AddOnClickListener(self.m_ChangeEquipButton, DelegateFactory.Action_GameObject(function(go) self:OnChangeEquipButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_SelectEquipButton, DelegateFactory.Action_GameObject(function(go) self:OnChangeEquipButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_ReplaceButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnReplaceButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_IdentifyButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnIdentifyButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_NeedItemRoot, DelegateFactory.Action_GameObject(function (go) self:OnNeedItemClick() end), false)
	self.inited = true

	self.m_CurWashItemType = nil
end

function LuaEquipSkillIdentifyView:SetDefaultItem(itemId)
	local item = CItemMgr.Inst:GetById(itemId)
	if item and item.IsEquip and item.Equip.Identifiable > 0 then
		self.m_ItemId = itemId
	else
		self.m_ItemId = nil
	end
end

function LuaEquipSkillIdentifyView:Init()
	if not self.inited then return end
	self.m_CandidateItemsTbl = nil
	self.m_ReplaceButton.Enabled = false
	self.m_NeedItemRoot:SetActive(false) --先隐藏道具消耗信息
	self.m_CurrentMoneyCtrl.gameObject:SetActive(false)--先隐藏银两消耗信息
	self.m_CurSkillFx:DestroyFx() --移除新技能特效
	self.m_IdentifyButton.Enabled = false --先禁掉，等消耗信息更新的时候再开启
	local item = CItemMgr.Inst:GetById(self.m_ItemId)
	if item and item.IsEquip and item.Equip.Identifiable > 0 then
		self.m_CurEquipIcon:LoadMaterial(item.Equip.BigIcon)
		if item.Equip.UseSquareBigIcon then
			self.m_CurEquipIcon.texture.height = self.m_CurEquipIcon.texture.width
		else
			self.m_CurEquipIcon.texture.height = self.m_CurEquipIcon.texture.width * 2
		end
		if item.Equip.IsPurpleEquipment or item.Equip.IsGhostEquipment then
			self.m_CurEquipQualityFx:LoadFx("Fx/UI/Prefab/UI_zhuangbeijianding_zise.prefab")
		elseif item.Equip.IsRedEquipment then
			self.m_CurEquipQualityFx:LoadFx("Fx/UI/Prefab/UI_zhuangbeijianding_hongse.prefab")
		elseif item.Equip.IsBlueEquipment then
			self.m_CurEquipQualityFx:LoadFx("Fx/UI/Prefab/UI_zhuangbeijianding_lanse.prefab")
		else
			self.m_CurEquipQualityFx:DestroyFx()
		end
		local skill = Skill_AllSkills.GetData(item.Equip.FixedIdentifySkillId)
		self:InitCurSkillInfo(skill, item.Equip)
		local tmpSkill = Skill_AllSkills.GetData(item.Equip.TempIdentifySkillId)
		self:InitTmpSkillInfo(tmpSkill, item.Equip)

		if skill and tmpSkill then
			self.m_CurEquipSkillRoot:SetActive(true)
			self.m_CurEquipSkillIcon:LoadSkillIcon(skill.SkillIcon)
		elseif not skill and not tmpSkill then
			self.m_CurEquipSkillRoot:SetActive(true)
			self.m_CurEquipSkillIcon:Clear()
		else
			self.m_CurEquipSkillRoot:SetActive(false)
			self.m_CurEquipSkillIcon:Clear()
		end

		if tmpSkill then
			self.m_ReplaceArrow:SetActive(true)
		else
			self.m_ReplaceArrow:SetActive(false)
		end 

		if skill and tmpSkill then
			Extensions.SetLocalPositionX(self.m_CurSkillRoot.transform, -430)
			Extensions.SetLocalPositionX(self.m_TmpSkillRoot.transform, 430)
		elseif skill then
			Extensions.SetLocalPositionX(self.m_CurSkillRoot.transform, 0)
		end

		self.m_ReplaceButton.Enabled = (skill~=nil and tmpSkill~=nil)
		if skill then
			self.m_IdentifyButton.Text = LocalString.GetString("再次鉴定")
		else
			self.m_IdentifyButton.Text = LocalString.GetString("鉴定技能")
		end
		self.m_ChangeEquipButton:SetActive(true)
		self.m_SelectEquipButton:SetActive(false)
		--请求消耗信息
		Gac2Gas.IdentifySkillRequestWashSkillCost(self.m_ItemId)
	else
		self.m_IdentifyButton.Text = LocalString.GetString("鉴定技能")
		self.m_ChangeEquipButton:SetActive(false)
		self.m_SelectEquipButton:SetActive(true)
		self.m_CurEquipIcon:Clear()
		self.m_CurEquipQualityFx:DestroyFx()
		self.m_CurEquipSkillIcon:Clear()
		self:InitCurSkillInfo(nil, nil)
		self.m_CurSkillFx:DestroyFx()
		self.m_ReplaceArrow:SetActive(false)
		self:InitTmpSkillInfo(nil, nil)
	end
end

function LuaEquipSkillIdentifyView:InitCurSkillInfo(skill, equip)
	if skill then
		self.m_CurSkillRoot:SetActive(true)
		self.m_CurSkillIcon:LoadSkillIcon(skill.SkillIcon)
		self.m_CurSkillLevelLabel.text = SafeStringFormat3("lv.%d", skill.Level)
		self.m_CurSkillNameLabel.text = skill.Name
		local passiveStr = ""
		if CLuaDesignMgr.Skill_AllSkills_GetIsPassiveSkill(skill) then
			passiveStr = LocalString.GetString("被动 ")
		end
		self.m_CurSkillDescLabel.text = SafeStringFormat3("[c][EF7C10]%s[-][/c]%s\n%s", passiveStr, skill.TranslatedDisplay, equip.IdentifySkillExpireTimeInfo)
	else
		self.m_CurSkillRoot:SetActive(false)
		self.m_CurSkillIcon:Clear()
		self.m_CurSkillLevelLabel.text = nil
		self.m_CurSkillNameLabel.text = nil
		self.m_CurSkillDescLabel.text = nil
	end
	self.m_CurSkillScrollView:ResetPosition()
end

function LuaEquipSkillIdentifyView:InitTmpSkillInfo(skill, equip)
	if skill then
		self.m_TmpSkillRoot:SetActive(true)
		self.m_TmpSkillIcon:LoadSkillIcon(skill.SkillIcon)
		self.m_TmpSkillLevelLabel.text = SafeStringFormat3("lv.%d", skill.Level)
		self.m_TmpSkillNameLabel.text = skill.Name
		local passiveStr = ""
		if CLuaDesignMgr.Skill_AllSkills_GetIsPassiveSkill(skill) then
			passiveStr = LocalString.GetString("被动 ")
		end
		self.m_TmpSkillDescLabel.text = SafeStringFormat3("[c][EF7C10]%s[-][/c]%s\n%s", passiveStr, skill.TranslatedDisplay, equip.IdentifyTempSkillExpireTimeInfo)
	else
		self.m_TmpSkillRoot:SetActive(false)
		self.m_TmpSkillIcon:Clear()
		self.m_TmpSkillLevelLabel.text = nil
		self.m_TmpSkillNameLabel.text = nil
		self.m_TmpSkillDescLabel.text = nil
	end
	self.m_TmpSkillScrollView:ResetPosition()
end

function LuaEquipSkillIdentifyView:OnSendItem(itemId)
	if self.m_ItemId == itemId then
		self:Init()
	elseif self.m_CandidateItemsTbl then
		--如果消耗道具数量发生变化，更新一下
		local commonItem = CItemMgr.Inst:GetById(itemId)
		if not commonItem or not commonItem.IsItem then
			return
		end
		for _, data in pairs(self.m_CandidateItemsTbl) do
			if self.m_CurWashItemType == data.washItemType then
				if data.itemTemplateId == commonItem.TemplateId then
					self:UpdateCostInfo(self.m_ItemId, self.m_CandidateItemsTbl)
				end
				break
			end
		end
	end
end

function LuaEquipSkillIdentifyView:OnSetItemAt(place, pos, oldItemId, newItemId)
	self:UpdateCostInfo(self.m_ItemId, self.m_CandidateItemsTbl)
end

function LuaEquipSkillIdentifyView:UpdateCostInfo(equipId, candidateItemsTbl)
	if not self.inited then return end
	if self.m_ItemId ~= equipId then return end

	self.m_CandidateItemsTbl = candidateItemsTbl

	if self.m_CandidateItemsTbl then
		self.m_IdentifyButton.Enabled = true
	else
		self.m_IdentifyButton.Enabled = false
	end
	local info = nil

	if candidateItemsTbl then
		for _, data in pairs(candidateItemsTbl) do
			if self.m_CurWashItemType == data.washItemType then
				info = data
				break
			end
		end
	end

	local onlyCostMoney = true
	local needMoney = 0
	if self.m_CandidateItemsTbl then
		for _, data in pairs(self.m_CandidateItemsTbl) do
			if data.num >= 1 then
				onlyCostMoney = false
				break
			else
				needMoney = data.needMoney
			end
		end
	end

	if onlyCostMoney and needMoney and needMoney > 0 then
		self.m_NeedItemRoot:SetActive(false)
		self.m_CurrentMoneyCtrl:SetType(EnumMoneyType.YinLiang)
		self.m_CurrentMoneyCtrl:SetCost(needMoney)
		self.m_CurrentMoneyCtrl.gameObject:SetActive(true)
	elseif info == nil then
		self.m_NeedItemRoot:SetActive(true)
		self.m_NeedItemIcon:Clear()
		self.m_NeedItemAmountLabel.text = nil
		self.m_NeedItemDisableSprite.enabled = true
		self.m_NeedItemAddSprite.enabled = true
		self.m_CurrentMoneyCtrl.gameObject:SetActive(false)
	else
		local item = Item_Item.GetData(info.itemTemplateId)
		if not item or info.num < 1 then
			self.m_NeedItemRoot:SetActive(false)
		else
			self.m_NeedItemRoot:SetActive(true)
			self.m_NeedItemIcon:LoadMaterial(item.Icon)
			local ownCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, info.itemTemplateId)

			ownCount = self.m_SelectableCostItem and self:GetSelectableCostItemCount() or ownCount

			if ownCount>=info.num then
				self.m_NeedItemAmountLabel.text = SafeStringFormat3("%d/%d", ownCount, info.num)
			else
				self.m_NeedItemAmountLabel.text = SafeStringFormat3("[c][ff0000]%d[-][/c]/%d", ownCount, info.num)
			end
			self.m_NeedItemDisableSprite.enabled = (ownCount < info.num)
			self.m_NeedItemAddSprite.enabled = false
		end

		self.m_CurrentMoneyCtrl:SetType(EnumMoneyType.YinLiang)
		self.m_CurrentMoneyCtrl:SetCost(info.needMoney)
		self.m_CurrentMoneyCtrl.gameObject:SetActive(info.needMoney > 0)
	end

	if self.m_NeedItemRoot.activeSelf then
		Extensions.SetLocalPositionX(self.m_CurrentMoneyCtrl.gameObject.transform, 40)
	else
		Extensions.SetLocalPositionX(self.m_CurrentMoneyCtrl.gameObject.transform, -120)
	end
end

function LuaEquipSkillIdentifyView:OnChangeEquipButtonClick()
	if not self.inited then return end

	CCommonItemSelectInfoMgr.Inst:ShowIdentifyEquipSelectWnd(DelegateFactory.Action_string_uint(function (itemId, templateId)
		self:SetDefaultItem(itemId)
		self:Init()
		self.m_CurWashItemType = nil
		self.m_SelectableCostItem = nil
	end))
end

function LuaEquipSkillIdentifyView:OnReplaceButtonClick()
	if not self.inited then return end
	if self.m_ItemId == nil then return end

	local commonItem = CItemMgr.Inst:GetById(self.m_ItemId)
	local equip = commonItem and commonItem.Equip
	if not equip then return end

	local endTime = CServerTimeMgr.ConvertTimeStampToZone8Time(equip.IdentifyEndTime)
	local now = CServerTimeMgr.Inst:GetZone8Time()
	local span = endTime:Subtract(now)
	if span.TotalDays > 30 then
		Gac2Gas.IdentifySkillRequestApplyTempSkill(self.m_ItemId, true) --有效期超过30天请求二次确认
	else
		Gac2Gas.IdentifySkillRequestApplyTempSkill(self.m_ItemId, false) --没超过30天直接替换
	end
end

function LuaEquipSkillIdentifyView:OnIdentifyButtonClick()
	if not self.inited then return end
	if self.m_ItemId == nil then return end
	if not self.m_CurWashItemType then
		--如果不是只消耗银两，那么提示选择道具并返回
		local onlyCostMoney = true
		if self.m_CandidateItemsTbl then
			for _, data in pairs(self.m_CandidateItemsTbl) do
				if data.num >= 1 then
					onlyCostMoney = false
					break
				end
			end
		end
		if not onlyCostMoney then
			g_MessageMgr:ShowMessage("Equip_ReIdentify_Fail_NoChoiceItem")
			return
		end
	end

	local item = CItemMgr.Inst:GetById(self.m_ItemId)
	if item and item.IsEquip and item.Equip.Identifiable > 0 then
		local tmpSkill = Skill_AllSkills.GetData(item.Equip.TempIdentifySkillId)
		if tmpSkill and IdentifySkill_IdentifySkill.GetData(item.Equip.TempIdentifySkillId/100).IsGoodSkill ==1 then
			local msg = g_MessageMgr:FormatMessage("Equip_ReIdentify_LoseGoodSkill_Confirm")
			MessageWndManager.ShowOKCancelMessage(msg, 
				DelegateFactory.Action(function() self:DoRequestWashSkill() end),
				 nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
		elseif not item.IsBinded then
			local msg = g_MessageMgr:FormatMessage("Equip_Identify_Bind_Confirm")
			MessageWndManager.ShowOKCancelMessage(msg, 
				DelegateFactory.Action(function() self:DoRequestWashSkill() end),
				 nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
		else
			self:DoRequestWashSkill()
		end
	end
end

function LuaEquipSkillIdentifyView:DoRequestWashSkill()
	if not self.inited then return end
	if not self.m_ItemId then return end
	if not self.m_CurWashItemType then
		local onlyCostMoney = true
		if self.m_CandidateItemsTbl then
			for _, data in pairs(self.m_CandidateItemsTbl) do
				if data.num > 1 then
					onlyCostMoney = false
					break
				end
			end
		end
		if not onlyCostMoney then
			return
		end
	end

	local ownCount = self:GetSelectableCostItemCount()
	if ownCount then
		local info = nil

		if self.m_CandidateItemsTbl then
			for _, data in pairs(self.m_CandidateItemsTbl) do
				if self.m_CurWashItemType == data.washItemType then
					info = data
					break
				end
			end
		end
		if info and ownCount < info.num then
			g_MessageMgr:ShowMessage("Equip_Identify_Lack_CostItem")
			return
		end
	end

		--这时候如果 m_CurWashItemType == nil 说明是onlyCostMoney
	Gac2Gas.IdentifySkillRequestWashSkill(self.m_ItemId, self.m_CurWashItemType or 0)
end

function LuaEquipSkillIdentifyView:OnNeedItemClick()
	if not self.m_CandidateItemsTbl or #self.m_CandidateItemsTbl < 2 then return end

	self.m_NeedItemSelectSprite.enabled = true

	local commonItem = CItemMgr.Inst:GetById(self.m_ItemId)
	local equip = commonItem and commonItem.Equip
	LuaEquipIdentifySkillMgr:ShowItemChooseWnd(function(selectableCostItem, washItemType)
		self.m_CurWashItemType = washItemType
		self.m_SelectableCostItem = selectableCostItem
		self:UpdateCostInfo(self.m_ItemId, self.m_CandidateItemsTbl)
	end, equip.NeedLevel, function()
		self.m_NeedItemSelectSprite.enabled = false
	end, self.m_SelectableCostItem)
end

function LuaEquipSkillIdentifyView:IdentifySkillRequestWashSkillSuccess(equipId, washItemType, curSkillChanged, tmpSkillChanged, oldTmpSkillId, newTmpSkillId)
	if self.m_ItemId and self.m_ItemId == equipId then
		local item = CItemMgr.Inst:GetById(self.m_ItemId)
		if not item or not item.IsEquip then return end

		local curSkillExists = Skill_AllSkills.Exists(item.Equip.FixedIdentifySkillId)
		local tmpSkillExists = Skill_AllSkills.Exists(item.Equip.TempIdentifySkillId)

		if curSkillExists and not tmpSkillExists then
			self.m_CurSkillFx:DestroyFx()
			self.m_CurSkillFx:LoadFx("Fx/UI/Prefab/UI_jinengjianding_01.prefab")
		end

		if tmpSkillExists then
			Extensions.SetLocalPositionX(self.m_TmpSkillRoot.transform, 430 * 4)
			--播放动画
			local tmpSkillAniLength = 0.5
			LuaTweenUtils.SetEase(LuaTweenUtils.TweenPositionX(self.m_TmpSkillRoot.transform, 430, tmpSkillAniLength), Ease.OutCubic)
			LuaTweenUtils.TweenAlpha(self.m_TmpSkillRoot.transform, 0, 1 , tmpSkillAniLength)

			if curSkillExists and oldTmpSkillId == 0 then
				--播放动画
				--当前技能左移
				Extensions.SetLocalPositionX(self.m_CurSkillRoot.transform, 0)
				local tweener = LuaTweenUtils.TweenPositionX(self.m_CurSkillRoot.transform, -430, 0.2)
				LuaTweenUtils.SetDelay(tweener, tmpSkillAniLength - 0.1)				
				LuaTweenUtils.SetEase(tweener, Ease.OutCubic)
				--当前技能小图标出现	
				self.m_CurEquipSkillIcon.gameObject:SetActive(false)
				self.m_CurEquipSkillIcon.transform.localScale = Vector3(1.65, 1.65, 1)
				LuaTweenUtils.OnStart(tweener, function ()
					self.m_CurEquipSkillIcon.gameObject:SetActive(true)
					local skillTweener = LuaTweenUtils.TweenScaleXY(self.m_CurEquipSkillIcon.transform, 1, 1, 0.3, Ease.OutElastic)		
				end)
						
				--箭头出现
				Extensions.SetLocalPositionX(self.m_ReplaceArrow.transform, 430)
				self.m_ReplaceArrow:GetComponent(typeof(UIWidget)).alpha = 0
				local arrowTweener = LuaTweenUtils.TweenPositionX(self.m_ReplaceArrow.transform, 0, 0.2)
				local arrowTweener2 = LuaTweenUtils.TweenAlpha(self.m_ReplaceArrow.transform, 0, 1 , 0.2)
				LuaTweenUtils.SetDelay(arrowTweener, tmpSkillAniLength)				
				LuaTweenUtils.SetEase(arrowTweener, Ease.OutCubic)
				LuaTweenUtils.SetDelay(arrowTweener2, tmpSkillAniLength)	
			end
		end
	end
end

function LuaEquipSkillIdentifyView:IdentifySkillRequestApplyTempSkillSuccess(equipId)
	if self.m_ItemId and self.m_ItemId == equipId then
		self.m_CurSkillFx:DestroyFx()
		self.m_CurSkillFx:LoadFx("Fx/UI/Prefab/UI_jinengjianding_01.prefab")
		--此时洗练的动画可能正在播放中，需要停止动画并重新设置位置
		LuaTweenUtils.DOKill(self.m_CurSkillRoot.transform, false)
		Extensions.SetLocalPositionX(self.m_CurSkillRoot.transform, 0)
	end
end

function LuaEquipSkillIdentifyView:GetSelectableCostItemCount()
	local ownCount = 0
	if self.m_SelectableCostItem == nil then
		return nil
	end
	for itemTemplateId, isSelected in pairs(self.m_SelectableCostItem) do
		if isSelected == true then
			ownCount = ownCount + CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemTemplateId)
		end
	end
	return ownCount
end

return LuaEquipSkillIdentifyView
