local UIRoot = import "UIRoot"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local Screen = import "UnityEngine.Screen"
local NGUITools = import "NGUITools"
local NGUIMath = import "NGUIMath"
local Extensions = import "Extensions"
local CSkillMgr = import "L10.Game.CSkillMgr"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonLuaWnd=import "L10.UI.CCommonLuaWnd"
local UITable=import "UITable"
local CCommonLuaScript=import "L10.UI.CCommonLuaScript"

CLuaSkillInfoWnd = class()
RegistClassMember(CLuaSkillInfoWnd,"skillTipTpl")
RegistClassMember(CLuaSkillInfoWnd,"table")
RegistClassMember(CLuaSkillInfoWnd,"m_Wnd")
CLuaSkillInfoWnd.FromAttrGroup = false--属性切换

function CLuaSkillInfoWnd:Awake()
    self.skillTipTpl = self.transform:Find("Anchor/SkillTip").gameObject
    self.table = self.transform:Find("Anchor/Table"):GetComponent(typeof(UITable))
    self.m_Wnd=self.gameObject:GetComponent(typeof(CCommonLuaWnd))

    self.skillTipTpl:SetActive(false)

end

-- Auto Generated!!
function CLuaSkillInfoWnd:Init( )

    Extensions.RemoveAllChildren(self.table.transform)
    local skillId = CSkillInfoMgr.skillInfo.skillId

    if CSkillInfoMgr.isMapiSkill and not ZuoQi_MapiSkill.Exists(skillId) then
        self.m_Wnd:Close()
        return
    elseif not CSkillInfoMgr.isMapiSkill and not Skill_AllSkills.Exists(skillId) then
        self.m_Wnd:Close()
        return
    end

    -- local skillIds = {}-- CreateFromClass(MakeGenericClass(List, UInt32))
    -- table.insert( skillIds,skillid )
    -- CommonDefs.ListAdd(skillIds, typeof(UInt32), skillId)
    -- do
    --     local i = 0
    --     while i < skillIds.Count do
            local tipGo = NGUITools.AddChild(self.table.gameObject, self.skillTipTpl)
            tipGo:SetActive(true)
            local tip = tipGo:GetComponent(typeof(CCommonLuaScript))
            if CSkillInfoMgr.isMapiSkill then
                -- CommonDefs.GetComponent_GameObject_Type(tipGo, typeof(CSkillTip)):Init(skillid)
                tip.m_LuaSelf:InitMapi(skillId)
            else
                -- CommonDefs.GetComponent_GameObject_Type(tipGo, typeof(CSkillTip)):Init(skillid, CSkillInfoMgr.skillInfo.playerLevel, CSkillInfoMgr.skillInfo.xiuweiGrade, CSkillInfoMgr.skillInfo.skillProp)
                tip.m_LuaSelf:InitSkill(skillId, 
                    CSkillInfoMgr.skillInfo.playerLevel, 
                    CSkillInfoMgr.skillInfo.xiuweiGrade, 
                    CSkillInfoMgr.skillInfo.skillProp)
            end
    --         i = i + 1
    --     end
    -- end
    self.table:Reposition()

    if CSkillInfoMgr.skillInfo.alignByTopPos then
        self:AlignByTopPos(CSkillInfoMgr.skillInfo.topWorldPos)
    else
        self:SetPosition(CommonDefs.ImplicitConvert_Vector3_Vector2(CSkillInfoMgr.skillInfo.screenPos))
    end
end
-- function CLuaSkillInfoWnd:Awake( )

-- end


function CLuaSkillInfoWnd:OnEnable( )
    g_ScriptEvent:AddListener("MainPlayerSkillPropUpdate", self, "OnMainPlayerSkillPropUpdate")
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
    -- EventManager.AddListener(EnumEventType.MainPlayerSkillPropUpdate, MakeDelegateFromCSFunction(self.OnMainPlayerSkillPropUpdate, Action0, self))
    -- EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(self.OnSendItem, MakeGenericClass(Action1, String), self))
end
function CLuaSkillInfoWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("MainPlayerSkillPropUpdate", self, "OnMainPlayerSkillPropUpdate")
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
    -- EventManager.RemoveListener(EnumEventType.MainPlayerSkillPropUpdate, MakeDelegateFromCSFunction(self.OnMainPlayerSkillPropUpdate, Action0, self))
    -- EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(self.OnSendItem, MakeGenericClass(Action1, String), self))
end

function CLuaSkillInfoWnd:SkillGetClass(skillId)
    return math.floor( skillId/100 )
end
function CLuaSkillInfoWnd:SkillGetLevel(skillId)
    return skillId%100
end

function CLuaSkillInfoWnd:OnMainPlayerSkillPropUpdate( )
    --升级界面或者技能更换界面上的色系技能需要刷新tip显示
    local skillId = CSkillInfoMgr.skillInfo.skillId
    local skillCls = self:SkillGetClass(skillId)
    local skillLv = self:SkillGetLevel(skillId)
    if not CSkillMgr.Inst:IsColorSystemSkill(skillCls) and not CSkillMgr.Inst:IsColorSystemBaseSkill(skillCls) then
        return
    end

    if CSkillInfoMgr.skillInfoContext == CSkillInfoMgr.EnumSkillInfoContext.MainPlayerSkillPreference 
    or CSkillInfoMgr.skillInfoContext == CSkillInfoMgr.EnumSkillInfoContext.MainPlayerSkillUpdate then
        if CClientMainPlayer.Inst ~= nil then
            local inEffectCls = CClientMainPlayer.Inst.SkillProp:GetInEffectColorSystemSkill(skillCls)
            if inEffectCls > 0 and inEffectCls ~= skillCls then
                local newSkillId = CLuaDesignMgr.Skill_AllSkills_GetSkillId(inEffectCls, skillLv)
                CSkillInfoMgr.UpdateColorSystemSkillId(newSkillId)
                self:Init()
            end
        end
    end
end

function CLuaSkillInfoWnd:OnSendItem( args ) 
    local itemId = args[0]
    if CSkillInfoMgr.skillInfo.identifySkillEquipId ~= itemId then
        return
    end
    --不是关注的装备
    local skillId = CSkillInfoMgr.skillInfo.skillId
    if not CSkillMgr.Inst:IsIdentifySkill(self:SkillGetClass(skillId)) then
        return
    end
    --当前不是鉴定技能

    self:Init()
end


function CLuaSkillInfoWnd:SetPosition( screenPos) 

    local b = NGUIMath.CalculateRelativeWidgetBounds(self.table.transform)
    local localPos = self.table.transform.parent:InverseTransformPoint(CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos))
    local centerX = localPos.x - b.size.x * 0.5
    --窗口右侧与点击位置对齐
    local centerY = localPos.y + b.size.y * 0.5
    --窗口底侧与点击位置对齐
    local size = CUICommonDef.GetVirtualScreenSize()
    local virtualScreenWidth = size.x
    local virtualScreenHeight = size.y
    --左右不超出屏幕范围
    if centerX - b.size.x * 0.5 < - virtualScreenWidth * 0.5 then
        centerX = - virtualScreenWidth * 0.5 + b.size.x * 0.5
    end
    if centerX + b.size.x * 0.5 > (virtualScreenWidth * 0.5) then
        centerX = virtualScreenWidth * 0.5 - b.size.x * 0.5
    end
    --上下不超出屏幕范围
    if centerY + b.size.y * 0.5 > virtualScreenHeight * 0.5 then
        centerY = virtualScreenHeight * 0.5 - b.size.y * 0.5
    elseif centerY - b.size.y * 0.5 < - virtualScreenHeight * 0.5 then
        centerY = - virtualScreenHeight * 0.5 + b.size.y * 0.5
    end
    self.table.transform.localPosition = Vector3(centerX, centerY, 0)
end

function CLuaSkillInfoWnd:AlignByTopPos( topPos) 

    local b = NGUIMath.CalculateRelativeWidgetBounds(self.table.transform)
    local topLocalPos = self.table.transform.parent:InverseTransformPoint(topPos)
    self.table.transform.localPosition = Vector3(topLocalPos.x, topLocalPos.y - b.extents.y, 0)
end
function CLuaSkillInfoWnd:Update( )
    self.m_Wnd:ClickThroughToClose()

    -- self:ClickThroughToClose()
end
function CLuaSkillInfoWnd:GetGuideGo( methodName) 
    if methodName == "GetHuaHunSkillColor" then
        return self:GetHuaHunSkillColor()
    end
    return nil
end
function CLuaSkillInfoWnd:GetHuaHunSkillColor( )
    if self.table.transform.childCount == 1 then
        local skillTip = self.table.transform:GetChild(0):GetComponent(typeof(CCommonLuaScript))
        if skillTip ~= nil then
            return skillTip.m_LuaSelf:GetHuaHunSkillColor()
        end
    end
    return nil
end

function CLuaSkillInfoWnd:OnDestroy()
    CLuaSkillInfoWnd.FromAttrGroup = false
end
