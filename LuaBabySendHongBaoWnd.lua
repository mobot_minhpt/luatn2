require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"

CLuaBabySendHongBaoWnd = class()
RegistClassMember(CLuaBabySendHongBaoWnd,"m_CloseBtn")
RegistClassMember(CLuaBabySendHongBaoWnd,"m_MoneyLabel")
RegistClassMember(CLuaBabySendHongBaoWnd,"m_MoneyLabelBg")
RegistClassMember(CLuaBabySendHongBaoWnd,"m_CurentMoneyCtrl")
RegistClassMember(CLuaBabySendHongBaoWnd,"m_SendButton")
RegistClassMember(CLuaBabySendHongBaoWnd,"m_HistoryButton")

RegistClassMember(CLuaBabySendHongBaoWnd,"m_CurrentValue")

function CLuaBabySendHongBaoWnd:Awake()
    
end

function CLuaBabySendHongBaoWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function CLuaBabySendHongBaoWnd:Init()
	self.m_CloseBtn = self.transform:Find("Wnd_Bg/CloseButton").gameObject
	CommonDefs.AddOnClickListener(self.m_CloseBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

	self.m_MoneyLabel = self.transform:Find("Anchor/CostLabel"):GetComponent(typeof(UILabel))
	self.m_MoneyLabelBg = self.transform:Find("Anchor/CostLabel/Bg").gameObject
	self.m_CurentMoneyCtrl = self.transform:Find("Anchor/Money"):GetComponent(typeof(CCurentMoneyCtrl))
	self.m_SendButton = self.transform:Find("Anchor/SendButton").gameObject
	self.m_HistoryButton = self.transform:Find("Anchor/HistoryButton").gameObject

	CommonDefs.AddOnClickListener(self.m_MoneyLabelBg, DelegateFactory.Action_GameObject(function(go) self:OnCurrentValueClick() end), false)
	CommonDefs.AddOnClickListener(self.m_SendButton, DelegateFactory.Action_GameObject(function(go) self:OnSendButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_HistoryButton, DelegateFactory.Action_GameObject(function(go) self:OnHistoryButtonClick() end), false)

	local setting = Baby_Setting.GetData()
	local LuckyMoneyLowerLimit = setting.LuckyMoneyLowerLimit
	local LuckyMoneyUpperLimit = setting.LuckyMoneyUpperLimit

	self.m_MoneyLabel.text = tostring(LuckyMoneyLowerLimit)
	self.m_CurrentValue = LuckyMoneyLowerLimit



end

function CLuaBabySendHongBaoWnd:OnCurrentValueClick()
	
	local setting = Baby_Setting.GetData()
	local LuckyMoneyLowerLimit = setting.LuckyMoneyLowerLimit
	local LuckyMoneyUpperLimit = setting.LuckyMoneyUpperLimit

	CLuaNumberInputMgr.ShowNumInputBox(LuckyMoneyLowerLimit, math.min(LuckyMoneyUpperLimit, self.m_CurentMoneyCtrl:GetOwn()), self.m_CurrentValue or LuckyMoneyLowerLimit, function (count) 
        self.m_MoneyLabel.text = tostring(count)
		self.m_CurrentValue = count
    end, nil, - 1)
end

function CLuaBabySendHongBaoWnd:OnSendButtonClick()
	LuaBabyMgr.RequestSendLuckMoney(self.m_CurrentValue)
end

function CLuaBabySendHongBaoWnd:OnHistoryButtonClick()
	LuaBabyMgr.ShowLuckMoneyHistoryWnd()
end
