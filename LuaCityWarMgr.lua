require("common/citywar/territorysceneinc")

local CGuildCityWarPlayData = import "L10.Game.CGuildCityWarPlayData"
local CCityBasicInfo = import "L10.Game.CCityBasicInfo"
local CCityUnitInfo = import "L10.Game.CCityUnitInfo"
local CCityMiscInfo = import "L10.Game.CCityMiscInfo"
local CCityWarMgr = import "L10.Game.CCityWarMgr"
local CCityWarUnit = import "L10.Game.CCityWarUnit"
local HousePopupMenuItemData= import "L10.UI.HousePopupMenuItemData"
local CoreScene = import "L10.Engine.Scene.CoreScene"
local CSEBarrierType = import "L10.Engine.EBarrierType"
local EUIModuleGroup = import "L10.UI.CUIManager+EUIModuleGroup"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CClientNpc = import "L10.Game.CClientNpc"
local Utility=import "L10.Engine.Utility"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local MsgPackImpl = import "MsgPackImpl"
local CScene = import "L10.Game.CScene"
local LocalString = import "LocalString"
local CTrackMgr = import "L10.Game.CTrackMgr"
local DelegateFactory = import "DelegateFactory"
local CActivityMgr = import "L10.Game.CActivityMgr"
local CMiniMap = import "L10.UI.CMiniMap"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType =import "L10.Game.EnumWarnFXType"
local Color = import "UnityEngine.Color"
local CPos = import "L10.Engine.CPos"

CLuaCityWarMgr = {}
CLuaCityWarMgr.MaxLevel = 5

-- Invite member time in LuaCityWarResultWnd
CLuaCityWarMgr.InviteTickTime = 0

CLuaCityWarMgr.CurrentGuildId = 0
CLuaCityWarMgr.CurrentGuildName = ""
CLuaCityWarMgr.CurrentCityId = ""
CLuaCityWarMgr.CurrentMaterial = 0
CLuaCityWarMgr.CurrentBasicInfo = nil
CLuaCityWarMgr.CurrentUnitInfo = nil
CLuaCityWarMgr.CurrentMiscInfo = nil

CLuaCityWarMgr.TerritoryScene = nil
CLuaCityWarMgr.CityDesignData = {}
CLuaCityWarMgr.UnitDesignData = {}

CLuaCityWarMgr.CurrentMapId = 0

-- City Info
CLuaCityWarMgr.CityInfoId = 0
CLuaCityWarMgr.CityInfoTemplateId = 0
CLuaCityWarMgr.CityInfoGuildId = 0
CLuaCityWarMgr.CityInfoLevel = 0
CLuaCityWarMgr.CityInfoGuildName = ""
CLuaCityWarMgr.CityInfoServerName = ""
CLuaCityWarMgr.CityInfoAssetAmount = 0
CLuaCityWarMgr.CityInfoBuildingAmount = 0
CLuaCityWarMgr.CityInfoStatus = 0
CLuaCityWarMgr.CityInfoDuration = 0
CLuaCityWarMgr.CityInfoMapTemplateId = 0
CLuaCityWarMgr.CityInfoPlayNum = 0
CLuaCityWarMgr.CityInfoPlayIdsUD = nil

CLuaCityWarMgr.LastUsedTemplateIdx = -1
CLuaCityWarMgr.CityGateNpcId = 20003631

-- Map Info
CLuaCityWarMgr.MapInfoTemplateId = 0
CLuaCityWarMgr.MapInfoOccupiedCount = 0
CLuaCityWarMgr.MapInfoMyGuildMapId = 0
CLuaCityWarMgr.MapInfoMyGuildCityId = 0

-- ZhanLong Item
CLuaCityWarMgr.ZhanLongGrabMaterialItemId = ""
CLuaCityWarMgr.ZhanLongGrabMaterialItemPlace = 0
CLuaCityWarMgr.ZhanLongGrabMaterialItemPos = 0
CLuaCityWarMgr.ZhanLongGrabMaterialItemTemplateId = 21030386
CLuaCityWarMgr.ZhanLongFlagTemplateId = 21030387

-- War Map Info
CLuaCityWarMgr.WarMapInfoMyMapId = 0
CLuaCityWarMgr.WarMapInfoMapInfoUD = nil
CLuaCityWarMgr.WarMapInfoMatchLeftTime = -1
CLuaCityWarMgr.WarMapInfoDeclaredMapId = 0
CLuaCityWarMgr.WarMapInfoIsMatchPeriod = false

-- War Info
CLuaCityWarMgr.WarForceInfo = {}
CLuaCityWarMgr.WarRebornPointInfo = {}
CLuaCityWarMgr.WarOccupyInfo = {}
CLuaCityWarMgr.WarIndex = 0
CLuaCityWarMgr.MaxRebornPointPerCity = 3
CLuaCityWarMgr.DefaultRebornPointUnitId = 9999999
CLuaCityWarMgr.RebornPointUnitTemplateId = 2501
CLuaCityWarMgr.AttackRebornPointMonsterId = 18003889
CLuaCityWarMgr.DefendRebornPointMonsterId = 18003888
CLuaCityWarMgr.MirrorWarHuFuFullProgress = 0
CLuaCityWarMgr.MirrorWarFightPrepareDuration = 0

-- Setting Info
CLuaCityWarMgr.TerritoryHuFuFullProgress = 0
CLuaCityWarMgr.UnitTypeIcon = nil
CLuaCityWarMgr.UnitTypeName = nil

-- My City Data
CLuaCityWarMgr.MyCityGuildPlayData = nil
CLuaCityWarMgr.MyCityBasicInfo = nil
CLuaCityWarMgr.MyCityUnitInfo = nil
CLuaCityWarMgr.MyCityMiscInfo = nil
CLuaCityWarMgr.MyCityWndOpenActionType = nil
CLuaCityWarMgr.MyCityWndJumpToGongFeng = nil --标记打开城池界面时是否直接跳转到供奉界面

-- Guild Material Info
CLuaCityWarMgr.GuildMaterialAmount = 0
CLuaCityWarMgr.GuildMaterialLimit = 0
CLuaCityWarMgr.IsFirstOpenSubmitWnd = false

BarrierType2Int = {[CSEBarrierType.eBT_NoBarrier] = 0, [CSEBarrierType.eBT_LowBarrier] = 1, [CSEBarrierType.eBT_MidBarrier] = 2, [CSEBarrierType.eBT_HighBarrier] = 3, [CSEBarrierType.eBT_OutRange] = 4}
Int2BarrierType = {[0] = CSEBarrierType.eBT_NoBarrier, [1] = CSEBarrierType.eBT_LowBarrier, [2] = CSEBarrierType.eBT_MidBarrier, [3] = CSEBarrierType.eBT_HighBarrier, [4] = CSEBarrierType.eBT_OutRange}

-- History
CLuaCityWarMgr.HistoryWinForce = 0
CLuaCityWarMgr.HistoryPlayResultUD = nil
CLuaCityWarMgr.HistoryForceInfoUD = nil
CLuaCityWarMgr.HistorySummaryInfoUD = nil
CLuaCityWarMgr.HistoryResultFlag = nil

--Watch Info
CLuaCityWarMgr.WatchInfo = nil

CLuaCityWarMgr.WatchPlayIds = {}

--Exchange Info
CLuaCityWarMgr.CurExchangeCnt = 0
CLuaCityWarMgr.MaxExchangeCnt = 0

-- Copy Scene Info
CLuaCityWarMgr.CopySceneDestinationPos = nil

-- Status Display name
CLuaCityWarMgr.StatusNameTable = {[0]=LocalString.GetString("未占领"), [1]={LocalString.GetString("非保护状态，可战斗"),LocalString.GetString("非保护状态，可宣战")},[2]=LocalString.GetString("非宣战期(18点开放)"), [3]=LocalString.GetString("战斗中"), [4]=LocalString.GetString("宣战准备中"), [5]=LocalString.GetString("宣战对战中"), [6]=LocalString.GetString("战斗冷却中"), [7]=LocalString.GetString("孤城守卫进行中"), [8]=LocalString.GetString("休战期(18点开放)"), [9]=LocalString.GetString("宣战保护中")}

function CLuaCityWarMgr:AddListener( ... )
	g_ScriptEvent:RemoveListener("OnGacAllModulesStartUp", self, "OnGacAllModulesStartUp")
	g_ScriptEvent:RemoveListener("LoadingSceneFinished", self, "InitCityWarTeleportFx")
	g_ScriptEvent:AddListener("OnGacAllModulesStartUp", self, "OnGacAllModulesStartUp")
	g_ScriptEvent:AddListener("LoadingSceneFinished", self, "InitCityWarTeleportFx")
end
CLuaCityWarMgr:AddListener()

function CLuaCityWarMgr:OnGacAllModulesStartUp()
	CLuaCityWarMgr:ParseDesignData()
end

function CLuaCityWarMgr:InitCityWarTeleportFx()
	if not CRenderScene.Inst then return end
	CRenderScene.Inst:RemoveCityWarTeleportFx()
	local tbl = {}
	if CCityWarMgr.Inst:IsInCityWarScene() then
		local line = CityWar_Setting.GetData().TeleportAreaFx
		local data = CityWar_Map.GetData(CScene.MainScene.SceneTemplateId)
		local neighborTbl = {data.WestMap, data.SouthMap, data.NorthMap, data.EastMap}
		for i = 0, #neighborTbl - 1 do
			if neighborTbl[i + 1] > 0 then
				for j = 0, 3 do
					table.insert(tbl, line[i * 4 + j])
				end
			end
		end
		if #tbl > 0 then
			CRenderScene.Inst:GenCityWarTeleportFx(Table2Array(tbl, MakeArrayClass(int)))
		end
	--else
		--self:InitCityWarCopyTeleportFx()
	end
end

function CLuaCityWarMgr:GetCityWarCopyNum(mapId1, mapId2)
	local map1, map2 = CityWar_Map.GetData(mapId1), CityWar_Map.GetData(mapId2)
	local level1, level2 = map1.Level, map2.Level
	if level1 ~= level2 then
		return level1 > level2 and map1.MajorCityMirrorWarPlayNum or map2.MajorCityMirrorWarPlayNum
	end
	return math.max(map1.MajorCityMirrorWarPlayNum, map2.MajorCityMirrorWarPlayNum)
end


function CLuaCityWarMgr:IsInWatchScene( ... )
	if not CScene.MainScene then return false end
	if (CScene.MainScene.SceneTemplateId >= 16000201 and CScene.MainScene.SceneTemplateId <= 16000205) then
		return true
	end
	return false
end

function CLuaCityWarMgr:OpenSecondaryMap(mapId)
	CLuaCityWarMgr.CurrentMapId = mapId
	CUIManager.ShowUI(CLuaUIResources.CityWarSecondaryMapWnd)
end

CLuaCityWarMgr.PrimaryMapDefaultIndex = EnumCityWarPrimaryMapWndTabIndex.eOccupy
function CLuaCityWarMgr:OpenPrimaryMap(index)
	if not index then index = EnumCityWarPrimaryMapWndTabIndex.eOccupy end
	if index<EnumCityWarPrimaryMapWndTabIndex.eTerritory or index>EnumCityWarPrimaryMapWndTabIndex.eYaYun then index = EnumCityWarPrimaryMapWndTabIndex.eOccupy end
	CLuaCityWarMgr.PrimaryMapDefaultIndex = index
	CUIManager.ShowUI(CLuaUIResources.CityWarPrimaryMapWnd)
end

CLuaCityWarMgr.CityInfoPlayerId = nil
CLuaCityWarMgr.CityInfoGuildId = nil
function CLuaCityWarMgr.ShowCityInfoWnd(playerId, guildId)
	CLuaCityWarMgr.CityInfoPlayerId = playerId or 0
	CLuaCityWarMgr.CityInfoGuildId = guildId or 0
	if guildId and guildId > 0 then
		CUIManager.ShowUI("CityWarCityInfoWnd")
	end
end

function CLuaCityWarMgr:CanTrack()
	if not CClientMainPlayer.Inst then return false end
	if CClientMainPlayer.Inst.IsTeamFollowing or CClientMainPlayer.Inst.IsGuaji or CClientMainPlayer.Inst.IsRushing or CClientMainPlayer.Inst.RO.IsKickOuting then
		return false
	end
	return true
end

-- For Windows shortcut key
function CLuaCityWarMgr:ShowOrCloseMap( ... )
	if CCityWarMgr.Inst:IsInCityWarScene() then
		CLuaCityWarMgr.CurrentMapId = CScene.MainScene.SceneTemplateId
		CUIManager.ShowOrCloseUI(CLuaUIResources.CityWarSecondaryMapWnd)
	elseif CCityWarMgr.Inst:IsInCityWarCopyScene() then
		CUIManager.ShowOrCloseUI(CLuaUIResources.CityWarPrimaryMapWnd)
	end
end

function CLuaCityWarMgr:IsMajorCity(cityId)
	local found = false
	CityWar_Map.Foreach(function(k, v)
		if cityId == v.MajorCity then found = true end
	end)
	return found
end

function CLuaCityWarMgr:IsNeighbor(myMapId, mapId)
	if not CityWar_Map.Exists(mapId) then return false end
	local myMap = CityWar_Map.GetData(myMapId)
	if not myMap then return false end
	if myMap.EastMap == mapId or myMap.WestMap == mapId or myMap.SouthMap == mapId or myMap.NorthMap == mapId then
		return true
	end
	return false
end

CLuaCityWarMgr.__CityDesignDataHolder = {}
setmetatable(CLuaCityWarMgr.CityDesignData, {
	__index = function(t, k)
		if not next(CLuaCityWarMgr.__CityDesignDataHolder) then
			CLuaCityWarMgr:ParseCityDesignData()
		end
		return CLuaCityWarMgr.__CityDesignDataHolder[k]
	end
})

function CLuaCityWarMgr:ParseCityDesignData()
	self.__CityDesignDataHolder = {}
	CityWar_MapCity.Foreach(function(templateId, designData)
		local data = {}
		-- 区域
		local lbx, lby, rtx, rty = string.match(designData.Rect, "(%d+),(%d+),(%d+),(%d+);?")
		lbx, lby, rtx, rty = tonumber(lbx), tonumber(lby), tonumber(rtx), tonumber(rty)
		data.Region = {lb = {x = lbx, y = lby}, rt = {x = rtx, y = rty}}

		self.__CityDesignDataHolder[templateId] = data
	end)
end

CLuaCityWarMgr.__UnitDesignDataHolder = {}
setmetatable(CLuaCityWarMgr.UnitDesignData, {
	__index = function(t, k)
		if not next(CLuaCityWarMgr.__UnitDesignDataHolder) then
			CLuaCityWarMgr:ParseUnitDesignData()
		end
		return CLuaCityWarMgr.__UnitDesignDataHolder[k]
	end
})

function CLuaCityWarMgr:ParseUnitDesignData()
	self.__UnitDesignDataHolder = {}
	CityWar_Unit.Foreach(function(templateId, designData)
		local data = {}
		local width, height = string.match(designData.Area, "(%d+)*(%d+)")
		width, height = tonumber(width), tonumber(height)
		data.Width = width
		data.Height = height

		data.ActionList = {}
		if designData.UseIcon and designData.UseIcon ~= "" then
			for act in string.gmatch(designData.UseIcon, "([^;]+);") do
				table.insert(data.ActionList, act)
			end
		end
		data.UsePlace = {}
		if designData.UsePlace and designData.UsePlace ~= "" then
			for usePlace in string.gmatch(designData.UsePlace, "([^;]+);") do
				usePlace = tonumber(usePlace)
				table.insert(data.UsePlace, usePlace)
			end
		end

		if designData.Barrier and designData.Barrier ~= "" then
			data.Barrier = {}
			for b in string.gmatch(designData.Barrier, "([^;]+);") do
				b = tonumber(b)
				table.insert(data.Barrier, b)
			end
		end

		data.HasSkillBlock = designData.HasSkillBlock

		data.ID = templateId

		data.Type = designData.Type

		self.__UnitDesignDataHolder[templateId] = data
	end)
end

function CLuaCityWarMgr:ParseDesignData()
	local CityWarSetting = CityWar_Setting.GetData()
	CLuaCityWarMgr.TerritoryHuFuFullProgress = tonumber(CityWarSetting.HuFuFullProgress)
	CLuaCityWarMgr.UnitTypeIcon = CityWarSetting.UnitTypeIcon
	CLuaCityWarMgr.UnitTypeName = CityWarSetting.UnitTypeName
	CLuaCityWarMgr.CityGateNpcId = tonumber(CityWarSetting.CityGateNpcId)

	local MirrorWarSetting = CityWar_MirrorWar.GetData()
	CLuaCityWarMgr.MaxRebornPointPerCity =  tonumber(MirrorWarSetting.MaxRebornPointPerCity)
	CLuaCityWarMgr.AttackRebornPointMonsterId = tonumber(MirrorWarSetting.AttackRebornPointMonsterId)
	CLuaCityWarMgr.DefendRebornPointMonsterId = tonumber(MirrorWarSetting.DefendRebornPointMonsterId)
	CLuaCityWarMgr.MirrorWarHuFuFullProgress = tonumber(MirrorWarSetting.HuFuFullProgress)
	CLuaCityWarMgr.MirrorWarFightPrepareDuration = tonumber(MirrorWarSetting.FightPrepareDuration)

	local ZhanLongSetting = CityWar_ZhanLong.GetData()
	CLuaCityWarMgr.ZhanLongFlagTemplateId = tonumber(ZhanLongSetting.FlagTemplateId)
	CLuaCityWarMgr.ZhanLongGrabMaterialItemTemplateId = tonumber(ZhanLongSetting.GrabMaterialItemId)
end

--Occupy info
CLuaCityWarMgr.OccupyGuildId = 0
CLuaCityWarMgr.OccupyMapId = 0
CLuaCityWarMgr.OccupyTemplateId = 0
CLuaCityWarMgr.OccupyCityId = 0
CLuaCityWarMgr.OccupyDuration = 60
CLuaCityWarMgr.MyMapId = 0
CLuaCityWarMgr.MyCityTemplateId = 0

function CLuaCityWarMgr:GetTypeLimit(gradeData, nType)
	if not gradeData then return 0 end
	local typeData = CityWar_UnitTypeName.GetData(nType)
	local typeName = typeData and typeData.TypeName
	return typeName and gradeData[typeName] or 0
end

function CLuaCityWarMgr:GetTotalJianzhuCount()
	local level = 0
	if CLuaCityWarMgr.CurrentBasicInfo then
		level = CLuaCityWarMgr.CurrentBasicInfo.Grade
	end
	local count = 0
	local ddata = CityWar_CityGrade.GetData(level)
	if ddata then
		CityWar_UnitTypeName.Foreach(function(typeId, typeData)
			local typeName = typeData and typeData.TypeName
			count = count + (typeName and ddata[typeName] or 0)
		end)
	end
	return count
end

function CLuaCityWarMgr:StopPlaceMode()
	if CCityWarMgr.Inst.PlaceMode then
		local excepts = {"MiddleNoticeCenter", "PlaceCityUnitWnd"}
		local List_String = MakeGenericClass(List,cs_string)
	    CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
	    CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)

	    CUIManager.CloseUI(CLuaUIResources.PlaceCityUnitWnd)
	    CCityWarMgr.Inst.PlaceMode = false

	    CCityWarMgr.Inst:ClearCurUnit()
		-- CameraFollow.Inst:CityWarAdjustCombatVehicleCamera()
		CameraFollow.Inst:ResetToDefault()

		CGuildMgr.Inst.CanCityBuild = false

		CLuaCityWarMgr:ClearAdjacentUnit()
	end
end

function CLuaCityWarMgr:GetPlacedCount()
	if CCityWarMgr.Inst and CCityWarMgr.Inst.UnitList then
		return CCityWarMgr.Inst.UnitList.Count
	end
	return 0
end

function CLuaCityWarMgr:GetPlacedCountForType(nType)
	local count = 0

    CommonDefs.DictIterate(CCityWarMgr.Inst.UnitList, DelegateFactory.Action_object_object(function (___key, ___value)
        local unit = ___value
        local unitDData = CLuaCityWarMgr.UnitDesignData[unit.TemplateId]
        if unitDData and unitDData.Type == nType then
        	count = count + 1
        end
    end))

	return count
end

function CLuaCityWarMgr:GetPlacedCountForTemplateId(templateId)
	local count = 0
	local baseTemplateId = math.floor(templateId/100)
    CommonDefs.DictIterate(CCityWarMgr.Inst.UnitList, DelegateFactory.Action_object_object(function (___key, ___value)
        local unit = ___value
        if math.floor(unit.TemplateId/100) == baseTemplateId then
        	count = count + 1
        end
    end))

	return count
end

function CLuaCityWarMgr:CreateCityUnit(cityId, templateId, unitId)
	CCityWarMgr.Inst:ClearCurUnit()
	local unit = CCityWarMgr.Inst:CreateUnit(unitId, templateId, 0, false)
	unit.IsPlayerOwner = true
	CCityWarMgr.Inst.CurUnit = unit
	CCityWarMgr.Inst.CurUnit.Selected = true
	CCityWarMgr.Inst.CurUnit.Moving = true
end
function CLuaCityWarMgr:AddCityUnit(unitId, templateId, x, y, dir, engineId)
	self:MoveCityUnitInternal(templateId, unitId, nil, nil, nil, x, y, dir)
	local cwUnit = CCityWarMgr.Inst:CreateUnit(unitId, templateId, 0, true)
	cwUnit.ServerPos = Vector3(x, 0, y)
	cwUnit.ServerRotateY = dir
	cwUnit.EngineId = engineId
	CommonDefs.DictAdd_LuaCall(CCityWarMgr.Inst.UnitList, unitId, cwUnit)
	local unitDData = CityWar_Unit.GetData(templateId)
	local guildId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.GuildId or 0
	if guildId == CLuaCityWarMgr.CurrentGuildId then
		cwUnit.IsPlayerOwner = true
	elseif unitDData and unitDData.HiddenToEnemy == 1 then
		cwUnit.Hidden = true
	end

	if cwUnit.ro then
		cwUnit:SetPosition(cwUnit.ServerPos)
		cwUnit.ro.Direction = 90 - dir
		cwUnit.ro.Visible = not cwUnit.Hidden
		CommonDefs.DictAdd_LuaCall(CCityWarMgr.Inst.RO2Unit, cwUnit.ro, cwUnit)
	end

	if cwUnit.RO and cwUnit.RO.Visible then
		if unitDData and unitDData.RangeFxId > 0 then
			cwUnit.RangeFx=CEffectMgr.Inst:AddObjectFX(unitDData.RangeFxId,cwUnit.RO,3,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
		end
	end
end

function CLuaCityWarMgr:AddCityUnitEngineId(engineId, unitId)
	if engineId ~= 0 then
		if CommonDefs.DictContains_LuaCall(CCityWarMgr.Inst.ClientObjectList, engineId) then
			CommonDefs.DictRemove_LuaCall(CCityWarMgr.Inst.ClientObjectList, engineId)
		end
		CommonDefs.DictAdd_LuaCall(CCityWarMgr.Inst.ClientObjectList, engineId, unitId)

		if CommonDefs.DictContains_LuaCall(CCityWarMgr.Inst.UnitList, unitId) then
			local unit = CommonDefs.DictGetValue_LuaCall(CCityWarMgr.Inst.UnitList, unitId)
			unit.EngineId = engineId
			local guildId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.GuildId or 0
			if guildId == CLuaCityWarMgr.CurrentGuildId  then
				unit.IsPlayerOwner=true
			end
		end
	end
end


function CLuaCityWarMgr:MoveCityUnitInternal(templateId, unitId, ox, oy, odir, x, y, dir)

	local function getGridBarrierAndOriginalGridBarrier(xx, yy)
			local obarrier = CoreScene.MainCoreScene:GetOriginalBarrierv(xx, yy)
			local barrier = CoreScene.MainCoreScene:GetBarrierv(xx, yy, true)
			obarrier = BarrierType2Int[obarrier] or 4
			barrier = BarrierType2Int[barrier] or 4
			return barrier, obarrier
		end
	local function setGridBarrier(xx, yy, barrier)
			CoreScene.MainCoreScene:SetGridDynamicBarrier(xx, yy, 0, Int2BarrierType[barrier])
		end
	local unitDesignData = CLuaCityWarMgr.UnitDesignData[templateId]
	if ox and oy and odir then
		self:RemoveCityUnitSkillBlock(ox, oy, odir, unitDesignData)
	end
	if x and y and dir then
		self:AddCityUnitSkillBlock(x, y, dir, unitDesignData)
	end

	CLuaCityWarMgr:GetTerritoryScene():MoveCityUnit(CLuaCityWarMgr.CurrentCityId, unitId, ox, oy, odir,
		x, y, dir, unitDesignData, getGridBarrierAndOriginalGridBarrier, setGridBarrier)
end


function CLuaCityWarMgr:AddCityUnitSkillBlock(x, y, dir, unitDesignData)
	if not CScene.MainScene then return end
	if unitDesignData.HasSkillBlock <= 0 then return end

	local barrierList = CLuaCityWarMgr:GetTerritoryScene():GetUnitBarrierList(unitDesignData, dir)
	local blocks = {}
	for _, b in ipairs(barrierList) do
		local xx, yy, barrierType = b.x + x, b.y + y, b.barrierType
		if barrierType == EBarrierType.eBT_HighBarrier then
			table.insert(blocks, xx)
			table.insert(blocks, yy)
		end
	end
	if #blocks <= 0 or #blocks > 10 then
		return
	end
	local default = CScene.MainScene.INVALID_POS_OF_WALL
	CScene.MainScene:AddSkillWall(blocks[1], blocks[2], blocks[3] or default, blocks[4] or default, blocks[5] or default, blocks[6] or default, blocks[7] or default, blocks[8] or default, blocks[9] or default, blocks[10] or default)
end

function CLuaCityWarMgr:RemoveCityUnitSkillBlock(x, y, dir, unitDesignData)
	if not CScene.MainScene then return end
	if unitDesignData.HasSkillBlock <= 0 then return end

	local barrierList = CLuaCityWarMgr:GetTerritoryScene():GetUnitBarrierList(unitDesignData, dir)
	local blocks = {}
	for _, b in ipairs(barrierList) do
		local xx, yy, barrierType = b.x + x, b.y + y, b.barrierType
		if barrierType == EBarrierType.eBT_HighBarrier then
			CScene.MainScene:RemoveSkillWall(xx, yy)
		end
	end
end

--判断玩家是否在自己的城池
function CLuaCityWarMgr.IsMainPlayerInOwnCity()
	local pos = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Pos or nil
	local guildId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.GuildId or 0

	if pos and CLuaCityWarMgr.CurrentBasicInfo and guildId>0 and guildId==CLuaCityWarMgr.CurrentGuildId then
		local cityTemplateId = CLuaCityWarMgr.CurrentBasicInfo.TemplateId
		local cityDesignData = CLuaCityWarMgr.CityDesignData[cityTemplateId]
		local limitRegion = cityDesignData.Region

		local left, bottom, right, top=limitRegion.lb.x, limitRegion.lb.y, limitRegion.rt.x, limitRegion.rt.y
		local worldPos = Utility.PixelPos2WorldPos(pos)
		local x,z=worldPos.x,worldPos.z

		if x>left and x<right and z>bottom and z<top then
			return true
		else
			return false
		end
	end
	return false
end
--判断当前城池是否是自己的
function CLuaCityWarMgr.IsOwnCurrentCity()
	local guildId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.GuildId or 0
	if CLuaCityWarMgr.CurrentBasicInfo and guildId>0 and guildId==CLuaCityWarMgr.CurrentGuildId then
		return true
	end
	return false
end

---- Begin 城建TIP
CLuaCityWarMgr.CityWarUnitInfoForTipTbl = {} --这个结构暂时考虑运行时不清空，数据量有限
function CLuaCityWarMgr.GetCityWarUnitInfoForTip(monsterId)
	if not monsterId then return end
	local hp = CLuaCityWarMgr.CityWarUnitInfoForTipTbl and CLuaCityWarMgr.CityWarUnitInfoForTipTbl[monsterId]
	if hp then
		return hp
	else
		Gac2Gas.QueryCityMonsterFightParam(monsterId, 0) --monsterId, reason
		return nil
	end
end

function CLuaCityWarMgr.SetCityWarUnitInfoForTip(monsterId, hp, context)
	--context(服务器端叫reason)字段预留，将来如果有多处使用再处理
	if CLuaCityWarMgr.CityWarUnitInfoForTipTbl == nil then
		CLuaCityWarMgr.CityWarUnitInfoForTipTbl = {}
	end

	CLuaCityWarMgr.CityWarUnitInfoForTipTbl[monsterId] = math.floor(hp)
	g_ScriptEvent:BroadcastInLua(EnumMessage.UpdateCityWarUnitInfoForTip, monsterId, context)
end
---- End 城建TIP

function CLuaCityWarMgr.RequestDeclareMirrorWar(guildId, guildName, cityMapId, cityTemplateId)
	Gac2Gas.RequestDeclareMirrorWarInfo(guildId, guildName, cityMapId, cityTemplateId)
end

--押镖
CLuaCityWarMgr.BiaoCheMenuTargetWorldPos = nil
CLuaCityWarMgr.BiaoCheMenuTargetWidth = nil
CLuaCityWarMgr.BiaoCheMenuTargetHeight = nil
CLuaCityWarMgr.BiaoCheMenuBiaoCheTbl = nil    --{mapId, progress, status}
function CLuaCityWarMgr.ShowBiaoCheMenu(targetWorldPos, targetWidth, targetHeight, biaoCheInfo)
	CLuaCityWarMgr.BiaoCheMenuTargetWorldPos = targetWorldPos
	CLuaCityWarMgr.BiaoCheMenuTargetWidth = targetWidth
	CLuaCityWarMgr.BiaoCheMenuTargetHeight = targetHeight
	if not biaoCheInfo then return end

	CLuaCityWarMgr.BiaoCheMenuBiaoCheTbl = {}

	local firstBiaoCheMapId = biaoCheInfo.biaoCheFMapId
	local secondBiaoCheMapId = biaoCheInfo.biaoCheSMapId
	if firstBiaoCheMapId and CityWar_Map.Exists(firstBiaoCheMapId) then
		table.insert(CLuaCityWarMgr.BiaoCheMenuBiaoCheTbl, {mapId=firstBiaoCheMapId, progress=biaoCheInfo.biaoCheFProgress, status=biaoCheInfo.biaoCheFStatus})
	end

	if secondBiaoCheMapId and CityWar_Map.Exists(secondBiaoCheMapId) then
		table.insert(CLuaCityWarMgr.BiaoCheMenuBiaoCheTbl, {mapId=secondBiaoCheMapId, progress=biaoCheInfo.biaoCheSProgress, status=biaoCheInfo.biaoCheSStatus})
	end
--	print("CLuaCityWarMgr.ShowBiaoCheMenu", firstBiaoCheMapId,secondBiaoCheMapId,  #CLuaCityWarMgr.BiaoCheMenuBiaoCheTbl)
	if #CLuaCityWarMgr.BiaoCheMenuBiaoCheTbl == 0 then
		g_MessageMgr:ShowMessage("YAYUN_BIAOCHE_NO_TAGERT_TO_TRANSFER")
	else
		CUIManager.ShowUI("CityWarBiaoCheMenu")
	end
end

function CLuaCityWarMgr.RequestTrackToBiaoChe(mapId, status)
	if not mapId then return end
	if not CityWar_Map.Exists(mapId) then return end
	if status~=EnumBiaoCheStatus.eSubmitRes and status~=EnumBiaoCheStatus.eYaYun then return end
	MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("YAYUN_TRACK_TO_BIAO_CHE_CONFIRM"), DelegateFactory.Action(function ()
		Gac2Gas.RequestTrackToGuildBiaoChe(mapId)
    end), nil, nil, nil, false)
end

CLuaCityWarMgr.BiaoCheResSubmitInfo = nil
function CLuaCityWarMgr.ShowBiaoCheSubmitWnd(npcEngineId, mapId, biaoCheStatus, biaoCheProgress, resourceId, closeTime)
	CLuaCityWarMgr.BiaoCheResSubmitInfo = {}
	CLuaCityWarMgr.BiaoCheResSubmitInfo["npcEngineId"] = npcEngineId
	CLuaCityWarMgr.BiaoCheResSubmitInfo["mapId"] = mapId
	CLuaCityWarMgr.BiaoCheResSubmitInfo["biaoCheStatus"] = biaoCheStatus
	CLuaCityWarMgr.BiaoCheResSubmitInfo["biaoCheProgress"] = biaoCheProgress
	CLuaCityWarMgr.BiaoCheResSubmitInfo["resourceId"] = resourceId
	CLuaCityWarMgr.BiaoCheResSubmitInfo["biaoCheCloseTime"] = closeTime
	CUIManager.ShowUI("CityWarBiaoCheResSubmitWnd")
end

CLuaCityWarMgr.BiaoChePlaceConfirmInfo = nil
function CLuaCityWarMgr.ShowPlaceBiaoCheConfirmWnd(mapId, guildSilver, cost, otherMapId)
	if not mapId then return end
	if not CityWar_Map.Exists(mapId) then return end

	CLuaCityWarMgr.BiaoChePlaceConfirmInfo = {}
	CLuaCityWarMgr.BiaoChePlaceConfirmInfo["mapId"] = mapId
	CLuaCityWarMgr.BiaoChePlaceConfirmInfo["guildSilver"] = guildSilver
	CLuaCityWarMgr.BiaoChePlaceConfirmInfo["cost"] = cost
	CLuaCityWarMgr.BiaoChePlaceConfirmInfo["otherMapId"] = otherMapId
	CUIManager.ShowUI("CityWarPlaceBiaoCheConfirmWnd")
end

function CLuaCityWarMgr.GetMsgByStatusAndProgress(status, progress)
	if status == EnumBiaoCheStatus.eSubmitRes then
		return g_MessageMgr:FormatMessage("YAYUN_GONG_FENG_SUBMITTING_DESC")
	elseif status == EnumBiaoCheStatus.eYaYun then
		return g_MessageMgr:FormatMessage("YAYUN_GONG_FENG_YAYUN_DESC")
	elseif status == EnumBiaoCheStatus.eYaYunEnd then
		return g_MessageMgr:FormatMessage("YAYUN_GONG_FENG_YAYUN_DONE_DESC")
	else
		return nil
	end
end

function CLuaCityWarMgr.OnMainPlayerCreated()
	local mainplayer = CClientMainPlayer.Inst
	if mainplayer then
		CLuaCityWarMgr.SyncYaYunTaskStatusChange(mainplayer.PlayProp.YaYunData.YaYunTaskStatus, 0, mainplayer.PlayProp.YaYunData.TaskExpireTime)
	end
end

EnumYaYunTaskStatus = {
	eNoTask = 0,		-- 没有任务
	eInYaYun = 1,		-- 正常押运中
	eLoseBiaoChe = 2,	-- 车子丢了
	eTaskFail = 3,		-- 任务失败了
}

function CLuaCityWarMgr.SyncYaYunTaskStatusChange(newStatus, reason, expireTime)

	local name = "YaYun_Fake_Task"

	if newStatus == EnumYaYunTaskStatus.eNoTask then
		CActivityMgr.Inst:DeleteActivity(name)
		return
	end

	local title = YaYun_Setting.GetData().TaskTitle
	local desc1 = YaYun_Setting.GetData().TaskNormalDesc
	local desc2 = YaYun_Setting.GetData().TaskLoseBiaoCheDesc
	local desc3 = LocalString.GetString("[ff0000]（失败）[-]\n任务已失败，点击此处可放弃任务")
	local onclick = DelegateFactory.Action(function()

		local mainplayer = CClientMainPlayer.Inst
		if not mainplayer then return end
		local status = mainplayer.PlayProp.YaYunData.YaYunTaskStatus
		local expireTime = mainplayer.PlayProp.YaYunData.TaskExpireTime
		if status == EnumYaYunTaskStatus.eLoseBiaoChe and CServerTimeMgr.Inst.timeStamp < expireTime then
			Gac2Gas.RequestTrackToSelfBiaoChe()
		else
			MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("GIVEUP_YAYUN_TASK_CONFIRM"), DelegateFactory.Action(function ()
				Gac2Gas.RequestGiveUpYaYunTask()
	    	end), nil, nil, nil, false)
	    end

	end)

	if newStatus == EnumYaYunTaskStatus.eInYaYun then
		CActivityMgr.Inst:UpdateAcitivity(name,desc1, onclick, expireTime, title, true)
	elseif newStatus == EnumYaYunTaskStatus.eLoseBiaoChe then
		CActivityMgr.Inst:UpdateAcitivity(name,desc2, onclick, expireTime, title, false)
	elseif newStatus == EnumYaYunTaskStatus.eTaskFail then
		CActivityMgr.Inst:UpdateAcitivity(name,desc3, onclick, 0, title, false)
	end
end

--提供给CCityWarMgr调用的，请慎重改动和使用
function CLuaCityWarMgr.MiniMapForceShowBiaoChe(biaoCheEngineId)
	return CLuaCityWarMgr.MainPlayerBiaoCheIsValidForMiniMap() and CLuaCityWarMgr.IsMyTeamBiaoChe(biaoCheEngineId)
end

function CLuaCityWarMgr.IsMyTeamBiaoChe(biaoCheEngineId)
	local mainplayer = CClientMainPlayer.Inst
	return mainplayer and biaoCheEngineId == mainplayer.PlayProp.YaYunData.TeamBiaoCheEngineId or false
end

function CLuaCityWarMgr.MainPlayerBiaoCheIsValidForMiniMap()
	--右上角小地图上，由于图标显示时机为ClientObjectCreate，因此在丢失可找回镖车的情况也显示
	local mainplayer = CClientMainPlayer.Inst
    local status = mainplayer and mainplayer.PlayProp.YaYunData.YaYunTaskStatus
    local expireTime = mainplayer and mainplayer.PlayProp.YaYunData.TaskExpireTime
    if (status == EnumYaYunTaskStatus.eInYaYun or status == EnumYaYunTaskStatus.eLoseBiaoChe) and (CServerTimeMgr.Inst.timeStamp < expireTime) then
    	return true
    else
    	return false
    end
end

function CLuaCityWarMgr.MainPlayerBiaoCheIsValidForCityWarMap()
	--二级地图和三级地图上，只有押运中显示
	local mainplayer = CClientMainPlayer.Inst
    local status = mainplayer and mainplayer.PlayProp.YaYunData.YaYunTaskStatus
    local expireTime = mainplayer and mainplayer.PlayProp.YaYunData.TaskExpireTime
    if status == EnumYaYunTaskStatus.eInYaYun  and CServerTimeMgr.Inst.timeStamp < expireTime then
    	return true
    else
    	return false
    end
end

--战龙任务道具的使用处理
local CTaskMgr = import "L10.Game.CTaskMgr"
local CItemMgr = import "L10.Game.CItemMgr"
CTaskMgr.m_hookRequestUseTaskItem = function (mgr, taskId, place, pos, itemId)
	-- 战龙道具的处理逻辑拷贝自PackageView
	if taskId == 21030384 then
		Gac2Gas.RequestUseBombForCityWarZhanLong(itemId, pos)
	elseif taskId == 21030385 then
		Gac2Gas.RequestDamageQiXieForCityWarZhanLong(itemId, pos)
	elseif taskId == 21030386 then
		Gac2Gas.RequestOpenGrabWndForCityWarZhanLong(itemId, pos)
	elseif taskId == 21030393 then
		Gac2Gas.RequestRepairWallForCityWarZhanLong(itemId, pos)
	elseif taskId == 21030394 then
		Gac2Gas.RequestRepairQiXieForCityWarZhanLong(itemId, pos)
	elseif taskId == 21030468 then
		Gac2Gas.RequestSummonMonsterForCityWarZhanLong(itemId, pos)
	elseif taskId == 21030469 then
		Gac2Gas.RequestSummonSpyNpcForCityWarZhanLong(itemId, pos)
	end

	local item = CItemMgr.Inst:GetById(itemId)
	if item and item.TemplateId == CLuaCityWarMgr.ZhanLongFlagTemplateId then
		Gac2Gas.RequestPlaceFlagForCityWarZhanLong(itemId, pos)
	else
		Gac2Gas.RequestUseItem(EnumToInt(place), pos, itemId, "")
	end
end

---

--------------------------------------------------------------------
--- 天门山相关
---------------------------------------------------------------------
CLuaCityWarMgr.TianMenShanStatInfo = nil
CLuaCityWarMgr.TianMenShanPlayersInCopy = nil

function CLuaCityWarMgr:RequestTianMenShanStat(statType)
	Gac2Gas.QueryTianMenShanBattleFightData(statType)
end

function CLuaCityWarMgr:RequestTianMenShanSceneInfo()
	Gac2Gas.QueryTianMenShanBattleCopyMemberInfo()
end

function CLuaCityWarMgr:ReqeustBroadCastTianMenShanStat(statType)
	Gac2Gas.BroadcastTianMenShanBattleFightData(statType)
end
---------------------------------------------------------------------

---------------------------------------------------------------------
--- 显示快捷摆放城墙的辅助unit
---------------------------------------------------------------------
CLuaCityWarMgr.OpenAuxiliaryDisplay = true -- 开关

CLuaCityWarMgr.adjacentUnit1 = nil
CLuaCityWarMgr.adjacentUnit2 = nil
CLuaCityWarMgr.RecentPlacedUnitId = -1

function CLuaCityWarMgr:ClearAdjacentUnit()
	if CLuaCityWarMgr.adjacentUnit1 then
		CLuaCityWarMgr.adjacentUnit1:Destroy()
	end
	if CLuaCityWarMgr.adjacentUnit2 then
		CLuaCityWarMgr.adjacentUnit2:Destroy()
	end

	CLuaCityWarMgr.adjacentUnit1, CLuaCityWarMgr.adjacentUnit2 = nil, nil
	CLuaCityWarMgr.RecentPlacedUnitId = -1
end

function CLuaCityWarMgr:TryCreateCityUnitAtFormerAdjacentPos(cityId, templateId, unitId)
	if not CLuaCityWarMgr.OpenAuxiliaryDisplay then return end

	local theChosenOne = CLuaCityWarMgr.adjacentUnit1 or CLuaCityWarMgr.adjacentUnit2
	local posX = math.floor(theChosenOne.RO.Position.x)
	local posZ = math.floor(theChosenOne.RO.Position.z)
	local rot = theChosenOne.RO.transform.eulerAngles.y
	CCityWarMgr.Inst:ClearCurUnit()
	local unit = CCityWarUnit.Create(unitId, templateId, posX, posZ, rot, 0, 1, 0, false)
	unit.IsPlayerOwner = true
	CCityWarMgr.Inst.CurUnit = unit
	CCityWarMgr.Inst.CurUnit.Selected = true
	CCityWarMgr.Inst.CurUnit.Moving = true

	unit:ShowActionPopupMenu()

	CLuaCityWarMgr:ClearAdjacentUnit()
end

function CLuaCityWarMgr:DisplayTwoAdjacentUnit(cityId, templateId, unitId, xPos, zPos, dir)
	if not CLuaCityWarMgr.OpenAuxiliaryDisplay then return end
	CLuaCityWarMgr:ClearAdjacentUnit()

	local designData = CLuaCityWarMgr.UnitDesignData[templateId]
	if designData.Type ~= 1 then --只对城墙显示辅助Unit
		return
	end

	--判断资材、数量限制等条件是否允许继续建造
	local level = 0
	if CLuaCityWarMgr.CurrentBasicInfo then
		level = CLuaCityWarMgr.CurrentBasicInfo.Grade
	end
	local gradeData = CityWar_CityGrade.GetData(level)
	local ddata = CityWar_Unit.GetData(templateId)
	if ddata and gradeData then
		local placedCount = CLuaCityWarMgr:GetPlacedCountForType(ddata.Type)
		local limitCount = CLuaCityWarMgr:GetTypeLimit(gradeData, ddata.Type)
		local zicai = CLuaCityWarMgr.CurrentMaterial

		if not (zicai >= ddata.Material and limitCount > placedCount) then
			return
		end
	end

	local yRot = (360 + 90 - dir) % 360

	local xOffset, zOffset = 0,0
		if designData then
		if yRot == 90 or yRot == 270 then
			xOffset = 0
			zOffset = designData.Height
		else
			xOffset = designData.Height
			zOffset = 0
		end
	end

	CLuaCityWarMgr.adjacentUnit1 = CCityWarUnit.Create(unitId, templateId, xPos + xOffset, zPos + zOffset, yRot, 0, 1, 0, false)
	CLuaCityWarMgr.adjacentUnit2 = CCityWarUnit.Create(unitId, templateId, xPos - xOffset, zPos - zOffset, yRot, 0, 1, 0, false)

	-- 显示白色outline
	CLuaCityWarMgr.UpdateAuxiliaryStateRender(CLuaCityWarMgr.adjacentUnit1)
	CLuaCityWarMgr.UpdateAuxiliaryStateRender(CLuaCityWarMgr.adjacentUnit2)


	if not CLuaCityWarMgr.adjacentUnit1:CheckMoveUnit() then
		CLuaCityWarMgr.adjacentUnit1:Destroy()
		CLuaCityWarMgr.adjacentUnit1 = nil
	end
	if not CLuaCityWarMgr.adjacentUnit2:CheckMoveUnit() then
		CLuaCityWarMgr.adjacentUnit2:Destroy()
		CLuaCityWarMgr.adjacentUnit2 = nil
	end
end

function CLuaCityWarMgr.CheckIfNotAuxiliaryUnitAndClear(cwUnit)
	if cwUnit ~= CLuaCityWarMgr.adjacentUnit1 and cwUnit ~= CLuaCityWarMgr.adjacentUnit2 then
		CLuaCityWarMgr:ClearAdjacentUnit()
	end
end

function CLuaCityWarMgr.GetCwUnitFromRO(RO)
	if not CLuaCityWarMgr.OpenAuxiliaryDisplay then return nil end

	if CLuaCityWarMgr.adjacentUnit1 ~= nil and RO == CLuaCityWarMgr.adjacentUnit1.RO then
		return CLuaCityWarMgr.adjacentUnit1
	elseif CLuaCityWarMgr.adjacentUnit2 ~= nil and RO == CLuaCityWarMgr.adjacentUnit2.RO then
		return CLuaCityWarMgr.adjacentUnit2
	else
		return nil
	end
end

function CLuaCityWarMgr.UpdateAuxiliaryStateRender(cwUnit)
	if not CLuaCityWarMgr.OpenAuxiliaryDisplay then return end

	local color = Color.white
	color.a = cwUnit.s_ShaderAlpha
	cwUnit.RO:SetOutLinedEffect(true, color)
end


--- CSharp hooks of Auxiliary Unit
CCityWarUnit.m_hookOnMouseUp = function(cwUnit)
	if CLuaCityWarMgr.OpenAuxiliaryDisplay and cwUnit == CLuaCityWarMgr.adjacentUnit1 then
		if CLuaCityWarMgr.adjacentUnit2 then
			CLuaCityWarMgr.adjacentUnit2:Destroy()
			CLuaCityWarMgr.adjacentUnit2 = nil
		end
		Gac2Gas.CreateCityUnit(CLuaCityWarMgr.CurrentCityId, cwUnit.TemplateId)
		return true
	elseif CLuaCityWarMgr.OpenAuxiliaryDisplay and cwUnit == CLuaCityWarMgr.adjacentUnit2 then
		if CLuaCityWarMgr.adjacentUnit1 then
			CLuaCityWarMgr.adjacentUnit1:Destroy()
			CLuaCityWarMgr.adjacentUnit1 = nil
		end
		Gac2Gas.CreateCityUnit(CLuaCityWarMgr.CurrentCityId, cwUnit.TemplateId)
		return true
	elseif cwUnit.Selected and cwUnit.Moving and CCityWarMgr.Inst.PlaceMode then
		cwUnit:ShowActionPopupMenu()
		return true
	elseif cwUnit.Selected and not cwUnit.Moving and cwUnit.mbMouseDownNotSelected then
		cwUnit.mbMouseDownNotSelected = false
		return true
	end

	return false
end
---------------------------------------------------------------------

--------------------------------------------------------------------- Rpc Start

function Gas2Gac.ShowOccupyHuFuConfirmWnd(guildId, mapId, templateId, occupyCityId, duration, currMapId, currTemplateId)
	CLuaCityWarMgr.OccupyGuildId = guildId
	CLuaCityWarMgr.OccupyMapId = mapId
	CLuaCityWarMgr.OccupyTemplateId = templateId
	CLuaCityWarMgr.OccupyCityId = occupyCityId
	CLuaCityWarMgr.OccupyDuration = duration

	CLuaCityWarMgr.MyMapId = currMapId
	CLuaCityWarMgr.MyCityTemplateId = currTemplateId
	CUIManager.ShowUI(CLuaUIResources.CityWarOccupyConfirmWnd)
end

function Gas2Gac.QueryTerritoryMapsOverviewResult(guildMapId, guildTemplateId, serverInfoUD, overViewUD, biaoCheInfoUd)
	g_ScriptEvent:BroadcastInLua("QueryTerritoryMapsOverviewResult", guildMapId, guildTemplateId, serverInfoUD, overViewUD, biaoCheInfoUd)
end

function Gas2Gac.QueryTerritoryMapDetailResult(mapId, guildCityTemplateId, mapDetailUD)
	g_ScriptEvent:BroadcastInLua("QueryTerritoryMapDetailResult", mapId, guildCityTemplateId, mapDetailUD)
end

function Gas2Gac.ShowTerritoryCityTips(mapId, templateId, cityId, ownerId, ownerName, ownerServerId, ownerServerName, grade, material, unitCount, fightState, duration, playNum, playIdsUD, isPriorityMatchPeriod, priorityMatchLeftTime)
	CLuaCityWarMgr.CityInfoId = cityId
	CLuaCityWarMgr.CityInfoTemplateId = templateId
	CLuaCityWarMgr.CityInfoGuildId = ownerId
	CLuaCityWarMgr.CityInfoLevel = grade
	CLuaCityWarMgr.CityInfoGuildName = ownerName
	CLuaCityWarMgr.CityInfoServerName = ownerServerName
	CLuaCityWarMgr.CityInfoAssetAmount = material
	CLuaCityWarMgr.CityInfoBuildingAmount = unitCount
	CLuaCityWarMgr.CityInfoStatus = fightState
	CLuaCityWarMgr.CityInfoDuration = duration
	CLuaCityWarMgr.CityInfoMapTemplateId = mapId
	CLuaCityWarMgr.CityInfoPlayNum = playNum
	CLuaCityWarMgr.CityInfoPlayIdsUD = playIdsUD

--	print("ShowTerritoryCityTips", mapId, templateId, cityId, ownerId, ownerName, ownerServerId, ownerServerName, grade, material, unitCount, fightState, duration, playNum, isPriorityMatchPeriod, priorityMatchLeftTime)
	g_ScriptEvent:BroadcastInLua("QueryCityInfoResult")
end

-- actionType -> EnumQueryCityDataAction
function Gas2Gac.SendMyCityData(actionType, guildPlayDataUD, basicInfoUD, unitInfoUD, miscInfoUD)
	local guildPlayData = CreateFromClass(CGuildCityWarPlayData)
	guildPlayData:LoadFromString(guildPlayDataUD)
	local basicInfo = CreateFromClass(CCityBasicInfo)
	basicInfo:LoadFromString(basicInfoUD)
	local unitInfo = CreateFromClass(CCityUnitInfo)
	unitInfo:LoadFromString(unitInfoUD)
	local miscInfo = CreateFromClass(CCityMiscInfo)
	miscInfo:LoadFromString(miscInfoUD)

	CLuaCityWarMgr.MyCityGuildPlayData, CLuaCityWarMgr.MyCityBasicInfo, CLuaCityWarMgr.MyCityUnitInfo, CLuaCityWarMgr.MyCityMiscInfo = guildPlayData, basicInfo, unitInfo, miscInfo
	CLuaCityWarMgr.MyCityWndOpenActionType = actionType
	CUIManager.ShowUI(CLuaUIResources.CityMainWnd)
	--g_ScriptEvent:BroadcastInLua("SyncMyCityData", guildPlayData, basicInfo, unitInfo, miscInfo)
end

function Gas2Gac.SyncGuildCityWarMaterial(guildId, material)
--	print("Gas2Gac.SyncGuildCityWarMaterial", guildId, material)
	local playerGuildId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.GuildId or 0
	if playerGuildId ~= 0 and playerGuildId == guildId then
		CLuaCityWarMgr.CurrentMaterial = material

		g_ScriptEvent:BroadcastInLua("SyncGuildCityWarMaterial")
	end
end

function Gas2Gac.ShowCityToPlayer(basicInfoUD, unitInfoUD, miscInfoUD)
	if CScene.MainScene then
		CScene.MainScene.IsEnableWallBlockSkill = true
	end

	local basicInfo = CreateFromClass(CCityBasicInfo)
	basicInfo:LoadFromString(basicInfoUD)
	local unitInfo = CreateFromClass(CCityUnitInfo)
	unitInfo:LoadFromString(unitInfoUD)
	local miscInfo = CreateFromClass(CCityMiscInfo)
	miscInfo:LoadFromString(miscInfoUD)

	-- 这个必须先设置上值，否则territoryScene里面的用到的时候不对
	CLuaCityWarMgr.CurrentGuildId = basicInfo.OwnerId
	CLuaCityWarMgr.CurrentGuildName = basicInfo.OwnerName
	CLuaCityWarMgr.CurrentCityId = basicInfo.Id

	CLuaCityWarMgr.CurrentBasicInfo = basicInfo
	CLuaCityWarMgr.CurrentUnitInfo = unitInfo
	CLuaCityWarMgr.CurrentMiscInfo = miscInfo

    CommonDefs.DictIterate(CLuaCityWarMgr.CurrentUnitInfo.Units, DelegateFactory.Action_object_object(function (___key, ___value)
        local unitData = ___value
        CLuaCityWarMgr:AddCityUnit(unitData.Id, unitData.TemplateId, unitData.X, unitData.Y, unitData.Dir, nil)
    end))


	--判断是否触发引导
	local guildId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.GuildId or 0
	if CLuaCityWarMgr.CurrentGuildId==guildId then
		CLuaGuideMgr.TryTriggerCityWarPlaceGuide()
	end

	g_ScriptEvent:BroadcastInLua("UpdateCityWarGrade")

	-- 重新寻路
	if CTrackMgr.Inst.isTracking then
		CClientMainPlayer.Inst.m_TargetPos = CPos.Zero
	end
end

function Gas2Gac.SendCityUnitAgentEngineIds(cityId, dataUD)
	local list = MsgPackImpl.unpack(dataUD)
	if not list then return end

	for i = 0, list.Count - 1, 2 do
		local unitId = list[i]
		local engineId = list[i + 1]
		CLuaCityWarMgr:AddCityUnitEngineId(engineId, unitId)
	end
end

function Gas2Gac.HideCity(cityId)
--	print("HideCity", cityId)
	CLuaCityWarMgr.CityInfoId = 0
	CLuaCityWarMgr.CityInfoTemplateId = 0
	CLuaCityWarMgr.CityInfoGuildId = 0
	CLuaCityWarMgr.CityInfoLevel = 0
	CLuaCityWarMgr.CityInfoGuildName = ""
	CLuaCityWarMgr.CityInfoServerName = ""
	CLuaCityWarMgr.CityInfoAssetAmount = 0
	CLuaCityWarMgr.CityInfoBuildingAmount = 0
	CLuaCityWarMgr.CityInfoStatus = 0
	CLuaCityWarMgr.CityInfoDuration = 0

	CLuaCityWarMgr.CurrentGuildId = 0
	CLuaCityWarMgr.CurrentGuildName = ""
	CLuaCityWarMgr.CurrentCityId = ""
	CLuaCityWarMgr.CurrentMaterial = 0
	CLuaCityWarMgr.CurrentBasicInfo = nil
	CLuaCityWarMgr.CurrentUnitInfo = nil
	CLuaCityWarMgr.CurrentMiscInfo = nil

	CLuaCityWarMgr.LastUsedTemplateIdx = -1
	CLuaCityWarMgr:StopPlaceMode()

	CLuaCityWarMgr:RemoveCityUnitFromMap()

	CLuaCityWarMgr:ClearCityData()
	CScene.MainScene:SkillWallClear()

	g_ScriptEvent:BroadcastInLua("HideCity")
end

function CLuaCityWarMgr:ClearCityData(bClearRebornPoints)
	if bClearRebornPoints then
		CommonDefs.DictClear(CCityWarMgr.Inst.RO2Unit)
		CommonDefs.DictClear(CCityWarMgr.Inst.ClientObjectList)
		CommonDefs.DictIterate(CCityWarMgr.Inst.UnitList, DelegateFactory.Action_object_object(function (___key, ___value)
			local unit = ___value
			unit:Destroy()
		end))
		CommonDefs.DictClear(CCityWarMgr.Inst.UnitList)
		CommonDefs.ListClear(CCityWarMgr.Inst.RebornPointsList)
	else
		-- 排除复活点
		CommonDefs.DictClear(CCityWarMgr.Inst.RO2Unit)

		local removeEngineIds = {}
		CommonDefs.DictIterate(CCityWarMgr.Inst.ClientObjectList, DelegateFactory.Action_object_object(function (___key, ___value)
			local unitId = ___value
			local unit = CommonDefs.DictGetValue_LuaCall(CCityWarMgr.Inst.UnitList, unitId)
			if unit and not CCityWarMgr.Inst:IsRebornPointUnit(unit) then
				table.insert(removeEngineIds, ___key)
			end
		end))
		for _, engineId in ipairs(removeEngineIds) do
			CommonDefs.DictRemove_LuaCall(CCityWarMgr.Inst.ClientObjectList, engineId)
		end

		local removeUnitIds = {}
		CommonDefs.DictIterate(CCityWarMgr.Inst.UnitList, DelegateFactory.Action_object_object(function (___key, ___value)
			local unit = ___value
			if unit and not CCityWarMgr.Inst:IsRebornPointUnit(unit) then
				table.insert(removeUnitIds, ___key)
			end
		end))
		for _, unitId in ipairs(removeUnitIds) do
			local unit = CommonDefs.DictGetValue_LuaCall(CCityWarMgr.Inst.UnitList, unitId)
			unit:Destroy()
			CommonDefs.DictRemove_LuaCall(CCityWarMgr.Inst.UnitList, unitId)
		end
	end

	if (CCityWarMgr.Inst.mLineGO ~= nil) then
		GameObject.Destroy(CCityWarMgr.Inst.mLineGO)
		CCityWarMgr.Inst.mLineGO = nil
	end
	if (CCityWarMgr.Inst.mHufuGridGO ~= nil) then
		GameObject.Destroy(CCityWarMgr.Inst.mHufuGridGO)
		CCityWarMgr.Inst.mHufuGridGO = nil
	end
end

function CLuaCityWarMgr:RemoveCityUnitFromMap()
    CommonDefs.DictIterate(CCityWarMgr.Inst.UnitList, DelegateFactory.Action_object_object(function (___key, ___value)
        local unit = ___value
		local serverX, serverZ, serverRot = unit.ServerPos.x, unit.ServerPos.z, unit.ServerRotateY
        CLuaCityWarMgr:MoveCityUnitInternal(unit.TemplateId, unit.ID, serverX, serverZ, serverRot)
    end))
end

function Gas2Gac.CreateCityUnit(cityId, templateId, unitId)
	if CLuaCityWarMgr.OpenAuxiliaryDisplay and (CLuaCityWarMgr.adjacentUnit1 or CLuaCityWarMgr.adjacentUnit2) then
		CLuaCityWarMgr:TryCreateCityUnitAtFormerAdjacentPos(cityId, templateId, unitId)
	else
		CLuaCityWarMgr:CreateCityUnit(cityId, templateId, unitId)
	end
end

function Gas2Gac.AddCityUnit(cityId, templateId, unitId, x, y, dir, engineId)
	CLuaCityWarMgr:AddCityUnitEngineId(engineId, unitId)
	CLuaCityWarMgr:AddCityUnit(unitId, templateId, x, y, dir, engineId)
	g_ScriptEvent:BroadcastInLua("CityWarUnitPlaceEvent")

	if CLuaCityWarMgr.OpenAuxiliaryDisplay and unitId == CLuaCityWarMgr.RecentPlacedUnitId then
		CLuaCityWarMgr:DisplayTwoAdjacentUnit(cityId, templateId, unitId, x, y, dir)
	end
end

function Gas2Gac.MoveCityUnit(cityId, unitId, x, y, dir, engineId)
	if CommonDefs.DictContains_LuaCall(CCityWarMgr.Inst.UnitList, unitId) then
		local unit = CommonDefs.DictGetValue_LuaCall(CCityWarMgr.Inst.UnitList, unitId)
		local origEngineId = unit.EngineId
		if origEngineId ~= 0 and CommonDefs.DictContains_LuaCall(CCityWarMgr.Inst.ClientObjectList, origEngineId) then
			CommonDefs.DictRemove_LuaCall(CCityWarMgr.Inst.ClientObjectList, origEngineId)
		end
		CommonDefs.DictAdd_LuaCall(CCityWarMgr.Inst.ClientObjectList, engineId, unitId)
		unit.EngineId = engineId
		local serverX, serverZ, serverRot = unit.ServerPos.x, unit.ServerPos.z, unit.ServerRotateY
		if not(serverX == x and serverZ == y and dir == serverRot) then
			CLuaCityWarMgr:MoveCityUnitInternal(unit.TemplateId, unitId, serverX, serverZ, serverRot, x, y, dir)
			unit.ServerPos = Vector3(x, 0, y)
			unit:SetPosition(unit.ServerPos)
			unit.ServerRotateY = dir
			if unit.ro then
				unit.ro.Direction = 90 - dir
				unit.ro.Visible = not unit.Hidden
			end
		end
		local guildId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.GuildId or 0
		if guildId == CLuaCityWarMgr.CurrentGuildId then
			unit.IsPlayerOwner = true
		end
		if unit.RO and unit.RO.Visible then
			local unitDData = CityWar_Unit.GetData(unit.TemplateId)
			if unitDData and unitDData.RangeFxId > 0 then
				unit.RangeFx=CEffectMgr.Inst:AddObjectFX(unitDData.RangeFxId,unit.RO,3,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
			end
		end
	end
end

function Gas2Gac.RemoveCityUnit(cityId, unitId, bNoRefreshUI)
	if CommonDefs.DictContains_LuaCall(CCityWarMgr.Inst.UnitList, unitId) then
		local unit = CommonDefs.DictGetValue_LuaCall(CCityWarMgr.Inst.UnitList, unitId)
		local serverX, serverZ, serverRot = unit.ServerPos.x, unit.ServerPos.z, unit.ServerRotateY
		CLuaCityWarMgr:MoveCityUnitInternal(unit.TemplateId, unitId, serverX, serverZ, serverRot, nil, nil, nil)

		if unit.ro and CommonDefs.DictContains_LuaCall(CCityWarMgr.Inst.RO2Unit, unit.ro) then
			CommonDefs.DictRemove_LuaCall(CCityWarMgr.Inst.RO2Unit, unit.ro)
		end

		CommonDefs.DictRemove_LuaCall(CCityWarMgr.Inst.UnitList, unitId)
		unit:Destroy()
		if not bNoRefreshUI then
			g_ScriptEvent:BroadcastInLua("CityWarUnitPlaceEvent")
		end
	end
end

function Gas2Gac.ClearCityUnits(cityId)
	if CLuaCityWarMgr.CurrentCityId ~= cityId then
		return
	end
	local unitIdList = {}
	CommonDefs.DictIterate(CCityWarMgr.Inst.UnitList, DelegateFactory.Action_object_object(function (___key, ___value)
		table.insert(unitIdList, ___key)
	end))
	for _, unitId in ipairs(unitIdList) do
		if CommonDefs.DictContains_LuaCall(CCityWarMgr.Inst.UnitList, unitId) then
			local unit = CommonDefs.DictGetValue_LuaCall(CCityWarMgr.Inst.UnitList, unitId)
			local serverX, serverZ, serverRot = unit.ServerPos.x, unit.ServerPos.z, unit.ServerRotateY
			CLuaCityWarMgr:MoveCityUnitInternal(unit.TemplateId, unitId, serverX, serverZ, serverRot, nil, nil, nil)

			if unit.ro and CommonDefs.DictContains_LuaCall(CCityWarMgr.Inst.RO2Unit, unit.ro) then
				CommonDefs.DictRemove_LuaCall(CCityWarMgr.Inst.RO2Unit, unit.ro)
			end

			CommonDefs.DictRemove_LuaCall(CCityWarMgr.Inst.UnitList, unitId)
			unit:Destroy()
		end
	end

	g_ScriptEvent:BroadcastInLua("CityWarUnitPlaceEvent")
end

function Gas2Gac.RequestUseCityWarLayoutTemplateCheckResult(cityId, bSuccess, idx)
--	print("Gas2Gac.RequestUseCityWarLayoutTemplateCheckResult", cityId, bSuccess)
	if bSuccess and  CLuaCityWarMgr.CurrentBasicInfo and CLuaCityWarMgr.CurrentBasicInfo.Id == cityId then
		CLuaCityWarMgr.LastUsedTemplateIdx = idx
		CCityWarMgr.Inst:UseLayout(idx)
	end
end

function Gas2Gac.UpgradeCityUnit(cityId, templateId, unitId, engineId)

	if CommonDefs.DictContains_LuaCall(CCityWarMgr.Inst.UnitList, unitId) then
		local unit = CommonDefs.DictGetValue_LuaCall(CCityWarMgr.Inst.UnitList, unitId)
		local oriEngineId = unit.EngineId
		if CommonDefs.DictContains_LuaCall(CCityWarMgr.Inst.ClientObjectList, oriEngineId) then
			CommonDefs.DictRemove_LuaCall(CCityWarMgr.Inst.ClientObjectList, oriEngineId)
		end
		CommonDefs.DictAdd_LuaCall(CCityWarMgr.Inst.ClientObjectList, engineId, unitId)

		unit.EngineId = engineId
		unit.TemplateId = templateId
		unit:ReloadResource()
		if unit.ro then
			unit.ro.Visible = not unit.Hidden
		end

		g_ScriptEvent:BroadcastInLua("CityWarUnitPlaceEvent")
	end
end

function Gas2Gac.SendRepairCityUnitInfo(cityId, templateId, unitId, needRepair, material)
	if not needRepair then
		g_MessageMgr:ShowMessage("REPAIR_CITY_UNIT_NOT_DAMAGED")
		return
	end
	local designData = CityWar_Unit.GetData(templateId)
	if not designData then return end
	local txtStr = SafeStringFormat3(LocalString.GetString("确定将%s维修至满耐久？"), designData.Name)
	QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.CityWarMaterial, txtStr, material, DelegateFactory.Action(function()
		Gac2Gas.RepairCityUnit(cityId, templateId, unitId)
		end), nil, false, true, EnumPlayScoreKey.NONE, false)
end

function Gas2Gac.SendRepairAllCityUnitInfo(cityId, needRepair, material)
	if not needRepair then
		g_MessageMgr:ShowMessage("REPAIR_ALL_CITY_UNITS_NOT_DAMAGED")
		return
	end
	QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.CityWarMaterial, LocalString.GetString("确定将所有城建一键维修至满耐久？"), material, DelegateFactory.Action(function()
		Gac2Gas.RepairAllCityUnit(cityId)
		end), nil, false, true, EnumPlayScoreKey.NONE, false)
end

function Gas2Gac.UpdateCityGrade(cityId, grade)
--	print("UpdateCityGrade", cityId, grade)
	if CLuaCityWarMgr.CurrentBasicInfo then
		local curCityId = CLuaCityWarMgr.CurrentBasicInfo.Id
		if curCityId == cityId then
			CLuaCityWarMgr.CurrentBasicInfo.Grade = grade
			g_ScriptEvent:BroadcastInLua("UpdateCityWarGrade")
		end
	end
end

function Gas2Gac.SyncCityOccupyingProgress(cityTemplateId, npcEngineId, progress, guildId, guildName)
--	print("SyncCityOccupyingProgress", cityTemplateId, npcEngineId, progress, guildId, guildName)
	g_ScriptEvent:BroadcastInLua("SyncCityOccupyingProgress", npcEngineId, guildName, progress)
end

function Gas2Gac.UpdateMirrorWarOccupyInfo(warIndex, force, activated, progress, occupyForce)
--	print("UpdateMirrorWarOccupyInfo", warIndex, force, activated, progress, occupyForce)
	CLuaCityWarMgr.WarOccupyInfo[warIndex * 10 + force] = {Activated = activated, Progress = progress, OccupyForce = occupyForce}
	g_ScriptEvent:BroadcastInLua("UpdateMirrorWarOccupyInfo", warIndex, force, activated, progress, occupyForce)
end

function Gas2Gac.QueryMirrorWarSummaryResult(finish, winForce, playResultUD, forceInfoUD, summaryInfoUD)
	g_ScriptEvent:BroadcastInLua("QueryMirrorWarSummaryResult", finish, winForce, playResultUD, forceInfoUD, summaryInfoUD)
	if not CUIManager.IsLoaded(CUIResources.CityWarResultWnd) then
		CUIManager.ShowUI(CUIResources.CityWarResultWnd)
	end
end

function Gas2Gac.QueryMirrorWarPlayInfoResult(warIndex, finish, winForce, forceInfoUD, summaryInfoUD, playerInfoUD)
	g_ScriptEvent:BroadcastInLua("QueryMirrorWarPlayInfoResult", warIndex, finish, winForce, forceInfoUD, summaryInfoUD, playerInfoUD)
end

function Gas2Gac.ShowMirrorWarMapInfo(myMapId, infoUD, isPriorityMatchPeriod, priorityMatchLeftTime, declaredPriorityMatchMapId)
	CLuaCityWarMgr.WarMapInfoMyMapId = myMapId
	CLuaCityWarMgr.WarMapInfoMapInfoUD = infoUD
	CLuaCityWarMgr.WarMapInfoIsMatchPeriod = isPriorityMatchPeriod
	CLuaCityWarMgr.WarMapInfoMatchLeftTime = priorityMatchLeftTime
	CLuaCityWarMgr.WarMapInfoDeclaredMapId = declaredPriorityMatchMapId
	CUIManager.ShowUI(CLuaUIResources.CityWarChallengeWnd)
end

function Gas2Gac.UpdateMirrorWarForceInfo(forceInfoUD, warIndex)
	CLuaCityWarMgr.WarForceInfo = {}
	local list = MsgPackImpl.unpack(forceInfoUD)
	if not list then return end
	for i = 0, list.Count - 1, 5 do
		local guildId = list[i]
		local force = list[i + 1]
		local guildName = list[i + 2]
		local mapId = list[i + 3]
		local templateId = list[i + 4]
--		print("UpdateMirrorWarForceInfo", guildId, force, guildName, mapId, templateId)
		table.insert(CLuaCityWarMgr.WarForceInfo, {GuildId = guildId, Force = force, GuildName = guildName, MapId = mapId, TemplateId = templateId})
	end
	CLuaCityWarMgr.WarIndex = warIndex
	CCityWarMgr.Inst.m_WarIndex = warIndex
	if CCityWarMgr.Inst:IsInCityWarCopyScene() then
		if CMiniMap.Instance then
            CMiniMap.Instance:UpdateMiniMapName(CScene.MainScene.SceneTemplateId, CScene.MainScene.SceneName.."-"..warIndex, 0)
        end
	end
	--CLuaCityWarMgr:InitCityWarCopyTeleportFx()
	g_ScriptEvent:BroadcastInLua("UpdateMirrorWarForceInfo")
end

CLuaCityWarMgr.PriorityMatchGuildId = nil
CLuaCityWarMgr.PriorityMatchGuildName = nil
CLuaCityWarMgr.PriorityMatchGroupDataTable = nil
CLuaCityWarMgr.PriorityMatchInGroup = false
CLuaCityWarMgr.PriorityMatchInPeriod = false
CLuaCityWarMgr.PriorityMatchLeftTime = nil
CLuaCityWarMgr.PriorityMatchMapId = nil
CLuaCityWarMgr.PriorityMatchTempalteId = nil
function Gas2Gac.SendMirrorWarPriorityMatchGroup(rivalGuildId, rivalGuildName, rivalMapId, rivalTemplateId, dataUD, inPriorityMatchGroup, isPriorityMatchPeriod, priorityMatchLeftTime)
	local list = MsgPackImpl.unpack(dataUD)
	if not list then return end
	CLuaCityWarMgr.PriorityMatchGuildId = rivalGuildId
	CLuaCityWarMgr.PriorityMatchGuildName = rivalGuildName
	CLuaCityWarMgr.PriorityMatchGroupDataTable = {}
	CLuaCityWarMgr.PriorityMatchInGroup = inPriorityMatchGroup
	CLuaCityWarMgr.PriorityMatchInPeriod = isPriorityMatchPeriod
	CLuaCityWarMgr.PriorityMatchLeftTime = priorityMatchLeftTime
	CLuaCityWarMgr.PriorityMatchMapId = rivalMapId
	CLuaCityWarMgr.PriorityMatchTempalteId = rivalTemplateId

	for i = 0, list.Count - 1, 9 do
		local mapId = list[i]
		local templateId = list[i + 1]
		local cityId = list[i + 2]
		local guildId = list[i + 3]
		local guildName = list[i + 4]
		local serverId = list[i + 5]
		local serverName = list[i + 6]
		local equipScore = list[i + 7]
		local mapLevel = list[i + 8]
		table.insert(CLuaCityWarMgr.PriorityMatchGroupDataTable, {MapId = mapId, TemplateId = templateId, CityId = cityId, GuildId = guildId, GuildName = guildName, ServerId = serverId, ServerName = serverName, Score = equipScore, MapLevel = mapLevel})
	end
	CUIManager.ShowUI(CLuaUIResources.CityWarDeclareRankWnd)
end

CLuaCityWarMgr.AwardTable = {}
CLuaCityWarMgr.StartTime = 0

function Gas2Gac.QueryTerritoryOccupyAwardListResult(startTime, dataUD)
	local list = MsgPackImpl.unpack(dataUD)
	if not list then return end
	local AwardTable = {}
	for i = 0, list.Count - 1, 6 do
		local t = {}
		t.awardType = list[i]		-- 1，day 2， week
		t.awardIndex = list[i + 1]
		t.awardTime = list[i + 2]
		t.expireTime = list[i + 3]
		t.temp = list[i + 4]		-- 1 累积中
		t.flag = list[i + 5]		-- 0 没领，1 已领取
		table.insert( AwardTable, t)
		-- print("TerritoryOccupyAwardListItem", t.awardType, t.awardIndex, t.awardTime, t.expireTime, t.temp, t.flag)
	end
	CLuaCityWarMgr.AwardTable = AwardTable
	CLuaCityWarMgr.StartTime = startTime
	CUIManager.ShowUI(CLuaUIResources.CityWarAwardWnd)
end

function Gas2Gac.QueryTerritoryDayOccupyAwardResult(temp, dayIndex, mapId, templateId, index, totalMingWang, totalExp, hourAwardsUD)
	-- print("QueryTerritoryDayOccupyAwardResult", temp, dayIndex, mapId, templateId, index, totalMingWang, totalExp)
	local list = MsgPackImpl.unpack(hourAwardsUD)
	if not list then return end
	local hourAwardTable = {}
	for i = 0, list.Count - 1, 2 do
		local t = {}
		t.mingwang = list[i]
		t.exp = list[i + 1]
		table.insert(hourAwardTable,t)
	end
	g_ScriptEvent:BroadcastInLua("QueryTerritoryDayOccupyAwardResult",temp, dayIndex, mapId, templateId, index, totalMingWang, totalExp, hourAwardTable)
end

function Gas2Gac.QueryTerritoryWeekOccupyAwardResult(temp, weekIndex, mapId, templateId, index, evaluation, yinpiao, banggong, mingwang)
	-- print("QueryTerritoryWeekOccupyAwardResult", temp, weekIndex, mapId, templateId, index, evaluation, yinpiao, banggong, mingwang)
	g_ScriptEvent:BroadcastInLua("QueryTerritoryWeekOccupyAwardResult",temp, weekIndex, mapId, templateId, index, evaluation, yinpiao, banggong, mingwang)
end

function Gas2Gac.RequestTerritoryOccupyAwardSuccess(awardType, awardIndex)
	-- print("RequestTerritoryOccupyAwardSuccess", awardType, awardIndex)
	g_ScriptEvent:BroadcastInLua("RequestTerritoryOccupyAwardSuccess",awardType, awardIndex)
end

function CLuaCityWarMgr:ClearRebornPoints()
	local rebornPointsList = CCityWarMgr.Inst.RebornPointsList
	for i = 0, rebornPointsList.Count - 1 do
		local unitId = rebornPointsList[i]
		if CommonDefs.DictContains_LuaCall(CCityWarMgr.Inst.UnitList, unitId) then
			local unit = CommonDefs.DictGetValue_LuaCall(CCityWarMgr.Inst.UnitList, unitId)
			unit:Destroy()
			CommonDefs.DictRemove_LuaCall(CCityWarMgr.Inst.UnitList, unitId)
		end
		local removeEngineId
		CommonDefs.DictIterateWithRet(CCityWarMgr.Inst.ClientObjectList, DelegateFactory.DictIterFunc(function (___key, ___value)
			if ___value == unitId then
				removeEngineId = ___key
				return true
			end
		end))
		if removeEngineId then
			CommonDefs.DictRemove_LuaCall(CCityWarMgr.Inst.ClientObjectList, removeEngineId)
		end
	end
	CommonDefs.ListClear(rebornPointsList)
end

function CLuaCityWarMgr:AddRebornPoints(engineId, x, y, dir)
	local unitId = CCityWarMgr.Inst.CurRebornPointUnitId + 1
	CCityWarMgr.Inst.CurRebornPointUnitId = unitId

	if CommonDefs.DictContains_LuaCall(CCityWarMgr.Inst.ClientObjectList, engineId) then
		CommonDefs.DictRemove_LuaCall(CCityWarMgr.Inst.ClientObjectList, engineId)
	end
	CommonDefs.DictAdd_LuaCall(CCityWarMgr.Inst.ClientObjectList, engineId, unitId)

	local unit = CCityWarUnit.Create(unitId, 2501, x, y, 0, engineId, 1, 0, false)
	unit.ServerPos = Vector3(x, 0, y)
	unit.ServerRotateY = dir
	unit.EngineId = engineId
	unit.Hidden = true

	CommonDefs.ListAdd_LuaCall(CCityWarMgr.Inst.RebornPointsList, unitId)
	CommonDefs.DictAdd_LuaCall(CCityWarMgr.Inst.UnitList, unitId, unit)

	if unit.ro then
		unit:SetPosition(unit.ServerPos)
		unit.ro.Direction = 90 - dir
	end
end

function Gas2Gac.UpdateMirrorWarRebornPoints(infoUD)
	CLuaCityWarMgr:ClearRebornPoints()

	CCityWarMgr.Inst.CurRebornPointUnitId = CCityWarMgr.Inst.DefaultRebornPointUnitId
	CLuaCityWarMgr.WarRebornPointInfo = {}
	local list = MsgPackImpl.unpack(infoUD)
	if not list then return end
	for i = 0, list.Count - 1, 5 do
		local mapId = list[i]
		local engineId = list[i + 1]
		local x = list[i + 2]
		local y = list[i + 3]
		local dir = list[i + 4]
--		print("RebornPoints", engineId, x, y)
		table.insert(CLuaCityWarMgr.WarRebornPointInfo, {MapId = mapId, EngineId = engineId, Posx = x, Posy = y, Dir = dir})

		CLuaCityWarMgr:AddRebornPoints(engineId, x, y, dir)
	end

	g_ScriptEvent:BroadcastInLua("UpdateMirrorWarRebornPoints")
end

function Gas2Gac.AlterCityMissionSuccess(mission)
--	print("AlterCityMissionSuccess", mission)
	g_ScriptEvent:BroadcastInLua("AlterCityMissionSuccess", mission)
end

function Gas2Gac.UpdateGuildBiaoCheInfo(actionType, biaoCheInfoUd)
	g_ScriptEvent:BroadcastInLua("UpdateGuildBiaoCheInfo", actionType, biaoCheInfoUd)
end
function Gas2Gac.SyncSetBiaoChePower(hasRight)
	g_ScriptEvent:BroadcastInLua("SyncSetBiaoChePower", hasRight)
end

-- 设置镖车时查询帮会资金的结果
function Gas2Gac.QueryGuildSilveForBiaoCheRes(guildSilver, mapId, cost, costUd)
	local list = MsgPackImpl.unpack(costUd)
	if not list then return end
	CLuaCityWarMgr.ShowPlaceBiaoCheConfirmWnd(mapId, guildSilver, cost, list[0])
end

function Gas2Gac.QuerySetGuildBiaoCheCostAllRes(costUd)
	local list = MsgPackImpl.unpack(costUd)
	if not list then return end

	local tbl = {}
	-- 花费list
	for i = 0, list.Count - 1, 2 do
		local mapId = list[i]
		local cost = list[i + 1]
		tbl[mapId] = cost
	end
	g_ScriptEvent:BroadcastInLua("QuerySetGuildBiaoCheCostAllResult", tbl)
end

function Gas2Gac.SyncBiaoCheNpcPos(npcTemplateId, x, y, biaoCheInfoUd)
	g_ScriptEvent:BroadcastInLua("SyncBiaoCheNpcPos", npcTemplateId, x, y, biaoCheInfoUd)
end

function Gas2Gac.SyncQueryGuildAltarResult(altarDataUd, biaoCheInfoUd)

	local zone2ProgressTbl = {}
	local activatedTbl = {}
	local biaoCheInfoTbl = {}
	local data = MsgPackImpl.unpack(altarDataUd)
	if data and data.Count == 6 then

		-- 首先是四个元素的进度
		local progressFire = data[0]
		local progressWood = data[1]
		local progressIce = data[2]
		local progressWind = data[3]

		-- 激活的两个圣坛元素的索引
		-- 这两个索引和 YaYun_ZoneNPC 第一列对应
		-- 有可能其中1-2个是0
		local activatedFirstIdx = data[4]
		local activatedSecondIdx = data[5]

		-- print(progressFire, progressWood, progressIce, progressWind, activatedFirstIdx, activatedSecondIdx)
		zone2ProgressTbl = {[1] = progressFire, [2] = progressWood, [3] = progressIce, [4] = progressWind}
		activatedTbl = {[1] = activatedFirstIdx, [2] = activatedSecondIdx}
	end
	if biaoCheInfoUd then
		local data = MsgPackImpl.unpack(biaoCheInfoUd)
		if data and data.Count >= 8 then
			biaoCheInfoTbl.biaoCheFMapId = tonumber(data[0])
			biaoCheInfoTbl.biaoCheSMapId = tonumber(data[1])
			biaoCheInfoTbl.biaoCheFProgress = tonumber(data[2])
			biaoCheInfoTbl.biaoCheSProgress = tonumber(data[3])
			biaoCheInfoTbl.biaoCheFStatus = tonumber(data[4])
			biaoCheInfoTbl.biaoCheSStatus = tonumber(data[5])
			biaoCheInfoTbl.biaoCheCloseTime = tonumber(data[6])
			-- data[7] 表示玩法是否处于押运中
		end
	end

	g_ScriptEvent:BroadcastInLua("SyncQueryGuildAltarResult", zone2ProgressTbl, activatedTbl, biaoCheInfoTbl)
end

-- 押运假任务的状态改变

-- 这个RPC会在很多情况下下发,可能是玩家/队长下线，接任务，丢失镖车，找到镖车，更换帮会等

-- newStatus 是新的状态
-- expireTime 是新的任务过期时间,到期后任务就无法提交了
-- reason 是改变的原因,参考服务端的 EnumYaYunTaskChangeReason,客户端应该用不到

-- 到期后服务端并不会主动去修改玩家的任务状态为过期
-- 需要客户端自己判断处理过期的任务为失败任务
function Gas2Gac.SyncYaYunTaskStatusChange(newStatus, reason, expireTime)
	local mainplayer = CClientMainPlayer.Inst
	if mainplayer then
		mainplayer.PlayProp.YaYunData.YaYunTaskStatus = newStatus
		mainplayer.PlayProp.YaYunData.TaskExpireTime = expireTime
	end
	CLuaCityWarMgr.SyncYaYunTaskStatusChange(newStatus, reason, expireTime)
end

-- 寻路到镖车
function Gas2Gac.DoTrackToBiaoChe(sceneId, biaoCheTemplateId, biaoCheMapId, posX, posY)

	local onSuccess = DelegateFactory.Action(function() CUIManager.ShowUI("CityWarBiaoCheConfirmWnd") end)

	CTrackMgr.Inst:FindNPC(biaoCheTemplateId, biaoCheMapId, posX, posY, sceneId, onSuccess)
end

function Gas2Gac.UpdateSubmitBiaoCheWnd(dataUD)
	local data = MsgPackImpl.unpack(dataUD)
	if data and data.Count == 3 then
		local mapId = data[0]
		local biaoCheStatus = data[1]
		local biaoCheProgress = data[2]

		g_ScriptEvent:BroadcastInLua("UpdateSubmitBiaoCheWnd", mapId, biaoCheStatus, biaoCheProgress)
	end
end

-- 解析方式和 UpdatePaoShangVehicleMapPos 一致
function Gas2Gac.UpdateYayunBiaoCheMapPos(posUd)
	local data = MsgPackImpl.unpack(posUd) -- List<object>
	g_ScriptEvent:BroadcastInLua("UpdateYayunBiaoCheMapPos", data)
end

function Gas2Gac.UpdateYaYunBiaoCheMonster(biaoCheEngineId)
	local mainplayer = CClientMainPlayer.Inst
	if mainplayer then
		mainplayer.PlayProp.YaYunData.TeamBiaoCheEngineId = biaoCheEngineId or 0
		g_ScriptEvent:BroadcastInLua("UpdateYaYunBiaoCheMonster")
	end
end

function Gas2Gac.SendCityWarHistory(dataUD)
	g_ScriptEvent:BroadcastInLua("SendCityWarHistory", dataUD)
end

function Gas2Gac.SendCityWarHistoryEnd()
	g_ScriptEvent:BroadcastInLua("SendCityWarHistoryEnd")
end

function Gas2Gac.SendCityWarHistoryPlaySummary(winForce, playResultUD, forceInfoUD, summaryInfoUD)
	CLuaCityWarMgr.HistoryWinForce = winForce
	CLuaCityWarMgr.HistoryPlayResultUD = playResultUD
	CLuaCityWarMgr.HistoryForceInfoUD = forceInfoUD
	CLuaCityWarMgr.HistorySummaryInfoUD = summaryInfoUD
	CLuaCityWarMgr.HistoryResultFlag = true
	CUIManager.ShowUI(CLuaUIResources.CityWarResultWnd)
end

function Gas2Gac.QueryMirrorWarWatchInfoResult(dataUD)
	CLuaCityWarMgr.WatchInfo = dataUD
	if not CUIManager.IsLoaded(CLuaUIResources.CityWarWatchWnd) then
		CUIManager.ShowUI(CLuaUIResources.CityWarWatchWnd)
	else
		g_ScriptEvent:BroadcastInLua("QueryMirrorWarWatchInfoResult", dataUD)
	end
end

function Gas2Gac.QueryEnemyPlayerPosResult(dataUD)
	g_ScriptEvent:BroadcastInLua("QueryEnemyPlayerPosResult", dataUD)
end

function Gas2Gac.ExchangeCityWarGuildZiCaiSuccess()
	g_ScriptEvent:BroadcastInLua("ExchangeCityWarGuildZiCaiSuccess")
end

-- 第一个占领城池,有设置镖车权限的管理弹出的设置镖车的窗口
function Gas2Gac.OpenSetBiaoCheWnd()
end

function Gas2Gac.QueryCityWarServerListResult(serverListUD, myServerGroupId)

	g_ScriptEvent:BroadcastInLua("QueryCityWarServerListResult", serverListUD, myServerGroupId)
end

function Gas2Gac.QueryCityWarServerCityListResult(cityListUD, serverId)

	g_ScriptEvent:BroadcastInLua("QueryCityWarServerCityListResult", cityListUD, serverId)
end

function Gas2Gac.QueryMirrorWarPlayNumResult(playNum)
	g_ScriptEvent:BroadcastInLua("QueryMirrorWarPlayNumResult", playNum)
end

function Gas2Gac.QueryCityWarOccupyOpenResult(bOpen)
	g_ScriptEvent:BroadcastInLua("QueryCityWarOccupyOpenResult", bOpen)
end

function Gas2Gac.QueryCityWarGuildToCityHashResult(dataUD)
	g_ScriptEvent:BroadcastInLua("QueryCityWarGuildToCityHashResult", dataUD)
end

function Gas2Gac.SendQueryCityFlagResult(guildId, flag)
	g_ScriptEvent:BroadcastInLua("SendQueryCityFlagResult", guildId, flag)
end

function Gas2Gac.SendQueryCityInfoResult_NotExist(guildId)
	-- 不存在什么也不做
end

function Gas2Gac.SendQueryCityInfoResult(guildId, guildPlayDataUD, basicInfoUD, unitInfoUD, miscInfoUD)
	-- 仿照Gas2Gac.SendMyCityData来解析
	local guildPlayData = CreateFromClass(CGuildCityWarPlayData)
	guildPlayData:LoadFromString(guildPlayDataUD)
	local basicInfo = CreateFromClass(CCityBasicInfo)
	basicInfo:LoadFromString(basicInfoUD)
	local unitInfo = CreateFromClass(CCityUnitInfo)
	unitInfo:LoadFromString(unitInfoUD)
	local miscInfo = CreateFromClass(CCityMiscInfo)
	miscInfo:LoadFromString(miscInfoUD)

	local myCityName = CityWar_MapCity.GetData(basicInfo.TemplateId).Name --占领城池名
	local regionName = CityWar_Map.GetData(basicInfo.MapId).Name --领土名称
	local mainCityName = CityWar_MapCity.GetData(CityWar_Map.GetData(basicInfo.MapId).MajorCity).Name --主城名称
	local grade = basicInfo.Grade --城池等级
	local guildName = basicInfo.OwnerName --帮会
	local material = guildPlayData.Material.."/"..CityWar_WareHouseGrade.GetData(miscInfo.WareHouseGrade).Material
	local repertoryLv = 0
	local gate = 0
	local wall = 0
	local huopao = 0
	local yuansuTower = 0
	local controlTower = 0
	local bomb = 0
	local zhongpao = 0
	local trap = 0
	local tanshenban = 0

	CommonDefs.DictIterate(unitInfo.Units, DelegateFactory.Action_object_object(function (key, value)
		local curType = CLuaCityWarMgr.UnitDesignData[value.TemplateId].Type
		if curType == EnumCityWarUnitType.eRepertory then
			repertoryLv = value.TemplateId % 100
		elseif curType == EnumCityWarUnitType.eGate then
			gate = gate + 1
		elseif curType == EnumCityWarUnitType.eWall then
			wall = wall + 1
		elseif curType == EnumCityWarUnitType.eHuoPao then
			huopao = huopao + 1
		elseif curType >= EnumCityWarUnitType.eWindTower and curType <= EnumCityWarUnitType.eWaterTower then
			yuansuTower = yuansuTower + 1
		elseif curType >= EnumCityWarUnitType.eBindTower and curType <= EnumCityWarUnitType.eTieTower then
			controlTower = controlTower + 1
		elseif curType == EnumCityWarUnitType.eBomb then
			bomb = bomb + 1
		elseif curType == EnumCityWarUnitType.eZhongPao then
			zhongpao = zhongpao + 1
		elseif curType == EnumCityWarUnitType.eTrap then
			trap = trap + 1
		elseif curType == EnumCityWarUnitType.eTanSheBan then
			tanshenban = tanshenban + 1
		end
	end))

	local tbl = {}
	tbl[1] = {name = LocalString.GetString("领土"), value = regionName}
	tbl[2] = {name = LocalString.GetString("主城"), value = mainCityName}
	tbl[3] = {name = LocalString.GetString("城池等级"), value = tostring(grade)}
	tbl[4] = {name = LocalString.GetString("帮会"), value = guildName}
	tbl[5] = {name = LocalString.GetString("仓库等级"), value = tostring(repertoryLv)}
	tbl[6] = {name = LocalString.GetString("资材"), value = material}
	tbl[7] = {name = LocalString.GetString("城门"), value = tostring(gate)}
	tbl[8] = {name = LocalString.GetString("城墙"), value = tostring(wall)}
	tbl[9] = {name = LocalString.GetString("喷射火炮"), value = tostring(huopao)}
	tbl[10] = {name = LocalString.GetString("重炮"), value = tostring(zhongpao)}
	tbl[11] = {name = LocalString.GetString("元素塔"), value = tostring(yuansuTower)}
	tbl[12] = {name = LocalString.GetString("控制塔"), value = tostring(controlTower)}
	tbl[13] = {name = LocalString.GetString("陷阱"), value = tostring(trap)}
	tbl[14] = {name = LocalString.GetString("弹射板"), value = tostring(tanshenban)}
	tbl[15] = {name = LocalString.GetString("炸弹"), value = tostring(bomb)}

	g_ScriptEvent:BroadcastInLua("SendQueryCityInfoResult", myCityName, tbl)

end

function Gas2Gac.SummonBackToOwnCity(senderId, senderName)
	local text = g_MessageMgr:FormatMessage("GUILD_MEMBER_BE_SUMMONED_TO_CITY_CONFIRM", tostring(senderId), senderName)
	MessageWndManager.ShowDefaultConfirm(text, 0, DelegateFactory.Action(function()
		Gac2Gas.AcceptSummonBackToOwnCity()
	end), nil, LocalString.GetString("同意"), LocalString.GetString("拒绝"))
end

function Gas2Gac.SummonTrackToGuildBiaoChe(senderId, senderName, mapId, mapName)
	local text = g_MessageMgr:FormatMessage("GUILD_MEMBER_BE_SUMMONED_TO_BIAOCHE_CONFIRM", tostring(senderId), senderName, mapName)
	MessageWndManager.ShowDefaultConfirm(text, 0, DelegateFactory.Action(function()
		Gac2Gas.AcceptSummonTrackToGuildBiaoChe(mapId)
	end), nil, LocalString.GetString("同意"), LocalString.GetString("拒绝"))
end

function Gas2Gac.SummonToMirrorWarPlay(senderId, senderName, warIndex)
	MessageWndManager.ShowDefaultConfirm(g_MessageMgr:FormatMessage("INVITE_GUILDMEMBER_TRANSFER_TO_CITYWAR_COPY"), 30, DelegateFactory.Action(function()
		Gac2Gas.AcceptSummonToMirrorWarPlay(warIndex)
	end), nil, LocalString.GetString("前往"), LocalString.GetString("取消"))
end

function Gas2Gac.AbandonCitySuccess(mapId, templateId)
	CUIManager.CloseUI(CLuaUIResources.CityMainWnd)
end

function Gas2Gac.QueryCityMonsterFightParamResult(monsterId, hpFull, reason)
	CLuaCityWarMgr.SetCityWarUnitInfoForTip(monsterId, hpFull, reason)
end

function Gas2Gac.QueryMirrorWarWatchEnableResult(bEnable)
	g_ScriptEvent:BroadcastInLua("QueryMirrorWarWatchEnableResult", bEnable)
end

CLuaCityWarMgr.m_TianMenShanLeftReliveCount = 0
function Gas2Gac.SyncTianMenShanBattleLeftReliveCount(count)
	CLuaCityWarMgr.m_TianMenShanLeftReliveCount = count
    g_ScriptEvent:BroadcastInLua("SyncTianMenShanBattleLeftReliveCount", count)
end

function Gas2Gac.SendTianMenShanBattleFightData(dataType, dataUD, playerCount, totalValue)
	local list = MsgPackImpl.unpack(dataUD)
	local n = list.Count
	if list and n % 4 == 0 and n / 4 == playerCount then
		local statTable = {}
		statTable.statType = dataType
		statTable.playerCount = playerCount
		statTable.statTotal = totalValue

		local players = {}
		for i = 0, n-1, 4 do
			local playerInfo = {}
			playerInfo.playerId = tonumber(list[i])
			playerInfo.playerName = tostring(list[i+1])
			playerInfo.playerClass = tonumber(list[i+2])
			playerInfo.playerStat = tonumber(list[i+3])

			table.insert(players, playerInfo)
		end

		table.sort(players, function(a,b)
			if a.playerStat == b.playerStat then
				return a.playerId < b.playerId
			end
			return a.playerStat > b.playerStat
		end)

		statTable.players = {}
		for i,v in ipairs(players) do
			statTable.players[i-1] = v
		end
		statTable.players.Count = #players

		CLuaCityWarMgr.TianMenShanStatInfo = statTable

		g_ScriptEvent:BroadcastInLua("OnRequestTianMenShanStatInfoFinished", dataType)
	end
end

function Gas2Gac.SendTianMenShanBattleCopyMemberInfo(dataUD, playerCount)
	local list = MsgPackImpl.unpack(dataUD)
	local n = list.Count
	if list and n % 5 == 0 and n / 5 == playerCount then
		local playersTable = {}

		for i = 0, n-1, 5 do
			local playerInfo = {}
			playerInfo.playerId = tonumber(list[i])
			playerInfo.playerName = tostring(list[i+1])
			playerInfo.playerClass = tonumber(list[i+2])
			playerInfo.playerGrade = tonumber(list[i+3])
			playerInfo.teamExists = tonumber(list[i+4]) > 0

			table.insert(playersTable, playerInfo)
		end

		table.sort(playersTable, function(a,b)
			if a.playerGrade == b.playerGrade then
				return a.playerId < b.playerId
			end
			return a.playerGrade > b.playerGrade
		end)

		CLuaCityWarMgr.TianMenShanPlayersInCopy = {}
		for i,v in ipairs(playersTable) do
			CLuaCityWarMgr.TianMenShanPlayersInCopy[i-1] = v
		end
		CLuaCityWarMgr.TianMenShanPlayersInCopy.Count = #playersTable

		g_ScriptEvent:BroadcastInLua("OnRequestTianMenShanSceneInfoFinished")
	end
end

function Gas2Gac.EnterSummonMirrorWarMineVehicle()
	g_ScriptEvent:BroadcastInLua("EnterSummonMirrorWarMineVehicle")
end

function Gas2Gac.SummonMirrorWarMineVehicleSuccess(posIdx)
	g_ScriptEvent:BroadcastInLua("SummonMirrorWarMineVehicleSuccess", posIdx)
end

function Gas2Gac.UpdateMirrorWarForceMineAndVehicleInfo(mineNum, leftVehicleNum, data_U)
	g_ScriptEvent:BroadcastInLua("UpdateMirrorWarForceMineAndVehicleInfo", mineNum, leftVehicleNum, data_U)
end

function Gas2Gac.OpenCreateOneLevelMajorCityGuide()
	CLuaGuideMgr.TryTriggerCityWarChallengeGuide()
end

function Gas2Gac.SendDeclareMirrorWarInfo(rivalGuildId, rivalGuildName, rivalMapId, rivalTemplateId, cost, guildSilver)
	local rivalRegionName = CityWar_Map.GetData(rivalMapId).Name
	local rivalCityName = CityWar_MapCity.GetData(rivalTemplateId).Name
	local msg = g_MessageMgr:FormatMessage("KFCZ_DECLEAR_CONFIRM_MSG", rivalRegionName, rivalCityName, rivalGuildName, cost)
	CLuaCommonConfirmWithInfoWndMgr:ShowWnd(msg, LocalString.GetString("消耗帮会资金"), cost, LocalString.GetString("拥有帮会资金"), guildSilver, function()
		Gac2Gas.DeclareMirrorWar(rivalGuildId)
		end)
end

--------------------------------------------------------------------- Rpc End


----- Lua Hook

function CLuaCityWarMgr:AddActionPopupMenuItem(cwUnit, items)
	local designData = CLuaCityWarMgr.UnitDesignData[cwUnit.TemplateId]
	if not designData then return end
	local actionList = designData.ActionList
	local usePlaceList = designData.UsePlace
	local unitId = cwUnit.ID
	local idx = 0
	for _, actIcon in ipairs(actionList) do
		local actionIdx = idx
		CommonDefs.ListAdd_LuaCall(items, HousePopupMenuItemData("", DelegateFactory.Action_int(function(_)
				if CLuaCityWarMgr.CurrentCityId and CLuaCityWarMgr.CurrentCityId ~= "" and CommonDefs.DictContains_LuaCall(CCityWarMgr.Inst.UnitList, unitId) then
					Gac2Gas.RequestUseCityUnit(CLuaCityWarMgr.CurrentCityId, unitId, actionIdx)
				end
			end), actIcon, true, true, -1))
		idx = idx + 1
	end
end

function CLuaCityWarMgr:CheckCityRights()
	local guildId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.GuildId or 0
	local curGuildId = 0
	if CLuaCityWarMgr.CurrentBasicInfo then
		curGuildId = CLuaCityWarMgr.CurrentBasicInfo.OwnerId
	end
	if not(guildId and curGuildId and guildId == curGuildId) then
		return false
	else
		return true
	end
end

CLuaCityWarMgr.CurrentUpgradeUnit = nil
function CLuaCityWarMgr:ShowUpgradeWnd(cwUnit)
	local unitDData = CityWar_Unit.GetData(cwUnit.TemplateId)
	if not unitDData then return end
	local typeDData = CityWar_UnitTypeName.GetData(unitDData.Type)
	if not typeDData then return end
	CLuaCityWarMgr.CurrentUpgradeUnit = cwUnit
	if typeDData.BatchType ~= 0 then
		CUIManager.ShowUI(CLuaUIResources.CityWarBatchUpgradeWnd)
	else
		CUIManager.ShowUI(CLuaUIResources.CityWarSingleUpgradeWnd)
	end
end

function CLuaCityWarMgr:GetTerritoryScene()
	if not CLuaCityWarMgr.TerritoryScene then
		CLuaCityWarMgr.TerritoryScene = CTerritoryScene:new()
	end
	return CLuaCityWarMgr.TerritoryScene
end

CCityWarUnit.m_hookGetPopupMenuItemDataFunc = function(cwUnit, items)
		local bIsRebornPoint = CCityWarMgr.Inst:IsRebornPointUnit(cwUnit)
		local unitId = cwUnit.ID
		local templateId = cwUnit.TemplateId
		local designData = CLuaCityWarMgr.UnitDesignData[templateId]
		local funcRequestPlace = function()
				local bCanMove = cwUnit:CheckMoveUnit()
				if bCanMove then
					if not CGuildMgr.Inst.CanCityBuild then
                        g_MessageMgr:ShowMessage("Guild_No_Power")
                        CCityWarMgr.Inst:ClearCurUnit()
                        return
                    end

					CCityWarMgr.Inst.CurUnit = nil
					cwUnit.Moving = false

					local yRot = cwUnit.RO.transform.eulerAngles.y
	                local x = cwUnit.RO.Position.x
	                local z = cwUnit.RO.Position.z

	                local xPos = math.floor(x)
	                local zPos = math.floor(z)
	                yRot = math.floor(yRot/90) * 90

					if CommonDefs.DictContains_LuaCall(CCityWarMgr.Inst.UnitList, cwUnit.ID) then
		                Gac2Gas.MoveCityUnit(CLuaCityWarMgr.CurrentCityId, cwUnit.TemplateId, cwUnit.ID, xPos, zPos, 90 - yRot)
		                CCityWarMgr.Inst.CurUnit = nil
						-- cwUnit.RO.Visible = false
						if CLuaCityWarMgr.OpenAuxiliaryDisplay then
							CLuaCityWarMgr.RecentPlacedUnitId = cwUnit.ID
						end
					else
						if cwUnit.TemplateId == CLuaCityWarMgr.RebornPointUnitTemplateId then
							Gac2Gas.BuildMirrorWarRebornPoint(xPos, zPos, 90 - yRot)
						else
							if CLuaCityWarMgr.OpenAuxiliaryDisplay then
								CLuaCityWarMgr.RecentPlacedUnitId = cwUnit.ID
							end
							Gac2Gas.BuildCityUnit(CLuaCityWarMgr.CurrentCityId, cwUnit.TemplateId, cwUnit.ID, xPos, zPos, 90 - yRot)
						end
						cwUnit:Destroy()
					end

				else
					g_MessageMgr:ShowMessage("CITY_UNIT_INVALID_PLACE");
					return
				end
			end

		local funcPlaceBack = function()
				cwUnit.Moving = false
				if not cwUnit:IsPlacedInServer() then
					cwUnit:Destroy()
				else
					cwUnit:OnCantPlaceExistedUnit()
				end
			end
		local funcPack = function()
				if bIsRebornPoint then
					if cwUnit.EngineId ~= 0 then
						Gac2Gas.DestroyMirrorWarRebornPoint(cwUnit.EngineId)
					end
				else
					Gac2Gas.PackUpCityUnit(CLuaCityWarMgr.CurrentCityId, templateId, unitId)
				end
			end


		local funcRequestRotate = function()
				local y = cwUnit.RO.transform.eulerAngles.y
	            local delta = 90
	            local newY = (y + delta) % 360

	            cwUnit.RO.transform.eulerAngles = Vector3(0, newY, 0);
			end
		local funcMsg = function()
				if not CLuaCityWarMgr:CheckCityRights() then
					g_MessageMgr:ShowMessage("CANNOT_USE_OTHER_CITY_UNIT")
					return
				end
				CItemInfoMgr.ShowCityWarUnitInfo(cwUnit.TemplateId, nil)
			end
		local funcUpgrade = function()
				if not CLuaCityWarMgr:CheckCityRights() then
					g_MessageMgr:ShowMessage("CANNOT_USE_OTHER_CITY_UNIT")
					return
				end

				CLuaCityWarMgr:ShowUpgradeWnd(cwUnit)
		end

		local funcRepair = function()
			if not CLuaCityWarMgr:CheckCityRights() then
				g_MessageMgr:ShowMessage("CANNOT_USE_OTHER_CITY_UNIT")
				return
			end
			Gac2Gas.RequestRepairCityUnitInfo(CLuaCityWarMgr.CurrentCityId, templateId, unitId)
		end

		local bIsInCopyScene = CCityWarMgr.Inst:IsInCityWarCopyScene()
		local bPlaceMode = CCityWarMgr.Inst.PlaceMode
		if bPlaceMode and not(bIsInCopyScene and not bIsRebornPoint) then
			if not(bIsRebornPoint and cwUnit:IsPlacedInServer()) then
				CommonDefs.ListAdd_LuaCall(items, HousePopupMenuItemData("", DelegateFactory.Action_int(function(idx)
						funcRequestPlace()
					end), "UI/Texture/Transparent/Material/housepopupmenu_loc.mat", true, true, -1))
				CommonDefs.ListAdd_LuaCall(items, HousePopupMenuItemData("", DelegateFactory.Action_int(function(idx)
						funcRequestRotate()
					end), "UI/Texture/Transparent/Material/housepopupmenu_xuanzhuan.mat", false, true, -1))

				CommonDefs.ListAdd_LuaCall(items, HousePopupMenuItemData("", DelegateFactory.Action_int(function(idx)
							funcPlaceBack()
						end), "UI/Texture/Transparent/Material/housepopupmenu_delete.mat", true, true, -1))
			end
		end

		if cwUnit:IsPlacedInServer() and not(bIsInCopyScene and not bIsRebornPoint) then
			if bPlaceMode then
				CommonDefs.ListAdd_LuaCall(items, HousePopupMenuItemData("", DelegateFactory.Action_int(function(idx)
						funcPack()
					end), "UI/Texture/Transparent/Material/housepopupmenu_shouhuicangku.mat", true, true, -1))
			end
			if not bIsRebornPoint then
				CommonDefs.ListAdd_LuaCall(items, HousePopupMenuItemData("", DelegateFactory.Action_int(function(idx)
						funcRepair()
					end), "UI/Texture/Transparent/Material/common_fight_weixiu.mat", true, true, -1))
				--local curLevel = templateId % 100
				--if curLevel < CLuaCityWarMgr.MaxLevel then
					CommonDefs.ListAdd_LuaCall(items, HousePopupMenuItemData("", DelegateFactory.Action_int(function(idx)
							funcUpgrade()
						end), "UI/Texture/Transparent/Material/common_fight_rank_up.mat", true, true, -1))
				--end
			end
			CommonDefs.ListAdd_LuaCall(items, HousePopupMenuItemData("", DelegateFactory.Action_int(function(idx)
						funcMsg()
					end), "UI/Texture/Transparent/Material/common_checkdetail.mat", true, true, -1))
		end

		if (CCityWarMgr.Inst:IsInCityWarCopyScene() or (CScene.MainScene and CScene.MainScene.SceneTemplateId == MonsterSiege_Setting.GetData().SceneTemplateId)) and (not bPlaceMode) then
			CommonDefs.ListClear(items)
		end

		if not bPlaceMode and not bIsRebornPoint then
			CLuaCityWarMgr:AddActionPopupMenuItem(cwUnit, items, bPlaceMode)
		end
	end

CCityWarUnit.s_CheckMoveUnitFunc = function(cwUnit)
		local id = cwUnit.ID
		local templateId = cwUnit.TemplateId
		local x = math.floor(cwUnit.RO.Position.x)
		local z = math.floor(cwUnit.RO.Position.z)
		local dir = math.floor(cwUnit.RO.transform.eulerAngles.y + 360 - 90) % 360
		local engineId = cwUnit.EngineId or 0

		local unitDesignData = CLuaCityWarMgr.UnitDesignData[cwUnit.TemplateId]

		local guildId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.GuildId or 0

		local function getUnitInfo(cityId, unitId)
				if CommonDefs.DictContains_LuaCall(CCityWarMgr.Inst.UnitList, unitId) then
					local unit = CommonDefs.DictGetValue_LuaCall(CCityWarMgr.Inst.UnitList, unitId)
					local info = {
							designData = CLuaCityWarMgr.UnitDesignData[unit.TemplateId],
							x = unit.ServerPos.x,
							y = unit.ServerPos.z,
							dir = (unit.ServerRotateY + 360)%360
						}
						return info
				end

			end
		local function getOriginalGridBarrierAndInfo(xx, yy)
			local barrier = CoreScene.MainCoreScene:GetOriginalBarrierv(xx, yy)
			local info = CoreScene.MainCoreScene:GetRoadInfo(xx, yy)

			return BarrierType2Int[barrier], info
		end
		local function isAnythingInGrid(xx, yy)
			local bAnything = false
			local objectid_list = CoreScene.MainCoreScene:GetGridAllObjectIdsWithFloor(xx, yy, 0)
			if not objectid_list or objectid_list.Count <= 0 then
				return bAnything
			end
			for i = 0, objectid_list.Count - 1 do
				local objectid = objectid_list[i]
				local obj = CClientObjectMgr.Inst:GetObject(objectid)
				if obj then
					if not (TypeIs(obj, typeof(CClientNpc)) and obj.TemplateId == CLuaCityWarMgr.CityGateNpcId) then
						if engineId ~= objectid then
							if not TypeIs(obj, typeof(CClientOtherPlayer)) then
								bAnything = true
								return true
							end
							-- 自己帮会其他玩家会挪走
							local player = TypeAs(obj, typeof(CClientOtherPlayer))
							if player == nil or player.BasicProp.GuildId ~= guildId then
								bAnything = true
								return true
							end
						end
					end
				end
			end
			return bAnything
		end
		local function getGridBarrierAndInfo(xx, yy)
			local barrier = CoreScene.MainCoreScene:GetBarrierv(xx, yy, true)
			local info = CoreScene.MainCoreScene:GetRoadInfo(xx, yy)

			return BarrierType2Int[barrier], info
		end


		local bCanput = true
		local bIsRebornPoint = CCityWarMgr.Inst:IsRebornPointUnit(cwUnit)
		if bIsRebornPoint then
			local mapId1, mapId2
			if #CLuaCityWarMgr.WarForceInfo >= 2 then
				mapId1, mapId2 = CLuaCityWarMgr.WarForceInfo[1].MapId, CLuaCityWarMgr.WarForceInfo[2].MapId
			end
			local leftMapId, rightMapId
			if mapId1 and mapId2 then
				if mapId1 == CityWar_Map.GetData(mapId2).WestMap or mapId1 == CityWar_Map.GetData(mapId2).NorthMap then
					leftMapId, rightMapId = mapId1, mapId2
				else
					leftMapId, rightMapId = mapId2, mapId1
				end
			end

			local leftSite = leftMapId and CityWar_Map.GetData(leftMapId).AttackSite
			local siteDesignData1 = leftSite and CLuaCityWarMgr.CityDesignData[leftSite]
			local region1 = siteDesignData1 and siteDesignData1.Region
			local left1, bottom1, right1, top1 = region1 and region1.lb.x or 0, region1 and region1.lb.y or 0, region1 and region1.rt.x or 0, region1 and region1.rt.y or 0

			local rightSite = rightMapId and CityWar_Map.GetData(rightMapId).DefendSite
			local siteDesignData2 = rightSite and CLuaCityWarMgr.CityDesignData[rightSite]
			local region2 = siteDesignData2 and siteDesignData2.Region
			local left2, bottom2, right2, top2 = region2 and region2.lb.x or 0, region2 and region2.lb.y or 0, region2 and region2.rt.x or 0, region2 and region2.rt.y or 0

			bCanput = CLuaCityWarMgr:GetTerritoryScene():CanBuildRebornPoint(left1, bottom1, right1, top1, left2, bottom2, right2, top2, x, z, dir, getGridBarrierAndInfo)
		else
			local cityTemplateId = CLuaCityWarMgr.CurrentBasicInfo and CLuaCityWarMgr.CurrentBasicInfo.TemplateId
			local cityDesignData = cityTemplateId and CLuaCityWarMgr.CityDesignData[cityTemplateId]
			local limitRegion = cityDesignData and cityDesignData.Region

			local left = limitRegion and limitRegion.lb.x or 0
			local bottom = limitRegion and limitRegion.lb.y or 0
			local right = limitRegion and limitRegion.rt.x or 0
			local top = limitRegion and limitRegion.rt.y or 0

			bCanput = CLuaCityWarMgr:GetTerritoryScene():CanMoveCityUnit(CLuaCityWarMgr.CurrentCityId, left, bottom, right, top,
				id, x, z, dir, unitDesignData, getUnitInfo, getOriginalGridBarrierAndInfo, isAnythingInGrid)
		end

		return bCanput
	end

CCityWarMgr.s_OnSceneDestroyFuc = function(context)
		CLuaCityWarMgr.CurrentGuildId = 0
		CLuaCityWarMgr.CurrentGuildName = ""
		CLuaCityWarMgr.CurrentCityId = ""
		CLuaCityWarMgr.CurrentMaterial = 0
		CLuaCityWarMgr.CurrentBasicInfo = nil
		CLuaCityWarMgr.CurrentUnitInfo = nil
		CLuaCityWarMgr.CurrentMiscInfo = nil

		CLuaCityWarMgr.TerritoryScene = nil

		CLuaCityWarMgr:StopPlaceMode()
		CCityWarMgr.Inst:ClearCurUnit()

		CLuaCityWarMgr:ClearCityData(true)
	end

CCityWarMgr.s_GetCurCityRegionFunc = function(context)

		local region = {}
		if CLuaCityWarMgr.CurrentBasicInfo then
			local cityTemplateId = CLuaCityWarMgr.CurrentBasicInfo.TemplateId
			local cityDesignData = CLuaCityWarMgr.CityDesignData[cityTemplateId]
			local limitRegion = cityDesignData.Region
			table.insert(region, limitRegion.lb.x)
			table.insert(region, limitRegion.lb.y)
			table.insert(region, limitRegion.rt.x)
			table.insert(region, limitRegion.rt.y)
		end

		return Table2Array(region, MakeArrayClass(int))
	end

function CLuaCityWarMgr:GetUnitBarrierList(templateId)
	return self.UnitDesignData[templateId] and self.UnitDesignData[templateId].Barrier or {}
end

CCityWarMgr.s_GetUnitBarrierList = function(context, templateId)
		local tbl = CLuaCityWarMgr:GetUnitBarrierList(templateId)
		return Table2Array(tbl, MakeArrayClass(int))
	end

CCityWarMgr.s_GetCurCityMaterialFunc = function(context)
		return CLuaCityWarMgr.CurrentMaterial or 0
	end

CCityWarMgr.s_GetCurCityIdFunc = function(context)
		return CLuaCityWarMgr.CurrentCityId or ""
	end

function Gas2Gac.QueryCityInfoForCityWarZhanLongResult(enemyGuildId, guildInfo_U)
	g_ScriptEvent:BroadcastInLua("QueryCityInfoForCityWarZhanLongResult", enemyGuildId, guildInfo_U)
end

function Gas2Gac.SetCityWarZhanLongEnemyGuildIdSuccess(enemyGuildId)
	g_ScriptEvent:BroadcastInLua("SetCityWarZhanLongEnemyGuildIdSuccess", enemyGuildId)
end

function Gas2Gac.OpenGrabMaterialWndForCityWarZhanLong(itemId, itemPlace, itemPos)
	CLuaCityWarMgr.ZhanLongGrabMaterialItemId = itemId
    CLuaCityWarMgr.ZhanLongGrabMaterialItemPlace = itemPlace
    CLuaCityWarMgr.ZhanLongGrabMaterialItemPos = itemPos
    CUIManager.ShowUI(CLuaUIResources.CityWarRobAssetWnd)
end

function Gas2Gac.ReplyCityWarGuildZiCaiInfo(zicai, limit)
	CLuaCityWarMgr.GuildMaterialAmount = zicai
	CLuaCityWarMgr.GuildMaterialLimit = limit
	CLuaCityWarMgr.IsFirstOpenSubmitWnd = not CUIManager.IsLoaded(CLuaUIResources.CityWarGuildMaterialSubmitWnd)
	CUIManager.ShowUI(CLuaUIResources.CityWarGuildMaterialSubmitWnd)
end

function Gas2Gac.ReplyActivatedAltarCount(count)
	g_ScriptEvent:BroadcastInLua("OnReplyActivatedAltarCount", count)
end

function Gas2Gac.SyncMonsterSiegePlayProgressInfo(fullHp, currentHp, killMonsterNum, totalMonsterNum, killBossNum)
	g_ScriptEvent:BroadcastInLua("OnUpdateMonsterSiegeInfo", fullHp, currentHp, killMonsterNum, totalMonsterNum, killBossNum)
end

function Gas2Gac.SendMonsterSiegeFightData(dataType, dataUD, playerCount, totalValue)
	-- print("===SendMonsterSiegeFightData=====", dataType, playerCount, totalValue)

	g_ScriptEvent:BroadcastInLua("SendMonsterSiegeFightData", dataType,dataUD, playerCount, totalValue)

end

function Gas2Gac.SendMonsterSiegeCopyMemberInfo(dataUD, playerCount)
	-- print("==SendMonsterSiegeCopyMemberInfo==", playerCount)
	g_ScriptEvent:BroadcastInLua("SendMonsterSiegeCopyMemberInfo", dataUD, playerCount)
end

----- Lua Hook End
