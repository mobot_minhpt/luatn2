
function class( ... )
	local NewClass = {}
	NewClass.Declared = {}
	NewClass.DeclaredCount = 0
  local arg = {...}
	NewClass.__base_list = {}
	NewClass.__sub_list = {}
	for i = 1, #arg do
		table.insert( NewClass.__base_list, arg[i] )
		table.insert(arg[i].__sub_list,NewClass)
	end

  -- build base map
  local function BuildBaseMap(map, class)
    map[class] = 1
    for k,v in pairs(class.__base_list) do
      BuildBaseMap(map, v)
    end
  end

  NewClass.__base_map = {}
  BuildBaseMap(NewClass.__base_map, NewClass)

	local __objmt_class = {}

	local __objmt_class_with_member = {}

	function __objmt_class_with_member.__index(tbl, key)
		local f = NewClass[key]
		if( is_class_member( f ) )then
			return f( tbl, nil )
		end
		return f
	end

	if CLASS_INDEX_CHECK then
		local function check_memeber(my_class, tbl, key)
			if my_class.Declared[key] or my_class[key] then
				return
			end
			if type(key) == "number" then
				return
			end
			if type(key) == "string" then
				if string.sub(key, 1, 1) == "_" then
					return
				end
				if string.sub(key, -8) == "_hObject" then
					return
				end
			end

			error( "ERROR INDEX AFTER Ctor():\t" .. tostring(tbl) .. "\tget " .. tostring(key) )
		end

		function __objmt_class.__index(tbl, key)
			check_memeber(NewClass, tbl, key)
			return NewClass[key]
		end
	else
		__objmt_class.__index = NewClass
	end

	if CLASS_NEWINDEX_CHECK then
		local function check_new_memeber(my_class, tbl, key, value)
			if key~="gameObject" and key~="transform" and key~="behavior" then
			if not my_class.Declared[key] and type(value) ~= "function" and type(key) ~= "number" then
				local CLogMgr=import "L10.CLogMgr"
				CLogMgr.LogWarning("Lua:RegistClassMember(" ..  GetClassName(my_class) .. ", \"" .. key	.. "\")")
				-- error("ERROR NEWINDEX AFTER Ctor():\t" .. tostring(tbl) .. "\tset " .. tostring(key) .."\tto " .. tostring(value) )
			end
			end
		end

		function __objmt_class.__newindex(tbl, key, value)
			check_new_memeber(NewClass, tbl, key, value)
			rawset(tbl, key, value)
		end

		function __objmt_class_with_member.__newindex(tbl, key, value)
			local f = NewClass and NewClass[key]
			if( is_class_member( f ) )then
				f( tbl, value )
				return
			end
			check_new_memeber(NewClass, tbl, key, value)
			rawset(tbl, key, value)
		end
	else
		function __objmt_class_with_member.__newindex(tbl, key, value)
			local f = NewClass and NewClass[key]
			if( is_class_member( f ) )then
				f( tbl, value )
				return
			end
			rawset(tbl, key, value)
		end
	end

	NewClass.__objmt_class = __objmt_class
	NewClass.__objmt_class_with_member = __objmt_class_with_member

	NewClass.objmt = __objmt_class

	NewClass.IsType = function( self, c )
		return ( self.__class == c )
	end

	NewClass.ctor = function( self, Instance , ...)
		for i,base_class in ipairs(self.__base_list) do
			if base_class.__engine_class then
				base_class.ctor( base_class, Instance)
			else
				base_class.ctor( base_class, Instance, ...)
			end
		end

		local _Ctor = rawget(self, "Ctor")
		if _Ctor then
			_Ctor(Instance, ...)
		end
	end

	NewClass.new_simple_wnd = function( self, scriptMgrInst, ... )
		local NewInstance = {}
--		local NewInstance = CreateTable_lua(0, self.DeclaredCount+1)
		NewInstance.__class = self
		setmetatable( NewInstance, __objmt_class )

		if NewClass.Components then
			if not scriptMgrInst then
				local log = import "L10.CLogMgr"
				log.LogError("calling RegistChildComponent not in simple_wnd_style")
			else
				for name, info in pairs(NewClass.Components) do
					NewInstance[name] = scriptMgrInst:GetWndComponent(info.Name, typeof(info.Type))
				end
			end
		end

		self.ctor( self, NewInstance , ...)
		return NewInstance
	end


	NewClass.new = function( self, ... )
		local NewInstance = {}
--		local NewInstance = CreateTable_lua(0, self.DeclaredCount+1)
		NewInstance.__class = self
		setmetatable( NewInstance, __objmt_class )
		self.ctor( self, NewInstance , ...)
		return NewInstance
	end

	__AllClasses = __AllClasses or {}
	table.insert( __AllClasses, NewClass )

	return NewClass
end

function IsClassObject( object, class )
  return type(object) == "table" and object.__class ~= nil and object.__class.__base_map[class] ~= nil
end

function CopyBaseKeys( my_class )
  my_class.SortedKeys = {}
  my_class.KeyIdMap = {}
  my_class.KeyTypeMap = {}
  for i = 1, #my_class.__base_list do
    local base_class = my_class.__base_list[i]
    if base_class.SortedKeys ~= nil then
      for k,v in pairs(base_class.SortedKeys) do
        if my_class.KeyIdMap[v] == nil then
          table.insert(my_class.SortedKeys, v)
          my_class.KeyIdMap[v] = #my_class.SortedKeys
          my_class.KeyTypeMap[v] = base_class.KeyTypeMap[v]
        end
      end
    end
  end
end

function RegistClassMember( my_class, member_name ,member_type)
   if my_class.SortedKeys == nil then
     CopyBaseKeys(my_class)
   end

   -- _开头的变量不包含在keyTable列表中
   if string.sub(member_name, 1, 1) ~= "_" then
     if my_class.KeyIdMap[member_name] == nil then
       table.insert(my_class.SortedKeys, member_name)
       my_class.KeyIdMap[member_name] = #my_class.SortedKeys
     end
	end

   my_class.Declared[member_name] = 1
   my_class.KeyTypeMap[member_name] = member_type
end

function InitWndInstance(wndClassName, scriptMgrInst)
	local wndClass = _G[wndClassName]
	if wndClass then
		return wndClass:new_simple_wnd(scriptMgrInst)
	else
		return nil
	end
end

function RegistChildComponent(my_class, name, extraName, componentType)

	RegistClassMember(my_class, name)
	my_class.Components = my_class.Components or {}
	if not componentType then
		componentType = extraName
		extraName = name
	end
	my_class.Components[name] = {Name = extraName, Type = componentType}
end

__mt_CPtr = {}
__mt_CPtr.GetValue = __getValueFromCPtr
__mt_CPtr.SetValue = __setValueOfCPtr
__mt_CPtr.__index = __mt_CPtr
function __mt_CPtr.__newindex( t, k, v )
	error( "attempt to index a CPtr table " )
end

__mt_ConstCPtr = {}
__mt_ConstCPtr.GetValue = __getValueFromCPtr
__mt_ConstCPtr.__index = __mt_ConstCPtr

function __mt_ConstCPtr.__newindex( t, k, v )
	error( "attempt to index a const CPtr table " )
end

__mt_readonly = {}
__mt_readonly.__index = __mt_readonly

function __mt_readonly.__newindex( t, k, v )
	error( "attempt to index a read only table " )
end

LocalString = import "LocalString"

function SafeStringFormat(...)
  local args = {...}
	local argc = select("#", ...)
  local function f()
    return string.format(unpack(args, 1, argc))
  end

  local function errHandler()
    return LocalString.GetString("消息文本格式错误:") .. tostring(args[1])
  end

  local statusCode, result = xpcall(f, errHandler)
  return result
end

OriginalSafeStringFormat = SafeStringFormat

function SafeStringFormat2(fmt0, ...)
    local args, order = {...}, {}
    local argc = select("#", ...)
    local orderCount = 0
    local formatError = nil

    if type(fmt0) ~= "string" then
        return LocalString.GetString("消息文本格式错误:") .. tostring(fmt0)
    end
    local fmt = fmt0:gsub('%%(%d+)%$', function(i)
        local index = tonumber(i)
        if index and index >= 1 and index <= argc then
            orderCount = orderCount + 1
            order[orderCount] = args[index]
            return '%'
        else
            formatError = LocalString.GetString("消息文本格式错误:") .. tostring(fmt0)
        end
    end)
    if formatError then
        return formatError
    end
    local function f()
        if orderCount == 0 then
            return string.format(fmt, unpack(args, 1, argc))
        else
            if orderCount > argc then
                return LocalString.GetString("消息文本格式错误:") .. tostring(fmt0)
            else
                return string.format(fmt, unpack(order, 1, orderCount))
            end
        end
    end
    local function errHandler()
        return LocalString.GetString("消息文本格式错误:") .. tostring(fmt0)
    end
    local statusCode, result = xpcall(f, errHandler)
    return result
end

CommonDefs = import "L10.Game.CommonDefs"
-- 海外版需要特殊的文本格式化函数
if CommonDefs.IS_KR_CLIENT or CommonDefs.IS_HMT_CLIENT or CommonDefs.IS_VN_CLIENT then
	SafeStringFormat = SafeStringFormat2
end
SafeStringFormat3 = SafeStringFormat

function ErrorHandler(err)
	local log = import "L10.CLogMgr"
	log.LogError(err)
end

function SAFE_CALL(func, ...)
	local args = {...}
	local argc = select("#", ...)
	local returns = { xpcall(function() return func(unpack(args, 1, argc)) end, ErrorHandler) }
	local Errcode = returns[1]
	if Errcode then
		--chenglong: 如果多个返回值中间有nil，这个地方可能会出现unpack不全的情况
		-- 需要重新计算返回值个数
		local count = 0
		for k,v in pairs(returns) do
			if k > count then count = k end
		end
		return unpack(returns, 2, count)
	else
		return
	end
end

function DebugPrint( n, ... )
	for k, v in ipairs( {...} ) do
		n = n[v]
	end
	if( type( n ) == "table" ) then
		print( n, "\n{" )
		for k, v in pairs( n ) do
			print( "", k, "=", v )
		end
		print( "}" )
	else
		print( n )
	end
end

function Reload(filename)
	local f = io.open(filename,"r")
	if not f then return end
	local class_update = {}
	local rpc_update = {}
	local action_reload
	for line in f:lines() do
		local class_name,func_name = line:match("^%s*function%s+([^.^:]+)[.:]([^(]+)")
		if class_name and func_name then
			if class_name == "ActionImp" then
				action_reload = true
			end
			local cls = rawget(_G,class_name)
			if cls and cls.__sub_list and next(cls.__sub_list) then
				local sub_all = class_update[class_name]
				if not sub_all then
					sub_all = {}
					class_update[class_name] = sub_all
					local tmp = {cls}
					local n = 1
					while tmp[n] do
						for _,v in ipairs(tmp[n].__sub_list) do
							if not sub_all[v] then
								table.insert(tmp,v)
							end
						end
						sub_all[tmp[n]] = {}
						n = n+1
					end
					sub_all[cls] = nil
				end
				local pre_func = cls[func_name]
				for sub_cls,tbl in pairs(sub_all) do
					if sub_cls[func_name] == pre_func then
						tbl[func_name] = 1
					end
				end
			end
			if RawRPC.RPCInfo[class_name] then
				if not rpc_update[class_name] then
					rpc_update[class_name] = {}
				end
				table.insert(rpc_update[class_name],func_name)
			end
		end
	end
	f:close()
	dofile(filename)
	for class_name,sub_all in pairs(class_update) do
		for sub_cls,tbl in pairs(sub_all) do
			for func_name,_ in pairs(tbl) do
				sub_cls[func_name] = _G[class_name][func_name]
			end
		end
	end
	for rpc,v in pairs(rpc_update) do
		for i,func_name in ipairs(v) do
			ResetRpc(rpc,func_name)
		end
	end
	--if action_reload and rawget(_G,"g_ActionMgr") then
	--	g_ActionMgr:LoadAction()
	--end
end

function ReloadClassFunction(script,class_name,func_name)
	if not class_name then
		class_name,func_name = script:match("^%s*function%s+([^.^:]+)[.:]([^(]+)")
	end
	if not class_name or not func_name then return end
	local sub_all = {}
	local cls = rawget(_G,class_name)
	if cls and cls.__sub_list and next(cls.__sub_list) then
		local tmp = {cls}
		local n = 1
		local pre_func = cls[func_name]
		while tmp[n] do
			for _,v in ipairs(tmp[n].__sub_list) do
				if sub_all[v] == nil then
					table.insert(tmp,v)
				end
			end
			sub_all[tmp[n]] = (tmp[n][func_name] == pre_func)
			n = n+1
		end
		sub_all[cls] = nil
		local ErrHandler = function(err)
			original_print("error:",err)
		end
		local ok = xpcall(function() loadstring(script)() end, ErrHandler)
		if ok then
			for sub_cls,need_patch in pairs(sub_all) do
				if need_patch then
					sub_cls[func_name] = cls[func_name]
					original_print(GetClassName(sub_cls),"yes")
				else
					original_print(GetClassName(sub_cls),"no")
				end
			end
			original_print("success!")
		end
	end
end

function ReloadFileRequired(filename)
	if not _loadfile[filename:gsub("/","\\")] then return end
	Reload(filename)
	RegisterTickWithDuration(original_print,1,1,filename)
end

function PatchLangDict(key, translationItem, skipErr)
	if skipErr == nil then
		skipErr = false
	end
	local languageVerifyTbl = {}
	for i = 1, LocalString.languageCount-1 do
		languageVerifyTbl[LocalString.languages[i]] = true
	end
	
	local val = ""
	for k, v in pairs(translationItem) do
		if languageVerifyTbl[k] then
			languageVerifyTbl[k] = false
		end
		if k == LocalString.language then
			val = v
		end
	end

	local passVerifyCheck = true
	if skipErr then
		for i = 1, LocalString.languageCount-1 do
			if languageVerifyTbl[LocalString.languages[i]] then
				passVerifyCheck = false
				error("PatchLangDict: " .. key .. " has not been translated in " .. LocalString.languages[i] .. " language!")
				break
			end
		end
	end

	if passVerifyCheck then
		LocalString.PatchLangDict(key, val)
	end
end

function RmLangDict(key)
	LocalString.PatchLangDict(key, key)
end

function GetOriginalByTranslation(key) 
	for k, v in pairs(LocalString.translation) do
		if v == key then
			return k
		end
	end
	
	return nil
end