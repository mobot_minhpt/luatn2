local UIRoot=import "UIRoot"

local UITable = import "UITable"
local UIScrollView = import "UIScrollView"
local Extensions = import "Extensions"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local CTipTitleItem = import "CTipTitleItem"
local Screen = import "UnityEngine.Screen"
local NGUIMath = import "NGUIMath"
local CBaseWnd = import "L10.UI.CBaseWnd"
local UIRect = import "UIRect"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local CButton = import "L10.UI.CButton"

LuaMessageTipWithButtonWnd = class()

RegistClassMember(LuaMessageTipWithButtonWnd, "m_Background")
RegistClassMember(LuaMessageTipWithButtonWnd, "m_TitleTempalte")
RegistClassMember(LuaMessageTipWithButtonWnd, "m_ParagraphTemplate")
RegistClassMember(LuaMessageTipWithButtonWnd, "m_PlainTextTemplate")
RegistClassMember(LuaMessageTipWithButtonWnd, "m_Table")
RegistClassMember(LuaMessageTipWithButtonWnd, "m_ScrollView")
RegistClassMember(LuaMessageTipWithButtonWnd, "m_ScrollViewIndicator")
RegistClassMember(LuaMessageTipWithButtonWnd, "m_Button")

RegistClassMember(LuaMessageTipWithButtonWnd, "m_MinHeight")


function LuaMessageTipWithButtonWnd:Init()
	
    self.m_Background = self.transform:Find("Anchor/Background"):GetComponent(typeof(UIWidget))
    self.m_TitleTempalte = self.transform:Find("Anchor/Background/Templates/TitleTemplate").gameObject
    self.m_ParagraphTemplate = self.transform:Find("Anchor/Background/Templates/ParagraphTemplate").gameObject
    self.m_PlainTextTemplate = self.transform:Find("Anchor/Background/Templates/PlainTextTemplate").gameObject
    self.m_Table = self.transform:Find("Anchor/Background/ContentScrollView/Table"):GetComponent(typeof(UITable))
    self.m_ScrollView = self.transform:Find("Anchor/Background/ContentScrollView"):GetComponent(typeof(UIScrollView))
    self.m_ScrollViewIndicator = self.transform:Find("Anchor/Background/ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator))
    self.m_Button = self.transform:Find("Anchor/Button"):GetComponent(typeof(CButton))
    self.m_TitleTempalte:SetActive(false)
    self.m_ParagraphTemplate:SetActive(false)
    self.m_PlainTextTemplate:SetActive(false)
    if LuaMessageTipMgr.m_ButtonText~=nil and LuaMessageTipMgr.m_Callback~=nil then
        self.m_Button.gameObject:SetActive(true)
        self.m_Button.Text = LuaMessageTipMgr.m_ButtonText
    else
        self.m_Button.gameObject:SetActive(false)
    end
    
    CommonDefs.AddOnClickListener(self.m_Button.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnButtonClick()
    end), false)


    self.m_MinHeight = 200

    Extensions.RemoveAllChildren(self.m_Table.transform)
    local isOrdinaryText = false
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(LuaMessageTipMgr.m_Message)
    if info==nil then isOrdinaryText = true end

    if isOrdinaryText then
        local go = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_PlainTextTemplate)
        go:SetActive(true)
        go:GetComponent(typeof(UILabel)).text = LuaMessageTipMgr.m_Message
    else

        if info.titleVisible then
            local titleGo = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_TitleTempalte)
            titleGo:SetActive(true)
            titleGo:GetComponent(typeof(CTipTitleItem)):Init(info.title)
        end
        do
            local i = 0
            while i < info.paragraphs.Count do
                local paragraphGo = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_ParagraphTemplate)
                paragraphGo:SetActive(true)
                paragraphGo:GetComponent(typeof(CTipParagraphItem)):Init(info.paragraphs[i], 0xffffffff)
                i = i + 1
            end
        end
    end
    
    self.m_ScrollView:ResetPosition()
    self.m_Table:Reposition()

    self:LayoutWnd()

end

function LuaMessageTipWithButtonWnd:Close()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaMessageTipWithButtonWnd:OnButtonClick()
    if LuaMessageTipMgr.m_Callback then
        LuaMessageTipMgr.m_Callback()
        LuaMessageTipMgr.m_Callback = nil
    end
    self:Close()
end

function LuaMessageTipWithButtonWnd:LayoutWnd()
    if LuaMessageTipMgr.m_ButtonText~=nil and LuaMessageTipMgr.m_Callback~=nil then
        self.m_ScrollView.panel.bottomAnchor.absolute = 120
    else
        self.m_ScrollView.panel.bottomAnchor.absolute = 30
    end

    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenWidth = Screen.width * scale * CUIManager.UIMainCamera.rect.width
    local virtualScreenHeight = Screen.height * scale

    local contentTopPadding = math.abs(self.m_ScrollView.panel.topAnchor.absolute)
    local contentBottomPadding = math.abs(self.m_ScrollView.panel.bottomAnchor.absolute)

    --计算高度
    local totalWndHeight = self:TotalHeightOfScrollViewContent() + contentTopPadding + contentBottomPadding + self.m_ScrollView.panel.clipSoftness.y * 2 + 1
    --加1避免由于精度误差导致scrollview刚好显示全table内容时可以滚动的问题
    local displayWndHeight = math.min(math.max(totalWndHeight, self.m_MinHeight), virtualScreenHeight)

    --设置背景高度
    self.m_Background.height = math.ceil(displayWndHeight)

    local rects = CommonDefs.GetComponentsInChildren_Component_Type_Boolean(self.gameObject:GetComponent(typeof(CBaseWnd)), typeof(UIRect), true)
    do
        local i = 0
        while i < rects.Length do
            if rects[i].updateAnchors == UIRect.AnchorUpdate.OnEnable or rects[i].updateAnchors == UIRect.AnchorUpdate.OnStart then
                rects[i]:ResetAndUpdateAnchors()
            end
            i = i + 1
        end
    end
    self.m_ScrollView:ResetPosition()
    self.m_ScrollViewIndicator:Layout()
    self.m_Background.transform.localPosition = Vector3.zero
end

function LuaMessageTipWithButtonWnd:TotalHeightOfScrollViewContent()
    return NGUIMath.CalculateRelativeWidgetBounds(self.m_Table.transform).size.y + self.m_Table.padding.y * 2
end

--------------------------------------------------------------------------------------

LuaMessageTipMgr = class()

LuaMessageTipMgr.m_Message = nil
LuaMessageTipMgr.m_ButtonText = nil
LuaMessageTipMgr.m_Callback = nil


function LuaMessageTipMgr:ShowMessage(message, buttonText, callback)
    self.m_Message = message
    self.m_ButtonText = buttonText
    self.m_Callback = callback
    CUIManager.ShowUI("MessageTipWithButtonWnd")
end

LuaMessageTipMgr.m_KvTable = nil
LuaMessageTipMgr.m_Title = nil
LuaMessageTipMgr.m_WndPosX = nil
function LuaMessageTipMgr:ShowMessageWithTable(kvTable, title, buttonText, callback, wndPosX)
    LuaMessageTipMgr.m_KvTable = kvTable
    LuaMessageTipMgr.m_Title = title
    LuaMessageTipMgr.m_ButtonText = buttonText
    LuaMessageTipMgr.m_Callback = callback
    LuaMessageTipMgr.m_WndPosX = wndPosX
    CUIManager.ShowUI(CLuaUIResources.SkillLevelDetailInfoWnd)
end
