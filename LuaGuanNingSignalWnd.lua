local UIGrid=import "UIGrid"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local Object=import "System.Object"
local QnSelectableButton=import "L10.UI.QnSelectableButton"
local UISprite = import "UISprite"
local Vector4 = import "UnityEngine.Vector4"

CLuaGuanNingSignalWnd=class()
RegistClassMember(CLuaGuanNingSignalWnd,"m_ItemTemplate")
RegistClassMember(CLuaGuanNingSignalWnd,"m_Grid")
RegistClassMember(CLuaGuanNingSignalWnd, "m_SignalButton")
RegistClassMember(CLuaGuanNingSignalWnd, "m_BulletChatButton")
RegistClassMember(CLuaGuanNingSignalWnd, "m_inited")

function CLuaGuanNingSignalWnd:Init()
    self.m_inited = false

    local signalState = CLuaPlayerSettings.Get("GuanNingSignalState", true)
    local bulletChatState = CLuaPlayerSettings.Get("GuanNingBulletChatState", true)

    self.m_SignalButton = self.transform:Find("Anchor/Background/SignalButton"):GetComponent(typeof(QnSelectableButton))
    self.m_BulletChatButton = self.transform:Find("Anchor/Background/BulletChatButton"):GetComponent(typeof(QnSelectableButton))
    
    self.m_SignalButton.OnButtonSelected = DelegateFactory.Action_bool(function(b)
        CLuaPlayerSettings.Set("GuanNingSignalState", b)
        if b then
            self.m_SignalButton.transform:GetComponent(typeof(UISprite)).spriteName = "common_thumb_open"
            if self.m_inited then g_MessageMgr:ShowMessage("GuanNing_Open_Signal") end
        else
            self.m_SignalButton.transform:GetComponent(typeof(UISprite)).spriteName = "common_thumb_close"
            if self.m_inited then g_MessageMgr:ShowMessage("GuanNing_Close_Signal") end
        end
        g_ScriptEvent:BroadcastInLua("GuanNingSignalStateChanged", b)
    end)

    self.m_BulletChatButton.OnButtonSelected = DelegateFactory.Action_bool(function(b)
        CLuaPlayerSettings.Set("GuanNingBulletChatState", b)
        if b then
            self.m_BulletChatButton.transform:GetComponent(typeof(UISprite)).spriteName = "common_thumb_open"
            if self.m_inited then g_MessageMgr:ShowMessage("GuanNing_Open_BulletChat") end
        else
            self.m_BulletChatButton.transform:GetComponent(typeof(UISprite)).spriteName = "common_thumb_close"
            if self.m_inited then g_MessageMgr:ShowMessage("GuanNing_Close_BulletChat") end
        end
        g_ScriptEvent:BroadcastInLua("GuanNingBulletChatChanged", b)
    end)

    self.m_SignalButton:SetSelected(signalState)
    self.m_BulletChatButton:SetSelected(bulletChatState)

    local worldPos=CLuaGuanNingMgr.m_ClickSignalWorldPos
    local localPos=self.transform:InverseTransformPoint(worldPos)
    LuaUtils.SetLocalPosition(FindChild(self.transform,"Anchor"),localPos.x-300+20,localPos.y-200,0)
    
    self.m_ItemTemplate=FindChild(self.transform,"ItemTemplate").gameObject
    self.m_ItemTemplate:SetActive(false)

    self.m_Grid=FindChild(self.transform,"Grid").gameObject

    local isCmd = CLuaGuanNingMgr:IsCommander()
    GuanNing_QuickCommand.Foreach(function(k,v)
        for type in string.gmatch(v.Type, '%d+') do 
            if type == '1' and not isCmd or type == '2' and isCmd then
                if v.Camp and v.Camp ~= 0 and v.Camp ~= CLuaGuanNingMgr:GetMyForce() + 1 then return end
                local item = NGUITools.AddChild(self.m_Grid, self.m_ItemTemplate)
                item:SetActive(true)
                FindChild(item.transform,"Content"):GetComponent(typeof(UILabel)).text=v.Content
                
                UIEventListener.Get(item).onClick=LuaUtils.VoidDelegate(function(go)
                    self:OnClick(k, go)
                end)
                break
            end
        end
    end)

    self.m_Grid:GetComponent(typeof(UIGrid)):Reposition()
    self.transform:Find("Anchor/Background"):GetComponent(typeof(UISprite)).height = math.min(805, NGUIMath.CalculateRelativeWidgetBounds(self.m_Grid.transform).size.y + 85)

    self.m_inited = true
end
function CLuaGuanNingSignalWnd:OnClick(id, go)
    local cdtime=6
    if CServerTimeMgr.Inst.timeStamp - CLuaGuanNingMgr.m_LastSendSignalTime < cdtime then
        local delta = cdtime - math.floor(CServerTimeMgr.Inst.timeStamp - CLuaGuanNingMgr.m_LastSendSignalTime)
        g_MessageMgr:ShowMessage("PVPCommand_CDTime",delta)
        CUIManager.CloseUI(CLuaUIResources.GuanNingSignalWnd)
        return
    end
    CLuaGuanNingMgr.m_LastSendSignalTime=CServerTimeMgr.Inst.timeStamp

    CUIManager.CloseUI(CLuaUIResources.GuanNingSignalWnd)
    Gac2Gas.SendPVPCommand(id)
end

function CLuaGuanNingSignalWnd:GetGuideGo(methodName)
    if methodName == "GetDanmuBtn" then
        return self.m_BulletChatButton.gameObject
	end
    return nil
end

return CLuaGuanNingSignalWnd