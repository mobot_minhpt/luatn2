-- Auto Generated!!
local CChuanjiabaoWordType = import "L10.Game.CChuanjiabaoWordType"
local CCommonItem = import "L10.Game.CCommonItem"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CQianKunDaiCompoundView = import "L10.UI.CQianKunDaiCompoundView"
local CQianKunDaiInputItem = import "L10.UI.CQianKunDaiInputItem"
local CQianKunDaiInputItem_ItemBag = import "L10.UI.CQianKunDaiInputItem_ItemBag"
local CQiangKunDaiInputItem_FishBag = import "L10.UI.CQiangKunDaiInputItem_FishBag"
local CQianKunDaiInputView = import "L10.UI.CQianKunDaiInputView"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local EnumQianKunDaiOperation = import "L10.Game.EnumQianKunDaiOperation"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
--local Gac2Gas = import "L10.Game.Gac2Gas"
local Item_Item = import "L10.Game.Item_Item"
local ItemInBagItemInfo = import "L10.Game.ItemInBagItemInfo"
local LocalString = import "LocalString"
local PlayerSettings = import "L10.Game.PlayerSettings"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local QianKunDai_Formula = import "L10.Game.QianKunDai_Formula"
local Quaternion = import "UnityEngine.Quaternion"
local String = import "System.String"
local StringActionKeyValuePair = import "L10.Game.StringActionKeyValuePair"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local QianKunDai_Setting = import "L10.Game.QianKunDai_Setting"
local CItemAccessTipMgr = import "L10.UI.CItemAccessTipMgr"
local CTooltip = import "L10.UI.CTooltip"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CQianKunDaiMgr = import "L10.Game.CQianKunDaiMgr"
local QianKunDai_Item = import "L10.Game.QianKunDai_Item"
local ShowType = import "L10.UI.CItemInfoMgr+ShowType"
local LinkItemInfo = import "L10.UI.CItemInfoMgr+LinkItemInfo"

CQianKunDaiInputView.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.autoInputBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.OnAutoInputClicked, VoidDelegate, this)
    UIEventListener.Get(this.selectFormulaBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.OnSelectFormulaBtnClicked, VoidDelegate, this)
end
CQianKunDaiInputView.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.QianKunDaiTakeOutItem, MakeDelegateFromCSFunction(this.OnTakeOut, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.QianKunDaiUpdateFormula, MakeDelegateFromCSFunction(this.UpdateSelectedFormula, MakeGenericClass(Action1, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnUpdateItemInfo, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.QianKunDaiSyncSelectItems, MakeDelegateFromCSFunction(this.SyncSelectedItems, MakeGenericClass(Action1, MakeGenericClass(List, String)), this))
    EventManager.AddListener(EnumEventType.QianKunDaiUpdateMaxId, MakeDelegateFromCSFunction(this.UpdateAlert, Action0, this))
    LuaQianKunDaiInputWnd:OnEnable(this)
end
CQianKunDaiInputView.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.QianKunDaiTakeOutItem, MakeDelegateFromCSFunction(this.OnTakeOut, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.QianKunDaiUpdateFormula, MakeDelegateFromCSFunction(this.UpdateSelectedFormula, MakeGenericClass(Action1, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnUpdateItemInfo, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.QianKunDaiSyncSelectItems, MakeDelegateFromCSFunction(this.SyncSelectedItems, MakeGenericClass(Action1, MakeGenericClass(List, String)), this))
    EventManager.RemoveListener(EnumEventType.QianKunDaiUpdateMaxId, MakeDelegateFromCSFunction(this.UpdateAlert, Action0, this))
    LuaQianKunDaiInputWnd:OnDisable()
end
CQianKunDaiInputView.m_InitQianKunDaiInput_CS2LuaHook = function (this, recipeId) 

    this.RecipeId = recipeId
    this.Formula = CQianKunDaiMgr.Inst:GetQianKunDaiFormula(recipeId)

    local qiankundai = QianKunDai_Formula.GetData(recipeId)
    if qiankundai ~= nil then
        this.selectFormulaBtn.Text = qiankundai.Type
    end

    this.itemCellTemplate:SetActive(false)
    this.itemCellTemplate_BagItem:SetActive(false)
    this.itemCellTemplate_FishItem:SetActive(false)
    this:UpdateAlert()
    this:ClearAllSelection()
    this:ShowSelectableItem()
end
CQianKunDaiInputView.m_NeedAlertMoreFormula_CS2LuaHook = function (this) 
    local maxId = 0
    QianKunDai_Formula.ForeachKey(DelegateFactory.Action_object(function (key) 
        if key > maxId then
            maxId = key
        end
    end))
    return maxId > PlayerSettings.QianKunDaiRecipeMaxId
end
CQianKunDaiInputView.m_UpdateAlert_CS2LuaHook = function (this) 
    this.moreFormulaAlert:SetActive(this:NeedAlertMoreFormula())
end
CQianKunDaiInputView.m_ClearAllSelection_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.ItemSelected)
end
CQianKunDaiInputView.m_GetSelectionCount_CS2LuaHook = function (this) 
    local total = 0
    if this.ItemSelected ~= nil then
        total = total + this.ItemSelected.Count
    end
    return total
end
CQianKunDaiInputView.m_ShowSelectableItem_CS2LuaHook = function (this)
    if not CClientMainPlayer.Inst then
        return
    end
    Extensions.RemoveAllChildren(this.grid.transform)
    CommonDefs.DictClear(this.itemDict)
    local itemProp = CClientMainPlayer.Inst.ItemProp
    local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)

    do
        local i = 1
        while i <= bagSize do
            local continue
            repeat
                local id = itemProp:GetItemAt(EnumItemPlace.Bag, i)
                local item = CItemMgr.Inst:GetById(id)

                if CommonDefs.ListContains(this.ItemSelected, typeof(String), id) then
                    continue = true
                    break
                end

                if not this.Formula:IsNeedItem(item) then
                    continue = true
                    break
                end

                if item ~= nil then
                    local instance = CommonDefs.Object_Instantiate(this.itemCellTemplate)
                    instance.transform.parent = this.grid.transform
                    instance.transform.localPosition = Vector3.zero
                    instance.transform.localRotation = Quaternion.identity
                    instance.transform.localScale = Vector3.one
                    instance:SetActive(true)
                    local cell = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CQianKunDaiInputItem))
                    cell:Init(id)
                    cell.OnItemClickDelegate = MakeDelegateFromCSFunction(this.OnItemClick, MakeGenericClass(Action1, CQianKunDaiInputItem), this)
                    cell.OnItemDoubleClickDelegate = MakeDelegateFromCSFunction(this.OnItemDoubleClick, MakeGenericClass(Action1, CQianKunDaiInputItem), this)
                    CommonDefs.DictAdd(this.itemDict, typeof(String), id, typeof(CQianKunDaiInputItem), cell)
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end

    -- 添加小书箱的物品
    if this.RecipeId == CQianKunDaiMgr.Inst.JiNengShuFormulaId then
        do
            local i = 1
            while i <= bagSize do
                local continue
                repeat
                    local id = itemProp:GetItemAt(EnumItemPlace.Bag, i)
                    local item = CItemMgr.Inst:GetById(id)
                    if item ~= nil and item.IsItem and item.Item:IsXiaoShuXiang() then
                        -- 小书箱里所有物品
                        if not CommonDefs.DictContains(this.bagItemDict, typeof(UInt32), i) then
                            local bagItems = CreateFromClass(MakeGenericClass(List, CQianKunDaiInputItem_ItemBag))
                            CommonDefs.DictAdd(this.bagItemDict, typeof(UInt32), i, typeof(MakeGenericClass(List, CQianKunDaiInputItem_ItemBag)), bagItems)
                        end

                        local allItems = item.Item:GetAllItemsInItem()
                        do
                            local j = 0
                            while j < allItems.Count do
                                local continue
                                repeat
                                    -- 检查是否被选中的小书箱物品
                                    local bagItemInfo = (function () 
                                        local __localValue = CreateFromClass(ItemInBagItemInfo)
                                        __localValue.BagItemPos = i
                                        __localValue.PosInBagItem = j
                                        __localValue.TemplateId = allItems[j].itemTemplateId
                                        __localValue.Count = allItems[j].itemCount
                                        __localValue.IsBind = allItems[j].isBind
                                        return __localValue
                                    end)()

                                    if CommonDefs.ListContains(this.ItemSelected, typeof(String), bagItemInfo:GetItemBagInfoString()) then
                                        continue = true
                                        break
                                    end

                                    local instance = CommonDefs.Object_Instantiate(this.itemCellTemplate_BagItem)
                                    instance.transform.parent = this.grid.transform
                                    instance.transform.localPosition = Vector3.zero
                                    instance.transform.localRotation = Quaternion.identity
                                    instance.transform.localScale = Vector3.one
                                    instance:SetActive(true)
                                    local bagItem = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CQianKunDaiInputItem_ItemBag))

                                    bagItem:Init(bagItemInfo)
                                    bagItem.OnItemClickDelegate = MakeDelegateFromCSFunction(this.OnItemClick_BagItem, MakeGenericClass(Action1, CQianKunDaiInputItem_ItemBag), this)
                                    bagItem.OnItemDoubleClickDelegate = MakeDelegateFromCSFunction(this.OnItemDoubleClick_BagItem, MakeGenericClass(Action1, CQianKunDaiInputItem_ItemBag), this)
                                    CommonDefs.ListAdd(CommonDefs.DictGetValue(this.bagItemDict, typeof(UInt32), i), typeof(CQianKunDaiInputItem_ItemBag), bagItem)
                                    continue = true
                                until 1
                                if not continue then
                                    break
                                end
                                j = j + 1
                            end
                        end
                    end
                    continue = true
                until 1
                if not continue then
                    break
                end
                i = i + 1
            end
        end
    end

    -- 添加纹饰囊的物品
    if this.RecipeId == CQianKunDaiMgr.Inst.WenShiFormulaId then
        do
            local i = 1
            while i <= bagSize do
                local continue
                repeat
                    local id = itemProp:GetItemAt(EnumItemPlace.Bag, i)
                    local item = CItemMgr.Inst:GetById(id)
                    if item ~= nil and item.IsItem and item.Item:IsWenShiNang() then
                        if not CommonDefs.DictContains(this.bagItemDict, typeof(UInt32), i) then
                            local bagItems = CreateFromClass(MakeGenericClass(List, CQianKunDaiInputItem_ItemBag))
                            CommonDefs.DictAdd(this.bagItemDict, typeof(UInt32), i, typeof(MakeGenericClass(List, CQianKunDaiInputItem_ItemBag)), bagItems)
                        end

                        local allItems = item.Item:GetAllItemsInItem()
                        do
                            local j = 0
                            while j < allItems.Count do
                                local continue
                                repeat
                                    local bagItemInfo = (function () 
                                        local __localValue = CreateFromClass(ItemInBagItemInfo)
                                        __localValue.BagItemPos = i
                                        __localValue.PosInBagItem = j
                                        __localValue.TemplateId = allItems[j].itemTemplateId
                                        __localValue.Count = allItems[j].itemCount
                                        __localValue.IsBind = allItems[j].isBind
                                        return __localValue
                                    end)()

                                    if CommonDefs.ListContains(this.ItemSelected, typeof(String), bagItemInfo:GetItemBagInfoString()) then
                                        continue = true
                                        break
                                    end

                                    local instance = CommonDefs.Object_Instantiate(this.itemCellTemplate_BagItem)
                                    instance.transform.parent = this.grid.transform
                                    instance.transform.localPosition = Vector3.zero
                                    instance.transform.localRotation = Quaternion.identity
                                    instance.transform.localScale = Vector3.one
                                    instance:SetActive(true)
                                    local bagItem = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CQianKunDaiInputItem_ItemBag))

                                    bagItem:Init(bagItemInfo)
                                    bagItem.OnItemClickDelegate = MakeDelegateFromCSFunction(this.OnItemClick_BagItem, MakeGenericClass(Action1, CQianKunDaiInputItem_ItemBag), this)
                                    bagItem.OnItemDoubleClickDelegate = MakeDelegateFromCSFunction(this.OnItemDoubleClick_BagItem, MakeGenericClass(Action1, CQianKunDaiInputItem_ItemBag), this)
                                    CommonDefs.ListAdd(CommonDefs.DictGetValue(this.bagItemDict, typeof(UInt32), i), typeof(CQianKunDaiInputItem_ItemBag), bagItem)
                                    continue = true
                                until 1
                                if not continue then
                                    break
                                end
                                j = j + 1
                            end
                        end
                    end
                    continue = true
                until 1
                if not continue then
                    break
                end
                i = i + 1
            end
        end
    end
	
    -- 添加合成灵力的提示物品
    if this.RecipeId == CQianKunDaiMgr.Inst.JiFenFormulaId then
        -- 添加鱼篓里的观赏鱼
        CommonDefs.DictClear(this.fishItemDict)
        local fishbasket = CClientMainPlayer.Inst.ItemProp.FishBasket
        if fishbasket then 
            local itemIdArray = fishbasket.Fishs
            local countArray = fishbasket.Count
            for i=1,itemIdArray.Length-1,1 do
                local t = {}
                t.itemId = itemIdArray[i]
                t.count = countArray[i]
                t.fishbagpos = i
                t.bagType = LuaZhenZhaiYuMgr.BagType.Fish

                local data = HouseFish_AllFishes.GetData(t.itemId)
                --鱼
                if QianKunDai_Item.GetData(t.itemId) and data and t.count>0 then
                    local instance = CommonDefs.Object_Instantiate(this.itemCellTemplate_FishItem)
                    instance.transform.parent = this.grid.transform
                    instance.transform.localPosition = Vector3.zero
                    instance.transform.localRotation = Quaternion.identity
                    instance.transform.localScale = Vector3.one
                    instance:SetActive(true)
                    local cell = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CQiangKunDaiInputItem_FishBag))
                    cell:Init(t.itemId,t.count,t.fishbagpos)
                    cell.OnItemClickDelegate = DelegateFactory.Action(function () 
                        this.SelectedFishId = t.itemId
                        this.SelectedFishBagPos = i
                        
                        CItemInfoMgr.showType = ShowType.QianKunDai
                        CItemInfoMgr.linkItemInfo = LinkItemInfo(t.itemId, false, this)
                        CItemInfoMgr.linkItemInfo.alignType = CItemInfoMgr.AlignType.ScreenLeft
                        CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)

                    end)
                    cell.OnItemDoubleClickDelegate = DelegateFactory.Action(function ()
                        this.SelectedFishId = t.itemId
                        this.SelectedFishBagPos = i
                        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("QianKunDai_CantRevert_PutFishInFishBag"), 
                        DelegateFactory.Action(function ()
                            Gac2Gas.MoveFishBasketToBag(EnumFishBasketType.Fish,this.SelectedFishBagPos,1) 
                        end),
                        nil,
                        LocalString.GetString("确定"), LocalString.GetString("取消"), false)
                    end)

                    CommonDefs.DictAdd(this.fishItemDict, typeof(UInt32), i, typeof(CQiangKunDaiInputItem_FishBag), cell)
                    
                end
            end
        end

        local setting = QianKunDai_Setting.GetData()
        local instance = CommonDefs.Object_Instantiate(this.itemCellTemplate)
        instance.transform.parent = this.grid.transform
        instance.transform.localPosition = Vector3.zero
        instance.transform.localRotation = Quaternion.identity
        instance.transform.localScale = Vector3.one
        instance:SetActive(true)
        local cell = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CQianKunDaiInputItem))
        cell:InitByTid(setting.JiFenHintItemId)
        cell.OnItemClickDelegate = DelegateFactory.Action_CQianKunDaiInputItem(function (go) 
            CItemAccessTipMgr.Inst:ShowAccessTip(nil, setting.JiFenHintItemId, false, go.transform, CTooltip.AlignType.Top)
        end)
        cell.OnItemDoubleClickDelegate = nil
        CommonDefs.DictAdd(this.itemDict, typeof(String), tostring(setting.JiFenHintItemId), typeof(CQianKunDaiInputItem), cell)
    end

    this.grid:Reposition()
    this.scrollView:ResetPosition()

    return
end
CQianKunDaiInputView.m_GetInputTotalCount_CS2LuaHook = function (this) 
    if this.RecipeId == CQianKunDaiMgr.Inst.JiFenFormulaId then
        return this.itemDict.Count + this.bagItemDict.Count + this.fishItemDict.Count - 1
    else
        return this.itemDict.Count + this.bagItemDict.Count + this.fishItemDict.Count
    end 
end
CQianKunDaiInputView.m_SyncSelectedItems_CS2LuaHook = function (this, items) 
    this.ItemSelected = InitializeListWithCollection(CreateFromClass(MakeGenericClass(List, String)), String, items)
    this:ShowSelectableItem()
end
CQianKunDaiInputView.m_OnAutoInputClicked_CS2LuaHook = function (this, go) 

    if this.Formula == nil then
        this.Formula = CQianKunDaiMgr.Inst:GetQianKunDaiFormula(this.RecipeId)
    end

    -- 清空已经选择的物品
    -- 此处如果是技能书合成（支持连续放入），则不该做如此处理
    if not this:IsContinuousInput() then
        this:ClearAllSelection()
        this:ShowSelectableItem()
        EventManager.Broadcast(EnumEventType.QianKunDaiClearAllSelection)
    end

    -- 可放入的物品小于等于0
    if this:GetInputTotalCount() < 1 then
        -- 支持连续放入且已放入数量>=1
        if this:IsContinuousInput() and this:GetSelectionCount() >= 1 then
            g_MessageMgr:ShowMessage("NOT_ENOUGH_MATERIALS")
            return
        else
            g_MessageMgr:ShowMessage("QIANKUNDAI_NO_MATERIAL")
            return
        end
    end
    -- 已放入的物品超过上限
    if this:GetSelectionCount() >= CQianKunDaiInputView.MAX_SELECTION_COUNT then
        g_MessageMgr:ShowMessage("MAX_HECHENG_NUM")
        return
    end

    local formula = QianKunDai_Formula.GetData(this.RecipeId)
    if formula ~= nil and formula.IsBangDing == 1 then
        this:AutoInput_BangDingFit()
    elseif formula ~= nil and formula.IsBangDing == 0 then
        this:AutoInput_BangDingUnFit()
    end
end
CQianKunDaiInputView.m_AutoInput_BangDingFit_CS2LuaHook = function (this) 
    local notBindedEnough = true
    local bindedEnough = true

    do
        local i = 0
        while i < this.Formula.FilterList.Count do
            notBindedEnough = notBindedEnough and this:CheckFilterEnough(this.Formula.FilterList[i], 0)
            bindedEnough = bindedEnough and this:CheckFilterEnough(this.Formula.FilterList[i], 1)
            i = i + 1
        end
    end

    -- 支持连续放入的合成公式，需要验证绑定信息是否与已放入的物品一致
    if this:IsContinuousInput() then
        if this:GetSelectionCount() < 1 and (not notBindedEnough) and not bindedEnough then
            g_MessageMgr:ShowMessage("QIANKUNDAI_NOT_ENOUGH_MATERIALS")
            return
        elseif not this:IsJiNengShuEnough(notBindedEnough, bindedEnough) then
            g_MessageMgr:ShowMessage("NOT_ENOUGH_MATERIALS")
            return
        end
    end

    -- 不绑定数量和绑定的数量都不足
    if not notBindedEnough and not bindedEnough then
        g_MessageMgr:ShowMessage("QIANKUNDAI_NOT_ENOUGH_MATERIALS")
        return
    end

    -- 某条pass规则下，通过的物品集合
    local passItems = CreateFromClass(MakeGenericClass(List, String))
    do
        local i = 0
        while i < this.Formula.FilterList.Count do
            local continue
            repeat
                CommonDefs.ListClear(passItems)
                local filter = this.Formula.FilterList[i]

                if filter.Operation == EnumQianKunDaiOperation.LessThan then
                    continue = true
                    break
                end
                local itemProp = CClientMainPlayer.Inst.ItemProp
                local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)
                do
                    local j = 1
                    while j <= bagSize do
                        local continue
                        repeat
                            local id = itemProp:GetItemAt(EnumItemPlace.Bag, j)
                            local item = CItemMgr.Inst:GetById(id)

                            if item == nil then
                                continue = true
                                break
                            end

                            if CommonDefs.ListContains(this.ItemSelected, typeof(String), item.Id) then
                                continue = true
                                break
                            end

                            if this.RecipeId == CQianKunDaiMgr.Inst.JiNengShuFormulaId and notBindedEnough then
                                if item ~= nil and item.IsItem and item.Item:IsXiaoShuXiang() then
                                    local allItems = item.Item:GetAllItemsInItem()
                                    do
                                        local k = 0
                                        while k < allItems.Count do
                                            local bagItemInfo = (function () 
                                                local __localValue = CreateFromClass(ItemInBagItemInfo)
                                                __localValue.BagItemPos = j
                                                __localValue.PosInBagItem = k
                                                __localValue.TemplateId = allItems[k].itemTemplateId
                                                __localValue.Count = allItems[k].itemCount
                                                __localValue.IsBind = allItems[k].isBind
                                                return __localValue
                                            end)()
                                            if not CommonDefs.ListContains(this.ItemSelected, typeof(String), bagItemInfo:GetItemBagInfoString()) then
                                                CommonDefs.ListAdd(passItems, typeof(String), bagItemInfo:GetItemBagInfoString())
                                            end
                                            k = k + 1
                                        end
                                    end
                                end
                            end

                            if this.RecipeId == CQianKunDaiMgr.Inst.WenShiFormulaId then
                                if item ~= nil and item.IsItem and item.Item:IsWenShiNang() then
                                    local allItems = item.Item:GetAllItemsInItem()
                                    do
                                        local k = 0
                                        while k < allItems.Count do
                                            local bagItemInfo = (function () 
                                                local __localValue = CreateFromClass(ItemInBagItemInfo)
                                                __localValue.BagItemPos = j
                                                __localValue.PosInBagItem = k
                                                __localValue.TemplateId = allItems[k].itemTemplateId
                                                __localValue.Count = allItems[k].itemCount
                                                __localValue.IsBind = allItems[k].isBind
                                                return __localValue
                                            end)()
                                            if not CommonDefs.ListContains(this.ItemSelected, typeof(String), bagItemInfo:GetItemBagInfoString()) then
                                                if notBindedEnough then
                                                    if not bagItemInfo.IsBind then
                                                        CommonDefs.ListAdd(passItems, typeof(String), bagItemInfo:GetItemBagInfoString())
                                                    end
                                                elseif not notBindedEnough and bindedEnough then
                                                    if bagItemInfo.IsBind then
                                                        CommonDefs.ListAdd(passItems, typeof(String), bagItemInfo:GetItemBagInfoString())
                                                    end
                                                end
                                            end
                                            k = k + 1
                                        end
                                    end
                                end
                            end

                            if not filter:PassFilter(item) then
                                continue = true
                                break
                            end

                            if notBindedEnough then
                                if not item.IsBinded then
                                    CommonDefs.ListAdd(passItems, typeof(String), item.Id)
                                end
                            elseif not notBindedEnough and bindedEnough then
                                if item.IsBinded then
                                    CommonDefs.ListAdd(passItems, typeof(String), item.Id)
                                end
                            end
                            continue = true
                        until 1
                        if not continue then
                            break
                        end
                        j = j + 1
                    end
                end

                -- 物品根据物品品质和TC决定放入的顺序
                this:AutoInputSort(passItems)

                -- 此处一次性放入的数据，装备合成比较多
                local itemCount = 0
                do
                    local j = 0
                    while itemCount < this:GetInputNeedCount(filter) and j < passItems.Count do
                        CommonDefs.ListAdd(this.ItemSelected, typeof(String), passItems[j])
                        if ItemInBagItemInfo.ItemIdIsFakedBagItem(passItems[j]) then
                            local info = ItemInBagItemInfo.ParseStringToInfo(passItems[j])
                            if info ~= nil then
                                itemCount = itemCount + info.Count
                            end
                        else
                            local item = CItemMgr.Inst:GetById(passItems[j])
                            if item ~= nil then
                                itemCount = itemCount + item.Amount
                            end
                        end
                        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(EnumItemPlace.Bag, passItems[j])
                        EventManager.BroadcastInternalForLua(EnumEventType.QianKunDaiPutInItem, {pos, passItems[j]})
                        j = j + 1
                    end
                end
                this:ShowSelectableItem()
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
end
CQianKunDaiInputView.m_AutoInput_BangDingUnFit_CS2LuaHook = function (this) 
    local enough = true
    do
        local i = 0
        while i < this.Formula.FilterList.Count do
            enough = enough and this:CheckFilterEnough(this.Formula.FilterList[i], 2)
            i = i + 1
        end
    end

    if not enough then
        g_MessageMgr:ShowMessage("QIANKUNDAI_NOT_ENOUGH_MATERIALS")
        return
    end

    local passItems = CreateFromClass(MakeGenericClass(List, String))
    if not CClientMainPlayer.Inst then
        return
    end
    do
        local i = 0
        while i < this.Formula.FilterList.Count do
            local continue
            repeat
                CommonDefs.ListClear(passItems)
                local filter = this.Formula.FilterList[i]

                if filter.Operation == EnumQianKunDaiOperation.LessThan then
                    continue = true
                    break
                end

                local itemProp = CClientMainPlayer.Inst.ItemProp
                local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)
                do
                    local j = 1
                    while j <= bagSize do
                        local continue
                        repeat
                            local id = itemProp:GetItemAt(EnumItemPlace.Bag, j)
                            local item = CItemMgr.Inst:GetById(id)

                            if item == nil then
                                continue = true
                                break
                            end

                            if CommonDefs.ListContains(this.ItemSelected, typeof(String), item.Id) then
                                continue = true
                                break
                            end

                            if this.RecipeId == CQianKunDaiMgr.Inst.WenShiFormulaId then
                                if item ~= nil and item.IsItem and item.Item:IsWenShiNang() then
                                    local allItems = item.Item:GetAllItemsInItem()
                                    do
                                        local k = 0
                                        while k < allItems.Count do
                                            local bagItemInfo = (function () 
                                                local __localValue = CreateFromClass(ItemInBagItemInfo)
                                                __localValue.BagItemPos = j
                                                __localValue.PosInBagItem = k
                                                __localValue.TemplateId = allItems[k].itemTemplateId
                                                __localValue.Count = allItems[k].itemCount
                                                __localValue.IsBind = allItems[k].isBind
                                                return __localValue
                                            end)()
                                            if not CommonDefs.ListContains(this.ItemSelected, typeof(String), bagItemInfo:GetItemBagInfoString()) then
                                                CommonDefs.ListAdd(passItems, typeof(String), bagItemInfo:GetItemBagInfoString())
                                            end
                                            k = k + 1
                                        end
                                    end
                                end
                            end

                            if not filter:PassFilter(item) then
                                continue = true
                                break
                            end

                            CommonDefs.ListAdd(passItems, typeof(String), item.Id)
                            continue = true
                        until 1
                        if not continue then
                            break
                        end
                        j = j + 1
                    end
                end

                -- 物品根据物品品质和TC决定放入的顺序
                this:AutoInputSort(passItems)

                -- 此处一次性放入的数据，装备合成比较多
                if this.RecipeId ~= CQianKunDaiMgr.Inst.JiFenFormulaId then
                    local itemCount = 0
                    do
                        local j = 0
                        while itemCount < this:GetInputNeedCount(filter) and j < passItems.Count do
                            CommonDefs.ListAdd(this.ItemSelected, typeof(String), passItems[j])
                            if ItemInBagItemInfo.ItemIdIsFakedBagItem(passItems[j]) then
                                local info = ItemInBagItemInfo.ParseStringToInfo(passItems[j])
                                if info ~= nil then
                                    itemCount = itemCount + info.Count
                                end
                            else
                                local item = CItemMgr.Inst:GetById(passItems[j])
                                if item ~= nil then
                                    itemCount = itemCount + item.Amount
                                end
                            end
                            local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(EnumItemPlace.Bag, passItems[j])
                            EventManager.BroadcastInternalForLua(EnumEventType.QianKunDaiPutInItem, {pos, passItems[j]})
                            j = j + 1
                        end
                    end
                else
                    do
                        local j = 0
                        while j < passItems.Count do
                            local item = CItemMgr.Inst:GetById(passItems[j])
                            local jifenItem = QianKunDai_Item.GetData(item.TemplateId)
                            if item ~= nil and jifenItem ~= nil then
                                local totalCount = CItemMgr.Inst:GetItemCount(item.TemplateId)
                                if totalCount >= jifenItem.MinNum then
                                    local itemCounter = 0
                                    do
                                        local k = 1
                                        while k <= bagSize do
                                            local id = itemProp:GetItemAt(EnumItemPlace.Bag, k)
                                            if id ~= nil then
                                                local itemToPut = CItemMgr.Inst:GetById(id)
                                                if itemToPut ~= nil then
                                                    if itemToPut.TemplateId == item.TemplateId then
                                                        CommonDefs.ListAdd(this.ItemSelected, typeof(String), itemToPut.Id)
                                                        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(EnumItemPlace.Bag, itemToPut.Id)
                                                        EventManager.BroadcastInternalForLua(EnumEventType.QianKunDaiPutInItem, {pos, itemToPut.Id})

                                                        itemCounter = itemCounter + itemToPut.Amount
                                                        if itemCounter >= jifenItem.MinNum then
                                                            break
                                                        end
                                                    end
                                                end
                                            end
                                            k = k + 1
                                        end
                                    end
                                    break
                                end
                            end
                            j = j + 1
                        end
                    end
                end

                this:ShowSelectableItem()
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
    return
end
CQianKunDaiInputView.m_IsContinuousInput_CS2LuaHook = function (this) 
    return this.RecipeId == CQianKunDaiMgr.Inst.JiNengShuFormulaId
end
CQianKunDaiInputView.m_IsJiNengShuEnough_CS2LuaHook = function (this, notBindedEnough, bindedEnough) 
    -- 如果技能书合成没有已选中的物品
    if this:GetSelectionCount() < 1 then
        return notBindedEnough or bindedEnough
    else
        local firstItem = this.ItemSelected[0]
        if this:ItemIdIsFakedBagItem(firstItem) then
            return notBindedEnough
        else
            local item = CItemMgr.Inst:GetById(firstItem)
            if item ~= nil then
                if item.IsBinded then
                    return bindedEnough
                else
                    return notBindedEnough
                end
            end
        end
    end
    return false
end
CQianKunDaiInputView.m_CheckFilterEnough_CS2LuaHook = function (this, filter, type) 

    if filter.Operation == EnumQianKunDaiOperation.LessThan then
        return true
    end
    if not CClientMainPlayer.Inst then
        return
    end
    local passItems = CreateFromClass(MakeGenericClass(List, String))
    local itemProp = CClientMainPlayer.Inst.ItemProp
    local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)

    do
        local i = 1
        while i <= bagSize do
            local continue
            repeat
                local id = itemProp:GetItemAt(EnumItemPlace.Bag, i)
                local item = CItemMgr.Inst:GetById(id)
                repeat
                    local default = type
                    if default == 0 then
                        if filter:PassFilter(item) and not item.IsBinded and not CommonDefs.ListContains(this.ItemSelected, typeof(String), id) then
                            CommonDefs.ListAdd(passItems, typeof(String), item.Id)
                        end
                        if this.RecipeId == CQianKunDaiMgr.Inst.JiNengShuFormulaId then
                            if item ~= nil and item.IsItem and item.Item:IsXiaoShuXiang() then
                                local allItems = item.Item:GetAllItemsInItem()
                                do
                                    local j = 0
                                    while j < allItems.Count do
                                        local continue
                                        repeat
                                            if allItems[j].isBind then
                                                continue = true
                                                break
                                            end

                                            local bagItemInfo = (function () 
                                                local __localValue = CreateFromClass(ItemInBagItemInfo)
                                                __localValue.BagItemPos = i
                                                __localValue.PosInBagItem = j
                                                __localValue.TemplateId = allItems[j].itemTemplateId
                                                __localValue.Count = allItems[j].itemCount
                                                __localValue.IsBind = allItems[j].isBind
                                                return __localValue
                                            end)()
                                            if not allItems[j].isBind and not CommonDefs.ListContains(this.ItemSelected, typeof(String), bagItemInfo:GetItemBagInfoString()) then
                                                CommonDefs.ListAdd(passItems, typeof(String), bagItemInfo:GetItemBagInfoString())
                                            end
                                            continue = true
                                        until 1
                                        if not continue then
                                            break
                                        end
                                        j = j + 1
                                    end
                                end
                            end
                        end
                        if this.RecipeId == CQianKunDaiMgr.Inst.WenShiFormulaId then
                            if item ~= nil and item.IsItem and item.Item:IsWenShiNang() then
                                local allItems = item.Item:GetAllItemsInItem()
                                do
                                    local j = 0
                                    while j < allItems.Count do
                                        local continue
                                        repeat
                                            if allItems[j].isBind then
                                                continue = true
                                                break
                                            end

                                            local bagItemInfo = (function () 
                                                local __localValue = CreateFromClass(ItemInBagItemInfo)
                                                __localValue.BagItemPos = i
                                                __localValue.PosInBagItem = j
                                                __localValue.TemplateId = allItems[j].itemTemplateId
                                                __localValue.Count = allItems[j].itemCount
                                                __localValue.IsBind = allItems[j].isBind
                                                return __localValue
                                            end)()
                                            if not allItems[j].isBind and not CommonDefs.ListContains(this.ItemSelected, typeof(String), bagItemInfo:GetItemBagInfoString()) then
                                                CommonDefs.ListAdd(passItems, typeof(String), bagItemInfo:GetItemBagInfoString())
                                            end
                                            continue = true
                                        until 1
                                        if not continue then
                                            break
                                        end
                                        j = j + 1
                                    end
                                end
                            end
                        end
                        break
                    elseif default == 1 then
                        if filter:PassFilter(item) and item.IsBinded and not CommonDefs.ListContains(this.ItemSelected, typeof(String), id) then
                            CommonDefs.ListAdd(passItems, typeof(String), item.Id)
                        end
                        if this.RecipeId == CQianKunDaiMgr.Inst.JiNengShuFormulaId then
                            if item ~= nil and item.IsItem and item.Item:IsXiaoShuXiang() then
                                local allItems = item.Item:GetAllItemsInItem()
                                do
                                    local j = 0
                                    while j < allItems.Count do
                                        local continue
                                        repeat
                                            if not allItems[j].isBind then
                                                continue = true
                                                break
                                            end

                                            local bagItemInfo = (function () 
                                                local __localValue = CreateFromClass(ItemInBagItemInfo)
                                                __localValue.BagItemPos = i
                                                __localValue.PosInBagItem = j
                                                __localValue.TemplateId = allItems[j].itemTemplateId
                                                __localValue.Count = allItems[j].itemCount
                                                __localValue.IsBind = allItems[j].isBind
                                                return __localValue
                                            end)()
                                            if allItems[j].isBind and not CommonDefs.ListContains(this.ItemSelected, typeof(String), bagItemInfo:GetItemBagInfoString()) then
                                                CommonDefs.ListAdd(passItems, typeof(String), bagItemInfo:GetItemBagInfoString())
                                            end
                                            continue = true
                                        until 1
                                        if not continue then
                                            break
                                        end
                                        j = j + 1
                                    end
                                end
                            end
                        end
                        if this.RecipeId == CQianKunDaiMgr.Inst.WenShiFormulaId then
                            if item ~= nil and item.IsItem and item.Item:IsWenShiNang() then
                                local allItems = item.Item:GetAllItemsInItem()
                                do
                                    local j = 0
                                    while j < allItems.Count do
                                        local continue
                                        repeat
                                            if not allItems[j].isBind then
                                                continue = true
                                                break
                                            end

                                            local bagItemInfo = (function () 
                                                local __localValue = CreateFromClass(ItemInBagItemInfo)
                                                __localValue.BagItemPos = i
                                                __localValue.PosInBagItem = j
                                                __localValue.TemplateId = allItems[j].itemTemplateId
                                                __localValue.Count = allItems[j].itemCount
                                                __localValue.IsBind = allItems[j].isBind
                                                return __localValue
                                            end)()
                                            if allItems[j].isBind and not CommonDefs.ListContains(this.ItemSelected, typeof(String), bagItemInfo:GetItemBagInfoString()) then
                                                CommonDefs.ListAdd(passItems, typeof(String), bagItemInfo:GetItemBagInfoString())
                                            end
                                            continue = true
                                        until 1
                                        if not continue then
                                            break
                                        end
                                        j = j + 1
                                    end
                                end
                            end
                        end
                        break
                    elseif default == 2 then
                        if filter:PassFilter(item) and not CommonDefs.ListContains(this.ItemSelected, typeof(String), id) then
                            CommonDefs.ListAdd(passItems, typeof(String), item.Id)
                        end
                        if this.RecipeId == CQianKunDaiMgr.Inst.JiNengShuFormulaId then
                            if item ~= nil and item.IsItem and item.Item:IsXiaoShuXiang() then
                                local allItems = item.Item:GetAllItemsInItem()
                                do
                                    local j = 0
                                    while j < allItems.Count do
                                        local bagItemInfo = (function () 
                                            local __localValue = CreateFromClass(ItemInBagItemInfo)
                                            __localValue.BagItemPos = i
                                            __localValue.PosInBagItem = j
                                            __localValue.TemplateId = allItems[j].itemTemplateId
                                            __localValue.Count = allItems[j].itemCount
                                            __localValue.IsBind = allItems[j].isBind
                                            return __localValue
                                        end)()
                                        if not CommonDefs.ListContains(this.ItemSelected, typeof(String), bagItemInfo:GetItemBagInfoString()) then
                                            CommonDefs.ListAdd(passItems, typeof(String), bagItemInfo:GetItemBagInfoString())
                                        end
                                        j = j + 1
                                    end
                                end
                            end
                        end
                        if this.RecipeId == CQianKunDaiMgr.Inst.WenShiFormulaId then
                            if item ~= nil and item.IsItem and item.Item:IsWenShiNang() then
                                local allItems = item.Item:GetAllItemsInItem()
                                do
                                    local j = 0
                                    while j < allItems.Count do
                                        local bagItemInfo = (function () 
                                            local __localValue = CreateFromClass(ItemInBagItemInfo)
                                            __localValue.BagItemPos = i
                                            __localValue.PosInBagItem = j
                                            __localValue.TemplateId = allItems[j].itemTemplateId
                                            __localValue.Count = allItems[j].itemCount
                                            __localValue.IsBind = allItems[j].isBind
                                            return __localValue
                                        end)()
                                        if not CommonDefs.ListContains(this.ItemSelected, typeof(String), bagItemInfo:GetItemBagInfoString()) then
                                            CommonDefs.ListAdd(passItems, typeof(String), bagItemInfo:GetItemBagInfoString())
                                        end
                                        j = j + 1
                                    end
                                end
                            end
                        end
                        break
                    end
                until 1
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
    if this.RecipeId ~= CQianKunDaiMgr.Inst.JiFenFormulaId then
        local count = 0
        do
            local i = 0
            while i < passItems.Count do
                local continue
                repeat
                    local item = CItemMgr.Inst:GetById(passItems[i])
                    if item ~= nil then
                        count = count + item.Amount
                        continue = true
                        break
                    end
                    if this:ItemIdIsFakedBagItem(passItems[i]) then
                        local info = ItemInBagItemInfo.ParseStringToInfo(passItems[i])
                        count = count + info.Count
                    end
                    continue = true
                until 1
                if not continue then
                    break
                end
                i = i + 1
            end
        end
        return count >= filter.Num
    else
        do
            local i = 0
            while i < passItems.Count do
                local item = CItemMgr.Inst:GetById(passItems[i])
                if item ~= nil then
                    local jifenItem = QianKunDai_Item.GetData(item.TemplateId)
                    if jifenItem ~= nil then
                        local totalCount = CItemMgr.Inst:GetItemCount(item.TemplateId)
                        if totalCount >= jifenItem.MinNum then
                            return true
                        end
                    end
                end
                i = i + 1
            end
        end
        return false
    end

    --return Lua.getLuaRetValue<bool>();
end
CQianKunDaiInputView.m_CheckFilterEnough_NotBinded_CS2LuaHook = function (this, filter) 

    if filter.Operation == EnumQianKunDaiOperation.LessThan then
        return true
    end
    if not CClientMainPlayer.Inst then
        return
    end
    local passNotBindedItems = CreateFromClass(MakeGenericClass(List, String))
    local itemProp = CClientMainPlayer.Inst.ItemProp
    local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)

    do
        local i = 1
        while i <= bagSize do
            local id = itemProp:GetItemAt(EnumItemPlace.Bag, i)
            local item = CItemMgr.Inst:GetById(id)
            if filter:PassFilter(item) and not item.IsBinded and not CommonDefs.ListContains(this.ItemSelected, typeof(String), id) then
                CommonDefs.ListAdd(passNotBindedItems, typeof(String), item.Id)
            end

            -- 如果是技能书合成
            if this.RecipeId == CQianKunDaiMgr.Inst.JiNengShuFormulaId then
                if item ~= nil and item.IsItem and item.Item:IsXiaoShuXiang() then
                    local allItems = item.Item:GetAllItemsInItem()
                    do
                        local j = 0
                        while j < allItems.Count do
                            local bagItemInfo = (function () 
                                local __localValue = CreateFromClass(ItemInBagItemInfo)
                                __localValue.BagItemPos = i
                                __localValue.PosInBagItem = j
                                __localValue.TemplateId = allItems[j].itemTemplateId
                                __localValue.Count = allItems[j].itemCount
                                __localValue.IsBind = allItems[j].isBind
                                return __localValue
                            end)()
                            if not CommonDefs.ListContains(this.ItemSelected, typeof(String), bagItemInfo:GetItemBagInfoString()) then
                                CommonDefs.ListAdd(passNotBindedItems, typeof(String), bagItemInfo:GetItemBagInfoString())
                            end
                            j = j + 1
                        end
                    end
                end
            end
            i = i + 1
        end
    end
    return passNotBindedItems.Count >= filter.Num
end
CQianKunDaiInputView.m_CheckFilterEnough_Binded_CS2LuaHook = function (this, filter) 
    if filter.Operation == EnumQianKunDaiOperation.LessThan then
        return true
    end
    if not CClientMainPlayer.Inst then
        return
    end
    local passBindedItems = CreateFromClass(MakeGenericClass(List, String))
    local itemProp = CClientMainPlayer.Inst.ItemProp
    local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)

    do
        local j = 1
        while j <= bagSize do
            local id = itemProp:GetItemAt(EnumItemPlace.Bag, j)
            local item = CItemMgr.Inst:GetById(id)
            if filter:PassFilter(item) and item.IsBinded and not CommonDefs.ListContains(this.ItemSelected, typeof(String), id) then
                CommonDefs.ListAdd(passBindedItems, typeof(String), item.Id)
            end
            j = j + 1
        end
    end
    return passBindedItems.Count >= filter.Num
end
CQianKunDaiInputView.m_AutoInputSort_CS2LuaHook = function (this, passedItems) 

    local items = CreateFromClass(MakeGenericClass(List, CCommonItem))
    do
        local i = 0
        while i < passedItems.Count do
            local item = CItemMgr.Inst:GetById(passedItems[i])
            CommonDefs.ListAdd(items, typeof(CCommonItem), item)
            i = i + 1
        end
    end

    repeat
        local default = this.RecipeId
        if default == 1 then
            this:AutoInputSort_Equip(items)
            CommonDefs.ListClear(passedItems)
            do
                local i = 0
                while i < items.Count do
                    CommonDefs.ListAdd(passedItems, typeof(String), items[i].Id)
                    i = i + 1
                end
            end
            break
        elseif default == 2 then
            this:AutoInputSort_ZhuShaBi(items)
            CommonDefs.ListClear(passedItems)
            do
                local i = 0
                while i < items.Count do
                    CommonDefs.ListAdd(passedItems, typeof(String), items[i].Id)
                    i = i + 1
                end
            end
            break
        elseif default == 3 then
            this:AutoInputSort_JiNengShu(passedItems)
            break
        elseif default == 4 then
            this:AutoInputSort_JiFen(items)
            CommonDefs.ListClear(passedItems)
            do
                local i = 0
                while i < items.Count do
                    CommonDefs.ListAdd(passedItems, typeof(String), items[i].Id)
                    i = i + 1
                end
            end
            break
        elseif default == 5 then
            this:AutoInputSort_JiNengShu(passedItems)
            break
        else
            this:AutoInputSort_Default(items)
            CommonDefs.ListClear(passedItems)
            do
                local i = 0
                while i < items.Count do
                    CommonDefs.ListAdd(passedItems, typeof(String), items[i].Id)
                    i = i + 1
                end
            end
            break
        end
    until 1
end
CQianKunDaiInputView.m_AutoInputSort_Equip_CS2LuaHook = function (this, passedItems) 
    CommonDefs.ListSort1(passedItems, typeof(CCommonItem), DelegateFactory.Comparison_CCommonItem(function (item1, item2) 
        if item1.IsEquip and item2.IsEquip then
            if EnumToInt(item1.Equip.Color) < EnumToInt(item2.Equip.Color) then
                return - 1
            elseif EnumToInt(item1.Equip.Color) > EnumToInt(item2.Equip.Color) then
                return 1
            else
                if item1.Equip.WordsCount < item2.Equip.WordsCount then
                    return - 1
                elseif item1.Equip.WordsCount > item2.Equip.WordsCount then
                    return 1
                else
                    local equip1 = EquipmentTemplate_Equip.GetData(item1.TemplateId)
                    local equip2 = EquipmentTemplate_Equip.GetData(item2.TemplateId)
                    if equip1.TC < equip2.TC then
                        return - 1
                    elseif equip1.TC > equip2.TC then
                        return 1
                    else
                        return 0
                    end
                end
            end
        elseif item1.IsItem and item2.IsItem then
            if EnumToInt(item1.Item.QualityType) < EnumToInt(item2.Item.QualityType) then
                return - 1
            elseif EnumToInt(item1.Item.QualityType) > EnumToInt(item2.Item.QualityType) then
                return 1
            else
                local it1 = Item_Item.GetData(item1.TemplateId)
                local it2 = Item_Item.GetData(item2.TemplateId)
                if it1.Tc < it2.Tc then
                    return - 1
                elseif it1.Tc > it2.Tc then
                    return 1
                else
                    return 0
                end
            end
        end
        return 0
    end))
end
CQianKunDaiInputView.m_AutoInputSort_ZhuShaBi_CS2LuaHook = function (this, passedItems) 
    CommonDefs.ListSort1(passedItems, typeof(CCommonItem), DelegateFactory.Comparison_CCommonItem(function (item1, item2) 
        if item1.IsItem and item2.IsItem then
            if item1.Item.Type == EnumItemType_lua.EvaluatedZhushabi and item2.Item.Type == EnumItemType_lua.EvaluatedZhushabi then
                local quality1 = this:GetZhuShaBiQuality(item1)
                local quality2 = this:GetZhuShaBiQuality(item2)

                if quality1 < quality2 then
                    return - 1
                elseif quality1 > quality2 then
                    return 1
                else
                    local it1 = Item_Item.GetData(item1.TemplateId)
                    local it2 = Item_Item.GetData(item2.TemplateId)
                    local adjustedGradeCheck1 = CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(it1)
                    local adjustedGradeCheck2 = CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(it2)
                    if adjustedGradeCheck1 < adjustedGradeCheck2 then
                        return - 1
                    elseif adjustedGradeCheck1 > adjustedGradeCheck2 then
                        return 1
                    else
                        return 0
                    end
                end
            else
                if EnumToInt(item1.Item.QualityType) < EnumToInt(item2.Item.QualityType) then
                    return - 1
                elseif EnumToInt(item1.Item.QualityType) > EnumToInt(item2.Item.QualityType) then
                    return 1
                else
                    local it1 = Item_Item.GetData(item1.TemplateId)
                    local it2 = Item_Item.GetData(item2.TemplateId)
                    if it1.Tc < it2.Tc then
                        return - 1
                    elseif it1.Tc > it2.Tc then
                        return 1
                    else
                        return 0
                    end
                end
            end
        end
        return 0
    end))
end
CQianKunDaiInputView.m_GetZhuShaBiQuality_CS2LuaHook = function (this, zhushabi) 
    local data = CreateFromClass(CChuanjiabaoWordType)
    data:LoadFromString(zhushabi.Item.ExtraVarData.Data)
    local wordCount = 0
    if data ~= nil then
        wordCount = data.Words.Count
    end
    if wordCount <= 4 then
        return 1
    elseif wordCount <= 6 then
        return 2
    else
        return 3
    end
end
CQianKunDaiInputView.m_AutoInputSort_JiNengShu_CS2LuaHook = function (this, passedItems) 
    CommonDefs.ListSort1(passedItems, typeof(String), DelegateFactory.Comparison_string(function (itemId1, itemId2) 
        if this:ItemIdIsFakedBagItem(itemId1) and this:ItemIdIsFakedBagItem(itemId2) then
            local info1 = ItemInBagItemInfo.ParseStringToInfo(itemId1)
            local info2 = ItemInBagItemInfo.ParseStringToInfo(itemId2)
            local item1 = Item_Item.GetData(info1.TemplateId)
            local item2 = Item_Item.GetData(info2.TemplateId)
            if item1.Tc < item2.Tc then
                return - 1
            elseif item1.Tc > item2.Tc then
                return 1
            else
                return 0
            end
        elseif not this:ItemIdIsFakedBagItem(itemId1) and this:ItemIdIsFakedBagItem(itemId2) then
            local info1 = CItemMgr.Inst:GetById(itemId1)
            local info2 = ItemInBagItemInfo.ParseStringToInfo(itemId2)
            local item1 = Item_Item.GetData(info1.TemplateId)
            local item2 = Item_Item.GetData(info2.TemplateId)
            if item1.Tc < item2.Tc then
                return - 1
            elseif item1.Tc > item2.Tc then
                return 1
            else
                return 0
            end
        elseif this:ItemIdIsFakedBagItem(itemId1) and not this:ItemIdIsFakedBagItem(itemId2) then
            local info1 = ItemInBagItemInfo.ParseStringToInfo(itemId1)
            local info2 = CItemMgr.Inst:GetById(itemId2)
            local item1 = Item_Item.GetData(info1.TemplateId)
            local item2 = Item_Item.GetData(info2.TemplateId)
            if item1.Tc < item2.Tc then
                return - 1
            elseif item1.Tc > item2.Tc then
                return 1
            else
                return 0
            end
        elseif not this:ItemIdIsFakedBagItem(itemId1) and not this:ItemIdIsFakedBagItem(itemId2) then
            local info1 = CItemMgr.Inst:GetById(itemId1)
            local info2 = CItemMgr.Inst:GetById(itemId2)
            local item1 = Item_Item.GetData(info1.TemplateId)
            local item2 = Item_Item.GetData(info2.TemplateId)
            if item1.Tc < item2.Tc then
                return - 1
            elseif item1.Tc > item2.Tc then
                return 1
            else
                return 0
            end
        end
        return 0
    end))
end
CQianKunDaiInputView.m_AutoInputSort_JiFen_CS2LuaHook = function (this, passedItems) 
    this:AutoInputSort_Default(passedItems)
end
CQianKunDaiInputView.m_AutoInputSort_Default_CS2LuaHook = function (this, passedItems) 
    CommonDefs.ListSort1(passedItems, typeof(CCommonItem), DelegateFactory.Comparison_CCommonItem(function (item1, item2) 
        if item1.IsItem and item2.IsItem then
            if EnumToInt(item1.Item.QualityType) < EnumToInt(item2.Item.QualityType) then
                return - 1
            elseif EnumToInt(item1.Item.QualityType) > EnumToInt(item2.Item.QualityType) then
                return 1
            else
                local it1 = Item_Item.GetData(item1.TemplateId)
                local it2 = Item_Item.GetData(item2.TemplateId)
                if it1.Tc < it2.Tc then
                    return - 1
                elseif it1.Tc > it2.Tc then
                    return 1
                else
                    return 0
                end
            end
        elseif item1.IsEquip and item2.IsEquip then
            if EnumToInt(item1.Equip.Color) < EnumToInt(item2.Equip.Color) then
                return - 1
            elseif EnumToInt(item1.Equip.Color) > EnumToInt(item2.Equip.Color) then
                return 1
            else
                local equip1 = EquipmentTemplate_Equip.GetData(item1.TemplateId)
                local equip2 = EquipmentTemplate_Equip.GetData(item2.TemplateId)
                if equip1.TC < equip2.TC then
                    return - 1
                elseif equip1.TC > equip2.TC then
                    return 1
                else
                    return 0
                end
            end
        end
        return 0
    end))
end
CQianKunDaiInputView.m_GetInputNeedCount_CS2LuaHook = function (this, filter) 
    repeat
        local default = filter.FormulaId
        if default == 1 then
            return CQianKunDaiInputView.MAX_SELECTION_COUNT
        else
            return filter.Num
        end
    until 1
end
CQianKunDaiInputView.m_OnSelectFormulaBtnClicked_CS2LuaHook = function (this, go) 
    this.selectFormulaBtn_Arrow.transform:Rotate(Vector3(0, 0, 180))
    local menuList = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))

    QianKunDai_Formula.ForeachKey(DelegateFactory.Action_object(function (key) 
        local formula = QianKunDai_Formula.GetData(key)
        if formula.IsPublic ~= 0 and formula.Status ~= 3 then
            CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(formula.Type, DelegateFactory.Action_int(function (idx) 
                this:SelectFormula(key)
            end), false, nil))
        end
    end))

    CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("更多"), DelegateFactory.Action_int(function (idx) 
        this:MoreFormula(idx)
    end), this:NeedAlertMoreFormula(), nil, EnumPopupMenuItemStyle.Orange))

    CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(menuList), go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, nil, 350, true, 320)
end
CQianKunDaiInputView.m_SelectFormula_CS2LuaHook = function (this, index) 
    EventManager.BroadcastInternalForLua(EnumEventType.QianKunDaiUpdateFormula, {index})
end
CQianKunDaiInputView.m_MoreFormula_CS2LuaHook = function (this, index) 
    CUIManager.ShowUI(CUIResources.QianKunDaiRecipeWnd)
end
CQianKunDaiInputView.m_UpdateSelectedFormula_CS2LuaHook = function (this, formulaId) 
    if this.RecipeId == formulaId then
        return
    end

    this.RecipeId = formulaId
    this:InitQianKunDaiInput(formulaId)
end
CQianKunDaiInputView.m_OnSetItemAt_CS2LuaHook = function (this, place, position, oldItemId, newItemId) 
    if place ~= EnumItemPlace.Bag or CClientMainPlayer.Inst == nil or position == 0 then
        return
    end
    -- 动画期间不做处理
    if CQianKunDaiCompoundView.InBagAnimation then
        return
    end
    this:ShowSelectableItem()
end
CQianKunDaiInputView.m_OnSendItem_CS2LuaHook = function (this, itemId) 

    -- 动画期间不做处理
    if CQianKunDaiCompoundView.InBagAnimation then
        return
    end

    if CommonDefs.DictContains(this.itemDict, typeof(String), itemId) then
        local inputItem = CommonDefs.DictGetValue(this.itemDict, typeof(String), itemId)
        inputItem:Init(itemId)
    else
        this:ShowSelectableItem()
    end
end
CQianKunDaiInputView.m_OnUpdateItemInfo_CS2LuaHook = function (this, itemId) 
    if System.String.IsNullOrEmpty(itemId) then
        return
    end

    if CommonDefs.DictContains(this.itemDict, typeof(String), itemId) then
        local input = CommonDefs.DictGetValue(this.itemDict, typeof(String), itemId)
        input:Init(itemId)
    end
end
CQianKunDaiInputView.m_OnPutIn_CS2LuaHook = function (this) 
    if this:GetSelectionCount() >= CQianKunDaiInputView.MAX_SELECTION_COUNT then
        g_MessageMgr:ShowMessage("MAX_HECHENG_NUM")
        return
    end
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    if item ~= nil and CClientMainPlayer.Inst ~= nil then
        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(EnumItemPlace.Bag, item.Id)
        CommonDefs.ListAdd(this.ItemSelected, typeof(String), item.Id)

        EventManager.BroadcastInternalForLua(EnumEventType.QianKunDaiPutInItem, {pos, this.SelectedItemId})
        this:ShowSelectableItem()
        CItemInfoMgr.CloseItemInfoWnd()
    end
end
CQianKunDaiInputView.m_OnPutIn_BagItem_CS2LuaHook = function (this) 

    if this:GetSelectionCount() >= CQianKunDaiInputView.MAX_SELECTION_COUNT then
        g_MessageMgr:ShowMessage("MAX_HECHENG_NUM")
        return
    end
    if this.SelectedBagItem ~= nil then
        CommonDefs.ListAdd(this.ItemSelected, typeof(String), this.SelectedBagItem:GetItemBagInfoString())
        EventManager.BroadcastInternalForLua(EnumEventType.QianKunDaiPutInItem, {this.SelectedBagItem.BagItemPos, this.SelectedBagItem:GetItemBagInfoString()})
        this:ShowSelectableItem()
        CItemInfoMgr.CloseItemInfoWnd()
    end
end
CQianKunDaiInputView.m_OnTakeOut_CS2LuaHook = function (this, itemId) 
    if CommonDefs.ListContains(this.ItemSelected, typeof(String), itemId) then
        CommonDefs.ListRemove(this.ItemSelected, typeof(String), itemId)
    end
    this:ShowSelectableItem()
end
CQianKunDaiInputView.m_OnSplitItem_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst ~= nil then
        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(EnumItemPlace.Bag, this.SelectedItemId)
        local item = CItemMgr.Inst:GetById(this.SelectedItemId)
        if pos > 0 and item ~= nil and item.IsItem and item.Item.IsOverlap and item.Amount > 1 then
            CLuaNumberInputMgr.ShowNumInputBox(1, item.Amount - 1, 1, function (val) 
                if val > 0 and val < item.Amount then
                    Gac2Gas.RequestSplitItem(EnumItemPlace_lua.Bag, pos, this.SelectedItemId, val)
                end
            end, LocalString.GetString("请输入需要拆分的数量"), -1)
        end
    end
end
CQianKunDaiInputView.m_OnSplitItem_BagItem_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst ~= nil then
        if this.SelectedBagItem ~= nil and this.SelectedBagItem.Count > 1 then
            CLuaNumberInputMgr.ShowNumInputBox(1, this.SelectedBagItem.Count - 1, 1, function (val) 
                if val > 0 and val < this.SelectedBagItem.Count then
                    Gac2Gas.RequestMoveItemOutFromItemBag(this.SelectedBagItem.BagItemPos, this.SelectedBagItem.PosInBagItem + 1, val)
                end
            end, LocalString.GetString("请输入需要拆分的数量"), -1)
        end
    end
end
CQianKunDaiInputView.m_GetActionPairs_CS2LuaHook = function (this, itemId, templateId) 
    if CClientMainPlayer.Inst == nil then
        return nil
    end

    if System.String.IsNullOrEmpty(itemId) then
        local item = Item_Item.GetData(templateId)
        if item == nil then
            return nil
        end
        local isGuanShangFish = HouseFish_AllFishes.GetData(templateId)
        local putInAction,splitItemAction
        if isGuanShangFish then
            --如果包裹有位置才能放入，需先从包裹中取出
            putInAction = StringActionKeyValuePair(LocalString.GetString("放入"), DelegateFactory.Action(function ()
                MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("QianKunDai_CantRevert_PutFishInFishBag"), 
                    DelegateFactory.Action(function ()
                        Gac2Gas.MoveFishBasketToBag(EnumFishBasketType.Fish,this.SelectedFishBagPos,1) 
                    end),
                    nil,
                    LocalString.GetString("确定"), LocalString.GetString("取消"), false)
            end))
        else
            putInAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("放入"), MakeDelegateFromCSFunction(this.OnPutIn_BagItem, Action0, this))
            splitItemAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("拆分"), MakeDelegateFromCSFunction(this.OnSplitItem_BagItem, Action0, this))
        end

        local actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
        CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), putInAction)
        if not isGuanShangFish and this.RecipeId == CQianKunDaiMgr.Inst.JiNengShuFormulaId or this.RecipeId == CQianKunDaiMgr.Inst.JiFenFormulaId or this.RecipeId == CQianKunDaiMgr.Inst.RanSeFormulaId then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), splitItemAction)
        end
        return actionPairs
    else
        local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
        local item = CItemMgr.Inst:GetById(itemId)
        if pos == 0 or item == nil then
            return nil
        end
        local putInAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("放入"), MakeDelegateFromCSFunction(this.OnPutIn, Action0, this))
        local splitItemAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("拆分"), MakeDelegateFromCSFunction(this.OnSplitItem, Action0, this))
        local actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
        CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), putInAction)
        if item.Amount > 1 and (this.RecipeId == CQianKunDaiMgr.Inst.JiNengShuFormulaId or this.RecipeId == CQianKunDaiMgr.Inst.JiFenFormulaId or (this.RecipeId == CQianKunDaiMgr.Inst.WenShiFormulaId and not item.IsBinded) or this.RecipeId == CQianKunDaiMgr.Inst.RanSeFormulaId) then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), splitItemAction)
        end
        return actionPairs
    end
end
CQianKunDaiInputView.m_ItemIdIsFakedBagItem_CS2LuaHook = function (this, itemId) 
    if System.String.IsNullOrEmpty(itemId) then
        return false
    end
    return (string.find(itemId, ":", 1, true) ~= nil)
end

LuaQianKunDaiInputWnd = {}
LuaQianKunDaiInputWnd.m_Wnd = nil

function LuaQianKunDaiInputWnd:OnEnable(wnd)
    LuaQianKunDaiInputWnd.m_Wnd = wnd
    g_ScriptEvent:AddListener("SendItem",self,"OnSendItem")
end

function LuaQianKunDaiInputWnd:OnDisable()
    LuaQianKunDaiInputWnd.m_Wnd = nil
    g_ScriptEvent:RemoveListener("SendItem",self,"OnSendItem")
end

function LuaQianKunDaiInputWnd:OnSendItem(args)
    local item = args and args[0] and CItemMgr.Inst:GetById(args[0]) or nil
    if item and LuaQianKunDaiInputWnd.m_Wnd.SelectedFishId and tonumber(item.TemplateId) == tonumber(LuaQianKunDaiInputWnd.m_Wnd.SelectedFishId) then
        LuaQianKunDaiInputWnd.m_Wnd.SelectedItemId = item.Id
        LuaQianKunDaiInputWnd.m_Wnd.SelectedFishId = nil
        LuaQianKunDaiInputWnd.m_Wnd:OnPutIn() 
        
    end
end