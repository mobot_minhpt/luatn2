require("common/common_include")

local CBabyModelTextureLoader = import "L10.UI.CBabyModelTextureLoader"
local BabyMgr = import "L10.Game.BabyMgr"
local CButton = import "L10.UI.CButton"

LuaBabyChooseWnd = class()

RegistChildComponent(LuaBabyChooseWnd, "OneNode", GameObject)
RegistChildComponent(LuaBabyChooseWnd, "TwoNode", GameObject)
RegistChildComponent(LuaBabyChooseWnd, "ChooseBtn", GameObject)
RegistChildComponent(LuaBabyChooseWnd, "TitleLabel", UILabel)

function LuaBabyChooseWnd:Init()
	self:InitClassMembers()
	self:InitValues()

end

function LuaBabyChooseWnd:InitClassMembers()
	

end

function LuaBabyChooseWnd:InitValues()
	local babyIdList = BabyMgr.Inst:GetBabyIdList()
  	local babyTable = {}
  	if not babyIdList or babyIdList.Count == 0 then
    	g_MessageMgr:ShowMessage("CUSTOM_STRING2", LuaBabyMgr.m_NoBabyToChooseMsg)
    	CUIManager.CloseUI(CLuaUIResources.BabyChooseWnd)
    	return
  	end

  	for i = 0, babyIdList.Count-1 do
  		local babyId = babyIdList[i]
  		if LuaBabyMgr.m_FilterBabyAction then
  			if LuaBabyMgr.m_FilterBabyAction(babyId) then
  				table.insert(babyTable, babyId)
  			end
  		else
  			table.insert(babyTable, babyId)
  		end
  	end

  	if #babyTable == 0 then
  		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LuaBabyMgr.m_NoBabyToChooseMsg)
    	CUIManager.CloseUI(CLuaUIResources.BabyChooseWnd)
    	return
  	end

  	if LuaBabyMgr.m_ChooseWndTitle then
  		self.TitleLabel.text = LuaBabyMgr.m_ChooseWndTitle
  	end
  	
  	self.OneNode:SetActive(false)
  	self.TwoNode:SetActive(false)

  	local chooseBtnScript = self.ChooseBtn:GetComponent(typeof(CButton))
  	if chooseBtnScript then
    	chooseBtnScript.Enabled = false
  	end

  	self:InitShowNode(babyTable)
end

function LuaBabyChooseWnd:InitShowNode(babyIdList)
	if #babyIdList == 1 then
    	self.OneNode:SetActive(true)
    	self.TwoNode:SetActive(false)
    	local babyInfo = BabyMgr.Inst:GetBabyById(babyIdList[1])
    	local nameText = self.OneNode.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    	local babyTexture = self.OneNode.transform:Find("babyTexture"):GetComponent(typeof(CBabyModelTextureLoader))
   		self:InitBabyShow(babyInfo, nameText, babyTexture)

   		local chooseBtnScript = self.ChooseBtn:GetComponent(typeof(CButton))
    	if chooseBtnScript then
      		chooseBtnScript.Enabled = true
    	end

    	local babyIndex = babyIdList[1]
    	local onSubmitClick = function(go)
      		if LuaBabyMgr.m_OnChooseBabyAction then
      			LuaBabyMgr.m_OnChooseBabyAction(babyIndex)
      		end
  		end
    	CommonDefs.AddOnClickListener(self.ChooseBtn,DelegateFactory.Action_GameObject(onSubmitClick),false)

  	elseif #babyIdList >= 2 then

    	self.OneNode:SetActive(false)
    	self.TwoNode:SetActive(true)
    	local babyInfo = BabyMgr.Inst:GetBabyById(babyIdList[1])
    	local nameText = self.TwoNode.transform:Find("left/NameLabel"):GetComponent(typeof(UILabel))
    	local babyTexture = self.TwoNode.transform:Find("left/babyTexture"):GetComponent(typeof(CBabyModelTextureLoader))
    	self:InitBabyShow(babyInfo,nameText,babyTexture)
    	local babyInfo2 = BabyMgr.Inst:GetBabyById(babyIdList[2])
    	local nameText2 = self.TwoNode.transform:Find("right/NameLabel"):GetComponent(typeof(UILabel))
    	local babyTexture2 = self.TwoNode.transform:Find("right/babyTexture"):GetComponent(typeof(CBabyModelTextureLoader))
    	self:InitBabyShow(babyInfo2,nameText2,babyTexture2)
    	local chooseBtnScript = self.ChooseBtn:GetComponent(typeof(CButton))
    	if chooseBtnScript then
      		chooseBtnScript.Enabled = false
    	end

    	local babyIndex = 0
    	local btn1 = self.TwoNode.transform:Find("left").gameObject
    	local btn2 = self.TwoNode.transform:Find("right").gameObject
    	local btn1Light = self.TwoNode.transform:Find("left/light").gameObject
    	local btn2Light = self.TwoNode.transform:Find("right/light").gameObject
    	btn1Light:SetActive(false)
    	btn2Light:SetActive(false)
    	local btn1Click = function(go)
      		babyIndex = babyIdList[1]
      		if chooseBtnScript then
        		chooseBtnScript.Enabled = true
      		end
      		btn1Light:SetActive(true)
      		btn2Light:SetActive(false)
    	end
    	local btn2Click = function(go)
      		babyIndex = babyIdList[2]
      		if chooseBtnScript then
        		chooseBtnScript.Enabled = true
      		end
      		btn1Light:SetActive(false)
      		btn2Light:SetActive(true)
    	end
    	CommonDefs.AddOnClickListener(btn1,DelegateFactory.Action_GameObject(btn1Click),false)
    	CommonDefs.AddOnClickListener(btn2,DelegateFactory.Action_GameObject(btn2Click),false)

    	local onSubmitClick = function(go)
      		if LuaBabyMgr.m_OnChooseBabyAction then
      			LuaBabyMgr.m_OnChooseBabyAction(babyIndex)
      		end
  		end
    	CommonDefs.AddOnClickListener(self.ChooseBtn, DelegateFactory.Action_GameObject(onSubmitClick),false)
  	end
end

function LuaBabyChooseWnd:InitBabyShow(babyInfo, nameText, babyTexture)
  	nameText.text = babyInfo.Name
  	babyTexture:Init(babyInfo, -180, false, 0)
end

function LuaBabyChooseWnd:OnBabyInfoDelete()
	CUIManager.CloseUI(CLuaUIResources.BabyChooseWnd)
end

function LuaBabyChooseWnd:OnEnable()
	g_ScriptEvent:AddListener("BabyInfoDelete", self, "OnBabyInfoDelete")
end

function LuaBabyChooseWnd:OnDisable()
	g_ScriptEvent:RemoveListener("BabyInfoDelete", self, "OnBabyInfoDelete")
end

return LuaBabyChooseWnd