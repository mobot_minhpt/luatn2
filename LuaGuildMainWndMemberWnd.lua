local CSwitchMgr=import "L10.Engine.CSwitchMgr"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local Profession = import "L10.Game.Profession"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Input = import "UnityEngine.Input"
local GuildDefine = import "L10.Game.GuildDefine"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local Constants = import "L10.Game.Constants"
local CMiniChatMgr = import "L10.UI.CMiniChatMgr"
local CGuildSortButton = import "L10.UI.CGuildSortButton"
local CGuildMgr = import "L10.Game.CGuildMgr"
-- local CGuildJobChangeModel = import "L10.UI.CGuildJobChangeModel"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local AlignType1 = import "CPlayerInfoMgr+AlignType"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
-- local DefaultOptionDataSource=import "L10.UI.DefaultOptionDataSource"
local QnRadioBox=import "L10.UI.QnRadioBox"
local QnLabel=import "L10.UI.QnLabel"
local QnTableView=import "L10.UI.QnTableView"
local QnButton=import "L10.UI.QnButton"
local QnIncreaseAndDecreaseButton=import "L10.UI.QnIncreaseAndDecreaseButton"
local PlayerSettings = import "L10.Game.PlayerSettings"


CLuaGuildMainWndMemberWnd = class()
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_MemberRadioBox")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_SortRadioBox_Member")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_MemberLabel")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_StudentsLabel")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_MemberList")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_MemberApplyList")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_SortRadioBox_Apply")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_MemberListBtn1")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_MemberListBtn3")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_MemberListBtn4")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_PlayBtn")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_MoreBtn")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_MorePanel")
RegistClassMember(CLuaGuildMainWndMemberWnd,"exportDataBtn")
RegistClassMember(CLuaGuildMainWndMemberWnd,"joinAssistGroupBtn")
RegistClassMember(CLuaGuildMainWndMemberWnd,"applylistBtn")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_CurrentIndex")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_CurrentPlayerIndex")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_AllGuildMemberInfos")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_AllGuildMemberHexinInfos")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_AllGuildMemberApplyInfos")
RegistClassMember(CLuaGuildMainWndMemberWnd,"pageTurnBtn")
RegistClassMember(CLuaGuildMainWndMemberWnd,"clearBtn")
RegistClassMember(CLuaGuildMainWndMemberWnd,"inviteFriendBtn")
RegistClassMember(CLuaGuildMainWndMemberWnd,"lastPageBtn")
RegistClassMember(CLuaGuildMainWndMemberWnd,"nextPageBtn")

RegistClassMember(CLuaGuildMainWndMemberWnd,"lastPageBtnBg")
RegistClassMember(CLuaGuildMainWndMemberWnd,"nextPageBtnBg")

RegistClassMember(CLuaGuildMainWndMemberWnd,"configBtn")

RegistClassMember(CLuaGuildMainWndMemberWnd,"FilterOnline")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_GuildMemberSortFlags")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_GuildHexinSortFlags")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_GuildApplySortFlags")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_ItemPage")
RegistClassMember(CLuaGuildMainWndMemberWnd,"m_IsGuildTerritorialWars")

function CLuaGuildMainWndMemberWnd:Awake()
    self.m_MemberRadioBox = self.transform:Find("LeftTopRow/QnRadioBox"):GetComponent(typeof(QnRadioBox))
    self.m_SortRadioBox_Member = self.transform:Find("QnTableViewMember/Header"):GetComponent(typeof(QnRadioBox))
    self.m_MemberLabel = self.transform:Find("LeftTopRow/Label/Member"):GetComponent(typeof(QnLabel))
    self.m_StudentsLabel = self.transform:Find("LeftTopRow/Label/Student"):GetComponent(typeof(QnLabel))
    self.m_MemberList = self.transform:Find("QnTableViewMember"):GetComponent(typeof(QnTableView))
    self.m_MemberApplyList = self.transform:Find("QnTableViewMemberApply"):GetComponent(typeof(QnTableView))
    self.m_SortRadioBox_Apply = self.transform:Find("QnTableViewMemberApply/Header"):GetComponent(typeof(QnRadioBox))
    self.m_MemberListBtn1 = self.transform:Find("QnTableViewMember/RightBottomRow/MemberListBtn1"):GetComponent(typeof(QnButton))
    self.m_MemberListBtn3 = self.transform:Find("QnTableViewMember/RightBottomRow/MemberListBtn3"):GetComponent(typeof(QnButton))
    self.m_MemberListBtn4 = self.transform:Find("QnTableViewMember/RightBottomRow/MemberListBtn4"):GetComponent(typeof(QnButton))
    self.m_PlayBtn = self.transform:Find("QnTableViewMember/RightBottomRow/PlayBtn").gameObject
    self.m_MoreBtn = self.transform:Find("QnTableViewMember/RightBottomRow/MoreBtn").gameObject
    self.m_MorePanel = self.transform:Find("QnTableViewMember/RightBottomRow/MorePanel").gameObject
    self.exportDataBtn = self.transform:Find("QnTableViewMember/RightBottomRow/MorePanel/ExportDataBtn").gameObject
    self.joinAssistGroupBtn = self.transform:Find("QnTableViewMember/RightBottomRow/MorePanel/JoinAssistGroup").gameObject
    self.applylistBtn = self.transform:Find("LeftTopRow/QnRadioBox/QnSelectableButton3").gameObject
    self.m_CurrentIndex = 0
    self.m_CurrentPlayerIndex = 0
    self.m_AllGuildMemberInfos = {}
    self.m_AllGuildMemberHexinInfos = {}
    self.m_AllGuildMemberApplyInfos = {}
    self.pageTurnBtn = self.transform:Find("QnTableViewMemberApply/Buttons/QnIncreaseAndDecreaseButton"):GetComponent(typeof(QnIncreaseAndDecreaseButton))
    self.clearBtn = self.transform:Find("QnTableViewMemberApply/Buttons/ClearBtn").gameObject--:GetComponent(typeof(QnButton))
    self.inviteFriendBtn = self.transform:Find("QnTableViewMemberApply/Buttons/InviteFriendBtn"):GetComponent(typeof(QnButton))
    self.nextPageBtn = self.transform:Find("QnTableViewMember/ChangePage/NextPageButton").gameObject
    self.lastPageBtn = self.transform:Find("QnTableViewMember/ChangePage/LastPageButton").gameObject

    self.nextPageBtnBg = self.transform:Find("QnTableViewMember/ChangePage/NextPageButtonBg").gameObject
    self.lastPageBtnBg = self.transform:Find("QnTableViewMember/ChangePage/LastPageButtonBg").gameObject

    self.configBtn = self.transform:Find("QnTableViewMember/RightBottomRow/MorePanel/OptionBtn").gameObject
    self.FilterOnline = false
    self.m_ItemPage = 0


    --这两句主要是保证qnradiobox的awake方法调用
    self.m_MemberList.gameObject:SetActive(true)
    self.m_MemberApplyList.gameObject:SetActive(true)
    self.transform.localPosition = Vector3.zero

    self:SetHeaderPage(self.m_ItemPage)
    UIEventListener.Get(self.inviteFriendBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) CUIManager.ShowUI(CUIResources.GuildInvitationWnd) end)
    UIEventListener.Get(self.nextPageBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClickPageChangeBtn(1) end)
    UIEventListener.Get(self.lastPageBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClickPageChangeBtn(-1) end)

    self:Awake_VNMerge()

    UIEventListener.Get(self.applylistBtn).onClick = DelegateFactory.VoidDelegate(function(go) 
        g_ScriptEvent:BroadcastInLua("GuildNotification",false)
    end)

    self.pageTurnBtn.onValueChanged = DelegateFactory.Action_int(function(val)
        local label = FindChild(self.pageTurnBtn.transform,"Label"):GetComponent(typeof(QnLabel))
        label.Text = SafeStringFormat3("%d/%d", val, self:GetPageCount())
        if self.m_CurrentIndex == 2 then
            self.m_MemberApplyList:ReloadData(true, true)
        end
    end)


    UIEventListener.Get(self.clearBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        -- TODO zzm
        local message = g_MessageMgr:FormatMessage("CUSTOM_STRING", LocalString.GetString("是否清空申请列表？"))
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
            Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "CleanApplyList", "", "")
        end), nil, nil, nil, false)
    end)

    UIEventListener.Get(self.configBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClickConfigButton(go) end)
    UIEventListener.Get(self.m_PlayBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnPlayBtnClick(go) end)
    UIEventListener.Get(self.exportDataBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnExportDataButtonClick(go) end)
    UIEventListener.Get(self.m_MoreBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnMoreBtnClick(go) end)
    self.m_MorePanel:SetActive(false)


    self.m_GuildMemberSortFlags={-1,-1,-1,-1,-1, -1,-1,-1,-1,-1, -1,-1, -1,-1}
    self.m_GuildHexinSortFlags={-1,-1,-1,-1,-1, -1,-1,-1,-1,-1, -1,-1, -1,-1}
    self.m_GuildApplySortFlags={-1,-1,-1,-1,-1, -1,-1}
    LuaGuildTerritorialWarsMgr.m_CanOpenContributeWnd = false
    Gac2Gas.CanOpenGTWRelatedPlayContributeWnd()
    Gac2Gas.GuildJuDianCanOpenContributionWnd()

end
function CLuaGuildMainWndMemberWnd:OnExportDataButtonClick(go)
    CUIManager.ShowUI(CUIResources.GuildMemberDataPickerWnd)
end
function CLuaGuildMainWndMemberWnd:OnClickConfigButton(go)
    CLuaOptionMgr.ShowDlg(
        function() return 1 end,
        function(index) return LocalString.GetString("只显示在线成员") end,
        function(index) return self.FilterOnline end,
        function(index,val) self.FilterOnline=val self:ReloadList() end,
        self.transform:TransformPoint(Vector3(600,-270,0))
    )
end
function CLuaGuildMainWndMemberWnd:OnMoreBtnClick(go)
    self.m_MorePanel:SetActive(true)
end

function CLuaGuildMainWndMemberWnd:Awake_VNMerge()
    if CommonDefs.IS_CN_CLIENT then
        UIEventListener.Get(self.joinAssistGroupBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnJoinAssistGroupClick(go) end)
    else
        self.joinAssistGroupBtn:SetActive(false)
    end
end

function CLuaGuildMainWndMemberWnd:SetHeaderPage( index)
    if index == 0 then
        self.m_SortRadioBox_Member:SetVisibleGroup(Table2Array({0, 1, 2, 3, 4, 5, 11},MakeArrayClass(Int32)), false)
    elseif index == 1 then
        self.m_SortRadioBox_Member:SetVisibleGroup(Table2Array({0,15, 6, 7,13, 8, 12}, MakeArrayClass(Int32)), false)
    elseif index ==2 then
        self.m_SortRadioBox_Member:SetVisibleGroup(Table2Array({0, 14, 9, 10}, MakeArrayClass(Int32)), false)
    end

    self.nextPageBtn:SetActive(index<2)
    self.nextPageBtnBg:SetActive(index<2)
    self.lastPageBtn:SetActive(index>0)
    self.lastPageBtnBg:SetActive(index>0)
end

function CLuaGuildMainWndMemberWnd:OnPlayBtnClick( go)
    local data = {}
    local callback = DelegateFactory.Action_int(function(index) self:OnPlayBtnSelect(index) end)
    table.insert(data, PopupMenuItemData(LocalString.GetString("帮会精英"), callback, false, nil))
    if LuaHengYuDangKouMgr:IsHengYuDangKouEnable() then -- 横屿荡寇开启
        table.insert( data,PopupMenuItemData(LocalString.GetString("荡寇精英"), callback,false,nil) )
    else
        table.insert( data,PopupMenuItemData(LocalString.GetString("寇岛精英"), callback,false,nil) )
    end
    if CGuildMgr.Inst.IsCityWarOpen then
        table.insert( data,PopupMenuItemData(LocalString.GetString("孤城守卫精英"), callback,false,nil) )
        table.insert( data,PopupMenuItemData(LocalString.GetString("城战管理"), callback,false,nil) )
        table.insert( data,PopupMenuItemData(LocalString.GetString("城战贡献"), callback,false,nil) )
    end
    if LuaGuildTerritorialWarsMgr.m_CanOpenContributeWnd then
        table.insert( data,PopupMenuItemData(LocalString.GetString("棋局贡献"), DelegateFactory.Action_int(function(index) self:OpenGuildTerritorialWarsContributionWnd() end),false,nil))
        table.insert( data,PopupMenuItemData(LocalString.GetString("棋局挑战精英"), DelegateFactory.Action_int(function(index) Gac2Gas.RequestGTWChallengeMemberInfo(true) end),false,nil))
    elseif LuaJuDianBattleMgr.CanOpenContributionWnd then
        table.insert( data,PopupMenuItemData(LocalString.GetString("棋局贡献"), DelegateFactory.Action_int(function(index) CUIManager.ShowUI(CLuaUIResources.JuDianBattleContributionWnd) end),false,nil))
        table.insert( data,PopupMenuItemData(LocalString.GetString("棋局挑战精英"), DelegateFactory.Action_int(function(index) Gac2Gas.RequestGuildJuDianChallengeMemberInfo(true) end),false,nil))
    end
    CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(data,MakeArrayClass(PopupMenuItemData)), go.transform, CPopupMenuInfoMgr.AlignType.Top)
end

function CLuaGuildMainWndMemberWnd:OpenGuildTerritorialWarsContributionWnd()
    CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsContributionWnd)
end

function CLuaGuildMainWndMemberWnd:OnPlayBtnSelect( index)
    if index == 0 then
        CUIManager.ShowUI(CLuaUIResources.BangHuiJingYingSettingWnd)
    elseif index == 1 then
        CUIManager.ShowUI(CLuaUIResources.KouDaoJingYingSettingWnd)
    elseif index ==2 then
        CUIManager.ShowUI(CLuaUIResources.CityWarMonsterSiegeSettingWnd)
    elseif index == 3 then
        CUIManager.ShowUI(CUIResources.CityWarSoldierWnd)
    elseif index == 4 then
        CUIManager.ShowUI(CLuaUIResources.CityWarContributionWnd)

    end
end
function CLuaGuildMainWndMemberWnd:OnJoinAssistGroupClick( go)
    if CommonDefs.IsPlayEnabled("AppHelper") then
        CWebBrowserMgr.Inst:OpenUrl("https://yxzs.163.com/qnm/join/?s=banghui")
    else
        g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
    end
end

function CLuaGuildMainWndMemberWnd:GetPageCount()
    local count = #self.m_AllGuildMemberApplyInfos
    local rtn = math.floor(count/6) + (count%6>0 and 1 or 0)
    return math.max(1,rtn)
end

function CLuaGuildMainWndMemberWnd:OnPageChange( label, val)
    label.Text = SafeStringFormat3("%d/%d", val, self:GetPageCount())
    if self.m_CurrentIndex == 2 then
        self.m_MemberApplyList:ReloadData(true, true)
    end
end

function CLuaGuildMainWndMemberWnd:OnClickPageChangeBtn(delta)
    local page = self.m_ItemPage + delta
    if page < 0 or page > 3 then return end
    self.m_ItemPage = page
    self:SetHeaderPage(self.m_ItemPage)
    self:OnGuildMemberItemChangePage(self.m_ItemPage)

end

function CLuaGuildMainWndMemberWnd:OnMyGuildInfoReceived()
    self:UpdateMemberNum()
end

function CLuaGuildMainWndMemberWnd:OnEnable( )
    g_ScriptEvent:AddListener("SetMemberExtraNameSuccess", self, "OnSetMemberExtraNameSuccess")
    g_ScriptEvent:AddListener("MyGuildInfoReceived", self, "OnMyGuildInfoReceived")
    g_ScriptEvent:AddListener("GuildInfoReceived", self, "OnGuildInfoReceived")
    g_ScriptEvent:AddListener("SendGuildInfo_GuildMemberApplyInfo", self, "OnSendGuildInfo_GuildMemberApplyInfo")

    g_ScriptEvent:AddListener("RequestOperationInGuildSucceed", self, "OnRequestOperationInGuildSucceed")

    CGuildMgr.Inst:GetGuildMemberInfo()
    CLuaGuildMgr.GetGuildMemberApplyInfo()
    CGuildMgr.Inst:GetForbiddenToSpeakInfo()

    self.m_AllGuildMemberInfos = nil
    self.m_AllGuildMemberHexinInfos = nil
    self.m_AllGuildMemberApplyInfos = nil

    self:UpdateMemberNum()
    self:SetExportButtonVisibility()

    g_ScriptEvent:AddListener("OnGuildForbiddenToSpeakInfoUpdate", self, "OnGuildForbiddenToSpeakInfoUpdate")
    g_ScriptEvent:AddListener("TrySelectGuildMember", self, "OnTrySelectGuildMember")
    g_ScriptEvent:AddListener("RemoveGuildMemberApplyInfoAfterOperation", self, "OnRemoveGuildMemberApplyInfoAfterOperation")
    g_ScriptEvent:AddListener("RemoveGuildMemberInfoAfterOperation", self, "OnRemoveGuildMemberInfoAfterOperation")
    g_ScriptEvent:AddListener("SendGuildMemberInfoAfterOperation", self, "OnSendGuildMemberInfoAfterOperation")
    g_ScriptEvent:AddListener("SendGuildInfo_MyGuildInfoDynamicOnly", self, "OnSendGuildInfo_MyGuildInfoDynamicOnly")
    
    
end
function CLuaGuildMainWndMemberWnd:OnTrySelectGuildMember()
    self:ResetRadioBox()
end
function CLuaGuildMainWndMemberWnd:SetExportButtonVisibility()
    self.exportDataBtn:SetActive(CSwitchMgr.EnableShowMemberExportWnd)
end

function CLuaGuildMainWndMemberWnd:ResetRadioBox( )
    self.m_MemberRadioBox:ChangeTo(0, true)
    local id = CGuildMgr.Inst.m_SelectMemberId
    if self.m_AllGuildMemberInfos ~= nil and id > 0 then
        local row = 0
        do
            local i = 0
            while i < #self.m_AllGuildMemberInfos do
                if self.m_AllGuildMemberInfos[i+1].PlayerId == id then
                    row = i
                    break
                end
                i = i + 1
            end
        end
        CGuildMgr.Inst.m_SelectMemberId = 0
        self.m_MemberList:SetSelectRow(row, true)
        self.m_MemberList:ScrollToRow(row)
    end
end
function CLuaGuildMainWndMemberWnd:UpdateMemberNum( )
    local dynamicInfo = CGuildMgr.Inst.m_GuildDynamicInfo
    if dynamicInfo then
        local onlineNum = dynamicInfo.OnlineMemberNum
        local memberNum = dynamicInfo.MemberNum
        local maxSize = CGuildMgr.Inst:GetMaxSize()
        if onlineNum and memberNum and maxSize then
            self.m_MemberLabel.Text = SafeStringFormat3("%s/%s/%s",tostring(onlineNum),tostring(memberNum),tostring(maxSize))
        else
            self.m_MemberLabel.Text = "0/0/0"
        end

        local onlineTrainee = dynamicInfo.OnlineTraineeNum
        local traineeNum = dynamicInfo.TraineeNum
        local maxSizeTrainee = CGuildMgr.Inst:GetMaxSizeTrainee()
        if onlineTrainee and traineeNum and maxSizeTrainee then
            self.m_StudentsLabel.Text = SafeStringFormat3("%s/%s/%s",tostring(onlineTrainee),tostring(traineeNum),tostring(maxSizeTrainee))
        else
            self.m_StudentsLabel.Text = "0/0/0"
        end
    else
        self.m_MemberLabel.Text = "0/0/0"
        self.m_StudentsLabel.Text = "0/0/0"
    end
end
function CLuaGuildMainWndMemberWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("SetMemberExtraNameSuccess", self, "OnSetMemberExtraNameSuccess")
    g_ScriptEvent:RemoveListener("MyGuildInfoReceived", self, "OnMyGuildInfoReceived")
    g_ScriptEvent:RemoveListener("GuildInfoReceived", self, "OnGuildInfoReceived")
    g_ScriptEvent:RemoveListener("SendGuildInfo_GuildMemberApplyInfo", self, "OnSendGuildInfo_GuildMemberApplyInfo")
    g_ScriptEvent:RemoveListener("RequestOperationInGuildSucceed", self, "OnRequestOperationInGuildSucceed")

    g_ScriptEvent:RemoveListener("OnGuildForbiddenToSpeakInfoUpdate", self, "OnGuildForbiddenToSpeakInfoUpdate")
    g_ScriptEvent:RemoveListener("TrySelectGuildMember", self, "OnTrySelectGuildMember")
    g_ScriptEvent:RemoveListener("RemoveGuildMemberApplyInfoAfterOperation", self, "OnRemoveGuildMemberApplyInfoAfterOperation")
    g_ScriptEvent:RemoveListener("RemoveGuildMemberInfoAfterOperation", self, "OnRemoveGuildMemberInfoAfterOperation")
    g_ScriptEvent:RemoveListener("SendGuildMemberInfoAfterOperation", self, "OnSendGuildMemberInfoAfterOperation")
    g_ScriptEvent:RemoveListener("SendGuildInfo_MyGuildInfoDynamicOnly", self, "OnSendGuildInfo_MyGuildInfoDynamicOnly")
    
end
function CLuaGuildMainWndMemberWnd:OnSendGuildInfo_MyGuildInfoDynamicOnly()
    self:UpdateMemberNum()
end

function CLuaGuildMainWndMemberWnd:OnSetMemberExtraNameSuccess(memberId,extraName)
    --刷新
    for i=1,self.m_MemberList.m_ItemList.Count do
        local item = self.m_MemberList.m_ItemList[i-1]
        if item then
            local row=item.Row

            local info = nil
            if self.m_CurrentIndex == 0 then
                info = self.m_AllGuildMemberInfos[row+1]
            elseif self.m_CurrentIndex==1 then
                info= self.m_AllGuildMemberHexinInfos[row+1]
            end
            if info and info.PlayerId == memberId then
                local m_LabelName = item.transform:Find("LabelName"):GetComponent(typeof(QnLabel))
                local extraName = CLuaGuildMgr.m_ExtraNames[memberId]
                local infoDynamic = CommonDefs.DictGetValue(CGuildMgr.Inst.m_GuildMemberInfo.DynamicInfos, typeof(UInt64), memberId)
                local preName = infoDynamic and infoDynamic.Name or ""
                m_LabelName.Text = extraName and extraName or preName
            end
        end
    end
end
function CLuaGuildMainWndMemberWnd:OnSendGuildMemberInfoAfterOperation(memberId,info,requestType, paramStr)
    self:OnGuildMemberInfoReceived()
end
function CLuaGuildMainWndMemberWnd:OnRemoveGuildMemberInfoAfterOperation(memberId,requestType, paramStr)
    self:OnGuildMemberInfoReceived()
end
function CLuaGuildMainWndMemberWnd:OnRemoveGuildMemberApplyInfoAfterOperation(applyPlayerId,requestType,paramStr)
    --申请列表移除该玩家
    self:OnSendGuildInfo_GuildMemberApplyInfo()
end

function CLuaGuildMainWndMemberWnd:OnRequestOperationInGuildSucceed(args)
    local operationType, paramStr = args[0],args[1]
    if "AcceptApplyAsMember" == operationType 
        or "ExpelMember" == operationType
        or "DismissMember" == operationType
        or "AppointMember" == operationType then
        --刷新成员数量 请求dynamic数据
        CLuaGuildMgr.GetMyGuildInfoDynamicOnly()
    end
    if (("LeaveGuild") == operationType) then
        CUIManager.CloseUI(CUIResources.GuildMainWnd)
    end
end
function CLuaGuildMainWndMemberWnd:SortGuildMemberInfo( t, type)
    if not CGuildMgr.Inst.m_GuildMemberInfo then
        return
    end
    local dynamicInfos = {}
    CommonDefs.DictIterate(CGuildMgr.Inst.m_GuildMemberInfo.DynamicInfos, DelegateFactory.Action_object_object(function (___key, ___value)
        dynamicInfos[___key]=___value
    end))

    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local flag = t==self.m_AllGuildMemberInfos and self.m_GuildMemberSortFlags[type] or self.m_GuildHexinSortFlags[type]
    if type==0 then flag=-1 end

    local function defaultSort(a,b)
        local ret=flag
        local a1,b1 = dynamicInfos[a.PlayerId],dynamicInfos[b.PlayerId]
        if a1.LastOnlineTime < b1.LastOnlineTime then
            ret = flag
        elseif a1.LastOnlineTime > b1.LastOnlineTime then
            ret = -flag
        elseif a.Title < b.Title then
            ret = flag
        elseif a.Title > b.Title then
            ret = -flag
        else
            return a.PlayerId<b.PlayerId
        end
        return ret<0
    end

    local default = type--CommonDefs.ConvertIntToEnum(typeof(EGuildSortType), type)
    if default == 0 then
        table.sort( t, function(a,b)
            -- local ret=flag
            -- local a1,b1 = dynamicInfos[a.PlayerId],dynamicInfos[b.PlayerId]
            if a.PlayerId == myId then
                return true
            elseif b.PlayerId == myId then
                return false
            else
                return defaultSort(a,b)
            end
            -- return ret<0
        end )
    elseif default == 1 then--EGuildSortType.Cls
            table.sort( t, function(a,b)
                local ret=flag
                local a1,b1 = dynamicInfos[a.PlayerId],dynamicInfos[b.PlayerId]
                if a.PlayerId == myId then
                    return true
                elseif b.PlayerId == myId then
                    return false
                elseif a1.Class<b1.Class then
                    ret = flag
                elseif a1.Class>b1.Class then
                    ret = -flag
                else
                    return defaultSort(a,b)
                end
                return ret<0
            end )
    elseif default == 2 then--EGuildSortType.Level
        table.sort( t, function(a,b)
            local ret=flag
            local a1,b1 = dynamicInfos[a.PlayerId],dynamicInfos[b.PlayerId]
            if a.PlayerId == myId then
                return true
            elseif b.PlayerId == myId then
                return false
            elseif a1.Level<b1.Level then
                ret = flag
            elseif a1.Level>b1.Level then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif default == 3 then--EGuildSortType.Title
        table.sort( t, function(a,b)
            local ret=flag
            if a.PlayerId == myId then
                return true
            elseif b.PlayerId == myId then
                return false
            elseif a.Title<b.Title then
                ret = -flag
            elseif a.Title>b.Title then
                ret = flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif default == 4 then--EGuildSortType.Contribution
        table.sort( t, function(a,b)
            local ret=flag
            if a.PlayerId == myId then
                return true
            elseif b.PlayerId == myId then
                return false
            elseif a.ContributionTotal<b.ContributionTotal then
                ret = flag
            elseif a.ContributionTotal>b.ContributionTotal then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif default == 5 then--EGuildSortType.LeagueScore
        table.sort( t, function(a,b)
            local ret=flag
            if a.PlayerId == myId then
                return true
            elseif b.PlayerId == myId then
                return false
            elseif a.GuildLeagueTimes<b.GuildLeagueTimes then
                ret = flag
            elseif a.GuildLeagueTimes>b.GuildLeagueTimes then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif default == 6 then--EGuildSortType.LeaugeTime
        table.sort( t, function(a,b)
            local ret=flag
            if a.PlayerId == myId then
                return true
            elseif b.PlayerId == myId then
                return false
            elseif a.MiscData.JoinGuildLeagueTime<b.MiscData.JoinGuildLeagueTime then
                ret = flag
            elseif a.MiscData.JoinGuildLeagueTime>b.MiscData.JoinGuildLeagueTime then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif default == 7 then--EGuildSortType.ZhanLongScore
        table.sort( t, function(a,b)
            local ret=flag
            if a.PlayerId == myId then
                return true
            elseif b.PlayerId == myId then
                return false
            elseif a.ZhanLongTimes<b.ZhanLongTimes then
                ret = flag
            elseif a.ZhanLongTimes>b.ZhanLongTimes then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif default == 8 then--EGuildSortType.PaoShangScore
        table.sort( t, function(a,b)
            local ret=flag
            if a.PlayerId == myId then
                return true
            elseif b.PlayerId == myId then
                return false
            elseif a.PaoShangTimes<b.PaoShangTimes then
                ret = flag
            elseif a.PaoShangTimes>b.PaoShangTimes then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif default == 9 then--EGuildSortType.JoinTime
        table.sort( t, function(a,b)
            local ret=flag
            if a.PlayerId == myId then
                return true
            elseif b.PlayerId == myId then
                return false
            elseif a.JoinTime<b.JoinTime then
                ret = flag
            elseif a.JoinTime>b.JoinTime then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif default == 10 then--EGuildSortType.OfflineTime
        table.sort( t, function(a,b)
            local ret=flag
            local a1,b1 = dynamicInfos[a.PlayerId],dynamicInfos[b.PlayerId]
            if a.PlayerId == myId then
                return true
            elseif b.PlayerId == myId then
                return false
            elseif a1.LastOnlineTime==0 and b1.LastOnlineTime>0 then
                ret = flag
            elseif a1.LastOnlineTime>0 and b1.LastOnlineTime==0 then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif default == 11 then--EGuildSortType.CurrentScene
        table.sort( t, function(a,b)
            local ret=flag
            local a1,b1 = dynamicInfos[a.PlayerId],dynamicInfos[b.PlayerId]
            if a.PlayerId == myId then
                return true
            elseif b.PlayerId == myId then
                return false
            elseif a1.MapTempId==0 and b1.MapTempId>0 then
                ret = flag
            elseif a1.MapTempId>0 and b1.MapTempId==0 then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif default == 14 then --EGuildSortType.ZiJin
        table.sort( t, function(a,b)
            local ret=flag
            if a.PlayerId == myId then
                return true
            elseif b.PlayerId == myId then
                return false
            elseif a.MiscData.AddGuildSilverTotal<b.MiscData.AddGuildSilverTotal then
                ret = flag
            elseif a.MiscData.AddGuildSilverTotal>b.MiscData.AddGuildSilverTotal then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    end
end
function CLuaGuildMainWndMemberWnd:SortGuildMemberApplyInfo( t, type)
    if t == nil then return end

    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local flag=self.m_GuildApplySortFlags[type]

    local function defaultSort(a,b)
        local ret=flag
        if a.Level < b.Level then
            ret = flag
        elseif a.Level > b.Level then
            ret = -flag
        elseif a.XiuWei < b.XiuWei then
            ret = flag
        elseif a.XiuWei > b.XiuWei then
            ret = -flag
        elseif a.EquipScore < b.EquipScore then
            ret = flag
        elseif a.EquipScore > b.EquipScore then
            ret = -flag
        else
            return a.PlayerId<b.PlayerId
        end
        return ret<0
    end

    local default = type--CommonDefs.ConvertIntToEnum(typeof(EGuildApplySortType), type)

    if default == 1 then--EGuildApplySortType.ClsName
        table.sort( t, function(a,b)
            local ret=flag
            if a.PlayerId == myId then
                return true
            elseif b.PlayerId == myId then
                return false
            elseif a.Class<b.Class then
                ret = flag
            elseif a.Class>b.Class then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif default == 2 then--EGuildApplySortType.Level
            table.sort( t, function(a,b)
                local ret=flag
                if a.PlayerId == myId then
                    return true
                elseif b.PlayerId == myId then
                    return false
                elseif a.Level<b.Level then
                    ret = flag
                elseif a.Level>b.Level then
                    ret = -flag
                else
                    return defaultSort(a,b)
                end
                return ret<0
            end )
    elseif default == 3 then--EGuildApplySortType.XiuWei
        table.sort( t, function(a,b)
            local ret=flag
            if a.PlayerId == myId then
                return true
            elseif b.PlayerId == myId then
                return false
            elseif a.XiuWei<b.XiuWei then
                ret = flag
            elseif a.XiuWei>b.XiuWei then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif default == 4 then--EGuildApplySortType.XiuLian
        table.sort( t, function(a,b)
            local ret=flag
            if a.PlayerId == myId then
                return true
            elseif b.PlayerId == myId then
                return false
            elseif a.XiuLian<b.XiuLian then
                ret = flag
            elseif a.XiuLian>b.XiuLian then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif default == 5 then--EGuildApplySortType.EquipScore
        table.sort( t, function(a,b)
            local ret=flag
            if a.PlayerId == myId then
                return true
            elseif b.PlayerId == myId then
                return false
            elseif a.EquipScore<b.EquipScore then
                ret = flag
            elseif a.EquipScore>b.EquipScore then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif default == 6 then--EGuildApplySortType.Message
        table.sort( t, function(a,b)
            local ret=flag
            local a1 = (a.Application and a.Application~="") and true or false
            local b1 = (b.Application and b.Application~="") and true or false
            if a.PlayerId == myId then
                return true
            elseif b.PlayerId == myId then
                return false
            elseif a1 and not b1 then
                ret = -flag
            elseif not a1 and b1 then
                ret = flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    end
end


function CLuaGuildMainWndMemberWnd:ReloadList( )
    self.m_AllGuildMemberInfos={}
    self.m_AllGuildMemberHexinInfos={}

    if CGuildMgr.Inst.m_GuildMemberInfo ~= nil then
        CommonDefs.DictIterate(CGuildMgr.Inst.m_GuildMemberInfo.Infos, DelegateFactory.Action_object_object(function (___key, ___value)
            local memberId = ___key
            local memberInfo = ___value

            if CommonDefs.DictContains(CGuildMgr.Inst.m_GuildMemberInfo.DynamicInfos, typeof(UInt64), memberId) then
                local infoDynamic = CommonDefs.DictGetValue(CGuildMgr.Inst.m_GuildMemberInfo.DynamicInfos, typeof(UInt64), memberId)
                --只显示在线的
                if self.FilterOnline then
                    if infoDynamic.LastOnlineTime <= 0 then
                        table.insert( self.m_AllGuildMemberInfos, memberInfo )
                        if memberInfo.Title < 30 then
                            table.insert( self.m_AllGuildMemberHexinInfos, memberInfo )
                        end
                    end
                else
                    table.insert( self.m_AllGuildMemberInfos, memberInfo )
                    if memberInfo.Title < 30 then
                        table.insert( self.m_AllGuildMemberHexinInfos, memberInfo )
                    end
                end
            else
                --CLogMgr.LogError("数据错误 " + memberId);
            end
        end))
    end
    self:SortGuildMemberInfo(self.m_AllGuildMemberInfos, 0)
    self:SortGuildMemberInfo(self.m_AllGuildMemberHexinInfos, 0)

    if self.m_CurrentIndex == 0 then
        self.m_MemberList:ReloadData(true, true)
        local id = CGuildMgr.Inst.m_SelectMemberId
        if id > 0 then
            local row = 0
            do
                local i = 0
                while i < #self.m_AllGuildMemberInfos do
                    if self.m_AllGuildMemberInfos[i+1].PlayerId == id then
                        row = i
                        break
                    end
                    i = i + 1
                end
            end
            CGuildMgr.Inst.m_SelectMemberId = 0
            self.m_MemberList:SetSelectRow(row, true)
            self.m_MemberList:ScrollToRow(row)
        end
    end
    if self.m_CurrentIndex == 1 then
        self.m_MemberList:ReloadData(true, true)
    end
    self.m_CurrentPlayerIndex = - 1
end
function CLuaGuildMainWndMemberWnd:OnGuildInfoReceived( args)
    local rpcType = tostring(args[0])
    if (("GuildMemberInfo") == rpcType) then--帮会成员
        self:OnGuildMemberInfoReceived()
    end
end
function CLuaGuildMainWndMemberWnd:OnGuildMemberInfoReceived()
    --缓存一下备注名称
    CLuaGuildMgr.m_ExtraNames={}
    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local info = CommonDefs.DictGetValue(CGuildMgr.Inst.m_GuildMemberInfo.Infos, typeof(UInt64), myId)
    if info then
        CommonDefs.DictIterate(CGuildMgr.Inst.m_GuildMemberInfo.Infos, DelegateFactory.Action_object_object(function (___key, ___value)
            local val = CommonDefs.DictGetValue(___value.MiscData.ExtraNameInfo, typeof(UInt64), myId)
            if val and val~="" then
                CLuaGuildMgr.m_ExtraNames[___value.PlayerId]=val
            end
        end))
    end
    self:ReloadList()
end

function CLuaGuildMainWndMemberWnd:OnSendGuildInfo_GuildMemberApplyInfo()
    self.m_AllGuildMemberApplyInfos = {}
    if CLuaGuildMgr.m_GuildMemberApplyInfo then
        for k,v in pairs(CLuaGuildMgr.m_GuildMemberApplyInfo) do
            table.insert( self.m_AllGuildMemberApplyInfos, v )
        end
    end

    self:SortGuildMemberApplyInfo(self.m_AllGuildMemberApplyInfos, 1)

    self.pageTurnBtn:Reset()
    self.pageTurnBtn:InitValueList(1, self:GetPageCount(),nil)
    self.pageTurnBtn:SetValue(1)

    if self.m_CurrentIndex == 2 then
        self.m_MemberApplyList:ReloadData(true, true)
    end
    self.m_CurrentPlayerIndex = - 1
end

function CLuaGuildMainWndMemberWnd:OnSelectAtRow( row)
    self.m_CurrentPlayerIndex = row
    if self.m_CurrentIndex == 0 or self.m_CurrentIndex == 1 then
        local memberInfo = self:getSelectedMemberInfo()
        if memberInfo ~= nil and memberInfo.PlayerId == CClientMainPlayer.Inst.BasicProp.Id then
            self.m_MemberListBtn3.Visible = true
            -- 选中自己
            self.m_MemberListBtn3.Text = LocalString.GetString("辞职")
            self.m_MemberListBtn3.OnClick = DelegateFactory.Action_QnButton(function(btn) self:ResignBtn_Clicked(btn) end)
        elseif self.m_MemberListBtn3.Visible then
            self:RebindButtonEvents(self.m_CurrentIndex)
        end

        if memberInfo ~= nil and not CClientMainPlayer.IsPlayerId(memberInfo.PlayerId) then
            local pos = Vector3(540 --[[490 + 50]], 0)
            pos = self.transform:TransformPoint(pos)
            CPlayerInfoMgr.ShowPlayerPopupMenu(memberInfo.PlayerId, EnumPlayerInfoContext.GuildMember, EChatPanel.Undefined, nil, nil, pos, AlignType1.Default)
        elseif memberInfo ~= nil and CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id == memberInfo.PlayerId then
            if CGuildMgr.Inst.IsForbiddenToSpeakOpen and CGuildMgr.Inst.CanForbidGuildSpeak and CGuildMgr.Inst:IsForbiddenToSpeak(memberInfo.PlayerId) then
                --选中自己的时候，如果被禁言了，因为不能显示菜单，这个根据策划的意见弹出一个专门的解禁按钮
                local pos = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)

                local t = {
                    PopupMenuItemData(LocalString.GetString("解除禁言"),DelegateFactory.Action_int(function (index) Gac2Gas.RequestEnableGuildSpeak(memberInfo.PlayerId) end),false,nil)
                }
                CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(t,MakeArrayClass(PopupMenuItemData)), pos, CPopupMenuInfoMgr.AlignType.Right)
            end
        end
    elseif self.m_CurrentIndex == 2 then
        local page = self.pageTurnBtn:GetValue()
        self.m_CurrentPlayerIndex = (page - 1) * 6 + row

        if self.m_CurrentPlayerIndex < #self.m_AllGuildMemberApplyInfos then
            local memberInfo = self.m_AllGuildMemberApplyInfos[self.m_CurrentPlayerIndex+1]
            if memberInfo ~= nil and not CClientMainPlayer.IsPlayerId(memberInfo.PlayerId) then
                local pos = Vector3( 540, 0,0)
                pos = self.transform:TransformPoint(pos)
                CPlayerInfoMgr.ShowPlayerPopupMenu(memberInfo.PlayerId, EnumPlayerInfoContext.GuildApplyMember, EChatPanel.Undefined, nil, nil, pos, AlignType1.Default)
            end
        end
    end
end
function CLuaGuildMainWndMemberWnd:Start( )
    self.m_MemberList.m_DataSource = DefaultTableViewDataSource.Create2(
        function()
            return self:NumberOfRows()
        end,
        function(item, index)
            return self:ItemAt(nil,index)
        end)
    self.m_MemberApplyList.m_DataSource = DefaultTableViewDataSource.Create2(
        function()
            return self:NumberOfRows()
        end,
        function(item, index)
            return self:ItemAt(nil,index)
        end)
    local callback = DelegateFactory.Action_int(function(row) self:OnSelectAtRow(row) end)
    self.m_MemberList.OnSelectAtRow = callback
    self.m_MemberApplyList.OnSelectAtRow = callback


    self.m_MemberList.gameObject:SetActive(true)
    self.m_MemberApplyList.gameObject:SetActive(false)

    self.m_MemberRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function (button, index)
        --0 帮会成员，1， 核心成员，2， 申请列表
        self:RebindButtonEvents(index)
        if index <= 1 then
            self.m_MemberList.gameObject:SetActive(true)
            self.m_MemberApplyList.gameObject:SetActive(false)
            self.m_MemberList:ReloadData(true, true)
            self.m_CurrentPlayerIndex = - 1
            self.m_SortRadioBox_Member:ChangeTo(- 1, true)
        else
            self.m_MemberList.gameObject:SetActive(false)
            self.m_MemberApplyList.gameObject:SetActive(true)
            self.m_MemberApplyList:ReloadData(true, true)
            self.m_CurrentPlayerIndex = - 1
            self.m_SortRadioBox_Apply:ChangeTo(- 1, true)
        end
    end)

    self.m_SortRadioBox_Member.OnSelect = DelegateFactory.Action_QnButton_int(function (button, index)
        --0 名称，1,职位，2，等级，3，职业，4，帮贡，4，入帮时间，6，离线时间
        -- self:ReloadDataAfterSort(index + 1)
        local sortButton = TypeAs(button, typeof(CGuildSortButton))
        if self.m_CurrentIndex == 0 then

            self.m_GuildMemberSortFlags[index+1] = - self.m_GuildMemberSortFlags[index+1]
            for i,v in ipairs(self.m_GuildMemberSortFlags) do
                if i~=index+1 then self.m_GuildMemberSortFlags[i] = -1 end
            end

            sortButton:SetSortTipStatus(self.m_GuildMemberSortFlags[index+1] < 0)

            self:SortGuildMemberInfo(self.m_AllGuildMemberInfos, index+1)
            self.m_MemberList:ReloadData(true, true)
            self.m_CurrentPlayerIndex = - 1
        elseif self.m_CurrentIndex == 1 then
            self.m_GuildHexinSortFlags[index+1] = - self.m_GuildHexinSortFlags[index+1]
            for i,v in ipairs(self.m_GuildHexinSortFlags) do
                if i~=index+1 then self.m_GuildHexinSortFlags[i] = -1 end
            end

            sortButton:SetSortTipStatus(self.m_GuildHexinSortFlags[index+1] < 0)

            self:SortGuildMemberInfo(self.m_AllGuildMemberHexinInfos, index+1)
            self.m_MemberList:ReloadData(true, true)
            self.m_CurrentPlayerIndex = - 1
        end
    end)
    self.m_SortRadioBox_Apply.OnSelect = DelegateFactory.Action_QnButton_int(function (button, index)
        local sortButton = TypeAs(button, typeof(CGuildSortButton))
        self.m_GuildApplySortFlags[index+1] = - self.m_GuildApplySortFlags[index+1]
        for i,v in ipairs(self.m_GuildApplySortFlags) do
            if i~=index+1 then
                self.m_GuildApplySortFlags[i] = -1
            end
        end

        sortButton:SetSortTipStatus(self.m_GuildApplySortFlags[index+1] < 0)

        self:SortGuildMemberApplyInfo(self.m_AllGuildMemberApplyInfos, index+1)
        if self.m_CurrentIndex == 2 then
            self.m_MemberApplyList:ReloadData(true, true)
        end
        self.m_CurrentPlayerIndex = - 1
    end)
    self.m_MemberRadioBox:ChangeTo(0, true)
    self:RebindButtonEvents(0)
end

function CLuaGuildMainWndMemberWnd:ReloadDataAfterSort_Apply( type)
    self:SortGuildMemberApplyInfo(self.m_AllGuildMemberApplyInfos, type)
    if self.m_CurrentIndex == 2 then
        self.m_MemberApplyList:ReloadData(true, true)
    end
    self.m_CurrentPlayerIndex = - 1
end

function CLuaGuildMainWndMemberWnd:RebindButtonEvents( index)
    self.m_CurrentIndex = index
    self.m_MemberListBtn1.Text = LocalString.GetString("脱离帮会")
    self.m_MemberListBtn1.OnClick = DelegateFactory.Action_QnButton(function(btn) self:OnMemberListBtn1Click(btn) end)

    self.m_MemberListBtn3.Text = LocalString.GetString("辞职")
    self.m_MemberListBtn3.OnClick = DelegateFactory.Action_QnButton(function(btn) self:ResignBtn_Clicked(btn) end)
    self.m_MemberListBtn3.Visible = false
    self.m_MemberListBtn4.Text = LocalString.GetString("群发消息")
    self.m_MemberListBtn4.OnClick = DelegateFactory.Action_QnButton(function(btn) self:BroadCastBtn_Clicked(btn) end)
end
function CLuaGuildMainWndMemberWnd:OnMemberListBtn1Click(btn)
    local menuList={}
    local callback = DelegateFactory.Action_int(function(index)
        if index == 0 then
            self:LeaveGuildBtn_Clicked(nil)
        elseif index == 1 then
            self:OnClickChangeGuildBtn(nil)
        end
    end)
    table.insert( menuList,PopupMenuItemData(LocalString.GetString("我要离帮"),callback,false,nil ))
    table.insert( menuList,PopupMenuItemData(LocalString.GetString("我要换帮"),callback,false,nil ))

    CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(menuList,MakeArrayClass(PopupMenuItemData)), btn.transform, CPopupMenuInfoMgr.AlignType.Top)
end

function CLuaGuildMainWndMemberWnd:OnClickChangeGuildBtn(btn)
    CLuaGuildMgr.ShowChangeGuild()
end

function CLuaGuildMainWndMemberWnd.LeaveGuildBtn_Clicked(button)
    -- 脱离帮会
    if CClientMainPlayer.Inst == nil then
        return
    end
    local message = g_MessageMgr:FormatMessage("Guild_Quit_Confirm")
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
        Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "LeaveGuild", "", "")
    end), nil, nil, nil, false)
end

function CLuaGuildMainWndMemberWnd:ResignBtn_Clicked(btn)
    if CGuildMgr.Inst:IsNeiWuFuOfficer(0) then
        local menuList={}

        local callback = DelegateFactory.Action_int(function(index)
            if index==0 then
                CGuildMgr.Inst:TryResign()
            elseif index==1 then
                CGuildMgr.Inst:TryResignNeiWuFu()
            end
        end)
        table.insert( menuList,PopupMenuItemData(CGuildMgr.Inst:GetMyTitle(),callback,false,nil ))
        table.insert( menuList,PopupMenuItemData(Guild_Setting.GetData().NeiWuFuOfficerName,callback,false,nil ))
        CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(menuList,MakeArrayClass(PopupMenuItemData)), btn.transform, CPopupMenuInfoMgr.AlignType.Top)
    else
        CGuildMgr.Inst:TryResign()
    end
end
function CLuaGuildMainWndMemberWnd:BroadCastBtn_Clicked(btn)
    CMiniChatMgr.Show(LocalString.GetString("帮会群发"), 30 , CMiniChatMgr.ChatType.Guild, DelegateFactory.Action_MiniChatCallBackInfo(function (data)
        local msg = data.msg
        if not msg or msg=="" then
            return
        end
        local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(msg, true)
        msg = ret.msg
        if msg == nil then
            return
        end

        if self.m_CurrentIndex == 0 then
            -- 全体帮会成员
            local name = "GroupMessage"
            if PlayerSettings.GuildBroadcastMsgToppingEnabled then name = "GroupMessageTopHighline" end
            Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, name, msg, ToStringWrap(ret.shouldBeIgnore))
        elseif self.m_CurrentIndex == 1 then
            -- 核心成员
            local name = "GroupMessageElite"
            if PlayerSettings.GuildBroadcastMsgToppingEnabled then name = "GroupMessageEliteTopHighline" end
            Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, name, msg, ToStringWrap(ret.shouldBeIgnore))
        end
    end))
end
function CLuaGuildMainWndMemberWnd:NumberOfRows( view)
    local num = 0
    -- if self.m_AllGuildMemberInfos == nil then
    --     return num
    -- end

    --0 帮会成员，1， 核心成员，2， 申请列表
    if self.m_CurrentIndex == 0 then
        return self.m_AllGuildMemberInfos and #self.m_AllGuildMemberInfos or 0
    elseif self.m_CurrentIndex == 1 then
        if self.m_AllGuildMemberInfos then
            do
                local i = 0
                while i < #self.m_AllGuildMemberInfos do
                    -- 精英以上的为核心
                    if self.m_AllGuildMemberInfos[i+1].Title < 30 then
                        num = num + 1
                    end
                    i = i + 1
                end
            end
        end
    elseif self.m_CurrentIndex == 2 then
        if self.m_AllGuildMemberApplyInfos == nil then
            return 0
        end

        local page = self.pageTurnBtn:GetValue()
        if page >= 1 and page < self:GetPageCount() then
            num = 6--CGuildMainWndMemberWnd.ItemPerPage
        else
            local count = #self.m_AllGuildMemberApplyInfos % 6--CGuildMainWndMemberWnd.ItemPerPage
            num = count == 0 and 6 or count
        end
    end
    return num
end
function CLuaGuildMainWndMemberWnd:ItemAt( view, row)
    if self.m_AllGuildMemberInfos == nil then
        return nil
    end

    --0 帮会成员，1， 核心成员，2， 申请列表
    if self.m_CurrentIndex == 0 then
        if CGuildMgr.Inst.m_GuildMemberInfo == nil then
            return nil
        end

        local info = self.m_AllGuildMemberInfos[row+1]
        local memberId = info.PlayerId
        if not CommonDefs.DictContains(CGuildMgr.Inst.m_GuildMemberInfo.DynamicInfos, typeof(UInt64), memberId) then
            --CLogMgr.LogError("数据错误 " + memberId);
            return nil
        end

        local item = self.m_MemberList:GetFromPool(0)
        local infoDynamic = CommonDefs.DictGetValue(CGuildMgr.Inst.m_GuildMemberInfo.DynamicInfos, typeof(UInt64), memberId)
        self:InitGuildMemberItem(item.transform,info, infoDynamic)
        local default = row % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite
        item:SetBackgroundTexture(default)
        return item
    elseif self.m_CurrentIndex == 1 then
        if CGuildMgr.Inst.m_GuildMemberInfo == nil then
            return nil
        end

        local info = self.m_AllGuildMemberHexinInfos[row+1]
        local memberId = info.PlayerId
        if not CommonDefs.DictContains(CGuildMgr.Inst.m_GuildMemberInfo.DynamicInfos, typeof(UInt64), memberId) then
            --CLogMgr.LogError("数据错误 " + memberId);
            return nil
        end

        local item = self.m_MemberList:GetFromPool(0)
        local infoDynamic = CommonDefs.DictGetValue(CGuildMgr.Inst.m_GuildMemberInfo.DynamicInfos, typeof(UInt64), memberId)
        self:InitGuildMemberItem(item.transform,info, infoDynamic)

        local extern = row % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite
        item:SetBackgroundTexture(extern)
        return item
    elseif self.m_CurrentIndex == 2 then
        if self.m_AllGuildMemberApplyInfos == nil then
            return nil
        end
        local page = self.pageTurnBtn:GetValue()
        local index = math.max(0,(page - 1) * 6 + row)

        if index < #self.m_AllGuildMemberApplyInfos then
            local item = self.m_MemberApplyList:GetFromPool(0)
            local info = self.m_AllGuildMemberApplyInfos[index+1]
            local ref = row % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite
            item:SetBackgroundTexture(ref)
            self:InitGuildMemberApplyItem(item.transform,info)
            return item
        end
    end

    return nil
end
function CLuaGuildMainWndMemberWnd:getSelectedMemberDynamicInfo( )
    local memberInfo = self:getSelectedMemberInfo()
    if memberInfo ~= nil then
        local playerId = memberInfo.PlayerId
        if CGuildMgr.Inst.m_GuildMemberInfo ~= nil then
            local memberDynamicInfo = CommonDefs.DictGetValue(CGuildMgr.Inst.m_GuildMemberInfo.DynamicInfos, typeof(UInt64), playerId)
            return memberDynamicInfo
        end
    end
    return nil
end
function CLuaGuildMainWndMemberWnd:getSelectedMemberInfo( )
    if self.m_CurrentPlayerIndex >= 0 then
        if self.m_CurrentIndex == 0 then
            return self.m_AllGuildMemberInfos[self.m_CurrentPlayerIndex+1]
        elseif self.m_CurrentIndex == 1 then
            return self.m_AllGuildMemberHexinInfos[self.m_CurrentPlayerIndex+1]
        end
    end
    return nil
end
function CLuaGuildMainWndMemberWnd:getSelectedMemberApplyInfo( )
    if self.m_CurrentPlayerIndex >= 0 then
        if self.m_CurrentIndex == 2 then
            return self.m_AllGuildMemberApplyInfos[self.m_CurrentPlayerIndex+1]
        end
    end
    return nil
end
function CLuaGuildMainWndMemberWnd:LeaveGuildBtn( button)
    -- 脱离帮会
    if CClientMainPlayer.Inst == nil then
        return
    end
    local message = g_MessageMgr:FormatMessage("Guild_Quit_Confirm")
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
        Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "LeaveGuild", "", "")
    end), nil, nil, nil, false)
end

function CLuaGuildMainWndMemberWnd:ClickOutBtn( button)
    local memberInfo = self:getSelectedMemberInfo()
    if memberInfo == nil then
        g_MessageMgr:ShowMessage("CUSTOM_STRING", LocalString.GetString("请选择一位玩家"))
        return
    end

    local memberDynamicInfo = self:getSelectedMemberDynamicInfo()
    local memberName = ""
    if memberDynamicInfo ~= nil then
        memberName = memberDynamicInfo.Name
    end
    -- TODO zzm
    local message = g_MessageMgr:FormatMessage("Guild_Expel_Confirm", memberName)
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
        Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "ExpelMember", tostring(memberInfo.PlayerId), "")
        CGuildMgr.Inst:GetMyGuildInfo()
    end), nil, nil, nil, false)
end

function CLuaGuildMainWndMemberWnd:ClearRequestListBtn( button)
    -- TODO zzm
    local message = g_MessageMgr:FormatMessage("CUSTOM_STRING", LocalString.GetString("是否清空申请列表？"))
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
        Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "CleanApplyList", "", "")
    end), nil, nil, nil, false)
end

function CLuaGuildMainWndMemberWnd:InitGuildMemberApplyItem(transform ,info)
    local m_LabelName = transform:Find("LabelName"):GetComponent(typeof(QnLabel))
    local m_LabelXiuWei = transform:Find("LabelXiuWei"):GetComponent(typeof(QnLabel))
    local m_LabelGrade = transform:Find("LabelGrade"):GetComponent(typeof(QnLabel))
    local clsSprite = transform:Find("Sprite"):GetComponent(typeof(UISprite))
    local m_LabelXiuLian = transform:Find("LabelXiuLian"):GetComponent(typeof(QnLabel))
    local m_LabelEquipScore = transform:Find("LabelEquipScore"):GetComponent(typeof(QnLabel))
    local messageLabel = transform:Find("MessageLabel"):GetComponent(typeof(UILabel))

    m_LabelName.Text = info.Name
    m_LabelXiuWei.Text = NumberComplexToString(info.XiuWei, typeof(Double), "F1")
    m_LabelGrade.Text = info.XianFanStatus>0 and SafeStringFormat3("[c][%s]Lv.%d[-][/c]", Constants.ColorOfFeiSheng, info.Level) or SafeStringFormat3("Lv.%d", info.Level)
    clsSprite.spriteName = Profession.GetIconByNumber(info.Class)
    --在线
    if info.IsOnline > 0 then
        CUICommonDef.SetActive(clsSprite.gameObject, true, true)
    else
        CUICommonDef.SetActive(clsSprite.gameObject, false, true)
    end

    m_LabelXiuLian.Text = tostring(info.XiuLian)
    m_LabelEquipScore.Text = tostring(info.EquipScore)

    messageLabel.text = info.Application
end

function CLuaGuildMainWndMemberWnd:InitGuildMemberItem(transform,info,infoDynamic)
    if info == nil or infoDynamic == nil then
        return
    end

    local clsSprite = transform:Find("LabelName/Sprite"):GetComponent(typeof(UISprite))
    local m_LabelName = transform:Find("LabelName"):GetComponent(typeof(QnLabel))
    local m_LabelGrade = transform:Find("LabelGrade"):GetComponent(typeof(QnLabel))
    local m_LabelOffice = transform:Find("LabelOffice"):GetComponent(typeof(QnLabel))
    local m_LabelContribution = transform:Find("LabelContribution"):GetComponent(typeof(QnLabel))
    local leagueScore = transform:Find("LeagueScore"):GetComponent(typeof(QnLabel))
    local zhanLongScore = transform:Find("ZhanLongScore"):GetComponent(typeof(QnLabel))
    local paoShangScore = transform:Find("PaoShangScore"):GetComponent(typeof(QnLabel))
    local m_LabelJoinTime = transform:Find("LabelJoinTime"):GetComponent(typeof(QnLabel))
    local m_LabelOfflineTime = transform:Find("LabelOfflineTime"):GetComponent(typeof(QnLabel))
    local leagueTime = transform:Find("LeagueTime"):GetComponent(typeof(QnLabel))
    local currentScene = transform:Find("LabelCurrentScene"):GetComponent(typeof(QnLabel))
    local forbiddenIcon = transform:Find("LabelName/ForbiddenIcon").gameObject

    local labelzijin = transform:Find("ZiJinScore"):GetComponent(typeof(QnLabel))

    local extraName = CLuaGuildMgr.m_ExtraNames[info.PlayerId]
    m_LabelName.Text = extraName and extraName or infoDynamic.Name

    m_LabelOffice.Text = GuildDefine.GetOfficeName(info.Title)
    if info.MiscData.IsNeiWuFuOfficer == 1 then
        m_LabelOffice.Text = m_LabelOffice.Text .. ("," .. Guild_Setting.GetData().NeiWuFuOfficerName)
    end
    m_LabelGrade.Text = infoDynamic.XianFanStatus > 0 and SafeStringFormat3("[c][%s]Lv.%d[-][/c]", Constants.ColorOfFeiSheng, infoDynamic.Level) or SafeStringFormat3("Lv.%d", infoDynamic.Level)

    clsSprite.spriteName = Profession.GetIconByNumber(infoDynamic.Class)

    leagueScore.Text = tostring(info.GuildLeagueTimes)

    local minute = (math.floor(info.MiscData.JoinGuildLeagueTime / 60))
    local hour = math.floor(minute / 60)
    if hour > 0 then
        leagueTime.Text =  SafeStringFormat3(LocalString.GetString("%d小时"), hour)
    else
        leagueTime.Text =  SafeStringFormat3(LocalString.GetString("%d分钟"), minute)
    end


    local contributionTime = CServerTimeMgr.ConvertTimeStampToZone8Time(info.LastContributionTime)
    local nowTime = CServerTimeMgr.Inst:GetZone8Time()
    if not CUICommonDef.IsInSameWeek(contributionTime, nowTime) then
        m_LabelContribution.Text = (((info.Contribution .. "/") .. 0) .. "/") .. info.ContributionTotal
    else
        m_LabelContribution.Text = (((info.Contribution .. "/") .. info.ContributionWeek) .. "/") .. info.ContributionTotal
    end

    local zijintime = CServerTimeMgr.ConvertTimeStampToZone8Time(info.MiscData.LastAddGuildSilverTime)
    labelzijin.Text = info.MiscData.AddGuildSilverWeek.."/"..info.MiscData.AddGuildSilverTotal
    if not CUICommonDef.IsInSameWeek(zijintime, nowTime) then
        labelzijin.Text = SafeStringFormat3("%d/%d", 0,info.MiscData.AddGuildSilverTotal)
    else
        labelzijin.Text = SafeStringFormat3("%d/%d", info.MiscData.AddGuildSilverWeek,info.MiscData.AddGuildSilverTotal)
    end

    local zhanlongTime = CServerTimeMgr.ConvertTimeStampToZone8Time(info.LastZhanLongTime)
    if not CUICommonDef.IsInSameWeek(zhanlongTime, nowTime) then
        zhanLongScore.Text = SafeStringFormat3("%d/%d", 0, info.ZhanLongTimes)
    else
        zhanLongScore.Text = SafeStringFormat3("%d/%d", info.WeekZhanLongTimes, info.ZhanLongTimes)
    end

    local paoshangTime = CServerTimeMgr.ConvertTimeStampToZone8Time(info.LastPaoShangTime)
    if not CUICommonDef.IsInSameWeek(paoshangTime, nowTime) then
        paoShangScore.Text = SafeStringFormat3("%d/%d", 0, info.PaoShangTimes)
    else
        paoShangScore.Text = SafeStringFormat3("%d/%d", info.WeekPaoShangTimes, info.PaoShangTimes)
    end

    local dateFormat = "yyyy-MM-dd"
    local joinTime = CServerTimeMgr.ConvertTimeStampToZone8Time(info.JoinTime)
    m_LabelJoinTime.Text = ToStringWrap(joinTime, dateFormat)

    local data = PublicMap_PublicMap.GetData(infoDynamic.MapTempId)
    if data ~= nil then
        currentScene.Text = data.Name
    else
        currentScene.Text = ""
    end

    if infoDynamic.LastOnlineTime <= 0 then
        m_LabelOfflineTime.Text = LocalString.GetString("在线")
        CUICommonDef.SetActive(clsSprite.gameObject, true, true)
    else
        local offlineTime = CServerTimeMgr.ConvertTimeStampToZone8Time(infoDynamic.LastOnlineTime)
        m_LabelOfflineTime.Text = ToStringWrap(offlineTime, dateFormat)
        CUICommonDef.SetActive(clsSprite.gameObject, false, true)
    end

    forbiddenIcon:SetActive(CGuildMgr.Inst:IsForbiddenToSpeak(info.PlayerId))

    self:OnGuildMemberItemChangePage(self.m_ItemPage)
end

function CLuaGuildMainWndMemberWnd:OnGuildMemberItemChangePage(page)
    for i=1,self.m_MemberList.m_ItemList.Count do
        local item = self.m_MemberList.m_ItemList[i-1]
        if item then
            local transform = item.transform
            local m_LabelGrade = transform:Find("LabelGrade"):GetComponent(typeof(QnLabel))
            local m_LabelOffice = transform:Find("LabelOffice"):GetComponent(typeof(QnLabel))
            local m_LabelContribution = transform:Find("LabelContribution"):GetComponent(typeof(QnLabel))
            local leagueScore = transform:Find("LeagueScore"):GetComponent(typeof(QnLabel))
            local zhanLongScore = transform:Find("ZhanLongScore"):GetComponent(typeof(QnLabel))
            local paoShangScore = transform:Find("PaoShangScore"):GetComponent(typeof(QnLabel))
            local m_LabelJoinTime = transform:Find("LabelJoinTime"):GetComponent(typeof(QnLabel))
            local m_LabelOfflineTime = transform:Find("LabelOfflineTime"):GetComponent(typeof(QnLabel))
            local leagueTime = transform:Find("LeagueTime"):GetComponent(typeof(QnLabel))
            local currentScene = transform:Find("LabelCurrentScene"):GetComponent(typeof(QnLabel))

            local zijingo = transform:Find("ZiJinScore").gameObject
            

            m_LabelGrade.gameObject:SetActive(self.m_ItemPage==0)
            m_LabelOffice.gameObject:SetActive(self.m_ItemPage==0)
            m_LabelContribution.gameObject:SetActive(self.m_ItemPage==0)
            leagueScore.gameObject:SetActive(self.m_ItemPage==0)
            leagueTime.gameObject:SetActive(self.m_ItemPage==0)

            zhanLongScore.gameObject:SetActive(self.m_ItemPage==1)
            paoShangScore.gameObject:SetActive(self.m_ItemPage==1)
            zijingo:SetActive(self.m_ItemPage==1)
            m_LabelJoinTime.gameObject:SetActive(self.m_ItemPage==1)

            m_LabelOfflineTime.gameObject:SetActive(self.m_ItemPage==2)
            currentScene.gameObject:SetActive(self.m_ItemPage==2)

        end
    end
end

function CLuaGuildMainWndMemberWnd:OnGuildForbiddenToSpeakInfoUpdate()
    for i=1,self.m_MemberList.m_ItemList.Count do
        local item = self.m_MemberList.m_ItemList[i-1]
            if item then
            local row=item.Row

            local info = nil
            if self.m_CurrentIndex == 0 then
                info = self.m_AllGuildMemberInfos and self.m_AllGuildMemberInfos[row+1]
            elseif self.m_CurrentIndex==1 then
                info= self.m_AllGuildMemberHexinInfos and self.m_AllGuildMemberHexinInfos[row+1]
            end
            if info then
                local forbiddenIcon = item.transform:Find("LabelName/ForbiddenIcon").gameObject
                forbiddenIcon:SetActive(CGuildMgr.Inst:IsForbiddenToSpeak(info.PlayerId))
            end
        end
    end
end
