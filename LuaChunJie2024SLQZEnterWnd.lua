local CMessageTipMgr         = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem      = import "L10.UI.CTipParagraphItem"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local Item_Item              = import "L10.Game.Item_Item"

LuaChunJie2024SLQZEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2024SLQZEnterWnd, "ruleTemplate", "RuleTemplate", GameObject)
RegistChildComponent(LuaChunJie2024SLQZEnterWnd, "ruleTable", "RuleTable", UITable)
RegistChildComponent(LuaChunJie2024SLQZEnterWnd, "rewardTimes", "RewardTimes", UILabel)
RegistChildComponent(LuaChunJie2024SLQZEnterWnd, "rewardTemplate", "RewardTemplate", GameObject)
RegistChildComponent(LuaChunJie2024SLQZEnterWnd, "rewardGrid", "RewardGrid", UIGrid)
RegistChildComponent(LuaChunJie2024SLQZEnterWnd, "startButton", "StartButton", GameObject)
RegistChildComponent(LuaChunJie2024SLQZEnterWnd, "tipButton", "TipButton", GameObject)
--@endregion RegistChildComponent end

RegistClassMember(LuaChunJie2024SLQZEnterWnd, "isMatching")

function LuaChunJie2024SLQZEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.startButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnStartButtonClick()
	end)

	UIEventListener.Get(self.tipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)
    --@endregion EventBind end
end

function LuaChunJie2024SLQZEnterWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncShuangLongQiangZhuApplyResult", self, "OnSyncShuangLongQiangZhuApplyResult")
end

function LuaChunJie2024SLQZEnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncShuangLongQiangZhuApplyResult", self, "OnSyncShuangLongQiangZhuApplyResult")
end

function LuaChunJie2024SLQZEnterWnd:OnSyncShuangLongQiangZhuApplyResult(bMatching, leftTimes, maxTimes)
	self.isMatching = bMatching
	self:UpdateStartButtonStatus()
	self.rewardTimes.text = SafeStringFormat3(LocalString.GetString("今日奖励次数 %d/%d"), leftTimes, maxTimes)
end


function LuaChunJie2024SLQZEnterWnd:Init()
	self:InitRuleTable()
	self:InitReward()
	self.startButton:SetActive(false)
	Gac2Gas.QueryShuangLongQiangZhuApply()
end

function LuaChunJie2024SLQZEnterWnd:InitRuleTable()
	self.ruleTemplate:SetActive(false)

	Extensions.RemoveAllChildren(self.ruleTable.transform)

	local levelTemplate = self.transform:Find("Anchor/Rule/LevelTemplate").gameObject
	levelTemplate:SetActive(false)
	local levelChild = NGUITools.AddChild(self.ruleTable.gameObject, levelTemplate)
	levelChild:SetActive(true)
	local minGrade = ChunJie2024_DoubleDragonSetting.GetData().MinGrade
	levelChild.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("所有成员满%d级"), minGrade)

	local teamTemplate = self.transform:Find("Anchor/Rule/TeamTemplate").gameObject
	teamTemplate:SetActive(false)
	local teamChild = NGUITools.AddChild(self.ruleTable.gameObject, teamTemplate)
	teamChild:SetActive(true)

	local msg =	g_MessageMgr:FormatMessage("CHUNJIE2024_SLQZ_RULE")
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info then
		do
			local i = 0
			while i < info.paragraphs.Count do							-- 根据信息创建并显示段落
				local paragraph = CUICommonDef.AddChild(self.ruleTable.gameObject, self.ruleTemplate) -- 添加段落Go
				paragraph:SetActive(true)
				local tip = CommonDefs.GetComponent_GameObject_Type(paragraph, typeof(CTipParagraphItem))
				tip:Init(info.paragraphs[i], 4294967295)				-- 初始化段落，color = 4294967295（0xFFFFFFFF）
				i = i + 1
			end
		end
    end

	self.ruleTable:Reposition()
end

function LuaChunJie2024SLQZEnterWnd:InitReward()
	self.rewardTemplate:SetActive(false)
	Extensions.RemoveAllChildren(self.rewardGrid.transform)

	local rewardItemIds = ChunJie2024_DoubleDragonSetting.GetData().RewardItemIds
	local count = rewardItemIds.Length
	for i = 1, count do
		local child = NGUITools.AddChild(self.rewardGrid.gameObject, self.rewardTemplate)
		child:SetActive(true)

		local qnReturnAward = child.transform:GetComponent(typeof(CQnReturnAwardTemplate))
        qnReturnAward:Init(Item_Item.GetData(rewardItemIds[i - 1]), 0)
	end
	self.rewardGrid:Reposition()

	self.rewardTimes.text = ""
end

function LuaChunJie2024SLQZEnterWnd:UpdateStartButtonStatus()
	self.startButton:SetActive(true)
	local isInTeam = CClientMainPlayer.Inst.TeamProp:IsInTeam()
	local startStr = isInTeam and LocalString.GetString("组队参与") or LocalString.GetString("开始匹配")
	local str = self.isMatching and LocalString.GetString("匹配中...") or startStr
	self.startButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = str
end

--@region UIEvent

function LuaChunJie2024SLQZEnterWnd:OnStartButtonClick()
	if self.isMatching then
		Gac2Gas.CancelApplyShuangLongQiangZhu()
	else
		Gac2Gas.ApplyShuangLongQiangZhu()
	end
end

function LuaChunJie2024SLQZEnterWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("CHUNJIE2024_SLQZ_TIP")
end

--@endregion UIEvent
