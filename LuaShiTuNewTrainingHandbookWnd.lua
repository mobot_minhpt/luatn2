local CButton = import "L10.UI.CButton"
local UIScrollView = import "UIScrollView"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CShiTuMgr = import "L10.Game.CShiTuMgr"

LuaShiTuNewTrainingHandbookWnd = class()

RegistChildComponent(LuaShiTuNewTrainingHandbookWnd, "m_RadioBox","RadioBox", QnRadioBox)
RegistChildComponent(LuaShiTuNewTrainingHandbookWnd, "m_ExplanationIcon","ExplanationIcon", CButton)
RegistChildComponent(LuaShiTuNewTrainingHandbookWnd, "m_TrainingPlanView","TrainingPlanView", GameObject)
RegistChildComponent(LuaShiTuNewTrainingHandbookWnd, "m_WeeklyHomeworkView","WeeklyHomeworkView", GameObject)
RegistChildComponent(LuaShiTuNewTrainingHandbookWnd, "m_ZunShiZhongDaoView","ZunShiZhongDaoView", GameObject)
RegistChildComponent(LuaShiTuNewTrainingHandbookWnd, "m_ScorllView1","ScorllView1", UIScrollView)
RegistChildComponent(LuaShiTuNewTrainingHandbookWnd, "m_ScorllView2","ScorllView2", UIScrollView)
RegistChildComponent(LuaShiTuNewTrainingHandbookWnd, "m_ShiTuInfoGrid","ShiTuInfoGrid", UIGrid)
RegistChildComponent(LuaShiTuNewTrainingHandbookWnd, "m_ShiTuInfoTemplate","ShiTuInfoTemplate", GameObject)

RegistClassMember(LuaShiTuNewTrainingHandbookWnd,"m_TabWindows")
RegistClassMember(LuaShiTuNewTrainingHandbookWnd,"m_AlertState")
RegistClassMember(LuaShiTuNewTrainingHandbookWnd,"m_ZunShiZhongDaoIndex")
RegistClassMember(LuaShiTuNewTrainingHandbookWnd,"m_ShiTuInfoRadioBox")
RegistClassMember(LuaShiTuNewTrainingHandbookWnd,"m_ShiTuInfoRadioBoxIndex")
RegistClassMember(LuaShiTuNewTrainingHandbookWnd,"m_IsInitShiTuInfo")
RegistClassMember(LuaShiTuNewTrainingHandbookWnd,"m_TabIndex")

function LuaShiTuNewTrainingHandbookWnd:Init()
    self.m_ShiTuInfoTemplate.gameObject:SetActive(false)
    self.m_TabWindows = { self.m_TrainingPlanView , self.m_WeeklyHomeworkView, self.m_ZunShiZhongDaoView }
    self.m_AlertState = {false, false, false}
    self.m_ZunShiZhongDaoIndex = 2
    self:HideRuShiDiZiTrainingPlanView()

    self.m_RadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnTabButtonSelected(btn, index)
    end)
    self:ShowAlertSprite(self.m_AlertState)

    UIEventListener.Get(self.m_ExplanationIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:ShowExplanation()
    end)

    self.m_RadioBox:ChangeTo(0, true)
end

function LuaShiTuNewTrainingHandbookWnd:ShowAlertSprite(alertState)
    for i = 0, self.m_RadioBox.m_RadioButtons.Length - 1 do
        local isShowAlert = alertState[i + 1]
        self.m_RadioBox.m_RadioButtons[i].transform:Find("Normal/AlertSprite").gameObject:SetActive(isShowAlert)
        self.m_RadioBox.m_RadioButtons[i].transform:Find("Highlight/AlertSprite").gameObject:SetActive(isShowAlert)
    end
end

function LuaShiTuNewTrainingHandbookWnd:OnEnable()
    g_ScriptEvent:AddListener("SendNewShiTuWenDaoInfo", self, "OnSendNewShiTuWenDaoInfo")
    g_ScriptEvent:AddListener("ShiTuTrainingHandbook_Alert", self, "OnAlert")
end

function LuaShiTuNewTrainingHandbookWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendNewShiTuWenDaoInfo", self, "OnSendNewShiTuWenDaoInfo")
    g_ScriptEvent:RemoveListener("ShiTuTrainingHandbook_Alert", self, "OnAlert")
    LuaShiTuTrainingHandbookMgr:OnWndClose()
end

function LuaShiTuNewTrainingHandbookWnd:OnSendNewShiTuWenDaoInfo()
    self:InitShiTuInfo()
end

function LuaShiTuNewTrainingHandbookWnd:GetDefaultShiTuInfoRadioBoxIndex()
    local myIndex = 1
    for i = 1, 4 do
        local data = LuaShiTuTrainingHandbookMgr.m_TudiSortedList[i]
        local playerId = data and data.tudiId or 0 
        if LuaShiTuTrainingHandbookMgr.m_IsShowForTeacher then
            if LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.otherPlayerId == playerId then
                myIndex = i
            end
        elseif self.m_IsInitShiTuInfo and not LuaShiTuTrainingHandbookMgr.m_IsShowForTeacher then
            local myPlayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
            if myPlayerId == playerId then
                myIndex = i
            end
        end
    end
    return myIndex
end

function LuaShiTuNewTrainingHandbookWnd:InitShiTuInfo()
    local myPlayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    if self.m_IsInitShiTuInfo then
        local myIndex = self:GetDefaultShiTuInfoRadioBoxIndex()
        if self.m_ShiTuInfoRadioBoxIndex ~= (myIndex - 1) then
            self.m_ShiTuInfoRadioBox:ChangeTo(myIndex - 1, true)
        end
        return
    end
    Extensions.RemoveAllChildren(self.m_ShiTuInfoGrid.transform)
    local titleArr = LuaShiTuTrainingHandbookMgr.m_IsShowForTeacher and 
        {LocalString.GetString("大徒弟"),LocalString.GetString("二徒弟"),LocalString.GetString("三徒弟"),LocalString.GetString("四徒弟")} or 
        {LocalString.GetString("大师兄"),LocalString.GetString("二师兄"),LocalString.GetString("三师兄"),LocalString.GetString("四师弟")}
    if not self.m_ShiTuInfoRadioBox then
        self.m_ShiTuInfoRadioBox = self.m_ShiTuInfoGrid.gameObject:AddComponent(typeof(QnRadioBox))
    end
    self.m_ShiTuInfoRadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), 4)
    local myIndex = 4
    for i = 1, 4 do
        local data = LuaShiTuTrainingHandbookMgr.m_TudiSortedList[i]
        local playerId = data and data.tudiId or 0 
        local gender = data and data.gender or 0 
        local title = titleArr[i]
        if not LuaShiTuTrainingHandbookMgr.m_IsShowForTeacher then
            if myIndex < i then
                title = string.gsub(title, LocalString.GetString("师兄"), LocalString.GetString("师弟"))
            end
            if gender == 1 then
                title = string.gsub(title, LocalString.GetString("师兄"), LocalString.GetString("师姐"))
                title = string.gsub(title, LocalString.GetString("师弟"), LocalString.GetString("师妹"))
            end
        end
        local obj = NGUITools.AddChild(self.m_ShiTuInfoGrid.gameObject, self.m_ShiTuInfoTemplate.gameObject)
        
        local you = obj.transform:Find("You").gameObject
        local titleLabel = obj.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
        local nameLabel = obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        local lvLabel = obj.transform:Find("LvLabel"):GetComponent(typeof(UILabel))
        local myLabel = obj.transform:Find("MyLabel").gameObject
        local portrait = obj.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
        local addBtn = obj.transform:Find("AddBtn").gameObject
        local highlighted = obj.transform:Find("Highlighted").gameObject
        local normal = obj.transform:Find("Normal").gameObject
        
        you.gameObject:SetActive(data and data.isBest)
        titleLabel.text = playerId == 0 and LocalString.GetString("暂无") or title
        nameLabel.text = data and data.tudiName
        lvLabel.text = data and SafeStringFormat3("Lv.%d",data.level) or ""
        lvLabel.color = (data and data.isFeiSheng) and NGUIText.ParseColor24("fe7900", 0) or Color.white
        myLabel.gameObject:SetActive(myPlayerId == playerId)
        if data then
            portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(data.class, data.gender, -1), false)
        end
        portrait.gameObject:SetActive(data ~= nil)
        addBtn.gameObject:SetActive(data == nil and LuaShiTuTrainingHandbookMgr.m_IsShowForTeacher)
        normal.gameObject:SetActive(not addBtn.gameObject.activeSelf)
        titleLabel.gameObject:SetActive(myPlayerId ~= playerId and not addBtn.gameObject.activeSelf) 
        lvLabel.gameObject:SetActive(data and not addBtn.gameObject.activeSelf)
        highlighted.gameObject:SetActive(false)
        UIEventListener.Get(addBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            CShiTuMgr.Inst.CurrentEnterType = CShiTuMgr.ShiTuChooseType.SHI
            local issuccess = LuaShiTuMgr:GetShiTuResultWndCacheData(self.m_IsTudiView and CShiTuMgr.ShiTuChooseType.TU or CShiTuMgr.ShiTuChooseType.SHI)
            if not issuccess then
                CUIManager.ShowUI(CUIResources.ShiTuChooseWnd)
                return
            end
            Gac2Gas.RequestRecommendTuDi(false)
        end)
        if myPlayerId == playerId then
            myIndex = i
        end
        obj.gameObject:SetActive(LuaShiTuTrainingHandbookMgr.m_IsShowForTeacher or (data ~= nil))
        self.m_ShiTuInfoRadioBox.m_RadioButtons[i - 1] = obj:GetComponent(typeof(QnSelectableButton))
        self.m_ShiTuInfoRadioBox.m_RadioButtons[i - 1].OnClick = MakeDelegateFromCSFunction(self.m_ShiTuInfoRadioBox.On_Click, MakeGenericClass(Action1, QnButton),self.m_ShiTuInfoRadioBox)
    end
    self.m_ShiTuInfoGrid:Reposition()
    self.m_ShiTuInfoRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnShiTuInfoRadioBoxSelected(btn, index)
    end)
    self.m_ShiTuInfoRadioBox:ChangeTo(self:GetDefaultShiTuInfoRadioBoxIndex() - 1, true)
    self.m_IsInitShiTuInfo = true
end

function LuaShiTuNewTrainingHandbookWnd:HideRuShiDiZiTrainingPlanView()
    if LuaShiTuTrainingHandbookMgr:IsTudiRuShi() then
        local arr = self.m_RadioBox.m_RadioButtons
        arr[2].gameObject:SetActive(false)
        for i = 0,1 do
            arr[i].m_Label.text = arr[i + 1].m_Label.text
        end
        self.m_RadioBox.m_RadioButtons = Table2Array({self.m_RadioBox.m_RadioButtons[0],self.m_RadioBox.m_RadioButtons[1]}, MakeArrayClass(QnSelectableButton))
        self.m_TabWindows = { self.m_WeeklyHomeworkView, self.m_ZunShiZhongDaoView }
        self.m_ZunShiZhongDaoIndex = 1
    end
end

function LuaShiTuNewTrainingHandbookWnd:OnUpdateShiTuPartnerInfo(baishitime)
    self.m_TimeLabel.text = baishitime
end

function LuaShiTuNewTrainingHandbookWnd:OnTabButtonSelected(btn, index)
    if (not self.m_TabWindows) or (index >= #self.m_TabWindows ) then return end
    for _index ,go in pairs(self.m_TabWindows) do
        go:SetActive(_index == (index + 1))
    end
    self.m_ScorllView1.gameObject:SetActive(index == 0)
    self.m_ScorllView1:ResetPosition()
    self.m_ScorllView2.gameObject:SetActive(index ~= 0)
    self.m_ScorllView2:ResetPosition()
    if self.m_AlertState[index + 1] then
        if index == self.m_ZunShiZhongDaoIndex then
            self.m_AlertState[index + 1] = false
        end
    end
    self:ShowAlertSprite(self.m_AlertState)
    for i = 0, self.m_RadioBox.m_RadioButtons.Length - 1 do
        self.m_RadioBox.m_RadioButtons[i].transform:Find("Normal").gameObject:SetActive(i ~= index)
        self.m_RadioBox.m_RadioButtons[i].transform:Find("Highlight").gameObject:SetActive(i == index)
    end
    self.m_TabIndex = index
end

function LuaShiTuNewTrainingHandbookWnd:OnShiTuInfoRadioBoxSelected(btn, index)
    local data = LuaShiTuTrainingHandbookMgr.m_TudiSortedList[index + 1]
    local playerId = data and data.tudiId or 0 
    local myPlayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    if not LuaShiTuTrainingHandbookMgr.m_IsShowForTeacher and playerId ~= myPlayerId and self.m_ShiTuInfoRadioBoxIndex then
        self.m_ShiTuInfoRadioBox:ChangeTo(self.m_ShiTuInfoRadioBoxIndex, false)
        g_MessageMgr:ShowMessage("ShiTuTrainingHandbookWnd_TuDi_Choose_ShiXiongDi")
        return
    elseif LuaShiTuTrainingHandbookMgr.m_IsShowForTeacher and data and not data.canSelect then
        self.m_ShiTuInfoRadioBox:ChangeTo(self.m_ShiTuInfoRadioBoxIndex, false)
        g_MessageMgr:ShowMessage("SHITU_CULTIVATE_NEED_TUDI_ONLINE")
        return
    end
    self.m_ShiTuInfoRadioBoxIndex = index
    LuaShiTuTrainingHandbookMgr.m_TudiPlayerId = playerId
    g_ScriptEvent:BroadcastInLua("ShiTuTrainingHandbookWnd_ChooseTudiPlayerId")
end

function LuaShiTuNewTrainingHandbookWnd:OnAlert(type, isAlert)
    local index = type - 1
    if LuaShiTuTrainingHandbookMgr:IsTudiRuShi() then
        index = index - 1
    end
    if (index < 0) or (index >= self.m_RadioBox.m_RadioButtons.Length) then return end
    self.m_AlertState[type] = isAlert
    self:ShowAlertSprite(self.m_AlertState)
end

function LuaShiTuNewTrainingHandbookWnd:ShowExplanation()
    g_MessageMgr:ShowMessage("ShiTuWenDaoRewardWnd_ReadMe")
end