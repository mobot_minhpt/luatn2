local CScene = import "L10.Game.CScene"
local EventManager = import "EventManager"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CScheduleMgr = import "L10.Game.CScheduleMgr"
local CClientObjectMgr	= import "L10.Game.CClientObjectMgr"
local CHeadInfoWnd = import "L10.UI.CHeadInfoWnd"

LuaZhouNianQing2021Mgr = {}
-- 周年庆2021护送生辰纲
LuaZhouNianQing2021Mgr.biaoChePosX = nil -- 镖车位置x
LuaZhouNianQing2021Mgr.biaoChePosY = nil -- 镖车位置y
LuaZhouNianQing2021Mgr.biaoCheNaiJiu = nil -- 镖车耐久
LuaZhouNianQing2021Mgr.complateState = nil -- 任务进度
LuaZhouNianQing2021Mgr.biaoCheId = nil -- 头车ID
LuaZhouNianQing2021Mgr.biaoCheObj = nil -- 头车obj

-- 周年庆2021同庆有礼
LuaZhouNianQing2021Mgr.qiFuPoint = 0 -- 累计祈福值
LuaZhouNianQing2021Mgr.rewardPlayers = nil -- 同庆有礼界面相关信息
LuaZhouNianQing2021Mgr.TongQinYouLiMsg = ""

LuaZhouNianQing2021Mgr.chouJiangTimes = 0 --抽奖次数
LuaZhouNianQing2021Mgr.selectdIndex = 0 --选中物品的下标
LuaZhouNianQing2021Mgr.itemInfos = nil --物品信息

LuaZhouNianQing2021Mgr.Appointment = 0
LuaZhouNianQing2021Mgr.Share = 0

LuaZhouNianQing2021Mgr.yaoQingHanInst = false -- 邀请函界面是否已经初始化

function LuaZhouNianQing2021Mgr.ShowTongQingYouLiChouJiangWnd(rewardTimes, randomIndx, str)
    if rewardTimes < 0 then 
        LuaZhouNianQing2021Mgr.chouJiangTimes = 0
    else
        LuaZhouNianQing2021Mgr.chouJiangTimes = rewardTimes
    end
    LuaZhouNianQing2021Mgr.selectdIndex = randomIndx
    LuaZhouNianQing2021Mgr.itemInfos = str
    CUIManager.ShowUI(CLuaUIResources.TongQingYouLiChouJiangWnd)
end

function LuaZhouNianQing2021Mgr.RequestChouJiang()
    if LuaZhouNianQing2021Mgr.chouJiangTimes > 0 then
        Gac2Gas.TongQingYouLi_GetReward();
    end
end


-- 当前是否已经进入生辰纲玩法
function LuaZhouNianQing2021Mgr.IsInPlay()
    LuaZhouNianQing2021Mgr.ClearData()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == ShengChenGang_Setting.GetData().GamePlayId then
            return true
        end
    end

    return false
end

-- 相关数据清空
function LuaZhouNianQing2021Mgr.ClearData()
    LuaZhouNianQing2021Mgr.biaoChePosX = nil -- 镖车位置x
	LuaZhouNianQing2021Mgr.biaoChePosY = nil -- 镖车位置y
	LuaZhouNianQing2021Mgr.biaoCheNaiJiu = nil -- 镖车耐久
	LuaZhouNianQing2021Mgr.complateState = nil -- 任务进度
	LuaZhouNianQing2021Mgr.biaoCheId = nil -- 头车ID
end

-- 更新生辰纲任务界面数据以及头车数据
function LuaZhouNianQing2021Mgr.UpdateShengChenGangPlayInfo(hpPercent, posX, posY, playProgress, biaoCheEngineId)
    LuaZhouNianQing2021Mgr.biaoChePosX = posX
    LuaZhouNianQing2021Mgr.biaoChePosY = posY
    LuaZhouNianQing2021Mgr.biaoCheNaiJiu = hpPercent
    LuaZhouNianQing2021Mgr.complateState = playProgress
    LuaZhouNianQing2021Mgr.biaoCheId = biaoCheEngineId
    LuaZhouNianQing2021Mgr.biaoCheObj = CClientObjectMgr.Inst:GetObject(LuaZhouNianQing2021Mgr.biaoCheId)
    LuaZhouNianQing2021Mgr.UpdateBiaoCheHpBarInfo()
    g_ScriptEvent:BroadcastInLua("ShengChenGangInfoUpdate", hpPercent, posX, posY, playProgress)
end

function LuaZhouNianQing2021Mgr.UpdateBiaoCheHpBarInfo()
    -- 更新车队血条
	if LuaZhouNianQing2021Mgr.biaoCheObj then
		LuaZhouNianQing2021Mgr.biaoCheObj.EnableHpBar = true
		LuaZhouNianQing2021Mgr.biaoCheObj.CustomBottomName = LocalString.GetString("镖车耐久")..LuaZhouNianQing2021Mgr.biaoCheNaiJiu.."%"
		CHeadInfoWnd.Instance:OnClientObjExcluded(LuaZhouNianQing2021Mgr.biaoCheId,false)
		EventManager.BroadcastInternalForLua(EnumEventType.UpdateBiaoCheNaiJiu, {LuaZhouNianQing2021Mgr.biaoCheId, LuaZhouNianQing2021Mgr.biaoCheNaiJiu, 100} )
	end

end

function LuaZhouNianQing2021Mgr.UpdateTongQingYouLiInfo(info)
    LuaZhouNianQing2021Mgr.rewardPlayers = info["RewadPlayers"]
    LuaZhouNianQing2021Mgr.Share = info["RewardCount_Share"]
    LuaZhouNianQing2021Mgr.Appointment = info["RewardCount_Appointment"]

    -- 红点更新
    local ScheduleId = 42030193	-- ScheduleId
    if ScheduleId == nil then return end
    local zhiBoTimeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(ZhouNianQing_ZhiBoSettings.GetData().ZhiBoStartTime) -- 直播时间戳
    if CServerTimeMgr.Inst.timeStamp < zhiBoTimeStamp then -- 直播开始之前，无分享或预约有红点显示
        if LuaZhouNianQing2021Mgr.Share == 0 or LuaZhouNianQing2021Mgr.Appointment == 0 then	
            CScheduleMgr.Inst:SetAlertState(ScheduleId,CScheduleMgr.EnumAlertState.Show,true)
        else 
            CScheduleMgr.Inst:SetAlertState(ScheduleId,CScheduleMgr.EnumAlertState.Clicked,true)
        end
    else
        if LuaZhouNianQing2021Mgr.Share == 0 then	-- 直播开始之后，无分享有红点显示
            CScheduleMgr.Inst:SetAlertState(ScheduleId,CScheduleMgr.EnumAlertState.Show,true)
        else 
            CScheduleMgr.Inst:SetAlertState(ScheduleId,CScheduleMgr.EnumAlertState.Clicked,true)
        end
    end
    g_ScriptEvent:BroadcastInLua("UpdateTongQingYouLiInfo", info)
end

local CCCChatMgr = import "L10.Game.CCCChatMgr"
local CCPlayerMgr = import "L10.Game.CCPlayerMgr"
local CCPlayer = import "CCPlayer"
local CClientCCVideoObj = import "L10.Game.CClientCCVideoObj"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Vector3 = import "UnityEngine.Vector3"
local PlayerSettings = import "L10.Game.PlayerSettings"

function LuaZhouNianQing2021Mgr.StopZhiBo()
    LuaZhouNianQing2021Mgr.OnSceneDestroy()
    LuaZhouNianQing2021Mgr.OnPlayerMove()
    CScene.MainScene.AllowZuoQi = true
end

LuaZhouNianQing2021Mgr.CameraLock = false
LuaZhouNianQing2021Mgr.IsPlaying = false

function LuaZhouNianQing2021Mgr.SyncCCInfo(RemoteChannelInfo)
    CUIManager.CloseUI(CLuaUIResources.CCLiveWnd)
    CScene.MainScene.AllowZuoQi = false
    
    LuaZhouNianQing2021Mgr.OnSceneDestroy()
    LuaZhouNianQing2021Mgr.IsPlaying = true
    
    CCCChatMgr.Inst:InitCurrentStation(RemoteChannelInfo)
    local url = CCCChatMgr.Inst.CCLiveURL

    SoundManager.Inst:StopBGMusic()
    LuaZhouNianQing2021Mgr.AddListener()

    CCPlayerMgr.Inst:Play(url)
    CCPlayerMgr.Inst:Lock("ZNQ_ZHIBO_CONFLICT")
end

function LuaZhouNianQing2021Mgr.AddListener()
    g_ScriptEvent:AddListener("OnCCPlayerBeginPlay",LuaZhouNianQing2021Mgr,"OnCCPlayerBeginPlay")
    g_ScriptEvent:AddListener("GasDisconnect", LuaZhouNianQing2021Mgr, "OnGasDisconnect")
    g_ScriptEvent:AddListener("RenderSceneInit", LuaZhouNianQing2021Mgr, "OnRenderSceneInit")
    g_ScriptEvent:AddListener("MainPlayerMoveStepped", LuaZhouNianQing2021Mgr, "OnPlayerMove")
    g_ScriptEvent:AddListener("OnSoundEnableChanged",LuaZhouNianQing2021Mgr,"OnSoundEnableChanged")
end

function LuaZhouNianQing2021Mgr.RemoveListener()
    g_ScriptEvent:RemoveListener("OnCCPlayerBeginPlay",LuaZhouNianQing2021Mgr,"OnCCPlayerBeginPlay")
    g_ScriptEvent:RemoveListener("GasDisconnect", LuaZhouNianQing2021Mgr, "OnGasDisconnect")
    g_ScriptEvent:RemoveListener("RenderSceneInit", LuaZhouNianQing2021Mgr, "OnRenderSceneInit")
    g_ScriptEvent:RemoveListener("MainPlayerMoveStepped", LuaZhouNianQing2021Mgr, "OnPlayerMove")
    g_ScriptEvent:RemoveListener("OnSoundEnableChanged",LuaZhouNianQing2021Mgr,"OnSoundEnableChanged")
end

function LuaZhouNianQing2021Mgr.OnCCPlayerBeginPlay( )
    LuaZhouNianQing2021Mgr.OnBeginPlay()
end

function LuaZhouNianQing2021Mgr.OnSoundEnableChanged()
    local setting = PlayerSettings.SoundEnabled
    if not CCPlayer.Inst:IsPlaying() then return end
    
    if LuaZhouNianQing2021Mgr.CameraLock then
        CCPlayer.Inst:MuteAudio(false)
    else
        CCPlayer.Inst:MuteAudio(not setting)
    end

end

function LuaZhouNianQing2021Mgr.OnRenderSceneInit()
    if CScene.MainScene and CScene.MainScene.SceneTemplateId ~= 16000224 then
        LuaZhouNianQing2021Mgr.OnSceneDestroy()
    end
end

function LuaZhouNianQing2021Mgr.OnSceneDestroy()
    LuaZhouNianQing2021Mgr.RemoveListener()

    LuaZhouNianQing2021Mgr.OnSoundEnableChanged()--重新设置CC直播是否开启语音

    if LuaZhouNianQing2021Mgr.Objs then
        local len = #LuaZhouNianQing2021Mgr.Objs
        for i = 1, len do
            local obj = LuaZhouNianQing2021Mgr.Objs[i]
            if obj then
                obj.RO:Destroy()
            end
        end
    end
    LuaZhouNianQing2021Mgr.Objs = nil
    
    CCPlayerMgr.Inst:Unlock()
    if CCPlayer.Inst:IsPlaying() then
        CCPlayerMgr.Inst:Stop()
    end
    
    CClientCCVideoObj.Inited = false
    LuaZhouNianQing2021Mgr.IsPlaying = false
    CUIManager.CloseUI(CLuaUIResources.ZhiBoScreenLookWnd)
    CUIManager.CloseUI(CLuaUIResources.CCDanMuWnd)
    SoundManager.Inst:StartBGMusic()
end

function LuaZhouNianQing2021Mgr.OnGasDisconnect()
    LuaZhouNianQing2021Mgr.StopZhiBo()
end

function LuaZhouNianQing2021Mgr.OnPlayerMove()
    
    local t = CameraFollow.Inst.TValue
    if  t <= CameraFollow.s_HideUINearBar or t > CameraFollow.m_HideUIFarBar then 
        return 
    end

    LuaZhouNianQing2021Mgr.CurScreenIndex = LuaZhouNianQing2021Mgr.CheckScreenPos()
    if LuaZhouNianQing2021Mgr.CurScreenIndex > 0 then
        if not CUIManager.IsLoaded(CLuaUIResources.ZhiBoScreenLookWnd) then
            CUIManager.ShowUI(CLuaUIResources.ZhiBoScreenLookWnd)
        end
    else
        CUIManager.CloseUI(CLuaUIResources.ZhiBoScreenLookWnd)
    end
    LuaZhouNianQing2021Mgr.ExitCameraLock()
end

function LuaZhouNianQing2021Mgr.CheckScreenPos()
    if LuaZhouNianQing2021Mgr.Objs == nil then 
        return 0 
    end
    local len = #LuaZhouNianQing2021Mgr.Objs
    local checkpos = CClientMainPlayer.Inst.WorldPos
    local data = ZhouNianQing_ZhiBoSettings.GetData()
    local maxr = data.ViewMaxRadius
    local minr = data.ViewMinRadius
    local ah = data.ViewHalfAngle
    for i=1,len do
        local vobj = LuaZhouNianQing2021Mgr.Objs[i]
        local spos = vobj.RO.Position
        spos.y = checkpos.y
        local dist = Vector3.Distance(spos,checkpos)
        if dist <= maxr and dist >= minr then
            local sforward = vobj.RO.transform.forward
            local dir = Vector3(checkpos.x - spos.x,0,checkpos.z-spos.z)
            local cvalue = Vector3.Dot(sforward,dir)
            if cvalue >= ah then
                return i
            end
        end
    end
    return 0
end

function LuaZhouNianQing2021Mgr.ExitCameraLock()
    if LuaZhouNianQing2021Mgr.CameraLock then--tui
        CameraFollow.Inst:SetFollowObj(CClientMainPlayer.Inst.RO, nil)
        CameraFollow.Inst:EnableZoom(true)
        LuaZhouNianQing2021Mgr.ShowHideUI(true)
        LuaZhouNianQing2021Mgr.CameraLock = false
        LuaZhouNianQing2021Mgr.OnSoundEnableChanged()
        CUIManager.CloseUI(CLuaUIResources.ZhiBoScreenLookWnd)
    end
    CUIManager.CloseUI(CLuaUIResources.CCDanMuWnd)
end

function LuaZhouNianQing2021Mgr.SetUpCamera()

    CameraFollow.Inst:ResetToDefault(true,false)
    CUIManager.ShowUI(CLuaUIResources.CCDanMuWnd)

    local data = ZhouNianQing_ZhiBoSettings.GetData()
    local dist = data.CameraDist
    local height = data.CameraHeight
    local screen = LuaZhouNianQing2021Mgr.Objs[LuaZhouNianQing2021Mgr.CurScreenIndex]
    local ppos = CClientMainPlayer.Inst.RO.Position
    local spos = screen.RO.Position
    spos.y = ppos.y
    local dir = CommonDefs.op_Subtraction_Vector3_Vector3(ppos, spos)
    dir = Vector3.Normalize(dir)
    local lendir = CommonDefs.op_Multiply_Vector3_Single(dir, dist)
    local cpos = CommonDefs.op_Addition_Vector3_Vector3(ppos, lendir)
    cpos.y = cpos.y + height
    
    CameraFollow.Inst:EnableZoom(false)
    CameraFollow.Inst:SetCameraStill(cpos, screen.RO)
    LuaZhouNianQing2021Mgr.ShowHideUI(false)
    LuaZhouNianQing2021Mgr.CameraLock = true
    LuaZhouNianQing2021Mgr.OnSoundEnableChanged()
    
    --停止寻路
    Gas2Gac.TryStopTrack()
end

function LuaZhouNianQing2021Mgr.ShowHideUI(tf)
    local hideExcepttb = {"ThumbnailChatWindow","CCDanMuWnd","ZhiBoScreenLookWnd"}
    local hideExcept = Table2List(hideExcepttb, MakeGenericClass(List, cs_string))
    if tf then
        CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, hideExcept, false, false)
        CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, false, false)
    else
        CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, hideExcept, false, false, false)
        CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, false, false, false)
    end
end

LuaZhouNianQing2021Mgr.Objs = nil
LuaZhouNianQing2021Mgr.CurScreenIndex = 0
LuaZhouNianQing2021Mgr.CameraLock = false

function LuaZhouNianQing2021Mgr.OnBeginPlay()
    if LuaZhouNianQing2021Mgr.Objs then
        local len = #LuaZhouNianQing2021Mgr.Objs
        for i = 1, len do
            local obj = LuaZhouNianQing2021Mgr.Objs[i]
            if obj then
                obj:Init()
            end
        end
        return
    end

    local res = ZhouNianQing_ZhiBoSettings.GetData().ScreenObjRes
    local transforms = ZhouNianQing_ZhiBoSettings.GetData().ScreenTransforms

    LuaZhouNianQing2021Mgr.Objs = {}
    local valuetable = g_LuaUtil:StrSplitAdv(transforms, ";")
    for i = 1, #valuetable do
        local vtable = g_LuaUtil:StrSplitAdv(valuetable[i], ",")
        local obj = CClientCCVideoObj.Create(res, vtable[1], vtable[2], vtable[3], vtable[4], vtable[5])
        LuaZhouNianQing2021Mgr.Objs[i] = obj
    end
    LuaZhouNianQing2021Mgr.OnSoundEnableChanged()
end
