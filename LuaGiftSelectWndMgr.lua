LuaGiftSelectWndMgr = class()

LuaGiftSelectWndMgr.ItemID = nil
function LuaGiftSelectWndMgr.Show(itemid)
    LuaGiftSelectWndMgr.ItemID = itemid
    CUIManager.ShowUI(CLuaUIResources.GiftSelectWnd)
end