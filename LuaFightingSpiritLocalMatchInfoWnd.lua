local UILabel = import "UILabel"
local Gac2Gas2 = import "L10.Game.Gac2Gas"
local CommonDefs = import "L10.Game.CommonDefs"
local CFightingSpiritMatchRecord = import "L10.Game.CFightingSpiritMatchRecord"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"
local CGameReplayMgr = import "L10.Game.CGameReplayMgr"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local CFightingSpiritTeam = import "L10.Game.CFightingSpiritTeam"
local CGroupMemberInfo = import "L10.Game.CGroupMemberInfo"

LuaFightingSpiritLocalMatchInfoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFightingSpiritLocalMatchInfoWnd, "RaceRoot", "RaceRoot", GameObject)
RegistChildComponent(LuaFightingSpiritLocalMatchInfoWnd, "Winner1", "Winner1", GameObject)
RegistChildComponent(LuaFightingSpiritLocalMatchInfoWnd, "Winner2", "Winner2", GameObject)
RegistChildComponent(LuaFightingSpiritLocalMatchInfoWnd, "TitleLabel", "TitleLabel", UILabel)

--@endregion RegistChildComponent end

function LuaFightingSpiritLocalMatchInfoWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

-- self.m_MatchInfo[matchIndex] = 
-- {teamId = {chlgTeamId1, chlgTeamId2}, 
-- teamName = {self:GetChlgTeamName(chlgTeamId1), self:GetChlgTeamName(chlgTeamId2)}, 
-- teamScore = {0, 0}, 
-- winTeamId = EMPTY_STRING, loseTeamId = EMPTY_STRING}

function LuaFightingSpiritLocalMatchInfoWnd:Init()
    Gac2Gas.QueryLocalDouHunPlayWatchInfo()
    Gac2Gas.QueryDouHunStage()
    self:SetDefault()

    local setting = DouHunTan_Setting.GetData()
    local seaon = setting.Season
    self.TitleLabel.text = g_MessageMgr:FormatMessage("DouHun_Local_MatchInfo_Wnd_Desc_Unstart", seaon)
end

function LuaFightingSpiritLocalMatchInfoWnd:OnData(memberData, matchData)
    -- self.m_ChlgPlayMemberInfo, {chlgTeam:GetId(), chlgTeam:GetAllMemberInfoTbl(), chlgTeam:GetLeaderId()})
    for i= 0, memberData.Count-1 do
        local data = memberData[i]

        local teamData = CreateFromClass(CFightingSpiritTeam)
        teamData.TeamID = data[0]

        if data.Count >= 4 then
            teamData.Slogan = data[3]
        end
        
        -- 处理成员信息
        local memberTbl = data[1]
        local memberList = CreateFromClass(MakeGenericClass(List, CGroupMemberInfo))
        for i=0, memberTbl.Count-1 do
            local member = CreateFromClass(CGroupMemberInfo)
            member:LoadFromString(memberTbl[i])
            CommonDefs.ListAdd(memberList, typeof(CGroupMemberInfo), member)
        end
        teamData.MemberList = memberList
        teamData.LeaderID = data[2]

        CFightingSpiritMgr.Instance:SetTeamInfo(teamData)
    end

    -- 保存当前的录像信息
    self.m_MatchRecord = {}
    local serverId = 0
    if CClientMainPlayer.Inst then
        serverId = CClientMainPlayer.Inst:GetMyServerId()
    end

    local matchIndex = DouHunCross_Setting.GetData().MatchIndex

    local winner1 = nil
    local winner2 = nil
    -- 生成数据
    if matchData then
        for i, data in ipairs(matchData) do
            local record = CreateFromClass(CFightingSpiritMatchRecord)
            record.ServerID1 = serverId
            record.ServerName1 = ""
            record.ServerID2 = serverId
            record.ServerName2 = ""

            -- 这个字段最重要 实际是matchIndex 用来确定录像
            record.Stage = serverId * 10 +i
            record.GameIndex = matchIndex
            record.MainRaceIndex = 0

            table.insert(self.m_MatchRecord, record)
        end

        -- 初始化
        for i, data in ipairs(matchData) do
            local item = self.RaceRoot.transform:Find(tostring(i)).gameObject
            self:InitItem(data, item, i)

            -- 设置获胜名称
            local winnerTeam = ""
            if data.winTeamId == data.teamId[2] then
                winnerTeam = data.teamName[2]
            elseif data.winTeamId == data.teamId[1] then
                winnerTeam = data.teamName[1]
            end

            if winnerTeam ~= "" then
                if i == 3 then
                    winner1 = winnerTeam
                elseif i == 5 then
                    winner2 = winnerTeam
                end
            end
        end
    end

    -- 设置名称
    if winner1 and winner2 and CClientMainPlayer.Inst then
        local serverId = CClientMainPlayer.Inst:GetMyServerId()
        local serverName = CClientMainPlayer.Inst:GetMyServerName()

        local nameList = DouHunCross_Setting.GetData().LevelName

        local setting = DouHunTan_Setting.GetData()
        local season = setting.Season
        local data = DouHunCross_GuanWangServer.GetData(serverId)
        if data == nil then
            data = DouHunCross_YingHeServer.GetData(serverId)
        end

        local levelName = nameList[math.ceil(data.Index/8)-1]
        if data then
            self.TitleLabel.text = g_MessageMgr:FormatMessage("DouHun_Local_MatchInfo_Wnd_Desc_Finish", winner1, winner2,
                serverName)
        end
    end
end

function LuaFightingSpiritLocalMatchInfoWnd:SetDefault()
    local defaultNameList = {}
    for i=1, 4 do
        table.insert(defaultNameList, LocalString.GetString("[888888]虚位以待"))
    end

    -- 初始化默认显示内容
    for i=1,2 do
        table.insert(defaultNameList, SafeStringFormat3(LocalString.GetString("[888888]第%s场胜者"), i))
    end
    for i=1,3 do
        table.insert(defaultNameList, SafeStringFormat3(LocalString.GetString("[888888]第%s场败者"), i))
    end
    table.insert(defaultNameList, LocalString.GetString("[888888]第4场胜者"))


    local timeList = {}
    local timeDesc = DouHunTan_Cron.GetData("ChallengeStart").Value
    local _, _, mm1, hh1, dd1, MM1, mm2, hh2, dd2, MM2, mm3, hh3, dd3, MM3 = string.find(timeDesc, "(%d+) (%d+) (%d+) (%d+) %*;(%d+) (%d+) (%d+) (%d+) %*;(%d+) (%d+) (%d+) (%d+) %*")

    table.insert(timeList, SafeStringFormat3( LocalString.GetString("%02d月%02d日 %02d:%02d"), tonumber(MM1), tonumber(dd1), tonumber(hh1), tonumber(mm1)))
    table.insert(timeList, SafeStringFormat3( LocalString.GetString("%02d月%02d日 %02d:%02d"), tonumber(MM2), tonumber(dd2), tonumber(hh2), tonumber(mm2)))
    table.insert(timeList, SafeStringFormat3( LocalString.GetString("%02d月%02d日 %02d:%02d"), tonumber(MM3), tonumber(dd3), tonumber(hh3), tonumber(mm3)))

    -- 设置默认显示
    for i=1,5 do
        local item = self.RaceRoot.transform:Find(tostring(i))
        local teamLabel1 = item.transform:Find("TeamName1"):GetComponent(typeof(UILabel))
        local scoreLabel1 = item.transform:Find("TeamName1/Score"):GetComponent(typeof(UILabel))
        local teamLabel2 = item.transform:Find("TeamName2"):GetComponent(typeof(UILabel))
        local scoreLabel2 = item.transform:Find("TeamName2/Score"):GetComponent(typeof(UILabel))
        local battleIcon = item.transform:Find("BattleIcon").gameObject
        local viewBtn = item.transform:Find("ViewBtn").gameObject

        battleIcon:SetActive(false)
        viewBtn:SetActive(false)
        teamLabel1.text = defaultNameList[i*2-1]
        teamLabel2.text = defaultNameList[i*2]

        scoreLabel1.text = ""
        scoreLabel2.text = ""

        local timeLabel = item:Find("TimeLabel"):GetComponent(typeof(UILabel))
        timeLabel.text = timeList[math.ceil(i/2)]
        local downloadBtn = item.transform:Find("DownloadBtn").gameObject
        local cannotWatchMark = item.transform:Find("CannotWatchMark").gameObject
        local downloadingMark = item.transform:Find("DownloadingMark").gameObject
        local playRecordBtn = item.transform:Find("PlayRecordBtn").gameObject

        cannotWatchMark:SetActive(false)
        playRecordBtn:SetActive(false)
        downloadingMark:SetActive(false)
        downloadBtn:SetActive(false)
    end

    local winner1TeamLabel = self.Winner1.transform:Find("TeamName"):GetComponent(typeof(UILabel))
    local winner1ServerLabel = self.Winner1.transform:Find("ServerName"):GetComponent(typeof(UILabel))
    local winner2TeamLabel = self.Winner2.transform:Find("TeamName"):GetComponent(typeof(UILabel))
    local winner2ServerLabel = self.Winner2.transform:Find("ServerName"):GetComponent(typeof(UILabel))

    winner1TeamLabel.text = LocalString.GetString("[888888]第3场胜者")
    winner2TeamLabel.text = LocalString.GetString("[888888]第5场胜者")

    local serverName = CClientMainPlayer.Inst:GetMyServerName()
    winner1ServerLabel.text = SafeStringFormat3(LocalString.GetString("%s·[fb3ffd]乾"), serverName)
    winner2ServerLabel.text = SafeStringFormat3(LocalString.GetString("%s·[edb743]坤"), serverName)
end

function LuaFightingSpiritLocalMatchInfoWnd:InitItem(data, item, index)
    local teamLabel1 = item.transform:Find("TeamName1"):GetComponent(typeof(UILabel))
    local scoreLabel1 = item.transform:Find("TeamName1/Score"):GetComponent(typeof(UILabel))
    local teamLabel2 = item.transform:Find("TeamName2"):GetComponent(typeof(UILabel))
    local scoreLabel2 = item.transform:Find("TeamName2/Score"):GetComponent(typeof(UILabel))
    local battleIcon = item.transform:Find("BattleIcon").gameObject

    battleIcon:SetActive(false)

    if data.teamName[1] == "" or data.teamName[2] == "" then
        scoreLabel1.gameObject:SetActive(false)
        scoreLabel2.gameObject:SetActive(false)
    else
        scoreLabel1.gameObject:SetActive(true)
        scoreLabel2.gameObject:SetActive(true)
        if data.teamName[1] and data.teamName[1] ~= "" then
            teamLabel1.text = LocalString.TranslateAndFormatText(data.teamName[1])
        end
        if data.teamName[2] and data.teamName[2] ~= "" then
            teamLabel2.text = LocalString.TranslateAndFormatText(data.teamName[2])
        end
    end

    local winnerTeam = ""
    local winnerTeamId = ""
    if data.winTeamId == data.teamId[1] and data.teamId[1] ~= "" then
        -- 队伍1赢了
        if data.teamName[1] and data.teamName[1] ~= "" then
            teamLabel1.text = LocalString.TranslateAndFormatText(data.teamName[1])
        end

        if data.teamName[2] and data.teamName[2] ~= "" then
            teamLabel2.text = "[888888]" .. LocalString.TranslateAndFormatText(data.teamName[2])
        end
        scoreLabel1.text = "[4A8EFF]" .. data.teamScore[1]
        scoreLabel2.text = "[EC7676]" .. data.teamScore[2]

        winnerTeam = LocalString.TranslateAndFormatText(data.teamName[1])
        winnerTeamId = data.teamId[1]
    elseif data.winTeamId == data.teamId[2] and data.teamId[2] ~= "" then
        -- 队伍2赢了
        if data.teamName[1] and data.teamName[1] ~= "" then
            teamLabel1.text = "[888888]" .. LocalString.TranslateAndFormatText(data.teamName[1])
        end
        if data.teamName[2] and data.teamName[2] ~= "" then
            teamLabel2.text = LocalString.TranslateAndFormatText(data.teamName[2])
        end
        scoreLabel2.text = "[4A8EFF]" .. data.teamScore[2]
        scoreLabel1.text = "[EC7676]" .. data.teamScore[1]

        winnerTeam = LocalString.TranslateAndFormatText(data.teamName[2])
        winnerTeamId = data.teamId[2]
    else
        -- 还没打 或者正在打
        if data.teamName[1] and data.teamName[1] ~= "" then
            teamLabel1.text = LocalString.TranslateAndFormatText(data.teamName[1])
        end
        if data.teamName[2] and data.teamName[2] ~= "" then
            teamLabel2.text = LocalString.TranslateAndFormatText(data.teamName[2])
        end
        scoreLabel1.text = ""
        scoreLabel2.text = ""

        -- 是否可以观战
        local viewBtn = item.transform:Find("ViewBtn").gameObject
        
        if data.sceneId ~= nil then
            viewBtn:SetActive(true)
            battleIcon:SetActive(true)

            UIEventListener.Get(viewBtn).onClick = DelegateFactory.VoidDelegate(function()
                Gac2Gas2.RequestWatchLocalDouHun(index)
            end)
        end
    end

    UIEventListener.Get(teamLabel1.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        if data.teamId[1] and data.teamId[1] ~= "" then
            CFightingSpiritMgr.Instance.CurrentTeamID = data.teamId[1]
            CUIManager.ShowUI(CUIResources.FightingSpiritTeamMemberListWnd)
        end
    end)

    UIEventListener.Get(teamLabel2.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        if data.teamId[2] and data.teamId[2] ~= "" then
            CFightingSpiritMgr.Instance.CurrentTeamID = data.teamId[2]
            CUIManager.ShowUI(CUIResources.FightingSpiritTeamMemberListWnd)
        end
    end)
    

    if winnerTeam ~= "" then
        if index == 3 then
            local label = self.Winner1.transform:Find("TeamName"):GetComponent(typeof(UILabel))
            label.text = winnerTeam

            UIEventListener.Get(label.gameObject).onClick = DelegateFactory.VoidDelegate(function()
                if winnerTeamId ~= "" then
                    CFightingSpiritMgr.Instance.CurrentTeamID = winnerTeamId
                    CUIManager.ShowUI(CUIResources.FightingSpiritTeamMemberListWnd)
                end
            end)
        elseif index == 5 then
            local label = self.Winner2.transform:Find("TeamName"):GetComponent(typeof(UILabel))
            label.text = winnerTeam

            UIEventListener.Get(label.gameObject).onClick = DelegateFactory.VoidDelegate(function()
                if winnerTeamId ~= "" then
                    CFightingSpiritMgr.Instance.CurrentTeamID = winnerTeamId
                    CUIManager.ShowUI(CUIResources.FightingSpiritTeamMemberListWnd)
                end
            end)
        end
    end

    -- 录像
    self:RefreshItemIcon(item, index)
end

-- 更新图标状态
function LuaFightingSpiritLocalMatchInfoWnd:RefreshItemIcon(item, index)
    local downloadBtn = item.transform:Find("DownloadBtn").gameObject
    local cannotWatchMark = item.transform:Find("CannotWatchMark").gameObject
    local downloadingMark = item.transform:Find("DownloadingMark").gameObject
    local playRecordBtn = item.transform:Find("PlayRecordBtn").gameObject

    local matchData = self.m_MatchRecord[index]

    UIEventListener.Get(playRecordBtn).onClick = DelegateFactory.VoidDelegate(function()
        -- 播放函数
        CFightingSpiritMgr.Instance:RequestPlayVideo(matchData)
    end)

    local downloadBtn = item.transform:Find("DownloadBtn").gameObject
    -- 下载和播放 调用相同的接口
    UIEventListener.Get(downloadBtn).onClick = DelegateFactory.VoidDelegate(function()
        -- 播放函数
        CFightingSpiritMgr.Instance:RequestPlayVideo(matchData)
    end)

    cannotWatchMark:SetActive(false)
    playRecordBtn:SetActive(false)
    downloadingMark:SetActive(false)
    downloadBtn:SetActive(false)
    
    if CFightingSpiritMgr.Instance:CanPlayThisGame(matchData) and CFightingSpiritMgr.Instance:IsGameWatchReplay(matchData.GameIndex) then
        local info = CFightingSpiritMgr.Instance:GetRecordCCPlayInfo(matchData)
        if info then
            cannotWatchMark:SetActive(false)
            playRecordBtn:SetActive(CGameReplayMgr.Instance:IsFileDownloaded(info.FileName))
            downloadingMark:SetActive(CGameReplayMgr.Instance:IsFileDownloading(info.FileName))
            downloadBtn:SetActive((not CGameReplayMgr.Instance:IsFileDownloading(info.FileName)) and (not CGameReplayMgr.Instance:IsFileDownloaded(info.FileName)))
        end
    end
end

-- 下好了录像
-- 根据文件名来判断是否要更新录像信息
function LuaFightingSpiritLocalMatchInfoWnd:OnVideoFilenameReceive(filename)
    for i, data in ipairs(self.m_MatchRecord) do
        local info = CFightingSpiritMgr.Instance:GetRecordCCPlayInfo(data)
        if info then
            if filename == info.FileName then
                local item = self.RaceRoot.transform:Find(tostring(i)).gameObject
                self:RefreshItemIcon(item, i)
            end
        end
    end
end

function LuaFightingSpiritLocalMatchInfoWnd:OnStage(stage, crossStage, localStage)
    local setting = DouHunTan_Setting.GetData()
    local seaon = setting.Season
    local challengeDate = setting.ChallengeDate

    print(localStage, "localStage")
    if localStage == EnumDouHunLocalStage.eSignUp then
        self.TitleLabel.text = g_MessageMgr:FormatMessage("DouHun_Local_MatchInfo_Wnd_Desc_SignUp", seaon, challengeDate)
    elseif localStage == EnumDouHunLocalStage.eVote then
        self.TitleLabel.text = g_MessageMgr:FormatMessage("DouHun_Local_MatchInfo_Wnd_Desc_Vote", seaon, challengeDate)
    elseif  localStage == EnumDouHunLocalStage.eChlgPrepare then
        self.TitleLabel.text = g_MessageMgr:FormatMessage("DouHun_Local_MatchInfo_Wnd_Desc_Prepare", seaon, challengeDate)
    elseif localStage == EnumDouHunLocalStage.eChlgStart then
        self.TitleLabel.text = g_MessageMgr:FormatMessage("DouHun_Local_MatchInfo_Wnd_Desc_Start", seaon)
    end
end

function LuaFightingSpiritLocalMatchInfoWnd:OnEnable()
    if self.m_UrlDownloadedAction == nil then
        self.m_UrlDownloadedAction = DelegateFactory.Action_uint(function(index)
            for i, data in ipairs(self.m_MatchRecord) do
                local item = self.RaceRoot.transform:Find(tostring(i)).gameObject
                self:RefreshItemIcon(item, i)
            end
        end)
    end

    if self.m_VideoDownloadedAction == nil then
        self.m_VideoDownloadedAction = DelegateFactory.Action_bool_string(function(flag, filename)
            self:OnVideoFilenameReceive(filename)
        end)
    end

    if self.m_VideoDownloadStartAction == nil then
        self.m_VideoDownloadStartAction = DelegateFactory.Action_string(function(filename)
            self:OnVideoFilenameReceive(filename)
        end)
    end

    if self.m_VideoRemovedAction == nil then
        self.m_VideoRemovedAction = DelegateFactory.Action_string(function(filename)
            self:OnVideoFilenameReceive(filename)
        end)
    end

    EventManager.AddListenerInternal(EnumEventType.FightingSpiritMatchRecordURLDownloadComplete, self.m_UrlDownloadedAction)
    EventManager.AddListenerInternal(EnumEventType.GameVideoDownloaded, self.m_VideoDownloadedAction)
    EventManager.AddListenerInternal(EnumEventType.GameVideoDownloadStart, self.m_VideoDownloadStartAction)
    EventManager.AddListenerInternal(EnumEventType.GameVideoRemoved, self.m_VideoRemovedAction)

    g_ScriptEvent:AddListener("QueryLocalDouHunPlayWatchInfoResult", self, "OnData")
    g_ScriptEvent:AddListener("QueryDouHunStageResult", self, "OnStage")
end

function LuaFightingSpiritLocalMatchInfoWnd:OnDisable()
    EventManager.RemoveListenerInternal(EnumEventType.FightingSpiritMatchRecordURLDownloadComplete, self.m_UrlDownloadedAction)
    EventManager.RemoveListenerInternal(EnumEventType.GameVideoDownloaded, self.m_VideoDownloadedAction)
    EventManager.RemoveListenerInternal(EnumEventType.GameVideoDownloadStart, self.m_VideoDownloadStartAction)
    EventManager.RemoveListenerInternal(EnumEventType.GameVideoRemoved, self.m_VideoRemovedAction)

    g_ScriptEvent:RemoveListener("QueryLocalDouHunPlayWatchInfoResult", self, "OnData")
    g_ScriptEvent:RemoveListener("QueryDouHunStageResult", self, "OnStage")
end


--@region UIEvent

--@endregion UIEvent

