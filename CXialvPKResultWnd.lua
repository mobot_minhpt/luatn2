-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CXialvPKResultWenshi = import "L10.UI.CXialvPKResultWenshi"
local CXialvPKResultWnd = import "L10.UI.CXialvPKResultWnd"
local CXialvPKResultWushi = import "L10.UI.CXialvPKResultWushi"
local Debug = import "UnityEngine.Debug"
local DelegateFactory = import "DelegateFactory"
local XialvPKRoundInfo = import "L10.UI.CXialvPKResultWnd+XialvPKRoundInfo"
local XialvPKTeamRoundData = import "L10.Game.XialvPKTeamRoundData"
CXialvPKResultWnd.m_Init_CS2LuaHook = function (this) 

    this.attackerPoint.text = "0"
    this.defendPoint.text = "0"

    this:UpdateXialvPKTeamRoundData(CXialvPKResultWnd.WinTeamList, CXialvPKResultWnd.RoundDataList, CXialvPKResultWnd.TeamDataList)
end
CXialvPKResultWnd.m_UpdateXialvPKTeamRoundData_CS2LuaHook = function (this, winTeamList, roundDataList, teamDataList) 

    CXialvPKResultWnd.WinTeamList = winTeamList
    if roundDataList.Count ~= 0 then
        CXialvPKResultWnd.RoundDataList = roundDataList
    end
    if teamDataList.Count ~= 0 then
        CXialvPKResultWnd.TeamDataList = teamDataList
    end

    local teamLeftId = 0
    local teamRightId = 0

    if teamDataList.Count == 2 then
        this.player1:Init(math.floor(teamDataList[0].leaderId), teamDataList[0].leaderName, teamDataList[0].leaderCls, teamDataList[0].leaderGender, teamDataList[0].leaderGrade)
        this.player2:Init(math.floor(teamDataList[0].xialvId), teamDataList[0].xialvName, teamDataList[0].xialvCls, teamDataList[0].xialvGender, teamDataList[0].xialvGrade)

        this.player3:Init(math.floor(teamDataList[1].leaderId), teamDataList[1].leaderName, teamDataList[1].leaderCls, teamDataList[1].leaderGender, teamDataList[1].leaderGrade)
        this.player4:Init(math.floor(teamDataList[1].xialvId), teamDataList[1].xialvName, teamDataList[1].xialvCls, teamDataList[1].xialvGender, teamDataList[1].xialvGrade)

        teamLeftId = teamDataList[0].teamId
        teamRightId = teamDataList[1].teamId

        this.heartSprite1.gameObject:SetActive(teamDataList[0].isMarried)
        this.heartSprite2.gameObject:SetActive(teamDataList[1].isMarried)
    end

    local team1Score = 0
    local team2Score = 0

    CommonDefs.ListIterate(winTeamList, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        if item == teamLeftId then
            team1Score = team1Score + 1
        elseif item == teamRightId then
            team2Score = team2Score + 1
        else
            Debug.LogError("error")
        end
    end))

    this.attackerPoint.text = tostring(team1Score)
    this.defendPoint.text = tostring(team2Score)

    CommonDefs.ListClear(this.infoList)

    this.isOver = team1Score == 2 or team2Score == 2

    local runingIndex = winTeamList.Count

    if winTeamList.Count == 2 and this.isOver then
        runingIndex = - 1
    end

    do
        local i = 0
        while i < CXialvPKResultWnd.ROUND_MAX do
            local info = CreateFromClass(XialvPKRoundInfo)
            --赢的队伍
            info.round = i + 1
            info.leftData = CommonDefs.ListFind(roundDataList, typeof(XialvPKTeamRoundData), DelegateFactory.Predicate_XialvPKTeamRoundData(function (p) 
                return p.round == (i + 1) and p.teamId == teamLeftId
            end))
            info.rightData = CommonDefs.ListFind(roundDataList, typeof(XialvPKTeamRoundData), DelegateFactory.Predicate_XialvPKTeamRoundData(function (p) 
                return p.round == (i + 1) and p.teamId == teamRightId
            end))
            info.winTeamId = i < winTeamList.Count and winTeamList[i] or - 1
            info.leftTeamId = teamLeftId
            info.rightTeamId = teamRightId
            CommonDefs.ListAdd(this.infoList, typeof(XialvPKRoundInfo), info)
            i = i + 1
        end
    end

    this.tableView.m_DataSource = this
    this.tableView:ReloadData(true, true)
    this.tableView:SetSelectRow(runingIndex, true)
end
CXialvPKResultWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if row < 0 or row >= this.infoList.Count then
        return nil
    end

    local roundInfo = this.infoList[row]
    local isRunning = (roundInfo.round == (CXialvPKResultWnd.WinTeamList.Count + 1)) and not this.isOver
    local notStarted = (roundInfo.round > (CXialvPKResultWnd.WinTeamList.Count + 1))
    if row ~= CXialvPKResultWnd.WENSHI_INDEX then
        local item = TypeAs(this.tableView:GetFromPool(0), typeof(CXialvPKResultWushi))
        item:Init(roundInfo.round, roundInfo.leftData, roundInfo.rightData, roundInfo.winTeamId, roundInfo.leftTeamId, roundInfo.rightTeamId, isRunning, notStarted)
        return item
    else
        local item = TypeAs(this.tableView:GetFromPool(1), typeof(CXialvPKResultWenshi))
        item:Init(roundInfo.round, roundInfo.leftData, roundInfo.rightData, roundInfo.winTeamId, roundInfo.leftTeamId, roundInfo.rightTeamId, isRunning, notStarted)
        return item
    end
end
