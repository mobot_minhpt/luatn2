local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CPayMgr = import "L10.Game.CPayMgr"
local CItem=import "L10.Game.CItem"

LuaSystemRecommendGiftWnd = class()

RegistChildComponent(LuaSystemRecommendGiftWnd, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaSystemRecommendGiftWnd, "BuyButton", "BuyButton", GameObject)
RegistChildComponent(LuaSystemRecommendGiftWnd, "GiftNameLabel", "GiftNameLabel", UILabel)
RegistChildComponent(LuaSystemRecommendGiftWnd, "DiscountLabel", "DiscountLabel", UILabel)
RegistChildComponent(LuaSystemRecommendGiftWnd, "GiftIcon", "GiftIcon", CUITexture)
RegistChildComponent(LuaSystemRecommendGiftWnd, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaSystemRecommendGiftWnd, "OriginalPriceLabel", "OriginalPriceLabel", UILabel)
RegistChildComponent(LuaSystemRecommendGiftWnd, "LimitLabel", "LimitLabel", UILabel)

RegistClassMember(LuaSystemRecommendGiftWnd,"m_RemainTime")
RegistClassMember(LuaSystemRecommendGiftWnd,"m_RecommendGiftCountDownTick")

function LuaSystemRecommendGiftWnd:Awake()
    UIEventListener.Get(self.BuyButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyButtonClick()
    end)
end

function LuaSystemRecommendGiftWnd:Init()
    if not LuaRecommendGiftMgr.CurItemId then
        return
    end
    local item = Item_Item.GetData(LuaRecommendGiftMgr.CurItemId)
    local dynamicItem = Mall_DynamicRmbPackageItem.GetData(LuaRecommendGiftMgr.CurItemId)
    local oriItemId = dynamicItem.RmbPackageItemId
    local mall = Mall_LingYuMallLimit.GetData(oriItemId)
    self.GiftNameLabel.text = item.Name
    self.DescLabel.text = CItem.GetItemDescription(LuaRecommendGiftMgr.CurItemId,true)
    self.GiftIcon:LoadMaterial(dynamicItem.Icon)
    if mall and mall.Discount ~= 0 then
        self.DiscountLabel.text = SafeStringFormat3(LocalString.GetString("%s折"), tostring(mall.Discount))
        local buttonLabel = self.BuyButton.transform:Find("Label"):GetComponent(typeof(UILabel))
        buttonLabel.text = SafeStringFormat3(LocalString.GetString("%s元购买"), tostring(mall.Jade))
        
        local orginalPrice = math.floor(mall.Jade * 10 / mall.Discount) 
        self.OriginalPriceLabel.text = SafeStringFormat3(LocalString.GetString("原价 %s元"), tostring(orginalPrice))

    end
    self.m_RemainTime = math.floor(LuaRecommendGiftMgr.ExpireTime - CServerTimeMgr.Inst.timeStamp)
    if self.m_RemainTime < 0 then
        self.m_RemainTime = 0
    end
    local h = math.floor(self.m_RemainTime / 3600)
    local m = math.floor((self.m_RemainTime - h * 3600)/60)
    local s = self.m_RemainTime - h*3600 - m*60
    if self.CountDownLabel ~= nil then
        self.CountDownLabel.text = SafeStringFormat3("%02d:%02d:%02d", h, m, s)
    end
    self.m_RecommendGiftCountDownTick = CTickMgr.Register(DelegateFactory.Action(function ()
        self.m_RemainTime = math.floor(LuaRecommendGiftMgr.ExpireTime - CServerTimeMgr.Inst.timeStamp)
        if self.m_RemainTime > 0 then
            local h = math.floor(self.m_RemainTime / 3600)
            local m = math.floor((self.m_RemainTime - h * 3600)/60)
            local s = self.m_RemainTime - h*3600 - m*60
            if self.CountDownLabel ~= nil then
                self.CountDownLabel.text = SafeStringFormat3("%02d:%02d:%02d", h, m, s)
            end
        else
            self.CountDownLabel.text = SafeStringFormat3("%02d:%02d:%02d", 0, 0, 0)
            self:CancelRecommendGiftTick()
        end
    end), 1000, ETickType.Loop)
end

function LuaSystemRecommendGiftWnd:CancelRecommendGiftTick()
    if self.m_RecommendGiftCountDownTick then
        invoke(self.m_RecommendGiftCountDownTick)
        self.m_RecommendGiftCountDownTick = nil
    end
end

function LuaSystemRecommendGiftWnd:OnDisable()
    self:CancelRecommendGiftTick()
end

--@region UIEvent
function LuaSystemRecommendGiftWnd:OnBuyButtonClick()
    if not LuaRecommendGiftMgr.CurItemId then
        return
    end
    local itemId = LuaRecommendGiftMgr.CurItemId
    local dynamicData = Mall_DynamicRmbPackageItem.GetData(itemId)
    if not dynamicData then return end

    local oriItemId = dynamicData.RmbPackageItemId
    local lingyuMall = Mall_LingYuMallLimit.GetData(oriItemId)
    if not lingyuMall then return end

    local pid = lingyuMall.RmbPID
    local targetPlayerId = 0
    LuaPayMgr.BuyDynamicRmbPackage(CPayMgr.Inst:PIDConverter(pid), targetPlayerId, itemId)
end
--@endregion
