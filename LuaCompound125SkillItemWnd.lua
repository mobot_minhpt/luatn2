local CItemMgr = import "L10.Game.CItemMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CTickMgr = import "L10.Engine.CTickMgr"
local ETickType = import "L10.Engine.ETickType"
local Animator = import "UnityEngine.Animator"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local State = import "L10.UI.CButton.State"
local CButton = import "L10.UI.CButton"

LuaCompound125SkillItemWnd = class()
RegistClassMember(LuaCompound125SkillItemWnd,"m_GetResourceBtn")
RegistClassMember(LuaCompound125SkillItemWnd,"m_ItemIcon")
RegistClassMember(LuaCompound125SkillItemWnd,"m_CompoundBtn")
RegistClassMember(LuaCompound125SkillItemWnd,"m_BottomLabel")
RegistClassMember(LuaCompound125SkillItemWnd,"m_Progress")
RegistClassMember(LuaCompound125SkillItemWnd,"m_IsForward")
RegistClassMember(LuaCompound125SkillItemWnd,"m_Tick")
RegistClassMember(LuaCompound125SkillItemWnd,"m_FaZhenTexture")
RegistClassMember(LuaCompound125SkillItemWnd,"m_FaZhenFx")
RegistClassMember(LuaCompound125SkillItemWnd,"m_BaoZhaFx")
RegistClassMember(LuaCompound125SkillItemWnd,"m_Animator")
RegistClassMember(LuaCompound125SkillItemWnd,"m_HasEssence")
RegistClassMember(LuaCompound125SkillItemWnd,"m_ResourceEnough")
-- 1:门派挑战 2：关宁 3：帮贡
RegistClassMember(LuaCompound125SkillItemWnd,"m_ResourceType")
RegistClassMember(LuaCompound125SkillItemWnd,"m_CompoundSuccess")
RegistClassMember(LuaCompound125SkillItemWnd,"m_FxName")


function LuaCompound125SkillItemWnd:Init()
    self:InitComponents()
    self:CheckoutResource()
    self:InitStatus()
    self.m_Progress = 0
    self.m_CompoundSuccess = false
    self.m_Animator = nil
    self.m_Tick = nil
    local fxList = {"UI_125jinengfazhen_lanse", "UI_125jinengfazhen_hongse", "UI_125jinengfazhen_zise"}

    local item = Item_Item.GetData(LuaCompound125SkillMgr.m_125SkillItemId)
    if item.NameColor == "COLOR_BLUE" then
        self.m_FxName = fxList[1]
    elseif item.NameColor == "COLOR_RED" then
        self.m_FxName = fxList[2]
    elseif item.NameColor == "COLOR_PURPLE" then
        self.m_FxName = fxList[3]
    else
        self.m_FxName = fxList[1]
    end

    self.m_FaZhenFx.OnLoadFxFinish = DelegateFactory.Action(function()
        self.m_Animator = CommonDefs.GetComponentInChildren_Component_Type(self.m_FaZhenFx.FxRoot, typeof(Animator))
        self:SetAniSpeed(0)
    end)
    self.m_FaZhenFx:LoadFx("fx/ui/prefab/".. self.m_FxName .. ".prefab")

    UIEventListener.Get(self.m_GetResourceBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        CUIManager.CloseUI(CUIResources.ItemAccessListWnd)
        CItemAccessListMgr.Inst:ShowItemAccessInfo(LuaCompound125SkillMgr.m_125EssenceId, false, self.transform, AlignType.Right)
    end)

    UIEventListener.Get(self.m_CompoundBtn.gameObject).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        if isPressed then
            if self:CheckTips(true) then
                self:SetAniSpeed(1)
                self.m_CompoundBtn:SetState(State.Highlighted)
            end
        else
            if self:CheckTips(false) and not self.m_CompoundSuccess then
                g_MessageMgr:ShowMessage("EXCHANGE_125_NO_LONG_PRESS")
                self:SetAniSpeed(-1)
                self.m_CompoundBtn:SetState(State.Normal)
            end
        end

        if self.m_Tick == nil then
            self.m_Tick = CTickMgr.Register(DelegateFactory.Action(function () 
                self:TickUpdate()
            end), 100, ETickType.Loop)
        end
    end)
end

function LuaCompound125SkillItemWnd:SetAniSpeed(speed)
    if self.m_Animator == nil then
        return
    end

    local nt = self.m_Animator:GetCurrentAnimatorStateInfo(0).normalizedTime
    if nt >1 then
        self.m_Animator:Play(self.m_FxName, 0, 1)
    elseif nt<0 then
        self.m_Animator:Play(self.m_FxName, 0, 0)
    end

    self.m_Animator:SetFloat("m_Speed", speed)
end

function LuaCompound125SkillItemWnd:CheckTips(showTips)
    if not self.m_HasEssence then
        if showTips then
            g_MessageMgr:ShowMessage("EXCHANGE_125_NO_ITEM")
        end
        return false
    end

    if not self.m_ResourceEnough then
        if showTips then
            if self.m_ResourceType == 1 then
                g_MessageMgr:ShowMessage("EXCHANGE_125_NO_MPTZ_SCORE")
            elseif self.m_ResourceType == 2 then
                g_MessageMgr:ShowMessage("EXCHANGE_125_NO_GNJC_SCORE")
            elseif self.m_ResourceType == 3 then
                g_MessageMgr:ShowMessage("EXCHANGE_125_NO_BANGGONG")
            end
        end
        return false
    end

    return true
end

function LuaCompound125SkillItemWnd:CheckoutResource()
    local own = 0
    if LuaCompound125SkillMgr.m_125ResourceType == 1 then
        self.m_ResourceType = 1
        own = CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.MPTZ)
    elseif LuaCompound125SkillMgr.m_125ResourceType == 2 then
        self.m_ResourceType = 2
        own = CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.GNJC)
    elseif LuaCompound125SkillMgr.m_125ResourceType == 3 then
        self.m_ResourceType = 3
        own = CClientMainPlayer.Inst.PlayProp.GuildData.Contribution
    end

    if LuaCompound125SkillMgr.m_125ResourceCount <= own then
        self.m_ResourceEnough = true
    else
        self.m_ResourceEnough = false
    end
end

function LuaCompound125SkillItemWnd:TickUpdate()
    if self.m_Animator == nil or self.m_CompoundSuccess then
        return
    end
    local nt = self.m_Animator:GetCurrentAnimatorStateInfo(0).normalizedTime

    if nt>1 then
        self:OnSuccess()
    end    
end

function LuaCompound125SkillItemWnd:OnSuccess()
    self.m_BaoZhaFx:LoadFx("fx/ui/prefab/".. self.m_FxName .. "_baokai.prefab")
    if self.m_ResourceType == 1 then
        Gac2Gas.RequestExchangeItems(43,1)
    elseif self.m_ResourceType == 2 then
        Gac2Gas.RequestExchangeItems(44,1)
    elseif self.m_ResourceType == 3 then
        Gac2Gas.RequestExchange125SkillWithBangGong()
    end
    self.m_CompoundSuccess = true
    self:InitStatus()

    CTickMgr.Register(DelegateFactory.Action(function () 
        CUIManager.CloseUI(CLuaUIResources.Compound125SkillItemWnd)
    end), 5000, ETickType.Once)
end

function LuaCompound125SkillItemWnd:InitComponents()
    self.m_ItemIcon = self.transform:Find("Anchor/ItemIcon"):GetComponent(typeof(CUITexture))
    self.m_CompoundBtn = self.transform:Find("Anchor/CompoundBtn"):GetComponent(typeof(CButton))
    self.m_GetResourceBtn = self.transform:Find("Anchor/GetResourceBtn").gameObject
    self.m_BottomLabel = self.transform:Find("Anchor/BottomLabel"):GetComponent(typeof(UILabel))
    self.m_FaZhenTexture = self.transform:Find("Anchor/Bg"):GetComponent(typeof(CUITexture))
    self.m_FaZhenFx = self.transform:Find("Anchor/FaZhenFx"):GetComponent(typeof(CUIFx))
    self.m_BaoZhaFx = self.transform:Find("Anchor/BaoZhaFx"):GetComponent(typeof(CUIFx))
end

function LuaCompound125SkillItemWnd:InitStatus()
    self.m_HasEssence = true
    if LuaCompound125SkillMgr.m_125EssenceId then
        local bindCount, notBindCount
        bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(LuaCompound125SkillMgr.m_125EssenceId)
        local sumCount = bindCount + notBindCount
        if sumCount > 0 then
            self.m_HasEssence = true
        else
            self.m_HasEssence = false
        end
    end


    local item = Item_Item.GetData(LuaCompound125SkillMgr.m_125EssenceId)
    if self.m_HasEssence then
        self.m_CompoundBtn.gameObject:SetActive(true)
        self.m_GetResourceBtn:SetActive(false)
        if LuaCompound125SkillMgr.m_125SkillItemId then
            self.m_ItemIcon.gameObject:SetActive(true)
            local template = Item_Item.GetData(LuaCompound125SkillMgr.m_125SkillItemId)
            if template ~= nil then
                self.m_ItemIcon:LoadMaterial(template.Icon)
                CUICommonDef.SetActive(self.m_ItemIcon.gameObject, false, true)
            end
        end
        self.m_BottomLabel.text = SafeStringFormat3(LocalString.GetString("长按[ffcb00]注灵[-]为%s注入灵力，本次注入需要消耗%s"), item.Name, "[ffcb00]" ..LuaCompound125SkillMgr.m_125Resource.. "[-]")
    else
        self.m_CompoundBtn.gameObject:SetActive(false)
        self.m_GetResourceBtn:SetActive(true)
        self.m_ItemIcon.gameObject:SetActive(false)
        self.m_BottomLabel.text = SafeStringFormat3(LocalString.GetString("点击添加[ffcb00]%s[-]"), item.Name)
    end

    if self.m_CompoundSuccess then
        local resultItem = Item_Item.GetData(LuaCompound125SkillMgr.m_125SkillItemId)
        self.m_CompoundBtn.gameObject:SetActive(false)
        self.m_GetResourceBtn:SetActive(false)
        self.m_ItemIcon.gameObject:SetActive(true)
        CUICommonDef.SetActive(self.m_ItemIcon.gameObject, true, true)
        self.m_BottomLabel.text = SafeStringFormat3(LocalString.GetString("完成注灵，获得[ffcb00]%s[-]"), resultItem.Name)
    end
end

function LuaCompound125SkillItemWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "InitStatus")
	g_ScriptEvent:AddListener("SetItemAt", self, "InitStatus")
end

function LuaCompound125SkillItemWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "InitStatus")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "InitStatus")
    if self.m_Tick ~= nil then
        self.m_Tick:Invoke()
    end
end
