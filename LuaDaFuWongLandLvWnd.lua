local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CButton = import "L10.UI.CButton"

LuaDaFuWongLandLvWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongLandLvWnd, "LandTexture", "LandTexture", CUITexture)
RegistChildComponent(LuaDaFuWongLandLvWnd, "LandName", "LandName", UILabel)
RegistChildComponent(LuaDaFuWongLandLvWnd, "curlv", "curlv", GameObject)
RegistChildComponent(LuaDaFuWongLandLvWnd, "afterlv", "afterlv", GameObject)
RegistChildComponent(LuaDaFuWongLandLvWnd, "curLabel", "curLabel", UILabel)
RegistChildComponent(LuaDaFuWongLandLvWnd, "AfterLabel", "AfterLabel", UILabel)
RegistChildComponent(LuaDaFuWongLandLvWnd, "MyMoneyLabel", "MyMoneyLabel", UILabel)
RegistChildComponent(LuaDaFuWongLandLvWnd, "BuyBtn", "BuyBtn", CButton)
RegistChildComponent(LuaDaFuWongLandLvWnd, "CostMoney", "CostMoney", UILabel)

RegistClassMember(LuaDaFuWongLandLvWnd, "m_BuyLand")
RegistClassMember(LuaDaFuWongLandLvWnd, "m_LandData")
RegistClassMember(LuaDaFuWongLandLvWnd, "m_CloseBtn")
function LuaDaFuWongLandLvWnd:Awake()
    --@region EventBind: Dont Modify Manually!


	
	UIEventListener.Get(self.BuyBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuyBtnClick()
	end)


    --@endregion EventBind end
	self.m_CloseBtn = self.transform:Find("Bg/CloseButton").gameObject
	UIEventListener.Get(self.m_CloseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseBtnClick()
	end)
end

function LuaDaFuWongLandLvWnd:Init()
	if not LuaDaFuWongMgr.CurLandId then return end
	self.m_BuyLand = LuaDaFuWongMgr.CurLandId
	self.m_LandData = DaFuWeng_Land.GetData(self.m_BuyLand)
	if not self.m_LandData then return end
	local id = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id
	local playerInfo = LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[id]
	if playerInfo then
		self.MyMoneyLabel.text = playerInfo.Money
	else
		self.MyMoneyLabel.text = "0"
	end
	
	self:InitIcon()
	self:InitDes()
	local countDown = DaFuWeng_Countdown.GetData(EnumDaFuWengStage.RoundGridLvUp).Time
	local endFunc = function() CUIManager.CloseUI(CLuaUIResources.DaFuWongLandLvWnd) end
	local durationFunc = function(time) end
	LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.BuyLand,countDown,durationFunc,endFunc)
end

function LuaDaFuWongLandLvWnd:InitLv(view,lv)
	if not view then return end
	for i = 0,view.transform.childCount - 1 do
		local go = view.transform:GetChild(i)
		local highlight = go.transform:Find("highlight").gameObject
        highlight.gameObject:SetActive(true)
		if i >= lv then
			highlight.gameObject:SetActive(false)
		end
		-- if go then
		-- 	go:GetComponent(typeof(UISprite)).spriteName = name
		-- end
	end
end

function LuaDaFuWongLandLvWnd:InitIcon()
	self.LandName.text = self.m_LandData.Name
	self.LandTexture:LoadMaterial(self.m_LandData.Icon)
end

function LuaDaFuWongLandLvWnd:InitDes()
	local landData = LuaDaFuWongMgr.LandInfo and LuaDaFuWongMgr.LandInfo[self.m_BuyLand]

	if landData then
		self.curLabel.text = LuaDaFuWongMgr:GetCutLandCostByLv(self.m_BuyLand,landData.lv)
		self.AfterLabel.text = LuaDaFuWongMgr:GetCutLandCostByLv(self.m_BuyLand,landData.lv + 1)
	else
		self.curLabel.text = self.m_LandData.Cost[0]
		self.AfterLabel.text = self.m_LandData.Cost[1]
	end
	self:InitLv(self.curlv,landData.lv)
	self:InitLv(self.afterlv,landData.lv + 1)
	self.CostMoney.text = LuaDaFuWongMgr.LandInfo[self.m_BuyLand].curLvupPrice
end

--@region UIEvent

function LuaDaFuWongLandLvWnd:OnBuyBtnClick()
	-- RPC
	Gac2Gas.DaFuWengLandLvUp(true)
end
function LuaDaFuWongLandLvWnd:OnCloseBtnClick()
	Gac2Gas.DaFuWengFinishCurStage(EnumDaFuWengStage.RoundGridLvUp)
	CUIManager.CloseUI(CLuaUIResources.DaFuWongLandLvWnd)
end
--@endregion UIEvent

function LuaDaFuWongLandLvWnd:OnStageUpdate(CurStage)
	if CurStage ~= EnumDaFuWengStage.RoundGridLvUp then
        CUIManager.CloseUI(CLuaUIResources.DaFuWongLandLvWnd)
    end
end

function LuaDaFuWongLandLvWnd:OnDisable()
	LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.BuyLand)
	g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
end

function LuaDaFuWongLandLvWnd:OnEnable()
	g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
end
