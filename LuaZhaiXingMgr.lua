
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"
local CJoystickWnd = import "L10.UI.CJoystickWnd"
local CScene = import "L10.Game.CScene"
local CRenderObject = import "L10.Engine.CRenderObject"
local CClientObjectRoot = import "L10.Game.CClientObjectRoot"
local CZhaiXingObject = import "L10.Game.CZhaiXingObject"
local GameObject = import "UnityEngine.GameObject"
local EShadowType = import "L10.Engine.EShadowType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Ray = import "UnityEngine.Ray"
local CommonDefs = import "L10.Game.CommonDefs"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CMainCamera = import "L10.Engine.CMainCamera"
local CPostEffectMgr = import "L10.Engine.CPostEffectMgr"
local CPostProcessingMgr = import "L10.Engine.PostProcessing.CPostProcessingMgr"
local CRainDropEffect = import "L10.Engine.CRainDropEffect"
local SoundManager = import "SoundManager"
local CParticleEffect = import "L10.Game.CParticleEffect"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local NormalCamera = import "L10.Engine.CameraControl.NormalCamera"

LuaZhaiXingMgr = {}

LuaZhaiXingMgr.m_Score = 0
LuaZhaiXingMgr.m_Time  = 0
LuaZhaiXingMgr.m_HistoryBest = 0
LuaZhaiXingMgr.m_FightEndTime = 0
LuaZhaiXingMgr.m_MaxNum= 7
LuaZhaiXingMgr.m_stage = nil
LuaZhaiXingMgr.m_Infos = nil
LuaZhaiXingMgr.m_NextInfoIndex = 0
LuaZhaiXingMgr.m_InZhaiXing = false
LuaZhaiXingMgr.m_MyPos = Vector3.zero
LuaZhaiXingMgr.m_Tick = nil
LuaZhaiXingMgr.m_SpeedTick = nil
LuaZhaiXingMgr.m_Speed = 17
LuaZhaiXingMgr.m_SpeedRate = 1
LuaZhaiXingMgr.m_SpeedUpRate = 1.2
LuaZhaiXingMgr.m_SpeedUpDuration = 10
LuaZhaiXingMgr.m_TotalTime = 180
LuaZhaiXingMgr.m_PrepareTime = 10
LuaZhaiXingMgr.m_objs = {}
LuaZhaiXingMgr.m_LastObj = nil
LuaZhaiXingMgr.m_ComponentScore = 30
LuaZhaiXingMgr.m_BigComponentScore = 36

function LuaZhaiXingMgr:AddListener()
    g_ScriptEvent:RemoveListener("OnZhaiXingObjectStateChange", self, "OnZhaiXingObjectStateChange")
    g_ScriptEvent:AddListener("OnZhaiXingObjectStateChange", self, "OnZhaiXingObjectStateChange")
end
LuaZhaiXingMgr:AddListener()
function LuaZhaiXingMgr:OnZhaiXingObjectStateChange(args)
    local index, infoIndex, key, bHit
    index = args[0]
    infoIndex = args[1]
    key = args[2]
    bHit = args[3]
    local info = self.m_Infos[infoIndex]
    if bHit == true then
        --print("Gac2Gas RequestZhaiXingSuccess", key)
        Gac2Gas.RequestZhaiXingSuccess(key)
        if info.type == 1 then
            self.m_Score = self.m_Score + self.m_ComponentScore
            self:OnPickupReward(index)
            g_ScriptEvent:BroadcastInLua("SyncZhaiXingScore", self.m_Score, self.m_FightEndTime)
        elseif info.type == 2 then
            self:OnGameOver()
        elseif info.type == 3 then
            self.m_Score = self.m_Score + self.m_BigComponentScore
            self:OnPickupReward(index)
            g_ScriptEvent:BroadcastInLua("SyncZhaiXingScore", self.m_Score, self.m_FightEndTime)
        end
    else
        --print("Gac2Gas RequestZhaiXingPassComponent", key)
        Gac2Gas.RequestZhaiXingPassComponent(key)
    end

    self.m_NextInfoIndex = self.m_NextInfoIndex + 1

    if self.m_NextInfoIndex < #self.m_Infos then
        self:ReActiveObject(index, self.m_NextInfoIndex)
    end
end

function LuaZhaiXingMgr:SyncZhaiXingComponent(stage, components)
    self.m_stage = stage
    self.m_Infos = components

    self:InitObjects()
    g_ScriptEvent:BroadcastInLua("SyncZhaiXingComponent")
end

function LuaZhaiXingMgr:ReplyZhaiXing(openStatus)
    g_ScriptEvent:BroadcastInLua("ReplyZhaiXing", openStatus)
end

function LuaZhaiXingMgr:ReplyZhaiXingSuccess(index)
    -- key和index差了10
end

function LuaZhaiXingMgr:ReplyZhaiXingPassComponent(index)
end

function LuaZhaiXingMgr:SyncZhaiXingSpeedUp(index)
    UnRegisterTick(LuaZhaiXingMgr.m_SpeedTick)
    LuaZhaiXingMgr.m_SpeedTick = RegisterTickOnce(function()
        self:SetObjectsSpeedRate(1)
    end, self.m_SpeedUpDuration * 1000)
    self:SetObjectsSpeedRate(self.m_SpeedUpRate)
end

function LuaZhaiXingMgr:SyncZhaiXingScore(score, fightEndTime)
    self.m_Score = score
    self.m_FightEndTime = fightEndTime
    g_ScriptEvent:BroadcastInLua("SyncZhaiXingScore", score, fightEndTime)
end

function LuaZhaiXingMgr:SyncZhaiXingSummary(score, timeUse, historyBest)
    self.m_Score = score
    self.m_Time = timeUse
    self.m_HistoryBest = historyBest
    CPostEffectMgr.Instance:StopUICameraPostEffect()
	CUIManager.ShowUI(CLuaUIResources.ZhaiXingResultWnd)
end

function LuaZhaiXingMgr:InZhaiXing()
    if CScene.MainScene then
        return CScene.MainScene.GamePlayDesignId == 51102267
	end
    return false
end

function LuaZhaiXingMgr:StartZhaiXing()
    self.m_ComponentScore = ZhaiXing_Setting.GetData().ComponentScore
    self.m_BigComponentScore = ZhaiXing_Setting.GetData().BigComponentScore
    self:ProcessZhaiXing(true)
    self:ActiveAllObjects(false)
    CPostEffectMgr.Instance:StartUICameraPostEffect()
    LuaZhaiXingMgr.m_Tick = RegisterTick(function()
        self:Tick()
    end, 100)
end

function LuaZhaiXingMgr:EndZhaiXing()
    self:ProcessZhaiXing(false)
    self:DestroyObjects()
    CPostEffectMgr.Instance:StopUICameraPostEffect()
    UnRegisterTick(LuaZhaiXingMgr.m_Tick)
end

function LuaZhaiXingMgr:ProcessZhaiXing(bStart)
    self.m_InZhaiXing = bStart
    CZhaiXingObject.Speed = self.m_Speed
    self:AdjustOrResetCameraParam(bStart)
    self:HideOrShowUI(bStart)
    self:ToggleOtherSetting(bStart)
    self:PlayOrEndBGM(bStart)
    self:ShowOrHideSceneRoot(bStart)
end

function LuaZhaiXingMgr:Tick()
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.RO then
        local ray = CreateFromClass(Ray, CClientMainPlayer.Inst.RO.transform.position, Vector3.down)

        local bFind, hit = CommonDefs.Physics_Raycast_Ray_RaycastHit_float(ray, self.m_Speed * self.m_SpeedRate * 0.2)
        if bFind then
            self:OnHit(hit)
        end
    end
end

function LuaZhaiXingMgr:OnHit(hit)
    local hitGo = hit.collider.gameObject
    local CZXObj = hitGo.transform.parent:GetComponent(typeof(CZhaiXingObject))

    if CZXObj and CZXObj.m_Enable == true then
        CZXObj:OnCollision()
    end
end

function LuaZhaiXingMgr:AdjustOrResetCameraParam(adjust)
    if CameraFollow.Inst == nil or NormalCamera.Inst == nil then
        return
    end
    local vec3 = CameraFollow.Inst.targetRZY
    local height = 0
    if adjust then
        vec3.x = 0
        vec3.y = 0.01
        vec3.z = 15
        height = 0
    else
        vec3.x = 237.5
        vec3.y = 12
        vec3.z = 9.5
        height = 0
    end
    NormalCamera.Inst.m_DeltaHeight = 0
    CameraFollow.Inst.targetRZY = vec3
    CameraFollow.Inst.RZY = vec3
    CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
    CameraFollow.Inst.m_ForbiddDragOrZoom = adjust
end

function LuaZhaiXingMgr:HideOrShowUI(bHide)
    local bShow = not bHide
    local hideExcepttb = {"Joystick", "SkillButtonBoard"}
    local hideExcept = Table2List(hideExcepttb, MakeGenericClass(List, cs_string))
    if bShow then
        CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, hideExcept, false, false)
        CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, false, false)
    else
        CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, hideExcept, false, false, false)
        CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, false, false, false)
    end
    if CJoystickWnd.Instance then
        CJoystickWnd.Instance:SetMountAndCaptureButtonVisible(bShow)
    end
    if CSkillButtonBoardWnd.Instance then
        CSkillButtonBoardWnd.Instance.switchTargetBtn:SetActive(bShow)
        CSkillButtonBoardWnd.Instance.guajiBtn:SetActive(bShow)
    end
end

function LuaZhaiXingMgr:ToggleOtherSetting(bOpen)
    if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.RO == nil then
        return
    end
    if bOpen then
        CClientMainPlayer.Inst:SetShaderMainPlayerPos()
    end
    CClientMainPlayer.Inst.ShaderPosEnable = bOpen
    CClientMainPlayer.Inst.RO.Selectable = not bOpen
    CClientMainPlayer.Inst.RO.ShadowType = not bOpen and EShadowType.Simple or EShadowType.None

    if bOpen then
        local petEngineIds  = CClientMainPlayer.Inst.PetEngineIds
        local lingShouObj   = CClientMainPlayer.Inst.CurrentLingShou
        if petEngineIds then
            for i = 0, petEngineIds.Count - 1 do 
                local pet = CClientObjectMgr.Inst:GetObject(petEngineIds[i])
                if pet then
                    pet:HideObject()
                end
            end
        end
        if lingShouObj then
            lingShouObj:HideObject()
        end
    end
end

function LuaZhaiXingMgr:InitObjects()
    self.m_MyPos = Vector3(15, 3.2, 17)
    local root = CClientObjectRoot.Inst.Other.gameObject
    self.m_objs = {}
    local count = #self.m_Infos > self.m_MaxNum and self.m_MaxNum or #self.m_Infos
    self.m_NextInfoIndex = count
    for i = 1, count do
        local info = self.m_Infos[i]
		local RO = CRenderObject.CreateRenderObject(root, "ZXObj" .. i)
        local ZXObj = CommonDefs.AddComponent_GameObject_Type(RO.gameObject, typeof(CZhaiXingObject))

        local objectPos = CommonDefs.op_Addition_Vector3_Vector3(Vector3(info.x, self:GetOffsetY(info.z / 100), info.y), self.m_MyPos)
        ZXObj:InitObject(i, RO)
        ZXObj:Spawn(self:GetSpawnFxId(info.type), objectPos, i, info.key, info.type)
        table.insert(self.m_objs, ZXObj)
        if i == count then
            self.m_LastObj = ZXObj
        end
    end

    -- if self.m_InZhaiXing == true then
    --     self:ActiveAllObjects(false)
    -- end
end

function LuaZhaiXingMgr:SyncZhaiXingPlayStart(score)
    self.m_Time = 0
    self.m_Score = score or 0
    if self.m_InZhaiXing == true then
        self:ActiveAllObjects(false)
    end
end

function LuaZhaiXingMgr:ReActiveObject(index, infoIndex)
    if self.m_Infos and self.m_objs then
    local info = self.m_Infos[infoIndex]
    local ZXObj = self.m_objs[index]
    local lastInfo = self.m_Infos[infoIndex-1] or info
        if info and ZXObj then
            local pos = self.m_MyPos
            pos.y = 0
            local objectPos = CommonDefs.op_Addition_Vector3_Vector3(Vector3(info.x, self:GetOffsetYByLast((info.z - lastInfo.z) / 100, self.m_LastObj), info.y), pos)
            ZXObj:Spawn(self:GetSpawnFxId(info.type), objectPos, infoIndex, info.key, info.type)
            ZXObj:StartMove(true)
            self.m_LastObj = ZXObj
        end
    end
end

function LuaZhaiXingMgr:ShowOrHideSceneRoot(isShow)
    local root = (GameObject.Find("SceneRoot") or {}).transform
    if root == nil then
        return
    end

    for i = 0, root.childCount - 1 do
        root:GetChild(i).gameObject:SetActive(isShow)
    end
end

LuaZhaiXingMgr.m_Sound = nil
function LuaZhaiXingMgr:PlayOrEndBGM(isPlay)
    SoundManager.Inst:StopBGMusic()
    if isPlay then
        if self.m_Sound ~= nil then
            SoundManager.Inst:StopSound(self.m_Sound)
            self.m_Sound = nil
        end
        local BGMs = ZhaiXing_Setting.GetData().ZhaiXingBGMs
        self.m_Sound = SoundManager.Inst:PlaySound(self.m_stage == 1 and BGMs[0] or BGMs[1], Vector3.zero, SoundManager.Inst.bgMusicVolume, nil, 0)
    else
        SoundManager.Inst:StopSound(self.m_Sound)
    end
end

function LuaZhaiXingMgr:ActiveAllObjects(animation)
    if self.m_objs then
        for i = 1, #self.m_objs do
            self.m_objs[i]:StartMove(animation)
        end
    end
end

function LuaZhaiXingMgr:DisableAllObjects()
    if self.m_objs then
        for i = 1, #self.m_objs do
            self.m_objs[i].m_Enable = false
            self.m_objs[i].m_Inited = false
        end
    end
end

function LuaZhaiXingMgr:DestroyObjects()
    for i = 1, #self.m_objs do
        GameObject.Destroy(self.m_objs[i].gameObject)
    end
    self.m_objs = {}
end

function LuaZhaiXingMgr:SetObjectsSpeedRate(speedRate)
    CZhaiXingObject.Speed = self.m_Speed * speedRate
    self.m_SpeedRate = speedRate
    g_ScriptEvent:BroadcastInLua("ZhaiXingSpeedRateChange")
end

function LuaZhaiXingMgr:GetSpawnFxId(type)
    if type == 1 then 
        return self.m_stage == 1 and 88802812 or 88802813 
    elseif type == 3 then
        return self.m_stage == 1 and 88802898 or 88802899 
    else
        return math.random(1, 8) + 88802801
    end
end

function LuaZhaiXingMgr:GetExplosionFxId()
    return self.m_stage == 1 and 88802810 or 88802811
end

function LuaZhaiXingMgr:OnPickupReward(index)
    local ZCXObj = self.m_objs[index]
    CEffectMgr.Inst:AddWorldPositionFX(self:GetExplosionFxId(), ZCXObj.transform.position, 0, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
    if self.m_stage == 2 then
        self:PlayRainyEffect()
    end
end

function LuaZhaiXingMgr:GetOffsetY(progress)
    return -(self.m_TotalTime  * (progress - self.m_Infos[1].z / 100) + self.m_PrepareTime) * self.m_Speed
end

function LuaZhaiXingMgr:GetOffsetYByLast(progressOffset, lastObj)
    return -(self.m_TotalTime  * progressOffset) * self.m_Speed + lastObj.transform.position.y
end

function LuaZhaiXingMgr:ChangeColor(color)
    CZhaiXingObject.changeAllObjColor(color)
    if CMainCamera.Main then
        local nColor = Color(color.r, color.g, color.b, 0)
        CMainCamera.Main.backgroundColor = nColor
    end
end

LuaZhaiXingMgr.m_RaindropTweener = nil

function LuaZhaiXingMgr:PlayRainyEffect()

end

function LuaZhaiXingMgr:OnGameOver()
    self:DisableAllObjects()
    self:HideMainPlayerFx()
end

function LuaZhaiXingMgr:HideMainPlayerFx()
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.RO then
        local RO = CClientMainPlayer.Inst.RO

        CParticleEffect.s_KeepParticleAlpha = false
        CommonDefs.DictIterate(RO.m_FXTable, DelegateFactory.Action_object_object(function(key,val)
            if val.Id == 88802762 then
                -- local colors = CreateFromClass(MakeArrayClass(Color), 1)
                -- colors[0] = Color.clear
                -- val.m_FX.ParticleColors = colors

                local effectCount = val.m_FX.m_Effects.Length
                for i = 0, effectCount - 1 do
                    local effect = val.m_FX.m_Effects[i]
                    if TypeIs(effect, typeof(CParticleEffect)) then
                        effect:ProcessColor(Color.clear)
                    end
                end
                --val.m_FX:Play()
            end
        end))
        CParticleEffect.s_KeepParticleAlpha = true
    end
end
