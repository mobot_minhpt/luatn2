local LocalString = import "LocalString"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
--local KeJu_Setting = import "L10.Game.KeJu_Setting"

LuaDanMuMgr = {}
LuaDanMuMgr.Data = {}
LuaDanMuMgr.newData = {}
LuaDanMuMgr.PlayerNameDict = {}
LuaDanMuMgr.DanMuRecord = {}
LuaDanMuMgr.RefreshTick = nil
LuaDanMuMgr.Recording = false
function LuaDanMuMgr:SendDanMu(_danmu, _player_id)
    table.insert(LuaDanMuMgr.Data, {playerId = _player_id, data = _danmu})
    table.insert(LuaDanMuMgr.newData, {playerId = _player_id, data = _danmu})
    --检查是否存有该玩家名字， 若没有则需请求服务器
    if(LuaDanMuMgr.PlayerNameDict[_player_id])then
    else
        LuaDanMuMgr.PlayerNameDict[_player_id] = "ID".._player_id
        Gac2Gas.RequestPlayerName(_player_id)
    end
end

function LuaDanMuMgr:GetPlayerName(_player_id)

    local trueId = tonumber(_player_id)
    local return_val = LuaDanMuMgr.PlayerNameDict[trueId] and LuaDanMuMgr.PlayerNameDict[trueId] or ("ID".._player_id)
    return return_val
end

function LuaDanMuMgr:SyncDanMu()
    g_ScriptEvent:BroadcastInLua("OnSyncDanMu", LuaDanMuMgr.newData)
    LuaDanMuMgr.newData = {}
end

function LuaDanMuMgr:Clean()

    if(#LuaDanMuMgr.Data > 0)then
        LuaDanMuMgr.Data = {}
        LuaDanMuMgr.newData = {}
        LuaDanMuMgr.PlayerNameDict = {}
        if(LuaDanMuMgr.RefreshTick ~= nil)then
            UnRegisterTick(LuaDanMuMgr.RefreshTick)
            LuaDanMuMgr.RefreshTick = nil
        end
        g_ScriptEvent:BroadcastInLua("OnCleanDanMu")
    end
    LuaDanMuMgr.Recording = false
end

function LuaDanMuMgr:OnReceivePlayerName(_player_id, _player_name)

    LuaDanMuMgr.PlayerNameDict[_player_id] = LocalString.GetString(_player_name)
    if(LuaDanMuMgr.RefreshTick == nil)then
        LuaDanMuMgr.RefreshTick = RegisterTickOnce(function()
            g_ScriptEvent:BroadcastInLua("OnRefreshDanMuNames")
            if(LuaDanMuMgr.RefreshTick ~= nil)then
                UnRegisterTick(LuaDanMuMgr.RefreshTick)
                LuaDanMuMgr.RefreshTick = nil
            end
        end, 250)
    end
end

function LuaDanMuMgr:InitDanMuMgr()
    if(not LuaDanMuMgr.Recording)then

        self:Clean()
        Gac2Gas.RequestKeJuDanMuRecord()
        LuaDanMuMgr.Recording = true
    end
end

--历史数据获取
function LuaDanMuMgr:OnStartRecieveDanMuRecord(_key)

    if(_key == "KeJu")then
        self.DanMuRecord = {}
    end
end

function LuaDanMuMgr:OnReceiveDanMuRecord(_key, _data)
    if(_key == "KeJu")then
        local list = TypeAs(MsgPackImpl.unpack(_data), typeof(MakeGenericClass(List, Object)))

        do
            local i = 0
            while i < list.Count do
                local continue = false
                repeat
                    local player_id = TypeAs(list[i], typeof(System.String))
                    local bullet = TypeAs(list[i + 1], typeof(System.String))
                    bullet = CWordFilterMgr.Inst:DoFilterOnReceiveWithoutVoiceCheck(0, bullet, nil, true)


                    if bullet == nil then
                        continue = true
                        break
                    end
                    table.insert(self.DanMuRecord, {playerId = player_id, data = bullet})
                    continue = true
                until 1
                if not continue then
                    break
                end
                i = i + 2
            end
        end

    end
end

function LuaDanMuMgr:OnReceiveDanMuRecordFinish(_key)

    if(_key == "KeJu" and (#self.DanMuRecord > 0))then
        local new_list = {}
        for k, v in ipairs(self.DanMuRecord)do
            table.insert(new_list, v)
            --检查是否存有该玩家名字， 若没有则需请求服务器
            if(LuaDanMuMgr.PlayerNameDict[v.playerId])then
            else
                LuaDanMuMgr.PlayerNameDict[v.playerId] = "ID"..v.playerId
                Gac2Gas.RequestPlayerName(v.playerId)
            end

        end
        for k, v in ipairs(LuaDanMuMgr.Data)do
            table.insert(new_list, v)
        end
        LuaDanMuMgr.Data = new_list
        self.DanMuRecord = {}

        g_ScriptEvent:BroadcastInLua("OnInitDanMu")
    end
end
