-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemMgr = import "L10.Game.CItemMgr"
local CMultiExpDetailview = import "L10.UI.CMultiExpDetailview"
local CommonDefs = import "L10.Game.CommonDefs"
local CPropertyItem = import "L10.Game.CPropertyItem"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumMultipleExpType = import "L10.Game.EnumMultipleExpType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local GameSetting_Client = import "L10.Game.GameSetting_Client"
local LocalString = import "LocalString"
local MultipleExp_BuyItem = import "L10.Game.MultipleExp_BuyItem"
local MultipleExp_GameSetting = import "L10.Game.MultipleExp_GameSetting"
local MultipleExp_TripleBuyItem = import "L10.Game.MultipleExp_TripleBuyItem"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local NGUIText = import "NGUIText"

CMultiExpDetailview.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.freezeButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.freezeButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnFreezeButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.m_UseButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_UseButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnUseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.purchaseButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.purchaseButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnPurchaseButtonClick, VoidDelegate, this), true)
    EventManager.AddListener(EnumEventType.OnQueryDoubleExpInfoReturn, MakeDelegateFromCSFunction(this.OnQueryDoubleExpInfoReturn, Action0, this))
    EventManager.AddListener(EnumEventType.ItemPropUpdated, MakeDelegateFromCSFunction(this.InitItemUse, Action0, this))
    Gac2Gas.RequestMultipleExpInfo()
end
CMultiExpDetailview.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.freezeButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.freezeButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnFreezeButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.m_UseButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_UseButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnUseButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.purchaseButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.purchaseButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnPurchaseButtonClick, VoidDelegate, this), false)
    EventManager.RemoveListener(EnumEventType.OnQueryDoubleExpInfoReturn, MakeDelegateFromCSFunction(this.OnQueryDoubleExpInfoReturn, Action0, this))
    EventManager.RemoveListener(EnumEventType.ItemPropUpdated, MakeDelegateFromCSFunction(this.InitItemUse, Action0, this))
    if this.cancelLeftTime ~= nil then
        invoke(this.cancelLeftTime)
    end
    if this.cancelCD ~= nil then
        invoke(this.cancelCD)
    end
end
CMultiExpDetailview.m_Init_CS2LuaHook = function (this) 
    --[[this.multiExpLeftTime = this.expType == EnumMultipleExpType.eDouble and CSigninMgr.Inst.doubleExpRestTime or CSigninMgr.Inst.tripleExpRestTime
    this.multiExpBuyCount = this.expType == EnumMultipleExpType.eDouble and CSigninMgr.Inst.doubleExpBuyCount or CSigninMgr.Inst.tripleExpBuyCount
    this.multiExpBuyTime = this.expType == EnumMultipleExpType.eDouble and CSigninMgr.Inst.doubleExpBuyTime or CSigninMgr.Inst.tripleExpBuyTime
    local default
    if this.expType == EnumMultipleExpType.eDouble then
        default = CSigninMgr.Inst.doubleExpIsDongJie
    else
        default = CSigninMgr.Inst.tripleExpIsDongJie
    end
    this.multiExpIsDongjie = default
    this.multiExpJiedongTime = this.expType == EnumMultipleExpType.eDouble and CSigninMgr.Inst.doubleExpJieDongTime or CSigninMgr.Inst.tripleExpJieDongTime
    this:InitLeftTimeDisplay()
    this:InitGetDoubleExp()
    this:InitItemUse()--]]
end

CMultiExpDetailview.m_OnQueryDoubleExpInfoReturn2LuaHook = function (this)
    this.multiExpLeftTime = this.expType == EnumMultipleExpType.eDouble and CSigninMgr.Inst.doubleExpRestTime or CSigninMgr.Inst.tripleExpRestTime
    this.multiExpBuyCount = this.expType == EnumMultipleExpType.eDouble and CSigninMgr.Inst.doubleExpBuyCount or CSigninMgr.Inst.tripleExpBuyCount
    this.multiExpBuyTime = this.expType == EnumMultipleExpType.eDouble and CSigninMgr.Inst.doubleExpBuyTime or CSigninMgr.Inst.tripleExpBuyTime
    local default
    if this.expType == EnumMultipleExpType.eDouble then
        default = CSigninMgr.Inst.doubleExpIsDongJie
    else
        default = CSigninMgr.Inst.tripleExpIsDongJie
    end
    this.multiExpIsDongjie = default
    this.multiExpJiedongTime = this.expType == EnumMultipleExpType.eDouble and CSigninMgr.Inst.doubleExpJieDongTime or CSigninMgr.Inst.tripleExpJieDongTime
    this:InitLeftTimeDisplay()
    this:InitGetDoubleExp()
    this:InitItemUse()
end

CMultiExpDetailview.m_InitItemUse_CS2LuaHook = function (this) 
    local data = GameSetting_Client.GetData()
    local default
    if (this.expType == EnumMultipleExpType.eDouble) then
        default = data.DoubleExperienceItemId
    else
        default = data.TrebleExperienceItemId
    end
    local templateIds = default
    local totalCount = 0
    do
        local i = 0
        while i < templateIds.Length do
            if i == 0 then
                local item = CItemMgr.Inst:GetItemTemplate(templateIds[i])
                if item ~= nil then
                    this.m_ItemTexture:LoadMaterial(item.Icon)
                end
            end
            totalCount = totalCount + CItemMgr.Inst:GetItemCount(templateIds[i])
            i = i + 1
        end
    end
    this.m_CountLabel.text = tostring(totalCount)
    this.m_DisableItemSprite.gameObject:SetActive(totalCount <= 0)
    this.m_UseButton.Enabled = (totalCount > 0)
end
CMultiExpDetailview.m_OnUseButtonClick_CS2LuaHook = function (this, button) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    local data = GameSetting_Client.GetData()
    local default
    if (this.expType == EnumMultipleExpType.eDouble) then
        default = data.DoubleExperienceItemId
    else
        default = data.TrebleExperienceItemId
    end
    local templateIds = default
    do
        local j = 0
        while j < templateIds.Length do
            local templateId = templateIds[j]
            local itemProp = CClientMainPlayer.Inst.ItemProp
            local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)
            do
                local i = 1
                while i <= bagSize do
                    local id = itemProp:GetItemAt(EnumItemPlace.Bag, i)
                    if id ~= nil then
                        local item = CItemMgr.Inst:GetById(id)
                        if item ~= nil and item.TemplateId == templateId then
                            Gac2Gas.RequestUseItem(EnumItemPlace_lua.Bag, i, item.Id, "UseMultiExp")
                            return
                        end
                    end
                    i = i + 1
                end
            end
            j = j + 1
        end
    end
end
CMultiExpDetailview.m_InitLeftTimeDisplay_CS2LuaHook = function (this)
    if this.multiExpLeftTime > 0 then
        local default
        if this.expType == EnumMultipleExpType.eDouble then
            default = LocalString.GetString("剩余双倍经验时间")
        else
            default = LocalString.GetString("剩余三倍经验时间")
        end
        this.leftTimeTitle.text = default
    else
        local extern
        if this.expType == EnumMultipleExpType.eDouble then
            extern = LocalString.GetString("当前无双倍经验时间")
        else
            extern = LocalString.GetString("当前无三倍经验时间")
        end
        this.leftTimeTitle.text = extern
    end
    if this.multiExpLeftTime > 0 and not this.multiExpIsDongjie then
        this.leftTimeValue.text = this:FormatTime(this.multiExpLeftTime, true)
        this.leftTimeValue.color = NGUIText.ParseColor24("06a42a", 0)
        if this.cancelLeftTime ~= nil then
            invoke(this.cancelLeftTime)
        end
        this.cancelLeftTime = CTickMgr.Register(MakeDelegateFromCSFunction(this.UpdateLeftTime, Action0, this), 1000, ETickType.Loop)
        if this.tips1 ~= nil then
            this.tips1.text = this.expType == EnumMultipleExpType.eDouble and g_MessageMgr:FormatMessage("Having_DoubleExp") or g_MessageMgr:FormatMessage("Having_TripleExp")
        end
    else
        this.leftTimeValue.text = this:FormatTime(this.multiExpLeftTime, true)
        this.leftTimeValue.color = NGUIText.ParseColor24("d0492a", 0)
        if this.tips1 ~= nil then
            this.tips1.text = this.expType == EnumMultipleExpType.eDouble and g_MessageMgr:FormatMessage("Do_Not_Have_DoubleExp") or g_MessageMgr:FormatMessage("Do_Not_Have_TripleExp")
        end
        if this.cancelLeftTime ~= nil then
            invoke(this.cancelLeftTime)
        end
    end
    this:InitFreezeButton()
    if LuaWelfareMgr.IsOpenBigMonthCard then
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.HasFeiSheng then
            LuaUtils.SetLocalPositionY(this.leftTimeTitle.transform, this.freezeButton.gameObject.activeSelf and 250 or 212)
            LuaUtils.SetLocalPositionY(this.leftTimeValue.transform, this.freezeButton.gameObject.activeSelf and 172 or 134) 
        else
            LuaUtils.SetLocalPositionY(this.leftTimeTitle.transform, this.freezeButton.gameObject.activeSelf and 290 or 230)
            LuaUtils.SetLocalPositionY(this.leftTimeValue.transform, this.freezeButton.gameObject.activeSelf and 210 or 150) 
            LuaUtils.SetLocalPositionY(this.freezeButton.transform, 110) 
        end
    end
end
CMultiExpDetailview.m_InitGetDoubleExp_CS2LuaHook = function (this) 
    local getFreeDoubleExp = CPropertyItem.IsSamePeriod(CSigninMgr.Inst.doubleExpReceiveTime, "w")
    if this.expType == EnumMultipleExpType.etriple then
        getFreeDoubleExp = true
    end
    if this.freeGet ~= nil then
        if not getFreeDoubleExp then
            this.freeGet.text = LocalString.GetString("每周免费1小时")
            this.freeGet.gameObject:SetActive(true)
        else
            this.freeGet.text = ""
            this.freeGet.gameObject:SetActive(false)
        end
    end
    this.purchase:SetActive(getFreeDoubleExp)
    this.purchaseButton.Enabled = true
    if not getFreeDoubleExp then
        this.purchaseButtonLabel.text = LocalString.GetString("领取")
        this.getAlert.enabled = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.MaxLevel >= MultipleExp_GameSetting.GetData().Receive_Grade
    else
        this.getAlert.enabled = false
        local totalCount = MultipleExp_BuyItem.GetDataCount()
        if this.expType == EnumMultipleExpType.etriple then
            totalCount = MultipleExp_TripleBuyItem.GetDataCount()
        end
        this.purchaseButtonLabel.text = System.String.Format(LocalString.GetString("购买({0}/{1})"), this.multiExpBuyCount, totalCount)
        this.purchaseButton.Enabled = this.multiExpBuyCount < totalCount
        if totalCount <= this.multiExpBuyCount then
            local default
            if this.expType == EnumMultipleExpType.eDouble then
                default = LocalString.GetString("本周双倍已售完")
            else
                default = LocalString.GetString("本周三倍已售完")
            end
            this.freeGet.text = default
            this.freeGet.gameObject:SetActive(true)
            this.purchase:SetActive(false)
        end
        local buyItem = MultipleExp_BuyItem.GetData(this.multiExpBuyCount + 1)
        local buyTripleItem = MultipleExp_TripleBuyItem.GetData(this.multiExpBuyCount + 1)
        if this.expType == EnumMultipleExpType.eDouble and buyItem ~= nil then
            this.priceLabel.text = System.String.Format(LocalString.GetString("{0}/1小时"), buyItem.Price)
            local extern
            if buyItem.MoneyType == EnumToInt(EnumMoneyType.LingYu) then
                extern = this.jadeIcon
            else
                extern = this.yuanbaoIcon
            end
            this.moneyIcon.spriteName = extern
        elseif this.expType == EnumMultipleExpType.etriple and buyTripleItem ~= nil then
            this.priceLabel.text = System.String.Format(LocalString.GetString("{0}/1小时"), buyTripleItem.Price)
            local ref
            if buyTripleItem.MoneyType == EnumToInt(EnumMoneyType.LingYu) then
                ref = this.jadeIcon
            else
                ref = this.yuanbaoIcon
            end
            this.moneyIcon.spriteName = ref
        end
    end
end
CMultiExpDetailview.m_InitFreezeButton_CS2LuaHook = function (this) 
    this.freezeButton.gameObject:SetActive(this.multiExpLeftTime > 0)
    if not this.multiExpIsDongjie then
        this.freezeButton:SetBackgroundSprite("common_btn_01_yellow")
        this.freezeButtonLabel.text = LocalString.GetString("冻结")
        local timeCoolDown = CServerTimeMgr.Inst.timeStamp - this.multiExpJiedongTime
        if timeCoolDown >= MultipleExp_GameSetting.GetData().DongJie_CD then
            this.freezeButton.Enabled = true
        else
            this.freezeButton.Enabled = false
            this.freezeCD = MultipleExp_GameSetting.GetData().DongJie_CD - math.floor(timeCoolDown)
            this.freezeButtonLabel.text = System.String.Format(LocalString.GetString("冻结({0})"), this:FormatTime(this.freezeCD, false))
            if this.cancelCD ~= nil then
                invoke(this.cancelCD)
            end
            this.cancelCD = CTickMgr.Register(MakeDelegateFromCSFunction(this.UpdateCD, Action0, this), 1000, ETickType.Loop)
        end
    else
        this.freezeButtonLabel.text = LocalString.GetString("解冻")
        this.freezeButton.Enabled = true
        this.freezeButton:SetBackgroundSprite("common_btn_09_normal")
    end
end
CMultiExpDetailview.m_UpdateCD_CS2LuaHook = function (this) 
    if this.freezeCD > 0 then
        this.freezeCD = this.freezeCD - 1
        this.freezeButtonLabel.text = System.String.Format(LocalString.GetString("冻结({0})"), this:FormatTime(this.freezeCD, false))
    else
        this.freezeButtonLabel.text = LocalString.GetString("冻结")
        this.freezeButton.Enabled = true
        if this.cancelCD ~= nil then
            invoke(this.cancelCD)
        end
    end
end
CMultiExpDetailview.m_UpdateLeftTime_CS2LuaHook = function (this) 
    if this.multiExpLeftTime > 0 then
        this.multiExpLeftTime = this.multiExpLeftTime - 1
    elseif this.cancelLeftTime ~= nil then
        invoke(this.cancelLeftTime)
    end
    this.leftTimeValue.text = this:FormatTime(this.multiExpLeftTime, true)
    this:InitFreezeButton()
end
CMultiExpDetailview.m_FormatTime_CS2LuaHook = function (this, time, showHour) 
    local text = ""
    if showHour then
        text = System.String.Format("{0}:{1}:{2}", this:FormatUnits(math.floor(time / 3600)), this:FormatUnits(math.floor(time / 60) % 60), this:FormatUnits(time % 60))
    else
        text = System.String.Format("{0}:{1}", this:FormatUnits(math.floor(time / 60) % 60), this:FormatUnits(time % 60))
    end
    return text
end
CMultiExpDetailview.m_FormatUnits_CS2LuaHook = function (this, value) 
    if math.floor(value / 10) >= 1 then
        return tostring(value)
    else
        return System.String.Format("0{0}", tostring(value))
    end
end
CMultiExpDetailview.m_OnFreezeButtonClick_CS2LuaHook = function (this, go) 
    if this.multiExpIsDongjie then
        Gac2Gas.RequestJieDongMultipleExp(EnumToInt(this.expType))
    else
        Gac2Gas.RequestDongJieMultipleExp(EnumToInt(this.expType))
    end
end
CMultiExpDetailview.m_OnPurchaseButtonClick_CS2LuaHook = function (this, go) 
    if not CPropertyItem.IsSamePeriod(CSigninMgr.Inst.doubleExpReceiveTime, "w") and this.expType == EnumMultipleExpType.eDouble then
        Gac2Gas.RequestReceiveFreeDoubleExp()
    else
        local buyItem = MultipleExp_BuyItem.GetData(this.multiExpBuyCount + 1)
        local buyTripleItem = MultipleExp_TripleBuyItem.GetData(this.multiExpBuyCount + 1)
        if this.expType == EnumMultipleExpType.eDouble and buyItem == nil then
            return
        end
        if this.expType == EnumMultipleExpType.etriple and buyTripleItem == nil then
            return
        end
        local default
        if this.expType == EnumMultipleExpType.eDouble then
            default = LocalString.GetString("确定购买1小时双倍时间？")
        else
            default = LocalString.GetString("确定购买1小时三倍时间？")
        end
        local txt = default
        if this.expType == EnumMultipleExpType.eDouble then
            QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(CommonDefs.ConvertIntToEnum(typeof(EnumMoneyType), buyItem.MoneyType), txt, buyItem.Price, DelegateFactory.Action(function () 
                Gac2Gas.RequestBuyMultipleExpItem(EnumToInt(this.expType))
            end), nil, false, true, EnumPlayScoreKey.NONE, false)
        else
            QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(CommonDefs.ConvertIntToEnum(typeof(EnumMoneyType), buyTripleItem.MoneyType), txt, buyTripleItem.Price, DelegateFactory.Action(function () 
                Gac2Gas.RequestBuyMultipleExpItem(EnumToInt(this.expType))
            end), nil, false, true, EnumPlayScoreKey.NONE, false)
        end
    end
end
