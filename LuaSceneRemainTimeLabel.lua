local CScene = import "L10.Game.CScene"
--通用的副本倒计时Label
LuaSceneRemainTimeLabel =class()
RegistClassMember(LuaSceneRemainTimeLabel,"m_RemainTimeLabel")

function LuaSceneRemainTimeLabel:Awake()
    self.m_RemainTimeLabel = self.transform:GetComponent(typeof(UILabel))
end


function LuaSceneRemainTimeLabel:OnEnable()
    self:SetRemainTimeVal()
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
    g_ScriptEvent:AddListener("ShowTimeCountDown", self, "OnShowTimeCountDown")
end


function LuaSceneRemainTimeLabel:OnDisable()
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("ShowTimeCountDown", self, "OnShowTimeCountDown")
end

function LuaSceneRemainTimeLabel:OnRemainTimeUpdate(args)
    self:SetRemainTimeVal()
end

function LuaSceneRemainTimeLabel:OnShowTimeCountDown(args)
	if self.m_CountDownRoot == nil then return end
    local mainScene = CScene.MainScene
    if mainScene then
        self.m_RemainTimeLabel.enabled =  mainScene.ShowTimeCountDown
    end
end

function LuaSceneRemainTimeLabel:SetRemainTimeVal()
    local mainScene = CScene.MainScene
    if mainScene then
        if mainScene.ShowTimeCountDown then
            self.m_RemainTimeLabel.text = self:GetRemainTimeText(mainScene.ShowTime)
        else
            self.m_RemainTimeLabel.text = ""
        end
    else
        self.m_RemainTimeLabel.text = ""
    end
end

function LuaSceneRemainTimeLabel:GetRemainTimeText(totalSeconds)
    if totalSeconds<0 then
        totalSeconds = 0
    end
    if totalSeconds>=3600 then
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}:{2:00}[-]"),  
         math.floor(totalSeconds / 3600),
         math.floor((totalSeconds % 3600) / 60), 
         totalSeconds % 60)
    else
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}[-]"), math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end