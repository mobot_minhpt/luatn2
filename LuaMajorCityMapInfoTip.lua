require("common/common_include")

local LuaUtils = import "LuaUtils"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local Extensions = import "Extensions"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local AlignType3 = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"

CLuaMajorCityMapInfoTip = class()
CLuaMajorCityMapInfoTip.Path = "ui/citywar/LuaMajorCityMapInfoTip"

RegistClassMember(CLuaMajorCityMapInfoTip, "m_Tick")
RegistClassMember(CLuaMajorCityMapInfoTip, "m_StatusLabel")
RegistClassMember(CLuaMajorCityMapInfoTip, "m_Time")

function CLuaMajorCityMapInfoTip:Init()
	local childName = {"CityName", "CityLevel", "OccupiedCount", "BelongName", "Status", "MapLevel"}
	local map = CityWar_Map.GetData(CLuaCityWarMgr.MapInfoTemplateId)
	local cityName = map and map.Name or ""
	local status = CLuaCityWarMgr.StatusNameTable[CLuaCityWarMgr.CityInfoStatus]
	if CLuaCityWarMgr.CityInfoStatus == 1 then
		if map.Level < 2 then
			status = status[1]
		else
			status = status[2]
		end
	end
	local content = {cityName, SafeStringFormat3(LocalString.GetString("%s级"), tostring(CLuaCityWarMgr.CityInfoLevel)), CLuaCityWarMgr.MapInfoOccupiedCount.."/9", CLuaCityWarMgr.CityInfoGuildName == "" and LocalString.GetString("无") or CLuaCityWarMgr.CityInfoGuildName.."("..CLuaCityWarMgr.CityInfoServerName..")", status, SafeStringFormat3(LocalString.GetString("%d级领土"), map.Level)}
	for i = 1, #childName do
		self.transform:Find("Offset/"..childName[i]):GetComponent(typeof(UILabel)).text = content[i]
	end

	if CLuaCityWarMgr.CityInfoStatus == 9 or CLuaCityWarMgr.CityInfoStatus == 6 or CLuaCityWarMgr.CityInfoStatus == 4 then
		self:InitTick()
	end

	local challengeObj = self.transform:Find("Offset/MajorCityRoot/ChallengeBtn").gameObject
	UIEventListener.Get(challengeObj).onClick = LuaUtils.VoidDelegate(function ( go )
		self:OnChallengeOrWatchBtnClick(go)
	end)

	local openMapObj = self.transform:Find("Offset/MajorCityRoot/OpenMapBtn").gameObject
	UIEventListener.Get(openMapObj).onClick = LuaUtils.VoidDelegate(function ( ... )
		CLuaCityWarMgr:OpenSecondaryMap(CLuaCityWarMgr.MapInfoTemplateId)
		CUIManager.CloseUI(CLuaUIResources.MajorCityMapInfoTip)
	end)

	if CLuaCityWarMgr.CityInfoPlayNum > 0 then
		challengeObj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("观战")
	elseif map.Level <= 1 or CLuaCityWarMgr.CityInfoGuildId <= 0 or (not CLuaCityWarMgr:IsMajorCity(CLuaCityWarMgr.MapInfoMyGuildCityId)) or (not CLuaCityWarMgr:IsNeighbor(CLuaCityWarMgr.MapInfoMyGuildMapId, CLuaCityWarMgr.MapInfoTemplateId)) then
		challengeObj:SetActive(false)
		Extensions.SetLocalPositionX(openMapObj.transform, 30)
	end
end

function CLuaMajorCityMapInfoTip:OnChallengeOrWatchBtnClick(go)
	if CLuaCityWarMgr.CityInfoPlayNum > 0 then
		local playIdList = MsgPackImpl.unpack(CLuaCityWarMgr.CityInfoPlayIdsUD)
		if not playIdList then return end
		if CLuaCityWarMgr.CityInfoPlayNum == 1 and playIdList.Count >= 1 then
			Gac2Gas.RequestWatchMirrorWar(playIdList[0])
			return
		else
			local popupMenuItemTable = {}
			local indexNameTbl = {LocalString.GetString("一"), LocalString.GetString("二"), LocalString.GetString("三")}
			for i = 1, CLuaCityWarMgr.CityInfoPlayNum do
				table.insert(popupMenuItemTable, PopupMenuItemData(LocalString.GetString("战场")..indexNameTbl[i], DelegateFactory.Action_int(function (index)
					Gac2Gas.RequestWatchMirrorWar(playIdList[i - 1])
					end), false, nil, EnumPopupMenuItemStyle.Default))
			end
			local popupMenuItemArray = Table2Array(popupMenuItemTable, MakeArrayClass(PopupMenuItemData))
			CPopupMenuInfoMgr.ShowPopupMenu(popupMenuItemArray, go.transform, AlignType3.Top, 1, nil, 600, true, 227)
		end
		return
	end

	CLuaCityWarMgr.RequestDeclareMirrorWar(CLuaCityWarMgr.CityInfoGuildId, CLuaCityWarMgr.CityInfoGuildName, CLuaCityWarMgr.CityInfoMapTemplateId, CLuaCityWarMgr.CityInfoTemplateId)
	CUIManager.CloseUI(CLuaUIResources.MajorCityMapInfoTip)
end

function CLuaMajorCityMapInfoTip:UpdateTime(time)
	local h, m, s = math.floor(time / 3600), math.floor((time % 3600) / 60), math.floor(time % 60)
	self.m_StatusLabel.text = CLuaCityWarMgr.StatusNameTable[CLuaCityWarMgr.CityInfoStatus]..SafeStringFormat3(LocalString.GetString("(%02d:%02d:%02d)"), h, m, s)
end

function CLuaMajorCityMapInfoTip:InitTick()
	self.m_StatusLabel = self.transform:Find("Offset/Status"):GetComponent(typeof(UILabel))
	self.m_Time = CLuaCityWarMgr.CityInfoDuration
	self:UpdateTime(self.m_Time)
	self.m_Tick = RegisterTickWithDuration(function ()
		self.m_Time = self.m_Time - 1
		self:UpdateTime(self.m_Time)
	end, 1000, self.m_Time * 1000)
end

function CLuaMajorCityMapInfoTip:OnDestroy( ... )
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
end
