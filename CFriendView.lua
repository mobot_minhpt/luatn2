-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFriendListItem = import "L10.UI.CFriendListItem"
local CFriendView = import "L10.UI.CFriendView"
local CIMMgr = import "L10.Game.CIMMgr"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CUIManager = import "L10.UI.CUIManager"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local GameObject = import "UnityEngine.GameObject"
local Int32 = import "System.Int32"
local PlayerOperationData = import "L10.Game.PlayerOperationData"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local UITabBar = import "L10.UI.UITabBar"

LuaFriendView = {}
LuaFriendView.lasetIndex = 0

function LuaFriendView:UpdateHuiLiu()
  local this = LuaFriendView.HuiLiuWnd
  if this then
    local huiliuBtn = this.transform:Find('HuiLiuBtn').gameObject
    huiliuBtn:SetActive(true)
    local onHuiliuClick = function(go)
      CUIManager.ShowUI(CLuaUIResources.HuiLiuNewMainWnd)
    end
    CommonDefs.AddOnClickListener(huiliuBtn,DelegateFactory.Action_GameObject(onHuiliuClick),false)
  end
end

LuaFriendView.m_Wnd = nil
function LuaFriendView:OnSendShiTuShiMenHasReward(hasReward1,hasReward2)
    local this = self.m_Wnd
end

function LuaFriendView:InTabBar(this)
    this.tabBar.gameObject:SetActive(true)
    this.tabBar2.gameObject:SetActive(true)
end

function LuaFriendView:OnEnable(this)
    self.m_Wnd = this
    g_ScriptEvent:AddListener("OnSendShiTuShiMenHasReward", self, "OnSendShiTuShiMenHasReward")
    Gac2Gas.RequestHasShiTuShiMenReward()
end

function LuaFriendView:OnDisable(this)
    self.m_Wnd = nil
    g_ScriptEvent:RemoveListener("OnSendShiTuShiMenHasReward", self, "OnSendShiTuShiMenHasReward")
end

CFriendView.m_Init_CS2LuaHook = function (this)

    this.recentListView.OnItemClickDelegate = MakeDelegateFromCSFunction(this.OnFriendItemClick, MakeGenericClass(Action1, CFriendListItem), this)
    this.contactListView.OnItemClickDelegate = MakeDelegateFromCSFunction(this.OnContactListFriendItemClick, MakeGenericClass(Action2, Int32, CFriendListItem), this)
    LuaFriendView:InTabBar(this)
    this:UpdateTabAlert()
    this:UpdateFriendRequestAlert()
    if CPersonalSpaceMgr.OpenPersonalSpaceEntrance then
        this.tabBar.gameObject:SetActive(true)
        this.tabBar2.gameObject:SetActive(false)
        this.spaceBtn:SetActive(true)
        this.spaceBtn.transform:Find("Alert").gameObject:SetActive(false)

        this.tabBar.OnTabChange = MakeDelegateFromCSFunction(this.OnTabChange, MakeGenericClass(Action2, GameObject, Int32), this)
        if PlayerOperationData.Inst.needChatWithOther then
            this.tabBar:ChangeTab(0, false)
        else
            this.tabBar:ChangeTab(this.index, false)
        end

        UIEventListener.Get(this.spaceBtn).onClick = MakeDelegateFromCSFunction(this.OnSpaceButtonClick, VoidDelegate, this)
    else
        this.tabBar.gameObject:SetActive(false)
        this.tabBar2.gameObject:SetActive(true)
        this.spaceBtn:SetActive(false)

        this.tabBar2.OnTabChange = MakeDelegateFromCSFunction(this.OnTabChange, MakeGenericClass(Action2, GameObject, Int32), this)
        if PlayerOperationData.Inst.needChatWithOther then
            this.tabBar2:ChangeTab(0, false)
        else
            this.tabBar2:ChangeTab(this.index, false)
        end
    end

    this:UpdatePersonalSpaceAlert()

    if LuaHuiLiuMgr.CheckNewHuiLiuOpen() then
      LuaFriendView.HuiLiuWnd = this
      Gac2Gas.QueryShowNewZhaoHuiBtn()
    else
      local huiliuBtn = this.transform:Find('HuiLiuBtn').gameObject
      huiliuBtn:SetActive(false)
    end
end
CFriendView.m_PersonalSpaceAlertInfoRet_CS2LuaHook = function (this, ret)
    if not this or not this.spaceBtn then
        return
    end

    if ret.code == 0 then
        if ret.data and ret.data.count > 0 then
            this.spaceBtn.transform:Find("Alert").gameObject:SetActive(true)
            if ret.data.count <= 99 then
                CommonDefs.GetComponent_Component_Type(this.spaceBtn.transform:Find("Alert/numlabel"), typeof(UILabel)).text = tostring(ret.data.count)
            else
                CommonDefs.GetComponent_Component_Type(this.spaceBtn.transform:Find("Alert/numlabel"), typeof(UILabel)).text = "..."
            end
        elseif ret.data and ret.data.redDot then
            this.spaceBtn.transform:Find("Alert").gameObject:SetActive(true)
            CommonDefs.GetComponent_Component_Type(this.spaceBtn.transform:Find("Alert/numlabel"), typeof(UILabel)).text = ""
        else
            this.spaceBtn.transform:Find("Alert").gameObject:SetActive(false)
        end
    end
end
CFriendView.m_OnEnable_CS2LuaHook = function (this)
    LuaFriendView:OnEnable(this)
    CFriendView.ChatOppositeId = System.UInt64.MaxValue
    EventManager.AddListener(EnumEventType.IMShouldShowAlertValueChange, MakeDelegateFromCSFunction(this.UpdateTabAlert, Action0, this))
    EventManager.AddListener(EnumEventType.FriendRequestAlertUpdate, MakeDelegateFromCSFunction(this.UpdateFriendRequestAlert, Action0, this))

	g_ScriptEvent:AddListener("UpdateHuiLiuBtn", LuaFriendView, "UpdateHuiLiu")
end
CFriendView.m_OnDisable_CS2LuaHook = function (this)
    LuaFriendView:OnDisable(this)
    LuaFriendView.current = nil
    CFriendView.ChatOppositeId = System.UInt64.MaxValue
    EventManager.RemoveListener(EnumEventType.IMShouldShowAlertValueChange, MakeDelegateFromCSFunction(this.UpdateTabAlert, Action0, this))
    EventManager.RemoveListener(EnumEventType.FriendRequestAlertUpdate, MakeDelegateFromCSFunction(this.UpdateFriendRequestAlert, Action0, this))

	g_ScriptEvent:RemoveListener("UpdateHuiLiuBtn", LuaFriendView, "UpdateHuiLiu")
end
CFriendView.m_OnTabChange_CS2LuaHook = function (this, go, index)
    LuaFriendView.current = nil
    CFriendView.ChatOppositeId = System.UInt64.MaxValue
    if index == 0 then
        if PlayerOperationData.Inst.needChatWithOther then
            CFriendView.ChatOppositeId = PlayerOperationData.Inst.chatOppositeId
            --if not CommonDefs.ListContains(CIMMgr.Inst.RecentList, typeof(UInt64), PlayerOperationData.Inst.chatOppositeId) then
            this.friendChatView:Init(0, false, CFriendView.ChatOppositeId, PlayerOperationData.Inst.chatOppositeName, false)
            --end
            --强行切换到最新联系人下，否则可能在最近交互下，或者没有调用更新
            if this.recentListView.gameObject.activeSelf then
                this.recentListView.tabBar:ChangeTab(0)
            else
                this.recentListView.curTabIndex = 0
            end
            PlayerOperationData.Inst.needChatWithOther = false
        else
            this.friendChatView:Init(0, false, CFriendView.ChatOppositeId, nil, true)
        end
    elseif index == 1 then
        this.friendChatView:Init(0, false, System.UInt32.MaxValue, nil, true)
    end
    local friendSearchViewIndex = 2
    this.recentListView.gameObject:SetActive(index == 0)
    this.contactListView.gameObject:SetActive(index == 1)
    this.requestListView.gameObject:SetActive(index == friendSearchViewIndex)
    this.friendChatView.gameObject:SetActive(index ~= friendSearchViewIndex)
    this.friendSearchView.gameObject:SetActive(index == friendSearchViewIndex)  
    LuaFriendView.lasetIndex = index
end
CFriendView.m_OnSpaceButtonClick_CS2LuaHook = function (this, go)
    if CClientMainPlayer.Inst == nil then
        return
    end
    this.spaceBtn.transform:Find("Alert").gameObject:SetActive(false)
    CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(CClientMainPlayer.Inst.Id, 0)
end
CFriendView.m_OnFriendItemClick_CS2LuaHook = function (this, item)
    if not this.friendChatView.gameObject.activeSelf then
        this.friendChatView.gameObject:SetActive(true)
    end

    if item == nil then
        this.friendChatView:Init(0, false, System.UInt64.MaxValue, nil, true)
    elseif item.FriendItemType == CFriendListItem.ItemType.JingLing then
        this.friendChatView:Init(0, false, item.PlayerId, nil, true)
        CJingLingMgr.Inst:ShowJingLingWnd()
    else
        if LuaFriendView.current == item then return end
        this.friendChatView:Init(0, false, item.PlayerId, item.PlayerName, false)
    end
    LuaFriendView.current = item
end
CFriendView.m_OnContactListFriendItemClick_CS2LuaHook = function (this, groupId, item)
    if not this.friendChatView.gameObject.activeSelf then
        this.friendChatView.gameObject:SetActive(true)
    end

    if item == nil then
        this.friendChatView:Init(0, false, System.UInt64.MaxValue, nil, true)
    elseif item.FriendItemType == CFriendListItem.ItemType.JingLing then
        this.friendChatView:Init(groupId, false, item.PlayerId, nil, true)
        CJingLingMgr.Inst:ShowJingLingWnd()
    elseif item.FriendItemType == CFriendListItem.ItemType.CloseFriend then
        this.friendChatView:Init(0, false, item.PlayerId, nil, true)
        CUIManager.ShowUI("FriendStatusWnd")
    else
        if LuaFriendView.current == item then return end
        this.friendChatView:Init(groupId, false, item.PlayerId, item.PlayerName, false)
    end
    LuaFriendView.current = item
end
CFriendView.m_UpdateTabAlert_CS2LuaHook = function (this)
    this.tabBar.transform:GetChild(0).transform:Find("Alert").gameObject:SetActive(CIMMgr.Inst:ShouldShowRecentAlertOnChat(CFriendView.ChatOppositeId))
    this.tabBar.transform:GetChild(1).transform:Find("Alert").gameObject:SetActive(false)
    this.tabBar2.transform:GetChild(0).transform:Find("Alert").gameObject:SetActive(CIMMgr.Inst:ShouldShowRecentAlertOnChat(CFriendView.ChatOppositeId))
    this.tabBar2.transform:GetChild(1).transform:Find("Alert").gameObject:SetActive(false)
end

CFriendView.m_hookUpdateFriendRequestAlert = function (this)
    local friendSearchViewIndex = 2
    this.tabBar.transform:GetChild(friendSearchViewIndex).transform:Find("Alert").gameObject:SetActive(CIMMgr.Inst.ShouldShowFriendRequestAlert)
    this.tabBar2.transform:GetChild(friendSearchViewIndex).transform:Find("Alert").gameObject:SetActive(CIMMgr.Inst.ShouldShowFriendRequestAlert)
end