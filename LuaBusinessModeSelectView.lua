local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CChatLinkMgr = import "CChatLinkMgr"

LuaBusinessModeSelectView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBusinessModeSelectView, "Mode1", "Mode1", CButton)
RegistChildComponent(LuaBusinessModeSelectView, "Mode2", "Mode2", CButton)
RegistChildComponent(LuaBusinessModeSelectView, "Mode3", "Mode3", CButton)
RegistChildComponent(LuaBusinessModeSelectView, "Mode4", "Mode4", CButton)
RegistChildComponent(LuaBusinessModeSelectView, "ModeTip", "ModeTip", UILabel)
RegistChildComponent(LuaBusinessModeSelectView, "StartBtn", "StartBtn", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaBusinessModeSelectView, "m_Mode")

function LuaBusinessModeSelectView:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    for i = 1, 4 do
        local curBtn = self["Mode"..i]
        local data = Business_Type.GetData(i)
        curBtn.label.text = data.TypeName
    end

    for i = 1, 4 do
        local curBtn = self["Mode"..i]
        UIEventListener.Get(curBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnModeBtnClick(go, i)
        end)
    end

    UIEventListener.Get(self.StartBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnStartBtnClick(go)
    end)
end

function LuaBusinessModeSelectView:Start()
    self.ModeTip.text = ""
end

--@region UIEvent

--@endregion UIEvent

function LuaBusinessModeSelectView:OnModeBtnClick(go, i)
    for j = 1, 4 do
        self["Mode"..j].Selected = i == j
    end
    local data = Business_Type.GetData(i)
    self.ModeTip.text = CChatLinkMgr.TranslateToNGUIText(g_MessageMgr:FormatMessage(data.IntroduceMessage), false)

    self.m_Mode = i
end


function LuaBusinessModeSelectView:OnStartBtnClick(go)
    if self.m_Mode then 
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("BUSINESS_START_MAKESURE"),DelegateFactory.Action(function()
            Gac2Gas.StartTradeSimulation(self.m_Mode)
        end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
    end
end
