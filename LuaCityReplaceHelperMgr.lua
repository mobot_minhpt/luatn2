require "base_common/cityreplace/CityReplaceHelperInc"
require "game/cityreplace/LuaCityReplaceFunc"
require "game/cityreplace/LuaMailContentReplaceMgr"

CLuaCityReplaceHelperMgr = {}
CLuaCityReplaceHelperMgr.m_Helper = CCityReplaceHelper:new("gac", GacCityReplaceHelperFunction, GacCityReplacePostAction)
CLuaCityReplaceHelperMgr.m_PatchOpen = false
CLuaCityReplaceHelperMgr.m_IsStart = false
CLuaCityReplaceHelperMgr.m_VerboseLog = false

g_ScriptEvent:AddListener("OnGacAllModulesStartUp", CLuaCityReplaceHelperMgr, "OnGacAllModulesStartUp")

local Utility = import "L10.Engine.Utility"
function CLuaCityReplaceHelperMgr:OnGacAllModulesStartUp()
    Utility.BeginSample("CityReplaceHelper")
    self.m_Helper:LoadAllReplaces()
    self:PatchOpen()
    self:StartAll()
    Utility.EndSample()
end

function CLuaCityReplaceHelperMgr:PatchOpen()
    self.m_PatchOpen = true
    CLuaMailContentReplaceMgr:PatchOpen()
end

function CLuaCityReplaceHelperMgr:StartAll()
    if not self.m_PatchOpen then return end
    self.m_IsStart = true
    self.m_Helper:Start()
    CLuaMailContentReplaceMgr:StartReplace()
end

function CLuaCityReplaceHelperMgr:StopAll()
    if not self.m_PatchOpen then return end
    self.m_IsStart = false
    self.m_Helper:Stop()
    CLuaMailContentReplaceMgr:StartRestore()
end

function CLuaCityReplaceHelperMgr:StartById(id, bNotRunPost)
    if not self.m_PatchOpen then return end
    if not self.m_IsStart then return end

    self.m_Helper:StartSingleById(id, bNotRunPost)
end

function CLuaCityReplaceHelperMgr:StopById(id, bNotRunPost)
    if not self.m_PatchOpen then return end
    if not self.m_IsStart then return end

    self.m_Helper:StopSingleById(id, bNotRunPost)
end

function CLuaCityReplaceHelperMgr:__Test_Write_Location_File(path)
    local Location = {}

    local CPublicMapMgr = import "L10.Game.CPublicMapMgr"

    CommonDefs.DictIterate(CPublicMapMgr.Instance.m_ObjectLocation, DelegateFactory.Action_object_object(function(objectType, objectInfo)
        CommonDefs.DictIterate(objectInfo, DelegateFactory.Action_object_object(function(id, sceneInfo)
            CommonDefs.DictIterate(sceneInfo, DelegateFactory.Action_object_object(function(mapId, posList)
                Location[objectType] = Location[objectType] or {}
                Location[objectType][id] = Location[objectType][id] or {}
                Location[objectType][id][mapId] = {}
            
                for i = 0, posList.Count - 1 do
                    table.insert(Location[objectType][id][mapId], {posList[i].x, posList[i].y, posList[i].z})
                end
            end))
        end))
    end))

    local _IntObjectType2Str = {
        [3] = "NPC",
        [2] = "Monster",
        [7] = "Pick",
        [8] = "Temple",
    }

    local fd = io.open(path,"w+")

    local objectTypeList = {}
    for objectType, objectInfo in pairs(Location) do
        table.insert(objectTypeList, objectType)
    end

    table.sort(objectTypeList, function(a, b)
        return _IntObjectType2Str[a] < _IntObjectType2Str[b]
    end)

    for _, objectType in pairs(objectTypeList) do
        local objectInfo = Location[objectType]

        local objectList = {}
        for id, sceneInfo in pairs(objectInfo) do
            table.insert(objectList, id)
        end

        table.sort(objectList, function(a, b) return a < b end)

        for _, id in pairs(objectList) do
            local sceneInfo= objectInfo[id]

            local mapList = {}
            for mapId, _ in pairs(sceneInfo) do
                table.insert(mapList, mapId)
            end
            table.sort(mapList, function(a, b) return a < b end)

            for _, mapId in pairs(mapList) do
                local posList = sceneInfo[mapId]

                local function _sort_by(a, b)
                    if a[1] ~= b[1] then
                        return a[1] < b[1]
                    end
            
                    if a[2] ~= b[2] then
                        return a[2] < b[2]
                    end
                end

                table.sort(posList, _sort_by)

                local posStrList = {}
                for _, pos in pairs(posList) do
                    table.insert(posStrList, SafeStringFormat3("{%s,%s,%s}", pos[1], pos[2], pos[3]))
                end

                fd:write(SafeStringFormat3("Location[\"%s\"][%s][%s] = {%s}\n", _IntObjectType2Str[objectType] or objectType, id, mapId, table.concat(posStrList, ",")))
            end
        end
    end
    fd:close()
end

function CLuaCityReplaceHelperMgr:__Test_Write_CrossMapPath_File(path)
    local CrossMapPath = {}

    local CPublicMapMgr = import "L10.Game.CPublicMapMgr"
    CommonDefs.DictIterate(CPublicMapMgr.Instance.m_CrossMapPath, DelegateFactory.Action_object_object(function(mapId, outs)
        CommonDefs.ListIterate(outs, DelegateFactory.Action_object(function (pathInfo)
            CrossMapPath[mapId] = CrossMapPath[mapId] or {}
            table.insert(CrossMapPath[mapId], {
                mapTemplateId = pathInfo.mapTemplateId,
                npcId = pathInfo.npcId,
                x = pathInfo.x,
                y = pathInfo.y,
                z = pathInfo.z,
                targetMapId = pathInfo.targetMapId,
                targetPosX = pathInfo.targetPosX,
                targetPosY = pathInfo.targetPosY,
                targetPosZ = pathInfo.targetPosZ,
                dialogId = pathInfo.dialogId,
                choiceIndex = pathInfo.choiceIndex,
                type = pathInfo.type,
            })
        end))
    end))

    local fd = io.open(path,"w+")

    -- 先按MapId由小到大排个序
    local mapIdList = {}
    for mapId, _ in pairs(CrossMapPath) do
        table.insert(mapIdList, mapId)
    end
    table.sort(mapIdList, function(a, b) return a < b end)

    local function _sort_by(a, b)
		if a.x ~= b.x then
			return a.x < b.x
		end

		if a.y ~= b.y then
			return a.y < b.y
		end

		if a.targetMapId ~= b.targetMapId then
			return a.targetMapId < b.targetMapId
        end
        
        if a.targetPosX ~= b.targetPosX then
			return a.targetPosX < b.targetPosX
        end
        
        if a.targetPosY ~= b.targetPosY then
			return a.targetPosY < b.targetPosY
        end

	end

    for _, mapId in pairs(mapIdList) do
        local outs = CrossMapPath[mapId]
        fd:write(SafeStringFormat3("CrossMapPath[%s] = {\n", mapId))

        -- 排个序，好比较
        table.sort(outs, _sort_by)

        for _, out in pairs(outs) do
            fd:write(SafeStringFormat3("\t{x = %s, y = %s, z = %s, target_map_id = %s, target_x = %s, target_y = %s, target_z = %s, npc_id = %s, dialog_id = %s, type = \"%s\", map_id = %s, choice_index = %s, },\n", 
                                                out.x, out.y, out.z, out.targetMapId, out.targetPosX, out.targetPosY, out.targetPosZ, out.npcId, out.dialogId, out.type, out.mapTemplateId, out.choiceIndex))
        end
        fd:write("}\n\n")
    end

    fd:close()
end
