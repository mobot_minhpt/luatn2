local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local Color = import "UnityEngine.Color"
local DelegateFactory = import "DelegateFactory"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaChildrenDayPlayFightInfoWnd = class()
RegistChildComponent(LuaChildrenDayPlayFightInfoWnd,"closeBtn", GameObject)
RegistChildComponent(LuaChildrenDayPlayFightInfoWnd,"topInfoNode", GameObject)
RegistChildComponent(LuaChildrenDayPlayFightInfoWnd,"templateNode", GameObject)
RegistChildComponent(LuaChildrenDayPlayFightInfoWnd,"table", UITable)
RegistChildComponent(LuaChildrenDayPlayFightInfoWnd,"scrollView", UIScrollView)

RegistClassMember(LuaChildrenDayPlayFightInfoWnd, "team1NodeTable")
RegistClassMember(LuaChildrenDayPlayFightInfoWnd, "team2NodeTable")

function LuaChildrenDayPlayFightInfoWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.ChildrenDayPlayFightInfoWnd)
end

function LuaChildrenDayPlayFightInfoWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaChildrenDayPlayFightInfoWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaChildrenDayPlayFightInfoWnd:InitNode(node,info)
	node.transform:Find('Name/text'):GetComponent(typeof(UILabel)).text = info.playerName
	node.transform:Find('rank'):GetComponent(typeof(UILabel)).text = info.rank
	node.transform:Find('score'):GetComponent(typeof(UILabel)).text = info.score
	if info.force == 1 then
		node.transform:Find('b').gameObject:SetActive(true)
		node.transform:Find('r').gameObject:SetActive(false)
	else
		node.transform:Find('b').gameObject:SetActive(false)
		node.transform:Find('r').gameObject:SetActive(true)
	end

	if info.playerId == CClientMainPlayer.Inst.Id then
		node.transform:Find('Name/text'):GetComponent(typeof(UILabel)).color = Color.green
		node.transform:Find('rank'):GetComponent(typeof(UILabel)).color = Color.green
		node.transform:Find('score'):GetComponent(typeof(UILabel)).color = Color.green
	end
end

function LuaChildrenDayPlayFightInfoWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	self.templateNode:SetActive(false)
	--(LuaChildrenDay2019Mgr.PengPengCheFightInfo,{rank = rank,playerId=playerId,score=score,force=force,playerName=playerName})
	local team1score = 0
	local team2score = 0
	local selfSide = nil
	for i,v in ipairs(LuaChildrenDay2019Mgr.PengPengCheFightInfo) do
		local playerInfo = v
		if playerInfo.force == 1 then
			team1score = team1score + playerInfo.score
		else
			team2score = team2score + playerInfo.score
		end

		if playerInfo.playerId == CClientMainPlayer.Inst.Id then
			selfSide = playerInfo.force
		end

		if playerInfo.rank > 0 then
			local node = NGUITools.AddChild(self.table.gameObject, self.templateNode)
			node:SetActive(true)
			self:InitNode(node,playerInfo)
		end
	end


	self.table:Reposition()
	self.scrollView:ResetPosition()

	self.team1NodeTable = {scoreLabel = self.topInfoNode.transform:Find('Bscore'):GetComponent(typeof(UILabel)),
	pNode = self.topInfoNode.transform:Find('pNodeB').gameObject,
	win = self.topInfoNode.transform:Find('pNodeBWin').gameObject,
	lose = self.topInfoNode.transform:Find('pNodeBLose').gameObject,
	draw = self.topInfoNode.transform:Find('pNodeBDraw').gameObject,
}
	self.team2NodeTable = {scoreLabel = self.topInfoNode.transform:Find('Rscore'):GetComponent(typeof(UILabel)),
	pNode = self.topInfoNode.transform:Find('pNodeR').gameObject,
	win = self.topInfoNode.transform:Find('pNodeRWin').gameObject,
	lose = self.topInfoNode.transform:Find('pNodeRLose').gameObject,
	draw = self.topInfoNode.transform:Find('pNodeRDraw').gameObject,
}


	self.team1NodeTable.pNode:SetActive(false)
	self.team1NodeTable.win:SetActive(false)
	self.team1NodeTable.lose:SetActive(false)
	self.team1NodeTable.draw:SetActive(false)

	self.team2NodeTable.pNode:SetActive(false)
	self.team2NodeTable.win:SetActive(false)
	self.team2NodeTable.lose:SetActive(false)
	self.team2NodeTable.draw:SetActive(false)

	self.team1NodeTable.scoreLabel.text = team1score
	self.team2NodeTable.scoreLabel.text = team2score

	if selfSide then
		if selfSide == 1 then
			self.team1NodeTable.pNode:SetActive(true)
		else
			self.team2NodeTable.pNode:SetActive(true)
		end
	end


end

return LuaChildrenDayPlayFightInfoWnd
