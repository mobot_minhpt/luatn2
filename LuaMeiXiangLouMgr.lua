local EventManager              = import "EventManager"
local EnumEventType             = import "EnumEventType"
local EnumSpeakingBubbleType    = import "L10.Game.EnumSpeakingBubbleType"
local CClientObjectMgr          = import "L10.Game.CClientObjectMgr"
local CClientMainPlayer         = import "L10.Game.CClientMainPlayer"
local CClientOtherPlayer        = import "L10.Game.CClientOtherPlayer"
local CScene                    = import "L10.Game.CScene"
local EChatPanel                = import "L10.Game.EChatPanel"
local CChatHelper               = import "L10.UI.CChatHelper"
local CSocialWndMgr             = import "L10.UI.CSocialWndMgr"
local CoreScene                 = import "L10.Engine.Scene.CoreScene"
local PlayerSettings            = import "L10.Game.PlayerSettings"

LuaMeiXiangLouMgr = {}

LuaMeiXiangLouMgr.s_OpenPlay = true

LuaMeiXiangLouMgr.m_BuffInfoUpdateFunc = DelegateFactory.Action_uint(function (engineId)
    LuaMeiXiangLouMgr:OnBuffInfoUpdate(engineId)
end)

LuaMeiXiangLouMgr.m_ChangeMeiXingLouDrunkBuffFunc = DelegateFactory.Action_uint_bool(function (engineId,active)
    local player = CClientMainPlayer.Inst
    if not player then return end
    if engineId == player.EngineId then
        g_ScriptEvent:BroadcastInLua("OnSwitchInviteWinePannel",active)
    end
end)

LuaMeiXiangLouMgr.m_TargetChangeAction = DelegateFactory.Action_uint(function (engineId)
    LuaMeiXiangLouMgr:OnBuffInfoUpdate(engineId)
end)

function LuaMeiXiangLouMgr:AddListener()
    --OnBuffInfoUpdate
    EventManager.RemoveListenerInternal(EnumEventType.OnBuffInfoUpdate, LuaMeiXiangLouMgr.m_BuffInfoUpdateFunc)
	EventManager.AddListenerInternal(EnumEventType.OnBuffInfoUpdate, LuaMeiXiangLouMgr.m_BuffInfoUpdateFunc)

    EventManager.RemoveListenerInternal(EnumEventType.OnChangeMeiXingLouDrunkBuff, LuaMeiXiangLouMgr.m_ChangeMeiXingLouDrunkBuffFunc)
	EventManager.AddListenerInternal(EnumEventType.OnChangeMeiXingLouDrunkBuff, LuaMeiXiangLouMgr.m_ChangeMeiXingLouDrunkBuffFunc)

    EventManager.RemoveListenerInternal(EnumEventType.ChangeTarget, LuaMeiXiangLouMgr.m_TargetChangeAction)
    EventManager.AddListenerInternal(EnumEventType.ChangeTarget, LuaMeiXiangLouMgr.m_TargetChangeAction)
end

function LuaMeiXiangLouMgr:RemoveListener()
    EventManager.RemoveListenerInternal(EnumEventType.OnBuffInfoUpdate, LuaMeiXiangLouMgr.m_BuffInfoUpdateFunc)
end

function LuaMeiXiangLouMgr:Enter()
    LuaMeiXiangLouMgr.m_DrunkBuffId = MeiXiangLou_Setting.GetData().WineBuffId
    LuaMeiXiangLouMgr.m_HighDrunkBuffId = MeiXiangLou_Setting.GetData().HighWineBuffId
    LuaMeiXiangLouMgr:AddListener()

    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer then
        local buffProp = mainPlayer.BuffProp
        if buffProp and buffProp.Buffs then
            if CommonDefs.DictContains(buffProp.Buffs, typeof(UInt32), LuaMeiXiangLouMgr.m_DrunkBuffId) or CommonDefs.DictContains(buffProp.Buffs, typeof(UInt32), LuaMeiXiangLouMgr.m_HighDrunkBuffId) then
                g_ScriptEvent:BroadcastInLua("OnSwitchInviteWinePannel",true)
            end
        end
    end
    LuaMeiXiangLouMgr.m_LastTimePosition = 0
end

function LuaMeiXiangLouMgr:Leave()
    LuaMeiXiangLouMgr:RemoveListener()
    g_ScriptEvent:BroadcastInLua("OnSwitchInviteWinePannel",false)

    if (LuaMeiXiangLouMgr.m_TwoPlayerResultData or LuaMeiXiangLouMgr.m_SingleResultData)then
        CUIManager.ShowUI(CLuaUIResources.MusicPlayHitResultWnd)
        CUIManager.CloseUI(CLuaUIResources.MusicPlayHitingWnd)
    end
end

function LuaMeiXiangLouMgr:OnMainPlayerCreated(sceneTemplateId)
    if LuaMeiXiangLouMgr.IsInMeiXiangLou() then
        LuaMeiXiangLouMgr:Enter()
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId ~= MeiXiangLou_Setting.GetData().SongPlayId then
            if (LuaMeiXiangLouMgr.m_TwoPlayerResultData or LuaMeiXiangLouMgr.m_SingleResultData)then
                CUIManager.ShowUI(CLuaUIResources.MusicPlayHitResultWnd)
                CUIManager.CloseUI(CLuaUIResources.MusicPlayHitingWnd)
            end
        end
    else
        LuaMeiXiangLouMgr:Leave()
    end 
end

function LuaMeiXiangLouMgr.IsInMeiXiangLou()
    if not LuaMeiXiangLouMgr.s_OpenPlay then
        return false
    end
    if not CScene.MainScene then 
        return false 
    end

    local mapId = CScene.MainScene.SceneTemplateId
    local meiXiangLouMap = MeiXiangLou_Setting.GetData().MeiXiangLouSceneId
    local isIn = false
    if mapId == meiXiangLouMap then
        isIn = true
    end
    return isIn
end

function LuaMeiXiangLouMgr.OpenInviteLaBaWnd(pos,itemid)
    LuaMeiXiangLouMgr.m_CurConsumePos = pos
    LuaMeiXiangLouMgr.m_CurConsumeItemId = itemid
    CUIManager.ShowUI(CLuaUIResources.QingJiuLaBaWnd)
end

--有人请酒时的闲话
function LuaMeiXiangLouMgr.OnInvitedWine(inviterEngineId,playerName)
    if not LuaMeiXiangLouMgr.IsInMeiXiangLou() then
        return
    end
    if not inviterEngineId or not playerName then
        return
    end

    local content = g_MessageMgr:FormatMessage("InviteWine_Gossip",playerName)
    local duration = 5

    local objs = CClientObjectMgr.Inst:GetAllObjects()
    --是否是发出邀请的玩家
    CommonDefs.EnumerableIterate(objs, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        if TypeIs(item, typeof(CClientMainPlayer)) or TypeIs(item, typeof(CClientOtherPlayer)) then
            local engineId = item.EngineId
            if engineId and engineId ~= inviterEngineId then
                EventManager.BroadcastInternalForLua(EnumEventType.SpeakingCreate, {engineId,content,EnumSpeakingBubbleType.Default,duration})
            end
        end
    end))
end

LuaMeiXiangLouMgr.m_TargetPlayer = nil
LuaMeiXiangLouMgr.m_PickDelegate =  DelegateFactory.VoidDelegate(function (p)
    if not LuaMeiXiangLouMgr.m_TargetPlayer then return end
    local objectid = LuaMeiXiangLouMgr.m_TargetPlayer.EngineId
    Gac2Gas.LiftUpPlayer(objectid)
end)

function LuaMeiXiangLouMgr:OnBuffInfoUpdate(engineId)
    --不能扶起自己
    local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if obj then
        local buffProp = obj.BuffProp
        if buffProp and buffProp.Buffs then
            if CommonDefs.DictContains_LuaCall(buffProp.Buffs, LuaMeiXiangLouMgr.m_DrunkBuffId) or CommonDefs.DictContains_LuaCall(buffProp.Buffs, LuaMeiXiangLouMgr.m_HighDrunkBuffId) then
                if CClientMainPlayer.Inst and engineId == CClientMainPlayer.Inst.EngineId then
                    g_ScriptEvent:BroadcastInLua("OnSwitchInviteWinePannel",true)
                else
                    LuaMeiXiangLouMgr.m_TargetPlayer = obj
                    LuaMeiXiangLouMgr.ShowPickUpWnd(LocalString.GetString("扶起"),obj.RO.Position,LuaMeiXiangLouMgr.m_PickDelegate)
                    return
                end    
            else
                if CClientMainPlayer.Inst and engineId == CClientMainPlayer.Inst.EngineId then
                    g_ScriptEvent:BroadcastInLua("OnSwitchInviteWinePannel",false)
                end
            end
        end
    end
    LuaMeiXiangLouMgr.HidePickUpWnd()
end

function LuaMeiXiangLouMgr.ShowPickUpWnd(btntext,pPos,pickDelegate)
    LuaSeaFishingMgr.m_CollectionPos = pPos
    LuaSeaFishingMgr.m_PickUpWndDelegate = pickDelegate
    LuaSeaFishingMgr.m_PickBtnText = btntext
    CUIManager.ShowUI(CLuaUIResources.SpecialHaiDiaoPlayPickUpWnd)
end
function LuaMeiXiangLouMgr.HidePickUpWnd()
    if CUIManager.IsLoaded(CLuaUIResources.SpecialHaiDiaoPlayPickUpWnd) then
        CUIManager.CloseUI(CLuaUIResources.SpecialHaiDiaoPlayPickUpWnd)
    end
end

--求救
function LuaMeiXiangLouMgr.AskSomeoneHelp(channel)
    if not CScene.MainScene then 
        return
    end
    local player = CClientMainPlayer.Inst
    if not player then
        return 
    end

    local pos = Utility.PixelPos2GridPos(player.Pos)
    local sceneTemplateId = CScene.MainScene.SceneTemplateId
    local posx = pos.x
    local posy = pos.y

    local link = g_MessageMgr:FormatMessage("MeiXiangLou_AskHelpLiftUp_Msg", sceneTemplateId,posx,posy)
    CChatHelper.SendMsgWithFilterOption(channel,link,0, true)
    CSocialWndMgr.ShowChatWnd(channel)
end

LuaMeiXiangLouMgr.m_IsShowHelpButton = false

------------------------------音游------------------------------
LuaMeiXiangLouMgr.s_ClientBeatSwitch = true

LuaMeiXiangLouMgr.m_OperatorColor = {
    "F8DDFF",
    "B3DEFF",
}

LuaMeiXiangLouMgr.m_PerpormColor = {
    "FF0000",
    "FFFFFF",
    "ffff99",
    "ff9900",
    "FF0000",
}

LuaMeiXiangLouMgr.m_Difficulty2Name = 
{
    LocalString.GetString("简单"),
    LocalString.GetString("中等"),
    LocalString.GetString("困难"),
}

LuaMeiXiangLouMgr.m_RankNameToId = {--songIdx..playerNum..difficullyIdx
--不看仙山看人间
    MeiXiangLouSong111 = 41000248,
    MeiXiangLouSong112 = 41000250,
    MeiXiangLouSong113 = 41000252,
    MeiXiangLouSong121 = 41000249,
    MeiXiangLouSong122 = 41000251,
    MeiXiangLouSong123 = 41000253,
--灼羽
    MeiXiangLouSong211 = 41000236,
    MeiXiangLouSong212 = 41000238,
    MeiXiangLouSong213 = 41000240,
    MeiXiangLouSong221 = 41000237,
    MeiXiangLouSong222 = 41000239,
    MeiXiangLouSong223 = 41000241,
--星火
    MeiXiangLouSong311 = 41000242,
    MeiXiangLouSong312 = 41000244,
    MeiXiangLouSong313 = 41000246,
    MeiXiangLouSong321 = 41000243,
    MeiXiangLouSong322 = 41000245,
    MeiXiangLouSong323 = 41000247,
--牵系
    MeiXiangLouSong411 = 41000268,
    MeiXiangLouSong412 = 41000270,
    MeiXiangLouSong413 = 41000272,
    MeiXiangLouSong421 = 41000269,
    MeiXiangLouSong422 = 41000271,
    MeiXiangLouSong423 = 41000273,
}
function LuaMeiXiangLouMgr.OpenMusicPlaySignWnd()
    CUIManager.ShowUI(CLuaUIResources.MusicPlaySignWnd)
end
function LuaMeiXiangLouMgr.OpenMusicPlayRankWnd(songId)
    LuaMeiXiangLouMgr.CurRankSongId = songId
    CUIManager.ShowUI(CLuaUIResources.MusicPlayRankWnd)
end

function LuaMeiXiangLouMgr.SongPlayTeamResult(totalScore, leaderId,playerId1,name1,class1,gender1, score1, beatNum1, playerId2,name2,class2,gender2, score2, beatNum2)
    LuaMeiXiangLouMgr.m_CurPlayModel = 1
    
    --
    local player = CClientMainPlayer.Inst
    if leaderId == playerId2 then
        LuaMeiXiangLouMgr.m_TwoPlayerResultData = {
            totalScore, playerId2, name2,class2,gender2,score2, beatNum2, playerId1, name1,class1,gender1,score1, beatNum1
        }
    else
        LuaMeiXiangLouMgr.m_TwoPlayerResultData = {
            totalScore, playerId1, name1,class1,gender1,score1, beatNum1, playerId2, name2,class2,gender2,score2, beatNum2
        }
        
    end
end
LuaMeiXiangLouMgr.m_SingleCombo = 0
function LuaMeiXiangLouMgr.SongPlaySingResult(playerId, score, perfect, good, ok, miss,maxCombo)
    LuaMeiXiangLouMgr.m_CurPlayModel = 0
    LuaMeiXiangLouMgr.m_SingleResultData = {
        playerId, score, perfect, good, ok, miss,maxCombo
    }

end

--gas2gac
LuaMeiXiangLouMgr.m_IsLeader = true
function LuaMeiXiangLouMgr.StartSongPlay(songId)
    LuaMeiXiangLouMgr.m_LastTimePosition = 0
    LuaMeiXiangLouMgr.m_SingleCombo = 0
    LuaMeiXiangLouMgr.m_LoseFocusTimeStamp = 0
    LuaMeiXiangLouMgr.m_GetFocusTimeStamp = 0

    local player = CClientMainPlayer.Inst
    if player and player.TeamProp then
        LuaMeiXiangLouMgr.m_IsLeader = player.TeamProp.TeamLeaderId == player.Id
    end
     
    LuaMeiXiangLouMgr.m_CurPlaySongId = songId
    if LuaMeiXiangLouMgr.m_Music then
        SoundManager.Inst:StopSound(LuaMeiXiangLouMgr.m_Music)
        LuaMeiXiangLouMgr.m_Music=nil
    end
    CUIManager.ShowUI(CLuaUIResources.MusicPlayHitingWnd)
end
LuaMeiXiangLouMgr.m_LastTimePosition = 0
LuaMeiXiangLouMgr.m_MusicVolunm = 0
LuaMeiXiangLouMgr.m_XianShanMusicGap = 6996
function LuaMeiXiangLouMgr.SongBeatArrived(songId, teamOp, rhythmId)
    LuaMeiXiangLouMgr.m_TwoPlayerResultData = nil
    LuaMeiXiangLouMgr.m_SingleResultData = nil
    LuaMeiXiangLouMgr.m_CurPlaySongId = songId
    LuaMeiXiangLouMgr.m_IsLeader = (teamOp == 0 or teamOp == 1) and true or false

    if not CUIManager.IsLoaded(CLuaUIResources.MusicPlayHitingWnd) then
		CUIManager.ShowUI(CLuaUIResources.MusicPlayHitingWnd)
	end
    local adjustOff =  0
    if songId <= 6 then--特殊处理不看仙山
        adjustOff = LuaMeiXiangLouMgr.m_XianShanMusicGap
    end

    if not LuaMeiXiangLouMgr.m_Music and LuaMeiXiangLouMgr.m_FocusStatus then
        local songInfo = MeiXiangLou_SongInfo.GetData(songId)
        local info = MeiXiangLou_Song.GetData(rhythmId)
        if songInfo then
            local event = songInfo.SongEvent
            local timeposition = info and info.Delay*1000 or 0
            if rhythmId <= songInfo.StartId then
                timeposition = 0
                LuaMeiXiangLouMgr.m_LastTimePosition = 0
                LuaMeiXiangLouMgr.m_SingleCombo = 0
                LuaMeiXiangLouMgr.m_LoseFocusTimeStamp = 0
                LuaMeiXiangLouMgr.m_GetFocusTimeStamp = 0
            else
                if timeposition/1000 < LuaMeiXiangLouMgr.m_LastTimePosition then
                    LuaMeiXiangLouMgr.m_SingleCombo = 0
                    return
                end
            end           

            LuaMeiXiangLouMgr.m_MusicVolunm = PlayerSettings.MusicEnabled and 1 or LuaMeiXiangLouMgr.m_MusicVolunm
            LuaMeiXiangLouMgr.m_Music = SoundManager.Inst:PlaySound(event,Vector3.zero, LuaMeiXiangLouMgr.m_MusicVolunm, nil, timeposition+adjustOff)
            SoundManager.Inst:AddMarkerCallback(LuaMeiXiangLouMgr.m_Music)
            g_ScriptEvent:BroadcastInLua("ServerSongBeatArrived",songId, teamOp, rhythmId,(timeposition+adjustOff)/1000)--to test
        end
    elseif LuaMeiXiangLouMgr.m_Music then
        local a,musicTimePos = LuaMeiXiangLouMgr.m_Music:getTimelinePosition()
        
        musicTimePos = musicTimePos - adjustOff
        local info = MeiXiangLou_Song.GetData(rhythmId)
        if info then
            local timeposition = info.Delay*1000    
            if (timeposition - musicTimePos) > 1*1000 then
                --print("change",timeposition/1000,musicTimePos/1000)    
                LuaMeiXiangLouMgr.m_Music:setTimelinePosition(timeposition+adjustOff)        
                g_ScriptEvent:BroadcastInLua("ServerSongBeatArrived",songId, teamOp, rhythmId,(timeposition+adjustOff)/1000)
            end
        end    
    end
    
end

function LuaMeiXiangLouMgr.SyncSongPlayPressButton(playerId, buttonIdx, perform, score)
    g_ScriptEvent:BroadcastInLua("SyncSongPlayPressButton",playerId, buttonIdx, perform, score)
end

LuaMeiXiangLouMgr.m_SongId2StartId = nil
LuaMeiXiangLouMgr.m_FocusStatus = true

------------------Merker -> DelayTime -------------
local SoundManager=import "SoundManager"
local CPlayerDataMgr = import "L10.Game.CPlayerDataMgr"
function LuaMeiXiangLouMgr:ExportSongMarkerDelayTime(songId,startId,endId)
    --getTimelinePosition
    if LuaMeiXiangLouMgr.m_MusicPlayTick then
        UnRegisterTick(LuaMeiXiangLouMgr.m_MusicPlayTick)
        LuaMeiXiangLouMgr.m_MusicPlayTick = nil
    end
    LuaMeiXiangLouMgr.m_ExportStr = ""
    SoundManager.s_BlockBgMusic=true
    SoundManager.Inst:StopBGMusic()

    local event = MeiXiangLou_SongInfo.GetData(songId).SongEvent
    if not startId or not endId then
        return
    end

    LuaMeiXiangLouMgr.m_MusicMarkerData = {}
    for idx = startId,endId,1 do
        local data = MeiXiangLou_Song.GetData(idx)
        if data then
            local info = {}
            info.Operator = data.Operator
            local ss = data.Buttons
            info.Buttons = {}
            info.LongPressTime = {}
            for cStr in string.gmatch(ss, "([^;]+)") do
                for btnzhStr in string.gmatch(cStr, "([^,]+)") do
                    local sbtn = tonumber(btnzhStr)
                    if sbtn then
                        table.insert(info.Buttons,sbtn)
                        table.insert(info.LongPressTime,0)
                    else
                        for btnIdx, longPreesTime in string.gmatch(btnzhStr, "(%d+)%(([^%s]-)%)") do
                            table.insert(info.Buttons,tonumber(btnIdx))
                            table.insert(info.LongPressTime,tonumber(longPreesTime))  
                        end
                    end
                end   
            end
            LuaMeiXiangLouMgr.m_MusicMarkerData[data.MusicMarker] = info
        end 
    end
    LuaMeiXiangLouMgr.m_Music = nil
    g_ScriptEvent:RemoveListener("OnFmodMarkerTrigger", self, "OnFmodMarkerTrigger")
    g_ScriptEvent:AddListener("OnFmodMarkerTrigger", self, "OnFmodMarkerTrigger")
    if PlayerSettings.MusicEnabled then
        LuaMeiXiangLouMgr.m_Music = SoundManager.Inst:PlaySound(event,Vector3.zero, 1, nil, 0)
        local len = SoundManager.Inst:GetEventLength(LuaMeiXiangLouMgr.m_Music)
        LuaMeiXiangLouMgr.m_MusicPlayTick = RegisterTick(function()
            g_ScriptEvent:RemoveListener("OnFmodMarkerTrigger", self, "OnFmodMarkerTrigger")
            SoundManager.Inst:StopSound(LuaMeiXiangLouMgr.m_Music) 
            CPlayerDataMgr.Inst:SavePlayerData("MeiXiangLouMarkerDelay",LuaMeiXiangLouMgr.m_ExportStr)    
        end, len)
        SoundManager.Inst:AddMarkerCallback(LuaMeiXiangLouMgr.m_Music)
    end
end

LuaMeiXiangLouMgr.m_ExportStr = ""
function LuaMeiXiangLouMgr:OnFmodMarkerTrigger(params)
    local path = params[0]
    local name = params[1]
    local position = params[2]
    local mark = name
    if LuaMeiXiangLouMgr.m_MusicMarkerData[mark] then
        
        
        LuaMeiXiangLouMgr.m_ExportStr = SafeStringFormat3("%s\n%s,%s",LuaMeiXiangLouMgr.m_ExportStr,mark,position/1000 - 1)
        LuaMeiXiangLouMgr.m_MusicMarkerData[mark] = nil
    end
    if LuaMeiXiangLouMgr.m_MusicMarkerData == {} then
        print("over!!!!!!")
    end
end