-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local ChujiaItem = import "L10.UI.ChujiaItem"
local ChujiaItemData = import "L10.UI.ChujiaListWnd+ChujiaItemData"
local ChujiaListWnd = import "L10.UI.ChujiaListWnd"
local ChujiaMgr = import "L10.Game.ChujiaMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Quaternion = import "UnityEngine.Quaternion"
local String = import "System.String"
local UInt32 = import "System.UInt32"
local UInt64 = import "System.UInt64"
local Vector3 = import "UnityEngine.Vector3"
ChujiaListWnd.m_Init_CS2LuaHook = function (this) 
    this:ClearData()
    this.titleLabel.text = nil
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end
    local infos = nil
    if ChujiaMgr.Type == ChujiaMgr.ChujiaListType.Visit then
        this.titleLabel.text = LocalString.GetString("探望好友")
        infos = ChujiaMgr.Inst:GetChujiaPlayersToVisit()
    elseif ChujiaMgr.Type == ChujiaMgr.ChujiaListType.Ransom then
        this.titleLabel.text = LocalString.GetString("好友赎身")
        infos = ChujiaMgr.Inst:GetChujiaPlayersToRansom()
    end
    if infos ~= nil then
        local itemInfos = CreateFromClass(MakeGenericClass(List, ChujiaItemData))
        CommonDefs.ListIterate(infos, DelegateFactory.Action_object(function (___value) 
            local player = ___value
            local info = CreateFromClass(ChujiaItemData, player.ID, player.Level, player.Name, CUICommonDef.GetPortraitName(player.Class, player.Gender, player.Expression), player.Silver)
            CommonDefs.ListAdd(itemInfos, typeof(ChujiaItemData), info)
        end))
        this:LoadData(itemInfos)
    end
end
ChujiaListWnd.m_LoadData_CS2LuaHook = function (this, itemInfos) 
    this:ClearData()
    do
        local i = 0
        while i < itemInfos.Count do
            local instance = TypeAs(GameObject.Instantiate(this.itemTemplate, Vector3.zero, Quaternion.identity), typeof(GameObject))
            instance.transform.parent = this.table.transform
            instance.transform.localScale = Vector3.one
            local item = CommonDefs.GetComponent_GameObject_Type(instance, typeof(ChujiaItem))
            if ChujiaMgr.Type == ChujiaMgr.ChujiaListType.Visit then
                item:Init(itemInfos[i].playerId, itemInfos[i].name, itemInfos[i].portraitName, itemInfos[i].level, LocalString.GetString("探望"), itemInfos[i].silver)
                item.OnOpButtonClickDelegate = MakeDelegateFromCSFunction(this.visitFriend, MakeGenericClass(Action3, UInt64, String, UInt32), this)
            elseif ChujiaMgr.Type == ChujiaMgr.ChujiaListType.Ransom then
                item:Init(itemInfos[i].playerId, itemInfos[i].name, itemInfos[i].portraitName, itemInfos[i].level, LocalString.GetString("赎身"), itemInfos[i].silver)
                item.OnOpButtonClickDelegate = MakeDelegateFromCSFunction(this.ransomFriend, MakeGenericClass(Action3, UInt64, String, UInt32), this)
            end
            instance:SetActive(true)
            i = i + 1
        end
    end

    this.table:Reposition()
    this.scrollView:ResetPosition()
end
ChujiaListWnd.m_visitFriend_CS2LuaHook = function (this, playerId, name, silverNeed) 
    local message = g_MessageMgr:FormatMessage("CHUJIA_VISIT_CONFIRM", name, silverNeed)
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
        if CClientMainPlayer.Inst.Silver > silverNeed then
            Gac2Gas.ConfirmVisitPlayerPay(playerId)
            CUIManager.CloseUI(CUIResources.ChujiaListWnd)
        else
            g_MessageMgr:ShowMessage("CHUJIA_VISIT_LESS_MONEY")
        end
    end), nil, nil, nil, false)
end
ChujiaListWnd.m_ransomFriend_CS2LuaHook = function (this, playerId, name, silverNeed) 
    local message = g_MessageMgr:FormatMessage("CHUJIA_RANSOM_CONFIRM", name, silverNeed)
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
        if CClientMainPlayer.Inst.Silver >= silverNeed then
            Gac2Gas.ConfirmRansomChuJiaPlayerPay(playerId)
            g_MessageMgr:ShowMessage("CHUJIA_ASK_RANSOM", name)
        else
            g_MessageMgr:ShowMessage("CHUJIA_RANSOM_LESS_MONEY")
        end
    end), nil, nil, nil, false)
end
