local LuaUtils = import "LuaUtils"
local UIEventListener = import "UIEventListener"
local Extensions = import "Extensions"
local UILabel = import "UILabel"
local NGUIText = import "NGUIText"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UIGrid = import "UIGrid"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local Vector3 = import "UnityEngine.Vector3"
local AlignType = import "CPlayerInfoMgr+AlignType"

LuaBaoZhuPlayEndWnd = class()
RegistChildComponent(LuaBaoZhuPlayEndWnd,"CloseButton", GameObject)
RegistChildComponent(LuaBaoZhuPlayEndWnd,"Btn", GameObject)
RegistChildComponent(LuaBaoZhuPlayEndWnd,"TableBody", CUIRestrictScrollView)
RegistChildComponent(LuaBaoZhuPlayEndWnd,"Grid", UIGrid)
RegistChildComponent(LuaBaoZhuPlayEndWnd,"ItemTemplate", GameObject)

--RegistClassMember(LuaBaoZhuPlayEndWnd, "m_HideExcept")
--RegistClassMember(LuaBaoZhuPlayEndWnd,"m_EvaluateInterval")
--RegistClassMember(LuaBaoZhuPlayEndWnd,"m_LastEvaluateTime")

function LuaBaoZhuPlayEndWnd:Close()
  CUIManager.CloseUI("BaoZhuPlayEndWnd")
end

function LuaBaoZhuPlayEndWnd:InitRankNodeData(node,data)
  node:SetActive(true)
  local rankLabel = node.transform:Find('RankLabel'):GetComponent(typeof(UILabel))
  local rankSprite = node.transform:Find('RankLabel/RankImage'):GetComponent(typeof(UISprite))
  rankSprite.gameObject:SetActive(true)
  local rank=data.rank
  if rank==1 then
    rankSprite.spriteName="headinfownd_jiashang_01"
  elseif rank==2 then
    rankSprite.spriteName="headinfownd_jiashang_02"
  elseif rank==3 then
    rankSprite.spriteName="headinfownd_jiashang_03"
  else
    rankLabel.text=tostring(rank)
    rankSprite.gameObject:SetActive(false)
  end

  node.transform:Find('NameLabel'):GetComponent(typeof(UILabel)).text = data.name
  node.transform:Find('ScoreLabel3'):GetComponent(typeof(UILabel)).text = data.score
  node.transform:Find('ScoreLabel1'):GetComponent(typeof(UILabel)).text = data.score1
  node.transform:Find('ScoreLabel2'):GetComponent(typeof(UILabel)).text = data.score2

  local bg = node.transform:Find('bg').gameObject
  bg:SetActive(false)

  if data.playerId == CClientMainPlayer.Inst.Id then
    bg:SetActive(true)
    local selfColor = NGUIText.ParseColor24("ffc800", 0)
    node.transform:Find('RankLabel'):GetComponent(typeof(UILabel)).color = selfColor
    node.transform:Find('NameLabel'):GetComponent(typeof(UILabel)).color = selfColor
    node.transform:Find('ScoreLabel3'):GetComponent(typeof(UILabel)).color = selfColor
    node.transform:Find('ScoreLabel1'):GetComponent(typeof(UILabel)).color = selfColor
    node.transform:Find('ScoreLabel2'):GetComponent(typeof(UILabel)).color = selfColor
  end
  local nameBtn = node.transform:Find('NameLabel').gameObject
  UIEventListener.Get(nameBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
    CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
  end)
end

function LuaBaoZhuPlayEndWnd:InitRank()
	Extensions.RemoveAllChildren(self.Grid.transform)
  --table.insert(LuaBaoZhuPlayMgr.ResultData,{rankPos, playerId, playerName, hideScore, deliverScore, totalScore})
  for i,v in ipairs(LuaBaoZhuPlayMgr.ResultData) do
    local go = NGUITools.AddChild(self.Grid.gameObject, self.ItemTemplate)
    self:InitRankNodeData(go,v)
  end

  self.Grid:Reposition()
  self.TableBody:ResetPosition()
end

function LuaBaoZhuPlayEndWnd:Init()
  UIEventListener.Get(self.CloseButton).onClick = LuaUtils.VoidDelegate(function ( ... )
    self:Close()
  end)

  UIEventListener.Get(self.Btn).onClick = LuaUtils.VoidDelegate(function ( ... )
    LuaBaoZhuPlayMgr.OpenPlayRankWnd()
    self:Close()
  end)
  self.ItemTemplate:SetActive(false)

  self:InitRank()
end

function LuaBaoZhuPlayEndWnd:OnDestroy()
end

function LuaBaoZhuPlayEndWnd:OnEnable( ... )
	--g_ScriptEvent:AddListener("SyncNewYearPrizeDrawIndex", self, "SyncDrawIndex")
end

function LuaBaoZhuPlayEndWnd:OnDisable( ... )
	--g_ScriptEvent:RemoveListener("SyncNewYearPrizeDrawIndex", self, "SyncDrawIndex")
end


return LuaBaoZhuPlayEndWnd
