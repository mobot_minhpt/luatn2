local CTitleDescTemplate=import "L10.UI.CTitleDescTemplate"
local UInt64StringKeyValuePair = import "L10.Game.UInt64StringKeyValuePair"
local NGUIText = import "NGUIText"
local Extensions = import "Extensions"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Color = import "UnityEngine.Color"
local CItemMgr = import "L10.Game.CItemMgr"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CClientChuanJiaBaoMgr = import "L10.Game.CClientChuanJiaBaoMgr"
local CARMgr = import "L10.Game.CARMgr"
local CUIRestrictScrollView=import "L10.UI.CUIRestrictScrollView"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"

CLuaChuanjiabaoInfoWindow = class()
RegistClassMember(CLuaChuanjiabaoInfoWindow,"scrollview")
RegistClassMember(CLuaChuanjiabaoInfoWindow,"contentTable")
RegistClassMember(CLuaChuanjiabaoInfoWindow,"nameLabel")
RegistClassMember(CLuaChuanjiabaoInfoWindow,"levelLabel")
RegistClassMember(CLuaChuanjiabaoInfoWindow,"playerRequireLabel")
RegistClassMember(CLuaChuanjiabaoInfoWindow,"shareButton")
RegistClassMember(CLuaChuanjiabaoInfoWindow,"getoutButton")
RegistClassMember(CLuaChuanjiabaoInfoWindow,"ARButton")
RegistClassMember(CLuaChuanjiabaoInfoWindow,"labelTemplate")
RegistClassMember(CLuaChuanjiabaoInfoWindow,"titleDescTemplate")
RegistClassMember(CLuaChuanjiabaoInfoWindow,"starTemplate")
RegistClassMember(CLuaChuanjiabaoInfoWindow,"wordTemplate")
RegistClassMember(CLuaChuanjiabaoInfoWindow,"spaceTemplate")
RegistClassMember(CLuaChuanjiabaoInfoWindow,"labelTemplate2")
RegistClassMember(CLuaChuanjiabaoInfoWindow,"highlightStar")
RegistClassMember(CLuaChuanjiabaoInfoWindow,"normalStar")
RegistClassMember(CLuaChuanjiabaoInfoWindow,"inited")
RegistClassMember(CLuaChuanjiabaoInfoWindow,"item")
RegistClassMember(CLuaChuanjiabaoInfoWindow,"selectedItemId")

function CLuaChuanjiabaoInfoWindow:Awake()
    self.scrollview = self.transform:Find("Info/Tips/Scrollview"):GetComponent(typeof(CUIRestrictScrollView))
    self.contentTable = self.transform:Find("Info/Tips/Scrollview/table"):GetComponent(typeof(UITable))
    self.nameLabel = self.transform:Find("Info/Name"):GetComponent(typeof(UILabel))
    self.levelLabel = self.transform:Find("Info/Name/Level"):GetComponent(typeof(UILabel))
    self.playerRequireLabel = self.transform:Find("Info/Name/Level/playerLevel"):GetComponent(typeof(UILabel))
    self.shareButton = self.transform:Find("ShareButton").gameObject
    self.getoutButton = self.transform:Find("GetOutButton").gameObject
    self.ARButton = self.transform:Find("ARButton").gameObject
    self.labelTemplate = self.transform:Find("Info/Tips/Scrollview/LabelTemplate").gameObject
    self.titleDescTemplate = self.transform:Find("Info/Tips/Scrollview/TitleDescTemplate").gameObject
    self.starTemplate = self.transform:Find("Info/Tips/Scrollview/StarTemplate").gameObject
    self.wordTemplate = self.transform:Find("Info/Tips/Scrollview/WordTemplate").gameObject
    self.spaceTemplate = self.transform:Find("Info/Tips/Scrollview/SpaceTemplate").gameObject
    self.labelTemplate2 = self.transform:Find("Info/Tips/Scrollview/LabelTemplate2").gameObject
    self.highlightStar = "playerAchievementView_star_1"
    self.normalStar = "playerAchievementView_star_2"
    self.inited = false
    self.item = nil
    self.selectedItemId = nil


    UIEventListener.Get(self.getoutButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnGetOutButtonClick(go)
    end)

    UIEventListener.Get(self.ARButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnARButtonClick(go)
    end)
end

function CLuaChuanjiabaoInfoWindow:Init( itemId, _chuanjiabaoShelf) 

    self.ARButton:SetActive(CARMgr.Inst.EnableAR and CARMgr.Inst:IsPlatformSupported())

    if self.inited then
        return
    end
    self.inited = true
    self.selectedItemId = itemId
    --chuanjiabaoShelf = _chuanjiabaoShelf;
    self.item = CItemMgr.Inst:GetById(itemId)
    if self.item == nil then
        return
    end
    self.nameLabel.text = self.item.Name
    local cjbGrade = Item_Item.GetData(self.item.TemplateId)
    if cjbGrade ~= nil then
        self.levelLabel.text = cjbGrade.Name
    else
        self.levelLabel.text = LocalString.GetString("传家宝")
    end

    self.playerRequireLabel.text = System.String.Format(LocalString.GetString("{0}级"), self.item.Grade)

    self:InitTips()
    self.shareButton.gameObject:SetActive(CSwitchMgr.EnableCJBShare)
end
function CLuaChuanjiabaoInfoWindow:InitTips( )
    Extensions.RemoveAllChildren(self.contentTable.transform)

    local data = self.item.Item.ChuanjiabaoItemInfo
    local wordInfo = data.WordInfo
    local washedStar = wordInfo.WashedStar or 0

    local cjbTemplate = Item_Item.GetData(self.item.TemplateId)

    local star =washedStar>0 and washedStar or wordInfo.Star
    local name = cjbTemplate.Name
    local cjbSetting = House_ChuanjiabaoSetting.GetData()
    local lv = 0
    if string.find(name,LocalString.GetString("1级")) then
        lv = cjbSetting.ChuanjiabaoLowLevel
    elseif string.find(name,LocalString.GetString("2级")) then
        lv = cjbSetting.ChuanjiabaoNormalLevel
    elseif string.find(name,LocalString.GetString("3级")) then
        lv = cjbSetting.ChuanjiabaoHighLevel
    end
    local numOfWord = wordInfo.Words.Count
    if IsZhuShaBiXiLianOpen() then
        self:AddTitleDesc(LocalString.GetString("基础属性"),"")
        self:AddItemToTable(SafeStringFormat3("   "..LocalString.GetString("家园基础属性 %.2f%%"),CLuaZhuShaBiXiLianMgr.GetWashBaseProp(lv,star,numOfWord)*100), Color.white)
        self:AddItemToTable(SafeStringFormat3("   "..LocalString.GetString("向天借命几率 %.2f%%"), CLuaZhuShaBiXiLianMgr.GetBorrowLifeProp(star,numOfWord)*100), Color.white)
    
        if data.WordInfo.Type == EChuanjiabaoType_lua.RenGe then
            local zhushabiData = ChuanjiabaoModel_RenGeWordOption.GetData(data.WordInfo.TypeParam)
            local zhushabiClasses = ChuanjiabaoModel_ZhuShaBiSubType.GetData(zhushabiData.ZhushabiSubType).Classes
            local classListStr = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), zhushabiClasses[0]))
            for i = 1, zhushabiClasses.Length - 1 do
                classListStr = classListStr..LocalString.GetString("、")..Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), zhushabiClasses[i]))
            end
            self:AddItemToTable(SafeStringFormat3("   "..LocalString.GetString("职业 %s"), classListStr), Color.white)
        end
    end
    if washedStar>0 then--洗过
        self:AddStar(LocalString.GetString("星级"), wordInfo.Star,data.WordInfo.WashedStar or 0)
    else--没洗过
        self:AddStar(LocalString.GetString("星级"), wordInfo.Star,wordInfo.Star)
    end

    -- self:AddStar(LocalString.GetString("星级"), data.WordInfo.Star, ChuanjiabaoModel_Setting.GetData().Chuanjiabao_Total_Score)

    local zsw = CClientFurnitureMgr.Inst:GetChuanjiabaoInZhuangshiwuData(self.item.TemplateId)
    if zsw ~= nil then
        local c = data.BasicInfo.Duration > 0 and Color.white or Color.red
        local durationText = System.String.Format("[c][{0}]{1}/{2}[-][/c]", NGUIText.EncodeColor24(c), data.BasicInfo.Duration, zsw.Duration)

        self:AddTitleDesc(LocalString.GetString("耐久度"), durationText)
        local location = Zhuangshiwu_Location.GetData(zsw.Location)
        if location ~= nil then
            self:AddTitleDesc(LocalString.GetString("可置于"), location.Location)
        end
    end

    self:AddSpace()
    local wordInfo = data.WordInfo
    local words = wordInfo.Words
    CommonDefs.DictIterate(words, DelegateFactory.Action_object_object(function (___key, ___value) 
        local word = Word_Word.GetData(___key)
        if word ~= nil then
            self:AddWord(System.String.Format(LocalString.GetString("[c][{2}]【{0}】{1}[-]"), word.Name, word.Description, CClientChuanJiaBaoMgr.Inst:GetChuanjiabaoWordColor(words.Count)), Color.white)
        end
    end))

    if CLuaZhuShaBiXiLianMgr.CanXiLian(wordInfo) then
        if wordInfo.WashCount and wordInfo.WashCount>0 then
            self:AddSpace()
            self:AddTitleDesc(LocalString.GetString("洗炼积分"),  wordInfo.WashCount)
        else
            self:AddSpace()
            self:AddItemToTable(LocalString.GetString("可洗炼"),  Color.yellow)
        end
    end

    self.nameLabel.text = System.String.Format("[c][{0}]{1}[-]", CClientChuanJiaBaoMgr.Inst:GetChuanjiabaoWordColor(words.Count), data.AppearInfo.Name)

    self:AddSpace()
    CClientChuanJiaBaoMgr.Inst.zhiciArtist = CreateFromClass(UInt64StringKeyValuePair, data.AppearInfo.Artist, "")
    CClientChuanJiaBaoMgr.Inst.jianbiArtist = CreateFromClass(UInt64StringKeyValuePair, data.WordInfo.JiandingArtist, "")
    CClientChuanJiaBaoMgr.Inst.kaiguangArtist = CreateFromClass(UInt64StringKeyValuePair, data.BasicInfo.KaiguangArtist, "")
    CClientChuanJiaBaoMgr.Inst.currentRequestItemId = self.item.Id
    Gac2Gas.RequestChuanjiabaoArtistName(data.AppearInfo.Artist, data.WordInfo.JiandingArtist, data.BasicInfo.KaiguangArtist)

    self.contentTable:Reposition()
    self.scrollview:ResetPosition()
end
function CLuaChuanjiabaoInfoWindow:AddItemToTable( text, color) 
    local instance = NGUITools.AddChild(self.contentTable.gameObject, self.labelTemplate)
    instance:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(UILabel)).text = text
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(UILabel)).color = color
end

function CLuaChuanjiabaoInfoWindow:AddTitleDesc(title,desc)
    local instance = NGUITools.AddChild(self.contentTable.gameObject, self.titleDescTemplate)
    instance:SetActive(true)
    instance:GetComponent(typeof(CTitleDescTemplate)):Init(title, desc)
end

function CLuaChuanjiabaoInfoWindow:AddStar( title, star, washedStar) 
    local instance = NGUITools.AddChild(self.contentTable.gameObject, self.starTemplate)
    instance:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(UILabel)).text = title
    local table = CommonDefs.GetComponentInChildren_GameObject_Type(instance, typeof(UITable))
    local sprite = CommonDefs.GetComponentInChildren_GameObject_Type(instance, typeof(UISprite))
    sprite.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(table.transform)
    local c1 = math.max(star,washedStar)
    local c2 = ChuanjiabaoModel_Setting.GetData().Chuanjiabao_Total_Score
    for i=1,c2 do
        local g = NGUITools.AddChild(table.gameObject, sprite.gameObject)
        g:SetActive(true)

        local sprite = g:GetComponent(typeof(UISprite))
        sprite.alpha = 1

        if i<=c1 then
            sprite.spriteName = "playerAchievementView_star_1"
            if i<=star and i>washedStar then
                sprite.alpha = 0.3
            end
        else
            sprite.spriteName = "playerAchievementView_star_2"
        end
    end

    table:Reposition()
end

function CLuaChuanjiabaoInfoWindow:AddWord( word, color)
    local instance = NGUITools.AddChild(self.contentTable.gameObject, self.wordTemplate)
    instance:SetActive(true)
    instance.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("[c][%s]%s[-][/c]", NGUIText.EncodeColor24(color), word)
end


function CLuaChuanjiabaoInfoWindow:AddLabel2(label1,label2)
    local instance = NGUITools.AddChild(self.contentTable.gameObject, self.labelTemplate2)
    instance:SetActive(true)
    instance:GetComponent(typeof(CTitleDescTemplate)):Init(label1, label2)
end

function CLuaChuanjiabaoInfoWindow:AddSpace()
    local instance = NGUITools.AddChild(self.contentTable.gameObject, self.spaceTemplate)
    instance:SetActive(true)
end

function CLuaChuanjiabaoInfoWindow:OnEnable()
    g_ScriptEvent:AddListener("RequestChuanjiabaoArtistResult",self,"OnChuanjiabaoArtistResult")
end
function CLuaChuanjiabaoInfoWindow:OnDisable()
    g_ScriptEvent:RemoveListener("RequestChuanjiabaoArtistResult",self,"OnChuanjiabaoArtistResult")
    self.inited = false
end

function CLuaChuanjiabaoInfoWindow:OnChuanjiabaoArtistResult( )
    local item = CItemMgr.Inst:GetById(CClientChuanJiaBaoMgr.Inst.currentRequestItemId)
    if item ~= nil and item.IsItem and item.Item.Type == EnumItemType_lua.Chuanjiabao then
        local data = item.Item.ChuanjiabaoItemInfo
        if CClientChuanJiaBaoMgr.Inst.zhiciArtist.Key == data.AppearInfo.Artist then
            self:AddItemToTable(System.String.Format(LocalString.GetString("{0}[c][acf8ff]制瓷[-]"), CClientChuanJiaBaoMgr.Inst.zhiciArtist.Value), Color.white)
        end
        if CClientChuanJiaBaoMgr.Inst.jianbiArtist.Key == data.WordInfo.JiandingArtist then
            self:AddItemToTable(System.String.Format(LocalString.GetString("{0}[c][acf8ff]定笔[-]"), CClientChuanJiaBaoMgr.Inst.jianbiArtist.Value), Color.white)
        end
        local time = CServerTimeMgr.ConvertTimeStampToZone8Time(data.BasicInfo.KaiguangTime)
        if CClientChuanJiaBaoMgr.Inst.kaiguangArtist.Key == data.BasicInfo.KaiguangArtist then
            self:AddLabel2(System.String.Format(LocalString.GetString("{0}[c][acf8ff]开光[-]"), CClientChuanJiaBaoMgr.Inst.kaiguangArtist.Value), System.String.Format(LocalString.GetString("{0}[c][acf8ff]年[-]{1}[c][acf8ff]月[-]{2}[c][acf8ff]日[-]"), time.Year, time.Month, time.Day))
        end
    end

    self.contentTable:Reposition()
    self.scrollview:ResetPosition()
end


function CLuaChuanjiabaoInfoWindow:OnGetOutButtonClick(go)
    Gac2Gas.MoveChuanjiabaoFromHouseToBag(CClientChuanJiaBaoMgr.Inst.currentSelectSuite, math.floor(self.item.Item.ChuanjiabaoItemInfo.WordInfo.Type / 100), self.item.Id)
end

function CLuaChuanjiabaoInfoWindow:OnARButtonClick(go)
    CARMgr.Inst:ShowChuanJiaBao(self.selectedItemId)
end
