local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CItemMgr = import "L10.Game.CItemMgr"
local CUITexture = import "L10.UI.CUITexture"
local Animation = import "UnityEngine.Animation"
local CForcesMgr = import "L10.Game.CForcesMgr"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"

LuaGuoQing2023JingYuanResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuoQing2023JingYuanResultWnd, "TimeLabel", UILabel)

RegistChildComponent(LuaGuoQing2023JingYuanResultWnd, "Team1", GameObject)
RegistChildComponent(LuaGuoQing2023JingYuanResultWnd, "Team2", GameObject)
RegistChildComponent(LuaGuoQing2023JingYuanResultWnd, "PlayerTemplate", GameObject)
RegistChildComponent(LuaGuoQing2023JingYuanResultWnd, "PlayerTemplate_2", GameObject)

RegistChildComponent(LuaGuoQing2023JingYuanResultWnd, "DailyReward", GameObject)
RegistChildComponent(LuaGuoQing2023JingYuanResultWnd, "CompetitionReward", GameObject)

RegistChildComponent(LuaGuoQing2023JingYuanResultWnd, "AgainBtn", GameObject)
RegistChildComponent(LuaGuoQing2023JingYuanResultWnd, "ShareBtn", GameObject)

RegistChildComponent(LuaGuoQing2023JingYuanResultWnd, "JingShiTable", UITable)
RegistChildComponent(LuaGuoQing2023JingYuanResultWnd, "UpTemplate", GameObject)
RegistChildComponent(LuaGuoQing2023JingYuanResultWnd, "DownTemplate", GameObject)

RegistClassMember(LuaGuoQing2023JingYuanResultWnd, "m_Animation")
RegistClassMember(LuaGuoQing2023JingYuanResultWnd, "m_JingShiList")
RegistClassMember(LuaGuoQing2023JingYuanResultWnd, "m_VfxTick")

--@endregion RegistChildComponent end

function LuaGuoQing2023JingYuanResultWnd:Awake()
    self.m_Animation = self.gameObject:GetComponent(typeof(Animation))
    UIEventListener.Get(self.AgainBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnAgainBtnClick()
    end)
    UIEventListener.Get(self.ShareBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnShareBtnClick()
    end)
end

function LuaGuoQing2023JingYuanResultWnd:Init()
    self.PlayerTemplate:SetActive(false)
    self.PlayerTemplate_2:SetActive(false)
    self.UpTemplate:SetActive(false)
    self.DownTemplate:SetActive(false)
    self.TimeLabel.text = LuaGamePlayMgr:GetRemainTimeText(LuaGuoQing2023Mgr.m_JingYuanPlay_ResultInfo.playTime)

    self:InitTeam(1, self.Team1, self.PlayerTemplate)
    self:InitTeam(2, self.Team2, self.PlayerTemplate_2)

    local setData = GuoQing2023_JingYuanPlay.GetData()
    self:InitDailyReward(setData.DailyRewardId)
    self:InitCompetitionReward(setData.CompetitiveRewardId)
    self:InitJingYuan()
    self:PlayAnimation()
end

function LuaGuoQing2023JingYuanResultWnd:PlayAnimation()
    if LuaGuoQing2023Mgr.m_JingYuanPlay_ResultInfo.win == 1 then
        self.m_Animation:Play("guoqing2023jingyuanresultwnd_win")
    elseif LuaGuoQing2023Mgr.m_JingYuanPlay_ResultInfo.win == 0 then
        self.m_Animation:Play("guoqing2023jingyuanresultwnd_lose")
    else
        self.m_Animation:Play("guoqing2023jingyuanresultwnd_draw")
    end
end

function LuaGuoQing2023JingYuanResultWnd:InitTeam(teamId, teamView, template)
    local teamInfo = LuaGuoQing2023Mgr.m_JingYuanPlay_ResultInfo.teamInfo[teamId]
    teamView.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel)).text = tostring(teamInfo.totalJingYuanCount)
    local table = teamView.transform:Find("PlayerTable"):GetComponent(typeof(UITable))
	Extensions.RemoveAllChildren(table.transform)
    for i = 1, #teamInfo.playerInfo do
		local cell = NGUITools.AddChild(table.gameObject, template)
        cell.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = teamInfo.playerInfo[i].name
        self:InitPlayerInfo(cell, teamInfo.playerInfo[i])
    end
end

function LuaGuoQing2023JingYuanResultWnd:InitPlayerInfo(cell, playerInfo)
    cell.gameObject:SetActive(true)
    local color = playerInfo.playerId == CClientMainPlayer.Inst.Id and "00ff00" or "ffffff"
    local nameLabel = cell.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text = SafeStringFormat3("[%s]%s", color, playerInfo.name)
    UIEventListener.Get(nameLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CPlayerInfoMgr.ShowPlayerPopupMenu(playerInfo.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, nameLabel.transform.position, AlignType.Right)
    end)
    cell.transform:Find("TalentCountLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("[%s]%s", color, playerInfo.kill)
    cell.transform:Find("JingYuanCountLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("[%s]%s", color, playerInfo.jingyuanCount)
end

function LuaGuoQing2023JingYuanResultWnd:InitDailyReward(itemId)
    local getEngageReward = LuaGuoQing2023Mgr.m_JingYuanPlay_ResultInfo.rewardInfo.getEngageReward
    self.DailyReward.transform:Find("Get").gameObject:SetActive(getEngageReward == 1)
    self.DailyReward.transform:Find("Full").gameObject:SetActive(getEngageReward == 0)
    self.DailyReward.transform:Find("Get/ItemCell"):GetComponent(typeof(CQnReturnAwardTemplate)):Init(CItemMgr.Inst:GetItemTemplate(itemId), 1)
end

function LuaGuoQing2023JingYuanResultWnd:InitCompetitionReward(itemId)
    local rewardInfo = LuaGuoQing2023Mgr.m_JingYuanPlay_ResultInfo.rewardInfo
    self.CompetitionReward.transform:Find("Get").gameObject:SetActive(rewardInfo.getExtraReward == 1)
    self.CompetitionReward.transform:Find("Not").gameObject:SetActive(rewardInfo.getExtraReward == 0 and rewardInfo.getPieceCount > 0)
    local jingShiCount = rewardInfo.maxPieceCount / 3
    self.CompetitionReward.transform:Find("Lock").gameObject:SetActive(rewardInfo.getExtraReward == 0 and rewardInfo.getPieceCount == 0 and jingShiCount < 9)
    self.CompetitionReward.transform:Find("Full").gameObject:SetActive(rewardInfo.getExtraReward == 0 and rewardInfo.getPieceCount == 0 and jingShiCount >= 9)
    self.CompetitionReward.transform:Find("Get/ItemCell"):GetComponent(typeof(CQnReturnAwardTemplate)):Init(CItemMgr.Inst:GetItemTemplate(itemId), 1)
end

function LuaGuoQing2023JingYuanResultWnd:InitJingYuan()
    local rewardInfo = LuaGuoQing2023Mgr.m_JingYuanPlay_ResultInfo.rewardInfo
    local countLabel = self.CompetitionReward.transform:Find("CountLabel/Label"):GetComponent(typeof(UILabel))
    countLabel.text = SafeStringFormat3("+%d", rewardInfo.getPieceCount)
    countLabel.transform.parent.gameObject:SetActive(rewardInfo.getPieceCount > 0)
    local unlock = rewardInfo.maxPieceCount / 3
	Extensions.RemoveAllChildren(self.JingShiTable.transform)
    local jingshiList = {}      -- 小三角
    self.m_JingShiList = {}     -- 大三角
    for i = 1, 9 do
        local template = i % 2 == 1 and self.UpTemplate or self.DownTemplate
        local cell = NGUITools.AddChild(self.JingShiTable.gameObject, template)
        cell.gameObject:SetActive(true)
        table.insert(self.m_JingShiList, cell)
        table.insert(jingshiList, {index1 = i, index2 = 1, cellTexture = cell.transform:Find("1"):GetComponent(typeof(CUITexture))})
        table.insert(jingshiList, {index1 = i, index2 = 2, cellTexture = cell.transform:Find("2"):GetComponent(typeof(CUITexture))})
        table.insert(jingshiList, {index1 = i, index2 = 3, cellTexture = cell.transform:Find("3"):GetComponent(typeof(CUITexture))})
        cell.transform:Find("lock").gameObject:SetActive(i > unlock)
    end
    self.JingShiTable:Reposition()
    self:SetJingShiStatus(rewardInfo, jingshiList)
end

function LuaGuoQing2023JingYuanResultWnd:SetJingShiStatus(rewardInfo, jingshiList)
    local img = {
        "UI/Texture/FestivalActivity/Festival_GuoQing/GuoQing2023/Material/guoqing2023jingyuanenterview_jingyuan__normal.mat",
        "UI/Texture/FestivalActivity/Festival_GuoQing/GuoQing2023/Material/guoqing2023jingyuanenterview_jingyuan_highlight.mat",
    }
    -- 小三角切图状态
    local lightCount = 0
    local showVFXList = {}
    for i = 1, #jingshiList do
        if lightCount < rewardInfo.hasPieceCount then
            print(rewardInfo.hasPieceCount , lightCount , rewardInfo.getPieceCount)
            if rewardInfo.hasPieceCount - lightCount <= rewardInfo.getPieceCount then
                jingshiList[i].cellTexture:LoadMaterial(img[1])
                table.insert(showVFXList, {index = jingshiList[i].index1, vfx = "vfx_single", small = jingshiList[i].cellTexture})
                if jingshiList[i].index2 == 3 then
                    table.insert(showVFXList, {index = jingshiList[i].index1, vfx = "vfx_group"})
                end
            else
                jingshiList[i].cellTexture:LoadMaterial(img[2])
            end
            lightCount = lightCount + 1
        else
            jingshiList[i].cellTexture:LoadMaterial(img[1])
        end
    end
    self:ShowJingShiFX(showVFXList)
end

function LuaGuoQing2023JingYuanResultWnd:ShowJingShiFX(showVFXList)
    local img = "Fx/Vfx/FestivalActivity/Festival_GuoQing/GuoQing2023/Materials/vfx_guoqing2023jingyuanenterview_jingyuan_baodian.mat"
    local delayTime = LuaGuoQing2023Mgr.m_JingYuanPlay_ResultInfo.win == 1 and 2000 or 1500
    local intervalTime = 500
    
    local index = 1
    if self.m_VfxTick then UnRegisterTick(self.m_VfxTick) end
    local showFx = function()
        if index > #showVFXList then
            if self.m_VfxTick then UnRegisterTick(self.m_VfxTick) end
            return
        end
        local cell = self.m_JingShiList[showVFXList[index].index]
        local vfx = cell.transform:Find(showVFXList[index].vfx)
        if showVFXList[index].small then showVFXList[index].small:LoadMaterial(img) end
        vfx.gameObject:SetActive(false)
        vfx.gameObject:SetActive(true)
        index = index + 1
    end

    self.m_VfxTick = RegisterTickOnce(function ()
        showFx()
        if self.m_VfxTick then UnRegisterTick(self.m_VfxTick) end
        self.m_VfxTick = RegisterTick(function ()
            showFx()
        end, intervalTime)
    end, delayTime)
end

--@region UIEvent

function LuaGuoQing2023JingYuanResultWnd:OnAgainBtnClick()
    LuaGuoQing2023Mgr:OpenGuoQing2023MainWnd()
end

function LuaGuoQing2023JingYuanResultWnd:OnShareBtnClick()
	CUICommonDef.CaptureScreenUIAndShare(CLuaUIResources.GuoQing2023JingYuanResultWnd, self.ShareBtn)
end

--@endregion UIEvent
function LuaGuoQing2023JingYuanResultWnd:OnDisable()
    if self.m_VfxTick then UnRegisterTick(self.m_VfxTick) end
end
