local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType1 = import "CPlayerInfoMgr+AlignType"
local CIMMgr = import "L10.Game.CIMMgr"
local Profession = import "L10.Game.Profession"

LuaEnemyZongPaiArrestListWnd = class()
RegistClassMember(LuaEnemyZongPaiArrestListWnd,"m_CountLabel")
RegistClassMember(LuaEnemyZongPaiArrestListWnd,"m_TableViewDataSource")
RegistClassMember(LuaEnemyZongPaiArrestListWnd,"m_TableView")
RegistClassMember(LuaEnemyZongPaiArrestListWnd,"m_Infos")
RegistClassMember(LuaEnemyZongPaiArrestListWnd,"m_PlayerData")

function LuaEnemyZongPaiArrestListWnd:Init()
    self.m_TableView = self.transform:Find("TableView"):GetComponent(typeof(QnTableView))
    self.m_CountLabel = self.transform:Find("Header/Label"):GetComponent(typeof(UILabel))

    local function initItem(item,index)
        self:InitItem(item,index)
    end

    self.m_TableViewDataSource = DefaultTableViewDataSource.CreateByCount(0,initItem)
    self.m_TableView.m_DataSource = self.m_TableViewDataSource

    if LuaZongMenMgr.m_ArrestListWndEmenySectId~= nil and LuaZongMenMgr.m_ArrestListWndEmenySectId ~= 0 then
        Gac2Gas.QueryEnemySectCaptured(LuaZongMenMgr.m_ArrestListWndEmenySectId)
    else
        CUIManager.CloseUI(CLuaUIResources.EnemyZongPaiArrestListWnd)
    end
end

function LuaEnemyZongPaiArrestListWnd:InitItem(item,index)
    -- 获取节点
    local tf = item.transform
    local nameLabel = FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel))
    local statusLabel = FindChild(tf,"Status"):GetComponent(typeof(UILabel))
    local clsSprite = FindChild(tf,"ClsSprite"):GetComponent(typeof(UISprite))

    -- 数据
    local playerId = self.m_Infos[5*index]
    local name = self.m_Infos[5*index+1]
    local cls = self.m_Infos[5*index+2]
    local status = self.m_Infos[5*index+3]
    local catchTime = self.m_Infos[5*index+4]

    clsSprite.spriteName = Profession.GetIconByNumber(cls)
    nameLabel.text = name
    UIEventListener.Get(nameLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        local pos = Vector3(640, 0)
        pos = nameLabel.transform:TransformPoint(pos)
        self:ShowPlayerPopupMenu(playerId, pos)
    end)
    if status == 1 then
        statusLabel.text = LocalString.GetString("[FFFE91]关押中")
    else
        statusLabel.text = LocalString.GetString("[FF5050]炼化中")
    end
end

function LuaEnemyZongPaiArrestListWnd:ShowPlayerPopupMenu(id, pos)
    CPlayerInfoMgr.ShowPlayerPopupMenu(id, CIMMgr.Inst:IsMyFriend(id) and EnumPlayerInfoContext.FriendInFriendWnd or EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, pos, AlignType1.Default)
end

-- info:  peopleName peopleClass 职业 peopleStatus 状态 1 关押, 0 炼化 catchTime 被抓时间
function LuaEnemyZongPaiArrestListWnd:InitInfo(infos)
    self.m_Infos = infos

    self.m_CountLabel.text = SafeStringFormat3(LocalString.GetString("%d位被抓弟子"), infos.Count/4)
    self.m_TableViewDataSource.count = infos.Count/5
    self.m_TableView:ReloadData(true,false)
end

function LuaEnemyZongPaiArrestListWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncEnemySectCaptured", self, "InitInfo")
end

function LuaEnemyZongPaiArrestListWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncEnemySectCaptured", self, "InitInfo")
end
