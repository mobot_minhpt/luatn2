local QnButton = import "L10.UI.QnButton"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CUIFx = import "L10.UI.CUIFx"
local UICamera = import "UICamera"
local CButton = import "L10.UI.CButton"

LuaZhaiXingEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhaiXingEnterWnd, "m_LeftCircle", "LeftCircle", QnButton)
RegistChildComponent(LuaZhaiXingEnterWnd, "m_LeftDesc", "LeftDesc", GameObject)
RegistChildComponent(LuaZhaiXingEnterWnd, "m_RightCircle", "RightCircle", QnButton)
RegistChildComponent(LuaZhaiXingEnterWnd, "m_RightDesc", "RightDesc", GameObject)
RegistChildComponent(LuaZhaiXingEnterWnd, "m_Cost", "Cost", GameObject)
RegistChildComponent(LuaZhaiXingEnterWnd, "m_CostName", "CostName", UILabel)
RegistChildComponent(LuaZhaiXingEnterWnd, "m_TipBtn", "TipBtn", QnButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaZhaiXingEnterWnd, "m_NeedCount")
RegistClassMember(LuaZhaiXingEnterWnd, "m_TemplateId")
RegistClassMember(LuaZhaiXingEnterWnd, "m_OwnCount")
RegistClassMember(LuaZhaiXingEnterWnd, "m_ClickFx")
RegistClassMember(LuaZhaiXingEnterWnd, "m_Bg")
RegistClassMember(LuaZhaiXingEnterWnd, "m_RankBtn")

function LuaZhaiXingEnterWnd:Awake()
    self.m_NeedCount = 1
    self.m_TemplateId = ZhaiXing_Setting.GetData().EnterSceneConsumeItemId
    self.m_ClickFx = self.transform:Find("Middle/ClickFx"):GetComponent(typeof(CUIFx))
    self.m_Bg = self.transform:Find("Middle/Parallax/Background/Bg").gameObject
    self.m_RankBtn = self.transform:Find("BottomRight/BlueButton"):GetComponent(typeof(CButton))
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaZhaiXingEnterWnd:Init()
    self.m_TipBtn.OnClick = DelegateFactory.Action_QnButton(function ()
        g_MessageMgr:ShowMessage("ZhaiXing_Rules")
    end)

    UIEventListener.Get(self.m_RankBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankBtnClick()
	end)

    self:InitCost(self.m_Cost, self.m_TemplateId)
    self:RefreshCost(self.m_Cost, self.m_TemplateId)
    Gac2Gas.QueryZhaiXingOpenStatus()
end

--@region UIEvent

--@endregion UIEvent

function LuaZhaiXingEnterWnd:SendItem(args)
	self:RefreshCost(self.m_Cost, self.m_TemplateId)
end


function LuaZhaiXingEnterWnd:SetItemAt(args)
	self:RefreshCost(self.m_Cost, self.m_TemplateId)
end

function LuaZhaiXingEnterWnd:ReplyZhaiXing(status)
    local info1 = {active = status[1] ~= 0 , stage = status[1], name = ZhaiXing_Setting.GetData().ThemeNames[0]}
    local info2 = {active = status[2] ~= 0 , stage = status[2], name = ZhaiXing_Setting.GetData().ThemeNames[1]}
    self:InitCircle(self.m_LeftCircle, self.m_LeftDesc, info1)
    self:InitCircle(self.m_RightCircle, self.m_RightDesc, info2)
end

function LuaZhaiXingEnterWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "SendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "SetItemAt")
    g_ScriptEvent:AddListener("ReplyZhaiXing", self, "ReplyZhaiXing")
end

function LuaZhaiXingEnterWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "SendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "SetItemAt")
    g_ScriptEvent:RemoveListener("ReplyZhaiXing", self, "ReplyZhaiXing")
end


function LuaZhaiXingEnterWnd:InitCircle(circle, desc, info)
    local enterLab  = circle.transform:Find("Offset/EnterLab").gameObject
    local themeBg   = circle.transform:Find("Offset/ThemeBg").gameObject
    local nullLab   = circle.transform:Find("Offset/NullLab").gameObject
    local nullBg    = circle.transform:Find("Offset/NullBg").gameObject
    local fx        = circle.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    local nullLab2  = desc.transform:Find("NullLab").gameObject
    local nameLab   = desc.transform:Find("ThemeLab"):GetComponent(typeof(UILabel))

    enterLab.gameObject:SetActive(info.active)
    themeBg.gameObject:SetActive(info.active)
    nameLab.gameObject:SetActive(info.active)
    nullLab.gameObject:SetActive(not info.active)
    nullLab2.gameObject:SetActive(not info.active)
    nullBg.gameObject:SetActive(not info.active)

    nameLab.text = info.name
    
    circle.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self.m_ClickFx.transform.position  = UICamera.lastWorldPosition
        self.m_ClickFx:DestroyFx()
        self.m_ClickFx:LoadFx("fx/ui/prefab/UI_zhaixing_xuanze_dianji.prefab")
        Gac2Gas.RequestEnterZhaiXingPlay(info.stage)
        LuaZhaiXingMgr.m_stage = info.stage
    end)

    if info.active then
        fx:LoadFx("fx/ui/prefab/UI_zhaixing_xuanze_xuanzhong.prefab")
    end
end


function LuaZhaiXingEnterWnd:InitCost(item, itemTemplateId)
    local btn       = item:GetComponent(typeof(QnButton))
    local icon      = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local getLab    = item.transform:Find("GetLab"):GetComponent(typeof(UILabel))
    local itemData  = Item_Item.GetData(itemTemplateId)

    icon:LoadMaterial(itemData.Icon)
    btn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        if getLab.gameObject.activeSelf then
            CItemAccessListMgr.Inst:ShowItemAccessInfo(itemTemplateId, true, btn.transform, AlignType.Right)
        else
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemTemplateId, false, nil, AlignType2.Default, 0, 0, 0, 0)
        end
    end)

    self.m_CostName.text = itemData.Name
end

function LuaZhaiXingEnterWnd:RefreshCost(item, itemTemplateId)
    local getLab    = item.transform:Find("GetLab"):GetComponent(typeof(UILabel))
    local numLab    = item.transform:Find("NumLab"):GetComponent(typeof(UILabel))
    local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemTemplateId)
    self.m_OwnCount = bindItemCountInBag + notBindItemCountInBag

    if self.m_OwnCount >= self.m_NeedCount then
        getLab.gameObject:SetActive(false)
        numLab.text = SafeStringFormat3("%d/%d", self.m_OwnCount, self.m_NeedCount)
    else
        getLab.gameObject:SetActive(true)
        numLab.text = SafeStringFormat3("[c][ff0000]%d[-][/c]/%d", self.m_OwnCount, self.m_NeedCount)
    end
end

function LuaZhaiXingEnterWnd:OnRankBtnClick()
    CUIManager.ShowUI(CLuaUIResources.ZhaiXingRankWnd)
end
