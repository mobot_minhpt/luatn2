local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CWelfareMgr = import "L10.UI.CWelfareMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CTaskMgr=import "L10.Game.CTaskMgr"
local Animation = import "UnityEngine.Animation"
local Task_Task = import "L10.Game.Task_Task"
LuaHalloween2022MainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHalloween2022MainWnd, "ActBtn1", "ActBtn1", GameObject)
RegistChildComponent(LuaHalloween2022MainWnd, "ActBtn2", "ActBtn2", GameObject)
RegistChildComponent(LuaHalloween2022MainWnd, "ActBtn3", "ActBtn3", GameObject)
RegistChildComponent(LuaHalloween2022MainWnd, "ActBtn4", "ActBtn4", GameObject)
RegistChildComponent(LuaHalloween2022MainWnd, "SignBtn", "SignBtn", GameObject)
RegistChildComponent(LuaHalloween2022MainWnd, "TimeLb", "TimeLb", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaHalloween2022MainWnd, "m_TrafficLightAlert")
RegistClassMember(LuaHalloween2022MainWnd, "m_isPlayAni")
function LuaHalloween2022MainWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    UIEventListener.Get(self.ActBtn1.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnActBtn1Click()
        end
    )

    UIEventListener.Get(self.ActBtn2.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnActBtn2Click()
        end
    )

    UIEventListener.Get(self.ActBtn3.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnActBtn3Click()
        end
    )

    UIEventListener.Get(self.ActBtn4.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnActBtn4Click()
        end
    )

    UIEventListener.Get(self.SignBtn.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnSignBtnClick()
        end
    )
    self.m_isPlayAni = false
    --@endregion EventBind end
end


--@region 事件

function LuaHalloween2022MainWnd:OnUpdateTask(args)
    if tonumber(args[0]) ~= Halloween2022_Setting.GetData().QingQiuTaskId then
        return
    end
    self:InitActBtn1()
end

--@endregion

function LuaHalloween2022MainWnd:Init()
    local showclipName = "halloween2022mainwnd_show"
    local animation = self.gameObject:GetComponent(typeof(Animation))
    local aniEnter = animation[clipname]
    if animation and aniEnter then 
        aniEnter.time = 0
        animation:Play(clipname)
    end
	self.TimeLb.text = Halloween2022_Setting.GetData().OpenTimeString
    self.m_TrafficLightAlert = self.ActBtn3.transform:Find("Alert").gameObject
    self:InitSignBtn() --签到按钮

    self:InitActBtn1() --青丘邀约

    self:InitActBtn2() --红包封面

    self:InitActBtn3() --不动如山

    self:InitActBtn4() --篝火守护
end

function LuaHalloween2022MainWnd:InitActBtn2()
    local alert = FindChild(self.ActBtn2.transform,"Alert")
    alert.gameObject:SetActive(LuaHalloween2022Mgr.HaveAcceptableHongBaoCover())
    self.ActBtn2.transform:Find("StateLb"):GetComponent(typeof(UILabel)).text = LocalString.GetString("解锁幻灵夜红包封面")
end

function LuaHalloween2022MainWnd:InitActBtn3()
    local timeStr = Halloween2022_Setting.GetData().TrafficPlayTimeString
    self.m_TrafficLightAlert:SetActive(LuaActivityRedDotMgr:IsRedDot(11))
	local statelb = FindChildWithType(self.ActBtn3.transform,"StateLb",typeof(UILabel))
	statelb.text = timeStr
end

function LuaHalloween2022MainWnd:InitActBtn4()
	local statelb = FindChildWithType(self.ActBtn4.transform,"StateLb",typeof(UILabel))
	statelb.text = Halloween2022_Setting.GetData().CampfirePlayTimeString
end

--@region 青丘邀约

function LuaHalloween2022MainWnd:GetQingQiuTaskState()
    local taskid = Halloween2022_Setting.GetData().QingQiuTaskId
    if not CClientMainPlayer.Inst then
        return
    end
    local taskProp = CClientMainPlayer.Inst.TaskProp
    if taskProp:IsMainPlayerTaskFinished(taskid) then --已经完成
        return 3
    elseif CommonDefs.DictContains(taskProp.CurrentTasks, typeof(UInt32), taskid) then
        return 2
    else
        local startT = Halloween2022_Setting.GetData().TaskTimeList[2]
        local endT = Halloween2022_Setting.GetData().TaskTimeList[3]
        local startTime =  CServerTimeMgr.Inst:GetTimeStampByStr(startT)
        local endTime = CServerTimeMgr.Inst:GetTimeStampByStr(endT)
        local nowTime = CServerTimeMgr.Inst.timeStamp
        if nowTime < startTime or nowTime > endTime then
            return 4
        end
        return 1
    end
end

function LuaHalloween2022MainWnd:InitActBtn1()
    local state = self:GetQingQiuTaskState()
    local state1 = FindChild(self.ActBtn1.transform, "State1")
    local state2 = FindChild(self.ActBtn1.transform, "State2")
    local state3 = FindChild(self.ActBtn1.transform, "State3")
    state1.transform:Find("StateLb"):GetComponent(typeof(UILabel)).text = LocalString.GetString("待接取")
    state1.gameObject:SetActive(state == 1 or state == 4)
    state2.gameObject:SetActive(state == 2)
    state3.gameObject:SetActive(state == 3)
end

--@endregion

--@region 签到

function LuaHalloween2022MainWnd:InitSignBtn()
    local nosign = self:IsHolidayAlert()
    local trans = self.SignBtn.transform
    local nosignstyle = trans:Find("NoSignStyle")
    local signstyle = trans:Find("SignStyle")
    nosignstyle.gameObject:SetActive(nosign == 1 or nosign == 2)
    signstyle.gameObject:SetActive(nosign == 3)
end

function LuaHalloween2022MainWnd:IsHolidayAlert()
    local holiday = CSigninMgr.Inst.holidayInfo
    if holiday and holiday.showSignin and not holiday.hasSignGift and holiday.todayInfo and holiday.todayInfo.Open == 1 then    -- 开活动并且未签到
        return 1
    elseif not holiday then -- 没开活动的时候
        return 2
    else return 3   
    end
end

--@endregion

--@region UIEvent

function LuaHalloween2022MainWnd:OnActBtn1Click()
    local taskid = Halloween2022_Setting.GetData().QingQiuTaskId
    local template =  Task_Task.GetData(taskid)
    if not CClientMainPlayer.Inst then return end
    local minGrade = template.GradeCheck[0]
    if CClientMainPlayer.Inst.HasFeiSheng then
        minGrade = template.GradeCheckFS1[0]
    end
    if CClientMainPlayer.Inst.Level < minGrade then
        g_MessageMgr:ShowMessage("Task_Need_Level", minGrade)
        return
    end
    local state = self:GetQingQiuTaskState()
    if state == 3 then
        g_MessageMgr:ShowCustomMsg(LocalString.GetString("你已经完成该任务了"))
    elseif state == 2 then
        g_MessageMgr:ShowCustomMsg(LocalString.GetString("你已经接取过该任务了"))
    elseif state == 4 then
        g_MessageMgr:ShowMessage("HALLOWEEN2022_NOT_OPEN_IN_TRIAL_DELIVERY")
    else
        Gac2Gas.RequestAutoAcceptTask(taskid)
    end
end

function LuaHalloween2022MainWnd:OnActBtn2Click()
    --@TODO 2022-08-22 17:00:40 打开红包封面
    CUIManager.ShowUI(CLuaUIResources.HalloweenHongBaoCoverWnd)
end

function LuaHalloween2022MainWnd:OnActBtn3Click()
    self.m_TrafficLightAlert:SetActive(false)
    local timeStr = Halloween2022_Setting.GetData().TrafficPlayTimeString
    local isInTime = LuaHalloween2022Mgr.IsInTrafficLightPlayTime()
    CUIManager.ShowUI(CLuaUIResources.HwMuTouRenEnterWnd)
    if isInTime then
        LuaActivityRedDotMgr:OnRedDotClicked(11)
    end
end

function LuaHalloween2022MainWnd:OnActBtn4Click()
    if self.m_isPlayAni then return end
    LuaHalloween2022Mgr.m_OpenFireDeferenceWndFromMain = true
    CUIManager.ShowUI(CLuaUIResources.HwFireDefenceEnterWnd)
    --CUIManager.ShowUI(CLuaUIResources.HwFireDefenceEnterWnd)
end

function LuaHalloween2022MainWnd:OnSignBtnClick()
    local signName = Halloween2022_Setting.GetData().QiaoDaoTabName
    local status = self:IsHolidayAlert()
    if status == 2 then
        g_MessageMgr:ShowMessage("HALLOWEEN2022_NOT_OPEN_IN_TRIAL_DELIVERY")
    else
        CWelfareMgr.OpenWelfareWnd(signName)
    end
    
end

function LuaHalloween2022MainWnd:DoOpenWndAnim()
    if self.m_isPlayAni then return end
    local closeBtn = self.transform:Find("CloseButton").gameObject:SetActive(false)
    local clipname = "halloween2022mainwnd_gouhuoclose"
    local animation = self.gameObject:GetComponent(typeof(Animation))
    local aniEnter = animation[clipname]
    local UIPanel = self.gameObject:GetComponent(typeof(UIPanel))
    UIPanel.depth = UIPanel.depth + 100
    if animation and aniEnter then 
        self.m_isPlayAni = true
        aniEnter.time = 0
        animation:Play(clipname)
        local CloseWndTick = RegisterTickOnce(function()
            CUIManager.CloseUI(CLuaUIResources.Halloween2022MainWnd)
        end,500)
    end
end

function LuaHalloween2022MainWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateTask", self, "OnUpdateTask")
    g_ScriptEvent:AddListener("OnHolidaySigninInfoUpdate", self, "InitSignBtn")
    g_ScriptEvent:AddListener("Halloween2022SyncAllowToUseHongBaoCover",self,"InitActBtn2")
    g_ScriptEvent:AddListener("OnHwFireDefenceEnterWndOpen",self,"DoOpenWndAnim")
end

function LuaHalloween2022MainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnHwFireDefenceEnterWndOpen",self,"DoOpenWndAnim")
    g_ScriptEvent:RemoveListener("UpdateTask", self, "OnUpdateTask")
    g_ScriptEvent:RemoveListener("OnHolidaySigninInfoUpdate", self, "InitActBtn2")
    g_ScriptEvent:RemoveListener("Halloween2022SyncAllowToUseHongBaoCover",self,"UpdateProgress")
end
--@endregion UIEvent
