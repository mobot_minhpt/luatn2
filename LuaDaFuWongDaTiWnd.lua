local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
LuaDaFuWongDaTiWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongDaTiWnd, "ChoiceTemplate", "ChoiceTemplate", GameObject)
RegistChildComponent(LuaDaFuWongDaTiWnd, "RewardLabel", "RewardLabel", UILabel)
RegistChildComponent(LuaDaFuWongDaTiWnd, "ItemCell", "ItemCell", GameObject)
RegistChildComponent(LuaDaFuWongDaTiWnd, "Table", "Table", GameObject)
RegistChildComponent(LuaDaFuWongDaTiWnd, "Question", "Question", UILabel)
RegistChildComponent(LuaDaFuWongDaTiWnd, "Progress", "Progress", GameObject)
RegistChildComponent(LuaDaFuWongDaTiWnd, "LeftTimeLabel", "LeftTimeLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongDaTiWnd, "m_CloseBtn")
RegistClassMember(LuaDaFuWongDaTiWnd, "m_SelectId")
RegistClassMember(LuaDaFuWongDaTiWnd, "m_ChoiceList")
RegistClassMember(LuaDaFuWongDaTiWnd, "m_totalTime")
RegistClassMember(LuaDaFuWongDaTiWnd, "m_QuestionId")
RegistClassMember(LuaDaFuWongDaTiWnd, "m_AnswerIndex")
RegistClassMember(LuaDaFuWongDaTiWnd, "m_IsClick")
RegistClassMember(LuaDaFuWongDaTiWnd, "m_ShowAnswerTime")
RegistClassMember(LuaDaFuWongDaTiWnd, "m_ShowAlertTime")
RegistClassMember(LuaDaFuWongDaTiWnd, "m_ShowRightTick")
RegistClassMember(LuaDaFuWongDaTiWnd, "m_HasInit")
RegistClassMember(LuaDaFuWongDaTiWnd, "m_PlayerHead")
function LuaDaFuWongDaTiWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_SelectId = 0
    self.m_AnswerIndex = 0
    self.m_QuestionId = 0
    self.m_IsClick = false
    self.m_CloseBtn = self.transform:Find("Wnd_Bg_Secondary_2/CloseButton").gameObject
    self.m_PlayerHead = self.transform:Find("Anchor/Content/PlayerHead").gameObject
    self.m_totalTime = DaFuWeng_Countdown.GetData(EnumDaFuWengStage.RoundGridShuYuan).Time 
    self.m_ShowAnswerTime = DaFuWeng_Setting.GetData().DaTiShowAnswerTime
    self.m_ShowAlertTime = DaFuWeng_Setting.GetData().DaTiAlertTime
    UIEventListener.Get(self.m_CloseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseBtnClick(0)
	end)
    self.ChoiceTemplate.gameObject:SetActive(false)
    self.Table.gameObject:SetActive(true)
    self.m_ShowRightTick = nil
    self.m_HasInit = false
end

function LuaDaFuWongDaTiWnd:Init()
    if not LuaDaFuWongMgr.CurQuestionId then return end
    if not self.m_HasInit then
        self.m_QuestionId = LuaDaFuWongMgr.CurQuestionId
        
        local CountDown = self.m_totalTime - self.m_ShowAnswerTime - LuaDaFuWongMgr.GetCardTime
        local endFunc = function() self:ShowRightAnswer(0, 0, 0, 0) end
        local durationFunc = function(time) self:OnTimeUpdate(time) end
        LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.Question,CountDown,durationFunc,endFunc)
        self:InitTitle()
        self:InitQuestion()
        self.m_HasInit = true
        self:ShowPlayerHead()
    end
    
end

function LuaDaFuWongDaTiWnd:InitTitle()
    self.RewardLabel.text = g_MessageMgr:FormatMessage("DaFuWong_DaTi_RewardTitle")
    local itemId = DaFuWeng_Setting.GetData().DaTiAwardItem
    local itemData = Item_Item.GetData(itemId)
    if not itemData then return end
    self.ItemCell.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)
    CommonDefs.AddOnClickListener(
        self.ItemCell.gameObject,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end

function LuaDaFuWongDaTiWnd:InitQuestion()
    local questionData = DaFuWeng_Question.GetData(self.m_QuestionId)
    if not questionData then return end
    local answerHead = {"A.","B.","C.","D."}
    self.Question.text = questionData.Describe
    self.m_ChoiceList = {}
    self.m_ChoiceList[1] = {answer = questionData.Option1}
    self.m_ChoiceList[2] = {answer = questionData.Option2}
    self.m_ChoiceList[3] = {answer = questionData.Option3}
    self.m_ChoiceList[4] = {answer = questionData.Option4}
    self.m_AnswerIndex = questionData.Answer
    Extensions.RemoveAllChildren(self.Table.transform)
    for i = 1,4 do
        local view = {}
        local go = CUICommonDef.AddChild(self.Table.gameObject,self.ChoiceTemplate.gameObject)
        local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
        local clickbg = go.transform:Find("clickbg").gameObject
        local isRight = go.transform:Find("Sprite").gameObject
        clickbg.gameObject:SetActive(false)
        isRight.gameObject:SetActive(false)
        label.text = answerHead[i]..self.m_ChoiceList[i].answer
        view = {go = go, label = label,clickbg = clickbg,isRight = isRight}
        go.gameObject:SetActive(true)
        UIEventListener.Get(go.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            if not LuaDaFuWongMgr.IsMyRound and not self.m_IsClick then 
                local id = LuaDaFuWongMgr.CurRoundPlayer
                local name = LuaDaFuWongMgr.PlayerInfo[id].name
                g_MessageMgr:ShowMessage("DaFuWeng_OtherPlayerOperate",name)
                return
            end
            self:OnAnswerClick(i)
        end)
        self.m_ChoiceList[i].view = view
    end
    self.Table:GetComponent(typeof(UITable)):Reposition()
end

function LuaDaFuWongDaTiWnd:OnAnswerClick(index)
    if self.m_IsClick then return end
    self.m_IsClick = true
    self.m_SelectId = index
    local cur = self.m_ChoiceList[self.m_SelectId]
    if cur then
        cur.view.clickbg.gameObject:SetActive(false)
        cur.view.label.color = NGUIText.ParseColor24("803916", 0)
    end
    local Select = self.m_ChoiceList[index]
    if Select then
        Select.view.clickbg.gameObject:SetActive(true)
        Select.view.label.color = NGUIText.ParseColor24("ffffff", 0)
    end
    if LuaDaFuWongMgr.IsMyRound then 
        Gac2Gas.DaFuWengShuYuanAnswer(index)
    end
end

function LuaDaFuWongDaTiWnd:ShowRightAnswer(questionId, answer, result, rewardId)
    LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.Question)

    self:OnAnswerClick(answer)
    self.transform:Find("Anchor/Content/LeftTime").gameObject:SetActive(false)
    local right = self.m_ChoiceList[self.m_AnswerIndex]
    if right then 
        right.view.isRight.gameObject:SetActive(true)
    end
    if self.m_ShowRightTick then UnRegisterTick(self.m_ShowRightTick) self.m_ShowRightTick = nil end
    self.m_ShowRightTick = RegisterTickOnce(function()
        self:OnCloseBtnClick(0)
    end,self.m_ShowAnswerTime * 1000)
end

function LuaDaFuWongDaTiWnd:OnTimeUpdate(leftTime)
    self.Progress:GetComponent(typeof(UISlider)).value = leftTime/(self.m_totalTime - self.m_ShowAnswerTime)
    self.LeftTimeLabel.text = SafeStringFormat3(LocalString.GetString("倒计时%d秒"),leftTime)
    if leftTime <= self.m_ShowAlertTime then
        self.Progress.transform:Find("Foreground"):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor24("bf816d", 0)
        self.LeftTimeLabel.color = NGUIText.ParseColor24("bf3600", 0)
    else
        self.Progress.transform:Find("Foreground"):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor24("6D99BE", 0)
        self.LeftTimeLabel.color = NGUIText.ParseColor24("366695", 0)
    end
end

function LuaDaFuWongDaTiWnd:OnCloseBtnClick(CurStage)
    if CurStage == 0 then 
        g_ScriptEvent:BroadcastInLua("OnDaFuWengFinishCurStage",EnumDaFuWengStage.RoundGridShuYuan)
    end
    if CurStage ~= EnumDaFuWengStage.RoundGridShuYuan then
        CUIManager.CloseUI(CLuaUIResources.DaFuWongDaTiWnd)
    end
end

function LuaDaFuWongDaTiWnd:ShowPlayerHead()
    if LuaDaFuWongMgr.IsMyRound then
        self.m_PlayerHead.gameObject:SetActive(false)
    else
        self.m_PlayerHead.gameObject:SetActive(true)
        local playerData = LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[LuaDaFuWongMgr.CurPlayerRound]
        local Portrait = self.m_PlayerHead.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
        local bg = self.m_PlayerHead.transform:Find("BG"):GetComponent(typeof(CUITexture))
        local Name = self.m_PlayerHead.transform:Find("Name"):GetComponent(typeof(UILabel))
        if playerData then
            Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(playerData.class, playerData.gender, -1), false)
			Name.transform:Find("Texture"):GetComponent(typeof(CUITexture)):LoadMaterial(LuaDaFuWongMgr:GetNameBgPath(playerData.round))
            bg:LoadMaterial(LuaDaFuWongMgr:GetHeadBgPath(playerData.round))
			Name.text = playerData.name
        end
    end
end

--@region UIEvent

--@endregion UIEvent

function LuaDaFuWongDaTiWnd:OnEnable()
    g_ScriptEvent:AddListener("DaFuWengShuYuanAnswerResult",self,"ShowRightAnswer")
    g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"OnCloseBtnClick")
end

function LuaDaFuWongDaTiWnd:OnDisable()
    g_ScriptEvent:RemoveListener("DaFuWengShuYuanAnswerResult",self,"ShowRightAnswer")
    g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnCloseBtnClick")
    LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.Question)
    if self.m_ShowRightTick then UnRegisterTick(self.m_ShowRightTick) self.m_ShowRightTick = nil end
end