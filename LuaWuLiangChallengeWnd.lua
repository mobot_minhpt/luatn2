local UITable = import "UITable"

local UILabel = import "UILabel"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local EnumClass = import "L10.Game.EnumClass"
local LocalString = import "LocalString"
local Profession = import "L10.Game.Profession"

LuaWuLiangChallengeWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuLiangChallengeWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaWuLiangChallengeWnd, "TeamBtn", "TeamBtn", GameObject)
RegistChildComponent(LuaWuLiangChallengeWnd, "ChallengeBtn", "ChallengeBtn", GameObject)
RegistChildComponent(LuaWuLiangChallengeWnd, "CompanionInfo", "CompanionInfo", GameObject)
RegistChildComponent(LuaWuLiangChallengeWnd, "RewardInfo", "RewardInfo", GameObject)
RegistChildComponent(LuaWuLiangChallengeWnd, "ContentTable", "ContentTable", UITable)
RegistChildComponent(LuaWuLiangChallengeWnd, "Non", "Non", GameObject)

--@endregion RegistChildComponent end

function LuaWuLiangChallengeWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TeamBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTeamBtnClick()
	end)


	
	UIEventListener.Get(self.ChallengeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChallengeBtnClick()
	end)


    --@endregion EventBind end
end

function LuaWuLiangChallengeWnd:Init()
    self.m_EnterData = LuaWuLiangMgr.enterData
    self.m_PlayId = LuaWuLiangMgr.enterData.Gate.PlayId

    self:InitCurrentHuoBan()
    self:InitCurrentReward()

    self.ContentTable:Reposition()
end

-- 设置当前阵容
function LuaWuLiangChallengeWnd:InitCurrentHuoBan()
    local chuZhanList = self.m_EnterData.ChuZhan
    local huobanInfo = self.m_EnterData.HuoBan

    for i = 1, 2 do
        local item = self.CompanionInfo.transform:Find(tostring(i)).gameObject
        item:SetActive(false)
    end

    local hasChuZhan = false
    for i, id in pairs(chuZhanList) do
        hasChuZhan = true

        local item = self.CompanionInfo.transform:Find(tostring(i))
        item.gameObject:SetActive(true)

        local icon = item:Find("Icon"):GetComponent(typeof(CUITexture))
        local sp = item:Find("Icon/Sprite"):GetComponent(typeof(UISprite))
        local nameLabel = item:Find("NameLabel"):GetComponent(typeof(UILabel))
        local skillLabel = item:Find("SkillLabel"):GetComponent(typeof(UILabel))
        local attrLabel = item:Find("AttributeLabel"):GetComponent(typeof(UILabel))

        local info = huobanInfo[id]
        local monsterData = Monster_Monster.GetData(id)
        local huoBanData = XinBaiLianDong_WuLiangHuoBan.GetData(id)
        local levelData = XinBaiLianDong_WuLiangInherit.GetData(info.InheritLevel or 0)

        print(id, huoBanData)
        icon:LoadNPCPortrait(monsterData.HeadIcon, false)

        if huoBanData then
            sp.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), huoBanData.Class))
            nameLabel.text = huoBanData.Name
        end
        
        local skillCount = 0
        if info and info.SkillLevelTbl then
            for k, v in pairs(info.SkillLevelTbl) do
                skillCount = skillCount + v
            end
        end
        skillLabel.text = tostring(skillCount)
        attrLabel.text = tostring(math.floor(levelData.PropertyCoefficient*100+huoBanData.PropertyCoefficient*100 + 0.01).."%")

        UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            -- 显示伙伴信息
            self.showTipHuobanId = id
            self.showTipHuobanInfo = info
            -- 请求
	        Gac2Gas.WuLiangShenJing_QueryHuoBanProp(id, false)
        end)
    end

    self.Non:SetActive(not hasChuZhan)
end

-- 设置通关奖励
function LuaWuLiangChallengeWnd:InitCurrentReward()
    local data = XinBaiLianDong_WuLiangPlay.GetDataBySubKey("GameplayId", self.m_PlayId)

    local diffLabelList = {LocalString.GetString("苦境"), LocalString.GetString("集境"), LocalString.GetString("灭境")}

    local diff = data.Id%10
    local level = math.floor(data.Id/100)
    self.TitleLabel.text = SafeStringFormat3(LocalString.GetString("%s·第%s层"), diffLabelList[diff], Extensions.ConvertToChineseString(level))
	
	-- 设置奖励物品
	local rewardList = data.ClientRewardItemId
	for i=1, 3 do
		local item = self.RewardInfo.transform:Find(tostring(i))
		local icon = item:Find("IconTexture"):GetComponent(typeof(CUITexture))

		if i <= rewardList.Length then
			item.gameObject:SetActive(true)
			local itemId = rewardList[i-1]
			local itemData = Item_Item.GetData(itemId)

			icon:LoadMaterial(itemData.Icon)

			UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
				CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
			end)
		else
			item.gameObject:SetActive(false)
		end
	end
end

function LuaWuLiangChallengeWnd:OnShowTip(huobanId, data)
	if self.showTipHuobanId and huobanId == self.showTipHuobanId then
        self.showTipHuobanInfo.MonsterId = huobanId
        self.showTipHuobanInfo.Attr = data
		LuaWuLiangMgr:OpenHuoBanTipWnd(self.showTipHuobanInfo)
	end
end

function LuaWuLiangChallengeWnd:OnEnable()
    g_ScriptEvent:AddListener("WuLiangShenJing_SetChuZhan", self, "Init")
    g_ScriptEvent:AddListener("WuLiangShenJing_Refresh_Challenge", self, "Init")
    g_ScriptEvent:AddListener("WuLiangShenJing_QueryHuoBanPropResult", self, "OnShowTip")
end

function LuaWuLiangChallengeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("WuLiangShenJing_SetChuZhan", self, "Init")
    g_ScriptEvent:RemoveListener("WuLiangShenJing_Refresh_Challenge", self, "Init")
    g_ScriptEvent:RemoveListener("WuLiangShenJing_QueryHuoBanPropResult", self, "OnShowTip")
end

--@region UIEvent

function LuaWuLiangChallengeWnd:OnChallengeBtnClick()
    Gac2Gas.WuLiangShenJing_EnterPlay(self.m_PlayId)
end


function LuaWuLiangChallengeWnd:OnTeamBtnClick()
    CUIManager.ShowUI(CLuaUIResources.WuLiangCompanionInfoWnd)
end


--@endregion UIEvent

