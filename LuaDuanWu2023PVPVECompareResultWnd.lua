
LuaDuanWu2023PVPVECompareResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaDuanWu2023PVPVECompareResultWnd, "myComponents")
RegistClassMember(LuaDuanWu2023PVPVECompareResultWnd, "otherComponents")
RegistClassMember(LuaDuanWu2023PVPVECompareResultWnd, "tweenFloat")
RegistClassMember(LuaDuanWu2023PVPVECompareResultWnd, "tick")

function LuaDuanWu2023PVPVECompareResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.myComponents = self:GetComponents(self.transform:Find("Anchor/Left"))
    self.otherComponents = self:GetComponents(self.transform:Find("Anchor/Right"))
end

function LuaDuanWu2023PVPVECompareResultWnd:GetComponents(root)
    return {
        portrait = root:Find("Portrait"):GetComponent(typeof(CUITexture)),
        name = root:Find("Portrait/Name"):GetComponent(typeof(UILabel)),
        score = root:Find("Score"):GetComponent(typeof(UILabel)),
        change = root:Find("Change"):GetComponent(typeof(UILabel)),
        win = root:Find("Result/Win").gameObject,
        lose = root:Find("Result/Lose").gameObject
    }
end

function LuaDuanWu2023PVPVECompareResultWnd:Init()
    local info = LuaDuanWu2023Mgr.pvpveCompareResultInfo

    local mainPlayer = CClientMainPlayer.Inst
    self.myComponents.name.text = mainPlayer.Name
    self.myComponents.portrait:LoadPortrait(CUICommonDef.GetPortraitName(mainPlayer.Class, mainPlayer.Gender, -1), false)
    self.myComponents.score.text = info.myScore
    self.myComponents.win:SetActive(info.meIsWinner)
    self.myComponents.lose:SetActive(not info.meIsWinner)

    self.otherComponents.name.text = info.otherPlayerName
    self.otherComponents.portrait:LoadPortrait(CUICommonDef.GetPortraitName(info.otherPlayerClass, info.otherPlayerGender, -1), false)
    self.otherComponents.score.text = info.otherScore
    self.otherComponents.win:SetActive(not info.meIsWinner)
    self.otherComponents.lose:SetActive(info.meIsWinner)

    local winChangeText = SafeStringFormat3("[00FF00](+%d)[-]", info.changeScore)
    local loseChangeText = SafeStringFormat3("[FF0000](-%d)[-]", info.changeScore)
    self.myComponents.change.text = info.meIsWinner and winChangeText or loseChangeText
    self.otherComponents.change.text = info.meIsWinner and loseChangeText or winChangeText

    self:Clear()
    self.tweenFloat = LuaTweenUtils.TweenFloat(0, 1, 1, function (val)
        self:UpdateScore(val)
    end, function()
        self.tweenFloat = nil
        self:UpdateScore(1)
    end)

    self.tick = RegisterTickOnce(function()
        CUIManager.CloseUI(CLuaUIResources.DuanWu2023PVPVECompareResultWnd)
    end, 3000)
end

function LuaDuanWu2023PVPVECompareResultWnd:UpdateScore(val)
    local info = LuaDuanWu2023Mgr.pvpveCompareResultInfo
    local change = math.floor(info.changeScore * val)
    local myChange = info.meIsWinner and change or -change
    self.myComponents.score.text = SafeStringFormat3(LocalString.GetString("%d分"), info.myScore + myChange)
    local otherChange = info.meIsWinner and -change or change
    self.otherComponents.score.text = SafeStringFormat3(LocalString.GetString("%d分"), info.otherScore + otherChange)
end

function LuaDuanWu2023PVPVECompareResultWnd:Clear()
    if self.tweenFloat then
        LuaTweenUtils.Kill(self.tweenFloat, true)
        self.tweenFloat = nil
    end

    if self.tick then
        UnRegisterTick(self.tick)
        self.tick = nil
    end
end

function LuaDuanWu2023PVPVECompareResultWnd:OnDestroy()
    self:Clear()
    LuaDuanWu2023Mgr.pvpveCompareResultInfo = {}
end

--@region UIEvent

--@endregion UIEvent
