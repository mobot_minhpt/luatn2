local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local CUITexture = import "L10.UI.CUITexture"
local CUICommonDef = import "L10.UI.CUICommonDef"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local MessageWndManager = import "L10.UI.MessageWndManager"

CLuaShenbingRestoreWnd = class()
CLuaShenbingRestoreWnd.Path = "ui/shenbing/LuaShenbingRestoreWnd"

RegistClassMember(CLuaShenbingRestoreWnd, "m_HasExtra")
RegistClassMember(CLuaShenbingRestoreWnd, "m_JQZTemplateId")
RegistClassMember(CLuaShenbingRestoreWnd, "m_BJMTemplateId")
RegistClassMember(CLuaShenbingRestoreWnd, "m_JQZLabel")
RegistClassMember(CLuaShenbingRestoreWnd, "m_OriginalJQZCount")
function CLuaShenbingRestoreWnd:Init()
	if not CClientMainPlayer.Inst then
		CUIManager.CloseUI(CLuaUIResources.ShenBingRestoreWnd)
		return
	end
	self.m_HasExtra = false
	self.m_OriginalJQZCount = 0
	self.m_JQZTemplateId = ShenBing_Setting.GetData().JqzItemId
	local itemId = CClientMainPlayer.Inst.ItemProp:GetItemAt(CommonDefs.ConvertIntToEnum(typeof(EnumItemPlace), LuaShenbingMgr.RestoreEquipPlace), LuaShenbingMgr.RestoreEquipPos)
	if not itemId then
		CUIManager.CloseUI(CLuaUIResources.ShenBingRestoreWnd)
		return
	end
	local commonItem = CItemMgr.Inst:GetById(itemId)
	if not commonItem then
		CUIManager.CloseUI(CLuaUIResources.ShenBingRestoreWnd)
		return
	end

	local shenbingEquipTrans = self.transform:Find("Anchor/ShenBingEquip")
	shenbingEquipTrans:Find("Texture"):GetComponent(typeof(CUITexture)):LoadMaterial(commonItem.Icon)
	shenbingEquipTrans:Find("Name"):GetComponent(typeof(UILabel)).text = commonItem.ColoredName
	shenbingEquipTrans:Find("kuang"):GetComponent(typeof(UISprite)).spriteName = CUICommonDef.GetItemCellBorder(commonItem.Equip.QualityType)

	local data = EquipmentTemplate_Equip.GetData(commonItem.TemplateId)
	if data then
			local originalEquipTrans = self.transform:Find("Anchor/OriginalEquip")
			local texObj = originalEquipTrans:Find("Texture").gameObject
			texObj:GetComponent(typeof(CUITexture)):LoadMaterial(data.Icon)
			originalEquipTrans:Find("Name"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("[c][%s]%s[-][/c]", NGUIText.EncodeColor24(commonItem.Equip.DisplayColor), commonItem.Equip.Name)
			originalEquipTrans:Find("kuang"):GetComponent(typeof(UISprite)).spriteName = CUICommonDef.GetItemCellBorder(commonItem.Equip.QualityType)
			UIEventListener.Get(texObj).onClick = LuaUtils.VoidDelegate(function()
				--CItemInfoMgr.ShowLinkItemTemplateInfo(commonItem.TemplateId, false, nil, AlignType2.ScreenLeft, 0, 0, 0, 0)
			end)
	end

	local returnRootTrans = self.transform:Find("Anchor/ReturnItemRoot")
	local dict = MsgPackImpl.unpack(LuaShenbingMgr.RestoreItemInfoUD)
	local cnt = 0
	local transCnt = 6
	local returnRootTrans7 = self.transform:Find("Anchor/ReturnItemRoot7")
	local itemCnt = dict.Count
	returnRootTrans.gameObject:SetActive(itemCnt <= transCnt)
	returnRootTrans7.gameObject:SetActive(itemCnt > transCnt)
	if itemCnt > transCnt then
		transCnt = 9
		returnRootTrans = returnRootTrans7
	end
	CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function(k, v)
		cnt = cnt + 1
		local itemTrans = returnRootTrans:Find("Item"..cnt)
		local texObj = itemTrans:Find("Texture").gameObject
		texObj:GetComponent(typeof(CUITexture)):LoadMaterial(Item_Item.GetData(k).Icon)
		local label = itemTrans:Find("Label"):GetComponent(typeof(UILabel))
		label.text = v
		UIEventListener.Get(texObj).onClick = LuaUtils.VoidDelegate(function()
			CItemInfoMgr.ShowLinkItemTemplateInfo(k, false, nil, AlignType2.ScreenLeft, 0, 0, 0, 0)
		end)
		if tonumber(k) == self.m_JQZTemplateId then
			self.m_JQZLabel = label
			self.m_OriginalJQZCount = v
		end
	end))
	for i = cnt + 1, transCnt do
		returnRootTrans:Find("Item"..i).gameObject:SetActive(false)
	end

	UIEventListener.Get(self.transform:Find("Anchor/RestoreBtn").gameObject).onClick = LuaUtils.VoidDelegate(function()
		if self.m_HasExtra then
			local cnt = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_BJMTemplateId)
			if cnt < LuaShenbingMgr.RestoreConsumeBJMNum then
				g_MessageMgr:ShowMessage("Shenbing_Restore_Lack_Item")
				return
			end
		end

		local text = g_MessageMgr:FormatMessage("ShenBing_Restore_Confirm_Msg", commonItem.Equip.ColoredDisplayName)
		MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(function ( ... )
			Gac2Gas.RequestRestoreShenBing(LuaShenbingMgr.RestoreEquipPlace, LuaShenbingMgr.RestoreEquipPos, commonItem.Id, self.m_HasExtra)
			CUIManager.CloseUI(CLuaUIResources.ShenBingRestoreWnd)
		end), nil, nil, nil, false)
	end)

	self:InitExtra()
end

function CLuaShenbingRestoreWnd:InitExtra()
	self.m_BJMTemplateId = 21030061
	local extraRootTrans = self.transform:Find("Anchor/ExtraRoot")
	extraRootTrans.gameObject:SetActive(LuaShenbingMgr.RestoreConsumeBJMNum > 0)
	local texObj = extraRootTrans:Find("Item/Texture").gameObject
	texObj:GetComponent(typeof(CUITexture)):LoadMaterial(Item_Item.GetData(self.m_BJMTemplateId).Icon)
	UIEventListener.Get(texObj).onClick = LuaUtils.VoidDelegate(function()
		CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_BJMTemplateId, false, nil, AlignType2.ScreenLeft, 0, 0, 0, 0)
	end)

	local checkBox = extraRootTrans:Find("QnCheckbox"):GetComponent(typeof(QnCheckBox))
	checkBox.OnValueChanged = DelegateFactory.Action_bool(function (v)
		self.m_HasExtra = v
		local cnt = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_BJMTemplateId)
		self:UpdateJQZ(cnt)
	end)
	checkBox:SetSelected(true, false)
	self:UpdateItem()

	local gotObj = extraRootTrans:Find("Item/Got").gameObject
	UIEventListener.Get(gotObj).onClick = LuaUtils.VoidDelegate(function()
		CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_BJMTemplateId, false, gotObj.transform, AlignType.Right)
	end)
end

function CLuaShenbingRestoreWnd:UpdateJQZ(bjmCnt)
	local jqzCnt = self.m_OriginalJQZCount
	if self.m_HasExtra then
		jqzCnt = self.m_OriginalJQZCount + LuaShenbingMgr.RestoreReturnJQZNum
	end
	if self.m_JQZLabel then
		self.m_JQZLabel.text = jqzCnt
	end
end

function CLuaShenbingRestoreWnd:UpdateItem(argv)
	local extraRootTrans = self.transform:Find("Anchor/ExtraRoot")
	local itemCnt = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_BJMTemplateId)
	local gotObj = extraRootTrans:Find("Item/Got").gameObject
	local bEnough = itemCnt >= LuaShenbingMgr.RestoreConsumeBJMNum
	gotObj:SetActive(not bEnough)
	extraRootTrans:Find("Item/Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("[%s]%s[-]/%s", bEnough and "FFFFFF" or "FF0000",  itemCnt, LuaShenbingMgr.RestoreConsumeBJMNum)

	self:UpdateJQZ(itemCnt)
end

function CLuaShenbingRestoreWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "UpdateItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "UpdateItem")
end

function CLuaShenbingRestoreWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "UpdateItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "UpdateItem")
end
