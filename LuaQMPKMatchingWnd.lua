local MessageWndManager = import "L10.UI.MessageWndManager"


CLuaQMPKMatchingWnd = class()
RegistClassMember(CLuaQMPKMatchingWnd,"m_CancelBtn")
RegistClassMember(CLuaQMPKMatchingWnd,"m_QieCuoBtn")

function CLuaQMPKMatchingWnd:Init()
    self.m_CancelBtn = self.transform:Find("Anchor/Cancel").gameObject
    self.m_QieCuoBtn = self.transform:Find("Anchor/Bottom/Challenge").gameObject
    UIEventListener.Get(self.m_CancelBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnCancelBtnClicked(go) end)
    UIEventListener.Get(self.m_QieCuoBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnQieCuoBtnClicked(go) end)

    local tipButton=FindChild(self.transform,"TipButton").gameObject
    UIEventListener.Get(tipButton).onClick=DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("QMPK_ZiYouPiPei_Tip")
    end)
end


function CLuaQMPKMatchingWnd:OnCancelBtnClicked( go) 
    Gac2Gas.RequestCancelSingUpQmpkFreeMatch()--自由匹配
    CUIManager.CloseUI(CLuaUIResources.QMPKMatchingWnd)
end
function CLuaQMPKMatchingWnd:OnQieCuoBtnClicked( go) 
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("QMPK_Matching_To_Qiecuo"), DelegateFactory.Action(function () 
        CUIManager.ShowUI(CLuaUIResources.QMPKQieCuoWnd)
        self:OnCancelBtnClicked(nil)
    end), nil, nil, nil, false)
end

return CLuaQMPKMatchingWnd
