local UITabBar          = import "L10.UI.UITabBar"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CServerTimeMgr    = import "L10.Game.CServerTimeMgr"
local EnumChineseDigits = import "L10.Game.EnumChineseDigits"
local ClientAction      = import "L10.UI.ClientAction"

LuaCommonPassportTaskView = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaCommonPassportTaskView, "typeTabBar")
RegistClassMember(LuaCommonPassportTaskView, "dailyTaskRedDot")
RegistClassMember(LuaCommonPassportTaskView, "weekTaskRedDot")
RegistClassMember(LuaCommonPassportTaskView, "seasonTaskRedDot")

RegistClassMember(LuaCommonPassportTaskView, "weekTemplate")
RegistClassMember(LuaCommonPassportTaskView, "weekGrid")
RegistClassMember(LuaCommonPassportTaskView, "taskTableView")
RegistClassMember(LuaCommonPassportTaskView, "taskTableView_High")

RegistClassMember(LuaCommonPassportTaskView, "passId")
RegistClassMember(LuaCommonPassportTaskView, "openTaskData") -- 当前开放的任务数据
RegistClassMember(LuaCommonPassportTaskView, "showBeforeWeekTask") -- 是否会存在跨周的周任务
RegistClassMember(LuaCommonPassportTaskView, "firstWeekTimeStamp") -- 第一周周一零点的时间戳（用于计算任务在第几周）
RegistClassMember(LuaCommonPassportTaskView, "maxWeek")
RegistClassMember(LuaCommonPassportTaskView, "curWeek") -- 今天在第几周
RegistClassMember(LuaCommonPassportTaskView, "weekTaskTbl") -- 不同周的任务信息
RegistClassMember(LuaCommonPassportTaskView, "selectWeekId") -- 当前选中的是第几周
RegistClassMember(LuaCommonPassportTaskView, "selectWeekGo") -- 当前选中的周Tab的gameObject

RegistClassMember(LuaCommonPassportTaskView, "finalRate") -- 任务经验加成率
RegistClassMember(LuaCommonPassportTaskView, "type2HasAward") -- 一类任务中是否有可领奖的任务

function LuaCommonPassportTaskView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitComponents()
end

function LuaCommonPassportTaskView:InitComponents()
    self.typeTabBar = self.transform:Find("TypeTabs"):GetComponent(typeof(UITabBar))
    self.typeTabBar:ReloadTabButtons()
    self.dailyTaskRedDot = self.typeTabBar.transform:Find("DailyTaskTab/Alert").gameObject
    self.weekTaskRedDot = self.typeTabBar.transform:Find("WeekTaskTab/Alert").gameObject
    self.seasonTaskRedDot = self.typeTabBar.transform:Find("SeasonTaskTab/Alert").gameObject

    self.weekTemplate = self.transform:Find("Week/WeekTemplate").gameObject
    self.weekGrid = self.transform:Find("Week/ScrollView/Grid"):GetComponent(typeof(UIGrid))
    self.taskTableView = self.transform:Find("TaskTableView"):GetComponent(typeof(QnAdvanceGridView))
    self.taskTableView_High = self.transform:Find("TaskTableView_High"):GetComponent(typeof(QnAdvanceGridView))

    self.weekTemplate:SetActive(false)
end

function LuaCommonPassportTaskView:OnEnable()
    g_ScriptEvent:AddListener("UpdatePassDataWithId", self, "OnUpdatePassDataWithId")
end

function LuaCommonPassportTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("UpdatePassDataWithId", self, "OnUpdatePassDataWithId")
end

function LuaCommonPassportTaskView:OnUpdatePassDataWithId(id)
    if self.passId == id then
        self:UpdateData()

        self:UpdateRedDot()
        self:UpdateTabBar()
        self:UpdateWeekGrid()
        self:UpdateTableView()
    end
end

function LuaCommonPassportTaskView:Init()
    self:InitData()
    self:UpdateData()
    self:InitTableView()

    self:UpdateRedDot()
    self:UpdateTabBar()
    self:UpdateWeekGrid()
    self:UpdateTableView()
end

function LuaCommonPassportTaskView:InitData()
    self.passId = LuaCommonPassportMgr.passId
    self.showBeforeWeekTask = Pass_ClientSetting.GetData(self.passId).ShowBeforeWeekTask > 0
    self.maxWeek = 10 -- 最大只支持显示到第十周

    -- 计算第一周零点的时间戳
    local beginTimeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(Pass_Setting.GetData(self.passId).BeginTime)
    local beginDateTime = CServerTimeMgr.ConvertTimeStampToZone8Time(beginTimeStamp)
    local week = EnumToInt(beginDateTime.DayOfWeek)
    if week == 0 then week = 7 end
    local dayBeginTimeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(SafeStringFormat3("%d-%d-%d 00:00", beginDateTime.Year, beginDateTime.Month, beginDateTime.Day))
    self.firstWeekTimeStamp = dayBeginTimeStamp - (week - 1) * 86400
end

function LuaCommonPassportTaskView:UpdateData()
    self.openTaskData = LuaCommonPassportMgr:GetOpenTaskData(self.passId)

    local weekOpenTaskData = self.openTaskData[2]
    if self.showBeforeWeekTask and weekOpenTaskData then
        self.curWeek = self:GetWeek(CServerTimeMgr.Inst.timeStamp)

        local tbl = {}
        for _, data in pairs(weekOpenTaskData) do
            local week = self:GetWeek(data.beginTimeStamp)
            if not tbl[week] then
                tbl[week] = {}
            end
            table.insert(tbl[week], data)
        end

        if not tbl[self.curWeek] then
            tbl[self.curWeek] = {}
        end

        -- 获取未解锁的周
        Pass_Task.Foreach(function(id, data)
            if data.PassID == self.passId then
                local serverTimeMgr = CServerTimeMgr.Inst
                local beginTimeStamp = serverTimeMgr:GetTimeStampByStr(data.BeginTime)
                local nowTimeStamp = serverTimeMgr.timeStamp
                if beginTimeStamp > nowTimeStamp then
                    local week = self:GetWeek(beginTimeStamp)
                    if not tbl[week] then
                        tbl[week] = {}
                    end
                end
            end
        end)

        self.weekTaskTbl = tbl
    end

    self.finalRate = LuaCommonPassportMgr:GetAdditionInfo(self.passId).finalRate
end

-- 获取时间戳在第几周
function LuaCommonPassportTaskView:GetWeek(timeStamp)
    return math.ceil((timeStamp - self.firstWeekTimeStamp + 1) / (86400 * 7))
end

function LuaCommonPassportTaskView:InitTableView()
    self.taskTableView_High.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            local openTaskData = self.openTaskData[self.typeTabBar.SelectedIndex + 1]
            return openTaskData and #openTaskData or 0
        end,
        function(item, index)
            self:InitItem(item, index, false)
        end
    )

    if self.showBeforeWeekTask then
        self.taskTableView.m_DataSource = DefaultTableViewDataSource.Create(
            function()
                return #self.weekTaskTbl[self.selectWeekId]
            end,
            function(item, index)
                self:InitItem(item, index, true)
            end
        )
    end
end

function LuaCommonPassportTaskView:InitItem(item, index, isWeek)
    local data = isWeek and self.weekTaskTbl[self.selectWeekId][index + 1] or self.openTaskData[self.typeTabBar.SelectedIndex + 1][index + 1]
    local taskId = data.id
    local taskDesignData = Pass_Task.GetData(taskId)
    local target = taskDesignData.Target
    local progress = data.isFinished and target or data.progress

    local progressColor
    if data.isFinished then
        progressColor = "FFFFFF"
    else
        local color = Pass_ClientSetting.GetData(self.passId).TaskProgressColor
        progressColor = String.IsNullOrEmpty(color) and "FF5050" or color
    end
    item.transform:Find("Desc"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("%s ([%s]%d[-]/%d)", taskDesignData.Desc, progressColor, progress, target)
    item.transform:Find("Slider"):GetComponent(typeof(UISlider)).value = progress / target
    item.transform:Find("Exp/Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("+%d", taskDesignData.ProgressReward)
    local addition = item.transform:Find("Exp/Addition"):GetComponent(typeof(UILabel))
    addition.gameObject:SetActive(self.finalRate > 0)
    if self.finalRate > 0 then
        addition.text = SafeStringFormat3("x%.1f", self.finalRate + 1)
        addition:ResetAndUpdateAnchors()
    end

    local receiveButton = item.transform:Find("ReceiveButton").gameObject
    local gotoButton = item.transform:Find("GoToButton").gameObject
    local finishedGo = item.transform:Find("Finished").gameObject
    receiveButton:SetActive(data.isReward)
    finishedGo:SetActive(data.isFinished)
    gotoButton:SetActive(not data.isReward and not data.isFinished)

    if data.isReward then
        UIEventListener.Get(receiveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnReceiveButtonClick(taskId)
        end)
    elseif not data.isFinished then
        UIEventListener.Get(gotoButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnGoToButtonClick(taskId)
        end)
    end

    item.transform:GetComponent(typeof(UIWidget)).alpha = data.isFinished and 0.5 or 1
end

-- 更新TabBar显示
function LuaCommonPassportTaskView:UpdateTabBar()
    self.typeTabBar:GetTabGoByIndex(0):SetActive(self.openTaskData[1] ~= nil)
    self.typeTabBar:GetTabGoByIndex(1):SetActive(self.openTaskData[2] ~= nil)
    self.typeTabBar:GetTabGoByIndex(2):SetActive(self.openTaskData[3] ~= nil)

    -- 默认选中 优先选中有奖励的Tab
    local selectId = self.typeTabBar.SelectedIndex
    if not self.openTaskData[selectId + 1] then selectId = -1 end
    if selectId < 0 then
        for i = 1, 3 do
            if self.type2HasAward[i] then
                selectId = i - 1
                break
            end
        end

        if selectId < 0 then
            for i = 1, 3 do
                if self.openTaskData[i] then
                    selectId = i - 1
                    break
                end
            end
        end
    end

    if selectId < 0 or not self.openTaskData[selectId + 1] then
        for i = 1, 3 do
            if self.openTaskData[i] then
                selectId = i - 1
                break
            end
        end
    end

    self.typeTabBar.transform:GetComponent(typeof(UIGrid)):Reposition()
    self.typeTabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTypeTabChange()
    end)
    if selectId >= 0 then
        self.typeTabBar:ChangeTab(selectId, true)
    end
    self:UpdateLeftTime()
end

-- 更新显示的剩余时间
function LuaCommonPassportTaskView:UpdateLeftTime()
    local rootTbl = {
        self.typeTabBar.transform:Find("DailyTaskTab"),
        self.typeTabBar.transform:Find("WeekTaskTab"),
        self.typeTabBar.transform:Find("SeasonTaskTab"),
    }

    local turnRedSecondsTbl = {3600 * 2, 86400, 86400 * 2}

    for i = 1, 3 do
        local leftTime_Green = rootTbl[i]:Find("LeftTime_Green"):GetComponent(typeof(UILabel))
        local leftTime_Red = rootTbl[i]:Find("LeftTime_Red"):GetComponent(typeof(UILabel))

        if i == 2 and self.showBeforeWeekTask then
            leftTime_Green.gameObject:SetActive(false)
            leftTime_Red.gameObject:SetActive(false)
        else
            local leftSeconds = self:GetLeftSeconds(i)
            if leftSeconds then
                local showRed = leftSeconds < turnRedSecondsTbl[i]
                leftTime_Green.gameObject:SetActive(not showRed)
                leftTime_Red.gameObject:SetActive(showRed)
                if showRed then
                    leftTime_Red.text = LuaCommonPassportMgr:GetLeftTimeText(leftSeconds)
                else
                    leftTime_Green.text = LuaCommonPassportMgr:GetLeftTimeText(leftSeconds)
                end
            else
                leftTime_Green.gameObject:SetActive(false)
                leftTime_Red.gameObject:SetActive(false)
            end
        end
    end
end

function LuaCommonPassportTaskView:GetLeftSeconds(i)
    local openTaskData = self.openTaskData[i]
    if not openTaskData then return end

    local latestTimeStamp = 0
    for _, data in pairs(openTaskData) do
        if data.endTimeStamp > latestTimeStamp then
            latestTimeStamp = data.endTimeStamp
        end
    end

    local leftSeconds = latestTimeStamp - CServerTimeMgr.Inst.timeStamp
    return leftSeconds
end

-- 更新红点
function LuaCommonPassportTaskView:UpdateRedDot()
    self.type2HasAward = {}
    local autoProgress = LuaCommonPassportMgr:GetAutoProgress(self.passId)
    for i = 1, 3 do
        self.type2HasAward[i] = not autoProgress and self:IsTypeTabHasRedDot(i)
    end

    self.dailyTaskRedDot:SetActive(self.type2HasAward[1])
    self.weekTaskRedDot:SetActive(self.type2HasAward[2])
    self.seasonTaskRedDot:SetActive(self.type2HasAward[3])
end

-- Tab上是否有红点
function LuaCommonPassportTaskView:IsTypeTabHasRedDot(i)
    local openTaskData = self.openTaskData[i]
    if not openTaskData then return false end

    local passInfo = LuaCommonPassportMgr:GetPassInfo(self.passId)
    if not passInfo then return false end

    for _, data in pairs(openTaskData) do
        local isTaskReward = LuaCommonPassportMgr:IsTaskReward(passInfo, data.id)
        if isTaskReward then return true end
    end
    return false
end

-- 更新周列表
function LuaCommonPassportTaskView:UpdateWeekGrid()
    if not self.showBeforeWeekTask or not self.openTaskData[2] then
        return
    end

    if not self.selectWeekId or (self.selectWeekId ~= self.curWeek and not self.weekTaskTbl[self.selectWeekId]) then
        self.selectWeekId = self.curWeek
    end

    local autoProgress = LuaCommonPassportMgr:GetAutoProgress(self.passId)
    Extensions.RemoveAllChildren(self.weekGrid.transform)
    for i = 1, self.maxWeek do
        if self.weekTaskTbl[i] then
            local isSelected = i == self.selectWeekId
            local isLocked = i > self.curWeek

            local child = NGUITools.AddChild(self.weekGrid.gameObject, self.weekTemplate)
            child:SetActive(true)
            local normal = child.transform:Find("Normal").gameObject
            local highlight = child.transform:Find("Highlight").gameObject
            normal:SetActive(not isSelected)
            highlight:SetActive(isSelected)
            if not isLocked then
                UIEventListener.Get(normal.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                    self:OnWeekClick(child, i)
                end)
            end

            local normalLabel = child.transform:Find("Label"):GetComponent(typeof(UILabel))
            local lock = child.transform:Find("Lock")
            local lockLabel = lock:Find("Label"):GetComponent(typeof(UILabel))
            normalLabel.gameObject:SetActive(not isLocked)
            lock.gameObject:SetActive(isLocked)
            local str = SafeStringFormat3(LocalString.GetString("第%s周"), EnumChineseDigits.GetDigit(i))
            normalLabel.text = str
            lockLabel.text = str
            child.transform:Find("Alert").gameObject:SetActive(not autoProgress and self:IsWeekHasRedDot(i))

            if isSelected then
                self.selectWeekGo = child
            end
        end
    end
    self.weekGrid:Reposition()
end

-- Week上是否有红点
function LuaCommonPassportTaskView:IsWeekHasRedDot(week)
    local openTaskData = self.weekTaskTbl[week]
    if #openTaskData == 0 then return false end

    local passInfo = LuaCommonPassportMgr:GetPassInfo(self.passId)
    if not passInfo then return false end

    for _, data in pairs(openTaskData) do
        local isTaskReward = LuaCommonPassportMgr:IsTaskReward(passInfo, data.id)
        if isTaskReward then return true end
    end
    return false
end

function LuaCommonPassportTaskView:UpdateTableView()
    local isWeek = self.showBeforeWeekTask and self.typeTabBar.SelectedIndex == 1
    self.weekGrid.gameObject:SetActive(isWeek)
    self.taskTableView.gameObject:SetActive(isWeek)
    self.taskTableView_High.gameObject:SetActive(not isWeek)
    if isWeek then
        self.taskTableView:ReloadData(true, false)
    else
        self.taskTableView_High:ReloadData(true, false)
    end
end

--@region UIEvent

function LuaCommonPassportTaskView:OnTypeTabChange()
    self:UpdateTableView()
end

function LuaCommonPassportTaskView:OnWeekClick(go, weekId)
    if self.selectWeekGo then
        self.selectWeekGo.transform:Find("Normal").gameObject:SetActive(true)
        self.selectWeekGo.transform:Find("Highlight").gameObject:SetActive(false)
    end

    go.transform:Find("Normal").gameObject:SetActive(false)
    go.transform:Find("Highlight").gameObject:SetActive(true)
    self.selectWeekId = weekId
    self.selectWeekGo = go
    self:UpdateTableView()
end

function LuaCommonPassportTaskView:OnReceiveButtonClick(taskId)
    Gac2Gas.RequestSubmitPassTask(self.passId, taskId)
end

function LuaCommonPassportTaskView:OnGoToButtonClick(taskId)
    local action = Pass_Task.GetData(taskId).Action
    if not System.String.IsNullOrEmpty(action) then
        ClientAction.DoAction(action)
    end
end

--@endregion UIEvent
