local UISprite = import "UISprite"
local UIEventListener = import "UIEventListener"
local NGUITools = import "NGUITools"
local LocalString = import "LocalString"
local Extensions = import "Extensions"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CUIResources = import "L10.UI.CUIResources"
local CUIManager = import "L10.UI.CUIManager"
local CTalismanPositionTemplate = import "L10.UI.CTalismanPositionTemplate"
local CTalismanMenuMgr = import "L10.UI.CTalismanMenuMgr"
-- local CQMPKTalismanEquipView = import "L10.UI.CQMPKTalismanEquipView"
local CPlayerProSaveMgr = import "L10.Game.CPlayerProSaveMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CUIAnimateNumber=import "L10.UI.CUIAnimateNumber"
local UIRotTable=import "UIRotTable"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local CPlayerTalismanItemCell=import "L10.UI.CPlayerTalismanItemCell"

CLuaQMPKTalismanEquipView = class()
RegistClassMember(CLuaQMPKTalismanEquipView,"equipScoreLabel")
RegistClassMember(CLuaQMPKTalismanEquipView,"animateNumber")
RegistClassMember(CLuaQMPKTalismanEquipView,"table")
RegistClassMember(CLuaQMPKTalismanEquipView,"talismanTemplate")
RegistClassMember(CLuaQMPKTalismanEquipView,"menuButton")
RegistClassMember(CLuaQMPKTalismanEquipView,"menuArrow")
RegistClassMember(CLuaQMPKTalismanEquipView,"menuLabel")
RegistClassMember(CLuaQMPKTalismanEquipView,"pointTable")
RegistClassMember(CLuaQMPKTalismanEquipView,"pointTemplate")
RegistClassMember(CLuaQMPKTalismanEquipView,"highlightSpriteName")
RegistClassMember(CLuaQMPKTalismanEquipView,"normalSpriteName")
RegistClassMember(CLuaQMPKTalismanEquipView,"normalPointName")
RegistClassMember(CLuaQMPKTalismanEquipView,"highlightPointName")
RegistClassMember(CLuaQMPKTalismanEquipView,"lastEquipScore")
RegistClassMember(CLuaQMPKTalismanEquipView,"positionList")
RegistClassMember(CLuaQMPKTalismanEquipView,"SelectedItemId")
RegistClassMember(CLuaQMPKTalismanEquipView,"SelectPos")
RegistClassMember(CLuaQMPKTalismanEquipView,"curSelectMenuIndex")
RegistClassMember(CLuaQMPKTalismanEquipView,"m_IsOtherPlayer")
RegistClassMember(CLuaQMPKTalismanEquipView,"m_OtherPlayerInfo")
RegistClassMember(CLuaQMPKTalismanEquipView,"equipScoreGo")
RegistClassMember(CLuaQMPKTalismanEquipView,"otherTalismanTemplate")

function CLuaQMPKTalismanEquipView:Awake()
    self.m_IsOtherPlayer = CLuaQMPKMgr.s_IsOtherPlayer
    self.m_OtherPlayerInfo = CLuaQMPKMgr.s_PlayerInfo

    self.equipScoreGo = self.transform:Find("EquipmentScore").gameObject
    self.equipScoreLabel = self.transform:Find("EquipmentScore/Label"):GetComponent(typeof(UILabel))

    self.animateNumber = self.equipScoreLabel.transform:GetComponent(typeof(CUIAnimateNumber))
    self.table = self.transform:Find("TalismanPlate/RotTable"):GetComponent(typeof(UIRotTable))
    self.talismanTemplate = self.transform:Find("TalismanPlate/TalismanTemplate").gameObject
    self.otherTalismanTemplate = self.transform:Find("TalismanPlate/TalismanCell").gameObject
    self.menuButton = self.transform:Find("SwitchTitle/Title/CurWarehouseBtn"):GetComponent(typeof(UISprite))
    self.menuArrow = self.transform:Find("SwitchTitle/Title/CurWarehouseBtn/Arrow")
    self.menuLabel = self.transform:Find("SwitchTitle/Title/CurWarehouseBtn/Label"):GetComponent(typeof(UILabel))
    self.pointTable = self.transform:Find("SwitchTitle/PageIndicatorTable"):GetComponent(typeof(UITable))
    self.pointTemplate = self.transform:Find("SwitchTitle/IndicatorTemplate").gameObject
--self.highlightSpriteName = ?
--self.normalSpriteName = ?
--self.normalPointName = ?
--self.highlightPointName = ?
self.lastEquipScore = 0
--self.positionList = ?
--self.SelectedItemId = ?
self.SelectPos = 0
self.curSelectMenuIndex = 0

end

-- Auto Generated!!
function CLuaQMPKTalismanEquipView:Init( index) 
    self.highlightSpriteName = "common_btn_02_highlight"
    self.normalSpriteName = "common_btn_02_highlight"
    self.normalPointName = "common_page_icon_normal"
    self.highlightPointName = "common_page_icon_highlight"

    self.positionList ={}
    self.curSelectMenuIndex = index
    self:InitMenu()
    self.equipScoreLabel.text = nil
    if CClientMainPlayer.Inst ~= nil then
        self.lastEquipScore = CClientMainPlayer.Inst.EquipScore
        self.animateNumber:Init(CClientMainPlayer.Inst.EquipScore)
    end
    Extensions.RemoveAllChildren(self.table.transform)
    self.talismanTemplate:SetActive(false)
    self.otherTalismanTemplate:SetActive(false)

    if self.m_IsOtherPlayer then
        self:InitOtherTalisman()
    else
        self:InitSelfTalisman()
    end

    self.table:Reposition()

    if self.m_IsOtherPlayer then
        self:InitOtherPlayerConfig()
    end
end
function CLuaQMPKTalismanEquipView:InitSelfTalisman( )
    self:InitPoint()
    CPlayerProSaveMgr.Inst:savePro()
    local startIdx = EnumBodyPosition_lua.TalismanQian
    local endIdx = EnumBodyPosition_lua.TalismanDui
    if self.curSelectMenuIndex == 1 then
        startIdx = startIdx + 8
        endIdx = endIdx + 8
    end
    do
        local i = startIdx
        while i <= endIdx do
            local instance = NGUITools.AddChild(self.table.gameObject, self.talismanTemplate)
            instance:SetActive(true)
            local template =instance:GetComponent(typeof(CTalismanPositionTemplate))
            template:Init(i)
            table.insert( self.positionList,template )
            UIEventListener.Get(instance).onClick = DelegateFactory.VoidDelegate(function(go)
                self:OnPositionClicked(go)
            end)
            i = i + 1
        end
    end
    
end
function CLuaQMPKTalismanEquipView:InitOtherTalisman()
    local startIdx = EnumBodyPosition_lua.TalismanQian
    local endIdx = EnumBodyPosition_lua.TalismanDui
    if self.curSelectMenuIndex == 1 then
        startIdx = startIdx + 8
        endIdx = endIdx + 8
    end
    do
        local i = startIdx
        while i <= endIdx do
            local instance = NGUITools.AddChild(self.table.gameObject, self.otherTalismanTemplate)
            instance:SetActive(true)
            local template = instance:GetComponent(typeof(CPlayerTalismanItemCell))
            template:Init(i, self.m_OtherPlayerInfo.Pos2Equip[i])
            table.insert(self.positionList,template)
            UIEventListener.Get(instance).onClick = DelegateFactory.VoidDelegate(function(go)
                self:OnOtherPlayerPositionClicked(go)
            end)
            i = i + 1
        end
    end
end
function CLuaQMPKTalismanEquipView:InitMenu( )
    self.menuLabel.text = System.String.Format(LocalString.GetString("法宝栏{0}"), self.curSelectMenuIndex + 1)
    self.menuButton.spriteName = self.normalSpriteName
    Extensions.SetLocalRotationZ(self.menuArrow, 0)

    UIEventListener.Get(self.menuButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnMenuButtonClick(go)
    end)
end
function CLuaQMPKTalismanEquipView:InitPoint( )
    Extensions.RemoveAllChildren(self.pointTable.transform)
    self.pointTemplate:SetActive(false)
    if not self.m_IsOtherPlayer and CTalismanMenuMgr.talismanMenu2Open then
        for i = 0, 1 do
            local instance = NGUITools.AddChild(self.pointTable.gameObject, self.pointTemplate)
            instance:SetActive(true)
            local sprite = instance:GetComponent(typeof(UISprite))
            if sprite ~= nil then
                sprite.spriteName = self.normalPointName
                if i == 0 and not CClientMainPlayer.Inst.ItemProp.IsEnableSecondaryTalisman then
                    sprite.spriteName = self.highlightPointName
                elseif i == 1 and CClientMainPlayer.Inst.ItemProp.IsEnableSecondaryTalisman then
                    sprite.spriteName = self.highlightPointName
                end
            end
        end
        self.pointTable:Reposition()
    end
end
function CLuaQMPKTalismanEquipView:OnEnable( )
    if not self.m_IsOtherPlayer then
        g_ScriptEvent:AddListener("MainPlayerTotalEquipScoreUpdate", self, "UpdateEquipScore")
        g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
        g_ScriptEvent:AddListener("OnTalismanMenuSelect", self, "OnTalismanMenuSelect")
        g_ScriptEvent:AddListener("OnTalismanMenuClose", self, "OnTalismanMenuClose")
    end
    g_ScriptEvent:AddListener("OnOtherTalismanMenuSelect", self, "OnOtherTalismanMenuSelect")
end
function CLuaQMPKTalismanEquipView:OnDisable( )
    if not self.m_IsOtherPlayer then
        g_ScriptEvent:RemoveListener("MainPlayerTotalEquipScoreUpdate", self, "UpdateEquipScore")
        g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
        g_ScriptEvent:RemoveListener("OnTalismanMenuSelect", self, "OnTalismanMenuSelect")
        g_ScriptEvent:RemoveListener("OnTalismanMenuClose", self, "OnTalismanMenuClose")
    end
    g_ScriptEvent:RemoveListener("OnOtherTalismanMenuSelect", self, "OnOtherTalismanMenuSelect")
    CUIManager.CloseUI(CUIResources.TalismanMenu)
end

function CLuaQMPKTalismanEquipView:OnTalismanMenuSelect(args)
    self:Init(args[0])
    self:OnPositionClicked(self.positionList[1].gameObject)
end
function CLuaQMPKTalismanEquipView:OnTalismanMenuClose(args)
    self:InitMenu()
end

function CLuaQMPKTalismanEquipView:OnOtherTalismanMenuSelect(index)
    self:Init(index)
    self:OnOtherPlayerPositionClicked(self.positionList[1].gameObject)
end

function CLuaQMPKTalismanEquipView:OnMenuButtonClick( go) 
    local postion = self.menuButton.transform.position
    local yOffset = self.menuButton.height * 0.5 * self.menuButton.transform.lossyScale.y
    CLuaTalismanMenu.m_MenuPosition = {x = postion.x, y = postion.y - yOffset, z = 0}
    CTalismanMenuMgr.OpenTalismanMenu(self.curSelectMenuIndex)
    self.menuButton.spriteName = self.highlightSpriteName
    Extensions.SetLocalRotationZ(self.menuArrow, 180)
end
function CLuaQMPKTalismanEquipView:OnSetItemAt( args) 
    local place =args[0]
    local position =args[1]
    local oldItemId=args[2]
    local  newItemId=args[3]
    if CClientMainPlayer.Inst == nil or position == 0 or place ~= EnumItemPlace.Body then
        return
    end
    self:UpdateTalisman()
end
function CLuaQMPKTalismanEquipView:UpdateTalisman( )
    for i,v in ipairs(self.positionList) do
        v:Init(self.positionList[i].position)
    end
end
function CLuaQMPKTalismanEquipView:OnPositionClicked( go) 
    local itemProp = CClientMainPlayer.Inst.ItemProp
    for i,v in ipairs(self.positionList) do
        if go==v.gameObject then
            local itemId = itemProp:GetItemAt(EnumItemPlace.Body, v.position)
            if itemId then
                local item = CItemMgr.Inst:GetById(itemId)
                --原有的回调方法改成消息方式
                if item ~= nil then
                    g_ScriptEvent:BroadcastInLua("QMPKTalismanClick",itemId, item.TemplateId)
                end
            else
                g_ScriptEvent:BroadcastInLua("QMPKTalismanClick",nil, 0)
            end
        end
    end
end
function CLuaQMPKTalismanEquipView:OnOtherPlayerPositionClicked( go) 
    for i,v in ipairs(self.positionList) do
        if go==v.gameObject then
            local item = self.m_OtherPlayerInfo.Pos2Equip[v.position]
            if item then
                g_ScriptEvent:BroadcastInLua("QMPKTalismanClick", item.Id, item.TemplateId)
            else
                g_ScriptEvent:BroadcastInLua("QMPKTalismanClick",nil, 0)
            end
        end
    end
end
function CLuaQMPKTalismanEquipView:UpdateEquipScore( )
    if CClientMainPlayer.Inst ~= nil then
        local newEquipScore = CClientMainPlayer.Inst.EquipScore
        if self.lastEquipScore ~= newEquipScore then
            self.animateNumber:UpdateValue(self.lastEquipScore, newEquipScore, 1)
            self.lastEquipScore = newEquipScore
        end
    end
    CPlayerProSaveMgr.Inst:updatePro()
    CPlayerProSaveMgr.Inst:savePro()
end

function CLuaQMPKTalismanEquipView:InitOtherPlayerConfig()
    self.equipScoreGo:SetActive(false)
end

return CLuaQMPKTalismanEquipView
