local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"

LuaUnlockCustomZswTemplateWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaUnlockCustomZswTemplateWnd, "CancelBtn", "CancelBtn", GameObject)
RegistChildComponent(LuaUnlockCustomZswTemplateWnd, "UnlockBtn", "UnlockBtn", GameObject)
RegistChildComponent(LuaUnlockCustomZswTemplateWnd, "NeedScoreLabel", "NeedScoreLabel", UILabel)
RegistChildComponent(LuaUnlockCustomZswTemplateWnd, "OwnScoreLabel", "OwnScoreLabel", UILabel)
RegistChildComponent(LuaUnlockCustomZswTemplateWnd, "MsgLabel", "MsgLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaUnlockCustomZswTemplateWnd,"m_TemplateUnlockCount")
RegistClassMember(LuaUnlockCustomZswTemplateWnd,"m_FreeTemplateCount")

function LuaUnlockCustomZswTemplateWnd:Awake()
	self.m_FreeTemplateCount = 0
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.CancelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelBtnClick()
	end)


	
	UIEventListener.Get(self.UnlockBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUnlockBtnClick()
	end)


    --@endregion EventBind end
end

function LuaUnlockCustomZswTemplateWnd:Init()
	self.MsgLabel.text = g_MessageMgr:FormatMessage("Unlock_CustomZswTemplate_Wnd_Tip")

	self.m_TemplateUnlockCount = 0
	local CustomTemplateSlotUnlockCost = Zhuangshiwu_Setting.GetData().CustomTemplateSlotUnlockCost
	for i=0,CustomTemplateSlotUnlockCost.Length-1,1 do
		if CustomTemplateSlotUnlockCost[i] == 0 then
			self.m_TemplateUnlockCount = self.m_TemplateUnlockCount + 1
		end
	end
	self.m_FreeTemplateCount = self.m_TemplateUnlockCount

	if CClientHouseMgr.Inst and CClientHouseMgr.Inst.mCurFurnitureProp2 and CClientHouseMgr.Inst.mCurFurnitureProp2.CustomFurnitureTemplateInfo then
		local infoMap = CClientHouseMgr.Inst.mCurFurnitureProp2.CustomFurnitureTemplateInfo
		local playerId = CClientMainPlayer.Inst.Id
		if infoMap and CommonDefs.DictContains_LuaCall(infoMap, playerId) then
			local customInfo = CommonDefs.DictGetValue_LuaCall(infoMap, playerId)
			self.m_TemplateUnlockCount = customInfo.Num
		end
	end

	local setting = Zhuangshiwu_Setting.GetData()
	local customTemplateSlotUnlockCost = setting.CustomTemplateSlotUnlockCost
	local needScore = 0

	if self.m_TemplateUnlockCount < setting.MaxCustomTemplateSlotNum then
		needScore = customTemplateSlotUnlockCost[self.m_TemplateUnlockCount]
	else
		CUIManager.CloseUI(CLuaUIResources.UnlockCustomZswTemplateWnd)
		return
	end
	self.NeedScoreLabel.text = needScore

	local ownScore = 0
	if CClientMainPlayer.Inst then
		ownScore = CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.JiaYuanJiFen)
	end
	self.OwnScoreLabel.text = ownScore
end

--@region UIEvent

function LuaUnlockCustomZswTemplateWnd:OnCancelBtnClick()
end

function LuaUnlockCustomZswTemplateWnd:OnUnlockBtnClick()
	Gac2Gas.RequestBuyCustomFurnitureTemplateSlot()
	CUIManager.CloseUI(CLuaUIResources.UnlockCustomZswTemplateWnd)
end

--@endregion UIEvent

