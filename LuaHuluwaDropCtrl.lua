require("3rdParty/ScriptEvent")
require("common/common_include")

local NGUITools = import "NGUITools"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CUIManager = import "L10.UI.CUIManager"
local Vector3 = import "UnityEngine.Vector3"
local Screen = import "UnityEngine.Screen"
local Random = import "UnityEngine.Random"

CLuaHuluwaDropCtrl=class()

--掉落物部分
RegistClassMember(CLuaHuluwaDropCtrl, "m_DropTemplate")

function CLuaHuluwaDropCtrl:Awake()
	self.m_DropTemplate:SetActive(false)
end

function CLuaHuluwaDropCtrl:OnSyncHuLuDrop(dropTimes, dropIds, dropCounts)
	for i=0, dropIds.Count -1 do
		self:DropOnItem(dropTimes, dropIds[i], dropCounts[i])
	end
end

function CLuaHuluwaDropCtrl:DropOnItem(dropTimes, dropId, count)
	for i = 1, count do
		local item = NGUITools.AddChild(self.gameObject, self.m_DropTemplate)
		item:SetActive(true)
		local pos = CUIManager.UIMainCamera:ScreenToWorldPoint(Vector3(Random.Range(0, Screen.width), Screen.height, 0));
		pos = self.transform:InverseTransformPoint(pos)
		item.transform.localPosition = pos
		local csScript = item:GetComponent(typeof(CCommonLuaScript))
		csScript.m_LuaSelf:InitXiaoWanFaId(dropTimes, dropId)
	end
end

return CLuaHuluwaDropCtrl
