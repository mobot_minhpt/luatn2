local DelegateFactory  = import "DelegateFactory"

LuaYinHunPoZhiQuestionWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaYinHunPoZhiQuestionWnd, "up")
RegistClassMember(LuaYinHunPoZhiQuestionWnd, "question")
RegistClassMember(LuaYinHunPoZhiQuestionWnd, "optionTemplate")
RegistClassMember(LuaYinHunPoZhiQuestionWnd, "grid")
RegistClassMember(LuaYinHunPoZhiQuestionWnd, "closeButton")

RegistClassMember(LuaYinHunPoZhiQuestionWnd, "tick")

function LuaYinHunPoZhiQuestionWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
    self:InitActive()
end

function LuaYinHunPoZhiQuestionWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.up = anchor:Find("Up"):GetComponent(typeof(UILabel))
    self.question = anchor:Find("Question/Label"):GetComponent(typeof(UILabel))
    self.optionTemplate = anchor:Find("OptionTemplate").gameObject
    self.grid = anchor:Find("Grid"):GetComponent(typeof(UIGrid))
    self.closeButton = self.transform:Find("Wnd_Bg_Secondary_2/CloseButton").gameObject
end

function LuaYinHunPoZhiQuestionWnd:InitEventListener()
    UIEventListener.Get(self.closeButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)
end

function LuaYinHunPoZhiQuestionWnd:InitActive()
    self.optionTemplate:SetActive(false)
end


function LuaYinHunPoZhiQuestionWnd:Init()
    self.up.text = g_MessageMgr:FormatMessage("YINHUNPOZHI_QUESTION_TIP")
    self.question.text = QingMing2022_YHPZQuestion.GetData(LuaQingMing2022Mgr.YHPZInfo.questionId).Question
    self:InitOptions()
end

-- 初始化选项
function LuaYinHunPoZhiQuestionWnd:InitOptions()
    Extensions.RemoveAllChildren(self.grid.transform)

    local choice = QingMing2022_YHPZQuestion.GetData(LuaQingMing2022Mgr.YHPZInfo.questionId).Choice
    local optionTbl = LuaQingMing2022Mgr:Array2Tbl(choice)
    if not optionTbl then return end

    -- 四个选项随机排列
    for i = 1, 4 do
        optionTbl[i] = {id = i, text = optionTbl[i]}
    end

    for i = 4, 1, -1 do
        local randomId = math.random(i)

        local child = CUICommonDef.AddChild(self.grid.gameObject, self.optionTemplate)
        child:SetActive(true)
        child.transform:Find("Label"):GetComponent(typeof(UILabel)).text = optionTbl[randomId].text

        local id = optionTbl[randomId].id
        UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnOptionClick(id)
        end)

        table.remove(optionTbl, randomId)
    end

    self.grid:Reposition()
end

function LuaYinHunPoZhiQuestionWnd:StartTick()
    self.tick = RegisterTickOnce(function()
        CUIManager.CloseUI(CLuaUIResources.YinHunPoZhiQuestionWnd)
    end, 1000 * LuaQingMing2022Mgr.YHPZInfo.timeout)
end

function LuaYinHunPoZhiQuestionWnd:OnDestroy()
    if self.tick then
        UnRegisterTick(self.tick)
    end
end

--@region UIEvent

function LuaYinHunPoZhiQuestionWnd:OnOptionClick(id)
    Gac2Gas.QingMing2022PVEAnswerQuestion(id)
    CUIManager.CloseUI(CLuaUIResources.YinHunPoZhiQuestionWnd)
end

function LuaYinHunPoZhiQuestionWnd:OnCloseButtonClick()
    Gac2Gas.QingMing2022PVECancelAnswer()
    CUIManager.CloseUI(CLuaUIResources.YinHunPoZhiQuestionWnd)
end

--@endregion UIEvent

