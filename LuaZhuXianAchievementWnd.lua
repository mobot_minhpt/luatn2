local CAchievementMgr=import "L10.Game.CAchievementMgr"
local CUITexture=import "L10.UI.CUITexture"
local CUIFx=import "L10.UI.CUIFx"
local UIPanel=import "UIPanel"
local LuaTweenUtils = import "LuaTweenUtils"
local UInt32 = import "System.UInt32"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local PathType = import "DG.Tweening.PathType"
local PathMode = import "DG.Tweening.PathMode"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Ease = import "DG.Tweening.Ease"
local Vector4 = import "UnityEngine.Vector4"

CLuaZhuXianAchievementWnd = class()
RegistChildComponent(CLuaZhuXianAchievementWnd, "DiscussionButton", "DiscussionButton", GameObject)

RegistClassMember(CLuaZhuXianAchievementWnd,"m_DataList")
RegistClassMember(CLuaZhuXianAchievementWnd,"m_ValidDataList")
RegistClassMember(CLuaZhuXianAchievementWnd,"m_AchievementId")
RegistClassMember(CLuaZhuXianAchievementWnd,"m_GroupId")
RegistClassMember(CLuaZhuXianAchievementWnd,"m_Index")
RegistClassMember(CLuaZhuXianAchievementWnd,"m_PageLabel")
RegistClassMember(CLuaZhuXianAchievementWnd,"m_DescLabel")
RegistClassMember(CLuaZhuXianAchievementWnd,"m_Texture")
RegistClassMember(CLuaZhuXianAchievementWnd,"m_NameTexture")
RegistClassMember(CLuaZhuXianAchievementWnd,"m_PageFx")
RegistClassMember(CLuaZhuXianAchievementWnd,"m_LeftButton")
RegistClassMember(CLuaZhuXianAchievementWnd,"m_RightButton")

RegistClassMember(CLuaZhuXianAchievementWnd,"starTable")
RegistClassMember(CLuaZhuXianAchievementWnd,"starTemplate")
RegistClassMember(CLuaZhuXianAchievementWnd,"m_AchievementName")
RegistClassMember(CLuaZhuXianAchievementWnd,"m_Condition")
RegistClassMember(CLuaZhuXianAchievementWnd,"awardTable")
RegistClassMember(CLuaZhuXianAchievementWnd,"yuanbaoLabel")
RegistClassMember(CLuaZhuXianAchievementWnd,"silverLabel")
RegistClassMember(CLuaZhuXianAchievementWnd,"freeSilverLabel")
RegistClassMember(CLuaZhuXianAchievementWnd,"expLabel")
RegistClassMember(CLuaZhuXianAchievementWnd,"titleLabel")
RegistClassMember(CLuaZhuXianAchievementWnd,"level")
RegistClassMember(CLuaZhuXianAchievementWnd,"m_FadeTick")

RegistClassMember(CLuaZhuXianAchievementWnd,"getAwardButton")
RegistClassMember(CLuaZhuXianAchievementWnd,"getAwardButtonFx")
RegistClassMember(CLuaZhuXianAchievementWnd,"m_UnfinishLabel")
RegistClassMember(CLuaZhuXianAchievementWnd,"m_FinishLabel")
RegistClassMember(CLuaZhuXianAchievementWnd,"m_ShowFxTick")
RegistClassMember(CLuaZhuXianAchievementWnd,"m_PageFxAnimator")
RegistClassMember(CLuaZhuXianAchievementWnd,"m_StartFlip")
CLuaZhuXianAchievementWnd.AppointedGroupId = false

function CLuaZhuXianAchievementWnd:Awake()
    self.transform:Find("Anchor"):GetComponent(typeof(UIPanel)).alpha=0
    self.m_Index = 0
    self.m_PageFxAnimator = nil
    self.m_PageFx=self.transform:Find("Anchor/PageFx"):GetComponent(typeof(CUIFx))
    -- self.m_PageFx.OnLoadFxFinish = DelegateFactory.Action(function()
    --     self.m_PageFxAnimator = CommonDefs.GetComponentInChildren_GameObject_Type(self.m_PageFx.gameObject, typeof(Animator))
    -- end)
    self.m_PageFx:LoadFx("Fx/UI/Prefab/UI_pusongling_fanye01.prefab")
    self.m_PageFx.gameObject:SetActive(false)
    self.starTable = self.transform:Find("Anchor/NormalPage/Stars"):GetComponent(typeof(UITable))
    self.starTemplate = self.transform:Find("Anchor/NormalPage/starTemplate").gameObject
    self.starTemplate:SetActive(false)
    self.m_AchievementName = self.transform:Find("Anchor/NormalPage/AchieveName"):GetComponent(typeof(UILabel))
    self.m_Condition = self.transform:Find("Anchor/NormalPage/RequireCondition"):GetComponent(typeof(UILabel))
    self.m_FadeTick = nil

    self.awardTable = self.transform:Find("Anchor/NormalPage/AwardList"):GetComponent(typeof(UITable))
    self.yuanbaoLabel = self.transform:Find("Anchor/NormalPage/AwardList/Yuanbao"):GetComponent(typeof(UILabel))
    self.silverLabel = self.transform:Find("Anchor/NormalPage/AwardList/Silver"):GetComponent(typeof(UILabel))
    self.freeSilverLabel = self.transform:Find("Anchor/NormalPage/AwardList/FreeSilver"):GetComponent(typeof(UILabel))
    self.expLabel = self.transform:Find("Anchor/NormalPage/AwardList/Exp"):GetComponent(typeof(UILabel))
    self.titleLabel = self.transform:Find("Anchor/NormalPage/AwardList/Title"):GetComponent(typeof(UILabel))
    self.getAwardButton = self.transform:Find("Anchor/NormalPage/GetAwardBtn").gameObject
    self.m_UnfinishLabel = self.transform:Find("Anchor/NormalPage/UnfinishedLabel").gameObject
    self.m_FinishLabel = self.transform:Find("Anchor/NormalPage/FinishedLabel").gameObject
    self.m_StartFlip = false

    --为领取奖励按钮添加特效
    self.getAwardButtonFx = self.transform:Find("AwardButtonFxRoot/AwardButtonFx"):GetComponent(typeof(CUIFx))
    local b = NGUIMath.CalculateRelativeWidgetBounds(self.getAwardButton.transform)
    local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 0, false)
    self.getAwardButtonFx.transform.localPosition = waypoints[0]
    LuaTweenUtils.DOLuaLocalPath(self.getAwardButtonFx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)

    --延迟添加效果
    self.m_ShowFxTick = RegisterTickOnce(function()
        local active_state = self.getAwardButtonFx.gameObject.active
        self.getAwardButtonFx:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
        self.getAwardButtonFx.gameObject:SetActive(active_state)
        UnRegisterTick(self.m_ShowFxTick)
        self.m_ShowFxTick = nil
        LuaPlotDiscussionMgr:CheckIsEnable()
    end, 1500)

    self.getAwardButtonFx.gameObject:SetActive(false)
    self.getAwardButton:SetActive(false)
    self.m_UnfinishLabel:SetActive(false)
    self.m_FinishLabel:SetActive(false)

    self.m_NameTexture = self.transform:Find("Anchor/NormalPage/NameTexture"):GetComponent(typeof(CUITexture))

    UIEventListener.Get(self.getAwardButton).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.PlayerGetAchievementAward(self.m_AchievementId, "")
        --手动刷新任务状态
        self.getAwardButton:SetActive(false)
        self.m_UnfinishLabel:SetActive(false)
        self.m_FinishLabel:SetActive(true)
    end)

    self.m_PageLabel = self.transform:Find("Anchor/PageLabel"):GetComponent(typeof(UILabel))
    self.m_PageLabel.text=nil

    self.m_DescLabel=self.transform:Find("Anchor/NormalPage/ScrollView/DescLabel"):GetComponent(typeof(UILabel))
    self.m_DescLabel.text = nil
    self.m_Texture=self.transform:Find("Anchor/NormalPage/RoleTexture"):GetComponent(typeof(CUITexture))
    self.m_RoleTexturePlaceHolder = self.transform:Find("Anchor/NormalPage/RoleTexturePlaceHolder").gameObject


    self.m_LeftButton = self.transform:Find("Anchor/LeftButton").gameObject
    self.m_RightButton = self.transform:Find("Anchor/RightButton").gameObject
    UIEventListener.Get(self.m_LeftButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self.m_Index = self.m_Index-1
        if self.m_Index<=0 then
            self.m_Index = 1
        else
            -- self.m_PageFx.gameObject:SetActive(false)
            -- self.m_PageFx.gameObject:SetActive(true)
            -- if(self.m_PageFxAnimator)then
            --     self.m_PageFxAnimator:Play("pusongling_fanye01",0,0.383)
            --     self.m_PageFxAnimator.speed = self.m_PageFxAnimator.speed * -1 
            -- end

            self:FadeToData(self.m_ValidDataList[self.m_Index])
        end
        self:UpdateButton()
    end)
    UIEventListener.Get(self.m_RightButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self.m_Index = self.m_Index+1
        if self.m_Index>#self.m_ValidDataList then
            self.m_Index = #self.m_ValidDataList
        else
            self.m_PageFx.gameObject:SetActive(false)
            self.m_PageFx.gameObject:SetActive(true)
            --self.m_PageFx:DestroyFx()
            --self.m_PageFx:LoadFx("Fx/UI/Prefab/UI_pusongling_fanye01.prefab")
            -- if(self.m_PageFxAnimator)then
            --     self.m_PageFxAnimator.speed = 1
            --     self.m_PageFxAnimator:Play("pusongling_fanye01",0,0)
            -- end

            self:FadeToData(self.m_ValidDataList[self.m_Index])
        end
        self:UpdateButton()
    end)

    UIEventListener.Get(self.DiscussionButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDiscussionButtonClick()
	end)
    self.DiscussionButton.gameObject:SetActive(false)
end

function CLuaZhuXianAchievementWnd:OnDiscussionButtonClick()
    self.DiscussionButton.transform:Find("AlertSprite").gameObject:SetActive(false)
    LuaPlotDiscussionMgr.m_ShowCardWndType = 2
    CUIManager.ShowUI(CLuaUIResources.PlotCardsWnd)
end

function CLuaZhuXianAchievementWnd:UpdateButton()
    if self.m_Index==1 then
        self.m_LeftButton:SetActive(false)
    else
        self.m_LeftButton:SetActive(true)
        self.m_LeftButton.transform:Find("Alert").gameObject:SetActive(self:CheckIfSpecifiedPagesCanGetAward(false))
    end
    if self.m_Index==#self.m_ValidDataList then
        self.m_RightButton:SetActive(false)
    else
        self.m_RightButton:SetActive(true)
        self.m_RightButton.transform:Find("Alert").gameObject:SetActive(self:CheckIfSpecifiedPagesCanGetAward(true))
    end
end

function CLuaZhuXianAchievementWnd:CollectValidDatas()
    self.m_ValidDataList = {}
    local first_available_achievement_page = nil
    local appointed_page = nil
    local first_task_unfinished = false
    for k, v in ipairs(self.m_DataList) do
        local cellList = CommonDefs.DictGetValue(CAchievementMgr.Inst.m_GroupList, typeof(UInt32), v)
        if(cellList)then
            local inserted = false
            for i=1,cellList.Count do
                local id = cellList[i - 1]
                local achievement = Achievement_Achievement.GetData(id)
                if (achievement ~= nil and CClientMainPlayer.Inst.PlayProp.Achievement:HasAchievement(id)) then --已完成任务 塞入列表
                    table.insert(self.m_ValidDataList, v)
                    inserted = true
                    if(first_available_achievement_page == nil and not CClientMainPlayer.Inst.PlayProp.Achievement:HasAwardFinished(id))then
                        first_available_achievement_page = #self.m_ValidDataList
                    end
                    if(CLuaZhuXianAchievementWnd.AppointedGroupId and v == CLuaZhuXianAchievementWnd.AppointedGroupId)then --若存在预指定的页码，在此赋值
                        appointed_page = #self.m_ValidDataList
                        CLuaZhuXianAchievementWnd.AppointedGroupId = false
                    end
                    break
                end
            end
            if(k == 1 and not inserted)then
                first_task_unfinished = true
                table.insert(self.m_ValidDataList, v)
            end
        end
    end
    if(not first_task_unfinished)then
         table.insert(self.m_ValidDataList, 0)
    end 
    if(first_available_achievement_page)then
        self.m_Index = first_available_achievement_page
    elseif(appointed_page)then
        self.m_Index = appointed_page
    end
end

function CLuaZhuXianAchievementWnd:Init()
    self.m_Index = 1
    local tabName = LocalString.GetString("妖鬼狐")
    local subTabName = LocalString.GetString("妖鬼狐")
    self.m_DataList = {}
    if CommonDefs.DictContains(CAchievementMgr.Inst.m_TabNameList, typeof(String), tabName) then
        local tabInfo = CommonDefs.DictGetValue(CAchievementMgr.Inst.m_TabNameList, typeof(String), tabName)
        if CommonDefs.DictContains(tabInfo.subTabName, typeof(String), subTabName) then
            local subTabInfo = CommonDefs.DictGetValue(tabInfo.subTabName, typeof(String), subTabName)
            local groupId = subTabInfo.groupId

            CommonDefs.ListIterate(groupId,DelegateFactory.Action_object(function(k)
                table.insert( self.m_DataList,k )
            end))
        else
            local groupId = tabInfo.groupId
            CommonDefs.ListIterate(groupId,DelegateFactory.Action_object(function(k)
                table.insert( self.m_DataList,k )
            end))
        end
    end
    self:CollectValidDatas()

    if #self.m_ValidDataList>0 then
        --self.m_Index = 1
        self:UpdateButton()
        self:InitData(self.m_ValidDataList[self.m_Index])
        self:SetPage()
    end


end

function CLuaZhuXianAchievementWnd:SetPage()
    self.m_PageLabel.text=SafeStringFormat3(LocalString.GetString("第 %d/%d 页"),self.m_Index,#self.m_ValidDataList)
end

function CLuaZhuXianAchievementWnd:InitData(groupId)
    self.m_GroupId = groupId
    if groupId==0 then
        self.transform:Find("Anchor/LastPage").gameObject:SetActive(true)
        self.transform:Find("Anchor/NormalPage").gameObject:SetActive(false)
        return
    end
    self.transform:Find("Anchor/LastPage").gameObject:SetActive(false)
    self.transform:Find("Anchor/NormalPage").gameObject:SetActive(true)

    self.level = 0

    local cellList = CommonDefs.DictGetValue(CAchievementMgr.Inst.m_GroupList, typeof(UInt32), self.m_GroupId)
    local achievement=nil
    if cellList then
        for i=1,cellList.Count do
            local id = cellList[i-1]
            achievement = Achievement_Achievement.GetData(id)
            if achievement ~= nil and not CClientMainPlayer.Inst.PlayProp.Achievement:HasAwardFinished(id) then
                self.m_AchievementId = id
                break
            elseif i == cellList.Count and achievement ~= nil and CClientMainPlayer.Inst.PlayProp.Achievement:HasAwardFinished(cellList[i - 1]) then
                self.m_AchievementId = id
                break
            end
        end

        for i=1,cellList.Count do
            local id = cellList[i-1]
            local a = Achievement_Achievement.GetData(id)
            if a ~= nil and CClientMainPlayer.Inst.PlayProp.Achievement:HasAchievement(id) then
                self.level = self.level + 1
            end
        end
    end


    if achievement == nil then
        return
    end
    self.m_AchievementName.text = achievement.Name
    self.m_Condition.text = achievement.RequirementDisplay
    self:InitAward(achievement)
    self:InitStar()
    local design = Achievement_NewMainStory.GetData(groupId)
    if design then
        self.m_DescLabel.text = design.BackStory
        self.m_Texture.gameObject:SetActive(true)
        self.m_NameTexture.gameObject:SetActive(true)
        self.m_RoleTexturePlaceHolder:SetActive(false)
        self.m_Texture:LoadMaterial(design.Picture)
        self.m_NameTexture:LoadMaterial(design.NamePicture)
    else
        self.m_DescLabel.text=nil
        self.m_Texture:Clear()
        self.m_NameTexture:Clear()
        self.m_Texture.gameObject:SetActive(false)
        self.m_NameTexture.gameObject:SetActive(false)
        self.m_RoleTexturePlaceHolder:SetActive(true)
    end

    
end

function CLuaZhuXianAchievementWnd:FadeToData(groupId)
    if(groupId ~= self.m_GroupId)then
        self:SetPage()
        local current = self.transform:Find("Anchor/NormalPage").gameObject
        local _to = current
        if(self.m_GroupId == 0)then
            current = self.transform:Find("Anchor/LastPage").gameObject
        elseif(groupId == 0)then
            _to = self.transform:Find("Anchor/LastPage").gameObject
        end
        --注意！！TweenAlpha不会更新非Active的子UI,为防止Alpha值混乱，在Tween开始前Diactivate所有可能会频繁开关的组件
        self.getAwardButtonFx.gameObject:SetActive(false)
        self.getAwardButton:SetActive(false)
        self.m_UnfinishLabel:SetActive(false)
        self.m_FinishLabel:SetActive(false)
        local start_alpha = 1
        if(self.m_StartFlip)then
            start_alpha = 0
        end
        self.m_StartFlip = true
        LuaTweenUtils.TweenAlpha(current.transform, start_alpha, 0, 0.3)
        if(self.m_FadeTick)then
            UnRegisterTick(self.m_FadeTick)
            self.m_FadeTick = nil
        end
        self.m_FadeTick = RegisterTickOnce(function()
            self:InitData(groupId)
            LuaTweenUtils.TweenAlpha(_to.transform, 0, 1, 0.3)
            self.m_StartFlip = false
        end, 300)
        --LuaTweenUtils.SetDelay(LuaTweenUtils.TweenAlpha(_to.transform, 0, 1, 0.3), 0.3)
    end
end

function CLuaZhuXianAchievementWnd:InitStar( )
    Extensions.RemoveAllChildren(self.starTable.transform)

    local cellList = CommonDefs.DictGetValue(CAchievementMgr.Inst.m_GroupList, typeof(UInt32), self.m_GroupId)
    for i=1,cellList.Count do
        local instance = NGUITools.AddChild(self.starTable.gameObject, self.starTemplate)
        instance:SetActive(true)  
        instance:GetComponent(typeof(UISprite)).spriteName = i <= self.level and "playerAchievementView_star_1" or "playerAchievementView_star_2"
    end
    self.starTable:Reposition()
end
function CLuaZhuXianAchievementWnd:InitAward( achievement) 
    self.yuanbaoLabel.text = tostring(achievement.YuanBaoReward)
    self.yuanbaoLabel.gameObject:SetActive(achievement.YuanBaoReward > 0)
    self.silverLabel.text = tostring(achievement.SilverReward)
    self.silverLabel.gameObject:SetActive(achievement.SilverReward > 0)
    self.freeSilverLabel.text = tostring(achievement.FreeSilverReward)
    self.freeSilverLabel.gameObject:SetActive(achievement.FreeSilverReward > 0)
    self.expLabel.text = tostring(achievement.ExpReward)
    self.expLabel.gameObject:SetActive(achievement.ExpReward > 0)

    if achievement.TitleReward ~= nil and achievement.TitleReward > 0 then
        local title = Title_Title.GetData(achievement.TitleReward)
        if title ~= nil then
            self.titleLabel.text = title.Name
            self.titleLabel.gameObject:SetActive(true)
        else
            self.titleLabel.gameObject:SetActive(false)
        end
    else
        self.titleLabel.gameObject:SetActive(false)
    end

    self.getAwardButtonFx.gameObject:SetActive(false)
    self.awardTable:Reposition()
    if CClientMainPlayer.Inst.PlayProp.Achievement:HasAchievement(self.m_AchievementId) then
        print(self.m_AchievementId,CClientMainPlayer.Inst.PlayProp.Achievement:HasAwardFinished(self.m_AchievementId))
        if not CClientMainPlayer.Inst.PlayProp.Achievement:HasAwardFinished(self.m_AchievementId) then
            self.getAwardButtonFx.gameObject:SetActive(true)
            self.getAwardButton:SetActive(true)
            self.m_UnfinishLabel:SetActive(false)
            self.m_FinishLabel:SetActive(false)
        else
            self.getAwardButton:SetActive(false)
            self.m_UnfinishLabel:SetActive(false)
            self.m_FinishLabel:SetActive(true)
        end
    else
         local progress = 0
        if CClientMainPlayer.Inst.PlayProp.Achievement.ProgressInfo then
            if CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.Achievement.ProgressInfo, typeof(UInt32), self.m_AchievementId) then
                progress = tonumber(CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.Achievement.ProgressInfo, typeof(UInt32), self.m_AchievementId).Progress[1])
            end
        end

        local totalProgress = 0
        if not System.String.IsNullOrEmpty(achievement.NeedProgress) then
            local strs = CommonDefs.StringSplit_ArrayChar(CommonDefs.StringSplit_ArrayChar(achievement.NeedProgress, ";")[0], ",")
            totalProgress = System.Int32.Parse(strs[strs.Length - 1])
        end
        if(progress ~= 0 and totalProgress ~= 0 and progress == totalProgress)then
            self.getAwardButton:SetActive(false)
            self.m_UnfinishLabel:SetActive(false)
            self.m_FinishLabel:SetActive(true)
        else
            self.getAwardButton:SetActive(false)
            self.m_UnfinishLabel:SetActive(true)
            self.m_FinishLabel:SetActive(false)
        end

    end
    -- --应为是用了TweenAlpha，需要重置3个奖励状态图标的Alpha通道
    -- self.getAwardButton:GetComponent(typeof(UIWidget)).alpha = 1
    -- self.m_UnfinishLabel:GetComponent(typeof(UIWidget)).alpha = 1
    -- self.m_FinishLabel:GetComponent(typeof(UIWidget)).alpha = 1
end

function CLuaZhuXianAchievementWnd:OnEnable()
    g_ScriptEvent:AddListener("OnPlotDiscussionEnable",self,"OnPlotDiscussionEnable")
    g_ScriptEvent:AddListener("OnPlotCommentShowRedAlert",self,"OnPlotCommentShowRedAlert")
    g_ScriptEvent:AddListener("GetAchievementAwardSuccess", self, "GetAchievementAwardSuccess")
    g_ScriptEvent:AddListener("AddPlayerAchievement", self, "AddPlayerAchievement")
    g_ScriptEvent:AddListener("UpdatePlayerAchievementProgress", self, "UpdatePlayerAchievementProgress")
end


function CLuaZhuXianAchievementWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnPlotDiscussionEnable",self,"OnPlotDiscussionEnable")
    g_ScriptEvent:RemoveListener("OnPlotCommentShowRedAlert",self,"OnPlotCommentShowRedAlert")
    g_ScriptEvent:RemoveListener("GetAchievementAwardSuccess", self, "GetAchievementAwardSuccess")
    g_ScriptEvent:RemoveListener("AddPlayerAchievement", self, "AddPlayerAchievement")
    g_ScriptEvent:RemoveListener("UpdatePlayerAchievementProgress", self, "UpdatePlayerAchievementProgress")
end

function CLuaZhuXianAchievementWnd:OnPlotDiscussionEnable()
    g_ScriptEvent:RemoveListener("OnPlotDiscussionEnable",self,"OnPlotDiscussionEnable")
    local isEnable = LuaPlotDiscussionMgr:IsOpen()
    self.DiscussionButton.gameObject:SetActive(isEnable)
    if isEnable then
        LuaPlotDiscussionMgr:RequestRedDotInfo()
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.ZhuXianAchievementWndDiscussionButton)
    end
end

function CLuaZhuXianAchievementWnd:OnPlotCommentShowRedAlert(haoYiXingCardId, yaoGuiHuCardId, isClear)
    local alertSprite = self.DiscussionButton.transform:Find("AlertSprite")
    if isClear then
        alertSprite.gameObject:SetActive(false)
    end
    if yaoGuiHuCardId ~= 0 then
        alertSprite.gameObject:SetActive(true)
    end
end

function CLuaZhuXianAchievementWnd:GetAchievementAwardSuccess(args)
    self:OnAchievementUpdate(args[0])
end

function CLuaZhuXianAchievementWnd:AddPlayerAchievement(args)
    self:OnAchievementUpdate(args[0])
end

function CLuaZhuXianAchievementWnd:UpdatePlayerAchievementProgress(args)
    self:OnAchievementUpdate(args[0])
end

function CLuaZhuXianAchievementWnd:OnAchievementUpdate(id) 
    if id == self.m_AchievementId then
        self:InitData(self.m_GroupId)
    end
end

function CLuaZhuXianAchievementWnd:OnDestroy()
    if(self.m_FadeTick)then
        UnRegisterTick(self.m_FadeTick)
        self.m_FadeTick = nil
    end
    if(self.m_ShowFxTick)then
        UnRegisterTick(self.m_ShowFxTick)
        self.m_ShowFxTick = nil
    end
end

function CLuaZhuXianAchievementWnd:GetGuideGo(methodName)
    if methodName == "GetDiscussionButton" then
        return self.DiscussionButton.gameObject
    end
    return nil
end

function CLuaZhuXianAchievementWnd:CheckIfSpecifiedPagesCanGetAward(isSubsequent)
    local tabName = LocalString.GetString("妖鬼狐")
    local subTabName = tabName
    local data_list = {}
    local available = false
    if CommonDefs.DictContains(CAchievementMgr.Inst.m_TabNameList, typeof(String), tabName) then
        local tabInfo = CommonDefs.DictGetValue(CAchievementMgr.Inst.m_TabNameList, typeof(String), tabName)
        if CommonDefs.DictContains(tabInfo.subTabName, typeof(String), subTabName) then
            local subTabInfo = CommonDefs.DictGetValue(tabInfo.subTabName, typeof(String), subTabName)
            local groupId = subTabInfo.groupId
            CommonDefs.ListIterate(groupId,DelegateFactory.Action_object(function(k)
                table.insert( data_list,k )
            end))
        else
            local groupId = tabInfo.groupId
            CommonDefs.ListIterate(groupId,DelegateFactory.Action_object(function(k)
                table.insert( data_list,k )
            end))
        end
    end
    local check = function(v)
        local cellList = CommonDefs.DictGetValue(CAchievementMgr.Inst.m_GroupList, typeof(UInt32), v)
        if(cellList)then
            for i=1,cellList.Count do
                local id = cellList[i - 1]
                local achievement = Achievement_Achievement.GetData(id)
                if (achievement ~= nil and CClientMainPlayer.Inst.PlayProp.Achievement:HasAchievement(id) and not CClientMainPlayer.Inst.PlayProp.Achievement:HasAwardFinished(id)) then --未领取成就
                    available = true
                    break
                end
            end
        end
    end
    local idx = self.m_Index
    if isSubsequent then
        for i = idx + 1, #data_list do
            check(data_list[i])
            if(available)then
                break
            end
        end
    else 
        for i = 1, idx - 1 do
            check(data_list[i])
            if(available)then
                break
            end
        end
    end
    return available
end
