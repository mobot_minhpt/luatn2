
local GameObject = import "UnityEngine.GameObject"
local COpenEntryMgr=import "L10.Game.COpenEntryMgr"

local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"

LuaHaMaoTaskMessageBox = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHaMaoTaskMessageBox, "Label1", "Label1", UILabel)
RegistChildComponent(LuaHaMaoTaskMessageBox, "Label2", "Label2", UILabel)
RegistChildComponent(LuaHaMaoTaskMessageBox, "ButtonLabel", "Label", UILabel)
RegistChildComponent(LuaHaMaoTaskMessageBox, "CancelButton", "CancelButton", GameObject)
RegistChildComponent(LuaHaMaoTaskMessageBox, "OkButton", "OkButton", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaHaMaoTaskMessageBox, "m_Tick")
RegistClassMember(LuaHaMaoTaskMessageBox, "m_LeftTime")

function LuaHaMaoTaskMessageBox:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaHaMaoTaskMessageBox:Init()
    UIEventListener.Get(self.CancelButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CLuaUIResources.HaMaoTaskMessageBox)
        g_ScriptEvent:BroadcastInLua("HideHaMaoButton")
    end)
    UIEventListener.Get(self.OkButton).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.RequestAutoAcceptTask(22131980)
        -- CTaskMgr.Inst:TrackToDoTask(22131980)
        CUIManager.CloseUI(CLuaUIResources.HaMaoTaskMessageBox)
        g_ScriptEvent:BroadcastInLua("HideHaMaoButton")
    end)

    self.Label1.text = g_MessageMgr:FormatMessage("HaoYiXing_Alert_Msg1")
    self.Label2.text = g_MessageMgr:FormatMessage("HaoYiXing_Alert_Msg2")

    self.ButtonLabel.text = LocalString.GetString("暂不接取")

    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
    end
    self.m_LeftTime = 40
    self.ButtonLabel.text = SafeStringFormat3(LocalString.GetString("暂不接取").."(%s)",self.m_LeftTime)
    self.m_Tick = RegisterTickWithDuration(function ()
        if not self:OnTick() then 
            UnRegisterTick(self.m_Tick)
            self.m_Tick=nil
            CUIManager.CloseUI(CLuaUIResources.HaMaoTaskMessageBox)
        end
    end,1000,self.m_LeftTime*1000)



end
function LuaHaMaoTaskMessageBox:OnTick()
    self.m_LeftTime = self.m_LeftTime - 1
    self.ButtonLabel.text = SafeStringFormat3(LocalString.GetString("暂不接取").."(%s)",self.m_LeftTime)
    if self.m_LeftTime<=0 then
        return false
    end
    return true
end

function LuaHaMaoTaskMessageBox:OnDestroy()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick=nil
    end
    COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.HaMaoEntry)
    g_ScriptEvent:BroadcastInLua("HideHaMaoButton")

end

--@region UIEvent

--@endregion UIEvent

