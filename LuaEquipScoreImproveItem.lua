require("common/common_include")

local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CUITexture = import "L10.UI.CUITexture"
local CPlayerCapacityMgr = import "L10.Game.CPlayerCapacityMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local EnumEquipScoreImproveType = import "L10.Game.CPlayerCapacityMgr+EnumEquipScoreImproveType"
local NGUIText = import "NGUIText"
local EnumItemPlace = import "L10.Game.EnumItemPlace"


LuaEquipScoreImproveItem = class()
RegistClassMember(LuaEquipScoreImproveItem,"gameObject")
RegistClassMember(LuaEquipScoreImproveItem,"transform")

RegistClassMember(LuaEquipScoreImproveItem,"itemCell")
RegistClassMember(LuaEquipScoreImproveItem,"iconTexture")
RegistClassMember(LuaEquipScoreImproveItem,"disableSprite")
RegistClassMember(LuaEquipScoreImproveItem,"qualitySprite")
RegistClassMember(LuaEquipScoreImproveItem,"bindSprite")
RegistClassMember(LuaEquipScoreImproveItem,"equipNameLabel")
RegistClassMember(LuaEquipScoreImproveItem,"equipDescLabel")
RegistClassMember(LuaEquipScoreImproveItem,"targetValueLabel")
RegistClassMember(LuaEquipScoreImproveItem,"targetDescLabel")
RegistClassMember(LuaEquipScoreImproveItem,"button")

RegistClassMember(LuaEquipScoreImproveItem,"itemId")
RegistClassMember(LuaEquipScoreImproveItem,"bodyPos")

RegistClassMember(LuaEquipScoreImproveItem,"notMatchColor")
RegistClassMember(LuaEquipScoreImproveItem,"matchColor")
RegistClassMember(LuaEquipScoreImproveItem,"isMatch")

function LuaEquipScoreImproveItem:Init(gameObject)
	self.gameObject = gameObject
	self.transform = gameObject.transform
	self.itemId = nil
	self.bodyPos = 0

	self.itemCell = self.transform:Find("ItemCell").gameObject
	self.iconTexture = self.transform:Find("ItemCell/IconTexture"):GetComponent(typeof(CUITexture))
	self.disableSprite = self.transform:Find("ItemCell/DisableSprite"):GetComponent(typeof(UISprite))
	self.bindSprite = self.transform:Find("ItemCell/BindSprite"):GetComponent(typeof(UISprite))
	self.qualitySprite = self.transform:Find("ItemCell/QualitySprite"):GetComponent(typeof(UISprite))
	self.equipNameLabel = self.transform:Find("EquipNameLabel"):GetComponent(typeof(UILabel))
	self.equipDescLabel = self.transform:Find("EquipDescLabel"):GetComponent(typeof(UILabel))
	self.targetValueLabel = self.transform:Find("TargetValueLabel"):GetComponent(typeof(UILabel))
	self.targetDescLabel = self.transform:Find("TargetDescLabel"):GetComponent(typeof(UILabel))
	self.button = self.transform:Find("Button").gameObject
	self.disableSprite.enabled = false

	self.notMatchColor = NGUIText.ParseColor24("FFED5F",0)
	self.matchColor =  NGUIText.ParseColor24("B2B2B2",0)
	self.isMatch = false

	CommonDefs.AddOnClickListener(self.itemCell, DelegateFactory.Action_GameObject(function(go) self:OnItemCellClick() end), false)
	CommonDefs.AddOnClickListener(self.button, DelegateFactory.Action_GameObject(function(go) self:OnButtonClick() end), false)
end

function LuaEquipScoreImproveItem:Show(itemId, bodyPos, icon, bindSpriteName, qualitySpriteName, equipName, equipDesc, targetVal, targetDesc, isMatch, descMatch)
	if not self.gameObject then return end
	self.itemId = itemId
	self.bodyPos = bodyPos or 0
	self.iconTexture:LoadMaterial(icon)
	self.bindSprite.spriteName = bindSpriteName
	self.qualitySprite.spriteName = qualitySpriteName
	self.equipNameLabel.text = equipName
	self.equipDescLabel.text = equipDesc
	self.targetValueLabel.text = targetVal
	self.targetDescLabel.text = targetDesc
	self.isMatch = isMatch or false

	if isMatch then
		self.targetValueLabel.color = self.matchColor
	else
		self.targetValueLabel.color = self.notMatchColor
	end

	if isMatch and descMatch then
		self.targetDescLabel.color = self.matchColor
	else
		self.targetDescLabel.color = self.notMatchColor
	end
end

function LuaEquipScoreImproveItem:OnItemCellClick()
	CItemInfoMgr.ShowLinkItemInfo(self.itemId, false, nil, AlignType.ScreenRight, 0, 0, 0, 0)
end

function LuaEquipScoreImproveItem:OnButtonClick()
	local curType = CPlayerCapacityMgr.Inst.CurImproveType
	if curType == EnumEquipScoreImproveType.BasicScore then
		 CEquipmentProcessMgr.ShowWithEquip(0, 0, self.itemId, EnumItemPlace.Body, self.bodyPos, false)
	elseif curType == EnumEquipScoreImproveType.IntensifyScore then
		 CEquipmentProcessMgr.ShowWithEquip(1, 0, self.itemId, EnumItemPlace.Body, self.bodyPos, false)
	elseif curType == EnumEquipScoreImproveType.HoleScore then
		 CEquipmentProcessMgr.ShowWithEquip(2, 0, self.itemId, EnumItemPlace.Body, self.bodyPos, false)
	end
end


return LuaEquipScoreImproveItem
