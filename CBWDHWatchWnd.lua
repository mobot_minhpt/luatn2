-- Auto Generated!!
local CBiWuDaHuiMgr = import "L10.Game.CBiWuDaHuiMgr"
local CBWDHWatchGroupItem = import "L10.UI.CBWDHWatchGroupItem"
local CBWDHWatchItem = import "L10.UI.CBWDHWatchItem"
local CBWDHWatchWnd = import "L10.UI.CBWDHWatchWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CXialvPKMgr = import "L10.Game.CXialvPKMgr"
local DelegateFactory = import "DelegateFactory"
local Gac2Gas = import "L10.Game.Gac2Gas"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CBWDHWatchWnd.m_Init_CS2LuaHook = function (this)
    this.tableView.m_DataSource = this
    this.groupTableView.m_DataSource = this
    this.groupTableView:ReloadData(true, true)
    this.groupTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        this.infoLabel.text = this:GetInfoText(0)
        this.watchInfoList = nil
        this.tableView:ReloadData(true, true)
        if not CBiWuDaHuiMgr.Inst.QMJJWatch and not CXialvPKMgr.Inst.WatchXialvPK_Local and not CXialvPKMgr.Inst.WatchXialvPK_Cross then
            --比武分成本服和跨服，跨服只显示开元组
            if CLuaStarBiwuMgr:IsInStarBiwu() then
              Gac2Gas.QueryBiWuStageWatchInfo(5)
            else
              Gac2Gas.QueryBiWuStageWatchInfo((row + 1))
            end
        elseif CBiWuDaHuiMgr.Inst.QMJJWatch then
            Gac2Gas.QueryQMJJStageWatchInfo((row + 1))
        elseif CXialvPKMgr.Inst.WatchXialvPK_Local then
            Gac2Gas.XiaLvPkQueryStageWatchInfo((row + 1))
        elseif CXialvPKMgr.Inst.WatchXialvPK_Cross then
            Gac2Gas.CrossXiaLvPkQueryStageWatchInfo(5)
        end
    end)
    this.groupTableView:SetSelectRow(0, true)
end
CBWDHWatchWnd.m_Awake_CS2LuaHook = function (this)
    UIEventListener.Get(this.watchBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        local row = this.tableView.currentSelectRow
        if row > - 1 and row < this.watchInfoList.Count  then
            local playId = this.watchInfoList[row].playId
            if not CBiWuDaHuiMgr.Inst.QMJJWatch and not CXialvPKMgr.Inst.WatchXialvPK_Local and not CXialvPKMgr.Inst.WatchXialvPK_Cross then
                if CLuaStarBiwuMgr:IsInStarBiwu() then
                  Gac2Gas.RequestWatchBiWu(5, playId)
                else
                  Gac2Gas.RequestWatchBiWu((this.groupTableView.currentSelectRow + 1), playId)
                end
            elseif CBiWuDaHuiMgr.Inst.QMJJWatch then
                Gac2Gas.RequestWatchQMJJ((this.groupTableView.currentSelectRow + 1), playId)
            elseif CXialvPKMgr.Inst.WatchXialvPK_Local then
                Gac2Gas.XiaLvPkRequestWatchPk((this.groupTableView.currentSelectRow + 1), playId)
            elseif CXialvPKMgr.Inst.WatchXialvPK_Cross then
                Gac2Gas.CrossXiaLvPkRequestWatchPk(5, playId)
            end
        end
        --Gac2Gas.RequestWatchBiWu(0, 0);//stage playId
    end)
    UIEventListener.Get(this.closeBtn).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    this.infoLabel.text = this:GetInfoText(0)
end
CBWDHWatchWnd.m_AutoRefresh_CS2LuaHook = function (this)
    this.infoLabel.text = this:GetInfoText(0)
    this.watchInfoList = nil
    this.tableView:ReloadData(true, true)
    if not CBiWuDaHuiMgr.Inst.QMJJWatch and not CXialvPKMgr.Inst.WatchXialvPK_Local and not CXialvPKMgr.Inst.WatchXialvPK_Cross then
        if CLuaStarBiwuMgr:IsInStarBiwu() then
          Gac2Gas.QueryBiWuStageWatchInfo(5)
        else
          Gac2Gas.QueryBiWuStageWatchInfo((this.groupTableView.currentSelectRow + 1))
        end
    elseif CBiWuDaHuiMgr.Inst.QMJJWatch then
        Gac2Gas.QueryQMJJStageWatchInfo((this.groupTableView.currentSelectRow + 1))
    elseif CXialvPKMgr.Inst.WatchXialvPK_Local then
        Gac2Gas.XiaLvPkQueryStageWatchInfo((this.groupTableView.currentSelectRow + 1))
    elseif CXialvPKMgr.Inst.WatchXialvPK_Cross then
        Gac2Gas.CrossXiaLvPkQueryStageWatchInfo(5)
    end
end
CBWDHWatchWnd.m_OnQueryBiWuStageWatchInfoResult_CS2LuaHook = function (this, stage, list)
    if stage == this.groupTableView.currentSelectRow + 1 or (CXialvPKMgr.Inst.WatchXialvPK_Cross) or CLuaStarBiwuMgr:IsInStarBiwu() then
        this.watchInfoList = list
        this.tableView:ReloadData(true, true)
        this.tableView:SetSelectRow(0, true)

        local count = 0
        CommonDefs.ListIterate(this.watchInfoList, DelegateFactory.Action_object(function (___value)
            local item = ___value
            count = count + item.teamCount
        end))

        this.infoLabel.text = this:GetInfoText(count)
    end
end
CBWDHWatchWnd.m_NumberOfRows_CS2LuaHook = function (this, view)
    if view == this.groupTableView then
        if CXialvPKMgr.Inst.WatchXialvPK_Cross then
            return 1
        end
        if CLuaStarBiwuMgr:IsInStarBiwu() then return 1 end
        return 5
    else
        if this.watchInfoList ~= nil then
            return this.watchInfoList.Count
        end
        return 0
    end
end
CBWDHWatchWnd.m_ItemAt_CS2LuaHook = function (this, view, row)
    if view == this.groupTableView then
        if CXialvPKMgr.Inst.WatchXialvPK_Cross or CLuaStarBiwuMgr:IsInStarBiwu() then
            local item = TypeAs(this.groupTableView:GetFromPool(0), typeof(CBWDHWatchGroupItem))
            item:Init(5)
            return item
        end
        local cmp = TypeAs(this.groupTableView:GetFromPool(0), typeof(CBWDHWatchGroupItem))
        cmp:Init(row + 1)
        return cmp
    else
        if this.watchInfoList ~= nil then
            if row < this.watchInfoList.Count then
                local item = TypeAs(this.tableView:GetFromPool(0), typeof(CBWDHWatchItem))
                if item ~= nil then
                    item:Init(this.watchInfoList[row], row)
                    return item
                end
            end
        end
    end
    return nil
end
