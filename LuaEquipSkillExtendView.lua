require("common/common_include")

local UIScrollView = import "UIScrollView"
local UIGrid  = import "UIGrid"
local Extensions = import "Extensions"
local CItemMgr = import "L10.Game.CItemMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CIdentifyEquipCell = import "L10.UI.Equipment.CIdentifyEquipCell"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local CButton = import "L10.UI.CButton"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local Item_Item = import "L10.Game.Item_Item"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemAccessTipMgr = import "L10.UI.CItemAccessTipMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local IdentifySkill_Settings = import "L10.Game.IdentifySkill_Settings"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local EnumSkillInfoContext = import "L10.UI.CSkillInfoMgr+EnumSkillInfoContext"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Vector3 = import "UnityEngine.Vector3"
local CUIFx = import "L10.UI.CUIFx"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaEquipSkillExtendView = class()
RegistClassMember(LuaEquipSkillExtendView,"inited")
RegistClassMember(LuaEquipSkillExtendView,"gameObject")
RegistClassMember(LuaEquipSkillExtendView,"transform")

RegistClassMember(LuaEquipSkillExtendView,"m_ScrollView")
RegistClassMember(LuaEquipSkillExtendView,"m_Grid")
RegistClassMember(LuaEquipSkillExtendView,"m_EquipCellTemplate")

RegistClassMember(LuaEquipSkillExtendView,"m_EquipIconTexture")
RegistClassMember(LuaEquipSkillExtendView,"m_EquipQualityFx")
RegistClassMember(LuaEquipSkillExtendView,"m_EquipSkillIconTexture")
RegistClassMember(LuaEquipSkillExtendView,"m_EquipSkillLevelLabel")
RegistClassMember(LuaEquipSkillExtendView,"m_EquipSkillNameLabel")
RegistClassMember(LuaEquipSkillExtendView,"m_EquipSkillExpiredTimeLabel")
RegistClassMember(LuaEquipSkillExtendView,"m_EquipSkillExtendTimesLabel")

RegistClassMember(LuaEquipSkillExtendView,"m_EquipSkillTotalExtendTimesLabel")

RegistClassMember(LuaEquipSkillExtendView,"m_NeedItemRoot")
RegistClassMember(LuaEquipSkillExtendView,"m_NeedItemIcon")
RegistClassMember(LuaEquipSkillExtendView,"m_NeedItemAmountLabel")
RegistClassMember(LuaEquipSkillExtendView,"m_NeedItemQualitySprite")
RegistClassMember(LuaEquipSkillExtendView,"m_NeedItemDisableSprite")

RegistClassMember(LuaEquipSkillExtendView,"m_ExtendButton")
RegistClassMember(LuaEquipSkillExtendView,"m_InfoButton")

RegistClassMember(LuaEquipSkillExtendView,"m_SkillFx")
RegistClassMember(LuaEquipSkillExtendView,"m_ExtendTimeFx")



RegistClassMember(LuaEquipSkillExtendView,"m_EquipId")
RegistClassMember(LuaEquipSkillExtendView,"m_NeedItemTemplateId")
RegistClassMember(LuaEquipSkillExtendView,"m_NeedItemNum")
RegistClassMember(LuaEquipSkillExtendView,"m_Cells")

function LuaEquipSkillExtendView:InitWithGameObject(viewRootGo)
    if self.inited then
    	return
    end
    self.gameObject = viewRootGo
    self.transform = viewRootGo.transform

    self.m_ScrollView = self.transform:Find("EquipList/ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_Grid = self.transform:Find("EquipList/ScrollView/Grid"):GetComponent(typeof(UIGrid))
    self.m_EquipCellTemplate = self.transform:Find("EquipList/ScrollView/EquipCell").gameObject

    self.m_EquipIconTexture = self.transform:Find("Bg/EquipIcon"):GetComponent(typeof(CUITexture))
    self.m_EquipQualityFx = self.transform:Find("Bg/EquipIcon/QualityFx"):GetComponent(typeof(CUIFx))
    self.m_EquipSkillIconTexture = self.transform:Find("Bg/Skill/SkillIcon"):GetComponent(typeof(CUITexture))
    self.m_EquipSkillLevelLabel = self.transform:Find("Bg/Skill/SkillIcon/LevelLabel"):GetComponent(typeof(UILabel))
    self.m_EquipSkillNameLabel = self.transform:Find("Bg/Skill/SkillIcon/NameLabel"):GetComponent(typeof(UILabel))
    self.m_EquipSkillExpiredTimeLabel = self.transform:Find("ExpiredTime/ExpiredTimeLabel"):GetComponent(typeof(UILabel))
    self.m_EquipSkillExtendTimesLabel = self.transform:Find("ExtendTimes/ExtendTimesLabel"):GetComponent(typeof(UILabel))

    self.m_EquipSkillTotalExtendTimesLabel = self.transform:Find("TotalExtendTimesLabel"):GetComponent(typeof(UILabel))

    self.m_NeedItemRoot = self.transform:Find("CostRoot/ItemCell").gameObject
	self.m_NeedItemIcon = self.transform:Find("CostRoot/ItemCell/IconTexture"):GetComponent(typeof(CUITexture))
	self.m_NeedItemAmountLabel = self.transform:Find("CostRoot/ItemCell/AmountLabel"):GetComponent(typeof(UILabel))
	self.m_NeedItemQualitySprite = self.transform:Find("CostRoot/ItemCell/QualitySprite"):GetComponent(typeof(UISprite))
	self.m_NeedItemDisableSprite = self.transform:Find("CostRoot/ItemCell/DisableSprite").gameObject

	self.m_ExtendButton = self.transform:Find("ExtendButton"):GetComponent(typeof(CButton))
    self.m_InfoButton = self.transform:Find("InfoButton").gameObject

    self.m_SkillFx = self.transform:Find("Bg/Skill/SkillFx"):GetComponent(typeof(CUIFx))
    self.m_ExtendTimeFx = self.transform:Find("ExpiredTime/ExpiredTimeLabel/ExtendTimeFx"):GetComponent(typeof(CUIFx))

    self.m_EquipCellTemplate:SetActive(false)

    CommonDefs.AddOnClickListener(self.m_NeedItemRoot, DelegateFactory.Action_GameObject(function(go) self:OnNeedItemClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_ExtendButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnExtendButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_EquipIconTexture.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnEquipIconClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_EquipSkillIconTexture.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnEquipSkillIconClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_InfoButton, DelegateFactory.Action_GameObject(function(go) self:OnInfoButtonClick() end), false)
    self.inited = true
end

function LuaEquipSkillExtendView:SetDefaultItem(itemId)
	local item = CItemMgr.Inst:GetById(itemId)
	if item and item.IsEquip and item.Equip.Identifiable > 0 then
		self.m_EquipId = itemId
	else
		self.m_EquipId = nil
	end
end

function LuaEquipSkillExtendView:Init()
	if not self.inited then return end

    Extensions.RemoveAllChildren(self.m_Grid.transform)
    self.m_Cells = {}
    local items = LuaEquipIdentifySkillMgr.GetCanExtendEquipments()

    for _, itemId in ipairs(items) do

    	local item = CItemMgr.Inst:GetById(itemId)
    	if item then
    		local go = CUICommonDef.AddChild(self.m_Grid.gameObject, self.m_EquipCellTemplate)
    		go:SetActive(true)
            local itemCell = go:GetComponent(typeof(CIdentifyEquipCell))
            itemCell:Init(itemId)
            CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function(go) self:OnItemCellClick(go) end), false)
            table.insert(self.m_Cells, itemCell)
    	end

    end
    self.m_Grid:Reposition()
    self.m_ScrollView:ResetPosition()
    self:UpdateCurEquipInfo(self.m_EquipId)
    self.m_SkillFx:DestroyFx()
    self.m_ExtendTimeFx:DestroyFx()
end

function LuaEquipSkillExtendView:RequestCostInfo()
	self:ClearCostInfo()
	if self.m_EquipId then
		local curCommonItem = self.m_EquipId and CItemMgr.Inst:GetById(self.m_EquipId)
		local curEquip = curCommonItem and curCommonItem.Equip
		if curEquip and curEquip.IdentifyExtendTimes < IdentifySkill_Settings.GetData().MaxIdentifyExtendTimes then
    		Gac2Gas.IdentifySkillRequestExtendSkill(self.m_EquipId, 1, true)
    	end
    end
end

function LuaEquipSkillExtendView:OnSendItem(itemId)
	if self.m_EquipId == itemId then
		self:UpdateCurEquipInfo(self.m_EquipId)
	else
        local commonItem = CItemMgr.Inst:GetById(itemId)
        if not commonItem or not commonItem.IsItem then
            return
        end
        --如果消耗道具数量发生变化，更新一下
        if commonItem.TemplateId == self.m_NeedItemTemplateId then
            self:UpdateCostInfo(self.m_EquipId, self.m_NeedItemTemplateId, self.m_NeedItemNum)
        end
    end
end

function LuaEquipSkillExtendView:OnSetItemAt(place, pos, oldItemId, newItemId)
	self:UpdateCostInfo(self.m_EquipId, self.m_NeedItemTemplateId, self.m_NeedItemNum)
end

function LuaEquipSkillExtendView:UpdateCostInfo(equipId, itemTemplateId, num)
	if self.m_EquipId~=equipId then return end
	self.m_NeedItemTemplateId = itemTemplateId
	self.m_NeedItemNum = num
	local item = Item_Item.GetData(itemTemplateId)
    if not item then
        self.m_NeedItemIcon:Clear()
        self.m_NeedItemQualitySprite.spriteName = CUICommonDef.GetItemCellBorder(nil, nil, false)
        self.m_NeedItemDisableSprite:SetActive(false)
        self.m_NeedItemAmountLabel.text = nil
        self.m_ExtendButton.Enabled = false
    else
    	self.m_NeedItemIcon:LoadMaterial(item.Icon)
        self.m_NeedItemQualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item, nil, false)
        local ownCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemTemplateId)
        self.m_NeedItemDisableSprite:SetActive(ownCount < num)
        if ownCount>=num then
            self.m_NeedItemAmountLabel.text = SafeStringFormat3("%d/%d", ownCount, num)
        else
            self.m_NeedItemAmountLabel.text = SafeStringFormat3("[c][ff0000]%d[-][/c]/%d", ownCount, num)
        end
        self.m_ExtendButton.Enabled = true
    end
end

function LuaEquipSkillExtendView:ClearCostInfo()
	self.m_NeedItemTemplateId = nil
	self.m_NeedItemNum = nil
	self.m_NeedItemIcon:Clear()
    self.m_NeedItemQualitySprite.spriteName = CUICommonDef.GetItemCellBorder(nil, nil, false)
    self.m_NeedItemDisableSprite:SetActive(false)
    self.m_NeedItemAmountLabel.text = nil
end

function LuaEquipSkillExtendView:UpdateCurEquipInfo(equipId)
	local curCommonItem = equipId and CItemMgr.Inst:GetById(equipId)
	local curEquip = curCommonItem and curCommonItem.Equip
	self.m_EquipId = equipId
	if curEquip then
		self.m_EquipIconTexture:LoadMaterial(curEquip.BigIcon)
        if curEquip.UseSquareBigIcon then
            self.m_EquipIconTexture.texture.height = self.m_EquipIconTexture.texture.width
        else
            self.m_EquipIconTexture.texture.height = self.m_EquipIconTexture.texture.width * 2
        end

        if curEquip.IsPurpleEquipment or curEquip.IsGhostEquipment then
            self.m_EquipQualityFx:LoadFx("Fx/UI/Prefab/UI_zhuangbeijianding_zise.prefab")
        elseif curEquip.IsRedEquipment then
            self.m_EquipQualityFx:LoadFx("Fx/UI/Prefab/UI_zhuangbeijianding_hongse.prefab")
        elseif curEquip.IsBlueEquipment then
            self.m_EquipQualityFx:LoadFx("Fx/UI/Prefab/UI_zhuangbeijianding_lanse.prefab")
        else
            self.m_EquipQualityFx:DestroyFx()
        end

		local skill = Skill_AllSkills.GetData(curEquip.FixedIdentifySkillId)
		if skill then
			self.m_EquipSkillIconTexture:LoadSkillIcon(skill.SkillIcon)
            self.m_EquipSkillLevelLabel.gameObject:SetActive(true)
            self.m_EquipSkillLevelLabel.text = SafeStringFormat3("lv.%d", skill.Level)
            self.m_EquipSkillNameLabel.text = skill.Name
		else
			self.m_EquipSkillIconTexture:Clear()
            self.m_EquipSkillLevelLabel.gameObject:SetActive(false)
            self.m_EquipSkillNameLabel.text = nil
		end
    	self.m_EquipSkillExpiredTimeLabel.text = curEquip.IdentifySkillExpireTimeInfoForExtendDisplay
    	if curEquip.IdentifyExtendTimes < IdentifySkill_Settings.GetData().MaxIdentifyExtendTimes then
            local days = math.floor(IdentifySkill_Settings.GetData().IdentifySkillExtendTime/3600/24)
            if curEquip.OriginalIdentifyEndTime <= CServerTimeMgr.Inst.timeStamp then
                self.m_EquipSkillExtendTimesLabel.text = SafeStringFormat3(LocalString.GetString("恢复到%d级，有效期延长%d天"), CLuaDesignMgr.Skill_AllSkills_GetLevel(curEquip.OriginalIdentifySkillId), days)
    		else
                self.m_EquipSkillExtendTimesLabel.text = SafeStringFormat3(LocalString.GetString("有效期延长%d天"), days)
            end
            self.m_ExtendButton.Enabled = true
    	else
    		self.m_EquipSkillExtendTimesLabel.text = LocalString.GetString("[c][ff0000]不可补益[-][/c]")
    		self.m_ExtendButton.Enabled = false
    	end

        self.m_EquipSkillTotalExtendTimesLabel.text = SafeStringFormat3(LocalString.GetString("累计补益[c][AC6112]%d/%d[-][/c]次"), curEquip.IdentifyExtendTimes, curEquip.MaxIdentifySkillExtendTimes)
	else
		self.m_EquipIconTexture:Clear()
		self.m_EquipSkillIconTexture:Clear()
        self.m_EquipSkillLevelLabel.text = nil
        self.m_EquipSkillLevelLabel.gameObject:SetActive(false)
        self.m_EquipSkillNameLabel.text = nil
    	self.m_EquipSkillExpiredTimeLabel.text = nil
    	self.m_EquipSkillExtendTimesLabel.text = nil
        self.m_EquipSkillTotalExtendTimesLabel.text = nil
    	self.m_ExtendButton.Enabled = false
	end
    self.m_ExtendButton.Enabled = false --请求消耗信息，先禁掉按钮
	self:RequestCostInfo()
end

function LuaEquipSkillExtendView:OnItemCellClick(go)
    local curCell = go:GetComponent(typeof(CIdentifyEquipCell))
    if curCell.equipId == nil then return end
    self:UpdateCurEquipInfo(curCell.equipId)

    for _, cell in pairs(self.m_Cells) do
       if cell == curCell then
       		cell:SetSelected(true)
       else
       		cell:SetSelected(false)
       end
    end
end

function LuaEquipSkillExtendView:OnNeedItemClick(go)
	if not self.m_NeedItemTemplateId or not self.m_NeedItemNum then return end
	local ownCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_NeedItemTemplateId)
    if ownCount < self.m_NeedItemNum then
        CItemAccessTipMgr.Inst:ShowAccessTip(nil, self.m_NeedItemTemplateId, false, go.transform, CTooltipAlignType.Top)
    end
end

function LuaEquipSkillExtendView:OnExtendButtonClick(go)
    local curCommonItem = self.m_EquipId and CItemMgr.Inst:GetById(self.m_EquipId)
    local curEquip = curCommonItem and curCommonItem.Equip
    if curEquip and self.m_NeedItemNum then
        local skill = Skill_AllSkills.GetData(curEquip.FixedIdentifySkillId)
        local msg = g_MessageMgr:FormatMessage("Equip_ReIdentify_DelayTime_Confirm", self.m_NeedItemNum, skill.Name)
        MessageWndManager.ShowOKCancelMessage(msg, 
            DelegateFactory.Action(function() 
                Gac2Gas.IdentifySkillRequestExtendSkill(self.m_EquipId, 1, false)
            end), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    end
end

function LuaEquipSkillExtendView:IdentifySkillRequestExtendSuccess(equipId)
    self:PlayExtendSuccessFx(equipId)
end

function LuaEquipSkillExtendView:PlayExtendSuccessFx(equipId)
    if self.m_EquipId and self.m_EquipId == equipId then
        self.m_SkillFx:DestroyFx()
        self.m_SkillFx:LoadFx("Fx/UI/Prefab/UI_jinengjianding_02.prefab")
        self.m_ExtendTimeFx:DestroyFx()
        self.m_ExtendTimeFx:LoadFx("Fx/UI/Prefab/UI_jinengjianding_yanshi.prefab")
    end
end

function LuaEquipSkillExtendView:OnEquipIconClick(go)
    local curCommonItem = CItemMgr.Inst:GetById(self.m_EquipId)
    local curEquip = curCommonItem and curCommonItem.Equip
    if curEquip then
        CItemInfoMgr.ShowLinkItemInfo(curEquip.Id, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end
end

function LuaEquipSkillExtendView:OnEquipSkillIconClick(go)
    local curCommonItem = CItemMgr.Inst:GetById(self.m_EquipId)
    local curEquip = curCommonItem and curCommonItem.Equip
    if curEquip then
        local skill = Skill_AllSkills.GetData(curEquip.FixedIdentifySkillId)
        CSkillInfoMgr.ShowSkillInfoWnd(curEquip.FixedIdentifySkillId, self.m_EquipId, false, Vector3.zero, EnumSkillInfoContext.MainPlayerSkillIdentify, true, 0, 0, nil)
    end
end

function LuaEquipSkillExtendView:OnInfoButtonClick()
    g_MessageMgr:ShowMessage("Equip_ReIdentify_DelayTime_ReadMe")
end


return LuaEquipSkillExtendView
