local CUITexture = import "L10.UI.CUITexture"
local CButton = import "L10.UI.CButton"
local House_SkyBoxKind = import "L10.Game.House_SkyBoxKind"
local CCommonLuaWnd = import "L10.UI.CCommonLuaWnd"
local Item_Item = import "L10.Game.Item_Item"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local Color = import "UnityEngine.Color"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Constants = import "L10.Game.Constants"

LuaHouseUnlockSkyboxWnd = class()

RegistChildComponent(LuaHouseUnlockSkyboxWnd, "HintLabel", UILabel)
RegistChildComponent(LuaHouseUnlockSkyboxWnd, "PreviewTexture", CUITexture)

RegistChildComponent(LuaHouseUnlockSkyboxWnd, "OwnFoodLabel", UILabel)
RegistChildComponent(LuaHouseUnlockSkyboxWnd, "OwnWoodLabel", UILabel)
RegistChildComponent(LuaHouseUnlockSkyboxWnd, "OwnStoneLabel", UILabel)
RegistChildComponent(LuaHouseUnlockSkyboxWnd, "OwnMoneyLabel", UILabel)
RegistChildComponent(LuaHouseUnlockSkyboxWnd, "AddFoodBtn", GameObject)
RegistChildComponent(LuaHouseUnlockSkyboxWnd, "AddWoodBtn", GameObject)
RegistChildComponent(LuaHouseUnlockSkyboxWnd, "AddStoneBtn", GameObject)
RegistChildComponent(LuaHouseUnlockSkyboxWnd, "AddMoneyBtn", GameObject)

RegistChildComponent(LuaHouseUnlockSkyboxWnd, "CostFoodLabel", UILabel)
RegistChildComponent(LuaHouseUnlockSkyboxWnd, "CostWoodLabel", UILabel)
RegistChildComponent(LuaHouseUnlockSkyboxWnd, "CostStoneLabel", UILabel)
RegistChildComponent(LuaHouseUnlockSkyboxWnd, "CostMoneyLabel", UILabel)

RegistChildComponent(LuaHouseUnlockSkyboxWnd, "UnlockButton", CButton)
RegistChildComponent(LuaHouseUnlockSkyboxWnd, "TimeLimitLabel", UILabel)

RegistClassMember(LuaHouseUnlockSkyboxWnd, "m_Wnd")

function LuaHouseUnlockSkyboxWnd:Init()
    self.m_Wnd = self.gameObject:GetComponent(typeof(CCommonLuaWnd))
    local skyKind = House_SkyBoxKind.GetData(CLuaHouseMgr.m_SelectedSkyKind)
    if not skyKind then 
        self.m_Wnd:Close()
        return
    end

    local skyItem = Item_Item.GetData(skyKind.ItemID)
    if not skyItem then 
        self.m_Wnd:Close()
        return
    end

    self.TimeLimitLabel.text = nil
    
    self.HintLabel.text = SafeStringFormat3(LocalString.GetString("解锁[ffcf0e]%s[-]天空效果"), skyItem.Name)
    self.CostWoodLabel.text = tostring(skyKind.Price[0])
    self.CostStoneLabel.text = tostring(skyKind.Price[1])
    self.CostFoodLabel.text = tostring(skyKind.Price[2])
    self.CostMoneyLabel.text = tostring(skyKind.YinLiangPrice)
    
    local path = SafeStringFormat3("assets/res/%s", skyKind.Preview)
    self.PreviewTexture:LoadMaterial(path)

    local food = CClientHouseMgr.Inst.mConstructProp.Food
    local stone = CClientHouseMgr.Inst.mConstructProp.Stone
    local wood = CClientHouseMgr.Inst.mConstructProp.Wood
    self.OwnFoodLabel.text = tostring(food)
    self.OwnStoneLabel.text = tostring(stone)
    self.OwnWoodLabel.text = tostring(wood)
    self.OwnMoneyLabel.text = tostring(CClientMainPlayer.Inst.Silver)

    CommonDefs.AddOnClickListener(self.UnlockButton.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnUnlockButtonClicked(go)
    end), false)

    CommonDefs.AddOnClickListener(self.AddFoodBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnGetButtonClicked(go)
    end), false)

    CommonDefs.AddOnClickListener(self.AddWoodBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnGetButtonClicked(go)
    end), false)

    CommonDefs.AddOnClickListener(self.AddStoneBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnGetButtonClicked(go)
    end), false)

    CommonDefs.AddOnClickListener(self.AddMoneyBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        local msg = g_MessageMgr:FormatMessage("NotEnough_Silver_QuickBuy")
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
			CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
		end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
    end), false)

    local SkyBoxUseTime = House_Setting.GetData().SkyBoxUseTime
    if CommonDefs.DictContains_LuaCall(SkyBoxUseTime, tostring(skyKind.Kind)) then
        self.TimeLimitLabel.text = SafeStringFormat3(LocalString.GetString("使用有效期：%s"), CommonDefs.DictGetValue_LuaCall(SkyBoxUseTime, tostring(skyKind.Kind)))
    end

    self:UpdateButtonStatus()
end


function LuaHouseUnlockSkyboxWnd:OnUnlockButtonClicked(go)
    Gac2Gas.RequestBuyHouseSkyBox(CLuaHouseMgr.m_SelectedSkyKind)
end

function LuaHouseUnlockSkyboxWnd:OnGetButtonClicked(go)
    -- 选中五石仓
    CShopMallMgr.ShowLinyuShoppingMall(21003558)
end


function LuaHouseUnlockSkyboxWnd:UpdateButtonStatus()
    local skyKind = House_SkyBoxKind.GetData(CLuaHouseMgr.m_SelectedSkyKind)
    if not skyKind then 
        self.m_Wnd:Close()
        return
    end

    local food = CClientHouseMgr.Inst.mConstructProp.Food
    local stone = CClientHouseMgr.Inst.mConstructProp.Stone
    local wood = CClientHouseMgr.Inst.mConstructProp.Wood
    local money = CClientMainPlayer.Inst.Silver

    self.OwnFoodLabel.color = Color.white
    if food < skyKind.Price[2] then
        self.OwnFoodLabel.color = Color.red
    end

    self.OwnStoneLabel.color = Color.white
    if stone < skyKind.Price[1] then
        self.OwnStoneLabel.color = Color.red
    end

    self.OwnWoodLabel.color = Color.white
    if wood < skyKind.Price[0] then
        self.OwnWoodLabel.color = Color.red
    end

    self.OwnMoneyLabel.color = Color.white
    if money < skyKind.YinLiangPrice then
        self.OwnMoneyLabel.color = Color.red
    end

    self.UnlockButton.Enabled = skyKind.Price[0] <= wood and skyKind.Price[1] <= stone and skyKind.Price[2] <= food and money >= skyKind.YinLiangPrice
end

function LuaHouseUnlockSkyboxWnd:OnMainPlayerUpdateMoney()
    self:Init()
end

function LuaHouseUnlockSkyboxWnd:OnUpdateHouseResource(args)
    self:Init()
end

function LuaHouseUnlockSkyboxWnd:OnEnable()
    Gac2Gas.QueryHouseData()
    g_ScriptEvent:AddListener("MainPlayerUpdateMoney", self, "OnMainPlayerUpdateMoney")
    g_ScriptEvent:AddListener("OnUpdateHouseResource", self, "OnUpdateHouseResource")
    g_ScriptEvent:AddListener("OnSendHouseData", self, "OnUpdateHouseResource")
end

function LuaHouseUnlockSkyboxWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("MainPlayerUpdateMoney", self, "OnMainPlayerUpdateMoney")
    g_ScriptEvent:RemoveListener("OnUpdateHouseResource", self, "OnUpdateHouseResource")
    g_ScriptEvent:RemoveListener("OnSendHouseData", self, "OnUpdateHouseResource")
end