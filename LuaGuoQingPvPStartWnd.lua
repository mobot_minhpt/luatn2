local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Animator = import "UnityEngine.Animator"

LuaGuoQingPvPStartWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuoQingPvPStartWnd, "Right_Player", "Right_Player", GameObject)
RegistChildComponent(LuaGuoQingPvPStartWnd, "Left_Player", "Left_Player", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaGuoQingPvPStartWnd, "mainAnimator")
RegistClassMember(LuaGuoQingPvPStartWnd, "tick1")
RegistClassMember(LuaGuoQingPvPStartWnd, "tick2")
function LuaGuoQingPvPStartWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.mainAnimator = self.transform:GetComponent(typeof(Animator))
    CUIManager.CloseUI(CLuaUIResources.GuoQingPvPSkillSelectWnd)
    self.tick1 = nil
    self.tick2 = nil
end

function LuaGuoQingPvPStartWnd:Init()
    self:ShowInfo()
    
    self.tick1 = RegisterTickOnce(function()
        self.mainAnimator:Play("guoqingpvpstartwnd_close", 0, 0)
        self.tick1 = nil
        self.tick2 = RegisterTickOnce(function()
            CUIManager.CloseUI(CLuaUIResources.GuoQingPvPStartWnd)
            self.tick2 = nil
        end, 200)
	end, 2300)
end
function  LuaGuoQingPvPStartWnd:OnEnable()
    self.mainAnimator.enabled = true
end

function  LuaGuoQingPvPStartWnd:OnDisable()
    if self.tick1 ~= nil then
        UnRegisterTick(self.tick1)
    end
    if self.tick2 ~= nil then
        UnRegisterTick(self.tick2)
    end
end

--@region UIEvent

function  LuaGuoQingPvPStartWnd:ClearInfo()
    for i = 1,3 do
        local objname = "Player"..tostring(i)
        local instance = self.Left_Player.transform:Find(objname).gameObject
        local Info = instance.transform:Find("Info").gameObject
        local skillicon = instance.transform:Find("skillicon").gameObject
        skillicon:SetActive(false)
        Info:SetActive(false)
    end
    for i = 1,3 do
        local objname = "Player"..tostring(i)
        local instance = self.Right_Player.transform:Find(objname).gameObject
        local Info = instance.transform:Find("Info").gameObject
        local skillicon = instance.transform:Find("skillicon").gameObject
        skillicon:SetActive(false)
        Info:SetActive(false)
    end
end

function  LuaGuoQingPvPStartWnd:ShowInfo()
    self:ClearInfo()
    for i = 1,#LuaGuoQingPvPMgr.DefendTeamSkillInfoData do
        local data = LuaGuoQingPvPMgr.DefendTeamSkillInfoData[i]

        local playerid = data[1]
        local name =data[2]
        local class = data[3]
        local gender = data[4]
        local playerheadicon = CUICommonDef.GetPortraitName(class, gender)

        local objname = "Player"..tostring(i)
        local instance = self.Left_Player.transform:Find(objname).gameObject
        local Info = instance.transform:Find("Info").gameObject
        
        instance:SetActive(true)
        Info:SetActive(true)

        local SkillGird = instance.transform:Find("Skill").gameObject
        local SpecialSkillGird = instance.transform:Find("Juezhao_Skill").gameObject
        local skillicon = instance.transform:Find("skillicon").gameObject

        skillicon:SetActive(false)

        for j = 5,1,-1 do
            local skillid = nil
            if data[j+5] ~= 0 then
                skillid = data[j+5]
            end
            local go=NGUITools.AddChild(SkillGird,skillicon)
            local mask = go.transform:Find("Mask").gameObject
            local icon = mask.transform:Find("Icon"):GetComponent(typeof(CUITexture))
            if skillid == nil then
                icon:Clear()
            else
                local skilldata = Skill_AllSkills.GetData(skillid)
                if skilldata ~= nil then
                    icon:LoadSkillIcon(skilldata.SkillIcon)
                else
                    icon:Clear()
                end     
            end
            
            go:SetActive(true)
        end 

        SkillGird:GetComponent(typeof(UIGrid)):Reposition()

        for j = 7,6,-1 do
            local skillid = nil
            if data[j+5] ~= 0 then
                skillid = data[j+5]
            end
            local go=NGUITools.AddChild(SpecialSkillGird,skillicon)
            local mask = go.transform:Find("Mask").gameObject
            local icon = mask.transform:Find("Icon"):GetComponent(typeof(CUITexture))
            
            if skillid == nil then
                icon:Clear()
            else
                local skilldata = Skill_AllSkills.GetData(skillid)
                if skilldata ~= nil then
                    icon:LoadSkillIcon(skilldata.SkillIcon)
                else
                    icon:Clear() --:Clear()
                end  
            end
            go:SetActive(true)
        end 
        
        SpecialSkillGird:GetComponent(typeof(UIGrid)):Reposition()

        local headicon = FindChild(instance.transform:Find("Info").transform,"Icon").gameObject:GetComponent(typeof(CUITexture))
        headicon:LoadNPCPortrait(playerheadicon)
        local nameobj = FindChild(instance.transform,"name").gameObject:GetComponent(typeof(UILabel))
        nameobj.text = name
    end

    for i = 1,#LuaGuoQingPvPMgr.AttackTeamSkillInfoData do
        local data = LuaGuoQingPvPMgr.AttackTeamSkillInfoData[i]

        local playerid = data[1]
        local name =data[2]
        local class = data[3]
        local gender = data[4]
        local playerheadicon = CUICommonDef.GetPortraitName(class, gender)

        local objname = "Player"..tostring(i)
        local instance = self.Right_Player.transform:Find(objname).gameObject
        local Info = instance.transform:Find("Info").gameObject
        
        instance:SetActive(true)
        Info:SetActive(true)

        local SkillGird = instance.transform:Find("Skill").gameObject
        local SpecialSkillGird = instance.transform:Find("Juezhao_Skill").gameObject
        local skillicon = instance.transform:Find("skillicon").gameObject

        skillicon:SetActive(false)

        for j = 1,5 do
            local skillid = nil
            if data[j+5] ~= 0 then
                skillid = data[j+5]
            end
            local go=NGUITools.AddChild(SkillGird,skillicon)
            local mask = go.transform:Find("Mask").gameObject
            local icon = mask.transform:Find("Icon"):GetComponent(typeof(CUITexture))
            
            if skillid == nil then
                icon:Clear()
            else
                local skilldata = Skill_AllSkills.GetData(skillid)
                if skilldata ~= nil then
                    icon:LoadSkillIcon(skilldata.SkillIcon)
                else
                    icon:Clear()
                end  
            end
            go:SetActive(true)
        end 
        SkillGird:GetComponent(typeof(UIGrid)):Reposition()
        for j = 6,7 do
            local skillid = nil
            if data[j+5] ~= 0 then
                skillid = data[j+5]
            end
            local go=NGUITools.AddChild(SpecialSkillGird,skillicon)
            local mask = go.transform:Find("Mask").gameObject
            local icon = mask.transform:Find("Icon"):GetComponent(typeof(CUITexture))
            
            if skillid == nil then
                icon:Clear()
            else
                local skilldata = Skill_AllSkills.GetData(skillid)
                if skilldata ~= nil then
                    icon:LoadSkillIcon(skilldata.SkillIcon)
                else
                    icon:Clear()
                end  
            end
            go:SetActive(true)
        end 
        
        SpecialSkillGird:GetComponent(typeof(UIGrid)):Reposition()

        local headicon = FindChild(instance.transform:Find("Info").transform,"Icon").gameObject:GetComponent(typeof(CUITexture))
        headicon:LoadNPCPortrait(playerheadicon)
        local nameobj = FindChild(instance.transform,"name").gameObject:GetComponent(typeof(UILabel))
        nameobj.text = name
    end


end
--@endregion UIEvent

