local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CWipeTexture = import "L10.UI.CWipeTexture"
local UIPanel = import "UIPanel"
local Input = import "UnityEngine.Input"
local UICamera = import "UICamera"
local Extensions = import "Extensions"
local CSharpResourceLoader = import "L10.Game.CSharpResourceLoader"
local CDepthOfFieldEffect = import "L10.Engine.CDepthOfFieldEffect"
local CPostEffectMgr = import "L10.Engine.CPostEffectMgr"
local CPostProcessingMgr = import "L10.Engine.PostProcessing.CPostProcessingMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CPos = import "L10.Engine.CPos"

LuaWipeTearsWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaWipeTearsWnd, "BlankTexture", "BlankTexture", CWipeTexture)
RegistChildComponent(LuaWipeTearsWnd, "GuideAni", "GuideAni", GameObject)
RegistChildComponent(LuaWipeTearsWnd, "LiuLeiFx", "LiuLeiFx", CUIFx)
RegistChildComponent(LuaWipeTearsWnd, "XingDianFx", "XingDianFx", CUIFx)
--@endregion RegistChildComponent end
RegistClassMember(LuaWipeTearsWnd,"m_ShowGuideTick")
RegistClassMember(LuaWipeTearsWnd,"m_Effect")
RegistClassMember(LuaWipeTearsWnd,"m_NextLiuLeiVal")
RegistClassMember(LuaWipeTearsWnd,"m_Tex")
RegistClassMember(LuaWipeTearsWnd,"m_LastMousePos")
RegistClassMember(LuaWipeTearsWnd,"m_Percentage")
RegistClassMember(LuaWipeTearsWnd,"m_WipeTimes")
function LuaWipeTearsWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.gameObject:GetComponent(typeof(UIPanel)).IgnoreIphoneXMargin = true
end

function LuaWipeTearsWnd:Init()
    self.m_NextLiuLeiVal = 0.1
    self.m_Percentage = 0.52
    self.m_WipeTimes = 1
    if CClientMainPlayer.Inst and CommonDefs.DictContains(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(uint), CLuaTaskMgr.m_WipeScreenTearsWnd_TaskId) then
        local task = CClientMainPlayer.Inst.TaskProp.CurrentTasks[CLuaTaskMgr.m_WipeScreenTearsWnd_TaskId]
        if task then
            local eventName = "WipeScreenTears"
            if CommonDefs.DictContains(task.NeedEnvets, typeof(String), eventName) then
    			local info = task.NeedEnvets[eventName]
                local list = g_LuaUtil:StrSplit(info.ExtraData.StringData,",")
                if #list == 2 then
                    self.m_Percentage = tonumber(list[1]) / 100
                    self.m_WipeTimes = tonumber(list[2])
                end
    		end
        end
    end
    self.m_Tex = self.BlankTexture.m_UITex.mainTexture
    self.GuideAni.gameObject:SetActive(false)

    if CPostProcessingMgr.s_NewPostProcessingOpen then
        self.m_tearEyeFx = CEffectMgr.Inst:AddWorldPositionFX(88804420, CPos(0,0), 0, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
    else
        CSharpResourceLoader.Inst:LoadGameObject("Fx/posteffect/Prefab/PhotoFilter/tearEye.prefab", DelegateFactory.Action_GameObject(function(obj)
            obj.gameObject:SetActive(true)
            self.m_Effect = obj:GetComponent(typeof(CDepthOfFieldEffect))
            self.m_Effect:SetBlurMaskTexture(self.BlankTexture.m_Tex)
            CPostEffectMgr.Instance:loadEffectCallBack(self.m_Effect, true)
        end))
    end
    
    self:InitWipeTexture()
    self:CancelShowGuideTick()
    self.m_ShowGuideTick = RegisterTick(function()
        self:ShowGuide()
    end,3000)
end

function LuaWipeTearsWnd:OnEnable()
    CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, nil, true, false, false)
	CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, false, false, false)
    CUIManager.instance:CloseAllPopupViews()
end

function LuaWipeTearsWnd:OnDisable()
    CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, false, false)
	CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, nil, true, false)
    self:CancelShowGuideTick()
    self:DisableEffect()
    LuaTweenUtils.DOKill(self.transform, false)
end

function LuaWipeTearsWnd:Update()
    if not self.BlankTexture.enabled then return end
    if Input.GetMouseButtonDown(0) then
        self.GuideAni.gameObject:SetActive(false)
        self:CancelShowGuideTick()
    elseif Input.GetMouseButton(0) then
        if self.m_LastMousePos then
            local stepX = 40 * (Input.mousePosition.x > self.m_LastMousePos.x and 1 or -1)
            local stepY = 40 * (Input.mousePosition.y > self.m_LastMousePos.y and 1 or -1)
            local dY = Input.mousePosition.y - self.m_LastMousePos.y
            local dX = Input.mousePosition.x - self.m_LastMousePos.x
            if math.abs(dX) > 0 and math.abs(dX) >= math.abs(dY) then
                for x = self.m_LastMousePos.x,Input.mousePosition.x, stepX do
                    local y = math.lerp(self.m_LastMousePos.y,Input.mousePosition.y, (x - self.m_LastMousePos.x)/dX)
                    self.BlankTexture:CheckPoint(Vector3(x,y,0))    
                end
                self.BlankTexture.m_Tex:SetPixels(0, 0, self.BlankTexture.m_Width, self.BlankTexture.m_Height, self.BlankTexture.pixels)
                self.BlankTexture.m_Tex:Apply()
            elseif math.abs(dY) > 0 and math.abs(dX) <= math.abs(dY) then
                for y = self.m_LastMousePos.y,Input.mousePosition.y, stepY do
                    local x = math.lerp(self.m_LastMousePos.x,Input.mousePosition.x, (y - self.m_LastMousePos.y)/dY)
                    self.BlankTexture:CheckPoint(Vector3(x,y,0))    
                end
                self.BlankTexture.m_Tex:SetPixels(0, 0, self.BlankTexture.m_Width, self.BlankTexture.m_Height, self.BlankTexture.pixels)
                self.BlankTexture.m_Tex:Apply()
            end
        end
        self.m_LastMousePos = Input.mousePosition
        if self.m_NextLiuLeiVal and self.m_NextLiuLeiVal < 1 then
            local val = self.BlankTexture.m_WipedPixelCount / self.BlankTexture.m_TotalPixelCount
            if val >= self.m_NextLiuLeiVal then
                local worldPos = UICamera.currentCamera:ScreenToWorldPoint(Input.mousePosition)
                worldPos.z = 0
                local obj = NGUITools.AddChild(self.gameObject,self.LiuLeiFx.gameObject)
                local fx = obj:GetComponent(typeof(CUIFx))
                fx.transform.position = worldPos
                fx:LoadFx("fx/ui/prefab/UI_yinghuochong_leishui.prefab") 
                self.m_NextLiuLeiVal = self.m_NextLiuLeiVal + 0.1
            end
        end
    elseif Input.GetMouseButtonUp(0) then 
        self.m_LastMousePos = nil
        local worldPos = UICamera.currentCamera:ScreenToWorldPoint(Input.mousePosition)
        worldPos.z = 0
        self.XingDianFx.transform.position = worldPos
        self.XingDianFx:DestroyFx() 
        self.XingDianFx:LoadFx("fx/ui/prefab/UI_yinghuochong_xingdian.prefab") 
    end
end
--@region UIEvent

--@endregion UIEvent
function LuaWipeTearsWnd:InitWipeTexture()
    self.BlankTexture:OnDestroy()
    self.BlankTexture.m_UITex.mainTexture = self.m_Tex
    self.BlankTexture:Init(1, 10, self.m_Percentage, false, DelegateFactory.Action(function()
        self.m_WipeTimes = self.m_WipeTimes - 1
        if self.m_WipeTimes <= 0 then
            self:DisableEffect()
            CUIManager.CloseUI(CLuaUIResources.WipeTearsWnd)
            Gac2Gas.WipeScreenTearsDone(CLuaTaskMgr.m_WipeScreenTearsWnd_TaskId)
        else
            local panel = self.gameObject:GetComponent(typeof(UIPanel))
            panel.alpha = 0
            self.m_NextLiuLeiVal = 0
            self:InitWipeTexture()
            self.BlankTexture.enabled = false
            LuaTweenUtils.DOKill(self.transform, false)
            local tweener = LuaTweenUtils.TweenFloat(0, 1, 2, function ( val )
                panel.alpha = val
                if val >=1 then
                    self.m_LastMousePos = nil
                    self.BlankTexture.enabled = true
                end
            end)
            LuaTweenUtils.SetTarget(tweener,self.transform)
        end
    end))
    if self.m_Effect then
        self.m_Effect:SetBlurMaskTexture(self.BlankTexture.m_Tex)
    end
end

function LuaWipeTearsWnd:ShowGuide()
    if self.BlankTexture.m_WipedPixelCount <= 0 then
        self.GuideAni.gameObject:SetActive(true)
        local tweener = LuaTweenUtils.TweenFloat(-100, 100, 2, function ( val )
            Extensions.SetLocalPositionX(self.GuideAni.transform, val)
            local pos = self.GuideAni.transform.position
            local screenPos = UICamera.currentCamera:WorldToScreenPoint(pos)  
            self.BlankTexture:CheckPoint(screenPos)    
            self.BlankTexture.m_Tex:SetPixels(0, 0, self.BlankTexture.m_Width, self.BlankTexture.m_Height, self.BlankTexture.pixels)
            self.BlankTexture.m_Tex:Apply()
        end,function()
            self:InitWipeTexture()
            self.GuideAni.gameObject:SetActive(false)
        end)
        LuaTweenUtils.SetTarget(tweener, self.transform)
    end
end

function LuaWipeTearsWnd:CancelShowGuideTick()
    if self.m_ShowGuideTick then
        UnRegisterTick(self.m_ShowGuideTick)
        self.m_ShowGuideTick = nil
    end
    LuaTweenUtils.DOKill(self.transform, false)
end

function LuaWipeTearsWnd:DisableEffect()
    if CPostProcessingMgr.s_NewPostProcessingOpen then
        if self.m_tearEyeFx then
            self.m_tearEyeFx:Destroy()
        end
    else
        if self.m_Effect then
            CPostEffectMgr.Instance:loadEffectCallBack(self.m_Effect, false)
        end
    end
end
