-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CDuobaoTableItem = import "L10.UI.CDuobaoTableItem"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
CDuobaoTableItem.m_UpdateData_CS2LuaHook = function (this, status, info) 
    this.m_TemplateId = info.ItemID
    this.m_YiKouJiaObj:SetActive(info.Need_FenShu == 1)
    local data = CItemMgr.Inst:GetItemTemplate(this.m_TemplateId)
    if data ~= nil then
        this.m_ItemNameLabel.text = info.ItemName
        this.m_Texture:LoadMaterial(data.Icon)
    end

    this.m_InvestMark:SetActive(info.MyInvestInCurrentGroup > 0 and not info.IsSoldOut)
    this.m_SoldoutMark:SetActive(info.IsSoldOut)
    if info.IsSoldOut then
        this.m_LeftCountLabel.text = System.String.Format(LocalString.GetString("余{0}组"), info.GroupCount - info.CurrentGroupIndex)
    else
        this.m_LeftCountLabel.text = System.String.Format(LocalString.GetString("余{0}组"), info.GroupCount - info.CurrentGroupIndex + 1)
    end
    this.m_ProgressBar:SetProgress(1 * info.CurrentGroupProgress / info.Need_FenShu)
    this.m_ProgressBar:SetText(System.String.Format("{0}/{1}", info.CurrentGroupProgress, info.Need_FenShu))
    this.m_ProgressBar:SetGray(info.IsSoldOut)
    this.m_CrossServerObj.gameObject:SetActive(info.IsCrossServer ~= 0)
end
CDuobaoTableItem.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_Texture.gameObject).onClick = DelegateFactory.VoidDelegate(function (go) 
        local item = CItemMgr.Inst:GetItemTemplate(this.m_TemplateId)
        CItemInfoMgr.ShowLinkItemTemplateInfo(this.m_TemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
        if this.OnClickItemTexture ~= nil then
            invoke(this.OnClickItemTexture)
        end
    end)
end
