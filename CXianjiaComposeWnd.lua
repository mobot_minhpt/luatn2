-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CXianjiaComposeWnd = import "L10.UI.CXianjiaComposeWnd"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumQualityType = import "L10.Game.EnumQualityType"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUIText = import "NGUIText"
local QualityColor = import "L10.Game.QualityColor"
local Talisman_MakeXianJia = import "L10.Game.Talisman_MakeXianJia"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CXianjiaComposeWnd.m_Init_CS2LuaHook = function (this) 
    Talisman_MakeXianJia.ForeachKey(DelegateFactory.Action_object(function (key) 
        this.itemTemplateId = key
    end))
    this.itemIcon.material = nil
    this.itemAmountLabel.text = ""
    this.itemNameLabel.text = ""
    this.talismanIcon.material = nil
    this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    this.selectSprite.enabled = false
    this.talismanName.text = ""
    this.moneyCtrl:SetCost(0)
    this.acquireGo:SetActive(false)
    local costItem = Item_Item.GetData(this.itemTemplateId)
    local makeXianjia = Talisman_MakeXianJia.GetData(this.itemTemplateId)
    if costItem ~= nil then
        this.itemIcon:LoadMaterial(costItem.Icon)
        local bindCount, notbindCount
        bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(this.itemTemplateId)
        this.countAvailable = bindCount + notbindCount >= makeXianjia.NeedCount
        if this.countAvailable then
            this.itemAmountLabel.text = System.String.Format("{0}/{1}", bindCount + notbindCount, makeXianjia.NeedCount)
        else
            this.itemAmountLabel.text = System.String.Format("[c][FF0000]{0}[-]/{1}", bindCount + notbindCount, makeXianjia.NeedCount)
        end
        this.acquireGo:SetActive(not this.countAvailable)
        this.itemNameLabel.text = costItem.Name
        this.itemNameLabel.color = GameSetting_Common_Wapper.Inst:GetColor(costItem.NameColor)
        this.moneyCtrl:SetCost(makeXianjia.Cost)
    end
end
CXianjiaComposeWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.composeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.composeButton).onClick, MakeDelegateFromCSFunction(this.OnComposeButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.talismanGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.talismanGo).onClick, MakeDelegateFromCSFunction(this.OnTalismanClick, VoidDelegate, this), true)
    UIEventListener.Get(this.itemGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.itemGo).onClick, MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this), true)
    UIEventListener.Get(this.acquireGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.acquireGo).onClick, MakeDelegateFromCSFunction(this.OnAcquireClick, VoidDelegate, this), true)

    EventManager.AddListenerInternal(EnumEventType.OnXianjiaSelect, MakeDelegateFromCSFunction(this.OnXianjiaSelect, MakeGenericClass(Action1, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.ComposeXianjiaSuccess, MakeDelegateFromCSFunction(this.ComposeSuccess, MakeGenericClass(Action2, UInt32, UInt32), this))
end
CXianjiaComposeWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.composeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.composeButton).onClick, MakeDelegateFromCSFunction(this.OnComposeButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.talismanGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.talismanGo).onClick, MakeDelegateFromCSFunction(this.OnTalismanClick, VoidDelegate, this), false)
    UIEventListener.Get(this.itemGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.itemGo).onClick, MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this), false)
    UIEventListener.Get(this.acquireGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.acquireGo).onClick, MakeDelegateFromCSFunction(this.OnAcquireClick, VoidDelegate, this), false)

    EventManager.RemoveListenerInternal(EnumEventType.OnXianjiaSelect, MakeDelegateFromCSFunction(this.OnXianjiaSelect, MakeGenericClass(Action1, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.ComposeXianjiaSuccess, MakeDelegateFromCSFunction(this.ComposeSuccess, MakeGenericClass(Action2, UInt32, UInt32), this))
end
CXianjiaComposeWnd.m_ComposeSuccess_CS2LuaHook = function (this, itemTemplateId, talismanTemplateId) 

    this.itemTemplateId = itemTemplateId
    this:Init()
    this:OnXianjiaSelect(talismanTemplateId)
end
CXianjiaComposeWnd.m_OnXianjiaSelect_CS2LuaHook = function (this, templateId) 

    local talisman = EquipmentTemplate_Equip.GetData(templateId)
    if talisman ~= nil then
        this.selectTalisman = templateId
        this.talismanIcon:LoadMaterial(talisman.Icon)
        this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), talisman.Color))
        this.selectSprite.enabled = false
        this.talismanName.text = talisman.Name
        this.talismanName.color = QualityColor.GetRGBValue(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), talisman.Color))
    end
end
CXianjiaComposeWnd.m_OnComposeButtonClick_CS2LuaHook = function (this, go) 

    if this.itemTemplateId > 0 and this.selectTalisman > 0 and this.countAvailable and CClientMainPlayer.Inst.Silver >= Talisman_MakeXianJia.GetData(this.itemTemplateId).Cost then
        MessageWndManager.ShowOKCancelMessage(System.String.Format(LocalString.GetString("确定要合成[c][{1}]{0}[-]吗？"), EquipmentTemplate_Equip.GetData(this.selectTalisman).Name, NGUIText.EncodeColor24(QualityColor.GetRGBValue(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), EquipmentTemplate_Equip.GetData(this.selectTalisman).Color)))), DelegateFactory.Action(function () 
            Gac2Gas.RequestMakeXianJiaTalisman(this.itemTemplateId, this.selectTalisman)
        end), nil, nil, nil, false)
    elseif not this.countAvailable then
        g_MessageMgr:ShowMessage("Talisman_Composite_No_Item")
    elseif this.selectTalisman == 0 then
        g_MessageMgr:ShowMessage("Talisman_Composite_Choose_Target")
    elseif CClientMainPlayer.Inst.Silver < Talisman_MakeXianJia.GetData(this.itemTemplateId).Cost then
        g_MessageMgr:ShowMessage("SILVER_NOT_ENOUGH")
    end
end
