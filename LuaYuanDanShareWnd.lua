local LuaUtils = import "LuaUtils"
local DelegateFactory = import "DelegateFactory"
local UIEventListener = import "UIEventListener"
local CUIFx = import "L10.UI.CUIFx"
local UICommonDef = import "L10.UI.CUICommonDef"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"

LuaYuanDanShareWnd = class()
RegistChildComponent(LuaYuanDanShareWnd,"CloseButton", GameObject)
RegistChildComponent(LuaYuanDanShareWnd,"Btn", GameObject)
RegistChildComponent(LuaYuanDanShareWnd,"fxNode", CUIFx)

RegistClassMember(LuaYuanDanShareWnd, "m_HideExcept")
--RegistClassMember(LuaYuanDanShareWnd,"m_EvaluateInterval")
--RegistClassMember(LuaYuanDanShareWnd,"m_LastEvaluateTime")

function LuaYuanDanShareWnd:Close()
  CUIManager.CloseUI(CLuaUIResources.YuanDanShareWnd)
end

function LuaYuanDanShareWnd:Init()

  UIEventListener.Get(self.CloseButton).onClick = LuaUtils.VoidDelegate(function ( ... )
    self:Close()
  end)

  local hideExcept = { "HeadInfoWnd", "MiddleNoticeCenter", "PopupNoticeWnd", "Joystick" }
  self.m_HideExcept = Table2List(hideExcept, MakeGenericClass(List, cs_string))
  CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, self.m_HideExcept, true, false, false)
  CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, false, false, false)

  UIEventListener.Get(self.Btn).onClick = LuaUtils.VoidDelegate(function ( ... )
  --   UICommonDef.CaptureScreen(
  --     "screenshot",
  --     true,
  --     false,
  --     nil,
  --     DelegateFactory.Action_string_bytes(
  --     function(fullPath, jpgBytes)
  --       ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
  --     end
  --   ),
  --   false
  -- )
  UICommonDef.CaptureScreenUIAndShare(CLuaUIResources.YuanDanShareWnd,nil)
  end)
  self.Btn.gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
  --self.fxNode:LoadFx("fx/ui/prefab/UI_xinnianyanhua_2021.prefab")
  self.fxNode:LoadFx(YuanDan_Setting.GetData().HappyNewYearShareWndFx)
end

function LuaYuanDanShareWnd:OnDestroy()
  CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, self.m_HideExcept, true, false)
  CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, false, false)
end

function LuaYuanDanShareWnd:OnEnable( ... )
	--g_ScriptEvent:AddListener("SyncNewYearPrizeDrawIndex", self, "SyncDrawIndex")
end

function LuaYuanDanShareWnd:OnDisable( ... )
	--g_ScriptEvent:RemoveListener("SyncNewYearPrizeDrawIndex", self, "SyncDrawIndex")
end


return LuaYuanDanShareWnd
