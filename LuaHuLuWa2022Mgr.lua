local CScene = import "L10.Game.CScene"
local EnumRecordContext = import "L10.Game.EnumRecordContext"
local EChatPanel = import "L10.Game.EChatPanel"
local CRecordingInfoMgr = import "L10.UI.CRecordingInfoMgr"
local EnumVoicePlatform = import "L10.Game.EnumVoicePlatform"
local Vector2 = import "UnityEngine.Vector2"
local FXClientInfoReporter = import "L10.Game.FXClientInfoReporter"
local CSkillAcquisitionMgr = import "L10.UI.CSkillAcquisitionMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local CRecordingWnd = import "L10.UI.CRecordingWnd"
local CUIActionMgr = import "L10.UI.CUIActionMgr"

LuaHuLuWa2022Mgr = {}

LuaHuLuWa2022Mgr.s_HuLuWaSwitch = false
--#region 三界历险
LuaHuLuWa2022Mgr.m_VisitedPosDict = {}
function LuaHuLuWa2022Mgr.OpenTravelWorldWnd()
    Gac2Gas.QueryHuluwaAdventureWndInfo()
end

function LuaHuLuWa2022Mgr.OpenTravelEventTip(targetId)
    LuaHuLuWa2022Mgr.m_TravelTipId = targetId
    CUIManager.ShowUI(CLuaUIResources.HuLuWaTravelEventPopWnd)
end
--#endregion

--#region 勇闯如意洞
LuaHuLuWa2022Mgr.m_IsRuYiServerSwitchOn = false--是否试投放
LuaHuLuWa2022Mgr.m_IsHuLuWaServerSwitchOn = false

LuaHuLuWa2022Mgr.m_RuYiDongWndTabIndex = 0 

function LuaHuLuWa2022Mgr.OnLeaveRuYiDong()
    LuaHuLuWa2022Mgr.BossPosInfo = nil 
    CLuaMiniMap:ClearHuluwaBoss()
end

function LuaHuLuWa2022Mgr.IsInRuYiDongPlay()
    if not CScene.MainScene then 
        return false 
    end

    local gamePlayId = CScene.MainScene.GamePlayDesignId
    local setting = HuluBrothers_Setting.GetData()
    
    local isIn = false
    if gamePlayId == setting.NormalPlayId or gamePlayId == setting.HardPlayId or gamePlayId == setting.HellPlayId then
        isIn = true
    end
    return isIn
end

function LuaHuLuWa2022Mgr:RuYiDongTopAndRightInit(wnd)   
    LuaTopAndRightMenuWndMgr:ShowTopRightWnd(wnd, CLuaUIResources.RuYiDongTopRightWnd,function ()
        local isShowWnd = LuaHuLuWa2022Mgr.IsInRuYiDongPlay()
        return isShowWnd
    end)
end

function LuaHuLuWa2022Mgr.OpenRuYiDongSignWnd()
    LuaHuLuWa2022Mgr.m_RuYiDongWndTabIndex = 0 
    CUIManager.ShowUI(CLuaUIResources.RuYiDongSignWnd)
end

function LuaHuLuWa2022Mgr.OpenRuYiDongRankWnd(gamePlayId)
    LuaHuLuWa2022Mgr.m_OpenRankFromGamePlayId = gamePlayId
    LuaHuLuWa2022Mgr.m_RuYiDongWndTabIndex = 1
    CUIManager.ShowUI(CLuaUIResources.RuYiDongSignWnd)
end

function LuaHuLuWa2022Mgr.OpenRuYiDongResultWnd(isRuYiDong,selfUseTime, playerId, playerName, bestUseTime)
    LuaHuLuWa2022Mgr.m_IsRuYiDongResult = isRuYiDong
    LuaHuLuWa2022Mgr.m_SelfUseTime = tonumber(selfUseTime)
    LuaHuLuWa2022Mgr.m_RecordPlayerName = playerName
    LuaHuLuWa2022Mgr.m_RecordUseTime = tonumber(bestUseTime)
    CUIManager.ShowUI(CLuaUIResources.RuYiDongResultWnd)
end

LuaHuLuWa2022Mgr.EnumPreviewType = 
{
    eRuYiDong = 1,
    eQiaoDuoRuYi = 2,
    eTravel = 3,
    ePreview = 4,--只查看  不切换
    eOther = 10
}
LuaHuLuWa2022Mgr.m_CurPreviewType = nil
LuaHuLuWa2022Mgr.m_HuLuWaChooseData = {} --key:idx data:{idx,playerId,playerName}
function LuaHuLuWa2022Mgr.OpenHuLuWaChoosePreviewWnd(type)--葫芦娃变身选择界面
    LuaHuLuWa2022Mgr.m_CurPreviewType = type
    CUIManager.ShowUI(CLuaUIResources.HuLuWaBianShenPreviewWnd)
end

function LuaHuLuWa2022Mgr.ShowHuLuWaSkillTip()
    local skills = {}--to list
    local tempSkillArray = CClientMainPlayer.Inst.SkillProp.ActiveTempSkill

    local list = CreateFromClass(MakeGenericClass(List, UInt32))
    local replaceSkill = HuluBrothers_Setting.GetData().ReplaceSkill
    for i = 1, tempSkillArray.Length-1 do
        if tempSkillArray[i] > 0 and tempSkillArray[i] ~= replaceSkill  then
            CommonDefs.ListAdd(list, typeof(UInt32), tempSkillArray[i])
        end
    end
    if list.Count == 0 then
        g_MessageMgr:ShowMessage("HuLuWa_GamePlay_SkillTip_NoTransform")
        return
    end
    CSkillAcquisitionMgr.ShowTempSkillsTip(list)
end
--#endregion


--#region 巧夺如意
LuaHuLuWa2022Mgr.m_QiaoDuoRuYiUnLockDic = {}
function LuaHuLuWa2022Mgr.IsInQiaoDuoRuYiPlay()
    if not CScene.MainScene then 
        return false 
    end

    local gamePlayId = CScene.MainScene.GamePlayDesignId
    local setting = HuluBrothers_Setting.GetData()
    
    local isIn = false

    if gamePlayId == setting.RuyiPlayId then
        isIn = true
    end
    return isIn
end

function LuaHuLuWa2022Mgr.OpenQiaoDuoRuYiSignWnd(bHideBtn)
    LuaHuLuWa2022Mgr.m_QiaoDuoRuYiUnLockDic = {}
    LuaHuLuWa2022Mgr.m_IsHideBtn = bHideBtn
    CUIManager.ShowUI(CLuaUIResources.QiaoDuoRuYiSignWnd)
end

function LuaHuLuWa2022Mgr.OpenQiaoDuoRuYiRankWnd()
    CUIManager.ShowUI(CLuaUIResources.QiaoDuoRuYiRankWnd)
end

function LuaHuLuWa2022Mgr.OpenUnLockWnd(idx,unLock)
    LuaHuLuWa2022Mgr.m_CurHuLuWaIdx = idx
    LuaHuLuWa2022Mgr.m_CurHuLuWaUnLock = unLock
    CUIManager.ShowUI(CLuaUIResources.HuLuWaUnLockWnd)
end

function LuaHuLuWa2022Mgr.ShowPasswordWnd()
    LuaHuLuWa2022Mgr.m_PasswordCountDownTime = 5
    LuaHuLuWa2022Mgr.m_Password = ""
    CUIManager.ShowUI(CLuaUIResources.RuYiVoicePasswordWnd)
end

--LuaHuLuWa2022Mgr.m_PasswordCountDownTick = nil
LuaHuLuWa2022Mgr.m_VoiceInputTime = 0
LuaHuLuWa2022Mgr.m_PreRecordingText = nil
function LuaHuLuWa2022Mgr.StartVoicePassword(go)
    LuaHuLuWa2022Mgr.m_PreRecordingText = CRecordingWnd.recordingText
    CRecordingWnd.recordingText = LocalString.GetString("喊出口令，释放技能")
    CRecordingInfoMgr.ShowRecordingWnd(EnumRecordContext.RuYi, EChatPanel.Undefined, go, EnumVoicePlatform.Default, false)
end

function LuaHuLuWa2022Mgr.CheckVoicePassword(voiceText)
    local setting = HuluBrothers_Setting.GetData()
    local replaceSkill = setting.ReplaceSkill
    local password = setting.RuYiPassWord
    password = StringTrim(password)

    local answer = StringTrim(voiceText)
    if not answer then answer = "" end
    g_MessageMgr:ShowMessage("CUSTOM_STRING2", answer)
    local _, result = string.find(answer, LocalString.GetString("如意如意"))
    if result then
        _, result = string.find(answer, LocalString.GetString("按我心意"))
        if result then
            _, result = string.find(answer, LocalString.GetString("快快显灵"))
        end
    end
    if not result then
        g_MessageMgr:ShowMessage("RuYi_Password_Wrong")
    else
        --释放技能
        g_MessageMgr:ShowMessage("RuYi_Password_Correct")
        LuaHuLuWa2022Mgr.m_VoiceInputTime = 0
        local mainplayer = CClientMainPlayer.Inst
        if mainplayer == nil or mainplayer.IsDie then
            return
        end
        mainplayer:TryCastSkill(replaceSkill, true, Vector2.zero)
        FXClientInfoReporter.Inst:ReportCastTempSkill(replaceSkill, "buttonclick")
    end
end

function LuaHuLuWa2022Mgr.InitRuYiAdditionalSkill()
    if CUIManager.IsLoaded(CLuaUIResources.RuYiAdditionSkillWnd) then
		g_ScriptEvent:BroadcastInLua("InitRuYiAdditionalSkill")
	else
		CUIManager.ShowUI(CLuaUIResources.RuYiAdditionSkillWnd)
	end
end

--#endregion

--#region 炼丹炉战令
LuaHuLuWa2022Mgr.m_ZhanLingData = {}
function LuaHuLuWa2022Mgr.OpenLianDanLuWnd()
    Gac2Gas.QueryLianDanLuPlayData()
end

function LuaHuLuWa2022Mgr.OpenUnLockZhanLingWnd()
    CUIManager.ShowUI(CLuaUIResources.HuLuWaUnLockDanHuWnd)
end
function LuaHuLuWa2022Mgr.OpenZhanLingShouCeWnd()
    --CUIManager.ShowUI(CLuaUIResources.HuLuWaLianDanShouCeWnd)
    Gac2Gas.RequestOpenLianDanLuTaskWnd()
end

LuaHuLuWa2022Mgr.m_Level2NeedProgress = nil
function LuaHuLuWa2022Mgr.SyncLianDanLuPlayData(p1, p2,p3, vip1, vip2, bAlert, receiveDataUD)
    LuaHuLuWa2022Mgr.m_ZhanLingData = {}
    ZhanLing_Reward.Foreach(function(level,data)
        local t = {}
        t.level = level
        t.flag = 0
        table.insert(LuaHuLuWa2022Mgr.m_ZhanLingData,t)
    end)

	local list = MsgPackImpl.unpack(receiveDataUD)
	if list and list.Count > 0 then
		for i = 0, list.Count - 1, 2 do
			local level = tonumber(list[i])
            local  flag = tonumber(list[i+1])
			local t = LuaHuLuWa2022Mgr.m_ZhanLingData[level]
			t.level = level
			t.flag = flag
		end		
	end
	LuaHuLuWa2022Mgr.m_ZhanLingP1 = p1
	LuaHuLuWa2022Mgr.m_ZhanLingP2 = p2
    LuaHuLuWa2022Mgr.m_ZhanLingP3 = p3
	LuaHuLuWa2022Mgr.m_IsVip1 = vip1 == 1
	LuaHuLuWa2022Mgr.m_IsVip2 = vip2 == 1
	LuaHuLuWa2022Mgr.m_IsFirstOpenZhanLing = bAlert

	
    if CUIManager.IsLoaded(CLuaUIResources.HuLuWaZhanLingWnd) then
		g_ScriptEvent:BroadcastInLua("SyncLianDanLuPlayData")
	else
		CUIManager.ShowUI(CLuaUIResources.HuLuWaZhanLingWnd)
	end
end

LuaHuLuWa2022Mgr.m_CurZhanLingLevel = 1
function LuaHuLuWa2022Mgr.GetCurZhanLingLevel()
    local level = 1
    local setting = ZhanLing_Setting.GetData()
    local vip2AddRate = setting.Vip2AddRate
    local vip2AddValue = setting.Vip2AddValue
    local vip1AddValue = setting.Vip1AddValue

    local p1 = LuaHuLuWa2022Mgr.m_ZhanLingP1
    local p2 = LuaHuLuWa2022Mgr.m_ZhanLingP2
    local p3 = LuaHuLuWa2022Mgr.m_ZhanLingP3
    -- vip2加成
    if LuaHuLuWa2022Mgr.m_IsVip2 then
		p2 = math.floor(p2 * vip2AddRate)
		p1 = p1 + vip2AddValue
	end

    -- vip1加成
    if LuaHuLuWa2022Mgr.m_IsVip1 then
		p1 = p1 + vip1AddValue
	end
    
    level,_ = LuaHuLuWa2022Mgr.GetLevelByTotalProgress(p1+p2+p3)

    return level,p1+p2+p3
end
function LuaHuLuWa2022Mgr.GetLevelByTotalProgress(progress)
    local maxLevel = 0
	local maxLevelProgress = 0
    if not LuaHuLuWa2022Mgr.m_Level2NeedProgress or not next(LuaHuLuWa2022Mgr.m_Level2NeedProgress) then
        LuaHuLuWa2022Mgr.m_Level2NeedProgress = {}
        ZhanLing_Reward.Foreach(function(level,data)
			LuaHuLuWa2022Mgr.m_Level2NeedProgress[level] = data.NeedProgress
		end)
    end

	for level, needProgress in pairs(LuaHuLuWa2022Mgr.m_Level2NeedProgress) do
		if progress >= needProgress and level > maxLevel then
			maxLevel = level
			maxLevelProgress = needProgress
		end
	end

	return maxLevel, maxLevelProgress
end

LuaHuLuWa2022Mgr.m_DailyTask = {}
LuaHuLuWa2022Mgr.m_WeekTask = {}
function LuaHuLuWa2022Mgr.SendLianDanLuTaskData(taskDataUD)
    LuaHuLuWa2022Mgr.m_DailyTask = {}
    LuaHuLuWa2022Mgr.m_WeekTask = {}
    local list = MsgPackImpl.unpack(taskDataUD)
	if  list then 
        for i = 0, list.Count - 1, 2 do
            local t = {}
            t.taskId = list[i]
            t.finishCount = list[i+1]
            t.progress = 0
            local task = ZhanLing_Task.GetData(t.taskId)
            if task then
                t.finishCount = math.min(task.Target,t.finishCount)
                t.progress = t.finishCount / task.Target
                if task.IsDailyTask == 1 then
                    table.insert(LuaHuLuWa2022Mgr.m_DailyTask,t)
                else
                    table.insert(LuaHuLuWa2022Mgr.m_WeekTask,t)
                end
            end            
        end
    end
	CUIManager.ShowUI(CLuaUIResources.HuLuWaLianDanShouCeWnd)
end

function LuaHuLuWa2022Mgr.OpenWnd(zhanlingId)
    local task = ZhanLing_Task.GetData(zhanlingId)
    local actionString = task.Action
    local actionName,parameters = string.match(actionString,"(%w+)%(([^%s]-)%)")

    if parameters == nil then
        local now = CServerTimeMgr.Inst:GetZone8Time()
        local beginDate =  CreateFromClass(DateTime, 2022, 5, 19, 18, 0, 0)
        local endDate =  CreateFromClass(DateTime, 2022, 5, 19, 19, 30, 0)
        if DateTime.Compare(now, beginDate) >= 0 and DateTime.Compare(now, endDate) <= 0 then
            --开放中
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("点击主界面屏幕“播”字按钮，观看周年庆直播。"))            
        elseif DateTime.Compare(now, beginDate) < 0 then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("18:00-19:30点击主界面屏幕“播”字按钮，观看周年庆直播。"))
        else
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("很遗憾，周年庆直播已结束。"))
        end
        return
    end

    local options = g_LuaUtil:StrSplitAdv(parameters,",")
    if actionName == "OpenWnd" then
        local wndName = options[1]
        wndName = string.match(wndName,"(%w+)")
        local methodName = options[2]
        CUIActionMgr.Inst:Show(wndName, methodName,nil,nil,nil,nil)
    end
end
--#endregion

function LuaHuLuWa2022Mgr.GetActivityState(str)
    local sweek, eweek,shour,smin,ehour,emin = string.match(str, "(%d+)-(%d+),(%d+):(%d+)-(%d+):(%d+)")
    sweek = tonumber(sweek)
    eweek = tonumber(eweek)
    shour = tonumber(shour)
    ehour = tonumber(ehour)
    smin = tonumber(smin)
    emin = tonumber(emin)
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local dayofweek = EnumToInt(now.DayOfWeek)--0,1,2,3,4,5,6

    if dayofweek == 0 then
        dayofweek = 7
    end
    local isInWeek,isInTime = false,false
    local weekstr = SafeStringFormat3(LocalString.GetString("周%s至周%s"),sweek==7 and LocalString.GetString("日") or CUICommonDef.IntToChinese(sweek),eweek==7 and LocalString.GetString("日") or CUICommonDef.IntToChinese(eweek)) 
    
    if sweek == 0 and eweek == 0 then
        isInWeek = true
        weekstr = LocalString.GetString("每天")    
    elseif dayofweek <= eweek and dayofweek >= sweek then
        isInWeek = true
    else
        isInWeek = false
    end

    if isInWeek then
        local minDate =  CreateFromClass(DateTime, now.Year, now.Month, now.Day, shour, smin, 0)
        local esec = 0
        if ehour==23 and emin == 59 then
            esec = 59
        end
        local maxDate =  CreateFromClass(DateTime, now.Year, now.Month, now.Day, ehour, emin, esec)
        if DateTime.Compare(now, minDate) >= 0 and DateTime.Compare(now, maxDate) <= 0 then
            --开放中
            isInTime = true
        else
            isInTime = false
        end
    end
    local isActivity = (isInWeek and isInTime)
    return isActivity
end