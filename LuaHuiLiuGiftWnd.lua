local CUITexture = import "L10.UI.CUITexture"
local CButton = import "L10.UI.CButton"
local CPayMgr = import "L10.Game.CPayMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local ETickType = import "L10.Engine.ETickType"

LuaHuiLiuGiftWnd = class()

RegistChildComponent(LuaHuiLiuGiftWnd, "CountDownLabel", UILabel)
RegistChildComponent(LuaHuiLiuGiftWnd, "GiftItem1", GameObject)
RegistChildComponent(LuaHuiLiuGiftWnd, "GiftItem2", GameObject)

RegistClassMember(LuaHuiLiuGiftWnd, "GiftInfos")
RegistClassMember(LuaHuiLiuGiftWnd, "ItemList")
RegistClassMember(LuaHuiLiuGiftWnd, "CountDownTick")
RegistClassMember(LuaHuiLiuGiftWnd, "RemainTime")

function LuaHuiLiuGiftWnd:Init()
    self.CountDownLabel.text = nil
    self.RemainTime = 0

    self.ItemList = {}
    table.insert(self.ItemList, self.GiftItem1)
    table.insert(self.ItemList, self.GiftItem2)
    
    self:OnCountDownTick()
    self:UpdateGiftInfos()
end

function LuaHuiLiuGiftWnd:UpdateGiftInfos()
    self.GiftInfos = {}

    -- 限购信息与倒计时在同一个rpc
    Huiliu_Gift.Foreach(function (key, data)
        local item = Item_Item.GetData(data.Item)
        local info = {
            ID = data.ID,
            ItemId = data.Item,
            Name = data.Name,
            Description = data.Description,
            Icon = data.Icon,
            CanBuy = LuaHuiLiuMgr.GiftInfos[data.Item]
        }
        table.insert(self.GiftInfos, info)
    end)

    for i = 1, #self.GiftInfos do
        self:InitGiftItem(self.ItemList[i], self.GiftInfos[i])
    end

    self:CancelCountDownTick()

    self.CountDownTick = CTickMgr.Register(DelegateFactory.Action(function ()
        self:OnCountDownTick()
    end), 1000, ETickType.Loop)
end

function LuaHuiLiuGiftWnd:OnCountDownTick()
    self.RemainTime = math.floor(LuaHuiLiuMgr.GiftCountDownEndTime - CServerTimeMgr.Inst.timeStamp)
    if self.RemainTime > 0 then
        local h = math.floor(self.RemainTime / 3600)
        local m = math.floor(self.RemainTime % 3600 / 60)
        local s = self.RemainTime % 60
        if self.CountDownLabel ~= nil then
            self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("限购倒计时 %02d:%02d:%02d"), h, m, s)
        end
    else
        self:CancelHuiliuTick()
    end
end

function LuaHuiLiuGiftWnd:InitGiftItem(go, info)
    if not go or not info then return end
    local GiftNameLabel = go.transform:Find("GiftNameLabel"):GetComponent(typeof(UILabel))
    local DiscountLabel = go.transform:Find("DiscountLabel"):GetComponent(typeof(UILabel))
    local GiftIcon = go.transform:Find("GiftIcon"):GetComponent(typeof(CUITexture))
    GiftIcon:Clear()

    local DescLabel = go.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
    local OriginalPriceLabel = go.transform:Find("OriginalPriceLabel"):GetComponent(typeof(UILabel))
    local BuyButton = go.transform:Find("BuyButton"):GetComponent(typeof(CButton))
    local LimitLabel = go.transform:Find("LimitLabel"):GetComponent(typeof(UILabel))

    GiftNameLabel.text = info.Name
    DescLabel.text = info.Description
    GiftIcon:LoadMaterial(info.Icon)

    BuyButton.Enabled = info.CanBuy

    local mall = Mall_LingYuMallLimit.GetData(info.ItemId)
    if mall and mall.Discount ~= 0 then
        DiscountLabel.text = SafeStringFormat3(LocalString.GetString("%s折"), tostring(mall.Discount))

        if info.CanBuy then
            BuyButton.Text = SafeStringFormat3(LocalString.GetString("%s元购买"), tostring(mall.Jade))
        else
            BuyButton.Text = LocalString.GetString("已购买")
        end
        
        local orginalPrice = math.floor(mall.Jade * 10 / mall.Discount) 
        OriginalPriceLabel.text = SafeStringFormat3(LocalString.GetString("原价 %s元"), tostring(orginalPrice))

        CommonDefs.AddOnClickListener(BuyButton.gameObject, DelegateFactory.Action_GameObject(function (go)
            self:OnButBtnClicked(go, info.ItemId)
        end), false)
    end

end

function LuaHuiLiuGiftWnd:OnButBtnClicked(go, itemId)
    local data = Mall_LingYuMallLimit.GetData(itemId)
    CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(data.RmbPID), 0)
end

-- 服务器通知玩家有一笔订单成功发货
function LuaHuiLiuGiftWnd:NotifyChargeDone()
    self:UpdateGiftInfos()
end


function LuaHuiLiuGiftWnd:OnEnable()
    g_ScriptEvent:AddListener("HuiLiuGiftNotifyChargeDone", self, "NotifyChargeDone")
end

function LuaHuiLiuGiftWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HuiLiuGiftNotifyChargeDone", self, "NotifyChargeDone")
    self:CancelCountDownTick()
end

function LuaHuiLiuGiftWnd:CancelCountDownTick()
    if self.CountDownTick then
        invoke(self.CountDownTick)
        self.CountDownTick = nil
    end
end

return LuaHuiLiuGiftWnd