-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CTalismanTransferItemCell = import "L10.UI.CTalismanTransferItemCell"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumQualityType = import "L10.Game.EnumQualityType"
local IdPartition = import "L10.Game.IdPartition"
local Object = import "System.Object"
CTalismanTransferItemCell.m_Init_CS2LuaHook = function (this, itemId) 

    this.itemId = itemId
    this.iconTexture:Clear()
    this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    this.Selected = false
    local item = CItemMgr.Inst:GetById(itemId)
    if item ~= nil and item.IsEquip and IdPartition.IdIsTalisman(item.TemplateId) then
        this.iconTexture:LoadMaterial(item.Icon)
        this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item.Equip.QualityType)
    end
end
CTalismanTransferItemCell.m_OnCellLongPress_CS2LuaHook = function (this) 
    if not System.String.IsNullOrEmpty(this.itemId) then
        CItemInfoMgr.ShowLinkItemInfo(this.itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end
    if this.OnSelectedDelegate ~= nil then
        GenericDelegateInvoke(this.OnSelectedDelegate, Table2ArrayWithCount({this.gameObject}, 1, MakeArrayClass(Object)))
    end
end
