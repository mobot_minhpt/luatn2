local UILabel = import "UILabel"
local UISlider = import "UISlider"
local CButton = import "L10.UI.CButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaHalloweenHongBaoCoverWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHalloweenHongBaoCoverWnd, "Label", "Label", UILabel)
RegistChildComponent(LuaHalloweenHongBaoCoverWnd, "ProgressBar", "ProgressBar", UISlider)
RegistChildComponent(LuaHalloweenHongBaoCoverWnd, "ProgressText", "ProgressText", UILabel)
RegistChildComponent(LuaHalloweenHongBaoCoverWnd, "ReceiveBtn", "ReceiveBtn", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaHalloweenHongBaoCoverWnd, "m_TotalJade")
RegistClassMember(LuaHalloweenHongBaoCoverWnd, "m_CurJade")
RegistClassMember(LuaHalloweenHongBaoCoverWnd, "m_HasGetCover")
RegistClassMember(LuaHalloweenHongBaoCoverWnd, "m_HasClickBtn")
function LuaHalloweenHongBaoCoverWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    UIEventListener.Get(self.ReceiveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReceiveBtnClick()
	end)
end

function LuaHalloweenHongBaoCoverWnd:Init()
    self.Label.text = g_MessageMgr:FormatMessage("Halloween2022_HongBaoCover_GetCoverTip")
    self.m_TotalJade = Halloween2022_Setting.GetData().AllowToUseCoverTotalJade
    self.m_CurJade = 0
    self.m_HasGetCover = false
    self.m_HasClickBtn = false
    self:UpdateProgress()
end

function LuaHalloweenHongBaoCoverWnd:UpdateProgress()
    local MainPlayer = CClientMainPlayer.Inst
    if MainPlayer then
        self.m_CurJade = math.min(MainPlayer.PlayProp.AccumulatedHongBaoJade,self.m_TotalJade)
        self.m_HasGetCover = MainPlayer.PlayProp.IsHongBaoCoverEnabled == 1
    end
    local hasGetJade = self.m_CurJade >= self.m_TotalJade
    self.ReceiveBtn.gameObject:SetActive(hasGetJade)
    self.ProgressText.text = SafeStringFormat3("%d/%d",self.m_CurJade,self.m_TotalJade)
    self.ProgressBar.value = self.m_CurJade/self.m_TotalJade
    self.ReceiveBtn.Enabled = not self.m_HasGetCover and hasGetJade

    if self.m_HasGetCover then
        self.ReceiveBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("已领取")
    else 
        self.ReceiveBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("领取封面")
    end
-- 弹出跳转红包封面收藏界面确认窗口
    if self.m_HasGetCover and self.m_HasClickBtn then
        local message = g_MessageMgr:FormatMessage("HALLOWEEN2022_HONGBAOCOVER_CONFIRM")
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
			CUIManager.ShowUI(CLuaUIResources.HongBaoCoverWnd)
		end), nil)
    end

end

function LuaHalloweenHongBaoCoverWnd:OnReceiveBtnClick()
    if LuaHalloween2022Mgr.isInHalloweenHongBaoCoverTime() then
        self.m_HasClickBtn = true
        Gac2Gas.RequestGetHalloween2022HongBaoCover() 
    else
        g_MessageMgr:ShowCustomMsg(g_MessageMgr:FormatMessage("HALLOWEEN2022_HongBaoCover_AFTERAUTOSEND"))
    end

end

function LuaHalloweenHongBaoCoverWnd:OnEnable()
    g_ScriptEvent:AddListener("Halloween2022SyncAllowToUseHongBaoCover",self,"UpdateProgress")
end

function LuaHalloweenHongBaoCoverWnd:OnDisable()
    g_ScriptEvent:RemoveListener("Halloween2022SyncAllowToUseHongBaoCover",self,"UpdateProgress")
end
--@region UIEvent

--@endregion UIEvent

