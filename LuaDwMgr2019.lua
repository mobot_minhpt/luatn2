local CUIManager        = import "L10.UI.CUIManager"
local CScene            = import "L10.Game.CScene"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

--2019端午管理中心
CLuaDwMgr2019 = {}

--[[
    @desc: 是否在粽子玩法中
    author:CodeGize
    time:2019-04-16 16:16:26
    @return:
]]
function CLuaDwMgr2019.IsInZongziPlay()
    if not CClientMainPlayer.Inst then return false end
    local setting = DuanWu_Setting.GetData()
    return CClientMainPlayer.Inst.PlayProp.PlayId == setting.ZongZi2019GamePlayId
end

function CLuaDwMgr2019.ShowDwWusesiTabenWnd(id)
    CLuaDwWusesiTabenWnd.OriID = id
    CUIManager.ShowUI("DwWusesiTabenWnd")
end

function CLuaDwMgr2019.ShowDwWusesiHeChengWnd()
    CUIManager.ShowUI("DwWusesiHeChengWnd")
end

function CLuaDwMgr2019.ShowDwZongziRuleWnd( )
    CUIManager.ShowUI("DwZongziRuleWnd")
end
--@region 事件

--[[ 
    @desc: 进入场景事件
    author:CodeGize
    time:2019-04-17 14:13:00
    @return:
]]
function CLuaDwMgr2019:OnRenderSceneInit()
    local dwsetting = DuanWu_Setting.GetData()
    if dwsetting == nil then return end

    local gameplay = dwsetting.ZongZi2019GamePlayId
    local gpdata = Gameplay_Gameplay.GetData(gameplay)
    if gpdata == nil then return end

    local cmap = CScene.MainScene
    if cmap == nil then return end
    if gpdata.MapId == cmap.SceneTemplateId then
        CUIManager.ShowUI("DwZongziRuleTipWnd")
    end
end

g_ScriptEvent:AddListener("RenderSceneInit", CLuaDwMgr2019, "OnRenderSceneInit")

