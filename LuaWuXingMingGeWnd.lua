LuaWuXingMingGeWnd =class()
RegistChildComponent(LuaWuXingMingGeWnd ,"m_WuXingTex","WuXingTex", CUITexture)

function LuaWuXingMingGeWnd:Init()
    self.m_WuXingTex.gameObject:SetActive(false)
    if LuaZongMenMgr.m_IsOpen then
        Gac2Gas.QueryPlayerMingGe()
    end
end

function LuaWuXingMingGeWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSyncPlayerMingGe", self, "OnSyncPlayerMingGe")
end

function LuaWuXingMingGeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncPlayerMingGe", self, "OnSyncPlayerMingGe")
end

function LuaWuXingMingGeWnd:OnSyncPlayerMingGe(mingge)
    self.m_WuXingTex:LoadMaterial(LuaZongMenMgr:GetMingGeMatPath(mingge))
    self.m_WuXingTex.gameObject:SetActive(true)
end