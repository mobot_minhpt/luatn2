require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local CPayMgr=import "L10.Game.CPayMgr"
local CShopMallMgr=import "L10.UI.CShopMallMgr"
local Mall_LingYuMallLimit=import "L10.Game.Mall_LingYuMallLimit"
local Mall_ItemPreview=import "L10.Game.Mall_ItemPreview"
local LuaUtils=import "LuaUtils"
local Item_Item=import "L10.Game.Item_Item"
local CItemMgr=import "L10.Game.CItemMgr"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local Gac2Gas=import "L10.Game.Gac2Gas"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"

CLuaFashionGiftMgr={}
CLuaFashionGiftMgr.SelectedGiftId=nil

--时装礼包
CLuaFashionGiftView=class()
RegistClassMember(CLuaFashionGiftView,"m_Template")
RegistClassMember(CLuaFashionGiftView,"m_PreviewItemTemplate")
RegistClassMember(CLuaFashionGiftView,"m_Ids")

function CLuaFashionGiftView:Init()
    local ids=CShopMallMgr.Inst:GetDiscountItemIds()
    self.m_Ids={}
    local len=ids.Count
    for i=0,len-1 do
        table.insert(self.m_Ids, ids[i])
    end

    self.m_Template=LuaGameObject.GetChildNoGC(self.transform,"Template").gameObject
    self.m_Template:SetActive(false)
    self.m_PreviewItemTemplate=LuaGameObject.GetChildNoGC(self.transform,"PreviewItemTemplate").gameObject
    self.m_PreviewItemTemplate:SetActive(false)

    CShopMallMgr.Inst:ClearLimitInfo()
    self:InitItems()
    Gac2Gas.RequestPlayerItemLimit(2)
    Gac2Gas.RequestAutoShangJiaItem()
end

function CLuaFashionGiftView:InitItems()
    local function OnBuyButtonClicked(go)
        self:OnClickBuyButton(go)
	end
    local function OnPreviewItemClicked(go)
        self:OnClickPreviewItem(go)
    end

    local grid=LuaGameObject.GetChildNoGC(self.transform,"Grid").grid
    local scrollView = self.transform:Find("ScrollView"):GetComponent(typeof(CUIRestrictScrollView))
    CUICommonDef.ClearTransform(grid.transform)

    for i,v in ipairs(self.m_Ids) do
        local go=NGUITools.AddChild(grid.gameObject,self.m_Template)
        go:SetActive(true)
        local designData=Mall_LingYuMallLimit.GetData(v)

        local itemData = CItemMgr.Inst:GetItemTemplate(v)

        LuaGameObject.GetChildNoGC(go.transform,"NameLabel").label.text=itemData.Name
        LuaGameObject.GetChildNoGC(go.transform,"DiscountLabel").label.text=SafeStringFormat3(LocalString.GetString("%d折"),designData.Discount)
        LuaGameObject.GetChildNoGC(go.transform,"PriceLabel").label.text=SafeStringFormat3(LocalString.GetString("售价   %d元"),designData.Jade)
        local limitCount=CShopMallMgr.Inst:GetLimitCount(v)
        LuaGameObject.GetChildNoGC(go.transform,"LimitLabel").label.text=SafeStringFormat3(LocalString.GetString("每月限购%d次"),limitCount)
        
        local button=LuaGameObject.GetChildNoGC(go.transform,"BuyButton").gameObject
        if limitCount>0 then
            CUICommonDef.SetActive(button,true,true)
        else
            CUICommonDef.SetActive(button,false,true)
        end

        CommonDefs.AddOnClickListener(button,DelegateFactory.Action_GameObject(OnBuyButtonClicked),false)

        local items=designData.ItemPreview
        local itemParent=LuaGameObject.GetChildNoGC(go.transform,"Grid").gameObject
        local len=items.Length
        if len>4 then
            for i=0,2 do
                local previewItem=NGUITools.AddChild(itemParent,self.m_PreviewItemTemplate)
                previewItem:SetActive(true)
                self:InitPreviewItem(previewItem,items[i],false)
                local x,y,z=self:GetPosition(i,4)
                LuaUtils.SetLocalPosition(previewItem.transform,x,y,z)
                CommonDefs.AddOnClickListener(previewItem,DelegateFactory.Action_GameObject(OnPreviewItemClicked),false)
            end
            --更多内容
            do
                local previewItem=NGUITools.AddChild(itemParent,self.m_PreviewItemTemplate)
                previewItem:SetActive(true)
                self:InitPreviewItem(previewItem,0,true)
                local x,y,z=self:GetPosition(i,4)
                LuaUtils.SetLocalPosition(previewItem.transform,x,y,z)
                CommonDefs.AddOnClickListener(previewItem,DelegateFactory.Action_GameObject(OnPreviewItemClicked),false)
            end
        else
            for i=0,len-1 do
                local previewItem=NGUITools.AddChild(itemParent,self.m_PreviewItemTemplate)
                previewItem:SetActive(true)
                self:InitPreviewItem(previewItem,items[i],false)
                local x,y,z=self:GetPosition(i,len)
                LuaUtils.SetLocalPosition(previewItem.transform,x,y,z)
                CommonDefs.AddOnClickListener(previewItem,DelegateFactory.Action_GameObject(OnPreviewItemClicked),false)
            end
        end
        -- itemGrid:Reposition()
    end
    grid:Reposition()
    scrollView:ResetPosition()
end

function  CLuaFashionGiftView:InitPreviewItem( go,id, more )
    if more then
        LuaGameObject.GetChildNoGC(go.transform,"Normal").gameObject:SetActive(false)
        LuaGameObject.GetChildNoGC(go.transform,"MoreContent").gameObject:SetActive(true)
    else
        LuaGameObject.GetChildNoGC(go.transform,"Normal").gameObject:SetActive(true)
        LuaGameObject.GetChildNoGC(go.transform,"MoreContent").gameObject:SetActive(false)

        local designData=Mall_ItemPreview.GetData(id)
        LuaGameObject.GetChildNoGC(go.transform,"DescLabel").label.text=tostring(designData.Desc)
        if designData.RandomTag>0 then
            LuaGameObject.GetChildNoGC(go.transform,"Random").gameObject:SetActive(true)
        else
            LuaGameObject.GetChildNoGC(go.transform,"Random").gameObject:SetActive(false)
        end
        local itemData=Item_Item.GetData(designData.ItemId)
        LuaGameObject.GetChildNoGC(go.transform,"IconTexture").cTexture:LoadMaterial(itemData.Icon,false)
    end
end

function CLuaFashionGiftView:GetPosition(index,num)
    if num==1 then
        return 0,0,0
    elseif num==2 then
        if index==0 then
            return -70,0,0
        else
            return 70,0,0
        end
    elseif num==3 then
        if index==0 then
            return 0,70,0
        elseif index==1 then
            return -70,-70,0
        elseif index==2 then
            return 70,-70,0
        end
    elseif num==4 then
        if index==0 then
            return -70,70,0
        elseif index==1 then
            return 70,-70,0
        elseif index==2 then
            return -70,-70,0
        elseif index==3 then
            return 70,-70,0
        end
    end
end

function CLuaFashionGiftView:OnClickBuyButton(go)
    local index=go.transform.parent:GetSiblingIndex()
    local id=self.m_Ids[index+1]
    local pid=Mall_LingYuMallLimit.GetData(id).RmbPID
    CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(pid),0)
end
function CLuaFashionGiftView:OnClickPreviewItem(go)
    local index=go.transform.parent.parent:GetSiblingIndex()
    local id=self.m_Ids[index+1]
    local items=Mall_LingYuMallLimit.GetData(id).ItemPreview
    local index2=go.transform:GetSiblingIndex()
    CLuaFashionGiftMgr.SelectedGiftId=id
    -- CUIManager.ShowUI(CUIResources.FashionGiftDetailWnd)
    if index2==3 and items.Length>4 then
        CUIManager.ShowUI(CUIResources.FashionGiftDetailWnd)
    else
        local itemId=Mall_ItemPreview.GetData(items[index2]).ItemId
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false,nil,AlignType.Default,0,0,0,0)
    end
end

function CLuaFashionGiftView:OnEnable()
    CShopMallMgr.Inst:EnableEvent()
    g_ScriptEvent:AddListener("UpdateMallLimit", self, "OnUpdateMallLimit")
    g_ScriptEvent:AddListener("UpdateShangXiaJiaZheKouLiBao", self, "OnUpdateShangXiaJiaZheKouLiBao")
end
function CLuaFashionGiftView:OnDisable()
    CShopMallMgr.Inst:DisableEvent()
    g_ScriptEvent:RemoveListener("UpdateMallLimit", self, "OnUpdateMallLimit")
    g_ScriptEvent:RemoveListener("UpdateShangXiaJiaZheKouLiBao", self, "OnUpdateShangXiaJiaZheKouLiBao")
end
function CLuaFashionGiftView:OnUpdateMallLimit()
    local tf=LuaGameObject.GetChildNoGC(self.transform,"Grid").transform
    local count=tf.childCount
    for i=1,count do
        local item=tf:GetChild(i-1)
        local button=LuaGameObject.GetChildNoGC(item,"BuyButton").gameObject
        
        local id=self.m_Ids[i]
        local limitCount=CShopMallMgr.Inst:GetLimitCount(id)
        LuaGameObject.GetChildNoGC(item,"LimitLabel").label.text=SafeStringFormat3(LocalString.GetString("每月限购%d次"),limitCount)
        if limitCount>0 then
            CUICommonDef.SetActive(button,true,true)
        else
            CUICommonDef.SetActive(button,false,true)
        end
    end
end
function CLuaFashionGiftView:OnUpdateShangXiaJiaZheKouLiBao(args)
    local list=args[0]
    local len=list.Count

    local tempT={}
    for i,v in ipairs(self.m_Ids) do
        tempT[v]=true
    end
    for i=1,len do
        local itemId=list[i-1]
        tempT[itemId]=true
    end
    self.m_Ids={}
    for k,v in pairs(tempT) do
        if v then
            table.insert( self.m_Ids,k )
        end
    end

    self:InitItems()
end

return CLuaFashionGiftView