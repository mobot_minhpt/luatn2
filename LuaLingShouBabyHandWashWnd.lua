local LuaGameObject=import "LuaGameObject"
local Gac2Gas=import "L10.Game.Gac2Gas"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local CItemMgr=import "L10.Game.CItemMgr"
local EnumItemPlace=import "L10.Game.EnumItemPlace"
local MessageMgr=import "L10.Game.MessageMgr"
local CGetMoneyMgr=import "L10.UI.CGetMoneyMgr"

CLuaLingShouBabyHandWashWnd=class(CLuaLingShouBabyBaseWashWnd)

function CLuaLingShouBabyHandWashWnd:Init()
    CLuaLingShouBabyBaseWashWnd.Init(self)
    --按钮置灰
    self:InitView()
end

function CLuaLingShouBabyHandWashWnd:InitView()
    LuaGameObject.GetChildNoGC(self.transform,"TitleLabel").label.text=LocalString.GetString("灵兽宝宝洗炼")
    LuaGameObject.GetChildNoGC(self.transform,"AccelerateButton").gameObject:SetActive(false)
end

--手动洗炼
function CLuaLingShouBabyHandWashWnd:RequestWash()
    --看看材料够不够
    local rst,pos,itemId=CItemMgr.Inst:FindItemByTemplateIdWithNotExpired(EnumItemPlace.Bag, CLuaLingShouMgr.BiLiuLuId)
    if rst then
        Gac2Gas.RequestWashLingShouBaby(self.m_LingShouId, LuaEnumItemPlace.Bag, pos, itemId, false)
        CUICommonDef.SetActive(self.m_AcceptButton,false,true)
        CUICommonDef.SetActive(self.m_StartButton,false,true)
        return
    else
        --没有材料了
        --尝试用灵玉购买
        if self:AutoCostLingyu() then
            --看看灵玉够不够
            if self.m_MoneyCtrl.moneyEnough then
                Gac2Gas.RequestWashLingShouBaby(self.m_LingShouId, LuaEnumItemPlace.Bag, 0, "",true)
                CUICommonDef.SetActive(self.m_AcceptButton,false,true)
                CUICommonDef.SetActive(self.m_StartButton,false,true)
                return
            else
                CGetMoneyMgr.Inst:GetLingYu(self.m_JadeCost,CLuaLingShouMgr.BiLiuLuId)
                return
            end
        else
            MessageMgr.Inst:ShowMessage("LINGSHOU_NOT_HAVE_WASH_ITEM",{})
            return
        end
        return
    end
end


function CLuaLingShouBabyHandWashWnd:OnEnable()
    CLuaLingShouBabyBaseWashWnd.OnEnable(self)
end
function CLuaLingShouBabyHandWashWnd:OnDisable()
    CLuaLingShouBabyBaseWashWnd.OnDisable(self)
end


function CLuaLingShouBabyHandWashWnd:OnUpdateLingShouBabyLastWashResult( args )
    local lingShouId=args[0]
    local babyQuality=args[1]
    local babySkillCls1=args[2]
    local babySkillCls2=args[3]
    --洗炼结束
    if self.m_LingShouId==lingShouId then
        self.m_View2:InitResult(self.m_BabyInfo,babyQuality,babySkillCls1,babySkillCls2)
        CUICommonDef.SetActive(self.m_AcceptButton,true,true)
        if babyQuality>self.m_BabyInfo.Quality then
            -- print("quality",babyQuality,self.m_BabyInfo.Quality)
            self.m_RecommendSprite.gameObject:SetActive(true)
        else
            self.m_RecommendSprite.gameObject:SetActive(false)
        end
        CUICommonDef.SetActive(self.m_StartButton,true,true)

        local details=CLingShouMgr.Inst:GetLingShouDetails(self.m_LingShouId)
        if details~=nil then
            self.m_BabyInfo=details.data.Props.Baby
        end
    end
end

return CLuaLingShouBabyHandWashWnd
