-- Auto Generated!!
local CQnReturnWindow = import "L10.UI.CQnReturnWindow"
local CSigninMgr = import "L10.Game.CSigninMgr"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
CQnReturnWindow.m_Init_CS2LuaHook = function (this) 

    CSigninMgr.Inst.hasCheckQnReturn = true
    EventManager.Broadcast(EnumEventType.OnCheckQnReturn)
    this.confirm:SetActive(not CSigninMgr.Inst.qnReturnInfo.hasConfirmBind)
    this.detailView:SetActive(CSigninMgr.Inst.qnReturnInfo.hasConfirmBind)
end
