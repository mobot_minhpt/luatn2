local GameObject			= import "UnityEngine.GameObject"
local StringSplitOptions    = import "System.StringSplitOptions"

local UILabel				= import "UILabel"
local DelegateFactory       = import "DelegateFactory"

local CUITexture                    = import "L10.UI.CUITexture"
local CUIManager                    = import "L10.UI.CUIManager"
local CItemInfoMgr			        = import "L10.UI.CItemInfoMgr"
local AlignType				        = import "L10.UI.CItemInfoMgr+AlignType"
local CItemAccessListMgr            = import "L10.UI.CItemAccessListMgr"
local AlignType2                    = import "L10.UI.CTooltip+AlignType"
local DefaultItemActionDataSource   = import "L10.UI.DefaultItemActionDataSource"
local CIndirectUIResources          = import "L10.UI.CIndirectUIResources"

local CItemMgr              = import "L10.Game.CItemMgr"
local CommonDefs            = import "L10.Game.CommonDefs"
local EnumItemPlace         = import "L10.Game.EnumItemPlace"

--中秋月邀伴赏月选物品界面
CLuaZQSelectionToolTip = class()

RegistChildComponent(CLuaZQSelectionToolTip, "EftLabel",		UILabel)
RegistChildComponent(CLuaZQSelectionToolTip, "EnterBtn",		GameObject)
RegistChildComponent(CLuaZQSelectionToolTip, "Items",			GameObject)
RegistChildComponent(CLuaZQSelectionToolTip, "Select",			GameObject)
RegistChildComponent(CLuaZQSelectionToolTip, "BgMask",			GameObject)

RegistClassMember(CLuaZQSelectionToolTip, "m_Data")
RegistClassMember(CLuaZQSelectionToolTip, "m_SelectIndex")

CLuaZQSelectionToolTip.SelectNpcTempId = nil
CLuaZQSelectionToolTip.PosX = 0
CLuaZQSelectionToolTip.Dir = 0

function CLuaZQSelectionToolTip.Show(npctempid,posx,dir)
    CLuaZQSelectionToolTip.PosX = posx
    CLuaZQSelectionToolTip.Dir = dir
    CLuaZQSelectionToolTip.SelectNpcTempId = npctempid
    CUIManager.ShowUI("ZQSelectionToolTip")
end

function CLuaZQSelectionToolTip:Init()

    local trans = self.gameObject.transform
    local pos = trans.position
    pos.x = CLuaZQSelectionToolTip.PosX
    trans.position = pos

    local localpos = trans.localPosition
    localpos.x = localpos.x + CLuaZQSelectionToolTip.Dir * 300
    trans.localPosition = localpos

    local npcid = CLuaZQSelectionToolTip.SelectNpcTempId
    if npcid == nil then return end

    self:InitData(npcid)

    self:Repaint()

    local btnclick = function(go)
        self:OnUseItemBtnClick()
    end
    CommonDefs.AddOnClickListener(self.EnterBtn,DelegateFactory.Action_GameObject(btnclick), false)

    local maskclick = function(go)
        self:OnMaskClick()
    end
    CommonDefs.AddOnClickListener(self.BgMask,DelegateFactory.Action_GameObject(maskclick), false)
end

function CLuaZQSelectionToolTip:OnMaskClick( )
    if self.tipopen then
        self.tipopen = false
        CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
    else
        CUIManager.CloseUI("ZQSelectionToolTip")
    end
end

function CLuaZQSelectionToolTip:Repaint()
    local ct = #self.m_Data

    for i=1,9 do
        local item = self.Items.transform:Find("Item"..i).gameObject
        if i <= ct then    
            item:SetActive(true)
            self:InitItem(item,self.m_Data[i].TempID)

            local onclick= function(go)
                self:OnItemSelect(item,i,true)
            end
            CommonDefs.AddOnClickListener(item, DelegateFactory.Action_GameObject(onclick), false)
            if i == self.m_SelectIndex then 
                self:OnItemSelect(item,i,false)
            end
        else
            item:SetActive(false)
        end
    end
end

function CLuaZQSelectionToolTip:OnUseItemBtnClick()
    local data = self.m_Data[self.m_SelectIndex]
    local hasfind,pos,itemid= CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag,data.TempID)
    if hasfind then
        Gac2Gas.RequestSubmitZhongQiuNpcFriendlinessItem(
            CLuaZQSelectionToolTip.SelectNpcTempId,
            EnumItemPlace.Bag,
            pos,
            itemid)
    end
end

function CLuaZQSelectionToolTip:InitData(npcid)
    self.m_Data = {}
    self.m_SelectIndex = 1
    local npcdatas = ZhongQiu2019_NpcFriendliness.GetData(npcid)
    if npcdatas == nil then return end
    local itemstrs = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(
        npcdatas.SubmitItems, 
        Table2ArrayWithCount({",",";"}, 2, MakeArrayClass(String)),
        StringSplitOptions.RemoveEmptyEntries)
    local index = 1
    for i=0,itemstrs.Length-1,2 do
        local itemid = itemstrs[i]
        local eftstr = tonumber(itemstrs[i+1])
        self.m_Data[index] = {TempID = itemid,EftStr = eftstr}
        index = index+1
    end
end

function CLuaZQSelectionToolTip:InitItem(ctrl,tempid)
    local tempdata = Item_Item.GetData(tempid)
    if tempdata == nil then return end

    local trans = ctrl.transform
    local icon = trans:Find("Texture").gameObject:GetComponent(typeof(CUITexture))
    local ctlabel = trans:Find("Name").gameObject:GetComponent(typeof(UILabel))
    local mask = trans:Find("mask")

    icon:LoadMaterial(tempdata.Icon)

    local itemcount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag,tempid)
    local color = itemcount <=0 and "[ff0000]" or "[ffffff]"
    ctlabel.text = color..tostring(itemcount).."[-]/1"
    mask.gameObject:SetActive(itemcount<=0)
end

function CLuaZQSelectionToolTip:OnItemSelect(ctrl,index,showtip)
    self.m_SelectIndex = index
    local data = self.m_Data[index]
    self.Select.transform.position = ctrl.transform.position
    self.EftLabel.text = data.EftStr>0 and "+"..data.EftStr or tostring(data.EftStr)
    if showtip then
        local t_name = {}
	    t_name[1] = LocalString.GetString("获得途径") 
	    local t_action = {}
        t_action[1] = function ()
            CItemAccessListMgr.Inst:ShowItemAccessInfo(data.TempID, false,ctrl.transform, AlignType2.Right)
        end
        local actionSource = DefaultItemActionDataSource.Create(1,t_action,t_name)
        CItemInfoMgr.ShowLinkItemTemplateInfo(data.TempID, false, actionSource, AlignType.Default, 0, 0, 0, 0)
        self.tipopen = true
    end
end

function CLuaZQSelectionToolTip:OnPlayerItemChange(args)
    self:Repaint()
end

function CLuaZQSelectionToolTip:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "OnPlayerItemChange")
end

function CLuaZQSelectionToolTip:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "OnPlayerItemChange")
end
