local Vector3 = import "UnityEngine.Vector3"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local Extensions = import "Extensions"
local Color = import "UnityEngine.Color"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local UIScrollView = import "UIScrollView"

CLuaGemGroupInfoDetailView = class()
RegistClassMember(CLuaGemGroupInfoDetailView,"nameLabel")
RegistClassMember(CLuaGemGroupInfoDetailView,"gradeCheckLabel")
RegistClassMember(CLuaGemGroupInfoDetailView,"descLabel")
RegistClassMember(CLuaGemGroupInfoDetailView,"table")
RegistClassMember(CLuaGemGroupInfoDetailView,"gemTemplate")
RegistClassMember(CLuaGemGroupInfoDetailView,"tips")
RegistClassMember(CLuaGemGroupInfoDetailView,"background")
RegistClassMember(CLuaGemGroupInfoDetailView,"t_GemList")
RegistClassMember(CLuaGemGroupInfoDetailView,"scrollview")
RegistClassMember(CLuaGemGroupInfoDetailView,"gemGroupId")

function CLuaGemGroupInfoDetailView:Awake()
    self.nameLabel = self.transform:Find("Header/ItemNameLabel"):GetComponent(typeof(UILabel))
    self.gradeCheckLabel = self.transform:Find("Header/level"):GetComponent(typeof(UILabel))
    self.descLabel = self.transform:Find("Scrollview/Desc"):GetComponent(typeof(UILabel))
    self.table = self.transform:Find("Scrollview/GemList/Table"):GetComponent(typeof(UITable))
    self.gemTemplate = self.transform:Find("Scrollview/GemList/GemTemplate").gameObject
    self.tips = self.transform:Find("Scrollview/Tips"):GetComponent(typeof(UILabel))
    self.background = self.transform:GetComponent(typeof(UISprite))
    self.t_GemList = self.transform:Find("Scrollview/GemList")
    self.scrollview = self.transform:Find("Scrollview"):GetComponent(typeof(UIScrollView))
self.gemGroupId = 0

end

function CLuaGemGroupInfoDetailView:Init( gemGroupId) 
    self.nameLabel.text = ""
    self.gradeCheckLabel.text = ""
    self.descLabel.text = ""
    Extensions.RemoveAllChildren(self.table.transform)
    self.gemTemplate:SetActive(false)
    self.gemGroupId = gemGroupId
    local gemGroup = GemGroup_GemGroup.GetData(gemGroupId)
    if gemGroup == nil then
        return
    end

    self.gradeCheckLabel.text = SafeStringFormat3(LocalString.GetString("%d级"), gemGroup.Grade)
    self.gradeCheckLabel.color = CClientMainPlayer.Inst.Level >= gemGroup.Grade and Color.white or Color.red

    self.nameLabel.text = SafeStringFormat3(LocalString.GetString("%d级%s介绍"), gemGroup.Level, gemGroup.Name)
    local c = GameSetting_Common_Wapper.Inst:GetColor(gemGroup.NameColor)
    self.nameLabel.color = c
    local builder = ""
    do
        local i = 0
        while i < gemGroup.Effect.Length do
            if i ~= 0 then
                builder = builder.."\n"
            end
            local word = Word_Word.GetData(gemGroup.Effect[i])
            builder = builder..word.Description
            i = i + 1
        end
    end
    self.descLabel.text = builder
    self.descLabel.color = c

    do
        local i = 0
        while i < gemGroup.Consist.Length do
            local jewel = Jewel_Jewel.GetData(gemGroup.Consist[i])
            local item = Item_Item.GetData(gemGroup.Consist[i])
            if jewel ~= nil and item ~= nil then
                local instance = NGUITools.AddChild(self.table.gameObject, self.gemTemplate)
                instance:SetActive(true)
                CommonDefs.GetComponent_GameObject_Type(instance, typeof(UILabel)).text = jewel.JewelName
                CommonDefs.GetComponent_GameObject_Type(instance, typeof(UILabel)).color = GameSetting_Common_Wapper.Inst:GetColor(item.NameColor)
            end
            i = i + 1
        end
    end
    self.table:Reposition()
    self:Reposition()
end
function CLuaGemGroupInfoDetailView:Reposition( )
    self.t_GemList.transform.localPosition = Vector3(self.t_GemList.transform.localPosition.x, self.descLabel.transform.localPosition.y - self.descLabel.height - 42, 0)
    self.tips.transform.localPosition = Vector3(self.tips.transform.localPosition.x, self.t_GemList.transform.localPosition.y - (self.table.padding.y + CommonDefs.GetComponent_GameObject_Type(self.gemTemplate, typeof(UILabel)).height) * (math.floor((self.table.transform.childCount - 1) / self.table.columns) + 1) - 34 - CommonDefs.GetComponent_Component_Type(self.t_GemList, typeof(UILabel)).height - self.table.padding.y, 0)
    local height = - self.tips.transform.localPosition.y + self.tips.height + 28
    self.scrollview:ResetPosition()
end

