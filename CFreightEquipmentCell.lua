-- Auto Generated!!
local CFreightEquipmentCell = import "L10.UI.CFreightEquipmentCell"
local CItemMgr = import "L10.Game.CItemMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"

CFreightEquipmentCell.m_Init_CS2LuaHook = function (this, id) 
    local item = CItemMgr.Inst:GetById(id)
    this.equipmentId = id
    if item == nil then
        return
    end
    if item.IsEquip then
        this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item.Equip.QualityType)
        this.amountLabel.gameObject:SetActive(false)
    elseif item.IsItem then
        this.qualitySprite.spriteName = CLuaItemMgr:GetItemCellBorder(id)
        this.amountLabel.gameObject:SetActive(true)
        this.amountLabel.text = tostring(item.Amount)
    end
    this.disableSprite.enabled = not item.MainPlayerIsFit
    local icon = item.Icon
    this.iconTexture:LoadMaterial(icon)
    this.bindSprite.spriteName = item.BindOrEquipCornerMark
end
