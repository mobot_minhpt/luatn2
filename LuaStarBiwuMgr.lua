local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CScene = import "L10.Game.CScene"
local CClientNpc = import "L10.Game.CClientNpc"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CQMPKZhanDui = import "L10.UI.CQMPKZhanDui"
local CQMPKMemberInfo = import "L10.Game.CQMPKMemberInfo"
local CCCChatMgr = import "L10.Game.CCCChatMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local MessageWndManager = import "L10.UI.MessageWndManager"
local DelegateFactroy = import "DelegateFactory"
local CQMPKScore = import "L10.UI.CQMPKScore"
local CPropertyAppearance = import "L10.Game.CPropertyAppearance"
local EnumClass = import "L10.Game.EnumClass"
local EnumGender = import "L10.Game.EnumGender"
local CGameVideoMgr = import "L10.Game.CGameVideoMgr"
local Utility = import "L10.Engine.Utility"
local HTTPHelper = import "L10.Game.HTTPHelper"
local Main = import "L10.Engine.Main"
local Utils = import "L10.Game.Utils"
local Object = import "System.Object"
local CLoginMgr = import "L10.Game.CLoginMgr"

CLuaStarBiwuMgr = {}
CLuaStarBiwuMgr.EnumStarBiwuShowPlayStatus = {
	eInit 			= 0,
	eInPaimai 		= 1,
	ePaimaiEnd 		= 2,
	eBiwuBegin 		= 3,
	eBiwuEnd 		= 4,
	eQieCuoSai 		= 5,
	ePiPeiSai   	= 6,
	eJiFenSai   	= 7,
	eXiaoZuSaiSai  	= 8,
	eZongJueSai 	= 9,
	eReShenSai  	= 10,
}

CLuaStarBiwuMgr.m_EnableGiftDanMu = true
CLuaStarBiwuMgr.m_IsStarBiwuScene = false

CLuaStarBiwuMgr.m_HistoryDataTable = {}
CLuaStarBiwuMgr.m_HistoryTeamIndex = 1

CLuaStarBiwuMgr.m_CurrentStatueObj = nil

CLuaStarBiwuMgr.m_FriendList = nil

CLuaStarBiwuMgr.m_bSelectFriend = false

CLuaStarBiwuMgr.JingCaiRecordTable = {}
CLuaStarBiwuMgr.JingCaiRecordCurrentIndex = 1

CLuaStarBiwuMgr.isVoteEnd = true
CLuaStarBiwuMgr.requestPlayerName = nil
CLuaStarBiwuMgr.voteBeginTime = nil
CLuaStarBiwuMgr.agreeList = {}
CLuaStarBiwuMgr.disagreeList = {}
CLuaStarBiwuMgr.choiceList = {}
CLuaStarBiwuMgr.championZhanduiName = ""
CLuaStarBiwuMgr.ShowWndIsQuDao = false  -- 是否官服界面
CLuaStarBiwuMgr.isQuDaoZhanDuiSearchWnd = false
CLuaStarBiwuMgr.isQuDaoCreateZhanDuiWnd = false
CLuaStarBiwuMgr.isQuDaoSelfZhanDuiWnd = false
CLuaStarBiwuMgr.RequestLeiGuTimesTime = 0
CLuaStarBiwuMgr.ShowSaiZhiCompare = true -- 是否需要显示赛制对比
CLuaStarBiwuMgr.m_GroupNum = 0 -- 总共有多少分组，第一个是巅峰组，之后普通组顺延
CLuaStarBiwuMgr.m_CurOpenDetailWndIndex = -1
CLuaStarBiwuMgr.m_CurOpeZhanDuiWndIndex = -1
CLuaStarBiwuMgr.m_MainPlayerGroup = 0
CLuaStarBiwuMgr.m_RequestZhanDuiMemberList = nil
CLuaStarBiwuMgr.m_InvitedZhanDuiMemberList = nil
CLuaStarBiwuMgr.m_PublishInfoToFreePlayer = false
CLuaStarBiwuMgr.m_ZhuanDuiId2ZhuanDuiName = nil
CLuaStarBiwuMgr.m_JiFenSaiMatchInfo = nil
CLuaStarBiwuMgr.m_JiFenSaiRankInfo = nil
CLuaStarBiwuMgr.m_JiFenSaiTotalRound = 0
CLuaStarBiwuMgr.m_MainPlayerStarScore = 0
CLuaStarBiwuMgr.m_InitShowGroup = 0

function CLuaStarBiwuMgr:OpenStarBiWuDetailWnd(index)
  if index then 
    CLuaStarBiwuMgr.m_CurOpenDetailWndIndex = index
  end
  CUIManager.ShowUI(CLuaUIResources.StarBiwuDetailInfoWnd)
end

function CLuaStarBiwuMgr:OpenStarBiWuZhanDuiWnd(index)
  if index then 
    CLuaStarBiwuMgr.m_CurOpeZhanDuiWndIndex = index
  end
  CUIManager.ShowUI(CLuaUIResources.StarBiwuZhanDuiPlatformWnd)
end

function CLuaStarBiwuMgr:GetPlayerIsGuanFu()
    return CLoginMgr.Inst:IsNetEaseOfficialLogin()
end

function CLuaStarBiwuMgr:RequestQueryStarBiwuZhanDuiList(page)
    Gac2Gas.RequestQueryStarBiwuZhanDuiList(page)
end

function CLuaStarBiwuMgr:OnSyncStarBiwuJifensaiMatchS13(matchInfo)
  if not matchInfo or not CLuaStarBiwuMgr.m_JiFenSaiMatchInfo then return end
  local NameList = CLuaStarBiwuMgr.m_ZhuanDuiId2ZhuanDuiName
  local matchTbl = {}
  matchTbl.round = matchInfo.Round
  matchTbl.matchs = {}
  CommonDefs.DictIterate(matchInfo.Infos, DelegateFactory.Action_object_object(function(key, v)
    local match = {}
    match.index = key
    match.attackId = v.AttackerZhanduiId
    match.attackName = NameList and NameList[v.AttackerZhanduiId] or ""
    match.defendId = v.DefenderZhanduiId
    match.defendName = NameList and NameList[v.DefenderZhanduiId] or ""
    match.attackScore = v.AttackerScore
    match.defendScore = v.DefenderScore
    match.winnerId = v.WinnerZhanduiId
    match.status = v.PlayStatus
		table.insert(matchTbl.matchs,match)
	end))
  table.insert(CLuaStarBiwuMgr.m_JiFenSaiMatchInfo, matchTbl)
end

function CLuaStarBiwuMgr:OnSyncStarBiwuJifensaiMatchS13End(day, group, round, selfGroup, selfZhanduiId, bMatchGenerated)
  CLuaStarBiwuMgr.m_MainPlayerGroup = selfGroup
  CLuaStarBiwuMgr.m_MySelfZhanduiId = selfZhanduiId
  g_ScriptEvent:BroadcastInLua("OnStarBiWuJiFenSaiResultData", selfZhanduiId,group,day)
end

function CLuaStarBiwuMgr:OnSyncStarBiwuJifensaiRankS13(rpcInfo)
  if not rpcInfo or not CLuaStarBiwuMgr.m_JiFenSaiRankInfo then return end
  local NameList = CLuaStarBiwuMgr.m_ZhuanDuiId2ZhuanDuiName
  CLuaStarBiwuMgr.m_JiFenSaiTotalRound = 0
  CommonDefs.DictIterate(rpcInfo.Infos, DelegateFactory.Action_object_object(function(key, v)
    local rankinfo = {}
    rankinfo.index = key
    rankinfo.zhanduiId = v.ZhanduiId
    rankinfo.zhanduiName = NameList and NameList[v.ZhanduiId] or ""
    rankinfo.winCount = v.MainRank.WinCount
    rankinfo.loseCount = v.MainRank.LoseCount
    rankinfo.winTime = v.MainRank.WinTime
    rankinfo.roundRank = {}
    local round = 0
		-- key_3 : 轮次
		CommonDefs.DictIterate(v.RoundRank, DelegateFactory.Action_object_object(function(key_3, v_3)
      round = round + 1
			rankinfo.roundRank[key_3] = {}
      rankinfo.roundRank[key_3].WinFlag = v_3.WinFlag
      rankinfo.roundRank[key_3].zhanduiId = v_3.MatchZhanduiId
      rankinfo.roundRank[key_3].zhanduiName = NameList and NameList[v_3.MatchZhanduiId] or ""
		end))
    table.insert(CLuaStarBiwuMgr.m_JiFenSaiRankInfo, rankinfo)
    CLuaStarBiwuMgr.m_JiFenSaiTotalRound = round
	end))
end

function CLuaStarBiwuMgr:OnSyncStarBiwuJifensaiRankS13End(group, selfGroup, selfZhanduiId)
  CLuaStarBiwuMgr.m_MainPlayerGroup = selfGroup
  CLuaStarBiwuMgr.m_MySelfZhanduiId = selfZhanduiId
  g_ScriptEvent:BroadcastInLua("OnStarBiWuJiFenSaiRankData", selfZhanduiId,selfGroup,group)
end

function CLuaStarBiwuMgr:OnSyncStarBiwuTaiTaiSaiMatchS13(group, selfGroup, selfZhanduiId, rpcInfo, currentMatchPlayIdx)
  CLuaStarBiwuMgr.m_MainPlayerGroup = selfGroup
  CLuaStarBiwuMgr.m_MySelfZhanduiId = selfZhanduiId
  local matchTbl = {}
  CommonDefs.DictIterate(rpcInfo.Infos, DelegateFactory.Action_object_object(function(key, v)
    local match = {}
    match.attackId = v.AttackerZhanduiId
    match.attackName = v.AttackerZhanDuiName
    match.defendId = v.DefenderZhanduiId
    match.defendName = v.DefenderZhanDuiName
    match.attackScore = v.AttackerScore
    match.defendScore = v.DefenderScore
    match.winnerId = v.WinnerZhanduiId
    match.status = v.PlayStatus
		table.insert(matchTbl,match)
	end))
  g_ScriptEvent:BroadcastInLua("OnStarBiWuTaoTaiSaiResultData", currentMatchPlayIdx,selfZhanduiId,matchTbl,group)
end

function CLuaStarBiwuMgr:OnSyncStarBiwuMyInviver(rpcInfo)
  if not rpcInfo then return end
  CommonDefs.DictIterate(rpcInfo.Infos, DelegateFactory.Action_object_object(function(key, v)
    local zhanduiInfo = {}
    zhanduiInfo.ZhanduiId = v.ZhanduiId
    zhanduiInfo.ZhanDuiName = v.ZhanDuiName
    zhanduiInfo.Capacity = v.Capacity
    zhanduiInfo.StarScore = v.StarScore
    zhanduiInfo.InviterId = v.InviterId
    zhanduiInfo.InviterName = v.InviterName
    zhanduiInfo.AcceptFlag = v.AcceptFlag
    zhanduiInfo.ZhanDuiMembers = {}
    CommonDefs.DictIterate(v.ZhanDuiMembers, DelegateFactory.Action_object_object(function(key_2, v_2)
			table.insert(zhanduiInfo.ZhanDuiMembers,{id = v_2.MemberId,class = v_2.MemberClass})
		end))
		table.insert(CLuaStarBiwuMgr.m_InvitedZhanDuiMemberList,zhanduiInfo)
	end))
end

function CLuaStarBiwuMgr:OnSyncStarBiwuFreememberS13(profession, sortMethed, memberInfoUd,  page, count, bPublishSelfInfo, totalPageCount)
  local list = MsgPackImpl.unpack(memberInfoUd)
	if not list then return end
  local memberList = {}
	for i = 0, list.Count - 1  do
    local info = list[i]
    local member = {}
		local memberId = info[0]
		local playerName = info[1]
		local fromServerName = info[2]
		local fakeAppearance = CreateFromClass(CPropertyAppearance)
		
		local capacity = info[4]
		local starScore = info[5]
		local profession = info[6]
		local gender = info[7]
		local grade = info[8]
		local xianShenStatus = info[9]
    fakeAppearance:LoadFromString(info[3], CommonDefs.ConvertIntToEnum(typeof(EnumClass), profession), CommonDefs.ConvertIntToEnum(typeof(EnumGender), gender))
    member.Id = memberId
    member.Name = playerName
    member.ServerName = fromServerName
    member.Appearance = fakeAppearance
    member.Capacity = capacity
    member.StarScore = starScore
    member.Level = grade
    member.Class = profession
    member.Gender = gender
    member.xianShenStatus = xianShenStatus
    table.insert(memberList, member)
	end
  CLuaStarBiwuMgr.m_PublishInfoToFreePlayer = bPublishSelfInfo
  g_ScriptEvent:BroadcastInLua("OnSyncStarBiwuFreememberS13", profession, sortMethed, memberList, page, count, bPublishSelfInfo, totalPageCount)
end

function CLuaStarBiwuMgr:OnSyncJiFenSaiZhuanDuiId2Name(rpcInfo)
  if not rpcInfo or not CLuaStarBiwuMgr.m_ZhuanDuiId2ZhuanDuiName then return end
  CommonDefs.DictIterate(rpcInfo.Infos, DelegateFactory.Action_object_object(function(key, v)
		CLuaStarBiwuMgr.m_ZhuanDuiId2ZhuanDuiName[key] = v
	end))
end

function CLuaStarBiwuMgr:OnSyncStarBiwuActivityStatusS13(signUpStatus, bSignUp, bHasFight)
  g_ScriptEvent:BroadcastInLua("OnSyncStarBiwuActivityStatusS13", signUpStatus, bSignUp, bHasFight)
end

function  CLuaStarBiwuMgr:OnSyncStarBiwuJifesanSaiGroupS13(selfGroup, groupCount, selfZhanduiId)
  CLuaStarBiwuMgr.m_MainPlayerGroup = selfGroup
  CLuaStarBiwuMgr.m_MySelfZhanduiId = selfZhanduiId
  CLuaStarBiwuMgr.m_GroupNum = groupCount
  g_ScriptEvent:BroadcastInLua("OnSyncStarBiwuJifesanSaiGroupS13")
end

function  CLuaStarBiwuMgr:OnSyncStarBiwuTaotaiSaiGroupS13(selfGroup, groupCount, selfZhanduiId)
  CLuaStarBiwuMgr.m_MainPlayerGroup = selfGroup
  CLuaStarBiwuMgr.m_MySelfZhanduiId = selfZhanduiId
  CLuaStarBiwuMgr.m_GroupNum = groupCount
  g_ScriptEvent:BroadcastInLua("OnSyncStarBiwuTaotaiSaiGroupS13")
end

function  CLuaStarBiwuMgr:OnSyncStarBiWuUpdatePlayerGroup(selfGroup)
  CLuaStarBiwuMgr.m_MainPlayerGroup = selfGroup
  g_ScriptEvent:BroadcastInLua("OnStarBiWuUpdatePlayerGroup")
end

function CLuaStarBiwuMgr:OnSyncStarBiWuStarScore(StarScore)
  CLuaStarBiwuMgr.m_MainPlayerStarScore = StarScore
  g_ScriptEvent:BroadcastInLua("OnStarBiWuUpdateMainPlayerStarScore")
end

function CLuaStarBiwuMgr:AddListener()
  g_ScriptEvent:RemoveListener("RenderSceneInit", self, "RenderSceneInit")
  g_ScriptEvent:AddListener("RenderSceneInit", self, "RenderSceneInit")

  g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "RenderSceneInit")
  g_ScriptEvent:AddListener("MainPlayerCreated", self, "RenderSceneInit")

  g_ScriptEvent:RemoveListener("OnClientNpcSelected", self, "OnClientNpcSelected")
  g_ScriptEvent:AddListener("OnClientNpcSelected", self, "OnClientNpcSelected")
end

CLuaStarBiwuMgr:AddListener()

function CLuaStarBiwuMgr:OnClientNpcSelected(argv)
  local clientObj = argv[0]
  if clientObj.TemplateId == CClientNpc.s_StarBiwuChampionStatueId then
    if not CUIManager.IsLoaded(CLuaUIResources.StarBiwuStatueBuildWnd) then
  		Gac2Gas.RequestSelectStarBiwuShowStatue(clientObj.EngineId)
  	end
    CLuaStarBiwuMgr.m_CurrentStatueObj = clientObj
    g_ScriptEvent:BroadcastInLua("StarBiwuStatueClicked")
  end
end

function CLuaStarBiwuMgr:RenderSceneInit()
  if not CRenderScene.Inst then return end
	--跨服明星赛和全民争霸赛的比赛和观战场景
  if CScene.MainScene ~= nil and (CScene.MainScene.SceneTemplateId == 16100845 or CScene.MainScene.SceneTemplateId == 16000219 or CScene.MainScene.SceneTemplateId == 16100304 or CScene.MainScene.SceneTemplateId == 16000142) then
    --local tbl = {73, 42, 43, 42, 42, 43, 42, 73, 43, 74, 73, 74, 74, 73, 74, 43}
    local tbl = {}
    local points = {{43, 44}, {72, 44}, {72, 73}, {43, 73}, {43, 44}}
    for k = 1, #points - 1 do
      table.insert(tbl, points[k][1])
      table.insert(tbl, points[k][2])
      table.insert(tbl, points[k+1][1])
      table.insert(tbl, points[k+1][2])
    end
    CRenderScene.Inst:GenSceneFx(Table2Array(tbl, MakeArrayClass(int)), 88801015, 2)
  else
    CRenderScene.Inst:RemoveSceneFx()
  end
  CLuaStarBiwuMgr.m_IsStarBiwuScene = false
  CUIManager.CloseUI(CLuaUIResources.StarBiwuMatchRecordWnd)
end

function CLuaStarBiwuMgr:IsInStarBiwu()
  --if CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetCurrentServerId() == 14 then
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetCurrentServerId() == 5021 then
        return true
    elseif CScene.MainScene and CScene.MainScene.GamePlayDesignId == 51101092 then
        return true
    end
    return false
end

function CLuaStarBiwuMgr:IsInStarBiwuWatch()
	if CScene.MainScene ~= nil and CScene.MainScene.SceneTemplateId == 16000219 then
		return true
	end
	return false
end

function CLuaStarBiwuMgr:GetQuestionInfo(info)
  local questionList = {}
  CommonDefs.DictIterate(info.Infos, DelegateFactory.Action_object_object(function(key, v)
    local question = {}
    question.QuestionId = v.QuestionId
    question.IsOpen = v.IsOpen
    question.RightAnswer = v.RightAnswer
    question.MyChoice = v.MyChoice
    table.insert(questionList, question)
		-- print(key, "QuestionId", v.QuestionId) -- Id
		-- print(key, "IsOpen", v.IsOpen) -- 1 表示已经开放问答， 0表示尚未开放
		-- print(key, "RightAnswer", v.RightAnswer) -- 正确答案，非0表示已经公布答案
		-- print(key, "MyChoice", v.MyChoice) -- 我的选择， 非0表示选了，0表示尚未选择
	end))
  g_ScriptEvent:BroadcastInLua("OnSyncStarBiwuQuestionInfo",questionList)
end

function CLuaStarBiwuMgr.OnEnterPlay()
    CLuaStarBiwuMgr.m_FightData={}
    CLuaStarBiwuMgr.m_MatchRecordList={}
    CUIManager.ShowUI(CLuaUIResources.CommonKillInfoTopWnd)
end

CLuaStarBiwuMgr.m_MatchRecordList={}
function CLuaStarBiwuMgr.ReplyStarBiwuJiFenSaiWatchList(selfZhanduiId, dataUD)
  local list=MsgPackImpl.unpack(dataUD)
  if not list then return end
  CLuaStarBiwuMgr.m_MatchRecordList={}
  for i=1,list.Count,6 do
      local record={
          m_Zhandui1=tonumber(list[i-1]),
          m_TeamName1=tostring(list[i]),
          m_Zhandui2=tonumber(list[i+1]),
          m_TeamName2=tostring(list[i+2]),
          m_Win=tonumber(list[i+3]),
          m_IsMatching=tonumber(list[i+4])>0
      }
      table.insert(CLuaStarBiwuMgr.m_MatchRecordList,record )
  end
  table.sort(CLuaStarBiwuMgr.m_MatchRecordList, function (a, b)
    if a.m_IsMatching and not b.m_IsMatching then
      return true
    elseif b.m_IsMatching and not a.m_IsMatching then
      return false
    elseif a.m_Win >= 0 and b.m_Win < 0 then
      return false
    elseif b.m_Win >= 0 and a.m_Win < 0 then
      return true
    else
      return a.m_Zhandui1 < b.m_Zhandui1
    end
  end)
  CUIManager.ShowUI(CLuaUIResources.StarBiwuMatchRecordWnd)
end

function Gas2Gac.SyncEnterStarBiwuScene()
	CLuaStarBiwuMgr.m_IsStarBiwuScene = true
  CLuaStarBiwuMgr:RenderSceneInit()
end

CLuaStarBiwuMgr.m_CurrentZhanDuiIndex = 1
-- 0:list 1:one 2:score
CLuaStarBiwuMgr.m_ShowZhanDuiMemberType = 0
function CLuaStarBiwuMgr:ShowZhanDuiMemberWnd(index, zhanduiId, type)
  if CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsCrossServerPlayer and zhanduiId ~= 0 then
    CLuaStarBiwuMgr.m_CurrentZhanDuiIndex = index
    CLuaStarBiwuMgr.m_CurrentZhanduiId = zhanduiId
    CLuaStarBiwuMgr.m_ShowZhanDuiMemberType = type
    CUIManager.ShowUI(CLuaUIResources.StarBiwuZhanDuiMemberListWnd)
  end
end

function CLuaStarBiwuMgr:ShowZhanDuiMember2Wnd(index, zhanduiId, type)
  if zhanduiId ~= 0 then
    CLuaStarBiwuMgr.m_CurrentZhanDuiIndex = index
    CLuaStarBiwuMgr.m_CurrentZhanduiId = zhanduiId
    CLuaStarBiwuMgr.m_ShowZhanDuiMemberType = type
    CUIManager.ShowUI(CLuaUIResources.StarBiwuZhanDuiMemberListWnd)
  end
end

function CLuaStarBiwuMgr:TopAndRightMenuWndInit(gamePlayId, wnd)
    if gamePlayId == 51101092 then
        wnd.contentNode:SetActive(false)
        CUIManager.ShowUI(CUIResources.FightingSpiritDamageWnd)
    else
        CUIManager.CloseUI(CUIResources.FightingSpiritDamageWnd)
    end
end

function CLuaStarBiwuMgr:GetPlayListUrl(season)
    local url = SafeStringFormat3("%s/starbiwu/list_%d.txt",CGameVideoMgr.GameVideoListBaseUrl,season)
    return url
end

CLuaStarBiwuMgr.m_VideoRecord = {}
function CLuaStarBiwuMgr:DownloadPlayList()
    self.m_VideoRecord = {}
    StarBiWuShow_AutoPatch.Foreach(function(key, data) 
        local season = data.Season
        local url =  Utility.TransformUrl(self:GetPlayListUrl(season))
        Main.Inst:StartCoroutine(HTTPHelper.GetUrl(url, DelegateFactory.Action_bool_string(function (success, result)
            if success then
                self:ParsePlayInfo(result,season)
                EventManager.BroadcastInternalForLua(EnumEventType.GameVideoListDownloaded, {true, "starbiwu"})
            else
                EventManager.BroadcastInternalForLua(EnumEventType.GameVideoListDownloaded, {false, "starbiwu"})
            end
        end)))
    end)
end

function CLuaStarBiwuMgr:ParsePlayInfo(jsonStr,season)
    if not self.m_VideoRecord then 
        self.m_VideoRecord = {}
    end
    self.m_VideoRecord[season] = {}
    local objList = Utils.Json.Deserialize(jsonStr)
    for i = 0,objList.Count - 1 do
        local dic = objList[i]
        local t = {}
        local res,info
        res, t.fileName = CommonDefs.DictTryGet(dic, typeof(String), "FileName", typeof(String))
        res, t.size = CommonDefs.DictTryGet(dic, typeof(String), "Size", typeof(Double))
        res, t.url = CommonDefs.DictTryGet(dic, typeof(String), "url", typeof(String))
        res, t.zip = CommonDefs.DictTryGet(dic, typeof(String), "zip", typeof(Boolean))

        res, info = CommonDefs.DictTryGet(dic, typeof(String), "PlayInfo", typeof(MakeGenericClass(Dictionary, String, Object)))
        if res then
            res, t.playIndex = CommonDefs.DictTryGet(info, typeof(String), "PlayIndex", typeof(UInt64))
            res, t.watchSceneId = CommonDefs.DictTryGet(info, typeof(String), "WatchSceneId", typeof(UInt64))
            res, t.data = CommonDefs.DictTryGet(info, typeof(String), "date", typeof(String))
            res, t.winTeamId = CommonDefs.DictTryGet(info, typeof(String), "WinTeamId",  typeof(UInt64))
            res, t.roundWinTeamId = CommonDefs.DictTryGet(info, typeof(String), "RoundWinTeamId",  typeof(MakeGenericClass(List, Object)))
            res, t.playStartTime = CommonDefs.DictTryGet(info, typeof(String), "PlayStartTime", typeof(Double))
            res, t.roundTime = CommonDefs.DictTryGet(info, typeof(String), "RoundTime", typeof(MakeGenericClass(List, Object)))
            res, t.stage = CommonDefs.DictTryGet(info, typeof(String), "Stage", typeof(UInt32))
            res, t.round = CommonDefs.DictTryGet(info, typeof(String), "Round", typeof(UInt32))
            res, t.group = CommonDefs.DictTryGet(info, typeof(String), "Group", typeof(UInt32))
            res, t.aliasName = CommonDefs.DictTryGet(info, typeof(String), "AliasName", typeof(String))
            res, t.competitionName = CommonDefs.DictTryGet(info, typeof(String), "PlayName", typeof(String))
            local list
            res, list = CommonDefs.DictTryGet(info, typeof(String), "TeamInfo", typeof(MakeGenericClass(List, Object)))
            t.teaminfos = {}
            if res then
                for j = 0, list.Count - 1 do
                    local teaminfo = list[j]
                    local item,playersList = {},nil
                    res, item.id = CommonDefs.DictTryGet(teaminfo, typeof(String), "Id", typeof(UInt64))
                    res, item.name = CommonDefs.DictTryGet(teaminfo, typeof(String), "Name", typeof(String))
                    res, item.leader = CommonDefs.DictTryGet(teaminfo, typeof(String), "Leader", typeof(UInt64))
                    res, playersList = CommonDefs.DictTryGet(teaminfo, typeof(String), "players", typeof(MakeGenericClass(List, Object)))

                    item.players = {}
                    if res then
                        for k = 0, playersList.Count - 1 do
                            local playerInfoDic = playersList[k]
                            local playerInfo = {}
                            res, playerInfo.id = CommonDefs.DictTryGet(playerInfoDic, typeof(String), "Id", typeof(UInt64))
                            res, playerInfo.level = CommonDefs.DictTryGet(playerInfoDic, typeof(String), "Level", typeof(UInt32))
                            res, playerInfo.name = CommonDefs.DictTryGet(playerInfoDic, typeof(String), "Name", typeof(String))
                            res, playerInfo.class = CommonDefs.DictTryGet(playerInfoDic, typeof(String), "Class", typeof(UInt32))

                            table.insert(item.players, playerInfo)
                        end
                    end
                    table.insert(t.teaminfos, item)
                end
            end
        end
        t.id = i + 1
        table.insert(self.m_VideoRecord[season], t)
    end
end

CLuaStarBiwuMgr.m_BattleDataWnd_AliasName = nil
CLuaStarBiwuMgr.m_BattleDataWnd_Season = 0
function CLuaStarBiwuMgr:OpenStarBiwuDataAnalysisWnd(aliasName, season)
    self.m_BattleDataWnd_AliasName = aliasName
    self.m_BattleDataWnd_Season = season
    CUIManager.ShowUI(CLuaUIResources.StarBiwuBattleDataWnd)
end

function CLuaStarBiwuMgr:GetHuiBaoBeiShu(listcount, resultTable, choice2BetList, currentBetPool)
    local maxvalue = 45
    local selectChoice2Bet = 0
    for j = 0, listcount - 1 do
        if resultTable[j+1] == 1 then
            selectChoice2Bet = bit.bor(selectChoice2Bet,  bit.lshift(1, listcount - 1 - j))
        end
    end
    local totalPool = StarBiWuShow_Setting.GetData().JiangCaiInit
    for i = 0, choice2BetList.Count - 1 do
          totalPool = totalPool + choice2BetList[i]
    end

    local value = choice2BetList[selectChoice2Bet]
    value = value <= 0 and maxvalue or (totalPool  * StarBiWuShow_Setting.GetData().JingCaiDiscount - value) / value
    return (value >= (maxvalue - 0.1)) and LocalString.GetString(">45倍") or
            SafeStringFormat3(LocalString.GetString("%.1f倍"), value)
end

CLuaStarBiwuMgr.m_JingCaiItemTipWndPos = nil
CLuaStarBiwuMgr.m_JingCaiItemTipWndTip = ""
function CLuaStarBiwuMgr:ShowStarBiwuJingCaiItemTipWnd(pos, tip)
  self.m_JingCaiItemTipWndPos = pos
  self.m_JingCaiItemTipWndTip = tip
  CUIManager.ShowUI(CLuaUIResources.StarBiwuJingCaiItemTipWnd)
end

CLuaStarBiwuMgr.m_IsInitStarBiwuPage = false
CLuaStarBiwuMgr.m_CurSeasonChampionInfo = nil
CLuaStarBiwuMgr.m_CurSeasonChampionMemberInfo = nil
function CLuaStarBiwuMgr:SyncStarBiwuCurrentSeasonChampionInfo(currentSeason, leanderId, teamName, rpcInfo)
  self.m_CurSeasonChampionInfo = {currentSeason, leanderId, teamName}
  self.m_CurSeasonChampionMemberInfo = {}
	CommonDefs.DictIterate(rpcInfo.Infos, DelegateFactory.Action_object_object(function(key, v)
    local member = {}
    member.MemberId = v.MemberId
    member.MemberName = v.MemberName
    member.MemberClass = v.MemberClass
    member.MemberGender = v.MemberGender
    member.MemberLever = v.MemberLever
    table.insert(self.m_CurSeasonChampionMemberInfo, member)
	end))
  g_ScriptEvent:BroadcastInLua("OnSyncStarBiwuCurrentSeasonChampionInfo",currentSeason, leanderId, teamName)
end

-- 明星表演赛冠军信息,可能会发多次,等待 SyncStarBiwuChampionEnd 到了说明发完了
function Gas2Gac.SyncStarBiwuChampion(championInfoUd)
	local list = MsgPackImpl.unpack(championInfoUd)
  for i = 0, list.Count - 1, 46 do
    local tbl = {}
    tbl["Index"] = list[i]
    tbl["LeaderId"] = list[i + 1]
    tbl["TeamName"] = list[i + 2]
    tbl["LeaderName"] = list[i + 3]

    local teamTbl = {}
    for j = 4, 45, 6 do
      local id = tonumber(list[i + j]) or 0
      if id > 0 then
        table.insert(teamTbl, {Id = id, Name = list[i + j + 1], Grade = list[i + j + 2], Class = list[i + j + 3], ServerName = list[i +j + 4], FeiShengGrade = list[i + j + 5]})
      end
    end
    tbl["TeamTable"] = teamTbl
    table.insert(CLuaStarBiwuMgr.m_HistoryDataTable, tbl)
  end
end

function Gas2Gac.SyncStarBiwuChampionEnd()
	g_ScriptEvent:BroadcastInLua("SyncStarBiwuChampionEnd")
end

function Gas2Gac.SyncSelectStarBiwuChampionNpc(engineId)
	if not CUIManager.IsLoaded(CLuaUIResources.StarBiwuStatueBuildWnd) then
		CUIManager.ShowUI(CLuaUIResources.StarBiwuStatueBuildWnd)
	end
end

-- 查询亲友团信息结果
function Gas2Gac.SyncLeaderQueryStarBiwuAudience(audienceUd)
  local list = MsgPackImpl.unpack(audienceUd)
  if not list then return end

  CLuaStarBiwuMgr.m_FriendList = {}
  for i = 0, list.Count - 1, 3 do
    local memberId = list[i]
    local memberName = list[i+1]
    local serverName = list[i+2]
    table.insert(CLuaStarBiwuMgr.m_FriendList, {PlayerId = memberId, PlayerName = memberName, ServerName = serverName})
  end
  CUIManager.ShowUI(CLuaUIResources.StarBiwuDeleteFriendWnd)
end

-- 查询战队返回结果,第一个是队长
function Gas2Gac.SyncQueryStarBiwuTeamInfo(teamUd)
  local list = MsgPackImpl.unpack(teamUd)

  for i = 0, list.Count - 1, 6 do
    local memberId = list[i]
    local name = list[i+1]
    local grade = list[i+2]
    local class = list[i+3]
    local currentXianshenGrade = list[i+4]
    local gender = list[i+5]
  end

end

-- Gac2Gas.LeaderRmStarBiwuAudienceFromTeam 返回的RPC
-- 移除亲友团成员成功后的确认消息
function Gas2Gac.SyncLeaderRmStarBiwuAudicenDone(playerId, playerName)
end

-- 被邀请成为战队成员,弹出二次确认框
function Gas2Gac.ShowInviteToStarBiwuCombatTeamConfirm(leaderId, leaderName)

end

function Gas2Gac.CreateStarBiwuZhanDuiSuccess()
  g_ScriptEvent:BroadcastInLua("CreateStarBiwuZhanDuiSuccess")
end

function Gas2Gac.ReplyStarBiwuSelfPersonalInfo(fromCharacterId, fromServerName, fromCharacterName, goodAtClass, locationId, onlineTimeId, canZhiHui, mark, zhanduiId, loginCount)
end

CLuaStarBiwuMgr.m_ZhanduiTable = {}
CLuaStarBiwuMgr.m_ZhanduiFlagTable = {}
CLuaStarBiwuMgr.m_HasZhanDui = false


function Gas2Gac.ReplyStarBiwuZhanDuiListBegin()
  CLuaStarBiwuMgr.m_ZhanduiTable = {}
  CLuaStarBiwuMgr.m_ZhanduiFlagTable = {}
end

function Gas2Gac.ReplyStarBiwuZhanDuiList(dataUD)

  local zhanduiList = CStarBiwuZhanduiListRPC()
	zhanduiList:LoadFromString(dataUD)
	
	if zhanduiList.Infos then
		CommonDefs.DictIterate(zhanduiList.Infos, DelegateFactory.Action_object_object(function(key, v)
		  local zhandui = {}
		  
		  zhandui.m_Id = v.ZhanduiId
		  zhandui.m_Name = v.ZhanDuiTitle
		  zhandui.m_Slogan = v.ZhanDuiKouHao
		  zhandui.m_Capacity = v.Capacity
		  zhandui.m_HasApplied = v.IsApply
		  zhandui.m_StarScore = v.StarScore

		  local tbl = {}
		  CommonDefs.DictIterate(v.ZhanDuiMembers, DelegateFactory.Action_object_object(function(key_2, v_2)
        table.insert(tbl, v_2.MemberClass)
      end))

      zhandui.m_Member = Table2Array(tbl, MakeArrayClass(UInt32))

      table.insert(CLuaStarBiwuMgr.m_ZhanduiTable, zhandui)
      table.insert(CLuaStarBiwuMgr.m_ZhanduiFlagTable, v.IsRetain)
    end))
  end
end

function Gas2Gac.ReplyStarBiwuZhanDuiListEnd(selfZhanDuiId, page, totalPage, source)
  CLuaStarBiwuMgr.m_MySelfZhanduiId = selfZhanDuiId
  CLuaStarBiwuMgr.m_HasZhanDui = selfZhanDuiId > 0
  g_ScriptEvent:BroadcastInLua("ReplyStarBiwuZhanDuiList", selfZhanDuiId, page, totalPage, source)
end

function Gas2Gac.PlayerRequestJoinStarBiwuZhanDui()
end

CLuaStarBiwuMgr.m_MySelfZhanduiId = 0
CLuaStarBiwuMgr.m_CurrentZhanduiId = 0
CLuaStarBiwuMgr.m_CurrentZhanduiName = ""
CLuaStarBiwuMgr.m_CurrentZhanduiSlogan = ""
CLuaStarBiwuMgr.m_HasApplyCurrentZhandui = false
CLuaStarBiwuMgr.m_MyZhanduiTip = ""
CLuaStarBiwuMgr.m_IsBiWuChuZhanSetting = 0
CLuaStarBiwuMgr.m_MemberInfoTable = {}
CLuaStarBiwuMgr.m_IsTeamLeader = false
CLuaStarBiwuMgr.m_ChuZhanInfoList = nil
CLuaStarBiwuMgr.BattleCount = 6
CLuaStarBiwuMgr.m_HideMemberFlag = 0

function Gas2Gac.ReplyStarBiwuZhanDuiInfo(selfZhanDuiId, zhanduiId, memberData, chuzhanData, title, kouhao, needClass, needZhiHui, isApply, tips, hideFlag)

    local memberData = MsgPackImpl.unpack(memberData)
  if not memberData then return end
  CLuaStarBiwuMgr.m_MemberInfoTable = {}
  CLuaStarBiwuMgr.m_IsTeamLeader = false
  local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
  for i = 0, memberData.Count - 1, 5 do
    local member = CQMPKMemberInfo()
    member.m_Id = memberData[i]
    if i == 0 and member.m_Id == myId then CLuaStarBiwuMgr.m_IsTeamLeader = true end
    member.m_Name = memberData[i + 1]
    member.m_Class = CommonDefs.ConvertIntToEnum(typeof(EnumClass), memberData[i + 2])
    member.m_Gender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), memberData[i + 3])
    member.m_Grade = memberData[i + 4]
    table.insert(CLuaStarBiwuMgr.m_MemberInfoTable, member)
  end

  local chuzhanData = MsgPackImpl.unpack(chuzhanData)
  local tbl = {}
  if chuzhanData then
     for i = 0, chuzhanData.Count - 1 do
       table.insert(tbl, chuzhanData[i])
     end
  end
  for i = #tbl, CLuaStarBiwuMgr.BattleCount - 1 do
    table.insert(tbl, 0)
  end
  CLuaStarBiwuMgr.m_ChuZhanInfoList = Table2Array(tbl, MakeArrayClass(UInt32))

  CLuaStarBiwuMgr.m_MySelfZhanduiId = selfZhanDuiId
  CLuaStarBiwuMgr.m_CurrentZhanduiId = zhanduiId
  CLuaStarBiwuMgr.m_CurrentZhanduiName = title
  CLuaStarBiwuMgr.m_CurrentZhanduiSlogan = kouhao
  CLuaStarBiwuMgr.m_HasApplyCurrentZhandui = isApply
  CLuaStarBiwuMgr.m_MyZhanduiTip = tips
  CLuaStarBiwuMgr.m_IsBiWuChuZhanSetting = 0
  CLuaStarBiwuMgr.m_HideMemberFlag = hideFlag

  g_ScriptEvent:BroadcastInLua("ReplyStarBiwuZhanDuiInfo")
end

function Gas2Gac.RequestJoinStarBiwuZhanDuiSuccess(zhanduiId)
  g_ScriptEvent:BroadcastInLua("RequestJoinStarBiwuZhanDuiSuccess", zhanduiId)
end

function Gas2Gac.ReplyStarBiwuZhanDuiApplyList(totalPage, dataUD)
  local list=MsgPackImpl.unpack(dataUD)
  if not list then return end

  local requestPlayers={}
  for i=1,list.Count,9 do
      local player={
          m_Id = tonumber(list[i-1]),
          m_Name = tostring(list[i]),
          m_Clazz = tonumber(list[i+1]),
          m_Capacity = tonumber(list[i+2]),
          m_Xiuwei = tonumber(list[i+3]),
          m_Xiulian = tonumber(list[i+4]),
          m_Score =tostring(list[i+5]),
          m_Server = tostring(list[i+6]),
          m_StarScore = tonumber(list[i+7]),
      }
      table.insert( requestPlayers,player )
  end
  CLuaStarBiwuMgr.m_RequestZhanDuiMemberList = requestPlayers
  g_ScriptEvent:BroadcastInLua("ReplyStarBiwuZhanDuiApplyList",requestPlayers)
end

function Gas2Gac.ReplyStarBiwuQieCuoTeamList(selfQieCuoId, page, totalPage, dataUD)
  local list=MsgPackImpl.unpack(dataUD)
  if not list then return end

  local teamList={}
  for i=1,list.Count,4 do
      local rawMembers=list[i+2]
      local members={}
      for j=1,rawMembers.Count do
          table.insert( members,tonumber(rawMembers[j-1]) )
      end
      local team={
          m_Id = tonumber(list[i-1]),
          m_LeaderId = tonumber(list[i]),
          m_LeaderName = tostring(list[i+1]),
          m_Member = members
      }
      table.insert( teamList,team )
  end
  g_ScriptEvent:BroadcastInLua("ReplyStarBiwuQieCuoTeamList", selfQieCuoId, page, totalPage,teamList)
end

function Gas2Gac.SignUpStarBiwuQieCuoSuccess(qieCuoId)
  g_ScriptEvent:BroadcastInLua("SignUpStarBiwuQieCuoSuccess")
end

function Gas2Gac.ReplyStarBiwuQieCuoTeam(selfQieCuoId, memberId, dataUD)
  Gas2Gac.ReplyStarBiwuQieCuoTeamList(selfQieCuoId,1,1,dataUD)
end

-- 被邀请成为战队成员
function Gas2Gac.StarBiwuZhanDuiBeInvite(zhanduiId, inviterName, zhanduiName)
  MessageWndManager.ShowDefaultConfirm(SafeStringFormat3(LocalString.GetString("<link player=%d,%s>邀请你加入[fcfc22]跨服明星赛战队[-]"), zhanduiId, inviterName), -1, DelegateFactroy.Action(function()
    Gac2Gas.StarBiwuZhanDuiConfirmBeInvite(zhanduiId)
  end), nil, nil, nil)
end

-- 打开出战信息面板
CLuaStarBiwuMgr.m_StarBiwuZhanDuiSettingWndCurRound = 0 
function Gas2Gac.OpenStarBiwuChuZhanInfoWnd(zhanduiId, zhanduiName, memberInfoUd, chuzhanInfoUd, currentRound)
  CLuaStarBiwuMgr.m_StarBiwuZhanDuiSettingWndCurRound = currentRound
  local memberData = MsgPackImpl.unpack(memberInfoUd)
  if not memberData then return end
  CLuaStarBiwuMgr.m_MemberInfoTable = {}
  CLuaStarBiwuMgr.m_IsTeamLeader = false
  local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
  for i = 0, memberData.Count - 1, 5 do
    local member = CQMPKMemberInfo()
    member.m_Id = memberData[i]
    if i == 0 and member.m_Id == myId then CLuaStarBiwuMgr.m_IsTeamLeader = true end
    member.m_Name = memberData[i + 1]
    member.m_Class = CommonDefs.ConvertIntToEnum(typeof(EnumClass), memberData[i + 2])
    member.m_Gender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), memberData[i + 3])
    member.m_Grade = memberData[i + 4]
    table.insert(CLuaStarBiwuMgr.m_MemberInfoTable, member)
  end

  local chuzhanData = MsgPackImpl.unpack(chuzhanInfoUd)
  local tbl = {}
  if chuzhanData then
     for i = 0, chuzhanData.Count - 1 do
       table.insert(tbl, chuzhanData[i])
     end
  end
  for i = #tbl, CLuaStarBiwuMgr.BattleCount - 1 do
    table.insert(tbl, 0)
  end
  CLuaStarBiwuMgr.m_ChuZhanInfoList = Table2Array(tbl, MakeArrayClass(UInt32))
  CLuaStarBiwuMgr.m_IsBiWuChuZhanSetting = 1
  CUIManager.ShowUI(CLuaUIResources.StarBiwuZhanDuiSettingWnd)
end

-- 设置出战信息成功
function Gas2Gac.StarBiwuSetChuZhanInfoSuccess()
end

-- 设置称号和口号成功
function Gas2Gac.UpdateStarBiwuZhanDuiTitleAndKouHaoSuccess(title, kouhao)
  CLuaStarBiwuMgr.m_CurrentZhanduiName = title
  CLuaStarBiwuMgr.m_CurrentZhanduiSlogan = kouhao
  g_ScriptEvent:BroadcastInLua("UpdateStarBiwuZhanDuiTitleAndKouHaoSuccess")
end

CLuaStarBiwuMgr.m_CurrentBattleStatus = nil
CLuaStarBiwuMgr.m_IsGameVideoBattleStatus = false
-- 副本中查看战况
function Gas2Gac.ReplyStarBiWuZhanKuang(attackName, defendName, attackWinCount, defendWinCount, finishRound, playRound, dataUD, selfForce, aliasName, stage, group, matchIdx, jieshu)
  local battle={
      m_AttackName = attackName,
      m_DefendName = defendName,
      m_AttackWinCount = attackWinCount,
      m_DefendWinCount = defendWinCount,
      m_FinishedRound = finishRound,
      m_PlayRound = playRound,
      m_SelfForce = selfForce,
      m_Status = {},
  }
  battle.m_JieShu = jieshu
  CLuaStarBiwuMgr.m_BattleDataWnd_AliasName = aliasName
  -- local statusData=MsgPackImpl.unpack(data[6])
  local statusData = MsgPackImpl.unpack(dataUD)
  for i=0,statusData.Count-1,7 do
      local status={
          m_Dps1 = statusData[i],
          m_Kill1 = statusData[i + 2],
          m_Name1 = {},
          m_Id1 = {},
          m_Class1 = {},

          m_Dps2 = statusData[i+3],
          m_Kill2 = statusData[i + 5],
          m_Name2 = {},
          m_Id2 = {},
          m_Class2 = {},

          m_Win = statusData[i+6],
          m_IsInBattle = battle.m_PlayRound==i/5+1,
      }
      local info=statusData[i+1]
      for j=0,info.Count-1,3 do
          table.insert( status.m_Id1,info[j] )
          table.insert( status.m_Name1,info[j+1] )
          table.insert( status.m_Class1,info[j+2] )
      end
      local info=statusData[i+4]
      for j=0,info.Count-1,3 do
          table.insert( status.m_Id2,info[j] )
          table.insert( status.m_Name2,info[j+1] )
          table.insert( status.m_Class2,info[j+2] )
      end

      table.insert( battle.m_Status,status )
  end
  CLuaStarBiwuMgr.m_CurrentBattleStatus=battle
  CLuaStarBiwuMgr.m_IsGameVideoBattleStatus = false
  g_ScriptEvent:BroadcastInLua("ReplyStarBiWuZhanKuang")
end

CLuaStarBiwuMgr.m_FightData = {}
-- 比武详细战斗信息
function Gas2Gac.ReplyStarBiwuDetailFightData(round, attackData, defendData, playStage, attackName, defendName)
  local data1={}
  local data2={}
  local list1=MsgPackImpl.unpack(attackData)
  local list2=MsgPackImpl.unpack(defendData)

  local parse=function(list,out)
      for i=2,list.Count,5 do
          local playerId=list[i-1]
          local clazz=list[i]
          local name=list[i+1]
          local fightdata=nil
          local rawFightData=list[i+2]
          --如果有数据
          fightdata={
              [1]=rawFightData[0],
              [2]=rawFightData[1],
              [3]=rawFightData[2],
              [4]=rawFightData[3],
              [5]=rawFightData[4],
              [6]=rawFightData[5],
              [7]=rawFightData[6],
              [8]=rawFightData[9]
          }
          local skilldata={}
          local rawSkillData=list[i+3]
          for i=1,rawSkillData.Count,2 do
              table.insert( skilldata, rawSkillData[i-1] )
              table.insert( skilldata, rawSkillData[i] )
          end
          table.insert( out,{playerId=playerId,clazz=clazz,name=name,fightdata=fightdata,skilldata=skilldata} )
      end
  end
  parse(list1,data1)
  parse(list2,data2)

  CLuaStarBiwuMgr.m_FightData[round]={[1]=data1,[2]=data2,name1=attackName,name2=defendName}


  g_ScriptEvent:BroadcastInLua("ReplyStarBiwuDetailFightData",round, attackData, defendData)
end

-- 所有成员的战斗信息
CLuaStarBiwuMgr.m_ZhanduiFightData = {}
function Gas2Gac.ReplyStarBiwuAllMemberFightData(playStage, playIndex, fightData, isWin)
  local list=MsgPackImpl.unpack(fightData)
  if not list then return end

  CLuaStarBiwuMgr.m_ZhanduiFightData={}
  for i=0,list.Count-1,20 do
      local fight={
          m_PlayerId = list[i],
          m_PlayerName = list[i+1],
          m_PlayerClass = list[i+2],
          m_PlayerGender = list[i+3],

          m_Dps = list[i+4],
          m_BestDps = list[i+5],
          m_Heal = list[i+6],
          m_BestHeal = list[i+7],
          m_UnderDamage = list[i+8],
          m_BestUnderDamage = list[i+9],
          m_KillNum = list[i+10],
          m_BestKillNum = list[i+11],
          m_Ctrl = list[i+12],
          m_BestCtrl = list[i+13],
          m_RmCtrl = list[i+14],
          m_BestRmCtrl = list[i+15],
          m_Die = list[i+16],
          m_BestDie = list[i+17],
          m_Relive = list[i+18],
          m_BestRelive = list[i+19],
      }
      table.insert( CLuaStarBiwuMgr.m_ZhanduiFightData, fight )
  end

  --CQuanMinPKMgr.Inst:SetCurrentFightName(playStage, playIndex)

  g_ScriptEvent:BroadcastInLua("ReplyStarBiwuAllMemberFightData",playStage,playIndex)
end

-- 玩法结束
function Gas2Gac.StarBiWuPlayEnd()
  CLuaStarBiwuMgr.m_IsGameVideoBattleStatus = false
  CUIManager.CloseUI(CLuaUIResources.StarBiwuBattleDataWnd)
  CUIManager.ShowUI(CLuaUIResources.StarBiwuCurrentBattleStatusWnd)
    CUIManager.CloseUI(CLuaUIResources.CommonKillInfoTopWnd)
  g_ScriptEvent:BroadcastInLua("StarBiWuPlayEnd")

  --比赛结束，请求所有的战斗数据
  for i=1,5 do
      if not CLuaStarBiwuMgr.m_FightData[i] then
          Gac2Gas.QueryStarBiwuDetailFightData(i)
      end
  end
end

CLuaStarBiwuMgr.m_PlayStage = nil
function Gas2Gac.SyncStarBiwuFightPlayData(playStage)
  CLuaStarBiwuMgr.m_PlayStage=playStage
end

function Gas2Gac.StarBiwuExcellentDataShare(playStage, tp, value)
end

CLuaStarBiwuMgr.m_HasMyScore = false
CLuaStarBiwuMgr.m_ScoreRankList = {}
function Gas2Gac.ReplyStarBiwuJiFenSaiRank(rankData, zhanduiId, rank, title, memberData, JiFen, avgWinTime)
  local data = MsgPackImpl.unpack(rankData)
  if not data then return end
  CLuaStarBiwuMgr.m_HasMyScore = false
  CLuaStarBiwuMgr.m_ScoreRankList = {}
  if zhanduiId > 0 then
    CLuaStarBiwuMgr.m_HasMyScore = true
    local myScore = CQMPKScore()
    myScore.m_Id = zhanduiId
    myScore.m_Score = JiFen
    myScore.m_Rank = rank
    myScore.m_Name = title
    myScore.m_Time = avgWinTime
    local mem = MsgPackImpl.unpack(memberData)
    if mem then
      local tbl = {}
      for i = 0, mem.Count - 1 do
        table.insert(tbl, mem[i])
      end
      myScore.m_Member = Table2Array(tbl, MakeArrayClass(UInt32))
    end
    table.insert(CLuaStarBiwuMgr.m_ScoreRankList, myScore)
  end

  local rankIdx = 0
  for i = 0, data.Count - 1, 5 do
    rankIdx = rankIdx + 1
    local score = CQMPKScore()
    score.m_Id = data[i]
    score.m_Name = data[i + 1]
    score.m_Rank = rankIdx
    score.m_Score = data[i + 3]
    score.m_Time = data[i + 4]
    local mem = data[i + 2]
    if mem then
      local tbl = {}
      for j = 0, mem.Count - 1 do
        table.insert(tbl, mem[j])
      end

      score.m_Member = Table2Array(tbl, MakeArrayClass(UInt32))
    end
    table.insert(CLuaStarBiwuMgr.m_ScoreRankList, score)
  end
  CUIManager.ShowUI(CLuaUIResources.StarBiwuScoreWnd)
end

function Gas2Gac.ReplyStarBiwuJiFenSaiWatchList(selfZhanduiId, dataUD)
  CLuaStarBiwuMgr.ReplyStarBiwuJiFenSaiWatchList(selfZhanduiId, dataUD)
end

function Gas2Gac.ReplyStarBiwuXiaoZuSaiSaiOverView(queryType, group, round, xiaozusaiStage, queryGroup, group1Ud, group2Ud, groud3Ud, group4Ud, extraUd)
    -- queryType: 1-> 排行, 2-> 战况, 这个数据是客户端 Gac2Gas.QueryStarBiwuXiaoZuSaiOverview(queryType) 发给服务端,现在带下来的
  -- group: 当前进行到第几组,取值0-4
  -- round: group的当前轮次,尚未开始的话,这里是0
  -- queryGroup: 当前请求的是哪一组的数据
  -- xiaozusaiStage: 状态标记,详细见服务端定义的状态 EnumStarBiwuFightXZSStage ,结合 round 控制页面标题的信息
  -- group1Ud: 甲组的情况,解开后是list,排行的话,数据组织是服务端的 GetSortedZhanDuiTblByGroup 函数, 战况对应服务端函数GetRoundMatchStatus
  -- extraUd: 预留给patch或者后续需求,暂时没有传任何数据
	g_ScriptEvent:BroadcastInLua("ReplyStarBiwuXiaoZuSaiSaiOverView", queryType, group, round, xiaozusaiStage, queryGroup, group1Ud, group2Ud, groud3Ud, group4Ud, extraUd)
end

function Gas2Gac.ReplyStarBiwuZongJueSaiOverView(index, zhanduiId, title, memberData, isFinish)
	local list = MsgPackImpl.unpack(memberData)
	if not list then return end
	local memberInfos = {}
	for i = 0, list.Count - 1 do
		local info = list[i]
		local memberId = info[0]
		local serverName = info[1]
		local fakeAppearance = CreateFromClass(CPropertyAppearance)
		fakeAppearance:LoadFromString(info[2], EnumClass.Undefined, EnumGender.Undefined)
		local memberName = info[3]
		table.insert(memberInfos, {
			memberId = memberId,
			memberName = memberName,
			serverName = serverName,
			fakeAppearance = fakeAppearance,
			})
	end
	if CUIManager.IsLoaded(CLuaUIResources.StarBiwuTopFourWnd) then
		g_ScriptEvent:BroadcastInLua("ReplyStarBiwuZongJueSaiOverView", index, zhanduiId, title, memberInfos, isFinish)
	else
		CUIManager.ShowUI(CLuaUIResources.StarBiwuTopFourWnd)
	end
end

CLuaStarBiwuMgr.m_FinalMatchRecordList = {}
function Gas2Gac.ReplyStarBiwuZongJueSaiWatchList(playIndex, zhanduiId1, zhanduiTitle1, zhanduiId2, zhanduiTitle2, winZhanDuiId, start)
	CLuaStarBiwuMgr.m_FinalMatchRecordList = {}
	if zhanduiId1>0 then
			local record={
					m_Zhandui1 = zhanduiId1,
					m_TeamName1 = zhanduiTitle1,
					m_Zhandui2 = zhanduiId2,
					m_TeamName2 = zhanduiTitle2,
					m_Win = winZhanDuiId,
					m_IsMatching = start>0
			}
			table.insert(CLuaStarBiwuMgr.m_FinalMatchRecordList, record)
	end
	g_ScriptEvent:BroadcastInLua("ReplyStarBiwuZongJueSaiWatchList", playIndex)
end

-- 设置是否隐藏战队信息成功的返回,hideFlag为1表示隐藏
function Gas2Gac.UpdateStarBiwuZhanDuiPublishSuccess(hideFlag)
	g_ScriptEvent:BroadcastInLua("UpdateStarBiwuZhanDuiPublishSuccess", hideFlag)
end

-- 竞猜查询当前竞猜的返回
-- jingcaiShutdownStatus: 枚举值 {1: 默认值，建议用之前的文本, 2:比赛中 3:结果已经出来了,此时rightChice有效,否则rightChice为-1}
function Gas2Gac.StarBiwuSendCurrentJingCaiItem(jingcaiItemTblUd, canBet, targetDate, targetStageName, maxBetValue, currentBetPool, jingcaiPhaseNum, phaseBet, canGetReward, matchTblUd, choice2BetValueUd, singleChoiceRateUd, jingcaiShutdownStatus, rightChice, showResultUd, jingcaiquanCount)

  local matchInfoList = g_MessagePack.unpack(matchTblUd)
  -- 如果对阵表已经出来的话，这里[1-4]分别是每个竞猜项的两个队伍名,对阵信息尚未出现的话,是空字符串

  local jingcaiItemList = MsgPackImpl.unpack(jingcaiItemTblUd)
  if not jingcaiItemList then return end

  -- jingcaiItemList[0-3] 中的数据是 当前竞猜的四个竞猜项

  -- canBet 是否能参与竞猜,这个控制最后一行字的显示,能就显示竞猜按钮,否则显示本期竞猜结束的红色字
  -- targetDate 比赛日,字符串
  -- targetStageName 赛程
  -- maxBetValue 投注上限
  -- currentBetPool 奖池

  -- phaseBet 当前期投入的金额
  -- canGetReward 可以领取的奖励

  -- jingcaiPhaseNum 当前第几期竞猜,这个数据是给后面的上行RPC(StarBiwuRequestJingCaiBet)用的,服务端用以校验数据,避免界面不刷新引发的错误


  -- 这里 0-15 共16个数据分别对应 0000-1111这16个选项的当前竞猜额(分母,注意可能为0)
  local choice2BetList = MsgPackImpl.unpack(choice2BetValueUd)
  if not choice2BetList then return end
  
  -- 这里 0-3 共4个数据分别对应4个竞猜项选"是"的玩家人数百分比(小数,区间[0-1])
  local singleChoiceList = MsgPackImpl.unpack(singleChoiceRateUd)
  if not singleChoiceList then return end

  local resultList = g_MessagePack.unpack(showResultUd)
	if not resultList then return end

	g_ScriptEvent:BroadcastInLua("StarBiwuSendCurrentJingCaiItem", jingcaiItemTblUd, canBet, targetDate, targetStageName, maxBetValue, currentBetPool, jingcaiPhaseNum,phaseBet,canGetReward,matchInfoList, choice2BetList, singleChoiceList,jingcaiShutdownStatus, rightChice, resultList, jingcaiquanCount)
end

-- 竞猜查询历史记录的返回
function Gas2Gac.StarBiwuSendJingCaiRecord(jingcaiRecordForClientUd, eachDataLen, canGetReward, jingcaiquanCount)
  if not jingcaiRecordForClientUd then return end
  g_ScriptEvent:BroadcastInLua("StarBiwuSendJingCaiRecord",jingcaiRecordForClientUd, eachDataLen,canGetReward,jingcaiquanCount)

  -- canGetReward 可领取的奖励
end

-- 竞猜查询竞猜记录详情的返回
function Gas2Gac.StarBiwuSendJingCaiRecordDetail(jingcaiItemTblUd, selfChoiceTblUd, rightChoiceTblUd, jingcaiStage, targetDate, matchTblUd, phaseBetPool, choice2BetValueUd, showResultUd)
  local jingcaiItemList = MsgPackImpl.unpack(jingcaiItemTblUd)
  if not jingcaiItemList then return end -- 这里包含了4个竞猜项

  local selfChoiceList = MsgPackImpl.unpack(selfChoiceTblUd)
  if not selfChoiceList then return end -- 这里[0-3]分别是自己的选择

  local rightChoiceList = MsgPackImpl.unpack(rightChoiceTblUd)
  if not rightChoiceList then return end -- 这里[0-3]分别是正确的选择

  -- targetDate -> 这是竞猜比赛日,来源就是策划配表中的比赛日
  local matchInfoList = g_MessagePack.unpack(matchTblUd)
  -- 如果对阵表已经出来的话，这里[0-1]分别是对阵的两个队伍名,对阵信息尚未出现的话,是空字符串

  g_ScriptEvent:BroadcastInLua("StarBiwuSendJingCaiRecordDetail",jingcaiItemList, selfChoiceList,rightChoiceList, jingcaiStage, targetDate, matchInfoList, phaseBetPool, MsgPackImpl.unpack(choice2BetValueUd))
end

-- 竞猜是否成功
function Gas2Gac.StarBiwuJingCaiBetResult(bSuccess, phaseBet)
  -- phaseBet 当前阶段已经投入的金额

  g_ScriptEvent:BroadcastInLua("StarBiwuJingCaiBetResult",phaseBet)
end

function Gas2Gac.StarBiwuJingCaiGetRewardResult(bSuccess, leftReward)
  -- bSuccess bool值,是否成功
  -- leftReward 剩余可领取的奖励
  g_ScriptEvent:BroadcastInLua("StarBiwuJingCaiGetRewardResult",bSuccess,leftReward)
end

-- 第四届明星赛的决赛对阵信息
function Gas2Gac.ReplyStarBiwuZongJueSaiMatch(matchDataUd, currentMatchPlayIdx, selfZhanduiId, syncType, canGetReward)
  g_ScriptEvent:BroadcastInLua("ReplyStarBiwuZongJueSaiMatch", currentMatchPlayIdx, selfZhanduiId, matchDataUd, syncType, canGetReward)
  -- syncType 表示本次同步数据的目的, 0 表示客户端查询决赛对阵的返回, 1表示查询决赛竞猜概况的返回
end

-- 当前是否处于竞猜中,给盛放的明星赛活动面板用的, true 表示处于竞猜中
function Gas2Gac.SyncCheckStarBiwuInJingCaiResult(bInJingCai)
  g_ScriptEvent:BroadcastInLua("SyncCheckStarBiwuInJingCaiResult",bInJingCai)
end

CLuaStarBiwuMgr.FinalJingCaiData = {}
-- 查询决赛竞猜项的返回  StarBiwuQueryJueSaiJingCaiItem -> StarBiwuSendJuesaiJingCaiItem
function Gas2Gac.StarBiwuSendJuesaiJingCaiItem(jingcaiItemTblUd, canBet, targetDate, targetStageName, maxBetValue, currentBetPool, jingcaiPhaseNum, phaseBet, canGetReward, matchTblUd)

  local matchInfoList = g_MessagePack.unpack(matchTblUd)
  -- 如果对阵表已经出来的话，这里[0-1]分别是对阵的两个队伍名,对阵信息尚未出现的话,是空字符串

  local jingcaiItemList = MsgPackImpl.unpack(jingcaiItemTblUd)
  if not jingcaiItemList then return end

  -- jingcaiItemList[0-3] 中的数据是 当前竞猜的四个竞猜项

  -- canBet 是否能参与竞猜,这个控制最后一行字的显示,能就显示竞猜按钮,否则显示本期竞猜结束的红色字
  -- targetDate 比赛日,字符串
  -- targetStageName 赛程
  -- maxBetValue 投注上限
  -- currentBetPool 奖池

  -- phaseBet 当前期投入的金额
  -- canGetReward 可以领取的奖励

  -- jingcaiPhaseNum 当前第几期竞猜,这个数据是给后面的上行RPC(StarBiwuRequestJingCaiBet)用的,服务端用以校验数据,避免界面不刷新引发的错误
	CLuaStarBiwuMgr.FinalJingCaiData = {jingcaiItemTblUd, canBet, targetDate, targetStageName, maxBetValue, currentBetPool, jingcaiPhaseNum, phaseBet, canGetReward, matchTblUd}
	CUIManager.ShowUI(CLuaUIResources.StarBiwuFinalJingCaiWnd)
end

function Gas2Gac.SyncStopStarBiwuFight(requestPlayerName, beginTime, agreeUd, disagreeUd, choiceUd, totalCount)
  -- requestPlayerName  -> 发起者名字
  -- beginTime -> 开始发起的时间,
  -- 注意断线重连时有可能服务端会下发很早之前发起的信息,客户端需要判断是否过期,过期的就别显示了

  CLuaStarBiwuMgr.requestPlayerName = requestPlayerName
  CLuaStarBiwuMgr.voteBeginTime = beginTime

  -- 已经同意的角色id
  local agreeList     = MsgPackImpl.unpack(agreeUd)
  local disagreeList  = MsgPackImpl.unpack(disagreeUd)
  local choiceList    = MsgPackImpl.unpack(choiceUd)

  CLuaStarBiwuMgr.agreeList = {}
  CLuaStarBiwuMgr.disagreeList = {}
  CLuaStarBiwuMgr.choiceList = {}

  for i=0,agreeList.Count-1,1 do

    CLuaStarBiwuMgr.agreeList[i] = agreeList[i]
  end

  for i=0,disagreeList.Count-1,1 do

    CLuaStarBiwuMgr.disagreeList[i] = disagreeList[i]
  end
  
  for i=0,choiceList.Count-1,1 do

    CLuaStarBiwuMgr.choiceList[i] = choiceList[i]
  end

  if not CUIManager.IsLoaded(CLuaUIResources.StarBiwuStopVoteWnd) then
    CUIManager.ShowUI(CLuaUIResources.StarBiwuStopVoteWnd)
    CLuaStarBiwuMgr.isVoteEnd = false
  else
    g_ScriptEvent:BroadcastInLua("SyncStopStarBiwuFight")
  end
end

-- 是否在观战界面添加竞猜按钮,true表示要加一个
-- ccid用以和SyncZhiBoStatus下发的ccid比较
function Gas2Gac.SyncAttachJingCaiButton(bShow, ccidListUd)
    local ccidListList = MsgPackImpl.unpack(ccidListUd)
    if not ccidListList then
      return
    end

    local bMatch = false
    local info = CCCChatMgr.Inst:GetCurrentStation_V2()

    if info then
        local currentCcid = tonumber(info.liveInfo.ccid)
        local currentCid = tonumber(info.cid)
        for i = 0,ccidListList.Count-1,1 do
            local id = tonumber(ccidListList[i])
            if id == 0 or id == currentCcid or id == currentCid then
                bMatch = true
                break
            end
        end
    end

    if bMatch then
        g_ScriptEvent:BroadcastInLua("OnCCLiveWndJingCaiButtonUpdateState", bShow)
    end
end

-- 明星赛击杀信息,前面是击杀者,后面是被杀者
function Gas2Gac.SyncPvpKillInfo(player1Ud, player2Ud)
    local player1Data = MsgPackImpl.unpack(player1Ud) -- 杀人的人
    local player2Data = MsgPackImpl.unpack(player2Ud) -- 被杀的人
    if player1Data and player2Data then
        local player1Id, player1Gender, player1Class, winnerName, player1Force = player1Data[0], player1Data[1], player1Data[2], player1Data[3], player1Data[4]
        local player2Id, player2Gender, player2Class, loserName, player2Force = player2Data[0], player2Data[1], player2Data[2], player2Data[3], player2Data[4]
        local winerPortrait = CUICommonDef.GetPortraitName(player1Class, player1Gender, -1)
        local loserPortrait = CUICommonDef.GetPortraitName(player2Class, player2Gender, -1)
        local winnerIsLeft = player1Force == (CLuaStarBiwuMgr:IsInStarBiwu() and 1 or 0)
        g_ScriptEvent:BroadcastInLua("KillInfoUpdate",winnerIsLeft, winerPortrait, loserPortrait, winnerName, loserName)
    end
end

function Gas2Gac.SyncPlayReport(id, alias, designPlayId, fightTime, teamInfoUd, playerInfoUd, reportInfoUd, roundScoreUd, chuzhanInfoUd, extraUd)
    local data = {}
    -- 队伍信息
    -- teamId --> {teamName = name, teamForce = force, members = [playerId1, playerId2]}
    local teamInfo = {}

    -- 玩家信息
    -- playerId --> {name = pName, class = pcls, teamId = tId, playerForce = force}
    local playerInfo = {}

    -- 战报
    -- 每一轮有5个report (5句话)
    -- round --> {
    --	{reportId = reportId, argList = argList},
    --	{reportId = reportId, argList = argList},
    -- }
    local reportInfo = {}

    -- 比分信息
    -- round --> {force = score}
    local scoreInfo = {}

    local teamList = MsgPackImpl.unpack(teamInfoUd)
    local step = math.ceil(teamList.Count / 2)
    for i = 1, teamList.Count, step do
        local teamId 	= teamList[i-1]
        local teamName 	= teamList[i]
        local teamForce = teamList[i+1]

        teamInfo[teamId] = {}
        teamInfo[teamId].teamName = teamName
        teamInfo[teamId].teamForce = teamForce
        teamInfo[teamId].members = {}
        if teamForce == 1 then
            data.leftTeamName = teamName
        else
            data.rightTeamName = teamName
        end
    end

    local playerList = MsgPackImpl.unpack(playerInfoUd)
    step = 4
    data.leftTeamMemberClasses = {}
    data.rightTeamMemberClasses = {}
    for i = 1, playerList.Count, step do
        local playerId 		= playerList[i-1]
        local playerName 	= playerList[i]
        local playerClass 	= playerList[i+1]
        local teamId 		= playerList[i+2]

        playerInfo[playerId] = {}

        local playerForce = 0
        if teamInfo[teamId] then
            table.insert(teamInfo[teamId].members, playerId)
            playerForce = teamInfo[teamId].teamForce
        end

        playerInfo[playerId].name 			= playerName
        playerInfo[playerId].playerClass 	= playerClass
        playerInfo[playerId].playerForce 	= playerForce
        playerInfo[playerId].teamId 		= teamId
        table.insert(playerForce == 0 and data.leftTeamMemberClasses or data.rightTeamMemberClasses, playerClass)
    end

    local reportList = MsgPackImpl.unpack(reportInfoUd)
    step = 3
    for i = 1,reportList.Count do
        for j = 1,reportList[i-1].Count, step do
            local round 	= reportList[i-1][j-1] -- 第几轮
            local reportId 	= reportList[i-1][j]
            local argList 	= reportList[i-1][j+1]

            reportInfo[round] = reportInfo[round] or {}
            table.insert(reportInfo[round], {reportId = reportId, argList = argList})
        end
    end

    local scoreList = MsgPackImpl.unpack(roundScoreUd)
    step = 5
    for i = 1,scoreList.Count do
        for j = 1,scoreList[i-1].Count,step do
            local round 	= scoreList[i-1][j-1]
            local force1 	= scoreList[i-1][j]
            local score1 	= scoreList[i-1][j+1]
            local force2 	= scoreList[i-1][j+2]
            local score2 	= scoreList[i-1][j+3]
           -- print("scoreList",i,round, force1,score1, force2,score2)
            scoreInfo[round] = {}
            scoreInfo[round][force1] = score1
            scoreInfo[round][force2] = score2
        end
    end

    -- 每一轮出战信息格式和解析方法
    local chuZhanList = MsgPackImpl.unpack(chuzhanInfoUd)
    local roundInfo = {}
    step = 2
    for round = 1,chuZhanList.Count do
      roundInfo[round] = roundInfo[round] or {}
      for j = 1,chuZhanList[round-1].Count,step do
        local playerId = chuZhanList[round-1][j-1]
        local force = chuZhanList[round-1][j]
          table.insert(roundInfo[round],{playerId = playerId, force = force})
      end
    end
    
    local zhuweiInfo = {}
    local extraList = MsgPackImpl.unpack(extraUd)
    if extraList.Count >= 1 then
      local zhuweiList = extraList[1]
      local step = 5
      for i = 1,zhuweiList.Count,step do
        local round 	= zhuweiList[i-1]
        local force1 	= zhuweiList[i]
        local value1 	= zhuweiList[i+1]
        local force2 	= zhuweiList[i+2]
        local value2 	= zhuweiList[i+3]
        -- print("zhuweiList",i,round, force1,score1, force2,score2)
        zhuweiInfo[round] = {}
        zhuweiInfo[round][force1] = value1
        zhuweiInfo[round][force2] = value2
      end
    end

    data.teamInfo = teamInfo
    data.playerInfo = playerInfo
    data.reportInfo = reportInfo
    data.scoreInfo = scoreInfo
    data.fightTime = fightTime
    data.roundInfo = roundInfo
    data.reportDataCount = scoreList.Count
    data.zhuweiInfo = zhuweiInfo

    g_ScriptEvent:BroadcastInLua("LoadStarBiwuFengHuoData", data)
end

-- 活动页面查询比赛状态
-- Gac2Gas.QueryStarBiwuStatusForActivity
-- 对应枚举值 EnumStarBiwuFightActivityStatus
--[[
EnumStarBiwuFightActivityStatus = {
    eInSignUpNoRight    = 1, -- 报名阶段，无权限
    eInSignUpHasRight   = 2, -- 报名阶段，有权限
    eAfterSignupNoMatch = 3, -- 报名结束,无比赛
    eAfterSignupInMatch = 4, -- 报名结束,有比赛
}
--]]
-- 请求查询观战列表继续使用 Gac2Gas.QueryStarBiwuWatchList InMatch	
CLuaStarBiwuMgr.m_ActivityInfo = nil
function Gas2Gac.SyncStarBiwuStatusForActivity(bOfficial, status)
  CLuaStarBiwuMgr.m_ActivityInfo = {bOfficial = bOfficial, status = status}
  g_ScriptEvent:BroadcastInLua("OnSyncStarBiwuStatusForActivity", bOfficial, status)
end

CLuaStarBiwuMgr.m_ZhanduiRenqiRankInfo = {}
function Gas2Gac.SyncStarBiwuZhanduiRenqiRank(rankUd, giftRankListUd)
  local giftRankList = g_MessagePack.unpack(giftRankListUd)
  local rankData = CStarbiwuRenqiRankRpc()
	rankData:LoadFromString(rankUd)
  CLuaStarBiwuMgr.m_ZhanduiRenqiRankInfo = {rankList = {}, giftRankSet = {}}
  if rankData.Infos then
		CommonDefs.DictIterate(rankData.Infos, DelegateFactory.Action_object_object(function(key,v)
			local rank = v.RankPos
      local zhanduiId = v.ZhanduiId
      local zhanduiName = v.ZhanduiName
      local renqi = v.Score
      local name1 = v.TopName
      local name2 = v.SecondName
			table.insert(CLuaStarBiwuMgr.m_ZhanduiRenqiRankInfo.rankList,{
        rank = rank, zhanduiName = zhanduiName, renqi = renqi, name1 = name1, name2 = name2,zhanduiId = zhanduiId
      }) 
		end))
	end
  for i, id in pairs(giftRankList) do
    CLuaStarBiwuMgr.m_ZhanduiRenqiRankInfo.giftRankSet[id] = true
  end
  g_ScriptEvent:BroadcastInLua("OnSyncStarBiwuZhanduiRenqiRank")
end

CLuaStarBiwuMgr.m_ZhanduiDetailRankInfo = {}
function Gas2Gac.SyncStarBiwuZhanduiDetailRank(renqi, rankUd, memberUd)
  local rankData = CStarbiwuDetailRankRpc()
	rankData:LoadFromString(rankUd)
  local memberList = g_MessagePack.unpack(memberUd)
  CLuaStarBiwuMgr.m_ZhanduiDetailRankInfo = {renqi = renqi, memberList = {}, rankList = {}}
  if rankData.Infos then
		CommonDefs.DictIterate(rankData.Infos, DelegateFactory.Action_object_object(function(key,v)
			local force = v.Force
      local rankPos = v.RankPos
      local playerId = v.PlayerId
      local playerName = v.Name
      local rankValue = v.Score
      local serverName = v.ServerName
			table.insert(CLuaStarBiwuMgr.m_ZhanduiDetailRankInfo.rankList,{
        force = force, rankPos = rankPos, playerId = playerId, playerName = playerName, rankValue = rankValue,serverName = serverName
      }) 
		end))
	end
  for i = 1,#memberList,4 do
    local playerId  = memberList[i]
    local playerCls = memberList[i+1]
    local playerName = memberList[i+2]
    local serverName = memberList[i+3]
    table.insert(CLuaStarBiwuMgr.m_ZhanduiDetailRankInfo.memberList,{playerId = playerId, playerCls = playerCls, playerName = playerName, serverName = serverName})
  end
  g_ScriptEvent:BroadcastInLua("OnSyncStarBiwuZhanduiDetailRank")
end

CLuaStarBiwuMgr.m_FightZhanduiIdInfo = {}
CLuaStarBiwuMgr.m_GiftContributionRankWndZhanDuiId = 0
CLuaStarBiwuMgr.m_SchedulePageQnDaoViewSwitchState = 0

-- Gac2Gas.QueryStarBiwuStatusForActivity 新增参数区分是否渠道，true表示官方
-- Gac2Gas.StarBiwuQueryLeiGuTimes 查询擂鼓剩余次数 返回这个RPC
-- Gac2Gas.StarBiwuRequestByLeiGuTimes 请求购买次数，参数1是StarBiWuShow_LeiGu中的id,参数2是购买次数,返回也是这个RPC
-- 同步玩家所拥有的擂鼓的次数
CLuaStarBiwuMgr.m_LeiguId2Times = {}
function Gas2Gac.SyncStarBiwuLeiGuTimes(leiguTimesUd)
  local leiguTimeList = g_MessagePack.unpack(leiguTimesUd)
  CLuaStarBiwuMgr.m_LeiguId2Times = {}
  for i = 1,#leiguTimeList,2 do
    local leiguId   = leiguTimeList[i] -- StarBiWuShow_LeiGu中的id
    local times     = leiguTimeList[i+1]
    CLuaStarBiwuMgr.m_LeiguId2Times[leiguId] = times
  end
  g_ScriptEvent:BroadcastInLua("OnSyncStarBiwuLeiGuTimes")
end

-- 渠道明星赛，热身赛战况
-- 渠道明星赛当前新增4个上行RPC
-- Gac2Gas.QueryStarBiwuReShenSaiZK_QD 查热身赛战况 -> SyncStarBiwuReShenSaiZK_QD
-- Gac2Gas.QueryStarBiwuZhengShiSaiZK_QD 查正式赛战况 -> SyncStarBiwuZhengShiSaiZK_QD
-- Gac2Gas.RequestEnterStarBiwuPrepare_QD 请求进入准备场
-- Gac2Gas.RequestWatchStarBiwuZhengShiSai_QD 请求观正式比赛

function Gas2Gac.SyncStarBiwuReShenSaiZK_QD(selfZhanduiId, selfZhanduiName, dataLen, dataUd)
	CLuaStarBiwuMgr:SyncStarBiwuReShenSaiZK_QD(selfZhanduiId, selfZhanduiName, dataLen, dataUd)
end

CLuaStarBiwuMgr.m_ReShenSaiZkDataList = {}
function CLuaStarBiwuMgr:SyncStarBiwuReShenSaiZK_QD(selfZhanduiId, selfZhanduiName, dataLen, dataUd)
	local dataList = g_MessagePack.unpack(dataUd)
  self.m_ReShenSaiZkDataList = {} 
  for round = 1, StarBiWuShow_QudaoSetting.GetData().ReShenSai_StartMatch_Time.Length do
    local t = {}
    t.selfZhanduiId = selfZhanduiId
    t.selfZhanduiName = selfZhanduiName
    t.round = round
    self.m_ReShenSaiZkDataList[round] = t
  end
	for i = 1, #dataList, dataLen do
		local round = dataList[i]
		local zhanduiId = dataList[i+1]
		local zhanduiName = dataList[i+2]
		local status = dataList[i+3] -- 对应服务端枚举值 EnumStarBiwuFightCommonResult
    local t = self.m_ReShenSaiZkDataList[round]
    t.zhanduiId = zhanduiId
    t.zhanduiName = zhanduiName
    t.status = status
    self.m_ReShenSaiZkDataList[round] = t
	end
  if not CUIManager.IsLoaded(CLuaUIResources.StarBiwuReShenBattleStatusWnd) then
    CUIManager.ShowUI(CLuaUIResources.StarBiwuReShenBattleStatusWnd)
  else
    g_ScriptEvent:BroadcastInLua("OnSyncStarBiwuReShenSaiZK")
  end
end

-- 渠道明星赛，正式赛战况
function Gas2Gac.SyncStarBiwuZhengShiSaiZK_QD(zhanduiDataLen, zhanduiDataUd, statusDataLen, biSaiStatusUd)
  CLuaStarBiwuMgr:SyncStarBiwuZhengShiSaiZK_QD(zhanduiDataLen, zhanduiDataUd, statusDataLen, biSaiStatusUd)
end

CLuaStarBiwuMgr.m_ZhengShiSaiZhanduiInfo = {}
CLuaStarBiwuMgr.m_ZhengShiSaiStatusInfo = {}
CLuaStarBiwuMgr.m_ZhengShiSaiZhanduiId2Idx = {}
function CLuaStarBiwuMgr:SyncStarBiwuZhengShiSaiZK_QD(zhanduiDataLen, zhanduiDataUd, statusDataLen, biSaiStatusUd)
  local zhanduiDataList = g_MessagePack.unpack(zhanduiDataUd)
	local biSaiStatusList = g_MessagePack.unpack(biSaiStatusUd)

  self.m_ZhengShiSaiZhanduiInfo = {}
  self.m_ZhengShiSaiZhanduiId2Idx = {}
	-- 1-16 战队名,这个1-16就直接对应交互中的 一 到 十六
	for idx,zhanduiInfo in pairs(zhanduiDataList) do
		local zhanduiId = zhanduiInfo[1]
		local zhanduiName = zhanduiInfo[2]
    self.m_ZhengShiSaiZhanduiInfo[idx] = {zhanduiId = zhanduiId,zhanduiName = zhanduiName}
    --print(idx, zhanduiName)
    if zhanduiId ~= 0 then
      self.m_ZhengShiSaiZhanduiId2Idx[zhanduiId] = idx
    end
	end

	-- 按时间顺序下发比赛战况，i = 1 表示第一场，2 表示第二场，以此内推,具体来说:
	-- 前八场是交互中 19:00开打的,再具体一点的话
	
	-- 第1场对应 交互中的 一和十六 的那一场
	-- 第2场对应 交互中的 二和十五 的那一场
	-- 第3场对应 交互中的 三和十四 的那一场
	-- 4 : 四 VS 十三
	-- 5 : 五 VS 十二
	-- 6 : 六 VS 十一
	-- 7 : 七 VS 十
	-- 8 : 八 VS 九
	
	-- 9-12 是交互中 19:30开打的
	-- 9 : 1 和 8 的胜者打
	-- 10: 2 和 7 的胜者打
	-- 11: 3 和 6 的胜者打
	-- 12: 4 和 5 的胜者打
	
	-- 13-14 是交互中20:00的两场，注意是两场，不包含冠军塞，冠军赛是15
	-- 13: 9 和 12 的胜者打
	-- 14: 10 和11 的胜者

	-- 15: 冠军赛

  local round = 1
  self.m_ZhengShiSaiStatusInfo = {}
	for i = 1,#biSaiStatusList,statusDataLen do
	    -- 对应服务端的枚举值 EnumStarBiwuFightCommonResult
	    -- 0表示尚未开始
	    -- 1表示进行中，结果没出来，可以观战
	    -- 2表示结果出来了
		local status = biSaiStatusList[i]
		-- 获胜的战队id，结果没出来之前就是0
		local winnerId = biSaiStatusList[i+1]
    self.m_ZhengShiSaiStatusInfo[round] = {status = status, winnerId = winnerId}
    --print(round,status,winnerId)
    round = round + 1
	end
  if not CUIManager.IsLoaded(CLuaUIResources.StarBiwuQuDaoBattleStatusWnd) then
    CUIManager.ShowUI(CLuaUIResources.StarBiwuQuDaoBattleStatusWnd)
  else
    g_ScriptEvent:BroadcastInLua("OnSyncStarBiwuZhengShiSaiZK")
  end
end

function Gas2Gac.SyncStarBiwuStarScore(starScore)
  CLuaStarBiwuMgr:OnSyncStarBiWuStarScore(starScore)
end

function Gas2Gac.SyncStarBiwuGroupIdx(groupIdx)
  CLuaStarBiwuMgr:OnSyncStarBiWuUpdatePlayerGroup(groupIdx)
end
