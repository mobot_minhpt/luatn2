local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local Profession = import "L10.Game.Profession"
local EChatPanel = import "L10.Game.EChatPanel"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local AlignType = import "CPlayerInfoMgr+AlignType"


CLuaCrossDouDiZhuVoteWnd = class()
RegistClassMember(CLuaCrossDouDiZhuVoteWnd,"m_TableView")
RegistClassMember(CLuaCrossDouDiZhuVoteWnd,"m_NumLabel")
RegistClassMember(CLuaCrossDouDiZhuVoteWnd,"m_InfoList")


function CLuaCrossDouDiZhuVoteWnd:Init()
    self.m_NumLabel = self.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
    self.m_NumLabel.text = SafeStringFormat3(LocalString.GetString("请选择心中的冠军候选人 %d/%d"),0,0)

    self.m_TableView=self.transform:Find("TableView"):GetComponent(typeof(QnTableView))
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_InfoList
        end,
        function(item,index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

            self:InitItem(item,index,self.m_InfoList[index+1])
        end)

    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        local data=self.m_InfoList[row+1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.joinId, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
        end
    end)

    self.m_InfoList = {}
    Gac2Gas.CrossDouDiZhuQueryJueSaiVoteList()
end

function CLuaCrossDouDiZhuVoteWnd:InitItem(item,index,info)
    local tf = item.transform
    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    local serverLabel = tf:Find("ServerLabel"):GetComponent(typeof(UILabel))
    local scoreLabel = tf:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    local clsSprite = tf:Find("ClsSprite"):GetComponent(typeof(UISprite))
    local votedMark = tf:Find("VotedLabel").gameObject

    nameLabel.text = info.Name
    serverLabel.text= info.ServerName
    clsSprite.spriteName = Profession.GetIconByNumber(info.Class)
    scoreLabel.text = tostring(info.HaiXuanScore)
    

    local playerId = info.joinId
    local button = tf:Find("Button").gameObject
    if info.IsVoted then
        button:SetActive(false)
        votedMark:SetActive(true)
    else
        votedMark:SetActive(false)
        button:SetActive(true)
        UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function(g)
            Gac2Gas.CrossDouDiZhuRequestJueSaiVote(playerId)
        end)
    end
end

function CLuaCrossDouDiZhuVoteWnd:OnEnable()
    g_ScriptEvent:AddListener("CrossDouDiZhuQueryJueSaiVoteListResult", self, "OnCrossDouDiZhuQueryJueSaiVoteListResult")

end

function CLuaCrossDouDiZhuVoteWnd:OnDisable()
    g_ScriptEvent:RemoveListener("CrossDouDiZhuQueryJueSaiVoteListResult", self, "OnCrossDouDiZhuQueryJueSaiVoteListResult")

end

function CLuaCrossDouDiZhuVoteWnd:OnCrossDouDiZhuQueryJueSaiVoteListResult(voteNum,maxNum,t)
    self.m_NumLabel.text = SafeStringFormat3(LocalString.GetString("请选择心中的冠军候选人 %d/%d"),voteNum,maxNum)

    self.m_InfoList = t
    self.m_TableView:ReloadData(true,true)
end
