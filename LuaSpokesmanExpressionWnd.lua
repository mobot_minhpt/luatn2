local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CButton = import "L10.UI.CButton"
local QnButton = import "L10.UI.QnButton"
local CUITexture = import "L10.UI.CUITexture"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local QnTableItem = import "L10.UI.QnTableItem"
local UILabel = import "UILabel"

CLuaSpokesmanExpressionWnd = class()

RegistClassMember(CLuaSpokesmanExpressionWnd, "m_TableView")
RegistClassMember(CLuaSpokesmanExpressionWnd, "m_DataList")
RegistClassMember(CLuaSpokesmanExpressionWnd, "m_TitleLab")
RegistClassMember(CLuaSpokesmanExpressionWnd, "m_DoActionBtn")
RegistClassMember(CLuaSpokesmanExpressionWnd, "m_LingYuCost")
RegistClassMember(CLuaSpokesmanExpressionWnd, "m_SelectedInfo")

RegistClassMember(CLuaSpokesmanExpressionWnd, "m_CenterPos")

function CLuaSpokesmanExpressionWnd:Awake()
    if not CClientMainPlayer.Inst then
        CUIManager.CloseUI(CLuaUIResources.SpokesmanExpressionWnd)
    end
    self.m_CenterPos = {CClientMainPlayer.Inst.RO.Position.x, CClientMainPlayer.Inst.RO.Position.z}
    self:InitComponents()
end

function CLuaSpokesmanExpressionWnd:InitComponents()
    self.m_TableView    = self.transform:Find("Anchor/Expressions"):GetComponent(typeof(QnTableView))
    self.m_TitleLab     = self.transform:Find("Anchor/Title/Lab"):GetComponent(typeof(UILabel))
    self.m_DoActionBtn  = self.transform:Find("Anchor/Button"):GetComponent(typeof(QnButton))
    self.m_LingYuCost   = self.transform:Find("Anchor/QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))

    self.m_LingYuCost.gameObject:SetActive(false)
end

function CLuaSpokesmanExpressionWnd:Init()
    --初始化表情table
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_DataList
        end,
        function(item, index)
            self:InitItem(item, self.m_DataList[index + 1])
        end
    )
    self.m_DataList = {}
    Spokesman_PayExpression.Foreach(function (key)
        local data = Spokesman_PayExpression.GetData(key)
        table.insert(self.m_DataList, data)
    end)

    self.m_TableView:ReloadData(true, false)

    --初始化其它控件
    self.m_TitleLab.text = g_MessageMgr:FormatMessage("SPOKESMAN_EXPRESSION_TITLE")
    self.m_LingYuCost:SetType(13, 0, false)

    self.m_DoActionBtn.OnClick = DelegateFactory.Action_QnButton(function (go)
        if not self.m_SelectedInfo then
            g_MessageMgr:ShowMessage("SPOKESMAN_EXPRESSION_NOSELECT")
            return
        end

        if not CShopMallMgr.TryCheckEnoughJade(self.m_SelectedInfo.Jade, uint.MaxValue) then
            return
        end
        Gac2Gas.RequestGlobalSpokesmanShowExpression(self.m_SelectedInfo.ExpressionId)
        CUIManager.CloseUI(CLuaUIResources.SpokesmanExpressionWnd)
    end)
end

function CLuaSpokesmanExpressionWnd:InitItem(item, info)
    local expressionInfo = Expression_Show.GetData(info.ExpressionId)

    local tableItem = item.transform:GetComponent(typeof(QnTableItem))
    local icon      = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local name      = item.transform:Find("Name"):GetComponent(typeof(UILabel))
    local btn       = item.transform:GetComponent(typeof(CButton))

    icon:LoadMaterial(expressionInfo.Icon)
    name.text = expressionInfo.ExpName

    tableItem.OnSelected = DelegateFactory.Action_GameObject(function (go)
        self.m_LingYuCost.gameObject:SetActive(true)
        self.m_LingYuCost:SetCost(info.Jade)

        self.m_SelectedInfo = info
    end)
end

function CLuaSpokesmanExpressionWnd:Update()
	if not CClientMainPlayer.Inst then
		CUIManager.CloseUI(CLuaUIResources.SpokesmanExpressionWnd)
		return
	end
	local x, z = CClientMainPlayer.Inst.RO.Position.x, CClientMainPlayer.Inst.RO.Position.z
	if math.abs(x - self.m_CenterPos[1]) > 10 or math.abs(z - self.m_CenterPos[2]) > 10 then
		CUIManager.CloseUI(CLuaUIResources.SpokesmanExpressionWnd)
		return
	end
end
