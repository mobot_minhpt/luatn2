local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UITabBar = import "L10.UI.UITabBar"
local UILabel = import "UILabel"

local CItemCountUpdate=import "L10.UI.CItemCountUpdate"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local CItemMgr = import "L10.Game.CItemMgr"
local CItem = import "L10.Game.CItem"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChatLinkMgr = import "CChatLinkMgr"

local QnTableView=import "L10.UI.QnTableView"
local QnAddSubAndInputButton=import "L10.UI.QnAddSubAndInputButton"
local CUITexture=import "L10.UI.CUITexture"
local CButton=import "L10.UI.CButton"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local MessageWndManager = import "L10.UI.MessageWndManager"
local BoxCollider = import "UnityEngine.BoxCollider"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local UILongPressButton = import "L10.UI.UILongPressButton"
local ShowType = import "L10.UI.CItemInfoMgr+ShowType"
local CItemInfoMgr_AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local LinkItemInfo = import "L10.UI.CItemInfoMgr+LinkItemInfo"
local CNPCShopInfoMgr=import "L10.UI.CNPCShopInfoMgr"

LuaSeaFishingShopWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaSeaFishingShopWnd, "ItemShowWnd", "ItemShowWnd", GameObject)
RegistChildComponent(LuaSeaFishingShopWnd, "ZhuangshiwuShopWnd", "ZhuangshiwuShopWnd", GameObject)
RegistChildComponent(LuaSeaFishingShopWnd, "TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaSeaFishingShopWnd, "PoolGradeLabel", "PoolGradeLabel", UILabel)

--@endregion RegistChildComponent end

function LuaSeaFishingShopWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.titleLabel = self.transform:Find("Wnd_Bg_Primary_Normal/TitleLabel"):GetComponent(typeof(UILabel))
    self.goodsTable = self.transform:Find("ItemShowWnd/GoodsGrid"):GetComponent(typeof(QnTableView))
    self.nameLabel = self.transform:Find("ItemShowWnd/Detail/Desc/NameLabel"):GetComponent(typeof(UILabel))
    self.levelLabel = self.transform:Find("ItemShowWnd/Detail/Desc/LevelLabel"):GetComponent(typeof(UILabel))
    self.typeLabel = self.transform:Find("ItemShowWnd/Detail/Desc/Type/TypeLabel"):GetComponent(typeof(UILabel))
    self.descRegion = self.transform:Find("ItemShowWnd/Detail/Desc/DescRegion/Container"):GetComponent(typeof(UIWidget))
    self.descLabel = self.transform:Find("ItemShowWnd/Detail/Desc/DescRegion/DescScrollView/DescLabel"):GetComponent(typeof(UILabel))
    self.descScrollView = self.transform:Find("ItemShowWnd/Detail/Desc/DescRegion/DescScrollView"):GetComponent(typeof(UIScrollView))
    self.numberInput = self.transform:Find("ItemShowWnd/Detail/Desc/QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
    self.icon = self.transform:Find("ItemShowWnd/Detail/Desc/Icon (1)"):GetComponent(typeof(CUITexture))
    self.buyBtn = self.transform:Find("ItemShowWnd/Detail/BuyBtn"):GetComponent(typeof(CButton))
    self.XiaoHaoPing = self.transform:Find("ItemShowWnd/Detail/Desc/XiaoHaoPing").gameObject
    self.selectedItemIndex = 0

    -- 装饰物商店
    self.ZswTable = self.ZhuangshiwuShopWnd.transform:Find("ZswTableView"):GetComponent(typeof(QnTableView))
    -- self.NextLevel = self.ZhuangshiwuShopWnd.transform:Find("PreviewOnShopLabel"):GetComponent(typeof(UILabel))
    self.buyZswBtn = self.ZhuangshiwuShopWnd.transform:Find("BuyZswBtn"):GetComponent(typeof(CButton))
    self.zswXiaoHaoPing = self.ZhuangshiwuShopWnd.transform:Find("ZswXiaoHaoPin").gameObject
    self.refreshBtn = self.ZhuangshiwuShopWnd.transform:Find("RefreshBtn").gameObject
    self.nextNameLabel = self.ZhuangshiwuShopWnd.transform:Find("PreviewOnShopLabel"):GetComponent(typeof(UILabel))
    self.moneytex1 =  self.ZhuangshiwuShopWnd.transform:Find("ZswCostAndOwnMoney/Cost/CostTex").gameObject
    self.moneytex2 = self.ZhuangshiwuShopWnd.transform:Find("ZswCostAndOwnMoney/Own/OwnTex").gameObject
    self.OwnLabel = self.ZhuangshiwuShopWnd.transform:Find("Own"):GetComponent(typeof(UILabel))

    self.m_ItemLookup={}
    self.m_GlobalLimitInfo = {}
    self.m_IsGlobalLimit = false
    self.m_IsOpenGlobalLimit = true
    self.descHeight = {self.descRegion.height, self.descRegion.height + 167}

    UIEventListener.Get(self.buyBtn.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        --最大数量 弹窗提示
        local templateId = LuaSeaFishingMgr.shopInfo.ItemGoods[self.selectedItemIndex+1][1]
        local type = LuaSeaFishingMgr.shopInfo.MoneyType
        local isYinLiang = type == EnumMoneyType.YinLiang or EnumMoneyType.YinPiao
        if isYinLiang and self.numberInput.m_MaxValue == self.numberInput.m_CurrentValue then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("NpcShop_Buy_Max_Confirm",self.numberInput.m_CurrentValue,self.nameLabel.text), DelegateFactory.Action(function () 
                Gac2Gas.BuyNpcShopItem(LuaSeaFishingMgr.shopInfo.NpcEngineId, LuaSeaFishingMgr.shopInfo.ShopId, templateId, self.numberInput:GetValue())
            end), nil, nil, nil, false)
            return
        end
        if not self:IsScoreMilestoneUnlocked(templateId) then
            g_MessageMgr:ShowMessage("NPC_SHOP_MILESTONE_LOCKED")
            return
        end
        Gac2Gas.BuyNpcShopItem(LuaSeaFishingMgr.shopInfo.NpcEngineId, LuaSeaFishingMgr.shopInfo.ShopId, templateId, self.numberInput:GetValue())
    end)

    UIEventListener.Get(self.buyZswBtn.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        --最大数量 弹窗提示
        local templateId = LuaSeaFishingMgr.zswShopInfo.ItemGoods[self.selectedZswItemIndex+1][1]
        local type = LuaSeaFishingMgr.zswShopInfo.MoneyType
        local isYinLiang = type == EnumMoneyType.YinLiang or EnumMoneyType.YinPiao
        if isYinLiang then
            local item = CItemMgr.Inst:GetItemTemplate(templateId)
            local name = item.Name
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("NpcShop_Buy_Max_Confirm",1, name), DelegateFactory.Action(function () 
                Gac2Gas.BuyNpcShopItem(LuaSeaFishingMgr.zswShopInfo.NpcEngineId, LuaSeaFishingMgr.zswShopInfo.ShopId, templateId, 1)
            end), nil, nil, nil, false)
            return
        end
        if not self:IsScoreMilestoneUnlocked(templateId) then
            g_MessageMgr:ShowMessage("NPC_SHOP_MILESTONE_LOCKED")
            return
        end
        Gac2Gas.BuyNpcShopItem(LuaSeaFishingMgr.zswShopInfo.NpcEngineId, LuaSeaFishingMgr.zswShopInfo.ShopId, templateId, 1)
    end)

    UIEventListener.Get(self.refreshBtn).onClick=DelegateFactory.VoidDelegate(function(go)
        local cost = LuaSeaFishingMgr.GetRefreshCost()
        if cost == nil then return end
        local enough = CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.FishingScore) >= cost
        if enough then
            self.ZhuangshiwuShopWnd:SetActive(false)
            Gac2Gas.RefreshSeaFishingDecorationShop(LuaSeaFishingMgr.Shop[2], CClientMainPlayer.Inst.Id)
        else
            g_MessageMgr:ShowMessage("FishingScore_JIFEN_NotEnough")
        end
    end)

    UIEventListener.Get(self.ZhuangshiwuShopWnd.transform:Find("Own/AddBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        g_MessageMgr:ShowMessage("FishingScore_JIFEN_GET")
    end)
end

function LuaSeaFishingShopWnd:Init()
    local item = Item_Item.GetData(LuaSeaFishingMgr.XiaoHaoPing)
    self.XiaoHaoPing:GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
    self.zswXiaoHaoPing:GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
    self.titleLabel.text = LocalString.GetString("海钓积分商店")
    self.PoolGradeLabel.text = SafeStringFormat3(LocalString.GetString("当前积分商店等级 %d级"), LuaSeaFishingMgr.GetShopGrade())
    self.inited = {false, false}
    self.selectedTab = -1
    self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        if self.selectedTab == index then return end
        index = index + 1
        if self.inited[index] == false then
            if index == 1 then
                self.selectedTab = 0
                self.inited[1] = true
                self.ItemShowWnd:SetActive(true)
                self.ZhuangshiwuShopWnd:SetActive(false)
                self:InitNormalShop()
            elseif index == 2 then
                self.TabBar:ChangeTab(0, true)
                CLuaNPCShopInfoMgr.ShowScoreShopById(LuaSeaFishingMgr.Shop[2])
            end
        else
            self.selectedTab = index - 1
            self.ItemShowWnd:SetActive(index == 1)
            self.ZhuangshiwuShopWnd:SetActive(index == 2)
        end
    end)
    self.TabBar:ChangeTab(0)
end

function LuaSeaFishingShopWnd:InitNormalShop()
    self.moneyCtrl = self.transform:Find("ItemShowWnd/Detail/Desc/GameObject/QnCostAndOwnMoney"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
    local isTempPlayScore = false
    local scoreShopData = Shop_PlayScore.GetData(LuaSeaFishingMgr.shopInfo.ShopId)
    if scoreShopData then
        local globalLimit = scoreShopData.GlobalLimit
        if globalLimit and globalLimit ~= "" then
            self.m_IsGlobalLimit = true
            for cronStr in string.gmatch(globalLimit, "([^;]+);?") do
                local t = {}
                local itemId,limitCount = string.match(cronStr, "(%d+),(%d+)")
                t.itemId = tonumber(itemId)
                t.limitCount = tonumber(limitCount)
                self.m_GlobalLimitInfo[t.itemId] = t
            end
        end
        isTempPlayScore = scoreShopData.ScoreType == 2--临时玩法积分
    end
    self.numberInput.onValueChanged = DelegateFactory.Action_uint(function(val)
        local price = LuaSeaFishingMgr.shopInfo.Prices[self.selectedItemIndex + 1]
        self.moneyCtrl:SetCost(val * price[1])
        if price[2] > 0 then
            local countUpdate = self.XiaoHaoPing:GetComponent(typeof(CItemCountUpdate))
            countUpdate:UpdateCount()
        end
    end)

    local type = LuaSeaFishingMgr.shopInfo.MoneyType
    local scoreKey = LuaSeaFishingMgr.shopInfo.PlayScoreKey

    self.buyBtn.Text = type == EnumMoneyType.Score and LocalString.GetString("兑换") or LocalString.GetString("购买")
    if isTempPlayScore then
        self.moneyCtrl:InitTempPlayScore(EnumTempPlayScoreKey_lua[scoreShopData.Key])
    else
        if type == EnumMoneyType.Score then
            self.moneyCtrl:SetType(EnumMoneyType.Score, scoreKey, true)
        else
            self.moneyCtrl:SetType(type, EnumPlayScoreKey.NONE, true)
        end
    end

    --数量变化的时候
    self.moneyCtrl.updateAction = function () 
        self:RefreshInputMinMax(self.goodsTable.currentSelectRow)--更新最大数量
    end

    self.goodsTable.m_DataSource=DefaultTableViewDataSource.Create(
        function() return #LuaSeaFishingMgr.shopInfo.ItemGoods end,
        function(item,index) self:InitItem(item.transform,index) end)

    self.goodsTable.OnSelectAtRow=DelegateFactory.Action_int(function(row)
        self.selectedItemIndex = row
        self:UpdateDescription()
        self:RefreshInputMinMax(row)
    end)

    self.goodsTable:ReloadData(false, false)
    self.goodsTable:SetSelectRow(0, true)
end

--@region UIEvent

--@endregion UIEvent

function LuaSeaFishingShopWnd:RefreshInputMinMax( row )
    local shopInfo = LuaSeaFishingMgr.shopInfo
    if shopInfo.ItemGoods[row + 1] == nil then return end
    --刷新最大最小值
    local templateId = shopInfo.ItemGoods[row + 1][1]
    local limit = LuaSeaFishingMgr.GetRemainCount(templateId)
    local price = math.max(shopInfo.Prices[row + 1][1], 1)
    local perlCost = math.max(shopInfo.Prices[row + 1][2], 1)
    --以防出现价格为0的情况
    local maxNum = math.floor(self.moneyCtrl:GetOwn() / price)

    local countUpdate = self.XiaoHaoPing:GetComponent(typeof(CItemCountUpdate))
    local xhpId = shopInfo.Prices[row + 1][4]
    if xhpId and xhpId>0 then
        maxNum = math.min(maxNum, math.floor(countUpdate.count / perlCost))
    end
    maxNum = math.min(maxNum, 999)
    
    if limit == 0 then
        self.numberInput:SetMinMax(1, 1, 1)
    elseif maxNum == 0 then
        self.numberInput:SetMinMax(1, 1, 1)
    else
        if maxNum >= limit then
            self.numberInput:SetMinMax(1, limit, 1)
        else
            self.numberInput:SetMinMax(1, maxNum, 1)
        end
    end
    --刷新
    templateId = shopInfo.ItemGoods[self.selectedItemIndex + 1][1]
    --不能用SetCost，会递归调用，引起崩溃
    local cost = self.numberInput:GetValue() * shopInfo.Prices[self.selectedItemIndex + 1][1]
    self.moneyCtrl.m_CostLabel.text = tostring(cost)
    --全服限购的物品每次只允许购买一个
    if self.m_IsGlobalLimit and self.m_GlobalLimitInfo[templateId] then
        self.numberInput:SetMinMax(1, 1, 1)
        self.numberInput:SetMinMax(1, 1, 1)
    end

    local countUpdate = self.XiaoHaoPing:GetComponent(typeof(CItemCountUpdate))
    countUpdate:UpdateCount()
end

function LuaSeaFishingShopWnd:UpdateDescription( )
    local shopInfo = LuaSeaFishingMgr.shopInfo
    local templateId = shopInfo.ItemGoods[self.selectedItemIndex + 1][1]

    local item = CItemMgr.Inst:GetItemTemplate(templateId)
    local name = item.Name
    local level = CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(item)
    local desc = CItem.GetItemDescription(item.ID,true)
    if ((item.TimesGroup > 0 and item.TimesPerDay) or (templateId == 21006125)) and CClientMainPlayer.Inst ~= nil then  --显示剩余使用次数
        local remainTimes = LuaSeaFishingMgr.GetRemainUsingTimes(item)
        desc = desc .. SafeStringFormat3(LocalString.GetString("\n还可使用[c][00FF00]%d[-][/c]次"), remainTimes)
    end
    self.icon:LoadMaterial(item.Icon)
    local data = Item_Type.GetData(item.Type)
    local itemType = data and data.Name or ""
    local xhpPrice = shopInfo.Prices[self.selectedItemIndex + 1][2]
    local xhpId = shopInfo.Prices[self.selectedItemIndex + 1][4]

    self.numberInput:SetValue(1, true)

    self.nameLabel.text = name
    local levelStr = level > 0 and "Lv." .. tostring(level) or nil
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level < level then
        levelStr = SafeStringFormat3("[c][ff0000]%s[-][/c]", levelStr)
    end

    if self:IsScoreMilestoneUnlocked(templateId) then
        self.levelLabel.text = levelStr
    else
        self.levelLabel.text = LocalString.GetString("[FF0000]未解锁[-]")
    end

    self.typeLabel.text = itemType
    self.descLabel.text = CChatLinkMgr.TranslateToNGUIText(desc, false)
    self.descScrollView:ResetPosition()
    self.moneyCtrl:SetCost(shopInfo.Prices[self.selectedItemIndex + 1][1])
    self.descRegion.height = xhpPrice > 0 and self.descHeight[1] or self.descHeight[2]
    self.ItemShowWnd.transform:Find("Detail/Desc/Split").gameObject:SetActive(xhpPrice > 0)
    self.descScrollView.gameObject:GetComponent(typeof(UIPanel)):ResetAndUpdateAnchors()
    self.XiaoHaoPing:SetActive(xhpPrice > 0)
    if xhpPrice > 0 then
        self.XiaoHaoPing:GetComponent(typeof(CUITexture)):LoadMaterial(Item_Item.GetData(xhpId).Icon) --消耗品图标

        local getNode = self.XiaoHaoPing.transform:Find("Mask").gameObject
        local countUpdate = self.XiaoHaoPing:GetComponent(typeof(CItemCountUpdate))
        countUpdate.templateId = xhpId
        countUpdate.onChange = DelegateFactory.Action_int(function (count) 
            local inputCount = self.numberInput:GetValue()
            local needCount = xhpPrice*inputCount
            if needCount <= count then
                countUpdate.format = "{0}/"..tostring(needCount)
                getNode:SetActive(false)
            else
                countUpdate.format = "[ff0000]{0}[-]/"..tostring(needCount)
                getNode:SetActive(true)
            end
        end)
        countUpdate:UpdateCount()

        UIEventListener.Get(self.XiaoHaoPing).onClick = DelegateFactory.VoidDelegate(function (p)
            local inputCount = self.numberInput:GetValue()
            local needCount = xhpPrice*inputCount
            if needCount <= countUpdate.count then
                CItemInfoMgr.ShowLinkItemTemplateInfo(xhpId, false)
            else
                CItemAccessListMgr.Inst:ShowItemAccessInfo(xhpId, false, nil, CTooltipAlignType.Right)
            end
        end)
    end

    --全服限购
    -- if self.m_IsGlobalLimit and self.m_GlobalLimitInfo[templateId] then
    --     Gac2Gas.QueryScoreShopGoodsRestStock(shopInfo.ShopId,templateId)
    -- else
    --     if not self.buyBtn.Enabled then
    --         self.buyBtn.Enabled = true
    --     end
    -- end
end

function LuaSeaFishingShopWnd:InitItem(transform,row)
    local shopInfo = LuaSeaFishingMgr.shopInfo
    local templateId = shopInfo.ItemGoods[row + 1][1]
    self.m_ItemLookup[templateId]=transform

    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local priceLabel = transform:Find("PriceLabel"):GetComponent(typeof(UILabel))
    local moneySprite = transform:Find("MoneySprite"):GetComponent(typeof(UITexture))
    -- local needSprite = transform:Find("NeedSprite"):GetComponent(typeof(UISprite))
    -- local disableSprite = transform:Find("Icon/DisableSprite"):GetComponent(typeof(UISprite))
    local levelLabel = transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local numLimitLabel = transform:Find("NumLimitLabel"):GetComponent(typeof(UILabel))
    local globalSellOutTag = transform:Find("GlobalSellOutTag").gameObject
    -- local lockSprite = transform:Find("LockSprite"):GetComponent(typeof(UISprite))
    -- local unlock = transform:Find("Unlock").gameObject
    local milestoneLabelLabel = transform:Find("Unlock/MilestoneLabelLabel"):GetComponent(typeof(UILabel))

    nameLabel.text = nil
    levelLabel.text = nil
    icon:Clear()
    -- moneySprite.spriteName = Money.GetIconName(shopInfo.MoneyType, shopInfo.PlayScoreKey)
    globalSellOutTag:SetActive(false)
    -- lockSprite.enabled = false
    -- unlock:SetActive(false)
    icon.alpha = 1
    -- 设置不可选中
    local opened = not LuaSeaFishingMgr.shopInfo.ItemGoods[row+1][2]
    transform:GetComponent(typeof(BoxCollider)).enabled = opened
    transform:Find("Mask").gameObject:SetActive(not opened)
    if not opened then
        numLimitLabel.text = LocalString.GetString("下一级商店等级开放")
    end

    -- if IdPartition.IdIsEquip(templateId) then
    --     if CTaskMgr.IsEquipSubmitItem(templateId) then
    --         needSprite.alpha = 1
    --     else
    --         needSprite.alpha = 0
    --     end
    -- else
    --     if CLuaNPCShopInfoMgr.taskNeededItems[templateId] then
    --         needSprite.alpha = 1
    --     else
    --         needSprite.alpha = 0
    --     end
    -- end
 
    -- disableSprite.enabled = false
    local gradeRequire = 0
    local item = CItemMgr.Inst:GetItemTemplate(templateId)
    if item ~= nil then
        nameLabel.text = item.Name
        gradeRequire = CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(item)
        icon:LoadMaterial(item.Icon)
        -- disableSprite.enabled = not CItem.GetMainPlayerIsFit(templateId, true)
    end


    local gradeStr = gradeRequire > 0 and "Lv." .. tostring(gradeRequire) or nil
    --等级不够 标红
    if gradeStr and CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level < gradeRequire then
        gradeStr = SafeStringFormat3("[c][ff0000]%s[-][/c]", gradeStr)
    end
    levelLabel.text = gradeStr

    priceLabel.text = tostring(shopInfo.Prices[row + 1][1])

    self:UpdateRemainCountInfo(templateId)

    if not self:IsScoreMilestoneUnlocked(templateId) then
        local scoreShopData = Shop_PlayScore.GetData(shopInfo.ShopId)
        local needScore = CommonDefs.DictGetValue_LuaCall(scoreShopData.ScoreMilestone, templateId)
        levelLabel.text = nil
        -- moneySprite.spriteName = nil
        -- lockSprite.enabled = true
        -- unlock:SetActive(true)
        priceLabel.text = nil
        icon.alpha = 0.5
        numLimitLabel.text = nil
        milestoneLabelLabel.text = SafeStringFormat3(LocalString.GetString("[9BC9FF]历史累计[-]%d[9BC9FF]积分解锁[-]"), needScore)
    end

    if self.m_IsGlobalLimit and self.m_GlobalLimitInfo[templateId] then
        Gac2Gas.QueryScoreShopGoodsRestStock(shopInfo.ShopId,templateId)
    end
end

--@desc 购买里程碑是否解锁
function LuaSeaFishingShopWnd:IsScoreMilestoneUnlocked(templateId)
    local shopInfo = LuaSeaFishingMgr.shopInfo
    local scoreShopData = Shop_PlayScore.GetData(shopInfo.ShopId)
    if scoreShopData and scoreShopData.ScoreMilestone and CommonDefs.DictContains_LuaCall(scoreShopData.ScoreMilestone, templateId) then
        local scoreKey = shopInfo.PlayScoreKey
        local totalEnum = CommonDefs.ConvertIntToEnum(typeof(EnumPlayScoreKey), EnumToInt(scoreKey)+1)
        local total = CClientMainPlayer.Inst.PlayProp:GetScoreByKey(totalEnum)
        local needScore = CommonDefs.DictGetValue_LuaCall(scoreShopData.ScoreMilestone, templateId)
        return total >= needScore
    end
    return true
end

function LuaSeaFishingShopWnd:OnNPCShopItemRemainCountChange( shopId, templateId, remainCount) 
    if LuaSeaFishingMgr.shopInfo.ShopId == shopId then
        LuaSeaFishingMgr.SetRemainCount(templateId,remainCount)
        self:RefreshInputMinMax(self.goodsTable.currentSelectRow)
        self:UpdateRemainCountInfo(templateId)
    end
end

function LuaSeaFishingShopWnd:UpdateRemainCountInfo(templateId)
    if not self.m_ItemLookup[templateId] then
        return 
    end
    if self.m_ItemLookup[templateId]:GetComponent(typeof(BoxCollider)).enabled == false then
        --不可选中
        return
    end

    local numLimitLabel = self.m_ItemLookup[templateId]:Find("NumLimitLabel"):GetComponent(typeof(UILabel))

    local limit = LuaSeaFishingMgr.GetRemainCount(templateId)
    local limitType = LuaSeaFishingMgr.GetLimitType(templateId)
    if limitType == 1 then
        numLimitLabel.text = SafeStringFormat3(LocalString.GetString("剩余%d"), limit)
    elseif limitType == 2 then
        numLimitLabel.text = SafeStringFormat3(LocalString.GetString("本月剩余%d"), limit)
    elseif limitType == 3 then
        numLimitLabel.text = SafeStringFormat3(LocalString.GetString("本周剩余%d"), limit)
    else
        numLimitLabel.text = nil
        local globalLimitLabel = self.m_ItemLookup[templateId]:Find("GlobalLimitLabel"):GetComponent(typeof(UILabel))
        if self.m_IsGlobalLimit and self.m_GlobalLimitInfo[templateId] then
            local limitCount = self.m_GlobalLimitInfo[templateId].limitCount
            globalLimitLabel.text = SafeStringFormat3(LocalString.GetString("全服限购%d"),limitCount)
        else
            globalLimitLabel.text = nil
        end
    end
end

function LuaSeaFishingShopWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateNpcShopLimitItemCount", self, "OnNPCShopItemRemainCountChange")
    g_ScriptEvent:AddListener("UpdateZswShop", self, "InitZswShop")
    g_ScriptEvent:AddListener("UpdateZswItemState", self, "UpdateBuyedState")
    g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate",self,"OnMainPlayerPlayPropUpdate")
    -- g_ScriptEvent:AddListener("MainPlayerLevelChange", self, "UpdateDisableStatus")
    -- g_ScriptEvent:AddListener("OnReplyScoreShopGoodsRestStock", self, "OnReplyScoreShopGoodsRestStock")   
end
function LuaSeaFishingShopWnd:OnDisable()
    CNPCShopInfoMgr.selectItemTemplateId = 0
	g_ScriptEvent:RemoveListener("UpdateNpcShopLimitItemCount", self, "OnNPCShopItemRemainCountChange")
    g_ScriptEvent:RemoveListener("UpdateZswShop", self, "InitZswShop")
    g_ScriptEvent:RemoveListener("UpdateZswItemState", self, "UpdateBuyedState")
    g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate",self,"OnMainPlayerPlayPropUpdate")
    -- g_ScriptEvent:RemoveListener("MainPlayerLevelChange", self, "UpdateDisableStatus")
    -- g_ScriptEvent:RemoveListener("OnReplyScoreShopGoodsRestStock", self, "OnReplyScoreShopGoodsRestStock") 
end

-- function LuaSeaFishingShopWnd:UpdateDisableStatus()
--     for k,transform in pairs(self.m_ItemLookup) do
--         local templateId=k
--         local disableSprite = transform:Find("Icon/DisableSprite"):GetComponent(typeof(UISprite))
--         if IdPartition.IdIsItem(templateId) then
--             disableSprite.enabled = not CItem.GetMainPlayerIsFit(templateId, false)
--         elseif IdPartition.IdIsEquip(templateId) then
--             disableSprite.enabled = not CEquipment.GetMainPlayerIsFit(templateId, false)
--         end
--     end
-- end

-- function LuaSeaFishingShopWnd:OnReplyScoreShopGoodsRestStock(shopId, itemTemplateId, stock)
--     local transform = self.m_ItemLookup[itemTemplateId]
--     local globalSellOutTag = transform:Find("GlobalSellOutTag").gameObject
--     globalSellOutTag:SetActive(stock <= 0 and self.m_IsOpenGlobalLimit)
--     local selectedId = LuaSeaFishingMgr.shopInfo[self.selectedItemIndex+1]
--     if itemTemplateId == selectedId then
--         self.buyBtn.Enabled = (stock > 0)
--     end
-- end

function LuaSeaFishingShopWnd:InitZswShop()
    local zswMoneyScript = self.ZhuangshiwuShopWnd.transform:Find("ZswCostAndOwnMoney"):GetComponent(typeof(CCommonLuaScript))
    if zswMoneyScript.m_LuaSelf == nil then
        zswMoneyScript:Awake()
    end
    self.zswMoneyCtrl = zswMoneyScript.m_LuaSelf

    local refreshCost = LuaSeaFishingMgr.GetRefreshCost()
    self.refreshBtn:SetActive(refreshCost ~= nil)
    if refreshCost ~= nil then
        self.refreshBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = tostring(refreshCost)
    end

    local nextLevelItems = {}
    local curLevelItems = {}

    local shopGrade = LuaSeaFishingMgr.GetShopGrade()
    local keys = {}
    HouseFishShop_ItemList.ForeachKey(function(k)
        table.insert( keys,k )
    end)
    local goods = {}
    for i,v in ipairs(keys) do
        local data = HouseFishShop_ItemList.GetData(v)
        if data.ShopLv==shopGrade then
            for i=1,data.Goods.Length do
                local k = data.Goods[i-1]
                if HouseFishShop_SaledItem.Exists(k) then
                    table.insert( goods,k )
                end
            end
            break
        end
    end
    --计算开放的总数
    local allGoods = {}
    for i,v in ipairs(keys) do
        local data = HouseFishShop_ItemList.GetData(v)
        if data.ShopLv<=shopGrade then
            for i=1,data.Goods.Length do
                local k = data.Goods[i-1]
                if HouseFishShop_SaledItem.Exists(k) then
                    allGoods[k] = true
                end
            end
        end
    end
    local cnt = 0
    for k,v in pairs(allGoods) do
        if v then
            cnt=cnt+1
        end
    end

    for i,v in ipairs(LuaSeaFishingMgr.zswShopInfo.ItemGoods) do
        if v[2] == true then
            table.insert( nextLevelItems, CItemMgr.Inst:GetItemTemplate(v[1]).Name)
        end
    end

    
    if #nextLevelItems >0 and #goods>0 then
        self.nextNameLabel.gameObject:SetActive(true)

        UIEventListener.Get(self.nextNameLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            g_MessageMgr:ShowMessage("FURNITURE_SHOP_NEXT_LV",
                cnt,
                CItemMgr.Inst:GetItemTemplate(goods[1]).Name,
                #goods,
                table.concat( nextLevelItems, "，"))
        end)
    else
        self.nextNameLabel.gameObject:SetActive(false)
    end

    self:OnMainPlayerPlayPropUpdate()

    self.ZswTable.m_DataSource=DefaultTableViewDataSource.Create(
        function()
            local count = 0
            for i, v in ipairs(LuaSeaFishingMgr.zswShopInfo.ItemGoods) do
                if v[2] == false then count = count + 1 end
            end
            return count
        end,
        function(item,index) self:InitZswItem(item.transform,index) end)

    self.ZswTable.OnSelectAtRow=DelegateFactory.Action_int(function(row)
        self.selectedZswItemIndex = row
        local shopInfo = LuaSeaFishingMgr.zswShopInfo
        local scoreCost = shopInfo.Prices[self.selectedZswItemIndex + 1][1]
        local xhpPrice = shopInfo.Prices[self.selectedZswItemIndex + 1][2]
        local lingyuCost = shopInfo.Prices[self.selectedZswItemIndex + 1][3]
        local xhpId = shopInfo.Prices[self.selectedZswItemIndex + 1][4]
        -- update cost
        local isTempPlayScore = false
        local scoreShopData = Shop_PlayScore.GetData(LuaSeaFishingMgr.zswShopInfo.ShopId)
        if scoreShopData then
            local globalLimit = scoreShopData.GlobalLimit
            if globalLimit and globalLimit ~= "" then
                self.m_IsGlobalLimit = true
                for cronStr in string.gmatch(globalLimit, "([^;]+);?") do
                    local t = {}
                    local itemId,limitCount = string.match(cronStr, "(%d+),(%d+)")
                    t.itemId = tonumber(itemId)
                    t.limitCount = tonumber(limitCount)
                    self.m_GlobalLimitInfo[t.itemId] = t
                end
            end
            isTempPlayScore = scoreShopData.ScoreType == 2--临时玩法积分
        end
    
        local type = lingyuCost > 0 and EnumMoneyType.LingYu or EnumMoneyType.Score
        local scoreKey = LuaSeaFishingMgr.zswShopInfo.PlayScoreKey
        self.buyZswBtn.Text = type == EnumMoneyType.Score and LocalString.GetString("兑换") or LocalString.GetString("购买")
        if isTempPlayScore then
            self.zswMoneyCtrl:InitTempPlayScore(EnumTempPlayScoreKey_lua[scoreShopData.Key])
        else
            if type == EnumMoneyType.Score then
                self.zswMoneyCtrl:SetType(EnumMoneyType.Score, scoreKey, true)
            else
                self.zswMoneyCtrl:SetType(type, EnumPlayScoreKey.NONE, true)
            end
        end
        self.moneytex1:SetActive(type == EnumMoneyType.Score)
        self.moneytex2:SetActive(type == EnumMoneyType.Score)
        self.zswMoneyCtrl:SetCost(type == EnumMoneyType.LingYu and lingyuCost or scoreCost)
        self.zswXiaoHaoPing:SetActive(xhpPrice > 0)
        if xhpPrice > 0 then
            self.zswXiaoHaoPing:GetComponent(typeof(CUITexture)):LoadMaterial(Item_Item.GetData(xhpId).Icon) --消耗品图标
            self.zswXiaoHaoPing.transform:Find("CountLabel"):GetComponent(typeof(UILabel)).text = xhpPrice
            local enough = xhpPrice <= LuaSeaFishingMgr.GetPerlCount()
            self.zswXiaoHaoPing.transform:Find("Mask").gameObject:SetActive(not enough)
            UIEventListener.Get(self.zswXiaoHaoPing).onClick = enough and 
            DelegateFactory.VoidDelegate(function (p)
                CItemInfoMgr.ShowLinkItemTemplateInfo(xhpId, false)
            end) or 
            DelegateFactory.VoidDelegate(function (p)
                CItemAccessListMgr.Inst:ShowItemAccessInfo(xhpId, false, nil, CTooltipAlignType.Top)
            end)
        end
    end)
    self.ZswTable:ReloadData(false, false)
    local selectedcol = 1
    for i = 1, 3 do
        if LuaSeaFishingMgr.zswShopInfo.Buyed[i] ~= true then
            selectedcol = i
            break
        end
    end
    if selectedcol ~= nil then
        self.ZswTable:SetSelectRow(selectedcol-1, true)
    end
    self.TabBar:ChangeTab(1, true)
    self.selectedTab = 1
    self.inited[2] = true
    self.ItemShowWnd:SetActive(false)
    self.ZhuangshiwuShopWnd:SetActive(true)
end

function LuaSeaFishingShopWnd:InitZswItem(transform, row)
    local shopInfo = LuaSeaFishingMgr.zswShopInfo
    local templateId = shopInfo.ItemGoods[row + 1][1]
    self.m_ItemLookup[templateId]=transform

    local icon = transform:Find("ZswIcon"):GetComponent(typeof(CUITexture))
    local nameLabel = transform:Find("ZswNameLabel"):GetComponent(typeof(UILabel))
    local description = transform:Find("ZswDescription"):GetComponent(typeof(UILabel))
    local buyedTag = transform:Find("BuyedTag").gameObject
    local buyedBrush = transform:Find("BuyedBrush").gameObject--笔刷

    icon:Clear()
    icon.alpha = 1

    local item = CItemMgr.Inst:GetItemTemplate(templateId)
    if item ~= nil then
        nameLabel.text = item.Name
        icon:LoadMaterial(item.Icon)
        local desp = string.gmatch(CItem.GetItemDescription(item.ID,true), "(#r.-#r)")()
        description.text = CChatLinkMgr.TranslateToNGUIText(desp, false)
    end
    buyedTag:SetActive(LuaSeaFishingMgr.zswShopInfo.Buyed[row + 1])
    buyedBrush:SetActive(LuaSeaFishingMgr.zswShopInfo.BuyedBrush[templateId] or false)

    transform:GetComponent(typeof(BoxCollider)).enabled = not LuaSeaFishingMgr.zswShopInfo.Buyed[row + 1]
    if not LuaSeaFishingMgr.zswShopInfo.Buyed[row + 1] then
        transform:GetComponent(typeof(UILongPressButton)).OnLongPressDelegate = DelegateFactory.Action(function ()
            CItemInfoMgr.showType = ShowType.Link
            CItemInfoMgr.linkItemInfo = LinkItemInfo(templateId, false, nil)
            CItemInfoMgr.linkItemInfo.alignType = CItemInfoMgr_AlignType.ScreenRight
            CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
        end)
    end
end

function LuaSeaFishingShopWnd:UpdateBuyedState(itemTemplateId)
    for i = 0, 2 do
        local zswTemplateId = LuaSeaFishingMgr.zswShopInfo.ItemGoods[i+1][1]
        if zswTemplateId == itemTemplateId then
            LuaSeaFishingMgr.zswShopInfo.Buyed[i+1] = true
            local zswItem = self.ZswTable.m_Grid.transform:GetChild(i)
            zswItem.transform:Find("BuyedTag").gameObject:SetActive(true)
            zswItem:GetComponent(typeof(BoxCollider)).enabled = false
            break
        end
    end
end

function LuaSeaFishingShopWnd:OnMainPlayerPlayPropUpdate()
    local own = CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.FishingScore)
    local cost = LuaSeaFishingMgr.GetRefreshCost()
    self.OwnLabel.text = (cost == nil or own >= cost) and tostring(own) or System.String.Format("[c][ff0000]{0}[-][/c]", own)
end
