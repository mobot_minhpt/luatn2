--import
local GameObject		= import "UnityEngine.GameObject"
local Vector3           = import "UnityEngine.Vector3"

local UIPanel           = import "UIPanel"
local UITexture			= import "UITexture"
local UILabel			= import "UILabel"
local DelegateFactory   = import "DelegateFactory"
local LuaTweenUtils     = import "LuaTweenUtils"

local CommonDefs        = import "L10.Game.CommonDefs"

local CUIManager        = import "L10.UI.CUIManager"

local Ease              = import "DG.Tweening.Ease"

--define
CLuaBookshelfSearchWnd = class()

--RegistChildComponent
RegistChildComponent(CLuaBookshelfSearchWnd, "Items",		GameObject)
RegistChildComponent(CLuaBookshelfSearchWnd, "Info",		GameObject)
RegistChildComponent(CLuaBookshelfSearchWnd, "Infobg",		GameObject)
RegistChildComponent(CLuaBookshelfSearchWnd, "Icon",		UITexture)
RegistChildComponent(CLuaBookshelfSearchWnd, "Anim",		GameObject)
RegistChildComponent(CLuaBookshelfSearchWnd, "NameLabel",	UILabel)
RegistChildComponent(CLuaBookshelfSearchWnd, "DesLabel",	UILabel)
RegistChildComponent(CLuaBookshelfSearchWnd, "OKBtn",	    GameObject)
RegistChildComponent(CLuaBookshelfSearchWnd, "RetryBtn",	GameObject)

--RegistClassMember
RegistClassMember(CLuaBookshelfSearchWnd, "m_guildIndex")
RegistClassMember(CLuaBookshelfSearchWnd, "m_lastIndex")
RegistClassMember(CLuaBookshelfSearchWnd, "m_ctrls")
RegistClassMember(CLuaBookshelfSearchWnd, "m_tween1")
RegistClassMember(CLuaBookshelfSearchWnd, "m_tween2")
RegistClassMember(CLuaBookshelfSearchWnd, "m_animtween")

CLuaBookshelfSearchWnd.TaskID = 0
function CLuaBookshelfSearchWnd.Open(taskid)
    CLuaBookshelfSearchWnd.TaskID = taskid
    CUIManager.ShowUI(CLuaUIResources.BookshelfSearchWnd)
end

--@region flow function

function CLuaBookshelfSearchWnd:Init()

    self.m_lastIndex = 0
    self.m_guildIndex = 1
    self.m_ctrls = {}

    for i=1,5 do
        local item = self.Items.transform:Find("Item"..i)
        self.m_ctrls[i] = item
        local click = function(go)
            self:OnItemClick(i)
        end
        CommonDefs.AddOnClickListener(item.gameObject,DelegateFactory.Action_GameObject(click), false)
    end

    local okbtnclick=function(go)
        self:OnOKBtnClick()
    end
    CommonDefs.AddOnClickListener(self.OKBtn,DelegateFactory.Action_GameObject(okbtnclick), false)

    local retrybtnclick=function(go)
        self:OnRetryBtnClick()
    end
    CommonDefs.AddOnClickListener(self.RetryBtn,DelegateFactory.Action_GameObject(retrybtnclick), false)

    self:ShowItemGlow(self.m_guildIndex,true)
end

function CLuaBookshelfSearchWnd:OnEnable()
end

function CLuaBookshelfSearchWnd:OnDisable()
    if self.m_tween1 ~= nil then
		LuaTweenUtils.Kill(self.m_tween1,false)
    end
    if self.m_tween2 ~= nil then
		LuaTweenUtils.Kill(self.m_tween2,false)
    end
    
    if self.m_animtween ~= nil then
		LuaTweenUtils.Kill(self.m_animtween,false)
	end
end

--@endregion

--[[
    @desc: 物品点击事件
    author:CodeGize
    time:2019-07-12 11:10:40
    --@index: 
    @return:
]]
function CLuaBookshelfSearchWnd:OnItemClick(index)
    self.Info:SetActive(true)
    self.Infobg:SetActive(true)
    self:ShowInfo(index)
    if self.m_guildIndex == index then
        if self.m_guildIndex > 0 then
            self:ShowItemGlow(self.m_guildIndex,false)
        end
        
        local nextindex = self.m_guildIndex + 1
        self.m_guildIndex = nextindex
        
    end
end

function CLuaBookshelfSearchWnd:OnRetryBtnClick()
    self.Info:SetActive(false)
    self.Infobg:SetActive(false)
    self:ShowItemGlow(self.m_guildIndex,true)
end

function CLuaBookshelfSearchWnd:OnOKBtnClick()
    Gac2Gas.RequestFinishShanGuiTask(CLuaBookshelfSearchWnd.TaskID)
    CUIManager.CloseUI(CLuaUIResources.BookshelfSearchWnd)
end

function CLuaBookshelfSearchWnd:ShowItemGlow(index,tf)
    if index <= 0 or index > 5 then return end
    local ctrl = self.m_ctrls[index]
    ctrl.transform:Find("glow").gameObject:SetActive(tf)
end

function CLuaBookshelfSearchWnd:ShowIcon(index,tf)
    if index <= 0 or index > 5 then return end
    local ctrl = self.Info.transform:Find("Item"..index)
    if ctrl ~= nil then 
        ctrl.gameObject:SetActive(tf)
    end
end

--[[
    @desc: 显示信息窗口
    author:CodeGize
    time:2019-07-12 11:16:35
    --@index: 
    @return:
]]
function CLuaBookshelfSearchWnd:ShowInfo(index)
    local key = "BookShelfItem"..index
    local msg = g_MessageMgr:FormatMessage(key)
    local strs = g_LuaUtil:StrSplit(msg,";")
    self.NameLabel.text = strs[1]
    self.DesLabel.text = strs[2]

    self:ShowIcon(self.m_lastIndex,false)
    self.m_lastIndex = index
    self:ShowIcon(self.m_lastIndex,true)

    local ctrl = self.m_ctrls[index]
    --local mat = ctrl:GetComponent(typeof(UITexture)).material
    --self.Icon.material = mat

    self.OKBtn:SetActive(index == 5)
    self.RetryBtn:SetActive(index ~= 5)

    --缩放和移动动画
    self.Info.transform.localPosition = ctrl.transform.localPosition
    self.Info.transform.localScale = Vector3.zero
    self.m_tween1 = LuaTweenUtils.TweenScaleXY(self.Info.transform,1,1,0.5,Ease.Unset)
    self.m_tween2 = LuaTweenUtils.TweenPosition(self.Info.transform,0,0,0,0.5)
    
    if index == 5 then 
        self:PlayAnim()
    end
end

function CLuaBookshelfSearchWnd:PlayAnim()
    local height = 650
    local trans = self.Anim.transform
    local upbg = trans:Find("Panel/upbg")
    local downbg = trans:Find("Panel/downbg")
    local panel = trans:Find("AnimPanel"):GetComponent(typeof(UIPanel))

    local actived = false 
    self.m_animtween = LuaTweenUtils.TweenFloat(40,height , 1, function (val)
        if not actived then
            self.Anim:SetActive(true)
        end
		local bgpos = upbg.localPosition
		bgpos.y = val / 2
		upbg.localPosition = bgpos
		bgpos.y = -bgpos.y
		downbg.localPosition = bgpos

		local v4 = panel.baseClipRegion
		v4.w = val
		panel.baseClipRegion = v4
		if val >= height then
			self.m_animtween=nil
		end
    end)
    LuaTweenUtils.SetDelay(self.m_animtween,1)
end