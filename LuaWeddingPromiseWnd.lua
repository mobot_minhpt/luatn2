local DelegateFactory = import "DelegateFactory"
local UILabel         = import "UILabel"
local UIInput         = import "UIInput"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"
local CWordFilterMgr  = import "L10.Game.CWordFilterMgr"
local UIEventListener = import "UIEventListener"
local CUIManager      = import "L10.UI.CUIManager"

LuaWeddingPromiseWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingPromiseWnd, "maleName")
RegistClassMember(LuaWeddingPromiseWnd, "femaleName")
RegistClassMember(LuaWeddingPromiseWnd, "input")
RegistClassMember(LuaWeddingPromiseWnd, "desc")
RegistClassMember(LuaWeddingPromiseWnd, "okButton")
RegistClassMember(LuaWeddingPromiseWnd, "okButtonLabel")
RegistClassMember(LuaWeddingPromiseWnd, "fx")

RegistClassMember(LuaWeddingPromiseWnd, "tick")
RegistClassMember(LuaWeddingPromiseWnd, "endTimeStamp")

function LuaWeddingPromiseWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
end

-- 初始化UI组件
function LuaWeddingPromiseWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")

    self.maleName = anchor:Find("Couple/MaleName"):GetComponent(typeof(UILabel))
    self.femaleName = anchor:Find("Couple/FemaleName"):GetComponent(typeof(UILabel))
    self.input = anchor:Find("Promise/InputLabel"):GetComponent(typeof(UIInput))
    self.desc = anchor:Find("Promise/Label"):GetComponent(typeof(UILabel))
    self.okButton = anchor:Find("OKButton")
    self.okButtonLabel = self.okButton:Find("Label"):GetComponent(typeof(UILabel))

    self.fx = self.transform:Find("BgWnd/Fx"):GetComponent(typeof(CUIFx))
end

-- 初始化点击响应
function LuaWeddingPromiseWnd:InitEventListener()
    UIEventListener.Get(self.okButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOKButtonClick()
	end)
end

function LuaWeddingPromiseWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncNewWeddingSceneInfo", self, "OnSyncSceneInfo")
end

function LuaWeddingPromiseWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncNewWeddingSceneInfo", self, "OnSyncSceneInfo")
end

function LuaWeddingPromiseWnd:OnSyncSceneInfo()
    self:InitDisplay()
end


function LuaWeddingPromiseWnd:Init()
    self.endTimeStamp = CServerTimeMgr.Inst.timeStamp + LuaWeddingIterationMgr.promiseCountDown
    self.fx:LoadFx(g_UIFxPaths.WeddingPromiseLightFx)
    self.desc.text = g_MessageMgr:FormatMessage("WEDDING_XUANYAN_CONTENT")
    self:StartTick()
    self:InitDisplay()
end

-- 初始化显示
function LuaWeddingPromiseWnd:InitDisplay()
    local sceneInfo = LuaWeddingIterationMgr.sceneInfo
    if sceneInfo.groomName then
        self.maleName.text = sceneInfo.groomName
    else
        self.maleName.text = ""
    end

    if sceneInfo.brideName then
        self.femaleName.text = sceneInfo.brideName
    else
        self.maleName.text = ""
    end
end

-- 开始倒计时
function LuaWeddingPromiseWnd:StartTick()
    self:UpdateCountDown()
    self:ClearTick()
    self.tick = RegisterTick(function()
		self:UpdateCountDown()
	end, 1000)
end

-- 更新时间
function LuaWeddingPromiseWnd:UpdateCountDown()
    local count = math.floor(self.endTimeStamp - CServerTimeMgr.Inst.timeStamp)
    if count > 0 then
        self.okButtonLabel.text = SafeStringFormat3(LocalString.GetString("我愿意(%d)"), count)
    else
        local ret = CWordFilterMgr.Inst:DoFilterOnSend(self.input.value, nil, LocalString.GetString("拜堂宣言"), true)
        local msg = ret.msg

        if msg == nil or ret.shouldBeIgnore or msg == "" then
            msg = g_MessageMgr:FormatMessage("Default_Marriage_Message")
        end

        Gac2Gas.RequestNewWeddingDeclare(msg)
        CUIManager.CloseUI(CLuaUIResources.WeddingPromiseWnd)
    end
end

-- 清除计时器
function LuaWeddingPromiseWnd:ClearTick()
	if self.tick then
		UnRegisterTick(self.tick)
		self.tick = nil
	end
end

-- 关闭界面时清除计时器
function LuaWeddingPromiseWnd:OnDestroy()
	self:ClearTick()
end

--@region UIEvent

function LuaWeddingPromiseWnd:OnOKButtonClick()
    -- 滤除关键字
    local ret = CWordFilterMgr.Inst:DoFilterOnSend(self.input.value, nil, LocalString.GetString("拜堂宣言"), true)
    local msg = ret.msg

    if msg == nil or ret.shouldBeIgnore then
        return
    end

    if msg == "" then
        msg = g_MessageMgr:FormatMessage("Default_Marriage_Message")
    end
    Gac2Gas.RequestNewWeddingDeclare(msg)
    CUIManager.CloseUI(CLuaUIResources.WeddingPromiseWnd)
end

--@endregion UIEvent
