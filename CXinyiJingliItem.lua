-- Auto Generated!!
local CJingLingWebImageMgr = import "L10.Game.CJingLingWebImageMgr"
local CXinyiJingliItem = import "L10.UI.CXinyiJingliItem"
local DelegateFactory = import "DelegateFactory"
--lsy 3
CXinyiJingliItem.m_Init_CS2LuaHook = function (this, _data) 
    this.data = _data
    this.CountLabel.text = _data.renqi
    CJingLingWebImageMgr.Inst:GetImageByUrl(this.data.avatar, DelegateFactory.Action_Texture2D(function (tex) 
        this.Photo.mainTexture = tex
    end))
end
