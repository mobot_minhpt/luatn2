-- Auto Generated!!
local CJXYSBossItem = import "L10.UI.CJXYSBossItem"
local CJXYSBossWnd = import "L10.UI.CJXYSBossWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CTeamMatchMgr = import "L10.Game.CTeamMatchMgr"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local Int32 = import "System.Int32"
local JiangXueYuanShuang_MapInfo = import "L10.Game.JiangXueYuanShuang_MapInfo"
local JXYSItemInfo = import "L10.UI.JXYSItemInfo"
local Task_QueryNpc = import "L10.Game.Task_QueryNpc"
--local TeamMatch_Activities = import "L10.Game.TeamMatch_Activities"
local UInt32 = import "System.UInt32"

local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local CJXYSMgr = import "L10.Game.CJXYSMgr"
local DelegateFactory = import "DelegateFactory"
local ETickType = import "L10.Engine.ETickType"
local L10 = import "L10"

CJXYSBossWnd.m_Init_CS2LuaHook = function (this) 
    this.mInfoList = CreateFromClass(MakeGenericClass(List, JXYSItemInfo))
    this.m_TeleportRemains = CreateFromClass(MakeGenericClass(Dictionary, Int32, Int32))
    this.tableView.m_DataSource = this
    do
        local i = 0
        while i < CJXYSBossWnd.JXYS_TELEPORTER_COUNT do
            local info = CreateFromClass(JXYSItemInfo)
            info.Index = i
            info.TeleporterFloor = (i * 10 + 1)
            local mapInfo = JiangXueYuanShuang_MapInfo.GetData(info.TeleporterFloor)
            if mapInfo ~= nil then
                info.NeedItemId = mapInfo.TeleportItemId
            end
            info.AppearFloor = CreateFromClass(MakeGenericClass(List, UInt32))
            info.Count = 0
            CommonDefs.ListAdd(this.mInfoList, typeof(JXYSItemInfo), info)
            i = i + 1
        end
    end
    this:GetDynamicActivityInfo()

    local refreshBossCronStr = JiangXueYuanShuang_Setting.GetData().RefreshBossCronStr 
    local currentTime = CServerTimeMgr.Inst:GetZone8Time()--self
    local curDayInWeek = EnumToInt(currentTime.DayOfWeek)
    local curHour = currentTime.Hour
    local curMin = currentTime.Minute
    local targetDayInWeek
    local targetTime
    local refreshBossTimeTbl = {}

    for cronStr in string.gmatch(refreshBossCronStr, "([^;]+);?") do
        local t = {}
        local min,hour,day,month,weekday = string.match(cronStr, "([^ ]+) ([^ ]+) ([^ ]+) ([^ ]+) ([^ ]+)")
        t.min, t.hour, t.day, t.month, t.weekday = tonumber(min),tonumber(hour),tonumber(day),tonumber(month),tonumber(weekday)
        table.insert(refreshBossTimeTbl,t)
    end
    table.sort(refreshBossTimeTbl, 
        function(a, b) 
            local r
            if a.weekday == b.weekday then        
                if a.hour == b.hour then
                    r = a.min < b.min
                else
                    r = a.hour < b.hour
                end
            else
                r = a.weekday < b.weekday
            end
            return r 
        end
    )

    local lastIndex = #refreshBossTimeTbl
    local isFindTarget = false
    do
        local index = 1
        while index <= lastIndex do
            local weekday = refreshBossTimeTbl[index].weekday
            local hour = refreshBossTimeTbl[index].hour
            local min = refreshBossTimeTbl[index].min
            if curDayInWeek > weekday then
            elseif curDayInWeek == weekday then
                if curHour < hour or (curHour == hour and curMin <= min) then                  
                    isFindTarget = true
                    targetDayInWeek = weekday
                    targetTime = refreshBossTimeTbl[index]
                    CJXYSMgr.TargetDateTime = DateTime(currentTime.Year,currentTime.Month,currentTime.Day,hour,min,0)
                    break
                end
            else
                isFindTarget = true
                targetDayInWeek = weekday
                targetTime = refreshBossTimeTbl[index]
                local dayDiff = weekday - curDayInWeek
                CJXYSMgr.TargetDateTime = DateTime(currentTime.Year,currentTime.Month,currentTime.Day,hour,min,0):AddDays(dayDiff)
                break
            end
            index = index + 1
        end
    end
    if not isFindTarget then
        targetTime = refreshBossTimeTbl[1]
        targetDayInWeek = targetTime.weekday
        local dayDiff = 7 - curDayInWeek + targetDayInWeek
        CJXYSMgr.TargetDateTime = DateTime(currentTime.Year,currentTime.Month,currentTime.Day,targetTime.hour,targetTime.min,0):AddDays(dayDiff)
    end
    local timespan = CommonDefs.op_Subtraction_DateTime_DateTime(CJXYSMgr.TargetDateTime, currentTime)
    CJXYSMgr.RemainMinutes = math.ceil(timespan.TotalSeconds/60)
   
    Gac2Gas.RequestJXYSRemainInfo()

    UIEventListener.Get(this.transform:Find("InfoButton").gameObject).onClick = DelegateFactory.VoidDelegate(function() g_MessageMgr:ShowMessage("JXYS_RULES_TIPS") end)
end
CJXYSBossWnd.m_GetDynamicActivityInfo_CS2LuaHook = function (this) 
    local data = Task_QueryNpc.GetData(CJXYSBossWnd.activityType)
    if data ~= nil then
        this.titleLabel.text = data.ActivityName
        Gac2Gas.GetDynamicActivityInfo(CJXYSBossWnd.activityType)
    end
end
CJXYSBossWnd.m_GetTeamMatchId_CS2LuaHook = function (this) 
    local findId = 0
    local data = Task_QueryNpc.GetData(CJXYSBossWnd.activityType)
    if data ~= nil then
        TeamMatch_Activities.Foreach(function (key, val) 
            if val.LinkedScheduleID == data.ScheduleID then
                findId = key
            end
        end)
    end
    return findId
end
CJXYSBossWnd.m_OnCreateTeamBtnClicked_CS2LuaHook = function (this, go) 
    if CTeamMgr.Inst:TeamExists() then
        if not CUIManager.IsLoaded(CUIResources.TeamWnd) then
            CUIManager.ShowUI(CUIResources.TeamWnd)
        end
    else
        local findId = this:GetTeamMatchId()
        CTeamMatchMgr.Inst:CreateTeam(findId)
    end
end
CJXYSBossWnd.m_GetFloorIdByMapId_CS2LuaHook = function (this, mapId) 
    local result = 0
    JiangXueYuanShuang_MapInfo.ForeachKey(DelegateFactory.Action_object(function (p) 
        local mapInfo = JiangXueYuanShuang_MapInfo.GetData(p)
        if mapInfo.MapId == mapId then
            result = mapInfo.ID
        end
    end))
    return result
end
CJXYSBossWnd.m_OnTeamInfoChanged_CS2LuaHook = function (this) 

    if CTeamMgr.Inst:TeamExists() then
        this:Close()
        if not CUIManager.IsLoaded(CUIResources.TeamWnd) then
            CUIManager.ShowUI(CUIResources.TeamWnd)
        end
    end
end
CJXYSBossWnd.m_GetTeleportRemains_CS2LuaHook = function (this, floor) 
    if this.m_TeleportRemains ~= nil then
        if CommonDefs.DictContains(this.m_TeleportRemains, typeof(Int32), floor) then
            return CommonDefs.DictGetValue(this.m_TeleportRemains, typeof(Int32), floor)
        end
    end
    return 0
end
CJXYSBossWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if row < this.mInfoList.Count then
        local item = TypeAs(this.tableView:GetFromPool(0), typeof(CJXYSBossItem))
        if item ~= nil then
            item:Init(this.mInfoList[row], this:GetTeleportRemains(this.mInfoList[row].TeleporterFloor))
            return item
        end
    end
    return nil
end
CJXYSBossWnd.m_hookUpdate = function (this)
    local currentTime = CServerTimeMgr.Inst:GetZone8Time()
    if currentTime and CJXYSMgr.TargetDateTime then
        local timespan = CommonDefs.op_Subtraction_DateTime_DateTime(CJXYSMgr.TargetDateTime, currentTime)
        CJXYSMgr.RemainMinutes = math.ceil(timespan.TotalSeconds/60)
        if CJXYSMgr.RemainMinutes < 0 and this.cancelTick == nil then
            this:CancelTick()
            Gac2Gas.GetDynamicActivityInfo(CJXYSBossWnd.activityType)
            this.cancelTick = L10.Engine.CTickMgr.Register(DelegateFactory.Action(function () 
                Gac2Gas.GetDynamicActivityInfo(CJXYSBossWnd.activityType)
            end), 1500, ETickType.Loop)
        end       
    end
end
CJXYSBossWnd.m_hookCancelTick = function (this)
    if this.cancelTick ~= nil then
        invoke(this.cancelTick)
        this.cancelTick = nil
    end
end
CJXYSBossWnd.m_hookOnReplyDynamicActivityInfo = function (this, type, currentCount, maxCount, list)  
    this:ProcessJXYSInfos(list)
    this.tableView:ReloadData(true)

    if this.mInfoList and this.cancelTick then
        for i=0,this.mInfoList.Count -1 do
            if this.mInfoList[i].LeftNum and this.mInfoList[i].LeftNum ~= 0 then
                this:CancelTick()
                break
            end
        end
    end
end
