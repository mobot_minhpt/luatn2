local Extensions = import "Extensions"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"

LuaZongMenGuiZeWnd = class()

RegistChildComponent(LuaZongMenGuiZeWnd,"m_TitleLabel","TitleLabel", UILabel)
RegistChildComponent(LuaZongMenGuiZeWnd,"m_Template","Template", GameObject)
RegistChildComponent(LuaZongMenGuiZeWnd,"m_Grid","Grid", UIGrid)
RegistChildComponent(LuaZongMenGuiZeWnd,"m_BgTexture","BgTexture", CUITexture)

function LuaZongMenGuiZeWnd:Init()
    self.m_Template:SetActive(false)
    self.m_TitleLabel.text = LuaZongMenMgr.m_GuiZeTitle
    Extensions.RemoveAllChildren(self.m_Grid.transform)
    local textArray = {LocalString.GetString("一"),LocalString.GetString("二"),LocalString.GetString("三"),LocalString.GetString("四"),
    LocalString.GetString("五"),LocalString.GetString("六"),LocalString.GetString("七")}
    local j = 1
    for i = 0, LuaZongMenMgr.m_GuiZe.Length - 1 do
        local id = tonumber(LuaZongMenMgr.m_GuiZe[i])
        local data = nil 
        if LuaZongMenMgr.m_GuiZeIsRule then
            data = Menpai_Rules.GetData(id) 
        else
            data = Menpai_JoinStandard.GetData(id)
        end
        if data then
            local paragraphGo = CUICommonDef.AddChild(self.m_Grid.gameObject, self.m_Template)
            CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem)):Init(data and data.Description or "---", 4294967295)
            paragraphGo.transform:Find("NumberSprite/Label"):GetComponent(typeof(UILabel)).text = textArray[j]
            paragraphGo:SetActive(true)
            j = j + 1
        end
    end
    local data = Menpai_SkyBoxKind.GetData(LuaZongMenMgr.m_GuiZeRaidMapId)
    self.m_BgTexture:LoadMaterial(data.Preview)
    self.m_Grid:Reposition()
end
