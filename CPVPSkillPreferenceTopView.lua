-- Auto Generated!!
local Boolean = import "System.Boolean"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuaJiMgr = import "L10.Game.CGuaJiMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CPVPSkillPreferenceTopView = import "L10.UI.CPVPSkillPreferenceTopView"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CSkillItemCell = import "L10.UI.CSkillItemCell"
local CSkillMgr = import "L10.Game.CSkillMgr"
local CTianFuSkillItemCell = import "L10.UI.CTianFuSkillItemCell"
local DelegateFactory = import "DelegateFactory"
local EnumTianFuSkillType = import "L10.Game.EnumTianFuSkillType"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local SkillKind = import "L10.Game.SkillKind"
local SkillSeriesInfo = import "L10.Game.SkillSeriesInfo"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local UISprite = import "UISprite"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local __Skill_AllSkills_Template = import "L10.Game.__Skill_AllSkills_Template"

CPVPSkillPreferenceTopView.m_Start_CS2LuaHook = function (this) 
    this.skillTemplate:SetActive(false)
    this.scaleTemplate:SetActive(false)
    this.arrowTemplate:SetActive(false)
    this.tianfuSkillTemplate:SetActive(false)
    this.clonedIcon:SetActive(false)
    UIEventListener.Get(this.tianfuAdjustBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.tianfuAdjustBtn).onClick, MakeDelegateFromCSFunction(this.OnTianFuAdjustButtonClick, VoidDelegate, this), true)
end
CPVPSkillPreferenceTopView.m_checkSkillAllowed_CS2LuaHook = function (this, skillId) 
    if LuaPVPMgr.IsRobotOpen() then
        local mainPlayer = CClientMainPlayer.Inst
        local skills
        if mainPlayer and mainPlayer.IsYingLing then
            skills = LuaPVPMgr.GetAutoAISkills(LuaPVPMgr.GetCurrentYingLingState())
        else
            skills = LuaPVPMgr.GetAutoAISkills()
        end
    
        for i = 1, #skills do
            if skills[i] == skillId then
                return true
            end
        end
        return false
    else
        if CommonDefs.HashSetContains(CGuaJiMgr.Inst.AllowedAISkill, typeof(UInt32), skillId) then
            return true
        end
        return false
    end
end
CPVPSkillPreferenceTopView.m_Init_CS2LuaHook = function (this, index) 
    if not this.inited or this.skillSeries == nil then
        this.inited = true
        this.tabBar.OnTabChange = MakeDelegateFromCSFunction(this.OnTabChange, MakeGenericClass(Action2, GameObject, Int32), this)
        if CClientMainPlayer.Inst ~= nil then
            if this.skillSeries == nil then
                this.skillSeries = CClientMainPlayer.Inst.SkillSeries
            end
            if this.skillSeries ~= nil then
                do
                    local i = 0
                    while i < this.skillSeries.Length do
                        if this.skillSeries[i].Value == LocalString.GetString("绝技") then
                            this.skillSeries[i] = CreateFromClass(SkillSeriesInfo, this.skillSeries[i].Key, LocalString.GetString("天赋技能"))
                            break
                        end
                        i = i + 1
                    end
                end
                local labels = CreateFromClass(MakeArrayClass(System.String), this.skillSeries.Length)

                do
                    local i = 0
                    while i < labels.Length do
                        labels[i] = this.skillSeries[i].Value
                        i = i + 1
                    end
                end
                this.tabBar:InitWithLabels(labels)
            end
        end
    end
    this.tabBar:ChangeTab(index, false)
end
CPVPSkillPreferenceTopView.m_OnTabChange_CS2LuaHook = function (this, go, index) 
    if this.skillSeries == nil then
        return
    end
    if this.tabBar.SelectedIndex == this.skillSeries.Length - 1 then
        this.tianfuRoot:SetActive(true)
        this.skillMapRoot:SetActive(false)
        this:LoadTianFuSkills()
        if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.SkillProp.TianFuPointUsed == 0 and CClientMainPlayer.Inst.BasicProp.XiuWeiGrade >= Constants.TianFuSkillUnlockLevel then
            CSkillInfoMgr.ShowTianFuSkillWnd(EnumTianFuSkillType.MPTZ, -1)
        end
    else
        this.tianfuRoot:SetActive(false)
        this.skillMapRoot:SetActive(true)
        this:LoadCommonSkills()
    end
end
CPVPSkillPreferenceTopView.m_LoadCommonSkills_CS2LuaHook = function (this) 
    this:Clear()
    if this.skillSeries == nil or this.tabBar.SelectedIndex < 0 or this.tabBar.SelectedIndex >= this.skillSeries.Length - 1 then
        return
    end
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer ~= nil then
        local info = CSkillMgr.Inst:GetPlayerProfessionSkillInfo(mainPlayer.Class, mainPlayer.SkillProp, this.skillSeries[this.tabBar.SelectedIndex].Key, mainPlayer.Level)
        local professionSkillIds = info.professionSkillIds
        local unlockLevels = info.unlockLevels
        local layoutDict = info.layoutDict

        if unlockLevels.Count == 0 then
            return
        end
        CommonDefs.ListSort1(unlockLevels, typeof(UInt32), DelegateFactory.Comparison_uint(function (level1, level2) 
            return NumberCompareTo(level1, level2)
        end))

        local horizontalGap = 900 / math.max(2, unlockLevels.Count - 1)

        do
            local i = 0
            while i < unlockLevels.Count do
                local item = NGUITools.AddChild(this.scaleLine.gameObject, this.scaleTemplate)
                item:SetActive(true)
                Extensions.SetLocalPositionX(item.transform, CommonDefs.DictGetValue(layoutDict, typeof(UInt32), unlockLevels[i]) * 900 --[[TotalLength]] + 50 --[[LeftOffset]])
                CommonDefs.GetComponent_GameObject_Type(item, typeof(UILabel)).text = tostring(unlockLevels[i])
                CommonDefs.GetComponentInChildren_GameObject_Type(item, typeof(UISprite)):ResetAndUpdateAnchors()
                i = i + 1
            end
        end

        if LuaSkillMgr.CheckMainPlayerYingLingBianShenSkill() then
            professionSkillIds = LuaSkillMgr.GetYingLingNewProfessionSkillIds(professionSkillIds, LuaPVPMgr.GetCurrentYingLingState() )
        end


        do
            local i = 0
            while i < professionSkillIds.Count do
                local item = NGUITools.AddChild(this.scaleLine.gameObject, this.skillTemplate)
                item:SetActive(true)
                local data = Skill_AllSkills.GetData(professionSkillIds[i].Key)
                local cell = CommonDefs.GetComponent_GameObject_Type(item, typeof(CSkillItemCell))
                local clsExists = mainPlayer.SkillProp:IsOriginalSkillClsExist(data.SkillClass)

                local originSkillId = mainPlayer.SkillProp:GetOriginalSkillIdByCls(data.SkillClass)
                local originSkillIdWithDeltaLevel = mainPlayer.SkillProp:GetSkillIdWithDeltaByCls(data.SkillClass, mainPlayer.Level)
                local originLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(originSkillId) or 0
                local originWithDeltaLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(originSkillIdWithDeltaLevel) or 0

                local showSign = this:checkSkillAllowed(math.floor(professionSkillIds[i].Key / 100))

                local feishengLevel = clsExists and mainPlayer.SkillProp:GetFeiShengModifyLevel(originSkillId, data.Kind, mainPlayer.Level) or 0
                local delta = originWithDeltaLevel - feishengLevel
                if mainPlayer.IsInXianShenStatus then
                    cell:InitFeiSheng(true, feishengLevel)
                else
                    cell:InitFeiSheng(false, 0)
                end

                cell:Init(data.SkillClass, originLevel, delta, true, data.SkillIcon, (originLevel == 0) or not showSign, nil)



                if showSign then
                    cell.OnDragStart = MakeDelegateFromCSFunction(this.OnDragSkillIconStart, MakeGenericClass(Action2, GameObject, UInt32), this)
                end
                cell.OnPress = MakeDelegateFromCSFunction(this.OnPressSkillIcon, MakeGenericClass(Action3, GameObject, Boolean, UInt32), this)
                UIEventListener.Get(item).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(item).onClick, MakeDelegateFromCSFunction(this.OnSkillItemClick, VoidDelegate, this), true)
                item.transform.localPosition = Vector3((data.Layout[1] / 1000) * 900 --[[TotalLength]] + 50 --[[LeftOffset]], data.Layout[0] > 0 and 100 or - 100, 0)
                CommonDefs.DictAdd(this.itemCellDict, typeof(UInt32), cell.ClassId, typeof(CSkillItemCell), cell)
                CommonDefs.ListAdd(this.itemCells, typeof(CSkillItemCell), cell)
                i = i + 1
            end
        end

        do
            local i = 0
            while i < professionSkillIds.Count do
                local data = Skill_AllSkills.GetData(professionSkillIds[i].Key)
                local pairs = data.AdjustedPreSkills
                if pairs ~= nil and pairs.Length > 0 then
                    do
                        local j = 0
                        while j < pairs.Length do
                            local realFromSkillCls = pairs[j][0]
                            if CSkillMgr.Inst:IsColorSystemBaseSkill(realFromSkillCls) then
                                realFromSkillCls = mainPlayer.SkillProp:GetInEffectColorSystemSkill(realFromSkillCls)
                            end

                             -- 影灵破云击特殊处理
                            if mainPlayer.IsYingLing then
                                local cls = LuaSkillMgr.GetYingLingRealSkillCls(realFromSkillCls,LuaPVPMgr.m_CurrentYingLingState)
                                if cls then
                                    realFromSkillCls = cls
                                end
                            end

                            if CommonDefs.DictContains(this.itemCellDict, typeof(UInt32), realFromSkillCls) then
                                --生成一条箭头
                                local item = NGUITools.AddChild(this.scaleLine.gameObject, this.arrowTemplate)
                                item:SetActive(true)
                                local sprite = CommonDefs.GetComponent_GameObject_Type(item, typeof(UISprite))
                                local from = CommonDefs.DictGetValue(this.itemCellDict, typeof(UInt32), realFromSkillCls)
                                local to = CommonDefs.DictGetValue(this.itemCellDict, typeof(UInt32), CLuaDesignMgr.Skill_AllSkills_GetClass(professionSkillIds[i].Key))
                                sprite.width = math.floor((to.transform.localPosition.x - from.transform.localPosition.x - to.Width * 0.5 - from.Width * 0.5))
                                sprite.transform.localPosition = CommonDefs.op_Addition_Vector3_Vector3(from.transform.localPosition, CommonDefs.op_Multiply_Vector3_Single(CommonDefs.op_Multiply_Vector3_Single(Vector3.right, from.Width), 0.5))
                            end
                            j = j + 1
                        end
                    end
                end
                i = i + 1
            end
        end

        if this.itemCells.Count > 0 then
            local idx = 0
            --for (int i = 0; i < items.Count; ++i)
            --{
            --    if (items[i].ClassId == CSkillInfoMgr.updateSkillInfo.skillClass)
            --    {
            --        idx = i;
            --        break;
            --    }
            --}
            this:OnSkillItemClick_WithShowTipOption(this.itemCells[idx].gameObject, false)
        end
    end
end
CPVPSkillPreferenceTopView.m_LoadTianFuSkills_CS2LuaHook = function (this) 
    this:Clear()
    if this.skillSeries == nil or this.tabBar.SelectedIndex ~= this.skillSeries.Length - 1 then
        return
    end

    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer ~= nil then
        local mptzTianFuSkillMap = CreateFromClass(MakeGenericClass(Dictionary, UInt32, UInt32))
        local skillIds = mainPlayer.SkillProp.TianFuSkillForAI
        do
            local i = 0
            while i < Constants.NumOfAvailableMPTZTianFuSkills do
                local skillCls = CLuaDesignMgr.Skill_AllSkills_GetClass(skillIds[i+LuaPVPMgr.GetTianFuBaseIndex()])
                if Skill_AllSkills.Exists(skillIds[i+LuaPVPMgr.GetTianFuBaseIndex()]) then
                    if not CommonDefs.DictContains(mptzTianFuSkillMap, typeof(UInt32), skillCls) then
   
                        CommonDefs.DictAdd(mptzTianFuSkillMap, typeof(UInt32), skillCls, typeof(UInt32), skillIds[i+LuaPVPMgr.GetTianFuBaseIndex()])
                    end
                end
                i = i + 1
            end
        end

        --获取mainPlayer的所有职业技能
        local prefix = 900 + EnumToInt(mainPlayer.Class)
        local needXiuWeiLevels = CreateFromClass(MakeGenericClass(List, Int32))
        local tianfuSkills = CreateFromClass(MakeGenericClass(Dictionary, Int32, UInt32))
        __Skill_AllSkills_Template.ForeachKey(DelegateFactory.Action_object(function (cls)
            if math.floor(cls / 1000) == prefix then
                --以(900+ClassId)开头，以01结尾
                local skillId = CLuaDesignMgr.Skill_AllSkills_GetSkillId(cls, 1)
                local data = Skill_AllSkills.GetData(skillId)
                if data.EKind ~= SkillKind.TianFuSkill then
                    return
                end
                local cls = CLuaDesignMgr.Skill_AllSkills_GetClass(skillId)
                if not CommonDefs.ListContains(needXiuWeiLevels, typeof(Int32), data.NeedXiuweiLevel) then
                    CommonDefs.ListAdd(needXiuWeiLevels, typeof(Int32), data.NeedXiuweiLevel)
                end
                if CommonDefs.DictContains(mptzTianFuSkillMap, typeof(UInt32), cls) then
                    CommonDefs.DictAdd(tianfuSkills, typeof(Int32), data.NeedXiuweiLevel, typeof(UInt32), CommonDefs.DictGetValue(mptzTianFuSkillMap, typeof(UInt32), cls))
                end
            end
        end))
        CommonDefs.ListSort1(needXiuWeiLevels, typeof(Int32), DelegateFactory.Comparison_int(function (level1, level2) 
            return NumberCompareTo(level1, level2)
        end))

        do
            local i = 0
            while i < needXiuWeiLevels.Count do
                local item = NGUITools.AddChild(this.tianfuGrid.gameObject, this.tianfuSkillTemplate)
                item:SetActive(true)
                local cell = CommonDefs.GetComponent_GameObject_Type(item, typeof(CTianFuSkillItemCell))
                if CommonDefs.DictContains(tianfuSkills, typeof(Int32), needXiuWeiLevels[i]) then
                    local skillId = CommonDefs.DictGetValue(tianfuSkills, typeof(Int32), needXiuWeiLevels[i])
                    local data = Skill_AllSkills.GetData(skillId)

                    local level = CLuaDesignMgr.Skill_AllSkills_GetLevel(skillId)
                    if mainPlayer.IsInXianShenStatus then
                        --uint originSkillId = mainPlayer.SkillProp.GetOriginalSkillIdByCls(skillId / 100);
                        local feishengLevel = mainPlayer.SkillProp:GetFeiShengModifyLevel(skillId, data.Kind, mainPlayer.Level)
                        cell:InitFeiSheng(true, feishengLevel)
                    else
                        cell:InitFeiSheng(false, 0)
                    end
                    cell:Init(data.SkillClass, level, needXiuWeiLevels[i], data.SkillIcon, mainPlayer.BasicProp.XiuWeiGrade < needXiuWeiLevels[i])
                    --cell.OnDragStart = this.OnDragSkillIconStart;
                    cell.OnPress = MakeDelegateFromCSFunction(this.OnPressSkillIcon, MakeGenericClass(Action3, GameObject, Boolean, UInt32), this)
                    UIEventListener.Get(item).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(item).onClick, MakeDelegateFromCSFunction(this.OnSkillItemClick, VoidDelegate, this), true)
                else
                    cell:Init(0, 0, needXiuWeiLevels[i], nil, mainPlayer.BasicProp.XiuWeiGrade < needXiuWeiLevels[i])
                end
                CommonDefs.ListAdd(this.tianfuItemCells, typeof(CTianFuSkillItemCell), cell)
                i = i + 1
            end
        end
        this.tianfuGrid:Reposition()
        if this.itemCells.Count > 0 then
            local idx = 0
            --for (int i = 0; i < items.Count; ++i)
            --{
            --    if (items[i].ClassId == CSkillInfoMgr.updateSkillInfo.skillClass)
            --    {
            --        idx = i;
            --        break;
            --    }
            --}
            this:OnSkillItemClick_WithShowTipOption(this.itemCells[idx].gameObject, false)
        end
    end
end
CPVPSkillPreferenceTopView.m_OnPressSkillIcon_CS2LuaHook = function (this, go, pressed, skillClassId) 
    if pressed then
        this:OnSkillItemClick_WithShowTipOption(go, false)
        --CSkillInfoMgr.ShowSkillInfoWnd(CLuaDesignMgr.Skill_AllSkills_GetSkillId(skillClassId, skillLevel == 0 ? 1 : skillLevel), tipTopPos.position);
    end
end
CPVPSkillPreferenceTopView.m_UpdateSkills_CS2LuaHook = function (this) 
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer ~= nil then
        if this.tabBar.SelectedIndex ~= this.skillSeries.Length - 1 and mainPlayer.IsYingLing and LuaPVPMgr.IsRobotOpen() then
            this:LoadCommonSkills()
        end
        
        if this.itemCells.Count > 0 then
            do
                local i = 0
                while i < this.itemCells.Count do
                    local classId = this.itemCells[i].ClassId
                    local clsExists = mainPlayer.SkillProp:IsOriginalSkillClsExist(classId)
                    local originSkillId = mainPlayer.SkillProp:GetOriginalSkillIdByCls(classId)
                    local originSkillIdWithDeltaLevel = mainPlayer.SkillProp:GetSkillIdWithDeltaByCls(classId, mainPlayer.Level)
                    local originLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(originSkillId) or 0
                    local originWithDeltaLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(originSkillIdWithDeltaLevel) or 0
                    local feishengLevel = clsExists and mainPlayer.SkillProp:GetFeiShengModifyLevel(originSkillId, EnumToInt(this.itemCells[i].Kind), mainPlayer.Level) or 0
                    local delta = originWithDeltaLevel - feishengLevel
                    if mainPlayer.IsInXianShenStatus then
                        this.itemCells[i]:InitFeiSheng(true, feishengLevel)
                    else
                        this.itemCells[i]:InitFeiSheng(false, 0)
                    end
                    local showSign = this:checkSkillAllowed(classId)
                    this.itemCells[i]:UpdateLevel(classId, originLevel, delta, true, (originLevel == 0) or not showSign, nil)
                    i = i + 1
                end
            end
            if this.OnSkillClassSelect ~= nil and this.selectedIndex >= 0 and this.selectedIndex < this.itemCells.Count then
                GenericDelegateInvoke(this.OnSkillClassSelect, Table2ArrayWithCount({this.itemCells[this.selectedIndex].ClassId}, 1, MakeArrayClass(Object)))
            end
        elseif this.tianfuItemCells.Count > 0 then
            this:LoadTianFuSkills()
        end
    end
end
CPVPSkillPreferenceTopView.m_Clear_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.scaleLine.transform)
    Extensions.RemoveAllChildren(this.tianfuGrid.transform)
    CommonDefs.DictClear(this.itemCellDict)
    CommonDefs.ListClear(this.itemCells)
    CommonDefs.ListClear(this.tianfuItemCells)
end
CPVPSkillPreferenceTopView.m_OnSkillItemClick_WithShowTipOption_CS2LuaHook = function (this, go, showTip) 
    if this.itemCells.Count > 0 then
        do
            local i = 0
            while i < this.itemCells.Count do
                if go:Equals(this.itemCells[i].gameObject) then
                    this.selectedIndex = i
                    this.itemCells[i].Selected = true
                    if showTip then
                        CSkillInfoMgr.ShowSkillInfoWnd(CLuaDesignMgr.Skill_AllSkills_GetSkillId(this.itemCells[i].ClassId, this.itemCells[i].OriginWithDeltaLevel == 0 and 1 or this.itemCells[i].OriginWithDeltaLevel), this.tipTopPos.transform.position, CSkillInfoMgr.EnumSkillInfoContext.MainPlayerMPTZ, true, 0, 0, nil)
                    end
                else
                    this.itemCells[i].Selected = false
                end
                i = i + 1
            end
        end
        if this.OnSkillClassSelect ~= nil then
            GenericDelegateInvoke(this.OnSkillClassSelect, Table2ArrayWithCount({this.itemCells[this.selectedIndex].ClassId}, 1, MakeArrayClass(Object)))
        end
    else
        do
            local i = 0
            while i < this.tianfuItemCells.Count do
                if go:Equals(this.tianfuItemCells[i].gameObject) then
                    this.selectedIndex = i
                    this.tianfuItemCells[i].Selected = true
                    if showTip then
                        CSkillInfoMgr.ShowSkillInfoWnd(CLuaDesignMgr.Skill_AllSkills_GetSkillId(this.tianfuItemCells[i].ClassId, this.tianfuItemCells[i].OriginLevel), this.tipTopPos.transform.position, CSkillInfoMgr.EnumSkillInfoContext.MainPlayerMPTZ, true, 0, 0, nil)
                    end
                else
                    this.tianfuItemCells[i].Selected = false
                end
                i = i + 1
            end
        end
        if this.OnSkillClassSelect ~= nil then
            GenericDelegateInvoke(this.OnSkillClassSelect, Table2ArrayWithCount({this.tianfuItemCells[this.selectedIndex].ClassId}, 1, MakeArrayClass(Object)))
        end
    end
end
