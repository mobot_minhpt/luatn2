local CScene=import "L10.Game.CScene"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local Utility = import "L10.Engine.Utility"
local CMiniMap = import "L10.UI.CMiniMap"
local CUIGameObjectPool = import "L10.UI.CUIGameObjectPool"
local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaTreasureSeaMiniMapWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaTreasureSeaMiniMapWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaTreasureSeaMiniMapWnd, "PlayerTemplate", "PlayerTemplate", GameObject)
RegistChildComponent(LuaTreasureSeaMiniMapWnd, "CountdownLabel", "CountdownLabel", UILabel)
RegistChildComponent(LuaTreasureSeaMiniMapWnd, "MapPool", "MapPool", CUIGameObjectPool)
RegistChildComponent(LuaTreasureSeaMiniMapWnd, "Root", "Root", Transform)
RegistChildComponent(LuaTreasureSeaMiniMapWnd, "MapTexture", "MapTexture", UITexture)
RegistChildComponent(LuaTreasureSeaMiniMapWnd, "PlayerGrid", "PlayerGrid", UIGrid)
RegistChildComponent(LuaTreasureSeaMiniMapWnd, "ShipRoot", "ShipRoot", Transform)
RegistChildComponent(LuaTreasureSeaMiniMapWnd, "MonsterRoot", "MonsterRoot", Transform)
RegistChildComponent(LuaTreasureSeaMiniMapWnd, "MainPlayer", "MainPlayer", GameObject)

--@endregion RegistChildComponent end


RegistClassMember(LuaTreasureSeaMiniMapWnd, "m_HelpCamera")
-- RegistClassMember(LuaTreasureSeaMiniMapWnd, "m_MainPlayerMark")
RegistClassMember(LuaTreasureSeaMiniMapWnd, "m_RequestTick")
RegistClassMember(LuaTreasureSeaMiniMapWnd, "m_BossGenTime")
RegistClassMember(LuaTreasureSeaMiniMapWnd, "m_NpcTemplateIds")
RegistClassMember(LuaTreasureSeaMiniMapWnd, "m_MonsterTemplateIds")
RegistClassMember(LuaTreasureSeaMiniMapWnd, "m_ResourceTemplateIds")


function LuaTreasureSeaMiniMapWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self.PlayerTemplate:SetActive(false)
    self.m_BossGenTime = 0


    local setting = NavalWar_Setting.GetData()

    self.m_NpcTemplateIds = {}
    for i = 1, setting.SupplyPoint.Length do
        self.m_NpcTemplateIds[setting.SupplyPoint[i-1]] = 9--补给点
    end

    self.m_ResourceTemplateIds = {}
    for i = 1, setting.SpecialEvents.Length do
        self.m_ResourceTemplateIds[setting.SpecialEvents[i-1]] = 3--特殊事件
    end
    for i = 1, setting.TreasureChest.Length do
        self.m_ResourceTemplateIds[setting.TreasureChest[i-1]] = 4--随机宝箱
    end

    --何罗巨妖2，幽冥船5，高级怪6，中级怪7，低级怪8
    self.m_MonsterTemplateIds = {}
    for i = 1, setting.SeaMonster.Length do
        self.m_MonsterTemplateIds[setting.SeaMonster[i-1]] = 2--何罗巨妖
    end
    for i = 1, setting.GhostShip.Length do
        self.m_MonsterTemplateIds[setting.GhostShip[i-1]] = 5--幽冥船
    end
    if setting.ShipMonsterLV3 then
        for i = 1, setting.ShipMonsterLV3.Length do
            self.m_MonsterTemplateIds[setting.ShipMonsterLV3[i-1]] = 6--高级怪
        end
    end
    if setting.ShipMonsterLV2 then
        for i = 1, setting.ShipMonsterLV2.Length do
            self.m_MonsterTemplateIds[setting.ShipMonsterLV2[i-1]] = 7
        end
    end
    if setting.ShipMonsterLV1 then
        for i = 1, setting.ShipMonsterLV1.Length do
            self.m_MonsterTemplateIds[setting.ShipMonsterLV1[i-1]] = 8
        end
    end
end

function LuaTreasureSeaMiniMapWnd:Init()
    self:OnRemainTimeUpdate()

    self.m_HelpCamera = CMiniMap.Instance.helpCamera
    self.MapTexture.mainTexture = CMiniMap.Instance.mapTexture.mainTexture
    if CClientMainPlayer.Inst then
        Gac2Gas.QuerySceneInfo(CClientMainPlayer.Inst.EngineId)
    end
    Gac2Gas.GetNavalShipNames()
    --定时请求动态位置(队友/怪)
    Gac2Gas.QueryMMapPosInfo()
    self.m_RequestTick = RegisterTick(function()
        Gac2Gas.QueryMMapPosInfo()
    end,5000)

    if CClientMainPlayer.Inst then
        self:UpdateMainPlayerPos()
    end
end

--@region UIEvent

--@endregion UIEvent

function LuaTreasureSeaMiniMapWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateNpcInfo", self, "OnUpdateNpcInfo")
    g_ScriptEvent:AddListener("QueryMonsterPosResultNew", self, "OnQueryMonsterPosResultNew")
    g_ScriptEvent:AddListener("QueryNavalPvpShipPosResult", self, "OnQueryNavalPvpShipPosResult")
    -- g_ScriptEvent:AddListener("ShowTimeCountDown", self, "OnShowTimeCountDown")
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
    g_ScriptEvent:AddListener("GetNavalShipNamesReturn", self, "OnGetNavalShipNamesReturn")
    
    
end
function LuaTreasureSeaMiniMapWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateNpcInfo", self, "OnUpdateNpcInfo")
    g_ScriptEvent:RemoveListener("QueryMonsterPosResultNew", self, "OnQueryMonsterPosResultNew")
    g_ScriptEvent:RemoveListener("QueryNavalPvpShipPosResult", self, "OnQueryNavalPvpShipPosResult")
    -- g_ScriptEvent:RemoveListener("ShowTimeCountDown", self, "OnShowTimeCountDown")
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("GetNavalShipNamesReturn", self, "OnGetNavalShipNamesReturn")
end
function LuaTreasureSeaMiniMapWnd:OnRemainTimeUpdate(args)
    local mainScene = CScene.MainScene
    if mainScene then
        if mainScene.ShowTimeCountDown then
            self.CountdownLabel.text = self:GetRemainTimeText(mainScene.ShowTime)
        else
            self.CountdownLabel.text = ""
        end
    else
        self.CountdownLabel.text = ""
    end
end
function LuaTreasureSeaMiniMapWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds<0 then
        totalSeconds = 0
    end
    if totalSeconds>=3600 then
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}:{2:00}[-]"),  
         math.floor(totalSeconds / 3600),
         math.floor((totalSeconds % 3600) / 60), 
         totalSeconds % 60)
    else
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}[-]"), math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaTreasureSeaMiniMapWnd:OnGetNavalShipNamesReturn(shipId2Name)
    if not LuaHaiZhanMgr.s_ShipId2Gold then return end
    local list = {}
    local myId = math.abs(CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.DriverId or 0)
    local myShipId = LuaHaiZhanMgr.s_EngineId2ShipId[myId] or 0
    for key, value in pairs(shipId2Name) do
        table.insert(list, key)
    end
    table.sort(list,function(a,b)
        local gold1 = LuaHaiZhanMgr.s_ShipId2Gold[a] or 0
        local gold2 = LuaHaiZhanMgr.s_ShipId2Gold[b] or 0
        if gold1>gold2 then
            return true
        elseif gold1<gold2 then
            return false
        else
            return a>b
        end
    end)
    local maxn = math.min(#list, 7)
    for index = 1, maxn do
        local shipId = list[index]
        local go = NGUITools.AddChild(self.PlayerGrid.gameObject,self.PlayerTemplate)
        go:SetActive(true)
        local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        local moneyLabel = go.transform:Find("MoneyLabel"):GetComponent(typeof(UILabel))
        local rankSprite = go.transform:Find("RankSprite"):GetComponent(typeof(UISprite))
        local rankLabel = go.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
        nameLabel.text = shipId2Name[shipId]

        if shipId == myShipId then
            nameLabel.color = NGUIText.ParseColor24("037C00", 0)
            moneyLabel.color = NGUIText.ParseColor24("037C00", 0)
        else
            nameLabel.color = NGUIText.ParseColor24("161637", 0)
            moneyLabel.color = NGUIText.ParseColor24("161637", 0)
        end

        moneyLabel.text = tostring(LuaHaiZhanMgr.s_ShipId2Gold[shipId] or 0)
        if index==1 then
            rankSprite.gameObject:SetActive(true)
            rankLabel.gameObject:SetActive(false)
            rankSprite.spriteName = "headinfownd_jiashang_01"
        elseif index==2 then
            rankSprite.gameObject:SetActive(true)
            rankLabel.gameObject:SetActive(false)
            rankSprite.spriteName = "headinfownd_jiashang_02"
        elseif index==3 then
            rankSprite.gameObject:SetActive(true)
            rankLabel.gameObject:SetActive(false)
            rankSprite.spriteName = "headinfownd_jiashang_03"
        else
            rankSprite.gameObject:SetActive(false)
            rankLabel.gameObject:SetActive(true)
            rankLabel.text = tostring(index)
        end
    end
    self.PlayerGrid:Reposition()
end
function LuaTreasureSeaMiniMapWnd:OnUpdateNpcInfo(args)
    -- print("OnUpdateNpcInfo")
    local childCount = self.Root.childCount
    for i = 1, childCount do
        local child = self.Root:GetChild(childCount-i)
        self.MapPool:Recycle(child.gameObject)
    end

    local npcList = args[0]
	for i = 0, npcList.Count - 1, 4 do--补给点
		local engineId,templateId,x,y = npcList[i + 0],npcList[i + 1],npcList[i + 2],npcList[i + 3]
        if self.m_NpcTemplateIds[templateId] then
            local type = self.m_NpcTemplateIds[templateId]

            --npc:特殊事件3、随机宝箱4、补给点9
            local go = self.MapPool:GetFromPool(type)
            go.transform.parent = self.Root
            go.transform.localScale = Vector3.one
            go:SetActive(true)
            go.transform.localPosition = self:CalculateMapPos(Vector3(x,0,y))
            go.name=tostring(templateId)
        end
	end
end


--定时请求动态位置(队友/怪)
function LuaTreasureSeaMiniMapWnd:OnQueryMonsterPosResultNew(monsterInfoData)
    -- print("OnQueryMonsterPosResultNew")

    local childCount = self.MonsterRoot.childCount
    for i = 1, childCount do
        local child = self.MonsterRoot:GetChild(childCount-i)
        self.MapPool:Recycle(child.gameObject)
    end

    for _, info in pairs(monsterInfoData) do
        local templateId = info.templateId 

        if self.m_MonsterTemplateIds[templateId] then
            local hp = info.hp
            local hpFull = info.hpFull
            local x = info.posX
            local y = info.posY
            local force = info.force

            local type = self.m_MonsterTemplateIds[templateId]

            --npc:何罗巨妖2，幽冥船5，高级怪6，中级怪7，低级怪8
            local go = self.MapPool:GetFromPool(type)
            go.transform.parent = self.MonsterRoot
            go.transform.localScale = Vector3.one
            go:SetActive(true)
            go.transform.localPosition = self:CalculateMapPos(Vector3(x,0,y))

            if type==2 then--何罗巨妖
                local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))

                if self.m_BossGenTime==0 then
                    label.gameObject:SetActive(false)
                else
                    label.gameObject:SetActive(true)
                    local hour = math.floor(self.m_BossGenTime/3600)
                    local min = math.floor(math.fmod(self.m_BossGenTime,3600)/60)
                    local sec = math.floor(math.fmod(self.m_BossGenTime,60))

                    if hour>0 then
                        label.text = string.format(LocalString.GetString("%02d:%02d:%02d后刷新"),hour,min,sec)
                    else
                        label.text = string.format(LocalString.GetString("%02d:%02d后刷新"),min,sec)
                    end
                end
            end
        end

    end
end

--其他玩家战船
function LuaTreasureSeaMiniMapWnd:OnQueryNavalPvpShipPosResult(bossGenTime,goldShipInfo, huntingShipInfo,resourceInfo)
    local childCount = self.ShipRoot.childCount
    for i = 1, childCount do
        local child = self.ShipRoot:GetChild(childCount-i)
        self.MapPool:Recycle(child.gameObject)
    end

    local myId = math.abs(CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.DriverId or 0)
    local created = {}

    if huntingShipInfo and #huntingShipInfo==5 then
        local shipId, engineId,name, x, y = unpack(huntingShipInfo)
        -- if not created[engineId] then
        created[engineId] = true
        local go = self.MapPool:GetFromPool(1)
        go.transform.parent = self.ShipRoot
        go.transform.localScale = Vector3.one
        local label = go.transform:Find("Label")
        if label then
            label.gameObject:SetActive(true)
        end
        go:SetActive(true)
        go.transform.localPosition = self:CalculateMapPos(Vector3(x,0,y))
        -- end
    end

    for index, value in ipairs(goldShipInfo) do
        local shipId, engineId,name, x, y = unpack(value)
        if not created[engineId] then
            if engineId==myId then
            else
                created[engineId] = true
                local child = self.MapPool:GetFromPool(1)
                child.transform.parent = self.ShipRoot
                child.transform.localScale = Vector3.one
                local label = child.transform:Find("Label")
                if label then
                    label.gameObject:SetActive(false)
                end
                child:SetActive(true)
                child.transform.localPosition = self:CalculateMapPos(Vector3(x,0,y))
            end
        end
    end

    --特殊事件/随机宝箱
    for index, value in ipairs(resourceInfo) do
        local templateId,x,y = unpack(value)
        if self.m_ResourceTemplateIds[templateId] then
            local type = self.m_ResourceTemplateIds[templateId]
            local go = self.MapPool:GetFromPool(type)
            go.transform.parent = self.ShipRoot
            go.transform.localScale = Vector3.one
            go:SetActive(true)
            go.transform.localPosition = self:CalculateMapPos(Vector3(x,0,y))
        end
    end
    

    self.m_BossGenTime = bossGenTime
end

function LuaTreasureSeaMiniMapWnd:CalculateMapPos(worldPos)
    local viewPos = self.m_HelpCamera:WorldToViewportPoint(worldPos)
    viewPos.x = viewPos.x-0.5
    viewPos.y = viewPos.y-0.5
    viewPos.z = 0

    local offset = self.MapTexture.transform.localPosition
    return Vector3(self.MapTexture.width * viewPos.x+offset.x, self.MapTexture.height * viewPos.y+offset.y, 0);
end

function LuaTreasureSeaMiniMapWnd:Update()
    self:UpdateMainPlayerPos()
end
function LuaTreasureSeaMiniMapWnd:UpdateMainPlayerPos()
    if self.MainPlayer then
        if CClientMainPlayer.Inst then
            local driverId = CClientMainPlayer.Inst.AppearanceProp.DriverId
            local driver = CClientObjectMgr.Inst:GetObject(math.abs(driverId))
            if driver and driver.m_PhysicsObject and driver.m_PhysicsObject.RO then
                self.MainPlayer.transform.localPosition = self:CalculateMapPos(driver.m_PhysicsObject.transform.position)
                local right = driver.m_PhysicsObject.transform.right
                self.MainPlayer.transform.right = Vector3(-right.z,right.x,0)--逆时针90°
            end
        end
    end
end

function LuaTreasureSeaMiniMapWnd:OnDestroy()
    if self.m_RequestTick then
        UnRegisterTick(self.m_RequestTick)
        self.m_RequestTick = nil
    end
end