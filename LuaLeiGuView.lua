local UILabel = import "UILabel"
local UIGrid = import "UIGrid"
local UITabBar = import "L10.UI.UITabBar"
local QnRadioBox = import "L10.UI.QnRadioBox"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local EnumCommonForce = import "L10.UI.EnumCommonForce"
local CFightingSpiritLiveWatchWndMgr = import "L10.UI.CFightingSpiritLiveWatchWndMgr"
local Screen = import "UnityEngine.Screen"
local UIRoot = import "UIRoot"
local CGameVideoMgr = import "L10.Game.CGameVideoMgr"
local Animator = import "UnityEngine.Animator"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaLeiGuView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLeiGuView, "ShowMainViewButton", "ShowMainViewButton", GameObject)
RegistChildComponent(LuaLeiGuView, "MainView", "MainView", GameObject)
RegistChildComponent(LuaLeiGuView, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaLeiGuView, "LeiGuButton", "LeiGuButton", GameObject)
RegistChildComponent(LuaLeiGuView, "QnRadioBox", "QnRadioBox", QnRadioBox)
RegistChildComponent(LuaLeiGuView, "TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaLeiGuView, "NoneRankLabel", "NoneRankLabel", UILabel)
RegistChildComponent(LuaLeiGuView, "LeftTemplate", "LeftTemplate", GameObject)
RegistChildComponent(LuaLeiGuView, "LeftGrid", "LeftGrid", UIGrid)
RegistChildComponent(LuaLeiGuView, "DanMuTemplate", "DanMuTemplate", GameObject)
RegistChildComponent(LuaLeiGuView, "PosList", "PosList", GameObject)
RegistChildComponent(LuaLeiGuView, "DaMuRoot", "DaMuRoot", GameObject)
RegistChildComponent(LuaLeiGuView, "DanMuView", "DanMuView", GameObject)
RegistChildComponent(LuaLeiGuView, "NoneLeiGuButton", "NoneLeiGuButton", GameObject)
RegistChildComponent(LuaLeiGuView, "NoneLeiGuLabel", "NoneLeiGuLabel", UILabel)
--@endregion RegistChildComponent end
RegistClassMember(LuaLeiGuView,"m_IsShowMainView")
RegistClassMember(LuaLeiGuView,"m_QnRadioBoxIndex")
RegistClassMember(LuaLeiGuView,"m_TabBarIndex")
-- RegistClassMember(LuaLeiGuView,"m_DanMuList")
RegistClassMember(LuaLeiGuView,"m_LastDanMuList")
RegistClassMember(LuaLeiGuView,"m_DanMuLeiGuAnimator")
RegistClassMember(LuaLeiGuView,"m_CurDanMuNum")
RegistClassMember(LuaLeiGuView,"m_ZhanduiId")

function LuaLeiGuView:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.ShowMainViewButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShowMainViewButtonClick()
	end)

	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

	UIEventListener.Get(self.LeiGuButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeiGuButtonClick()
	end)

	UIEventListener.Get(self.NoneLeiGuButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnNoneLeiGuButtonClick()
	end)
    --@endregion EventBind end
end

function LuaLeiGuView:Start()
	self.LeftTemplate.gameObject:SetActive(false)
	self.DanMuTemplate.gameObject:SetActive(false)
	self.NoneRankLabel.text = g_MessageMgr:FormatMessage("LeiGuView_NoneRank")
	local canShowLeigu = CLuaStarBiwuMgr.m_CurrentBattleStatus and
			((CLuaStarBiwuMgr.m_CurrentBattleStatus.m_Stage == EnumStarBiwuFightStage.eJiFenSai and CLuaStarBiwuMgr.m_CurrentBattleStatus.m_Group == 1) or CLuaStarBiwuMgr.m_CurrentBattleStatus.m_Stage == EnumStarBiwuFightStage.eZongJueSai)
	self.ShowMainViewButton.gameObject:SetActive(CLuaStarBiwuMgr:IsInStarBiwuWatch() and CGameVideoMgr.Inst:IsLiveMode() and canShowLeigu)
	self.m_IsShowMainView = false
	self.MainView.gameObject:SetActive(self.m_IsShowMainView)
	self:InitShowMainViewButton(not self.m_IsShowMainView)
	self:InitQnRadioBox()
	self:InitTabBar()
	self.m_LastDanMuList = {}
	self.m_CurDanMuNum = 0
	self.m_DanMuLeiGuAnimator = self.DanMuView:GetComponent(typeof( Animator))
	self.m_DanMuLeiGuAnimator:SetBool("start", false)
	if CServerTimeMgr.Inst.timeStamp - CLuaStarBiwuMgr.RequestLeiGuTimesTime > 5 then
		Gac2Gas.StarBiwuQueryLeiGuTimes()
	else
		self:OnSyncStarBiwuLeiGuTimes()
	end
	CLuaStarBiwuMgr.RequestLeiGuTimesTime = CServerTimeMgr.Inst.timeStamp
end

function LuaLeiGuView:InitShowMainViewButton(isNormal)
	self.ShowMainViewButton.transform:Find("Normal").gameObject:SetActive(isNormal)
	self.ShowMainViewButton.transform:Find("Highlight").gameObject:SetActive(not isNormal)
end

function LuaLeiGuView:InitQnRadioBox()
	StarBiWuShow_LeiGu.Foreach(function (k,v)
		local btn = self.QnRadioBox.m_RadioButtons[k - 1]
		local valLabel = btn.transform:Find("ValLabel"):GetComponent(typeof(UILabel))
		local timesLabel = btn.transform:Find("TimesLabel"):GetComponent(typeof(UILabel))
		btn.Text = v.Name
		valLabel.text = v.RenQi
		timesLabel.text = 0
	end)
	self.QnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
		self.m_QnRadioBoxIndex = index
		for i = 0, self.QnRadioBox.m_RadioButtons.Length - 1 do
			local button = self.QnRadioBox.m_RadioButtons[i]
			button.transform:Find("Select").gameObject:SetActive(i == index)
		end
		local times = CLuaStarBiwuMgr.m_LeiguId2Times[index + 1]
		local nameLabel = btn.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
		self.LeiGuButton.gameObject:SetActive(times and times > 0) 
		self.NoneLeiGuButton.gameObject:SetActive(times == nil or times == 0) 
		self.NoneLeiGuLabel.gameObject:SetActive(times == nil or times == 0) 
		self.NoneLeiGuLabel.text = SafeStringFormat3(LocalString.GetString("当前没有可擂鼓的%s"),nameLabel.text)
		--self:ShowRadioBoxSelectFx()
	end)
end

-- function LuaLeiGuView:ShowRadioBoxSelectFx()
-- 	if not self.m_QnRadioBoxIndex then
-- 		return
-- 	end
-- 	local btn = self.QnRadioBox.m_RadioButtons[self.m_QnRadioBoxIndex]
-- 	local ani = btn:GetComponent(typeof(Animation))
-- 	local fx = btn.transform:Find("CUIFx (1)")
-- 	if ani then
-- 		ani:Stop()
-- 		ani:Play("fightingspiritlivewatchwnd_xiaochuizi")
-- 		fx.gameObject:SetActive(false)
-- 		fx.gameObject:SetActive(true)
-- 	end
-- end

function LuaLeiGuView:InitTabBar()
	local info1=CFightingSpiritLiveWatchWndMgr.LeftForce2ServerName
    local info2=CFightingSpiritLiveWatchWndMgr.RightForce2ServerName
    local leftInfo=info1
    local rightInfo=info2
	if info1.Force~= EnumCommonForce.eAttack then
		leftInfo=info2
		rightInfo=info1
	end
	self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index) 
        self:OnTabChange(go, index)
    end)
	self.TabBar:ChangeTab(0, false)
end

function LuaLeiGuView:OnEnable()
	CLuaFightingSpiritMgr.m_ShowLeiGuView = false
	g_ScriptEvent:AddListener("OnSyncStarBiwuZhanduiDetailRank",self,"OnSyncStarBiwuZhanduiDetailRank")
	g_ScriptEvent:AddListener("OnStarBiwuFightZhanduiId",self,"OnStarBiwuFightZhanduiId")
	g_ScriptEvent:AddListener("OnStarBiwuFightSendGift",self,"OnStarBiwuFightSendGift")
	g_ScriptEvent:AddListener("OnSyncStarBiwuLeiGuTimes",self,"OnSyncStarBiwuLeiGuTimes")
	self:Start()
end

function LuaLeiGuView:OnDisable()
	LuaTweenUtils.DOKill(self.transform, false)
	CLuaFightingSpiritMgr.m_ShowLeiGuView = false
	g_ScriptEvent:RemoveListener("OnSyncStarBiwuZhanduiDetailRank",self,"OnSyncStarBiwuZhanduiDetailRank")
	g_ScriptEvent:RemoveListener("OnStarBiwuFightZhanduiId",self,"OnStarBiwuFightZhanduiId")
	g_ScriptEvent:RemoveListener("OnStarBiwuFightSendGift",self,"OnStarBiwuFightSendGift")
	g_ScriptEvent:RemoveListener("OnSyncStarBiwuLeiGuTimes",self,"OnSyncStarBiwuLeiGuTimes")
end

function LuaLeiGuView:OnSyncStarBiwuLeiGuTimes()
	for i = 0, self.QnRadioBox.m_RadioButtons.Length - 1 do
		local giftId = i + 1
		local times = CLuaStarBiwuMgr.m_LeiguId2Times[giftId]
		local btn = self.QnRadioBox.m_RadioButtons[i]
		local timesLabel = btn.transform:Find("TimesLabel"):GetComponent(typeof(UILabel))
		timesLabel.text = times and times or 0
	end
	if self.m_QnRadioBoxIndex == nil then
		self.m_QnRadioBoxIndex = 2
		for i = 3, 1 do
			local times = CLuaStarBiwuMgr.m_LeiguId2Times[i]
			if times and times > 0 then
				self.m_QnRadioBoxIndex = i - 1
			end
		end
		self.QnRadioBox:ChangeTo(self.m_QnRadioBoxIndex, true) --默认选中价值高且有次数的
	else
		local times = CLuaStarBiwuMgr.m_LeiguId2Times[self.m_QnRadioBoxIndex + 1]
		self.LeiGuButton.gameObject:SetActive(times and times > 0) 
		self.NoneLeiGuButton.gameObject:SetActive(times == nil or times == 0) 
		self.NoneLeiGuLabel.gameObject:SetActive(times == nil or times == 0) 
	end
end

function LuaLeiGuView:OnStarBiwuFightSendGift(playerId, playerName, force, giftType)
	local myId = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id or 0
	if PlayerPrefs.GetInt("LeiGuView_HideDanMu") > 0 and myId ~= playerId then
		return
	end
	if giftType == 3 then
		if myId == playerId or PlayerPrefs.GetInt("LeiGuView_HideLeiGuVFx") == nil or PlayerPrefs.GetInt("LeiGuView_HideLeiGuVFx") == 0 then
			local obj1Arr = {
				self.DanMuView.transform:Find("CUIFxlan"),
				self.DanMuView.transform:Find("Gu/jingu/qiang/jingu_blue"),
				self.DanMuView.transform:Find("Gu/jingu/qiang (1)/jingu_blue"),
			}
			local obj2Arr = {
				self.DanMuView.transform:Find("CUIFxhong"),
				self.DanMuView.transform:Find("Gu/jingu/qiang/jingu_red"),
				self.DanMuView.transform:Find("Gu/jingu/qiang (1)/jingu_red"),
			}
			for _, obj in pairs(obj1Arr) do
				obj.gameObject:SetActive(force == 1)
			end
			for _, obj in pairs(obj2Arr) do
				obj.gameObject:SetActive(force == 0)
			end
			self.m_DanMuLeiGuAnimator:SetBool("start", false)
			self.m_DanMuLeiGuAnimator:Play("fightingspiritlivewatchwnd_morenguanbi")
			self.m_DanMuLeiGuAnimator:SetBool("start", true)
		end
	end
	if (self.m_CurDanMuNum > StarBiWuShow_Setting.GetData().LeiGuDanMuMax) and myId ~= playerId then
		return
	end
	local obj = NGUITools.AddChild(self.DaMuRoot.gameObject, self.DanMuTemplate.gameObject)
	obj.gameObject:SetActive(true)
	local data = StarBiWuShow_LeiGu.GetData(giftType)
	local posNum = self.PosList.transform.childCount
	local random = math.random(posNum)
	local child = self.PosList.transform:GetChild(random - 1)
	local label1 = obj.transform:Find("Label_High"):GetComponent(typeof(UILabel))
	local label2 = obj.transform:Find("Label_Low_Red"):GetComponent(typeof(UILabel))
	local label3 = obj.transform:Find("Label_Low_Blue"):GetComponent(typeof(UILabel))
	local sprite1 = obj.transform:Find("Sprite1"):GetComponent(typeof(UIWidget))
	local sprite2 = obj.transform:Find("Sprite2")
	local sprite3 = obj.transform:Find("Sprite3")
	local labelText = SafeStringFormat3(LocalString.GetString("[%s]为%s擂动了%s"),playerName,force == EnumCommonForce_lua.eAttack and LocalString.GetString("蓝方") or LocalString.GetString("红方"), data.Name)
	label1.text = labelText
	label2.text = labelText
	label3.text = labelText
	label1.gameObject:SetActive(giftType == 3)
	label2.gameObject:SetActive(giftType ~= 3 and force ~= EnumCommonForce_lua.eAttack)
	label3.gameObject:SetActive(giftType ~= 3 and force == EnumCommonForce_lua.eAttack)
	sprite1.gameObject:SetActive(giftType == 1)
	sprite2.gameObject:SetActive(giftType == 2)
	sprite3.gameObject:SetActive(giftType == 3)
	sprite3.transform:Find("Red").gameObject:SetActive(force ~= EnumCommonForce_lua.eAttack)
	sprite3.transform:Find("Blue").gameObject:SetActive(force == EnumCommonForce_lua.eAttack)
	local minPosX = 0
	local width = sprite1.width
	if self.m_LastDanMuList[random] and self.m_LastDanMuList[random].transform then
		if self.m_LastDanMuList[random].transform.localPosition.y == child.transform.localPosition.y then
			local lastDanMuLabel = self.m_LastDanMuList[random].transform:Find("Label_High"):GetComponent(typeof(UILabel))
			local x = self.m_LastDanMuList[random].transform.localPosition.x + width + lastDanMuLabel.width
			minPosX = math.max(x, minPosX)
		end
	end
	obj.transform.localPosition = Vector3(minPosX, child.transform.localPosition.y, 0)
	local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenWidth = Screen.width * scale
	local time = (minPosX + virtualScreenWidth) / 320
	local tweener = LuaTweenUtils.TweenFloat(minPosX, -virtualScreenWidth, time, function(val)
        obj.transform.localPosition = Vector3(val, child.transform.localPosition.y, 0)
    end,function ()
		for i = 1,posNum do
			if self.m_LastDanMuList[random] == obj then
				self.m_LastDanMuList[random] = nil
			end
		end
		GameObject.Destroy(obj.gameObject)
		self.m_CurDanMuNum = self.m_CurDanMuNum - 1
	end)
	LuaTweenUtils.SetTarget(tweener,self.transform)
	self.m_LastDanMuList[random] = obj
	self.m_CurDanMuNum = self.m_CurDanMuNum + 1
end

function LuaLeiGuView:OnStarBiwuFightZhanduiId()
	self:OnTabChange(nil, self.m_TabBarIndex)
end

function LuaLeiGuView:OnSyncStarBiwuZhanduiDetailRank()
	local hasRank = CLuaStarBiwuMgr.m_ZhanduiDetailRankInfo and CLuaStarBiwuMgr.m_ZhanduiDetailRankInfo.rankList and (#CLuaStarBiwuMgr.m_ZhanduiDetailRankInfo.rankList > 0)
	self.NoneRankLabel.gameObject:SetActive(not hasRank)
	Extensions.RemoveAllChildren(self.LeftGrid.transform)
	if not hasRank or not CClientMainPlayer.Inst then
		return
	end
	--local myforce = self.m_TabBarIndex == 0 and EnumCommonForce_lua.eAttack or EnumCommonForce_lua.eDefend
	local list = {{playerId = CClientMainPlayer.Inst.Id, name = CClientMainPlayer.Inst.Name, rankValue = 0, serverName = CClientMainPlayer.Inst:GetMyServerName(), rank = 0}}
	for i, v in pairs(CLuaStarBiwuMgr.m_ZhanduiDetailRankInfo.rankList) do
		local t = {playerId = v.playerId, name = v.playerName, rankValue = v.rankValue, serverName = v.serverName , rank = v.rankPos}
		if v.playerId == CClientMainPlayer.Inst.Id then
			list[1].rank = v.rankPos
			list[1].rankValue = v.rankValue
		elseif v.rankPos == 1 then
			list[2] = t
		elseif v.rankPos == 2 then
			list[3] = t
		end
	end
	self:InitLeftRankView(list)
end

function LuaLeiGuView:InitLeftRankView(list)
	for i, t in pairs(list) do
		if t then
			local obj = NGUITools.AddChild(self.LeftGrid.gameObject, self.LeftTemplate.gameObject)
			obj.gameObject:SetActive(true)
			local rankImage = obj.transform:Find("RankImage"):GetComponent(typeof(CUITexture))
			local rankLabel = obj.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
			local serverNameLabel = obj.transform:Find("ServerNameLabel"):GetComponent(typeof(UILabel))
			local valLabel = obj.transform:Find("ValLabel"):GetComponent(typeof(UILabel))
			local nameLabel = obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
			local my = obj.transform:Find("My")
			local mynameLabel = obj.transform:Find("My/NameLabel"):GetComponent(typeof(UILabel))

			local rank = t.rank

			rankLabel.text = ""
			rankImage:LoadMaterial(nil)
			if rank==1 then
				rankImage:LoadMaterial("UI/Texture/Transparent/Material/grabchristmasgiftrankwnd_huangguang_01.mat")
			elseif rank==2 then
				rankImage:LoadMaterial("UI/Texture/Transparent/Material/grabchristmasgiftrankwnd_huangguang_02.mat")
			elseif rank==3 then
				rankImage:LoadMaterial("UI/Texture/Transparent/Material/grabchristmasgiftrankwnd_huangguang_03.mat")
			else
				if rank > 0 then
					rankLabel.text = rank
				else
					rankLabel.text = "-"
				end
			end
			serverNameLabel.text = t.serverName
			valLabel.text = t.rankValue
			nameLabel.text = t.name
			nameLabel.gameObject:SetActive(i ~= 1)
			my.gameObject:SetActive(i == 1)
			mynameLabel.text = t.name
		end
	end
	self.LeftGrid:Reposition()
end

--@region UIEvent

function LuaLeiGuView:OnTipButtonClick()
	g_MessageMgr:ShowMessage("LeiGuView_ReadMe")
end

function LuaLeiGuView:OnShowMainViewButtonClick()
	self.m_IsShowMainView = not self.m_IsShowMainView
	self:InitShowMainViewButton(not self.m_IsShowMainView)
	self.MainView.gameObject:SetActive(self.m_IsShowMainView)
	self:ShowOrHideThumbnailChatWnd(not self.m_IsShowMainView)
	CLuaFightingSpiritMgr.m_ShowLeiGuView = self.m_IsShowMainView 
	if self.m_IsShowMainView then
		--self.TabBar:ChangeTab(0, false)
		--self:ShowRadioBoxSelectFx()
	end
end

function LuaLeiGuView:OnLeiGuButtonClick()
	local force = self.m_TabBarIndex == 0 and EnumCommonForce_lua.eAttack or EnumCommonForce_lua.eDefend
	local zhanduiId = CLuaStarBiwuMgr.m_FightZhanduiIdInfo[force] and CLuaStarBiwuMgr.m_FightZhanduiIdInfo[force] or 0
	Gac2Gas.RequestStarBiwuSendGift(self.m_QnRadioBoxIndex + 1, force, zhanduiId)
end

function LuaLeiGuView:OnNoneLeiGuButtonClick()
	CLuaStarBiwuMgr.m_LeiGuTimesWndDefaultIndex = self.m_QnRadioBoxIndex
	CUIManager.ShowUI(CLuaUIResources.StarBiwuBuyLeiGuTimesWnd)
end

function LuaLeiGuView:ShowOrHideThumbnailChatWnd(val)
    g_ScriptEvent:BroadcastInLua("ShowOrHideThumbnailChatWnd", val)
    PlayerPrefs.SetInt("FightingSpiritLiveWatchWnd_HideThumbnailChatWnd", val and 0 or 1)
end

function LuaLeiGuView:OnTabChange(go, index)
	self.m_TabBarIndex = index
	for i = 1, 2 do
		local btn = self.TabBar:GetTabGoByIndex(i - 1)
		btn.transform:Find("Noarmal").gameObject:SetActive((i - 1) ~= index)
		btn.transform:Find("Highlight").gameObject:SetActive((i - 1) == index)
	end
	local force = self.m_TabBarIndex == 0 and EnumCommonForce_lua.eAttack or EnumCommonForce_lua.eDefend
	local zhanduiId = CLuaStarBiwuMgr.m_FightZhanduiIdInfo[force] and CLuaStarBiwuMgr.m_FightZhanduiIdInfo[force] or 0
	self.MainView.transform:Find("Bg/Red").gameObject:SetActive(force == 0)
	self.MainView.transform:Find("Bg/Blue").gameObject:SetActive(force == 1)
	if self.m_ZhanduiId ~= zhanduiId then
		Gac2Gas.QueryStarBiwuZhanduiDetailRank(zhanduiId)
	end
	self.m_ZhanduiId = zhanduiId 
end

--@endregion UIEvent

