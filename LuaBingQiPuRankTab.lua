local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local QnTableView = import "L10.UI.QnTableView"
local CCommonSelector = import "L10.UI.CCommonSelector"
local UILabel = import "UILabel"
local UITabBar = import "L10.UI.UITabBar"
local UISprite = import "UISprite"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local LuaGameObject = import "LuaGameObject"
local CItemMgr = import "L10.Game.CItemMgr"

LuaBingQiPuRankTab = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBingQiPuRankTab, "MasterView", "MasterView", QnAdvanceGridView)
RegistChildComponent(LuaBingQiPuRankTab, "InfoTable1", "InfoTable1", QnTableView)
RegistChildComponent(LuaBingQiPuRankTab, "Title4", "Title4", UILabel)
RegistChildComponent(LuaBingQiPuRankTab, "SubTabs", "SubTabs", UITabBar)
RegistChildComponent(LuaBingQiPuRankTab, "DropdownBtn", "DropdownBtn", CCommonSelector)
RegistChildComponent(LuaBingQiPuRankTab, "SelectBtn", "SelectBtn", CCommonSelector)
RegistChildComponent(LuaBingQiPuRankTab, "RuleBtn", "RuleBtn", UISprite)
RegistChildComponent(LuaBingQiPuRankTab, "View2", "View2", GameObject)
RegistChildComponent(LuaBingQiPuRankTab, "View1", "View1", GameObject)
RegistChildComponent(LuaBingQiPuRankTab, "InfoTable2", "InfoTable2", QnTableView)

--@endregion RegistChildComponent end

RegistClassMember(LuaBingQiPuRankTab, "m_TabIndex")
RegistClassMember(LuaBingQiPuRankTab, "m_WeaponType")
RegistClassMember(LuaBingQiPuRankTab, "m_EquipType")
RegistClassMember(LuaBingQiPuRankTab, "m_RankType")
RegistClassMember(LuaBingQiPuRankTab, "m_MasterViewDS")

RegistClassMember(LuaBingQiPuRankTab, "m_CfgDatas")
RegistClassMember(LuaBingQiPuRankTab, "m_Rank1")
RegistClassMember(LuaBingQiPuRankTab, "m_Rank2")
RegistClassMember(LuaBingQiPuRankTab, "m_dataSource1")
RegistClassMember(LuaBingQiPuRankTab, "m_dataSource2")
RegistClassMember(LuaBingQiPuRankTab, "m_ForceUpdate")
RegistClassMember(LuaBingQiPuRankTab, "m_ForceUpdate2")

function LuaBingQiPuRankTab:Awake()
    --@region EventBind: Dont Modify Manually!

	self.SubTabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnSubTabsTabChange(index)
	end)

	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)

    --@endregion EventBind end

	LuaBingQiPuMgr.m_RankInfo = nil
	LuaBingQiPuMgr.m_RankInfo2 = nil
end

function LuaBingQiPuRankTab:OnEnable()
	g_ScriptEvent:AddListener("SendBingQiPuRank", self, "UpdateRankInfo")
end

function LuaBingQiPuRankTab:OnDisable()
	g_ScriptEvent:RemoveListener("SendBingQiPuRank", self, "UpdateRankInfo")
end

function LuaBingQiPuRankTab:UpdateRankInfo()
	local datas = LuaBingQiPuMgr.GetRank(self.m_EquipType,self.m_WeaponType)
	self.m_Rank1 = self:GetFiltedData(datas,self.m_RankType)
	self.m_Rank2 = self:GetFiltedData(LuaBingQiPuMgr.m_RankInfo2,nil)
	
	self:Refresh()
end

function LuaBingQiPuRankTab:Init()
	
	self.m_WeaponType = 0
	self.m_EquipType = 0--默认武器
	self.m_RankType = 0
	self.m_TabIndex = -1

	self:InitCfgData()

	local function InitItem1(item,index)
        self:InitRankItem(item,index)
    end

    self.m_dataSource1=DefaultTableViewDataSource.CreateByCount(0,InitItem1)
    self.InfoTable1.m_DataSource=self.m_dataSource1
    self.InfoTable1.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow1(row)
    end)

	--self.m_dataSource1=DefaultTableViewDataSource.CreateByCount(0,InitItem1)
    self.InfoTable2.m_DataSource=self.m_dataSource1
    self.InfoTable2.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow1(row)
    end)

	self:InitMasterView()
	self:InitTypeDropdownBtn()

	self.SubTabs:ChangeTab(0,false)	--默认十大兵器
end

function LuaBingQiPuRankTab:RequireRankData(force)
	if self.m_TabIndex == 1 then--奇珍
		local datas = LuaBingQiPuMgr.m_RankInfo2
		if force or datas == nil then
			Gac2Gas.BQPQueryPreciousRank()
		else
			self:UpdateRankInfo()
		end
	else
		if self.m_RankType == 1 or self.m_RankType == 2 then--人气或者装备评分需要从服务器拉取数据
			local wtype = self.m_WeaponType
			if self.m_EquipType ~= 1 then 
				wtype = 0
			end
			Gac2Gas.BQPQueryTopTenRank(self.m_EquipType,wtype,self.m_RankType)
		else
			local datas = LuaBingQiPuMgr.GetRank(self.m_EquipType,self.m_WeaponType)
			if datas then
				self:UpdateRankInfo()
			end
			--其他数据不会自动去请求
		end
	end
end

function LuaBingQiPuRankTab:InitCfgData()
	self.m_CfgDatas = {}
	self.m_CfgDatas.RankNames = {}

	BingQiPu_RankType.Foreach(function (key,value)
		if value.Status > 0 then return end
		local rtype = value.Type
		if self.m_CfgDatas.RankNames[rtype] == nil then
			self.m_CfgDatas.RankNames[rtype] = value.Desc
		end
	end)

	self.m_CfgDatas.WeaponTypes = {
		{
			Name = LocalString.GetString("全部类型"),
			WType = 0
		}
	}

	
	self.m_CfgDatas.EquipTypeData = {
		{
			Name = LocalString.GetString("武器"),
			RankTypes = {},
			EType = 1
		}
	}
	BingQiPu_EquipType.Foreach(function (key,value)
		local data = nil
		if value.FstTab == 1 then 
			table.insert(self.m_CfgDatas.WeaponTypes,{Name = value.SecTabName,WType = value.SecTab})
			data = self.m_CfgDatas.EquipTypeData[1]
		else
			data = {}
			data.RankTypes = {}
			data.Name = value.SecTabName
			data.EType = value.FstTab
			table.insert(self.m_CfgDatas.EquipTypeData,data)
		end
		local tps = value.RankType
		for i=0,tps.Length-1 do
			data.RankTypes[i+1] = tps[i]
		end
	end)
end

function LuaBingQiPuRankTab:GetRankTypeName(rtype)
	return self.m_CfgDatas.RankNames[rtype] or ""
end

function LuaBingQiPuRankTab:InitSelectBtn(datas)
	local menuList = CreateFromClass(MakeGenericClass(List, String))

	for i=1,#datas do
		local rtype = datas[i]
		local rname = self.m_CfgDatas.RankNames[rtype]
		CommonDefs.ListAdd(menuList, typeof(String), rname)
	end

	self.SelectBtn:SetPopupAlignType(AlignType.Top)
	self.SelectBtn:SetMenuItemStyle(EnumPopupMenuItemStyle.Light)
	self.SelectBtn:Init(menuList, DelegateFactory.Action_int(function ( index )
        local menuName = menuList[index]
		self:OnSelectRankType(index,menuName)
    end))

	if LuaBingQiPuMgr.m_StageStatus == EnumBingQiPuStage.eVote then
		self:OnSelectRankType(1,menuList[1])
	else
		self:OnSelectRankType(0,menuList[0])
	end
end

function LuaBingQiPuRankTab:InitTypeDropdownBtn()
	local datas = self.m_CfgDatas.WeaponTypes
	local menuList = CreateFromClass(MakeGenericClass(List, String))
	for i=1,#datas do
		CommonDefs.ListAdd(menuList, typeof(String), datas[i].Name)
	end
	self.DropdownBtn:SetColumCount(2)
	self.DropdownBtn:SetPopupAlignType(AlignType.Top)
	self.DropdownBtn:SetMenuItemStyle(EnumPopupMenuItemStyle.Light)
	self.DropdownBtn:Init(menuList, DelegateFactory.Action_int(function ( index )
        local menuName = menuList[index]
        self.DropdownBtn:SetName(menuName)
		self:OnSelectWeaponType(index)
    end))
end

function LuaBingQiPuRankTab:OnSelectWeaponType(index)
	local wtype = self.m_CfgDatas.WeaponTypes[index+1].WType
	if self.m_WeaponType == wtype then return end
	self.m_WeaponType = wtype
	self:RequireRankData(false)
end

function LuaBingQiPuRankTab:OnSelectRankType(index,menuName)
	local data = self.m_CfgDatas.EquipTypeData[self.m_EquipType].RankTypes
	local rankindex = data[index+1]
	if self.m_RankType == rankindex then return end
	self.m_RankType = rankindex
	self.SelectBtn:SetName(SafeStringFormat3(LocalString.GetString("按%s排序"), menuName))
	self.Title4.text = menuName
	self:RequireRankData(false)
end

--@region Master区域

function LuaBingQiPuRankTab:InitMasterView()
	local datas = self.m_CfgDatas.EquipTypeData
	local function initItem(item,index)
		self:InitMasterItem(item,index)
	end
	self.m_MasterViewDS = DefaultTableViewDataSource.CreateByCount(#datas,initItem)
	self.MasterView.m_DataSource=self.m_MasterViewDS
	self.MasterView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
	    self:OnMasterItemSelect(row)
	end)

	self.MasterView:ReloadData(true,false)
	local item = self.MasterView:GetItemAtRow(0)
	item:SetSelected(true,false)
end

function LuaBingQiPuRankTab:OnMasterItemSelect(row)
	self.DropdownBtn.gameObject:SetActive(row == 0)
	if self.m_EquipType == row+1 then return end
	self.m_EquipType = row+1
	local ranktypes = self.m_CfgDatas.EquipTypeData[row+1].RankTypes
	self.m_RankType = 0
	self:InitSelectBtn(ranktypes)
end

function LuaBingQiPuRankTab:InitMasterItem(item,index)
	local trans = item.transform
	local cfg = self.m_CfgDatas.EquipTypeData[index+1]
	local label = LuaGameObject.GetChildNoGC(trans,"Label").label
	label.text = cfg.Name
	local taggo = LuaGameObject.GetChildNoGC(trans,"Committed").gameObject
	local commitedtyps = LuaBingQiPuMgr.m_EquipTypesCommited
	if commitedtyps then
		for i=1,#commitedtyps do
			if commitedtyps[i] == cfg.EType then
				taggo:SetActive(true)
				return
			end
		end
	end
	taggo:SetActive(false)
end

function LuaBingQiPuRankTab:FillMasterItemTag(item)
	local trans = item.transform
	local cfg = self.m_CfgDatas.EquipTypeData[item.Row+1]
	local taggo = LuaGameObject.GetChildNoGC(trans,"Committed").gameObject
	local commitedtyps = LuaBingQiPuMgr.m_EquipTypesCommited
	for i=1,#commitedtyps do
		if commitedtyps[i] == cfg.EType then
			taggo:SetActive(true)
			return
		end
	end
	taggo:SetActive(false)
end

--@endregion

function LuaBingQiPuRankTab:Refresh()
	if self.m_TabIndex == 0 then
		self:RefreshRank1()
	elseif self.m_TabIndex == 1 then
		self:RefreshRank2()
	end
end

--根据数据显示十大兵器榜
function LuaBingQiPuRankTab:RefreshRank1()
	self.m_dataSource1.count=#self.m_Rank1
	self.InfoTable1:ReloadData(true,false)

	local items = self.MasterView:GetVisibleItems()
	for i=0,items.Count-1 do
		self:FillMasterItemTag(items[i])
	end
end

--根据数据显示奇珍榜
function LuaBingQiPuRankTab:RefreshRank2()
	self.m_dataSource1.count=#self.m_Rank2
	self.InfoTable2:ReloadData(true,false)
end

--根据条件筛选和排序数据
function LuaBingQiPuRankTab:GetFiltedData(datas,rtype)
	if datas == nil then datas = {} return datas end 
	if #datas < 2 then return datas end
	if rtype and rtype ~= 1 and rtype ~= 2 then
		local compareScore = function(a, b)
			local ascore = a.ranks[rtype]
			local bscore = b.ranks[rtype]
			if ascore == bscore then 
				return a.rankPos < b.rankPos
			end
			return ascore > bscore
		end
		table.sort(datas, compareScore)
		for i=1,#datas do
			datas[i].rankPos = i
		end
	end

	local compareSelf = function(a, b)
		if a== nil or b == nil then
			return true
		end
		if a.ownerId == CClientMainPlayer.Inst.Id then
			if b.ownerId == CClientMainPlayer.Inst.Id then
				return a.rankPos < b.rankPos
			else
				return true
			end
		else
			if b.ownerId == CClientMainPlayer.Inst.Id then 
				return false
			end
		end
		return a.rankPos < b.rankPos
	end

	table.sort(datas, compareSelf)
	return datas
end

function LuaBingQiPuRankTab:OnSelectAtRow1(row)
	
	local data = nil--self.m_Rank2[row+1]
	if self.m_TabIndex == 0 then 
		data = self.m_Rank1[row+1]
	elseif self.m_TabIndex == 1 then
		data = self.m_Rank2[row+1]
	end
	if data then
		LuaBingQiPuMgr.QueryBQPDetailInfo(data.equipId)
	end
end

function LuaBingQiPuRankTab:InitRankItem(item, index)
	if index % 2 == 1 then
		item:SetBackgroundTexture("common_bg_mission_background_n")
	else
		item:SetBackgroundTexture("common_bg_mission_background_s")
	end

	local tf=item.transform
	local rankLabel=LuaGameObject.GetChildNoGC(tf,"RankLabel").label
    rankLabel.text=""
    local rankSprite=LuaGameObject.GetChildNoGC(tf,"RankImage").sprite
    rankSprite.spriteName=""

    local equipLabel=LuaGameObject.GetChildNoGC(tf,"EquipLabel").label
    equipLabel.text=""
    local ownerLabel=LuaGameObject.GetChildNoGC(tf,"OwnerLabel").label
    ownerLabel.text=""
    local scoreLabel=LuaGameObject.GetChildNoGC(tf,"ScoreLabel").label
    scoreLabel.text=""
    
	local fgo = LuaGameObject.GetChildNoGC(tf,"Feature")
    local info = nil
	if self.m_TabIndex == 0 then
		info = self.m_Rank1[index+1]
	else
		info = self.m_Rank2[index+1]
	end

	

    local isSelf = info.ownerId == CClientMainPlayer.Inst.Id
    local color = ""
	local color2 = ""
    if isSelf then
    	color = "00FF60"
		color2 = "00FF60"
    else
		color = "FFFFFF"
		color2 = "FFFFFF"
		if info.rankPos <=10 then
			color2 = "FFFE91"    		
		end
    end

    local rank = info.rankPos
    if rank==1 then
        rankSprite.spriteName="Rank_No.1"
    elseif rank==2 then
        rankSprite.spriteName="Rank_No.2png"
    elseif rank==3 then
        rankSprite.spriteName="Rank_No.3png"
    else
		if rank > 0 then
        	rankLabel.text = SafeStringFormat3("[%s]%s[-]", color2, tostring(rank)) 
		else
			rankLabel.text = SafeStringFormat3("[%s]%s[-]", color2, "-")
		end 
    end

	if String.IsNullOrEmpty(info.equipName) then
    	local template = EquipmentTemplate_Equip.GetData(info.templateId)
    	local equipName = template.Name
    	if info.isFake and info.isFake == 1 then
    		equipName = template.AliasName
    	end
    	equipLabel.text = SafeStringFormat3("[%s]%s[-]", color, equipName) 
	else
		equipLabel.text = SafeStringFormat3("[%s]%s[-]", color, info.equipName) 
	end

	ownerLabel.text = SafeStringFormat3("[%s]%s[-]", color, info.ownerName)

    if isSelf then
    	if rank > 100 then
    		rankLabel.text="100+"
    		local citem = CItemMgr.Inst:GetById(info.equipId)
    		if citem then
    			equipLabel.text=SafeStringFormat3("[%s]%s[-]", color, citem.Name) 
    		end
    	end
    end
	if self.m_TabIndex == 0 then
		if self.m_RankType == 1 then--装备评分
			scoreLabel.text = SafeStringFormat3("[%s]%s[-]", color, info.equipScore)
		elseif self.m_RankType == 2 then--总人气
			scoreLabel.text = SafeStringFormat3("[%s]%s[-]", color, info.renqiRank)
		else
			local score = info.ranks[self.m_RankType]
			scoreLabel.text = SafeStringFormat3("[%s]%s[-]", color, score)
		end
	else
		scoreLabel.text = SafeStringFormat3("[%s]%s[-]", color, info.renqiRank)
	end

	if fgo then
		local flabel = fgo.label
		if flabel then
			local tagcfg = BingQiPu_TreasureTag.GetData(info.tag)
			if tagcfg then
				flabel.text = SafeStringFormat3("[%s]%s[-]", color, tagcfg.TreasureVoteNum)
			else 
				flabel.text = ""
			end
		end
	end

    if isSelf then
    	item:SetBackgroundTexture("common_bg_mission_background_highlight")
    end
end

--@region UIEvent

function LuaBingQiPuRankTab:OnSubTabsTabChange(index)
	self.m_TabIndex = index

	self.View1:SetActive(self.m_TabIndex == 0)
	self.View2:SetActive(self.m_TabIndex == 1)
	if self.m_TabIndex == 1 then
		self:RequireRankData(false)
	end
end

function LuaBingQiPuRankTab:OnRuleBtnClick()
	g_MessageMgr:ShowMessage("BQP_COMMIT_TIPS")
end

--@endregion UIEvent

