local DelegateFactory  = import "DelegateFactory"
local CScheduleMgr=import "L10.Game.CScheduleMgr"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local CTaskMgr=import "L10.Game.CTaskMgr"
local COpenEntryMgr = import "L10.Game.COpenEntryMgr"
local CGuideMessager = import "L10.Game.Guide.CGuideMessager"
local CChatLinkMgr = import "CChatLinkMgr"

LuaJieRiPage = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJieRiPage, "Content", "Content", GameObject)
RegistChildComponent(LuaJieRiPage, "JieRiButtons", "JieRiButtons", GameObject)
RegistChildComponent(LuaJieRiPage, "Poster", "Poster", GameObject)
RegistChildComponent(LuaJieRiPage, "TemplateButton", "TemplateButton", GameObject)
RegistChildComponent(LuaJieRiPage, "Region", "Region", GameObject)
--@endregion RegistChildComponent end

RegistClassMember(LuaJieRiPage,"m_TableView")
RegistClassMember(LuaJieRiPage,"m_Data")
RegistClassMember(LuaJieRiPage,"m_SelectedId") -- 1, 2, 3, ...
RegistClassMember(LuaJieRiPage,"m_Groups")
RegistClassMember(LuaJieRiPage,"m_MainPlayerLevel")
RegistClassMember(LuaJieRiPage,"m_HasFeiSheng")
RegistClassMember(LuaJieRiPage,"m_XianshenLevel")
RegistClassMember(LuaJieRiPage,"m_RegionSize")
RegistClassMember(LuaJieRiPage,"m_ShowPoster")
RegistClassMember(LuaJieRiPage,"m_OthersPosition")

function LuaJieRiPage:Awake()
    self.TemplateButton:SetActive(false)
    self.m_TableView = self.Content.transform:Find("Grid"):GetComponent(typeof(QnTableView))
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self.m_ShowPoster and (#self.m_Data-1) or #self.m_Data
        end,
        function(item,index)
            index = self.m_ShowPoster and index+2 or index+1
            self:InitItem(item.transform, self.m_Data[index])
        end)
    
    self.m_yPosition = self.m_TableView.transform.localPosition.y
    self.m_OthersPosition = self.Poster.transform:Find("Others").localPosition
end

function LuaJieRiPage:Init()
    if CClientMainPlayer.Inst then
        self.m_MainPlayerLevel=CClientMainPlayer.Inst.Level
        self.m_HasFeiSheng=CClientMainPlayer.Inst.HasFeiSheng
        self.m_XianshenLevel=CClientMainPlayer.Inst.XianShenLevel
    end
    self.m_TabDict, self.m_TabList = self:GetJieRiDataList(CLuaScheduleMgr.GetJieRiList())
    self.m_RegionSize = (#CLuaScheduleMgr.m_PicList==0 or not CLuaScheduleMgr.GetBannarEnable()) and 945 or 801
    self:ShowPage()
end
--@region UIEvent
function LuaJieRiPage:OnEnable()
    g_ScriptEvent:AddListener("UpdateJieRiGroupAlertState", self, "UpdateAlertState")
end

function LuaJieRiPage:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateJieRiGroupAlertState", self, "UpdateAlertState")
end

--@endregion UIEvent

function LuaJieRiPage:ShowPage()
    self.tabHeight = 0
    self.m_SelectedId = 1
    Extensions.RemoveAllChildren(self.JieRiButtons.transform)
    -- 选择性隐藏Tab
    self.m_ShowTab = #self.m_TabList > 1
    if self.m_ShowTab then
        for i, name in ipairs(self.m_TabList) do
            local barItem = NGUITools.AddChild(self.JieRiButtons, self.TemplateButton)
            barItem.transform:Find("Alert").gameObject:SetActive(self:GetAlertState(i))
            barItem.transform:Find("normal"):GetComponent(typeof(UILabel)).text = name
            barItem.transform:Find("highlight"):GetComponent(typeof(UILabel)).text = name
            barItem.transform:Find("normal").gameObject:SetActive(i ~= self.m_SelectedId)
            barItem.transform:Find("highlight").gameObject:SetActive(i == self.m_SelectedId)
            UIEventListener.Get(barItem).onClick = DelegateFactory.VoidDelegate(function (go)
                local index =  go.transform:GetSiblingIndex()+1
                if index == self.m_SelectedId then return end
                local btn = self.JieRiButtons.transform:GetChild(self.m_SelectedId-1)
                btn.transform:Find("normal").gameObject:SetActive(true)
                btn.transform:Find("highlight").gameObject:SetActive(false)
                self.m_SelectedId = index
                self:ShowJieRiPageItem()
            end)
            barItem:SetActive(true)
        end
        self.JieRiButtons:GetComponent(typeof(UIGrid)):Reposition()
        self.tabHeight = self.transform:Find("TabList"):GetComponent(typeof(UIPanel)).height
    end
    self:ShowJieRiPageItem()
end

function LuaJieRiPage:ShowJieRiPageItem()
    -- Tab
    if self.m_ShowTab then
        local btn = self.JieRiButtons.transform:GetChild(self.m_SelectedId - 1)
        btn:Find("normal").gameObject:SetActive(false)
        btn:Find("highlight").gameObject:SetActive(true)
    end

    self.m_Data = self.m_TabDict[self.m_TabList[self.m_SelectedId]]
    local schedule = Task_Schedule.GetData(self.m_Data[1].activityId)
    local groupId = CLuaScheduleMgr.GetJieRiGroupId(self.m_Data[1].activityId)
    local group = groupId and Task_JieRiGroup.GetData(groupId)

    --优先级为1的活动才有banner
    if schedule.PriorityLevel==1 then
        self.m_ShowPoster = groupId and group and group.Picture and #group.Picture > 0
    else
        self.m_ShowPoster = false
    end

    self.Region:GetComponent(typeof(UIWidget)).height = self.m_RegionSize - self.tabHeight
    local posterHeight = self.m_ShowPoster and group.PictureHeight or 0
    self.Content:GetComponent(typeof(UIPanel)):ResetAndUpdateAnchors()
    local pos = self.m_TableView.transform.localPosition
    self.m_TableView.transform.localPosition = Vector3(pos.x, self.m_yPosition - posterHeight - 27, pos.z)
    self.m_TableView:ReloadData(true,false)

    local poster = self.Poster
    poster:SetActive(self.m_ShowPoster)
    -- Poster
    if self.m_ShowPoster then
        local tex = poster.transform:Find("Tex"):GetComponent(typeof(CUITexture))
        tex.texture.height = posterHeight
        tex:LoadMaterial(group.Picture)
        poster.transform:Find("Others/Description"):GetComponent(typeof(UILabel)).text = CChatLinkMgr.TranslateToNGUIText(schedule.ActivityDescription, false)
        local btn = poster.transform:Find("Others/Button").gameObject
        local offsetY = - posterHeight + 300
        poster.transform:Find("Others").localPosition = Vector3(self.m_OthersPosition.x, self.m_OthersPosition.y + offsetY, self.m_OthersPosition.z)
        local info = self.m_Data[1]
        local alertSprite = poster.transform:Find("Others/Button/Alert"):GetComponent(typeof(UISprite))
        local levelRequireLabel = poster.transform:Find("Others/LevelRequireLabel"):GetComponent(typeof(UILabel))
        --红点提示
        alertSprite.enabled = false
        if info.lastingTime>0 then--非日常
            if CScheduleMgr.Inst:GetAlertState(info.activityId) == CScheduleMgr.EnumAlertState.Show 
                and not CTaskMgr.Inst:IsInProgress(info.taskId) 
                and self.m_Data[1].FinishedTimes==0 then
                alertSprite.enabled = true
            end
        end
        levelRequireLabel.gameObject:SetActive(false)
        btn:SetActive(true)
        if info.lastingTime>0 then--限时
            local now=CServerTimeMgr.Inst:GetZone8Time()
            local nowTime=(now.Hour*60+now.Minute)*60+now.Second
            local startTime=(info.hour * 60 + info.minute) * 60 + CScheduleMgr.DelayTriggerSecond
            local endTime=(info.hour * 60 + info.minute) * 60 + info.lastingTime*60
            if nowTime> startTime and nowTime<=endTime then
                levelRequireLabel.gameObject:SetActive(false)
            elseif nowTime<startTime then--没开始
                btn:SetActive(false)
                levelRequireLabel.gameObject:SetActive(true)
                levelRequireLabel.text = SafeStringFormat3("%02d:%02d", info.hour, info.minute)
            elseif nowTime>endTime then--已结束
                btn:SetActive(false)
                levelRequireLabel.gameObject:SetActive(true)
                levelRequireLabel.text = LocalString.GetString("已结束")
            end
        end

        UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnClickCanJiaButton(poster.transform, self.m_Data[1], "Others/Button/Alert")
        end)
        UIEventListener.Get(poster.transform:Find("Tex").gameObject).onClick = DelegateFactory.VoidDelegate(function(p)
            CLuaScheduleMgr.selectedScheduleInfo=self.m_Data[1]
            CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
        end)
    end

    self.Content:GetComponent(typeof(UIScrollView)):ResetPosition()
end

function LuaJieRiPage:InitItem(transform, info)
    local schedule = Task_Schedule.GetData(info.activityId)
    if not schedule then return end

    UIEventListener.Get(transform.gameObject).onClick=DelegateFactory.VoidDelegate(function(p)
        CLuaScheduleMgr.selectedScheduleInfo=info
        CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
    end)

    local countLabel = transform:Find("Content/CountLabel"):GetComponent(typeof(UILabel))
    local huoliLabel = transform:Find("Content/HuoliLabel"):GetComponent(typeof(UILabel))
    local nameLabel = transform:Find("Content/NameLabel"):GetComponent(typeof(UILabel))
    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local btn = transform:Find("Content/Btn").gameObject
    local alertSprite = transform:Find("Content/Alert"):GetComponent(typeof(UISprite))
    local btnLabel = transform:Find("Content/Btn/Label"):GetComponent(typeof(UILabel))
    local levelRequireLabel = transform:Find("Content/LevelRequireLabel"):GetComponent(typeof(UILabel))
    local finishSprite = transform:Find("Content/FinishSprite"):GetComponent(typeof(UISprite))
    local description = transform:Find("Content/Description"):GetComponent(typeof(UILabel))
    finishSprite.gameObject:SetActive(false)
    local contentNode = transform:Find("Content").gameObject

    local finishedTimes=info.FinishedTimes--每日完成次数
    local totalTimes=CLuaScheduleMgr.GetDailyTimes(schedule)--每日次数上限
    local restrictTimes=CScheduleMgr.GetRestrictTimes(info.activityId, info.taskId)--最大次数

    nameLabel.text = schedule.TaskName
    description.text = CChatLinkMgr.TranslateToNGUIText(schedule.ActivityDescription, false)
    icon.material = nil
    --图标: 奖励或者装备
    if schedule.Reward ~= nil and schedule.Reward.Length > 0 then
        local id = schedule.Reward[0]
        local template = Item_Item.GetData(id)
        if template ~= nil then
            icon:LoadMaterial(template.Icon)
        else
            local equipTemplate = EquipmentTemplate_Equip.GetData(id)
            if equipTemplate ~= nil then
                icon:LoadMaterial(equipTemplate.Icon)
            end
        end
    end

    if schedule.ButtonType == 0 then
        btnLabel.text = LocalString.GetString("参加")
    elseif schedule.ButtonType == 1 then
        btnLabel.text = LocalString.GetString("领取")
    elseif schedule.ButtonType == 2 then
        btnLabel.text = LocalString.GetString("查看")
    end

    if totalTimes == 0 or schedule.Type == "NewBieSchoolTask" then--没有次数
        countLabel.enabled = false
        huoliLabel.text = ""
    else
        countLabel.enabled = true

        if schedule.Type=="JuQing" then
            countLabel.text = SafeStringFormat3(LocalString.GetString("次数 %d/%d"),finishedTimes, totalTimes+CLuaScheduleMgr.m_WuHuoQiQinCount)
        else
            countLabel.text =SafeStringFormat3(LocalString.GetString("次数 %d/%d"),finishedTimes, totalTimes)
        end
        --活力显示
        if not CLuaScheduleMgr.NeedShowHuoli(schedule) then
            huoliLabel.text =nil-- LocalString.GetString("活力 -/-")
        else
            local curHuoli =CLuaScheduleMgr.GetHuoli(schedule.HuoLi)
            local huoli = math.min(totalTimes, finishedTimes) * curHuoli
            huoliLabel.text = SafeStringFormat3(LocalString.GetString("活力 %d/%d"), huoli, CLuaScheduleMgr.GetDailyTimes(schedule) * curHuoli)
        end
    end


    self:SetTaskState(schedule, info, btn, levelRequireLabel, finishSprite, contentNode, alertSprite, totalTimes, finishedTimes, restrictTimes)

    UIEventListener.Get(btn).onClick=DelegateFactory.VoidDelegate(function(go)
        self:OnClickCanJiaButton(transform,info)
    end)
end

-- 节日名称先从JieRiGroup中寻找，找不到再从TaskName中寻找
function LuaJieRiPage:GetJieRiDataList(list)
    local tabDict = {}
    local TabList = {}

    for i, info in ipairs(list) do
        local groupId = CLuaScheduleMgr.GetJieRiGroupId(info.activityId)
        local name = nil
        if groupId then
            name = Task_JieRiGroup.GetData(groupId).Name
        else
            local schedule = Task_Schedule.GetData(info.activityId)
            name = string.match(schedule.TaskName, "%[(.-)%]")
        end
        if not name or #name == 0 then
            name = "Error"
        end

        if tabDict[name] == nil then
            tabDict[name] = {}
            table.insert(TabList, name)
        end
        table.insert(tabDict[name], info)
    end
    -- 任务排序 tabDict.data
    for key, list in pairs(tabDict) do
        table.sort(list, function (a, b)
            local scheduleData1 = Task_Schedule.GetData(a.activityId)
            local scheduleData2 = Task_Schedule.GetData(b.activityId)
            if scheduleData1 and scheduleData2 then
                return scheduleData1.PriorityLevel < scheduleData2.PriorityLevel
            end
            return true
        end)
    end
    -- 标签排序 TabList
    table.sort(TabList, function (a, b)
        local groupId1 = CLuaScheduleMgr.GetJieRiGroupId(tabDict[a][1].activityId)
        local groupId2 = CLuaScheduleMgr.GetJieRiGroupId(tabDict[b][1].activityId)

        local GetTimeStamp = function(str)
            local timestamp = 0
            for t in string.gmatch(str, "%d+") do
                timestamp = timestamp*60 + t
            end
            return timestamp
        end
        local ts1 = groupId1 and GetTimeStamp(Task_JieRiGroup.GetData(groupId1).StartTime) or 0
        local ts2 = groupId2 and GetTimeStamp(Task_JieRiGroup.GetData(groupId2).StartTime) or 0
        return ts1 > ts2
    end)
    return tabDict, TabList
end

function LuaJieRiPage:OnClickCanJiaButton(transform, info, alert)
    local activityId = info.activityId
    --跨服
    if not CLuaScheduleMgr.m_CrossServerActivityIds[activityId] and CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsCrossServerPlayer then
        g_MessageMgr:ShowMessage("CROSS_SERVER_NOT_USE_THE_FEATURE")
        return
    end
    local schedule = Task_Schedule.GetData(activityId)
    if not schedule then return end

    local isXianShi=schedule.TabType==2

    --限时活动要检查 
    if isXianShi and CLuaScheduleMgr.m_TaskCheck and not CTaskMgr.Inst:CheckTaskTime(info.taskId) then
        g_MessageMgr:ShowMessage("GAMEPLAY_NOT_OPEN")
        return
    end

    CLuaScheduleMgr.TrackSchedule(info)

    if transform then
        --红点逻辑
        alert = alert or "Content/Alert"
        local alertSprite = transform:Find(alert):GetComponent(typeof(UISprite))
        alertSprite.enabled = false
        if activityId == 42000401 then
            COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.InviteNpcAlert)
        end
    end

    CScheduleMgr.Inst:SetAlertState(activityId, CScheduleMgr.EnumAlertState.Clicked, false)
    EventManager.Broadcast(EnumEventType.UpdateScheduleAlert)

    --引导
    if transform then
        local messager = transform:GetComponent(typeof(CGuideMessager)) -- CommonDefs.GetComponent_GameObject_Type(self.gameObject, typeof(CGuideMessager))
        if messager ~= nil then
            messager:Click()
        end
    end

    CUIManager.CloseUI(CLuaUIResources.ScheduleWnd)
end

function LuaJieRiPage:IsLevelEnough(taskData)
    if self.m_HasFeiSheng then
        if taskData.GradeCheckFS1 and taskData.GradeCheckFS1.Length>0 then
            local gradeCheck=taskData.GradeCheckFS1[0]
            if self.m_XianshenLevel>=gradeCheck then
                return true
            end
        end
    else
        return self.m_MainPlayerLevel>=taskData.Level
    end
    return false
end

function LuaJieRiPage:UpdateAlertState()
    if self.m_TabList == nil then 
        self.m_TabDict, self.m_TabList = self:GetJieRiDataList(CLuaScheduleMgr.GetJieRiList())
    end
    local state = false
    for i, v in ipairs(self.m_TabList) do
        local s = self:GetAlertState(i)
        if self.m_ShowTab then
            local tab = self.JieRiButtons.transform:GetChild(i-1)
            tab.transform:Find("Alert").gameObject:SetActive(s)
        end
        state = state or s
    end
end

function LuaJieRiPage:GetAlertState(index)
    local infos = self.m_TabDict[self.m_TabList[index]]
    local state = false
    for i, info in ipairs(infos) do
        state = state or (CScheduleMgr.Inst:GetAlertState(info.activityId) == CScheduleMgr.EnumAlertState.Show and not CTaskMgr.Inst:IsInProgress(info.taskId) 
        and info.FinishedTimes==0)
    end
    return state
end

function LuaJieRiPage:SetTaskState(schedule, info, btn, levelRequireLabel, finishSprite, contentNode, alertSprite, totalTimes, finishedTimes, restrictTimes)
    --红点提示
    alertSprite.enabled = false
    if info.lastingTime>0 then--非日常
        if CScheduleMgr.Inst:GetAlertState(info.activityId) == CScheduleMgr.EnumAlertState.Show 
            and not CTaskMgr.Inst:IsInProgress(info.taskId) 
            and finishedTimes==0 then
            alertSprite.enabled = true
        end
    end
    if info.activityId == 42000401 and (CScheduleMgr.Inst:GetAlertState(info.activityId) == CScheduleMgr.EnumAlertState.Show 
            and not CTaskMgr.Inst:IsInProgress(info.taskId) 
            and finishedTimes==0 )then
            alertSprite.enabled = true
    end
    
    levelRequireLabel.gameObject:SetActive(false)
    btn:SetActive(true)

    CUICommonDef.SetActive(contentNode, true, true)
    if totalTimes>0 and finishedTimes >= restrictTimes then
        -- ret=false--已完成次数
        finishSprite.gameObject:SetActive(true)--完成
        CUICommonDef.SetActive(contentNode, false, true)
        btn:SetActive(false)
    else
        local taskTemplate=Task_Task.GetData(info.taskId)
        
        if self:IsLevelEnough(taskTemplate) then
            if CLuaScheduleMgr.IsInProgress(info.taskId) then
                levelRequireLabel.text = LocalString.GetString("已接")--进行中 不能参加
                levelRequireLabel.gameObject:SetActive(true)
                btn:SetActive(false)
            else
                if schedule.NoJoinButton>0 then
                    -- ret=false
                    levelRequireLabel.gameObject:SetActive(false)
                    btn:SetActive(false)
                else
                    if info.lastingTime>0 then--限时
                        local now=CServerTimeMgr.Inst:GetZone8Time()
                        local nowTime=(now.Hour*60+now.Minute)*60+now.Second
                        local startTime=(info.hour * 60 + info.minute) * 60 + CScheduleMgr.DelayTriggerSecond
                        local endTime=(info.hour * 60 + info.minute) * 60 + info.lastingTime*60
                        if nowTime> startTime and nowTime<=endTime then
                            --
                            levelRequireLabel.gameObject:SetActive(false)
                        elseif nowTime<startTime then--没开始
                            btn:SetActive(false)
                            levelRequireLabel.gameObject:SetActive(true)
                            levelRequireLabel.text = SafeStringFormat3("%02d:%02d", info.hour, info.minute)
                        elseif nowTime>endTime then--已结束
                            btn:SetActive(false)
                            levelRequireLabel.gameObject:SetActive(true)
                            levelRequireLabel.text = LocalString.GetString("已结束")
                            CUICommonDef.SetActive(contentNode, false, true)
                        end
                    end
                end
            end
        else
            --等级不够
            levelRequireLabel.text = SafeStringFormat3(LocalString.GetString("%d级开启"), taskTemplate.Level)
            levelRequireLabel.gameObject:SetActive(true)
            btn:SetActive(false)
        end
    end
end
