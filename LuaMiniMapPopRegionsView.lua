require("3rdParty/ScriptEvent")
require("common/common_include")

local Utility = import "L10.Engine.Utility"
local CMiniMapPopWnd = import "L10.UI.CMiniMapPopWnd"
local Color = import "UnityEngine.Color"

LuaMiniMapPopRegionsView = class()

RegistChildComponent(LuaMiniMapPopRegionsView, "RegionSprite", UISprite)
RegistChildComponent(LuaMiniMapPopRegionsView, "RegionRoot", GameObject)

RegistClassMember(LuaMiniMapPopRegionsView, "MiniMapPopWnd")
RegistClassMember(LuaMiniMapPopRegionsView, "BottomLeftX")
RegistClassMember(LuaMiniMapPopRegionsView, "BottomLeftY")
RegistClassMember(LuaMiniMapPopRegionsView, "TopRightX")
RegistClassMember(LuaMiniMapPopRegionsView, "TopRightY")

function LuaMiniMapPopRegionsView:Init()
    self.BottomLeftX = -831
    self.BottomLeftY = -478
    self.TopRightX = 831
    self.TopRightY = 478
    self.RegionSprite.gameObject:SetActive(false)
    self.MiniMapPopWnd = self.transform.parent.parent.parent.parent.parent:GetComponent(typeof(CMiniMapPopWnd))
end
--[[
(xw1, yw1) warning区(外围) 最小点，左下角
(xw2, yw2) warning区(外围) 最大点，右上角

(xs1, ys1) 安全区(内部) 最小点，左下角
(xs2, ys2) 安全区(内部) 最大点，右上角
]]--
function LuaMiniMapPopRegionsView:CreateRegion(xw1, yw1, xw2, yw2, xs1, ys1, xs2, ys2, color, needRemove)
    
    if needRemove then
        Extensions.RemoveAllChildren(self.RegionRoot.transform)
    end

    local row1 = math.floor(yw2 - ys2)
    local row2 = math.floor(ys2 - ys1)
    local row3 = math.floor(yw2 - yw1) - row1 - row2

    local rowList = {row1, row2, row3}

    local xrow1 = ys2 + row1 * 0.5
    local xrow2 = ys2 - row2 * 0.5
    local xrow3 = ys2 - row2 - row3 * 0.5

    local xrowList = {xrow1, xrow2, xrow3}
    
    local column1 = math.floor(xs1 - xw1)
    local column2 = math.floor(xs2 - xs1)
    local column3 = math.floor(xw2 - xw1) - column1 - column2

    local columnList = {column1, column2, column3}

    local ycolumn1 = xs1 - column1 * 0.5
    local ycolumn2 = xs1 + column2 * 0.5
    local ycolumn3 = xs1 + column2 + column3 * 0.5

    local yColumnList = {ycolumn1, ycolumn2, ycolumn3}

    for i = 1, 9 do
        if i ~= 5 then
            local x = math.floor( (i-1) / 3) + 1
            local y = (i-1) % 3 + 1
            local instance = NGUITools.AddChild(self.RegionRoot, self.RegionSprite.gameObject)
            local sprite = instance:GetComponent(typeof(UISprite))

            sprite.width = math.ceil(columnList[y])
            sprite.height = math.ceil(rowList[x])
            sprite.color = color
            sprite.alpha = 0.25

            Extensions.SetLocalPositionY(sprite.transform, xrowList[x])
            Extensions.SetLocalPositionX(sprite.transform, yColumnList[y])
            instance:SetActive(true)
        end
    end
end

function LuaMiniMapPopRegionsView:UpdateChiJiRegions()
    local worldPosBL = Utility.GridPos2WorldPos(LuaPUBGMgr.m_CenterPosX - LuaPUBGMgr.m_HalfSideLength, LuaPUBGMgr.m_CenterPosY - LuaPUBGMgr.m_HalfSideLength)
    local worldPosTR = Utility.GridPos2WorldPos(LuaPUBGMgr.m_CenterPosX + LuaPUBGMgr.m_HalfSideLength, LuaPUBGMgr.m_CenterPosY + LuaPUBGMgr.m_HalfSideLength)
    local mapPosBL = self.MiniMapPopWnd:CalculateMapPos(worldPosBL)
    local mapPosTR = self.MiniMapPopWnd:CalculateMapPos(worldPosTR)
    self:CreateRegion(self.BottomLeftX, self.BottomLeftY, self.TopRightX, self.TopRightY, mapPosBL.x, mapPosBL.y, mapPosTR.x, mapPosTR.y, Color.red, true)

    if LuaPUBGMgr.m_NextCenterPosX ~= LuaPUBGMgr.m_CenterPosX or LuaPUBGMgr.m_CenterPosY ~= LuaPUBGMgr.m_NextCenterPosY or LuaPUBGMgr.m_HalfSideLength ~= LuaPUBGMgr.m_NextHalfSideLength then
        local worldPosBLNext = Utility.GridPos2WorldPos(LuaPUBGMgr.m_NextCenterPosX - LuaPUBGMgr.m_NextHalfSideLength, LuaPUBGMgr.m_NextCenterPosY - LuaPUBGMgr.m_NextHalfSideLength)
        local worldPosTRNext = Utility.GridPos2WorldPos(LuaPUBGMgr.m_NextCenterPosX + LuaPUBGMgr.m_NextHalfSideLength, LuaPUBGMgr.m_NextCenterPosY + LuaPUBGMgr.m_NextHalfSideLength)
        local mapPosBLNext = self.MiniMapPopWnd:CalculateMapPos(worldPosBLNext)
        local mapPosTRNext = self.MiniMapPopWnd:CalculateMapPos(worldPosTRNext)
        self:CreateRegion(mapPosBL.x, mapPosBL.y, mapPosTR.x, mapPosTR.y, mapPosBLNext.x, mapPosBLNext.y, mapPosTRNext.x, mapPosTRNext.y, Color.yellow, false)
    end
end

function LuaMiniMapPopRegionsView:OnEnable()
    g_ScriptEvent:AddListener("UpdateChiJiMiniMap", self, "UpdateChiJiRegions")
end

function LuaMiniMapPopRegionsView:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateChiJiMiniMap", self, "UpdateChiJiRegions")
end

return LuaMiniMapPopRegionsView
