-- Auto Generated!!
local AlignType = import "CPlayerInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CWeekendGameplayMgr = import "L10.Game.CWeekendGameplayMgr"
local CWeekendPlayRankInfo = import "L10.Game.CWeekendPlayRankInfo"
local CWeekendPlayRankTemplate = import "L10.UI.CWeekendPlayRankTemplate"
local CWeekendPlayRankWnd = import "L10.UI.CWeekendPlayRankWnd"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UIEventListener = import "UIEventListener"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CWeekendPlayRankWnd.m_Init_CS2LuaHook = function (this) 
    this.playerRank.text = LocalString.GetString("未上榜")
    this.playerName.text = CClientMainPlayer.Inst.Name
    this.playerDamage.text = ""
    Extensions.RemoveAllChildren(this.table.transform)
    this.rankTemplate:SetActive(false)
    do
        local i = 0
        while i < CWeekendGameplayMgr.Inst.rankList.Count do
            local info = CWeekendGameplayMgr.Inst.rankList[i]
            if i == 0 then
                if info.rank == 0 then
                    this.playerRank.text = LocalString.GetString("未上榜")
                else
                    this.playerRank.text = tostring(info.rank)
                end
                this.playerName.text = info.playerName
                this.playerDamage.text = tostring(info.damageData)
            else
                local instance = NGUITools.AddChild(this.table.gameObject, this.rankTemplate)
                instance:SetActive(true)
                CommonDefs.GetComponent_GameObject_Type(instance, typeof(CWeekendPlayRankTemplate)):Init(tostring(info.rank), info.playerName, tostring(info.damageData))
                UIEventListener.Get(instance).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(instance).onClick, MakeDelegateFromCSFunction(this.OnListClick, VoidDelegate, this), true)
                CommonDefs.ListAdd(this.list, typeof(GameObject), instance)
                CommonDefs.ListAdd(this.rankList, typeof(CWeekendPlayRankInfo), info)
            end
            i = i + 1
        end
    end
    this.table:Reposition()
    this.scrollView:ResetPosition()
end
CWeekendPlayRankWnd.m_OnListClick_CS2LuaHook = function (this, go) 

    do
        local i = 0
        while i < this.list.Count do
            CommonDefs.GetComponent_GameObject_Type(this.list[i], typeof(QnSelectableButton)):SetSelected(go == this.list[i], false)
            if go == this.list[i] then
                CPlayerInfoMgr.ShowPlayerPopupMenu(this.rankList[i].playerId, EnumPlayerInfoContext.RankList, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
            end
            i = i + 1
        end
    end
end
