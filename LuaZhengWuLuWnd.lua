local UIGrid = import "UIGrid"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UITabBar = import "L10.UI.UITabBar"
local PathType = import "DG.Tweening.PathType"
local Vector4 = import "UnityEngine.Vector4"
local PathMode = import "DG.Tweening.PathMode"
local Ease = import "DG.Tweening.Ease"

LuaZhengWuLuWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhengWuLuWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaZhengWuLuWnd, "Tabs", "Tabs", GameObject)
RegistChildComponent(LuaZhengWuLuWnd, "Template", "Template", GameObject)

--@endregion RegistChildComponent end

function LuaZhengWuLuWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaZhengWuLuWnd:Init()
    self.transform:Find("Content/PageTitle"):GetComponent(typeof(UILabel)).text = LocalString.GetString("证物录")
    self.Template:SetActive(false)
    self.Tabs:GetComponent(typeof(UITabBar)).OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self.m_SelectedType = index + 1
        self:Show()
        for i = 1, 3 do
            self:UpdateTab(i)
        end
	end)
    self.m_Cell = {}
    for i = 0, self.Grid.transform.childCount-1 do
        table.insert(self.m_Cell, self.Grid.transform:GetChild(i).gameObject)
    end
    self.m_SelectedType = 1

    if LuaZhengWuLuMgr.highlight then
        for i, infos in ipairs(LuaZhengWuLuMgr.Infos) do
            for _, info in ipairs(infos) do
                if info.ID == LuaZhengWuLuMgr.highlight then
                    self.m_SelectedType = i
                    break
                end
            end
        end
    end

    self.Tabs:GetComponent(typeof(UITabBar)):ChangeTab(self.m_SelectedType-1)
end

function LuaZhengWuLuWnd:Show()
    self:InitGrid()
    for _, info in ipairs(LuaZhengWuLuMgr.Infos[self.m_SelectedType]) do
        local cell = self:GetCell()
        self:UpdateCell(cell, info)
    end
    self.Grid:Reposition()
end

function LuaZhengWuLuWnd:InitGrid()
    self.m_CellIndex = 1
    for i,v in ipairs(self.m_Cell) do
        v:SetActive(false)
    end
end

function LuaZhengWuLuWnd:GetCell()
    if self.m_CellIndex > #self.m_Cell then
        table.insert(self.m_Cell, NGUITools.AddChild(self.Grid.gameObject, self.Template))
    end
    local ret = self.m_Cell[self.m_CellIndex]
    ret:SetActive(true)
    self.m_CellIndex = self.m_CellIndex + 1
    return ret
end

function LuaZhengWuLuWnd:UpdateCell(cell, info)
    cell.transform:Find("ItemLabel").gameObject:SetActive(not info.Unlocked)
    cell.transform:Find("unlock").gameObject:SetActive(info.Unlocked)
    if info.Unlocked then
        cell.transform:Find("unlock/ItemLabel"):GetComponent(typeof(UILabel)).text = info.Name
    end
    cell.transform:Find("normal").gameObject:SetActive(not info.Unlocked)
    cell.transform:Find("AlertSprite").gameObject:SetActive(info.Unlocked and not info.Readed)
    local fx = cell.transform:Find("fx"):GetComponent(typeof(CUIFx))
    if LuaZhengWuLuMgr.highlight and info.ID == LuaZhengWuLuMgr.highlight then
        LuaTweenUtils.DOKill(fx.transform, false)
        fx.transform.localPosition=Vector3(0,0,0)
        local b = NGUIMath.CalculateRelativeWidgetBounds(cell.transform)
        local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
        fx.transform.localPosition = waypoints[0]
        fx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
        LuaTweenUtils.DOLuaLocalPath(fx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
    else
        fx:DestroyFx()
        LuaTweenUtils.DOKill(fx.transform, false)
    end
    UIEventListener.Get(cell).onClick = DelegateFactory.VoidDelegate(function (go)
        if info.Unlocked == true then
            LuaZhengWuLuMgr.OpenTipWnd(info.ID)
            if LuaZhengWuLuMgr.highlight and info.ID == LuaZhengWuLuMgr.highlight then --点击过后去掉提示特效
                LuaZhengWuLuMgr.highlight = nil
                fx:DestroyFx()
                LuaTweenUtils.DOKill(fx.transform, false)
            end
        end
    end)
end

function LuaZhengWuLuWnd:UpdateTab(type)
    local unreaded = false
    for i, info in ipairs(LuaZhengWuLuMgr.Infos[type]) do
        unreaded = unreaded or (info.Unlocked and not info.Readed)
    end
    local tf = self.Tabs.transform:GetChild(type - 1)
    tf:Find("AlertSprite").gameObject:SetActive(unreaded)

    local hightlight = type == self.m_SelectedType
    tf:Find("Label").gameObject:SetActive(not hightlight)
    tf:Find("LabelHighlight").gameObject:SetActive(hightlight)
    tf:Find("Sprite").gameObject:SetActive(not hightlight)
    tf:Find("SpriteHighlight").gameObject:SetActive(hightlight)
end

--@region UIEvent
function LuaZhengWuLuWnd:OnEnable( )
    g_ScriptEvent:AddListener("SyncZhengWuLuItemStatus", self, "OnSyncZhengWuLuItemStatus")
end
function LuaZhengWuLuWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("SyncZhengWuLuItemStatus", self, "OnSyncZhengWuLuItemStatus")
end
--@endregion UIEvent

function LuaZhengWuLuWnd:OnSyncZhengWuLuItemStatus(zhengWuId)
    for i, info in ipairs(LuaZhengWuLuMgr.Infos[self.m_SelectedType]) do
        -- if zhengWuId == info.ID then
            local cell = self.m_Cell[i]
            self:UpdateCell(cell, info)
        -- end
    end
    for i = 1, 3 do
        self:UpdateTab(i)
    end
end
