-- Auto Generated!!
local AlignType = import "L10.UI.CTooltip+AlignType"
local AlignType1 = import "L10.UI.CItemInfoMgr+AlignType"
local CChatLinkMgr = import "CChatLinkMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local CIMMgr = import "L10.Game.CIMMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CPersonalSpace_GetMomentItem = import "L10.Game.CPersonalSpace_GetMomentItem"
local CPersonalSpace_MomentCacheData = import "L10.Game.CPersonalSpace_MomentCacheData"
local CPersonalSpaceGMWnd = import "L10.UI.CPersonalSpaceGMWnd"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CPersonalSpaceWnd = import "L10.UI.CPersonalSpaceWnd"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local GameplayItem_Setting = import "L10.Game.GameplayItem_Setting"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Object = import "UnityEngine.Object"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local Quaternion = import "UnityEngine.Quaternion"
local String = import "System.String"
local UICamera = import "UICamera"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local UInt64 = import "System.UInt64"
local UIPanel = import "UIPanel"
local UIScrollView = import "UIScrollView"
local UISprite = import "UISprite"
local UITable = import "UITable"
local UITexture = import "UITexture"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CPersonalSpaceGMWnd.m_UpdateTopShowRes_CS2LuaHook = function (this, data)
    local pid = data.targetid
    if pid > 0 and pid == CPersonalSpaceMgr.Inst.GM_ID then
        this.renqiText.text = tostring(data.renqi)
        this.flowerText.text = tostring(data.flowerrenqi)
        CExpressionMgr.Inst.Renqi = (data.renqi)
    end
end
CPersonalSpaceGMWnd.m_Init_CS2LuaHook = function (this)
    this.statusTemplateBar:SetActive(false)
    UIEventListener.Get(this.closeBtn).onClick = MakeDelegateFromCSFunction(this.CloseWnd, VoidDelegate, this)
    UIEventListener.Get(this.xinyiBtn).onClick = MakeDelegateFromCSFunction(this.OnClickXinyi, VoidDelegate, this)
    this.selfPortraitNode.transform.parent.gameObject:SetActive(true)
    this.sendFlowerBtn.transform.parent.parent.gameObject:SetActive(true)
    this.sendFlowerPanel.transform:Find('ItemShow/icon'):GetComponent(typeof(UITexture)).depth = 114

    UIEventListener.Get(this.addRenqiBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        CPersonalSpaceMgr.Inst:RequestAddPopularity(CPersonalSpaceMgr.Inst.GM_ID, CPersonalSpaceMgr.Inst.GM_Name)
    end)

    UIEventListener.Get(this.sendFlowerBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        this:InitSendFlowerPanel(CPersonalSpaceMgr.Inst.GM_ID, CPersonalSpaceMgr.Inst.GM_Name, 0, 1)
    end)

    UIEventListener.Get(this.selfSpaceBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        if CClientMainPlayer.Inst ~= nil then
            CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(CClientMainPlayer.Inst.Id, 0)
            this:Close()
        end
    end)

    this:InitUserProfileInfo(CPersonalSpaceMgr.Inst.GM_ID)
    this:InitMomentInfo(CPersonalSpaceMgr.Inst.GM_ID)
    this:InitXinyi()
end
CPersonalSpaceGMWnd.m_InitUserProfileInfo_CS2LuaHook = function (this, playerID)
    this.renqiText.text = ""
    this.flowerText.text = ""

    CPersonalSpaceMgr.Inst:GetUserProfile(playerID, DelegateFactory.Action_CPersonalSpace_UserProfile_Ret(function (data)
        if not this.systemPortrait then
            return
        end

        if data.code == 0 then
            local _data = data.data

            if not System.String.IsNullOrEmpty(_data.rolename) then
                this.playerNameText.text = _data.rolename
            end

            if not System.String.IsNullOrEmpty(_data.signature) then
                this.signatureText.text = _data.signature
            end

            local cls = _data.clazz
            local gender = _data.gender
            this.systemPortrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender, _data.expression_base.expression), false)
            local textTexture = CommonDefs.GetComponent_Component_Type(this.systemPortrait.transform.parent:Find("text"), typeof(CUITexture))
            CExpressionMgr.Inst:SetIconTextInfo(textTexture, _data.expression_base, _data.expression_extra, true)
            local sticker = CommonDefs.GetComponent_Component_Type(this.systemPortrait.transform.parent:Find("sticker"), typeof(CUITexture))
            CExpressionMgr.Inst:SetIconTextInfo(sticker, _data.expression_base, _data.expression_extra, false)


            if System.String.IsNullOrEmpty(_data.photo) then
                this.selfPortraitNode:SetActive(false)
                this.systemPortraitNode:SetActive(true)
            else
                this.selfPortraitNode:SetActive(true)
                this.systemPortraitNode:SetActive(false)
                CPersonalSpaceMgr.Inst:DownLoadPortraitPic(_data.photo, this.selfPortraitNodeTexture, nil)
            end

            this.renqiText.text = tostring(_data.renqi)
            this.flowerText.text = tostring(_data.flowerrenqi)
        end
    end), nil)
end
CPersonalSpaceGMWnd.m_SetShowMoreBtn_CS2LuaHook = function (this, showMoreBtn, targetid)
    UIEventListener.Get(showMoreBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        showMoreBtn:SetActive(false)
        local lastId = 0
        if this.momentCacheList.Count > 0 then
            local lastData = this.momentCacheList[this.momentCacheList.Count - 1]
            lastId = lastData.data.id
        end
        CPersonalSpaceMgr.Inst:GetMoments(targetid, lastId, DelegateFactory.Action_CPersonalSpace_GetMoments_Ret(function (data)

            if not this or this == nil then
                return
            end
            if data.data.list ~= nil then
                CPersonalSpaceMgr.Inst:updateMomentCacheList(this.momentCacheDic, this.momentCacheList, this.poolNode)
                -- save the old node for next show use, this can speed up the init
                this:UpdateFriendCircleScrollView(data, true)
            else
                this.statusListScrollViewIndicator.enableShowNode = false
                --TODO show no more
            end
        end), nil)
    end)
end
CPersonalSpaceGMWnd.m_InitMomentInfo_CS2LuaHook = function (this, playerID)
    CPersonalSpaceMgr.Inst:GetMoments(CPersonalSpaceMgr.Inst.GM_ID, 0, DelegateFactory.Action_CPersonalSpace_GetMoments_Ret(function (data)
        if not this or this == nil then
            return
        end

        if not this.statusListScrollViewIndicator then
            return
        end

        this.statusListScrollViewIndicator.enableShowNode = true
        this:UpdateFriendCircleScrollView(data, false)
    end), nil)

    local showMoreBtn = this.statusListScrollViewIndicator.transform:Find("ShowNode").gameObject
    this:SetShowMoreBtn(showMoreBtn, CPersonalSpaceMgr.Inst.GM_ID)
end
CPersonalSpaceGMWnd.m_updateMomentCache_CS2LuaHook = function (this, newlist, noReset)
    if newlist.Length <= 0 then
        return
    end

    local needReList = false

    do
        local i = 0
        while i < newlist.Length do
            local cacheData = CreateFromClass(CPersonalSpace_MomentCacheData)
            cacheData.data = newlist[i]
            --if (!CPersonalSpaceMgr.Inst.momentCacheDic.ContainsKey(cacheData.data.id))
            if not CommonDefs.DictContains(this.momentCacheDic, typeof(UInt64), cacheData.data.id) then
                needReList = true

                if not noReset then
                    local node = NGUITools.AddChild(this.poolNode, this.statusTemplateBar)

                    node:SetActive(true)
                    cacheData.node = this:GenerateFriendCircleListNodeFunction(node, cacheData.data, this.statusListScrollView, true)
                    node:SetActive(false)
                end
                CommonDefs.DictSet(this.momentCacheDic, typeof(UInt64), cacheData.data.id, typeof(CPersonalSpace_MomentCacheData), cacheData)
            else
                local node = CommonDefs.DictGetValue(this.momentCacheDic, typeof(UInt64), cacheData.data.id).node
                if node ~= nil then
                    if not noReset then
                        local activeSign = false
                        if node.activeSelf then
                            activeSign = true
                        end
                        if not activeSign then
                            node:SetActive(true)
                        end
                        cacheData.node = this:GenerateFriendCircleListNodeFunction(node, cacheData.data, this.statusListScrollView, false)
                        if not activeSign then
                            node:SetActive(false)
                        end
                    end

                    CommonDefs.DictSet(this.momentCacheDic, typeof(UInt64), cacheData.data.id, typeof(CPersonalSpace_MomentCacheData), cacheData)
                else
                    needReList = true

                    if not noReset then
                        node = NGUITools.AddChild(this.poolNode, this.statusTemplateBar)

                        node:SetActive(true)
                        cacheData.node = this:GenerateFriendCircleListNodeFunction(node, cacheData.data, this.statusListScrollView, true)
                        node:SetActive(false)
                    end

                    CommonDefs.DictSet(this.momentCacheDic, typeof(UInt64), cacheData.data.id, typeof(CPersonalSpace_MomentCacheData), cacheData)
                end
            end
            i = i + 1
        end
    end

    if needReList then
        CPersonalSpaceMgr.Inst:updateMomentCacheList(this.momentCacheDic, this.momentCacheList, this.poolNode)
    end
end
CPersonalSpaceGMWnd.m_InitDetailPicPanel_CS2LuaHook = function (this, info, texture)
    this.detailPicPanel:SetActive(true)
    local backBtn = this.detailPicPanel.transform:Find("darkbg").gameObject
    local picNode = this.detailPicPanel.transform:Find("pic").gameObject
    local smallPicNode = this.detailPicPanel.transform:Find("smallpic").gameObject
    CommonDefs.GetComponent_GameObject_Type(smallPicNode, typeof(UITexture)).mainTexture = texture
    picNode:SetActive(false)
    smallPicNode:SetActive(true)
    UIEventListener.Get(backBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        this.detailPicPanel:SetActive(false)
        local mainTexture = CommonDefs.GetComponent_GameObject_Type(picNode, typeof(UITexture)).mainTexture
        CommonDefs.GetComponent_GameObject_Type(picNode, typeof(UITexture)).mainTexture = nil
        Object.Destroy(mainTexture)
    end)

    CPersonalSpaceMgr.DownLoadPic(info.pic, CommonDefs.GetComponent_GameObject_Type(picNode, typeof(UITexture)), CPersonalSpaceMgr.MaxPicHeight, CPersonalSpaceMgr.MaxPicWidth, DelegateFactory.Action(function ()
        if not this then
            return
        end
        picNode:SetActive(true)
        smallPicNode:SetActive(false)
    end), false, true)
end
CPersonalSpaceGMWnd.m_SetMomentPic_CS2LuaHook = function (this, node, info)
    if System.String.IsNullOrEmpty(info.thumb) then
        return
    end

    --TODO check cache
    this:cleanBtn(node)

    CPersonalSpaceMgr.Inst:DownLoadPortraitPic(info.thumb, CommonDefs.GetComponent_GameObject_Type(node, typeof(UITexture)), DelegateFactory.Action_Texture(function (texture)
        if not this then
            return
        end
        UIEventListener.Get(node).onClick = DelegateFactory.VoidDelegate(function (p)
            this:InitDetailPicPanel(info, texture)
        end)
    end))
end
CPersonalSpaceGMWnd.m_GenerateFriendCircleListNodeFunction_CS2LuaHook = function (this, node, info, scrollView, totalReNew)
    CPersonalSpaceMgr.Inst:SetNodeDragScroll(node, scrollView)
    local subInfo = node.transform:Find("subInfo")
    local statusLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("status"), typeof(UILabel))
    local favorIcon = node.transform:Find("subInfo/favoricon").gameObject
    local favorbuttonSign = node.transform:Find("subInfo/favoricon/UI_aixin").gameObject
    local favorNumLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("subInfo/favornum"), typeof(UILabel))

    if totalReNew then
        local showText = string.gsub(string.gsub(info.text, "&lt;", "<"), "&gt;", ">")
        showText = CChatMgr.Inst:FilterYangYangEmotion(showText)
        statusLabel.text = CChatLinkMgr.TranslateToNGUIText(showText, false)
        UIEventListener.Get(statusLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
            local url = CommonDefs.GetComponent_GameObject_Type(statusLabel.gameObject, typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
            if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
                return
            end
        end)
    end

    local infoTime = info.createtime / 1000
    local nowTime = CServerTimeMgr.Inst.timeStamp
    local disTime = nowTime - infoTime
    if disTime < 3600 then
        CommonDefs.GetComponent_Component_Type(node.transform:Find("time"), typeof(UILabel)).text = CPersonalSpaceMgr.MonthNameArray[0]
    elseif disTime < 86400 --[[24 * 3600]] then
        CommonDefs.GetComponent_Component_Type(node.transform:Find("time"), typeof(UILabel)).text = CPersonalSpaceMgr.MonthNameArray[0]
    else
        local infoTimeDate = CServerTimeMgr.ConvertTimeStampToZone8Time(infoTime)
        CommonDefs.GetComponent_Component_Type(node.transform:Find("time"), typeof(UILabel)).text = ((infoTimeDate.Month .. LocalString.GetString("月")) .. infoTimeDate.Day) .. LocalString.GetString("日")
    end

    local yShift = - statusLabel.height

    local picNode1 = node.transform:Find("pic1").gameObject
    local picNode2 = node.transform:Find("pic2").gameObject
    local picNode3 = node.transform:Find("pic3").gameObject

    local picNodeArray = Table2ArrayWithCount({picNode1, picNode2, picNode3}, 3, MakeArrayClass(GameObject))
    picNode1:SetActive(false)
    picNode2:SetActive(false)
    picNode3:SetActive(false)

    if info.imglist ~= nil and info.imglist.Length > 0 then
        do
            local i = 0
            while i < info.imglist.Length do
                local imageInfo = info.imglist[i]
                local picNode = picNodeArray[i]
                picNode:SetActive(true)
                this:SetMomentPic(picNode, imageInfo)
                picNode.transform.localPosition = Vector3(picNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift, picNode.transform.localPosition.z)
                i = i + 1
            end
        end
        yShift = yShift - CPersonalSpaceWnd.DefaultPicHeight
    end

    subInfo.localPosition = Vector3(subInfo.localPosition.x, yShift, subInfo.localPosition.z)

    --//favor
    favorIcon:SetActive(false)
    favorbuttonSign:SetActive(false)

    if info.zanlist ~= nil and info.zancount > 0 then
        if info.is_user_liked then
            favorIcon:SetActive(true)
        else
            favorIcon:SetActive(false)
        end

        favorNumLabel.text = tostring(info.zancount)

        do
            local zan_i = 0
            while zan_i < info.zanlist.Length do
                local zanInfo = info.zanlist[zan_i]
                if CClientMainPlayer.Inst ~= nil then
                    if zanInfo.roleid == CClientMainPlayer.Inst.Id then
                        if info.showFavorSign then
                            favorbuttonSign:SetActive(true)
                            info.showFavorSign = false
                            this:updateMomentCache(Table2ArrayWithCount({info}, 1, MakeArrayClass(CPersonalSpace_GetMomentItem)), true)
                        else
                            favorbuttonSign:SetActive(false)
                        end
                    end
                end
                zan_i = zan_i + 1
            end
        end
    else
        favorNumLabel.text = "0"
    end

    local totalHeight = 0

    local selfbgBtnSprite = CommonDefs.GetComponent_Component_Type(node.transform:Find("selfbgbutton"), typeof(UISprite))

    if info.imglist ~= nil and info.imglist.Length > 0 then
        selfbgBtnSprite.height = CPersonalSpaceWnd.DefaultSelfBgButtonHeight + totalHeight + CPersonalSpaceWnd.DefaultPicHeight
    else
        selfbgBtnSprite.height = CPersonalSpaceWnd.DefaultSelfBgButtonHeight + totalHeight
    end

    UIEventListener.Get(node.transform:Find("subInfo/favorbutton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        CPersonalSpaceMgr.Inst:LikeMoment(info.id, favorIcon.activeSelf, DelegateFactory.Action_CPersonalSpace_BaseRet(function (data)

            if not this or this == nil then
                return
            end
            if data.code == 0 then
                CPersonalSpaceMgr.Inst:GetSingleMomentInfo(info.id, DelegateFactory.Action_CPersonalSpace_GetSingleMoment_Ret(function (ret)
                    if not this then
                        return
                    end
                    if ret.code == 0 then
                        local newItem = ret.data
                        if not favorIcon.activeSelf and newItem.is_user_liked then
                            newItem.showFavorSign = true
                        else
                            newItem.showFavorSign = false
                        end

                        local updateList = Table2ArrayWithCount({newItem}, 1, MakeArrayClass(CPersonalSpace_GetMomentItem))

                        this:updateMomentCache(updateList, false)
                        this:ReposFriendCircleList(scrollView, nil, nil)
                    end
                end), nil)
            end
        end), DelegateFactory.Action(function ()
        end))
    end)

    return node
end
CPersonalSpaceGMWnd.m_ReposFriendCircleList_CS2LuaHook = function (this, _scrollView, _table, _panel)
    if _scrollView == nil then
        _scrollView = this.statusListScrollView
    end

    if _table == nil then
        _table = CommonDefs.GetComponentInChildren_Component_Type(_scrollView, typeof(UITable))
        if _table == nil then
            return
        end
    end

    if _panel == nil then
        _panel = CommonDefs.GetComponent_Component_Type(_scrollView, typeof(UIPanel))
        if _panel == nil then
            return
        end
    end

    local savePos = _scrollView.transform.localPosition
    local saveOffsetY = _panel.clipOffset.y

    _table:Reposition()
    _scrollView:ResetPosition()

    _scrollView.transform.localPosition = savePos
    _panel.clipOffset = Vector2(_panel.clipOffset.x, saveOffsetY)
end
CPersonalSpaceGMWnd.m_UpdateFriendCircleScrollView_CS2LuaHook = function (this, data, savePos)
    if data.data.list == nil or (data.data.list ~= nil and data.data.list.Length < CPersonalSpaceGMWnd.MaxEveryGetNum) then
        this.statusListScrollViewIndicator.enableShowNode = false
    end

    if data.code == 0 then
        if data.data.list ~= nil and data.data.list.Length > 0 then
            this:updateMomentCache(data.data.list, false)
        end
    end
    this:InitMomentCircleList(this.statusListScrollView, this.statusListPanel, this.statusListTable, this.momentCacheList, this.sendStatusPanel_emptyNode, savePos)
end
CPersonalSpaceGMWnd.m_InitMomentCircleList_CS2LuaHook = function (this, _scrollView, _panel, _table, momentCacheList, emptyNode, savePosSign)

    local savePos = _scrollView.transform.localPosition
    local saveOffsetY = _panel.clipOffset.y

    Extensions.RemoveAllChildren(_table.transform)

    if momentCacheList.Count <= 0 then
        return
    end

    local count = 0
    do
        local i = 0
        while i < momentCacheList.Count do
            local node = momentCacheList[i].node

            if node ~= nil and _table.gameObject ~= nil then
                node:SetActive(false)
                local t = node.transform
                t.parent = _table.gameObject.transform
                node:SetActive(true)
                t.localPosition = Vector3.zero
                t.localRotation = Quaternion.identity
                t.localScale = Vector3.one
                node.layer = _table.gameObject.layer

                if count == 0 then
                    CommonDefs.GetComponent_GameObject_Type(node.transform:Find("time/divide2").gameObject, typeof(UISprite)).alpha = 0
                else
                    CommonDefs.GetComponent_GameObject_Type(node.transform:Find("time/divide2").gameObject, typeof(UISprite)).alpha = 1
                end

                count = count + 1

                node.transform:Find("time").gameObject:SetActive(true)
                node.transform:Find("selfbgbutton").gameObject:SetActive(true)
                CommonDefs.GetComponent_Component_Type(node.transform:Find("selfbgbutton"), typeof(UISprite)):ResizeCollider()
            end
            i = i + 1
        end
    end

    if _table.transform.childCount <= 0 then
        emptyNode:SetActive(true)
    else
        emptyNode:SetActive(false)
    end

    _table:Reposition()
    _scrollView:ResetPosition()

    if savePosSign then
        _scrollView.transform.localPosition = savePos
        _panel.clipOffset = Vector2(_panel.clipOffset.x, saveOffsetY)
    end
end
CPersonalSpaceGMWnd.m_cleanBtn_CS2LuaHook = function (this, btn)
    UIEventListener.Get(btn).onClick = nil
end
CPersonalSpaceGMWnd.m_setSendFlowerBtn_CS2LuaHook = function (this, itemId, go, qnAddButton, targetId, targetName)
    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (p)
        local sendNum = qnAddButton:GetValue()
        local itemHaveNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemId)
        if sendNum <= itemHaveNum then
            if sendNum <= 0 then
                g_MessageMgr:ShowMessage("ENTER_NUMBER_TIP")
                return
            end
            CPersonalSpaceMgr.Inst:SendFlower(targetId, targetName, itemId, sendNum, CIMMgr.IM_GM_SubID)
        else
            CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, false, go.transform, AlignType.Right)
        end
    end)
end
CPersonalSpaceGMWnd.m_OnSendItem_CS2LuaHook = function (this, itemId)
    if CClientMainPlayer.Inst == nil then
        return
    end

    local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(EnumItemPlace.Bag, itemId)

    if pos > 0 then
        local commonItem = CItemMgr.Inst:GetById(itemId)
        if commonItem ~= nil and commonItem.TemplateId == this.SendFlowerNowShowTargetId then
            local itemHaveNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, commonItem.TemplateId)
            local ItemNum = CommonDefs.GetComponent_Component_Type(this.sendFlowerPanel.transform:Find("ItemShow/num"), typeof(UILabel))
            ItemNum.text = tostring(itemHaveNum)

            if this.SendFlowerNowShowPlayerId > 0 then
                this:InitSendFlowerPanel(this.SendFlowerNowShowPlayerId, this.SendFlowerNowShowPlayerName, this.SendFlowerNowShowIndex, this.SendFlowerNowShowInputValue)
            end
        end
    end
end
CPersonalSpaceGMWnd.m_OnSetItemAt_CS2LuaHook = function (this, place, pos, oldId, newId)
    local commonItem = CItemMgr.Inst:GetById(newId)
    if commonItem ~= nil and commonItem.TemplateId == this.SendFlowerNowShowTargetId then
        local itemHaveNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, commonItem.TemplateId)
        local ItemNum = CommonDefs.GetComponent_Component_Type(this.sendFlowerPanel.transform:Find("ItemShow/num"), typeof(UILabel))
        ItemNum.text = tostring(itemHaveNum)
    else
        local ItemNum = CommonDefs.GetComponent_Component_Type(this.sendFlowerPanel.transform:Find("ItemShow/num"), typeof(UILabel))
        ItemNum.text = "0"
    end

    if this.SendFlowerNowShowPlayerId > 0 then
        this:InitSendFlowerPanel(this.SendFlowerNowShowPlayerId, this.SendFlowerNowShowPlayerName, this.SendFlowerNowShowIndex, this.SendFlowerNowShowInputValue)
    end
end
CPersonalSpaceGMWnd.m_InitSendFlowerPanel_CS2LuaHook = function (this, targetId, targetName, chooseIndex, inputValue)
    this.sendFlowerPanel:SetActive(true)
    this.sendFlowerTemplate:SetActive(false)
    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    this.SendFlowerNowShowPlayerId = targetId
    this.SendFlowerNowShowPlayerName = targetName

    UIEventListener.Get(this.sendFlowerPanel.transform:Find("CloseButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        this.sendFlowerPanel:SetActive(false)
        EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
        EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    end)

    local ItemLabel = CommonDefs.GetComponent_Component_Type(this.sendFlowerPanel.transform:Find("ItemShow/name"), typeof(UILabel))
    local ItemNum = CommonDefs.GetComponent_Component_Type(this.sendFlowerPanel.transform:Find("ItemShow/num"), typeof(UILabel))
    local ItemIcon = CommonDefs.GetComponent_Component_Type(this.sendFlowerPanel.transform:Find("ItemShow/icon"), typeof(CUITexture))

    local scrollview = CommonDefs.GetComponent_Component_Type(this.sendFlowerPanel.transform:Find("ScrollView"), typeof(UIScrollView))
    local table = CommonDefs.GetComponent_Component_Type(this.sendFlowerPanel.transform:Find("ScrollView/Table"), typeof(UITable))

    local getMoreBtn = this.sendFlowerPanel.transform:Find("GetMoreButton").gameObject
    local submitBtn = this.sendFlowerPanel.transform:Find("SubmitButton").gameObject
    this:cleanBtn(getMoreBtn)
    this:cleanBtn(submitBtn)

    local qnAddButton = CommonDefs.GetComponent_Component_Type(this.sendFlowerPanel.transform:Find("BuyArea/QnIncreseAndDecreaseButton"), typeof(QnAddSubAndInputButton))
    qnAddButton:SetValue(inputValue, true)
    this:cleanBtn(ItemIcon.gameObject)

    Extensions.RemoveAllChildren(table.transform)
    local flowerArray = GameplayItem_Setting.GetData().PersonalSpace_FlowerItemIds
    local showNode = CreateFromClass(MakeGenericClass(List, GameObject))

    do
        local i = 0
        while i < flowerArray.Length do
            local itemId = flowerArray[i][0]
            local nowIndex = i
            local data = Item_Item.GetData(itemId)
            if data ~= nil then
                local node = NGUITools.AddChild(table.gameObject, this.sendFlowerTemplate)
                node:SetActive(true)
                CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = data.Name
                CommonDefs.GetComponent_Component_Type(node.transform:Find("icon"), typeof(CUITexture)):LoadMaterial(data.Icon)
                local itemHaveNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemId)
                local numTextColor = Color.white
                if itemHaveNum <= 0 then
                    numTextColor = Color.red
                end
                --UILabel
                local numLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("num"), typeof(UILabel))
                numLabel.text = tostring(itemHaveNum)
                numLabel.color = numTextColor

                local showBg = node.transform:Find("bg_light").gameObject
                CommonDefs.ListAdd(showNode, typeof(GameObject), showBg)
                UIEventListener.Get(node).onClick = DelegateFactory.VoidDelegate(function (p)
                    this.SendFlowerNowShowIndex = nowIndex
                    this.SendFlowerNowShowTargetId = itemId
                    ItemIcon:LoadMaterial(data.Icon)
                    ItemLabel.text = data.Name
                    local numTextColor1 = Color.white
                    if itemHaveNum <= 0 then
                        numTextColor1 = Color.red
                    end
                    ItemNum.color = numTextColor1

                    ItemNum.text = tostring(itemHaveNum)
                    this:setGoToBuyBtn(itemId, getMoreBtn)
                    this:setSendFlowerBtn(itemId, submitBtn, qnAddButton, targetId, targetName)

                    UIEventListener.Get(ItemIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (s)
                        CItemInfoMgr.ShowLinkItemTemplateInfo(data.ID, false, nil, AlignType1.Default, 0, 0, 0, 0)
                    end)

                    CommonDefs.ListIterate(showNode, DelegateFactory.Action_object(function (___value)
                        local g = ___value
                        g:SetActive(false)
                    end))

                    showBg:SetActive(true)

                    qnAddButton.onValueChanged = DelegateFactory.Action_uint(function (value)
                        this.SendFlowerNowShowInputValue = value
                        if value > itemHaveNum then
                            ItemNum.color = Color.red
                        else
                            ItemNum.color = Color.white
                        end
                    end)
                    qnAddButton:SetValue(inputValue, true)
                end)

                UIEventListener.Get(node.transform:Find("icon").gameObject).onClick = DelegateFactory.VoidDelegate(function (s)
                    CItemInfoMgr.ShowLinkItemTemplateInfo(data.ID, false, nil, AlignType1.Default, 0, 0, 0, 0)
                end)

                if i == chooseIndex then
                    node:SendMessage("OnClick")
                end
            end
            i = i + 1
        end
    end
    table:Reposition()
    scrollview:ResetPosition()
end
