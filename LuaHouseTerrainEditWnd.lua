local QnSelectableButton = import "L10.UI.QnSelectableButton"

local Object = import "System.Object"
local Transform = import "UnityEngine.Transform"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CMainCamera=import "L10.Engine.CMainCamera"
local Screen=import "UnityEngine.Screen"
local CCameraParam=import "L10.Engine.CameraControl.CCameraParam"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"

local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"
local QnNewSlider = import "L10.UI.QnNewSlider"
local Vector2=import "UnityEngine.Vector2"

local MouseInputHandler = import "L10.Engine.MouseInputHandler"
local GameObject = import "UnityEngine.GameObject"

local EUIModuleGroup = import "L10.UI.CUIManager+EUIModuleGroup"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local SceneBrush=import "L10.Game.SceneBrush"
local Input=import "UnityEngine.Input"

LuaHouseTerrainEditWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHouseTerrainEditWnd, "CloseBtn", "CloseBtn", GameObject)
RegistChildComponent(LuaHouseTerrainEditWnd, "CameraSlider", "CameraSlider", QnNewSlider)
RegistChildComponent(LuaHouseTerrainEditWnd, "BrushSlider", "BrushSlider", QnNewSlider)
RegistChildComponent(LuaHouseTerrainEditWnd, "SizeSlider", "SizeSlider", QnNewSlider)
RegistChildComponent(LuaHouseTerrainEditWnd, "GrassNode", "GrassNode", Transform)
RegistChildComponent(LuaHouseTerrainEditWnd, "TerrainNode", "TerrainNode", Transform)
RegistChildComponent(LuaHouseTerrainEditWnd, "AddGrassButton", "AddGrassButton", GameObject)
RegistChildComponent(LuaHouseTerrainEditWnd, "ExpandTerrainButton", "ExpandTerrainButton", GameObject)
RegistChildComponent(LuaHouseTerrainEditWnd, "ExpandGrassButton", "ExpandGrassButton", GameObject)
RegistChildComponent(LuaHouseTerrainEditWnd, "BrushNode", "BrushNode", GameObject)
RegistChildComponent(LuaHouseTerrainEditWnd, "SaveButton", "SaveButton", GameObject)
RegistChildComponent(LuaHouseTerrainEditWnd, "ChangeTerrainButton", "ChangeTerrainButton", GameObject)
RegistChildComponent(LuaHouseTerrainEditWnd, "CameraBtn", "CameraBtn", QnSelectableButton)
RegistChildComponent(LuaHouseTerrainEditWnd, "MenueView", "MenueView", GameObject)
RegistChildComponent(LuaHouseTerrainEditWnd, "MenueBtn", "MenueBtn", QnSelectableButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaHouseTerrainEditWnd,"m_SceneBrush")
RegistClassMember(LuaHouseTerrainEditWnd,"m_IsEditing")
RegistClassMember(LuaHouseTerrainEditWnd,"m_IsEditingGrass")
RegistClassMember(LuaHouseTerrainEditWnd,"m_GrassTypes")
RegistClassMember(LuaHouseTerrainEditWnd,"m_GrassUsedCountLookup")
RegistClassMember(LuaHouseTerrainEditWnd,"m_SelectedGrassType")
RegistClassMember(LuaHouseTerrainEditWnd,"m_SelectedTerrainType")
RegistClassMember(LuaHouseTerrainEditWnd,"m_SelectedTerrainChannel")
RegistClassMember(LuaHouseTerrainEditWnd,"m_CameraSliderChanged")
RegistClassMember(LuaHouseTerrainEditWnd,"m_AllGrassCount")



function LuaHouseTerrainEditWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.CloseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseBtnClick()
	end)


	
	UIEventListener.Get(self.AddGrassButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddGrassButtonClick()
	end)


	
	UIEventListener.Get(self.ExpandTerrainButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:SetTerrainEdit()
	    self:OnExpandTerrainButtonClick()
	end)


	
	UIEventListener.Get(self.ExpandGrassButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:SetGrassEdit()
	    self:OnExpandGrassButtonClick()
	end)


	
	UIEventListener.Get(self.SaveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSaveButtonClick()
	end)


	
	UIEventListener.Get(self.ChangeTerrainButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChangeTerrainButtonClick()
	end)


    --@endregion EventBind end
    self.BrushNode:SetActive(false)

    self.m_IsEditingGrass=false

    self.m_SceneBrush = SceneBrush(1,"Grass_Brush")
    self.m_SceneBrush:SetColor(Color.green)

    local isMingYuan = CClientFurnitureMgr.Inst.m_IsMingYuan

    local setting = House_GroundGrassSetting.GetData()
    local sizeX,sizeZ = 0,0
    local baseX,baseZ = 0,0
    if CClientFurnitureMgr.Inst.m_IsMingYuan then
        baseX,baseZ = setting.MingYuanGrassBase[0],setting.MingYuanGrassBase[1]
        sizeX,sizeZ = setting.MingYuanDiBiaoSize[0],setting.MingYuanDiBiaoSize[1]
    else
        baseX,baseZ = setting.NormalGrassBase[0],setting.NormalGrassBase[1]
        sizeX,sizeZ = setting.DiBiaoSize[0],setting.DiBiaoSize[1]
    end
    self.m_SceneBrush.regionStart = Vector2(baseX,baseZ)
    self.m_SceneBrush.regionEnd = Vector2(baseX+sizeX-1,baseZ+sizeZ-1)

    self.CameraSlider.OnValueChanged = DelegateFactory.Action_float(function(value)
        if self.m_CameraSliderChanged then return end
        self:OnCameraSliderChanged(value)
	end)
    self.BrushSlider.OnValueChanged = DelegateFactory.Action_float(function(value)
        self:OnBrushSliderChanged(value)
	end)
    local brushThumb = self.BrushSlider.transform:GetComponent(typeof(UISlider)).thumb.gameObject
    UIEventListener.Get(brushThumb).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        if isPressed then
            if not self.BrushNode.activeSelf then
                self.m_SceneBrush:Show()
                self.m_SceneBrush:SetPosition(Vector3(Screen.width/2,Screen.height/2,0),true)

                local screenPos = CMainCamera.Main:WorldToScreenPoint(self.m_SceneBrush.mBrushObj.transform.position)
                screenPos.z=0
                local nguiWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
                nguiWorldPos.z=0
                self.BrushNode.transform.position = nguiWorldPos
                self.BrushNode:SetActive(true)
            end
        else
            self.m_SceneBrush:Hide()
            self.BrushNode:SetActive(false)
        end
    end)

    self.SizeSlider.OnValueChanged = DelegateFactory.Action_float(function(value)
        self:OnSizeSliderChanged(value)
	end)


    UIEventListener.Get(self.TerrainNode.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:SetTerrainEdit()
	end)
    UIEventListener.Get(self.GrassNode.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:SetGrassEdit()
	end)

    self.MenueBtn.OnButtonSelected = DelegateFactory.Action_bool(function(b)
        self.MenueView:SetActive(not b)
    end)
    self.CameraBtn.OnButtonSelected = DelegateFactory.Action_bool(function(b)
        CClientFurnitureMgr.Inst.TerrainEditCanRotate = b
    end)


    self.SizeSlider.m_Value = 0.5
    self.SizeSlider:SetPercentage()

    if not IsOpenHouseGroundEdit() then
        self.TerrainNode.gameObject:SetActive(false)
    end
end
function LuaHouseTerrainEditWnd:SetTerrainEdit()
    self.m_IsEditingGrass=false
    self:FoldGrassMenu()

    -- self.SizeSlider.gameObject:SetActive(false)
    self.SizeSlider.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("透明度")
	    
    self.TerrainNode:Find("Selected").gameObject:SetActive(true)
    self.GrassNode:Find("Selected").gameObject:SetActive(false)
end
function LuaHouseTerrainEditWnd:SetGrassEdit()
    self.m_IsEditingGrass=true
    self:FoldTerrainMenu()

    -- self.SizeSlider.gameObject:SetActive(true)
    self.SizeSlider.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("花草大小")

    self.TerrainNode:Find("Selected").gameObject:SetActive(false)
    self.GrassNode:Find("Selected").gameObject:SetActive(true)
end


function LuaHouseTerrainEditWnd:OnCameraSliderChanged(value)
    local relValue = (value-0.5)*2
    CameraFollow.Inst.m_T = relValue
    CameraFollow.Inst:SetCurrentRZY()
end
function LuaHouseTerrainEditWnd:OnBrushSliderChanged(value)

    local size = 1+ math.floor(value*4)
    self.m_SceneBrush:UpdateBrush(size)

    self.BrushSlider.m_UISlider.value = math.floor(value*4)/4
end
function LuaHouseTerrainEditWnd:OnSizeSliderChanged(value)
    self.m_SceneBrush.minGrassSize = value*(24-5)+5
    self.m_SceneBrush.minGroundValue = math.floor(value*(15-1))+1--0~1对应1~15

    if not self.m_IsEditingGrass then
        self.SizeSlider.m_UISlider.value = math.floor(value*14)/14
    end
end

function LuaHouseTerrainEditWnd:Init()
    self.m_AllGrassCount = {}
    CommonDefs.DictIterate(CClientHouseMgr.Inst:GetAllGrassEditCount(), 
        DelegateFactory.Action_object_object(function (___key, ___value) 
            self.m_AllGrassCount[tonumber(___key)] = tonumber(___value)
        end))

    self.m_SelectedTerrainChannel = 0
    self.m_SelectedTerrainType = 0

    if CClientHouseMgr.Inst.mFurnitureProp3 then
        local bindedBrush = CClientHouseMgr.Inst.mFurnitureProp3.GroundContainer.BindedBrush
        self.m_SelectedTerrainType = bindedBrush[1]
    end


    self.m_SelectedGrassType = 1
    self.m_SceneBrush.grassType = self.m_SelectedGrassType

    self.m_GrassTypes = {}--不刷草
    House_AllGrass.ForeachKey(function(k)
        table.insert( self.m_GrassTypes,k )
    end)

    -- self.m_GrassUsedCountLookup = LuaHouseTerrainMgr.GetGrassUsedCount()

    self:InitCurrentGrassItem()
    self:InitCurrentTerrainItem()
    self:SetGrassEdit()
    self:FoldGrassMenu()
end

--@region UIEvent

function LuaHouseTerrainEditWnd:OnCloseBtnClick()
    local dict1 = CClientHouseMgr.Inst:GetGrassDirtyData()
    local dict2 = CClientHouseMgr.Inst:GetGroundDirtyData()

    if dict1.Count>0 or dict2.Count>0 then
        MessageWndManager.ShowOKCancelMessage(
            g_MessageMgr:FormatMessage("Close_TerrainEdit_Confim"),
            DelegateFactory.Action(function ()
                CClientHouseMgr.Inst:ClearGrassEditData()
                CClientHouseMgr.Inst:ClearGroundEditData()
                CUIManager.CloseUI(CLuaUIResources.HouseTerrainEditWnd)
            end),
        nil,
        LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    else
        CUIManager.CloseUI(CLuaUIResources.HouseTerrainEditWnd)
    end
end


function LuaHouseTerrainEditWnd:OnAddGrassButtonClick()
    LuaHouseExchangeGrassWnd.s_SelectedGrassType = self.m_SelectedGrassType
    CUIManager.ShowUI(CLuaUIResources.HouseExchangeGrassWnd)
end


local function setLocalPosX(tf,newX)
    local prePos = tf.localPosition
    prePos.x = newX
    tf.localPosition.x = prePos
end

function LuaHouseTerrainEditWnd:UnfoldTerrainMenu()
    local unfold = false
    local name = self.TerrainNode:Find("Name")
    local selectedSprite = self.TerrainNode:Find("Selected/Selected"):GetComponent(typeof(UISprite))
    local currentItem = self.TerrainNode:Find("CurrentItem").gameObject
    local selected2 = self.TerrainNode:Find("Selected/Selected2").gameObject
    selected2:SetActive(false)

    self.ExpandTerrainButton.transform.localPosition = Vector3(-575,-32,0)
    self.ExpandTerrainButton.transform.localEulerAngles = Vector3(0,0,180)
    name.localPosition = Vector3(-550,20,0)
    selectedSprite.width = 950

    currentItem:SetActive(false)
    self.ChangeTerrainButton:SetActive(false)

    local tableView = self.TerrainNode:Find("TableView"):GetComponent(typeof(QnTableView))
    tableView.gameObject:SetActive(true)

    tableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return 4
        end,
        function(item,row)
            self:InitTerrainItem(item,row)
        end)

    tableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        local bindedBursh = CClientHouseMgr.Inst.mFurnitureProp3.GroundContainer.BindedBrush
        local type = bindedBursh[row+1]

        self.m_SelectedTerrainChannel = row
        self.m_SelectedTerrainType = type
        self:InitCurrentTerrainItem()

        self.m_SceneBrush.m_GroundChannel = row

        if unfold then
            self:FoldTerrainMenu()
            unfold=false
        end
    end)
    tableView:ReloadData(true,false)
    tableView:SetSelectRow(self.m_SelectedTerrainChannel,true)
    unfold = true
end
function LuaHouseTerrainEditWnd:InitTerrainItem(item,row)
    local tf = item.transform
    local basicMark = tf:Find("BasicMark").gameObject
    basicMark:SetActive(row==0)

    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    local button = tf:Find("Button").gameObject
    local add = tf:Find("Add").gameObject
    local icon = tf:Find("Icon"):GetComponent(typeof(CUITexture))
    local bindedBursh = CClientHouseMgr.Inst.mFurnitureProp3.GroundContainer.BindedBrush
    local type = bindedBursh[row+1]

    local designData = House_AllGround.GetData(type)

    if row==0 then
        button:SetActive(true)
        add:SetActive(false)
        nameLabel.text = designData.Name
        icon:LoadMaterial(designData.Icon)
    else
        if type>0 then
            nameLabel.text = designData.Name
            icon:LoadMaterial(designData.Icon)
            button:SetActive(true)

            add:SetActive(false)
        else
            nameLabel.text = LocalString.GetString("空")
            icon:Clear()
            button:SetActive(false)
            add:SetActive(true)
        end
    end
    
    UIEventListener.Get(add).onClick = DelegateFactory.VoidDelegate(function(g)
        LuaHouseExchangeTerrainWnd.s_Channel = row
        LuaHouseExchangeTerrainWnd.s_Type = 0
        CUIManager.ShowUI(CLuaUIResources.HouseExchangeTerrainWnd)
    end)

    UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function(go)
        LuaHouseExchangeTerrainWnd.s_Channel = row--0~3

        local bindedBursh = CClientHouseMgr.Inst.mFurnitureProp3.GroundContainer.BindedBrush
        local type = bindedBursh[row+1]

        LuaHouseExchangeTerrainWnd.s_Type = type
        CUIManager.ShowUI(CLuaUIResources.HouseExchangeTerrainWnd)
    end)


end

function LuaHouseTerrainEditWnd:FoldTerrainMenu()
    --收缩
    local name = self.TerrainNode:Find("Name")
    local selectedSprite = self.TerrainNode:Find("Selected/Selected"):GetComponent(typeof(UISprite))
    local currentItem = self.TerrainNode:Find("CurrentItem").gameObject
    local selected2 = self.TerrainNode:Find("Selected/Selected2").gameObject
    selected2:SetActive(true)

    self.ExpandTerrainButton.transform.localPosition = Vector3(-80,-32,0)
    self.ExpandTerrainButton.transform.localEulerAngles = Vector3(0,0,0)
    name.localPosition = Vector3(-57,20,0)
    selectedSprite.width = 490

    currentItem:SetActive(true)
    self.ChangeTerrainButton:SetActive(true)

    local tableView = self.TerrainNode:Find("TableView"):GetComponent(typeof(QnTableView))
    tableView.gameObject:SetActive(false)


end
function LuaHouseTerrainEditWnd:OnExpandTerrainButtonClick()
    if self.ExpandTerrainButton.transform.localPosition.x<-500 then
        self:FoldTerrainMenu()
    else
        self:UnfoldTerrainMenu()
    end
end

function LuaHouseTerrainEditWnd:UnfoldGrassMenu()
    local unfold = false
    local name = self.GrassNode:Find("Name")
    local selectedSprite = self.GrassNode:Find("Selected/Selected"):GetComponent(typeof(UISprite))
    local currentItem = self.GrassNode:Find("CurrentItem").gameObject
    local selected2 = self.GrassNode:Find("Selected/Selected2").gameObject

    selected2:SetActive(false)

    self.ExpandGrassButton.transform.localPosition = Vector3(-730,-32,0)
    self.ExpandGrassButton.transform.localEulerAngles = Vector3(0,0,180)
    name.localPosition = Vector3(-710,20,0)
    selectedSprite.width = 1200
    --展开
    currentItem:SetActive(false)
    self.AddGrassButton:SetActive(false)

    local tableView = self.GrassNode:Find("TableView"):GetComponent(typeof(QnTableView))
    tableView.gameObject:SetActive(true)
    tableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_GrassTypes
        end,
        function(item,row)
            self:InitGrassItem(item,self.m_GrassTypes[row+1])
        end)

    tableView.OnSelectAtRow = DelegateFactory.Action_int(function(index)
        self.m_SelectedGrassType = self.m_GrassTypes[index+1]
        self.m_SceneBrush.grassType = self.m_SelectedGrassType
        self:InitCurrentGrassItem()

        if unfold then
            self:FoldGrassMenu()
            unfold = false
        end
    end)
    tableView:ReloadData(true,false)
    local targetRow = 0
    for i,v in ipairs(self.m_GrassTypes) do
        if v==self.m_SelectedGrassType then
            targetRow=i-1
        end
    end
    tableView:SetSelectRow(targetRow,true)
    unfold = true
end



function LuaHouseTerrainEditWnd:FoldGrassMenu()
    --收起菜单
    local name = self.GrassNode:Find("Name")
    local selectedSprite = self.GrassNode:Find("Selected/Selected"):GetComponent(typeof(UISprite))
    local currentItem = self.GrassNode:Find("CurrentItem").gameObject
    local selected2 = self.GrassNode:Find("Selected/Selected2").gameObject
    selected2:SetActive(true)

    self.ExpandGrassButton.transform.localPosition = Vector3(-80,-32,0)
    self.ExpandGrassButton.transform.localEulerAngles = Vector3(0,0,0)
    name.localPosition = Vector3(-57,20,0)
    selectedSprite.width = 490
    --收起来
    currentItem:SetActive(true)
    self.AddGrassButton:SetActive(true)
    local clearButton = currentItem.transform:Find("ClearButton").gameObject

    if self.m_SelectedGrassType==0 then
        self.AddGrassButton:SetActive(false)
        clearButton:SetActive(true)
        UIEventListener.Get(clearButton).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnClickClearGrassButton()
        end)
    else
        clearButton:SetActive(false)

        local max = LuaHouseTerrainMgr.GetGrassMaxNum(self.m_SelectedGrassType)
        local limitNum = LuaHouseTerrainMgr.GetGrassLimitNum(self.m_SelectedGrassType)

        if max<limitNum then--上限100个
            self.AddGrassButton:SetActive(true)
        else
            self.AddGrassButton:SetActive(false)
        end
    end

    local tableView = self.GrassNode:Find("TableView"):GetComponent(typeof(QnTableView))
    tableView.gameObject:SetActive(false)
end

function LuaHouseTerrainEditWnd:OnClickClearGrassButton()
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Clear_AllGrass_Confirm"),
    function()
        self:ClearGrass()
    end, nil, nil, nil, false)
end
function LuaHouseTerrainEditWnd:OnExpandGrassButtonClick()
    if self.ExpandGrassButton.transform.localPosition.x<-500 then
        self:FoldGrassMenu()
    else
        self:UnfoldGrassMenu()
    end
end
function LuaHouseTerrainEditWnd:InitCurrentGrassItem()
    local currentItem = self.GrassNode:Find("CurrentItem").gameObject

    local tf=currentItem.transform
    local countLabel = tf:Find("AmountLabel"):GetComponent(typeof(UILabel))
    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    local icon = tf:GetComponent(typeof(CUITexture))

    if self.m_SelectedGrassType==0 then
        countLabel.text = nil

    else
        local max = LuaHouseTerrainMgr.GetGrassMaxNum(self.m_SelectedGrassType)
        local usedCount = self.m_AllGrassCount[self.m_SelectedGrassType] or 0
        countLabel.text = SafeStringFormat3( "%d/%d",usedCount,max )
    end
    local designData = House_AllGrass.GetData(self.m_SelectedGrassType)
    nameLabel.text = designData.Name
    icon:LoadMaterial(designData.Icon)
end

function LuaHouseTerrainEditWnd:InitCurrentTerrainItem()
    local currentItem = self.TerrainNode:Find("CurrentItem").gameObject

    local tf=currentItem.transform
    local basicMark = tf:Find("BasicMark").gameObject
    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    local icon = tf:GetComponent(typeof(CUITexture))

    if self.m_SelectedTerrainChannel==0 then
        basicMark:SetActive(true)
        local designData = House_AllGround.GetData(self.m_SelectedTerrainType)
        nameLabel.text = designData.Name
        icon:LoadMaterial(designData.Icon)
    else
        basicMark:SetActive(false)

        if self.m_SelectedTerrainType==0 then
            nameLabel.text = LocalString.GetString("空")
            icon:Clear()
        else
            local designData = House_AllGround.GetData(self.m_SelectedTerrainType)
            nameLabel.text = designData.Name
            icon:LoadMaterial(designData.Icon)
        end
    end
end

function LuaHouseTerrainEditWnd:InitGrassItem(item,grassType)
    local tf=item.transform
    local nameLabel=tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    local button = tf:Find("Button").gameObject
    local clearButton = tf:Find("ClearButton").gameObject
    local countLabel = tf:Find("AmountLabel"):GetComponent(typeof(UILabel))
    local icon = tf:Find("Icon"):GetComponent(typeof(CUITexture))
    local designData = House_AllGrass.GetData(grassType)
    icon:LoadMaterial(designData.Icon)
    nameLabel.text = designData.Name

    if grassType==0 then
        button:SetActive(false)
        clearButton:SetActive(true)
        UIEventListener.Get(clearButton).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnClickClearGrassButton()
        end)
        countLabel.text=nil
    else
        clearButton:SetActive(false)
        local max = LuaHouseTerrainMgr.GetGrassMaxNum(grassType)
        local usedCount = self.m_AllGrassCount[grassType] or 0
        local limitNum = LuaHouseTerrainMgr.GetGrassLimitNum(grassType)

        if max<limitNum then--上限100个
            button:SetActive(true)
            UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function(go)
                LuaHouseExchangeGrassWnd.s_SelectedGrassType = grassType
                CUIManager.ShowUI(CLuaUIResources.HouseExchangeGrassWnd)
            end)
        else
            button:SetActive(false)
        end
        countLabel.text = SafeStringFormat3( "%d/%d",usedCount,max )
    end
end


function LuaHouseTerrainEditWnd:OnSaveButtonClick()
    self:SaveGrass()
    LuaHouseTerrainMgr:SaveGround()
end
function LuaHouseTerrainEditWnd:SaveGrass()
    local dict = CClientHouseMgr.Inst:GetGrassDirtyData()
    local raw = {}

    CommonDefs.DictIterate(dict,DelegateFactory.Action_object_object(function (___key, ___value)
        table.insert( raw, ___key )
        table.insert( raw, ___value )
    end))

    return self:InternalSaveGrass(raw)
end
function LuaHouseTerrainEditWnd:ClearGrass()
    local raw = {}
    local bytes = CClientHouseMgr.Inst.mFurnitureProp3.GrassContainer.Grass
    for i=1,bytes.Length do
        local b = bytes[i-1]
        if b>0 then
            table.insert( raw,i-1 )
            table.insert( raw,0 )
        end
    end
    self:InternalSaveGrass(raw)
end
function LuaHouseTerrainEditWnd:InternalSaveGrass(raw)
    local setting = House_GroundGrassSetting.GetData()
    local width = setting.MaxGGSize[0]
    local height = setting.MaxGGSize[1]

    local baseX,baseZ = 0,0
    if CClientFurnitureMgr.Inst.m_IsMingYuan then
        baseX,baseZ = setting.MingYuanGrassBase[0],setting.MingYuanGrassBase[1]
    else
        baseX,baseZ = setting.NormalGrassBase[0],setting.NormalGrassBase[1]
    end


	local maxCount = 60--一个小u存30条数据，1条rpc也就120条数据
    local listTbl = {}
    local list = CreateFromClass(MakeGenericClass(List, Object))
    for i=1,#raw,maxCount do
		list:Clear()
        for j=1,maxCount,2 do
            local idx1 = (i-1)+j
            local idx2 = idx1+1
            local k = raw[idx1]
            local v = raw[idx2]
            if k and v then
                local z = math.floor(k/width)
                local x = k%width
                z = z+baseZ
                x = x+baseX

                local type = bit.band(v, 0xf)
                local height = bit.rshift(v,4)
                CommonDefs.ListAdd(list, typeof(UInt32), math.floor(x))
                CommonDefs.ListAdd(list, typeof(UInt32), math.floor(z))
                CommonDefs.ListAdd(list, typeof(UInt32), math.floor(type))
                CommonDefs.ListAdd(list, typeof(UInt32), math.floor(height))
                -- print("request",x,z,type,height)
            end
        end
        table.insert(listTbl,MsgPackImpl.pack(list))
    end


    local rpcCount = math.ceil(#listTbl/4)
    -- print("rpcCount",rpcCount)
    local empty = CreateFromClass(MakeGenericClass(List, Object))
    local u = MsgPackImpl.pack(empty)
    for i=0,rpcCount-1 do
        RegisterTickOnce(function()
            local arg1 = listTbl[i*4+1] and listTbl[i*4+1] or u
            local arg2 = listTbl[i*4+2] and listTbl[i*4+2] or u
            local arg3 = listTbl[i*4+3] and listTbl[i*4+3] or u
            local arg4 = listTbl[i*4+4] and listTbl[i*4+4] or u
            Gac2Gas.RequestSetGrassType(arg1,arg2,arg3,arg4)
        end,100*i+10)
    end
    return rpcCount
end

function LuaHouseTerrainEditWnd:OnChangeTerrainButtonClick()
    LuaHouseExchangeTerrainWnd.s_Channel = self.m_SelectedTerrainChannel
    LuaHouseExchangeTerrainWnd.s_Type = self.m_SelectedTerrainType
    CUIManager.ShowUI(CLuaUIResources.HouseExchangeTerrainWnd)
end


--@endregion UIEvent
local canalert = true
local tick = nil
function LuaHouseTerrainEditWnd:Update()
    if CClientFurnitureMgr.Inst.TerrainEditCanRotate then
        return
    end

    local canedit = false
    if Input.GetMouseButtonDown(0) then 
        if not CUICommonDef.IsOverUI then
            self.m_SceneBrush:Show()
            self.m_IsEditing = true
            canedit = true
            -- needalert=true--点击的时候才提示

            if tick then
                UnRegisterTick(tick)
                tick=nil
            end
            canalert=true
        end
    elseif Input.GetMouseButton(0) then
        if self.m_IsEditing then
            canedit = true
        end
    elseif Input.GetMouseButtonUp(0) then
            self.m_SceneBrush:Hide()
            self.m_IsEditing = false
    end

    if canedit then
        self.m_SceneBrush:SetPosition(Input.mousePosition,true)
        if self.m_IsEditingGrass then
            local added=false
            if self.m_SelectedGrassType>0 then
                local max = LuaHouseTerrainMgr.GetGrassMaxNum(self.m_SelectedGrassType)
                local cur = CClientHouseMgr.Inst:GetGrassEditCount(self.m_SelectedGrassType)
                local maxAddCount = math.max(0,max-cur)
                self.m_SceneBrush:ApplyBrushStroke(self.m_SceneBrush.Position,self.m_SceneBrush.brushSize,maxAddCount)
            else
                -- added = true
                self.m_SceneBrush:ApplyBrushStroke(self.m_SceneBrush.Position,self.m_SceneBrush.brushSize,0)
            end
            --更新所有的格子数量
            CommonDefs.DictIterate(CClientHouseMgr.Inst:GetAllGrassEditCount(), DelegateFactory.Action_object_object(function (___key, ___value) 
                self.m_AllGrassCount[tonumber(___key)] = tonumber(___value)
            end))
            self:UpdateAllGrassCount()
        else
            if self.m_SelectedTerrainChannel>0 and self.m_SelectedTerrainType==0 then
                --其他通道没有笔刷的时候 没有效果
            else
                self.m_SceneBrush:ApplyBrushStroke2(self.m_SceneBrush.Position,self.m_SceneBrush.brushSize)
            end
        end
    end

    if canalert then
        local havealert = false
        if self.m_SceneBrush.m_ExceedRegion then
            g_MessageMgr:ShowMessage("Grass_Region_Exceed")
            havealert=true
        elseif self.m_SceneBrush.m_InvalidRegion then
            g_MessageMgr:ShowMessage("Grass_Region_InValid")
            havealert=true
        elseif self.m_SceneBrush.m_CountExceed then
            g_MessageMgr:ShowMessage("GRASS_GRID_EXCEED")
            havealert=true
        end
        self.m_SceneBrush.m_ExceedRegion = false
        self.m_SceneBrush.m_InvalidRegion = false
        self.m_SceneBrush.m_CountExceed = false
        --4秒提示一次
        if havealert then
            canalert=false
            tick = RegisterTickOnce(function()
                canalert=true
                tick=nil
            end,4000)
        end
    end
end

function LuaHouseTerrainEditWnd:OnDestroy()
    Gac2Gas.RequestEndEditYardGround()
    if self.m_SceneBrush then
        self.m_SceneBrush:OnDestroy()
    end
end

local enablePostEffect = false
function LuaHouseTerrainEditWnd:OnEnable()
    enablePostEffect = CMainCamera.m_EnablePostEffect
    CMainCamera.m_EnablePostEffect = false
    CameraFollow.Inst.mShowWnd = false
    CameraFollow.Inst.mAffectUI = false
    CCameraParam.MIN_DIST_TO_PLAYER = 28.6104 - 10
    CCameraParam.MIN_DIST_TO_PLAYER_Default = 28.6104 - 10
    CCameraParam.MAX_DIST_TO_PLAYER = 28.6104 + 10
    CameraFollow.Inst.m_T = 0
    CameraFollow.Inst:SetCurrentRZY()

    MouseInputHandler.Inst.ForbidClickMove = true
    CClientFurnitureMgr.Inst.TerrainEditMode = true
    CClientFurnitureMgr.Inst.TerrainEditCanRotate = false

    local excepts = {"MiddleNoticeCenter","Joystick"}
	local List_String = MakeGenericClass(List,cs_string)
	CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
    CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)

    -- g_ScriptEvent:AddListener("OnSendHouseData", self, "OnUpdateHousePoolGridNum")
    g_ScriptEvent:AddListener("MainCameraAngleChanged",self,"OnMainCameraAngleChanged")
    g_ScriptEvent:AddListener("SyncHouseGrassGridNum", self, "OnSyncHouseGrassGridNum")
    g_ScriptEvent:AddListener("BindHouseGroundBrushType", self, "OnBindHouseGroundBrushType")
    g_ScriptEvent:AddListener("SyncHouseGrassInfo",self,"OnSyncHouseGrassInfo")
    g_ScriptEvent:AddListener("SyncHouseGroundInfo",self,"OnSyncHouseGroundInfo")
    

end
function LuaHouseTerrainEditWnd:OnDisable()
    CMainCamera.m_EnablePostEffect = enablePostEffect
    CCameraParam.MIN_DIST_TO_PLAYER = 1.2
    CCameraParam.MIN_DIST_TO_PLAYER_Default = 1.2
    CCameraParam.MAX_DIST_TO_PLAYER = 28.6104

    MouseInputHandler.Inst.ForbidClickMove = false
    CClientFurnitureMgr.Inst.TerrainEditMode = false
    CClientFurnitureMgr.Inst.TerrainEditCanRotate = false
    CameraFollow.Inst:ResetToDefault()
    

    local excepts = {"MiddleNoticeCenter","Joystick"}
	local List_String = MakeGenericClass(List,cs_string)
	CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
    CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)
    if CClientHouseMgr.Inst:IsInOwnHouse() then
        CUIManager.ShowUI(CUIResources.HouseMinimap)
    end
    -- g_ScriptEvent:AddListener("OnSendHouseData", self, "OnUpdateHousePoolGridNum")
    g_ScriptEvent:RemoveListener("MainCameraAngleChanged",self,"OnMainCameraAngleChanged")
    g_ScriptEvent:RemoveListener("BindHouseGroundBrushType", self, "OnBindHouseGroundBrushType")
    g_ScriptEvent:RemoveListener("SyncHouseGrassGridNum", self, "OnSyncHouseGrassGridNum")
    g_ScriptEvent:RemoveListener("SyncHouseGrassInfo",self,"OnSyncHouseGrassInfo")
    g_ScriptEvent:RemoveListener("SyncHouseGroundInfo",self,"OnSyncHouseGroundInfo")
end
function LuaHouseTerrainEditWnd:OnSyncHouseGrassInfo()
    -- self.m_GrassUsedCountLookup = LuaHouseTerrainMgr.GetGrassUsedCount()
    self.m_AllGrassCount = {}
    CommonDefs.DictIterate(CClientHouseMgr.Inst:GetAllGrassEditCount(), 
        DelegateFactory.Action_object_object(function (___key, ___value) 
            self.m_AllGrassCount[tonumber(___key)] = tonumber(___value)
        end))


    self:InitCurrentGrassItem()

    local tableView = self.GrassNode:Find("TableView"):GetComponent(typeof(QnTableView))
    tableView:ReloadData(false,false)

    local show_save_message = true
    RegisterTickOnce(function()
        if show_save_message then
            g_MessageMgr:ShowMessage("Save_HousePool_Succeed")
            show_save_message = false
        end
    end,1000)
end
--主要是为了只弹一个提示
local show_save_message = false
function LuaHouseTerrainEditWnd:OnSyncHouseGroundInfo()
    --保存成功
    show_save_message = true
    RegisterTickOnce(function()
        if show_save_message then
            g_MessageMgr:ShowMessage("Save_HousePool_Succeed")
            show_save_message = false
        end
    end,1000)
end

function LuaHouseTerrainEditWnd:OnMainCameraAngleChanged()
    self.m_CameraSliderChanged = true
    self.CameraSlider.m_Value = CameraFollow.Inst.m_T/2+0.5
    self.m_CameraSliderChanged = false
end
function LuaHouseTerrainEditWnd:OnSyncHouseGrassGridNum(grassType, count)
    self:InitCurrentGrassItem()

    local tableView = self.GrassNode:Find("TableView"):GetComponent(typeof(QnTableView))
    tableView:ReloadData(false,false)
end
function LuaHouseTerrainEditWnd:UpdateAllGrassCount()
    local countLabel = self.GrassNode:Find("CurrentItem"):Find("AmountLabel"):GetComponent(typeof(UILabel))
    if self.m_SelectedGrassType>0 then
        local max = LuaHouseTerrainMgr.GetGrassMaxNum(self.m_SelectedGrassType)
        local usedCount = self.m_AllGrassCount[self.m_SelectedGrassType] or 0
        countLabel.text = SafeStringFormat3( "%d/%d",usedCount,max )
    end

    local tableView = self.GrassNode:Find("TableView"):GetComponent(typeof(QnTableView))
    if tableView.Count>0 then
        for i,grassType in ipairs(self.m_GrassTypes) do
            if grassType>0 then
                local item = tableView:GetItemAtRow(i-1)
                local countLabel = item.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
                local max = LuaHouseTerrainMgr.GetGrassMaxNum(grassType)
                local usedCount = self.m_AllGrassCount[grassType] or 0
                countLabel.text = SafeStringFormat3( "%d/%d",usedCount,max )
            end
        end
    end
end
function LuaHouseTerrainEditWnd:OnBindHouseGroundBrushType(brushPos, brushType)
    local tableView = self.TerrainNode:Find("TableView"):GetComponent(typeof(QnTableView))
    tableView:ReloadData(false,false)


    self.m_SelectedTerrainType = brushType
    self.m_SelectedTerrainChannel=brushPos-1
    if self.m_SelectedTerrainType==0 then
        self.m_SelectedTerrainChannel = 0
    end
    
    self:InitCurrentTerrainItem()
    tableView:SetSelectRow(self.m_SelectedTerrainChannel,true)
end
