-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CTianFuListItem = import "L10.UI.CTianFuListItem"
local LocalString = import "LocalString"
local Object = import "System.Object"
local PlayerSettings = import "L10.Game.PlayerSettings"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTianFuListItem.m_Init_CS2LuaHook = function (this, skillLevel, skillClass) 

    this.Selected = false
    this.skillIcon:Clear()
    this.skillLvLable.text = ""
    this.nameLabel.text = nil
    this.attackTypeLabel.text = nil
    this.needPlayerLevelLabel.text = nil
    this.descLabel.text = nil
    this.m_DeltaLevelDescLabel.gameObject:SetActive(false)

    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end
    local data = Skill_AllSkills.GetData(CLuaDesignMgr.Skill_AllSkills_GetSkillId(skillClass, skillLevel > 0 and skillLevel or 1))
    if data ~= nil then
        this.skillIcon:LoadSkillIcon(data.SkillIcon)
        this.nameLabel.text = data.Name
        
        local originWithDeltaLevel = mainPlayer.SkillProp:GetTianFuSkilleltaLevel(data.SkillClass, mainPlayer.Level)

        if originWithDeltaLevel > 0 then
            this.m_DeltaLevelDescLabel.gameObject:SetActive(true)
            this.m_DeltaLevelDescLabel.text = g_MessageMgr:FormatMessage("DELTA_TIANFU_DESC", originWithDeltaLevel, data._Before_Extend_Level)
        end

        if skillLevel > 0 or originWithDeltaLevel > 0 then
            local totalLevel = (originWithDeltaLevel + skillLevel)
            if totalLevel > data._Before_Extend_Level then
                totalLevel = data._Before_Extend_Level
            end
            this.skillLvLable.text = tostring(totalLevel)
        end
        this.attackTypeLabel.text = data.AttackModeDisplay
        this.needPlayerLevelLabel.text = tostring(data.PlayerLevel)
        local default
        if (PlayerSettings.SkillSummaryDescEnabled and not System.String.IsNullOrEmpty(data.Display_jian)) then
            default = data.Display_jian
        else
            default = data.Display
        end
        this.descLabel.text = CChatLinkMgr.TranslateToNGUIText(default, false)
    end
    this.descScrollView:ResetPosition()
    this.skillId = CLuaDesignMgr.Skill_AllSkills_GetSkillId(skillClass, skillLevel == 0 and 1 or skillLevel)

    if this.m_FeiSheng and skillLevel > 0 and this.m_FeiShengLevel ~= skillLevel then
        this.descLabel.text = (System.String.Format(LocalString.GetString("凡身时，天赋等级{0}级"), skillLevel) .. "\n") .. this.descLabel.text

        this.m_NormalNode:SetActive(false)
        this.m_FeiShengNode:SetActive(true)
        local modifyLabel = CommonDefs.GetComponent_Component_Type(this.m_FeiShengNode.transform:Find("ModifyLevel"), typeof(UILabel))
        local needLabel = CommonDefs.GetComponent_Component_Type(this.m_FeiShengNode.transform:Find("NeedLevel"), typeof(UILabel))

        local modifiedLevel = this.m_FeiShengLevel
        local originWithDeltaLevel = mainPlayer.SkillProp:GetTianFuSkilleltaLevel(data.SkillClass, mainPlayer.Level)
        if modifiedLevel > 0 and originWithDeltaLevel > 0 then
            modifiedLevel = (originWithDeltaLevel + modifiedLevel)
            if modifiedLevel > data._Before_Extend_Level then
                modifiedLevel = data._Before_Extend_Level
            end
        end

        modifyLabel.text = System.String.Format(LocalString.GetString("修正等级  {0}"), modifiedLevel)
        this.m_FeiShengDescLabel.gameObject:SetActive(true)
        this.m_SplitGo:SetActive(true)
        local skillData = Skill_AllSkills.GetData(skillClass * 100 + modifiedLevel)
        if skillData ~= nil then
            needLabel.text = System.String.Format(LocalString.GetString("需求等级  {0}"), skillData.PlayerLevel)
            local extern
            if (PlayerSettings.SkillSummaryDescEnabled and not System.String.IsNullOrEmpty(skillData.Display_jian)) then
                extern = skillData.Display_jian
            else
                extern = skillData.Display
            end
            local content = CChatLinkMgr.TranslateToNGUIText(extern, false)
            this.m_FeiShengDescLabel.text = System.String.Format("[c][{0}]{1}[-][/c]", Constants.ColorOfFeiSheng, content)
        else
            needLabel.text = ""
            this.m_FeiShengDescLabel.text = System.String.Format(LocalString.GetString("[c][{0}]该天赋暂不生效[-][/c]"), Constants.ColorOfFeiSheng)
        end
        this.m_Table:Reposition()
    else
        this.m_NormalNode:SetActive(true)
        this.m_FeiShengNode:SetActive(false)
        this.m_FeiShengDescLabel.gameObject:SetActive(false)
        this.m_SplitGo:SetActive(false)
        this.m_Table:Reposition()
    end
end
CTianFuListItem.m_InitFeiSheng_CS2LuaHook = function (this, active, level) 
    this.m_FeiSheng = active
    this.m_FeiShengLevel = level
end
CTianFuListItem.m_Start_CS2LuaHook = function (this) 

    UIEventListener.Get(this.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.gameObject).onClick, MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this), true)
    UIEventListener.Get(this.descLabel.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.descLabel.gameObject).onClick, MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this), true)
    UIEventListener.Get(this.m_FeiShengDescLabel.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_FeiShengDescLabel.gameObject).onClick, MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this), true)
    UIEventListener.Get(this.skillIcon.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.skillIcon.gameObject).onClick, MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this), true)
    UIEventListener.Get(this.baseInfoRoot).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.baseInfoRoot).onClick, MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this), true)
end
CTianFuListItem.m_OnItemClick_CS2LuaHook = function (this, go) 

    if this.OnItemClickDelegate ~= nil then
        GenericDelegateInvoke(this.OnItemClickDelegate, Table2ArrayWithCount({this.gameObject}, 1, MakeArrayClass(Object)))
    end
end
