local CChatMgr = import "L10.Game.CChatMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CIMMgr = import "L10.Game.CIMMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPersonalSpace_CircleGenerateType = import "L10.Game.CPersonalSpace_CircleGenerateType"
local CPersonalSpaceDetailMoment = import "L10.UI.CPersonalSpaceDetailMoment"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CPersonalSpaceWnd = import "L10.UI.CPersonalSpaceWnd"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUITexture = import "L10.UI.CUITexture"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local L10 = import "L10"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUIText = import "NGUIText"
local NGUITools = import "NGUITools"
local Time = import "UnityEngine.Time"
local TweenPosition = import "TweenPosition"
local UICamera = import "UICamera"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt64 = import "System.UInt64"
local UIPanel = import "UIPanel"
local UIScrollView = import "UIScrollView"
local UISprite = import "UISprite"
local UITable = import "UITable"
local UITexture = import "UITexture"
local UIWidget = import "UIWidget"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local UIScrollViewIndicatorCustom = import "UIScrollViewIndicatorCustom"
local CChatInput = import "L10.UI.CChatInput"
local CChatInputMgrEParentType = import "L10.UI.CChatInputMgr+EParentType"
local CChatLinkMgr = import "CChatLinkMgr"
local CChatInputMgr = import "L10.UI.CChatInputMgr"
local EChatPanel = import "L10.Game.EChatPanel"

LuaPersonalSpaceDetailView=class()
RegistChildComponent(LuaPersonalSpaceDetailView,"favorTemplateNode", GameObject)
RegistChildComponent(LuaPersonalSpaceDetailView,"messageTemplateNode", GameObject)
RegistChildComponent(LuaPersonalSpaceDetailView,"backBtn", GameObject)
RegistChildComponent(LuaPersonalSpaceDetailView,"scrollView", UIScrollView)
RegistChildComponent(LuaPersonalSpaceDetailView,"scrollViewIndicator", UIScrollViewIndicatorCustom)
RegistChildComponent(LuaPersonalSpaceDetailView,"table", UITable)
RegistChildComponent(LuaPersonalSpaceDetailView,"infoTemplateNode", GameObject)

RegistClassMember(LuaPersonalSpaceDetailView,"sendStatusPanel_cancelBtn", GameObject)
RegistClassMember(LuaPersonalSpaceDetailView,"sendStatusPanel_statusInput", CChatInput)
RegistClassMember(LuaPersonalSpaceDetailView,"sendStatusPanel_statusText", UILabel)
RegistClassMember(LuaPersonalSpaceDetailView,"sendStatusPanel_emotionBtn", GameObject)

RegistClassMember(LuaPersonalSpaceDetailView, "formerPaneNode")
RegistClassMember(LuaPersonalSpaceDetailView, "defaultTableHeight")
RegistClassMember(LuaPersonalSpaceDetailView, "maxInfoHeight")
RegistClassMember(LuaPersonalSpaceDetailView, "defaultPicHeight")
RegistClassMember(LuaPersonalSpaceDetailView, "ReplyPrefix")
RegistClassMember(LuaPersonalSpaceDetailView, "ReplyMidfix")
RegistClassMember(LuaPersonalSpaceDetailView, "lastSendTime")
RegistClassMember(LuaPersonalSpaceDetailView, "ReplyPersonId")
RegistClassMember(LuaPersonalSpaceDetailView, "ReplyPersonName")
RegistClassMember(LuaPersonalSpaceDetailView, "existingLinks")
RegistClassMember(LuaPersonalSpaceDetailView, "MaxEveryGetNum")
RegistClassMember(LuaPersonalSpaceDetailView, "saveDefaultPivot")
RegistClassMember(LuaPersonalSpaceDetailView, "DeleteCommentAlertString")
RegistClassMember(LuaPersonalSpaceDetailView, "m_Info")
RegistClassMember(LuaPersonalSpaceDetailView, "m_FormerNode")
RegistClassMember(LuaPersonalSpaceDetailView, "panel")
RegistClassMember(LuaPersonalSpaceDetailView, "m_DefaultChatInputListener")
RegistClassMember(LuaPersonalSpaceDetailView, "barNode")


function LuaPersonalSpaceDetailView:OnBackClick()
  self.gameObject:SetActive(false)
  if self.m_FormerNode then
    self.m_FormerNode:SetActive(true)
  end

  g_ScriptEvent:BroadcastInLua('UpdatePersonalSpaceHotTalkWnd')
end

function LuaPersonalSpaceDetailView:InitFromNewData(info, showType)
  LuaPersonalSpaceMgrReal.GetSingleMomentInfo(info.id, function (ret)
    if not self or not self.backBtn then
      return
    end
    if ret.code == 0 then
      local newItem = ret.data
      self:InitNodeInfo(newItem, self.m_FormerNode, UIWidget.Pivot.Top, showType, true)
    end
  end)
end

function LuaPersonalSpaceDetailView:Init(args)
  local info = args[0]
  local showTalk = args[1]
  local formerNode = args[2]
  self.m_FormerNode = formerNode

  if info then
    local onBackClick = function(go)
      self:OnBackClick()
    end
    CommonDefs.AddOnClickListener(self.backBtn,DelegateFactory.Action_GameObject(onBackClick),false)
    self.defaultTableHeight = 695
    self.maxInfoHeight = 450
    self.defaultPicHeight = 20
    self.ReplyPrefix = LocalString.GetString('回复')
    self.ReplyMidfix = LocalString.GetString('回复')
    self.MaxEveryGetNum = 10
    self.DeleteCommentAlertString = LocalString.GetString('确定删除这条评论?')
    self.panel = self.scrollView:GetComponent(typeof(UIPanel))
    self.lastSendTime = 0
    --(_info, formerNode, defaultPivot, showTalk, totalReNew)
    self:InitNodeInfo(info,formerNode,UIWidget.Pivot.Top,showTalk,true)
    --self:InitFromNewData(info,showType)
  else
    self:OnBackClick()
  end
end

function LuaPersonalSpaceDetailView:ResetInputInfo()
    self.ReplyPersonId = 0
    self.ReplyPersonName = ""
    CommonDefs.DictClear(self.existingLinks)
    self.sendStatusPanel_statusInput.input.value = ""
end
function LuaPersonalSpaceDetailView:SetInputDefaultValue(roleid, rolename, node, info)
    if not info.commentable then
        g_MessageMgr:ShowMessage("CANNOT_COMMENT")
        return
    end

    self.ReplyPersonId = roleid
    self.ReplyPersonName = rolename
    self.sendStatusPanel_statusInput.input.value = ""
    self.sendStatusPanel_statusText.text = self.ReplyPrefix .. self.ReplyPersonName
    self.sendStatusPanel_statusInput.input.defaultText = self.ReplyPrefix .. self.ReplyPersonName
    if self.barNode then
      CommonDefs.GetComponent_Component_Type(self.barNode, typeof(TweenPosition)):PlayForward()
    end
end
function LuaPersonalSpaceDetailView:ShowInputNode(node, info)
    if info.commentable then
        local defaultReplyName = info.rolename
        if System.String.IsNullOrEmpty(defaultReplyName) then
            defaultReplyName = tostring(info.roleid)
        end

        self.sendStatusPanel_statusInput.input.value = ""
        self.sendStatusPanel_statusText.text = self.ReplyPrefix .. defaultReplyName
        self.sendStatusPanel_statusInput.input.defaultText = self.ReplyPrefix .. defaultReplyName
        if self.barNode then
          CommonDefs.GetComponent_Component_Type(self.barNode, typeof(TweenPosition)):PlayForward()
        end
    else
        g_MessageMgr:ShowMessage("CANNOT_COMMENT")
    end
end

function LuaPersonalSpaceDetailView:InitCommentListNode(commentlist, node, info, reNew, savePos)
    --if reNew then
    --    Extensions.RemoveAllChildren(node.transform)
    --end

    if commentlist and #commentlist > 0 then
        do
            local com_i = 1
            while com_i <= #commentlist do
                local comment_info = commentlist[com_i]
                local messageNode = NGUITools.AddChild(self.table.gameObject, self.messageTemplateNode)
                messageNode:SetActive(true)
                if #self.SaveCommentList == 0 then
                    messageNode.transform:Find("icon").gameObject:SetActive(true)
                else
                    messageNode.transform:Find("icon").gameObject:SetActive(false)
                end

                local showText = CPersonalSpaceMgr.GeneratePlayerName(comment_info.roleid, comment_info.rolename)
                if comment_info and comment_info.replyinfo and comment_info.replyinfo.roleid and comment_info.replyinfo.roleid ~= 0 then
                    showText = showText .. (self.ReplyMidfix .. CPersonalSpaceMgr.GeneratePlayerName(comment_info.replyinfo.roleid, comment_info.replyinfo.rolename))
                end

                showText = showText .. comment_info.text
                local textNode = messageNode.transform:Find("text")
                local textNodeLabel = CommonDefs.GetComponent_Component_Type(textNode, typeof(UILabel))
                showText = CChatMgr.Inst:FilterYangYangEmotion(showText)
                textNodeLabel.text = CChatLinkMgr.TranslateToNGUIText(showText, false)

                UIEventListener.Get(textNode.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
                    local index = textNodeLabel:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
                    if index == 0 then
                        index = - 1
                    end

                    local url = textNodeLabel:GetUrlAtCharacterIndex(index)
                    if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
                        CPersonalSpaceMgr.Inst:SetPlayerReportDefaultInfo(EnumReportId_lua.eMengDaoMoment, info.text, nil, info.id)
                        return
                    else
                        self:SetInputDefaultValue(comment_info.roleid, comment_info.rolename, self.gameObject, info)
                    end
                end)

                local commentStruct = {}
                commentStruct.id = comment_info.id
                commentStruct.node = messageNode
                table.insert(self.SaveCommentList,commentStruct)

                local deleteBtn = messageNode.transform:Find("deletebutton").gameObject
                deleteBtn:SetActive(false)
                if comment_info.roleid == CClientMainPlayer.Inst.Id or info.roleid == CClientMainPlayer.Inst.Id then
                    deleteBtn:SetActive(true)
                    UIEventListener.Get(deleteBtn).onClick = DelegateFactory.VoidDelegate(function (p)
                        MessageWndManager.ShowConfirmMessage(CPersonalSpaceDetailMoment.DeleteCommentAlertString, 10, false, DelegateFactory.Action(function ()
                            CPersonalSpaceMgr.Inst:DelComment(comment_info.id, DelegateFactory.Action_CPersonalSpace_BaseRet(function (data)
                              if data.code == 0 then
                                LuaPersonalSpaceMgrReal.GetSingleMomentInfo(self.m_Info.id, function (ret)
                                  if not self or not self.backBtn then
                                    return
                                  end
                                  if ret.code == 0 then
                                    local newItem = ret.data
                                    self:InitNodeInfo(newItem, self.m_FormerNode, UIWidget.Pivot.Top, false, false)
                                  end
                                end)
                              end
                            end), nil)
                        end), nil)
                    end)
                end
                com_i = com_i + 1
            end
        end
    end

    self:InitCommentList(savePos)
end

function LuaPersonalSpaceDetailView:InitNodeInfo(_info, formerNode, defaultPivot, showTalk, totalReNew)
  LuaPersonalSpaceMgrReal.UpdateShowInfoList(_info)
  self.saveDefaultPivot = defaultPivot
  self.SaveCommentList = {}

  local info = _info

  if _info.type == CPersonalSpace_CircleGenerateType.HotMoments then
    if not CommonDefs.DictContains(CPersonalSpaceMgr.Inst.hotMomentDic, typeof(UInt64), _info.id) then
      self:BackFunction(_info.type)
      return
    end
    info = CommonDefs.DictGetValue(CPersonalSpaceMgr.Inst.hotMomentDic, typeof(UInt64), _info.id).data
  elseif _info.type == CPersonalSpace_CircleGenerateType.AllHotMoments then
    if not CommonDefs.DictContains(CPersonalSpaceMgr.Inst.allhotMomentDic, typeof(UInt64), _info.id) then
      self:BackFunction(info.type)
      return
    end
    info = CommonDefs.DictGetValue(CPersonalSpaceMgr.Inst.allhotMomentDic, typeof(UInt64), _info.id).data
  else
    --if (CPersonalSpaceMgr.Inst.momentCacheDic.ContainsKey(_info.id))
    --{
    --    info = CPersonalSpaceMgr.Inst.momentCacheDic[_info.id].data;
    --}
    --else if (CPersonalSpaceMgr.Inst.otherMomentCacheDic.ContainsKey(_info.id))
    --{
    --    info = CPersonalSpaceMgr.Inst.otherMomentCacheDic[_info.id].data;
    --}
    --else
    --{
    --    //BackFunction(info.type);
    --    //return;
    --}

    info = _info
  end

  self.m_Info = info
  self.m_FormerNode = formerNode

  self.favorTemplateNode:SetActive(false)
  self.messageTemplateNode:SetActive(false)
  self.infoTemplateNode:SetActive(false)

  Extensions.RemoveAllChildren(self.table.transform)

  local node = NGUITools.AddChild(self.table.gameObject, self.infoTemplateNode)
  node:SetActive(true)
  self.sendStatusPanel_cancelBtn = node.transform:Find("subInfo/bar/node/SendStatus/cancelBtn").gameObject
  self.sendStatusPanel_statusText = node.transform:Find("subInfo/bar/node/SendStatus/statusInput/uiinput/statusText"):GetComponent(typeof(UILabel))
  self.sendStatusPanel_emotionBtn = node.transform:Find("subInfo/bar/node/SendStatus/emotionBtn").gameObject
  self.sendStatusPanel_statusInput = node.transform:Find("subInfo/bar/node/SendStatus/statusInput"):GetComponent(typeof(CChatInput))
  self.barNode = node.transform:Find("subInfo/bar/node")
  local oldNode = self.gameObject
  local statusLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("status"), typeof(UILabel))

  local subInfo = node.transform:Find("subInfo")

  --if totalReNew then
  if true then
    self:ResetInputInfo()

    self.formerPaneNode = formerNode
    self.formerPaneNode:SetActive(false)

    if System.String.IsNullOrEmpty(info.rolename) then
      CommonDefs.GetComponent_Component_Type(oldNode.transform:Find("InfoItemOld/name"), typeof(UILabel)).text = tostring(info.roleid)
    else
      CommonDefs.GetComponent_Component_Type(oldNode.transform:Find("InfoItemOld/name"), typeof(UILabel)).text = info.rolename
    end

    local vipNode = oldNode.transform:Find("InfoItemOld/name/vipicon")
    if vipNode ~= nil then
      CPersonalSpaceMgr.Inst:SetSpLevelNode(info.splevel, vipNode.gameObject)
    end

    if info.roleid == CIMMgr.PERSONAL_SPACE_HOT_POINT_ID then
      --node.transform.FindChild("icon").GetComponent<CUITexture>().LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender));
      CommonDefs.GetComponent_Component_Type(oldNode.transform:Find("InfoItemOld/lv"), typeof(UILabel)).text = ""
      CommonDefs.GetComponent_Component_Type(oldNode.transform:Find("InfoItemOld/icon"), typeof(CUITexture)):LoadNPCPortrait(CPersonalSpaceMgr.Inst:GetSpaceGMIcon(), false)
    elseif CPersonalSpaceMgr.CheckAndLoadGongZhongHaoPortraitPic(info.roleid, oldNode.transform:Find("InfoItemOld/icon").gameObject, CommonDefs.GetComponent_Component_Type(oldNode.transform:Find("InfoItemOld/lv"), typeof(UILabel))) then
    else
      --oldNode.transform.FindChild("InfoItemOld/lv").GetComponent<UILabel>().text = info.grade.ToString();
      local levelLabel = CommonDefs.GetComponent_Component_Type(oldNode.transform:Find("InfoItemOld/lv"), typeof(UILabel))
      local default
      if info.xianfanstatus > 0 then
        default = L10.Game.Constants.ColorOfFeiSheng
      else
        default = NGUIText.EncodeColor24(levelLabel.color)
      end
      levelLabel.text = System.String.Format(LocalString.GetString("[c][{0}]{1}[-][/c]"), default, "lv." .. info.grade)

      --if (!string.IsNullOrEmpty(info.clazz) && !string.IsNullOrEmpty(info.gender))
      --{
      --    uint cls = uint.Parse(info.clazz);
      --    uint gender = uint.Parse(info.gender);
      --    oldNode.transform.FindChild("InfoItemOld/icon").GetComponent<CUITexture>().LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender));
      --}
      LuaPersonalSpaceMgrReal.LoadPortraitPic(info.photo, info.clazz, info.gender, oldNode.transform:Find("InfoItemOld/icon").gameObject, info.expression_base)

      if info.roleid ~= CPersonalSpaceWnd.Instance.SelfRoleInfo.id then
        local imgArray = nil
        if info and info.imglist then
          Table2ArrayWithCount(info.imglist, #info.imglist, MakeArrayClass(String))
        else
          Table2ArrayWithCount({}, 0, MakeArrayClass(String))
        end
        CPersonalSpaceMgr.Inst:addPlayerLinkBtn(oldNode.transform:Find("InfoItemOld/icon/button").gameObject, info.roleid, EnumReportId_lua.eMengDaoMoment, info.text, imgArray, info.id, 0)
      end
    end

    local showText = string.gsub(string.gsub(info.text, "&lt;", "<"), "&gt;", ">")
    showText = showText .. LuaPersonalSpaceMgrReal.GenerateForwardString(info.previousforwards)
    showText = CChatMgr.Inst:FilterYangYangEmotion(showText)
    statusLabel.text = SafeStringFormat3('[c][FFFFFF]%s[-][/c]',CChatLinkMgr.TranslateToNGUIText(showText, false))

    UIEventListener.Get(statusLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
      local url = CommonDefs.GetComponent_GameObject_Type(statusLabel.gameObject, typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
      if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
        return
      end
    end)

    --subInfo.localPosition = new Vector3(subInfo.localPosition.x, -statusLabel.height, subInfo.localPosition.z);

    local yShift = - statusLabel.height

    --GameObject picNode1 = node.transform.FindChild("pic1").gameObject;
    --GameObject picNode2 = node.transform.FindChild("pic2").gameObject;
    --GameObject picNode3 = node.transform.FindChild("pic3").gameObject;
    local speNode = node.transform:Find("spepic").gameObject

    --GameObject[] picNodeArray = new GameObject[] { picNode1, picNode2, picNode3 };

    --picNode1.SetActive(false);
    --picNode2.SetActive(false);
    --picNode3.SetActive(false);
    local picNodeArray = CreateFromClass(MakeGenericClass(List, GameObject))
    for pic_i = 1, 6 do
      local picNode = node.transform:Find("pic" .. tostring(pic_i))
      if picNode ~= nil then
        picNode.gameObject:SetActive(false)
        CommonDefs.ListAdd(picNodeArray, typeof(GameObject), picNode.gameObject)
      end
    end

    speNode:SetActive(false)

    if info.speimage ~= nil and not System.String.IsNullOrEmpty(info.speimage.url) then
      speNode:SetActive(true)
      CPersonalSpaceMgr.DownLoadPic(info.speimage.url, CommonDefs.GetComponent_GameObject_Type(speNode, typeof(UITexture)), 160, 800, DelegateFactory.Action(function ()
        if self.backBtn == nil then
          return
        end
        UIEventListener.Get(speNode).onClick = DelegateFactory.VoidDelegate(function (p)
          CWebBrowserMgr.Inst:OpenUrl(info.speimage.jump_url)
        end)
      end), false, true)
      speNode.transform.localPosition = Vector3(speNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift, speNode.transform.localPosition.z)
      yShift = yShift - CPersonalSpaceWnd.DefaultPicHeight
    elseif info.videolist and #info.videolist > 0 then
      local videoNode = node.transform:Find('pic1').gameObject
      LuaPersonalSpaceMgrReal.SetVideoPlayPic(videoNode,100)
      local videoData = info.videolist[1]
      videoNode:SetActive(true)
      CPersonalSpaceMgr.DownLoadPic(videoData.snapshot, CommonDefs.GetComponent_GameObject_Type(videoNode, typeof(UITexture)), 160, 250, DelegateFactory.Action(function ()
        if self.backBtn == nil then
          return
        end
        UIEventListener.Get(videoNode).onClick = DelegateFactory.VoidDelegate(function (p)
          LuaPersonalSpaceMgrReal.MoviePlayUrl = videoData.url
          CUIManager.ShowUI(CLuaUIResources.DetailMovieShowWnd)
        end)
      end), true, true)
      videoNode.transform.localPosition = Vector3(videoNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift, videoNode.transform.localPosition.z)
      yShift = yShift - CPersonalSpaceWnd.DefaultPicHeight
    else
      if info.imglist and #info.imglist > 0 then
        do
          local i = 1
          while i <= #info.imglist do
            local imageInfo = info.imglist[i]
            local picNode = picNodeArray[i-1]
            picNode:SetActive(true)
            self:SetMomentPic(picNode, info.imglist, i)
            if i <= 3 then
              picNode.transform.localPosition = Vector3(picNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift, picNode.transform.localPosition.z)
            else
              picNode.transform.localPosition = Vector3(picNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift - CPersonalSpaceWnd.DefaultTwoRowPicHeight, picNode.transform.localPosition.z)
            end
            i = i + 1
          end
        end
        if #info.imglist <= 3 then
          yShift = yShift - CPersonalSpaceWnd.DefaultPicHeight
        else
          yShift = yShift - CPersonalSpaceWnd.DefaultPicHeight
          yShift = yShift - CPersonalSpaceWnd.DefaultTwoRowPicHeight
        end
      end
    end

    local forwardNode = node.transform:Find("forwardinfo").gameObject
    local forward_bgBtn = node.transform:Find("forwardinfo/forwardbg").gameObject
    if info.forwardmoment and tonumber(info.forwardmoment.id) and tonumber(info.forwardmoment.id) > 0 then
      forwardNode:SetActive(true)

      if System.String.IsNullOrEmpty(info.forwardmoment.rolename) then
        CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("name"), typeof(UILabel)).text = tostring(info.forwardmoment.roleid)
      else
        CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("name"), typeof(UILabel)).text = info.forwardmoment.rolename
      end

      local forwardVipNode = forwardNode.transform:Find("name/vipicon")
      if forwardVipNode ~= nil then
        CPersonalSpaceMgr.Inst:SetSpLevelNode(info.forwardmoment.splevel, forwardVipNode.gameObject)
      end

      if CClientMainPlayer.Inst:GetMyServerId() ~= info.forwardmoment.server_id and info.forwardmoment.server_id ~= 0 then
        CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("name"), typeof(UILabel)).text = CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("name"), typeof(UILabel)).text .. (CPersonalSpaceMgr.ServerSplitChar .. info.forwardmoment.server_name)
      end

      if info.forwardmoment.roleid == CIMMgr.PERSONAL_SPACE_HOT_POINT_ID then
        CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("lv"), typeof(UILabel)).text = ""
        CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CPersonalSpaceMgr.Inst:GetSpaceGMIcon(), false)
      elseif CPersonalSpaceMgr.CheckAndLoadGongZhongHaoPortraitPic(info.forwardmoment.roleid, forwardNode.transform:Find("icon").gameObject, CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("lv"), typeof(UILabel))) then
      else
        --forwardNode.transform.FindChild("lv").GetComponent<UILabel>().text = info.forwardmoment.grade.ToString();
        local levelLabel = CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("lv"), typeof(UILabel))
        local extern
        if info.forwardmoment.xianfanstatus > 0 then
          extern = L10.Game.Constants.ColorOfFeiSheng
        else
          extern = NGUIText.EncodeColor24(levelLabel.color)
        end
        levelLabel.text = System.String.Format(LocalString.GetString("[c][{0}]{1}[-][/c]"), extern, "lv." .. info.forwardmoment.grade)

        --if (!string.IsNullOrEmpty(info.forwardmoment.clazz) && !string.IsNullOrEmpty(info.forwardmoment.gender))
        --{
        --    uint cls = uint.Parse(info.forwardmoment.clazz);
        --    uint gender = uint.Parse(info.forwardmoment.gender);
        --    forwardNode.transform.FindChild("icon").GetComponent<CUITexture>().LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender));
        --}
        CPersonalSpaceMgr.LoadPortraitPic(info.forwardmoment.photo, info.forwardmoment.clazz, info.forwardmoment.gender, forwardNode.transform:Find("icon").gameObject, info.forwardmoment.expression_base)

        if info.forwardmoment.roleid ~= self.personalSpaceWnd.SelfRoleInfo.id then
          CPersonalSpaceMgr.Inst:addPlayerLinkBtn(forwardNode.transform:Find("icon/button").gameObject, info.forwardmoment.roleid, EnumReportId_lua.eMengDaoMoment, info.forwardmoment.text, info.forwardmoment.imglist, info.forwardmoment.id, 0)
        end
      end

      local forward_showText = string.gsub(string.gsub(info.forwardmoment.text, "&lt;", "<"), "&gt;", ">")
      forward_showText = CChatMgr.Inst:FilterYangYangEmotion(forward_showText)
      local forwardLabel = CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("status"), typeof(UILabel))
      --forwardLabel.text = CChatLinkMgr.TranslateToNGUIText(forward_showText);
      CPersonalSpaceMgr.SetTextMore(forward_showText, forwardLabel)

      UIEventListener.Get(forwardLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        local url = CommonDefs.GetComponent_GameObject_Type(statusLabel.gameObject, typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
        if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
          return
        else
          self.personalSpaceWnd:ShowMomentDetailThroughFriendCircleById(info.forwardmoment.id, false, formerNode)
        end
      end)

      --GameObject forward_picNode1 = forwardNode.transform.FindChild("pic1").gameObject;
      --GameObject forward_picNode2 = forwardNode.transform.FindChild("pic2").gameObject;
      --GameObject forward_picNode3 = forwardNode.transform.FindChild("pic3").gameObject;
      local forward_speNode = forwardNode.transform:Find("spepic").gameObject

      --GameObject[] forward_picNodeArray = new GameObject[] { forward_picNode1, forward_picNode2, forward_picNode3 };
      --forward_picNode1.SetActive(false);
      --forward_picNode2.SetActive(false);
      --forward_picNode3.SetActive(false);
      forward_speNode:SetActive(false)

      local forward_picNodeArray = CreateFromClass(MakeGenericClass(List, GameObject))
      for pic_i = 1, 6 do
        local picNode = forwardNode.transform:Find("pic" .. tostring(pic_i))
        if picNode ~= nil then
          picNode.gameObject:SetActive(false)
          CommonDefs.ListAdd(forward_picNodeArray, typeof(GameObject), picNode.gameObject)
        end
      end

      forward_bgBtn:SetActive(true)

      UIEventListener.Get(forward_bgBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self.personalSpaceWnd:ShowMomentDetailThroughFriendCircleById(info.forwardmoment.id, false, formerNode)
      end)

      forwardNode.transform.localPosition = Vector3(forwardNode.transform.localPosition.x, self.defaultPicHeight + yShift, forwardNode.transform.localPosition.z)
      --forward_bgBtn.transform.localPosition = forwardNode.transform.localPosition;

      local newHeight = 0
      local newHeight1 = 0

      if info.forwardmoment.speimage ~= nil and not System.String.IsNullOrEmpty(info.forwardmoment.speimage.url) then
        forward_speNode:SetActive(true)
        CPersonalSpaceMgr.DownLoadPic(info.forwardmoment.speimage.url, CommonDefs.GetComponent_GameObject_Type(forward_speNode, typeof(UITexture)), 160, 800, DelegateFactory.Action(function ()
          if self.backBtn == nil then
            return
          end
          UIEventListener.Get(forward_speNode).onClick = DelegateFactory.VoidDelegate(function (p)
            CWebBrowserMgr.Inst:OpenUrl(info.forwardmoment.speimage.jump_url)
          end)
        end), false, true)

        --if self.maxInfoHeight <= CPersonalSpaceMgr.ForwardInfoHeight2 - yShift then
        --    newHeight = self.maxInfoHeight + yShift
        --else
        newHeight = CPersonalSpaceMgr.ForwardInfoHeight2
        --end
        newHeight1 = CPersonalSpaceMgr.ForwardInfoHeight2
      else
        if info.forwardmoment.imglist and #info.forwardmoment.imglist > 0 then
          do
            local i = 1
            while i <= #info.forwardmoment.imglist do
              local imageInfo = info.forwardmoment.imglist[i]
              local picNode = forward_picNodeArray[i-1]
              picNode:SetActive(true)
              self.personalSpaceWnd:SetMomentPic(picNode, info.forwardmoment.imglist, i)
              i = i + 1
            end
          end
          if #info.forwardmoment.imglist <= 3 then
            --if self.maxInfoHeight <= CPersonalSpaceMgr.ForwardInfoHeight2 - yShift then
            --    newHeight = self.maxInfoHeight + yShift
            --else
            newHeight = CPersonalSpaceMgr.ForwardInfoHeight2
            --end

            newHeight1 = CPersonalSpaceMgr.ForwardInfoHeight2
          else
            --if self.maxInfoHeight <= CPersonalSpaceMgr.ForwardInfoHeight2 - yShift + CPersonalSpaceWnd.DefaultTwoRowPicHeight then
            --    newHeight = self.maxInfoHeight + yShift
            --else
            newHeight = CPersonalSpaceMgr.ForwardInfoHeight2 + CPersonalSpaceWnd.DefaultTwoRowPicHeight
            --end

            newHeight1 = CPersonalSpaceMgr.ForwardInfoHeight2 + CPersonalSpaceWnd.DefaultTwoRowPicHeight
          end
        else
          --if self.maxInfoHeight <= CPersonalSpaceMgr.ForwardInfoHeight1 - yShift then
          --    newHeight = self.maxInfoHeight + yShift
          --else
          newHeight = CPersonalSpaceMgr.ForwardInfoHeight1
          --end
          newHeight1 = CPersonalSpaceMgr.ForwardInfoHeight1
        end
      end

      if newHeight <= 0 then
        forward_bgBtn:SetActive(false)
        forwardNode:SetActive(false)
      else
        CommonDefs.GetComponent_GameObject_Type(forward_bgBtn, typeof(UISprite)).height = newHeight1
        yShift = yShift - newHeight
      end

      CommonDefs.GetComponent_GameObject_Type(forward_bgBtn, typeof(UISprite)):ResizeCollider()
    else
      forwardNode:SetActive(false)
      forward_bgBtn:SetActive(false)
    end

    --CommonDefs.GetComponent_Component_Type(oldNode.transform:Find("containbg"), typeof(UISprite)).height = - yShift + 15
    --CommonDefs.GetComponent_Component_Type(oldNode.transform:Find("contain"), typeof(UIPanel)):UpdateAnchors()



    subInfo.localPosition = Vector3(subInfo.localPosition.x, yShift - 260, subInfo.localPosition.z)

    local scrollViewPanel = CommonDefs.GetComponent_GameObject_Type(self.scrollView.gameObject, typeof(UIPanel))
    --scrollViewPanel.SetRect(scrollViewPanel.transform.position.x, scrollViewPanel.transform.position.y + statusLabel.height / 2, scrollViewPanel.width, defaultTableHeight - statusLabel.height);
    --local commentHeight = self.defaultTableHeight + yShift
    --if commentHeight < 500 then
    --  commentHeight = 500
    --end

    --CommonDefs.GetComponent_Component_Type(subInfo:Find("bg"), typeof(UISprite)).height = commentHeight

    scrollViewPanel:ResetAndUpdateAnchors()
    self.scrollViewIndicator:Layout()

    --///input

    local defaultReplyName = info.rolename
    if System.String.IsNullOrEmpty(defaultReplyName) then
      defaultReplyName = tostring(info.roleid)
    end
    self.sendStatusPanel_statusText.text = CPersonalSpaceDetailMoment.ReplyPrefix .. defaultReplyName
    self.sendStatusPanel_statusInput.input.defaultText = CPersonalSpaceDetailMoment.ReplyPrefix .. defaultReplyName

    if showTalk then
      self:ShowInputNode(subInfo.gameObject, info)
    end

    UIEventListener.Get(subInfo:Find("bar/node/leavemessagebutton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
      self:ShowInputNode(subInfo.gameObject, info)
    end)

    UIEventListener.Get(self.sendStatusPanel_cancelBtn).onClick = DelegateFactory.VoidDelegate(function (p)
      CommonDefs.GetComponent_Component_Type(subInfo:Find("bar/node"), typeof(TweenPosition)):PlayReverse()
    end)

    if not self.m_DefaultChatInputListener then
      self.m_DefaultChatInputListener = LuaPersonalSpaceMgrReal.GetChatListener(self.sendStatusPanel_statusInput)
    end

    self.sendStatusPanel_statusInput.OnSend = DelegateFactory.Action_string(function (msg)
      self:OnSubmitBtnClick(msg)
    end)

    CommonDefs.AddOnClickListener(self.sendStatusPanel_emotionBtn, DelegateFactory.Action_GameObject(function(go) self:OnEmotionButtonClick(go) end), false)
  end


  local infoTime = info.createtime / 1000
  local nowTime = CServerTimeMgr.Inst.timeStamp
  local disTime = nowTime - infoTime
  if disTime < 3600 then
    CommonDefs.GetComponent_Component_Type(subInfo:Find("bar/node/time"), typeof(UILabel)).text = math.ceil(disTime / 60) .. LocalString.GetString("分钟前")
  elseif disTime < 86400 --[[24 * 3600]] then
    CommonDefs.GetComponent_Component_Type(subInfo:Find("bar/node/time"), typeof(UILabel)).text = math.ceil(disTime / 3600) .. LocalString.GetString("小时前")
  else
    local infoTimeDate = CServerTimeMgr.ConvertTimeStampToZone8Time(infoTime)
    CommonDefs.GetComponent_Component_Type(subInfo:Find("bar/node/time"), typeof(UILabel)).text = (((infoTimeDate.Year .. "-") .. infoTimeDate.Month) .. "-") .. infoTimeDate.Day
  end

  --Extensions.RemoveAllChildren(self.table.transform)
  --//favor
  local favorIcon = subInfo:Find("bar/node/favorbutton/favoricon").gameObject
  local favorNumLabel = CommonDefs.GetComponent_Component_Type(subInfo:Find("bar/node/favorbutton/favornum"), typeof(UILabel))
  favorIcon:SetActive(false)
  local favorText = ""

  if info.zanlist ~= nil and info.zancount > 0 then
    if info.is_user_liked then
      favorIcon:SetActive(true)
    else
      favorIcon:SetActive(false)
    end

    favorNumLabel.text = tostring(info.zancount)
    local favorNode = NGUITools.AddChild(self.table.gameObject, self.favorTemplateNode)
    favorNode:SetActive(true)
    CommonDefs.GetComponent_Component_Type(favorNode.transform:Find("text"), typeof(UILabel)).text = ""
    do
      local zan_i = 1
      while zan_i <= #info.zanlist do
        local zanInfo = info.zanlist[zan_i]
        local name = zanInfo.rolename
        if System.String.IsNullOrEmpty(name) then
          name = tostring(zanInfo.roleid)
        end
        favorText = favorText .. CPersonalSpaceMgr.GeneratePlayerName(zanInfo.roleid, name)
        zan_i = zan_i + 1
      end
    end
    local textNodeLabel = CommonDefs.GetComponent_Component_Type(favorNode.transform:Find("text"), typeof(UILabel))

    textNodeLabel.text = CChatLinkMgr.TranslateToNGUIText(favorText, false)
    UIEventListener.Get(textNodeLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
      local index = textNodeLabel:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
      if index == 0 then
        index = - 1
      end

      local url = textNodeLabel:GetUrlAtCharacterIndex(index)

      if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
        return
      end
    end)

    --self:InitCommentList(false)
  else
    favorNumLabel.text = "0"
  end


  local showMoreBtn = self.scrollViewIndicator.transform:Find("ShowNode").gameObject
  self:SetShowMoreBtn(showMoreBtn, info)

  --comment
  local leaveMessageNumText = CommonDefs.GetComponent_Component_Type(subInfo:Find("bar/node/leavemessagebutton/leavemessagenum"), typeof(UILabel))
  if info.commentlist ~= nil and #info.commentlist > 0 then
    leaveMessageNumText.text = tostring(info.commentcount)
    self:InitCommentListNode(info.commentlist, self.table.gameObject, info, false, false)
    if #info.commentlist >= CPersonalSpaceDetailMoment.MaxEveryGetNum then
      self.scrollViewIndicator.enableShowNode = true
    else
      self.scrollViewIndicator.enableShowNode = false
    end
  else
    self.scrollViewIndicator.enableShowNode = false
    leaveMessageNumText.text = "0"
  end

  local forwardBtn = subInfo:Find("bar/node/forwardbutton").gameObject
  local forwardNum = CommonDefs.GetComponent_Component_Type(subInfo:Find("bar/node/forwardbutton/num"), typeof(UILabel))
  forwardNum.text = tostring(info.forwardcount)
  UIEventListener.Get(forwardBtn).onClick = DelegateFactory.VoidDelegate(function (p)
    if info.forwardmoment and info.forwardmoment.id and info.forwardmoment.id > 0 and info.forwardmoment.status and info.forwardmoment.status == - 1 then
      g_MessageMgr:ShowMessage("PersonalSpaceCannotForward")
    else
      --CPersonalSpaceMgr.Inst.saveForwardInfo = info
      LuaPersonalSpaceMgrReal.forwardInfo = info
      CUIManager.ShowUI(CLuaUIResources.PersonalSpaceForwardLuaWnd)
    end
  end)

  local favorbuttonSign = subInfo:Find("bar/node/favorbutton/favoricon/UI_aixin").gameObject
  UIEventListener.Get(subInfo:Find("bar/node/favorbutton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
    CPersonalSpaceMgr.Inst:LikeMoment(info.id, favorIcon.activeSelf, DelegateFactory.Action_CPersonalSpace_BaseRet(function (data)
      if self.backBtn == nil then
        return
      end
      if data.code == 0 then
        LuaPersonalSpaceMgrReal.GetSingleMomentInfo(info.id, function (ret)
          if not self or not self.backBtn then
            return
          end
          if ret.code == 0 then
            local newItem = ret.data
            --newItem.type = generateType
            if not favorIcon.activeSelf and newItem.is_user_liked then
              newItem.showFavorSign = true
            else
              newItem.showFavorSign = false
            end
            info.showFavorSign = newItem.showFavorSign
            info.zancount = newItem.zancount
            info.is_user_liked = newItem.is_user_liked

            self:InitNodeInfo(newItem, formerNode, UIWidget.Pivot.Top, false, false)
          end
        end)
      end
    end), DelegateFactory.Action(function ()
    end))
  end)

  self.table:Reposition()
  self.scrollView:ResetPosition()
end

function LuaPersonalSpaceDetailView:OnEmotionButtonClick(go)
  CChatInputMgr.ShowChatInputWnd(CChatInputMgrEParentType.PersonalSpace, self.m_DefaultChatInputListener, go, EChatPanel.Undefined, 0, 0)
end

function LuaPersonalSpaceDetailView:OnSubmitBtnClick(msg)
  if not CClientMainPlayer.Inst then
    return
  end

  if System.String.IsNullOrEmpty(msg) then
    g_MessageMgr:ShowMessage("ENTER_TEXT_TIP")
    return
  end
  if System.String.IsNullOrEmpty(msg) then
    self:ResetInputInfo()
    return
  end
  if Time.realtimeSinceStartup - self.lastSendTime < 1 then
    return
  end
  self.lastSendTime = Time.realtimeSinceStartup
  local sendText = msg --CChatInputLink.Encapsulate(msg, self.existingLinks)
  --sendText = CChatMgr.Inst:FilterYangYangEmotion(sendText)
  --local roleInfo = CPersonalSpaceMgr.Inst:GetRoleInfo()
  --if (ReplyPersonId == ulong.Parse(roleInfo.id))
  --   ReplyPersonId = 0;
  local replyId = self.ReplyPersonId
  local replyName = self.ReplyPersonName

  CPersonalSpaceMgr.Inst:AddComment(self.m_Info.id, sendText, self.ReplyPersonId, DelegateFactory.Action_string_CPersonalSpace_AdvRet(function (textAfterFilter, data)
    if not self or not self.backBtn then
      return
    end
    if data.code == 0 then
      if self.gameObject.activeSelf then
        LuaPersonalSpaceMgrReal.GetSingleMomentInfo(self.m_Info.id, function (ret)
          if not self or not self.backBtn then
            return
          end
          if ret.code == 0 then
            if replyId ~= 0 then
              self:SetInputDefaultValue(replyId, replyName, self.gameObject, self.m_Info)
            end
            local newItem = ret.data
            self:InitNodeInfo(newItem, self.m_FormerNode, UIWidget.Pivot.Top, false, false)
          end
        end)
      end
    end
  end), nil)
  self:ResetInputInfo()
end

function LuaPersonalSpaceDetailView:SetMomentPic(node, infoArray, index)
  local info = infoArray[index]

  if System.String.IsNullOrEmpty(info.thumb) then
    return
  end

  --TODO check cache
  LuaPersonalSpaceMgrReal.CleanBtn(node)

  --CPersonalSpaceMgr.Inst.DownLoadPortraitPic(info.thumb, node.GetComponent<UITexture>(), delegate(Texture texture)
  --{

  --});

  CPersonalSpaceMgr.DownLoadPic(info.thumb, CommonDefs.GetComponent_GameObject_Type(node, typeof(UITexture)), CPersonalSpaceMgr.MaxSmallPicHeight, CPersonalSpaceMgr.MaxSmallPicWidth, DelegateFactory.Action(function ()
    if not self or not self.scrollView then
      return
    end
    UIEventListener.Get(node).onClick = DelegateFactory.VoidDelegate(function (p)
      --self:InitDetailPicPanel(infoArray, index, nil)
      LuaPersonalSpaceMgrReal.InitDetailPicPanel(infoArray, index)
    end)
  end), false, true)
end

function LuaPersonalSpaceDetailView:InitCommentList(savePosSign)

  self.scrollView.contentPivot = UIWidget.Pivot.Top

  if math.floor(self.scrollView.bounds.size.y - self.scrollView.panel.baseClipRegion.w + 0.5) > 0 then
    if self.saveDefaultPivot then
      self.scrollView.contentPivot = self.saveDefaultPivot
    end
  end

  local savePos = self.scrollView.transform.localPosition
  local saveOffsetY = self.panel.clipOffset.y
  self.table:Reposition()
  --self.table.gameObject:SetActive(false)
  self.scrollView:ResetPosition()

  if savePosSign then
    self.scrollView.transform.localPosition = savePos
    self.panel.clipOffset = Vector2(self.panel.clipOffset.x, saveOffsetY)
  end
end

function LuaPersonalSpaceDetailView:SetShowMoreBtn(showMoreBtn,info)
  UIEventListener.Get(showMoreBtn).onClick = DelegateFactory.VoidDelegate(function (p)
    showMoreBtn:SetActive(false)
    local lastId = 0
    if self.SaveCommentList and #self.SaveCommentList > 0 then
      local cstruct = self.SaveCommentList[#self.SaveCommentList]
      lastId = cstruct.id
    end
    LuaPersonalSpaceMgrReal.GetComments(lastId, function (data)
      if not self or not self.backBtn then
        return
      end
      if data and data.data then
        if #data.data > 0 then
          if #data.data < self.MaxEveryGetNum then
            self.scrollViewIndicator.enableShowNode = false
          end

          self:InitCommentListNode(data.data, self.table.gameObject, info, false, true)
        else
          self.scrollViewIndicator.enableShowNode = false
        end
      else
        self.scrollViewIndicator.enableShowNode = false
        --TODO show no more
      end
    end)
  end)
end

function LuaPersonalSpaceDetailView:OnEnable()
  --g_ScriptEvent:AddListener("UpdateTaoQuanInfo", self, "UpdateInfo")
end

function LuaPersonalSpaceDetailView:OnDisable()
  --g_ScriptEvent:RemoveListener("UpdateTaoQuanInfo", self, "UpdateInfo")
end


function LuaPersonalSpaceDetailView:OnDestroy()

end

return LuaPersonalSpaceDetailView
