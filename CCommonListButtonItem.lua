-- Auto Generated!!
local CCommonListButtonItem = import "CCommonListButtonItem"
local Object = import "System.Object"
local  CButton = import "L10.UI.CButton"
CCommonListButtonItem.m_OnClick_CS2LuaHook = function (this) 
    if this.clickAction ~= nil then
        GenericDelegateInvoke(this.clickAction, Table2ArrayWithCount({this.Index}, 1, MakeArrayClass(Object)))
    end
end
CCommonListButtonItem.m_SetHighLight_CS2LuaHook = function (this, flag)
	local button = this.gameObject:GetComponent(typeof(CButton))
	if button then
		button.Selected = flag
        return
	end
end
