local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
CLuaLianLianKanQuestionWnd=class()
RegistClassMember(CLuaLianLianKanQuestionWnd,"m_LeftTime")
RegistClassMember(CLuaLianLianKanQuestionWnd,"m_Tick")
RegistClassMember(CLuaLianLianKanQuestionWnd,"m_CountdownLabel")

function CLuaLianLianKanQuestionWnd:Init()
    local titleLabel=FindChild(self.transform,"TitleLabel"):GetComponent(typeof(UILabel))
    self.m_CountdownLabel=FindChild(self.transform,"CountdownLabel"):GetComponent(typeof(UILabel))
    local choiceLabel1=FindChild(self.transform,"ChoiceLabel1"):GetComponent(typeof(UILabel))
    local choiceLabel2=FindChild(self.transform,"ChoiceLabel2"):GetComponent(typeof(UILabel))

    local choice1=FindChild(self.transform,"Choice1").gameObject
    local choice2=FindChild(self.transform,"Choice2").gameObject

    titleLabel.text = CLuaLianLianKanMgr.m_Question
    local myId=CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local time=CServerTimeMgr.Inst:GetZone8Time().Ticks
    math.randomseed(myId+time)
    local r=math.random()


    local normal = r>0.5
    if normal then
        choiceLabel1.text = CLuaLianLianKanMgr.m_Answer1
        choiceLabel2.text = CLuaLianLianKanMgr.m_Answer2
    else
        choiceLabel1.text = CLuaLianLianKanMgr.m_Answer2
        choiceLabel2.text = CLuaLianLianKanMgr.m_Answer1
    end

    UIEventListener.Get(choice1).onClick=DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.LianLianKanPickAnswer(normal and 1 or 2)
        CUIManager.CloseUI("LianLianKanQuestionWnd")
    end)
    UIEventListener.Get(choice2).onClick=DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.LianLianKanPickAnswer(normal and 2 or 1)
        CUIManager.CloseUI("LianLianKanQuestionWnd")
    end)
    self:StartCountdown()
end

function CLuaLianLianKanQuestionWnd:StartCountdown()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
    end
    -- self.m_LeftTime=leftTime
    self.m_LeftTime=3
    self.m_Tick = RegisterTickWithDuration(function ()
        self.m_LeftTime = self.m_LeftTime - 1
        self.m_CountdownLabel.text = SafeStringFormat3(LocalString.GetString("%d秒后自动关闭"),self.m_LeftTime)
        if self.m_LeftTime<=0 then
            UnRegisterTick(self.m_Tick)
            self.m_Tick=nil
        end
    end,1000,3000)
end

function CLuaLianLianKanQuestionWnd:OnDestroy()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
    end
end