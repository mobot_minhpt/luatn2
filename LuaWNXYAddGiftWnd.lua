local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local GameObject = import "UnityEngine.GameObject"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local UILongPressButton = import "L10.UI.UILongPressButton"

LuaWNXYAddGiftWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaWNXYAddGiftWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaWNXYAddGiftWnd, "ClearGiftBtn", "ClearGiftBtn", GameObject)
RegistChildComponent(LuaWNXYAddGiftWnd, "AddGiftBtn", "AddGiftBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaWNXYAddGiftWnd,"m_CardGifts")
RegistClassMember(LuaWNXYAddGiftWnd,"m_ItemId2CardGiftList")
RegistClassMember(LuaWNXYAddGiftWnd,"m_SelectGiftItem")
function LuaWNXYAddGiftWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.ClearGiftBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnClearGiftBtnClick()
	end)


	
	UIEventListener.Get(self.AddGiftBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddGiftBtnClick()
	end)


    --@endregion EventBind end
end

function LuaWNXYAddGiftWnd:Init()
	self.m_CardGifts = {}
	self.m_ItemId2CardGiftList = {}
	local setting = Christmas2021_Setting.GetData()
	local gifts = setting.CardGift
	for i=0,gifts.Length-3,3 do
		local gift = {}
		gift.ItemId = gifts[i]
		gift.YinLiang = gifts[i+1]
		gift.LingYu = gifts[i+2]
		gift.CostType = gift.YinLiang == 0 and 2 or 1
		self.m_CardGifts[gift.ItemId] = gift
		table.insert(self.m_CardGifts,gift)
		self.m_ItemId2CardGiftList[gift.ItemId] = gift
	end

	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_CardGifts
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

	self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self.m_SelectGiftItem = self.m_CardGifts[row+1].ItemId
    end)

	self.TableView:ReloadData(false,false)
end

function LuaWNXYAddGiftWnd:InitItem(item,row)
	local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local costLabel = item.transform:Find("Cost/CostLabel"):GetComponent(typeof(UILabel))
	local costSprite = item.transform:Find("Cost"):GetComponent(typeof(UISprite))
	local numberInput = item.transform:Find("NumberInput"):GetComponent(typeof(QnAddSubAndInputButton))
	local longPressCmp = item:GetComponent(typeof(UILongPressButton))

	local gift = self.m_CardGifts[row+1]
	local item = Item_Item.GetData(gift.ItemId)
	if item then
		icon:LoadMaterial(item.Icon)
		nameLabel.text = item.Name
	end

	local spriteName
	local singleCost = 0
	local giftCount = 0
	if gift.CostType == 1 then--银两
		spriteName = "packagewnd_yinliang"
		singleCost = gift.YinLiang
	elseif gift.CostType == 2 then--灵玉
		spriteName = "packagewnd_lingyu"
		singleCost = gift.LingYu
	end
	costSprite.spriteName = spriteName
	costLabel.text = singleCost

	numberInput:SetValue(1, true)
	local maxCount = 30
    numberInput:SetMinMax(1, maxCount, 1)

	gift.Count = 1

	numberInput.onValueChanged = DelegateFactory.Action_uint(function (value) 
		gift.Count = tonumber(value)
		self.TableView:SetSelectRow(row,true)
	end)
	if row == 0 then
		numberInput:SetValue(1, true)
	end

	longPressCmp.OnLongPressDelegate = DelegateFactory.Action(function()
        CItemInfoMgr.ShowLinkItemTemplateInfo(gift.ItemId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
    end)
end


--@region UIEvent

function LuaWNXYAddGiftWnd:OnClearGiftBtnClick()
	local gift = nil
	LuaChristmas2021Mgr.CurGiftItemId = nil
	g_ScriptEvent:BroadcastInLua("RefreshGiveCardGift", gift)
	CUIManager.CloseUI(CLuaUIResources.WNXYAddGiftWnd)
end

function LuaWNXYAddGiftWnd:OnAddGiftBtnClick()
	if not self.m_SelectGiftItem then
		g_MessageMgr:ShowMessage("Christmas2021_WNXY_Please_SelectGift")
		return 
	end
	LuaChristmas2021Mgr.CurGiftItemId = self.m_SelectGiftItem
	local gift = self.m_ItemId2CardGiftList[LuaChristmas2021Mgr.CurGiftItemId]
	if gift.Count <= 0 then
		g_MessageMgr:ShowMessage("Christmas2021_WNXY_AddGiftCount")
		return
	end
	g_ScriptEvent:BroadcastInLua("RefreshGiveCardGift",gift)
	CUIManager.CloseUI(CLuaUIResources.WNXYAddGiftWnd)
end

--@endregion UIEvent

