require("3rdParty/ScriptEvent")
require("common/common_include")

local Random = import "UnityEngine.Random"
local Vector3 = import "UnityEngine.Vector3"
local Time = import "UnityEngine.Time"
local Gac2Gas = import "L10.Game.Gac2Gas"
local CommonDefs = import "L10.Game.CommonDefs"
local HuluBrothers_XiaoWanFa = import "L10.Game.HuluBrothers_XiaoWanFa"
local HuluBrothers_Setting = import "L10.Game.HuluBrothers_Setting"
local CMainCamera = import "L10.Engine.CMainCamera"
local CHuluBrothersZhaoHuanData = import "L10.UI.CHuluBrothersZhaoHuanData"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local EDamageText = import "L10.Game.EDamageText"

CLuaHuluwaDropEffect =class()
RegistClassMember(CLuaHuluwaDropEffect, "m_Tex")

RegistClassMember(CLuaHuluwaDropEffect, "m_DropTimes")
RegistClassMember(CLuaHuluwaDropEffect, "m_XiaoWanFaId")
RegistClassMember(CLuaHuluwaDropEffect, "m_DropSpeed")
RegistClassMember(CLuaHuluwaDropEffect, "m_RotateSpeed")
RegistClassMember(CLuaHuluwaDropEffect, "m_LifeTime")

RegistClassMember(CLuaHuluwaDropEffect, "m_bClicked")
RegistClassMember(CLuaHuluwaDropEffect, "m_MoveTarget")
RegistClassMember(CLuaHuluwaDropEffect, "m_MoveTargetSpeed")
RegistClassMember(CLuaHuluwaDropEffect, "m_XiShouFxId")

function CLuaHuluwaDropEffect:Awake()
	self.m_LifeTime = 12
    self.m_DropSpeed = Random.Range(1, 1.5) * HuluBrothers_Setting.GetData().FallDownSpeed
    self.m_RotateSpeed = Random.Range(60, 270)
    self.m_bClicked = false
    self.m_MoveTarget = Vector3.zero
    self.m_MoveTargetSpeed = 1000
    self.m_XiShouFxId = 88800353

    local onClick = function(go)
    	self.m_bClicked = true
    	local targetNpc =  CClientObjectMgr.Inst:GetObject(CHuluBrothersZhaoHuanData.Inst.NpcEngineId)
    	if targetNpc then
    		local pos = targetNpc.WorldPos
    		pos.y = pos.y + 2.5
	    	local pos = CMainCamera.Inst.Main:WorldToScreenPoint(pos)
	    	local pos = CUIManager.UIMainCamera:ScreenToWorldPoint(pos)
			pos = self.transform.parent:InverseTransformPoint(pos)
			pos.z = 0
			self.m_MoveTarget = pos
		end
	end
	CommonDefs.AddOnClickListener(self.gameObject, DelegateFactory.Action_GameObject(onClick), false)
end

function CLuaHuluwaDropEffect:InitXiaoWanFaId(dropTimes, id)
	self.m_XiaoWanFaId = id
	self.m_DropTimes = dropTimes
	local designData = HuluBrothers_XiaoWanFa.GetData(self.m_XiaoWanFaId)
	if designData then
		self.m_Tex:LoadMaterial(designData.Icon)
	end
end

function CLuaHuluwaDropEffect:Update()
	if not self.m_bClicked then
		self.transform.localPosition = Vector3(self.transform.localPosition.x, self.transform.localPosition.y - self.m_DropSpeed * Time.deltaTime, 0);
		self.transform.localEulerAngles = Vector3(0, 0, self.transform.localEulerAngles.z + self.m_RotateSpeed * Time.deltaTime)
		self.m_LifeTime = self.m_LifeTime - Time.deltaTime
		if self.m_LifeTime < 0 then
			GameObject.Destroy(self.gameObject)
		end
	else
		self.transform.localEulerAngles = Vector3(0, 0, self.transform.localEulerAngles.z + self.m_RotateSpeed * Time.deltaTime)
        self.transform.localPosition = Vector3.MoveTowards(self.transform.localPosition, self.m_MoveTarget, self.m_MoveTargetSpeed * Time.deltaTime)
		if Vector3.Distance(self.transform.localPosition,  self.m_MoveTarget) < 0.05 then
			self:ProcessGetHuluDrop()
		end
	end
end
function CLuaHuluwaDropEffect:ProcessGetHuluDrop()
	--销毁掉落物
	GameObject.Destroy(self.gameObject)
	--给葫芦加飘字
	local targetNpc =  CClientObjectMgr.Inst:GetObject(CHuluBrothersZhaoHuanData.Inst.NpcEngineId)
	if targetNpc then
		--添加吸收时的特效
		CEffectMgr.Inst:AddObjectFX(self.m_XiShouFxId, targetNpc.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
		
		local designData = HuluBrothers_XiaoWanFa.GetData(self.m_XiaoWanFaId)
		if designData then
			if designData.Action == "AddScore" then
				targetNpc.RO:ShowDamageText(designData.Effect, EDamageText.Hp, false)
			end
			if designData.Id == 2 then -- 闪电
				local shanDianFxId = HuluBrothers_Setting.GetData().Shandian_FX
				CEffectMgr.Inst:AddObjectFX(shanDianFxId, targetNpc.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
			end
		end
	end
	--通知服务器
	Gac2Gas.RequestGetHuLuDrop(self.m_DropTimes, self.m_XiaoWanFaId)
end
return CLuaHuluwaDropEffect
