local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local Animation = import "UnityEngine.Animation"
LuaDaFuWongWorldEventWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongWorldEventWnd, "Title", "Title", UILabel)
RegistChildComponent(LuaDaFuWongWorldEventWnd, "Desc", "Desc", UILabel)
RegistChildComponent(LuaDaFuWongWorldEventWnd, "Tip", "Tip", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongWorldEventWnd, "m_anim")
RegistClassMember(LuaDaFuWongWorldEventWnd, "m_aniTick")
RegistClassMember(LuaDaFuWongWorldEventWnd, "m_closeBtn")
RegistClassMember(LuaDaFuWongWorldEventWnd, "m_PlayEnd")
function LuaDaFuWongWorldEventWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_anim = self.gameObject:GetComponent(typeof(Animation))
    self.m_closeBtn = self.transform:Find("CloseBtn")
    UIEventListener.Get(self.m_closeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:PlayEnd()
	end)
    self.m_aniTick = nil
    self.m_PlayEnd = false
end

function LuaDaFuWongWorldEventWnd:Init()
    self:InitInfo()
    SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_World_event", Vector3.zero, nil, 0)
    local countDown = DaFuWeng_Countdown.GetData(EnumDaFuWengStage.RoundWorldEvent).Time
	local endFunc = function() self:PlayEnd() end
	local durationFunc = function(time) end
	LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.WorldEvent,countDown,durationFunc,endFunc)
end
function LuaDaFuWongWorldEventWnd:PlayEnd()
    -- if self.m_PlayEnd then return end
    -- if self.m_anim then
    --     self.m_anim:Play("dafuweng_tanchaung_close")
    --     if self.m_aniTick then
    --         UnRegisterTick(self.m_aniTick)
    --         self.m_aniTick = nil
    --     end
    --     self.m_PlayEnd = true
    --     self.m_aniTick = RegisterTickOnce(function()
    --         CUIManager.CloseUI(CLuaUIResources.DaFuWongWorldEventWnd)
    --     end, self.m_anim.clip.length * 1000)
    -- end
    CUIManager.CloseUI(CLuaUIResources.DaFuWongWorldEventWnd)
end
function LuaDaFuWongWorldEventWnd:InitInfo()
    if LuaDaFuWongMgr.EventInfo then
        self.Title.text = LuaDaFuWongMgr.EventInfo.title
        self.Desc.text = LuaDaFuWongMgr.EventInfo.text
        self.Tip.text = LuaDaFuWongMgr.EventInfo.effect
    end
end

function LuaDaFuWongWorldEventWnd:OnStageUpdate(CurStage)
    if CurStage ~= EnumDaFuWengStage.RoundWorldEvent then
        CUIManager.CloseUI(CLuaUIResources.DaFuWongWorldEventWnd)
    end
end

--@region UIEvent

--@endregion UIEvent
function LuaDaFuWongWorldEventWnd:OnDisable()
    if self.m_aniTick then
        UnRegisterTick(self.m_aniTick)
        self.m_aniTick = nil
    end
    LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.WorldEvent)
    g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
    
end
function LuaDaFuWongWorldEventWnd:OnEnable()
    g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
end