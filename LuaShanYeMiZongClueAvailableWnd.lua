local CButton = import "L10.UI.CButton"
local CUITexture = import "L10.UI.CUITexture"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaShanYeMiZongClueAvailableWnd = class()
LuaShanYeMiZongClueAvailableWnd.s_ClueId = nil

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShanYeMiZongClueAvailableWnd, "ApplyButton", "ApplyButton", CButton)
RegistChildComponent(LuaShanYeMiZongClueAvailableWnd, "IconTexture", "IconTexture", CUITexture)

--@endregion RegistChildComponent end

function LuaShanYeMiZongClueAvailableWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ApplyButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnApplyButtonClick()
	end)


    --@endregion EventBind end
end

function LuaShanYeMiZongClueAvailableWnd:Init()
    if LuaShanYeMiZongClueAvailableWnd.s_ClueId then
        local data = ShuJia2022_XianSuo.GetData(LuaShanYeMiZongClueAvailableWnd.s_ClueId)
        local icon = data.icon
        if icon and icon ~= "" then
            self.IconTexture:LoadMaterial(icon)
        end
    end
end

--@region UIEvent

function LuaShanYeMiZongClueAvailableWnd:OnApplyButtonClick()
    LuaShanYeMiZongMainWnd.s_OpenWndAndChooseClueId = LuaShanYeMiZongClueAvailableWnd.s_ClueId
    CUIManager.ShowUI(CLuaUIResources.ShanYeMiZongMainWnd)
    CUIManager.CloseUI(CLuaUIResources.ShanYeMiZongClueAvailableWnd)
end


--@endregion UIEvent

