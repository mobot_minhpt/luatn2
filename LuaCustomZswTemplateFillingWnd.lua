local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"

LuaCustomZswTemplateFillingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaCustomZswTemplateFillingWnd, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaCustomZswTemplateFillingWnd, "Count", "Count", UILabel)
RegistChildComponent(LuaCustomZswTemplateFillingWnd, "Mask", "Mask", GameObject)
RegistChildComponent(LuaCustomZswTemplateFillingWnd, "Name", "Name", UILabel)
RegistChildComponent(LuaCustomZswTemplateFillingWnd, "Desc", "Desc", UILabel)
RegistChildComponent(LuaCustomZswTemplateFillingWnd, "Anchor", "Anchor", GameObject)
RegistChildComponent(LuaCustomZswTemplateFillingWnd, "DefaultItem", "DefaultItem", GameObject)


--@endregion RegistChildComponent end
RegistClassMember(LuaCustomZswTemplateFillingWnd,"m_TemplateIdx")
RegistClassMember(LuaCustomZswTemplateFillingWnd,"m_FillSlotIdx")
RegistClassMember(LuaCustomZswTemplateFillingWnd,"m_ZswId")
function LuaCustomZswTemplateFillingWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.DefaultItem.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDefaultItemClick(go)
	end)


    --@endregion EventBind end
end

function LuaCustomZswTemplateFillingWnd:Init()
	self.m_TemplateIdx = CLuaZswTemplateMgr.CustomIndex
    self.m_FillSlotIdx = CLuaZswTemplateMgr.FillSlotIdx
	local templateData = CLuaZswTemplateMgr.GetCustomTemplateByIndex(self.m_TemplateIdx)
	local components = templateData.components
	local component = components[self.m_FillSlotIdx]
	local zswId = component.id
	self.m_ZswId = zswId
	local zswData = Zhuangshiwu_Zhuangshiwu.GetData(zswId)
	if not zswData then
		return
	end
	local itemId = zswData.ItemId
	local data = Item_Item.GetData(itemId)
	if not data then
		return
	end
	self.Icon:LoadMaterial(data.Icon)
	self.Name.text = zswData.Name
	self.Desc.text = SafeStringFormat3(LocalString.GetString("%d级%s 家园%d级"),zswData.Grade,CClientFurnitureMgr.GetTypeNameByType(zswData.Type),zswData.HouseGrade)
	local canFillCount = CLuaZswTemplateMgr.GetCanFillZswCount(self.m_TemplateIdx,zswId)
	self.Count.text = canFillCount
	self.Mask:SetActive(canFillCount <= 0)
	if canFillCount <= 0 then
		self.Count.color = Color.red
	else
		self.Count.color = Color.white
	end
end

--@region UIEvent

function LuaCustomZswTemplateFillingWnd:OnDefaultItemClick(go)
	local canFillCount = CLuaZswTemplateMgr.GetCanFillZswCount(self.m_TemplateIdx,self.m_ZswId)
	local itemId = Zhuangshiwu_Zhuangshiwu.GetData(self.m_ZswId).ItemId
	if canFillCount <= 0 then   --get
		CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId,true,go.transform,AlignType.Left)
	else						--fill
		CLuaZswTemplateMgr.AddFillingZsw(self.m_TemplateIdx,self.m_ZswId,self.m_FillSlotIdx,true)
		CUIManager.CloseUI(CLuaUIResources.CustomZswTemplateFillingWnd)
	end
end

--@endregion UIEvent

