local UITexture = import "UITexture"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUIFx = import "L10.UI.CUIFx"
local UISprite = import "UISprite"
local UILabel = import "UILabel"
local CTopAndRightTipWnd = import "L10.UI.CTopAndRightTipWnd"
local CTooltip = import "L10.UI.CTooltip"
local AlignType = import "L10.UI.CTooltip.AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CoreScene = import "L10.Engine.Scene.CoreScene"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CClientNpc = import "L10.Game.CClientNpc"
local CMainCamera = import "L10.Engine.CMainCamera"

LuaUpDownWorldTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaUpDownWorldTopRightWnd, "RightBtn", "RightBtn", GameObject)
RegistChildComponent(LuaUpDownWorldTopRightWnd, "ScoreButton", "ScoreButton", GameObject)
RegistChildComponent(LuaUpDownWorldTopRightWnd, "KillMonButton", "KillMonButton", GameObject)
RegistChildComponent(LuaUpDownWorldTopRightWnd, "TowerButton", "TowerButton", GameObject)
RegistChildComponent(LuaUpDownWorldTopRightWnd, "Fx", "Fx", CUIFx)
RegistChildComponent(LuaUpDownWorldTopRightWnd, "Circle", "Circle", UITexture)
RegistChildComponent(LuaUpDownWorldTopRightWnd, "TowerLabel", "TowerLabel", UILabel)
RegistChildComponent(LuaUpDownWorldTopRightWnd, "KillMonLabel", "KillMonLabel", UILabel)
RegistChildComponent(LuaUpDownWorldTopRightWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaUpDownWorldTopRightWnd, "Tips", "Tips", GameObject)
RegistChildComponent(LuaUpDownWorldTopRightWnd, "ExchangeBtn", "ExchangeBtn", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaUpDownWorldTopRightWnd,"m_curEngineID")

function LuaUpDownWorldTopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.RightBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightBtnClick()
	end)

	UIEventListener.Get(self.ScoreButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnScoreButtonClick()
	end)

	UIEventListener.Get(self.KillMonButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnKillMonButtonClick()
	end)

	UIEventListener.Get(self.TowerButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTowerButtonClick()
	end)

	UIEventListener.Get(self.ExchangeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExchangeBtnClick()
	end)

    --@endregion EventBind end
end

function LuaUpDownWorldTopRightWnd:Init()
    self.Tips:SetActive(false)
    self.ExchangeBtn:SetActive(false)
    self:OnSyncUpDownWorldBattleScore()
    self:OnSyncUpDownWorldBattleInfo()
end

function LuaUpDownWorldTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSyncUpDownWorldBattleInfo", self, "OnSyncUpDownWorldBattleInfo")
    g_ScriptEvent:AddListener("OnSyncUpDownWorldBattleScore", self, "OnSyncUpDownWorldBattleScore")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("MainPlayerMoveStepped", self, "OnPlayerMove")
    g_ScriptEvent:AddListener("OnSyncFxEvent", self, "OnSyncFxEvent")
end

function LuaUpDownWorldTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncUpDownWorldBattleInfo", self, "OnSyncUpDownWorldBattleInfo")
    g_ScriptEvent:RemoveListener("OnSyncUpDownWorldBattleScore", self, "OnSyncUpDownWorldBattleScore")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("MainPlayerMoveStepped", self, "OnPlayerMove")
    g_ScriptEvent:RemoveListener("OnSyncFxEvent", self, "OnSyncFxEvent")
end

function LuaUpDownWorldTopRightWnd:OnSyncFxEvent(type)
    local root = self.Tips.transform
    local tips1 = FindChildWithType(root,"Tip01",typeof(GameObject))
    local tips2 = FindChildWithType(root,"Tip02",typeof(GameObject))
    tips1:SetActive(type == 1)
    tips2:SetActive(type == 2)
end

function LuaUpDownWorldTopRightWnd:GetCheckPosObj()
    local player = CClientMainPlayer.Inst
    if not player then return nil end
    local pos = player.RO.Position
    for x=-1,1,1 do
        for y=-1,1,1 do
            local xx = math.floor(pos.x) + x
            local yy = math.floor(pos.z) + y

            local objectid_list = CoreScene.MainCoreScene:GetGridAllObjectIdsWithFloor(xx, yy, 0)
            if objectid_list and objectid_list.Count > 0 then
                for i = 1, objectid_list.Count do
                    local engineId = objectid_list[i-1]
                    local obj = CClientObjectMgr.Inst:GetObject(engineId)
                    if TypeIs(obj,typeof(CClientNpc)) then
                        if obj.TemplateId == 20007794 or obj.TemplateId == 20007795 or  obj.TemplateId == 20007796 then
                            return obj
                        end
                    end
                end
            end
        end
    end
    return nil
end

function LuaUpDownWorldTopRightWnd:OnPlayerMove()
    local obj = self:GetCheckPosObj()
    if not obj then
        self.m_curEngineID = 0
        self.ExchangeBtn:SetActive(false)
        return 
    end
    self.m_curEngineID = obj.EngineId
    local wpos = obj.RO.TopAnchorPos
    wpos.y = (wpos.y + obj.RO.Position.y)/2
    local spos = CMainCamera.Main:WorldToScreenPoint(wpos)
    spos.z = 0
    local wpos2 = CUIManager.UIMainCamera:ScreenToWorldPoint(spos)
    self.ExchangeBtn.transform.position = wpos2
    self.ExchangeBtn:SetActive(true)
end

function LuaUpDownWorldTopRightWnd:OnHideTopAndRightTipWnd()
    self.RightBtn.transform.localEulerAngles = Vector3(0, 0, 180)
end

function LuaUpDownWorldTopRightWnd:OnSyncUpDownWorldBattleScore()
    local data = LuaHanJia2024Mgr.UpDownWorldData
    local score = data.Score
    self.ScoreLabel.text = tostring(score)
end

function LuaUpDownWorldTopRightWnd:OnSyncUpDownWorldBattleInfo()
    local data = LuaHanJia2024Mgr.UpDownWorldData
    local floorindex = data.Floor
    local count = data.Count
    local totle = data.Total
    self.TowerButton:SetActive(floorindex == 3)
    self.TowerLabel.text = LocalString.GetString("余") .. count
    
    self.KillMonButton:SetActive(floorindex ~= 3)
    self.KillMonLabel.text = count .. "/" .. totle
    self.Circle.fillAmount = math.min(1, count / totle)
	self.Fx.gameObject:SetActive(count>=totle)
    
end

function LuaUpDownWorldTopRightWnd:ShowToolTip(transform,msg)
	CTooltip.Show(msg, transform, AlignType.Bottom)
end

--@region UIEvent

function LuaUpDownWorldTopRightWnd:OnRightBtnClick()
    self.RightBtn.transform.localEulerAngles = Vector3(0, 0, 0)
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaUpDownWorldTopRightWnd:OnScoreButtonClick()
	self:ShowToolTip(self.ScoreButton.transform,g_MessageMgr:FormatMessage("UPDOWNWORLD_SCORE_BTN_DESC"))
end

function LuaUpDownWorldTopRightWnd:OnKillMonButtonClick()
	self:ShowToolTip(self.KillMonButton.transform,g_MessageMgr:FormatMessage("UPDOWNWORLD_KILLMON_BTN_DESC"))
end

function LuaUpDownWorldTopRightWnd:OnTowerButtonClick()
	self:ShowToolTip(self.TowerButton.transform,g_MessageMgr:FormatMessage("UPDOWNWORLD_TOWER_BTN_DESC"))
end

function LuaUpDownWorldTopRightWnd:OnExchangeBtnClick()
    if self.m_curEngineID > 0 then
        CClientMainPlayer.Inst:InteractWithNpc(self.m_curEngineID);
    end
end

--@endregion UIEvent
