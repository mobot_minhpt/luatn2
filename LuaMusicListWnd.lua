local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaMusicListWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaMusicListWnd, "QnTableView", "QnTableView", QnAdvanceGridView)
RegistChildComponent(LuaMusicListWnd, "GetMoreMusicButton", "GetMoreMusicButton", GameObject)
RegistChildComponent(LuaMusicListWnd, "NoneMusicLabel", "NoneMusicLabel", GameObject)
RegistClassMember(LuaMusicListWnd, "m_MusicDataList")
--@endregion RegistChildComponent end

function LuaMusicListWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	self.QnTableView.m_DataSource = DefaultTableViewDataSource.Create(
		function()
			return self:NumberOfRows()
		end,
		function(item, index)
			return self:ItemAt(item,index)
		end
	)
	--local callback = DelegateFactory.Action_int(function(row) self:OnSelectAtRow(row) end)	self.QnTableView.OnSelectAtRow = callback
	self.m_MusicDataList = CLuaMusicBoxMgr.m_MusicBoxList
	self.QnTableView:ReloadData(true,true)
	self.NoneMusicLabel:SetActive(self:NumberOfRows() == 0)
	UIEventListener.Get(self.GetMoreMusicButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGetMoreMusicButtonClick()
	end)
	CLuaMusicBoxMgr:EnableMusicBox()
    --@endregion EventBind end
end

function LuaMusicListWnd:OnEnable()
	g_ScriptEvent:AddListener("OnSendMusicBoxState", self, "OnSendMusicBoxState")
	Gac2Gas.RequestMusicBoxPlayList()
end

function LuaMusicListWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSendMusicBoxState", self, "OnSendMusicBoxState")
end

function LuaMusicListWnd:OnSendMusicBoxState(currentSong, currentState, progress)
	self.m_MusicDataList = CLuaMusicBoxMgr.m_MusicBoxList
	self.NoneMusicLabel:SetActive(self:NumberOfRows() == 0)
	self.QnTableView:ReloadData(true,false)
end

--@region UIEvent

function LuaMusicListWnd:OnGetMoreMusicButtonClick()
	g_MessageMgr:ShowMessage("MUSIC_BOX_GET_NOTICE")
	-- local itemGetId = 21031349
	-- print(itemGetId)
	-- CItemAccessListMgr.Inst:ShowItemAccessInfo(itemGetId, false, self.GetMoreMusicButton.transform, AlignType.Right)
end

function LuaMusicListWnd:ItemAt(item, row)
	if not self.m_MusicDataList then return end
	local data = self.m_MusicDataList[row + 1]
	self:InitItem(item.transform,data)
end

function LuaMusicListWnd:InitItem(transform,data)
	local playButton = transform:Find("PlayButton").gameObject
	local stopButton = transform:Find("StopButton").gameObject
	local playSprite = transform:Find("PlaySprite").gameObject
	local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local periodLabel = transform:Find("PeriodLabel"):GetComponent(typeof(UILabel))
	local now = CServerTimeMgr.Inst.timeStamp
	local isPlaying = CLuaMusicBoxMgr.m_CurPlayMusicIndex == data.Index and CLuaMusicBoxMgr.m_CurPlayMusicState and CLuaMusicBoxMgr.m_CurPlayMusicState == EnumMusicBoxState.Playing and now < data.ExpireTime
	playButton:SetActive(not isPlaying)
	stopButton:SetActive(isPlaying)
	playSprite:SetActive(isPlaying)
	
	local myMusicData = data
	UIEventListener.Get(playButton).onClick = DelegateFactory.VoidDelegate(function (go)
		local curMusicData, musicIndex = CLuaMusicBoxMgr:GetCurMusicDataAndIndex()
        CLuaMusicBoxMgr:PlayMusicBox(myMusicData, curMusicData.BuffID == myMusicData.BuffID )
	end)
	UIEventListener.Get(stopButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    CLuaMusicBoxMgr:StopMusicBox()
	end)
	nameLabel.text = data.Name
	local periodText = System.String.Format(now < data.ExpireTime and LocalString.GetString("有效期至 {0}") or LocalString.GetString("[E52626]已失效 {0}"), ToStringWrap(CServerTimeMgr.ConvertTimeStampToZone8Time(data.ExpireTime), "yyyy-MM-dd"))
	periodLabel.text = CUICommonDef.TranslateToNGUIText( periodText)
	if data.ExpireTime == 0 then periodLabel.text = "" end
end

function LuaMusicListWnd:NumberOfRows()
	return self.m_MusicDataList and #self.m_MusicDataList or 0
end

function LuaMusicListWnd:OnQnTableViewSelectAtRow(row)
end

--@endregion UIEvent

