require("common/common_include")
local CommonDefs = import "L10.Game.CommonDefs"
local Extensions = import "Extensions"
local Profession = import "L10.Game.Profession"
local CSkillItemCell = import "L10.UI.CSkillItemCell"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local CTeamMgr = import "L10.Game.CTeamMgr"

LuaSnowAdventureStatwnd = class()

--@region RegistChildComponent: Dont Modify Manually!

--@endregion RegistChildComponent end

function LuaSnowAdventureStatwnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.SkillItemTemplate = self.transform:Find("Anchor/SkillItemTemplate")
    self:RefreshData()
end

function LuaSnowAdventureStatwnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaSnowAdventureStatwnd:RefreshData()
    local statData = LuaHanJia2023Mgr.battleStat
    local templateObj = self.transform:Find("Anchor/Rank/AdvView/Pool/Template").gameObject
    templateObj:SetActive(false)
    local grid = self.transform:Find("Anchor/Rank/AdvView/Scroll View/Grid"):GetComponent(typeof(UIGrid))
    Extensions.RemoveAllChildren(grid.transform)
    local iCnt = 0
    for k, v in pairs(statData or {}) do
        local playerId = k
        --队员可能离队
        local isValidData = false
        if CClientMainPlayer.Inst then
            if playerId == CClientMainPlayer.Inst.Id then
                isValidData = true
            elseif CTeamMgr.Inst:GetMemberById(playerId) then
                isValidData = true
            end
        end
        if isValidData then
            local obj = NGUITools.AddChild(grid.gameObject, templateObj)
            obj:SetActive(true)
            self:InitItem(obj.transform, k, v, iCnt % 2 == 0)
            iCnt = iCnt + 1
        end
    end
    grid:Reposition()
end

function LuaSnowAdventureStatwnd:InitItem(rootTrans, playerId, data, isOdd)
    local isMe = CClientMainPlayer.Inst and (playerId == CClientMainPlayer.Inst.Id) or false
    local labelColor = isMe and Color.green or Color.white
    
    local snowmanType = data.snowmanType
    local snowmanLevel = data.snowmanLv
    local dps = data.dps
    local heal = data.heal
    local ctrlNum = data.ctrlNum
    local skillUseInfo = data.skillUseInfo
    local name = ""
    if isMe then
        name = CClientMainPlayer.Inst.RealName
    else
        local info = CTeamMgr.Inst:GetMemberById(playerId)
        name = info.m_MemberName
    end

    rootTrans:Find("MineBg").gameObject:SetActive(isMe)
    rootTrans:Find("OtherBg").gameObject:SetActive(not isMe)
    
    rootTrans:Find("PlayerNameLabel"):GetComponent(typeof(UILabel)).text = name
    rootTrans:Find("PlayerNameLabel"):GetComponent(typeof(UILabel)).color = labelColor
    rootTrans:Find("DamageLabel"):GetComponent(typeof(UILabel)).text = dps
    rootTrans:Find("DamageLabel"):GetComponent(typeof(UILabel)).color = labelColor
    rootTrans:Find("HealLabel"):GetComponent(typeof(UILabel)).text = heal
    rootTrans:Find("HealLabel"):GetComponent(typeof(UILabel)).color = labelColor
    rootTrans:Find("ControlLabel"):GetComponent(typeof(UILabel)).text = ctrlNum
    rootTrans:Find("ControlLabel"):GetComponent(typeof(UILabel)).color = labelColor

    local snowmanConfig = HanJia2023_XuePoLiXianSnowman.GetData(snowmanType)
    rootTrans:Find("PlayerCarrer"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(snowmanConfig.Career)
    
    local skillContent =  rootTrans:Find("SkillScrollView/SkillContent")
    for k, v in pairs(skillUseInfo) do
        local skillGo = CommonDefs.Object_Instantiate(self.SkillItemTemplate)
        local cell = skillGo:GetComponent(typeof(CSkillItemCell))
        skillGo.transform.parent = skillContent.transform
        skillGo.transform.localScale = Vector3.one
        skillGo.gameObject:SetActive(true)
        
        local skillLevel = snowmanLevel
        for _skillLevel = snowmanLevel, 1, -1 do
            if (Skill_AllSkills.Exists(tonumber(k)*100+ _skillLevel)) then
                skillLevel = _skillLevel
                break
            end
        end
        
        local skillId = k * 100 + skillLevel
        local skillData = Skill_AllSkills.GetData(skillId)
        cell:Init(skillId, 0, 0, false, skillData and skillData.SkillIcon, false, nil)
        skillGo.transform:Find("SpellLabel"):GetComponent(typeof(UILabel)).text = v
        skillGo.transform:Find("SpellLabel"):GetComponent(typeof(UILabel)).color = labelColor
        UIEventListener.Get(cell.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
            CSkillInfoMgr.ShowSkillInfoWnd(skillId, true, 0, 0, nil)
        end)
    end
    skillContent:GetComponent(typeof(UIGrid)):Reposition()
end 