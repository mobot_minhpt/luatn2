local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaWuYi2023GetRewardView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuYi2023GetRewardView, "YellowBg", GameObject)
RegistChildComponent(LuaWuYi2023GetRewardView, "GreenBg", GameObject)
--@endregion RegistChildComponent end

function LuaWuYi2023GetRewardView:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.YellowBg.gameObject:SetActive(LuaWuYi2023Mgr.m_GetRewardWndType == 0)
    self.GreenBg.gameObject:SetActive(LuaWuYi2023Mgr.m_GetRewardWndType == 1)
end

--@region UIEvent

--@endregion UIEvent

