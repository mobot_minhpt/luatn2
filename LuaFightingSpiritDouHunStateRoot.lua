local CUIFx = import "L10.UI.CUIFx"

local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local Gac2Gas2 = import "L10.Game.Gac2Gas"
local DouHunTan_Setting = import "L10.Game.DouHunTan_Setting"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"

LuaFightingSpiritDouHunStateRoot = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFightingSpiritDouHunStateRoot, "StatusItem1", "StatusItem1", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunStateRoot, "StatusItem2", "StatusItem2", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunStateRoot, "StatusItem3", "StatusItem3", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunStateRoot, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaFightingSpiritDouHunStateRoot, "RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunStateRoot, "StatusFx", "StatusFx", CUIFx)

--@endregion RegistChildComponent end
RegistClassMember(LuaFightingSpiritDouHunStateRoot, "m_ProgressTextureList")
RegistClassMember(LuaFightingSpiritDouHunStateRoot, "m_CountLabelList")
RegistClassMember(LuaFightingSpiritDouHunStateRoot, "m_RankBtnList")
RegistClassMember(LuaFightingSpiritDouHunStateRoot, "m_ShowDataAction")
RegistClassMember(LuaFightingSpiritDouHunStateRoot, "m_HasInit")
RegistClassMember(LuaFightingSpiritDouHunStateRoot, "m_FxList")
RegistClassMember(LuaFightingSpiritDouHunStateRoot, "m_FxState")

function LuaFightingSpiritDouHunStateRoot:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)


    --@endregion EventBind end
end

function LuaFightingSpiritDouHunStateRoot:Init()

	local itemList = {self.StatusItem1, self.StatusItem2, self.StatusItem3}

	self.m_HasInit = true
	self.m_ProgressTextureList = {}
	self.m_CountLabelList = {}
	self.m_RankBtnList = {}

	-- 特效
	self.m_FxState = {}
	for i=1,3 do
		self.m_FxState[i] = false
	end

	self.StatusFx.OnLoadFxFinish = DelegateFactory.Action(function()
		self.m_FxList = {}
		local root = self.StatusFx.transform:Find("__FxRoot__"):GetChild(0).transform
		table.insert(self.m_FxList, root:Find("Green").gameObject)
		table.insert(self.m_FxList, root:Find("Yellow").gameObject)
		table.insert(self.m_FxList, root:Find("Blue").gameObject)
		self:UpdateFx()
	end)
	self.StatusFx:LoadFx("fx/ui/prefab/UI_DouHunZhuangtai.prefab")

	for i=1, 3 do
		table.insert(self.m_ProgressTextureList, itemList[i].transform:Find("ProgressTexture"):GetComponent(typeof(UITexture)))
		table.insert(self.m_CountLabelList, itemList[i].transform:Find("CountLabel"):GetComponent(typeof(UILabel)))
		table.insert(self.m_RankBtnList, itemList[i].transform:Find("RankBtn").gameObject)

		self.m_CountLabelList[i].text = "0/0"
		self.m_ProgressTextureList[i].fillAmount = 0

		UIEventListener.Get(self.m_RankBtnList[i]).onClick = DelegateFactory.VoidDelegate(function (go)
			--print("Gac2Gas.QueryDouHunTrainRank(i)", i)
			Gac2Gas.QueryDouHunTrainRank(i)
		end)
	end

	self.DescLabel.text = g_MessageMgr:FormatMessage("DouHun_State_Title_Desc")
end

function LuaFightingSpiritDouHunStateRoot:UpdateFx()
	if self.m_FxList then
		for i=1, #self.m_FxList do
			self.m_FxList[i]:SetActive(self.m_FxState[i])
		end
	end
end

function LuaFightingSpiritDouHunStateRoot:ShowData()
	if not self.m_HasInit then
		self:Init()
	end
	
	local data = DouHunTan_Setting.GetData()
	local maxValueList = {data.MaxJingLi, data.MaxQiLi, data.MaxShenLi}
	for i=1, 3 do
		local value = CFightingSpiritMgr.Instance:GetDouhunTrainValue(i)
		local maxValue = maxValueList[i]
		self.m_CountLabelList[i].text = tostring(value) .. "/" .. tostring(maxValue)
		--self.m_FxList[i]:SetActive(value == maxValue)
		self.m_FxState[i] = value == maxValue
		self.m_ProgressTextureList[i].fillAmount = value/maxValue
	end
	self:UpdateFx()
end

-- serverId, serverName, serverCount
function LuaFightingSpiritDouHunStateRoot:OnTrainRankResult(rankType, rankList, mainPlayerData)
	print("OnTrainRankResult")
	local spriteNameList = {"common_hpandmp_green", "common_hpandmp_max", "common_hpandmp_mp"}
	local titleList = {LocalString.GetString("精力"), LocalString.GetString("气力"), LocalString.GetString("神力")}

	LuaCommonProgressRankWndMgr.m_RankData = rankList
	LuaCommonProgressRankWndMgr.m_MainPlayerData = mainPlayerData
	LuaCommonProgressRankWndMgr.m_BottomText = g_MessageMgr:FormatMessage("DouHun_Foster_Rank_Wnd_Desc")
	if CommonDefs.IS_VN_CLIENT then
		if LocalString.language == "cn" then
			LuaCommonProgressRankWndMgr.m_TitleText = titleList[rankType] .. LocalString.GetString("培养排行榜")
		else
			LuaCommonProgressRankWndMgr.m_TitleText = LocalString.GetString("培养排行榜") .. titleList[rankType]
		end
	else
		LuaCommonProgressRankWndMgr.m_TitleText = titleList[rankType] .. LocalString.GetString("培养排行榜")
	end
	LuaCommonProgressRankWndMgr.m_HeaderText1 = LocalString.GetString("排名")
	LuaCommonProgressRankWndMgr.m_HeaderText2 = LocalString.GetString("服务器")
	LuaCommonProgressRankWndMgr.m_HeaderText3 = SafeStringFormat3(LocalString.GetString("斗魂%s培养进度"), titleList[rankType])
	LuaCommonProgressRankWndMgr.m_ProgressBarSpritename = spriteNameList[rankType]
	LuaCommonProgressRankWndMgr.m_BottomBtnText = LocalString.GetString("前去培养")
	LuaCommonProgressRankWndMgr.m_OnBottomBtnClick = function()
		-- 打开培养界面
		g_ScriptEvent:BroadcastInLua("FightingSpiritDouHunWndShowPage", 2, rankType)
		CUIManager.CloseUI(CLuaUIResources.CommonProgressRankWnd)
	end

	CUIManager.ShowUI(CLuaUIResources.CommonProgressRankWnd)
end

function LuaFightingSpiritDouHunStateRoot:OnEnable()
	-- 请求放在onEnable里面
	Gac2Gas2.QueryDouHunTrainValue()

    if self.m_ShowDataAction == nil then
        self.m_ShowDataAction = DelegateFactory.Action(function()
			self:ShowData()
        end)
    end
    EventManager.AddListenerInternal(EnumEventType.FightingSpiritDouhunTrainRes, self.m_ShowDataAction)
	g_ScriptEvent:AddListener("QueryDouHunTrainRankResult", self, "OnTrainRankResult")
end

function LuaFightingSpiritDouHunStateRoot:OnDisable()
    EventManager.RemoveListenerInternal(EnumEventType.FightingSpiritDouhunTrainRes, self.m_ShowDataAction)
	g_ScriptEvent:RemoveListener("QueryDouHunTrainRankResult", self, "OnTrainRankResult")
end

--@region UIEvent

function LuaFightingSpiritDouHunStateRoot:OnRuleBtnClick()
	g_MessageMgr:ShowMessage("DouHun_State_Rule_Desc")
end

--@endregion UIEvent

