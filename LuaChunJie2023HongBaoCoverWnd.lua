local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"

LuaChunJie2023HongBaoCoverWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaChunJie2023HongBaoCoverWnd, "Label", "Label", UILabel)
RegistChildComponent(LuaChunJie2023HongBaoCoverWnd, "ProgressBar", "ProgressBar", UISlider)
RegistChildComponent(LuaChunJie2023HongBaoCoverWnd, "ProgressText", "ProgressText", UILabel)

RegistClassMember(LuaChunJie2023HongBaoCoverWnd, "m_TotalJade") -- 总灵玉
RegistClassMember(LuaChunJie2023HongBaoCoverWnd, "m_CurJade")   -- 当前灵玉
--@endregion RegistChildComponent end

function LuaChunJie2023HongBaoCoverWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaChunJie2023HongBaoCoverWnd:Init()
    self.Label.text = g_MessageMgr:FormatMessage("ChunJie2023_HongBaoCover_GetCoverTip")
    self.m_TotalJade = ChunJie_ZJNHSetting.GetData().HongBaoCoverUnlockNeedJade
    self:UpdateProgress()
end

function LuaChunJie2023HongBaoCoverWnd:UpdateProgress()
    local MainPlayer = CClientMainPlayer.Inst
    if MainPlayer then
        self.m_CurJade = math.min(CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eSpring2023_HongBaoJade),self.m_TotalJade)
    end
    self.ProgressText.text = SafeStringFormat3("%d/%d",self.m_CurJade,self.m_TotalJade)
    self.ProgressBar.value = self.m_CurJade/self.m_TotalJade
end

--@region UIEvent

--@endregion UIEvent

