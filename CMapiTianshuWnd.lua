-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CMapiTianshuWnd = import "L10.UI.CMapiTianshuWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local VoidDelegate = import "UIEventListener+VoidDelegate"
--local ZuoQi_MapiYumashiUpgrade = import "L10.Game.ZuoQi_MapiYumashiUpgrade"
CMapiTianshuWnd.m_OnEnable_CS2LuaHook = function (this)
    UIEventListener.Get(this.m_CloseBtn).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.m_IncreaseBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.OnIncreaseBtnClick, VoidDelegate, this)
    UIEventListener.Get(this.m_DecreaseBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.OnDecreaseBtnClick, VoidDelegate, this)
    this.m_DescTemplate:SetActive(false)
end
CMapiTianshuWnd.m_ShowTianshu_CS2LuaHook = function (this, page)
    this.m_DecreaseBtn.Enabled = page > 1
    this.m_IncreaseBtn.Enabled = page < this.m_TotalPage
    this.m_PageLabel.text = System.String.Format("{0}/{1}", page, this.m_TotalPage)
    Extensions.RemoveAllChildren(this.m_DescriptionGrid.transform)
    do
        local i = (page - 1) * CMapiTianshuWnd.s_CountInPage local cnt = this.m_DescriptionList.Count local end_ = page * CMapiTianshuWnd.s_CountInPage
        while i < cnt and i < end_ do
            local go = NGUITools.AddChild(this.m_DescriptionGrid.gameObject, this.m_DescTemplate)
            go:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(go, typeof(UILabel)).text = this.m_DescriptionList[i]
            CommonDefs.GetComponentsInChildren_GameObject_Type_Boolean(go, typeof(UISprite), true)[0].gameObject:SetActive(i >= this.m_YumaLevel)
            i = i + 1
        end
    end
    this.m_DescriptionGrid:Reposition()
end
CMapiTianshuWnd.m_Init_CS2LuaHook = function (this)
    local yumaData = CClientMainPlayer.Inst.PlayProp.YumaPlayData
    if nil == yumaData then
        this:Close()
        return
    end
    this.m_YumaLevel = yumaData.Level
    CommonDefs.ListClear(this.m_DescriptionList)
    ZuoQi_MapiYumashiUpgrade.Foreach(function (key, data)
        CommonDefs.ListAdd(this.m_DescriptionList, typeof(String), data.TianshuDesc)
    end)
    this.m_TotalPage = math.floor(this.m_DescriptionList.Count / CMapiTianshuWnd.s_CountInPage) + this.m_DescriptionList.Count % CMapiTianshuWnd.s_CountInPage
    this:ShowTianshu(1)
end
