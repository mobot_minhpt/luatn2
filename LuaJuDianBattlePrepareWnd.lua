local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"

LuaJuDianBattlePrepareWnd = class()
RegistChildComponent(LuaJuDianBattlePrepareWnd, "Tabs", UITabBar)
RegistChildComponent(LuaJuDianBattlePrepareWnd, "JuDianBattlePrepareView", GameObject)
RegistChildComponent(LuaJuDianBattlePrepareWnd, "JuDianBattleConsecrateView", GameObject)

function LuaJuDianBattlePrepareWnd:Awake()
    self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabsTabChange(index)
	end)
end

function LuaJuDianBattlePrepareWnd:Init()
    self.Tabs:ChangeTab(0, false)
    self:OnTabsTabChange(0)
end

--@region UIEvent

function LuaJuDianBattlePrepareWnd:OnTabsTabChange(index)
    self.JuDianBattlePrepareView:SetActive(index == 0)
    self.JuDianBattleConsecrateView:SetActive(index == 1)
end


--@endregion UIEvent
