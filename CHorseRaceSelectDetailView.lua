-- Auto Generated!!
local CHorseRaceSelectDetailView = import "L10.UI.CHorseRaceSelectDetailView"
local CMapiViewSimpleItem = import "L10.UI.CMapiViewSimpleItem"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CUIDirectories = import "L10.UI.CUIDirectories"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local ZuoQi_MapiSkill = import "L10.Game.ZuoQi_MapiSkill"
local ZuoQi_Setting = import "L10.Game.ZuoQi_Setting"
CHorseRaceSelectDetailView.m_Init_CS2LuaHook = function (this, data) 
    this.data = data
    this:OnRequestSelectHorseResult()
    this.nameLevelLabel.text = System.String.Format("lv.{0}  {1}", data.mapiLevel, data.mapiName)
    --ZuoQi_MapiType type = ZuoQi_MapiType.GetData(data.quality);
    this.typeLabel.text = ""
    Gac2Gas.QueryMapiDetail(data.id)
end
CHorseRaceSelectDetailView.m_OnSyncMapiInfo_CS2LuaHook = function (this, zuoqiId) 
    if this.data == nil then
        return
    end
    if this.data.id == zuoqiId then
        local mapiInfo = nil
        do
            local i = 0
            while i < CZuoQiMgr.Inst.zuoqis.Count do
                if zuoqiId == CZuoQiMgr.Inst.zuoqis[i].id then
                    mapiInfo = CZuoQiMgr.Inst.zuoqis[i].mapiInfo
                    break
                end
                i = i + 1
            end
        end

        if mapiInfo ~= nil then
            this:ShowMapiInfo(zuoqiId, mapiInfo)
            this:InitSkill(mapiInfo)
            this.textureLoader:Init(mapiInfo, nil, nil, -135, nil)
        end
    end
end
CHorseRaceSelectDetailView.m_ShowMapiInfo_CS2LuaHook = function (this, zuoqiId, mapiInfo) 
    local appearanceName = CZuoQiMgr.GetAppearanceName(mapiInfo.PinzhongId, mapiInfo.Quality)
    local attrName = CZuoQiMgr.GetAttributeName(mapiInfo.AttributeNameId)
    this.typeLabel.text = System.String.Format(LocalString.GetString("品种  {0}·{1}"), appearanceName, attrName)
    this.speedLabel.text = tostring(mapiInfo.Speed)
    this.agilityLabel.text = tostring(mapiInfo.Lingqiao)
    --accLabel.text = LocalString.GetString("暂无");
    this.durationLabel.text = tostring(mapiInfo.Naili)
    this.grassLabel.text = tostring(mapiInfo.CaodiShiying)
    this.rockLabel.text = tostring(mapiInfo.YandiShiying)
    this.mudLabel.text = tostring(mapiInfo.NidiShiying)
end
CHorseRaceSelectDetailView.m_InitSkill_CS2LuaHook = function (this, mapiInfo) 
    Extensions.RemoveAllChildren(this.skillTable.transform)
    this.skillTemplate:SetActive(false)
    do
        local i = 0
        while i < ZuoQi_Setting.GetData().MapiMaxSkill do
            local instance = NGUITools.AddChild(this.skillTable.gameObject, this.skillTemplate)
            instance:SetActive(true)
            local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CMapiViewSimpleItem))
            if template ~= nil then
                local skillIcon = nil
                local showAdd = false
                local textureAction = nil
                if i < mapiInfo.Skills.Length then
                    local skillId = mapiInfo.Skills[i + 1]
                    local skill = ZuoQi_MapiSkill.GetData(skillId)
                    if skill ~= nil then
                        skillIcon = (CUIDirectories.SkillIconsMaterialDir .. skill.Icon) .. ".mat"
                        showAdd = false

                        textureAction = DelegateFactory.Action(function () 
                            CSkillInfoMgr.ShowSkillInfoWnd(skillId, instance.transform.position, CSkillInfoMgr.EnumSkillInfoContext.MapiSkill, true, 0, 0, nil)
                        end)
                    end
                end
                template:Init(skillIcon, "", 0, showAdd, DelegateFactory.Action(function () 
                    g_MessageMgr:ShowMessage("Mapi_Learn_Skill_Hint")
                end), nil, textureAction, nil)
            end
            i = i + 1
        end
    end
    this.skillTable:Reposition()
end
