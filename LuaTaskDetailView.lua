local UISprite = import "UISprite"
local UInt32 = import "System.UInt32"
local UILabel = import "UILabel"
local UIEventListener = import "UIEventListener"
local Task_Task = import "L10.Game.Task_Task"
local StringBuilder = import "System.Text.StringBuilder"
local NGUIText = import "NGUIText"
local MessageWndManager = import "L10.UI.MessageWndManager"
local LocalString = import "LocalString"
local Item_Item = import "L10.Game.Item_Item"
local IdPartition = import "L10.Game.IdPartition"
local Extensions = import "Extensions"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EnumQualityType = import "L10.Game.EnumQualityType"
local DelegateFactory = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local CUIManager = import "L10.UI.CUIManager"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChatLinkMgr = import "CChatLinkMgr"

local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr.AlignType"
local UIAlertIcon = import "L10.UI.UIAlertIcon"
local GameSetting_Client = import "L10.Game.GameSetting_Client"
local QnCheckBox = import "L10.UI.QnCheckBox"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local UIWidget = import "UIWidget"


CLuaTaskDetailView = class()
RegistClassMember(CLuaTaskDetailView,"detailScrollView")
RegistClassMember(CLuaTaskDetailView,"detailTable")
RegistClassMember(CLuaTaskDetailView,"taskNameLabel") --显示任务名称
RegistClassMember(CLuaTaskDetailView, "m_CheckBoxInfoLabel")
RegistClassMember(CLuaTaskDetailView, "m_CheckBox")
RegistClassMember(CLuaTaskDetailView, "m_CheckBoxBg")
--Condition
RegistClassMember(CLuaTaskDetailView,"conditionSection") --任务条件
RegistClassMember(CLuaTaskDetailView,"conditionLabel") --显示任务完成条件
--Description
RegistClassMember(CLuaTaskDetailView,"descriptionSection") --任务描述
RegistClassMember(CLuaTaskDetailView,"descriptionLabel") --显示任务描述
--Awards
RegistClassMember(CLuaTaskDetailView,"awardsSection")
RegistClassMember(CLuaTaskDetailView,"itemsScrollView")
RegistClassMember(CLuaTaskDetailView,"itemsTable")
RegistClassMember(CLuaTaskDetailView,"itemTemplate")
RegistClassMember(CLuaTaskDetailView,"otherAwardsLabel")

RegistClassMember(CLuaTaskDetailView,"giveUpButton") --
RegistClassMember(CLuaTaskDetailView,"goToButton")
RegistClassMember(CLuaTaskDetailView,"giveupAlert")
RegistClassMember(CLuaTaskDetailView,"taskDetailTitle")

RegistClassMember(CLuaTaskDetailView,"templateId")
RegistClassMember(CLuaTaskDetailView,"lowerItemsScrollViewPosY")
RegistClassMember(CLuaTaskDetailView,"higherItemsScrollViewPosY")

RegistClassMember(CLuaTaskDetailView, "bGiveupConfirm")

function CLuaTaskDetailView:Awake()
    self.detailScrollView = self.transform:Find("DetailScrollView"):GetComponent(typeof(UIScrollView))
    self.detailTable = self.transform:Find("DetailScrollView/DetailTable"):GetComponent(typeof(UITable))
    self.taskNameLabel = self.transform:Find("DetailScrollView/DetailTable/NameLabel"):GetComponent(typeof(UILabel))
    self.m_CheckBoxInfoLabel = self.transform:Find("CheckBoxInfoLabel").gameObject
    self.m_CheckBox = self.transform:Find("DetailScrollView/DetailTable/NameLabel/Checkbox"):GetComponent(typeof(QnCheckBox))
    self.m_CheckBoxBg = self.transform:Find("DetailScrollView/DetailTable/NameLabel/Checkbox/Background"):GetComponent(typeof(UIWidget))
    self.conditionSection = self.transform:Find("DetailScrollView/DetailTable/ConditionSection").gameObject
    self.conditionLabel = self.transform:Find("DetailScrollView/DetailTable/ConditionSection/ConditionLabel"):GetComponent(typeof(UILabel))
    self.descriptionSection = self.transform:Find("DetailScrollView/DetailTable/DescriptionSection").gameObject
    self.descriptionLabel = self.transform:Find("DetailScrollView/DetailTable/DescriptionSection/DescriptionLabel"):GetComponent(typeof(UILabel))
    self.awardsSection = self.transform:Find("DetailScrollView/DetailTable/AwardsSection").gameObject
    self.itemsScrollView = self.transform:Find("DetailScrollView/DetailTable/AwardsSection/ItemsScrollView"):GetComponent(typeof(UIScrollView))
    self.itemsTable = self.transform:Find("DetailScrollView/DetailTable/AwardsSection/ItemsScrollView/Table"):GetComponent(typeof(UITable))
    self.itemTemplate = self.transform:Find("DetailScrollView/DetailTable/AwardsSection/ItemsScrollView/ItemCell").gameObject
    self.otherAwardsLabel = self.transform:Find("DetailScrollView/DetailTable/AwardsSection/OtherAwardsLabel"):GetComponent(typeof(UILabel))
    self.giveUpButton = self.transform:Find("ButtonsTable/GiveUpButton").gameObject
    self.goToButton = self.transform:Find("ButtonsTable/GoToButton").gameObject
    self.giveupAlert = self.transform:Find("ButtonsTable/GiveUpButton/Alert"):GetComponent(typeof(UIAlertIcon))
    self.taskDetailTitle = self.transform:Find("DetailScrollView/DetailTable/DescriptionSection").gameObject
    self.templateId = 0
    self.lowerItemsScrollViewPosY = -258
    self.higherItemsScrollViewPosY = -176

end

function CLuaTaskDetailView:Start()
    
end

-- Auto Generated!!
--UpLevel方法在svn revision 75822提交中废弃了，这里也去掉

function CLuaTaskDetailView:Init(templateId)
    if self.taskNameLabel then 
        CommonDefs.AddOnClickListener(self.giveUpButton, DelegateFactory.Action_GameObject(function(go) self:OnGiveUpButtonClick(go) end), false)
        CommonDefs.AddOnClickListener(self.goToButton, DelegateFactory.Action_GameObject(function(go) self:OnGoToButtonClick(go) end), false) 
        self.itemTemplate:SetActive(false)
        self.m_CheckBox.OnValueChanged = DelegateFactory.Action_bool(function (selected)
            self:OnCheckBoxValueChanged(selected)
        end)
    end
    
    self:ReloadTaskDescription(templateId or 0)
end

function CLuaTaskDetailView:OnEnable()
    g_ScriptEvent:AddListener("RefreshTaskStatus", self, "OnRefreshTaskStatus") -- C# definination
end

function CLuaTaskDetailView:OnDisable()
    g_ScriptEvent:RemoveListener("RefreshTaskStatus", self, "OnRefreshTaskStatus") -- C# definination
end

function CLuaTaskDetailView:OnRefreshTaskStatus()
    self:ReloadTaskDescription(self.templateId or 0)
end

function CLuaTaskDetailView:InitCheckBox(taskTemplate, isAcceptedTask)
    if taskTemplate==nil or not isAcceptedTask then --可接任务不显示
        self.m_CheckBoxInfoLabel:SetActive(false)
        self.m_CheckBox.gameObject:SetActive(false)
    else
        self.m_CheckBoxInfoLabel:SetActive(true)
        self.m_CheckBox.gameObject:SetActive(true)
        self.m_CheckBoxBg.enabled = (taskTemplate.DisplayNode ~= 1) --非主线任务才显示背景
        local ids = CLuaPlayerSettings.GetHiddenInTaskTrackPanelIds()
        local hidden = CLuaTaskMgr.HiddenInTaskTrackPanel(taskTemplate.ID, ids)
        self.m_CheckBox:SetSelected(not hidden, true)
    end
end

function CLuaTaskDetailView:OnCheckBoxValueChanged(selected)

    if not selected then
        local taskTemplate = self.templateId and Task_Task.GetData(self.templateId) or nil
        if taskTemplate and taskTemplate.DisplayNode == 1 then
            self.m_CheckBox:SetSelected(true, true) --主线任务不允许隐藏
            g_MessageMgr:ShowMessage("MainTaskTrackCancelNotice")
            return
        end
    end

    local ids = CLuaPlayerSettings.GetHiddenInTaskTrackPanelIds()
    if self.templateId then
        if selected then
            ids[self.templateId] = nil
            g_MessageMgr:ShowMessage("Task_Visible_In_Tracking_Panel")
        else
            ids[self.templateId]  = 1
            g_MessageMgr:ShowMessage("Task_Invisible_In_Tracking_Panel")
        end
        CLuaPlayerSettings.SetHiddenInTaskTrackPanelIds(ids)
        g_ScriptEvent:BroadcastInLua("OnUpdateTaskVisibleInTrackPanelStatus", self.templateId)
        EventManager.BroadcastInternalForLua(EnumEventType.RefreshTaskStatus, {}) -- 通知到CTaskListView
    end
end

function CLuaTaskDetailView:ReloadTaskDescription( templateId)
    if not self.taskNameLabel then return end
    self.taskNameLabel.text = nil
    self.conditionLabel.text = nil
    self.descriptionLabel.text = nil
    self.taskDetailTitle:SetActive(false)
    self.templateId = templateId
    self.giveUpButton:SetActive(false)
    self.goToButton:SetActive(false)
    self.bGiveupConfirm = false

    LuaXinBaiLianDongMgr.ExclusiveShenYao_TaskDetailLabel = nil

    local conditionText = ""
    local template = Task_Task.GetData(templateId)
    local isAcceptedTask = false
	local isReviewingTask = false
    if CClientMainPlayer.Inst ~= nil then
        local taskProperty = CClientMainPlayer.Inst.TaskProp
		isReviewingTask = CommonDefs.DictContains(taskProperty.ReviewingTasks, typeof(UInt32), templateId)
        if CommonDefs.DictContains(taskProperty.CurrentTasks, typeof(UInt32), templateId) then
            --已接任务
            isAcceptedTask = true
            self.giveUpButton:SetActive(true)
            self.goToButton:SetActive(true)
            self.giveupAlert:SetVisible(false, false, 0)
            self.taskDetailTitle:SetActive(true)
            self.taskNameLabel.text = template.Display
            self.bGiveupConfirm = template.GiveupConfirm > 0

            local task = CommonDefs.DictGetValue(taskProperty.CurrentTasks, typeof(UInt32), templateId)

            if task.IsFailed then
                self.taskNameLabel.text = self.taskNameLabel.text .. LocalString.GetString("  [c][ff0000]失败[-][/c]")
                self.goToButton:SetActive(false)
                self.giveupAlert:SetVisible(true, false, 0)
            elseif task:TaskCanSubmit() then
                self.taskNameLabel.text = self.taskNameLabel.text .. LocalString.GetString("  [c][14ff0e]完成[-][/c]")
            end

            local normalStatus = true
            if task.OverGrade > 0 then
                local minGrade = template.GradeCheck[0]
                if CClientMainPlayer.Inst.HasFeiSheng then
                    minGrade = template.GradeCheckFS1[0]
                end
                if CClientMainPlayer.Inst.MaxLevel >= 30 then
                    conditionText = g_MessageMgr:FormatMessage("Task_Need_Level", minGrade)
                else
                    conditionText = g_MessageMgr:FormatMessage("Task_Need_Level_Below_30", minGrade)
                end
                self.goToButton:SetActive(false)
                normalStatus = false
            elseif CLuaTaskMgr.IsBagSpaceNotEnough(task) then
                conditionText = CLuaTaskMgr.GetBagSpaceNotEnoughDescription(task)
                normalStatus = false
            end
            if CTaskMgr.Inst:IsFestivalTaskTimeOut(task) then
                if template.ShowTimeoutTips == 2 then
                    local setting_Client = GameSetting_Client.GetData()
                    conditionText = setting_Client.MainTaskReplaceTips
                else
                    conditionText = LocalString.GetString("该任务已过期，请放弃")
                end
                self.goToButton:SetActive(false)
                normalStatus = false
            end
            if normalStatus then
                if CTaskMgr.Inst:IsShenBingPreTask(templateId) then
                    conditionText = CTaskMgr.Inst:GetTaskNeedEvetsDescription(task)
                else
                    local local_ = CTaskMgr.Inst:GetTaskTrackPos(CommonDefs.DictGetValue(taskProperty.CurrentTasks, typeof(UInt32), templateId))
                    if local_ ~= nil then

                        if System.String.IsNullOrEmpty(template.Locations) then
                            conditionText = CTaskMgr.Inst:GetTaskTrackDescriptionForTaskWnd(task)
                        else
                            local trackInfo = local_.trackInfo
                            conditionText = trackInfo
                            local taskTrackDesc = CTaskMgr.Inst:GetTaskTrackDescriptionForTaskWnd(task)
                            if not System.String.IsNullOrEmpty(taskTrackDesc) then
                                conditionText = System.String.Format("{0}\n{1}", trackInfo, taskTrackDesc)
                            end
                        end
                    else
                        if task.CanSubmit == 1 and template.SubmitNPC == 0 then
                            if template.TaskLocations ~= nil and template.TaskLocations.Length >= 1 then
                                local trackInfo = template.TaskLocations[template.TaskLocations.Length - 1].trackInfo
                                conditionText = trackInfo
                            end
                        end
                    end

                    if CLuaTaskMgr.NeedCustomizeTrackInfo(templateId) then
                        conditionText = CLuaTaskMgr.GetCustomizedTrackInfo(templateId)
                    end
                end
            end

            local extraStr = task.ExtraData.StringData
            local npcName
            if extraStr and extraStr ~= "" then
                local npcId = string.match(extraStr,"sinpc:(%d+);")
                if npcId then
                    npcName = NPC_NPC.GetData(npcId).Name
                    conditionText = SafeStringFormat3(conditionText,npcName)
                end
            end

            if templateId == SectInviteNpc_Setting.GetData().VisitTaskId then
                self.descriptionLabel.text = CTaskMgr.Inst:GetTaskDescriptionSimple(templateId)
            elseif LuaInviteNpcMgr.IsFriendlinessTask(templateId) then
                local desc = CTaskMgr.Inst:GetTaskDescriptionSimple(templateId)
                local extraStr = task.ExtraData.StringData
                if npcName then
                    desc = SafeStringFormat3(desc,npcName)
                    self.descriptionLabel.text = System.String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor24(self.descriptionLabel.color), CChatLinkMgr.TranslateToNGUIText(desc,false))
                end
            else
                self.descriptionLabel.text = System.String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor24(self.descriptionLabel.color), CTaskMgr.Inst:GetTaskDescription(templateId))
            end

            if template.GamePlay == "ShenYao" then
                LuaXinBaiLianDongMgr.ExclusiveShenYao_TaskDetailLabel = self.descriptionLabel
                if LuaXinBaiLianDongMgr:ExclusiveShenYaoTaskTickFunc(true) then
                    self.goToButton:SetActive(true)
                    CommonDefs.AddOnClickListener(self.goToButton, DelegateFactory.Action_GameObject(function(go) 
                        CUIManager.CloseUI(CUIResources.TaskWnd)
                        LuaXinBaiLianDongMgr:ProcessExclusiveShenYaoTask()
                    end), false)
                else
                    self.goToButton:SetActive(false)
                    conditionText = LocalString.GetString("该任务已过期，请放弃")
                end
            end

            self.conditionLabel.text = System.String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor24(self.conditionLabel.color), CChatLinkMgr.TranslateToNGUIText(conditionText, false))
            -- 灵符任务22029597 需要特殊处理
            if string.find(template.GamePlay, "SchoolSect") or string.find(template.GamePlay, "eSectFuZhiTask") then
                CLuaTaskListItem:UpdateTaskDescriptionLabel(task,self.descriptionLabel.text,self.descriptionLabel,template.GamePlay)
                CLuaTaskListItem:UpdateTaskDescriptionLabel(task,self.conditionLabel.text,self.conditionLabel,template.GamePlay)
            end
        elseif CommonDefs.DictContains(taskProperty.AcceptableTasks, typeof(UInt32), templateId) then
            --可接任务
            isAcceptedTask = false
            self.taskNameLabel.text = template.Display

            self.descriptionLabel.text = System.String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor24(self.descriptionLabel.color), CTaskMgr.Inst:GetTaskDescription(templateId))
            self.giveUpButton:SetActive(false)
            self.goToButton:SetActive(true)
            self.taskDetailTitle:SetActive(true)
        end
    end

    self:InitCheckBox(template, isAcceptedTask)

    self.conditionSection:SetActive(not System.String.IsNullOrEmpty(conditionText))

    --#region 任务奖励
    self:ShowAwards(template, isAcceptedTask, isReviewingTask)
    --#endregion

    self.detailTable:Reposition()
    self.detailScrollView:ResetPosition()
end
function CLuaTaskDetailView:ShowAwards( t, isAcceptedTask, isReviewingTask)
    local showAwards = false

    if isAcceptedTask then
        --丁师傅需求：已接任务中不再显示任务奖励相关内容
        self.awardsSection:SetActive(showAwards)
        self.itemsTable:Reposition()
        self.itemsScrollView:ResetPosition()
        return showAwards
    end

    local itemAwards = CTaskMgr.Inst:GetTaskItemAwards(t)
    local exp = CTaskMgr.Inst:GetTaskExpAwards(t)
    local yinliang = CTaskMgr.Inst:GetTaskYinLiangAwards(t)
    local yinpiao = CTaskMgr.Inst:GetTaskYinPiaoAwards(t)
	if isReviewingTask then
		local reviewTemplate = Task_ReviewTask.GetData(t.ID)
		if reviewTemplate and reviewTemplate.UseReviewItemReward == 1 then
			itemAwards = reviewTemplate.ItemAwards
		end
		exp = 0
		yinliang = 0
		yinpiao = 0
	end

    if exp > 0 or yinliang > 0 or yinpiao > 0 then
        showAwards = true
        local builder = NewStringBuilderWraper(StringBuilder)
        if exp > 0 then
            builder:Append("#290")
            --builder.Append("x");
            CommonDefs.StringBuilder_Append_StringBuilder_UInt64(builder, exp)
        end
        if yinliang > 0 then
            if builder.Length > 0 then
                builder:Append("      ")
            end
            builder:Append("#287")
            --builder.Append("x");
            CommonDefs.StringBuilder_Append_StringBuilder_UInt64(builder, yinliang)
        end
        if yinpiao > 0 then
            if builder.Length > 0 then
                builder:Append("      ")
            end
            builder:Append("#288")
            --builder.Append("x");
            CommonDefs.StringBuilder_Append_StringBuilder_UInt64(builder, yinpiao)
        end
        self.otherAwardsLabel.text = ToStringWrap(builder)
        Extensions.SetLocalPositionY(self.itemsScrollView.transform, self.lowerItemsScrollViewPosY)
        --下移
    else
        self.otherAwardsLabel.text = nil
        Extensions.SetLocalPositionY(self.itemsScrollView.transform, self.higherItemsScrollViewPosY)
        --上移
    end
    local n = self.itemsTable.transform.childCount
    do
        local i = 0
        while i < n do
            self.itemsTable.transform:GetChild(i).gameObject:SetActive(false)
            i = i + 1
        end
    end
    if itemAwards ~= nil and itemAwards.Length > 0 then
        showAwards = true
        do
            local i = 0
            while i < itemAwards.Length do
                local templateId = itemAwards[i][0]
                local count = itemAwards[i][1]
                local go = nil
                if i < n then
                    go = self.itemsTable.transform:GetChild(i).gameObject
                else
                    go = CUICommonDef.AddChild(self.itemsTable.gameObject, self.itemTemplate)
                end
                go:SetActive(true)

                local iconTexture = CommonDefs.GetComponent_Component_Type(go.transform:Find("IconTexture"), typeof(CUITexture))
                local amountLabel = CommonDefs.GetComponent_Component_Type(go.transform:Find("AmountLabel"), typeof(UILabel))
                local qualitySprite = CommonDefs.GetComponent_Component_Type(go.transform:Find("QualitySprite"), typeof(UISprite))

                if IdPartition.IdIsItem(templateId) then
                    local item = Item_Item.GetData(templateId)
                    iconTexture:LoadMaterial(item.Icon)
                    if count > 1 then
                        amountLabel.text = tostring(count)
                    else
                        amountLabel.text = nil
                    end
                    qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
                elseif IdPartition.IdIsEquip(templateId) then
                    local equip = EquipmentTemplate_Equip.GetData(templateId)
                    iconTexture:LoadMaterial(equip.Icon)
                    if count > 1 then
                        amountLabel.text = tostring(count)
                    else
                        amountLabel.text = nil
                    end
                    qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), equip.Color))
                else
                    iconTexture:Clear()
                    amountLabel.text = nil
                    qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
                end

                UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (itemGo)
                    self:OnItemClick(itemGo, templateId)
                end)
                i = i + 1
            end
        end
    end

    self.awardsSection:SetActive(showAwards)
    self.itemsTable:Reposition()
    self.itemsScrollView:ResetPosition()

    return showAwards
end

function CLuaTaskDetailView:OnItemClick(go, templateId)
    --仅显示模板信息
    CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
end

function CLuaTaskDetailView:OnGiveUpButtonClick( go)
    local confirmMsg = CTaskMgr.Inst:GetGiveUpTaskConfirmMessage(self.templateId)
    if confirmMsg == nil then
        if self.bGiveupConfirm then
            MessageWndManager.ShowOKCancelMessage(SafeStringFormat3(LocalString.GetString("确定放弃%s?"), self.taskNameLabel.text), DelegateFactory.Action(function ()
                self:DoGiveUpTask()
            end), nil, nil, nil, false)
        else
            self:DoGiveUpTask()
        end
    else
        local message = g_MessageMgr:FormatMessage(confirmMsg)
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
            self:DoGiveUpTask()
        end), nil, nil, nil, false)
    end
end

function CLuaTaskDetailView:DoGiveUpTask()
    Gac2Gas.RequestGiveUpTask(self.templateId)
    CUIManager.CloseUI("TaskWnd")
end

function CLuaTaskDetailView:OnGoToButtonClick( go)
    local template = Task_Task.GetData(self.templateId)
    if template and template.StartTime ~= nil then
        CLuaTaskMgr.RequestTaskOpenTimeCheck(self.templateId)
    else
        CLuaTaskMgr.GoToAcceptTask(self.templateId)
    end
end
