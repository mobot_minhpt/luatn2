local QnTabButton = import "L10.UI.QnTabButton"

local PoolGridHelper = import "L10.Engine.PoolGridHelper"
local QnNewSlider = import "L10.UI.QnNewSlider"
local VirtualJoyStick = import "L10.UI.VirtualJoyStick"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local EUIModuleGroup = import "L10.UI.CUIManager+EUIModuleGroup"
local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local Physics = import "UnityEngine.Physics"
local Tags = import "L10.Game.Tags"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local GameObject = import "UnityEngine.GameObject"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local Object = import "System.Object"
local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"
local FurnitureScene = import "L10.Game.FurnitureScene"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local CSEBarrierType = import "L10.Engine.EBarrierType"
local CoreScene = import "L10.Engine.Scene.CoreScene"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local UInt32 = import "System.UInt32"
local UISprite = import "UISprite"
local CGrassFast=import "L10.Engine.CGrassFast"

LuaPoolEditWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPoolEditWnd, "CloseBtn", "CloseBtn", GameObject)
RegistChildComponent(LuaPoolEditWnd, "SaveBtn", "SaveBtn", GameObject)
RegistChildComponent(LuaPoolEditWnd, "PoolAreaLabel", "PoolAreaLabel", UILabel)
RegistChildComponent(LuaPoolEditWnd, "BgTexture", "BgTexture", GameObject)
RegistChildComponent(LuaPoolEditWnd, "NotEnoughTip", "NotEnoughTip", GameObject)
RegistChildComponent(LuaPoolEditWnd, "AddPoolBtn", "AddPoolBtn", GameObject)
RegistChildComponent(LuaPoolEditWnd, "TipLabel", "TipLabel", UILabel)
RegistChildComponent(LuaPoolEditWnd, "VirtualJoyStick", "VirtualJoyStick", VirtualJoyStick)
RegistChildComponent(LuaPoolEditWnd, "TableView", "TableView", GameObject)
RegistChildComponent(LuaPoolEditWnd, "CameraSlider", "CameraSlider", QnNewSlider)
RegistChildComponent(LuaPoolEditWnd, "BrushNode", "BrushNode", GameObject)
RegistChildComponent(LuaPoolEditWnd, "BrushSlider", "BrushSlider", QnNewSlider)
RegistChildComponent(LuaPoolEditWnd, "BrushPreview", "BrushPreview", UISprite)
RegistChildComponent(LuaPoolEditWnd, "MenueView", "MenueView", GameObject)
RegistChildComponent(LuaPoolEditWnd, "MenueBtn", "MenueBtn", QnTabButton)
--@endregion RegistChildComponent end
RegistClassMember(LuaPoolEditWnd, "BrushThumb")
RegistClassMember(LuaPoolEditWnd, "CameraThumb")
RegistClassMember(LuaPoolEditWnd, "m_TerrainTypes")
RegistClassMember(LuaPoolEditWnd, "m_TableView")
RegistClassMember(LuaPoolEditWnd, "m_PoolList")
RegistClassMember(LuaPoolEditWnd, "m_TerrainFast")
RegistClassMember(LuaPoolEditWnd, "m_SelectType")
RegistClassMember(LuaPoolEditWnd, "m_MapWidth")
RegistClassMember(LuaPoolEditWnd, "m_CurPoolCount")
RegistClassMember(LuaPoolEditWnd, "m_MaxPoolCount")
RegistClassMember(LuaPoolEditWnd, "m_CurStoneCount")
RegistClassMember(LuaPoolEditWnd, "m_NeedStoneCount")
RegistClassMember(LuaPoolEditWnd, "m_KuoZhangLevel")
RegistClassMember(LuaPoolEditWnd, "m_IsPoolNumEnough")
RegistClassMember(LuaPoolEditWnd, "m_IsShiCaiEnough")
RegistClassMember(LuaPoolEditWnd, "m_YardDecorationPrivalege")
RegistClassMember(LuaPoolEditWnd, "m_LastClickPos")

RegistClassMember(LuaPoolEditWnd, "m_Brush")
RegistClassMember(LuaPoolEditWnd, "m_MinBrushSize")

function LuaPoolEditWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.CloseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseBtnClick()
	end)

	UIEventListener.Get(self.SaveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSaveBtnClick()
	end)

	UIEventListener.Get(self.AddPoolBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddPoolBtnClick()
	end)

    --@endregion EventBind end
    UIEventListener.Get(self.MenueBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(g)
        self:OnMenueBtnClick()
    end)
    self.m_TerrainTypes = {
        LocalString.GetString("温泉池"),
        LocalString.GetString("水池"),
        LocalString.GetString("平地")
    }

    self.m_PoolChunkIndexList = {}
    self.m_SelectType = 1
    self.m_IsMingYuan = CClientFurnitureMgr.Inst.m_IsMingYuan
    self.m_TableView = self.TableView:GetComponent(typeof(QnTableView))
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.m_TerrainTypes
        end,
        function(item,row)
            self:InitItem(item,row+1)
        end)
    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        if row == 0 and not self.m_IsMingYuan then
            self.m_TableView:SetSelectRow(self.m_SelectType-1,true)
        else
            self.m_SelectType = row+1
        end
        if self.m_SelectType == 1 then
            self.TipLabel.text = LocalString.GetString("摇杆移动视野，点击要修建温泉池的地面")
        elseif self.m_SelectType == 2 then
            self.TipLabel.text = LocalString.GetString("摇杆移动视野，点击要修建水池的地面")
        elseif self.m_SelectType == 3 then
            self.TipLabel.text = LocalString.GetString("摇杆移动视野，点击要修建平地的水池")
        end
        for i=1,#self.m_TerrainTypes,1 do
            local item = self.m_TableView:GetItemAtRow(i-1)
            local selected = item.transform:Find("Selected"):GetComponent(typeof(UISprite))
            local alpha = i == self.m_SelectType and 1 or 0
            selected.alpha = alpha
        end
    end)
    self.m_TableView:SetSelectRow(1,true)
    if not CRenderScene.Inst then
        return 
    end
    self.m_TerrainFast = CRenderScene.Inst.TerrainFast
    self.m_MapWidth = CRenderScene.Inst.MapWidth

    local setting = House_Setting.GetData()

    self.m_BasePoolCount = self.m_IsMingYuan and setting.MingYuanBasePoolGridNum or setting.BasePoolGridNum
    self.m_MaxPoolCount = CClientHouseMgr.Inst.mCurFurnitureProp2.PoolGridNum + self.m_BasePoolCount
    self.m_CurStoneCount = CClientHouseMgr.Inst.mConstructProp.Stone
    self.m_CurPoolCount = LuaPoolMgr.CurPoolCount

    local OnDragJoyStick = function(dir)
		self:OnDragJoyStick(dir)
	end
	local OnDragFinished = function()
		self:OnDragFinished()
	end
	CommonDefs.AddVSOnDraggingListener(self.VirtualJoyStick.gameObject,DelegateFactory.Action_Vector3(OnDragJoyStick),true)
	CommonDefs.AddVSOnDragFinishListener(self.VirtualJoyStick.gameObject,DelegateFactory.Action(OnDragFinished),true)
    self.CameraSlider.OnValueChanged = DelegateFactory.Action_float(function(value)
        self:OnCameraSliderChanged(value)
	end)
    self.BrushSlider.OnValueChanged = DelegateFactory.Action_float(function(value)
        self:OnBrushSliderChanged(value)
	end)

    self.BrushThumb = self.BrushSlider.transform:GetComponent(typeof(UISlider)).thumb.gameObject
    self.CameraThumb = self.CameraSlider.transform:GetComponent(typeof(UISlider)).thumb.gameObject
    UIEventListener.Get(self.BrushThumb).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        if isPressed then
            if not self.BrushNode.activeSelf then
                self.BrushNode:SetActive(true)
            end
            self.m_IsDraging = true
        else
            self.BrushNode:SetActive(false)
            self.m_IsDraging = false
        end
    end)
    UIEventListener.Get(self.CameraThumb).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        self.m_IsDraging = isPressed
    end)

    self.m_NeedStoneCount = 0

    if LuaPoolMgr.CameraGo and LuaPoolMgr.CameraGo.transform then
        local pos = LuaPoolMgr.CameraGo.transform.localPosition
        if pos.x > LuaPoolMgr.Basex + LuaPoolMgr.MaxWidth  then
            pos.x = LuaPoolMgr.Basex + LuaPoolMgr.MaxWidth
        elseif pos.x < LuaPoolMgr.Basex then
            pos.x = LuaPoolMgr.Basex
        end
        if pos.z > LuaPoolMgr.Basey + LuaPoolMgr.MaxHeight then
            pos.z = LuaPoolMgr.Basey + LuaPoolMgr.MaxHeight
        elseif pos.z < LuaPoolMgr.Basey then
            pos.z = LuaPoolMgr.Basey
        end
        LuaPoolMgr.CameraGo.transform.localPosition = pos
    end
    self.m_KuoZhangLevel = 1

    self.m_IsPoolNumEnough = true
    self.m_IsShiCaiEnough = true
    self.m_YardDecorationPrivalege = false
    self.m_LastClickPos = nil

    self.m_Brush = self.transform:Find("Brush")
    self.m_Brush.gameObject:SetActive(false)
    self.BrushNode:SetActive(false)
    self.m_MinBrushSize = self.BrushPreview.width
end

function LuaPoolEditWnd:OnMenueBtnClick()
    local selected = not self.MenueBtn.Selected
    self.MenueBtn.Selected = selected
    local sprite = self.MenueBtn.transform:GetComponent(typeof(UISprite))
    if selected then
        self.MenueView:SetActive(false)
        if sprite then
            sprite.alpha = 1
        end
    else
        self.MenueView:SetActive(true)
        if sprite then
            sprite.alpha = 0.5
        end
    end
end

function LuaPoolEditWnd:OnCameraSliderChanged(value)
    local camera = LuaPoolMgr.CameraGo
    if camera and camera.transform then
        local pos = camera.transform.localPosition
        pos.y = LuaPoolMgr.MinCameraY + (LuaPoolMgr.MaxCameraY-LuaPoolMgr.MinCameraY) * value
        camera.transform.localPosition = pos
    end
end

function LuaPoolEditWnd:OnBrushSliderChanged(value)
    --self.BrushNode:SetActive(true)
    local size = self.m_MinBrushSize * (1+value)
    self.BrushPreview.width = size
    self.BrushPreview.height = size
end

function LuaPoolEditWnd:OnDragJoyStick(dir)
    self.m_IsDraging = true
	local camera = LuaPoolMgr.CameraGo
    if camera and camera.transform then
        local pos = camera.transform.localPosition
        pos.z = pos.z - dir.x*0.1
        pos.x = pos.x + dir.y*0.1
        if pos.x > LuaPoolMgr.Basex + LuaPoolMgr.MaxWidth  then
            pos.x = LuaPoolMgr.Basex + LuaPoolMgr.MaxWidth
        elseif pos.x < LuaPoolMgr.Basex then
            pos.x = LuaPoolMgr.Basex
        end
        if pos.z > LuaPoolMgr.Basey + LuaPoolMgr.MaxHeight then
            pos.z = LuaPoolMgr.Basey + LuaPoolMgr.MaxHeight
        elseif pos.z < LuaPoolMgr.Basey then
            pos.z = LuaPoolMgr.Basey
        end
        camera.transform.localPosition = pos
    end
end

function LuaPoolEditWnd:OnDragFinished()
	self.m_IsDraging = false
end

function LuaPoolEditWnd:Init()
    --进入场景先去掉水的clip
    local terrainFast = CRenderScene.Inst.TerrainFast
    CommonDefs.DictClear(terrainFast.clipDict)
    terrainFast:GenerateTerrainMesh()


    self.m_TableView:ReloadData(false, false)
    self:InitPoolCountLabel()
    if not CClientHouseMgr.Inst or not CClientHouseMgr.Inst.mCurConstruectProp then
        return
    end
    local zhengwuGrade = CClientHouseMgr.Inst.mCurConstruectProp.ZhengwuGrade
    local zhengwudata = House_ZhengwuUpgrade.GetData(zhengwuGrade)
    self.m_KuoZhangLevel = zhengwudata.KuozhangLevel
    --检查房客权限
    if CClientHouseMgr.Inst:IsCurHouseRoomer() then
        self.m_YardDecorationPrivalege = false
        if CClientMainPlayer.Inst then
            Gac2Gas.QueryRoomerPrivalegeInfo(CClientMainPlayer.Inst.Id)
        end
    else
        self.m_YardDecorationPrivalege = true
    end
    self.BrushNode:SetActive(false)
    local sprite = self.MenueBtn.transform:GetComponent(typeof(UISprite))
    if sprite then
        sprite.alpha = 0.5
    end
end

function LuaPoolEditWnd:InitItem(item,row)
    local name = self.m_TerrainTypes[row]
    local nameLabel = item.transform:Find("Name"):GetComponent(typeof(UILabel))
    local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local mask = item.transform:Find("Icon/Mask").gameObject
    local path = "UI/Texture/Item_House/Material/"
    local clearBtn = item.transform:Find("ClearBtn").gameObject
    mask:SetActive(false)
    clearBtn:SetActive(false)
    UIEventListener.Get(clearBtn).onClick = nil
    if row == 1 then
        path = path.."jiayuan_wenquanchi.mat"
        mask:SetActive(not self.m_IsMingYuan)
    elseif row == 2 then
        path = path.."jiayuan_shuichi.mat"
    elseif row == 3 then
        path = path.."jiayuan_caodi.mat"
        clearBtn:SetActive(true)
        local msg = g_MessageMgr:FormatMessage("Clear_AllPoolToLand_Confirm")
        UIEventListener.Get(clearBtn).onClick = DelegateFactory.VoidDelegate(function (go)
            MessageWndManager.ShowOKCancelMessage(msg,
            DelegateFactory.Action(function ()
                self:ClearAllOpenPoolAndWarmPool()
            end),
            nil,
            LocalString.GetString("确定"), LocalString.GetString("取消"), false)
        end)
    end
    local tip = item.transform:Find("Name (1)").gameObject
    tip:SetActive(row==1)
    icon:LoadMaterial(path)
    nameLabel.text = name
end

--填平开放区域的所有水池和温泉池
function LuaPoolEditWnd:ClearAllOpenPoolAndWarmPool()
    if not self.m_YardDecorationPrivalege then
        g_MessageMgr:ShowMessage("House_Privalege_Not_Enough")
        return
    end
    if not CRenderScene.Inst then
        return 
    end

    local x,y
    for key,val in pairs(LuaPoolMgr.TerrianTypeList) do
        if not val then val = LuaPoolMgr.LandType end
        if val and val ~= LuaPoolMgr.LandType then
            y = math.floor(key / self.m_MapWidth)
            x = key % self.m_MapWidth

            local canSetLand = self:CheckCanSetLand(nil,true,x,y)
            if canSetLand then
                LuaPoolMgr.TerrianTypeList[key] = LuaPoolMgr.LandType     
            end
        end
    end

    local terrainFast = CRenderScene.Inst.TerrainFast
    CommonDefs.DictClear(terrainFast.clipDict)

    self:CheckEditorChange(false)
    Gac2Gas.RequestSetPoolBegin()
    local isNoLand = not LuaPoolMgr.m_LandList or not next(LuaPoolMgr.m_LandList)
    local isNoWarmLand = not LuaPoolMgr.m_WarmLandList or not next(LuaPoolMgr.m_WarmLandList)
    if isNoLand or isNoWarmLand then
        LuaPoolMgr.LoadPreClipData()
    end
    if isNoLand then
        LuaPoolMgr.GenBorder(0)
    else
        self:OnSetNormalPool(3)
    end
    if isNoWarmLand then
        LuaPoolMgr.GenBorder(1)
    else
        self:OnSetNormalPool(4)
    end
    if isNoLand or isNoWarmLand then
        LuaPoolMgr.UpdatePoolGridHelper()
        self.m_CurPoolCount = LuaPoolMgr.CurPoolCount
        self:InitPoolCountLabel()
    end
    Gac2Gas.RequestSetPoolEnd()
end

function LuaPoolEditWnd:InitPoolCountLabel()
    if self.m_CurPoolCount > self.m_MaxPoolCount then
        self.m_IsPoolNumEnough = false
        self.PoolAreaLabel.text = SafeStringFormat3(LocalString.GetString("水域面积 [c][FF0000]%d[-][/c]/%d"),self.m_CurPoolCount,self.m_MaxPoolCount)
    else
        self.m_IsPoolNumEnough = true
        self.PoolAreaLabel.text = SafeStringFormat3(LocalString.GetString("水域面积 %d/%d"),self.m_CurPoolCount,self.m_MaxPoolCount)
    end
    local addCount = self.m_CurPoolCount - self.m_MaxPoolCount
    if addCount < 0 then 
        addCount = 0 
    end
    self.m_NeedStoneCount = addCount *  House_Setting.GetData().PoolPricePerGrid
    self.m_CurStoneCount = CClientHouseMgr.Inst.mConstructProp.Stone
    self.NotEnoughTip:SetActive(self.m_NeedStoneCount > 0 and self.m_NeedStoneCount > self.m_CurStoneCount)
    self.m_IsShiCaiEnough = self.m_NeedStoneCount <= 0 or self.m_NeedStoneCount <= self.m_CurStoneCount
end

--@region UIEvent

function LuaPoolEditWnd:OnTouchBgClick()
    self:SetPool(Input.mousePosition)
end

function LuaPoolEditWnd:OnSetNormalPool(type,onlyCheak)
    if not CRenderScene.Inst then
        return 
    end
    local terrainFast = CRenderScene.Inst.TerrainFast
    local count = 0
    if type == 1 then
        count = #LuaPoolMgr.m_WarmPoolList
    elseif type == 2 then
        count = #LuaPoolMgr.m_PoolList
    elseif type == 3 then
        count = #LuaPoolMgr.m_LandList
    elseif type == 4 then
        count = #LuaPoolMgr.m_WarmLandList
    end
    if count == 0 then return end
	local pointsPerSeg = 60
    local rpcCount = math.floor(count/pointsPerSeg/4)+1
    local listTbl = {}
    local list = CreateFromClass(MakeGenericClass(List, Object))
    local listx = CreateFromClass(MakeGenericClass(List, UInt32))
    local listy = CreateFromClass(MakeGenericClass(List, UInt32))

    for i=1,count,pointsPerSeg do
		list:Clear()
		listx:Clear()
		listy:Clear()
        for j=0,pointsPerSeg-1,1 do
            local index
            local isSameType = false
            local isClip = false
            if type == 1 then
                index = LuaPoolMgr.m_WarmPoolList[i+j]
            elseif type == 2 then
                index = LuaPoolMgr.m_PoolList[i+j]
            elseif type == 3 then
                index = LuaPoolMgr.m_LandList[i+j]
            elseif type == 4 then
                index = LuaPoolMgr.m_WarmLandList[i+j]
            end
            if index then
                local z = math.floor(index / self.m_MapWidth)
                local x = index % self.m_MapWidth      
                CommonDefs.ListAdd(listx, typeof(UInt32), math.floor(x))
                CommonDefs.ListAdd(listy, typeof(UInt32), math.floor(z))
            end
        end
        CommonDefs.ListAdd(list, typeof(Object), CommonDefs.ListConvert(listx))
        CommonDefs.ListAdd(list, typeof(Object), CommonDefs.ListConvert(listy))
        table.insert(listTbl,MsgPackImpl.pack(list))
    end

    local x = CreateFromClass(MakeGenericClass(List, Object))
    local y = CreateFromClass(MakeGenericClass(List, Object))
    local empty = CreateFromClass(MakeGenericClass(List, Object))
    CommonDefs.ListAdd(empty, typeof(Object), x)
    CommonDefs.ListAdd(empty, typeof(Object), y)
    local u = MsgPackImpl.pack(empty)

    if not onlyCheak then
        for i=0,rpcCount-1,1 do
            local arg1 = listTbl[i*4+1] and listTbl[i*4+1] or u
            local arg2 = listTbl[i*4+2] and listTbl[i*4+2] or u
            local arg3 = listTbl[i*4+3] and listTbl[i*4+3] or u
            local arg4 = listTbl[i*4+4] and listTbl[i*4+4] or u

            if type == 1 then
                Gac2Gas.RequestSetWarmPool(arg1,arg2,arg3,arg4)
            elseif type == 2 then
                Gac2Gas.RequestSetPool(arg1,arg2,arg3,arg4)
            elseif type == 3 then
                Gac2Gas.RequestDelPool(arg1,arg2,arg3,arg4)
            elseif type == 4 then
                Gac2Gas.RequestDelWarmPool(arg1,arg2,arg3,arg4)
            end
        end
    end
end

function LuaPoolEditWnd:OnSaveBtnClick()
    if not CRenderScene.Inst then
        return 
    end
    local terrainFast = CRenderScene.Inst.TerrainFast
    CommonDefs.DictClear(terrainFast.clipDict)
    
    if not self.m_IsPoolNumEnough and self.m_IsShiCaiEnough then
        LuaPoolMgr.m_BuyPoolNeedSave = true
        LuaPoolMgr.OpenBuyPoolComfirmWnd(self.m_NeedStoneCount, self.m_CurPoolCount,self.m_CurPoolCount - self.m_MaxPoolCount)
    elseif not self.m_IsPoolNumEnough then
        g_MessageMgr:ShowMessage("NotEnoughShiCai_To_SavePool")
    else
        self:CheckEditorChange(false)----
        Gac2Gas.RequestSetPoolBegin()
        self:OnSetNormalPool(3)
        self:OnSetNormalPool(2)
        
        if self.m_IsMingYuan then
            self:OnSetNormalPool(4)
            self:OnSetNormalPool(1)
        end
        Gac2Gas.RequestSetPoolEnd()
    end      
end

function LuaPoolEditWnd:OnAddPoolBtnClick()
    if CClientHouseMgr.Inst:IsCurHouseRoomer() then
        g_MessageMgr:ShowMessage("Pool_Edit_Fangke_Cannot_Do")
        return
    end
    CUIManager.ShowUI(CLuaUIResources.AddPoolMaxCountWnd)
end


function LuaPoolEditWnd:OnCloseBtnClick()
    local msg = g_MessageMgr:FormatMessage("Close_PoolEditor_Confim")
    self:CheckEditorChange(true)
    if LuaPoolMgr.IsEditChanged then
        MessageWndManager.ShowOKCancelMessage(msg,
        DelegateFactory.Action(function ()
            CUIManager.CloseUI(CLuaUIResources.PoolEditWnd)
        end),
        nil,
        LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    else
        CUIManager.CloseUI(CLuaUIResources.PoolEditWnd)
    end
end

function LuaPoolEditWnd:CheckEditorChange(notSave)
    LuaPoolMgr.IsEditChanged = false
    LuaPoolMgr.m_PoolList = {}
    LuaPoolMgr.m_LandList = {}
    LuaPoolMgr.m_WarmPoolList = {}
    LuaPoolMgr.m_WarmLandList = {}
    for key,val in pairs(LuaPoolMgr.TerrianTypeList) do
        local server = LuaPoolMgr.ServerTerrianTypeList[key]
        if not val then val = LuaPoolMgr.LandType end
        if not server then server = LuaPoolMgr.LandType end
        if server ~= val then
            LuaPoolMgr.IsEditChanged = true
            if not notSave then
                --水池
                if val == LuaPoolMgr.PoolType then
                    table.insert(LuaPoolMgr.m_PoolList,key)
                    --如果原来是温泉池 需要把温泉填平
                    if server == LuaPoolMgr.WarmPoolType then
                        table.insert(LuaPoolMgr.m_WarmLandList,key)
                    end
                --温泉
                elseif val == LuaPoolMgr.WarmPoolType then
                    table.insert(LuaPoolMgr.m_WarmPoolList,key)
                    --如果原来是水池 需要把水池填平
                    if server == LuaPoolMgr.PoolType then
                        table.insert(LuaPoolMgr.m_LandList,key)
                    end
                --平地
                else
                    if server == LuaPoolMgr.PoolType then
                        table.insert(LuaPoolMgr.m_LandList,key)
                    end
                    if server == LuaPoolMgr.WarmPoolType then
                        table.insert(LuaPoolMgr.m_WarmLandList,key)
                    end
                end  
            end          
        end
    end
end


--@endregion UIEvent
function LuaPoolEditWnd:OnEnable()
    local excepts = {"MiddleNoticeCenter"}
	local List_String = MakeGenericClass(List,cs_string)
	CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
    CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)
    g_ScriptEvent:AddListener("UpdateHousePoolGridNum", self, "OnUpdateHousePoolGridNum")
    g_ScriptEvent:AddListener("OnUpdateHouseResource", self, "OnUpdateHousePoolGridNum")
    g_ScriptEvent:AddListener("OnSendHouseData", self, "OnUpdateHousePoolGridNum")
    g_ScriptEvent:AddListener("QueryRoomerPrivalegeInfoResult", self, "OnQueryRoomerPrivalegeInfoResult")
    g_ScriptEvent:AddListener("UpdateAllKindPool", self, "OnClearPool")
end

function LuaPoolEditWnd:OnDisable()
    local excepts = {"MiddleNoticeCenter"}
	local List_String = MakeGenericClass(List,cs_string)
	CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
    CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)
    g_ScriptEvent:RemoveListener("UpdateHousePoolGridNum", self, "OnUpdateHousePoolGridNum")
    g_ScriptEvent:RemoveListener("OnUpdateHouseResource", self, "OnUpdateHousePoolGridNum")
    g_ScriptEvent:RemoveListener("OnSendHouseData", self, "OnUpdateHousePoolGridNum")
    g_ScriptEvent:RemoveListener("QueryRoomerPrivalegeInfoResult", self, "OnQueryRoomerPrivalegeInfoResult")
    g_ScriptEvent:RemoveListener("UpdateAllKindPool", self, "OnClearPool")
    Gac2Gas.RequestCancelEditPool()

    LuaPoolMgr.QuitPoolEditer()
    LuaPoolMgr.m_PoolList = {}
    LuaPoolMgr.m_WarmPoolList = {}
end

function LuaPoolEditWnd:OnQueryRoomerPrivalegeInfoResult(args)
    local opInfo = args[5]
    local roomerId = args[1]
    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id
    if myId==roomerId then
        self.m_YardDecorationPrivalege = false
        if opInfo then
            do
                local i = 0
                while i < opInfo.Count do
                    local idx = math.floor(tonumber(opInfo[i] or 0))
                    local v = math.floor(tonumber(opInfo[i + 1] or 0))
                    if idx == EHouseEditableOperation_lua.OpYardFurniture and v ~= 0 then
                        self.m_YardDecorationPrivalege = true
                        break
                    end
                    i = i + 2
                end
            end 
        end
    end
    -- self.m_TableView:ReloadData(false, false)
end

function LuaPoolEditWnd:OnSetBrush()
    self.m_Brush.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
    self.m_Brush:GetComponent(typeof(UISprite)).width = self.BrushPreview.width
    self.m_Brush:GetComponent(typeof(UISprite)).height = self.BrushPreview.height
    --self.m_Brush.position = LuaPoolMgr.PoolEditorCamera:ScreenToWorldPoint(Input.mousePosition)
    
end

function LuaPoolEditWnd:Update()
    if Input.GetMouseButtonDown(0) then 
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           CUICommonDef.IsChild(self.BgTexture.transform, UICamera.lastHit.collider.transform)))then 
            self.m_Brush.gameObject:SetActive(true)
            if self.m_SelectType == 1 then --温泉
                self:SetPool()
            elseif self.m_SelectType == 2 then
                self:SetPool()
            elseif self.m_SelectType == 3 then
                self:SetLand()
            end
        end
        return
    end
    --test 连续
    if Input.GetMouseButton(0) then
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           CUICommonDef.IsChild(self.BgTexture.transform, UICamera.lastHit.collider.transform))) then
            if self.m_SelectType == 1 then
                self:SetPool()
            elseif self.m_SelectType == 2 then
                self:SetPool()
            elseif self.m_SelectType == 3 then
                self:SetLand()
            end
        end
    end

    if Input.GetMouseButtonUp(0) then
        self.m_LastClickPos = nil
        self.m_Brush.gameObject:SetActive(false)
    end
end

function LuaPoolEditWnd:SetPool(isLand)
    self:OnSetBrush()
    if not CRenderScene.Inst then
        return 
    end
    local terrainFast = CRenderScene.Inst.TerrainFast
    local ray = LuaPoolMgr.PoolEditorCamera:ScreenPointToRay(Input.mousePosition)
    local dist = LuaPoolMgr.PoolEditorCamera.farClipPlane - LuaPoolMgr.PoolEditorCamera.nearClipPlane

    local wpos = self.m_Brush.position
    local lpos = self.m_Brush.localPosition
    local lpos1 = Vector3(lpos.x - self.BrushPreview.width/2,lpos.y,lpos.z)
    local lpos2 = Vector3(lpos.x + self.BrushPreview.width/2,lpos.y,lpos.z)
    local lpos3 = Vector3(lpos.x , lpos.y-self.BrushPreview.height/2 , lpos.z)
    local lpos4 = Vector3(lpos.x , lpos.y+self.BrushPreview.height/2 , lpos.z)

    local wpos1 = self.m_Brush.parent:TransformPoint(lpos1)
    local wpos2 = self.m_Brush.parent:TransformPoint(lpos2)
    local wpos3 = self.m_Brush.parent:TransformPoint(lpos3)
    local wpos4 = self.m_Brush.parent:TransformPoint(lpos4)


    local spos1 = CUIManager.UIMainCamera:WorldToScreenPoint(wpos1)
    local spos2 = CUIManager.UIMainCamera:WorldToScreenPoint(wpos2)
    local spos3 = CUIManager.UIMainCamera:WorldToScreenPoint(wpos3)
    local spos4 = CUIManager.UIMainCamera:WorldToScreenPoint(wpos4)

    local ray1 = LuaPoolMgr.PoolEditorCamera:ScreenPointToRay(spos1)
    local ray2 = LuaPoolMgr.PoolEditorCamera:ScreenPointToRay(spos2)
    local ray3 = LuaPoolMgr.PoolEditorCamera:ScreenPointToRay(spos3)
    local ray4 = LuaPoolMgr.PoolEditorCamera:ScreenPointToRay(spos4)
    local hitsarr = {}
    local hits1 = Physics.RaycastAll(ray1, dist, LuaPoolMgr.PoolEditorCamera.cullingMask)
    local hits2 = Physics.RaycastAll(ray2, dist, LuaPoolMgr.PoolEditorCamera.cullingMask)
    local hits3 = Physics.RaycastAll(ray3, dist, LuaPoolMgr.PoolEditorCamera.cullingMask)
    local hits4 = Physics.RaycastAll(ray4, dist, LuaPoolMgr.PoolEditorCamera.cullingMask)
    hitsarr = {hits1,hits2,hits3,hits4}
    local hitObjPos = {}
    for i=1,#hitsarr,1 do
        local hits = hitsarr[i]
        for i=0,hits.Length-1 do
            local pos
            local canSet = true
            local collider = hits[i].collider.gameObject
            local name = collider.gameObject.name
        
            if collider:CompareTag(Tags.Ground) and StringStartWith(name,"TerrainChunk") then
                pos = hits[i].point
            elseif string.find(name,"Water") then
                pos = hits[i].point
            end
            if pos then
                table.insert(hitObjPos,pos)
            end
        end
    end
    local minx,maxx,minz,maxz
    for i,objPos in ipairs(hitObjPos) do
        minx = minx and minx or objPos.x
        minx = math.min(minx,objPos.x)
        maxx = maxx and maxx or objPos.x 
        maxx = math.max(maxx,objPos.x)

        minz = minz and minz or objPos.z
        minz = math.min(minz,objPos.z)
        maxz = maxz and maxz or objPos.z 
        maxz = math.max(maxz,objPos.z)
    end

    minx = math.floor(minx + 0.5)
    maxx = math.floor(maxx + 0.5)
    minz = math.floor(minz + 0.5)
    maxz = math.floor(maxz + 0.5)
    for x = minx,maxx-1,1 do
        for z = minz,maxz-1,1 do
            if not isLand then
                self:SetPoolAt(Vector3(x,0,z))
            else
                self:SetLandAt(Vector3(x,0,z))
            end
            
        end
    end
    
end

--填平地
function LuaPoolEditWnd:SetLand()
    self:SetPool(true)
end

function LuaPoolEditWnd:SetLandAt(pos)
    local t = {}
    t.position = {
        x = math.floor(pos.x),
        z = math.floor(pos.z),
    }

    local terrainFast = CRenderScene.Inst.TerrainFast
    local chunkInfo = LuaPoolMgr.GetChunkInfoFromPos(t.position.x,t.position.z,terrainFast)
    if not chunkInfo then  return end
    t.cgridX = chunkInfo.cgridX
    t.cgridY = chunkInfo.cgridY
    t.chunkIndex = chunkInfo.chunkIndex
    if not self:CheckCanSetLand(chunkInfo) then
        return
    end

    if LuaPoolMgr.IsPoolAt(t.position.x,t.position.z,LuaPoolMgr.PoolType_Pool) then
        LuaPoolMgr.TerrianTypeList[t.position.z*self.m_MapWidth + t.position.x] = LuaPoolMgr.LandType
        LuaPoolMgr.GenBorder(0,true,t.position.x,t.position.z)
    elseif LuaPoolMgr.IsPoolAt(t.position.x,t.position.z,LuaPoolMgr.PoolType_WarmPool) then
        LuaPoolMgr.TerrianTypeList[t.position.z*self.m_MapWidth + t.position.x] = LuaPoolMgr.LandType
        LuaPoolMgr.GenBorder(1,true,t.position.x,t.position.z)
    end
    --LuaPoolMgr.TerrianTypeList[t.position.z*self.m_MapWidth + t.position.x] = LuaPoolMgr.LandType

    local basex,basey = PoolGridHelper.Inst.m_Basex,PoolGridHelper.Inst.m_Basey
    local x , y=t.position.x-basex,t.position.z-basey
    PoolGridHelper.Inst:SetPixelColor(x,y,PoolGridHelper.s_LandColor)
    LuaPoolMgr.TrySetIce(x,y,false)

    self.m_CurPoolCount = self.m_CurPoolCount - 1
    self:InitPoolCountLabel()
    --LuaPoolMgr.IsEditChanged = true
end

function LuaPoolEditWnd:CheakJiaYuanKuaZhang(xx,zz)
    local kuozhangLvl = self.m_KuoZhangLevel
    local CheckKuozhangLvl = nil

	local notExtendedMask = 0
	local extOrders = {ERoadInfo.eRI_PLAY2, ERoadInfo.eRI_UNUSED6, ERoadInfo.eRI_HUNTING, ERoadInfo.eRI_UNUSED8, ERoadInfo.eRI_UNUSED9}
	for lvl = 1, 5 do
		if (kuozhangLvl-1) < lvl then
			notExtendedMask = bit.bor(notExtendedMask, bit.lshift(1, extOrders[lvl]))
		end
	end

	CheckKuozhangLvl = function(t)
		return bit.band(notExtendedMask, t) == 0
	end
    local res = FurnitureScene.GetOriginalGridBarrierAndInfo(xx, zz)
    if res and res[1] then
        local t = res[1]
        if CheckKuozhangLvl and not CheckKuozhangLvl(res[1]) then
            return false
        end
    end
    return true
end

function LuaPoolEditWnd:CheckCanSetLand(chunkInfo,ignoreMessage,x,y)
    if self.m_IsDraging then
        return false 
    end
    if not self.m_YardDecorationPrivalege then
        if not ignoreMessage then
            g_MessageMgr:ShowMessage("House_Privalege_Not_Enough")
        end
        return false
    end
    local y = chunkInfo and chunkInfo.position.z or y
    local x = chunkInfo and chunkInfo.position.x or x
    local type = LuaPoolMgr.TerrianTypeList[y*self.m_MapWidth + x]
    if type ~= LuaPoolMgr.PoolType and type ~= LuaPoolMgr.WarmPoolType then
        return false
    end

    local index = y * self.m_MapWidth + x
    if x > LuaPoolMgr.Basex + LuaPoolMgr.MaxWidth or x < LuaPoolMgr.Basex or y > LuaPoolMgr.Basey + LuaPoolMgr.MaxHeight or y < LuaPoolMgr.Basey then
        if not ignoreMessage then
            g_MessageMgr:ShowMessage("Cant_EditPool_Here")
        end
        return false
    end
    if not self:CheakJiaYuanKuaZhang(x,y) then
        if not ignoreMessage then
            g_MessageMgr:ShowMessage("Cant_EditPool_Here")
        end
        return false
    end

    if CoreScene.MainCoreScene:GetBarriervIgnorePoolBarrier(x, y, false) ~= CSEBarrierType.eBT_NoBarrier then
        if not ignoreMessage then
            g_MessageMgr:ShowMessage("Cant_EditPool_With_Barrier")
        end
        return false
    end

    if g_FurnitureScene and g_FurnitureScene:CanSetPool(x,y) then
        if not ignoreMessage then 
            g_MessageMgr:ShowMessage("EditPool_RemoveFurniture_Furst")
        end
        return false
    end
    if CClientFurnitureMgr.Inst:IsSpecialPoolAt(x,y) then
        if not ignoreMessage then
            g_MessageMgr:ShowMessage("EditPool_RemoveFurniture_Furst")
        end
        return false
    end
    return true
end

function LuaPoolEditWnd:SetPoolAt(pos)
    local t = {}
    t.position = {
        x = math.floor(pos.x),
        z = math.floor(pos.z),
    }
    local terrainFast = CRenderScene.Inst.TerrainFast
    local chunkInfo = LuaPoolMgr.GetChunkInfoFromPos(t.position.x,t.position.z,terrainFast)
    if not chunkInfo then  return end
    t.cgridX = chunkInfo.cgridX
    t.cgridY = chunkInfo.cgridY
    t.chunkIndex = chunkInfo.chunkIndex
    if not self:CheckCanSetPool(chunkInfo) then
        return
    end

    if self.m_SelectType == 1 then
        LuaPoolMgr.TerrianTypeList[t.position.z*self.m_MapWidth + t.position.x] = LuaPoolMgr.WarmPoolType
        table.insert(LuaPoolMgr.m_WarmPoolPointList,t.position.x)--
		table.insert(LuaPoolMgr.m_WarmPoolPointList,t.position.z)--
        LuaPoolMgr.GenBorder(1,true,t.position.x,t.position.z)

		local basex,basey = PoolGridHelper.Inst.m_Basex,PoolGridHelper.Inst.m_Basey
        local x , y=t.position.x-basex,t.position.z-basey
        PoolGridHelper.Inst:SetPixelColor(x,y,PoolGridHelper.s_WaterColor)
        LuaPoolMgr.TrySetIce(x,y,true)
    elseif self.m_SelectType == 2 then
        LuaPoolMgr.TerrianTypeList[t.position.z*self.m_MapWidth + t.position.x] = LuaPoolMgr.PoolType
        table.insert(LuaPoolMgr.m_PoolPointList,t.position.x)--
		table.insert(LuaPoolMgr.m_PoolPointList,t.position.z)--
        LuaPoolMgr.GenBorder(0,true,t.position.x,t.position.z)

		local basex,basey = PoolGridHelper.Inst.m_Basex,PoolGridHelper.Inst.m_Basey
        local x , y=t.position.x-basex,t.position.z-basey
        PoolGridHelper.Inst:SetPixelColor(x,y,PoolGridHelper.s_WaterColor)
        LuaPoolMgr.TrySetIce(x,y,false)
    end
    self.m_CurPoolCount = self.m_CurPoolCount + 1
    self:InitPoolCountLabel()
    --LuaPoolMgr.IsEditChanged = true
end

function LuaPoolEditWnd:CheckCanSetPool(chunkInfo)
    if self.m_IsDraging then
        return 
    end
    if not self.m_YardDecorationPrivalege then
        g_MessageMgr:ShowMessage("House_Privalege_Not_Enough")
        return
    end
    local x = chunkInfo.position.x
    local y = chunkInfo.position.z
    local type = LuaPoolMgr.TerrianTypeList[y*self.m_MapWidth + x]
    if type and type ~= LuaPoolMgr.LandType then
        return false
    end

    local index = y * self.m_MapWidth + x
    if x > LuaPoolMgr.Basex + LuaPoolMgr.MaxWidth or x < LuaPoolMgr.Basex or y > LuaPoolMgr.Basey + LuaPoolMgr.MaxHeight or y < LuaPoolMgr.Basey then
        g_MessageMgr:ShowMessage("Cant_EditPool_Here")
        return 
    end
    if not self:CheakJiaYuanKuaZhang(x,y) then
        g_MessageMgr:ShowMessage("Cant_EditPool_Here")
        return 
    end
    if CoreScene.MainCoreScene:GetBarriervIgnorePoolBarrier(x, y, false) ~= CSEBarrierType.eBT_NoBarrier then
        g_MessageMgr:ShowMessage("Cant_EditPool_With_Barrier")
        return 
    end
    if g_FurnitureScene and g_FurnitureScene:CanSetPool(x,y) then 
        g_MessageMgr:ShowMessage("EditPool_RemoveFurniture_Furst")
        return 
    end
    if CClientFurnitureMgr.Inst:IsSpecialPoolAt(x,y) then
        g_MessageMgr:ShowMessage("EditPool_RemoveFurniture_Furst")
        return
    end
    --桥的两端必须摆放在陆地上，也是不允许编辑水池的
    if CClientFurnitureMgr.Inst:IsBridgeMustOnLandPart(x,y) then
        g_MessageMgr:ShowMessage("EditPool_RemoveFurniture_Furst")
        return 
    end
    if CClientFurnitureMgr.Inst:IsByWarmPoolStair(x,y) then
        g_MessageMgr:ShowMessage("EditPool_RemoveFurniture_Furst")
        return 
    end

    if CClientFurnitureMgr.Inst:IsGrassAt(x,y) then
        g_MessageMgr:ShowMessage("Cant_EditPool_In_Grass")
        return 
    end
    if CClientFurnitureMgr.Inst.m_IsWinter and self:IsWinterGrassAt(x,y) then
        g_MessageMgr:ShowMessage("Cant_EditPool_In_Grass")
        return
    end

    return true
end

function LuaPoolEditWnd:OnUpdateHousePoolGridNum(houseId, poolgridnum)
    self.m_MaxPoolCount = CClientHouseMgr.Inst.mCurFurnitureProp2.PoolGridNum + self.m_BasePoolCount
    self.m_CurStoneCount = CClientHouseMgr.Inst.mConstructProp.Stone
    self:InitPoolCountLabel()
    if self.m_MaxPoolCount >= self.m_CurPoolCount then
        if houseId and poolgridnum and LuaPoolMgr.m_BuyPoolNeedSave then
            LuaPoolMgr.m_BuyPoolNeedSave = false
            self:CheckEditorChange(nil)
            Gac2Gas.RequestSetPoolBegin()
            self:OnSetNormalPool(3)
            self:OnSetNormalPool(2)
            
            if self.m_IsMingYuan then
                self:OnSetNormalPool(4)
                self:OnSetNormalPool(1)
            end
            Gac2Gas.RequestSetPoolEnd()
        end
    end
end

function LuaPoolEditWnd:OnDestroy()
    CClientFurnitureMgr.Inst:RefreshTerrainPool()
end

function LuaPoolEditWnd:OnClearPool()
    self.m_CurPoolCount = LuaPoolMgr.CurPoolCount
    self:InitPoolCountLabel()
end

function LuaPoolEditWnd:IsWinterGrassAt(x,z)
    local bytes = CClientHouseMgr.Inst.mCurFurnitureProp3.GrassContainer.Grass
    local maxChunkIndex = CRenderScene.Inst.GrassFast.m_Chunks.Length-1
    local chunkWidth = CRenderScene.Inst.GrassFast.m_ChunkWidth

    local baseX = 0
    local baseZ = 0
    local width = 0
    local height = 0
    
    local setting = House_GroundGrassSetting.GetData()
    if CClientFurnitureMgr.Inst.m_IsMingYuan then
        baseX,baseZ = setting.MingYuanGrassBase[0],setting.MingYuanGrassBase[1]
        sizeX,sizeZ = setting.MingYuanDiBiaoSize[0],setting.MingYuanDiBiaoSize[1]
    else
        baseX,baseZ = setting.NormalGrassBase[0],setting.NormalGrassBase[1]
        sizeX,sizeZ = setting.DiBiaoSize[0],setting.DiBiaoSize[1]
    end
    width = setting.MaxGGSize[0]
    height = setting.MaxGGSize[1]

    local chunkX = x * 2 / CGrassFast.CHUNK_RESOLUTION
    local chunkZ = z * 2 / CGrassFast.CHUNK_RESOLUTION

    local i = (z-baseZ)*width + (x-baseX)
        
    local chunkIndex = chunkX + chunkZ * chunkWidth
    if chunkIndex <= maxChunkIndex then
        local v = bytes[i]
        local type = bit.band(v, 0xf)
        return type > 0
    end
    return false
end