local DelegateFactory      = import "DelegateFactory"
local CTrackMgr            = import "L10.Game.CTrackMgr"
local CExpressionActionMgr = import "L10.Game.CExpressionActionMgr"

LuaValentine2023YWQSExpressionWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaValentine2023YWQSExpressionWnd, "grid")

function LuaValentine2023YWQSExpressionWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaValentine2023YWQSExpressionWnd:Init()
    local bg = self.transform:Find("Bg"):GetComponent(typeof(UIWidget))
    local template = bg.transform:Find("Template").gameObject
    self.grid = bg.transform:Find("Grid"):GetComponent(typeof(UIGrid))
    template:SetActive(false)

    local expressions = Valentine2023_YWQS_Setting.GetData().ShowExpressions
    Extensions.RemoveAllChildren(self.grid.transform)
    CExpressionActionMgr.Inst:InitData()
    for i = 0, expressions.Length - 1 do
        local child = NGUITools.AddChild(self.grid.gameObject, template)
        child:SetActive(true)

        local expressionId = expressions[i]
        CommonDefs.EnumerableIterate(CExpressionActionMgr.Inst.ThumbnailInfos.Values, DelegateFactory.Action_object(function (v)
            if v:ContainsExpression(expressions[i]) then
                expressionId = v:GetID()

            end
        end))

        child.transform:Find("Highlight").gameObject:SetActive(false)
        local data = Expression_Show.GetData(expressionId)
        child.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(data.Icon)
        child.transform:Find("Name"):GetComponent(typeof(UILabel)).text = data.ExpName

        UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnItemClick(i, expressionId)
        end)
    end
    self.grid:Reposition()

    self:UpdateBg(bg)
end

function LuaValentine2023YWQSExpressionWnd:UpdateBg(bg)
    local bounds = NGUIMath.CalculateRelativeWidgetBounds(self.grid.transform)
    bg.width = bounds.size.x + 20
    bg.height = bounds.size.y + 20

    bounds = NGUIMath.CalculateAbsoluteWidgetBounds(LuaValentine2023Mgr.YWQSInfo.lovingShowButton.transform)
    local worldPos = Vector3(bounds.min.x, bounds.center.y, 0)
    local localPos = bg.transform.parent:InverseTransformPoint(worldPos)
    LuaUtils.SetLocalPosition(bg.transform, localPos.x - bg.width / 2 - 20, localPos.y, 0)
end

--@region UIEvent

function LuaValentine2023YWQSExpressionWnd:OnItemClick(i, expressionId)
    self.grid.transform:GetChild(i):Find("Highlight").gameObject:SetActive(true)

    if CClientMainPlayer.Inst then
        local define = Expression_Define.GetData(expressionId)
        if define.TargetPlayerDist > 0 and CClientMainPlayer.Inst.Target then
            local target = CClientMainPlayer.Inst.Target
            CTrackMgr.Inst:Track(target.EngineId, define.TargetPlayerDist, DelegateFactory.Action(function ()
                Gac2Gas.ValentineYWQSShowDoExpression(expressionId)
            end), nil, false, true)
        else
            Gac2Gas.ValentineYWQSShowDoExpression(expressionId)
        end
    end
    CUIManager.CloseUI(CLuaUIResources.Valentine2023YWQSExpressionWnd)
end

--@endregion UIEvent
