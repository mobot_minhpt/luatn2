-- Auto Generated!!
local CEquipmentHoleTransferOptionsItem = import "L10.UI.CEquipmentHoleTransferOptionsItem"
local CItemMgr = import "L10.Game.CItemMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumQualityType = import "L10.Game.EnumQualityType"
CEquipmentHoleTransferOptionsItem.m_Init_CS2LuaHook = function (this, itemId) 
    this.itemIcon:Clear()
    this.bindSprite.enabled = false
    this.cellBorder.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    this.Selected = false
    this.ItemId = itemId

    local item = CItemMgr.Inst:GetById(itemId)
    if item ~= nil and item.IsEquip then
        this.itemIcon:LoadMaterial(item.Icon)
        this.bindSprite.enabled = item.IsBinded
        this.cellBorder.spriteName = CUICommonDef.GetItemCellBorder(item.Equip.QualityType)
    end
end
