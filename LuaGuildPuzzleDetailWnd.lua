local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Profession=import "L10.Game.Profession"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local UILabel=import "UILabel"
local QnTableView=import "L10.UI.QnTableView"

LuaGuildPuzzleDetailWnd = class()

--@region RegistChildComponent: Dont Modify Manually!


--@endregion RegistChildComponent end

function LuaGuildPuzzleDetailWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaGuildPuzzleDetailWnd:Init()
    Gac2Gas.QueryGuildPinTuPlayerData()
    self.m_DataList = {}

    self.m_TableView = self.transform:Find("TableView"):GetComponent(typeof(QnTableView))
    local function initItem(item,index)
        if index % 2 == 1 then
            item:SetBackgroundTexture("common_bg_mission_background_n")
        else
            item:SetBackgroundTexture("common_bg_mission_background_s")
        end
        self:InitItem(item,index)
    end

    self.m_TableViewDataSource = DefaultTableViewDataSource.CreateByCount(0,initItem)
    self.m_TableView.m_DataSource = self.m_TableViewDataSource
    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)

    self.m_VoidLabel = self.m_TableView.transform:Find("Label").gameObject

    self.m_Btn = self.transform:Find("Bottom/RemindBtn").gameObject
    UIEventListener.Get(self.m_Btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    Gac2Gas.RemindUseGuildPinTuChip()
	end)
end

function LuaGuildPuzzleDetailWnd:OnSelectAtRow(row)
    local data = self.m_DataList[row+1]
    if data then
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, "", nil, Vector3.zero, AlignType.Default)
    end
end

function LuaGuildPuzzleDetailWnd:InitItem(item,index)
    local tf = item.transform
    local nameLabel = FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel))
    local usedLabel = FindChild(tf,"UsedLabel"):GetComponent(typeof(UILabel))
    local unuseLabel = FindChild(tf,"UnuseLabel"):GetComponent(typeof(UILabel))
    
    local info = self.m_DataList[index + 1]
    local clsSprite = tf:Find("ClsSprite"):GetComponent(typeof(UISprite))
    clsSprite.spriteName = Profession.GetIconByNumber(info.playerClass)

    -- 不在线变灰
    CUICommonDef.SetActive(clsSprite.gameObject, info.isOnline, true)

    nameLabel.text = info.playerName
    usedLabel.text = tostring(info.playerPinTuTimes)
    unuseLabel.text = tostring(info.playerChipNum)
end

function LuaGuildPuzzleDetailWnd:OnData(dataList)
    self.m_DataList = dataList

    self.m_TableViewDataSource.count= #dataList
    self.m_VoidLabel:SetActive(#dataList == 0)
    self.m_TableView:ReloadData(true,false)
end

function LuaGuildPuzzleDetailWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryGuildPinTuPlayerDataResult", self, "OnData")
end

function LuaGuildPuzzleDetailWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryGuildPinTuPlayerDataResult", self, "OnData")
end

--@region UIEvent

--@endregion UIEvent

