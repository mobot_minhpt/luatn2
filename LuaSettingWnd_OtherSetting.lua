local LoadPicMgr = import "L10.Game.LoadPicMgr"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local String = import "System.String"
local QnCheckBoxGroup = import "L10.UI.QnCheckBoxGroup"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CTooltip = import "L10.UI.CTooltip"

LuaSettingWnd_OtherSetting = class()

RegistClassMember(LuaSettingWnd_OtherSetting, "m_DetailSettings")
RegistClassMember(LuaSettingWnd_OtherSetting, "m_DetailSettingsGroup")
RegistClassMember(LuaSettingWnd_OtherSetting, "m_TapButton")
RegistClassMember(LuaSettingWnd_OtherSetting, "m_JoystickButton")

function LuaSettingWnd_OtherSetting:Awake()
    self.m_DetailSettings = {
        {title = LocalString.GetString("自动响应组队跟随") ,propertyName = "AutoFollowTeamLeader"},
        {title = LocalString.GetString("好友消息推送")     ,propertyName = "PushFriendMessage"},
        {title = LocalString.GetString("目标可切灵兽鬼灵") ,propertyName = "SwitchTargetSkipPetAndLingShouDisabled"},
        {title = LocalString.GetString("自动修复耐久度")   ,propertyName = "AutoRepairEnabled"},
        --{title = LocalString.GetString("首屏显示绝技")     ,propertyName = "DirectShowJueJiEnabled"},
    }
    if not CommonDefs.IsInMobileDevice() then
        table.insert(self.m_DetailSettings,
{title = LocalString.GetString("关闭游戏进行确认") ,propertyName = "PCQuitShowConfirmWndEnabled"})
    end
    if CSwitchMgr.EnableVoiceAssistant then
        table.insert(self.m_DetailSettings,
{title = LocalString.GetString("开启语音助手")     ,propertyName = "VoiceAssistantEnabled"})
    end
    if LoadPicMgr.EnableChatReceivePicShowSign then
        table.insert(self.m_DetailSettings,
{title = LocalString.GetString("聊天自动收图")     ,propertyName = "ChatReceivePicShow"})
    end

    self:InitComponents()
    self:Init()
    self:LoadData()
end

function LuaSettingWnd_OtherSetting:InitComponents()
    self.m_JoystickButton = self.transform:Find("OtherSettings/Joystick"):GetComponent(typeof(QnSelectableButton))
    self.m_TapButton = self.transform:Find("OtherSettings/Tap"):GetComponent(typeof(QnSelectableButton))
    self.m_DetailSettingsGroup = self.transform:Find("OtherSettings/OtherSettings/QnCheckBoxGroup"):GetComponent(typeof(QnCheckBoxGroup))
end

function LuaSettingWnd_OtherSetting:Init()
    local detailtitles_buf = {}
    for i,v in ipairs(self.m_DetailSettings) do
        table.insert(detailtitles_buf, v.title)
    end
    local detailtitles = Table2Array(detailtitles_buf, MakeArrayClass(String))
    self.m_DetailSettingsGroup:InitWithOptions(detailtitles, true, false)
    self.m_DetailSettingsGroup.OnSelect = DelegateFactory.Action_QnCheckBox_int(function(checkbox,level)
        self:OnSelect(level)
    end)
    self.m_TapButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:On_TapButtonButtonClicked(btn)
    end)
    self.m_JoystickButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:On_JoystickButtonButtonClicked(btn)
    end)
end

function LuaSettingWnd_OtherSetting:OnEnable()
    self:LoadData()
    g_ScriptEvent:AddListener("ReloadSettings", self, "LoadData")
end

function LuaSettingWnd_OtherSetting:OnDisable()
    g_ScriptEvent:RemoveListener("ReloadSettings", self, "LoadData")
end

function LuaSettingWnd_OtherSetting:LoadData()
    for i = 1,#self.m_DetailSettings do
        local option = self.m_DetailSettings[i]
        self.m_DetailSettingsGroup[i - 1].Selected = PlayerSettings[option.propertyName]
        local checkbox = self.m_DetailSettingsGroup[i - 1]
        local infoButton = checkbox.transform:Find("InfoButton")
        if infoButton then
            infoButton.gameObject:SetActive(option.msg~=nil)
            if option.msg then
                UIEventListener.Get(infoButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                    CTooltip.ShowAtTop(g_MessageMgr:FormatMessage(option.msg), go.transform)
                end)
            end
        end
    end
    self.m_TapButton:SetSelected(PlayerSettings.TapEnabled, false)
    self.m_JoystickButton:SetSelected(PlayerSettings.JoystickEnabled, false)
end

function LuaSettingWnd_OtherSetting:On_TapButtonButtonClicked(btn)
    PlayerSettings.TapEnabled = self.m_TapButton:isSeleted()
    if not PlayerSettings.TapEnabled and not PlayerSettings.JoystickEnabled then
        PlayerSettings.JoystickEnabled = true
        self.m_JoystickButton:SetSelected(PlayerSettings.JoystickEnabled, false)
    end
end

function LuaSettingWnd_OtherSetting:On_JoystickButtonButtonClicked(btn)
    PlayerSettings.JoystickEnabled = self.m_JoystickButton:isSeleted()
    if not PlayerSettings.TapEnabled and not PlayerSettings.JoystickEnabled then
        PlayerSettings.TapEnabled = true
        self.m_TapButton:SetSelected(PlayerSettings.TapEnabled, false)
    end
end

function LuaSettingWnd_OtherSetting:OnSelect(level)
    local option = self.m_DetailSettings[level + 1]
    local enable = self.m_DetailSettingsGroup[level].Selected
    PlayerSettings[option.propertyName] = enable
end