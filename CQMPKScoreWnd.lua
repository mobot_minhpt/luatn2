-- Auto Generated!!
local CQMPKScoreItem = import "L10.UI.CQMPKScoreItem"
local CQMPKScoreWnd = import "L10.UI.CQMPKScoreWnd"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local Int32 = import "System.Int32"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CQMPKScoreWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_CloseBtn).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)

    this.m_MyScoreObj:SetActive(false)
    this.m_ScoreObj:SetActive(false)
end
CQMPKScoreWnd.m_Init_CS2LuaHook = function (this) 
    this.m_TableView.m_DataSource = this
    this.m_TableView:ReloadData(false, false)
    this.m_TableView.OnSelectAtRow = MakeDelegateFromCSFunction(this.OnSelectAtRow, MakeGenericClass(Action1, Int32), this)
end
CQMPKScoreWnd.m_ItemAt_CS2LuaHook = function (this, view, index) 
    if index < 0 and index >= CQuanMinPKMgr.Inst.m_ScoreRankList.Count then
        return nil
    end

    local item
    if index == 0 and CQuanMinPKMgr.Inst.m_HasMyScore then
        item = TypeAs(view:GetFromPool(0), typeof(CQMPKScoreItem))
    else
        item = TypeAs(view:GetFromPool(1), typeof(CQMPKScoreItem))
    end
    if nil == item then
        return nil
    end
    item:Init(CQuanMinPKMgr.Inst.m_ScoreRankList[index], index, index == 0 and CQuanMinPKMgr.Inst.m_HasMyScore)
    return item
end
