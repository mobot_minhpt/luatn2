local CGuildMgr = import "L10.Game.CGuildMgr"
local UILabel = import "UILabel"
local UIGrid = import "UIGrid"
local GameObject = import "UnityEngine.GameObject"

LuaGuildTerritorialWarsMapRankView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildTerritorialWarsMapRankView, "MyGuildNameLabel", "MyGuildNameLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsMapRankView, "MyGuildRankLabel", "MyGuildRankLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsMapRankView, "MyGuildScoreLabel", "MyGuildScoreLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsMapRankView, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaGuildTerritorialWarsMapRankView, "RankTemplate", "RankTemplate", GameObject)

--@endregion RegistChildComponent end

function LuaGuildTerritorialWarsMapRankView:Awake()
    self.RankTemplate.gameObject:SetActive(false)
    LuaGuildTerritorialWarsMgr.m_NotOpenSituationWnd = true
    if not LuaGuildTerritorialWarsMgr:IsInGuide() then
        Gac2Gas.RequestTerritoryWarScoreData()
    end
    local guildinfo = LuaGuildTerritorialWarsMgr.m_GuildInfo[LuaGuildTerritorialWarsMgr.m_GuildId]
    if guildinfo then
        self.MyGuildNameLabel.text = guildinfo.guildName
    end
    self.MyGuildRankLabel.text = LocalString.GetString("——")
end

function LuaGuildTerritorialWarsMapRankView:OnEnable()
    g_ScriptEvent:AddListener("OnSendTerritoryWarScoreInfo",self,"OnSendTerritoryWarScoreInfo")
end

function LuaGuildTerritorialWarsMapRankView:OnDisable()
    g_ScriptEvent:RemoveListener("OnSendTerritoryWarScoreInfo",self,"OnSendTerritoryWarScoreInfo")
end

function LuaGuildTerritorialWarsMapRankView:OnSendTerritoryWarScoreInfo()
    local curScoreInfo = LuaGuildTerritorialWarsMgr.m_CurScoreInfo
    self.MyGuildScoreLabel.text = curScoreInfo.currentScore
    local list = LuaGuildTerritorialWarsMgr.m_GuildScoreInfo
    for i = 1,3 do
        local obj = NGUITools.AddChild(self.Grid.gameObject,self.RankTemplate)
        self:InitItem(obj, list[i], i)
    end
    self.Grid:Reposition()
    for i = 1,#list do
        local data = list[i]
        if data.guildId == LuaGuildTerritorialWarsMgr.m_CurScoreInfo.myGuildId then
            self.MyGuildRankLabel.text = i
        end
    end
end

function LuaGuildTerritorialWarsMapRankView:InitItem(obj, data, i)
    obj.gameObject:SetActive(true)
    local scoreLabel = obj.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    local nameLabel = obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local firstSprite = obj.transform:Find("FirstSprite")
    local secondSprite = obj.transform:Find("SecondSprite")
    local thirdSprite = obj.transform:Find("ThirdSprite")

    firstSprite.gameObject:SetActive(i == 1)
    secondSprite.gameObject:SetActive(i == 2)
    thirdSprite.gameObject:SetActive(i == 3)

    scoreLabel.alpha = data and 1 or 0.5
    nameLabel.alpha = data and 1 or 0.5
    scoreLabel.text = data and math.floor(data.score) or LocalString.GetString("——")
    nameLabel.text = data and data.guildName or LocalString.GetString("暂无")
end

--@region UIEvent

--@endregion UIEvent

