local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Object = import "System.Object"

LuaOffWorldPha1Wnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaOffWorldPha1Wnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaOffWorldPha1Wnd, "right", "right", GameObject)
RegistChildComponent(LuaOffWorldPha1Wnd, "wrong", "wrong", GameObject)

--@endregion RegistChildComponent end

function LuaOffWorldPha1Wnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

RegistClassMember(LuaOffWorldPha1Wnd, "taskID")
RegistClassMember(LuaOffWorldPha1Wnd, "tipLabel")
RegistClassMember(LuaOffWorldPha1Wnd, "taskPassNum")
RegistClassMember(LuaOffWorldPha1Wnd, "chooseTable")

function LuaOffWorldPha1Wnd:Init()
  local quitMessage = LocalString.GetString('需要找出至少[c][fcff00]3幅[-][/c]古卷才能完成任务，是否确定退出？退出后再做此任务需要重新寻找古卷哦~')
  local onCloseClick = function(go)
    if self.taskPassNum and self.taskPassNum >= 3 then
      self:Close()
    else
      MessageWndManager.ShowOKCancelMessage(quitMessage,DelegateFactory.Action(function()
        self:Close()
      end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
    end
  end
  CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)

  self.taskID = OffWorldPass_Setting.GetData().ParallelWorldReelTaskId
  self:InitClickNode()
  self.tipLabel = self.right.transform.parent:Find('TipTextNode'):GetComponent(typeof(UILabel))
  self:UpdateTipLabel()
end

--@region UIEvent

--@endregion UIEvent
function LuaOffWorldPha1Wnd:UpdateTipLabel()
  if not self.taskPassNum then
    self.taskPassNum = 0
  end
  if self.taskPassNum > 0 then
    self.tipLabel.text = SafeStringFormat3(LocalString.GetString('找出异世红尘流出的古卷 (%s/3)'),self.taskPassNum)
  else
    self.tipLabel.text = SafeStringFormat3(LocalString.GetString('找出异世红尘流出的古卷 (%s/3)'),'[c][FF0000]0[-][/c]')
  end
end

function LuaOffWorldPha1Wnd:InitRightNode(node,index)
  local highNode = node.transform:Find('highlight').gameObject
  highNode:SetActive(false)

  if not self.chooseTable then
    self.chooseTable = {}
  end

  local onClick = function(go)
    LuaOffWorldPassMgr.ShowPicIndex = index
    local data = OffWorldPass_ParallelWorldReel.GetData(index)
    if data.parallelworldreel == 1 and not self.chooseTable[index] then
      if not self.taskPassNum then
        self.taskPassNum = 0
      end
      self.taskPassNum = self.taskPassNum + 1
      self.chooseTable[index] = true
      self:UpdateTipLabel()
      if self.taskPassNum >= 3 then
        self:Finish()
      end
    end
    CUIManager.ShowUI(CLuaUIResources.OffWorldPha1PicWnd)
  end
  CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(onClick),false)

--  UIEventListener.Get(node).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
--    if isPressed then
--      highNode:SetActive(true)
--    else
--      highNode:SetActive(false)
--    end
--  end)
end

function LuaOffWorldPha1Wnd:InitWrongNode(node,index)
  local onClick = function(go)
    g_MessageMgr:ShowMessage('CUSTOM_STRING2',LocalString.GetString('这幅画太过残破，已经看不出内容了'))
  end
  CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(onClick),false)
end

function LuaOffWorldPha1Wnd:InitClickNode()
  local rightNodeTable = {self.right.transform:Find('btn1').gameObject,
  self.right.transform:Find('btn2').gameObject,
  self.right.transform:Find('btn3').gameObject,
  self.right.transform:Find('btn4').gameObject,
  self.right.transform:Find('btn5').gameObject}
  local wrongNodeTable = {self.wrong.transform:Find('btn1').gameObject,
  self.wrong.transform:Find('btn2').gameObject,
  self.wrong.transform:Find('btn3').gameObject,
  self.wrong.transform:Find('btn4').gameObject,
  self.wrong.transform:Find('btn5').gameObject}
  for i,v in ipairs(rightNodeTable) do
    self:InitRightNode(v,i)
  end
  for i,v in ipairs(wrongNodeTable) do
    self:InitWrongNode(v,i)
  end
end

function LuaOffWorldPha1Wnd:Close()
  CUIManager.CloseUI(CLuaUIResources.OffWorldPha1Wnd)
end

function LuaOffWorldPha1Wnd:Finish()
	--Gac2Gas.FinishTaskPainting(self.taskID)
	local empty = CreateFromClass(MakeGenericClass(List, Object))
	Gac2Gas.FinishClientTaskEvent(self.taskID,"PickScrolls",MsgPackImpl.pack(empty))
--	RegisterTickWithDuration(function ()
--		self:Close()
--	end,2000,2000)
end
