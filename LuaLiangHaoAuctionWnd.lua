require("common/common_include")

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CBaseWnd = import "L10.UI.CBaseWnd"
local UILabel = import "UILabel"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"

LuaLiangHaoAuctionWnd = class()

RegistClassMember(LuaLiangHaoAuctionWnd, "m_LiangHaoLabel")
RegistClassMember(LuaLiangHaoAuctionWnd, "m_TopBidderInfoLabel")
RegistClassMember(LuaLiangHaoAuctionWnd, "m_TopBidderPriceLabel")
RegistClassMember(LuaLiangHaoAuctionWnd, "m_AddSubInputButton")
RegistClassMember(LuaLiangHaoAuctionWnd, "m_MyPriceLabel")
RegistClassMember(LuaLiangHaoAuctionWnd, "m_AuctionButton")

function LuaLiangHaoAuctionWnd:Init()

	self.m_LiangHaoLabel = self.transform:Find("Anchor/LiangHaoLabel"):GetComponent(typeof(UILabel))
	self.m_TopBidderInfoLabel = self.transform:Find("Anchor/TopBidder/PlayerInfoLabel"):GetComponent(typeof(UILabel))
	self.m_TopBidderPriceLabel = self.transform:Find("Anchor/TopBidder/PriceLabel"):GetComponent(typeof(UILabel))
	self.m_AddSubInputButton = self.transform:Find("Anchor/MyAuction/AddSubInputButton"):GetComponent(typeof(QnAddSubAndInputButton))
	self.m_MyPriceLabel = self.transform:Find("Anchor/MyAuction/PriceLabel"):GetComponent(typeof(UILabel))
	self.m_AuctionButton = self.transform:Find("Anchor/AuctionButton").gameObject
	
	CommonDefs.AddOnClickListener(self.m_AuctionButton, DelegateFactory.Action_GameObject(function(go) self:OnAuctionButtonClick() end), false)

	local data = LuaLiangHaoMgr.AuctionLiangHaoInfo
	self.m_LiangHaoLabel.text = tostring(data.lianghaoId)
	self.m_TopBidderInfoLabel.text = data.topBidderName
	self.m_TopBidderPriceLabel.text = tostring(data.topBidderPrice)

	self.m_AddSubInputButton:SetMinMax(data.minPrice, data.maxPrice, data.priceStep)
  	self.m_AddSubInputButton:SetValue(data.minPrice, true)
  	self.m_AddSubInputButton:SetNumberInputAlignType(CTooltipAlignType.Right)
	self.m_MyPriceLabel.text = tostring(math.max(0, data.minPrice - data.myPrice))

	self.m_AddSubInputButton.onValueChanged = DelegateFactory.Action_uint(function (val)
		self:OnAuctionValueChanged(val)
	end)
end

function LuaLiangHaoAuctionWnd:OnAuctionValueChanged(val)
	local data = LuaLiangHaoMgr.AuctionLiangHaoInfo
	self.m_MyPriceLabel.text = tostring(math.max(0, val - data.myPrice))
end

function LuaLiangHaoAuctionWnd:OnAuctionButtonClick()
	local data = LuaLiangHaoMgr.AuctionLiangHaoInfo
	local id = data.lianghaoId
	local lingyu = self.m_AddSubInputButton:GetValue()
	local msgName = nil
	local lingyuDisplay = lingyu
	if LuaLiangHaoMgr.AuctionLiangHaoInfo.myPrice == 0 then --初次竞价
		msgName = "Lianghao_Auction_Confirm"
	else
		msgName = "Lianghao_ReAuction_Confirm"
		lingyuDisplay = lingyu - data.myPrice
	end
	MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage(msgName,lingyuDisplay, id), 
		DelegateFactory.Action(function() 
			LuaLiangHaoMgr.RequestAuctionLiangHao(id, lingyu) --坤少的坑，这里要用真实价格
			self:Close()
		end),nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
	
end

function LuaLiangHaoAuctionWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end
