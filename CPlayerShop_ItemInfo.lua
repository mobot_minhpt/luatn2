-- Auto Generated!!
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerShop_ItemInfo = import "L10.UI.CPlayerShop_ItemInfo"
local CPlayerShopItem = import "L10.UI.CPlayerShopItem"
local CPlayerShopItemData = import "L10.UI.CPlayerShopItemData"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local Double = import "System.Double"
local EnumEventType = import "EnumEventType"
local CItemMgr = import "L10.Game.CItemMgr"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local QnButton = import "L10.UI.QnButton"
local SearchOption = import "L10.UI.SearchOption"
local String = import "System.String"
local Time = import "UnityEngine.Time"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local UIRect = import "UIRect"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local Word_Word = import "L10.Game.Word_Word"
local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CPlayerShopPopupMenuInfoMgr = import "L10.UI.CPlayerShopPopupMenuInfoMgr"
local QnTipButton = import "L10.UI.QnTipButton"
local Object = import"System.Object"
local IdPartition = import "L10.Game.IdPartition"
CPlayerShop_ItemInfo.m_Init_CS2LuaHook = function (this, templateId, option, publicity, enablefrequency) 
    this:InitWordFilter(CPlayerShopMgr.Inst:FromAliasTemplateId(templateId))
    this.m_EnablePageFrequency = enablefrequency
    this.m_TemplateId = templateId
    LuaPlayerShop_ItemInfo:Init(this,templateId)
    this.m_SearchOption = option
    this.m_Publicity = publicity
    this.m_PageButton:SetValue(1, false)
    this.m_PageButton:SetMinMax(1, this.m_TotalPages, 1)
    this.m_FocusButton.Text = LocalString.GetString("关注")
    this.m_ContactButton.Visible = false
    this:DoSearch(1)
    if this.m_TemplateId > 0 then
        Gac2Gas.QueryPlayerShopRecommendPrice(this.m_TemplateId)
    end
    this:UpdateBatchBuyButtonVisible(templateId)
end
CPlayerShop_ItemInfo.m_Init2_CS2LuaHook = function (this, wordIdStr, gradeRange, type, subtype, option, publicity, enablefrequency) 
    this:InitWordFilter(0)
    this.m_EnablePageFrequency = enablefrequency
    this.m_TemplateId = 0
    LuaPlayerShop_ItemInfo:Init(this,0)
    this.m_WordIdStr = wordIdStr
    this.m_GradeRange = gradeRange
    this.m_Type = type
    this.m_SubType = subtype
    this.m_SearchOption = option
    this.m_Publicity = publicity
    this.m_PageButton:SetValue(1, false)
    this.m_PageButton:SetMinMax(1, this.m_TotalPages, 1)
    this.m_FocusButton.Text = LocalString.GetString("关注")
    this.m_ContactButton.Visible = false
    this:DoSearch(1)
    this:UpdateBatchBuyButtonVisible(0)
end
CPlayerShop_ItemInfo.m_Init3_CS2LuaHook = function (this, grade, type, color, enablefrequency) 
    this:InitWordFilter(0)
    this.m_EnablePageFrequency = enablefrequency
    this.m_TemplateId = 0
    LuaPlayerShop_ItemInfo:Init(this,0)
    this.m_WordIdStr = nil
    this.m_GradeRange = grade
    this.m_Type = type
    this.m_Color = color
    this.m_SearchOption = SearchOption.All
    this.m_Publicity = false
    this.m_PageButton:SetValue(1, false)
    this.m_PageButton:SetMinMax(1, this.m_TotalPages, 1)
    this.m_FocusButton.Text = LocalString.GetString("关注")
    this.m_ContactButton.Visible = false
    this:DoSearch(1)
    this:UpdateBatchBuyButtonVisible(0)
end
CPlayerShop_ItemInfo.m_CanTryNextSearch_CS2LuaHook = function (this) 
    if this.m_EnablePageFrequency then
        --限制查询接口的调用频率
        if CPlayerShop_ItemInfo.s_EnableRestrictEvaluateFrequency then
            if Time.realtimeSinceStartup - this.m_LastEvaluateTime < this.m_EvaluateInterval then
                this.m_PageButton:SetValue(this.m_CurrentPage, false)
                this.m_PageButton:OverrideText(System.String.Format("{0}/{1}", this.m_CurrentPage, this.m_TotalPages))
                g_MessageMgr:ShowMessage("RPC_TOO_FREQUENT")
                return false
            end
            this.m_LastEvaluateTime = Time.realtimeSinceStartup
        end
    end
    return true
end
CPlayerShop_ItemInfo.m_DoSearch_CS2LuaHook = function (this, page)    if not this:CanTryNextSearch() then
        return
    end

    this.m_CurrentPage = page
    this.m_PageButton:OverrideText(System.String.Format("{0}/{1}", this.m_CurrentPage, this.m_TotalPages))
    local startIndex = ((this.m_CurrentPage - 1) * CPlayerShop_ItemInfo.m_CountPerPage + 1)
    local endIndex = (this.m_CurrentPage * CPlayerShop_ItemInfo.m_CountPerPage)
    --清空数据再查询
    CommonDefs.ListClear(this.m_SearchResultData)
    this.m_SearchResultTable:ReloadData(false, false)
    if this.m_SearchOption == SearchOption.AllPublicity then
        Gac2Gas.QueryPlayerShopPubliclyItems(startIndex, endIndex, LuaPlayerShop_ItemInfo.m_SortIndex > 0)
    else
        --根据词条或颜色搜索
        if this.m_TemplateId == 0 then
            if not System.String.IsNullOrEmpty(this.m_WordIdStr) then
                local wordIds = {}
                for id in string.gmatch(this.m_WordIdStr, "(%d+)") do
                    table.insert(wordIds, tonumber(id))
                end
                local wordIds_U = MsgPackImpl.pack(Table2List(wordIds, MakeGenericClass(List, Object)))
                Gac2Gas.QueryPlayerShopSearchedByWords(wordIds_U, this.m_GradeRange, this.m_Type, this.m_SubType, EnumToInt(this.m_SearchOption), startIndex, endIndex, this.m_Publicity)
            else
                Gac2Gas.QueryPlayerShopSearchZhuangShiWuByColor(startIndex, endIndex, this.m_GradeRange, this.m_Type, this.m_Color)
            end
        else
            Gac2Gas.QueryPlayerShopSearchedItems(this.m_TemplateId, EnumToInt(this.m_SearchOption), startIndex, endIndex, this.m_Publicity, this.m_TargetWordId)
        end
    end
end
CPlayerShop_ItemInfo.m_OnEnable_CS2LuaHook = function (this) 
    this.m_SearchResultTable.OnSelectAtRow = CommonDefs.CombineListner_Action_int(this.m_SearchResultTable.OnSelectAtRow, MakeDelegateFromCSFunction(this.OnSelectSearchResultTable, MakeGenericClass(Action1, Int32), this), true)
    this.m_BuyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_BuyButton.OnClick, MakeDelegateFromCSFunction(this.OnClickBuyButton, MakeGenericClass(Action1, QnButton), this), true)
    this.m_FocusButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_FocusButton.OnClick, MakeDelegateFromCSFunction(this.OnFocusButtonClick, MakeGenericClass(Action1, QnButton), this), true)
    this.m_ContactButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_ContactButton.OnClick, MakeDelegateFromCSFunction(this.OnContactButtonClick, MakeGenericClass(Action1, QnButton), this), true)
    if this.m_BatchBuyButton ~= nil then
        this.m_BatchBuyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_BatchBuyButton.OnClick, MakeDelegateFromCSFunction(this.OnBatchBuyButtonClick, MakeGenericClass(Action1, QnButton), this), true)
    end
    this.m_PageButton.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_PageButton.onValueChanged, MakeDelegateFromCSFunction(this.OnPageChanged, MakeGenericClass(Action1, UInt32), this), true)
    EventManager.AddListenerInternal(EnumEventType.QueryPlayerShopSearchedItemsResult, MakeDelegateFromCSFunction(this.OnRecieveSearchResult, MakeGenericClass(Action4, MakeGenericClass(List, CPlayerShopItemData), UInt32, UInt32, UInt32), this))
    --EventManager.AddListener<List<CPlayerShopItemData>, uint>(EnumEventType.QueryPlayerShopPubliclyItemsResult, OnRecieveSearchResult);
    EventManager.AddListenerInternal(EnumEventType.BuyItemFromPlayerShopSuccess, MakeDelegateFromCSFunction(this.OnBuyItemFromPlayerShopSuccess, MakeGenericClass(Action3, UInt32, String, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.PlayerShopCollectItemSuccess, MakeDelegateFromCSFunction(this.OnPlayerShopCollectItemSuccess, MakeGenericClass(Action3, Double, UInt32, String), this))
    EventManager.AddListenerInternal(EnumEventType.CancelPlayerShopCollectItemSuccess, MakeDelegateFromCSFunction(this.OnCancelPlayerShopCollectItemSuccess, MakeGenericClass(Action1, MakeGenericClass(List, CPlayerShopItemData)), this))
    EventManager.AddListenerInternal(EnumEventType.QueryPlayerShopRecommendPriceResult, MakeDelegateFromCSFunction(this.OnQueryPlayerShopRecommendPriceResult, MakeGenericClass(Action4, UInt32, UInt32, UInt32, UInt32), this))
    local qnTipButton = this.transform:Find("QnTipButton")
    if qnTipButton then
        qnTipButton = qnTipButton:GetComponent(typeof(QnTipButton))
        local arr = {LocalString.GetString("按时间"),LocalString.GetString("按关注量")}
        qnTipButton.Text = arr[1]
        local popupMenuItemDataArr = {}
        for i = 1,2 do
            table.insert(popupMenuItemDataArr,PopupMenuItemData(arr[i], DelegateFactory.Action_int(function (index) 
                qnTipButton.Text = arr[i]
                LuaPlayerShop_ItemInfo.m_SortIndex = index
                this.m_PageButton:SetValue(1,true)
            end)))
        end
        CommonDefs.AddOnClickListener(qnTipButton.gameObject,DelegateFactory.Action_GameObject(function (go)
            CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(Table2Array(popupMenuItemDataArr, MakeArrayClass(PopupMenuItemData)), qnTipButton.transform, CPopupMenuInfoMgrAlignType.Bottom, 1, nil, nil, DelegateFactory.Action(function ()
                qnTipButton:SetTipStatus(false)
            end), 600,350)
        end),false)
    end
    LuaPlayerShop_ItemInfo:OnEnable(this)
end
CPlayerShop_ItemInfo.m_OnDisable_CS2LuaHook = function (this) 
    this.m_SearchResultTable.OnSelectAtRow = CommonDefs.CombineListner_Action_int(this.m_SearchResultTable.OnSelectAtRow, MakeDelegateFromCSFunction(this.OnSelectSearchResultTable, MakeGenericClass(Action1, Int32), this), false)
    this.m_BuyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_BuyButton.OnClick, MakeDelegateFromCSFunction(this.OnClickBuyButton, MakeGenericClass(Action1, QnButton), this), false)
    this.m_FocusButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_FocusButton.OnClick, MakeDelegateFromCSFunction(this.OnFocusButtonClick, MakeGenericClass(Action1, QnButton), this), false)
    this.m_ContactButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_ContactButton.OnClick, MakeDelegateFromCSFunction(this.OnContactButtonClick, MakeGenericClass(Action1, QnButton), this), false)
    if this.m_BatchBuyButton ~= nil then
        this.m_BatchBuyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_BatchBuyButton.OnClick, MakeDelegateFromCSFunction(this.OnBatchBuyButtonClick, MakeGenericClass(Action1, QnButton), this), false)
    end
    this.m_PageButton.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_PageButton.onValueChanged, MakeDelegateFromCSFunction(this.OnPageChanged, MakeGenericClass(Action1, UInt32), this), false)
    EventManager.RemoveListenerInternal(EnumEventType.QueryPlayerShopSearchedItemsResult, MakeDelegateFromCSFunction(this.OnRecieveSearchResult, MakeGenericClass(Action4, MakeGenericClass(List, CPlayerShopItemData), UInt32, UInt32, UInt32), this))
    --EventManager.RemoveListener<List<CPlayerShopItemData>, uint>(EnumEventType.QueryPlayerShopPubliclyItemsResult, OnRecieveSearchResult);
    EventManager.RemoveListenerInternal(EnumEventType.BuyItemFromPlayerShopSuccess, MakeDelegateFromCSFunction(this.OnBuyItemFromPlayerShopSuccess, MakeGenericClass(Action3, UInt32, String, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.PlayerShopCollectItemSuccess, MakeDelegateFromCSFunction(this.OnPlayerShopCollectItemSuccess, MakeGenericClass(Action3, Double, UInt32, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.CancelPlayerShopCollectItemSuccess, MakeDelegateFromCSFunction(this.OnCancelPlayerShopCollectItemSuccess, MakeGenericClass(Action1, MakeGenericClass(List, CPlayerShopItemData)), this))
    EventManager.RemoveListenerInternal(EnumEventType.QueryPlayerShopRecommendPriceResult, MakeDelegateFromCSFunction(this.OnQueryPlayerShopRecommendPriceResult, MakeGenericClass(Action4, UInt32, UInt32, UInt32, UInt32), this))
    LuaPlayerShop_ItemInfo.m_SortIndex = 0
    CPlayerShopMgr.Inst:DisableBatchBuyEventListeners()
    LuaPlayerShop_ItemInfo:OnDisable()
end
CPlayerShop_ItemInfo.m_OnQueryPlayerShopRecommendPriceResult_CS2LuaHook = function (this, templateId, basisPrice, minPrice, maxPrice) 
    if templateId == this.m_TemplateId then
        CPlayerShopMgr.BatchBuyCurrentPrice = basisPrice
        CPlayerShopMgr.BatchBuyRecommendPrice = basisPrice
        CPlayerShopMgr.BatchBuyMinPrice = minPrice
        CPlayerShopMgr.BatchBuyMaxPrice = maxPrice
        --CLogMgr.Log("OnQueryPlayerShopRecommendPriceResult " + templateId.ToString() + " " + basisPrice.ToString());
    end
end
CPlayerShop_ItemInfo.m_OnFocusButtonClick_CS2LuaHook = function (this, button)
    if this.m_SelectItemIndex < 0 or this.m_SelectItemIndex >= this.m_SearchResultData.Count then

        if this.m_Publicity then
            g_MessageMgr:ShowMessage("PLAYERSHOP_NO_SELECTION")
        else
            local allFocus = CPlayerShopMgr.Inst:GetAllFocusTemplate()
            if CommonDefs.HashSetContains(allFocus, typeof(UInt32), this.m_TemplateId) then
                CommonDefs.HashSetRemove(allFocus, typeof(UInt32), this.m_TemplateId)
                CPlayerShopMgr.Inst:SetAllFocusTemplate(allFocus)
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("取消关注成功"))
                this.m_FocusButton.Text = LocalString.GetString("关注")
            else
                CommonDefs.HashSetAdd(allFocus, typeof(UInt32), this.m_TemplateId)
                if allFocus.Count <= CPlayerShopMgr.s_FocusTemplateLimit then
                    CPlayerShopMgr.Inst:SetAllFocusTemplate(allFocus)
                    g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("关注成功"))
                    this.m_FocusButton.Text = LocalString.GetString("取消关注")
                else
                    g_MessageMgr:ShowMessage("PLAYER_SHOP_MAX_COLLECTIONS")
                end
            end
        end

        return
    end
    local data = this.m_SearchResultData[this.m_SelectItemIndex]
    if LuaPlayerShop_ItemInfo:CheckConfirmDisplaySplitItemId(data) then
        local msg = g_MessageMgr:FormatMessage(data.IsFocus and "PLAYER_SHOP_DISPLAY_SPLIT_CANCEL_FOLLOW" or "PLAYER_SHOP_DISPLAY_SPLIT_FOLLOW")
            MessageWndManager.ShowOKMessage(msg, DelegateFactory.Action(function ()
                CPlayerShopMgr.Inst:FocusOrCancelFocus(data)
            end))
        return
    end
    CPlayerShopMgr.Inst:FocusOrCancelFocus(data)
end
CPlayerShop_ItemInfo.m_OnContactButtonClick_CS2LuaHook = function (this, button)
    --联系店主
    if this.m_SelectItemIndex < 0 or this.m_SelectItemIndex >= this.m_SearchResultData.Count then
        g_MessageMgr:ShowMessage("PLAYERSHOP_NO_SELECTION")
        return
    end
    local data = this.m_SearchResultData[this.m_SelectItemIndex]
    CPlayerShopMgr.Inst:ContactShopOwner(data.OwnerId, data.OwnerName)
end
CPlayerShop_ItemInfo.m_OnBatchBuyButtonClick_CS2LuaHook = function (this, button) 
    CPlayerShopMgr.BatchBuyTemplateId = this.m_TemplateId
    CPlayerShopMgr.BatchBuyRecommendCount = 1
    CPlayerShopMgr.BatchBuyMaxCount = 999
    CPlayerShopMgr.BatchBuyMinCount = 1
    local data = Item_Item.GetData(this.m_TemplateId)
    if data ~= nil then
        CPlayerShopMgr.BatchBuyTemplateName = data.Name
    else
        CPlayerShopMgr.BatchBuyTemplateName = ""
    end
    LuaPlayerShopMgr.m_BatchBuyLowestPrice = this.m_SearchResultData.Count > 0 and this.m_SearchResultData[0].Price or 0
    for i = 0,this.m_SearchResultData.Count - 1 do
        local data =  this.m_SearchResultData[i]
        if data.Price < LuaPlayerShopMgr.m_BatchBuyLowestPrice then
            LuaPlayerShopMgr.m_BatchBuyLowestPrice = data.Price
        end
    end
    --CPlayerShopMgr.BatchBuyItemDatas = this.m_SearchResultData;
    CUIManager.ShowUI(CUIResources.PlayerShopBatchBuyWnd)
end
CPlayerShop_ItemInfo.m_OnClickBuyButton_CS2LuaHook = function (this, button)
    if this.m_SelectItemIndex < 0 or this.m_SelectItemIndex >= this.m_SearchResultData.Count then
        g_MessageMgr:ShowMessage("PLAYERSHOP_NO_SELECTION")
        return
    end
    local data = this.m_SearchResultData[this.m_SelectItemIndex]
    CPlayerShopMgr.Inst:BuyItemFromShop(data)
end
CPlayerShop_ItemInfo.m_OnSelectSearchResultTable_CS2LuaHook = function (this, index)
    if this.m_SelectItemIndex ~= index then
        this.m_SelectItemIndex = index
        local data = this.m_SearchResultData[index]
        if data ~= nil then
            this.m_ContactButton.gameObject:SetActive(data.Item.IsPrecious)
            if this.m_BatchBuyButton then
                this:UpdateBatchBuyButtonVisibleWithShopItem(data)
            end
        end
        local default
        if data.IsFocus then
            default = LocalString.GetString("取消关注")
        else
            default = LocalString.GetString("关注")
        end
        this.m_FocusButton.Text = default
    end
end
CPlayerShop_ItemInfo.m_UpdateBatchBuyButtonVisible_CS2LuaHook = function (this, templateId) 
    local canBatchBuy = CPlayerShopMgr.Inst:GetCanBatchBuy(templateId)
    if this.m_BatchBuyButton then
        this.m_BatchBuyButton.gameObject:SetActive(canBatchBuy)
    end
    local canOnlyBatchBuy = canBatchBuy and CPlayerShopMgr.Inst:GetCanOnlyBatchBuy(templateId)
    this.m_BuyButton.Visible = not canOnlyBatchBuy
end
CPlayerShop_ItemInfo.m_UpdateBatchBuyButtonVisibleWithShopItem_CS2LuaHook = function (this, data) 
    local canBatchBuy = CPlayerShopMgr.Inst:GetCanBatchBuy(data.Item.TemplateId)
    canBatchBuy = canBatchBuy and not data.Item.IsPrecious
    if this.m_BatchBuyButton then
        this.m_BatchBuyButton.gameObject:SetActive(canBatchBuy)
    end
    local canOnlyBatchBuy = canBatchBuy and CPlayerShopMgr.Inst:GetCanOnlyBatchBuy(data.Item.TemplateId)
    this.m_BuyButton.Visible = not canOnlyBatchBuy
end
CPlayerShop_ItemInfo.m_OnRecieveSearchResult_CS2LuaHook = function (this, data, totalCount, indexStart, indexEnd)
    this.m_TotalCount = totalCount
    this.m_TotalPages = math.ceil(1 * totalCount / CPlayerShop_ItemInfo.m_CountPerPage)
    if this.m_TotalPages <= 0 then
        this.m_TotalPages = 1
    end
    this.m_PageButton:SetMinMax(1, this.m_TotalPages, 1)
    this.m_PageButton:OverrideText(System.String.Format("{0}/{1}", this.m_CurrentPage, this.m_TotalPages))

    this.m_SearchResultData = LuaPlayerShopMgr.SplitSearchResultData(data)
    --值设置第一页的内容
    if indexStart == 1 then
        CPlayerShopMgr.Inst:SetBatchBuyItemDatas(data)
    end
    this.m_SearchResultTable:ReloadData(true, false)
    this.m_SelectItemIndex = - 1
    if data.Count > 0 then
        this.m_SearchResultTable:SetSelectRow(0, true)
    end
    if this.m_SearchOption == SearchOption.AllPublicity then
        CommonDefs.GetComponent_GameObject_Type(this.m_NoShopItemLabel, typeof(UILabel)).text = LocalString.GetString("暂时没有任何公示期物品")
    else
        if this.m_TargetWordId == 0 then
            if not this.m_Publicity then
                CommonDefs.GetComponent_GameObject_Type(this.m_NoShopItemLabel, typeof(UILabel)).text = LocalString.GetString("暂时没有该商品供买卖\n\r可先进行关注")
            else
                CommonDefs.GetComponent_GameObject_Type(this.m_NoShopItemLabel, typeof(UILabel)).text = LocalString.GetString("暂时没有该商品供买卖")
            end
        else
            CommonDefs.GetComponent_GameObject_Type(this.m_NoShopItemLabel, typeof(UILabel)).text = LocalString.GetString("没有包含此词条的装备")
        end
    end
    this.m_NoShopItemLabel:SetActive(this.m_SearchResultData.Count == 0)
    if this.m_SearchResultData.Count == 0 then
        local allFocus = CPlayerShopMgr.Inst:GetAllFocusTemplate()
        if CommonDefs.HashSetContains(allFocus, typeof(UInt32), this.m_TemplateId) then
            this.m_FocusButton.Text = LocalString.GetString("取消关注")
        else
            this.m_FocusButton.Text = LocalString.GetString("关注")
        end
    end
end
CPlayerShop_ItemInfo.m_OnPlayerShopCollectItemSuccess_CS2LuaHook = function (this, shopId, shopPlace, ItemId)
    do
        local i = 0
        while i < this.m_SearchResultData.Count do
            if this.m_SearchResultData[i].ShopId == math.floor(shopId) and this.m_SearchResultData[i].ShelfPlace == shopPlace then
                this.m_SearchResultData[i].IsFocus = true
                if this.m_SelectItemIndex == i then
                    this.m_FocusButton.Text = LocalString.GetString("取消关注")
                end
                local item = TypeAs(this.m_SearchResultTable:GetItemAtRow(i), typeof(CPlayerShopItem))
                if item ~= nil then
                    item:UpdateData(this.m_SearchResultData[i], false, false)
                end
            end
            i = i + 1
        end
    end
end
CPlayerShop_ItemInfo.m_OnCancelPlayerShopCollectItemSuccess_CS2LuaHook = function (this, list)    do
        local j = 0
        while j < list.Count do
            local data = list[j]
            do
                local i = 0
                while i < this.m_SearchResultData.Count do
                    if this.m_SearchResultData[i].ShopId == data.ShopId and this.m_SearchResultData[i].ShelfPlace == data.ShelfPlace then
                        this.m_SearchResultData[i].IsFocus = false
                        if this.m_SelectItemIndex == i then
                            this.m_FocusButton.Text = LocalString.GetString("关注")
                        end
                        local item = TypeAs(this.m_SearchResultTable:GetItemAtRow(i), typeof(CPlayerShopItem))
                        if item ~= nil then
                            item:UpdateData(this.m_SearchResultData[i], false, false)
                        end
                    end
                    i = i + 1
                end
            end
            j = j + 1
        end
    end
end
CPlayerShop_ItemInfo.m_InitWordFilter_CS2LuaHook = function (this, templateId) 
    this.m_TargetWordId = 0
    if this.m_WordFilterButton ~= nil then
        this.m_WordFilterButton.OnClick = MakeDelegateFromCSFunction(this.OnClickWordFilterButton, MakeGenericClass(Action1, QnButton), this)
        local buttonVisible = false
        local data = EquipmentTemplate_Equip.GetData(templateId)
        if data ~= nil then
            local type = data.Type
            local subtype = data.SubType
            --EquipBaptize_WordOption.Foreach(DelegateFactory.Action_uint_EquipBaptize_WordOption(function (id, data2) 
            EquipBaptize_WordOption.Foreach(function (id, data2) 
                if data2.Type == type and data2.SubType == subtype then
                    CommonDefs.ListClear(this.m_FilterWords)
                    CommonDefs.ListAddRange(this.m_FilterWords, data2.Word)
                    CommonDefs.ListInsert(this.m_FilterWords, 0, typeof(UInt32), 0)
                    local list = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
                    do
                        local i = 0
                        while i < this.m_FilterWords.Count do
                            if this.m_FilterWords[i] ~= 0 then
                                local word = Word_Word.GetData(this.m_FilterWords[i] * 100 + 1)
                                CommonDefs.ListAdd(list, typeof(PopupMenuItemData), PopupMenuItemData(word.WordDescription, MakeDelegateFromCSFunction(this.FilterWordAction, MakeGenericClass(Action1, Int32), this), false, nil))
                            end
                            i = i + 1
                        end
                    end
                    CommonDefs.ListInsert(list, 0, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("不限词条"), MakeDelegateFromCSFunction(this.FilterWordAction, MakeGenericClass(Action1, Int32), this), false, nil))
                    this.m_FilterWordActions = CommonDefs.ListToArray(list)
                    buttonVisible = true
                end
            end)
        end
        if this.m_WordFilterDesp ~= nil then
            if buttonVisible then
                this.m_WordFilterDesp.text = LocalString.GetString("可根据词条对装备进行筛选")
            else
                this.m_WordFilterDesp.text = LocalString.GetString("该物品不可进行筛选")
            end
        end
        this.m_WordFilterButton.Text = LocalString.GetString("不限词条")
        this.m_WordFilterButton.Visible = buttonVisible
        this.m_WordFilterDesp.gameObject:SetActive(buttonVisible)
        this:ResetTableViewSize(buttonVisible)
    end
end
CPlayerShop_ItemInfo.m_ResetTableViewSize_CS2LuaHook = function (this, filterButtonVisible) 
    this:InitOriginalHeight()    
    local m_FinalHeight = filterButtonVisible and this.m_TableOriginalHeight or this.m_TableOriginalHeight + 100
    this.m_TableBg.height = m_FinalHeight
    local rects = CommonDefs.GetComponentsInChildren_Component_Type(this.m_SearchResultTable, typeof(UIRect))
    do
        local i = 0
        while i < rects.Length do
            rects[i]:ResetAndUpdateAnchors()
            i = i + 1
        end
    end
    local indicator = CommonDefs.GetComponentInChildren_Component_Type(this.m_SearchResultTable, typeof(UIScrollViewIndicator))
    if indicator ~= nil then
        indicator:Layout()
    end
end
CPlayerShop_ItemInfo.m_OnClickWordFilterButton_CS2LuaHook = function (this, button) 
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(this.m_FilterWordActions, button.transform, AlignType.Bottom, 1, nil, nil, DelegateFactory.Action(function () 
        this.m_WordFilterButton:SetTipStatus(false)
    end), 600,420)
end
CPlayerShop_ItemInfo.m_FilterWordAction_CS2LuaHook = function (this, index) 
    if index >= 0 and index < this.m_FilterWords.Count then
        this.m_TargetWordId = this.m_FilterWords[index]
        this.m_WordFilterButton.Text = this.m_FilterWordActions[index].text
        this:DoSearch(this.m_CurrentPage)
    end
end
CPlayerShop_ItemInfo.m_hookOnPageChanged= function (this, page) 

    --部分道具禁止翻页
    if LuaPlayerShop_ItemInfo:CheckDisablePageChanged(this.m_TemplateId) and page ~= 1 then
        this.m_PageButton:SetValue(1, false)
        this.m_PageButton:OverrideText(System.String.Format("{0}/{1}", this.m_CurrentPage, this.m_TotalPages))
        g_MessageMgr:ShowMessage("YiShi_Forbidd_Pagedown_Tip")
        return
    end

    this:DoSearch(page)
end
LuaPlayerShop_ItemInfo = {}
LuaPlayerShop_ItemInfo.m_SortIndex = 0
LuaPlayerShop_ItemInfo.m_Wnd = nil
LuaPlayerShop_ItemInfo.m_TemplateId = 0
function LuaPlayerShop_ItemInfo:CheckDisablePageChanged(TemplateId)
    local data = Item_Item.GetData(TemplateId)
    if not data then return false end
    local type = data.Type
    local tyepData = Item_Type.GetData(type)
    if not tyepData then return false end
    if tyepData.DisablePageChanged == 2 then
        return CommonDefs.HashSetContains(tyepData.DisablePageChangedItems, typeof(UInt32), TemplateId)
    end
    return tyepData.DisablePageChanged > 0 
end 

function LuaPlayerShop_ItemInfo:Init(this,TemplateId)
    -- local disablePageSprite = this.m_PageButton.transform:Find("DisablePageSprite")
    -- if disablePageSprite then
    --     disablePageSprite.gameObject:SetActive(LuaPlayerShop_ItemInfo:CheckDisablePageChanged(TemplateId)) 
    -- end
    self.m_TemplateId = TemplateId
    self:UpdateOwnedNumLabel()
end

function LuaPlayerShop_ItemInfo:UpdateOwnedNumLabel()
    if not self.m_Wnd then return end
    local ownedNumLabel = self.m_Wnd.transform:Find("OwnedNumLabelRoot/OwnedNumLabel")
    if ownedNumLabel then
        local hasCount = CItemMgr.Inst:GetItemCount(self.m_TemplateId)
        ownedNumLabel.gameObject:SetActive(IdPartition.IdIsItem(self.m_TemplateId))
        ownedNumLabel:GetComponent(typeof(UILabel)).text = IdPartition.IdIsItem(self.m_TemplateId) and SafeStringFormat3(LocalString.GetString("已拥有:%d"),hasCount) or ""
    end
end

function LuaPlayerShop_ItemInfo:CheckConfirmDisplaySplitItemId(data)
    if not self.m_Wnd then return false end
    if LuaPlayerShopMgr.IsDisplaySplitItemId(self.m_Wnd.m_TemplateId) then
        local num = 0
        for i = 0,self.m_Wnd.m_SearchResultData.Count - 1 do
            if self.m_Wnd.m_SearchResultData[i].Item and self.m_Wnd.m_SearchResultData[i].ShopId == data.ShopId and self.m_Wnd.m_SearchResultData[i].ShelfPlace == data.ShelfPlace then
                num = num + 1
            end
        end
        if num > 1 and data.Item then
            return true
        end
    end
    return false
end

function LuaPlayerShop_ItemInfo:OnEnable(this)
    self.m_Wnd = this
    g_ScriptEvent:AddListener("SendItem", self, "SendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "SetItemAt")
end

function LuaPlayerShop_ItemInfo:OnDisable()
    self.m_Wnd = nil
    g_ScriptEvent:RemoveListener("SendItem", self, "SendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "SetItemAt")
end

function LuaPlayerShop_ItemInfo:SendItem()
    self:UpdateOwnedNumLabel()
end

function LuaPlayerShop_ItemInfo:SetItemAt()
    self:UpdateOwnedNumLabel()
end