local TweenAlpha = import "TweenAlpha"
local CUITexture = import "L10.UI.CUITexture"

CLuaWuCaiShaBingDetailWnd = class()

function CLuaWuCaiShaBingDetailWnd:Init()
	local colorTbl = {"FF4343", "F3D24F", "46EC78"}
	local teamNameTbl = {LocalString.GetString("[%s]红队沙冰[-]"), LocalString.GetString("[%s]黄队沙冰[-]"), LocalString.GetString("[%s]绿队沙冰[-]")}
	local showGroupId = LuaWuCaiShaBingMgr.m_ShowGroupId
	local playerGroupId = LuaWuCaiShaBingMgr.m_PlayerGroupId
	local progressData = LuaWuCaiShaBingMgr.m_ProgressData

	local titleLabel = self.transform:Find("Anchor/Title/Label"):GetComponent(typeof(UILabel))
	titleLabel.text = SafeStringFormat3(teamNameTbl[showGroupId], colorTbl[showGroupId])

	local submitBtn = self.transform:Find("Anchor/SubmitButton").gameObject
	local stealBtn = self.transform:Find("Anchor/StealButton").gameObject
	local teamSprite = self.transform:Find("Anchor/Title/Sprite").gameObject
	if showGroupId == playerGroupId then
		teamSprite:SetActive(true)
		titleLabel.gameObject.transform.localPosition = Vector3(23, 0, 0)

		submitBtn:SetActive(true)
		stealBtn:SetActive(false)

		-- 注册提交点击事件
		CommonDefs.AddOnClickListener(submitBtn, DelegateFactory.Action_GameObject(function()
			Gac2Gas.RequestSubmitMaterial()
		end), false)
	else
		teamSprite:SetActive(false)
		titleLabel.gameObject.transform.localPosition = Vector3(0, 0, 0)

		submitBtn:SetActive(false)
		stealBtn:SetActive(true)

		-- 注册偷取的事件
		local materialInfo = QingLiangYiXia_Setting.GetData().MaterialInfo
		for i = 0, materialInfo.Length - 1 do
			local childStealBtn = stealBtn.transform:Find("StealBtn" .. tostring(i + 1)).gameObject
			CommonDefs.AddOnClickListener(childStealBtn, DelegateFactory.Action_GameObject(function()
				Gac2Gas.RequestStealMaterial(materialInfo[i])
			end), false)
		end
	end

	local textureTbl = {"shabing_tao.mat", "shabing_mangguo.mat", "shabing_xigua.mat"}
	local texture = self.transform:Find("Anchor/Progress1/Texture"):GetComponent(typeof(CUITexture))
	texture:LoadMaterial("UI/Texture/Transparent/Material/" .. textureTbl[showGroupId], false, nil)

	local nameTbl = {LocalString.GetString("桃子"), LocalString.GetString("芒果"), LocalString.GetString("西瓜")}
	local nameLabel = self.transform:Find("Anchor/Progress1/Name"):GetComponent(typeof(UILabel))
	nameLabel.text = nameTbl[showGroupId]

	for i = 1, #progressData do
		if i % 2 == 0 then
			local idx = math.floor(i / 2)
			local progressGo = self.transform:Find("Anchor/Progress" .. tostring(idx)).gameObject
				
			local label = progressGo.transform:Find("Label"):GetComponent(typeof(UILabel))
			label.text = SafeStringFormat3("%d/%d", progressData[i - 1], progressData[i])
			
			local progressBar = progressGo.transform:Find("Progress Bar"):GetComponent(typeof(UISlider))
			progressBar.sliderValue = progressData[i] == 0 and 0 or progressData[i - 1] / progressData[i]

			local progressLabel = progressGo.transform:Find("Progress Bar/Foreground"):GetComponent(typeof(UISprite))
			progressLabel.color = NGUIText.ParseColor24(colorTbl[showGroupId], 0)
		end
	end
end

function CLuaWuCaiShaBingDetailWnd:WuCaiShaBingSubmitMaterialDone(itemTemplateId)
	local materialInfo = QingLiangYiXia_Setting.GetData().MaterialInfo
	local idx = 1
	for i = 0, materialInfo.Length - 1 do
		if itemTemplateId == materialInfo[i] then
			idx = i + 2
		end
	end

	local Label = self.transform:Find("Anchor/Progress" .. tostring(idx) .. "/Tip"):GetComponent(typeof(UILabel))
	Label.text = "[58F587]+1[-]"
	Label.alpha = 1
	TweenAlpha.Begin(Label.gameObject, 1, 0)
end

function CLuaWuCaiShaBingDetailWnd:WuCaiShaBingStealMaterialDone(itemTemplateId)
	local materialInfo = QingLiangYiXia_Setting.GetData().MaterialInfo
	local idx = 1
	for i = 0, materialInfo.Length - 1 do
		if itemTemplateId == materialInfo[i] then
			idx = i + 1
		end
	end

	local Label = self.transform:Find("Anchor/Progress" .. tostring(idx) .. "/Tip"):GetComponent(typeof(UILabel))
	Label.text = "[F55875]-1[-]"
	Label.alpha = 1
	TweenAlpha.Begin(Label.gameObject, 1, 0)
end

function CLuaWuCaiShaBingDetailWnd:OnEnable()
	g_ScriptEvent:AddListener("WuCaiShaBingSubmitMaterialDone", self, "WuCaiShaBingSubmitMaterialDone")
	g_ScriptEvent:AddListener("WuCaiShaBingStealMaterialDone", self, "WuCaiShaBingStealMaterialDone")
end

function CLuaWuCaiShaBingDetailWnd:OnDisable()
	g_ScriptEvent:RemoveListener("WuCaiShaBingSubmitMaterialDone", self, "WuCaiShaBingSubmitMaterialDone")
	g_ScriptEvent:RemoveListener("WuCaiShaBingStealMaterialDone", self, "WuCaiShaBingStealMaterialDone")
end

return CLuaWuCaiShaBingDetailWnd