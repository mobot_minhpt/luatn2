local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local QnTableView = import "L10.UI.QnTableView"
local GameObject = import "UnityEngine.GameObject"
local NGUIText = import "NGUIText"

LuaSectHongbaoDetailNewWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaSectHongbaoDetailNewWnd, "Portrait", "Portrait", CUITexture)
RegistChildComponent(LuaSectHongbaoDetailNewWnd, "Name", "Name", UILabel)
RegistChildComponent(LuaSectHongbaoDetailNewWnd, "Description", "Description", UILabel)
RegistChildComponent(LuaSectHongbaoDetailNewWnd, "QnTableView", "QnTableView", QnTableView)
RegistChildComponent(LuaSectHongbaoDetailNewWnd, "CostLabel", "CostLabel", UILabel)
RegistChildComponent(LuaSectHongbaoDetailNewWnd, "ProgressLabel", "ProgressLabel", UILabel)
RegistChildComponent(LuaSectHongbaoDetailNewWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaSectHongbaoDetailNewWnd, "BottomStateLabel", "BottomStateLabel", UILabel)
--@endregion RegistChildComponent end
RegistClassMember(LuaSectHongbaoDetailNewWnd, "m_SectHongBaoId")

function LuaSectHongbaoDetailNewWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)

	self.QnTableView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return LuaZongMenMgr.m_SectHongBaoItemDetailData and #LuaZongMenMgr.m_SectHongBaoItemDetailData or 0
    end, function(item, index)
        self:InitItem(item, index)
    end)
    --@endregion EventBind end
end

function LuaSectHongbaoDetailNewWnd:OnEnable()
	g_ScriptEvent:AddListener("OnSendSectItemPackDetail",self,"Init")
end

function LuaSectHongbaoDetailNewWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSendSectItemPackDetail",self,"Init")
end

function LuaSectHongbaoDetailNewWnd:Init()
	self.m_SectHongBaoId = LuaZongMenMgr.m_SectHongBaoId
	local playerData = LuaZongMenMgr.m_SectHongBaoItemPlayerData
	if playerData then
		local class = playerData.class
		local gender = playerData.gender
		local ownerName = playerData.ownerName
		local content = playerData.content
		local finishPlayerCount = playerData.finishPlayerCount
		local jade = playerData.jade
		local state = playerData.state
		local totalCount = playerData.totalCount
		self.Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(class, gender, -1), false)
		self.Name.text = ownerName
		self.Description.text = content
		self.ProgressLabel.color = Color.white
		self.ProgressLabel.text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3(LocalString.GetString("[6C1F12]已领取[bd3e40]%d/%d"),finishPlayerCount,totalCount))
		self.CostLabel.text = jade
		self.BottomStateLabel.gameObject:SetActive(state ~= 0)
		self.Button.gameObject:SetActive(state == 0)
		self.BottomStateLabel.text = state == 1 and LocalString.GetString("抽奖已结束") or LocalString.GetString("已过期")
	end
	self.QnTableView:ReloadData(true,true)
end

function LuaSectHongbaoDetailNewWnd:InitItem(item, index)
	local data = LuaZongMenMgr.m_SectHongBaoItemDetailData[index + 1]
	local playerName = data.playerName
	local itemId = data.itemId
	local itemCount = data.itemCount
	local status = data.status
	local playerId = data.playerId
	local itemData = Item_Item.GetData(itemId)
	local pos = data.pos
	local isBest = data.isBest

	local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local infoLabel = item.transform:Find("InfoLabel"):GetComponent(typeof(UILabel))
	local highLight = item.transform:Find("HighLight").gameObject
	local best = item.transform:Find("Best").gameObject
	
	nameLabel.text = playerName
	nameLabel.color = status == 1 and NGUIText.ParseColor24("ff5a00", 0) or NGUIText.ParseColor24("D2704D", 0)
	infoLabel.color = status == 1 and NGUIText.ParseColor24("ff5a00", 0) or NGUIText.ParseColor24("D2704D", 0)
	highLight.gameObject:SetActive(CClientMainPlayer.Inst and playerId == CClientMainPlayer.Inst.Id)
	if CClientMainPlayer.Inst and playerId == CClientMainPlayer.Inst.Id then
		local color = NGUIText.ParseColor24("fffd66", 0)
		nameLabel.color = color
		infoLabel.color = color
	end
	local text = LocalString.GetString("抽奖中")
	if itemData and status == 0 then
		text = itemCount == 1 and SafeStringFormat3(LocalString.GetString("获得%s"),itemData.Name) or SafeStringFormat3(LocalString.GetString("获得%sx%d"),itemData.Name,itemCount)
	elseif status == 2 then
		text = SafeStringFormat3(LocalString.GetString("排队第%s位"),pos)
	end
	infoLabel.text =  text
	best.gameObject:SetActive(isBest)
end

--@region UIEvent

function LuaSectHongbaoDetailNewWnd:OnButtonClick()
	LuaZongMenMgr.m_SectHongBaoId = self.m_SectHongBaoId
	CUIManager.ShowUI(CLuaUIResources.SectHongBaoLotteryWnd)
end

--@endregion UIEvent

