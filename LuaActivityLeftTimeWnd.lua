local CTeamQuickJoinInfoMgr = import "L10.UI.CTeamQuickJoinInfoMgr"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CTeamMatchMgr = import "L10.Game.CTeamMatchMgr"
local CActivityLeftTimeWnd = import "L10.UI.CActivityLeftTimeWnd"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local QnTableView = import "L10.UI.QnTableView"


CLuaActivityLeftTimeWnd = class()
RegistClassMember(CLuaActivityLeftTimeWnd,"titleLabel")
RegistClassMember(CLuaActivityLeftTimeWnd,"finishLabel")
RegistClassMember(CLuaActivityLeftTimeWnd,"tableView")
RegistClassMember(CLuaActivityLeftTimeWnd,"refreshBtn")
RegistClassMember(CLuaActivityLeftTimeWnd,"createTeamBtn")
RegistClassMember(CLuaActivityLeftTimeWnd,"joinTeamBtn")
RegistClassMember(CLuaActivityLeftTimeWnd,"closeBtn")
RegistClassMember(CLuaActivityLeftTimeWnd,"mInfoList")

function CLuaActivityLeftTimeWnd:Awake()
    self.titleLabel = self.transform:Find("Anchor/TitleLabel"):GetComponent(typeof(UILabel))
    self.finishLabel = self.transform:Find("Anchor/FinishLabel"):GetComponent(typeof(UILabel))
    self.tableView = self.transform:Find("Anchor/InfoTable"):GetComponent(typeof(QnTableView))

    self.mInfoList = {}

    self.tableView.m_DataSource = DefaultTableViewDataSource.Create(function() 
        return #self.mInfoList
    end, function(item, index)
        self:InitItem(item.transform,index)
    end)

    self.refreshBtn = self.transform:Find("RefreshBtn").gameObject
    self.createTeamBtn = self.transform:Find("CreateTeamBtn").gameObject
    self.joinTeamBtn = self.transform:Find("JoinTeamBtn").gameObject

    UIEventListener.Get(self.refreshBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        self:GetDynamicActivityInfo()
    end)

    UIEventListener.Get(self.createTeamBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        if not CTeamMgr.Inst:TeamExists() then
            local findId = self:GetTeamMatchId()
            CTeamMatchMgr.Inst:CreateTeam(findId)
        end
        if not CUIManager.IsLoaded(CUIResources.TeamWnd) then
            CUIManager.ShowUI(CUIResources.TeamWnd)
        end
    end)
    UIEventListener.Get(self.joinTeamBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        --快捷组队
        local findId = self:GetTeamMatchId()
        CTeamQuickJoinInfoMgr.Inst:ShowTeamQuickJoinWndWithActivityId(findId)
    end)
end

function CLuaActivityLeftTimeWnd:Init()
    self.finishLabel.enabled = false
    self:GetDynamicActivityInfo()
end

function CLuaActivityLeftTimeWnd:InitItem(tf,index)
    local btn = tf:Find("GotoBtn").gameObject
    local info = self.mInfoList[index+1]
    UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function (p) 
        if info then
            CUIManager.CloseUI(CUIResources.ActivityLeftTimeWnd)
            Gac2Gas.RequestGoDynamicActivitySite(info.activityType, info.sceneTemplateId, info.extra)
        end
    end)
    local nameLabel = tf:Find("MapLabel"):GetComponent(typeof(UILabel))
    if info.sceneName ~= "" then
        nameLabel.text = info.sceneName
    else
        local map = PublicMap_PublicMap.GetData(info.sceneTemplateId)
        if map then
            nameLabel.text = map.Name
        else
            nameLabel.text = ""
        end
    end
    local leftNumLabel = tf:Find("LeftTimeLabel"):GetComponent(typeof(UILabel))
    leftNumLabel.text = SafeStringFormat3(LocalString.GetString("剩余%d"), info.leftNum)
end

function CLuaActivityLeftTimeWnd:GetDynamicActivityInfo( )
    local data = Task_QueryNpc.GetData(CActivityLeftTimeWnd.activityType)
    if data ~= nil then
        self.titleLabel.text = data.ActivityName
        Gac2Gas.GetDynamicActivityInfo(CActivityLeftTimeWnd.activityType)
    end
end

function CLuaActivityLeftTimeWnd:GetTeamMatchId( )
    local findId = 0
    local data = Task_QueryNpc.GetData(CActivityLeftTimeWnd.activityType)
    if data ~= nil then
        TeamMatch_Activities.Foreach(function (key, val) 
            if val.LinkedScheduleID == data.ScheduleID then
                findId = key
            end
        end)
    end
    return findId
end
function CLuaActivityLeftTimeWnd:OnTeamInfoChanged( )
    if CTeamMgr.Inst:TeamExists() then
        self:Close()
        if not CUIManager.IsLoaded(CUIResources.TeamWnd) then
            CUIManager.ShowUI(CUIResources.TeamWnd)
        end
    end
end
function CLuaActivityLeftTimeWnd:OnReplyDynamicActivityInfo( args ) 
    local type, currentCount, maxCount, list = args[0],args[1],args[2],args[3]
    self.mInfoList = {}
    for i=1,list.Count do
        table.insert( self.mInfoList,list[i-1] )
    end
    if currentCount >= maxCount then
        self.finishLabel.enabled = true
    end
    self.tableView:ReloadData(true, false)
end
function CLuaActivityLeftTimeWnd:NumberOfRows( view) 
    if self.mInfoList ~= nil then
        return self.mInfoList.Count
    end
    return 0
end
function CLuaActivityLeftTimeWnd:OnEnable()
    g_ScriptEvent:AddListener("ReplyDynamicActivityInfo",self,"OnReplyDynamicActivityInfo")

end
function CLuaActivityLeftTimeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReplyDynamicActivityInfo",self,"OnReplyDynamicActivityInfo")
end