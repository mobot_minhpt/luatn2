local CScene = import "L10.Game.CScene"
local CTaskAndTeamWnd = import "L10.UI.CTaskAndTeamWnd"
local CFacialMgr = import "L10.Game.CFacialMgr"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"

if rawget(_G, "LuaYaoYeManJuanMgr") then
    g_ScriptEvent:RemoveListener("GasDisconnect", LuaYaoYeManJuanMgr, "OnGasDisconnect")
end

LuaYaoYeManJuanMgr = class()

g_ScriptEvent:AddListener("GasDisconnect", LuaYaoYeManJuanMgr, "OnGasDisconnect")

LuaYaoYeManJuanMgr.m_GamePlayId = nil
LuaYaoYeManJuanMgr.m_Stage = nil
LuaYaoYeManJuanMgr.m_CurLeaf = 0
LuaYaoYeManJuanMgr.m_TaskName = ""
LuaYaoYeManJuanMgr.m_Desc = ""
LuaYaoYeManJuanMgr.m_PlayerId2FacialId = {}

LuaYaoYeManJuanMgr.m_IconStatus = false

function LuaYaoYeManJuanMgr:OnGasDisconnect() -- 断线的时候要重置一下,因为可能会错过关闭的rpc
    self.m_IconStatus = false
end

function LuaYaoYeManJuanMgr:OnMainPlayerCreated(playId)
    if CTaskAndTeamWnd.Instance then
        CTaskAndTeamWnd.Instance:ChangeToTaskTab()
    end
    g_ScriptEvent:AddListener("UpdateGamePlayStageInfo", self, "OnUpdateGamePlayStageInfo")
    g_ScriptEvent:AddListener("ClientObjCreate", self, "OnClientObjCreate")
end

function LuaYaoYeManJuanMgr:OnMainPlayerDestroyed()
    g_ScriptEvent:RemoveListener("UpdateGamePlayStageInfo", self, "OnUpdateGamePlayStageInfo")
    g_ScriptEvent:RemoveListener("ClientObjCreate", self, "OnClientObjCreate")
    self.m_PlayerId2FacialId = {}
end

function LuaYaoYeManJuanMgr:OnUpdateGamePlayStageInfo(gameplayId, title, content)
	if gameplayId == self:GetGamePlayId() then
		self.m_TaskName = title
        self.m_Desc = content
        g_ScriptEvent:BroadcastInLua("UpdateYaoYeManJuanStageInfo")
    end
end

function LuaYaoYeManJuanMgr:OnClientObjCreate(args)
    local obj = CClientObjectMgr.Inst:GetObject(args[0])
    if TypeIs(obj, typeof(CClientOtherPlayer)) then
        local playerId = TypeAs(obj, typeof(CClientOtherPlayer)).PlayerId
        if self.m_PlayerId2FacialId[playerId] then
            self:SetFacial(playerId, self.m_PlayerId2FacialId[playerId])
        end
    end
end

function LuaYaoYeManJuanMgr:GetGamePlayId()
    if self.m_GamePlayId == nil then
        self.m_GamePlayId = tonumber(YaoYeManJuan_Setting.GetData("GameplayId").Value)
    end
    return self.m_GamePlayId
end

function LuaYaoYeManJuanMgr:IsInPlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == self:GetGamePlayId() then
            return true
        end
    end
    return false
end


function LuaYaoYeManJuanMgr:YaoYeManJuan_SyncPlayState(PlayState, EnterStateTime, NextStateTime, ExtraInfoTbl)
    self.m_Stage = PlayState
    if self.m_Stage == EnumYaoYeManJuanPlayState.Stage_1 then
        self.m_CurLeaf = (ExtraInfoTbl or {0})[1]
    elseif self.m_Stage == EnumYaoYeManJuanPlayState.Stage_2 then
        ExtraInfoTbl = ExtraInfoTbl or {}
        for k, v in pairs(ExtraInfoTbl) do
            self:SetFacial(k, v)
        end
    end
    g_ScriptEvent:BroadcastInLua("YaoYeManJuan_SyncPlayState", PlayState, EnterStateTime, NextStateTime, ExtraInfoTbl)
end

function LuaYaoYeManJuanMgr:SetFacial(playerId, facialId)
    self.m_PlayerId2FacialId[playerId] = facialId
    local obj = CClientPlayerMgr.Inst:GetPlayer(playerId)
    if obj == nil or obj.RO == nil then
        return
    end
    local data = YaoYeManJuan_Facial.GetData(facialId)
    if data == nil then
        return
    end
    CFacialMgr.LoadAvatarDataByPath(obj.RO, SafeStringFormat3("Assets/Res/Character/Player3/FaceData/%s.ms.asset", data.FacialKey))
end