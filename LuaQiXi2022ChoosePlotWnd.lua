local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local BoxCollider = import "UnityEngine.BoxCollider"

LuaQiXi2022ChoosePlotWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "TeamChannelButton", "TeamChannelButton", GameObject)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "PlotButton", "PlotButton", GameObject)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "TopLabel", "TopLabel", UILabel)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "LeftTexture", "LeftTexture", CUITexture)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "RightTexture", "RightTexture", CUITexture)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "LeftNameLabel", "LeftNameLabel", UILabel)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "LeftChooseLabel", "LeftChooseLabel", UILabel)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "LeftPlayerTexture1", "LeftPlayerTexture1", CUITexture)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "LeftPlayerTexture2", "LeftPlayerTexture2", CUITexture)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "LeftHighlight", "LeftHighlight", GameObject)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "RightNameLabel", "RightNameLabel", UILabel)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "RightChooseLabel", "RightChooseLabel", UILabel)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "RightPlayerTexture1", "RightPlayerTexture1", CUITexture)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "RightPlayerTexture2", "RightPlayerTexture2", CUITexture)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "RightHighlight", "RightHighlight", GameObject)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "LeftChoose", "LeftChoose", GameObject)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "RightChoose", "RightChoose", GameObject)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "LeftArrow", "LeftArrow", GameObject)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "RightArrow", "RightArrow", GameObject)
RegistChildComponent(LuaQiXi2022ChoosePlotWnd, "Fx", "Fx", CUIFx)

--@endregion RegistChildComponent end
RegistClassMember(LuaQiXi2022ChoosePlotWnd, "m_ChooseOptions")
RegistClassMember(LuaQiXi2022ChoosePlotWnd, "m_OtherPlayerChooseOptions")
RegistClassMember(LuaQiXi2022ChoosePlotWnd, "m_LeftChooseData")
RegistClassMember(LuaQiXi2022ChoosePlotWnd, "m_RightChooseData")
RegistClassMember(LuaQiXi2022ChoosePlotWnd, "m_PortraitName1")
RegistClassMember(LuaQiXi2022ChoosePlotWnd, "m_PortraitName2")

function LuaQiXi2022ChoosePlotWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TeamChannelButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTeamChannelButtonClick()
	end)


	
	UIEventListener.Get(self.LeftChoose.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeftChooseClick()
	end)


	
	UIEventListener.Get(self.RightChoose.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightChooseClick()
	end)


    UIEventListener.Get(self.PlotButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPlotButtonClick()
	end)
	--@endregion EventBind end
end

function LuaQiXi2022ChoosePlotWnd:Init()
	self.Fx:LoadFx("fx/ui/prefab/UI_mgd_juqing_ye.prefab")
	self.LeftPlayerTexture1.gameObject:SetActive(true)
	self.LeftPlayerTexture1:LoadMaterial(nil)
	self.RightPlayerTexture1.gameObject:SetActive(true)
	self.RightPlayerTexture1:LoadMaterial(nil)
	self.LeftPlayerTexture2.gameObject:SetActive(true)
	self.LeftPlayerTexture2:LoadMaterial(nil)
	self.RightPlayerTexture2.gameObject:SetActive(true)
	self.RightPlayerTexture2:LoadMaterial(nil)
	self.LeftArrow.gameObject:SetActive(false)
	self.RightArrow.gameObject:SetActive(false)
	self:OnSyncOptionInfo()
end

function LuaQiXi2022ChoosePlotWnd:OnEnable()
	g_ScriptEvent:AddListener("SFEQ_SyncOptionInfo", self, "OnSyncOptionInfo")
	g_ScriptEvent:AddListener("SFEQ_ChangeOptionText", self, "OnChangeOptionText")
	g_ScriptEvent:AddListener("SFEQ_OnOptionCountDownTick", self, "OnOptionCountDownTick")
	g_ScriptEvent:AddListener("SFEQ_PlayerClickOption", self, "OnPlayerClickOption")
end

function LuaQiXi2022ChoosePlotWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SFEQ_SyncOptionInfo", self, "OnSyncOptionInfo")
	g_ScriptEvent:RemoveListener("SFEQ_ChangeOptionText", self, "OnChangeOptionText")
	g_ScriptEvent:RemoveListener("SFEQ_OnOptionCountDownTick", self, "OnOptionCountDownTick")
	g_ScriptEvent:RemoveListener("SFEQ_PlayerClickOption", self, "OnPlayerClickOption")
end

function LuaQiXi2022ChoosePlotWnd:OnSyncOptionInfo()
	local optionData = LuaQiXi2022Mgr.m_OptionData
	local textStatus = tonumber(optionData.textStatus) 
    local countdown = tonumber(optionData.countdown)
    local options = optionData.options
	local playerinfo = optionData.playerInfo 
	for _, info in pairs(playerinfo) do
		local id, name, class, gender = info[1], info[2], info[3], info[4]
		if CClientMainPlayer.Inst and id == CClientMainPlayer.Inst.Id then
			self.m_PortraitName1 = CUICommonDef.GetPortraitName(class, gender, -1)
		else
			self.m_PortraitName2 = CUICommonDef.GetPortraitName(class, gender, -1)
		end
	end

	local leftData,rightData 
	local leftStatus, rightStatus
	for optionId, t in pairs(options) do
		local data = QiXi2022_SFEQAnswerDescription.GetData(optionId)
		if not leftData then
			leftData = data
			leftStatus = t.status
		else
			rightData = data
			rightStatus = t.status
		end
	end
	if leftData and rightData then
		if leftData.ID > rightData.ID then
			local tempData = leftData
			local tempStatus = leftStatus
			leftData = rightData
			leftStatus = rightStatus
			rightData = tempData 
			rightStatus = tempStatus
		end
	end

	self.LeftHighlight.gameObject:SetActive(false)
	self.RightHighlight.gameObject:SetActive(false)

	if leftData then
		self.LeftNameLabel.text = leftData.Name
		self.LeftChooseLabel.text = leftData.Text
		self.LeftTexture:LoadMaterial(leftData.PortraitName) 
		self.LeftTexture.gameObject:SetActive(not String.IsNullOrEmpty(leftData.PortraitName))
		self.m_LeftChooseData = leftData
		local locked = leftStatus == false
		Extensions.SetLocalPositionZ(self.LeftChoose.transform, locked and -1 or 0)
	end

	if rightData then
		self.RightNameLabel.text = rightData.Name
		self.RightChooseLabel.text = rightData.Text
		self.RightTexture:LoadMaterial(rightData.PortraitName) 
		self.RightTexture.gameObject:SetActive(not String.IsNullOrEmpty(rightData.PortraitName))
		self.m_RightChooseData = rightData
		local locked = rightStatus == false
		Extensions.SetLocalPositionZ(self.RightChoose.transform, locked and -1 or 0)
	end

	for optionId, arr in pairs(options) do
		for _, playerId in pairs(arr.players) do
			self:OnPlayerClickOption(playerId, optionId)
		end
	end

	self:OnChangeOptionText(textStatus)
	self:OnOptionCountDownTick(countdown)
end

function LuaQiXi2022ChoosePlotWnd:OnChangeOptionText(textStatus)
	local playerName1, playerName2 = self.LeftNameLabel.text, self.RightNameLabel.text
	local choosePlayerName = self.m_ChooseOptions == 1 and playerName1 or playerName2
	if textStatus == 1 then
		self.TopLabel.text = g_MessageMgr:FormatMessage("QiXi2022ChoosePlotWnd_Top1",playerName1, playerName2)
	elseif textStatus == 2 then
		self.TopLabel.text = g_MessageMgr:FormatMessage("QiXi2022ChoosePlotWnd_Top2")
	elseif textStatus == 3 then
		self.TopLabel.text = g_MessageMgr:FormatMessage("QiXi2022ChoosePlotWnd_Top3",choosePlayerName)
	end
end

function LuaQiXi2022ChoosePlotWnd:OnOptionCountDownTick(countDown)
	self.CountDownLabel.text = countDown
end

function LuaQiXi2022ChoosePlotWnd:OnPlayerClickOption(playerId, optionId)
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == playerId and self.m_PortraitName1 then
		if self.m_LeftChooseData.ID == optionId then
			self.m_ChooseOptions = 1
			self.LeftPlayerTexture1:LoadNPCPortrait(self.m_PortraitName1)
			self.RightPlayerTexture1:LoadMaterial(nil)
		elseif self.m_RightChooseData.ID == optionId then
			self.m_ChooseOptions = 2
			self.LeftPlayerTexture1:LoadMaterial(nil)
			self.RightPlayerTexture1:LoadNPCPortrait(self.m_PortraitName1)
		end
		self.LeftHighlight.gameObject:SetActive(self.m_ChooseOptions == 1)
		self.RightHighlight.gameObject:SetActive(self.m_ChooseOptions == 2)
		self.LeftArrow.gameObject:SetActive(self.m_ChooseOptions == 1)
		self.RightArrow.gameObject:SetActive(self.m_ChooseOptions == 2)
	elseif self.m_PortraitName2 then
		if self.m_LeftChooseData.ID == optionId then
			self.LeftPlayerTexture2:LoadNPCPortrait(self.m_PortraitName2)
			self.RightPlayerTexture2:LoadMaterial(nil)
		elseif self.m_RightChooseData.ID == optionId then
			self.LeftPlayerTexture2:LoadMaterial(nil)
			self.RightPlayerTexture2:LoadNPCPortrait(self.m_PortraitName2)
		end
	end
end

--@region UIEvent

function LuaQiXi2022ChoosePlotWnd:OnTeamChannelButtonClick()
	CSocialWndMgr.ShowChatWnd(EChatPanel.Team)
end

function LuaQiXi2022ChoosePlotWnd:OnLeftChooseClick()
	self.m_ChooseOptions = 1
	self:OnChoose()
end

function LuaQiXi2022ChoosePlotWnd:OnRightChooseClick()
	self.m_ChooseOptions = 2
	self:OnChoose()
end

function LuaQiXi2022ChoosePlotWnd:OnChoose()
	if self.m_LeftChooseData and self.m_RightChooseData then
		local chooseOptionId = self.m_ChooseOptions == 1 and self.m_LeftChooseData.ID or self.m_RightChooseData.ID
		Gac2Gas.SFEQ_ClickOption(chooseOptionId)
	end
end


function LuaQiXi2022ChoosePlotWnd:OnPlotButtonClick()
	LuaQiXi2022Mgr.m_PlotWndShowFlowChart = true
	LuaQiXi2022Mgr.m_PlotWndResultStage = -1 
	CUIManager.ShowUI(CLuaUIResources.QiXi2022PlotWnd)
end
--@endregion UIEvent
