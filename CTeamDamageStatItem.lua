-- Auto Generated!!
local CTeamDamageStatItem = import "L10.UI.CTeamDamageStatItem"
CTeamDamageStatItem.m_Init_CS2LuaHook = function (this, data, isSelf) 
    this.rankLabel.color = isSelf and this.SelfInfoTextColor or this.OtherInfoTextColor
    this.nameLabel.color = isSelf and this.SelfInfoTextColor or this.OtherInfoTextColor
    this.damageValueLabel.color = isSelf and this.SelfInfoTextColor or this.OtherInfoTextColor
    this.damagePercentageLabel.color = isSelf and this.SelfInfoTextColor or this.OtherInfoTextColor

    this.rankLabel.text = tostring(data.index)
    this.nameLabel.text = data.playerName
    this.damageValueLabel.text = tostring(data.val)
    this.damagePercentageLabel.text = System.String.Format("{0:F1}%", data.percentage * 100)
end
