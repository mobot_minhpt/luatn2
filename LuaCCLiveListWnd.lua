require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CButton = import "L10.UI.CButton"
local UITable = import "UITable"
local UIInput = import "UIInput"
local CCCChatMgr = import "L10.Game.CCCChatMgr"
local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CCCLiveListItem = import "L10.UI.CCCLiveListItem"
local Vector3 = import "UnityEngine.Vector3"
local Vector4 = import "UnityEngine.Vector4"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local CCMiniAPIMgr = import "L10.Game.CCMiniAPIMgr"

LuaCCLiveListWnd = class()
RegistClassMember(LuaCCLiveListWnd,"m_closeBtn")

RegistClassMember(LuaCCLiveListWnd,"m_LabelRoot")
RegistClassMember(LuaCCLiveListWnd,"m_LabelScrollView")
RegistClassMember(LuaCCLiveListWnd,"m_LabelTemplate")
RegistClassMember(LuaCCLiveListWnd,"m_LabelTable")

RegistClassMember(LuaCCLiveListWnd,"m_SearchBar")
RegistClassMember(LuaCCLiveListWnd,"m_SearchButton")
RegistClassMember(LuaCCLiveListWnd,"m_BackButton")
RegistClassMember(LuaCCLiveListWnd,"m_Input")

RegistClassMember(LuaCCLiveListWnd,"m_LabelRootBtnTable")
RegistClassMember(LuaCCLiveListWnd,"m_LabelRootShowSearchButton")
RegistClassMember(LuaCCLiveListWnd,"m_LabelRootBindCCButton")
RegistClassMember(LuaCCLiveListWnd,"m_SearchBarBindCCButton")
--一键直播
RegistClassMember(LuaCCLiveListWnd,"m_LabelRootCCBroadcastLiveButton")
RegistClassMember(LuaCCLiveListWnd,"m_SearchBarCCBroadcastLiveButton")
RegistClassMember(LuaCCLiveListWnd,"m_SearchBarBtnTable")

RegistClassMember(LuaCCLiveListWnd,"m_TableViewDataSource")
RegistClassMember(LuaCCLiveListWnd,"m_TableView")
RegistClassMember(LuaCCLiveListWnd,"m_NoZhuboHintLabel")

RegistClassMember(LuaCCLiveListWnd,"m_FollowingLabel")
RegistClassMember(LuaCCLiveListWnd,"m_AllLabel")
RegistClassMember(LuaCCLiveListWnd,"m_LabelButtons")
RegistClassMember(LuaCCLiveListWnd,"m_DisplayResult")

function LuaCCLiveListWnd:Awake()
    
end

function LuaCCLiveListWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaCCLiveListWnd:Init()
	self.m_closeBtn = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
	CommonDefs.AddOnClickListener(self.m_closeBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

	self.m_LabelRoot = self.transform:Find("Anchor/Head/LabelRoot").gameObject
	self.m_LabelScrollView =  self.transform:Find("Anchor/Head/LabelRoot/ScrollView"):GetComponent(typeof(UIScrollView))
	self.m_LabelTemplate = self.transform:Find("Anchor/Head/LabelRoot/ScrollView/LabelButton").gameObject
	self.m_LabelTable = self.transform:Find("Anchor/Head/LabelRoot/ScrollView/LabelTable"):GetComponent(typeof(UITable))

	self.m_LabelTemplate:SetActive(false)
	CUICommonDef.ClearTransform(self.m_LabelTable.transform)

	self.m_SearchBar = self.transform:Find("Anchor/Head/SearchBar").gameObject
	self.m_SearchButton = self.transform:Find("Anchor/Head/SearchBar/SearchButton").gameObject
	self.m_BackButton = self.transform:Find("Anchor/Head/SearchBar/BackButton").gameObject
	self.m_Input = self.transform:Find("Anchor/Head/SearchBar/Input"):GetComponent(typeof(UIInput))

	self.m_LabelRootBtnTable = self.m_LabelRoot.transform:Find("Table"):GetComponent(typeof(UITable))
	self.m_LabelRootShowSearchButton = self.m_LabelRoot.transform:Find("Table/ShowSearchButton").gameObject
	self.m_LabelRootBindCCButton = self.m_LabelRoot.transform:Find("Table/BindCCButton").gameObject
	self.m_SearchBarBindCCButton = self.m_SearchBar.transform:Find("Table/BindCCButton").gameObject
	self.m_LabelRootCCBroadcastLiveButton = self.m_LabelRoot.transform:Find("Table/CCBroadcastLiveButton").gameObject
	self.m_SearchBarCCBroadcastLiveButton = self.m_SearchBar.transform:Find("Table/CCBroadcastLiveButton").gameObject
	self.m_SearchBarBtnTable = self.m_SearchBar.transform:Find("Table"):GetComponent(typeof(UITable))

	self.m_SearchBar:SetActive(false)
	self.m_LabelRoot:SetActive(true)
	self:InitBindButton()

	self.m_TableView = self.transform:Find("Anchor/Body/TableView"):GetComponent(typeof(QnTableView))
	self.m_NoZhuboHintLabel = self.transform:Find("Anchor/Body/NoZhuboHintLabel"):GetComponent(typeof(UILabel))
	self.m_NoZhuboHintLabel.text = nil

	CommonDefs.AddOnClickListener(self.m_LabelRootShowSearchButton, DelegateFactory.Action_GameObject(function(go) self:OnShowSearchButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_LabelRootBindCCButton, DelegateFactory.Action_GameObject(function(go) self:OnBindCCButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_SearchBarBindCCButton, DelegateFactory.Action_GameObject(function(go) self:OnBindCCButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_LabelRootCCBroadcastLiveButton, DelegateFactory.Action_GameObject(function(go) self:OnCCBroadcastLiveButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_SearchBarCCBroadcastLiveButton, DelegateFactory.Action_GameObject(function(go) self:OnCCBroadcastLiveButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_SearchButton, DelegateFactory.Action_GameObject(function(go) self:OnSearchButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_BackButton, DelegateFactory.Action_GameObject(function(go) self:OnBackButtonClick() end), false)

	self.m_FollowingLabel = LocalString.GetString("我的关注")
	self.m_AllLabel = LocalString.GetString("全部")

	self:InitLabels(false)

	self.m_TableViewDataSource=DefaultTableViewDataSource.Create(function() 
			return self:NumberOfItems()  
		end, function(item, index)
			self:InitItem(item, index)
		end)
    self.m_TableView.m_DataSource=self.m_TableViewDataSource
    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)

    CCCChatMgr.Inst:RequestStationList()
end

function LuaCCLiveListWnd:InitLabels(defaultSelect)
	self.m_LabelButtons = {}
	for i=0,self.m_LabelTable.transform.childCount-1 do
		self.m_LabelTable.transform:GetChild(i).gameObject:SetActive(false)
	end

	local tbl = {self.m_FollowingLabel,self.m_AllLabel}

	--从主播列表获取标签并排序
	local text2weightTbl = {}
	local cnt = 0
	for i=0,CCCChatMgr.Inst.AllStations.Count-1 do
		local station = CCCChatMgr.Inst.AllStations[i]
		--只显示开播的主播电台
		if station.liveInfo.isLive then
			if station.labels.Count > 0 then
				for j=0,station.labels.Count-1 do
					local label = station.labels[j]
					if not text2weightTbl[label.text] then
						text2weightTbl[label.text] = {weight=label.weight, index=cnt}
						cnt = cnt+1
					end
				end
			end
		end
	end

	local sortedTbl = {}
	for k,v in pairs(text2weightTbl) do
		table.insert(sortedTbl, {text = k, weight = v.weight, index = v.index})
	end
	table.sort(sortedTbl,function (a,b)
		if a.weight == b.weight then
			return a.index < b.index
		else
			return b.weight < a.weight
		end
	end)

	for _,v in pairs(sortedTbl) do
		table.insert(tbl, v.text)
	end

	local i = 0
	local n = self.m_LabelTable.transform.childCount
	for _,text in pairs(tbl) do
		local go = nil
		if i<n then
			go = self.m_LabelTable.transform:GetChild(i).gameObject
		else
			go = CUICommonDef.AddChild(self.m_LabelTable.gameObject, self.m_LabelTemplate)
		end
		i = i+1
		go:SetActive(true)
		go:GetComponent(typeof(CButton)).Text = text
		table.insert(self.m_LabelButtons, go)
		CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function(go) self:OnLabelButtonClick(go) end), false)
	end
	self.m_LabelTable:Reposition()
	self.m_LabelScrollView:ResetPosition()
	if defaultSelect then
		self:OnLabelButtonClick(self.m_LabelButtons[2]) -- 默认选中全部
	end
end

function LuaCCLiveListWnd:OnLabelButtonClick(go)
	if not self.m_LabelButtons then return end
	for _,buttonGo in pairs(self.m_LabelButtons) do
		local cbutton = buttonGo:GetComponent(typeof(CButton))
		if buttonGo == go then
			if cbutton.Selected then return end
			cbutton.Selected = true
			self:ShowList()
		else
			cbutton.Selected = false
		end
	end
end

function LuaCCLiveListWnd:NumberOfItems()
	return #self.m_DisplayResult
end

function LuaCCLiveListWnd:InitItem(item, index)
	local station = self.m_DisplayResult[index+1]
	if not station then return end
	local listitem = item.gameObject:GetComponent(typeof(CCCLiveListItem))
	listitem:Init(station)
end

function LuaCCLiveListWnd:OnSelectAtRow(row)
	-- body
end

function LuaCCLiveListWnd:ShowList()
	self.m_DisplayResult = {}

	local onlyShowLive = true
	self.m_NoZhuboHintLabel.text = nil

	if self.m_SearchBar.activeSelf then
		-- 关键词匹配
		local key = self.m_Input.value
		local needFilter = (key ~= nil and key ~= "")
		for i=0,CCCChatMgr.Inst.AllStations.Count-1 do
			local station = CCCChatMgr.Inst.AllStations[i]
			--只显示开播的主播电台
			if not onlyShowLive or station.liveInfo.isLive then
				if not needFilter then
					table.insert(self.m_DisplayResult, station)
				else
					if station.liveInfo.nickName and string.find(station.liveInfo.nickName, key, 1, true) then
						table.insert(self.m_DisplayResult, station)
					elseif station.liveInfo.title and string.find(station.liveInfo.title, key, 1, true) then
						table.insert(self.m_DisplayResult, station)
					end
				end
			end
		end

		if #self.m_DisplayResult == 0 then
			self.m_NoZhuboHintLabel.text = LocalString.GetString("没有搜索到相关主播")
		end
	else
		--标签匹配
		local filterFollowing = false
		local noFilter = false
		local label = nil
		for _,buttonGo in pairs(self.m_LabelButtons) do
			local cbutton = buttonGo:GetComponent(typeof(CButton))
			if cbutton.Selected then
				label = cbutton.Text
				if label == self.m_FollowingLabel then
					filterFollowing = true
				elseif label == self.m_AllLabel then
					noFilter = true
				end
				break
			end
		end
		for i=0,CCCChatMgr.Inst.AllStations.Count-1 do
			local station = CCCChatMgr.Inst.AllStations[i]
			--只显示开播的主播电台
			if not onlyShowLive or station.liveInfo.isLive then
				if filterFollowing then
					if CCCChatMgr.Inst:IsCCFollowing(station.liveInfo.ccid) then
						table.insert(self.m_DisplayResult, station)
					end
				elseif noFilter then
					table.insert(self.m_DisplayResult, station)
				else
					for j=0,station.labels.Count-1 do
						if station.labels[j].text == label then
							table.insert(self.m_DisplayResult, station)
							break
						end
					end
				end
			end
		end

		if #self.m_DisplayResult == 0 then
			if filterFollowing then
				self.m_NoZhuboHintLabel.text = LocalString.GetString("你还没有关注过任何主播哦")
			else
				self.m_NoZhuboHintLabel.text = LocalString.GetString("没有搜索到相关主播")
			end
		end
	end

	self.m_TableView:ReloadData(true,false)
end

function LuaCCLiveListWnd:OnEnable()
	 g_ScriptEvent:AddListener("OnZhuboStationListUpdate", self, "OnZhuboStationListUpdate")
end

function LuaCCLiveListWnd:OnDisable()
	 g_ScriptEvent:RemoveListener("OnZhuboStationListUpdate", self, "OnZhuboStationListUpdate")
end

function LuaCCLiveListWnd:OnZhuboStationListUpdate(args)
	self:InitLabels(true)
end

function LuaCCLiveListWnd:OnDestroy()
	self.m_LabelButtons = nil
end

function LuaCCLiveListWnd:OnShowSearchButtonClick()
	self.m_LabelRoot:SetActive(false)
	self.m_SearchBar:SetActive(true)
	self:InitBindButton()
	self:ShowList()
end

function LuaCCLiveListWnd:OnSearchButtonClick()
	self:ShowList()
end

function LuaCCLiveListWnd:OnBackButtonClick()
	self.m_LabelRoot:SetActive(true)
	self.m_SearchBar:SetActive(false)
	self:InitBindButton()
	self:ShowList()
end

function LuaCCLiveListWnd:OnBindCCButtonClick()
	if not self:BindCCEnabled() then
		return
	end
	 CInputBoxMgr.ShowInputBox(LocalString.GetString("CC直播账号绑定"), 
		DelegateFactory.Action_string(function(input)
			if input~= "" then
				LuaCCChatMgr.DoBindCC(input)
			end
		end), 100, true, nil, LocalString.GetString("请输入CC直播绑定验证码"))
end

function LuaCCLiveListWnd:InitBindButton()
	if self.m_LabelRoot.activeSelf then

		local extraBtnCount = 0
		self.m_LabelRootBindCCButton:SetActive(self:BindCCEnabled())
		extraBtnCount = self:BindCCEnabled() and extraBtnCount + 1 or extraBtnCount
		self.m_LabelRootCCBroadcastLiveButton:SetActive(self:CCBroadcastLiveEnabled())
		extraBtnCount = self:CCBroadcastLiveEnabled() and extraBtnCount + 1 or extraBtnCount
		
		if extraBtnCount == 0 then
			self.m_LabelScrollView.panel.baseClipRegion = Vector4(618, 30, 1476, 112)
		elseif extraBtnCount == 1 then
			self.m_LabelScrollView.panel.baseClipRegion = Vector4(618, 30, 1476, 112)
		elseif extraBtnCount == 2 then
			self.m_LabelScrollView.panel.baseClipRegion = Vector4(618, 30, 1476, 112)
		end

		self.m_LabelRootBtnTable:Reposition()
	elseif self.m_SearchBar.activeSelf then

		local extraBtnCount = 0
		self.m_SearchBarBindCCButton:SetActive(self:BindCCEnabled())
		extraBtnCount = self:BindCCEnabled() and extraBtnCount + 1 or extraBtnCount
		self.m_SearchBarCCBroadcastLiveButton:SetActive(self:CCBroadcastLiveEnabled())
		extraBtnCount = self:CCBroadcastLiveEnabled() and extraBtnCount + 1 or extraBtnCount

		if extraBtnCount == 0 then
			self.m_Input:GetComponent(typeof(UISprite)).width = 1228
			self.m_SearchButton.transform.localPosition = Vector3(602.5,-8.3,0)
		elseif extraBtnCount == 1 then
			self.m_Input:GetComponent(typeof(UISprite)).width = 1154
			self.m_SearchButton.transform.localPosition = Vector3(526.5,-8.3,0)
		elseif extraBtnCount == 2 then
			self.m_Input:GetComponent(typeof(UISprite)).width = 1084
			self.m_SearchButton.transform.localPosition = Vector3(450.5,-8.3,0)
		end

		self.m_SearchBarBtnTable:Reposition()
	end
end

function LuaCCLiveListWnd:BindCCEnabled()
	return LuaWelfareMgr.EnableBindCC()
end

function LuaCCLiveListWnd:CCBroadcastLiveEnabled()
	return CCMiniAPIMgr.Inst:CCBroadcastLiveEnabled()
end

function LuaCCLiveListWnd:OnCCBroadcastLiveButtonClick()
	CCMiniAPIMgr.Inst:ShowCCBroadcastLive()
end

return LuaCCLiveListWnd
