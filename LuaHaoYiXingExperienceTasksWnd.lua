local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UITable = import "UITable"

LuaHaoYiXingExperienceTasksWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHaoYiXingExperienceTasksWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaHaoYiXingExperienceTasksWnd, "Table", "Table", UITable)

--@endregion RegistChildComponent end

function LuaHaoYiXingExperienceTasksWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaHaoYiXingExperienceTasksWnd:Init()
    self.Template:SetActive(false)
    Extensions.RemoveAllChildren(self.Table.transform)
    SpokesmanTCG_ErHaTCGScore.Foreach(function (key,data)
        local obj = NGUITools.AddChild(self.Table.gameObject,self.Template)
        local descLabel1 = obj.transform:Find("Anchor/DescLabel1"):GetComponent(typeof(UILabel))
        local descLabel2 = obj.transform:Find("Anchor/DescLabel2"):GetComponent(typeof(UILabel))
        local isLock = key >= LuaHaoYiXingMgr.m_CurMainTaskIdx
        descLabel1.text = data.Desc
        descLabel1.alpha = isLock and 0.8 or 1
        descLabel2.text = data.TaskName
        descLabel2.gameObject:SetActive(not isLock)
        obj.transform:Find("Anchor/LockSprite").gameObject:SetActive(isLock)
        obj.transform:Find("Anchor/Bg01").gameObject:SetActive(key ~= LuaHaoYiXingMgr.m_CurrentDoingMainTaskIdx)
        obj.transform:Find("Anchor/Bg01"):GetComponent(typeof(UITexture)).alpha = isLock and 0.8 or 1
        obj.transform:Find("Anchor/Bg02").gameObject:SetActive(key == LuaHaoYiXingMgr.m_CurrentDoingMainTaskIdx)
        Extensions.SetLocalPositionZ(obj.transform:Find("Anchor"),isLock and -1 or 0)
        obj:SetActive(true)
        local id = key
        UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (go)
            Gac2Gas.ErHaTCGReviewMainTask(id)
        end)
    end)
    self.Table:Reposition()
end
--@region UIEvent

--@endregion UIEvent

