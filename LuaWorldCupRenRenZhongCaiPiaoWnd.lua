require("common/common_include")

local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local CUICommonDef = import "L10.UI.CUICommonDef"

CLuaWorldCupRenRenZhongCaiPiaoWnd=class()
RegistClassMember(CLuaWorldCupRenRenZhongCaiPiaoWnd, "m_CopyButton")
RegistClassMember(CLuaWorldCupRenRenZhongCaiPiaoWnd, "m_CodeLabel")

function CLuaWorldCupRenRenZhongCaiPiaoWnd:OnEnable()
    self.m_CopyButton = FindChild(self.transform, "CopyButton"):GetComponent(typeof(CButton))
    self.m_CodeLabel = FindChild(self.transform, "CodeLabel"):GetComponent(typeof(UILabel))

    self.m_CodeLabel.text = CLuaWorldCupMgr.m_RenRenZhongCaiPiaoCode or ""

    CommonDefs.AddOnClickListener(self.m_CopyButton.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnClickCopyButton()
    end), false)
end

function CLuaWorldCupRenRenZhongCaiPiaoWnd:OnDisable()
end

function CLuaWorldCupRenRenZhongCaiPiaoWnd:OnClickCopyButton()
    if not CLuaWorldCupMgr.m_RenRenZhongCaiPiaoCode then
        return
    end

    CUICommonDef.clipboardText = CLuaWorldCupMgr.m_RenRenZhongCaiPiaoCode

    g_MessageMgr:ShowMessage("WorldCup_RenRenZhong_CaiPiao_Copy_Success")
end


return CLuaWorldCupRenRenZhongCaiPiaoWnd
