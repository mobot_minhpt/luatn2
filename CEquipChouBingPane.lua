-- Auto Generated!!
local AlignType = import "L10.UI.CTooltip+AlignType"
local CEquipChouBingPane = import "L10.UI.CEquipChouBingPane"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CEquipWuZuanReplaceMgr = import "L10.Game.CEquipWuZuanReplaceMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EventDelegate = import "EventDelegate"
local MessageWndManager = import "L10.UI.MessageWndManager"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CEquipChouBingPane.m_OnEnterUI_CS2LuaHook = function (this) 

    if CEquipWuZuanReplaceMgr.Inst.IsWuZuanReplace then
        this.toggle1.value = false
        this.toggle2.value = true
        CEquipWuZuanReplaceMgr.Inst.IsWuZuanReplace = false
    else
        this.toggle1.value = true
        this.toggle2.value = false
    end
end
CEquipChouBingPane.m_OnSelected_CS2LuaHook = function (this, itemId) 
    if System.String.IsNullOrEmpty(itemId) then
        this:InitNull()
    else
        this.equipItem:UpdateData(itemId)

        local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
        if equip == nil then
            return
        end
        local data = CItemMgr.Inst:GetById(equip.itemId)
        if data ~= nil and data.IsBinded then
            this.choubingCost.onlyCostBindItem = false
            this.choubingCost.isBinded = true
        else
            this.choubingCost.onlyCostBindItem = false
            this.choubingCost.isBinded = false
        end
        this.choubingCost:Init()

        this.wuzuanCost:UpdateCost()

        --toggle组件有一些问题
        if this.toggle1.gameObject.activeInHierarchy then
            if this.canChouBing then
                this.toggle1.value = true
            elseif not this.canChouBing and this.canWuZuan then
                this.toggle2.value = true
            end
        end

        this:UpdateBtnState()
    end
end
CEquipChouBingPane.m_UpdateBtnState_CS2LuaHook = function (this) 
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    if equip == nil then
        this.equipItem:InitNull()
        this.warningLabel.gameObject:SetActive(false)
        return
    end
    local data = CItemMgr.Inst:GetById(equip.itemId)
    if data == nil then
        return
    end
    if this.isChouBing then
        if EnumToInt(data.Equip.Color) >= EnumQualityType_lua.Red then
            CUICommonDef.SetActive(this.choubingBtn, true, true)
        else
            CUICommonDef.SetActive(this.choubingBtn, false, true)
        end
    else
        --看看有没有无钻这个词条
        local list = data.Equip:GetFixedWordList(false)
        CommonDefs.ListAddRange(list, data.Equip:GetExtWordList(false))
        if CommonDefs.ListContains(list, typeof(UInt32), CEquipWuZuanReplaceMgr.Inst.WuZuanWordId) then
            CUICommonDef.SetActive(this.choubingBtn, true, true)
        else
            CUICommonDef.SetActive(this.choubingBtn, false, true)
        end
    end
    this:UpdateToggleState()
end
CEquipChouBingPane.m_UpdateToggleState_CS2LuaHook = function (this) 
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    if equip == nil then
        this.equipItem:InitNull()
        this.warningLabel.gameObject:SetActive(false)
        return
    end
    local data = CItemMgr.Inst:GetById(equip.itemId)
    if data == nil then
        return
    end

    if not this.canChouBing then
        CUICommonDef.SetActive(this.option1Go, false, false)
    else
        CUICommonDef.SetActive(this.option1Go, true, false)
    end
    if not this.canWuZuan then
        CUICommonDef.SetActive(this.option2Go, false, false)
    else
        CUICommonDef.SetActive(this.option2Go, true, false)
    end
end
CEquipChouBingPane.m_RequestWuZuan_CS2LuaHook = function (this) 
    --WUZUAN_UNBOUND %s的一条无钻属性置换为神赐需消耗%s个五曜神珠并使其绑定，是否继续？
    --WUZUAN_BOUND %s的一条无钻属性置换为神赐需消耗%s个五曜神珠，是否继续？
    --WUZUAN_UNBOUND %s的一条无钻属性置换为神赐需消耗%s个五曜神珠并使其绑定，是否继续？
    --WUZUAN_BOUND %s的一条无钻属性置换为神赐需消耗%s个五曜神珠，是否继续？
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local data = CItemMgr.Inst:GetById(equip.itemId)

    local default
    if data.IsBinded then
        default = g_MessageMgr:FormatMessage("WUZUAN_BOUND", data.ColoredName, this.wuzuanCost.costNum)
    else
        default = g_MessageMgr:FormatMessage("WUZUAN_UNBOUND", data.ColoredName, this.wuzuanCost.costNum)
    end
    local msg = default
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
        Gac2Gas.RequestReplaceWuZuan(EnumToInt(equip.place), equip.pos, equip.itemId)
    end), nil, nil, nil, false)
end
CEquipChouBingPane.m_Awake_CS2LuaHook = function (this) 
    this.warningLabel.gameObject:SetActive(false)
    this:InitNull()
    this:OnEnterUI()
    UIEventListener.Get(this.choubingBtn).onClick = MakeDelegateFromCSFunction(this.OnClickChouBingButton, VoidDelegate, this)

    CommonDefs.ListAdd(this.toggle1.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        if this.toggle1.value then
            this.chouBingContent:SetActive(true)
            this.wuZuanContent:SetActive(false)
            this:UpdateBtnState()
        end
    end)))
    CommonDefs.ListAdd(this.toggle2.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        if this.toggle2.value then
            this.chouBingContent:SetActive(false)
            this.wuZuanContent:SetActive(true)
            this:UpdateBtnState()
        end
    end)))
    this.toggle1.optionCanBeNone = true
    this.toggle2.optionCanBeNone = true
    UIEventListener.Get(this.option1Go).onClick = MakeDelegateFromCSFunction(this.OnClickOption1Go, VoidDelegate, this)
    UIEventListener.Get(this.option2Go).onClick = MakeDelegateFromCSFunction(this.OnClickOption2Go, VoidDelegate, this)
end
CEquipChouBingPane.m_OnClickOption1Go_CS2LuaHook = function (this, go) 
    if this.canChouBing then
        this.toggle1.value = true
        -- !toggle1.value;
    else
        g_MessageMgr:ShowMessage("ZHIHUAN_TISHI")
    end
end
CEquipChouBingPane.m_OnClickOption2Go_CS2LuaHook = function (this, go) 
    if this.canWuZuan then
        this.toggle2.value = true
        -- !toggle2.value;
    else
        g_MessageMgr:ShowMessage("WUZUAN_TISHI")
    end
end
CEquipChouBingPane.m_OnClickChouBingButton_CS2LuaHook = function (this, go) 
    if this.isChouBing then
        if not CEquipmentProcessMgr.Inst:CheckOperationConflict(CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize) then
            return
        end

        CUIManager.ShowUI(CUIResources.EquipChouBingWnd)
    else
        if this.wuzuanCost.matEnough then
            local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
            local data = CItemMgr.Inst:GetById(equip.itemId)
            local secondWordSet = data.Equip.HasTwoWordSet and data.Equip.IsSecondWordSetActive or false

            if data.Equip:HasTempWordDataInWordSet(secondWordSet) then
                local msg = g_MessageMgr:FormatMessage("CITIAO_NOTICE")
                MessageWndManager.ShowOKCancelMessage(msg, MakeDelegateFromCSFunction(this.RequestWuZuan, Action0, this), nil, nil, nil, false)
            elseif data.Equip:HasTempWordDataInWordSet(not secondWordSet) then
                local msg = g_MessageMgr:FormatMessage("CITIAO_NOTICE_OTHERSET")
                MessageWndManager.ShowOKCancelMessage(msg, MakeDelegateFromCSFunction(this.RequestWuZuan, Action0, this), nil, nil, nil, false)
            else
                this:RequestWuZuan()
            end
        else
            --快速购买
            --如果没有的时候 点击的时候显示获取途径
            local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
            CItemAccessListMgr.Inst:ShowItemAccessInfo(this.wuzuanCost.templateId, false, this.choubingBtn.transform, AlignType.Right)
        end
    end
end
