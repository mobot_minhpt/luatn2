local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Profession     = import "L10.Game.Profession"
local EnumClass      = import "L10.Game.EnumClass"
local CChatMgr       = import "L10.Game.CChatMgr"

LuaTeamRecruitMgr = {}

LuaTeamRecruitMgr.searchList = {}
LuaTeamRecruitMgr.searchCurrentPageNum = 1
LuaTeamRecruitMgr.searchTotalPageNum = 1
LuaTeamRecruitMgr.myData = {}

LuaTeamRecruitMgr.dataForDetailWnd = {}

-- 打开招募
function LuaTeamRecruitMgr:OpenTeamRecruitWnd()
    Gac2Gas.RequestQueryTeamRecruit(0, 0, "", false, false, false)
end

-- 有我的招募
function LuaTeamRecruitMgr:HasMyRecruit()
    if self.myData.id then
        local leftTime = self.myData.duration - (CServerTimeMgr.Inst.timeStamp - self.myData.startTime)
        if leftTime > 0 then
            return true
        end
    end
    return false
end

function LuaTeamRecruitMgr:GetMyRecruitTargetName()
    return TeamRecruit_Type.GetData(self.myData.target).Name
end

function LuaTeamRecruitMgr:GetMyRecruitLinkMsg()
    local level = SafeStringFormat3("%d-%d", self.myData.gardeStart, self.myData.gardeEnd)
    local target = TeamRecruit_Type.GetData(self.myData.target).Name
    local typeList = self.myData.typeList

    local class = ""
    if typeList[1][2] > 0 then
        class = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), typeList[1][2]))
    else
        class = SafeStringFormat3(LocalString.GetString("%s职业"), TeamRecruit_Feature.GetData(typeList[1][1]).Name)
    end
    local equipScore = typeList[1][3] > 0 and SafeStringFormat3(LocalString.GetString("-装评≥%d"), typeList[1][3]) or ""
    local intent = SafeStringFormat3("%s%s", class, equipScore)
    local msg = System.String.IsNullOrEmpty(self.myData.msg) and "" or SafeStringFormat3(LocalString.GetString("留言：%s"), self.myData.msg)
    return g_MessageMgr:FormatMessage("TEAM_RECRUIT_CHAT_SHARE", level, target, intent, msg)
end

-- 打开我的招募
function LuaTeamRecruitMgr:ShowMyTeamRecruitWnd(playerId, startTime)
    if CClientMainPlayer.Inst then
        Gac2Gas.QueryTeamRecruitByIdAndTime(playerId, startTime)
    end
end

--#region Gas2Gac

function LuaTeamRecruitMgr:QueryTeamRecruitResult(recruitListUD, currentPageNum, pageNum)
    local list = g_MessagePack.unpack(recruitListUD)

    self.searchList = {}
    if not list or #list == 0 then
        self.searchCurrentPageNum = 1
        self.searchTotalPageNum = 1
    else
        for _, data in ipairs(list) do
            table.insert(self.searchList, {
                id = data[1],
                name = data[2],
                class = data[3],
                gender = data[4],
                grade = data[5],
                target = data[6],
                gardeStart = data[7],
                gardeEnd = data[8],
                msg = data[9],
                typeList = data[10],
                startTime = data[11],
                duration = data[12],
                check = data[13]
            })
        end

        self.searchCurrentPageNum = math.max(currentPageNum, 1)
        self.searchTotalPageNum = math.max(pageNum, 1)
    end

    if CUIManager.IsLoaded(CLuaUIResources.TeamRecruitWnd) then
        EventManager.BroadcastInternalForLua(EnumEventType.TeamRecruitSearchInfo, {})
    else
        CUIManager.ShowUI(CLuaUIResources.TeamRecruitWnd)
    end
end

function LuaTeamRecruitMgr:QueryOwnTeamRecruitResult(recruitListUD)
    self:UpdateOwnTeamRecruit(recruitListUD)
end

function LuaTeamRecruitMgr:QueryTeamRecruitByIdAndTimeResult(recruitListUD)
    self.dataForDetailWnd = {}
    local data = g_MessagePack.unpack(recruitListUD)
    if data and #data > 0 then
        self.dataForDetailWnd = {
            id = data[1],
            name = data[2],
            class = data[3],
            gender = data[4],
            grade = data[5],
            target = data[6],
            gardeStart = data[7],
            gardeEnd = data[8],
            msg = data[9],
            typeList = data[10],
            startTime = data[11],
            duration = data[12],
            check = data[13]
        }
        CUIManager.ShowUI(CLuaUIResources.TeamRecruitDetailWnd)
    end
end

function LuaTeamRecruitMgr:TeamRecruitAddSuccess(recruitListUD)
    self:UpdateOwnTeamRecruit(recruitListUD)
    CChatMgr.Inst:SendChatMsg(CChatMgr.CHANNEL_NAME_WORLD, self:GetMyRecruitLinkMsg(), true)
    EventManager.BroadcastInternalForLua(EnumEventType.TeamRecruitSendSucc, {})
end

function LuaTeamRecruitMgr:TeamRecruitCancelSuccess()
    self.myData = {}
    EventManager.BroadcastInternalForLua(EnumEventType.TeamRecruitSelfInfo, {})
    EventManager.BroadcastInternalForLua(EnumEventType.TeamRecruitSendSucc, {})
end

--#endregion Gas2Gac

function LuaTeamRecruitMgr:UpdateOwnTeamRecruit(recruitListUD)
    self.myData = {}
    local data = g_MessagePack.unpack(recruitListUD)
    if data and #data > 0 then
        self.myData = {
            id = data[1],
            name = data[2],
            class = data[3],
            gender = data[4],
            grade = data[5],
            target = data[6],
            gardeStart = data[7],
            gardeEnd = data[8],
            msg = data[9],
            typeList = data[10],
            startTime = data[11],
            duration = data[12],
            check = data[13]
        }
    end
    EventManager.BroadcastInternalForLua(EnumEventType.TeamRecruitSelfInfo, {})
end
