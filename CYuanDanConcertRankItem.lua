-- Auto Generated!!
local Constants = import "L10.Game.Constants"
local CYuanDanConcertRankItem = import "L10.UI.CYuanDanConcertRankItem"
CYuanDanConcertRankItem.m_Init_CS2LuaHook = function (this, info, isOdd) 

    this.playerId = info.playerId
    this.playerName = info.playerName
    this.rankLabel.text = tostring(info.rank)
    this.nameLabel.text = info.playerName
    this.flowerNumLabel.text = tostring(info.flowerNum)

    local default
    if not isOdd then
        default = Constants.EvenBgSprite
    else
        default = Constants.OddBgSpirite
    end
    this.button:SetBackgroundSprite(default)

    local spriteName = this:GetRankImageByRankIndex(info.rank)
    if not System.String.IsNullOrEmpty(spriteName) then
        this.rankImage.spriteName = spriteName
        this.rankImage:MakePixelPerfect()
    else
        this.rankImage.spriteName = nil
    end
end
CYuanDanConcertRankItem.m_GetRankImageByRankIndex_CS2LuaHook = function (this, index) 

    repeat
        local default = index
        if default == (1) then
            return Constants.RankFirstSpriteName
        elseif default == (2) then
            return Constants.RankSecondSpriteName
        elseif default == (3) then
            return Constants.RankThirdSpriteName
        else
            return nil
        end
    until 1
end
