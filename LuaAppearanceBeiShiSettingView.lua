local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CGuashiSlider = import "L10.UI.CGuashiSlider"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local QnTabButton = import "L10.UI.QnTabButton"
local QnNewSlider = import "L10.UI.QnNewSlider"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
--copy from LuaBeiShiRoot by guangzhong. old file removed
LuaAppearanceBeiShiSettingView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaAppearanceBeiShiSettingView, "xSlider", "xSlider", QnNewSlider)
RegistChildComponent(LuaAppearanceBeiShiSettingView, "ySlider", "ySlider", QnNewSlider)
RegistChildComponent(LuaAppearanceBeiShiSettingView, "zSlider", "zSlider", QnNewSlider)
RegistChildComponent(LuaAppearanceBeiShiSettingView, "saveBtn", "saveBtn", GameObject)
RegistChildComponent(LuaAppearanceBeiShiSettingView, "resetBtn", "resetBtn", GameObject)
RegistChildComponent(LuaAppearanceBeiShiSettingView, "CurrentLabel", "CurrentLabel", UILabel)
RegistChildComponent(LuaAppearanceBeiShiSettingView, "Tabs", "Tabs", QnTabView)
RegistChildComponent(LuaAppearanceBeiShiSettingView, "BackBtn", "BackBtn", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaAppearanceBeiShiSettingView, "m_dirty")
RegistClassMember(LuaAppearanceBeiShiSettingView, "m_x")
RegistClassMember(LuaAppearanceBeiShiSettingView, "m_y")
RegistClassMember(LuaAppearanceBeiShiSettingView, "m_z")
RegistClassMember(LuaAppearanceBeiShiSettingView, "m_curTab")
RegistClassMember(LuaAppearanceBeiShiSettingView, "m_activeTab")

function LuaAppearanceBeiShiSettingView:Awake()
    self.m_curTab = 0
    self.m_dirty = false
    self.m_activeTab = -1

    --@region EventBind: Dont Modify Manually!
    UIEventListener.Get(self.saveBtn.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnSaveButtonClick()
        end
    )

    UIEventListener.Get(self.resetBtn.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnResetButtonClick()
        end
    )

    self.Tabs.OnSelect =
        DelegateFactory.Action_QnTabButton_int(
        function(btn, index)
            self:OnTabsSelect(index)
        end
    )

    UIEventListener.Get(self.BackBtn.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnBackButtonClick()
        end
    )

    --@endregion EventBind end
end

function LuaAppearanceBeiShiSettingView:OnPosChange()
    g_ScriptEvent:BroadcastInLua("OnBeiShiSetPosChange", self.m_x or 0, self.m_y or 0, self.m_z or 0)
end

function LuaAppearanceBeiShiSettingView:SetTabState(qntabbtn, isactive)
    local child = qntabbtn.transform:Find("Current")
    child.gameObject:SetActive(isactive)
end

function LuaAppearanceBeiShiSettingView:SetCurrentSaveTab(index)
    if self.m_activeTab >= 0 then
        local lasttabbtn = self.Tabs.m_TabButtons[self.m_activeTab]
        self:SetTabState(lasttabbtn, false)
    end
    self.m_activeTab = index
    local tabbtn = self.Tabs.m_TabButtons[self.m_activeTab]
    self:SetTabState(tabbtn, true)
end

function LuaAppearanceBeiShiSettingView:Init()
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp then
        local index = CClientMainPlayer.Inst.PlayProp.ActiveBackPendantPosTemplateIdx
        if index then
            local tab = math.min(math.max(1, index), 3)
            self.Tabs:ChangeTo(tab - 1)
        else
            self.Tabs:ChangeTo(0)
        end
    end

    self.xSlider.OnValueChanged =
        DelegateFactory.Action_float(
        function(value)
            if math.abs(self.m_x - value) < 0.001 then
                return
            end
            self.m_x = value
            self.m_dirty = true
            self:OnPosChange()
        end
    )

    self.ySlider.OnValueChanged =
        DelegateFactory.Action_float(
        function(value)
            if math.abs(self.m_y - value) < 0.001 then
                return
            end
            self.m_y = value
            self.m_dirty = true
            self:OnPosChange()
        end
    )

    self.zSlider.OnValueChanged =
        DelegateFactory.Action_float(
        function(value)
            if math.abs(self.m_z - value) < 0.001 then
                return
            end
            self.m_z = value
            self.m_dirty = true
            self:OnPosChange()
        end
    )
end

function LuaAppearanceBeiShiSettingView:ChangeTabTo(tabindex)
    if tabindex == self.m_curTab then
        return
    end

    if self.m_dirty then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("CONFIRM_BACKPENDANT_SAVE"), function()
            self:SaveIndexData()
            self:SavePosData()
            self.m_curTab = tabindex
            self:InitTab()
        end, function()
            self.m_curTab = tabindex
            self:InitTab()
        end, nil, nil, false)
    else
        self.m_curTab = tabindex
        self:InitTab()
    end
end

function LuaAppearanceBeiShiSettingView:InitTab()
    self:SetCurrentSaveTab(self.m_curTab - 1)
    local havedata = false
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp then
        local bppts = CClientMainPlayer.Inst.PlayProp.BackPendantPosTemplate --dic
        if bppts then
            local data = CommonDefs.DictGetValue_LuaCall(bppts, self.m_curTab)
            if data then
                self.m_x = (data.PosData[1] + 128) / 255
                self.m_y = (data.PosData[2] + 128) / 255
                self.m_z = (data.PosData[3] + 128) / 255
                havedata = true
            end
        end
    end
    if not havedata then
        self.m_x, self.m_y, self.m_z = self:GetBackPendantOriOffsetScale()
    end

    self.xSlider.m_Value = self.m_x
    self.ySlider.m_Value = self.m_y
    self.zSlider.m_Value = self.m_z
    self:OnPosChange()
    self.m_dirty = false
end

function LuaAppearanceBeiShiSettingView:GetBackPendantOriOffsetScale()
    if not CClientMainPlayer.Inst then
        return 0, 0, 0
    end

    local job = EnumToInt(CClientMainPlayer.Inst.Class)
    local gender = EnumToInt(CClientMainPlayer.Inst.Gender)

    local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Body, 13)
    local item = CItemMgr.Inst:GetById(id)

    local fid = CClientMainPlayer.Inst.AppearanceProp.BackPendantFashionId
    local data = nil

    if fid > 0 then --有拓本或者时装
        local taben = EquipmentTemplate_Equip.GetData(fid)
        if taben then
            data = EquipmentTemplate_BackPendantExt.GetData(fid)
        else
            data = Fashion_BackPendantExt.GetData(fid)
        end
    else
        data = EquipmentTemplate_BackPendantExt.GetData(item.TemplateId)
    end

    if data == nil then
        return 0, 0, 0
    end

    local x, y, z = 0, 0, 0
    local offsets = data.DefaultOffset
    if offsets then
        for i = 0, offsets.Length - 1, 5 do
            if offsets[i] == job and offsets[i + 1] == gender then
                x = offsets[i + 2]
                y = offsets[i + 3]
                z = offsets[i + 4]
                break
            end
        end
    end

    local sx = (x - data.XMinMax[0]) / (data.XMinMax[1] - data.XMinMax[0])
    local sy = (y - data.YMinMax[0]) / (data.YMinMax[1] - data.YMinMax[0])
    local sz = (z - data.ZMinMax[0]) / (data.ZMinMax[1] - data.ZMinMax[0])
    return sx, sy, sz
end

function LuaAppearanceBeiShiSettingView:SavePosData() --保存位置信息，意味着肯定激活了这个页签
    if self.m_dirty then --有位置变更
        local x = math.ceil(self.m_x * 255) - 128
        local y = math.ceil(self.m_y * 255) - 128
        local z = math.ceil(self.m_z * 255) - 128
        Gac2Gas.SaveBackPendantPos2Template(self.m_curTab, x, y, z)
    end
    self.m_dirty = false
end

function LuaAppearanceBeiShiSettingView:SaveIndexData()
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp then
        local index = CClientMainPlayer.Inst.PlayProp.ActiveBackPendantPosTemplateIdx
        if not index or index ~= self.m_curTab then --从来没有保存过或者切换过位置方案
            Gac2Gas.SwitchBackPendantPosTemplate(self.m_curTab)
        end
    end
end

function LuaAppearanceBeiShiSettingView:Exit()
    if LuaAppearancePreviewMgr.IsBeiShiFromOut then
        LuaAppearancePreviewMgr:CloseAppearanceWnd()
    else
        g_ScriptEvent:BroadcastInLua("CloseBeiShiSetting")
    end
end

--@region UIEvent

function LuaAppearanceBeiShiSettingView:OnSaveButtonClick()
    self:SaveIndexData()
    self:SavePosData()
end

function LuaAppearanceBeiShiSettingView:OnResetButtonClick()
    local x, y, z = self.GetBackPendantOriOffsetScale()
    self.m_dirty = math.abs(self.m_x - x) >= 0.001 or math.abs(self.m_y - y) >= 0.001 or math.abs(self.m_z - z) >= 0.001
    self.m_x, self.m_y, self.m_z = x, y, z
    self.xSlider.m_Value = self.m_x
    self.ySlider.m_Value = self.m_y
    self.zSlider.m_Value = self.m_z
    self:OnPosChange()
end

function LuaAppearanceBeiShiSettingView:OnTabsSelect(index)
    local rindex = index + 1
    self:ChangeTabTo(rindex)
end

function LuaAppearanceBeiShiSettingView:OnBackButtonClick()
    if self.m_dirty then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("CONFIRM_BACKPENDANT_SAVE"), function()
            self:SaveIndexData()
            self:SavePosData()
            self:Exit()
        end, function()
            self:SaveIndexData()
            self:InitTab()
            self:Exit()
        end, nil, nil, false)
    else
        self:SaveIndexData()
        self:Exit()
    end
end
--@endregion UIEvent
