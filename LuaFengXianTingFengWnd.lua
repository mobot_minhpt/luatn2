require("common/common_include")

LuaFengXianTingFengWnd = class()

RegistChildComponent(LuaFengXianTingFengWnd, "PlayerNameLabel", UILabel)
RegistChildComponent(LuaFengXianTingFengWnd, "XianZhiLabel", UILabel)
RegistChildComponent(LuaFengXianTingFengWnd, "ShenJiLabel", UILabel)

-- 听封界面
function LuaFengXianTingFengWnd:Init()
	self.PlayerNameLabel.text = SafeStringFormat3(LocalString.GetString("[734B2D]%s[-] [152D56]为[-]"), tostring(CLuaXianzhiMgr.m_TingFengPlayerName))
	self.XianZhiLabel.text = tostring(CLuaXianzhiMgr.m_TingFengXianZhiName)
	self.ShenJiLabel.text = SafeStringFormat3(LocalString.GetString("[152D56]赐[-]  [952200]%s[-] "), tostring(CLuaXianzhiMgr.m_TingFengSkillName))
end

return LuaFengXianTingFengWnd