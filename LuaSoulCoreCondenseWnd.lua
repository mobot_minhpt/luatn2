local CButton = import "L10.UI.CButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CUIFx = import "L10.UI.CUIFx"
local ParticleSystem = import "UnityEngine.ParticleSystem"
local Renderer = import "UnityEngine.Renderer"
local MaterialPropertyBlock = import "UnityEngine.MaterialPropertyBlock"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local LuaTweenUtils = import "LuaTweenUtils"
local Screen = import "UnityEngine.Screen"
local Camera = import "UnityEngine.Camera"
local CommonDefs = import "L10.Game.CommonDefs"
local UICamera = import "UICamera"
local Ease = import "DG.Tweening.Ease"
local CModelAutoRotate = import "L10.UI.CModelAutoRotate"
local CRenderObject = import "L10.Engine.CRenderObject"
local LayerDefine = import "L10.Engine.LayerDefine"
local Collider = import "UnityEngine.Collider"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CTrackMgr = import "L10.Game.CTrackMgr"
local AnimationCurve = import "UnityEngine.AnimationCurve"
local Keyframe = import "UnityEngine.Keyframe"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CWeatherMgr = import "L10.Game.CWeatherMgr"

CLuaSoulCoreCondenseWnd = class()

RegistClassMember(CLuaSoulCoreCondenseWnd, "m_Init")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_Tab")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_TimeLab")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_WeatherTip")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_TipsLab")

RegistClassMember(CLuaSoulCoreCondenseWnd, "m_ModelTex")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_ModelBg")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_ModelBg2")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_Slider")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_SliderLab")

RegistClassMember(CLuaSoulCoreCondenseWnd, "m_CancelBtn")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_JiasuBtn")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_TipBtn")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_NingjieFx")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_JiasuFx")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_FlashFx")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_FinishFx")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_Tick")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_TimeCount")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_Duration")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_CurIndex")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_Tips")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_FinishTick")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_RemainTimeCache")

RegistClassMember(CLuaSoulCoreCondenseWnd, "m_TimeRoot")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_BtnsRoot")
--雕琢
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_ExplosionFx")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_ClickFxs")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_ClickFxIndex")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_CarveTips")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_CarvingTip")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_FinishTip")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_CarveSlider")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_Tweener")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_TweenerIsComplete")

--原石相关
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_SoulCoreOSPrefab")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_Rotation")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_IsDraging")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_IsCarveFinish")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_AutoRotSpeed")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_ModelCamera")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_MaxPartCount")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_CurPartCount")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_PartTable")

RegistClassMember(CLuaSoulCoreCondenseWnd, "m_ZeroPoint")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_Width")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_Height")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_AdjustScale")

RegistClassMember(CLuaSoulCoreCondenseWnd, "m_Root")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_CoreRo")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_AutoRotate")
RegistClassMember(CLuaSoulCoreCondenseWnd, "ModelTextureName")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_YOffset")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_YLocalOffset")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_CloseBtn")

RegistClassMember(CLuaSoulCoreCondenseWnd, "m_ProgressFx")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_LeftCurveTab")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_RightCurveTab")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_LeftCurve")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_RightCurve")

RegistClassMember(CLuaSoulCoreCondenseWnd, "m_ChangeScale")
RegistClassMember(CLuaSoulCoreCondenseWnd, "m_ChangeType")
function CLuaSoulCoreCondenseWnd:Awake()
    self:InitComponents()
    self.m_Tab = 0 --0未初始化, 1凝结界面, 2雕琢
    self.m_TimeCount = 0
    self.m_Duration = 0.5
    self.m_CurIndex = nil
    self.m_Tips = {}
    self.m_Slider.value = 0.081 -- 0.946
    self.m_SliderLab.text = "00%"
    self.m_TweenerIsComplete = false
    self.m_RemainTimeCache = 0

    --原石相关
    self.ModelTextureName = "__SoulCoreCarveModelCamera__"
    self.m_SoulCoreOSPrefab = "Character/NPC/lnpc_gem/Prefab/lnpc_gem_786.prefab"
    self.m_Rotation = 180
    self.m_IsDraging = false
    self.m_IsCarveFinish = false
    self.m_AutoRotSpeed = SoulCore_Settings.GetData().SoulCoreCarveRotSpeed or 12
    self.m_PartTable = {}
    self.m_ClickFxIndex = 1
    self.m_YOffset = 0.1

    self.m_LeftCurveTab =  { {0.231, 0}, {0.260, 0.342}, {0.468, 0.676}, {0.611, 0.661}, {0.716, 0.726}, {0.831, 0.472}, {0.896, 0.349}, {0.923, 0}}
    self.m_RightCurveTab =  { {0.231, 0}, {0.260, 0.358}, {0.362, 0.624}, {0.506, 0.697}, {0.537, 0.726}, {0.698, 0.678}, {0.772, 0.610}, {0.883, 0.385}, {0.923, 0}}

    self.m_LeftCurve = CreateFromClass(AnimationCurve)
    self.m_RightCurve = CreateFromClass(AnimationCurve)
    for i = 1, #self.m_LeftCurveTab do
        local keyAndValue = self.m_LeftCurveTab[i]
        local k = CreateFromClass(Keyframe, keyAndValue[1], keyAndValue[2])
        CUICommonDef.SetKeyframeTangeMode(k, 21) -- LeftTangent_Linear & RightTangent_Linear
        CUICommonDef.SetAnimationCurveAddKey(self.m_LeftCurve, k)
    end

    for i = 1, #self.m_RightCurveTab do
        local keyAndValue = self.m_RightCurveTab[i]
        local k = CreateFromClass(Keyframe, keyAndValue[1], keyAndValue[2])
        CUICommonDef.SetKeyframeTangeMode(k, 21) -- LeftTangent_Linear & RightTangent_Linear
        CUICommonDef.SetAnimationCurveAddKey(self.m_RightCurve, k)
    end
end

function CLuaSoulCoreCondenseWnd:InitComponents()
    self.m_TimeLab      = self.transform:Find("Root/Middle/RemainTime/TimeLab"):GetComponent(typeof(UILabel))
    self.m_WeatherTip   = self.transform:Find("Root/Top/Tips"):GetComponent(typeof(UILabel))
    self.m_TipsLab      = self.transform:Find("Root/Bottom/Tips"):GetComponent(typeof(UILabel))
    self.m_ModelTex     = self.transform:Find("Root/Middle/SoulCore"):GetComponent(typeof(UITexture))
    self.m_ModelBg      = self.transform:Find("Root/Middle/SoulCore/Texture"):GetComponent(typeof(UITexture))
    self.m_ModelBg2     = self.transform:Find("Root/Middle/SoulCore/Texture02"):GetComponent(typeof(UITexture))
    self.m_Slider       = self.transform:Find("Root/Middle/SoulCore/Slider"):GetComponent(typeof(UISlider))
    self.m_SliderLab    = self.transform:Find("Root/Middle/SoulCore/Slider/ProgressLab"):GetComponent(typeof(UILabel))
    self.m_CancelBtn    = self.transform:Find("Root/Middle/Btns/UndoBtn"):GetComponent(typeof(CButton))
    self.m_JiasuBtn     = self.transform:Find("Root/Middle/Btns/JiasuBtn"):GetComponent(typeof(CButton))
    self.m_TipBtn       = self.transform:Find("Root/Middle/RemainTime/TipBtn"):GetComponent(typeof(QnButton))
    self.m_NingjieFx    = self.transform:Find("Root/Middle/SoulCore/NingjieFx"):GetComponent(typeof(CUIFx))
    self.m_JiasuFx      = self.transform:Find("Root/Middle/SoulCore/JiasuFx"):GetComponent(typeof(CUIFx))
    self.m_FlashFx      = self.transform:Find("Root/Middle/SoulCore/FlashFx"):GetComponent(typeof(CUIFx))
    self.m_FinishFx     = self.transform:Find("Root/Middle/SoulCore/FinishFx"):GetComponent(typeof(CUIFx))
    self.m_ProgressFx   = self.transform:Find("Root/Middle/SoulCore/ProgressFx"):GetComponent(typeof(CUIFx))

    self.m_TimeRoot     = self.transform:Find("Root/Middle/RemainTime").gameObject
    self.m_BtnsRoot     = self.transform:Find("Root/Middle/Btns").gameObject
    --雕琢
    self.m_ExplosionFx  = self.transform:Find("Root/Middle/SoulCore/ExplosionFx"):GetComponent(typeof(CUIFx))
    self.m_CarveTips    = self.transform:Find("Root/Middle/CarveTips").gameObject
    self.m_CarvingTip   = self.transform:Find("Root/Middle/CarveTips/CarvingTip"):GetComponent(typeof(UILabel))
    self.m_FinishTip    = self.transform:Find("Root/Middle/CarveTips/FinishTip"):GetComponent(typeof(UILabel))
    self.m_CarveSlider  = self.transform:Find("Root/Top/CarveSlider"):GetComponent(typeof(UISlider))
    self.m_AutoRotate   = self.transform:Find("Root/Middle/SoulCore"):GetComponent(typeof(CModelAutoRotate))
    self.m_CloseBtn     = self.transform:Find("CloseButton"):GetComponent(typeof(CButton))

    local clickFxs = self.transform:Find("Root/Middle/SoulCore/ClickFxs")
    self.m_ClickFxs = {}
    for i = 1, clickFxs.childCount do
        table.insert(self.m_ClickFxs, clickFxs:GetChild(i - 1):GetComponent(typeof(CUIFx)))
    end
    
end

function CLuaSoulCoreCondenseWnd:Init()
    self.m_CarveSlider.value = 0

    CommonDefs.AddOnClickListener(self.m_CloseBtn.gameObject, DelegateFactory.Action_GameObject(function(go)
        self:OnCloseBtnClicked(go)
    end), false)
    
    -- 重置灵核外观直接进入雕琢环节
    if LuaZongMenMgr.m_SoulCoreCondenseInReset then
        self:SwitchToCarve()
    else
        Gac2Gas.QueryCondenseLeftTime()
    end
end

function CLuaSoulCoreCondenseWnd:OnEnable()
    g_ScriptEvent:AddListener("SoulCoreCondenseUpdate", self, "OnSoulCoreCondenseUpdate")
    g_ScriptEvent:AddListener("SoulCoreCondenseJiasu", self, "OnSoulCoreCondenseJiasu")
    g_ScriptEvent:AddListener("SoulCoreCondenseFinish", self, "OnSoulCoreCondenseFinish")
    g_ScriptEvent:AddListener("WeatherIsChanged", self, "OnWeatherIsChanged")
end

function CLuaSoulCoreCondenseWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SoulCoreCondenseUpdate", self, "OnSoulCoreCondenseUpdate")
    g_ScriptEvent:RemoveListener("SoulCoreCondenseJiasu", self, "OnSoulCoreCondenseJiasu")
    g_ScriptEvent:RemoveListener("SoulCoreCondenseFinish", self, "OnSoulCoreCondenseFinish")
    g_ScriptEvent:RemoveListener("WeatherIsChanged", self, "OnWeatherIsChanged")
end
--更新界面状态
function CLuaSoulCoreCondenseWnd:OnSoulCoreCondenseUpdate()
    if self.m_Tab == 0 then
        if LuaZongMenMgr.m_SoulCoreCondensePhase == 1 then
            self:SwitchToCondense()
        elseif LuaZongMenMgr.m_SoulCoreCondensePhase == 2 then
            self:SwitchToCarve()
        end
    end

    --凝结界面的一些状态更新
    if self.m_Tab == 1 then
        --是否暂停
        self:CheckPauseOrResume()

        --天气变化
        self:CheckWeather()

        --刷新进度
        self:RefreshCondenseProgress()
    end
end

function CLuaSoulCoreCondenseWnd:OnCloseBtnClicked(go)
    if LuaZongMenMgr.m_SoulCoreCondenseInReset and not self.m_IsCarveFinish then
        local msg = g_MessageMgr:FormatMessage("SoulCore_Condense_CloseConfirm")
        g_MessageMgr:ShowOkCancelMessage(msg, function()
            CUIManager.CloseUI(CLuaUIResources.SoulCoreCondenseWnd)
          end, nil, nil, nil, false)
    else
        CUIManager.CloseUI(CLuaUIResources.SoulCoreCondenseWnd)
    end
end

function CLuaSoulCoreCondenseWnd:OnSoulCoreCondenseJiasu()
    self.m_JiasuFx:DestroyFx()
    self.m_JiasuFx:LoadFx("fx/ui/prefab/ui_lingheningjie_01_1.prefab")
    self:OnSoulCoreCondenseUpdate()
end
--切换至凝结
function CLuaSoulCoreCondenseWnd:SwitchToCondense()
    self.m_Tab = 1
    self:SwitchComponenet(self.m_Tab)

    self.m_NingjieFx.OnLoadFxFinish = DelegateFactory.Action(function ()
        self:CheckWeather()
    end)

    SoulCore_CondenseTips.Foreach(function(k,v)
        table.insert(self.m_Tips, v.Message)
    end)
    local index = math.random(1, #self.m_Tips)
    self.m_CurIndex = index
    self.m_TipsLab.text = self.m_Tips[index]

    self.m_WeatherTip.text = g_MessageMgr:FormatMessage("SoulCore_Condense_WeatherTip")

    CommonDefs.AddOnClickListener(self.m_CancelBtn.gameObject, DelegateFactory.Action_GameObject(function(go)
        if LuaZongMenMgr.m_SoulCoreCondensePause then
            CTrackMgr.Inst:FindNPC(tonumber(Menpai_Setting.GetData("TongTianTaNpcId").Value), tonumber(Menpai_Setting.GetData("RaidMapId").Value), 0, 0, nil, DelegateFactory.Action(function ()
                Gac2Gas.RequestResumeCondense()
            end))
        else
            Gac2Gas.RequestPauseCondense()
        end
    end), false)

    CommonDefs.AddOnClickListener(self.m_JiasuBtn.gameObject, DelegateFactory.Action_GameObject(function(go)
        self:OnJiasuBtnClick()
    end), false)

    self.m_TipBtn.OnClick = DelegateFactory.Action_QnButton(function(Lab)
        g_MessageMgr:ShowMessage("SoulCore_Condense_Tip")
    end)
    
    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.SoulCoreCondenseJiasu)
    self.m_Tick = RegisterTick(function()
        self:OnTick()
    end, self.m_Duration * 1000)
end
--切换至雕琢
function CLuaSoulCoreCondenseWnd:SwitchToCarve()
    self.m_Tab = 2
    self:SwitchComponenet(self.m_Tab)
    self.m_FinishTip.gameObject:SetActive(false)
    self.m_CarvingTip.gameObject:SetActive(true)
    self.m_ModelBg2.gameObject:SetActive(true)

    self.m_ProgressFx:DestroyFx()
    --放大背景及原石
    --背景1 1004*826 -> 1492*1209 (1,-3) -> (-14,-75) 1 -> 0.5
    --背景2 383*333 -> 1072*934
    --原石250*250? -> 350*350
    --self.m_Tweener = LuaTweenUtils.TweenScale(self.m_ModelTex.transform, Vector3(1, 1, 1), Vector3(1.3, 1.3, 1), 1.5)
    self.m_Tweener = LuaTweenUtils.TweenFloat(0, 1, 0.75, function (x)
        self.m_ModelBg.width = 1004 + 488 * x
        self.m_ModelBg.height = 826 + 383 * x
        self.m_ModelBg.alpha = 1 - 0.5 * x

        self.m_ModelBg2.width = 383 + 689 * x
        self.m_ModelBg2.height = 333 + 601 * x

        self.m_ModelTex.width = 250 + 450 * x
        self.m_ModelTex.height = 250 + 450 * x
    end)
    self.m_Tweener2 = LuaTweenUtils.DOLocalMove(self.m_ModelBg.transform, Vector3(-14, -75, 0), 0.75, false)
    LuaTweenUtils.SetEase(self.m_Tweener, Ease.OutQuad)
    LuaTweenUtils.SetEase(self.m_Tweener2, Ease.OutQuad)
    LuaTweenUtils.OnComplete(self.m_Tweener, function ()
        local localScale = self.m_ModelTex.transform.localScale
        self.m_ZeroPoint = UICamera.currentCamera:WorldToScreenPoint(self.m_ModelTex.transform.position)
        self.m_Width = localScale.x * self.m_ModelTex.width
        self.m_Height = localScale.y * self.m_ModelTex.height
        self.m_AdjustScale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
        self.m_TweenerIsComplete = true
    end)

    CommonDefs.AddOnDragListener(self.m_ModelTex.gameObject, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
		self:OnScreenDrag(go, delta)
    end), false)

    CommonDefs.AddOnClickListener(self.m_ModelTex.gameObject, DelegateFactory.Action_GameObject(function (go)
		self:OnClick(go)
    end), false)
    
    UIEventListener.Get(self.m_ModelTex.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(g)
        self.m_IsDraging = true
        self:CheckIfNeedRotate()
    end)

    UIEventListener.Get(self.m_ModelTex.gameObject).onDragEnd = DelegateFactory.VoidDelegate(function(g)
        self.m_IsDraging = false
        self:CheckIfNeedRotate()
    end)
    
    self.m_ChangeType, self.m_ChangeScale = self:GetLevelChangeTypeAndChangeScale()
    self.m_ModelTex.mainTexture = CUIManager.CreateModelTexture(self.ModelTextureName, LuaDefaultModelTextureLoader.Create(function (ro)
        self.m_Root = ro.transform.parent
        self:LoadModel(ro)
        self:InitShadowCameraAndLight()
        self.m_ModelCamera = CUIManager.instance.transform:Find(self.ModelTextureName):GetComponent(typeof(Camera))
    end) , 180, 0, -0.61, 4.66, false, true, 1)
end
--切换控件
function CLuaSoulCoreCondenseWnd:SwitchComponenet(tab)
    local isCarveTab = tab == 2
    self.m_Slider.gameObject:SetActive(not isCarveTab)
    self.m_TipsLab.gameObject:SetActive(not isCarveTab)
    self.m_WeatherTip.gameObject:SetActive(not isCarveTab)
    self.m_TimeRoot.gameObject:SetActive(not isCarveTab)
    self.m_BtnsRoot.gameObject:SetActive(not isCarveTab)

    self.m_CarveTips.gameObject:SetActive(isCarveTab)
    self.m_CarveSlider.gameObject:SetActive(isCarveTab)
end
--凝结
function CLuaSoulCoreCondenseWnd:OnSoulCoreCondenseFinish()
    if CUIManager.IsLoaded(CLuaUIResources.CommonItemChooseWnd) then
        CUIManager.CloseUI(CLuaUIResources.CommonItemChooseWnd)
    end

    if self.m_Tick ~= nil then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
    self.m_JiasuFx:DestroyFx()
    self.m_JiasuFx:LoadFx("fx/ui/prefab/ui_lingheningjie_01_1.prefab")

    self.m_FinishTick = RegisterTickOnce(function()
        self.m_FlashFx:LoadFx("fx/ui/prefab/ui_lingheningjie_03.prefab")
        self.m_FinishFx:LoadFx("fx/ui/prefab/ui_lingheningjie_02.prefab")
        self.m_WeatherTip.gameObject:SetActive(false)
        self.m_NingjieFx:DestroyFx()

        self:SwitchToCarve()
    end, 1500)
end

function CLuaSoulCoreCondenseWnd:CheckPauseOrResume()
    --是否暂停
    if LuaZongMenMgr.m_SoulCoreCondensePause then
        self:OnCondensePause()
    else
        self:OnCondenseResume()
    end
end

function CLuaSoulCoreCondenseWnd:OnWeatherIsChanged()
    self:CheckWeather()
end

function CLuaSoulCoreCondenseWnd:CheckWeather()
    --天气变化
    local currentWeathers = ((CWeatherMgr.Inst or {}).m_CurrentWeather or {}).Weathers
    if currentWeathers ~= nil and currentWeathers.Length > 0 then
        local currentWeather = currentWeathers[0]
        local data = SoulCore_ZongPaiWeather.GetData(EnumToInt(currentWeather))
        if data ~= nil then
            local type = data.Type
            if type == 2 or type == 3 then
                self:OnStartWeather()
                return
            end
        end
    end

    self:OnEndWeather()
end

function CLuaSoulCoreCondenseWnd:OnCondensePause()
    self.m_NingjieFx:DestroyFx()
    self.m_FinishFx:LoadFx("fx/ui/prefab/ui_lingheningjie_02.prefab")

    self.m_CancelBtn.label.text = LocalString.GetString("开始凝结")
end

function CLuaSoulCoreCondenseWnd:OnCondenseResume()
    self.m_FinishFx:DestroyFx()
    self.m_NingjieFx:LoadFx("fx/ui/prefab/ui_lingheningjie_01.prefab")

    self.m_CancelBtn.label.text = LocalString.GetString("暂停凝结")
end

function CLuaSoulCoreCondenseWnd:OnTick()
    self:RefreshCondenseProgress()

    --显示随机tip
    self.m_TimeCount = self.m_TimeCount + self.m_Duration
    if self.m_TimeCount > 20 then
        self.m_TimeCount = 0
        local index = math.random(1, #self.m_Tips)
        index = index == self.m_CurIndex and (index + 1) % #self.m_Tips or index
        if self.m_Tips[index] ~= nil then
            self.m_CurIndex = index
            self.m_TipsLab.text = self.m_Tips[index]
        end
    end
end

function CLuaSoulCoreCondenseWnd:RefreshCondenseProgress()
    local remainTime = nil
    self.m_RemainTimeCache = self.m_RemainTimeCache - self.m_Duration * LuaZongMenMgr.m_SoulCoreCondenseSpeed
    --计算剩余时间
    if LuaZongMenMgr.m_SoulCoreCondensePause then
        remainTime = LuaZongMenMgr.m_SoulCoreCondenseLeftTime
        remainTime = remainTime * LuaZongMenMgr.m_SoulCoreCondenseSpeed
    else
        remainTime = LuaZongMenMgr.m_SoulCoreCondenseEndTime - CServerTimeMgr.Inst.timeStamp
        remainTime = remainTime * LuaZongMenMgr.m_SoulCoreCondenseSpeed

        if math.abs(self.m_RemainTimeCache - remainTime) < 3 then
            remainTime = self.m_RemainTimeCache
        else 
            self.m_RemainTimeCache = remainTime
        end
    end
    if remainTime < 0 then
        remainTime = 0
    end
    --显示倒计时
    local minute = math.floor(remainTime / 60)
    local second = remainTime - minute * 60
    self.m_TimeLab.text = SafeStringFormat3("%02d:%02d", minute, second >= 0 and second or 0)
    --显示进度
    local progress = 1 - remainTime / LuaZongMenMgr.m_SoulCoreCondenseTotoalTime
    if progress < 0 then
        progress = 0
    end
    self.m_Slider.value = 0.865 * progress + 0.081 --填充进度映射
    self:AdjustSliderFx(progress)

    self.m_SliderLab.text = SafeStringFormat3("%2d%%", progress * 100)
end

function CLuaSoulCoreCondenseWnd:AdjustSliderFx(progress)
    if progress < 0.98 then 
        local value = 0.692 * progress + 0.231 --0.923
        self.m_ProgressFx:LoadFx("fx/ui/prefab/UI_yuanshi_yeti.prefab")
        local offsetX = (self.m_RightCurve:Evaluate(value) - self.m_LeftCurve:Evaluate(value)) * 0.5
        local scale = (self.m_RightCurve:Evaluate(value) + self.m_LeftCurve:Evaluate(value)) * 0.74
        LuaUtils.SetLocalPosition(self.m_ProgressFx.transform, offsetX * 209 - 10, -114 + 275 * progress, 0)
        LuaUtils.SetLocalScale(self.m_ProgressFx.transform, scale, 1, 1)
    else 
        self.m_ProgressFx:DestroyFx()
    end
end

function CLuaSoulCoreCondenseWnd:OnJiasuBtnClick()
    -- if not self:CheckHasSatisfyItem() then
    --     g_MessageMgr:ShowMessage("SoulCore_Condense_NoSatisfyItem")
    --     return
    -- end

    if not CUIManager.IsLoaded(CLuaUIResources.CommonItemChooseWnd) then
        local titleStr = LocalString.GetString("选择要加入的道具")
        local btnStr = LocalString.GetString("加入道具")
        local descStr = nil
        local enableMultiSelect = false
        local initFunc = nil
        local sortFunc = nil
        local itemSelectFunc = function (itemId)
            return self:IsSatisfyItem(itemId)
        end
        local commitFunc = function (items)
            if #items == 0 then
                g_MessageMgr:ShowMessage("SoulCore_Condense_NoSelectedItem")
                return
            end

            if items[1] ~= nil and not System.String.IsNullOrEmpty(items[1]) then
                local item = CItemMgr.Inst:GetById(items[1])
                local data = LianHua_Item.GetData(item.TemplateId)
                local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, item.Id)

                if data.Precious == 1 then
                    local speed = LuaZongMenMgr.m_SoulCoreCondenseSpeed and LuaZongMenMgr.m_SoulCoreCondenseSpeed or 1
                    local shortenTime = LuaZongMenMgr:GetCondenseShortenTime(data.Quality) * speed
                    -- 计算提速时间
                    local minute = math.floor(shortenTime / 60)
                    local second = shortenTime - minute * 60
                    local message = g_MessageMgr:FormatMessage("SoulCore_Condense_JiasuConfirm", item.ColoredName, SafeStringFormat3("%02d:%02d", minute, second))
                    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
                        Gac2Gas.RequestSubmitCondenseItem(EnumToInt(EnumItemPlace.Bag), pos, item.Id)
                    end), nil, nil, nil, false)
                else
                    Gac2Gas.RequestSubmitCondenseItem(EnumToInt(EnumItemPlace.Bag), pos, item.Id)
                end
            end
        end
        local clickFunc = function (descLab, items)
            if items[1] ~= nil and not System.String.IsNullOrEmpty(items[1]) then
                local item = CItemMgr.Inst:GetById(items[1])
                local data = LianHua_Item.GetData(item.TemplateId)
                local speed = LuaZongMenMgr.m_SoulCoreCondenseSpeed and LuaZongMenMgr.m_SoulCoreCondenseSpeed or 1
                local shortenTime = LuaZongMenMgr:GetCondenseShortenTime(data.Quality) * speed
                
                -- 计算提速时间
                local minute = math.floor(shortenTime / 60)
                local second = shortenTime - minute * 60
                descLab.text = SafeStringFormat3(LocalString.GetString("加入后可提速%02d:%02d"), minute, second)
            end
        end
        local accessTemplateIds = {}

        table.insert(accessTemplateIds, SoulCore_Settings.GetData().SoulCoreNingJieItem)
        
        LuaCommonItemChooseMgr:ShowItemChooseWnd(titleStr, btnStr, descStr, enableMultiSelect, itemSelectFunc, sortFunc, initFunc, commitFunc, clickFunc, nil, accessTemplateIds)
    end
end

function CLuaSoulCoreCondenseWnd:IsSatisfyItem(itemId)
    local data = nil
    if itemId ~= nil and not System.String.IsNullOrEmpty(itemId) then
        local item = CItemMgr.Inst:GetById(itemId)
        if item ~= nil then
            data = LianHua_Item.GetData(tonumber(item.TemplateId))
        end
    end
    -- 筛选
    return data ~= nil and data.Access == 1
end

function CLuaSoulCoreCondenseWnd:CheckHasSatisfyItem()
    local count = CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag)
    -- 遍历包裹里的物品
    for i=0, count-1 do
        local itemId = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i + 1)
        -- 确定合适的物品
        if self:IsSatisfyItem(itemId) then
            return true
        end
    end
    return false
end

function CLuaSoulCoreCondenseWnd:OnStartWeather()
    self.m_WeatherTip.gameObject:SetActive(true)
    self:TryChangeFxSpeed(2)
end

function CLuaSoulCoreCondenseWnd:OnEndWeather()
    self.m_WeatherTip.gameObject:SetActive(false)
    self:TryChangeFxSpeed(1)
end

function CLuaSoulCoreCondenseWnd:TryChangeFxSpeed(speed)
    local t1 = FindChild(self.m_NingjieFx.transform, "shoucici")
    local t2 = FindChild(self.m_NingjieFx.transform, "niuqu")
    if t1 == nil or t2 == nil then
        return
    end
    local fx1 = t1:GetComponent(typeof(ParticleSystem))
    local fx2 = t2:GetComponent(typeof(Renderer))
    
    if CUICommonDef.SetParticleSysLiftTimeAndEmissionRate ~= nil then
        CUICommonDef.SetParticleSysLiftTimeAndEmissionRate(fx1, 2 / speed, 2 * speed)
    end
    local mpb = CreateFromClass(MaterialPropertyBlock)
    fx2:GetPropertyBlock(mpb)
    mpb:SetFloat("_Add_U_Speed", -0.1 * speed)
    mpb:SetFloat("_Add_V_Speed", 0.2 * speed)
    fx2:SetPropertyBlock(mpb)
end

function CLuaSoulCoreCondenseWnd:GetGuideGo(methodName)
    if methodName == "GetJiasuButton" then
        return self.m_JiasuBtn.gameObject
    end
    return nil
end
--凝结end
--雕琢
function CLuaSoulCoreCondenseWnd:OnFinishCarve()
    --雕琢剩余part
    for i = 1 , #self.m_PartTable do
        local partGo = self.m_PartTable[i]
        if partGo ~= nil and partGo.active == true then
            self:OnPartCarved(partGo)
        end
    end

    self.m_IsCarveFinish = true
    self.m_FinishFx:DestroyFx()
    self.m_FinishTip.gameObject:SetActive(true)
    self.m_CarvingTip.gameObject:SetActive(false)
    self.m_CarveSlider.gameObject:SetActive(false)

    self.m_ExplosionFx:LoadFx("fx/ui/prefab/UI_linghediaozhuo_zhakai.prefab")


    local soulCoreProp = CClientMainPlayer.Inst.SkillProp.SoulCore
    local DisplayPalletId = soulCoreProp.DisplayPalletId > 0 and soulCoreProp.DisplayPalletId or 1
    local DisplayFxId = soulCoreProp.DisplayFxId > 0 and soulCoreProp.DisplayFxId or 1
    local DisplayCoreParticles = soulCoreProp.DisplayCoreParticles > 0 and soulCoreProp.DisplayCoreParticles or 1
    local DisplayCoreEffect = soulCoreProp.DisplayCoreEffect > 0 and soulCoreProp.DisplayCoreEffect or 1

    local palletRo = CRenderObject.CreateRenderObject(self.m_Root.gameObject, "pallet")
    LuaUtils.SetLocalPositionY(palletRo.transform, -2)
    palletRo:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(renderObject)
        NGUITools.SetLayer(renderObject.gameObject, LayerDefine.ModelForNGUI_3D)
    end))
    palletRo:LoadMain(LuaZongMenMgr:GetSoulCorePalletIdPath(CClientMainPlayer.Inst.SkillProp.SoulCore.DisplayPalletId), nil, true, false)
    LuaTweenUtils.DOLocalMoveY(palletRo.transform, self.m_YOffset, 1, false)
    LuaTweenUtils.TweenFloat(30, 30, 0.75, function (x)
        self.m_ModelCamera.fieldOfView = x
    end)

    -- local colors = CreateFromClass(MakeArrayClass(Color), 1)
    -- colors[0] = Color.blue
    local colors = nil

    local mingGe = CClientMainPlayer.Inst.BasicProp.MingGe
    local fx = CEffectMgr.Inst:AddObjectFX(LuaZongMenMgr:GetSoulCoreFxId(DisplayFxId),self.m_CoreRo,0,1,1,colors,false,EnumWarnFXType.None, Vector3(0, self.m_YLocalOffset, 0), Vector3.zero, DelegateFactory.Action_GameObject(function(go)
        LuaZongMenMgr:SetSoulCoreFxColor(go, true, mingGe)
    end))
    self.m_CoreRo:AddFX("DisplayFx", fx)

    local fx = CEffectMgr.Inst:AddObjectFX(LuaZongMenMgr:GetSoulCoreParticlesId(DisplayCoreParticles),self.m_CoreRo,0,1,1,colors,false,EnumWarnFXType.None, Vector3(0, self.m_YLocalOffset, 0), Vector3.zero, DelegateFactory.Action_GameObject(function(go)
        LuaZongMenMgr:SetSoulCoreFxColor(go, false, mingGe)
    end))
    self.m_CoreRo:AddFX("DisplayCoreParticles)", fx)

    local fx = CEffectMgr.Inst:AddObjectFX(LuaZongMenMgr:GetSoulCoreCoreEffectId(DisplayCoreEffect, self.m_ChangeType),self.m_CoreRo,0,1,1,colors,false,EnumWarnFXType.None, Vector3(0, self.m_YLocalOffset, 0), Vector3.zero, DelegateFactory.Action_GameObject(function(go)
        LuaZongMenMgr:SetSoulCoreFxColor(go, false, mingGe)
    end))
    self.m_CoreRo:AddFX("DisplayCoreEffect", fx)


    self.m_CoreRo.Layer = LayerDefine.ModelForNGUI_3D
    -- 重置灵核外观只是客户端本地走个过场，没有与服务器交互
    if not LuaZongMenMgr.m_SoulCoreCondenseInReset then
        Gac2Gas.RequestFinishSoulcoreCarving()
    end
end

function CLuaSoulCoreCondenseWnd:LoadModel(ro)
    ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(renderObject)
        self:OnLoadFinished(renderObject)
    end))
    ro:LoadMain(self.m_SoulCoreOSPrefab, nil, false, false)
end

function CLuaSoulCoreCondenseWnd:OnLoadFinished(ro)
    ro.Layer = LayerDefine.ModelForNGUI_3D

    local os = ro.transform:GetChild(0)
    LuaUtils.SetLocalScale(os, 1.5, 1.5, 1.5)
    local maxRate = SoulCore_Settings.GetData().SoulCoreCarveMaxRate or 1
    self.m_MaxPartCount = os.transform.childCount - 1
    self.m_MaxPartCount = self.m_MaxPartCount * maxRate
    self.m_CurPartCount = self.m_MaxPartCount

    -- 原石perfab下最后一个child是TopAnchor,-2以忽略
    for i = 0, os.childCount - 2 do
        local go = os:GetChild(i).gameObject
        self.m_PartTable[i+1] = go
    end

    os:GetComponent(typeof(Collider)).enabled = false
    local coreRo = CRenderObject.CreateRenderObject(ro.gameObject, "core")
    local mingGe = CClientMainPlayer.Inst.BasicProp.MingGe
    local DisplayCoreId = CClientMainPlayer.Inst.SkillProp.SoulCore.DisplayCoreId
    LuaZongMenMgr:SetSoulCoreCoreColor(ro, 2, 2)
    coreRo:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(renderObject)
        local soulCore = renderObject.transform:GetChild(0)
        soulCore:GetComponent(typeof(Collider)).enabled = false

        local centerTrans = soulCore:Find("Center")
        local centerLocalOffset = centerTrans and centerTrans.localPosition.y or 0
        self.m_YOffset = 0.56 - centerLocalOffset
        self.m_YLocalOffset = centerLocalOffset
        LuaUtils.SetLocalPositionY(coreRo.transform, self.m_YOffset)

        NGUITools.SetLayer(renderObject.gameObject, LayerDefine.ModelForNGUI_3D)
        LuaZongMenMgr:SetSoulCoreCoreColor(renderObject, SoulCore_DisplayCoreId.GetData(DisplayCoreId).Type, mingGe)
    end))
    coreRo:LoadMain(LuaZongMenMgr:GetSoulCoreCorePath(DisplayCoreId), nil, true, false)

    self.m_CoreRo = coreRo
    self.m_AutoRotate:Init(self.ModelTextureName, 180, self.m_AutoRotSpeed)
    self:CheckIfNeedRotate()
end

function CLuaSoulCoreCondenseWnd:OnScreenDrag(go, delta)
    local rot = -delta.x / Screen.width * 360
	self.m_AutoRotate:Rotate(rot)
end

function CLuaSoulCoreCondenseWnd:OnClick(go)
    --放大tweener中不让雕琢,雕琢完毕不让雕琢
    if not self.m_TweenerIsComplete or self.m_IsCarveFinish then
        return
    end

    local currentPos = UICamera.currentTouch.pos
    local x = ((currentPos.x - self.m_ZeroPoint.x) * self.m_AdjustScale + self.m_Width / 2) / self.m_Width
    local y = ((currentPos.y - self.m_ZeroPoint.y ) * self.m_AdjustScale + self.m_Height / 2) / self.m_Height

    local ray = self.m_ModelCamera:ViewportPointToRay(Vector3(x,y,0))

    local bFind, hit = CommonDefs.Physics_Raycast_Ray_RaycastHit(ray)
    if bFind then
        self:OnPartClicked(hit)
    end
end

function CLuaSoulCoreCondenseWnd:OnPartClicked(hit)
    local hitPos = hit.point
    local hitGo = hit.collider.gameObject

    self:OnPartCarved(hitGo)
    --TEST 影响的碎片的范围
    local effectRange = SoulCore_Settings.GetData().SoulCoreCarveEffectRange or 0
    for i = 1 , #self.m_PartTable do
        local partGo = self.m_PartTable[i]

        if partGo ~= nil and partGo.active == true and hitGo ~= partGo then
            local length = (CommonDefs.op_Subtraction_Vector3_Vector3(hitPos, partGo.transform.position)).magnitude
            if length <= effectRange then
                self:OnPartCarved(partGo)
            end
        end
    end

    if self.m_CurPartCount >= 0.1 then
        self.m_CarveSlider.value = 1 - self.m_CurPartCount / self.m_MaxPartCount
    else
        self.m_CarveSlider.value = 0
    end

    if not self.m_IsCarveFinish and self.m_CurPartCount <= 0.1 then
        self:OnFinishCarve()
    end

    self:CheckIfNeedRotate()
end

function CLuaSoulCoreCondenseWnd:PlayCarveFx(go)
    local clickFx = self.m_ClickFxs[self.m_ClickFxIndex]
    self.m_ClickFxIndex = self.m_ClickFxIndex + 1
    if self.m_ClickFxIndex > #self.m_ClickFxs then
        self.m_ClickFxIndex = 1
    end

    local modelViewPos = self.m_ModelCamera:WorldToViewportPoint(go.transform.position)
    local x = (modelViewPos.x * self.m_Width - self.m_Width / 2 ) / self.m_AdjustScale + self.m_ZeroPoint.x
    local y = (modelViewPos.y * self.m_Height - self.m_Height / 2 ) / self.m_AdjustScale + self.m_ZeroPoint.y

    clickFx.transform.position  = UICamera.currentCamera:ScreenToWorldPoint(Vector3(x,y,0))
    clickFx:DestroyFx()
    clickFx:LoadFx("fx/ui/prefab/UI_linghediaozhuo_dianji.prefab")
end

function CLuaSoulCoreCondenseWnd:OnPartCarved(go)
    self:PlayCarveFx(go)
    self.m_CurPartCount = self.m_CurPartCount - 1
    go:SetActive(false)
end

function CLuaSoulCoreCondenseWnd:CheckIfNeedRotate()
    local IsNeedRotate = self.m_Tab == 2 and not self.m_IsDraging
    self.m_AutoRotate.IsRotating = IsNeedRotate
end

--雕琢end
function CLuaSoulCoreCondenseWnd:OnDestroy()
    if self.m_Tick ~= nil then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end

    if self.m_FinishTick ~= nil then
        UnRegisterTick(self.m_FinishTick)
        self.m_FinishTick = nil
    end

    if self.m_Tweener ~= nil then
        LuaTweenUtils.Kill(self.m_Tweener, false)
    end

    if self.m_Tweener2 ~= nil then
        LuaTweenUtils.Kill(self.m_Tweener2, false)
    end

    self.m_ModelTex.mainTexture = nil
    CUIManager.DestroyModelTexture(self.ModelTextureName)
    -- 重置灵核外观清除标记
    LuaZongMenMgr.m_SoulCoreCondenseInReset = false

    if CUIManager.IsLoaded(CLuaUIResources.CommonItemChooseWnd) then
        CUIManager.CloseUI(CLuaUIResources.CommonItemChooseWnd)
    end
end

function CLuaSoulCoreCondenseWnd:InitShadowCameraAndLight()
    local t = CUIManager.instance.transform:Find(self.ModelTextureName)
    if not t then return end
    if t:Find("LightAndFloorAndWall") ~= nil then
        return
    end
    local newRO = CRenderObject.CreateRenderObject(t.gameObject, "LightAndFloorAndWall")
    local fx = CEffectMgr.Inst:AddObjectFX(88801543, newRO ,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero, DelegateFactory.Action_GameObject(function(go)
        NGUITools.SetLayer(newRO.gameObject,LayerDefine.Effect_3D)
        local beijingRenderer = go.transform:Find("Anchor/beijing"):GetComponent(typeof(Renderer))
        local spotLight = go.transform:Find("Anchor/Spotlight")
        local particle = go.transform:Find("Anchor/PretendParticleEffect"):GetComponent(typeof(ParticleSystem))

        LuaUtils.SetLocalPositionY(beijingRenderer.transform, -15)
        local mpb = CreateFromClass(MaterialPropertyBlock)
        beijingRenderer:GetPropertyBlock(mpb)
        mpb:SetColor("_Color", NGUIText.ParseColor24("00030EFF", 0))
        beijingRenderer:SetPropertyBlock(mpb)
        spotLight.gameObject:SetActive(false)--00030EFF
        LuaUtils.SetLocalPosition(particle.transform, 4.04, -7.29, -3.66)
        particle.maxParticles = 15
    end))
    newRO:AddFX("LightAndFloorAndWall", fx, -1)
end

function CLuaSoulCoreCondenseWnd:GetLevelChangeTypeAndChangeScale()
    local mainPlayer = (CClientMainPlayer.Inst or {})
    local soulCoreProp = (mainPlayer.SkillProp or {}).SoulCore
    local level = soulCoreProp.Level
    local levelSoulCoreChange = SoulCore_Settings.GetData().SoulCoreChange
    local changeType = 0
    local changeScale = levelSoulCoreChange[0][1] 
    for i = levelSoulCoreChange.Length - 1, 0, -1 do
        if level >= levelSoulCoreChange[i][0] then
            changeType = i
            changeScale = levelSoulCoreChange[i][1] 
            break
        end
    end

    return changeType, changeScale
end
