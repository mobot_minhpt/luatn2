local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CFriendChatView = import "L10.UI.CFriendChatView"
local EnumEventType = import "EnumEventType"
local CIMMgr = import "L10.Game.CIMMgr"
local CChatMgr = import "L10.Game.CChatMgr"

LuaQuickChatWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQuickChatWnd, "FriendChatView", CFriendChatView)
RegistChildComponent(LuaQuickChatWnd, "ContentBg", UISprite)
RegistChildComponent(LuaQuickChatWnd, "ScrollView", UIPanel)
RegistChildComponent(LuaQuickChatWnd, "FriendlinessLabel", GameObject)

RegistClassMember(LuaQuickChatWnd, "m_OppositeId")
RegistClassMember(LuaQuickChatWnd, "m_OppositeName")
RegistClassMember(LuaQuickChatWnd, "m_ContentBgHeight")
--@endregion RegistChildComponent end

function LuaQuickChatWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaQuickChatWnd:Init()
    self.m_OppositeId = CChatMgr.Inst.m_QuickChatOppositeId
    self.m_OppositeName = CChatMgr.Inst.m_QuickChatOppositeName
    self.m_ContentBgHeight = self.ContentBg.height
    self.FriendChatView:Init(0, false, self.m_OppositeId, self.m_OppositeName, false)
    self:SpecialInit()
end

function LuaQuickChatWnd:SpecialInit()
    self.FriendlinessLabel:SetActive(false)
    self.ContentBg.height = self.m_ContentBgHeight
    self.ScrollView:ResetAndUpdateAnchors()
    self.FriendChatView.tableView:LoadDataFromTail()
end

function LuaQuickChatWnd:OnRecvIMMsgInfo(senderId)
    if senderId == self.m_OppositeId and not CUIManager.IsLoaded("FriendWnd") then
        CIMMgr.Inst:FetchIMMsg(senderId)
    end
end

--@region UIEvent

--@endregion UIEvent

function LuaQuickChatWnd:OnEnable()
    self.m_SelectSuccessAction =  DelegateFactory.Action_ulong(function(senderId)
        self:OnRecvIMMsgInfo(senderId)
    end)
    EventManager.AddListenerInternal(EnumEventType.RecvIMMsgInfo, self.m_SelectSuccessAction)
end

function LuaQuickChatWnd:OnDisable()
    EventManager.RemoveListenerInternal(EnumEventType.RecvIMMsgInfo, self.m_SelectSuccessAction)
end