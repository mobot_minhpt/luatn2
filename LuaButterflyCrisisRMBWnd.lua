local UIGrid = import "UIGrid"
local ButterflyCrisis_RMB = import "L10.Game.ButterflyCrisis_RMB"
local CUITexture = import "L10.UI.CUITexture"
local Mall_LingYuMallLimit = import "L10.Game.Mall_LingYuMallLimit"
local Extensions = import "Extensions"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CPayMgr = import "L10.Game.CPayMgr"
local CButton = import "L10.UI.CButton"
local CShopMallMgr = import "L10.UI.CShopMallMgr"

LuaButterflyCrisisRMBWnd = class()

--@region 

-- RMB礼包展示
RegistChildComponent(LuaButterflyCrisisRMBWnd, "RMBRewardGrid", UIGrid)
RegistChildComponent(LuaButterflyCrisisRMBWnd, "RMBRewardItem", GameObject)

RegistClassMember(LuaButterflyCrisisRMBWnd, "RewardItemList")
RegistClassMember(LuaButterflyCrisisRMBWnd, "SelectedRMBIndex")

-- 详情
RegistChildComponent(LuaButterflyCrisisRMBWnd, "DetailInfos", GameObject)
RegistChildComponent(LuaButterflyCrisisRMBWnd, "DetailRewardNameLabel", UILabel)
RegistChildComponent(LuaButterflyCrisisRMBWnd, "DetailInfoGrid", UIGrid)
RegistChildComponent(LuaButterflyCrisisRMBWnd, "DetailInfoItem", GameObject)
RegistChildComponent(LuaButterflyCrisisRMBWnd, "BuyBtn", CButton)
RegistChildComponent(LuaButterflyCrisisRMBWnd, "LockHintLabel", UILabel)
RegistChildComponent(LuaButterflyCrisisRMBWnd, "BoughtStatus", GameObject)

-- RMB礼包的信息
RegistClassMember(LuaButterflyCrisisRMBWnd, "RMBPackageInfos")
RegistClassMember(LuaButterflyCrisisRMBWnd, "GiftLimits")

--@endregion


function LuaButterflyCrisisRMBWnd:Awake()
    self.RMBRewardItem:SetActive(false)
    self.DetailInfoItem:SetActive(false)
    self.RMBPackageInfos = {}
    self.GiftLimits = {}
    self.SelectedRMBIndex = 0
end


function LuaButterflyCrisisRMBWnd:Init()
    Gac2Gas.RequestPlayerItemLimit(EShopMallRegion_lua.ELingyuMallLimit)
end

--@desc 更新RMB礼包相关的信息
function LuaButterflyCrisisRMBWnd:UpdateRMBPackageInfos()
    self.RMBPackageInfos = {}

    ButterflyCrisis_RMB.ForeachKey(DelegateFactory.Action_object(function (key)
        local rmbInfo = ButterflyCrisis_RMB.GetData(key)
        if rmbInfo then
            -- isOpen: NPC是否对外开放, isFinished: 是否通关, isBought: 是否购买过
            -- 打开RMB礼包界面的时候，应该白蝶风波主界面是开的，所以有相关Boss信息
            local isOpen = LuaButterflyCrisisMgr.m_NPCOpenInfo[key]
            local isFinished = LuaButterflyCrisisMgr.m_BossSolvedInfo[key]
            local isBought = self:IsRMBPackageBought(rmbInfo.ItemId)
            table.insert(self.RMBPackageInfos, {RmbInfo = rmbInfo, isOpen = isOpen, isFinished = isFinished, isBought = isBought})
        end
    end))
end

--@desc 获取RMB礼包的限购信息（是否可以购买）
function LuaButterflyCrisisRMBWnd:IsRMBPackageBought(itemId)
    if not self.GiftLimits or not itemId then return false end
    if not self.GiftLimits[itemId] then return false end
    return self.GiftLimits[itemId] <= 0
end

--@desc 刷新界面上部的RMB礼包Grid
function LuaButterflyCrisisRMBWnd:UpdateRewardGrid()
    self:UpdateRMBPackageInfos()

    self.RewardItemList = {}
    CUICommonDef.ClearTransform(self.RMBRewardGrid.transform)

    for i = 1, #self.RMBPackageInfos do
        local info = self.RMBPackageInfos[i]
        local go = NGUITools.AddChild(self.RMBRewardGrid.gameObject, self.RMBRewardItem)
        self:InitRewardItem(go, info, i)
        go:SetActive(true)
        table.insert(self.RewardItemList, go)
    end

    self.RMBRewardGrid:Reposition()
    self:UpdateSelections()
end

--@desc 默认选中第一个未购买且开放的礼包
function LuaButterflyCrisisRMBWnd:UpdateSelections()
    -- 首次打开生效
    if self.SelectedRMBIndex == 0 then
        local needSelectIndex= 0
        for i = 1, #self.RMBPackageInfos do
            local info = self.RMBPackageInfos[i]
            if info and info.isOpen and not info.isBought then
                if needSelectIndex == 0 then
                    needSelectIndex = i
                end
            end
        end

        self:OnRewardItemClicked(needSelectIndex)
    end
end

--@desc 初始化奖励
--@go: 对象
--@info: RMBPackage数据
--@index: info在RMBPackageInfos中的index，也是go在RewardItemList中的index
function LuaButterflyCrisisRMBWnd:InitRewardItem(go, info, index)
    if not go or not info then return end
    local rewardNameLabel = go.transform:Find("RewardNameLabel"):GetComponent(typeof(UILabel))
    rewardNameLabel.text = nil

    local rewardPriceLabel = go.transform:Find("RewardPriceLabel"):GetComponent(typeof(UILabel))
    rewardPriceLabel.text = nil

    local originalPriceLabel = go.transform:Find("OriginalPriceLabel"):GetComponent(typeof(UILabel))
    originalPriceLabel.gameObject:SetActive(false)

    local rewardStatusSprite = go.transform:Find("RewardStatusSprite").gameObject
    rewardStatusSprite:SetActive(false)

    local rewardIconTexture = go.transform:Find("RewardIconTexture"):GetComponent(typeof(CUITexture))
    rewardIconTexture:Clear()

    local rewardSelected = go.transform:Find("RewardSelected").gameObject
    rewardSelected:SetActive(false)

    local rewardUnlock = go.transform:Find("RewardUnlock").gameObject
    rewardUnlock:SetActive(false)

    local rewardDiscount = go.transform:Find("RewardDiscount").gameObject
    rewardDiscount:SetActive(false)

    local rewardDiscountLabel = go.transform:Find("RewardDiscount/RewardDiscountLabel"):GetComponent(typeof(UILabel))

    local item = Item_Item.GetData(info.RmbInfo.ItemId)
    local lingyuMall = Mall_LingYuMallLimit.GetData(info.RmbInfo.ItemId)

    rewardUnlock:SetActive(not info.isOpen)
    rewardIconTexture.gameObject:SetActive(info.isOpen)

    if not item or not lingyuMall then return end

    if not info.isOpen then
        -- NPC 还未开放
        Extensions.SetLocalPositionZ(go.transform, -1)
    else
        rewardNameLabel.text = item.Name
        rewardIconTexture:LoadMaterial(item.Icon)
        rewardPriceLabel.text = SafeStringFormat3(LocalString.GetString("%s元"), tostring(lingyuMall.Jade))

        if lingyuMall.Discount ~= 0 then
            local orginalPrice = lingyuMall.Jade  * 10 / lingyuMall.Discount
            originalPriceLabel.text = SafeStringFormat3(LocalString.GetString("%s元"), tostring(math.floor(orginalPrice)))
            rewardDiscountLabel.text = SafeStringFormat3(LocalString.GetString("%s折"), tostring(lingyuMall.Discount))
        end
        
        if not info.isFinished then
            -- 玩家还未通关
            Extensions.SetLocalPositionZ(go.transform, -1)
        else
            Extensions.SetLocalPositionZ(go.transform, 0)
        end

        -- 根据是否购买显示购买状态和价格
        rewardPriceLabel.gameObject:SetActive(not info.isBought)
        originalPriceLabel.gameObject:SetActive(not info.isBought and lingyuMall.Discount ~= 0)
        rewardDiscount:SetActive(not info.isBought and lingyuMall.Discount ~= 0)
        rewardStatusSprite:SetActive(info.isBought)
    end

    CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function (go)
        if not info.isOpen then
            g_MessageMgr:ShowMessage("Butterfly_Crisis_RMB_NPC_Not_Open")
        else
            self:OnRewardItemClicked(index)
        end
    end), false)
end

-- 遍历RewardItemList，处理选中状态
function LuaButterflyCrisisRMBWnd:ShowRewardItemsSelection()
    for i = 1, #self.RewardItemList do
        self:SetRewardItemSelected(self.RewardItemList[i], i == self.SelectedRMBIndex)
    end
end

--@item: 对象
--@isSelected: 是否被选中
function LuaButterflyCrisisRMBWnd:SetRewardItemSelected(item, isSelected)
    if not item then return end
    local rewardSelected = item.transform:Find("RewardSelected").gameObject
    rewardSelected:SetActive(isSelected) 
end

function LuaButterflyCrisisRMBWnd:OnRewardItemClicked(index)
    if self.SelectedRMBIndex ~= index then
        
        self.SelectedRMBIndex = index
        self:ShowRewardItemsSelection()
        self:ShowDetailInfo()
    end
end

--@region 礼包详情

--@desc 显示RMB礼包对应的详细信息, 如果NPC尚未解锁，则不用显示对应的详情
function LuaButterflyCrisisRMBWnd:ShowDetailInfo()
    if not self.RMBPackageInfos then return end

    local info = self.RMBPackageInfos[self.SelectedRMBIndex]

    self.DetailInfos:SetActive(info and info.isOpen)

    if not info then return end

    if not info.isOpen then
        g_MessageMgr:ShowMessage("Butterfly_Crisis_RMB_NPC_Not_Open")
    else
        local item = Item_Item.GetData(info.RmbInfo.ItemId)
        self.DetailRewardNameLabel.text = item.Name
        self:UpdateDetailInfoGrid()
        self:UpdateRMBPackageStatus()
    end
end

--@desc 更新RMB礼包所包含的物品
function LuaButterflyCrisisRMBWnd:UpdateDetailInfoGrid()
    CUICommonDef.ClearTransform(self.DetailInfoGrid.transform)

    local info = self.RMBPackageInfos[self.SelectedRMBIndex]
    if not info then return end

    local items = info.RmbInfo.IncludedItemId

    for i = 0, items.Length-1, 2 do
        local go = NGUITools.AddChild(self.DetailInfoGrid.gameObject, self.DetailInfoItem)
        local itemId = items[i]
        local count = items[i+1]
        self:InitDetailInfoItem(go, itemId, count)
        go:SetActive(true)
    end

    self.DetailInfoGrid:Reposition()
end

--@go: 对象
--@itemId: 奖励物品ID
--@count: 奖励物品的个数
function LuaButterflyCrisisRMBWnd:InitDetailInfoItem(go, itemId, count)
    if not go then return end
    
    local iconTexture = go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local countLabel = go.transform:Find("CountLabel"):GetComponent(typeof(UILabel))

    local item = Item_Item.GetData(itemId)
    if not item then return end

    iconTexture:LoadMaterial(item.Icon)
    countLabel.text = tostring(count)

    CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function (gameObject)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
    end), false)
end

--@desc 更新礼包详情的状态
function LuaButterflyCrisisRMBWnd:UpdateRMBPackageStatus()
    local info = self.RMBPackageInfos[self.SelectedRMBIndex]
    if not info then return end

    local playData = ButterflyCrisis_Play.GetData(self.SelectedRMBIndex)
    self.LockHintLabel.gameObject:SetActive(not info.isFinished)
    if playData then
        local npc = NPC_NPC.GetData(playData.NpcTemplateId)
        if npc then
            self.LockHintLabel.text = g_MessageMgr:FormatMessage("Butterfly_Crisis_RMB_Reward_Hint", npc.Name)
        end
    end
    
    if info.isBought then
        self.BuyBtn.gameObject:SetActive(false)
        self.BoughtStatus:SetActive(true)
    else
        self.BuyBtn.gameObject:SetActive(true)
        self.BoughtStatus:SetActive(false)
        if not info.isFinished then
            self.BuyBtn.Text = LocalString.GetString("未解锁")
            self.BuyBtn.Enabled = false
        else
            self.BuyBtn.Text = LocalString.GetString("购买")
            self.BuyBtn.Enabled = true
            CommonDefs.AddOnClickListener(self.BuyBtn.gameObject, DelegateFactory.Action_GameObject(function ( go )
                local data = Mall_LingYuMallLimit.GetData(info.RmbInfo.ItemId)
                CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(data.RmbPID), 0)
            end), false)
        end
    end
end

--@desc 更新CShopMgr中整体礼包的限购信息
function LuaButterflyCrisisRMBWnd:UpdateMarketLimit(args)

    local regionId = args[0]
    local limitInfo = args[1]
    self.GiftLimits = {}
    for i = 0 , limitInfo.Count-1, 2 do
        local key = math.floor(tonumber(limitInfo[i] or 0))
        local val = math.floor(tonumber(limitInfo[i + 1] or 0))
        self.GiftLimits[key] = val
    end

    self:UpdateRMBPackageLimits()
end

--@desc 刷新RMB界面
function LuaButterflyCrisisRMBWnd:UpdateRMBPackageLimits()
    self:UpdateRewardGrid()
    self:UpdateRMBPackageStatus()
end

function LuaButterflyCrisisRMBWnd:OnEnable()
    CShopMallMgr.Inst:EnableEvent()
    g_ScriptEvent:AddListener("SendMallMarketItemLimitUpdate", self, "UpdateMarketLimit")
    g_ScriptEvent:AddListener("AutoShangJiaItemUpdate", self, "UpdateMarketLimit")
end

function LuaButterflyCrisisRMBWnd:OnDisable()
    CShopMallMgr.Inst:DisableEvent()
    g_ScriptEvent:RemoveListener("SendMallMarketItemLimitUpdate", self, "UpdateMarketLimit")
    g_ScriptEvent:RemoveListener("AutoShangJiaItemUpdate", self, "UpdateMarketLimit")
end

--@endregion

