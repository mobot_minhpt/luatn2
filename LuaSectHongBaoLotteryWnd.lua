local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local QnTableItem = import "L10.UI.QnTableItem"
local CItem = import "L10.Game.CItem"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Ease = import "DG.Tweening.Ease"
local CButton = import "L10.UI.CButton"

LuaSectHongBaoLotteryWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSectHongBaoLotteryWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaSectHongBaoLotteryWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaSectHongBaoLotteryWnd, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaSectHongBaoLotteryWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaSectHongBaoLotteryWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaSectHongBaoLotteryWnd, "MainView", "MainView", GameObject)
RegistChildComponent(LuaSectHongBaoLotteryWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaSectHongBaoLotteryWnd, "InfoLabel", "InfoLabel", UILabel)
--@endregion RegistChildComponent end
RegistClassMember(LuaSectHongBaoLotteryWnd, "m_ItemPosList")
RegistClassMember(LuaSectHongBaoLotteryWnd, "m_BtnList")
RegistClassMember(LuaSectHongBaoLotteryWnd, "m_DataList")
RegistClassMember(LuaSectHongBaoLotteryWnd, "m_Tick")
RegistClassMember(LuaSectHongBaoLotteryWnd, "m_LotteryDeadline")

function LuaSectHongBaoLotteryWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)
	
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)


    --@endregion EventBind end
end

function LuaSectHongBaoLotteryWnd:Init()
	self.ItemTemplate.gameObject:SetActive(false)
	Gac2Gas.RequestWatchSectItemRedPackLottery(LuaZongMenMgr.m_SectHongBaoId)
end

function LuaSectHongBaoLotteryWnd:OnEnable()
	g_ScriptEvent:AddListener("OnSendSectItemPackLotteryInfo",self,"OnSendSectItemPackLotteryInfo")
	--g_ScriptEvent:AddListener("OnSendWinSectItemPack",self,"OnSendWinSectItemPack")
end

function LuaSectHongBaoLotteryWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSendSectItemPackLotteryInfo",self,"OnSendSectItemPackLotteryInfo")
	Gac2Gas.RequestLeaveWatchSectItemRedPackLottery()
	self:CancelTick()
	self:KillTweener()
end

function LuaSectHongBaoLotteryWnd:OnSendSectItemPackLotteryInfo(currentLotteryPlayerId, currentLotteryPlayerName, myStatus, myQueuePos,myLotteryDeadline, playAnimate, animateFinishTime, animateStopPos, lotteryInfoUD)
    --print("SendSectItemPackLotteryInfo", currentLotteryPlayerId, currentLotteryPlayerName, myStatus, myQueuePos,myLotteryDeadline, playAnimate, animateFinishTime, animateStopPos)
    -- 我的抽奖状态 0 未参与抽奖 1 已抽完 2 当前是自己抽且未确认参加抽奖 3 当前是自己抽且未点抽奖按钮 4 当前是自己抽且已点抽奖按钮 5 还在排队等待抽奖
	self.NameLabel.text = String.IsNullOrEmpty(currentLotteryPlayerName) and LocalString.GetString("无") or currentLotteryPlayerName
	local list = lotteryInfoUD and MsgPackImpl.unpack(lotteryInfoUD)
	Extensions.RemoveAllChildren(self.MainView.transform)
	local t = myLotteryDeadline - CServerTimeMgr.Inst.timeStamp
	self.m_LotteryDeadline = myLotteryDeadline
	self:ShowCountDownLabel()
	if t > 0 then
		self:CancelTick()
		self.m_Tick = RegisterTick(function ()
			self:ShowCountDownLabel()
		end,500)
	end
	self.m_BtnList = {}
	self.m_DataList = {}
	self:InitItemPosList( math.floor(list.Count / 5))
	
	local hasDrawed = false
    for i = 0, list.Count-1, 5 do
        local itemId = list[i] -- 道具ID
        local itemCount = list[i+1] -- 道具数量
        local isBest = list[i+2] -- 是否最佳
        local winnerId = list[i+3] -- 获得的玩家ID
        local winnerName = list[i+4] -- 获得的玩家名
        --print("SendSectItemPackLotteryInfo detail:", itemId, itemCount, isBest, winnerId, winnerName)
		local t = {itemId = itemId, itemCount = itemCount, isBest = isBest, winnerId = winnerId, winnerName = winnerName}
		if CClientMainPlayer.Inst and winnerId == CClientMainPlayer.Inst.Id then
			hasDrawed = true
		end
        table.insert(self.m_DataList, t)
		local obj = NGUITools.AddChild(self.MainView.gameObject, self.ItemTemplate.gameObject)
		self:InitItem(obj,t,math.floor(i / 5))
    end
	self.Button.gameObject:SetActive(myStatus == 3)
	self.InfoLabel.text = hasDrawed and LocalString.GetString("已完成抽奖") or ""
	if myStatus == 5 then
		self.InfoLabel.text = SafeStringFormat3(LocalString.GetString("排队中 第%d位"),myQueuePos)
	end
	self.InfoLabel.gameObject:SetActive(hasDrawed or myStatus == 5)
	self:ShowAni(playAnimate, animateFinishTime, animateStopPos)
end

function LuaSectHongBaoLotteryWnd:InitItemPosList(length)
	self.m_ItemPosList = {}
	local data = SectItemRedPack_Position.GetData(length)
	if data then
		for i = 0,data.ItemsPos2.Length - 1 do
			local itemPosData = data.ItemsPos2[i]
			if itemPosData.Length == 2 then
				table.insert(self.m_ItemPosList,{x = itemPosData[0], y = itemPosData[1]})
			end
		end
	end
end

function LuaSectHongBaoLotteryWnd:ShowAni(playAnimate, animateFinishTime, animateStopPos)
	if playAnimate then
		self:KillTweener()
		local t = (animateFinishTime - CServerTimeMgr.Inst.timeStamp) 
		local itemNum = 0
		local minMoveNum = 0
		local itemIndexList = {}
		for index,t in pairs(self.m_DataList) do
			if index == animateStopPos then
				minMoveNum = itemNum
			end
			if t.winnerId == 0 then
				itemIndexList[itemNum] = index
				itemNum = itemNum + 1
			end
		end
		local animationMinNum = SectItemRedPack_Setting.GetData().AnimationMinNum
		local animationMaxNum = SectItemRedPack_Setting.GetData().AnimationMaxNum
		local list = {}
		for i = minMoveNum, animationMaxNum, itemNum do
			if i >=animationMinNum then
				table.insert(list, i)
			end
		end
		if itemNum == 1 or #list == 0 then
			self:SelectBtn(animateStopPos)
			return
		end
		local randomIndex = math.random(#list)
		local moveNum = list[randomIndex]
		--print(playAnimate, animateFinishTime, animateStopPos,itemNum,minMoveNum,animationMinNum,animationMaxNum,randomIndex,moveNum)
		local tweener = LuaTweenUtils.TweenInt(1, moveNum, t, function ( val )
			local index = math.fmod(val,itemNum)
			--print(val,index,itemIndexList[index])
			self:SelectBtn(itemIndexList[index])
			if val == moveNum then
				self:KillTweener()
			end
		end)
		LuaTweenUtils.SetEase(tweener, Ease.OutQuad)
	else
		self:KillTweener()
	end
end

function LuaSectHongBaoLotteryWnd:KillTweener()
	if self.m_Tweener then
		LuaTweenUtils.DOKill(self.m_Tweener,false)
	end
end

function LuaSectHongBaoLotteryWnd:SelectBtn(index)
	if self.m_BtnList then
		for _index,btn in pairs(self.m_BtnList) do
			if btn then
				btn:SetSelected(index == _index,false)
			end
		end
	end
end

function LuaSectHongBaoLotteryWnd:ShowCountDownLabel(myLotteryDeadline)
	local t = self.m_LotteryDeadline - CServerTimeMgr.Inst.timeStamp
	self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("%d秒后自动放弃"),math.floor(t))
	self.CountDownLabel.gameObject:SetActive(t > 0)
	self.Button:GetComponent(typeof(CButton)).Enabled = t > 0
end

function LuaSectHongBaoLotteryWnd:InitItem(obj,t,index)
	local itemId = t.itemId -- 道具ID
	local itemCount = t.itemCount -- 道具数量
	local isBest = t.isBest -- 是否最佳
	local winnerId = t.winnerId -- 获得的玩家ID
	local winnerName = t.winnerName -- 获得的玩家名
	local itemData = Item_Item.GetData(itemId)
	local posData = self.m_ItemPosList[index + 1]

	local itemNameLabel = obj.transform:Find("ItemNameLabel"):GetComponent(typeof(UILabel))
	local nameLabel = obj.transform:Find("Accept/NameLabel"):GetComponent(typeof(UILabel))
	local bestRoot = obj.transform:Find("Best")
	local acceptRoot = obj.transform:Find("Accept")
	local btn = obj:GetComponent(typeof(QnTableItem))
	local item = obj.transform:Find("ItemCell")
	local iconTexture = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local qualitySprite = item.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
	local numLabel = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))

	obj.gameObject:SetActive(true)

	if itemData then
		itemNameLabel.text = itemData.Name
		iconTexture:LoadMaterial(itemData.Icon)
		qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(CItem.GetQualityType(itemId))
	end
	acceptRoot.gameObject:SetActive(winnerId ~= 0)
	nameLabel.text = winnerName
	numLabel.text = itemCount
	bestRoot.gameObject:SetActive(isBest)
	btn:SetSelected(false,false)
	if posData then
		obj.transform.localPosition = Vector3(posData.x, posData.y, 0)
	end
	table.insert(self.m_BtnList,btn)
end

function LuaSectHongBaoLotteryWnd:CancelTick()
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
	end
end

--@region UIEvent

function LuaSectHongBaoLotteryWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("SectHongBaoLotteryWnd_ReadMe")
end

function LuaSectHongBaoLotteryWnd:OnCloseButtonClick()
	local t = self.m_LotteryDeadline - CServerTimeMgr.Inst.timeStamp
	if t > 0 then
		local msg = g_MessageMgr:FormatMessage("SectHongBaoLotteryWnd_CloseWnd_Confirm")
		MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
			CUIManager.CloseUI(CLuaUIResources.SectHongBaoLotteryWnd)
		end),nil,LocalString.GetString("确认"),LocalString.GetString("取消"),false)
		return
	end
	CUIManager.CloseUI(CLuaUIResources.SectHongBaoLotteryWnd)
end

function LuaSectHongBaoLotteryWnd:OnButtonClick()
	if CClientMainPlayer.Inst then
		Gac2Gas.RequestSectItemRedPackStartMyLottery(LuaZongMenMgr.m_SectHongBaoId)
		self:CancelTick()
		self.CountDownLabel.gameObject:SetActive(false)
	end
end

--@endregion UIEvent

