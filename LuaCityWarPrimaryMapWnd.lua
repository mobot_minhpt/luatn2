require("common/common_include")

local LuaUtils = import "LuaUtils"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local NGUITools = import "NGUITools"
local UITable = import "UITable"
local CScene = import "L10.Game.CScene"
local NGUIText = import "NGUIText"
local UISprite = import "UISprite"
local QnCheckBox = import "L10.UI.QnCheckBox"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Vector3 = import "UnityEngine.Vector3"
local CUIFx = import "L10.UI.CUIFx"
local UITexture = import "UITexture"
local CCityWarMgr = import "L10.Game.CCityWarMgr"
local Extensions = import "Extensions"
local CTooltip = import "L10.UI.CTooltip"
local AlignType = import "L10.UI.CTooltip+AlignType"

CLuaCityWarPrimaryMapWnd = class()
CLuaCityWarPrimaryMapWnd.Path = "ui/citywar/LuaCityWarPrimaryMapWnd"

RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_MapRootTrans")
RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_ShowInfoRootObj")
RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_HideInfoRootObj")
RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_RegionObjTable")
RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_LastSelectedObj")
RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_MapId2OccupiedCountTable")

RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_CurrentChosenIndex")
--BiaoChe
RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_BiaoCheSettingRoot")
RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_BiaoCheSettingButton")
RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_BiaoCheBackButton")
RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_BiaoCheInfoLabel")

RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_FirstBiaoCheGo")
RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_SecondBiaoCheGo")

RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_BiaoCheInfo")
RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_IsInBiaoCheSettingStatus")
RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_HasSetBiaoCheRight")
RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_MapId2CostInfo")
RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_YaYunTabOnceOpened") --押运tab是否曾经打开过

--Battle Flag
RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_BattleFlagObj")
RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_BattleFlagRootObj")

RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_ChooseButtons")

RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_CloseButton")
RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_InfoButtonObj")

RegistClassMember(CLuaCityWarPrimaryMapWnd, "m_IsOccupyOpen")

function CLuaCityWarPrimaryMapWnd:Init( ... )
	-- CLuaGuideMgr.TryTriggerCityWarPrimaryMapGuide()
	CUIManager.CloseUI(CLuaUIResources.CityWarSecondaryMapWnd)
end

function CLuaCityWarPrimaryMapWnd:Awake( ... )

    self.m_IsOccupyOpen = false
    Gac2Gas.QueryCityWarOccupyOpen()

	self.m_ChooseButtons = {
		self.transform:Find("ButtonRoot/InfoBtn").gameObject,
		self.transform:Find("ButtonRoot/OccupyBtn").gameObject,
		self.transform:Find("ButtonRoot/EscortBtn").gameObject
	}

	UIEventListener.Get(self.transform:Find("ButtonRoot/SecondaryBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		--CUIManager.ShowUI(CLuaUIResources.CityWarSecondaryMapWnd)
		local mapId
		if CCityWarMgr.Inst:IsInCityWarScene() then
			mapId = CScene.MainScene.SceneTemplateId
		elseif CCityWarMgr.Inst:IsInMirrorWarScene() then
			mapId = CScene.MainScene.SceneTemplateId - 100526
		else
			return
		end
		CLuaCityWarMgr:OpenSecondaryMap(mapId)
	end)
	UIEventListener.Get(self.transform:Find("ButtonRoot/BackCityBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		Gac2Gas.RequestBackToOwnCity()
	end)
	local biaocheBtn = self.transform:Find("ButtonRoot/BiaoCheBtn").gameObject
	UIEventListener.Get(biaocheBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnBiaoCheMenuButtonClick(biaocheBtn)
	end)
	UIEventListener.Get(self.transform:Find("ButtonRoot/BackServerBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		Gac2Gas.RequestLeaveCityWarTerritory()
	end)
	self.m_ShowInfoRootObj = self.transform:Find("InfoRoot/ShowRoot").gameObject
	self.m_HideInfoRootObj = self.transform:Find("InfoRoot/HideRoot").gameObject
	self.m_HideInfoRootObj:SetActive(false)
	UIEventListener.Get(self.transform:Find("InfoRoot/ShowRoot/HideBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		self.m_HideInfoRootObj:SetActive(true)
		self.m_ShowInfoRootObj:SetActive(false)
	end)
	UIEventListener.Get(self.m_HideInfoRootObj).onClick = LuaUtils.VoidDelegate(function ( ... )
		self.m_ShowInfoRootObj:SetActive(true)
		self.m_HideInfoRootObj:SetActive(false)
	end)

	self.m_BattleFlagObj = self.transform:Find("MapRoot/BattleFlag").gameObject
	self.m_BattleFlagRootObj = self.transform:Find("MapRoot/BattleFlagRoot").gameObject
	self.m_BattleFlagObj:SetActive(false)

	self.m_BiaoCheSettingRoot = self.transform:Find("TopRight/BiaoChe").gameObject
	self.m_BiaoCheSettingButton = self.transform:Find("TopRight/BiaoChe/SettingButton").gameObject
	self.m_BiaoCheBackButton = self.transform:Find("TopRight/BiaoChe/BackButton").gameObject
	self.m_BiaoCheInfoLabel = self.transform:Find("TopRight/BiaoChe/Label/ValueLabel"):GetComponent(typeof(UILabel))
	self.m_FirstBiaoCheGo = self.transform:Find("MapRoot/BiaoCheInfo/FirstBiaoChe").gameObject
	self.m_SecondBiaoCheGo = self.transform:Find("MapRoot/BiaoCheInfo/SecondBiaoChe").gameObject
	self.m_CloseButton = self.transform:Find("TopRight/CloseButton").gameObject
	UIEventListener.Get(self.m_BiaoCheSettingButton).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnBiaoCheSettingButtonClick()
	end)

	UIEventListener.Get(self.m_BiaoCheBackButton).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnBiaoCheBackButtonClick()
	end)

	self.m_BiaoCheInfo = {}
	self.m_MapId2CostInfo = {}

	self:InitMap()

	self.m_InfoButtonObj = self.transform:Find("TopRight/InfoButton").gameObject
	UIEventListener.Get(self.m_InfoButtonObj).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnInfoButtonClick()
	end)
end

function CLuaCityWarPrimaryMapWnd:OnInfoButtonClick( ... )
	local str = self.m_IsOccupyOpen and g_MessageMgr:FormatMessage("KFCZ_KAIZHAN_MAP_INTERFACE_TIP") or g_MessageMgr:FormatMessage("KFCZ_XIUZHAN_MAP_INTERFACE_TIP")
	CTooltip.Show(str, self.m_InfoButtonObj.transform, AlignType.Bottom)
end

function CLuaCityWarPrimaryMapWnd:InitMap( ... )
	self.m_MapRootTrans = self.transform:Find("MapRoot")
	local templateObj = self.transform:Find("MapRoot/Center").gameObject
	local nameTable = {"ChiTian", "QingTian", "XuanTian", "SuTian"}
	local rootTransTable = {}
	self.m_RegionObjTable = {}
	for i = 1, #nameTable do
		table.insert(rootTransTable, self.transform:Find("MapRoot/"..nameTable[i]):GetComponent(typeof(UITable)))
	end

	local mapIdTable = {}
	CityWar_Map.ForeachKey(function(key)
		table.insert(mapIdTable, key)
	end)
	table.sort(mapIdTable)

	for _, mapId in ipairs(mapIdTable) do
		local obj
		local map = CityWar_Map.GetData(mapId)
		if map.Zone == 0 then
			self.m_RegionObjTable[mapId] = templateObj
			self:InitRegionBasicInfo(templateObj, mapId)
			obj = templateObj
		else
			obj = NGUITools.AddChild(rootTransTable[map.Zone].gameObject, templateObj)
			self:InitRegionBasicInfo(obj, mapId)
			self.m_RegionObjTable[mapId] = obj
		end

		obj.transform:Find("Occupy/Flags/ProtectedFlag").gameObject:SetActive(false)
		obj.transform:Find("CommonFlags/GuildFlag").gameObject:SetActive(false)
		obj.transform:Find("CommonFlags/LocationFlag").gameObject:SetActive(false)
		obj.transform:Find("Occupy/Color").gameObject:SetActive(false)
	end

	for i = 1, 4 do
		rootTransTable[i]:Reposition()
	end

	self:InitChooseBtn()
	self.m_YaYunTabOnceOpened = false
	self:ChooseBtn(CLuaCityWarMgr.PrimaryMapDefaultIndex or EnumCityWarPrimaryMapWndTabIndex.eOccupy)
end

function CLuaCityWarPrimaryMapWnd:InitChooseBtn( ... )
	for i,v in ipairs(self.m_ChooseButtons) do
		UIEventListener.Get(v).onClick = LuaUtils.VoidDelegate(function ( ... )
			self:ChooseBtn(i)
		end)
	end
end

function CLuaCityWarPrimaryMapWnd:ChooseBtn(index)

	if index == EnumCityWarPrimaryMapWndTabIndex.eYaYun then
		self.m_YaYunTabOnceOpened = true
	end

	local nameTable = {"Info", "Occupy", "Escort"}
	for k, v in pairs(self.m_RegionObjTable) do
		for i = 1, #nameTable do
			v.transform:Find(nameTable[i]).gameObject:SetActive(index == i)
		end
	end

	for i,v in ipairs(self.m_ChooseButtons) do
		local selectedObj = v.transform:Find("Selected").gameObject
		selectedObj:SetActive(index==i)
		v:GetComponent(typeof(UITexture)).alpha = index == i and 1.0 or 0.5
	end

	self:CancelSelection()

	self.m_CurrentChosenIndex = index

	if self:IsInBiaoCheView() then
		Gac2Gas.QuerySetBiaoChePower()--查询是否有权限
	end

	self:CheckIfShowBiaoCheSetting()

	-- Show Battle Flag
	self.m_BattleFlagRootObj:SetActive(index == 2)
end

function CLuaCityWarPrimaryMapWnd:InitRegionBasicInfo(obj, mapId)
	local map = CityWar_Map.GetData(mapId)
	if not map then return end
	obj.transform:Find("Occupy/Level"):GetComponent(typeof(UILabel)).text = "lv."..map.Level
	self:SetRegionSelected(obj, false)
	UIEventListener.Get(obj).onClick = LuaUtils.VoidDelegate(function ( go )
		self:OnRegionClick(go, mapId)
	end)
end

function CLuaCityWarPrimaryMapWnd:SetRegionSelected(obj, bSelected)
	if not obj then return end
	obj.transform:Find("Selected").gameObject:SetActive(bSelected)
end

function CLuaCityWarPrimaryMapWnd:OnRegionClick(go, mapId)

	if self.m_IsInBiaoCheSettingStatus then
		self:CheckAndSetBiaoChe(go, mapId)
		return
	end

	self:SetRegionSelected(self.m_LastSelectedObj, false)
	self:SetRegionSelected(go, true)
	self.m_LastSelectedObj = go
	CLuaCityWarMgr.MapInfoTemplateId = mapId
	CLuaCityWarMgr.MapInfoOccupiedCount = self.m_MapId2OccupiedCountTable and self.m_MapId2OccupiedCountTable[mapId] or 0
	Gac2Gas.QueryTerritoryCityTips(mapId, CityWar_Map.GetData(mapId).MajorCity)
end

function CLuaCityWarPrimaryMapWnd:CancelSelection()
	--取消选中状态
	for k, v in pairs(self.m_RegionObjTable) do
		self:SetRegionSelected(v, false)
	end
end

function CLuaCityWarPrimaryMapWnd:QueryTerritoryMapsOverviewResult(guildMapId, guildTemplateId, serverInfoUD, overViewUD, biaoCheInfoUd)
	CLuaCityWarMgr.MapInfoMyGuildMapId = guildMapId
	CLuaCityWarMgr.MapInfoMyGuildCityId = guildTemplateId

	local myMapId = 0
    if CScene.MainScene then
        myMapId = CScene.MainScene.SceneTemplateId
    end

	local nameTable = {"UnOccupied"}
	local serverNameTable = {LocalString.GetString("主城未被占领")}
	local serverIdTable = {0}
	local serverCountTable = {37}
	local colorTbl = {"EE9E65", "8FB7DD", "E1E4E0", "B27E77", "c7a2d7", "a6cf77", "82dbc9", "ece98f"}
	local colorTable = {0}
	for i = 1, #colorTbl do
		table.insert(colorTable, NGUIText.ParseColor24(colorTbl[i], 0))
	end

	local list = MsgPackImpl.unpack(serverInfoUD)
	if not list then return end
	local index = 2
	for i = 0, list.Count - 1, 2 do
		--serverIdTable[index] = list[i]
		--serverNameTable[index] = list[i + 1]
		--index = index + 1
		table.insert(serverIdTable, list[i])
		table.insert(serverNameTable, list[i + 1])
		table.insert(serverCountTable, 0)
	end

	--镖车
	self:ParseBiaoCheInfo(biaoCheInfoUd, true)

	local warMapTbl = {}

	self.m_MapId2OccupiedCountTable = {}
	local dict = MsgPackImpl.unpack(overViewUD)
	CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function(key,val)
		local occupiedCount = val[0]
		local serverId = val[1]
		local protected = val[2]
		local warMapId = val[3]

		local serverIndex = 2
		--Find server
		for i = 2, #serverIdTable do
			if serverId == serverIdTable[i] then
				serverCountTable[i] = serverCountTable[i] + 1
				serverIndex = i
				break
			end
		end
		key = tonumber(key)
		self.m_MapId2OccupiedCountTable[key] = occupiedCount
		if self.m_RegionObjTable[key] then
			local obj = self.m_RegionObjTable[key]
			obj.transform:Find("Occupy/Flags/ProtectedFlag").gameObject:SetActive(protected > 0)
			local guildFlagObj = obj.transform:Find("CommonFlags/GuildFlag").gameObject
			guildFlagObj:SetActive(key == guildMapId)
			if key == guildMapId and not CLuaCityWarMgr:IsMajorCity(guildTemplateId) then
				guildFlagObj:GetComponent(typeof(UISprite)).spriteName = "citywarprimarymapwnd_chengchiweizhi_02"
			end
			obj.transform:Find("Info/OccupiedCount"):GetComponent(typeof(UILabel)).text = occupiedCount.."/9"
			obj.transform:Find("CommonFlags/LocationFlag").gameObject:SetActive(myMapId == key)
			local colorObj = obj.transform:Find("Occupy/Color").gameObject
			colorObj:SetActive(serverId > 0)
			colorObj:GetComponent(typeof(UISprite)).color = colorTable[serverIndex]
		end

		-- Add Battle Flag
		if warMapId > 0 and not warMapTbl[key] then
			local obj = NGUITools.AddChild(self.m_BattleFlagRootObj, self.m_BattleFlagObj)
			obj:SetActive(true)
			local map1Pos = self.m_MapRootTrans:InverseTransformPoint(self.m_RegionObjTable[key].transform.position)
			local map2Pos = self.m_MapRootTrans:InverseTransformPoint(self.m_RegionObjTable[warMapId].transform.position)
			obj.transform.localPosition = Vector3((map1Pos.x + map2Pos.x) / 2, (map1Pos.y + map2Pos.y) / 2, 0)
			obj:GetComponent(typeof(CUIFx)):LoadFx("fx/ui/prefab/UI_duizhanzhuangtai.prefab")

			warMapTbl[key] = true
			warMapTbl[warMapId] = true
		end
	end))

	for i = 2, #serverCountTable do
		serverCountTable[1] = serverCountTable[1] - serverCountTable[i]
	end

	local rootObj = self.transform:Find("InfoRoot/ShowRoot/Scrollview/Table/Table").gameObject
	Extensions.RemoveAllChildren(rootObj.transform)
	local templateObj = self.transform:Find("InfoRoot/ShowRoot/Scrollview/Table/ServerTemplate").gameObject
	templateObj:SetActive(false)
	for i = 2, #serverIdTable do
		local obj = NGUITools.AddChild(rootObj, templateObj)
		obj:SetActive(true)
		obj:GetComponent(typeof(UISprite)).color = colorTable[i]
		obj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = serverNameTable[i]
		obj.transform:Find("Count"):GetComponent(typeof(UILabel)).text = serverCountTable[i]
	end
	rootObj:GetComponent(typeof(UITable)):Reposition()
	self.transform:Find("InfoRoot/ShowRoot/Scrollview/Table/UnOccupied"):Find("Count"):GetComponent(typeof(UILabel)).text = serverCountTable[1]
end

function CLuaCityWarPrimaryMapWnd:QueryCityInfoResult( ... )
	CUIManager.ShowUI(CLuaUIResources.MajorCityMapInfoTip)
end

function CLuaCityWarPrimaryMapWnd:UpdateGuildBiaoCheInfo(actionType, biaoCheInfoUd)
	self:ParseBiaoCheInfo(biaoCheInfoUd, false)
end

function CLuaCityWarPrimaryMapWnd:SyncSetBiaoChePower(hasRight)
	self.m_HasSetBiaoCheRight= hasRight or false
	self:CheckIfShowBiaoCheSetting()
end

function CLuaCityWarPrimaryMapWnd:QueryCityWarOccupyOpenResult( bOpen )
	self.m_IsOccupyOpen = bOpen
	self:OnInfoButtonClick()
end

function CLuaCityWarPrimaryMapWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("QueryTerritoryMapsOverviewResult", self, "QueryTerritoryMapsOverviewResult")
	g_ScriptEvent:AddListener("QueryCityInfoResult", self, "QueryCityInfoResult")
	g_ScriptEvent:AddListener("UpdateGuildBiaoCheInfo", self, "UpdateGuildBiaoCheInfo")
	g_ScriptEvent:AddListener("SyncSetBiaoChePower", self, "SyncSetBiaoChePower")
	g_ScriptEvent:AddListener("QuerySetGuildBiaoCheCostAllResult", self, "QuerySetGuildBiaoCheCostAllResult")
	g_ScriptEvent:AddListener("QueryCityWarOccupyOpenResult", self, "QueryCityWarOccupyOpenResult")
	Gac2Gas.QueryTerritoryMapsOverview()
end

function CLuaCityWarPrimaryMapWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("QueryTerritoryMapsOverviewResult", self, "QueryTerritoryMapsOverviewResult")
	g_ScriptEvent:RemoveListener("QueryCityInfoResult", self, "QueryCityInfoResult")
	g_ScriptEvent:RemoveListener("UpdateGuildBiaoCheInfo", self, "UpdateGuildBiaoCheInfo")
	g_ScriptEvent:RemoveListener("SyncSetBiaoChePower", self, "SyncSetBiaoChePower")
	g_ScriptEvent:RemoveListener("QuerySetGuildBiaoCheCostAllResult", self, "QuerySetGuildBiaoCheCostAllResult")
	g_ScriptEvent:RemoveListener("QueryCityWarOccupyOpenResult", self, "QueryCityWarOccupyOpenResult")
end

---- begin 设置镖车

function CLuaCityWarPrimaryMapWnd:ParseBiaoCheInfo(biaoCheInfoUd, checkBiaoCheTabOnceOpen)
	if not biaoCheInfoUd then return end
	local data = MsgPackImpl.unpack(biaoCheInfoUd)
	if data and data.Count >= 8 then
		self.m_BiaoCheInfo.biaoCheFMapId = tonumber(data[0])
		self.m_BiaoCheInfo.biaoCheSMapId = tonumber(data[1])
		self.m_BiaoCheInfo.biaoCheFProgress = tonumber(data[2])
		self.m_BiaoCheInfo.biaoCheSProgress = tonumber(data[3])
		self.m_BiaoCheInfo.biaoCheFStatus = tonumber(data[4])
		self.m_BiaoCheInfo.biaoCheSStatus = tonumber(data[5])
		self.m_BiaoCheInfo.biaoCheCloseTime = tonumber(data[6])
		if checkBiaoCheTabOnceOpen then
			local val = tonumber(data[7]) --语义是玩法是否处于押运中,1处于,0不处于
			if val > 0 and not self.m_YaYunTabOnceOpened then
				self:ChooseBtn(EnumCityWarPrimaryMapWndTabIndex.eYaYun)
			end
			self.m_YaYunTabOnceOpened = true -- 无论如何都要把变量置为true
		end
	end

	self:CheckIfShowBiaoCheSetting()
end

function CLuaCityWarPrimaryMapWnd:IsInBiaoCheView()
	return self.m_CurrentChosenIndex and self.m_CurrentChosenIndex == EnumCityWarPrimaryMapWndTabIndex.eYaYun
end

function CLuaCityWarPrimaryMapWnd:CheckIfShowBiaoCheSetting()
	if not self.m_BiaoCheSettingRoot then return end
	if not self:IsInBiaoCheView() then
		self.m_IsInBiaoCheSettingStatus = false
		self.m_BiaoCheSettingRoot:SetActive(false)
		self:ShowBiaoCheInfo()
		--self.m_CloseButton:SetActive(true)
		return
	end

	if self.m_HasSetBiaoCheRight then
		self.m_BiaoCheSettingRoot:SetActive(true)
		if self.m_IsInBiaoCheSettingStatus then
			self.m_BiaoCheBackButton:SetActive(true)
			self.m_BiaoCheSettingButton:SetActive(false)
		else
			self.m_BiaoCheBackButton:SetActive(false)
			self.m_BiaoCheSettingButton:SetActive(true)
		end
		--self.m_CloseButton:SetActive(not self.m_IsInBiaoCheSettingStatus)
	else
		self.m_IsInBiaoCheSettingStatus = false
		self.m_BiaoCheSettingRoot:SetActive(false)
		--self.m_CloseButton:SetActive(true)
	end

	if self.m_BiaoCheSettingRoot.activeSelf then

		local totalBiaoCheCount = 2 --目前看是定死的
		local placedBiaoCheCount = 0
		if self.m_BiaoCheInfo.biaoCheFMapId and CityWar_Map.Exists(self.m_BiaoCheInfo.biaoCheFMapId) then
			placedBiaoCheCount = placedBiaoCheCount + 1
		end
		if self.m_BiaoCheInfo.biaoCheSMapId and CityWar_Map.Exists(self.m_BiaoCheInfo.biaoCheSMapId) then
			placedBiaoCheCount = placedBiaoCheCount + 1
		end

		self.m_BiaoCheInfoLabel.text = SafeStringFormat3("%d/%d", placedBiaoCheCount, totalBiaoCheCount)
	end

	self:ShowBiaoCheInfo()
end

function CLuaCityWarPrimaryMapWnd:SetBiaoCheAppearance(biaoCheGo, mapId, status, progress)
	if not biaoCheGo then return end
	if not status or not progress then return end
	local prepareGo = biaoCheGo.transform:Find("Prepare").gameObject
	local runGo = biaoCheGo.transform:Find("Run").gameObject
	local label = biaoCheGo.transform:Find("Label"):GetComponent(typeof(UILabel))

	CUICommonDef.SetGrey(prepareGo, (status == EnumBiaoCheStatus.eYaYunEnd)) --是否置为灰色

	if status == EnumBiaoCheStatus.eYaYun then
		-- 处于押镖状态
		prepareGo:SetActive(false)
		runGo:SetActive(true)
		runGo.transform:Find("FX"):GetComponent(typeof(CUIFx)):LoadFx("fx/ui/prefab/UI_duizhanzhuangtai_huoche.prefab")
		label.text = LocalString.GetString("押运中")
		label.color = EnumBiaoCheStatusTextColor.eYaYun
	else
		--处于非押镖状态
		prepareGo:SetActive(true)
		runGo:SetActive(false)
		if status == EnumBiaoCheStatus.eYaYunEnd then
			label.text = LocalString.GetString("准备中")
			label.color = EnumBiaoCheStatusTextColor.eYaYunEnd
		else
			label.text = LocalString.GetString("装填中")
			label.color = EnumBiaoCheStatusTextColor.eSubmitting
		end
	end
	if self.m_IsInBiaoCheSettingStatus then
		label.gameObject:SetActive(false)
	else
		label.gameObject:SetActive(true)
	end
end

function CLuaCityWarPrimaryMapWnd:ShowBiaoCheInfo()
	if not self:IsInBiaoCheView() then
		self.m_FirstBiaoCheGo:SetActive(false)
		self.m_SecondBiaoCheGo:SetActive(false)
		return
	end

	local firstBiaoCheMapId = 0
	local secondBiaoCheMapId = 0
	if self.m_BiaoCheInfo.biaoCheFMapId and CityWar_Map.Exists(self.m_BiaoCheInfo.biaoCheFMapId) then
		--设置第一辆镖车
		firstBiaoCheMapId = self.m_BiaoCheInfo.biaoCheFMapId
		self.m_FirstBiaoCheGo:SetActive(true)
		self:SetBiaoCheAppearance(self.m_FirstBiaoCheGo, firstBiaoCheMapId, self.m_BiaoCheInfo.biaoCheFStatus, self.m_BiaoCheInfo.biaoCheFProgress)
	else
		self.m_FirstBiaoCheGo:SetActive(false)
	end

	if self.m_BiaoCheInfo.biaoCheSMapId and CityWar_Map.Exists(self.m_BiaoCheInfo.biaoCheSMapId) then
		--设置第二辆镖车
		secondBiaoCheMapId = self.m_BiaoCheInfo.biaoCheSMapId
		self.m_SecondBiaoCheGo:SetActive(true)
		self:SetBiaoCheAppearance(self.m_SecondBiaoCheGo, secondBiaoCheMapId, self.m_BiaoCheInfo.biaoCheSStatus, self.m_BiaoCheInfo.biaoCheSProgress)
	else
		self.m_SecondBiaoCheGo:SetActive(false)
	end

	--放置镖车到指定位置
	for k, v in pairs(self.m_RegionObjTable) do
		local checkbox = v.transform:Find("Escort/Checkbox"):GetComponent(typeof(QnCheckBox))
		local lbl = v.transform:Find("Escort/Label"):GetComponent(typeof(UILabel))
		if self.m_IsInBiaoCheSettingStatus then
			checkbox.gameObject:SetActive(self:CanSetBiaoCheAt(k))
			checkbox.Selected = (k==firstBiaoCheMapId or k==secondBiaoCheMapId)
			local disabled = false
			if k==firstBiaoCheMapId then
				disabled = (self.m_BiaoCheInfo.biaoCheFStatus >= EnumBiaoCheStatus.eYaYun)
			elseif k==secondBiaoCheMapId then
				disabled = (self.m_BiaoCheInfo.biaoCheSStatus >= EnumBiaoCheStatus.eYaYun)
			end
			CUICommonDef.SetActive(checkbox.gameObject, not disabled, false)
			lbl.text = self:GetMapCostText(k)
		else
			checkbox.gameObject:SetActive(false)
			lbl.text = ""
		end

		if k == firstBiaoCheMapId then
			self.m_FirstBiaoCheGo.transform.position = v.transform.position
		elseif k == secondBiaoCheMapId then
			self.m_SecondBiaoCheGo.transform.position = v.transform.position
		end
	end
end

function CLuaCityWarPrimaryMapWnd:ShowSetBiaoCheCostInfo()
	if not self:IsInBiaoCheView() then return end

	for k, v in pairs(self.m_RegionObjTable) do
		local lbl = v.transform:Find("Escort/Label"):GetComponent(typeof(UILabel))
		if self.m_IsInBiaoCheSettingStatus and self:CanSetBiaoCheAt(k) then
			lbl.text = self:GetMapCostText(k)
		else
			lbl.text = ""
		end
	end

end

function CLuaCityWarPrimaryMapWnd:GetMapCostText(mapId)
	if mapId and self.m_MapId2CostInfo and self.m_MapId2CostInfo[mapId] then
		local val = self.m_MapId2CostInfo[mapId]
		if val<10000 then return tostring(val) end
		return tostring(math.floor(val/10000)).."w"
	end
end

function CLuaCityWarPrimaryMapWnd:CanSetBiaoCheAt(mapId)
	--检查当前地图上是否可以放置镖车，用来排除帮会所在zone，以及金元洞天
	local mapData = CityWar_Map.GetData(mapId)
	if not mapData then return false end
	--如果还没有占领城池，也不能放置镖车
	local myGuildMapData = CityWar_Map.GetData(CLuaCityWarMgr.MapInfoMyGuildMapId or 0)
	if not myGuildMapData then return false end

	local zone = mapData.Zone
	if zone <=0 then return false end --金元洞天
	local myGuildZone = myGuildMapData.Zone
	return zone ~= myGuildZone
end

function CLuaCityWarPrimaryMapWnd:OnBiaoCheSettingButtonClick()
	self.m_IsInBiaoCheSettingStatus = true
	self:CancelSelection()
	self.m_MapId2CostInfo = {}
	Gac2Gas.QuerySetGuildBiaoCheCostAll(CLuaCityWarMgr.MapInfoMyGuildMapId or 0) --查询每个地图上放置镖车的帮会资金消耗
	self:CheckIfShowBiaoCheSetting()
end

function CLuaCityWarPrimaryMapWnd:OnBiaoCheBackButtonClick()
	self.m_IsInBiaoCheSettingStatus = false
	self:CheckIfShowBiaoCheSetting()
end

function CLuaCityWarPrimaryMapWnd:OnBiaoCheMenuButtonClick(go)
	CLuaCityWarMgr.ShowBiaoCheMenu(go.transform.position,
		go:GetComponent(typeof(UIWidget)).width,
		go:GetComponent(typeof(UIWidget)).height,
		self.m_BiaoCheInfo)
end

function CLuaCityWarPrimaryMapWnd:CheckAndSetBiaoChe(go,mapId)

	if not self:CanSetBiaoCheAt(mapId) then return end

	local totalBiaoCheCount = 2
	local placedBiaoCheCount = 0
	local firstBiaoCheMapId = 0
	local secondBiaoCheMapId = 0
	if self.m_BiaoCheInfo.biaoCheFMapId and CityWar_Map.Exists(self.m_BiaoCheInfo.biaoCheFMapId) then
		placedBiaoCheCount = placedBiaoCheCount + 1
		firstBiaoCheMapId = self.m_BiaoCheInfo.biaoCheFMapId
	end
	if self.m_BiaoCheInfo.biaoCheSMapId and CityWar_Map.Exists(self.m_BiaoCheInfo.biaoCheSMapId) then
		placedBiaoCheCount = placedBiaoCheCount + 1
		secondBiaoCheMapId = self.m_BiaoCheInfo.biaoCheSMapId
	end

	if mapId == firstBiaoCheMapId then
		--当前位置放置了第一辆镖车
		if self.m_BiaoCheInfo.biaoCheFStatus >= EnumBiaoCheStatus.eYaYun then
			--押运状态或者押运结束，不能操作
		else
			MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("YAYUN_RESET_BIAOCHE_CONFIRM"), DelegateFactory.Action(function ()
				Gac2Gas.RequestResetBiaoCheMap(mapId, secondBiaoCheMapId)
    		end), nil, nil, nil, false)
		end

	elseif mapId == secondBiaoCheMapId then
		--当前位置放置了第二辆镖车
		if self.m_BiaoCheInfo.biaoCheSStatus >= EnumBiaoCheStatus.eYaYun then
			--押运状态或者押运结束，不能操作
		else
			MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("YAYUN_RESET_BIAOCHE_CONFIRM"), DelegateFactory.Action(function ()
				Gac2Gas.RequestResetBiaoCheMap(mapId, firstBiaoCheMapId)
    		end), nil, nil, nil, false)
		end

	elseif placedBiaoCheCount == 2 then
		g_MessageMgr:ShowMessage("YAYUN_SET_BIAOCHE_NO_LEFT_BIAOCHE")
	else
		--可以放置镖车
		--去请求一下帮会资金再请求放置
		Gac2Gas.QueryGuildSilveForBiaoChe(mapId, CLuaCityWarMgr.MapInfoMyGuildMapId or 0)
	end
end

function CLuaCityWarPrimaryMapWnd:QuerySetGuildBiaoCheCostAllResult(mapId2CostTbl)
	self.m_MapId2CostInfo = mapId2CostTbl
	self:ShowSetBiaoCheCostInfo()
end

---- end 设置镖车

function CLuaCityWarPrimaryMapWnd:GetGuideGo(methodName)
    if methodName=="GetOccupyButton" then
		return self.m_ChooseButtons[2]
	elseif methodName=="GetLingTuButton" then
		return self.m_ChooseButtons[1]
	elseif methodName=="GetYaBiaoButton" then
		return self.m_ChooseButtons[3]
	elseif methodName=="GetRandomRect" then
		local mapRoot = self.transform:Find("MapRoot")
		local nodes = {
			mapRoot:Find("SuTian"),
			mapRoot:Find("QingTian"),
			mapRoot:Find("ChiTian"),
			mapRoot:Find("XuanTian"),
		}
		local node = nodes[math.random(4)]
		return node:GetChild(math.random( 9 )-1).gameObject
	end
    return nil
end
