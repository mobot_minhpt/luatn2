local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CMainCamera = import "L10.Engine.CMainCamera"
local Vector3 = import "UnityEngine.Vector3"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"

LuaZongMenTopRightWnd = class()

RegistChildComponent(LuaZongMenTopRightWnd,"m_RightBtn","RightBtn", GameObject)
RegistChildComponent(LuaZongMenTopRightWnd,"m_ZongMenButton","ZongMenButton", GameObject)
RegistChildComponent(LuaZongMenTopRightWnd,"m_SoulCoreButton","SoulCoreButton", GameObject)
RegistChildComponent(LuaZongMenTopRightWnd,"m_ZongMenButtonAlert","ZongMenButtonAlert", GameObject)

RegistClassMember(LuaZongMenTopRightWnd, "m_SoulCoreRedPoint")
RegistClassMember(LuaZongMenTopRightWnd, "m_SoulCoreHeadInfo")
RegistClassMember(LuaZongMenTopRightWnd, "m_SoulCoreName")
RegistClassMember(LuaZongMenTopRightWnd, "m_CachedPos")
RegistClassMember(LuaZongMenTopRightWnd, "m_ViewPos")
RegistClassMember(LuaZongMenTopRightWnd, "m_ScreenPos")
RegistClassMember(LuaZongMenTopRightWnd, "m_TopWorldPos")
RegistClassMember(LuaZongMenTopRightWnd, "m_PracticeButton")
RegistClassMember(LuaZongMenTopRightWnd, "m_Grid")

function LuaZongMenTopRightWnd:Awake()
    self.m_SoulCoreHeadInfo = self.transform:Find("SelfSoulCoreHeadInfo").gameObject
    self.m_SoulCoreName     = self.transform:Find("SelfSoulCoreHeadInfo/NameBoard/NameBoard"):GetComponent(typeof(UILabel))
    self.m_SoulCoreRedPoint = self.m_SoulCoreButton.transform:Find("redpoint").gameObject
    self.m_PracticeButton   = self.transform:Find("Anchor/Grid/PracticeButton").gameObject
    self.m_Grid             = self.transform:Find("Anchor/Grid"):GetComponent(typeof(UIGrid))
    -- body
    self.m_PracticeButton:SetActive(false)
    self.m_SoulCoreRedPoint:SetActive(false)
end

function LuaZongMenTopRightWnd:Init()
    self.m_ZongMenButtonAlert.gameObject:SetActive(false)
    self.m_CachedPos = self.m_SoulCoreHeadInfo.transform.position
    UIEventListener.Get(self.m_RightBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self.m_RightBtn.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
    UIEventListener.Get(self.m_ZongMenButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self.m_ZongMenButtonAlert.gameObject:SetActive(false)
        CUIManager.ShowUI(CLuaUIResources.ZongMenMainWnd)
    end)
    UIEventListener.Get(self.m_SoulCoreButton).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.ShowUI(CLuaUIResources.SoulCoreWnd)
    end)
    UIEventListener.Get(self.m_PracticeButton).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.ShowUI(CLuaUIResources.ZongMenPracticeRuleAndRankWnd)
    end)
    self:CheckSoulCore()
    self:CheckSoulCoreRedPoint()
    self:TryShowMainPlayerSoulCoreHeadInfo()
    self:QuerySectBoomPlayStatus()
    if not LuaZongMenMgr.m_ZongMenProp then
        if LuaZongMenMgr.m_IsOpen then
            Gac2Gas.GetSectXinWuInfo()
        end
    end
    if CClientMainPlayer.Inst then
        Gac2Gas.QuerySectRedAlert(CClientMainPlayer.Inst.BasicProp.SectId)
    end
end

function LuaZongMenTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("UpdateMainPlayerSoulCoreLevel", self, "OnUpdateMainPlayerSoulCoreLevel")
    g_ScriptEvent:AddListener("MainPlayerSoulCoreCreate", self, "TryShowMainPlayerSoulCoreHeadInfo")
    g_ScriptEvent:AddListener("OnOpenSoulCoreWnd", self, "OnOpenSoulCoreWnd")
    g_ScriptEvent:AddListener("UpdateMainPlayerSoulCoreMana", self, "CheckSoulCoreRedPoint")
    g_ScriptEvent:AddListener("NotifySectBoomPlayStart", self, "OnNotifySectBoomPlayStart")
    g_ScriptEvent:AddListener("SyncSectXiuxingPlayStatus_Ite", self, "OnSyncSectXiuxingPlayStatus_Ite")
    g_ScriptEvent:AddListener("SceneSectXiuxingPlayEnd", self, "OnSceneSectXiuxingPlayEnd")
    g_ScriptEvent:AddListener("OnSyncSectRedAlert", self, "OnSyncSectRedAlert")
end

function LuaZongMenTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("UpdateMainPlayerSoulCoreLevel", self, "OnUpdateMainPlayerSoulCoreLevel")
    g_ScriptEvent:RemoveListener("MainPlayerSoulCoreCreate", self, "TryShowMainPlayerSoulCoreHeadInfo")
    g_ScriptEvent:RemoveListener("OnOpenSoulCoreWnd", self, "OnOpenSoulCoreWnd")
    g_ScriptEvent:RemoveListener("UpdateMainPlayerSoulCoreMana", self, "CheckSoulCoreRedPoint")
    g_ScriptEvent:RemoveListener("NotifySectBoomPlayStart", self, "OnNotifySectBoomPlayStart")
    g_ScriptEvent:RemoveListener("SyncSectXiuxingPlayStatus_Ite", self, "OnSyncSectXiuxingPlayStatus_Ite")
    g_ScriptEvent:RemoveListener("SceneSectXiuxingPlayEnd", self, "OnSceneSectXiuxingPlayEnd")
    g_ScriptEvent:RemoveListener("OnSyncSectRedAlert", self, "OnSyncSectRedAlert")

    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end
end

function LuaZongMenTopRightWnd:OnSyncSectRedAlert(sectId, bHasRedAlert)
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId == sectId then
        self.m_ZongMenButtonAlert.gameObject:SetActive(bHasRedAlert)
    end
end

function LuaZongMenTopRightWnd:OnNotifySectBoomPlayStart()
    self:QuerySectBoomPlayStatus()
end

function LuaZongMenTopRightWnd:QuerySectBoomPlayStatus()
    if CClientMainPlayer.Inst then
        Gac2Gas.RequestSectXiuxingPlayStatus_Ite(CClientMainPlayer.Inst.BasicProp.SectId)
    end
    -- Gas2Gac.SyncSectXiuxingPlayStatus_Ite
end

function LuaZongMenTopRightWnd:OnSyncSectXiuxingPlayStatus_Ite(sectId, playStage, bJoined)
    if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.BasicProp.SectId ~= sectId then
        return
    end
    self.m_PracticeButton:SetActive(playStage ~= EnumSectXiuxingPlayStage.eEnd)
    self.m_Grid:Reposition()
    if bJoined == false and playStage == EnumSectXiuxingPlayStage.eSignup then
        if LuaZongMenMgr.m_SectXiuxingPlayStage ~= EnumSectXiuxingPlayStage.eSignup then
            CGuideMgr.Inst:StartGuide(EnumGuideKey.ZongMenPracticeGuide, 0)
        end
    end
    LuaZongMenMgr.m_SectXiuxingPlayStage = playStage
end

function LuaZongMenTopRightWnd:OnSceneSectXiuxingPlayEnd()
    self.m_PracticeButton:SetActive(false)
    self.m_Grid:Reposition()
end

function LuaZongMenTopRightWnd:GetGuideGo(methodName)
    if methodName == "GetRightBtn" then
        return self.m_RightBtn
    elseif methodName == "GetZongMenButton" then
        return self.m_ZongMenButton
    elseif methodName == "GetZongMenPracticeButton" then
        return self.m_PracticeButton
    end
end

function LuaZongMenTopRightWnd:OnHideTopAndRightTipWnd()
    self.m_RightBtn.transform.localEulerAngles = Vector3(0, 0, 180)
end

function LuaZongMenTopRightWnd:CheckSoulCore()
    self:OnUpdateMainPlayerSoulCoreLevel()
end

function LuaZongMenTopRightWnd:CheckSoulCoreRedPoint()
    if CClientMainPlayer.Inst == nil or LuaZongMenMgr.m_LastOpenSoulCoreWndLv == nil then
        return false
    end
    local mainPlayer = CClientMainPlayer.Inst
    local soulCoreLevel = mainPlayer.SkillProp.SoulCore.Level
    if soulCoreLevel > 0 and soulCoreLevel ~= LuaZongMenMgr.m_LastOpenSoulCoreWndLv then
        self.m_SoulCoreRedPoint:SetActive(LuaZongMenMgr:IsMainPlayerSoulCoreUpgradeable())
    end
end

function LuaZongMenTopRightWnd:OnUpdateMainPlayerSoulCoreLevel()
    local soulCoreLv = (((CClientMainPlayer.Inst or {}).SkillProp or {}).SoulCore or {}).Level or 0
    local showSoulCore = soulCoreLv > 0

    self.m_SoulCoreButton:SetActive(showSoulCore)
    self.m_Grid:Reposition()
end

function LuaZongMenTopRightWnd:OnOpenSoulCoreWnd()
    self.m_SoulCoreRedPoint:SetActive(false)
end

function LuaZongMenTopRightWnd:TryShowMainPlayerSoulCoreHeadInfo()
    --判断一下场景
    if not LuaZongMenMgr:IsMySectScene() or LuaZongMenMgr.m_SelfSoulCoreGo == nil then
        self.m_SoulCoreHeadInfo:SetActive(false)
        self.m_SoulCoreName.text = ""
        return
    end

    self.m_SoulCoreHeadInfo:SetActive(true)
    self.m_SoulCoreName.text = SafeStringFormat3(LocalString.GetString("我的灵核"))
end

function LuaZongMenTopRightWnd:Update()
    if LuaZongMenMgr.m_SelfSoulCoreGo and LuaZongMenMgr:IsMySectScene() and self.m_SoulCoreHeadInfo.activeSelf and not CommonDefs.IsUnityObjectNull(LuaZongMenMgr.m_SelfSoulCoreGo) then
        local vec3 = LuaZongMenMgr.m_SelfSoulCoreGo.transform.position
        vec3.y = vec3.y + 1.2
        self.m_ViewPos = CMainCamera.Main:WorldToViewportPoint(vec3)
        
        if self.m_ViewPos and self.m_ViewPos .z > 0 and self.m_ViewPos .x > 0 and self.m_ViewPos .x < 1 and self.m_ViewPos .y > 0 and self.m_ViewPos .y < 1 then
            self.m_ScreenPos = CMainCamera.Main:WorldToScreenPoint(vec3)
        else  
            self.m_ScreenPos = CommonDefs.op_Multiply_Vector3_Single(Vector3.up, 3000)
        end

        if self.m_ScreenPos ~= nil then
            self.m_ScreenPos.z = 0
            self.m_TopWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_ScreenPos)

            if self.m_CachedPos.x ~= self.m_TopWorldPos.x or self.m_CachedPos.y ~= self.m_TopWorldPos.y then
                self.m_SoulCoreHeadInfo.transform.position = self.m_TopWorldPos
                self.m_CachedPos = self.m_TopWorldPos
            end
        end
    end
end