local CMainCamera = import "L10.Engine.CMainCamera"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local Color = import "UnityEngine.Color"
local Physics = import "UnityEngine.Physics"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local MeshFilter = import "UnityEngine.MeshFilter"
local MeshRenderer = import "UnityEngine.MeshRenderer"
local CUITexture = import "L10.UI.CUITexture"
local UITexture = import "UITexture"
local UITable = import "UITable"
local UIScrollView = import "UIScrollView"
local Extensions = import "Extensions"
local MessageWndManager = import "L10.UI.MessageWndManager"
local RenderTexture = import "UnityEngine.RenderTexture"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local TextureWrapMode = import "UnityEngine.TextureWrapMode"
local FilterMode = import "UnityEngine.FilterMode"
local Camera = import "UnityEngine.Camera"
local GameObject = import "UnityEngine.GameObject"
local CameraClearFlags = import "UnityEngine.CameraClearFlags"
local LayerDefine = import "L10.Engine.LayerDefine"
local Quaternion = import "UnityEngine.Quaternion"
local CPreDrawMgr = import "L10.Engine.CPreDrawMgr"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local UIPanel = import "UIPanel"
local Mesh = import "UnityEngine.Mesh"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientFurniture = import "L10.Game.CClientFurniture"
local Material = import "UnityEngine.Material"
local ShadowCastingMode = import "UnityEngine.Rendering.ShadowCastingMode"
local Tags = import "L10.Game.Tags"
local Setting = import "L10.Engine.Setting"
local LineRenderer = import "UnityEngine.LineRenderer"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CSpeakingBubble = import "L10.UI.CSpeakingBubble"
local EnumSpeakingBubbleType = import "L10.Game.EnumSpeakingBubbleType"

LuaStarInviteWatchWnd=class()
RegistChildComponent(LuaStarInviteWatchWnd,"closeBtn", GameObject)
RegistChildComponent(LuaStarInviteWatchWnd,"showTexture", UITexture)
RegistChildComponent(LuaStarInviteWatchWnd,"midLeft", GameObject)
RegistChildComponent(LuaStarInviteWatchWnd,"topRight", GameObject)
RegistChildComponent(LuaStarInviteWatchWnd,"bottomRight", GameObject)
RegistChildComponent(LuaStarInviteWatchWnd,"opNode", GameObject)
RegistChildComponent(LuaStarInviteWatchWnd,"gossipNode", GameObject)
RegistChildComponent(LuaStarInviteWatchWnd,"gossipFatherNode", GameObject)

RegistClassMember(LuaStarInviteWatchWnd, "m_OriginalShowAllObject")
RegistClassMember(LuaStarInviteWatchWnd, "cameraNode")
RegistClassMember(LuaStarInviteWatchWnd, "m_Tick")
RegistClassMember(LuaStarInviteWatchWnd, "cameraScript")
RegistClassMember(LuaStarInviteWatchWnd, "nowClickBtn")
RegistClassMember(LuaStarInviteWatchWnd, "nowClickIndex")
RegistClassMember(LuaStarInviteWatchWnd, "setPosState")
RegistClassMember(LuaStarInviteWatchWnd, "nowChooseClientObject")
RegistClassMember(LuaStarInviteWatchWnd, "nowChooseClientId")
RegistClassMember(LuaStarInviteWatchWnd, "followNode")
RegistClassMember(LuaStarInviteWatchWnd, "setPosCanMove")

RegistClassMember(LuaStarInviteWatchWnd, "screenWidthRatio")
RegistClassMember(LuaStarInviteWatchWnd, "screenHeightRatio")

RegistClassMember(LuaStarInviteWatchWnd, "putAreaCenter")
RegistClassMember(LuaStarInviteWatchWnd, "putAreaWidth")
RegistClassMember(LuaStarInviteWatchWnd, "putAreaHeight")

RegistClassMember(LuaStarInviteWatchWnd, "maxShowNum")

RegistClassMember(LuaStarInviteWatchWnd, "saveClientSign")
RegistClassMember(LuaStarInviteWatchWnd, "saveClientPos")
RegistClassMember(LuaStarInviteWatchWnd, "saveClientRotation")

RegistClassMember(LuaStarInviteWatchWnd, "nowChooseTabNode")
RegistClassMember(LuaStarInviteWatchWnd, "leftNodeDefaultPos")
RegistClassMember(LuaStarInviteWatchWnd, "leftNodeMoveDis")

RegistClassMember(LuaStarInviteWatchWnd, "rotateShift")
RegistClassMember(LuaStarInviteWatchWnd, "moveShift")
RegistClassMember(LuaStarInviteWatchWnd, "gossipNodeTable")

function LuaStarInviteWatchWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateStarInviteAudienceRelease", self, "UpdateStarInviteAudienceRelease")
	g_ScriptEvent:AddListener("UpdateStarInviteAudiencePack", self, "UpdateStarInviteAudiencePack")
	g_ScriptEvent:AddListener("UpdateStarInviteNpcPosSucc", self, "UpdateStarInviteNpcPosSucc")
	g_ScriptEvent:AddListener('UpdateStarInviteNpcGossipShow', self, 'UpdateStarInviteNpcGossipShow')
	g_ScriptEvent:AddListener("OnScreenChange", self, "UpdateScreen")
end

function LuaStarInviteWatchWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateStarInviteAudienceRelease", self, "UpdateStarInviteAudienceRelease")
	g_ScriptEvent:RemoveListener("UpdateStarInviteAudiencePack", self, "UpdateStarInviteAudiencePack")
	g_ScriptEvent:RemoveListener("UpdateStarInviteNpcPosSucc", self, "UpdateStarInviteNpcPosSucc")
	g_ScriptEvent:RemoveListener('UpdateStarInviteNpcGossipShow', self, 'UpdateStarInviteNpcGossipShow')
	g_ScriptEvent:RemoveListener("OnScreenChange", self, "UpdateScreen")
end

function LuaStarInviteWatchWnd:UpdateScreen()
	if self.cameraNode then
		GameObject.Destroy(self.cameraNode)
		self.cameraNode = nil
	end
	local screenWidth = CommonDefs.GameScreenWidth
  local screenHeight = CommonDefs.GameScreenHeight
	if 1920 / screenWidth > 1080 / screenHeight then
		self.showTexture.width = 1920--Screen.width
		self.showTexture.height = screenHeight * 1920 / screenWidth--Screen.height
	else
		self.showTexture.width = screenWidth * 1080 / screenHeight--Screen.width
		self.showTexture.height = 1080--Screen.height
	end
	self.screenWidthRatio = self.showTexture.width / screenWidth
	self.screenHeightRatio = self.showTexture.height / screenHeight

	local renderTex = RenderTexture(self.showTexture.width,self.showTexture.height,32,RenderTextureFormat.ARGB32,RenderTextureReadWrite.Default)
	renderTex.wrapMode = TextureWrapMode.Clamp
	renderTex.filterMode = FilterMode.Bilinear
	renderTex.anisoLevel = 2
	--local TargetCamera = self.TargetCameraNode:GetComponent(typeof(Camera))
	self.showTexture.mainTexture = renderTex

	self.cameraNode = GameObject("__StarInviteWatchCamera__")
	local cameraScript = CommonDefs.AddCameraComponent(self.cameraNode)
	cameraScript.clearFlags = CameraClearFlags.Skybox
  --cameraScript.backgroundColor = Color(49 / 255, 77 / 255, 121 / 255, 5 / 255)
  cameraScript.cullingMask = 2^LayerDefine.ModelForNGUI_3D + 2^LayerDefine.Effect_3D + 2^LayerDefine.Default + 2^LayerDefine.Water + 2^LayerDefine.WaterReflection + 2^LayerDefine.Terrain + 2^LayerDefine.NPC
  cameraScript.orthographic = false
  cameraScript.fieldOfView = 36
  cameraScript.farClipPlane = 40
  cameraScript.nearClipPlane = 0.1
  cameraScript.depth = -10
  cameraScript.transform.parent = CUIManager.instance.transform
	local settingData = StarBiWuShow_Setting.GetData()
	local cameraPos = LuaStarInviteMgr.split(settingData.CameraPos,',')
	local rotateShift = settingData.QinYouRotateAngle
	self.rotateShift = rotateShift
	local cameraRatation = LuaStarInviteMgr.split(settingData.CameraRotate,',')
	self.cameraNode.transform.position = Vector3(cameraPos[1],cameraPos[2],cameraPos[3])
  self.cameraNode.transform.localRotation = Quaternion.Euler(cameraRatation[1],cameraRatation[2],cameraRatation[3])
  --cameraScript.transform.localScale = Vector3.one
	cameraScript.targetTexture = renderTex
	self.cameraScript = cameraScript
end

function LuaStarInviteWatchWnd:UpdateStarInviteNpcGossipShow(engineId)
	if engineId then
		local clientObject = CClientObjectMgr.Inst:GetObject(engineId)
		if clientObject then
			local gossipNode = nil
			if not self.gossipNodeTable then
				self.gossipNodeTable = {}
			end

			if self.gossipNodeTable[engineId] and self.gossipNodeTable[engineId].node then
				gossipNode = self.gossipNodeTable[engineId].node
			else
				gossipNode = NGUITools.AddChild(self.gossipFatherNode,self.gossipNode)
				self.gossipNodeTable[engineId] = {}
				self.gossipNodeTable[engineId].node = gossipNode
			end
			gossipNode:SetActive(true)
			self.gossipNodeTable[engineId].time = os.time()

			local gossipText = LuaStarInviteMgr.ShowGossipSign[engineId]
			local speakBubble = gossipNode:GetComponent(typeof(CSpeakingBubble))
			speakBubble:Init(gossipText,EnumSpeakingBubbleType.Default,DelegateFactory.Action(function()
			end),5)

			local pos = self:WorldPos2ScreenPos(clientObject.RO.Position,self.cameraScript)
			gossipNode.transform.localPosition = Vector3(pos.x,pos.y+150,pos.z)
		end
	end
end

function LuaStarInviteWatchWnd:UpdateStarInviteNpcPosSucc(id)
	self:ResetMove()
	self:InitMidLeft(nil,true)
	--self:SetMove(id)
	--self:SaveClientInfo()
end

function LuaStarInviteWatchWnd:UpdateStarInviteAudienceRelease(id)
	self:ResetMove()
	self:InitMidLeft(id,true)
	self:SetMove(id)
end

function LuaStarInviteWatchWnd:UpdateStarInviteAudiencePack(id)
	self:ResetMove()
	self:InitMidLeft(nil,true)
end

function LuaStarInviteWatchWnd:LeaveWnd()
	CUIManager.CloseUI(CLuaUIResources.StarInviteWatchWnd)
	Gac2Gas.RequestLeavePlay()
end

function LuaStarInviteWatchWnd:Init()
	local onCloseClick = function(go)
		MessageWndManager.ShowOKCancelMessage(LocalString.GetString("确定要退出吗？"), DelegateFactory.Action(function ()
				self:LeaveWnd()
		end), nil, nil, nil, false)
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	CPreDrawMgr.m_bEnableRectPreDraw = false

	local screenWidth = CommonDefs.GameScreenWidth
  local screenHeight = CommonDefs.GameScreenHeight
	if 1920 / screenWidth > 1080 / screenHeight then
		self.showTexture.width = 1920--Screen.width
		self.showTexture.height = screenHeight * 1920 / screenWidth--Screen.height
	else
		self.showTexture.width = screenWidth * 1080 / screenHeight--Screen.width
		self.showTexture.height = 1080--Screen.height
	end
	self.screenWidthRatio = self.showTexture.width / screenWidth
	self.screenHeightRatio = self.showTexture.height / screenHeight

	local renderTex = RenderTexture(self.showTexture.width,self.showTexture.height,32,RenderTextureFormat.ARGB32,RenderTextureReadWrite.Default)
	renderTex.wrapMode = TextureWrapMode.Clamp
	renderTex.filterMode = FilterMode.Bilinear
	renderTex.anisoLevel = 2
	self.showTexture.mainTexture = renderTex

	self.cameraNode = GameObject("__StarInviteWatchCamera__")
	local cameraScript = CommonDefs.AddCameraComponent(self.cameraNode)
    CMainCamera.Inst:SetCameraEnableStatus(false,"use_another_camera", false)
	cameraScript.clearFlags = CameraClearFlags.Skybox
  --cameraScript.backgroundColor = Color(49 / 255, 77 / 255, 121 / 255, 5 / 255)
  cameraScript.cullingMask = 2^LayerDefine.ModelForNGUI_3D + 2^LayerDefine.Effect_3D + 2^LayerDefine.Default + 2^LayerDefine.Water + 2^LayerDefine.WaterReflection + 2^LayerDefine.Terrain + 2^LayerDefine.NPC
  cameraScript.orthographic = false
  cameraScript.fieldOfView = 36
  cameraScript.farClipPlane = 40
  cameraScript.nearClipPlane = 0.1
  cameraScript.depth = -10
  cameraScript.transform.parent = CUIManager.instance.transform
	local settingData = StarBiWuShow_Setting.GetData()
	local cameraPos = LuaStarInviteMgr.split(settingData.CameraPos,',')
	local rotateShift = settingData.QinYouRotateAngle
	self.rotateShift = rotateShift
	local cameraRatation = LuaStarInviteMgr.split(settingData.CameraRotate,',')
	self.cameraNode.transform.position = Vector3(cameraPos[1],cameraPos[2],cameraPos[3])
  self.cameraNode.transform.localRotation = Quaternion.Euler(cameraRatation[1],cameraRatation[2],cameraRatation[3])
  --cameraScript.transform.localScale = Vector3.one
	cameraScript.targetTexture = renderTex
	self.cameraScript = cameraScript
	--lineNode = GameObject("__KitePlayLine__")
	--local lineRenderScript = lineNode:AddComponent(typeof(LineRenderer))
	--lineRenderScript.material:SetColor("_Color", Color.white)
	--lineRenderScript:SetWidth(1,1)
	--lineRenderScript:SetPosition(0,cameraNode.transform.position)
	--lineRenderScript:SetPosition(1,Vector3(selfData.kitePos[1],selfData.kitePos[2],selfData.kitePos[3]))

	self.m_OriginalShowAllObject = CRenderScene.Inst.ShowAllObject
	CRenderScene.Inst.ShowAllObject = true

	local gossipBtn = self.bottomRight.transform:Find('chatBtn/Tap'):GetComponent(typeof(QnSelectableButton))
	local victoryBtn = self.bottomRight.transform:Find('winBtn/Tap'):GetComponent(typeof(QnSelectableButton))

	if LuaStarInviteMgr.ShowGossipSign and LuaStarInviteMgr.ShowGossipSign == 1 then
		gossipBtn:SetSelected(true,false)
	else
		gossipBtn:SetSelected(false,false)
	end

	if LuaStarInviteMgr.ShowVictorySign and LuaStarInviteMgr.ShowVictorySign == 1 then
		victoryBtn:SetSelected(true,false)
	else
		victoryBtn:SetSelected(false,false)
	end

	gossipBtn.OnButtonSelected = DelegateFactory.Action_bool(function(b)
		if b then
			Gac2Gas.StarBiwuSetAudienceViewGossip(true)
		else
			Gac2Gas.StarBiwuSetAudienceViewGossip(false)
		end
	end)

	victoryBtn.OnButtonSelected = DelegateFactory.Action_bool(function(b)
		if b then
			Gac2Gas.StarBiwuSetAudienceViewVictory(true)
		else
			Gac2Gas.StarBiwuSetAudienceViewVictory(false)
		end
	end)

	self:InitTotalInfo()
end

function LuaStarInviteWatchWnd:InitMidLeft(chooseId,savePosSign)
	local scrollView = self.midLeft.transform:Find('ScrollView'):GetComponent(typeof(UIScrollView))
	local panel = self.midLeft.transform:Find('ScrollView'):GetComponent(typeof(UIPanel))
	local table = self.midLeft.transform:Find('ScrollView/Table'):GetComponent(typeof(UITable))
	local templateNode = self.midLeft.transform:Find('InfoItem').gameObject
	local titleNum = self.midLeft.transform:Find('title/num'):GetComponent(typeof(UILabel))

  local savePos = scrollView.transform.localPosition
  local saveOffsetY = panel.clipOffset.y

	templateNode:SetActive(false)
	Extensions.RemoveAllChildren(table.transform)
	self.nowChooseTabNode = nil

	local count = 0
	if LuaStarInviteMgr.ShowAudienceTable then
		for i,v in ipairs(LuaStarInviteMgr.ShowAudienceTable) do
			local node = NGUITools.AddChild(table.gameObject,templateNode)
			node:SetActive(true)
			node.transform:Find('icon'):GetComponent(typeof(CUITexture)):LoadPortrait(CUICommonDef.GetPortraitName(v.clazz,v.gender),true)
			node.transform:Find('name'):GetComponent(typeof(UILabel)).text = v.name
			local putBtn = node.transform:Find('putBtn').gameObject
			putBtn:SetActive(false)
			local clickBtn = node.transform:Find('bg').gameObject
			local clickSignNode = node.transform:Find('clickSign').gameObject
			clickSignNode:SetActive(false)
			local onPutBtn = function(go)
				Gac2Gas.StarBiwuAudienceRelease(v.id)
			end
			CommonDefs.AddOnClickListener(putBtn,DelegateFactory.Action_GameObject(onPutBtn),false)

			local onClickBtn = function(go)
				if self.nowChooseTabNode then
					self.nowChooseTabNode:SetActive(false)
					self.nowChooseTabNode = nil
				end
				self:CancelMove()
				if not LuaStarInviteMgr.ShowAudienceObjectTable or not LuaStarInviteMgr.ShowAudienceObjectTable[v.id] then
					Gac2Gas.StarBiwuAudienceRelease(v.id)
				elseif LuaStarInviteMgr.ShowAudienceObjectTable[v.id] then
					self:SetMove(v.id)
				end
				clickSignNode:SetActive(true)
				self.nowChooseTabNode = clickSignNode
			end
			CommonDefs.AddOnClickListener(clickBtn,DelegateFactory.Action_GameObject(onClickBtn),false)

			if chooseId and chooseId == v.id then
				--onClickBtn()
				clickSignNode:SetActive(true)
				self.nowChooseTabNode = clickSignNode
			end

			count = count + 1
		end
	end

	table:Reposition()
	scrollView:ResetPosition()

	titleNum.text = count .. '/' .. self.maxShowNum

  if savePosSign then
    scrollView.transform.localPosition = savePos
    panel.clipOffset = Vector2(panel.clipOffset.x, saveOffsetY)
  end
		--table.insert(LuaStarInviteMgr.ShowAudienceTable,{id = id,atype = atype,name = name,clazz = clazz,gender = gender})

	local leftPackBtn = self.midLeft.transform:Find('title/rightBtn').gameObject

	local onLeftPackBtn = function(go)
		--self.leftNodeDefaultPos = self.midLeft.transform.localPosition
		--self.leftNodeMoveDis = 265
		--local pos = self.midLeft.transform.localPosition
		local rotateZ = leftPackBtn.transform.eulerAngles.z
		if rotateZ > 0 then
			LuaUtils.SetLocalRotation(leftPackBtn.transform,0,0,0)
			self.midLeft.transform.localPosition = self.leftNodeDefaultPos
		else
			LuaUtils.SetLocalRotation(leftPackBtn.transform,0,0,180)
			self.midLeft.transform.localPosition = Vector3(self.leftNodeDefaultPos.x - self.leftNodeMoveDis,self.leftNodeDefaultPos.y,self.leftNodeDefaultPos.z)
		end
	end
	CommonDefs.AddOnClickListener(leftPackBtn,DelegateFactory.Action_GameObject(onLeftPackBtn),false)
end

function LuaStarInviteWatchWnd:SetFollowNode(cClientNode)
	if not self.followNode then
		local width = 1
		local height = 1
		local count = width * height
		local node = GameObject("__FollowNode__")
		node.transform.localPosition = Vector3.zero
		node.transform.localEulerAngles = Vector3.zero
		local mf = node:AddComponent(typeof(MeshFilter))
		local mMesh = CreateFromClass(Mesh)
		mMesh.name = 'grid'
		local mVTable = {Vector3(-0.5,0,-0.5),Vector3(-0.5,0,0.5),Vector3(0.5,0,-0.5),Vector3(0.5,0,0.5)}
		local mTTable = {0,1,2,1,3,2}
--		for i=1,4*count do
--			table.insert(mVTable,CreateFromClass(Vector3))
--		end
--		for i=1,6*count do
--			table.insert(mTTable,0)
--		end
		local mVertices = Table2ArrayWithCount(mVTable, #mVTable, MakeArrayClass(Vector3))
		local mTriangles = Table2ArrayWithCount(mTTable, #mTTable, MakeArrayClass(int))
		mMesh.vertices = mVertices
		mMesh.triangles = mTriangles
		mf.sharedMesh = mMesh

		local mr = node:AddComponent(typeof(MeshRenderer))
		mr.sharedMaterial = CreateFromClass(Material,CClientFurniture.s_GridColorMaterial)
		mr.sharedMaterial:SetColor('_GridColor', Color.green)
		mr.shadowCastingMode = ShadowCastingMode.Off
		mr.receiveShadows = false

		self.followNode = node
	end

	if cClientNode and self.followNode then
		if cClientNode.RO then
			local roPosition = cClientNode.RO.Position
			self.followNode.transform.position = Vector3(roPosition.x,roPosition.y+0.1,roPosition.z)
			self:CheckValid()
		end
	end
end

function LuaStarInviteWatchWnd:ResetMove()
	self.nowChooseClientObject = nil
	self.nowChooseClientId = nil
	self.saveClientSign = false
	self.saveClientPos = nil
	self.saveClientRotation = nil

	self.setPosState = false
	self.moveShift = nil
	if self.followNode then
		GameObject.Destroy(self.followNode)
		self.followNode = nil
	end
	self.opNode:SetActive(false)
end

function LuaStarInviteWatchWnd:WorldPos2ScreenPos(worldPos, camera)
	local screenPos = camera:WorldToScreenPoint(worldPos)
	local localPos = {x = screenPos.x, y = screenPos.y}
	return Vector3(localPos.x - self.showTexture.width/2, localPos.y - self.showTexture.height/2,0)
end

function LuaStarInviteWatchWnd:SetMove(id) -- start move one npc
	local engineId = LuaStarInviteMgr.ShowAudienceObjectTable[id]
	if engineId then
		local clientObject = CClientObjectMgr.Inst:GetObject(engineId)
		if clientObject then
			self.nowChooseClientObject = clientObject
			self.nowChooseClientId = id
			self.saveClientSign = true
			self:SetFollowNode(clientObject)
			self.setPosState = true
			if not self.opNode.activeSelf then
				self.opNode:SetActive(true)
			end
			local pos = self:WorldPos2ScreenPos(self.nowChooseClientObject.RO.Position,self.cameraScript)
			self.opNode.transform.localPosition = pos
			self:InitOpNode()
		end
	end
end

function LuaStarInviteWatchWnd:CancelMove()
	if self.saveClientPos and self.saveClientRotation and self.nowChooseClientObject and self.nowChooseClientObject.RO then
		self.nowChooseClientObject.RO.Position = self.saveClientPos
		self.nowChooseClientObject.RO.transform.eulerAngles = self.saveClientRotation
		--self.followNode.transform.position = self.saveClientPos
		--self.followNode.transform.position = Vector3(self.saveClientPos.x,self.saveClientPos.y + 0.1,self.saveClientPos.z)
	end
end

function LuaStarInviteWatchWnd:ScreenPoint2WorldPos(screenPoint,watchCamera)
	local hit = nil
	local ray = watchCamera:ScreenPointToRay(screenPoint)
	local dist = watchCamera.farClipPlane
	local hits = Physics.RaycastAll(ray, dist, watchCamera.cullingMask)
	if not hits or hits.Length == 0 then
		return nil
	end
	local hitDis = watchCamera.farClipPlane
	for i=0,hits.Length - 1 do
		local hitData = hits[i]
		if hitData.collider:CompareTag(Tags.Ground) then
			if hitDis > hitData.distance then
				hit = hitData
				hitDis = hitData.distance
			end
		end
	end

	if hit then
		--local pixelPos = Utility.WorldPos2PixelPos(hit.point)
		return hit.point --{x = hit.point.x, z = hit.point.z}
	end
end

function LuaStarInviteWatchWnd:UpdatePos(pixelPos,screenPos)
	if pixelPos and self.nowChooseClientObject and self.followNode then
		self.followNode.transform.position = Vector3(pixelPos.x,pixelPos.y + 0.1,pixelPos.z)
		self.nowChooseClientObject.RO.Position = pixelPos

		if not self.opNode.activeSelf then
			self.opNode:SetActive(true)
		end
		self.opNode.transform.localPosition = Vector3(screenPos.x - self.showTexture.width/2, screenPos.y - self.showTexture.height/2,screenPos.z)
	end

	self:CheckValid()
end

function LuaStarInviteWatchWnd:InitOpNode()
		local submitBtn = self.opNode.transform:Find('opPanel1/btn1').gameObject
		local onSubmitBtn = function(go)
			local valid = self:CheckValid()
			if valid then
				local posX = math.ceil(self.nowChooseClientObject.RO.Position.x * Setting.eGridSpan)
				local posZ = math.ceil(self.nowChooseClientObject.RO.Position.z * Setting.eGridSpan)
				local rotation = 90 - self.nowChooseClientObject.RO.transform.eulerAngles.y
				if rotation < 0 then
					rotation = rotation + 360
				end
				Gac2Gas.StarBiwuSetAudienceLocation(self.nowChooseClientId,posX,posZ,rotation)
			else
				g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请放置在绿色框中!"))
			end
		end
		CommonDefs.AddOnClickListener(submitBtn,DelegateFactory.Action_GameObject(onSubmitBtn),false)

		local cancelBtn = self.opNode.transform:Find('opPanel1/btn5').gameObject
		local onCancelBtn = function(go)
			if self.saveClientPos and self.saveClientRotation then
				self.nowChooseClientObject.RO.Position = self.saveClientPos
				self.nowChooseClientObject.RO.transform.eulerAngles = self.saveClientRotation
				--self.followNode.transform.position = self.saveClientPos
				self.followNode.transform.position = Vector3(self.saveClientPos.x,self.saveClientPos.y + 0.1,self.saveClientPos.z)
			end
			self.opNode:SetActive(false)
		end
		CommonDefs.AddOnClickListener(cancelBtn,DelegateFactory.Action_GameObject(onCancelBtn),false)

		local putBackBtn = self.opNode.transform:Find('opPanel1/btn2').gameObject
		local onPutBackBtn = function(go)
			Gac2Gas.StarBiwuAudiencePack(self.nowChooseClientId)
			self.opNode:SetActive(false)
		end
		CommonDefs.AddOnClickListener(putBackBtn,DelegateFactory.Action_GameObject(onPutBackBtn),false)

		local settingBtn = self.opNode.transform:Find('opPanel1/btn3').gameObject
		local onSettingBtn = function(go)
			--CUIManager.ShowUI(CLuaUIResources.StarInviteSettingWnd)
			Gac2Gas.QueryAudienceAction(self.nowChooseClientId)
		end
		CommonDefs.AddOnClickListener(settingBtn,DelegateFactory.Action_GameObject(onSettingBtn),false)

		local rotateBtn = self.opNode.transform:Find('opPanel1/btn4').gameObject
		local onRotateBtn = function(go)
			local nowY = self.nowChooseClientObject.RO.transform.eulerAngles.y
			local delta = self.rotateShift
			local newY = (nowY + delta) % 360

			self.nowChooseClientObject.RO.transform.eulerAngles = Vector3(0,newY,0)
		end
		CommonDefs.AddOnClickListener(rotateBtn,DelegateFactory.Action_GameObject(onRotateBtn),false)

end

function LuaStarInviteWatchWnd:SaveClientInfo()
	if self.nowChooseClientObject then
		self.saveClientPos = self.nowChooseClientObject.RO.Position
		self.saveClientRotation = self.nowChooseClientObject.RO.transform.eulerAngles
	end

	--if not self.opNode.activeSelf then
		--self.opNode:SetActive(true)


	--end
end

function LuaStarInviteWatchWnd:CheckValid()
	if self.followNode then
		local move = self.followNode.transform.position
		if move.z < self.putAreaCenter.z + self.putAreaWidth / 2 and move.z > self.putAreaCenter.z - self.putAreaWidth / 2 then
			if move.x < self.putAreaCenter.x + self.putAreaHeight / 2 and move.x > self.putAreaCenter.x - self.putAreaHeight / 2 then
				local mr = self.followNode:GetComponent(typeof(MeshRenderer))
				mr.sharedMaterial:SetColor('_GridColor', Color.green)
				return true, move
			end
		end
	end
	local mr = self.followNode:GetComponent(typeof(MeshRenderer))
	mr.sharedMaterial:SetColor('_GridColor', Color.red)
	return false
end

function LuaStarInviteWatchWnd:Update()
	if LuaStarInviteMgr.CancelUpdate then
		return
	end
	if self.setPosState then
		if Input.GetMouseButtonDown(0) then
			local posX = Input.mousePosition.x * self.screenWidthRatio
			local posY = Input.mousePosition.y * self.screenHeightRatio
			local screenPos = Vector3(posX,posY,0)
			local pixelPos = self:ScreenPoint2WorldPos(screenPos,self.cameraScript)
			if pixelPos and self.nowChooseClientObject and self.followNode then
				local followPos = self.followNode.transform.position
				local disX = (pixelPos.x - followPos.x)
				local disZ = (pixelPos.z - followPos.z)
				if math.abs(disX) + math.abs(disZ) < 3 then
					if self.saveClientSign then
						self.saveClientSign = false
						self:SaveClientInfo()
						self.moveShift = {x = disX,y = disZ}
					end
					--if self.moveShift then
					--	self:UpdatePos(pixelPos,Vector3(screenPos.x + self.moveShift.x,screenPos.y + self.moveShift.y,0))
					--else
						self:UpdatePos(pixelPos,screenPos)
					--end
					self.setPosCanMove = true
				else
					self.setPosCanMove = false
				end
			end
		elseif Input.GetMouseButton(0) then
			if self.setPosCanMove then
				local posX = Input.mousePosition.x * self.screenWidthRatio
				local posY = Input.mousePosition.y * self.screenHeightRatio
				local screenPos = Vector3(posX,posY,0)
				local pixelPos = self:ScreenPoint2WorldPos(screenPos,self.cameraScript)
				--if self.moveShift then
				--	self:UpdatePos(pixelPos,Vector3(screenPos.x + self.moveShift.x,screenPos.y + self.moveShift.y,0))
				--else
					self:UpdatePos(pixelPos,screenPos)
				--end
			end
		elseif Input.GetMouseButtonUp(0) then
			if self.setPosCanMove then
				local posX = Input.mousePosition.x * self.screenWidthRatio
				local posY = Input.mousePosition.y * self.screenHeightRatio
				local screenPos = Vector3(posX,posY,0)
				local pixelPos = self:ScreenPoint2WorldPos(screenPos,self.cameraScript)
				--if self.moveShift then
				--	self:UpdatePos(pixelPos,Vector3(screenPos.x + self.moveShift.x,screenPos.y + self.moveShift.y,0))
				--else
					self:UpdatePos(pixelPos,screenPos)
				--end
				self.setPosCanMove = false
			end
		end
	end
end

function LuaStarInviteWatchWnd:InitBottomRight()
end

function LuaStarInviteWatchWnd:InitData()
	local settingData = StarBiWuShow_Setting.GetData()
	local centerPos = LuaStarInviteMgr.split(settingData.QinYouPosCenter,',')

	local center = {x = centerPos[1],y = centerPos[2],z = centerPos[3]} --Setting.eGridSpan
	local width = settingData.QinYouAreaLength
	local height = settingData.QinYouAreaWide
	self.putAreaCenter = center
	self.putAreaWidth = width
	self.putAreaHeight = height

	local maxFriendNum = tonumber(settingData.TotleFriendNum)
	if not maxFriendNum then
		maxFriendNum = 16
	end
	self.maxShowNum = maxFriendNum

	-- init show area
	local linePos = {
		{Vector3(center.x-height/2,center.y,center.z-width/2),Vector3(center.x+height/2,center.y,center.z-width/2)},
		{Vector3(center.x+height/2,center.y,center.z-width/2),Vector3(center.x+height/2,center.y,center.z+width/2)},
		{Vector3(center.x+height/2,center.y,center.z+width/2),Vector3(center.x-height/2,center.y,center.z+width/2)},
		{Vector3(center.x-height/2,center.y,center.z+width/2),Vector3(center.x-height/2,center.y,center.z-width/2)},
	}

	local lineTable = {}

	for i=1,4 do
		local lineNode = GameObject("__StarInviteShowLine__" .. i)
		local lineRenderScript = lineNode:AddComponent(typeof(LineRenderer))
--		local shader = Shader.Find('L10/Normal/Diffuse')
--		if shader then
--			lineRenderScript.material.shader = shader
--		end
--		lineRenderScript.material:SetColor("_Color", Color.green)
		lineRenderScript:SetWidth(0.1,0.1)
		lineRenderScript:SetPosition(0,linePos[i][1])
		lineRenderScript:SetPosition(1,linePos[i][2])
		lineRenderScript.receiveShadows = false
		local CRenderObject = import "L10.Engine.CRenderObject"
		lineRenderScript.sharedMaterial = CreateFromClass(Material,CRenderObject.s_OutlinedEffectMaterial)
		lineRenderScript.sharedMaterial:SetColor('_OutlineColor', Color.green)
		lineRenderScript.sharedMaterial:SetFloat('_LoopTime', 0)
		lineRenderScript.shadowCastingMode = ShadowCastingMode.Off
		lineRenderScript.receiveShadows = false
		table.insert(lineTable,lineNode)
	end
	self.lineTable = lineTable

	self.leftNodeDefaultPos = self.midLeft.transform.localPosition
	self.leftNodeMoveDis = 265
end

function LuaStarInviteWatchWnd:InitTotalInfo()
	self:InitData()
	self:InitMidLeft()
	self:InitBottomRight()
	self.opNode:SetActive(false)
end

function LuaStarInviteWatchWnd:OnDestroy()
    CMainCamera.Inst:SetCameraEnableStatus(true,"use_another_camera", false)
	CPreDrawMgr.m_bEnableRectPreDraw = true
	CRenderScene.Inst.ShowAllObject = self.m_OriginalShowAllObject

	if self.cameraNode then
		GameObject.Destroy(self.cameraNode)
		self.cameraNode = nil
	end
	if self.followNode then
		GameObject.Destroy(self.followNode)
		self.followNode = nil
	end
	if self.lineTable then
		for i,v in pairs(self.lineTable) do
			GameObject.Destroy(v)
		end
		self.lineTable = nil
	end
	if self.m_Tick then
			UnRegisterTick(self.m_Tick)
			self.m_Tick = nil
	end
end

return LuaStarInviteWatchWnd
