local CFashionPreviewTextureLoader = import "L10.UI.CFashionPreviewTextureLoader"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local NGUITools = import "NGUITools"
local DelegateFactory = import "DelegateFactory"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local Extensions = import "Extensions"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local QnButton = import "L10.UI.QnButton"
local Vector3 = import "UnityEngine.Vector3"
local UISprite = import "UISprite"
local EnumShopMallFashionPreviewType = import "L10.UI.EnumShopMallFashionPreviewType"
local UIEventListener = import "UIEventListener"
local CItemMgr = import "L10.Game.CItemMgr"
local String = import "System.String"
local LocalString = import "LocalString"
local Money = import "L10.Game.Money"
local CUITexture = import "L10.UI.CUITexture"
local QnLabel = import "L10.UI.QnLabel"
local UIGrid = import "UIGrid"
local UIScrollView = import "UIScrollView"
local IdPartition = import "L10.Game.IdPartition"
local CItem = import "L10.Game.CItem"
local CEquipment = import "L10.Game.CEquipment"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CUIManager = import "L10.UI.CUIManager"
local GestureRecognizer = import "L10.Engine.GestureRecognizer"
local GestureType = import "L10.Engine.GestureType"
local QnRadioBox = import "L10.UI.QnRadioBox"
local CFashionPreviewMgr = import "L10.UI.CFashionPreviewMgr"
local UITexture = import "UITexture"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local Shader = import "UnityEngine.Shader"
local CRenderObject = import "L10.Engine.CRenderObject"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local LayerDefine = import "L10.Engine.LayerDefine"
local QualitySettings = import "UnityEngine.QualitySettings"
local CMainCamera = import "L10.Engine.CMainCamera"
local Quaternion = import "UnityEngine.Quaternion"
local UIPanel = import "UIPanel"
local CCPlayerCtrl = import "L10.Game.CCPlayerCtrl"
local CWinCGMgr = import "L10.Game.CWinCGMgr"
local QnTableItem = import "L10.UI.QnTableItem"
local EnumGender = import "L10.Game.EnumGender"
local UIProgressBar = import "UIProgressBar"
local CClientObject = import "L10.Game.CClientObject"
local ShaderLodParams = import "L10.Game.ShaderLodParams"
local QnCheckBox = import "L10.UI.QnCheckBox"
local UILabel = import "UILabel"
local CSharpResourceLoader = import "L10.Game.CSharpResourceLoader"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"

LuaShopMallFashionPreviewWnd = class()

RegistChildComponent(LuaShopMallFashionPreviewWnd, "qnModelPreviewer", CFashionPreviewTextureLoader)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "tabButtons", GameObject)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "scrollViewBg", UITexture)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "scrollView", UIScrollView)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "buyButton", QnButton)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "pool", GameObject)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "grid", UIGrid)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "qnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "changeXianFanRadioBox", QnRadioBox)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "takeOffButton", QnButton)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "BuyArea", GameObject)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "BuySingletonShopGoodsButton", QnButton)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "OpenUrlButton", QnButton)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "m_DoExpressionButton", "DoExpressionButton",QnButton)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "m_ShopRoot", "ShopRoot", QnTableView)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "m_TransformButton", "TransformButton", QnButton)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "m_TransformHighlight", "highlight", GameObject)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "m_BlindBoxView", "BlindBoxView", GameObject)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "m_XianFanButton2", "XianFanButton2", QnSelectableButton)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "m_BlindBoxViewLastPageBtn", "BlindBoxViewLastPageBtn", GameObject)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "m_BlindBoxViewNextPageBtn", "BlindBoxViewNextPageBtn", GameObject)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "m_BlindBoxNameLabel", "BlindBoxNameLabel", UILabel)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "m_BlindBoxSpriteTemplate", "BlindBoxSpriteTemplate", GameObject)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "m_BlindBoxViewTable", "BlindBoxViewTable", UITable)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "headAlphaPreviewBtn", "headAlphaPreviewBtn", QnCheckBox)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "m_LeftBottomBtnGrid", "LeftBottomBtnGrid", UIGrid)
RegistChildComponent(LuaShopMallFashionPreviewWnd, "m_ZuoQiTransformAniBtn", "ZuoQiTransformAniBtn",QnButton)

RegistClassMember(LuaShopMallFashionPreviewWnd, "curCategory")
RegistClassMember(LuaShopMallFashionPreviewWnd, "mallInfo")
RegistClassMember(LuaShopMallFashionPreviewWnd, "tabs")
RegistClassMember(LuaShopMallFashionPreviewWnd, "isShrinkShopRoot")
RegistClassMember(LuaShopMallFashionPreviewWnd, "curSelectableSequence")
RegistClassMember(LuaShopMallFashionPreviewWnd, "curCostMoney")
RegistClassMember(LuaShopMallFashionPreviewWnd, "itemID2ShopMallItemMap")
RegistClassMember(LuaShopMallFashionPreviewWnd, "onPinchInDelegate")
RegistClassMember(LuaShopMallFashionPreviewWnd, "onPinchOutDelegate")
RegistClassMember(LuaShopMallFashionPreviewWnd, "isSupportFeiSheng")
RegistClassMember(LuaShopMallFashionPreviewWnd, "localModelScale")
RegistClassMember(LuaShopMallFashionPreviewWnd, "localModelPos")
RegistClassMember(LuaShopMallFashionPreviewWnd, "defaultModelLocalPos")
RegistClassMember(LuaShopMallFashionPreviewWnd, "maxPreviewScale")
RegistClassMember(LuaShopMallFashionPreviewWnd, "maxPreviewOffsetY")
RegistClassMember(LuaShopMallFashionPreviewWnd, "curSelectableFashionID")
RegistClassMember(LuaShopMallFashionPreviewWnd, "babyFashionMallDict")
RegistClassMember(LuaShopMallFashionPreviewWnd, "bfirstInit")
RegistClassMember(LuaShopMallFashionPreviewWnd, "shadowCamera")
RegistClassMember(LuaShopMallFashionPreviewWnd, "shadowCameraRelativePos")
RegistClassMember(LuaShopMallFashionPreviewWnd, "lightAndFloorAndWall")
RegistClassMember(LuaShopMallFashionPreviewWnd, "lightAndFloorAndWallRelativePos")
RegistClassMember(LuaShopMallFashionPreviewWnd, "lastpixelLightCount")
RegistClassMember(LuaShopMallFashionPreviewWnd, "floor")
RegistClassMember(LuaShopMallFashionPreviewWnd, "beiguangLight")
RegistClassMember(LuaShopMallFashionPreviewWnd, "floorInitialLocalPos")
RegistClassMember(LuaShopMallFashionPreviewWnd, "modelRoot")
RegistClassMember(LuaShopMallFashionPreviewWnd, "m_CgName")
RegistClassMember(LuaShopMallFashionPreviewWnd, "m_OwnedShopItems") --拥有的所有道具
RegistClassMember(LuaShopMallFashionPreviewWnd, "m_ItemID2IsPreview")
RegistClassMember(LuaShopMallFashionPreviewWnd, "transformHighlight")
RegistClassMember(LuaShopMallFashionPreviewWnd, "m_BlindBoxResultItemId")
RegistClassMember(LuaShopMallFashionPreviewWnd, "m_EmptyLabel")
RegistClassMember(LuaShopMallFashionPreviewWnd, "m_FashionTransformSpecialExpressionData")
RegistClassMember(LuaShopMallFashionPreviewWnd, "m_YiChuBgScene")

function LuaShopMallFashionPreviewWnd:Init()
    self.m_YiChuBgScene = nil
    self.gameObject:GetComponent(typeof(UIPanel)).IgnoreIphoneXMargin = true
    self:InitData()
    self.localModelScale = 1
    self.curCategory = LuaShopMallFashionPreviewMgr:GetCurCategory()

    self.itemID2ShopMallItemMap = {}
    self.curSelectableSequence = {}
    self.m_ItemID2IsPreview = {}
    self.curCostMoney = 0
    self:LoadMallInfo()
    self.m_BlindBoxSpriteTemplate:SetActive(false)
    UIEventListener.Get(self.qnModelPreviewer.resetButton).onClick =
        CommonDefs.CombineListner_VoidDelegate(
        UIEventListener.Get(self.qnModelPreviewer.resetButton).onClick,
        DelegateFactory.VoidDelegate(
            function()
                self:OnRestButtonClick()
            end
        ),
        true
    )
    UIEventListener.Get(self.m_BlindBoxViewLastPageBtn).onClick =
        CommonDefs.CombineListner_VoidDelegate(
        UIEventListener.Get(self.qnModelPreviewer.resetButton).onClick,
        DelegateFactory.VoidDelegate(
            function()
                self:ChangeBlindBoxViewPage(true)
            end
        ),
        true
    )
    UIEventListener.Get(self.m_BlindBoxViewNextPageBtn).onClick =
        CommonDefs.CombineListner_VoidDelegate(
        UIEventListener.Get(self.qnModelPreviewer.resetButton).onClick,
        DelegateFactory.VoidDelegate(
            function()
                self:ChangeBlindBoxViewPage(false)
            end
        ),
        true
    )
    self.takeOffButton.OnClick =
        DelegateFactory.Action_QnButton(
        function(btn)
            self:OnTakeOffButtonClick()
        end
    )
    self.OpenUrlButton.gameObject:SetActive(false)
    self.OpenUrlButton.OnClick =
        DelegateFactory.Action_QnButton(
        function(btn)
            self:OpenUrl()
        end
    )
    self.m_TransformButton.OnClick =
        DelegateFactory.Action_QnButton(
        function(btn)
            self:Transform()
        end
    )
    self.m_DoExpressionButton.OnClick =
        DelegateFactory.Action_QnButton(
        function(btn)
            self:DoExpression()
        end
    )
    self.headAlphaPreviewBtn.OnValueChanged =
        DelegateFactory.Action_bool(
        function(val)
            self:ProcessSetHeadAlpha(val)
        end
    )
    UIEventListener.Get(self.m_ZuoQiTransformAniBtn.gameObject).onClick = DelegateFactory.VoidDelegate(
        function()
            self:OnZuoQiTransformAniBtnClick()
        end
    )
    self:InitModelPreviewer()
    self:InitHeadAlpha()
    self:InitRadioBox()
    self:InitTabButtons()
    self:InitShopRoot()
    self:InitSellRoot()
    self:InitTransformButton()
    self:InitDoExpressionButton()
    -- 选中玩家打开界面前点击的时装，坐骑，伞
    --self:UpdateCurSelectableSequence()
end

function LuaShopMallFashionPreviewWnd:InitHeadAlpha()
    local haveha = false
    if CClientMainPlayer.Inst then
        haveha = LuaFashionMgr.FashionHaveHeadAlpha(CFashionPreviewMgr.HeadFashionID, CClientMainPlayer.Inst.Gender)
    end
    self.headAlphaPreviewBtn.gameObject:SetActive(haveha)
end

function LuaShopMallFashionPreviewWnd:ProcessSetHeadAlpha(val)
    local appearData = self.qnModelPreviewer.needShowAppearance
    appearData.HideTouSha = val and 1 or 0
    if val then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("你已关闭头部时装的头饰显示"))
    else
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("你已开启头部时装的头饰显示"))
    end
    self:InitModelPreviewer()
end

function LuaShopMallFashionPreviewWnd:ChangeBlindBoxViewPage(isLastPage)
    if not LuaShopMallFashionPreviewMgr.blindBoxItemId then
        return
    end
    local blindBoxData = Item_BlindBox.GetData(LuaShopMallFashionPreviewMgr.blindBoxItemId)
    local lingYuMalldata = Mall_LingYuMall.GetData(LuaShopMallFashionPreviewMgr.blindBoxItemId)
    if not lingYuMalldata then
        lingYuMalldata = Mall_LingYuMallLimit.GetData(LuaShopMallFashionPreviewMgr.blindBoxItemId)
    end
    if not blindBoxData or not lingYuMalldata then
        return
    end
    local progress = self.m_DoExpressionButton.transform:Find("Progress"):GetComponent(typeof(UIProgressBar))
    LuaTweenUtils.DOKill(progress.transform, false)
    progress.gameObject:SetActive(false)
    local newIndex = (LuaShopMallFashionPreviewMgr.blindBoxSelectIndex + (isLastPage and -1 or 1) + blindBoxData.List.Length) % blindBoxData.List.Length
    local vehicleId, pos2fashionIdMap, itemIds = LuaShopMallFashionPreviewMgr:GetBlindBoxResult(newIndex, blindBoxData, lingYuMalldata)
    self:PreviewBlindBoxWithVehicleIdAndFashionId(itemIds, vehicleId, pos2fashionIdMap, LuaShopMallFashionPreviewMgr.blindBoxItemId)
    self:InitBlindBoxView(LuaShopMallFashionPreviewMgr.blindBoxItemId)
end

function LuaShopMallFashionPreviewWnd:OpenUrl()
    if self.m_CgName then
        if not CCPlayerCtrl.IsSupportted() then
            g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("客户端引擎版本过旧，不能播放此视频，请更新版本后再试"))
            return
        end
        local url = CommonDefs.IS_PUB_RELEASE and "http://l10.gph.netease.com/fashionpreviewvideo/" or "http://l10-patch.leihuo.netease.com/fashionpreviewvideo/"
        CWinCGMgr.Inst.m_CGUrl = url .. self.m_CgName
        CUIManager.ShowUI(CUIResources.WinCGWnd)
    end
end

--调整了一下纹理的大小或位置后在不改策划表的前提下，用以修正模型的位置
function LuaShopMallFashionPreviewWnd:FixPos(pos)
    return pos.x + 0.56, pos.y, pos.z / 2
end

function LuaShopMallFashionPreviewWnd:InitData()
    local s = Fashion_Setting.GetData("FashionPreview_DefaultModelLocalPos").Value
    local function split(str, reps)
        local resultStrList = {}
        string.gsub(
            str,
            "[^" .. reps .. "]+",
            function(w)
                table.insert(resultStrList, w)
            end
        )
        return resultStrList[1], resultStrList[2], resultStrList[3]
    end
    self.defaultModelLocalPos = Vector3.zero
    self.defaultModelLocalPos.x, self.defaultModelLocalPos.y, self.defaultModelLocalPos.z = split(s, ",")
    self.defaultModelLocalPos.x, self.defaultModelLocalPos.y, self.defaultModelLocalPos.z = LuaShopMallFashionPreviewWnd:FixPos(self.defaultModelLocalPos)
    if CClientMainPlayer.Inst then
        Fashion_FashionPreview.Foreach(
            function(key, data)
                local class = CommonDefs.Convert_ToUInt32(CClientMainPlayer.Inst.Class)
                local gender = CommonDefs.Convert_ToUInt32(CClientMainPlayer.Inst.Gender)
                if data.Class == class and data.Gender == gender then
                    self.maxPreviewScale = data.MaxPreviewScale
                    self.maxPreviewOffsetY = data.MaxPreviewOffsetY
                end
            end
        )
    end
end

function LuaShopMallFashionPreviewWnd:OnEnable()
    self.onPinchInDelegate =
        DelegateFactory.Action_GenericGesture(
        function(gesture)
            self:OnPinchIn(gesture)
        end
    )
    self.onPinchOutDelegate =
        DelegateFactory.Action_GenericGesture(
        function(gesture)
            self:OnPinchOut(gesture)
        end
    )
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchIn, self.onPinchInDelegate)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchOut, self.onPinchOutDelegate)
    g_ScriptEvent:AddListener("MouseScrollWheel", self, "OnMouseScrollWheel")
    g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:AddListener("OnSyncRepertoryForMallPreviewDone", self, "OnSyncRepertoryForMallPreviewDone")
    g_ScriptEvent:AddListener("OnLingyuLimitUpdatedForSearchTemplateId", self, "OnLingyuLimitUpdatedForSearchTemplateId")
    if LuaShopMallFashionPreviewMgr.enableMultiLight then
        self.lastpixelLightCount = 0
        QualitySettings.pixelLightCount = 3
        if Shader.globalMaximumLOD == ShaderLodParams.SHADER_LOD_HIGH_LEVEL then
            Shader.globalMaximumLOD = ShaderLodParams.SHADER_LOD_MULTILIGHT
        end
    end
end

function LuaShopMallFashionPreviewWnd:OnDisable()
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchIn, self.onPinchInDelegate)
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchOut, self.onPinchOutDelegate)
    self.onPinchInDelegate = nil
    self.onPinchOutDelegate = nil
    g_ScriptEvent:RemoveListener("MouseScrollWheel", self, "OnMouseScrollWheel")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:RemoveListener("OnSyncRepertoryForMallPreviewDone", self, "OnSyncRepertoryForMallPreviewDone")
    g_ScriptEvent:RemoveListener("OnLingyuLimitUpdatedForSearchTemplateId", self, "OnLingyuLimitUpdatedForSearchTemplateId")
    LuaShopMallFashionPreviewMgr:OnClose()
    if LuaShopMallFashionPreviewMgr.enableMultiLight and self.lastpixelLightCount then
        QualitySettings.pixelLightCount = self.lastpixelLightCount
        if Shader.globalMaximumLOD == ShaderLodParams.SHADER_LOD_MULTILIGHT then
            Shader.globalMaximumLOD = ShaderLodParams.SHADER_LOD_HIGH_LEVEL
        end
    end
    local progress = self.m_DoExpressionButton.transform:Find("Progress"):GetComponent(typeof(UIProgressBar))
    LuaTweenUtils.DOKill(progress.transform, false)
end

function LuaShopMallFashionPreviewWnd:OnLingyuLimitUpdatedForSearchTemplateId()
    self:Reload()
end

function LuaShopMallFashionPreviewWnd:OnSetItemAt(args)
    local place, position, oldItemId, newItemId = args[0], args[1], args[2], args[3]
    local item = CItemMgr.Inst:GetById(newItemId)
    if item then
        local templateId = item.TemplateId
        self.m_OwnedShopItems = LuaShopMallFashionPreviewMgr:GetOwnedShopItemsInfo()

        for i = 0, self.m_ShopRoot.m_ItemList.Count - 1 do
            local item = self.m_ShopRoot.m_ItemList[i]
            local mall = self.mallInfo[item.Row + 1]
            local id = mall.ItemId
            local isOwned = self.m_OwnedShopItems[id]
            if id == templateId then
                local ownerSprite = item.transform:Find("Owner").gameObject
                ownerSprite:SetActive(isOwned)
            end
        end
    end
end

function LuaShopMallFashionPreviewWnd:ClearShopMallFashionPreview()
    CFashionPreviewMgr.HeadFashionID = 0
    CFashionPreviewMgr.BodyFashionID = 0
    CFashionPreviewMgr.ZuoQiFashionID = 0
    CFashionPreviewMgr.UmbrellaFashionID = 0
end

function LuaShopMallFashionPreviewWnd:FixInitModelPreviewer()
    --待坐骑和角色全部加载完后才显示角色以修复短暂的卡在地上的现象
    self.qnModelPreviewer:LoadModel()
    self.qnModelPreviewer.m_RO.Visible = false
    self.qnModelPreviewer.m_RO:AddLoadAllFinishedListener(
        DelegateFactory.Action_CRenderObject(
            function(ro)
                if ro.VehicleRO then
                    ro.VehicleRO:AddLoadAllFinishedListener(
                        DelegateFactory.Action_CRenderObject(
                            function(_ro)
                                self.qnModelPreviewer.m_RO.Visible = true
                                if CFashionPreviewMgr.UmbrellaFashionID > 0 then
                                    local gender =  CClientMainPlayer.Inst and EnumToInt(CClientMainPlayer.Inst.AppearanceProp.Gender) or 0
                                    CClientObject.ShowExpressionAction(self.qnModelPreviewer.m_RO, CFashionPreviewMgr.UmbrellaFashionID,  gender)
                                end
                            end
                        )
                    )
                else
                    self.qnModelPreviewer.m_RO.Visible = true
                end
            end
        )
    )
end

function LuaShopMallFashionPreviewWnd:InitModelPreviewer(isNeedResetModelScale)
    LuaShopMallFashionPreviewMgr:InitForLuozhuangSetting()
    if not self.bfirstInit then
        self.qnModelPreviewer:Init(false)
    else
        self:FixInitModelPreviewer()
    end

    LuaModelTextureLoaderMgr:SetModelCameraColor(self.qnModelPreviewer.identifier)

    if isNeedResetModelScale then
        self.localModelScale = 1
    end

    local zuoqi = ZuoQi_ZuoQi.GetData(CFashionPreviewMgr.ZuoQiFashionID)
    local d = (zuoqi and zuoqi.ShopMallMaxPreviewScale or self.maxPreviewScale) - 1
    local t = (self.localModelScale - 1) / d
    t = math.max(math.min(1, t), 0)
    local y = math.lerp(0, zuoqi and zuoqi.ShopMallMaxPreviewYOffSet or self.maxPreviewOffsetY, t)
    local pos = Vector3(self.defaultModelLocalPos.x, self.defaultModelLocalPos.y + y, self.defaultModelLocalPos.z)

    self:SetModelPosAndScale(pos, self.localModelScale)
    if not self.bfirstInit then
        self:InitShadowCameraAndLight()
    end

    if CFashionPreviewMgr.ZuoQiFashionID ~= 0 then
        local zuoqi = ZuoQi_ZuoQi.GetData(CFashionPreviewMgr.ZuoQiFashionID)
        if zuoqi then
            self.qnModelPreviewer.m_RO:AddLoadAllFinishedListener(
                DelegateFactory.Action_CRenderObject(
                    function(ro)
                        if ro.VehicleRO then
                            ro.VehicleRO:AddLoadAllFinishedListener(
                                DelegateFactory.Action_CRenderObject(
                                    function(_ro)
                                        _ro.transform.localRotation = Quaternion.Euler(zuoqi.ShopMallMaxPreviewRX, zuoqi.ShopMallMaxPreviewRY, 0)
                                    end
                                )
                            )
                        end
                    end
                )
            )
            if self.beiguangLight then
                self.beiguangLight.gameObject:SetActive(zuoqi.DisableShopMallBeiGuang == 0)
            end
            if self.floor then
                self.floor.transform.localPosition = Vector3(self.floorInitialLocalPos.x, self.floorInitialLocalPos.y + zuoqi.ShopMallFloorHeight, self.floorInitialLocalPos.z)
            end
        end
    else
        if self.floor then
            self.floor.transform.localPosition = self.floorInitialLocalPos
        end
        if self.beiguangLight then
            self.beiguangLight.gameObject:SetActive(true)
        end
    end

    if not self.bfirstInit then
        self.bfirstInit = true
    end
end

function LuaShopMallFashionPreviewWnd:SetModelPosAndScale(pos, scale)
    self.localModelScale = scale
    if scale > 0 then
        pos = Vector3(pos.x / scale, pos.y / scale, pos.z / scale)
    end
    self.localModelPos = pos
    local PreviewScale = 1
    local zuoqi = ZuoQi_ZuoQi.GetData(CFashionPreviewMgr.ZuoQiFashionID)
    if zuoqi then
        PreviewScale = zuoqi.PreviewScale
    end
    self:SetYiChuBgScenePosAndScale(PreviewScale)
    CUIManager.SetModelScale(self.qnModelPreviewer.identifier, Vector3(PreviewScale, PreviewScale, PreviewScale))
    CUIManager.SetModelPosition(self.qnModelPreviewer.identifier, pos)
    if not LuaShopMallFashionPreviewMgr.enableMultiLight then
        return
    end
    self:SetShadowCameraAndLightPos(pos)
    CMainCamera.Inst:OnPreRenderShadowmap()
end

function LuaShopMallFashionPreviewWnd:SetYiChuBgScenePosAndScale(PreviewScale)
    if self:NeedShowYiChuScene() and self.m_YiChuBgScene and PreviewScale > 0 then
        self.m_YiChuBgScene.transform.localScale = Vector3(0.5 / PreviewScale,0.5 / PreviewScale,0.5  / PreviewScale)
        self.m_YiChuBgScene.transform.localPosition = Vector3(22.83 / PreviewScale, 0, -65.85 / PreviewScale)
    end
end

function LuaShopMallFashionPreviewWnd:InitRadioBox()
    local appearData = self.qnModelPreviewer.needShowAppearance
    self.isSupportFeiSheng = CClientMainPlayer.IsFashionSupportFeiSheng(appearData.HeadFashionId, appearData.BodyFashionId, appearData.HideHeadFashionEffect, appearData.HideBodyFashionEffect)
    self.changeXianFanRadioBox:Awake()
    self.changeXianFanRadioBox.OnSelect =
        DelegateFactory.Action_QnButton_int(
        function(btn, index)
            self.m_XianFanButton2:SetSelected(index == 1, false)
        end
    )
    if CClientMainPlayer.Inst then
        self.changeXianFanRadioBox.Visible = CClientMainPlayer.Inst.HasFeiSheng
        self.m_XianFanButton2.Visible = CClientMainPlayer.Inst.HasFeiSheng
    end
    local isXianShen = appearData.FeiShengAppearanceXianFanStatus > 0 and self.isSupportFeiSheng
    self.changeXianFanRadioBox:ChangeTo(isXianShen and 1 or 0, false)
    self.m_XianFanButton2:SetSelected(isXianShen, false)
    self.m_XianFanButton2.OnButtonSelected =
        DelegateFactory.Action_bool(
        function(b)
            self.changeXianFanRadioBox:ChangeTo(b and 1 or 0, false)
            self:SetXianFan(b, appearData)
        end
    )
end

function LuaShopMallFashionPreviewWnd:SetXianFan(isXianShen, appearData)
    if isXianShen then
        if self.isSupportFeiSheng then
            appearData.FeiShengAppearanceXianFanStatus = 1
        else
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("穿戴凡身拓本时无法预览仙身外观"))
            self.changeXianFanRadioBox:ChangeTo(0, false)
        end
    elseif CClientMainPlayer.Inst then
        if CClientMainPlayer.Inst.Is150GhostBody or CClientMainPlayer.Inst.Is150GhostHead then
            g_MessageMgr:ShowMessage("Tip_FanShen_Ghost150")
        end
        appearData.FeiShengAppearanceXianFanStatus = 0
    end
    if self.m_DoExpressionButton.gameObject.activeSelf then
        self:DoExpression()
    else
        self:InitModelPreviewer()
    end
end

function LuaShopMallFashionPreviewWnd:InitTabButtons()
    local cats = LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory
    local ids = {
        cats.ZuoQi,
        cats.ShiZhuang,
        cats.Umbrella,
        cats.BackPendant
    }

    self.tabs = {}
    for i = 1, #ids do
        local category = ids[i]
        local data = Mall_LingYuMallSubCategory.GetData(category)
        local obj = self.tabButtons.transform:Find("Tab" .. i).gameObject
        local btn = obj:GetComponent(typeof(QnSelectableButton))
        table.insert(self.tabs, btn)
        btn.m_BackGround.mSpriteName = data.SpriteName
        local t = category
        btn.m_BackGround.alpha = 0.5
        btn.OnClick =
            DelegateFactory.Action_QnButton(
            function(btn)
                self:OnTabButtonClick(t)

                for _, _btn in pairs(self.tabs) do
                    if btn ~= _btn then
                        _btn:SetSelected(false, false)
                        _btn.m_BackGround.alpha = 0.5
                    end
                end
                btn:SetSelected(true, false)
                btn.m_BackGround.alpha = 1
            end
        )
        btn:SetSelected(category == self.curCategory, false)
        btn.m_BackGround.alpha = category == self.curCategory and 1 or 0.5
        obj:SetActive(true)
    end
end

function LuaShopMallFashionPreviewWnd:InitShopRoot()
    self.m_EmptyLabel = FindChildWithType(self.m_ShopRoot.transform, "EmptyLabel", typeof(UILabel))
    self.m_ShopRoot.m_DataSource =
        DefaultTableViewDataSource.Create2(
        function()
            return self:NumberOfRows()
        end,
        function(item, index)
            return self:ItemAt(nil, index)
        end
    )
    local callback =
        DelegateFactory.Action_int(
        function(row)
            self:OnSelectAtRow(row)
        end
    )
    self.m_ShopRoot.OnSelectAtRow = callback
    self:Reload()
    self.isShrinkShopRoot = false
end

function LuaShopMallFashionPreviewWnd:InitSellRoot()
    self.qnCostAndOwnMoney.m_AddMoneyButton.OnClick =
        CommonDefs.CombineListner_Action_QnButton(
        self.qnCostAndOwnMoney.m_AddMoneyButton.OnClick,
        DelegateFactory.Action_QnButton(
            function(btn)
                CUIManager.CloseUI(CUIResources.ShopMallFashionPreviewWnd)
            end
        ),
        true
    )
    self.buyButton.OnClick =
        DelegateFactory.Action_QnButton(
        function(btn)
            self:OnBuyButtonClick()
        end
    )
    self.BuySingletonShopGoodsButton.OnClick =
        DelegateFactory.Action_QnButton(
        function(btn)
            self:BuySingletonShopGoods()
        end
    )
end

function LuaShopMallFashionPreviewWnd:OnTabButtonClick(category)
    self.curCategory = category
    LuaShopMallFashionPreviewMgr.blindBoxItemId = 0
    if self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ShiZhuang then
        if LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId ~= 0 then
            LuaShopMallFashionPreviewMgr.blindBoxItemId = LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId
        elseif LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId ~= 0 then
            LuaShopMallFashionPreviewMgr.blindBoxItemId = LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId
        end
        LuaShopMallFashionPreviewMgr.blindBoxSelectIndex = LuaShopMallFashionPreviewMgr.fashionBlindBoxSelectIndex
    elseif self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ZuoQi then
        LuaShopMallFashionPreviewMgr.blindBoxItemId = LuaShopMallFashionPreviewMgr.zuoqiBlindBoxItemId
        LuaShopMallFashionPreviewMgr.blindBoxSelectIndex = LuaShopMallFashionPreviewMgr.zuoqiBlindBoxSelectIndex
    end
    self:UpdateCostMoney()
    self:Reload()
end

function LuaShopMallFashionPreviewWnd:OnBuyButtonClick()
    local costMoney = 0
    for itemID, shopMallTemlate in pairs(self.curSelectableSequence) do
        costMoney = costMoney + shopMallTemlate.Price
    end
    if costMoney > 0 then
        LuaShopMallFashionPreviewMgr:ShowTryToBuyWnd(self.curSelectableSequence)
    end
end

function LuaShopMallFashionPreviewWnd:LoadMallInfo()
    local list = CShopMallMgr.GetLingYuMallInfo(3, self.curCategory)
    self.babyFashionMallDict = {}
    Baby_Fashion.ForeachKey(
        function(key)
            local fashion = Baby_Fashion.GetData(key)
            self.babyFashionMallDict[fashion.ItemID] = true
        end
    )
    self.mallInfo = {}
    for i = 0, list.Count - 1 do
        local template = list[i]
        local templateId = template.ItemId
        if not self.babyFashionMallDict[templateId] then
            table.insert(self.mallInfo, template)
        end
    end
    for i = 1, #self.mallInfo do
        self.itemID2ShopMallItemMap[self.mallInfo[i].ItemId] = self.mallInfo[i]
    end
end

function LuaShopMallFashionPreviewWnd:Reload()
    self:LoadMallInfo()
    self.m_OwnedShopItems = LuaShopMallFashionPreviewMgr:GetOwnedShopItemsInfo()
    self.m_ShopRoot:ReloadData(true, true)
    self.scrollView:ResetPosition()
    self:UpdateCurSelectableSequence()
    local ct = self:NumberOfRows()
    if self.m_EmptyLabel then
        self.m_EmptyLabel.gameObject:SetActive(ct <= 0)
        self.m_EmptyLabel.text = LocalString.GetString("当前没有可以预览的物品")
    end
end

function LuaShopMallFashionPreviewWnd:NumberOfRows()
    return self.mallInfo and #self.mallInfo or 0
end

function LuaShopMallFashionPreviewWnd:ItemAt(view, row)
    if not self.mallInfo then
        return
    end
    local template = self.mallInfo[row + 1]
    local item = self.m_ShopRoot:GetFromPool(self.isShrinkShopRoot and 1 or 0)
    self:InitItem(item.transform, template)
    return item
end

function LuaShopMallFashionPreviewWnd:InitItem(transform, template)
    local templateId = template.ItemId
    local isOwned = self.m_OwnedShopItems[templateId]
    local showDiscount = template.Discount
    local price = template.Price
    local discountInfo
    local isPreview = self.m_ItemID2IsPreview[templateId]
    local previewTag = transform:Find("PreviewTag"):GetComponent(typeof(UISprite))
    local transformableIcon = transform:Find("Transformable").gameObject

    previewTag.gameObject:SetActive(isPreview)

    if template.Discount > 0.05 and not template.PreSell then
        if CommonDefs.IS_VN_CLIENT then
            showDiscount = showDiscount * 10
            discountInfo = String.Format(LocalString.GetString("{0:N1}折"), showDiscount)
        elseif CommonDefs.IS_KR_CLIENT then
            showDiscount = showDiscount * 10
            discountInfo = String.Format("{0:N1}%", showDiscount)
        else
            discountInfo = String.Format(LocalString.GetString("{0:N1}折"), showDiscount)
        end
    end
    transform:Find("Sprite"):GetComponent(typeof(UISprite)).spriteName = Money.GetIconName(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE)
    local item = CItemMgr.Inst:GetItemTemplate(templateId)
    local texture = transform:Find("Texture"):GetComponent(typeof(CUITexture))

    texture:LoadMaterial(item.Icon)
    local nameLabel = transform:Find("Name")
    if nameLabel then
        nameLabel:GetComponent(typeof(QnLabel)).Text = item.Name
    end
    local priceLabel = transform:Find("Price")
    if priceLabel then
        priceLabel:GetComponent(typeof(QnLabel)).Text = tostring(price)
    end
    local discountLabel = transform:Find("Discount"):GetComponent(typeof(QnLabel))
    if not String.IsNullOrEmpty(discountInfo) then
        discountLabel.Text = discountInfo
        discountLabel.gameObject:SetActive(true)
    else
        discountLabel.gameObject:SetActive(false)
    end
    local disableSprite = transform:Find("Texture/DisableSprite"):GetComponent(typeof(UISprite))
    disableSprite.enabled = false
    if IdPartition.IdIsItem(templateId) then
        disableSprite.enabled = not CItem.GetMainPlayerIsFit(templateId, true)
    elseif IdPartition.IdIsEquip(templateId) then
        disableSprite.enabled = not CEquipment.GetMainPlayerIsFit(templateId, true)
    end
    local ownerSprite = transform:Find("Owner").gameObject
    ownerSprite:SetActive(isOwned)
    transform.gameObject:SetActive(true)
    local avalibleCountLabel = transform:Find("Texture/AvalibleCount"):GetComponent(typeof(QnLabel))
    avalibleCountLabel.Text = System.String.Format(LocalString.GetString("限购{0}"), template.AvalibleCount)
    avalibleCountLabel.gameObject:SetActive(template.AvalibleCount >= 0) 
    local soldoutSprite = transform:Find("Texture/Soldout")
    soldoutSprite.gameObject:SetActive(template.AvalibleCount == 0)

    transformableIcon:SetActive(CLuaShopMallMgr:IsShowTransformableFashionIcon(templateId))
end

function LuaShopMallFashionPreviewWnd:RefreshAllOwnerItemInfo()
    --self:Reload()
    self.m_OwnedShopItems = LuaShopMallFashionPreviewMgr:GetOwnedShopItemsInfo()

    for i = 0, self.m_ShopRoot.m_ItemList.Count - 1 do
        local item = self.m_ShopRoot.m_ItemList[i]
        local mall = self.mallInfo[item.Row + 1]
        local id = mall.ItemId
        local isOwned = self.m_OwnedShopItems[id]
        local ownerSprite = item.transform:Find("Owner").gameObject
        ownerSprite:SetActive(isOwned)
    end
end

function LuaShopMallFashionPreviewWnd:OnSelectAtRow(row)
    local template = self.mallInfo[row + 1]
    local templateId = template.ItemId
    self:OnItemClick(nil, templateId)
    self:UpdateCurSelectableSequence()
    self:InitTransformButton()
    self:InitDoExpressionButton()
    self:InitHeadAlpha()
end

function LuaShopMallFashionPreviewWnd:OnItemClick(btn, templateId)
    local _item = CItemMgr.Inst:GetItemTemplate(templateId)
    if _item.Type == EnumItemType_lua.Fashion then
        self:PreviewFashion(_item.ID)
    elseif _item.Type == EnumItemType_lua.Mount then
        self:PreviewZuoQi(_item.ID)
    elseif _item.Type == EnumItemType_lua.Other then
        self:PreviewKaiSan(_item.ID)
    elseif _item.Type == EnumItemType_lua.Chest then
        self:PreviewBlindBox(_item.ID)
    end

    self.changeXianFanRadioBox.Visible = CClientMainPlayer.Inst.HasFeiSheng and _item.Type ~= EnumItemType_lua.Chest
end

function LuaShopMallFashionPreviewWnd:PreviewFashion(id, blindBoxItemId, notInitModelPreviewer)
    local data = LuaShopMallFashionPreviewMgr:GetBlindBoxFashionData(id)
    if data then
        local fashionId = data.ID
        if fashionId ~= 0 then
            local fashion = Fashion_Fashion.GetData(fashionId)
            if fashion then
                local sexualityIsFit = true
                if fashion.GenderLimit > 0 then
                    if fashion.GenderLimit == 1 and CClientMainPlayer.Inst.AppearanceProp.Gender ~= EnumGender.Male then
                        sexualityIsFit = false
                    elseif fashion.GenderLimit == 2 and CClientMainPlayer.Inst.AppearanceProp.Gender ~= EnumGender.Female then
                        sexualityIsFit = false
                    end
                end
                if not sexualityIsFit then
                    g_MessageMgr:ShowMessage("Fashion_Gender_Limit_Cannot_TakeOn")
                    return
                end
            end
            CFashionPreviewMgr.curShopMallPreviewType = EnumShopMallFashionPreviewType.PreviewAllFashion
            local pos = Fashion_Fashion.GetData(fashionId).Position
            if pos == 1 then
                LuaShopMallFashionPreviewMgr:SetBodyFashion(fashionId, blindBoxItemId and blindBoxItemId or 0)
            elseif pos == 0 then
                LuaShopMallFashionPreviewMgr:SetHeadFashion(fashionId, blindBoxItemId and blindBoxItemId or 0)
            elseif pos == 3 then
                LuaShopMallFashionPreviewMgr:SetBackPendantFashion(fashionId, 0)
            end
            if notInitModelPreviewer then
                return
            end
            self:InitModelPreviewer()
        end
    end
end

function LuaShopMallFashionPreviewWnd:PreviewZuoQi(id, blindBoxItemId, notInitModelPreviewer)
    local data = ZuoQi_ZuoQi.GetDataBySubKey("ItemID", id)
    if data then
        local vehicleId = data.ID
        if vehicleId ~= 0 then
            CFashionPreviewMgr.curShopMallPreviewType = EnumShopMallFashionPreviewType.PreviewAllFashion
            LuaShopMallFashionPreviewMgr:SetZuoQiFashion(vehicleId, blindBoxItemId and blindBoxItemId or 0)
            self.m_ZuoQiTransformAniBtn.gameObject:SetActive(CZuoQiMgr.IsTransformZuoQi(CFashionPreviewMgr.ZuoQiFashionID))
            self.m_LeftBottomBtnGrid:Reposition()
            if notInitModelPreviewer then
                return
            end
            self:InitModelPreviewer(true)
        end
    end
end

function LuaShopMallFashionPreviewWnd:PreviewKaiSan(id)
    local hasFound = false
    local expression_Appearance_PreviewDefine
    Expression_Appearance.Foreach(
        function(key, data)
            if data.Group == LuaShopMallFashionPreviewMgr.KaiSanAppearanceGroup then
                if data.ItemId == id and not hasFound then
                    expression_Appearance_PreviewDefine = data.PreviewDefine
                    hasFound = true
                    CFashionPreviewMgr.curShopMallPreviewType = EnumShopMallFashionPreviewType.PreviewAllFashion
                    LuaShopMallFashionPreviewMgr:SetUmbrellaFashion(expression_Appearance_PreviewDefine)
                    self:InitModelPreviewer()
                end
            end
        end
    )
end

function LuaShopMallFashionPreviewWnd:PreviewBlindBox(id)
    if self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ShiZhuang then
        LuaShopMallFashionPreviewMgr.recentlyChooseFashion = nil
    elseif self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ZuoQi then
        CFashionPreviewMgr.ZuoQiFashionID = 0
    end

    local previewChestData = Item_PreviewChest.GetData(id)
    if previewChestData then
        LuaShopMallFashionPreviewMgr:SetPreviewChest(previewChestData)
        self:InitModelPreviewer()
        return
    end
    
    local blindBoxData = Item_BlindBox.GetData(id)
    local vehicleId, pos2fashionIdMap, itemIds = LuaShopMallFashionPreviewMgr:RandomGetBlindBoxResult(id, blindBoxData)
    self:PreviewBlindBoxWithVehicleIdAndFashionId(itemIds, vehicleId, pos2fashionIdMap, id)
end

function LuaShopMallFashionPreviewWnd:PreviewBlindBoxWithVehicleIdAndFashionId(itemIds, vehicleId, pos2fashionIdMap, blindBoxItemId)
    self.m_BlindBoxResultItemIds = itemIds
    for i = 0, itemIds.Length - 1 do
        local itemId = itemIds[i]
        if vehicleId ~= 0 then
            self:PreviewZuoQi(itemId, blindBoxItemId, true)
        else
            self:PreviewFashion(itemId, blindBoxItemId, true)
        end
        local blindBoxData = Item_BlindBox.GetData(itemId)
        if blindBoxData then
            print("blindBoxData", blindBoxData, blindBoxData.Names[LuaShopMallFashionPreviewMgr.blindBoxSelectIndex])

            self.m_BlindBoxNameLabel.text = blindBoxData.Names[LuaShopMallFashionPreviewMgr.blindBoxSelectIndex]
        end
    end
    self:InitModelPreviewer()
end

function LuaShopMallFashionPreviewWnd:UpdateCostMoney()
    self.curCostMoney = 0

    local itemId, b_canBuy, previewUrl = self:GetCurSelectSingletonShopGoods()
    self.m_CgName = previewUrl
    self.OpenUrlButton.gameObject:SetActive(not CommonDefs.IS_HMT_CLIENT and not CommonDefs.IS_VN_CLIENT and self.m_CgName and not cs_string.IsNullOrEmpty(self.m_CgName))
    if b_canBuy then
        local Item = self.curSelectableSequence[itemId]
        if Item then
            self.curCostMoney = Item.Price
            self.qnCostAndOwnMoney:SetType(Item.CanUseBindJade > 0 and EnumMoneyType.LingYu_WithBind or EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
        end
    end
    self.qnCostAndOwnMoney:SetCost(self.curCostMoney)
    self.m_LeftBottomBtnGrid:Reposition()
end

function LuaShopMallFashionPreviewWnd:UpdateCurSelectableSequence()
    self.curSelectableSequence = {}
    local headpreviewChestId = nil
    local headFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.HeadFashionID)
    if headFashion then
        self.curSelectableSequence[headFashion.ItemID] = self.itemID2ShopMallItemMap[headFashion.ItemID]
        headpreviewChestId = LuaShopMallFashionPreviewMgr.m_FashionItemId2PreviewChestId[headFashion.ItemID]
        if headpreviewChestId and LuaShopMallFashionPreviewMgr.m_PreviewChestId ~= 0 then
            self.curSelectableSequence[headFashion.ItemID] = nil
        end
    end
    local bodypreviewChestId = nil
    local bodyFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.BodyFashionID)
    if bodyFashion then
        self.curSelectableSequence[bodyFashion.ItemID] = self.itemID2ShopMallItemMap[bodyFashion.ItemID]
        bodypreviewChestId = LuaShopMallFashionPreviewMgr.m_FashionItemId2PreviewChestId[bodyFashion.ItemID]
        if bodypreviewChestId and LuaShopMallFashionPreviewMgr.m_PreviewChestId ~= 0 then
            self.curSelectableSequence[bodyFashion.ItemID] = nil
            self.curSelectableSequence[bodypreviewChestId] = self.itemID2ShopMallItemMap[bodypreviewChestId]
        end
    end

    local backpendantFashion = Fashion_Fashion.GetData(LuaShopMallFashionPreviewMgr.backpendantFashionId)
    if backpendantFashion then
        self.curSelectableSequence[backpendantFashion.ItemID] = self.itemID2ShopMallItemMap[backpendantFashion.ItemID]
    end

    local foundExpressionAppearanceKey = nil
    Expression_Appearance.Foreach(
        function(key, data)
            if foundExpressionAppearanceKey == nil and data.Group == LuaShopMallFashionPreviewMgr.KaiSanAppearanceGroup and CFashionPreviewMgr.UmbrellaFashionID == data.PreviewDefine then
                foundExpressionAppearanceKey = data.ItemId
                self.curSelectableSequence[foundExpressionAppearanceKey] = self.itemID2ShopMallItemMap[foundExpressionAppearanceKey]
            end
        end
    )

    local zuoqi = ZuoQi_ZuoQi.GetData(CFashionPreviewMgr.ZuoQiFashionID)
    if zuoqi then
        self.curSelectableSequence[zuoqi.ItemID] = self.itemID2ShopMallItemMap[zuoqi.ItemID]
    end
    local blindBoxData = Item_BlindBox.GetData(LuaShopMallFashionPreviewMgr.blindBoxItemId)
    if blindBoxData then
        self.curSelectableSequence[LuaShopMallFashionPreviewMgr.blindBoxItemId] = self.itemID2ShopMallItemMap[LuaShopMallFashionPreviewMgr.blindBoxItemId]
    end
    self.curSelectableSequence[LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId] = self.itemID2ShopMallItemMap[LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId]
    self.curSelectableSequence[LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId] = self.itemID2ShopMallItemMap[LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId]
    self.curSelectableSequence[LuaShopMallFashionPreviewMgr.zuoqiBlindBoxItemId] = self.itemID2ShopMallItemMap[LuaShopMallFashionPreviewMgr.zuoqiBlindBoxItemId]
    local selectId = self:GetCurSelectSingletonShopGoods()
    for index, template in pairs(self.mallInfo) do
        local id = template.ItemId
        local isSelected, isPreview = false, false
        if self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ShiZhuang then
            if LuaShopMallFashionPreviewMgr.recentlyChooseFashion and id == LuaShopMallFashionPreviewMgr.recentlyChooseFashion.ItemID and LuaShopMallFashionPreviewMgr.m_PreviewChestId == 0 then
                isSelected = true
            end
            if (LuaShopMallFashionPreviewMgr.m_PreviewChestId == 0) and ((headFashion and (id == headFashion.ItemID)) or (bodyFashion and (id == bodyFashion.ItemID))) then
                isPreview = true
            end
            if id == LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId or id == LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId then
                isPreview = true
            end
            if headpreviewChestId == id and bodypreviewChestId == id and LuaShopMallFashionPreviewMgr.m_PreviewChestId == id then
                isSelected = true
                isPreview = true
            end
        elseif self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ZuoQi then
            if zuoqi and (id == zuoqi.ItemID) then
                isSelected = true
                isPreview = true
            end
            if id == LuaShopMallFashionPreviewMgr.zuoqiBlindBoxItemId then
                isSelected = true
                isPreview = true
            end
        elseif self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.BackPendant then
            if backpendantFashion and (id == backpendantFashion.ItemID) then
                isSelected = true
                isPreview = true
            end
        elseif self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.Umbrella then
            if foundExpressionAppearanceKey and (id == foundExpressionAppearanceKey) then
                isSelected = true
                isPreview = true
            end
        end
        self.m_ItemID2IsPreview[tonumber(id)] = isPreview
        if isSelected then
            selectId = id
        end
    end
    for i = 0, self.m_ShopRoot.m_ItemList.Count - 1 do
        local item = self.m_ShopRoot.m_ItemList[i]
        local template = self.mallInfo[item.Row + 1]
        local id = template.ItemId
        local isPreview = self.m_ItemID2IsPreview[tonumber(id)]
        item.transform:Find("PreviewTag").gameObject:SetActive(isPreview)
        item.transform:GetComponent(typeof(QnTableItem)).m_BackGround.spriteName = id == selectId and "common_btn_07_highlight" or "common_btn_07_normal"
    end
    self:UpdateCostMoney()
    g_ScriptEvent:BroadcastInLua("ShopMallFashionPreviewWnd_UpdateDescription", {selectId, self.curCategory})
    self:InitBlindBoxView(selectId)
end

function LuaShopMallFashionPreviewWnd:InitBlindBoxView(selectId)
    self.m_BlindBoxView:SetActive(selectId ~= 0 and selectId == LuaShopMallFashionPreviewMgr.blindBoxItemId)
    self.changeXianFanRadioBox.Visible = CClientMainPlayer.Inst.HasFeiSheng and (LuaShopMallFashionPreviewMgr.blindBoxItemId == 0 or selectId ~= LuaShopMallFashionPreviewMgr.blindBoxItemId)
    if selectId == LuaShopMallFashionPreviewMgr.blindBoxItemId then
        local blindBoxData = Item_BlindBox.GetData(selectId)
        if not blindBoxData then
            return
        end
        self.m_BlindBoxResultItemIds = blindBoxData.List[LuaShopMallFashionPreviewMgr.blindBoxSelectIndex]
        self.m_BlindBoxNameLabel.text = blindBoxData.Names[LuaShopMallFashionPreviewMgr.blindBoxSelectIndex]
        Extensions.RemoveAllChildren(self.m_BlindBoxViewTable.transform)
        for i = 0, blindBoxData.List.Length - 1 do
            local go = NGUITools.AddChild(self.m_BlindBoxViewTable.gameObject, self.m_BlindBoxSpriteTemplate)
            go:SetActive(true)
            go:GetComponent(typeof(UISprite)).enabled = i == LuaShopMallFashionPreviewMgr.blindBoxSelectIndex
        end
        self.m_BlindBoxViewTable:Reposition()
        g_ScriptEvent:BroadcastInLua("ShopMallFashionPreviewWnd_UpdateDescription", {selectId, self.curCategory})
    end
end

function LuaShopMallFashionPreviewWnd:OnRestButtonClick()
    self:SetModelPosAndScale(self.defaultModelLocalPos, 1)
end

function LuaShopMallFashionPreviewWnd:OnTakeOffButtonClick()
    CFashionPreviewMgr.curShopMallPreviewType = EnumShopMallFashionPreviewType.TakeOffAlllFashion
    LuaShopMallFashionPreviewMgr:OnTakeOff()
    self:InitModelPreviewer()
    LuaShopMallFashionPreviewMgr.recentlyChooseFashion = nil
    g_ScriptEvent:BroadcastInLua("ShopMallFashionPreviewWnd_UpdateDescription", {nil, self.curCategory})
    for index, template in pairs(self.mallInfo) do
        self.m_ShopRoot:SetSelectRow(index - 1, false)
    end
    self:UpdateCurSelectableSequence()
    self.m_ZuoQiTransformAniBtn.gameObject:SetActive(CZuoQiMgr.IsTransformZuoQi(CFashionPreviewMgr.ZuoQiFashionID))
    self:InitDoExpressionButton()
end

function LuaShopMallFashionPreviewWnd:OnZuoQiTransformAniBtnClick()
    local zuoqitId = CFashionPreviewMgr.ZuoQiFashionID
    local data = ZuoQi_Transformers.GetData(zuoqitId)
    local data2 = ZuoQi_Transformers.GetDataBySubKey("TransformID", zuoqitId)
    if data then
        CFashionPreviewMgr.ZuoQiFashionID = data.TransformID   
    elseif data2 then
        CFashionPreviewMgr.ZuoQiFashionID = data2.ID
    end
    
    if(CFashionPreviewMgr.ZuoQiFashionID == 0) then
        return
    end
    CZuoQiMgr.ProcessZuoQiTransformAni(self.qnModelPreviewer.m_RO, CFashionPreviewMgr.ZuoQiFashionID)
    CClientObject.GetInVehicleAsDriver(self.qnModelPreviewer.m_RO, CFashionPreviewMgr.ZuoQiFashionID, true, nil, false, 0)
    local zuoqiData = ZuoQi_ZuoQi.GetData(CFashionPreviewMgr.ZuoQiFashionID)
    if zuoqiData then
        local itemId = zuoqiData.ItemID
        g_ScriptEvent:BroadcastInLua("ShopMallFashionPreviewWnd_UpdateDescription", {itemId , self.curCategory})
    end
end

function LuaShopMallFashionPreviewWnd:GetCurSelectSingletonShopGoods()
    local itemId, b_canBuy, previewUrl
    if self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ZuoQi then
        --坐骑
        local zuoqi = ZuoQi_ZuoQi.GetData(CFashionPreviewMgr.ZuoQiFashionID)
        if zuoqi then
            itemId = zuoqi.ItemID
            previewUrl = zuoqi.PreviewVideoUrl
            b_canBuy = true
        end
        if LuaShopMallFashionPreviewMgr.zuoqiBlindBoxItemId ~= 0 then
            itemId = LuaShopMallFashionPreviewMgr.zuoqiBlindBoxItemId
            b_canBuy = true
            local blindBoxData = Item_BlindBox.GetData(itemId)
            if blindBoxData then
                previewUrl = blindBoxData.PreviewVideoUrl
            end
        end
    elseif self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.BackPendant then
        local beishi = Fashion_Fashion.GetData(LuaShopMallFashionPreviewMgr.backpendantFashionId)
        if beishi then
            itemId = beishi.ItemID
            previewUrl = beishi.PreviewVideoUrl
            b_canBuy = true
        end
    elseif self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ShiZhuang then
        --时装
        local blindBoxItemId = 0
        if CFashionPreviewMgr.HeadFashionID == 0 then
            local bodyFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.BodyFashionID)
            if bodyFashion then
                itemId = bodyFashion.ItemID
                previewUrl = bodyFashion.PreviewVideoUrl
                b_canBuy = true
            end
            if LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId ~= 0 then
                blindBoxItemId = LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId
            end
        elseif CFashionPreviewMgr.BodyFashionID == 0 then
            local headFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.HeadFashionID)
            if headFashion then
                itemId = headFashion.ItemID
                previewUrl = headFashion.PreviewVideoUrl
                b_canBuy = true
            end
            if LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId ~= 0 then
                blindBoxItemId = LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId
            end
        elseif LuaShopMallFashionPreviewMgr.recentlyChooseFashion then
            itemId = LuaShopMallFashionPreviewMgr.recentlyChooseFashion.ItemID
            previewUrl = LuaShopMallFashionPreviewMgr.recentlyChooseFashion.PreviewVideoUrl
            b_canBuy = true
            if LuaShopMallFashionPreviewMgr.recentlyChooseFashion.Position == 0 and LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId ~= 0 then
                blindBoxItemId = LuaShopMallFashionPreviewMgr.headfashionBlindBoxItemId
            elseif LuaShopMallFashionPreviewMgr.recentlyChooseFashion.Position == 1 and LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId ~= 0 then
                blindBoxItemId = LuaShopMallFashionPreviewMgr.bodyfashionBlindBoxItemId
            end
        end
        if blindBoxItemId ~= 0 then
            itemId = blindBoxItemId
            b_canBuy = true
            local blindBoxData = Item_BlindBox.GetData(itemId)
            if blindBoxData then
                previewUrl = blindBoxData.PreviewVideoUrl
            end
        end
        if LuaShopMallFashionPreviewMgr.m_PreviewChestId ~= 0 then
            itemId = LuaShopMallFashionPreviewMgr.m_PreviewChestId
            b_canBuy = true
            local previewChestData = Item_PreviewChest.GetData(itemId)
            if previewChestData then
                previewUrl = previewChestData.PreviewVideoUrl
            end
        end
    elseif self.curCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.Umbrella then
        --伞
        local foundExpressionAppearanceKey = nil
        Expression_Appearance.Foreach(
            function(key, data)
                if foundExpressionAppearanceKey == nil and data.Group == LuaShopMallFashionPreviewMgr.KaiSanAppearanceGroup and CFashionPreviewMgr.UmbrellaFashionID == data.PreviewDefine then
                    itemId = data.ItemId
                    foundExpressionAppearanceKey = itemId
                    b_canBuy = true
                end
            end
        )
    end
    return itemId, b_canBuy, previewUrl
end

function LuaShopMallFashionPreviewWnd:BuySingletonShopGoods()
    local itemId, b_canBuy = self:GetCurSelectSingletonShopGoods()
    if b_canBuy then
        local item = self.curSelectableSequence[itemId]
        LuaShopMallFashionPreviewMgr:BuyMallItem(item, item.AvalibleCount >= 0 and EShopMallRegion_lua.ELingyuMallLimit or EShopMallRegion_lua.ELingyuMall, 1)
    else
        local msgTable = {
            [1] = LocalString.GetString("坐骑"),
            [2] = LocalString.GetString("时装"),
            [3] = LocalString.GetString("伞面"),
            [10] = LocalString.GetString("背饰"),
        }
        g_MessageMgr:ShowMessage("ShopMallFashionPreviewWnd_Verify_ShopMallGoods_IsSelected", msgTable[self.curCategory])
    end
end

function LuaShopMallFashionPreviewWnd:OnPinchIn(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0), 1)
    self:OnPinch(-pinchScale)
end

function LuaShopMallFashionPreviewWnd:OnPinchOut(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0), 1)
    self:OnPinch(pinchScale)
end

function LuaShopMallFashionPreviewWnd:OnMouseScrollWheel()
    local v = Input.GetAxis("Mouse ScrollWheel")
    self:OnPinch(v)
end

function LuaShopMallFashionPreviewWnd:OnPinch(delta)
    local zuoqi = ZuoQi_ZuoQi.GetData(CFashionPreviewMgr.ZuoQiFashionID)
    local d = (zuoqi and zuoqi.ShopMallMaxPreviewScale or self.maxPreviewScale) - 1
    local t = (self.localModelScale - 1) / d

    t = t + delta

    t = math.max(math.min(1, t), 0)
    local s = math.lerp(1, zuoqi and zuoqi.ShopMallMaxPreviewScale or self.maxPreviewScale, t)
    local y = math.lerp(0, zuoqi and zuoqi.ShopMallMaxPreviewYOffSet or self.maxPreviewOffsetY, t)
    local pos = Vector3(self.defaultModelLocalPos.x, self.defaultModelLocalPos.y + y, self.defaultModelLocalPos.z)
    self:SetModelPosAndScale(pos, s)
end

function LuaShopMallFashionPreviewWnd:InitShadowCameraAndLight()
    if not LuaShopMallFashionPreviewMgr.enableMultiLight then
        return
    end
    local t = CUIManager.instance.transform:Find(self.qnModelPreviewer.identifier)
    if not t then
        return
    end
    self.shadowCamera = t:Find("UI Model Shadow Camera")
    if not self.shadowCamera then
        return
    end
    self.shadowCameraRelativePos = {
        self.localModelPos.x - self.shadowCamera.localPosition.x,
        self.localModelPos.y - self.shadowCamera.localPosition.y,
        self.localModelPos.z - self.shadowCamera.localPosition.z
    }
    self:InitScene(t)
end

function LuaShopMallFashionPreviewWnd:NeedShowYiChuScene()
    return true
end

function LuaShopMallFashionPreviewWnd:InitScene(roRoot)
    if self:NeedShowYiChuScene() then
        CSharpResourceLoader.Inst:LoadGameObject("Levels/Dynamic/yichu_prefab_night.prefab", DelegateFactory.Action_GameObject(function(obj)
            if roRoot then
                local go = GameObject.Instantiate(obj, Vector3.zero, Quaternion.identity)
                local modelRoot = roRoot.transform:Find("ModelRoot")
                if go and modelRoot then
                    self.m_YiChuBgScene = go
                    NGUITools.SetLayer(go, LayerDefine.ModelForNGUI_3D)
                    go.transform.parent = modelRoot.transform
                    local light = go.transform:Find("yichu_night")
                    if light then light.gameObject:SetActive(false) end
                    local PreviewScale = 1
                    local zuoqi = ZuoQi_ZuoQi.GetData(CFashionPreviewMgr.ZuoQiFashionID)
                    if zuoqi then
                        PreviewScale = zuoqi.PreviewScale
                    end
                    self:SetYiChuBgScenePosAndScale(PreviewScale)
                    go.transform.localRotation = Quaternion.identity
                else
                    GameObject.Destroy(go)
                end
            end
        end))
    else
        local newRO = CRenderObject.CreateRenderObject(roRoot.gameObject, "LightAndFloorAndWall")
        local fx =
            CEffectMgr.Inst:AddObjectFX(
            88801543,
            newRO,
            0,
            1,
            1,
            nil,
            false,
            EnumWarnFXType.None,
            Vector3.zero,
            Vector3.zero,
            DelegateFactory.Action_GameObject(
                function(go)
                    NGUITools.SetLayer(newRO.gameObject, LayerDefine.Effect_3D)
                    self.floor = go.transform:Find("Anchor/beijing")
                    self.floorInitialLocalPos = self.floor.transform.localPosition
                    self.beiguangLight = go.transform:Find("Anchor/beiguang")
                end
            )
        )
        self.lightAndFloorAndWall = newRO.transform
        self.lightAndFloorAndWallRelativePos = {
            self.localModelPos.x - self.lightAndFloorAndWall.localPosition.x,
            self.localModelPos.y - self.lightAndFloorAndWall.localPosition.y,
            self.localModelPos.z - self.lightAndFloorAndWall.localPosition.z
        }
        newRO:AddFX("LightAndFloorAndWall", fx, -1)
    end
end

function LuaShopMallFashionPreviewWnd:SetShadowCameraAndLightPos(pos)
    if not LuaShopMallFashionPreviewMgr.enableMultiLight then
        return
    end
    if not self.shadowCamera then
        return
    end
    if (not self.shadowCameraRelativePos) or (#self.shadowCameraRelativePos ~= 3) then
        return
    end
    self.shadowCamera.transform.localPosition = Vector3(pos.x - self.shadowCameraRelativePos[1], pos.y - self.shadowCameraRelativePos[2], pos.z - self.shadowCameraRelativePos[3])
    if not self:NeedShowYiChuScene() then 
        self.lightAndFloorAndWall.transform.localPosition =
            Vector3(pos.x - self.lightAndFloorAndWallRelativePos[1], pos.y - self.lightAndFloorAndWallRelativePos[2], pos.z - self.lightAndFloorAndWallRelativePos[3])
    end
end

function LuaShopMallFashionPreviewWnd:GetModelRoot()
    if not self.modelRoot then
        local t = CUIManager.instance.transform:Find(self.qnModelPreviewer.identifier)
        if t then
            self.modelRoot = t:Find("ModelRoot")
        end
    end
    return self.modelRoot
end

function LuaShopMallFashionPreviewWnd:OnSyncRepertoryForMallPreviewDone()
    self:RefreshAllOwnerItemInfo()
end

function LuaShopMallFashionPreviewWnd:InitTransformButton()
    if IsTransformableFashionOpen() and CFashionPreviewMgr.UmbrellaFashionID == 0 then
        local isTransformable = LuaShopMallFashionPreviewMgr:IsTransformable()
        self.m_TransformButton.gameObject:SetActive(isTransformable)
        if not isTransformable then
            LuaShopMallFashionPreviewMgr.isTransformed = false
        end
    else
        local changeState = LuaShopMallFashionPreviewMgr.isTransformed ~= false
        self.m_TransformButton.gameObject:SetActive(false)
        LuaShopMallFashionPreviewMgr.isTransformed = false
        if changeState then
            self:InitModelPreviewer()
        end
    end
    self.m_ZuoQiTransformAniBtn.gameObject:SetActive(CZuoQiMgr.IsTransformZuoQi(CFashionPreviewMgr.ZuoQiFashionID))
    self:UpdateTransformState()
    self.m_LeftBottomBtnGrid:Reposition()
end

function LuaShopMallFashionPreviewWnd:Transform()
    LuaShopMallFashionPreviewMgr.isTransformed = not LuaShopMallFashionPreviewMgr.isTransformed
    self:UpdateTransformState()

    self:InitModelPreviewer()
    self:InitDoExpressionButton()
end

function LuaShopMallFashionPreviewWnd:UpdateTransformState()
    if LuaShopMallFashionPreviewMgr.isTransformed then
        self.m_TransformButton.Text = LocalString.GetString("褪形")
        self.m_TransformHighlight:SetActive(true)
    else
        self.m_TransformButton.Text = LocalString.GetString("化形")
        self.m_TransformHighlight:SetActive(false)
    end
end

function LuaShopMallFashionPreviewWnd:DoExpression()
    if CClientMainPlayer.Inst == nil then
        return
    end
    if not self.m_DoExpressionButton.gameObject.activeSelf then
        return
    end
    if CFashionPreviewMgr.ZuoQiFashionID ~= 0 then
        CFashionPreviewMgr.ZuoQiFashionID = 0
    end
    if CFashionPreviewMgr.UmbrellaFashionID ~= 0 then
        CFashionPreviewMgr.UmbrellaFashionID = 0
    end
    self.m_ZuoQiTransformAniBtn.gameObject:SetActive(CZuoQiMgr.IsTransformZuoQi(CFashionPreviewMgr.ZuoQiFashionID))
    self.m_LeftBottomBtnGrid:Reposition()
    self:UpdateCurSelectableSequence()
    local appearData = self.qnModelPreviewer.needShowAppearance
    local headFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.HeadFashionID ~= 0 and CFashionPreviewMgr.HeadFashionID or appearData.HeadFashionId)
    if headFashion == nil then
        return
    end
    local time = CClientMainPlayer.Inst:GetExpressionAnimationTime(headFashion.SpecialExpression) 
    local progress = self.m_DoExpressionButton.transform:Find("Progress"):GetComponent(typeof(UIProgressBar))
    progress.gameObject:SetActive(true)
    LuaTweenUtils.DOKill(progress.transform, false)
    local tween = LuaTweenUtils.TweenFloat(0, 1, time, function ( val )
        progress.value = val
    end,function ()
        progress.gameObject:SetActive(false)
    end)
    LuaTweenUtils.SetTarget(tween,progress.transform)
    self:InitModelPreviewer(true)
    local expressionId = self.m_FashionTransformSpecialExpressionData and self.m_FashionTransformSpecialExpressionData.ExpressionId or headFashion.SpecialExpression
    CClientMainPlayer.ShowExpressionAction(self.qnModelPreviewer.m_RO,CClientMainPlayer.Inst.Gender,expressionId)
end

function LuaShopMallFashionPreviewWnd:InitDoExpressionButton()
    self.m_DoExpressionButton.gameObject:SetActive(false)
    local appearData = self.qnModelPreviewer.needShowAppearance
    local headFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.HeadFashionID ~= 0 and CFashionPreviewMgr.HeadFashionID or appearData.HeadFashionId)
    local bodyFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.BodyFashionID ~= 0 and CFashionPreviewMgr.BodyFashionID or appearData.BodyFashionId)
    local showBtn = false
    self.m_FashionTransformSpecialExpressionData = nil
    if CClientMainPlayer.Inst.AppearanceProp.Gender ~= EnumGender.Monster and headFashion and bodyFashion and headFashion.SpecialExpression ~= 0 and headFashion.SpecialExpression == bodyFashion.SpecialExpression then
        showBtn = true
    elseif LuaShopMallFashionPreviewMgr.isTransformed and self.qnModelPreviewer.m_FashionPreviewAppearanceData then
        local monsterId = self.qnModelPreviewer.m_FashionPreviewAppearanceData.ResourceId
        self.m_FashionTransformSpecialExpressionData = Fashion_TransformSpecialExpression.GetData(monsterId)
        showBtn = self.m_FashionTransformSpecialExpressionData ~= nil
    elseif not LuaShopMallFashionPreviewMgr.isTransformed and CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsTransformOn and CFashionPreviewMgr.BodyFashionID == 0 and CFashionPreviewMgr.HeadFashionID == 0 then
        local monsterId = CClientMainPlayer.Inst.AppearanceProp.ResourceId
        self.m_FashionTransformSpecialExpressionData = Fashion_TransformSpecialExpression.GetData(monsterId)
        showBtn = self.m_FashionTransformSpecialExpressionData ~= nil
    end
    if showBtn then
        self.m_DoExpressionButton.gameObject:SetActive(true)
        local progress = self.m_DoExpressionButton.transform:Find("Progress"):GetComponent(typeof(UIProgressBar))
        progress.gameObject:SetActive(false)
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.FashionPreviewWndDoExpressionButton)
    end
    self.m_LeftBottomBtnGrid:Reposition()
end

function LuaShopMallFashionPreviewWnd:GetGuideGo(methodName)
    if methodName == "GetDoExpressionButton" then
        return self.m_DoExpressionButton.gameObject
    end
    return nil
end