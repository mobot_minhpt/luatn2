-- Auto Generated!!
local CSnowBallTopRightWnd = import "L10.UI.CSnowBallTopRightWnd"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local LocalString = import "LocalString"
local Vector3 = import "UnityEngine.Vector3"
CSnowBallTopRightWnd.m_OnExpandBtnClicked_CS2LuaHook = function (this, go) 
    local rotation = this.expandBtn.transform.localEulerAngles
    this.expandBtn.transform.localEulerAngles = Vector3(rotation.x, rotation.y, 180)

    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end
CSnowBallTopRightWnd.m_CalEliteMonsterTime_CS2LuaHook = function (this) 
    if this.leftTime <= 0 then
        this.timeLabel.text = ""
    else
        local min = math.floor(this.leftTime / 60)
        local sec = math.ceil(this.leftTime - min * 60)
        this.timeLabel.text = System.String.Format(LocalString.GetString("{0}:{1}后出现冰原祭司"), min, sec)
    end
end
CSnowBallTopRightWnd.m_CalNextRunTime_CS2LuaHook = function (this) 
    if this.nextRunTime <= 0 then
        this.nextRunTimeLabel.gameObject:SetActive(false)
        this.nextRunTimeLabel.text = ""
    else
        this.nextRunTimeLabel.gameObject:SetActive(true)
        local min = math.floor(this.nextRunTime / 60)
        local sec = math.ceil(this.nextRunTime - min * 60)
        this.nextRunTimeLabel.text = System.String.Format(LocalString.GetString("{0}:{1}后暴风雪区域扩大"), min, sec)
    end
end
