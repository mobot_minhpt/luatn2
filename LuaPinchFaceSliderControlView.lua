local GameObject = import "UnityEngine.GameObject"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UITable = import "UITable"
local EnumGender = import "L10.Game.EnumGender"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local ColorExt = import "L10.Game.ColorExt"
local QnTipButton = import "L10.UI.QnTipButton"
local Animation = import "UnityEngine.Animation"
local EmDisplayType = import "L10.Engine.CFacialAsset+EmDisplayType"
local QnCheckBoxGroup = import "L10.UI.QnCheckBoxGroup"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"

LuaPinchFaceSliderControlView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPinchFaceSliderControlView, "RightScrollView", "RightScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaPinchFaceSliderControlView, "RightTable", "RightTable", UITable)
RegistChildComponent(LuaPinchFaceSliderControlView, "TwoDimenSlider", "TwoDimenSlider", GameObject)
RegistChildComponent(LuaPinchFaceSliderControlView, "OneDimenSlider", "OneDimenSlider", GameObject)
RegistChildComponent(LuaPinchFaceSliderControlView, "CustomColorSlider", "CustomColorSlider", GameObject)
RegistChildComponent(LuaPinchFaceSliderControlView, "ColorSelectionTemplate", "ColorSelectionTemplate", GameObject)
RegistChildComponent(LuaPinchFaceSliderControlView, "CustomColorBtnTemplate", "CustomColorBtnTemplate", GameObject)
RegistChildComponent(LuaPinchFaceSliderControlView, "MainViewAnchor", "MainViewAnchor", GameObject)
RegistChildComponent(LuaPinchFaceSliderControlView, "FinishedCustomColorBtn", "FinishedCustomColorBtn", GameObject)
RegistChildComponent(LuaPinchFaceSliderControlView, "SpecialColorSelectionTemplate", "SpecialColorSelectionTemplate", GameObject)
RegistChildComponent(LuaPinchFaceSliderControlView, "QnCheckBoxGroupTemplate", "QnCheckBoxGroupTemplate", GameObject)
RegistChildComponent(LuaPinchFaceSliderControlView, "HairColorModifyTemplate", "HairColorModifyTemplate", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaPinchFaceSliderControlView, "m_FaceOneDimenSliderMap")
RegistClassMember(LuaPinchFaceSliderControlView, "m_FaceTwoDimenSliderMap")
RegistClassMember(LuaPinchFaceSliderControlView, "m_FaceValMin")
RegistClassMember(LuaPinchFaceSliderControlView, "m_FaceValMax")
RegistClassMember(LuaPinchFaceSliderControlView, "m_FacialOneDimenSliderMap")
RegistClassMember(LuaPinchFaceSliderControlView, "m_FacialTwoDimenSliderMap")
RegistClassMember(LuaPinchFaceSliderControlView, "m_FacialValMin")
RegistClassMember(LuaPinchFaceSliderControlView, "m_FacialValMax")
RegistClassMember(LuaPinchFaceSliderControlView, "m_FacialTabBarArr")
RegistClassMember(LuaPinchFaceSliderControlView, "m_FacialCustomColorSliderIdSet")
RegistClassMember(LuaPinchFaceSliderControlView, "m_FacialStyleData")
RegistClassMember(LuaPinchFaceSliderControlView, "m_FacialCurPartIndex")
RegistClassMember(LuaPinchFaceSliderControlView, "m_ColorTemplateTex")
RegistClassMember(LuaPinchFaceSliderControlView, "m_ColorTemplateSize")
RegistClassMember(LuaPinchFaceSliderControlView, "m_HairColorQnRadioBox")
RegistClassMember(LuaPinchFaceSliderControlView, "m_FacialColorQnRadioBox")
RegistClassMember(LuaPinchFaceSliderControlView, "m_FacialColorIdList")
RegistClassMember(LuaPinchFaceSliderControlView, "m_IsShowCustomColorView")
RegistClassMember(LuaPinchFaceSliderControlView, "m_FacialEyeOptionIndex")
RegistClassMember(LuaPinchFaceSliderControlView, "m_CurCustomColorSlider")
RegistClassMember(LuaPinchFaceSliderControlView, "m_HasChangedCustomColor")
RegistClassMember(LuaPinchFaceSliderControlView, "m_SimuControlSliderIdMap")
RegistClassMember(LuaPinchFaceSliderControlView, "m_TwoDimenSliderX")
RegistClassMember(LuaPinchFaceSliderControlView, "m_TwoDimenSliderY")
RegistClassMember(LuaPinchFaceSliderControlView, "m_MianShiQnCheckBoxGroup")
RegistClassMember(LuaPinchFaceSliderControlView, "m_HairColorModifyItem")
RegistClassMember(LuaPinchFaceSliderControlView, "m_HairColorModifyTemplateId")
RegistClassMember(LuaPinchFaceSliderControlView, "m_HairColorList")

function LuaPinchFaceSliderControlView:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    UIEventListener.Get(self.FinishedCustomColorBtn.transform:Find("CommonButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFinishedCustomColorBtnClick()
	end)
end

function LuaPinchFaceSliderControlView:Start()
    self.OneDimenSlider.gameObject:SetActive(false)
    self.TwoDimenSlider.gameObject:SetActive(true)
    self.TwoDimenSlider.transform:Find("CoordinateAxisBg/Bg/CUIFx_dianji").gameObject:SetActive(true)
    self.TwoDimenSlider.transform:Find("CoordinateAxisBg/Bg/CUIFx_xh").gameObject:SetActive(true)
    self.TwoDimenSlider.transform:Find("CoordinateAxisBg/Bg/CUIFx_dianji/right1").gameObject:SetActive(false)
	self.TwoDimenSlider.transform:Find("CoordinateAxisBg/Bg/CUIFx_dianji/right2").gameObject:SetActive(false)
	self.TwoDimenSlider.transform:Find("CoordinateAxisBg/Bg/CUIFx_dianji/left1").gameObject:SetActive(false)
	self.TwoDimenSlider.transform:Find("CoordinateAxisBg/Bg/CUIFx_dianji/left2").gameObject:SetActive(false)
    local twoDimenSliderAxisBg = self.TwoDimenSlider.transform:Find("CoordinateAxisBg"):GetComponent(typeof(UIWidget))
    twoDimenSliderAxisBg.width = 420
    twoDimenSliderAxisBg.height = 420
    twoDimenSliderAxisBg:ResizeCollider()
    self.TwoDimenSlider.gameObject:SetActive(false)
    self.CustomColorSlider.gameObject:SetActive(false)
    self.ColorSelectionTemplate.gameObject:SetActive(false)
    self.SpecialColorSelectionTemplate.gameObject:SetActive(false)
    self.CustomColorBtnTemplate.gameObject:SetActive(false)
    self.QnCheckBoxGroupTemplate.gameObject:SetActive(false)
    self.HairColorModifyTemplate.gameObject:SetActive(false)
    self.m_IsShowCustomColorView = false
    self.MainViewAnchor.gameObject:SetActive(not self.m_IsShowCustomColorView)
    self.FinishedCustomColorBtn.gameObject:SetActive(self.m_IsShowCustomColorView)
end

function LuaPinchFaceSliderControlView:ShowAni()
    local ani = self.transform:Find("Right/Offset"):GetComponent(typeof(Animation))
    ani:Stop()
    ani:Play()
end

--@region Public
function LuaPinchFaceSliderControlView:InitFaceSliderView(faceTabBarData)
    self.m_FaceValMin, self.m_FaceValMax = - 1, 1
    self.m_FaceOneDimenSliderMap = {}
    self.m_FaceTwoDimenSliderMap = {}
    self.m_FacialEyeOptionIndex = 1
    --self.RightBg.gameObject:SetActive(true)
    self.MainViewAnchor.gameObject:SetActive(true)
    self.FinishedCustomColorBtn.gameObject:SetActive(false)
    LuaPinchFaceMgr:SetBlurTexVisible(false)
    Extensions.SetLocalPositionX(self.RightTable.transform,-215)
    Extensions.RemoveAllChildren(self.RightTable.transform)
    for i = 0, faceTabBarData.Bar.Length - 1 do
        local arr = faceTabBarData.Bar[i]
        if arr.Length == 1 then
            local sliderId = arr[0]
            local obj = NGUITools.AddChild(self.RightTable.gameObject, self.OneDimenSlider.gameObject)
            obj:SetActive(true)
            self:InitFaceOneDimenSlider(obj, sliderId)
            self.m_FaceOneDimenSliderMap[sliderId] = obj
        elseif arr.Length == 2 then
            local sliderId1, sliderId2 = arr[0], arr[1]
            local sliderData1, sliderData2 = PinchFace_FaceSliderData.GetData(sliderId1), PinchFace_FaceSliderData.GetData(sliderId2)
            local desArr = {}
            for j = 0, 1 do
                table.insert(desArr, sliderData1.SliderBar[j])
            end
            for j = 0, 1 do
                table.insert(desArr, sliderData2.SliderBar[j])
            end
            local obj = NGUITools.AddChild(self.RightTable.gameObject, self.TwoDimenSlider.gameObject)
            obj:SetActive(true)
            LuaPinchFaceMgr:SetBlurTexVisible(true)
            local script = obj:GetComponent(typeof(CCommonLuaScript))
            script.m_LuaSelf:SetLabels(LocalString.GetString("微调"), desArr)
            self:InitFaceTwoDimenSlider(obj, sliderId1, sliderId2)
            self.m_FaceTwoDimenSliderMap[sliderId1 * 10000 + sliderId2] = obj
        end
    end
    self.RightTable:Reposition()
    self.RightScrollView:ResetPosition()
    self:ShowAni()
    self:ResetModifyModeHairColor()
end

function LuaPinchFaceSliderControlView:InitFacialSliderView(tabBarKey, facialTabBarArr, styleData)
    LuaPinchFaceMgr:SetBlurTexVisible(false)
    self.m_FacialValMin, self.m_FacialValMax = 0, 1
    self.m_FacialCurPartIndex = tabBarKey
    self.m_FacialOneDimenSliderMap = {}
    self.m_FacialTwoDimenSliderMap = {}
    self.m_FacialTabBarArr = facialTabBarArr
    self.m_FacialStyleData = styleData
    self.m_FacialColorQnRadioBox = nil
    self.m_MianShiQnCheckBoxGroup = nil
    self.m_FacialColorIdList = {}
    self.m_FacialEyeOptionIndex = (tabBarKey == LuaPinchFaceMgr.m_LeftEyeFacialTabKey) and self.m_FacialEyeOptionIndex or 1
    local hasCustomColor = false
    if facialTabBarArr then
        for i = 0, facialTabBarArr.Length - 1, 2 do
            local arr1, arr2 = facialTabBarArr[i], facialTabBarArr[i + 1]
            hasCustomColor = hasCustomColor or arr1.Length == 3
        end
    end
    local tabBarData = PinchFace_FacialTabBarData.GetData(self.m_FacialCurPartIndex)
    self.m_FacialCustomColorSliderIdSet = {}
    if tabBarData.FashionColorInfo then
        for i = 0, tabBarData.FashionColorInfo.Length - 1 do
            local sliderId = tabBarData.FashionColorInfo[i]
            if sliderId > 0 then
                self.m_FacialCustomColorSliderIdSet[sliderId] = true
            end
        end
    end
    self:ShowFacialSliderViewWithoutCustomColor(facialTabBarArr, styleData)
    self:ShowAni()
    self:ResetModifyModeHairColor()
end

function LuaPinchFaceSliderControlView:InitHairSliderView()
    --self.RightBg.gameObject:SetActive(true)
    self.MainViewAnchor.gameObject:SetActive(true)
    self.FinishedCustomColorBtn.gameObject:SetActive(false)
    self.m_FacialCurPartIndex = - 1
    self.m_FacialEyeOptionIndex = 1
    LuaPinchFaceMgr:SetBlurTexVisible(false)
    Extensions.SetLocalPositionX(self.RightTable.transform,-215)
    Extensions.RemoveAllChildren(self.RightTable.transform)
    local initializationData = LuaPinchFaceMgr:GetInitializationData()
    if initializationData then
        local colorList = {}
        for i = 0, initializationData.HairColor.Length - 1 do
            table.insert(colorList, NGUIText.ParseColor24(initializationData.NewHairColorPreview[i], 0))
        end
        self.m_HairColorList = colorList
        self.m_HairColorQnRadioBox = self:InitColorSelectionTemplate(colorList, nil)
        self.m_HairColorQnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
            self:OnSelectHairColor(index, colorList)
        end)
        self.m_HairColorQnRadioBox.CurrentIndex = -1
        if LuaPinchFaceMgr.m_HairId ~= 9 and LuaPinchFaceMgr.m_AdvancedHairColorIndex == 0 then
            self.m_HairColorQnRadioBox:ChangeTo(LuaPinchFaceMgr.m_HeadId, LuaPinchFaceMgr.m_IsShowLuoZhuangHair)
            self:UpdateHairColorModifyItemVisible() 
        end
    end
    if LuaPinchFaceMgr.m_IsInModifyMode then
        self:CreateHairColorModifyTemplate()
    end
    self.RightTable:Reposition()
    self.RightScrollView:ResetPosition()
    self:ShowAni()
end
--@endregion Public

--@region FaceSliderView
function LuaPinchFaceSliderControlView:ShowFacialCustomColorSliderView(facialTabBarArr, styleData)
    self.m_FacialColorQnRadioBox = nil
    self.m_FacialColorIdList = {}
    self.m_FacialOneDimenSliderMap = {}
    self.m_FacialTwoDimenSliderMap = {}
    self.m_HasChangedCustomColor = false
    self.m_IsShowCustomColorView = true
    self.m_CurCustomColorSlider = nil
    self.m_SimuControlSliderIdMap = {}
    Extensions.SetLocalPositionX(self.RightTable.transform,-235)
    Extensions.RemoveAllChildren(self.RightTable.transform)
    if facialTabBarArr then
        for i = 0, facialTabBarArr.Length - 1, 2 do
            local arr1, arr2 = facialTabBarArr[i], facialTabBarArr[i + 1]
            if arr1.Length == 1 and arr2 then
                local sliderId = tonumber(arr1[0])
                if self.m_FacialCustomColorSliderIdSet[sliderId] then
                    local title = arr2[0]
                    self:CreateFacialOneDimenSlider(title, sliderId)
                end
            elseif arr1.Length == 2 and arr2 then
                local sliderId1, sliderId2 = tonumber(arr1[0]), tonumber(arr1[1])
                if self.m_FacialCustomColorSliderIdSet[sliderId1] and self.m_FacialCustomColorSliderIdSet[sliderId2] then
                    local desArr = {}
                    for i = 0, arr2.Length - 1 do
                        table.insert(desArr, arr2[i])
                    end
                    if #desArr == 4 then
                        self:CreateFacialTwoDimenSlider(desArr, sliderId1, sliderId2)
                    elseif #desArr == 1 then
                        self.m_SimuControlSliderIdMap[sliderId1] = sliderId2
                        self.m_SimuControlSliderIdMap[sliderId2] = sliderId1
                        self:CreateFacialOneDimenSlider(desArr[1], sliderId1)
                    end
                end
            elseif arr1.Length == 3 then
                local sliderId1, sliderId2, sliderId3 = tonumber(arr1[0]), tonumber(arr1[1]), tonumber(arr1[2])
                self:CreateCustomColorSlider(sliderId1, sliderId2, sliderId3, styleData)
            end
        end
    end
    --self.RightBg.gameObject:SetActive(true)
    self.MainViewAnchor.gameObject:SetActive(not self.m_IsShowCustomColorView)
    self.FinishedCustomColorBtn.gameObject:SetActive(self.m_IsShowCustomColorView)
    self.RightTable:Reposition()
    self.RightScrollView:ResetPosition()
    self:ShowAni()
end

function LuaPinchFaceSliderControlView:ShowFacialSliderViewWithoutCustomColor(facialTabBarArr, styleData)
    self.m_FacialColorQnRadioBox = nil
    self.m_FacialColorIdList = {}
    self.m_FacialOneDimenSliderMap = {}
    self.m_FacialTwoDimenSliderMap = {}
    self.m_IsShowCustomColorView = false
    self.m_CurCustomColorSlider = nil
    self.m_SimuControlSliderIdMap = {}
    Extensions.SetLocalPositionX(self.RightTable.transform,-215)
    Extensions.RemoveAllChildren(self.RightTable.transform)
    local hasCustomColor = false
    if facialTabBarArr then
        for i = 0, facialTabBarArr.Length - 1, 2 do
            if facialTabBarArr[i] then
                hasCustomColor = hasCustomColor or facialTabBarArr[i].Length == 3
            end
        end
    end
    local tabBarData = PinchFace_FacialTabBarData.GetData(self.m_FacialCurPartIndex)
    local presetColorList = styleData.PresetColorList and styleData.PresetColorList or tabBarData.PresetColorList
    if not presetColorList or presetColorList.Length == 0 then
        hasCustomColor = false
    end
    local hasSlider = false
    if facialTabBarArr then
        for i = 0, facialTabBarArr.Length - 1, 2 do
            local arr1, arr2 = facialTabBarArr[i], facialTabBarArr[i + 1]
            if arr1.Length == 1 and arr2 then
                local sliderId = tonumber(arr1[0])
                if (not self.m_FacialCustomColorSliderIdSet[sliderId]) or (not hasCustomColor) then
                    hasSlider = true
                    local title = arr2[0]
                    self:CreateFacialOneDimenSlider(title, sliderId)
                end
            elseif arr1.Length == 2 and arr2 then
                local sliderId1, sliderId2 = tonumber(arr1[0]), tonumber(arr1[1])
                if (not self.m_FacialCustomColorSliderIdSet[sliderId1] and not self.m_FacialCustomColorSliderIdSet[sliderId2]) or (not hasCustomColor) then
                    hasSlider = true
                    local desArr = {}
                    for i = 0, arr2.Length - 1 do
                        table.insert(desArr, arr2[i])
                    end
                    if #desArr == 4 then
                        self:CreateFacialTwoDimenSlider(desArr, sliderId1, sliderId2)
                    elseif #desArr == 1 then
                        self.m_SimuControlSliderIdMap[sliderId1] = sliderId2
                        self.m_SimuControlSliderIdMap[sliderId2] = sliderId1
                        self:CreateFacialOneDimenSlider(desArr[1], sliderId1)
                    end
                end
            elseif arr1.Length == 3 then
                hasSlider = true
                local sliderId1, sliderId2, sliderId3 = tonumber(arr1[0]), tonumber(arr1[1]), tonumber(arr1[2])
                local presetColorList = styleData.PresetColorList and styleData.PresetColorList or tabBarData.PresetColorList
                if not presetColorList or presetColorList.Length == 0 then
                    self:CreateCustomColorSlider(sliderId1, sliderId2, sliderId3, styleData)
                else
                    self:CreateFacialColorSelectionTemplate(sliderId1, sliderId2, sliderId3, styleData)
                end
            end
        end
    end
    if self.m_FacialCurPartIndex == 8 and styleData and styleData.AllowedMirroring and styleData.AllowedMirroring > 0 then
        self:InitMianShiQnCheckBoxGroup()
    end
    --self.RightBg.gameObject:SetActive(hasSlider)
    self.MainViewAnchor.gameObject:SetActive(not self.m_IsShowCustomColorView)
    self.FinishedCustomColorBtn.gameObject:SetActive(self.m_IsShowCustomColorView)
    self.RightTable:Reposition()
    self.RightScrollView:ResetPosition()
    self:ShowAni()
end

function LuaPinchFaceSliderControlView:CreateFacialColorSelectionTemplate(sliderId1, sliderId2, sliderId3, styleData)
    local tabBarData = PinchFace_FacialTabBarData.GetData(self.m_FacialCurPartIndex)
    local presetColorList = styleData.PresetColorList and styleData.PresetColorList or tabBarData.PresetColorList
    local colorList = {}
    local guangzeAlphaList = nil
    local colorIdList = {}
    if presetColorList then
        guangzeAlphaList = {}
        for i = 0, presetColorList.Length - 1 do
            local id = presetColorList[i]
            local colorData = PinchFace_FashionColor.GetData(id)
            local color =  LuaPinchFaceMgr:GetFashionColor(self.m_FacialCurPartIndex, sliderId1, sliderId2, sliderId3, styleData, colorData)
            if self.m_FacialCurPartIndex == 7 then
                local curFaceSkinData = LuaPinchFaceMgr:GetCurFaceSkinData()
                if curFaceSkinData and curFaceSkinData.SkinColor.Length == 4 then
                    local faceSkinColor = curFaceSkinData.SkinColor
                    color = Color(color.r * 0.5 + faceSkinColor[0] * 0.5, color.g * 0.5 + faceSkinColor[1] * 0.5, color.b * 0.5 + faceSkinColor[2] * 0.5, color.a * 0.5 + faceSkinColor[3] * 0.5)
                end
            elseif self.m_FacialCurPartIndex == 6 then
                local curFaceSkinData = LuaPinchFaceMgr:GetCurFaceSkinData()
                if curFaceSkinData and curFaceSkinData.MouthColor.Length == 4 then
                    local faceSkinColor = curFaceSkinData.MouthColor
                    color = Color(color.r * color.a + faceSkinColor[0] * (1-color.a), color.g * color.a + faceSkinColor[1] * (1-color.a), color.b * color.a + faceSkinColor[2] * (1-color.a), color.a )---* 0.5 + faceSkinColor[3] * 0.5)
                    
                    local h, s, v = ColorExt.RGB2HSV(color)
                    v = math.max(0,math.min(1,v + 0.2))
                    s = math.max(0,math.min(1,s - 0.1))
                    color = ColorExt.HSV2RGB(h, s, v)
                end
            end
            local gloss = 0
            if colorData.Gloss > 0 and LuaPinchFaceMgr.m_CurEnumGender == EnumGender.Female then
                gloss = colorData.Gloss
            end
            table.insert(guangzeAlphaList, gloss)
            table.insert(colorList, color)
            table.insert(colorIdList, id)
        end
    end
    local optionTextList = LuaPinchFaceMgr.m_LeftEyeFacialTabKey == self.m_FacialCurPartIndex and {
        LocalString.GetString("双眼"), LocalString.GetString("左眼"), LocalString.GetString("右眼")
    } or nil
    local radioBox = self:InitColorSelectionTemplate(colorList, guangzeAlphaList, optionTextList, function(index)
        self:SelectFacialEyeOption(index)
    end)
    radioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSelectFacialColor(index, colorIdList, true)
    end)
    local lastSelectColorId = LuaPinchFaceMgr.m_LastFacialPartindex2ColorId[self.m_FacialCurPartIndex]
    if lastSelectColorId then
        for i = 1, #colorIdList do
            if colorIdList[i] == lastSelectColorId then
                radioBox:ChangeTo(i - 1, false)
                break
            end
        end
    end

    self.m_FacialColorQnRadioBox = radioBox
    self.m_FacialColorIdList = colorIdList

    local showCustomSVValue = styleData.CustomSVValue == 1  or tabBarData.CustomSVValue == 1 
    if showCustomSVValue then
        local btn = NGUITools.AddChild(self.RightTable.gameObject, self.CustomColorBtnTemplate.gameObject)
        btn.gameObject:SetActive(true)
        UIEventListener.Get(btn.transform:Find("ShowCustomColorBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnCustomColorBtnClick()
        end)
    end
end

function LuaPinchFaceSliderControlView:CreateFacialTwoDimenSlider(desArr, sliderId1, sliderId2)
    local obj = NGUITools.AddChild(self.RightTable.gameObject, self.TwoDimenSlider.gameObject)
    obj:SetActive(true)
    LuaPinchFaceMgr:SetBlurTexVisible(true)
    local script = obj:GetComponent(typeof(CCommonLuaScript))
    script.m_LuaSelf:SetLabels(LocalString.GetString("微调"), desArr)
    self:InitFacialTwoDimenSlider(obj, self.m_FacialCurPartIndex, sliderId1, sliderId2)
    self:SetFacialTwoDimenSliderDefaultValue(obj, self.m_FacialCurPartIndex, sliderId1, sliderId2)
    self.m_FacialTwoDimenSliderMap[sliderId1 * 10000 + sliderId2] = obj
end

function LuaPinchFaceSliderControlView:InitFaceTwoDimenSlider(obj, sliderId1, sliderId2)
    local script = obj:GetComponent(typeof(CCommonLuaScript))
    self.m_TwoDimenSliderX = 0
    self.m_TwoDimenSliderY = 0
    script.m_LuaSelf:Init(function (x, y) 
        self:ShowTwoDimenSliderFx(script, x, y)
        local xValue, yValue  = self:SliderValue2FaceVal(sliderId2, x, -1, 1), self:SliderValue2FaceVal(sliderId1, y, -1, 1)
        LuaPinchFaceMgr:SetFaceSliderData({[sliderId1] = yValue, [sliderId2] = xValue})
    end, function (x, y)
        local xValue, yValue  = self:SliderValue2FaceVal(sliderId2, x, -1, 1), self:SliderValue2FaceVal(sliderId1, y, -1, 1)
        LuaPinchFaceMgr:RecordLastFaceSliderData({[sliderId1] = yValue, [sliderId2] = xValue})
    end,function (x, y)
        local xValue, yValue  = self:SliderValue2FaceVal(sliderId2, x, -1, 1), self:SliderValue2FaceVal(sliderId1, y, -1, 1)
        LuaPinchFaceMgr:SetFaceSliderData({[sliderId1] = yValue, [sliderId2] = xValue}, true)
    end)
    self:SetFaceTwoDimenSliderDefaultValue(obj, sliderId1, sliderId2)
end

function LuaPinchFaceSliderControlView:SetFaceTwoDimenSliderDefaultValue(obj, sliderId1, sliderId2)
    local script = obj:GetComponent(typeof(CCommonLuaScript))
    if script.m_LuaSelf.m_IsDragging then
        return
    end
    local v1 = LuaPinchFaceMgr:GetFaceDnaDataVal(sliderId1)
    local v2 = LuaPinchFaceMgr:GetFaceDnaDataVal(sliderId2)
    local x = self:FaceVal2SliderValue(sliderId2, v2, -1, 1)
    local y = self:FaceVal2SliderValue(sliderId1, v1, -1, 1)
    script.m_LuaSelf:SetValue(x, y, false)
    self:ShowTwoDimenSliderFx(script, x, y)
end

function LuaPinchFaceSliderControlView:CreateFacialOneDimenSlider(title, sliderId)
    local obj = NGUITools.AddChild(self.RightTable.gameObject, self.OneDimenSlider.gameObject)
    obj:SetActive(true)
    self:InitFacialOneDimenSlider(obj, self.m_FacialCurPartIndex, sliderId, title)
    self:SetFacialOneDimenSliderDefaultValue(obj, self.m_FacialCurPartIndex, sliderId)
    self.m_FacialOneDimenSliderMap[sliderId] = obj
end

function LuaPinchFaceSliderControlView:InitFaceOneDimenSlider(obj, sliderId)
    local script = obj:GetComponent(typeof(CCommonLuaScript))
    local sliderData = PinchFace_FaceSliderData.GetData(sliderId)
    script.m_LuaSelf:Init(sliderData.Content, 0, 100, function (v, isRecord)
        local sliderVal = self:SliderValue2FaceVal(sliderId, v, 0, 1)
        LuaPinchFaceMgr:SetFaceSliderData({[sliderId] = sliderVal}, isRecord)
    end,function (v)
        local sliderVal = self:SliderValue2FaceVal(sliderId, v, 0, 1)
        LuaPinchFaceMgr:RecordLastFaceSliderData({[sliderId] = sliderVal})
    end)
    self:SetFaceOneDimenSliderDefaultValue(obj, sliderId)
end

function LuaPinchFaceSliderControlView:SetFaceOneDimenSliderDefaultValue(obj, sliderId)
    local script = obj:GetComponent(typeof(CCommonLuaScript))
    local v = LuaPinchFaceMgr:GetFaceDnaDataVal(sliderId)
    local defaultValue = self:FaceVal2SliderValue(sliderId, v, 0, 1)
    script.m_LuaSelf:SetDefaultValue(defaultValue)
end

function LuaPinchFaceSliderControlView:FaceVal2SliderValue(sliderId, v, sliderValMin, sliderValMax)
    local sliderData = PinchFace_FaceSliderData.GetData(sliderId)
    local min, max = LuaPinchFaceMgr:GetFaceSliderValRange(sliderData)
    local percent = math.min(1, math.max(0, (v - min)/(max - min))) 
    if sliderData.IsReverse > 0 then
        percent = 1 - percent
    end
    local sliderValue = math.lerp(sliderValMin, sliderValMax, percent)
    return sliderValue
end

function LuaPinchFaceSliderControlView:SliderValue2FaceVal(sliderId, sliderValue, sliderValMin, sliderValMax)
    local sliderData = PinchFace_FaceSliderData.GetData(sliderId)
    local percent = math.min(1, math.max(0, (sliderValue - sliderValMin)/(sliderValMax - sliderValMin))) 
    if sliderData.IsReverse > 0 then
        percent = 1 - percent
    end
    local min, max = LuaPinchFaceMgr:GetFaceSliderValRange(sliderData)
    local v = math.lerp(min, max, percent)
    return v
end

--@endregion FaceSliderView

--@region FacialSliderView
function LuaPinchFaceSliderControlView:InitFacialOneDimenSlider(obj, tabBarKey, sliderId, title)
    local script = obj:GetComponent(typeof(CCommonLuaScript))
    script.m_LuaSelf:Init(title, 0, 100, function (v, isRecord)
        local key2id2Val = self:GetFacialOneDimenSliderData(tabBarKey, sliderId, v)
        LuaPinchFaceMgr:SetFacialPartDataVal(key2id2Val, isRecord and not self.m_IsShowCustomColorView) 
        if self.m_IsShowCustomColorView then
            self.m_HasChangedCustomColor = self.m_HasChangedCustomColor or isRecord  
        end
    end,function (v)
        if not self.m_IsShowCustomColorView then
            LuaPinchFaceMgr:RecordLastFacialPartData(tabBarKey)
            if tabBarKey == LuaPinchFaceMgr.m_LeftEyeFacialTabKey then
                LuaPinchFaceMgr:RecordLastFacialPartData(LuaPinchFaceMgr.m_RightEyeFacialTabKey)
            end
        end
    end)
end

function LuaPinchFaceSliderControlView:GetFacialOneDimenSliderData(tabBarKey, sliderId, v)
    local styleId = LuaPinchFaceMgr:GetPartStyleId(tabBarKey)
    local key2id2Val = {[tabBarKey]= {[sliderId] = self:SliderValue2FacialVal(tabBarKey, styleId, sliderId, v, 0, 1)}}
    if self.m_SimuControlSliderIdMap[sliderId] then
        local sliderId2 = self.m_SimuControlSliderIdMap[sliderId]
        key2id2Val[tabBarKey][sliderId2] = key2id2Val[tabBarKey][sliderId]
    end
    key2id2Val[LuaPinchFaceMgr.m_RightEyeFacialTabKey] = key2id2Val[LuaPinchFaceMgr.m_LeftEyeFacialTabKey]
    return key2id2Val
end

function LuaPinchFaceSliderControlView:SetFacialOneDimenSliderDefaultValue(obj, tabBarKey,  sliderId)
    local script = obj:GetComponent(typeof(CCommonLuaScript))
    local v =  LuaPinchFaceMgr:GetFacialPartDataVal(tabBarKey, sliderId)
    local styleId = LuaPinchFaceMgr:GetPartStyleId(tabBarKey)
    local defaultValue = self:FacialVal2SliderValue(tabBarKey, styleId, sliderId, v, 0, 1)
    script.m_LuaSelf:SetDefaultValue(defaultValue)
    if self.m_IsShowCustomColorView then
        LuaPinchFaceMgr:RecordLastFacialPartData(tabBarKey)
    end
end

function LuaPinchFaceSliderControlView:InitFacialTwoDimenSlider(obj, tabBarKey, sliderId1, sliderId2)
    local script = obj:GetComponent(typeof(CCommonLuaScript))
    self.m_TwoDimenSliderX = 0
    self.m_TwoDimenSliderY = 0
    script.m_LuaSelf:Init(function (x, y) 
        self:ShowTwoDimenSliderFx(script, x, y)
        local tabBarKey2sliderId2val = self:GetFacialTwoDimenSliderData(tabBarKey, sliderId1, sliderId2, x, y)
        LuaPinchFaceMgr:SetFacialPartDataVal(tabBarKey2sliderId2val)
    end, function (x, y)
        if not self.m_IsShowCustomColorView then
            LuaPinchFaceMgr:RecordLastFacialPartData(tabBarKey)
        end
    end,function (x, y)
        local tabBarKey2sliderId2val = self:GetFacialTwoDimenSliderData(tabBarKey, sliderId1, sliderId2, x, y)
        LuaPinchFaceMgr:SetFacialPartDataVal(tabBarKey2sliderId2val,  not self.m_IsShowCustomColorView)
        if self.m_IsShowCustomColorView then
            self.m_HasChangedCustomColor = true
        end
    end)
    if tabBarKey == 8 then
        script.m_LuaSelf:SetResetXYAction(function() 
            LuaPinchFaceMgr:SelectFacialPartStyle(tabBarKey, self.m_FacialStyleData, true)
        end)
    end
end

function LuaPinchFaceSliderControlView:GetFacialTwoDimenSliderData(tabBarKey, sliderId1, sliderId2, x, y)
    local styleId = LuaPinchFaceMgr:GetPartStyleId(tabBarKey)
    local xValue, yValue  = self:SliderValue2FacialVal(tabBarKey, styleId, sliderId2, x, -1, 1), self:SliderValue2FacialVal(tabBarKey, styleId, sliderId1, y, -1, 1)
    local tabBarKey2sliderId2val = {[tabBarKey] = {[sliderId1] = yValue, [sliderId2] = xValue}}
    tabBarKey2sliderId2val[LuaPinchFaceMgr.m_RightEyeFacialTabKey] = tabBarKey2sliderId2val[LuaPinchFaceMgr.m_LeftEyeFacialTabKey]
    return tabBarKey2sliderId2val
end

function LuaPinchFaceSliderControlView:SetFacialTwoDimenSliderDefaultValue(obj, tabBarKey, sliderId1, sliderId2)
    local script = obj:GetComponent(typeof(CCommonLuaScript))
    if script.m_LuaSelf.m_IsDragging then
        return
    end
    local styleId = LuaPinchFaceMgr:GetPartStyleId(tabBarKey)
    local v1 = LuaPinchFaceMgr:GetFacialPartDataVal(tabBarKey, sliderId1)
    local v2 = LuaPinchFaceMgr:GetFacialPartDataVal(tabBarKey, sliderId2)
    local x = self:FacialVal2SliderValue(tabBarKey, styleId, sliderId2, v2, -1, 1)
    local y = self:FacialVal2SliderValue(tabBarKey, styleId, sliderId1, v1, -1, 1)
    script.m_LuaSelf:SetValue(x, y, false)
    if self.m_IsShowCustomColorView then
        LuaPinchFaceMgr:RecordLastFacialPartData(tabBarKey)
    end
    self:ShowTwoDimenSliderFx(script, x, y)
end

function LuaPinchFaceSliderControlView:FacialVal2SliderValue(tabBarKey, styleId, sliderId, val, sliderValMin, sliderValMax)
    local rangeMin, rangeMax = LuaPinchFaceMgr:GetFacialSliderRangeVal(tabBarKey, styleId, sliderId)
    local min, max = math.lerp(self.m_FacialValMin, self.m_FacialValMax, rangeMin),  math.lerp(self.m_FacialValMin, self.m_FacialValMax, rangeMax) 
    local percent = math.min(1, math.max(0, (val - min)/(max - min))) 
    local sliderValue = math.lerp(sliderValMin, sliderValMax, percent)
    return sliderValue
end

function LuaPinchFaceSliderControlView:SliderValue2FacialVal(tabBarKey, styleId, sliderId, sliderValue, sliderValMin, sliderValMax)
    local rangeMin, rangeMax = LuaPinchFaceMgr:GetFacialSliderRangeVal(tabBarKey, styleId, sliderId)
    local percent = math.min(1, math.max(0, (sliderValue - sliderValMin)/(sliderValMax - sliderValMin))) 
    local min, max = math.lerp(self.m_FacialValMin, self.m_FacialValMax, rangeMin),  math.lerp(self.m_FacialValMin, self.m_FacialValMax, rangeMax) 
    local v = math.lerp(min, max, percent)
    return v
end

function LuaPinchFaceSliderControlView:InitColorSelectionTemplate(colorList, guangzeList, optionTextList, onSelectOptionAction)
    if self.m_FacialCurPartIndex == 6 then
        return self:InitSpecialColorSelectionTemplate(colorList, guangzeList)
    end
    local obj = NGUITools.AddChild(self.RightTable.gameObject, self.ColorSelectionTemplate.gameObject)
    obj:SetActive(true)
    self:InitColorSelectionOptionPanel(obj, optionTextList, onSelectOptionAction)
    local colorGrid = obj.transform:Find("ColorGrid"):GetComponent(typeof(UIGrid))
    local template = obj.transform:Find("ColorTextureTemplate").gameObject
    template.gameObject:SetActive(false)
    local widget = obj:GetComponent(typeof(UIWidget))
    local initialHeight = widget.height
    Extensions.RemoveAllChildren(colorGrid.transform)
    for i = 1, #colorList do
        local colorTexObj = NGUITools.AddChild(colorGrid.gameObject, template.gameObject)
        colorTexObj:SetActive(true)

        local tex = colorTexObj.transform:Find("Tex"):GetComponent(typeof(UITexture))
        tex.color = colorList[i]

        local guangze = colorTexObj.transform:Find("GuangZe"):GetComponent(typeof(UITexture))
        guangze.gameObject:SetActive(guangzeList ~= nil)
        if guangzeList then
            guangze.alpha = guangzeList[i]
        end
    end
    colorGrid:Reposition()
    widget.height = initialHeight + colorGrid.cellHeight * (math.ceil(#colorList / colorGrid.maxPerLine)) + self.RightTable.padding.y * 2
    local qnRadioBox = colorGrid.gameObject:AddComponent(typeof(QnRadioBox))
    qnRadioBox.m_Table = colorGrid
    qnRadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), #colorList)
    for i = 0, colorGrid.transform.childCount - 1 do
        local go = colorGrid.transform:GetChild(i).gameObject
        qnRadioBox.m_RadioButtons[i] = go.transform:Find("Tex"):GetComponent(typeof(QnSelectableButton))
        qnRadioBox.m_RadioButtons[i].OnClick = MakeDelegateFromCSFunction(qnRadioBox.On_Click, MakeGenericClass(Action1, QnButton), qnRadioBox)
        qnRadioBox.m_RadioButtons[i]:SetSelected(false, false)
    end
    return qnRadioBox
end

function LuaPinchFaceSliderControlView:InitColorSelectionOptionPanel(obj, optionTextList, onSelectOptionAction)
    local optionTemplate = obj.transform:Find("OptionTemplate")
    local optionViewGrid = obj.transform:Find("OptionPanel/OptionViewGrid"):GetComponent(typeof(UIGrid))
    local optionBg = obj.transform:Find("OptionPanel/OptionBg")
    local optionQnTipButton = obj.transform:Find("OptionQnTipButton"):GetComponent(typeof(QnTipButton))

    optionViewGrid.gameObject:SetActive(false)
    optionQnTipButton:SetTipStatus(false)
    optionTemplate.gameObject:SetActive(false)
    optionBg.gameObject:SetActive(false)
    
    UIEventListener.Get(optionBg.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    optionViewGrid.gameObject:SetActive(false)
        optionQnTipButton:SetTipStatus(false)
        optionBg.gameObject:SetActive(false)
	end)
    optionQnTipButton.OnClick = DelegateFactory.Action_QnButton(function (button)
        optionViewGrid.gameObject:SetActive(true)
        optionQnTipButton:SetTipStatus(true)
        optionBg.gameObject:SetActive(true)
        optionViewGrid:Reposition()
    end)

    optionQnTipButton.gameObject:SetActive(optionTextList ~= nil and #optionTextList > 1)
    Extensions.RemoveAllChildren(optionViewGrid.transform)

    local optionObjList = {}

    local onSelectOption = function(index, ignoreCallback)
        if not optionTextList then return end
        optionQnTipButton.Text = optionTextList[index]
        optionViewGrid.gameObject:SetActive(false)
        optionQnTipButton:SetTipStatus(false)
        optionBg.gameObject:SetActive(false)
        for i, obj in pairs(optionObjList) do
            obj:SetActive(index ~= i)
        end
        if onSelectOptionAction and not ignoreCallback then
            onSelectOptionAction(index)
        end
        optionViewGrid:Reposition()
    end

    if optionTextList and #optionTextList > 1 then
        optionQnTipButton.Text = optionTextList[1]
        for i = 1, #optionTextList do
            local text = optionTextList[i]
            local obj = NGUITools.AddChild(optionViewGrid.gameObject, optionTemplate.gameObject)
            local label = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
            label.text = text
            obj:SetActive(true)
            local index = i
            UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (go)
                onSelectOption(index)
            end)
            table.insert(optionObjList, obj)
        end
        optionViewGrid:Reposition()
        onSelectOption(self.m_FacialEyeOptionIndex and self.m_FacialEyeOptionIndex or 1)
    end
end

function LuaPinchFaceSliderControlView:InitSpecialColorSelectionTemplate(colorList, guangzeList)
    local obj = NGUITools.AddChild(self.RightTable.gameObject, self.SpecialColorSelectionTemplate.gameObject)
    obj:SetActive(true)
    local male = obj.transform:Find("Male")
    local female = obj.transform:Find("Female")
    male.gameObject:SetActive(LuaPinchFaceMgr.m_CurEnumGender == EnumGender.Male)
    female.gameObject:SetActive(LuaPinchFaceMgr.m_CurEnumGender == EnumGender.Female)
    local root = LuaPinchFaceMgr.m_CurEnumGender == EnumGender.Male and male or female
    for i = 1, #colorList do
        local colorTexObj = root.transform:GetChild(i - 1)
        if colorTexObj then
            colorTexObj.gameObject:SetActive(true)

            local tex = colorTexObj.transform:Find("Tex"):GetComponent(typeof(UITexture))
            tex.color = colorList[i]
        end
    end
    local qnRadioBox = root.gameObject:AddComponent(typeof(QnRadioBox))
    qnRadioBox.m_Table = nil
    qnRadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), #colorList)
    local index = 0
    for i = 0, root.transform.childCount - 1 do
        local go = root.transform:GetChild(i).gameObject
        local tex = go.transform:Find("Tex")
        if tex then
            local btn = tex:GetComponent(typeof(QnSelectableButton))
            if btn then
                qnRadioBox.m_RadioButtons[index] = btn
                qnRadioBox.m_RadioButtons[index].OnClick = MakeDelegateFromCSFunction(qnRadioBox.On_Click, MakeGenericClass(Action1, QnButton), qnRadioBox)
                qnRadioBox.m_RadioButtons[index]:SetSelected(false, false)
                index = index + 1 
            end
        end
    end
    return qnRadioBox
end

function LuaPinchFaceSliderControlView:CreateCustomColorSlider(sliderId1, sliderId2, sliderId3, styleData)
    local obj = NGUITools.AddChild(self.RightTable.gameObject, self.CustomColorSlider.gameObject)
    obj:SetActive(true)
    local script = obj:GetComponent(typeof(CCommonLuaScript))

    local tabBarData = PinchFace_FacialTabBarData.GetData(self.m_FacialCurPartIndex)

    local hmin, hmax, smin, smax, vmin, vmax = 0, 1, 0, 1, 0, 1
    if styleData.CustomColor == 1 or tabBarData.CustomColor == 1 then
        local rangeHValue = styleData.RangeHValue and styleData.RangeHValue or tabBarData.RangeHValue
        if rangeHValue then
            hmin, hmax = rangeHValue[0], rangeHValue[1]
            local hmin2, hmax2 = LuaPinchFaceMgr:GetFacialSliderRangeVal(self.m_FacialCurPartIndex, styleData.Id, sliderId1)
            if hmin2 ~= 0 or hmax2 ~= 1 then
                hmin = math.max(hmin, hmin2)
                hmax = math.min(hmax, hmax2)
            end
        end
    end
    smin, smax = LuaPinchFaceMgr:GetFacialSliderRangeVal(self.m_FacialCurPartIndex, styleData.Id, sliderId2)
    vmin, vmax = LuaPinchFaceMgr:GetFacialSliderRangeVal(self.m_FacialCurPartIndex, styleData.Id, sliderId3)
    local fc = LuaPinchFaceMgr.m_RO.FacialAsset
    if fc then
        local fapd = fc.FacialPartDatas[self.m_FacialCurPartIndex]
        if fapd then
            local displayType = fapd:GetSliderDisplayType(sliderId1)
            if displayType == EmDisplayType.Color_HSV then
                local colorRGBA = styleData.MainTexColor
                if colorRGBA and colorRGBA.Length == 4 then
                    local color = Color(colorRGBA[0],colorRGBA[1],colorRGBA[2],colorRGBA[3])
                    local h, s, v = ColorExt.RGB2HSV(color)
                    local _smin = fapd:GetCurSliderValue(sliderId2, smin) + s 
                    local _smax = fapd:GetCurSliderValue(sliderId2, smax) + s
                    local _vmin = fapd:GetCurSliderValue(sliderId3, vmin) + v
                    local _vmax = fapd:GetCurSliderValue(sliderId3, vmax) + v
                    _smin = math.min(1, math.max(0, _smin))
                    _smax = math.min(1, math.max(0, _smax))
                    _vmin = math.min(1, math.max(0, _vmin))
                    _vmax = math.min(1, math.max(0, _vmax))
                    if _smax > _smin then
                        smax, smin = _smax, _smin
                    end
                    if _vmax > _vmin then
                        vmax, vmin = _vmax, _vmin
                    end
                end
            end
        end
    end
    local customSVValue = styleData.CustomSVValue == 1  or tabBarData.CustomSVValue == 1 
    script.m_LuaSelf:Init(hmin, hmax, smin, smax, vmin, vmax, customSVValue, function (hsv)
        local key2hsv = self:GetTabBarKey2HSVData(hsv, self.m_FacialCurPartIndex)
        LuaPinchFaceMgr:SetFacialPartDataColorHSV(key2hsv, sliderId1, sliderId2, sliderId3, styleData, false)
    end,function(hsv)
        if not self.m_IsShowCustomColorView then
            local key2hsv = self:GetTabBarKey2HSVData(hsv, self.m_FacialCurPartIndex)
            LuaPinchFaceMgr:RecordFacialPartDataColorHSV(key2hsv)
        end
    end,function(hsv)
        local key2hsv = self:GetTabBarKey2HSVData(hsv, self.m_FacialCurPartIndex)
        LuaPinchFaceMgr:SetFacialPartDataColorHSV(key2hsv, sliderId1, sliderId2, sliderId3, styleData, not self.m_IsShowCustomColorView)
        if self.m_IsShowCustomColorView then
            self.m_HasChangedCustomColor = true
        end
    end)
    script.m_LuaSelf:SetTwoDimenTexPixelsType(self.m_FacialCurPartIndex == 8)
    local key = self.m_FacialCurPartIndex
    if LuaPinchFaceMgr.m_LeftEyeFacialTabKey == self.m_FacialCurPartIndex then
        key = self.m_FacialEyeOptionIndex == 3 and LuaPinchFaceMgr.m_RightEyeFacialTabKey or LuaPinchFaceMgr.m_LeftEyeFacialTabKey
    end
    local defaultHSV = LuaPinchFaceMgr:GetFacialPartDataColorHSV(key, sliderId1, sliderId2, sliderId3, styleData)
    script.m_LuaSelf:SetHSV(defaultHSV.x,defaultHSV.y,defaultHSV.z, true)
    if self.m_IsShowCustomColorView then
        local key2hsv = self:GetTabBarKey2HSVData(defaultHSV, self.m_FacialCurPartIndex)
        LuaPinchFaceMgr:RecordFacialPartDataColorHSV(key2hsv)
    end
    self.m_CurCustomColorSlider = script.m_LuaSelf
end

function LuaPinchFaceSliderControlView:SelectFacialEyeOption(index)
    if self.m_FacialEyeOptionIndex == index then
        return
    end
    self.m_FacialEyeOptionIndex = index
    if LuaPinchFaceMgr.m_LeftEyeFacialTabKey == self.m_FacialCurPartIndex then
        local styleId1 = LuaPinchFaceMgr:GetPartStyleId(LuaPinchFaceMgr.m_LeftEyeFacialTabKey)
        local styleId2 = LuaPinchFaceMgr:GetPartStyleId(LuaPinchFaceMgr.m_RightEyeFacialTabKey)
        if styleId1 == 0 and styleId2 == 0 then
            local path1 = LuaPinchFaceMgr:GetCurPresetDataStylePath(LuaPinchFaceMgr.m_LeftEyeFacialTabKey)
            local path2 = LuaPinchFaceMgr:GetCurPresetDataStylePath(LuaPinchFaceMgr.m_RightEyeFacialTabKey)
            if path1 ~= path2 then
                local path = index == 3 and path2 or path1
                self.m_FacialStyleData.MainTexColor = LuaPinchFaceMgr.m_FacialStylePath2MainTexColor[path]
                self:InitFacialSliderView(self.m_FacialCurPartIndex, self.m_FacialTabBarArr, self.m_FacialStyleData)
            end
        end
    end
end

function LuaPinchFaceSliderControlView:GetTabBarKey2HSVData(hsv, tabBarKey)
    local _, h = math.modf(hsv.x)
    hsv = Vector3(h, hsv.y, hsv.z)
    local key2hsv = {}
    if LuaPinchFaceMgr.m_LeftEyeFacialTabKey == tabBarKey then
        if self.m_FacialEyeOptionIndex == 1 or self.m_FacialEyeOptionIndex == 2 then
            key2hsv[LuaPinchFaceMgr.m_LeftEyeFacialTabKey] = hsv 
        end
        if self.m_FacialEyeOptionIndex == 1 or self.m_FacialEyeOptionIndex == 3 then
            key2hsv[LuaPinchFaceMgr.m_RightEyeFacialTabKey] = hsv 
        end
    else
        key2hsv[tabBarKey] = hsv 
    end
    return key2hsv
end

function LuaPinchFaceSliderControlView:CreateQnCheckBoxGroup(checkboxtitles, onSelectQnCheckBoxGroupAction, defaultSelectIndex)
    local obj = NGUITools.AddChild(self.RightTable.gameObject, self.QnCheckBoxGroupTemplate.gameObject)
    obj:SetActive(true)
    local qnCheckBoxGroup = obj:GetComponent(typeof(QnCheckBoxGroup))
    qnCheckBoxGroup:InitWithOptions(checkboxtitles, false, false)
    qnCheckBoxGroup.OnSelect = DelegateFactory.Action_QnCheckBox_int(function(checkbox,level)
        if onSelectQnCheckBoxGroupAction then
            onSelectQnCheckBoxGroupAction(level)
        end
    end)
    qnCheckBoxGroup:SetSelect(defaultSelectIndex, true, true)
    return qnCheckBoxGroup
end

function LuaPinchFaceSliderControlView:InitMianShiQnCheckBoxGroup()
    local checkboxtitles = Table2ArrayWithCount({LocalString.GetString("默认"), LocalString.GetString("镜像")}, 2, MakeArrayClass(System.String))
    local val = LuaPinchFaceMgr:GetFacialPartDataVal(8, 8)
    local defaultSelectIndex = val > 0 and 1 or 0
    self.m_MianShiQnCheckBoxGroup = self:CreateQnCheckBoxGroup(checkboxtitles, function (index)
        local val = index == 0 and 0 or 1
        local tabBarKey2sliderId2val = {[8]= {[8] = val}}
        LuaPinchFaceMgr:SetFacialPartDataVal(tabBarKey2sliderId2val, true) 
    end, defaultSelectIndex)
end

function LuaPinchFaceSliderControlView:CreateHairColorModifyTemplate()
    local obj = NGUITools.AddChild(self.RightTable.gameObject, self.HairColorModifyTemplate.gameObject)
    obj:SetActive(true)
    local icon = obj.transform:Find("Container/Icon"):GetComponent(typeof(CUITexture))
    local button = obj.transform:Find("Button")
    self.m_HairColorModifyTemplateId = 21001450
    local itemData = Item_Item.GetData(self.m_HairColorModifyTemplateId)
    icon:LoadMaterial(itemData.Icon)
    self:InitHairColorModifyItem(obj)
    UIEventListener.Get(button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_HairColorModifyTemplateId)
        if count < 1 then
            g_MessageMgr:ShowMessage("PinchFace_ChangeHairColor_NoItem")
            return
        end
        local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, self.m_HairColorModifyTemplateId)
        if not (default and pos and itemId) then
            g_MessageMgr:ShowMessage("PinchFace_ChangeHairColor_NoItem")
            return
        end
        local advanceHair = CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.AdvancedHairColor
        local advanceHairColorId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.AdvancedHairColorId
        if (advanceHair and advanceHair ~= "") or (advanceHairColorId ~= 0) then
            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("CheckRanFaJi_AdvancedColor_Fade"), function ()
                Gac2Gas.TakeOffAdvancedHairColor(0)
                g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("PinchFace_ChangeHairColor_Confirm"), function ()
                    Gac2Gas.RequestChangeHairColor(itemId, EnumItemPlace.Bag, pos, LuaPinchFaceMgr.m_HeadId)
                end, nil, nil, nil, false)
            end, nil, nil, nil, false)
            return
        end
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("PinchFace_ChangeHairColor_Confirm"), function ()
            Gac2Gas.RequestChangeHairColor(itemId, EnumItemPlace.Bag, pos, LuaPinchFaceMgr.m_HeadId)
        end, nil, nil, nil, false)
    end)
    self.m_HairColorModifyItem = obj
    self:UpdateHairColorModifyItemVisible() 
end

function LuaPinchFaceSliderControlView:InitHairColorModifyItem(obj)
    local templateId = self.m_HairColorModifyTemplateId
    if not obj or CommonDefs.IsNull(obj) then
        return
    end
    local countLabel = obj.transform:Find("Container/CountLabel"):GetComponent(typeof(UILabel))
    local disableIcon = obj.transform:Find("Container/DisableIcon")
    local icon = obj.transform:Find("Container/Icon"):GetComponent(typeof(CUITexture))
    local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, templateId)
    if count < 1 then
        countLabel.text = SafeStringFormat3("[c][ff0000]%d[-][/c]/%d", count, 1)
        disableIcon.gameObject:SetActive(true)
    else
        countLabel.text = SafeStringFormat3("%d/%d", count, 1)
        disableIcon.gameObject:SetActive(false)
    end
    UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if count < 1 then
            CShopMallMgr.ShowLinyuShoppingMall(self.m_HairColorModifyTemplateId)
        else
            CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_HairColorModifyTemplateId)
        end
    end)
end

function LuaPinchFaceSliderControlView:UpdateHairColorModifyItemVisible()
    if not self.m_HairColorModifyItem or CommonDefs.IsNull(self.m_HairColorModifyItem) or not self.m_HairColorQnRadioBox or CommonDefs.IsNull(self.m_HairColorQnRadioBox) then
        return
    end
    if CClientMainPlayer.Inst then
        local appearanceProp = CClientMainPlayer.Inst.AppearanceProp
        local visible = self.m_HairColorQnRadioBox.CurrentSelectIndex ~= - 1 and self.m_HairColorQnRadioBox.CurrentSelectIndex ~= appearanceProp.HeadId 
        self.m_HairColorModifyItem.gameObject:SetActive(visible)
        self.RightTable:Reposition()
    end
end

function LuaPinchFaceSliderControlView:ResetModifyModeHairColor()
    if not LuaPinchFaceMgr.m_IsInModifyMode then return end
    if CClientMainPlayer.Inst then
        local appearanceProp = CClientMainPlayer.Inst.AppearanceProp
        if appearanceProp.HeadId ~= LuaPinchFaceMgr.m_HeadId then
            self:OnSelectHairColor(appearanceProp.HeadId)
        end
    end
end
--@endregion FacialSliderView

--@region UIEvent
function LuaPinchFaceSliderControlView:OnSelectHairColor(index, colorList)
    if not colorList then 
        colorList = self.m_HairColorList
    end
    if not colorList then
        return
    end
    if (LuaPinchFaceMgr.m_IsInModifyMode and LuaPinchFaceMgr.m_AdvancedHairColorIndex ~= 0) then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("CheckRanFaJi_AdvancedColor_Fade"), function ()
            self:ConfirmSelectHairColor(index, colorList)
        end, function()
            self.m_HairColorQnRadioBox:On_Click(nil)
            self.m_HairColorQnRadioBox.CurrentIndex = -1
            self:UpdateHairColorModifyItemVisible()
        end, nil, nil, false)
        return
    end
    self:ConfirmSelectHairColor(index, colorList)
end

function LuaPinchFaceSliderControlView:ConfirmSelectHairColor(index, colorList)
    local newHeadId = index
    local lastColor = colorList[LuaPinchFaceMgr.m_HeadId + 1]
    local newColor = colorList[newHeadId + 1]
    if LuaPinchFaceMgr:IsDressedFashionHair() then
        g_MessageMgr:ShowMessage("PinchFace_ChangeHairColor_TakeOffFashion")
    end
    LuaPinchFaceMgr:ChangeHairColor(lastColor, newColor, newHeadId)
    self:UpdateHairColorModifyItemVisible()
end

function LuaPinchFaceSliderControlView:OnSelectFacialColor(index, colorIdList, isChanged)
    local id = colorIdList[index + 1]
    local tabBarKey2colorid = {[self.m_FacialCurPartIndex] = id}
    if LuaPinchFaceMgr.m_LeftEyeFacialTabKey == self.m_FacialCurPartIndex then
        tabBarKey2colorid = {}
        if self.m_FacialEyeOptionIndex == 1 or self.m_FacialEyeOptionIndex == 2 then
            tabBarKey2colorid[LuaPinchFaceMgr.m_LeftEyeFacialTabKey] = id
        end
        if self.m_FacialEyeOptionIndex == 1 or self.m_FacialEyeOptionIndex == 3 then
            tabBarKey2colorid[LuaPinchFaceMgr.m_RightEyeFacialTabKey] = id
        end
    end
    LuaPinchFaceMgr:SetFacialFashionColor(tabBarKey2colorid, isChanged)
end

function LuaPinchFaceSliderControlView:OnCustomColorBtnClick()
    self:ShowFacialCustomColorSliderView(self.m_FacialTabBarArr,  self.m_FacialStyleData)
end

function LuaPinchFaceSliderControlView:OnFinishedCustomColorBtnClick()
    local sliderIndex1, sliderIndex2, sliderIndex3 = 0, 0, 0
    local key2hsv, tabBarKey2sliderId2val = nil, {}
    if self.m_FacialTabBarArr then
        for i = 0, self.m_FacialTabBarArr.Length - 1, 2 do
            local arr1, arr2 = self.m_FacialTabBarArr[i], self.m_FacialTabBarArr[i + 1]
            if arr1.Length == 1 and arr2 then
                local sliderId = tonumber(arr1[0])
                if self.m_FacialCustomColorSliderIdSet[sliderId] then
                    local v = LuaPinchFaceMgr:GetFacialPartDataVal(self.m_FacialCurPartIndex, sliderId)
                    local styleId = LuaPinchFaceMgr:GetPartStyleId(self.m_FacialCurPartIndex)
                    local defaultValue = self:FacialVal2SliderValue(self.m_FacialCurPartIndex, styleId, sliderId, v, 0, 1)
                    local key2id2Val = self:GetFacialOneDimenSliderData(self.m_FacialCurPartIndex, sliderId, defaultValue)
                    for k, id2Val in pairs(key2id2Val) do
                        if not tabBarKey2sliderId2val[k] then
                            tabBarKey2sliderId2val[k] = {}
                        end
                        for id, val in pairs(id2Val) do
                            tabBarKey2sliderId2val[k][id] = val
                        end
                    end
                end
            elseif arr1.Length == 2 and arr2 then
                local sliderId1, sliderId2 = tonumber(arr1[0]), tonumber(arr1[1])
                if self.m_FacialCustomColorSliderIdSet[sliderId1] and self.m_FacialCustomColorSliderIdSet[sliderId2] then
                    local styleId = LuaPinchFaceMgr:GetPartStyleId(self.m_FacialCurPartIndex)
                    local v1 = LuaPinchFaceMgr:GetFacialPartDataVal(self.m_FacialCurPartIndex, sliderId1)
                    local v2 = LuaPinchFaceMgr:GetFacialPartDataVal(self.m_FacialCurPartIndex, sliderId2)
                    local x = self:FacialVal2SliderValue(self.m_FacialCurPartIndex, styleId, sliderId2, v2, -1, 1)
                    local y = self:FacialVal2SliderValue(self.m_FacialCurPartIndex, styleId, sliderId1, v1, -1, 1)
                    local key2id2Val = self:GetFacialTwoDimenSliderData(self.m_FacialCurPartIndex, sliderId1, sliderId2, x, y)
                    for k, id2Val in pairs(key2id2Val) do
                        if not tabBarKey2sliderId2val[k] then
                            tabBarKey2sliderId2val[k] = {}
                        end
                        for id, val in pairs(id2Val) do
                            tabBarKey2sliderId2val[k][id] = val
                        end
                    end
                end
            elseif arr1.Length == 3 then
                sliderIndex1, sliderIndex2, sliderIndex3 = tonumber(arr1[0]), tonumber(arr1[1]), tonumber(arr1[2])
                local defaultHSV = LuaPinchFaceMgr:GetFacialPartDataColorHSV(self.m_FacialCurPartIndex, sliderIndex1, sliderIndex2, sliderIndex3, self.m_FacialStyleData)    
                key2hsv = {}
                local _, h = math.modf(defaultHSV.x)
                defaultHSV = Vector3(h, defaultHSV.y, defaultHSV.z)
                key2hsv[self.m_FacialCurPartIndex] = defaultHSV
                if self.m_FacialCurPartIndex == LuaPinchFaceMgr.m_LeftEyeFacialTabKey then
                    defaultHSV = LuaPinchFaceMgr:GetFacialPartDataColorHSV(LuaPinchFaceMgr.m_RightEyeFacialTabKey, sliderIndex1, sliderIndex2, sliderIndex3, self.m_FacialStyleData)
                    _, h = math.modf(defaultHSV.x)
                    defaultHSV = Vector3(h, defaultHSV.y, defaultHSV.z)
                    key2hsv[LuaPinchFaceMgr.m_RightEyeFacialTabKey] = defaultHSV
                end
            end
        end
    end
    self:ShowFacialSliderViewWithoutCustomColor(self.m_FacialTabBarArr,  self.m_FacialStyleData)
    if key2hsv and tabBarKey2sliderId2val and self.m_HasChangedCustomColor then
        LuaPinchFaceMgr:SetCustomColor(key2hsv, sliderIndex1, sliderIndex2, sliderIndex3, tabBarKey2sliderId2val, self.m_FacialStyleData, self.m_HasChangedCustomColor)
        self.m_HasChangedCustomColor = false
    end
    local ani = self.transform.parent:GetComponent(typeof(Animation))
    ani:Stop()
    ani:Play()
end
--@endregion UIEvent

function LuaPinchFaceSliderControlView:OnEnable()
    g_ScriptEvent:AddListener("OnFaceDNAChanged", self, "OnFaceDNAChanged")
    g_ScriptEvent:AddListener("OnFacialPartDataValChanged", self, "OnFacialPartDataValChanged")
    g_ScriptEvent:AddListener("OnChangeHairColor", self, "OnChangeHairColor")
    g_ScriptEvent:AddListener("OnSetFacialFashionColor", self, "OnSetFacialFashionColor")
    g_ScriptEvent:AddListener("OnSetFacialPartDataColorHSV", self, "OnSetFacialPartDataColorHSV")
    g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:AddListener("SendItem", self, "SendItem")
end

function LuaPinchFaceSliderControlView:OnDisable()
    g_ScriptEvent:RemoveListener("OnFaceDNAChanged", self, "OnFaceDNAChanged")
    g_ScriptEvent:RemoveListener("OnFacialPartDataValChanged", self, "OnFacialPartDataValChanged")
    g_ScriptEvent:RemoveListener("OnChangeHairColor", self, "OnChangeHairColor")
    g_ScriptEvent:RemoveListener("OnSetFacialFashionColor", self, "OnSetFacialFashionColor")
    g_ScriptEvent:RemoveListener("OnSetFacialPartDataColorHSV", self, "OnSetFacialPartDataColorHSV")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:RemoveListener("SendItem", self, "SendItem")
    self:ResetModifyModeHairColor()
end

function LuaPinchFaceSliderControlView:SendItem()
    if self.m_HairColorModifyTemplateId and self.m_HairColorModifyItem and not CommonDefs.IsUnityObjectNull(self.m_HairColorModifyItem) then
        self:InitHairColorModifyItem(self.m_HairColorModifyItem)
    end
end

function LuaPinchFaceSliderControlView:OnSetItemAt(args)
    if self.m_HairColorModifyTemplateId and self.m_HairColorModifyItem and not CommonDefs.IsUnityObjectNull(self.m_HairColorModifyItem) then
        self:InitHairColorModifyItem(self.m_HairColorModifyItem)
    end
end

function LuaPinchFaceSliderControlView:OnSetFacialPartDataColorHSV(key2hsv)
    local hsv = key2hsv[self.m_FacialCurPartIndex]
    if hsv then
        if self.m_CurCustomColorSlider then
            self.m_CurCustomColorSlider:SetHSV(hsv.x,hsv.y,hsv.z, true)
        end
    end
    local qnRadioBox = self.m_FacialColorQnRadioBox
    if qnRadioBox ~= nil and not CommonDefs.IsUnityObjectNull(qnRadioBox) and qnRadioBox.CurrentIndex >= 0 then
        qnRadioBox.m_RadioButtons[qnRadioBox.CurrentIndex]:SetSelected(false, false)
        qnRadioBox.CurrentIndex = -1  
    end
end

function LuaPinchFaceSliderControlView:OnSetFacialFashionColor(tabBarKey2colorid)
    local curKey = self.m_FacialCurPartIndex
    if LuaPinchFaceMgr.m_LeftEyeFacialTabKey == self.m_FacialCurPartIndex then
        if self.m_FacialEyeOptionIndex == 3 then
            curKey = LuaPinchFaceMgr.m_RightEyeFacialTabKey
        end
    end
    local newid = tabBarKey2colorid[curKey]
    local qnRadioBox = self.m_FacialColorQnRadioBox
    if qnRadioBox ~= nil and not CommonDefs.IsUnityObjectNull(qnRadioBox) and self.m_FacialColorIdList then
        local curid = self.m_FacialColorIdList[qnRadioBox.CurrentSelectIndex + 1]
        if newid and curid ~= newid then
            local defaultIndex = 0
            for i = 1, #self.m_FacialColorIdList do
                if self.m_FacialColorIdList[i] == newid then
                    defaultIndex = i - 1
                    qnRadioBox:ChangeTo(defaultIndex, false)
                    break
                end
            end
        elseif newid == nil and qnRadioBox.CurrentIndex >= 0 then
            qnRadioBox.m_RadioButtons[qnRadioBox.CurrentIndex]:SetSelected(false, false)
            qnRadioBox.CurrentIndex = -1
        end   
    end
end

function LuaPinchFaceSliderControlView:OnChangeHairColor(color, headId)
    if self.m_HairColorQnRadioBox ~= nil and not CommonDefs.IsNull(self.m_HairColorQnRadioBox) then
        if self.m_HairColorQnRadioBox.CurrentSelectIndex ~= headId then
            self.m_HairColorQnRadioBox:ChangeTo(headId, false)
            self:UpdateHairColorModifyItemVisible()
        end
    end
end
    
function LuaPinchFaceSliderControlView:OnFaceDNAChanged(id2ValueMap)
    if self.m_FaceOneDimenSliderMap then
        for sliderId, obj in pairs(self.m_FaceOneDimenSliderMap) do
            local value = id2ValueMap[sliderId]
            if not CommonDefs.IsUnityObjectNull(obj) and value then
                self:SetFaceOneDimenSliderDefaultValue(obj, sliderId)
            end
        end
    end
    if self.m_FaceTwoDimenSliderMap then
        for num, obj in pairs(self.m_FaceTwoDimenSliderMap) do
            local sliderId1, sliderId2 = math.modf(num / 10000)
            sliderId2 = num - sliderId1 * 10000
            local value1 = id2ValueMap[sliderId1]
            local value2 = id2ValueMap[sliderId2]
            if not CommonDefs.IsUnityObjectNull(obj) and value1 and value2 then
                self:SetFaceTwoDimenSliderDefaultValue(obj, sliderId1, sliderId2)
            end
        end
    end
end

function LuaPinchFaceSliderControlView:OnFacialPartDataValChanged(tabBarKey2sliderId2val)
    if self.m_FacialOneDimenSliderMap then
        local sliderId2val = tabBarKey2sliderId2val[self.m_FacialCurPartIndex]
        if sliderId2val then
            for sliderId, val in pairs(sliderId2val) do
                local obj = self.m_FacialOneDimenSliderMap[sliderId]
                if not CommonDefs.IsUnityObjectNull(obj) then
                    self:SetFacialOneDimenSliderDefaultValue(obj, self.m_FacialCurPartIndex,  sliderId)
                end
            end
        end
    end
    if self.m_FacialTwoDimenSliderMap then
        local sliderId2val = tabBarKey2sliderId2val[self.m_FacialCurPartIndex]
        if sliderId2val then
            for num, obj in pairs(self.m_FacialTwoDimenSliderMap) do
                local sliderId1, sliderId2 = math.modf(num / 10000)
                sliderId2 = num - sliderId1 * 10000
                local value1 = sliderId2val[sliderId1]
                local value2 = sliderId2val[sliderId2]
                if not CommonDefs.IsUnityObjectNull(obj) and value1 and value2 then
                    self:SetFacialTwoDimenSliderDefaultValue(obj, self.m_FacialCurPartIndex, sliderId1, sliderId2)
                end
            end
        end
    end
    if self.m_FacialCurPartIndex == 8 and self.m_MianShiQnCheckBoxGroup and not CommonDefs.IsUnityObjectNull(self.m_MianShiQnCheckBoxGroup) then
        local sliderId2val = tabBarKey2sliderId2val[self.m_FacialCurPartIndex]
        if sliderId2val[8] then
            local defaultSelectIndex = sliderId2val[8] > 0 and 1 or 0
            self.m_MianShiQnCheckBoxGroup:SetSelect(defaultSelectIndex, true, true)
        end
    end
end


function LuaPinchFaceSliderControlView:ShowTwoDimenSliderFx(slider, x, y)
	slider.transform:Find("CoordinateAxisBg/Bg/CUIFx_xh").gameObject:SetActive(x == 0 and y == 0)
	local lastObj = self:GetTwoDimenSliderFx(slider, self.m_TwoDimenSliderX, self.m_TwoDimenSliderY)
	local curObj = self:GetTwoDimenSliderFx(slider, x, y)
	if lastObj ~= curObj then 
		if lastObj then
			lastObj.gameObject:SetActive(true)
			local lastAni = lastObj:GetComponent(typeof(Animation))
			lastAni:Stop()
			lastAni:Play("pinchfacepreselectionview_xiangxian_hide")
		end
		if curObj then
			curObj.gameObject:SetActive(true)
			local curAni = curObj:GetComponent(typeof(Animation))
			curAni:Stop()
			curAni:Play("pinchfacepreselectionview_xiangxian_show")
		end
	end
    self.m_TwoDimenSliderX = x
    self.m_TwoDimenSliderY = y
end

function LuaPinchFaceSliderControlView:GetTwoDimenSliderFx(slider, x, y)
	if x and y then
		if x > 0 and y > 0 then
			return slider.transform:Find("CoordinateAxisBg/Bg/CUIFx_dianji/right1")
		elseif x > 0 and y < 0 then
			return slider.transform:Find("CoordinateAxisBg/Bg/CUIFx_dianji/right2")
		elseif x < 0 and y > 0 then
			return slider.transform:Find("CoordinateAxisBg/Bg/CUIFx_dianji/left1")
		elseif x < 0 and y < 0 then
			return slider.transform:Find("CoordinateAxisBg/Bg/CUIFx_dianji/left2")
		end
	end
	return nil
end