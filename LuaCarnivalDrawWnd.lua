local Item_Item    = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local Animation    = import "UnityEngine.Animation"

LuaCarnivalDrawWnd = class()

RegistClassMember(LuaCarnivalDrawWnd, "tabs")
RegistClassMember(LuaCarnivalDrawWnd, "selectTabId")
RegistClassMember(LuaCarnivalDrawWnd, "tableRoot")
RegistClassMember(LuaCarnivalDrawWnd, "rotateRoot")
RegistClassMember(LuaCarnivalDrawWnd, "silverIcon")
RegistClassMember(LuaCarnivalDrawWnd, "goldenIcon")
RegistClassMember(LuaCarnivalDrawWnd, "changeAnimation")
RegistClassMember(LuaCarnivalDrawWnd, "goldenGo")
RegistClassMember(LuaCarnivalDrawWnd, "rotateStopHint")
RegistClassMember(LuaCarnivalDrawWnd, "rotateStopHintTick")

RegistClassMember(LuaCarnivalDrawWnd, "bigTicketCount")
RegistClassMember(LuaCarnivalDrawWnd, "smallTicketCount")

RegistClassMember(LuaCarnivalDrawWnd, "bigOrder2ItemId")
RegistClassMember(LuaCarnivalDrawWnd, "smallOrder2ItemId")

RegistClassMember(LuaCarnivalDrawWnd, "tweener")
RegistClassMember(LuaCarnivalDrawWnd, "roundCount")
RegistClassMember(LuaCarnivalDrawWnd, "tweenDuration")
RegistClassMember(LuaCarnivalDrawWnd, "curAngle")

function LuaCarnivalDrawWnd:Awake()
	self:InitTabs()

	UIEventListener.Get(self.transform:Find("Anchor/Turntable/DrawButton").gameObject).onClick = LuaUtils.VoidDelegate(function (go)
		self:OnDrawButtonClick()
	end)
	self.tableRoot = self.transform:Find("Anchor/Turntable/Table")
	self.rotateRoot = self.transform:Find("Anchor/Turntable/RotateRoot")
	self.silverIcon = self.transform:Find("Anchor/Turntable/DrawButton/SilverIcon").gameObject
	self.goldenIcon = self.transform:Find("Anchor/Turntable/DrawButton/GoldenIcon").gameObject
	self.goldenGo = self.transform:Find("Anchor/Turntable/Golden").gameObject
	self.rotateStopHint = self.rotateRoot:Find("vfx_rotate_stop_hint").gameObject
	self.changeAnimtion = self.transform:Find("Anchor/Turntable"):GetComponent(typeof(Animation))
end

function LuaCarnivalDrawWnd:InitTabs()
	self.tabs = {}

	local goldenTab = self.transform:Find("Anchor/Tabs/Golden")
	local silverTab = self.transform:Find("Anchor/Tabs/Silver")
	local tabTransTbl = {goldenTab, silverTab}

	for i = 1, 2 do
		local trans = tabTransTbl[i]
		self.tabs[i] = {
			go = trans.gameObject,
			countLabel = trans:Find("Label"):GetComponent(typeof(UILabel)),
			selected = trans:Find("Selected").gameObject,
			animation = trans:GetComponent(typeof(Animation))
		}

		UIEventListener.Get(trans.gameObject).onClick = LuaUtils.VoidDelegate(function (go)
			self:OnTabClick(i)
		end)
	end
end


function LuaCarnivalDrawWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncSmallPrizeDrawIndex", self, "OnSyncSmallPrizeDrawIndex")
	g_ScriptEvent:AddListener("SyncBigPrizeDrawIndex", self, "OnSyncBigPrizeDrawIndex")
	g_ScriptEvent:AddListener("SyncCarnivalBigTicketCount", self, "OnSyncCarnivalTicketCount")
	g_ScriptEvent:AddListener("SyncCarnivalSmallTicketCount", self, "OnSyncCarnivalTicketCount")
end

function LuaCarnivalDrawWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncSmallPrizeDrawIndex", self, "OnSyncSmallPrizeDrawIndex")
	g_ScriptEvent:RemoveListener("SyncBigPrizeDrawIndex", self, "OnSyncBigPrizeDrawIndex")
	g_ScriptEvent:RemoveListener("SyncCarnivalBigTicketCount", self, "OnSyncCarnivalTicketCount")
	g_ScriptEvent:RemoveListener("SyncCarnivalSmallTicketCount", self, "OnSyncCarnivalTicketCount")
end

function LuaCarnivalDrawWnd:OnSyncSmallPrizeDrawIndex(getItemId)
	if self.selectTabId == 2 then
		self:GetAward(getItemId)
	end
end

function LuaCarnivalDrawWnd:OnSyncBigPrizeDrawIndex(getItemId)
	if self.selectTabId == 1 then
		self:GetAward(getItemId)
	end
end

function LuaCarnivalDrawWnd:OnSyncCarnivalTicketCount()
	self:UpdateTicketCount()
end

function LuaCarnivalDrawWnd:GetAward(getItemId)
	local order2ItemId = self.selectTabId == 1 and self.bigOrder2ItemId or self.smallOrder2ItemId
	local awardOrder = 1
	for order, itemId in pairs(order2ItemId) do
		if itemId == getItemId then
			awardOrder = order
			break
		end
	end

	-- 缓动函数 x^2
	self:ClearTween()
	local curAngle = self.curAngle % 360
	local targetAngle = 45 * (awardOrder - 1) + 360 * self.roundCount
	self.tweener = LuaTweenUtils.TweenFloat(-1, 0, self.tweenDuration, function(val)
		local angle = -1 * val * val * (targetAngle - curAngle) + targetAngle
		local newAngle = math.floor((angle / 45) + 0.5) * 45
		LuaUtils.SetLocalRotationZ(self.rotateRoot, newAngle)
		if newAngle == targetAngle then
			self:ClearTween()
		end
	end, function()
		LuaUtils.SetLocalRotationZ(self.rotateRoot, targetAngle)
		self.rotateStopHint:SetActive(false)
		self.rotateStopHint:SetActive(true)
		self.curAngle = targetAngle
		self:ClearTick()
		local num = self.selectTabId == 1 and JiaNianHua_BigPrizeWeight.GetData(getItemId).Num or JiaNianHua_SmallPrizeWeight.GetData(getItemId).Num
		self.rotateStopHintTick = RegisterTickOnce(function()
			local list = {
				{ItemID = getItemId, Count = num}
			}
			local buttonList = {
				{spriteName = "blue", buttonLabel = LocalString.GetString("确定"), clickCB = function() CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd) end},
			}
			LuaCommonGetRewardWnd.m_materialName = "gongxihuode"
			LuaCommonGetRewardWnd.m_Reward1List = list
			LuaCommonGetRewardWnd.m_Reward2Label = nil
			LuaCommonGetRewardWnd.m_Reward2List = {}
			LuaCommonGetRewardWnd.m_hint = ""
			LuaCommonGetRewardWnd.m_button = buttonList
			CUIManager.ShowUI(CLuaUIResources.CommonGetRewardWnd)
		end, 200)
	end)
end


function LuaCarnivalDrawWnd:Init()
	self:InitRotateParams()
	self:UpdateTicketCount()

	self:ParseOrder2ItemId()
	self.selectTabId = 0
	self:OnTabClick((self.bigTicketCount == 0 and self.smallTicketCount > 0) and 2 or 1)
end

function LuaCarnivalDrawWnd:InitRotateParams()
	self.roundCount = 3
	self.tweenDuration = 4
end

function LuaCarnivalDrawWnd:UpdateTicketCount()
	self.bigTicketCount = LuaCarnivalCollectMgr:GetBigTicketCount()
	self.smallTicketCount = LuaCarnivalCollectMgr:GetSmallTicketCount()
	self.tabs[1].countLabel.text = "x" .. self.bigTicketCount
	self.tabs[2].countLabel.text = "x" .. self.smallTicketCount
end

function LuaCarnivalDrawWnd:ParseOrder2ItemId()
	self.smallOrder2ItemId = {}
	JiaNianHua_SmallPrizeWeight.Foreach(function(itemId, data)
		self.smallOrder2ItemId[data.Order] = itemId
	end)

	self.bigOrder2ItemId = {}
	local hasFeiSheng = CClientMainPlayer.Inst and CClientMainPlayer.Inst.HasFeiSheng or false
	JiaNianHua_BigPrizeWeight.Foreach(function(itemId, data)
		if (itemId == 21000709 and hasFeiSheng) or (itemId == 21004564 and not hasFeiSheng) then
			return
		end
		self.bigOrder2ItemId[data.Order] = itemId
	end)
end

function LuaCarnivalDrawWnd:UpdateTable()
	local order2ItemId = self.selectTabId == 1 and self.bigOrder2ItemId or self.smallOrder2ItemId
	for i = 1, 8 do
		local itemId = order2ItemId[i]
		local child = self.tableRoot:GetChild(i - 1)
		local num = self.selectTabId == 1 and JiaNianHua_BigPrizeWeight.GetData(itemId).Num or JiaNianHua_SmallPrizeWeight.GetData(itemId).Num
		child:Find("Panel/Label"):GetComponent(typeof(UILabel)).text = num
		local icon = child:Find("Mask/Icon"):GetComponent(typeof(CUITexture))
		icon:LoadMaterial(Item_Item.GetData(itemId).Icon)

		UIEventListener.Get(icon.gameObject).onClick = LuaUtils.VoidDelegate(function (go)
			self:OnItemClick(itemId)
		end)
	end
end

function LuaCarnivalDrawWnd:ClearTween()
	if self.tweener then
		LuaTweenUtils.Kill(self.tweener, true)
	end
	self.tweener = nil
end

function LuaCarnivalDrawWnd:ClearTick()
	if self.rotateStopHintTick then
		UnRegisterTick(self.rotateStopHintTick)
		self.rotateStopHintTick = nil
	end
end

function LuaCarnivalDrawWnd:OnDestroy()
	self:ClearTween()
	self:ClearTick()
end


function LuaCarnivalDrawWnd:OnTabClick(i)
	if self.selectTabId == i then
		return
	end

	self:ClearTween()
	self.tabs[i].selected:SetActive(true)
	self.tabs[3 - i].selected:SetActive(false)
	LuaUtils.SetLocalRotationZ(self.rotateRoot, 0)
	self.curAngle = 0
	self.silverIcon:SetActive(i == 2)
	self.goldenIcon:SetActive(i == 1)
	self.goldenGo:SetActive(i == 1)
	if self.selectTabId > 0 then
		self.tabs[i].animation:Play()
		self.changeAnimtion:Play(i == 2 and "carnivaldrawwnd_turntable_change01" or "carnivaldrawwnd_turntable_change02")
	end
	self.selectTabId = i
	self:UpdateTable()
end

function LuaCarnivalDrawWnd:OnDrawButtonClick()
	if self.selectTabId == 1 then
		if self.bigTicketCount > 0 then
			Gac2Gas.RequestBigPrizeDraw()
		else
			g_MessageMgr:ShowMessage("CARNIVALCOLLECT_BIG_TICKET_NOT_ENOUGH")
		end
	elseif self.selectTabId == 2 then
		if self.smallTicketCount > 0 then
			Gac2Gas.RequestSmallPrizeDraw(1)
		else
			g_MessageMgr:ShowMessage("CARNIVALCOLLECT_SMALL_TICKET_NOT_ENOUGH")
		end
	end
end

function LuaCarnivalDrawWnd:OnItemClick(itemId)
	CItemInfoMgr.ShowLinkItemTemplateInfo(itemId)
end
