local LinkItemInfo = import "L10.UI.CItemInfoMgr+LinkItemInfo"
local MessageWndManager=import "L10.UI.MessageWndManager"
local CWordFilterMgr=import "L10.Game.CWordFilterMgr"
local CIndirectUIResources=import "L10.UI.CIndirectUIResources"
local ShowType = import "L10.UI.CItemInfoMgr+ShowType"
local CInputBoxMgr=import "L10.UI.CInputBoxMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CSkillInfoMgr=import "L10.UI.CSkillInfoMgr"
local EnumQualityType=import "L10.Game.EnumQualityType"
local CUITexture = import "L10.UI.CUITexture"
local IdPartition=import "L10.Game.IdPartition"
local CItemMgr=import "L10.Game.CItemMgr"
local EnumItemPlace=import "L10.Game.EnumItemPlace"
local Color = import "UnityEngine.Color"
local CUIMaterialPaths=import "L10.UI.CUIMaterialPaths"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local CLingShouBaseMgr = import "L10.Game.CLingShouBaseMgr"
local CSkillMgr=import "L10.Game.CSkillMgr"
local QnTabButton=import "L10.UI.QnTabButton"
local CScene = import "L10.Game.CScene"
local CommonDefs = import "L10.Game.CommonDefs"
local EnumChineseDigits = import "L10.Game.EnumChineseDigits"

CLuaPropertySwitchWnd = class()
RegistClassMember(CLuaPropertySwitchWnd,"m_Tabs")
RegistClassMember(CLuaPropertySwitchWnd,"m_Fabaos")
RegistClassMember(CLuaPropertySwitchWnd,"m_EquipIcons")
RegistClassMember(CLuaPropertySwitchWnd,"m_FabaoTitleLabel")
RegistClassMember(CLuaPropertySwitchWnd,"m_EquipPositions")
RegistClassMember(CLuaPropertySwitchWnd,"m_Equips")
RegistClassMember(CLuaPropertySwitchWnd,"m_LingShouItems")
RegistClassMember(CLuaPropertySwitchWnd,"m_SkillItems")
RegistClassMember(CLuaPropertySwitchWnd,"m_SkillNames")
RegistClassMember(CLuaPropertySwitchWnd,"m_SkillTitleLabel")
RegistClassMember(CLuaPropertySwitchWnd,"m_TianFuSkillItems")
RegistClassMember(CLuaPropertySwitchWnd,"m_SwitchButtons")
RegistClassMember(CLuaPropertySwitchWnd,"m_TabView")
RegistClassMember(CLuaPropertySwitchWnd,"m_GroupNameLabel")
RegistClassMember(CLuaPropertySwitchWnd,"m_ConfirmButton")
RegistClassMember(CLuaPropertySwitchWnd,"m_ShengXiaoMark")
RegistClassMember(CLuaPropertySwitchWnd,"m_IndexLabel")

RegistClassMember(CLuaPropertySwitchWnd,"m_Tips")


CLuaPropertySwitchWnd.m_GroupIndex=0
CLuaPropertySwitchWnd.m_SelectLingShouId=0
-- CLuaPropertySwitchWnd.m_RequestedGroupIndex=0

function CLuaPropertySwitchWnd:ChangeName()
    local byteCount = 8

    local text = SafeStringFormat3(LocalString.GetString("名称最多%d个字符(%d个汉字)"), byteCount, math.floor(byteCount / 2))
    if CommonDefs.IsSeaMultiLang() and (LocalString.language == "en" or LocalString.language == "ina") then
        text = SafeStringFormat3(LocalString.GetString("名称最多%d个字母"), byteCount)
    end

    CInputBoxMgr.ShowInputBox(text, DelegateFactory.Action_string(function (newName) 
        newName = StringTrim(newName)
        if newName==nil or newName=="" then
            g_MessageMgr:ShowMessage("ATTRGROUP_NAME_LEN_ERR")
            return
        end
        if CUICommonDef.GetStrByteLength(newName) > 8 then
            g_MessageMgr:ShowMessage("ATTRGROUP_NAME_LEN_ERR")
            return
        end
        if not CWordFilterMgr.Inst:CheckName(newName, LocalString.GetString("一键切换属性时的组合名")) then
            g_MessageMgr:ShowMessage("Name_Violation")
            return
        end
        Gac2Gas.RequestSetNameInAttrGroup(CLuaPropertySwitchWnd.m_GroupIndex,newName)
    end), 32, true, nil, nil)
end

function CLuaPropertySwitchWnd:Awake()
    self.m_SkillNames = {
        LocalString.GetString("技能·一"),
        LocalString.GetString("技能·二"),
        LocalString.GetString("技能·三"),
        LocalString.GetString("技能·四"),
        LocalString.GetString("技能·五"),
    }
    self.m_EquipIcons = {
        -- CUIMaterialPaths.EquipSlotWeapon,
        -- "UI/Texture/Item_Equipment/Wuqi/Icon/Material/wp_001_01-b.mat",
        "UI/Texture/NonTransparent/Material/yijianhuanzhuang_citiao_01.mat",
        CUIMaterialPaths.EquipSlotHeadwear,
        CUIMaterialPaths.EquipSlotNecklace,

        CUIMaterialPaths.EquipSlotBracelet,
        -- CUIMaterialPaths.EquipSlotClothes,
        -- "UI/Texture/Item_Equipment/Jiazhou/Icon/Material/jinshukuijia_01-b.mat",
        "UI/Texture/NonTransparent/Material/yijianhuanzhuang_citiao_02.mat",
        CUIMaterialPaths.EquipSlotBracelet,

        CUIMaterialPaths.EquipSlotRing,
        -- CUIMaterialPaths.EquipSlotShield,
        -- "UI/Texture/Item_Equipment/Dunpai/Icon/Material/dun_05-b.mat",
        "UI/Texture/NonTransparent/Material/yijianhuanzhuang_citiao_03.mat",
        CUIMaterialPaths.EquipSlotRing,

        CUIMaterialPaths.EquipSlotGlove,
        CUIMaterialPaths.EquipSlotBelt,
        CUIMaterialPaths.EquipSlotShoe,
    }
    
    self.m_EquipPositions = {
        LuaEnumBodyPosition.Weapon,
        LuaEnumBodyPosition.Headwear,
        LuaEnumBodyPosition.Necklace,

        LuaEnumBodyPosition.LeftBracelet,
        LuaEnumBodyPosition.Clothes,
        LuaEnumBodyPosition.RightBracelet,

        LuaEnumBodyPosition.LeftRing,
        LuaEnumBodyPosition.Shield,
        LuaEnumBodyPosition.RightRing,

        LuaEnumBodyPosition.Gloves,
        LuaEnumBodyPosition.Belt,
        LuaEnumBodyPosition.Shoes,
    }
    local tipButton = self.transform:Find("TipButton").gameObject
    UIEventListener.Get(tipButton).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("SXQH_Tip")
    end)
    self.m_SwitchButtons = {}
    self.m_Tips={}

    local tf = self.transform:Find("SwitchButtons")
    for i=1,5 do
        local btn = tf:GetChild(i-1):GetComponent(typeof(QnTabButton))
        table.insert( self.m_SwitchButtons,btn )

        UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function(g)
            self:OnClickTab(g)
        end)
        local tip = btn.transform:Find("Tip").gameObject
        table.insert(self.m_Tips,tip)
        local lock=btn.transform:Find("Lock")--.gameObject
        if lock then
            lock.gameObject:SetActive(false)
        end
    end
end

function CLuaPropertySwitchWnd:OnClickTab(g)
    local cmp = g:GetComponent(typeof(QnTabButton))
    local idx = 0
    for i,v in ipairs(self.m_SwitchButtons) do
        if cmp==v then
            idx=i
            break
        end
    end
    if idx>0 then
        if idx>=3 and not self:IsAttrGroupUnlocked(3) then
            local lingyu = AttrGroup_Group.GetData(3).LingYu
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("YiJianHuanZhuang_NeedMoneyConfig",3,lingyu), DelegateFactory.Action(function () 
                Gac2Gas.RequestUnlockAttrGroup(3)
            end), nil, nil, nil, false)
            return
        elseif idx>=4 and not self:IsAttrGroupUnlocked(4) then
            local lingyu = AttrGroup_Group.GetData(4).LingYu
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("YiJianHuanZhuang_NeedMoneyConfig",4,lingyu), DelegateFactory.Action(function () 
                Gac2Gas.RequestUnlockAttrGroup(4)
            end), nil, nil, nil, false)
            return
        elseif idx>=5 and not self:IsAttrGroupUnlocked(5) then
            local lingyu = AttrGroup_Group.GetData(5).LingYu
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("YiJianHuanZhuang_NeedMoneyConfig",5,lingyu), DelegateFactory.Action(function () 
                Gac2Gas.RequestUnlockAttrGroup(5)
            end), nil, nil, nil, false)
            return
        end
        self:ChangeIndex(idx)
    end
end

function CLuaPropertySwitchWnd:ChangeIndex(idx)
    for i,v in ipairs(self.m_SwitchButtons) do
        if idx==i then
            v.Selected = true
            self:OnClickSwitchButton(i)
        else
            v.Selected=false
        end
    end
end

function CLuaPropertySwitchWnd:Init()
    CLuaPropertySwitchWnd.m_GroupIndex = 1

    self.m_IndexLabel=self.transform:Find("IndexLabel"):GetComponent(typeof(UILabel))
    self.m_ShengXiaoMark = self.transform:Find("shengxiao").gameObject
    self.m_ConfirmButton = self.transform:Find("ConfirmButton").gameObject
    UIEventListener.Get(self.m_ConfirmButton).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.RequestSwitchAttrGroup(CLuaPropertySwitchWnd.m_GroupIndex)
    end)


    local changeNameButton = self.transform:Find("ChangNameBtn").gameObject
    UIEventListener.Get(changeNameButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:ChangeName()
    end)

    self.m_GroupNameLabel = changeNameButton.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
    self.m_GroupNameLabel.text= nil

    self.m_SkillItems = {}
    self.m_TianFuSkillItems = {}
    self.m_LingShouItems = {}
    self.m_Fabaos = {}
    self.m_Equips={}


    local skillTf = FindChild(self.transform,"Skill")
    self.m_SkillTitleLabel = skillTf:Find("TitleLabel"):GetComponent(typeof(UILabel))
    local tianfuRoot = skillTf:Find("TianFuRoot")
    local tianfuItem = tianfuRoot:Find("TianFuSkillItem").gameObject
    tianfuItem:SetActive(false)

    local grid = tianfuRoot:Find("TianFuSkillGrid")
    Extensions.RemoveAllChildren(grid.transform)
    for i=1,4 do
        local go = NGUITools.AddChild(grid.gameObject,tianfuItem)
        go:SetActive(true)
        table.insert( self.m_TianFuSkillItems, go )
    end
    grid:GetComponent(typeof(UIGrid)):Reposition()


    local skillButtonRoot = skillTf:Find("SkillButtonRoot")
    for i=1,5 do
        local go = skillButtonRoot:GetChild(i-1).gameObject
        table.insert( self.m_SkillItems, go )
    end
    local selectButton = skillTf:Find("SelectButton").gameObject


    UIEventListener.Get(selectButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickSkillSelectButton(go)
    end)


    local fabaoTf= FindChild(self.transform,"FaBao")
    self.m_FabaoTitleLabel = fabaoTf:Find("TitleLabel"):GetComponent(typeof(UILabel))
    local fabaoItem = fabaoTf:Find("TalismanTemplate").gameObject
    fabaoItem:SetActive(false)
    local grid = fabaoTf:Find("Grid")
    local texts = {
        LocalString.GetString("乾"),
        LocalString.GetString("巽"),
        LocalString.GetString("坎"),
        LocalString.GetString("艮"),

        LocalString.GetString("坤"),
        LocalString.GetString("震"),
        LocalString.GetString("离"),
        LocalString.GetString("兑"),
    }
    Extensions.RemoveAllChildren(grid.transform)
    for i=1,8 do
        local go = NGUITools.AddChild(grid.gameObject,fabaoItem)
        go:SetActive(true)
        table.insert( self.m_Fabaos,go )
        local label = go.transform:Find("Position"):GetComponent(typeof(UILabel))
        label.text = texts[i]
    end
    grid:GetComponent(typeof(UIGrid)):Reposition()

    local selectButton = fabaoTf:Find("SelectButton").gameObject
    UIEventListener.Get(selectButton).onClick = DelegateFactory.VoidDelegate(function(go)
        if not CClientMainPlayer.Inst then return end
        CLuaPropertySwitchPopupMenu.m_Mode = 1
        CUIManager.ShowUI(CLuaUIResources.PropertySwitchPopupMenu)
    end)

    local equipTf = FindChild(self.transform,"Equip")
    local equipItem = equipTf:Find("EquipItem").gameObject
    equipItem:SetActive(false)
    local grid = equipTf:Find("Grid")
    Extensions.RemoveAllChildren(grid.transform)
    for i=1,12 do
        local go = NGUITools.AddChild(grid.gameObject,equipItem)
        go:SetActive(true)
        table.insert( self.m_Equips,go )
    end
    grid:GetComponent(typeof(UIGrid)):Reposition()

    self:InitEquips()

    local lingshouTf = FindChild(self.transform,"LingShou")
    local item1 = lingshouTf:Find("Item1").gameObject
    local item2 = lingshouTf:Find("Item2").gameObject
    local item3 = lingshouTf:Find("Item3").gameObject
    table.insert( self.m_LingShouItems, item1 )
    table.insert( self.m_LingShouItems, item2 )
    table.insert( self.m_LingShouItems, item3 )
    for i,v in ipairs(self.m_LingShouItems) do
        UIEventListener.Get(v).onClick = DelegateFactory.VoidDelegate(function(go)
            self:SetLingShou(i)
        end)
    end


    --请求灵兽信息
    Gac2Gas.RequestAllLingShouOverview(0)--lingshou
    self:InitTabStates()

	--未解锁请求检查解锁
	local playProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp
	if playProp and playProp.AttrGroupUnlock == 0 then
		Gac2Gas.RequestUnlockAttrGroup(0)
	end
end

function CLuaPropertySwitchWnd:SetLingShou(type)
    if not CClientMainPlayer.Inst then return end
    CLuaPropertySwitchWnd.m_SelectLingShouId = nil
    local v = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.AttrGroups,CLuaPropertySwitchWnd.m_GroupIndex)
    if v then
        if type==1 then
            CLuaPropertySwitchWnd.m_SelectLingShouId = v.BattleLingShouId
        elseif type==2 then
            CLuaPropertySwitchWnd.m_SelectLingShouId = v.AssistLingShouId
        else
            CLuaPropertySwitchWnd.m_SelectLingShouId = v.BindedPartnerLingShouId
        end
    else
        if type==1 then
            CLuaPropertySwitchWnd.m_SelectLingShouId = CClientMainPlayer.Inst.BasicProp.LastLingShouId
        elseif type==2 then
            CLuaPropertySwitchWnd.m_SelectLingShouId = CClientMainPlayer.Inst.BasicProp.AssistLingShouId
        else
            CLuaPropertySwitchWnd.m_SelectLingShouId = CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId
        end
    end
    CLuaLingShouOtherMgr.QueryMainPlayerLingShouForProperySwitch()
end

function CLuaPropertySwitchWnd:OnClickSwitchButton(idx)
    CLuaPropertySwitchWnd.m_GroupIndex = idx
    self.m_IndexLabel.text = tostring(CLuaPropertySwitchWnd.m_GroupIndex)
    self:InitAttrGroup(idx)

end

function CLuaPropertySwitchWnd:InitTabStates()
    local function setlock(index,state)
        local tf = self.m_SwitchButtons[index].transform
        local lock = tf:Find("Lock").gameObject
        lock:SetActive(state)
        local label = tf:Find("Label").gameObject
        label:SetActive(not state)
    end
    if not self:IsAttrGroupUnlocked(3) then
        setlock(3,true)
        setlock(4,true)
        setlock(5,true)
    elseif not self:IsAttrGroupUnlocked(4) then
        setlock(3,false)
        setlock(4,true)
        setlock(5,true)
    elseif not self:IsAttrGroupUnlocked(5) then
        setlock(3,false)
        setlock(4,false)
        setlock(5,true)
    else
        setlock(3,false)
        setlock(4,false)
        setlock(5,false)
    end
end

function CLuaPropertySwitchWnd:OnClickSkillSelectButton(go)
    if not CClientMainPlayer.Inst then return end
    CLuaPropertySwitchPopupMenu.m_Mode = 0
    CUIManager.ShowUI(CLuaUIResources.PropertySwitchPopupMenu)
end

function CLuaPropertySwitchWnd:InitSkills(templateIdx)
    local skillProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.SkillProp
    self.m_SkillTitleLabel.text = skillProp and skillProp.SkillTemplateName[templateIdx] or LocalString.GetString("新组合")-- self.m_SkillNames[templateIdx]

    if not CClientMainPlayer.Inst then return end
    local skillProp = CClientMainPlayer.Inst.SkillProp
    local playerLevel = CClientMainPlayer.Inst.Level
    local isInXianShenStatus = CClientMainPlayer.Inst.IsInXianShenStatus

    local tianfuSkills = {}


    local baseIdx = (templateIdx-1)*5

    local playerCls = CClientMainPlayer.Inst.BasicProp.Class
    local xiuwei = CClientMainPlayer.Inst.BasicProp.XiuWeiGrade
    local xiuweiRequire = {40,60,80,100}

    local defaultSkill = 0
    local klass = CClientMainPlayer.Inst.BasicProp.Class
    Initialization_Init.Foreach(function(k,v)
        if v.Class == klass then
            defaultSkill=v.DefaultSkill
        end
    end)
	for i = 1,5 do
        local skillCls = skillProp.NewActiveSkillTemplate[baseIdx+i]
        local go = self.m_SkillItems[i]
        local icon = go.transform:Find("Mask/__SkillIcon__"):GetComponent(typeof(CUITexture))
        local colorIcon = go.transform:Find("Panel/ColorSystemIcon"):GetComponent(typeof(UISprite))
        colorIcon.gameObject:SetActive(false)

        if skillCls and (skillCls~=0) then
            --如果是色系技能cls，则先取其基础技能cls，并从SkillMap中拿到原始技能id 。。。
            local skillId = 0
            if Skill_ColorSkill.Exists(skillCls) then--是否是色系技能
                local originSkillCls =Skill_ColorSkill.GetData(skillCls).BaseSkill_ID
                local originSkillId = skillProp:GetSkillIdWithDeltaByCls(originSkillCls,playerLevel)

                local skillLevel = originSkillId % 100
                skillId = skillCls*100 + skillLevel

                colorIcon.gameObject:SetActive(true)
                colorIcon.spriteName = CSkillMgr.Inst:GetColorSystemSkillIcon(skillCls)
            elseif CSkillMgr.Inst:IsColorSystemBaseSkill(skillCls) then
                skillId = skillProp:GetSkillIdWithDeltaByCls(skillCls,playerLevel)

                colorIcon.gameObject:SetActive(true)
                colorIcon.spriteName = CSkillMgr.Inst:GetColorSystemSkillIcon(skillId)
            else
                skillId = skillProp:GetSkillIdWithDeltaByCls(skillCls, playerLevel)
                if skillId == 0 then
                    skillId = defaultSkill
                end
            end
            

            local skillData = Skill_AllSkills.GetData(skillId)
            if skillData then
                icon:LoadSkillIcon(skillData.SkillIcon)
                UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function(g)
                    CLuaSkillInfoWnd.FromAttrGroup = true
                    CSkillInfoMgr.ShowSkillInfoWnd(skillId,true,0,0,nil)
                end)
            else
                icon:LoadSkillIcon(nil)
                UIEventListener.Get(go).onClick = nil
            end
        else
            icon:LoadSkillIcon(nil)
            UIEventListener.Get(go).onClick = nil
        end

        local tianfuSkillId = 0
        for j = 1, 5 do
            local skillId = skillProp.TianFuSkillTemplate[baseIdx+j]
            local data = Skill_AllSkills.GetData(skillId)
            if data and data.NeedXiuweiLevel == xiuweiRequire[i] then
                tianfuSkillId = skillId
                break
            end
        end
        tianfuSkillId = self:ProcessTianFuSkillId(tianfuSkillId,skillProp,playerLevel)


        if i<5 then--40、60、80、100
            local go = self.m_TianFuSkillItems[i]
            local tianfuIcon = go.transform:Find("SkillIcon"):GetComponent(typeof(CUITexture))
            local passiveIcon = go.transform:Find("PassiveIcon").gameObject
            local lockSprite = go.transform:Find("LockedSprite").gameObject
            local levelLabel = go.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
            levelLabel.gameObject:SetActive(false)
            passiveIcon:SetActive(false)

            if xiuwei<xiuweiRequire[i] then
                lockSprite:SetActive(true)
            else
                lockSprite:SetActive(false)
                passiveIcon:SetActive(false)
                if tianfuSkillId and (tianfuSkillId~=0) then
                    local tianfuSkillCls = math.floor(tianfuSkillId/100)
                    local tianfuSkillPlayerCls = math.floor(tianfuSkillId/100/1000)%100--GetSkillPlayerClsById
                    if tianfuSkillPlayerCls ~= playerCls then
                        tianfuIcon:LoadSkillIcon(nil)
                        UIEventListener.Get(go).onClick = nil
                    else
                        local skillData = Skill_AllSkills.GetData(tianfuSkillId)
                        if skillData then
                            tianfuIcon:LoadSkillIcon(skillData.SkillIcon)
                            passiveIcon:SetActive(skillData.Category == 0)--被动技能
                            levelLabel.gameObject:SetActive(true)

                            if isInXianShenStatus then
                                local skillLv = skillProp:GetFeiShengModifyLevel(tianfuSkillId,skillData.Kind,playerLevel)
                                if skillLv~=skillData.Level then

                                    levelLabel.text = SafeStringFormat3("[c][ff7900]%d[-][/c]",self:AdjustTianFuSkillLevelByShenBing(skillLv, skillData, playerLevel, skillProp))
                                else
                                    levelLabel.text = tostring(self:AdjustTianFuSkillLevelByShenBing(skillData.Level, skillData, playerLevel, skillProp))
                                end
                            else
                                levelLabel.text = tostring(self:AdjustTianFuSkillLevelByShenBing(skillData.Level, skillData, playerLevel, skillProp))
                            end
                            UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(go)
                                CLuaSkillInfoWnd.FromAttrGroup = true
                                CSkillInfoMgr.ShowSkillInfoWnd(tianfuSkillId,true,0,0,nil)
                            end)
                        end
                    end
                else
                    tianfuIcon:LoadSkillIcon(nil)
                    UIEventListener.Get(go).onClick = nil
                end
            end
        end
    end
end

function CLuaPropertySwitchWnd:AdjustTianFuSkillLevelByShenBing(originLevel, skillData, playerLevel, skillProp)
    local originWithDeltaLevel = skillProp:GetTianFuSkilleltaLevel(math.floor(skillData.ID/100), playerLevel)
    local totalLevel = originLevel
    if originLevel > 0 and originWithDeltaLevel > 0 then
        totalLevel = (originLevel + originWithDeltaLevel)
        if totalLevel > skillData._Before_Extend_Level then
            totalLevel = skillData._Before_Extend_Level
        end
    end
    return totalLevel
end

--修正等级
function CLuaPropertySwitchWnd:ProcessTianFuSkillId(tianfuSkillId,skillProp,playerLevel)
    -- local classId = math.floor(tianfuSkillId/100)
    -- local originLevel = tianfuSkillId%100
    -- local originWithDeltaLevel = skillProp:GetTianFuSkilleltaLevel(classId,playerLevel)

    -- local totalLevel = originLevel
    -- if originLevel>0 and originWithDeltaLevel>0 then
    --     totalLevel = originLevel+originWithDeltaLevel
    -- end

    -- return classId*100+totalLevel
    return tianfuSkillId
end

function CLuaPropertySwitchWnd:InitFaBao(idx)
    self.m_FabaoTitleLabel.text = LocalString.GetString("法宝·") .. EnumChineseDigits.GetDigit(idx)

    local startIdx = LuaEnumBodyPosition.TalismanQian
    local endIdx = LuaEnumBodyPosition.TalismanDui
    startIdx = startIdx + (idx-1)*8
    endIdx = endIdx + (idx-1)*8

    local itemProp = CClientMainPlayer.Inst.ItemProp
    local itemIds = {}
    for i=startIdx,endIdx do
        local itemId = itemProp:GetItemAt(EnumItemPlace.Body, i) or ""
        table.insert( itemIds,itemId )
    end

    
    for i,v in ipairs(itemIds) do
        self:InitFaBaoItem(self.m_Fabaos[i].transform,v)
    end

end
function CLuaPropertySwitchWnd:InitFaBaoItem(transform,itemId)
    local iconTexture = transform:Find("Panel/Texture"):GetComponent(typeof(CUITexture))
    local qualitySprite = transform:Find("Quality"):GetComponent(typeof(UISprite))
    local talisman = CItemMgr.Inst:GetById(itemId)
    if talisman and IdPartition.IdIsTalisman(talisman.TemplateId) then
        iconTexture:LoadMaterial(talisman.Icon)
        qualitySprite.color = talisman.Equip.DisplayColor
        -- disableSprite.gameObject:SetActive(talisman.Equip.Disable > 0)
        UIEventListener.Get(transform.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            CItemInfoMgr.ShowLinkItemInfo(talisman, false, nil, AlignType.Default, 0, 0, 0, 0, 0)
        end)
    else
        -- disableSprite.gameObject:SetActive(false)
        iconTexture:Clear()
        qualitySprite.color = Color.white
        UIEventListener.Get(transform.gameObject).onClick = nil
    end

end



function CLuaPropertySwitchWnd:InitEquips()
    if not CClientMainPlayer.Inst then return end
    local itemProp = CClientMainPlayer.Inst.ItemProp

    for i,v in ipairs(self.m_EquipPositions) do
        local go = self.m_Equips[i]
        local icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
        local background = go.transform:Find("Bg"):GetComponent(typeof(UISprite))
        local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
        local itemId = itemProp:GetItemAt(EnumItemPlace.Body, v)
        local type = EnumQualityType.None

        local qualitySprite = go:GetComponent(typeof(UISprite))
        if itemId and itemId~="" then
            local item = CItemMgr.Inst:GetById(itemId)
            if item then
                type = item.Equip.Color
                icon:LoadMaterial(item.Equip.Icon)
                CUICommonDef.SetActive(icon.gameObject,true,true)
                qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item.Equip.QualityType)

                UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(go)
                    self:OnClickEquip(itemId,v)
                end)
                label.gameObject:SetActive(true)

                local wordSetIdx = item.Equip.ActiveWordSetIdx
                local holeSetIdx = item.Equip.ActiveHoleSetIdx
                local wordSetStr = wordSetIdx==2 and LocalString.GetString("二") or LocalString.GetString("一")
                local holeSetStr = holeSetIdx==2 and LocalString.GetString("二") or LocalString.GetString("一")
                label.text = wordSetStr.."·"..holeSetStr
            end
        else
            -- label.text = LocalString.GetString("一").."·"..LocalString.GetString("一")
            label.gameObject:SetActive(false)
            if i==8 and CClientMainPlayer.Inst and CommonDefs.IsZhanKuang(CClientMainPlayer.Inst.Class) then
                icon:LoadMaterial("UI/Texture/NonTransparent/Material/yijianhuanzhuang_citiao_01.mat")
                icon.uiTexture.flip = UIBasicSprite.Flip.Horizontally
            else
                icon:LoadMaterial(self.m_EquipIcons[i])
                icon.uiTexture.flip = UIBasicSprite.Flip.Nothing
            end
            CUICommonDef.SetActive(icon.gameObject,false,true)
            qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
            UIEventListener.Get(go).onClick = nil
        end
    end
end

function CLuaPropertySwitchWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateAttrGroupAt", self, "OnUpdateAttrGroupAt")
    g_ScriptEvent:AddListener("GetAllLingShouOverview", self, "OnGetAllLingShouOverview")
    g_ScriptEvent:AddListener("ReplyEquipInfoInAttrGroup",self,"OnReplyEquipInfoInAttrGroup")
    g_ScriptEvent:AddListener("SwitchAttrGroupSuccess",self,"OnSwitchAttrGroupSuccess")
    g_ScriptEvent:AddListener("UpdateAttrGroupUnlock",self,"OnUpdateAttrGroupUnlock")
    
end
function CLuaPropertySwitchWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateAttrGroupAt", self, "OnUpdateAttrGroupAt")
    g_ScriptEvent:RemoveListener("GetAllLingShouOverview", self, "OnGetAllLingShouOverview")
    g_ScriptEvent:RemoveListener("ReplyEquipInfoInAttrGroup",self,"OnReplyEquipInfoInAttrGroup")
    g_ScriptEvent:RemoveListener("SwitchAttrGroupSuccess",self,"OnSwitchAttrGroupSuccess")
    g_ScriptEvent:RemoveListener("UpdateAttrGroupUnlock",self,"OnUpdateAttrGroupUnlock")

end
function CLuaPropertySwitchWnd:OnUpdateAttrGroupUnlock()
    self:InitTabStates()
end
function CLuaPropertySwitchWnd:OnSwitchAttrGroupSuccess(groupIdx)
    for i=1,5 do
        self.m_Tips[i]:SetActive(groupIdx==i)
    end
    if CLuaPropertySwitchWnd.m_GroupIndex==groupIdx then
        self.m_ConfirmButton:SetActive(false)
        self.m_ShengXiaoMark:SetActive(true)
    else
        self.m_ConfirmButton:SetActive(true)
        self.m_ShengXiaoMark:SetActive(false)
    end

    local v = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.AttrGroups,groupIdx)
    if v then
        self:InitLingShou(v.BattleLingShouId,v.AssistLingShouId,v.BindedPartnerLingShouId)
        self:InitEquipSetIdxes(v.BodyEquipWordSetIdxes,v.BodyEquipHoleSetIdxes)
    end
end

function CLuaPropertySwitchWnd:OnReplyEquipInfoInAttrGroup(itemId, wordSetIdx, holeSetIdx, gemGroupId, feiShengAdjust,item)
    CLuaEquipInfoWnd.FromAttrGroup = true
    CItemInfoMgr.showType = ShowType.Link
    CItemInfoMgr.linkItemInfo = LinkItemInfo(item, false, nil)
    if item ~= nil then
        CUIManager.ShowUI(CIndirectUIResources.EquipInfoWnd)
    end
end


function CLuaPropertySwitchWnd:SetGroupName(groupIdx,name)
    if name==nil or name=="" then
        if groupIdx==1 or groupIdx==0 then
            self.m_GroupNameLabel.text=LocalString.GetString("组合一")
        elseif groupIdx==2 then
            self.m_GroupNameLabel.text=LocalString.GetString("组合二")
        elseif groupIdx==3 then
            self.m_GroupNameLabel.text=LocalString.GetString("组合三")
        elseif groupIdx==4 then
            self.m_GroupNameLabel.text=LocalString.GetString("组合四")
        elseif groupIdx==5 then
            self.m_GroupNameLabel.text=LocalString.GetString("组合五")
        end
    else
        self.m_GroupNameLabel.text= name
    end
end

function CLuaPropertySwitchWnd:OnUpdateAttrGroupAt(groupIdx,eItem,group)
    -- print(groupIdx,eItem,group)
    if eItem == LuaEnumAttrGroupItem.Name then
        -- self.m_GroupNameLabel.text= group.Name
        self:SetGroupName(groupIdx,group.Name)
    elseif eItem ==LuaEnumAttrGroupItem.SkillTemplate then
        self:InitSkills(group.SkillTemplateIdx)
    elseif eItem ==LuaEnumAttrGroupItem.TalismanSet then
        self:InitFaBao(group.TalismanSetIdx)
    elseif eItem ==LuaEnumAttrGroupItem.BodyEquipWordSetIdx or eItem ==LuaEnumAttrGroupItem.BodyEquipHoleSetIdx then
        self:InitEquipSetIdxes(group.BodyEquipWordSetIdxes,group.BodyEquipHoleSetIdxes)
    elseif eItem ==LuaEnumAttrGroupItem.LingShou or eItem ==LuaEnumAttrGroupItem.AssistLingShou or eItem ==LuaEnumAttrGroupItem.BindedPartnerLingShou then
        self:InitLingShou(group.BattleLingShouId,group.AssistLingShouId,group.BindedPartnerLingShouId)
    elseif eItem == LuaEnumAttrGroupItem.Any then
        self:InitAttrGroup(CLuaPropertySwitchWnd.m_GroupIndex)
    end
end

function CLuaPropertySwitchWnd:InitAttrGroup(groupIdx)
    if not CClientMainPlayer.Inst then return nil end

    local activeIdx = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.ActiveAttrGroupIdx or 1
    if activeIdx==0 then
        activeIdx=1
    end
    for i=1,5 do
        self.m_Tips[i]:SetActive(activeIdx==i)
    end
    if activeIdx==groupIdx then
        self.m_ConfirmButton:SetActive(false)
        self.m_ShengXiaoMark:SetActive(true)
    else
        self.m_ConfirmButton:SetActive(true)
        self.m_ShengXiaoMark:SetActive(false)
    end

    local v = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.AttrGroups,groupIdx)
    if v then
        -- self.m_GroupNameLabel.text= v.Name
        self:SetGroupName(groupIdx,v.Name)
        self:InitSkills(v.SkillTemplateIdx)
        self:InitFaBao(v.TalismanSetIdx)
        
        self:InitLingShou(v.BattleLingShouId,v.AssistLingShouId,v.BindedPartnerLingShouId)
        self:InitEquipSetIdxes(v.BodyEquipWordSetIdxes,v.BodyEquipHoleSetIdxes)
    else

        --使用默认的数据
        self:InitSkills(1)
        self:InitFaBao(1)
        
        local lastLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.LastLingShouId or nil
        local assistLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.AssistLingShouId or nil
        local bindPartenerLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId or nil

        self:InitLingShou(lastLingShouId,assistLingShouId,bindPartenerLingShouId)
        self:InitEquipSetIdxes(nil,nil)
    end

end


function CLuaPropertySwitchWnd:IsAttrGroupUnlocked(groupIdx)
    if not CClientMainPlayer.Inst then return false end

    local v = CClientMainPlayer.Inst.PlayProp.AttrGroupUnlock
	v = v or 0
	local r = bit.band(v, bit.lshift(1, groupIdx - 1))
	return r > 0
end


function CLuaPropertySwitchWnd:OnGetAllLingShouOverview()
    local activeIdx = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.ActiveAttrGroupIdx or 1
    if activeIdx==0 then
        activeIdx=1
    end
    -- for i=1,5 do
    --     CUICommonDef.SetActive(self.m_SwitchButtons[i].gameObject,self:IsAttrGroupUnlocked(i),true)
    -- end
    -- self.m_TabView:ChangeTo(activeIdx-1)
    self:ChangeIndex(activeIdx)
end

function CLuaPropertySwitchWnd:InitLingShou(BattleLingShouId,AssistLingShouId,BindedPartnerLingShouId)
    local lingshouTf = self.transform:Find("LingShou")
    local item1 = lingshouTf:Find("Item1")
    local item2 = lingshouTf:Find("Item2")
    local item3 = lingshouTf:Find("Item3")

    local battleData = nil
    local assistData = nil
    local bindPartenerData = nil

    local list = CLingShouMgr.Inst:GetLingShouOverviewList()
    for i=1,list.Count do
        if BattleLingShouId == list[i-1].id then
            battleData = list[i-1]
        elseif AssistLingShouId == list[i-1].id then
            assistData = list[i-1]
        elseif BindedPartnerLingShouId == list[i-1].id then
            bindPartenerData = list[i-1]
        end
    end

    local needCheck = false
    local activeIdx = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.ActiveAttrGroupIdx or 1
    if activeIdx==0 then
        activeIdx=1
    end
    if activeIdx==CLuaPropertySwitchWnd.m_GroupIndex then
        needCheck = true
    end

    local lastLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.LastLingShouId or nil
	local fuTiLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.FuTiLingShouId or nil
    local assistLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.AssistLingShouId or nil
    local bindPartenerLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId or nil

    local function initItem(tf,templateId,noMark)
        local icon = tf:GetComponent(typeof(CUITexture))
        local add = tf:Find("AddSprite").gameObject
        local mark = tf:Find("DisableSprite").gameObject
        mark:SetActive(false)
        if not noMark and needCheck then
            mark:SetActive(true)
        end
        if templateId>0 then
            icon:LoadNPCPortrait(CLingShouBaseMgr.GetLingShouIcon(templateId), false)
            add:SetActive(false)
        else
            icon:Clear()
            add:SetActive(true)
        end
    end

	local futiLingShouMode = CScene.MainScene and CScene.MainScene.LingShouMode == 1
	if futiLingShouMode then
		initItem(item1,battleData and battleData.templateId or 0, BattleLingShouId==fuTiLingShouId)
	else
		initItem(item1,battleData and battleData.templateId or 0, BattleLingShouId==lastLingShouId)
	end
    initItem(item2,assistData and assistData.templateId or 0, AssistLingShouId==assistLingShouId)
    initItem(item3,bindPartenerData and bindPartenerData.templateId or 0, BindedPartnerLingShouId==bindPartenerLingShouId)

end

function CLuaPropertySwitchWnd:InitEquipSetIdxes(BodyEquipWordSetIdxes,BodyEquipHoleSetIdxes)
    local itemProp = CClientMainPlayer.Inst.ItemProp

    local activeIdx = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.ActiveAttrGroupIdx or 1
    if activeIdx==0 then
        activeIdx=1
    end
    local needCheck = activeIdx==CLuaPropertySwitchWnd.m_GroupIndex
    if BodyEquipHoleSetIdxes and BodyEquipHoleSetIdxes then
        for i,v in ipairs(self.m_EquipPositions) do
            local go = self.m_Equips[i]
            local wordSetIdx = BodyEquipWordSetIdxes[v]
            local holeSetIdx = BodyEquipHoleSetIdxes[v]
            local itemId = itemProp:GetItemAt(EnumItemPlace.Body, v)
            local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
            
            if itemId and itemId~="" then
                --判断一下当前装备是否有第二套
                local item = CItemMgr.Inst:GetById(itemId)

                local wordSetStr = wordSetIdx==2 and LocalString.GetString("二") or LocalString.GetString("一")
                if needCheck then
                    local activeWordSetIdx = item.Equip.ActiveWordSetIdx==2 and 2 or 1
                    if wordSetIdx~=activeWordSetIdx then
                        wordSetStr = "[c][ff0000]"..wordSetStr.."[-][/c]"
                    end
                end
                local holeSetStr = holeSetIdx==2 and LocalString.GetString("二") or LocalString.GetString("一")
                if needCheck then
                    local activeHoleSetIdx = item.Equip.ActiveHoleSetIdx==2 and 2 or 1
                    if holeSetIdx~=activeHoleSetIdx then
                        holeSetStr = "[c][ff0000]"..holeSetStr.."[-][/c]"
                    end
                end
                label.gameObject:SetActive(true)
                label.text = wordSetStr.."·"..holeSetStr
            else
                label.gameObject:SetActive(false)
            end
        end
    else
        for i,v in ipairs(self.m_EquipPositions) do
            local go = self.m_Equips[i]
            local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
            local itemId = itemProp:GetItemAt(EnumItemPlace.Body, v)
            
            if itemId and itemId~="" then
                local item = CItemMgr.Inst:GetById(itemId)
                local wordSetIdx = item.Equip.ActiveWordSetIdx
                local holeSetIdx = item.Equip.ActiveHoleSetIdx
                local wordSetStr = wordSetIdx==2 and LocalString.GetString("二") or LocalString.GetString("一")
                local holeSetStr = holeSetIdx==2 and LocalString.GetString("二") or LocalString.GetString("一")
                label.gameObject:SetActive(true)
                label.text = wordSetStr.."·"..holeSetStr
            else
                label.gameObject:SetActive(false)
            end
        end
    end
end
function CLuaPropertySwitchWnd:OnClickEquip(itemId,pos)
    local group = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.AttrGroups,CLuaPropertySwitchWnd.m_GroupIndex)
    local item = CItemMgr.Inst:GetById(itemId)
    local wordSetIdx = item.Equip.ActiveWordSetIdx
    local holeSetIdx = item.Equip.ActiveHoleSetIdx

    if group then
        wordSetIdx = group.BodyEquipWordSetIdxes[pos]
        holeSetIdx = group.BodyEquipHoleSetIdxes[pos]
    end
    wordSetIdx = wordSetIdx==2 and 2 or 1
    holeSetIdx = holeSetIdx==2 and 2 or 1
    -- print(wordSetIdx,holeSetIdx)
    Gac2Gas.RequestEquipInfoInAttrGroup(itemId,wordSetIdx,holeSetIdx)
end
