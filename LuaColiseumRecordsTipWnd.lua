local CommonDefs = import "L10.Game.CommonDefs"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"

LuaColiseumRecordsTipWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaColiseumRecordsTipWnd, "GridView", "GridView", QnAdvanceGridView)

--@endregion RegistChildComponent end
RegistClassMember(LuaColiseumRecordsTipWnd,"m_RecordsTipWndData")

function LuaColiseumRecordsTipWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaColiseumRecordsTipWnd:Init()
    self.GridView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.m_RecordsTipWndData
        end,
        function(item,index) self:InitItem(item,index) end
    )
    Gac2Gas.RequestArenaClassRecord()
end

function LuaColiseumRecordsTipWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSendArenaClassRecord", self, "OnSendArenaClassRecord")
end

function LuaColiseumRecordsTipWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSendArenaClassRecord", self, "OnSendArenaClassRecord")
end

function LuaColiseumRecordsTipWnd:OnSendArenaClassRecord()
    self:InitRecordsTipWndData()
    self.GridView:ReloadData(true, true)
end
--@region UIEvent

--@endregion UIEvent

function LuaColiseumRecordsTipWnd:InitItem(item,index)
    local tf = item.transform
    local data = self.m_RecordsTipWndData[index + 1]

    local class = data.class 
    local winTimes = data.winTimes
    local loseTimes = data.loseTimes
    local battleNum = winTimes + loseTimes
    local winRate = (battleNum ~= 0) and (winTimes / battleNum) or -1

    tf:Find("ClassSprite"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), class))
    tf:Find("WinNumLabel"):GetComponent(typeof(UILabel)).text = (battleNum ~= 0) and winTimes or "-"
    tf:Find("LoseNumLabel"):GetComponent(typeof(UILabel)).text = (battleNum ~= 0) and loseTimes or "-"
    local winRateNumLabel = tf:Find("WinRateNumLabel"):GetComponent(typeof(UILabel))
    winRateNumLabel.text = (battleNum ~= 0) and math.floor(winRate * 100) .."%" or "-"
    winRateNumLabel.color = (battleNum ~= 0) and NGUIText.ParseColor24(winRate >= 0.5 and "00ff60" or "ff5050", 0) or Color.white
end

function LuaColiseumRecordsTipWnd:InitRecordsTipWndData()
    self.m_RecordsTipWndData = {}
    local allClassDict = {}
    Initialization_Init.Foreach(function(k,v)
        if v.Status == 0 then
            allClassDict[v.Class] =  true
        end
    end)
    for class,_ in pairs(allClassDict) do
        local t = LuaColiseumMgr.m_RecordsTipWndData[class]
        table.insert(self.m_RecordsTipWndData, {class = class, winTimes = t.winTimes and t.winTimes or 0, loseTimes = t.lostTimes and t.lostTimes or 0})
    end
    
    for i = 1,#self.m_RecordsTipWndData do
        local class = self.m_RecordsTipWndData[i].class
        local data = LuaColiseumMgr.m_RecordsTipWndData[class]
        local winTimes = data.winTimes
        local loseTimes = data.lostTimes
        local battleNum = winTimes + loseTimes
        self.m_RecordsTipWndData[i].winTimes = winTimes
        self.m_RecordsTipWndData[i].loseTimes = loseTimes
        self.m_RecordsTipWndData[i].winRate = (battleNum ~= 0) and (winTimes / battleNum) or -1
    end

    table.sort(self.m_RecordsTipWndData,function (a, b)
        if a.winRate == b.winRate then
            return a.class < b.class 
        end
        return a.winRate > b.winRate
    end)
end
