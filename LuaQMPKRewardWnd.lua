local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local CWelfareBonusAwardTemplate = import "L10.UI.CWelfareBonusAwardTemplate"
local CItemMgr = import "L10.Game.CItemMgr"

LuaQMPKRewardWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQMPKRewardWnd, "Scrollview", "Scrollview", CUIRestrictScrollView)
RegistChildComponent(LuaQMPKRewardWnd, "Table", "Table", UITable)
RegistChildComponent(LuaQMPKRewardWnd, "Template", "Template", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaQMPKRewardWnd, "m_RewardInfos")

function LuaQMPKRewardWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.Template:SetActive(false)
end

function LuaQMPKRewardWnd:Init()
    self.m_RewardInfos = {}
    QuanMinPK_Reward.ForeachKey(function (key)
        local data = QuanMinPK_Reward.GetData(key)
        local array = data.RewardItems
        local tbl = {}
        if array then
            for i = 0, array.Length - 1 do
                table.insert(tbl, array[i])
            end
        end
        local descArray = data.RewardDescription
        local descTab = {}
        if descArray then
            for i = 0, descArray.Length - 1 do
                table.insert(descTab, descArray[i])
            end
        end
        table.insert(self.m_RewardInfos, {RankName = data.RankName, RankFontSize = data.RankFontSize, RewardItems = tbl, RewardDescs = descTab})
    end)

    for i = 1, #self.m_RewardInfos do
        local go = NGUITools.AddChild(self.Table.gameObject, self.Template)
        self:InitItem(go, self.m_RewardInfos[i])
        go:SetActive(true)
    end

    self.Table:Reposition()
end

--@region UIEvent

--@endregion UIEvent

function LuaQMPKRewardWnd:InitItem(item, info)
    local titleLab  = item.transform:Find("TitleLab"):GetComponent(typeof(UILabel))
    local table     = item.transform:Find("Table"):GetComponent(typeof(UIGrid))
    local itemCell  = item.transform:Find("ItemCell").gameObject

    titleLab.text = info.RankName
    titleLab.fontSize = info.RankFontSize
    itemCell:SetActive(false)
    for i = 1, #info.RewardItems do
        local go = NGUITools.AddChild(table.gameObject, itemCell)
        local welfareBonusAward = go:GetComponent(typeof(CWelfareBonusAwardTemplate))
        local itemData = CItemMgr.Inst:GetItemTemplate(info.RewardItems[i])
        welfareBonusAward:Init(itemData, info.RewardDescs[i] or "1", false, self.Scrollview)
        go:SetActive(true)
    end

    table:Reposition()
end