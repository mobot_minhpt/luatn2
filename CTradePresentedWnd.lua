-- Auto Generated!!
local CButton = import "L10.UI.CButton"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CIMMgr = import "L10.Game.CIMMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CTradeMgr = import "L10.Game.CTradeMgr"
local CTradePresentedWnd = import "L10.UI.CTradePresentedWnd"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local NGUIText = import "NGUIText"
local Profession = import "L10.Game.Profession"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTradePresentedWnd.m_Init_CS2LuaHook = function (this) 
    local obj = TypeAs(CClientObjectMgr.Inst:GetObject(CTradeMgr.Inst.targetEngineId), typeof(CClientOtherPlayer))
    if CTradeMgr.Inst.targetEngineId == 0 then
        g_MessageMgr:ShowMessage("OTHER_PLAYER_LEAVE")
        Gac2Gas.CancelTrade()
        return
    end
    local isFriend
    if obj ~= nil then
        local playerId = obj.PlayerId
        isFriend = CIMMgr.Inst:IsMyFriend(playerId)
    else
        g_MessageMgr:ShowMessage("OTHER_PLAYER_LEAVE")
        Gac2Gas.CancelTrade()
        return
    end
    if not CTradeMgr.Inst.IsPlayerPresenter then
        this.sellPlayerInfo:Init(LocalString.GetString("我"), "", "", "", Color.white)
        local default
        if isFriend then
            default = LocalString.GetString("好友")
        else
            default = LocalString.GetString("陌生人")
        end
        this.buyPlayerInfo:Init(obj.Name, tostring(obj.Level), Profession.GetFullName(obj.Class), default, isFriend and NGUIText.ParseColor24("73ff2e", 0) or NGUIText.ParseColor24("f35151", 0))
    else
        this.buyPlayerInfo:Init(LocalString.GetString("我"), "", "", "", Color.white)
        local extern
        if isFriend then
            extern = LocalString.GetString("好友")
        else
            extern = LocalString.GetString("陌生人")
        end
        this.sellPlayerInfo:Init(obj.Name, tostring(obj.Level), Profession.GetFullName(obj.Class), extern, isFriend and NGUIText.ParseColor24("73ff2e", 0) or NGUIText.ParseColor24("f35151", 0))
    end
    this.detailView:Init()
end
CTradePresentedWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.tradeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.tradeButton).onClick, MakeDelegateFromCSFunction(this.OnLockBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.cancelButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.cancelButton).onClick, MakeDelegateFromCSFunction(this.OnCancelBtnClick, VoidDelegate, this), true)
    EventManager.AddListener(EnumEventType.TargetLockTrade, MakeDelegateFromCSFunction(this.OnTargetLockTrade, Action0, this))
    EventManager.AddListener(EnumEventType.PlayerLockTrade, MakeDelegateFromCSFunction(this.OnPlayerLockTrade, Action0, this))
end
CTradePresentedWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.tradeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.tradeButton).onClick, MakeDelegateFromCSFunction(this.OnLockBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.cancelButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.cancelButton).onClick, MakeDelegateFromCSFunction(this.OnCancelBtnClick, VoidDelegate, this), false)
    EventManager.RemoveListener(EnumEventType.TargetLockTrade, MakeDelegateFromCSFunction(this.OnTargetLockTrade, Action0, this))
    EventManager.RemoveListener(EnumEventType.PlayerLockTrade, MakeDelegateFromCSFunction(this.OnPlayerLockTrade, Action0, this))
end
CTradePresentedWnd.m_OnLockBtnClick_CS2LuaHook = function (this, go) 

    if this.lockLabel.text == LocalString.GetString("锁定") then
        if not CTradeMgr.Inst.IsPlayerPresenter and CTradeMgr.Inst.targetTradeItem == nil and CTradeMgr.Inst.targetTradeEquip == nil then
            g_MessageMgr:ShowMessage("ZengSong_Fail_Item_Cannot_Be_Null")
        elseif CTradeMgr.Inst.IsPlayerPresenter and CTradeMgr.Inst.playerTradeItem == nil and CTradeMgr.Inst.playerTradeEquip == nil then
            g_MessageMgr:ShowMessage("ZengSong_Fail_Item_Cannot_Be_Null")
        else
            Gac2Gas.LockTradeShelf()
        end
    else
        if CommonDefs.GetComponent_GameObject_Type(this.tradeButton, typeof(CButton)).Enabled then
            Gac2Gas.ConfirmTrade(true)
            CommonDefs.GetComponent_GameObject_Type(this.tradeButton, typeof(CButton)).Enabled = false
        end
    end
end
CTradePresentedWnd.m_OnPlayerLockTrade_CS2LuaHook = function (this) 

    if CTradeMgr.Inst.IsPlayerPresenter then
        this.lockLabel.text = LocalString.GetString("赠送")
    else
        this.lockLabel.text = LocalString.GetString("接受")
    end
    CommonDefs.GetComponent_GameObject_Type(this.tradeButton, typeof(CButton)).Enabled = CTradeMgr.Inst.targetLock
    if CTradeMgr.Inst.IsPlayerPresenter then
        this.detailView:OnLockItem()
    end
end
CTradePresentedWnd.m_OnTargetLockTrade_CS2LuaHook = function (this) 

    if CTradeMgr.Inst.playerLock then
        CommonDefs.GetComponent_GameObject_Type(this.tradeButton, typeof(CButton)).Enabled = true
    end
    if not CTradeMgr.Inst.IsPlayerPresenter then
        this.detailView:OnLockItem()
    end
end
