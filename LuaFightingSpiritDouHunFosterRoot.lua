local DelegateFactory  = import "DelegateFactory"
local QnRadioBox = import "L10.UI.QnRadioBox"
local GameObject = import "UnityEngine.GameObject"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local UILabel = import "UILabel"
local UIProgressBar = import "UIProgressBar"
local CPackageItemCell = import "L10.UI.CPackageItemCell"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local CFightingSpiritMgr=import "L10.Game.CFightingSpiritMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CFightingSpiritCommitMaterialWnd = import "L10.UI.CFightingSpiritCommitMaterialWnd"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local String = import "System.String"
local Gac2Gas2 = import "L10.Game.Gac2Gas"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaFightingSpiritDouHunFosterRoot = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFightingSpiritDouHunFosterRoot, "RadioBox", "RadioBox", QnRadioBox)
RegistChildComponent(LuaFightingSpiritDouHunFosterRoot, "ComitBtn", "ComitBtn", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunFosterRoot, "RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunFosterRoot, "AdvView", "AdvView", QnAdvanceGridView)
RegistChildComponent(LuaFightingSpiritDouHunFosterRoot, "AddSubBtn", "AddSubBtn", QnAddSubAndInputButton)
RegistChildComponent(LuaFightingSpiritDouHunFosterRoot, "ValueGetLabel", "ValueGetLabel", UILabel)
RegistChildComponent(LuaFightingSpiritDouHunFosterRoot, "PointGetLabel", "PointGetLabel", UILabel)
RegistChildComponent(LuaFightingSpiritDouHunFosterRoot, "ItemCell", "ItemCell", CPackageItemCell)
RegistChildComponent(LuaFightingSpiritDouHunFosterRoot, "EmptyLabel", "EmptyLabel", UILabel)
RegistChildComponent(LuaFightingSpiritDouHunFosterRoot, "HintLabel", "HintLabel", UILabel)
RegistChildComponent(LuaFightingSpiritDouHunFosterRoot, "ProgressBar", "ProgressBar", UIProgressBar)
RegistChildComponent(LuaFightingSpiritDouHunFosterRoot, "ProgressLabel", "ProgressLabel", UILabel)
RegistChildComponent(LuaFightingSpiritDouHunFosterRoot, "TodayGetLabel", "TodayGetLabel", UILabel)
RegistChildComponent(LuaFightingSpiritDouHunFosterRoot, "TypeLabel", "TypeLabel", UILabel)

--@endregion RegistChildComponent end
-- 当前选中的tab 0，1，2
RegistClassMember(LuaFightingSpiritDouHunFosterRoot, "m_ShowIndex")
RegistClassMember(LuaFightingSpiritDouHunFosterRoot, "m_ItemList")
-- 目前获得与当日最高获得 需要一个RPC来获取该参数
RegistClassMember(LuaFightingSpiritDouHunFosterRoot, "m_HasInited")
RegistClassMember(LuaFightingSpiritDouHunFosterRoot, "m_SelectedItemID")
RegistClassMember(LuaFightingSpiritDouHunFosterRoot, "m_ShowDataAction")

function LuaFightingSpiritDouHunFosterRoot:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ComitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnComitBtnClick()
	end)


	
	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)


    --@endregion EventBind end
end

function LuaFightingSpiritDouHunFosterRoot:InitWnd()
	self.m_ShowIndex = 0
	if self.m_NeedToChangeIndex and self.m_NeedToChangeIndex > 0 then
		self.m_ShowIndex = self.m_NeedToChangeIndex
		self.m_NeedToChangeIndex = 0
	end

	self.ItemCell:Init("", false, false, false, nil)

	self.RadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
		self.m_ShowIndex = index
		self.m_NeedToChangeIndex = 0
		self:ShowPackage()
		self:ShowProgress()
	end)

	self.RadioBox:ChangeTo(self.m_ShowIndex, true)

	self.AddSubBtn.onValueChanged = DelegateFactory.Action_uint(function(index)
		self:ShowProgress()
	end)

	self.m_ItemList = {}

	-- 设置Grid
	self.AdvView.m_DataSource = DefaultTableViewDataSource.Create(function() 
        return #self.m_ItemList
    end, function(item, index)
        self:InitItem(item, index)
    end)

	-- 设置选中状态
	self.AdvView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        for i=0, self.AdvView.Count -1 do
			local item = self.AdvView:GetItemAtRow(i)
			local cell = item:GetComponent(typeof(CPackageItemCell))
			if cell then
				local sp = cell.transform:Find("SelectedSprite"):GetComponent(typeof(UISprite))
				sp.gameObject:SetActive(i == row)
				sp.enabled = (i == row)
				--cell.Selected = i == row
			end
		end
    end)
end

-- 初始化Item函数
function LuaFightingSpiritDouHunFosterRoot:InitItem(item, index)
	--print("inititem", item, index)
	local cell = item:GetComponent(typeof(CPackageItemCell))
	cell.OnItemClickDelegate  = DelegateFactory.Action_CPackageItemCell(
		function(cell)
			self:OnItemClick(cell)
		end
	)

	cell.OnItemLongPressDelegate  = DelegateFactory.Action_CPackageItemCell(
		function(cell)
			self:OnLongPress(cell)
		end
	)

	cell:Init(self.m_ItemList[index+1].ItemID, false, false, false);
    cell.Selected = cell.ItemId == self.m_SelectedItemID;
end

-- 切换显示
function LuaFightingSpiritDouHunFosterRoot:ChangeTab(index)
	--print("LuaFightingSpiritDouHunFosterRoot:ChangeTab(index)", index)
	self.m_NeedToChangeIndex = index-1
	self.RadioBox:ChangeTo(index-1, true)
end

function LuaFightingSpiritDouHunFosterRoot:OnItemClick(cell)
	if not self.m_HasInited then
		g_MessageMgr:ShowMessage("DOU_HUN_TRAIN_NOT_OPEN")
		return
	end

	if self.m_SelectedItemID == cell.ItemId then
		return
	end

	self.m_SelectedItemID = cell.ItemId
	self.ItemCell:Init(self.m_SelectedItemID, false, false, false)

	local item = CItemMgr.Inst:GetById(self.m_SelectedItemID)
	local data = DouHunTan_TrainSubmit.GetData(item.TemplateId)
	local setting = DouHunTan_Setting.GetData()
	local trainValue = CFightingSpiritMgr.Instance:GetDouhunTrainValue(self.m_ShowIndex+1)
	local maxValueList = {setting.MaxJingLi, setting.MaxQiLi, setting.MaxShenLi}

	local progressNotFull = trainValue < maxValueList[self.m_ShowIndex + 1]
	local socreLeft = CFightingSpiritCommitMaterialWnd.dailyMaxScore - CFightingSpiritCommitMaterialWnd.currentScore
	if progressNotFull then
		socreLeft = socreLeft /2
	end

	local itemCount = math.min(math.ceil(socreLeft/ data.TrainScore), item.Amount)
	self.AddSubBtn:SetMinMax(1, itemCount, 1)
	self.AddSubBtn:SetValue(1)
	
	self:ShowProgress()
	CUICommonDef.SetActive(self.ComitBtn, not System.String.IsNullOrEmpty(self.m_SelectedItemID), true)
end

function LuaFightingSpiritDouHunFosterRoot:OnLongPress(cell)
	local item = CItemMgr.Inst:GetById(cell.ItemId)
	if item then
		CItemInfoMgr.ShowLinkItemInfo(item, true, nil, AlignType.Default, 0, 0, 0, 0, 0)
	end
end

function LuaFightingSpiritDouHunFosterRoot:ShowPackage()
	local maxSize = CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag)
	self.m_ItemList = {}

	for i = 1, maxSize do
		local itemId = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
		if itemId then
			local item = CItemMgr.Inst:GetById(itemId)
			if CFightingSpiritMgr.Instance:IsMaterial(item.TemplateId, self.m_ShowIndex + 1) then
				if not item.IsItem or not item.Item:IsExpire() then
					local info = {}
					info.ItemID = itemId
					info.Pos = i
					info.Count = item.Amount
					table.insert(self.m_ItemList, info)
				end
			end
		end
	end

	self.EmptyLabel.gameObject:SetActive(#self.m_ItemList == 0)

	if self.m_ShowIndex == 0 then
		self.EmptyLabel.text = g_MessageMgr:FormatMessage("SUBMIT_JINGLI_INTERFACE_MSG")
	elseif self.m_ShowIndex == 1 then
		self.EmptyLabel.text = g_MessageMgr:FormatMessage("SUBMIT_QILI_INTERFACE_MSG")
	else
		self.EmptyLabel.text = g_MessageMgr:FormatMessage("SUBMIT_SHENLI_INTERFACE_MSG")
	end

	self.AddSubBtn:SetMinMax(0, 0, 1)
	self.AddSubBtn:SetValue(0)
	self.ItemCell:Init("", false, false, false, nil)
	self.m_SelectedItemID = nil

	--print("ReloadData", 0)
	--print(debug.traceback())
	self.AdvView:ReloadData(true, false)
end

function LuaFightingSpiritDouHunFosterRoot:ShowProgress()
	if not self.m_HasInited then
		return
	end

	self.TodayGetLabel.text = SafeStringFormat3(LocalString.GetString("今日还可获得%s斗魂积分"), CFightingSpiritCommitMaterialWnd.dailyMaxScore - CFightingSpiritCommitMaterialWnd.currentScore)

	local data = DouHunTan_Setting.GetData()
	local trainValue = CFightingSpiritMgr.Instance:GetDouhunTrainValue(self.m_ShowIndex + 1)

	local maxValueList = {data.MaxJingLi, data.MaxQiLi, data.MaxShenLi}
	--print("Max", data.MaxJingLi, data.MaxQiLi, data.MaxShenLi)
	local descList = {LocalString.GetString("精力"), LocalString.GetString("气力"), LocalString.GetString("神力")}

	local maxValue = maxValueList[self.m_ShowIndex+1]
	local desc = descList[self.m_ShowIndex+1]

	--print("progress",self.m_ShowIndex, trainValue, maxValue)
	-- 处理显示
	self.ProgressBar.value = math.min(trainValue, maxValue)/ maxValue
	self.ProgressLabel.text = SafeStringFormat3("%s/%s", tostring(math.min(trainValue, maxValue)), tostring(maxValue))

	local bgList = {"common_hpandmp_green", "common_hpandmp_max", "common_hpandmp_mp"}
	local progressSprite = self.ProgressBar.transform:Find("Sprite"):GetComponent(typeof(UISprite))
	progressSprite.spriteName = bgList[self.m_ShowIndex + 1]

	local barLabel = self.ProgressBar.transform:Find("Label"):GetComponent(typeof(UILabel))
	barLabel.text = SafeStringFormat3("%s/%s", tostring(math.min(trainValue, maxValue)), tostring(maxValue))
	local progressNotFull = trainValue < maxValue
	self.TypeLabel.text = desc

	local count = self.AddSubBtn:GetValue()
	local getValue = 0
	local getPoint = 0

	if not System.String.IsNullOrEmpty(self.ItemCell.ItemId) then
		local item = CItemMgr.Inst:GetById(self.ItemCell.ItemId)
		if item then
			local itemData = DouHunTan_TrainSubmit.GetData(item.TemplateId)
			getValue = itemData.TrainValue
			getPoint = itemData.TrainScore
		end
	end

	local pointCanEarn = getPoint * count
	if progressNotFull then
		pointCanEarn = pointCanEarn * 2
	end

	-- 显示结果
	self.ValueGetLabel.text = SafeStringFormat3(LocalString.GetString("%s斗魂积分"), pointCanEarn)
	self.PointGetLabel.text = SafeStringFormat3(LocalString.GetString("[FFFF00]%s[-]斗魂%s"), getValue*count, desc)
end

function LuaFightingSpiritDouHunFosterRoot:OnEnable()
	self:InitWnd()
	-- 请求每日数据
	Gac2Gas.OpenDouHunSubmitItemWnd()
    if self.m_ShowDataAction == nil then
        self.m_ShowDataAction = DelegateFactory.Action(function()
			--print("self:ShowPackage()", 2)
			self:ShowPackage()
			self:ShowProgress()
        end)
    end
	if self.m_SetDataAction == nil then
        self.m_SetDataAction = DelegateFactory.Action_uint_uint(function(current, max)
			self.m_HasInited = true
			CFightingSpiritCommitMaterialWnd.currentScore = current
			CFightingSpiritCommitMaterialWnd.dailyMaxScore = max
			self:ShowProgress()
        end)
    end

    EventManager.AddListenerInternal(EnumEventType.FightingSpiritDouhunTrainRes, self.m_ShowDataAction)
    EventManager.AddListenerInternal(EnumEventType.FightingSpiritSubmitInfoReceive, self.m_SetDataAction)
end

function LuaFightingSpiritDouHunFosterRoot:OnDisable()
    EventManager.RemoveListenerInternal(EnumEventType.FightingSpiritDouhunTrainRes, self.m_ShowDataAction)
	EventManager.RemoveListenerInternal(EnumEventType.FightingSpiritSubmitInfoReceive, self.m_SetDataAction)
end

--@region UIEvent

function LuaFightingSpiritDouHunFosterRoot:OnComitBtnClick()
	if not self.m_HasInited then
		g_MessageMgr:ShowMessage("DOU_HUN_TRAIN_NOT_OPEN")
		return
	end

	local left = CFightingSpiritCommitMaterialWnd.dailyMaxScore - CFightingSpiritCommitMaterialWnd.currentScore
	--print(left)
	if left <=0 then
		g_MessageMgr:ShowMessage("DOU_HUN_INTEGRATION_UPPER_LIMIT")
		return
	end

	--print(self.m_SelectedItemID)
	if not String.IsNullOrEmpty(self.m_SelectedItemID) then
		local count = self.AddSubBtn:GetValue()
		--print(count)
		if count > 0 then
			local pos = 0
			local n = CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag)
			for i = 1, n do
				local itemId = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
				--print(itemId, self.m_SelectedItemID)
                if itemId ~= nil and itemId == self.m_SelectedItemID then
					pos = i
				end
			end

			if pos ~= 0 then
				local trainP = 0
				if not String.IsNullOrEmpty(self.m_SelectedItemID) then
					local item = CItemMgr.Inst:GetById(self.ItemCell.ItemId)
					if item then
						trainP = DouHunTan_TrainSubmit.GetData(item.TemplateId).TrainScore
					end
				end

				local value = CFightingSpiritMgr.Instance:GetDouhunTrainValue(self.m_ShowIndex + 1)
				local setting = DouHunTan_Setting.GetData()
				local maxValueList = {setting.MaxJingLi, setting.MaxQiLi, setting.MaxShenLi}

				--print(setting.MaxJingLi, setting.MaxQiLi, setting.MaxShenLi)
				local progressNotFull = value < maxValueList[self.m_ShowIndex+1]

				local scoreCanEarn = trainP * count
				if progressNotFull then
					scoreCanEarn = scoreCanEarn * 2
				end

				if scoreCanEarn < CFightingSpiritCommitMaterialWnd.dailyMaxScore - CFightingSpiritCommitMaterialWnd.currentScore then
					Gac2Gas2.SubmitDouHunTrainItem(self.m_SelectedItemID, EnumToInt(EnumItemPlace.Bag), pos, count)
				else
					g_MessageMgr:ShowOkCancelMessage(LocalString.GetString("你提交斗魂培养道具所获得积分超过日上限，是否还要继续提交？"), function()
						Gac2Gas2.SubmitDouHunTrainItem(self.m_SelectedItemID, EnumToInt(EnumItemPlace.Bag), pos, count)
					end, nil, nil, nil, false)
				end
			end
		end
	end
end

function LuaFightingSpiritDouHunFosterRoot:OnRuleBtnClick()
	g_MessageMgr:ShowMessage("DouHun_Foster_Rlue_Desc")
end

--@endregion UIEvent

