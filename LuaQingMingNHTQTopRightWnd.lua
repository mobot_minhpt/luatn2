local GameObject = import "UnityEngine.GameObject"
local CTopAndRightTipWnd=import "L10.UI.CTopAndRightTipWnd"

LuaQingMingNHTQTopRightWnd = class()

RegistChildComponent(LuaQingMingNHTQTopRightWnd, "rightBtn", "rightBtn", GameObject)
RegistChildComponent(LuaQingMingNHTQTopRightWnd, "node1", "node1", GameObject)
RegistChildComponent(LuaQingMingNHTQTopRightWnd, "node2", "node2", GameObject)
RegistChildComponent(LuaQingMingNHTQTopRightWnd, "node3", "node3", GameObject)
RegistChildComponent(LuaQingMingNHTQTopRightWnd, "HPItem", "HPItem", GameObject)
RegistChildComponent(LuaQingMingNHTQTopRightWnd, "divide1", "divide1", GameObject)
RegistChildComponent(LuaQingMingNHTQTopRightWnd, "divide2", "divide2", GameObject)
RegistChildComponent(LuaQingMingNHTQTopRightWnd, "text", "text", UILabel)
RegistChildComponent(LuaQingMingNHTQTopRightWnd, "num", "num", UILabel)

RegistClassMember(LuaQingMingNHTQTopRightWnd, "percentTable")
RegistClassMember(LuaQingMingNHTQTopRightWnd, "percentMaxLength")
RegistClassMember(LuaQingMingNHTQTopRightWnd, "m_Tick")
RegistClassMember(LuaQingMingNHTQTopRightWnd, "restSec")
RegistClassMember(LuaQingMingNHTQTopRightWnd, "currentStage")

function LuaQingMingNHTQTopRightWnd:Awake()
end

function LuaQingMingNHTQTopRightWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateQingMingCGYJTopInfo", self, "UpdateTop")
end

function LuaQingMingNHTQTopRightWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateQingMingCGYJTopInfo", self, "UpdateTop")
end

function LuaQingMingNHTQTopRightWnd:OneTick()
  self.restSec = self.restSec - 1
  if self.restSec < 0 then
    self.restSec = 0
  end

  if self.restSec == 0 then
      self.num.text = '0:0'
  else
    local min = math.floor(self.restSec/60)
    local sec = self.restSec%60
    if sec < 10 then
      self.num.text = min..':0'..sec
    else
      self.num.text = min..':'..sec
    end
  end

  if self.currentStage == 2 then
    local percent = self.restSec / self.percentTable[1]
    local percentSprite = self.HPItem:GetComponent(typeof(UISprite))
    percentSprite.width = percent * self.percentMaxLength
  end
end

function LuaQingMingNHTQTopRightWnd:UpdateTick(rest)
  self.restSec = rest
  self:OneTick()

	if self.m_Tick then
			UnRegisterTick(self.m_Tick)
	end

	self.m_Tick = RegisterTickWithDuration(function ()
    self:OneTick()
	end,1000,rest*1000)
end

function LuaQingMingNHTQTopRightWnd:UpdateTop()
  local info = LuaQingMing2021Mgr.GameStatus

  if not self.percentTable then
    self:InitData()
  end

  --LuaQingMing2021Mgr.GameStatus = {playStatus, leftTime, awardLv}
  if info then
    local playStatus = info[1]
    self.currentStage = playStatus
    local textString = {LocalString.GetString('准备倒计时'),LocalString.GetString('挑战倒计时'),LocalString.GetString('结束倒计时')}
    self.text.text = textString[playStatus]


    self:UpdateTick(tonumber(info[2]))
    --
    if playStatus == 1 then

    elseif playStatus >= 2 then
      local percent = info[2] / self.percentTable[1]
      local awardLv = info[3]
      local result = info[5]

      if playStatus == 3 then
        percent = info[4] / self.percentTable[1]
				LuaQingMing2021Mgr.MonsterStatus = nil
      end

      local percentSprite = self.HPItem:GetComponent(typeof(UISprite))
      percentSprite.width = percent * self.percentMaxLength
			percentSprite.spriteName = 'common_hpandmp_green'
      if percent < self.percentTable[5] then
        --percentSprite.color = Color.green
				percentSprite.spriteName = 'common_hpandmp_max'
      elseif percent < self.percentTable[6] then
        --percentSprite.color = Color.red
				percentSprite.spriteName = 'common_hpandmp_hp'
      end

      if awardLv == 1 then
        self.node1.transform.localPosition = Vector3(self.node1.transform.localPosition.x,self.node1.transform.localPosition.y,1)
        self.node2.transform.localPosition = Vector3(self.node2.transform.localPosition.x,self.node2.transform.localPosition.y,1)
        self.node3.transform.localPosition = Vector3(self.node3.transform.localPosition.x,self.node3.transform.localPosition.y,1)
      elseif awardLv == 2 then
        self.node1.transform.localPosition = Vector3(self.node1.transform.localPosition.x,self.node1.transform.localPosition.y,1)
        self.node2.transform.localPosition = Vector3(self.node2.transform.localPosition.x,self.node2.transform.localPosition.y,1)
        self.node3.transform.localPosition = Vector3(self.node3.transform.localPosition.x,self.node3.transform.localPosition.y,-1)
      elseif awardLv == 3 then
        self.node1.transform.localPosition = Vector3(self.node1.transform.localPosition.x,self.node1.transform.localPosition.y,1)
        self.node2.transform.localPosition = Vector3(self.node2.transform.localPosition.x,self.node2.transform.localPosition.y,-1)
        self.node3.transform.localPosition = Vector3(self.node3.transform.localPosition.x,self.node3.transform.localPosition.y,-1)
      elseif result == 1 then
        self.node1.transform.localPosition = Vector3(self.node1.transform.localPosition.x,self.node1.transform.localPosition.y,-1)
        self.node2.transform.localPosition = Vector3(self.node2.transform.localPosition.x,self.node2.transform.localPosition.y,-1)
        self.node3.transform.localPosition = Vector3(self.node3.transform.localPosition.x,self.node3.transform.localPosition.y,-1)
      end
    end
  end
end

function LuaQingMingNHTQTopRightWnd:InitData()
  if self.percentTable then
    return
  end

  self.percentTable = {}
  local max = QingMing2021_Setting.GetData().FightDuration
  self.percentTable[1] = max

  local info = QingMing2021_Setting.GetData().AwardInfo
  local infoTable = g_LuaUtil:StrSplit(info,";")
  for i,v in ipairs(infoTable) do
    local dataTable = g_LuaUtil:StrSplit(v,",")
    if dataTable and tonumber(dataTable[1]) then
      table.insert(self.percentTable,tonumber(dataTable[1]))
    end
  end

  self.percentMaxLength = 357
  local percent1 = self.percentTable[2] / self.percentTable[1]
  local percent2 = self.percentTable[3] / self.percentTable[1]
  table.insert(self.percentTable,percent1)
  table.insert(self.percentTable,percent2)

  self.divide1.transform.localPosition = Vector3((1- percent1)*self.percentMaxLength,0,0)
  self.divide2.transform.localPosition = Vector3((1- percent2)*self.percentMaxLength,0,0)
end

function LuaQingMingNHTQTopRightWnd:Init()
	UIEventListener.Get(self.rightBtn).onClick=LuaUtils.VoidDelegate(function(go)
		LuaUtils.SetLocalRotation(self.rightBtn.transform,0,0,0)
		CTopAndRightTipWnd.showPackage = false
		CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
	end)

  self:InitData()
  self:UpdateTop()
end

function LuaQingMingNHTQTopRightWnd:OnDestroy()
	if self.m_Tick then
    UnRegisterTick(self.m_Tick)
    self.m_Tick = nil
	end
  LuaQingMing2021Mgr.GameStatus = nil
end

--@region UIEvent

--@endregion
