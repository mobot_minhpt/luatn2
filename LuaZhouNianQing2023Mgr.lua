local CPayMgr = import "L10.Game.CPayMgr"
local CUIActionMgr = import "L10.UI.CUIActionMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local CScheduleMgr = import "L10.Game.CScheduleMgr"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CTempRelationInfo = import "L10.Game.CTempRelationInfo"

LuaZhouNianQing2023Mgr = {}

--#region 战令

function LuaZhouNianQing2023Mgr:QueryPass()
    --if self:IsPassOpen() then 
        Gac2Gas.QueryLianDanLuAlertInfo()
        Gac2Gas.QueryLianDanLuPlayData()
    --end
end

function LuaZhouNianQing2023Mgr:IsPassOpen()
    return CLuaScheduleMgr:IsFestivalTaskOpen(22121497, 31)
    --[[local info = LuaActivityRedDotMgr.m_ScheduleDict[42030312]
    if info then 
        local now = CServerTimeMgr.Inst:GetZone8Time()
        local nowTime = (now.Hour * 60 + now.Minute) * 60 + now.Second
        local startTime = info.StartTime
        local endTime = info.EndTime
        local task = info:GetTaskInfo()
        if task then
            if System.String.IsNullOrEmpty(task.StartTime) then
                return true
            elseif nowTime >= startTime and nowTime <= endTime then
                if CScheduleMgr.Patch_CheckTask then
                    if CTaskMgr.Inst:CheckTaskTime(info.taskId) then
                        return true
                    end
                else
                    return true
                end 
            end
        end
    end
    return false--]]
end

LuaZhouNianQing2023Mgr.m_bLoadingCnt = nil
function LuaZhouNianQing2023Mgr:OpenZhanLingWnd()
    self.m_bLoadingCnt = {0,0,0}
    Gac2Gas.RequestOpenLianDanLuTaskWnd()
    Gac2Gas.QueryLianDanLuAlertInfo()
    Gac2Gas.QueryLianDanLuPlayData()
end

function LuaZhouNianQing2023Mgr:TryShowZhanLingWnd(index)
    if self.m_bLoadingCnt[index] == 0 then 
        self.m_bLoadingCnt[index] = 1
    else
        g_ScriptEvent:BroadcastInLua("ZNQ2023_SyncZhanLingPlayData")
    end

    if self.m_bLoadingCnt[1] + self.m_bLoadingCnt[2] + self.m_bLoadingCnt[3] == 3 then
        self.m_bLoadingCnt = nil
        CUIManager.ShowUI(CLuaUIResources.ZhouNianQingPassportWnd)
    end
end

function LuaZhouNianQing2023Mgr:OpenUnLockZhanLingWnd()
    CUIManager.ShowUI(CLuaUIResources.ZhouNianQingPassportUnlockWnd)
end

LuaZhouNianQing2023Mgr.m_bAlert = false
function LuaZhouNianQing2023Mgr:SendZhanLingAlertInfo(p1, p2, p3, vip1, vip2, bAlert)
    self.m_ZhanLingP1 = p1
	self.m_ZhanLingP2 = p2
    self.m_ZhanLingP3 = p3
	self.m_IsVip1 = vip1 == 1
	self.m_IsVip2 = vip2 == 1
    self.m_bAlert = bAlert
    if self.m_bLoadingCnt then
        self:TryShowZhanLingWnd(2)
    else
        g_ScriptEvent:BroadcastInLua("ZNQ2023_SyncZhanLingPlayData")
    end
end

LuaZhouNianQing2023Mgr.m_ZhanLingData = {}
LuaZhouNianQing2023Mgr.m_ZhanLingP1 = 0
LuaZhouNianQing2023Mgr.m_ZhanLingP2 = 0
LuaZhouNianQing2023Mgr.m_ZhanLingP3 = 0
LuaZhouNianQing2023Mgr.m_IsVip1 = false
LuaZhouNianQing2023Mgr.m_IsVip2 = false
function LuaZhouNianQing2023Mgr:SyncZhanLingPlayData(p1, p2, p3, vip1, vip2, receiveDataUD)
    self.m_ZhanLingData = {}
    ZhanLing_Reward.Foreach(function(level, data)
        local t = {}
        t.level = level
        t.flag = 0
        t.item1 = data.ItemRewards1 and {data.ItemRewards1[0], data.ItemRewards1[1]}
        t.item2 = data.ItemRewards2 and {data.ItemRewards2[0], data.ItemRewards2[1]}
        t.item3 = data.ItemRewards3 and {data.ItemRewards3[0], data.ItemRewards3[1]}
        t.needProgress = data.NeedProgress
        table.insert(self.m_ZhanLingData, t)
    end)

	local list = MsgPackImpl.unpack(receiveDataUD)
	if list and list.Count > 0 then
		for i = 0, list.Count - 1, 2 do
			local level = tonumber(list[i])
            local flag = tonumber(list[i+1])
			local t = self.m_ZhanLingData[level]
			t.level = level
			t.flag = flag
		end		
	end
	self.m_ZhanLingP1 = p1
	self.m_ZhanLingP2 = p2
    self.m_ZhanLingP3 = p3
	self.m_IsVip1 = vip1 == 1
	self.m_IsVip2 = vip2 == 1
	
    if self.m_bLoadingCnt then
        self:TryShowZhanLingWnd(3)
    else
        g_ScriptEvent:BroadcastInLua("ZNQ2023_SyncZhanLingPlayData")
	end
end

LuaZhouNianQing2023Mgr.m_DailyTask = {}
LuaZhouNianQing2023Mgr.m_WeekTask = {}
LuaZhouNianQing2023Mgr.m_AllTaskFinish = true
function LuaZhouNianQing2023Mgr:SendZhanLingTaskData(taskDataUD)
    self.m_DailyTask = {}
    self.m_WeekTask = {}
    local list = MsgPackImpl.unpack(taskDataUD)
	if list then 
        self.m_AllTaskFinish = true
        for i = 0, list.Count - 1, 2 do
            local t = {}
            t.taskId = list[i]
            t.finishCount = list[i+1]
            t.progress = 0
            local task = ZhanLing_Task.GetData(t.taskId)
            if task then
                t.finishCount = math.min(task.Target,t.finishCount)
                t.progress = t.finishCount / task.Target
                if t.progress < 1 then 
                    self.m_AllTaskFinish = false
                end
                if task.IsDailyTask == 1 then
                    table.insert(self.m_DailyTask,t)
                else
                    table.insert(self.m_WeekTask,t)
                end
            end            
        end
    end
    if self.m_bLoadingCnt then
        self:TryShowZhanLingWnd(1)
    end
end

function LuaZhouNianQing2023Mgr:GetFinishTaskCnt(isDaily)
    local tbl = isDaily and self.m_DailyTask or self.m_WeekTask
    local cnt = 0
    for _, v in ipairs(tbl) do
        if v.progress == 1 then 
            cnt = cnt + 1
        end
    end
    return cnt
end

function LuaZhouNianQing2023Mgr:UnLockZhanLingVipSuccess(vip)
    self.m_IsVip1 = vip >= 1
	self.m_IsVip2 = vip >= 2
    g_ScriptEvent:BroadcastInLua("ZNQ2023_UnLockZhanLingVipSuccess", vip)
end

function LuaZhouNianQing2023Mgr:MergeItems(items)
    table.sort(items, function(a, b)
        return a.ItemID < b.ItemID
    end)
    local ret = {}
    for _, v in ipairs(items) do
        if #ret == 0 or ret[#ret].ItemID ~= v.ItemID then 
            table.insert(ret, v)
        else
            ret[#ret].Count = ret[#ret].Count + v.Count
        end
    end
    return ret
end

function LuaZhouNianQing2023Mgr:ReceiveZhanLingRewardSuccess(level, index)
    local lv, _ = self:GetCurZhanLingLevel()
    local items = {}
    if level == 0 and index == 0 then
		for i = 1, lv do
			local data =  self.m_ZhanLingData[i]
			if not data then
				data = {}
			end
			local flag = 1
            if data.item1 and data.flag and data.flag % 2 == 0 then
                table.insert(items, {ItemID = data.item1[1], Count = data.item1[2]})
            end
			if self.m_IsVip1 then
				flag = flag + 2
                if data.item2 and data.flag and math.floor(data.flag / 2) % 2 == 0 then
                    table.insert(items, {ItemID = data.item2[1], Count = data.item2[2]})
                end 
			end
			if self.m_IsVip2 then
				flag = flag + 4
                if data.item3 and data.flag and math.floor(data.flag / 4) % 2 == 0 then
                    table.insert(items, {ItemID = data.item3[1], Count = data.item3[2]})
                end 
			end
			data.flag = flag
			data.level = i
		end

        LuaCommonGetRewardWnd.m_Reward1List = self:MergeItems(items)
        items = {}
        for i = 1, lv do
            if not self.m_IsVip1 and self.m_ZhanLingData[i].item2 then 
                table.insert(items, {ItemID = self.m_ZhanLingData[i].item2[1], Count = self.m_ZhanLingData[i].item2[2]})
            end
            if not self.m_IsVip2 and self.m_ZhanLingData[i].item3 then 
                table.insert(items, {ItemID = self.m_ZhanLingData[i].item3[1], Count = self.m_ZhanLingData[i].item3[2]})
            end
        end
        LuaCommonGetRewardWnd.m_Reward2List = self:MergeItems(items)
        LuaCommonGetRewardWnd.m_materialName = "gongxihuode"
        if not self.m_IsVip1 and not self.m_IsVip2 then 
            LuaCommonGetRewardWnd.m_Reward2Label = LocalString.GetString("解锁[ff5050]首夏清和灯[-]、[ff88ff]四季同辉灯[-]还可获得：")
        elseif not self.m_IsVip1 then 
            LuaCommonGetRewardWnd.m_Reward2Label = LocalString.GetString("解锁[ff5050]首夏清和灯[-]还可获得：")
        elseif not self.m_IsVip2 then 
            LuaCommonGetRewardWnd.m_Reward2Label = LocalString.GetString("解锁[ff88ff]四季同辉灯[-]还可获得：")
        else
            LuaCommonGetRewardWnd.m_Reward2Label = ""
        end
        
        LuaCommonGetRewardWnd.m_hint = ""
        LuaCommonGetRewardWnd.m_button = {
            {
                spriteName = "blue", buttonLabel = LocalString.GetString("确定"), 
                clickCB = function() 
                    CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd) 
                end
            },
        }
        if not self.m_IsVip1 or not self.m_IsVip2 then 
            LuaCommonGetRewardWnd.m_button[2] =
            {
                spriteName = "yellow", buttonLabel = LocalString.GetString("解锁高级愿灯"), 
                clickCB = function() 
                    CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd) 
                    CUIManager.ShowUI(CLuaUIResources.ZhouNianQingPassportUnlockWnd)
                end
            }
        end
        CUIManager.ShowUI(CLuaUIResources.CommonGetRewardWnd)
	else
		local data = LuaZhouNianQing2023Mgr.m_ZhanLingData[level]
		if data then
			local flag = data.flag
			data.flag = bit.bor(flag,2^(index-1))
		end
	end

    g_ScriptEvent:BroadcastInLua("ZNQ2023_ReceiveZhanLingRewardSuccess", level, index)
end

function LuaZhouNianQing2023Mgr:GetCurZhanLingLevel()
    local setting = ZhanLing_Setting.GetData()
    local vip2AddRate = setting.Vip2AddRate
    local vip2AddValue = setting.Vip2AddValue
    local vip1AddValue = setting.Vip1AddValue

    local p1 = self.m_ZhanLingP1
    local p2 = self.m_ZhanLingP2
    local p3 = self.m_ZhanLingP3
    -- vip2加成
    if self.m_IsVip2 then
		p2 = math.floor(p2 * vip2AddRate)
		p1 = p1 + vip2AddValue
	end

    -- vip1加成
    if self.m_IsVip1 then
		p1 = p1 + vip1AddValue
	end
    
    local level, _ = self:GetLevelByTotalProgress(p1+p2+p3)

    return level, p1 + p2 + p3
end

LuaZhouNianQing2023Mgr.m_Level2NeedProgress = nil
function LuaZhouNianQing2023Mgr:GetLevelByTotalProgress(progress)
    local maxLevel = 0
	local maxLevelProgress = 0
    if not self.m_Level2NeedProgress or not next(self.m_Level2NeedProgress) then
        self.m_Level2NeedProgress = {}
        ZhanLing_Reward.Foreach(function(level,data)
			self.m_Level2NeedProgress[level] = data.NeedProgress
		end)
    end

	for level, needProgress in pairs(self.m_Level2NeedProgress) do
		if progress >= needProgress and level > maxLevel then
			maxLevel = level
			maxLevelProgress = needProgress
		end
	end

	return maxLevel, maxLevelProgress
end

function LuaZhouNianQing2023Mgr:BuyVip(vip)
    local setting = ZhanLing_Setting.GetData()
    if vip == 1 then
        Gac2Gas.BuyMallItem(EShopMallRegion_lua.ELingyuMallLimit, setting.Vip1PID, 1)
    else
        Gac2Gas.BuyMallItem(EShopMallRegion_lua.ELingyuMallLimit, setting.Vip2PID, 1)
    end
end

function LuaZhouNianQing2023Mgr:OpenWnd(zhanlingId)
    local task = ZhanLing_Task.GetData(zhanlingId)
    local actionString = task.Action
    local actionName,parameters = string.match(actionString,"(%w+)%(([^%s]-)%)")

    --[[if parameters == nil then
        local now = CServerTimeMgr.Inst:GetZone8Time()
        local beginDate =  CreateFromClass(DateTime, 2022, 5, 19, 18, 0, 0)
        local endDate =  CreateFromClass(DateTime, 2022, 5, 19, 19, 30, 0)
        if DateTime.Compare(now, beginDate) >= 0 and DateTime.Compare(now, endDate) <= 0 then
            --开放中
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("点击主界面屏幕“播”字按钮，观看周年庆直播。"))            
        elseif DateTime.Compare(now, beginDate) < 0 then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("18:00-19:30点击主界面屏幕“播”字按钮，观看周年庆直播。"))
        else
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("很遗憾，周年庆直播已结束。"))
        end
        return
    end--]]

    local options = g_LuaUtil:StrSplitAdv(parameters,",")
    if actionName == "OpenWnd" then
        local wndName = options[1]
        wndName = string.match(wndName,"(%w+)")
        local methodName = options[2]
        CUIActionMgr.Inst:Show(wndName, methodName,nil,nil,nil,nil)
    end
end

--#endregion 战令


function LuaZhouNianQing2023Mgr:MakeTabenSuccess(count)
    LuaCommonGetRewardWnd.m_materialName = "gongxihuode"
    LuaCommonGetRewardWnd.m_Reward1List = {{ItemID=ZhouNianQing2023_Setting.GetData().TabenItemId, Count=count}}
    LuaCommonGetRewardWnd.m_Reward2Label = ""
    LuaCommonGetRewardWnd.m_Reward2List = {}
    LuaCommonGetRewardWnd.m_hint = ""
    LuaCommonGetRewardWnd.m_button = {
        {spriteName = "yellow", buttonLabel = LocalString.GetString("确定"), clickCB = function() CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd) end},
    }
    CUIManager.ShowUI(CLuaUIResources.CommonGetRewardWnd)
end

LuaZhouNianQing2023Mgr.m_WaitForMainPlayerTick = nil
function LuaZhouNianQing2023Mgr:WaitForMainPlayerCreated(func)
    if CClientMainPlayer.Inst then 
        func() 
        return
    end
    UnRegisterTick(self.m_WaitForMainPlayerTick)
    self.m_WaitForMainPlayerTick = RegisterTick(function()
        if CClientMainPlayer.Inst then 
            func()
            UnRegisterTick(self.m_WaitForMainPlayerTick)
        end
    end, 1000)
end

function LuaZhouNianQing2023Mgr:SetConfidantId(playerId)
    self:WaitForMainPlayerCreated(function()
        local info = CreateFromClass(CTempRelationInfo)
        info.Data.m_data = CommonDefs.UTF8Encoding_GetBytes(tostring(playerId))
        info.ExpiredTime = CServerTimeMgr.Inst:GetTimeStampByStr(ZhouNianQing2023_PSZJSetting.GetData().PlayCloseTime)
        CommonDefs.DictSet_LuaCall(CClientMainPlayer.Inst.RelationshipProp.TempRelationData, 1, info)
    end)
    g_ScriptEvent:BroadcastInLua("ZNQ2023_SetConfidantId", playerId)
end
