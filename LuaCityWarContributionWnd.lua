require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UISprite = import "UISprite"
local UILabel = import "UILabel"
local UITabBar = import "L10.UI.UITabBar"
local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local Profession = import "L10.Game.Profession"
local CButton = import "L10.UI.CButton"
local LocalString = import "LocalString"
local Constants = import "L10.Game.Constants"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local CPlayerInfoMgrAlignType = import "CPlayerInfoMgr+AlignType"
local UITable = import "UITable"
local UIPanel = import "UIPanel"
local UIWidget = import "UIWidget"
local UIScrollViewIndicator = import "UIScrollViewIndicator"

LuaCityWarContributionWnd = class()   

RegistClassMember(LuaCityWarContributionWnd, "m_CloseBtn")
RegistClassMember(LuaCityWarContributionWnd, "m_TabBar")

RegistClassMember(LuaCityWarContributionWnd, "m_HeadTable")
RegistClassMember(LuaCityWarContributionWnd, "m_HeadNameBtn")
RegistClassMember(LuaCityWarContributionWnd, "m_HeadLevelBtn")
RegistClassMember(LuaCityWarContributionWnd, "m_HeadYaYunTimesBtn")
RegistClassMember(LuaCityWarContributionWnd, "m_HeadZhuangTianGongFengTimesBtn")
RegistClassMember(LuaCityWarContributionWnd, "m_HeadZiCaiTimesBtn")
RegistClassMember(LuaCityWarContributionWnd, "m_HeadGateProgressTimesBtn")
RegistClassMember(LuaCityWarContributionWnd, "m_HeadPublicMapScoreBtn")
RegistClassMember(LuaCityWarContributionWnd, "m_HeadMirrorWarScoreBtn")

RegistClassMember(LuaCityWarContributionWnd, "m_MyTable")
RegistClassMember(LuaCityWarContributionWnd, "m_MyIcon")
RegistClassMember(LuaCityWarContributionWnd, "m_MyNameLabel")
RegistClassMember(LuaCityWarContributionWnd, "m_MyLevelLabel")
RegistClassMember(LuaCityWarContributionWnd, "m_MyYaYunTimesLabel")
RegistClassMember(LuaCityWarContributionWnd, "m_MyZhuangTianGongFengTimesLabel")
RegistClassMember(LuaCityWarContributionWnd, "m_MyZiCaiTimesLabel")
RegistClassMember(LuaCityWarContributionWnd, "m_MyGateProgressTimesLabel")
RegistClassMember(LuaCityWarContributionWnd, "m_MyPublicMapScoreLabel")
RegistClassMember(LuaCityWarContributionWnd, "m_MyMirrorWarScoreLabel")

RegistClassMember(LuaCityWarContributionWnd,"m_TableView")
RegistClassMember(LuaCityWarContributionWnd,"m_TableViewDataSource")
RegistClassMember(LuaCityWarContributionWnd,"m_DisplayResult")
RegistClassMember(LuaCityWarContributionWnd,"m_MainPlayerInfo")

RegistClassMember(LuaCityWarContributionWnd,"m_HeadButtons")
RegistClassMember(LuaCityWarContributionWnd,"m_SortIndex")
RegistClassMember(LuaCityWarContributionWnd,"m_SortOrder")

RegistClassMember(LuaCityWarContributionWnd,"m_IsSeasonData")

RegistClassMember(LuaCityWarContributionWnd,"m_ScrollViewPanel")
RegistClassMember(LuaCityWarContributionWnd,"m_ScrollViewContainerWidget")
RegistClassMember(LuaCityWarContributionWnd,"m_UIScrollViewIndicator")
RegistClassMember(LuaCityWarContributionWnd,"m_BottomLabel")

function LuaCityWarContributionWnd:Awake()
    
end

function LuaCityWarContributionWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaCityWarContributionWnd:Init()
	self.m_CloseBtn = self.transform:Find("Wnd_Bg_Primary_Tab/CloseButton").gameObject
	CommonDefs.AddOnClickListener(self.m_CloseBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)
	self.m_TabBar = self.transform:Find("TabBar"):GetComponent(typeof(UITabBar))
	self.m_TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index) self:OnTabChange(go, index) end)

	self.m_TableView = self.transform:Find("TableView"):GetComponent(typeof(QnTableView))
	self.m_TableViewDataSource=DefaultTableViewDataSource.Create(function() 
			return self:NumberOfItems()  
		end, function(item, index)
			self:InitItem(item, index)
		end)

	self.m_DisplayResult = {}
	self.m_MainPlayerInfo = {}
	self.m_TableView.m_DataSource=self.m_TableViewDataSource
    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)

    self.m_HeadTable = self.transform:Find("TableView/Header/Table"):GetComponent(typeof(UITable))
    self.m_HeadNameBtn = self.transform:Find("TableView/Header/Table/Name"):GetComponent(typeof(CButton))
	self.m_HeadLevelBtn = self.transform:Find("TableView/Header/Table/Level"):GetComponent(typeof(CButton))
	self.m_HeadYaYunTimesBtn = self.transform:Find("TableView/Header/Table/YaYunTimes"):GetComponent(typeof(CButton))
	self.m_HeadZhuangTianGongFengTimesBtn = self.transform:Find("TableView/Header/Table/ZhuangTianGongFengTimes"):GetComponent(typeof(CButton))
	self.m_HeadZiCaiTimesBtn = self.transform:Find("TableView/Header/Table/ZiCaiTimes"):GetComponent(typeof(CButton))
	self.m_HeadGateProgressTimesBtn = self.transform:Find("TableView/Header/Table/GateProgressTimes"):GetComponent(typeof(CButton))
	self.m_HeadPublicMapScoreBtn = self.transform:Find("TableView/Header/Table/PublicMapScore"):GetComponent(typeof(CButton))
	self.m_HeadMirrorWarScoreBtn = self.transform:Find("TableView/Header/Table/MirrorWarScore"):GetComponent(typeof(CButton))

	self.m_HeadButtons = {}
	table.insert(self.m_HeadButtons, {button=self.m_HeadNameBtn, defaultText=LocalString.GetString("角色职业、名称"), status=0}) -- 0默认 -1降序 1升序
	table.insert(self.m_HeadButtons, {button=self.m_HeadLevelBtn, defaultText=LocalString.GetString("等级"), status=0})
	table.insert(self.m_HeadButtons, {button=self.m_HeadYaYunTimesBtn, defaultText=LocalString.GetString("押镖"), status=0})
	table.insert(self.m_HeadButtons, {button=self.m_HeadZhuangTianGongFengTimesBtn, defaultText=LocalString.GetString("装填/供奉资源"), status=0})
	table.insert(self.m_HeadButtons, {button=self.m_HeadZiCaiTimesBtn, defaultText=LocalString.GetString("资源提交"), status=0})
	table.insert(self.m_HeadButtons, {button=self.m_HeadGateProgressTimesBtn, defaultText=LocalString.GetString("建灵飞镜"), status=0})
	table.insert(self.m_HeadButtons, {button=self.m_HeadPublicMapScoreBtn, defaultText=LocalString.GetString("野外评价"), status=0})
	table.insert(self.m_HeadButtons, {button=self.m_HeadMirrorWarScoreBtn, defaultText=LocalString.GetString("宣战评价"), status=0})

	for _,val in pairs(self.m_HeadButtons) do
		CommonDefs.AddOnClickListener(val.button.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnHeaderButtonClick(go) end), false)
	end

    local mainplayerRoot = self.transform:Find("TableView/Header/MainPlayer/Table")
    self.m_MyTable = mainplayerRoot:GetComponent(typeof(UITable))
    self.m_MyIcon = mainplayerRoot:Find("Icon"):GetComponent(typeof(UISprite))
	self.m_MyNameLabel = mainplayerRoot:Find("NameLabel"):GetComponent(typeof(UILabel))
	self.m_MyLevelLabel = mainplayerRoot:Find("LevelLabel"):GetComponent(typeof(UILabel))
	self.m_MyYaYunTimesLabel = mainplayerRoot:Find("YaYunTimesLabel"):GetComponent(typeof(UILabel))
	self.m_MyZhuangTianGongFengTimesLabel = mainplayerRoot:Find("ZhuangTianGongFengTimesLabel"):GetComponent(typeof(UILabel))
	self.m_MyZiCaiTimesLabel = mainplayerRoot:Find("ZiCaiTimesLabel"):GetComponent(typeof(UILabel))
	self.m_MyGateProgressTimesLabel = mainplayerRoot:Find("GateProgressTimesLabel"):GetComponent(typeof(UILabel))
	self.m_MyPublicMapScoreLabel = mainplayerRoot:Find("PublicMapScoreLabel"):GetComponent(typeof(UILabel))
	self.m_MyMirrorWarScoreLabel = mainplayerRoot:Find("MirrorWarScoreLabel"):GetComponent(typeof(UILabel))

	self.m_ScrollViewPanel = self.transform:Find("TableView/ScrollView"):GetComponent(typeof(UIPanel))
	self.m_ScrollViewContainerWidget = self.transform:Find("TableView/Container"):GetComponent(typeof(UIWidget))
	self.m_UIScrollViewIndicator = self.transform:Find("TableView/ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator))
	self.m_BottomLabel = self.transform:Find("TableView/BottomLabel"):GetComponent(typeof(UILabel))
	self.m_BottomLabel.text = g_MessageMgr:FormatMessage("CityWarContributionWnd_BottomText")

	self.m_IsSeasonData = false
	self.m_TabBar:ChangeTab(0)
end

function LuaCityWarContributionWnd:NumberOfItems()
	return #self.m_DisplayResult
end

function LuaCityWarContributionWnd:InitItem(item, index)
	local info = self.m_DisplayResult[index+1]
	if not info then return end
	local root = item.transform:Find("Table")

	local m_Table = root:GetComponent(typeof(UITable))
   	local m_Icon = root:Find("Icon"):GetComponent(typeof(UISprite))
	local m_NameLabel = root:Find("NameLabel"):GetComponent(typeof(UILabel))
	local m_LevelLabel = root:Find("LevelLabel"):GetComponent(typeof(UILabel))
	local m_YaYunTimesLabel = root:Find("YaYunTimesLabel"):GetComponent(typeof(UILabel))
	local m_ZhuangTianGongFengTimesLabel = root:Find("ZhuangTianGongFengTimesLabel"):GetComponent(typeof(UILabel))
	local m_ZiCaiTimesLabel = root:Find("ZiCaiTimesLabel"):GetComponent(typeof(UILabel))
	local m_GateProgressTimesLabel = root:Find("GateProgressTimesLabel"):GetComponent(typeof(UILabel))
	local m_PublicMapScoreLabel = root:Find("PublicMapScoreLabel"):GetComponent(typeof(UILabel))
	local m_MirrorWarScoreLabel = root:Find("MirrorWarScoreLabel"):GetComponent(typeof(UILabel))

	m_Icon.spriteName = Profession.GetIconByNumber(info.class)
	CUICommonDef.SetGrey(m_Icon.gameObject, not info.isOnline)
	m_NameLabel.text = info.name
	m_LevelLabel.text = CUICommonDef.GetColoredLevelString(info.xianFanStatus and info.xianFanStatus>0, info.level, m_LevelLabel.color)
	m_YaYunTimesLabel.text = tostring(info.yayunTimes)
	m_ZhuangTianGongFengTimesLabel.text = SafeStringFormat3("%d/%d",info.zhuangtianTimes, info.gongfengTimes)
	m_ZiCaiTimesLabel.text = tostring(info.zicaiTimes)
	m_GateProgressTimesLabel.text = tostring(info.gateProgressTimes)
	m_PublicMapScoreLabel.text = tostring(info.publicMapScore)
	m_MirrorWarScoreLabel.text = tostring(info.mirrorWarScore)

	--赛季比周多了一项，对排版进行特殊处理
	local origionWidth = 180
	local extendWidth = 180 + 60
	if self.m_IsSeasonData then
		m_GateProgressTimesLabel.gameObject:SetActive(true)
		m_ZiCaiTimesLabel.width = origionWidth
		m_PublicMapScoreLabel.width = origionWidth
		m_MirrorWarScoreLabel.width = origionWidth
	else
		m_GateProgressTimesLabel.gameObject:SetActive(false)
		m_ZiCaiTimesLabel.width = extendWidth
		m_PublicMapScoreLabel.width = extendWidth
		m_MirrorWarScoreLabel.width = extendWidth
	end

	m_Table:Reposition()

	if (index+1) % 2 == 0 then
		item:SetBackgroundTexture(Constants.EvenBgSprite)
	else
		item:SetBackgroundTexture(Constants.OddBgSpirite)
	end
end

function LuaCityWarContributionWnd:OnSelectAtRow(row)
	local info = self.m_DisplayResult[row+1]
	if not info then return end
	CPlayerInfoMgr.ShowPlayerPopupMenu(info.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, 
		CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgrAlignType.Default)
end

function LuaCityWarContributionWnd:OnTabChange(go, index)

	if index == 0 then
		self.m_ScrollViewContainerWidget.height = 717
		self.m_BottomLabel.gameObject:SetActive(true)
		CLuaGuildMgr.m_CityWarWeekInfo = {}
		Gac2Gas.RequestGetGuildInfo("CityWarWeekInfo")

	elseif index == 1 then
		self.m_ScrollViewContainerWidget.height = 777
		self.m_BottomLabel.gameObject:SetActive(false)
		CLuaGuildMgr.m_CityWarSeasonInfo = {}
		Gac2Gas.RequestGetGuildInfo("CityWarSeasonInfo")
	end

	self.m_ScrollViewPanel:ResetAndUpdateAnchors()
	self.m_UIScrollViewIndicator:Layout()

	self.m_DisplayResult = {}
	self.m_MainPlayerInfo = {}
	self.m_SortIndex = EnumCityWarContributionSortIndex.eDefault
	self.m_SortOrder = EnumCityWarContributionSortOrder.eDefault
	for idx,val in pairs(self.m_HeadButtons) do
		val.status = 0
		val.button.Text = val.defaultText
	end
	self:LoadData()
end

function LuaCityWarContributionWnd:OnHeaderButtonClick(go)

	for idx,val in pairs(self.m_HeadButtons) do
		if go == val.button.gameObject then
			self.m_SortIndex = idx
			if val.status == 0 or val.status ==1 then
				val.status = -1
				self.m_SortOrder = EnumCityWarContributionSortOrder.eDescendingOrder
				val.button.Text = val.defaultText..LocalString.GetString("▼")
			else
				val.status = 1
				self.m_SortOrder = EnumCityWarContributionSortOrder.eAscendingOrder
				val.button.Text = val.defaultText..LocalString.GetString("▲")
			end
		else
			val.status = 0
			val.button.Text = val.defaultText
		end
	end

	self:LoadData()
end

function LuaCityWarContributionWnd:SortDisplayResult()
	local idx = self.m_SortIndex or EnumCityWarContributionSortIndex.eDefault -- 默认按是否在线
	local sortOrder = self.m_SortOrder or EnumCityWarContributionSortOrder.eDescendingOrder

	table.sort(self.m_DisplayResult, function (a, b)
		local result = 0
		if idx == EnumCityWarContributionSortIndex.eName then --名字
			if sortOrder == EnumCityWarContributionSortOrder.eDescendingOrder then --降序
				result = cs_string.Compare(b.name, a.name)
			else
				result = cs_string.Compare(a.name, b.name)
			end
			
		elseif idx == EnumCityWarContributionSortIndex.eLevel then --等级
			result = self:CompareInteger(a, b, "xianFanStatus", sortOrder) --先根据仙凡状态排序
			if result == 0 then
				result = self:CompareInteger(a, b, "level", sortOrder)
			end
		elseif idx == EnumCityWarContributionSortIndex.eYaYunTimes then --押镖次数
			result = self:CompareInteger(a, b, "yayunTimes", sortOrder)
		elseif idx == EnumCityWarContributionSortIndex.eZhuangTianGongFengTimes then --装填/供奉次数 优先按照供奉，然后按装填 - by尹辉
			result = self:CompareInteger(a, b, "gongfengTimes", sortOrder)
			if result == 0 then
				result = self:CompareInteger(a, b, "zhuangtianTimes", sortOrder)
			end
		elseif idx == EnumCityWarContributionSortIndex.eZiCaiTimes then --城建资材提交次数
			result = self:CompareInteger(a, b, "zicaiTimes", sortOrder)
		elseif idx == EnumCityWarContributionSortIndex.eGateProgressTimes then --建灵飞镜
			result = self:CompareInteger(a, b, "gateProgressTimes", sortOrder)
		elseif idx == EnumCityWarContributionSortIndex.ePublicMapScore then --野外评价
			result = self:CompareInteger(a, b, "publicMapScore", sortOrder)
		elseif idx == EnumCityWarContributionSortIndex.eMirrorWarScore then --宣战评价
			result = self:CompareInteger(a, b, "mirrorWarScore", sortOrder)
		else
			if a.isOnline ~= b.isOnline then
				return a.isOnline --默认在线排前面
			end
		end

		if result~=0 then
			return result<0
		else
			--无论升序降序，排序条件不满足的情况下，在线排前面
			if a.isOnline ~= b.isOnline then
				return a.isOnline
			end
			if sortOrder == EnumCityWarContributionSortOrder.eDescendingOrder then --降序

				return b.playerId < a.playerId
			else
				return a.playerId < b.playerId
			end
		end
	end)
end

function LuaCityWarContributionWnd:CompareInteger(a,b,fieldName,sortOrder)
	local result = 0
	if sortOrder == EnumCityWarContributionSortOrder.eDescendingOrder then --降序
		if b[fieldName] ~= a[fieldName] then
			if b[fieldName] < a[fieldName] then
				result = -1
			else
				result = 1
			end
		end
	else
		if b[fieldName] ~= a[fieldName] then
			if a[fieldName] < b[fieldName] then
				result = -1
			else
				result = 1
			end
		end
	end
	return result
end

function LuaCityWarContributionWnd:LoadData()
	-- 排序
	self:SortDisplayResult()

	-- MainPlayerInfo
	if self.m_MainPlayerInfo and self.m_MainPlayerInfo.playerId then
		self.m_MyIcon.spriteName =  Profession.GetIconByNumber(self.m_MainPlayerInfo.class)
		self.m_MyNameLabel.text = self.m_MainPlayerInfo.name
		self.m_MyLevelLabel.text = CUICommonDef.GetColoredLevelString(self.m_MainPlayerInfo.xianFanStatus and self.m_MainPlayerInfo.xianFanStatus > 0, self.m_MainPlayerInfo.level, self.m_MyLevelLabel.color)
		self.m_MyYaYunTimesLabel.text = tostring(self.m_MainPlayerInfo.yayunTimes)
		self.m_MyZhuangTianGongFengTimesLabel.text = SafeStringFormat3("%d/%d",self.m_MainPlayerInfo.zhuangtianTimes, self.m_MainPlayerInfo.gongfengTimes)
		self.m_MyZiCaiTimesLabel.text = tostring(self.m_MainPlayerInfo.zicaiTimes)
		self.m_MyGateProgressTimesLabel.text = tostring(self.m_MainPlayerInfo.gateProgressTimes)
		self.m_MyPublicMapScoreLabel.text = tostring(self.m_MainPlayerInfo.publicMapScore)
		self.m_MyMirrorWarScoreLabel.text = tostring(self.m_MainPlayerInfo.mirrorWarScore)
	else
		self.m_MyIcon.spriteName =  nil
		self.m_MyNameLabel.text = nil
		self.m_MyLevelLabel.text = nil
		self.m_MyYaYunTimesLabel.text = nil
		self.m_MyZhuangTianGongFengTimesLabel.text = nil
		self.m_MyZiCaiTimesLabel.text = nil
		self.m_MyGateProgressTimesLabel.text = nil
		self.m_MyPublicMapScoreLabel.text = nil
		self.m_MyMirrorWarScoreLabel.text = nil
	end
	--赛季比周多了一项，对排版进行特殊处理
	local origionWidth = 180
	local extendWidth = 180 + 60
	if self.m_IsSeasonData then
		self.m_HeadGateProgressTimesBtn.gameObject:SetActive(true)
		self.m_HeadZiCaiTimesBtn.background.width = origionWidth
		self.m_HeadPublicMapScoreBtn.background.width = origionWidth
		self.m_HeadMirrorWarScoreBtn.background.width = origionWidth

		self.m_MyGateProgressTimesLabel.gameObject:SetActive(true)
		self.m_MyZiCaiTimesLabel.width = origionWidth
		self.m_MyPublicMapScoreLabel.width = origionWidth
		self.m_MyMirrorWarScoreLabel.width = origionWidth
	else
		self.m_HeadGateProgressTimesBtn.gameObject:SetActive(false)
		self.m_HeadZiCaiTimesBtn.background.width = extendWidth
		self.m_HeadPublicMapScoreBtn.background.width = extendWidth
		self.m_HeadMirrorWarScoreBtn.background.width = extendWidth
		self.m_MyGateProgressTimesLabel.gameObject:SetActive(false)
		self.m_MyZiCaiTimesLabel.width = extendWidth
		self.m_MyPublicMapScoreLabel.width = extendWidth
		self.m_MyMirrorWarScoreLabel.width = extendWidth
	end

	self.m_HeadTable:Reposition()
	self.m_MyTable:Reposition()

	self.m_TableView:ReloadData(true,false)
end

function LuaCityWarContributionWnd:OnEnable()
	g_ScriptEvent:AddListener("SendGuildInfo_CityWarWeekInfo", self, "SendGuildInfo_CityWarWeekInfo")
	g_ScriptEvent:AddListener("SendGuildInfo_CityWarSeasonInfo", self, "SendGuildInfo_CityWarSeasonInfo")
end

function LuaCityWarContributionWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendGuildInfo_CityWarWeekInfo", self, "SendGuildInfo_CityWarWeekInfo")
	g_ScriptEvent:RemoveListener("SendGuildInfo_CityWarSeasonInfo", self, "SendGuildInfo_CityWarSeasonInfo")
end

function LuaCityWarContributionWnd:SendGuildInfo_CityWarWeekInfo()
	self.m_DisplayResult = {}
	self.m_MainPlayerInfo = {}
	local mainplayerId = 0
	if CClientMainPlayer.Inst then mainplayerId = CClientMainPlayer.Inst.Id end
	if CLuaGuildMgr.m_CityWarWeekInfo then
		for memberId, data in pairs(CLuaGuildMgr.m_CityWarWeekInfo) do

			local info = {
				playerId=data.PlayerId, 
				name=data.Name, 
				level=data.Level, 
				class=data.Class, 
				isOnline=(data.IsOnline>0), 
				xianFanStatus=data.XianFanStatus,
				yayunTimes = data.SubmitToGuildTimes,
				zhuangtianTimes = data.SubmitToBiaoCheTimes,
				gongfengTimes = data.SubmitToGuildResource,
				zicaiTimes = data.SubmitToGuildZiCai,
				gateProgressTimes = 0,--week数据没有SubmitToGuildResource
				publicMapScore = data.PublicMapScore,
				mirrorWarScore = data.MirrorWarScore }
			if mainplayerId == memberId then
				self.m_MainPlayerInfo = info
			else
				table.insert(self.m_DisplayResult, info)
			end
		end
	end
	self.m_IsSeasonData = false
	self:LoadData()
end

function LuaCityWarContributionWnd:SendGuildInfo_CityWarSeasonInfo()
	self.m_DisplayResult = {}
	self.m_MainPlayerInfo = {}
	local mainplayerId = 0
	if CClientMainPlayer.Inst then mainplayerId = CClientMainPlayer.Inst.Id end
	if CLuaGuildMgr.m_CityWarSeasonInfo then
		for memberId, data in pairs(CLuaGuildMgr.m_CityWarSeasonInfo) do

			local info = {
				playerId=data.PlayerId, 
				name=data.Name, 
				level=data.Level, 
				class=data.Class, 
				isOnline=(data.IsOnline>0), 
				xianFanStatus=data.XianFanStatus,
				yayunTimes = data.SubmitToGuildTimes,
				zhuangtianTimes = data.SubmitToBiaoCheTimes,
				gongfengTimes = data.SubmitToGuildResource,
				zicaiTimes = data.SubmitToGuildZiCai,
				gateProgressTimes = data.SubmitGateProgressTimes,
				publicMapScore = data.PublicMapScore,
				mirrorWarScore = data.MirrorWarScore }
			if mainplayerId == memberId then
				self.m_MainPlayerInfo = info
			else
				table.insert(self.m_DisplayResult, info)
			end
		end
	end
	self.m_IsSeasonData = true
	self:LoadData()
end
