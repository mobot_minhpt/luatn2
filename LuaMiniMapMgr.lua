require("common/common_include")

local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaMiniMapMgr = class()

LuaMiniMapMgr.BiaoCheMonsterIds = nil

function LuaMiniMapMgr.ForceShowBiaoChe(monsterTemplateId, monsterEngineId)
    --只显示自己的镖车
    if not LuaMiniMapMgr.BiaoCheMonsterIds then
    	--跟随玩家的镖车MonsterId,前四个是正常镖车，后四个是损坏的镖车
    	LuaMiniMapMgr.BiaoCheMonsterIds = {}
    	LuaMiniMapMgr.BiaoCheMonsterIds[18003923] = 1
    	LuaMiniMapMgr.BiaoCheMonsterIds[18003924] = 1
    	LuaMiniMapMgr.BiaoCheMonsterIds[18003925] = 1
    	LuaMiniMapMgr.BiaoCheMonsterIds[18003926] = 1
    	LuaMiniMapMgr.BiaoCheMonsterIds[18003746] = 1
    	LuaMiniMapMgr.BiaoCheMonsterIds[18003747] = 1
    	LuaMiniMapMgr.BiaoCheMonsterIds[18003748] = 1
    	LuaMiniMapMgr.BiaoCheMonsterIds[18003749] = 1
    end

    if LuaMiniMapMgr.BiaoCheMonsterIds[monsterTemplateId] then
        local mainplayer = CClientMainPlayer.Inst
        if mainplayer ~= nil and CLuaCityWarMgr.MainPlayerBiaoCheIsValidForMiniMap() and monsterEngineId == mainplayer.PlayProp.YaYunData.TeamBiaoCheEngineId then
            return true
        end
    end
    return false
end

LuaMiniMapMgr.m_PopWndWorldMapReplaceInfoDict = {}

function LuaMiniMapMgr:ChangePopWndWorldMapInfo(sceneId)
    local data = ShiMenShouWei_Scene.GetData(sceneId)
    if data then
        self.m_PopWndWorldMapReplaceInfoDict[data.RootName] = {
            Icon = data.ReplaceIcons.."_building",
            MapId = data.TeleportRedirect,
            NameIcon = data.ReplaceIcons,
        }
    end
end

