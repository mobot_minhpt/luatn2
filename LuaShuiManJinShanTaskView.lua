local CScene = import "L10.Game.CScene"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CChatLinkMgr=import "CChatLinkMgr"
local UICamera = import "UICamera"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"
local CButton = import "L10.UI.CButton"
local String = import "System.String"

LuaShuiManJinShanTaskView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShuiManJinShanTaskView, "TaskName", "TaskName", UILabel)
RegistChildComponent(LuaShuiManJinShanTaskView, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaShuiManJinShanTaskView, "LeaveBtn", "LeaveBtn", CButton)
RegistChildComponent(LuaShuiManJinShanTaskView, "RuleBtn", "RuleBtn", CButton)
RegistChildComponent(LuaShuiManJinShanTaskView, "DescLab", "DescLab", UILabel)

--@endregion RegistChildComponent end

function LuaShuiManJinShanTaskView:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    UIEventListener.Get(self.LeaveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveBtnClick(go)
	end)

    UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick(go)
	end)
end

function LuaShuiManJinShanTaskView:Init()
    self:SwitchState(LuaShuiManJinShanMgr.m_PlayState)
end

function LuaShuiManJinShanTaskView:OnEnable()
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:AddListener("ShuiManJinShan_SyncPlayState", self, "OnShuiManJinShan_SyncPlayState")
end

function LuaShuiManJinShanTaskView:OnDisable( )
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("ShuiManJinShan_SyncPlayState", self, "OnShuiManJinShan_SyncPlayState")
end

function LuaShuiManJinShanTaskView:OnSceneRemainTimeUpdate(args)
    if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("剩余 [c][00ff00]%s[-][/c]"), self:GetRemainTimeText(CScene.MainScene.ShowTime))
        else
            self.CountDownLabel.text = nil
        end
    else
        self.CountDownLabel.text = nil
    end
end

function LuaShuiManJinShanTaskView:GetRemainTimeText(totalSeconds)
    if totalSeconds <= 1 then
        return ""
    end
    local time
    if totalSeconds >= 3600 then
        time = SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        time = SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
    return time
end

function LuaShuiManJinShanTaskView:OnShuiManJinShan_SyncPlayState(playState)
    self:SwitchState(playState)
end

function LuaShuiManJinShanTaskView:SwitchState(playState)
    if not CClientMainPlayer.Inst then return end
    local playState = playState or 0
    local stageData = XinBaiLianDong_ShuiManJinShanStage.GetData(playState)
    if stageData == nil then
        return
    end
    self.TaskName.text = SafeStringFormat3(LocalString.GetString("[水漫金山]%s"), stageData.StageName)
    self.DescLab.text = stageData.StageInfo
end
--@region UIEvent

function LuaShuiManJinShanTaskView:OnLeaveBtnClick(go)
    CGamePlayMgr.Inst:LeavePlay()
end

function LuaShuiManJinShanTaskView:OnRuleBtnClick(go)
    local stageData = XinBaiLianDong_ShuiManJinShanStage.GetData(LuaShuiManJinShanMgr.m_PlayState)
    if stageData == nil then
        return
    end
    g_MessageMgr:ShowMessage(stageData.StageInfoTip)
end
--@endregion UIEvent
