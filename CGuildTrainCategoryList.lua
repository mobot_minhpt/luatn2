-- Auto Generated!!
local CGuildTrainCategoryList = import "L10.UI.CGuildTrainCategoryList"
local CGuildTrainCategoryTemplate = import "L10.UI.CGuildTrainCategoryTemplate"
local CGuildTrainMgr = import "L10.Game.CGuildTrainMgr"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
CGuildTrainCategoryList.m_Init_CS2LuaHook = function (this) 

    Extensions.RemoveAllChildren(this.table.transform)
    this.categoryTemplate:SetActive(true)

    local categoryList = CGuildTrainMgr.Inst.trainCategoryList
    do
        local i = 0
        while i < categoryList.Count do
            local category = NGUITools.AddChild(this.table.gameObject, this.categoryTemplate)
            CommonDefs.GetComponent_GameObject_Type(category, typeof(CGuildTrainCategoryTemplate)):Init(i)
            i = i + 1
        end
    end
    this.categoryTemplate:SetActive(false)
    this.table:Reposition()
    this.scrollview:ResetPosition()
end
