local CUIFx = import "L10.UI.CUIFx"

local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UIProgressBar = import "UIProgressBar"
local CTickMgr = import "L10.Engine.CTickMgr"
local ETickType = import "L10.Engine.ETickType"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CPostEffect = import "L10.Engine.CPostEffect"

LuaGuanNingResurrectionWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuanNingResurrectionWnd, "ProgressLabel", "ProgressLabel", UILabel)
RegistChildComponent(LuaGuanNingResurrectionWnd, "Heart", "Heart", GameObject)
RegistChildComponent(LuaGuanNingResurrectionWnd, "RiseFx", "RiseFx", CUIFx)
RegistChildComponent(LuaGuanNingResurrectionWnd, "RebornFx", "RebornFx", CUIFx)
RegistChildComponent(LuaGuanNingResurrectionWnd, "LeftProgress", "LeftProgress", UIProgressBar)
RegistChildComponent(LuaGuanNingResurrectionWnd, "RightProgress", "RightProgress", UIProgressBar)

--@endregion RegistChildComponent end

RegistClassMember(LuaGuanNingResurrectionWnd, "m_ClickHintTick")
RegistClassMember(LuaGuanNingResurrectionWnd, "m_AutoRiseTick")
RegistClassMember(LuaGuanNingResurrectionWnd, "m_CloseWndTick")
RegistClassMember(LuaGuanNingResurrectionWnd, "m_OnMainplayerFightPropUpdate")

function LuaGuanNingResurrectionWnd:Awake()
    --CPostEffect.EnableDieMat();
    self.LeftProgress.value = 0
    self.RightProgress.value = 0
    self.ProgressLabel.text = 0

    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.Heart.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnHeartClick()
	end)

    --@endregion EventBind end

    self.m_OnMainplayerFightPropUpdate = DelegateFactory.Action(function()
        self:OnMainplayerFightPropUpdate()
    end)
end

function LuaGuanNingResurrectionWnd:Init()
    g_MessageMgr:ShowMessage("GuanNing_Resurrection_Tip")
    self:RefreshClickHint()
    self.m_AutoRiseTick = CTickMgr.Register(DelegateFactory.Action(function()
        self:ProgressRise(10)
    end), 2000, ETickType.Loop)

    self.RiseFx:LoadFx("fx/ui/prefab/UI_Guanning_shangzhang.prefab")
end

function LuaGuanNingResurrectionWnd:OnEnable()
    EventManager.AddListenerInternal(EnumEventType.MainplayerFightPropUpdate, self.m_OnMainplayerFightPropUpdate)
end

function LuaGuanNingResurrectionWnd:OnDisable()
    EventManager.RemoveListenerInternal(EnumEventType.MainplayerFightPropUpdate, self.m_OnMainplayerFightPropUpdate)
end

function LuaGuanNingResurrectionWnd:OnDestroy()
    --CPostEffect.DisableDieMat() --OnLeave_Death()
    self:CancelClickHintTick()
    self:CancelAutoRiseTick()
    self:CancelCloseWndTick()
end

--@region UIEvent

function LuaGuanNingResurrectionWnd:OnHeartClick()
    self:RefreshClickHint()
    self:ProgressRise(10)
end


--@endregion UIEvent

function LuaGuanNingResurrectionWnd:OnMainplayerFightPropUpdate()
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsAlive and self.ProgressLabel.text ~= "0" then
        CUIManager.CloseUI(CLuaUIResources.GuanNingResurrectionWnd)
    end
end

function LuaGuanNingResurrectionWnd:ProgressRise(percent) -- 0~100
    --self.RiseFx:LoadFx("fx/ui/prefab/UI_Guanning_shangzhang.prefab")
    if self.LeftProgress.value + percent / 100 < 1 then
        self.LeftProgress.value = self.LeftProgress.value + percent / 100
        self.RightProgress.value = self.LeftProgress.value
    else
        self.LeftProgress.value = 1
        self.RightProgress.value = 1
        self.RebornFx:LoadFx("fx/ui/prefab/UI_Guanning_fuhuo.prefab")
        self.m_CloseWndTick = CTickMgr.Register(DelegateFactory.Action(function()
            Gac2Gas.RequestGnjcFastReborn()
        end), 2000, ETickType.Once)
    end
    self.ProgressLabel.text = math.floor(self.LeftProgress.value * 100)
end

function LuaGuanNingResurrectionWnd:RefreshClickHint()
    self:CancelClickHintTick()
    self.m_ClickHintTick = CTickMgr.Register(DelegateFactory.Action(function()
        g_MessageMgr:ShowMessage("GuanNing_Resurrection_Tip")
        self:RefreshClickHint()
    end), 5000, ETickType.Once)
end

function LuaGuanNingResurrectionWnd:CancelClickHintTick()
    if self.m_ClickHintTick then
        invoke(self.m_ClickHintTick)
        self.m_ClickHintTick = nil
    end
end

function LuaGuanNingResurrectionWnd:CancelAutoRiseTick()
    if self.m_AutoRiseTick then
        invoke(self.m_AutoRiseTick)
        self.m_AutoRiseTick = nil
    end
end

function LuaGuanNingResurrectionWnd:CancelCloseWndTick()
    if self.m_CloseWndTick then
        invoke(self.m_CloseWndTick)
        self.m_CloseWndTick = nil
    end
end
