local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UIInput = import "UIInput"
local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CJingLingMgr = import "L10.Game.CJingLingMgr"

LuaZongMenNewSearchWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZongMenNewSearchWnd, "ReadMeButton", "ReadMeButton", GameObject)
RegistChildComponent(LuaZongMenNewSearchWnd, "Input", "Input", UIInput)
RegistChildComponent(LuaZongMenNewSearchWnd, "SearchButton", "SearchButton", GameObject)
RegistChildComponent(LuaZongMenNewSearchWnd, "QnTableView", "QnTableView", QnTableView)
RegistChildComponent(LuaZongMenNewSearchWnd, "ChatButton", "ChatButton", GameObject)
RegistChildComponent(LuaZongMenNewSearchWnd, "JoinButton", "JoinButton", GameObject)
RegistChildComponent(LuaZongMenNewSearchWnd, "ApplyForButton", "ApplyForButton", GameObject)
RegistChildComponent(LuaZongMenNewSearchWnd, "RuleLabel", "RuleLabel", UILabel)
RegistChildComponent(LuaZongMenNewSearchWnd, "WuXingLabel", "WuXingLabel", UILabel)
RegistChildComponent(LuaZongMenNewSearchWnd, "LeaderNameLabel", "LeaderNameLabel", UILabel)
RegistChildComponent(LuaZongMenNewSearchWnd, "NoneWuXingLabel", "NoneWuXingLabel", GameObject)
RegistChildComponent(LuaZongMenNewSearchWnd, "NoneLeaderNameLabel", "NoneLeaderNameLabel", GameObject)
RegistChildComponent(LuaZongMenNewSearchWnd, "NoneRuleLabel", "NoneRuleLabel", GameObject)
RegistChildComponent(LuaZongMenNewSearchWnd, "NeedCheckLabel", "NeedCheckLabel", GameObject)
RegistChildComponent(LuaZongMenNewSearchWnd, "NotNeedCheckLabel", "NotNeedCheckLabel", GameObject)
RegistChildComponent(LuaZongMenNewSearchWnd, "ShowJoinStandardButton", "ShowJoinStandardButton", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaZongMenNewSearchWnd, "m_RecommendList")
RegistClassMember(LuaZongMenNewSearchWnd, "m_SelectIndex")

function LuaZongMenNewSearchWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    if CommonDefs.IS_VN_CLIENT then
        self.ReadMeButton.gameObject:SetActive(false)
    end
	
	UIEventListener.Get(self.ReadMeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReadMeButtonClick()
	end)


	
	UIEventListener.Get(self.SearchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSearchButtonClick()
	end)


	
	UIEventListener.Get(self.ChatButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChatButtonClick()
	end)


	
	UIEventListener.Get(self.JoinButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJoinButtonClick()
	end)


	
	UIEventListener.Get(self.ApplyForButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnApplyForButtonClick()
	end)



    UIEventListener.Get(self.NeedCheckLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:ShowJoinStandard()
	end)



    UIEventListener.Get(self.ShowJoinStandardButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:ShowJoinStandard()
	end)
    --@endregion EventBind end
end

function LuaZongMenNewSearchWnd:Init()
	self.NoneRuleLabel.gameObject:SetActive(true)
	self.NoneLeaderNameLabel.gameObject:SetActive(true)
	self.NoneWuXingLabel.gameObject:SetActive(true)
	self.RuleLabel.gameObject:SetActive(false)
	self.LeaderNameLabel.gameObject:SetActive(false)
	self.WuXingLabel.gameObject:SetActive(false)
    self.NeedCheckLabel.gameObject:SetActive(false)
    self.NotNeedCheckLabel.gameObject:SetActive(false)
	self.QnTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self.m_RecommendList and #self.m_RecommendList or 0
        end,
        function(item, index)
            self:ItemAt(item,index)
        end)
    self.QnTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
	if CClientMainPlayer.Inst then
        Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId,"recommend",0)
    end
end

function LuaZongMenNewSearchWnd:ItemAt(item,index)
    if not self.m_RecommendList then return end
    local data = self.m_RecommendList[index + 1]

	local labelID = item.transform:Find("LabelID"):GetComponent(typeof(UILabel))
	local labelName = item.transform:Find("LabelName"):GetComponent(typeof(UILabel))
	local labelNum = item.transform:Find("LabelNum"):GetComponent(typeof(UILabel))
	local labelLiveness = item.transform:Find("LabelLiveness"):GetComponent(typeof(UILabel))
	local labelMiShu = item.transform:Find("LabelMiShu"):GetComponent(typeof(UILabel))
	local levelSprite = item.transform:Find("LevelSprite"):GetComponent(typeof(UISprite))
	local zheng = item.transform:Find("Zheng")
	local xie = item.transform:Find("Xie")

	labelID.text = data.SectId
	labelName.text = data.Name
	labelNum.text = data.MemberCount
	labelMiShu.text = data.MishuLevel --
    labelLiveness.text = data.Activity
    levelSprite.spriteName =  SafeStringFormat3("guildwnd_grade_0%d", data.Level == 0 and 1 or data.Level)
    zheng.gameObject:SetActive(data.IsXiePai == 0)
    xie.gameObject:SetActive(data.IsXiePai > 0)

	item.transform:GetComponent(typeof(QnButton)):SetBackgroundTexture(index % 2 == 0 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
end

function LuaZongMenNewSearchWnd:OnSelectAtRow(row)
    self.m_SelectIndex = row
	self.NoneRuleLabel.gameObject:SetActive(false)
	self.NoneLeaderNameLabel.gameObject:SetActive(false)
	self.NoneWuXingLabel.gameObject:SetActive(false)
	self.RuleLabel.gameObject:SetActive(true)
	self.LeaderNameLabel.gameObject:SetActive(true)
	self.WuXingLabel.gameObject:SetActive(true)

	local data = self.m_RecommendList[self.m_SelectIndex + 1]
	local wuxing = SoulCore_WuXing.GetData(data.FaZhenWuXing ~= 0 and data.FaZhenWuXing or 1)
    local curName = wuxing.Name 
    local downName =  SoulCore_WuXing.GetData(data.AvoidWuxing ~= 0 and data.AvoidWuxing or 1).Name
    local wuXingText = CUICommonDef.TranslateToNGUIText(self:GetWuXingColorWord(SafeStringFormat3(LocalString.GetString("当前:%s"),curName)))
    local avoidWuXingText = CUICommonDef.TranslateToNGUIText(self:GetWuXingColorWord(SafeStringFormat3(LocalString.GetString("规避:%s"),downName)))
	self.WuXingLabel.text = (data.FaZhenWuXing ~= 0 and wuXingText or "") .. (data.AvoidWuxing ~= 0 and avoidWuXingText or "")
    Gac2Gas.QuerySectMission_Ite(data.SectId)
    local leaderWuXing = SoulCore_WuXing.GetData(data.LeaderWuXing ~= 0 and data.LeaderWuXing or 1).Name
    self.LeaderNameLabel.text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3(LocalString.GetString("%s(%s[FFFFFF])"),data.LeaderName,self:GetWuXingColorWord(leaderWuXing)))
    self.NeedCheckLabel.gameObject:SetActive(data.CheckJoinStd > 0)
    self.NotNeedCheckLabel.gameObject:SetActive(data.CheckJoinStd == 0)
end

function LuaZongMenNewSearchWnd:GetWuXingColorWord(str)
    local colorDict = {
        [LocalString.GetString("金")] = LocalString.GetString("[ffc800]金"),
        [LocalString.GetString("木")] = LocalString.GetString("[04fa21]木"),
        [LocalString.GetString("水")] = LocalString.GetString("[64ffef]水"),
        [LocalString.GetString("火")] = LocalString.GetString("[ff0024]火"),
        [LocalString.GetString("土")] = LocalString.GetString("[bfb794]土")
    }
    local word = str
    for key, val in pairs(colorDict) do
        word = word:gsub(key, val)
    end
    return word 
end


function LuaZongMenNewSearchWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSyncSectMission_Ite", self, "OnSyncSectMission_Ite")
    g_ScriptEvent:AddListener("OnZongMenSearchResult", self, "OnZongMenSearchResult")
    g_ScriptEvent:AddListener("OnSectRecommendInfoResult", self, "OnSectRecommendInfoResult")
end

function LuaZongMenNewSearchWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncSectMission_Ite", self, "OnSyncSectMission_Ite")
    g_ScriptEvent:RemoveListener("OnZongMenSearchResult", self, "OnZongMenSearchResult")
    g_ScriptEvent:RemoveListener("OnSectRecommendInfoResult", self, "OnSectRecommendInfoResult")
end

function LuaZongMenNewSearchWnd:OnSyncSectMission_Ite(sectId, missionStr)
    local data = self.m_RecommendList[self.m_SelectIndex + 1]
    if data.SectId == sectId then
        self.RuleLabel.text = missionStr
    end
end

function LuaZongMenNewSearchWnd:OnSectRecommendInfoResult(data)
    self.m_RecommendList = data
	table.sort(self.m_RecommendList, function (a, b)
        if a.Level == b.Level then
            if a.MishuLevel == b.MishuLevel then
                return a.SectId < b.SectId
            else
                return a.MishuLevel > b.MishuLevel
            end
        else
            return a.Level > b.Level
        end
    end)
	self.QnTableView:ReloadData(true, true)
end

function LuaZongMenNewSearchWnd:OnZongMenSearchResult(data)
	self.m_RecommendList = {}
	local t = { 
		FaZhenWuXing = data.FaZhenWuXing,AvoidWuxing = data.AvoidWuxing, SectId = data.Id,
		Name = data.Name.StringData, MemberCount = data.DiziCount, MishuLevel = data.MiShuLevel, 
        IsXiePai= data.IsXiePai,Activity = data.Activity,Level = data.Level,LeaderName = data.LeaderName,
        JoinStandard = data.JoinStandard,CheckJoinStd = data.CheckJoinStd
    }
	table.insert(self.m_RecommendList,t)
	self.QnTableView:ReloadData(true, true)
	self.QnTableView:SetSelectRow(0, true)
end
--@region UIEvent

function LuaZongMenNewSearchWnd:OnReadMeButtonClick()
	local textArray = {LocalString.GetString("查看宗派介绍"),LocalString.GetString("如何加入宗派"),LocalString.GetString("如何创建宗派"),LocalString.GetString("了解灵核系统")}
    local t = {}
    for k,text in pairs(textArray) do
        local item  = PopupMenuItemData(text,DelegateFactory.Action_int(function (idx)
            CJingLingMgr.Inst:ShowJingLingWnd(text, "o_push", true, false, nil, false)
        end),false,nil)
        table.insert(t, item)
    end
    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, self.ReadMeButton.transform, AlignType.Top,1,DelegateFactory.Action(function()
       
    end),600,true,296)
end

function LuaZongMenNewSearchWnd:OnSearchButtonClick()
	local str = tostring(self.Input.value)
    if System.String.IsNullOrEmpty(str) then 
        Gac2Gas.QueryToJoinSectInfo(0)
        return 
    end
    Gac2Gas.QueryToJoinSectInfo(tonumber(str))
end

function LuaZongMenNewSearchWnd:OnChatButtonClick()
	if not self.m_SelectIndex then 
        g_MessageMgr:ShowMessage("NoneSelect_SearchRecommendSect")
        return 
    end
    local data = self.m_RecommendList[self.m_SelectIndex + 1]
    Gac2Gas.RequestContactSectManager(data.SectId)
end

function LuaZongMenNewSearchWnd:OnJoinButtonClick()
	if not self.m_SelectIndex then 
        g_MessageMgr:ShowMessage("NoneSelect_JoinSect")
        return 
    end
	local data = self.m_RecommendList[self.m_SelectIndex + 1]
    local msg =  g_MessageMgr:FormatMessage("Join_ZongMen_Confirm", data.Name)
    MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
        --LuaZongMenMgr:JoinZongMen(data.SectId, false, nil, false)
        Gac2Gas.RequestApplySect_Ite(data.SectId)
    end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
end

function LuaZongMenNewSearchWnd:OnApplyForButtonClick()
    Gac2Gas.RequestApplyAllSect_Ite()
end

function LuaZongMenNewSearchWnd:ShowJoinStandard()
    if not self.m_SelectIndex then 
        g_MessageMgr:ShowMessage("NoneSelect_JoinSect")
        return 
    end
	local data = self.m_RecommendList[self.m_SelectIndex + 1]
    LuaZongMenMgr.m_GuiZe = data.JoinStandard
    if LuaZongMenMgr.m_GuiZe then
        CUIManager.ShowUI(CLuaUIResources.ZongMenJoinStandardSearchResultWnd)
    end
end
--@endregion UIEvent

