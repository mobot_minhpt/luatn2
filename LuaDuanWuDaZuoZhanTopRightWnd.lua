require("common/common_include")
local CTopAndRightTipWnd=import "L10.UI.CTopAndRightTipWnd"
local Time=import "UnityEngine.Time"

CLuaDuanWuDaZuoZhanTopRightWnd=class()
RegistClassMember(CLuaDuanWuDaZuoZhanTopRightWnd,"m_ExpandButton")
RegistClassMember(CLuaDuanWuDaZuoZhanTopRightWnd,"m_ScoreLabel")
RegistClassMember(CLuaDuanWuDaZuoZhanTopRightWnd,"m_TimeLabel")
-- RegistClassMember(CLuaDuanWuDaZuoZhanTopRightWnd,"m_CountdownCtl")
-- RegistClassMember(CLuaDuanWuDaZuoZhanTopRightWnd,"m_PlayTime")


function CLuaDuanWuDaZuoZhanTopRightWnd:Init()
    -- self.m_PlayTime=0
    self.m_ExpandButton=FindChild(self.transform,"ExpandButton").gameObject
    self.m_ScoreLabel=FindChild(self.transform,"ScoreLabel"):GetComponent(typeof(UILabel))
    self.m_TimeLabel=FindChild(self.transform,"TimeLabel"):GetComponent(typeof(UILabel))
    self.m_ScoreLabel.text=tostring(CLuaDuanWuDaZuoZhanMgr.m_PlayScore)
    self.m_TimeLabel.text=nil

    UIEventListener.Get(self.m_ExpandButton).onClick=LuaUtils.VoidDelegate(function(go)
        LuaUtils.SetLocalRotation(self.m_ExpandButton.transform,0,0,0)
        CTopAndRightTipWnd.showPackage = false
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
    
end
function CLuaDuanWuDaZuoZhanTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("DuanWuDZZ_SendPlayerPlayInfoToClient", self, "OnDuanWuDZZ_SendPlayerPlayInfoToClient")
    
end
function CLuaDuanWuDaZuoZhanTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("DuanWuDZZ_SendPlayerPlayInfoToClient", self, "OnDuanWuDZZ_SendPlayerPlayInfoToClient")

    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end
end

function CLuaDuanWuDaZuoZhanTopRightWnd:OnHideTopAndRightTipWnd()
    self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

function CLuaDuanWuDaZuoZhanTopRightWnd:OnDuanWuDZZ_SendPlayerPlayInfoToClient(playerScore,playTime)
    self.m_ScoreLabel.text=tostring(playerScore)
    -- self.m_PlayTime=playTime
end
function CLuaDuanWuDaZuoZhanTopRightWnd:FormatTime(time)
    local minute=math.floor(time/60)
    local second=time%60
    return SafeStringFormat3("%02d:%02d",minute,second)
end
function CLuaDuanWuDaZuoZhanTopRightWnd:Update()
    -- if self.m_TimeLabel then
        CLuaDuanWuDaZuoZhanMgr.m_PlayTime=CLuaDuanWuDaZuoZhanMgr.m_PlayTime+Time.unscaledDeltaTime
        self.m_TimeLabel.text=self:FormatTime(CLuaDuanWuDaZuoZhanMgr.m_PlayTime)
    -- end
end
return CLuaDuanWuDaZuoZhanTopRightWnd
