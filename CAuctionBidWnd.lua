-- Auto Generated!!
local CAuctionBidWnd = import "L10.UI.CAuctionBidWnd"
local CAuctionItemInfo = import "L10.Game.CAuctionItemInfo"
local CAuctionMgr = import "L10.Game.CAuctionMgr"
local CItem = import "L10.Game.CItem"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local QualityColor = import "L10.Game.QualityColor"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CAuctionBidWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.buyButton).onClick = MakeDelegateFromCSFunction(this.onBuyButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.bidButton).onClick = MakeDelegateFromCSFunction(this.onBidButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.onCloseButtonClick, VoidDelegate, this)
    this.increaseDecreaseButton.onValueChanged = MakeDelegateFromCSFunction(this.onValueChange, MakeGenericClass(Action1, UInt32), this)
    this.itemInfo = CAuctionMgr.Inst:GetCurrentItemInfo(CAuctionMgr.Inst.FocusInstanceID)
    if not this.itemInfo then 
        CUIManager.CloseUI(CUIResources.AuctionBidWnd)
        return
    end
    this.item = CAuctionMgr.Inst:GetItem(this.itemInfo.InstanceID)
    if this.item ~= nil then
        this.iconContainer:LoadMaterial(this.item.Icon)
        this.nameLabel.text = this.item.Name
        if this.item.IsItem then
            this.nameLabel.color = this.item.Item.Color
        elseif this.item.IsEquip then
            this.nameLabel.color = this.item.Equip.DisplayColor
        end
    else
        local itemTemplate = Item_Item.GetData(this.itemInfo.TemplateID)
        this.iconContainer:LoadMaterial(itemTemplate.Icon)
        this.nameLabel.text = itemTemplate.Name
        this.nameLabel.color = QualityColor.GetRGBValue(CItem.GetQualityType(this.itemInfo.TemplateID))
    end
    if this.itemInfo.FixPrice > 0 then
        this.fixpriceRoot.gameObject:SetActive(true)
        this.fixPriceLabel.text = tostring(this.itemInfo.FixPrice)
        this.buyButton:SetActive(true)
    else
        this.fixpriceRoot.gameObject:SetActive(false)
        local pos = this.bidButton.transform.localPosition
        pos = Vector3(0, pos.y, pos.z)
        this.bidButton.transform.localPosition = pos
        this.buyButton:SetActive(false)
    end
    if this.itemInfo.Count > 1 then
        this.countLabel.text = tostring(this.itemInfo.Count)
    else
        this.countLabel.text = ""
    end
    this.currentPriceLabel.text = tostring(this.itemInfo.Price)
    this.originPriceLabel.text = tostring(this.itemInfo.StartPrice)
    this.selfPriceLabel.text = tostring(this.itemInfo.SelfPrice)
    local step = math.floor((this.itemInfo.StartPrice * 0.05)) + 1
    local max = this.itemInfo.Price + step * 4
    if this.itemInfo.FixPrice > 0 then
        max = this.itemInfo.FixPrice
    end
    this.increaseDecreaseButton:SetMinMax((this.itemInfo.Price + step), max, step)
    this.increaseDecreaseButton:SetValue((this.itemInfo.Price + step), true)
end
CAuctionBidWnd.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.OnAuctionItemPriceRefresh, MakeDelegateFromCSFunction(this.onPriceChange, MakeGenericClass(Action3, String, UInt32, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.OnAuctionBuySuccess, MakeDelegateFromCSFunction(this.onBuySuccess, MakeGenericClass(Action1, String), this))
end
CAuctionBidWnd.m_OnDisable_CS2LuaHook = function (this) 

    EventManager.RemoveListenerInternal(EnumEventType.OnAuctionItemPriceRefresh, MakeDelegateFromCSFunction(this.onPriceChange, MakeGenericClass(Action3, String, UInt32, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnAuctionBuySuccess, MakeDelegateFromCSFunction(this.onBuySuccess, MakeGenericClass(Action1, String), this))
end
CAuctionBidWnd.m_onPriceChange_CS2LuaHook = function (this, id, newPrice, ownPrice) 
    if id ~= this.itemInfo.AuctionID then
        return
    end
    this.currentPriceLabel.text = tostring(newPrice)
    this.selfPriceLabel.text = tostring(ownPrice)
    local step = math.floor((this.itemInfo.StartPrice * 0.05)) + 1
    if this.increaseDecreaseButton:GetValue() < (newPrice + step) then
        local max = this.itemInfo.Price + step * 4
        if this.itemInfo.AuctionType == CAuctionItemInfo.EnumItemAuctionType.Guild then
            max = this.itemInfo.FixPrice
        end
        this.increaseDecreaseButton:SetMinMax((this.itemInfo.Price + step), max, step)
        this.increaseDecreaseButton:SetValue((this.itemInfo.Price + step), true)
    end
end
CAuctionBidWnd.m_onBuySuccess_CS2LuaHook = function (this, auctionID) 
    if this.itemInfo.AuctionID == auctionID then
        CUIManager.CloseUI(CUIResources.AuctionBidWnd)
    end
end
CAuctionBidWnd.m_onBuyButtonClick_CS2LuaHook = function (this, go) 
    this.increaseDecreaseButton:SetValue((this.itemInfo.FixPrice), true)
    local msg = g_MessageMgr:FormatMessage("Paimai_Oneshoot_Confirm", this.increaseDecreaseButton:GetValue(), this.itemInfo.Name)
    MessageWndManager.ShowOKCancelMessage(msg, MakeDelegateFromCSFunction(this.confirmBuy, Action0, this), nil, nil, nil, false)
end
CAuctionBidWnd.m_onValueChange_CS2LuaHook = function (this, v) 
    if this.itemInfo == nil then
        return
    end
    local z = (v - this.itemInfo.Price)
    if z > 0 then
        local percentage = math.floor((z * 100 / (this.itemInfo.StartPrice)))
        this.addPriceLabel.text = System.String.Format(LocalString.GetString("加价{0}%"), percentage)
    end
end
