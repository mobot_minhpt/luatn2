-- Auto Generated!!
local CLingShouPinZhi = import "L10.UI.CLingShouPinZhi"
local EnumLingShouQuality = import "L10.Game.EnumLingShouQuality"
CLingShouPinZhi.m_Init_EnumLingShouQuality_CS2LuaHook = function (this, quality) 
    repeat
        local default = quality
        if default == EnumLingShouQuality.Jia then
            this.sprite.spriteName = CLingShouPinZhi.PINZHI_JIA
            break
        elseif default == EnumLingShouQuality.Yi then
            this.sprite.spriteName = CLingShouPinZhi.PINZHI_YI
            break
        elseif default == EnumLingShouQuality.Bing then
            this.sprite.spriteName = CLingShouPinZhi.PINZHI_BING
            break
        elseif default == EnumLingShouQuality.Ding then
            this.sprite.spriteName = CLingShouPinZhi.PINZHI_DING
            break
        elseif default == EnumLingShouQuality.Wu then
            this.sprite.spriteName = CLingShouPinZhi.PINZHI_WU
            break
        elseif default == EnumLingShouQuality.Ji then
            this.sprite.spriteName = CLingShouPinZhi.PINZHI_JI
            break
        end
    until 1
end
