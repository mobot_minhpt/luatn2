local Color = import "UnityEngine.Color"
local UILabel = import "UILabel"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CUITexture = import "L10.UI.CUITexture"

CLuaQYXTSignUpWnd = class()

RegistChildComponent(CLuaQYXTSignUpWnd, "TipButton", GameObject)
RegistChildComponent(CLuaQYXTSignUpWnd, "RestCountLabel", UILabel)
RegistChildComponent(CLuaQYXTSignUpWnd, "MatchBtn", GameObject)
RegistChildComponent(CLuaQYXTSignUpWnd, "CancelRoot", GameObject)
RegistChildComponent(CLuaQYXTSignUpWnd, "CancelBtn", GameObject)
RegistChildComponent(CLuaQYXTSignUpWnd, "DescLabel", UILabel)
RegistChildComponent(CLuaQYXTSignUpWnd, "SkillIcon1", CUITexture)
RegistChildComponent(CLuaQYXTSignUpWnd, "SkillIcon2", CUITexture)

RegistClassMember(CLuaQYXTSignUpWnd, "m_SatgeTabs")
RegistClassMember(CLuaQYXTSignUpWnd, "m_SelectStage")

function CLuaQYXTSignUpWnd:Awake()
    self.m_StageTabs = {
        self.StageTab1,
        self.StageTab2
    }

    UIEventListener.Get(self.MatchBtn).onClick  =DelegateFactory.VoidDelegate(function(go)
        self:OnMatchBtnClick()
    end)

    UIEventListener.Get(self.CancelBtn).onClick  =DelegateFactory.VoidDelegate(function(go)
        self:OnCancelBtnClick()
    end)

    UIEventListener.Get(self.TipButton).onClick  =DelegateFactory.VoidDelegate(function(go)
        --@策划配表
        g_MessageMgr:ShowMessage("Valentine2020_QYXT_TIP")
    end)

    UIEventListener.Get(self.SkillIcon1.gameObject).onClick  =DelegateFactory.VoidDelegate(function(go)
        local skillId = Valentine2020_Setting.GetData().TeleportSkillId
        CSkillInfoMgr.ShowSkillInfoWnd(skillId, true, 0, 0, nil)
    end)

    UIEventListener.Get(self.SkillIcon2.gameObject).onClick  =DelegateFactory.VoidDelegate(function(go)
        local skillId = Valentine2020_Setting.GetData().EmbraceSkillId
        CSkillInfoMgr.ShowSkillInfoWnd(skillId, true, 0, 0, nil)
    end)

    self.DescLabel.text = Valentine2020_Setting.GetData().QYXTDescription
end

function CLuaQYXTSignUpWnd:Init()
    self:InitSkillIcons()
    --查询报名状态
    if CLuaQYXTMgr.SignUpWndNotShow == true then
        CLuaQYXTMgr.SignUpWndNotShow = false
        self:OnRefreshQYXTSignUpWnd(CLuaQYXTMgr.signUpStatus,CLuaQYXTMgr.remainTimes)
    end
end

function CLuaQYXTSignUpWnd:InitSkillIcons()
    local skillId1 = Valentine2020_Setting.GetData().TeleportSkillId
    local skillId2 = Valentine2020_Setting.GetData().EmbraceSkillId
    local skillData1 = Skill_AllSkills.GetData(skillId1)
    local skillData2 = Skill_AllSkills.GetData(skillId2)

    self.SkillIcon1:LoadMaterial(SafeStringFormat3("UI/Texture/Skill/Material/%s.mat",skillData1.SkillIcon))
    self.SkillIcon2:LoadMaterial(SafeStringFormat3("UI/Texture/Skill/Material/%s.mat",skillData2.SkillIcon))
end

function CLuaQYXTSignUpWnd:OnMatchBtnClick()
    Gac2Gas.RequestSignUpQingYiXiangTongPlay()
end

function CLuaQYXTSignUpWnd:OnCancelBtnClick()
    Gac2Gas.CancelQingYiXiangTongPlaySignUp()
end

function CLuaQYXTSignUpWnd:OnRefreshQYXTSignUpWnd(status,num)

    if status then
        self.CancelRoot:SetActive(true)
        self.MatchBtn:SetActive(false)
    else
        self.CancelRoot:SetActive(false)
        self.MatchBtn:SetActive(true)
    end
    
    self.RestCountLabel.text = num
    if num <=0 then
        self.RestCountLabel.color = Color.red
    end
end

function CLuaQYXTSignUpWnd:OnEnable()
    g_ScriptEvent:AddListener("RefreshQYXTSignUpWnd",self,"OnRefreshQYXTSignUpWnd")
end

function CLuaQYXTSignUpWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RefreshQYXTSignUpWnd",self,"OnRefreshQYXTSignUpWnd")
end


