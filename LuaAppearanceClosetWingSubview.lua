local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local EnumGender = import "L10.Game.EnumGender"
local UISprite = import "UISprite"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaAppearanceClosetWingSubview = class()

RegistChildComponent(LuaAppearanceClosetWingSubview,"m_ClosetWingItem", "CommonClosetItemCell", GameObject)
RegistChildComponent(LuaAppearanceClosetWingSubview,"m_SwitchButton", "CommonHeaderSwitch", CButton)
RegistChildComponent(LuaAppearanceClosetWingSubview,"m_ItemDisplay", "AppearanceCommonButtonDisplayView", CCommonLuaScript)
RegistChildComponent(LuaAppearanceClosetWingSubview,"m_ContentTable", "ContentTable", UITable)
RegistChildComponent(LuaAppearanceClosetWingSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)

RegistClassMember(LuaAppearanceClosetWingSubview, "m_MyWings")
RegistClassMember(LuaAppearanceClosetWingSubview, "m_SelectedDataId")

function LuaAppearanceClosetWingSubview:Awake()
end

function LuaAppearanceClosetWingSubview:Init()
    --组件共用，每次需要重新关联响应方法
    self.m_SwitchButton.gameObject:SetActive(true)
    self.m_SwitchButton.Text = LocalString.GetString("翅膀开关")
    UIEventListener.Get(self.m_SwitchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnSwitchButtonClick() end)

    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceClosetWingSubview:LoadData()
    self.m_MyWings = LuaAppearancePreviewMgr:GetAllWingInfo(true)
    Extensions.RemoveAllChildren(self.m_ContentTable.transform)
    self.m_ContentTable.gameObject:SetActive(true)

    for i = 1, # self.m_MyWings do
        local child = CUICommonDef.AddChild(self.m_ContentTable.gameObject, self.m_ClosetWingItem)
        child:SetActive(true)
        self:InitItem(child, self.m_MyWings[i])
    end
    self.m_ContentTable:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceClosetWingSubview:SetDefaultSelection()
    self.m_SelectedDataId = LuaAppearancePreviewMgr:IsShowWing() and LuaAppearancePreviewMgr:GetCurrentInUseWing() or 0
end

function LuaAppearanceClosetWingSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end

    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        local appearanceData = self.m_MyWings[i+1]
        if appearanceData.id == self.m_SelectedDataId then
            self:OnItemClick(childGo)
            break
        end
    end
end

function LuaAppearanceClosetWingSubview:InitItem(itemGo, appearanceData)
    local iconTexture = itemGo.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local cornerGo = itemGo.transform:Find("Item/Corner").gameObject
    local nameLabel = itemGo.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local conditionLabel = itemGo.transform:Find("ConditionLabel"):GetComponent(typeof(UILabel))
    if CClientMainPlayer.Inst then
        iconTexture:LoadMaterial(CClientMainPlayer.Inst.Gender == EnumGender.Male and appearanceData.icon or appearanceData.femaleIcon)
    else
        iconTexture:Clear()
    end
    itemGo:GetComponent(typeof(CButton)).Selected = (self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.wingId or false)
    cornerGo:SetActive(LuaAppearancePreviewMgr:IsCurrentInUseWing(appearanceData.id))
    nameLabel.text = appearanceData.colorName
    conditionLabel.text = appearanceData.condition

    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go)
    end)
end

function LuaAppearanceClosetWingSubview:OnItemClick(go)
    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:GetComponent(typeof(CButton)).Selected = true
            self.m_SelectedDataId = self.m_MyWings[i+1].id
            if not LuaAppearancePreviewMgr:IsShowWing() then
                g_MessageMgr:ShowMessage("Appearance_Preview_Hide_Wing_Tip")
            end
        else
            childGo.transform:GetComponent(typeof(CButton)).Selected = false
        end
    end
    self:UpdateButtonsDisplay()
    g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerWing", self.m_SelectedDataId)
end

function LuaAppearanceClosetWingSubview:UpdateButtonsDisplay()
    self.m_SwitchButton.Selected = LuaAppearancePreviewMgr:IsShowWing()
    if self.m_SelectedDataId == 0 then
        self.m_ItemDisplay.gameObject:SetActive(false)
        return
    end

    self.m_ItemDisplay.gameObject:SetActive(true)
    local buttonTbl = {}
    local exist = self.m_SelectedDataId and LuaAppearancePreviewMgr:MainPlayerHasWing(self.m_SelectedDataId) or false
    local inUse = self.m_SelectedDataId and LuaAppearancePreviewMgr:IsCurrentInUseWing(self.m_SelectedDataId) or false
    if exist and not inUse then
        table.insert(buttonTbl, {text=LocalString.GetString("更换"), isYellow=false, action=function(go) self:OnApplyButtonClick() end})
    elseif exist and inUse then
        table.insert(buttonTbl, {text=LocalString.GetString("脱下"), isYellow=false, action=function(go) self:OnRemoveButtonClick() end})
    end

    self.m_ItemDisplay:Init(buttonTbl)
end

function LuaAppearanceClosetWingSubview:OnApplyButtonClick()
    if self.m_SelectedDataId and self.m_SelectedDataId>0 then
        LuaAppearancePreviewMgr:RequestSetWing(self.m_SelectedDataId)
    end
end

function LuaAppearanceClosetWingSubview:OnRemoveButtonClick()
    LuaAppearancePreviewMgr:RequestSetWing(0)
end

function LuaAppearanceClosetWingSubview:OnSwitchButtonClick()
    self.m_SwitchButton.Selected = not self.m_SwitchButton.Selected
    local bShow = LuaAppearancePreviewMgr:IsShowWing()
    LuaAppearancePreviewMgr:ChangeWingVisibility(not bShow) --等等请求返回再刷新
end

function  LuaAppearanceClosetWingSubview:OnEnable()
    g_ScriptEvent:AddListener("WingFxUpdate", self, "OnWingFxUpdate")
    g_ScriptEvent:AddListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:AddListener("AppearancePropertySettingInfoReturn",self,"OnAppearancePropertySettingInfoReturn") --开关切换
end

function  LuaAppearanceClosetWingSubview:OnDisable()
    g_ScriptEvent:RemoveListener("WingFxUpdate", self, "OnWingFxUpdate")
    g_ScriptEvent:RemoveListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:RemoveListener("AppearancePropertySettingInfoReturn",self,"OnAppearancePropertySettingInfoReturn")
end

--翅膀切换
function LuaAppearanceClosetWingSubview:OnWingFxUpdate(args)
    local engineId = args[0]
    local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if TypeIs(obj, typeof(CClientMainPlayer)) then
        self:SetDefaultSelection()
		self:LoadData()
	end
end
--仙凡切换
function LuaAppearanceClosetWingSubview:SyncMainPlayerAppearancePropUpdate()
    self:InitSelection()
end
--开关切换
function LuaAppearanceClosetWingSubview:OnAppearancePropertySettingInfoReturn(args)
    self:SetDefaultSelection()
    self:InitSelection()
end
