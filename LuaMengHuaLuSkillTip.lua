require("common/common_include")

local UIScrollView=import "UIScrollView"
local UIWidget = import "UIWidget"
local UITable = import "UITable"
local UIRoot=import "UIRoot"
local Screen=import "UnityEngine.Screen"
local NGUIMath=import "NGUIMath"
local Mathf = import "UnityEngine.Mathf"
local Extensions = import "Extensions"
local MengHuaLu_Skill = import "L10.Game.MengHuaLu_Skill"

LuaMengHuaLuSkillTip=class()

RegistChildComponent(LuaMengHuaLuSkillTip, "SkillNameLabel", UILabel)
RegistChildComponent(LuaMengHuaLuSkillTip, "Background", UIWidget)
RegistChildComponent(LuaMengHuaLuSkillTip, "Table", UITable)
RegistChildComponent(LuaMengHuaLuSkillTip, "ScrollView", UIScrollView)
RegistChildComponent(LuaMengHuaLuSkillTip, "Header", UIWidget)
RegistChildComponent(LuaMengHuaLuSkillTip, "InfoTemplate", GameObject)

RegistClassMember(LuaMengHuaLuSkillTip, "SelectedSkillId")

function LuaMengHuaLuSkillTip:Init()
    
    self.InfoTemplate:SetActive(false)
    self:UpdateSkillInfo()

    self.Table:Reposition()

    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenHeight = Screen.height * scale
    local contentTopPadding = Mathf.Abs(self.ScrollView.panel.topAnchor.absolute)
    local contentBottomPadding = Mathf.Abs(self.ScrollView.panel.bottomAnchor.absolute)
    local totalHeight = NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform).size.y + (self.Table.padding.y * 2) + contentTopPadding + contentBottomPadding + (self.ScrollView.panel.clipSoftness.y * 2)
    local displayWndHeight = Mathf.Clamp(totalHeight, contentTopPadding + contentBottomPadding + 80, virtualScreenHeight - 100)
    self.Background:GetComponent(typeof(UIWidget)).height = Mathf.CeilToInt(displayWndHeight)
    self.Header:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
    self.ScrollView.panel:ResetAndUpdateAnchors()
    self.ScrollView:ResetPosition()
end


function LuaMengHuaLuSkillTip:UpdateSkillInfo()
    if not LuaMengHuaLuMgr.m_SelectedSkillInfo then 
        CUIManager.CloseUI(CLuaUIResources.MengHuaLuSkillTip)
        return
    end

    Extensions.RemoveAllChildren(self.Table.transform)

    local skill = MengHuaLu_Skill.GetData(LuaMengHuaLuMgr.m_SelectedSkillInfo.skillId)
    self.SkillNameLabel.text = skill.Name

    -- 花费法力
    for i = 1, 2 do
        local go = CUICommonDef.AddChild(self.Table.gameObject, self.InfoTemplate)
        self:InitSkillItem(go, i, skill, LuaMengHuaLuMgr.m_SelectedSkillInfo.comsume)
        go:SetActive(true)
    end
end

function LuaMengHuaLuSkillTip:InitSkillItem(go, index, skill, comsume)
    local label = go.transform:GetComponent(typeof(UILabel))
    if index == 1 then
        label.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]消耗法力[-] %s"), comsume)
    elseif index == 2 then
        label.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]技能描述[-] %s"), skill.Description)
    end
end

function LuaMengHuaLuSkillTip:Update()
    self:ClickThroughToClose()
end

function LuaMengHuaLuSkillTip:ClickThroughToClose()
    if Input.GetMouseButtonDown(0) then       
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform) and
            CUICommonDef.GetNearestPanelDepth(self.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
            CUIManager.CloseUI(CLuaUIResources.MengHuaLuSkillTip)
        end
    end
end

return LuaMengHuaLuSkillTip
