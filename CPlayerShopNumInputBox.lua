-- Auto Generated!!
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerShopNumberInputMgr = import "L10.UI.CPlayerShopNumberInputMgr"
local CPlayerShopNumInputBox = import "L10.UI.CPlayerShopNumInputBox"
local CUIManager = import "L10.UI.CUIManager"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CPlayerShopNumInputBox.m_Init_CS2LuaHook = function (this) 

    this.m_BuyCountButton:SetMinMax(CPlayerShopNumberInputMgr.MinVal, CPlayerShopNumberInputMgr.MaxVal, 1)
    this.m_BuyCountButton:SetValue(CPlayerShopNumberInputMgr.DefaultVal, true)

    this.m_ItemTexture:LoadMaterial(CPlayerShopNumberInputMgr.IconPath)
    local ownedNumLabel = this.transform:Find("Anchor/OwnedNumLabel")
    if ownedNumLabel then
        local hasCount = CLuaShopMallMgr.m_PlayerShopNumInputBoxOwnedCount
        ownedNumLabel:GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("已拥有:%d"),hasCount)
    end
end
CPlayerShopNumInputBox.m_OnOkButtonClick_CS2LuaHook = function (this, go) 
    if CPlayerShopNumberInputMgr.OnCompleteInput ~= nil then
        GenericDelegateInvoke(CPlayerShopNumberInputMgr.OnCompleteInput, Table2ArrayWithCount({this.m_BuyCountButton:GetValue()}, 1, MakeArrayClass(Object)))
    end
    this:Close()
end
CPlayerShopNumInputBox.m_OnEnable_CS2LuaHook = function (this) 
    this.m_BuyCountButton.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_BuyCountButton.onValueChanged, MakeDelegateFromCSFunction(this.OnBuyCountChanged, MakeGenericClass(Action1, UInt32), this), true)
    UIEventListener.Get(this.okButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.okButton).onClick, MakeDelegateFromCSFunction(this.OnOkButtonClick, VoidDelegate, this), true)
    if this.cancelButton ~= nil then
        UIEventListener.Get(this.cancelButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.cancelButton).onClick, MakeDelegateFromCSFunction(this.OnCancelButtonClick, VoidDelegate, this), true)
    end
end
CPlayerShopNumInputBox.m_OnDisable_CS2LuaHook = function (this) 
    this.m_BuyCountButton.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_BuyCountButton.onValueChanged, MakeDelegateFromCSFunction(this.OnBuyCountChanged, MakeGenericClass(Action1, UInt32), this), true)
    UIEventListener.Get(this.okButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.okButton).onClick, MakeDelegateFromCSFunction(this.OnOkButtonClick, VoidDelegate, this), false)
    if this.cancelButton ~= nil then
        UIEventListener.Get(this.cancelButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.cancelButton).onClick, MakeDelegateFromCSFunction(this.OnCancelButtonClick, VoidDelegate, this), false)
    end
    CLuaShopMallMgr.m_PlayerShopNumInputBoxOwnedCount = 0
end


CPlayerShopNumberInputMgr.m_ShowNumInputBox_CS2LuaHook = function (icon, singlePrice, minVal, maxVal, defalutVal, onCompleteInput) 

    CPlayerShopNumberInputMgr.iconPath = icon
    CPlayerShopNumberInputMgr.singlePrice = singlePrice
    CPlayerShopNumberInputMgr.minVal = minVal
    CPlayerShopNumberInputMgr.maxVal = maxVal
    CPlayerShopNumberInputMgr.onCompleteInput = onCompleteInput
    CPlayerShopNumberInputMgr.defalutVal = defalutVal
    CUIManager.ShowUI(CIndirectUIResources.PlayerShopNumberInputBox)
end
