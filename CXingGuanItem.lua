-- Auto Generated!!
local Byte = import "System.Byte"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CXingGuanItem = import "L10.UI.CXingGuanItem"
local CXingGuanWnd = import "L10.UI.CXingGuanWnd"
local LocalString = import "LocalString"
CXingGuanItem.m_Init_CS2LuaHook = function (this, con) 
    this.m_NameLabel.text = con.m_Name
    this.m_GroupNameLabel.text = (con.m_XingXiu .. LocalString.GetString("·")) .. con.m_TypeName
    this.m_CostLabel.text = tostring(con.m_StarList.Count)
    this.m_IconTex:LoadMaterial(System.String.Format(CXingGuanWnd.IMAGE_PATTERN, con.m_ImageName))
    this.m_Con = con
    if CXingGuanWnd.Inst:IsConstellationVisiable(con.m_Id) then
        this.m_InValidGo:SetActive(not CLuaConstellationWnd:IsPass(con))
    else 
        this.m_InValidGo:SetActive(false)
    end
    this:UpdateStarCount()
end
CXingGuanItem.m_UpdateStarCount_CS2LuaHook = function (this) 
    local staredCount = 0
    if CClientMainPlayer.Inst ~= nil and CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.XingguanProperty.XingguanData, typeof(Byte), this.m_Con.m_Id) then
        staredCount = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.XingguanProperty.XingguanData, typeof(Byte), this.m_Con.m_Id).StaredCount
    end
    this.m_CostLabel.text = (staredCount .. "/") .. tostring(this.m_Con.m_StarList.Count)
    this.m_LightedBg.gameObject:SetActive(staredCount == this.m_Con.m_StarList.Count)
    if CXingGuanWnd.Inst:IsConstellationVisiable(this.m_Con.m_Id) then
        this.m_InValidGo:SetActive(not CLuaConstellationWnd:IsPass(this.m_Con))
    else 
        this.m_InValidGo:SetActive(false)
    end
end
