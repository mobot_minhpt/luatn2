require("common/common_include")

local Gac2Gas=import "L10.Game.Gac2Gas"
local UILabel = import "UILabel"
local MsgPackImpl = import "MsgPackImpl"
local LocalString = import "LocalString"
local UISprite = import "UISprite"
local CUITexture = import "L10.UI.CUITexture"

CLuaWorldCupJueSaiView=class()

RegistClassMember(CLuaWorldCupJueSaiView,"m_NameLabelTbl")
RegistClassMember(CLuaWorldCupJueSaiView,"m_IconTextureTbl")
RegistClassMember(CLuaWorldCupJueSaiView,"m_Id2DefaultName")

RegistClassMember(CLuaWorldCupJueSaiView,"m_GameObjectTbl")
RegistClassMember(CLuaWorldCupJueSaiView,"m_WinnerFronTbl")

RegistClassMember(CLuaWorldCupJueSaiView,"m_ChampionTexture")
RegistClassMember(CLuaWorldCupJueSaiView,"m_ChampionLabel")
RegistClassMember(CLuaWorldCupJueSaiView,"m_ChampionLine1")
RegistClassMember(CLuaWorldCupJueSaiView,"m_ChampionLine2")

function CLuaWorldCupJueSaiView:Init(tf)

    self.m_NameLabelTbl={}
    self.m_NameLabelTbl["491"] = tf.transform:Find("MainArea/NameTbl1/C1"):GetComponent(typeof(UILabel))
    self.m_NameLabelTbl["492"] = tf.transform:Find("MainArea/NameTbl1/D2"):GetComponent(typeof(UILabel))
    self.m_NameLabelTbl["501"] = tf.transform:Find("MainArea/NameTbl1/A1"):GetComponent(typeof(UILabel))
    self.m_NameLabelTbl["502"] = tf.transform:Find("MainArea/NameTbl1/B2"):GetComponent(typeof(UILabel))
    self.m_NameLabelTbl["511"] = tf.transform:Find("MainArea/NameTbl2/B1"):GetComponent(typeof(UILabel))
    self.m_NameLabelTbl["512"] = tf.transform:Find("MainArea/NameTbl2/A2"):GetComponent(typeof(UILabel))
    self.m_NameLabelTbl["521"] = tf.transform:Find("MainArea/NameTbl2/D1"):GetComponent(typeof(UILabel))
    self.m_NameLabelTbl["522"] = tf.transform:Find("MainArea/NameTbl2/C2"):GetComponent(typeof(UILabel))
    self.m_NameLabelTbl["531"] = tf.transform:Find("MainArea/NameTbl1/E1"):GetComponent(typeof(UILabel))
    self.m_NameLabelTbl["532"] = tf.transform:Find("MainArea/NameTbl1/F2"):GetComponent(typeof(UILabel))
    self.m_NameLabelTbl["541"] = tf.transform:Find("MainArea/NameTbl1/G1"):GetComponent(typeof(UILabel))
    self.m_NameLabelTbl["542"] = tf.transform:Find("MainArea/NameTbl1/H2"):GetComponent(typeof(UILabel))
    self.m_NameLabelTbl["551"] = tf.transform:Find("MainArea/NameTbl2/F1"):GetComponent(typeof(UILabel))
    self.m_NameLabelTbl["552"] = tf.transform:Find("MainArea/NameTbl2/E2"):GetComponent(typeof(UILabel))
    self.m_NameLabelTbl["561"] = tf.transform:Find("MainArea/NameTbl2/H1"):GetComponent(typeof(UILabel))
    self.m_NameLabelTbl["562"] = tf.transform:Find("MainArea/NameTbl2/G2"):GetComponent(typeof(UILabel))

    self.m_IconTextureTbl={}
    self.m_IconTextureTbl["491"] = tf.transform:Find("MainArea/IconTbl1/C1/Texture"):GetComponent(typeof(CUITexture))
    self.m_IconTextureTbl["492"] = tf.transform:Find("MainArea/IconTbl1/D2/Texture"):GetComponent(typeof(CUITexture))
    self.m_IconTextureTbl["501"] = tf.transform:Find("MainArea/IconTbl1/A1/Texture"):GetComponent(typeof(CUITexture))
    self.m_IconTextureTbl["502"] = tf.transform:Find("MainArea/IconTbl1/B2/Texture"):GetComponent(typeof(CUITexture))
    self.m_IconTextureTbl["511"] = tf.transform:Find("MainArea/IconTbl2/B1/Texture"):GetComponent(typeof(CUITexture))
    self.m_IconTextureTbl["512"] = tf.transform:Find("MainArea/IconTbl2/A2/Texture"):GetComponent(typeof(CUITexture))
    self.m_IconTextureTbl["521"] = tf.transform:Find("MainArea/IconTbl2/D1/Texture"):GetComponent(typeof(CUITexture))
    self.m_IconTextureTbl["522"] = tf.transform:Find("MainArea/IconTbl2/C2/Texture"):GetComponent(typeof(CUITexture))
    self.m_IconTextureTbl["531"] = tf.transform:Find("MainArea/IconTbl1/E1/Texture"):GetComponent(typeof(CUITexture))
    self.m_IconTextureTbl["532"] = tf.transform:Find("MainArea/IconTbl1/F2/Texture"):GetComponent(typeof(CUITexture))
    self.m_IconTextureTbl["541"] = tf.transform:Find("MainArea/IconTbl1/G1/Texture"):GetComponent(typeof(CUITexture))
    self.m_IconTextureTbl["542"] = tf.transform:Find("MainArea/IconTbl1/H2/Texture"):GetComponent(typeof(CUITexture))
    self.m_IconTextureTbl["551"] = tf.transform:Find("MainArea/IconTbl2/F1/Texture"):GetComponent(typeof(CUITexture))
    self.m_IconTextureTbl["552"] = tf.transform:Find("MainArea/IconTbl2/E2/Texture"):GetComponent(typeof(CUITexture))
    self.m_IconTextureTbl["561"] = tf.transform:Find("MainArea/IconTbl2/H1/Texture"):GetComponent(typeof(CUITexture))
    self.m_IconTextureTbl["562"] = tf.transform:Find("MainArea/IconTbl2/G2/Texture"):GetComponent(typeof(CUITexture))

    self.m_GameObjectTbl = {}
    self.m_GameObjectTbl["571"] = tf.transform:Find("MainArea/Static/Left/A571").gameObject
    self.m_GameObjectTbl["572"] = tf.transform:Find("MainArea/Static/Left/A572").gameObject
    self.m_GameObjectTbl["581"] = tf.transform:Find("MainArea/Static/Left/A581").gameObject
    self.m_GameObjectTbl["582"] = tf.transform:Find("MainArea/Static/Left/A582").gameObject
    self.m_GameObjectTbl["611"] = tf.transform:Find("MainArea/Static/Left/A611").gameObject
    self.m_GameObjectTbl["612"] = tf.transform:Find("MainArea/Static/Left/A612").gameObject
    self.m_GameObjectTbl["641"] = tf.transform:Find("MainArea/Static/Left/A641").gameObject

    self.m_GameObjectTbl["591"] = tf.transform:Find("MainArea/Static/Right/A591").gameObject
    self.m_GameObjectTbl["592"] = tf.transform:Find("MainArea/Static/Right/A592").gameObject
    self.m_GameObjectTbl["601"] = tf.transform:Find("MainArea/Static/Right/A601").gameObject
    self.m_GameObjectTbl["602"] = tf.transform:Find("MainArea/Static/Right/A602").gameObject
    self.m_GameObjectTbl["621"] = tf.transform:Find("MainArea/Static/Right/A621").gameObject
    self.m_GameObjectTbl["622"] = tf.transform:Find("MainArea/Static/Right/A622").gameObject
    self.m_GameObjectTbl["642"] = tf.transform:Find("MainArea/Static/Right/A642").gameObject


    self.m_Id2DefaultName = {}
    self.m_Id2DefaultName["491"] = "C1"
    self.m_Id2DefaultName["492"] = "D2"
    self.m_Id2DefaultName["501"] = "A1"
    self.m_Id2DefaultName["502"] = "B2"
    self.m_Id2DefaultName["511"] = "B1"
    self.m_Id2DefaultName["512"] = "A2"
    self.m_Id2DefaultName["521"] = "D1"
    self.m_Id2DefaultName["522"] = "C2"
    self.m_Id2DefaultName["531"] = "E1"
    self.m_Id2DefaultName["532"] = "F2"
    self.m_Id2DefaultName["541"] = "G1"
    self.m_Id2DefaultName["542"] = "H2"
    self.m_Id2DefaultName["551"] = "F1"
    self.m_Id2DefaultName["552"] = "E2"
    self.m_Id2DefaultName["561"] = "H1"
    self.m_Id2DefaultName["562"] = "G2"

    self.m_WinnerFronTbl = {}
    self.m_WinnerFronTbl[57] = {49, 50}
    self.m_WinnerFronTbl[58] = {53, 54}
    self.m_WinnerFronTbl[59] = {55, 56}
    self.m_WinnerFronTbl[60] = {51, 52}
    self.m_WinnerFronTbl[61] = {57, 58}
    self.m_WinnerFronTbl[62] = {59, 60}
    self.m_WinnerFronTbl[64] = {61, 62}

    self.m_ChampionTexture = tf.transform:Find("MainArea/Static/Texture"):GetComponent(typeof(CUITexture))
    self.m_ChampionLabel = tf.transform:Find("MainArea/Static/Sprite/Label"):GetComponent(typeof(UILabel))
    self.m_ChampionLine1 = tf.transform:Find("MainArea/Static/Line1"):GetComponent(typeof(UISprite))
    self.m_ChampionLine2 = tf.transform:Find("MainArea/Static/Line2"):GetComponent(typeof(UISprite))

    self:ResetUI()
end

function CLuaWorldCupJueSaiView:ResetUI()
    for k, v in pairs(self.m_NameLabelTbl) do
        v.text = self.m_Id2DefaultName[k] or ""
    end

    for k, v in pairs(self.m_IconTextureTbl) do
        v.gameObject:SetActive(false)
    end

    for k, v in pairs(self.m_GameObjectTbl) do
        local Texture = v.transform:Find("Texture"):GetComponent(typeof(CUITexture))
        local Line1 = v.transform:Find("Line1"):GetComponent(typeof(UISprite))
        local Line2 = v.transform:Find("Line2"):GetComponent(typeof(UISprite))
        local Line3 = v.transform:Find("Line3"):GetComponent(typeof(UISprite))
        local Line4 = v.transform:Find("Line4"):GetComponent(typeof(UISprite))
        self:SetNormalColor(Line1)
        self:SetNormalColor(Line2)
        self:SetNormalColor(Line3)
        self:SetNormalColor(Line4)
        Texture.gameObject:SetActive(false)
    end

    self.m_ChampionLabel.text = ""
    self:SetNormalColor(self.m_ChampionLine1)
    self:SetNormalColor(self.m_ChampionLine2)
end

function CLuaWorldCupJueSaiView:Refresh(data)
    self:ResetUI()

    local list = MsgPackImpl.unpack(data)
    local Round2ResultInfo = {}
    for i=0, list.Count-1, 6 do
        local round = list[i]
        local tid1 = list[i+1]
        local tid2 = list[i+2]
        local score1 = list[i+3]
        local score2 = list[i+4]
        local dianqiu = list[i+5]

        Round2ResultInfo[round] = {tid1, tid2, score1, score2, dianqiu}
    end

    for round, info in pairs(Round2ResultInfo) do
        if round >= 49 and round <= 56 then
            if info[1] and info[1] ~= -1 then
                self:JueSai4956Handle1(Round2ResultInfo, info[1], round, 1)
            end
            if info[2] and info[2] ~= -1 then
                self:JueSai4956Handle1(Round2ResultInfo, info[2], round, 2)
            end
        elseif round >= 57 and round <= 64 and round ~= 63 then
            if info[1] and info[1] ~= -1 then
                self:JueSai5764Handle1(Round2ResultInfo, info[1], round, 1)
            end
            if info[2] and info[2] ~= -1 then
                self:JueSai5764Handle1(Round2ResultInfo, info[2], round, 2)
            end
        end
    end

    -- 尝试找出冠军
    local ChampionId = nil
    if Round2ResultInfo[64] then
        if Round2ResultInfo[64][3] ~= -1 and Round2ResultInfo[64][4] ~= -1 then
            if Round2ResultInfo[64][3] > Round2ResultInfo[64][4] then
                ChampionId = Round2ResultInfo[64][1]
            else
                ChampionId = Round2ResultInfo[64][2]
            end
        end
    end
    if ChampionId then
        local TeamInfo = CLuaWorldCupMgr.GetTeamInfoById(ChampionId)
        self.m_ChampionLabel.text = LocalString.GetString(TeamInfo.Name)
        self:SetActiveColor(self.m_ChampionLine1)
        self:SetActiveColor(self.m_ChampionLine2)
    end
end

function CLuaWorldCupJueSaiView:JueSai4956Handle1(Round2ResultInfo, tid, round, index)
    local key = SafeStringFormat("%s%s", round, index)
    local TeamInfo = CLuaWorldCupMgr.GetTeamInfoById(tid)
    self.m_NameLabelTbl[key].text = LocalString.GetString(TeamInfo.Name)
    self.m_IconTextureTbl[key]:LoadMaterial(TeamInfo.Icon)
    self.m_IconTextureTbl[key].gameObject:SetActive(true)
end

function CLuaWorldCupJueSaiView:JueSai5764Handle1(Round2ResultInfo, tid, round, index)
    local key = SafeStringFormat("%s%s", round, index)
    local TeamInfo = CLuaWorldCupMgr.GetTeamInfoById(tid)
    
    local Texture = self.m_GameObjectTbl[key].transform:Find("Texture"):GetComponent(typeof(CUITexture))
    Texture:LoadMaterial(TeamInfo.Icon)
    Texture.gameObject:SetActive(true)

    local Line1 = self.m_GameObjectTbl[key].transform:Find("Line1"):GetComponent(typeof(UISprite))
    local Line2 = self.m_GameObjectTbl[key].transform:Find("Line2"):GetComponent(typeof(UISprite))
    local Line3 = self.m_GameObjectTbl[key].transform:Find("Line3"):GetComponent(typeof(UISprite))
    local Line4 = self.m_GameObjectTbl[key].transform:Find("Line4"):GetComponent(typeof(UISprite))

    local fromRound = self.m_WinnerFronTbl[round][index]
    if tid == Round2ResultInfo[fromRound][1] then
        self:SetActiveColor(Line1)
        self:SetActiveColor(Line2)
        self:SetNormalColor(Line3)
        self:SetNormalColor(Line4)
    elseif tid == Round2ResultInfo[fromRound][2] then
        self:SetActiveColor(Line3)
        self:SetActiveColor(Line4)
        self:SetNormalColor(Line1)
        self:SetNormalColor(Line2)
    else
        -- 不可能
    end
end

function CLuaWorldCupJueSaiView:OnSelected()
    Gac2Gas.QueryWorldCupAgendaInfo()
end

function CLuaWorldCupJueSaiView:SetNormalColor(line)
    if line then
        LuaUtils.SetUIWidgetColor(line, 41, 67, 105, 255)
    end
end

function CLuaWorldCupJueSaiView:SetActiveColor(line)
    if line then
        LuaUtils.SetUIWidgetColor(line, 0, 255, 0, 255)
    end
end

return CLuaWorldCupJueSaiView
