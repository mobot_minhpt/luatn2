local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemMgr = import "L10.Game.CItemMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CTooltip = import "L10.UI.CTooltip"

LuaFuLiQuan2022MainWnd = class()

function LuaFuLiQuan2022MainWnd:Awake()
    self:InitUIComponent()
    self:InitUIEvent()
    self:InitUIData()
    self:RefreshUIWidget()
end

function LuaFuLiQuan2022MainWnd:Init()

end

function LuaFuLiQuan2022MainWnd:InitUIData()
    local neteaseOffical = (CommonDefs.IS_CN_CLIENT and CLoginMgr.Inst:IsNetEaseOfficialLogin())
    local mainplayer = CClientMainPlayer.Inst
    --2023版没有万能卡, 跟2022版非官网一致, 所以把变量改成默认false
    self.isOfficialClient = false
    --if neteaseOffical and mainplayer then
    --    self.isOfficialClient = true
    --end

    local settingConfigData = Double11_Setting.GetData()
    self.specialCardItemId = settingConfigData.WannengCardItemId
    self.normalCardItemIdList = {settingConfigData.CardItemId[0], settingConfigData.CardItemId[1], settingConfigData.CardItemId[2], settingConfigData.CardItemId[3]}
end

function LuaFuLiQuan2022MainWnd:InitUIComponent()
    self.officialCardContent = self.transform:Find("Anchor/OfficialContent")
    self.officialSpecialCardObject = self.officialCardContent:Find("SpeicalCardButton").gameObject
    self.officialNormalCardObjectList = {}
    table.insert(self.officialNormalCardObjectList, self.officialCardContent:Find("OfficialFirstCardButton").gameObject)
    table.insert(self.officialNormalCardObjectList, self.officialCardContent:Find("OfficialSecondCardButton").gameObject)
    table.insert(self.officialNormalCardObjectList, self.officialCardContent:Find("OfficialThirdCardButton").gameObject)
    table.insert(self.officialNormalCardObjectList, self.officialCardContent:Find("OfficialFourthCardButton").gameObject)

    self.unofficialCardContent = self.transform:Find("Anchor/UnofficialContent")
    self.unofficialNormalCardObjectList = {}
    table.insert(self.unofficialNormalCardObjectList, self.unofficialCardContent:Find("UnofficialFirstCardButton").gameObject)
    table.insert(self.unofficialNormalCardObjectList, self.unofficialCardContent:Find("UnofficialSecondCardButton").gameObject)
    table.insert(self.unofficialNormalCardObjectList, self.unofficialCardContent:Find("UnofficialThirdCardButton").gameObject)
    table.insert(self.unofficialNormalCardObjectList, self.unofficialCardContent:Find("UnofficialFourthCardButton").gameObject)
    
    self.ruleDescButton = self.transform:Find("Anchor/RuleDescBtn")
    self.exchangeButton = self.transform:Find("Anchor/ExchangeButton")
end

function LuaFuLiQuan2022MainWnd:InitUIEvent()
    UIEventListener.Get(self.ruleDescButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("Double11_FuLiQuan_Tips")
    end)

    UIEventListener.Get(self.exchangeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        self:OnClickExchangeButton()
    end)
end

function LuaFuLiQuan2022MainWnd:OnClickExchangeButton()
    local isSatisfyExchangeDemand = true
    for i = 1, #self.normalCardItemIdList do
        local totalCount = CItemMgr.Inst:GetItemCount(self.normalCardItemIdList[i])
        if totalCount == 0 then
            isSatisfyExchangeDemand = false
            break
        end
    end

    if isSatisfyExchangeDemand then
        -- 四类卡都有, 满足兑换需求
        Gac2Gas.RequestExchangeItems(139,1)
    else    
        -- 不满足兑换需求
        g_MessageMgr:ShowMessage("FuLiQuan_Collect_Four_Cards_Reminder")
    end
end

function LuaFuLiQuan2022MainWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "OnPlayerItemChange")
end

function LuaFuLiQuan2022MainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "OnPlayerItemChange")
end

function LuaFuLiQuan2022MainWnd:OnPlayerItemChange()
    self:RefreshUIWidget()
end

function LuaFuLiQuan2022MainWnd:RefreshUIWidget()
    if self.isOfficialClient then
        self.officialCardContent.gameObject:SetActive(true)
        self.unofficialCardContent.gameObject:SetActive(false)
        self:RefreshCardData(self.officialSpecialCardObject, self.specialCardItemId)
        for i = 1, #self.normalCardItemIdList do
            self:RefreshCardData(self.officialNormalCardObjectList[i], self.normalCardItemIdList[i])
        end
    else    
        self.officialCardContent.gameObject:SetActive(false)
        self.unofficialCardContent.gameObject:SetActive(true)
        for i = 1, #self.normalCardItemIdList do
            self:RefreshCardData(self.unofficialNormalCardObjectList[i], self.normalCardItemIdList[i])
        end
    end
end

function LuaFuLiQuan2022MainWnd:RefreshCardData(cardObject, cardItemId)
    local totalCount = CItemMgr.Inst:GetItemCount(cardItemId)
    
    local numberLabel = cardObject.transform:Find("Background/NumberLabel"):GetComponent(typeof(UILabel))
    numberLabel.text = LocalString.GetString("拥有  ") .. totalCount
    
    local showGrayState = (totalCount == 0)
    local bgObject = cardObject.transform:Find("bg_Texture").gameObject
    CUICommonDef.SetActive(bgObject, not showGrayState)
    cardObject.transform:Find("GetFlag").gameObject:SetActive(showGrayState)

    UIEventListener.Get(cardObject).onClick = DelegateFactory.VoidDelegate(function (_)
        self:OnClickCard(cardObject, cardItemId, showGrayState)
    end)
end

function LuaFuLiQuan2022MainWnd:OnClickCard(cardObject, cardItemId, showGrayState)
    if showGrayState then
        -- 未拥有的点击交互
        CItemAccessListMgr.Inst:ShowItemAccessInfo(cardItemId, false, cardObject.transform, CTooltip.AlignType.Right)
    else    
        -- 已拥有的点击交互
        if cardItemId == self.specialCardItemId then
            -- 这张卡是万能卡, 需要特殊处理
            local function Action(index)
                if index == 0 then
                    --使用万能卡
                    CUIManager.ShowUI(CLuaUIResources.FuLiQuan2022ExchangeWnd)
                else
                    --获取途径
                    CItemAccessListMgr.Inst:ShowItemAccessInfo(cardItemId, false, cardObject.transform, CTooltip.AlignType.Right)
                end
            end
            
            local selectItem = DelegateFactory.Action_int(Action)
            local t = {}
            local item1 = PopupMenuItemData(LocalString.GetString("使用万能卡"), selectItem, false, nil)
            table.insert(t, item1)
            local item2 = PopupMenuItemData(LocalString.GetString("获取途径"), selectItem, false, nil)
            table.insert(t, item2)
            local array = Table2Array(t, MakeArrayClass(PopupMenuItemData))
            CPopupMenuInfoMgr.ShowPopupMenu(array, cardObject.transform, CPopupMenuInfoMgr.AlignType.Right)
        else
            -- 这张卡是普通卡
            local function Action(index)
                if index == 0 then
                    --获取途径
                    CItemAccessListMgr.Inst:ShowItemAccessInfo(cardItemId, false, cardObject.transform, CTooltip.AlignType.Right)
                end
            end

            local selectItem = DelegateFactory.Action_int(Action)
            local t = {}
            local item1 = PopupMenuItemData(LocalString.GetString("获取途径"), selectItem, false, nil)
            table.insert(t, item1)
            local array = Table2Array(t, MakeArrayClass(PopupMenuItemData))
            CPopupMenuInfoMgr.ShowPopupMenu(array, cardObject.transform, CPopupMenuInfoMgr.AlignType.Right)
        end
    end
end 