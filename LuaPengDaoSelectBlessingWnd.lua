local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UISprite = import "UISprite"
local QnRadioBox = import "L10.UI.QnRadioBox"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local DelegateFactory  = import "DelegateFactory"
local Buff_Buff = import "L10.Game.Buff_Buff"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"

LuaPengDaoSelectBlessingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPengDaoSelectBlessingWnd, "OkButton", "OkButton", GameObject)
RegistChildComponent(LuaPengDaoSelectBlessingWnd, "RightChooseTemplate", "RightChooseTemplate", QnSelectableButton)
RegistChildComponent(LuaPengDaoSelectBlessingWnd, "RadioBox", "RadioBox", QnRadioBox)
RegistChildComponent(LuaPengDaoSelectBlessingWnd, "LeftScrollView", "LeftScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaPengDaoSelectBlessingWnd, "LeftMainTable", "LeftMainTable", UITable)
RegistChildComponent(LuaPengDaoSelectBlessingWnd, "LeftSkillTemplate", "LeftSkillTemplate", GameObject)
RegistChildComponent(LuaPengDaoSelectBlessingWnd, "LeftInfoLabelTemplate", "LeftInfoLabelTemplate", GameObject)
RegistChildComponent(LuaPengDaoSelectBlessingWnd, "OwnSkillTable", "OwnSkillTable", UITable)
RegistChildComponent(LuaPengDaoSelectBlessingWnd, "LeftBuffLabelTemplate", "LeftBuffLabelTemplate", GameObject)
RegistChildComponent(LuaPengDaoSelectBlessingWnd, "OwnBuffTable", "OwnBuffTable", UITable)
RegistChildComponent(LuaPengDaoSelectBlessingWnd, "SkillBackground", "SkillBackground", UISprite)
RegistChildComponent(LuaPengDaoSelectBlessingWnd, "BuffBackground", "BuffBackground", UISprite)
RegistChildComponent(LuaPengDaoSelectBlessingWnd, "LeftSkillView", "LeftSkillView", GameObject)
RegistChildComponent(LuaPengDaoSelectBlessingWnd, "LeftBuffView", "LeftBuffView", GameObject)
RegistChildComponent(LuaPengDaoSelectBlessingWnd, "RightChooseTemplate_highlight", "RightChooseTemplate_highlight", QnSelectableButton)
RegistChildComponent(LuaPengDaoSelectBlessingWnd, "TopBuffLabelTemplate", "TopBuffLabelTemplate", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaPengDaoSelectBlessingWnd,"m_SelectId")
RegistClassMember(LuaPengDaoSelectBlessingWnd,"m_ReplaceType")
RegistClassMember(LuaPengDaoSelectBlessingWnd,"m_ReplaceId")
RegistClassMember(LuaPengDaoSelectBlessingWnd,"m_SelectIndex")
RegistClassMember(LuaPengDaoSelectBlessingWnd,"m_NeedCloseWndAfterReplacing")
RegistClassMember(LuaPengDaoSelectBlessingWnd,"m_RightChooseTemplateHighlightPos")

function LuaPengDaoSelectBlessingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)

    --@endregion EventBind end
end

function LuaPengDaoSelectBlessingWnd:Init()
    self:InitLeftView()
    self:InitRadioBox()
end

function LuaPengDaoSelectBlessingWnd:InitLeftView()
    self:InitLeftSkillView()
    self:InitLeftBuffView()
    self.LeftMainTable:Reposition()
end

function LuaPengDaoSelectBlessingWnd:InitLeftSkillView()
    self.LeftSkillTemplate.gameObject:SetActive(false)
    self.LeftInfoLabelTemplate.gameObject:SetActive(false)
    self.TopBuffLabelTemplate.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.OwnSkillTable.transform) 
    self.LeftSkillView.gameObject:SetActive(LuaPengDaoMgr.m_ShowNextLevelBlessingInfo.extraSkills and #LuaPengDaoMgr.m_ShowNextLevelBlessingInfo.extraSkills > 0)
    if LuaPengDaoMgr.m_ShowNextLevelBlessingInfo.extraSkills then
        if LuaPengDaoMgr.m_ShowNextLevelBlessingInfo.selectedBlessings then
            for blessingId, info in pairs(LuaPengDaoMgr.m_ShowNextLevelBlessingInfo.selectedBlessings) do
                local data = PengDaoFuYao_InsiderGain.GetData(blessingId)
                if data and data.Type == 1 and data.IsSkillGain > 0 then
                    local obj = NGUITools.AddChild(self.OwnSkillTable.gameObject, self.TopBuffLabelTemplate.gameObject)
                    obj.gameObject:SetActive(true)
                    local label = obj:GetComponent(typeof(UILabel))
                    label.text = self:GetInsiderGainDesc(blessingId,info[2])
                    label:ResizeCollider()
                end
            end
        end
        for _, info in pairs(LuaPengDaoMgr.m_ShowNextLevelBlessingInfo.extraSkills) do
            local type, id = info[1], info[2]
            local skillId = 0
            local obj = NGUITools.AddChild(self.OwnSkillTable.gameObject, self.LeftSkillTemplate.gameObject)
            obj.gameObject:SetActive(true)
            local skillIcon = obj.transform:Find("Texture"):GetComponent(typeof(CUITexture))
            local nameLabel = obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
            if type == 1 then
                local data = PengDaoFuYao_InsiderGain.GetData(id)
                skillId = data.GainId
                nameLabel.text = data.Name
            else
                local data = PengDaoFuYao_OutsiderSkill.GetData(id)
                skillId = data.SkillId * 100 + 1
                nameLabel.text = data.Name
            end
            local skillData = Skill_AllSkills.GetData(skillId)
            skillIcon:LoadSkillIcon(skillData and skillData.SkillIcon or nil)
            UIEventListener.Get(skillIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                CSkillInfoMgr.ShowSkillInfoWnd(skillId, true, 0, 0, nil)
            end)
        end
    end
    local hasSkill = LuaPengDaoMgr.m_ShowNextLevelBlessingInfo.extraSkills and (#LuaPengDaoMgr.m_ShowNextLevelBlessingInfo.extraSkills > 0) or false
    if hasSkill then
        local obj = NGUITools.AddChild(self.OwnSkillTable.gameObject, self.LeftInfoLabelTemplate.gameObject)
        obj.gameObject:SetActive(true)
    end
    self.OwnSkillTable:Reposition()
end

function LuaPengDaoSelectBlessingWnd:InitLeftBuffView()
    self.LeftBuffLabelTemplate.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.OwnBuffTable.transform)
    self.LeftBuffView.gameObject:SetActive(false)
    if LuaPengDaoMgr.m_ShowNextLevelBlessingInfo.selectedBlessings then
        for blessingId, info in pairs(LuaPengDaoMgr.m_ShowNextLevelBlessingInfo.selectedBlessings) do
            local data = PengDaoFuYao_InsiderGain.GetData(blessingId)
            if data.IsSkillGain == 0 then
                self.LeftBuffView.gameObject:SetActive(true)
                local obj = NGUITools.AddChild(self.OwnBuffTable.gameObject, self.LeftBuffLabelTemplate.gameObject)
                obj.gameObject:SetActive(true)
                local label = obj:GetComponent(typeof(UILabel))
                label.text = self:GetInsiderGainDesc(blessingId,info[2])
                label:ResizeCollider()
            end
        end
    end
    self.OwnBuffTable:Reposition()
end

function LuaPengDaoSelectBlessingWnd:GetInsiderGainDesc(id,list)
    local data = PengDaoFuYao_InsiderGain.GetData(id)
    if list then
        if #list == 1 then
            return SafeStringFormat3(data.Desc, list[1])
        elseif #list == 2 then
            return SafeStringFormat3(data.Desc, list[1], list[2])
        elseif #list == 3 then
            return SafeStringFormat3(data.Desc, list[1], list[2], list[3])
        end
    end
    return data.Desc
end

function LuaPengDaoSelectBlessingWnd:InitRadioBox()
    self.RightChooseTemplate.gameObject:SetActive(false)
    self.RightChooseTemplate_highlight.gameObject:SetActive(false)
    self.m_RightChooseTemplateHighlightPos = self.RightChooseTemplate_highlight.transform.localPosition
    Extensions.RemoveAllChildren(self.RadioBox.m_Table.transform)
    if LuaPengDaoMgr.m_ShowNextLevelBlessingInfo.candidateBlessings then
        self.RadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), #LuaPengDaoMgr.m_ShowNextLevelBlessingInfo.candidateBlessings)
        for i, info in pairs(LuaPengDaoMgr.m_ShowNextLevelBlessingInfo.candidateBlessings) do
            local obj = NGUITools.AddChild(self.RadioBox.m_Table.gameObject, self.RightChooseTemplate.gameObject)
            self:InitRightChooseTemplate(obj,  info)
            obj:SetActive(true)
            self.RadioBox.m_RadioButtons[i - 1] = obj:GetComponent(typeof(QnSelectableButton))
            self.RadioBox.m_RadioButtons[i - 1].OnClick = MakeDelegateFromCSFunction(self.RadioBox.On_Click, MakeGenericClass(Action1, QnButton), self.RadioBox)
        end
    end
    self.RadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnRadioBoxSelected(btn,index)
    end)
    self.RadioBox.m_Table:Reposition()
    self.m_SelectId = 0
    self.m_ReplaceType = 0 --0不替换，1替换随机增益，2替换装配技能
    self.m_ReplaceId = 0
    self.RadioBox:ChangeTo(0, true)
end

function LuaPengDaoSelectBlessingWnd:InitRightChooseTemplate(obj, info)
    local id, list = info[1],info[2]
    local yellow = obj.transform:Find("Yellow")
    local blue = obj.transform:Find("Blue")
    local red = obj.transform:Find("Red")
    local purple = obj.transform:Find("Purple")
    local colorArr = {yellow, blue, red, purple}
    local nameColorArr = {"fffe91","95c7ff","ffad92","c591ff"}
    local icon = obj.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local nameLabel = obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local desLabel = obj.transform:Find("DesLabel"):GetComponent(typeof(UILabel)) 
    local skillObj = obj.transform:Find("Skill")
    local buffObj = obj.transform:Find("Buff")
    local skillBottomLabel = skillObj.transform:Find("SkillBottomLabel"):GetComponent(typeof(UILabel))
    local buffBottomLabel = buffObj.transform:Find("BuffBottomLabel"):GetComponent(typeof(UILabel))
    local data = PengDaoFuYao_InsiderGain.GetData(id)
    skillObj.gameObject:SetActive(data.Type == 2)
    buffObj.gameObject:SetActive(data.Type == 1)
    local detailDesc = self:GetInsiderGainDesc(info[1],info[2])
    desLabel.text = data.NeedDetailMsg > 0 and data.DesSimplify or detailDesc
    nameLabel.text = data.Name
    nameLabel.color = NGUIText.ParseColor24(nameColorArr[data.Rarity], 0)
    for rarity, colorObj in pairs(colorArr) do
        colorObj.gameObject:SetActive(rarity == data.Rarity) 
    end
    if data.Type == 1 then
        local buffData = Buff_Buff.GetData(data.GainId)
        icon:LoadMaterial(buffData.Icon)
        buffBottomLabel.gameObject:SetActive(data.NeedDetailMsg > 0)
        buffBottomLabel.text = g_MessageMgr:FormatMessage("PengDaoSelectBlessingWnd_BuffBottomLabel")
        UIEventListener.Get(buffBottomLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:ShowBuffTip(data.Name, detailDesc)
        end)
        UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:ShowBuffTip(data.Name, detailDesc)
        end)
    else
        local skillId = data.GainId
        local skillData = Skill_AllSkills.GetData(skillId)
        icon:LoadSkillIcon(skillData.SkillIcon)
        skillBottomLabel.text = g_MessageMgr:FormatMessage("PengDaoSelectBlessingWnd_SkillBottomLabel")
        UIEventListener.Get(skillBottomLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:ShowSkillTip(skillId, icon)
        end)
        UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:ShowSkillTip(skillId, icon)
        end)
    end
end

function LuaPengDaoSelectBlessingWnd:ShowSkillTip(skillId, item)
    CSkillInfoMgr.ShowSkillInfoWnd(skillId, item.transform.position, CSkillInfoMgr.EnumSkillInfoContext.Default, true, 0, 0, nil)
end

function LuaPengDaoSelectBlessingWnd:ShowBuffTip(name, detailDesc)
    g_MessageMgr:ShowMessage("PENGDAOFUYAO_INSIDERGAIN_DETAIL",name, detailDesc)
end

function LuaPengDaoSelectBlessingWnd:OnEnable()
    g_ScriptEvent:AddListener("PengDaoReplaceSkillWnd_ReplaceSkill", self, "OnReplaceSkill")
end

function LuaPengDaoSelectBlessingWnd:OnDisable()
    g_ScriptEvent:RemoveListener("PengDaoReplaceSkillWnd_ReplaceSkill", self, "OnReplaceSkill")
end

function LuaPengDaoSelectBlessingWnd:OnReplaceSkill(index)
    if index < 0 then
        self.m_NeedCloseWndAfterReplacing = false
        return
    end
    local info = LuaPengDaoMgr.m_ShowNextLevelBlessingInfo.extraSkills[index + 1]
    local type, id = info[1], info[2]
    local skillId = 0
    if type == 1 then
        local data = PengDaoFuYao_InsiderGain.GetData(id)
        skillId = data.GainId
        self.m_ReplaceType = 1
    else
        local data = PengDaoFuYao_OutsiderSkill.GetData(id)
        skillId = data.SkillId * 100 + 1
        self.m_ReplaceType = 2
    end
    self.m_SelectId = LuaPengDaoMgr.m_ReplaceBlessingId 
    self.m_ReplaceId = id
    if self.m_NeedCloseWndAfterReplacing then
        Gac2Gas.PDFY_SelectBlessing(self.m_SelectId, self.m_ReplaceType, self.m_ReplaceId)
        g_ScriptEvent:BroadcastInLua("PDFY_SelectBlessing",self.m_SelectId, self.m_ReplaceType, self.m_ReplaceId)
        CUIManager.CloseUI(CLuaUIResources.PengDaoSelectBlessingWnd)
    end
end
--@region UIEvent

function LuaPengDaoSelectBlessingWnd:OnOkButtonClick()
    if self.m_SelectId == 0 then
        local msg = g_MessageMgr:FormatMessage("PengDaoSelectBlessingWnd_ReplaceSkill_Confirm")
        MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
            self.m_NeedCloseWndAfterReplacing = true
            CUIManager.ShowUI(CLuaUIResources.PengDaoReplaceSkillWnd)
		end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
        return
    end
    Gac2Gas.PDFY_SelectBlessing(self.m_SelectId, self.m_ReplaceType, self.m_ReplaceId)
    g_ScriptEvent:BroadcastInLua("PDFY_SelectBlessing",self.m_SelectId, self.m_ReplaceType, self.m_ReplaceId)
    CUIManager.CloseUI(CLuaUIResources.PengDaoSelectBlessingWnd)
end

function LuaPengDaoSelectBlessingWnd:OnRadioBoxSelected(btn,index)
    self.m_SelectIndex = index
    local info = LuaPengDaoMgr.m_ShowNextLevelBlessingInfo.candidateBlessings[index + 1]
    self:InitRightChooseTemplate(self.RightChooseTemplate_highlight,  info)
    self.RightChooseTemplate_highlight.gameObject:SetActive(true)
    for i = 0, self.RadioBox.m_RadioButtons.Length - 1 do
        local normalBtn = self.RadioBox.m_RadioButtons[i]
        normalBtn.gameObject:SetActive(i ~= index)
        if i == index then
            local x = self.m_RightChooseTemplateHighlightPos.x + normalBtn.transform.localPosition.x - self.RadioBox.m_RadioButtons[0].transform.localPosition.x
            local y = self.m_RightChooseTemplateHighlightPos.y
            self.RightChooseTemplate_highlight.transform.localPosition = Vector3(x, y, 0) 
        end
    end
    local id = info[1]
    self.m_SelectId = 0
    self.m_ReplaceType = 0 
    self.m_ReplaceId = 0
    local data = PengDaoFuYao_InsiderGain.GetData(id)
    if data.Type == 1 then
        self.m_SelectId = info[1]
    elseif data.Type == 2 then
        if LuaPengDaoMgr.m_ShowNextLevelBlessingInfo.extraSkills and #LuaPengDaoMgr.m_ShowNextLevelBlessingInfo.extraSkills == 2 then
            LuaPengDaoMgr.m_ReplaceBlessingId = info[1]
        else
            self.m_SelectId = info[1]
        end
    end
    self.RadioBox.m_Table:Reposition()
end
--@endregion UIEvent

