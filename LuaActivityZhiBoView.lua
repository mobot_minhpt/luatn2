local DelegateFactory = import "DelegateFactory"
local UILabel = import "UILabel"
local LocalString = import "LocalString"

CLuaActivityZhiBoView = class()
RegistClassMember(CLuaActivityZhiBoView,"m_Labels")
RegistClassMember(CLuaActivityZhiBoView,"m_LabelWidth")
RegistClassMember(CLuaActivityZhiBoView, "m_Abstract")
RegistClassMember(CLuaActivityZhiBoView, "ScrollTick")
RegistClassMember(CLuaActivityZhiBoView, "m_AbstractRoot")
CLuaActivityZhiBoView.m_ScrollSpeed = 50
CLuaActivityZhiBoView.m_ScrollSpeedAngle = 1
CLuaActivityZhiBoView.m_Space = 10
CLuaActivityZhiBoView.m_Degree = 0
--CLuaActivityZhiBoView.ScrollTick = nil


function CLuaActivityZhiBoView:OnEnable()

    self.m_AbstractRoot = self.gameObject.transform.parent.parent:Find("AbstractRootAnchor/AbstractRoot").gameObject

    if(self.ScrollTick)then
        UnRegisterTick(self.ScrollTick)
        self.ScrollTick=nil
    end
    if self.m_AbstractRoot then
        self.m_AbstractRoot:SetActive(true)
        self.m_AbstractRoot.transform.localPosition = self.transform.localPosition
        -- CommonDefs.AddOnClickListener(self.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnZhiBoButtonClick(go) end), false)
        UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnZhiBoButtonClick(go)
        end)

        self.m_Abstract = self.m_AbstractRoot.transform:Find("Abstract").gameObject
        local label_root = self.m_Abstract.transform:Find("ViewPanel").gameObject

        self.m_Labels = {}
        table.insert(self.m_Labels, label_root.transform:Find("Label0").gameObject:GetComponent(typeof(UILabel)))
        table.insert(self.m_Labels, label_root.transform:Find("Label1").gameObject:GetComponent(typeof(UILabel)))
        self:ResetAbstractLabels()
    end

    g_ScriptEvent:AddListener("RefreshDynamicActivity",self,"OnRefreshDynamicActivity")

end

function CLuaActivityZhiBoView:Update()
    if self.m_AbstractRoot then
        self.m_AbstractRoot.transform.localPosition = self.transform.localPosition
    end
end

function CLuaActivityZhiBoView:OnDisable()
    g_ScriptEvent:RemoveListener("RefreshDynamicActivity",self,"OnRefreshDynamicActivity")
    if(self.ScrollTick)then
        UnRegisterTick(self.ScrollTick)
        self.ScrollTick=nil
    end
    if self.m_AbstractRoot then
        self.m_AbstractRoot:SetActive(false)
    end
end
function CLuaActivityZhiBoView:OnRefreshDynamicActivity()
    if self.m_AbstractRoot then
        self.m_AbstractRoot.transform.localPosition = self.transform.localPosition
    end
end

function CLuaActivityZhiBoView:OnDestroy()

    if(self.ScrollTick)then
        UnRegisterTick(self.ScrollTick)
        self.ScrollTick=nil
    end
end

function CLuaActivityZhiBoView:OnZhiBoButtonClick(go)
    CLuaActivityZhiBoMgr.OnZhiboButtonClick()
end

function CLuaActivityZhiBoView:ResetAbstractLabels()
    if(CLuaActivityZhiBoMgr.Abstract and #CLuaActivityZhiBoMgr.Abstract > 0)then
        local leng = self:CalculateStringLength(CLuaActivityZhiBoMgr.Abstract)
        self.m_Abstract:SetActive(true)
        if(self.ScrollTick)then
            UnRegisterTick(self.ScrollTick)
            self.ScrollTick=nil
        end
        if(leng <= 4)then
            self.m_Labels[1].text = LocalString.GetString(CLuaActivityZhiBoMgr.Abstract)
            self.m_Labels[1].transform.localPosition = Vector3(0,0,0)
            self.m_Labels[1].gameObject:SetActive(true)
            self.m_Labels[2].gameObject:SetActive(false)
        else
            self.m_Labels[1].gameObject:SetActive(true)
            self.m_Labels[2].gameObject:SetActive(true)
            self.m_Labels[1].text = LocalString.GetString(CLuaActivityZhiBoMgr.Abstract)
            self.m_Labels[2].text = LocalString.GetString(CLuaActivityZhiBoMgr.Abstract)
            self.m_LabelWidth = self.m_Labels[1].width
            CLuaActivityZhiBoView.m_ScrollSpeedAngle = 180 * CLuaActivityZhiBoView.m_ScrollSpeed / (self.m_LabelWidth + CLuaActivityZhiBoView.m_Space)

            self.ScrollTick = RegisterTick(function()
                self:UpdateAbstract()
            end,100)
        end
    else
        if(self.ScrollTick)then
            UnRegisterTick(self.ScrollTick)
            self.ScrollTick=nil
        end
        self.m_Abstract:SetActive(false)
    end
end

function CLuaActivityZhiBoView:UpdateAbstract()

    CLuaActivityZhiBoView.m_Degree = CLuaActivityZhiBoView.m_Degree + CLuaActivityZhiBoView.m_ScrollSpeedAngle * 0.032
    if(CLuaActivityZhiBoView.m_Degree > 360)then
        CLuaActivityZhiBoView.m_Degree = CLuaActivityZhiBoView.m_Degree - 360
    end
    local secondary = CLuaActivityZhiBoView.m_Degree - 180
    if(secondary < 0)then
        secondary = secondary + 360
    end
    local interval = self.m_LabelWidth + CLuaActivityZhiBoView.m_Space
    local maxinterval = interval + 121
    local distance = CLuaActivityZhiBoView.m_Degree / 180 * interval
    if(distance <= maxinterval)then
        self.m_Labels[1].gameObject:SetActive(true)
        self.m_Labels[1].transform.localPosition = Vector3(maxinterval * 0.5 -(distance),0,0)
    else
        self.m_Labels[1].gameObject:SetActive(false)
    end

    distance = secondary / 180 * interval
    if(distance <= maxinterval)then
        self.m_Labels[2].gameObject:SetActive(true)
        self.m_Labels[2].transform.localPosition = Vector3(maxinterval * 0.5 -(distance),0,0)
    else
        self.m_Labels[2].gameObject:SetActive(false)
    end
end

function CLuaActivityZhiBoView:CalculateStringLength(_str)
    local length = 0
    local id = 1
    for i = 1, 9999, 1 do
        local current_byte = string.byte(_str, id)
        local byte_count = 1
        if(current_byte > 239)then--4
            byte_count = 4
        elseif(current_byte > 223)then--3
            byte_count = 3
        elseif(current_byte > 128)then--2
            byte_count = 2
        end
        length = length + 1
        id = id + byte_count
        if(id > #_str)then
            break
        end
    end
    return length
end
