require("common/common_include")
local CUICommonDef = import "L10.UI.CUICommonDef"
local GameObject  = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local CUIManager = import "L10.UI.CUIManager"
local System = import "System"
local UISprite = import "UISprite"
local Color = import "UnityEngine.Color"
local MessageWndManager = import "L10.UI.MessageWndManager"
local LocalString = import "LocalString"

CLuaRecallDetailWnd=class()
RegistChildComponent(CLuaRecallDetailWnd, "m_DetailBtn", CButton)
RegistChildComponent(CLuaRecallDetailWnd, "m_Yaoqingma", CButton)
RegistChildComponent(CLuaRecallDetailWnd, "m_copyBtn", CButton)
RegistChildComponent(CLuaRecallDetailWnd, "Pool", GameObject)
RegistChildComponent(CLuaRecallDetailWnd, "huiliunumLabel", UILabel)
RegistChildComponent(CLuaRecallDetailWnd, "copyPanel", GameObject)

RegistClassMember(CLuaRecallDetailWnd,"pressDuration")
RegistClassMember(CLuaRecallDetailWnd,"ispressed")

function CLuaRecallDetailWnd:Awake()

	self.pressDuration=0
	self.ispressed=false


	CommonDefs.AddOnClickListener(self.m_DetailBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
		CUIManager.ShowUI(CLuaUIResources.PopupRecallWnd)
		
	end), false)

	CommonDefs.AddOnClickListener(self.m_copyBtn.gameObject, DelegateFactory.Action_GameObject(function (go)

		local yaoqingmaLabel = self.m_Yaoqingma.gameObject:GetComponent(typeof(UILabel))
		CUICommonDef.clipboardText = yaoqingmaLabel.text;
		
		self.copyPanel.gameObject:SetActive(false)
		local text = LocalString.GetString("已复制到剪切板")
        MessageWndManager.ShowOKMessage(text, nil)
        	
	end), false)

	local onPress = function(go,flag)
    	if flag then
			self.ispressed=true
		else
			self.ispressed=false
			self.pressDuration=0
		end
	end

    CommonDefs.AddOnPressListener(self.m_Yaoqingma.gameObject,DelegateFactory.Action_GameObject_bool(onPress),false)	
    self:DefaultInit()
end

function CLuaRecallDetailWnd:Init()

	
	
	
end

function CLuaRecallDetailWnd:Update()
	if self.ispressed then
		self.pressDuration = self.pressDuration + Time.deltaTime
		if self.pressDuration>0.5 then
			self.copyPanel.gameObject:SetActive(true)
			self.ispressed=false
		end
	end


end

function CLuaRecallDetailWnd:InitData(finishedList)

	if not self.gameObject.activeSelf then return end

	local index = 1
	local tbl = {}
	MergeBattle_Event.Foreach(function(k, v)
		if v.PlayGroup == 5 then
			table.insert(tbl, k)
		end
	end)
	table.sort(tbl)
	for _, k in ipairs(tbl) do
		self:InitItemToTable(index,false,k)
		index=index+1
	end
    
	if finishedList then
		for i = 0, finishedList.Count - 1 do
			local ddata = MergeBattle_Event.GetData(finishedList[i])
			if ddata.PlayGroup==5 then --
				if ddata.Score==100 then
					self:InitItemToTable(1,true,finishedList[i])
				elseif ddata.Score==300 then
					self:InitItemToTable(2,true,finishedList[i])
				elseif ddata.Score==600 then
					self:InitItemToTable(3,true,finishedList[i])
				elseif ddata.Score==1000 then
					self:InitItemToTable(4,true,finishedList[i])
				end
			end
		end
	end
end

function CLuaRecallDetailWnd:InitItemToTable(index,isover,id)
	--local Pool=FindChild(tf,"Pool"):GetComponent(GameObject)
	local item =CommonDefs.GetComponent_Component_Type(self.Pool.gameObject.transform:Find(System.String.Format(LocalString.GetString("Item{0}"), index)), typeof(UISprite))
	if item then
		local textLabel = CommonDefs.GetComponent_Component_Type(item.gameObject.transform:Find("text"), typeof(UILabel))
	    local levelLabel = CommonDefs.GetComponent_Component_Type(item.gameObject.transform:Find("level"), typeof(UILabel))
		local ddata = MergeBattle_Event.GetData(id)
	    textLabel.text=ddata.Description
	    levelLabel.text=System.String.Format(LocalString.GetString("奖励 {0}积分"), ddata.Score)
		if isover then
			textLabel.color=Color.white
	    	levelLabel.color=Color.white
	    	textLabel.alpha=0.5
	    	levelLabel.alpha=0.5
	    	item.gameObject.transform:Find("Sprite").gameObject:SetActive(true)
	    else
	    	textLabel.color=Color.white
	    	levelLabel.color=Color.yellow
	    	textLabel.alpha=1
	    	levelLabel.alpha=1
	    	item.gameObject.transform:Find("Sprite").gameObject:SetActive(false)
	    end
	end

end

function CLuaRecallDetailWnd:InitBind(bindCode,count,finishedList)
	local yaoqingmaLabel = self.m_Yaoqingma.gameObject:GetComponent(typeof(UILabel))
	yaoqingmaLabel.text= bindCode
	self.huiliunumLabel.text=count 
end

function CLuaRecallDetailWnd:DefaultInit()
	self.copyPanel.gameObject:SetActive(false)
	local yaoqingmaLabel = self.m_Yaoqingma.gameObject:GetComponent(typeof(UILabel))
	yaoqingmaLabel.text=""
	self.huiliunumLabel.text=0 
	local index = 1
	local tbl = {}
	MergeBattle_Event.Foreach(function(k, v)
		if v.PlayGroup == 5 then
			table.insert(tbl, k)
		end
	end)
	table.sort(tbl)
	for _, k in ipairs(tbl) do
		self:InitItemToTable(index,false,k)
		index=index+1
	end
end

function CLuaRecallDetailWnd:OnEnable()
	--self.m_copyBtn.gameObject:SetActive(false)
	self.copyPanel.gameObject:SetActive(false)
	g_ScriptEvent:AddListener("QueryMergeBattlePersonalEventsResult", self, "InitData")
	g_ScriptEvent:AddListener("QueryMergeBattleInviteHuiLiuInfoResult", self, "InitBind")
	Gac2Gas.QueryMergeBattleInviteHuiLiuInfo()
    Gac2Gas.QueryMergeBattlePersonalEvents()
	
end

function CLuaRecallDetailWnd:OnDisable()
	g_ScriptEvent:RemoveListener("QueryMergeBattlePersonalEventsResult", self, "InitData")
	g_ScriptEvent:RemoveListener("QueryMergeBattleInviteHuiLiuInfoResult", self, "InitBind")
end

