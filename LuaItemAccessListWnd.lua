local UIRoot = import "UIRoot"
local Object = import "System.Object"
local NGUITools = import "NGUITools"
local NGUIMath = import "NGUIMath"
local Extensions = import "Extensions"
local CTooltip = import "L10.UI.CTooltip"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CCommonLuaWnd=import "L10.UI.CCommonLuaWnd"
local CUIRestrictScrollView=import "L10.UI.CUIRestrictScrollView"
local CItemAccessTemplate=import "L10.UI.CItemAccessTemplate"
local MsgPackImpl = import "MsgPackImpl"

CLuaItemAccessListWnd = class()
RegistClassMember(CLuaItemAccessListWnd,"nameLabel")
RegistClassMember(CLuaItemAccessListWnd,"table")
RegistClassMember(CLuaItemAccessListWnd,"scrollview")
RegistClassMember(CLuaItemAccessListWnd,"accessTemplate")
RegistClassMember(CLuaItemAccessListWnd,"background")
RegistClassMember(CLuaItemAccessListWnd,"notEnoughLabel")
RegistClassMember(CLuaItemAccessListWnd,"m_playershopItem")
RegistClassMember(CLuaItemAccessListWnd,"m_Wnd")
RegistClassMember(CLuaItemAccessListWnd,"m_BgHeight")

function CLuaItemAccessListWnd:Awake()
    self.m_Wnd=self.gameObject:GetComponent(typeof(CCommonLuaWnd))

    self.nameLabel = self.transform:Find("Anchor/Background/Name"):GetComponent(typeof(UILabel))
    self.table = self.transform:Find("Anchor/Background/ScrollView/AccessList"):GetComponent(typeof(UITable))
    self.scrollview = self.transform:Find("Anchor/Background/ScrollView"):GetComponent(typeof(CUIRestrictScrollView))
    self.accessTemplate = self.transform:Find("Anchor/Background/ScrollView/AccessTemplate").gameObject
    self.background = self.transform:Find("Anchor/Background"):GetComponent(typeof(UISprite))
    self.notEnoughLabel = self.transform:Find("Anchor/Background/NotEnough").gameObject
    self.m_playershopItem = self.transform:Find(""):GetComponent(typeof(CItemAccessTemplate))

    self.m_BgHeight = self.background.height
end

function CLuaItemAccessListWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryPlayerShopOnShelfNumResult",self,"OnQueryPlayerShopOnShelfNumResult")
end
function CLuaItemAccessListWnd:OnDisable()
    self.m_playershopItem=nil
    g_ScriptEvent:RemoveListener("QueryPlayerShopOnShelfNumResult",self,"OnQueryPlayerShopOnShelfNumResult")

end
function CLuaItemAccessListWnd:Update()
    self.m_Wnd:ClickThroughToClose()
end

function CLuaItemAccessListWnd:OnQueryPlayerShopOnShelfNumResult( args) 
local selledCounts, publicity = args[0],args[1]
    for i=0,selledCounts.Count-1,2 do
        local id = selledCounts[i]
        local count = selledCounts[i + 1]

        local templateId = CItemAccessListMgr.Inst.ItemTemplateId
        local itemGetData = ItemGet_item.GetData(templateId)
        if itemGetData and itemGetData.SearchID_WanJia>0 then
            templateId = itemGetData.SearchID_WanJia
        end
        if templateId == id then
            if self.m_playershopItem ~= nil then
                local str = SafeStringFormat3(LocalString.GetString("玩家商店([f6f871]%s[-]家商店在售)"), count)
                self.m_playershopItem:ChangeName(count>20 and LocalString.GetString("玩家商店(多家商店在售)") or str)
            end
        end
    end
end
function CLuaItemAccessListWnd:Init( )
    self.notEnoughLabel:SetActive(not CItemAccessListMgr.Inst.NumEnough)
    self.nameLabel.text = LocalString.GetString("获得途径")
    Extensions.RemoveAllChildren(self.table.transform)
    self.accessTemplate:SetActive(false)
    local cmpstr = LocalString.GetString("玩家商店")

    for i=1,CItemAccessListMgr.Inst.AccessList.Count do
        local itemData = CItemAccessListMgr.Inst.AccessList[i-1]

        local accessGo = NGUITools.AddChild(self.table.gameObject, self.accessTemplate)
        accessGo:SetActive(true)
        local itemAccess = CommonDefs.GetComponent_GameObject_Type(accessGo, typeof(CItemAccessTemplate))
        itemAccess:Init(itemData.accessName, itemData.btnName, itemData.onBtnClicked)
        if itemData.accessName == cmpstr then
            self.m_playershopItem = itemAccess
            local templateId = CItemAccessListMgr.Inst.ItemTemplateId
            local itemGetData = ItemGet_item.GetData(templateId)
            if itemGetData and itemGetData.SearchID_WanJia>0 then
                templateId = itemGetData.SearchID_WanJia
            end
            local data = CreateFromClass(MakeGenericClass(List, Object))
            local data2 = CreateFromClass(MakeGenericClass(List, Object))
            CommonDefs.ListAdd(data, typeof(UInt32), templateId)
			-- data2 肯定为空 再发一次就好 没必要创建一个新的
            Gac2Gas.QueryPlayerShopOnShelfNum(MsgPackImpl.pack(data), MsgPackImpl.pack(data2), MsgPackImpl.pack(data2), false, SearchOption_lua.All, false)
        end
    end

    self.background.height = self.m_BgHeight + (math.min(CItemAccessListMgr.Inst.AccessList.Count, 4) * (100 + math.floor(self.table.padding.y)))
    self.table:Reposition()
    self.scrollview:ResetPosition()
    self:SetPosition(CItemAccessListMgr.Inst.alignType, CItemAccessListMgr.Inst.targetCenterPos, CItemAccessListMgr.Inst.targetSize)
end
function CLuaItemAccessListWnd:SetPosition( type, worldPos, targetSize) 
    local localPos = self.background.transform.parent:InverseTransformPoint(worldPos)
    local selfBounds = NGUIMath.CalculateRelativeWidgetBounds(self.background.transform)
    repeat
        local default = type
        if default == CTooltip.AlignType.Default then
            self.background.transform.localPosition = localPos
            break
        elseif default == CTooltip.AlignType.Left then
            self.background.transform.localPosition = Vector3(localPos.x - self.background.localSize.x * 0.5 - targetSize.x * 0.5 - 10, localPos.y + selfBounds.size.y * 0.5, 0)
            break
        elseif default == CTooltip.AlignType.Right then
            self.background.transform.localPosition = Vector3(localPos.x + self.background.localSize.x * 0.5 + targetSize.x * 0.5 + 10, localPos.y + selfBounds.size.y * 0.5, 0)
            break
        elseif default == CTooltip.AlignType.Bottom then
            self.background.transform.localPosition = Vector3(localPos.x, - 10, 0)
            break
        elseif default == CTooltip.AlignType.Top then
            self.background.transform.localPosition = Vector3(localPos.x, localPos.y + targetSize.y * 0.5 + selfBounds.size.y + 10, 0)
            break
        elseif default == CTooltip.AlignType.Absolute then
            self.background.transform.localPosition = CItemAccessListMgr.Inst.absoluteLocalPosition
            break
        end
    until 1
    local scale = UIRoot.GetPixelSizeAdjustment(self.background.gameObject)
    local mainScreenSize = CUIManager.MainScreenSize
    local virtualScreenWidth = mainScreenSize.x * scale
    local virtualScreenHeight = mainScreenSize.y * scale
    local offsetX = self.background.transform.localPosition.x
    local offsetY = self.background.transform.localPosition.y

    if UIRoot.EnableIPhoneXAdaptation then
        virtualScreenWidth = virtualScreenWidth - UIRoot.IPhoneXWidthMargin*2
    end
    
    --左右不超出屏幕范围
    if offsetX < (- virtualScreenWidth * 0.5 + self.background.width * 0.5) then
        offsetX = - virtualScreenWidth * 0.5 + self.background.width * 0.5 + 25
    end
    if offsetX > (virtualScreenWidth * 0.5 - self.background.width * 0.5) then
        offsetX = virtualScreenWidth * 0.5 - self.background.width * 0.5 - 25
    end
    --上下不超出屏幕范围
    if offsetY < (- virtualScreenHeight * 0.5 + self.background.height) then
        offsetY = - virtualScreenHeight * 0.5 + self.background.height + 25
    end
    if offsetY > virtualScreenHeight * 0.5 then
        offsetY = virtualScreenHeight * 0.5 - 25
    end
    self.background.transform.localPosition = Vector3(offsetX, offsetY, 0)
end
