local UILabel = import "UILabel"
local QnButton = import "L10.UI.QnButton"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UIProgressBar = import "UIProgressBar"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CCommonLuaWnd = import "L10.UI.CCommonLuaWnd"
local UIPanel = import "UIPanel"
local DelegateFactory  = import "DelegateFactory"

LuaShanYeMiZongGuessWnd = class()
LuaShanYeMiZongGuessWnd.s_GuessId = nil
LuaShanYeMiZongGuessWnd.s_GuessState = nil -- 0:猜想选择 1:等待结果 2:结果揭晓

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShanYeMiZongGuessWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaShanYeMiZongGuessWnd, "QuestionLabel", "QuestionLabel", UILabel)
RegistChildComponent(LuaShanYeMiZongGuessWnd, "TipButton", "TipButton", QnButton)
RegistChildComponent(LuaShanYeMiZongGuessWnd, "Btn0", "Btn0", QnSelectableButton)
RegistChildComponent(LuaShanYeMiZongGuessWnd, "Btn1", "Btn1", QnSelectableButton)
RegistChildComponent(LuaShanYeMiZongGuessWnd, "Btn2", "Btn2", QnSelectableButton)
RegistChildComponent(LuaShanYeMiZongGuessWnd, "Btn3", "Btn3", QnSelectableButton)
RegistChildComponent(LuaShanYeMiZongGuessWnd, "OkButton", "OkButton", QnButton)
RegistChildComponent(LuaShanYeMiZongGuessWnd, "WaitForResultHint", "WaitForResultHint", UILabel)

--@endregion RegistChildComponent end

RegistClassMember(LuaShanYeMiZongGuessWnd, "m_Panel")
RegistClassMember(LuaShanYeMiZongGuessWnd, "m_CurChoice")

function LuaShanYeMiZongGuessWnd:Awake()
    self.m_Panel = self.transform:GetComponent(typeof(UIPanel))
    --self.m_Panel.alpha = 0
    
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.Btn0.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtn0Click()
	end)


	
	UIEventListener.Get(self.Btn1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtn1Click()
	end)


	
	UIEventListener.Get(self.Btn2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtn2Click()
	end)


	
	UIEventListener.Get(self.Btn3.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtn3Click()
	end)


	
	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)


    --@endregion EventBind end
end

function LuaShanYeMiZongGuessWnd:Init()
    self.WaitForResultHint.gameObject:SetActive(false)
    self.OkButton.gameObject:SetActive(false)
    if LuaShanYeMiZongGuessWnd.s_GuessId then
        if LuaShanYeMiZongGuessWnd.s_GuessId == 1 then
            self.TitleLabel.text = LocalString.GetString("猜想一")
        elseif LuaShanYeMiZongGuessWnd.s_GuessId == 2 then 
            self.TitleLabel.text = LocalString.GetString("猜想二")
        else
            self.TitleLabel.text = LocalString.GetString("猜想三")
        end
        local data = ShuJia2022_CaiXiang.GetData(LuaShanYeMiZongGuessWnd.s_GuessId)
        if data then
            self.QuestionLabel.text = data.Question
            for i = 0, 3 do
                self["Btn"..i].Selectable = false
            end
            self.Btn0.transform:Find("Label"):GetComponent(typeof(UILabel)).text = data.Answer1
            self.Btn1.transform:Find("Label"):GetComponent(typeof(UILabel)).text = data.Answer2
            self.Btn2.transform:Find("Label"):GetComponent(typeof(UILabel)).text = data.Answer3
            self.Btn3.transform:Find("Label"):GetComponent(typeof(UILabel)).text = data.Answer4
        end
        if LuaShanYeMiZongGuessWnd.s_GuessState and LuaShanYeMiZongGuessWnd.s_GuessState > 0 then
            Gac2Gas.QueryShanYeMiZongGambleInfo(LuaShanYeMiZongGuessWnd.s_GuessId)
        else
            --self.m_Panel.alpha = 1
            self.OkButton.gameObject:SetActive(true)
        end
    end
end

function LuaShanYeMiZongGuessWnd:OnEnable()
    g_ScriptEvent:AddListener("SendShanYeMiZongCaiXiangInfo", self, "OnSendShanYeMiZongCaiXiangInfo")
    g_ScriptEvent:AddListener("SendShanYeMiZongInfo", self, "RefreshAll")
end

function LuaShanYeMiZongGuessWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendShanYeMiZongCaiXiangInfo", self, "OnSendShanYeMiZongCaiXiangInfo")
    g_ScriptEvent:RemoveListener("SendShanYeMiZongInfo", self, "RefreshAll")
end

function LuaShanYeMiZongGuessWnd:OnBtnClick(btnId)
    if LuaShanYeMiZongGuessWnd.s_GuessState and LuaShanYeMiZongGuessWnd.s_GuessState == 0 then 
        if self.m_CurChoice == btnId then
            self.m_CurChoice = nil
        else
            self.m_CurChoice = btnId
            self["Btn"..(btnId-1)]:SetSelected(true)
        end
        for i = 1, 4 do
            if i ~= self.m_CurChoice then
                self["Btn"..(i-1)]:SetSelected(false)
            end
        end
    end
end

--@region UIEvent

function LuaShanYeMiZongGuessWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("ShuJia2022_CaiXiang_Tips")
end

function LuaShanYeMiZongGuessWnd:OnBtn0Click()
    self:OnBtnClick(1)
end

function LuaShanYeMiZongGuessWnd:OnBtn1Click()
    self:OnBtnClick(2)
end

function LuaShanYeMiZongGuessWnd:OnBtn2Click()
    self:OnBtnClick(3)
end

function LuaShanYeMiZongGuessWnd:OnBtn3Click()
    self:OnBtnClick(4)
end

function LuaShanYeMiZongGuessWnd:OnOkButtonClick()
    if LuaShanYeMiZongGuessWnd.s_GuessId and self.m_CurChoice then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("SYMZ_CAIXIANG_CONFIRM"), function()
            LuaShanYeMiZongGuessWnd.s_GuessState = 1
            Gac2Gas.ShanYeMiZongRequestGamble(LuaShanYeMiZongGuessWnd.s_GuessId, self.m_CurChoice)
        end, nil, nil, nil, false)
    else
        g_MessageMgr:ShowMessage("ShuJia2022_CaiXiang_Choose")
        --print("Haven't Chose Btn")
    end
end


--@endregion UIEvent

function LuaShanYeMiZongGuessWnd:OnSendShanYeMiZongCaiXiangInfo(choiceInfo, myChoice, rightAnswer)
    for i = 0, 3 do
        local progress = self["Btn"..i].transform:Find("Progress"):GetComponent(typeof(UIProgressBar))
        progress.value = choiceInfo[i + 1]
        if LuaShanYeMiZongGuessWnd.s_GuessState > 0 then
            progress.gameObject:SetActive(true)
        else
            progress.gameObject:SetActive(false)
        end
    end
    if myChoice and myChoice > 0 then
        self["Btn"..(myChoice - 1)]:SetSelected(true)
        if LuaShanYeMiZongGuessWnd.s_GuessState == 1 then
            self.WaitForResultHint.text = LocalString.GetString("你已完成选择，真相尚待揭晓")
        else
            self["Btn"..(rightAnswer - 1)].transform:Find("CorrectSprite").gameObject:SetActive(true)
            if myChoice == rightAnswer then
                self.WaitForResultHint.text = LocalString.GetString("真相已揭晓，恭喜成功猜对")
                self.WaitForResultHint.color = Color.green
            else
                self["Btn"..(myChoice - 1)].transform:Find("WrongSprite").gameObject:SetActive(true)
                self.WaitForResultHint.text = LocalString.GetString("真相已揭晓，很遗憾未能猜对")
                self.WaitForResultHint.color = Color.red
            end
        end
    else
        if LuaShanYeMiZongGuessWnd.s_GuessState == 1 then
            self.WaitForResultHint.text = LocalString.GetString("你未曾做出选择，真相尚待揭晓")
        else
            self["Btn"..(rightAnswer - 1)].transform:Find("CorrectSprite").gameObject:SetActive(true)
            self.WaitForResultHint.text = LocalString.GetString("真相已揭晓，你未曾做出选择")
        end
    end
    self.WaitForResultHint.gameObject:SetActive(true)
    --self.m_Panel.alpha = 1
end

-- 领取成功后会刷新主界面
function LuaShanYeMiZongGuessWnd:RefreshAll()
    if LuaShanYeMiZongGuessWnd.s_GuessState and LuaShanYeMiZongGuessWnd.s_GuessState == 1 then
        self:Init()
    end
end
