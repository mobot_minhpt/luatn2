local UIProgressBar = import "UIProgressBar"

local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"

local CScene=import "L10.Game.CScene"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"

LuaChristmas2023BingXuePlayPlayingWnd = class()
RegistClassMember(LuaChristmas2023BingXuePlayPlayingWnd, "m_SceneInfo")             -- 场景信息
RegistClassMember(LuaChristmas2023BingXuePlayPlayingWnd, "m_IsStartPick")           -- 采集模式倒计时是否开始
RegistClassMember(LuaChristmas2023BingXuePlayPlayingWnd, "m_StartPickTick")         -- "开始采集"字体播放动画
RegistClassMember(LuaChristmas2023BingXuePlayPlayingWnd, "m_CountDownShowTick")     -- 倒计时出现延迟任务
RegistClassMember(LuaChristmas2023BingXuePlayPlayingWnd, "m_PlayCountDownListener") -- 玩法倒计时监听事件

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChristmas2023BingXuePlayPlayingWnd, "BossMode", "BossMode", GameObject)
RegistChildComponent(LuaChristmas2023BingXuePlayPlayingWnd, "PickMode", "PickMode", GameObject)
RegistChildComponent(LuaChristmas2023BingXuePlayPlayingWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaChristmas2023BingXuePlayPlayingWnd, "ProgressBar", "ProgressBar", UIProgressBar)
RegistChildComponent(LuaChristmas2023BingXuePlayPlayingWnd, "HpLabel", "HpLabel", UILabel)
RegistChildComponent(LuaChristmas2023BingXuePlayPlayingWnd, "CountDown", "CountDown", GameObject)
RegistChildComponent(LuaChristmas2023BingXuePlayPlayingWnd, "StartPick", "StartPick", GameObject)
RegistChildComponent(LuaChristmas2023BingXuePlayPlayingWnd, "ExpandButton", "ExpandButton", GameObject)
RegistChildComponent(LuaChristmas2023BingXuePlayPlayingWnd, "Anchor", "Anchor", GameObject)
RegistChildComponent(LuaChristmas2023BingXuePlayPlayingWnd, "PlayCountDownLabel", "PlayCountDownLabel", UILabel)
RegistChildComponent(LuaChristmas2023BingXuePlayPlayingWnd, "CountDownLabel", "CountDownLabel", UILabel)

--@endregion RegistChildComponent end

function LuaChristmas2023BingXuePlayPlayingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    -- 右侧按钮绑定功能窗口
    UIEventListener.Get(self.ExpandButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
end
-- 初始化雪球积分
function LuaChristmas2023BingXuePlayPlayingWnd:InitPickMode()
    local jifenlist = Christmas2023_BingXuePlay.GetData().PickJifen
    for i = 0, 2 do
        local jifenlabel = self.PickMode.transform:GetChild(i):Find("Label"):GetComponent(typeof(UILabel))
        jifenlabel.text = '+' .. jifenlist[i]
    end
end
function LuaChristmas2023BingXuePlayPlayingWnd:Init()
    self.Anchor:SetActive(false) -- 收到第一个同步rpc之前先隐藏界面

    self:InitPickMode()

    self.m_SceneInfo = nil
    self.m_IsStartPick = false
    self.StartPick:SetActive(false)

    self:OnSyncBingXuePlay2023PlayData()
end
-- 加载boss模式数据
function LuaChristmas2023BingXuePlayPlayingWnd:LoadBossMode()
    self.BossMode:SetActive(true)
    self.PickMode:SetActive(false)
    self.CountDown:SetActive(false)
    if not self.m_SceneInfo then return end
    self.TitleLabel.text = LocalString.GetString("击败凛冬之王")
    self.ProgressBar.value = self.m_SceneInfo.BossHp / self.m_SceneInfo.BossFullHp
    self.HpLabel.text = self.m_SceneInfo.BossHp
end
-- 加载收集模式数据
function LuaChristmas2023BingXuePlayPlayingWnd:LoadPickMode()
    self.BossMode:SetActive(false)
    self.PickMode:SetActive(true)
    if not self.m_SceneInfo then return end
    self.TitleLabel.text = SafeStringFormat3(LocalString.GetString("采集雪球获得积分[ffff00]%d[-]"), self.m_SceneInfo.Score)
    if self.m_IsStartPick and self.m_SceneInfo.LeftPickTime > 0 then
        self.CountDown:SetActive(true)
        local color = self.m_SceneInfo.LeftPickTime > 10 and "ffff00" or "ff5050"
        self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("采集将于[%s]%d[-]秒后结束"), color, self.m_SceneInfo.LeftPickTime)
    else
        self.CountDown:SetActive(false)
    end
end
-- 同步数据
function LuaChristmas2023BingXuePlayPlayingWnd:OnSyncBingXuePlay2023PlayData()
    if not LuaChristmas2023Mgr.m_SceneInfo then return end
    self.Anchor:SetActive(true) -- 收到rpc后显示界面
    self.m_SceneInfo = LuaChristmas2023Mgr.m_SceneInfo
    if self.m_SceneInfo.Stage == LuaChristmas2023Mgr.EnumStage.Boss then
        self:LoadBossMode()
    else
        self:LoadPickMode()
    end
end
-- 采集模式开启
function LuaChristmas2023BingXuePlayPlayingWnd:OnStartBingXuePlay2023PickPart()
    self.StartPick:SetActive(true)
    if self.m_StartPickTick then
        UnRegisterTick(self.m_StartPickTick)
        self.m_StartPickTick = nil
    end
    self.m_StartPickTick = RegisterTickOnce(function()
        self.StartPick:SetActive(false)
    end, 2300)
    if self.m_CountDownShowTick then
        UnRegisterTick(self.m_CountDownShowTick)
        self.m_CountDownShowTick = nil
    end
    self.m_CountDownShowTick = RegisterTickOnce(function()
        self.m_IsStartPick = true
        self:LoadPickMode()
    end, 1000)
end
-- 功能窗口隐藏时翻转右侧按钮
function LuaChristmas2023BingXuePlayPlayingWnd:OnHideTopAndRightTipWnd()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end
-- 更新玩法倒计时
function LuaChristmas2023BingXuePlayPlayingWnd:UpdateRemainTime()
    local lefttime = 0
    if CScene.MainScene then
        lefttime = CScene.MainScene.ShowTime
    end
    if lefttime < 0 then lefttime = 0 end
    if lefttime >= 3600 then
        self.PlayCountDownLabel.text = SafeStringFormat3("%02d:%02d:%02d", math.floor(lefttime / 3600), math.floor(lefttime % 3600 / 60), lefttime % 60)
    else
        self.PlayCountDownLabel.text = SafeStringFormat3("%02d:%02d", math.floor(lefttime / 60), math.floor(lefttime % 60))
    end
end
function LuaChristmas2023BingXuePlayPlayingWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSyncBingXuePlay2023PlayData", self, "OnSyncBingXuePlay2023PlayData")
    g_ScriptEvent:AddListener("OnStartBingXuePlay2023PickPart", self, "OnStartBingXuePlay2023PickPart")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    self.m_PlayCountDownListener = DelegateFactory.Action_int(function(leftTime)
        self:UpdateRemainTime()
    end)
    EventManager.AddListenerInternal(EnumEventType.SceneRemainTimeUpdate, self.m_PlayCountDownListener)
end
function LuaChristmas2023BingXuePlayPlayingWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncBingXuePlay2023PlayData", self, "OnSyncBingXuePlay2023PlayData")
    g_ScriptEvent:RemoveListener("OnStartBingXuePlay2023PickPart", self, "OnStartBingXuePlay2023PickPart")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    EventManager.RemoveListenerInternal(EnumEventType.SceneRemainTimeUpdate, self.m_PlayCountDownListener)
    LuaChristmas2023Mgr.m_SceneInfo = nil
    LuaChristmas2023Mgr.m_LastScore = 0
    LuaChristmas2023Mgr.m_IsAward = false
    if self.m_StartPickTick then
        UnRegisterTick(self.m_StartPickTick)
        self.m_StartPickTick = nil
    end
    if self.m_CountDownShowTick then
        UnRegisterTick(self.m_CountDownShowTick)
        self.m_CountDownShowTick = nil
    end
end

--@region UIEvent

--@endregion UIEvent

