local UISearchView = import "L10.UI.UISearchView"
local CButton = import "L10.UI.CButton"
local CGameReplayMgr = import "L10.Game.CGameReplayMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgrAlignType = import  "L10.UI.CPopupMenuInfoMgr+AlignType"
local CPlayerShopPopupMenuInfoMgr = import "L10.UI.CPlayerShopPopupMenuInfoMgr"
--跨服明星赛事回顾
LuaStarBiwuEventReviewWnd = class()

RegistChildComponent(LuaStarBiwuEventReviewWnd,"m_ChooseButton1","ChooseButton1", CButton)
RegistChildComponent(LuaStarBiwuEventReviewWnd,"m_ChooseButton2","ChooseButton2", CButton)
RegistChildComponent(LuaStarBiwuEventReviewWnd,"m_ChooseButton3","ChooseButton3", CButton)
RegistChildComponent(LuaStarBiwuEventReviewWnd,"m_SearchView","SearchView", UISearchView)
RegistChildComponent(LuaStarBiwuEventReviewWnd,"m_OpenWndBtn","OpenWndBtn", GameObject)
RegistChildComponent(LuaStarBiwuEventReviewWnd,"m_TestWatchBtn","TestWatchBtn", GameObject)
RegistChildComponent(LuaStarBiwuEventReviewWnd,"m_EnterWatchBtn","EnterWatchBtn", GameObject)
RegistChildComponent(LuaStarBiwuEventReviewWnd,"m_DeleteButton","DeleteButton", GameObject)
RegistChildComponent(LuaStarBiwuEventReviewWnd,"m_BottomButtonsTable","BottomButtonsTable", UITable)
RegistChildComponent(LuaStarBiwuEventReviewWnd,"m_Grid","Grid", UIGrid)
RegistChildComponent(LuaStarBiwuEventReviewWnd,"m_CompetitionTemplate","CompetitionTemplate", GameObject)
RegistChildComponent(LuaStarBiwuEventReviewWnd,"m_ScrollView","ScrollView", UIScrollView)
RegistChildComponent(LuaStarBiwuEventReviewWnd,"m_NoneResWord","NoneResWord", GameObject)
RegistChildComponent(LuaStarBiwuEventReviewWnd,"m_Pool","Pool", GameObject)

RegistClassMember(LuaStarBiwuEventReviewWnd, "m_PoolObjs")
RegistClassMember(LuaStarBiwuEventReviewWnd, "m_Data")
RegistClassMember(LuaStarBiwuEventReviewWnd, "m_ShowObjs")
RegistClassMember(LuaStarBiwuEventReviewWnd, "m_CurrentData")
RegistClassMember(LuaStarBiwuEventReviewWnd, "m_CurrentSelectGo")
RegistClassMember(LuaStarBiwuEventReviewWnd, "m_SelectIndex1")
RegistClassMember(LuaStarBiwuEventReviewWnd, "m_SelectIndex2")
RegistClassMember(LuaStarBiwuEventReviewWnd, "m_SelectIndex3")
RegistClassMember(LuaStarBiwuEventReviewWnd, "m_SearchText")
RegistClassMember(LuaStarBiwuEventReviewWnd, "m_BtnTextArray1")
RegistClassMember(LuaStarBiwuEventReviewWnd, "m_BtnTextArray2")
RegistClassMember(LuaStarBiwuEventReviewWnd, "m_BtnTextArray3")
RegistClassMember(LuaStarBiwuEventReviewWnd, "m_SelectIndex1ToSeasonMap")
RegistClassMember(LuaStarBiwuEventReviewWnd, "m_Btn1ImageNameArray")

function LuaStarBiwuEventReviewWnd:Init()
    self.m_NoneResWord:SetActive(false)
    self.m_Pool:SetActive(false)
    self.m_CompetitionTemplate:SetActive(false)
    self.m_ChooseButton1.gameObject:SetActive(false)
    self.m_ChooseButton2.gameObject:SetActive(false)
    self.m_ChooseButton3.gameObject:SetActive(false)
    self.m_OpenWndBtn:SetActive(true)
    self.m_TestWatchBtn:SetActive(true)
    self.m_EnterWatchBtn:SetActive(false)
    self.m_DeleteButton:SetActive(false)
    self.m_BottomButtonsTable:Reposition()
    self.m_PoolObjs = {}
    self.m_ShowObjs = {}
    self.m_BtnTextArray1 = {}
    self.m_BtnTextArray2 = {}
    self.m_BtnTextArray3 = {}
    self.m_SelectIndex1 = 1
    self.m_SelectIndex2 = 1
    UIEventListener.Get(self.m_ChooseButton1.gameObject).onClick = DelegateFactory.VoidDelegate(function(p)
        self:OnChooseButton1Clicked()
    end)
    UIEventListener.Get(self.m_ChooseButton2.gameObject).onClick = DelegateFactory.VoidDelegate(function(p)
        self:OnChooseButton2Clicked()
    end)
    UIEventListener.Get(self.m_ChooseButton3.gameObject).onClick = DelegateFactory.VoidDelegate(function(p)
        self:OnChooseButton3Clicked()
    end)
    UIEventListener.Get(self.m_OpenWndBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(p)
        self:OpenDataAnalysisWnd()
    end)
    UIEventListener.Get(self.m_TestWatchBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(p)
        self:TestDownloadVideo()
    end)
    UIEventListener.Get(self.m_EnterWatchBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(p)
        self:WatchVideo()
    end)
    UIEventListener.Get(self.m_DeleteButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(p)
        self:DeleteVideo()
    end)
    self.m_SearchView.OnSearch = DelegateFactory.Action_string(function(str) self:OnSearchBtnClicked(str) end)
    self.m_SearchView.OnValueChanged = DelegateFactory.Action_string(function(str) self:OnSearchViewValueChanged(str) end)
    CLuaStarBiwuMgr:DownloadPlayList()
end

function LuaStarBiwuEventReviewWnd:OnEnable()
    g_ScriptEvent:AddListener("GameVideoListDownloaded", self, "OnGameVideoListDownloaded")
    g_ScriptEvent:AddListener("GameVideoDownloaded", self, "OnGameVideoDownloaded")
end

function LuaStarBiwuEventReviewWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GameVideoListDownloaded", self, "OnGameVideoListDownloaded")
    g_ScriptEvent:RemoveListener("GameVideoDownloaded", self, "OnGameVideoDownloaded")
end

function LuaStarBiwuEventReviewWnd:OnGameVideoListDownloaded(args)
    local success = args[0]
    local playName = args[1]
    if success then
        if playName == "starbiwu" and CLuaStarBiwuMgr.m_VideoRecord then
            self:OnDataLoad(CLuaStarBiwuMgr.m_VideoRecord)
        end
    end
end

function LuaStarBiwuEventReviewWnd:OnGameVideoDownloaded(args)
    local success = args[0]
    local fileName = args[1]
    if success then
        local season = self.m_SelectIndex1ToSeasonMap[self.m_SelectIndex1]
        local data = self.m_Data[season]
        for k, v in pairs(data) do
            if v.fileName == fileName then
                self:RefreshCompetitionTemplate(self.m_ShowObjs[v.id], v)
                self:OnSelect(self.m_CurrentData)
                break
            end
        end
    end
end

function LuaStarBiwuEventReviewWnd:OnDataLoad(data)
    self.m_Data = data
    self:InitChooseBtns()
    self:InitCompetitionEvents()
end

function LuaStarBiwuEventReviewWnd:SelectDatas()
    if not self.m_Data then return {} end
    local temp = {}
    local stage2Index = {}
    stage2Index[EnumStarBiwuFightStage.eJiFenSai] = 2
    stage2Index[EnumStarBiwuFightStage.eXiaoZuSai] = 3
    stage2Index[EnumStarBiwuFightStage.eZongJueSai] = 4
    local season = self.m_SelectIndex1ToSeasonMap[self.m_SelectIndex1]
    local data = self.m_Data[season]
    if not data then return {} end
    for k, v in pairs(data) do
        local matchIdx = v.playIndex ---积分赛轮次
        local index2 = stage2Index[v.stage]
        if index2 then
            if index2 == self.m_SelectIndex2 or 1 == self.m_SelectIndex2 then
                local find = 1 == self.m_SelectIndex3
                if v.stage == EnumStarBiwuFightStage.eJiFenSai then
                    find = find or (matchIdx == (self.m_SelectIndex3 - 1))
                elseif v.stage == EnumStarBiwuFightStage.eXiaoZuSai then
                    find = find or (v.group == (self.m_SelectIndex3 - 1))
                end
                if find then
                    table.insert(temp, v)
                end
            end
        end
    end
    local res = {}
    for k, v in pairs(temp) do
        if self.m_SearchText then
            local leftTeamName = v.teaminfos[1].name
            local rightTeamName = v.teaminfos[2].name
            if string.find(leftTeamName, self.m_SearchText) or string.find(rightTeamName, self.m_SearchText) then
                table.insert(res, v)
            end
        else
            table.insert(res, v)
        end
    end
    table.sort(res,function(a,b)
        return a.playStartTime > b.playStartTime
    end)
    return res
end

function LuaStarBiwuEventReviewWnd:InitCompetitionEvents()
    local data = self:SelectDatas()
    self.m_NoneResWord.gameObject:SetActive(#data == 0)

    for k,go in pairs(self.m_ShowObjs) do
        self:RecycleCompetitionTemplate(go)
    end
    self.m_ShowObjs ={}
    self.m_OpenWndBtn:SetActive(true)
    self.m_TestWatchBtn:SetActive(true)
    self.m_EnterWatchBtn:SetActive(false)
    self.m_DeleteButton:SetActive(false)
    self.m_CurrentData = nil
    self.m_BottomButtonsTable:Reposition()

    if #data == 0 then
        self.m_ScrollView:ResetPosition()
        return
    end

    for i = 1,#data do
        local go = self:InitCompetitionTemplate(data[i])
        local btn = go:GetComponent(typeof(CButton))
        btn.normalSprite = i % 2 == 0 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite
        btn.Selected = false
    end

    self.m_Grid:Reposition()
    self.m_ScrollView:ResetPosition()

    self:OnSelect(data[1])
end

function LuaStarBiwuEventReviewWnd:OnSelect(data)
    local isDownloaded = CGameReplayMgr.Instance:IsFileDownloaded(data.fileName)
    local isDownloading = CGameReplayMgr.Instance:IsFileDownloading(data.fileName)
    local unAllowedWatch = cs_string.IsNullOrEmpty(data.aliasName)

    self.m_DeleteButton:SetActive(isDownloaded)
    self.m_TestWatchBtn:SetActive(not isDownloaded and not isDownloading)
    self.m_EnterWatchBtn:SetActive(isDownloaded and not unAllowedWatch)
    self.m_BottomButtonsTable:Reposition()


    if self.m_CurrentData and self.m_ShowObjs[self.m_CurrentData.id] then
        self.m_ShowObjs[self.m_CurrentData.id]:GetComponent(typeof(CButton)).Selected = false
    end
    self.m_CurrentData = data
    if self.m_CurrentData and self.m_ShowObjs[self.m_CurrentData.id] then
        self.m_ShowObjs[self.m_CurrentData.id]:GetComponent(typeof(CButton)).Selected = true
    end
end

function LuaStarBiwuEventReviewWnd:InitCompetitionTemplate(data)
    local go = self:GetCompetitionTemplate()
    go.transform.parent = self.m_Grid.transform
    go:SetActive(true)

    self:RefreshCompetitionTemplate(go, data)

    self.m_ShowObjs[data.id] = go
    return go
end

function LuaStarBiwuEventReviewWnd:InitChooseBtns()
    self:InitChooseBtn1Array()
    self.m_BtnTextArray2 =  {LocalString.GetString("全部"),LocalString.GetString("积分赛"),LocalString.GetString("小组赛"),LocalString.GetString("淘汰赛")}
    local textArray1 = {LocalString.GetString("全部"),LocalString.GetString("甲组"),LocalString.GetString("乙组"),LocalString.GetString("丙组"),LocalString.GetString("丁组")}
    local textArray2 = {
        LocalString.GetString("全部"),LocalString.GetString("第一轮"),LocalString.GetString("第二轮"),
        LocalString.GetString("第三轮"),LocalString.GetString("第四轮"),LocalString.GetString("第五轮"),
        LocalString.GetString("第六轮"),LocalString.GetString("第七轮"),LocalString.GetString("第八轮"),
        LocalString.GetString("第九轮"),LocalString.GetString("第十轮"),LocalString.GetString("第十一轮"),
        LocalString.GetString("第十二轮")
    }
    self.m_BtnTextArray3 = self.m_SelectIndex2 == 2 and textArray2 or (self.m_SelectIndex2 == 3 and textArray1 or {})
    self.m_SelectIndex3 = 1
    local textArrayList = {self.m_BtnTextArray1,self.m_BtnTextArray2,self.m_BtnTextArray3}
    local btnList = {self.m_ChooseButton1,self.m_ChooseButton2,self.m_ChooseButton3}
    local indexList = {self.m_SelectIndex1,self.m_SelectIndex2,self.m_SelectIndex3}
    for i = 1, 3 do
        local textArray = textArrayList[i]
        if #textArray > 0 then
            btnList[i].Text = textArray[indexList[i]]
        end
    end
    self.m_ChooseButton1.gameObject:SetActive(true)
    self.m_ChooseButton2.gameObject:SetActive(true)
    self.m_ChooseButton3.gameObject:SetActive(#self.m_BtnTextArray3 > 0)
end

function LuaStarBiwuEventReviewWnd:InitChooseBtn1Array()
    self.m_BtnTextArray1 = {}
    self.m_SelectIndex1ToSeasonMap = {}
    StarBiWuShow_AutoPatch.Foreach(function(key, data) 
        if self.m_Data[data.Season] then
            local s = string.gsub(data.Desc, LocalString.GetString("跨服明星赛"), "")
            table.insert(self.m_BtnTextArray1, 1, s) 
            table.insert(self.m_SelectIndex1ToSeasonMap, 1, data.Season) 
        end
	end)
    self.m_Btn1ImageNameArray = {}
    for index, season in pairs(self.m_SelectIndex1ToSeasonMap) do
        local isShowSprite = false
        local data = self.m_Data[season]
        if data then 
            for i = 1,#data do
                for _, item in pairs(data[i].teaminfos) do
                    for _,info in pairs(item.players) do
                        if CClientMainPlayer.Inst and info.id == CClientMainPlayer.Inst.Id then
                            isShowSprite = true
                        end
                    end
                end
            end
        end
        self.m_Btn1ImageNameArray[index] = isShowSprite and "common_battletype_personal" or ""
    end
end

function LuaStarBiwuEventReviewWnd:RefreshCompetitionTemplate(go, data)
    local scoreRes = data.winTeamId == data.teaminfos[1].id
    local leftTeamName = data.teaminfos[1].name
    local rightTeamName = data.teaminfos[2].name
    local competitionName = data.competitionName
    local isDownloaded = CGameReplayMgr.Instance:IsFileDownloaded(data.fileName)
    local isDownloading = CGameReplayMgr.Instance:IsFileDownloading(data.fileName)
    local unAllowedWatch = cs_string.IsNullOrEmpty(data.fileName)
    local leftWinNum, rightWinNum = 0,0
    local scoreArrList = StarBiWuShow_Setting.GetData().Round_Duration_And_Score
    local season = self.m_SelectIndex1ToSeasonMap[self.m_SelectIndex1]
    for i = 0, data.roundWinTeamId.Count - 1 do
        if tonumber(data.roundWinTeamId[i]) == data.teaminfos[1].id then 
            leftWinNum = leftWinNum + ((season >= 11) and scoreArrList[i][1] or 1)
        else
            rightWinNum = rightWinNum  + ((season >= 11) and scoreArrList[i][1] or 1)
        end
    end

    go.transform:Find("ResSprite1"):GetComponent(typeof(UISprite)).spriteName = scoreRes and "common_fight_result_win" or (scoreRes == 0 and "common_fight_result_tie" or "common_fight_result_lose")
    go.transform:Find("ResSprite2"):GetComponent(typeof(UISprite)).spriteName = not scoreRes and "common_fight_result_win" or (scoreRes == 0 and "common_fight_result_tie" or "common_fight_result_lose")
    go.transform:Find("NameLabel1"):GetComponent(typeof(UILabel)).text = leftTeamName
    go.transform:Find("NameLabel2"):GetComponent(typeof(UILabel)).text = rightTeamName
    go.transform:Find("CompetitionLabel"):GetComponent(typeof(UILabel)).text = competitionName
    go.transform:Find("VSLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("%d:%d",leftWinNum, rightWinNum)
    go.transform:Find("DownloadingSprite").gameObject:SetActive(not isDownloaded and isDownloading and not unAllowedWatch)
    go.transform:Find("FinishedSprite").gameObject:SetActive(isDownloaded and not unAllowedWatch)
    go.transform:Find("NotAllowedWatchSprite").gameObject:SetActive(unAllowedWatch)
    local btn = go:GetComponent(typeof(CButton))
    btn.Selected = false
    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(p)
        self:OnSelect(data)
    end)
end

function LuaStarBiwuEventReviewWnd:GetCompetitionTemplate(data)
    if #self.m_PoolObjs > 0 then
        local len = #self.m_PoolObjs
        local go = self.m_PoolObjs[len]
        table.remove(self.m_PoolObjs, len)
        return go
    end
    local go = NGUITools.AddChild(self.m_Pool.gameObject, self.m_CompetitionTemplate)
    return go
end

function LuaStarBiwuEventReviewWnd:RecycleCompetitionTemplate(go)
    go.transform.parent = self.m_Pool.transform
    table.insert(self.m_PoolObjs, go)
end

function LuaStarBiwuEventReviewWnd:OnChooseButtonClicked(btn, textArray, imageNameArray, onClick)
    Extensions.SetLocalRotationZ(btn.transform:Find("ChooseButtonArrow"), 180)
    local t = {}
    for index,text in pairs(textArray) do
        local item=PopupMenuItemData(text,DelegateFactory.Action_int(function (idx)
            btn.Text = textArray[idx + 1]
            onClick(idx + 1)
        end),false,imageNameArray[index])
        table.insert(t, item)
    end
    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    -- CPopupMenuInfoMgr.ShowPopupMenu(array, btn.transform, AlignType.Bottom,1,DelegateFactory.Action(function()
    --     Extensions.SetLocalRotationZ(btn.transform:Find("ChooseButtonArrow"), 0)
    -- end),600,true,296)
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(array, btn.transform, CPopupMenuInfoMgrAlignType.Bottom, 1, nil, nil, DelegateFactory.Action(function ()
        Extensions.SetLocalRotationZ(btn.transform:Find("ChooseButtonArrow"), 0)
    end), 600,320)
    btn.Selected = true
end

function LuaStarBiwuEventReviewWnd:OnChooseButton1Clicked()
    self:OnChooseButtonClicked(self.m_ChooseButton1, self.m_BtnTextArray1, self.m_Btn1ImageNameArray, function (idx)
        self.m_SelectIndex1 = idx
        self:InitCompetitionEvents()
        local season = self.m_SelectIndex1ToSeasonMap[self.m_SelectIndex1]
        self.m_OpenWndBtn.gameObject:SetActive(season >= 10)
    end)
end

function LuaStarBiwuEventReviewWnd:OnChooseButton2Clicked()
    local imageNameArray = {}
    self:OnChooseButtonClicked(self.m_ChooseButton2, self.m_BtnTextArray2, imageNameArray, function (idx)
        self.m_SelectIndex2 = idx
        self:InitChooseBtns()
        self:InitCompetitionEvents()
    end)
end

function LuaStarBiwuEventReviewWnd:OnChooseButton3Clicked()
    local imageNameArray = {}
    self:OnChooseButtonClicked(self.m_ChooseButton3, self.m_BtnTextArray3, imageNameArray, function (idx)
        self.m_SelectIndex3 = idx
        self:InitCompetitionEvents()
    end)
end

function LuaStarBiwuEventReviewWnd:OnSearchBtnClicked(str)
    self:OnSearchViewValueChanged(str)
end

function LuaStarBiwuEventReviewWnd:OnSearchViewValueChanged(str)
    self.m_SearchText = str
    if cs_string.IsNullOrEmpty(self.m_SearchText) then
        self.m_SearchText = nil
    end
    self:InitCompetitionEvents()
end

function LuaStarBiwuEventReviewWnd:OpenDataAnalysisWnd()
    local data = self.m_CurrentData
    if not data then
        g_MessageMgr:ShowMessage("StarBiwuEventReviewWnd_NoneSelect")
        return
    end
    local season = self.m_SelectIndex1ToSeasonMap[self.m_SelectIndex1]
    CLuaStarBiwuMgr:OpenStarBiwuDataAnalysisWnd(data.aliasName,season)
end

function LuaStarBiwuEventReviewWnd:TestDownloadVideo()
    local data = self.m_CurrentData
    if not data then
        g_MessageMgr:ShowMessage("StarBiwuEventReviewWnd_NoneSelect")
        return
    end

    local isDownloading = CGameReplayMgr.Instance:IsFileDownloading(data.fileName)
    if isDownloading then
        --g_MessageMgr:ShowMessage("StarBiwuEventReviewWnd_IsDownloading")
        return
    end

    local hasVideo =not cs_string.IsNullOrEmpty(data.fileName)
    if not hasVideo then
        g_MessageMgr:ShowMessage("StarBiwuEventReviewWnd_NoneVideo")
        return
    end

    local msg = CommonDefs.IsInWifiNetwork and g_MessageMgr:FormatMessage("StarBiwuEventReviewWnd_WatchVideo_NeedDownload_NotWifi",self:GetCurrentVideoSize()) or g_MessageMgr:FormatMessage("StarBiwuEventReviewWnd_WatchVideo_NeedDownload")
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
        local data = self.m_CurrentData
        local fileName ,url, zip = data.fileName, data.url, data.zip
        CGameReplayMgr.Instance:DownloadFile(fileName ,url, zip)
        self:RefreshCompetitionTemplate(self.m_ShowObjs[data.id], data)
    end), nil, LocalString.GetString("下载"), nil, false)
end

function LuaStarBiwuEventReviewWnd:GetCurrentVideoSize()
    local data = self.m_CurrentData
    return SafeStringFormat3("%.2f",data.size/1000000)
end

function LuaStarBiwuEventReviewWnd:WatchVideo()
    local data = self.m_CurrentData
    Gac2Gas.RequestEnterGameVideoScene(data.watchSceneId,data.fileName)
end

function LuaStarBiwuEventReviewWnd:DeleteVideo()
    local data = self.m_CurrentData
    if not data then
        g_MessageMgr:ShowMessage("StarBiwuEventReviewWnd_NoneSelect")
        return
    end
    local fileName = data.fileName
    CGameReplayMgr.Instance:DeleteFile(fileName)
    g_MessageMgr:ShowMessage("StarBiwuEventReviewWnd_HasDeleteVideo")
    self:RefreshCompetitionTemplate(self.m_ShowObjs[data.id], data)
    self:OnSelect(data)
end
