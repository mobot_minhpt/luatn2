local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CPropertyAppearance = import "L10.Game.CPropertyAppearance"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local EnumGender = import "L10.Game.EnumGender"
local EnumClass = import "L10.Game.EnumClass"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"

LuaFightingSpiritChampionPreviewWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaFightingSpiritChampionPreviewWnd, "LevelNameLabel", "LevelNameLabel", UILabel)
RegistChildComponent(LuaFightingSpiritChampionPreviewWnd, "TeamNameLabel", "TeamNameLabel", UILabel)
RegistChildComponent(LuaFightingSpiritChampionPreviewWnd, "PlayerInfoRoot", "PlayerInfoRoot", GameObject)
RegistChildComponent(LuaFightingSpiritChampionPreviewWnd, "ShareBtn", "ShareBtn", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaFightingSpiritChampionPreviewWnd, "CloseBtn")
RegistClassMember(LuaFightingSpiritChampionPreviewWnd, "Logo")

function LuaFightingSpiritChampionPreviewWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)


    --@endregion EventBind end

	self.CloseBtn = self.transform:Find("closebutton_Texture").gameObject
	self.Logo = self.transform:Find("BgTexture/Logo").gameObject
	self.Logo:SetActive(false)
	self.transform:Find("Title"):GetComponent(typeof(UILabel)).text = LocalString.GetString("第"..DouHunTan_Setting.GetData().Season.."届太一斗魂坛")
end

function LuaFightingSpiritChampionPreviewWnd:Init()
	self.LevelNameLabel.text = CLuaFightingSpiritMgr.LevelName
	self.TeamNameLabel.text = CLuaFightingSpiritMgr.ServerName

	local id2Lv = CLuaFightingSpiritMgr.ChampionID2Lv

	local apparenceData = CLuaFightingSpiritMgr.GroupAppearanceInfo

	for i=1, 10 do
		local item = self.PlayerInfoRoot.transform:Find(tostring(i)).gameObject
		item:SetActive(false)
	end

	-- Name, Gender, Profession, appearanceProp:SaveToString(), playerId, Level
	local index = 1
	for i = 0, apparenceData.Count-1 do
		local data = apparenceData[i]

		local name = data[0]
		local gender =  CommonDefs.ConvertIntToEnum(typeof(EnumGender), tonumber(data[1]))
		local profession = CommonDefs.ConvertIntToEnum(typeof(EnumClass), tonumber(data[2]))
		local app = CreateFromClass(CPropertyAppearance)
		local playerId = data[4]
		local level = id2Lv[playerId]
		local strData = TypeAs(data[3], typeof(MakeArrayClass(System.Byte)))

		app:LoadFromString(strData, profession, gender)
		
		local item = self.PlayerInfoRoot.transform:Find(tostring(index)).gameObject
		item:SetActive(true)
		
		local modexTexture = item.transform:Find("FakeModel/ModelTexture"):GetComponent(typeof(UITexture))
		local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
		local levelLabel = item.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))

		if level then
			levelLabel.text = "lv." .. tostring(level)
		end

		nameLabel.text = name
		-- 加载模型
		local loader = LuaDefaultModelTextureLoader.Create(function (ro)
			CClientMainPlayer.LoadResource(ro, app, true, 1, 0, false, 0, false, 0, false, nil, false)
		end)
		modexTexture.mainTexture = CUIManager.CreateModelTexture("_FightingSpiritChampionPreview__".. tostring(index), loader, 180, 0, -1, 6, false,true,1)

		index = index + 1
	end

	self.m_Count = index - 1
end

function LuaFightingSpiritChampionPreviewWnd:OnDisable()
	for i=1, self.m_Count do
		CUIManager.DestroyModelTexture("_FightingSpiritChampionPreview__".. tostring(i))
	end
end

--@region UIEvent

function LuaFightingSpiritChampionPreviewWnd:OnShareBtnClick()
	self.ShareBtn:SetActive(false)
	self.CloseBtn:SetActive(false)
	self.Logo:SetActive(true)
	CUIManager.SetUITop(CLuaUIResources.FightingSpiritChampionPreviewWnd)
	CUICommonDef.CaptureScreen("screenshot", true, false, nil, DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
		ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
		self.ShareBtn:SetActive(true)
		self.CloseBtn:SetActive(true)
		self.Logo:SetActive(false)
		CUIManager.ResetUITop(CLuaUIResources.FightingSpiritChampionPreviewWnd)
	end))
end

--@endregion UIEvent

