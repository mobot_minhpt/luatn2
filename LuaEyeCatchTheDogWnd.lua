local CUIClipTexture = import "CUIClipTexture"
local TouchPhase = import "UnityEngine.TouchPhase"
local CMainCamera = import "L10.Engine.CMainCamera"
local Physics = import "UnityEngine.Physics"
local LayerDefine = import "L10.Engine.LayerDefine"
local UIPanel = import "UIPanel"
local Object = import "System.Object"
local CRenderObject = import "L10.Engine.CRenderObject"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientNpc = import "L10.Game.CClientNpc"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CScene = import "L10.Game.CScene"
local CTeamFollowMgr = import "L10.Game.CTeamFollowMgr"

LuaEyeCatchTheDogWnd = class()
RegistChildComponent(LuaEyeCatchTheDogWnd,"m_ClipTexture","ClipTexture", CUIClipTexture)
RegistChildComponent(LuaEyeCatchTheDogWnd,"m_Region","Region", UIWidget)
RegistChildComponent(LuaEyeCatchTheDogWnd,"m_FindFx","FindFx", CUIFx)
RegistChildComponent(LuaEyeCatchTheDogWnd,"m_EyeFx","EyeFx", CUIFx)
RegistChildComponent(LuaEyeCatchTheDogWnd,"m_Btn","Btn", GameObject)
RegistChildComponent(LuaEyeCatchTheDogWnd,"m_Anchor","Anchor", GameObject)
RegistChildComponent(LuaEyeCatchTheDogWnd,"m_CloseButton","CloseButton", GameObject)

RegistClassMember(LuaEyeCatchTheDogWnd, "m_IsDragging")
RegistClassMember(LuaEyeCatchTheDogWnd, "m_LastDragPos")
RegistClassMember(LuaEyeCatchTheDogWnd, "m_FingerIndex")
RegistClassMember(LuaEyeCatchTheDogWnd, "m_TargetTemplateId")
RegistClassMember(LuaEyeCatchTheDogWnd, "m_CloseWndTick")
RegistClassMember(LuaEyeCatchTheDogWnd, "m_HideExceptList")
RegistClassMember(LuaEyeCatchTheDogWnd, "m_bFinishedTask")

function LuaEyeCatchTheDogWnd:Init()
    self.gameObject:GetComponent(typeof(UIPanel)).IgnoreIphoneXMargin = true

    self.m_ClipTexture:InitRegion(0, 0, self.m_Region.width, self.m_Region.height)
    UIEventListener.Get(self.m_Region.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(go)
        self:OnDragStart(go)
    end)
    UIEventListener.Get(self.m_Btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnClick(go)
    end)
    UIEventListener.Get(self.m_CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:Close(go)
    end)
    self.m_TargetTemplateId = CLuaTaskMgr.m_EyeCatchTheDog_NPCTemplateId
    self.m_EyeFx:LoadFx("Fx/UI/Prefab/UI_tianyanzhuagou_liuguang.prefab")
end

function LuaEyeCatchTheDogWnd:Close(go)
    if not self.m_bFinishedTask then
        CTeamFollowMgr.Inst:RequestCancelTeamFollow()
        Gac2Gas.RequestLeavePlay()
    end
end

function LuaEyeCatchTheDogWnd:Update()
    if nil == self.m_LastDragPos then
        self.m_LastDragPos = Vector3.zero
    end
    if not self.m_IsDragging then
        return
    end
    if CommonDefs.IsInMobileDevice() then
        for i = 0,Input.touchCount - 1 do
            local touch = Input.GetTouch(i)
            if touch.fingerId == self.m_FingerIndex then
                if touch.phase ~= TouchPhase.Ended and touch.phase ~= TouchPhase.Canceled then
                    if touch.phase ~= TouchPhase.Stationary then
                        self.m_LastDragPos = Vector3(touch.position.x, touch.position.y, 0)
                        self.m_Region.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_LastDragPos)
                        if self:CheckTheCrosshair() then
                            self.m_FindFx:LoadFx("Fx/UI/Prefab/UI_tianyanzhuagou_zhaodao.prefab")
                        else
                            self.m_FindFx:DestroyFx()
                        end
                    end
                    return
                else
                    break
                end
            end
        end
    else
        if Input.GetMouseButton(0) then
            self.m_Region.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
            self.m_LastDragPos = Input.mousePosition
            if self:CheckTheCrosshair() then
                self.m_FindFx:LoadFx("Fx/UI/Prefab/UI_tianyanzhuagou_zhaodao.prefab")
            else
                self.m_FindFx:DestroyFx()
            end
            if not Input.GetMouseButtonUp(0) then
                return
            end
        end
    end
    self.m_IsDragging = false
    self.m_FingerIndex = -1
end

function LuaEyeCatchTheDogWnd:OnEnable()
    local hideExcept = { "MiddleNoticeCenter" }
    self.m_HideExceptList = Table2List(hideExcept, MakeGenericClass(List, cs_string))
    self:InitCamera()
    CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, self.m_HideExceptList,  false, true)
    CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, self.m_HideExceptList,  false, true)
end

function LuaEyeCatchTheDogWnd:OnDisable()
    self:EndMoveCamera()
    CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, self.m_HideExceptList,  false, true)
    CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, self.m_HideExceptList,  false, true)
    self:CancelCloseWndTick()
end

function LuaEyeCatchTheDogWnd:InitCamera()
    if CameraFollow.Inst then
        local cameraData = ZhuJueJuQing_Setting.GetData().EyeCatchTheDogWnd_Camera
        CameraFollow.Inst:BeginAICamera(Vector3(0, 0, 0), Vector3(0, 0, 0), 3600, false)
        CameraFollow.Inst:AICameraMoveTo(
                Vector3(cameraData[0], cameraData[1], cameraData[2]),
                Vector3(cameraData[3], cameraData[4], cameraData[5]),0.1)
    end
end

function LuaEyeCatchTheDogWnd:EndMoveCamera()
    CameraFollow.Inst:EndAICamera()
end

function LuaEyeCatchTheDogWnd:OnDragStart(go)
    self.m_IsDragging = true
    self.m_LastDragPos = Input.mousePosition
    if CommonDefs.IsInMobileDevice() then
        self.m_LastDragPos = Input.GetTouch(0).position
        self.m_FingerIndex = Input.GetTouch(0).fingerId
    end
end

function LuaEyeCatchTheDogWnd:OnClick(go)
    if not CMainCamera.Main then return end

    if self:CheckTheCrosshair() then
        local empty = CreateFromClass(MakeGenericClass(List, Object))
        if CameraFollow.Inst then
            local cameraData = ZhuJueJuQing_Setting.GetData().EyeCatchTheDogWnd_Camera
            CameraFollow.Inst:AICameraMoveTo(
                    Vector3(cameraData[6], cameraData[7], cameraData[8]),
                    Vector3(cameraData[9], cameraData[10], cameraData[11]),4)
            self.m_Anchor.gameObject:SetActive(false)
        end
        self.m_CloseWndTick = RegisterTickOnce(function ()
            self.m_bFinishedTask = true
            Gac2Gas.FinishClientTaskEvent(CLuaTaskMgr.m_EyeCatchTheDog_TaskId,"EyeCatchTheDog",MsgPackImpl.pack(empty))
        end,5000)
        return
    end

    g_MessageMgr:ShowMessage("EyeCatchTheDogWnd_Failed")
end

function LuaEyeCatchTheDogWnd:CancelCloseWndTick()
    if self.m_CloseWndTick then
        UnRegisterTick(self.m_CloseWndTick)
        self.m_CloseWndTick = nil
    end
end

function LuaEyeCatchTheDogWnd:CheckTheCrosshair()
    if not CMainCamera.Main then return false end
    if nil == self.m_LastDragPos then
        self.m_LastDragPos = Vector3.zero
    end
    local screenPos = self.m_LastDragPos
    local ray = CMainCamera.Main:ScreenPointToRay(screenPos)
    local dist = CMainCamera.Main.farClipPlane - CMainCamera.Main.nearClipPlane
    local hits = Physics.RaycastAll(ray, dist, bit.lshift(1,LayerDefine.NPC))
    for i = 0, hits.Length - 1 do
        local collider = hits[i].collider.gameObject
        local p = collider.transform.parent
        if p then
            local ro = p.gameObject:GetComponent(typeof(CRenderObject))
            if ro and CClientObjectMgr.Inst then
                local co = CClientObjectMgr.Inst:GetObject(ro)
                if co then
                    local npc = TypeAs(CClientObjectMgr.Inst:GetObject(co.EngineId), typeof(CClientNpc))
                    if self.m_TargetTemplateId == npc.TemplateId then
                        return true
                    end
                end
            end
        end
    end
    return false
end

LuaEyeCatchTheDogMgr = class()
LuaEyeCatchTheDogMgr.OnMainPlayerCreated = function()
    if CScene.MainScene then
        local gamePlayId=CScene.MainScene.GamePlayDesignId
        if gamePlayId == 51101465 then--天眼抓狗玩法
            local npcTemplateId = ZhuJueJuQing_Setting.GetData().EyeCatchTheDogWnd_DogNpcTemplateID
            CLuaTaskMgr.ShowEyeCatchTheDogWnd(22204534, npcTemplateId)
        end
    end
end