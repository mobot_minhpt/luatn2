local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local EnumGender = import "L10.Game.EnumGender"
local UISprite = import "UISprite"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaAppearanceWingSubview = class()

RegistChildComponent(LuaAppearanceWingSubview,"m_WingItem", "CommonItemCell", GameObject)
RegistChildComponent(LuaAppearanceWingSubview,"m_ItemDisplay", "AppearanceCommonItemDisplayView", CCommonLuaScript)
RegistChildComponent(LuaAppearanceWingSubview,"m_SwitchButton", "CommonHeaderSwitch", CButton)
RegistChildComponent(LuaAppearanceWingSubview,"m_ContentGrid", "ContentGrid", UIGrid)
RegistChildComponent(LuaAppearanceWingSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)

RegistClassMember(LuaAppearanceWingSubview, "m_AllWings")
RegistClassMember(LuaAppearanceWingSubview, "m_SelectedDataId")

function LuaAppearanceWingSubview:Awake()
end

function LuaAppearanceWingSubview:Init()
    self.m_SwitchButton.gameObject:SetActive(true)
    --组件共用，每次需要重新关联响应方法
    self.m_SwitchButton.Text = LocalString.GetString("翅膀开关")
    UIEventListener.Get(self.m_SwitchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnSwitchButtonClick() end)
    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceWingSubview:LoadData()
    self.m_AllWings = LuaAppearancePreviewMgr:GetAllWingInfo(false)
    Extensions.RemoveAllChildren(self.m_ContentGrid.transform)
    self.m_ContentGrid.gameObject:SetActive(true)

    for i = 1, # self.m_AllWings do
        local child = CUICommonDef.AddChild(self.m_ContentGrid.gameObject, self.m_WingItem)
        child:SetActive(true)
        self:InitItem(child, self.m_AllWings[i])
    end
    self.m_ContentGrid:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceWingSubview:SetDefaultSelection()
    self.m_SelectedDataId = LuaAppearancePreviewMgr:IsShowWing() and LuaAppearancePreviewMgr:GetCurrentInUseWing() or 0
end

function LuaAppearanceWingSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end

    local childCount = self.m_ContentGrid.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        local appearanceData = self.m_AllWings[i+1]
        if appearanceData.id == self.m_SelectedDataId then
            self:OnItemClick(childGo)
            break
        end
    end
end

function LuaAppearanceWingSubview:InitItem(itemGo, appearanceData)
    local iconTexture = itemGo.transform:GetComponent(typeof(CUITexture))
    local disabledGo = itemGo.transform:Find("Disabled").gameObject
    local lockedGo = itemGo.transform:Find("Locked").gameObject
    local cornerGo = itemGo.transform:Find("Corner").gameObject
    local selectedGo = itemGo.transform:Find("Selected").gameObject
    
    if CClientMainPlayer.Inst then
        iconTexture:LoadMaterial(CClientMainPlayer.Inst.Gender == EnumGender.Male and appearanceData.icon or appearanceData.femaleIcon)
        lockedGo:SetActive(not LuaAppearancePreviewMgr:MainPlayerHasWing(appearanceData.id))
    else
        iconTexture:Clear()
        lockedGo:SetActive(false)
    end
    disabledGo:SetActive(lockedGo.activeSelf)
    cornerGo:SetActive(LuaAppearancePreviewMgr:IsCurrentInUseWing(appearanceData.id))
    selectedGo:SetActive(self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id)

    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go)
    end)
end

function LuaAppearanceWingSubview:OnItemClick(go)
    local childCount = self.m_ContentGrid.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:Find("Selected").gameObject:SetActive(true)
            self.m_SelectedDataId = self.m_AllWings[i+1].id
            if not LuaAppearancePreviewMgr:IsShowWing() then
                g_MessageMgr:ShowMessage("Appearance_Preview_Hide_Wing_Tip")
            end
        else
            childGo.transform:Find("Selected").gameObject:SetActive(false)
        end
    end
    self:UpdateItemDisplay()
    g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerWing", self.m_SelectedDataId)
end

function LuaAppearanceWingSubview:UpdateItemDisplay()
    self.m_ItemDisplay.gameObject:SetActive(true)
    self.m_SwitchButton.Selected = LuaAppearancePreviewMgr:IsShowWing()
    local wingId = self.m_SelectedDataId and self.m_SelectedDataId or 0
    if wingId==0 then
        self.m_ItemDisplay:Clear()
        return
    end
    local appearanceData = nil
    for i=1,#self.m_AllWings do
        if self.m_AllWings[i].id == self.m_SelectedDataId then
            appearanceData = self.m_AllWings[i]
            break
        end
    end
    
    local icon = appearanceData.icon
    local disabled = true
    local locked = true
    local buttonTbl = {}
    if CClientMainPlayer.Inst then
        icon = CClientMainPlayer.Inst.Gender == EnumGender.Male and appearanceData.icon or appearanceData.femaleIcon
        local exist = LuaAppearancePreviewMgr:MainPlayerHasWing(appearanceData.id)
        local inUse = LuaAppearancePreviewMgr:IsCurrentInUseWing(appearanceData.id)
        disabled = not exist
        locked = not exist
        if exist and not inUse then
            table.insert(buttonTbl, {text=LocalString.GetString("更换"), isYellow=false, action=function(go) self:OnApplyButtonClick() end})
        elseif exist and inUse then
            table.insert(buttonTbl, {text=LocalString.GetString("脱下"), isYellow=false, action=function(go) self:OnRemoveButtonClick() end})
        end
    end

    self.m_ItemDisplay:Init(icon, appearanceData.colorName, appearanceData.condition, "", false, disabled, locked, buttonTbl)
end

function LuaAppearanceWingSubview:OnApplyButtonClick()
    if self.m_SelectedDataId and self.m_SelectedDataId>0 then
        LuaAppearancePreviewMgr:RequestSetWing(self.m_SelectedDataId)
    end
end

function LuaAppearanceWingSubview:OnRemoveButtonClick()
    LuaAppearancePreviewMgr:RequestSetWing(0)
end

function LuaAppearanceWingSubview:OnSwitchButtonClick()
    self.m_SwitchButton.Selected = not self.m_SwitchButton.Selected
    local bShow = LuaAppearancePreviewMgr:IsShowWing()
    LuaAppearancePreviewMgr:ChangeWingVisibility(not bShow) --等等请求返回再刷新
end

function  LuaAppearanceWingSubview:OnEnable()
    g_ScriptEvent:AddListener("WingFxUpdate", self, "OnWingFxUpdate")
    g_ScriptEvent:AddListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:AddListener("AppearancePropertySettingInfoReturn",self,"OnAppearancePropertySettingInfoReturn") --开关切换
end

function  LuaAppearanceWingSubview:OnDisable()
    g_ScriptEvent:RemoveListener("WingFxUpdate", self, "OnWingFxUpdate")
    g_ScriptEvent:RemoveListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:RemoveListener("AppearancePropertySettingInfoReturn",self,"OnAppearancePropertySettingInfoReturn")
end

--翅膀切换
function LuaAppearanceWingSubview:OnWingFxUpdate(args)
    local engineId = args[0]
    local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if TypeIs(obj, typeof(CClientMainPlayer)) then
        self:SetDefaultSelection()
		self:LoadData()
	end
end
--仙凡切换
function LuaAppearanceWingSubview:SyncMainPlayerAppearancePropUpdate()
    self:InitSelection()
end
--开关切换
function LuaAppearanceWingSubview:OnAppearancePropertySettingInfoReturn(args)
    self:SetDefaultSelection()
    self:InitSelection()
end
