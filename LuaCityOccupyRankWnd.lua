require("common/common_include")

local UIGrid = import "UIGrid"
local Extensions = import "Extensions"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CRankData = import "L10.UI.CRankData"
local NGUITools = import "NGUITools"
local Constants = import "L10.Game.Constants"

CLuaCityOccupyRankWnd = class()
CLuaCityOccupyRankWnd.Path = "ui/citywar/LuaCityOccupyRankWnd"    

RegistClassMember(CLuaCityOccupyRankWnd, "m_MyGuildObj")

function CLuaCityOccupyRankWnd:Awake( ... )
	Gac2Gas.QueryRank(45100003)
	self.m_MyGuildObj = self.transform:Find("Rank/MyGuild").gameObject
	self.m_MyGuildObj:SetActive(false)
end

function CLuaCityOccupyRankWnd:Init()
	
end

function CLuaCityOccupyRankWnd:OnRankDataReady( ... )
	if CLuaRankData.m_CurRankId ~= 45100003 then return end
	local templateObj = self.transform:Find("Rank/AdvView/Pool/Template").gameObject
	templateObj:SetActive(false)
	local grid = self.transform:Find("Rank/AdvView/Scroll View/Grid"):GetComponent(typeof(UIGrid))
	self.m_MyGuildObj:SetActive(true)
	self:InitItem(self.m_MyGuildObj.transform, CRankData.Inst.MainPlayerRankInfo, true, true)
	Extensions.RemoveAllChildren(grid.transform)
	for i = 0, CRankData.Inst.RankList.Count - 1 do
		local obj = NGUITools.AddChild(grid.gameObject, templateObj)
		obj:SetActive(true)
		self:InitItem(obj.transform, CRankData.Inst.RankList[i], false, i % 2 == 0)
	end
	grid:Reposition()
end

function CLuaCityOccupyRankWnd:InitItem(rootTrans, data, isMyGuild, isOdd)
	local extraUD = data.extraData
	local extra = extraUD and MsgPackImpl.unpack(extraUD)
	local progress = extra[0]	--金元洞天占领时间
	local progress2 = extra[1]	--四级领土占领时间

	rootTrans:Find("GuildNameLabel"):GetComponent(typeof(UILabel)).text = data.Guild_Name
	rootTrans:Find("ServerNameLabel"):GetComponent(typeof(UILabel)).text = data.serverName
	rootTrans:Find("GuildGradeLabel"):GetComponent(typeof(UILabel)).text = data.Level
	rootTrans:Find("GuildPeopleLabel"):GetComponent(typeof(UILabel)).text = data.Member
	rootTrans:Find("ProgressLabel2"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%d时%d分"), math.floor(progress2 / 3600), math.floor(progress2 % 3600 / 60))
	rootTrans:Find("ProgressLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%d时%d分"), math.floor(progress / 3600), math.floor(progress % 3600 / 60))

	if not isMyGuild then
		rootTrans:GetComponent(typeof(UISprite)).spriteName = isOdd and Constants.NewOddBgSprite or Constants.NewEvenBgSprite
	end

	local rankSprite = rootTrans:Find("RankSprite"):GetComponent(typeof(UISprite))
	rankSprite.gameObject:SetActive(data.Rank <= 3 and data.Rank >= 1)
	if data.Rank > 3 then
		rootTrans:Find("RankLabel"):GetComponent(typeof(UILabel)).text = data.Rank
	elseif data.Rank == 0 then
		rootTrans:Find("RankLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("未上榜")
	else
		local spriteNames = {"Rank_No.1", "Rank_No.2png", "Rank_No.3png"}
		rankSprite.spriteName = spriteNames[data.Rank]
	end
end

function CLuaCityOccupyRankWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function CLuaCityOccupyRankWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end