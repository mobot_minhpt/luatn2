require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local CPayMgr=import "L10.Game.CPayMgr"
local Mall_LingYuMallLimit=import "L10.Game.Mall_LingYuMallLimit"
local Mall_ItemPreview=import "L10.Game.Mall_ItemPreview"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local Item_Item=import "L10.Game.Item_Item"
local CShopMallMgr=import "L10.UI.CShopMallMgr"


--时装礼包
CLuaFashionGiftDetailWnd=class()
RegistClassMember(CLuaFashionGiftDetailWnd,"m_Template")
RegistClassMember(CLuaFashionGiftDetailWnd,"m_BuyButton")


function CLuaFashionGiftDetailWnd:Init()
    self.m_Template=LuaGameObject.GetChildNoGC(self.transform,"Template").gameObject
    self.m_Template:SetActive(false)

    self.m_BuyButton=LuaGameObject.GetChildNoGC(self.transform,"OKButton").gameObject
    local function OnClickBuyButton(go)
        local id=CLuaFashionGiftMgr.SelectedGiftId
        local data=Mall_LingYuMallLimit.GetData(id)
        if data then
            local pid=CPayMgr.Inst:PIDConverter(data.RmbPID)
            CPayMgr.Inst:BuyProduct(pid,0)
        end
    end
    CommonDefs.AddOnClickListener(self.m_BuyButton,DelegateFactory.Action_GameObject(OnClickBuyButton),false)
    self:OnUpdateMallLimit()

    local function OnPreviewItemClicked(go)
        local index=go.transform:GetSiblingIndex()
        local id=CLuaFashionGiftMgr.SelectedGiftId
        local data=Mall_LingYuMallLimit.GetData(id)
        if data then
            local itemId=Mall_ItemPreview.GetData(data.ItemPreview[index]).ItemId
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false,nil,AlignType.ScreenRight,0,0,0,0)
        end
    end

    local itemGrid=LuaGameObject.GetChildNoGC(self.transform,"Grid").grid
    local id=CLuaFashionGiftMgr.SelectedGiftId
    local designData=Mall_LingYuMallLimit.GetData(id)
    local items=designData.ItemPreview
    local len=items.Length
    for i=0,len-1 do
        local previewItem=NGUITools.AddChild(itemGrid.gameObject,self.m_Template)
        previewItem:SetActive(true)
        self:InitPreviewItem(previewItem,items[i])
        CommonDefs.AddOnClickListener(previewItem,DelegateFactory.Action_GameObject(OnPreviewItemClicked),false)
    end
    itemGrid:Reposition()


    LuaGameObject.GetChildNoGC(self.transform,"PriceLabel").label.text=SafeStringFormat3(LocalString.GetString("售价   %d元"),designData.Jade)
end
function  CLuaFashionGiftDetailWnd:InitPreviewItem( go,id )
    local designData=Mall_ItemPreview.GetData(id)
    LuaGameObject.GetChildNoGC(go.transform,"DescLabel").label.text=tostring(designData.Desc)
    if designData.RandomTag>0 then
        LuaGameObject.GetChildNoGC(go.transform,"Random").gameObject:SetActive(true)
    else
        LuaGameObject.GetChildNoGC(go.transform,"Random").gameObject:SetActive(false)
    end
    local itemData=Item_Item.GetData(designData.ItemId)
    LuaGameObject.GetChildNoGC(go.transform,"IconTexture").cTexture:LoadMaterial(itemData.Icon,false)
end

function CLuaFashionGiftDetailWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateMallLimit", self, "OnUpdateMallLimit")
end
function CLuaFashionGiftDetailWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateMallLimit", self, "OnUpdateMallLimit")
end
function CLuaFashionGiftDetailWnd:OnUpdateMallLimit()
    local id=CLuaFashionGiftMgr.SelectedGiftId
    local limitCount=CShopMallMgr.Inst:GetLimitCount(id)
    if limitCount>0 then
        CUICommonDef.SetActive(self.m_BuyButton,true,true)
    else
        CUICommonDef.SetActive(self.m_BuyButton,false,true)
    end
end
return CLuaFashionGiftDetailWnd
