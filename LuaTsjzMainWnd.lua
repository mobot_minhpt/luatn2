--@region import
local GameObject                = import "UnityEngine.GameObject"
local CScheduleMgr              = import "L10.Game.CScheduleMgr"
local CommonDefs                = import "L10.Game.CommonDefs"
local CUITexture				= import "L10.UI.CUITexture"
local CUIManager                = import "L10.UI.CUIManager"
local QnTabButton               = import "L10.UI.QnTabButton"
local DelegateFactory		    = import "DelegateFactory"
local UIEventListener		    = import "UIEventListener"
local UILabel                   = import "UILabel"
local LocalString               = import "LocalString"
local LuaGameObject             = import "LuaGameObject"
local UISlider                  = import "UISlider"
--@endregion

--天朔卷轴主界面
LuaTsjzMainWnd = class()

--@region Regist

--RegistChildComponent

RegistChildComponent(LuaTsjzMainWnd, "LvLb",		    UILabel)
RegistChildComponent(LuaTsjzMainWnd, "LvBg",		    CUITexture)

RegistChildComponent(LuaTsjzMainWnd, "NeedExpLb",		UILabel)
RegistChildComponent(LuaTsjzMainWnd, "ExpSlider",	    UISlider)
RegistChildComponent(LuaTsjzMainWnd, "HelpBtn",	        GameObject)
RegistChildComponent(LuaTsjzMainWnd, "BuyAdvPassBtn",	GameObject)
RegistChildComponent(LuaTsjzMainWnd, "RewardBtn",	    GameObject)
RegistChildComponent(LuaTsjzMainWnd, "TaskBtn",	        GameObject)
RegistChildComponent(LuaTsjzMainWnd, "RewardTabPanel",	GameObject)
RegistChildComponent(LuaTsjzMainWnd, "TaskTabPanel",	GameObject)
RegistChildComponent(LuaTsjzMainWnd, "ExtSprite",	    GameObject)
RegistChildComponent(LuaTsjzMainWnd, "BuyLevelBtn",	    GameObject)

--RegistClassMember
RegistClassMember(LuaTsjzMainWnd,  "CurTabIndex")

--@endregion

function LuaTsjzMainWnd:Awake()

    self.CurTabIndex = -1

    UIEventListener.Get(self.HelpBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnHelpBtnClick()
    end)

    UIEventListener.Get(self.BuyAdvPassBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyAdvPassBtnClick()
    end)

    UIEventListener.Get(self.BuyLevelBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyLevelBtnClick()
    end)

    UIEventListener.Get(self.RewardBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSelectTab(0)
    end)

    UIEventListener.Get(self.TaskBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSelectTab(1)
    end)
    self:OnSelectTab(0)

    g_ScriptEvent:AddListener("OnTsjzDataChange", self, "OnTsjzDataChange")
end

function LuaTsjzMainWnd:OnDestroy()
    LuaHanJiaMgr.TcjhMainData = nil
    g_ScriptEvent:RemoveListener("OnTsjzDataChange", self, "OnTsjzDataChange")
end

function LuaTsjzMainWnd:Init()
    self:OnTsjzDataChange()
end

function LuaTsjzMainWnd:OnTsjzDataChange()

    local cfg = LuaHanJiaMgr.TcjhMainData.Cfg
    local lv = LuaHanJiaMgr.TcjhMainData.Lv

    local lvexp = self:GetLvExp(lv)
    local curexp =  LuaHanJiaMgr.TcjhMainData.Exp - lvexp  --为当前等级下的经验值，不会超过当前等级所需经验的上上限
    local passtype = LuaHanJiaMgr.TcjhMainData.PassType --0 免费通行证, 1 高级通行证, 2 高级通行证豪华版,3全买

    local curcfg = cfg[lv]
    
    if curcfg then
        self.LvLb.text = tostring(lv)
        --self.LvBg:LoadMaterial(curcfg.Icon)
        if lv >= LuaHanJiaMgr.TcjhMainData.MaxLv then
            self.NeedExpLb.text = LocalString.GetString("画轴已满级")
            self.ExpSlider.value = 1
            CUICommonDef.SetActive(self.BuyLevelBtn,false,true)
        else
            local nextcfg = cfg[lv+1]
            local needexp = nextcfg.NeedJiuQi - curexp
            self.NeedExpLb.text = LocalString.GetString("升至下级还需")..needexp
            self.ExpSlider.value = curexp / nextcfg.NeedJiuQi
            CUICommonDef.SetActive(self.BuyLevelBtn,true,true)
        end
    end

    self.BuyLevelBtn:SetActive(passtype~=0)
    self.BuyAdvPassBtn:SetActive(passtype~=3)
    
    self.ExtSprite:SetActive(passtype == 2 or passtype == 3)--豪华酒壶

    local fx = LuaGameObject.GetChildNoGC(self.BuyAdvPassBtn.transform,"Fx").uiFx
    fx.Visible = passtype ~= 0

    local alertgo1 =  LuaGameObject.GetChildNoGC(self.RewardBtn.transform, "AlertSprite").gameObject
    local alertgo2 =  LuaGameObject.GetChildNoGC(self.TaskBtn.transform, "AlertSprite").gameObject

    local alert1 = self:HaveReward()
    local alert2 = self:HaveTaskReward()
    alertgo1:SetActive(alert1)
    alertgo2:SetActive(alert2)
    if alert1 or alert2 then
        CScheduleMgr.Inst:SetAlertState(42010102,CScheduleMgr.EnumAlertState.Show,true)
    else
        CScheduleMgr.Inst:SetAlertState(42010102,CScheduleMgr.EnumAlertState.Hide,true)
    end
end

function LuaTsjzMainWnd:HaveTaskReward()
    local tasks = LuaHanJiaMgr.TcjhMainData.TaskData
    for i=1,#tasks do
        local cfg = HanJia2022_TianShuoTask.GetData(tasks[i].TaskID)
        if tasks[i].IsRewarded == false then 
            if cfg.Target == 0 or cfg.Target <= tasks[i].Progress then
                return true 
            end
        end
    end
    return false
end

function LuaTsjzMainWnd:HaveReward()
    local lv = LuaHanJiaMgr.TcjhMainData.Lv
    local lvlimit = LuaHanJiaMgr.TcjhMainData.MaxLv
    local passtype = LuaHanJiaMgr.TcjhMainData.PassType
    local nLv = LuaHanJiaMgr.TcjhMainData.RewardNmlLv
    local aLv = LuaHanJiaMgr.TcjhMainData.RewardAdvLv
    local aLv2 = LuaHanJiaMgr.TcjhMainData.RewardAdvLv2
    if nLv < lv and nLv < lvlimit  then return true end
    if (passtype == 1 or passtype ==3) and aLv < lv then return true end
    if (passtype == 2 or passtype ==3) and aLv2 < lv then return true end
    return false
end

--[[
    @desc: 获得从等级1到某个等级所需的经验值
    author:{author}
    time:2020-11-03 10:21:11
    --@lv: 
    @return:
]]
function LuaTsjzMainWnd:GetLvExp(lv)
    local res = 0
    local cfg = LuaHanJiaMgr.TcjhMainData.Cfg
    local min = math.min(#cfg,lv)
    for i = 1, min do
        res  = res + cfg[i].NeedJiuQi
    end
    return res
end



--@region UIEvent

--[[
    @desc: 页签切换
    author:{author}
    time:2020-10-30 10:49:43
    --@tab: 0:奖励页签；1:任务页签
    @return:
]]
function LuaTsjzMainWnd:OnSelectTab(tab)
    if self.CurTabIndex == tab then return end
    self.CurTabIndex = tab
    self.RewardTabPanel:SetActive(self.CurTabIndex == 0)
    self.TaskTabPanel:SetActive(self.CurTabIndex == 1)

    local rewardbtn = CommonDefs.GetComponent_GameObject_Type(self.RewardBtn,typeof(QnTabButton))
    local taskbtn = CommonDefs.GetComponent_GameObject_Type(self.TaskBtn,typeof(QnTabButton))
    rewardbtn.Selected = self.CurTabIndex == 0
    taskbtn.Selected = self.CurTabIndex == 1
end

function LuaTsjzMainWnd:OnHelpBtnClick()
    --local lvlimit = LuaHanJiaMgr.TcjhMainData.MaxLv
    g_MessageMgr:ShowMessage("TSJZ_Rule")--Msg:TSJZ_Rule
end

function LuaTsjzMainWnd:OnBuyAdvPassBtnClick()
    local passtype = LuaHanJiaMgr.TcjhMainData.PassType
    if passtype ~= 3 then
        CUIManager.ShowUI(CLuaUIResources.TsjzBuyAdvPassWnd)
    end
end

function LuaTsjzMainWnd:OnBuyLevelBtnClick()
    CUIManager.ShowUI(CLuaUIResources.TcjhBuyLevelWnd)
end

--@endregion
