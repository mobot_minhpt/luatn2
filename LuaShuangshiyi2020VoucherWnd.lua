local Item_Item=import "L10.Game.Item_Item"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local UILabel = import "UILabel"
local LocalString = import "LocalString"
local CommonDefs = import "L10.Game.CommonDefs"

CLuaShuangshiyi2020VoucherWnd=class()
RegistClassMember(CLuaShuangshiyi2020VoucherWnd,"m_YuanbaoReward")
RegistClassMember(CLuaShuangshiyi2020VoucherWnd,"m_LingyuReward")
RegistClassMember(CLuaShuangshiyi2020VoucherWnd,"m_CommitBtn")
RegistClassMember(CLuaShuangshiyi2020VoucherWnd,"m_ConcelBtn")
RegistClassMember(CLuaShuangshiyi2020VoucherWnd,"m_RewardContent")
RegistClassMember(CLuaShuangshiyi2020VoucherWnd,"m_ExchangeCount")
RegistClassMember(CLuaShuangshiyi2020VoucherWnd,"m_VoucherList")
RegistClassMember(CLuaShuangshiyi2020VoucherWnd,"m_IsOnlyYuanbaoReward")

function CLuaShuangshiyi2020VoucherWnd:InitComponents()
    self.m_YuanbaoReward = self.transform:Find("Anchor/Reward/Yuanbao").gameObject
    self.m_LingyuReward = self.transform:Find("Anchor/Reward/Lingyu").gameObject
    self.m_CommitBtn = self.transform:Find("Anchor/Bottom/CommitBtn").gameObject
    self.m_ConcelBtn = self.transform:Find("Anchor/Bottom/ConcelBtn").gameObject
    self.m_RewardContent = self.transform:Find("Anchor/Content/ScrollView/RewardContent"):GetComponent(typeof(UILabel))
    self.m_ExchangeCount = self.transform:Find("Anchor/Bottom/ExchangeCount"):GetComponent(typeof(UILabel))

    if CommonDefs.IS_HMT_CLIENT then
        self.transform:Find("Anchor/Reward/Yuanbao/Title"):GetComponent(typeof(UILabel)).text = LocalString.GetString("暂时没有可兑换的周年庆礼包，仅能获得元宝奖励")
    elseif CommonDefs.IS_VN_CLIENT then
        self.transform:Find("Anchor/Reward/Yuanbao/Title"):GetComponent(typeof(UILabel)).text = LocalString.GetString("暂时没有可兑换的双十礼包，仅能获得元宝奖励")
    end
end

function CLuaShuangshiyi2020VoucherWnd:Init()
    self:InitComponents()

    UIEventListener.Get(self.m_CommitBtn).onClick = DelegateFactory.VoidDelegate(
        function(go)
            if self.m_IsOnlyYuanbaoReward then
                local text = g_MessageMgr:FormatMessage("CHANGXIANGQUAN_USE_CONFIRM")
                MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(
                    function()
                        Gac2Gas:RequestUseDouble11Voucher()
                        CUIManager.CloseUI(CLuaUIResources.Shuangshiyi2020VoucherWnd)
                    end), nil, nil, nil, false)
            else
                Gac2Gas:RequestUseDouble11Voucher()
                CUIManager.CloseUI(CLuaUIResources.Shuangshiyi2020VoucherWnd)
            end
        end)

    UIEventListener.Get(self.m_ConcelBtn).onClick = DelegateFactory.VoidDelegate(
        function(go)
            CUIManager.CloseUI(CLuaUIResources.Shuangshiyi2020VoucherWnd)
        end)

    local exChangeTime = Double11_Setting.GetData().VoucherMaxUseTime - CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eShuangshiyi2020Voucher)
    self.m_ExchangeCount.text = tostring(exChangeTime)

    if CLuaShuangshiyi2020Mgr.pList and CLuaShuangshiyi2020Mgr.vList then
        self:Double11VouchersInfo(CLuaShuangshiyi2020Mgr.pList, CLuaShuangshiyi2020Mgr.vList)
    else
        CUIManager.CloseUI(CLuaUIResources.Shuangshiyi2020VoucherWnd)
    end

    if CUIManager.IsLoaded(CIndirectUIResources.ItemInfoWnd) then
        CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
    end
end


function CLuaShuangshiyi2020VoucherWnd:Double11VouchersInfo(pList, vList)
    self.m_VoucherList = {}

    local contentStr = LocalString.GetString("[ACF8FF]双十一礼包购买:\n")
    if CommonDefs.IS_HMT_CLIENT then
        contentStr = LocalString.GetString("[ACF8FF]周年庆礼包购买:\n")
    elseif CommonDefs.IS_VN_CLIENT then
        contentStr = LocalString.GetString("[ACF8FF]双十礼包购买:\n")
    end
    local formatStr = LocalString.GetString("[FFFE91]%s：[FFFFFF]奖励%d绑定灵玉（购买[FFFE91]%d[FFFFFF], 兑换[FFFE91]%d[FFFFFF]）\n")
    local lingyuCount = 0
    local libaoName = 0

    -- 按服务器发来的顺序进行排列
    local voucherTable = {}
    Double11_Vouchers.Foreach(function (key, value)
        local data = {}
        data["ItemId"] = value.ItemId
        data["Jade"] = value.Jade
        table.insert(voucherTable, data)
    end)

    table.sort(voucherTable, 
        function(a, b)
            return a.Jade > b.Jade or (a.Jade == b.Jade and a.ItemId < b.ItemId)
        end
    )

    local indexMap = {}
    for i=1,#voucherTable do
        local data = voucherTable[i]
        indexMap[data.ItemId] = i
    end
    
    Double11_Vouchers.Foreach(function (key, data)
        local index = indexMap[data.ItemId]
        local pCount = pList.Count>=index and pList[index-1] or 0
        local vCount = vList.Count>=index and vList[index-1] or 0
        local itemData = Item_Item.GetData(data.ItemId)
        local rewardStr = SafeStringFormat3(formatStr, itemData.Name, data.Jade, pCount, vCount)
        -- 当前兑换最大的
        if pCount-vCount>0 and data.Jade > lingyuCount then
            libaoName = itemData.Name
            lingyuCount = data.Jade
        end
        contentStr = contentStr .. rewardStr
    end)
    
    self.m_RewardContent.text = contentStr

    -- 元宝奖励数量
    local baseReward = Double11_Setting.GetData().VoucherBaseReward
    -- 购买礼包为空，仅有元宝奖励
	if lingyuCount == 0 then
        self.m_YuanbaoReward.gameObject:SetActive(true)
        self.m_LingyuReward.gameObject:SetActive(false)
        local yuanbaoLabel = self.m_YuanbaoReward.transform:Find("YuanBaoCount"):GetComponent(typeof(UILabel))
        yuanbaoLabel.text = tostring(baseReward)
        self.m_IsOnlyYuanbaoReward = true
    elseif lingyuCount>0 then
        self.m_YuanbaoReward.gameObject:SetActive(false)
        self.m_LingyuReward.gameObject:SetActive(true)
        local lingyuLabel = self.m_LingyuReward.transform:Find("LingyuCount"):GetComponent(typeof(UILabel))
        local yuanbaoLabel = self.m_LingyuReward.transform:Find("YuanBaoCount"):GetComponent(typeof(UILabel))
        local title = self.m_LingyuReward.transform:Find("Title"):GetComponent(typeof(UILabel))
        yuanbaoLabel.text = tostring(baseReward)
        lingyuLabel.text = tostring(lingyuCount)
        title.text = System.String.Format(LocalString.GetString("由于您购买了{0}，可兑换以下奖励："), libaoName)
        self.m_IsOnlyYuanbaoReward = false
    end
end
