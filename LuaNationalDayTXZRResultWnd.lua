local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CBaseWnd = import "L10.UI.CBaseWnd"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"

LuaNationalDayTXZRResultWnd = class()

RegistClassMember(LuaNationalDayTXZRResultWnd, "m_CloseButton")
RegistClassMember(LuaNationalDayTXZRResultWnd, "m_ScoreLabel")
RegistClassMember(LuaNationalDayTXZRResultWnd, "m_TitleTexture")

function LuaNationalDayTXZRResultWnd:Init()

	self.m_CloseButton = self.transform:Find("Anchor/CloseButton").gameObject
	self.m_ScoreLabel = self.transform:Find("Anchor/ScoreLabel"):GetComponent(typeof(UILabel))
	self.m_TitleTexture = self.transform:Find("Anchor/TitleTexture"):GetComponent(typeof(CUITexture))

	CommonDefs.AddOnClickListener(self.m_CloseButton, DelegateFactory.Action_GameObject(function(go) self:OnCloseButtonClick() end), false)

	local score = LuaNationalDayMgr.m_TXZRResultScore
	self.m_ScoreLabel.text = tostring(score)
	if score == 0 then
		self.m_TitleTexture:LoadMaterial("UI/Texture/Transparent/Material/txzz_chenghao_2.mat")
	elseif score == 9 then
		self.m_TitleTexture:LoadMaterial("UI/Texture/Transparent/Material/txzz_chenghao_1.mat")
	else
		self.m_TitleTexture:Clear()
	end
end

function LuaNationalDayTXZRResultWnd:OnCloseButtonClick()
	self:Close()
end

function LuaNationalDayTXZRResultWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end
