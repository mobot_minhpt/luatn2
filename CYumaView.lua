-- Auto Generated!!
local Byte = import "System.Byte"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CSelectMapiWnd = import "L10.UI.CSelectMapiWnd"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CYumaView = import "L10.UI.CYumaView"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local MapiSelectWndType = import "L10.UI.MapiSelectWndType"
local MessageWndManager = import "L10.UI.MessageWndManager"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local ZuoQi_MapiYumashiUpgrade = import "L10.Game.ZuoQi_MapiYumashiUpgrade"
CYumaView.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.OnSendYumaInfo, MakeDelegateFromCSFunction(this.OnSendYumaInfo, MakeGenericClass(Action1, MakeArrayClass(Byte)), this))
    EventManager.AddListener(EnumEventType.OnSyncYumaPlayData, MakeDelegateFromCSFunction(this.OnSyncYumaPlayData, Action0, this))

    UIEventListener.Get(this.tianshuBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.tianshuBtn).onClick, MakeDelegateFromCSFunction(this.OnTianshuClick, VoidDelegate, this), true)
    UIEventListener.Get(this.fanyuBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.fanyuBtn).onClick, MakeDelegateFromCSFunction(this.OnFanyuClick, VoidDelegate, this), true)
    UIEventListener.Get(this.fangmuBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.fangmuBtn).onClick, MakeDelegateFromCSFunction(this.OnFangmuClick, VoidDelegate, this), true)
    UIEventListener.Get(this.questionBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.questionBtn).onClick, MakeDelegateFromCSFunction(this.OnQuestionBtnClick, VoidDelegate, this), true)
end
CYumaView.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.OnSendYumaInfo, MakeDelegateFromCSFunction(this.OnSendYumaInfo, MakeGenericClass(Action1, MakeArrayClass(Byte)), this))
    EventManager.RemoveListener(EnumEventType.OnSyncYumaPlayData, MakeDelegateFromCSFunction(this.OnSyncYumaPlayData, Action0, this))

    UIEventListener.Get(this.tianshuBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.tianshuBtn).onClick, MakeDelegateFromCSFunction(this.OnTianshuClick, VoidDelegate, this), false)
    UIEventListener.Get(this.fanyuBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.fanyuBtn).onClick, MakeDelegateFromCSFunction(this.OnFanyuClick, VoidDelegate, this), false)
    UIEventListener.Get(this.fangmuBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.fangmuBtn).onClick, MakeDelegateFromCSFunction(this.OnFangmuClick, VoidDelegate, this), false)
    UIEventListener.Get(this.questionBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.questionBtn).onClick, MakeDelegateFromCSFunction(this.OnQuestionBtnClick, VoidDelegate, this), false)
end
CYumaView.m_OnSyncYumaPlayData_CS2LuaHook = function (this) 
    local yumaData = CClientMainPlayer.Inst.PlayProp.YumaPlayData
    this.lvlLabel.text = System.String.Format("Lv.{0}", yumaData.Level)
    local upgradeData = ZuoQi_MapiYumashiUpgrade.GetData(yumaData.Level)
    if upgradeData ~= nil then
        this.huizhangTexture:LoadMaterial(upgradeData.Icon)
        this.titleLabel.text = upgradeData.Title
        this.expLabel.text = System.String.Format("{0}/{1}", yumaData.Exp, upgradeData.Exp)
        this.expSlider.value = math.max(0, math.min(1, yumaData.Exp / upgradeData.Exp))
    end
    this.fanyuCountLabel.text = tostring(yumaData.FanyuNum)
    this.preciousCountLabel.text = tostring(yumaData.FanyuPreciousNum)
    this.lingfuCountLabel.text = tostring(yumaData.AcquireLingfuNum)
    this.championCountLabel.text = tostring(yumaData.AcquireChampionNum)
    this.skillCountLabel.text = tostring(yumaData.AcquireSkillNum)
end
CYumaView.m_OnSendYumaInfo_CS2LuaHook = function (this, mapiInfo) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    this:OnSyncYumaPlayData()

    this:ShowMapiList(mapiInfo)
end
CYumaView.m_ShowMapiList_CS2LuaHook = function (this, mapiInfo) 

    local list = TypeAs(MsgPackImpl.unpack(mapiInfo), typeof(MakeGenericClass(List, Object)))
    if list ~= nil and list.Count % 6 == 0 then
        do
            local i = 0
            while i < list.Count do
                local idx = math.floor(i / 6)
                if this.mapiList.Length > idx then
                    local zuoqiId = tostring(list[i])
                    local name = tostring(list[i + 1])
                    local level = math.floor(tonumber(list[i + 2] or 0))
                    local qinmidu = tonumber(list[i + 3])
                    local qingxu = math.floor(tonumber(list[i + 4] or 0))
                    local msg = tostring(list[i + 5])

                    local msgtxt = g_MessageMgr:FormatMessage(msg, name)

                    local bUnderFangmu = false
                    local furnitureInfo = CClientHouseMgr.Inst.mFurnitureProp
                    if furnitureInfo ~= nil then
                        CommonDefs.DictIterate(furnitureInfo.MapiAppearanceInfo, DelegateFactory.Action_object_object(function (___key, ___value) 
                            local v = {}
                            v.Key = ___key
                            v.Value = ___value
                            if v.Value.ZuoqiId == zuoqiId then
                                bUnderFangmu = true
                            end
                        end))
                    end

                    this.mapiList[idx]:Init(zuoqiId, qingxu, msgtxt, qinmidu, name, level, bUnderFangmu)
                end
                i = i + 6
            end
        end
        do
            local i = math.floor(list.Count / 6)
            while i < this.mapiList.Length do
                this.mapiList[i]:InitNull()
                i = i + 1
            end
        end
    end
end
CYumaView.m_OnFanyuClick_CS2LuaHook = function (this, go) 
    if CZuoQiMgr.Inst:GetMapiCount() <= 0 then
        g_MessageMgr:ShowMessage("Mapi_View_No_Mapi_Found")
        return
    end

    CUIManager.ShowUI(CUIResources.MapiFanyuWnd)
end
CYumaView.m_OnFangmuClick_CS2LuaHook = function (this, go) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    if System.String.IsNullOrEmpty(CClientMainPlayer.Inst.ItemProp.HouseId) then
        g_MessageMgr:ShowMessage("Mapi_Cant_Fangmu_No_House")
        return
    end
    if CZuoQiMgr.Inst:GetMapiCount() <= 0 then
        g_MessageMgr:ShowMessage("Mapi_View_No_Mapi_Found")
        return
    end
    if not CClientHouseMgr.Inst:IsInOwnHouse() then
        MessageWndManager.ShowOKCancelMessage(LocalString.GetString("在自己家中才能进行操作，是否现在回家？"), DelegateFactory.Action(function () 
            Gac2Gas.RequestEnterHome()
        end), nil, nil, nil, false)
        return
    end
    CSelectMapiWnd.sType = MapiSelectWndType.eSelectFangmu
    CUIManager.ShowUI(CUIResources.SelectMapiWnd)
end
