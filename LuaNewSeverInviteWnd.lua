local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local Vector3 = import "UnityEngine.Vector3"
local UISprite = import "UISprite"
local Extensions = import "Extensions"
local Profession = import "L10.Game.Profession"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CButton = import "L10.UI.CButton"
local Item_Item = import "L10.Game.Item_Item"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CLoginMgr = import "L10.Game.CLoginMgr"

LuaNewSeverInviteWnd = class()
RegistChildComponent(LuaNewSeverInviteWnd,"closeBtn", GameObject)
RegistChildComponent(LuaNewSeverInviteWnd,"tipBtn", GameObject)
RegistChildComponent(LuaNewSeverInviteWnd,"templateNode", GameObject)
RegistChildComponent(LuaNewSeverInviteWnd,"table", UITable)
RegistChildComponent(LuaNewSeverInviteWnd,"scrollView", UIScrollView)
RegistChildComponent(LuaNewSeverInviteWnd,"playerTemplateNode", GameObject)
RegistChildComponent(LuaNewSeverInviteWnd,"playerTable", UITable)
RegistChildComponent(LuaNewSeverInviteWnd,"playerScrollView", UIScrollView)

--RegistClassMember(LuaNewSeverInviteWnd, "colorIndexTable")

function LuaNewSeverInviteWnd:Close()
	Gac2Gas.TeamAppointmentCheckHasReward()
	CUIManager.CloseUI(CLuaUIResources.NewSeverInviteWnd)
end

function LuaNewSeverInviteWnd:Split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

function LuaNewSeverInviteWnd:OnEnable()
	g_ScriptEvent:AddListener("TeamAppointmentPlayerQueryRewardResult", self, "RefreshData")
end

function LuaNewSeverInviteWnd:OnDisable()
	g_ScriptEvent:RemoveListener("TeamAppointmentPlayerQueryRewardResult", self, "RefreshData")
end

function LuaNewSeverInviteWnd:RefreshData()
  self:Init()
end

function LuaNewSeverInviteWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	local onTipClick = function(go)
		g_MessageMgr:ShowMessage("TeamReverse_Tips")
	end
	CommonDefs.AddOnClickListener(self.tipBtn,DelegateFactory.Action_GameObject(onTipClick),false)

  self.templateNode:SetActive(false)
  self.playerTemplateNode:SetActive(false)

	Extensions.RemoveAllChildren(self.table.transform)
	Extensions.RemoveAllChildren(self.playerTable.transform)

	local config = PlayConfig_ServerIdEnabled.GetData(CLoginMgr.Inst.ServerGroupId)
	local rewardChooseType = config.TeamAppointment

  if LuaTeamAppointMgr.rewardTable then
    for i,v in ipairs(LuaTeamAppointMgr.rewardTable) do
      local rewardId = v.rewardId
      local canReward = v.canReward
			local node = NGUITools.AddChild(self.table.gameObject, self.templateNode)
      local rewardData = nil
			if rewardChooseType == 1 then
				rewardData = TeamReverse_Reward.GetData(rewardId)
			elseif rewardChooseType == 2 then
				rewardData = TeamReverse_UrsReward.GetData(rewardId)
			end

			node:SetActive(true)
      node.transform:Find('text'):GetComponent(typeof(UILabel)).text = rewardData.Description
      local itemId = rewardData.Icon

      local itemData = Item_Item.GetData(itemId)
      local templateScript = node.transform:Find('Award'):GetComponent(typeof(CQnReturnAwardTemplate))
      if itemData and templateScript then
        templateScript:Init(itemData, 0)
      end
      --local iconBtn = node.transform:Find('Award/Icon').gameObject
      --CommonDefs.AddOnClickListener(iconBtn, DelegateFactory.Action_GameObject(function(go)
        --CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgr.AlignType.Top, btn.transform.position.x, btn.transform.position.y, width, height)
      --end), false)
      local btn = node.transform:Find('GetBonusBtn').gameObject
      local cBtn = btn:GetComponent(typeof(CButton))
      local btnText = btn.transform:Find('Label'):GetComponent(typeof(UILabel))
      local btnAlert = btn.transform:Find('Alert').gameObject
      local textTable = {LocalString.GetString('领取'),LocalString.GetString('可领取'),LocalString.GetString('已领取')}
      btnText.text = textTable[canReward]
      btnAlert:SetActive(false)
      if canReward == 1 then
        cBtn.Enabled = false
      elseif canReward == 2 then
        btnAlert:SetActive(true)
        cBtn.Enabled = true
      elseif canReward == 3 then
        cBtn.Enabled = false
      end

      local onGetRewardClick = function(go)
        Gac2Gas.TeamAppointmentPlayerGetReward(rewardId)
      end
      CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(onGetRewardClick),false)
    end
  end

  if LuaTeamAppointMgr.teamPlayerTable then
    for i,v in ipairs(LuaTeamAppointMgr.teamPlayerTable) do
			local node = NGUITools.AddChild(self.playerTable.gameObject, self.playerTemplateNode)
			node:SetActive(true)
      node.transform:Find('name'):GetComponent(typeof(UILabel)).text = v.Name
      node.transform:Find('JobIcon'):GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(v.Class)
      node.transform:Find('lv'):GetComponent(typeof(UILabel)).text = v.Level
			local playerId = v.Id
			local onNodeClick = function(go)
				CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
			end
			CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(onNodeClick),false)
    end
  end

  self.table:Reposition()
  self.scrollView:ResetPosition()

  self.playerTable:Reposition()
  self.playerScrollView:ResetPosition()
end

return LuaNewSeverInviteWnd
