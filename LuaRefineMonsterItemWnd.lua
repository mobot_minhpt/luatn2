local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local QnTabButton = import "L10.UI.QnTabButton"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local State = import "L10.UI.QnTabButton.State"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaRefineMonsterItemWnd = class()
RegistClassMember(LuaRefineMonsterItemWnd,"m_PersonalDataSource")
RegistClassMember(LuaRefineMonsterItemWnd,"m_PersonalTable")
RegistClassMember(LuaRefineMonsterItemWnd,"m_ZongMenDataSource")
RegistClassMember(LuaRefineMonsterItemWnd,"m_ZongMenTable")
RegistClassMember(LuaRefineMonsterItemWnd,"m_PersonalBtn")
RegistClassMember(LuaRefineMonsterItemWnd,"m_ZongMenBtn")
RegistClassMember(LuaRefineMonsterItemWnd,"m_RefreshLabel")
RegistClassMember(LuaRefineMonsterItemWnd,"m_QuestBtn")
RegistClassMember(LuaRefineMonsterItemWnd,"m_CommitBtn")
RegistClassMember(LuaRefineMonsterItemWnd,"m_AdvanceCommitBtn")
RegistClassMember(LuaRefineMonsterItemWnd,"m_PersonalRewardLabel")
RegistClassMember(LuaRefineMonsterItemWnd,"m_PersonalCountLabel")
RegistClassMember(LuaRefineMonsterItemWnd,"m_RecordBtn")
RegistClassMember(LuaRefineMonsterItemWnd,"m_ZongMenRewardLabel")
RegistClassMember(LuaRefineMonsterItemWnd,"m_ZongMenCountLabel")
RegistClassMember(LuaRefineMonsterItemWnd,"m_PersonalRoot")
RegistClassMember(LuaRefineMonsterItemWnd,"m_ZongMenRoot")
-- 传入信息
RegistClassMember(LuaRefineMonsterItemWnd,"m_PersonalData")
RegistClassMember(LuaRefineMonsterItemWnd,"m_PersonalAdvanceData")
RegistClassMember(LuaRefineMonsterItemWnd,"m_PersonalLowest")
RegistClassMember(LuaRefineMonsterItemWnd,"m_AdvancePersonalLowest")
RegistClassMember(LuaRefineMonsterItemWnd,"m_ZongMenData")
RegistClassMember(LuaRefineMonsterItemWnd,"m_ZongMenLowest")
RegistClassMember(LuaRefineMonsterItemWnd,"m_CloseBtn")
RegistClassMember(LuaRefineMonsterItemWnd,"m_LingHeLv")
RegistClassMember(LuaRefineMonsterItemWnd,"m_SoulCoreInfo")
RegistClassMember(LuaRefineMonsterItemWnd,"m_SoulCoreAdvanceInfo")
RegistClassMember(LuaRefineMonsterItemWnd,"m_AdvanceItemList")

function LuaRefineMonsterItemWnd:Init()
    Gac2Gas.QuerySectPersonalInputItemInfo()
    Gac2Gas.QuerySectTopInputItemInfo()
    self.m_LingHeLv = (((CClientMainPlayer.Inst or {}).SkillProp or {}).SoulCore or {}).Level or 1

    self.m_PersonalTable = self.transform:Find("Anchor/Personal"):GetComponent(typeof(QnTableView))
    self.m_ZongMenTable = self.transform:Find("Anchor/ZongMen"):GetComponent(typeof(QnTableView))
    self.m_ZongMenBtn = self.transform:Find("Anchor/Top/ZongMenTab"):GetComponent(typeof(QnTabButton))
    self.m_PersonalBtn = self.transform:Find("Anchor/Top/PersonalTab"):GetComponent(typeof(QnTabButton))
    self.m_RefreshLabel = self.transform:Find("Anchor/Top/RefreshLabel/Label"):GetComponent(typeof(UILabel))

    self.m_PersonalRewardLabel = self.transform:Find("Anchor/Bottom/PersonalRoot/Reward"):GetComponent(typeof(UILabel))
    self.m_PersonalCountLabel = self.transform:Find("Anchor/Bottom/PersonalRoot/Count"):GetComponent(typeof(UILabel))
    self.m_QuestBtn = self.transform:Find("Anchor/Bottom/QuestBtn").gameObject
    self.m_CommitBtn = self.transform:Find("Anchor/Bottom/PersonalRoot/CommitBtn").gameObject
    self.m_AdvanceCommitBtn = self.transform:Find("Anchor/Bottom/PersonalRoot/AdvanceCommitBtn").gameObject
    self.m_PersonalRoot = self.transform:Find("Anchor/Bottom/PersonalRoot").gameObject
    self.m_CloseBtn = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject

    self.m_ZongMenRewardLabel = self.transform:Find("Anchor/Bottom/ZongMenRoot/Reward"):GetComponent(typeof(UILabel))
    self.m_ZongMenCountLabel = self.transform:Find("Anchor/Bottom/ZongMenRoot/Count"):GetComponent(typeof(UILabel))
    self.m_RecordBtn = self.transform:Find("Anchor/Bottom/ZongMenRoot/RecordBtn").gameObject
    self.m_ZongMenRoot = self.transform:Find("Anchor/Bottom/ZongMenRoot").gameObject

    -- 设置状态
    self:SetStatus(true)
    g_ScriptEvent:BroadcastInLua("RefineMonsterItemRotDotDisable", nil)

    local function initItemPresonal(item,index)
        self:InitItem(item, index, true, false)
    end
    local function initItemZongMen(item,index)
        self:InitItem(item, index, false, false)
    end

    self.m_PersonalData = nil
    self.m_ZongMenData = nil
    self.m_PersonalAdvanceData = nil

    self.m_SoulCoreInfo = {}
    self.m_SoulCoreAdvanceInfo = {}
    SoulCore_SoulCore.Foreach(function(id, data)
        self.m_SoulCoreInfo[id] = data.LingCaiLimitCount
        self.m_SoulCoreAdvanceInfo[id] = data.AdvancedLingCaiLimitCount
    end)

    self.m_PersonalDataSource = DefaultTableViewDataSource.CreateByCount(0, initItemPresonal)
    self.m_PersonalTable.m_DataSource = self.m_PersonalDataSource
    self.m_PersonalTable.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnPersonalSelectAtRow(row)
    end)

    self.m_ZongMenDataSource = DefaultTableViewDataSource.CreateByCount(0, initItemZongMen)
    self.m_ZongMenTable.m_DataSource = self.m_ZongMenDataSource
    self.m_ZongMenTable.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnZongMenSelectAtRow(row)
    end)

    self.m_AdvanceItemList = {}
    for i=1,4 do
        table.insert(self.m_AdvanceItemList, self.transform:Find("Anchor/Personal/Additive/Advance/Content/"..i).gameObject)
    end

    UIEventListener.Get(self.m_CommitBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        local status = LuaZongMenMgr.m_RefineMonsterStatusTable

        if status==nil or not status["isInputSoulCore"] or status["templateId"] == 0 then
            g_MessageMgr:ShowMessage("PLEASE_PUT_LINGHE_LIANHUAGUAI_BEFORE")
            return
        end

        if LuaZongMenMgr:CheckFaZhenOperation(LocalString.GetString("投入炼化灵材")) then 
            LuaZongMenMgr.m_LingCaiCount = self.m_PersonalData.Count
            LuaZongMenMgr.m_LingCaiLowest = self.m_PersonalLowest
            LuaZongMenMgr.m_AdvanceLingCaiCount = self.m_PersonalAdvanceData.Count
            LuaZongMenMgr.m_AdvanceLingCaiLowest = self.m_AdvancePersonalLowest
            LuaZongMenMgr:OpenLingCaiChooseWnd(false)
        end
    end)

    UIEventListener.Get(self.m_AdvanceCommitBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        local status = LuaZongMenMgr.m_RefineMonsterStatusTable

        if status==nil or not status["isInputSoulCore"] or status["templateId"] == 0 then
            g_MessageMgr:ShowMessage("PLEASE_PUT_LINGHE_LIANHUAGUAI_BEFORE")
            return
        end

        if self:IsAdvanceOpen() then
            if LuaZongMenMgr:CheckFaZhenOperation(LocalString.GetString("投入炼化灵材")) then 
                if self.m_LingHeLv and self.m_LingHeLv > 0 and SoulCore_SoulCore.GetData(self.m_LingHeLv).AdvancedLingCaiLimitCount>0 then
                    LuaZongMenMgr.m_LingCaiCount = self.m_PersonalData.Count
                    LuaZongMenMgr.m_LingCaiLowest = self.m_PersonalLowest
                    LuaZongMenMgr.m_AdvanceLingCaiCount = self.m_PersonalAdvanceData.Count
                    LuaZongMenMgr.m_AdvanceLingCaiLowest = self.m_AdvancePersonalLowest
                    LuaZongMenMgr:OpenLingCaiChooseWnd(true)
                else
                    g_MessageMgr:ShowMessage("LianHua_Advanced_LingCai_LimitCount_Zero")
                end
            end
        else
            g_MessageMgr:ShowMessage("LianHua_Advanced_LingCai_Function_Not_Open")
        end
    end)

    UIEventListener.Get(self.m_QuestBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        g_MessageMgr:ShowMessage("LianHuaItem__Introduction")
    end)

    UIEventListener.Get(self.m_RecordBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.ShowUI(CLuaUIResources.RefineMonsterRecordWnd)
    end)

    UIEventListener.Get(self.m_ZongMenBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        self:SetStatus(false)
        if self.m_ZongMenData and self.m_ZongMenDataSource.count ~= self.m_ZongMenData.Count /3 then
            self.m_ZongMenDataSource.count = self.m_ZongMenData.Count /3
            self.m_ZongMenTable:ReloadData(true,false)
        end
    end)

    UIEventListener.Get(self.m_PersonalBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        self:SetStatus(true)
        if self.m_PersonalData and self.m_PersonalDataSource.count ~= self.m_PersonalData.Count then
            self.m_PersonalDataSource.count = LianHua_Setting.GetData().PersonalLianHuaItemLimit
            self.m_PersonalTable:ReloadData(true,false)
        end
    end)

    UIEventListener.Get(self.m_CloseBtn).onClick = DelegateFactory.VoidDelegate(function()
        self:OnClose()
    end)
end

function LuaRefineMonsterItemWnd:IsAdvanceOpen()
    return true
end

function LuaRefineMonsterItemWnd:SetStatus(isPersonal)
    self.m_PersonalTable.gameObject:SetActive(isPersonal)
    self.m_ZongMenTable.gameObject:SetActive(not isPersonal)
    self.m_PersonalRoot:SetActive(isPersonal)
    self.m_ZongMenRoot:SetActive(not isPersonal)
    self.m_ZongMenBtn:SetState( (not isPersonal) and State.Highlighted or State.Normal)
    self.m_PersonalBtn:SetState(isPersonal and State.Highlighted or State.Normal)
end

function LuaRefineMonsterItemWnd:OnZongMenSelectAtRow(row)
    local templateId = self.m_ZongMenData[row*3]
    local playerId = self.m_ZongMenData[row*3+1]
    local playerName = self.m_ZongMenData[row*3+2]
    local tbl = {}
    local data = LianHua_Item.GetData(templateId)
    table.insert(tbl, SafeStringFormat3(LocalString.GetString("[ACF9FF]评分[-] %d"), data.Quality))
    table.insert(tbl, SafeStringFormat3(LocalString.GetString("由[FFFE91]%s[-]提交"), tostring(playerName)))
    LuaZongMenMgr.m_RefineMonsterItemLabels = tbl
    CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, AlignType.Default, 0, 0, 0, 0)
end

function LuaRefineMonsterItemWnd:OnPersonalSelectAtRow(row)
    -- 高级物资不选中
    self:SetSelected(-1)
    if row < self.m_PersonalData.Count then
        local templateId = self.m_PersonalData[row]
        local tbl = {}
        local data = LianHua_Item.GetData(templateId)
        table.insert(tbl, SafeStringFormat3(LocalString.GetString("[ACF9FF]评分[-] %d"), data.Quality))
        LuaZongMenMgr.m_RefineMonsterItemLabels = tbl
        CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, AlignType.Default, 0, 0, 0, 0)
    elseif self.m_LingHeLv and self.m_LingHeLv > 0 and row >= SoulCore_SoulCore.GetData(self.m_LingHeLv).LingCaiLimitCount then
        for i=1,#self.m_SoulCoreInfo do
            if self.m_SoulCoreInfo[i]>= row+1 then
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("解锁第%d格灵材需要灵核等级达到%d级"), row+1, i))
                return
            end
        end
    end
end

function LuaRefineMonsterItemWnd:InitItem(item, index, isPersonal, isAdvance)
    local iconTexture = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local ownerIcon = item.transform:Find("OwnerIcon").gameObject

    local templateId = nil
    if isPersonal then
        ownerIcon:SetActive(false)
        local lockIcon = item.transform:Find("LockIcon").gameObject

        if not isAdvance then
            if index < self.m_PersonalData.Count then
                templateId = self.m_PersonalData[index]
                lockIcon:SetActive(false)
            elseif self.m_LingHeLv and self.m_LingHeLv > 0 and index < SoulCore_SoulCore.GetData(self.m_LingHeLv).LingCaiLimitCount then
                lockIcon:SetActive(false)
            else
                lockIcon:SetActive(true)
            end
        else
            if index < self.m_PersonalAdvanceData.Count then
                templateId = self.m_PersonalAdvanceData[index]
                lockIcon:SetActive(false)
            elseif self.m_LingHeLv and self.m_LingHeLv > 0 and index < SoulCore_SoulCore.GetData(self.m_LingHeLv).AdvancedLingCaiLimitCount then
                lockIcon:SetActive(false)
            else
                lockIcon:SetActive(true)
            end
        end
    else
        templateId = self.m_ZongMenData[index*3]
        local playerId = self.m_ZongMenData[index*3+1]
        ownerIcon:SetActive(playerId == (CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id))
    end
    -- 图标

    if templateId then
        local data = Item_Item.GetData(templateId)
        if data == nil then
            data = EquipmentTemplate_Equip.GetData(templateId)
        end

        if data then
            iconTexture:LoadMaterial(data.Icon)
        end
    end
end

function LuaRefineMonsterItemWnd:ScondToString(sconds)
    local current = CServerTimeMgr.Inst.timeStamp
    sconds = sconds - current
    if sconds <= 0 then
        return SafeStringFormat3(LocalString.GetString("1小时内"))
    end

    local days = math.floor(sconds / 86400)
    local mod = math.fmod(sconds, 86400)
    local hours = math.floor( mod / 3600)
    mod = math.fmod(mod, 3600)
    local mins = math.floor(mod / 60)

    if days == 0 then
        if hours == 0 then
            return SafeStringFormat3(LocalString.GetString("1小时内"), mins)
        else
            return SafeStringFormat3(LocalString.GetString("%d时后"), hours)
        end
    else
        return SafeStringFormat3(LocalString.GetString("%d天后"), days)
    end
end

function LuaRefineMonsterItemWnd:InitPersonalTable(nextRefreshTime, data, advanceData, typeList)
    self.m_RefreshLabel.text = self:ScondToString(nextRefreshTime)
    self.m_PersonalData = data
    self.m_PersonalAdvanceData = advanceData

    LuaZongMenMgr.m_LingCaiTypeList = {}
    for i=0, typeList.Count-1 do
        table.insert(LuaZongMenMgr.m_LingCaiTypeList, typeList[i])
    end

    if #LuaZongMenMgr.m_LingCaiTypeList == 0 then
        local defaultData = LianHua_Setting.GetData().DefaultCommitItemType
        for i=0, defaultData.Length-1 do
            table.insert(LuaZongMenMgr.m_LingCaiTypeList, defaultData[i])
        end
    end

    if self.m_LingHeLv and self.m_LingHeLv > 0 then
        self.m_PersonalCountLabel.text = SafeStringFormat3("%d/%d", data.Count, SoulCore_SoulCore.GetData(self.m_LingHeLv).LingCaiLimitCount)
    else
        self.m_PersonalCountLabel.text = "0/0"
    end
    
    local count = 0
    for i=0,data.Count-1 do
        local itemData = LianHua_Item.GetData(data[i])
        if itemData~=nil then
            count = count + itemData.LingLi
        end
    end

    LuaZongMenMgr.m_AdvanceLingCaiLowest = 0
    self.m_AdvancePersonalLowest = 0
    -- 增加了高级灵材的灵力值
    for i=0,advanceData.Count-1 do
        local itemData = LianHua_Item.GetData(advanceData[i])
        if itemData~=nil then
            local q = itemData.Quality
            if self.m_AdvancePersonalLowest == 0 or q<self.m_AdvancePersonalLowest then
                self.m_AdvancePersonalLowest = q
            end
            count = count + itemData.LingLi
        end
    end
    LuaZongMenMgr.m_AdvanceLingCaiLowest = self.m_AdvancePersonalLowest

    LuaZongMenMgr.m_PersonalLingCaiSum = count
    self.m_PersonalRewardLabel.text = SafeStringFormat3("%0.1f", math.floor((LuaZongMenMgr:GetPersonalLingCaiParam() or 0)*10)/10)

    --最低评分
    if data.Count>0 then
        self.m_PersonalLowest = LianHua_Item.GetData(data[data.Count-1]).Quality
    else
        self.m_PersonalLowest = 0
    end
    -- 初始化
    if self.m_PersonalTable.gameObject.activeSelf then
        self.m_PersonalDataSource.count= LianHua_Setting.GetData().PersonalLianHuaItemLimit
        self.m_PersonalTable:ReloadData(true,false)
    end

    LuaZongMenMgr.m_LingCaiCount = self.m_PersonalData.Count
    LuaZongMenMgr.m_LingCaiLowest = self.m_PersonalLowest
    LuaZongMenMgr.m_AdvanceLingCaiCount = self.m_PersonalAdvanceData.Count

    self:InitAdvanceItem(advanceData)
    g_ScriptEvent:BroadcastInLua("RefineMonsterItemUpdatePersonalTab")
end

function LuaRefineMonsterItemWnd:InitAdvanceItem(data)
    for i=1,4 do
        self:InitItem(self.m_AdvanceItemList[i], i-1, true, true)
        UIEventListener.Get(self.m_AdvanceItemList[i]).onClick = DelegateFactory.VoidDelegate(function()
            self:OnAdvanceItemClick(i-1)
        end)
    end
end

function LuaRefineMonsterItemWnd:SetSelected(index)
    for i=1,4 do
        local selectSp = self.m_AdvanceItemList[i].transform:Find("SelectedSprite").gameObject
        selectSp:SetActive(index == i and not selectSp.activeSelf)
    end
end

function LuaRefineMonsterItemWnd:OnAdvanceItemClick(row)
    if row < self.m_PersonalAdvanceData.Count then
        local templateId = self.m_PersonalAdvanceData[row]
        local tbl = {}
        local data = LianHua_Item.GetData(templateId)
        table.insert(tbl, SafeStringFormat3(LocalString.GetString("[ACF9FF]评分[-] %d"), data.Quality))
        LuaZongMenMgr.m_RefineMonsterItemLabels = tbl
        CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, AlignType.Default, 0, 0, 0, 0)
    elseif self.m_LingHeLv and self.m_LingHeLv > 0 and row >= SoulCore_SoulCore.GetData(self.m_LingHeLv).AdvancedLingCaiLimitCount then
        for i=1,#self.m_SoulCoreAdvanceInfo do
            if self.m_SoulCoreAdvanceInfo[i]>= row+1 then
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("解锁第%d格高级灵材需要灵核等级达到%d级"), row+1, i))
            end
        end
    end

    if self.m_PersonalTable.m_CurrentSelectRow ~= -1 and self.m_PersonalTable.m_CurrentSelectRow < self.m_PersonalTable.m_ItemList.Count then
        local cur = self.m_PersonalTable.m_ItemList[self.m_PersonalTable.m_CurrentSelectRow]
        cur:SetSelected(false, false)
    end
    -- 设置选中框
    self:SetSelected(row+1)
end

function LuaRefineMonsterItemWnd:InitZongMenTable(nextRefreshTime, data)
    self.m_RefreshLabel.text = self:ScondToString(nextRefreshTime)
    self.m_ZongMenData = data
    self.m_ZongMenCountLabel.text = SafeStringFormat3("%d/%d", data.Count/3, LianHua_Setting.GetData().ZongMenLianHuaItemLimit)

    local count = 0
    local sum = 0

    for i=0, data.Count/3 -1 do
        local itemData = LianHua_Item.GetData(data[i*3])
        if itemData~=nil then
            count = count + itemData.LingLi
            if i<50 then
                sum = sum + itemData.LingLi
            end
        end
    end
    LuaZongMenMgr.m_ZongMenLingCaiSum = sum
    -- 保留三位小数
    self.m_ZongMenRewardLabel.text = SafeStringFormat3("%0.3f", math.floor((LuaZongMenMgr:GetZongMenLingCaiParam() or 0)*1000)/1000)
    -- 保留一位小数
    self.m_PersonalRewardLabel.text = SafeStringFormat3("%0.1f", math.floor((LuaZongMenMgr:GetPersonalLingCaiParam() or 0)*10)/10)
    g_ScriptEvent:BroadcastInLua("RefineMonsterItemUpdateZongMenLingLi")

    --最低评分
    if data.Count>2 then
        self.m_ZongMenLowest = LianHua_Item.GetData(data[data.Count-3]).Quality
    else
        self.m_ZongMenLowest = 0
    end

    -- 初始化
    if self.m_ZongMenTable.gameObject.activeSelf then
        self.m_ZongMenDataSource.count= data.Count/3
        self.m_ZongMenTable:ReloadData(true,false)
    end
end

function LuaRefineMonsterItemWnd:GetGuideGo(methodName)
    if methodName == "GetCommitBtn" and self.m_CommitBtn.activeSelf then
        return self.m_CommitBtn
    elseif methodName == "GetCloseBtn" then
        return self.m_CloseBtn
    end
    return nil
end

function LuaRefineMonsterItemWnd:NeedToRefresh()
    self.m_LingHeLv = (((CClientMainPlayer.Inst or {}).SkillProp or {}).SoulCore or {}).Level or 1
    Gac2Gas.QuerySectPersonalInputItemInfo()
    Gac2Gas.QuerySectTopInputItemInfo()
end

function LuaRefineMonsterItemWnd:OnEnable()
	g_ScriptEvent:AddListener("QuerySectTopInputItemInfoResult", self, "InitZongMenTable")
	g_ScriptEvent:AddListener("QuerySectPersonalInputItemInfoResult", self, "InitPersonalTable")
	g_ScriptEvent:AddListener("PutItemIntoFaZhenSuccess", self, "NeedToRefresh")
end

function LuaRefineMonsterItemWnd:OnDisable()
	g_ScriptEvent:RemoveListener("QuerySectTopInputItemInfoResult", self, "InitZongMenTable")
	g_ScriptEvent:RemoveListener("QuerySectPersonalInputItemInfoResult", self, "InitPersonalTable")
	g_ScriptEvent:RemoveListener("PutItemIntoFaZhenSuccess", self, "NeedToRefresh")
end

function LuaRefineMonsterItemWnd:OnClose()
    CUIManager.CloseUI(CLuaUIResources.RefineMonsterItemWnd)
    CUIManager.CloseUI(CLuaUIResources.CommonItemChooseWnd)
    CUIManager.CloseUI(CLuaUIResources.RefineMonsterItemChooseWnd)
end
