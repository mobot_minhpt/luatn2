-- Auto Generated!!
local Boolean = import "System.Boolean"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CTooltip = import "L10.UI.CTooltip"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CVehicleWnd = import "L10.UI.CVehicleWnd"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local LuaGac2Gas = Gac2Gas
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local PlayerSettings = import "L10.Game.PlayerSettings"
local QnCheckBox = import "L10.UI.QnCheckBox"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local ZuoQi_Setting = import "L10.Game.ZuoQi_Setting"
local CClientObject = import "L10.Game.CClientObject"
local CVehicleTemplate = import "L10.UI.CVehicleTemplate"
CVehicleWnd.m_Init_CS2LuaHook = function (this) 

    --modelPreviewer.Init();


    this.vehicleList:Init()

    local zuoqitid = CZuoQiMgr.Inst.curSelectedZuoqiTid
    local zuoqiid = CZuoQiMgr.Inst.curSelectedZuoqiId
    local box = CommonDefs.GetComponent_GameObject_Type(this.setDefaultBoxCkBox, typeof(QnCheckBox))
    local bIsDefault = false
    if zuoqitid == 0 then
        box:SetSelected(false, true)
        zuoqitid = CClientMainPlayer.Inst.AppearanceProp.ZuoQiTemplateId
        zuoqiid = CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId
    else
        bIsDefault = (CClientMainPlayer.Inst.BasicProp.DefaultZuoQiId == CZuoQiMgr.Inst.curSelectedZuoqiId)
        box:SetSelected(bIsDefault, true)
    end

    if bIsDefault then
        box.Text = LocalString.GetString("已设置默认")
        CUICommonDef.SetActive(box.gameObject, false, true)
    else
        box.Text = LocalString.GetString("设置为默认坐骑")
        CUICommonDef.SetActive(box.gameObject, true, true)
    end

    local autoRideBox = CommonDefs.GetComponent_Component_Type(this.autoRideCkBox, typeof(QnCheckBox))
    if autoRideBox then
        autoRideBox:SetSelected(PlayerSettings.AutoRideEnabled, true)
    end

    -- 变身时装
    local monsterId = LuaAppearancePreviewMgr.GetCurrentTransformMonsterId()
    local newzuoqitid = CZuoQiMgr.GetTransformZuoQiTemplateId(zuoqiid, zuoqitid)
    this.modelPreviewer:PreviewMainPlayer(CClientMainPlayer.Inst.AppearanceProp.HideGemFxToOtherPlayer, CClientMainPlayer.Inst.AppearanceProp.IntesifySuitId, 0, 0, 0, newzuoqitid, 180, zuoqiid, monsterId)
    LuaModelTextureLoaderMgr:SetModelCameraColor(this.modelPreviewer.identifier)

    this.exchangeButton.gameObject:SetActive(CZuoQiMgr.IsTransformZuoQi(zuoqitid))
    UIEventListener.Get(this.exchangeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        LuaVehicleWnd:OnExchangeButtonClicked()
	end)

    this.modelPanel:ResetAndUpdateAnchors()
end
CVehicleWnd.m_OnEnable_CS2LuaHook = function (this) 
    this:Init()
    UIEventListener.Get(this.getMoreButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.getMoreButton).onClick, MakeDelegateFromCSFunction(this.OnGetMoreButtonClick, VoidDelegate, this), true)

    local box = CommonDefs.GetComponent_GameObject_Type(this.setDefaultBoxCkBox, typeof(QnCheckBox))
    box.OnValueChanged = CommonDefs.CombineListner_Action_bool(box.OnValueChanged, MakeDelegateFromCSFunction(this.OnSetDefaultZuoqiClick, MakeGenericClass(Action1, Boolean), this), true)
    this.autoRideCkBox.OnValueChanged = CommonDefs.CombineListner_Action_bool(this.autoRideCkBox.OnValueChanged, MakeDelegateFromCSFunction(this.OnAutoRideClick, MakeGenericClass(Action1, Boolean), this), true)
    EventManager.AddListener(EnumEventType.OnSetDefaultVehicle, MakeDelegateFromCSFunction(this.OnSetDefaultVehicle, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.OnVehicleSelected, MakeDelegateFromCSFunction(this.OnVehicleSelected, MakeGenericClass(Action1, Boolean), this))
    LuaVehicleWnd:OnEnable(this)
end
CVehicleWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.getMoreButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.getMoreButton).onClick, MakeDelegateFromCSFunction(this.OnGetMoreButtonClick, VoidDelegate, this), false)
    local box = CommonDefs.GetComponent_GameObject_Type(this.setDefaultBoxCkBox, typeof(QnCheckBox))
    box.OnValueChanged = CommonDefs.CombineListner_Action_bool(box.OnValueChanged, MakeDelegateFromCSFunction(this.OnSetDefaultZuoqiClick, MakeGenericClass(Action1, Boolean), this), false)
    this.autoRideCkBox.OnValueChanged = CommonDefs.CombineListner_Action_bool(this.autoRideCkBox.OnValueChanged, MakeDelegateFromCSFunction(this.OnAutoRideClick, MakeGenericClass(Action1, Boolean), this), false)
    EventManager.RemoveListener(EnumEventType.OnSetDefaultVehicle, MakeDelegateFromCSFunction(this.OnSetDefaultVehicle, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.OnVehicleSelected, MakeDelegateFromCSFunction(this.OnVehicleSelected, MakeGenericClass(Action1, Boolean), this))
    LuaVehicleWnd:OnDisable()
end
CVehicleWnd.m_OnVehicleSelected_CS2LuaHook = function (this, bDefault) 

    local box = CommonDefs.GetComponent_GameObject_Type(this.setDefaultBoxCkBox, typeof(QnCheckBox))
    box:SetSelected(bDefault, true)

    if bDefault then
        box.Text = LocalString.GetString("已设置默认")
        CUICommonDef.SetActive(box.gameObject, false, true)
    else
        box.Text = LocalString.GetString("设置为默认坐骑")
        CUICommonDef.SetActive(box.gameObject, true, true)
    end

    this.exchangeButton.gameObject:SetActive(CZuoQiMgr.IsTransformZuoQi(CZuoQiMgr.Inst.curSelectedZuoqiTid))
end
CVehicleWnd.m_OnSetDefaultVehicle_CS2LuaHook = function (this) 

    local box = CommonDefs.GetComponent_GameObject_Type(this.setDefaultBoxCkBox, typeof(QnCheckBox))
    if box ~= nil then
        local vid = CClientMainPlayer.Inst.BasicProp.DefaultZuoQiId
        if vid ~= nil and vid == CZuoQiMgr.Inst.curSelectedZuoqiId then
            box:SetSelected(true, true)
            box.Text = LocalString.GetString("已设置默认")
            CUICommonDef.SetActive(box.gameObject, false, true)
        else
            box:SetSelected(false, true)
            box.Text = LocalString.GetString("设置为默认坐骑")
            CUICommonDef.SetActive(box.gameObject, true, true)
        end
    end
end
CVehicleWnd.m_OnSetDefaultZuoqiClick_CS2LuaHook = function (this, bSelected) 

    local vid = CZuoQiMgr.Inst.curSelectedZuoqiId
    if vid == nil then
        local box = CommonDefs.GetComponent_GameObject_Type(this.setDefaultBoxCkBox, typeof(QnCheckBox))
        if box ~= nil then
            box:SetSelected(false, true)
        end
        g_MessageMgr:ShowMessage("CUSTOM_STRING", LocalString.GetString("尚未选中坐骑!"))
        return
    end

    if vid ~= nil and vid ~= CClientMainPlayer.Inst.BasicProp.DefaultZuoQiId then
        Gac2Gas.RequestSetDefaultZuoQi(vid)
    else
        local box = CommonDefs.GetComponent_GameObject_Type(this.setDefaultBoxCkBox, typeof(QnCheckBox))
        if box ~= nil and vid ~= nil and CClientMainPlayer.Inst.BasicProp.DefaultZuoQiId == vid then
            box:SetSelected(true, true)
        end
    end
end
CVehicleWnd.m_OnGetMoreButtonClick_CS2LuaHook = function (this, go) 

    local data = ZuoQi_Setting.GetData()
    local itemId = data.ItemGetId
    CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, true, go.transform, CTooltip.AlignType.Top)
end

LuaVehicleWnd = {}
LuaVehicleWnd.m_Wnd = nil
function LuaVehicleWnd:OnEnable(wnd)
    self.m_Wnd = wnd
end

function LuaVehicleWnd:OnDisable()
    self.m_Wnd = nil
end

function LuaVehicleWnd:OnExchangeButtonClicked()
    local zuoqiid = CZuoQiMgr.Inst.curSelectedZuoqiId
    local zuoqitid = CZuoQiMgr.Inst.curSelectedZuoqiTid
    local data = ZuoQi_Transformers.GetData(zuoqitid)
    LuaGac2Gas.RequestSwitchZuoQiPassengerMode(zuoqiid, data and 1 or 0)
    local data2 = ZuoQi_Transformers.GetDataBySubKey("TransformID", zuoqitid)
    if data then
        zuoqitid = data.TransformID   
    elseif data2 then
        zuoqitid = data2.ID
    end
    CZuoQiMgr.Inst.curSelectedZuoqiTid = zuoqitid
    CZuoQiMgr.ProcessZuoQiTransformAni(self.m_Wnd.modelPreviewer.m_RO, zuoqitid)
    CClientObject.GetInVehicleAsDriver(self.m_Wnd.modelPreviewer.m_RO, zuoqitid, true, nil, false, 0)
    local zuoqiData = ZuoQi_ZuoQi.GetData(zuoqitid)
    if zuoqiData and self.m_Wnd.modelPreviewer.corideLabel then
        local corideCount = zuoqiData.CoRide
        local s = corideCount == 0 and LocalString.GetString("可1人独骑") or SafeStringFormat3(LocalString.GetString("可%d人同骑"),corideCount + 1)
        self.m_Wnd.modelPreviewer.corideLabel.text = s
    end
    local count = self.m_Wnd.vehicleList.table.transform.childCount
    for i = 0, count - 1 do
        local go = self.m_Wnd.vehicleList.table.transform:GetChild(i).gameObject
        local template = CommonDefs.GetComponent_GameObject_Type(go, typeof(CVehicleTemplate))
        if template and template.vehicleID == zuoqiid then
            for i = 0, CZuoQiMgr.Inst.zuoqis.Count - 1 do
                local zuoqi = CZuoQiMgr.Inst.zuoqis[i]
                if zuoqi.id == zuoqiid then
                    template:Init(zuoqi.id, zuoqitid, zuoqi.expTime)
                    break
                end
            end
            break
        end
    end
end