LuaCommonTextImageRuleMgr = {}

LuaCommonTextImageRuleMgr.m_TextRuleInfo = {}
LuaCommonTextImageRuleMgr.m_ImageRuleInfo = {}

-- 打开图文界面
function LuaCommonTextImageRuleMgr:ShowWnd(picRuleId)
    local data = Message_PicRule.GetData(picRuleId)
    if data then
        self.m_TextRuleInfo = {}
        self.m_ImageRuleInfo = {}

        local textData = data.Text
        CommonDefs.DictIterate(textData, DelegateFactory.Action_object_object(function(key,val)
            local info = {}
            info.Title = key
            info.Msg = val
            table.insert(self.m_TextRuleInfo, info)
        end))

        local imageData = data.Image
        CommonDefs.DictIterate(imageData, DelegateFactory.Action_object_object(function(key,val)
            local info = {}
            info.ImagePath = key
            info.Msg = val
            table.insert(self.m_ImageRuleInfo, info)
        end))

        CUIManager.ShowUI(CLuaUIResources.CommonTextImageRuleWnd)
    end
end

-- 打开图片引导界面
function LuaCommonTextImageRuleMgr:ShowOnlyImageWnd(picRuleId)
    local data = Message_PicRule.GetData(picRuleId)
    if data then
        local imagePaths = {}
        local msgs = {}

        local imageData = data.Image
        CommonDefs.DictIterate(imageData, DelegateFactory.Action_object_object(function(key,val)
            table.insert(imagePaths, key)
            table.insert(msgs, g_MessageMgr:FormatMessage(val))
        end))

        LuaImageRuleMgr:ShowCommonImageRuleWnd(imagePaths, msgs)
    end
end