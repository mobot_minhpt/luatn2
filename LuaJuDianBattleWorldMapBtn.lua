local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaJuDianBattleWorldMapBtn = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaJuDianBattleWorldMapBtn, "Btn", "Btn", GameObject)

--@endregion RegistChildComponent end

function LuaJuDianBattleWorldMapBtn:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaJuDianBattleWorldMapBtn:OnEnable()
    UIEventListener.Get(self.Btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        print("OpenWorldMap")
	    LuaJuDianBattleMgr:OpenWorldMap()
	end)

end

--@region UIEvent

--@endregion UIEvent

