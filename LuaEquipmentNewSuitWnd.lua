local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local UISprite = import "UISprite"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

local CEquipmentIntensifyMgr = import "L10.Game.CEquipmentIntensifyMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumItemPlaceSize = import "L10.Game.EnumItemPlaceSize"
local QualityColor = import "L10.Game.QualityColor"
local EnumQualityType = import "L10.Game.EnumQualityType"
local StringBuilder = import "System.Text.StringBuilder"

LuaEquipmentNewSuitWnd = class()
RegistClassMember(LuaEquipmentNewSuitWnd, "m_Intensify") -- 强化组件（1为当前，2为下一级）
RegistClassMember(LuaEquipmentNewSuitWnd, "m_Forge")     -- 锻造组件（1为当前，2为下一级）

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaEquipmentNewSuitWnd, "IntensifyTable", "IntensifyTable", UITable)
RegistChildComponent(LuaEquipmentNewSuitWnd, "CurIntensify", "CurIntensify", GameObject)
RegistChildComponent(LuaEquipmentNewSuitWnd, "NextIntensify", "NextIntensify", GameObject)
RegistChildComponent(LuaEquipmentNewSuitWnd, "IntensifyBg", "IntensifyBg", UISprite)
RegistChildComponent(LuaEquipmentNewSuitWnd, "ForgeTable", "ForgeTable", UITable)
RegistChildComponent(LuaEquipmentNewSuitWnd, "CurForge", "CurForge", GameObject)
RegistChildComponent(LuaEquipmentNewSuitWnd, "NextForge", "NextForge", GameObject)
RegistChildComponent(LuaEquipmentNewSuitWnd, "ForgeBg", "ForgeBg", UISprite)

--@endregion RegistChildComponent end

-- 初始化组件表
function LuaEquipmentNewSuitWnd:InitComponent()
    self.m_Intensify = {}
    local trans = self.CurIntensify.transform
    local MakeStarArray = function(tf)
        local arr = CreateFromClass(MakeArrayClass(UISprite), 5)
        for i = 0, arr.Length - 1 do
            arr[i] = tf:Find(tostring(i + 1)):GetComponent(typeof(UISprite))
        end
        return arr
    end
    table.insert(self.m_Intensify, {
        go = self.CurIntensify,
        title = trans:Find("CurTitleLabel"):GetComponent(typeof(UILabel)),
        addition = trans:Find("CurAdditionLabel"):GetComponent(typeof(UILabel)),
        score = trans:Find("CurScoreLabel"):GetComponent(typeof(UILabel)),
        desc = trans:Find("CurDesc/CurDescLabel"):GetComponent(typeof(UILabel)),
        star = MakeStarArray(trans:Find("CurDesc/StarList")),
    })
    trans = self.NextIntensify.transform
    table.insert(self.m_Intensify, {
        go = self.NextIntensify,
        title = trans:Find("NextTitleLabel"):GetComponent(typeof(UILabel)),
        addition = trans:Find("NextAdditionLabel"):GetComponent(typeof(UILabel)),
        score = trans:Find("NextScoreLabel"):GetComponent(typeof(UILabel)),
        desc = trans:Find("NextDesc/NextDescLabel"):GetComponent(typeof(UILabel)),
        star = MakeStarArray(trans:Find("NextDesc/StarList")),
    })

    self.m_Forge = {}
    trans = self.CurForge.transform
    table.insert(self.m_Forge, {
        go = self.CurForge,
        title = trans:Find("CurTitleLabel"):GetComponent(typeof(UILabel)),
        addition = trans:Find("CurAdditionLabel"):GetComponent(typeof(UILabel)),
        score = trans:Find("CurScoreLabel"):GetComponent(typeof(UILabel)),
        desc = trans:Find("CurDesc/CurDescLabel"):GetComponent(typeof(UILabel)),
        quality = trans:Find("CurDesc/CurQualityLabel"):GetComponent(typeof(UILabel)),
    })
    trans = self.NextForge.transform
    table.insert(self.m_Forge, {
        go = self.NextForge,
        title = trans:Find("NextTitleLabel"):GetComponent(typeof(UILabel)),
        addition = trans:Find("NextAdditionLabel"):GetComponent(typeof(UILabel)),
        score = trans:Find("NextScoreLabel"):GetComponent(typeof(UILabel)),
        desc = trans:Find("NextDesc/NextDescLabel"):GetComponent(typeof(UILabel)),
        quality = trans:Find("NextDesc/NextQualityLabel"):GetComponent(typeof(UILabel)),
    })
end
function LuaEquipmentNewSuitWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self:InitComponent()
end
function LuaEquipmentNewSuitWnd:Init()
    if CLuaEquipmentSuitInfoMgr.IsShowMainPlayerSuit then
        if CClientMainPlayer.Inst ~= nil then
            self:OnUpdateData(CClientMainPlayer.Inst)
        else
            self:UpdateData(0, 0)
        end
    end
end
-- 转换属性词条
function LuaEquipmentNewSuitWnd:GetSuitPropertyAddText(addPropertyStr, splitstr, propword)
    local addProperty = CreateFromClass(StringBuilder)
    local splits = g_LuaUtil:StrSplit(addPropertyStr, ';')
    if splits then
        for i = 1, #splits do
            if not System.String.IsNullOrEmpty(splits[i]) then
                local splits2 = g_LuaUtil:StrSplit(splits[i], '=')
                local key = StringTrim(splits2[1])
                local val = StringTrim(splits2[2])
                local keyStr = ""
                if key == "AdjCor" then
                    keyStr = LocalString.GetString("根骨")
                elseif key == "AdjStr" then
                    keyStr = LocalString.GetString("力量")
                elseif key == "AdjSta" then
                    keyStr = LocalString.GetString("精力")
                elseif key == "AdjInt" then
                    keyStr = LocalString.GetString("智力")
                elseif key == "AdjAgi" then
                    keyStr = LocalString.GetString("敏捷")
                end
                addProperty:AppendLine(keyStr .. splitstr .. "+" .. val)
            end
        end
    end
    if propword then
        addProperty:AppendLine(Word_Word.GetData(propword).Description)
    end
    return StringTrim(addProperty:ToString())
end
-- 获取装备数量
function LuaEquipmentNewSuitWnd:GetEquipNum(suitDat)
    if suitDat.ID < 9 then
        return suitDat.NoWeaponEquipNum
    else
        if CClientMainPlayer.Inst == nil then
            return suitDat.NoWeaponEquipNum
        end
        if CommonDefs.IsZhanKuang(CClientMainPlayer.Inst.Class) then --战狂按照单手算
            return suitDat.EquipNum
        end
        local count = EnumItemPlaceSize.GetPlaceSize(EnumItemPlace.Body)
        local itemProp = CClientMainPlayer.Inst.ItemProp
        for i = 1, count do
            local itemId = itemProp:GetItemAt(EnumItemPlace.Body, i)
            local commonItem = CItemMgr.Inst:GetById(itemId)
            if commonItem ~= nil and commonItem.IsEquip and commonItem.Equip.IsWeapon then
                local equip = EquipmentTemplate_Equip.GetData(commonItem.TemplateId)
                if equip ~= nil and equip.BothHand == 1 then
                    return suitDat.BothHandEquip
                else
                    return suitDat.EquipNum
                end
            end
        end
        return suitDat.NoWeaponEquipNum
    end
end
-- 更新强化信息
function LuaEquipmentNewSuitWnd:UpdateIntensify(tbl, index, intensifySuitId, intensifySuitFxIndex)
    local Suit = EquipIntensify_Suit.GetData(intensifySuitId)
    if not Suit then
        tbl.go:SetActive(false)
        return
    end
    tbl.go:SetActive(true)

    if index == 1 then
        if intensifySuitFxIndex > 0 and not (Suit.SecondDesc == nil or Suit.SecondDesc == "") then
            tbl.title.text = Suit.SecondDesc
        else
            tbl.title.text = Suit.Desc
        end
    else
        tbl.title.text = SafeStringFormat3(LocalString.GetString("下一级    %d级套装"), Suit.IntensifyLevel)
    end

    local splitstr = string.match(tbl.score.text, "%s+")
    tbl.addition.text = self:GetSuitPropertyAddText(Suit.AddProperty, splitstr)
    tbl.score.text = (LocalString.GetString("装备评分") .. splitstr .. "+") .. Suit.Score

    local equipnum = self:GetEquipNum(Suit)
    tbl.desc.text = SafeStringFormat3(LocalString.GetString("穿戴%d件强化%d级"), equipnum, Suit.IntensifyLevel)
    CUICommonDef.UpdatePerFectionIcons(tbl.star, Suit.Perfection, Suit.IntensifyLevel)

    if index > 1 then
        local equips = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Body)
        local count = 0
        CommonDefs.ListIterate(equips, DelegateFactory.Action_object(function(___value)
            local equip = ___value
            local item = CItemMgr.Inst:GetById(equip.itemId)
            if CEquipmentIntensifyMgr.Inst:CheckEquipTypeCanIntensify(equip.itemId) and item.Equip.IntensifyLevel >= Suit.IntensifyLevel and item.Equip.Duration > 0 and item.Equip.IsLostSoul == 0 then
                local addvalue = item.Equip:GetIntensifyTotalValue()
                local nextperfection = CEquipmentIntensifyMgr.Inst:CalcEquipIntensifyPerfectionByLevel(item.Equip,
                    Suit.IntensifyLevel)
                local perfection = math.max(item.Equip.Perfection, nextperfection)
                if perfection >= Suit.Perfection and addvalue >= Suit.IntensifyNumber then
                    count = count + 1
                end
            end
        end))
        tbl.desc.text = SafeStringFormat3(LocalString.GetString("穿戴%d件强化%d级（%d/%d）"), equipnum, Suit.IntensifyLevel, count, equipnum)
    end
end
-- 颜色品质文本
function LuaEquipmentNewSuitWnd:GetQualityText(quality)
    local color = QualityColor.GetRGBValue(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), quality))
    local colorstr = NGUIText.EncodeColor24(color)
    local colorname = EquipmentTemplate_Color.GetData(quality).Name
    return SafeStringFormat3(LocalString.GetString("品质为[c][%s]%s或%s以上[-][/c]的装备"), colorstr, colorname, colorname)
end
-- 更新锻造信息
function LuaEquipmentNewSuitWnd:UpdateForge(tbl, index, forgeSuitId)
    local Suit = ExtraEquip_Suit.GetData(forgeSuitId)
    if not Suit then
        tbl.go:SetActive(false)
        return
    end
    tbl.go:SetActive(true)

    if index == 1 then
        tbl.title.text = SafeStringFormat3(LocalString.GetString("锻造%d级套装"), Suit.ForgeLevel)
    else
        tbl.title.text = SafeStringFormat3(LocalString.GetString("下一级    %d级套装"), Suit.ForgeLevel)
    end

    local splitstr = string.match(tbl.score.text, "%s+")
    local propword = nil
    if not System.String.IsNullOrEmpty(Suit.PropWord) then
        propword = tonumber(Suit.PropWord)
    end
    tbl.addition.text = self:GetSuitPropertyAddText(Suit.AddProperty, splitstr, propword)
    tbl.score.text = (LocalString.GetString("装备评分") .. splitstr .. "+") .. Suit.Score

    local equipnum = Suit.EquipNum
    tbl.desc.text = SafeStringFormat3(LocalString.GetString("穿戴%d件锻造%d级"), equipnum, Suit.ForgeLevel)
    local qualitylimit = (Suit.Color and Suit.Color > 0) and Suit.Color or 1
    tbl.quality.text = self:GetQualityText(qualitylimit)

    if index > 1 then
        local equips = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Body)
        local count = 0
        CommonDefs.ListIterate(equips, DelegateFactory.Action_object(function(___value)
            local equip = ___value
            local item = CItemMgr.Inst:GetById(equip.itemId).Equip
            local level = item.ForgeLevel and item.ForgeLevel or 0
            if item.IsFeiShengAdjusted and item.IsFeiShengAdjusted > 0 and item.AdjustedFeiShengForgeLevel then
                level = item.AdjustedFeiShengForgeLevel
            end
            local quality = item.Color and EnumToInt(item.Color) or 0
            if level >= Suit.ForgeLevel and quality >= qualitylimit then count = count + 1 end
        end))
        tbl.desc.text = SafeStringFormat3(LocalString.GetString("穿戴%d件锻造%d级（%d/%d）"), equipnum, Suit.ForgeLevel, count, equipnum)
    end
end
-- 更新信息
function LuaEquipmentNewSuitWnd:UpdateData(intensifySuitId, intensifySuitFxIndex, forgeSuitId)
    self:UpdateIntensify(self.m_Intensify[1], 1, intensifySuitId, intensifySuitFxIndex)
    self:UpdateIntensify(self.m_Intensify[2], 2, intensifySuitId + 1, 0)
    self:UpdateForge(self.m_Forge[1], 1, forgeSuitId)
    self:UpdateForge(self.m_Forge[2], 2, forgeSuitId + 1)
    local height = NGUIMath.CalculateRelativeWidgetBounds(self.IntensifyTable.transform).size.y
    self.IntensifyBg.height = math.floor((height + 22.5))
    height = NGUIMath.CalculateRelativeWidgetBounds(self.ForgeTable.transform).size.y
    self.ForgeBg.height = math.floor((height + 22.5))
end
function LuaEquipmentNewSuitWnd:OnUpdateData(obj)
    if not CLuaEquipmentSuitInfoMgr.IsShowMainPlayerSuit then return end
    if obj == nil or not (TypeIs(obj, typeof(CClientMainPlayer))) then return end
    local intensifySuitId = 0
    local intensifySuitFxIndex = 0
    local forgeSuitId = 0
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.ItemProp ~= nil then
        intensifySuitId = CClientMainPlayer.Inst.ItemProp.IntesifySuitId
        intensifySuitFxIndex = CClientMainPlayer.Inst.ItemProp.IntesifySuitFxIndex
        forgeSuitId = CClientMainPlayer.Inst.ItemProp.ExtraEquipForgeSuitId
    end
    self:UpdateData(intensifySuitId, intensifySuitFxIndex, forgeSuitId)
end
function LuaEquipmentNewSuitWnd:OnEnable()
    g_ScriptEvent:AddListener("EquipmentSuitUpdate", self, "OnUpdateData")
    g_ScriptEvent:AddListener("ExtraEquipForgeSuitUpdate", self, "OnUpdateData")
end
function LuaEquipmentNewSuitWnd:OnDisable()
    g_ScriptEvent:RemoveListener("EquipmentSuitUpdate", self, "OnUpdateData")
    g_ScriptEvent:RemoveListener("ExtraEquipForgeSuitUpdate", self, "OnUpdateData")
end

--@region UIEvent

--@endregion UIEvent

