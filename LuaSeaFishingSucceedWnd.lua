local CUIFx = import "L10.UI.CUIFx"

local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local CUITexture = import "L10.UI.CUITexture"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"

LuaSeaFishingSucceedWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSeaFishingSucceedWnd, "GamePlayNameLabel", "GamePlayNameLabel", UILabel)
RegistChildComponent(LuaSeaFishingSucceedWnd, "Grid", "Grid", GameObject)
RegistChildComponent(LuaSeaFishingSucceedWnd, "AwardItem", "AwardItem", GameObject)
RegistChildComponent(LuaSeaFishingSucceedWnd, "AwardScoreLabel", "AwardScoreLabel", UILabel)
RegistChildComponent(LuaSeaFishingSucceedWnd, "SpecialFx", "SpecialFx", CUIFx)
RegistChildComponent(LuaSeaFishingSucceedWnd, "SucceedFx", "SucceedFx", CUIFx)

--@endregion RegistChildComponent end

function LuaSeaFishingSucceedWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.AwardItem:SetActive(false)
end

function LuaSeaFishingSucceedWnd:Init()
    self.SucceedFx:LoadFx("fx/ui/prefab/UI_haidiaotiaozhanchenggong_01.prefab")
    local gameplayId = LuaSeaFishingMgr.m_CurHaiDiaoGameplayId
    local difficulty = LuaSeaFishingMgr.m_SeaFishingGameplays[gameplayId] 

    LuaSeaFishingMgr.m_LastDifficult = difficulty

    local setting = HaiDiaoFuBen_Settings.GetData()
    local mailId = 0
    local awardScore = 0
    if gameplayId == setting.SpecialGameplayId then
        mailId = setting.SpecialGameplayRewardMail
        awardScore = setting.SpecialSeaFishingScoreAward
        self.SpecialFx:LoadFx("fx/ui/prefab/UI_haidiaotiaozhanchenggong_02.prefab")
    else
        if not LuaSeaFishingMgr.m_LastDifficult or LuaSeaFishingMgr.m_LastDifficult == 0 then
            LuaSeaFishingMgr.m_LastDifficult = 1
        end
        local index = (LuaSeaFishingMgr.m_LastDifficult-1) * 2 + 1
        mailId = setting.GameplayRewardMails[index]
        awardScore = setting.SeaFishingScoreAward[LuaSeaFishingMgr.m_LastDifficult-1]
    end

    Extensions.RemoveAllChildren(self.Grid.transform)

    if mailId ~= 0 then
		local maildata = Mail_Mail.GetData(mailId)
		local itemstr = maildata.Items
		for cronStr in string.gmatch(itemstr, "([^;]+);?") do
			local t = {}
			local itemId,count,bind = string.match(cronStr, "(%d+),(%d+),(%d+)")
			itemId,count,bind= tonumber(itemId),tonumber(count),tonumber(bind)
			local item = Item_Item.GetData(itemId)
			local award = NGUITools.AddChild(self.Grid,self.AwardItem)
            award:SetActive(true)
			local icon = award.transform:Find("Icon"):GetComponent(typeof(CUITexture))
			icon:LoadMaterial(item.Icon)
            UIEventListener.Get(award).onClick = DelegateFactory.VoidDelegate(function (go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false,nil,AlignType.Default, 0, 0, 0, 0)
            end)
        
		end
	end
    self.Grid.transform:GetComponent(typeof(UIGrid)):Reposition()
    local difficultyName = Gameplay_Gameplay.GetData(gameplayId).Name
    local color = LuaSeaFishingMgr.DifficultColor[LuaSeaFishingMgr.m_LastDifficult]
    self.GamePlayNameLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]%s[-][/c]"),color,difficultyName)

    self.AwardScoreLabel.text = SafeStringFormat3("[c][%s]+%d[-][/c]",color,awardScore)
end

--@region UIEvent

--@endregion UIEvent

