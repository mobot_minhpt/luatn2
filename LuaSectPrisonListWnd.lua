local UILabel = import "UILabel"
local Object = import "System.Object"

local QnTableView = import "L10.UI.QnTableView"
local GameObject = import "UnityEngine.GameObject"
local QnCheckBox = import "L10.UI.QnCheckBox"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local Vector3 = import "UnityEngine.Vector3"
local Profession=import "L10.Game.Profession"

LuaSectPrisonListWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSectPrisonListWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaSectPrisonListWnd, "ReleaseBtn", "ReleaseBtn", GameObject)
RegistChildComponent(LuaSectPrisonListWnd, "AllSelectBtn", "AllSelectBtn", GameObject)
RegistChildComponent(LuaSectPrisonListWnd, "VoidLabel", "VoidLabel", GameObject)
RegistChildComponent(LuaSectPrisonListWnd, "BtnLabel", "BtnLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaSectPrisonListWnd,"m_TableViewDataSource")
RegistClassMember(LuaSectPrisonListWnd,"m_PlayerData")
RegistClassMember(LuaSectPrisonListWnd,"m_CheckBoxList")
RegistClassMember(LuaSectPrisonListWnd,"m_IsFullSected")

function LuaSectPrisonListWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ReleaseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReleaseBtnClick()
	end)


	
	UIEventListener.Get(self.AllSelectBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAllSelectBtnClick()
	end)


    --@endregion EventBind end
end

function LuaSectPrisonListWnd:Init()
    local function initItem(item,index)
        if index % 2 == 1 then
            item:SetBackgroundTexture("common_bg_mission_background_n")
        else
            item:SetBackgroundTexture("common_bg_mission_background_s")
        end
        self:InitItem(item,index)
    end

    self.m_IsFullSected = false
    self.m_TableViewDataSource = DefaultTableViewDataSource.CreateByCount(0,initItem)
    self.TableView.m_DataSource = self.m_TableViewDataSource
    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)

    if LuaZongMenMgr.m_SectPrisonListInfo then
        self:InitInfo()
    end
end

function LuaSectPrisonListWnd:OnSelectAtRow(index)
    local data = self.m_PlayerData[index+1]
    CPlayerInfoMgr.ShowPlayerPopupMenu(data.Id, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, 
        CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
end

function LuaSectPrisonListWnd:InitItem(item,index)
    local data = self.m_PlayerData[index+1]

    -- 获取节点
    local tf = item.transform

    local playerNameLabel = FindChild(tf,"PlayerNameLabel"):GetComponent(typeof(UILabel))
    local levelLabel = FindChild(tf,"LevelLabel"):GetComponent(typeof(UILabel))
    local zongPaiLabel = FindChild(tf,"ZongPaiLabel"):GetComponent(typeof(UILabel))
    local bangHuiLabel = FindChild(tf,"BangHuiLabel"):GetComponent(typeof(UILabel))
    local timeLabel = FindChild(tf,"TimeLabel"):GetComponent(typeof(UILabel))
    local checkBox = FindChild(tf,"CheckBox"):GetComponent(typeof(QnCheckBox))
    local clsSprite = FindChild(tf,"ClsSprite"):GetComponent(typeof(UISprite))

    clsSprite.spriteName = Profession.GetIconByNumber(data.Cls)
    playerNameLabel.text = data.Name
    levelLabel.text = "lv." .. tostring(data.Level)
    zongPaiLabel.text = tostring(data.ZongPai)
    bangHuiLabel.text = tostring(data.BangHui)
    timeLabel.text = LuaZongMenMgr:ScondToString(data.ReleaseTime)

    checkBox:SetSelected(false, true)
    checkBox.OnValueChanged = DelegateFactory.Action_bool(
        function(select)
            self:CheckBtnStatus()
        end
    )

    table.insert(self.m_CheckBoxList, checkBox)
end

-- info: playerId, name, class, grade, xianFanStatus, sectName, guildName, remainingDetentionTime
function LuaSectPrisonListWnd:InitInfo()
    local infos = LuaZongMenMgr.m_SectPrisonListInfo
    if infos == nil then
        return
    end

    self.VoidLabel:SetActive(infos.Count == 0)
    if infos.Count == 0 then
        CUICommonDef.SetActive(self.AllSelectBtn, false, true)
        CUICommonDef.SetActive(self.ReleaseBtn, false, true)
    end

    self.m_PlayerData = {}
    for i=0, infos.Count-8, 8 do
        local m = {}
        m.Id = infos[i]
        m.Name = infos[i+1]
        m.Cls = infos[i+2]
        m.Level = infos[i+3]
        m.XianFanStatus = infos[i+4]
        m.ZongPai = infos[i+5]
        m.BangHui = infos[i+6]
        m.ReleaseTime = infos[i+7]
        table.insert(self.m_PlayerData, m)
    end

    self.m_CheckBoxList = {}
    self.m_TableViewDataSource.count = #self.m_PlayerData
    self.TableView:ReloadData(true,false)
end

function LuaSectPrisonListWnd:CheckBtnStatus()
	local isFullSelected = true
    if self.m_CheckBoxList == nil then
        return
    end

    for i=1, #self.m_CheckBoxList do
        if not self.m_CheckBoxList[i].Selected then
            isFullSelected = false
        end
    end

    if isFullSelected then
        self.BtnLabel.text = LocalString.GetString("取消全选")
        self.m_IsFullSected = true
    else
        self.BtnLabel.text = LocalString.GetString("全选")
        self.m_IsFullSected = false
    end
end

function LuaSectPrisonListWnd:OnEnable()
	g_ScriptEvent:AddListener("SectPrisonListInfo", self, "InitInfo")
end

function LuaSectPrisonListWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SectPrisonListInfo", self, "InitInfo")
end

--@region UIEvent

function LuaSectPrisonListWnd:OnReleaseBtnClick()
    if self.m_CheckBoxList==nil or #self.m_CheckBoxList == 0 then
        g_MessageMgr:ShowMessage("Sect_Prison_Not_Choose_Player")
        return
    end

    -- 发送RPC
    if CClientMainPlayer.Inst then
        local select = CreateFromClass(MakeGenericClass(List, Object))
        for i=1, #self.m_CheckBoxList do
            if self.m_CheckBoxList[i].Selected then
                CommonDefs.ListAdd(select, typeof(UInt64), self.m_PlayerData[i].Id)
            end
        end

        if select.Count == 0 then
            g_MessageMgr:ShowMessage("Sect_Prison_Not_Choose_Player")
        else
            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Sect_Prison_Release_Confirm", select.Count), function()
                Gac2Gas.OfficerReleaseLaoFangBeiZhuoPlayers(CClientMainPlayer.Inst.BasicProp.SectId, MsgPackImpl.pack(select), CClientMainPlayer.Inst.Id)
            end, nil, nil, nil, false)
        end
    end
end

function LuaSectPrisonListWnd:OnAllSelectBtnClick()
    if self.m_CheckBoxList==nil or #self.m_CheckBoxList == 0 then
        return
    end

    -- 设置状态
    local curSelectStatus = self.m_IsFullSected
    for i=1,#self.m_CheckBoxList do
        self.m_CheckBoxList[i]:SetSelected(not curSelectStatus, true)
    end

    self:CheckBtnStatus()
end


--@endregion UIEvent
