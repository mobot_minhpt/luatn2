require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local CItemMgr = import "L10.Game.CItemMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local UILabel = import "UILabel"
local UIScrollView = import "UIScrollView"
local UIGrid = import "UIGrid"
local Extensions = import "Extensions"
local CUITexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"
local Item_Item = import "L10.Game.Item_Item"
local DelegateFactory = import "DelegateFactory"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local CItem = import "L10.Game.CItem"
local NGUIMath = import "NGUIMath"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUIText = import "NGUIText"

LuaExchangeYuanBaoWithBaoShiWnd = class()
RegistClassMember(LuaExchangeYuanBaoWithBaoShiWnd,"m_closeBtn")
RegistClassMember(LuaExchangeYuanBaoWithBaoShiWnd,"m_Background")
RegistClassMember(LuaExchangeYuanBaoWithBaoShiWnd,"m_ScrollView")
RegistClassMember(LuaExchangeYuanBaoWithBaoShiWnd,"m_Grid")
RegistClassMember(LuaExchangeYuanBaoWithBaoShiWnd,"m_Template")
RegistClassMember(LuaExchangeYuanBaoWithBaoShiWnd,"m_AddSubButton")
RegistClassMember(LuaExchangeYuanBaoWithBaoShiWnd,"m_ExchangeButton")

RegistClassMember(LuaExchangeYuanBaoWithBaoShiWnd,"m_ItemObjs")
RegistClassMember(LuaExchangeYuanBaoWithBaoShiWnd,"m_SelectedTemplateId")
RegistClassMember(LuaExchangeYuanBaoWithBaoShiWnd,"m_SelectedValue")


function LuaExchangeYuanBaoWithBaoShiWnd:Awake()
    
end

function LuaExchangeYuanBaoWithBaoShiWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaExchangeYuanBaoWithBaoShiWnd:Init()
	self.m_closeBtn = self.transform:Find("Anchor/Offset/CloseButton").gameObject
	CommonDefs.AddOnClickListener(self.m_closeBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

	self.m_Background = self.transform:Find("Anchor/Offset/Background"):GetComponent(typeof(UISprite))
	self.m_ScrollView = self.transform:Find("Anchor/Offset/Items/ItemsScrollView"):GetComponent(typeof(UIScrollView))
	self.m_Grid = self.transform:Find("Anchor/Offset/Items/ItemsScrollView/Grid"):GetComponent(typeof(UIGrid))
	self.m_Template = self.transform:Find("Anchor/Offset/Items/ItemsScrollView/ItemCell").gameObject
	self.m_AddSubButton = self.transform:Find("Anchor/Offset/QnIncreaseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
	self.m_ExchangeLabel = self.transform:Find("Anchor/Offset/YuanBao/YuanBaoNumLabel"):GetComponent(typeof(UILabel))
	self.m_ExchangeButton = self.transform:Find("Anchor/Offset/ExchangeButton").gameObject
	self.m_Template:SetActive(false)

	self.m_AddSubButton.onValueChanged = DelegateFactory.Action_uint(function (value)
		self:OnAddSubValueChanged(value)
	end)

	CommonDefs.AddOnClickListener(self.m_ExchangeButton, DelegateFactory.Action_GameObject(function(go) self:OnExchangeButtonClick() end), false)

	self:ShowGems()
end

function LuaExchangeYuanBaoWithBaoShiWnd:ShowGems()
	self.m_ItemObjs = {}
	self.m_SelectedTemplateId = nil
	self.m_SelectedValue = 0
	Extensions.RemoveAllChildren(self.m_Grid.transform)
	local gemsInfo = self:GetGemsInfo()
	for _,val in pairs(gemsInfo) do
		local templateId = val.templateId
		local count = val.count
		local go = CUICommonDef.AddChild(self.m_Grid.gameObject, self.m_Template)
		go:SetActive(true)
		table.insert(self.m_ItemObjs, {id = templateId, amount = count, obj = go})

		local icon = go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
		local amountLabel = go.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
		local disableSprite = go.transform:Find("DisableSprite"):GetComponent(typeof(UISprite))
		local qualitySprite = go.transform:Find("QualitySprite"):GetComponent(typeof(UISprite)) --用不到
		local selectedSprite = go.transform:Find("SelectedSprite"):GetComponent(typeof(UISprite))
		local bindSprite = go.transform:Find("BindSprite"):GetComponent(typeof(UISprite)) --用不到
		local template = Item_Item.GetData(templateId)
		icon:LoadMaterial(template.Icon)
		if count > 1 then
			amountLabel.text = tostring(count)
		else
			amountLabel.text = nil
		end
		disableSprite.enabled = (not CItem.GetMainPlayerIsFit(templateId, false))
		selectedSprite.enabled = false

		CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function(go) self:OnItemClick(go) end), false)
	end
	self.m_Grid:Reposition()
	self.m_ScrollView:ResetPosition()

	self.m_AddSubButton:SetMinMax(0,0,1)
	self.m_AddSubButton:SetValue(0, true)
end

function LuaExchangeYuanBaoWithBaoShiWnd:OnAddSubValueChanged(newValue)
	self.m_SelectedValue = newValue
	if self.m_SelectedTemplateId and CommonDefs.DictContains(GameSetting_Common_Wapper.BaoShiExchangeYuanBaoInfo, typeof(uint), self.m_SelectedTemplateId) then
        local val = CommonDefs.DictGetValue(GameSetting_Common_Wapper.BaoShiExchangeYuanBaoInfo, typeof(uint), self.m_SelectedTemplateId)
        self.m_ExchangeLabel.text = tostring(newValue * val)
    else
    	self.m_ExchangeLabel.text = nil
    end
end

function LuaExchangeYuanBaoWithBaoShiWnd:OnItemClick(go)
	for _,val in pairs(self.m_ItemObjs) do
		local templateId = val.id
		local amount = val.amount
		local itemGo = val.obj
		local selectedSprite = itemGo.transform:Find("SelectedSprite"):GetComponent(typeof(UISprite))
		if itemGo == go then
			selectedSprite.enabled = true
			self.m_SelectedTemplateId = templateId
			self.m_AddSubButton:SetMinMax(1,amount,1)
			self.m_AddSubButton:SetValue(1, true)

			local b1 = NGUIMath.CalculateRelativeWidgetBounds(self.m_Background.transform);
            local b2 = NGUIMath.CalculateRelativeWidgetBounds(self.m_Background.transform, self.m_closeBtn.transform)
            local height = b1.size.y
            local worldCenterY = self.m_Background.transform:TransformPoint(b1.center).y
            --TDDO check 
            --b1:Encapsulate(b2)
            local width = b1.size.x
            local worldCenterX = self.m_Background.transform:TransformPoint(b1.center).x
            CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, CItemInfoMgrAlignType.Right, worldCenterX, worldCenterY, width, height)
		else
			selectedSprite.enabled = false
		end
	end
end

function LuaExchangeYuanBaoWithBaoShiWnd:GetGemsInfo()
	local result = {}
	local mainplayer = CClientMainPlayer.Inst
    if mainplayer then
		-- bag
	    local bagSize = mainplayer.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
	    for i=1,bagSize do
	    	local itemId = mainplayer.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
	    	local item = CItemMgr.Inst:GetById(itemId)
	    	if item and item.IsItem and item.IsBinded and item.Item.Type == EnumItemType_lua.Gem then
	    		if CommonDefs.DictContains(GameSetting_Common_Wapper.BaoShiExchangeYuanBaoInfo, typeof(uint), item.TemplateId) then
		    		if result[item.TemplateId] then
		    			result[item.TemplateId] = result[item.TemplateId] + item.Amount
		    		else
		    			result[item.TemplateId] = item.Amount
		    		end
		    	end
	    	end
	    end
	end

	local sortedResult = {}
	for k,v in pairs(result) do
		table.insert(sortedResult, {templateId=k, count=v})
	end

	table.sort(sortedResult, function (a, b)
		return a.templateId < b.templateId --result中已经对TemplateId进行了去重处理
	end)

	return sortedResult -- TemplateId, Count
end

function LuaExchangeYuanBaoWithBaoShiWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
end

function LuaExchangeYuanBaoWithBaoShiWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
end

function LuaExchangeYuanBaoWithBaoShiWnd:OnSendItem(args)
	local itemId = args[0]
	local item = CItemMgr.Inst:GetById(itemId)
	if item and item.IsItem and item.IsBinded and item.Item.Type == EnumItemType_lua.Gem then
		--refresh
		self:ShowGems()
	end
end

function LuaExchangeYuanBaoWithBaoShiWnd:OnSetItemAt(args)
	local place = args[0]
	local pos = args[1]
	local oldItemId = args[2]
	local newItemId = args[3]

	if place ~= EnumItemPlace.Bag then return end

	local oldItem = CItemMgr.Inst:GetById(oldItemId)
	local newItem = CItemMgr.Inst:GetById(newItemId)

	if oldItem and oldItem.IsItem and oldItem.IsBinded and oldItem.Item.Type == EnumItemType_lua.Gem then
		--refresh
		self:ShowGems()
	elseif newItem and newItem.IsItem and newItem.IsBinded and newItem.Item.Type == EnumItemType_lua.Gem then
		--refresh
		self:ShowGems()
	end
end

function LuaExchangeYuanBaoWithBaoShiWnd:OnExchangeButtonClick()
	if self.m_SelectedTemplateId and self.m_SelectedValue and  self.m_SelectedValue>0 then
		local item = Item_Item.GetData(self.m_SelectedTemplateId)
		if item and CommonDefs.DictContains(GameSetting_Common_Wapper.BaoShiExchangeYuanBaoInfo, typeof(uint), self.m_SelectedTemplateId) then
	        local val = CommonDefs.DictGetValue(GameSetting_Common_Wapper.BaoShiExchangeYuanBaoInfo, typeof(uint), self.m_SelectedTemplateId)
	        local yuanbao = self.m_SelectedValue * val
	        local coloredName = SafeStringFormat3("[%s]%s[-]", NGUIText.EncodeColor24(CItem.GetColor(self.m_SelectedTemplateId)), item.Name)
	        local msg = g_MessageMgr:FormatMessage("BaoShi_Exchange_YuanBao_Confirm", self.m_SelectedValue, coloredName, yuanbao)
			MessageWndManager.ShowOKCancelMessage(msg, 
				DelegateFactory.Action(function() Gac2Gas.RequestExchangeYuanBaoWithBaoShi(self.m_SelectedTemplateId, self.m_SelectedValue) end),
			 	nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
	    end	
	else
		g_MessageMgr:ShowMessage("Need_Select_BaoShi_To_Exchange_YuanBao")
	end
end

return LuaExchangeYuanBaoWithBaoShiWnd
