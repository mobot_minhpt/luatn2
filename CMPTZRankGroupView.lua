-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CMPTZRankGroupListItem = import "L10.UI.CMPTZRankGroupListItem"
local CMPTZRankGroupView = import "L10.UI.CMPTZRankGroupView"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local Object = import "System.Object"
local PlayerGradeGroup = import "L10.Game.PlayerGradeGroup"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CMPTZRankGroupView.m_InitList_CS2LuaHook = function (this) 

    if this.inited then
        return
    end
    this.inited = true
    Extensions.RemoveAllChildren(this.grid.transform)
    CommonDefs.ListClear(this.items)
    local defaultGo = CUICommonDef.AddChild(this.grid.gameObject, this.itemTemplate)
    defaultGo:SetActive(true)
    local defaultItem = CommonDefs.GetComponent_GameObject_Type(defaultGo, typeof(CMPTZRankGroupListItem))
    UIEventListener.Get(defaultGo).onClick = MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this)
    defaultItem.Text = LocalString.GetString("全部")
    CommonDefs.ListAdd(this.items, typeof(CMPTZRankGroupListItem), defaultItem)
    do
        local i = 0
        while i < PlayerGradeGroup.Groups.Length do
            local go = CUICommonDef.AddChild(this.grid.gameObject, this.itemTemplate)
            go:SetActive(true)
            local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CMPTZRankGroupListItem))
            item.Text = PlayerGradeGroup.GetGroupNameByIndex(i)
            UIEventListener.Get(go).onClick = MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this)
            CommonDefs.ListAdd(this.items, typeof(CMPTZRankGroupListItem), item)
            i = i + 1
        end
    end
    this.grid:Reposition()
    this.scrollView:ResetPosition()
end
CMPTZRankGroupView.m_Init_CS2LuaHook = function (this, cls) 

    this:InitList()
    this.curSelectedIndex = 0

    if CMPTZRankGroupView.s_EnableDefaultSelectMainPlayerGradeGroup then
        if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Class == cls then
            local level = CClientMainPlayer.Inst.HasFeiSheng and CClientMainPlayer.Inst.XianShenLevel or CClientMainPlayer.Inst.Level
            this.curSelectedIndex = PlayerGradeGroup.GetGroupIndexByGrade(level) + 1
        end
    end

    if this.curSelectedIndex >= 0 and this.curSelectedIndex < this.items.Count then
        this:OnItemClick(this.items[this.curSelectedIndex].gameObject)
    end
end
CMPTZRankGroupView.m_OnItemClick_CS2LuaHook = function (this, go) 

    do
        local i = 0
        while i < this.items.Count do
            local selected = (this.items[i].gameObject == go)
            this.items[i].Selected = selected
            if selected then
                this.curSelectedIndex = i
                if this.OnGroupSelected ~= nil then
                    GenericDelegateInvoke(this.OnGroupSelected, Table2ArrayWithCount({this.curSelectedIndex}, 1, MakeArrayClass(Object)))
                end
            end
            i = i + 1
        end
    end
end
