local UIGrid = import "UIGrid"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UILabel = import "UILabel"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local QnTableView=import "L10.UI.QnTableView"
local CSortButton = import "L10.UI.CSortButton"
local QnRadioBox = import "L10.UI.QnRadioBox"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local Constants = import "L10.Game.Constants"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"

LuaGuildTerritorialWarsSituationWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "GetScoreSpeedLabel", "GetScoreSpeedLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "OccupyNumLabel", "OccupyNumLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "ScoreScrollView", "ScoreScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "LeftGrid", "LeftGrid", UIGrid)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "MapButton", "MapButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "LeftTemplate", "LeftTemplate", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "RightGrid", "RightGrid", UIGrid)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "RightTemplate", "RightTemplate", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "Tabs", "Tabs", UITabBar)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "ScoreInfoView", "ScoreInfoView", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "RankDataView", "RankDataView", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "TeleportToGuildLeaderButton", "TeleportToGuildLeaderButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "RankTableView", "RankTableView", QnTableView)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "RankDataViewHeader", "RankDataViewHeader", QnRadioBox)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "ScoreSpeedDetailButton", "ScoreSpeedDetailButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "PlayerInfoNode1", "PlayerInfoNode1", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "PlayerInfoNode2", "PlayerInfoNode2", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "PlayerInfoNode3", "PlayerInfoNode3", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsSituationWnd, "PlayerInfoNode4", "PlayerInfoNode4", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaGuildTerritorialWarsSituationWnd,"m_SortFlags")
RegistClassMember(LuaGuildTerritorialWarsSituationWnd,"m_RankList")
RegistClassMember(LuaGuildTerritorialWarsSituationWnd,"m_SortFlagIndex")

function LuaGuildTerritorialWarsSituationWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.MapButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMapButtonClick()
	end)

    UIEventListener.Get(self.TeleportToGuildLeaderButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTeleportToGuildLeaderButtonClick()
	end)

    UIEventListener.Get(self.ScoreSpeedDetailButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnScoreSpeedDetailButtonClick()
	end)

    UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

    self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabsTabChange(index)
	end)

    --@endregion EventBind end
end

function LuaGuildTerritorialWarsSituationWnd:Init()
    self:InitRankDataView()
    self:InitLeftView()
    self:InitRightView()
    self.Tabs:ChangeTab(0, false)
end

function LuaGuildTerritorialWarsSituationWnd:InitRankDataView()
    self.m_SortFlags = {}
    for i = 1, self.RankDataViewHeader.m_RadioButtons.Length do
        self.m_SortFlags[i] = 1
    end
    self.RankTableView.m_DataSource = DefaultTableViewDataSource.Create(function() 
        return self.m_RankList and #self.m_RankList or 0
    end, function(item, index)
        self:InitTableViewItem(item, index)
    end)

    self.RankTableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnSelectRankTableViewAtRow(row)
    end)
    self.RankDataViewHeader.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
		self:OnSelectRankDataView(btn, index)
	end)
    self.RankDataViewHeader:Awake()
    self.RankDataViewHeader:On_Click(nil)
end

function LuaGuildTerritorialWarsSituationWnd:SortRankList()
    if not self.m_RankList then return end
    local sortKeyArray = {"kill","beKill","damage","ce","relive","ctrl","rmCtrl","damageTaken","heal"}
    local flag = self.m_SortFlags[self.m_SortFlagIndex]
    local sortKey = sortKeyArray[self.m_SortFlagIndex]
    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    table.sort(self.m_RankList,function(a,b)
        if myId == a.playerId then 
            return true
        elseif myId == b.playerId then 
            return false
        end
        if not sortKey or not flag then
            return a.playerId < b.playerId
        else
            local val = a[sortKey] - b[sortKey]
            if val == 0 then
                return a.playerId < b.playerId
            end
            return (val * flag) > 0
        end
    end)
    self.RankTableView:ReloadData(true, false)
    self:InitPlayerInfoNode(self.PlayerInfoNode1, "kill")
    self:InitPlayerInfoNode(self.PlayerInfoNode2, "ce")
    self:InitPlayerInfoNode(self.PlayerInfoNode3, "ctrl")
    self:InitPlayerInfoNode(self.PlayerInfoNode4, "heal")
end

function LuaGuildTerritorialWarsSituationWnd:InitPlayerInfoNode(node, key)
    local portrait = node.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
    local nameLabel = node.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local valLabel = node.transform:Find("ValLabel"):GetComponent(typeof(UILabel))
    local list = {}
    for i = 1, #self.m_RankList do
        table.insert(list, self.m_RankList[i])
    end
    if CClientMainPlayer.Inst then
        portrait:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName, false)
        nameLabel.text = CClientMainPlayer.Inst.Name
        valLabel.text = 0
        nameLabel.color = Color.green
    end
    if #list > 0 then
        local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
        table.sort(list, function (a,b)
            local v1, v2 = a[key], b[key]
            if v1 == v2 then
                return a.playerId < b.playerId
            end
            return v1 > v2
        end)
        local data = list[1]
        portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(data.class, data.gender, -1), false)
        nameLabel.text = data.playerName
        valLabel.text = self:GetRankValueStr(data[key])
        nameLabel.color = data.playerId == myId and Color.green or Color.white
        UIEventListener.Get(portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
        end)
    end
end

function LuaGuildTerritorialWarsSituationWnd:InitTableViewItem(item, index)
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local classSprite = item.transform:Find("NameLabel/ClassSprite"):GetComponent(typeof(UISprite))
    local waiYuanTex = item.transform:Find("NameLabel/WaiYuanTex").gameObject
    local headLabel2 = item.transform:Find("HeadLabel2"):GetComponent(typeof(UILabel))
    local headLabel3 = item.transform:Find("HeadLabel3"):GetComponent(typeof(UILabel))
    local headLabel4 = item.transform:Find("HeadLabel4"):GetComponent(typeof(UILabel))
    local headLabel5 = item.transform:Find("HeadLabel5"):GetComponent(typeof(UILabel))
    local headLabel6 = item.transform:Find("HeadLabel6"):GetComponent(typeof(UILabel))
    local headLabel7 = item.transform:Find("HeadLabel7"):GetComponent(typeof(UILabel))
    local headLabel8 = item.transform:Find("HeadLabel8"):GetComponent(typeof(UILabel))
    local headLabel9 = item.transform:Find("HeadLabel9"):GetComponent(typeof(UILabel))
    local headLabel10 = item.transform:Find("HeadLabel10"):GetComponent(typeof(UILabel))
    local itemSprite = item.transform:GetComponent(typeof(UISprite))
    local mySprite = item.transform:Find("My")

    local data = self.m_RankList[index + 1]
    local isMe = CClientMainPlayer.Inst and (data.playerId == CClientMainPlayer.Inst.Id)
    local labelColor = isMe and Color.green or Color.white
    nameLabel.color = labelColor
    headLabel2.color = labelColor
    headLabel3.color = labelColor
    headLabel4.color = labelColor
    headLabel5.color = labelColor
    headLabel6.color = labelColor
    headLabel7.color = labelColor
    headLabel8.color = labelColor
    headLabel9.color = labelColor
    headLabel10.color = labelColor
    itemSprite.spriteName =  index % 2 == 0 and Constants.NewOddBgSprite or Constants.NewEvenBgSprite
    mySprite.gameObject:SetActive(isMe)
    nameLabel.text = data.playerName
    classSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), data.class))
    waiYuanTex.gameObject:SetActive(data.isForeignAid)
    headLabel2.text = self:GetRankValueStr(data.kill)
    headLabel3.text = self:GetRankValueStr(data.beKill)
    headLabel4.text = self:GetRankValueStr(data.damage)
    headLabel5.text = self:GetRankValueStr(data.ce)
    headLabel6.text = self:GetRankValueStr(data.relive)
    headLabel7.text = self:GetRankValueStr(data.ctrl)
    headLabel8.text = self:GetRankValueStr(data.rmCtrl)
    headLabel9.text = self:GetRankValueStr(data.damageTaken)
    headLabel10.text = self:GetRankValueStr(data.heal)

    item:GetComponent(typeof(UISprite)).spriteName = index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite
end

function LuaGuildTerritorialWarsSituationWnd:GetRankValueStr(val)
    local d =  val / 10000
    if d >= 1 then
        return SafeStringFormat3(LocalString.GetString("%d万"), d)
    end
    return math.floor(val)
end

function LuaGuildTerritorialWarsSituationWnd:OnSelectRankTableViewAtRow(row)
    local data = self.m_RankList[row + 1]
    local playerId = data.playerId
    CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
end

function LuaGuildTerritorialWarsSituationWnd:InitLeftView()
    self.LeftTemplate.gameObject:SetActive(false)
    local curScoreInfo = LuaGuildTerritorialWarsMgr.m_CurScoreInfo
    local score = curScoreInfo.currentScore
    local occupyNum = curScoreInfo.gridCount
    local mapNum = GuildTerritoryWar_Setting.GetData().MaxOccupyAreaNum
    local getScoreSpeed = curScoreInfo.scoreSpeed
    self.ScoreLabel.text = score
    
    local extraSpeed = 0
    if curScoreInfo.fromSlaveSpeed then
        for i = 0, curScoreInfo.fromSlaveSpeed.Count - 1,2 do
            extraSpeed = extraSpeed + curScoreInfo.fromSlaveSpeed[i + 1]
        end
    end
    if curScoreInfo.toMasterSpeed then
        for i = 0, curScoreInfo.toMasterSpeed.Count - 1,2 do
            extraSpeed = extraSpeed - curScoreInfo.toMasterSpeed[i + 1]
        end
    end
    local getGetScoreSpeedUpSprite = self.GetScoreSpeedLabel.transform:Find("UpSprite").gameObject
    local getGetScoreSpeedDownSprite = self.GetScoreSpeedLabel.transform:Find("DownSprite").gameObject
    getGetScoreSpeedUpSprite.gameObject:SetActive(extraSpeed > 0)
    getGetScoreSpeedDownSprite.gameObject:SetActive(extraSpeed < 0)
    -- self.ScoreSpeedDetailButton.gameObject:SetActive(extraSpeed ~= 0)
    self.GetScoreSpeedLabel.text = SafeStringFormat3(LocalString.GetString("+%d积分/分钟"),getScoreSpeed)
    self.GetScoreSpeedLabel.color = Color.white
    if extraSpeed > 0 then
        self.GetScoreSpeedLabel.color = NGUIText.ParseColor24("00ff60", 0)
    elseif extraSpeed < 0 then
        self.GetScoreSpeedLabel.color = NGUIText.ParseColor24("ff5050", 0)
    end
    --self.OccupyNumLabel.text = SafeStringFormat3(LocalString.GetString("占领地块%d/%d"),occupyNum,mapNum)
    Extensions.RemoveAllChildren(self.LeftGrid.transform)
    local list = LuaGuildTerritorialWarsMgr.m_ScoreEventInfo
    for i = 1,#list do
        if list[i].designData.TaskWeek == LuaGuildTerritorialWarsMgr.m_WeekCount then
            local obj = NGUITools.AddChild(self.LeftGrid.gameObject,self.LeftTemplate)
            self:InitLeftItem(obj, list[i], i)
        end
    end
    self.LeftGrid:Reposition()
    self.ScoreScrollView:ResetPosition()
end

function LuaGuildTerritorialWarsSituationWnd:InitRightItem(obj, data, rankIndex)
    local name = data.guildName
    local serverName = data.serverName
    local score = data.score
    local guildId = data.guildId
    local occupyNum = data.occupyNum
    local myGuildId = LuaGuildTerritorialWarsMgr.m_CurScoreInfo.myGuildId
    local isMyGuild = guildId == myGuildId
    local labelColor = NGUIText.ParseColor24(isMyGuild and "00ff60" or "ffffff", 0)
    local noneRank = score == 0 and occupyNum == 0

    local firstSprite = obj.transform:Find("FirstSprite").gameObject
    local secondSprite = obj.transform:Find("SecondSprite").gameObject
    local thirdSprite = obj.transform:Find("ThirdSprite").gameObject
    local nameLabel = obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local rankLabel = obj.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local scoreLabel = obj.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    local serverNameLabel = obj.transform:Find("ServerNameLabel"):GetComponent(typeof(UILabel))
    local occupyNumLabel = obj.transform:Find("OccupyNumLabel"):GetComponent(typeof(UILabel))
    local memberCountLabel = obj.transform:Find("MemberCountLabel"):GetComponent(typeof(UILabel))
    local mainCityGo = obj.transform:Find("MainCity").gameObject
    local guildDeleteGo = obj.transform:Find("GuildDelete").gameObject
    local slaveGo = obj.transform:Find("Slave").gameObject

    obj.gameObject:SetActive(true)
    firstSprite.gameObject:SetActive(rankIndex == 1 and not noneRank)
    secondSprite.gameObject:SetActive(rankIndex == 2 and not noneRank)
    thirdSprite.gameObject:SetActive(rankIndex == 3 and not noneRank)
    rankLabel.gameObject:SetActive(rankIndex > 3 or noneRank)
    rankLabel.text = noneRank and LocalString.GetString("—") or rankIndex
    nameLabel.text = name
    mainCityGo.gameObject:SetActive(data.isMainCityOwner)
    guildDeleteGo.gameObject:SetActive(data.guildDeleted)
    slaveGo.gameObject:SetActive(data.isSlave and not data.guildDeleted)
    memberCountLabel.text = data.memberCount
    --nameLabel.text = SafeStringFormat3("%s%s", name, data.guildDeleted and LocalString.GetString("[ff5050](解散)") or "")
    scoreLabel.text = math.floor(score)
    serverNameLabel.text = serverName
    occupyNumLabel.text = occupyNum
    rankLabel.color = labelColor
    nameLabel.color = labelColor
    scoreLabel.color = labelColor
    serverNameLabel.color = labelColor
    occupyNumLabel.color = labelColor
    memberCountLabel.color = labelColor
end

function LuaGuildTerritorialWarsSituationWnd:InitRightView()
    self.RightTemplate.gameObject:SetActive(false)
    local list = LuaGuildTerritorialWarsMgr.m_GuildScoreInfo
    for i = 1,#list do
        local obj = NGUITools.AddChild(self.RightGrid.gameObject,self.RightTemplate)
        self:InitRightItem(obj, list[i],i)
    end

    self.RightGrid:Reposition()
end

function LuaGuildTerritorialWarsSituationWnd:InitLeftItem(obj, data, index)
    local finished = data.isFinished 
    local taskDescription = data.designData.Desc

    obj.gameObject:SetActive(true)
    local indexRoot = obj.transform:Find("IndexRoot"):GetComponent(typeof(UIPanel))
    local taskLabel = indexRoot.transform:Find("TaskLabel"):GetComponent(typeof(UILabel))
    local scoreLabel = indexRoot.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    local lineSprite1 = taskLabel.transform:Find("LineSprite")
    local lineSprite2 = scoreLabel.transform:Find("LineSprite")

    taskLabel.text = SafeStringFormat3(taskDescription, data.progress, data.designData.Count)
    scoreLabel.text = SafeStringFormat3(LocalString.GetString("+%d积分"), data.designData.RewardScore)
    taskLabel.alpha = finished and 0.3 or 1
    scoreLabel.alpha = finished and 0.3 or 1
    lineSprite1.gameObject:SetActive(finished)
    lineSprite2.gameObject:SetActive(finished)
end

--@region UIEvent

function LuaGuildTerritorialWarsSituationWnd:OnMapButtonClick()
    CUIManager.CloseUI(CLuaUIResources.GuildTerritorialWarsMapWnd)
	Gac2Gas.RequestTerritoryWarMapOverview()
end

function LuaGuildTerritorialWarsSituationWnd:OnTeleportToGuildLeaderButtonClick()
    local msg = g_MessageMgr:FormatMessage("TerritoryWarTeleportToGuildLeader_Confirm")
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		Gac2Gas.TerritoryWarTeleportToGuildLeader()
	end),nil,LocalString.GetString("前往"),LocalString.GetString("取消"),false)
end

function LuaGuildTerritorialWarsSituationWnd:OnScoreSpeedDetailButtonClick()
    CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsScoreSpeedDetailWnd)
end

function LuaGuildTerritorialWarsSituationWnd:OnTabsTabChange(index)
    self.ScoreInfoView:SetActive(index == 0)
	self.RankDataView:SetActive(index == 1)
    if index == 1 then
        Gac2Gas.RequestTerritoryWarFightDetail()
    end
end

function LuaGuildTerritorialWarsSituationWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("GuildTerritorialWars_Rank_ReadMe")
end

function LuaGuildTerritorialWarsSituationWnd:OnSelectRankDataView(btn, btnindex)
    local index = btnindex + 1
    self.m_SortFlagIndex = index
    self.m_SortFlags[index] = - self.m_SortFlags[index]
    for i = 1,#self.m_SortFlags do
        if i ~= index then
            self.m_SortFlags[i] = 1
        end
    end
    local sortBtn = TypeAs(btn, typeof(CSortButton))
    sortBtn:SetSortTipStatus(self.m_SortFlags[index] < 0)
    self:SortRankList()
end
--@endregion UIEvent

function LuaGuildTerritorialWarsSituationWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSendTerritoryWarFightDetail", self, "OnSendTerritoryWarFightDetail")
end

function LuaGuildTerritorialWarsSituationWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSendTerritoryWarFightDetail", self, "OnSendTerritoryWarFightDetail")
end

function LuaGuildTerritorialWarsSituationWnd:OnSendTerritoryWarFightDetail()
    self.m_RankList = LuaGuildTerritorialWarsMgr.m_FightDetailInfo
    self:SortRankList()
end