-- Auto Generated!!
local CGuildLeagueMgr = import "L10.Game.CGuildLeagueMgr"
local CMiniMapGuildLeagueBossTemplate = import "L10.UI.CMiniMapGuildLeagueBossTemplate"
local Monster_Monster = import "L10.Game.Monster_Monster"
local UISprite = import "UISprite"
local Vector3 = import "UnityEngine.Vector3"
CMiniMapGuildLeagueBossTemplate.m_Init_CS2LuaHook = function (this, info) 
    this.mInfo = info
    local data = Monster_Monster.GetData(info.templateId)
    if data ~= nil then
        this.icon:LoadNPCPortrait(data.HeadIcon, false)
    end
    if data.Boss == 1 then
        this.icon.transform.localScale = Vector3(0.4,0.4,1)
        this.slider.gameObject:SetActive(false) --小boss不显示血条
        return
    else
        this.icon.transform.localScale = Vector3(1,1,1)
        this.slider.gameObject:SetActive(true) 
    end
    
    this.slider.value = info.hp / info.hpFull
    local myForce = CGuildLeagueMgr.Inst:GetMyForce()
    if info.force == 1 then
        (TypeAs(this.slider.foregroundWidget, typeof(UISprite))).spriteName = CMiniMapGuildLeagueBossTemplate.JiangJunWeiHpName
    elseif info.force == 0 then
        (TypeAs(this.slider.foregroundWidget, typeof(UISprite))).spriteName = CMiniMapGuildLeagueBossTemplate.XuanXiaoNuHpName
    else
        (TypeAs(this.slider.foregroundWidget, typeof(UISprite))).spriteName = CMiniMapGuildLeagueBossTemplate.EnemyHpName
    end
end
