local QnButton = import "L10.UI.QnButton"
local GameObject = import "UnityEngine.GameObject"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local QnModelPreviewer = import "L10.UI.QnModelPreviewer"
local UITabBar = import "L10.UI.UITabBar"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CCommonSelector = import "L10.UI.CCommonSelector"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local UILabel = import "UILabel"
local LuaGameObject = import "LuaGameObject"
local DelegateFactory = import "DelegateFactory"
local UIEventListener = import "UIEventListener"
local LocalString = import "LocalString"
local EWeaponVisibleReason = import "L10.Engine.EWeaponVisibleReason"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EnumYingLingState = import "L10.Game.EnumYingLingState"
local UIGrid = import "UIGrid"

local Vector3 = import "UnityEngine.Vector3"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local AlignType = import "CPlayerInfoMgr+AlignType"

LuaFuxiAniPreviewWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFuxiAniPreviewWnd, "ApdView1", "ApdView1", UIGrid)
RegistChildComponent(LuaFuxiAniPreviewWnd, "HelpButton", "HelpButton", GameObject)
RegistChildComponent(LuaFuxiAniPreviewWnd, "LbEmptyDes", "LbEmptyDes", UILabel)
RegistChildComponent(LuaFuxiAniPreviewWnd, "ShopRoot", "ShopRoot", QnAdvanceGridView)
RegistChildComponent(LuaFuxiAniPreviewWnd, "ModelPreviewer", "ModelPreviewer", QnModelPreviewer)
RegistChildComponent(LuaFuxiAniPreviewWnd, "tabButtons", "tabButtons", UITabBar)
RegistChildComponent(LuaFuxiAniPreviewWnd, "UploadButton", "UploadButton", GameObject)
RegistChildComponent(LuaFuxiAniPreviewWnd, "LbUploadCount", "LbUploadCount", UILabel)
RegistChildComponent(LuaFuxiAniPreviewWnd, "DelBtn", "DelBtn", QnButton)
RegistChildComponent(LuaFuxiAniPreviewWnd, "DelBtn2", "DelBtn2", QnButton)
RegistChildComponent(LuaFuxiAniPreviewWnd, "InfoButton", "InfoButton", QnButton)
RegistChildComponent(LuaFuxiAniPreviewWnd, "RenameBtn", "RenameBtn", QnButton)
RegistChildComponent(LuaFuxiAniPreviewWnd, "ShareBtn1", "ShareBtn1", QnButton)
RegistChildComponent(LuaFuxiAniPreviewWnd, "CollectBtn", "CollectBtn", QnButton)
RegistChildComponent(LuaFuxiAniPreviewWnd, "PageCtrl", "PageCtrl", QnAddSubAndInputButton)
RegistChildComponent(LuaFuxiAniPreviewWnd, "DropdownBtn", "DropdownBtn", CCommonSelector)
RegistChildComponent(LuaFuxiAniPreviewWnd, "ExpiryDate", "ExpiryDate", GameObject)

--@endregion RegistChildComponent end

--@region RegistMember

RegistClassMember(LuaFuxiAniPreviewWnd,"m_sortIndex")
RegistClassMember(LuaFuxiAniPreviewWnd,"m_selectMotionData")
RegistClassMember(LuaFuxiAniPreviewWnd,"m_webPage")
RegistClassMember(LuaFuxiAniPreviewWnd,"m_webPageDatas")  --其他人的数据
RegistClassMember(LuaFuxiAniPreviewWnd,"m_webPlayerDatas") --自己上传的数据
RegistClassMember(LuaFuxiAniPreviewWnd,"m_playerdatas") --自己收藏的数据
RegistClassMember(LuaFuxiAniPreviewWnd,"m_tabIndex")
RegistClassMember(LuaFuxiAniPreviewWnd,"m_haveShareData")
RegistClassMember(LuaFuxiAniPreviewWnd,"m_shareData")
RegistClassMember(LuaFuxiAniPreviewWnd,"m_uploadState")

--@endregion

function LuaFuxiAniPreviewWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.HelpButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnHelpButtonClick(go)
	end)

	self.tabButtons.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OntabButtonsTabChange(index)
	end)

	UIEventListener.Get(self.UploadButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUploadButtonClick(go)
	end)

	UIEventListener.Get(self.DelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDelBtnClick(go)
	end)

    UIEventListener.Get(self.DelBtn2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnDelBtn2Click(go)
    end)
	
	UIEventListener.Get(self.RenameBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRenameBtnClick(go)
	end)

	UIEventListener.Get(self.ShareBtn1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtn1Click(go)
	end)

	UIEventListener.Get(self.CollectBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCollectBtnClick(go)
	end)

    UIEventListener.Get(self.InfoButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go) 
        self:OnInfoBtnClick(go)
    end)

    --@endregion EventBind end

    self.PageCtrl.onValueChanged = DelegateFactory.Action_uint(function(value)
        self:TurnPage(value)
    end)

    local tagnames = {LocalString.GetString("热门"),LocalString.GetString("最新")}
    local textList = Table2List(tagnames, MakeGenericClass(List, cs_string))
	self.DropdownBtn:Init(textList, DelegateFactory.Action_int(function (index)
        self.m_sortIndex = index
        self.DropdownBtn:SetName(tagnames[index+1])
        local tag = self:GetSortTag(self.m_sortIndex)
        LuaFuxiAniMgr.ReqFuxiAllDataInfo(1,tag)
    end))
end

function LuaFuxiAniPreviewWnd:OnEnable()
    g_ScriptEvent:AddListener("OnGetFuxiPlayerOwnData", self, "OnGetFuxiPlayerOwnData")
    g_ScriptEvent:AddListener("OnGetFuxiPlayerDataInfo", self, "OnGetFuxiPlayerDataInfo")
    g_ScriptEvent:AddListener("OnGetFuxiAllDataInfo", self, "OnGetFuxiAllDataInfo")
    g_ScriptEvent:AddListener("OnGetFuxiAniSpeData", self, "OnGetFuxiAniSpeData")
end

function LuaFuxiAniPreviewWnd:OnDisable()
    LuaFuxiAniMgr.ShareMotionID = nil
    --清空分享信息
    g_ScriptEvent:RemoveListener("OnGetFuxiPlayerOwnData", self, "OnGetFuxiPlayerOwnData")
    g_ScriptEvent:RemoveListener("OnGetFuxiPlayerDataInfo", self, "OnGetFuxiPlayerDataInfo")
    g_ScriptEvent:RemoveListener("OnGetFuxiAllDataInfo", self, "OnGetFuxiAllDataInfo")
    g_ScriptEvent:RemoveListener("OnGetFuxiAniSpeData", self, "OnGetFuxiAniSpeData")
end

function LuaFuxiAniPreviewWnd:Init()
    self.ModelPreviewer.ShowCloth = false
    self.m_sortIndex = 0
    self.m_selectMotionData = nil --选中的动作
    
    self.m_webPage = 0 --web服务器分页数据当前页,0表示无数据
    self.m_webPageDatas = {} --web服务器分页数据当前页,0表示无数据
    self.m_webPlayerDatas = {} --web服务器玩家数据

    self.m_playerdatas = {} --我的动作含收藏的所有数据

    self.m_tabIndex = 0 --界面页签，0:玩家动作；1:浏览其他
    self.m_haveShareData = false
    --是否是通过分享打开的
    self.m_shareData = nil --分享数据

    self.m_uploadState = 0 --0:锁定上传，1:正常消耗上传，2：无消耗上传，3替换上传

    self.ModelPreviewer.IsShowHighRes = true
    self.ModelPreviewer:PreviewMainPlayer(System.UInt32.MaxValue, 0, 0)

    --异步请求web上的我的上传数据
    LuaFuxiAniMgr.ReqFuxiPlayerDataInfo()
    --请求服务器数据我的收藏数据
    LuaFuxiAniMgr.InitFuxiData()
    --请求第一页其他数据
    self.PageCtrl:SetValue(1, true)

    if LuaFuxiAniMgr.ShareMotionID ~= nil then --点击分享链接打开时，直接切到浏览其他页面
        self.m_haveShareData = true
        self.tabButtons:ChangeTab(2, true)
        self.m_tabIndex = 2
        LuaFuxiAniMgr.ReqFuxiDataInfos({LuaFuxiAniMgr.ShareMotionID})
    else
        self.tabButtons:ChangeTab(0, false)
    end
end

function LuaFuxiAniPreviewWnd:GetSortTag(index)
    if index == 1 then
        return "new"
    end
    return "hot"
end

--[[
    @desc: web服务器返回玩家数据
    author:Codee
    time:2021-04-23 16:56:03
    --@datas: 
    @return:
]]
function LuaFuxiAniPreviewWnd:OnGetFuxiPlayerDataInfo(datas)
    self.m_webPlayerDatas = datas
    if not self.m_haveShareData then
        self:OntabButtonsTabChange(self.m_tabIndex)
    end
end

--[[
    @desc: web服务器返回分页数据
    author:Codee
    time:2021-04-23 16:56:57
    --@datas: 
    @return:
]]
function LuaFuxiAniPreviewWnd:OnGetFuxiAllDataInfo(res)
    self.m_webPageDatas = res
    if not self.m_webPageDatas.datas then
        self.m_webPageDatas.datas = {}
    end
    if self.m_haveShareData then
        if  self.m_shareData then
            table.insert(self.m_webPageDatas.datas,1,self.m_shareData)
            self:OntabButtonsTabChange(self.m_tabIndex)
        end
    else
        self:OntabButtonsTabChange(self.m_tabIndex)
    end
end

--[[
    @desc: web服务器返回指定数据
    author:Codee
    time:2021-04-23 18:15:23
    --@datas: 
    @return:
]]
function LuaFuxiAniPreviewWnd:OnGetFuxiAniSpeData(datas)
    if datas == nil then return end
    --将web数据与服务器数据合并
    self:MergeGameDataWithWebData(datas)

    if self.m_haveShareData and self.m_shareData == nil then --需要一个分享的数据
        if #datas == 1 then
            if datas[1].motionId == LuaFuxiAniMgr.ShareMotionID then
                self.m_shareData = datas[1]
                if self.m_webPageDatas and self.m_webPageDatas.datas then
                    table.insert(self.m_webPageDatas.datas,1,self.m_shareData)
                    self:OntabButtonsTabChange(self.m_tabIndex)
                end
            end
        end
    end
end

--@endregion

--@region UIEvent

function LuaFuxiAniPreviewWnd:OnHelpButtonClick(go)
    g_MessageMgr:ShowMessage("FUXI_DANCE_EXPRESSION_TIP")
end

function LuaFuxiAniPreviewWnd:OntabButtonsTabChange(index)
    self.m_selectMotionData = nil
    self.m_tabIndex = index
    if index == 0  then
        self:InitTab0()
    elseif index == 1 then
        self:InitTab1()
    else
        self:InitTab2()
    end
end

function LuaFuxiAniPreviewWnd:OnUploadButtonClick(go)
    Gac2Gas.FuXiDanceUploadPreCheck()
end

--删除我的收藏
function LuaFuxiAniPreviewWnd:OnDelBtnClick(go)
    if self.m_selectMotionData == nil then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("请先选择一个动作"))
        return
    end
    local msg = g_MessageMgr:FormatMessage("FUXIDANCE_DELETE_CONFIRM_NOTICE",self.m_selectMotionData.motionName)
    local okfunc = function()
        if self.m_selectMotionData.index == nil then
        else
            Gac2Gas.DeleteFuXiDanceMotion(self.m_selectMotionData.index,self.m_selectMotionData.motionId)
        end
    end
    g_MessageMgr:ShowOkCancelMessage(msg,okfunc,nil, nil, nil, false)
end

--删除我的上传视频
function LuaFuxiAniPreviewWnd:OnDelBtn2Click(go)
    if self.m_selectMotionData == nil then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("请先选择一个动作"))
        return
    end
    local msg = LocalString.GetString("删除原视频文件需要前往网页端个人中心进行处理，是否立即前往？")
    local okfunc = function()
        LuaFuxiAniMgr.OpenUploadWebPage()
    end
    g_MessageMgr:ShowOkCancelMessage(msg,okfunc,nil, nil, nil, false)
end

function LuaFuxiAniPreviewWnd:OnInfoBtnClick(go)
    if self.m_selectMotionData == nil then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("请先选择一个动作"))
        return
    end
    local playerId = self.m_selectMotionData.roleId
    if playerId > 0 then
        CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
    end
end

function LuaFuxiAniPreviewWnd:OnRenameBtnClick(go)
    if self.m_selectMotionData == nil then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("请先选择一个动作"))
        return
    end
    self:ShowRenameWnd()
end

function LuaFuxiAniPreviewWnd:OnShareBtn1Click(go)
    self:ProcessShare(go)
end

function LuaFuxiAniPreviewWnd:OnCollectBtnClick(go)
    if self.m_selectMotionData == nil then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("请先选择一个动作"))
        return
    end

    if self:IsFullCount() then 
        g_MessageMgr:ShowMessage("FUXIDANCE_COUNT_LIMIT_NOTICE") 
        return
    end

    if self:IsTimeWarn(self.m_selectMotionData) then
        local msg = g_MessageMgr:FormatMessage("CUSTOM_STRING",LocalString.GetString("该动作即将过期，过期后无法继续使用，确定要下载吗？") )--todo:该动作将于x天后过期，过期后无法继续使用，确定要下载吗？
        local okfunc = function()
            LuaFuxiAniMgr.ReqDownload(self.m_selectMotionData.motionId)
        end
        g_MessageMgr:ShowOkCancelMessage(msg,okfunc,nil, nil, nil, false)
    else
        LuaFuxiAniMgr.ReqDownload(self.m_selectMotionData.motionId)
    end
end

--从服务器获取的个人收藏数据，需要请求网站数据并合并
function LuaFuxiAniPreviewWnd:OnGetFuxiPlayerOwnData()
    self.m_playerdatas = {}
    local tbs ={}
    local datas = LuaFuxiAniMgr.FuxiData
    for i=1,#datas do
        local item = datas[i]
        table.insert(self.m_playerdatas,item)
        if datas[i].roleId ~= CClientMainPlayer.Inst.Id and datas[i].motionId ~= 0 then
            table.insert(tbs,datas[i].motionId)
        end
    end
    if #tbs > 0 then
        LuaFuxiAniMgr.ReqFuxiDataInfos(tbs)
    end
end

function LuaFuxiAniPreviewWnd:MergeGameDataWithWebData(datas)
    for i=1,#self.m_playerdatas do
        local data = self.m_playerdatas[i]
        local pagedata = self:FindWebData(data.motionId,datas)
        if pagedata then
            if data.status ~= pagedata.status then 
                LuaFuxiAniMgr.ReqSyncStatus(data.index,data.motionId,pagedata.status)
            end
            data.downloadNum = pagedata.downloadNum
            data.expire = pagedata.expire
            data.roleName = pagedata.roleName
            data.roleId = pagedata.roleId
        end
    end
end

function LuaFuxiAniPreviewWnd:IsFullCount()
    local datas = self.m_playerdatas
    if datas == nil then 
        return true
    end
    local len = #datas
    return len >= FuXiDance_Setting.GetData().FuxiDanceLimitCount
end

function LuaFuxiAniPreviewWnd:IsTimeWarn(data)
    if data.status == EnumFuxiDanceMotionStatus.eCheckSuccess then
        local lefttime = data.expire - CServerTimeMgr.Inst.timeStamp
        return lefttime <= 3600 * 24 and lefttime > 0
    else
        return false
    end
end

function LuaFuxiAniPreviewWnd:FindWebData(motionid,tb1)
    if tb1 ~= nil then
        for i=1,#tb1 do
            local data = tb1[i]
            if data.motionId == motionid then 
                return data
            end
        end
    end
    return nil
end

function LuaFuxiAniPreviewWnd:IsInMyList(motionid)
    if self.m_playerdatas == nil then return false end
    for i=1,#self.m_playerdatas do
        if self.m_playerdatas[i].motionId == motionid then 
            return true
        end
    end
    return false
end

--@endregion UIEvent

--[[
    @desc: 初始化玩家动作页
    author:Codee
    time:2021-04-23 17:30:09
    @return:
]]
function LuaFuxiAniPreviewWnd:InitTab0()
    self.m_selectMotionData = nil
    self.UploadButton.gameObject:SetActive(true)

    self.ShareBtn1.gameObject:SetActive(true)
    self.RenameBtn.gameObject:SetActive(true)
    self.DelBtn.gameObject:SetActive(true)
    self.DelBtn2.gameObject:SetActive(false)
    self.CollectBtn.gameObject:SetActive(false)
    self.InfoButton.gameObject:SetActive(true)
    self.LbUploadCount.gameObject:SetActive(true)
    self.PageCtrl.gameObject:SetActive(false)
    self.DropdownBtn.gameObject:SetActive(false)
    self.ApdView1:Reposition()

    self.LbEmptyDes.text = ""
    local datas = self.m_playerdatas
    if datas == nil or #datas == 0 then 
        self.LbEmptyDes.text = g_MessageMgr:FormatMessage("FUXIDANCE_MY_DANCE_EMPTY")
    end

    local maxcount = FuXiDance_Setting.GetData().FuxiDanceLimitCount
    self.LbUploadCount.text = #datas.."/"..maxcount

    self.ShopRoot.m_DataSource =
        DefaultTableViewDataSource.Create(
        function()
            return #datas
        end,
        function(item, index)
            self:InitItem1(item, datas[index + 1])
        end
    )
    self.ShopRoot:ReloadData(true, false)
end
--[[
    @desc: 初始化我的上传页面
    author:Codee
    time:2021-04-23 17:29:39
    --@datas: 
    @return:
]]
function LuaFuxiAniPreviewWnd:InitTab1()
    self.m_selectMotionData = nil
    self.UploadButton.gameObject:SetActive(true)

    self.ShareBtn1.gameObject:SetActive(true)
    self.RenameBtn.gameObject:SetActive(false)
    self.DelBtn.gameObject:SetActive(false)
    self.DelBtn2.gameObject:SetActive(true)
    self.CollectBtn.gameObject:SetActive(true)
    self.InfoButton.gameObject:SetActive(false)
    self.LbUploadCount.gameObject:SetActive(true)
    self.PageCtrl.gameObject:SetActive(false)
    self.DropdownBtn.gameObject:SetActive(false)
    self.ApdView1:Reposition()

    self.LbEmptyDes.text = ""
    local datas = self.m_webPlayerDatas
    if datas == nil or #datas == 0 then 
        self.LbEmptyDes.text = g_MessageMgr:FormatMessage("FUXIDANCE_MY_DANCE_EMPTY")
    end

    local maxcount = FuXiDance_Setting.GetData().FuxiDanceLimitCount
    self.LbUploadCount.text = #datas.."/"..maxcount

    self.ShopRoot.m_DataSource =
        DefaultTableViewDataSource.Create(
        function()
            return #datas
        end,
        function(item, index)
            self:InitItem1(item, datas[index + 1])
        end
    )
    self.ShopRoot:ReloadData(true, false)
end
--[[
    @desc: 初始化浏览其他页
    author:Codee
    time:2021-04-23 17:29:39
    --@datas: 
    @return:
]]
function LuaFuxiAniPreviewWnd:InitTab2()
    self.m_selectMotionData = nil
    self.ExpiryDate:SetActive(false)

    self.UploadButton.gameObject:SetActive(false)

    self.ShareBtn1.gameObject:SetActive(true)
    self.RenameBtn.gameObject:SetActive(false)
    self.DelBtn.gameObject:SetActive(false)
    self.DelBtn2.gameObject:SetActive(false)
    self.CollectBtn.gameObject:SetActive(true)
    self.InfoButton.gameObject:SetActive(true)
    self.LbUploadCount.gameObject:SetActive(false)
    self.PageCtrl.gameObject:SetActive(true)
    self.DropdownBtn.gameObject:SetActive(true)
    self.ApdView1:Reposition()

    self.LbEmptyDes.text = ""

	local datas = self.m_webPageDatas.datas or {}
	if #datas == 0 then 
        self.LbEmptyDes.text = g_MessageMgr:FormatMessage("FUXIDANCE_OTHER_DANCE_EMPTY")
    end

    self.PageCtrl:SetMinMax(1, self.m_webPageDatas.totalPage, 1)

    self.ShopRoot.m_DataSource =
        DefaultTableViewDataSource.Create(
        function()
            return #datas
        end,
        function(item, index)
            self:InitItem1(item, datas[index + 1])
        end
    )
    self.ShopRoot:ReloadData(true, false)

    if self.m_shareData ~= nil then 
        self.ShopRoot:SetSelectRow(0,true)
    end
end

function LuaFuxiAniPreviewWnd:InitItem1(item,data)
	local trans = item.transform
    local emptytrnas = trans:Find("Empty")
	local infotrans = trans:Find("Info")
	local lbname = LuaGameObject.GetChildNoGC(infotrans,"LbName").label
	local signgo = LuaGameObject.GetChildNoGC(infotrans,"Sign").gameObject

    local statusgo1 = LuaGameObject.GetChildNoGC(infotrans,"StatusChecking").gameObject
    local statusgo2 = LuaGameObject.GetChildNoGC(infotrans,"StatusCheckFaild").gameObject

	local authorgo = LuaGameObject.GetChildNoGC(infotrans,"Author").gameObject
	local lbauthorname = LuaGameObject.GetChildNoGC(authorgo.transform,"LbAuthorName").label
    local lbtimewarngo = LuaGameObject.GetChildNoGC(authorgo.transform,"LbTimeWarn").gameObject
    local lbtimeexpirego = LuaGameObject.GetChildNoGC(authorgo.transform,"LbTimeExpire").gameObject

    local downloadgo = LuaGameObject.GetChildNoGC(infotrans,"SpDownload").gameObject
	local lbdowncount = LuaGameObject.GetChildNoGC(downloadgo.transform,"LbDownCount").label
	local collectedgo = LuaGameObject.GetChildNoGC(downloadgo.transform,"Collected").gameObject
	local uncollectedgo = LuaGameObject.GetChildNoGC(downloadgo.transform,"UnCollected").gameObject

    infotrans.gameObject:SetActive(true)
    emptytrnas.gameObject:SetActive(false)

    lbname.text = data.motionName
    
    statusgo1:SetActive(data.status == EnumFuxiDanceMotionStatus.eChecking)
    statusgo2:SetActive(data.status == EnumFuxiDanceMotionStatus.eCheckFaild)
    downloadgo:SetActive(data.status == EnumFuxiDanceMotionStatus.eCheckSuccess)
    if data.status == EnumFuxiDanceMotionStatus.eCheckSuccess then
        local ct = self:IsInMyList(data.motionId)
        collectedgo:SetActive(ct)
        uncollectedgo:SetActive(not ct)
    end
    lbdowncount.text = tostring(math.max(data.downloadNum, 0))

    lbauthorname.color = data.isMine and Color(0,1,60/255, 0.5) or Color(1,1,1, 0.5)
    if data.status == EnumFuxiDanceMotionStatus.eDeleted or data.downloadNum < 0 then
        lbauthorname.color = Color(1,80/255,80/255,1)
        lbauthorname.text= LocalString.GetString("该动作已由原作者删除")
    else
        lbauthorname.text = data.roleName..' '..LocalString.GetString("上传")
    end

    local curtime = CServerTimeMgr.Inst.timeStamp
    lbtimewarngo:SetActive(self:IsTimeWarn(data))
    lbtimeexpirego:SetActive(data.outtime)
    
    if self.m_shareData ~= nil and self.m_shareData == data then
        signgo:SetActive(true)
    else
        signgo:SetActive(false)
    end

    item.OnSelected = DelegateFactory.Action_GameObject(function(go)
        self:OnMotionSelect(data)
    end)
end

function LuaFuxiAniPreviewWnd:PlayAni(data)
    if CClientMainPlayer.Inst.CurYingLingState == EnumYingLingState.eMonster or CClientMainPlayer.Inst.AppearanceProp.ResourceId > 0 then
        g_MessageMgr:ShowMessage("EXPRESSION_NO_ANI")
        return
    end
    if data.status == EnumFuxiDanceMotionStatus.eDeleted or data.outtime then
        return
    end
    local aniname = CClientMainPlayer.Inst:GetFuxiDanceName(data.motionId)
    local ro = self.ModelPreviewer.m_RO
    ro:SetWeaponVisible(EWeaponVisibleReason.Expression, false)
    ro.Frozen = false
    ro:DoAni(aniname, false, 0, 1,0.15,true,1)
    ro.AniEndCallback = DelegateFactory.Action(function()
        ro.AniEndCallback = nil
        ro:DoAni(ro.NewRoleStandAni, true, 0, 1, 0.15, true, 1)
    end)
end

function LuaFuxiAniPreviewWnd:OnMotionSelect(data)
    self.m_selectMotionData = data
    if data.status == EnumFuxiDanceMotionStatus.eChecking or data.status == EnumFuxiDanceMotionStatus.eCheckFaild then
        self.ShareBtn1.Enabled = false
        self.RenameBtn.Enabled = false
        self.DelBtn.Enabled = false
        self.CollectBtn.Enabled = false
    else
        self:PlayAni(data)
        self.ShareBtn1.Enabled = true
        self.RenameBtn.Enabled = true
        self.DelBtn.Enabled = true
        self.CollectBtn.Enabled = not self:IsInMyList(data.motionId)
    end

    self.ExpiryDate:SetActive(data.status == EnumFuxiDanceMotionStatus.eCheckSuccess)
    local timelabel = LuaGameObject.GetChildNoGC(self.ExpiryDate.transform,"TimeLabel").label
    timelabel.text = (CServerTimeMgr.ConvertTimeStampToZone8Time(data.expire)):ToString("yyyy-MM-dd hh:mm")
    if data.outtime then
        timelabel.color = Color(1,80/255,80/255,1)
    elseif self:IsTimeWarn(data) then
        timelabel.color = Color(1,254/255,145/255,1)
    else
        timelabel.color = Color(1,1,1,1)
    end
end

function LuaFuxiAniPreviewWnd:TurnPage(page)
    self.m_webPage = page
    local tag = self:GetSortTag(self.m_sortIndex)
    LuaFuxiAniMgr.ReqFuxiAllDataInfo(page, tag)
end

function LuaFuxiAniPreviewWnd:ProcessShare(go)
    if self.m_selectMotionData == nil then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("请先选择一个动作"))
        return
    end
    local textArray = {
        LocalString.GetString("至世界"),--1
        LocalString.GetString("至帮会"),--2
        LocalString.GetString("至队伍"),--3
        LocalString.GetString("至好友")--4
    }

    local selectShareItem =
        DelegateFactory.Action_int(
        function(idx)--0到4
            local index = idx+1
            if index == 4 then
                local callback = function (playerId)
                    LuaFuxiAniMgr.ShareMotionTo(index,self.m_selectMotionData.motionId,tostring(playerId))
                end
                CCommonPlayerListMgr.Inst:ShowFriendsToShareJingLingAnswer(DelegateFactory.Action_ulong(callback))
            else
                LuaFuxiAniMgr.ShareMotionTo(index,self.m_selectMotionData.motionId,"")
            end
        end
    )

    local t = {}
    for k, text in pairs(textArray) do
        local item = PopupMenuItemData(text, selectShareItem)
        table.insert(t, item)
    end
    local array = Table2Array(t, MakeArrayClass(PopupMenuItemData))

    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, CPopupMenuInfoMgr.AlignType.Right)
end

--@region UI相关

--[[
    @desc: 显示改名界面
    author:Codee
    time:2021-04-23 16:52:10
    @return:
]]
function LuaFuxiAniPreviewWnd:ShowRenameWnd()
    local uplimit = 4
    local content = LocalString.GetString("请输入新的动作名称(最多4个字)")
    local cost = 0
    if self.m_selectMotionData.roleId ~= CClientMainPlayer.Inst.Id then
        cost = FuXiDance_Setting.GetData().RenameFuxiDanceNeedSilver
    end
    
    CInputBoxMgr.ShowInputBoxWithCost(content, DelegateFactory.Action_string(function (str)
        if CUICommonDef.GetStrByteLength(str) > uplimit * 2 then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("输入的名字过长"))
            --g_MessageMgr:ShowMessage("LINGSHOU_NAME_TOO_LONG")
            return
        elseif str==nil or str=="" then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("输入的名字不能为空"))
            --g_MessageMgr:ShowMessage("LINGSHOU_NAME_NOT_NULL")
            return
        elseif not CWordFilterMgr.Inst:CheckName(str) then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("输入的名字含有不允许的字符"))
            --g_MessageMgr:ShowMessage("LingShou_Name_Violation")
            return
        end
        --采用新的名字
        if self.m_selectMotionData.index== nil then
        else
            Gac2Gas.ReNameFuXiDanceMotion(self.m_selectMotionData.index,self.m_selectMotionData.motionId,str)
        end
        CUIManager.CloseUI(CIndirectUIResources.InputBox)
    end),EnumMoneyType.YinPiao, cost,uplimit * 5, true, nil, nil)
end

--@endregion

function LuaFuxiAniPreviewWnd:GetGuideGo(methodname)
    if methodname == "GetGuide1" then
        return self.UploadButton.gameObject
    elseif methodname == "GetGuide2" then
        return self.tabButtons.transform:Find("qnSelectableButton2").gameObject
    end
end
