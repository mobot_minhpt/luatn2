require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UITexture = import "UITexture"
local DelegateFactory = import "DelegateFactory"
local CPreciousItemShareMgr = import "L10.Game.CPreciousItemShareMgr"

LuaEquipBaptizeShareWnd = class()
RegistClassMember(LuaEquipBaptizeShareWnd,"closeBtn")
RegistClassMember(LuaEquipBaptizeShareWnd,"snapshotTexture")
RegistClassMember(LuaEquipBaptizeShareWnd,"share2MengDaoBtn")


function LuaEquipBaptizeShareWnd:Awake()

end

function LuaEquipBaptizeShareWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaEquipBaptizeShareWnd:Init()
	self.closeBtn = self.transform:Find("Wnd_Bg_Secondary_2/CloseButton").gameObject
	self.snapshotTexture = self.transform:Find("Anchor/Texture"):GetComponent(typeof(UITexture))
	self.share2MengDaoBtn = self.transform:Find("Anchor/Table/Share2MengDaoButton").gameObject
	
	CommonDefs.AddOnClickListener(self.closeBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)
	CommonDefs.AddOnClickListener(self.share2MengDaoBtn, DelegateFactory.Action_GameObject(function(go) self:OnShare2MengDaoButtonClick(go) end), false)

	CUICommonDef.LoadLocalFile2Texture(self.snapshotTexture,CPreciousItemShareMgr.Inst.EquipBaptizeSnapshotPath,560,315)
end

function LuaEquipBaptizeShareWnd:OnShare2MengDaoButtonClick( go )
	CPreciousItemShareMgr.Inst:UploadBaptizeSnapshotToPersonalSpace()
end

function LuaEquipBaptizeShareWnd:OnDestroy( go )

end
