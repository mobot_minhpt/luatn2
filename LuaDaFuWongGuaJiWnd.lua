local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"

LuaDaFuWongGuaJiWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongGuaJiWnd, "Desc", "Desc", UILabel)
RegistChildComponent(LuaDaFuWongGuaJiWnd, "OKBtn", "OKBtn", GameObject)

--@endregion RegistChildComponent end

function LuaDaFuWongGuaJiWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.OKBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOKBtnClick()
	end)

    --@endregion EventBind end
end

function LuaDaFuWongGuaJiWnd:Init()
	self.Desc.text = g_MessageMgr:FormatMessage("DaFuWeng_GuaJiWnd_Desc")
end

--@region UIEvent

function LuaDaFuWongGuaJiWnd:OnOKBtnClick()
	Gac2Gas.DaFuWengSetGuaJi(false)
	CUIManager.CloseUI(CLuaUIResources.DaFuWongGuaJiWnd)
end

--@endregion UIEvent

