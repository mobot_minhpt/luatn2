local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local Title_Title = import "L10.Game.Title_Title"
local CTitleMgr = import "L10.Game.CTitleMgr"

LuaBiAnYingXueChooseWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaBiAnYingXueChooseWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaBiAnYingXueChooseWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaBiAnYingXueChooseWnd, "TitleLabel", "TitleLabel", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaBiAnYingXueChooseWnd,"m_TailList")
RegistClassMember(LuaBiAnYingXueChooseWnd,"m_ConditionSatisfiedList")
function LuaBiAnYingXueChooseWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_TailList = {}
    self.m_ConditionSatisfiedList = {}
end

function LuaBiAnYingXueChooseWnd:InitItem(item,row)
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local conditionLabel = item.transform:Find("ConditionLabel"):GetComponent(typeof(UILabel))
    local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local btn = item.transform:Find("GetBtn").gameObject
    local tailId = self.m_TailList[row+1]
    local data = Buff_zhixianbuff.GetData(tailId)
    local condition = ""
    local titleName = ""
    local accessName = ""
    if data then
        local buff = Buff_Buff.GetData(data.BuffID*100 + 1)
        if buff then
            nameLabel.text = buff.Name
        end
        local title = Title_Title.GetData(data.TitleID)
        if title then
            condition = SafeStringFormat3(LocalString.GetString("需拥有称号 %s"),title.Name)
            titleName = title.Name
            local co = CTitleMgr.Inst:GetTitleColor(title)
            titleName = SafeStringFormat3("#c%s%s#n",NGUIText.EncodeColor24(co),titleName)
            titleName = CUICommonDef.TranslateToNGUIText(titleName)
        end
        accessName = data.TitleAccess
        icon:LoadMaterial(data.Icon)
    end
    conditionLabel.text = condition
    local hasOwnTitle = self.m_ConditionSatisfiedList[tailId]
    conditionLabel.color = hasOwnTitle and NGUIText.ParseColor24("00ff60", 0) or NGUIText.ParseColor24("ffffff", 0)
    conditionLabel.alpha = hasOwnTitle and 1 or 0.3

    UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
        if hasOwnTitle then
            local tailId = self.m_TailList[row+1]
            local idx = Buff_zhixianbuff.GetData(tailId).TailID
            Gac2Gas.RequestUseItem(LuaZhuJueJuQingMgr.BiAnItemPlace, LuaZhuJueJuQingMgr.BiAnItemPos, LuaZhuJueJuQingMgr.BiAnItemId, tostring(idx))
        else
            g_MessageMgr:ShowMessage("CANNOT_EXCHANGETAIL_WHITHOUT_TITLE",titleName,accessName)
        end
    end)
end

function LuaBiAnYingXueChooseWnd:Init()
    if not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.PlayProp then return end
    
    Buff_zhixianbuff.Foreach(function(key,data)
        if data.TitleID and data.TitleID ~= 0 then
            table.insert(self.m_TailList,key)
            local titleId = data.TitleID
            local hasOwnTitle = CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.Titles, typeof(UInt32), titleId)
            self.m_ConditionSatisfiedList[key] = hasOwnTitle
        end
    end)

    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_TailList
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

    table.sort(self.m_TailList,function(a,b)
        local obtain1 = self.m_ConditionSatisfiedList[a] and 1 or 0
        local obtain2 = self.m_ConditionSatisfiedList[b] and 1 or 0
        if obtain1 == obtain2 then
            return a < b
        else
            return obtain1 > obtain2
        end
    end)

    self.TableView:ReloadData(false,false)
end

--@region UIEvent

--@endregion UIEvent

