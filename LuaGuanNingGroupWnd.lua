local NGUIText=import "NGUIText"
local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"

CLuaGuanNingGroupWnd = class()
RegistClassMember(CLuaGuanNingGroupWnd,"m_TableView1")
RegistClassMember(CLuaGuanNingGroupWnd,"m_TableView2")
RegistClassMember(CLuaGuanNingGroupWnd,"m_GroupCount")
RegistClassMember(CLuaGuanNingGroupWnd,"m_MyGroup")
RegistClassMember(CLuaGuanNingGroupWnd,"m_MyGroupServerNames")
RegistClassMember(CLuaGuanNingGroupWnd,"m_MyGroupServerIds")
RegistClassMember(CLuaGuanNingGroupWnd,"m_TitleLabel")

CLuaGuanNingGroupWnd.m_ServerGroup = 0
CLuaGuanNingGroupWnd.m_ServerNames = {}
function CLuaGuanNingGroupWnd:Init()
    CLuaGuanNingGroupWnd.m_ServerNames = {}
    CLuaGuanNingGroupWnd.m_ServerGroup = 0
    self.m_MyGroupServerNames = {}
    self.m_MyGroupServerIds = {}

    self.m_TitleLabel=self.transform:Find("Anchor/NameLabel"):GetComponent(typeof(UILabel))
    self.m_TitleLabel.text = ""

    self.m_TableView1 = self.transform:Find("Anchor/TableView1"):GetComponent(typeof(QnTableView))
    self.m_TableView2 = self.transform:Find("Anchor/TableView2"):GetComponent(typeof(QnTableView))

    self.m_TableView1.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.m_MyGroupServerNames
    end,
    function(item,row)
        local label = item.transform:Find("Label"):GetComponent(typeof(UILabel))
        label.text = self.m_MyGroupServerNames[row+1]
    end)

    self.m_TableView2.m_DataSource = DefaultTableViewDataSource.Create(function()
            return self.m_GroupCount
        end,
        function(item,row)
            local label = item.transform:Find("Label"):GetComponent(typeof(UILabel))
            label.text = SafeStringFormat3(LocalString.GetString("跨服关宁%d组"),row+1)
            local myServerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetMyServerId()
            if self.m_MyGroupServerIds[row+1] == myServerId then
                label.color = NGUIText.ParseColor24("ffc900", 0)
            else
                label.color = NGUIText.ParseColor24("83C9FF", 0)
            end
        end)
    self.m_TableView2.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        Gac2Gas.QueryCrossGnjcServerInfo(row+1)
    end)



    Gac2Gas.QueryCrossGnjcGroupInfo()
end

function CLuaGuanNingGroupWnd:OnEnable()
    g_ScriptEvent:AddListener("ReplyCrossGnjcGroupInfoQueryResult", self, "OnReplyCrossGnjcGroupInfoQueryResult")
    g_ScriptEvent:AddListener("ReplyCrossGnjcServerInfoQueryResult", self, "OnReplyCrossGnjcServerInfoQueryResult")
end

function CLuaGuanNingGroupWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReplyCrossGnjcGroupInfoQueryResult", self, "OnReplyCrossGnjcGroupInfoQueryResult")
    g_ScriptEvent:RemoveListener("ReplyCrossGnjcServerInfoQueryResult", self, "OnReplyCrossGnjcServerInfoQueryResult")
end

function CLuaGuanNingGroupWnd:OnReplyCrossGnjcGroupInfoQueryResult(group,count,ids,names)
    self.m_GroupCount=count
    self.m_MyGroup=group

    self.m_TableView2:ReloadData(true, false)
    self.m_TitleLabel.text = SafeStringFormat3(LocalString.GetString("跨服关宁%d组"),group)

    self.m_MyGroupServerNames = names
    self.m_MyGroupServerIds = ids

    self.m_TableView1:ReloadData(true, false)
end
function CLuaGuanNingGroupWnd:OnReplyCrossGnjcServerInfoQueryResult(group,t)
    CLuaGuanNingGroupWnd.m_ServerNames = t
    CLuaGuanNingGroupWnd.m_ServerGroup = group
    CUIManager.ShowUI(CLuaUIResources.GuanNingGroupInfoTip)
end