-- Auto Generated!!
local CQnReturnConfirm = import "L10.UI.CQnReturnConfirm"
local DelegateFactory = import "DelegateFactory"
local Gac2Gas = import "L10.Game.Gac2Gas"
local MessageWndManager = import "L10.UI.MessageWndManager"
CQnReturnConfirm.m_OnOkButtonClick_CS2LuaHook = function (this, go) 

    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Recharge_Return_Or_Not"), DelegateFactory.Action(function () 
        Gac2Gas.ConfirmQnRechargeReturn(true)
    end), nil, nil, nil, false)
end
