local CGroupIMMgr=import "L10.Game.CGroupIMMgr"

LuaGroupIMMgr = {}
 
function LuaGroupIMMgr:IsShiTuIMId(id)
    if CommonDefs.DictContains(CGroupIMMgr.Inst.m_GroupIMs, typeof(ulong), id) then
        local groupIM = CGroupIMMgr.Inst.m_GroupIMs[id]
        return groupIM.Type == CGroupIMMgr.GroupIMType_ShiTu
    end
    return false
end