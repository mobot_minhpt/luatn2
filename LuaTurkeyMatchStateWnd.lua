local Transform = import "UnityEngine.Transform"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UISlider = import "UISlider"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CMainCamera = import "L10.Engine.CMainCamera"
local Vector3 = import "UnityEngine.Vector3"
local CPos = import "L10.Engine.CPos"
local Utility = import "L10.Engine.Utility"
local CScene = import "L10.Game.CScene"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"

LuaTurkeyMatchStateWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaTurkeyMatchStateWnd, "LeftSlider", "LeftSlider", UISlider)
RegistChildComponent(LuaTurkeyMatchStateWnd, "LeftTopName", "LeftTopName", GameObject)
RegistChildComponent(LuaTurkeyMatchStateWnd, "LeftTopNameLab", "LeftTopNameLab", UILabel)
RegistChildComponent(LuaTurkeyMatchStateWnd, "RightSlider", "RightSlider", UISlider)
RegistChildComponent(LuaTurkeyMatchStateWnd, "RightHpLab", "RightHpLab", UILabel)
RegistChildComponent(LuaTurkeyMatchStateWnd, "RightTopName", "RightTopName", GameObject)
RegistChildComponent(LuaTurkeyMatchStateWnd, "RightTopNameLab", "RightTopNameLab", UILabel)
RegistChildComponent(LuaTurkeyMatchStateWnd, "RightBtn", "RightBtn", GameObject)
RegistChildComponent(LuaTurkeyMatchStateWnd, "LeftHpLab", "LeftHpLab", UILabel)
RegistChildComponent(LuaTurkeyMatchStateWnd, "Dynamic", "Dynamic", GameObject)
RegistChildComponent(LuaTurkeyMatchStateWnd, "State1", "State1", Transform)
RegistChildComponent(LuaTurkeyMatchStateWnd, "State2", "State2", Transform)
RegistChildComponent(LuaTurkeyMatchStateWnd, "SnowManSlider1", "SnowManSlider1", UISlider)
RegistChildComponent(LuaTurkeyMatchStateWnd, "SnowManSlider2", "SnowManSlider2", UISlider)
RegistChildComponent(LuaTurkeyMatchStateWnd, "SnowManHpLab1", "SnowManHpLab1", UILabel)
RegistChildComponent(LuaTurkeyMatchStateWnd, "SnowManHpLab2", "SnowManHpLab2", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaTurkeyMatchStateWnd, "m_TopAnchor1")
RegistClassMember(LuaTurkeyMatchStateWnd, "m_TopAnchor2")
RegistClassMember(LuaTurkeyMatchStateWnd, "m_CachePos1")
RegistClassMember(LuaTurkeyMatchStateWnd, "m_CachePos2")
RegistClassMember(LuaTurkeyMatchStateWnd, "m_Holders1")
RegistClassMember(LuaTurkeyMatchStateWnd, "m_Holders2")
RegistClassMember(LuaTurkeyMatchStateWnd, "m_LeftState")
RegistClassMember(LuaTurkeyMatchStateWnd, "m_RightState")
RegistClassMember(LuaTurkeyMatchStateWnd, "m_InitedHolder")
RegistClassMember(LuaTurkeyMatchStateWnd, "m_Threshold1")
RegistClassMember(LuaTurkeyMatchStateWnd, "m_Threshold2")

function LuaTurkeyMatchStateWnd:Awake()
    self.m_LeftState = self.LeftSlider.transform.parent
    self.m_RightState = self.RightSlider.transform.parent
    self.m_Holders1 = {}
    self.m_Holders2 = {}
    table.insert(self.m_Holders1, self.m_LeftState:Find("Phase/1"))
    table.insert(self.m_Holders1, self.m_RightState:Find("Phase/1"))
    table.insert(self.m_Holders1, self.State1:Find("Phase/1"))
    table.insert(self.m_Holders1, self.State2:Find("Phase/1"))
    table.insert(self.m_Holders2, self.m_LeftState:Find("Phase/2"))
    table.insert(self.m_Holders2, self.m_RightState:Find("Phase/2"))
    table.insert(self.m_Holders2, self.State1:Find("Phase/2"))
    table.insert(self.m_Holders2, self.State2:Find("Phase/2"))
    -- 323
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    UIEventListener.Get(self.RightBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self.RightBtn.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)

    self.LeftTopName:SetActive(false)
    self.RightTopName:SetActive(false)

    self.m_CachePos1 = self.State1.position
    self.m_CachePos2 = self.State2.position

    local thresholds = Christmas2022_Setting.GetData().SnowmanStageHpThresholds
    self.m_Threshold1 = thresholds[0]
    self.m_Threshold2 = thresholds[1]
    self.m_InitedHolder = false
end

function LuaTurkeyMatchStateWnd:Init()
    for i = 1, 2 do
        local info = LuaChristmas2022Mgr.m_SnowManInfos[i]
        if info then
            self:OnUpdateTurkeyMatchSnowmanHp(i, info[1], info[2], info[3])
        end
    end
    self:Update()
end

function LuaTurkeyMatchStateWnd:InitHolder(maxHp)
    if maxHp and self.m_InitedHolder == false then
        for i = 1, #self.m_Holders1 do
            LuaUtils.SetLocalPositionX(self.m_Holders1[i], 323 * self.m_Threshold1 / maxHp)
        end

        for i = 1, #self.m_Holders2 do
            LuaUtils.SetLocalPositionX(self.m_Holders2[i], 323 * self.m_Threshold2 / maxHp)
        end
    end
end

function LuaTurkeyMatchStateWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("UpdateTurkeyMatchSnowmanHp", self, "OnUpdateTurkeyMatchSnowmanHp")
end

function LuaTurkeyMatchStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("UpdateTurkeyMatchSnowmanHp", self, "OnUpdateTurkeyMatchSnowmanHp")
end

--@region UIEvent

--@endregion UIEvent

function LuaTurkeyMatchStateWnd:OnHideTopAndRightTipWnd()
    self.RightBtn.transform.localEulerAngles = Vector3(0, 0, 180)
end


function LuaTurkeyMatchStateWnd:OnUpdateTurkeyMatchSnowmanHp(snowmanTeamId, curHp, curHpFull, maxDamagePlayerName)
    self:InitHolder(curHpFull)
    if snowmanTeamId == 1 then
        self:UpdateSliderAndLab(self.LeftSlider, self.LeftHpLab, curHp, curHpFull)
        self:UpdateSliderAndLab(self.SnowManSlider1, self.SnowManHpLab1, curHp, curHpFull)
        if not System.String.IsNullOrEmpty(maxDamagePlayerName) then
            local title = self.LeftTopName.transform:Find("Title"):GetComponent(typeof(UIWidget))
            self.LeftTopName:SetActive(true)
            self.LeftTopNameLab.text = maxDamagePlayerName
            title:ResetAndUpdateAnchors()
        end
    else
        self:UpdateSliderAndLab(self.RightSlider, self.RightHpLab, curHp, curHpFull)
        self:UpdateSliderAndLab(self.SnowManSlider2, self.SnowManHpLab2, curHp, curHpFull)
        if not System.String.IsNullOrEmpty(maxDamagePlayerName) then
            local title = self.RightTopName.transform:Find("Title"):GetComponent(typeof(UIWidget))
            self.RightTopName:SetActive(true)
            self.RightTopNameLab.text = maxDamagePlayerName
            title:ResetAndUpdateAnchors()
        end
    end
end

function LuaTurkeyMatchStateWnd:UpdateSliderAndLab(slider, lab, val, maxVal)
    local percent = val / maxVal
    slider.value = percent
    lab.text = val
    lab.gameObject:SetActive(percent <= 0.5)
end

function LuaTurkeyMatchStateWnd:Update()
    if CScene.MainScene == nil or CRenderScene.Inst == nil then
        return
    end
    if self.m_TopAnchor1 == nil then
        local snowmanPositions = Christmas2022_Setting.GetData().SnowmanPositions
        self.m_TopAnchor1 = Utility.GridPos2WorldPos(snowmanPositions[0], snowmanPositions[1])
        self.m_TopAnchor1.y = self.m_TopAnchor1.y + 2.5
        self.m_TopAnchor2 = Utility.GridPos2WorldPos(snowmanPositions[3], snowmanPositions[4])
        self.m_TopAnchor2.y = self.m_TopAnchor2.y + 2.5
    end

    self.m_CachePos1 = self:UpdateSnowManHeadInfo(self.State1, self.m_TopAnchor1, self.m_CachePos1)
    self.m_CachePos2 = self:UpdateSnowManHeadInfo(self.State2, self.m_TopAnchor2, self.m_CachePos2)
end

function LuaTurkeyMatchStateWnd:UpdateSnowManHeadInfo(target, topAnchor, cachePos)
    local viewPos = CMainCamera.Main:WorldToViewportPoint(topAnchor)
    
    local screenPos = nil
    if viewPos and viewPos.z and viewPos.z > 0 and viewPos.x > 0 and viewPos.x < 1 and viewPos.y > 0 and viewPos.y < 1 then
        screenPos = CMainCamera.Main:WorldToScreenPoint(topAnchor)
    else  
        screenPos = Vector3(0, 3000, 0)
    end
    screenPos.z = 0
    local topWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)

    if cachePos.x ~= topWorldPos.x or cachePos.x ~= topWorldPos.x then
        target.position = topWorldPos
        cachePos = topWorldPos
    end
    return cachePos
end