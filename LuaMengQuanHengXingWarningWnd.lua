local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Animator = import "UnityEngine.Animator"
local CTickMgr = import "L10.Engine.CTickMgr"
local Animation = import "UnityEngine.Animation"

LuaMengQuanHengXingWarningWnd = class()
LuaMengQuanHengXingWarningWnd.playState = nil

function LuaMengQuanHengXingWarningWnd:Awake()
    self:InitUIComponent()
    self:InitUIData()
end

function LuaMengQuanHengXingWarningWnd:Init()
    
end

function LuaMengQuanHengXingWarningWnd:InitUIComponent()
    self.warningLabel = self.transform:Find("Offset/Count_Label/MinuteLabel"):GetComponent(typeof(UILabel))
    self.timeObj = self.transform:Find("Offset/Count_Label").gameObject
    self.dogObj = self.transform:Find("Offset/dog_Label").gameObject
    self.dogLabel = self.dogObj.transform:GetComponent(typeof(UILabel))

    self.animationComp = self.transform:GetComponent(typeof(Animation))
end

function LuaMengQuanHengXingWarningWnd:InitUIData()
    if LuaMengQuanHengXingWarningWnd.playState then
        self:OnUpdateMengQuanHengXingWarning(LuaMengQuanHengXingWarningWnd.playState)
    end
end    

function LuaMengQuanHengXingWarningWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateMengQuanHengXingWarning", self, "OnUpdateMengQuanHengXingWarning")
    g_ScriptEvent:AddListener("HideMengQuanHengXingWarning", self, "OnHideMengQuanHengXingWarning")
end

function LuaMengQuanHengXingWarningWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateMengQuanHengXingWarning", self, "OnUpdateMengQuanHengXingWarning")
    g_ScriptEvent:RemoveListener("HideMengQuanHengXingWarning", self, "OnHideMengQuanHengXingWarning")
    if self.hideWndTick then
        UnRegisterTick(self.hideWndTick)
        self.hideWndTick = nil
    end
end

function LuaMengQuanHengXingWarningWnd:OnHideMengQuanHengXingWarning()
    CUIManager.CloseUI(CLuaUIResources.MengQuan2023WarningWnd)
end

function LuaMengQuanHengXingWarningWnd:OnUpdateMengQuanHengXingWarning(playState)
    LuaMengQuanHengXingWarningWnd.playState = playState
    if playState == EnumDouble11MQLDPlayState.eFirstStage then
        self.timeObj:SetActive(false)
        self.dogObj:SetActive(true)
        self.dogLabel.text = LocalString.GetString("游戏开始")
        --self.animator:Play("mengquanhengxingwarningwnd_show", 0, 0)
    elseif playState == EnumDouble11MQHXPlayState.Warning then
        self.timeObj:SetActive(true)
        self.dogObj:SetActive(false)
        local crazyGameDuration = Double11_MQHX.GetData().PlayCrazyDuration
        self.warningLabel.text = System.String.Format(LocalString.GetString("倒计时{0}分钟"), crazyGameDuration / 60)
        --self.animator:Play("mengquanhengxingwarningwnd_show", 0, 0)
    elseif playState == EnumDouble11MQLDPlayState.eSecondStage then
        self.timeObj:SetActive(false)
        self.dogObj:SetActive(true)
        self.dogLabel.text = LocalString.GetString("疯狂单身狗降临")
        --self.animator:Play("mengquanhengxingwarningwnd_show", 0, 0)
    elseif playState == EnumDouble11MQLDPlayState.eEnd then
        self.timeObj:SetActive(false)
        self.dogObj:SetActive(true)
        self.dogLabel.text = LocalString.GetString("游戏结束")
        --self.animator:Play("mengquanhengxingwarningwnd_show", 0, 0)
    end

    if self.hideWndTick then
        UnRegisterTick(self.hideWndTick)
        self.hideWndTick = nil
    end
    self.hideWndTick = CTickMgr.Register(DelegateFactory.Action(function ()
        CUIManager.CloseUI(CLuaUIResources.MengQuan2023WarningWnd) 
    end), 3000, ETickType.Once)
end