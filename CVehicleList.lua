-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClothespressEmptyTemplate = import "L10.UI.CClothespressEmptyTemplate"
local CFashionVehicleRenewalMgr = import "L10.UI.CFashionVehicleRenewalMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CVehicleList = import "L10.UI.CVehicleList"
local CVehicleTemplate = import "L10.UI.CVehicleTemplate"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local GameSetting_Common = import "L10.Game.GameSetting_Common"
local L10 = import "L10"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local QnButton = import "L10.UI.QnButton"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local RenewalType = import "L10.UI.RenewalType"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local UIPanel = import "UIPanel"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local ZuoQi_Setting = import "L10.Game.ZuoQi_Setting"
local ZuoQi_ZuoQi = import "L10.Game.ZuoQi_ZuoQi"
local ZuoQiData = import "L10.Game.ZuoQiData"
local ZuoqiSort = import "L10.UI.ZuoqiSort"

CVehicleList.m_Init_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.qnSelectButtonList)

    this:UpdateZuoqiNumLabel()

    --this.OnFashionChange(0);   
    Extensions.RemoveAllChildren(this.table.transform)
    this.vehicleTemplate:SetActive(false)
    this.emptyVehicleTemplate:SetActive(false)
    local zuoqiList = CZuoQiMgr.Inst.zuoqis

    if CClientMainPlayer.Inst == nil or zuoqiList == nil then
        return
    end

    local defaultZuoqiTid = 0
    local l = CreateFromClass(MakeGenericClass(List, ZuoQiData))
    local topList = CreateFromClass(MakeGenericClass(List, ZuoQiData))
    CommonDefs.ListIterate(CZuoQiMgr.Inst.zuoqis, DelegateFactory.Action_object(function (___value) 
        local zuoqi = ___value
        local zqdata = ZuoQi_ZuoQi.GetData(zuoqi.templateId)
        if zqdata ~= nil then
            if zuoqi.id == CClientMainPlayer.Inst.BasicProp.DefaultZuoQiId then
                defaultZuoqiTid = zuoqi.templateId
                CommonDefs.ListInsert(topList, 0, typeof(ZuoQiData), zuoqi)
            elseif zuoqi.expTime == 0 then
                CommonDefs.ListAdd(topList, typeof(ZuoQiData), zuoqi)
            else
                CommonDefs.ListAdd(l, typeof(ZuoQiData), zuoqi)
            end
        end
    end))
    CommonDefs.ListSort1(l, typeof(ZuoQiData), CreateFromClass(ZuoqiSort))
    CommonDefs.ListAddRange(topList, l)

    local bFoundCurSelected = false
    CommonDefs.ListIterate(topList, DelegateFactory.Action_object(function (___value) 
        local zuoqi = ___value
        local go = NGUITools.AddChild(this.table.gameObject, this.vehicleTemplate)
        go:SetActive(true)

        local template = CommonDefs.GetComponent_GameObject_Type(go, typeof(CVehicleTemplate))
        
        local tid = CZuoQiMgr.GetTransformZuoQiTemplateId(zuoqi.id, zuoqi.templateId)
        template:Init(zuoqi.id,  tid, zuoqi.expTime)

        local button = CommonDefs.GetComponent_Component_Type(template, typeof(QnSelectableButton))
        CommonDefs.ListAdd(this.qnSelectButtonList, typeof(QnSelectableButton), button)
        button:SetSelected(false, false)
        button.OnClick = CommonDefs.CombineListner_Action_QnButton(button.OnClick, MakeDelegateFromCSFunction(this.OnSelectButtonClick, MakeGenericClass(Action1, QnButton), this), true)
        template.onQnButtonClick = MakeDelegateFromCSFunction(this.OnSelectButtonClick, MakeGenericClass(Action1, QnButton), this)
        if zuoqi.id == CZuoQiMgr.Inst.curSelectedZuoqiId then
            bFoundCurSelected = true
        end
    end))
    if CZuoQiMgr.openVehicleExpand then
        this:InitEmptyVehicle()
    end
    this.table:Reposition()

    -- 变身时装
    local monsterId = LuaAppearancePreviewMgr.GetCurrentTransformMonsterId()
    -- 如果找到了当前选中的，需要那一行改成选中状态
    if bFoundCurSelected and CZuoQiMgr.Inst.curSelectedZuoqiId ~= nil and CZuoQiMgr.Inst.curSelectedZuoqiTid ~= 0 then
        this:SelectVehicle(CZuoQiMgr.Inst.curSelectedZuoqiId, CZuoQiMgr.Inst.curSelectedZuoqiTid)
        this.modelPreviewer:PreviewMainPlayer(CClientMainPlayer.Inst.AppearanceProp.HideGemFxToOtherPlayer, CClientMainPlayer.Inst.AppearanceProp.IntesifySuitId, 0, 0, 0, CZuoQiMgr.Inst.curSelectedZuoqiTid, CVehicleList.sDefaultRotation, CZuoQiMgr.Inst.curSelectedZuoqiId, monsterId)
    elseif CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId ~= nil and CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId ~= "" and CZuoQiMgr.Inst.curSelectedZuoqiId == nil and CZuoQiMgr.Inst.curSelectedZuoqiTid == 0 then
        this:SelectVehicle(CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId, CClientMainPlayer.Inst.AppearanceProp.ZuoQiTemplateId)
        this.modelPreviewer:PreviewMainPlayer(CClientMainPlayer.Inst.AppearanceProp.HideGemFxToOtherPlayer, CClientMainPlayer.Inst.AppearanceProp.IntesifySuitId, 0, 0, 0, CClientMainPlayer.Inst.AppearanceProp.ZuoQiTemplateId, CVehicleList.sDefaultRotation, CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId, monsterId)
    elseif System.String.IsNullOrEmpty(CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId) and CZuoQiMgr.Inst.curSelectedZuoqiId == nil and not System.String.IsNullOrEmpty(CClientMainPlayer.Inst.BasicProp.DefaultZuoQiId) and defaultZuoqiTid ~= 0 then
        this:SelectVehicle(CClientMainPlayer.Inst.BasicProp.DefaultZuoQiId, defaultZuoqiTid)
        this.modelPreviewer:PreviewMainPlayer(CClientMainPlayer.Inst.AppearanceProp.HideGemFxToOtherPlayer, CClientMainPlayer.Inst.AppearanceProp.IntesifySuitId, 0, 0, 0, defaultZuoqiTid, CVehicleList.sDefaultRotation, CClientMainPlayer.Inst.BasicProp.DefaultZuoQiId, monsterId)
    elseif (CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId == nil or CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId == "") and CZuoQiMgr.Inst.curSelectedZuoqiId == nil and CZuoQiMgr.Inst.curSelectedZuoqiTid == 0 and l.Count > 0 then
        local zuoqidata = l[0]
        this:SelectVehicle(zuoqidata.id, zuoqidata.templateId)

        this.modelPreviewer:PreviewMainPlayer(CClientMainPlayer.Inst.AppearanceProp.HideGemFxToOtherPlayer, CClientMainPlayer.Inst.AppearanceProp.IntesifySuitId, 0, 0, 0, zuoqidata.templateId, CVehicleList.sDefaultRotation, zuoqidata.id, monsterId)
    elseif not bFoundCurSelected and CZuoQiMgr.Inst.curSelectedZuoqiId ~= nil then
        this.modelPreviewer:PreviewMainPlayer(CClientMainPlayer.Inst.AppearanceProp.HideGemFxToOtherPlayer, CClientMainPlayer.Inst.AppearanceProp.IntesifySuitId, 0, 0, 0, CClientMainPlayer.Inst.AppearanceProp.ZuoQiTemplateId, CVehicleList.sDefaultRotation, CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId, monsterId)

        CZuoQiMgr.Inst.curSelectedZuoqiId = nil
        CZuoQiMgr.Inst.curSelectedZuoqiTid = 0
    else
        this.scrollView:ResetPosition()
    end
end
CVehicleList.m_UpdateZuoqiNumLabel_CS2LuaHook = function (this) 
    this.zuoqiCount = 0
    this.mapiCount = 0
    do
        local i = 0
        while i < CZuoQiMgr.Inst.zuoqis.Count do
            local zqdata = CZuoQiMgr.Inst.zuoqis[i]
            if zqdata ~= nil then
                if CZuoQiMgr.IsMapiTemplateId(zqdata.templateId) then
                    this.mapiCount = this.mapiCount + 1
                else
                    this.zuoqiCount = this.zuoqiCount + 1
                end
            end
            i = i + 1
        end
    end
    if CZuoQiMgr.IsMapiEnable() then
        this.numLimitLabel.text = CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("我的坐骑（{0}/{1}） 我的马匹（{2}/{3}）"), {this.zuoqiCount, CClientMainPlayer.Inst.FightProp.ZuoQiNumber, this.mapiCount, ZuoQi_Setting.GetData().MapiMaxCount})
    else
        this.numLimitLabel.text = System.String.Format(LocalString.GetString("我的坐骑（{0}/{1}）"), this.zuoqiCount, CClientMainPlayer.Inst.FightProp.ZuoQiNumber)
    end
end
CVehicleList.m_InitEmptyVehicle_CS2LuaHook = function (this) 
    local emptyAvailable = CClientMainPlayer.Inst.FightProp.ZuoQiNumber - this.zuoqiCount

    do
        local i = 0
        while i < emptyAvailable do
            local instance = NGUITools.AddChild(this.table.gameObject, this.emptyVehicleTemplate)
            instance:SetActive(true)
            local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CClothespressEmptyTemplate))
            if template ~= nil then
                template:Init(false, true, false)
            end
            i = i + 1
        end
    end
    local maxSize = GameSetting_Common.GetData().MaxZuoQiNumber
    if CClientMainPlayer.Inst.FightProp.ZuoQiNumber < maxSize then
        local instance = NGUITools.AddChild(this.table.gameObject, this.emptyVehicleTemplate)
        instance:SetActive(true)
        local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CClothespressEmptyTemplate))
        if template ~= nil then
            template:Init(true, true, false)
        end
    end
end
CVehicleList.m_OnEnable_CS2LuaHook = function (this)
    LuaVehicleList.Target = this
    EventManager.AddListenerInternal(EnumEventType.OnRenewVehicle, LuaVehicleList.OnRenewVehicle)
    EventManager.AddListener(EnumEventType.OnAddDeleteVehicle, MakeDelegateFromCSFunction(this.Init, Action0, this))
    EventManager.AddListener(EnumEventType.OnRideOnOffVehicle, MakeDelegateFromCSFunction(this.OnVehicleChange, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.OnFreightSubmitEquipSelected, MakeDelegateFromCSFunction(this.OnAddVehicleByItem, MakeGenericClass(Action2, String, String), this))
    EventManager.AddListener(EnumEventType.OnFashionOrVehicleLimitChanged, MakeDelegateFromCSFunction(this.OnVehicleLimitChanged, Action0, this))
    UIEventListener.Get(this.onekeyRenewalBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.onekeyRenewalBtn).onClick, MakeDelegateFromCSFunction(this.OnRenewalBtnClick, VoidDelegate, this), true)
end
CVehicleList.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.OnRenewVehicle, LuaVehicleList.OnRenewVehicle)
    LuaVehicleList.Target = nil
    EventManager.RemoveListener(EnumEventType.OnAddDeleteVehicle, MakeDelegateFromCSFunction(this.Init, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnRideOnOffVehicle, MakeDelegateFromCSFunction(this.OnVehicleChange, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.OnFreightSubmitEquipSelected, MakeDelegateFromCSFunction(this.OnAddVehicleByItem, MakeGenericClass(Action2, String, String), this))
    EventManager.RemoveListener(EnumEventType.OnFashionOrVehicleLimitChanged, MakeDelegateFromCSFunction(this.OnVehicleLimitChanged, Action0, this))
    UIEventListener.Get(this.onekeyRenewalBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.onekeyRenewalBtn).onClick, MakeDelegateFromCSFunction(this.OnRenewalBtnClick, VoidDelegate, this), false)
end
CVehicleList.m_OnSelectButtonClick_CS2LuaHook = function (this, button) 
    do
        local i = 0
        while i < this.qnSelectButtonList.Count do
            this.qnSelectButtonList[i]:SetSelected(button == this.qnSelectButtonList[i], false)
            i = i + 1
        end
    end

    local go = button.gameObject
    local vehicle = CommonDefs.GetComponent_GameObject_Type(go, typeof(CVehicleTemplate))
    if vehicle ~= nil then
        CZuoQiMgr.Inst.curSelectedZuoqiTid = vehicle.vehicleTID
        CZuoQiMgr.Inst.curSelectedZuoqiId = vehicle.vehicleID

        -- 变身时装
        local monsterId = LuaAppearancePreviewMgr.GetCurrentTransformMonsterId()
        this.modelPreviewer:PreviewMainPlayer(CClientMainPlayer.Inst.AppearanceProp.HideGemFxToOtherPlayer, CClientMainPlayer.Inst.AppearanceProp.IntesifySuitId, 0, 0, 0, vehicle.vehicleTID, CVehicleList.sDefaultRotation, vehicle.vehicleID, monsterId)
        EventManager.BroadcastInternalForLua(EnumEventType.OnVehicleSelected, {vehicle.vehicleID == CClientMainPlayer.Inst.BasicProp.DefaultZuoQiId})
    end
end
CVehicleList.m_SelectVehicle_CS2LuaHook = function (this, vehicleId, vehicleTid) 

    local index = 0
    do
        local i = 0
        while i < this.qnSelectButtonList.Count do
            if vehicleId == CommonDefs.GetComponent_Component_Type(this.qnSelectButtonList[i], typeof(CVehicleTemplate)).vehicleID then
                this.qnSelectButtonList[i]:SetSelected(true, false)
                index = i
                break
            else
                this.qnSelectButtonList[i]:SetSelected(false, false)
            end
            i = i + 1
        end
    end
    local offset = math.min(index * (this.vehicleSprite.height + this.table.padding.y) / (this.table.transform.childCount * (this.vehicleSprite.height + this.table.padding.y) - CommonDefs.GetComponent_Component_Type(this.scrollView, typeof(UIPanel)).height), 1)
    --scrollView.SetDragAmount(0, offset, false);
    this:StartCoroutine(this:SetDragAmount(offset))

    CZuoQiMgr.Inst.curSelectedZuoqiTid = vehicleTid
    CZuoQiMgr.Inst.curSelectedZuoqiId = vehicleId
    EventManager.BroadcastInternalForLua(EnumEventType.OnVehicleSelected, {vehicleId == CClientMainPlayer.Inst.BasicProp.DefaultZuoQiId})
end
CVehicleList.m_OnRenewalBtnClick_CS2LuaHook = function (this, go) 

    CFashionVehicleRenewalMgr.type = RenewalType.VEHICLE
    CommonDefs.DictClear(CFashionVehicleRenewalMgr.vehicleRenewalList)
    local curtime = CServerTimeMgr.Inst.timeStamp
    CommonDefs.ListIterate(CZuoQiMgr.Inst.zuoqis, DelegateFactory.Action_object(function (___value) 
        local zuoqi = ___value
        local data = ZuoQi_ZuoQi.GetData(zuoqi.templateId)
        if data ~= nil and data.CanRenewal == 1 and zuoqi.expTime < curtime and not data.RenewalAvailableTime then
            CommonDefs.DictAdd(CFashionVehicleRenewalMgr.vehicleRenewalList, typeof(String), zuoqi.id, typeof(UInt32), zuoqi.templateId)
        end
    end))

    if CFashionVehicleRenewalMgr.vehicleRenewalList.Count > 0 then
        CUIManager.ShowUI(CUIResources.FashionRenewalWnd)
    else
        g_MessageMgr:ShowMessage("NO_VEHICLE_NEED_RENEW")
    end
end
CVehicleList.m_GetMountButton_CS2LuaHook = function (this) 
    local count = this.table.transform.childCount
    do
        local i = 0
        while i < count do
            local go = this.table.transform:GetChild(i).gameObject
            local template = CommonDefs.GetComponent_GameObject_Type(go, typeof(CVehicleTemplate))
            if template ~= nil then
                local rtn = template:GetRideOnButton()
                if rtn ~= nil then
                    return rtn
                end
            end
            i = i + 1
        end
    end
    --找不到 结束引导
    L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
    return nil
end

--------------------------------------------------
LuaVehicleList = {}
LuaVehicleList.Target = nil

LuaVehicleList.OnRenewVehicleFunc = function (vid, tid, expTime)
    if LuaVehicleList.Target then 
        LuaVehicleList.Target:Init()
    end
end

LuaVehicleList.OnRenewVehicle = DelegateFactory.Action_string_uint_double(
    LuaVehicleList.OnRenewVehicleFunc)
