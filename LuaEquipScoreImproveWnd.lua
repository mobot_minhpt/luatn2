require("common/common_include")
require("ui/equip/LuaEquipScoreImproveItem")

local CBaseWnd = import "L10.UI.CBaseWnd"
local CPlayerCapacityMgr = import "L10.Game.CPlayerCapacityMgr"
local EnumEquipScoreImproveType = import "L10.Game.CPlayerCapacityMgr+EnumEquipScoreImproveType"
local CItemMgr = import "L10.Game.CItemMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local LocalString = import "LocalString"
local UILabel = import "UILabel"
local UIScrollView = import "UIScrollView"
local UITable = import "UITable"
local Extensions = import "Extensions"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonSelector = import "L10.UI.CCommonSelector"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local GemGroup_GemGroup = import "L10.Game.GemGroup_GemGroup"
local EnumBodyPosition = import "L10.Game.EnumBodyPosition"

LuaEquipScoreImproveWnd = class()
RegistClassMember(LuaEquipScoreImproveWnd,"closeBtn")
RegistClassMember(LuaEquipScoreImproveWnd,"titleLabel")
RegistClassMember(LuaEquipScoreImproveWnd,"currentTotalScoreLabel")
RegistClassMember(LuaEquipScoreImproveWnd,"targetTotalScoreSelector")
RegistClassMember(LuaEquipScoreImproveWnd,"table")
RegistClassMember(LuaEquipScoreImproveWnd,"scrollView")
RegistClassMember(LuaEquipScoreImproveWnd,"itemTemplate")

RegistClassMember(LuaEquipScoreImproveWnd,"items")
RegistClassMember(LuaEquipScoreImproveWnd,"equipDataList")

function LuaEquipScoreImproveWnd:Awake()
    
end

function LuaEquipScoreImproveWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaEquipScoreImproveWnd:Init()
	self.closeBtn = self.transform:Find("Wnd_Bg_Secondary_1/CloseButton").gameObject
	self.titleLabel = self.transform:Find("Wnd_Bg_Secondary_1/TitleLabel"):GetComponent(typeof(UILabel))
	self.currentTotalScoreLabel = self.transform:Find("Anchor/Header/CurrentScore/CurrentScoreLabel"):GetComponent(typeof(UILabel))
	self.targetTotalScoreSelector = self.transform:Find("Anchor/Header/TargetScore/TargetScoreSelector"):GetComponent(typeof(CCommonSelector))
	self.table = self.transform:Find("Anchor/ScrollView/Table"):GetComponent(typeof(UITable))
	self.scrollView = self.transform:Find("Anchor/ScrollView"):GetComponent(typeof(UIScrollView))
	self.itemTemplate = self.transform:Find("Anchor/ScrollView/Template").gameObject
	
	self.itemTemplate:SetActive(false)

	CommonDefs.AddOnClickListener(self.closeBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

	local curType = CPlayerCapacityMgr.Inst.CurImproveType
	if curType == EnumEquipScoreImproveType.BasicScore then
		self.titleLabel.text = LocalString.GetString("基础评分加强")
	elseif curType == EnumEquipScoreImproveType.IntensifyScore then
		self.titleLabel.text = LocalString.GetString("强化评分加强")
	elseif curType == EnumEquipScoreImproveType.HoleScore then
		self.titleLabel.text = LocalString.GetString("宝石评分加强")
	end

	self.targetTotalScoreSelector:Init(CPlayerCapacityMgr.Inst.targetScoreSelections, DelegateFactory.Action_int(function ( index )
		CPlayerCapacityMgr.Inst:UpdateDefaultTargetImproveScore(index)
		self.targetTotalScoreSelector:SetName(CPlayerCapacityMgr.Inst:GetCurrentTargetSelectionName())
		CJingLingMgr.Inst:QueryJingLingForEquipScoreImprovement(CPlayerCapacityMgr.Inst.DefaultTargetImproveScore)
	end))
	self.targetTotalScoreSelector:SetName(CPlayerCapacityMgr.Inst:GetCurrentTargetSelectionName())

	self.items = {}
	self.equipDataList = {}

	Extensions.RemoveAllChildren(self.table.transform)
	local mainplayer = CClientMainPlayer.Inst
	if mainplayer == nil then
		return
	end

	self.currentTotalScoreLabel.text = tostring(mainplayer.EquipScore)

	for i=1,12 do
		if curType ~= EnumEquipScoreImproveType.IntensifyScore 
			or (i~=EnumToInt(EnumBodyPosition.Bracelet) and i~=EnumToInt(EnumBodyPosition.Bracelet)+3 and i~=EnumToInt(EnumBodyPosition.Necklace)) then
			--手镯和项链不参与强化
			local id = mainplayer.ItemProp:GetItemAt(EnumItemPlace.Body, i)
			if id and id~= "" then
				local go = CUICommonDef.AddChild(self.table.gameObject, self.itemTemplate)
				go:SetActive(true)
				local item = LuaEquipScoreImproveItem:new()
				item:Init(go)
				table.insert(self.equipDataList, {id = id, bodyPos = i, isMatch = false})
				table.insert(self.items, item)
			end
		end
	end

	self.table:Reposition()
	self.scrollView:ResetPosition()

	self:RequestInfo()
end

function LuaEquipScoreImproveWnd:RequestInfo()
	CJingLingMgr.Inst:QueryJingLingForEquipScoreImprovement(CPlayerCapacityMgr.Inst.DefaultTargetImproveScore)
end

function LuaEquipScoreImproveWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:AddListener("OnEquipScoreImprovementInfoReceived", self, "OnEquipScoreImprovementInfoReceived")
end

function LuaEquipScoreImproveWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:RemoveListener("OnEquipScoreImprovementInfoReceived", self, "OnEquipScoreImprovementInfoReceived")
end

function LuaEquipScoreImproveWnd:OnEquipScoreImprovementInfoReceived()
	self:RefreshData()
end


function LuaEquipScoreImproveWnd:OnSendItem(args)
	local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Body, args[0])
	if pos > 0 then
		for _,equipData in ipairs(self.equipDataList) do
			if equipData.bodyPos==pos then
				self:RefreshData()
				break
			end
		end
	end
end

function LuaEquipScoreImproveWnd:RefreshData()
	if not self.items then return end
	local mainplayer = CClientMainPlayer.Inst
	if not mainplayer then return end

	for _,equipData in ipairs(self.equipDataList) do
		self:InitEquipData(equipData)
	end

	--排序
	table.sort(self.equipDataList, function(a, b)
		if a.isMatch and b.isMatch then
			if a.targetDescDiff == b.targetDescDiff then
				return a.bodyPos < b.bodyPos --lua下要求稳定排序,按装备位置从小到大排序
			elseif a.targetDescDiff < b.targetDescDiff then
				return false
			else
				return true
			end
		elseif (not a.isMatch) and (not b.isMatch) then
			if a.targetDiff == b.targetDiff then
				return a.bodyPos < b.bodyPos --lua下要求稳定排序,按装备位置从小到大排序
			elseif a.targetDiff < b.targetDiff then
				return false
			else
				return true
			end
		elseif a.isMatch then
			return false
		else
			return true
		end
	end)

	--刷新显示
	if #self.items ~= #self.equipDataList then return end
	for i,equipData in ipairs(self.equipDataList) do
		self.items[i]:Show(equipData.id, equipData.bodyPos, equipData.icon,
			equipData.bindSpriteName, equipData.qualitySpritename, equipData.name, 
			equipData.equipDesc, equipData.targetVal, equipData.targetDesc, equipData.isMatch, equipData.targetDescDiff<=0)
	end
end

function LuaEquipScoreImproveWnd:InitEquipData(equipData)
	if not equipData then return end
	local mainplayer = CClientMainPlayer.Inst
	if not mainplayer then return end
	local info = CPlayerCapacityMgr.Inst:GetEquipScoreImproveInfo(equipData.bodyPos)
	local id  = mainplayer.ItemProp:GetItemAt(EnumItemPlace.Body, equipData.bodyPos)
	local commonitem = CItemMgr.Inst:GetById(id)
	local equip = commonitem and commonitem.Equip

	equipData.icon = nil
	equipData.bindSpriteName = nil
	equipData.qualitySpritename = nil
	equipData.name = nil
	equipData.equipDesc = nil
	equipData.targetVal = nil
	equipData.targetDesc = nil
	equipData.isMatch = false
	equipData.targetDiff = 0 -- 主要用于排序
	equipData.targetDescDiff = 0 --主要用于排序

	local curType = CPlayerCapacityMgr.Inst.CurImproveType
	if equip then
		equipData.icon = EquipmentTemplate_Equip.GetData(equip.TemplateId).Icon
		equipData.bindSpriteName = commonitem.BindOrEquipCornerMark
		equipData.qualitySpritename = CUICommonDef.GetItemCellBorder(equip.QualityType)
		equipData.name = equip.ColoredDisplayName
		if curType == EnumEquipScoreImproveType.BasicScore then
			equipData.equipDesc = SafeStringFormat3(LocalString.GetString("基础评分: %d"), equip.BasicScore or 0)
			equipData.targetVal = LocalString.GetString("目标评分: ")
			if info then
				equipData.targetVal = equipData.targetVal..tostring(info.basicScore)
				equipData.isMatch = (equip.BasicScore >= info.basicScore)
				equipData.targetDiff = info.basicScore - equip.BasicScore
				equipData.targetDescDiff = 0
			end
		elseif curType == EnumEquipScoreImproveType.IntensifyScore then
			equipData.equipDesc = SafeStringFormat3(LocalString.GetString("基础属性提升: %d%%"), equip:GetIntensifyTotalValue() or 0)
			equipData.targetVal = LocalString.GetString("目标强化等级: ")
			if info and info.intensifyScore > 0 then
				equipData.targetVal = equipData.targetVal..tostring(info.intensifyLv)
				equipData.targetDesc = SafeStringFormat3(LocalString.GetString("基础属性提升: %d%%"), info.intensifyVal)
				equipData.isMatch = (equip.QiangHuaScore >= info.intensifyScore)
				equipData.targetDiff = info.intensifyScore - equip.QiangHuaScore
				equipData.targetDescDiff = info.intensifyVal - equip:GetIntensifyTotalValue()
			else
				equipData.targetVal = equipData.targetVal..LocalString.GetString("暂无")
				equipData.targetDesc = nil
				equipData.isMatch = true
				equipData.targetDiff = 0
				equipData.targetDescDiff = 0
			end
		elseif curType == EnumEquipScoreImproveType.HoleScore then
			local gemGroup =  GemGroup_GemGroup.GetData(equip.GemGroupId)
			if gemGroup then
				equipData.equipDesc = SafeStringFormat3("%s(%d): %d", gemGroup.Name, gemGroup.Level, equip.BaoShiScore)
			else
				equipData.equipDesc = SafeStringFormat3(LocalString.GetString("宝石评分: %d"), equip.BaoShiScore)
			end
			equipData.targetVal = LocalString.GetString("目标评分: ")
			if info and info.baoshiScore > 0 then
				equipData.targetVal = equipData.targetVal..tostring(info.baoshiScore)
				if info.szl_name ~= nil and info.szl_name ~= "" then
					equipData.targetDesc = SafeStringFormat3(LocalString.GetString("%.1f%%玩家镶嵌%s(%s级)"), info.szl_percent, info.szl_name, info.szl_lv)
				else
					equipData.targetDesc = LocalString.GetString("石之灵暂无")
				end
				equipData.isMatch = (equip.BaoShiScore >= info.baoshiScore)
				equipData.targetDiff = info.baoshiScore - equip.BaoShiScore
				equipData.targetDescDiff = 0
			else
				equipData.targetVal = equipData.targetVal..LocalString.GetString("暂无")
				equipData.targetDesc = nil
				equipData.isMatch = true
				equipData.targetDiff = 0
				equipData.targetDescDiff = 0
			end
		end
	end
end

return LuaEquipScoreImproveWnd
