local CScene = import "L10.Game.CScene"

LuaCuJuMgr = {}

LuaCuJuMgr.Info = {}
LuaCuJuMgr.ResultInfo = {}

-- 打开位置选择界面
function LuaCuJuMgr:OpenCuJuSelectWnd(data)
    local rawData = g_MessagePack.unpack(data)
    self.Info.posInfo = rawData[1]
    self.Info.selectEndTimeStamp = rawData[2]
    CUIManager.ShowUI(CLuaUIResources.CuJuSelectWnd)
end

-- 更新位置选择
function LuaCuJuMgr:UpdateCuJuPlayerPlace(place2PlayerInfo_U)
    self.Info.posInfo = g_MessagePack.unpack(place2PlayerInfo_U)
    g_ScriptEvent:BroadcastInLua("UpdateCuJuPlayerPlace")
end

-- 位置选择成功
function LuaCuJuMgr:SetCuJuPlayerPlaceSuccess(position)
    self.Info.myPos = position
    g_ScriptEvent:BroadcastInLua("SetCuJuPlayerPlaceSuccess")
end

-- 当前在蹴鞠场景中
function LuaCuJuMgr:IsInCuJu()
    if CScene.MainScene and CScene.MainScene.SceneTemplateId == CuJu_Setting.GetData().CuJuMapId then
        return true
    end
    return false
end

-- 每周排名信息
function LuaCuJuMgr:QueryCuJuWeekRankResult(playerRank, playerInfo_U, totalRank_U)
    g_ScriptEvent:BroadcastInLua("CuJuRankUpdate", false, playerRank, g_MessagePack.unpack(playerInfo_U), g_MessagePack.unpack(totalRank_U))
end

-- 历史排名信息
function LuaCuJuMgr:QueryCuJuTotalRankResult(playerRank, playerInfo_U, totalRank_U)
    g_ScriptEvent:BroadcastInLua("CuJuRankUpdate", true, playerRank, g_MessagePack.unpack(playerInfo_U), g_MessagePack.unpack(totalRank_U))
end

-- 打开结果界面
function LuaCuJuMgr:OpenCuJuPlayerPlayInfoWnd(scoreInfo_U, playInfo_U, winForce)
    if not CClientMainPlayer.Inst then return end

    self.ResultInfo.scoreInfo = g_MessagePack.unpack(scoreInfo_U)
    local playInfo = g_MessagePack.unpack(playInfo_U)

    self.ResultInfo.playerInfo = {}
    self.ResultInfo.playerInfo[1] = {}
    self.ResultInfo.playerInfo[2] = {}

    self.ResultInfo.winForce = winForce
    for i = 1, #playInfo do
        local data = playInfo[i]
        local name, goal, steal, force, playerId, class = data[1], data[2], data[3], data[4], data[5], data[6]

        if force == EnumCommonForce_lua.eDefend then
            table.insert(self.ResultInfo.playerInfo[1], {name = name, goal = goal, steal = steal, playerId = playerId, class = class})
        elseif force == EnumCommonForce_lua.eAttack then
            table.insert(self.ResultInfo.playerInfo[2], {name = name, goal = goal, steal = steal, playerId = playerId, class = class})
        end

        if playerId == CClientMainPlayer.Inst.Id then
            self.ResultInfo.myForce = force
        end
    end
    CUIManager.ShowUI(CLuaUIResources.CuJuResultWnd)
end

-- 同步球的位置和得分
function LuaCuJuMgr:UpdateCuJuBallPosAndScore(ballPos_U, forceScore_U)
    local ballPos = g_MessagePack.unpack(ballPos_U)
    if ballPos and #ballPos >= 2 then
        g_ScriptEvent:BroadcastInLua("UpdateCuJuBallPos", ballPos)
    end

    local forceScore = g_MessagePack.unpack(forceScore_U)
    if forceScore and #forceScore >= 2 then
        g_ScriptEvent:BroadcastInLua("UpdateCuJuTotalScore", forceScore)
    end
end
