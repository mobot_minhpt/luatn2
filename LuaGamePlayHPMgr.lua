local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local MainPlayerFrame = import "L10.UI.MainPlayerFrame"
local CCurrentTargetWnd = import "L10.UI.CCurrentTargetWnd"
local CDuoMaoMaoMgr = import "L10.Game.CDuoMaoMaoMgr"
local GameplayHPInfo = import "L10.Game.GameplayHPInfo"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local EDamageText = import "L10.Game.EDamageText"

LuaGamePlayHpMgr = class()

---------------------------------------------------------------------------------------------------------------------
-- 玩法血量显示，分为离散的与连续的两种，独立于玩家自身血量
-- 影响主角、目标、头顶、队伍的血量显示
---------------------------------------------------------------------------------------------------------------------

function LuaGamePlayHpMgr:AddHpInfo(playerId, hp, fullHp, hasAddHp, type, showDamage)
    if CGamePlayMgr.Inst and CommonDefs.DictContains(CGamePlayMgr.Inst.GamePlayHPs, typeof(UInt64), playerId) then
		local curData = CommonDefs.DictGetValue(CGamePlayMgr.Inst.GamePlayHPs, typeof(UInt64), playerId)
        local curHp = curData.hp

        if showDamage then
            -- 加减血量飘字逻辑
            local player = CClientPlayerMgr.Inst:GetPlayer(playerId)
            if player and hp ~= curHp then
                player:ShowDamageText(hp-curHp, EDamageText.Hp, false)
            end
        end
	end

    local data = CreateFromClass(GameplayHPInfo)
    data.playerId = playerId
    data.hp = hp
    data.fullHp = fullHp
    data.hasAddHp = hasAddHp
    data.type = type

    if CGamePlayMgr.Inst then
        CommonDefs.DictSet(CGamePlayMgr.Inst.GamePlayHPs, typeof(UInt64), playerId, typeof(GameplayHPInfo), data)
    end
    EventManager.BroadcastInternalForLua(EnumEventType.OnGameplayHPUpdate, {playerId, hp, fullHp, hasAddHp, type})
end

-- 判断主角色是否显示玩法血量
-- 分为离散血量GamePlayHPBar 和 连续血量continuousGameplayHPBar
MainPlayerFrame.m_hookInitGamePlayHp = function(this)
    if CDuoMaoMaoMgr.Inst.InHideAndSeek then
        -- 特殊处理的躲猫猫
        local isHider = CDuoMaoMaoMgr.Inst:isHider(CClientMainPlayer.Inst)
        this.gameplayHPBar:SetActive(isHider)
        this.hpBar.gameObject:SetActive(not isHider)
        this.continuousGameplayHPBar.gameObject:SetActive(false)
    elseif LuaYuanXiao2021Mgr.IsInTangYuanPlay() or LuaBaoZhuPlayMgr.IsInPlay() then
        -- 离散血条玩法
        this.gameplayHPBar:SetActive(true)
        this.hpBar.gameObject:SetActive(false)
        this.continuousGameplayHPBar.gameObject:SetActive(false)
    elseif LuaLuoCha2021Mgr:IsInGamePlay() then
        -- 连续血量玩法
        this.gameplayHPBar:SetActive(false)
        this.hpBar.gameObject:SetActive(false)
        this.continuousGameplayHPBar.gameObject:SetActive(true)
    else
        this.gameplayHPBar:SetActive(false)
        this.continuousGameplayHPBar.gameObject:SetActive(false)
        this.hpBar.gameObject:SetActive(true)
    end
end

-- 目标信息更新玩法血量
CCurrentTargetWnd.m_hookUpdateGameplayHp = function(this, playerId, hp, fullHp, hasAddHp, type)
    local obj = CClientObjectMgr.Inst:GetObject(this.targetEngineId)
    if LuaYuanXiao2021Mgr.IsInTangYuanPlay() or LuaBaoZhuPlayMgr.IsInPlay() or LuaLuoCha2021Mgr:IsInGamePlay() or LuaHalloween2023Mgr:IsCandyDeliveryGaming() then
        this:UpdateGamePlayHpInfo(obj)
    else
        this:UpdateDuoMaoMaoInfo(obj)
    end
end


-- 玩家头顶是否显示玩法血条在C#代码中
-- CHeadInfoWnd.NeedShowGameplayHP()
