local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgr  = import "L10.UI.CItemInfoMgr"

LuaOffWorldPassReward1Wnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaOffWorldPassReward1Wnd, "CloseBtn", "CloseBtn", GameObject)
RegistChildComponent(LuaOffWorldPassReward1Wnd, "ScrollView", "ScrollView", GameObject)
RegistChildComponent(LuaOffWorldPassReward1Wnd, "Table", "Table", GameObject)
RegistChildComponent(LuaOffWorldPassReward1Wnd, "ItemCell", "ItemCell", GameObject)
RegistChildComponent(LuaOffWorldPassReward1Wnd, "ConfirmBtn", "ConfirmBtn", GameObject)

--@endregion RegistChildComponent end

function LuaOffWorldPassReward1Wnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaOffWorldPassReward1Wnd:Init()
  local onCloseClick = function(go)
    CUIManager.CloseUI(CLuaUIResources.OffWorldPassReward1Wnd)
  end

  CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
  CommonDefs.AddOnClickListener(self.ConfirmBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

  self.ItemCell:SetActive(false)

  self:InitBonus()
end

--@region UIEvent

--@endregion UIEvent

function LuaOffWorldPassReward1Wnd:InitBonus()
	for i,v in ipairs(LuaOffWorldPassMgr.BonusItemTable) do
    local awardId = v[1]
    local count = v[2]

    local award = NGUITools.AddChild(self.Table,self.ItemCell)
    award:SetActive(true)
    local iconName = Item_Item.GetData(tonumber(awardId)).Icon
    local texture = award.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    texture:LoadMaterial(iconName)
    UIEventListener.Get(texture.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
      CItemInfoMgr.ShowLinkItemTemplateInfo(awardId)
    end)
    if count > 0 then
      award.transform:Find("AmountLabel"):GetComponent(typeof(UILabel)).text = count
    else
      award.transform:Find("AmountLabel"):GetComponent(typeof(UILabel)).text = ''
    end
  end

	self.Table:GetComponent(typeof(UITable)):Reposition()
	--self.ScrollView:GetComponent(typeof(UIScrollView)):ResetPosition()
end
