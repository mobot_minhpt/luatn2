local UISearchBar = import "L10.UI.UISearchBar"
local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local UIInput = import "UIInput"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnTableView = import "L10.UI.QnTableView"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CIMMgr = import "L10.Game.CIMMgr"
local CMiddleNoticeMgr = import "L10.UI.CMiddleNoticeMgr"
local Constants = import "L10.Game.Constants"
local Profession = import "L10.Game.Profession"
local CCommonPlayerDisplayData = import "L10.Game.CCommonPlayerDisplayData"
local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"

LuaCommonSearchPlayerWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCommonSearchPlayerWnd, "VoidLabel", "VoidLabel", GameObject)
RegistChildComponent(LuaCommonSearchPlayerWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaCommonSearchPlayerWnd, "SearchBar", "SearchBar", UISearchBar)
RegistChildComponent(LuaCommonSearchPlayerWnd, "ClearBtn", "ClearBtn", CButton)
RegistChildComponent(LuaCommonSearchPlayerWnd, "SearchBtn", "SearchBtn", CButton)
RegistChildComponent(LuaCommonSearchPlayerWnd, "Input", "Input", UIInput)

--@endregion RegistChildComponent end
RegistClassMember(LuaCommonSearchPlayerWnd,"m_SelectSuccessAction")
RegistClassMember(LuaCommonSearchPlayerWnd,"m_SelectFailAction")
RegistClassMember(LuaCommonSearchPlayerWnd,"m_PlayerData")
RegistClassMember(LuaCommonSearchPlayerWnd,"m_TableViewDataSource")

function LuaCommonSearchPlayerWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ClearBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnClearBtnClick()
	end)


	
	UIEventListener.Get(self.SearchBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSearchBtnClick()
	end)


    --@endregion EventBind end
end

function LuaCommonSearchPlayerWnd:Init()
	local function initItem(item,index)
        self:InitItem(item,index)
    end

	self.VoidLabel:SetActive(false)
	self.ClearBtn.gameObject:SetActive(false)

	self.m_TableViewDataSource = DefaultTableViewDataSource.CreateByCount(0,initItem)
    self.TableView.m_DataSource = self.m_TableViewDataSource

	self.SearchBar.OnChange = DelegateFactory.Action_string(function(str)
		self.ClearBtn.gameObject:SetActive(str~= "")
	end)

	self:InitFriends()
end

function LuaCommonSearchPlayerWnd:InitFriends()
	local player = CClientMainPlayer.Inst
	self.m_PlayerData = {}
	if player then
		local friendlinessTbl = {}
		CommonDefs.DictIterate(player.RelationshipProp.Friends, DelegateFactory.Action_object_object(function(k, v)
			if CIMMgr.Inst:IsOnline(k) and CIMMgr.Inst:IsSameServerFriend(k) then
				table.insert(friendlinessTbl, {k, v.Friendliness})
			end
		end))
		table.sort(friendlinessTbl, function(v1, v2) return v1[2] > v2[2] end)

		for _, info in pairs(friendlinessTbl) do
			local basicInfo = CIMMgr.Inst:GetBasicInfo(info[1])
			if basicInfo then
				--local data = CreateFromClass(CCommonPlayerDisplayData, basicInfo.ID, basicInfo.Name, basicInfo.Level, basicInfo.Class, basicInfo.Gender, basicInfo.Expression, true)
				local data = {XianFanStatus = basicInfo.XianFanStatus,Class = basicInfo.Class, Name = basicInfo.Name, Level = basicInfo.Level, Gender = basicInfo.Gender, ID = basicInfo.ID}
				table.insert(self.m_PlayerData, data)
			end
		end
	end
	self.m_TableViewDataSource.count = #self.m_PlayerData
    self.TableView:ReloadData(true,false)
    self.VoidLabel.gameObject:SetActive(#self.m_PlayerData ==0)
end

function LuaCommonSearchPlayerWnd:OnReturnSearchSuccess(infos)
	self.m_PlayerData = {}
	for i=0, infos.Count-1 do
		local data = infos[i]
		if data.ID ~= CClientMainPlayer.Inst.Id then
			table.insert(self.m_PlayerData, data)
		end
	end
	self.m_TableViewDataSource.count = #self.m_PlayerData
    self.TableView:ReloadData(true,false)
    self.VoidLabel.gameObject:SetActive(#self.m_PlayerData ==0)
end

function LuaCommonSearchPlayerWnd:InitItem(item,index)
    local tf = item.transform
    local nameLabel = FindChild(tf,"PlayerNameLabel"):GetComponent(typeof(UILabel))
    local clsIcon = FindChild(tf,"ClsIcon"):GetComponent(typeof(UISprite))
    local clsTexture = FindChild(tf,"ClsTexture"):GetComponent(typeof(CUITexture))
    local levelLabel = FindChild(tf,"LevelLabel"):GetComponent(typeof(UILabel))
    local selectBtn = FindChild(tf,"SelectBtn").gameObject
    
    local data = self.m_PlayerData[index+1]

    clsIcon.spriteName = Profession.GetIconByNumber(data.Class)
    nameLabel.text = data.Name
    levelLabel.text = SafeStringFormat3("lv.%d", data.Level)
	levelLabel.color = data.XianFanStatus > 0 and NGUIText.ParseColor24("fe7900", 0) or Color.white
	clsTexture:LoadNPCPortrait(CUICommonDef.GetPortraitName(data.Class, data.Gender, -1), false)

	UIEventListener.Get(selectBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    EventManager.BroadcastInternalForLua(EnumEventType.SelectPlayer, {data.ID, data.Name})
		CUIManager.CloseUI(CLuaUIResources.CommonSearchPlayerWnd)
	end)
end

function LuaCommonSearchPlayerWnd:OnReturnSearchFail()
    self.m_TableViewDataSource.count = 0
    self.TableView:ReloadData(true,false)
    self.VoidLabel.gameObject:SetActive(true)
end

function LuaCommonSearchPlayerWnd:OnEnable()
	self.m_SelectSuccessAction = DelegateFactory.Action_List_CRelationshipBasicPlayerInfo(function(infos)

		self:OnReturnSearchSuccess(infos)
	end)
	self.m_SelectFailAction = DelegateFactory.Action(function()

		self:OnReturnSearchFail()
	end)

	EventManager.AddListenerInternal(EnumEventType.SearchPlayerSuccess, self.m_SelectSuccessAction)
    EventManager.AddListenerInternal(EnumEventType.SearchPlayerFail, self.m_SelectFailAction)
end

function LuaCommonSearchPlayerWnd:OnDisable()
	EventManager.RemoveListenerInternal(EnumEventType.SearchPlayerSuccess, self.m_SelectSuccessAction)
    EventManager.RemoveListenerInternal(EnumEventType.SearchPlayerFail, self.m_SelectFailAction)
end


--@region UIEvent

function LuaCommonSearchPlayerWnd:OnSearchBtnClick()
	local keywords = StringTrim(self.Input.value)
    if System.String.IsNullOrEmpty(keywords) or CUICommonDef.GetStrByteLength(keywords) < Constants.MaxNumOfFriendSearchBytes then
        CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("关键词长度太短！"), 1)
        return
    end
    CIMMgr.Inst:SearchOnlinePlayer(keywords)
end

function LuaCommonSearchPlayerWnd:OnClearBtnClick()
	self.Input.value = ""
end

--@endregion UIEvent

