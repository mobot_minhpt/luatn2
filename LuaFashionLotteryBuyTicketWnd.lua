local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemMgr=import "L10.Game.CItemMgr"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CShopMallMgr = import "L10.UI.CShopMallMgr"

LuaFashionLotteryBuyTicketWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFashionLotteryBuyTicketWnd, "Ticket", GameObject)
RegistChildComponent(LuaFashionLotteryBuyTicketWnd, "OwnedCount", UILabel)    
RegistChildComponent(LuaFashionLotteryBuyTicketWnd, "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)          
RegistChildComponent(LuaFashionLotteryBuyTicketWnd, "QnCostAndOwnMoney", CCurentMoneyCtrl)

RegistChildComponent(LuaFashionLotteryBuyTicketWnd, "RuleBtn", GameObject)
RegistChildComponent(LuaFashionLotteryBuyTicketWnd, "ConfrimBtn", GameObject)
RegistChildComponent(LuaFashionLotteryBuyTicketWnd, "DiscountTip", UILabel)

RegistClassMember(LuaFashionLotteryBuyTicketWnd, "m_TicketItemId")
--@endregion RegistChildComponent end

function LuaFashionLotteryBuyTicketWnd:Awake()
    UIEventListener.Get(self.RuleBtn).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:OnRuleBtnClick()
    end)
    UIEventListener.Get(self.ConfrimBtn).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:OnConfrimBtnClick()
    end)
end

function LuaFashionLotteryBuyTicketWnd:Init()
    self:InitTicket()
    self.DiscountTip.gameObject:SetActive(false)
    
    self.QnCostAndOwnMoney:SetCost(0)
    self.QnIncreseAndDecreaseButton:SetValue(1)
    self.QnIncreseAndDecreaseButton:SetMinMax(1, 1, 1)
    self:OnValueChanged(1)

    -- 先初始化后再绑定行为，避免打开时多次查询
    self.QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function(val)
        self:OnValueChanged(val)
    end)
end

function LuaFashionLotteryBuyTicketWnd:InitQnIncreseAndDecreaseButton(price, originalPrice)
    local jade = CClientMainPlayer.Inst.Jade
    if CShopMallMgr.CanUseBindJadeBuy(LuaFashionLotteryMgr.m_LotteryTicketId) then
        jade = CClientMainPlayer.Inst.Jade + CClientMainPlayer.Inst.BindJade
    end
    local maxCount = 1
    if jade >= price then
        maxCount = math.floor((jade - price) / originalPrice) + 1
        maxCount = math.min(maxCount, 999)
    end
    self.QnIncreseAndDecreaseButton:SetMinMax(1, maxCount, 1)
end

function LuaFashionLotteryBuyTicketWnd:InitTicket()
    local ticketData = CItemMgr.Inst:GetItemTemplate(LuaFashionLotteryMgr.m_LotteryTicketId)
    if ticketData then
        self.Ticket.transform:Find("Name"):GetComponent(typeof(UILabel)).text = ticketData.Name
        self.Ticket.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(ticketData.Icon)
        self.OwnedCount.text = tostring(LuaFashionLotteryMgr:GetLotteryTicketCount())
    end
end

function LuaFashionLotteryBuyTicketWnd:RefreshItem()
    self.OwnedCount.text = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_TicketItemId)
    Gac2Gas.FashionLotteryQueryTicketPrice(self.QnIncreseAndDecreaseButton:GetValue())
end

function LuaFashionLotteryBuyTicketWnd:OnValueChanged(val)
    Gac2Gas.FashionLotteryQueryTicketPrice(self.QnIncreseAndDecreaseButton:GetValue())
end

function LuaFashionLotteryBuyTicketWnd:OnFashionLotteryQueryTicketPriceResult(count, price, originalPrice, discount)
    if count == 1 then
        self:InitQnIncreseAndDecreaseButton(price, originalPrice)
    end
    if count == self.QnIncreseAndDecreaseButton:GetValue() then
        self.QnCostAndOwnMoney:SetCost(price)
        if discount < 1 then
            self.DiscountTip.gameObject:SetActive(true)
            self.DiscountTip.text = SafeStringFormat3(LocalString.GetString("首份%d折"), discount * 10)
        else
            self.DiscountTip.gameObject:SetActive(false)
        end
    end
end

--@region UIEvent
function LuaFashionLotteryBuyTicketWnd:OnRuleBtnClick()
    g_MessageMgr:ShowMessage("FashionLottery_BuyTicket_Rule_Tip")
end

function LuaFashionLotteryBuyTicketWnd:OnConfrimBtnClick()
    Gac2Gas.FashionLotteryBuyTicket(self.QnIncreseAndDecreaseButton:GetValue())
    CUIManager.CloseUI(LuaFashionLotteryMgr.m_OpenWnd.BuyTicketWnd)
end
--@endregion UIEvent

function LuaFashionLotteryBuyTicketWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "RefreshItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "RefreshItem")
    g_ScriptEvent:AddListener("FashionLotteryQueryTicketPriceResult", self, "OnFashionLotteryQueryTicketPriceResult")
end

function LuaFashionLotteryBuyTicketWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "RefreshItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "RefreshItem")
    g_ScriptEvent:RemoveListener("FashionLotteryQueryTicketPriceResult", self, "OnFashionLotteryQueryTicketPriceResult")
end