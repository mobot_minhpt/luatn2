require("common/common_include")

local Gac2Gas=import "L10.Game.Gac2Gas"
local UILabel = import "UILabel"
local UIScrollView = import "UIScrollView"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CButton = import "L10.UI.CButton"
local MsgPackImpl = import "MsgPackImpl"
local LocalString = import "LocalString"
local Extensions = import "Extensions"
local UISprite = import "UISprite"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUITexture = import "L10.UI.CUITexture"
local LuaUtils = import "LuaUtils"
local Color = import "UnityEngine.Color"
local UITable = import "UITable"
local UIProgressBar = import "UIProgressBar"
local CUIFx = import "L10.UI.CUIFx"

CLuaWorldCupHomeTeamView=class()

RegistClassMember(CLuaWorldCupHomeTeamView, "m_Stage1View")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_Stage2View")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_MainArea")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_GroupInfo")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_CountryList")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_CountryInfo")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_ConfirmChoiceButton")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_HomeTeamTipsButton")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_ChooseHomeTeamId")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_WinNumLabel")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_LoseNumLabel")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_TieNumLabel")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_TeamIconTexture")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_TeamNameLabel")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_XiaoZuSaiInfoLabel")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_LastChooseHomeTeamGameObject")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_CancelChangeButton")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_ConfirmChangeButton")


RegistClassMember(CLuaWorldCupHomeTeamView, "m_ScorllView")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_Table")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_ItemTemplate")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_ChangeHomeTeamButton")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_SaiChengButton")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_LiBaoUIFx")

RegistClassMember(CLuaWorldCupHomeTeamView, "m_ProgressBar")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_ProgressBarLabel1")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_ProgressBarLabel2")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_ProgressBarLabel3")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_ProgressBarLabel4")
RegistClassMember(CLuaWorldCupHomeTeamView, "m_ProgressBarLabel5")

function CLuaWorldCupHomeTeamView:Init(tf)
    self.m_Stage1View = tf.transform:Find("Stage1View").gameObject
    self.m_Stage2View = tf.transform:Find("Stage2View").gameObject
    self.m_MainArea = tf.transform:Find("Stage1View/MainArea").gameObject
    self.m_GroupInfo = tf.transform:Find("Stage1View/GroupInfo").gameObject
    self.m_CountryInfo = tf.transform:Find("Stage1View/GroupInfo/CountryList/CountryInfo").gameObject
    self.m_ConfirmChoiceButton = tf.transform:Find("Stage1View/BottomArea/ConfirmChoiceButton"):GetComponent(typeof(CButton))
    self.m_HomeTeamTipsButton = tf.transform:Find("Stage1View/BottomArea/HomeTeamTipsButton"):GetComponent(typeof(CButton))
    self.m_CancelChangeButton = tf.transform:Find("Stage1View/BottomArea/CancelChangeButton"):GetComponent(typeof(CButton))
    self.m_ConfirmChangeButton = tf.transform:Find("Stage1View/BottomArea/ConfirmChangeButton"):GetComponent(typeof(CButton))

    self.m_WinNumLabel = tf.transform:Find("Stage2View/TopArea/WinNum"):GetComponent(typeof(UILabel))
    self.m_LoseNumLabel = tf.transform:Find("Stage2View/TopArea/LoseNum"):GetComponent(typeof(UILabel))
    self.m_TieNumLabel = tf.transform:Find("Stage2View/TopArea/TieNum"):GetComponent(typeof(UILabel))

    self.m_TeamIconTexture = tf.transform:Find("Stage2View/TopArea/TeamIcon"):GetComponent(typeof(CUITexture))
    self.m_TeamNameLabel = tf.transform:Find("Stage2View/TopArea/TeamName"):GetComponent(typeof(UILabel))
    self.m_XiaoZuSaiInfoLabel = tf.transform:Find("Stage2View/TopArea/XiaoZuSaiInfo"):GetComponent(typeof(UILabel))

    self.m_GroupInfo:SetActive(false)
    self.m_CountryInfo:SetActive(false)

    self.m_ScorllView = tf.transform:Find("Stage2View/ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_Table = tf.transform:Find("Stage2View/ScrollView/Table"):GetComponent(typeof(UITable))
    self.m_ItemTemplate = tf.transform:Find("Stage2View/ScrollView/PlayInfoItem").gameObject
    self.m_ItemTemplate:SetActive(false)  --强制False，防止被改坏

    self.m_ChangeHomeTeamButton = tf.transform:Find("Stage2View/BottomArea/ChangeHomeTeamButton"):GetComponent(typeof(CButton))
    self.m_SaiChengButton = tf.transform:Find("Stage2View/BottomArea/SaiChengButton"):GetComponent(typeof(CButton))

    self.m_LiBaoUIFx = tf.transform:Find("Stage2View/TopArea/LiBaoUIFx"):GetComponent(typeof(CUIFx))

    self.m_ProgressBar = tf.transform:Find("Stage2View/BottomArea/ProgressBar"):GetComponent(typeof(UIProgressBar))
    self.m_ProgressBarLabel1 = tf.transform:Find("Stage2View/BottomArea/ProgressBar/Dot1/Label"):GetComponent(typeof(UILabel))
    self.m_ProgressBarLabel2 = tf.transform:Find("Stage2View/BottomArea/ProgressBar/Dot2/Label"):GetComponent(typeof(UILabel))
    self.m_ProgressBarLabel3 = tf.transform:Find("Stage2View/BottomArea/ProgressBar/Dot3/Label"):GetComponent(typeof(UILabel))
    self.m_ProgressBarLabel4 = tf.transform:Find("Stage2View/BottomArea/ProgressBar/Dot4/Label"):GetComponent(typeof(UILabel))
    self.m_ProgressBarLabel5 = tf.transform:Find("Stage2View/BottomArea/ProgressBar/Dot5/Label"):GetComponent(typeof(UILabel))

    -- 初始化设置成0, 避免多人使用同一设备造成干扰, 反正立马就会请求主队信息
    CLuaWorldCupMgr.m_HomeTeamId = 0

    -- 获取主队信息
    Gac2Gas.QueryWorldCupHomeTeamPlayInfo()

    self.m_ChooseHomeTeamId = 0
    CommonDefs.AddOnClickListener(self.m_ConfirmChoiceButton.gameObject, DelegateFactory.Action_GameObject(function (go)
        if self.m_ChooseHomeTeamId == 0 then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请选择一个队伍吧"))
            return
        end
        Gac2Gas.RequestSetWorldCupHomeTeam(self.m_ChooseHomeTeamId)
    end), false)

    CommonDefs.AddOnClickListener(self.m_HomeTeamTipsButton.gameObject, DelegateFactory.Action_GameObject(function (go)
        g_MessageMgr:ShowMessage("WorldCup_HomeTeam_Choose_Tips")
    end), false)

    CommonDefs.AddOnClickListener(self.m_ChangeHomeTeamButton.gameObject, DelegateFactory.Action_GameObject(function (go)
        self.m_Stage1View:SetActive(true)
        self.m_Stage2View:SetActive(false)
        self:Stage1Refresh()
    end), false)

    CommonDefs.AddOnClickListener(self.m_SaiChengButton.gameObject, DelegateFactory.Action_GameObject(function (go)
        CUIManager.ShowUI("WorldCupAgendaWnd")
    end), false)

    CommonDefs.AddOnClickListener(self.m_CancelChangeButton.gameObject, DelegateFactory.Action_GameObject(function (go)
        self.m_Stage1View:SetActive(false)
        self.m_Stage2View:SetActive(true)
    end), false)

    CommonDefs.AddOnClickListener(self.m_ConfirmChangeButton.gameObject, DelegateFactory.Action_GameObject(function (go)
        if self.m_ChooseHomeTeamId == 0 then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请选择一个队伍吧"))
            return
        end

        if self.m_ChooseHomeTeamId == CLuaWorldCupMgr.m_HomeTeamId then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("该队伍已经是你的主队了"))
            return
        end

        local msg = SafeStringFormat(LocalString.GetString("更换主队需要消耗 %s 银两"), CLuaWorldCupMgr.m_SetHomeTeamNeedSilver)
        local okFunc = function () Gac2Gas.RequestSetWorldCupHomeTeam(self.m_ChooseHomeTeamId) end
        g_MessageMgr:ShowOkCancelMessage(msg, okFunc, nil, LocalString.GetString("更换"), LocalString.GetString("取消"), true)
    end), false)

    CommonDefs.AddOnClickListener(self.m_LiBaoUIFx.gameObject, DelegateFactory.Action_GameObject(function (go)
        Gac2Gas.RequestReceiveWorldCupHomeTeamAward()
    end), false)
end

function CLuaWorldCupHomeTeamView:Stage1Refresh()
    self.m_LastChooseHomeTeamGameObject = nil
    self.m_ChooseHomeTeamId = 0

    Extensions.RemoveAllChildren(self.m_MainArea.transform)
    local xx = -592
    local yy = 233
    local xshift = 378
    local yshift = 413
    local PosTbl = {{xx, yy},        {xx+xshift,  yy},        {2*xshift+xx, yy},        {3*xshift+xx, yy}, 
                    {xx, yy-yshift}, {xx+xshift,  yy-yshift}, {2*xshift+xx, yy-yshift}, {3*xshift+xx, yy-yshift}}

    local GroupNameTbl = {"A", "B", "C", "D", "E", "F", "G", "H",}
    for i=1, 8 do
        local go = CUICommonDef.AddChild(self.m_MainArea, self.m_GroupInfo)
        go:SetActive(true)
        local GroupName = go.transform:Find("GroupName"):GetComponent(typeof(UILabel))
        GroupName.text = SafeStringFormat(LocalString.GetString("%s组"), GroupNameTbl[i])
        local CountryList = go.transform:Find("CountryList").gameObject
        for j=1, 4 do
            local go2 = CUICommonDef.AddChild(CountryList, self.m_CountryInfo)
            go2:SetActive(true)
            local CountryName = go2.transform:Find("CountryName"):GetComponent(typeof(UILabel))
            local CountryIcon = go2.transform:Find("CountryIcon"):GetComponent(typeof(CUITexture))

            local tid = (i-1)*4 + j
            local TeamInfo = CLuaWorldCupMgr.GetTeamInfoById(tid)
            CountryName.text = LocalString.GetString(TeamInfo.Name)
            if tid == CLuaWorldCupMgr.m_HomeTeamId then
                CountryName.color = Color.green
            end

            CountryIcon:LoadMaterial(TeamInfo.Icon)
            LuaUtils.SetLocalPosition(go2.transform, 0, 100-88*(j-1), 0)

            CommonDefs.AddOnClickListener(go2.gameObject, DelegateFactory.Action_GameObject(function (go3)
                if self.m_LastChooseHomeTeamGameObject then
                    self.m_LastChooseHomeTeamGameObject:GetComponent(typeof(CButton)).Selected = false
                end
                go3:GetComponent(typeof(CButton)).Selected = true
                self.m_LastChooseHomeTeamGameObject = go3
                self.m_ChooseHomeTeamId = tid
            end), false)
        end
        LuaUtils.SetLocalPosition(go.transform, PosTbl[i][1], PosTbl[i][2], 0)
    end

    -- 需要区分是否为第一次
    if CLuaWorldCupMgr.m_HomeTeamId == 0 then
        self.m_CancelChangeButton.gameObject:SetActive(false)
        self.m_ConfirmChangeButton.gameObject:SetActive(false)
        self.m_ConfirmChoiceButton.gameObject:SetActive(true)
    else
        self.m_CancelChangeButton.gameObject:SetActive(true)
        self.m_ConfirmChangeButton.gameObject:SetActive(true)
        self.m_ConfirmChoiceButton.gameObject:SetActive(false)
    end
end

function CLuaWorldCupHomeTeamView:Stage2Refresh(tid, resultData, bAlert, groupRank, settimes, taotaiStage)
    if bAlert then
        self.m_LiBaoUIFx.gameObject:SetActive(true)
    else
        self.m_LiBaoUIFx.gameObject:SetActive(false)
    end

    -- 清空内容
    Extensions.RemoveAllChildren(self.m_Table.transform)

    local WinNum = 0
    local LoseNum = 0
    local TieNum = 0

    local maxRound = 0

    local list = MsgPackImpl.unpack(resultData)
    for i=0, list.Count-1, 6 do
        local go = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_ItemTemplate)
        go:SetActive(true)

        local round = list[i]
        local tid1 = list[i+1]
        local tid2 = list[i+2]
        local score1 = list[i+3]
        local score2 = list[i+4]
        local dianqiu = list[i+5]

        if round > maxRound then
            maxRound = round
        end

        local PlayInfo = CLuaWorldCupMgr.GetPlayInfoById(round)
        local PlayTime = go.transform:Find("PlayTime"):GetComponent(typeof(UILabel))
        PlayTime.text = LocalString.GetString(PlayInfo.Time)

        local PlayRound = go.transform:Find("PlayRound"):GetComponent(typeof(UILabel))
        PlayRound.text = LocalString.GetString(PlayInfo.RoundInfo)

        local TeamInfo1 = CLuaWorldCupMgr.GetTeamInfoById(tid1)
        local Team1Name = go.transform:Find("Team1Name"):GetComponent(typeof(UILabel))
        Team1Name.text = LocalString.GetString(TeamInfo1.Name)
        if tid1 == CLuaWorldCupMgr.m_HomeTeamId then
            Team1Name.color = Color.green
        end

        local TeamInfo2 = CLuaWorldCupMgr.GetTeamInfoById(tid2)
        local Team2Name = go.transform:Find("Team2Name"):GetComponent(typeof(UILabel))
        Team2Name.text = LocalString.GetString(TeamInfo2.Name)
        if tid2 == CLuaWorldCupMgr.m_HomeTeamId then
            Team2Name.color = Color.green
        end

        local Texture1 = go.transform:Find("Texture1"):GetComponent(typeof(CUITexture))
        Texture1:LoadMaterial(TeamInfo1.Icon)

        local Texture2 = go.transform:Find("Texture2"):GetComponent(typeof(CUITexture))
        Texture2:LoadMaterial(TeamInfo2.Icon)

        local DianQiu = go.transform:Find("DianQiu"):GetComponent(typeof(UISprite))
        if dianqiu == 1 then
            DianQiu.gameObject:SetActive(true)
        else
            DianQiu.gameObject:SetActive(false)
        end

        local ResultWin = go.transform:Find("ResultWin"):GetComponent(typeof(UISprite))
        local ResultLose = go.transform:Find("ResultLose"):GetComponent(typeof(UISprite))
        local ResultTie = go.transform:Find("ResultTie"):GetComponent(typeof(UISprite))
        ResultWin.gameObject:SetActive(false)
        ResultLose.gameObject:SetActive(false)
        ResultTie.gameObject:SetActive(false)

        -- 没有比赛结果什么也不显示
        local vs1 = go.transform:Find("vs1"):GetComponent(typeof(UISprite))
        local vs2 = go.transform:Find("vs2"):GetComponent(typeof(UILabel))
        if score1 == -1 or score2 == -1 then
            vs1.gameObject:SetActive(true)
            vs2.gameObject:SetActive(false)
        else
            vs1.gameObject:SetActive(false)
            vs2.gameObject:SetActive(true)
            vs2.text = SafeStringFormat(LocalString.GetString("%s:%s"), score1, score2)

            if score1 > score2 then
                if tid1 == tid then
                    ResultWin.gameObject:SetActive(true)
                    WinNum = WinNum + 1
                else
                    ResultLose.gameObject:SetActive(true)
                    LoseNum = LoseNum + 1
                end
            elseif score1 < score2 then
                if tid2 == tid then
                    ResultWin.gameObject:SetActive(true)
                    WinNum = WinNum + 1
                else
                    ResultLose.gameObject:SetActive(true)
                    LoseNum = LoseNum + 1
                end
            else
                TieNum = TieNum + 1
                ResultTie.gameObject:SetActive(true)
            end
        end
    end

    self.m_Table:Reposition()
    self.m_ScorllView:ResetPosition()

    self.m_WinNumLabel.text = tostring(WinNum)
    self.m_LoseNumLabel.text = tostring(LoseNum)
    self.m_TieNumLabel.text = tostring(TieNum)

    local TeamInfo = CLuaWorldCupMgr.GetTeamInfoById(tid)
    self.m_TeamIconTexture:LoadMaterial(TeamInfo.Icon)
    self.m_TeamNameLabel.text = SafeStringFormat(LocalString.GetString("%s-%s组"), TeamInfo.Name, TeamInfo.Group)
    if groupRank == 0 then
        self.m_XiaoZuSaiInfoLabel.text = LocalString.GetString("小组赛排名：暂无")
    else
        self.m_XiaoZuSaiInfoLabel.text = SafeStringFormat(LocalString.GetString("小组赛排名：%s"), groupRank)
    end

    if settimes >= CLuaWorldCupMgr.m_MaxSetHomeTeamTimes then
        self.m_ChangeHomeTeamButton.Enabled = false
    else
        self.m_ChangeHomeTeamButton.Enabled = true
    end

    self:HandleProgressBar(taotaiStage, maxRound)
end

function CLuaWorldCupHomeTeamView:HandleProgressBar(stage, maxRound)
    self.m_ProgressBar.value = 0
    self.m_ProgressBarLabel1.text = LocalString.GetString("小组赛")
    self.m_ProgressBarLabel2.text = LocalString.GetString("1/8决赛")
    self.m_ProgressBarLabel3.text = LocalString.GetString("1/4决赛")
    self.m_ProgressBarLabel4.text = LocalString.GetString("半决赛")
    self.m_ProgressBarLabel5.text = LocalString.GetString("决赛")

    self.m_ProgressBarLabel1.color = NGUIText.ParseColor24("ACF8FF", 0)
    self.m_ProgressBarLabel2.color = NGUIText.ParseColor24("ACF8FF", 0)
    self.m_ProgressBarLabel3.color = NGUIText.ParseColor24("ACF8FF", 0)
    self.m_ProgressBarLabel4.color = NGUIText.ParseColor24("ACF8FF", 0)
    self.m_ProgressBarLabel5.color = NGUIText.ParseColor24("ACF8FF", 0)
    
    if not stage then
        stage = 0
    end

    -- 0表示还没被淘汰
    if stage == 0 then
        local value = CLuaWorldCupMgr.GetProgressBarValueByMaxRound(maxRound)
        self.m_ProgressBar.value = value
        return
    end

    if stage == 1 then
        self.m_ProgressBar.value = 0
        self.m_ProgressBarLabel1.text = LocalString.GetString("小组赛淘汰")
        self.m_ProgressBarLabel1.color = Color.red
    elseif stage == 2 then
        self.m_ProgressBar.value = 0.25
        self.m_ProgressBarLabel2.text = LocalString.GetString("1/8决赛淘汰")
        self.m_ProgressBarLabel2.color = Color.red
    elseif stage == 3 then
        self.m_ProgressBar.value = 0.5
        self.m_ProgressBarLabel3.text = LocalString.GetString("1/4决赛淘汰")
        self.m_ProgressBarLabel3.color = Color.red
    elseif stage == 4 then
        self.m_ProgressBar.value = 0.75
        self.m_ProgressBarLabel4.text = LocalString.GetString("半决赛淘汰")
        self.m_ProgressBarLabel4.color = Color.red
    elseif stage == 5 then
        self.m_ProgressBar.value = 1
        self.m_ProgressBarLabel5.text = LocalString.GetString("决赛淘汰")
        self.m_ProgressBarLabel5.color = Color.red
    elseif stage == 6 then
        self.m_ProgressBar.value = 1
    end
end

function CLuaWorldCupHomeTeamView:OnSelected()
    -- 玩家选择时再次请求主队信息, 相当于是刷新
    Gac2Gas.QueryWorldCupHomeTeamPlayInfo()
end

function CLuaWorldCupHomeTeamView:ReplyWorldCupHomeTeamPlayInfo(tid, resultData, bAlert, groupRank, settimes, taotaiStage)
    -- 根据是否有主队来决定显示哪个界面
    if CLuaWorldCupMgr.m_HomeTeamId and CLuaWorldCupMgr.m_HomeTeamId~=0 then
        self.m_Stage1View:SetActive(false)
        self.m_Stage2View:SetActive(true)
        self:Stage2Refresh(tid, resultData, bAlert, groupRank, settimes, taotaiStage)
    else
        self.m_Stage1View:SetActive(true)
        self.m_Stage2View:SetActive(false)
        self:Stage1Refresh()
    end
end

return CLuaWorldCupHomeTeamView
