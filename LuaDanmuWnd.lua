local Vector3 = import "UnityEngine.Vector3"
local UIRoot = import "UIRoot"
local GameSetting_Common = import "L10.Game.GameSetting_Common"
local Extensions = import "Extensions"
local Ease = import "DG.Tweening.Ease"
local CYuanDanMgr = import "L10.Game.CYuanDanMgr"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Color = import "UnityEngine.Color"
local CDanmuMgr = import "L10.UI.CDanmuMgr"

local CUIGameObjectPool=import "L10.UI.CUIGameObjectPool"
local QnSelectableButton=import "L10.UI.QnSelectableButton"


CLuaDanmuWnd = class()
RegistClassMember(CLuaDanmuWnd,"MaxDanMuLength")
RegistClassMember(CLuaDanmuWnd,"settingBtn")
RegistClassMember(CLuaDanmuWnd,"closeBtn")
RegistClassMember(CLuaDanmuWnd,"sendBtn")
RegistClassMember(CLuaDanmuWnd,"zhankaiBtn")
RegistClassMember(CLuaDanmuWnd,"bulletScreen")
RegistClassMember(CLuaDanmuWnd,"screen")
RegistClassMember(CLuaDanmuWnd,"pool")
RegistClassMember(CLuaDanmuWnd,"zhankaiTf")
RegistClassMember(CLuaDanmuWnd,"shousuoTf")
RegistClassMember(CLuaDanmuWnd,"chatInput")

function CLuaDanmuWnd:Awake()
    self.MaxDanMuLength = 0
    self.settingBtn = self.transform:Find("Anchor/ZhanKai/Anchor/Setting"):GetComponent(typeof(QnSelectableButton))
    self.closeBtn = self.transform:Find("Anchor/ZhanKai/Anchor/CloseButton").gameObject
    self.sendBtn = self.transform:Find("Anchor/ZhanKai/SendButton").gameObject
    self.bulletScreen = self.transform:Find("BulletScreen/ScreenBound"):GetComponent(typeof(UIWidget))
    self.screen = self.transform:Find("BulletScreen").gameObject
    self.pool = self.transform:Find("BulletScreen/Pool"):GetComponent(typeof(CUIGameObjectPool))
    self.zhankaiTf = self.transform:Find("Anchor/ZhanKai")
    self.chatInput = self.transform:Find("Anchor/ZhanKai/ChatInput"):GetComponent(typeof(UIInput))


    if UIRoot.EnableIPhoneXAdaptation then
        local x = self.settingBtn.transform.localPosition.x
        Extensions.SetLocalPositionX(self.settingBtn.transform, x - UIRoot.VirtualIPhoneXWidthMargin)
        x = self.closeBtn.transform.localPosition.x
        Extensions.SetLocalPositionX(self.closeBtn.transform, x - UIRoot.VirtualIPhoneXWidthMargin)
    end
    self.chatInput.characterLimit = self.MaxDanMuLength
    self.zhankaiTf.gameObject:SetActive(false)

    self.settingBtn:SetSelected(true, false)
    UIEventListener.Get(self.closeBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self.zhankaiTf.gameObject:SetActive(false)
        g_ScriptEvent:BroadcastInLua("ShowDanmuEntry",true)
    end)

    self.settingBtn.OnButtonSelected = DelegateFactory.Action_bool(function(b)
        self:OnSettingButtonSelected(b)
    end)
    UIEventListener.Get(self.sendBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSendButtonClicked(go)
    end)
end



function CLuaDanmuWnd:Init( )
    self.MaxDanMuLength = GameSetting_Common.GetData().MaxDanMuLength
    self:OpenDanMu()
end

function CLuaDanmuWnd:OnSettingButtonSelected( b) 
    if b then
        self:OpenDanMu()
    else
        self:CloseDanMu()
    end
end
function CLuaDanmuWnd:OnInputValueChange( )
    --限制输入字数
    local str = self.chatInput.value
    local len = self:GetLength(str)
    if len > self.MaxDanMuLength then
        repeat
            str = CommonDefs.StringSubstring2(str, 0, CommonDefs.StringLength(str) - 1)
            len = self:GetLength(str)
        until not (len > self.MaxDanMuLength)
        self.chatInput.value = str
    end
end
function CLuaDanmuWnd:OnRecvChatMsg( args) 
    local str=args[0]
    do
        local i = 0
        while i < str.Count do
            local continue
            repeat
                local bullet = str[i]

                bullet = CWordFilterMgr.Inst:DoFilterOnReceiveWithoutVoiceCheck(0, bullet, nil, true)

                if bullet == nil then
                    continue = true
                    break
                end

                self:OnAddBullet(bullet)
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
end
function CLuaDanmuWnd:OnAddBullet( content) 
    if content==nil or content=="" then
        return
    end
    local label = self.pool:GetFromPool(0)
    label.transform.parent = self.bulletScreen.transform
    label.transform.localScale = Vector3.one
    label:SetActive(true)
    local bullet = CommonDefs.GetComponent_GameObject_Type(label, typeof(UILabel))
    bullet.text = content
    bullet.color = Color(UnityEngine_Random(0.5, 1), UnityEngine_Random(0.5, 1), UnityEngine_Random(0.5, 1))
    bullet.fontSize = UnityEngine_Random(38, 52)
    bullet.transform.localPosition = Vector3(math.floor(self.bulletScreen.width / 2), UnityEngine_Random(math.floor(- self.bulletScreen.height / 2), math.floor(self.bulletScreen.height / 2)), 0)
    CommonDefs.OnComplete_Tweener(CommonDefs.SetEase_Tweener(LuaTweenUtils.DOLocalMoveX(bullet.transform, math.floor(- self.bulletScreen.width / 2) - bullet.width, UnityEngine_Random(6, 12), false), Ease.Linear), DelegateFactory.TweenCallback(function () 
        self.pool:Recycle(bullet.gameObject)
    end))
end
function CLuaDanmuWnd:OpenDanMu( )
    Gac2Gas.RequestOpenDanMu()
    self.screen.gameObject:SetActive(true)
end
function CLuaDanmuWnd:CloseDanMu( )
    Gac2Gas.RequestCloseDanMu()
    self.screen.gameObject:SetActive(false)
end
function CLuaDanmuWnd:OnSendButtonClicked( go) 
    if self.chatInput.value==nil or self.chatInput.value=="" then
        return
    end
    local chatContent = self.chatInput.value
    local chatLength = self:GetLength(chatContent)
    if chatLength <= self.MaxDanMuLength then
        if not CDanmuMgr.countdown then
            local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(chatContent, true)
            chatContent = ret.msg
            if chatContent == nil then
                return
            end

            if CYuanDanMgr.Inst.IsInConcertPlay then
                Gac2Gas.NewYearConcertSendDanMu(chatContent, ret.shouldBeIgnore)
            else
                Gac2Gas.SendDanMu(chatContent, ret.shouldBeIgnore)
            end
            CDanmuMgr.Record()
            self.chatInput.value = ""
        else
            local span = CommonDefs.op_Subtraction_DateTime_DateTime(CServerTimeMgr.Inst:GetZone8Time(), CDanmuMgr.startSendTime)
            local leftSecond = math.floor((CDanmuMgr.cd - span.TotalSeconds))
            if leftSecond > 0 then
                g_MessageMgr:ShowMessage("BIWU_GUANZHAN_DANMU_CD", leftSecond)
            end
        end
    else
        g_MessageMgr:ShowMessage("BIWU_GUANZHAN_INPUT_LIMITED", self.MaxDanMuLength)
    end
end



function CLuaDanmuWnd:OnEnable()
    g_ScriptEvent:AddListener("SendDanMu",self,"OnRecvChatMsg")    
    g_ScriptEvent:AddListener("MainPlayerDestroyed",self,"OnMainPlayerDestroyed")    
    g_ScriptEvent:AddListener("ShowDanmuInput",self,"OnShowDanmuInput")    
    
end
function CLuaDanmuWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendDanMu",self,"OnRecvChatMsg")    
    g_ScriptEvent:RemoveListener("MainPlayerDestroyed",self,"OnMainPlayerDestroyed")
    g_ScriptEvent:RemoveListener("ShowDanmuInput",self,"OnShowDanmuInput")    

end
function CLuaDanmuWnd:OnShowDanmuInput()
    self.zhankaiTf.gameObject:SetActive(true)
end

function CLuaDanmuWnd:OnMainPlayerDestroyed()
    CUIManager.CloseUI(CUIResources.DanmuWnd)
	g_ScriptEvent:BroadcastInLua("ShowDanmuEntry",false)
end

function CLuaDanmuWnd:GetLength(str)
    return math.floor(string.len( str )/3)
end
