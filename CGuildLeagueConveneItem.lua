-- Auto Generated!!
local CGuildLeagueConveneItem = import "L10.UI.CGuildLeagueConveneItem"
local CGuildLeagueConveneSortMgr = import "L10.Game.CGuildLeagueConveneSortMgr"
local CGuildLeagueMgr = import "L10.Game.CGuildLeagueMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Double = import "System.Double"
local EnumClass = import "L10.Game.EnumClass"
local Gac2Gas = import "L10.Game.Gac2Gas"
local GuildDefine = import "L10.Game.GuildDefine"
local LocalString = import "LocalString"
local Profession = import "L10.Game.Profession"
local UIEventListener = import "UIEventListener"
local UInt64 = import "System.UInt64"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildLeagueConveneItem.m_Init_CS2LuaHook = function (this, info) 
    this.mInfo = info
    this.nameLabel.text = info.Name
    this.clsSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), info.Class))

    this.levelLabel.text = System.String.Format("Lv.{0}", info.Level)
    local isForeignAidMember = CGuildLeagueMgr.Inst:IsForeignAidMember(info)
    if not isForeignAidMember then
        this.rankLabel.text = GuildDefine.GetOfficeName(info.Title)
    else
        this.rankLabel.text = info.Title == EnumGuildForeignAidApplyInfoType.eElite and LocalString.GetString("精英外援") or LocalString.GetString("普通外援")
    end
    --xiuweiLabel.text = info.XiuWei.ToString();
    this.xiuweiLabel.text = NumberComplexToString(NumberTruncate(info.XiuWei, 1), typeof(Double), "F1")
    --一位小数显示
    this.xiulianLabel.text = tostring(info.XiuLian)
    --xiulianLabel.text = info.XiuLian.Truncate(1).ToString("F1");//一位小数显示

    this.equipScoreLabel.text = tostring(info.EquipScore)

    UIEventListener.Get(this.conveneBtn).onClick = MakeDelegateFromCSFunction(this.OnClickConveneButton, VoidDelegate, this)

    if info ~= nil and CommonDefs.ListContains(CGuildLeagueConveneSortMgr.convenedPlayerIds, typeof(UInt64), info.PlayerId) then
        CUICommonDef.SetActive(this.conveneBtn, false, true)
        this.btnLabel.text = LocalString.GetString("征召中")
    else
        this.btnLabel.text = LocalString.GetString("征召")
        CUICommonDef.SetActive(this.conveneBtn, true, true)
        if not CGuildLeagueMgr.Inst:CanBeConvened(info) then
            CUICommonDef.SetActive(this.conveneBtn, false, true)
        elseif CGuildLeagueMgr.Inst.inGuildLeagueMainScene and info.Extra == EnumGuildLeagueCallUpType_lua.eVice then
            CUICommonDef.SetActive(this.conveneBtn, true, false)
            UIEventListener.Get(this.conveneBtn).onClick = MakeDelegateFromCSFunction(this.OnClickConveneButtonNoAccess, VoidDelegate, this)
        end
    end
end
CGuildLeagueConveneItem.m_OnClickConveneButton_CS2LuaHook = function (this, go) 
    if CGuildLeagueMgr.Inst.inGuildLeagueMainScene then
        Gac2Gas.RequestCallUpOnePlayer(this.mInfo.PlayerId, EnumGuildLeagueCallUpType_lua.eMain)
    elseif CGuildLeagueMgr.Inst.inGuildLeagueViceScene then
        Gac2Gas.RequestCallUpOnePlayer(this.mInfo.PlayerId, EnumGuildLeagueCallUpType_lua.eVice)
    end

    if this.mInfo ~= nil then
        if not CommonDefs.ListContains(CGuildLeagueConveneSortMgr.convenedPlayerIds, typeof(UInt64), this.mInfo.PlayerId) then
            CommonDefs.ListAdd(CGuildLeagueConveneSortMgr.convenedPlayerIds, typeof(UInt64), this.mInfo.PlayerId)
        end
    end

    CUICommonDef.SetActive(this.conveneBtn, false, true)
    this.btnLabel.text = LocalString.GetString("征召中")
end
