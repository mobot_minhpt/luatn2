local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CForcesMgr = import "L10.Game.CForcesMgr"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local MessageWndManager = import "L10.UI.MessageWndManager"

LuaGuoQing2023Mgr = {}

function LuaGuoQing2023Mgr:OpenGuoQing2023MainWnd()
    Gac2Gas.QueryGuoQing2023Info()
end

LuaGuoQing2023Mgr.m_GuoQing2023Info = nil
function LuaGuoQing2023Mgr:QueryGuoQing2023InfoResult(jyOpen, jyOpenMatch, jyPlayEnd, jyTimes, hzOpen, hzPlayEnd, hzTimes, hzNextTime)
    self.m_GuoQing2023Info = {}
    self.m_GuoQing2023Info.jyOpen = jyOpen
    self.m_GuoQing2023Info.jyOpenMatch = jyOpenMatch
    self.m_GuoQing2023Info.jyPlayEnd = jyPlayEnd
    self.m_GuoQing2023Info.jyTimes = jyTimes
    self.m_GuoQing2023Info.hzOpen = hzOpen
    self.m_GuoQing2023Info.hzPlayEnd = hzPlayEnd
    self.m_GuoQing2023Info.hzTimes = hzTimes
    self.m_GuoQing2023Info.hzNextTime = hzNextTime
    CUIManager.ShowUI(CLuaUIResources.GuoQing2023MainWnd)
end

------------------------
--- 晶元争夺战
------------------------
function LuaGuoQing2023Mgr:OnMainPlayerCreated(gamePlayId)
    if gamePlayId == self:GetJingYuanPlayGamePlayId() then
        LuaCommonGamePlayTaskViewMgr:AppendGameplayInfo(gamePlayId, 
        true, nil,
        false,
        true, function() return Gameplay_Gameplay.GetData(gamePlayId).Name end,
        true, function() return g_MessageMgr:FormatMessage("GuoQing2023_JIngYuanGame_TaskView_Desc") end,
        true, nil, nil,
        true, nil, function() g_MessageMgr:ShowMessage("GuoQing2023_JIngYuanGame_TaskView_Tip") end)

        CUIManager.ShowUI(CLuaUIResources.GuoQing2023JingYuanTopRightWnd)
        CUIManager.CloseUI(CLuaUIResources.GuoQing2023MainWnd)
        self:OnJingYuanPlayUpdatePlayerForceInfo()
    end
end

function LuaGuoQing2023Mgr:OnMainPlayerDestroyed()
    LuaGuoQing2023Mgr.m_JingYuanPlay_GameInfo = nil
end

LuaGuoQing2023Mgr.m_JingYuanPlay_GamePlayId = nil
function LuaGuoQing2023Mgr:GetJingYuanPlayGamePlayId()
    if self.m_JingYuanPlay_GamePlayId == nil then
        self.m_JingYuanPlay_GamePlayId = GuoQing2023_JingYuanPlay.GetData().GamePlayId
    end
    return LuaGuoQing2023Mgr.m_JingYuanPlay_GamePlayId
end

LuaGuoQing2023Mgr.m_JingYuanPlay_PlayTimeStr = nil
function LuaGuoQing2023Mgr:CheckJingYuanOpen()
    if self.m_JingYuanPlay_PlayTimeStr == nil then
        self.m_JingYuanPlay_PlayTimeStr = GuoQing2023_JingYuanPlay.GetData().PlayTimeString
    end
    local timeStr = self.m_JingYuanPlay_PlayTimeStr
    local startHour,startMin,endHour,endMin = string.match(timeStr,"(%d+):(%d+)-(%d+):(%d+)")
    local startTimeStamp = CServerTimeMgr.Inst:GetTodayTimeStampByTime(startHour,startMin,0)
    local endTimeStamp = CServerTimeMgr.Inst:GetTodayTimeStampByTime(endHour,endMin,0)
    local nowTime = CServerTimeMgr.Inst.timeStamp
    return timeStr, nowTime >= startTimeStamp and nowTime <= endTimeStamp
end

-- 结算
LuaGuoQing2023Mgr.m_JingYuanPlay_ResultInfo = nil
function LuaGuoQing2023Mgr:SyncJingYuanPlayResult(win, playTime, teamInfoU, playerInfoU, rewardInfoU)
    self.m_JingYuanPlay_ResultInfo = {}
    self.m_JingYuanPlay_ResultInfo.win = win            -- 是否胜利
    self.m_JingYuanPlay_ResultInfo.playTime = playTime      -- 比赛进行时长
    self.m_JingYuanPlay_ResultInfo.rewardInfo = g_MessagePack.unpack(rewardInfoU)
    -- rewardInfo.getEngageReward, rewardInfo.getPieceCount, rewardInfo.hasPieceCount, rewardInfo.maxPieceCount
    local teamJingYuanCount = g_MessagePack.unpack(teamInfoU)
    -- teamJingYuanCount[force] = count
    self.m_JingYuanPlay_ResultInfo.teamInfo = {}
    local playerInfo = g_MessagePack.unpack(playerInfoU)
    -- playerInfo[playerId] = info.playerId, info.name, info.force, info.talentLevel, info.jingyuanCount
    local playerInfo_force1 = {}
    local playerInfo_force2 = {}
    for _, info in ipairs(playerInfo) do
        local forcetable = info.force == 0 and playerInfo_force1 or playerInfo_force2
        table.insert(forcetable, info)
    end
    local sortFunc = function (a, b)
        return a.jingyuanCount > b.jingyuanCount
    end
    table.sort(playerInfo_force1, sortFunc)
    table.sort(playerInfo_force2, sortFunc)
    self.m_JingYuanPlay_ResultInfo.teamInfo[1] = {totalJingYuanCount = teamJingYuanCount[0] or 0, playerInfo = playerInfo_force1}
    self.m_JingYuanPlay_ResultInfo.teamInfo[2] = {totalJingYuanCount = teamJingYuanCount[1] or 0, playerInfo = playerInfo_force2}
    
    CUIManager.ShowUI(CLuaUIResources.GuoQing2023JingYuanResultWnd)
end

-- 晶元战队伍信息
function LuaGuoQing2023Mgr:OnJingYuanPlayUpdatePlayerForceInfo()
    local iconPath = {
        'UI/Texture/FestivalActivity/Festival_GuoQing/GuoQing2023/Material/guoqing2023jingyuanresultwnd_zhuyan_small.mat',
        'UI/Texture/FestivalActivity/Festival_GuoQing/GuoQing2023/Material/guoqing2023jingyuanresultwnd_cangming_small.mat',
    }
    local dict = {}
    if CForcesMgr.Inst.m_PlayForceInfo then
        CommonDefs.DictIterate(CForcesMgr.Inst.m_PlayForceInfo, DelegateFactory.Action_object_object(function (___key, ___value)
            dict[___key] = {texturePath = iconPath[___value + 1]}
        end)) 
    end
    LuaGamePlayMgr:SetPlayerHeadSpeIconInfos(dict, false, true)
end

LuaGuoQing2023Mgr.m_JingYuanPlay_GameInfo = nil
function LuaGuoQing2023Mgr:SyncArenaPlayerJingYuanInfo(teamJingYuanCount, playerJingYuanCount, addJingYuanPlayerId)
    self.m_JingYuanPlay_GameInfo = {}
    self.m_JingYuanPlay_GameInfo.teamInfo = g_MessagePack.unpack(teamJingYuanCount)
    g_ScriptEvent:BroadcastInLua("SyncArenaTeamJingYuanInfo")
    -- teamInfo[force] = jingyuan
    self.m_JingYuanPlay_GameInfo.playerInfo = g_MessagePack.unpack(playerJingYuanCount)
    -- playerInfo[engineId] = jingyuan
    for _, playerInfo in pairs(self.m_JingYuanPlay_GameInfo.playerInfo) do
        self:OnJingYuanHeadInfoUpdate(playerInfo[1], playerInfo[2], playerInfo[3], playerInfo[2] == addJingYuanPlayerId)
    end
end

-- 晶元头顶信息
function LuaGuoQing2023Mgr:OnJingYuanHeadInfoUpdate(playerId, engineId, jingyuanCount, isGain)
    if CForcesMgr.Inst.m_PlayForceInfo:TryGetValue(playerId) == false then return end

    local args = {}
    args[0] = engineId

    local textureForm = {}
    textureForm.texturePath = "UI/Texture/Transparent/Material/huluwazhanlingwnd_white.mat"
    textureForm.textureSize = {120, 120}
    textureForm.FxPath = isGain and "Fx/Vfx/Prefab/Dynamic/common_vfx_baodianzong.prefab" or nil
    textureForm.FxTime = 2000
    textureForm.color = Color.black
    textureForm.alpha = 0.8

    local LabelForm = {}
    LabelForm.FontType = "shufa"
    LabelForm.content = tostring(jingyuanCount)
    LabelForm.LabelSize = 42
    LabelForm.Color = CForcesMgr.Inst.m_PlayForceInfo[playerId] == 0 and NGUIText.ParseColor24("ff5e5e", 0) or NGUIText.ParseColor24("72cfff", 0)

    args[1] = textureForm
    args[2] = LabelForm
    args[3] = {}
    args[4] = jingyuanCount > 0
    args[5] = "GuoQing2023JingYuan"

    g_ScriptEvent:BroadcastInLua("UpdatePlayerHeadInfoCountDown",args)

    -- RegisterTickOnce(function ()
    --     args[1] = nil
    --     args[2] = LabelForm
    --     g_ScriptEvent:BroadcastInLua("UpdatePlayerHeadInfoCountDown",args)
    -- end, 100)
end

function LuaGuoQing2023Mgr:JingYuanPlayRemind()
    local msg = g_MessageMgr:FormatMessage("GuoQing2023_JingYuanPlay_Remind")
    MessageWndManager.ShowConfirmMessage(msg, 300, false, DelegateFactory.Action(function() 
        self:OpenGuoQing2023MainWnd()
    end), nil)
end
------------------------
--- 守护杭州
------------------------