

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CMainCamera = import "L10.Engine.CMainCamera"
local Vector3 = import "UnityEngine.Vector3"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"

LuaZongMenXuanMeiButtonWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZongMenXuanMeiButtonWnd, "XuanMei", "XuanMei", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaZongMenXuanMeiButtonWnd, "m_TopAnchor")
RegistClassMember(LuaZongMenXuanMeiButtonWnd, "m_CachedPos")
RegistClassMember(LuaZongMenXuanMeiButtonWnd, "m_ViewPos")
RegistClassMember(LuaZongMenXuanMeiButtonWnd, "m_ScreenPos")
RegistClassMember(LuaZongMenXuanMeiButtonWnd, "m_TopWorldPos")
RegistClassMember(LuaZongMenXuanMeiButtonWnd, "m_CurrentNpcId")

function LuaZongMenXuanMeiButtonWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.XuanMei.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnXuanMeiClick()
	end)

    --@endregion EventBind end
end

function LuaZongMenXuanMeiButtonWnd:Init()
	self.m_CurrentNpcId = LuaZongMenMgr.m_CurrentXuanMeiNpc
	if self.m_CurrentNpcId ~=nil then
		local clientObj = CClientObjectMgr.Inst:GetObject(self.m_CurrentNpcId)
		if clientObj and clientObj.RO then
			self.m_TopAnchor = clientObj.RO.transform.position
		end
	end
	self.m_CachedPos = self.transform.position
	self:Update()
end

function LuaZongMenXuanMeiButtonWnd:Update()
    -- npcid发生更新时
	if self.m_CurrentNpcId ~= LuaZongMenMgr.m_CurrentXuanMeiNpc then
		self.m_CurrentNpcId = LuaZongMenMgr.m_CurrentXuanMeiNpc
		if self.m_CurrentNpcId ~=nil then
			local clientObj = CClientObjectMgr.Inst:GetObject(self.m_CurrentNpcId)
			if clientObj and clientObj.RO then
				self.m_TopAnchor = clientObj.RO.transform.position
			end
		end
	end

    self.m_ViewPos = CMainCamera.Main:WorldToViewportPoint(self.m_TopAnchor)
    
    if self.m_ViewPos and self.m_ViewPos.z and self.m_ViewPos.z > 0 and 
            self.m_ViewPos.x > 0 and self.m_ViewPos.x < 1 and self.m_ViewPos.y > 0 and self.m_ViewPos.y < 1 then
        self.m_ScreenPos = CMainCamera.Main:WorldToScreenPoint(self.m_TopAnchor)
    else  
        self.m_ScreenPos = CommonDefs.op_Multiply_Vector3_Single(Vector3.up, 3000)
    end
    self.m_ScreenPos.z = 0
    self.m_TopWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_ScreenPos)

    if self.m_CachedPos.x ~= self.m_TopWorldPos.x or self.m_CachedPos.x ~= self.m_TopWorldPos.x then
        self.transform.position = self.m_TopWorldPos
        self.m_CachedPos = self.m_TopWorldPos
    end
end

--@region UIEvent

function LuaZongMenXuanMeiButtonWnd:OnXuanMeiClick()
	if self.m_CurrentNpcId then
		Gac2Gas.QuerySectVoteNpcVoteId(self.m_CurrentNpcId)
	end
end

--@endregion UIEvent

