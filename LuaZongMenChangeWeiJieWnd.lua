local UITabBar = import "L10.UI.UITabBar"

LuaZongMenChangeWeiJieWnd = class()

RegistChildComponent(LuaZongMenChangeWeiJieWnd,"m_Tabs","Tabs", UITabBar)
RegistChildComponent(LuaZongMenChangeWeiJieWnd,"m_DesLabel","DesLabel", UILabel)
RegistChildComponent(LuaZongMenChangeWeiJieWnd,"m_OkButton","OkButton", GameObject)
RegistChildComponent(LuaZongMenChangeWeiJieWnd,"m_NameLabel1","NameLabel1", UILabel)
RegistChildComponent(LuaZongMenChangeWeiJieWnd,"m_NameLabel2","NameLabel2", UILabel)

RegistClassMember(LuaZongMenChangeWeiJieWnd, "m_Office")
RegistClassMember(LuaZongMenChangeWeiJieWnd, "m_Name")
RegistClassMember(LuaZongMenChangeWeiJieWnd, "m_SelectIndex")
RegistClassMember(LuaZongMenChangeWeiJieWnd, "m_SelectRes")

function LuaZongMenChangeWeiJieWnd:Init()
    self.m_Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnTabChange(index)
    end)
    self.m_Tabs:ChangeTab(0, false)
    UIEventListener.Get(self.m_OkButton).onClick = DelegateFactory.VoidDelegate(function(go) 
        self:OnOkButtonClicked(go) 
    end)
    self.m_SelectRes = {}
    local array = {
        [EnumSectOffice.eZhangMen] = LocalString.GetString("门主"),
        [EnumSectOffice.eZhangLao] = LocalString.GetString("长老"),
        [EnumSectOffice.eDiZi] = LocalString.GetString("弟子")
    }
    self.m_NameLabel1.text = array[1]
    self.m_NameLabel2.text = array[2]
    local info = LuaZongMenMgr.m_TempMemberView_PlayerPopupMenu_MemberInfo
    self.m_Office = info.Office
    self.m_Name = info.Name
    self.m_DesLabel.text = SafeStringFormat3(LocalString.GetString("%s当前阶位为%s"),info.Name,array[info.Office]) 
    self.m_SelectRes = {
        info.Office ~= EnumSectOffice.eZhangMen and EnumSectOffice.eZhangMen or EnumSectOffice.eZhangLao,
        (info.Office ~= EnumSectOffice.eDiZi) and EnumSectOffice.eDiZi or EnumSectOffice.eZhangLao
    }
    self.m_NameLabel1.text = array[self.m_SelectRes[1]]
    self.m_NameLabel2.text = array[self.m_SelectRes[2]]
end

function LuaZongMenChangeWeiJieWnd:OnTabChange(index)
    self.m_SelectIndex = index + 1
end

function LuaZongMenChangeWeiJieWnd:OnOkButtonClicked(go)
    if not self.m_SelectRes then return end
    if CClientMainPlayer.Inst then
        if self.m_SelectRes[self.m_SelectIndex] == EnumSectOffice.eZhangMen then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ZongMen_ChangeZhangMen_Confirm",self.m_Name),DelegateFactory.Action(function()
                Gac2Gas.RequestSetSectOffice(CClientMainPlayer.Inst.BasicProp.SectId,LuaZongMenMgr.m_ChangeWeiJiePlayerId, self.m_SelectRes[self.m_SelectIndex])
                CUIManager.CloseUI(CLuaUIResources.ZongMenChangeWeiJieWnd)
            end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
            return
        end    
        Gac2Gas.RequestSetSectOffice(CClientMainPlayer.Inst.BasicProp.SectId,LuaZongMenMgr.m_ChangeWeiJiePlayerId, self.m_SelectRes[self.m_SelectIndex])
        CUIManager.CloseUI(CLuaUIResources.ZongMenChangeWeiJieWnd)
    end
    
end
