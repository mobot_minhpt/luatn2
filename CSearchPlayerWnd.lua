-- Auto Generated!!
local CSearchPlayerWnd = import "L10.UI.CSearchPlayerWnd"
local Gac2Gas = import "L10.Game.Gac2Gas"
CSearchPlayerWnd.m_OnClickSearchButton_CS2LuaHook = function (this, go)
    if not System.String.IsNullOrEmpty(this.input.value) then
        local id
        local default
        default, id = System.Double.TryParse(this.input.value)
        if default then
            --Gac2Gas.QueryLoveLetterPlayerInfo(id);
            Gac2Gas.QueryLoveLetterReceiverInfo(id)
        else
            --
        end
    end
end
CSearchPlayerWnd.m_OnQueryLoveLetterPlayerInfoResult_CS2LuaHook = function (this, playerId, name)
    this.PlayerId = playerId
    this.PlayerName = name
    --CQingShuMgr.Inst.selectedPlayerId = playerId;
    if playerId > 0 then
        this.result:SetActive(true)
        this.noResult:SetActive(false)
        this.nameLabel.text = name
    else
        this.noResult:SetActive(true)
        this.result:SetActive(false)
    end
end

CSearchPlayerWnd.m_hookInit = function (this)
  this.result:SetActive(false)
  this.noResult:SetActive(false)

  -- 明星邀请赛复用
  if CLuaStarBiwuMgr.m_bSelectFriend then
    this.transform:Find("Anchor/Input/Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("点击此处输入玩家ID")
    this.result.transform:Find("BgSprite/SelectBtn/Label"):GetComponent(typeof(UILabel)).text =  LocalString.GetString("邀请")
  end
end
