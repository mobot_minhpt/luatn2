local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"
local UILabel = import "UILabel"
local UIScrollView = import "UIScrollView"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local CBaseItemInfoRow = import "L10.UI.CBaseItemInfoRow"
local UIRect = import "UIRect"
local Screen = import "UnityEngine.Screen"
local UIRoot = import "UIRoot"
local CCommonLuaWnd = import "L10.UI.CCommonLuaWnd"
local Extensions = import "Extensions"
local CChatLinkMgr = import "CChatLinkMgr"

LuaBusinessItemInfoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaBusinessItemInfoWnd, "IconTexture", "IconTexture", CUITexture)
RegistChildComponent(LuaBusinessItemInfoWnd, "QualitySprite", "QualitySprite", UISprite)
RegistChildComponent(LuaBusinessItemInfoWnd, "ItemNameLabel", "ItemNameLabel", UILabel)
RegistChildComponent(LuaBusinessItemInfoWnd, "CategoryLabel", "CategoryLabel", UILabel)
RegistChildComponent(LuaBusinessItemInfoWnd, "ContentScrollView", "ContentScrollView", UIScrollView)
RegistChildComponent(LuaBusinessItemInfoWnd, "Table", "Table", UITable)
RegistChildComponent(LuaBusinessItemInfoWnd, "LabelTemplate", "LabelTemplate", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaBusinessItemInfoWnd, "NameColor")
RegistClassMember(LuaBusinessItemInfoWnd, "ValueColor")
RegistClassMember(LuaBusinessItemInfoWnd, "minContentWidth")
RegistClassMember(LuaBusinessItemInfoWnd, "maxContentWidth")
RegistClassMember(LuaBusinessItemInfoWnd, "m_Wnd")
RegistClassMember(LuaBusinessItemInfoWnd, "background")

function LuaBusinessItemInfoWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.NameColor = NGUIText.ParseColor24("FFED5F",0)
    self.ValueColor = NGUIText.ParseColor24("E8D0AA",0)
    self.minContentWidth = 512
    self.maxContentWidth = 600
    self.m_Wnd = self.gameObject:GetComponent(typeof(CCommonLuaWnd))
    self.background = self.transform:Find("Table/Background"):GetComponent(typeof(UISprite))
end

function LuaBusinessItemInfoWnd:Init()
    self:Init0(LuaBusinessMgr.m_BusinessCurGoods)
end

--@region UIEvent

--@endregion UIEvent

function LuaBusinessItemInfoWnd:Init0(goodsType)
    local data = Business_Goods.GetData(goodsType)

    self.IconTexture:LoadMaterial(data.Icon)
    self.ItemNameLabel.text = data.Name
    local cityData = Business_City.GetData(LuaBusinessMgr:GetCityOfSpecialGoods(goodsType))
    self.CategoryLabel.text = cityData and SafeStringFormat3(LocalString.GetString("%s特产"), cityData.CityName) or ""
    Extensions.RemoveAllChildren(self.Table.transform)
    self:AddItemToTable(CChatLinkMgr.TranslateToNGUIText(data.Description, false), self.ValueColor)

    self:LayoutWnd()
end

function LuaBusinessItemInfoWnd:AddItemToTable(text, color)
    local instance = NGUITools.AddChild(self.Table.gameObject, self.LabelTemplate)
    instance:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(UILabel)).text = text
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(UILabel)).color = color
end

function LuaBusinessItemInfoWnd:LayoutWnd()
    local maxWidth = 0
    do
        local i = 0
        while i < self.Table.transform.childCount do
            local row = CommonDefs.GetComponent_Component_Type(self.Table.transform:GetChild(i), typeof(CBaseItemInfoRow))
            if row ~= nil then
                local rowMaxWidth = row:GetMaxContentWidth()
                if maxWidth < rowMaxWidth then
                    maxWidth = rowMaxWidth
                end
            end
            i = i + 1
        end
    end

    local actualContentWidth = math.min(math.max(maxWidth, self.minContentWidth), self.maxContentWidth)

    do
        local i = 0
        while i < self.Table.transform.childCount do
            local row = CommonDefs.GetComponent_Component_Type(self.Table.transform:GetChild(i), typeof(CBaseItemInfoRow))
            if row ~= nil then
                row:SetContentWidth(actualContentWidth)
            end
            i = i + 1
        end
    end

    self.Table:Reposition()

    self.background.width = math.ceil(actualContentWidth + math.abs(self.ContentScrollView.panel.leftAnchor.absolute) + math.abs(self.ContentScrollView.panel.rightAnchor.absolute))

    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenWidth = Screen.width * CUIManager.UIMainCamera.rect.width * scale
    local virtualScreenHeight = Screen.height * scale

    local contentTopPadding = math.abs(self.ContentScrollView.panel.topAnchor.absolute)
    local contentBottomPadding = math.abs(self.ContentScrollView.panel.bottomAnchor.absolute)

    local totalWndHeight = self:TotalHeightOfScrollViewContent() + contentTopPadding + contentBottomPadding + self.ContentScrollView.panel.clipSoftness.y * 2
    local displayWndHeight = math.min(math.max(totalWndHeight, contentTopPadding + contentBottomPadding + 10), virtualScreenHeight)

    --设置背景高度
    self.background.height = math.ceil(displayWndHeight)

    local rects = CommonDefs.GetComponentsInChildren_Component_Type_Boolean(self.transform, typeof(UIRect), true)
    do
        local i = 0
        while i < rects.Length do
            if rects[i].updateAnchors == UIRect.AnchorUpdate.OnEnable or rects[i].updateAnchors == UIRect.AnchorUpdate.OnStart then
                rects[i]:ResetAndUpdateAnchors()
            end
            i = i + 1
        end
    end

    self.ContentScrollView:ResetPosition()
    self.background.transform.localPosition = Vector3.zero
    self.Table:Reposition()
end

function LuaBusinessItemInfoWnd:TotalHeightOfScrollViewContent( )
    return NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform).size.y + self.Table.padding.y * 2
end

function LuaBusinessItemInfoWnd:Update( )
    self.m_Wnd:ClickThroughToClose()
end