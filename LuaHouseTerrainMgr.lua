local Object = import "System.Object"
local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"
local CTerrainFast=import "L10.Engine.CTerrainFast"
local CRenderScene=import "L10.Engine.Scene.CRenderScene"
local CGrassFast=import "L10.Engine.CGrassFast"
local CameraFollow=import "L10.Engine.CameraControl.CameraFollow"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"

if not _G["LuaHouseTerrainMgr"] then
    LuaHouseTerrainMgr = {}
end

function LuaHouseTerrainMgr.IsEditTerrainTemplateId(id)
    return id == 9519
end

function LuaHouseTerrainMgr.RequestEditTerrain()
    if CClientHouseMgr.Inst:IsCurHouseRoomer() then
        if not CLuaHouseMgr.YardDecorationPrivalege then
            g_MessageMgr:ShowMessage("ROOMER_CANNOT_USE")
            return
        end
    end
    if not CClientHouseMgr.Inst:IsPlayerInYard() then
        g_MessageMgr:ShowMessage("Cant_EditGrass_In_Room")
        return
    end

    if CClientHouseMgr.IsWinterStyle() then
        g_MessageMgr:ShowMessage("Cannot_EditTerrain_InWinter")
        return
    end
    if CClientHouseMgr.Inst.mConstructProp and CClientHouseMgr.Inst.mConstructProp.ZhengwuGrade<4 then
        g_MessageMgr:ShowMessage("Edit_Dibiao_Need_Upgrade_Zhengwu")
        return
    end
    Gac2Gas.RequestEditYardGround()
end
function LuaHouseTerrainMgr.BeginEditTerrain()
    
    CClientFurnitureMgr.Inst.mbContinuedDown = false
    CClientFurnitureMgr.Inst.mbIsContinued = false
    CClientFurnitureMgr.Inst.FurnishMode = false
    CUIManager.CloseUI(CUIResources.FurnitureRightTopWnd)
    CUIManager.CloseUI(CUIResources.FurnitureSmallTypeWnd)
    CUIManager.CloseUI(CUIResources.FurnitureBigTypeWnd)
    CUIManager.CloseUI(CUIResources.UseZuoanWnd)
    CUIManager.CloseUI(CUIResources.FurnitureSlider)
    
    CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, CClientFurnitureMgr.Inst.m_Except, false, false)
    CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, false, false)

    CameraFollow.Inst.m_IsInRoomFurnish = false
    CClientFurnitureMgr.Inst:ClearCurFurniture()

    local isMingYuan = CClientFurnitureMgr.Inst.m_IsMingYuan
    local basex,basey,height,width = GetPoolBase(isMingYuan, false),GetPoolSize(isMingYuan)
    -- PoolGridHelper.Inst:DrawHelpMeshs(basex,basey,width,height)

    -- CClientObjectRoot.Inst:HideMainPlayer(true)

    CUIManager.ShowUI(CLuaUIResources.HouseTerrainEditWnd)
end

function LuaHouseTerrainMgr.GetGrassUsedCount()
    if not CClientHouseMgr.Inst.mFurnitureProp3 then return nil end

    local lookup = {}
    local grass = CClientHouseMgr.Inst.mFurnitureProp3.GrassContainer.Grass
    for i=1,grass.Length do
        local v = grass[i-1]
	    local grassType = bit.band(v, 0xf)
        if grassType>0 then
            if not lookup[grassType] then
                lookup[grassType] = 0
            end
            lookup[grassType] = lookup[grassType]+1
        end
    end
    return lookup
end

function LuaHouseTerrainMgr.DebugChunkList()
    CRenderScene.Inst.TerrainFast:InitCustomGroundChunkList()
    local list = CRenderScene.Inst.TerrainFast.CustomGroundChunkList
    print("chunk list:")
    for i=1,list.Count do
        print(list[i-1])
    end
end

function LuaHouseTerrainMgr.DebugGrassCount()
    local setting = House_GroundGrassSetting.GetData()
    local width = setting.MaxGGSize[0]
    local baseX,baseZ = 0,0
    if CClientFurnitureMgr.Inst.m_IsMingYuan then
        baseX,baseZ = setting.MingYuanGrassBase[0],setting.MingYuanGrassBase[1]
    else
        baseX,baseZ = setting.NormalGrassBase[0],setting.NormalGrassBase[1]
    end

    if not CClientHouseMgr.Inst.mFurnitureProp3 then return nil end

    local chunkWidth = CRenderScene.Inst.GrassFast.m_ChunkWidth
    print("chunkWidth",chunkWidth)

    local lookup = {}
    local chunkLookup = {}
    local grass = CClientHouseMgr.Inst.mCurFurnitureProp3.GrassContainer.Grass

    for i=1,grass.Length do
        local x = (i-1)%width+baseX
        local z = math.floor((i-1)/width)+baseZ
        local chunkX = math.floor(x/16)
        local chunkZ = math.floor(z/16)

        local chunkIndex = chunkX +chunkZ*chunkWidth
        
        local v = grass[i-1]
	    local grassType = bit.band(v, 0xf)
        if grassType>0 then
            if not lookup[grassType] then
                lookup[grassType] = 0
            end
            lookup[grassType] = lookup[grassType]+1

            if not chunkLookup[chunkIndex] then
                chunkLookup[chunkIndex] = {}
            end
            chunkLookup[chunkIndex][grassType] = (chunkLookup[chunkIndex][grassType] or 0)+1
        end
    end
    for k,v in pairs(chunkLookup) do
        print("chunkIndex",k)
        for k2,v2 in pairs(v) do
            print("type",k2,":",v2)
        end
    end
    -- return lookup
end

function LuaHouseTerrainMgr.GetGrassMaxNum(grassType)
    if grassType==0 then return 0 end
    if CClientHouseMgr.Inst.mFurnitureProp3 then
        return CClientHouseMgr.Inst.mFurnitureProp3.GrassContainer.MaxGridNum[grassType]+House_AllGrass.GetData(grassType).DefaultNum
    else
        return 0
    end
end
--每种草的上限
function LuaHouseTerrainMgr.GetGrassLimitNum(grassType)
    if grassType==0 then return 0 end
    return House_AllGrass.GetData(grassType).MaxGrids
end
function LuaHouseTerrainMgr.SyncHouseGrassInfo(houseId,grassInfoUD)
    -- print("SyncHouseGrassInfo")
    local setting = House_GroundGrassSetting.GetData()
    local width = setting.MaxGGSize[0]
    local baseX,baseZ = 0,0
    if CClientFurnitureMgr.Inst.m_IsMingYuan then
        baseX,baseZ = setting.MingYuanGrassBase[0],setting.MingYuanGrassBase[1]
    else
        baseX,baseZ = setting.NormalGrassBase[0],setting.NormalGrassBase[1]
    end
    

    local bytes = CClientHouseMgr.Inst.mCurFurnitureProp3.GrassContainer.Grass
	local list = MsgPackImpl.unpack(grassInfoUD)
    -- print(list.Count)
    local dirty = {}
    local CHUNK_RESOLUTION=CGrassFast.CHUNK_RESOLUTION
    local ChunkWidth=CRenderScene.Inst.GrassFast.m_ChunkWidth

	for i = 0, list.Count-1, 4 do
		local x, z, grassType,grassHeigh = list[i], list[i+1], list[i+2], list[i+3]

        local chunkX = math.floor(x*2/CHUNK_RESOLUTION)
        local chunkZ = math.floor(z*2/CHUNK_RESOLUTION)

        local chunkIndex = chunkX+chunkZ*ChunkWidth
        dirty[chunkIndex] = true

        x = x-baseX
        z = z-baseZ
        grassType = bit.band(grassType, 0xf)
        grassHeigh = bit.band(grassHeigh, 0xf)
        local v = bit.bor(bit.lshift(grassHeigh, 4), grassType)
        -- print(x,z,grassType,grassHeigh,v)
        local idx = z*width+x
        bytes[idx] = v
        -- print(idx,v)
	end

    CClientHouseMgr.Inst:ClearGrassEditData()

    local list = CreateFromClass(MakeGenericClass(List, Int32))
    for k,v in pairs(dirty) do
        CommonDefs.ListAdd(list, typeof(Int32), k)
    end
    CClientHouseMgr.Inst:RefreshGrass(list)
    --同步自身
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.HouseId == houseId then
        CClientHouseMgr.Inst.mFurnitureProp3.GrassContainer.Grass = bytes
    end

	g_ScriptEvent:BroadcastInLua("SyncHouseGrassInfo")
end

function LuaHouseTerrainMgr.SyncHouseGroundInfo(houseId,groundChgUD)
    -- print("SyncHouseGroundInfo")
    local setting = House_GroundGrassSetting.GetData()
    local width = setting.MaxGGSize[0]
    local baseX,baseZ = 0,0
    if CClientFurnitureMgr.Inst.m_IsMingYuan then
        baseX,baseZ = setting.MingYuanGrassBase[0],setting.MingYuanGrassBase[1]
    else
        baseX,baseZ = setting.NormalGrassBase[0],setting.NormalGrassBase[1]
    end
    

    local bitset = CClientHouseMgr.Inst.mCurFurnitureProp3.GroundContainer.CustomGround
	local list = MsgPackImpl.unpack(groundChgUD)
    local dirty = {}
    local CHUNK_RESOLUTION=CTerrainFast.CHUNK_RESOLUTION
    local ChunkWidth=CRenderScene.Inst.TerrainFast.m_ChunkWidth


    local function setAlpha(alpha,idx)
        local b1 = bit.band(alpha,1)
        local b2 = bit.band(alpha,2)
        local b3 = bit.band(alpha,4)
        local b4 = bit.band(alpha,8)
        -- print(b1,b2,b3,b4)
        bitset:SetBit(idx,b1>0 and true or false)
        bitset:SetBit(idx+1,b2>0 and true or false)
        bitset:SetBit(idx+2,b3>0 and true or false)
        bitset:SetBit(idx+3,b4>0 and true or false)
        -- print(idx,bitset:GetBit(idx),bitset:GetBit(idx+1),bitset:GetBit(idx+2),bitset:GetBit(idx+3))
    end

	for i = 0, list.Count-1, 5 do
		local x, z, alpha1,alpha2,alpha3 = list[i], list[i+1], list[i+2], list[i+3], list[i+4]
        -- print("sync",x, z, alpha1,alpha2,alpha3)

        local chunkX = math.floor(x*2/CHUNK_RESOLUTION)
        local chunkZ = math.floor(z*2/CHUNK_RESOLUTION)

        local chunkIndex = chunkX+chunkZ*ChunkWidth
        dirty[chunkIndex] = true

        x = x-baseX
        z = z-baseZ

        local v = 0
        local idx = (z*width+x)*12
        -- print(x,z,idx)

        setAlpha(alpha1,idx)
        setAlpha(alpha2,idx+4)
        setAlpha(alpha3,idx+8)
	end

    CClientHouseMgr.Inst:ClearGroundEditData()

    local list = CreateFromClass(MakeGenericClass(List, Int32))
    for k,v in pairs(dirty) do
        CommonDefs.ListAdd(list, typeof(Int32), k)
    end
    CClientHouseMgr.Inst:UpdateGround(list)

    --同步自身
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.HouseId == houseId then
        CClientHouseMgr.Inst.mFurnitureProp3.GroundContainer.CustomGround = bitset
    end

	g_ScriptEvent:BroadcastInLua("SyncHouseGroundInfo")
end


function LuaHouseTerrainMgr:SaveGround()
    local setting = House_GroundGrassSetting.GetData()
    local width = setting.MaxGGSize[0]
    local height = setting.MaxGGSize[1]

    local baseX,baseZ = 0,0
    if CClientFurnitureMgr.Inst.m_IsMingYuan then
        baseX,baseZ = setting.MingYuanGrassBase[0],setting.MingYuanGrassBase[1]
    else
        baseX,baseZ = setting.NormalGrassBase[0],setting.NormalGrassBase[1]
    end
    local raw = {}
    local dict = CClientHouseMgr.Inst:GetGroundDirtyData()
    CommonDefs.DictIterate(dict,DelegateFactory.Action_object_object(function (___key, ___value)
        table.insert( raw, ___key )
        table.insert( raw, ___value )
    end))
    -- print(#raw)
	local maxCount = 40--一个小u存40条数据，1条rpc也就160条数据
    local listTbl = {}
    local list = CreateFromClass(MakeGenericClass(List, Object))
    for i=1,#raw,maxCount do
		list:Clear()
        for j=1,maxCount,2 do
            local idx1 = (i-1)+j
            local idx2 = idx1+1
            local k = raw[idx1]
            local v = raw[idx2]
            -- print(k,v)
            if k and v then
                local z = math.floor(k/width)
                local x = k%width
                z = z+baseZ
                x = x+baseX

                local b1 = bit.band(v,255)/17
                local b2 = bit.band(bit.rshift(v,8),255)/17
                local b3 = bit.band(bit.rshift(v,16),255)/17
                b1,b2,b3=math.floor(b1),math.floor(b2),math.floor(b3)
                -- print(x,z,b1,b2,b3)

                CommonDefs.ListAdd(list, typeof(UInt32), math.floor(x))
                CommonDefs.ListAdd(list, typeof(UInt32), math.floor(z))
                CommonDefs.ListAdd(list, typeof(UInt32), math.floor(b1))
                CommonDefs.ListAdd(list, typeof(UInt32), math.floor(b2))
                CommonDefs.ListAdd(list, typeof(UInt32), math.floor(b3))
                -- print("request",x,z,v)
            end
        end
        table.insert(listTbl,MsgPackImpl.pack(list))
    end


    local rpcCount = math.ceil(#listTbl/4)
    -- print("rpcCount",rpcCount)
    local empty = CreateFromClass(MakeGenericClass(List, Object))
    local u = MsgPackImpl.pack(empty)
    for i=0,rpcCount-1 do
        RegisterTickOnce(function()
            local arg1 = listTbl[i*4+1] and listTbl[i*4+1] or u
            local arg2 = listTbl[i*4+2] and listTbl[i*4+2] or u
            local arg3 = listTbl[i*4+3] and listTbl[i*4+3] or u
            local arg4 = listTbl[i*4+4] and listTbl[i*4+4] or u
            Gac2Gas.RequestSetGroundType(arg1,arg2,arg3,arg4)
        end,100*i+10)
    end
    return rpcCount
end