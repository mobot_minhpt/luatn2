require("common/common_include")

local LuaUtils = import "LuaUtils"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local Constants = import "L10.Game.Constants"
local UISprite = import "UISprite"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local AlignType3 = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local QnButton = import "L10.UI.QnButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

CLuaCityWarWatchWnd = class()
CLuaCityWarWatchWnd.Path = "ui/citywar/LuaCityWarWatchWnd"
RegistClassMember(CLuaCityWarWatchWnd, "m_GridView")
RegistClassMember(CLuaCityWarWatchWnd, "m_DataTable")
RegistClassMember(CLuaCityWarWatchWnd, "m_WatchButton")
RegistClassMember(CLuaCityWarWatchWnd, "m_SelectIndex")
RegistClassMember(CLuaCityWarWatchWnd, "m_SelectSprite")

function CLuaCityWarWatchWnd:Init( ... )
	--Gac2Gas.QueryMirrorWarWatchInfo()

	self.m_GridView = self.transform:Find("Anchor/ListView"):GetComponent(typeof(QnAdvanceGridView))
    self.m_GridView.m_DataSource = DefaultTableViewDataSource.Create(function()
			return #self.m_DataTable
		end, function(item, index)
			self:InitItem(item.gameObject, index + 1)
		end)
    self.m_GridView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
    end)

    UIEventListener.Get(self.transform:Find("Anchor/ButtonList/RefreshBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
			Gac2Gas.QueryMirrorWarWatchInfo()
    end)

    self.m_WatchButton = self.transform:Find("Anchor/ButtonList/WatchBtn"):GetComponent(typeof(QnButton))
    self.m_WatchButton.Enabled = false
    UIEventListener.Get(self.m_WatchButton.gameObject).onClick = LuaUtils.VoidDelegate(function ( go )
    	local popupMenuItemTable = {}
		local indexNameTbl = {LocalString.GetString("一"), LocalString.GetString("二"), LocalString.GetString("三")}
		local playNum = math.min(self.m_DataTable[self.m_SelectIndex].PlayNum, 3)
		for i = 1, playNum do
			table.insert(popupMenuItemTable, PopupMenuItemData(LocalString.GetString("战场")..indexNameTbl[i], DelegateFactory.Action_int(function (index)
				Gac2Gas.RequestWatchMirrorWar(self.m_DataTable[self.m_SelectIndex].PlayId[i])
				end), false, nil, EnumPopupMenuItemStyle.Default))
		end
		local popupMenuItemArray = Table2Array(popupMenuItemTable, MakeArrayClass(PopupMenuItemData))
		CPopupMenuInfoMgr.ShowPopupMenu(popupMenuItemArray, go.transform, AlignType3.Top, 1, nil, 600, true, 227)
    end)

    if CLuaCityWarMgr.WatchInfo then
    	self:QueryMirrorWarWatchInfoResult(CLuaCityWarMgr.WatchInfo)
    end
end

function CLuaCityWarWatchWnd:InitItem(obj, index)
	local transTbl = {"ServerName1", "ServerName2", "GuildName1", "GuildName2", "RegionName1", "RegionName2", "Status"}
	local data = self.m_DataTable[index]
	local map1 = CityWar_Map.GetData(tonumber(data.MapId1))
	local map2 = CityWar_Map.GetData(tonumber(data.MapId2))
	local regionName1 = SafeStringFormat3(LocalString.GetString("%s·%d级"), map1.Name, map1.Level)
	local regionName2 = SafeStringFormat3(LocalString.GetString("%s·%d级"), map2.Name, map2.Level)
	local bPrepare = data.CreateTime + CLuaCityWarMgr.MirrorWarFightPrepareDuration > CServerTimeMgr.Inst.timeStamp
  local valueTbl = {data.ServerName1, data.ServerName2, data.GuildName1, data.GuildName2, regionName1, regionName2, bPrepare}
  for i = 1, #transTbl do
		local label = obj.transform:Find(transTbl[i]):GetComponent(typeof(UILabel))
		label.text = valueTbl[i]
		if i == 7 then
			label.color = bPrepare and Color.yellow or Color.red
			label.text = bPrepare and LocalString.GetString("准备中") or LocalString.GetString("开战中")
		end
  end
  local bgSprite = obj.transform:Find("Bg"):GetComponent(typeof(UISprite))
  bgSprite.spriteName = index % 2 == 0 and Constants.GreyItemBgSpriteName or Constants.WhiteItemBgSpriteName
  UIEventListener.Get(bgSprite.gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
  	if self.m_SelectSprite then self.m_SelectSprite.spriteName = self.m_SelectIndex % 2 == 0 and Constants.GreyItemBgSpriteName or Constants.WhiteItemBgSpriteName end
  	self.m_SelectIndex = index
  	self.m_SelectSprite = bgSprite
  	bgSprite.spriteName = Constants.ChosenItemBgSpriteName
  	self.m_WatchButton.Enabled = true
  end)
end

function CLuaCityWarWatchWnd:QueryMirrorWarWatchInfoResult(dataUD)
	self.m_DataTable = {}
	local list = MsgPackImpl.unpack(dataUD)
	if not list then return end
	for i = 0, list.Count - 1, 14 do
		local warId = list[i]
		local createTime = list[i + 1]
		local playId1 = list[i + 2]
		local playId2 = list[i + 3]
		local playId3 = list[i + 4]
		local serverName1 = list[i + 5]
		local guildName1 = list[i + 6]
		local mapId1 = list[i + 7]
		local templateId1 = list[i + 8]
		local serverName2 = list[i + 9]
		local guildName2 = list[i + 10]
		local mapId2 = list[i + 11]
		local templateId2 = list[i + 12]
		local playNum = list[i + 13]
		table.insert(self.m_DataTable, {WarId = warId, PlayId = {playId1, playId2, playId3}, ServerName1 = serverName1, ServerName2 = serverName2, GuildName1 = guildName1, GuildName2 = guildName2, MapId1 = mapId1, MapId2 = mapId2, PlayNum = playNum, CreateTime = createTime})
	end
	self.m_GridView:ReloadData(true, false)
	self.m_WatchButton.Enabled = false
end

function CLuaCityWarWatchWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("QueryMirrorWarWatchInfoResult", self, "QueryMirrorWarWatchInfoResult")
end

function CLuaCityWarWatchWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("QueryMirrorWarWatchInfoResult", self, "QueryMirrorWarWatchInfoResult")
end
