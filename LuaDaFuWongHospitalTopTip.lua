local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CMainCamera = import "L10.Engine.CMainCamera"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CPos = import "L10.Engine.CPos"
local CUITexture = import "L10.UI.CUITexture"
local CPos = import "L10.Engine.CPos"
local CSetUIScreenPosition = import "L10.UI.CSetUIScreenPosition"
LuaDaFuWongHospitalTopTip = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaFuWongHospitalTopTip, "Table", "Table", GameObject)
RegistChildComponent(LuaDaFuWongHospitalTopTip, "Template", "Template", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongHospitalTopTip, "m_touxiangBgList")
RegistClassMember(LuaDaFuWongHospitalTopTip, "m_Pos")
RegistClassMember(LuaDaFuWongHospitalTopTip, "m_LandId")
RegistClassMember(LuaDaFuWongHospitalTopTip, "m_Height")
RegistClassMember(LuaDaFuWongHospitalTopTip, "m_ViewList")
function LuaDaFuWongHospitalTopTip:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_LandId = 0
    self.m_Pos = nil
    self.m_Height = DaFuWeng_Setting.GetData().LandUIHeight
    self.m_ViewList = nil
    self.m_touxiangBgList = {"dafuwongplaylandeopwnd_huang","dafuwongplaylandeopwnd_hong","dafuwongplaylandeopwnd_zi","dafuwongplaylandeopwnd_lv"}
end
-- function LuaDaFuWongHospitalTopTip:Update()
--     if not self.m_Pos or not CMainCamera.Main  then return end
--     local viewPos = CMainCamera.Main:WorldToViewportPoint(self.m_Pos)
-- 	local isInView = viewPos.z > 0 and viewPos.x > 0 and viewPos.x < 1 and viewPos.y > 0 and viewPos.y < 1
-- 	local screenPos = isInView and CMainCamera.Main:WorldToScreenPoint(self.m_Pos) or Vector3(0,3000,0)
-- 	screenPos.z = 0
-- 	local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
-- 	self.transform.position = worldPos
-- end
function LuaDaFuWongHospitalTopTip:Init(data)
    Extensions.RemoveAllChildren(self.Table.transform)
    self.m_LandId = data.ID
    local pos = Utility.PixelPos2WorldPos(data.Pos[0], data.Pos[1])
    self.m_Pos = Vector3(pos.x, pos.y + self.m_Height, pos.z)
    self.gameObject:GetComponent(typeof(CSetUIScreenPosition)).Pos = self.m_Pos
    self.m_ViewList = {}
end

function LuaDaFuWongHospitalTopTip:UpdateData(hospitalData)
    local data = hospitalData
    table.sort(data,function(a,b) return a[2] < b[2] end)
    if #self.m_ViewList < #data then
        for i = 1,#data do
            if self.m_ViewList[i] then
                self:InitCell(data[i],i)
            else
                local go = CUICommonDef.AddChild(self.Table.gameObject,self.Template)
                local bg = go.transform:Find("Wnd_Bg/bg_touxiang"):GetComponent(typeof(CUITexture))
                local Icon = go.transform:Find("Content/Icon"):GetComponent(typeof(CUITexture))
                local MyIcon = go.transform:Find("Content/MyIcon").gameObject
                local buffLabel = go.transform:Find("Content/Buff/NumLabel"):GetComponent(typeof(UILabel))
                local buffIcon = go.transform:Find("Content/Buff/Icon"):GetComponent(typeof(UISprite))
                go.gameObject:SetActive(true)
                self.m_ViewList[i] = {go = go,bg = bg,Icon = Icon,MyIcon = MyIcon,buffLabel = buffLabel,buffIcon = buffIcon,id = 0}
                self:InitCell(data[i],i)
            end 
        end
    else
        for i = 1,#self.m_ViewList do
            if data[i] then
                self:InitCell(data[i],i)
            else
                self.m_ViewList[i].go.gameObject:SetActive(false)
                self.m_ViewList[i].id = 0
            end
        end
    end
    self.Table:GetComponent(typeof(UITable)):Reposition()
end

function LuaDaFuWongHospitalTopTip:InitCell(data,index)
    self.m_ViewList[index].go.gameObject:SetActive(true)
    if data[2] <= 0 then 
        self.m_ViewList[index].go.gameObject:SetActive(false) 
        self.m_ViewList[index].id = 0
        return
    end
    
    if self.m_ViewList[index].id ~= data[1] then
        self.m_ViewList[index].id = data[1]
        local buffData = DaFuWeng_Buff.GetData(data[3])
        self.m_ViewList[index].buffIcon.spriteName = buffData.Icon
        self.m_ViewList[index].buffLabel.text = data[2]

        local id = data[1]
        self.m_ViewList[index].MyIcon.gameObject:SetActive(CClientMainPlayer.Inst and id == CClientMainPlayer.Inst.Id)
        local playerData = LuaDaFuWongMgr.PlayerInfo[id]
        self.m_ViewList[index].Icon:LoadNPCPortrait(CUICommonDef.GetPortraitName(playerData.class, playerData.gender, -1), false)
        self.m_ViewList[index].bg:LoadMaterial(self:GetTexturePath(playerData.round))
    else
        self.m_ViewList[index].buffLabel.text = data[2]
    end
end
function LuaDaFuWongHospitalTopTip:GetTexturePath(index)
    local name = self.m_touxiangBgList[index]
    local path = SafeStringFormat3("UI/Texture/FestivalActivity/Festival_DaFuWong/Material/%s.mat",name)
    return path
end
--@region UIEvent

--@endregion UIEvent

