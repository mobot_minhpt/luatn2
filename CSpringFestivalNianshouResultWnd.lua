-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CSpringFestivalMgr = import "L10.Game.CSpringFestivalMgr"
local CSpringFestivalNianshouResultWnd = import "L10.UI.CSpringFestivalNianshouResultWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local Object = import "UnityEngine.Object"
local Time = import "UnityEngine.Time"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
CSpringFestivalNianshouResultWnd.m_Init_CS2LuaHook = function (this) 

    this.playerInfoNode:SetActive(false)
    this.iconNode:SetActive(false)

    this.fxNode:LoadFx(CUIFxPaths.NianshouGameResultFx)
    --fxNode.LoadFx(CUIFxPaths.SnowballResultFx);
    --public bool nianshouGameResult_isWin;
    --public bool nianshouGameResult_isNianShou;
    --public uint nianshouGameResult_score; 
    --public uint nianshouGameResult_killCount;
    --public uint nianshouGameResult_nianHuoCount;
    this.rankLabel.text = tostring(CSpringFestivalMgr.Instance.nianshouGameResult_score)
    if CSpringFestivalMgr.Instance.nianshouGameResult_isWin then
        this.winNode:SetActive(true)
        this.loseNode:SetActive(false)
    else
        this.winNode:SetActive(false)
        this.loseNode:SetActive(true)
    end

    if not CSpringFestivalMgr.Instance.nianshouGameResult_isNianShou then
        this.posLabel.text = LocalString.GetString("勇士")
        this.killLabel.text = LocalString.GetString("击杀年兽:")
        this.getLabel.text = LocalString.GetString("拾取年货:")
    else
        this.posLabel.text = LocalString.GetString("年兽")
        this.killLabel.text = LocalString.GetString("击杀勇士:")
        this.getLabel.text = LocalString.GetString("剩余年货:")
    end

    this.killNumLabel.text = tostring(CSpringFestivalMgr.Instance.nianshouGameResult_killCount)
    this.getNumLabel.text = tostring(CSpringFestivalMgr.Instance.nianshouGameResult_nianHuoCount)

    UIEventListener.Get(this.backBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)

    UIEventListener.Get(this.checkBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        Gac2Gas.RequestNSDZZBattleInfo()
    end)

    UIEventListener.Get(this.shareBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        --OpenSharePanel();
        this:OnLocalSaveButtonClick()
    end)

    this.playerInfoNode:SetActive(true)
    this.iconNode:SetActive(true)

    CommonDefs.GetComponent_Component_Type(this.playerInfoNode.transform:Find("name"), typeof(UILabel)).text = CClientMainPlayer.Inst.Name
    local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
    CommonDefs.GetComponent_Component_Type(this.playerInfoNode.transform:Find("server"), typeof(UILabel)).text = myGameServer.name
    CommonDefs.GetComponent_Component_Type(this.playerInfoNode.transform:Find("ID"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Id)
    CommonDefs.GetComponent_Component_Type(this.playerInfoNode.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), false)
    CommonDefs.GetComponent_Component_Type(this.playerInfoNode.transform:Find("icon/lv"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Level)
end
CSpringFestivalNianshouResultWnd.m_OpenSharePanel_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return
    end

    this.backBtn:SetActive(false)
    this.shareBtn:SetActive(false)
    this.checkBtn:SetActive(false)
end
CSpringFestivalNianshouResultWnd.m_OnLocalSaveButtonClick_CS2LuaHook = function (this) 

    if Time.realtimeSinceStartup - this.m_LastEvaluateTime < this.m_EvaluateInterval then
        --MessageMgr.Inst.ShowMessage("RPC_TOO_FREQUENT");
        return
    end
    this.m_LastEvaluateTime = Time.realtimeSinceStartup

    --EventManager.Broadcast(EnumEventType.OnTiezhiItemEditEnable, 0);

    this:StartCoroutine(this:delayCaptureScreen())
end
CSpringFestivalNianshouResultWnd.m_ClearTexture_CS2LuaHook = function (this) 
    if this.m_CachedTexture ~= nil then
        Object.Destroy(this.m_CachedTexture)
    end
end
