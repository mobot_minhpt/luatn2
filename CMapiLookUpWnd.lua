-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CMapiLookUpWnd = import "L10.UI.CMapiLookUpWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CUIDirectories = import "L10.UI.CUIDirectories"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local Double = import "System.Double"
local EnumEventType = import "EnumEventType"
local EnumMapiGender = import "L10.Game.EnumMapiGender"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local ZuoQi_MapiSkill = import "L10.Game.ZuoQi_MapiSkill"
local ZuoQi_MapiYumashiUpgrade = import "L10.Game.ZuoQi_MapiYumashiUpgrade"
local ZuoQi_Setting = import "L10.Game.ZuoQi_Setting"
CMapiLookUpWnd.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.QueryMapiInfoResult, MakeDelegateFromCSFunction(this.ShowMapiInfo, MakeGenericClass(Action1, UInt32), this))
    UIEventListener.Get(this.m_CloseButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.m_LeftArrowObj).onClick = MakeDelegateFromCSFunction(this.OnLeftArrowClick, VoidDelegate, this)
    UIEventListener.Get(this.m_RightArrowObj).onClick = MakeDelegateFromCSFunction(this.OnRightArrowClick, VoidDelegate, this)
    UIEventListener.Get(this.m_GenderSprite.gameObject).onClick = MakeDelegateFromCSFunction(this.OnGenderSpriteClick, VoidDelegate, this)
end
CMapiLookUpWnd.m_OnGenderSpriteClick_CS2LuaHook = function (this, go) 
    if CZuoQiMgr.Inst.m_CurrentMapi ~= nil and CZuoQiMgr.Inst.m_CurrentMapi.MapiInfo ~= nil then
        CZuoQiMgr.Lua_MapiFanyuWndGender = CommonDefs.ConvertIntToEnum(typeof(EnumMapiGender), CZuoQiMgr.Inst.m_CurrentMapi.MapiInfo.Gender)
        CZuoQiMgr.Lua_MapiFanyuWndFanyued = CZuoQiMgr.Inst.m_CurrentMapi.MapiInfo.FanyuTimes > 0
        CUIManager.ShowUI(CUIResources.MapiFanyuInfoWnd)
    end
end
CMapiLookUpWnd.m_Init_CS2LuaHook = function (this) 
    if CZuoQiMgr.Inst.m_OtherMapiOverviewList.Count <= 0 then
        this:Close()
        return
    end

    local ownerLabel = this.transform:Find("ShowArea/OwnerName"):GetComponent(typeof(UILabel))
    ownerLabel.text = CZuoQiMgr.Inst.m_OtherPlayerName

    this.m_CurrentMapiIndex = 0
    CZuoQiMgr.Inst.m_CurrentMapiId = CZuoQiMgr.Inst.m_OtherMapiOverviewList[this.m_CurrentMapiIndex].m_Id
    Gac2Gas.QueryMapiInfo(CZuoQiMgr.Inst.m_OtherPlayerId, CZuoQiMgr.Inst.m_OtherMapiOverviewList[this.m_CurrentMapiIndex].m_Id)
    this.m_LeftArrowObj:SetActive(false)
    this.m_RightArrowObj:SetActive(CZuoQiMgr.Inst.m_OtherMapiOverviewList.Count > 1)
end
CMapiLookUpWnd.m_OnLeftArrowClick_CS2LuaHook = function (this, go) 
    if this.m_CurrentMapiIndex > 0 then
        CZuoQiMgr.Inst.m_CurrentMapiId = CZuoQiMgr.Inst.m_OtherMapiOverviewList[this.m_CurrentMapiIndex - 1].m_Id
        Gac2Gas.QueryMapiInfo(CZuoQiMgr.Inst.m_OtherPlayerId, CZuoQiMgr.Inst.m_CurrentMapiId)
    end
end
CMapiLookUpWnd.m_OnRightArrowClick_CS2LuaHook = function (this, go) 
    if this.m_CurrentMapiIndex < CZuoQiMgr.Inst.m_OtherMapiOverviewList.Count - 1 then
        CZuoQiMgr.Inst.m_CurrentMapiId = CZuoQiMgr.Inst.m_OtherMapiOverviewList[this.m_CurrentMapiIndex + 1].m_Id
        Gac2Gas.QueryMapiInfo(CZuoQiMgr.Inst.m_OtherPlayerId, CZuoQiMgr.Inst.m_CurrentMapiId)
    end
end
CMapiLookUpWnd.m_UpdateArrowStatus_CS2LuaHook = function (this) 
    do
        local i = 0 local cnt = CZuoQiMgr.Inst.m_OtherMapiOverviewList.Count
        while i < cnt do
            if CZuoQiMgr.Inst.m_OtherMapiOverviewList[i].m_Id == CZuoQiMgr.Inst.m_CurrentMapiId then
                this.m_CurrentMapiIndex = i
                break
            end
            i = i + 1
        end
    end
    this.m_LeftArrowObj:SetActive(this.m_CurrentMapiIndex > 0)
    this.m_RightArrowObj:SetActive(this.m_CurrentMapiIndex < CZuoQiMgr.Inst.m_OtherMapiOverviewList.Count - 1)
end
CMapiLookUpWnd.m_UpdatePlayerInfo_CS2LuaHook = function (this) 
    if nil == CZuoQiMgr.Inst.m_OtherYumaPlayInfo then
        return
    end
    this.m_MasterLevelLabel.text = System.String.Format("Lv.{0}", CZuoQiMgr.Inst.m_OtherYumaPlayInfo.Level)
    local upgradeData = ZuoQi_MapiYumashiUpgrade.GetData(CZuoQiMgr.Inst.m_OtherYumaPlayInfo.Level)
    if upgradeData ~= nil then
        this.m_MasterIconTex:LoadMaterial(upgradeData.Icon)
        this.m_MasterTitleLabel.text = upgradeData.Title
    end
end
CMapiLookUpWnd.m_ShowMapiInfo_CS2LuaHook = function (this, pinfen) 
    if nil == CZuoQiMgr.Inst.m_CurrentMapi then
        return
    end
    this.m_ScoreLabel.text = tostring(pinfen)

    this:UpdateArrowStatus()
    this:UpdatePlayerInfo()
    local mapiInfo = CZuoQiMgr.Inst.m_CurrentMapi.MapiInfo
    this.m_NameLabel.text = mapiInfo.Name
    this.m_GenderSprite.spriteName = (mapiInfo.Gender == EnumMapiGender_lua.eMale) and Constants.LingShou_Yang or Constants.LingShou_Yin
    this.m_LevelLabel.text = System.String.Format("Lv.{0}", mapiInfo.Level)
    local newChicun = mapiInfo.Chicun * ZuoQi_Setting.GetData().MapiHight
    this.m_HeightLabel.text = NumberComplexToString(newChicun, typeof(Double), "#0.00")
    this.m_IntimacyLabel.text = tostring((math.floor((mapiInfo.Qinmidu))))

    local appearanceName = CZuoQiMgr.GetAppearanceName(mapiInfo.PinzhongId, mapiInfo.Quality)
    local attrName = CZuoQiMgr.GetAttributeName(mapiInfo.AttributeNameId)
    this.m_TypeLabel.text = System.String.Format(LocalString.GetString("{0}·{1}"), appearanceName, attrName)
    this.m_TypeLabel.color = CZuoQiMgr.GetMapiColor(mapiInfo.Quality)
    local pzLabel = CommonDefs.GetComponent_Component_Type(this.m_TypeLabel.transform:Find("ShengaoLabel"), typeof(UILabel))
    pzLabel.color = CZuoQiMgr.GetMapiColor(mapiInfo.Quality)

    this.m_SpeedLabel.text = tostring(mapiInfo.Speed)
    this.m_DelicacyLabel.text = tostring(mapiInfo.Lingqiao)
    this.m_EnduranceLabel.text = tostring(mapiInfo.Naili)
    this.m_MeadowFitnessLabel.text = tostring(mapiInfo.CaodiShiying)
    this.m_MudFitnessLabel.text = tostring(mapiInfo.NidiShiying)
    this.m_RockFitnessLabel.text = tostring(mapiInfo.YandiShiying)
    this.generationLabel.text = System.String.Format(LocalString.GetString("{0}代"), mapiInfo.Generation)

    do
        local i = 0
        while i < this.m_SkillList.Length do
            local item = this.m_SkillList[i]
            local skillIcon = nil
            local showAdd = true
            local textureAction = nil
            if i + 1 < mapiInfo.Skills.Length then
                local skillId = mapiInfo.Skills[i + 1]
                local skill = ZuoQi_MapiSkill.GetData(skillId)
                if skill ~= nil then
                    skillIcon = (CUIDirectories.SkillIconsMaterialDir .. skill.Icon) .. ".mat"
                    showAdd = false

                    textureAction = DelegateFactory.Action(function () 
                        CSkillInfoMgr.ShowSkillInfoWnd(skillId, item.transform.position, CSkillInfoMgr.EnumSkillInfoContext.MapiSkill, true, 0, 0, nil)
                    end)
                end
            end
            item:Init(skillIcon, "", 0, showAdd, DelegateFactory.Action(function () 
                g_MessageMgr:ShowMessage("Mapi_Learn_Skill_Hint")
            end), nil, textureAction, nil)
            i = i + 1
        end
    end

    do
        local i = 0
        while i < this.m_EquipList.Length do
            local equipIcon = nil
            local showAdd = true
            local lockAction = nil
            local addAction = nil
            local textureAction = nil
            if i < CZuoQiMgr.Inst.m_CurrentMapiEquipList.Count then
                local equipId = CZuoQiMgr.Inst.m_CurrentMapiEquipList[i]
                if not System.String.IsNullOrEmpty(equipId) then
                    local itemData = Item_Item.GetData(ZuoQi_Setting.GetData().MapiLingfuItemId[i])
                    if itemData ~= nil then
                        equipIcon = itemData.Icon
                        showAdd = false
                        textureAction = DelegateFactory.Action(function () 
                            CItemInfoMgr.ShowLinkItemInfo(equipId, true, nil, AlignType.Default, 0, 0, 0, 0)
                        end)
                     end
                end
            end

            local qualitySpriteName = nil
            if i < CZuoQiMgr.Inst.m_CurrentMapiQualityList.Count then
                qualitySpriteName = CZuoQiMgr.GetLingFuItemCellBorder(CZuoQiMgr.Inst.m_CurrentMapiQualityList[i])
            end

            this.m_EquipList[i]:Init(equipIcon, "", 0, showAdd, lockAction, addAction, textureAction, qualitySpriteName)
            i = i + 1
        end
    end
    this.m_TextureLoader:Init(mapiInfo, nil, nil, -135, nil)
end
