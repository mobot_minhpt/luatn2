--import
local GameObject			= import "UnityEngine.GameObject"

local UILabel               = import "UILabel"
local DelegateFactory		= import "DelegateFactory"

local CUIManager            = import "L10.UI.CUIManager"
local CUIResources          = import "L10.UI.CUIResources"

local CommonDefs			= import "L10.Game.CommonDefs"
local Vector3               = import "UnityEngine.Vector3"
--define
CLuaBugWeiChangTopRightWnd = class()

--RegistChildComponent
RegistChildComponent(CLuaBugWeiChangTopRightWnd, "ExpandButton",    GameObject)
RegistChildComponent(CLuaBugWeiChangTopRightWnd, "CountLabel1",	    UILabel)
RegistChildComponent(CLuaBugWeiChangTopRightWnd, "CountLabel2",	    UILabel)
RegistChildComponent(CLuaBugWeiChangTopRightWnd, "Content",		    GameObject)

--RegistClassMember
--RegistClassMember(CLuaBugWeiChangTopRightWnd, "Name")

--@region flow function

function CLuaBugWeiChangTopRightWnd:Init()
    if CLuaBugWeiChangTopRightWnd.Data ~= nil then
        self.CountLabel1.text = tostring(CLuaBugWeiChangTopRightWnd.Data.GetCount)
        self.CountLabel2.text = tostring(CLuaBugWeiChangTopRightWnd.Data.LeftCount)
    end
end

function CLuaBugWeiChangTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("OnSyncBugHuntProgress", self, "OnSyncBugHuntProgress")
    local btnclick = function(go)
        self:OnExpandButtonClick()
    end

    CommonDefs.AddOnClickListener(self.ExpandButton, DelegateFactory.Action_GameObject(btnclick), false)
end

function CLuaBugWeiChangTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("OnSyncBugHuntProgress", self, "OnSyncBugHuntProgress")
end

--@endregion

function CLuaBugWeiChangTopRightWnd:OnExpandButtonClick()
	self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
	self.Content:SetActive(false)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function CLuaBugWeiChangTopRightWnd:OnHideTopAndRightTipWnd()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
	self.Content:SetActive(true)
end
CLuaBugWeiChangTopRightWnd.Data = nil
function CLuaBugWeiChangTopRightWnd:OnSyncBugHuntProgress(catchedCount,leftCount)
    self.CountLabel1.text = tostring(catchedCount)
    self.CountLabel2.text = tostring(leftCount)
end
