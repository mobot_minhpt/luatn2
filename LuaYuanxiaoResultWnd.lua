local UILabel = import "UILabel"

CLuaYuanxiaoResultWnd = class()

CLuaYuanxiaoResultWnd.Path = "ui/yuanxiao/LuaYuanxiaoResultWnd"

function CLuaYuanxiaoResultWnd:Init()
  self.transform:Find("Anchor/Label/ScoreLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%d分"), LuaYuanxiaoMgr.TeamScore)
  self.transform:Find("Anchor/Label"):GetComponent(typeof(UILabel)).text = LuaYuanxiaoMgr.MyScore <= 0 and LocalString.GetString("很遗憾你没有制作成功的汤圆。") or SafeStringFormat3(LocalString.GetString("恭喜你完成了[ffed5f]%d[-]个汤圆的制作！"), LuaYuanxiaoMgr.MyScore)
end
