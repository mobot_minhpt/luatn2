
luaGuildVoteMgr = {}

----------主界面
luaGuildVoteMgr.VoteData = {}
luaGuildVoteMgr.Limit = false           --  是否有投票资格

----------投票及揭晓详细界面----
luaGuildVoteMgr.QuestionId = 0          --  ID
luaGuildVoteMgr.QuestionContent = ""    --  内容
luaGuildVoteMgr.IsVoted = false         --  是否投过票
luaGuildVoteMgr.VotePlayerId = 0        --  投的是谁的票（没投的时候就是0）
luaGuildVoteMgr.IsFinish = false        --  是否已揭晓
luaGuildVoteMgr.WinPlayerId = 0         --  获胜者ID
luaGuildVoteMgr.PlayerOptionTable = {}  --  可投票玩家选项table

----------发起投票
luaGuildVoteMgr.SendContent = ""        --  发给服务器的问题内筒
luaGuildVoteMgr.SendTitle = ""          --  发给服务器的title
luaGuildVoteMgr.GuildMemberTable = {}   --  工会成员列表

function luaGuildVoteMgr:ClearVoteData()
    luaGuildVoteMgr.VoteData = {}
    luaGuildVoteMgr.Limit = false
    luaGuildVoteMgr:ClearEditData()
end

function luaGuildVoteMgr:ClearDetailData()
    luaGuildVoteMgr.QuestionId = 0          
    luaGuildVoteMgr.QuestionContent = ""    
    luaGuildVoteMgr.IsVoted = false         
    luaGuildVoteMgr.VotePlayerId = 0        
    luaGuildVoteMgr.IsFinish = false        
    luaGuildVoteMgr.WinPlayerId = 0   
    luaGuildVoteMgr.PlayerOptionTable = {}      
end

function luaGuildVoteMgr:ClearEditData()
    luaGuildVoteMgr.SendContent = ""        
    luaGuildVoteMgr.SendTitle = ""    
    luaGuildVoteMgr.GuildMemberTable = {} 
end
