local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaWorldEventMgr2021 = class()

LuaWorldEventMgr2021.FengHuaLuDatas = nil
LuaWorldEventMgr2021.FengHuaLuWishedDatas = nil
LuaWorldEventMgr2021.Enabled = false

function Gas2Gac.SyncFengHuaItemStatus(itemId, enabled, rewarded, bXuYuan)
    local info = CClientMainPlayer.Inst.PlayProp.FengHuaLuInfo
    if info then
        info:SetBit(itemId,enabled)
    end
    local rewardinfo = CClientMainPlayer.Inst.PlayProp.FengHuaLuAwardInfo
    if rewardinfo then
        rewardinfo:SetBit(itemId,rewarded)
    end

    local wishinfo = CClientMainPlayer.Inst.PlayProp.FengHuaLuXuYuanInfo
    if wishinfo then
        wishinfo:SetBit(itemId,bXuYuan)
    end

	LuaWorldEventMgr2021.InitFengHuaLuData()
end

function LuaWorldEventMgr2021.InitFengHuaLuData()
    local p = CClientMainPlayer.Inst.PlayProp.FengHuaLuInfo
    local ap = CClientMainPlayer.Inst.PlayProp.FengHuaLuAwardInfo
    local wp = CClientMainPlayer.Inst.PlayProp.FengHuaLuXuYuanInfo
    local size = p.bitsize
    local datas = {}
    local wisheddatas = {}
    for i=1,size-1 do
        local ison = p:GetBit(i)
        local rewarded = ap:GetBit(i)
        local wished = wp and wp:GetBit(i)
        if ison then 
            local cfg = WorldEvent_FengHuaLu.GetData(i)
            if cfg then
                datas[#datas+1] = {
                    ID = i,
                    Unlock = ison,
                    Rewarded = rewarded,
                    Index = cfg.Index,
                    Type = cfg.Type,
                }
            end
        elseif wished then
            wisheddatas[i] = true
        end
    end
    table.sort(datas, function(a, b)
        if a.Type == b.Type then
            if a.Index == b.Index then 
                return a.ID < b.ID
            else
                return a.Index < b.Index
            end
        else
            return a.Type < b.Type    
        end   
    end)
    LuaWorldEventMgr2021.ProgressReward = CClientMainPlayer.Inst.PlayProp.FengHuaLuStageAwardId
    LuaWorldEventMgr2021.FengHuaLuDatas = datas
    LuaWorldEventMgr2021.FengHuaLuWishedDatas = wisheddatas
    g_ScriptEvent:BroadcastInLua("SyncFengHuaItemStatus")
end

function LuaWorldEventMgr2021.ShowFengHuaLu()
    if not LuaWorldEventMgr2021.Enabled then return end
    LuaWorldEventMgr2021.CurID = -1
    LuaWorldEventMgr2021.InitFengHuaLuData()
    CUIManager.ShowUI(CLuaUIResources.SanJieFengHuaLuWnd)
end

function LuaWorldEventMgr2021.ShowFengHuaLuID(id)
    if not LuaWorldEventMgr2021.Enabled then return end
    local cid = tonumber(id)
    local p = CClientMainPlayer.Inst.PlayProp.FengHuaLuInfo;
    if p:GetBit(cid) then
        LuaWorldEventMgr2021.InitFengHuaLuData()
        LuaWorldEventMgr2021.CurID = cid
        CUIManager.ShowUI(CLuaUIResources.SanJieFengHuaLuWnd)
    else
        g_MessageMgr:ShowMessage("FENGHUALU_UNLOCK_TIP")
    end
end

LuaWorldEventMgr2021.WishID = 0

function LuaWorldEventMgr2021.ShowFengHuaLuWishWnd(id)
    if not LuaWorldEventMgr2021.Enabled then return end
    LuaWorldEventMgr2021.WishID = id
    CUIManager.ShowUI(CLuaUIResources.SanJieFengHuaLuWishWnd)
end

function LuaWorldEventMgr2021.CloseFengHuaLuWishWnd()
    CUIManager.CloseUI(CLuaUIResources.SanJieFengHuaLuWishWnd)
end

function LuaWorldEventMgr2021.RequestWish()
    Gac2Gas.RequestFengHuaLuXuYuan(LuaWorldEventMgr2021.WishID)
end

function LuaWorldEventMgr2021.GetProgressReward()
    Gac2Gas.RequestGetFengHuaLuStageAward()
end

function Gas2Gac.RequestGetFengHuaLuStageAwardSuccess(maxAwardId)
    CClientMainPlayer.Inst.PlayProp.FengHuaLuStageAwardId = maxAwardId
    LuaWorldEventMgr2021.ProgressReward = maxAwardId
    g_ScriptEvent:BroadcastInLua("SyncFengHuaProgressReward")--只更新进度奖励信息
end
