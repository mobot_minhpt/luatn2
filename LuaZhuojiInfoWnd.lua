local LuaGameObject=import "LuaGameObject"

local UIScrollView = import "UIScrollView"
local Extensions = import "Extensions"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Color = import "UnityEngine.Color"

CLuaZhuojiInfoWnd = class()
RegistClassMember(CLuaZhuojiInfoWnd, "m_Template")
RegistClassMember(CLuaZhuojiInfoWnd, "m_Table")
RegistClassMember(CLuaZhuojiInfoWnd, "m_ScrollView")

function CLuaZhuojiInfoWnd:Init()
	local scrollView = LuaGameObject.GetChildNoGC(self.transform, "ScrollView").gameObject
	self.m_ScrollView = CommonDefs.GetComponent_GameObject_Type(scrollView, typeof(UIScrollView))

	self.m_Table = LuaGameObject.GetChildNoGC(scrollView.transform, "Table").table
	Extensions.RemoveAllChildren(self.m_Table.transform)
	self.m_Template = LuaGameObject.GetChildNoGC(self.transform, "Template").gameObject

	local tbl = {}
	for _, res in pairs(CLuaLiuYiMgr.ZhuojiResult) do
		if res.name and res.rank and res.force and res.id and res.score then
			table.insert(tbl, res)
		end
	end
	table.sort(tbl, function(res1, res2) return res1.rank < res2.rank end)

	local force2name = {[1] = LocalString.GetString("红队"), [2] = LocalString.GetString("黄队"), [3] = LocalString.GetString("蓝队"), [4] = LocalString.GetString("绿队"), }
	for _, res in ipairs(tbl) do
		local unit = NGUITools.AddChild(self.m_Table.gameObject, self.m_Template)
		unit:SetActive(true)

		local rank = res.rank
		local rankLabel = LuaGameObject.GetChildNoGC(unit.transform, "rank").label
		rankLabel.text = rank .. ""

		local name = res.name
		local nameLabel = LuaGameObject.GetChildNoGC(unit.transform, "name").label
		nameLabel.text = name

		local force = res.force
		local forceLabel = LuaGameObject.GetChildNoGC(unit.transform, "force").label
		forceLabel.text = force2name[force] or ""

		local score = res.score
		local scoreLabel = LuaGameObject.GetChildNoGC(unit.transform, "score").label
		scoreLabel.text = score

		if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == res.id then
			rankLabel.color = Color.green
			nameLabel.color = Color.green
			forceLabel.color = Color.green
			scoreLabel.color = Color.green
		else
			rankLabel.color = Color.white
			nameLabel.color = Color.white
			forceLabel.color = Color.white
			scoreLabel.color = Color.white
		end
	end

	self.m_Table:Reposition()
	self.m_ScrollView:ResetPosition()
	self.m_Template:SetActive(false)
end

return CLuaZhuojiInfoWnd