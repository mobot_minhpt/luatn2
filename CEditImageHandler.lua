-- Auto Generated!!
local BoolDelegate = import "UIEventListener+BoolDelegate"
local CEditImageHandler = import "L10.UI.CEditImageHandler"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local UIEventListener = import "UIEventListener"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CEditImageHandler.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.confirmBtn).onClick = MakeDelegateFromCSFunction(this.OnClickConfirmBtn, VoidDelegate, this)
    UIEventListener.Get(this.cancleBtn).onClick = MakeDelegateFromCSFunction(this.OnClickCancleBtn, VoidDelegate, this)

    UIEventListener.Get(this.rotateBtn).onPress = MakeDelegateFromCSFunction(this.OnPressRotateBtn, BoolDelegate, this)
    UIEventListener.Get(this.pivot).onPress = MakeDelegateFromCSFunction(this.OnPressPivot, BoolDelegate, this)
end
CEditImageHandler.m_OnClickConfirmBtn_CS2LuaHook = function (this, go) 
    if this.onConfirm ~= nil then
        invoke(this.onConfirm)
    end
    --relatedWidget = null;
    this.gameObject:SetActive(false)
end
CEditImageHandler.m_OnClickCancleBtn_CS2LuaHook = function (this, go) 
    if this.onCancle ~= nil then
        invoke(this.onCancle)
    end

    --relatedWidget = null;
    this.gameObject:SetActive(false)
end
CEditImageHandler.m_InitTxtParams_CS2LuaHook = function (this, widget, expression_txt_offset_x, expression_txt_offset_y, expression_txt_scale_x, expression_txt_scale_y, expression_txt_rotation) 
    this.relatedWidget = widget

    this.rawWidth = CExpressionMgr.TxtWidth
    --=CExpressionMgr.Inst.;
    this.rawHeight = CExpressionMgr.TxtHeight
    --=TxtHeight;

    this.relatedWidget.transform.localPosition = Vector3(expression_txt_offset_x, expression_txt_offset_y)
    if expression_txt_scale_x == 0 then
        expression_txt_scale_x = 100
    end
    this.relatedWidget.width = math.floor((CExpressionMgr.TxtWidth * (expression_txt_scale_x / 100)))
    this.relatedWidget.height = math.floor((CExpressionMgr.TxtHeight * (expression_txt_scale_x / 100)))
    this.relatedWidget.transform.localEulerAngles = Vector3(0, 0, expression_txt_rotation)

    this:UpdateWidget()
end
CEditImageHandler.m_InitStickerParams_CS2LuaHook = function (this, widget, sticker_offset_x, sticker_offset_y, sticker_scale_x, sticker_scale_y, sticker_rotation) 
    this.relatedWidget = widget

    this.rawWidth = CExpressionMgr.StickerSize
    --=CExpressionMgr.Inst.;
    this.rawHeight = CExpressionMgr.StickerSize
    --=TxtHeight;

    this.relatedWidget.transform.localPosition = Vector3(sticker_offset_x, sticker_offset_y)
    if sticker_scale_x == 0 then
        sticker_scale_x = 100
    end
    this.relatedWidget.width = math.floor((CExpressionMgr.StickerSize * (sticker_scale_x / 100)))
    this.relatedWidget.height = math.floor((CExpressionMgr.StickerSize * (sticker_scale_y / 100)))
    this.relatedWidget.transform.localEulerAngles = Vector3(0, 0, sticker_rotation)

    this:UpdateWidget()
end
CEditImageHandler.m_SetRelatedWidget_CS2LuaHook = function (this, widget) 
    if widget == nil then
        this.relatedWidget = nil
        return
    end
    this.relatedWidget = widget
    this:UpdateWidget()
end
CEditImageHandler.m_UpdateRotate_CS2LuaHook = function (this, line, fromVec, toVec) 
    if (CommonDefs.op_Subtraction_Vector3_Vector3(toVec, fromVec)).x > 0 then
        local angle = Vector3.Angle(CommonDefs.op_Subtraction_Vector3_Vector3(toVec, fromVec), Vector3(0, - 1))
        line.localEulerAngles = Vector3(0, 0, angle)
    else
        local angle = Vector3.Angle(CommonDefs.op_Subtraction_Vector3_Vector3(toVec, fromVec), Vector3(0, - 1))
        line.localEulerAngles = Vector3(0, 0, - angle)
    end
end
