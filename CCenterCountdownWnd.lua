local CCenterCountdownWnd = import "L10.UI.CCenterCountdownWnd"
local Animation = import "UnityEngine.Animation"
local CScene=import "L10.Game.CScene"

CCenterCountdownWnd.m_hookInit = function (this)
    CLuaCenterCountdownWnd:Init(this)
    this.start = true
    this.elapseSecond = 0
    this.nextTickTime = this.startTime
    this.lable.text = ""
end

CCenterCountdownWnd.m_hookPlay = function (this)
    CLuaCenterCountdownWnd:Play()
    this.lable.text = this.count - this.elapseSecond
    this.tweener:ResetToBeginning()
    this.tweener:PlayForward()
    this.alphaTweener:ResetToBeginning()
    this.alphaTweener:PlayForward()
end

CLuaCenterCountdownWnd = {}
CLuaCenterCountdownWnd.m_Wnd = nil
CLuaCenterCountdownWnd.m_Bg = nil
CLuaCenterCountdownWnd.m_BgScaleAnim = nil
CLuaCenterCountdownWnd.m_CurGamePlay = nil

function CLuaCenterCountdownWnd:Init(wnd)
    self.m_Wnd = wnd

    if not CommonDefs.IsUnityObjectNull(self.m_Bg) then
        self.m_Bg:SetActive(false)
        self.m_Bg = nil 
        self.m_BgScaleAnim = nil
    end

    if self.m_CurGamePlay == 51103060 or self.m_CurGamePlay == 51103056 then -- 圣诞2022温暖平安夜 融雪追麋鹿
        self.m_CurGamePlay = nil
        self.m_Bg           = self.m_Wnd.transform:Find("SpecialBg/Christmas2022").gameObject
        self.m_BgScaleAnim  = self.m_Wnd.transform:Find("SpecialBg/Christmas2022/Animation/Christmas2022"):GetComponent(typeof(Animation))
    end

    if not CommonDefs.IsUnityObjectNull(self.m_Bg) then
        self.m_Bg:SetActive(true)
    end
end

function CLuaCenterCountdownWnd:Play()
    if not CommonDefs.IsUnityObjectNull(self.m_BgScaleAnim) then
        self.m_BgScaleAnim:Play()
    end
end