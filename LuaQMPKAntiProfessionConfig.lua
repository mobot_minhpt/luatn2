local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Transform = import "UnityEngine.Transform"
local GameObject = import "UnityEngine.GameObject"
local QnButton = import "L10.UI.QnButton"
local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local EnumClass = import "L10.Game.EnumClass"
local Profession = import "L10.Game.Profession"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Constants = import "L10.Game.Constants"
local UICircleGrid = import "L10.UI.UICircleGrid"

CLuaQMPKAntiProfessionConfig = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(CLuaQMPKAntiProfessionConfig, "Circle", "Circle", UICircleGrid)
RegistChildComponent(CLuaQMPKAntiProfessionConfig, "AntiTemplate", "AntiTemplate", GameObject)
RegistChildComponent(CLuaQMPKAntiProfessionConfig, "CurWuXing", "CurWuXing", QnButton)
RegistChildComponent(CLuaQMPKAntiProfessionConfig, "EffectsTable", "EffectsTable", QnTableView)
RegistChildComponent(CLuaQMPKAntiProfessionConfig, "LeftLvLab", "LeftLvLab", UILabel)
RegistChildComponent(CLuaQMPKAntiProfessionConfig, "OpBtn", "OpBtn", QnButton)
RegistChildComponent(CLuaQMPKAntiProfessionConfig, "TipBtn", "TipBtn", QnButton)

--@endregion RegistChildComponent end
RegistClassMember(CLuaQMPKAntiProfessionConfig, "m_WuXingTex")
RegistClassMember(CLuaQMPKAntiProfessionConfig, "m_CurSlotNum")
RegistClassMember(CLuaQMPKAntiProfessionConfig, "m_EffectsInfos")
RegistClassMember(CLuaQMPKAntiProfessionConfig, "m_TotallMaxLv")
RegistClassMember(CLuaQMPKAntiProfessionConfig, "m_IsOtherPlayer")
RegistClassMember(CLuaQMPKAntiProfessionConfig, "m_OtherPlayerInfo")
RegistClassMember(CLuaQMPKAntiProfessionConfig, "m_LeftLv")

function CLuaQMPKAntiProfessionConfig:Awake()
    self.m_IsOtherPlayer = CLuaQMPKMgr.s_IsOtherPlayer
    self.m_OtherPlayerInfo = CLuaQMPKMgr.s_PlayerInfo
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_WuXingTex = self.CurWuXing:GetComponent(typeof(CUITexture))
    self.m_LeftLv = self.transform:Find("Anchor/Right/RightBottom/LeftLv").gameObject

    self.CurWuXing.OnClick = DelegateFactory.Action_QnButton(function (btn)
        if self.m_IsOtherPlayer then
            return
        end
        CUIManager.ShowUI(CLuaUIResources.QMPKWuXingAdjustWnd)
    end)

    self.OpBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        if self.m_IsOtherPlayer then
            self:LearnOtherConfig()
            return
        end
        CUIManager.ShowUI(CLuaUIResources.QMPKAntiprofessionAdjustWnd)
    end)

    self.TipBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        g_MessageMgr:ShowMessage("ANTIPROFESSION_VIEW_TIP")
    end)

    self.m_TotallMaxLv = QuanMinPK_Setting.GetData().TotalAntiProfessionPointNum

    self.m_CurSlotNum = (EnumToInt(EnumClass.Undefined) - 2)
end

function CLuaQMPKAntiProfessionConfig:Init()
    self:LoadAntiProfessions()

    self:InitEffects()
    
    self:RefreshWuXing()

    self:RefreshLeftLv()

    if self.m_OtherPlayerInfo then
        self:InitOtherPlayerConfig()
    end
end

--@region UIEvent

--@endregion UIEvent

function CLuaQMPKAntiProfessionConfig:OnEnable()
    if not self.m_IsOtherPlayer then
        g_ScriptEvent:AddListener("OnSyncPlayerMingGe", self, "OnSyncPlayerMingGe")
    end
    g_ScriptEvent:AddListener("SetQmpkAntiProfessionDataSuccess", self, "OnSetQmpkAntiProfessionDataSuccess")
    self:Init()
end

function CLuaQMPKAntiProfessionConfig:OnDisable()
    if not self.m_IsOtherPlayer then
        g_ScriptEvent:RemoveListener("OnSyncPlayerMingGe", self, "OnSyncPlayerMingGe")
    end
    g_ScriptEvent:RemoveListener("SetQmpkAntiProfessionDataSuccess", self, "OnSetQmpkAntiProfessionDataSuccess")
end

function CLuaQMPKAntiProfessionConfig:OnSyncPlayerMingGe()
    g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("修改配置成功"))
    self:RefreshWuXing()
end

function CLuaQMPKAntiProfessionConfig:OnSetQmpkAntiProfessionDataSuccess()
    if self.m_IsOtherPlayer then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("克符学习成功"))
        return
    else
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("修改配置成功"))
    end

    self:LoadAntiProfessions()
    self:RefreshEffects()
    self:RefreshLeftLv()
end

function CLuaQMPKAntiProfessionConfig:LoadAntiProfessions()
    self.AntiTemplate:SetActive(false)
    local needReposition = false
    for i = 1,  self.m_CurSlotNum - self.Circle.transform.childCount do
        local go = NGUITools.AddChild(self.Circle.gameObject, self.AntiTemplate)
        go:SetActive(true)
        needReposition = true
    end
    if needReposition then
        self.Circle:Reposition()
    end

    for i = 0, self.Circle.transform.childCount - 1 do
        local item = self.Circle.transform:GetChild(i)
        local info 
        if self.m_IsOtherPlayer then
            local curLv = self.m_OtherPlayerInfo.AntiprofessionInfo[i + 1]
            info = {CurLevel = curLv, Profession = i + 1}
        else
            info = CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill, i + 1) and CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill[i + 1] or nil
        end

        self:InitItem(i, item, info)
    end
end

function CLuaQMPKAntiProfessionConfig:InitItem(index, item, info)
    local Btn       = item:GetComponent(typeof(QnButton))
    local NameLab   = item:Find("NameLab"):GetComponent(typeof(UILabel))
    local SeqLab    = item:Find("SeqLab"):GetComponent(typeof(UILabel))
    local LvLab     = item:Find("LvLab"):GetComponent(typeof(UILabel))
    local Icon      = item:Find("Icon"):GetComponent(typeof(CUITexture))
    local Alert     = item:Find("Alert").gameObject
    local Lock      = item.transform:Find("Lock").gameObject

    if index < self.m_CurSlotNum then
        local curLevel = info.CurLevel
        local profession = info.Profession
        local proClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), profession)
        local proName = Profession.GetName(proClass)

        if not self.m_IsOtherPlayer and CClientMainPlayer.Inst.IsInXianShenStatus then
            local index = index + 1
            local oriLv = curLevel
            local adjustLv = LuaZongMenMgr:GetAdjustAntiProfessionSkillLevel(index, oriLv)
            if oriLv ~= adjustLv then
                LvLab.text = SafeStringFormat3("[c][%s]lv.%d%%[-][/c]", Constants.ColorOfFeiSheng, adjustLv)
            else
                LvLab.text = SafeStringFormat3("lv.%d%%", oriLv)
            end
        else
            LvLab.text = SafeStringFormat3("lv.%d%%", curLevel)
        end

        NameLab.text = LocalString.GetString("克") .. proName
        SeqLab.text = index + 1
        LvLab.gameObject:SetActive(true)
        LvLab.text = "lv." .. curLevel
        Icon.gameObject:SetActive(true)
        Icon:LoadMaterial(Profession.GetLargeIcon(proClass))
    elseif index == self.m_CurSlotNum then
        NameLab.text = ""
        SeqLab.text = index + 1
        LvLab.gameObject:SetActive(false)
        Icon.gameObject:SetActive(false)
    else
        NameLab.text = ""
        SeqLab.text = index + 1
        LvLab.gameObject:SetActive(false)
        Icon.gameObject:SetActive(false)
    end
    Alert:SetActive(false)
    Lock:SetActive(false)

    local dir = CommonDefs.op_Subtraction_Vector3_Vector3(self.Circle.transform.position, Icon.transform.position)
    local offset = CommonDefs.op_Multiply_Vector3_Single(dir, 0.3)
    NameLab.transform.position = CommonDefs.op_Addition_Vector3_Vector3(Icon.transform.position, offset)
end


function CLuaQMPKAntiProfessionConfig:InitEffects()
    self.EffectsTable.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_EffectsInfos
        end,
        function(item, index)
            self:InitEffect(item, self.m_EffectsInfos[index + 1])
        end
    )

    self:RefreshEffects()
end

function CLuaQMPKAntiProfessionConfig:RefreshEffects()
    self.m_EffectsInfos = {}

    if self.m_IsOtherPlayer then
        for i = 1, #self.m_OtherPlayerInfo.AntiprofessionInfo do
            local curLv = self.m_OtherPlayerInfo.AntiprofessionInfo[i]
            local info = {CurLevel = curLv, Profession = i}
            table.insert(self.m_EffectsInfos, {key = i, value = info})
        end
    else
        for i = 1, self.m_CurSlotNum do
            local ret, data = CommonDefs.DictTryGet_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill, i)
            table.insert(self.m_EffectsInfos, {key = i, value = data})
        end
    end
    
    self.EffectsTable:ReloadData(true, false)
end

function CLuaQMPKAntiProfessionConfig:InitEffect(item, info)
    local lab = item.transform:Find("Lab"):GetComponent(typeof(UILabel))

    local profession = info.value.Profession
    local proClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), profession)

    local oriData = SoulCore_AntiProfessionLevel.GetData(info.value.CurLevel)
    local oriMul =  oriData and oriData.AntiProfessionMul or 0
    local effectStr
    
    if not self.m_IsOtherPlayer and CClientMainPlayer.Inst.IsInXianShenStatus then
        local index = info.key
        local oriLv = info.value.CurLevel
        local adjustLv = LuaZongMenMgr:GetAdjustAntiProfessionSkillLevel(index, oriLv)
        if oriLv ~= adjustLv then
            local adjustData = SoulCore_AntiProfessionLevel.GetData(adjustLv)
            local adjustMul =  adjustData and adjustData.AntiProfessionMul or 0
            effectStr = SafeStringFormat3("[c][%s]+%.1f%%[-][/c] (%.1f%%)", Constants.ColorOfFeiSheng, adjustMul * 100, oriMul * 100)
        else
            effectStr = SafeStringFormat3("+%.1f%%", oriMul * 100)
        end
    else
        effectStr = SafeStringFormat3("+%.1f%%", oriMul * 100)
    end

    lab.text = SafeStringFormat3(LocalString.GetString("对%s伤害 %s"), Profession.GetFullName(proClass) , effectStr)
end

function CLuaQMPKAntiProfessionConfig:RefreshWuXing()
    local wuXing
    if self.m_IsOtherPlayer then
        wuXing = self.m_OtherPlayerInfo.MingGe
    else
        local mainPlayer = (CClientMainPlayer.Inst or {})
        wuXing = (mainPlayer.BasicProp or {}).MingGe
    end
    self.m_WuXingTex:LoadMaterial(LuaZongMenMgr:GetMingGeMatPath(wuXing))
end

function CLuaQMPKAntiProfessionConfig:RefreshLeftLv()
    local count = 0
    for i = 0, self.Circle.transform.childCount - 1 do
        local info = CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill, i + 1) and CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill[i + 1] or nil
        if info then
            count = count + info.CurLevel
        end
    end
    local leftAdjustNum = self.m_TotallMaxLv - count
    self.LeftLvLab.text = leftAdjustNum
end

function CLuaQMPKAntiProfessionConfig:LearnOtherConfig()
    local requestTab = {}
    for i = 1, #self.m_OtherPlayerInfo.AntiprofessionInfo do
        local curLv = self.m_OtherPlayerInfo.AntiprofessionInfo[i]
        table.insert(requestTab, curLv)
    end
    local requestStr = table.concat(requestTab, ",")
    Gac2Gas.RequestSetQmpkAntiProfessionData(requestStr)

    CUIManager.CloseUI(CLuaUIResources.QMPKAntiprofessionAdjustWnd)
end

function CLuaQMPKAntiProfessionConfig:InitOtherPlayerConfig()
    self.OpBtn.Text = LocalString.GetString("学习此百克符")
    self.TipBtn.gameObject:SetActive(false)
    self.m_LeftLv:SetActive(false)

    for i = 0, self.CurWuXing.transform.childCount - 1 do
        self.CurWuXing.transform:GetChild(i).gameObject:SetActive(false)
    end

    self.CurWuXing.OnClick = nil
end

function CLuaQMPKAntiProfessionConfig:OnPageCloseOrChange(finalFunc)
    if finalFunc then finalFunc() end
    return false
end