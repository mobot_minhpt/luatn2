local QnButton = import "L10.UI.QnButton"
local CButton = import "L10.UI.CButton"
local QnTipButton = import "L10.UI.QnTipButton"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UITabBar = import "L10.UI.UITabBar"
local EnumNumber = import "L10.Game.EnumNumber"
local Movement = import "UIScrollView+Movement"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPlayerShopPopupMenuInfoMgr = import "L10.UI.CPlayerShopPopupMenuInfoMgr"
local CPopupMenuInfoMgrAlignType = import  "L10.UI.CPopupMenuInfoMgr+AlignType"
local UISimpleTableView = import "L10.UI.UISimpleTableView"
local DefaultUISimpleTableViewDataSource = import "L10.UI.DefaultUISimpleTableViewDataSource"
local CScheduleAlertCtl=import "L10.UI.CScheduleAlertCtl"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaStarBiWuJiFenSaiDetailView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaStarBiWuJiFenSaiDetailView, "TipButton", "TipButton", QnButton)
RegistChildComponent(LuaStarBiWuJiFenSaiDetailView, "TipLabel", "TipLabel", UILabel)
RegistChildComponent(LuaStarBiWuJiFenSaiDetailView, "JingCaiButton", "JingCaiButton", CButton)
RegistChildComponent(LuaStarBiWuJiFenSaiDetailView, "TabTemplate", "TabTemplate", GameObject)
RegistChildComponent(LuaStarBiWuJiFenSaiDetailView, "RankTemplate", "RankTemplate", GameObject)
RegistChildComponent(LuaStarBiWuJiFenSaiDetailView, "TabTable", "TabTable", UITabBar)
RegistChildComponent(LuaStarBiWuJiFenSaiDetailView, "WordFilterButton", "WordFilterButton", QnTipButton)
RegistChildComponent(LuaStarBiWuJiFenSaiDetailView, "Empty", "Empty", UILabel)
RegistChildComponent(LuaStarBiWuJiFenSaiDetailView, "GuanZhan", "GuanZhan", GameObject)
RegistChildComponent(LuaStarBiWuJiFenSaiDetailView, "Rank", "Rank", GameObject)
RegistChildComponent(LuaStarBiWuJiFenSaiDetailView, "RankTableHead", "RankTableHead", GameObject)
RegistChildComponent(LuaStarBiWuJiFenSaiDetailView, "RankTableBody", "RankTableBody", UISimpleTableView)
RegistChildComponent(LuaStarBiWuJiFenSaiDetailView, "HorizontalScrollViewIndicator", "HorizontalScrollViewIndicator", GameObject)
RegistChildComponent(LuaStarBiWuJiFenSaiDetailView, "GuanZhanTableBody", "GuanZhanTableBody", UISimpleTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaStarBiWuJiFenSaiDetailView, "m_SelfZhanDuiId")
RegistClassMember(LuaStarBiWuJiFenSaiDetailView, "m_CurrentMatchIndex")
RegistClassMember(LuaStarBiWuJiFenSaiDetailView, "m_CurSelectGroup")
RegistClassMember(LuaStarBiWuJiFenSaiDetailView, "m_CurSelectTabIndex")
RegistClassMember(LuaStarBiWuJiFenSaiDetailView, "m_MainPlayerCurGroup")
RegistClassMember(LuaStarBiWuJiFenSaiDetailView, "m_GroupIndex2Name")
RegistClassMember(LuaStarBiWuJiFenSaiDetailView, "m_FilterActions")
RegistClassMember(LuaStarBiWuJiFenSaiDetailView, "m_DateTabList")
RegistClassMember(LuaStarBiWuJiFenSaiDetailView, "m_GuanZhanData")
RegistClassMember(LuaStarBiWuJiFenSaiDetailView, "m_RankData")
RegistClassMember(LuaStarBiWuJiFenSaiDetailView, "m_MatchItemTemplate")
RegistClassMember(LuaStarBiWuJiFenSaiDetailView, "m_RankStatusItemTemplate")
RegistClassMember(LuaStarBiWuJiFenSaiDetailView, "m_HorizontalRankScrollArrow")
RegistClassMember(LuaStarBiWuJiFenSaiDetailView, "m_MaxRankRound")
RegistClassMember(LuaStarBiWuJiFenSaiDetailView, "m_RoundItem")
RegistClassMember(LuaStarBiWuJiFenSaiDetailView, "m_SplitItem")
RegistClassMember(LuaStarBiWuJiFenSaiDetailView, "m_RankItem")
function LuaStarBiWuJiFenSaiDetailView:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.JingCaiButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJingCaiButtonClick()
	end)


	
	UIEventListener.Get(self.WordFilterButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnWordFilterButtonClick()
	end)


    --@endregion EventBind end

	self.TipLabel.text = g_MessageMgr:FormatMessage("StarBiWu_JiFenSai_Rule_Title")
	self.m_CurSelectGroup = 0		-- 当前所选组别
	self.m_GroupIndex2Name = nil	-- 组别id对应组名
	self.m_FilterActions = nil		-- 组别筛选列表
	self.m_DateTabList = nil		-- 日期tab
	self.m_GuanZhanData = nil		-- 对局数据
	self.m_RankData = nil			-- 排行榜数据
	self.m_SelfZhanDuiId = 0		-- 自己的战队id
	self.m_CurrentMatchIndex = 0	-- 当前正在比赛的playIdx,也是观战请求 RequestWatchStarBiwuZongJueSai 的参数
	self.m_CurSelectTabIndex = 0	-- 当前所选tab，1-3积分赛天数，4-积分赛排名
	self.m_MainPlayerCurGroup = 0	-- 自己当前所在组别
	self.m_MaxRankRound = 15 		-- 排行榜总共进行了几轮积分赛

	local alertCtrl = self.JingCaiButton.gameObject:GetComponent(typeof(CScheduleAlertCtl))
    alertCtrl.tabType = 0
	alertCtrl.scheduleId = StarBiWuShow_Setting.GetData().MatchServerSchedule

	self.WordFilterButton.gameObject:SetActive(false)
	--self.HorizontalScrollViewIndicator.gameObject:GetComponent(typeof(UIScrollViewIndicator)).disableLayout = true
	self.TabTemplate.gameObject:SetActive(false)
	self.RankTemplate.gameObject:SetActive(false)
	self.m_MatchItemTemplate = self.GuanZhanTableBody.transform:Find("Pool/ItemTemplate").gameObject
	self.m_SplitItem = self.GuanZhanTableBody.transform:Find("Pool/Split").gameObject
	self.m_RankStatusItemTemplate = self.RankTableBody.transform:Find("Pool/Status").gameObject
	self.m_RoundItem = self.GuanZhanTableBody.transform:Find("Pool/RoundItem").gameObject
	self.m_RankItem = self.RankTableBody.transform:Find("Pool/ItemTemplate").gameObject
	self.m_RankStatusItemTemplate.gameObject:SetActive(false)
	self.m_HorizontalRankScrollArrow = self.HorizontalScrollViewIndicator.transform:Find("GradientSprite/ArrowSprite").gameObject
	self:InitTabHeader()
	self:InitRankScroll()
	self.GuanZhanTableBody:Clear()
	self.GuanZhanTableBody.dataSource = DefaultUISimpleTableViewDataSource.Create(
		function()
			return #self.m_GuanZhanData
		end,
		function(index)
			return self:UpdateGuanZhanItem(index)
		end
	)
end

function LuaStarBiWuJiFenSaiDetailView:OnGetGroupInfo()
	-- 初始化组别信息
	self.m_GroupIndex2Name = {}
	local totalNum = CLuaStarBiwuMgr.m_GroupNum
	local groupList = StarBiWuShow_Setting.GetData().NewGroupName
	for i = 1, totalNum do
		if i == 1 then
			self.m_GroupIndex2Name[1] = SafeStringFormat3(LocalString.GetString("[d293e9]%s[-]"),groupList[i - 1])
		elseif i <= 11 then
			self.m_GroupIndex2Name[i] = SafeStringFormat3(LocalString.GetString("[acf8ff]%s[-]"),groupList[i - 1])
		else
			self.m_GroupIndex2Name[i] = SafeStringFormat3(LocalString.GetString("[acf8ff]普通%d组[-]"),i - 1)
		end
	end
	-- 获得玩家所在组
	self.m_MainPlayerCurGroup = CLuaStarBiwuMgr.m_MainPlayerGroup
	self.m_CurSelectGroup = self.m_MainPlayerCurGroup > 0 and self.m_MainPlayerCurGroup or 1	
	if CLuaStarBiwuMgr.m_GroupNum <= 0 then self.m_CurSelectGroup = 0 end -- 没分组的情况
	self.m_FilterActions = {}
	for i = 1, totalNum do
		table.insert(self.m_FilterActions,PopupMenuItemData(self.m_GroupIndex2Name[i], DelegateFactory.Action_int(function (index) 
			if self.m_CurSelectTabIndex <= 3 then
				-- 请求指定组积分赛信息
				Gac2Gas.QueryStarBiwuJifensaiMatchS13(self.m_CurSelectTabIndex,i,1)
			else
				Gac2Gas.QueryStarBiwuJifensaiMainRankS13(i)
			end
		end),false, self.m_MainPlayerCurGroup == i and "common_battletype_personal" or nil))
	end
	UIEventListener.Get(self.WordFilterButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnWordFilterButtonClick()
	end)
	if CLuaStarBiwuMgr.m_InitShowGroup > 0 and CLuaStarBiwuMgr.m_InitShowGroup <= totalNum then
		self.m_CurSelectGroup = CLuaStarBiwuMgr.m_InitShowGroup
	end

	local curDay = self:UpdateTabDateTime()
	-- 请求当前所在组当前天比赛信息
	Gac2Gas.QueryStarBiwuJifensaiMatchS13(curDay,self.m_CurSelectGroup,1)
end
function LuaStarBiWuJiFenSaiDetailView:InitTabHeader()
	Extensions.RemoveAllChildren(self.TabTable.transform)
	self.m_DateTabList = {}
	for i = 1,3 do
		local Tab = CUICommonDef.AddChild(self.TabTable.gameObject,self.TabTemplate.gameObject)
		local tabLabel = Tab.transform:Find("Label"):GetComponent(typeof(UILabel))
		tabLabel.text = ""
		Tab.gameObject:SetActive(true)
		self.m_DateTabList[i] = {go = Tab,tabLabel = tabLabel}
	end

	local rankTab = CUICommonDef.AddChild(self.TabTable.gameObject,self.RankTemplate.gameObject)
	local tabLabel = rankTab.transform:Find("Label"):GetComponent(typeof(UILabel))
	tabLabel.text = LocalString.GetString("积分赛排名")
	rankTab.gameObject:SetActive(true)
	self.TabTable.transform:GetComponent(typeof(UITable)):Reposition()
	self.TabTable.enabled = true
	self.TabTable:ReloadTabButtons()
	self.TabTable.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabChange(index)
	end)
end

function LuaStarBiWuJiFenSaiDetailView:InitRankScroll()
	local content = self.Rank.transform:Find("content")
	local headScrollView = self.RankTableHead.gameObject:GetComponent(typeof(UIScrollView))
	local headPanel = self.RankTableHead.gameObject:GetComponent(typeof(UIPanel))
	local BodyScrollView = self.RankTableBody.gameObject:GetComponent(typeof(UIScrollView))
	local BodyPanel = self.RankTableBody.gameObject:GetComponent(typeof(UIPanel))
	local hasSetScrollView = false
	local isPressed = false
	local isHorizontal = false
	
	UIEventListener.Get(content.gameObject).onDrag = DelegateFactory.VectorDelegate(function(g,delta)
		local x = delta.x
		local y = delta.y
		if (math.abs(x) < math.abs(y)) then
			isHorizontal = false
		else
			isHorizontal = true
		end
		if not hasSetScrollView then
			hasSetScrollView = true
			BodyScrollView.movement = isHorizontal and Movement.Horizontal or Movement.Vertical
		end
		self.m_HorizontalRankScrollArrow.gameObject:SetActive(false)
		BodyScrollView:Drag()
		headPanel.clipOffset = Vector2(BodyPanel.clipOffset.x,headPanel.clipOffset.y)
		Extensions.SetLocalPositionX(headPanel.transform,BodyPanel.transform.localPosition.x)
	end)
	UIEventListener.Get(content.gameObject).onPress = DelegateFactory.BoolDelegate(function (go, ispress)
		if(isPressed ~= ispress) then hasSetScrollView = false end
		if (not ispress) then
			local bounds = BodyScrollView.bounds
			local clip = BodyPanel.finalClipRegion
			local hx = clip.z == 0 and Screen.width or clip.z * 0.5
			local isEnd = math.floor(bounds.max.x - (clip.x + hx) + 0.5) < 0
			self.m_HorizontalRankScrollArrow.gameObject:SetActive(not isEnd)
		end
		isPressed = ispress
        BodyScrollView:Press(ispress)
    end)

	self.RankTableBody:Clear()
	self.RankTableBody.dataSource = DefaultUISimpleTableViewDataSource.Create(
		function()
			return #self.m_RankData
		end,
		function(index)
			return self:UpdateRankViewItem(index)
		end
	)
end

function LuaStarBiWuJiFenSaiDetailView:InitRankHeader()
	self.m_MaxRankRound = CLuaStarBiwuMgr.m_JiFenSaiTotalRound
	local tableHead = self.RankTableHead.transform:Find("TableHeader/Table")
	local roundTemplate = self.RankTableHead.transform:Find("roundTemplate")
	roundTemplate.gameObject:SetActive(false)
	local childCount = tableHead.childCount
	if childCount < self.m_MaxRankRound + 3 then
		for i = childCount + 1,self.m_MaxRankRound + 3 do
			local go = CUICommonDef.AddChild(tableHead.gameObject,roundTemplate.gameObject)
			go.gameObject:SetActive(true)
		end
	elseif childCount > self.m_MaxRankRound + 3 then
		for i = childCount - 1 ,self.m_MaxRankRound + 3,-1 do
			tableHead:GetChild(i).gameObject:SetActive(false)
		end
	end

	if self.m_MaxRankRound > 0 then
		for i = 4,self.m_MaxRankRound + 3 do
			local round = tableHead:GetChild(i - 1)
			round.gameObject:SetActive(true)
			local roundLabel = round.transform:GetComponent(typeof(UILabel))
			roundLabel.text = SafeStringFormat3(LocalString.GetString("第%d轮"), i - 3)
		end
	end
	tableHead:GetComponent(typeof(UITable)):Reposition()
	local tableHeader = self.RankTableHead.transform:Find("TableHeader"):GetComponent(typeof(UISprite))
	tableHeader.width = 580 + (self.m_MaxRankRound * 230)
	local itemTemplate = self.RankTableBody.transform:Find("Pool/ItemTemplate")
	local line = itemTemplate.transform:Find("Line"):GetComponent(typeof(UISprite))
	line.width = 580 + (self.m_MaxRankRound * 230)
end

function LuaStarBiWuJiFenSaiDetailView:OnTabChange(index)
	if index < 3 then
		-- 请求前三天数据
		Gac2Gas.QueryStarBiwuJifensaiMatchS13(index + 1,self.m_CurSelectGroup,1)
	else
		-- 请求排行榜数据
		Gac2Gas.QueryStarBiwuJifensaiMainRankS13(self.m_CurSelectGroup)
	end
	self.TabTable:ChangeTab(math.max(self.m_CurSelectTabIndex - 1,0),true)
end

function LuaStarBiWuJiFenSaiDetailView:UpdateTabDateTime()
	local timeData = StarBiWuShow_Setting.GetData().JiFenStageTime
	if self.m_CurSelectGroup ~= 1 then
		timeData = StarBiWuShow_Setting.GetData().JiFenStageTimeCommonGroup
	end
	local zone8 = CServerTimeMgr.Inst:GetZone8Time()
    local month, day = zone8.Month, zone8.Day
	local currDayIndex = 1
	for i = 1, 3 do
        local isCurrentDay = month == timeData[(i - 1) * 2] and day == timeData[(i - 1) * 2 + 1]
		self.m_DateTabList[i].tabLabel.text =  SafeStringFormat3(LocalString.GetString("第%d日(%d.%d)"), i,timeData[(i - 1) * 2], timeData[(i - 1) * 2 + 1])
		if isCurrentDay then currDayIndex = i end
    end
	return currDayIndex
end

function LuaStarBiWuJiFenSaiDetailView:UpdateWordFilterButton()
	if not self.m_GroupIndex2Name or not self.m_GroupIndex2Name[self.m_CurSelectGroup] then 
		self.WordFilterButton.gameObject:SetActive(false)
		return
	end
	self.WordFilterButton.gameObject:SetActive(true)
	local btnlabel = self.WordFilterButton.transform:Find("Label"):GetComponent(typeof(UILabel))
	btnlabel.text = self.m_GroupIndex2Name and self.m_GroupIndex2Name[self.m_CurSelectGroup] or ""
end

function LuaStarBiWuJiFenSaiDetailView:OnResultJiFenSaiData(selfZhanduiId,groupId,dayIndexId)
	g_ScriptEvent:BroadcastInLua("OnStarBiWuDetailInfoWndChange",SafeStringFormat3(LocalString.GetString("%s积分赛"),self.m_GroupIndex2Name[groupId] or ""))
	self.m_CurSelectTabIndex = dayIndexId
	self.m_CurSelectGroup = groupId
	self.m_SelfZhanDuiId = selfZhanduiId
	self:UpdateWordFilterButton()
	self:UpdateTabDateTime()
	self.TabTable:ChangeTab(math.max(self.m_CurSelectTabIndex - 1,0),true)
	local matchData = CLuaStarBiwuMgr.m_JiFenSaiMatchInfo
	if #matchData <= 0 then
		if self.m_CurSelectGroup == 1 then
			self.Empty.text = g_MessageMgr:FormatMessage("Star_BiWu_JiFenSai_Group_Empty_Top")	-- 巅峰组
		else
			self.Empty.text = g_MessageMgr:FormatMessage("Star_BiWu_JiFenSai_Group_Empty_Common") -- 普通组
		end
		
		self.Empty.gameObject:SetActive(true)
		self.JingCaiButton.gameObject:SetActive(false)
		self.GuanZhan.gameObject:SetActive(false)
		self.Rank.gameObject:SetActive(false)
		return
	else
		self.Empty.gameObject:SetActive(false)
		self.GuanZhan.gameObject:SetActive(true)
		self.Rank.gameObject:SetActive(false)
		self.JingCaiButton.gameObject:SetActive(true)
	end
	self:UpdateGuanZhanView(matchData)
end

function LuaStarBiWuJiFenSaiDetailView:OnResultJiFenSaiRankData(selfZhanduiId,selfGroupId,groupId)
	g_ScriptEvent:BroadcastInLua("OnStarBiWuDetailInfoWndChange",SafeStringFormat3(LocalString.GetString("%s积分赛"),self.m_GroupIndex2Name[groupId] or ""))
	self.m_CurSelectTabIndex = 4
	self.m_CurSelectGroup = groupId
	self.m_SelfZhanDuiId = selfZhanduiId
	self:UpdateWordFilterButton()
	self:UpdateTabDateTime()
	self.TabTable:ChangeTab(self.m_CurSelectTabIndex - 1,true)
	local rankData = CLuaStarBiwuMgr.m_JiFenSaiRankInfo
	if #rankData <= 0 then
		if self.m_CurSelectGroup == 1 then
			self.Empty.text = g_MessageMgr:FormatMessage("Star_BiWu_JiFenSai_Rank_Empty_Top")	-- 巅峰组
		else
			self.Empty.text = g_MessageMgr:FormatMessage("Star_BiWu_JiFenSai_Rank_Empty_Common") -- 普通组
		end
		
		self.Empty.gameObject:SetActive(true)
		self.JingCaiButton.gameObject:SetActive(false)
		self.GuanZhan.gameObject:SetActive(false)
		self.Rank.gameObject:SetActive(false)
		return
	else
		self.Empty.gameObject:SetActive(false)
		self.GuanZhan.gameObject:SetActive(false)
		self.Rank.gameObject:SetActive(true)
		self.JingCaiButton.gameObject:SetActive(true)
	end
	table.sort(rankData,function (a,b)
		if a.winCount > b.winCount then
			return true
		elseif a.winCount == b.winCount then
			if a.winTime == b.winTime then
				return a.zhanduiId < b.zhanduiId
			else
				return a.winTime < b.winTime
			end
		else
			return false
		end
	end)
	local headPanel = self.RankTableHead.gameObject:GetComponent(typeof(UIPanel))
	local BodyScrollView = self.RankTableBody.gameObject:GetComponent(typeof(UIScrollView))
	local BodyPanel = self.RankTableBody.gameObject:GetComponent(typeof(UIPanel))
	BodyScrollView.movement = Movement.Horizontal
	BodyScrollView:ResetPosition()
	BodyScrollView.movement = Movement.Vertical
	headPanel.clipOffset = Vector2(BodyPanel.clipOffset.x,headPanel.clipOffset.y)
	Extensions.SetLocalPositionX(headPanel.transform,BodyPanel.transform.localPosition.x)
	self:InitRankHeader()
	self:UpdateRankView(rankData)
end

function LuaStarBiWuJiFenSaiDetailView:UpdateGuanZhanView(matchsData)
	self.m_GuanZhanData = {}
	local matchRoundTime = StarBiWuShow_Setting.GetData().JiFenSaiStartTimeShow
	for i = 1,#matchsData do
		local time = ""
		local matchData = matchsData[i]
		time = matchRoundTime[(matchData.round - 1) % matchRoundTime.Length]
		table.insert(self.m_GuanZhanData,{
			type = 1,
			round = matchData.round,
			time = time,
		})
		for j = 1,#matchData.matchs,2 do
			local matchItem1Info = matchData.matchs[j]
			local matchItem1 = matchItem1Info and {
				rank = j;
				index = matchItem1Info.index,
				attackId = matchItem1Info.attackId,
				defendId = matchItem1Info.defendId,
				attackName = matchItem1Info.attackName,
				defendName = matchItem1Info.defendName,
				attackScore = matchItem1Info.attackScore,
				defendScore = matchItem1Info.defendScore,
				winnerId = matchItem1Info.winnerId,
				status = matchItem1Info.status, -- 0 未开始 1 进行中 2 已结束
			} or nil
			local matchItem2Info = matchData.matchs[j + 1]
			local matchItem2 =  matchItem2Info and {
				rank = j + 1;
				index = matchItem2Info.index,
				attackId = matchItem2Info.attackId,
				defendId = matchItem2Info.defendId,
				attackName = matchItem2Info.attackName,
				defendName = matchItem2Info.defendName,
				attackScore = matchItem2Info.attackScore,
				defendScore = matchItem2Info.defendScore,
				winnerId = matchItem2Info.winnerId,
				status = matchItem2Info.status, -- 0 未开始 1 进行中 2 已结束
			} or nil
			table.insert(self.m_GuanZhanData,{
				type = 2,
				round = matchData.round,
				item1 = matchItem1,
				item2 = matchItem2,
			})
		end
		if i ~= #matchsData then
			table.insert(self.m_GuanZhanData,{
				type = 3,
				round = matchData.round,
			})
		end
	end
    self.GuanZhanTableBody:LoadData(0, false)
end

function LuaStarBiWuJiFenSaiDetailView:UpdateGuanZhanItem(roundIndex)
	local data = self.m_GuanZhanData[roundIndex+1]
	if data.type == 1 then
		local RoundItem = self.GuanZhanTableBody:DequeueReusableCellWithIdentifier("RoundItem")
		if RoundItem == nil then
			RoundItem = self.GuanZhanTableBody:AllocNewCellWithIdentifier(self.m_RoundItem,"RoundItem")
		end
		local nameLabel = RoundItem.transform:Find("RoundTitle"):GetComponent(typeof(UILabel))
		nameLabel.text = SafeStringFormat3(LocalString.GetString("第%s轮 %s"),self:GetRoundStr(data.round),data.time)
		return RoundItem
	elseif data.type == 2 then
		local matchItem = self.GuanZhanTableBody:DequeueReusableCellWithIdentifier("ItemTemplate")
		if matchItem == nil then
			matchItem = self.GuanZhanTableBody:AllocNewCellWithIdentifier(self.m_MatchItemTemplate,"ItemTemplate")
		end
		local item1 = matchItem.transform:Find("Item1"):GetComponent(typeof(CCommonLuaScript))
		local item2 = matchItem.transform:Find("Item2"):GetComponent(typeof(CCommonLuaScript))
		item1.m_LuaSelf:Init(data.item1,self.m_SelfZhanDuiId)
		item2.m_LuaSelf:Init(data.item2,self.m_SelfZhanDuiId)
		return matchItem
	else
		Splititem = self.GuanZhanTableBody:DequeueReusableCellWithIdentifier("Split")
		if Splititem == nil then
			Splititem = self.GuanZhanTableBody:AllocNewCellWithIdentifier(self.m_SplitItem,"Split")
		end
		return Splititem
	end
end

function LuaStarBiWuJiFenSaiDetailView:GetRoundStr(round)
	if round <= 10 then
		return EnumNumber.GetNum(round)
	elseif round < 20 then
		return SafeStringFormat3(LocalString.GetString("十%s"),EnumNumber.GetNum(round % 10))
	end
end

function LuaStarBiWuJiFenSaiDetailView:UpdateRankView(rankData)
	self.m_RankData = rankData

    self.RankTableBody:LoadData(0, false)
end

function LuaStarBiWuJiFenSaiDetailView:UpdateRankViewItem(index)
	local data = self.m_RankData[index+1]
	local item = self.RankTableBody:DequeueReusableCellWithIdentifier("RankTemplate")
	if item == nil then
		item = self.RankTableBody:AllocNewCellWithIdentifier(self.m_RankItem,"RankTemplate")
	end
	local itemtable = item.transform:Find("Table")
	local childCount = itemtable.childCount
	if childCount < self.m_MaxRankRound + 4 then
		for i = childCount + 1,self.m_MaxRankRound + 4 do
			local go = CUICommonDef.AddChild(itemtable.gameObject,self.m_RankStatusItemTemplate.gameObject)
			go.gameObject:SetActive(true)
		end
	elseif childCount > self.m_MaxRankRound + 4 then
		for i = childCount - 1,self.m_MaxRankRound + 4,-1 do
			itemtable:GetChild(i).gameObject:SetActive(false)
		end
	end
	
	local matchInfo = data.roundRank
	local rank = itemtable.transform:Find("rank"):GetComponent(typeof(UILabel))
	local rankImage = itemtable.transform:Find("rank/rankImage"):GetComponent(typeof(UISprite))
	local name = itemtable.transform:Find("name"):GetComponent(typeof(UILabel))
	local score = itemtable.transform:Find("score"):GetComponent(typeof(UILabel))
	local time = itemtable.transform:Find("time"):GetComponent(typeof(UILabel))
	local isHasResult = data.winCount > 0 or data.loseCount > 0
	rank.text = tostring(index + 1)
	name.text = data.zhanduiName
	if isHasResult then
		score.text = SafeStringFormat3(LocalString.GetString("%d[32c98c]胜[-]%d[ff6565]负[-]"),data.winCount,data.loseCount)
		rankImage.color =  index + 1 <= 8 and NGUIText.ParseColor24("1f5b43", 0) or NGUIText.ParseColor24("613d50", 0)
		time.text = SafeStringFormat3("%02d:%02d",math.floor(data.winTime / 60),data.winTime % 60)
	else
		score.text = "--"
		time.text = "--"
		rankImage.color = NGUIText.ParseColor24("14294C", 0)
	end
	for i = 4,self.m_MaxRankRound + 3 do
		local statusitem = itemtable:GetChild(i).gameObject
		statusitem.gameObject:SetActive(true)
		local statusBg = statusitem:GetComponent(typeof(UISprite))
		local name = statusitem.transform:Find("Label"):GetComponent(typeof(UILabel))
		name.text = matchInfo[i - 3].zhanduiName
		statusBg.color = NGUIText.ParseColor24("324A6E", 0)
		if isHasResult then
			if matchInfo[i - 3].WinFlag == 1 then
				statusBg.color = NGUIText.ParseColor24("1f5b43", 0)
			elseif matchInfo[i - 3].WinFlag == 0 then
				statusBg.color = NGUIText.ParseColor24("613d50", 0)
			end
		end
	end
	itemtable:GetComponent(typeof(UITable)):Reposition()
	local line = item.transform:Find("Line"):GetComponent(typeof(UISprite))
	line.width = 580 + (self.m_MaxRankRound * 230)
	return item
	-- local bg = item:GetComponent(typeof(UISprite))
	-- bg.spriteName = index % 2 == 0 and "common_bg_mission_background_n" or "common_bg_mission_background_s"
end

--@region UIEvent

function LuaStarBiWuJiFenSaiDetailView:OnTipButtonClick()
	g_MessageMgr:ShowMessage("StarBiWu_JiFenSai_Rule_Tip")
end

function LuaStarBiWuJiFenSaiDetailView:OnJingCaiButtonClick()
	CLuaScheduleMgr.UpdateAlertStatus(StarBiWuShow_Setting.GetData().MatchServerSchedule,LuaEnumAlertState.Clicked)
	CUIManager.ShowUI(CLuaUIResources.StarBiwuJingCaiWnd)
end

function LuaStarBiWuJiFenSaiDetailView:OnWordFilterButtonClick()
	self.WordFilterButton:SetTipStatus(false)
	CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenuWithdefaultSelectedIndex(Table2Array(self.m_FilterActions, MakeArrayClass(PopupMenuItemData)), self.m_CurSelectGroup - 1, self.WordFilterButton.transform, CPopupMenuInfoMgrAlignType.Bottom, 1, nil, nil, DelegateFactory.Action(function ()
        self.WordFilterButton:SetTipStatus(true)
    end), 600,300)
end
--@endregion UIEvent

function LuaStarBiWuJiFenSaiDetailView:OnEnable()
	-- 服务器返回数据监听
	g_ScriptEvent:AddListener("OnSyncStarBiwuJifesanSaiGroupS13", self, "OnGetGroupInfo")
	g_ScriptEvent:AddListener("OnStarBiWuJiFenSaiResultData", self, "OnResultJiFenSaiData")
	g_ScriptEvent:AddListener("OnStarBiWuJiFenSaiRankData", self, "OnResultJiFenSaiRankData")
	if not self.m_GroupIndex2Name then
		Gac2Gas.QueryStarBiwuJifesanSaiGroupS13()
		-- 请求分组情况
	end
	if self.m_GroupIndex2Name then
		g_ScriptEvent:BroadcastInLua("OnStarBiWuDetailInfoWndChange",SafeStringFormat3(LocalString.GetString("%s积分赛"),self.m_GroupIndex2Name[self.m_CurSelectGroup] or ""))
	end
end

function LuaStarBiWuJiFenSaiDetailView:OnDisable()
	g_ScriptEvent:RemoveListener("OnSyncStarBiwuJifesanSaiGroupS13", self, "OnGetGroupInfo")
	g_ScriptEvent:RemoveListener("OnStarBiWuJiFenSaiResultData", self, "OnResultJiFenSaiData")
	g_ScriptEvent:RemoveListener("OnStarBiWuJiFenSaiRankData", self, "OnResultJiFenSaiRankData")
end

