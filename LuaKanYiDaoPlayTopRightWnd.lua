local UILabel            = import "UILabel"
local DelegateFactory    = import "DelegateFactory"
local CButton            = import "L10.UI.CButton"
local UITable            = import "UITable"
local GameObject         = import "UnityEngine.GameObject"
local Color              = import "UnityEngine.Color"
local NGUIText           = import "NGUIText"
local CTopAndRightTipWnd = import "L10.UI.CTopAndRightTipWnd"

LuaKanYiDaoPlayTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaKanYiDaoPlayTopRightWnd, "ExpandButton", "ExpandButton", CButton)
RegistChildComponent(LuaKanYiDaoPlayTopRightWnd, "TaskTable", "TaskTable", UITable)
RegistChildComponent(LuaKanYiDaoPlayTopRightWnd, "TaskTemplate", "TaskTemplate", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaKanYiDaoPlayTopRightWnd, "tableLabels")
RegistClassMember(LuaKanYiDaoPlayTopRightWnd, "baseLabels")

function LuaKanYiDaoPlayTopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)

	self.TaskTemplate:SetActive(false)
    --@endregion EventBind end
end

function LuaKanYiDaoPlayTopRightWnd:Init()
	self:InitTaskInfo()
end

function LuaKanYiDaoPlayTopRightWnd:OnEnable()
	g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
	g_ScriptEvent:AddListener("SyncKanYiDaoTaskInfo", self, "OnDataUpdate")
end

function LuaKanYiDaoPlayTopRightWnd:OnDisable()
	g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncKanYiDaoTaskInfo", self, "OnDataUpdate")
end

function LuaKanYiDaoPlayTopRightWnd:OnDataUpdate()
	self:UpdateTaskInfo()
end

function LuaKanYiDaoPlayTopRightWnd:OnHideTopAndRightTipWnd()
    LuaUtils.SetLocalRotation(self.ExpandButton.transform, 0, 0, 180)
end

--@region UIEvent

function LuaKanYiDaoPlayTopRightWnd:OnExpandButtonClick()
	LuaUtils.SetLocalRotation(self.ExpandButton.transform, 0, 0, 0)
	CTopAndRightTipWnd.showPackage = false
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

-- 初始化任务信息
function LuaKanYiDaoPlayTopRightWnd:InitTaskInfo()
	Extensions.RemoveAllChildren(self.TaskTable.transform)
	self.tableLabels = {}
	for id = 1, 3 do
		local taskItem = CUICommonDef.AddChild(self.TaskTable.gameObject, self.TaskTemplate)
		local taskItemTF = taskItem.transform
		local textLabel = taskItemTF:Find("TextLabel"):GetComponent(typeof(UILabel))
		local numLabel = taskItemTF:Find("NumLabel"):GetComponent(typeof(UILabel))
		local fx = taskItemTF:Find("Fx"):GetComponent(typeof(CUIFx))
		taskItem:SetActive(true)
		table.insert(self.tableLabels, {textLabel = textLabel, numLabel = numLabel, fx = fx, isInProgress = false})
	end
	self.TaskTable:Reposition()
	self.baseLabels = {}
	-- 待读表
	for id = 1, 3 do
		table.insert(self.baseLabels, {text = Double11_KanYiDaoTask.GetData(id).Name,
										num = Double11_KanYiDaoTask.GetData(id).TotalNum})
	end
	self:UpdateTaskInfo()

	-- 未收到服务器数据时进行初始化
	if not LuaShuangshiyi2021Mgr.KanYiDaoTaskTable then
		for id = 1, 3 do
			local tableLabel = self.tableLabels[id]
			tableLabel.textLabel.text = self.baseLabels[id].text
			tableLabel.numLabel.text = SafeStringFormat3("%d/%d", 0, self.baseLabels[id].num)
		end
	end
end

-- 更新任务信息
function LuaKanYiDaoPlayTopRightWnd:UpdateTaskInfo()
	local taskTable = LuaShuangshiyi2021Mgr.KanYiDaoTaskTable
	if not taskTable then return end
	local hasTaskInProgress = false
	for id, data in pairs(taskTable) do
		local finishTimes = data.finishTimes
		local totalTimes = self.baseLabels[id].num
		local tableLabel = self.tableLabels[id]
		-- 修改颜色
		if finishTimes == totalTimes then
			tableLabel.textLabel.text = LocalString.GetString("[s]"..self.baseLabels[id].text.."[/s]")
			tableLabel.numLabel.text = SafeStringFormat3("[s]%d/%d[/s]", finishTimes, totalTimes)
			local finishColor = NGUIText.ParseColor24("00FF60", 0)
			tableLabel.textLabel.color = finishColor
			tableLabel.numLabel.color = finishColor
		else
			tableLabel.textLabel.text = self.baseLabels[id].text
			tableLabel.numLabel.text = SafeStringFormat3("%d/%d", finishTimes, totalTimes)
			tableLabel.textLabel.color = Color.white
			tableLabel.numLabel.color = Color.white
		end
		-- 正在进行的任务底下有粒子流动特效
		if hasTaskInProgress == false then
			if finishTimes == 0 or (finishTimes > 0 and finishTimes < totalTimes) then
				hasTaskInProgress = true
				if not tableLabel.isInProgress then
					tableLabel.isInProgress = true
					tableLabel.fx:LoadFx(g_UIFxPaths.TaskInProgressFxPath)
				end
			else
				tableLabel.isInProgress = false
				tableLabel.fx:DestroyFx()
			end
		else
			tableLabel.isInProgress = false
			tableLabel.fx:DestroyFx()
			-- 未开始的任务透明度为30%
			if finishTimes == 0 then
				tableLabel.textLabel.alpha = 0.3
				tableLabel.numLabel.alpha = 0.3
			end
		end
	end
end

--@endregion UIEvent

