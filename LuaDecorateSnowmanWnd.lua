local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local CButton = import "L10.UI.CButton"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local UIEventListener = import "UIEventListener"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"

LuaDecorateSnowmanWnd = class()

RegistChildComponent(LuaDecorateSnowmanWnd,"m_ScrollView", "ScrollView",CUIRestrictScrollView)
RegistChildComponent(LuaDecorateSnowmanWnd,"m_Template","Template", GameObject)
RegistChildComponent(LuaDecorateSnowmanWnd,"m_Grid","Grid", UIGrid)
RegistChildComponent(LuaDecorateSnowmanWnd,"m_Button","Button01",CButton)
RegistChildComponent(LuaDecorateSnowmanWnd,"m_Button02","Button02",CButton)
RegistChildComponent(LuaDecorateSnowmanWnd,"m_FakeModel","FakeModel",UITexture)

RegistClassMember(LuaDecorateSnowmanWnd,"m_CurSelectItem")
RegistClassMember(LuaDecorateSnowmanWnd,"m_ModelTextureLoader")
RegistClassMember(LuaDecorateSnowmanWnd,"m_ModelName")

function LuaDecorateSnowmanWnd:Init()
    self.m_ModelName = "_DecorateSnowmanPreview_"

    UIEventListener.Get(self.m_Button.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:OnDecorateButtonClick()
    end)

    UIEventListener.Get(self.m_Button02.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:OnGetButtonClick()
    end)

    local data = Zhuangshiwu_Zhuangshiwu.GetData(HanJia2020_Setting.GetData().FinishedSnowMan)
    self:ReloadModel(data)

    self:InitItems()
end

function LuaDecorateSnowmanWnd:OnEnable()
    g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
end

function LuaDecorateSnowmanWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
end

function LuaDecorateSnowmanWnd:OnDestroy()
    CUIManager.DestroyModelTexture(self.m_ModelName)
end

function LuaDecorateSnowmanWnd:OnSetItemAt( args ) 
    local  place, position, oldItemId, newItemId = args[0],args[1],args[2],args[3]
    local item = CItemMgr.Inst:GetById(newItemId)
    if item then
        local templateId = item.TemplateId
        local arr = HanJia2020_Setting.GetData().DecorationItemId
        for i = 0, arr.Length - 1 do
            if arr[i] == templateId then
                self:InitItems()
                break
            end
        end
    end
end

--@region 初始化雪人列表

function LuaDecorateSnowmanWnd:InitItems()
    self.m_Template:SetActive(false)

    local arr = HanJia2020_Setting.GetData().DecoratedSnowMan
    Extensions.RemoveAllChildren(self.m_Grid.transform)
    for i = 0, arr.Length - 1 do
        local go = NGUITools.AddChild(self.m_Grid.gameObject, self.m_Template)
        go:SetActive(true)
        local zhuangshiwuID =  arr[i]
        local data = Zhuangshiwu_Zhuangshiwu.GetData(zhuangshiwuID)
        local item = self:InitItem(go, data, HanJia2020_Setting.GetData().DecorationItemId[i])

        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function()
            self:OnChoose(item)
        end)
    end

    self.m_Grid:Reposition()
    self.m_ScrollView:ResetPosition()
end

function LuaDecorateSnowmanWnd:InitItem(go, zswdata, ItemId)
    local item = {}
    item.ItemId = ItemId
    item.centerLabel = go.transform:Find("CenterLabel"):GetComponent(typeof(UILabel))
    item.centerLabel.enabled = false
    item.mask = go.transform:Find("Mask"):GetComponent(typeof(UISprite))
    item.mask.enabled = false
    item.tex = go.transform:Find("Texture"):GetComponent(typeof(CUITexture))
    item.selectSprite = go.transform:Find("Select"):GetComponent(typeof(UISprite))
    item.selectSprite.enabled = false
    local itemData = Item_Item.GetData(zswdata.ItemId)
    if itemData then
        item.tex:LoadMaterial(itemData.Icon)
    end
    item.zswsdata = zswdata
    item.name = Item_Item.GetData(ItemId).Name
    local num = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, ItemId)
    if num == 0 then
        item.mask.enabled = true
        item.centerLabel.enabled = true
    end
    return item
end

function LuaDecorateSnowmanWnd:ShowItemAccessInfo(item)
    CItemAccessListMgr.Inst:ShowItemAccessInfo(item.ItemId, false, item.centerLabel.transform, CTooltipAlignType.Right)
end

function LuaDecorateSnowmanWnd:OnChoose(item)
    self.m_Button.gameObject:SetActive(not item.centerLabel.enabled)
    self.m_Button02.gameObject:SetActive(item.centerLabel.enabled)

    if self.m_CurSelectItem and self.m_CurSelectItem ~= item then
        self.m_CurSelectItem.selectSprite.enabled = false
    end

    self.m_CurSelectItem = item
    self.m_CurSelectItem.selectSprite.enabled = true

    self:ReloadModel(item.zswsdata)
end

--@endregion
function LuaDecorateSnowmanWnd:OnGetButtonClick()
    local item = self.m_CurSelectItem
    self:ShowItemAccessInfo(item)
end

function LuaDecorateSnowmanWnd:OnDecorateButtonClick()
    if self.m_CurSelectItem then
        local msg = g_MessageMgr:FormatMessage("Decorate_Snowman_Confirm", self.m_CurSelectItem.name)
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
            local ItemId = self.m_CurSelectItem.ItemId
            local default, pos, id = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, ItemId)
            Gac2Gas.RequestDecorateSnowman(HanJia2020Mgr.decorateSnowManID, id, pos)
        end), nil, LocalString.GetString("确认"), LocalString.GetString("取消"), false)
    else
        g_MessageMgr:ShowMessage("HanJia2020_No_ZhuangShiWu_Decorate")
    end
end


--@region 预览
function LuaDecorateSnowmanWnd:LoadModel(ro, prefabname)
    ro:LoadMain(prefabname, nil, false, false)
end

function LuaDecorateSnowmanWnd:ReloadModel(zswdata)

    local prefabname = "Assets/Res/Character/Jiayuan/" .. zswdata.ResName .. "/Prefab/" .. zswdata.ResName .."_"..SafeStringFormat3("%02d",zswdata.ResId) ..".prefab"
    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadModel(ro, prefabname)
    end)
    self.m_FakeModel.mainTexture = CUIManager.CreateModelTexture(self.m_ModelName, self.m_ModelTextureLoader,180,0,-0.71,4.66,false,true,1,true)
end
--@endregion