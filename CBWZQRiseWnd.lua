-- Auto Generated!!
local CBWZQMgr = import "L10.Game.CBWZQMgr"
local CBWZQRiseWnd = import "L10.UI.CBWZQRiseWnd"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local UIEventListener = import "UIEventListener"
CBWZQRiseWnd.m_Init_CS2LuaHook = function (this) 
    this.moneyCtrl:SetCost(CBWZQMgr.Inst.baomingFee)
    this.moneyCtrl:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, true)

    UIEventListener.Get(this.submitBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        if this.moneyCtrl.moneyEnough then
            CUIManager.ShowUI(CUIResources.BWZQAttendWnd)
            this:Close()
        else
            g_MessageMgr:ShowMessage("SILVER_NOT_ENOUGH")
        end
    end)

    UIEventListener.Get(this.cancelBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)
end
