require("common/common_include")

local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CBaseWnd = import "L10.UI.CBaseWnd"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"


LuaChristmasSendGiftWnd = class()

RegistClassMember(LuaChristmasSendGiftWnd, "m_CloseButton")
RegistClassMember(LuaChristmasSendGiftWnd, "m_TimesLabel")
RegistClassMember(LuaChristmasSendGiftWnd, "m_GiftTable")
RegistClassMember(LuaChristmasSendGiftWnd, "m_GiftTemplate")
RegistClassMember(LuaChristmasSendGiftWnd, "m_MoneyCtrl")
RegistClassMember(LuaChristmasSendGiftWnd, "m_SendButton")

RegistClassMember(LuaChristmasSendGiftWnd, "m_GiftId2ItemObjTbl")
RegistClassMember(LuaChristmasSendGiftWnd, "m_SelectedGiftId")


function LuaChristmasSendGiftWnd:Init()

	self.m_CloseButton = self.transform:Find("Wnd_Bg_Secondary_2/CloseButton").gameObject
	self.m_TimesLabel = self.transform:Find("Anchor/TimesLabel"):GetComponent(typeof(UILabel))
	self.m_GiftTable = self.transform:Find("Anchor/Table"):GetComponent(typeof(UITable))
	self.m_GiftTemplate = self.transform:Find("Anchor/Item").gameObject
	self.m_MoneyCtrl = self.transform:Find("Anchor/MoneyCtrl"):GetComponent(typeof(CCurentMoneyCtrl))
	self.m_SendButton = self.transform:Find("Anchor/SendButton"):GetComponent(typeof(CButton))

	CommonDefs.AddOnClickListener(self.m_CloseButton, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)
	CommonDefs.AddOnClickListener(self.m_SendButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnSendButtonClick() end), false)

	self.m_GiftTemplate:SetActive(false)
	self.m_GiftId2ItemObjTbl = {}
	Extensions.RemoveAllChildren(self.m_GiftTable.transform)
	
	local firstGo = nil

	ShengDan_Gift.Foreach(function (id, data)
		local instance = CUICommonDef.AddChild(self.m_GiftTable.gameObject, self.m_GiftTemplate)
		instance:SetActive(true)

		local iconTexture = instance.transform:Find("Cell/IconTexture"):GetComponent(typeof(CUITexture))
		local itemNameLabel = instance.transform:Find("Cell/ItemNameLabel"):GetComponent(typeof(UILabel))
		local freeLabel = instance.transform:Find("Cell/FreeLabel"):GetComponent(typeof(UILabel))
		local costLabel = instance.transform:Find("Cell/Cost"):GetComponent(typeof(UILabel))

		iconTexture:LoadMaterial(Item_Item.GetData(ShengDan_Gift.GetData(id).ItemId).Icon)
		itemNameLabel.text = data.Desc
		costLabel.text = data.Jade

		CommonDefs.AddOnClickListener(instance, DelegateFactory.Action_GameObject(function(go) self:OnItemClick(go) end), false)
		CommonDefs.AddOnClickListener(iconTexture.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnItemIconClick(instance, iconTexture.gameObject) end), false)

		table.insert(self.m_GiftId2ItemObjTbl, id, instance)

		if not firstGo then firstGo = instance end
	end)
	self.m_SelectedGiftId = 0
	self:InitDisplay()
	if firstGo then --默认选中第一个
		self:OnItemClick(firstGo)
	end
end

function LuaChristmasSendGiftWnd:InitDisplay()
	if not self.m_GiftId2ItemObjTbl then return end

	local mainplayer = CClientMainPlayer.Inst
	if not mainplayer then return end
	local sendGiftTimes = mainplayer.PlayProp:GetSendChristmasGiftTimes()
	local sendSmallGiftTimes = mainplayer.PlayProp:GetSendChristmasSmallGiftTimes()
	local maxSendTimes = ShengDan_Setting.GetData().MaxSendGiftCount
	self.m_TimesLabel.text = SafeStringFormat3("%d/%d", sendGiftTimes, maxSendTimes)

	for id,instance in pairs(self.m_GiftId2ItemObjTbl) do
		local freeLabel = instance.transform:Find("Cell/FreeLabel"):GetComponent(typeof(UILabel))
		local costLabel = instance.transform:Find("Cell/Cost"):GetComponent(typeof(UILabel))

		local showFreeLabel = false
		if self:IsSmallGift(id) and sendSmallGiftTimes==0 then
			showFreeLabel = true
		end

		freeLabel.gameObject:SetActive(showFreeLabel)
		costLabel.gameObject:SetActive(not showFreeLabel)
	end

	self:OnSelectGift(self.m_SelectedGiftId)
end

function LuaChristmasSendGiftWnd:OnSelectGift(giftId)
	local mainplayer = CClientMainPlayer.Inst
	if not mainplayer then return end
	local sendGiftTimes = mainplayer.PlayProp:GetSendChristmasGiftTimes()
	local sendSmallGiftTimes = mainplayer.PlayProp:GetSendChristmasSmallGiftTimes()
	local maxSendTimes = ShengDan_Setting.GetData().MaxSendGiftCount
	self.m_SelectedGiftId = giftId
	local cost = 0
	if giftId and giftId>0 then
		cost = ShengDan_Gift.GetData(giftId).Jade
		if self:IsSmallGift(giftId) and sendSmallGiftTimes==0 then
			cost = 0
		end
	end
	self.m_MoneyCtrl:SetCost(cost)
	self.m_SendButton.Enabled = (sendGiftTimes < maxSendTimes) and self.m_SelectedGiftId and self.m_SelectedGiftId > 0
end

function LuaChristmasSendGiftWnd:IsSmallGift(giftId)
	return giftId and giftId == 1
end

function LuaChristmasSendGiftWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaChristmasSendGiftWnd:OnSendButtonClick()
	if not self.m_SelectedGiftId or self.m_SelectedGiftId == 0 then return end
	Gac2Gas.RequestSendChistmasGiftToHouse(self.m_SelectedGiftId)
end

function LuaChristmasSendGiftWnd:OnItemClick(go)
	if not self.m_GiftId2ItemObjTbl then return end
	for k,v in pairs(self.m_GiftId2ItemObjTbl) do

		local cb = v:GetComponent(typeof(CButton))
		local selected = (v==go)
		cb.Selected = selected
		if selected then
			self:OnSelectGift(k)
		end
	end
end

function LuaChristmasSendGiftWnd:OnItemIconClick(itemGo, itemIconGo)
	if not self.m_GiftId2ItemObjTbl then return end
	for k,v in pairs(self.m_GiftId2ItemObjTbl) do

		local cb = v:GetComponent(typeof(CButton))
		local selected = (v==itemGo)
		cb.Selected = selected
		if selected then
			self:OnSelectGift(k)
			CItemInfoMgr.ShowLinkItemTemplateInfo(ShengDan_Gift.GetData(k).ItemId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
		end
	end
end

function LuaChristmasSendGiftWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "UpdateTempPlayTimesWithKey")
end

function LuaChristmasSendGiftWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "UpdateTempPlayTimesWithKey")
end

function LuaChristmasSendGiftWnd:UpdateTempPlayTimesWithKey(args)
	if not args or args.Length~=3 then return end
	--粗糙一点，不做具体key的判断了
	self:InitDisplay()
end
