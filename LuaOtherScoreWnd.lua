local LuaGameObject=import "LuaGameObject"
local Shop_PlayScore=import "L10.Game.Shop_PlayScore"
local Gac2Gas=import "L10.Game.Gac2Gas"
local MessageMgr=import "L10.Game.MessageMgr"
local CBiWuDaHuiMgr=import "L10.Game.CBiWuDaHuiMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"

CLuaOtherScoreWnd=class()
RegistClassMember(CLuaOtherScoreWnd,"m_ItemTempalte")
RegistClassMember(CLuaOtherScoreWnd,"m_DataList")
RegistClassMember(CLuaOtherScoreWnd,"m_DataGoList")
RegistClassMember(CLuaOtherScoreWnd,"m_CacheParam1")
RegistClassMember(CLuaOtherScoreWnd,"m_CacheParam2")

function CLuaOtherScoreWnd:Init()
    self.m_ItemTempalte=LuaGameObject.GetChildNoGC(self.transform,"ItemTemplate").gameObject
    self.m_ItemTempalte:SetActive(false)

    local parent=LuaGameObject.GetChildNoGC(self.transform,"ItemGrid").transform
    local keys={}
    Shop_PlayScore.ForeachKey(DelegateFactory.Action_object(function(key)
        local data=Shop_PlayScore.GetData(key)
        if data and data.ForbidShow==0 and data.CanShop==0 then
            table.insert(keys, key)
        end
    end))
    self.m_DataList={}
    self.m_DataGoList={}
    local function OnClickButton(go)
        for key, value in pairs(self.m_DataGoList) do
            local btn= LuaGameObject.GetChildNoGC(value.transform,"Button").gameObject
            if btn==go then
                local data=self.m_DataList[key]
                if data.Key=="BIWU" then
                    CUIManager.ShowUI(CUIResources.BWDHScoreWnd);
                end
            end
        end
    end
    local function OnClickTip(go)
        for key, value in pairs(self.m_DataGoList) do
            local btn= LuaGameObject.GetChildNoGC(value.transform,"TipButton").gameObject
            if btn==go then
                local data=self.m_DataList[key]
                if data.Key=="GIFT" then
                    MessageMgr.Inst:ShowMessage(data.TipMessage, {self.m_CacheParam1, self.m_CacheParam2});
                else
                    MessageMgr.Inst:ShowMessage(data.TipMessage,{});
                end
            end
        end
    end
    for i=1,#keys do
        local data=Shop_PlayScore.GetData(keys[i])
        table.insert(self.m_DataList, data)
        local go=NGUITools.AddChild(parent.gameObject,self.m_ItemTempalte)
        table.insert(self.m_DataGoList, go)

        go:SetActive(true)
        LuaGameObject.GetChildNoGC(go.transform,"Icon").cTexture:LoadMaterial(data.ScoreIcon)
        if data.Key=="BIWU" then
            LuaGameObject.GetChildNoGC(go.transform,"DisplayNameLabel").label.text = SafeStringFormat3(LocalString.GetString("比武大会%s"),CBiWuDaHuiMgr.Inst:GetMyStageDesc())
        else
            LuaGameObject.GetChildNoGC(go.transform,"DisplayNameLabel").label.text=data.ScoreName
        end

        LuaGameObject.GetChildNoGC(go.transform,"ValueLabel").label.text=" "

        if(data.TipMessage~="") then
            local g = LuaGameObject.GetChildNoGC(go.transform,"TipButton").gameObject
            g:SetActive(true)
            UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
                OnClickTip(go)
            end)
        else
            LuaGameObject.GetChildNoGC(go.transform,"TipButton").gameObject:SetActive(false)
        end
        local g = LuaGameObject.GetChildNoGC(go.transform,"Button").gameObject
        UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
            OnClickButton(go)
        end)

        if data.ShowButton>0 then
            LuaGameObject.GetChildNoGC(go.transform,"Button").gameObject:SetActive(true)
        else
            LuaGameObject.GetChildNoGC(go.transform,"Button").gameObject:SetActive(false)
        end
        if data.Key=="BIWU" then
            Gac2Gas.QueryBiWuScore()
        elseif data.Key=="GIFT" then
            Gac2Gas.RequestGiftLimit()
        elseif data.Key =="XZS" then
          if CClientMainPlayer.Inst then
            local score = CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.XZS)
            LuaGameObject.GetChildNoGC(go.transform,"ValueLabel").label.text=
                SafeStringFormat3(LocalString.GetString("[c][FFFFFF]当前[-][/c] %d"),score)
          end
        end
    end
end
function CLuaOtherScoreWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryPlayerCurrentBiWuScore", self, "OnQueryPlayerCurrentBiWuScore")
    g_ScriptEvent:AddListener("RequestGiftLimitResult", self, "OnRequestGiftLimitResult")
end

function CLuaOtherScoreWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryPlayerCurrentBiWuScore", self, "OnQueryPlayerCurrentBiWuScore")
    g_ScriptEvent:RemoveListener("RequestGiftLimitResult", self, "OnRequestGiftLimitResult")
end

function CLuaOtherScoreWnd:OnQueryPlayerCurrentBiWuScore(args)
    local len=table.getn(self.m_DataList)
    for i=1,len do
        local data=self.m_DataList[i]
        if data.Key=="BIWU" then
            LuaGameObject.GetChildNoGC(self.m_DataGoList[i].transform,"ValueLabel").label.text=
                SafeStringFormat3(LocalString.GetString("[c][FFFFFF]当前[-][/c] %d"),args[0])--args从0开始
            break
        end
    end
end

--args从0开始
function CLuaOtherScoreWnd:OnRequestGiftLimitResult(args)
    local len=table.getn(self.m_DataList)
    for i=1,len do
        local data=self.m_DataList[i]
        if data.Key=="GIFT" then
            self.m_CacheParam1=args[1]
            self.m_CacheParam2=args[2]
            LuaGameObject.GetChildNoGC(self.m_DataGoList[i].transform,"ValueLabel").label.text=
                SafeStringFormat3(LocalString.GetString("[c][FFFFFF]当前[-][/c] %d"),args[0])
            break
        end
    end
end
