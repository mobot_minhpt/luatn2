-- Auto Generated!!
local CAuctionHistoryItem = import "L10.UI.CAuctionHistoryItem"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local LocalString = import "LocalString"
local CItem = import "L10.Game.CItem"
local QualityColor = import "L10.Game.QualityColor"
local EnumQualityType = import "L10.Game.EnumQualityType"
CAuctionHistoryItem.m_SetInfo_CS2LuaHook = function (this, info) 
    local item = Item_Item.GetData(info.TemplateID)
    if item then
        this.nameLabel.text = item.Name
        this.nameLabel.color = QualityColor.GetRGBValue(CItem.GetQualityType(info.TemplateID))
    else -- 不是物品，查询装备
        local equipment = EquipmentTemplate_Equip.GetData(info.TemplateID)
        if equipment then
            this.nameLabel.text = equipment.Name
            this.nameLabel.color = QualityColor.GetRGBValue(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), equipment.Color))
        end
    end

    if info.Price >= 1 then
        this.price.text = tostring(info.Price)
        this.buyer.text = info.BuyerName
    else
        this.price.text = LocalString.GetString("[aaaaaa]流拍[-]")
        this.buyer.text = LocalString.GetString("[aaaaaa]无[-]")
    end

    local dateTime = CServerTimeMgr.ConvertTimeStampToZone8Time(info.EndTime)
    this.date.text = System.String.Format("{0:yyyy-MM-dd}", dateTime)
end
