local CommonDefs = import "L10.Game.CommonDefs"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CPinchFaceHubMgr = import "L10.Game.CPinchFaceHubMgr"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local QnCheckBox = import "L10.UI.QnCheckBox"
local UIInput = import "UIInput"
local Gac2Login = import "L10.Game.Gac2Login"

LuaPinchFaceHubReportWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaPinchFaceHubReportWnd, "CommitBtn", "CommitBtn", GameObject)
RegistChildComponent(LuaPinchFaceHubReportWnd, "PlayerLabel", "PlayerLabel", UILabel)
RegistChildComponent(LuaPinchFaceHubReportWnd, "ChaoxiCheckBox", "ChaoxiCheckBox", QnCheckBox)
RegistChildComponent(LuaPinchFaceHubReportWnd, "BuYaCheckBox", "BuYaCheckBox", QnCheckBox)
RegistChildComponent(LuaPinchFaceHubReportWnd, "WeiGuiCheckBox", "WeiGuiCheckBox", QnCheckBox)
RegistChildComponent(LuaPinchFaceHubReportWnd, "MiaoSuWeiGuiCheckBox", "MiaoSuWeiGuiCheckBox", QnCheckBox)
RegistChildComponent(LuaPinchFaceHubReportWnd, "Input", "Input", UIInput)

--@endregion RegistChildComponent end

function LuaPinchFaceHubReportWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.CommitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCommitBtnClick()
	end)


    --@endregion EventBind end
end

function LuaPinchFaceHubReportWnd:Init()
	self.m_PlayerId = LuaPinchFaceMgr.m_ReportPlayerId
	self.m_Title = LuaPinchFaceMgr.m_ReportTitle 
	self.m_ShareId = LuaPinchFaceMgr.m_ReportShareId

	self.PlayerLabel.text = SafeStringFormat3(LocalString.GetString("%s（作者ID %s）"), self.m_Title, tostring(self.m_PlayerId))

	self.ChaoxiCheckBox:SetSelected(false, false)
	self.BuYaCheckBox:SetSelected(false, false)
	self.WeiGuiCheckBox:SetSelected(false, false)
	self.MiaoSuWeiGuiCheckBox:SetSelected(false, false)
end

--@region UIEvent

function LuaPinchFaceHubReportWnd:OnCommitBtnClick()
	local hasContent = self.ChaoxiCheckBox.Selected or self.BuYaCheckBox.Selected or self.WeiGuiCheckBox.Selected or 
	self.MiaoSuWeiGuiCheckBox.Selected or (self.Input.value and self.Input.value ~= "")

	if not hasContent then
		g_MessageMgr:ShowMessage("PinchFaceHub_Report_Need_Content")
		return
	end

	CPinchFaceHubMgr.Inst:GetByShareId(self.m_ShareId, DelegateFactory.Action_CPinchFaceHub_ShareWork_Ret(function(ret)
		local data = ret.data
		local roleInfo = data.accusedRole

		local selfServerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetMyServerId() or 0
		local selfAccoutId = CLoginMgr.Inst.UID or 0
		local selfRoleId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
		local selfRoleName = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name or ""
		local selfLv = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level or 0
		local selfVip = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.Vip.Level or 0

		local serverId = roleInfo.server
		local roleId = roleInfo.roleId
		local roleName = roleInfo.roleName
		local roleLv = roleInfo.roleLevel
		local roleVip = roleInfo.vip

		local shareId = self.m_ShareId
		local image = data.image
		local title = data.title
		local desc = data.desc
		local createTime = data.createTime
		local hot = 0

		local illegalType = ""
		if self.ChaoxiCheckBox.Selected then
			illegalType = LocalString.GetString("抄袭 ")
		end
		if self.BuYaCheckBox.Selected then
			illegalType = SafeStringFormat3("%s%s", illegalType, LocalString.GetString("不雅 "))
		end
		if self.WeiGuiCheckBox.Selected then
			illegalType = SafeStringFormat3("%s%s", illegalType, LocalString.GetString("作者名违规 "))
		end
		if self.MiaoSuWeiGuiCheckBox.Selected then
			illegalType = SafeStringFormat3("%s%s", illegalType, LocalString.GetString("作品名或描述违规"))
		end

		local reason = self.Input.value

		if CLoginMgr.Inst:IsInGame() then
			Gac2Gas.RequestFacialAccuse(serverId, roleId, roleName, roleLv, roleVip, shareId, image,
				title, desc, createTime, illegalType, reason, hot)
		else
			Gac2Login.RequestFacialAccuse(serverId, roleId, roleName, roleLv, roleVip, shareId, image,
				title, desc, createTime, illegalType, reason, hot)
		end
		

		g_MessageMgr:ShowMessage("PinchFaceHub_Report_Success")
		CUIManager.CloseUI(CLuaUIResources.PinchFaceHubReportWnd)
	end
	))
end

--@endregion UIEvent

