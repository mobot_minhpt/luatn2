local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CScene = import "L10.Game.CScene"

CLuaMengGuiJieTaskView = class()

RegistChildComponent(CLuaMengGuiJieTaskView, "TaskDesc", UILabel)
RegistChildComponent(CLuaMengGuiJieTaskView, "LeftTimeLabel", UILabel)
RegistChildComponent(CLuaMengGuiJieTaskView, "LeftBaoXiangLabel", UILabel)
RegistChildComponent(CLuaMengGuiJieTaskView, "GuidLabel", UILabel)
RegistChildComponent(CLuaMengGuiJieTaskView, "RuleBtn", GameObject)
RegistChildComponent(CLuaMengGuiJieTaskView, "LeaveBtn", GameObject)

function CLuaMengGuiJieTaskView:OnEnable()
    local totalBaoXiangNum = Halloween2020_MengGuiJie.GetData().TotalBaoXiangNum
    self.LeftTimeLabel.text = LocalString.GetString("剩余宝箱：")
    self.LeftBaoXiangLabel.text = LocalString.GetString("剩余时间：")  .. "0/" .. totalBaoXiangNum

    if CLuaHalloween2020Mgr.playerForce == 0 then
        self.GuidLabel.text = LocalString.GetString("本局为逃跑方，点击规则可查看详情。")
        self.TaskDesc.text = g_MessageMgr:FormatMessage("HALLOWEEN2020_MENGGUIJIE_ESCAPE_DESC", totalBaoXiangNum) --避开抓捕，开启所有宝箱！靠近灵豆可获得一定的能力。
    else
        self.GuidLabel.text = LocalString.GetString("本局为抓捕方，点击规则可查看详情。")
        self.TaskDesc.text = g_MessageMgr:FormatMessage("HALLOWEEN2020_MENGGUIJIE_CATCH_DESC") --抓捕逃跑方
    end

    UIEventListener.Get(self.LeaveBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CGamePlayMgr.Inst:LeavePlay()
    end)
    UIEventListener.Get(self.RuleBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.MengGuiJieTipWnd)
    end)

    g_ScriptEvent:AddListener("SyncHalloween2020ArrestPlayInfo", self, "OnSyncHalloween2020ArrestPlayInfo")
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
end

function CLuaMengGuiJieTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("SyncHalloween2020ArrestPlayInfo", self, "OnSyncHalloween2020ArrestPlayInfo")
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
end

function CLuaMengGuiJieTaskView:OnSyncHalloween2020ArrestPlayInfo(restCount, totalCount, playerInfos)
    self.LeftTimeLabel.text = LocalString.GetString("剩余宝箱：") .. SafeStringFormat3("%d/%d", restCount, totalCount)
end

function CLuaMengGuiJieTaskView:OnSceneRemainTimeUpdate(args)
    if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            self.LeftBaoXiangLabel.text = LocalString.GetString("剩余时间：") .. self:GetRemainTimeText(CScene.MainScene.ShowTime)
        else
            self.LeftBaoXiangLabel.text = nil
        end
    else
        self.LeftBaoXiangLabel.text = nil
    end
end

function CLuaMengGuiJieTaskView:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end