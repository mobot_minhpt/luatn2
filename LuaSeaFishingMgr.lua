local CScene = import "L10.Game.CScene"
local CoreScene=import "L10.Engine.Scene.CoreScene"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"
local CClientMonster = import "L10.Game.CClientMonster"
local CClientObjectMgr =import "L10.Game.CClientObjectMgr"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow" 
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local EnumPlayScoreString=import "L10.Game.EnumPlayScoreString"
local CItemMgr=import "L10.Game.CItemMgr"
local EnumMoneyType=import "L10.Game.EnumMoneyType"
local EnumTempPlayDataKey = import "L10.Game.EnumTempPlayDataKey"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"

LuaSeaFishingMgr = {}
--开关
LuaSeaFishingMgr.s_SeaFishingSwith = true --海钓玩法
LuaSeaFishingMgr.s_FightingGuideSwitch = true --搏斗引导开关

LuaSeaFishingMgr.m_SeaFishingGameplays = nil
function LuaSeaFishingMgr.IsInPlay()
    if not LuaSeaFishingMgr.m_SeaFishingGameplays then
        LuaSeaFishingMgr.m_SeaFishingGameplays = {}
        local setting = HaiDiaoFuBen_Settings.GetData()
        local gameplayIds = setting.GameplayIds
        local specialGamePlayId = setting.SpecialGameplayId
        LuaSeaFishingMgr.m_SeaFishingGameplays[specialGamePlayId] = 5
        for i=0,gameplayIds.Length-1,2 do
            local id = gameplayIds[i+1]
            LuaSeaFishingMgr.m_SeaFishingGameplays[id] = gameplayIds[i]
        end
    end
    if not CScene.MainScene then return false end
    local gamePlayId = CScene.MainScene.GamePlayDesignId
    if LuaSeaFishingMgr.m_SeaFishingGameplays[gamePlayId] then
        return true
    end
    return false
end

LuaSeaFishingMgr.m_BaitsItemIdList = {}
LuaSeaFishingMgr.m_FishingTickList = {}

function LuaSeaFishingMgr.OpenFishGetAndRecordWnd(fishItemTempId, weight)
    LuaSeaFishingMgr.m_HarvestFishItemId = fishItemTempId
    LuaSeaFishingMgr.m_FishWeight = weight
    CUIManager.ShowUI(CLuaUIResources.FishGetAndRecordWnd)
end

--镇宅鱼
LuaSeaFishingMgr.SelectdZhenZhaiFishZswId = nil
function LuaSeaFishingMgr.OpenZhenZhaiYuDetailWnd(fishIndex,zhenzhaiInfo)
    LuaSeaFishingMgr.SelectdZhenZhaiFishZswId = fishIndex
    LuaSeaFishingMgr.SelectdZhenZhaiFishInfo = zhenzhaiInfo
    CUIManager.ShowUI(CLuaUIResources.ZhenZhaiYuDetailWnd)
end
--鱼篓
function LuaSeaFishingMgr.OpenFishCreelWnd(zhenzhaiyuInfo)
    LuaSeaFishingMgr.SelectdZhenZhaiFishInfo = nil--zhenzhaiInfo
    LuaZhenZhaiYuMgr.m_FishCreelOpenType = LuaZhenZhaiYuMgr.FrishCreelType.Put
    CUIManager.ShowUI(CLuaUIResources.FishCreelWnd)
end

function LuaSeaFishingMgr.OnUpdateFishBasket(basketUD)
    local fishBasketType = CFishBasketType()
	if not fishBasketType:LoadFromString(basketUD) then
		return
	end
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp then
        CClientMainPlayer.Inst.ItemProp.FishBasket = fishBasketType
    end
    g_ScriptEvent:BroadcastInLua("UpdateFishBasket", fishBasketType)
    g_ScriptEvent:BroadcastInLua("RemoveShinningHouseFishItem",nil)
end

LuaSeaFishingMgr.m_PlacedZhenZhaiYu = {}--itemId count
function LuaSeaFishingMgr.OnUpdateHouseFishInfo(fishInfo_u)--SaveToString
    local fishInfoType = CHouseFishType()
	if not fishInfoType:LoadFromString(fishInfo_u) then
		return
	end
    CClientHouseMgr.Inst.mCurFurnitureProp2.FishInfo = fishInfoType

    CUIManager.CloseUI(CLuaUIResources.FishCreelWnd)
    CLuaHouseFishMgr:OnUpdateHouseFishInfo()
    g_ScriptEvent:BroadcastInLua("UpdateHouseFishInfo")
end

--海钓记录
function LuaSeaFishingMgr.OpenServerRecordWnd()
    Gac2Gas.RequestQueryTopFisher()
end

function LuaSeaFishingMgr.OnRequestQueryTopFisher_Result(dataUD)
    if dataUD then
        LuaSeaFishingMgr.m_ServerRecordList = {}
        local list = MsgPackImpl.unpack(dataUD)
        if list and list.Count >= 5 then
            local playerIds = list[1]
            local fishIdxs = list[0]
            local weights = list[3]
            local names = list[2]
            local times = list[4]

            for i=0,playerIds.Count-1,1 do
                local t = {}
                t.Name = names[i]
                t.PlayerId = tonumber(playerIds[i])
                t.Weight = tonumber(weights[i])
                t.FishIdx = tonumber(fishIdxs[i])
                --t.FishIdx = HouseFish_AllFishes.GetData(t.FishIdx).FishIdx
                t.Timestamp = tonumber(times[i])
                LuaSeaFishingMgr.m_ServerRecordList[t.FishIdx] = t
            end
        end
        CUIManager.ShowUI(CLuaUIResources.SeaFishingServerRecordWnd)
    end
end

function LuaSeaFishingMgr:OnRefreshShinning(templateId,isShinning)
    if not CClientMainPlayer.Inst then return end
    local fishbasket = CClientMainPlayer.Inst.ItemProp.FishBasket
	if not fishbasket then return end 

    local data = HouseFish_AllFishes.GetData(templateId)
    local idx = 1
    if data then--fish
        idx = data.ShinningIdx
        fishbasket.ShinningFish:SetBit(idx,isShinning)
    else 
        data = HouseFish_AllOther.GetData(templateId)
        if data then
            idx = data.ShinningIdx
            fishbasket.ShinningOther:SetBit(idx,isShinning)
        end
    end

    --self:InitFishBasket()
    --self.ReloadData(false,false)
end
-------------------------------------------------海钓副本-------------------------------
LuaSeaFishingMgr.m_CurHaiDiaoGameplayId = nil
LuaSeaFishingMgr.m_CurHaiDiaoStageId = nil
LuaSeaFishingMgr.m_CurParams = nil
LuaSeaFishingMgr.m_CurGameplayId = 0
function LuaSeaFishingMgr.InitTaskViewWithStage(stageId,gamePlayId,params)
    LuaSeaFishingMgr.m_CurHaiDiaoStageId = stageId
    LuaSeaFishingMgr.m_CurHaiDiaoGameplayId = gamePlayId
    LuaSeaFishingMgr.m_CurParams = params
    g_ScriptEvent:BroadcastInLua("InitTaskViewWithStage", stageId,gamePlayId,params)
end

--海钓副本报名界面
function LuaSeaFishingMgr.OpenSignHaiDiaoFuBenWnd()
    Gac2Gas.RequestSeaFishingPlayInfo()
end

--海钓副本结算
LuaSeaFishingMgr.DifficultName = {
    [1] = LocalString.GetString("简单"),
    [2] = LocalString.GetString("普通"),
    [3] = LocalString.GetString("困难"),
    [4] = LocalString.GetString("地狱"),
    [5] = LocalString.GetString("幻海妖雾"),
}
LuaSeaFishingMgr.DifficultColor = {
    [1] = "FFF68D",
    [2] = "FF9F58",
    [3] = "A3F1FF",
    [4] = "FB7E7E",
    [5] = "FF7FFF",
}

function LuaSeaFishingMgr.SyncSeaFishingPlayResult(gameplayId, isWin)
    if isWin then
        LuaSeaFishingMgr.m_CurHaiDiaoGameplayId = gameplayId
        CUIManager.ShowUI(CLuaUIResources.SeaFishingSucceedWnd)
    else
        local difficulty = LuaSeaFishingMgr.m_SeaFishingGameplays[gameplayId] 
        if difficulty > 4 then return end

        local difficultyName = LuaSeaFishingMgr.DifficultName[difficulty]
        LuaSeaFishingMgr.m_LastDifficult = difficulty
        local msg = g_MessageMgr:FormatMessage("HaiDaiFuBen_Fail_Replay_Confim", difficultyName)
        MessageWndManager.ShowOKCancelMessage(msg,
            DelegateFactory.Action(function ()
                Gac2Gas.RequestSeaFishingPlayInfo()
            end),
            nil,
        LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    end
end

LuaSeaFishingMgr.m_RewardedTimes = 0
LuaSeaFishingMgr.m_LastDifficult = 0
LuaSeaFishingMgr.m_CanEnterSpecialPlay = false
LuaSeaFishingMgr.m_FixBoatStage = nil
function LuaSeaFishingMgr.OnSendSeaFishingPlayInfo(rewardTimes, lastDifficult, canEnterSpecialPlay)
    LuaSeaFishingMgr.m_RewardedTimes = rewardTimes
    LuaSeaFishingMgr.m_LastDifficult = lastDifficult
    LuaSeaFishingMgr.m_PassDifficultRecord = lastDifficult
    LuaSeaFishingMgr.m_CanEnterSpecialPlay = canEnterSpecialPlay
    CUIManager.ShowUI(CLuaUIResources.SeaFishingPlayWnd)
end

function LuaSeaFishingMgr.OnUpdateSeaFishingPlayStageInfo(stage, params)
    local gamePlayId = CScene.MainScene.GamePlayDesignId
    LuaSeaFishingMgr.InitTaskViewWithStage(stage,gamePlayId,params)
end

--修船玩法
function LuaSeaFishingMgr:AddListener()
    g_ScriptEvent:RemoveListener("MainPlayerMoveStepped", self, "OnMoveStepped")
    g_ScriptEvent:AddListener("MainPlayerMoveStepped", self, "OnMoveStepped")
end
function LuaSeaFishingMgr:RemoveListener()
    g_ScriptEvent:RemoveListener("MainPlayerMoveStepped", self, "OnMoveStepped")
end

LuaSeaFishingMgr.m_LastHintStatus = false
function LuaSeaFishingMgr.HintSkillBtn(hintSkill,isHint)
    if isHint == LuaSeaFishingMgr.m_LastHintStatus then
        return
    end
    LuaSeaFishingMgr.m_LastHintStatus = isHint
    g_ScriptEvent:BroadcastInLua("HintHaiDiaoFuBenSkillBtn",hintSkill,isHint)
end

--LuaSeaFishingMgr:AddListener()

function LuaSeaFishingMgr:OnMoveStepped()
    --if not LuaSeaFishingMgr.IsInPlay() then return end
    if not LuaSeaFishingMgr.m_FixBoatStage then
        LuaSeaFishingMgr.m_FixBoatStage = HaiDiaoFuBen_Settings.GetData().FixBoatStage
    end
    if not LuaSeaFishingMgr.m_CurHaiDiaoStageId or LuaSeaFishingMgr.m_CurHaiDiaoStageId ~= LuaSeaFishingMgr.m_FixBoatStage then
        return
    end
    local player = CClientMainPlayer.Inst
    if not player then return end
    local pos = player.RO.Position
    local monsterEngineId
    local monsterPos
    local bugMonsterTemplateId
    local hintSkill
    for x=-1,1,1 do
        for y=-1,1,1 do
            local xx = math.floor(pos.x) + x
            local yy = math.floor(pos.z) + y
            local bAnything,objectid,mpos,monsterId = LuaSeaFishingMgr.IsAnyMonsterInGrid(xx,yy)
            if bAnything and objectid then
                if LuaSeaFishingMgr.m_CollectionMonsters[monsterId] then
                    monsterEngineId = objectid
                    monsterPos = mpos
                    break
                else
                    --if monsterId == 
                    if not LuaSeaFishingMgr.Monster2Skill then
                        LuaSeaFishingMgr.Monster2Skill = {}
                        local setting = HaiDiaoFuBen_Settings.GetData()
                        LuaSeaFishingMgr.Monster2Skill[setting.WaterPoolMonster_Guide[0]] = setting.WaterPoolMonster_Guide[1]
                        LuaSeaFishingMgr.Monster2Skill[setting.HoleMonsterId_Guide[0]] = setting.HoleMonsterId_Guide[1]
                        LuaSeaFishingMgr.Monster2Skill[setting.HoleWithWoodMonsterId_Guide[0]] = setting.HoleWithWoodMonsterId_Guide[1]
                    end
                    hintSkill = LuaSeaFishingMgr.Monster2Skill[monsterId]
                    if hintSkill then
                        break
                    end
                end
            end
        end
    end
    if monsterEngineId then
        local pickDelegate =  DelegateFactory.VoidDelegate(function (p)
            if LuaSeaFishingMgr.m_CollectTargetEngineId then
                Gac2Gas.CollectRepairToolInSeaFishingPlay(LuaSeaFishingMgr.m_CollectTargetEngineId)
            end
        end)
        LuaSeaFishingMgr.ShowPickUpWnd(LocalString.GetString("拾取"),monsterEngineId,monsterPos,pickDelegate)
    else
        LuaSeaFishingMgr.HidePickUpWnd()
    end

    if hintSkill then
        LuaSeaFishingMgr.HintSkillBtn(hintSkill,true)
    else
        LuaSeaFishingMgr.HintSkillBtn(hintSkill,false)
    end
end

LuaSeaFishingMgr.m_CollectionMonsters = nil
LuaSeaFishingMgr.m_TopAnchorY = 2
function LuaSeaFishingMgr.IsAnyMonsterInGrid(xx,yy)
    local bAnything = false
    local objectid_list = CoreScene.MainCoreScene:GetGridAllObjectIdsWithFloor(xx, yy, 0)
    if not objectid_list or objectid_list.Count <= 0 then
        return false
    end
    for i = 0, objectid_list.Count - 1 do
        local objectid = objectid_list[i]
        if not CClientObjectMgr.Inst then return false end
        local obj = CClientObjectMgr.Inst:GetObject(objectid)
        if obj then
            if (TypeIs(obj, typeof(CClientMonster))) then
                bAnything = true
                if not LuaSeaFishingMgr.m_CollectionMonsters then
                    LuaSeaFishingMgr.m_CollectionMonsters = {}
                    local collections = HaiDiaoFuBen_Settings.GetData().MonsterIdWithSkillAndBuff
                    for i=0,collections.Length-3,3 do
                        LuaSeaFishingMgr.m_CollectionMonsters[collections[i]] = true
                    end
                end
                --[[if LuaSeaFishingMgr.m_CollectionMonsters[obj.TemplateId] then
                    return bAnything,objectid,obj.RO.Position,obj.TemplateId
                end--]]
                return bAnything,objectid,obj.RO.Position,obj.TemplateId
            end           
        end
    end
    return bAnything
end

function LuaSeaFishingMgr.ShowPickUpWnd(btntext,targetEngineId,monsterPos,pickDelegate)
    LuaSeaFishingMgr.m_CollectTargetEngineId = targetEngineId
    LuaSeaFishingMgr.m_CollectionPos = monsterPos
    LuaSeaFishingMgr.m_PickUpWndDelegate = pickDelegate
    LuaSeaFishingMgr.m_PickBtnText = btntext
    if not CUIManager.IsLoaded(CLuaUIResources.SpecialHaiDiaoPlayPickUpWnd) then
        CUIManager.ShowUI(CLuaUIResources.SpecialHaiDiaoPlayPickUpWnd)
    end
end
function LuaSeaFishingMgr.HidePickUpWnd()
    if CUIManager.IsLoaded(CLuaUIResources.SpecialHaiDiaoPlayPickUpWnd) then
        CUIManager.CloseUI(CLuaUIResources.SpecialHaiDiaoPlayPickUpWnd)
    end
end
-----------------------------------------------钓鱼----------------------------------------------
LuaSeaFishingMgr.m_MinWaitTime = 2
LuaSeaFishingMgr.m_MaxWaitTime = 5
LuaSeaFishingMgr.m_NoticeTime = 3
LuaSeaFishingMgr.m_CurPoleLevel = nil
LuaSeaFishingMgr.m_SelectedPoleType = nil
LuaSeaFishingMgr.m_WeightSpeed = 0.5
LuaSeaFishingMgr.CameraGo = nil
LuaSeaFishingMgr.m_YuErItemId2PlayerLevel = nil

function LuaSeaFishingMgr.TryGetYuErPlayerLevel(itemId)
    if not LuaSeaFishingMgr.m_YuErItemId2PlayerLevel then
        LuaSeaFishingMgr.m_YuErItemId2PlayerLevel = {}
        local setting = HouseFish_Setting.GetData()
        local fishFoodTempId = setting.FishFoodTempId
        local fishFoodTempLv = setting.FishFoodTempLv
        for i=0,fishFoodTempId.Length-1,1 do
            LuaSeaFishingMgr.m_YuErItemId2PlayerLevel[fishFoodTempId[i]] = {fishFoodTempLv[i],i+1}
        end
    end
    return LuaSeaFishingMgr.m_YuErItemId2PlayerLevel[itemId]
end

function LuaSeaFishingMgr.GetNeedSkillLvByYuerItemId(itemId)
    local setting = HouseFish_Setting.GetData()
    local fishFoodTempId = setting.FishFoodTempId
    local yuerLv = 99
    for i=0,fishFoodTempId.Length-1,1 do 
        if fishFoodTempId[i] == itemId then
            yuerLv = i+1
        end
    end
    local fishFoodWeightMaxLv = setting.FishFoodWeightMaxLv
    local needSkillLevel
    if yuerLv <= 1 then
        needSkillLevel = 1
    elseif fishFoodWeightMaxLv.Length >= yuerLv-1 then
        needSkillLevel = fishFoodWeightMaxLv[yuerLv-1-1] + 1
    end
    --print(itemId,yuerLv,needSkillLevel)
    return needSkillLevel
end

function LuaSeaFishingMgr.OpenPoleAppearancePreviewWnd(curLevel,wearPoleType)
    if not wearPoleType or wearPoleType == 0 then
        wearPoleType = 1
    end
    LuaSeaFishingMgr.m_CurPoleLevel = curLevel
    LuaSeaFishingMgr.m_SelectedPoleType = wearPoleType
    CUIManager.ShowUI(CLuaUIResources.FishingRodAppearanceWnd)
end

function LuaSeaFishingMgr.OnSetFishPoleSuccess(poleType)
    LuaSeaFishingMgr.m_SelectedPoleType = poleType
    CClientMainPlayer.Inst.AppearanceProp.FishPoleType = poleType
    g_ScriptEvent:BroadcastInLua("OnSetFishPoleSuccess",poleType)
    CUIManager.CloseUI(CLuaUIResources.FishingRodAppearanceWnd)
end

function LuaSeaFishingMgr.GetAddSectionCount()
    if LuaSeaFishingMgr.s_SeaFishingSwith then
        return 1
    else
        return 0
    end
end

--鱼饵
function LuaSeaFishingMgr.UpdateFishFood(fishFoodItemTempId)
    CClientMainPlayer.Inst.ItemProp.FishFood = fishFoodItemTempId
    g_ScriptEvent:BroadcastInLua("UpdateFishFood")
end

LuaSeaFishingMgr.m_IsShowEntryButton = false
function LuaSeaFishingMgr.OnSwitchFishingPannel(bIsOpen)
    if not LuaSeaFishingMgr.s_SeaFishingSwith then
        return
    end

    --不是钓鱼场景不显示钓鱼入口
    if bIsOpen and CScene.MainScene and CScene.MainScene.SceneTemplateId ~= HouseFish_Fishing.GetData().FishingSceneTempId then
        return
    end

    g_ScriptEvent:BroadcastInLua("OnSwitchFishingPannel",bIsOpen)
    LuaSeaFishingMgr.m_IsShowEntryButton = bIsOpen
    if not bIsOpen and CUIManager.IsLoaded(CUIResources.HouseFishingWnd) then
        CUIManager.CloseUI(CUIResources.HouseFishingWnd)
    end
end

function LuaSeaFishingMgr.SetFishingCamera()
    local setting = HouseFish_Setting.GetData()
    local player = CClientMainPlayer.Inst
    if not player then return end
    local direction = player.RO.Direction
    local ryz = CameraFollow.Inst.targetRZY
    ryz.x = direction + 180 + setting.CameraParam[0]
    ryz.z = setting.CameraParam[2]
    ryz.y = setting.CameraParam[1]
    CameraFollow.Inst.targetRZY = ryz
    CameraFollow.Inst.CurCameraParam:ApplyRZY(ryz)
end

function LuaSeaFishingMgr.RevertFishingCamera()
    CameraFollow.Inst:ResetToDefault()
end

function LuaSeaFishingMgr.OnSetCurrentFishingWaitDuration(startTime, waitDuration)
    LuaSeaFishingMgr.m_IsFishing = true
    local player = CClientMainPlayer.Inst
    if not player then return end

    --抛竿
    local playerRo = CClientMainPlayer.Inst.RO
    local expressionId = 47000749
    if LuaSeaFishingMgr.m_CurGameplayId == HaiDiaoFuBen_Settings.GetData().SpecialGameplayId then
        expressionId = HaiDiaoFuBen_Settings.GetData().FishingExpression[0]
    end
    player:ShowExpressionActionState(expressionId)

    local startLength = playerRo:GetAniLength("fishing_new_start")
    local tick = RegisterTickOnce(function()

        local expression = 47000750
        if LuaSeaFishingMgr.m_CurGameplayId == HaiDiaoFuBen_Settings.GetData().SpecialGameplayId then
            expression = HaiDiaoFuBen_Settings.GetData().FishingExpression[1]
        end
        player:ShowExpressionActionState(expression)
    end, startLength*1000)
    table.insert(LuaSeaFishingMgr.m_FishingTickList,tick)
    g_ScriptEvent:BroadcastInLua("SetCurrentFishingWaitDuration",startTime, waitDuration)
end

function LuaSeaFishingMgr.OnFishingHarvestBegin(fishItemTempId)
    -- 咬竿
    local expressionId = 47000751
    if LuaSeaFishingMgr.m_CurGameplayId == HaiDiaoFuBen_Settings.GetData().SpecialGameplayId then
        expressionId = HaiDiaoFuBen_Settings.GetData().FishingExpression[2]
    end
    local player = CClientMainPlayer.Inst
    if not player then return end
    player:ShowExpressionActionState(expressionId)
    g_ScriptEvent:BroadcastInLua("FishingHarvestBegin")

    local setting = HouseFish_Setting.GetData()
    local fxId = setting.BowenFxId
    local distance = setting.BowenFxDistance
    local pos = player.RO.transform:InverseTransformPoint(player.RO.Position)
    pos.z = pos.z + distance
    local worldPos = player.RO.transform:TransformPoint(pos)
    LuaSeaFishingMgr.m_BowenFx = CEffectMgr.Inst:AddWorldPositionFX(fxId, worldPos, 0,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
end

function LuaSeaFishingMgr.OnBeginFishingFight()

    local expressionId = 47000752
    if LuaSeaFishingMgr.m_CurGameplayId == HaiDiaoFuBen_Settings.GetData().SpecialGameplayId then
		expressionId = HaiDiaoFuBen_Settings.GetData().FishingExpression[3]
	end
    local player = CClientMainPlayer.Inst
    if not player then return end
    local playerRo = CClientMainPlayer.Inst.RO
    player:ShowExpressionActionState(expressionId)

    if LuaSeaFishingMgr.m_BowenFx then
		LuaSeaFishingMgr.m_BowenFx:Destroy()
		LuaSeaFishingMgr.m_BowenFx = nil
	end
end

function LuaSeaFishingMgr.PlayShouGanAni()

    local expressionId = 47000753
    if LuaSeaFishingMgr.m_CurGameplayId == HaiDiaoFuBen_Settings.GetData().SpecialGameplayId then
		expressionId = HaiDiaoFuBen_Settings.GetData().FishingExpression[4]
	end
    local player = CClientMainPlayer.Inst
    if not player then return end
    local playerRo = CClientMainPlayer.Inst.RO
    player:ShowExpressionActionState(expressionId)

    if LuaSeaFishingMgr.m_BowenFx then
		LuaSeaFishingMgr.m_BowenFx:Destroy()
		LuaSeaFishingMgr.m_BowenFx = nil
	end
end

function LuaSeaFishingMgr.OnFishingHarvestSuccess(fishItemTempId, weight)
    local expressionId = 47000753
    if LuaSeaFishingMgr.m_CurGameplayId == HaiDiaoFuBen_Settings.GetData().SpecialGameplayId then
		expressionId = HaiDiaoFuBen_Settings.GetData().FishingExpression[4]
	end
    local player = CClientMainPlayer.Inst
    if not player then return end
    local playerRo = CClientMainPlayer.Inst.RO
    player:ShowExpressionActionState(expressionId)

    if LuaSeaFishingMgr.m_BowenFx then
		LuaSeaFishingMgr.m_BowenFx:Destroy()
		LuaSeaFishingMgr.m_BowenFx = nil
	end

    local startLength = playerRo:GetAniLength("fishing_new_end")
    local tick = RegisterTickOnce(function()
        LuaSeaFishingMgr.OpenFishGetAndRecordWnd(fishItemTempId, weight)
        --恢复站立
        LuaSeaFishingMgr.m_IsFishing = false
        if LuaSeaFishingMgr.m_CurGameplayId == HaiDiaoFuBen_Settings.GetData().SpecialGameplayId then
            CSkillButtonBoardWnd.Show(true)
        end
    end, startLength*1000)
    table.insert(LuaSeaFishingMgr.m_FishingTickList,tick)
end

function LuaSeaFishingMgr.StopFishing()
    local expressionId = 47000753
    if LuaSeaFishingMgr.m_CurGameplayId == HaiDiaoFuBen_Settings.GetData().SpecialGameplayId then
		expressionId = HaiDiaoFuBen_Settings.GetData().FishingExpression[4]
	end
    local player = CClientMainPlayer.Inst
    if not player then return end
    local playerRo = CClientMainPlayer.Inst.RO
    player:ShowExpressionActionState(expressionId)

    if LuaSeaFishingMgr.m_BowenFx then
		LuaSeaFishingMgr.m_BowenFx:Destroy()
		LuaSeaFishingMgr.m_BowenFx = nil
	end

    local startLength = playerRo:GetAniLength("fishing_new_end")
    local tick = RegisterTickOnce(function()
        LuaSeaFishingMgr.m_IsFishing = false
    end, startLength*1000)
    table.insert(LuaSeaFishingMgr.m_FishingTickList,tick)
end

function LuaSeaFishingMgr.OnHouseFishingFightFailed()
    --LuaSeaFishingMgr.RevertFishingCamera()
    LuaSeaFishingMgr.PlayShouGanAni()
    local player = CClientMainPlayer.Inst
    if not player then return end
    local playerRo = CClientMainPlayer.Inst.RO
    local anilength = playerRo:GetAniLength("fishing_new_end")
    local tick = RegisterTickOnce(function()
        --恢复站立
        LuaSeaFishingMgr.m_IsFishing = false
    end, anilength*1000)
    table.insert(LuaSeaFishingMgr.m_FishingTickList,tick)
end



--鱼竿
LuaSeaFishingMgr.m_UnlockedLevel2YuganId = nil
function LuaSeaFishingMgr.GetYuganIdBySkillLevel(level)
    if not LuaSeaFishingMgr.m_UnlockedLevel2YuganId or not next(LuaSeaFishingMgr.m_UnlockedLevel2YuganId) then
        LuaSeaFishingMgr.m_UnlockedLevel2YuganId = {}
        HouseFish_PoleType.Foreach(function(k,v)
            LuaSeaFishingMgr.m_UnlockedLevel2YuganId[v.UnLockedLevel] = v.TemplateID
        end)
    end

    local lastFindLevel = 0
    local yuganId
    for unlocklevel , templateId in pairs(LuaSeaFishingMgr.m_UnlockedLevel2YuganId) do
        if level >= unlocklevel and unlocklevel > lastFindLevel then
            yuganId = templateId
            lastFindLevel = unlocklevel
        end
    end
    return yuganId
end

--幻灯片
LuaSeaFishingMgr.m_PlayCommicId = nil
LuaSeaFishingMgr.m_ComicDuration = nil
function LuaSeaFishingMgr.ShowIllustrations(id,duration)
    LuaSeaFishingMgr.m_PlayCommicId = id
    LuaSeaFishingMgr.m_ComicDuration = duration
    CUIManager.ShowUI(CLuaUIResources.ComicPPTWnd)
end

--图鉴
function LuaSeaFishingMgr.ShowTuJianWnd()
    CUIManager.ShowUI(CLuaUIResources.SeaFishingTuJianWnd)
end

LuaSeaFishingMgr.Shop = {34000081, 34000082}
LuaSeaFishingMgr.XiaoHaoPing = 21051209
function LuaSeaFishingMgr.ShowSeaFishingShopWnd(npcEngineId, poolGrade, shopId, goods, limits,buyedItemList)
    -- 积分商店解锁条件
    if LuaSeaFishingMgr.GetShopGrade(poolGrade) == 0 then
        g_MessageMgr:ShowMessage("SEAFISHINGSHOP_LEVELLIMIT")
        return
    end
    if CClientHouseMgr.Inst:HaveHouse() == 0 then
        g_MessageMgr:ShowMessage("SEAFISHINGSHOP_NO_HOUSE")
        return
    end

    if shopId == LuaSeaFishingMgr.Shop[1] then
        LuaSeaFishingMgr.InitSeaFishingShopWnd(npcEngineId, poolGrade, shopId, goods, limits, true,buyedItemList)
        CUIManager.ShowUI(CLuaUIResources.SeaFishingShopWnd)
    elseif shopId == LuaSeaFishingMgr.Shop[2] then
        LuaSeaFishingMgr.InitSeaFishingShopWnd(npcEngineId, poolGrade, shopId, goods, limits, false,buyedItemList)
        g_ScriptEvent:BroadcastInLua("UpdateZswShop")
    end
end

function LuaSeaFishingMgr.InitSeaFishingShopWnd(npcEngineId, poolGrade, shopId, goods, limits, normal,buyedItemList)
    local key = Shop_PlayScore.GetData(shopId).Key

    local itemList = MsgPackImpl.unpack(goods)
    if normal then

        local ItemGoods = {}
        local Prices = {}
        LuaSeaFishingMgr.shopInfo = {
            PlayScoreKey = EnumPlayScoreString.GetKey(key),
            NpcEngineId = 0,
            ShopId = shopId,
            MoneyType = EnumMoneyType.Score,
            ItemGoods = {},
            Prices={}, -- price, perlCost, LingYuCost, 消耗品的templateId，这个数据来自Shop_PlayScore[shopId]和HouseFishShop_SaledItem
            Limits = {},
            PoolGrade = poolGrade,
            ItemIndex = {},
        }

        -- itemId, price, PearlId, PearlCost, isOpenNextLevel,
        for i=0,itemList.Count-1,5 do
            table.insert(  LuaSeaFishingMgr.shopInfo.ItemIndex, math.floor(i/5) + 1  )
            table.insert(  ItemGoods, {itemList[i], itemList[i+4]} )
            local xhpId = nil
            local xhpCount = itemList[i+3]
            if xhpCount > 0 then 
                xhpId = itemList[i+2]
            end
            table.insert(  Prices, {itemList[i+1], xhpCount, 0, xhpId} )  -- 增加一个消耗品的id
        end
        -- item 显示顺序排序
        table.sort(LuaSeaFishingMgr.shopInfo.ItemIndex, function (a, b)
            -- bool compare (a < b)
            return ItemGoods[a][2] == ItemGoods[b][2] and ItemGoods[a][1] < ItemGoods[b][1] or (ItemGoods[a][2] and 1 or 0) < (ItemGoods[b][2] and 1 or 0)
        end)
        -- 之前写的时候忘记使用 ItemIndex，在这里修补下
        for i, v in ipairs(LuaSeaFishingMgr.shopInfo.ItemIndex) do
            table.insert(  LuaSeaFishingMgr.shopInfo.ItemGoods, ItemGoods[v] )
            table.insert(  LuaSeaFishingMgr.shopInfo.Prices, Prices[v] )
        end
        ItemGoods = nil
        Prices = nil
    else
        LuaSeaFishingMgr.zswShopInfo = {
            PlayScoreKey = EnumPlayScoreString.GetKey(key),
            NpcEngineId = 0,
            ShopId = shopId,
            MoneyType = nil,
            ItemGoods = {},
            Prices={}, -- price, perlCost, LingYuCost
            Limits = {},
            PoolGrade = poolGrade,
            ItemIndex = {},
            Buyed = {},
            BuyedBrush = {}
        }

        local buyedBrushList = MsgPackImpl.unpack(buyedItemList)
        for i=1,buyedBrushList.Count do
            LuaSeaFishingMgr.zswShopInfo.BuyedBrush[tonumber(buyedBrushList[i-1])] = true
        end
    
        -- itemId, ScoreCost, PearlId, PearlCost, LingYuCost, isOpenNextLevel,isSoldOut
        for i=0,itemList.Count-1,7 do
            table.insert(  LuaSeaFishingMgr.zswShopInfo.ItemIndex, math.floor(i/7) + 1  )
            table.insert(  LuaSeaFishingMgr.zswShopInfo.ItemGoods, {itemList[i], itemList[i+5]} )
            local xhpId = nil
            local xhpCount = itemList[i+3]
            if xhpCount > 0 then
                xhpId = itemList[i+2]
            end
            table.insert(  LuaSeaFishingMgr.zswShopInfo.Prices, {itemList[i+1], xhpCount, itemList[i+4], xhpId} )
            table.insert(  LuaSeaFishingMgr.zswShopInfo.Buyed, itemList[i+6]  )
        end

        -- item 显示顺序排序
        table.sort(LuaSeaFishingMgr.zswShopInfo.ItemIndex, function (a, b)
            return not LuaSeaFishingMgr.zswShopInfo.ItemGoods[a][2] and LuaSeaFishingMgr.zswShopInfo.ItemGoods[b][2]
        end)
    end

    local list = MsgPackImpl.unpack(limits)
    if list ~= nil and list.Count % 3 == 0 then
        do
            local i = 0
            while i < list.Count do
                local info = {
                    templateId =list[i],
                    limit = list[i + 1],
                    limitType = list[i + 2],
                }
                if normal then
                    LuaSeaFishingMgr.shopInfo.Limits[info.templateId]=info
                else
                    LuaSeaFishingMgr.zswShopInfo.Limits[info.templateId]=info
                end
                i = i + 3
            end
        end
    end
end

function LuaSeaFishingMgr.GetRemainCount(templateId)
    if LuaSeaFishingMgr.shopInfo.Limits[templateId] then
        return LuaSeaFishingMgr.shopInfo.Limits[templateId].limit
    end
    return 999
end

function LuaSeaFishingMgr.SetRemainCount(templateId,remainCount)
    if LuaSeaFishingMgr.shopInfo.Limits[templateId] then
        LuaSeaFishingMgr.shopInfo.Limits[templateId].limit=remainCount
    end
end

function LuaSeaFishingMgr.GetLimitType(templateId)
    if LuaSeaFishingMgr.shopInfo.Limits[templateId] then
        return LuaSeaFishingMgr.shopInfo.Limits[templateId].limitType
    end
    return 0
end

-- 消耗品沧溟珠数量
function LuaSeaFishingMgr.GetPerlCount()
	local bindCount, notbindCount
	bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(LuaSeaFishingMgr.XiaoHaoPing)
    return bindCount + notbindCount
end

--刷新所需积分数量
function LuaSeaFishingMgr.GetRefreshCost()
    local setting = Shop_Setting.GetData()
    local num = setting.RefreshTimeMaxNum
    local formulaId = setting.RefreshConsumptionFormulaId
    local tempPlayData = CClientMainPlayer.Inst.PlayProp.TempPlayData
    local times = 1
    if CommonDefs.DictContains(tempPlayData, typeof(UInt32), EnumToInt(EnumTempPlayDataKey.eSeaFishingDecorationShopDailyRefreshTimes)) then
        local info = CommonDefs.DictGetValue(tempPlayData, typeof(UInt32), EnumToInt(EnumTempPlayDataKey.eSeaFishingDecorationShopDailyRefreshTimes))
        -- 过期置为 1
        if CServerTimeMgr.Inst.timeStamp <= info.ExpiredTime then
            times = tonumber(info.Data.StringData)
        end
    end
    if times < num then
        return AllFormulas.Action_Formula[formulaId].Formula(nil, nil, {times})
    end
end

function LuaSeaFishingMgr.GetShopGrade(poolGrade)
    local data = HouseFishShop_ItemList.GetData(poolGrade or LuaSeaFishingMgr.shopInfo.PoolGrade)
    if data and data.ShopLv then
        return data.ShopLv
    end
    return 0
end

function LuaSeaFishingMgr.GetRemainUsingTimes(item)
    if CClientMainPlayer.Inst == nil then return 0 end

    local useTimes = CClientMainPlayer.Inst.ItemProp:GetItemUseTimes(item.TimesGroup, item.ItemPeriod)
    local remainTimes = item.TimesPerDay - useTimes
    if item.TimesPerDayParam > 0 then
        local fun = AllFormulas.Action_Formula[item.TimesPerDayParam]
        local timesPerDayLimit = tonumber(fun.Formula(nil, nil, {CClientMainPlayer.Inst.Level, CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, CClientMainPlayer.Inst.BasicProp.CreateTime,CClientMainPlayer.Inst.ItemProp:GetHolidayWithoutFishingPermanent()}))
        remainTimes = timesPerDayLimit - useTimes
    end
    if remainTimes < 0 then
        remainTimes = 0
    end
    if item.ID == 21006125 then
        local formulaId = GameplayItem_Setting.GetData().PermanentProp_ReviseLimitFormulaId
        local formula = GetFormula(formulaId)
        if formula then
            if CClientMainPlayer.Inst then
                local fightProp = CClientMainPlayer.Inst.FightProp
                local soulCoreProp = CClientMainPlayer.Inst.SkillProp.SoulCore
                local maxTimes = formula and formula(nil, nil, {soulCoreProp.Level})
                local useTiems = 0
                useTiems = useTiems + fightProp:GetParam(EPlayerFightProp.RevisePermanentCor)
                useTiems = useTiems + fightProp:GetParam(EPlayerFightProp.RevisePermanentSta)
                useTiems = useTiems + fightProp:GetParam(EPlayerFightProp.RevisePermanentStr)
                useTiems = useTiems + fightProp:GetParam(EPlayerFightProp.RevisePermanentInt)
                useTiems = useTiems + fightProp:GetParam(EPlayerFightProp.RevisePermanentAgi)
                remainTimes = maxTimes - useTiems
            end
        end
    end
    return remainTimes
end
