local UIPanel = import "UIPanel"
local CBaseWnd = import "L10.UI.CBaseWnd"
local CUIFx = import "L10.UI.CUIFx"
local Camera = import "UnityEngine.Camera"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local NGUITools = import "NGUITools"
local LayerDefine = import "L10.Engine.LayerDefine"
local QnButton = import "L10.UI.QnButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local LuaTweenUtils = import "LuaTweenUtils"
local TweenAlpha = import "TweenAlpha"
local Color = import "UnityEngine.Color"

LuaLeafWithMemoryWnd = class()
RegistClassMember(LuaLeafWithMemoryWnd,"m_CloseButton")
RegistClassMember(LuaLeafWithMemoryWnd,"m_LeafFx")
RegistClassMember(LuaLeafWithMemoryWnd,"m_LeaveCenter")
RegistClassMember(LuaLeafWithMemoryWnd,"m_Bg")
RegistClassMember(LuaLeafWithMemoryWnd,"m_DisplayCamera")
RegistClassMember(LuaLeafWithMemoryWnd,"m_SubtitleLabel")
RegistClassMember(LuaLeafWithMemoryWnd,"m_WaitLeafStopTick")
RegistClassMember(LuaLeafWithMemoryWnd,"m_QnBtn")
RegistClassMember(LuaLeafWithMemoryWnd,"m_EffectTweenAlpha")
RegistClassMember(LuaLeafWithMemoryWnd,"m_FingerGuideGo")

function LuaLeafWithMemoryWnd:Awake()
    local Panel= self.gameObject:GetComponent(typeof(UIPanel))
    Panel.IgnoreIphoneXMargin = true
end

function LuaLeafWithMemoryWnd:Close()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaLeafWithMemoryWnd:Init()
    self.m_CloseButton = self.transform:Find("Camera/Panel/CloseButton").gameObject
    self.m_LeafFx = self.transform:Find("Camera/Panel/TweenAlpha/LeaveCenter/LeaveFx"):GetComponent(typeof(CUIFx))
    self.m_LeaveCenter = self.transform:Find("Camera/Panel/TweenAlpha/LeaveCenter").transform
    self.m_Bg = self.transform:Find("Camera/Panel/WhiteBg"):GetComponent(typeof(UISprite))
    self.m_DisplayCamera = self.transform:Find("Camera"):GetComponent(typeof(Camera))
    self.m_DisplayCamera.rect = CUICommonDef.GetCurrentCameraRect()
    self.m_SubtitleLabel = self.transform:Find("Camera/Panel/SubtitleLabel"):GetComponent(typeof(UILabel))
    self.m_QnBtn = self.m_LeafFx:GetComponent(typeof(QnButton))
    self.m_EffectTweenAlpha = self.transform:Find("Camera/Panel/TweenAlpha"):GetComponent(typeof(TweenAlpha))
    self.m_FingerGuideGo = self.transform:Find("Camera/Panel/TweenAlpha/Finger").gameObject
    self.m_FingerGuideGo:SetActive(false)
    self.m_EffectTweenAlpha.enabled = false
    self.m_EffectTweenAlpha.duration = CLuaTaskMgr.m_LeafWithMemoryWnd_FadeAlphaTime
    self:CancelWaitLeafStopTick()
    self.m_LeafFx.OnLoadFxFinish = nil

    CommonDefs.AddOnClickListener(self.m_CloseButton, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

    NGUITools.SetLayer(self.transform:Find("Camera/Panel").gameObject,LayerDefine.CutSceneUI)
    self.m_SubtitleLabel.enabled = false
    self.m_Bg.color = Color.black
    self.m_Bg.alpha = 0.5
    self.m_Bg.enabled = true

    self:ShowLeafFx()
end

function LuaLeafWithMemoryWnd:OnDisable()
    self.m_LeafFx.OnLoadFxFinish = nil
    self:CancelWaitLeafStopTick()
end

function LuaLeafWithMemoryWnd:ShowLeafFx()
    self.m_LeafFx.OnLoadFxFinish = DelegateFactory.Action(function() self:OnEffectLoad() end)

    if CLuaTaskMgr.m_LeafWithMemoryWnd_ColorId == 1 then
        self.m_LeafFx:LoadFx("fx/ui/prefab/UI_shuye_feichu_01.prefab")
    else
        self.m_LeafFx:LoadFx("fx/ui/prefab/UI_shuye_feichu_02.prefab")
    end
end

function LuaLeafWithMemoryWnd:OnEffectLoad()
    self.m_LeafFx.OnLoadFxFinish = nil
    self:RegisterWaitLeafStopTick()
end

function LuaLeafWithMemoryWnd:RegisterWaitLeafStopTick()
    if(self.m_WaitLeafStopTick) then return end
    self.m_WaitLeafStopTick = RegisterTickOnce(function ( ... )
        self:CancelWaitLeafStopTick()
        self.m_QnBtn.OnClick =
        DelegateFactory.Action_QnButton(function(btn)
            self:OnClick()
        end)
        self.m_FingerGuideGo:SetActive(true)
    end, 15000)
end

function LuaLeafWithMemoryWnd:CancelWaitLeafStopTick()
    if self.m_WaitLeafStopTick then
        UnRegisterTick(self.m_WaitLeafStopTick)
        self.m_WaitLeafStopTick = nil
    end
end

function LuaLeafWithMemoryWnd:OnClick()
    self.m_FingerGuideGo:SetActive(false)
    self.m_QnBtn.OnClick = nil
    self.m_Bg.color = Color.white
    self.m_Bg.alpha = 0.5
    local fadeWhiteTween = LuaTweenUtils.TweenAlpha(self.m_Bg, 0, 1, CLuaTaskMgr.m_LeafWithMemoryWnd_FadeAlphaTime)
    self.m_EffectTweenAlpha.enabled = true
    LuaTweenUtils.OnComplete(fadeWhiteTween, function ( ... )
        CLuaTaskMgr.CompleteLeafWithMemoryWnd()
        self:Close()
    end)
end