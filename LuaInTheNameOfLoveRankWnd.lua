local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local QnTableView = import "L10.UI.QnTableView"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"
local Profession = import "L10.Game.Profession"
local CommonDefs = import "L10.Game.CommonDefs"
local EnumClass = import "L10.Game.EnumClass"

LuaInTheNameOfLoveRankWnd = class()
RegistClassMember(LuaInTheNameOfLoveRankWnd, "tableView")
RegistClassMember(LuaInTheNameOfLoveRankWnd, "MainPlayerInfo")

function LuaInTheNameOfLoveRankWnd:Awake()
    self.tableView = self.transform:Find("RankListTableView"):GetComponent(typeof(QnTableView))
    self.MainPlayerInfo = self.transform:Find("RankListTableView/MainPlayerInfo").gameObject
end

function LuaInTheNameOfLoveRankWnd:Init()
    LuaInTheNameOfLoveRankWnd:InitSelf(self.MainPlayerInfo)

    self.tableView.m_DataSource =
        DefaultTableViewDataSource.CreateByCount(
        1,
        function(item, row)
            self:InitItem(item, row)
        end
    )

    self.tableView.m_DataSource.count =
        LuaValentine2019Mgr.m_InTheNameOfLoveOthersInfo and #LuaValentine2019Mgr.m_InTheNameOfLoveOthersInfo or 0
    self.tableView:ReloadData(true, false)
end

function LuaInTheNameOfLoveRankWnd:InitItem(item, row)
    local info =
        LuaValentine2019Mgr.m_InTheNameOfLoveOthersInfo and LuaValentine2019Mgr.m_InTheNameOfLoveOthersInfo[row + 1]

    if not info then
        return
    end

    --背景黑白间隔
    if row % 2 == 0 then
        item:SetBackgroundTexture("common_textbg_02_light")
    else
        item:SetBackgroundTexture("common_textbg_02_dark")
    end

    local table = item.transform:Find("Table"):GetComponent(typeof(UITable))
    local rank = item.transform:Find("Table/rank"):GetComponent(typeof(UILabel))
    local rankSprite = item.transform:Find("Table/rank/rankImage"):GetComponent(typeof(UISprite))
    local name = item.transform:Find("Table/name"):GetComponent(typeof(UILabel))
    local job = item.transform:Find("Table/job"):GetComponent(typeof(UILabel))
    local guild = item.transform:Find("Table/guild"):GetComponent(typeof(UILabel))
    local flowerNum = item.transform:Find("Table/flowerNum"):GetComponent(typeof(UILabel))

    info.rank = tonumber(info.rank)
    rank.text = info.rank > 3 and tostring(info.rank) or ""

    if info.rank == 1 then
        rankSprite.spriteName = "Rank_No.1"
    elseif info.rank == 2 then
        rankSprite.spriteName = "Rank_No.2png"
    elseif info.rank == 3 then
        rankSprite.spriteName = "Rank_No.3png"
    else
        rankSprite.spriteName = ""
        rankSprite.gameObject:SetActive(false)
    end
    rankSprite:MakePixelPerfect()

    name.text = info.playerName
    job.text = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), info.playerClass))
    guild.text = info.guildName
    flowerNum.text = info.flowerNum

    table:Reposition()

    CommonDefs.AddOnClickListener(
        item.gameObject,
        DelegateFactory.Action_GameObject(
            function(go)
                CPlayerInfoMgr.ShowPlayerPopupMenu(
                    info.playerId,
                    EnumPlayerInfoContext.Undefined,
                    EChatPanel.Undefined,
                    nil,
                    nil,
                    CommonDefs.GetDefaultValue(typeof(Vector3)),
                    AlignType.Default
                )
            end
        ),
        true
    )
end

function LuaInTheNameOfLoveRankWnd:InitSelf(item)
    local table = item.transform:Find("Table"):GetComponent(typeof(UITable))
    local rank = item.transform:Find("Table/rank"):GetComponent(typeof(UILabel))
    local name = item.transform:Find("Table/name"):GetComponent(typeof(UILabel))
    local job = item.transform:Find("Table/job"):GetComponent(typeof(UILabel))
    local guild = item.transform:Find("Table/guild"):GetComponent(typeof(UILabel))
    local flowerNum = item.transform:Find("Table/flowerNum"):GetComponent(typeof(UILabel))

    local mainPlayer =  CClientMainPlayer.Inst

    if LuaValentine2019Mgr.m_InTheNameOfLoveSelfRank == -1 then
        rank.text = LocalString.GetString("未上榜")
    else
        rank.text = tostring(LuaValentine2019Mgr.m_InTheNameOfLoveSelfRank)
    end

    if mainPlayer == nil then return end

    name.text = mainPlayer.Name
    job.text = Profession.GetFullName(mainPlayer.Class)
    guild.text = LuaValentine2019Mgr.m_InTheNameOfLoveSelfGuildName
    flowerNum.text = tostring(LuaValentine2019Mgr.m_InTheNameOfLoveSelfFlowerNum)

    table:Reposition()
end

function LuaInTheNameOfLoveRankWnd:OnEnable()
end

function LuaInTheNameOfLoveRankWnd:OnDisable()
end

function LuaInTheNameOfLoveRankWnd:OnDestroy()
end

return LuaInTheNameOfLoveRankWnd
