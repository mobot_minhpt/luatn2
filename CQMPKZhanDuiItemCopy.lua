-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CQMPKZhanDuiItemCopy = import "L10.UI.CQMPKZhanDuiItemCopy"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local EnumClass = import "L10.Game.EnumClass"
local Extensions = import "Extensions"
local L10 = import "L10"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local UISprite = import "UISprite"
CQMPKZhanDuiItemCopy.m_OnJoinQMPKZhanDuiSuccess_CS2LuaHook = function (this, zhanduiId) 
    if zhanduiId ~= this.m_ZhanDuiId then
        return
    end
    this.m_ApllyTextLabel.text = LocalString.GetString("取消申请")
    this.m_HasApplyObj:SetActive(true)
    this.m_IsApplied = true
    CQuanMinPKMgr.Inst.m_ZhanDuiList[this.m_CurrentIndex].m_HasApplied = 1
end
CQMPKZhanDuiItemCopy.m_OnCancelJoinZhanDui_CS2LuaHook = function (this, zhanduiId) 
    if zhanduiId ~= this.m_ZhanDuiId then
        return
    end
    this.m_ApllyTextLabel.text = LocalString.GetString("申请")
    this.m_HasApplyObj:SetActive(false)
    this.m_IsApplied = false
    CQuanMinPKMgr.Inst.m_ZhanDuiList[this.m_CurrentIndex].m_HasApplied = 0
end
CQMPKZhanDuiItemCopy.m_Init_CS2LuaHook = function (this, zhanDui, row) 
    this.m_CurrentIndex = row
    if row % 2 == 0 then
        this:SetBackgroundTexture(Constants.WhiteItemBgSpriteName)
    else
        this:SetBackgroundTexture(Constants.GreyItemBgSpriteName)
    end
    this.m_ZhanDuiId = zhanDui.m_Id
    this.m_NameLabel.text = zhanDui.m_Name
    this.m_SloganLabel.text = zhanDui.m_Slogan
    Extensions.RemoveAllChildren(this.m_MemberGrid.transform)
    do
        local i = 0 local len = zhanDui.m_Member.Length
        while i < len do
            local go = NGUITools.AddChild(this.m_MemberGrid.gameObject, this.m_MemberObj)
            go:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(go, typeof(UISprite)).spriteName = L10.Game.Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), zhanDui.m_Member[i]))
            i = i + 1
        end
    end
    this.m_MemberGrid:Reposition()
    if zhanDui.m_Member.Length >= CQuanMinPKMgr.Inst.m_ZhanDuiMemberMaxCount then
        this.m_FullZhanDuiObj:SetActive(true)
        this.m_ApplyBtn:SetActive(false)
        this.m_HasApplyObj:SetActive(false)
    else
        this.m_IsApplied = zhanDui.m_HasApplied > 0
        this.m_FullZhanDuiObj:SetActive(false)
        this.m_ApplyBtn:SetActive(true)
        if this.m_IsApplied then
            this.m_ApllyTextLabel.text = LocalString.GetString("取消申请")
        else
            this.m_ApllyTextLabel.text = LocalString.GetString("申请")
        end
        this.m_HasApplyObj:SetActive(this.m_IsApplied)
    end
end
CQMPKZhanDuiItemCopy.m_OnApplyBtnClicked_CS2LuaHook = function (this, go) 
    if not this.m_IsApplied then
        Gac2Gas.RequestJoinQmpkZhanDui(this.m_ZhanDuiId)
    else
        this.m_ApllyTextLabel.text = LocalString.GetString("申请")
        this.m_HasApplyObj:SetActive(false)
        Gac2Gas.RequestCancelJoinQmpkZhanDui(this.m_ZhanDuiId)
        this.m_IsApplied = false
        CQuanMinPKMgr.Inst.m_ZhanDuiList[this.m_CurrentIndex].m_HasApplied = 0
    end
end
