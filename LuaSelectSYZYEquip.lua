require("3rdParty/ScriptEvent")
require("common/common_include")
local CQianKunDaiMgr=import "L10.Game.CQianKunDaiMgr"
local LuaGameObject=import "LuaGameObject"
local LocalString = import "LocalString"
local MessageMgr = import "L10.Game.MessageMgr"
local Gac2Gas = import "L10.Game.Gac2Gas"
local CItemMgr = import "L10.Game.CItemMgr"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local Vector3 = import "UnityEngine.Vector3"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local GameObject = import "UnityEngine.GameObject"
local QnCheckBox = import "L10.UI.QnCheckBox"
local MessageWndManager = import "L10.UI.MessageWndManager"
local FeiSheng_QianKunDai = import "L10.Game.FeiSheng_QianKunDai"

CLuaSelectSYZYEquip=class()
RegistClassMember(CLuaSelectSYZYEquip, "mItemTemplate")
RegistClassMember(CLuaSelectSYZYEquip, "mStatusLabel")
RegistClassMember(CLuaSelectSYZYEquip, "mStatusSprite")
RegistClassMember(CLuaSelectSYZYEquip, "mDescLabel")
RegistClassMember(CLuaSelectSYZYEquip, "mNoEquipLabel")
RegistClassMember(CLuaSelectSYZYEquip, "mTargetLabel")
RegistClassMember(CLuaSelectSYZYEquip, "mLianhuaBtn")
RegistClassMember(CLuaSelectSYZYEquip, "mTable")
RegistClassMember(CLuaSelectSYZYEquip, "mScrollView")
RegistClassMember(CLuaSelectSYZYEquip, "m_RefineDoneTypeScore")

RegistClassMember(CLuaSelectSYZYEquip, "mSelectedEquip")
RegistClassMember(CLuaSelectSYZYEquip, "mCheckBoxList")
RegistClassMember(CLuaSelectSYZYEquip, "mSelectedSpriteList")


function CLuaSelectSYZYEquip:Awake()
    self.mItemTemplate= LuaGameObject.GetChildNoGC(self.transform, "ItemTemplate").gameObject
    self.mItemTemplate:SetActive(false)
    self.mStatusLabel = LuaGameObject.GetChildNoGC(self.transform, "StatusLabel").label
    self.mStatusSprite = LuaGameObject.GetChildNoGC(self.transform, "StatusSprite").sprite
    self.mDescLabel= LuaGameObject.GetChildNoGC(self.transform, "DescLabel").label
    self.mNoEquipLabel= LuaGameObject.GetChildNoGC(self.transform, "NoEquipLabel").label
    self.mTargetLabel= LuaGameObject.GetChildNoGC(self.transform, "ResLabel").label
    self.mLianhuaBtn= LuaGameObject.GetChildNoGC(self.transform, "BtnLianhua").gameObject
    CommonDefs.AddOnClickListener(self.mLianhuaBtn,DelegateFactory.Action_GameObject(function(go)
            self:OnLianhuaBtnClick(go)
        end),false)
    self.mTable = LuaGameObject.GetChildNoGC(self.transform, "FurnitureScroll").grid
    
    local fs = LuaGameObject.GetChildNoGC(self.transform, "FurnitureScroll")
    self.mScrollView = fs.gameObject:GetComponent(typeof(CUIRestrictScrollView))

	local feishengDData = FeiSheng_QianKunDai.GetData()
	self.m_RefineDoneTypeScore = feishengDData.RefineDoneTypeScore
end

function CLuaSelectSYZYEquip:OnLianhuaBtnClick(go)
	if not next(self.mSelectedEquip) then
		MessageMgr.Inst:ShowMessage("Shou_Yuan_Zhen_Yin_No_Equip_Selected", {})
		return 
	end
	
	local bContainPrecious = false
	for itemId, _ in pairs(self.mSelectedEquip) do
		local item = CItemMgr.Inst:GetById(itemId)
		if item and item.IsPrecious then
			bContainPrecious = true
			break
		end
	end
	
	local onOk = function()
			for itemId, _ in pairs(self.mSelectedEquip) do
				local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
				if pos then
					Gac2Gas.RequestRefineQianKunDai(1, pos, itemId, CQianKunDaiMgr.Lua_AntiType)
				end
			end
			CUIManager.CloseUI(CUIResources.SelectSYZYEquip)
		end
	if bContainPrecious then
		local str=MessageMgr.Inst:FormatMessage("Shou_Yuan_Zhen_Yin_Lianhua_Confirm",{})
		MessageWndManager.ShowOKCancelMessage(str,DelegateFactory.Action(onOk),nil,LocalString.GetString("炼化"),nil,false)
	else
		onOk()
	end
end

function CLuaSelectSYZYEquip:OnEnable()
    g_ScriptEvent:AddListener("OnShouYuanZhenYingUpdate", self, "Init")
end

function CLuaSelectSYZYEquip:OnDisable()
    g_ScriptEvent:RemoveListener("OnShouYuanZhenYingUpdate", self, "Init")
end

function CLuaSelectSYZYEquip:GetAntiName(antiType)
	local AntiType2Name = {
		[1] = LocalString.GetString("风"),
		[2] = LocalString.GetString("火"),
		[3] = LocalString.GetString("冰"),
		[4] = LocalString.GetString("电"),
		[5] = LocalString.GetString("毒"),
		[6] = LocalString.GetString("光"),
		[7] = LocalString.GetString("幻"),
		[8] = LocalString.GetString("水"),
	}
	return antiType and AntiType2Name[antiType] or ""
end

function CLuaSelectSYZYEquip:GetAntiSpriteName(antiType)
	local AntiType2SpriteName = {
		[1] = "skill_attextmode_feng",
		[2] = "skill_attextmode_huo",
		[3] = "skill_attextmode_bing",
		[4] = "skill_attextmode_dian",
		[5] = "skill_attextmode_du",
		[6] = "skill_attextmode_guang",
		[7] = "skill_attextmode_huan",
		[8] = "skill_attextmode_shui",
	}
	return antiType and AntiType2SpriteName[antiType] or ""
end

function CLuaSelectSYZYEquip:Init()
	self.mSelectedEquip = {}
	self.mCheckBoxList = {}
	self.mSelectedSpriteList = {}
	Extensions.RemoveAllChildren(self.mTable.transform)
	
	local count = 0
	local IterateDataTable = function(key, val)
			local item = CItemMgr.Inst:GetById(key)
			if item then
				count = count + 1
				
				local itemGO = GameObject.Instantiate(self.mItemTemplate.gameObject)
				itemGO.transform.parent = self.mTable.transform
				itemGO.transform.localScale = Vector3.one
				itemGO:SetActive(true)
				
				local itemdata = EquipmentTemplate_Equip.GetData(item.TemplateId)
				if itemdata then
					local tex = LuaGameObject.GetChildNoGC(itemGO.transform, "Texture").cTexture
					if tex then
						tex:LoadMaterial(itemdata.Icon)
					end
					
					local qSprite = LuaGameObject.GetChildNoGC(itemGO.transform, "QualitySprite").sprite
					if qSprite then
						qSprite.spriteName = CUICommonDef.GetItemCellBorder(item.Equip.QualityType)
					end
					
					local sSprite = LuaGameObject.GetChildNoGC(itemGO.transform, "BtnSelectSprite").sprite
					if sSprite then
						sSprite.gameObject:SetActive(false)
						self.mSelectedSpriteList[key] = sSprite
					end
					
					local qCheckGO = LuaGameObject.GetChildNoGC(itemGO.transform, "CheckBox").gameObject
					local qCheck = qCheckGO and qCheckGO:GetComponent(typeof(QnCheckBox))
					if qCheck then
						qCheck:SetSelected(false, true)						
						self.mCheckBoxList[key] = qCheck
					end
					
					CommonDefs.AddOnClickListener(itemGO, DelegateFactory.Action_GameObject(function(go)
		            self:OnEquipItemClick(key)
		        end),false)
				end
			end
		end
	CommonDefs.DictIterate(CQianKunDaiMgr.Lua_CadidateEquipItem, DelegateFactory.Action_object_object(IterateDataTable))
	self.mTable:Reposition()
	self.mScrollView:ResetPosition()
	
	self.mNoEquipLabel.gameObject:SetActive(count<=0)
	
	local val = CQianKunDaiMgr.Lua_CurAntiScore[CQianKunDaiMgr.Lua_AntiType-1]
	if val then
		local percent = val * 100 / self.m_RefineDoneTypeScore
		self.mStatusLabel.text = SafeStringFormat3(LocalString.GetString("炼化进度:[c][00FF00]%.1f%%[-][/c]"), percent)
	end
	self.mStatusSprite.spriteName = self:GetAntiSpriteName(CQianKunDaiMgr.Lua_AntiType)
	
	local antiName = self:GetAntiName(CQianKunDaiMgr.Lua_AntiType)
	self.mDescLabel.text = SafeStringFormat3(LocalString.GetString("炼化带%s元素抗的红、紫装(鬼装)，可增加%s阵旗炼化进度。"), antiName, antiName)
	
	self:UpdateTargetLabel()
end

function CLuaSelectSYZYEquip:OnEquipItemClick(itemId)
  local item = CItemMgr.Inst:GetById(itemId)
  if item then
	  if self.mSelectedEquip[itemId] then
			self.mSelectedEquip[itemId] = nil
			if self.mCheckBoxList[itemId] then
				self.mCheckBoxList[itemId]:SetSelected(false, true)
			end
	  else
			self.mSelectedEquip[itemId] = true
			if self.mCheckBoxList[itemId] then
				self.mCheckBoxList[itemId]:SetSelected(true, true)
			end
	  end
	  self:UpdateTargetLabel()
  	CItemInfoMgr.ShowLinkItemInfo(itemId,false,nil,AlignType.Default, self.mStatusSprite.transform.position.x, self.mStatusSprite.transform.position.y, self.mStatusSprite.width, self.mStatusSprite.height)
  	
  	for k, sprite in pairs(self.mSelectedSpriteList) do
  		sprite.gameObject:SetActive(k == itemId)
  	end
  end
end

function CLuaSelectSYZYEquip:UpdateTargetLabel()
	local totalVal = 0
	for itemId, _ in pairs(self.mSelectedEquip) do
		local val = CommonDefs.DictGetValue(CQianKunDaiMgr.Lua_CadidateEquipItem, typeof(cs_string), itemId) or 0
		totalVal = totalVal + val
	end
	self.mTargetLabel.text = SafeStringFormat3(LocalString.GetString("炼化后进度增加:[c][00FF00]%.1f%%[-][/c]"), totalVal)
end

return CLuaSelectSYZYEquip
