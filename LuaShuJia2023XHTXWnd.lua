local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"
local QnButton = import "L10.UI.QnButton"
local UILabel = import "UILabel"
local QnTableView = import "L10.UI.QnTableView"
local UISlider = import "UISlider"
local Transform = import "UnityEngine.Transform"
local CButton = import "L10.UI.CButton"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CTrackMgr = import "L10.Game.CTrackMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local Color = import "UnityEngine.Color"
local Ease = import "DG.Tweening.Ease"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Vector4 = import "UnityEngine.Vector4"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaShuJia2023XHTXWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaShuJia2023XHTXWnd, "TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaShuJia2023XHTXWnd, "TabAlert1", "TabAlert1", GameObject)
RegistChildComponent(LuaShuJia2023XHTXWnd, "TabAlert2", "TabAlert2", GameObject)
RegistChildComponent(LuaShuJia2023XHTXWnd, "TipButton", "TipButton", QnButton)
RegistChildComponent(LuaShuJia2023XHTXWnd, "EXPBonusLab", "EXPBonusLab", UILabel)
RegistChildComponent(LuaShuJia2023XHTXWnd, "TaskEXPTip", "TaskEXPTip", GameObject)
RegistChildComponent(LuaShuJia2023XHTXWnd, "TaskEXPTipLab", "TaskEXPTipLab", UILabel)
RegistChildComponent(LuaShuJia2023XHTXWnd, "TeamState", "TeamState", GameObject)
RegistChildComponent(LuaShuJia2023XHTXWnd, "TaskState", "TaskState", GameObject)
RegistChildComponent(LuaShuJia2023XHTXWnd, "MemberTable", "MemberTable", QnTableView)
RegistChildComponent(LuaShuJia2023XHTXWnd, "TaskTable", "TaskTable", QnTableView)
RegistChildComponent(LuaShuJia2023XHTXWnd, "TotalProgressLab", "TotalProgressLab", UILabel)
RegistChildComponent(LuaShuJia2023XHTXWnd, "TotalSlider", "TotalSlider", UISlider)
RegistChildComponent(LuaShuJia2023XHTXWnd, "CheckPoints", "CheckPoints", Transform)
RegistChildComponent(LuaShuJia2023XHTXWnd, "PointTemplate", "PointTemplate", GameObject)
RegistChildComponent(LuaShuJia2023XHTXWnd, "InviteTipLab", "InviteTipLab", UILabel)
RegistChildComponent(LuaShuJia2023XHTXWnd, "FindNpcBtn", "FindNpcBtn", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaShuJia2023XHTXWnd, "m_PlayerInfos")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_TaskInfos")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_SliderWidth")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_RewardedData")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_OldProgress")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_NormalPassBonce")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_HighPassBonce")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_FeiShengLabCol")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_NormalLabCol")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_LvNormalLabCol")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_HighlightLabCol")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_ProgressMax")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_RewardCount")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_TotalProgress")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_RewardInfos")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_PassId")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_RewardOffse")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_ValidPlayerCount")

RegistClassMember(LuaShuJia2023XHTXWnd, "m_NoneTeamState")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_NoneTipLab")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_InviteBtn")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_FindNpcBtn2")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_RightTop")
RegistClassMember(LuaShuJia2023XHTXWnd, "m_TabBarIdx")
function LuaShuJia2023XHTXWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_NoneTeamState= self.transform:Find("Anchor/RightMiddle/NoneTeamState").gameObject
    self.m_NoneTipLab   = self.transform:Find("Anchor/RightMiddle/NoneTeamState/DefaultLabel"):GetComponent(typeof(UILabel))
    self.m_InviteBtn    = self.transform:Find("Anchor/RightMiddle/NoneTeamState/InviteBtn"):GetComponent(typeof(CButton))
    self.m_FindNpcBtn2  = self.transform:Find("Anchor/RightMiddle/NoneTeamState/FindNpcBtn"):GetComponent(typeof(CButton))
    self.m_RightTop     = self.transform:Find("Anchor/RightTop").gameObject

    self.PointTemplate:SetActive(false)

    self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabBarTabChange(index)
	end)

    self.MemberTable.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_PlayerInfos
        end,
        function(item, index)
            self:InitPlayerItem(item, index, self.m_PlayerInfos[index+1])
        end
    )

    self.MemberTable.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)

    self.TaskTable.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_TaskInfos
        end,
        function(item, index)
            self:InitTaskItem(item, self.m_TaskInfos[index+1])
        end
    )

    self.TipButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        g_MessageMgr:ShowMessage("SHUJIA2023_XHTX_TIP")
    end)

    CommonDefs.AddOnClickListener(self.FindNpcBtn.gameObject, DelegateFactory.Action_GameObject(function(go)
        self:OnFindNpcBtnClicked(go)
    end), false)

    CommonDefs.AddOnClickListener(self.m_FindNpcBtn2.gameObject, DelegateFactory.Action_GameObject(function(go)
        self:OnFindNpcBtnClicked(go)
    end), false)

    CommonDefs.AddOnClickListener(self.m_InviteBtn.gameObject, DelegateFactory.Action_GameObject(function(go)
        if CClientMainPlayer.Inst == nil then
            return
        end
        local playerId = CClientMainPlayer.Inst.Id
        local playerName = CClientMainPlayer.Inst.Name
        self:ShowCreateTeamPopMenu(playerId, playerName, go)
    end), false)

    self.InviteTipLab.text  = g_MessageMgr:FormatMessage("SHUJIA2023_XHTX_INVITETIP")
    self.TaskEXPTipLab.text = g_MessageMgr:FormatMessage("SHUJIA2023_XHTX_EXPTIP")
    self.m_NoneTipLab.text  = g_MessageMgr:FormatMessage("SHUJIA2023_XHTX_NONETEAMTIP")

    local bonceDatas        = g_LuaUtil:StrSplit(ShuJia2023_Setting.GetData("XHTXVip2Rate").Value, ",")
    self.m_NormalPassBonce  = tonumber(bonceDatas[1]) * 0.01
    self.m_HighPassBonce    = tonumber(bonceDatas[2]) * 0.01
    self.m_FeiShengLabCol   = NGUIText.ParseColor24("E76700", 0)
    self.m_NormalLabCol     = NGUIText.ParseColor24("FFFFFF", 0)
    self.m_LvNormalLabCol   = NGUIText.ParseColor24("6B3E16", 0)
    self.m_HighlightLabCol  = NGUIText.ParseColor24("A6FF94", 0)

    self.TeamState:SetActive(true)
    self.TaskState:SetActive(false) 
    self.TabAlert1:SetActive(false)
    self.TabAlert2:SetActive(false)
    self.EXPBonusLab.text = "0%"
    self.TotalProgressLab.text = 0
    self.TotalSlider.value = 0
    self.m_TotalProgress = 0
    self.m_ValidPlayerCount = 0
    self.m_PlayerInfos = {}

    self.m_SliderWidth = self.TotalSlider.mFG.width
    self.m_RewardCount = 0
    self.m_ProgressMax = 0
    self.m_PassId = tonumber(ShuJia2023_Setting.GetData("ShuJiaPassId").Value)
    local rewardDatas = g_LuaUtil:StrSplit(ShuJia2023_Setting.GetData("XHTXRewardId").Value, ";")
    self.m_RewardInfos = {}
    for i = 1, #rewardDatas do
        local rewardStr = rewardDatas[i]
        if rewardStr ~= "" then
            local rewardData = g_LuaUtil:StrSplit(rewardStr, ",")
            local pointVal = tonumber(rewardData[1])
            local itemId = tonumber(rewardData[2])
            table.insert(self.m_RewardInfos, {pointVal = pointVal, status = 0, itemId = itemId})
            self.m_RewardCount = self.m_RewardCount + 1
            self.m_ProgressMax = math.max(self.m_ProgressMax, pointVal)
        end
    end
end

function LuaShuJia2023XHTXWnd:Init()
    self.m_TabBarIdx = 0
    self.TabBar:ChangeTab(0, false)

    self:RefreshTaskInfo()
    Gac2Gas.XianHaiTongXingPlayerQueryTeamInfo()
end

function LuaShuJia2023XHTXWnd:OnEnable()
    g_ScriptEvent:AddListener("CommonPassortUnlockVipSuccess", self, "OnCommonPassortUnlockVipSuccess")
    g_ScriptEvent:AddListener("XianHaiTongXingPlayerQueryTeamInfo_Result", self, "OnXianHaiTongXingPlayerQueryTeamInfo_Result")
    g_ScriptEvent:AddListener("XianHaiTongXingPlayerQueryTeamProgress_Result", self, "OnXianHaiTongXingPlayerQueryTeamProgress_Result")
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey", self, "OnUpdateTempPlayDataWithKey")
end

function LuaShuJia2023XHTXWnd:OnDisable()
    g_ScriptEvent:RemoveListener("CommonPassortUnlockVipSuccess", self, "OnCommonPassortUnlockVipSuccess")
    g_ScriptEvent:RemoveListener("XianHaiTongXingPlayerQueryTeamInfo_Result", self, "OnXianHaiTongXingPlayerQueryTeamInfo_Result")
    g_ScriptEvent:RemoveListener("XianHaiTongXingPlayerQueryTeamProgress_Result", self, "OnXianHaiTongXingPlayerQueryTeamProgress_Result")
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey", self, "OnUpdateTempPlayDataWithKey")
end
--@region UIEvent
function LuaShuJia2023XHTXWnd:OnTabBarTabChange(index)
    self.m_TabBarIdx = index
    local isNoneTeam = #self.m_PlayerInfos == 0
    self.m_NoneTeamState:SetActive(index == 0 and isNoneTeam)
    self.TeamState:SetActive(index == 0 and not isNoneTeam)

    self.m_RightTop:SetActive(not(index == 0 and isNoneTeam))
    self.FindNpcBtn.gameObject:SetActive(index == 0)
    self.InviteTipLab.gameObject:SetActive(index == 0)

    self.TaskState:SetActive(index == 1)
    self.TaskEXPTip:SetActive(index == 1)

end

function LuaShuJia2023XHTXWnd:OnFindNpcBtnClicked(go)
    local npcDatas = g_LuaUtil:StrSplit(ShuJia2023_Setting.GetData("XHTXNPC").Value, ";")[1]
    local npcData = g_LuaUtil:StrSplit(npcDatas, ",")
    CTrackMgr.Inst:FindNPC(tonumber(npcData[4]), tonumber(npcData[1]), tonumber(npcData[2]), tonumber(npcData[3]), nil, nil)
    CUIManager.instance:CloseAllPopupViews()
end
--@endregion UIEvent
function LuaShuJia2023XHTXWnd:OnCommonPassortUnlockVipSuccess(passId, vip1Unlock, vip2Unlock)
    if self.m_PassId == passId and CClientMainPlayer.Inst then
        for i = 1, #self.m_PlayerInfos do
            local playerInfo = self.m_PlayerInfos[i]
            if playerInfo.isValid and playerInfo.playerId == CClientMainPlayer.Inst.Id then
                if vip1Unlock then
                    playerInfo.normalPass = true
                end
                if vip2Unlock then
                    playerInfo.highPass = true
                end
                break
            end
        end

        local totalBonce = 0
        for i = 1, #self.m_PlayerInfos do
            local playerInfo = self.m_PlayerInfos[i]
            totalBonce = totalBonce + (playerInfo.normalPass and self.m_NormalPassBonce or 0) + (playerInfo.highPass and self.m_HighPassBonce or 0)
        end
        self.EXPBonusLab.text = SafeStringFormat3("+%.f%%", totalBonce * 100)
        
        self.MemberTable:ReloadData(true, true)
    end
end
function LuaShuJia2023XHTXWnd:OnXianHaiTongXingPlayerQueryTeamInfo_Result(member1Id, member2Id, member3Id, member4Id, member5Id, vipInfo, progress, extraInfo)
    self.m_ValidPlayerCount = 0
    self.m_TotalProgress = progress
    self.m_PlayerInfos = {}
    for i = 1, #extraInfo, 6 do
        local playerInfo = {
            isValid = extraInfo[i + 1] ~= 0 and extraInfo[i + 3] ~= 0,
            name = extraInfo[i],
            class = extraInfo[i + 1],
            gender = extraInfo[i + 2],
            level = extraInfo[i + 3],
            isFeisheng = extraInfo[i + 4],
            isOnline = extraInfo[i + 5],
        }
        table.insert(self.m_PlayerInfos, playerInfo)
        self.m_ValidPlayerCount = self.m_ValidPlayerCount + (playerInfo.isValid and 1 or 0)
    end

    local totalBonce = 0
    for i = 1, #self.m_PlayerInfos do
        local playerInfo = self.m_PlayerInfos[i]
        playerInfo.normalPass = bit.band(vipInfo, bit.lshift(1, i * 2 - 2)) > 0
        playerInfo.highPass = bit.band(vipInfo, bit.lshift(1, i * 2 - 1)) > 0
        totalBonce = totalBonce + (playerInfo.normalPass and self.m_NormalPassBonce or 0) + (playerInfo.highPass and self.m_HighPassBonce or 0)
    end

    if #self.m_PlayerInfos == 5 then
        self.m_PlayerInfos[1].playerId = member1Id
        self.m_PlayerInfos[2].playerId = member2Id
        self.m_PlayerInfos[3].playerId = member3Id
        self.m_PlayerInfos[4].playerId = member4Id
        self.m_PlayerInfos[5].playerId = member5Id
    end

    self.EXPBonusLab.text = SafeStringFormat3("+%.f%%", totalBonce * 100)

    self.MemberTable:ReloadData(true, true)
    self.TabBar:ChangeTab(self.m_TabBarIdx, false)

    self:InitRewardSilder()
end

function LuaShuJia2023XHTXWnd:OnXianHaiTongXingPlayerQueryTeamProgress_Result(progress)
    self.m_TotalProgress = progress
    self:InitRewardSilder()
end

function LuaShuJia2023XHTXWnd:OnUpdateTempPlayDataWithKey(args)
    local key = args[0]
    if key == EnumTempPlayDataKey_lua.eXianHaiTongXingData then
        self:RefreshTaskInfo()
    end
end

function LuaShuJia2023XHTXWnd:InitRewardSilder()
    for i = 1, #self.m_RewardInfos do 
        local rewardInfo = self.m_RewardInfos[i]
        local status = 0
        if bit.band(self.m_RewardedData, bit.lshift(1, i - 1)) > 0 then
            status = 2
        elseif self.m_OldProgress >= rewardInfo.pointVal then
            status = 2
        elseif rewardInfo.pointVal <= self.m_TotalProgress then
            status = 1
        end
        rewardInfo.status = status
    end
    for i = 1, #self.m_RewardInfos - self.CheckPoints.childCount do
        local go = NGUITools.AddChild(self.CheckPoints.gameObject, self.PointTemplate)
        go:SetActive(true)
    end

    for i = 0, self.CheckPoints.childCount - 1 do
        local item = self.CheckPoints:GetChild(i)
        self:InitRewardItem(item, i, self.m_RewardInfos[i+1])
    end
    
    local maxVal = self.m_ProgressMax
    local curVal = self.m_TotalProgress
    self.TotalSlider.value = curVal / maxVal
    self.TotalProgressLab.text = curVal
    self.TotalSlider.thumb.gameObject:SetActive(curVal ~= 0)
end


function LuaShuJia2023XHTXWnd:RefreshTaskInfo()
    local timeOffset = CServerTimeMgr.Inst.timeStamp - CServerTimeMgr.Inst:GetTimeStampByStr(ShuJia2023_Setting.GetData("XHTXBeginTime").Value)
    timeOffset = math.max(timeOffset, 0)
    local curWeek = math.ceil(timeOffset / 604800)

    local taskInfos = {}
    local taskId2Count = {}
    local taskId2Status = {}
    self.m_RewardedData = 0
    self.m_OldProgress = 0
    if CClientMainPlayer.Inst and CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumTempPlayDataKey_lua.eXianHaiTongXingData) then
        local playDataU = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumTempPlayDataKey_lua.eXianHaiTongXingData)
        local tab = g_MessagePack.unpack(playDataU.Data.Data)
        if tab then
            local week = tab[EnumXianHaiTongXingDataType.eWeek]
            self.m_OldProgress = (tab[EnumXianHaiTongXingDataType.eOldProgress] or {})[week] or 0
            self.m_RewardedData = (tab[EnumXianHaiTongXingDataType.eReward] or {})[week] or 0
            taskId2Count = tab[EnumXianHaiTongXingDataType.eTaskCount] or {}
            taskId2Status = tab[EnumXianHaiTongXingDataType.eTaskStatus] or {}
        end
    end

    ShuJia2023_XHTXTask.ForeachKey(function (key) 
        local taskInfo = {}
        taskInfo.taskId = key
        local data = ShuJia2023_XHTXTask.GetData(key)
        if data and data.Week == curWeek then
            taskInfo.count = taskId2Count[key] or 0
            taskInfo.status = taskId2Status[key] or 0
            taskInfo.Desc = data.Desc
            taskInfo.Target = data.Target
            taskInfo.ProgressReward = data.ProgressReward
            table.insert(taskInfos, taskInfo)
        end
    end)

    table.sort(taskInfos, function(a, b)
        return a.status < b.status or (a.status == b.status and a.taskId < b.taskId)
	end)

    self.m_TaskInfos = taskInfos
    self.TaskTable:ReloadData(true, true)
    
    self:InitRewardSilder()
end

function LuaShuJia2023XHTXWnd:InitPlayerItem(item, index, info)
    local icon          = item:GetComponent(typeof(CUITexture))
    local lvLab         = item.transform:Find("PlayerInfo/PlayerLevel"):GetComponent(typeof(UILabel))
    local nameLab       = item.transform:Find("PlayerInfo/PlayerName"):GetComponent(typeof(UILabel))
    local leader        = item.transform:Find("PlayerInfo/LeaderIcon").gameObject
    local normalPass    = item.transform:Find("PlayerInfo/NormalPass"):GetComponent(typeof(UITexture))
    local highPass      = item.transform:Find("PlayerInfo/HighPass"):GetComponent(typeof(UITexture))
    local normalBonceLab= item.transform:Find("PlayerInfo/NormalPassBonceLab"):GetComponent(typeof(UILabel))
    local highBonceLab  = item.transform:Find("PlayerInfo/HighPassBonceLab"):GetComponent(typeof(UILabel))
    local noneTip       = item.transform:Find("NoneTip").gameObject
    local opBtn         = item.transform:Find("OpBtn"):GetComponent(typeof(CButton))

    local isValid = info.isValid
    local isMainPlayer = info.playerId == CClientMainPlayer.Inst.Id
    local isFeisheng = info.isFeisheng == 1
    local isLeader = index == 0

    if isValid then
        local portraitName = CUICommonDef.GetPortraitName(info.class, info.gender, -1)
        icon:LoadNPCPortrait(portraitName, false)

        lvLab.text = info.level
        lvLab.color = isFeisheng and self.m_FeiShengLabCol or self.m_LvNormalLabCol

        nameLab.text = info.name
        nameLab.color = isMainPlayer and self.m_HighlightLabCol or self.m_NormalLabCol

        leader:SetActive(isLeader)

        normalPass.gameObject:SetActive(true)
        highPass.gameObject:SetActive(true)
        normalBonceLab.gameObject:SetActive(true)
        highBonceLab.gameObject:SetActive(true)

        -- 低阶战令
        normalPass.alpha = info.normalPass and 1 or 0.3
        normalBonceLab.text = info.normalPass and SafeStringFormat3("+%.f%%", self.m_NormalPassBonce * 100) or "+0%"
        normalBonceLab.alpha = info.normalPass and 1 or 0.5
        -- 高阶战令
        highPass.alpha = info.highPass and 1 or 0.3
        highBonceLab.text = info.highPass and  SafeStringFormat3("+%.f%%", self.m_HighPassBonce * 100) or "+0%"
        highBonceLab.alpha = info.highPass and 1 or 0.5

        noneTip:SetActive(false)
        if info.normalPass and info.highPass then
            opBtn.gameObject:SetActive(false)
        else
            opBtn.gameObject:SetActive(true)
            opBtn.label.text = isMainPlayer and LocalString.GetString("购买海图") or LocalString.GetString("赠送海图")
            CommonDefs.AddOnClickListener(opBtn.gameObject, DelegateFactory.Action_GameObject(function(go)
                if isMainPlayer then
                    -- 购买战令
                    LuaCommonPassportMgr:OpenVIPUnlockWnd(self.m_PassId)
                else
                    -- 赠送战令
                    if info.isOnline then
                        LuaShuJia2023Mgr.m_PresentPlayerName = info.name
                        LuaShuJia2023Mgr.m_PresentPlayerId = info.playerId
                        LuaShuJia2023Mgr.m_PresentPlayerVipStatus = {info.normalPass, info.highPass}
                        CUIManager.ShowUI(CLuaUIResources.ShuJia2023XHTXPresentGivenWnd)
                    else
                        g_MessageMgr:ShowMessage("SHUJIA2023_XHTX_PRESENTPASS_NOONLINE")
                    end
                end
            end), false)
        end
    else
        icon:LoadMaterial("")
        lvLab.text = ""
        nameLab.text = ""
        leader:SetActive(false)
        normalPass.gameObject:SetActive(false)
        highPass.gameObject:SetActive(false)
        normalBonceLab.gameObject:SetActive(false)
        highBonceLab.gameObject:SetActive(false)
        noneTip:SetActive(true)
        opBtn.gameObject:SetActive(true)
        opBtn.label.text = LocalString.GetString("邀请加入")
        CommonDefs.AddOnClickListener(opBtn.gameObject, DelegateFactory.Action_GameObject(function(go)
            -- 邀请加入
            self:ShowShareTeamPopMenu(self.m_PlayerInfos[1].playerId, self.m_PlayerInfos[1].name, go)
        end), false)
    end
end

function LuaShuJia2023XHTXWnd:OnSelectAtRow(row)
    local info = self.m_PlayerInfos[row+1]
    local item = self.MemberTable:GetItemAtRow(row)

    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == info.playerId then
        local rows = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
        local exitTeam = PopupMenuItemData(LocalString.GetString("退出同行"), DelegateFactory.Action_int(function (index) 
            if CClientMainPlayer.Inst == nil then
                return
            end
            local msg = g_MessageMgr:FormatMessage("SHUJIA2023_XHTX_LEVETEAM_MAKESURE")
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
                Gac2Gas.XianHaiTongXingLeaveTeam(CClientMainPlayer.Inst.Id, false)
            end), nil, nil, nil, false)
        end), false, nil)
    
        CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), exitTeam)
        CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(rows), item.transform, CPopupMenuInfoMgr.AlignType.Center)
    elseif self.m_PlayerInfos[1] and CClientMainPlayer.Inst and self.m_PlayerInfos[1].playerId == CClientMainPlayer.Inst.Id then
        CPlayerInfoMgr.ShowPlayerPopupMenu(info.playerId, EnumPlayerInfoContext.XHTXTeamLeader, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgr.AlignType.Left)
    else
        CPlayerInfoMgr.ShowPlayerPopupMenu(info.playerId, EnumPlayerInfoContext.PersonalSpace, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgr.AlignType.Left)
    end
end

function LuaShuJia2023XHTXWnd:InitTaskItem(item, info)
    local descLab   = item.transform:Find("TaskDesc"):GetComponent(typeof(UILabel))
    local slider    = item.transform:Find("Slider"):GetComponent(typeof(UISlider))
    local sliderLab = item.transform:Find("Slider/SliderLab"):GetComponent(typeof(UILabel))
    local inprogress= item.transform:Find("InProgress").gameObject
    local finished  = item.transform:Find("Finished").gameObject
    local rewardLab = item.transform:Find("TaskReward"):GetComponent(typeof(UILabel))

    descLab.text = info.Desc
    local count = math.min(info.count, info.Target)
    sliderLab.text = SafeStringFormat3("%d/%d", count, info.Target)
    slider.value = count / info.Target
    inprogress:SetActive(info.status == 0)
    finished:SetActive(info.status ~= 0)
    rewardLab.text = SafeStringFormat3("+%d", info.ProgressReward)
end

function LuaShuJia2023XHTXWnd:InitRewardItem(item, index, info)
    local btn           = item:GetComponent(typeof(QnButton))
    local chestTf       = item:Find("Chest")
    local open          = item:Find("Chest/Open").gameObject
    local close         = item:Find("Chest/Close").gameObject
    local pointLab      = item:Find("PointLab"):GetComponent(typeof(UILabel))
    local openableFx    = item:Find("OpenFx").gameObject
    
    local scale = (index + 1) % 3 == 0 and 0.32 or 0.25
    LuaUtils.SetLocalScale(chestTf, scale, scale, 1)
    LuaUtils.SetLocalPosition(item.transform, self.m_SliderWidth * info.pointVal / self.m_ProgressMax, 0, 0)
    close:SetActive(info.status ~= 2)
    open:SetActive(info.status == 2)
    pointLab.text = info.pointVal

    btn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        if info.status == 1 then
            -- 领取奖励
            Gac2Gas.XianHaiTongXingGetReward(index + 1)
        else
            -- 查看信息
            CItemInfoMgr.ShowLinkItemTemplateInfo(info.itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end
    end)

    openableFx:SetActive(info.status == 1)
    LuaTweenUtils.DOKill(openableFx.transform, false)
    if info.status == 1 then
        local b = NGUIMath.CalculateRelativeWidgetBounds(btn.transform)
        local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
        openableFx.transform.localPosition = waypoints[0]
        LuaTweenUtils.DOLuaLocalPath(openableFx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
    end
end

function LuaShuJia2023XHTXWnd:ShowShareTeamPopMenu(teamLeaderId, teamLeaderName, shareBtn)
    if CClientMainPlayer.Inst == nil then
        return
    end
    local shareText = g_MessageMgr:FormatMessage("SHUJIA2023_XHTX_SHARETEAM", teamLeaderName, 5 - self.m_ValidPlayerCount, teamLeaderId, "leader")
    LuaInGameShareMgr.Share(bit.bor(EnumInGameShareChannelDefine.eWorld, EnumInGameShareChannelDefine.eGuild, EnumInGameShareChannelDefine.eTeam) , function(channel)
        return self:GetShareText(channel, shareText)
    end, shareBtn.transform, CPopupMenuInfoMgr.AlignType.Top, nil, self.m_Buttons)
end

function LuaShuJia2023XHTXWnd:ShowCreateTeamPopMenu(playerId, playerName, shareBtn)
    if CClientMainPlayer.Inst == nil then
        return
    end
    local shareText = g_MessageMgr:FormatMessage("SHUJIA2023_XHTX_ZHAOMU", playerId, "leader")
    LuaInGameShareMgr.Share(bit.bor(EnumInGameShareChannelDefine.eWorld, EnumInGameShareChannelDefine.eGuild, EnumInGameShareChannelDefine.eTeam) , function(channel)
        return self:GetShareText(channel, shareText)
    end, shareBtn.transform, CPopupMenuInfoMgr.AlignType.Top, nil, self.m_Buttons)
end

function LuaShuJia2023XHTXWnd:GetShareText(channel, shareText)
    if channel == EnumInGameShareChannelDefine.eWorld then
        local currentTime = CServerTimeMgr.Inst.timeStamp
        local nextTime = LuaShuJia2023Mgr.m_XHTXLastWorldShareTime or 0
        if nextTime >= currentTime then
            g_MessageMgr:ShowMessage("Speak_Wait_For_Time_In_World_Channel", math.ceil(nextTime - currentTime))
            return ""
        end
        LuaShuJia2023Mgr.m_XHTXLastWorldShareTime = currentTime + 30
    end
    return shareText
end