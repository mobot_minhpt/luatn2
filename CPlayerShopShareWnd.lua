-- Auto Generated!!
local CChatHelper = import "L10.UI.CChatHelper"
local CChatLinkMgr = import "CChatLinkMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local CPlayerShopShareWnd = import "L10.UI.CPlayerShopShareWnd"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local LocalString = import "LocalString"
local NGUIText = import "NGUIText"
local QnButton = import "L10.UI.QnButton"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CPlayerShopShareWnd.m_Init_CS2LuaHook = function (this) 
    this.m_ShareToGuildButton.Enabled = (CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst:IsInGuild())
    this.m_ShareToTeamButton.Enabled = (CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.TeamProp:IsInTeam())
    local msg = CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("<link item={0},[{1},{2},{3}]> 售价：{4}\n"), {CPlayerShopShareWnd.m_Item.Item.TemplateId, CPlayerShopShareWnd.m_Item.Item.Id, CPlayerShopShareWnd.m_Item.Item.Name, NGUIText.EncodeColor24(CPlayerShopShareWnd.m_Item.Item.DisplayColor), CPlayerShopShareWnd.m_Item.Price})
    this.m_ItemLabel.text = CChatLinkMgr.TranslateToNGUIText(msg, false)
    this.m_Input.OnValueChanged = MakeDelegateFromCSFunction(this.OnInputChanged, MakeGenericClass(Action1, String), this)
    UIEventListener.Get(this.m_CloseButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    this.m_ShareToFriendButton.OnClick = MakeDelegateFromCSFunction(this.OnShareToFriendButtonClick, MakeGenericClass(Action1, QnButton), this)
    this.m_ShareToGuildButton.OnClick = MakeDelegateFromCSFunction(this.OnShareToGuildButtonClick, MakeGenericClass(Action1, QnButton), this)
    this.m_ShareToTeamButton.OnClick = MakeDelegateFromCSFunction(this.OnShareToTeamButtonClick, MakeGenericClass(Action1, QnButton), this)
end
CPlayerShopShareWnd.m_OnShareToFriendButtonClick_CS2LuaHook = function (this, button) 
    CCommonPlayerListMgr.Inst:ShowFriendsToShareJingLingAnswer(DelegateFactory.Action_ulong(function (playerId) 
        this:ShareItem(CPlayerShopShareWnd.m_Item, EChatPanel.Friend, playerId)
    end))
end
CPlayerShopShareWnd.m_ShareItem_CS2LuaHook = function (this, item, channel, playerId) 
    --<link item=%s,[%s,%s,%s]>
    local input = this.m_Input.Text == nil and "" or this.m_Input.Text
    if CommonDefs.StringLength(input) > 0 then
        local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(input, true)
        if ret.msg == nil then
            return
        else
            input = ret.msg
        end
    end
    local msg = CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("<link item={0},{1},{2},{3}> 售价：{4} {5}"), {
        item.Item.TemplateId, 
        item.Item.Id, 
        item.Item.Name, 
        NGUIText.EncodeColor24(item.Item.DisplayColor), 
        item.Price, 
        input
    })
    CChatHelper.SendMsg(channel, msg, playerId)
    this:Close()
end
