-- Auto Generated!!
local CClientNpc = import "L10.Game.CClientNpc"
local CDangongBulletAndBird = import "CDangongBulletAndBird"
local CRenderObject = import "L10.Engine.CRenderObject"
local Physics = import "UnityEngine.Physics"
local UICamera = import "UICamera"
local Vector3 = import "UnityEngine.Vector3"
CDangongBulletAndBird.m_detectHit_CS2LuaHook = function (this) 
    local ray = UICamera.currentCamera:ScreenPointToRay(UICamera.currentCamera:WorldToScreenPoint(this.cachedTrans.position))
    local mask = UICamera.currentCamera.cullingMask
    local hits = Physics.RaycastAll(ray, System.Int32.MaxValue, mask)
    do
        local i = 0
        while i < hits.Length do
            local go = hits[i].collider.gameObject
            if (string.find(go.name, "BirdTemplate", 1, true) ~= nil) then
                return CommonDefs.GetComponent_GameObject_Type(go, typeof(CDangongBulletAndBird))
            end
            i = i + 1
        end
    end
    return nil
end
CDangongBulletAndBird.m_loadBird_CS2LuaHook = function (this) 
    this.modelRO = CommonDefs.GetComponentInChildren_Component_Type(this, typeof(CRenderObject))
    if this.modelRO then
        this.modelRO:AddLoadAllFinishedListener(MakeDelegateFromCSFunction(this.onRenderObjLoadComplete, MakeGenericClass(Action1, CRenderObject), this))
        CClientNpc.LoadResource(this.modelRO, CDangongBulletAndBird.templateID, true)
        local scale = 1 / UICamera.currentCamera.transform.parent.localScale.x
        this.modelRO.transform.localScale = Vector3(scale, scale, scale)
        this.modelRO.transform.localPosition = Vector3(0, - 600, 0)
    end
end
