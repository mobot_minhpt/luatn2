local UIGrid = import "UIGrid"

local UITable = import "UITable"
local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local UILongPressButton = import "L10.UI.UILongPressButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
LuaShuangshiyi2023LotteryReplacementWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShuangshiyi2023LotteryReplacementWnd, "ItemObj", "ItemObj", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryReplacementWnd, "ReplacementText", "ReplacementText", UILabel)
RegistChildComponent(LuaShuangshiyi2023LotteryReplacementWnd, "ForbiddenItem1", "ForbiddenItem1", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryReplacementWnd, "ForbiddenItem2", "ForbiddenItem2", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryReplacementWnd, "ForbiddenItem3", "ForbiddenItem3", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryReplacementWnd, "ReplacementConsumeText", "ReplacementConsumeText", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryReplacementWnd, "ReplacementOwnText", "ReplacementOwnText", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryReplacementWnd, "ReplacementButton", "ReplacementButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryReplacementWnd, "RuleButton", "RuleButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryReplacementWnd, "AddReplacementButton", "AddReplacementButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryReplacementWnd, "ReplacementBottomTips", "ReplacementBottomTips", UILabel)
RegistChildComponent(LuaShuangshiyi2023LotteryReplacementWnd, "Grid", "Grid", UIGrid)

--@endregion RegistChildComponent end

function LuaShuangshiyi2023LotteryReplacementWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    
    self:InitWndData()
    self:RefreshConstUI()
    self:RefreshVariableUI()
    self:InitUIEvent()
end

function LuaShuangshiyi2023LotteryReplacementWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaShuangshiyi2023LotteryReplacementWnd:InitWndData()
    self.overviewItemData = {}
    self.maxQuality = 0
    
    Double11_RechargeLotteryItems.Foreach(function (cfgId, data)
        local quality = LuaShuangshiyi2023Mgr:GetItemQuality(data.ItemId)
        if quality > self.maxQuality then
            self.maxQuality = quality
        end
        if self.overviewItemData[quality] == nil then
            self.overviewItemData[quality] = {}
        end
        table.insert(self.overviewItemData[quality], cfgId)
    end)

    self.rechargeData = {}
    self.lotteryData = {}
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer and CommonDefs.DictContains_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11RechargeData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11RechargeData)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        self.rechargeData = list
    end
    if mainPlayer and CommonDefs.DictContains_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11LotteryData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11LotteryData)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        self.lotteryData = list
    end
    
    self.forbidItemList = self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eReplacementItems] and self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eReplacementItems] or {}
    self.curSelectItemList = {}
    
    self.isButtonOnPress = false
    self.isButtonOnPressing = false
    self.buttonpressStartTime = 0
    self.longPressThreshold = 0.2
    self.longPressItemId = nil
    self.longPressItemObj = nil
end

function LuaShuangshiyi2023LotteryReplacementWnd:OnClickItem(itemObj, itemId)
    for i = 1, #self.forbidItemList do
        if self.forbidItemList[i] == itemId then
            g_MessageMgr:ShowMessage("Double11_2023Lottery_Replace_SelectBefore")
            return
        end
    end
    
    local isNewSelect = false
    local clickSelectIndex = nil
    for i = 1, #self.curSelectItemList do
        if self.curSelectItemList[i] == itemId then
            isNewSelect = true
            clickSelectIndex = i
            break
        end
    end
    if isNewSelect then
        --点击取消
        table.remove(self.curSelectItemList, clickSelectIndex)
        itemObj.transform:Find("IsSelected").gameObject:SetActive(false)
        itemObj.transform:Find("CheckIconBg/CheckIcon").gameObject:SetActive(false)
        itemObj.transform:Find("XSign").gameObject:SetActive(false)
        self:RefreshVariableUI()
    else
        --点击选中
        if #self.forbidItemList + #self.curSelectItemList >= 3 then
            g_MessageMgr:ShowMessage("Double11_2023Lottery_Replace_On_Select_Over_Three")
            return
        end 
        
        --选中
        table.insert(self.curSelectItemList, itemId)
        itemObj.transform:Find("IsSelected").gameObject:SetActive(true)
        itemObj.transform:Find("CheckIconBg/CheckIcon").gameObject:SetActive(true)
        itemObj.transform:Find("XSign").gameObject:SetActive(true)
        self:RefreshVariableUI()
    end
end

function LuaShuangshiyi2023LotteryReplacementWnd:RefreshConstUI()
    self.ReplacementText.text = g_MessageMgr:FormatMessage("Double11_2023Lottery_Replacement_Tips")
    self.ReplacementBottomTips.text = g_MessageMgr:FormatMessage("Double11_2023Lottery_Replacement_BottomTips")
    
    self.ItemObj:SetActive(false)
    --refresh itemList
    for quality = self.maxQuality, 1, -1 do
        local itemList = self.overviewItemData[quality] and self.overviewItemData[quality] or {}
        for i = 1, #itemList do
            local cfgId = itemList[i]
            local cfgData = Double11_RechargeLotteryItems.GetData(cfgId)
            local itemId = cfgData.ItemId
            local itemData = Item_Item.GetData(itemId)
            
            local templateItem = CommonDefs.Object_Instantiate(self.ItemObj)
            templateItem:SetActive(true)
            templateItem.transform.parent = self.Grid.transform

            local isFormerSelect = false
            for i = 1, #self.forbidItemList do
                if self.forbidItemList[i] == cfgId then
                    isFormerSelect = true
                    break
                end
            end
            if isFormerSelect then
                templateItem.transform:Find("IsSelected").gameObject:SetActive(true)
                templateItem.transform:Find("CheckIconBg/CheckIcon").gameObject:SetActive(true)
                Extensions.SetLocalPositionZ(templateItem.transform:Find("IsSelected"), -1)
                Extensions.SetLocalPositionZ(templateItem.transform:Find("CheckIconBg/CheckIcon"), -1)
            else
                templateItem.transform:Find("IsSelected").gameObject:SetActive(false)
                templateItem.transform:Find("CheckIconBg/CheckIcon").gameObject:SetActive(false)
                Extensions.SetLocalPositionZ(templateItem.transform:Find("IsSelected"), 0)
                Extensions.SetLocalPositionZ(templateItem.transform:Find("CheckIconBg/CheckIcon"), 0)
            end

            local texture = templateItem.transform:Find("Icon"):GetComponent(typeof(CUITexture))
            if itemData then texture:LoadMaterial(itemData.Icon) end
            templateItem.transform:Find("Quality"):GetComponent(typeof(UISprite)).spriteName = CUICommonDef.GetItemCellBorder(itemData.NameColor)
            local isBind = (cfgData.Bind == 1) or (itemData.Lock == 1)
            templateItem.transform:Find("BindSprite").gameObject:SetActive(false)
            templateItem.transform:Find("AmountLabel"):GetComponent(typeof(UILabel)).text = cfgData.Amount > 1 and cfgData.Amount or ""

            UIEventListener.Get(templateItem).onPress = LuaUtils.BoolDelegate(function (go, isPressed) 
                self.isButtonOnPress = isPressed
                if isPressed then
                    self.longPressItemId = cfgId
                    self.longpressItemObj = templateItem
                end
            end)
        end
    end
    self.Grid:Reposition()
end

function LuaShuangshiyi2023LotteryReplacementWnd:Update()
    if self.isButtonOnPressing then
        local timeinterval = CServerTimeMgr.Inst.timeStamp - self.buttonpressStartTime
        if timeinterval > self.longPressThreshold then
            self.isButtonOnPressing = false
            self.isButtonOnPress = false
            local itemId = Double11_RechargeLotteryItems.GetData(self.longPressItemId).ItemId
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
        else
            if not self.isButtonOnPress then
                self.isButtonOnPressing = false
                self.isButtonOnPress = false
                self:OnClickItem(self.longpressItemObj, self.longPressItemId)
            end
        end
    else
        if self.isButtonOnPress then
            self.isButtonOnPressing = true
            self.buttonpressStartTime = CServerTimeMgr.Inst.timeStamp
        end
    end
end


function LuaShuangshiyi2023LotteryReplacementWnd:RefreshVariableUI()
    --refresh replacement items
    local forbidItemList = self.forbidItemList
    local forbidItemLength = #forbidItemList
    for i = 1, forbidItemLength do
        local forbidItemObj = self["ForbiddenItem"..i]
        self:InitOneItem(forbidItemObj, forbidItemList[i])
        forbidItemObj.transform:Find("XSign").gameObject:SetActive(true)
    end

    for i = forbidItemLength + 1, 3 do
        local forbidItemObj = self["ForbiddenItem"..i]
        if self.curSelectItemList[i-forbidItemLength] then
            self:InitOneItem(forbidItemObj, self.curSelectItemList[i-forbidItemLength])
            forbidItemObj.transform:Find("XSign").gameObject:SetActive(true)
        else
            forbidItemObj.transform:Find("XSign").gameObject:SetActive(false)
            forbidItemObj.transform:Find("Border"):GetComponent(typeof(UISprite)).spriteName = ""
            forbidItemObj.transform:Find("BindSprite").gameObject:SetActive(false)
            forbidItemObj.transform:Find("AmountLabel"):GetComponent(typeof(UILabel)).text = ""
            local texture = forbidItemObj.transform:Find("Item"):GetComponent(typeof(UITexture))
            texture.material = nil
            UIEventListener.Get(forbidItemObj).onClick = DelegateFactory.VoidDelegate(function (_)
                --cancel
            end)
        end
    end
    
    --refresh replacement item number
    local remainReplacementTime = (self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurReplacementTimes] and self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurReplacementTimes] or 0)
    self.ReplacementOwnText.transform:Find("NumberText"):GetComponent(typeof(UILabel)).text = remainReplacementTime
    self.ReplacementConsumeText.transform:Find("NumberText"):GetComponent(typeof(UILabel)).text = #self.curSelectItemList
end 


function LuaShuangshiyi2023LotteryReplacementWnd:InitUIEvent()
    UIEventListener.Get(self.AddReplacementButton).onClick = DelegateFactory.VoidDelegate(function (_)
        --没道具, 弹出获得渠道
        local itemId = 21021450
        CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, false, self.AddReplacementButton.transform, CTooltipAlignType.Right)
    end)
    UIEventListener.Get(self.RuleButton).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("Double11_2023Lottery_Replacement_Rule")
    end)
    UIEventListener.Get(self.ReplacementButton).onClick = DelegateFactory.VoidDelegate(function (_)
        local forbidItemList = self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eReplacementItems] and self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eReplacementItems] or {}
        local forbidItemLength = #forbidItemList
        if forbidItemLength >= 3 then
            g_MessageMgr:ShowMessage("Double11_2023Lottery_Replacement_Full_Tips")
            return
        end
        --已选一个必中
        local mustGetItemId = self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eMustGetItem] and self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eMustGetItem] or 0
        if mustGetItemId ~= 0 then
            g_MessageMgr:ShowMessage("Double11_2023Lottery_MustGet_Full_Tips")
            return
        end
        
        local curSelectLength = #self.curSelectItemList
        if curSelectLength > 0 then
            CUIManager.CloseUI("Shuangshiyi2023LotteryReplacementWnd")
            Gac2Gas.RequestSet2023Double11ReplacementItems(g_MessagePack.pack(self.curSelectItemList))
        else
            g_MessageMgr:ShowMessage("Double11_2023Lottery_Replace_No_Select_Click_Ensure_Button")
        end
    end)
end

function LuaShuangshiyi2023LotteryReplacementWnd:InitOneItem(curItem, cfgId)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local cfgData = Double11_RechargeLotteryItems.GetData(cfgId)
    local itemID = cfgData.ItemId
    local ItemData = Item_Item.GetData(itemID)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end
    --overviewItemData
    curItem.transform:Find("Border"):GetComponent(typeof(UISprite)).spriteName = CUICommonDef.GetItemCellBorder(ItemData.NameColor)
    local isBind = (cfgData.Bind == 1) or (ItemData.Lock == 1)
    curItem.transform:Find("BindSprite").gameObject:SetActive(false)
    curItem.transform:Find("AmountLabel"):GetComponent(typeof(UILabel)).text = cfgData.Amount > 1 and cfgData.Amount or ""

    UIEventListener.Get(curItem).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
end
