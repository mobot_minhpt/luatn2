local UIGrid = import "UIGrid"

local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local CCPlayerCtrl = import "L10.Game.CCPlayerCtrl"
local AMPlayer = import "L10.Game.CutScene.AMPlayer"
local CSkillItemCell = import "L10.UI.CSkillItemCell"
local Animation = import "UnityEngine.Animation"

LuaProfessionTransferZhanKuangIntroductionWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "PositionButton", "PositionButton", GameObject)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "SpecialButton", "SpecialButton", GameObject)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "SkillButton", "SkillButton", GameObject)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "RightTabs", "RightTabs", GameObject)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "PositionContent", "PositionContent", GameObject)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "SpecialContent", "SpecialContent", GameObject)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "SkillContent", "SkillContent", GameObject)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "BackgroundDesc", "BackgroundDesc", UILabel)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "BackgroundScroll", "BackgroundScroll", GameObject)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "HowToPlayScroll", "HowToPlayScroll", GameObject)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "HowToPlayDesc", "HowToPlayDesc", UILabel)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "TryPlayButton", "TryPlayButton", GameObject)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "DoubleWeaponSetting", "DoubleWeaponSetting", GameObject)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "RageSetting", "RageSetting", GameObject)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "SkillTemplate", "SkillTemplate", GameObject)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "SkillGrid", "SkillGrid", GameObject)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "DoubleWeaponContent01", "DoubleWeaponContent01", UILabel)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "RageContent01", "RageContent01", UILabel)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "BackgroundGrid", "BackgroundGrid", UITable)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "HowToPlayGrid", "HowToPlayGrid", UIGrid)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "DoubleWeaponTitle", "DoubleWeaponTitle", UILabel)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "DoubleWeaponContent02", "DoubleWeaponContent02", UILabel)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "DoubleWeaponContent03", "DoubleWeaponContent03", UILabel)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "DoubleWeaponContent04", "DoubleWeaponContent04", UILabel)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "RageTitle", "RageTitle", UILabel)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "RageTitle01", "RageTitle01", UILabel)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "RageTitle02", "RageTitle02", UILabel)
RegistChildComponent(LuaProfessionTransferZhanKuangIntroductionWnd, "RageContent02", "RageContent02", UILabel)

--@endregion RegistChildComponent end

function LuaProfessionTransferZhanKuangIntroductionWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self:InitWndData()
    self:RefreshConstUI()
    self:InitUIEvent()
    
    self:OnClickLeftButton(1)
end

function LuaProfessionTransferZhanKuangIntroductionWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaProfessionTransferZhanKuangIntroductionWnd:InitWndData()
    self.leftButtonList = {self.PositionButton, self.SpecialButton, self.SkillButton}
    self.rightContentList = {self.PositionContent, self.SpecialContent, self.SkillContent}
    self.animationNameList = {"professiontransfer_tab01", "professiontransfer_tab02", "professiontransfer_tab03"}
    
    self.aniComp = self.transform:GetComponent(typeof(Animation))
end
    
function LuaProfessionTransferZhanKuangIntroductionWnd:RefreshConstUI()
    self.TryPlayButton:SetActive(false)
    
    self:RefreshPositionContent()
    self:RefreshSpecialContent()
    self:RefreshSkillContent()
end

function LuaProfessionTransferZhanKuangIntroductionWnd:InitUIEvent()
    for i = 1, #self.leftButtonList do
        UIEventListener.Get(self.leftButtonList[i]).onClick = DelegateFactory.VoidDelegate(function (_)
            self:OnClickLeftButton(i)
            self.aniComp:Stop()
            self.aniComp:Play(self.animationNameList[i])
        end)
    end

    UIEventListener.Get(self.TryPlayButton).onClick = DelegateFactory.VoidDelegate(function (_)
        local message = g_MessageMgr:FormatMessage("PROFESSIONTRANSFER_EXPERIENCE_RECONFIRM")
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
            print("reconfirm click tryplaybutton")
        end), nil, nil, nil, false)        
    end)
end

function LuaProfessionTransferZhanKuangIntroductionWnd:OnClickLeftButton(leftButtonIndex)
    for i = 1, #self.leftButtonList do
        if self.leftButtonList[i] then
            self.leftButtonList[i].transform:Find("IsSelected").gameObject:SetActive(leftButtonIndex == i)
        end
    end
    for i = 1, #self.rightContentList do
        if self.rightContentList[i] then
            self.rightContentList[i].gameObject:SetActive(leftButtonIndex == i)
        end
    end
end 

function LuaProfessionTransferZhanKuangIntroductionWnd:RefreshPositionContent()
    local templateList = {self.BackgroundDesc, self.HowToPlayDesc}
    local gridList = {self.BackgroundGrid, self.HowToPlayGrid}
    local originalStringList = {ProfessionTransfer_ZhanKuangDescription.GetData().BackgroundText, ProfessionTransfer_ZhanKuangDescription.GetData().TipsText}
    local scrollList = {self.BackgroundScroll, self.HowToPlayScroll}
    for i = 1, 2 do
        templateList[i].gameObject:SetActive(false)
        Extensions.RemoveAllChildren(gridList[i].transform)
        local strList = g_LuaUtil:StrSplit(originalStringList[i], "|")
        for strIdx = 1, #strList do
            local templateItem = CommonDefs.Object_Instantiate(templateList[i].gameObject)
            templateItem:SetActive(true)
            templateItem.transform.parent = gridList[i].transform
            templateItem:GetComponent(typeof(UILabel)).text = strList[strIdx]
        end
        gridList[i]:Reposition()
        scrollList[i].transform:GetComponent(typeof(CUIRestrictScrollView)):ResetPosition()
    end
end

function LuaProfessionTransferZhanKuangIntroductionWnd:RefreshSpecialContent()
    self.DoubleWeaponTitle.text = ProfessionTransfer_ZhanKuangDescription.GetData().IntroductionDoubleWeaponTitle
    self.DoubleWeaponContent01.text = ProfessionTransfer_ZhanKuangDescription.GetData().IntroductionDoubleWeaponContent1
    self.DoubleWeaponContent02.text = ProfessionTransfer_ZhanKuangDescription.GetData().IntroductionDoubleWeaponContent2
    self.DoubleWeaponContent03.text = ProfessionTransfer_ZhanKuangDescription.GetData().IntroductionDoubleWeaponContent3
    self.DoubleWeaponContent04.text = ProfessionTransfer_ZhanKuangDescription.GetData().IntroductionDoubleWeaponContent4
    self.RageTitle.text = ProfessionTransfer_ZhanKuangDescription.GetData().IntroductionRageTitle
    self.RageTitle01.text = ProfessionTransfer_ZhanKuangDescription.GetData().IntroductionRageTitle1
    self.RageContent01.text = ProfessionTransfer_ZhanKuangDescription.GetData().IntroductionRageContent1
    self.RageTitle02.text = ProfessionTransfer_ZhanKuangDescription.GetData().IntroductionRageTitle2
    self.RageContent02.text = ProfessionTransfer_ZhanKuangDescription.GetData().IntroductionRageContent2
end

function LuaProfessionTransferZhanKuangIntroductionWnd:RefreshSkillContent()
    self.SkillTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.SkillGrid.transform)
    
    local skillIdStr = ProfessionTransfer_ZhanKuangDescription.GetData().SkillIds
    local skillIds = g_LuaUtil:StrSplit(skillIdStr,",")
    local skillVideosStr = ProfessionTransfer_ZhanKuangDescription.GetData().SkillVideos
    local skillVideos = g_LuaUtil:StrSplit(skillVideosStr,",")
    local skillTypeList = {LocalString.GetString("70级绝技"), LocalString.GetString("100级绝技"),
                           LocalString.GetString("5级技能"), LocalString.GetString("50级技能"), LocalString.GetString("70级技能")
    }
    for i = 1, #skillIds do
        local skillId = skillIds[i]
        local skillVideo = skillVideos[i]
        
        local go = CommonDefs.Object_Instantiate(self.SkillTemplate)
        go:SetActive(true)
        go.transform.parent = self.SkillGrid.transform
        
        local skillCfg = Skill_AllSkills.GetData(skillId * 100 + 1)
        local showSpecial = i <= 2
        local skillBg = go.transform:Find("SkillBg")
        local skillName = go.transform:Find("SkillName"):GetComponent(typeof(UILabel))
        local skillitem = go.transform:Find("SkillItemTemplate"):GetComponent(typeof(CSkillItemCell))
        local skillType = go.transform:Find("SkillType"):GetComponent(typeof(UILabel))
        local skillIntroductionScroll = go.transform:Find("SkillBriefIntroductionScroll"):GetComponent(typeof(CUIRestrictScrollView))
        local skillDesc = go.transform:Find("SkillBriefIntroductionScroll/SkillBriefIntroductionDesc"):GetComponent(typeof(UILabel))
        local playSkillVideo = go.transform:Find("PlaySkillVideo")
        playSkillVideo.gameObject:SetActive(false)
        
        --skillBg.color = showSpecial and NGUIText.ParseColor24("808080", 0) or NGUIText.ParseColor24("FFFFFF", 0)
        skillBg.transform:Find("Ultimate").gameObject:SetActive(showSpecial)
        skillBg.transform:Find("Normal").gameObject:SetActive(not showSpecial)

        skillName.text = skillCfg.Name
        skillitem:Init(skillId, 0, 0, false, skillCfg and skillCfg.SkillIcon, false, nil)
        skillType.text = skillTypeList[i] and skillTypeList[i] or ""
        skillType.color = showSpecial and NGUIText.ParseColor24("AD23A6", 0) or NGUIText.ParseColor24("BA6000", 0)
        skillDesc.text = skillCfg.Display_jian
        skillIntroductionScroll:ResetPosition()
        UIEventListener.Get(playSkillVideo.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
            if not CCPlayerCtrl.IsSupportted() then
                g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("客户端引擎版本过旧，不能播放此视频，请更新版本后再试"))
                return
            end
            AMPlayer.Inst:PlayCG(skillVideo, nil, 1)
        end)
    end
    
    self.SkillGrid.transform:GetComponent(typeof(UIGrid)):Reposition()
end
