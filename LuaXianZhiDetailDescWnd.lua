require("common/common_include")

local CUITexture = import "L10.UI.CUITexture"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"

LuaXianZhiDetailDescWnd = class()

RegistChildComponent(LuaXianZhiDetailDescWnd, "XianZhiNameLabel", UILabel)
RegistChildComponent(LuaXianZhiDetailDescWnd, "XianZhiLocationLabel", UILabel)
RegistChildComponent(LuaXianZhiDetailDescWnd, "DescLabel", UILabel)
RegistChildComponent(LuaXianZhiDetailDescWnd, "DescPanel", CUIRestrictScrollView)
RegistChildComponent(LuaXianZhiDetailDescWnd, "XianZhiTypeIcon", CUITexture)

function LuaXianZhiDetailDescWnd:Init()
	local status = CLuaXianzhiMgr.GetXianZhiStatus()
	if status == EnumXianZhiStatus.NotFengXian then
		CUIManager.CloseUI(CLuaUIResources.XianZhiDetailDescWnd)
		return
	end

	local titleId = CLuaXianzhiMgr.GetValidTitleId()
	local xianZhiTitle = XianZhi_Title.GetData(titleId)

	self.XianZhiLocationLabel.text = CLuaXianzhiMgr.GetXianZhiLocation()
	self.XianZhiNameLabel.text = CLuaXianzhiMgr.GetXianZhiTitleName()

	if xianZhiTitle then
		self.XianZhiNameLabel.text = xianZhiTitle.TitleName
		self.DescLabel.text = g_MessageMgr:Format(xianZhiTitle.Des)

		local xianZhiTypeId = xianZhiTitle.TitleType
		local xianZhiType = XianZhi_TitleType.GetData(xianZhiTypeId)
		self.XianZhiTypeIcon:LoadSkillIcon(StringTrim(xianZhiType.Icon))
	else
		self.DescLabel.text = nil
		self.XianZhiNameLabel.text = nil
	end

	self.DescPanel:ResetPosition()
end

return LuaXianZhiDetailDescWnd
