require("common/common_include")

local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local Vector3 = import "UnityEngine.Vector3"
local CChristmasHongLvMgr = import "L10.Game.CChristmasHongLvMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Utility = import "L10.Engine.Utility"
local CPos = import "L10.Engine.CPos"
local EnumObjectType = import "L10.Game.EnumObjectType"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local DelegateFactory = import "DelegateFactory"
local CClientNpc = import "L10.Game.CClientNpc"

LuaChristmasMgr = class()

LuaChristmasMgr.ChristmasTreeBreedNpcId = 0
LuaChristmasMgr.ChristmasStormFx = nil
LuaChristmasMgr.m_MonsterLeftTimeTick = nil
LuaChristmasMgr.m_ObjId2EndTimeTbl = nil
LuaChristmasMgr.m_StormFxId = 88800470
function LuaChristmasMgr.ShowChristmasTreeBreedWnd(npcEngineId)
	LuaChristmasMgr.ChristmasTreeBreedNpcId = npcEngineId
	CUIManager.ShowUI("ChristmasTreeBreedWnd")
end

function LuaChristmasMgr.SyncChristmasTreeProgressChanged(level, idx, newProgress, newSubmitTimes, rewardStatus)
	g_ScriptEvent:BroadcastInLua("OnSyncChristmasTreeProgressChanged", level, idx, newProgress, newSubmitTimes, rewardStatus)
end

function LuaChristmasMgr.SyncChristmasTreeLevelUp(oriTreeLevel, newTreeLevel, newNpcEngineId, progressTbl, rewardStatus, submitTbl)
	LuaChristmasMgr.ChristmasTreeBreedNpcId = newNpcEngineId
	g_ScriptEvent:BroadcastInLua("OnSyncChristmasTreeLevelUp", oriTreeLevel, newTreeLevel, newNpcEngineId, progressTbl, rewardStatus, submitTbl)
end

function LuaChristmasMgr.SyncChristmasTreeInfo(treeLevel, progressTbl, rewardStatus, submitTbl)
	g_ScriptEvent:BroadcastInLua("OnSyncChristmasTreeInfo", treeLevel, progressTbl, rewardStatus, submitTbl)
end

function LuaChristmasMgr.RaiseChristmasTree(itemTempId, count)
	Gac2Gas.RaiseChristmasTree(itemTempId, count, LuaChristmasMgr.ChristmasTreeBreedNpcId)
end

function LuaChristmasMgr.GetGiftFromChristmasTree()
	Gac2Gas.GetGiftFromChristmasTree(LuaChristmasMgr.ChristmasTreeBreedNpcId)
end

function LuaChristmasMgr.SyncChristmasStromEffect(bShow, gridPosX, gridPosY)
	local mainplayer = CClientMainPlayer.Inst
	if not mainplayer then return end

	if LuaChristmasMgr.ChristmasStormFx then
		LuaChristmasMgr.ChristmasStormFx:Destroy()
		LuaChristmasMgr.ChristmasStormFx = nil
	end
	if bShow then
		local pos = CPos(gridPosX, gridPosY)
		LuaChristmasMgr.ChristmasStormFx = CEffectMgr.Inst:AddWorldPositionFX(LuaChristmasMgr.m_StormFxId, Utility.GridPos2PixelPos(pos), mainplayer.Inst.Dir, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
	end
end
function LuaChristmasMgr.OnMainPlayerCreated()
	if not CChristmasHongLvMgr.Inst:IsInChristmasVillage() then
		LuaChristmasMgr.CancelTick()
		LuaChristmasMgr.m_ObjId2EndTimeTbl = nil
	end
end

function LuaChristmasMgr.SyncChristmasSnowBallLeftTime(engineId, endTime)
	if not LuaChristmasMgr.m_ObjId2EndTimeTbl then
		LuaChristmasMgr.m_ObjId2EndTimeTbl = {}
	end
	LuaChristmasMgr.m_ObjId2EndTimeTbl[engineId] = endTime

	LuaChristmasMgr.StartTick()
end

function LuaChristmasMgr.BatchSyncChristmasSnowBallLeftTime(engineId2EndTimeTbl)
	if not LuaChristmasMgr.m_ObjId2EndTimeTbl then
		LuaChristmasMgr.m_ObjId2EndTimeTbl = {}
	end
	for engineId,endTime in pairs(engineId2EndTimeTbl) do
		LuaChristmasMgr.m_ObjId2EndTimeTbl[engineId] = endTime
	end

	LuaChristmasMgr.StartTick()
end

function LuaChristmasMgr.StartTick()
	if not LuaChristmasMgr.m_MonsterLeftTimeTick then
		LuaChristmasMgr.UpdateMonsterLeftTime()
		LuaChristmasMgr.m_MonsterLeftTimeTick = CTickMgr.Register(DelegateFactory.Action(function ()
			LuaChristmasMgr.UpdateMonsterLeftTime()
		end), 1 * 1000, ETickType.Loop)
	end
end

function LuaChristmasMgr.CancelTick()
	if LuaChristmasMgr.m_MonsterLeftTimeTick then
		invoke(LuaChristmasMgr.m_MonsterLeftTimeTick)
		LuaChristmasMgr.m_MonsterLeftTimeTick = nil
	end
end

function LuaChristmasMgr.UpdateMonsterLeftTime()
	if not LuaChristmasMgr.m_ObjId2EndTimeTbl then
		return
	end

	for engineId,endTime in pairs(LuaChristmasMgr.m_ObjId2EndTimeTbl) do
		local obj = CClientObjectMgr.Inst:GetObject(engineId)
    	if obj and obj.ObjectType == EnumObjectType.Monster then
    		local intervalSeconds = endTime - CServerTimeMgr.Inst.timeStamp
    		if intervalSeconds <0 or intervalSeconds>10 then
        		obj.CustomBottomName = nil
        	else
        		obj.CustomBottomName = SafeStringFormat3("[ff0000]%d[-]", math.floor(intervalSeconds))
        	end
    		EventManager.BroadcastInternalForLua(EnumEventType.ClientObjChangeName, {engineId})
    	end
    end
end

-- xueRenNpcEngineId 雪人Npc
-- curStage为0时 也就是未开始时 ShengDan_SnowBall无数据 用雪人默认大小和名字
-- showFx是否播放模型缩放比例变化的特效
function LuaChristmasMgr.SyncShengDanXueRenXueQiuNum(xueRenNpcEngineId, curStage, showFx)
	if curStage ~= 0 then
		local templateId = ShengDan_CreateSnowman.GetData().XueRenTemplateId
		local basicScale = NPC_NPC.GetData(templateId).Scale
		local obj = CClientObjectMgr.Inst:GetObject(xueRenNpcEngineId)
		local snowmanData = ShengDan_SnowBall.GetData(curStage)
		if basicScale and obj and snowmanData ~= nil then
			local trueScale = snowmanData.NpcScale * basicScale
			--名称
			local npc = TypeAs(obj, typeof(CClientNpc))
			npc:SyncNpcNameAndTitle(xueRenNpcEngineId, templateId, snowmanData.NpcName, nil)

			--升级特效
			if showFx == true then
				local fxId = ShengDan_CreateSnowman.GetData().SnowmanUpgradeFxId
				if fxId and fxId > 0 then
					obj.DelayExecuteQueue:AppendCommand(DelegateFactory.Action_uint(function (id)
						obj.RO.Scale = trueScale
						local fx = CEffectMgr.Inst:AddObjectFX(fxId, obj.RO, 0, trueScale, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
						obj.RO:AddFX(fxId, fx, -1)
					end))
				end
			else
				obj.RO.Scale = trueScale
			end

			--超级大雪人特效
			if curStage == ShengDan_SnowBall.GetDataCount() then 
				local fxId = ShengDan_CreateSnowman.GetData().SupperSnowmanFxId
				local fx = CEffectMgr.Inst:AddObjectFX(fxId, obj.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
				obj.RO:AddFX(fxId, fx, -1)
			end
		end
	end

end

function LuaChristmasMgr.SyncShengDanCatchXueQiuNum(xueQiuNum)
	g_ScriptEvent:BroadcastInLua("SyncShengDanCatchXueQiuNum", xueQiuNum)
end

---------------------------
---------Gas2Gac-----------
---------------------------
-- 圣诞树进度改变
-- 参数分别代表 
-- 	level: 树等级
-- 	idx: 本次变化的索引
-- 	newProgress: 新的进度
-- 	newSubmitTimes: 新的提交次数
-- 	rewardStatus: 领取奖励的状态, 0表示无奖,1表示可以领(仍然需要到时间才能领), 2表示今天领取过了
function Gas2Gac.SyncChristmasTreeProgressChanged(level, idx, newProgress, newSubmitTimes, rewardStatus)
	
	LuaChristmasMgr.SyncChristmasTreeProgressChanged(level, idx, newProgress, newSubmitTimes, rewardStatus)
end

-- 圣诞树等级增加
-- 参数分别代表
-- 	oriTreeLevel: 原先的树的等级
--	newTreeLevel: 新的等级
-- 	newNpcEngineId: 新的树的引擎id
--	extraInfoUd解开后是一个list
	-- list[1] - list[3] -> 进度
	-- list[4] - list[6] -> 当日提交次数
	-- list[7] -> 领取奖励的状态, 0表示无奖,1表示可以领(仍然需要到时间才能领), 2表示今天领取过了
function Gas2Gac.SyncChristmasTreeLevelUp(oriTreeLevel, newTreeLevel, newNpcEngineId, extraInfoUd)

	local list = MsgPackImpl.unpack(extraInfoUd)
	if not list or list.Count~=7 then return end
	local progressTbl = {}
	table.insert(progressTbl, 1, tonumber(list[0]))
	table.insert(progressTbl, 2, tonumber(list[1]))
	table.insert(progressTbl, 3, tonumber(list[2]))
	local submitTbl = {}
	table.insert(submitTbl, 1, tonumber(list[3]))
	table.insert(submitTbl, 2, tonumber(list[4]))
	table.insert(submitTbl, 3, tonumber(list[5]))
	local rewardStatus = tonumber(list[6])

	LuaChristmasMgr.SyncChristmasTreeLevelUp(oriTreeLevel, newTreeLevel, newNpcEngineId, progressTbl, rewardStatus, submitTbl)
end

-- 查询圣诞树信息的返回, infoUd 解开后是一个list
	-- list[1] -> 圣诞树等级
	-- list[2] - list[4] -> 进度
	-- list[5] -> 领奖状态, 0 表示无奖,1 表示可以领(仍然需要到时间才能领), 2表示今天领取过了
	-- list[6] - list[8] -> 当日提交次数
function Gas2Gac.SyncChristmasTreeInfo(infoUd)
	local list = MsgPackImpl.unpack(infoUd)
	if not list or list.Count~=8 then return end
	local treeLevel = tonumber(list[0]) --下标从0开始
	local progressTbl = {}
	table.insert(progressTbl, 1, tonumber(list[1]))
	table.insert(progressTbl, 2, tonumber(list[2]))
	table.insert(progressTbl, 3, tonumber(list[3]))
	local rewardStatus = tonumber(list[4])
	local submitTbl = {}
	table.insert(submitTbl, 1, tonumber(list[5]))
	table.insert(submitTbl, 2, tonumber(list[6]))
	table.insert(submitTbl, 3, tonumber(list[7]))

	LuaChristmasMgr.SyncChristmasTreeInfo(treeLevel, progressTbl, rewardStatus, submitTbl)
end

-- 树掉落礼物(表现上,树需要有个摇动的过程)
function Gas2Gac.SyncChristmasTreeDropItem(npcEngineId)

	local obj = CClientObjectMgr.Inst:GetObject(npcEngineId)
    if obj then
        obj:ShowExpressionActionState(47000281)
    end

end

-- 同步场景中的格子信息
-- force 表示颜色
-- gridPosUD 解开后是个list,list中是格子坐标对,不是grid坐标，而是(1,1)这些表示格子的信息,场景中有64个格子,这里的范围是 (1,1) - (8,8)

function Gas2Gac.BatchSyncGridForceInHongLvZuoZhanPlay(force, gridPosUD)

	local list = MsgPackImpl.unpack(gridPosUD)
	if not list then return end
	for i=0,list.Count-1,2 do
		local x = list[i]
		local y = list[i+1]
		CChristmasHongLvMgr.Inst:SyncGridForceInHongLvZuoZhan(x, y, force)
	end

end

-- 显示暴风雪特效, bShow:true显示,false不显示
-- 后面两个是 Grid 坐标
function Gas2Gac.SyncChristmasStromEffect(bShow, posX, posY)
	LuaChristmasMgr.SyncChristmasStromEffect(bShow, posX, posY)
end

-- 格子闪烁
function Gas2Gac.BatchSyncGridTwinkleInHongLvZuoZhanPlay(force, gridPosUD)

	local list = MsgPackImpl.unpack(gridPosUD)
	if not list then return end
	for i=0,list.Count-1,2 do
		local x = list[i]
		local y = list[i+1]
		CChristmasHongLvMgr.Inst:SyncGridForceInHongLvZuoZhan(x, y, force)
	end

end

-- 副本中小怪倒计时结束时间
function Gas2Gac.SyncChristmasSnowBallLeftTime(engineId, endTime)
	LuaChristmasMgr.SyncChristmasSnowBallLeftTime(engineId, endTime)
end

-- 副本中小怪倒计时结束时间批量同步,玩家掉线进入副本时同步 {engineId1, endTime1, ..}
function Gas2Gac.BatchSyncChristmasSnowBallLeftTime(endTimeUd)
	local list = MsgPackImpl.unpack(endTimeUd)
	if not list then return end
	local tbl = {}
	for i=0,list.Count-1,2 do
		local engineId = list[i]
		local endTime = list[i+1]
		tbl[engineId] = endTime
		LuaChristmasMgr.BatchSyncChristmasSnowBallLeftTime(tbl)
	end
end

-- 圣诞老人BOSS在帮会被击杀,需要播放特效
function Gas2Gac.SyncChristmasSantaKilledInGuild()
end

-- 圣诞树升级
function Gas2Gac.ShowSendShengTreeLevelUpFx(oldEngineId, oriTreeLevel, newNpcEngineId, newLevel)
	--播放升级特效
	local obj = CClientObjectMgr.Inst:GetObject(newNpcEngineId)
	local fxId = 0
	if ShengDan_RaiseTree.Exists(newLevel) then
		fxId = ShengDan_RaiseTree.GetData(newLevel).ChristmasTreeLevelUpFx
	end
    if obj and obj.ObjectType == EnumObjectType.NPC and fxId and fxId > 0 then
    	obj.DelayExecuteQueue:AppendCommand(DelegateFactory.Action_uint(function (id)

    		local fx = CEffectMgr.Inst:AddObjectFX(fxId, obj.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
        	obj.RO:AddFX(fxId, fx, -1)
    	end))
    end
end

-- 圣诞老人送礼物,需要播放子弹特效
function Gas2Gac.ShowSendShengDanGiftFx(srcEngineId, dstEngineIdUd)

	local list = MsgPackImpl.unpack(dstEngineIdUd)
	if not list then return end
	for i=0,list.Count-1 do
		
		local dstEngineId = list[i]

		local srcObj = CClientObjectMgr.Inst:GetObject(srcEngineId)
		local dstObj = CClientObjectMgr.Inst:GetObject(dstEngineId)

		-- 两个特效的ID，要读表，这里先写死
		local throwFxId = 88800848
		local boomFxId = 88800849

		if srcObj and dstObj then
			local fx = CEffectMgr.Inst:AddBulletFX(
				throwFxId,
				srcObj.RO,
				dstObj.RO,
				0,1.0,-1,nil)
			srcObj.RO:UnRefFX(fx)
		end

		--要获取prefab里的数据，这里先写死
		local srcSlot = "Center"
		local dstSlot = "Center"
		local bSpeed = 13.0
		local bPlaySpeed = 1.0
		local target = dstObj.RO:GetSlotPosition(dstSlot)
		local pos = srcObj.RO:GetSlotPosition(srcSlot)

		--扔礼物的时间周期
		local leftTime = Vector3.Distance(target,pos) / (bSpeed * bPlaySpeed / 1000.0)

		if dstObj then
			local fx = CEffectMgr.Inst:AddObjectFX(
				boomFxId,
				dstObj.RO,
				math.ceil(leftTime),1.0,1.0,nil,false,EnumWarnFXType.None,Vector3(0,0,0),Vector3(0,0,0), nil
			)
			dstObj.RO:AddFX(boomFxId, fx, -1)
		end
	end
end