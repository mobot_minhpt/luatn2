-- Auto Generated!!
local Boolean = import "System.Boolean"
local CButton = import "L10.UI.CButton"
local CCommonListButtonItem = import "CCommonListButtonItem"
local CFriendGroupModifyListItem = import "L10.UI.CFriendGroupModifyListItem"
local CFriendGroupModifyWnd = import "L10.UI.CFriendGroupModifyWnd"
local CIMMgr = import "L10.Game.CIMMgr"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CResourceMgr = import "L10.Engine.CResourceMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local GameSetting_Common = import "L10.Game.GameSetting_Common"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Object = import "System.Object"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt64 = import "System.UInt64"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CFriendGroupModifyWnd.m_Init_CS2LuaHook = function (this) 
    this.friendItemAdvView.m_DataSource = this
    --      refreshFriendItems();
    Gac2Gas.QueryFriendlinessTbl()
    this:showGroupItems()
    this:onGroupShowClick(1)
end
CFriendGroupModifyWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.closeWnd, VoidDelegate, this), true)
    UIEventListener.Get(this.deleteFriendButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.deleteFriendButton).onClick, MakeDelegateFromCSFunction(this.onDeleteButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.modifyGroupButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.modifyGroupButton).onClick, MakeDelegateFromCSFunction(this.onModifyButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.modifyNameButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.modifyNameButton).onClick, MakeDelegateFromCSFunction(this.onModifyNameClick, VoidDelegate, this), true)
    UIEventListener.Get(this.groupSettingButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.groupSettingButton).onClick, MakeDelegateFromCSFunction(this.onGroupSettingButtonClick, VoidDelegate, this), true)
    EventManager.AddListener(EnumEventType.MainplayerRelationshipPropUpdate, MakeDelegateFromCSFunction(this.onFriendGroupChange, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_SetFriendGroupName, MakeDelegateFromCSFunction(this.OnSetFriendGroupName, MakeGenericClass(Action2, Int32, String), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_AdjustFriendGroup, MakeDelegateFromCSFunction(this.onFriendGroupAdjust, MakeGenericClass(Action2, UInt64, Int32), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_AdjustFriendGroupWithMultiPlayer, MakeDelegateFromCSFunction(this.onFriendGourpMultiAdjust, MakeGenericClass(Action2, MakeGenericClass(List, Object), Int32), this))
    EventManager.AddListener(EnumEventType.OnAllFriendinessUpdate, MakeDelegateFromCSFunction(this.onFriendinessChange, Action0, this))
    EventManager.AddListener(EnumEventType.OnDeleteMultiFriendSuccess, MakeDelegateFromCSFunction(this.refreshFriendItems, Action0, this))
end
CFriendGroupModifyWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.closeWnd, VoidDelegate, this), false)
    UIEventListener.Get(this.modifyGroupButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.modifyGroupButton).onClick, MakeDelegateFromCSFunction(this.onModifyButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.modifyNameButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.modifyNameButton).onClick, MakeDelegateFromCSFunction(this.onModifyNameClick, VoidDelegate, this), false)
    UIEventListener.Get(this.groupSettingButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.groupSettingButton).onClick, MakeDelegateFromCSFunction(this.onGroupSettingButtonClick, VoidDelegate, this), false)
    EventManager.RemoveListener(EnumEventType.MainplayerRelationshipPropUpdate, MakeDelegateFromCSFunction(this.onFriendGroupChange, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_AdjustFriendGroup, MakeDelegateFromCSFunction(this.onFriendGroupAdjust, MakeGenericClass(Action2, UInt64, Int32), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_SetFriendGroupName, MakeDelegateFromCSFunction(this.OnSetFriendGroupName, MakeGenericClass(Action2, Int32, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_AdjustFriendGroupWithMultiPlayer, MakeDelegateFromCSFunction(this.onFriendGourpMultiAdjust, MakeGenericClass(Action2, MakeGenericClass(List, Object), Int32), this))
    EventManager.RemoveListener(EnumEventType.OnAllFriendinessUpdate, MakeDelegateFromCSFunction(this.onFriendinessChange, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnDeleteMultiFriendSuccess, MakeDelegateFromCSFunction(this.refreshFriendItems, Action0, this))
end
CFriendGroupModifyWnd.m_refreshFriendItemsData_CS2LuaHook = function (this) 
    this.currentFriendList = CIMMgr.Inst:GetFriendsByGroup(this.groupIndex)
    this.countLabel.text = System.String.Format(LocalString.GetString("共{0}人"), this.currentFriendList.Count)
    CommonDefs.ListClear(this.checkList)
end
CFriendGroupModifyWnd.m_clearFriendItems_CS2LuaHook = function (this) 
    local list = this.friendItemGrid:GetChildList()
    do
        local i = 0
        while i < list.Count do
            CResourceMgr.Inst:Destroy(list[i].gameObject)
            i = i + 1
        end
    end
    CommonDefs.ListClear(this.currentFriendList)
end
CFriendGroupModifyWnd.m_onItemCkeck_CS2LuaHook = function (this, ID, check) 
    this:hideModifyChoices()
    if check then
        if this.checkList:FindIndex(DelegateFactory.Predicate_ulong(function (x) 
            return x == ID
        end)) < 0 then
            CommonDefs.ListAdd(this.checkList, typeof(UInt64), ID)
        end
    else
        if this.checkList:FindIndex(DelegateFactory.Predicate_ulong(function (x) 
            return x == ID
        end)) >= 0 then
            CommonDefs.ListRemove(this.checkList, typeof(UInt64), ID)
        end
    end
end
CFriendGroupModifyWnd.m_showGroup_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.groupItems.Count do
            if i + 1 == this.groupIndex then
                this.groupItems[i]:SetHighLight(true)
            else
                this.groupItems[i]:SetHighLight(false)
            end
            i = i + 1
        end
    end
    this:refreshFriendItems()
    this:hideModifyChoices()
end
CFriendGroupModifyWnd.m_showGroupItems_CS2LuaHook = function (this) 
    --      groupItems.Clear();
    for i = 0, 3 do
        local go = CommonDefs.Object_Instantiate(this.groupItemPrefab)
        this.groupItemGrid:AddChild(go.transform)
        go.transform.localScale = Vector3.one
        go:SetActive(true)
        local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CCommonListButtonItem))
        item.Index = i + 1
        item.NameLabel.text = CIMMgr.Inst:GetFriendGroupName(i + 1)
        item.clickAction = MakeDelegateFromCSFunction(this.onGroupShowClick, MakeGenericClass(Action1, Int32), this)
        CommonDefs.ListAdd(this.groupItems, typeof(CCommonListButtonItem), item)
    end
    this.groupItemPrefab:SetActive(false)
    this.groupItemGrid.repositionNow = true
end
CFriendGroupModifyWnd.m_refreshGroupNames_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.groupItems.Count do
            local item = this.groupItems[i]
            item.NameLabel.text = CIMMgr.Inst:GetFriendGroupName(i + 1)
            i = i + 1
        end
    end
end
CFriendGroupModifyWnd.m_showModifyChoices_CS2LuaHook = function (this) 
    this.choiceItemScrollView.transform.parent.parent.gameObject:SetActive(true)
    if this.groupChoiceItems.Count == 0 then
        for i = 0, 3 do
            local go = CommonDefs.Object_Instantiate(this.groupItemPrefab)
            this.choiceItemGrid:AddChild(go.transform)
            go.transform.localScale = Vector3.one
            go.transform.localPosition = Vector3.zero
            local cb = CommonDefs.AddComponent_GameObject_Type(go, typeof(CButton))
            cb.enableAnimation = true
            local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CCommonListButtonItem))
            item.clickAction = MakeDelegateFromCSFunction(this.onGroupModifyClick, MakeGenericClass(Action1, Int32), this)
            item.Index = i + 1
            CommonDefs.ListAdd(this.groupChoiceItems, typeof(CCommonListButtonItem), item)
            go:SetActive(true)
        end
        this.choiceItemGrid.repositionNow = true
    end
    for i = 0, 3 do
        local item = this.groupChoiceItems[i]
        item.NameLabel.text = CIMMgr.Inst:GetFriendGroupName(i + 1)
    end
end
CFriendGroupModifyWnd.m_showModifyNamePanel_CS2LuaHook = function (this) 
    CInputBoxMgr.ShowInputBox(LocalString.GetString("请输入新的分组名称"), DelegateFactory.Action_string(function (str) 
        --int uplimit = CLingShouMgr.Inst.maxNameLength;// int.Parse(limit);
        --string str = Extensions.RemoveBlank(input.value);//移除空格
        if System.String.IsNullOrEmpty(str) then
            g_MessageMgr:ShowMessage("FENZU_NAME_NOT_NULL")
            return
        elseif CUICommonDef.GetStrByteLength(str) > 10 or CUICommonDef.GetStrByteLength(str) < 4 then
            g_MessageMgr:ShowMessage("FENZU_INPUT_NAME_LENGTH")
            return
        elseif not CWordFilterMgr.Inst:CheckName(str) then
            g_MessageMgr:ShowMessage("FENZU_VIOLATION")
            return
        end
        CIMMgr.Inst:SetFriendGroupName(this.groupIndex, str)
        CUIManager.CloseUI(CIndirectUIResources.InputBox)
    end), 10, true, nil, LocalString.GetString("点此输入，最多10个字符(5个汉字)"))
    --名字输入时不做限制
end
CFriendGroupModifyWnd.m_onDeleteButtonClick_CS2LuaHook = function (this, go) 
    if this.checkList.Count > 100 then
        g_MessageMgr:ShowMessage("MAX_DELETEFRIEND_COUNT")
        return
    end
    if this.checkList.Count > 0 then
        this:doFriendinessCheck()
    end
end
CFriendGroupModifyWnd.m_doFriendinessCheck_CS2LuaHook = function (this) 
    local setting = GameSetting_Common.GetData()
    local count = 0
    do
        local i = 0
        while i < this.checkList.Count do
            local id = this.checkList[i]
            if CIMMgr.Inst:GetFriendliness(id) >= setting.Friendly_Degree then
                count = count + 1
            end
            i = i + 1
        end
    end
    if count == 0 then
        local str = g_MessageMgr:FormatMessage("CONFIRM_DELETE_FRIEND")
        MessageWndManager.ShowOKCancelMessage(str, DelegateFactory.Action(function () 
            CIMMgr.Inst:DelMultiFriend(this.checkList)
        end), nil, nil, nil, false)
    else
        local str = g_MessageMgr:FormatMessage("COMFIRM_DELETE_CLOSEFRIEND", count, setting.Friendly_Degree)
        MessageWndManager.ShowDelayOKCancelMessage(str, DelegateFactory.Action(function () 
            CIMMgr.Inst:DelMultiFriend(this.checkList)
        end), nil, 3)
    end
end
CFriendGroupModifyWnd.m_onGroupShowClick_CS2LuaHook = function (this, index) 
    if this.groupIndex == index then
        return
    end
    this.groupIndex = index
    this:showGroup()
end
CFriendGroupModifyWnd.m_onGroupModifyClick_CS2LuaHook = function (this, index) 
    CIMMgr.Inst:AdjustFriendGroupWithMultiPlayer(this.checkList, index)

    this:hideModifyChoices()
end
CFriendGroupModifyWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local cmp = TypeAs(view:GetFromPool(0), typeof(CFriendGroupModifyListItem))
    cmp.checkCallback = MakeDelegateFromCSFunction(this.onItemCkeck, MakeGenericClass(Action2, UInt64, Boolean), this)
    if this.currentFriendList.Count > 0 then
        cmp.ID = this.currentFriendList[row]
        local info = CIMMgr.Inst:GetBasicInfo(cmp.ID)
        local groupName = CIMMgr.Inst:GetFriendGroupName(this.groupIndex)
        local offlineTime = - 1
        if CIMMgr.Inst:IsOnline(cmp.ID) then
            offlineTime = 0
        else
            offlineTime = CIMMgr.Inst:GetFriendOfflineTime(cmp.ID)
            if offlineTime == 0 then
                offlineTime = - 1
                groupName = " "
            end
        end
        cmp:SetFriendInfo(info.Name, groupName, info.Level, CIMMgr.Inst:GetFriendliness(cmp.ID), info.Class, info.Gender, info.Expression, info.XianFanStatus > 0, offlineTime)
        cmp.Checked = CommonDefs.ListContains(this.checkList, typeof(UInt64), cmp.ID)
    end
    return cmp
end

--#endregion
