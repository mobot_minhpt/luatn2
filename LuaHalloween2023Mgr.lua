local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CEffectMgr = import "L10.Game.CEffectMgr"
local Vector3 = import "UnityEngine.Vector3"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CDirectorEffect = import "L10.Game.CDirectorEffect"
local CClientObjectRoot = import "L10.Game.CClientObjectRoot"
local CRenderObject = import "L10.Engine.CRenderObject"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CStatusMgr = import "L10.Game.CStatusMgr"
local EPropStatus = import "L10.Game.EPropStatus"

LuaHalloween2023Mgr = {}

function LuaHalloween2023Mgr:IsGameOpen(timeStr)
    local startHour, startMin, endHour, endMin = string.match(timeStr,"(%d+):(%d+)-(%d+):(%d+)")
    local startTimeStamp = CServerTimeMgr.Inst:GetTodayTimeStampByTime(startHour,startMin,0)
    local endTimeStamp = CServerTimeMgr.Inst:GetTodayTimeStampByTime(endHour,endMin,0)
    local nowTime = CServerTimeMgr.Inst.timeStamp
    return nowTime >= startTimeStamp and nowTime <= endTimeStamp
end

------------------------
-- 抢单大战
------------------------
EnumCandyDeliveryActionType = {  --和服务器端定义保持一致
	eRushHit = 1,
	eBeRushHit = 2,
    eDie = 3,
    eRelive = 4,
    eDying = 5,
}

LuaHalloween2023Mgr.m_MyTeamRoleInfo = nil
function LuaHalloween2023Mgr:SetMyTeamRoleInfo(driverPlayerId, passengerPlayerId)
    self.m_MyTeamRoleInfo = {}
    self.m_MyTeamRoleInfo.driverPlayerId = driverPlayerId
    self.m_MyTeamRoleInfo.passengerPlayerId = passengerPlayerId
end

function LuaHalloween2023Mgr:SyncMyTeamRoleInfo(driverPlayerId, passengerPlayerId)
    self:SetMyTeamRoleInfo(driverPlayerId, passengerPlayerId)
    g_ScriptEvent:BroadcastInLua("SyncMyTeamRoleInfo")
end

LuaHalloween2023Mgr.m_OrderBattle_GamePlayId = nil
LuaHalloween2023Mgr.m_CandyDeliveryGameSetData = nil      -- 游戏的活动设置信息，存一份避免重复读表
function LuaHalloween2023Mgr:GetOrderBattleGamePlayId()
    if self.m_OrderBattle_GamePlayId == nil then
        self.m_OrderBattle_GamePlayId = Halloween2023_Setting.GetData().CandyDeliveryPlayId
    end
    return self.m_OrderBattle_GamePlayId
end


LuaHalloween2023Mgr.m_CandyDeliveryGaming = nil
function LuaHalloween2023Mgr:OnMainPlayerCreated(gamePlayId)
    if gamePlayId == self:GetOrderBattleGamePlayId() then
        self.m_CandyDeliveryGaming = true
        LuaCommonGamePlayTaskViewMgr:AppendGameplayInfo(gamePlayId, 
        true, nil,
        false,
        true, function() return Gameplay_Gameplay.GetData(gamePlayId).Name end,
        true, function() return g_MessageMgr:FormatMessage("Halloween2023_CandyDeliveryPlay_TaskView_Desc") end,
        true, nil, nil,
        true, nil, function() LuaHalloween2023Mgr:OpenHalloween2023OrderBattleRuleWnd("TaskViewRuleBtn") end)
        if self.m_CandyDeliveryGameSetData == nil then self:InitCandyDeliveryGameSetData() end
        
        if CUIManager.IsLoaded(CLuaUIResources.Halloween2023OrderBattleResultWnd) then
            CUIManager.CloseUI(CLuaUIResources.Halloween2023OrderBattleResultWnd)
        end
        g_ScriptEvent:AddListener("OnAddBuffFX", self, "OnAddBuffFX")
        g_ScriptEvent:AddListener("OnRemoveBuffFX", self, "OnRemoveBuffFX")
    end
end

function LuaHalloween2023Mgr:OnMainPlayerDestroyed()
    self.CandyDeliveryCampInfo = nil
    self.m_MyTeamRoleInfo = {}
    if self.m_CandyDirectorEffect then
        self.m_CandyDirectorEffect.directorEffect.m_TargetRO:Destroy()
        self.m_CandyDirectorEffect = nil
    end
    g_ScriptEvent:RemoveListener("OnAddBuffFX", self, "OnAddBuffFX")
    g_ScriptEvent:RemoveListener("OnRemoveBuffFX", self, "OnRemoveBuffFX")
end

function LuaHalloween2023Mgr:IsCandyDeliveryGaming()
    return self.m_CandyDeliveryGaming
end

function LuaHalloween2023Mgr:InitCandyDeliveryGameSetData()
    self.m_CandyDeliveryGameSetData = {}
    local setData = Halloween2023_Setting.GetData()
    local skillId = setData.CandyDeliveryPassengerSkills[2]
    local skillData = Skill_AllSkills.GetData(skillId)
    self.m_CandyDeliveryGameSetData.SlowDownSkillId = skillId
    self.m_CandyDeliveryGameSetData.SlowDownSkillRange = skillData.Range

    self.m_CandyDeliveryGameSetData.MaxCarHp = setData.CandyDeliveryMaxCarHp

    local deliveryNpcInfos = setData.CandyDeliveryNpcInfos
    local candyDeliveryNpcInfos = {}
    for i = 0, deliveryNpcInfos.Length - 1 do
        local info = deliveryNpcInfos[i]
        candyDeliveryNpcInfos[info[0]] = Vector3(info[1], 0, info[2])
    end
    self.m_CandyDeliveryGameSetData.candyDeliveryNpcInfos = candyDeliveryNpcInfos

    local deliveryId2Score = setData.CandyDeliveryId2Score
    local candyBuffInfos = {}
    for i = 0, deliveryId2Score.Length - 1 do
        local info = deliveryId2Score[i]
        local buffId = info[1]
        local npcId = info[2]
        local ortherInfo = Halloween2023_CandyBuff.GetData(buffId)   -- buffId -> score、color
        candyBuffInfos[buffId] = {Score = ortherInfo.Score, Color = ortherInfo.Color, NPCId = npcId}
    end
    self.m_CandyDeliveryGameSetData.candyBuffInfos = candyBuffInfos

    local candyForceSet = {}    -- zuoQiId -> candyForceData
    Halloween2023_CandyForce.Foreach(function (id, data)
        candyForceSet[id] = {Name = data.Name, ZuiQiTexture = data.ZuiQiTexture, Color = data.Color}
    end)
    self.m_CandyDeliveryGameSetData.candyForceSet = candyForceSet
end

LuaHalloween2023Mgr.CandyDeliveryCampInfo = nil
LuaHalloween2023Mgr.CandyDeliveryCampRank = nil
function LuaHalloween2023Mgr:SyncCandyDeliveryCampInfo(isAll, campInfo)
    self.m_CandyDeliveryGaming = true
    local myPlayerId =  CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local campInfo = g_MessagePack.unpack(campInfo)
    if self.CandyDeliveryCampInfo == nil then self.CandyDeliveryCampInfo = {} end
    if self.m_CandyDeliveryGameSetData == nil then self:InitCandyDeliveryGameSetData() end
    for i = 1, #campInfo, 9 do
        local data = {}
        data.campId = campInfo[i]
        data.zuoQiId = campInfo[i + 1]
        data.driverPlayerId = campInfo[i + 2]
        data.driverEngineId = campInfo[i + 3]
        data.passengerPlayerId = campInfo[i + 4]
        data.passengerEngineId = campInfo[i + 5]
        data.score = campInfo[i + 6]
        data.campHp = campInfo[i + 7]
        data.candyBuffId = campInfo[i + 8]
        data.myTeam = data.driverPlayerId == myPlayerId or data.passengerPlayerId == myPlayerId
        data.update = true
        self.CandyDeliveryCampInfo[data.campId] = data
        if data.myTeam then
            self.m_MyTeamRoleInfo = {}
            self.m_MyTeamRoleInfo.driverPlayerId = data.driverPlayerId
            self.m_MyTeamRoleInfo.driverEngineId = data.driverEngineId
            self.m_MyTeamRoleInfo.passengerPlayerId = data.passengerPlayerId
            self.m_MyTeamRoleInfo.passengerEngineId = data.passengerEngineId
            self.m_MyTeamRoleInfo.zuoQiId = data.zuoQiId
        end
    end
    -- 排序
    self.CandyDeliveryCampRank = {}
    for _, data in pairs(self.CandyDeliveryCampInfo) do
        table.insert(self.CandyDeliveryCampRank, data)
    end
    table.sort(self.CandyDeliveryCampRank, function (a, b)
        return a.score > b.score
    end)
    if CUIManager.IsLoaded(CLuaUIResources.Halloween2023OrderBattleTopRightWnd) then
        g_ScriptEvent:BroadcastInLua("SyncCandyDeliveryCampInfo")
    else
        CUIManager.ShowUI(CLuaUIResources.Halloween2023OrderBattleTopRightWnd)
	end
    self:UpdateHeadInfo()
end

-- 玩家头顶信息更新
function LuaHalloween2023Mgr:UpdateHeadInfo()
    for _, data in pairs(LuaHalloween2023Mgr.CandyDeliveryCampInfo) do
        if data.update then
            self:OnTeamHpUpdate(data.driverPlayerId, data.passengerPlayerId, data.campHp)
            self:OnTeamBuffUpdate(data.driverEngineId, 0)
            self:OnTeamBuffUpdate(data.passengerEngineId, data.candyBuffId)
            if CClientMainPlayer.Inst and (data.driverEngineId == CClientMainPlayer.Inst.EngineId or data.passengerEngineId == CClientMainPlayer.Inst.EngineId) then
                self:UpdateDirectorEffect(data.candyBuffId)
            end
            -- 重新检查一下减速特效(断线重连)
            local obj = CClientObjectMgr.Inst:GetObject(data.driverEngineId)
            if obj and CommonDefs.DictContains_LuaCall(obj.BuffProp.Buffs, 66042301) then
                self:SyncCandyDeliveryVehicleSlowFx(data.driverEngineId, true)
            end
            data.update = false
        end
    end
end

-- 播放坐骑动画
LuaHalloween2023Mgr.m_CandyDeliveryStatusType = {}
function LuaHalloween2023Mgr:SyncCandyDeliveryActionType(engineId, actionType)
    local driverRo = CClientObjectMgr.Inst:GetObject(engineId)
    local vehicleRO = driverRo and driverRo.VehicleRO or nil
    self.m_CandyDeliveryStatusType[engineId] = actionType
    if vehicleRO then
        if actionType == EnumCandyDeliveryActionType.eDying then
            self:SyncCandyDeliveryVehicleSlowFx(engineId, false)    -- 死亡关闭减速特效
            vehicleRO:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(renderObject)
                local expression_define = Expression_Define.GetData(47001283)
                if expression_define and renderObject then
                    vehicleRO:DoAni(expression_define.AniName, expression_define.Loop == 1, expression_define.StartTime,1.0,0.15, true, 1.0)
                end
            end))
        else
            vehicleRO.SepAniEndCallback = nil
            vehicleRO:SetSepSkeleton("__root__", 0.15);
            if actionType == EnumCandyDeliveryActionType.eDie then
                self:SyncCandyDeliveryVehicleSlowFx(engineId, false)
                vehicleRO:DoSepAni("die01", false, 0, 1, 0, true, 1)
                return
            elseif actionType == EnumCandyDeliveryActionType.eRushHit then
                vehicleRO:DoSepAni("rush01",false,0,1,0.15,true,1);
            elseif actionType == EnumCandyDeliveryActionType.eBeRushHit then
                vehicleRO:DoSepAni("rushed01", false, 0, 1, 0, true, 1)
            elseif actionType == EnumCandyDeliveryActionType.eRelive then
                vehicleRO:DoSepAni("bianxing01_stand01", false, 0, 1, 0, true, 1)
            end
            -- 除了死亡，其他动作播放完毕后恢复到站立动作
            vehicleRO.SepAniEndCallback = DelegateFactory.Action(function()
                if vehicleRO and self.m_CandyDeliveryGaming then
                    vehicleRO:SetSepSkeleton(nil, 0.15)
                    vehicleRO.SepAniEndCallback = nil
                end
            end)
        end
    end
end

function LuaHalloween2023Mgr:OnAddBuffFX(args)
    local engineId = args[0]
    local buffId = args[1]
    local isDie = self.m_CandyDeliveryStatusType[engineId] == EnumCandyDeliveryActionType.eDie or self.m_CandyDeliveryStatusType[engineId] == EnumCandyDeliveryActionType.eDying
    if not isDie and math.floor(buffId / 100) == 660423 then  -- 减速硬糖
        self:SyncCandyDeliveryVehicleSlowFx(engineId, true)
    end
end

function LuaHalloween2023Mgr:OnRemoveBuffFX(args)
    local engineId = args[0]
    local buffId = args[1]
    if math.floor(buffId / 100) == 660423 then  -- 减速硬糖
        self:SyncCandyDeliveryVehicleSlowFx(engineId, false)
    end
end

function LuaHalloween2023Mgr:SyncCandyDeliveryVehicleSlowFx(engineId, isShow)
    local driverRo = CClientObjectMgr.Inst:GetObject(engineId)
    local vehicleRO = driverRo and driverRo.VehicleRO or nil
    if vehicleRO == nil then return end
    if isShow and not vehicleRO:ContainFx("CandyDeliveryVehicleSlowFx") then
        local fx = CEffectMgr.Inst:AddObjectFX(88804907,vehicleRO,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
        vehicleRO:AddFX("CandyDeliveryVehicleSlowFx", fx)
    elseif not isShow and vehicleRO:ContainFx("CandyDeliveryVehicleSlowFx") then
        vehicleRO:RemoveFX("CandyDeliveryVehicleSlowFx")
    end
end

-- 骑手头顶血条
function LuaHalloween2023Mgr:OnTeamHpUpdate(driverPlayerId, passengerPlayerId, hp)
    LuaGamePlayHpMgr:AddHpInfo(driverPlayerId, hp, self.m_CandyDeliveryGameSetData.MaxCarHp, false, 1, false)
    LuaGamePlayHpMgr:AddHpInfo(passengerPlayerId, hp, self.m_CandyDeliveryGameSetData.MaxCarHp, false, 2, false)
end

-- 获得糖果后显示箭头指向目标NPC
LuaHalloween2023Mgr.m_CandyDirectorEffect = nil
function LuaHalloween2023Mgr:UpdateDirectorEffect(buffId)
    if self.m_CandyDeliveryGameSetData == nil then self:InitCandyDeliveryGameSetData() end
    local vehicleRO = CClientMainPlayer.Inst.VehicleRO
    if vehicleRO == nil then return end
    local buffData = self.m_CandyDeliveryGameSetData.candyBuffInfos[buffId]
    if buffData == nil then
        vehicleRO:RemoveFX("CandyDirectorEffect")
        if self.m_CandyDirectorEffect then
            self.m_CandyDirectorEffect.directorEffect.m_TargetRO:Destroy()
            self.m_CandyDirectorEffect = nil
        end
        return
    end
    if self.m_CandyDirectorEffect == nil or self.m_CandyDirectorEffect.directorEffect == nil or not vehicleRO:ContainFx("CandyDirectorEffect") then
        if vehicleRO:ContainFx("CandyDirectorEffect") then vehicleRO:RemoveFX("CandyDirectorEffect") end
        local fxId = Halloween2023_Setting.GetData().CandyDirectorFx
        local fx = CEffectMgr.Inst:AddObjectFX(fxId,vehicleRO,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero, DelegateFactory.Action_GameObject(function(go)
            self.m_CandyDirectorEffect = {}
            self.m_CandyDirectorEffect.directorEffect = go:GetComponent(typeof(CDirectorEffect))
            self:SetDirectorEffectTargetRO(buffId)
        end))
        vehicleRO:AddFX("CandyDirectorEffect", fx)
    else
        self:SetDirectorEffectTargetRO(buffId)
    end
end

function LuaHalloween2023Mgr:SetDirectorEffectTargetRO(buffId)
    if self.m_CandyDirectorEffect.buffId ~= buffId then
        if not self.m_CandyDirectorEffect.targetRo then
            if not CClientObjectRoot.Inst then return end
            local root = CClientObjectRoot.Inst.Other.gameObject
            self.m_CandyDirectorEffect.targetRo = CRenderObject.CreateRenderObject(root, "CandyDirectorNPC")
        end
        local buffInfo = self.m_CandyDeliveryGameSetData.candyBuffInfos[buffId]
        local npcInfo = self.m_CandyDeliveryGameSetData.candyDeliveryNpcInfos[buffInfo.NPCId]
        self.m_CandyDirectorEffect.targetRo.Position = npcInfo
        self.m_CandyDirectorEffect.directorEffect.m_TargetRO = self.m_CandyDirectorEffect.targetRo
    end
end

-- 乘客头顶分数
function LuaHalloween2023Mgr:OnTeamBuffUpdate(engineId, buffId)
    local args = {}

    local buffData = self.m_CandyDeliveryGameSetData.candyBuffInfos[buffId]
    if buffData then
        local textureForm = {}
        textureForm.texturePath = "UI/Texture/Transparent/Material/huluwazhanlingwnd_white.mat"
        textureForm.textureSize = {120, 120}
        textureForm.color = Color.black
        textureForm.alpha = 0.8
        args[1] = textureForm

        local LabelForm = {}
        LabelForm.FontType = "shufa"
        LabelForm.content = tostring(buffData.Score)
        LabelForm.LabelSize = 42
        LabelForm.Color = NGUIText.ParseColor24(buffData.Color, 0)
        args[2] = LabelForm
    end

    args[0] = engineId
    args[3] = {}
    args[4] = buffData ~= nil
    args[5] = "Halloween2023OrderBattle"

    g_ScriptEvent:BroadcastInLua("UpdatePlayerHeadInfoCountDown",args)
end

-- 结算
LuaHalloween2023Mgr.m_CandyDeliveryResultInfo = nil
function LuaHalloween2023Mgr:SendCandyDeliveryResultInfo(resultInfo)
    self.m_CandyDeliveryGaming = false
    if self.m_CandyDeliveryGameSetData == nil then self:InitCandyDeliveryGameSetData() end
    local resultInfo = g_MessagePack.unpack(resultInfo)
    self.m_CandyDeliveryResultInfo = {}
    self.m_CandyDeliveryResultInfo.Rank = resultInfo[1]
    self.m_CandyDeliveryResultInfo.Score = resultInfo[2]
    self.m_CandyDeliveryResultInfo.OrderCount = resultInfo[3]
    CUIManager.ShowUI(CLuaUIResources.Halloween2023OrderBattleResultWnd)
end

-- 规则界面
LuaHalloween2023Mgr.m_ShowOrderBattleRuleWndType = nil
function LuaHalloween2023Mgr:OpenHalloween2023OrderBattleRuleWnd(type)
    self.m_ShowOrderBattleRuleWndType = type
    CUIManager.ShowUI(CLuaUIResources.Halloween2023OrderBattleRuleWnd)
end

function LuaHalloween2023Mgr:ShowHalloween2023OrderBattleRuleWnd(zuoQiId, driverPlayerId, passengerPlayerId)
    if self.m_MyTeamRoleInfo == nil then self.m_MyTeamRoleInfo = {} end
    self.m_MyTeamRoleInfo.driverPlayerId = driverPlayerId
    self.m_MyTeamRoleInfo.passengerPlayerId = passengerPlayerId
    self.m_MyTeamRoleInfo.zuoQiId = zuoQiId
    LuaHalloween2023Mgr:OpenHalloween2023OrderBattleRuleWnd("GameBegin")
end

-- 糖果生成倒计时
function LuaHalloween2023Mgr:SendCandyDeliveryCountDown(countDown)
end

-- 糖果生成特效
function LuaHalloween2023Mgr:ShowCandyDeliverySummonFx(fromEngineId, toEngineId)  
    local monster = CClientObjectMgr.Inst:GetObject(toEngineId)
    local fromObj = CClientObjectMgr.Inst:GetObject(fromEngineId)
    if not monster or not fromObj then return end
    -- 先隐藏
    monster.RO.gameObject:SetActive(false)
    monster.Visible = false
    local fxId = Halloween2023_Setting.GetData().CandyBulletFX
    local fx = CEffectMgr.Inst:AddBulletFX(fxId[0], fromObj.RO, monster.RO, 0, 1, -1, DelegateFactory.Action(function()
        -- 特效播放完毕再显示
        if not monster then return end
        monster.RO.gameObject:SetActive(true)
        monster.Visible = true
        local pos = monster.RO.transform.position
        CEffectMgr.Inst:AddWorldPositionFX(fxId[1], pos, 0,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
    end))
    fromObj.RO:UnRefFX(fx)
end

-- 减速硬糖技能检查
function LuaHalloween2023Mgr:CastSkillCheckInCandyDelivery(skillId)
    if not self.m_CandyDeliveryGaming then return false end
    if self.m_CandyDeliveryGameSetData == nil then self:InitCandyDeliveryGameSetData() end
    if skillId ~= self.m_CandyDeliveryGameSetData.SlowDownSkillId then return true end
    if CClientMainPlayer.Inst == nil then return true end
    local myObj = CClientMainPlayer.Inst
    local target = CClientMainPlayer.Inst.Target

    local check = self:CheckTarget(target, myObj)   -- 当前目标检查
    if check == 0 then
        return self:SetNewTarget(myObj)
    elseif check == 1 then
        return true
    else
        return false
    end
end

function LuaHalloween2023Mgr:CheckTarget(target, myObj)
    if target then
        local campInfo = LuaHalloween2023Mgr.CandyDeliveryCampInfo
        for _, info in pairs(campInfo) do
            local targetEngineId = target.EngineId
            if not info.myTeam then
                if info.passengerEngineId == targetEngineId then
                    targetEngineId = info.driverEngineId
                end
                if info.driverEngineId == targetEngineId then
                    local curTarget = CClientObjectMgr.Inst:GetObject(targetEngineId)
                    local dist = Vector3.Distance(Utility.PixelPos2WorldPos(myObj.Pos), Utility.PixelPos2WorldPos(curTarget.Pos))
                    if CStatusMgr.Inst:GetStatus(curTarget,EPropStatus.GetId("Halloween2023Dead")) == 1 then
                        return 0    -- 选中不是合法目标
                    elseif dist <= self.m_CandyDeliveryGameSetData.SlowDownSkillRange then
                        CClientMainPlayer.Inst.Target = curTarget
                        return 1    -- 合法目标
                    else
                        g_MessageMgr:ShowMessage("Halloween2023_CandyDelivery_SlowSkill_OverDistance")
                        return 2    -- 目标超出范围
                    end
                end
            end

        end
    end
    return 0    -- 没有目标/选中不是合法目标
end

function LuaHalloween2023Mgr:SetNewTarget(myObj)
    local distList = {}
    local campInfo = LuaHalloween2023Mgr.CandyDeliveryCampInfo
    for campId, info in pairs(campInfo) do
        if not info.myTeam then
            local targetObj = CClientObjectMgr.Inst:GetObject(info.driverEngineId)
            local dist = Vector3.Distance(myObj.RO.transform.position, targetObj.RO.transform.position)
            if dist < self.m_CandyDeliveryGameSetData.SlowDownSkillRange and CStatusMgr.Inst:GetStatus(targetObj,EPropStatus.GetId("Halloween2023Dead")) == 0 then
                table.insert(distList, {campId = campId, dist = dist, targetObj = targetObj})
            end
        end
    end
    if #distList > 0 then
        table.sort(distList, function (a, b)
            return a.dist < b.dist
        end)
        CClientMainPlayer.Inst.Target = distList[1].targetObj
        return true
    else
        g_MessageMgr:ShowMessage("Halloween2023_CandyDelivery_SlowSkill_NoTarget")
        return false
    end
end