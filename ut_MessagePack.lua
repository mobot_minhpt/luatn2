
require("Lua/core/ScriptEngine");
require("Lua/util/luaunit");

local lDumpTable = function(t)
	print("\nDumpTable: " .. tostring(t) .. "");
	for k,v in pairs(t) do
		print(" pairs: [" .. tostring(k) .. "]=[" .. tostring(v) .. "]");
		if(type(v) == "table") then
			DumpTable(v);
		end
	end
	print("EndDump: " .. tostring(t) .. "\n");
end

local tPacket = 
{
	_c = "MyTestPacket";
	
	bKey0 = false;
	bKey1 = true;
	
	strKey1 = "str key 1 value";
	strKey2 = "中国";
	
	iKey1 = 9527;
	iKey2 = -1927;
	
	fKey1 = -2.763400;
	fKey2 = 3.1415926;

	MySubArray  = {2, 4, 6, 8, 10};

	MySubObject = {
		_c = "MySubObject";
		k1 = "v1";
		k2 = "v2";
	};

};

local tPacketPvp = {
	_c = "FinishPvpFight";

	targetPlayerId = "";
};

local tPvpMatchAccept = {
	_c = "PvpMatchAccept";
	state = 9527;
	inviter = "inviter player A";
	inviteeDefendZhaoshi = "invitee defend zhaoshi haha";
};

local strPackBuf = g_MessagePack.pack( tPacket );

local tUnPack = g_MessagePack.unpack(strPackBuf);

--lDumpTable(tUnPack);
if(true) then
	local strHexDump = g_LuaUtil:GetStringHex(strPackBuf);
	local iBufLen = string.len(strPackBuf);
	-- print( string.format("lua bytelen[%d]: %s", iBufLen, strHexDump) ) ;
	
	local bRet = g_NetworkImpl:ScriptSendPacket("MyTestPacket", tPacket);
	--local bRet = g_NetworkImpl:ScriptSendPacket("FinishPvpFight", tPacketPvp);
	--local bRet = g_NetworkImpl:ScriptSendPacket("PvpMatchAccept", tPvpMatchAccept);
	
	return bRet;
end
