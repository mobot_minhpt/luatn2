-- Auto Generated!!
local CSecondPwdConfirmWnd = import "L10.UI.CSecondPwdConfirmWnd"
local DelegateFactory = import "DelegateFactory"
local MessageWndManager = import "L10.UI.MessageWndManager"
CSecondPwdConfirmWnd.m_OnClickForgetPwdButton_CS2LuaHook = function (this, go) 
    --忘记密码可通过关联手机并发送手机验证码进行强制解除。。。
    local str = g_MessageMgr:FormatMessage("Forget_Secondary_Password")
    MessageWndManager.ShowOKCancelMessage(str, DelegateFactory.Action(function () 
        Gac2Gas.RequestClearSecondaryPassword()
    end), nil, nil, nil, false)
end
CSecondPwdConfirmWnd.m_OnClickOkButton_CS2LuaHook = function (this, go) 
    if System.String.IsNullOrEmpty(this.input.value) then
        g_MessageMgr:ShowMessage("Please_Enter_Password")
        return
    end

    if CSecondPwdConfirmWnd.RequestCloseSecondPwd then
        Gac2Gas.RequestCloseSecondaryPassword(this.input.value)
    else
        Gac2Gas.RequestVerifySecondaryPassword(this.input.value)
    end
    --this.Close();
end
