local UIInput = import "UIInput"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CRecordingInfoMgr = import "L10.UI.CRecordingInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local EnumRecordContext = import "L10.Game.EnumRecordContext"
local EnumVoicePlatform = import "L10.Game.EnumVoicePlatform"
local EChatPanel = import "L10.Game.EChatPanel"
local CGuildMgr=import "L10.Game.CGuildMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Animation = import "UnityEngine.Animation"
local CCommonUITween = import "L10.UI.CCommonUITween"
local CChatLinkMgr = import "CChatLinkMgr"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"

LuaGuildPuzzleWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuildPuzzleWnd, "PicItem", "PicItem", GameObject)
RegistChildComponent(LuaGuildPuzzleWnd, "PlayPuzzle", "PlayPuzzle", GameObject)
RegistChildComponent(LuaGuildPuzzleWnd, "PiecePuzzle", "PiecePuzzle", GameObject)
RegistChildComponent(LuaGuildPuzzleWnd, "FullPuzzle", "FullPuzzle", GameObject)
RegistChildComponent(LuaGuildPuzzleWnd, "StateLabel", "StateLabel", UILabel)
RegistChildComponent(LuaGuildPuzzleWnd, "PlayerCountLabel", "PlayerCountLabel", UILabel)
RegistChildComponent(LuaGuildPuzzleWnd, "PercentLabel", "PercentLabel", UILabel)
RegistChildComponent(LuaGuildPuzzleWnd, "PlayTimeLabel", "PlayTimeLabel", UILabel)
RegistChildComponent(LuaGuildPuzzleWnd, "Puzzle1", "Puzzle1", GameObject)
RegistChildComponent(LuaGuildPuzzleWnd, "Puzzle2", "Puzzle2", GameObject)
RegistChildComponent(LuaGuildPuzzleWnd, "RemainPuzzleLabel", "RemainPuzzleLabel", UILabel)
RegistChildComponent(LuaGuildPuzzleWnd, "FinishLabel", "FinishLabel", UILabel)
RegistChildComponent(LuaGuildPuzzleWnd, "GetBtn", "GetBtn", GameObject)
RegistChildComponent(LuaGuildPuzzleWnd, "GetBtnAlert", "GetBtnAlert", GameObject)
RegistChildComponent(LuaGuildPuzzleWnd, "QuestionLabel", "QuestionLabel", UILabel)
RegistChildComponent(LuaGuildPuzzleWnd, "HelpBtn", "HelpBtn", GameObject)
RegistChildComponent(LuaGuildPuzzleWnd, "AnswerBtn", "AnswerBtn", GameObject)
RegistChildComponent(LuaGuildPuzzleWnd, "AnswerBtnLabel", "AnswerBtnLabel", UILabel)
RegistChildComponent(LuaGuildPuzzleWnd, "VoiceBtn", "VoiceBtn", GameObject)
RegistChildComponent(LuaGuildPuzzleWnd, "Input", "Input", UIInput)
RegistChildComponent(LuaGuildPuzzleWnd, "Right", "Right", GameObject)
RegistChildComponent(LuaGuildPuzzleWnd, "Wrong", "Wrong", GameObject)
RegistChildComponent(LuaGuildPuzzleWnd, "AnswerTipLabel", "AnswerTipLabel", UILabel)
RegistChildComponent(LuaGuildPuzzleWnd, "PiecePuzzleTexture", "PiecePuzzleTexture", GameObject)
RegistChildComponent(LuaGuildPuzzleWnd, "BigPuzzle", "BigPuzzle", GameObject)
RegistChildComponent(LuaGuildPuzzleWnd, "BigPuzzlePicTable", "BigPuzzlePicTable", GameObject)
RegistChildComponent(LuaGuildPuzzleWnd, "PicTable", "PicTable", GameObject)
RegistChildComponent(LuaGuildPuzzleWnd, "GuildName", "GuildName", UILabel)
RegistChildComponent(LuaGuildPuzzleWnd, "CompleteTimeLabel", "CompleteTimeLabel", UILabel)
RegistChildComponent(LuaGuildPuzzleWnd, "ShareBtn", "ShareBtn", GameObject)
RegistChildComponent(LuaGuildPuzzleWnd, "CompleteDescLabel", "CompleteDescLabel", UILabel)

--@endregion RegistChildComponent end

function LuaGuildPuzzleWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.GetBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGetBtnClick()
	end)

	UIEventListener.Get(self.HelpBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnHelpBtnClick()
	end)

	UIEventListener.Get(self.AnswerBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAnswerBtnClick()
	end)

	UIEventListener.Get(self.VoiceBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnVoiceBtnClick()
	end)

	UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)

    --@endregion EventBind end
end

function LuaGuildPuzzleWnd:Init()
    self.PicItem.transform:GetComponent(typeof(UITexture)).enabled = true
    self.PicItem:SetActive(false)

    -- 拖动
    self.m_IsDragging = false
    self.m_TweenList = {}

    self.m_IsFirstInFinish = true
    
    -- 初始化
    self.m_Data = CLuaGuildMgr.m_PinTuDetailData
    self.m_PinTuId = self.m_Data.id

    self.PlayPuzzle:SetActive(true)
    self.FullPuzzle:SetActive(false)
    self.PiecePuzzle.transform:GetComponent(typeof(CCommonUITween)):PlayDisapperAnimation()

    self:InitWnd()

    if self.m_Data.finishtime <= 0 then
        self.transform:GetComponent(typeof(Animation)):Play("guildpuzzlewnd_show")
    end

    self.m_SendTickList = {}
end

function LuaGuildPuzzleWnd:InitWnd()
    if self.m_FullPuzzleInited then
        return
    end

    local chipList = {}
    -- 初始化拥有的碎片
    for i = 1, 2 do
        local chipId = self.m_Data.chipIdList[i] or 0
        table.insert(chipList, chipId)

        if self.m_CacheChipList == nil or self.m_CacheChipList[i] ~= chipId then
            self:InitSelfPuzzleItem(i, chipId)
        end
    end

    self.m_CacheChipList = chipList

    -- 初始化主拼图
    self:InitMainPuzzle()
    self:InitBottom()
    self:InitStage(CLuaGuildMgr.m_PinTuStage, CLuaGuildMgr.m_PinTuDuration)

    if self.m_Data.finishtime > 0 then
        self:InitFullPuzzle()
    else
        self.m_IsFirstInFinish = false
    end
end

function LuaGuildPuzzleWnd:InitStage(stage, duration)
    UnRegisterTick(self.m_Tick)
    self.m_Duration = duration

    self.m_CanPinTu = true

    local durationLabel = self.PlayPuzzle.transform:Find("Top/StateLabel"):GetComponent(typeof(UILabel))
    local stageLabel = self.PlayPuzzle.transform:Find("Top/StateLabel/Label"):GetComponent(typeof(UILabel))

    if stage == EnumGuildPinTuPlayStage.eNotOpen then
        -- 未开启
        stageLabel.text = LocalString.GetString("未开启")
        durationLabel.text = ""
        self.m_CanPinTu = false
    elseif stage == EnumGuildPinTuPlayStage.eCountDown then
        -- 倒计时
        stageLabel.text = LocalString.GetString("未开启")
        durationLabel.text = ""
        self.m_CanPinTu = false
    elseif stage == EnumGuildPinTuPlayStage.eOpen then
        -- 开启玩法
        stageLabel.text = LocalString.GetString("拼图进行中")
        durationLabel.text = CUICommonDef.SecondsToTimeString(self.m_Duration, true)

        self.m_Tick = RegisterTick(function()
            self.m_Duration = self.m_Duration - 1
            if self.m_Duration < 0 then
                self.m_Duration = 0
            end
            durationLabel.text = CUICommonDef.SecondsToTimeString(self.m_Duration, true)
        end, 1000)
        self.m_CanPinTu = true
    elseif stage == EnumGuildPinTuPlayStage.eEnd then
        -- 已结束
        stageLabel.text = LocalString.GetString("[c][FF0000]本周拼图已结束[-][/c]")
        durationLabel.text = ""
        self.m_CanPinTu = false
    end
end

function LuaGuildPuzzleWnd:InitGetPieceWnd(chipId)
    self.PlayPuzzle:SetActive(true)
    self.FullPuzzle:SetActive(false)
    self.PiecePuzzle:SetActive(true)

    local place = chipId % 100
    local matPath = SafeStringFormat3("UI/Texture/PinTu/material/pintu_%02d_%02d.mat", self.m_Data.id, place)
    self.PiecePuzzleTexture.gameObject:GetComponent(typeof(CUITexture)):LoadMaterial(matPath)
    self.PiecePuzzle.transform:GetComponent(typeof(CCommonUITween)):PlayAppearAnimation()

    UIEventListener.Get(self.PiecePuzzle.transform:Find("Bg").gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self.PlayPuzzle:SetActive(true)
        self.FullPuzzle:SetActive(false)
        self.PiecePuzzle.transform:GetComponent(typeof(CCommonUITween)):PlayDisapperAnimation()

        self.m_GetChipId = chipId
        self:InitWnd()
    end)
end

function LuaGuildPuzzleWnd:InitBottom()
    local data = GuildPinTu_Picture.GetData(self.m_PinTuId)
    self.QuestionLabel.text = data.Question
    
    self.Right:SetActive(false)
    self.Wrong:SetActive(false)
    self.VoiceBtn:SetActive(false)
    self.AnswerTipLabel.gameObject:SetActive(false)

    local inputResult = self.PlayPuzzle.transform:Find("Bottom/InputResult").gameObject

    UnRegisterTick(self.m_AnswerLabelTick)

    if self.m_Data.answered ~= 0 then
        self.Input.value = self.m_Data.answer
        self.Input.gameObject:SetActive(false)
        inputResult:SetActive(true)
        
        local label = self.PlayPuzzle.transform:Find("Bottom/InputResult/Label"):GetComponent(typeof(UILabel))
        label.text = self.m_Data.answer

        self.Right:SetActive(self.m_Data.answer == data.Answer)
        self.Wrong:SetActive(self.m_Data.answer ~= data.Answer)

        CUICommonDef.SetActive(self.AnswerBtn.gameObject, false, true)
        self.AnswerBtnLabel.text = LocalString.GetString("已作答")
    elseif self.m_Data.answerCountdown > 0 then
        self.VoiceBtn:SetActive(true)
        self.Input.gameObject:SetActive(false)

        inputResult:SetActive(true)
        self.Right:SetActive(false)
        self.Wrong:SetActive(false)

        self.AnswerBtnLabel.text = LocalString.GetString("作答")
        self.AnswerTipLabel.text = g_MessageMgr:FormatMessage("Guild_Puzzle_Answer_Tip")

        local label = self.PlayPuzzle.transform:Find("Bottom/InputResult/Label"):GetComponent(typeof(UILabel))
        self.m_AnswerLabelTick = RegisterTick(function()
            self.m_Data.answerCountdown = self.m_Data.answerCountdown - 1
            if self.m_Data.answerCountdown < 0 then
                self.m_Data.answerCountdown = 0
                UnRegisterTick(self.m_AnswerLabelTick)
                self:InitWnd()
            end
            label.text = SafeStringFormat3(LocalString.GetString("%s后开始作答"), CUICommonDef.SecondsToTimeString(self.m_Data.answerCountdown, true))
        end, 1000)
    else
        self.VoiceBtn:SetActive(true)
        self.Input.enabled = true
        self.Input.gameObject:SetActive(true)
        inputResult:SetActive(false)

        CUICommonDef.SetActive(self.AnswerBtn.gameObject, true, true)
        self.AnswerBtnLabel.text = LocalString.GetString("作答")

        self.AnswerTipLabel.gameObject:SetActive(true)
        self.AnswerTipLabel.text = g_MessageMgr:FormatMessage("Guild_Puzzle_Answer_Tip")
    end

    local playTime = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eGuildPinTuMonthlyTimes) or 0
    local curTime = #self.m_Data.chipIdList
    local totalTime = GuildPinTu_GameSetting.GetData().MaxPinTuTimes

    if playTime >= totalTime then
        self.PlayTimeLabel.text = SafeStringFormat3("[FF0000]%s[-]/%s", tostring(playTime), tostring(totalTime))
    else
        self.PlayTimeLabel.text = SafeStringFormat3("%s/%s", tostring(playTime), tostring(totalTime))
    end

    local canGet = playTime + curTime < totalTime
    self.GetBtnAlert:SetActive(canGet)
    CUICommonDef.SetActive(self.GetBtn.gameObject, canGet, true)

    -- 是否次数用完
    local hasFinish = playTime >= GuildPinTu_GameSetting.GetData().MaxPinTuTimes

    self.FinishLabel.gameObject:SetActive(hasFinish)
    self.Puzzle1.gameObject:SetActive(not hasFinish)
    self.Puzzle2.gameObject:SetActive(not hasFinish)
    self.GetBtn.gameObject:SetActive(not hasFinish)
    self.RemainPuzzleLabel.gameObject:SetActive(not hasFinish)
    
    self.PlayTimeLabel.transform:Find("Label").gameObject:SetActive(not hasFinish)

    -- 语音输入按钮
    UIEventListener.Get(self.VoiceBtn.gameObject).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        if self.m_Data.answerCountdown > 0 then
            g_MessageMgr:ShowMessage("Guild_Puzzle_Answer_Not_Open")
        else
            if isPressed then
                CRecordingInfoMgr.ShowRecordingWnd(EnumRecordContext.QueQiao, EChatPanel.Guild, go, EnumVoicePlatform.Default, false)
            else
                CRecordingInfoMgr.CloseRecordingWnd(not go:Equals(CUICommonDef.SelectedUI))
            end
        end
    end)
end

function LuaGuildPuzzleWnd:InitSelfPuzzleItem(index, chipId)
    local puzzleNode = nil
    local originPos = Vector3(0,0,0)
    if index == 1 then
        puzzleNode = self.Puzzle1
        originPos = Vector3(683, 97+54, 0)
    elseif index == 2 then
        puzzleNode = self.Puzzle2
        originPos = Vector3(683, 97-96, 0)
    end
    puzzleNode:SetActive(true)

    -- 清空
    local content = puzzleNode.transform:Find("Content")
    CUICommonDef.ClearTransform(content)

    -- 清除相关tick
    UnRegisterTick(self.m_SendTick)
    UnRegisterTick(self.m_SuccessTick)

    if self.m_SendTickList == nil then
        self.m_SendTickList = {}
    end

    for i =1, #self.m_SendTickList do
        UnRegisterTick(self.m_SendTickList[i])
    end

    if self.m_TweenList and self.m_TweenList[index] then
        LuaTweenUtils.Kill(self.m_TweenList[index], false)
        self.m_TweenList[index] = nil
    end

    local place = chipId % 100
    local picId = math.floor(chipId / 1000)

    local revertBtn = puzzleNode.transform:Find("RevertBtn").gameObject
    local voidLabel = puzzleNode.transform:Find("VoidLabel").gameObject

    if place > 0 then
        -- 添加一个item
        local item = NGUITools.AddChild(content.gameObject, self.PicItem.gameObject)
        item.gameObject:SetActive(true)

        local depthTable = {}
        table.insert(depthTable, item.transform:Find("Bg"):GetComponent(typeof(UITexture)))
        table.insert(depthTable, item.transform:Find("Puzzle"):GetComponent(typeof(UITexture)))
        table.insert(depthTable, item.transform:Find("Outline"):GetComponent(typeof(UITexture)))
        table.insert(depthTable, item.transform:GetComponent(typeof(UITexture)))
        table.insert(depthTable,revertBtn.transform:GetComponent(typeof(UITexture)))
        table.insert(depthTable, revertBtn.transform:Find("Label"):GetComponent(typeof(UILabel)))
        table.insert(depthTable, item.transform:Find("CUIFxzhengque"):GetComponent(typeof(UITexture)))
        table.insert(depthTable, item.transform:Find("CUIFxcuowu"):GetComponent(typeof(UITexture)))

        for k,v in ipairs(depthTable) do
            v.depth = v.depth + 10
        end

        local matPath = SafeStringFormat3("UI/Texture/PinTu/material/pintu_%02d_%02d.mat", picId, place)
        item.transform:GetComponent(typeof(CUITexture)):LoadMaterial(matPath)
        puzzleNode.transform:GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/Transparent/Material/guildpuzzleenterwnd_pintu_yingzi.mat")

        voidLabel:SetActive(false)
        revertBtn:SetActive(true)

        local zhengque = item.transform:Find("CUIFxzhengque").gameObject
        local cuowu = item.transform:Find("CUIFxcuowu").gameObject
        zhengque:SetActive(false)
        cuowu:SetActive(false)

        if self.m_GetChipId == chipId then
            item.transform.position = self.PiecePuzzle.transform:Find("PiecePuzzleTexture").position
            if self.m_TweenList[index] then
                LuaTweenUtils.Kill(self.m_TweenList[index], false)
            end
            self.m_TweenList[index] = LuaTweenUtils.DOLocalMove(item.transform, Vector3(0,0,0), 0.5, false)
            self.m_GetChipId = 0
        end

        -- 回退碎片
        UIEventListener.Get(revertBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Guild_Puzzle_Return_Chip_Tip"), function()
                Gac2Gas.RequestReturnGuildPinTuChip(chipId)
            end, nil, nil, nil, false)
        end)

        -- 拖动
        UIEventListener.Get(item.gameObject).onDragStart = DelegateFactory.VoidDelegate(function (go)
            self.m_IsDragging = true
            if self.m_TweenList[index] then
                LuaTweenUtils.Kill(self.m_TweenList[index], false)
            end
            revertBtn:SetActive(false)
        end)

        UIEventListener.Get(item.gameObject).onDrag = DelegateFactory.VectorDelegate(function(go, delta)
            local curPos = go.transform.localPosition
            local targetX = curPos.x + delta.x
            local targetY = curPos.y + delta.y

            local currentPos = UICamera.currentTouch.pos
            go.transform.position = UICamera.currentCamera:ScreenToWorldPoint(Vector3(currentPos.x,currentPos.y,0))
        end)

        UIEventListener.Get(item.gameObject).onDragEnd = DelegateFactory.VoidDelegate(function (go)
            self.m_IsDragging = false

            -- 判断位置
            local pos = go.transform.localPosition
            if index == 2 then
                pos.y = pos.y - 190
            end

            local x = math.floor((pos.x + 1420) / 260 + 0.5)
            local y = math.floor((pos.y - 110) / -180 + 0.5)

            local targetPlace = 0
            if x >= 0 and y >= 0 then
                targetPlace = y * 5 + x + 1
            end

            -- 是否在拼图区域
            if targetPlace >= 1 and targetPlace <= 20 then
                if place == targetPlace and picId == self.m_Data.id then
                    local tf = self.PicTable.transform:GetChild(targetPlace -1)

                    if self.m_CanPinTu then
                        if tf then
                            -- 移动到准确位置
                            self.m_TweenList[index] = LuaTweenUtils.DOMove(go.transform, tf.position, 0.3, false)
                        end
    
                        UnRegisterTick(self.m_SuccessTick)
                        self.m_SuccessTick = RegisterTickOnce(function()
                            -- 拼图成功
                            zhengque:SetActive(false)
                            zhengque:SetActive(true)
                        end, 300)
    
                        UnRegisterTick(self.m_SendTickList[index])
                        self.m_SendTickList[index] = RegisterTickOnce(function()
                            zhengque:SetActive(false)
                            cuowu:SetActive(false)
                            revertBtn:SetActive(true)
                            Gac2Gas.RequestUseGuildPinTuChip(self.m_PinTuId, chipId, place)
                        end, 1000)
                    else
                        -- 拼对了 但是不能拼了
                        revertBtn:SetActive(true)

                        Gac2Gas.RequestUseGuildPinTuChip(self.m_PinTuId, chipId, place)

                        if self.m_TweenList[index] then
                            LuaTweenUtils.Kill(self.m_TweenList[index], false)
                        end
                        self.m_TweenList[index] = LuaTweenUtils.DOLocalMove(go.transform, Vector3(0,0,0), 0.5, false)
                    end
                    return
                else
                    -- 拼图失败
                    cuowu:SetActive(false)
                    cuowu:SetActive(true)

                    UnRegisterTick(self.m_SendTickList[index])
                    self.m_SendTickList[index] = RegisterTickOnce(function()
                        if self.m_TweenList[index] then
                            LuaTweenUtils.Kill(self.m_TweenList[index], false)
                        end
                        self.m_TweenList[index] = LuaTweenUtils.DOLocalMove(go.transform, Vector3(0,0,0), 0.5, false)
                        zhengque:SetActive(false)
                        cuowu:SetActive(false)

                        revertBtn:SetActive(true)
                    end, 1000)
                end
            else
                -- 移动
                revertBtn:SetActive(true)
                if self.m_TweenList[index] then
                    LuaTweenUtils.Kill(self.m_TweenList[index], false)
                end
                self.m_TweenList[index] = LuaTweenUtils.DOLocalMove(go.transform, Vector3(0,0,0), 0.5, false)
            end
        end)
    else
        voidLabel:SetActive(true)
        revertBtn:SetActive(false)

        puzzleNode.transform:GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/Transparent/Material/guildpuzzleenterwnd_pintu_zanwu.mat")
    end
end

-- index从1开始
function LuaGuildPuzzleWnd:HasSetChip(index, setchips)
    local result = bit.band(bit.lshift(1 ,index-1), setchips)
    return result > 0
end

-- 获取已经拿到的拼图数量
function LuaGuildPuzzleWnd:GetNum(getchips)
    local sum = 0
    for i = 1,20 do
        local result = bit.band(bit.lshift(1 ,i-1), getchips)
        if result > 0 then
            sum = sum + 1
        end
    end
    return sum
end

-- 获取已经拿到的拼图数量
function LuaGuildPuzzleWnd:InitFullPuzzle()
    self.PlayPuzzle:SetActive(true)
    self.FullPuzzle:SetActive(true)
    self.PiecePuzzle:SetActive(false)

    -- 信息
    self.GuildName.text = CGuildMgr.Inst.m_GuildInfo and CGuildMgr.Inst.m_GuildInfo.Name or ""
    local dateTime = CServerTimeMgr.ConvertTimeStampToZone8Time(self.m_Data.finishtime)
    self.CompleteTimeLabel.text = System.String.Format("{0:yyyy-MM-dd}", dateTime)
    self.CompleteDescLabel.text = ""

    if self.m_IsFirstInFinish then
        self.transform:GetComponent(typeof(Animation)):Play("guildpuzzlewnd_full_1")
    else
        self.transform:GetComponent(typeof(Animation)):Play("guildpuzzlewnd_full")
    end

    local matPath = SafeStringFormat3("UI/Texture/PinTu/material/pintu_%02d.mat", self.m_PinTuId)
    self.FullPuzzle.transform:Find("BigPuzzle/Puzzle"):GetComponent(typeof(CUITexture)):LoadMaterial(matPath)
    -- 关闭
    local closeBtn = self.FullPuzzle.transform:Find("CloseBtn2").gameObject
    UIEventListener.Get(closeBtn).onClick = DelegateFactory.VoidDelegate(function()
        CUIManager.CloseUI(CLuaUIResources.GuildPuzzleWnd)
    end)

    self.m_FullPuzzleInited = true
end

-- id, getchips, setchips, answered, answer, finishtime, pinTuPlayerCount, chipIdList
function LuaGuildPuzzleWnd:InitMainPuzzle()
    local sum = 0
    local id = self.m_Data.id

    --拼图图片与偏移
    local picData = GuildPinTu_Picture.GetData(id)
    local picRes = picData.Res

    -- 20块拼图都安装上
    local table = self.PlayPuzzle.transform:Find("BigPuzzle/PicTable"):GetComponent(typeof(UITable))
    Extensions.RemoveAllChildren(table.transform)
    for i=1, 20 do
        local g = NGUITools.AddChild(table.gameObject, self.PicItem)
        g:SetActive(true)

        local texture = g.transform:Find("Puzzle"):GetComponent(typeof(CUITexture))
        texture.gameObject:SetActive(true)
        local matPath = SafeStringFormat3("UI/Texture/PinTu/material/pintu_%02d_%02d.mat", id, i)

        if self:HasSetChip(i, self.m_Data.setchips) then
            sum = sum + 1
            texture:LoadMaterial(matPath)
        else
            texture:LoadMaterial("")
        end
    end
    table:Reposition()

    -- 进度
    self.PercentLabel.text = SafeStringFormat3("%s%%", tostring(sum * 100 / 20))

    -- 人数
    local maxPlayer = GuildPinTu_GameSetting.GetData().MaxPlayerOnePinTu
    local curPlayer = self.m_Data.pinTuPlayerCount

    if curPlayer == maxPlayer then
        -- 红色
        self.PlayerCountLabel.text = SafeStringFormat3("[c][FF0000]%s[-][/c]/%s", curPlayer, maxPlayer)
    else
        -- 绿色
        self.PlayerCountLabel.text = SafeStringFormat3("[c][1E8F50]%s[-][/c]/%s", curPlayer, maxPlayer)
    end

    -- 剩余拼图
    local residueNum = self:GetNum(self.m_Data.getchips)
    self.RemainPuzzleLabel.text = SafeStringFormat3(LocalString.GetString("剩余碎片 %s"), 20 - residueNum)
end

-- 翻译后的语音输入
function LuaGuildPuzzleWnd:OnVoiceTranslateFinished(args)
    local context = args[0]
	local voiceId = args[1]
	local result = args[2]
    if self.m_Data.answered == 0 then
        self.Input.value = result
    end
end

-- 拼图数据
function LuaGuildPuzzleWnd:OnData(id, getchips, setchips, answered, answer, finishtime, pinTuPlayerCount, chipIdList, answerCountdown)
    self.m_Data = {}
    self.m_Data.id = id
    self.m_Data.getchips = getchips
    self.m_Data.setchips = setchips
    self.m_Data.answered = answered
    self.m_Data.answer = answer
    self.m_Data.finishtime = finishtime
    self.m_Data.pinTuPlayerCount = pinTuPlayerCount
    self.m_Data.chipIdList = chipIdList
    self.m_Data.answerCountdown = answerCountdown

    self:InitWnd()
end

-- 获取新的碎片
function LuaGuildPuzzleWnd:OnGetGuildPinTuChipSuccess(pictureId, chipId)
    if pictureId == self.m_PinTuId and self.m_Data then
        table.insert(self.m_Data.chipIdList, chipId)

        local place = chipId % 1000
        self.m_Data.getchips = bit.bor(bit.lshift(1 ,place-1), self.m_Data.getchips)

        self:InitGetPieceWnd(chipId)
    end
end

-- 返回碎片
function LuaGuildPuzzleWnd:OnReturnGuildPinTuChipSuccess(pictureId, chipId)
    if self.m_Data then
        local list = {}

        for i = 1, #self.m_Data.chipIdList do
            if self.m_Data.chipIdList[i] ~= chipId then
                table.insert(list, self.m_Data.chipIdList[i])
            end
        end
        self.m_Data.chipIdList = list

        local place = chipId % 1000
        self.m_Data.getchips = bit.band(bit.bnot(bit.lshift(1 ,place-1)), self.m_Data.getchips)

        self:InitWnd()
    end
end

-- 使用碎片成功
function LuaGuildPuzzleWnd:OnUseGuildPinTuChipSuccess(pictureId, chipId, place)
    if pictureId == self.m_PinTuId and self.m_Data then
        -- 移除碎片
        local list = {}
        for i = 1, #self.m_Data.chipIdList do
            if self.m_Data.chipIdList[i] ~= chipId then
                table.insert(list, self.m_Data.chipIdList[i])
            end
        end
        self.m_Data.chipIdList = list
        
        -- 拼入拼图
        self.m_Data.setchips = bit.bor(bit.lshift(1 ,place-1), self.m_Data.setchips)

        self:InitWnd()
    end
end

-- 使用碎片失败
function LuaGuildPuzzleWnd:OnUseGuildPinTuChipFail(pictureId, chipId, place)

end

-- 答题成功
function LuaGuildPuzzleWnd:OnAnswerGuildPinTuQuestionSuccess(pictureId, answered, answer, placeListUD)
    if pictureId == self.m_PinTuId and self.m_Data then
        self.m_Data.answered = answered
        self.m_Data.answer = answer

        -- 拼入拼图
        for i, place in ipairs(placeListUD) do
            self.m_Data.getchips = bit.bor(bit.lshift(1 ,place-1), self.m_Data.getchips)
            self.m_Data.setchips = bit.bor(bit.lshift(1 ,place-1), self.m_Data.setchips)

            -- 删掉自己拥有的碎片
            local chipId = pictureId * 1000 + place
            local list = {}
            for j = 1, #self.m_Data.chipIdList do
                if self.m_Data.chipIdList[j] ~= chipId then
                    table.insert(list, self.m_Data.chipIdList[j])
                end
            end
            self.m_Data.chipIdList = list
        end
        
        self:InitWnd()
    end

    self.transform:Find("VFX2"):GetComponent(typeof(Animation)):Play("nuodongpintufankui_1")
end

-- 答题失败
function LuaGuildPuzzleWnd:OnAnswerGuildPinTuQuestionFail(pictureId, answered, answer)
    if pictureId == self.m_PinTuId and self.m_Data then
        self.m_Data.answered = answered
        self.m_Data.answer = answer
        
        self:InitWnd()
    end
end

-- 玩家拼上拼图
function LuaGuildPuzzleWnd:OnPlayerFillGuildPinTu(playerId, pictureId, place, id, getchips, setchips, answered, answer, finishtime)
    if pictureId == self.m_PinTuId and self.m_Data then
        self.m_Data.answered = answered
        self.m_Data.answer = answer
        self.m_Data.getchips = getchips
        self.m_Data.setchips = setchips
        self.m_Data.finishtime = finishtime
        
        self:InitWnd()
    end
end

-- 玩家答题
function LuaGuildPuzzleWnd:OnPlayerAnswerGuildPinTuQuestion(playerId, pictureId, placeList, id, getchips, setchips, answered, answer, finishtime)
    if pictureId == self.m_PinTuId and self.m_Data then
        self.m_Data.answered = answered
        self.m_Data.answer = answer
        self.m_Data.getchips = getchips
        self.m_Data.setchips = setchips
        self.m_Data.finishtime = finishtime

        -- 拼入拼图
        for i, place in ipairs(placeList) do
            -- 删掉自己拥有的碎片
            local chipId = pictureId * 1000 + place
            local list = {}
            for j = 1, #self.m_Data.chipIdList do
                if self.m_Data.chipIdList[j] ~= chipId then
                    table.insert(list, self.m_Data.chipIdList[j])
                end
            end
            self.m_Data.chipIdList = list
        end
        
        self:InitWnd()
    end
end

-- 玩家数量变更，进入
function LuaGuildPuzzleWnd:OnPlayerEnterGuildPinTu(playerId, pictureId, pinTuPlayerCount)
    self.m_Data.pinTuPlayerCount = pinTuPlayerCount
    self:InitWnd()
end

-- 玩家数量变更，退出
function LuaGuildPuzzleWnd:OnPlayerLeaveGuildPinTu(playerId, pictureId, pinTuPlayerCount)
    self.m_Data.pinTuPlayerCount = pinTuPlayerCount
    self:InitWnd()
end

-- 游戏开始
function LuaGuildPuzzleWnd:OnGuildPinTuPlayStart()
    CLuaGuildMgr.m_PinTuStage = EnumGuildPinTuPlayStage.eOpen
    self:InitStage(CLuaGuildMgr.m_PinTuStage, CLuaGuildMgr.m_PinTuDuration)

    local kaishi = self.transform:Find("VFX1/kaishi")
    kaishi.gameObject:SetActive(true)
    kaishi:GetComponent(typeof(Animation)):Play("guildpuzzlewnd_kaishipintu")
end

-- 游戏结束
function LuaGuildPuzzleWnd:OnGuildPinTuPlayEnd()
    CLuaGuildMgr.m_PinTuStage = EnumGuildPinTuPlayStage.eEnd
    self:InitStage(CLuaGuildMgr.m_PinTuStage, CLuaGuildMgr.m_PinTuDuration)

    local jieshu = self.transform:Find("VFX1/jieshu")
    jieshu.gameObject:SetActive(true)
    jieshu:GetComponent(typeof(Animation)):Play("guildpuzzlewnd_pintujieshu")
end

function LuaGuildPuzzleWnd:OnEnable()
    g_ScriptEvent:AddListener("OnVoiceTranslateFinished", self, "OnVoiceTranslateFinished")
    g_ScriptEvent:AddListener("OpenGuildPinTuDetailWnd", self, "OnData")
    g_ScriptEvent:AddListener("GetGuildPinTuChipSuccess", self, "OnGetGuildPinTuChipSuccess")
    g_ScriptEvent:AddListener("ReturnGuildPinTuChipSuccess", self, "OnReturnGuildPinTuChipSuccess")
    g_ScriptEvent:AddListener("UseGuildPinTuChipFail", self, "OnUseGuildPinTuChipFail")
    g_ScriptEvent:AddListener("UseGuildPinTuChipSuccess", self, "OnUseGuildPinTuChipSuccess")
    g_ScriptEvent:AddListener("AnswerGuildPinTuQuestionSuccess", self, "OnAnswerGuildPinTuQuestionSuccess")
    g_ScriptEvent:AddListener("AnswerGuildPinTuQuestionFail", self, "OnAnswerGuildPinTuQuestionFail")
    g_ScriptEvent:AddListener("PlayerFillGuildPinTu", self, "OnPlayerFillGuildPinTu")
    g_ScriptEvent:AddListener("PlayerAnswerGuildPinTuQuestion", self, "OnPlayerAnswerGuildPinTuQuestion")
    g_ScriptEvent:AddListener("PlayerEnterGuildPinTu", self, "OnPlayerEnterGuildPinTu")
    g_ScriptEvent:AddListener("PlayerLeaveGuildPinTu", self, "OnPlayerLeaveGuildPinTu")
    g_ScriptEvent:AddListener("GuildPinTuPlayStart", self, "OnGuildPinTuPlayStart")
    g_ScriptEvent:AddListener("GuildPinTuPlayEnd", self, "OnGuildPinTuPlayEnd")
end

function LuaGuildPuzzleWnd:OnDisable()
    -- 离开时候要请求退出
    Gac2Gas.RequestLeaveGuildPinTu(self.m_PinTuId)

    for _, t in ipairs(self.m_TweenList) do
        if t then
            LuaTweenUtils.Kill(t, false)
        end
    end

    if self.m_SendTickList == nil then
        self.m_SendTickList = {}
    end
    
    for i =1, #self.m_SendTickList do
        UnRegisterTick(self.m_SendTickList[i])
    end

    UnRegisterTick(self.m_Tick)
    UnRegisterTick(self.m_SendTick)
    UnRegisterTick(self.m_AnswerLabelTick)
    UnRegisterTick(self.m_SuccessTick)
    UnRegisterTick(self.m_ShareBtnClickTick)

    CRecordingInfoMgr.CloseRecordingWnd(true)
    g_ScriptEvent:RemoveListener("OnVoiceTranslateFinished", self, "OnVoiceTranslateFinished")
    g_ScriptEvent:RemoveListener("OpenGuildPinTuDetailWnd", self, "OnData")
    g_ScriptEvent:RemoveListener("GetGuildPinTuChipSuccess", self, "OnGetGuildPinTuChipSuccess")
    g_ScriptEvent:RemoveListener("ReturnGuildPinTuChipSuccess", self, "OnReturnGuildPinTuChipSuccess")
    g_ScriptEvent:RemoveListener("UseGuildPinTuChipFail", self, "OnUseGuildPinTuChipFail")
    g_ScriptEvent:RemoveListener("UseGuildPinTuChipSuccess", self, "OnUseGuildPinTuChipSuccess")
    g_ScriptEvent:RemoveListener("AnswerGuildPinTuQuestionSuccess", self, "OnAnswerGuildPinTuQuestionSuccess")
    g_ScriptEvent:RemoveListener("AnswerGuildPinTuQuestionFail", self, "OnAnswerGuildPinTuQuestionFail")
    g_ScriptEvent:RemoveListener("PlayerFillGuildPinTu", self, "OnPlayerFillGuildPinTu")
    g_ScriptEvent:RemoveListener("PlayerAnswerGuildPinTuQuestion", self, "OnPlayerAnswerGuildPinTuQuestion")
    g_ScriptEvent:RemoveListener("PlayerEnterGuildPinTu", self, "OnPlayerEnterGuildPinTu")
    g_ScriptEvent:RemoveListener("PlayerLeaveGuildPinTu", self, "OnPlayerLeaveGuildPinTu")
    g_ScriptEvent:RemoveListener("GuildPinTuPlayStart", self, "OnGuildPinTuPlayStart")
    g_ScriptEvent:RemoveListener("GuildPinTuPlayEnd", self, "OnGuildPinTuPlayEnd")
end

--@region UIEvent

function LuaGuildPuzzleWnd:OnGetBtnClick()
    local residueNum = self:GetNum(self.m_Data.getchips)
    if residueNum == 20 then
        g_MessageMgr:ShowMessage("Guild_Puzzle_Out_Of_Chips")
    else
        Gac2Gas.RequestGetGuildPinTuChip(self.m_PinTuId)
    end
end

function LuaGuildPuzzleWnd:OnHelpBtnClick()
    Gac2Gas.AskForHelpGuildPinTuQuestion(self.m_PinTuId)
end

function LuaGuildPuzzleWnd:OnAnswerBtnClick()
    -- 请求答题
    if self.m_Data.answered == 0 and self.m_Data.answerCountdown <= 0 then
        local input = self.Input.value
        
        if input and input ~= "" then
            local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(input, false)
  	        if ret.msg == nil or ret.shouldBeIgnore then 
                g_MessageMgr:ShowMessage("Speech_Violation")
                return
            end

            local rightAnwser = GuildPinTu_Picture.GetData(self.m_PinTuId).Answer
            local e = string.find(input, rightAnwser)

            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Guild_Puzzle_Answer_Question_Tip"), function()
                if e then
                    Gac2Gas.RequestAnswerGuildPinTuQuestion(self.m_PinTuId, rightAnwser)
                else
                    Gac2Gas.RequestAnswerGuildPinTuQuestion(self.m_PinTuId, self.Input.value)
                end
            end, nil, nil, nil, false)     
        else
            g_MessageMgr:ShowMessage("Guild_Puzzle_Please_Input_Answer")
        end
    else
        g_MessageMgr:ShowMessage("Guild_Puzzle_Answer_Not_Open")
    end
end

function LuaGuildPuzzleWnd:OnVoiceBtnClick()
    if self.m_Data.answered > 0 then
        g_MessageMgr:ShowMessage("Guild_Puzzle_Answer_Not_Open")
    else
        g_MessageMgr:ShowMessage("VoiceBtn_Need_Long_Press")
    end
end

function LuaGuildPuzzleWnd:OnShareBtnClick()
    self.FullPuzzle.transform:Find("CloseBtn2").gameObject:SetActive(false)
    self.ShareBtn.gameObject:SetActive(false)

    UnRegisterTick(self.m_ShareBtnClickTick)
    self.m_ShareBtnClickTick = RegisterTick(function()
        self.FullPuzzle.transform:Find("CloseBtn2").gameObject:SetActive(true)
        self.ShareBtn.gameObject:SetActive(true)
    end, 100)

    CUICommonDef.CaptureScreenAndShare()
end


--@endregion UIEvent

