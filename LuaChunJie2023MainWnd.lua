local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CWelfareMgr = import "L10.UI.CWelfareMgr"
local CScheduleMgr = import "L10.Game.CScheduleMgr"
LuaChunJie2023MainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaChunJie2023MainWnd, "NianHuoBtn", QnButton)
RegistChildComponent(LuaChunJie2023MainWnd, "ShengYanBtn", QnButton)
RegistChildComponent(LuaChunJie2023MainWnd, "ShouLieBtn", QnButton)

RegistChildComponent(LuaChunJie2023MainWnd, "HongBaoRainBtn", QnButton)
RegistChildComponent(LuaChunJie2023MainWnd, "HuoShuYinHuaBtn", QnButton)

RegistChildComponent(LuaChunJie2023MainWnd, "HongBaoBtn", QnButton)
RegistChildComponent(LuaChunJie2023MainWnd, "SignInBtn", QnButton)

RegistChildComponent(LuaChunJie2023MainWnd, "TimeLb", UILabel)

RegistClassMember(LuaChunJie2023MainWnd, "m_Alert_ShenYan")
RegistClassMember(LuaChunJie2023MainWnd, "m_Alert_NianHuo")
--@endregion RegistChildComponent end

function LuaChunJie2023MainWnd:Awake()
    self.m_Alert_ShenYan = self.ShengYanBtn.transform:Find("Alert").gameObject
    self.m_Alert_NianHuo = self.NianHuoBtn.transform:Find("Alert").gameObject
end

function LuaChunJie2023MainWnd:Init()
    local data = ChunJie_Setting2023.GetData()
    self.TimeLb.text = data.PlayTimeString

    local openType_ZJNH = LuaChunJie2023Mgr:IsTaskOpen(data.ZhengJiuNianHuoTaskID)
    local stateText = openType_ZJNH == 3
                      and SafeStringFormat3(LocalString.GetString("已完成%d/%d"), LuaChunJie2023Mgr.ZZNH_FinishTaskCount, #LuaChunJie2023Mgr.ZZNH_TaskLst)
                      or LocalString.GetString("单人剧情任务")
    self:InitOneButton( self.NianHuoBtn,
                        false,
                        LuaActivityRedDotMgr:IsRedDot(14),
                        LocalString.GetString("拯救年货"),
                        stateText,
                        function() self:OnNianHuoBtnClick() end,
                        openType_ZJNH)

    self:InitOneButton( self.ShengYanBtn,
                        false,
                        LuaActivityRedDotMgr:IsRedDot(13),
                        LocalString.GetString("饕餮盛宴"),
                        ChunJie_TTSYSetting.GetData().PlayTimeString,
                        function() self:OnShengYanBtnClick() end,
                        LuaChunJie2023Mgr:IsTaskOpen(data.TaoTieShengYanTaskID))
                       
    self:InitOneButton( self.ShouLieBtn,
                        false, 
                        false,
                        LocalString.GetString("狩猎饕餮"),
                        LocalString.GetString("组队挑战"),
                        function() self:OnShouLieBtnClick() end,
                        LuaChunJie2023Mgr:IsTaskOpen(data.ShouLieTaoTieTaskID))

    self:InitOneButton( self.HongBaoRainBtn,
                        true, 
                        false,
                        LocalString.GetString("红包雨"),
                        System.String.Empty,
                        function() self:OnHongBaoRainBtnClick() end,
                        LuaChunJie2023Mgr:IsTaskOpen(Task_Schedule.GetData(data.HongBaoRainActivityID).TaskID[0]))
            
    self:InitOneButton( self.HuoShuYinHuaBtn,
                        false, 
                        false,
                        LocalString.GetString("火树银花"),
                        System.String.Empty,
                        function() self:OnHuoShuYinHuaBtnClick() end,
                        LuaChunJie2023Mgr:IsTaskOpen(Task_Schedule.GetData(data.HuoShuYinHuaActivityID).TaskID[0]))

    self:InitOneButton( self.HongBaoBtn,
                        true, 
                        false,
                        LocalString.GetString("红包封面"),
                        System.String.Empty,
                        function() self:OnHongBaoBtnClick() end,
                        LuaChunJie2023Mgr:IsTaskOpen(data.HongBaoCoverTaskID))
                        
    --self:InitOneButton( self.SignInBtn,
    --                    true, 
    --                    false,
    --                    LocalString.GetString("春节签到"),
    --                    System.String.Empty,
    --                    function() self:OnSignInBtnClick() end,
    --                    LuaChunJie2023Mgr:IsTaskOpen(data.ChunJieSignInTaskID))
    self.SignInBtn.gameObject:SetActive(false)
    self.GiftButton = self.transform:Find("BottomBtns/GiftButton")
    self.springCouponsActivityId = 42030282
    self.GiftButton.gameObject:SetActive(CScheduleMgr.Inst:IsCanJoinSchedule(self.springCouponsActivityId, true))
    UIEventListener.Get(self.GiftButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        CUIManager.ShowUI(CLuaUIResources.SpringCouponsWnd)
    end)
end

function LuaChunJie2023MainWnd:InitOneButton(btn, hideButtonIfNotOpen, showAlert, openBtnText, stateText, openClickFunc, openType)
    local realOpen = openType == 3
    -- 点击功能
    UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if realOpen then
            openClickFunc()
        elseif openType == 1 then
            g_MessageMgr:ShowMessage("CHUNJIE2023_NOT_OPEN_IN_TRIAL_DELIVERY")
        elseif openType == 2 then
            g_MessageMgr:ShowMessage("CHUNJIE2023_NOT_OPEN")
        end
    end)
    -- 未开启按钮处理
    btn.gameObject:SetActive(realOpen or not hideButtonIfNotOpen)
    btn.transform:Find("Bg").gameObject:SetActive(realOpen)
    local closedObject = btn.transform:Find("Closed")
    if closedObject then closedObject.gameObject:SetActive(not realOpen) end
    -- 红点
    local alertObject = btn.transform:Find("Alert")
    if alertObject then alertObject.gameObject:SetActive(realOpen and showAlert) end
    -- 按钮下方提示
    local stateLabel = btn.transform:Find("StateLb") or btn.transform:Find("Bg/zhi_02/StateLb")
    if stateLabel then
        stateLabel.gameObject:SetActive(realOpen)
        stateLabel:GetComponent(typeof(UILabel)).text = stateText
    end
end

--@region UIEvent

function LuaChunJie2023MainWnd:OnNianHuoBtnClick()
    if self.m_Alert_NianHuo.activeSelf then
        self.m_Alert_NianHuo:SetActive(false)
        LuaActivityRedDotMgr:OnRedDotClicked(14)
    end
    CUIManager.ShowUI(CLuaUIResources.ChunJie2023NianHuoWnd)
end
function LuaChunJie2023MainWnd:OnShengYanBtnClick()
    if self.m_Alert_ShenYan.activeSelf then
        self.m_Alert_ShenYan:SetActive(false)
        LuaActivityRedDotMgr:OnRedDotClicked(13)
    end
    CUIManager.ShowUI(CLuaUIResources.ChunJie2023ShengYanEnterWnd)
end
function LuaChunJie2023MainWnd:OnShouLieBtnClick()
    CUIManager.ShowUI(CLuaUIResources.ChunJie2023ShouLieEnterWnd)
end

function LuaChunJie2023MainWnd:OnHongBaoRainBtnClick()
    CLuaScheduleMgr:ShowScheduleInfo(ChunJie_Setting2023.GetData().HongBaoRainActivityID)
end
function LuaChunJie2023MainWnd:OnHuoShuYinHuaBtnClick()
    CLuaScheduleMgr:ShowScheduleInfo(ChunJie_Setting2023.GetData().HuoShuYinHuaActivityID)
end

function LuaChunJie2023MainWnd:OnHongBaoBtnClick()
    CUIManager.ShowUI(CLuaUIResources.ChunJie2023HongBaoCoverWnd)
end
function LuaChunJie2023MainWnd:OnSignInBtnClick()
    CWelfareMgr.OpenWelfareWnd(ChunJie_Setting2023.GetData().QianDaoTabName)
end
--@endregion UIEvent
