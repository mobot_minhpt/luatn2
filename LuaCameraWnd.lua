local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UITexture = import "UITexture"
local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local WebCamTexture = import "UnityEngine.WebCamTexture"
local Screen = import "UnityEngine.Screen"
local TextureWrapMode = import "UnityEngine.TextureWrapMode"
local CMainCamera = import "L10.Engine.CMainCamera"
local Application = import "UnityEngine.Application"
local File = import "System.IO.File"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local TweenAlpha = import "TweenAlpha"
local Texture2D = import "UnityEngine.Texture2D"
local TextureFormat = import "UnityEngine.TextureFormat"
local ScreenOrientation = import "UnityEngine.ScreenOrientation"
local LoadPicMgr = import "L10.Game.LoadPicMgr"
local Utility = import "L10.Engine.Utility"

LuaCameraWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaCameraWnd, "CamTexture", "CamTexture", UITexture)
RegistChildComponent(LuaCameraWnd, "CloseBtn", "CloseBtn", CButton)
RegistChildComponent(LuaCameraWnd, "TopTipLab", "TopTipLab", UILabel)
RegistChildComponent(LuaCameraWnd, "CenterTip", "CenterTip", GameObject)
RegistChildComponent(LuaCameraWnd, "CaptureBtn", "CaptureBtn", CButton)
RegistChildComponent(LuaCameraWnd, "SwitchBtn", "SwitchBtn", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaCameraWnd, "m_WebTex")
RegistClassMember(LuaCameraWnd, "m_TargetFPS")
RegistClassMember(LuaCameraWnd, "m_ResolutionCoef")
RegistClassMember(LuaCameraWnd, "m_NeedSave")
RegistClassMember(LuaCameraWnd, "m_CurCameraIndex")
RegistClassMember(LuaCameraWnd, "m_BlankTextureTween")
RegistClassMember(LuaCameraWnd, "m_FinishTick")
RegistClassMember(LuaCameraWnd, "m_Callback")
RegistClassMember(LuaCameraWnd, "m_IsFrontfaceCamera")
RegistClassMember(LuaCameraWnd, "m_DelayInitTick")
RegistClassMember(LuaCameraWnd, "m_FlipTick")
RegistClassMember(LuaCameraWnd, "m_CaptureSprite")

function LuaCameraWnd:Awake()
    self.m_BlankTextureTween= self.transform:Find("BlankTexture"):GetComponent(typeof(TweenAlpha))
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.CaptureBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:Capture()
    end)

    UIEventListener.Get(self.SwitchBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:SwitchCamera()
    end)
end

function LuaCameraWnd:Init()
    if WebCamTexture.devices.Length == 0 then
        CUIManager.CloseUI(CLuaUIResources.CameraWnd)
        return
    end
    self:InitCameraConfig()
    self:InitCameraTex(self.m_CurCameraIndex)
    UnRegisterTick(self.m_FlipTick)
    self.m_FlipTick = RegisterTick(function()
        self:TryFlipPhoto()
    end, 1000)
end

--@region UIEvent

--@endregion UIEvent

function LuaCameraWnd:OnEnable()
    CMainCamera.Inst:SetCameraEnableStatus(false, "use_device_camera", false)
    Screen.orientation = ScreenOrientation.LandscapeLeft
end

function LuaCameraWnd:OnDisable()
    CMainCamera.Inst:SetCameraEnableStatus(true, "use_device_camera", false)
    CommonDefs.AutoRotateToLandscape()
end

function LuaCameraWnd:OnTick()
    self:TryFlipPhoto()
end

function LuaCameraWnd:TryFlipPhoto()
    if self.m_FinishTick then
        return
    end

    if self.m_WebTex == nil then
        return
    end
    local webCamTexture = self.m_WebTex
    local currentCWNeeded = 0
    if self.m_IsFrontfaceCamera then
        currentCWNeeded = webCamTexture.videoRotationAngle
    else
        currentCWNeeded = -webCamTexture.videoRotationAngle
    end
    
    if webCamTexture.videoVerticallyMirrored then
        currentCWNeeded = currentCWNeeded + 180
    end
    
    self.CamTexture.transform.localEulerAngles = Vector3(0, 0, currentCWNeeded)
    local mirrored = self:XOR(webCamTexture.videoVerticallyMirrored, self.m_IsFrontfaceCamera)
    
    LuaUtils.SetLocalScale(self.CamTexture.transform, mirrored and -1 or 1, 1, 1)
end

function LuaCameraWnd:XOR(A, B)
    return (A and not B) or (not A and B)
end

function LuaCameraWnd:OnDestroy()
    if self.m_WebTex ~= nil then
        self.m_WebTex:Stop()
        self.m_WebTex = nil
    end

    UnRegisterTick(self.m_DelayInitTick)
    UnRegisterTick(self.m_FinishTick)
end

function LuaCameraWnd:InitCameraConfig()
    self.m_CurCameraIndex = LuaCameraMgr.m_DefaultCamIndex % WebCamTexture.devices.Length
    self.m_TargetFPS = LuaCameraMgr.m_TargetFPS
    self.m_ResolutionCoef = LuaCameraMgr.m_ResolutionCoef
    self.TopTipLab.text = LuaCameraMgr.m_TopTipMessage and g_MessageMgr:FormatMessage(LuaCameraMgr.m_TopTipMessage) or ""
    for i = 0, self.CenterTip.transform.childCount - 1 do
        local child = self.CenterTip.transform:GetChild(i)
        child.gameObject:SetActive(child.name == LuaCameraMgr.m_CenterTipName)
    end
    self.m_NeedSave = LuaCameraMgr.m_NeedSave
    self.m_Callback = LuaCameraMgr.m_Callback
end

function LuaCameraWnd:InitCameraTex(index)
    if self.m_FinishTick then
        return
    end

    local devices = WebCamTexture.devices
    if index > devices.Length then
        return
    end
    self.m_CurCameraIndex = index
    local device = devices[index]
    local deviceName = device.name
    self.m_WebTex = CreateFromClass(WebCamTexture, deviceName, Screen.width * self.m_ResolutionCoef, Screen.height * self.m_ResolutionCoef, self.m_TargetFPS)
    self.m_IsFrontfaceCamera = index == 1
    self.m_WebTex.wrapMode = TextureWrapMode.Repeat
    self.CamTexture.mainTexture = nil
    self.m_WebTex:Play()
    
    -- 延迟获取摄像头分辨率,否则可能获取失败
    UnRegisterTick(self.m_DelayInitTick)
    self.m_DelayInitTick = RegisterTickOnce(function()
        self:DelayInitCameraTex()
        self.m_DelayInitTick = nil
    end, 1000)
end

function LuaCameraWnd:DelayInitCameraTex()
    local width = self.m_WebTex.width
    local height = self.m_WebTex.height
    local clipWidth = Screen.width
    local clipHeight = Screen.height
    self.m_MinScale = math.max(math.max(clipWidth / width, clipHeight / height), 1)
    self.CamTexture.width = self.m_WebTex.width * self.m_MinScale
    self.CamTexture.height = self.m_WebTex.height * self.m_MinScale
    self.CamTexture.mainTexture = self.m_WebTex
    self:TryFlipPhoto()
end

function LuaCameraWnd:Capture()
    if self.m_FinishTick then
        return
    end

    local texture = self.m_WebTex
    if texture == nil then
        return
    end

    local tex = CreateFromClass(Texture2D, texture.width, texture.height, TextureFormat.RGB24, false)
    local bytes = texture:GetPixels()
    tex:SetPixels(bytes)
    tex:Apply()

    local webCamTexture = self.m_WebTex
    local currentCWNeeded = 0
    if self.m_IsFrontfaceCamera then
        currentCWNeeded = webCamTexture.videoRotationAngle
    else
        currentCWNeeded = -webCamTexture.videoRotationAngle
    end
    
    if webCamTexture.videoVerticallyMirrored then
        currentCWNeeded = currentCWNeeded + 180
    end
    local mirrored = self:XOR(webCamTexture.videoVerticallyMirrored, self.m_IsFrontfaceCamera)
    local isRotate180 = currentCWNeeded % 360 == 180
    local flipX = isRotate180 and (not mirrored) or mirrored
    local flipY = isRotate180
    -- 在 iOS Metal 中，调用 WebCamTexture.GetPixels32() 会撤消操作系统提供的网络摄像头纹理中的垂直翻转
    if CommonDefs.IsIOSPlatform()then
        flipX = not (isRotate180 and mirrored or (not mirrored))
        flipY = not flipY
    end

    tex = LoadPicMgr.FlipTexture(tex, flipX, flipY)
    self.CamTexture.mainTexture = tex
    LuaUtils.SetLocalScale(self.CamTexture.transform, 1, 1, 1)
    self.CamTexture.transform.localEulerAngles = Vector3(0, 0, 0)

    if self.m_WebTex ~= nil then
        self.m_WebTex:Stop()
        self.m_WebTex = nil
    end

    self.m_BlankTextureTween:ResetToBeginning()
    self.m_BlankTextureTween:PlayForward()
    self.m_FinishTick = RegisterTickOnce(function ()
        self:OnFinished(CommonDefs.EncodeToPNG(tex))
        self.m_FinishTick = nil
    end, 1000)
end

function LuaCameraWnd:SwitchCamera()
    if self.m_FinishTick or self.m_DelayInitTick then
        return
    end

    local devices = WebCamTexture.devices
    if devices.Length <= 1 then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("只有1个摄像头！"))
    end

    if self.m_WebTex ~= nil then
        self.m_WebTex:Stop()
        self.m_WebTex = nil
    end

    local newCameraIndex = (self.m_CurCameraIndex + 1) % 2
    self:InitCameraTex(newCameraIndex)
end

function LuaCameraWnd:OnFinished(bytes)
    if self.m_NeedSave then
        self:SaveImage(bytes)
    end

    if self.m_Callback then
        self.m_Callback(bytes)
    end

    CUIManager.CloseUI(CLuaUIResources.CameraWnd)
end

function LuaCameraWnd:SaveImage(textureBytes)
    local filename = Utility.persistentDataPath .. "/screenshot.jpg"
    if Application.platform == RuntimePlatform.WindowsPlayer or Application.platform == RuntimePlatform.WindowsEditor then
        local CWinUtility = import "L10.UI.CWinUtility"
        local fileName
        local default
        default, fileName = CWinUtility.Inst:GetSaveFileName("jpg")
        if default then
            File.WriteAllBytes(fileName, textureBytes)
            self:Share(fileName)
        end
    else
        if pcall(function() NativeTools.SaveImage(textureBytes) end) then
            if Application.platform == RuntimePlatform.Android then
                g_MessageMgr:ShowMessage("SAVE_PICTURE_SUCCEED")
            end
        end
    end
end