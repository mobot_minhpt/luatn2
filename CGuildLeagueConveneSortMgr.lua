-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuildLeagueConveneSortMgr = import "L10.Game.CGuildLeagueConveneSortMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local UInt64 = import "System.UInt64"
CGuildLeagueConveneSortMgr.m_CompareDefault_CS2LuaHook = function (info1, info2) 
    local flag = CGuildLeagueConveneSortMgr.m_GuildLeagueConveneSortFlags[ESortType_CGuildLeagueConveneSortMgr_lua.Default]

    if info1.PlayerId == info2.PlayerId then
        return 0
    end

    -- 1. 玩家本人在最前面
    if info1.PlayerId == CClientMainPlayer.Inst.BasicProp.Id then
        return - 1
    end
    if info2.PlayerId == CClientMainPlayer.Inst.BasicProp.Id then
        return 1
    end

    -- 3. 职位高的玩家在前面
    if info1.Title < info2.Title then
        return - 1
    elseif info1.Title > info2.Title then
        return 1
    else
        -- 4. 玩家id排序
        if info1.PlayerId < info2.PlayerId then
            return - 1
        elseif info1.PlayerId > info2.PlayerId then
            return 1
        else
            return 0
        end
    end
end
CGuildLeagueConveneSortMgr.m_CompareClsName_CS2LuaHook = function (info1, info2) 
    local flag = CGuildLeagueConveneSortMgr.m_GuildLeagueConveneSortFlags[ESortType_CGuildLeagueConveneSortMgr_lua.ClsName]

    -- 1. 根据玩家名字排序
    if CClientMainPlayer.Inst ~= nil then
        if info1.PlayerId == CClientMainPlayer.Inst.BasicProp.Id then
            return - 1
        end
        if info2.PlayerId == CClientMainPlayer.Inst.BasicProp.Id then
            return 1
        end
    end
    if NumberCompareTo(info1.Class, info2.Class) < 0 then
        return - flag
    elseif NumberCompareTo(info1.Class, info2.Class) > 0 then
        return flag
    end

    if CommonDefs.StringCompareTo(info1.Name, info2.Name) < 0 then
        return - flag
    elseif CommonDefs.StringCompareTo(info1.Name, info2.Name) > 0 then
        return flag
    end

    -- 接下来按照默认排序
    return CGuildLeagueConveneSortMgr.CompareDefault(info1, info2)
end
CGuildLeagueConveneSortMgr.m_CompareLevel_CS2LuaHook = function (info1, info2) 
    local flag = CGuildLeagueConveneSortMgr.m_GuildLeagueConveneSortFlags[ESortType_CGuildLeagueConveneSortMgr_lua.Level]
    if CClientMainPlayer.Inst ~= nil then
        if info1.PlayerId == CClientMainPlayer.Inst.BasicProp.Id then
            return - 1
        end
        if info2.PlayerId == CClientMainPlayer.Inst.BasicProp.Id then
            return 1
        end
    end
    if NumberCompareTo(info1.Level, info2.Level) < 0 then
        return flag
    elseif NumberCompareTo(info1.Level, info2.Level) > 0 then
        return - flag
    end

    return CGuildLeagueConveneSortMgr.CompareDefault(info1, info2)
end
CGuildLeagueConveneSortMgr.m_CompareTitle_CS2LuaHook = function (info1, info2) 
    local flag = CGuildLeagueConveneSortMgr.m_GuildLeagueConveneSortFlags[ESortType_CGuildLeagueConveneSortMgr_lua.Title]
    if CClientMainPlayer.Inst ~= nil then
        if info1.PlayerId == CClientMainPlayer.Inst.BasicProp.Id then
            return - 1
        end
        if info2.PlayerId == CClientMainPlayer.Inst.BasicProp.Id then
            return 1
        end
    end
    if NumberCompareTo(info1.Title, info2.Title) < 0 then
        return - flag
    elseif NumberCompareTo(info1.Title, info2.Title) > 0 then
        return flag
    end

    return CGuildLeagueConveneSortMgr.CompareDefault(info1, info2)
end
CGuildLeagueConveneSortMgr.m_CompareXiuwei_CS2LuaHook = function (info1, info2) 
    local flag = CGuildLeagueConveneSortMgr.m_GuildLeagueConveneSortFlags[ESortType_CGuildLeagueConveneSortMgr_lua.Xiuwei]
    if CClientMainPlayer.Inst ~= nil then
        if info1.PlayerId == CClientMainPlayer.Inst.BasicProp.Id then
            return - 1
        end
        if info2.PlayerId == CClientMainPlayer.Inst.BasicProp.Id then
            return 1
        end
    end
    if NumberCompareTo(info1.XiuWei, info2.XiuWei) < 0 then
        return flag
    elseif NumberCompareTo(info1.XiuWei, info2.XiuWei) > 0 then
        return - flag
    end

    return CGuildLeagueConveneSortMgr.CompareDefault(info1, info2)
end
CGuildLeagueConveneSortMgr.m_CompareXiulian_CS2LuaHook = function (info1, info2) 
    local flag = CGuildLeagueConveneSortMgr.m_GuildLeagueConveneSortFlags[ESortType_CGuildLeagueConveneSortMgr_lua.Xiulian]
    if CClientMainPlayer.Inst ~= nil then
        if info1.PlayerId == CClientMainPlayer.Inst.BasicProp.Id then
            return - 1
        end
        if info2.PlayerId == CClientMainPlayer.Inst.BasicProp.Id then
            return 1
        end
    end
    if NumberCompareTo(info1.XiuLian, info2.XiuLian) < 0 then
        return flag
    elseif NumberCompareTo(info1.XiuLian, info2.XiuLian) > 0 then
        return - flag
    end

    return CGuildLeagueConveneSortMgr.CompareDefault(info1, info2)
end
CGuildLeagueConveneSortMgr.m_CompareEquipScore_CS2LuaHook = function (info1, info2) 
    local flag = CGuildLeagueConveneSortMgr.m_GuildLeagueConveneSortFlags[ESortType_CGuildLeagueConveneSortMgr_lua.EquipScore]
    if CClientMainPlayer.Inst ~= nil then
        if info1.PlayerId == CClientMainPlayer.Inst.BasicProp.Id then
            return - 1
        end
        if info2.PlayerId == CClientMainPlayer.Inst.BasicProp.Id then
            return 1
        end
    end
    if NumberCompareTo(info1.EquipScore, info2.EquipScore) < 0 then
        return flag
    elseif NumberCompareTo(info1.EquipScore, info2.EquipScore) > 0 then
        return - flag
    end

    return CGuildLeagueConveneSortMgr.CompareDefault(info1, info2)
end
CGuildLeagueConveneSortMgr.m_CompareConvene_CS2LuaHook = function (info1, info2) 
    local flag = CGuildLeagueConveneSortMgr.m_GuildLeagueConveneSortFlags[ESortType_CGuildLeagueConveneSortMgr_lua.EquipScore]
    if CClientMainPlayer.Inst ~= nil then
        if info1.PlayerId == CClientMainPlayer.Inst.BasicProp.Id then
            return - 1
        end
        if info2.PlayerId == CClientMainPlayer.Inst.BasicProp.Id then
            return 1
        end
    end
    if CommonDefs.ListContains(CGuildLeagueConveneSortMgr.convenedPlayerIds, typeof(UInt64), info1.PlayerId) and not CommonDefs.ListContains(CGuildLeagueConveneSortMgr.convenedPlayerIds, typeof(UInt64), info2.PlayerId) then
        return - flag
    elseif not CommonDefs.ListContains(CGuildLeagueConveneSortMgr.convenedPlayerIds, typeof(UInt64), info1.PlayerId) and CommonDefs.ListContains(CGuildLeagueConveneSortMgr.convenedPlayerIds, typeof(UInt64), info2.PlayerId) then
        return flag
    end

    return CGuildLeagueConveneSortMgr.CompareDefault(info1, info2)
end
