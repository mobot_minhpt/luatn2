local QnTableView = import "L10.UI.QnTableView"
local QnTabButton = import "L10.UI.QnTabButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UILabel = import "UILabel"
local Vector3 = import "UnityEngine.Vector3"
local Vector4 = import "UnityEngine.Vector4"
local CButton = import "L10.UI.CButton"
local QnButton = import "L10.UI.QnButton"
local UITabBar = import "L10.UI.UITabBar"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local MessageMgr = import "L10.Game.MessageMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local StringBuilder = import "System.Text.StringBuilder"
local CCommonItem = import "L10.Game.CCommonItem"
local CItemMgr = import "L10.Game.CItemMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CPopupMenuInfoMgrAlignType = import  "L10.UI.CPopupMenuInfoMgr+AlignType"
local CChatMgr = import "L10.Game.CChatMgr"
local SendChatMsgResult = import "L10.Game.SendChatMsgResult"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
LuaPropertyGuideDetailWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPropertyGuideDetailWnd, "Header", "Header", GameObject)
RegistChildComponent(LuaPropertyGuideDetailWnd, "TabScollView", "TabScollView", CUIRestrictScrollView)
RegistChildComponent(LuaPropertyGuideDetailWnd, "TabTemplate", "TabTemplate", QnTabButton)
RegistChildComponent(LuaPropertyGuideDetailWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaPropertyGuideDetailWnd, "TableBody", "TableBody", GameObject)
RegistChildComponent(LuaPropertyGuideDetailWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaPropertyGuideDetailWnd, "FirstTitleTemplate", "FirstTitleTemplate", GameObject)
RegistChildComponent(LuaPropertyGuideDetailWnd, "SecondTitleTemplate", "SecondTitleTemplate", GameObject)
RegistChildComponent(LuaPropertyGuideDetailWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaPropertyGuideDetailWnd, "FengHuaBtn", "FengHuaBtn", CButton)
RegistChildComponent(LuaPropertyGuideDetailWnd, "EnhanceButton", "EnhanceButton", CButton)
RegistChildComponent(LuaPropertyGuideDetailWnd, "ShareButton", "ShareButton", QnButton)
RegistChildComponent(LuaPropertyGuideDetailWnd, "SwitchButton", "SwitchButton", QnButton)
RegistChildComponent(LuaPropertyGuideDetailWnd, "TabTable", "TabTable", UITabBar)

--@endregion RegistChildComponent end
RegistClassMember(LuaPropertyGuideDetailWnd, "m_PropertyID")
RegistClassMember(LuaPropertyGuideDetailWnd, "m_PropertyData")
RegistClassMember(LuaPropertyGuideDetailWnd, "m_TabPropertyIDList")
RegistClassMember(LuaPropertyGuideDetailWnd, "m_TabIndex")
RegistClassMember(LuaPropertyGuideDetailWnd, "m_CurTabIndex")
RegistClassMember(LuaPropertyGuideDetailWnd, "m_TabViewList")
RegistClassMember(LuaPropertyGuideDetailWnd, "m_HasInitTab")
RegistClassMember(LuaPropertyGuideDetailWnd, "m_SelectInfo")
RegistClassMember(LuaPropertyGuideDetailWnd, "m_Title2Paragraph")
RegistClassMember(LuaPropertyGuideDetailWnd, "m_MainPropertyValue")
RegistClassMember(LuaPropertyGuideDetailWnd, "m_ShareStatus")
RegistClassMember(LuaPropertyGuideDetailWnd, "m_SendChatShowMsg")
RegistClassMember(LuaPropertyGuideDetailWnd, "m_ShowChatMsg")
RegistClassMember(LuaPropertyGuideDetailWnd, "m_ListBtn")
function LuaPropertyGuideDetailWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.FengHuaBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFengHuaBtnClick()
	end)


	
	UIEventListener.Get(self.EnhanceButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnhanceButtonClick()
	end)


	
	UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick(go)
	end)

	UIEventListener.Get(self.SwitchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSwitchButtonClick()
	end)

    --@endregion EventBind end
    self.m_PropertyData = nil
    self.m_TabPropertyIDList = nil
    self.m_HasInitTab = false
    self.m_TabIndex = -1
    self.m_CurTabIndex = -1
    self.m_TabViewList = nil
    self.TabTable.enabled = false
    self.TabTemplate.gameObject:SetActive(false)
    
    
    self.m_MainPropertyValue = 0
    self.m_ShareStatus = false
    self.m_ShowChatMsg = false
    self.m_SendChatShowMsg = DelegateFactory.Action(
        function()
            if LuaPlayerPropertyMgr.ShareInfo and LuaPlayerPropertyMgr.ShareInfo.isShared and CServerTimeMgr.Inst.timeStamp - LuaPlayerPropertyMgr.ShareInfo.time < 2 then
                g_MessageMgr:ShowMessage("PropGuide_Online_Share_Success")
            end
            LuaPlayerPropertyMgr.ShareInfo = nil
        end)
end

function LuaPropertyGuideDetailWnd:Init()
    if LuaPlayerPropertyMgr.PropertyGuideID <= 0 then return end
    self.m_PropertyID = LuaPlayerPropertyMgr.PropertyGuideID
    local data = PropGuide_Property.GetData(self.m_PropertyID)
    if not data then return end
    self.m_Title2Paragraph = {}
    self.m_ListBtn = {}
    self.m_SelectInfo = {}
    self.m_PropertyData = data
    --if not self.m_HasInitTab then
        self:InitTable()
        --self.m_HasInitTab = true
    --end
    self:UpdateShareBtn()
    self:UpdateValue()
    self.transform:Find("Wnd_Bg_Secondary_1/TitleLabel"):GetComponent(typeof(UILabel)).text = PropGuide_Setting.GetData().PropertyGuideName
    self.TableBody:GetComponent(typeof(UIScrollView)):ResetPosition()
    if CClientMainPlayer.Inst and LuaPlayerPropertyMgr.PropertyGuide then
        if CClientMainPlayer.Inst.Level >= LuaPlayerPropertyMgr:GetPropertyGuideLevel() then
            CLuaGuideMgr.TryTriggerGuide(235)
        end
    end
    for k,v in pairs(self.m_ListBtn) do
        self:OnBtnClick(v,k)
    end
end
function LuaPropertyGuideDetailWnd:InitTable()
    if not self.m_PropertyData then return end
    local childTab = self.m_PropertyData.ChildTab
    self.m_TabPropertyIDList = {}
    Extensions.RemoveAllChildren(self.TabTable.transform)
    self.Header:SetActive(false)

    if not childTab then 
        local table = self.TableBody.transform:Find("Table").transform
        local tableBodyPanel = self.TableBody.transform:GetComponent(typeof(UIPanel))
        tableBodyPanel.baseClipRegion = Vector4(-10,-203,1366,710)
        table.localPosition = Vector3(-691,18,0)
        self.transform:Find("TableView/Sprite"):GetComponent(typeof(UISprite)):ResetAndUpdateAnchors()
        self.TabTable.OnTabChange = nil
        self.m_CurTabIndex = -1
        return
    end
    local count = 0
    local tabIndex = 0

    for i = 0 ,childTab.Length - 1 do
        local data = PropGuide_Property.GetData(childTab[i])
        if data then
            local Tab = CUICommonDef.AddChild(self.TabTable.gameObject,self.TabTemplate.gameObject)
            Tab.transform:Find("Label"):GetComponent(typeof(UILabel)).text = data.Name
            Tab.gameObject:SetActive(true)
            self.m_TabPropertyIDList[count] = childTab[i]
            if childTab[i] == self.m_PropertyID then tabIndex = count end
            count = count + 1
        end
    end
    self.m_TabIndex = tabIndex
    self.TabTable.transform:GetComponent(typeof(UITable)):Reposition()
    if count ~= 0 then
        self.Header:SetActive(true)
        self.TabTable.enabled = true
        self.TabTable:ReloadTabButtons()
        local headHeight = self.TabTemplate.transform:GetComponent(typeof(UIWidget)).height - 10
        local tableBodyPanel = self.TableBody.transform:GetComponent(typeof(UIPanel))

        tableBodyPanel.baseClipRegion = Vector4(-10,-240,1366,636)
        local table = self.TableBody.transform:Find("Table").transform
        table.localPosition = Vector3(-691,-56,0)
        if not self.TabTable.OnTabChange then
            self.TabTable.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
                self:OnTabChange(index)
            end)
        end
    end

    self.m_CurTabIndex = tabIndex
    self.transform:Find("TableView/Sprite"):GetComponent(typeof(UISprite)):ResetAndUpdateAnchors()

end

function LuaPropertyGuideDetailWnd:OnTabChange(index)
    if self.m_TabIndex == index then 
        return
    end
    self.m_CurTabIndex = index
    self.TabTable:ChangeTab(self.m_TabIndex,true)
    local propID = self.m_TabPropertyIDList[index]
    -- 请求服务器数据
    LuaPlayerPropertyMgr:ShowPlayerPropertyDetailWnd(propID)
end
function LuaPropertyGuideDetailWnd:UpdateValue()
    self.m_PropertyID = LuaPlayerPropertyMgr.PropertyGuideID
    self.m_PropertyData = LuaPlayerPropertyMgr.PlayerPropData.PropData--PropGuide_Property.GetData(self.m_PropertyID)--LuaPlayerPropertyMgr.PropData
    if not self.m_PropertyData or not CClientMainPlayer.Inst or not LuaPlayerPropertyMgr.PlayerPropData then return end
    if self.m_CurTabIndex ~= -1 then
        self.TabTable:ChangeTab(self.m_CurTabIndex,true)
        self.m_TabIndex = self.m_CurTabIndex
    end
    self.m_Title2Paragraph = {}
    self.m_SelectInfo = {}
    local t = {}
    local val = 0
    val = LuaPlayerPropertyMgr.PlayerPropData.total.all.main
    self.m_MainPropertyValue = LuaPlayerPropertyMgr.PlayerPropData.total.all.main
    table.insert(t,{    -- 一级标题
        type = 1,
        Name = self.m_PropertyData.Name,
        Des = MessageMgr.Inst:FormatMessage(self.m_PropertyData.Desc,{}),
        Value = val,
    })
    if self.m_PropertyData.BaseWords then   -- 基础加成
        local list = self.m_PropertyData.BaseWords
        table.insert(t,{    -- 二级标题
            type = 2,
            Name = LocalString.GetString("基础加成"),
            Value = self:GetTitleValue(1),
            index = 1,
        })
        self:GetThirdTitleTable(t,1,list)
    end
    if self.m_PropertyData.PrecentWords then    -- 百分比加成
        local list = self.m_PropertyData.PrecentWords
        table.insert(t,{    -- 二级标题
            type = 2,
            Name = LocalString.GetString("百分比加成"),
            Value = self:GetTitleValue(2),
            index = 2,
        })
        self:GetThirdTitleTable(t,2,list)
    end
    if self.m_PropertyData.ExtraWords then  -- 额外加成
        local list = self.m_PropertyData.ExtraWords
        table.insert(t,{    -- 二级标题
            type = 2,
            Name = LocalString.GetString("额外加成"),
            Value = self:GetTitleValue(3),
            index = 3,
        })
        self:GetThirdTitleTable(t,3,list)
    end
    if self.m_PropertyData.OtherWords then  -- 其他加成
        local list = self.m_PropertyData.OtherWords
        table.insert(t,{    -- 二级标题
            type = 2,
            Name = LocalString.GetString("其他加成"),
            Value = self:GetTitleValue(4),
            index = 4,
        })
        self:GetThirdTitleTable(t,4,list)
    end
    table.insert(t,{    -- 结尾留空
        type = 4,
    })
    self.m_TabViewList = t
    self:UpdateTable()
end
function LuaPropertyGuideDetailWnd:GetThirdTitleTable(wordtable,index,list)
    local wordlist = {}
    for k,v in pairs(list) do
        if v then
            table.insert(wordlist,{ -- 三级级标题
                type = 3,
                Value = self:GetItemValue(index,k),
                index = index,
                wordId = k,
            })
        end
    end
    table.sort(wordlist,function(a,b) 
        local aval = math.floor(math.abs(a.Value.val))
        local bval = math.floor(math.abs(b.Value.val))
        if aval > 0 and bval <= 0 then return true
        else return false end
    end)
    for k,v in pairs(wordlist) do
        table.insert(wordtable,v)
    end
end

-- 每级标题的数值总和,解析服务器发来的数据
-- 1 基础加成 2 百分比加成 3 额外加成 4 其他加成
function LuaPropertyGuideDetailWnd:GetTitleValue(titleID)
    local res = 0
    self.m_SelectInfo[titleID] = true
    if titleID == 1 then
        res = LuaPlayerPropertyMgr.PlayerPropData.total.base.main
    elseif titleID == 2 then
        res = LuaPlayerPropertyMgr.PlayerPropData.total.precent.main
    elseif titleID == 3 then
        res = LuaPlayerPropertyMgr.PlayerPropData.total.extra.main
    elseif titleID == 4 then
        res = LuaPlayerPropertyMgr.PlayerPropData.total.other.main
    end

    return res
end
-- 每级标题下每隔三级标题的内容
function LuaPropertyGuideDetailWnd:GetItemValue(titleID,wordID)
    local wordData = nil
    if wordID >= 1000 then
        wordData = PropGuide_PropertyWords.GetData(wordID - titleID * 1000)
    else
        wordData = PropGuide_PropertyWords.GetData(wordID)
    end
    local t = {val = 0,desc = ""}
    local equipList = {}
    if not wordData then return t end

    local wordName = wordData.Desc
    local specialID = PropGuide_Setting.GetData().SpecialPropertyWordID
    if wordID == specialID then
        wordName =  PropGuide_Property.GetData(self.m_PropertyID).SpecialWordDes
    end
    if LuaPlayerPropertyMgr.PlayerPropData.propWords[wordID] then
        t.val = LuaPlayerPropertyMgr.PlayerPropData.propWords[wordID].val
        if titleID == 2 then t.val = t.val * 100 end
        equipList = LuaPlayerPropertyMgr.PlayerPropData.propWords[wordID].equip
    end
    t.desc,t.val = self:BuildString(wordName,t.val,wordData.wndName,equipList,titleID == 2)

    return t
end

function LuaPropertyGuideDetailWnd:BuildString(worldName,val,wndName,equipList,isPrecent)
    local stringbuilder = NewStringBuilderWraper(StringBuilder)
    stringbuilder:Append(worldName)
    local absVal = math.floor(math.abs(val))
    local resVal = absVal
    if isPrecent then 
        absVal = math.abs(val) - math.abs(val)%0.01
        if math.abs(absVal - 0) < 0.01 then resVal = 0 else resVal = 10 end
    end
    if val < 0 and absVal ~= 0 then 
        stringbuilder:Append(" - ")
    else
        stringbuilder:Append(" + ")
    end

    local value = 0
    if isPrecent then value = tostring(absVal) .. "%" 
    else value = tostring(absVal) end
    stringbuilder:Append(value)

    if wndName and not System.String.IsNullOrEmpty(wndName) then
        stringbuilder:Append(CUICommonDef.TranslateToNGUIText(wndName))
    end

    if equipList and #equipList > 0 then
        for i = 1,#equipList do
            local item = CItemMgr.Inst:GetById(equipList[i])
            local sign = " "
            local existingLinks = CreateFromClass(MakeGenericClass(Dictionary, Int32, CChatInputLink))
            if i ~= 1 then sign = LocalString.GetString("、") end
            if item then
                local text = CChatInputLink.AppendItemLink(sign,item,existingLinks)
                local itemLink = CChatInputLink.Encapsulate(text, existingLinks)
                stringbuilder:Append(CUICommonDef.TranslateToNGUIText(itemLink))
            end
        end
    end
    return stringbuilder:ToString(),resVal
end
function LuaPropertyGuideDetailWnd:UpdateTable()
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create2(
        function()
            if self.m_TabViewList then
                return #self.m_TabViewList
            end
            return 0
        end,
        function(view,row)
            local item = nil
            local index = 0
            index = self.m_TabViewList[row+1].type - 1
            item = view:GetFromPool(index)
            self:InitItemGo(item,self.m_TabViewList[row+1])
            return item
        end
    )
    self.TableView:ReloadData(false,true)

end
function LuaPropertyGuideDetailWnd:InitItemGo(item,data)
    if not item then return end
    item.gameObject:SetActive(true)
    if data.type == 1 then
        item.gameObject.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = data.Name
        item.gameObject.transform:Find("ValueLabel"):GetComponent(typeof(UILabel)).text = self:GetStringVal(data.Value)
        item.gameObject.transform:Find("DesLabel"):GetComponent(typeof(UILabel)).text = data.Des
        item.gameObject.transform:Find("ValueLabel"):GetComponent(typeof(UILabel)):ResetAndUpdateAnchors()
    elseif data.type == 2 then
        item.gameObject.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = data.Name
        if data.Value >= 0 then
            item.gameObject.transform:Find("ValueLabel"):GetComponent(typeof(UILabel)).text = "+" .. tostring(math.floor(math.abs(data.Value)))
        else
            item.gameObject.transform:Find("ValueLabel"):GetComponent(typeof(UILabel)).text = "-" .. tostring(math.floor(math.abs(data.Value)))
        end
        item.gameObject.transform:Find("ValueLabel"):GetComponent(typeof(UILabel)):ResetAndUpdateAnchors()
        local btn = item.gameObject.transform:Find("Btn")
        btn.transform.localEulerAngles = Vector3(0, 0, 0)
        UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnBtnClick(go,data.index)
        end)
        self.m_ListBtn[data.index] = btn.gameObject
    elseif data.type == 3 then
        local label = item.gameObject.transform:Find("Label"):GetComponent(typeof(UILabel))
        label.text = data.Value.desc
        if math.floor(math.abs(data.Value.val)) == 0 then
            item:GetComponent(typeof(UIWidget)).alpha = 0.3
        else
            item:GetComponent(typeof(UIWidget)).alpha = 1
        end
        if data.wordId == 26 and data.Value.val > 0 then
            item:GetComponent(typeof(UIWidget)).alpha = 1
        end
        UIEventListener.Get(label.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            local lablel = go:GetComponent(typeof(UILabel))
            local url = label:GetUrlAtPosition(UICamera.lastWorldPosition)

            if url then CChatLinkMgr.ProcessLinkClick(url,nil) end
        end)
        if data.index then
            if not self.m_Title2Paragraph[data.index] then self.m_Title2Paragraph[data.index] = {} end
            table.insert(self.m_Title2Paragraph[data.index],item)
        end
    end
end
function LuaPropertyGuideDetailWnd:GetStringVal(val)
    return tostring(LuaPlayerPropertyMgr:GetTotalValue(self.m_PropertyData.FightPropID,val))
end
function LuaPropertyGuideDetailWnd:UpdateShareBtn()
    if self.m_PropertyID ~= LuaPlayerPropertyMgr.PropertyGuideID then return end
    self.m_ShareStatus = LuaPlayerPropertyMgr.PlayerIsShareProp
    local sprite = self.SwitchButton.transform:GetComponent(typeof(UISprite))
    if self.m_ShareStatus then
        sprite.spriteName = "common_thumb_open"
    else
        sprite.spriteName = "common_thumb_close"
    end
end
--@region UIEvent

function LuaPropertyGuideDetailWnd:OnFengHuaBtnClick()
    -- 打开排行榜
    LuaPlayerPropertyMgr:QueryPropRank(self.m_PropertyID)
end

function LuaPropertyGuideDetailWnd:OnEnhanceButtonClick()
    -- 打开加强建议
    if not self.m_PropertyData.CapacityKeys then return end
    local capacityList = self.m_PropertyData.CapacityKeys
    local sub = LuaPlayerPropertyMgr:GetCapacityRecommendSubTable(capacityList)
    local capacityTable = {
        key = "Property",
        name = self.m_PropertyData.Name,
        ignore = true,
        sub = sub,
    }
    CLuaPlayerCapacityMgr.AddNewModuleDef(capacityTable)
    CLuaPlayerCapacityRecommendWnd.fromKey = "Property"
    CUIManager.ShowUI(CUIResources.PlayerCapacityRecommendWnd)
end

function LuaPropertyGuideDetailWnd:OnShareButtonClick(go)
    local data = PropGuide_Property.GetData(self.m_PropertyID)
    if not data then return end
    local tbl = {}
    local textFunc = function (channel) return SafeStringFormat3(self.m_PropertyData.ShareDesc,self.m_MainPropertyValue) end
    if not CommonDefs.IS_VN_CLIENT then
        local shareToMengDao =  PopupMenuItemData(LocalString.GetString("分享至梦岛"),
                DelegateFactory.Action_int(function (index)
                    -- 分享
                    CUICommonDef.CaptureScreenUIAndShare(CLuaUIResources.PropertyGuideDetailWnd,nil)
                end), false, nil)
        table.insert(tbl, shareToMengDao)
    end
    local shareTest = g_MessageMgr:FormatMessage("PropGuide_Online_Share_Msg", data.ShareDesc,data.Name,math.floor(self.m_MainPropertyValue),self.m_PropertyID)
    local shareToWorld = self:GetSharePopItem(LocalString.GetString("分享至世界"),shareTest,CChatMgr.CHANNEL_NAME_WORLD)
    table.insert(tbl, shareToWorld)

    local shareToGuild = self:GetSharePopItem(LocalString.GetString("分享至帮会"),shareTest,CChatMgr.CHANNEL_NAME_ORG)
    table.insert(tbl, shareToGuild)

    local shareToTeam = self:GetSharePopItem(LocalString.GetString("分享至队伍"),shareTest,CChatMgr.CHANNEL_NAME_TEAM)
    table.insert(tbl, shareToTeam)


    local array = Table2Array(tbl, MakeArrayClass(PopupMenuItemData))

    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, CPopupMenuInfoMgrAlignType.Top, 1, nil, 600, true, 320)
    
end

function LuaPropertyGuideDetailWnd:GetSharePopItem(name,shareString,channel)
    return PopupMenuItemData(name,
    DelegateFactory.Action_int(function (index)
        if channel == CChatMgr.CHANNEL_NAME_ORG then    -- 帮会
            if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.GuildId <= 0 then
                g_MessageMgr:ShowMessage("HEBAZI_NO_GUILD")
                return
            end
        elseif channel == CChatMgr.CHANNEL_NAME_TEAM then
            if CTeamMgr.Inst and not CTeamMgr.Inst:TeamExists() then
                g_MessageMgr:ShowMessage("HEBAZI_NO_TEAM")
                return
            end
        end
        if CClientMainPlayer.Inst and not CClientMainPlayer.Inst.HasFeiSheng and CClientMainPlayer.Inst.Level < 10 and channel == CChatMgr.CHANNEL_NAME_WORLD then
            g_MessageMgr:ShowMessage("CHAT_WORLD_CHANNEL_PLAYER_LEVEL_NOT_ENOUGH",10)
            return
        end
        self:OnShareFinished()
        if not self.m_ShareStatus  then
            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("PropGuide_Online_Share_Ask"), function()
                LuaPlayerPropertyMgr:SetShareInfo(shareString,channel,false)
                self:OnRequestShare()
            end, nil, nil, nil, false)
            return
        end
        local shareText = shareString
        if shareText==nil or shareText=="" then
                return
        end
        local result = CChatMgr.Inst:SendChatMsg(channel, shareText, true)

        if result == SendChatMsgResult.Success then
            --g_MessageMgr:ShowMessage("PropGuide_Online_Share_Success")
            LuaPlayerPropertyMgr:SetShareInfo(shareText,channel,true)
        end

    end), false, nil)
end

function LuaPropertyGuideDetailWnd:OnRequestShare()
    local data = PropGuide_Property.GetData(self.m_PropertyID)
    if not data then return end
    local type = CommonDefs.ConvertIntToEnum(typeof(EPlayerFightProp), data.FightPropID)
    local typeString = CommonDefs.Convert_ToString(type)
    LuaPlayerPropertyMgr.TemplatePropertyGuideID = self.m_PropertyID
    Gac2Gas.PlayerRequestSharePropComposition(typeString,1)
end

function LuaPropertyGuideDetailWnd:OnSwitchButtonClick()
    local data = PropGuide_Property.GetData(self.m_PropertyID)
    if not data then return end
    local type = CommonDefs.ConvertIntToEnum(typeof(EPlayerFightProp), data.FightPropID)
    local typeString = CommonDefs.Convert_ToString(type)
    if self.m_ShareStatus then
        -- 取消分享
        LuaPlayerPropertyMgr.TemplatePropertyGuideID = self.m_PropertyID
        Gac2Gas.PlayerCancelSharePropComposition(typeString)
        --Gac2Gas.PlayerQueryPropShareRecord(typeString) 
    else
        -- 请求分享
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("PropGuide_ShareProperty_Confim",data.Name), function()
            LuaPlayerPropertyMgr.TemplatePropertyGuideID = self.m_PropertyID
            Gac2Gas.PlayerRequestSharePropComposition(typeString,0)
            --Gac2Gas.PlayerQueryPropShareRecord(typeString) 
        end, nil, nil, nil, false)
    end
end

function LuaPropertyGuideDetailWnd:GetGuideGo(methodName)
    if methodName == "GetEnhanceBtn" then
        return self.EnhanceButton.gameObject
    elseif methodName == "GetRankBtn" then
        return self.FengHuaBtn.gameObject
    elseif methodName == "GetShareBtn" then
        return self.ShareButton.gameObject
    elseif methodName == "GetExpandBtn" then
        return self.m_ListBtn and self.m_ListBtn[1]
    end
end


--@endregion UIEvent
function LuaPropertyGuideDetailWnd:OnBtnClick(go,index)
    if not index or not go then return end
    
    local goList = self.m_Title2Paragraph[index]
    local curStatus =  not self.m_SelectInfo[index]
    go:GetComponent(typeof(UISprite)).flip = curStatus and  UIBasicSprite.Flip.Vertically or UIBasicSprite.Flip.Nothing
    --go.transform.localEulerAngles = curStatus and Vector3(0, 0, 0) or Vector3(0, 0, 180)
    if goList then
        for i = 1,#goList do
            goList[i].gameObject:SetActive(curStatus)
        end
    end
    self.m_SelectInfo[index] = curStatus
    self.TableBody.transform:Find("Table"):GetComponent(typeof(UITable)):Reposition()
    self.TableView.transform:Find("ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator)):Layout()
end

function LuaPropertyGuideDetailWnd:OnShareFinished()
    local data = PropGuide_Property.GetData(self.m_PropertyID)
    if not data then return end
    local type = CommonDefs.ConvertIntToEnum(typeof(EPlayerFightProp), data.FightPropID)
    local typeString = CommonDefs.Convert_ToString(type)
    Gac2Gas.PlayerSharedPropGuideInChat(typeString)
end

function LuaPropertyGuideDetailWnd:OnEnable()
    g_ScriptEvent:AddListener("OnPlayerQueryPropShareResult", self, "UpdateShareBtn")
    g_ScriptEvent:AddListener("PersonalSpaceShareFinished", self, "OnShareFinished")
    EventManager.AddListener(EnumEventType.OnSendChatMsgSuccess, self.m_SendChatShowMsg)
end

function LuaPropertyGuideDetailWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnPlayerQueryPropShareResult", self, "UpdateShareBtn")
    g_ScriptEvent:RemoveListener("PersonalSpaceShareFinished", self, "OnShareFinished")
    EventManager.RemoveListener(EnumEventType.OnSendChatMsgSuccess, self.m_SendChatShowMsg)
end