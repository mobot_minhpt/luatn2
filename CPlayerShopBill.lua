-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerShopBill = import "L10.UI.CPlayerShopBill"
local CPlayerShopBillData = import "L10.UI.CPlayerShopBillData"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local Object = import "System.Object"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
CPlayerShopBill.m_Init_CS2LuaHook = function (this)
    local time = CServerTimeMgr.Inst:GetZone8Time()

    CommonDefs.ListClear(this.m_DateTimeList)
    CommonDefs.ListClear(this.m_DateTimeActions)
    for i = 0, 4 do
        CommonDefs.ListAdd(this.m_DateTimeList, typeof(DateTime), time)
        CommonDefs.ListAdd(this.m_DateTimeActions, typeof(PopupMenuItemData), PopupMenuItemData(ToStringWrap(time, "yyyy-MM-dd"), MakeDelegateFromCSFunction(this.SelectQueryTime, MakeGenericClass(Action1, Int32), this), false, nil))
        time = time:AddDays(- 1)
    end
    this.m_TipLabel.text = System.String.Format(LocalString.GetString("玩家商店交易需要扣除[ff0000]{0}%[-]的交易税。"), math.ceil(CPlayerShopMgr.Transaction_Tax_Client * 100))
    this:QueryRecordsOnTime(this.m_DateTimeList[0])
end
CPlayerShopBill.m_SelectQueryTime_CS2LuaHook = function (this, row) 

    if this.m_DateTimeList ~= nil then
        local time = this.m_DateTimeList[row]
        this:QueryRecordsOnTime(time)
    end
end
CPlayerShopBill.m_OnQueryTradeRecordsResult_CS2LuaHook = function (this, timeformat, shopId, m_BillData)
    CommonDefs.ListClear(this.m_BillList)
    do
        local i = 0
        while i < m_BillData.Count do
            local billData = TypeAs(m_BillData[i], typeof(MakeGenericClass(List, Object)))
            CommonDefs.ListAdd(this.m_BillList, typeof(CPlayerShopBillData), CreateFromClass(CPlayerShopBillData, billData))
            i = i + 1
        end
    end
    this.m_TableView:ReloadData(false, false)
end
