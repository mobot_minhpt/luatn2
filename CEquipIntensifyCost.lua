-- Auto Generated!!
local AlignType = import "L10.UI.CTooltip+AlignType"
local CEquipIntensifyCost = import "L10.UI.CEquipIntensifyCost"
local CEquipmentIntensifyMgr = import "L10.Game.CEquipmentIntensifyMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EquipIntensify_Intensify = import "L10.Game.EquipIntensify_Intensify"
local EventDelegate = import "EventDelegate"
local EventManager = import "EventManager"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local Mall_LingYuMall = import "L10.Game.Mall_LingYuMall"
local PreSellContextType = import "L10.UI.PreSellContextType"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
CEquipIntensifyCost.m_Awake_CS2LuaHook = function (this) 
    if this.jadeNode ~= nil then
        this.jadeNode.gameObject:SetActive(false)
    end

    this.yuanbaoNode.gameObject:SetActive(false)

    this.autoCostToggle.value = CEquipmentIntensifyMgr.Inst.isAutoCostYuanBao
    CommonDefs.ListAdd(this.autoCostToggle.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        --暂时全局变量
        CEquipmentIntensifyMgr.Inst.isAutoCostYuanBao = this.autoCostToggle.value

        this:UpdateUI()
    end)))
    this.countUpdate.onChange = DelegateFactory.Action_int(function (val) 
        this:UpdateUI()
        this:UpdateCost()
        this:UpdateItemColor()
    end)
    if this.getGo ~= nil then
        UIEventListener.Get(this.getGo).onClick = DelegateFactory.VoidDelegate(function (p) 
            if this.m_NeedItemTemplateId > 0 then
                CItemAccessListMgr.Inst:ShowItemAccessInfo(this.m_NeedItemTemplateId, false, this.transform, AlignType.Right)
            end
        end)
    end
end
CEquipIntensifyCost.m_UpdateUI_CS2LuaHook = function (this) 
    if this.countUpdate.count < 5 then
        if this.getGo ~= nil then
            this.getGo:SetActive(true)
        end
    else
        if this.getGo ~= nil then
            this.getGo:SetActive(false)
        end
    end
    if not this.autoCostToggle.value then
        if this.jadeNode ~= nil then
            this.jadeNode.gameObject:SetActive(false)
        end
        this.yuanbaoNode.gameObject:SetActive(false)
        this.wushanshiNode.gameObject:SetActive(true)
    else
        if this.mInfo == nil then
            return
        end

        if this.countUpdate.count < 5 then
            if this.mInfo.Level > 10 then
                if this.jadeNode ~= nil then
                    this.jadeNode.gameObject:SetActive(true)
                end
                this.wushanshiNode.gameObject:SetActive(false)
                this.yuanbaoNode.gameObject:SetActive(false)
            else
                if this.jadeNode ~= nil then
                    this.jadeNode.gameObject:SetActive(false)
                end
                this.wushanshiNode.gameObject:SetActive(false)
                this.yuanbaoNode.gameObject:SetActive(true)
            end
        else
            if this.jadeNode ~= nil then
                this.jadeNode.gameObject:SetActive(false)
            end
            this.yuanbaoNode.gameObject:SetActive(false)
            this.wushanshiNode.gameObject:SetActive(true)
        end
    end
end
CEquipIntensifyCost.m_OnPutInItemChanged_CS2LuaHook = function (this, currentValue) 
    local info = CEquipmentIntensifyMgr.Inst.SelectedIntensifyDetail
    if info == nil then
        return
    end
    local config = EquipIntensify_Intensify.GetData(info.Level)
    --if (info.addValue > 0 && PutItemButton.GetValue() < config.AgainItemLimit)
    --{
    --    List<object> datas = new List<object>() { config.AgainItemLimit };
    --    MsgDisplayerHolder.Inst.DisplayMessage("INTENSIFY_AGAIN_NEED_ITEM_COUNT", datas);
    --    PutItemButton.SetValue(config.AgainItemLimit);
    --}

    --yuanBaoCost.costYuanBao = (ulong)((currentValue - countUpdate.count) * mPrice);
    this:UpdateCost()
    --UpdateSuccessRatio();

    this:UpdateItemColor()
end
CEquipIntensifyCost.m_UpdateCost_CS2LuaHook = function (this) 
    local config = EquipIntensify_Intensify.GetData(this.mInfo.Level)
    if this.mInfo.Level < 11 then
        local count = 5 - this.countUpdate.count
        count = count > 0 and count or 0
        this.yuanBaoCost.costYuanBao = (count * this.mPrice)
    else
        local costJade = Mall_LingYuMall.GetData(config.ItemId)
        if this.jadeCost ~= nil then
            local count = 5 - this.countUpdate.count
            count = count > 0 and count or 0
            this.jadeCost:SetCostJade(config.ItemId, (costJade.Jade * count))
            --按5个来扣
        end
    end
end
CEquipIntensifyCost.m_UpdateItemColor_CS2LuaHook = function (this) 
    if this.mInfo.Level > 10 then
        if 5 > this.countUpdate.count then
            this.countUpdate.format = "[c][ff0000]{0}[-]/5[/c]"
        else
            this.countUpdate.format = "[c][00ff00]{0}[-]/5[/c]"
        end
    else
        if 5 > this.countUpdate.count then
            this.countUpdate.format = "[c][ff0000]{0}[-]/5[/c]"
        else
            this.countUpdate.format = "[c][00ff00]{0}[-]/5[/c]"
        end
    end
end
CEquipIntensifyCost.m_SetInfo_CS2LuaHook = function (this, m_Info) 
    this.mInfo = m_Info
    if this.titleLabel ~= nil then
        this.titleLabel.text = System.String.Format(LocalString.GetString("{0}级强化"), m_Info.Level)
    end
    --CIntensifyDetailInfo m_Info = CEquipmentIntensifyMgr.Inst.SelectedIntensifyDetail;
    local config = EquipIntensify_Intensify.GetData(m_Info.Level)
    if config == nil then
        return
    end
    this.m_NeedItemTemplateId = config.ItemId
    local template = Item_Item.GetData(this.m_NeedItemTemplateId)
    this.nameLabel.text = template.Name

    this.icon:LoadMaterial(template.Icon)
    this.countUpdate.templateId = config.ItemId

    this.countUpdate.format = "{0}/5"

    this.countUpdate:UpdateCount()

    --1到10级 显示
    if m_Info.Level < 11 then
        if this.autoCostLabel ~= nil then
            this.autoCostLabel.text = LocalString.GetString("数量不足自动消耗元宝")
        end
    else
        if this.autoCostLabel ~= nil then
            this.autoCostLabel.text = LocalString.GetString("数量不足自动消耗灵玉")
        end

        local costJade = Mall_LingYuMall.GetData(config.ItemId)
        if this.jadeCost ~= nil then
            this.jadeCost:SetCostJade(config.ItemId, (costJade.Jade * (5 - this.countUpdate.count)))
        end
        --按5个来扣
    end

    this:UpdateCost()

    --UpdateSuccessRatio();

    this:UpdateItemColor()

    this:RequestPrice()
end
CEquipIntensifyCost.m_OnEnable_CS2LuaHook = function (this) 
    this:RequestPrice()

    --每隔15秒请求一次
    this:InvokeRepeating("RequestPrice", 15, 15)

    EventManager.AddListenerInternal(EnumEventType.PreSellMarketItemDone, MakeDelegateFromCSFunction(this.OnPreSellDone, MakeGenericClass(Action3, UInt32, UInt32, UInt32), this))
end
CEquipIntensifyCost.m_OnPreSellDone_CS2LuaHook = function (this, itemId, maxCount, price) 
    if CUICommonDef.PreSellContext ~= PreSellContextType.Equip then
        return
    end
    this.mPrice = price
    local count = this.wushanshiCount - this.countUpdate.count
    count = count > 0 and count or 0
    this.yuanBaoCost.costYuanBao = (count * price)
end
