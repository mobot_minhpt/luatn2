local CTeamMgr = import "L10.Game.CTeamMgr"
local Profession = import "L10.Game.Profession"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"

LuaShuJiaHaiZhanResultWnd = class()
LuaShuJiaHaiZhanResultWnd.s_Time = 0
LuaShuJiaHaiZhanResultWnd.s_GamePlayId = 0
LuaShuJiaHaiZhanResultWnd.s_IsHard = 0

RegistClassMember(LuaShuJiaHaiZhanResultWnd, "Logo")
--RegistClassMember(LuaShuJiaHaiZhanResultWnd, "FightBtn")
RegistClassMember(LuaShuJiaHaiZhanResultWnd, "ShareBtn")
RegistClassMember(LuaShuJiaHaiZhanResultWnd, "CloseBtn")
RegistClassMember(LuaShuJiaHaiZhanResultWnd, "TimeLb")
RegistClassMember(LuaShuJiaHaiZhanResultWnd, "HintLb")

function LuaShuJiaHaiZhanResultWnd:Awake()
    for i = 0, self.transform.childCount - 1 do 
        local child = self.transform:GetChild(i)
        LuaUtils.SetLocalPositionY(child, child.localPosition.y - 40)
    end
    self.Logo = self.transform:Find("Other/Logo").gameObject
    self.FightBtn = self.transform:Find("Other/FightBtn").gameObject
    self.FightBtn:SetActive(false)
    self.ShareBtn = self.transform:Find("Info/ShareButton").gameObject
    self.CloseBtn = self.transform:Find("Other/CloseBtn").gameObject
    self.TimeLb = self.transform:Find("Info/Time"):GetComponent(typeof(UILabel))
    self.HintLb = self.transform:Find("Info/RewardHint"):GetComponent(typeof(UILabel))

    UIEventListener.Get(self.ShareBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self.ShareBtn:SetActive(false)
        self.CloseBtn:SetActive(false)
        self.Logo:SetActive(true)
        CUIManager.SetUITop(CLuaUIResources.ShuJiaHaiZhanResultWnd)
        CUICommonDef.CaptureScreen("screenshot", true, false, nil, DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            self.ShareBtn:SetActive(true)
            self.CloseBtn:SetActive(true)
            self.Logo:SetActive(false)
            CUIManager.ResetUITop(CLuaUIResources.ShuJiaHaiZhanResultWnd)
        end))
    end)
end

function LuaShuJiaHaiZhanResultWnd:Init()
    self.transform:Find("Bg/Win_bg/Background/shujiahaizhansignupwnd_bg/OceanFight_Icon").gameObject:SetActive(LuaShuJiaHaiZhanResultWnd.s_GamePlayId == NavalWar_Setting.GetData().PveSeaPlayId)
    self.transform:Find("Bg/Win_bg/Background/shujiahaizhansignupwnd_bg/FireTree_Icon").gameObject:SetActive(LuaShuJiaHaiZhanResultWnd.s_GamePlayId == NavalWar_Setting.GetData().WTSSEasyGameplayId)
    self.TimeLb.text = SafeStringFormat3(LocalString.GetString("%02d:%02d"), LuaShuJiaHaiZhanResultWnd.s_Time / 60, LuaShuJiaHaiZhanResultWnd.s_Time % 60)
    if not CClientMainPlayer.Inst then return end
    local role = self.transform:Find("Info/ModeLb/RoleItem1")
    role.gameObject:SetActive(true)
    role:Find("JobSp"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIcon(CClientMainPlayer.Inst.Class)
    role:Find("NameLb"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Name
    role:Find("LvLb"):GetComponent(typeof(UILabel)).text = "Lv."..CClientMainPlayer.Inst.Level
    local members = CTeamMgr.Inst.Members
    local idx = 1
    if members then
        for i = 2, 5 do 
            local role = self.transform:Find("Info/ModeLb/RoleItem"..i)
            if i > members.Count then role.gameObject:SetActive(false) 
            else
                role.gameObject:SetActive(true)
                local member = members[idx - 1]
                if member.m_MemberId == CClientMainPlayer.Inst.Id then
                    idx = idx + 1
                    member = members[idx - 1]
                end
                role:Find("JobSp"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIcon(member.m_MemberClass)
                role:Find("NameLb"):GetComponent(typeof(UILabel)).text = member.m_MemberName
                role:Find("LvLb"):GetComponent(typeof(UILabel)).text = "Lv."..member.m_MemberLevel
                idx = idx + 1
            end
        end
    end
    if LuaShuJiaHaiZhanResultWnd.s_IsHard == 1 then
        self.HintLb.text = LocalString.GetString("首通奖励已领取")
    else
        local txt = LocalString.GetString("本周奖励次数")
        if LuaShuJiaHaiZhanResultWnd.s_GamePlayId == NavalWar_Setting.GetData().PveSeaPlayId then
            txt = txt..CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eNavalWarPveEasyRewardTimes).."/"..NavalWar_Setting.GetData().PveRewardWeekLimit
        elseif LuaShuJiaHaiZhanResultWnd.s_GamePlayId == NavalWar_Setting.GetData().WTSSEasyGameplayId then
            txt = txt..CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eWTSSEasyTimes).."/"..NavalWar_Setting.GetData().WTSSWeekLimit
        end
        self.HintLb.text = txt
    end
end
