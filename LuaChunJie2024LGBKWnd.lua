local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemMgr = import "L10.Game.CItemMgr"
local CUITexture = import "L10.UI.CUITexture"
local QnAddSubAndInputButton=import "L10.UI.QnAddSubAndInputButton"
local CChatLinkMgr = import "CChatLinkMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CPayMgr = import "L10.Game.CPayMgr"

LuaChunJie2024LGBKWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2024LGBKWnd, "TimeLabel", UILabel)
RegistChildComponent(LuaChunJie2024LGBKWnd, "RuleBtn", GameObject)
RegistChildComponent(LuaChunJie2024LGBKWnd, "BuyBtn", GameObject)
RegistChildComponent(LuaChunJie2024LGBKWnd, "CouponRuleBtn", GameObject)

RegistChildComponent(LuaChunJie2024LGBKWnd, "ItemGrid", UIGrid)
RegistChildComponent(LuaChunJie2024LGBKWnd, "ItemTemplate", GameObject)
RegistChildComponent(LuaChunJie2024LGBKWnd, "RMBItemRoot", GameObject)
RegistChildComponent(LuaChunJie2024LGBKWnd, "JadeItemRoot", GameObject)
RegistChildComponent(LuaChunJie2024LGBKWnd, "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaChunJie2024LGBKWnd, "QnCostAndOwnMoney", GameObject)

RegistClassMember(LuaChunJie2024LGBKWnd, "m_ItemList")
RegistClassMember(LuaChunJie2024LGBKWnd, "m_ItemIndex")
--@endregion RegistChildComponent end

function LuaChunJie2024LGBKWnd:Awake()
    UIEventListener.Get(self.RuleBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRuleBtnClick()
    end)
    UIEventListener.Get(self.BuyBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnBuyBtnClick()
    end)
    UIEventListener.Get(self.CouponRuleBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCouponRuleBtnClick()
    end)
    self.QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function(val)
        self:OnValueChanged(val)
    end)
end

function LuaChunJie2024LGBKWnd:Init()
    self:InitItems()
end

function LuaChunJie2024LGBKWnd:InitItems()
    local mallList = CShopMallMgr.GetLingYuMallInfo(6, 0)
    self.ItemTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.ItemGrid.transform)
    self.m_ItemList = {}
    for i = 0, mallList.Count - 1 do
        local cell = NGUITools.AddChild(self.ItemGrid.gameObject, self.ItemTemplate)
        self:InitOneItem(cell, i + 1, mallList[i])
    end
    self.ItemGrid:Reposition()
    self:OnItemClick(1)
end

function LuaChunJie2024LGBKWnd:InitOneItem(cell, index, template)
    local itemInfo = CItemMgr.Inst:GetItemTemplate(template.ItemId)
    if itemInfo == nil then return true end
    local info = {}
    info.ShopMallTemlate = template
    info.Name = itemInfo.Name
    info.Description = itemInfo.Description
    cell.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(itemInfo.Icon)
    cell.transform:Find("NameLabel"):GetComponent("UILabel").text = itemInfo.Name

    -- 价格
    local rmbPrice = cell.transform:Find("RMBPrice")
    local jadePrice = cell.transform:Find("JadePrice")
    info.OriginalPrice = template.Discount ~= 0 and math.floor(template.Price / template.Discount * 10) or template.Price
    info.Highlight = cell.transform:Find("Highlight")

    if not System.String.IsNullOrEmpty(template.RmbPID) then -- RMB礼包
        rmbPrice.gameObject:SetActive(true)
        jadePrice.gameObject:SetActive(false)
        rmbPrice:GetComponent("UILabel").text = template.Price
    else  -- 灵玉礼包
        rmbPrice.gameObject:SetActive(false)
        jadePrice.gameObject:SetActive(true)
        self:InitJadePirceShow(jadePrice, template.Price, info.OriginalPrice, template.Discount)
    end
    UIEventListener.Get(cell.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnItemClick(index)
    end)
    table.insert(self.m_ItemList, info)
    cell.gameObject:SetActive(true)
end

function LuaChunJie2024LGBKWnd:InitJadePirceShow(jadePrice, price, originalPrice, discount)
    local noDiscount = jadePrice.transform:Find("NoDiscount")
    local withDiscount = jadePrice.transform:Find("WithDiscount")
    if discount == nil or discount == 0 then  -- 没有折扣
        noDiscount.gameObject:SetActive(true)
        withDiscount.gameObject:SetActive(false)
        noDiscount:GetComponent("UILabel").text = price
    else
        noDiscount.gameObject:SetActive(false)
        withDiscount.gameObject:SetActive(true)
        withDiscount:GetComponent("UILabel").text = price
        withDiscount.transform:Find("OriginalPrice"):GetComponent(typeof(UILabel)).text = tostring(originalPrice)

        local discountTip = withDiscount.transform:Find("DiscountTip")
        if discountTip then
            discountTip:GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%s折"), tostring(discount))
        end
    end
end

function LuaChunJie2024LGBKWnd:UpdateJadeSellRoot(info)
    self.RMBItemRoot:SetActive(false)
    self.JadeItemRoot:SetActive(true)
    self.JadeItemRoot.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = info.Name
    self.JadeItemRoot.transform:Find("Label"):GetComponent(typeof(UILabel)).text = CChatLinkMgr.TranslateToNGUIText(info.Description)
    self.QnIncreseAndDecreaseButton:SetValue(1, true)
end

function LuaChunJie2024LGBKWnd:UpdateRMBSellRoot(info)
    self.RMBItemRoot:SetActive(true)
    self.JadeItemRoot:SetActive(false)
    self.RMBItemRoot.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = info.Name
    self.RMBItemRoot.transform:Find("Label"):GetComponent(typeof(UILabel)).text = CChatLinkMgr.TranslateToNGUIText(info.Description)
    self.RMBItemRoot.transform:Find("RMBDiscountInfo/OriginalPrice"):GetComponent(typeof(UILabel)).text = info.OriginalPrice
    self.RMBItemRoot.transform:Find("RMBDiscountInfo/DiscountPrice"):GetComponent(typeof(UILabel)).text = info.ShopMallTemlate.Price
    self.RMBItemRoot.transform:Find("RMBDiscountInfo/DiscountTip"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%s折"), tostring(info.ShopMallTemlate.Discount))
end

--@region UIEvent
function LuaChunJie2024LGBKWnd:OnRuleBtnClick()
end

function LuaChunJie2024LGBKWnd:OnBuyBtnClick()
    if self.m_ItemIndex == nil then return end
    local info = self.m_ItemList[self.m_ItemIndex]
    if info == nil then return end
    local template = info.ShopMallTemlate
    if System.String.IsNullOrEmpty(template.RmbPID) then
        local count = self.QnIncreseAndDecreaseButton:GetValue()
        if count > 0 and CShopMallMgr.TryCheckEnoughJade(count * template.Price, template.ItemId) then
            -- CLinyuShopWndHolder:OnBuyButtonClick(template, count, this)
        end
    else
        if template.AvalibleCount == 0 then
            g_MessageMgr:ShowMessage("MALL_RMB_LIMIT")
        else
            CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(template.RmbPID), 0)
        end
    end
end

function LuaChunJie2024LGBKWnd:OnCouponRuleBtnClick()
end

function LuaChunJie2024LGBKWnd:OnItemClick(index)
    self.m_ItemIndex = index
    local info = self.m_ItemList[index]
    if info == nil then return end
    for i = 1, #self.m_ItemList do
        self.m_ItemList[i].Highlight.gameObject:SetActive(i == index)
    end
    if not System.String.IsNullOrEmpty(info.ShopMallTemlate.RmbPID) then
        self:UpdateRMBSellRoot(info)
    else
        self:UpdateJadeSellRoot(info)
    end
end

function LuaChunJie2024LGBKWnd:OnValueChanged(val)
    if self.m_ItemIndex == nil then return end
    local info = self.m_ItemList[self.m_ItemIndex]
    if info == nil then return end
    local cost = info.ShopMallTemlate.Price * val
    local originalCost = info.OriginalPrice * val
    self:InitJadePirceShow(self.QnCostAndOwnMoney, cost, originalCost, info.ShopMallTemlate.Discount)
end

--@endregion UIEvent