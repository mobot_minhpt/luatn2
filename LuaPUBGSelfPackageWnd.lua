require("3rdParty/ScriptEvent")
require("common/common_include")

local ChiJi_Item = import "L10.Game.ChiJi_Item"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EnumQualityType = import "L10.Game.EnumQualityType"
local UITabBar = import "L10.UI.UITabBar"
local CBodyEquipSlot = import "L10.UI.CBodyEquipSlot"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaPUBGSelfPackageWnd = class()

RegistChildComponent(LuaPUBGSelfPackageWnd, "PackageView", CCommonLuaScript)

RegistChildComponent(LuaPUBGSelfPackageWnd, "TabBar", UITabBar)
RegistChildComponent(LuaPUBGSelfPackageWnd, "PlayerEquipmentView", GameObject)
RegistChildComponent(LuaPUBGSelfPackageWnd, "PlayerPropertyView", GameObject)

RegistChildComponent(LuaPUBGSelfPackageWnd, "EquipmentScoreLabel", UILabel)
RegistChildComponent(LuaPUBGSelfPackageWnd, "QiangHuaLabel", UILabel)
RegistChildComponent(LuaPUBGSelfPackageWnd, "RightRingSlot", CBodyEquipSlot)
RegistChildComponent(LuaPUBGSelfPackageWnd, "HeadwearSlot", CBodyEquipSlot)
RegistChildComponent(LuaPUBGSelfPackageWnd, "AppearanceSlot", CBodyEquipSlot)
RegistChildComponent(LuaPUBGSelfPackageWnd, "NecklaceSlot", CBodyEquipSlot)
RegistChildComponent(LuaPUBGSelfPackageWnd, "LeftBraceletSlot", CBodyEquipSlot)
RegistChildComponent(LuaPUBGSelfPackageWnd, "RightBraceletSlot", CBodyEquipSlot)
RegistChildComponent(LuaPUBGSelfPackageWnd, "LeftRingSlot", CBodyEquipSlot)
RegistChildComponent(LuaPUBGSelfPackageWnd, "GlovesSlot", CBodyEquipSlot)
RegistChildComponent(LuaPUBGSelfPackageWnd, "BeltSlot", CBodyEquipSlot)
RegistChildComponent(LuaPUBGSelfPackageWnd, "ShoesSlot", CBodyEquipSlot)
RegistChildComponent(LuaPUBGSelfPackageWnd, "WeaponSlot", CBodyEquipSlot)
RegistChildComponent(LuaPUBGSelfPackageWnd, "ClothesSlot", CBodyEquipSlot)
RegistChildComponent(LuaPUBGSelfPackageWnd, "ShieldSlot", CBodyEquipSlot)

RegistChildComponent(LuaPUBGSelfPackageWnd, "HpFull", GameObject)
RegistChildComponent(LuaPUBGSelfPackageWnd, "Attack", GameObject)
RegistChildComponent(LuaPUBGSelfPackageWnd, "Hit", GameObject)
RegistChildComponent(LuaPUBGSelfPackageWnd, "Def", GameObject)
RegistChildComponent(LuaPUBGSelfPackageWnd, "Miss", GameObject)
RegistChildComponent(LuaPUBGSelfPackageWnd, "Hp", GameObject)

RegistClassMember(LuaPUBGSelfPackageWnd, "SelectTabIndex")
RegistClassMember(LuaPUBGSelfPackageWnd, "BodyEquipmentSlots")
RegistClassMember(LuaPUBGSelfPackageWnd, "EquipmentInfos")

function LuaPUBGSelfPackageWnd:Awake()
    self.BodyEquipmentSlots = {}
    self.EquipmentInfos = {}
    self.SelectTabIndex = 0
    self.EquipmentScoreLabel.text = ""
end

function LuaPUBGSelfPackageWnd:Init()
    self.PackageView:Init({})

    self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnTabChange(index)
    end)
    self.TabBar:ChangeTab(self.SelectTabIndex, true)
    self:OnTabChange(self.SelectTabIndex)

    self:InitBodySlotList()
    self:UpdatePlayerEquipment()
end

-------------------- TabBar --------------------

function LuaPUBGSelfPackageWnd:OnTabChange(index)
    self.SelectTabIndex = index
    if index == 0 then
        self.PlayerEquipmentView:SetActive(true)
        self.PlayerPropertyView:SetActive(false)
    elseif index == 1 then
        self.PlayerEquipmentView:SetActive(false)
        self.PlayerPropertyView:SetActive(true)
        self:UpdatePlayerProperties()
    end
end

-------------------- End of TabBar --------------------


-------------------------- PlayerEquipment --------------------------

function LuaPUBGSelfPackageWnd:InitBodySlotList()

    self.BodyEquipmentSlots = {}
    table.insert(self.BodyEquipmentSlots, self.WeaponSlot)
    table.insert(self.BodyEquipmentSlots, self.ShieldSlot)
    table.insert(self.BodyEquipmentSlots, self.HeadwearSlot)
    table.insert(self.BodyEquipmentSlots, self.ClothesSlot)
    table.insert(self.BodyEquipmentSlots, self.BeltSlot)
    table.insert(self.BodyEquipmentSlots, self.GlovesSlot)
    table.insert(self.BodyEquipmentSlots, self.ShoesSlot)
    table.insert(self.BodyEquipmentSlots, self.LeftRingSlot)
    table.insert(self.BodyEquipmentSlots, self.RightBraceletSlot)
    table.insert(self.BodyEquipmentSlots, self.NecklaceSlot)
end

function LuaPUBGSelfPackageWnd:UpdatePlayerEquipment()

    self.EquipmentInfos = LuaPUBGMgr.GetEquipmentInfos()

    for k, v in pairs(self.EquipmentInfos) do
        -- 去掉手镯与戒指
        if k ~= 8 and k ~= 9 then
            if v == 0 then
                self:UpdateBodyEquipSlot(0, self.BodyEquipmentSlots[k], k)
            else
                self:UpdateBodyEquipSlot(v.itemId, self.BodyEquipmentSlots[k], k)
            end
        end
        
    end

    self:UpdateQiangHuaLevel()
end

function LuaPUBGSelfPackageWnd:UpdateQiangHuaLevel()
    local level = LuaPUBGMgr.GetQiangHuaLevel()
    self.QiangHuaLabel.text = tostring(level)
end

--[[ public enum EnumBodyPosition
    {
        Undefined = 0,
        Weapon = 1,
        Shield = 2,
        Casque = 3,
        Armour = 4,
        Belt = 5,
        Gloves = 6,
        Shoes = 7,
        Ring = 8,
        Bracelet = 9,
        Necklace = 10,
}]]--
function LuaPUBGSelfPackageWnd:UpdateBodyEquipSlot(index, bodySlot, bodyPosition)
    local chijiItem = ChiJi_Item.GetData(index)
    if not chijiItem then
        bodySlot:Init(nil, CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), 0), false, nil, bodyPosition)
    else
        local equip = EquipmentTemplate_Equip.GetData(chijiItem.ItemId)
        if equip then
            bodySlot:Init(equip.BigIcon, CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), equip.Color), false, nil, bodyPosition)
            CommonDefs.AddOnClickListener(bodySlot.gameObject, DelegateFactory.Action_GameObject(function (go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(chijiItem.ItemId, false, nil, AlignType.ScreenLeft, 0, 0, 0, 0)
            end), false)
        end
    end

    
end
-------------------------- End of PlayerEquipment --------------------------


-------------------------- PlayerProperty --------------------------


function LuaPUBGSelfPackageWnd:UpdatePlayerProperties()
    if not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.FightProp then return end
    self:SetProperty(self.HpFull, CClientMainPlayer.Inst.FightProp:GetParam(EPlayerFightProp.PlayHpFull))
    self:SetProperty(self.Attack, CClientMainPlayer.Inst.FightProp:GetParam(EPlayerFightProp.PlayAtt))
    self:SetProperty(self.Hit, CClientMainPlayer.Inst.FightProp:GetParam(EPlayerFightProp.PlayHit))
    self:SetProperty(self.Def, CClientMainPlayer.Inst.FightProp:GetParam(EPlayerFightProp.PlayDef))
    self:SetProperty(self.Miss, CClientMainPlayer.Inst.FightProp:GetParam(EPlayerFightProp.PlayMiss))
    self:SetProperty(self.Hp, CClientMainPlayer.Inst.FightProp:GetParam(EPlayerFightProp.PlayHp))
end

function LuaPUBGSelfPackageWnd:SetProperty(go, value)
    if not go then return end
    local ValueLabel = go.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))

    ValueLabel.text = tostring(value)
end




-------------------------- End of PlayerProperty --------------------------

function LuaPUBGSelfPackageWnd:UpdateSingleEquip(pos, info)
    if not info then return end
    if LuaPUBGMgr.IsEquip(info.itemId) then
        self:UpdatePlayerEquipment()
    end
end

function LuaPUBGSelfPackageWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateWholePUBGPackageInfos", self, "UpdatePlayerEquipment")
    g_ScriptEvent:AddListener("UpdateSinglePUBGPackageInfo", self, "UpdateSingleEquip")
    g_ScriptEvent:AddListener("SyncChiJiFightProp", self, "UpdatePlayerProperties")
end

function LuaPUBGSelfPackageWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateWholePUBGPackageInfos", self, "UpdatePlayerEquipment")
    g_ScriptEvent:RemoveListener("UpdateSinglePUBGPackageInfo", self, "UpdateSingleEquip")
    g_ScriptEvent:RemoveListener("SyncChiJiFightProp", self, "UpdatePlayerProperties")
end
return LuaPUBGSelfPackageWnd