require("common/common_include")

local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local Time = import "UnityEngine.Time"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UISprite = import "UISprite"
local Color = import "UnityEngine.Color"
local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"

LuaChristmasTreeAddItemWnd = class()

RegistClassMember(LuaChristmasTreeAddItemWnd,"m_PressArea")
RegistClassMember(LuaChristmasTreeAddItemWnd,"m_ItemCell")
RegistClassMember(LuaChristmasTreeAddItemWnd,"m_Background")
RegistClassMember(LuaChristmasTreeAddItemWnd,"m_AmountLabel")
RegistClassMember(LuaChristmasTreeAddItemWnd,"m_DisableSprite")
RegistClassMember(LuaChristmasTreeAddItemWnd,"m_ItemCUITexture")
RegistClassMember(LuaChristmasTreeAddItemWnd,"m_NameLabel")

RegistClassMember(LuaChristmasTreeAddItemWnd,"m_ItemID")
RegistClassMember(LuaChristmasTreeAddItemWnd,"m_ItemPressed")
RegistClassMember(LuaChristmasTreeAddItemWnd,"m_LastPressTime")
RegistClassMember(LuaChristmasTreeAddItemWnd,"m_ItemAmount")

function LuaChristmasTreeAddItemWnd:Init()
    self.m_PressArea = self.transform:Find("Anchor/PressArea").gameObject
    self.m_ItemCell = self.transform:Find("Anchor/PressArea/ItemCell").gameObject
    self.m_Background = self.transform:Find("Anchor/Background"):GetComponent(typeof(UISprite))
    self.m_AmountLabel = self.transform:Find("Anchor/PressArea/ItemCell/AmountLabel"):GetComponent(typeof(UILabel))
    self.m_DisableSprite = self.transform:Find("Anchor/PressArea/ItemCell/DisableSprite").gameObject
    self.m_ItemCUITexture = self.transform:Find("Anchor/PressArea/ItemCell/IconTexture"):GetComponent(typeof(CUITexture))
    self.m_NameLabel = self.transform:Find("Anchor/PressArea/NameLabel"):GetComponent(typeof(UILabel))

    local mgr = LuaChristmasTreeAddItemMgr
    self.m_ItemID = mgr.ItemID

    --加载图标
    local ItemData = Item_Item.GetData(self.m_ItemID)
    if ItemData then
        self.m_ItemCUITexture:LoadMaterial(ItemData.Icon)
        self.m_NameLabel.text=ItemData.Name
    end

    --获取包裹中物品数量
    self.m_ItemAmount = 0
    self:UpdateItemAmount()

    --注册Press事件
    self.m_ItemPressed = false
    self.m_LastPressTime = 0
    CommonDefs.AddOnPressListener(self.m_PressArea,DelegateFactory.Action_GameObject_bool(
        function(go,pressed)
            -- 没物品的时候显示途径窗口
            if (self.m_ItemAmount == 0) and (not self.m_ItemPressed) and pressed then
                CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_ItemID, false, go.transform, CTooltipAlignType.Left)
            end
            self.m_ItemPressed = pressed
        end
    ),true)

    self:Layoutwnd()
end

function LuaChristmasTreeAddItemWnd:UpdateItemAmount()
    self.m_ItemAmount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_ItemID)
    self.m_AmountLabel.text = tostring(self.m_ItemAmount)
    if self.m_ItemAmount == 0 then
        self.m_DisableSprite:SetActive(true)
        self.m_AmountLabel.color = Color.red
    else
        self.m_DisableSprite:SetActive(false)
        self.m_AmountLabel.color = Color.white
    end
end

function LuaChristmasTreeAddItemWnd:Update()
    if self.m_ItemPressed and Time.realtimeSinceStartup - self.m_LastPressTime >= 0.1 then
        self.m_LastPressTime = Time.realtimeSinceStartup
        self:RequestSubmit()
    end
end

function LuaChristmasTreeAddItemWnd:Layoutwnd()
    local mgr = LuaChristmasTreeAddItemMgr
    local newCenter = CUICommonDef.AdjustPosition(mgr.alignType, self.m_Background.width, self.m_Background.height, mgr.targetWorldPos, mgr.targetWidth, mgr.targetHeight)
    self.transform.localPosition = newCenter
end

function LuaChristmasTreeAddItemWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
end

function LuaChristmasTreeAddItemWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
end

function LuaChristmasTreeAddItemWnd:OnSendItem(args)
    self:UpdateItemAmount()
end

function LuaChristmasTreeAddItemWnd:OnSetItemAt(args)
    self:UpdateItemAmount()
end

--向服务器请求提交道具
function LuaChristmasTreeAddItemWnd:RequestSubmit()
    if not self.m_ItemID then return end
    if self.m_ItemAmount > 0 then 
        LuaChristmasMgr.RaiseChristmasTree(self.m_ItemID, 1)
    end
end
