-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonItemSelectCell = import "L10.UI.CCommonItemSelectCell"
local CCommonItemSelectCellData = import "L10.UI.CCommonItemSelectCellData"
local CCommonItemSelectInfoMgr = import "L10.UI.CCommonItemSelectInfoMgr"
local CCommonItemSelectWnd = import "L10.UI.CCommonItemSelectWnd"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local NGUIMath = import "NGUIMath"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CCommonItemSelectWnd.m_Init_CS2LuaHook = function (this) 

    this.SelectedIndex = - 1
    this.titleLabel.text = CCommonItemSelectInfoMgr.Inst.Title
    this:LoadData()
end
CCommonItemSelectWnd.m_LoadData_CS2LuaHook = function (this) 

    Extensions.RemoveAllChildren(this.grid.transform)
    CommonDefs.ListClear(this.items)
    local allData = CCommonItemSelectInfoMgr.Inst.AllData
    do
        local i = 0
        while i < allData.Count do
            local obj = CUICommonDef.AddChild(this.grid.gameObject, this.itemTpl)
            obj:SetActive(true)
            local item = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CCommonItemSelectCell))
            item:Init(i, allData[i])
            UIEventListener.Get(obj).onClick = MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this)
            CommonDefs.ListAdd(this.items, typeof(CCommonItemSelectCell), item)
            i = i + 1
        end
    end
    this.grid:Reposition()
    this.scrollView:ResetPosition()

    if this.items.Count > 0 then
        this:OnItemClick(this.items[0].gameObject)
    end
end
CCommonItemSelectWnd.m_OnItemClick_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.items.Count do
            local selected = (this.items[i].gameObject == go)
            this.items[i].Selected = selected
            if selected then
                this.SelectedIndex = i
                local b1 = NGUIMath.CalculateRelativeWidgetBounds(this.background.transform)
                local height = b1.size.y
                local worldCenterY = this.background.transform:TransformPoint(b1.center).y
                local width = b1.size.x
                local worldCenterX = this.background.transform:TransformPoint(b1.center).x
                if not System.String.IsNullOrEmpty(this.items[i].ItemId) then
                    CItemInfoMgr.ShowLinkItemInfo(this.items[i].ItemId, false, this, CItemInfoMgr.AlignType.Right, worldCenterX, worldCenterY, width, height)
                else
                    CItemInfoMgr.ShowLinkItemTemplateInfo(this.items[i].TemplateId, false, this, CItemInfoMgr.AlignType.Right, worldCenterX, worldCenterY, width, height)
                end
            end
            i = i + 1
        end
    end
end


CCommonItemSelectInfoMgr.m_OnSelectItem_CS2LuaHook = function (this, index) 
    if index >= 0 and index < this.m_AllData.Count then
        CUIManager.CloseUI(CUIResources.CommonItemSelectWnd)
        if this.m_OnSelectCallback ~= nil then
            GenericDelegateInvoke(this.m_OnSelectCallback, Table2ArrayWithCount({this.m_AllData[index].itemId, this.m_AllData[index].templateId}, 2, MakeArrayClass(Object)))
            this.m_OnSelectCallback = nil
        end
    end
end
CCommonItemSelectInfoMgr.m_ShowJueJiExchangeItemSelectWnd_CS2LuaHook = function (this, title, validTemplateIds, onSelectCallback) 
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil then
        return
    end

    this.Title = title
    CommonDefs.ListClear(this.m_AllData)
    this.m_OnSelectCallback = onSelectCallback

    local bagSize = mainplayer.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    do
        local i = 1
        while i <= bagSize do
            local id = mainplayer.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
            if not System.String.IsNullOrEmpty(id) then
                local item = CItemMgr.Inst:GetById(id)
                if item ~= nil and CommonDefs.HashSetContains(validTemplateIds, typeof(UInt32), item.TemplateId) then
                    local data = CreateFromClass(CCommonItemSelectCellData, id, item.TemplateId, true, item.Amount)
                    CommonDefs.ListAdd(this.m_AllData, typeof(CCommonItemSelectCellData), data)
                end
            end
            i = i + 1
        end
    end

    CUIManager.ShowUI(CUIResources.CommonItemSelectWnd)
end
CCommonItemSelectInfoMgr.m_ShowIdentifyEquipSelectWnd_CS2LuaHook = function (this, onSelectCallback) 
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil then
        return
    end

    this.Title = LocalString.GetString("选择装备")
    CommonDefs.ListClear(this.m_AllData)
    this.m_OnSelectCallback = onSelectCallback

    local bodySize = mainplayer.ItemProp:GetPlaceSize(EnumItemPlace.Body)
    do
        local i = 1
        while i <= bodySize do
            local id = mainplayer.ItemProp:GetItemAt(EnumItemPlace.Body, i)
            if not System.String.IsNullOrEmpty(id) then
                local item = CItemMgr.Inst:GetById(id)
                if item ~= nil and item.IsEquip and item.Equip.IsIdentifiable then
                    local data = CreateFromClass(CCommonItemSelectCellData, id, item.TemplateId, true, item.Amount)
                    CommonDefs.ListAdd(this.m_AllData, typeof(CCommonItemSelectCellData), data)
                end
            end
            i = i + 1
        end
    end

    local bagSize = mainplayer.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    do
        local i = 1
        while i <= bagSize do
            local id = mainplayer.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
            if not System.String.IsNullOrEmpty(id) then
                local item = CItemMgr.Inst:GetById(id)
                if item ~= nil and item.IsEquip and item.Equip.IsIdentifiable then
                    local data = CreateFromClass(CCommonItemSelectCellData, id, item.TemplateId, true, item.Amount)
                    CommonDefs.ListAdd(this.m_AllData, typeof(CCommonItemSelectCellData), data)
                end
            end
            i = i + 1
        end
    end

    CUIManager.ShowUI(CUIResources.CommonItemSelectWnd)
end
