-- Auto Generated!!
local Byte = import "System.Byte"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CHousePuppetMgr = import "L10.Game.CHousePuppetMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPuppetInteractWnd = import "L10.UI.CPuppetInteractWnd"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Int32 = import "System.Int32"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
CPuppetInteractWnd.m_Init_CS2LuaHook = function (this) 
    this.tabBar.OnTabChange = MakeDelegateFromCSFunction(this.TabChange, MakeGenericClass(Action2, GameObject, Int32), this)

    UIEventListener.Get(this.backBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        CHousePuppetMgr.Inst.SaveInteractPuppetEngineId = 0
        this:Close()
    end)

    this.tabBar:ChangeTab(0, false)
end
CPuppetInteractWnd.m_TabChange_CS2LuaHook = function (this, go, index) 
    if index == 0 then
        this.expressionActionView:SetActive(true)
        this.gossipView:SetActive(false)
    elseif index == 1 then
        this.expressionActionView:SetActive(false)
        this.gossipView:SetActive(true)
        this:InitGossip()
    end
end
CPuppetInteractWnd.m_InitGossip_CS2LuaHook = function (this) 
    local engineId = CHousePuppetMgr.Inst.SaveInteractPuppetEngineId
    local idx = 0
    local gossip = nil
    if CommonDefs.DictContains(CHousePuppetMgr.Inst.EngineToIdxDic, typeof(UInt32), engineId) then
        idx = CommonDefs.DictGetValue(CHousePuppetMgr.Inst.EngineToIdxDic, typeof(UInt32), engineId)
    end

    if CClientHouseMgr.Inst.mCurFurnitureProp ~= nil and CommonDefs.DictContains(CClientHouseMgr.Inst.mCurFurnitureProp.PuppetInfo, typeof(Byte), idx) then
        local cf = CommonDefs.DictGetValue(CClientHouseMgr.Inst.mCurFurnitureProp.PuppetInfo, typeof(Byte), idx)
    elseif CClientHouseMgr.Inst.mCurFurnitureProp ~= nil and CommonDefs.DictContains(CClientHouseMgr.Inst.mCurFurnitureProp.FriendPuppetInfo, typeof(Byte), idx) then
        local cf = CommonDefs.DictGetValue(CClientHouseMgr.Inst.mCurFurnitureProp.FriendPuppetInfo, typeof(Byte), idx)
        gossip = cf.Gossip
    end

    for i = 0, 2 do
        local label = this.gossipLabel[i]
        local indexbyte = (i + 1)
        if gossip ~= nil and CommonDefs.DictContains(gossip, typeof(Byte), indexbyte) then
            label.text = CommonDefs.DictGetValue(gossip, typeof(Byte), indexbyte)
        end
        local btn = this.gossipResetBtn[i]
        local index = i
        UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function (p) 
            this:ShowResetGossipWnd(index)
        end)
    end
end
