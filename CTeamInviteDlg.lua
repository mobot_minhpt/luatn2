-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CTeamInviteDlg = import "L10.UI.Team.CTeamInviteDlg"
local CTeamInviteDlgMgr = import "L10.UI.Team.CTeamInviteDlgMgr"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local EnumEventType = import "EnumEventType"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local InviteInfo = import "L10.Game.CTeamMgr+InviteInfo"
local LocalString = import "LocalString"
local TeamMatch_Activities = import "L10.Game.TeamMatch_Activities"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local UICamera = import "UICamera"
local CChatLinkMgr = import "CChatLinkMgr"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local TeamGroupInviterInfo = import "L10.Game.CTeamGroupMgr+TeamGroupInviterInfo"

CTeamInviteDlg.m_Init_CS2LuaHook = function (this) 
    this:Refresh()
    this:DoCancel()
    this.tick = 0
    this.cancel = CTickMgr.Register(MakeDelegateFromCSFunction(this.OnTick, Action0, this), 1000, ETickType.Loop)
end
CTeamInviteDlg.m_OnTick_CS2LuaHook = function (this) 
    this.tick = this.tick + 1
    if this.tick >= CTeamInviteDlgMgr.m_Time then
        this:DoCancel()
        if CTeamInviteDlgMgr.m_OnTimeAction ~= nil then
            invoke(CTeamInviteDlgMgr.m_OnTimeAction)
        end
    end
end
CTeamInviteDlg.m_Awake_CS2LuaHook = function (this) 
    --拒绝
    --拒绝
    UIEventListener.Get(this.refuseButton).onClick = MakeDelegateFromCSFunction(this.OnClickRefuseButton, VoidDelegate, this)
    --接受
    UIEventListener.Get(this.acceptButton).onClick = MakeDelegateFromCSFunction(this.OnClickAcceptButton, VoidDelegate, this)
    --关闭
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)

    EventManager.AddListener(EnumEventType.TeamInvite, MakeDelegateFromCSFunction(this.Refresh, Action0, this))

    CommonDefs.AddOnClickListener(this.infoLabel.gameObject, DelegateFactory.Action_GameObject(function(go)
        local url = this.infoLabel:GetUrlAtPosition(UICamera.lastWorldPosition)
        if url then
            CChatLinkMgr.ProcessLinkClick(url, nil)
        end
    end), false)
end

CTeamInviteDlgMgr.m_ShowInviteDlg_CS2LuaHook = function (displayInfo, acceptAct, rejectAct, closeAct, refreshAct, time, onTimeAct) 
    CTeamInviteDlgMgr.m_DisplayInfo = displayInfo
    CTeamInviteDlgMgr.m_AcceptAction = acceptAct
    CTeamInviteDlgMgr.m_RejectAction = rejectAct
    CTeamInviteDlgMgr.m_CloseAction = closeAct
    CTeamInviteDlgMgr.m_Refresh = refreshAct
    CTeamInviteDlgMgr.m_Time = time
    CTeamInviteDlgMgr.m_OnTimeAction = onTimeAct
    CUIManager.ShowUI(CUIResources.TeamInviteDlg)
end

CTeamInviteDlgMgr.m_hookShowTeamInviteDlg = function ()
    if LuaCommonSideDialogMgr.m_IsOpen then
        CTeamInviteDlgMgr.Refresh()
        local curInviteInfo = CTeamMgr.Inst:GetLatestInviteInfo()
        if curInviteInfo then
            CommonDefs.ListRemove(CTeamMgr.Inst.inviteInfos, typeof(InviteInfo), curInviteInfo)
            LuaCommonSideDialogMgr:ClearSameKeyDataWithSelectAction("InviteDlg", function(data)
                return data.extraData and data.extraData.inviterId ~= curInviteInfo.inviterId
            end)
        end
        local text = CTeamInviteDlgMgr.m_DisplayInfo
        LuaCommonSideDialogMgr:ShowDialog(text, "InviteDlg",
            LuaCommonSideDialogMgr:GetBtnInfo(LocalString.GetString("拒绝"), function()
                if curInviteInfo ~= nil then
                    Gac2Gas.RefuseInvitePlayerJoinTeam(curInviteInfo.inviterId)
                end
            end, false, false, 0, false, true), 
            LuaCommonSideDialogMgr:GetBtnInfo(LocalString.GetString("同意"), function()
                if curInviteInfo ~= nil then
                    Gac2Gas.AcceptInvitePlayerJoinTeam(curInviteInfo.inviterId)
                end
            end, true, true),nil,function()
        end, curInviteInfo)
        return
    end
    CTeamInviteDlgMgr.ShowInviteDlg("", DelegateFactory.Action(function()
        CTeamInviteDlgMgr.OnClickAcceptButton() 
    end), DelegateFactory.Action(function()
        CTeamInviteDlgMgr.OnClickRefuseButton() 
    end), DelegateFactory.Action(function()
        CTeamInviteDlgMgr.OnClose() 
    end), DelegateFactory.Action(function()
        CTeamInviteDlgMgr.Refresh()
    end), 60, nil)
end

CTeamGroupMgr.m_hookShowTeamGroupInviteDlg = function (this)
    if LuaCommonSideDialogMgr.m_IsOpen then
        local curInviteInfo = CTeamGroupMgr.Instance:GetLastInviter()
        local text = CUICommonDef.TranslateToNGUIText(g_MessageMgr:FormatMessage("TEAM_GROUP_INVITE_JOIN_TEAM_GROUP", SafeStringFormat3("<link player=%s,%s>",curInviteInfo.Id, curInviteInfo.name)))
        if curInviteInfo ~= nil then
            CommonDefs.ListRemove(CTeamGroupMgr.Instance.inviteInfos, typeof(TeamGroupInviterInfo), curInviteInfo)
            LuaCommonSideDialogMgr:ClearSameKeyDataWithSelectAction("GroupInviteDlg", function(data)
                return data.extraData and data.extraData.Id ~= curInviteInfo.Id
            end)
        end
        LuaCommonSideDialogMgr:ShowDialog(text, "GroupInviteDlg",
            LuaCommonSideDialogMgr:GetBtnInfo(LocalString.GetString("拒绝"), function()
            end, false, false, 60, false , true), 
            LuaCommonSideDialogMgr:GetBtnInfo(LocalString.GetString("同意"), function()
                if curInviteInfo ~= nil then
                    Gac2Gas.AcceptJoinTeamGroupInvite(curInviteInfo.Id, curInviteInfo.bTeam)
                end
            end, true, true),nil,function()
        end, curInviteInfo)
        return
    end
    CTeamInviteDlgMgr.ShowInviteDlg("", DelegateFactory.Action(function()
        CTeamGroupMgr.Instance:OnAcceptTeamGroupInvite()
    end), DelegateFactory.Action(function()
        CTeamGroupMgr.Instance:OnCloseTeamGroupInvite()
    end), DelegateFactory.Action(function()
        CTeamGroupMgr.Instance:OnCloseTeamGroupInvite()
    end), DelegateFactory.Action(function()
        CTeamGroupMgr.Instance:RefreshTeamGroupInvite()
    end), 60, DelegateFactory.Action(function()
        CTeamGroupMgr.Instance:OnTimeAction()
    end))
end

CTeamInviteDlgMgr.m_OnClickRefuseButton_CS2LuaHook = function () 
    local curInviteInfo = CTeamMgr.Inst:GetLatestInviteInfo()
    if curInviteInfo ~= nil then
        Gac2Gas.RefuseInvitePlayerJoinTeam(curInviteInfo.inviterId)
    end
    CommonDefs.ListRemove(CTeamMgr.Inst.inviteInfos, typeof(InviteInfo), curInviteInfo)
end
CTeamInviteDlgMgr.m_OnClickAcceptButton_CS2LuaHook = function () 
    local curInviteInfo = CTeamMgr.Inst:GetLatestInviteInfo()
    if curInviteInfo ~= nil then
        Gac2Gas.AcceptInvitePlayerJoinTeam(curInviteInfo.inviterId)
    end
    CommonDefs.ListClear(CTeamMgr.Inst.inviteInfos)
end
CTeamInviteDlgMgr.m_Refresh_CS2LuaHook = function () 
    --刷新界面信息
    local curInviteInfo = CTeamMgr.Inst:GetLatestInviteInfo()
    if curInviteInfo ~= nil then
        local msg
        if curInviteInfo.matchingActivity ~= 0 and TeamMatch_Activities.GetData(curInviteInfo.matchingActivity) ~= nil then
            local data = TeamMatch_Activities.GetData(curInviteInfo.matchingActivity)
            msg = g_MessageMgr:FormatMessage("TEAM_INVITE_TARGET", curInviteInfo.inviterId, CTeamInviteDlgMgr.GetPlayerDisplayName(curInviteInfo, data), data.Name)
        else
            local teamName = curInviteInfo.teamName == "" and curInviteInfo.inviterName or curInviteInfo.teamName
            msg = g_MessageMgr:FormatMessage("TEAM_INVITE", curInviteInfo.inviterId, curInviteInfo.inviterName, curInviteInfo.teamLeaderId, teamName)
        end

        CTeamInviteDlgMgr.m_DisplayInfo = CUICommonDef.TranslateToNGUIText(msg)
    else
        CUIManager.CloseUI(CUIResources.TeamInviteDlg)
    end
end
CTeamInviteDlgMgr.m_GetPlayerDisplayName_CS2LuaHook = function (inviteinfo, activity) 
    local name = inviteinfo.inviterName
    if activity.Name == LocalString.GetString("一条龙") then
        name = System.String.Format(LocalString.GetString("{0} Lv.{1}"), inviteinfo.inviterName, inviteinfo.inviterGrade)
    end
    return name
end
