local UILabel = import "UILabel"
local UITable = import "UITable"
local UISprite = import "UISprite"
local QnTableView = import "L10.UI.QnTableView"
local QnTableItem = import "L10.UI.QnTableItem"
local CUITexture = import "L10.UI.CUITexture"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local Constants = import "L10.Game.Constants"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

CLuaMengGuiJieResultWnd = class()

RegistChildComponent(CLuaMengGuiJieResultWnd, "LeftTableView", QnTableView)
RegistChildComponent(CLuaMengGuiJieResultWnd, "LeftTemplateItem", QnTableItem)
RegistChildComponent(CLuaMengGuiJieResultWnd, "RightTableView", QnTableView)
RegistChildComponent(CLuaMengGuiJieResultWnd, "RightTemplateItem", QnTableItem)
RegistChildComponent(CLuaMengGuiJieResultWnd, "LeftResultNode", CUITexture)
RegistChildComponent(CLuaMengGuiJieResultWnd, "RightResultNode", CUITexture)
RegistChildComponent(CLuaMengGuiJieResultWnd, "leftSelfNode", GameObject)
RegistChildComponent(CLuaMengGuiJieResultWnd, "rightSelfNode", GameObject)

RegistClassMember(CLuaMengGuiJieResultWnd, "MainPlayerIndex")

function CLuaMengGuiJieResultWnd:Init()
    self.MainPlayerIndex = 0

    self.leftSelfNode.gameObject:SetActive(CLuaHalloween2020Mgr.playerForce == 0)
    self.rightSelfNode.gameObject:SetActive(CLuaHalloween2020Mgr.playerForce ~= 0)

    if CLuaHalloween2020Mgr.winForce == 0 then
        self.LeftResultNode.transform:Find("SucceedTexture").gameObject:SetActive(true)
        self.RightResultNode.transform:Find("FailTexture").gameObject:SetActive(true)
    elseif CLuaHalloween2020Mgr.winForce == 1 then
        self.LeftResultNode.transform:Find("FailTexture").gameObject:SetActive(true)
        self.RightResultNode.transform:Find("SucceedTexture").gameObject:SetActive(true)
    elseif CLuaHalloween2020Mgr.winForce == 2 then
        self.LeftResultNode.transform:Find("PingTexture").gameObject:SetActive(true)
        self.RightResultNode.transform:Find("PingTexture").gameObject:SetActive(true)
    end

    self.LeftTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #CLuaHalloween2020Mgr.defendInfo
        end,
        function(item, index)
            self:InitDefendItem(item, CLuaHalloween2020Mgr.defendInfo[index + 1], index % 2 == 0, index)
        end
    )
    self.RightTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #CLuaHalloween2020Mgr.attackInfo
        end,
        function(item, index)
            self:InitAttackItem(item, CLuaHalloween2020Mgr.attackInfo[index + 1], index % 2 == 0, index)
        end
    )

    self.LeftTableView:ReloadData(true, false)
    self.RightTableView:ReloadData(true, false)
    self.LeftTableView.transform:Find("LeftTable"):GetComponent(typeof(UITable)):Reposition()
    self.RightTableView.transform:Find("RightTable"):GetComponent(typeof(UITable)):Reposition()

    if CLuaHalloween2020Mgr.playerForce == 0 then
        self.LeftTableView:SetSelectRow(self.MainPlayerIndex, true)
    else
        self.RightTableView:SetSelectRow(self.MainPlayerIndex, true)
    end
end

--playerId, playerName, baoxiangNum, lingdouNum, killNum
function CLuaMengGuiJieResultWnd:InitDefendItem(item, info, isOdd, index)
    local tableItem     = item:GetComponent(typeof(QnTableItem))
    local nameLab       = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local baoxiangLab   = item.transform:Find("BaoXiangLabel"):GetComponent(typeof(UILabel))
    local lingDouLab    = item.transform:Find("LingDouLabel"):GetComponent(typeof(UILabel))
    local jiShaLab      = item.transform:Find("JiShaLabel"):GetComponent(typeof(UILabel))
    local bg            = item.transform:Find("Bg"):GetComponent(typeof(UISprite))

    if (CClientMainPlayer.Inst or {}).Id == info.playerId then
        self.MainPlayerIndex = index
    end
    nameLab.text    = info.playerName
    baoxiangLab.text= info.baoxiangNum
    lingDouLab.text = info.lingdouNum
    jiShaLab.text   = info.killNum
    bg.spriteName   = isOdd and Constants.NewOddBgSprite or Constants.NewEvenBgSprite
end

function CLuaMengGuiJieResultWnd:InitAttackItem(item, info, isOdd, index)
    local tableItem     = item:GetComponent(typeof(QnTableItem))
    local nameLab       = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local catchLab      = item.transform:Find("CatchLabel"):GetComponent(typeof(UILabel))
    local bg            = item.transform:Find("Bg"):GetComponent(typeof(UISprite))

    if (CClientMainPlayer.Inst or {}).Id == info.playerId then
        self.MainPlayerIndex = index
    end
    nameLab.text = info.playerName
    catchLab.text = info.killNum
    bg.spriteName = isOdd and Constants.NewOddBgSprite or Constants.NewEvenBgSprite
end

function CLuaMengGuiJieResultWnd:OnDestroy()
    CLuaHalloween2020Mgr.attackInfo = {}
    CLuaHalloween2020Mgr.defendInfo = {}
end