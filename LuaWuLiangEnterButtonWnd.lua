local GameObject = import "UnityEngine.GameObject"
local CRenderScene=import "L10.Engine.Scene.CRenderScene"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType =import "L10.Game.EnumWarnFXType"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CPos = import "L10.Engine.CPos"
local CMainCamera = import "L10.Engine.CMainCamera"
local COpenEntryMgr = import "L10.Game.COpenEntryMgr"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local CScene = import "L10.Game.CScene"

LuaWuLiangEnterButtonWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuLiangEnterButtonWnd, "EnterBtn_ku", "EnterBtn_ku", GameObject)
RegistChildComponent(LuaWuLiangEnterButtonWnd, "EnterBtn_ji", "EnterBtn_ji", GameObject)
RegistChildComponent(LuaWuLiangEnterButtonWnd, "EnterBtn_mie", "EnterBtn_mie", GameObject)
RegistChildComponent(LuaWuLiangEnterButtonWnd, "EnterBtn_xuanguan", "EnterBtn_xuanguan", GameObject)

--@endregion RegistChildComponent end

function LuaWuLiangEnterButtonWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.EnterBtn_ku.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterBtn_kuClick()
	end)


	
	UIEventListener.Get(self.EnterBtn_ji.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterBtn_jiClick()
	end)


	
	UIEventListener.Get(self.EnterBtn_mie.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterBtn_mieClick()
	end)


	
	UIEventListener.Get(self.EnterBtn_xuanguan.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterBtn_xuanguanClick()
	end)


    --@endregion EventBind end
end

function LuaWuLiangEnterButtonWnd:Init()

end

function LuaWuLiangEnterButtonWnd:OnEnable()
    self.m_TopAnchor = nil

    self.EnterBtn_ku:SetActive(false)
    self.EnterBtn_ji:SetActive(false)
    self.EnterBtn_mie:SetActive(false)
    self.EnterBtn_xuanguan:SetActive(false)

    local posAndHeight = XinBaiLianDong_Setting.GetData().LeiFengTaCenterPosAndHieght
    local height = posAndHeight[2]

    if LuaWuLiangMgr.isEnterPrepare then
        local setting = XinBaiLianDong_Setting.GetData()
        -- 中心区域特效（常驻、踏入高亮）
	    local centerFxList = setting.LeiFengTaCenterFxList

        local centerPos = Utility.PixelPos2WorldPos(CreateFromClass(CPos, posAndHeight[0], posAndHeight[1]))
		self.m_Fx = CEffectMgr.Inst:AddWorldPositionFX(centerFxList[1], centerPos, 0,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
        
        self.m_TopAnchor = centerPos
        self.m_TopAnchor.y = self.m_TopAnchor.y + height

        self.EnterBtn_xuanguan:SetActive(true)
    else
        local data = LuaWuLiangMgr.enterData.Gate

        local playId = data.PlayId
        local playData = XinBaiLianDong_WuLiangPlay.GetDataBySubKey("GameplayId", playId)
        local btnList = {self.EnterBtn_ku, self.EnterBtn_ji, self.EnterBtn_mie}
        local diff = playData.Id%10
        btnList[diff]:SetActive(true)

        self.m_TopAnchor = Utility.GridPos2WorldPos(data.X, data.Y)
        self.m_TopAnchor.y = self.m_TopAnchor.y + height
    end

    self:Update()
end

function LuaWuLiangEnterButtonWnd:Update()
    -- 更新位置 位于法阵正上方
    if self.m_TopAnchor then
        self.m_ViewPos = CMainCamera.Main:WorldToViewportPoint(self.m_TopAnchor)
    
        if self.m_ViewPos and self.m_ViewPos.z and self.m_ViewPos.z > 0 and 
                self.m_ViewPos.x > 0 and self.m_ViewPos.x < 1 and self.m_ViewPos.y > 0 and self.m_ViewPos.y < 1 then
            self.m_ScreenPos = CMainCamera.Main:WorldToScreenPoint(self.m_TopAnchor)
        else  
            self.m_ScreenPos = CommonDefs.op_Multiply_Vector3_Single(Vector3.up, 3000)
        end
        self.m_ScreenPos.z = 0
        self.m_TopWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_ScreenPos)

        if self.m_CachedPos == nil or self.m_CachedPos.x ~= self.m_TopWorldPos.x or self.m_CachedPos.x ~= self.m_TopWorldPos.x then
            self.transform.position = self.m_TopWorldPos
            self.m_CachedPos = self.m_TopWorldPos
        end
    end
end

function LuaWuLiangEnterButtonWnd:GetGuideGo(methodName)
    if methodName == "GetBtn" then
        return self.EnterBtn_xuanguan
	end
	return nil
end

function LuaWuLiangEnterButtonWnd:OnDisable()
    if self.m_Fx then
        self.m_Fx:Destroy()
    end
end

function LuaWuLiangEnterButtonWnd:OnEnterBtnClick()
    if LuaWuLiangMgr.isEnterPrepare then
        CUIManager.ShowUI(CLuaUIResources.WuLiangLevelWnd)
    else
        CUIManager.ShowUI(CLuaUIResources.WuLiangChallengeWnd)
    end
end

--@region UIEvent



function LuaWuLiangEnterButtonWnd:OnEnterBtn_kuClick()
    self:OnEnterBtnClick()
end

function LuaWuLiangEnterButtonWnd:OnEnterBtn_jiClick()
    self:OnEnterBtnClick()
end

function LuaWuLiangEnterButtonWnd:OnEnterBtn_mieClick()
    self:OnEnterBtnClick()
end

function LuaWuLiangEnterButtonWnd:OnEnterBtn_xuanguanClick()
    self:OnEnterBtnClick()
end


--@endregion UIEvent

