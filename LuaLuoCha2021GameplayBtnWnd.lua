local UILabel = import "UILabel"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CClientNpc = import "L10.Game.CClientNpc"
local CMainCamera = import "L10.Engine.CMainCamera"
local Object = import "System.Object"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local Guide_Logic = import "L10.Game.Guide_Logic"

LuaLuoCha2021GameplayBtnWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLuoCha2021GameplayBtnWnd, "Btn", "Btn", GameObject)
RegistChildComponent(LuaLuoCha2021GameplayBtnWnd, "BtnLabel", "BtnLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_OnClientCreateAction")
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_OnGuideFinishAction")
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_NpcTable")
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_CachedPos")
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_ViewPos")
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_ScreenPos")
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_TopWorldPos")
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_CurBtnType")
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_CurEngineId")
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_IsLuoCha")
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_HasInit")
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_FortNpcId")
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_DiedPlayerNpcId")
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_CompleteBarrierNpcIdList")
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_DestroyedBarrierNpcIdList")
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_InteractiveDistance")
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_OnMainPlayerCreatedAction")
-- 是否是任务场景中 特殊处理一下按钮的显示
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_TaskMode")
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_GuideTick")
RegistClassMember(LuaLuoCha2021GameplayBtnWnd, "m_GuideTickTick")

function LuaLuoCha2021GameplayBtnWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtnClick()
	end)


    --@endregion EventBind end
end

function LuaLuoCha2021GameplayBtnWnd:Init()
    self.m_CachedPos = self.Btn.transform.position

    self.m_TaskMode = LuaLuoCha2021Mgr:IsInTask()

    self.m_CurBtnType = 0
    self.m_NpcTable = {}

    for i=1,4 do
        local t = {}
        self.m_NpcTable[i] = t
    end

    -- 各种id初始化
    local data = LuoChaHaiShi_Setting.GetData()
    self.m_FortNpcId = data.FortNpc[0]
    self.m_DiedPlayerNpcId = data.DiedPlayerNpcId
    self.m_InteractiveDistance = data.InteractiveDistance

    self.m_DestroyedBarrierNpcIdList = {}
    self.m_CompleteBarrierNpcIdList = {}

    LuoChaHaiShi_RoadBarrierInfo.Foreach(function(k, data)
            table.insert(self.m_DestroyedBarrierNpcIdList, data.DestroyedNpcId)
            table.insert(self.m_CompleteBarrierNpcIdList, data.CompletedNpcId)
        end
    )

    CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
        if TypeIs(obj, typeof(CClientNpc)) then
            self:CheckNpc(obj, obj.EngineId)
        end
    end))

    self:Update()

    self:CheckGuide()
end

function LuaLuoCha2021GameplayBtnWnd:CheckTaskAndGuide(taskid, guideid)
    if CClientMainPlayer.Inst and CommonDefs.ListContains(CClientMainPlayer.Inst.TaskProp.CurrentTaskList, typeof(UInt32), taskid) then
        local task = CommonDefs.DictGetValue(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), taskid)
        if task ~= nil and  task.CanSubmit == 0 then
            local logic = Guide_Logic.GetData(guideid)
            --CGuideMgr.Inst:TriggerGuide(logic)
            CGuideMgr.Inst:StartGuide(guideid, CGuideMgr.Inst.mSubphase)
        end
    end
end

function LuaLuoCha2021GameplayBtnWnd:OnClientCreate(engineId)
    local clientObj = CClientObjectMgr.Inst:GetObject(engineId)
    local npc = TypeAs(clientObj, typeof(CClientNpc))
    if npc then
        self:CheckNpc(npc, engineId)
    end
end

function LuaLuoCha2021GameplayBtnWnd:CheckGuide()
    if LuaLuoCha2021Mgr:IsInTask() then
        self:CheckTaskAndGuide(22111434, 156)
        self:CheckTaskAndGuide(22111442, 157)
        self:CheckTaskAndGuide(22111444, 158)
    end
end

function LuaLuoCha2021GameplayBtnWnd:CheckNpc(npc, engineId)
    local templateId = npc.TemplateId
    local isLuoCha = LuaLuoCha2021Mgr:IsLuoChaPlayer()
    local data = LuoChaHaiShi_Setting.GetData()

    if templateId == self.m_FortNpcId and ((not isLuoCha) or self.m_TaskMode) then
        -- 炮台
        table.insert(self.m_NpcTable[1], engineId)
    elseif templateId == self.m_DiedPlayerNpcId and ((not isLuoCha) or self.m_TaskMode) then
        -- 死亡玩家
        table.insert(self.m_NpcTable[2], engineId)
    else
        -- 障碍：完整障碍
        if (isLuoCha or self.m_TaskMode) then
            for i=1, #self.m_CompleteBarrierNpcIdList do
                if templateId == self.m_CompleteBarrierNpcIdList[i] then
                    table.insert(self.m_NpcTable[3], engineId)
                end
            end
        end

        -- 障碍：被破坏障碍
        if (not isLuoCha) or self.m_TaskMode then
            for i=1, #self.m_DestroyedBarrierNpcIdList do
                if templateId == self.m_DestroyedBarrierNpcIdList[i] then
                    table.insert(self.m_NpcTable[4], engineId)
                end
            end
        end
    end

    -- 炮台特效处理
    -- 只要出现的话身上必有一个特效
    if LuaLuoCha2021Mgr:IsInGamePlay() then
        if templateId == self.m_FortNpcId then
            if LuaLuoCha2021Mgr.m_SumPickCount < 20 then
                local fxid = data.FortStateFx[0]
                CEffectMgr.Inst:AddObjectFX(fxid, npc.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
            else
                local fxid = data.FortStateFx[1]
                CEffectMgr.Inst:AddObjectFX(fxid, npc.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
            end
        end
    end
end

function LuaLuoCha2021GameplayBtnWnd:Update()
    if CClientMainPlayer.Inst == nil then
        return
    end
    local mainPlayerPos = CClientMainPlayer.Inst.RO.transform.position

    local curBtnType = -1
    local curPosition = -1
    local curDist = -1

    self.m_CurEngineId = -1
    -- 检查npc位置
    for type, npcList in ipairs(self.m_NpcTable) do
        for i=1, #npcList do
            local engineId = npcList[i]
            local clientObj = CClientObjectMgr.Inst:GetObject(engineId)
            local npc = TypeAs(clientObj, typeof(CClientNpc))
            if npc~=nil then
                local npcPosition = npc.RO.transform.position
                local distance = CommonDefs.op_Subtraction_Vector3_Vector3(mainPlayerPos, npcPosition).magnitude
                if (distance < curDist or curDist <0) and distance < self.m_InteractiveDistance then
                    curDist = distance
                    curBtnType = type
                    curPosition = npcPosition
                    self.m_CurEngineId = engineId
                end
            end
        end
    end

    
    -- 没有找到合适的目标
    if curBtnType == -1 then
        self.m_CurBtnType = -1
        self.m_ScreenPos = CommonDefs.op_Multiply_Vector3_Single(Vector3.up, 3000)
        self.m_TopWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_ScreenPos)
        if self.m_CachedPos.x ~= self.m_TopWorldPos.x or self.m_CachedPos.x ~= self.m_TopWorldPos.x then
            self.transform.position = self.m_TopWorldPos
            self.m_CachedPos = self.m_TopWorldPos
        end
        return
    end

    -- 更新位置 位于目标上
    self.m_ViewPos = CMainCamera.Main:WorldToViewportPoint(curPosition)
    
    if self.m_ViewPos and self.m_ViewPos.z and self.m_ViewPos.z > 0 and 
            self.m_ViewPos.x > 0 and self.m_ViewPos.x < 1 and self.m_ViewPos.y > 0 and self.m_ViewPos.y < 1 then
        self.m_ScreenPos = CMainCamera.Main:WorldToScreenPoint(curPosition)
    else  
        self.m_ScreenPos = CommonDefs.op_Multiply_Vector3_Single(Vector3.up, 3000)
    end
    self.m_ScreenPos.z = 0
    self.m_TopWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_ScreenPos)

    if self.m_CachedPos.x ~= self.m_TopWorldPos.x or self.m_CachedPos.x ~= self.m_TopWorldPos.x then
        self.transform.position = self.m_TopWorldPos
        self.m_CachedPos = self.m_TopWorldPos
    end

    -- 更新按钮类型
    if self.m_CurBtnType ~= curBtnType then
        self.m_CurBtnType = curBtnType
        local isLuoCha = LuaLuoCha2021Mgr:IsLuoChaPlayer()
        if self.m_CurBtnType == 1 and ((not isLuoCha) or self.m_TaskMode) then
            self.BtnLabel.text = LocalString.GetString("提交")
        elseif self.m_CurBtnType == 2 and ((not isLuoCha) or self.m_TaskMode) then
            self.BtnLabel.text = LocalString.GetString("复活")
        elseif self.m_CurBtnType == 3 then
            self.BtnLabel.text = LocalString.GetString("破坏")
        elseif self.m_CurBtnType == 4 then
            self.BtnLabel.text = LocalString.GetString("修复")
        end
    end
end

function LuaLuoCha2021GameplayBtnWnd:OnEnable()
    if self.m_OnClientCreateAction == nil then
        self.m_OnClientCreateAction = DelegateFactory.Action_uint(function(engineId)
            self:OnClientCreate(engineId)
        end)
    end
    EventManager.AddListenerInternal(EnumEventType.ClientObjCreate, self.m_OnClientCreateAction)

    if self.m_OnGuideFinishAction == nil then
        self.m_OnGuideFinishAction = DelegateFactory.Action_uint(function(guideId)
            local empty = CreateFromClass(MakeGenericClass(List, Object))
            if guideId == 156 then
                --发个rpc通知任务完成
                Gac2Gas.FinishClientTaskEvent(22111434, "FinishLuoChaPlayLuoChaGuide", MsgPackImpl.pack(empty))
                
            elseif guideId == 157 then
                Gac2Gas.FinishClientTaskEvent(22111442, "FinishLuoChaPlayPlayerGuide1", MsgPackImpl.pack(empty))
            elseif guideId == 158 then
                Gac2Gas.FinishClientTaskEvent(22111444, "FinishLuoChaPlayPlayerGuide2", MsgPackImpl.pack(empty))
            end
        end)
    end
    EventManager.AddListenerInternal(EnumEventType.GuideEnd, self.m_OnGuideFinishAction)

    if self.m_OnUpdateTaskAction == nil then
        self.m_OnUpdateTaskAction = DelegateFactory.Action_uint(function(taskId)
            self:CheckGuide()
        end)
    end
    EventManager.AddListenerInternal(EnumEventType.UpdateTask, self.m_OnUpdateTaskAction)

    UnRegisterTick(self.m_GuideTick)
    UnRegisterTick(self.m_GuideTickTick)
    if LuaLuoCha2021Mgr:IsInTask() then
        self.m_GuideTickTick = RegisterTickOnce(
            function()
                self:CheckGuide()
                self.m_GuideTick = RegisterTick(function()
                    self:CheckGuide()
                end, 7777)
            end, 1777
        )
    end
    if self.m_OnMainPlayerCreatedAction == nil then
        self.m_OnMainPlayerCreatedAction = DelegateFactory.Action(function()
            UnRegisterTick(self.m_GuideTick)
            UnRegisterTick(self.m_GuideTickTick)
            self.m_GuideTickTick = RegisterTickOnce(
                function()
                    self:CheckGuide()
                    self.m_GuideTick = RegisterTick(function()
                        self:CheckGuide()
                    end, 7777)
                end, 1777
            )
        end)
    end
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerCreated, self.m_OnMainPlayerCreatedAction)
end

function LuaLuoCha2021GameplayBtnWnd:OnDisable()
    EventManager.RemoveListenerInternal(EnumEventType.ClientObjCreate, self.m_OnClientCreateAction)
    EventManager.RemoveListenerInternal(EnumEventType.GuideEnd, self.m_OnGuideFinishAction)
    EventManager.RemoveListenerInternal(EnumEventType.UpdateTask, self.m_OnUpdateTaskAction)
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerCreated, self.m_OnMainPlayerCreatedAction)
    UnRegisterTick(self.m_GuideTick)
    UnRegisterTick(self.m_GuideTickTick)
end

function LuaLuoCha2021GameplayBtnWnd:GetGuideGo(methodName)
    if methodName == "GetDestoryFazhenBtn" and self.m_CurBtnType == 3 then
        return self.Btn
    elseif methodName == "GetRepairFazhenBtn" and self.m_CurBtnType == 4 then
        return self.Btn
    elseif methodName == "GetRepairFortBtn" and self.m_CurBtnType == 1 then
        return self.Btn
    end
    return nil
end

--@region UIEvent

function LuaLuoCha2021GameplayBtnWnd:OnBtnClick()
    -- 根据不同的按钮类型 发送不同的RPC
    if self.m_TaskMode then
        if CGuideMgr.Inst.mPhase == 157 and CGuideMgr.Inst.mSubphase == 1 and self.m_CurBtnType == 4 then
        end
    else
        if self.m_CurBtnType ~= -1 and self.m_CurEngineId ~= -1 then
            local isLuoCha = LuaLuoCha2021Mgr:IsLuoChaPlayer()
            -- 1是炮台 2是死亡玩家 3是破坏障碍 4是修复障碍
            if self.m_CurBtnType == 1 then
                Gac2Gas.FortCommitPick(self.m_CurEngineId)
            elseif self.m_CurBtnType == 2 then
                if not isLuoCha then
                    Gac2Gas.ResurrectPlayer(self.m_CurEngineId)
                end
            elseif self.m_CurBtnType == 3 or self.m_CurBtnType == 4 then
                Gac2Gas.InteractWithRoadBarrier(self.m_CurEngineId)
            end
        end
    end
end


--@endregion UIEvent

