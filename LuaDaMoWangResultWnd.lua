local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local UILabel = import "UILabel"
local UISlider = import "UISlider"
local CUITexture = import "L10.UI.CUITexture"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaDaMoWangResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaMoWangResultWnd, "CloseBtn", "CloseBtn", GameObject)
RegistChildComponent(LuaDaMoWangResultWnd, "ShareBtn", "ShareBtn", GameObject)
RegistChildComponent(LuaDaMoWangResultWnd, "PlayerInfoNode", "PlayerInfoNode", GameObject)
RegistChildComponent(LuaDaMoWangResultWnd, "BossNode", "BossNode", GameObject)
RegistChildComponent(LuaDaMoWangResultWnd, "ChallengerNode", "ChallengerNode", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaMoWangResultWnd, "m_AwardTable")
RegistClassMember(LuaDaMoWangResultWnd, "m_StampTick")
RegistClassMember(LuaDaMoWangResultWnd, "m_HpPrecentTick")
RegistClassMember(LuaDaMoWangResultWnd, "m_ShareTick")

function LuaDaMoWangResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	local onShareClick = function(go)
	  self:SetOpBtnVisible(false)
	  if self.m_ShareTick then
		UnRegisterTick(self.m_ShareTick)
		self.m_ShareTick = nil
	  end
	  self.m_ShareTick = RegisterTickWithDuration(function ()
		self:SetOpBtnVisible(true)
	  end, 1000, 1000)
	  CUICommonDef.CaptureScreenAndShare()
	end
	CommonDefs.AddOnClickListener(self.ShareBtn,DelegateFactory.Action_GameObject(onShareClick),false)
    --@endregion EventBind end
end

function LuaDaMoWangResultWnd:Init()
	LuaShuJia2021Mgr.InitInfo()
	self.m_AwardTable = {}
	self.BossNode:SetActive(LuaShuJia2021Mgr.isBoss)
	self.ChallengerNode:SetActive(not LuaShuJia2021Mgr.isBoss)

	self:InitPlayerInfo()
	self:InitNode(LuaShuJia2021Mgr.isBoss)
end

function LuaDaMoWangResultWnd:InitNode(isBoss)
	if isBoss then
		self:InitBossAndAward(self.BossNode)
		self:InitBossNode()
	else
		self:InitBossAndAward(self.ChallengerNode)
		self:InitChallengerNode()
	end
end

function LuaDaMoWangResultWnd:InitBossAndAward(go)
	local awardIdTable = LuaShuJia2021Mgr.awardIdTable
	for i=1,3 do
		local templateItem = go.transform:Find("AwardTable/AwardTemplate"..i).gameObject
		if LuaShuJia2021Mgr.isBoss then
			templateItem.transform:Find("AwardItemCell"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init(awardIdTable[i])
		else
			templateItem.transform:Find("AwardItemCell"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init(awardIdTable[4 - i])
		end
		table.insert(self.m_AwardTable, templateItem)
	end
	go.transform:Find("BossIcon"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(LuaShuJia2021Mgr.bossIcon)
	go.transform:Find("BossName"):GetComponent(typeof(UILabel)).text = LuaShuJia2021Mgr.bossName
end

function LuaDaMoWangResultWnd:InitBossNode()
	if self.m_AwardTable == {} then self:InitBossAndAward(self.BossNode) end
	for i=1,3 do
		self.m_AwardTable[i].transform:Find("AwardLabel"):GetComponent(typeof(UILabel)).text = LuaShuJia2021Mgr.bossTipTextTable[i]
		self.m_AwardTable[i].transform:Find("Stamp").gameObject:SetActive(false)
		self.m_AwardTable[i].transform:Find("Stamp/CompleteStamp").gameObject:SetActive(LuaShuJia2021Mgr.AchieveInPlay[i])
		self.m_AwardTable[i].transform:Find("Stamp/UnCompleteStamp").gameObject:SetActive(not (LuaShuJia2021Mgr.AchieveInPlay[i]))
	end

	local interval = 200 --时间间隔0.2s
	local result = 3 * interval 
	local temp = 0
	-- 延迟显示Stamp
	self.m_StampTick = RegisterTickWithDuration(function ()
		if temp > result then
			UnRegisterTick(self.m_StampTick)
			self.m_StampTick = nil
			return 
		end
		local index = temp/interval+1
		self.m_AwardTable[index].transform:Find("Stamp").gameObject:SetActive(true)
		local awardItemCell = self.m_AwardTable[index].transform:Find("AwardItemCell"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
		awardItemCell:SetAwardComplete(LuaShuJia2021Mgr.AchieveInPlay[index])
		if LuaShuJia2021Mgr.GetAwardInPlay[index] then LuaShuJia2021Mgr.ShowJieSuanFx(self.m_AwardTable[index]) end
		temp = temp + interval
	end, interval, result)
end

function LuaDaMoWangResultWnd:InitChallengerNode()
	if self.m_AwardTable == {} then self:InitBossAndAward(self.BossNode) end
	for i=1,3 do
		self.m_AwardTable[i].transform:Find("PrecentLabel"):GetComponent(typeof(UILabel)).text =SafeStringFormat3( "%d%%",(1 - LuaShuJia2021Mgr.ChallengerTipHpPrecentTable[4 - i] )*100)
	end
	local BossHpPrecentLabel = self.ChallengerNode.transform:Find("BossHpPrecent"):GetComponent(typeof(UILabel))
	local BossHpBar = self.ChallengerNode.transform:Find("BossHpBar"):GetComponent(typeof(UISlider))

	local Interval = 200 --时间间隔0.2s
	local Result = 3 * Interval 
	local temp = 0
	self.m_StampTick = RegisterTickWithDuration(function ()
		if temp > Result then
			UnRegisterTick(self.m_StampTick)
			self.m_StampTick = nil
			return 
		end
		local index = (3 - temp/Interval)
		local awardItemCell = self.m_AwardTable[index].transform:Find("AwardItemCell"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
		awardItemCell:SetAwardComplete(LuaShuJia2021Mgr.AchieveInPlay[4 - index])
		if LuaShuJia2021Mgr.GetAwardInPlay[4 - index] then LuaShuJia2021Mgr.ShowJieSuanFx(self.m_AwardTable[index]) end
		temp = temp + Interval
	end, Interval, Result)

	local result = (1 - LuaShuJia2021Mgr.bossHpPrecent) * 100
	if result < 0 then result = 0 end
	local showTime = 500 -- 动画过程0.5s
	local tempPrecent = 100
	BossHpPrecentLabel.text = tempPrecent
	BossHpBar.value = tempPrecent/100
	if result == 100 then return end
	local interval = math.ceil(showTime / (100 - result))


	self.m_HpPrecentTick = RegisterTickWithDuration(function ()
		if tempPrecent < result then
			tempPrecent = result
			UnRegisterTick(self.m_HpPrecentTick)
			self.m_HpPrecentTick = nil
			return 
		end
		tempPrecent = tempPrecent - 1
		BossHpPrecentLabel.text = tempPrecent
		BossHpBar.value = tempPrecent/100
	end, interval , interval*(100 - result))


end
--@region UIEvent

function LuaDaMoWangResultWnd:SetOpBtnVisible(sign)
	self.CloseBtn:SetActive(sign)
	self.ShareBtn:SetActive(sign)
end

function LuaDaMoWangResultWnd:InitPlayerInfo()
	if CLoginMgr.Inst then
		local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = myGameServer.name
	end
	if CClientMainPlayer.Inst then
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Name"), typeof(UILabel)).text = CClientMainPlayer.Inst.RealName
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Icon/Lv"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Level)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), false)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("ID"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Id)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = CClientMainPlayer.Inst:GetMyServerName()
	elseif LuaShuJia2021Mgr.m_Name then
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Name"), typeof(UILabel)).text = LuaShuJia2021Mgr.m_Name
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Icon/Lv"), typeof(UILabel)).text = tostring(LuaShuJia2021Mgr.m_Level)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(LuaShuJia2021Mgr.m_Class, LuaShuJia2021Mgr.m_Gender, -1), false)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("ID"), typeof(UILabel)).text = tostring(LuaShuJia2021Mgr.m_Id)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = LuaShuJia2021Mgr.m_ServerName
	end
end

function LuaDaMoWangResultWnd:OnDisable()
	if self.m_StampTick~=nil then UnRegisterTick(self.m_StampTick) end
	if self.m_HpPrecentTick~=nil then UnRegisterTick(self.m_HpPrecentTick) end
	if self.m_ShareTick~=nil then UnRegisterTick(self.m_ShareTick) end
end

--@endregion UIEvent

