local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local UILabel = import "UILabel"
local UITable = import "UITable"
local Extensions = import "Extensions"
local MessageMgr = import "L10.Game.MessageMgr"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"

CLuaMengGuiJieTipWnd = class()

RegistChildComponent(CLuaMengGuiJieTipWnd, "Table", UITable)
RegistChildComponent(CLuaMengGuiJieTipWnd, "ParagraphTemplate", GameObject)
RegistChildComponent(CLuaMengGuiJieTipWnd, "GuidNode", GameObject)
RegistChildComponent(CLuaMengGuiJieTipWnd, "GuidLabel", UILabel)


function CLuaMengGuiJieTipWnd:Init()
    if CLuaHalloween2020Mgr.IsInPlay() then
        self.GuidNode.gameObject:SetActive(true)
        if CLuaHalloween2020Mgr.playerForce == 0 then
            self.GuidLabel.text = LocalString.GetString("本局中为逃跑方!")
        else
            self.GuidLabel.text = LocalString.GetString("本局中为抓捕方!")
        end
    else
        self.GuidNode.gameObject:SetActive(false)
    end
    

    Extensions.RemoveAllChildren(self.Table.transform)
    local ruleMessage = MessageMgr.Inst:FormatMessage("HALLOWEEN2020_MENGGUIJIE_TIP", {})
    local ruleTipInfo = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(ruleMessage)
    if ruleTipInfo ~= nil then
        for i = 0, ruleTipInfo.paragraphs.Count - 1 do
            local rule = CUICommonDef.AddChild(self.Table.gameObject, self.ParagraphTemplate):GetComponent(typeof(CTipParagraphItem))
            rule.gameObject:SetActive(true)
            rule:Init(ruleTipInfo.paragraphs[i], 0xffffffff)
        end
    end
    self.Table:Reposition()
end
