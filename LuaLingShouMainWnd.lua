local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local QnTabView=import "L10.UI.QnTabView"
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"


CLuaLingShouMainWnd = class()
RegistClassMember(CLuaLingShouMainWnd,"qnTabView")
RegistClassMember(CLuaLingShouMainWnd,"Title")
RegistClassMember(CLuaLingShouMainWnd,"tabButtons")
RegistClassMember(CLuaLingShouMainWnd,"buttons")
RegistClassMember(CLuaLingShouMainWnd,"pinzhiIcon")
RegistClassMember(CLuaLingShouMainWnd,"yinyangButton")
RegistClassMember(CLuaLingShouMainWnd,"levelLimitButton")

RegistClassMember(CLuaLingShouMainWnd,"m_SkillTab")
RegistClassMember(CLuaLingShouMainWnd,"m_XiuWuTab")

RegistClassMember(CLuaLingShouMainWnd,"m_OpenSkillLv")
RegistClassMember(CLuaLingShouMainWnd,"m_OpenXiuWuLv")

CLuaLingShouMainWnd.tabIndex=0
CLuaLingShouMainWnd.openUseWnd=false
CLuaLingShouMainWnd.fromPackage=false

function CLuaLingShouMainWnd.UseSiLingWan()
    CLuaLingShouMainWnd.tabIndex=3
    CLuaLingShouMainWnd.openUseWnd=false
    CLuaLingShouMainWnd.fromPackage=true
    CUIManager.ShowUI(CUIResources.LingShouMainWnd)
end
function CLuaLingShouMainWnd.UseCangHaiShiXin()
    CLuaLingShouMainWnd.tabIndex=3
    CLuaLingShouMainWnd.openUseWnd=false
    CLuaLingShouMainWnd.fromPackage=true
    CUIManager.ShowUI(CUIResources.LingShouMainWnd)
end

function CLuaLingShouMainWnd.UseBiLiuLu()
    CLuaLingShouMainWnd.tabIndex=1
    CLuaLingShouMainWnd.openUseWnd=false
    CLuaLingShouMainWnd.fromPackage=true
    CUIManager.ShowUI(CUIResources.LingShouMainWnd)
end

function CLuaLingShouMainWnd.UseShouYuan()
    CLuaLingShouMainWnd.tabIndex=0
    CLuaLingShouMainWnd.openUseWnd=true
    CLuaLingShouMainWnd.fromPackage=true
    CLuaLingShouUseWnd.useType = 1
    CUIManager.ShowUI(CUIResources.LingShouMainWnd)
end


function CLuaLingShouMainWnd.UseRenShenGuo()
    CLuaLingShouMainWnd.tabIndex=0
    CLuaLingShouMainWnd.openUseWnd=true
    CLuaLingShouMainWnd.fromPackage=true
    CLuaLingShouUseWnd.useType = 0--CLingShouUseWnd.UseType.Exp
    CUIManager.ShowUI(CUIResources.LingShouMainWnd)
end

function CLuaLingShouMainWnd.UseSkillBook()
    CLuaLingShouMainWnd.tabIndex=2
    CLuaLingShouMainWnd.openUseWnd=false
    CLuaLingShouMainWnd.fromPackage=true
    CUIManager.ShowUI(CUIResources.LingShouMainWnd)
end
function CLuaLingShouMainWnd.UseBabyExpItem()
    CLuaLingShouMainWnd.UseRenShenGuo()
end


function CLuaLingShouMainWnd:Awake()

    self.m_OpenSkillLv = LingShou_Setting.GetData().OpenSkillLv
    self.m_OpenXiuWuLv = LingShou_Setting.GetData().OpenXiuWuLv

    self.qnTabView = self.transform:Find("TabContents"):GetComponent(typeof(QnTabView))
    self.Title = self.transform:Find("Wnd_Bg_Primary_Tab/TitleLabel"):GetComponent(typeof(UILabel))
    -- self.closeBtn = self.transform:Find("Wnd_Bg_Primary_Tab/CloseButton").gameObject
    self.tabButtons = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs").gameObject
    self.buttons = self.transform:Find("TabContents/Property/Content/Normal/Buttons").gameObject
    self.pinzhiIcon = self.transform:Find("TabContents/Property/Content/Property/Pinzhi").gameObject
    self.yinyangButton = self.transform:Find("TabContents/Property/Content/Property/GenderSprite").gameObject
    self.levelLimitButton = self.transform:Find("TabContents/Property/Content/LevelLimitButton").gameObject

    if CLingShouMgr.Inst.fromNPC then
        self.tabButtons:SetActive(false)
        self.buttons:SetActive(false)
        --CLingShouMgr.Inst.fromFangSheng = false;
    end
    if CLingShouMgr.Inst.isExchangeFurniture then
        self.Title.text = LocalString.GetString("灵兽装饰物转化")
    else
        self.Title.text = LocalString.GetString("灵兽")
    end

    self.m_SkillTab = FindChild(self.transform,"Tab3")
    self.m_XiuWuTab = FindChild(self.transform,"Tab4")
end
function CLuaLingShouMainWnd:Init( )
    self.qnTabView.OnSelect = DelegateFactory.Action_QnTabButton_int(function(btn,index)
        self:OnTabSelected(btn,index)
    end)

    self.qnTabView:ChangeTo(CLuaLingShouMainWnd.tabIndex)
    CLuaLingShouMainWnd.tabIndex = 0

    if CLuaLingShouMainWnd.openUseWnd == true then
        CUIManager.ShowUI(CUIResources.LingShouUseWnd)
        CLuaLingShouMainWnd.openUseWnd = false
    end
end
function CLuaLingShouMainWnd:OnTabSelected( button, selectedIndex) 
    -- 全民PK特殊处理一下
    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
        if selectedIndex ~= 0 then
            g_MessageMgr:ShowMessage("QMPK_FORBID_LINGSHOU")
            -- self:ChangeTo(0)
            self.qnTabView:ChangeTo(0)
            return
        end
    end

    if selectedIndex == 0 then
    elseif selectedIndex == 1 then
    elseif selectedIndex == 2 or selectedIndex == 3 then
        local lock = button.transform:Find("Lock")
        if lock and lock.gameObject.activeSelf then
            self.qnTabView:ChangeTo(0)
        end
    end
end
function CLuaLingShouMainWnd:OnDestroy( )
    CLingShouMgr.Inst.fromNPC = false
    CLingShouMgr.Inst.isExchangeFurniture = false

    --关掉窗口的时候把详情信息去掉，每次再重新获取
    CLingShouMgr.Inst:ClearLingShouData()

    --把相关的窗口都关掉
    CUIManager.CloseUI(CUIResources.LingShouAutoWashWnd)
    CUIManager.CloseUI(CUIResources.LingShouWashResultWnd)
    CUIManager.CloseUI(CUIResources.LingShouBattleConfigWnd)
    CUIManager.CloseUI(CUIResources.LingShouLearnSkillWnd)
    CUIManager.CloseUI(CUIResources.LingShouPurchaseWnd)
    CUIManager.CloseUI(CUIResources.LingShouUnlockWnd)
    CUIManager.CloseUI(CUIResources.LingShouUseWnd)
end
function CLuaLingShouMainWnd:GetGuideGo( methodName) 
    if methodName == "GetPinZhiIcon" then
        return self.pinzhiIcon
    elseif methodName == "GetYinYangButton" then
        if self.yinyangButton.activeInHierarchy then
            return self.yinyangButton
        else
            CGuideMgr.Inst:EndCurrentPhase()
        end
    elseif methodName == "GetLevelLimitButton" then
        -- if self.levelLimitButton.activeInHierarchy then
            return self.levelLimitButton
        -- else
        --     CGuideMgr.Inst:EndCurrentPhase()
        -- end
    else
        -- CLogMgr.LogError(LocalString.GetString("引导数据表填写错误"))
        return nil
    end
end

function CLuaLingShouMainWnd:OnEnable()
    g_ScriptEvent:AddListener("SelectLingShouAtRow", self, "SelectLingShouAtRow")
    g_ScriptEvent:AddListener("UpdateLingShouOverview", self, "OnUpdateLingShouOverview")

end
function CLuaLingShouMainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SelectLingShouAtRow", self, "SelectLingShouAtRow")
    g_ScriptEvent:RemoveListener("UpdateLingShouOverview", self, "OnUpdateLingShouOverview")

end

function CLuaLingShouMainWnd:SelectLingShouAtRow(row)
    -- local row = args[0]

    local list = CLingShouMgr.Inst:GetLingShouOverviewList()
    local tabIndex = self.qnTabView.CurrentSelectTab
    if row < list.Count then
        local level = list[row].level
        if (level < self.m_OpenSkillLv and tabIndex==2) or (level<self.m_OpenXiuWuLv and tabIndex==3) then
            self.qnTabView:ChangeTo(0)
            if CLuaLingShouMainWnd.fromPackage then
                g_MessageMgr:ShowMessage(tabIndex == 2 and "LingShou_LearnSkill_Limit" or "LingShou_XiuWu_Limit")
                CLuaLingShouMainWnd.fromPackage = false
            end
        end
    else
        if CLuaLingShouMainWnd.fromPackage then
            g_MessageMgr:ShowMessage(tabIndex == 2 and "LingShou_LearnSkill_Limit" or "LingShou_XiuWu_Limit")
            CLuaLingShouMainWnd.fromPackage = false
        end
    end


    local function func(transform,levelRequire)
        local lockTf = transform:Find("Lock")
        local unlockTf = transform:Find("Label")

        local list = CLingShouMgr.Inst:GetLingShouOverviewList()
        if row < list.Count then
            local level = list[row].level
            if level >= levelRequire then
                lockTf.gameObject:SetActive(false)
                unlockTf.gameObject:SetActive(true)
            else
                lockTf.gameObject:SetActive(true)
                unlockTf.gameObject:SetActive(false)
            end
        else
            lockTf.gameObject:SetActive(true)
            unlockTf.gameObject:SetActive(false)
        end
    end
    func(self.m_SkillTab,self.m_OpenSkillLv)
    func(self.m_XiuWuTab,self.m_OpenXiuWuLv)
end

function CLuaLingShouMainWnd:OnUpdateLingShouOverview(args)
    local lingshouId=args[0]
    local tabIndex = self.qnTabView.CurrentSelectTab

    if CLingShouMgr.Inst.selectedLingShou == lingshouId then
        local data = CLingShouMgr.Inst:GetLingShouOverview(lingshouId)
        local function func(transform,levelRequire)
            local lockTf = transform:Find("Lock")
            local unlockTf = transform:Find("Label")
            if data.level> levelRequire then
                lockTf.gameObject:SetActive(false)
                unlockTf.gameObject:SetActive(true)
            end
        end
        func(self.m_SkillTab,self.m_OpenSkillLv)
        func(self.m_XiuWuTab,self.m_OpenXiuWuLv)
    end
end
