-- Auto Generated!!
local Byte = import "System.Byte"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CHousePuppetMgr = import "L10.Game.CHousePuppetMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPuppetAppearanceWnd = import "L10.UI.CPuppetAppearanceWnd"
local CPuppetClothesMgr = import "L10.UI.CPuppetClothesMgr"
CPuppetAppearanceWnd.m_Load_CS2LuaHook = function (this, renderObj) 
    local idx = CPuppetClothesMgr.selectIdx
    if idx == 0 then
        return
    end
    local playerPuppetInfo = CommonDefs.DictGetValue(CClientHouseMgr.Inst.mCurFurnitureProp.PuppetInfo, typeof(Byte), idx)
    if playerPuppetInfo ~= nil then
        CHousePuppetMgr.Inst:InitCPRenderObject(renderObj, playerPuppetInfo)
    end
end

