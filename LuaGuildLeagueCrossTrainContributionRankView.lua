local DelegateFactory  = import "DelegateFactory"
local UIScrollView  = import "UIScrollView"
local UITable = import "UITable"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local AlignType = import "CPlayerInfoMgr+AlignType"
local Vector3 = import "UnityEngine.Vector3"

LuaGuildLeagueCrossTrainContributionRankView = class()

RegistClassMember(LuaGuildLeagueCrossTrainContributionRankView, "m_TimeLabel")
RegistClassMember(LuaGuildLeagueCrossTrainContributionRankView, "m_Template")
RegistClassMember(LuaGuildLeagueCrossTrainContributionRankView, "m_ScrollView")
RegistClassMember(LuaGuildLeagueCrossTrainContributionRankView, "m_Table")
RegistClassMember(LuaGuildLeagueCrossTrainContributionRankView, "m_EmptyLabel")

function LuaGuildLeagueCrossTrainContributionRankView:Awake()
    self.m_Template = self.transform:Find("ScrollView/Item").gameObject
    self.m_ScrollView = self.transform:Find("ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_Table = self.transform:Find("ScrollView/Table"):GetComponent(typeof(UITable))
    self.m_EmptyLabel = self.transform:Find("EmptyLabel").gameObject
    self.m_Template:SetActive(false)
end

function LuaGuildLeagueCrossTrainContributionRankView:Init()
    self:LoadData({})
    LuaGuildLeagueCrossMgr:QueryGuildLeagueTrainRank()
end

function LuaGuildLeagueCrossTrainContributionRankView:OnEnable()
    g_ScriptEvent:AddListener("OnQueryGuildLeagueTrainRankResult", self, "OnQueryGuildLeagueTrainRankResult")
    self:Init()
end

function LuaGuildLeagueCrossTrainContributionRankView:OnDisable()
    g_ScriptEvent:RemoveListener("OnQueryGuildLeagueTrainRankResult", self, "OnQueryGuildLeagueTrainRankResult")
end

function LuaGuildLeagueCrossTrainContributionRankView:OnQueryGuildLeagueTrainRankResult(rankInfoTbl)
    self:LoadData(rankInfoTbl)
end

function LuaGuildLeagueCrossTrainContributionRankView:LoadData(rankInfoTbl)
    self.m_EmptyLabel:SetActive(#rankInfoTbl==0)
    Extensions.RemoveAllChildren(self.m_Table.transform)

    for i=1, #rankInfoTbl do
        local child = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_Template)
        child:SetActive(true)
        local info = rankInfoTbl[i]
        self:InitOneRankInfo(child.transform, info)
        if i % 2 == 1 then
            child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.OddBgSpirite)
        else
            child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.EvenBgSprite)
        end
        UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnRankItemClick(child.gameObject, info) end)
    end
    self.m_Table:Reposition()
    self.m_ScrollView:ResetPosition()
end

function LuaGuildLeagueCrossTrainContributionRankView:InitOneRankInfo(transRoot, rankInfo)
    transRoot:Find("Icon").gameObject:SetActive(rankInfo.isMyGuild)
    local rankLabel = transRoot:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankIcon = transRoot:Find("RankLabel/RankIcon"):GetComponent(typeof(UISprite))
    local playerNameLabel = transRoot:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
    local guildNameLabel = transRoot:Find("GuildNameLabel"):GetComponent(typeof(UILabel))
    local scoreLabel = transRoot:Find("ScoreLabel"):GetComponent(typeof(UILabel)) 
    local color = rankInfo.isMyGuild and NGUIText.ParseColor24("04DC16", 0) or NGUIText.ParseColor24("FFFFFF", 0)
    rankLabel.text = rankInfo.rank > 0 and tostring(rankInfo.rank) or LocalString.GetString("未上榜")
    rankLabel.color = color
    rankIcon.spriteName = self:GetRankSpriteName(rankInfo.rank)
    playerNameLabel.text = rankInfo.playerName
    playerNameLabel.color = color
    guildNameLabel.text = rankInfo.guildName
    guildNameLabel.color = color
    if rankInfo.isMyGuild and rankInfo.rank<=0 then
        scoreLabel.text = SafeStringFormat3(LocalString.GetString("%d(上榜需%d)"), rankInfo.score, LuaGuildLeagueCrossMgr.m_TrainInfo.rankMinScore + 1 ) --TODO 可能需要洪磊同步
    else
        scoreLabel.text = tostring(rankInfo.score)
    end
    scoreLabel.color = color
end

function LuaGuildLeagueCrossTrainContributionRankView:GetRankSpriteName(rank)
    if rank == 1 then
        return g_sprites.RankFirstSpriteName
    elseif rank == 2 then
        return g_sprites.RankSecondSpriteName
    elseif rank == 3 then
        return g_sprites.RankThirdSpriteName
    else
        return ""
    end
end

function LuaGuildLeagueCrossTrainContributionRankView:OnRankItemClick(go, info)
    CPlayerInfoMgr.ShowPlayerPopupMenu(info.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, Vector3.zero, AlignType.Default)
end


