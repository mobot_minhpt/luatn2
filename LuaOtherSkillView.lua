local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local CellIndexType = import "L10.UI.UITableView+CellIndexType"
local COtherSkillDetailView = import "L10.UI.COtherSkillDetailView"
local CExpressionActionView = import "L10.UI.CExpressionActionView"
local CExpressionActionMgr = import "L10.Game.CExpressionActionMgr"
local CellIndex = import "L10.UI.UITableView+CellIndex"
local UIDefaultTableView=import "L10.UI.UIDefaultTableView"
local UIDefaultTableViewCell=import "L10.UI.UIDefaultTableViewCell"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaOtherSkillView =class()
RegistClassMember(LuaOtherSkillView,"m_MasterView")
RegistClassMember(LuaOtherSkillView,"m_DetailView")
RegistClassMember(LuaOtherSkillView,"m_ExpressionActionView")
RegistClassMember(LuaOtherSkillView,"m_FishingActionView")
RegistClassMember(LuaOtherSkillView,"m_ZongPaiSkillView")

RegistClassMember(LuaOtherSkillView,"categories")
RegistClassMember(LuaOtherSkillView,"cateroryDict")
RegistClassMember(LuaOtherSkillView,"skillCategotyStart")--技能类别起始index
RegistClassMember(LuaOtherSkillView,"expressionActions")--表情动作的类别，因为不需要显示具体的cell，因此只有一个对象
--仙职技能
RegistClassMember(LuaOtherSkillView,"xianzhiSkillId")
RegistClassMember(LuaOtherSkillView,"xianzhiSectionId")
RegistClassMember(LuaOtherSkillView,"m_XianZhiSkillTab")
--钓鱼技能
RegistClassMember(LuaOtherSkillView,"fishingSectionId")
--长生诀
RegistClassMember(LuaOtherSkillView,"changshengjueSkillId")
RegistClassMember(LuaOtherSkillView,"changshengjueSectionId")
LuaOtherSkillView.expressionActionOpen = true

function LuaOtherSkillView:Awake()
    self.skillCategotyStart = 1
    self:InitComponents()
end


function LuaOtherSkillView:InitComponents()
    self.m_DetailView = self.transform:Find("DetailView"):GetComponent(typeof(COtherSkillDetailView))
    self.m_ExpressionActionView = self.transform:Find("ExpressionActionView"):GetComponent(typeof(CExpressionActionView))
    self.m_MasterView = self.transform:Find("MasterView"):GetComponent(typeof(UIDefaultTableView))
    self.m_FishingActionView = self.transform:Find("FishingSkillView"):GetComponent(typeof(CCommonLuaScript))
    self.m_ZongPaiSkillView = self.transform:Find("ZongPaiSkillView"):GetComponent(typeof(CCommonLuaScript))

    self.m_MasterView:Init(function()
            if CommonDefs.IS_VN_CLIENT or CommonDefs.IS_HMT_CLIENT then 
                return #self.categories + self.skillCategotyStart + 1 + LuaSeaFishingMgr.GetAddSectionCount()
            else 
                return #self.categories + self.skillCategotyStart + 2 + LuaSeaFishingMgr.GetAddSectionCount()
            end
        end,
        function(section)
            if section>=self.skillCategotyStart and section<#self.categories+self.skillCategotyStart then
                return #self.categories[section-self.skillCategotyStart+1].skills
            elseif section==self.changshengjueSectionId then
                return 1
            elseif section==self.xianzhiSectionId then
                -- 仙职技能只能有一个
                return 1
            elseif section == self.fishingSectionId then
                return 0
            end
            return 0
        end,
        function(cell,index)
            local item = cell:GetComponent(typeof(UIDefaultTableViewCell))
            local label = cell.transform:Find("Label"):GetComponent(typeof(UILabel))
            local reddot = cell.transform:Find("RedDot").gameObject
            reddot:SetActive(false)
            if index.section<self.skillCategotyStart and index.type==CellIndexType.Section then
                local text = SafeStringFormat3("%s(%s)",self.expressionActions.name,self.expressionActions.actionCount)
                label.text = text
                return
            end

            if index.type == CellIndexType.Section and index.section>=self.skillCategotyStart and index.section< #self.categories+self.skillCategotyStart then
                local category = self.categories[index.section-self.skillCategotyStart+1]
                label.text = SafeStringFormat3("%s(%s)",category.name,#category.skills)
            elseif index.type == CellIndexType.Section and index.section == self.changshengjueSectionId then
                label.text = LocalString.GetString("宗派技能(1)")
            elseif index.type == CellIndexType.Row and index.section == self.changshengjueSectionId then
                local changshengjueSkill = Skill_AllSkills.GetData(self.changshengjueSkillId)
                label.text = changshengjueSkill.Name
            elseif index.type == CellIndexType.Section and index.section == self.xianzhiSectionId then
                label.text = LocalString.GetString("仙职技能(1)")
                if CGuideMgr.Inst:IsInPhase(EnumGuideKey.XianZhiSkill) then
                    reddot:SetActive(true)
                end
                self.m_XianZhiSkillTab = cell
            elseif index.type == CellIndexType.Row and index.section == self.xianzhiSectionId then
                local xianZhiSkill = Skill_AllSkills.GetData(self.xianzhiSkillId)
                label.text = xianZhiSkill.Name
                if CGuideMgr.Inst:IsInPhase(EnumGuideKey.XianZhiSkill) then
                    reddot:SetActive(true)
                end
            elseif index.type == CellIndexType.Section and index.section == self.fishingSectionId then
                local text = LocalString.GetString("海钓技能")
                label.text = text
                return
            else
                local category = self.categories[index.section-self.skillCategotyStart+1]
                local skill = __Skill_AllSkills_Template.GetData(category.skills[index.row+1].skillClassId)
                label.text = skill and skill.Name or ""
            end
        end,
        function(index,expanded)
            if index.type == CellIndexType.Section then
                if index.section < self.skillCategotyStart then
                    self:OnExpressionActionSeleted()
                elseif index.section == self.fishingSectionId then
                    self:OnFishingActionSelected()
                end
            end
        end,
        function(index)
            if index.section>=self.skillCategotyStart and index.section<#self.categories+self.skillCategotyStart then
                local c = self.categories[index.section-self.skillCategotyStart+1]
                if index.row>=0 and index.row<#c.skills then
                    self:OnSkillSelected(c.skills[index.row+1].id)
                    return
                end
            end
            if index.section==self.changshengjueSectionId then
                self:OnChangShengJueSkillSelected(self.changshengjueSkillId)
            end
            if index.section==self.xianzhiSectionId then
                self:OnXianZhiActionSelected(self.xianzhiSkill)
            end
        end
    )
end

function LuaOtherSkillView:Init()
    self.categories = {}
    self.cateroryDict = {}
    if CClientMainPlayer.Inst then
        Skill_OtherSkill.Foreach(function(key,data)
            if data.IsShow_Unlearn==0 or CClientMainPlayer.Inst.SkillProp:IsOriginalSkillClsExist(data.SkillID) then
                if not self.cateroryDict[data.Category] then
                    local category = {
                        id = data.Category,
                        name = Skill_OtherSkillType.GetData(data.Category).CategoryName,
                        skills = {},
                    }
                    self.cateroryDict[data.Category] = category
                    table.insert( self.categories, category )
                end
                local skills = self.cateroryDict[data.Category].skills
                table.insert( skills,{
                    id = data.ID,
                    skillClassId = data.SkillID,
                    order = data.Order
                } )

            end
        end)
        table.sort( self.categories, function(a,b)
            return a.id<b.id         
        end )
        for i,v in ipairs(self.categories) do
            local skills = v.skills
            table.sort( skills,function(a,b)
                if a.order~=b.order then
                    return a.order<b.order
                else
                    return a.id<b.id
                end
            end )
        end

        local actionCount = 0
        if LuaOtherSkillView.expressionActionOpen then
            if CExpressionActionMgr.Inst.OtherSkillInfos.Count == 0 then
                CExpressionActionMgr.Inst:InitData()
            end

            local infosDict = CExpressionActionMgr.Inst.OtherSkillInfos
            CommonDefs.DictIterate(infosDict, DelegateFactory.Action_object_object(function(key,val)
                if val:GetDisplay() and val:CanBeUsed() then
                    actionCount = actionCount + 1
                end
            end))
        end
        self.expressionActions = {
            name = LocalString.GetString("表情动作"),
            actionCount = actionCount,
        }
    end

    self.changshengjueSkillId = SoulCore_Settings.GetData().HpSkills * 100 + 1
    self.xianzhiSkillId = CLuaXianzhiMgr.GetXianZhiSKillId()
    if self.xianzhiSkillId == 0 then
        -- 没有仙职技能 就采用默认的
        self.xianzhiSkillId = CLuaDesignMgr.Skill_AllSkills_GetSkillId(XianZhi_Setting.GetData().FakeSkillId, 1)
    end

    -- HMT和VN屏蔽仙职技能
    if CommonDefs.IS_VN_CLIENT or CommonDefs.IS_HMT_CLIENT then 
        local startId = #self.categories + self.skillCategotyStart
        self.changshengjueSectionId = startId
        self.fishingSectionId = startId + 1
    else 
        local startId = #self.categories + self.skillCategotyStart
        self.changshengjueSectionId = startId
        -- 仙职显示在otherSkill之后
        self.xianzhiSectionId = startId + 1
        --钓鱼技能显示在仙职之后
        self.fishingSectionId = startId + 2
    end

    self.m_MasterView:LoadData(CellIndex(0, 0, CellIndexType.Section), true)

end

function LuaOtherSkillView:OnEnable()
    self:Init()
    g_ScriptEvent:AddListener("UpdateRemainXianZhiTime", self, "OnUpdateXianZhi")
	g_ScriptEvent:AddListener("LevelUpXianZhiSuccess", self, "OnUpdateXianZhi")
    g_ScriptEvent:AddListener("GuideEnd", self, "OnGuideEnd")
end

function LuaOtherSkillView:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateRemainXianZhiTime", self, "OnUpdateXianZhi")
	g_ScriptEvent:RemoveListener("LevelUpXianZhiSuccess", self, "OnUpdateXianZhi")
    g_ScriptEvent:RemoveListener("GuideEnd", self, "OnGuideEnd")
end

function LuaOtherSkillView:OnGuideEnd(args)
    local phase = args[0]
    if phase == EnumGuideKey.XianZhiSkill then

        local parent = self.m_MasterView.transform:Find("Scroll View/Table")
        for i=1,parent.childCount do
            local child = parent:GetChild(i-1)
            local reddot = child:Find("RedDot").gameObject
            reddot:SetActive(false)
        end
    end
end

function LuaOtherSkillView:OnSkillSelected(otherSkill)
    self.m_FishingActionView.gameObject:SetActive(false)
    self.m_ExpressionActionView.gameObject:SetActive(false)
    self.m_DetailView.gameObject:SetActive(true)
    self.m_ZongPaiSkillView.gameObject:SetActive(false)
    self.m_DetailView:Init(otherSkill)
end

function LuaOtherSkillView:OnExpressionActionSeleted()
    self.m_FishingActionView.gameObject:SetActive(false)
    self.m_DetailView.gameObject:SetActive(false)
    self.m_ExpressionActionView.gameObject:SetActive(true)
    self.m_ZongPaiSkillView.gameObject:SetActive(false)
    self.m_ExpressionActionView:Init()
end

function LuaOtherSkillView:OnXianZhiActionSelected(xianzhiSkill)
    Gac2Gas.RequestOpenXianZhiWnd()
    self.m_ExpressionActionView.gameObject:SetActive(false)
    self.m_FishingActionView.gameObject:SetActive(false)
    self.m_DetailView.gameObject:SetActive(true)
    self.m_ZongPaiSkillView.gameObject:SetActive(false)
    if xianzhiSkill and xianzhiSkill>0 then
        self.m_DetailView:InitXianZhiSkill(xianzhiSkill)
    end
end

function LuaOtherSkillView:OnFishingActionSelected()
    self.m_DetailView.gameObject:SetActive(false)
    self.m_ExpressionActionView.gameObject:SetActive(false)
    self.m_FishingActionView.gameObject:SetActive(true)
    self.m_ZongPaiSkillView.gameObject:SetActive(false)
    self.m_FishingActionView.m_LuaSelf:Init()
end

function LuaOtherSkillView:OnUpdateXianZhi()
    local xianzhiSkillId = CLuaXianzhiMgr.GetXianZhiSKillId()
    if xianzhiSkillId == 0 then
        -- 没有仙职技能 就采用默认的
        xianzhiSkillId = CLuaDesignMgr.Skill_AllSkills_GetSkillId(XianZhi_Setting.GetData().FakeSkillId, 1)
    end
    self.m_DetailView:InitXianZhiSkill(xianzhiSkillId)
end

function LuaOtherSkillView:GetXianZhiSkillTab()
    return self.m_XianZhiSkillTab
end

function LuaOtherSkillView:GetSeaFishingSkillButton()
    local cellIndex = CellIndex(self.fishingSectionId, 0, CellIndexType.Section)
    local cell = self.m_MasterView:GetCellByIndex(cellIndex)
    if cell then
        return cell
    end
    return nil
end

function LuaOtherSkillView:OnChangShengJueSkillSelected(changShengJueSkill)
    self.m_ExpressionActionView.gameObject:SetActive(false)
    self.m_FishingActionView.gameObject:SetActive(false)
    self.m_DetailView.gameObject:SetActive(false)
    self.m_ZongPaiSkillView.gameObject:SetActive(true)
end