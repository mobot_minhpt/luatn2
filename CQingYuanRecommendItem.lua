-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CQingYuanRecommendItem = import "L10.UI.CQingYuanRecommendItem"
local EnumClass = import "L10.Game.EnumClass"
local L10 = import "L10"
local Profession = import "L10.Game.Profession"
CQingYuanRecommendItem.m_Init_CS2LuaHook = function (this, info) 
    local portraitName = L10.UI.CUICommonDef.GetPortraitName(info.cls, info.gender, -1)
    this.portrait:LoadNPCPortrait(portraitName, false)

    this.nameLabel.text = info.name
    this.levelLabel.text = "lv." .. tostring(info.level)
    this.clsSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), info.cls))
end
