local UITexture = import "UITexture"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local TweenPosition = import "TweenPosition"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"

LuaZNQZhiBoYaoQingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZNQZhiBoYaoQingWnd, "DesLabel", "DesLabel", UILabel)
RegistChildComponent(LuaZNQZhiBoYaoQingWnd, "LeftTimeLabel", "LeftTimeLabel", UILabel)
RegistChildComponent(LuaZNQZhiBoYaoQingWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaZNQZhiBoYaoQingWnd, "AcceptBtnLabel", "AcceptBtnLabel", UILabel)
RegistChildComponent(LuaZNQZhiBoYaoQingWnd, "AcceptBtn", "AcceptBtn", CButton)
RegistChildComponent(LuaZNQZhiBoYaoQingWnd, "InfoNode", "InfoNode", GameObject)
RegistChildComponent(LuaZNQZhiBoYaoQingWnd, "ZuoQiTexture", "ZuoQiTexture", GameObject)
RegistChildComponent(LuaZNQZhiBoYaoQingWnd, "TitleLabel", "TitleLabel", GameObject)
RegistChildComponent(LuaZNQZhiBoYaoQingWnd, "Bg", "Bg", UITexture)
RegistChildComponent(LuaZNQZhiBoYaoQingWnd, "ZuoQiRoot", "ZuoQiRoot", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaZNQZhiBoYaoQingWnd, "m_CountDownTick")
RegistClassMember(LuaZNQZhiBoYaoQingWnd, "m_TimeStamp")
RegistClassMember(LuaZNQZhiBoYaoQingWnd, "m_LeftTime")

function LuaZNQZhiBoYaoQingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.AcceptBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAcceptBtnClick()
	end)


    --@endregion EventBind end
end

function LuaZNQZhiBoYaoQingWnd:Init()
	if LuaZhouNianQing2021Mgr.yaoQingHanInst == true then return end

	-- 等待展开特效结束时显示UI内容
	self.InfoNode:SetActive(false)

	self:StartOpenAnim()
	local tween = self.transform:Find("WndBg/Right"):GetComponent(typeof(TweenPosition))
	CommonDefs.AddEventDelegate(tween.onFinished, DelegateFactory.Action(function ()
		self:ShowContent()
	end))

	-- 获取直播时间戳
	if self.m_TimeStamp == nil then
		local zhiBoTime = ZhouNianQing_ZhiBoSettings.GetData().ZhiBoStartTime
		self.m_TimeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(zhiBoTime)
	end
	-- 更新展示信息
	self:UpdateInfo()

	LuaZhouNianQing2021Mgr.yaoQingHanInst = true
end


function LuaZNQZhiBoYaoQingWnd:StartOpenAnim()
	self.Bg.gameObject:SetActive(true)

	self.Bg.fillAmount = 0.25

	local interval = 10 -- 间隔
	local showTime = 800 -- 动画时间

	local tick = nil
	tick = RegisterTickWithDuration(function ()
		if self.Bg.fillAmount >= 0.98 then
			UnRegisterTick(tick)
			return
		end

		if self.Bg.fillAmount <= (1 - interval/showTime) then
			self.Bg.fillAmount = self.Bg.fillAmount + interval/showTime
		end

	end, interval, showTime)

end

function LuaZNQZhiBoYaoQingWnd:OnDisable( )
end

function LuaZNQZhiBoYaoQingWnd:UpdateInfo()
	self:UpdateCountDown()
	if self.m_CountDownTick then 
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end

	self.m_CountDownTick = RegisterTick(function() self:UpdateCountDown() end, 1000)


end

--@region UIEvent

function LuaZNQZhiBoYaoQingWnd:OnAcceptBtnClick()
	if self.m_LeftTime == nil then return end
	-- 直播开始前预约，记录报名日志
	if self.m_LeftTime > 0 then
		Gac2Gas.ZNQZhiBo_Appointment()
		Gac2Gas.TongQingYouLi_Appointment()
	-- 直播开始后点击，玩家进入直播现场
	else
		Gac2Gas.ZNQZhiBo_RequestTeleport()
		CUIManager.CloseUI(CLuaUIResources.ZNQZhiBoYaoQingWnd)	--关闭UI界面
	end

end

--@endregion UIEvent

-- 显示UI内容
function LuaZNQZhiBoYaoQingWnd:ShowContent()
	self.InfoNode:SetActive(true)
end

function LuaZNQZhiBoYaoQingWnd:UpdateCountDown()
	self.m_LeftTime = math.floor(self.m_TimeStamp - CServerTimeMgr.Inst.timeStamp)

	-- 直播未开始
	if self.m_LeftTime and self.m_LeftTime > 0 then
		-- 更新按钮状态
		local isAppointment = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eZNQZhiBo_Appointment)
		if isAppointment  ~= 0 and LuaZhouNianQing2021Mgr.Appointment ~= 0 then
			self.AcceptBtn.Enabled = false
			self.AcceptBtnLabel.text = LocalString.GetString("已接受邀请")
		else
			self.AcceptBtn.Enabled = true
			self.AcceptBtnLabel.text = LocalString.GetString("接受邀请")
		end
		-- 更新label信息
		local msg = g_MessageMgr:FormatMessage("ZNQ_ZHIBO_RESERVE_MSG") 
		if msg then
			self.DesLabel.text = msg
		end
		-- 更新时间
		local day = math.floor( self.m_LeftTime / (3600 * 24) )
		local hour = math.floor((self.m_LeftTime - day * 3600 * 24) / 3600)
		local minute = math.floor((self.m_LeftTime - day * 3600 * 24 - hour * 3600) / 60)
		local second = (self.m_LeftTime - day * 3600 * 24 - hour * 3600 - minute * 60)
		self.LeftTimeLabel.text = LocalString.GetString("直播开启倒计时:")
		if day > 0 then
			self.TimeLabel.text = SafeStringFormat3( LocalString.GetString("%d天%d小时%d分"), day, hour, minute)
		elseif hour > 0 then
			self.TimeLabel.text = SafeStringFormat3( LocalString.GetString("%d小时%d分%d秒"), hour, minute, second)
		elseif minute > 0 then
			self.TimeLabel.text = SafeStringFormat3( LocalString.GetString("%d分%d秒"), minute, second)
		elseif second > 0 then
			self.TimeLabel.text = SafeStringFormat3( LocalString.GetString("%d秒"), second)
		end
	end
	-- 直播已经开始
	if self.m_LeftTime and self.m_LeftTime <= 0 then
		-- 更新label信息
		local msg = g_MessageMgr:FormatMessage("ZNQ_ZHIBO_INVITE_MSG")
		if msg then
			self.DesLabel.text = msg
		end
		-- 更新按钮状态
		self.AcceptBtn.Enabled = true
		self.AcceptBtnLabel.text = LocalString.GetString("前往观看")
		self.LeftTimeLabel.text = LocalString.GetString("")
		self.TimeLabel.text = LocalString.GetString("")
	end
		
end

function LuaZNQZhiBoYaoQingWnd:OnDestroy()
	LuaZhouNianQing2021Mgr.yaoQingHanInst = false
	if self.m_CountDownTick then
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end
end
