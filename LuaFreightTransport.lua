local MessageWndManager = import "L10.UI.MessageWndManager"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local EnumQualityType=import "L10.Game.EnumQualityType"
local CItemMgr=import "L10.Game.CItemMgr"
-- local CTransportAwardItemCell = import "L10.UI.CTransportAwardItemCell"
local CTaskMgr = import "L10.Game.CTaskMgr"
-- local CFreightTransport = import "L10.UI.CFreightTransport"
local CFreightMgr = import "L10.Game.CFreightMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CButton = import "L10.UI.CButton"
local CTooltip=import "L10.UI.CTooltip"

CLuaFreightTransport = class()
RegistClassMember(CLuaFreightTransport,"awardTable")
RegistClassMember(CLuaFreightTransport,"itemCell")
RegistClassMember(CLuaFreightTransport,"tipBtn")
RegistClassMember(CLuaFreightTransport,"consignBtn")
RegistClassMember(CLuaFreightTransport,"alert")

function CLuaFreightTransport:Awake()
    self.awardTable = self.transform:Find("AwardTable"):GetComponent(typeof(UITable))
    self.itemCell = self.transform:Find("ItemCell").gameObject
    self.tipBtn = self.transform:Find("Tips").gameObject
    self.consignBtn = self.transform:Find("Consign").gameObject
    self.alert = self.transform:Find("Consign/Alert").gameObject

    UIEventListener.Get(self.tipBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnTipsClicked(go)
    end)
    UIEventListener.Get(self.consignBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnConsignBtnClick(go)
    end)

end

function CLuaFreightTransport:InitAwardItem(tf,ID,count)
    local icon = tf:Find("Icon"):GetComponent(typeof(CUITexture))
    local amount = tf:Find("Amount"):GetComponent(typeof(UILabel))
    local quality = tf:Find("Quality"):GetComponent(typeof(UISprite))

    local equip = nil
    local item = CItemMgr.Inst:GetItemTemplate(ID)
    if item then
        equip = GuildFreight_Equipments.GetData(ID)
    end
    amount.text = count == 0 and "" or tostring(count)
    if item then
        icon:LoadMaterial(item.Icon)
        if quality then
            quality.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
        end
    elseif equip then
        icon:LoadMaterial(equip.Icon)
        if quality then
            quality.spriteName = CUICommonDef.GetItemCellBorder(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), equip.Color))
        end
    end
    UIEventListener.Get(tf.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(ID)
    end)
end

function CLuaFreightTransport:Init( )
    Extensions.RemoveAllChildren(self.awardTable.transform)
    self.itemCell:SetActive(false)
    do
        local i = 0
        while i < CFreightMgr.Inst.awardList.Count do
            local baseInfo = CFreightMgr.Inst.awardList[i]
            local item = NGUITools.AddChild(self.awardTable.gameObject, self.itemCell)
            -- CommonDefs.GetComponent_GameObject_Type(item, typeof(CTransportAwardItemCell)):Init(baseInfo.templateId, baseInfo.amount)
            self:InitAwardItem(item.transform,baseInfo.templateId, baseInfo.amount)
            item:SetActive(true)
            i = i + 1
        end
    end
    self.awardTable:Reposition()
    if CClientMainPlayer.Inst.Id == CFreightMgr.Inst.playerId and CFreightMgr.Inst.endTime > CFreightMgr.Inst.currentTime then
        local freightSettings = GuildFreight_GameSetting.GetData()
        local fillComplete = ((CFreightMgr.Inst:FilledCount() + CFreightMgr.Inst:FilledByHelpCount()) >= freightSettings.FillBoxCountCanSubmitTask)
        --setoutBtn.GetComponent<CButton>().Enabled = fillComplete;
        CommonDefs.GetComponent_GameObject_Type(self.consignBtn, typeof(CButton)).Enabled = fillComplete
        --requestHelpBtn.GetComponent<CButton>().Enabled = true;
        self.alert:SetActive(fillComplete)
    else
        -- setoutBtn.GetComponent<CButton>().Enabled = false;
        CommonDefs.GetComponent_GameObject_Type(self.consignBtn, typeof(CButton)).Enabled = false
        self.alert:SetActive(false)
        --requestHelpBtn.GetComponent<CButton>().Enabled = false;
    end
end
function CLuaFreightTransport:OnTipsClicked(go)
    CTooltip.Show(GuildFreight_GameSetting.GetData().Tipstwo, self.tipBtn.transform, CTooltip.AlignType.Top)
end
function CLuaFreightTransport:OnEnable( )
    --UIEventListener.Get(requestHelpBtn).onClick += self.OnRequestHelpBtnClick;
    --UIEventListener.Get(setoutBtn).onClick += self.OnSetoutBtnClick;

    -- EventManager.AddListenerInternal(EnumEventType.OnFillCargoSuccess, < MakeDelegateFromCSFunction >(self.OnFilledCargoSuccess, MakeGenericClass(Action1, Int32), self))
    g_ScriptEvent:AddListener("OnFillCargoSuccess", self, "OnFilledCargoSuccess")
end
function CLuaFreightTransport:OnDisable( )
    --UIEventListener.Get(requestHelpBtn).onClick -= self.OnRequestHelpBtnClick;
    -- UIEventListener.Get(self.consignBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(self.consignBtn).onClick, < MakeDelegateFromCSFunction >(self.OnConsignBtnClick, VoidDelegate, self), false)
    -- UIEventListener.Get(self.tipBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(self.tipBtn).onClick, < MakeDelegateFromCSFunction >(self.OnTipsClicked, VoidDelegate, self), false)
    --UIEventListener.Get(setoutBtn).onClick -= self.OnSetoutBtnClick;

    -- EventManager.RemoveListenerInternal(EnumEventType.OnFillCargoSuccess, < MakeDelegateFromCSFunction >(self.OnFilledCargoSuccess, MakeGenericClass(Action1, Int32), self))
    g_ScriptEvent:RemoveListener("OnFillCargoSuccess", self, "OnFilledCargoSuccess")
end
function CLuaFreightTransport:OnConsignBtnClick( go) 
    --托运，交任务
    if CTaskMgr.Inst:IsTrackingToDoTask(CFreightMgr.Inst.taskId) then
        return
    end
    if ((CFreightMgr.Inst:FilledCount() + CFreightMgr.Inst:FilledByHelpCount()) < CFreightMgr.Inst.cargoList.Count) then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Complete_Packing_Or_Not"), DelegateFactory.Action(function () 
            CTaskMgr.Inst:TrackToDoTask(CFreightMgr.Inst.taskId)
            CUIManager.CloseUI(CUIResources.FreightWnd)
        end), nil, nil, nil, false)
    else
        CTaskMgr.Inst:TrackToDoTask(CFreightMgr.Inst.taskId)
        CUIManager.CloseUI(CUIResources.FreightWnd)
    end
end
function CLuaFreightTransport:OnFilledCargoSuccess( args) 
    local index = args[0]
    if CClientMainPlayer.Inst.Id == CFreightMgr.Inst.playerId then
        local fillComplete = ((CFreightMgr.Inst:FilledCount() + CFreightMgr.Inst:FilledByHelpCount()) >= GuildFreight_GameSetting.GetData().FillBoxCountCanSubmitTask)
        --setoutBtn.GetComponent<CButton>().Enabled = fillComplete;
        CommonDefs.GetComponent_GameObject_Type(self.consignBtn, typeof(CButton)).Enabled = fillComplete
        self.alert:SetActive(fillComplete)
    end
end

