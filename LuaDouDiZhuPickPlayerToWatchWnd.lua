

CLuaDouDiZhuPickPlayerToWatchWnd=class()


function CLuaDouDiZhuPickPlayerToWatchWnd:Init()
    
    local button1=FindChild(self.transform,"Button1").gameObject
    local button2=FindChild(self.transform,"Button2").gameObject
    local button3=FindChild(self.transform,"Button3").gameObject
    local buttons={button1,button2,button3}

    for i,v in ipairs(CLuaDouDiZhuMgr.m_WatchPlayers) do
        buttons[i].transform:Find("Label"):GetComponent(typeof(UILabel)).text=v[3]

        UIEventListener.Get(buttons[i]).onClick=LuaUtils.VoidDelegate(function(go)
            Gac2Gas.RequestWatchPlayerDouDiZhu(v[2])
            CUIManager.CloseUI(CUIResources.DouDiZhuPickPlayerToWatchWnd)
        end)
    end

end


return CLuaDouDiZhuPickPlayerToWatchWnd
