local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CChatHelper = import "L10.UI.CChatHelper"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local Animation = import "UnityEngine.Animation"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
LuaYaYunHui2023QuestionWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaYaYunHui2023QuestionWnd, "TabTemplate", "TabTemplate", GameObject)
RegistChildComponent(LuaYaYunHui2023QuestionWnd, "Table", "Table", GameObject)
RegistChildComponent(LuaYaYunHui2023QuestionWnd, "Question", "Question", UILabel)
RegistChildComponent(LuaYaYunHui2023QuestionWnd, "Describe", "Describe", UILabel)
RegistChildComponent(LuaYaYunHui2023QuestionWnd, "TemplateBtn", "TemplateBtn", GameObject)
RegistChildComponent(LuaYaYunHui2023QuestionWnd, "Grid", "Grid", GameObject)
RegistChildComponent(LuaYaYunHui2023QuestionWnd, "Sharebtn", "Sharebtn", GameObject)
RegistChildComponent(LuaYaYunHui2023QuestionWnd, "RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaYaYunHui2023QuestionWnd, "ItemLink", "ItemLink", UILabel)
RegistChildComponent(LuaYaYunHui2023QuestionWnd, "ItemLink1", "ItemLink1", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaYaYunHui2023QuestionWnd, "m_QuestionList")
RegistClassMember(LuaYaYunHui2023QuestionWnd, "m_SelectInfoList")
RegistClassMember(LuaYaYunHui2023QuestionWnd, "m_AnswerViewList")
RegistClassMember(LuaYaYunHui2023QuestionWnd, "m_TabViewList")
RegistClassMember(LuaYaYunHui2023QuestionWnd, "m_TotalOpenDay")
RegistClassMember(LuaYaYunHui2023QuestionWnd, "m_CurTabSelect")
RegistClassMember(LuaYaYunHui2023QuestionWnd, "m_AnswerData")
RegistClassMember(LuaYaYunHui2023QuestionWnd, "m_CurQuestionDesInfo")
RegistClassMember(LuaYaYunHui2023QuestionWnd, "m_Ani")
function LuaYaYunHui2023QuestionWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.TabTemplate.gameObject:SetActive(false)
    self.TemplateBtn.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.Table.transform)
    Extensions.RemoveAllChildren(self.Grid.transform)
    self.m_Ani = self.transform:GetComponent(typeof(Animation)) 
    self.Question.text = nil
    self.Describe.text = nil

    self.m_SelectInfoList = nil
    self.m_QuestionList = nil
    self.m_TabViewList = nil
    self.m_AnswerViewList = nil
    self.m_TotalOpenDay = nil
    self.m_CurTabSelect = 0
    self.m_AnswerData = nil
    self.m_CurQuestionDesInfo = nil
end

function LuaYaYunHui2023QuestionWnd:Init()
    Gac2Gas.AsianGames2023RequestQuestions()
    self:InitBottom()
end

function LuaYaYunHui2023QuestionWnd:InitBottom()
    local itemList = AsianGames2023_Setting.GetData().QuestionRewardTemplateIds
    local id1 = itemList[0]
    local id2 = itemList[1]
    local id1Data = Item_Item.GetData(id1)
    local id2Data = Item_Item.GetData(id2)
    local icon1 = self.transform:Find("Anchor/QuestionView/Right/BottomView/Texture/AwardView").gameObject
    local icon2 = self.transform:Find("Anchor/QuestionView/Right/BottomView/AwardView1").gameObject
    if id1Data then
        --self.ItemLink.text = SafeStringFormat3("[%s]",id1Data.Name)
        self.ItemLink.gameObject:SetActive(false)
        UIEventListener.Get(icon1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(id1, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
        end)
    end
    if id2Data then
        --self.ItemLink1.text = SafeStringFormat3("[%s]",id2Data.Name)
        self.ItemLink1.gameObject:SetActive(false)
        UIEventListener.Get(icon2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(id2, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
        end)
    end
    UIEventListener.Get(self.Sharebtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnShareBtnClick()
    end)
    UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        g_MessageMgr:ShowMessage("YaYunHui2023_QuestionRule")
    end)
end

function LuaYaYunHui2023QuestionWnd:OnShareBtnClick()
    local popupList={}
    table.insert( popupList,PopupMenuItemData(LocalString.GetString("分享至世界"), DelegateFactory.Action_int(function(index) 
        self:ShareQuestion(EChatPanel.World)
    end),false, nil, EnumPopupMenuItemStyle.Default))
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst:IsInGuild() then
        table.insert( popupList,PopupMenuItemData(LocalString.GetString("分享至帮会"), DelegateFactory.Action_int(function(index) 
            self:ShareQuestion(EChatPanel.Guild)
        end),false, nil, EnumPopupMenuItemStyle.Default))
    end
    if CTeamMgr.Inst:TeamExists() then
        table.insert( popupList,PopupMenuItemData(LocalString.GetString("分享至队伍"), DelegateFactory.Action_int(function(index) 
            self:ShareQuestion(EChatPanel.Team)
        end),false, nil, EnumPopupMenuItemStyle.Default))
    end
    local array=Table2Array(popupList,MakeArrayClass(PopupMenuItemData))
    local side = CPopupMenuInfoMgr.AlignType.Right
    CPopupMenuInfoMgr.ShowPopupMenu(array,self.Sharebtn.transform, side, 1, nil, 600, true, 220)
end
function LuaYaYunHui2023QuestionWnd:ShareQuestion(channel)
    if not self.m_CurQuestionDesInfo then return end
    local optionLabelArray = {"A.","B.","C.","D."}
    local optionText = ""
    for i = 1,#self.m_CurQuestionDesInfo.answerList do
        local optionLabel = optionLabelArray[i]
        local answer = self.m_CurQuestionDesInfo.answerList[i]
        optionLabel = optionLabel .. answer
        optionText = optionText .. ((i > 1) and " " or "") .. optionLabel
    end
    local questionText = self.m_CurQuestionDesInfo.question
    local link = g_MessageMgr:FormatMessage("YaYunHui2023_Share_Question", questionText, optionText)
    CChatHelper.SendMsgWithFilterOption(channel,link,0, true)
    CSocialWndMgr.ShowChatWnd()
end


function LuaYaYunHui2023QuestionWnd:OnAsianGames2023RequestQuestionsResult(questionCurDay,questionsCache,questionPlayerAnswersCache)
    self.m_TotalOpenDay = questionCurDay

    self.m_QuestionList = {}
    for i = 1,#questionsCache,7 do
        local id = questionsCache[i]
        self.m_QuestionList[id] = {
            questionDes = questionsCache[i + 1],
            option1 = questionsCache[i + 2],
            option2 = questionsCache[i + 3],
            option3 = questionsCache[i + 4],
            option4 = questionsCache[i + 5],
            analyze = questionsCache[i + 6],
        } 
    end

    self.m_SelectInfoList = {}
    for i = 1,#questionPlayerAnswersCache do
        self.m_SelectInfoList[i] = questionPlayerAnswersCache[i]
    end
    self:GetPlayerAnswerData()
    if not self.m_TabViewList then
        self:InitTabView()
    end
    for i = self.m_TotalOpenDay,1,-1 do
        if not self.m_AnswerData or self.m_AnswerData[i] == 0 then
            self:OnSelectTab(i)
            local scrollView = self.transform:Find("Anchor/QuestionView/Left/ScrollView"):GetComponent(typeof(CUIRestrictScrollView))
            CUICommonDef.SetFullyVisible(self.m_TabViewList[i].highlightView.gameObject,scrollView)  
            return
        end
    end
    self:OnSelectTab(questionCurDay)
end

function LuaYaYunHui2023QuestionWnd:InitTabView()
    self.m_TabViewList = {}
    Extensions.RemoveAllChildren(self.Table.transform)
    local startTimeStr = AsianGames2023_Setting.GetData().QuestionStartTime
    local year,month,day,_ , _ = string.match(startTimeStr,"(%d+)-(%d+)-(%d+) (%d+):(%d+)")
    for i = self.m_TotalOpenDay,1,-1 do
        local tab = CUICommonDef.AddChild(self.Table.gameObject,self.TabTemplate.gameObject)
        tab.gameObject:SetActive(true)
        local highlight = tab.transform:Find("Highlight").gameObject
        local normal = tab.transform:Find("Normal").gameObject
        highlight.gameObject:SetActive(false)
        normal.gameObject:SetActive(true)
        local curDay = day + i - 1
        local curMonth = month
        if curDay > 30 then
            curDay = curDay - 30
            curMonth = month + 1
        end 
        local dateStr = SafeStringFormat3(LocalString.GetString("%d月%d日"),curMonth,curDay)
        highlight.transform:Find("Label"):GetComponent(typeof(UILabel)).text = dateStr
        normal.transform:Find("Label"):GetComponent(typeof(UILabel)).text = dateStr
        local normalRedDot = normal.transform:Find("Sprite").gameObject
        local highlightRedDot = highlight.transform:Find("Sprite").gameObject
        local animation = tab.transform:GetComponent(typeof(Animation))
        normalRedDot.gameObject:SetActive(not self.m_AnswerData or self.m_AnswerData[i] == 0)
        highlightRedDot.gameObject:SetActive(not self.m_AnswerData or self.m_AnswerData[i] == 0)
        self.m_TabViewList[i] = {
            id = i,
            normalView = normal,
            highlightView = highlight,
            nRedDot = normalRedDot,
            sRedDot = highlightRedDot,
            ani = animation,
        }
        local index = i
        UIEventListener.Get(tab.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnSelectTab(index)
        end)
    end
    self.Table:GetComponent(typeof(UITable)):Reposition()
end

function LuaYaYunHui2023QuestionWnd:OnSelectTab(index)
    if self.m_Ani and self.m_CurTabSelect ~= index then
        self.m_Ani:Stop()
        self.m_Ani:Play("yayunhui2023questionwnd_tab")
    end
    if not self.m_TabViewList then return end
    self.m_CurQuestionDesInfo = {}
    if self.m_TabViewList[self.m_CurTabSelect] then
        self.m_TabViewList[self.m_CurTabSelect].ani:Stop()
        self.m_TabViewList[self.m_CurTabSelect].normalView.gameObject:SetActive(true)
        self.m_TabViewList[self.m_CurTabSelect].highlightView.gameObject:SetActive(false)
    end
    if self.m_TabViewList[index] then
        self.m_TabViewList[index].ani:Stop()
        self.m_TabViewList[index].ani:Play("yayunhui2023questionwnd_tab_riqi")
        self.m_TabViewList[index].normalView.gameObject:SetActive(false)
        self.m_TabViewList[index].highlightView.gameObject:SetActive(true)
        
        local Question = ""
        local analyze = ""
        local answerList = {}

        if self.m_QuestionList and self.m_QuestionList[index] then
            Question = self.m_QuestionList[index].questionDes
            analyze = self.m_QuestionList[index].analyze
            answerList = {self.m_QuestionList[index].option1,self.m_QuestionList[index].option2,self.m_QuestionList[index].option3,self.m_QuestionList[index].option4}
        else
            local data = AsianGames2023_Questions.GetData(index)
            if data then
                Question = data.QuestionDescription
                analyze = data.QuestionAnalyze
                answerList = {data.OptionOneDescription,data.OptionTwoDescription,data.OptionThreeDescription,data.OptionFourDescription}
            end
        end
        self.m_CurQuestionDesInfo.question = Question
        self.m_CurQuestionDesInfo.answerList = answerList
        self.Question.text = Question
        self.Describe.text = analyze
        self.Describe.alpha = (self.m_AnswerData and self.m_AnswerData[index] ~= 0) and 1 or 0
        self:SetAnswerView(index,answerList)
    end
    self.m_CurTabSelect = index
end

function LuaYaYunHui2023QuestionWnd:SetAnswerView(index,answerList)
    if not self.m_AnswerViewList then
        self.m_AnswerViewList = {}
        for i= 1,4 do
            local answerView = CUICommonDef.AddChild(self.Grid.gameObject,self.TemplateBtn.gameObject)
            answerView.gameObject:SetActive(true)
            local desLabel = answerView.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
            local progressBar = answerView.transform:Find("ProgressBar"):GetComponent(typeof(UISlider))
            local rightView = answerView.transform:Find("Res/right02").gameObject
            local wrongView = answerView.transform:Find("Res/Wrong02").gameObject
            local right01View = answerView.transform:Find("Res/right").gameObject
            local wrong01View = answerView.transform:Find("Res/Wrong").gameObject
            local selectBg = answerView.transform:Find("SelectBg").gameObject
            selectBg.gameObject:SetActive(false)
            -- rightView.gameObject:SetActive(false)
            -- wrongView.gameObject:SetActive(false)
            local Res = answerView.transform:Find("Res").gameObject
            Res.gameObject:SetActive(false)
            local animation = answerView.transform:GetComponent(typeof(Animation))
            progressBar.gameObject:SetActive(false)
            local cuifx = answerView.transform:Find("Res/CUIFx").gameObject
            cuifx.gameObject:SetActive(false)
            self.m_AnswerViewList[i] = {
                view = answerView,
                label = desLabel,
                progress = progressBar,
                right02 = rightView,
                wrong02 = wrongView,
                right = right01View,
                wrong = wrong01View,
                ResView = Res,
                anim = animation,
                highlight = selectBg,
                fx = cuifx,
            }
            local index = i
            UIEventListener.Get(answerView.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                self:OnSelectAnswer(index)
            end)
        end
        self.Grid.gameObject:GetComponent(typeof(UIGrid)):Reposition()
    end
    local options = {"A.","B.","C.","D."}
    local Answerdata = self.m_AnswerData and self.m_AnswerData[index] or 0
    local playerSelectAnswer = Answerdata % 10
    local rightAnswer = Answerdata > 10 and math.floor(Answerdata / 10) or playerSelectAnswer
    local selectInfo = self.m_SelectInfoList and self.m_SelectInfoList[index] or {0,0,0,0}
    local totalNum = selectInfo[1] + selectInfo[2] + selectInfo[3] + selectInfo[4]
    for i = 1,#self.m_AnswerViewList do
        local view = self.m_AnswerViewList[i]
        view.label.text =  options[i] .. (answerList[i] or "")
        view.progress.value = totalNum > 0 and selectInfo[i] / totalNum or 0
        view.progress.gameObject:SetActive(Answerdata > 0)
        view.anim:Stop()
        if playerSelectAnswer > 0 and i == rightAnswer then
            view.ResView.gameObject:SetActive(true)
            view.fx.gameObject:SetActive(false)
            if i == playerSelectAnswer and self.m_CurTabSelect == index then
                view.right.gameObject:SetActive(false)
                view.wrong.gameObject:SetActive(false)
                view.anim:Play("yayunhui2023questionwnd_right")
            else
                view.right02.gameObject:SetActive(false)
                view.wrong02.gameObject:SetActive(false)
                view.right.gameObject:SetActive(true)
                view.wrong.gameObject:SetActive(false)
            end
        elseif Answerdata > 10 and i == playerSelectAnswer then
            view.ResView.gameObject:SetActive(true)
            view.fx.gameObject:SetActive(false)
            if self.m_CurTabSelect == index then 
                view.right.gameObject:SetActive(false)
                view.wrong.gameObject:SetActive(false)
                view.anim:Play("yayunhui2023questionwnd_wrong")
            else
                view.right02.gameObject:SetActive(false)
                view.wrong02.gameObject:SetActive(false)
                view.right.gameObject:SetActive(false)
                view.wrong.gameObject:SetActive(true) 
            end
        else
            view.ResView.gameObject:SetActive(false)
        end
        -- view.right.gameObject:SetActive(playerSelectAnswer > 0 and i == rightAnswer)
        -- view.wrong.gameObject:SetActive(Answerdata > 10 and i == playerSelectAnswer)
        view.highlight.gameObject:SetActive(i == playerSelectAnswer)
    end
end

function LuaYaYunHui2023QuestionWnd:OnSelectAnswer(index)
    if self.m_AnswerData and self.m_AnswerData[self.m_CurTabSelect] and self.m_AnswerData[self.m_CurTabSelect] > 0 then return end
    Gac2Gas.AsianGames2023QuestionSubmitAnswer(self.m_CurTabSelect, index)
end


function LuaYaYunHui2023QuestionWnd:OnUpdatePlayDataWithKey(argv)
    if argv[0] ~= EnumTempPlayDataKey_lua.eAsianGames2023Data then return end
	self:GetPlayerAnswerData()
    self:UpdateRedDot()
    self:OnSelectTab(self.m_CurTabSelect)
end

function LuaYaYunHui2023QuestionWnd:UpdateRedDot()
    if not self.m_TabViewList then return end
    for k,v in pairs(self.m_TabViewList) do
        local id = v.id
        v.nRedDot.gameObject:SetActive(not self.m_AnswerData or self.m_AnswerData[id] == 0)
        v.sRedDot.gameObject:SetActive(not self.m_AnswerData or self.m_AnswerData[id] == 0)
    end
end

function LuaYaYunHui2023QuestionWnd:GetPlayerAnswerData()
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eAsianGames2023Data) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eAsianGames2023Data)
        local tab = g_MessagePack.unpack(playDataU.Data.Data)
        self.m_AnswerData = tab and tab[EnumAsianGames2023DataType.eQuestion] or {}
    end
end

--@region UIEvent

--@endregion UIEvent

function LuaYaYunHui2023QuestionWnd:OnEnable()
    g_ScriptEvent:AddListener("AsianGames2023RequestQuestionsResult",self,"OnAsianGames2023RequestQuestionsResult")
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey",self,"OnUpdatePlayDataWithKey")
end


function LuaYaYunHui2023QuestionWnd:OnDisable()
    g_ScriptEvent:RemoveListener("AsianGames2023RequestQuestionsResult",self,"OnAsianGames2023RequestQuestionsResult")
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey",self,"OnUpdatePlayDataWithKey")
end