local TweenRotation = import "TweenRotation"

local UILabel = import "UILabel"

local CUIFx = import "L10.UI.CUIFx"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local EUIModuleGroup = import "L10.UI.CUIManager+EUIModuleGroup"
local UISprite = import "UISprite"
local UITexture = import "UITexture"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local Vector2 = import "UnityEngine.Vector2"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local CItemMgr=import "L10.Game.CItemMgr"
local EnumItemPlace=import "L10.Game.EnumItemPlace"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local TweenAlpha = import "TweenAlpha"
local COpenEntryMgr = import "L10.Game.COpenEntryMgr"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CoreScene=import "L10.Engine.Scene.CoreScene"
local NormalCamera = import "L10.Engine.CameraControl.NormalCamera"

LuaHouseFishingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHouseFishingWnd, "QuitBtn", "QuitBtn", GameObject)
RegistChildComponent(LuaHouseFishingWnd, "CreelBtn", "CreelBtn", GameObject)
RegistChildComponent(LuaHouseFishingWnd, "Tip", "Tip", GameObject)
RegistChildComponent(LuaHouseFishingWnd, "FightingView", "FightingView", GameObject)
RegistChildComponent(LuaHouseFishingWnd, "FishingBtn", "FishingBtn", GameObject)
RegistChildComponent(LuaHouseFishingWnd, "GreenArea", "GreenArea", UITexture)
RegistChildComponent(LuaHouseFishingWnd, "Anchor", "Anchor", GameObject)
RegistChildComponent(LuaHouseFishingWnd, "MoveArea", "MoveArea", UITexture)
RegistChildComponent(LuaHouseFishingWnd, "AddBaitSprite", "AddBaitSprite", GameObject)
RegistChildComponent(LuaHouseFishingWnd, "UIFx", "UIFx", CUIFx)
RegistChildComponent(LuaHouseFishingWnd, "FxNode", "FxNode", GameObject)
RegistChildComponent(LuaHouseFishingWnd, "UIFx2", "UIFx2", CUIFx)
RegistChildComponent(LuaHouseFishingWnd, "BaitIcon", "BaitIcon", CUITexture)
RegistChildComponent(LuaHouseFishingWnd, "GuideView", "GuideView", GameObject)
RegistChildComponent(LuaHouseFishingWnd, "Notice", "Notice", GameObject)
RegistChildComponent(LuaHouseFishingWnd, "NoticeLabel", "NoticeLabel", UILabel)
RegistChildComponent(LuaHouseFishingWnd, "HangleTexture", "HangleTexture", TweenRotation)
RegistChildComponent(LuaHouseFishingWnd, "BaitBtn", "BaitBtn", GameObject)
RegistChildComponent(LuaHouseFishingWnd, "ProgressTexture", "ProgressTexture", UITexture)
RegistChildComponent(LuaHouseFishingWnd, "FishLine", "FishLine", UISprite)
RegistChildComponent(LuaHouseFishingWnd, "FightBtnGuideNode", "FightBtnGuideNode", GameObject)
RegistChildComponent(LuaHouseFishingWnd, "AnchorGuideNode", "AnchorGuideNode", GameObject)
RegistChildComponent(LuaHouseFishingWnd, "ProgressGuideNode", "ProgressGuideNode", GameObject)
RegistChildComponent(LuaHouseFishingWnd, "FishBaitAlert", "FishBaitAlert", GameObject)
RegistChildComponent(LuaHouseFishingWnd, "CreelAlert", "CreelAlert", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaHouseFishingWnd,"m_ServerAnchorPos")
RegistClassMember(LuaHouseFishingWnd,"m_FishPos")
RegistClassMember(LuaHouseFishingWnd,"m_MoveLength")
RegistClassMember(LuaHouseFishingWnd,"m_GreenLength")
RegistClassMember(LuaHouseFishingWnd,"m_IsFighting")
RegistClassMember(LuaHouseFishingWnd,"m_CurBait")
RegistClassMember(LuaHouseFishingWnd,"m_LengthScale")--进度条长度比例
RegistClassMember(LuaHouseFishingWnd,"m_ProgressStartX")
RegistClassMember(LuaHouseFishingWnd,"m_AnchorPos")
RegistClassMember(LuaHouseFishingWnd,"m_LastTime")
RegistClassMember(LuaHouseFishingWnd,"m_CenterPos")
RegistClassMember(LuaHouseFishingWnd,"m_StartFightTime")
RegistClassMember(LuaHouseFishingWnd,"m_StartFightTimeStamp")
RegistClassMember(LuaHouseFishingWnd,"m_CenterServerPos") 
RegistClassMember(LuaHouseFishingWnd,"m_FxLabel") 
RegistClassMember(LuaHouseFishingWnd,"m_FxTick") 
RegistClassMember(LuaHouseFishingWnd,"m_BestYuGanWeaponId")
RegistClassMember(LuaHouseFishingWnd,"m_WearYuGanWeaponId")
RegistClassMember(LuaHouseFishingWnd,"m_AskedChangeYuGan")
RegistClassMember(LuaHouseFishingWnd,"m_TweenAlpha")
RegistClassMember(LuaHouseFishingWnd,"m_NeedShouGan")
RegistClassMember(LuaHouseFishingWnd,"m_IsFishing")
RegistClassMember(LuaHouseFishingWnd,"m_BgMask")
RegistClassMember(LuaHouseFishingWnd,"m_LastPos")
RegistClassMember(LuaHouseFishingWnd,"m_NoticeList")
RegistClassMember(LuaHouseFishingWnd,"m_NoticeHideTick")
RegistClassMember(LuaHouseFishingWnd,"m_NoticeShowTick")
RegistClassMember(LuaHouseFishingWnd,"m_NeedNotice")
RegistClassMember(LuaHouseFishingWnd,"m_StrengthColor")
RegistClassMember(LuaHouseFishingWnd,"m_IsRotationReverse")
RegistClassMember(LuaHouseFishingWnd,"m_IsCameraInit")
RegistClassMember(LuaHouseFishingWnd,"m_BaitCountLabel")
RegistClassMember(LuaHouseFishingWnd,"m_BaitCount")
RegistClassMember(LuaHouseFishingWnd,"m_NeedBaitAlert")

function LuaHouseFishingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.QuitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnQuitBtnClick()
	end)


	
	UIEventListener.Get(self.CreelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCreelBtnClick()
	end)


	
	UIEventListener.Get(self.FishingBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFishingBtnClick()
	end)


	
	UIEventListener.Get(self.BaitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBaitBtnClick()
	end)


    --@endregion EventBind end
	self.m_StrengthColor = {
		["Green"] = "3BEE3C",
		["Red"] = "ff0000",
	}
	self.m_IsRotationReverse = false
	self.m_TergetAngle = -359

	self.m_LastTime = CServerTimeMgr.Inst.timeStamp
	self.m_IsFighting = false
	self.m_IsCameraInit = false
	self.m_BaitCount = 0

	local setting = HaiDiaoFuBen_Settings.GetData()
	self.m_SpecialFishingGampleId = setting.SpecialGameplayId
	self.m_FxLabel = self.FxNode.transform:Find("FxLabel"):GetComponent(typeof(UILabel))
	self.m_TweenAlpha = self.FishingBtn.transform:Find("bg"):GetComponent(typeof(TweenAlpha))
	self.m_TweenAlpha.enabled = false
	self.m_NeedShouGan = false
	self.GuideView:SetActive(true)
	LuaSeaFishingMgr.m_PreEnableZoom = CameraFollow.Inst.mbEnableZoom
	self.m_NoticeList = HouseFish_Setting.GetData().RandomBubbleContent
	self.Notice:SetActive(false)
	self.m_BaitCountLabel = self.BaitBtn.transform:Find("CountPanel/CountLabel"):GetComponent(typeof(UILabel))
	self.m_GuideStage = 0

	--模拟
	local fishingsetting = HouseFish_Fishing.GetData()
	self.GreenSpeed = fishingsetting.GreenSpeed
	self.GreenSpeedInc = fishingsetting.GreenSpeedInc
	self.GreenSpeedMax = fishingsetting.GreenSpeedMax
	self.GreenSpeedDec = fishingsetting.GreenSpeedDec
	self.ProgressIncSpeed = fishingsetting.ProgressIncSpeed
	self.ProgressDecSpeed = fishingsetting.ProgressDecSpeed
	self.TagMoveSpeedSet = fishingsetting.TagMoveSpeed
	self.GreenDragDeltaSpeed = 0
	self.CreelAlert:SetActive(false)
end

function LuaHouseFishingWnd:Init()
	local cls = HouseFish_Setting.GetData().FishSkillId
	self.m_CurSkillId = CClientMainPlayer.Inst.SkillProp:GetSkillIdWithDeltaByCls(cls, CClientMainPlayer.Inst.Level)
	self.m_SkillLevel = CLuaDesignMgr.Skill_AllSkills_GetLevel(self.m_CurSkillId)
	self:InitBait()
	self.FishBaitAlert:SetActive(self.m_NeedBaitAlert)
	
	self:HideFightingView()
	self:HideOtherObjectForSpecialFishing()
	self.Tip:SetActive(false)
	

	self.m_MoveLength = self.MoveArea.width
	self.m_ProgressStartX = self.MoveArea.transform.localPosition.x - self.MoveArea.width/2
	self.GuideMinx = self.m_ProgressStartX + 20
	self.GuideMaxx = self.m_ProgressStartX + self.MoveArea.width - 20

	self.m_GreenLength = self.GreenArea.width
	local data = HouseFish_Fishing.GetData()
	self.m_ProgressLen = data.ProgressLen
	self.m_LengthScale = self.m_ProgressLen / self.m_MoveLength
	
	self.m_BgMask = self.transform:Find("_BgMask_").gameObject
	self.m_LastPos = Vector2(0,0)
	UIEventListener.Get(self.m_BgMask).onDrag = DelegateFactory.VectorDelegate(function (go, delta)
        self:OnDrag(delta)
    end)

	--拓本
	local isWeaponSwitchOn = (CClientMainPlayer.Inst.AppearanceProp.HideWeaponFashionEffect == 0)
	if isWeaponSwitchOn then
		local weaponFashionId = CClientMainPlayer.Inst.AppearanceProp.WeaponFashionId
		local equipData = EquipmentTemplate_Equip.GetData(weaponFashionId)
		--不是鱼竿拓本的话需要关掉武器外观显示
		if not (equipData and equipData.Type == 1 and equipData.SubType == 51) then
			Gac2Gas.RequestSetFashionHide_Permanent(2, 1)
			g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("你的武器拓本外观已关闭"))
		end
	end
	--alert
	self:InitAlert()

end

function LuaHouseFishingWnd:InitFightingGuide()
	self.m_IsInFightingGuide = true
	self.FightingView:SetActive(true)
	self.HangleTexture.enabled = self.m_AreaScaledSpeed and self.m_AreaScaledSpeed ~= 0
	self:StartFightingGuide(1)
end

function LuaHouseFishingWnd:ClickThroughToNextGuide()
    if Input.GetMouseButtonDown(0) then 
		--点在非钓鱼按钮上，才会触发下一步引导
		if self.m_IsInFightingGuide and (UICamera.lastHit.collider and CUICommonDef.IsChild(self.FishingBtn.transform, UICamera.lastHit.collider.transform)) then
			return
		end

		self:StartFightingGuide(-1)
		if (self.m_GuideStage == 2 or self.m_GuideStage == 3) and not self.m_IsFighting then
			self.m_IsFighting = true
			self.m_LastTime = CServerTimeMgr.Inst.timeStamp
			self.m_GuideStage = self.m_GuideStage + 1
		elseif self.m_GuideStage == 1 then
			self.TagMoveSpeed = -self.TagMoveSpeedSet[math.random(0, self.TagMoveSpeedSet.Length-1)]
			self:OnBeginFishingFight(50, self.TagMoveSpeed, 50, -self.GreenSpeed, 10, 0, 0)
			self.HangleTexture.enabled = self.m_AreaScaledSpeed and self.m_AreaScaledSpeed ~= 0
			if self.m_GuideTick then
				UnRegisterTick(self.m_GuideTick)
				self.m_GuideTick = nil
			end
			self.m_GuideTick = RegisterTickOnce(function()
				self:StartFightingGuide(self.m_GuideStage)
				self.m_IsFighting = false
			end, 3000)			
			self.m_GuideStage = 2
		end
    end
end

function LuaHouseFishingWnd:StartFightingGuide(stage)
	--print("StartFightingGuide",stage)
	self.FightBtnGuideNode:SetActive(stage==1)
	self.AnchorGuideNode:SetActive(stage==2)
	self.ProgressGuideNode:SetActive(stage==3)
	if stage == 1 or stage == 2 or stage ==3 then
		self.m_GuideStage = stage
	end
end

function LuaHouseFishingWnd:HideOtherObjectForSpecialFishing()
	if LuaSeaFishingMgr.m_CurGameplayId == self.m_SpecialFishingGampleId then
		self.FishingBtn:SetActive(false)
		self.CreelBtn:SetActive(false)
		self.BaitBtn:SetActive(false)
		self.QuitBtn:SetActive(false)
	end
end

--@region UIEvent

function LuaHouseFishingWnd:OnQuitBtnClick()
	if LuaSeaFishingMgr.m_IsFishing then 
		local msg = g_MessageMgr:FormatMessage("Quit_SeaFishing_InFishing_Comfirm")
		MessageWndManager.ShowOKCancelMessage(msg, 
            DelegateFactory.Action(function ()				
                Gac2Gas.RequestCancelFishing()
				LuaSeaFishingMgr.m_IsFishing = false
				self:AskWearFightingWeaponWhenClose()
				for i,tick in pairs(LuaSeaFishingMgr.m_FishingTickList) do
					if tick then
						UnRegisterTick(tick)
						tick = nil
					end
				end
				LuaSeaFishingMgr.m_FishingTickList = {}	

				CUIManager.CloseUI(CUIResources.HouseFishingWnd)
            end),
            nil,
            LocalString.GetString("确定"), LocalString.GetString("取消"), false)
	else
			self:AskWearFightingWeaponWhenClose()
			for i,tick in pairs(LuaSeaFishingMgr.m_FishingTickList) do
				if tick then
					UnRegisterTick(tick)
					tick = nil
				end
			end
			LuaSeaFishingMgr.m_FishingTickList = {}	
			CUIManager.CloseUI(CUIResources.HouseFishingWnd)
	end
end

function LuaHouseFishingWnd:AskWearFightingWeaponWhenClose()
	self:CheckWearYugan()
	if not self.m_WearYuGanWeaponId then
		return
	end

	local count = CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag)
	local highestScore = 0
	local pos,itemId
	for i=1,count,1 do
		local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
		if id ~= nil then
			local citem = CItemMgr.Inst:GetById(id)
			if citem.IsEquip then
				local equip = citem.Equip
				if equip.IsBinded and equip.IsWeapon then
					local score = citem.Equip.Score
					if score > highestScore then
						highestScore = score
						pos = i
						itemId = id
					end
				end						
			end
		end
	end
	if pos and itemId then
		CItemInfoMgr.EquipRemind(pos, itemId)
	else
		g_MessageMgr:ShowMessage("Cant_Find_Suitable_BindedEquip")
	end
end


function LuaHouseFishingWnd:OnCreelBtnClick()
	--yulou
	if CGuideMgr.Inst:IsInPhase(151) then
		return
	end
	self:InitFightingView()
end

function LuaHouseFishingWnd:OnFishingBtnClick()
	--收杆
	if self.m_NeedShouGan then
		self:CancelShowTick()
		if not self.m_IsFighting then
			Gac2Gas.RequestHarvestFish()
			self:HideFightingView()
			self:ShowFx(LocalString.GetString("[FFFF00]上钩！[-]"))
			LuaSeaFishingMgr.StopFishing()
			--LuaSeaFishingMgr.StopFishing()
		end
		
		self.m_NeedShouGan = false	
		self.m_TweenAlpha.enabled = false
		return
	end

	--钓鱼中 不能点击
	if LuaSeaFishingMgr.m_IsFishing then
		return 
	end

	if CGuideMgr.Inst:IsInPhase(151) then
		return
	end

	self:CheckWearYugan()
	if not self.m_AskedChangeYuGan and self.m_WearYuGanWeaponId ~= self.m_BestYuGanWeaponId then
		local weardata = self.m_WearYuGanWeaponId and HouseFish_PoleType.GetDataBySubKey("TemplateID",self.m_WearYuGanWeaponId) or nil
		local bestdata = HouseFish_PoleType.GetDataBySubKey("TemplateID",self.m_BestYuGanWeaponId)
		if not weardata or weardata.UnLockedLevel <= bestdata.UnLockedLevel then
			self:ShowFastUseEquipTip()
			return 
		end
	end
	if not self.m_WearYuGanWeaponId then
		return
	end

	local coreScene = CoreScene.MainCoreScene
	local x, y = CClientMainPlayer.Inst.Pos.x/64, CClientMainPlayer.Inst.Pos.y/64
	local sign = bit.lshift(1, ERoadInfo.eRI_PLAY1)
	local dir = nil
	for i = 1, 3 do
		local road =  coreScene:GetRoadInfo(x-i, y-i)
		if bit.band(road, sign) > 0 then dir = 225 break end
		road =  coreScene:GetRoadInfo(x-i, y)
		if bit.band(road, sign) > 0 then dir = 180 break end
		road =  coreScene:GetRoadInfo(x-i, y+i)
		if bit.band(road, sign) > 0 then dir = 135 break end
		road =  coreScene:GetRoadInfo(x, y-i)
		if bit.band(road, sign) > 0 then dir = 270 break end
		road =  coreScene:GetRoadInfo(x, y+i)
		if bit.band(road, sign) > 0 then dir = 90 break end
		road =  coreScene:GetRoadInfo(x+i, y-i)
		if bit.band(road, sign) > 0 then dir = 315 break end
		road =  coreScene:GetRoadInfo(x+i, y)
		if bit.band(road, sign) > 0 then dir = 0 break end
		road =  coreScene:GetRoadInfo(x+i, y+i)
		if bit.band(road, sign) > 0 then dir = 45 break end
	end

	if not dir then return end

	CClientMainPlayer.Inst.Dir = dir

	if self.m_NeedBaitAlert then
		g_MessageMgr:ShowMessage("FISHING_NOT_USE_FOOD")
		self.m_NeedBaitAlert = false
	end


	if self.m_IsInFightingGuide and LuaSeaFishingMgr.s_FightingGuideSwitch then 
		return
	end
	
	Gac2Gas.RequestStartFishing()
end

function LuaHouseFishingWnd:OnFishingBtnPress(isPressed)
	if not self.m_IsFighting then
		return
	end
	
	local press = isPressed and 1 or 0
	press = tonumber(press)
	Gac2Gas.ChangeFishingButtonPressStatus(press,0)
end

function LuaHouseFishingWnd:OnClientFishingBtnPress(press)
	if (self.m_GuideStage == 2) or
	self.m_GuideStage >= 3 and self.m_IsFighting then
		if press then
			self.GreenDragDeltaSpeed = self.GreenSpeedInc
		else
			self.GreenDragDeltaSpeed = -self.GreenSpeedDec
		end
	end
	
end

function LuaHouseFishingWnd:OnBaitBtnClick()
	self.FishBaitAlert:SetActive(false)
	if CGuideMgr.Inst:IsInPhase(151) then
		return
	end
	CUIManager.ShowUI(CLuaUIResources.FishBaitWnd)
end

--@endregion UIEvent
function LuaHouseFishingWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateFishFood",self,"OnUpdateFishFood")
	g_ScriptEvent:AddListener("BeginFishingFight",self,"OnBeginFishingFight")
	g_ScriptEvent:AddListener("UpdateFishingFightState",self,"OnUpdateFishingFightState")
	g_ScriptEvent:AddListener("FishingHarvestSuccess",self,"OnFishingHarvestSuccess")
	g_ScriptEvent:AddListener("HouseFishingFightFailed",self,"OnHouseFishingFightFailed")
	g_ScriptEvent:AddListener("FishingHarvestBegin",self,"OnFishingHarvestBegin")
	g_ScriptEvent:AddListener("EquipSuccess", self, "OnEquipSuccess")
	g_ScriptEvent:AddListener("SetCurrentFishingWaitDuration", self, "OnConsumeBait")
	g_ScriptEvent:AddListener("StartHouseFishingGuide", self, "OnStartHouseFishingGuide")
	g_ScriptEvent:AddListener("MarkNewShinningHouseFishItem",self,"OnMarkNewShinningHouseFishItem")
	g_ScriptEvent:AddListener("RemoveShinningHouseFishItem",self,"OnRemoveShinningHouseFishItem")
end

function LuaHouseFishingWnd:OnDisable()
    local excepts = {"MiddleNoticeCenter","GuideWnd"}
	local List_String = MakeGenericClass(List,cs_string)
	CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
    CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)

	g_ScriptEvent:RemoveListener("UpdateFishFood",self,"OnUpdateFishFood")
	g_ScriptEvent:RemoveListener("BeginFishingFight",self,"OnBeginFishingFight")
	g_ScriptEvent:RemoveListener("UpdateFishingFightState",self,"OnUpdateFishingFightState")
	g_ScriptEvent:RemoveListener("FishingHarvestSuccess",self,"OnFishingHarvestSuccess")
	g_ScriptEvent:RemoveListener("HouseFishingFightFailed",self,"OnHouseFishingFightFailed")
	g_ScriptEvent:RemoveListener("FishingHarvestBegin",self,"OnFishingHarvestBegin")
	g_ScriptEvent:RemoveListener("EquipSuccess", self, "OnEquipSuccess")
	g_ScriptEvent:RemoveListener("SetCurrentFishingWaitDuration", self, "OnConsumeBait")
	g_ScriptEvent:RemoveListener("StartHouseFishingGuide", self, "OnStartHouseFishingGuide")
	g_ScriptEvent:RemoveListener("MarkNewShinningHouseFishItem",self,"OnMarkNewShinningHouseFishItem")
	g_ScriptEvent:RemoveListener("RemoveShinningHouseFishItem",self,"OnRemoveShinningHouseFishItem")

	if self.m_FxTick then
		UnRegisterTick(self.m_FxTick)
		self.m_FxTick = nil
	end
	if self.m_NoticeShowTick then
		UnRegisterTick(self.m_NoticeShowTick)
		self.m_NoticeShowTick = nil
	end
	if self.m_NoticeHideTick then
		UnRegisterTick(self.m_NoticeHideTick)
		self.m_NoticeHideTick = nil
	end
	if self.m_GuideTick then
		UnRegisterTick(self.m_GuideTick)
		self.m_GuideTick = nil
	end

	LuaSeaFishingMgr.RevertFishingCamera()
end

function LuaHouseFishingWnd:OnDrag(delta)
	local cf = NormalCamera.Inst
	local hideT1 = cf.m_HideUIFarBar - 0.05
	local hideT2 = cf.s_HideUINearBar + 0.05

	local y = delta.y
	local x = delta.x

	if cf.m_T <= hideT2 then
		y = -1
	end
	if cf.m_T >= hideT1 then
		y = 1
		
	end

	y = math.min(y,10)
	y = math.max(y,-10)
	local deltaPos = Vector2(x,y)
	NormalCamera.Inst.mbEnableZoom = false
	NormalCamera.Inst:OnDrag(deltaPos,true)
end

function LuaHouseFishingWnd:Update()
	self:UpdateMouseInput()
	--正在搏斗引导中
	if self.m_IsInFightingGuide == true then
		self:ClickThroughToNextGuide()
		if self.m_GuideStage<1 then return end
	end

	if not self.m_IsFighting then return end
	if not self.m_LastTime then return end

	local now = CServerTimeMgr.Inst.timeStamp
	local delta = now - self.m_LastTime
	self.m_LastTime = now

	if self.m_IsInFightingGuide and self.m_GuideStage >= 3 then
		--print(delta)
		self:UpdateGuidingSpeed(delta)
	end
	
	self:UpdateStrengthArea(delta)
	self:UpdateAnchorPos(delta)
	self:UpdateColorAndRotate(delta)
	self:UpdateFishLine()

	if self.m_IsInFightingGuide and self.m_GuideStage >= 3 then
		if self.m_GuideStage == 3 and self.ProgressTexture.fillAmount >= 0.8 then
			self:StartFightingGuide(self.m_GuideStage)
			self.m_IsFighting = false
		end
		if self.m_GuideStage == 4 and self.ProgressTexture.fillAmount >= 1 then
			-- self.m_IsInFightingGuide = false
			self:HideFightingView()
			Gac2Gas.SendFishForFinishingGuide()
			LuaSeaFishingMgr.StopFishing()
			self.m_NeedShouGan = false	
		end
	end
end

function LuaHouseFishingWnd:UpdateMouseInput()
	if Input.GetMouseButtonDown(0) then 
		if (UICamera.lastHit.collider and
		CUICommonDef.IsChild(self.FishingBtn.transform, UICamera.lastHit.collider.transform)) then
			--客戶端模拟
			if self.m_IsInFightingGuide then
				self:OnClientFishingBtnPress(true)
			else
				self:OnFishingBtnPress(true)
			end
		end
        return
    end
    
    if Input.GetMouseButtonUp(0) then
        if (UICamera.lastHit.collider and
		CUICommonDef.IsChild(self.FishingBtn.transform, UICamera.lastHit.collider.transform)) then
			--客戶端模拟
			if self.m_IsInFightingGuide then
				self:OnClientFishingBtnPress(false)
			else
				self:OnFishingBtnPress(false)
			end
		end
		if self.m_GuideStage == 4 and self.ProgressTexture.fillAmount >= 1 then
			self.m_IsInFightingGuide = false
		end
    end
end

function LuaHouseFishingWnd:UpdateGuidingSpeed(delta)
	--print(self.m_AreaScaledSpeed)
	self.m_AreaScaledSpeed = self.m_AreaScaledSpeed + self.GreenDragDeltaSpeed / self.m_LengthScale * delta
end

function LuaHouseFishingWnd:OnUpdateFishFood()
	if not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.ItemProp then
		return
	end
	self.m_CurBait = CClientMainPlayer.Inst.ItemProp.FishFood
	self:InitBait()
	if CUIManager.IsLoaded(CLuaUIResources.FishBaitWnd) then
		CUIManager.CloseUI(CLuaUIResources.FishBaitWnd)
	end
end

function LuaHouseFishingWnd:InitFightingView()
	CUIManager.ShowUI(CLuaUIResources.SeaFishingPackageWnd)
end

function LuaHouseFishingWnd:InitBait()
	local itemProp = CClientMainPlayer.Inst.ItemProp
	if not itemProp or not itemProp.FishFood or itemProp.FishFood == 0 then
		self.AddBaitSprite:SetActive(true)
		self.BaitIcon:LoadMaterial(nil)
		self.m_BaitCountLabel.text = ""
		self.m_BaitCount = 0
		self.m_NeedBaitAlert = true
	else
		self.m_NeedBaitAlert = false
		self.m_CurBait = itemProp.FishFood
		self.AddBaitSprite:SetActive(false)
		local path = Item_Item.GetData(self.m_CurBait).Icon
		self.BaitIcon:LoadMaterial(path)
		local bindCount, notBindCount
    	bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(self.m_CurBait)
		local baitCount = bindCount + notBindCount
		local pos = self.BaitIcon.transform.localPosition
		
		local wearYuGan = nil
		local equipListOnBody = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Body)
		for i=1,equipListOnBody.Count,1 do
			if HouseFish_PoleType.GetDataBySubKey("TemplateID",equipListOnBody[i-1].templateId) then
				wearYuGan = equipListOnBody[i-1].templateId
				break
			end
		end

		local poleData = HouseFish_PoleType.GetDataBySubKey("TemplateID",wearYuGan)
		local yuganLevel = poleData and poleData.PoleLevel or 0
		local t = LuaSeaFishingMgr.TryGetYuErPlayerLevel(self.m_CurBait)
		if not t then return end
		local needPlayerLevel = t[1]
		local yuerLevel = t[2]
		
		if baitCount <= 0 or (LuaSeaFishingMgr.GetNeedSkillLvByYuerItemId(self.m_CurBait) and self.m_SkillLevel < LuaSeaFishingMgr.GetNeedSkillLvByYuerItemId(self.m_CurBait)) then
			pos.z = -1
		elseif yuerLevel > yuganLevel then--鱼竿等级不够
			pos.z = -1
		else
			pos.z = 0
		end
		self.BaitIcon.transform.localPosition = pos
		self.m_BaitCountLabel.text = baitCount
		self.m_BaitCount = baitCount
	end
end

function LuaHouseFishingWnd:HideFightingView()
	self.m_LastTime = nil
	self.m_IsFighting = false
	
	self.FightingView:SetActive(false)
end

--callback
function LuaHouseFishingWnd:OnBeginFishingFight(tagPos, tagSpeed, greenPos, greenSpeed, greenLen, percent, time)
	--print("fishing fight begin:",tagPos, tagSpeed, greenPos, greenSpeed, greenLen, percent, time)
	self:ShowFx(LocalString.GetString("[FFFF00]稀有！[-]"))
	self.FightingView:SetActive(true)
	self.m_IsFighting = true
	self.m_TweenAlpha.enabled = false
	self.m_NeedShouGan = false

	--anchor
	local ax = self.m_ProgressStartX + tagPos / self.m_LengthScale
	local anchorPos = self.Anchor.transform.localPosition
	anchorPos.x = ax
	self.m_AnchorPos = anchorPos
	self.m_ServerAnchorPos = anchorPos
	self.Anchor.transform.localPosition = self.m_AnchorPos
	--初始化力度条的位置
	local moveAreaPos = self.GreenArea.transform.localPosition
	local x = self.m_ProgressStartX + greenPos/self.m_LengthScale
	moveAreaPos.x = x
	self.m_ServerCenterPos = moveAreaPos
	self.m_CenterPos = self.m_ServerCenterPos
	self.GreenArea.transform.localPosition = self.m_CenterPos
	self.GreenArea.width = greenLen/self.m_LengthScale*2

	self.m_AreaScaledSpeed = greenSpeed / self.m_LengthScale
	self.m_AnchorScaledSpeed = tagSpeed / self.m_LengthScale
	self.m_StartFightTimeStamp = CServerTimeMgr.Inst.timeStamp
	self.m_LastTime = self.m_StartFightTimeStamp

	--进度条
	self.m_ServerPercent = percent
	self.m_CurPercent = percent
	self.ProgressTexture.fillAmount = percent
end

function LuaHouseFishingWnd:UpdateStrengthArea(delta)
	if not delta then
		delta = 0
	end

	local origspeed = self.m_AreaScaledSpeed
	local diff =  self.m_ServerCenterPos.x - self.m_CenterPos.x
	local mul = origspeed > 0 and math.exp(math.min(1.1, diff)) or math.exp(math.min(1.1, -diff))
	local speed = origspeed * mul
	local x = self.m_CenterPos.x + speed * delta
	local serverx = self.m_ServerCenterPos.x + origspeed*delta
	serverx = math.min(serverx,self.MoveArea.transform.localPosition.x + self.MoveArea.width/2 - self.GreenArea.width/2)
	serverx = math.max(serverx,self.m_ProgressStartX + self.GreenArea.width/2)
	self.m_ServerCenterPos.x = serverx

	x = math.min(x,self.MoveArea.transform.localPosition.x + self.MoveArea.width/2 - self.GreenArea.width/2)
	x = math.max(x,self.m_ProgressStartX + self.GreenArea.width/2)
	self.m_CenterPos.x = x 

	self.GreenArea.transform.localPosition = self.m_CenterPos

	if self.m_IsInFightingGuide then
		if x == self.MoveArea.transform.localPosition.x + self.MoveArea.width/2 - self.GreenArea.width/2 or
		 x == self.m_ProgressStartX + self.GreenArea.width/2 then
			self.m_AreaScaledSpeed = 0
		end
	end
end

function LuaHouseFishingWnd:UpdateAnchorPos(delta)
	if not delta then
		delta = 0
	end

	local origspeed = self.m_AnchorScaledSpeed
	local diff =  self.m_ServerAnchorPos.x - self.m_AnchorPos.x
	local mul = origspeed > 0 and math.exp(math.min(1.1, diff)) or math.exp(math.min(1.1, -diff))
	local speed = origspeed * mul
	local x = self.m_AnchorPos.x + speed * delta
	
	x = math.min(x,self.GuideMaxx)
	x = math.max(x,self.GuideMinx)
	local serverx = self.m_ServerAnchorPos.x + origspeed*delta
	serverx = math.min(serverx,self.GuideMaxx)
	serverx = math.max(serverx,self.GuideMinx)
	self.m_ServerAnchorPos.x = serverx
	self.m_AnchorPos.x = x 
	self.Anchor.transform.localPosition = self.m_AnchorPos

	--引导模拟时浮标来回移动
	if self.m_IsInFightingGuide then 
		if x == self.GuideMaxx or x == self.GuideMinx then
			self.m_AnchorScaledSpeed = - self.m_AnchorScaledSpeed
		end
	end
end

function LuaHouseFishingWnd:UpdateColorAndRotate(delta)
	local anchorPos = self.Anchor.transform.localPosition
	local strengthPos = self.GreenArea.transform.localPosition
	local strengthLen = self.GreenArea.width / 2
	if math.abs(anchorPos.x - strengthPos.x ) > strengthLen then
		self.GreenArea.color = NGUIText.ParseColor24(self.m_StrengthColor.Red, 0)
		self.HangleTexture.to = Vector3(0,0,359)
		if self.m_GuideStage >= 3 or not self.m_IsInFightingGuide then
			self.ProgressTexture.fillAmount = self.ProgressTexture.fillAmount - self.ProgressDecSpeed * delta
		end
	else
		self.GreenArea.color = NGUIText.ParseColor24(self.m_StrengthColor.Green, 0)
		self.HangleTexture.to = Vector3(0,0,-359)
		if self.m_GuideStage >= 3 or not self.m_IsInFightingGuide then
			self.ProgressTexture.fillAmount = self.ProgressTexture.fillAmount + self.ProgressIncSpeed * delta
		end
	end
end

function LuaHouseFishingWnd:UpdateFishLine()
	local anchorPos = self.Anchor.transform.localPosition
	local lineRightPos = self.FishLine.transform.localPosition
	local width = math.abs(anchorPos.x - lineRightPos.x)
	self.FishLine.width = width
end

function LuaHouseFishingWnd:OnUpdateFishingFightState(tagPos, tagSpeed, greenPos, greenSpeed, greenLen, percent, time)
	--print("update",tagPos, tagSpeed, greenPos, greenSpeed, greenLen, percent, time)
	self.m_ServerPercent = percent
	self.m_CurPercent = percent
	self.ProgressTexture.fillAmount = percent
	-- --绿条白条的位置
	local centerPos = self.GreenArea.transform.localPosition
	local cx = self.m_ProgressStartX + greenPos/self.m_LengthScale
	self.m_ServerCenterPos = Vector3(cx,centerPos.y,centerPos.z)
	self.m_AreaScaledSpeed = greenSpeed / self.m_LengthScale
	
	local ax = self.m_ProgressStartX + tagPos / self.m_LengthScale
	local anchorPos = self.Anchor.transform.localPosition
	self.m_ServerAnchorPos = Vector3(ax, anchorPos.y, anchorPos.z)
	self.m_AnchorScaledSpeed = tagSpeed / self.m_LengthScale
end

function LuaHouseFishingWnd:ShowFx(text)
	self.FxNode:SetActive(true)
	self.UIFx:DestroyFx()
	self.UIFx2:DestroyFx()
	self.UIFx:LoadFx("fx/ui/prefab/UI_haidiao_shouhuo.prefab")
	self.UIFx2:LoadFx("fx/ui/prefab/UI_haidiao_shouhuo_beijing.prefab")
	self.m_FxLabel.text = text

	if self.m_FxTick then
		UnRegisterTick(self.m_FxTick)
		self.m_FxTick = nil
	end
	self.m_FxTick = RegisterTickOnce(function()
		self.FxNode:SetActive(false)
	end, 2000)
end

function LuaHouseFishingWnd:OnFishingHarvestSuccess(fishItemTempId, weight)
	self.m_NeedShouGan = false
	self:ShowFx(LocalString.GetString("[FFFF00]上钩！[-]"))
	self:HideFightingView()
	self.m_TweenAlpha.enabled = false

	local isAlert = self.CreelAlert.activeSelf
	if isAlert then
		return
	end

	if not CClientMainPlayer.Inst then return end
	local fishbasket = CClientMainPlayer.Inst.ItemProp.FishBasket
	if not fishbasket then return end 
	local shinningFish = fishbasket.ShinningFish
	local shinningOther = fishbasket.ShinningOther
	local data = HouseFish_AllFishes.GetData(fishItemTempId)
	local needShinning  = false   
	if data then
        local fishIdx = data.ShinningIdx
        needShinning = shinningFish:GetBit(fishIdx)
	else
		data = HouseFish_AllOther.GetData(fishItemTempId)
		local fishIdx = data.ShinningIdx
        needShinning = shinningOther:GetBit(fishIdx)
	end
	if needShinning then
		self.CreelAlert:SetActive(true)
	end
end

function LuaHouseFishingWnd:OnHouseFishingFightFailed()
	self.m_NeedShouGan = false
	self:ShowFx(LocalString.GetString("[FF0000]已脱钩[-]"))
	self:CancelShowTick()
	self:HideFightingView()
	self.m_TweenAlpha.enabled = false
end

function LuaHouseFishingWnd:CheckWearYugan()
	local cls = HouseFish_Setting.GetData().FishSkillId
	self.m_CurSkillId = CClientMainPlayer.Inst.SkillProp:GetSkillIdWithDeltaByCls(cls, CClientMainPlayer.Inst.Level)
	self.m_SkillLevel = CLuaDesignMgr.Skill_AllSkills_GetLevel(self.m_CurSkillId)

	self.m_BestYuGanWeaponId = LuaSeaFishingMgr.GetYuganIdBySkillLevel(self.m_SkillLevel)
	self.m_WearYuGanWeaponId = nil

    local equipListOnBody = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Body)
    local find = false
    for i=1,equipListOnBody.Count,1 do
            if equipListOnBody[i-1].templateId == self.m_BestYuGanWeaponId then
				self.m_WearYuGanWeaponId = self.m_BestYuGanWeaponId
                find = true
                break
            end
			if HouseFish_PoleType.GetDataBySubKey("TemplateID",equipListOnBody[i-1].templateId) then
				self.m_WearYuGanWeaponId = equipListOnBody[i-1].templateId
			end
    end
	return find
end

function LuaHouseFishingWnd:ShowFastUseEquipTip()
    local count = CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag)
	local findYuGanInBag
	local findYuGanLevel = 0
	local pos,itemId
    for i=1,count,1 do
        local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
        if id ~= nil then
            local citem = CItemMgr.Inst:GetById(id)
			if citem.TemplateId == self.m_BestYuGanWeaponId then
				findYuGanInBag = self.m_BestYuGanWeaponId
				pos = i
				itemId = id
				break
			end
			local data = HouseFish_PoleType.GetDataBySubKey("TemplateID",citem.TemplateId)

			if data and data.UnLockedLevel > findYuGanLevel and data.UnLockedLevel <= self.m_SkillLevel then
				findYuGanInBag = citem.TemplateId
				pos = i
				itemId = id
				findYuGanLevel = data.UnLockedLevel
			end
        end
    end
	--print(self.m_BestYuGanWeaponId,self.m_WearYuGanWeaponId,findYuGanInBag)
	if self.m_WearYuGanWeaponId and self.m_WearYuGanWeaponId == self.m_BestYuGanWeaponId then
		return
	end
	--装备了低等级的鱼竿 只需要问一次
	if self.m_WearYuGanWeaponId and self.m_AskedChangeYuGan then
		return
	end
	if self.m_WearYuGanWeaponId then
		self.m_AskedChangeYuGan = true
	end

	if not findYuGanInBag then
		g_MessageMgr:ShowMessage("SeaFishing_Need_Wear_YuGan")
		return
	end
	if self.m_AskedChangeYuGan then
		g_MessageMgr:ShowMessage("Need_Suitable_YuGan")
	end
	CItemInfoMgr.EquipRemind(pos, itemId)
end

function LuaHouseFishingWnd:OnFishingHarvestBegin()
	self.m_TweenAlpha.enabled = true
	self.m_NeedShouGan = true
	self.m_NeedNotice = true
	self:onSetCurrentFishingWaitDuration()
end

function LuaHouseFishingWnd:OnEquipSuccess(args)
	local pos = args[0]
	local equipId = args[1]
	local item = CItemMgr.Inst:GetById(equipId)
	if item and HouseFish_PoleType.GetDataBySubKey("TemplateID",item.TemplateId) then
		self.m_WearYuGanWeaponId = item.TemplateId
		self:InitBait()
	end
end

function LuaHouseFishingWnd:InitAndShowNotice()
	if not self.m_NoticeList then return end
	local count = self.m_NoticeList.Length
	local index = UnityEngine_Random(0,count)
	local text = self.m_NoticeList[index]
	if text then
		self.Notice:SetActive(true)
		self.NoticeLabel.text = text
	end
end

function LuaHouseFishingWnd:onSetCurrentFishingWaitDuration(startTime, waitDuration)
	if self.m_NoticeShowTick then
		UnRegisterTick(self.m_NoticeShowTick)
		self.m_NoticeShowTick = nil
	end
	

	local wait2ShowTime = UnityEngine_Random(LuaSeaFishingMgr.m_MinWaitTime, LuaSeaFishingMgr.m_MaxWaitTime+1)
	local wait2HideTime = LuaSeaFishingMgr.m_NoticeTime

	self.m_NoticeShowTick = RegisterTickOnce(function()
		self:InitAndShowNotice()
		if self.m_NoticeHideTick then
			UnRegisterTick(self.m_NoticeHideTick)
			self.m_NoticeHideTick = nil
		end
		self.m_NoticeHideTick = RegisterTickOnce(function()
			self.Notice:SetActive(false)
			if self.m_NeedNotice then
				local wtime = UnityEngine_Random(LuaSeaFishingMgr.m_MinWaitTime, LuaSeaFishingMgr.m_MaxWaitTime+1)
				self.m_NoticeShowTick = RegisterTickOnce(function()
					self:InitAndShowNotice()
					if self.m_NoticeHideTick then
						UnRegisterTick(self.m_NoticeHideTick)
						self.m_NoticeHideTick = nil
					end
					self.m_NoticeHideTick = RegisterTickOnce(function()
						self.Notice:SetActive(false)
						if self.m_NeedNotice then
							
						end
					end,wait2HideTime*1000)
			
				end,wtime*1000)
			end
		end,wait2HideTime*1000)

	end,wait2ShowTime*1000)
end

function LuaHouseFishingWnd:CancelShowTick()
	self.m_NeedNotice = false
	if self.m_NoticeShowTick then
		UnRegisterTick(self.m_NoticeShowTick)
		self.m_NoticeShowTick = nil
	end
	self.Notice:SetActive(false)
end

function LuaHouseFishingWnd:RefreshBaitCount(count)
	local pos = self.BaitIcon.transform.localPosition
	if count <= 0 then
		pos.z = -1
	else
		pos.z = 0
	end
	self.m_BaitCountLabel.text = count
	self.BaitIcon.transform.localPosition = pos
end

function LuaHouseFishingWnd:OnConsumeBait(startTime, waitDuration)
	if self.m_BaitCount > 0 then
		self.m_BaitCount = self.m_BaitCount - 1
		self:RefreshBaitCount(self.m_BaitCount)
	end
	if not self.m_IsCameraInit then
		--只有第一次抛竿时 镜头才会调整
		LuaSeaFishingMgr.SetFishingCamera()
		self.m_IsCameraInit = true
	end
end

function LuaHouseFishingWnd:OnStartHouseFishingGuide(fishTempId)
	--模拟第一次客户端的搏斗
	if LuaSeaFishingMgr.s_FightingGuideSwitch then
		self.m_LastTime = CServerTimeMgr.Inst.timeStamp
		if self.m_GuideStage < 1 then
			self:InitFightingGuide()
		end
	end
end

function LuaHouseFishingWnd:InitAlert()
	if not CClientMainPlayer.Inst then return end

	local fishbasket = CClientMainPlayer.Inst.ItemProp.FishBasket
	if not fishbasket then return end 
	local shinningFish = fishbasket.ShinningFish
    local shinningOther = fishbasket.ShinningOther
	local needAlert = false

	--Fish要不要shinning
	local itemIdArray = fishbasket.Fishs
	local countArray = fishbasket.Count
	for i=1,itemIdArray.Length-1,1 do
		local itemId = itemIdArray[i]
        local count = countArray[i]
		local data = HouseFish_AllFishes.GetData(itemId)        
		if data and count>0 then
            local fishIdx = data.ShinningIdx
            local needShinning = shinningFish:GetBit(fishIdx)
			if needShinning then
				needAlert = true
				break
			end
		end
	end

	--other要不要shinning
	if not needAlert then
		local otherArray = fishbasket.Others
		local otherCountArray = fishbasket.OtherCount
		for i=1,otherArray.Length-1,1 do
			local itemId = otherArray[i]
			local count = otherCountArray[i]
			local data = HouseFish_AllOther.GetData(itemId)
			if data and count>0 then
				local idx = data.ShinningIdx
				local needShinning = shinningOther:GetBit(idx)
				if needShinning then
					needAlert = true
					break
				end
			end
		end
	end

	self.CreelAlert:SetActive(needAlert)
end

function LuaHouseFishingWnd:OnMarkNewShinningHouseFishItem(templateId)
	self.CreelAlert:SetActive(true)
end
function LuaHouseFishingWnd:OnRemoveShinningHouseFishItem(templateId)
	self:InitAlert()
end

--引导
function LuaHouseFishingWnd:GetGuideGo(methodName)
    if methodName == "GetBaitButton" then
        return self.BaitBtn
    end
	if methodName == "GetFishingButton" then
		return self.FishingBtn
	end
	if methodName == "GetFishBasketButton" then
		return self.CreelBtn
	end
    return nil
end
