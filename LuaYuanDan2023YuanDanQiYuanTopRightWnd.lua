local CUIGameObjectPool = import "L10.UI.CUIGameObjectPool"
local UILabel = import "UILabel"
local Animation = import "UnityEngine.Animation"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local GameObject = import "UnityEngine.GameObject"
local CUIFx = import "L10.UI.CUIFx"

LuaYuanDan2023YuanDanQiYuanTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaYuanDan2023YuanDanQiYuanTopRightWnd, "RightBtn", "RightBtn", CButton)
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanTopRightWnd, "FuBar", "FuBar", GameObject)
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanTopRightWnd, "XiBar", "XiBar", GameObject)
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanTopRightWnd, "CaiBar", "CaiBar", GameObject)
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanTopRightWnd, "AddNum", "AddNum", UILabel)
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanTopRightWnd, "Pool", "Pool", CUIGameObjectPool)
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanTopRightWnd, "WaveScore", "WaveScore", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaYuanDan2023YuanDanQiYuanTopRightWnd, "m_OneMaxProgress")
RegistClassMember(LuaYuanDan2023YuanDanQiYuanTopRightWnd, "m_MaxProgress")
RegistClassMember(LuaYuanDan2023YuanDanQiYuanTopRightWnd, "m_ScoreTypeList")
RegistClassMember(LuaYuanDan2023YuanDanQiYuanTopRightWnd, "m_HasShowFx")
RegistClassMember(LuaYuanDan2023YuanDanQiYuanTopRightWnd, "m_Ani")
RegistClassMember(LuaYuanDan2023YuanDanQiYuanTopRightWnd, "m_AniBg")
RegistClassMember(LuaYuanDan2023YuanDanQiYuanTopRightWnd, "m_CurProgress")
RegistClassMember(LuaYuanDan2023YuanDanQiYuanTopRightWnd, "m_CurMember")
function LuaYuanDan2023YuanDanQiYuanTopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    UIEventListener.Get(self.RightBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightBtnClick()
	end)
    --@endregion EventBind end
    self.m_CurMember = 0
end

function LuaYuanDan2023YuanDanQiYuanTopRightWnd:Init()
    self.m_AniBg = nil
    self.m_MaxProgress = YuanDan2023_YuanDanQiYuan.GetData().MaxProgress
    self.m_OneMaxProgress = self.m_MaxProgress / 3
    self.m_ScoreTypeList = {LocalString.GetString("[f1aeff]福气+%d[-]"),LocalString.GetString("[ffa37b]喜气+%d[-]"),LocalString.GetString("[eddd92]财气+%d[-]")}
    self.AddNum.gameObject:SetActive(false)
    self.m_CurProgress = {}
    if not LuaYuanDan2023Mgr.scroeInfo then
        self:UpdateProgress(0,0,0)
    else
        local data = LuaYuanDan2023Mgr.scroeInfo
        self:UpdateProgress(data.Fu,data.Xi,data.Cai)
    end
    self.m_Ani = self.gameObject:GetComponent(typeof(Animation))
    self.m_HasShowFx = {false,false,false}
    self:SetFxActive(false)
end

function LuaYuanDan2023YuanDanQiYuanTopRightWnd:UpdateProgress(Fu,Xi,Cai)
    self:UpdateOneProgress(self.FuBar,Fu,Fu >= self.m_MaxProgress , self.m_CurProgress[1] == Fu)
    self:UpdateOneProgress(self.XiBar,Xi,Xi >= self.m_MaxProgress , self.m_CurProgress[2] == Xi)
    self:UpdateOneProgress(self.CaiBar,Cai,Cai >= self.m_MaxProgress , self.m_CurProgress[3] == Cai)
    self:CheckAni(Fu,Xi,Cai)
    self.m_CurProgress[1] = Fu
    self.m_CurProgress[2] = Xi
    self.m_CurProgress[3] = Cai
end

function LuaYuanDan2023YuanDanQiYuanTopRightWnd:UpdateOneProgress(obj,progress,showFx,isEqual)
    if self.m_OneMaxProgress == 0 then return end
    local First = obj.transform:Find("FirstProgress"):GetComponent(typeof(UISlider))
    local Second = obj.transform:Find("SecondProgress"):GetComponent(typeof(UISlider))
    local Third = obj.transform:Find("ThirdProgress"):GetComponent(typeof(UISlider))
    local FullFx = obj.transform:Find("FullFx").gameObject
    if showFx and not isEqual then
        FullFx:SetActive(true)
        FullFx:GetComponent(typeof(Animation)):Play("yuandan2023yuandanqiyuantoprightwnd_jinduman")
    elseif not showFx then
        FullFx:SetActive(false)
    end

    First.value = math.min(math.max(0,progress),self.m_OneMaxProgress) * 1.0 / self.m_OneMaxProgress
    Second.value = math.min(math.max(0,progress - self.m_OneMaxProgress),self.m_OneMaxProgress) * 1.0  / self.m_OneMaxProgress
    Third.value = math.min(math.max(0,progress - self.m_OneMaxProgress * 2),self.m_OneMaxProgress) * 1.0  / self.m_OneMaxProgress
end

function LuaYuanDan2023YuanDanQiYuanTopRightWnd:OnRightBtnClick()
    self.RightBtn.transform.localEulerAngles = Vector3(0, 0, 0)
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaYuanDan2023YuanDanQiYuanTopRightWnd:OnHideTopAndRightTipWnd()
    self.RightBtn.transform.localEulerAngles = Vector3(0, 0, 180)
end
function LuaYuanDan2023YuanDanQiYuanTopRightWnd:OnPlayerAddScore(type,score)
    local addNumLabel = self.Pool:GetFromPool(0)
    self.m_CurMember = self.m_CurMember + 1
    addNumLabel.transform.parent = self.WaveScore.transform
    addNumLabel.gameObject:SetActive(true)
    addNumLabel:GetComponent(typeof(UILabel)).text = SafeStringFormat3(self.m_ScoreTypeList[type], score)
    self:SetWaveWordBgColor(addNumLabel,type)
    local tweenAlpha = addNumLabel.transform:GetComponent(typeof(TweenAlpha))
    local tweenScale = addNumLabel.transform:GetComponent(typeof(TweenScale))
    local tweenPosition = addNumLabel.transform:GetComponent(typeof(TweenPosition))

    tweenAlpha:ResetToBeginning()
    tweenScale:ResetToBeginning()
    tweenPosition:ResetToBeginning()

    tweenAlpha:PlayForward()
    tweenScale:PlayForward()
    tweenPosition:PlayForward()

    tweenScale:SetOnFinished(DelegateFactory.Callback(function()  
        addNumLabel.transform.parent = nil
        addNumLabel:SetActive(false)
        self.Pool:Recycle(addNumLabel.gameObject)
        self.m_CurMember = self.m_CurMember - 1
    end))
end

function LuaYuanDan2023YuanDanQiYuanTopRightWnd:SetWaveWordBgColor(waveWord,type)
    local bg1Color = {"743a81","941e39","953100"}
    local bg2Color = {"bb54d1","b72625","cb832d"}
    waveWord.transform:Find("bg1"):GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24(bg1Color[type], 0)
    waveWord.transform:Find("bg2"):GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24(bg2Color[type], 0)
    local depth = {11,10,12,9,12}
    local Wavelist = CommonDefs.GetComponentsInChildren_Component_Type(waveWord.transform,typeof(UIWidget))
    for i = 0,Wavelist.Length - 1 do
        Wavelist[i].depth = depth[i + 1] + (self.m_CurMember - 1) * 5
    end
end
function LuaYuanDan2023YuanDanQiYuanTopRightWnd:CheckAni(Fu,Xi,Cai)
    local aniName = {"yuandan2023yuandanqiyuantoprightwnd_fu","yuandan2023yuandanqiyuantoprightwnd_xi","yuandan2023yuandanqiyuantoprightwnd_cai"}
    if self.m_Ani then
        if Fu >= self.m_MaxProgress and not self.m_HasShowFx[1] and self.m_Ani[aniName[1]] then self:SetFxActive(true) self.m_Ani:Play(aniName[1]) self.m_HasShowFx[1] = true end
        if Xi >= self.m_MaxProgress and not self.m_HasShowFx[2] and self.m_Ani[aniName[2]] then self:SetFxActive(true) self.m_Ani:Play(aniName[2]) self.m_HasShowFx[2] = true end
        if Cai >= self.m_MaxProgress and not self.m_HasShowFx[3] and self.m_Ani[aniName[3]] then self:SetFxActive(true) self.m_Ani:Play(aniName[3]) self.m_HasShowFx[3] = true end
    end
end
function LuaYuanDan2023YuanDanQiYuanTopRightWnd:SetFxActive(show)
    if self.m_AniBg == nil then
        self.m_AniBg = {}
        self.m_AniBg[1] = self.transform:Find("Texture_bg (1)").gameObject
        self.m_AniBg[2] = self.transform:Find("Texture_bg (2)").gameObject
    end
    for i=1,#self.m_AniBg do
        self.m_AniBg[i]:SetActive(show)
    end
end
function LuaYuanDan2023YuanDanQiYuanTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateYuanDanQiYuanPlayProgress",self,"UpdateProgress")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("YuanDanQiYuanOnPlayerAddScore",self,"OnPlayerAddScore")
end

function LuaYuanDan2023YuanDanQiYuanTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateYuanDanQiYuanPlayProgress",self,"UpdateProgress")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("YuanDanQiYuanOnPlayerAddScore",self,"OnPlayerAddScore")
    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
		CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
	end
end
--@region UIEvent

--@endregion UIEvent

