local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local ClientAction = import "L10.UI.ClientAction"

LuaGuildTerritorialWarsEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildTerritorialWarsEnterWnd, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaGuildTerritorialWarsEnterWnd, "Table", "Table", UITable)
RegistChildComponent(LuaGuildTerritorialWarsEnterWnd, "TemplateNode", "TemplateNode", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsEnterWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsEnterWnd, "MapButton", "MapButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsEnterWnd, "JoinButton", "JoinButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsEnterWnd, "BottomLabel", "BottomLabel", UILabel)

--@endregion RegistChildComponent end

function LuaGuildTerritorialWarsEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.MapButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMapButtonClick()
	end)

	UIEventListener.Get(self.JoinButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJoinButtonClick()
	end)

    --@endregion EventBind end
end

function LuaGuildTerritorialWarsEnterWnd:Init()
	self.TimeLabel.text = GuildTerritoryWar_Season.GetData(GuildTerritoryWar_Setting.GetData().CurrentSeasonId).EnterWndTopText
	self:InitParagraphItems()
	self.BottomLabel.text = g_MessageMgr:FormatMessage("GuildTerritorialWarsEnterWnd_BottomText")
end

--@region UIEvent

function LuaGuildTerritorialWarsEnterWnd:OnMapButtonClick()
	Gac2Gas.RequestTerritoryWarMapOverview()
end

function LuaGuildTerritorialWarsEnterWnd:OnJoinButtonClick()
	ClientAction.DoAction(GuildLeagueCross_Setting.GetData().GoToAction)
	CUIManager.CloseUI(CLuaUIResources.GuildTerritorialWarsEnterWnd)
end

--@endregion UIEvent

function LuaGuildTerritorialWarsEnterWnd:InitParagraphItems()
	self.TemplateNode:SetActive(false)
	Extensions.RemoveAllChildren(self.Table.transform)
	local msg = g_MessageMgr:FormatMessage("GuildTerritorialWarsViewEnterWnd_ReadMe")
	if System.String.IsNullOrEmpty(msg) then
        return
    end
	local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then
        return
    end
	for i = 0,info.paragraphs.Count - 1 do
		local paragraphGo = CUICommonDef.AddChild(self.Table.gameObject, self.TemplateNode)
		paragraphGo:SetActive(true)
		CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem)):Init(info.paragraphs[i], 4294967295)
	end
	self.ScrollView:ResetPosition()
    self.Table:Reposition()
end
