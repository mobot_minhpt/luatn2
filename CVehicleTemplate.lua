-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFashionVehicleRenewalMgr = import "L10.UI.CFashionVehicleRenewalMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CStatusMgr = import "L10.Game.CStatusMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CVehicleTemplate = import "L10.UI.CVehicleTemplate"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local Double = import "System.Double"
local EnumEventType = import "EnumEventType"
local EPropStatus = import "L10.Game.EPropStatus"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Object = import "System.Object"
local QnButton = import "L10.UI.QnButton"
local RenewalType = import "L10.UI.RenewalType"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local ZuoQi_Setting = import "L10.Game.ZuoQi_Setting"
local ZuoQi_ZuoQi = import "L10.Game.ZuoQi_ZuoQi"

CVehicleTemplate.m_Init_CS2LuaHook = function (this, id, tid, expTime) 
    this.iconTexture.material = nil
    this.nameLabel.text = ""
    this.positionLabel.text = ""
    this.leftTimeLabel.text = ""
    this.noRenewalGo.text = ""
    this.rideonButton:SetActive(true)
    this.rideonTag:SetActive(false)
    this.transform:Find("RenewalLabel").gameObject:SetActive(false)

    local data = ZuoQi_ZuoQi.GetData(tid)
    local zqdata = CZuoQiMgr.Inst:GetZuoQiById(id)
    if data ~= nil then
        local itemid = data.ItemID
        local itemdata = Item_Item.GetData(itemid)
        local icon = ""
        if itemdata ~= nil then
            icon = itemdata.Icon
        end

        if zqdata ~= nil and zqdata.mapiInfo ~= nil then
            local setting = ZuoQi_Setting.GetData()
            icon = setting.MapiIcon[zqdata.mapiInfo.FuseId]
            if zqdata.quality == 4 then
                icon = setting.SpecialMapiIcon[0]
            elseif zqdata.quality == 5 then
				icon = setting.SpecialMapiIcon[1]
			end
        end

        this.iconTexture:LoadMaterial(icon)



        local curtime = CServerTimeMgr.Inst.timeStamp
        local dt = CServerTimeMgr.ConvertTimeStampToZone8Time(expTime)

        local bDefaultZuoqi = (id == CClientMainPlayer.Inst.BasicProp.DefaultZuoQiId)
        this.rideonTag:SetActive(bDefaultZuoqi)

        if data.AvailableTime ~= 0 and curtime > expTime then
            CommonDefs.GetComponentInChildren_GameObject_Type(this.rideonButton, typeof(UILabel)).text = LocalString.GetString("续费")
            this.rideonButton:SetActive(data.CanRenewal ~= 0)

            UIEventListener.Get(this.rideonButton).onClick = MakeDelegateFromCSFunction(this.OnRenewalButtonClick, VoidDelegate, this)

            this.leftTimeLabel.color = Color(0.9, 0.15, 0.15, 0.8)
            this.leftTimeLabel.text = CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("已失效  {0}-{1}-{2},{3}:{4}"), {dt.Year, dt.Month, dt.Day, this:FormatTime(dt.Hour), this:FormatTime(dt.Minute)})
            if data.RenewalAvailableTime then
                local renewalCutOffTime = CServerTimeMgr.Inst:GetDayBegin(dt)
                renewalCutOffTime = renewalCutOffTime:AddDays(data.RenewalRange[0] + 1)   
                local outOfData = (renewalCutOffTime:Subtract(CServerTimeMgr.Inst:GetZone8Time())).TotalSeconds <= 0
                local isRenewalCountEnough = zqdata.renewalCount < data.RenewalAvailableTime[1]
                local canRenewal = isRenewalCountEnough and (not outOfData)
                this.rideonButton:SetActive(canRenewal)
                if canRenewal then
                    this.transform:Find("RenewalLabel").gameObject:SetActive(true)
                    this.leftTimeLabel.color = Color(1,1,1,0.8)
                    local text = SafeStringFormat(LocalString.GetString("[E52626]已失效  [999595]%d-%d-%d前可续费"), renewalCutOffTime.Year, renewalCutOffTime.Month, renewalCutOffTime.Day)
                    this.leftTimeLabel.text = CUICommonDef.TranslateToNGUIText(text)
                    UIEventListener.Get(this.rideonButton).onClick = DelegateFactory.VoidDelegate(function(go)
                        LuaVehicleRenewalMgr:OpenVehicleRenewalWnd(zqdata)
                    end)       
                end
            end
        else
            local bCurZuoQi = (id == CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId)
            local default
            if bCurZuoQi then
                default = LocalString.GetString("下马")
            else
                default = LocalString.GetString("上马")
            end
            CommonDefs.GetComponentInChildren_GameObject_Type(this.rideonButton, typeof(UILabel)).text = default
            if bCurZuoQi then
                UIEventListener.Get(this.rideonButton).onClick = MakeDelegateFromCSFunction(this.OnRideOffButtonClick, VoidDelegate, this)
            else
                UIEventListener.Get(this.rideonButton).onClick = MakeDelegateFromCSFunction(this.OnRideOnButtonClick, VoidDelegate, this)
            end

            this.leftTimeLabel.color = Color(1, 1, 1, 0.5)
            if data.AvailableTime == 0 then
                this.leftTimeLabel.text = LocalString.GetString("永久有效")
            else
                this.leftTimeLabel.text = CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("有效期至（{0}-{1}-{2},{3}:{4}）"), {dt.Year, dt.Month, dt.Day, this:FormatTime(dt.Hour), this:FormatTime(dt.Minute)})
            end
        end

        local bFeixingZuoqi = (data.BarrierType > 1)
        local extern
        if bFeixingZuoqi then
            extern = LocalString.GetString("飞行")
        else
            extern = LocalString.GetString("地面")
        end
        this.positionLabel.text = extern


        this.vehicleID = id
        this.vehicleTID = tid

        this.nameLabel.text = data.Name

        if zqdata ~= nil and zqdata.mapiInfo ~= nil then
            this.nameLabel.text = zqdata.mapiInfo.Name
        end
        if CZuoQiMgr.IsMapiTemplateId(tid) then
            this.leftTimeLabel.gameObject:SetActive(false)
        end
    end

    CUICommonDef.SetActive(this.deleteButton, not CZuoQiMgr.IsMapiTemplateId(tid), true)
end
CVehicleTemplate.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.deleteButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.deleteButton).onClick, MakeDelegateFromCSFunction(this.OnDeleteButtonClickd, VoidDelegate, this), true)
    EventManager.AddListener(EnumEventType.OnRideOnOffVehicle, MakeDelegateFromCSFunction(this.OnRideOnOffVehicle, Action0, this))
    EventManager.AddListener(EnumEventType.OnSetDefaultVehicle, MakeDelegateFromCSFunction(this.OnSetDefaultVehicle, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.OnRenewVehicle, MakeDelegateFromCSFunction(this.OnRenewVehicle, MakeGenericClass(Action3, String, UInt32, Double), this))
    EventManager.AddListenerInternal(EnumEventType.OnSyncMapiInfo, MakeDelegateFromCSFunction(this.OnSyncMapiInfo, MakeGenericClass(Action1, String), this))
end
CVehicleTemplate.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.deleteButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.deleteButton).onClick, MakeDelegateFromCSFunction(this.OnDeleteButtonClickd, VoidDelegate, this), false)
    EventManager.RemoveListener(EnumEventType.OnRideOnOffVehicle, MakeDelegateFromCSFunction(this.OnRideOnOffVehicle, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnSetDefaultVehicle, MakeDelegateFromCSFunction(this.OnSetDefaultVehicle, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.OnRenewVehicle, MakeDelegateFromCSFunction(this.OnRenewVehicle, MakeGenericClass(Action3, String, UInt32, Double), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnSyncMapiInfo, MakeDelegateFromCSFunction(this.OnSyncMapiInfo, MakeGenericClass(Action1, String), this))
end
CVehicleTemplate.m_OnSyncMapiInfo_CS2LuaHook = function (this, zuoqiId) 
    if this.vehicleID == zuoqiId then
        local zqdata = CZuoQiMgr.Inst:GetZuoQiById(zuoqiId)
        if zqdata ~= nil and zqdata.mapiInfo ~= nil then
            this:Init(zuoqiId, zqdata.templateId, zqdata.expTime)
        end
    end
end
CVehicleTemplate.m_OnSetDefaultVehicle_CS2LuaHook = function (this) 
    local vid = CClientMainPlayer.Inst.BasicProp.DefaultZuoQiId

    if vid ~= nil and vid == this.vehicleID then
        this.rideonTag:SetActive(true)
    else
        this.rideonTag:SetActive(false)
    end
end
CVehicleTemplate.m_OnRideOnButtonClick_CS2LuaHook = function (this, go) 

    if CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("TeamFollow")) == 1 then
        g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("组队跟随状态下不能上坐骑！"))
        return
    end

    if this.vehicleID ~= nil then
        if not CZuoQiMgr.Inst:CheckMapiNotInMajiu(this.vehicleID) then
            return
        end
        Gac2Gas.RequestRideOnZuoQi(this.vehicleID)

        if this.onQnButtonClick ~= nil then
            GenericDelegateInvoke(this.onQnButtonClick, Table2ArrayWithCount({CommonDefs.GetComponent_GameObject_Type(this.gameObject, typeof(QnButton))}, 1, MakeArrayClass(Object)))
        end
    end
end
CVehicleTemplate.m_OnRideOffButtonClick_CS2LuaHook = function (this, go) 

    if CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("TeamFollow")) == 1 then
        g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("组队跟随状态下不能下坐骑！"))
        return
    end

    if this.vehicleID ~= nil then
        Gac2Gas.RequestRideOffZuoQi(this.vehicleID)
        if this.onQnButtonClick ~= nil then
            GenericDelegateInvoke(this.onQnButtonClick, Table2ArrayWithCount({CommonDefs.GetComponent_GameObject_Type(this.gameObject, typeof(QnButton))}, 1, MakeArrayClass(Object)))
        end
    end
end
CVehicleTemplate.m_OnRenewalButtonClick_CS2LuaHook = function (this, go) 

    if this.onQnButtonClick ~= nil then
        GenericDelegateInvoke(this.onQnButtonClick, Table2ArrayWithCount({CommonDefs.GetComponent_GameObject_Type(this.gameObject, typeof(QnButton))}, 1, MakeArrayClass(Object)))
    end

    CFashionVehicleRenewalMgr.type = RenewalType.VEHICLE

    CommonDefs.DictClear(CFashionVehicleRenewalMgr.vehicleRenewalList)
    CommonDefs.DictAdd(CFashionVehicleRenewalMgr.vehicleRenewalList, typeof(String), this.vehicleID, typeof(UInt32), this.vehicleTID)
    CUIManager.ShowUI(CUIResources.FashionRenewalWnd)
end
CVehicleTemplate.m_OnDeleteButtonClickd_CS2LuaHook = function (this, go) 

    local data = ZuoQi_ZuoQi.GetData(this.vehicleTID)
    if data ~= nil and this.vehicleID ~= nil then
        local context = g_MessageMgr:FormatMessage("Zuoqi_Discard_Notice", data.Name)
        MessageWndManager.ShowOKCancelMessage(context, DelegateFactory.Action(function () 
            Gac2Gas.RequestDiscardZuoQi(this.vehicleID, false)
        end), nil, nil, nil, false)
    end
end
CVehicleTemplate.m_GetRideOnButton_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return nil
    end
    local bCurZuoQi = (this.vehicleID == CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId)
    if bCurZuoQi then
        return nil
    else
        if not this.rideonButton.activeSelf then
            return nil
        end
        --考虑坐骑失效的情况
        local text = CommonDefs.GetComponentInChildren_GameObject_Type(this.rideonButton, typeof(UILabel)).text
        if text ~= LocalString.GetString("上马") then
            return nil
        end
        return this.rideonButton
    end
end
