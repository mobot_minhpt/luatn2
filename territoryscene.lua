function CTerritoryScene:Ctor()
	self.m_Grid = {}
	self.m_GridWidth = 5
	self.m_GridHeight = 5

	self.m_BarrierListCache = {}
	self.m_BarrierMapCache = {}
end

function CTerritoryScene:ForeachUnitBarrierDo(designData, dir, foo)
	local function rotate(x, y)
		if dir % 360 == 90 then
			return y, - x
		elseif dir % 360 == 180 then
			return - x, - y
		elseif dir %360 == 270 then
			return - y, x
		else
			return x, y
		end
	end

	local width = designData.Width
	local height = designData.Height
	local xCenter, yCenter = math.floor((width - 1) / 2), math.floor((height - 1) / 2)
	for row = 0, height - 1 do
		for col = 0, width - 1 do
			local rx, ry = rotate(col - xCenter, row - yCenter)
			if foo then foo(rx, ry) end
		end
	end
end


function CTerritoryScene:GetUnitBarrierList(designData, dir)
--	print("CTerritoryScene:GetUnitBarrierList", dir)
	if self.m_BarrierListCache[designData.ID] and self.m_BarrierListCache[designData.ID][dir] then
		return self.m_BarrierListCache[designData.ID][dir]
	else
		local barrierList = {}
		self:ForeachUnitBarrierDo(designData, dir, function(x, y)
			local idx = (x + math.floor(designData.Height/2))*designData.Width+y+math.floor(designData.Width/2) + 1
			local b = designData.Barrier and designData.Barrier[idx] or EBarrierType.eBT_HighBarrier
			table.insert(barrierList, {x = x, y = y, barrierType = b})
		end)
		if not self.m_BarrierListCache[designData.ID] then
			self.m_BarrierListCache[designData.ID] = {}
		end
		self.m_BarrierListCache[designData.ID][dir] = barrierList
		return barrierList
	end
end

function CTerritoryScene:GetUnitBarrierMap(designData, dir)
	if self.m_BarrierMapCache[designData.ID] and self.m_BarrierMapCache[designData.ID][dir] then
		return self.m_BarrierMapCache[designData.ID][dir]
	else
		local barrierMap = {}
		self:ForeachUnitBarrierDo(designData, dir, function(x, y)
			barrierMap[x] = barrierMap[x] or {}
			local idx = (x+math.floor(designData.Height/2))*designData.Width+y+math.floor(designData.Width/2) + 1
			barrierMap[x][y] = designData.Barrier and designData.Barrier[idx] or EBarrierType.eBT_HighBarrier
		end)
		if not self.m_BarrierMapCache[designData.ID] then
			self.m_BarrierMapCache[designData.ID] = {}
		end
		self.m_BarrierMapCache[designData.ID][dir] = barrierMap
		return barrierMap
	end
end

function CTerritoryScene:GetValidRoadTypeSet()
	local validRoadTypes = {ERoadInfo.eRI_PLAY2}
	local validRoadSet = 0
	for _, tp in ipairs(validRoadTypes) do
		validRoadSet = bit.bor(validRoadSet, bit.lshift(1, tp))
	end
	return validRoadSet
end

function CTerritoryScene:IsValidRoadType(tp, validRoadSet)
	return bit.band(tp, validRoadSet) ~= 0
end

function CTerritoryScene:CanMoveCityUnit(cityId, left, bottom, right, top, unitId, x, y, dir, designData, getUnitInfo, getOriginalGridBarrierAndInfo, isAnythingInGrid)

	local barrierList = self:GetUnitBarrierList(designData, dir)
	local validRoadSet = self:GetValidRoadTypeSet()

	-- 检查城池区域
	for _, b in ipairs(barrierList) do
		local xx, yy = b.x + x, b.y + y
		if xx < left or xx > right or yy < bottom or yy > top then

			return
		end
	end

	-- 检查障碍
	for _, b in ipairs(barrierList) do
		local xx, yy = b.x + x, b.y + y
		local ob, t = getOriginalGridBarrierAndInfo(xx, yy)
		-- 无障碍才让放
		if b.barrierType > 0 and ob ~= EBarrierType.eBT_NoBarrier then

			return
		end
		if b.barrierType > 0 and isAnythingInGrid and isAnythingInGrid(xx, yy) then

			return
		end
		if not self:IsValidRoadType(t, validRoadSet) then

			return
		end
	end

	-- 检查其他单位
	local candidates = {}
	local bleft, bright, bbottom, btop = self:GetGridRect(barrierList, x, y)
	local cityGrid = self.m_Grid[cityId]
	for row = bbottom, btop do
		for col = bleft, bright do
			if cityGrid and cityGrid[row] and cityGrid[row][col] then
				for otherUnitId, _ in pairs(cityGrid[row][col]) do
					candidates[otherUnitId] = true
				end
			end
		end
	end
	for otherUnitId, _ in pairs(candidates) do
		local info = getUnitInfo(cityId, otherUnitId)
		if info then
			local otherBarrierMap = self:GetUnitBarrierMap(info.designData, info.dir)
			for _, b in ipairs(barrierList) do
				local xx, yy = b.x + x - info.x, b.y + y - info.y
				if b.barrierType > 0 and otherBarrierMap and otherBarrierMap[xx] and otherBarrierMap[xx][yy] and otherBarrierMap[xx][yy] > 0 and otherUnitId ~= unitId then


					return
				end
			end
		else

			return
		end
	end
	return true
end

function CTerritoryScene:MoveCityUnit(cityId, unitId, ox, oy, odir, x, y, dir, designData, getGridBarrierAndOriginalGridBarrier, setGridBarrier)
	if ox and oy and odir and getGridBarrierAndOriginalGridBarrier and setGridBarrier then
		local barrierList = self:GetUnitBarrierList(designData, odir)
		self:RemoveCityUnitFromMap(cityId, unitId, barrierList, ox, oy)
		for _, b in ipairs(barrierList) do
			local xx, yy = b.x + ox, b.y + oy
			local cb, ob = getGridBarrierAndOriginalGridBarrier(xx, yy)
			if cb ~= EBarrierType.eBT_OutRange then
				setGridBarrier(xx, yy, ob)
			end
		end
	end

	if x and y and dir and getGridBarrierAndOriginalGridBarrier and setGridBarrier then
		local barrierList = self:GetUnitBarrierList(designData, dir)
		self:AddCityUnitToMap(cityId, unitId, barrierList, x, y)
		for _, b in ipairs(barrierList) do
			local xx, yy, barrierType = b.x + x, b.y + y, b.barrierType
			local cb, ob = getGridBarrierAndOriginalGridBarrier(xx, yy)
			if cb ~= EBarrierType.eBT_OutRange and barrierType >= cb then
				setGridBarrier(xx, yy, barrierType)
			end
		end
	end
end

function CTerritoryScene:GetGridRect(barrierList, x, y)
	local left, right, bottom, top
	for _, b in ipairs(barrierList) do
		local xx, yy = b.x + x, b.y + y
		if not left or left > xx then
			left = xx
		end
		if not right or right < xx then
			right = xx
		end
		if not bottom or bottom > yy then
			bottom = yy
		end
		if not top or top < yy then
			top = yy
		end
	end
	left = left and math.floor(left / self.m_GridWidth) or 0
	right = right and math.ceil(right / self.m_GridWidth) or -1
	bottom = bottom and math.floor(bottom / self.m_GridHeight) or 0
	top = top and math.ceil(top / self.m_GridHeight) or -1
	return left, right, bottom, top
end

function CTerritoryScene:AddCityUnitToMap(cityId, unitId, barrierList, x, y)
	local left, right, bottom, top = self:GetGridRect(barrierList, x, y)
	self.m_Grid[cityId] = self.m_Grid[cityId] or {}
	local cityGrid = self.m_Grid[cityId]
	for row = bottom, top do
		for col = left, right do
			cityGrid[row] = cityGrid[row] or {}
			cityGrid[row][col] = cityGrid[row][col] or {}
			cityGrid[row][col][unitId] = true
		end
	end

end

function CTerritoryScene:RemoveCityUnitFromMap(cityId, unitId, barrierList, x, y)
	local left, right, bottom, top = self:GetGridRect(barrierList, x, y)
	local cityGrid = self.m_Grid[cityId]
	for row = bottom, top do
		for col = left, right do
			if cityGrid and cityGrid[row] and cityGrid[row][col] then
				cityGrid[row][col][unitId] = nil
				if not next(cityGrid[row][col]) then
					cityGrid[row][col] = nil
				end
				if not next(cityGrid[row]) then
					cityGrid[row] = nil
				end
			end
		end
	end

end

-- 建造复活点检查
function CTerritoryScene:CanBuildRebornPoint(left1, bottom1, right1, top1, left2, bottom2, right2, top2, x, y, dir, getGridBarrierAndInfo)


	-- 边界未获取到则不可建
	if left1 <= 0 or bottom1 <= 0 or right1 <= 0 or top1 <= 0 then
		return false
	end
	if left2 <= 0 or bottom2 <= 0 or right2 <= 0 or top2 <= 0 then
		return false
	end

	-- 两个城内不可建
	if x >= left1 and x <= right1 and y >= bottom1 and y <= top1 then
		return false, "KFCZ_BUILD_REBORN_POINT_AREA_LIMIT"
	end
	if x >= left2 and x <= right2 and y >= bottom2 and y <= top2 then
		return false, "KFCZ_BUILD_REBORN_POINT_AREA_LIMIT"
	end

	-- 无障和低障能刷怪
	local b, t = getGridBarrierAndInfo(x, y)
	if not (b == EBarrierType.eBT_NoBarrier or b == EBarrierType.eBT_LowBarrier) then
		return false, "KFCZ_BUILD_REBORN_POINT_HAS_BARRIER"
	end

	return true
end
