local UITexture = import "UITexture"
local Color = import "UnityEngine.Color"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CForcesMgr = import "L10.Game.CForcesMgr"

LuaLuoCha2021PlayingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLuoCha2021PlayingWnd, "ExpandButton", "ExpandButton", GameObject)
RegistChildComponent(LuaLuoCha2021PlayingWnd, "LuoChaKillRoot", "LuoChaKillRoot", GameObject)
RegistChildComponent(LuaLuoCha2021PlayingWnd, "LuoChaProgressLabel", "LuoChaProgressLabel", UILabel)
RegistChildComponent(LuaLuoCha2021PlayingWnd, "LuoChaProgress", "LuoChaProgress", UITexture)
RegistChildComponent(LuaLuoCha2021PlayingWnd, "LuoCha", "LuoCha", GameObject)
RegistChildComponent(LuaLuoCha2021PlayingWnd, "RenLei", "RenLei", GameObject)
RegistChildComponent(LuaLuoCha2021PlayingWnd, "RenLeiProgress", "RenLeiProgress", UITexture)
RegistChildComponent(LuaLuoCha2021PlayingWnd, "RenLeiProgressLabel", "RenLeiProgressLabel", UILabel)
RegistChildComponent(LuaLuoCha2021PlayingWnd, "RenLeiCurrentCount", "RenLeiCurrentCount", UILabel)
RegistChildComponent(LuaLuoCha2021PlayingWnd, "RightProgress", "RightProgress", UITexture)
RegistChildComponent(LuaLuoCha2021PlayingWnd, "RightProgressLabel", "RightProgressLabel", UILabel)
RegistChildComponent(LuaLuoCha2021PlayingWnd, "Lv", "Lv", UILabel)
RegistChildComponent(LuaLuoCha2021PlayingWnd, "LearnFx", "LearnFx", GameObject)

--@endregion RegistChildComponent end
-- 人类是0 罗刹是1 未判定是-1
RegistClassMember(LuaLuoCha2021PlayingWnd,"m_IsLuoCha")
RegistClassMember(LuaLuoCha2021PlayingWnd,"m_HasInited")
RegistClassMember(LuaLuoCha2021PlayingWnd,"m_LuoChaKillMark")
RegistClassMember(LuaLuoCha2021PlayingWnd,"m_LuoChaKillFx")
RegistClassMember(LuaLuoCha2021PlayingWnd,"m_MaxPickCount")
RegistClassMember(LuaLuoCha2021PlayingWnd,"m_CanLearnSkill")
-- 信息
RegistClassMember(LuaLuoCha2021PlayingWnd,"m_CurSumPickCount")
RegistClassMember(LuaLuoCha2021PlayingWnd,"m_RenLeiCurrentPickCount")
RegistClassMember(LuaLuoCha2021PlayingWnd,"m_RenLeiMaxPickCount")
RegistClassMember(LuaLuoCha2021PlayingWnd,"m_RenLeiUpgradeNeedPickCount")

function LuaLuoCha2021PlayingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)


	
	UIEventListener.Get(self.RightProgress.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightProgressClick()
	end)


    --@endregion EventBind end
end

function LuaLuoCha2021PlayingWnd:Init()
    self.m_LuoChaKillMark = {}
    self.m_LuoChaKillFx = {}

    for i=1, 5 do
        local mark = self.LuoChaKillRoot.transform:Find(tostring(i).."/Mark").gameObject
        local fx = self.LuoChaKillRoot.transform:Find(tostring(i).."/Fx"):GetComponent(typeof(CUIFx))
        table.insert(self.m_LuoChaKillMark, mark)
        table.insert(self.m_LuoChaKillFx, fx)
    end

    self:InitDesignData()

    self.m_RenLeiCurrentPickCount = 0
    self.m_RenLeiMaxPickCount = 2
    self.m_CurSumPickCount = 0
    self.m_RenLeiUpgradeNeedPickCount = 8
    self.LuoChaProgress.fillAmount = 0
    self.RenLeiProgress.fillAmount = 0
    self.m_IsLuoCha = -1
    self:CheckStatus()
    self.m_CanLearnSkill = false
    self.m_HasInited = true
    self.RenLeiCurrentCount.text = SafeStringFormat3("%s/%s", tostring(self.m_RenLeiCurrentPickCount), tostring(self.m_RenLeiMaxPickCount))

    -- 尝试缓存初始化
    self:InitFromCacheData()
end

function LuaLuoCha2021PlayingWnd:CheckFxRoot()
    if self.m_LuoChaKillMark == nil or self.m_LuoChaKillFx == nil then
        self.m_LuoChaKillMark = {}
        self.m_LuoChaKillFx = {}

        for i=1, 5 do
            local mark = self.LuoChaKillRoot.transform:Find(tostring(i).."/Mark").gameObject
            local fx = self.LuoChaKillRoot.transform:Find(tostring(i).."/Fx"):GetComponent(typeof(CUIFx))
            table.insert(self.m_LuoChaKillMark, mark)
            table.insert(self.m_LuoChaKillFx, fx)
        end
    end
end

function LuaLuoCha2021PlayingWnd:InitFromCacheData()
    -- 检查阵营
    self:CheckStatus()

    self.m_CurSumPickCount = LuaLuoCha2021Mgr.m_SumPickCount
    self.m_RenLeiCurrentPickCount = LuaLuoCha2021Mgr.m_RenLeiCurrentPickCount
    self.m_RenLeiMaxPickCount = LuaLuoCha2021Mgr.m_RenLeiMaxPickCount
    self.m_RenLeiUpgradeNeedPickCount = LuaLuoCha2021Mgr.m_RenLeiUpgradeNeedPickCount

    -- 上方
    if self.m_IsLuoCha == 1 then
        self:SyncLuoChaPlayerAliveCount(LuaLuoCha2021Mgr.m_RenLeiAliveCount)

        local percent = LuaLuoCha2021Mgr.m_SumPickCount / self.m_MaxPickCount
        self.LuoChaProgress.fillAmount = percent
        self.LuoChaProgressLabel.text = SafeStringFormat3("%s/%s", tostring(LuaLuoCha2021Mgr.m_SumPickCount), tostring(self.m_MaxPickCount))

        if percent > 0.75 then
            self.LuoChaProgress.color = Color.red
            self.LuoChaProgressLabel.color = Color.red
        else
            self.LuoChaProgress.color = Color.yellow
            self.LuoChaProgressLabel.color = Color.yellow
        end
    else
        self.RenLeiCurrentCount.text = SafeStringFormat3("%s/%s", tostring(self.m_RenLeiCurrentPickCount), tostring(self.m_RenLeiMaxPickCount))

        local percent = LuaLuoCha2021Mgr.m_SumPickCount / self.m_MaxPickCount
        self.RenLeiProgress.fillAmount = percent
        self.RenLeiProgressLabel.text = SafeStringFormat3("%s/%s", tostring(LuaLuoCha2021Mgr.m_SumPickCount), tostring(self.m_MaxPickCount))

        if percent > 0.75 then
            self.RenLeiProgress.color = Color.red
            self.RenLeiProgressLabel.color = Color.red
        else
            self.RenLeiProgress.color = Color.yellow
            self.RenLeiProgressLabel.color = Color.yellow
        end
    end
    

    -- 右侧进度
    if self.m_IsLuoCha == 1 then
        local percent = LuaLuoCha2021Mgr.m_LuoChaPower / LuaLuoCha2021Mgr.m_LuoChaUpgradeNeedPower
        self.Lv.text = "lv." .. tostring(LuaLuoCha2021Mgr.m_Level)
        self.Lv.gameObject:SetActive(true)
        self.RightProgressLabel.text = SafeStringFormat3("%d/%d", LuaLuoCha2021Mgr.m_LuoChaPower, LuaLuoCha2021Mgr.m_LuoChaUpgradeNeedPower)
        self.RightProgress.fillAmount = percent
    else
        local percent = LuaLuoCha2021Mgr.m_SumPickCount / LuaLuoCha2021Mgr.m_RenLeiUpgradeNeedPickCount
        self.Lv.gameObject:SetActive(false)
        self.RightProgressLabel.text = SafeStringFormat3("%d/%d", LuaLuoCha2021Mgr.m_SumPickCount, LuaLuoCha2021Mgr.m_RenLeiUpgradeNeedPickCount )
        self.RightProgress.fillAmount = percent
    end
    
    if LuaLuoCha2021Mgr.m_LearnSkillCount > 0 then
        self.m_CanLearnSkill = true
        self.LearnFx:SetActive(true)
    else
        self.m_CanLearnSkill = false
        self.LearnFx:SetActive(false)
    end
end

function LuaLuoCha2021PlayingWnd:InitDesignData()
    self.m_MaxPickCount = LuoChaHaiShi_Setting.GetData().FortNeedPickCount
end

function LuaLuoCha2021PlayingWnd:CheckStatus()
    if CForcesMgr.Inst and CClientMainPlayer.Inst then
        -- 罗刹是1 人类是0
        self.m_IsLuoCha = CForcesMgr.Inst:GetPlayerForce(CClientMainPlayer.Inst.Id)
        self:SetStatus()
    end
end

function LuaLuoCha2021PlayingWnd:SyncLuoChaPlayerAliveCount(aliveCount)
    self:CheckStatus()
    self:CheckFxRoot()
    if self.m_IsLuoCha == 1 then
        for i=1, #self.m_LuoChaKillMark do
            local status = i <= 5- aliveCount
            if status ~= self.m_LuoChaKillMark[i].activeSelf then
                self.m_LuoChaKillMark[i]:SetActive(i <= 5- aliveCount)
                if not status then
                    self.m_LuoChaKillFx[i]:LoadFx("fx/ui/prefab/UI_luoshahaishi_xiufu.prefab")
                else
                    self.m_LuoChaKillFx[i]:LoadFx("fx/ui/prefab/UI_luoshahaishi_jibai.prefab")
                end
            end
        end
    end
end

-- 个人当前拾取
function LuaLuoCha2021PlayingWnd:SyncLuoChaDefenderPickInfo(pickInfo)
    self:CheckStatus()
    self.m_RenLeiCurrentPickCount = pickInfo
    self.RenLeiCurrentCount.text = SafeStringFormat3("%s/%s", tostring(pickInfo), tostring(self.m_RenLeiMaxPickCount))
end

-- 总体进度
function LuaLuoCha2021PlayingWnd:UpdateLuoChaFortPickCount(fortPickCount)
    self:CheckStatus()

    self.m_CurSumPickCount = fortPickCount
    local percent = fortPickCount/self.m_MaxPickCount
    if self.m_IsLuoCha == 1 then
        self.LuoChaProgress.fillAmount = percent
        self.LuoChaProgressLabel.text = SafeStringFormat3("%s/%s", tostring(fortPickCount), tostring(self.m_MaxPickCount))

        if percent > 0.75 then
            self.LuoChaProgress.color = Color.red
            self.LuoChaProgressLabel.color = Color.red
        else
            self.LuoChaProgress.color = Color.yellow
            self.LuoChaProgressLabel.color = Color.yellow
        end
    elseif self.m_IsLuoCha == 0 then
        self.RenLeiProgress.fillAmount = percent
        self.RenLeiProgressLabel.text = SafeStringFormat3("%s/%s", tostring(fortPickCount), tostring(self.m_MaxPickCount))
        if percent > 0.75 then
            self.RenLeiProgress.color = Color.red
            self.RenLeiProgressLabel.color = Color.red
        else
            self.RenLeiProgress.color = Color.yellow
            self.RenLeiProgressLabel.color = Color.yellow
        end
        self.RightProgressLabel.text = SafeStringFormat3("%d/%d", fortPickCount, self.m_RenLeiUpgradeNeedPickCount )
        self.RightProgress.fillAmount = fortPickCount/self.m_RenLeiUpgradeNeedPickCount
    end
end

function LuaLuoCha2021PlayingWnd:SetStatus()
    self.LuoCha:SetActive(self.m_IsLuoCha == 1)
    self.RenLei:SetActive(not (self.m_IsLuoCha == 1))
end

-- 罗刹玩家信息同步
function LuaLuoCha2021PlayingWnd:SyncLuoChaAttackerLevelInfo(level, power, upgradeNeedPower, learnedSkillCount, skillCount)
    self:CheckStatus()
    self.Lv.text = "lv." .. tostring(level)
    self.RightProgressLabel.text = SafeStringFormat3("%d/%d", power, upgradeNeedPower)
    self.RightProgress.fillAmount = power/upgradeNeedPower

    if skillCount > learnedSkillCount then
        self.m_CanLearnSkill = true
        self.LearnFx:SetActive(true)
    else
        self.m_CanLearnSkill = false
        self.LearnFx:SetActive(false)
    end
end

-- 人类玩家信息同步
function LuaLuoCha2021PlayingWnd:SyncLuoChaDefenderLevelInfo(level, learnedSkillCount, skillCount, maxPickCount, upgradeNeedPick)
    self:CheckStatus()
    self.Lv.text = "lv." .. tostring(level)
    self.Lv.gameObject:SetActive(false)

    -- 学习技能的进度
    local curCount = self.m_CurSumPickCount
    self.m_RenLeiUpgradeNeedPickCount = upgradeNeedPick
    self.RightProgressLabel.text = SafeStringFormat3("%d/%d", curCount, upgradeNeedPick)
    self.RightProgress.fillAmount = curCount/upgradeNeedPick

    self.m_RenLeiMaxPickCount = maxPickCount
    self.RenLeiCurrentCount.text = SafeStringFormat3("%s/%s", tostring(self.m_RenLeiCurrentPickCount), tostring(self.m_RenLeiMaxPickCount))

    if skillCount > learnedSkillCount then
        self.m_CanLearnSkill = true
        self.LearnFx:SetActive(true)
    else
        self.m_CanLearnSkill = false
        self.LearnFx:SetActive(false)
    end
end

function LuaLuoCha2021PlayingWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SyncLuoChaPlayerAliveCount", self, "SyncLuoChaPlayerAliveCount")
    g_ScriptEvent:AddListener("SyncLuoChaDefenderPickInfo", self, "SyncLuoChaDefenderPickInfo")
    g_ScriptEvent:AddListener("UpdateLuoChaFortPickCount", self, "UpdateLuoChaFortPickCount")
    g_ScriptEvent:AddListener("SyncLuoChaDefenderLevelInfo", self, "SyncLuoChaDefenderLevelInfo")
    g_ScriptEvent:AddListener("SyncLuoChaAttackerLevelInfo", self, "SyncLuoChaAttackerLevelInfo")
end

function LuaLuoCha2021PlayingWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncLuoChaPlayerAliveCount", self, "SyncLuoChaPlayerAliveCount")
    g_ScriptEvent:RemoveListener("SyncLuoChaDefenderPickInfo", self, "SyncLuoChaDefenderPickInfo")
    g_ScriptEvent:RemoveListener("UpdateLuoChaFortPickCount", self, "UpdateLuoChaFortPickCount")
    g_ScriptEvent:RemoveListener("SyncLuoChaDefenderLevelInfo", self, "SyncLuoChaDefenderLevelInfo")
    g_ScriptEvent:RemoveListener("SyncLuoChaAttackerLevelInfo", self, "SyncLuoChaAttackerLevelInfo")
    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end
    LuaLuoCha2021Mgr:ClearCacheInfo()
end

function LuaLuoCha2021PlayingWnd:OnHideTopAndRightTipWnd()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

--@region UIEvent

function LuaLuoCha2021PlayingWnd:OnExpandButtonClick()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaLuoCha2021PlayingWnd:OnRightProgressClick()
    if self.m_CanLearnSkill then
        CUIManager.ShowUI(CLuaUIResources.LuoCha2021LearnSkillWnd)
    else
        -- 收集物资或击杀人类可增加进度，进度满后可学习新技能
        if self.m_IsLuoCha == 1 then
            g_MessageMgr:ShowMessage("LuoChaHaiShi_Learn_Skill_Tip_LuoCha")
        else
            g_MessageMgr:ShowMessage("LuoChaHaiShi_Learn_Skill_Tip_RenLei")
        end
    end
end


--@endregion UIEvent

