-- Auto Generated!!
local CLingShouSkillSwitchSuccessWnd = import "L10.UI.CLingShouSkillSwitchSuccessWnd"
local CLingShouSwitchSkillMgr = import "L10.Game.CLingShouSwitchSkillMgr"
CLingShouSkillSwitchSuccessWnd.m_GetLingShouDetails_CS2LuaHook = function (this, lingShouId, details) 
    if CLingShouSwitchSkillMgr.Inst.destLingShouId == lingShouId then
        --更新界面
        if details ~= nil then
            --加载技能
            this.nameLabel.text = details.data.Name
            this.textureLoader:Init(details)
            this.slots:InitSlots(details.data.Props.SkillListForSave)
        else
            this.slots:InitNull()
            this.textureLoader:InitNull()
            this.nameLabel.text = ""
        end
    end
end
