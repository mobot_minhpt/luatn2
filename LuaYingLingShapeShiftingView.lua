LuaYingLingShapeShiftingView = class()
RegistChildComponent(LuaYingLingShapeShiftingView,"m_LeftButtonPos","LeftButtonPos", GameObject)
RegistChildComponent(LuaYingLingShapeShiftingView,"m_RightButtonPos","RightButtonPos", GameObject)
RegistChildComponent(LuaYingLingShapeShiftingView,"m_ButtonMale","ButtonMale", GameObject)
RegistChildComponent(LuaYingLingShapeShiftingView,"m_ButtonFemale","ButtonFemale", GameObject)
RegistChildComponent(LuaYingLingShapeShiftingView,"m_ButtonMonster","ButtonMonster", GameObject)
RegistChildComponent(LuaYingLingShapeShiftingView,"m_Bg","Bg", CUITexture)
RegistClassMember(LuaYingLingShapeShiftingView,"m_States")
RegistClassMember(LuaYingLingShapeShiftingView,"m_PresupposedStates")
RegistClassMember(LuaYingLingShapeShiftingView,"m_BtnsTable")

function LuaYingLingShapeShiftingView:Start()
    CommonDefs.AddOnClickListener(self.m_ButtonMale, DelegateFactory.Action_GameObject(function(go) self:OnButtonClick(go, EnumYingLingState.eMale) end), false)
    CommonDefs.AddOnClickListener(self.m_ButtonFemale, DelegateFactory.Action_GameObject(function(go) self:OnButtonClick(go, EnumYingLingState.eFemale) end), false)
    CommonDefs.AddOnClickListener(self.m_ButtonMonster, DelegateFactory.Action_GameObject(function(go) self:OnButtonClick(go, EnumYingLingState.eMonster) end), false)
end

function LuaYingLingShapeShiftingView:OnEnable()
    g_ScriptEvent:AddListener("UpdateYingLingState", self, "UpdateYingLingState")
    self:InitPresupposedStates()
    self:InitDisplay()
end

function LuaYingLingShapeShiftingView:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateYingLingState", self, "UpdateYingLingState")
end

function LuaYingLingShapeShiftingView:Update()
    if not CUICommonDef.CheckPressWhenButtonDown(self.transform, true) then
        self.gameObject:SetActive(false)
    end
end

function LuaYingLingShapeShiftingView:OnButtonClick(go, state)
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer then
        local skillId = mainplayer.SkillProp:GetSkillIdWithDeltaByCls(Skill_Setting.GetData().YingLingBianShenSkillId, mainplayer.Level)
        local realSkillId = mainplayer.SkillProp:GetAlternativeSkillIdWithDelta(skillId, mainplayer.Level)
        local remain = mainplayer.CooldownProp:GetServerRemainTime(realSkillId)
        if remain<=0 then
            mainplayer:StopAutoCastSkill(true)
        end
    end
    LuaSkillMgr.RequestYingLingTransform(state)
    self.gameObject:SetActive(false)
end

function LuaYingLingShapeShiftingView:UpdateYingLingState(args)
    if not args then return end
    
    local engineId = args[0]
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer and mainplayer.EngineId == engineId then
        self:InitDisplay()
    end
end

function LuaYingLingShapeShiftingView:InitDisplay()
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return end
    local yinglingState = mainplayer and mainplayer.CurYingLingState and EnumToInt(mainplayer.CurYingLingState) or 0
    self.m_Bg:LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/raw_yinglingskill_0%d.mat", yinglingState) , false)
    local presupposedStates = self.m_PresupposedStates[yinglingState]
    if not presupposedStates then return end
    self.m_ButtonMale.gameObject:SetActive(false)
    self.m_ButtonFemale.gameObject:SetActive(false)
    self.m_ButtonMonster.gameObject:SetActive(false)
    for i = 1, 2 do
        local pos = (i == 1) and self.m_LeftButtonPos or self.m_RightButtonPos
        local state = presupposedStates[i]
        local btn
        if EnumYingLingState.eMale == state then btn = self.m_ButtonMale
        elseif EnumYingLingState.eFemale == state then btn = self.m_ButtonFemale
        elseif EnumYingLingState.eMonster == state then btn = self.m_ButtonMonster
        else return end
        btn.transform:SetParent(pos.transform)
        btn.transform.localPosition = Vector3.zero
        btn.gameObject:SetActive(true)
    end
end

function LuaYingLingShapeShiftingView:InitPresupposedStates()
    self.m_PresupposedStates = {}
    self.m_PresupposedStates[EnumYingLingState.eMale] = {EnumYingLingState.eMonster, EnumYingLingState.eFemale}
    self.m_PresupposedStates[EnumYingLingState.eFemale] = {EnumYingLingState.eMale, EnumYingLingState.eMonster}
    self.m_PresupposedStates[EnumYingLingState.eMonster] = {EnumYingLingState.eMale, EnumYingLingState.eFemale}
end
