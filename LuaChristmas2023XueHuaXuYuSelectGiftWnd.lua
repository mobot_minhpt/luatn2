local GameObject = import "UnityEngine.GameObject"

local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"

local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local UILabel = import "UILabel"

local QnRadioBox = import "L10.UI.QnRadioBox"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"

local Money = import "L10.Game.Money"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaChristmas2023XueHuaXuYuSelectGiftWnd = class()
RegistClassMember(LuaChristmas2023XueHuaXuYuSelectGiftWnd, "m_HuaLiItemId")  -- 冬日华礼itemid
RegistClassMember(LuaChristmas2023XueHuaXuYuSelectGiftWnd, "m_HuaLiTexture") -- 冬日华礼图标
RegistClassMember(LuaChristmas2023XueHuaXuYuSelectGiftWnd, "m_HuaLiPrice")   -- 冬日华礼灵玉单价
RegistClassMember(LuaChristmas2023XueHuaXuYuSelectGiftWnd, "m_XinLiItemId")  -- 冬日心礼itemid
RegistClassMember(LuaChristmas2023XueHuaXuYuSelectGiftWnd, "m_XinLiTexture") -- 冬日心礼图标
RegistClassMember(LuaChristmas2023XueHuaXuYuSelectGiftWnd, "m_XinLiPrice")   -- 冬日心礼银两单价
RegistClassMember(LuaChristmas2023XueHuaXuYuSelectGiftWnd, "m_MaxCount")     -- 礼物最大个数
RegistClassMember(LuaChristmas2023XueHuaXuYuSelectGiftWnd, "m_CurrentGift")  -- 选择的礼物
RegistClassMember(LuaChristmas2023XueHuaXuYuSelectGiftWnd, "m_CurrentPrice") -- 当前单价
RegistClassMember(LuaChristmas2023XueHuaXuYuSelectGiftWnd, "m_CurrentCount") -- 当前个数

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChristmas2023XueHuaXuYuSelectGiftWnd, "GiftTabs", "GiftTabs", QnRadioBox)
RegistChildComponent(LuaChristmas2023XueHuaXuYuSelectGiftWnd, "TotalNumber", "TotalNumber", UILabel)
RegistChildComponent(LuaChristmas2023XueHuaXuYuSelectGiftWnd, "NumberButton", "NumberButton", QnAddSubAndInputButton)
RegistChildComponent(LuaChristmas2023XueHuaXuYuSelectGiftWnd, "HasMoney", "HasMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaChristmas2023XueHuaXuYuSelectGiftWnd, "AddBtn", "AddBtn", GameObject)

--@endregion RegistChildComponent end

function LuaChristmas2023XueHuaXuYuSelectGiftWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    -- 绑定添加按钮
    UIEventListener.Get(self.AddBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.HasMoney.moneyEnough then
            LuaChristmas2023Mgr.m_SelectGift = self.m_CurrentGift
            if self.m_CurrentGift == LuaChristmas2023Mgr.EnumSelectGift.HuaLi then
                LuaChristmas2023Mgr.m_SelectItemId = self.m_HuaLiItemId
                LuaChristmas2023Mgr.m_SelectTexture = self.m_HuaLiTexture
            else
                LuaChristmas2023Mgr.m_SelectItemId = self.m_XinLiItemId
                LuaChristmas2023Mgr.m_SelectTexture = self.m_XinLiTexture
            end
            LuaChristmas2023Mgr.m_SelectPrice = self.m_CurrentCount * self.m_CurrentPrice
            LuaChristmas2023Mgr.m_SelectCount = self.m_CurrentCount
            if CUIManager.IsLoaded(CLuaUIResources.Christmas2023XueHuaXuYuSendGiftWnd) then
                LuaChristmas2023Mgr:PutGift() -- 放入礼物
            end
            CUIManager.CloseUI(CLuaUIResources.Christmas2023XueHuaXuYuSelectGiftWnd)
        else
            local msg = g_MessageMgr:FormatMessage("Christmas_Share_NotEnough")
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
                LuaChristmas2023Mgr.m_SelectGift = -1
                if CUIManager.IsLoaded(CLuaUIResources.Christmas2023XueHuaXuYuSendGiftWnd) then
                    LuaChristmas2023Mgr:PutGift() -- 放入礼物
                end
                CUIManager.CloseUI(CLuaUIResources.Christmas2023XueHuaXuYuSelectGiftWnd)
            end), nil, nil, nil, false)
        end
    end)
end
-- 初始化礼物信息
function LuaChristmas2023XueHuaXuYuSelectGiftWnd:InitItem()
    local giftlist = Christmas2021_Setting.GetData().CardGift
    self.m_XinLiItemId = giftlist[0]
    self.m_XinLiPrice = giftlist[1]
    self.m_HuaLiItemId = giftlist[3]
    self.m_HuaLiPrice = giftlist[5]
    -- 华礼
    local item = Item_Item.GetData(self.m_HuaLiItemId)
    if item then
        local tf = self.GiftTabs.transform:GetChild(0)
        tf:Find("NameLabel"):GetComponent(typeof(UILabel)).text = item.Name
        self.m_HuaLiTexture = item.Icon
        tf:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
        UIEventListener.Get(tf:Find("Icon").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_HuaLiItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end)
        tf:Find("Price/Label"):GetComponent(typeof(UILabel)).text = self.m_HuaLiPrice
        tf:Find("Price/Sprite"):GetComponent(typeof(UISprite)).spriteName = Money.GetIconName(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE)
    end
    -- 心礼
    item = Item_Item.GetData(self.m_XinLiItemId)
    if item then
        local tf = self.GiftTabs.transform:GetChild(1)
        tf:Find("NameLabel"):GetComponent(typeof(UILabel)).text = item.Name
        self.m_XinLiTexture = item.Icon
        tf:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
        UIEventListener.Get(tf:Find("Icon").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_XinLiItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end)
        tf:Find("Price/Label"):GetComponent(typeof(UILabel)).text = self.m_XinLiPrice
        tf:Find("Price/Sprite"):GetComponent(typeof(UISprite)).spriteName = Money.GetIconName(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE)
    end
end
-- 初始化礼物个数按钮
function LuaChristmas2023XueHuaXuYuSelectGiftWnd:InitNumberButton()
    self.m_MaxCount = tonumber(Christmas2023_Setting.GetData().SendGiftCountLimit)
    self.NumberButton:SetMinMax(1, self.m_MaxCount, 1)
    self.NumberButton:SetValue(1, false)
    self.m_CurrentCount = 1
    self.NumberButton.onValueChanged = DelegateFactory.Action_uint(function(value)
        self.TotalNumber.text = SafeStringFormat3(LocalString.GetString("礼物数量 (%d/%d)"), value, self.m_MaxCount)
        self.HasMoney:SetCost(value * self.m_CurrentPrice) -- 计算价格
        self.m_CurrentCount = value
    end)
end
-- 初始化礼物选择grid
function LuaChristmas2023XueHuaXuYuSelectGiftWnd:InitGiftTabs()
    self.GiftTabs.OnSelect = DelegateFactory.Action_QnButton_int(function(go, index)
        if index == 0 then
            self:LoadHuaLi()
            self.m_CurrentGift = LuaChristmas2023Mgr.EnumSelectGift.HuaLi
        else
            self:LoadXinLi()
            self.m_CurrentGift = LuaChristmas2023Mgr.EnumSelectGift.XinLi
        end
    end)
    self.GiftTabs:ChangeTo(0, true)
    self.m_CurrentGift = LuaChristmas2023Mgr.EnumSelectGift.HuaLi
end
-- 加载冬日华礼
function LuaChristmas2023XueHuaXuYuSelectGiftWnd:LoadHuaLi()
    self.HasMoney:SetType(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true) -- 显示灵玉
    self.m_CurrentPrice = self.m_HuaLiPrice
    self.NumberButton:SetValue(self.m_CurrentCount, true)
end
-- 加载冬日心礼
function LuaChristmas2023XueHuaXuYuSelectGiftWnd:LoadXinLi()
    self.HasMoney:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, true) -- 显示银两
    self.m_CurrentPrice = self.m_XinLiPrice
    self.NumberButton:SetValue(self.m_CurrentCount, true)
end
function LuaChristmas2023XueHuaXuYuSelectGiftWnd:Init()
    self:InitItem()
    self:InitNumberButton()
    self:InitGiftTabs()
end

--@region UIEvent

--@endregion UIEvent

