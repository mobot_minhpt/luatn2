local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local UISearchView=import "L10.UI.UISearchView"
local QnButton=import "L10.UI.QnButton"
local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local Profession=import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local CTooltip=import "L10.UI.CTooltip"

CLuaStarBiwuZhanDuiSearchWnd = class()
CLuaStarBiwuZhanDuiSearchWnd.Path = "ui/starbiwu/LuaStarBiwuZhanDuiSearchWnd"

RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_CloseBtn")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_SearchBtn")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_ZhanDuiBtn")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_ZhanDuiBtnText")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_IncreseaBtn")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_DecreaseBtn")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_PageLabel")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_NoZhanDuiRoot")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_HasZhanDuiRoot")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_NoZhanDuiTable")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_HasZhanDuiTable")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_AdvSearchBtn")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_AdvSearchBtn2")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_AllZhanduiBtn")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_NormalSearchRoot")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_AdvSearchRoot")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_CurrentPage")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_HasZhanDui")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_Source")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_TotalPage")

RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_TableViewDataSource")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_ZhanDuiListHead")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_SortType")
RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd,"m_OnePageTotalShowTeam")
function CLuaStarBiwuZhanDuiSearchWnd:Awake()
    self.m_SearchBtn = self.transform:Find("Anchor/BottomView/NormalRoot/SearchView"):GetComponent(typeof(UISearchView))
    self.m_ZhanDuiBtn = self.transform:Find("Anchor/BottomView/Create").gameObject
    self.m_ZhanDuiBtnText = self.transform:Find("Anchor/BottomView/Create/Label"):GetComponent(typeof(UILabel))
    self.m_IncreseaBtn = self.transform:Find("Anchor/BottomView/QnIncreseAndDecreaseButton/IncreaseButton"):GetComponent(typeof(QnButton))
    self.m_DecreaseBtn = self.transform:Find("Anchor/BottomView/QnIncreseAndDecreaseButton/DecreaseButton"):GetComponent(typeof(QnButton))
    self.m_PageLabel = self.transform:Find("Anchor/BottomView/QnIncreseAndDecreaseButton/Label"):GetComponent(typeof(UILabel))
    self.m_NoZhanDuiRoot = self.transform:Find("Anchor/ZhanDuiList (1)").gameObject
    self.m_HasZhanDuiRoot = self.transform:Find("Anchor/ZhanDuiList").gameObject
    self.m_NoZhanDuiTable = self.transform:Find("Anchor/ZhanDuiList (1)/ListView"):GetComponent(typeof(QnTableView))
    self.m_HasZhanDuiTable = self.transform:Find("Anchor/ZhanDuiList/ListView"):GetComponent(typeof(QnTableView))
    self.m_AdvSearchBtn = self.transform:Find("Anchor/BottomView/NormalRoot/AdvSearchButton").gameObject
    self.m_AdvSearchBtn:SetActive(false)
    self.m_AdvSearchBtn2 = self.transform:Find("Anchor/BottomView/AdvRoot/AdvSearchButton").gameObject
    self.m_AdvSearchBtn2:SetActive(false)
    self.m_AllZhanduiBtn = self.transform:Find("Anchor/BottomView/AdvRoot/AllZhanduiButton").gameObject
    self.m_NormalSearchRoot = self.transform:Find("Anchor/BottomView/NormalRoot").gameObject
    self.m_AdvSearchRoot = self.transform:Find("Anchor/BottomView/AdvRoot").gameObject

    self.m_ZhanDuiListHead = {}
    self.m_ZhanDuiListHead[1] = self.m_HasZhanDuiRoot.transform:Find("Title/Consist"):GetComponent(typeof(UILabel))
    self.m_ZhanDuiListHead[2] = self.m_HasZhanDuiRoot.transform:Find("Title/Capacity"):GetComponent(typeof(UILabel))
    self.m_ZhanDuiListHead[3] = self.m_NoZhanDuiRoot.transform:Find("Title/Consist"):GetComponent(typeof(UILabel))
    self.m_ZhanDuiListHead[4] = self.m_NoZhanDuiRoot.transform:Find("Title/Capacity"):GetComponent(typeof(UILabel))
    self.m_SortType = 0
self.m_CurrentPage = 1
self.m_HasZhanDui = false
self.m_Source = 0
self.m_TotalPage = 1
self.m_OnePageTotalShowTeam = 7

end

-- Auto Generated!!
function CLuaStarBiwuZhanDuiSearchWnd:OnEnable( )
    UIEventListener.Get(self.m_ZhanDuiBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnZhanDuiBtnClicked(go) end)

    UIEventListener.Get(self.m_IncreseaBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnIncreaseBtnClicked(go) end)
    UIEventListener.Get(self.m_DecreaseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnDecreaseBtnClicked(go) end)
    UIEventListener.Get(self.m_PageLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClickPageLabel(go) end)

    UIEventListener.Get(self.m_AdvSearchBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnAdvSearchBtnClicked(go) end)
    UIEventListener.Get(self.m_AdvSearchBtn2).onClick = DelegateFactory.VoidDelegate(function(go) self:OnAdvSearchBtnClicked(go) end)
    UIEventListener.Get(self.m_AllZhanduiBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnAllZhanduiBtnClicked(go) end)

    g_ScriptEvent:AddListener("ReplyStarBiwuZhanDuiList", self, "ReplyStarBiwuZhanDuiList")
    g_ScriptEvent:AddListener("RequestJoinStarBiwuZhanDuiSuccess", self, "RequestJoinStarBiwuZhanDuiSuccess")
    g_ScriptEvent:AddListener("StarBiwuCancelJoinZhanDuiByPlayer", self, "StarBiwuCancelJoinZhanDuiByPlayer")
end
function CLuaStarBiwuZhanDuiSearchWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("ReplyStarBiwuZhanDuiList", self, "ReplyStarBiwuZhanDuiList")
    g_ScriptEvent:RemoveListener("RequestJoinStarBiwuZhanDuiSuccess", self, "RequestJoinStarBiwuZhanDuiSuccess")
    g_ScriptEvent:RemoveListener("StarBiwuCancelJoinZhanDuiByPlayer", self, "StarBiwuCancelJoinZhanDuiByPlayer")
    CLuaStarBiwuMgr.isQuDaoZhanDuiSearchWnd = false
end

function CLuaStarBiwuZhanDuiSearchWnd:OnClickPageLabel(go)
    local min=0
    local max=math.max(1,self.m_TotalPage)

    local val=max
    local num=0
    while val>=1 do
        num = num+1
        val = val/10
    end

    CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(min, max, self.m_CurrentPage, num, DelegateFactory.Action_int(function (val)
        self.m_PageLabel.text = tostring(val)
    end), DelegateFactory.Action_int(function (val)
        local page=math.max(1,val)
        page=math.min(self.m_TotalPage,page)
        self.m_PageLabel.text = SafeStringFormat3("%d/%d", page, self.m_TotalPage)

        if self.m_Source == 1 then
            self:SetZhanDuiListPage(page)
            --CLuaStarBiwuMgr:RequestQueryStarBiwuZhanDuiList(page)
        elseif self.m_Source == 2 then
            --Gac2Gas.SearchQmpkZhanDuiByClassAndZhiHui(CQuanMinPKMgr.Inst.m_SearchZhanduiClass, CQuanMinPKMgr.Inst.m_SearchZhanduiCommandIndex, page)
        end
    end), self.m_PageLabel, CTooltip.AlignType.Top, true)
end

function CLuaStarBiwuZhanDuiSearchWnd:OnReplySelfPersonalInfo()
    local info=CQuanMinPKMgr.Inst.m_MyPersonalInfo
    if info then
        if info.m_ZhanDuiId>0 then--有战队
        else--没有战队
        end
        self:Init()
    end
end

function CLuaStarBiwuZhanDuiSearchWnd:OnIncreaseBtnClicked( go)
    if self.m_CurrentPage >= self.m_TotalPage then
        return
    end
    if self.m_Source == 1 then
        self:SetZhanDuiListPage(self.m_CurrentPage + 1)
        --CLuaStarBiwuMgr:RequestQueryStarBiwuZhanDuiList(self.m_CurrentPage + 1)
    elseif self.m_Source == 2 then
        --Gac2Gas.SearchQmpkZhanDuiByClassAndZhiHui(CQuanMinPKMgr.Inst.m_SearchZhanduiClass, CQuanMinPKMgr.Inst.m_SearchZhanduiCommandIndex, self.m_CurrentPage + 1)
    end
end
function CLuaStarBiwuZhanDuiSearchWnd:OnDecreaseBtnClicked( go)
    if self.m_CurrentPage <= 1 then
        return
    end
    if self.m_Source == 1 then
        self:SetZhanDuiListPage(self.m_CurrentPage - 1)
        --CLuaStarBiwuMgr:RequestQueryStarBiwuZhanDuiList(self.m_CurrentPage - 1)
    elseif self.m_Source == 2 then
        --Gac2Gas.SearchQmpkZhanDuiByClassAndZhiHui(CQuanMinPKMgr.Inst.m_SearchZhanduiClass, CQuanMinPKMgr.Inst.m_SearchZhanduiCommandIndex, self.m_CurrentPage - 1)
    end
end
function CLuaStarBiwuZhanDuiSearchWnd:OnAdvSearchBtnClicked( go)
    CUIManager.ShowUI(CLuaUIResources.QMPKSearchRecruitWnd)
end
function CLuaStarBiwuZhanDuiSearchWnd:OnAllZhanduiBtnClicked( go)
    self.m_CurrentPage = 1
    CLuaStarBiwuMgr:RequestQueryStarBiwuZhanDuiList(self.m_CurrentPage)
end

function CLuaStarBiwuZhanDuiSearchWnd:OnListHeaderClick(index)
    if not self.m_ZhanDuiListHead or not self.m_ZhanDuiListHead[index] then return end
    self.m_ZhanDuiListHead[1].text = LocalString.GetString("职业组成")
    self.m_ZhanDuiListHead[2].text = LocalString.GetString("总战斗力")
    self.m_ZhanDuiListHead[3].text = LocalString.GetString("职业组成")
    self.m_ZhanDuiListHead[4].text = LocalString.GetString("总战斗力")
    if math.abs(self.m_SortType) == index then
        self.m_SortType = -1 * self.m_SortType
    else
        self.m_SortType = index
    end
    if self.m_SortType > 0 then
        self.m_ZhanDuiListHead[index].text = self.m_ZhanDuiListHead[index].text .. LocalString.GetString("▼")
    elseif self.m_SortType < 0 then
        self.m_ZhanDuiListHead[index].text = self.m_ZhanDuiListHead[index].text .. LocalString.GetString("▲")
    end
    if index == 2 or index == 4 and self.m_SortType ~= 0 then
        table.sort(CLuaStarBiwuMgr.m_ZhanduiTable,function(a,b)
            if self.m_SortType > 0 then
                return a.m_Capacity > b.m_Capacity
            else
                return a.m_Capacity < b.m_Capacity
            end
        end)
    end
    if index == 1 or index == 3 and self.m_SortType ~= 0 then
        table.sort(CLuaStarBiwuMgr.m_ZhanduiTable,function(a,b)
            if self.m_SortType > 0 then
                return a.m_Member.Length > b.m_Member.Length
            else
                return a.m_Member.Length < b.m_Member.Length
            end
        end)
    end

    if self.m_HasZhanDui then
        self.m_HasZhanDuiTable:ReloadData(true, false)
    else
        self.m_NoZhanDuiTable:ReloadData(true, false)
    end
end

function CLuaStarBiwuZhanDuiSearchWnd:InitListHead()
    self.m_SortType = 0
    self.m_ZhanDuiListHead[1].text = LocalString.GetString("职业组成")
    self.m_ZhanDuiListHead[2].text = LocalString.GetString("总战斗力")
    self.m_ZhanDuiListHead[3].text = LocalString.GetString("职业组成")
    self.m_ZhanDuiListHead[4].text = LocalString.GetString("总战斗力")
    for k,v in pairs(self.m_ZhanDuiListHead) do
        UIEventListener.Get(v.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
            self:OnListHeaderClick(k)
        end)
    end
end

function CLuaStarBiwuZhanDuiSearchWnd:ReplyStarBiwuZhanDuiList(selfZhanDuiId, page, totalPage, source)
    self.m_Source = source
    self.m_HasZhanDui = selfZhanDuiId > 0
    self.m_NoZhanDuiRoot:SetActive(not self.m_HasZhanDui)
    self.m_HasZhanDuiRoot:SetActive(self.m_HasZhanDui)
    self:InitListHead()

    if self.m_CurrentPage <= 1 then
        self.m_DecreaseBtn.Enabled = false
    else
        self.m_DecreaseBtn.Enabled = true
    end
    if self.m_CurrentPage >= totalPage then
        self.m_IncreseaBtn.Enabled = false
    else
        self.m_IncreseaBtn.Enabled = true
    end
    totalPage = math.ceil( #CLuaStarBiwuMgr.m_ZhanduiTable / self.m_OnePageTotalShowTeam )
    if totalPage == 0 then
        totalPage = 1
    end
    self.m_TotalPage = totalPage

    self.m_NoZhanDuiTable.m_DataSource.count = self.m_OnePageTotalShowTeam

    if self.m_HasZhanDui then
        self.m_ZhanDuiBtnText.text = LocalString.GetString("我的战队")
        self.m_HasZhanDuiTable:ReloadData(true, false)
        self.m_HasZhanDuiTable.OnSelectAtRow = DelegateFactory.Action_int(function(index) self:OnSelectAtRow(index) end)
    else
        self.m_ZhanDuiBtnText.text = LocalString.GetString("创建战队")
        self.m_NoZhanDuiTable:ReloadData(true, false)
        self.m_NoZhanDuiTable.OnSelectAtRow = DelegateFactory.Action_int(function(index) self:OnSelectAtRow(index) end)
    end

    --self.m_CurrentPage = page

    self.m_PageLabel.text = SafeStringFormat3( "%d/%d",page,totalPage )

    self.m_AdvSearchRoot:SetActive(source == 2)
    self.m_NormalSearchRoot:SetActive(source == 1)
end
function CLuaStarBiwuZhanDuiSearchWnd:OnSelectAtRow( index)
    local zhanduiId = 0
    index = self.m_OnePageTotalShowTeam * (self.m_CurrentPage - 1) + index + 1
    if CLuaStarBiwuMgr.m_ZhanduiTable and (index <= #CLuaStarBiwuMgr.m_ZhanduiTable) then
        zhanduiId = CLuaStarBiwuMgr.m_ZhanduiTable[index].m_Id
    end
    CLuaStarBiwuMgr:ShowZhanDuiMemberWnd(index, zhanduiId, 0)
end
function CLuaStarBiwuZhanDuiSearchWnd:Init( )
    local function initItem(item,row)
        local index = self.m_OnePageTotalShowTeam * (self.m_CurrentPage - 1) + row + 1
        if index <= #CLuaStarBiwuMgr.m_ZhanduiTable then
            self:InitQMPKZhanDuiItem(item, index, row%2==0)
        else
            item.gameObject:SetActive(false)
        end
    end
    local count = self.m_OnePageTotalShowTeam

    local dataSource=DefaultTableViewDataSource.CreateByCount(count,initItem)
    self.m_NoZhanDuiTable.m_DataSource = dataSource
    self.m_HasZhanDuiTable.m_DataSource = dataSource

    self.m_SearchBtn.OnSearch =DelegateFactory.Action_string(function(str) self:OnSearchBtnClicked(str) end)

    CLuaStarBiwuMgr:RequestQueryStarBiwuZhanDuiList(self.m_CurrentPage)
    self.m_NoZhanDuiRoot:SetActive(false)
    self.m_HasZhanDuiRoot:SetActive(false)
end
function CLuaStarBiwuZhanDuiSearchWnd:OnSearchBtnClicked( value)
    local id=tonumber(value)
    if not id then
        g_MessageMgr:ShowMessage("QMPK_Search_Zhandui_Only_By_Id")
        return
    end
    if CLuaStarBiwuMgr.isQuDaoZhanDuiSearchWnd then
        Gac2Gas.QueryStarBiwuZhanDuiInfoByMemberId_QD(id)
        return
    end
    Gac2Gas.QueryStarBiwuZhanDuiInfoByMemberId(id)
end
function CLuaStarBiwuZhanDuiSearchWnd:OnZhanDuiBtnClicked( go)
    if self.m_HasZhanDui then
        CLuaStarBiwuMgr.isQuDaoSelfZhanDuiWnd = CLuaStarBiwuMgr.isQuDaoZhanDuiSearchWnd
        CUIManager.ShowUI(CLuaUIResources.StarBiwuSelfZhanDuiWnd)
    else
        CLuaStarBiwuMgr.isQuDaoCreateZhanDuiWnd = CLuaStarBiwuMgr.isQuDaoZhanDuiSearchWnd
        CUIManager.ShowUI(CLuaUIResources.StarBiwuCreateZhanDuiWnd)
    end
end

function CLuaStarBiwuZhanDuiSearchWnd:InitQMPKZhanDuiItem(tableItem, index, isOdd)
    tableItem.gameObject:SetActive(true)
    local zhanDui = CLuaStarBiwuMgr.m_ZhanduiTable[index]
    local bRetainFlag = CLuaStarBiwuMgr.m_ZhanduiFlagTable[index] > 0
    local transform=tableItem.transform
    local m_NameLabel = transform:Find("Name"):GetComponent(typeof(UILabel))
    local m_SloganLabel = transform:Find("Slogan"):GetComponent(typeof(UILabel))
    local m_MemberGrid = transform:Find("MemberGrid"):GetComponent(typeof(UIGrid))
    local m_Capacity = transform:Find("Capacity"):GetComponent(typeof(UILabel))
    local m_MemberObj = transform:Find("Member").gameObject
    m_MemberObj:SetActive(false)

    transform:Find("RetainFlag").gameObject:SetActive(bRetainFlag)

    -- local tableItem=transform:GetComponent(typeof(QnTableItem))
    if isOdd then
        tableItem:SetBackgroundTexture(g_sprites.EvenBgSprite)
    else
        tableItem:SetBackgroundTexture(g_sprites.OddBgSpirite)
    end
    m_NameLabel.text = zhanDui.m_Name
    m_SloganLabel.text = zhanDui.m_Slogan
    m_Capacity.text = tostring(zhanDui.m_Capacity)
    Extensions.RemoveAllChildren(m_MemberGrid.transform)
    do
        local i = 0 local len = zhanDui.m_Member and zhanDui.m_Member.Length or 0
        while i < len do
            local go = NGUITools.AddChild(m_MemberGrid.gameObject, m_MemberObj)
            go:SetActive(true)
            if zhanDui.m_Member[i] > 0 then
                go:GetComponent(typeof(UISprite)).spriteName= Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), zhanDui.m_Member[i]))
                go.transform:Find("Sprite").gameObject:SetActive(false)
            end
            i = i + 1
        end
    end
    m_MemberGrid:Reposition()

    self:UpdateApplyStatus(transform, zhanDui)
end

RegistClassMember(CLuaStarBiwuZhanDuiSearchWnd, "m_ZhanduiObj2IdTable")
function CLuaStarBiwuZhanDuiSearchWnd:UpdateApplyStatus(trans, zhanDui)
  -- No ZhanDui
  if self.m_HasZhanDui then return end
  local applyObj = trans:Find("Apply").gameObject
  local applyLabel = applyObj.transform:Find("ApplyName"):GetComponent(typeof(UILabel))
  local fullObj = trans:Find("FullTip").gameObject
  local hasAppliedObj = trans:Find("Apply/Checked").gameObject
  if not self.m_ZhanduiObj2IdTable then self.m_ZhanduiObj2IdTable = {} end
  self.m_ZhanduiObj2IdTable[trans] = zhanDui
  local bFull = zhanDui.m_Member.Length >= StarBiWuShow_Setting.GetData().Max_ZhanDui_MemberNum
  fullObj:SetActive(bFull)
  applyObj:SetActive(not bFull)
  hasAppliedObj:SetActive(false)
  local bHasApplied = zhanDui.m_HasApplied > 0
  if not bFull then
    hasAppliedObj:SetActive(bHasApplied)
    applyLabel.text = bHasApplied and LocalString.GetString("取消申请") or LocalString.GetString("申请")
  end
  UIEventListener.Get(applyObj).onClick = LuaUtils.VoidDelegate(function()
    if not bHasApplied then
      Gac2Gas.RequestJoinStarBiwuZhanDui(zhanDui.m_Id, "")
    else
      Gac2Gas.RequestCancelJoinStarBiwuZhanDui(zhanDui.m_Id)
      applyLabel.text = LocalString.GetString("申请")
      hasAppliedObj:SetActive(false)
      zhanDui.m_HasApplied = 0
      bHasApplied = false
    end
  end)
end

function CLuaStarBiwuZhanDuiSearchWnd:RequestJoinStarBiwuZhanDuiSuccess(zhanduiId)
  for k, v in pairs(self.m_ZhanduiObj2IdTable) do
    if v.m_Id == zhanduiId then
      v.m_HasApplied = 1
      self:UpdateApplyStatus(k, v)
    end
  end
end

function CLuaStarBiwuZhanDuiSearchWnd:StarBiwuCancelJoinZhanDuiByPlayer(zhanduiId)
  for k, v in pairs(self.m_ZhanduiObj2IdTable) do
    if v.m_Id == zhanduiId then
      v.m_HasApplied = 0
      self:UpdateApplyStatus(k, v)
    end
  end
end

function CLuaStarBiwuZhanDuiSearchWnd:SetZhanDuiListPage(page)
    self.m_CurrentPage = math.max(1,math.min(page,self.m_TotalPage))
    if self.m_HasZhanDui then
        self.m_HasZhanDuiTable:ReloadData(true, false)
    else
        self.m_NoZhanDuiTable:ReloadData(true, false)
    end
    self.m_PageLabel.text = SafeStringFormat3( "%d/%d",self.m_CurrentPage,self.m_TotalPage )
    if self.m_CurrentPage <= 1 then
        self.m_DecreaseBtn.Enabled = false
    else
        self.m_DecreaseBtn.Enabled = true
    end
    if self.m_CurrentPage >= self.m_TotalPage then
        self.m_IncreseaBtn.Enabled = false
    else
        self.m_IncreseaBtn.Enabled = true
    end
    self.m_AdvSearchRoot:SetActive(self.m_Source == 2)
    self.m_NormalSearchRoot:SetActive(self.m_Source == 1)
end
