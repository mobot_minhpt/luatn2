-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CDyeMgr = import "L10.UI.CDyeMgr"
local CDyeWnd = import "L10.UI.CDyeWnd"
local Color = import "UnityEngine.Color"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local Initialization_Init = import "L10.Game.Initialization_Init"
local NGUIText = import "NGUIText"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local EnumYingLingState = import "L10.Game.EnumYingLingState"

CDyeWnd.m_Init_CS2LuaHook = function (this)
    this:InitHair()
    this.modelPreviewer:PreviewMainPlayer(System.UInt32.MaxValue, 1, 0)
    UIEventListener.Get(this.closeBtn).onClick = (DelegateFactory.VoidDelegate(function (p)
        this:ShowFailMessage()
        CUIManager.CloseUI(CUIResources.DyeWnd)
    end))
    local advanceHair = CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.AdvancedHairColor
    local advanceHairColorId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.AdvancedHairColorId
    if (advanceHair and advanceHair ~= "") or (advanceHairColorId ~= 0) then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("CheckRanFaJi_AdvancedColor_Fade"), function ()
            Gac2Gas.TakeOffAdvancedHairColor(0)
        end, nil, nil, nil, false)
    end
end
CDyeWnd.m_OnDyeBtnClick_CS2LuaHook = function (this, go)
    if this.selectedHairId ~= this.curHairId then
        if CDyeMgr.OnDye ~= nil then
            GenericDelegateInvoke(CDyeMgr.OnDye, Table2ArrayWithCount({this.selectedHairId}, 1, MakeArrayClass(Object)))
        end
        this.curHairId = this.selectedHairId
        this:ShowSuccMessage()
    else
        this:ShowFailMessage()
    end
    CUIManager.CloseUI(CUIResources.DyeWnd)
end
CDyeWnd.m_InitHair_CS2LuaHook = function (this)
    local mainPlayer = CClientMainPlayer.Inst
    local Class = mainPlayer.Class
    local Gender = mainPlayer.Gender
    if mainPlayer and mainPlayer.IsYingLing then
        if mainPlayer.CurYingLingState ~= EnumYingLingState.eMonster then
            Gender = mainPlayer.AppearanceGender
        end
    end
    local character = Initialization_Init.GetData(EnumToInt(Class) * 100 + EnumToInt(Gender))

    do
        local i = 0
        while i < this.hairCells.Length do
            if i < character.HairColor.Length then
                this.hairCells[i].HairColor = NGUIText.ParseColor24(character.HairColor[i], 0)
            else
                this.hairCells[i].HairColor = Color.black
            end
            this.hairCells[i].Selected = false
            this.hairCells[i].isCurrent = false
            i = i + 1
        end
    end
    this.curHairId = CClientMainPlayer.Inst.AppearanceProp.HeadId this.selectedHairId = this.curHairId
    this.hairCells[this.selectedHairId].Selected = true
    this.hairCells[this.curHairId].isCurrent = true
end
CDyeWnd.m_OnHairClick_CS2LuaHook = function (this, go)
    do
        local i = 0
        while i < this.hairCells.Length do
            if go == this.hairCells[i].gameObject then
                this.hairCells[i].Selected = true
                this.selectedHairId = i
                this.modelPreviewer:PreviewMainPlayer(this.selectedHairId, 1, 0)
            else
                this.hairCells[i].Selected = false
            end
            i = i + 1
        end
    end
end

CDyeMgr.m_hookShow  = function(OnDye)
    CDyeMgr.OnDye = OnDye
	local mainPlayer = CClientMainPlayer.Inst
	if mainPlayer == nil then
		return
	end
    CUIManager.ShowUI(CUIResources.DyeWnd)
end
