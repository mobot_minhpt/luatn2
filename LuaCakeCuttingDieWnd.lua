local UILabel = import "UILabel"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaCakeCuttingDieWnd = class()
RegistChildComponent(LuaCakeCuttingDieWnd,"num", UILabel)
RegistClassMember(LuaCakeCuttingDieWnd, "showTime")

function LuaCakeCuttingDieWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.CakeCuttingDieWnd)
end

function LuaCakeCuttingDieWnd:Update()
	if self.showTime then
		local restTime = self.showTime - CServerTimeMgr.Inst.timeStamp
		if restTime <= 0 then
			self.num.text = '0'
			self:Close()
		else
			self.num.text = math.ceil(restTime)
		end
	end
end

function LuaCakeCuttingDieWnd:OnEnable()
	--g_ScriptEvent:AddListener("LotteryAddSucc", self, "AddLotterySucc")
end

function LuaCakeCuttingDieWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("LotteryAddSucc", self, "AddLotterySucc")
end

function LuaCakeCuttingDieWnd:Init()
	self.num.text = '0'
	self.showTime = CServerTimeMgr.Inst.timeStamp + 3
end

return LuaCakeCuttingDieWnd
