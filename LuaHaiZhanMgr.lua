local LineRenderer = import "UnityEngine.LineRenderer"
local Material=import "UnityEngine.Material"
local ShaderEx=import "ShaderEx"
local CTeamMgr = import "L10.Game.CTeamMgr"
local SoundManager = import "SoundManager"
local CSceneAmbientSound=import "CSceneAmbientSound"
local PlayerSettings=import "L10.Game.PlayerSettings"
local CScene=import "L10.Game.CScene"
local CBox2dObject = import "L10.Engine.CBox2dObject"
local CBoatAnimator=import "L10.Engine.CBoatAnimator"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"
local WeatherManager=import "CTT3.Weather.WeatherManager"
local PeriodType=import "CTT3.Weather.PeriodType"
local LayerDefine=import "L10.Engine.LayerDefine"
local CapsuleCollider = import "UnityEngine.CapsuleCollider"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local EKickOutType = import "L10.Engine.EKickOutType"
local CDynamicWorldPositionFX=import "L10.Game.CDynamicWorldPositionFX"
local CPhysicsInputMgr=import "L10.Engine.CPhysicsInputMgr"
local BoxCollider = import "UnityEngine.BoxCollider"
local Component=import "UnityEngine.Component"
local CClientMonster=import "L10.Game.CClientMonster"
local PrimitiveType=import "UnityEngine.PrimitiveType"
local CPos = import "L10.Engine.CPos"
local CPhysicsObject=import "L10.Engine.CPhysicsObject"
local Rect=import "UnityEngine.Rect"
local Buoyancy=import "Buoyancy"
local LayerMask=import "UnityEngine.LayerMask"
local Rigidbody=import "UnityEngine.Rigidbody"
local CPhysicsWorld=import "L10.Engine.CPhysicsWorld"
local CRenderObject = import "L10.Engine.CRenderObject"
local CClientObjectRoot=import "L10.Game.CClientObjectRoot"
local Quaternion=import "UnityEngine.Quaternion"
local CPhysicsMgr=import "L10.Engine.CPhysicsMgr"
local CPhysicsTrigger=import "L10.Engine.CPhysicsTrigger"
local CMainCamera=import "L10.Engine.CMainCamera"
local ForceMode=import "UnityEngine.ForceMode"
local CPos = import "L10.Engine.CPos"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CPhysicsSyncMgr=import "L10.Engine.CPhysicsSyncMgr"
local CPhysicsObjectMgr=import "L10.Engine.CPhysicsObjectMgr"
local CPhysicsTimeMgr=import "L10.Engine.CPhysicsTimeMgr"
local Vector2=import "UnityEngine.Vector2"
local FramePacket=import "L10.Engine.FramePacket"
local FrameInputPacket=import "L10.Engine.FrameInputPacket"
local FrameStatusPacket=import "L10.Engine.FrameStatusPacket"
local File=import "System.IO.File"
local FrameLuaPacket=import "L10.Engine.FrameLuaPacket"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"



require "ui/haizhan/clientship"

LuaHaiZhanMgr = {}
LuaHaiZhanMgr.s_Debug = true
LuaHaiZhanMgr.s_EnableDebugShape = false

LuaHaiZhanMgr.s_InCabin = false
LuaHaiZhanMgr.s_MyRole = 0--1:舵手、2:炮手、3:水手
LuaHaiZhanMgr.s_MyPos = 0

LuaHaiZhanMgr.s_Hp = 0
LuaHaiZhanMgr.s_CurrHpFull = 0
LuaHaiZhanMgr.s_HpFull = 0
--开炮cd
LuaHaiZhanMgr.s_CDInfo = nil
LuaHaiZhanMgr.s_CmdCDInfo = nil

--资源
LuaHaiZhanMgr.s_CntInfo = nil
LuaHaiZhanMgr.s_BoardCnt = 0
LuaHaiZhanMgr.s_TmpSkills = nil
LuaHaiZhanMgr.s_FireNum = nil


LuaHaiZhanMgr.s_PickEngineId = 0

LuaHaiZhanMgr.s_SeatInfo = nil
LuaHaiZhanMgr.s_StageFxs = nil
LuaHaiZhanMgr.s_Stage = nil

LuaHaiZhanMgr.s_Status = {}
LuaHaiZhanMgr.s_ShipId2EngineId = {}
LuaHaiZhanMgr.s_EngineId2ShipId = {}
LuaHaiZhanMgr.s_ShipId2Gold = {}
LuaHaiZhanMgr.s_ShipId2Exp = {}
LuaHaiZhanMgr.s_ShipIdGoldRank = {}
LuaHaiZhanMgr.s_PvpPlayerNum = 0

LuaHaiZhanMgr.s_MyShipId = 0

FramePacket.m_hookSend = function(this)
    if TypeIs(this, typeof(FrameInputPacket)) then
        local t = {
            tonumber(this.inputType),
            tonumber(this.frameId),
            tonumber(this.level),
        }
        Gac2Gas.SyncPhysicsMove(g_MessagePack.pack(t))
    elseif TypeIs(this, typeof(FrameStatusPacket)) then
        local t = {
            tonumber(EnumPhysicsCmdType.eSpeed),
            tonumber(this.frameId),
            tonumber(this.speedCmd),
        }
        Gac2Gas.SyncPhysicsMove(g_MessagePack.pack_use_float(t))

        local designData = Physics_SpeedCmd.GetData(this.speedCmd)
        if not LuaHaiZhanMgr.s_CmdCDInfo then
            LuaHaiZhanMgr.s_CmdCDInfo = {}
        end
        LuaHaiZhanMgr.s_CmdCDInfo[this.speedCmd] = this.frameId+designData.cdFrame
    end
end

function LuaHaiZhanMgr.Pos2Role(pos)
    if pos==1 then
        return 1
    elseif pos==2 or pos==3 or pos==4 then
        return 2
    elseif pos==5 then
        return 3
    end
end

function LuaHaiZhanMgr:IsInTrainingGameplay() -- 是否在新手副本
    local setting = NavalWar_Setting.GetData()
    return CScene.MainScene and (CScene.MainScene.GamePlayDesignId == setting.TrainingSeaPlayId or CScene.MainScene.GamePlayDesignId == setting.TrainingCabinPlayId)
end

function LuaHaiZhanMgr:IsInPvpPlay() -- 是否在海战PVP(藏宝海域)副本
    local setting = NavalWar_Setting.GetData()
    return CScene.MainScene and (CScene.MainScene.GamePlayDesignId == setting.PvpSeaPlayId or CScene.MainScene.GamePlayDesignId == setting.PvpCabinPlayId)
end

function LuaHaiZhanMgr:EnableChangePos()
    return not self:IsInTrainingGameplay() or (CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.BuffProp.Buffs, NavalWar_Setting.GetData().TrainingChangePositionBuff * 100 + 1))
end

function LuaHaiZhanMgr:EnableSwitchCabinAndSea()
    return not self:IsInTrainingGameplay() or (CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.BuffProp.Buffs, NavalWar_Setting.GetData().TrainingCabinBuff * 100 + 1) and not LuaHaiZhanMgr.s_InCabin)
end

function LuaHaiZhanMgr.IsPvpPlay()
    if CScene.MainScene and CScene.MainScene.GamePlayDesignId == NavalWar_Setting.GetData().PvpSeaPlayId then
        return true
    end
    return false
end

function LuaHaiZhanMgr:OnMainPlayerCreated(gamePlayId)
    g_AutoChangeToTaskTab = true

    self.s_InCabin = false
    local setting = NavalWar_Setting.GetData()
    if gamePlayId==setting.GamePlayId 
    or gamePlayId==setting.TrainingSeaPlayId 
    or gamePlayId==setting.PvpSeaPlayId then
        CUIManager.ShowUI(CLuaUIResources.HaiZhanDriveWnd)
        CUIManager.ShowUI(CLuaUIResources.HaiZhanStateWnd)

        if File.Exists("physics.log") then
            File.Delete("physics.log")
        end
    elseif gamePlayId==setting.CabinGamePlayId 
    or gamePlayId==setting.TrainingCabinPlayId 
    or gamePlayId==setting.PvpCabinPlayId then
        self.s_InCabin = true
        CUIManager.ShowUI(CLuaUIResources.HaiZhanStateWnd)
    end

    if not self:IsInPvpPlay() then
        LuaCommonGamePlayTaskViewMgr:AppendGameplayInfoByTbl(
        {
            gameplayId = gamePlayId,
            bShowCountDown = true,
            bUpdateBySyncStateInfoRPC = self.s_InCabin and gamePlayId - 1 or true, -- 船舱使用海面玩法副本id同步
            bShowTitle = true,
            getTitleFunc = function() return Gameplay_Gameplay.GetData(gamePlayId).Name end,
            bShowContent = true,
            getContentFunc = function() return nil end,
            bShowLeaveBtn = true,
            clickLeaveBtnFunc = function()
                if gamePlayId == setting.TrainingPrePlayId or gamePlayId == setting.TrainingSeaPlayId or gamePlayId == setting.TrainingCabinPlayId then
                    Gac2Gas.RequestLeavePlay()
                else
                    local msg = CTeamMgr.Inst:MainPlayerIsTeamLeader() and g_MessageMgr:FormatMessage("NAVALWAR_PVE_TEAM_LEADER_LEAVE_SCENE") or g_MessageMgr:FormatMessage("LEAVE_COPY_CONFIRM")
                    g_MessageMgr:ShowOkCancelMessage(msg, 
                        function()
                            Gac2Gas.RequestLeavePlay()
                        end, nil, nil, nil, false)
                end
            end,
            bShowRuleBtn = false,
        })
    end

    --设置天气系统的时间
    if LuaHaiZhanMgr:IsInPvpPlay() then
        local second = NavalWar_Setting.GetData().PvpEndTime-CScene.MainScene.LeftTime
        second = math.max(0,second)
        WeatherManager.SetPeriod(PeriodType.Daytime,0,second)
    end

    LuaHaiZhanMgr.CreateBorderFx(gamePlayId)
end

function LuaHaiZhanMgr:OnMainPlayerDestroyed()
    LuaHaiZhanMgr.s_CntInfo = nil
    LuaHaiZhanMgr.s_BoardCnt = 0
    LuaHaiZhanMgr.s_SeatInfo=nil
    LuaHaiZhanMgr.s_Stage = nil
    LuaHaiZhanMgr.s_MyRole = 0
    LuaHaiZhanMgr.s_MyPos = 0
    LuaHaiZhanMgr.s_HuntInfo = nil

    self.s_Hp = 0
    self.s_CurrHpFull = 0
    self.s_HpFull = 0
    self.s_FireNum = nil
    self.s_Status = {}
    self.s_ShipId2EngineId = {}
    self.s_EngineId2ShipId = {}
    self.s_ShipId2Gold = {}
    self.s_ShipId2Exp = {}
    self.s_ShipIdGoldRank = {}
    self.s_PvpPlayerNum = 0
    self.s_TmpSkills = nil

    if LuaHaiZhanMgr.s_StageFxs then
        for key, value in pairs(LuaHaiZhanMgr.s_StageFxs) do
            if value then
                value:Destroy()
            end
        end
        LuaHaiZhanMgr.s_StageFxs = nil
    end

    WeatherManager.SetPeriod(PeriodType.Daytime,0,0)

    --清空快照pool
    LuaClientPhysicsBullet.s_SnapshotPool = {}
    LuaClientPhysicsShip.s_SnapshotPool = {}
    LuaClientPhysicsBullet.s_BulletCacheTeamplate = {}
end
function LuaHaiZhanMgr.CreateBorderFx(gamePlayId)
    local setting = NavalWar_GameplaySetting.GetData(gamePlayId)
    if not setting then return end
    local baseHeight = CPhysicsWorld.Inst:GetBaseHeight()
    if setting.Barrier then
        for i = 1, setting.Barrier.Length,4 do
            local left = setting.Barrier[i-1]
            local bottom = setting.Barrier[i]
            local width = setting.Barrier[i+1]
            local height = setting.Barrier[i+2]
            CEffectMgr.Inst:AddWorldPositionFX(88804161, Vector3(left-1, baseHeight, bottom-1), 90+0, 0, 1, -1, EnumWarnFXType.None, nil, width+2, 1, nil);
            CEffectMgr.Inst:AddWorldPositionFX(88804161, Vector3(left + width+1, baseHeight, bottom-1), -90-90, 0, 1, -1, EnumWarnFXType.None, nil, height+2, 1, nil);
            CEffectMgr.Inst:AddWorldPositionFX(88804161, Vector3(left + width+1, baseHeight, bottom + height+1), -90, 0, 1, -1, EnumWarnFXType.None, nil, width+2, 1, nil);
            CEffectMgr.Inst:AddWorldPositionFX(88804161, Vector3(left-1, baseHeight, bottom+ height+1), -90+90, 0, 1, -1, EnumWarnFXType.None, nil, height+2, 1, nil);
        end
    end
    local baseHeight = CPhysicsWorld.Inst:GetBaseHeight()
    if setting.PolygonBarrier then
        local tbl = {}
        for i = 1, setting.PolygonBarrier.Length,2 do
            table.insert(tbl,Vector3(setting.PolygonBarrier[i-1],baseHeight,setting.PolygonBarrier[i]))
        end
        LuaHaiZhanMgr.CreateLine(tbl)
    end
end
function LuaHaiZhanMgr.CreateLine(points)
    local baseHeight = CPhysicsWorld.Inst:GetBaseHeight()
    CEffectMgr.Inst:AddWorldPositionFX(88804192, Vector3(0, baseHeight, 0), 90, 0, 1, -1, EnumWarnFXType.None, nil, 1, 1, DelegateFactory.Action_GameObject(function(go)
        if go and not CommonDefs.IsNull(go) then
            local cmp = go.transform:Find("line"):GetComponent(typeof(LineRenderer))
            cmp.loop=true
            cmp.positionCount = #points
            local array = Table2Array(points, MakeArrayClass(Vector3))
            cmp:SetPositions(array)
        end
    end))
end

function LuaHaiZhanMgr.SendBullet(bulletTemplateId)
    if LuaHaiZhanMgr.s_MyRole == 2 then
        local cdIdx = LuaHaiZhanMgr.s_MyPos
        local dir = CMainCamera.Main.transform.right
        local v = Vector2(-dir.z,dir.x).normalized
        local speed = Physics_Trigger.GetData(bulletTemplateId).speed
        Gac2Gas.RequestFireNavalWarCannon(bulletTemplateId, v.x*speed, v.y*speed, cdIdx)
    end
end

function LuaHaiZhanMgr.CreateShip(frameId,physicsObjectId, x, y, dir,templateId,controllerType)
    local engineId = math.abs(physicsObjectId)
    local clientObject = CClientObjectMgr.Inst:GetObject(engineId)
    if not clientObject then return end
    if not TypeIs(clientObject, typeof(CClientMonster)) then return end

    --已经创建了，不管
    if clientObject.m_PhysicsObject then 
        return 
    end


    local parent = CClientObjectRoot.Inst:GetParent("Monster")
    local layer = LayerMask.NameToLayer("Monster")

    local go = GameObject(tostring(physicsObjectId))
    go.layer = layer
    go.transform.parent = CPhysicsMgr.Inst.PhysicsObjectRoot

    go.transform.localPosition = Vector3(x, 0, y)
    go.transform.localScale = Vector3.one
    -- //这里是ro的角度
    go.transform.localEulerAngles = Vector3(0, 90 - dir, 0)

    clientObject.m_PhysicsObject = go:AddComponent(typeof(CPhysicsObject))
    local physicsObject = clientObject.m_PhysicsObject
    physicsObject.EngineId = clientObject.EngineId
    physicsObject:SetFrameId(frameId)

    physicsObject:Init(physicsObjectId)

    local shapeId = Physics_Object.GetData(templateId).shape
    SetPhysicsShape(physicsObject.box2dRb,shapeId)

    local luaObject = LuaClientPhysicsShip:new(physicsObjectId,physicsObject)
    physicsObject:InitLuaSelf(luaObject)

    luaObject:Init({
        objectTemplateId = templateId,
        controllerType = controllerType
    })

    --处理显示
    local ro = CRenderObject.CreateRenderObject(parent, tostring(physicsObjectId), false)
    ro.Layer = LayerDefine.Monster

    ro.Position = Vector3(x, CPhysicsWorld.Inst:GetBaseHeight(), y)
    ro.Direction = 90 - dir
    
    local designData = NavalWar_Ship.GetData(templateId)
    ro:LoadMain(designData.PrefabPath)
    ro.Selectable = true
    ro.Scale = designData.Scale

    ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(ro)
        local rb = ro:GetComponentInChildren(typeof(Rigidbody))
        rb.isKinematic = false
        rb.mass = 2
        rb.drag = 1
        rb.angularDrag = 0.75

        local cmp = ro.m_MainObject:AddComponent(typeof(Buoyancy))
        cmp.ypos = designData.yOffset

        local collider = ro.m_MainObject:GetComponent(typeof(CapsuleCollider))
        if collider then
            local shapeId = Physics_Object.GetData(templateId).shape
            local designData = Physics_ObjectShape.GetData(shapeId)
            if designData then
                collider.center = Vector3(0,0.5,0)
                collider.radius = designData.radius
                collider.height = designData.height+designData.radius*2
                collider.direction = 2--z-Axis
            end
        end

        local rb2d = clientObject.m_PhysicsObject.box2dRb
        local box2dObject = rb2d.gameObject:GetComponent(typeof(CBox2dObject))
        local animator = ro.m_MainObject:AddComponent(typeof(CBoatAnimator))
        animator:Init(box2dObject)

    end))
    physicsObject.RO = ro
    luaObject:SetMoveEffectActive(true)

	g_ScriptEvent:BroadcastInLua("CreateShip",physicsObjectId)
end

function LuaHaiZhanMgr.SyncNavalWarCabinShipHp(hp, currHpFull, HpFull)
    LuaHaiZhanMgr.s_Hp = hp
    LuaHaiZhanMgr.s_CurrHpFull = currHpFull
    LuaHaiZhanMgr.s_HpFull = HpFull
	g_ScriptEvent:BroadcastInLua("SyncNavalWarCabinShipHp",hp, currHpFull, HpFull)


    local myPOId = CClientMainPlayer.Inst.AppearanceProp.DriverId
    local po = g_PhysicsObjectHandlers[myPOId]
    if po then
        po:RefreshHurtFx(hp,HpFull)
    end
end
function LuaHaiZhanMgr.SyncNavalSpecialMonsterHp(engineId, hp, currentHpFull, hpFull)
	g_ScriptEvent:BroadcastInLua("SyncNavalSpecialMonsterHp",engineId, hp, currentHpFull, hpFull)

    local po = g_PhysicsObjectHandlers[-engineId]
    if po and po.RefreshHurtFx then--有可能是LuaClientPhysicsObject
        po:RefreshHurtFx(hp,hpFull)
    end
end

function LuaHaiZhanMgr.SyncNavalPhysicsFrameId(frameId)
    Gac2Gas.RequestPhysicsServerTime(CPhysicsTimeMgr.Inst:GetLocalTime())
end

function LuaHaiZhanMgr.SyncNavalMove(poId, ud)
    local t = g_MessagePack.unpack(ud)
    local pkt = nil
    local frameType = t[1]
    if frameType <= 2 then
        pkt = FrameInputPacket()
        pkt.physicsObjectId = poId
        pkt.inputType = t[1]--0，1，2
        pkt.frameId = t[2]
        pkt.level = t[3]
        pkt.serverFrameId = t[4] or 0
        CPhysicsSyncMgr.Inst:AddServerFrame(pkt)

    elseif frameType==3 then
        pkt = FrameStatusPacket()
        pkt.physicsObjectId = poId
        pkt.frameId = t[2]
        pkt.speedCmd = t[3]
        pkt.serverFrameId = t[4] or 0

        --serverFrameId为空 表示服务器丢弃了这个状态包，
        --frameId==serverFrameId 表示服务器接收了这个包，
        --frameId<serverFrameId 表示服务器会延迟执行
        if pkt.serverFrameId==0 then
        -- elseif pkt.frameId == pkt.serverFrameId then
        elseif pkt.frameId<=pkt.serverFrameId then
            --服务器延迟执行
            if CClientMainPlayer.Inst then
                local myPOId = CClientMainPlayer.Inst.AppearanceProp.DriverId
                if myPOId==pkt.physicsObjectId then
                    local designData = Physics_SpeedCmd.GetData(pkt.speedCmd)
                    LuaHaiZhanMgr.s_CmdCDInfo[pkt.speedCmd] = pkt.serverFrameId+designData.cdFrame
                end
            end
        end

        CPhysicsSyncMgr.Inst:AddServerFrame(pkt)
    end
end

function LuaHaiZhanMgr.SyncPhysicsStartTime(startTime, currTime)
    CPhysicsTimeMgr.Inst.StartTimeInMilliSeconds = startTime
end
--更新rtt，找到最接近服务器的时间
function LuaHaiZhanMgr.SyncPhysicsServerTime(serverTime,clientTime)
    local curentClientTime = CPhysicsTimeMgr.Inst:GetLocalTime()
    local rtt = curentClientTime-clientTime
    if rtt<=0 then
        print("SyncPhysicsServerTime impossible")
        return 
    end
    -- print("SyncPhysicsServerTime",rtt)

    local minRtt = CPhysicsTimeMgr.Inst.minRtt
    if minRtt==0 or minRtt>rtt then
        minRtt = rtt
        CPhysicsTimeMgr.Inst.minRtt = minRtt
        CPhysicsTimeMgr.Inst.mTimeDifference = minRtt/2+serverTime-curentClientTime
    end
    CPhysicsTimeMgr.Inst.IsSynced = true
end


--physicsObjId<0的时候，是引擎id
-->0的时候是playerId,现在不可能是playerId
--或者是trigger的自增id
function LuaHaiZhanMgr.CreatePhysicsObject(frameId,physicsObjId, controllerType, extraUd)
    local t = g_MessagePack.unpack(extraUd)
    local objectTemplateId = t[1]
    local pixelx = t[2]
    local pixely = t[3]
    local dir = t[4]
    -- print("CreatePhysicsObject",unpack(t))

    if controllerType == EnumPhysicsController.ePhysicsShip then
        LuaHaiZhanMgr.CreateShip(frameId,physicsObjId, pixelx/64, pixely/64, dir,objectTemplateId,controllerType)
	elseif controllerType == EnumPhysicsController.ePhysicsAIShip then
		LuaHaiZhanMgr.CreateShip(frameId,physicsObjId, pixelx/64, pixely/64, dir,objectTemplateId,controllerType)
    else
        LuaHaiZhanMgr.CreateStillPhysicsObject(frameId,physicsObjId,objectTemplateId,pixelx/64, pixely/64,dir)
	end
end
function LuaHaiZhanMgr.CreateStillPhysicsObject(frameId,physicsObjId,templateId,x,y,dir)
    --创建一个静态的障碍
    local engineId = math.abs(physicsObjId)
    local clientObject = CClientObjectMgr.Inst:GetObject(engineId)
    if not clientObject then return end
    if clientObject.m_PhysicsObject then return end

    local layer = LayerMask.NameToLayer("Monster")
    local parent = CClientObjectRoot.Inst:GetParent("Monster")

    local go = GameObject(tostring(physicsObjId))
    go.layer = layer
    go.transform.parent = CPhysicsMgr.Inst.PhysicsObjectRoot

    go.transform.localPosition = Vector3(x, 0, y)
    go.transform.localScale = Vector3.one
    go.transform.localEulerAngles = Vector3(0, 90 - dir, 0)

    clientObject.m_PhysicsObject = go:AddComponent(typeof(CPhysicsObject))
    local physicsObject = clientObject.m_PhysicsObject
    physicsObject.EngineId = clientObject.EngineId
    physicsObject:SetFrameId(frameId)

    physicsObject:Init(physicsObjId)
    local shapeId = Physics_Object.GetData(templateId).shape
    SetPhysicsShape(physicsObject.box2dRb,shapeId)

    local luaObject = LuaClientPhysicsObject:new(physicsObjId,physicsObject)
    physicsObject:InitLuaSelf(luaObject)
    luaObject:Init({
        objectTemplateId = templateId,
    })
end

function LuaHaiZhanMgr.OnHitByPhysicsTrigger(frameId, physicsObjectId, triggerId, templateId)
    local bullet = CPhysicsObjectMgr.Inst:GetTrigger(triggerId)
    if bullet then
        -- local po = CPhysicsObjectMgr.Inst:GetPhysicsObject(physicsObjectId)
        bullet.isDead = true
        bullet.targetPhysicsObjectId = physicsObjectId
    end
end

function LuaHaiZhanMgr.CreateDebugShape(shapeId,x, y,dir,lifeCycle,vx, vy, w)
    if not LuaHaiZhanMgr.s_EnableDebugShape then return end

    local yScale=0.1
    local shapeData = Physics_TriggerShape.GetData(shapeId)
        local baseHeight = CPhysicsWorld.Inst:GetBaseHeight()
        if shapeData.shapeType==1 then--胶囊体 默认是2米长 1米宽
        elseif shapeData.shapeType==2 then--圆
            local go = GameObject.CreatePrimitive(PrimitiveType.Cylinder)
            go.transform.localPosition = Vector3(x,baseHeight,y)
            go.transform.localScale = Vector3(shapeData.radius*2,yScale,shapeData.radius*2)
            GameObject.Destroy(go,lifeCycle/20)
        elseif shapeData.shapeType==3 then--方块
            local go = GameObject.CreatePrimitive(PrimitiveType.Cube)
            go.transform.localPosition = Vector3(x,baseHeight,y)
            go.transform.localScale = Vector3(shapeData.radius*2,yScale,shapeData.height*2)
            --此处的角度计算和ro不同，模型默认是朝向x轴正方向，ro是朝向y轴正方向
            go.transform.localEulerAngles = Vector3(0,-dir/PHYSICS_PI*180,0)
            GameObject.Destroy(go,lifeCycle/20)
        elseif shapeData.shapeType==4 then--等腰三角形
            local go = GameObject("temp")
            go.transform.localPosition = Vector3(x,baseHeight,y)

            local child = GameObject.CreatePrimitive(PrimitiveType.Cube)
            child.transform.parent = go.transform
            child.transform.localPosition = Vector3(shapeData.radius/2,0,0)
            child.transform.localScale = Vector3(shapeData.radius,yScale,1)
            
            local child = GameObject.CreatePrimitive(PrimitiveType.Cube)
            child.transform.parent = go.transform
            child.transform.localPosition = Vector3(shapeData.radius,0,0)
            child.transform.localScale = Vector3(1,yScale,shapeData.height*2)

            local child = GameObject.CreatePrimitive(PrimitiveType.Cube)
            child.transform.parent = go.transform
            child.transform.localPosition = Vector3(shapeData.radius/2,0,0)
            child.transform.localScale = Vector3(1,yScale,shapeData.height)

            local child = GameObject.CreatePrimitive(PrimitiveType.Cube)
            child.transform.parent = go.transform
            child.transform.localPosition = Vector3(shapeData.radius/4,0,0)
            child.transform.localScale = Vector3(1,yScale,shapeData.height/2)

            local child = GameObject.CreatePrimitive(PrimitiveType.Cube)
            child.transform.parent = go.transform
            child.transform.localPosition = Vector3(shapeData.radius/4*3,0,0)
            child.transform.localScale = Vector3(1,yScale,shapeData.height*1.5)

            go.transform.localEulerAngles = Vector3(0,-dir/PHYSICS_PI*180,0)
            GameObject.Destroy(go,lifeCycle/20)
        elseif shapeData.shapeType==5 then
            local go = GameObject("temp")
            go.transform.localPosition = Vector3(x,baseHeight,y)

            local child = GameObject.CreatePrimitive(PrimitiveType.Cube)
            child.transform.parent = go.transform
            child.transform.localPosition = Vector3(shapeData.radius,0,0)
            child.transform.localScale = Vector3(shapeData.radius*2,yScale,shapeData.height*2)

            go.transform.localEulerAngles = Vector3(0,-dir/PHYSICS_PI*180,0)
            --一定要删掉，带collider的好像会影响重心
            Component.DestroyImmediate(child:GetComponent(typeof(BoxCollider)))

            local rb = go:AddComponent(typeof(Rigidbody))
            rb.useGravity=false
            rb.angularVelocity = Vector3(0,-w*PHYSICS_PI/180,0)
            GameObject.Destroy(go,lifeCycle/20)
        end
end

--lifeCycle, x, y, ownerid, vx, vy
--TODO w还没接
function LuaHaiZhanMgr.CreatePhysicsTrigger(frameId, id, templateId, dataUd)
    if not CClientMainPlayer.Inst then return end
    local t = g_MessagePack.unpack(dataUd)
    local lifeCycle, x, y,dir, ownerid, vx, vy, w,extra = t[1],t[2],t[3],t[4],t[5],t[6],t[7],t[8],t[9]

    local triggerData = Physics_Trigger.GetData(templateId)
    if not triggerData then return end

    LuaHaiZhanMgr.CreateDebugShape(triggerData.shape,x, y,dir,lifeCycle,vx, vy, w)

    if triggerData.bCannon>0 then
        return
    end

    if triggerData.FX and triggerData.FX.Length>0 then
        --创建特效
        local baseHeight = CPhysicsWorld.Inst:GetBaseHeight()
        for i = 1, triggerData.FX.Length do
            local fxId = triggerData.FX[i-1]
            local fx = CEffectMgr.Inst:AddWorldPositionFX(fxId, Vector3(x,baseHeight,y), 0,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
            RegisterTickOnce(function()
                if fx then
                    fx:Destroy()
                end
            end,lifeCycle*50)
        end
    end
end


function LuaHaiZhanMgr.FreezePhysicsObject(frameId, id, templateId,countDownInfo)
    -- CPhysicsSyncMgr.Inst:Freeze(frameId, id, templateId)

    local pkt = FrameLuaPacket()
    pkt.type = EnumLuaPacketType.eFreeze
    pkt.physicsObjectId = id
    pkt.serverFrameId = frameId
    pkt.table = {
        isFreeze = true,
        templateId = templateId,
        countDownInfo = countDownInfo,
    }
    CommonDefs.ListAdd(CPhysicsSyncMgr.Inst.commands_other, typeof(FramePacket), pkt)
end
function LuaHaiZhanMgr.UnFreezePhysicsObject(frameId, id)
    -- CPhysicsSyncMgr.Inst:UnFreeze(frameId, id)
    local pkt = FrameLuaPacket()
    pkt.type = EnumLuaPacketType.eFreeze
    pkt.physicsObjectId = id
    pkt.serverFrameId = frameId
    pkt.table = {
        isFreeze = false,
    }
    CommonDefs.ListAdd(CPhysicsSyncMgr.Inst.commands_other, typeof(FramePacket), pkt)
end

function LuaHaiZhanMgr.AddPhysicsEffector(frameId, id, triggerId,dataU)
    local t = g_MessagePack.unpack_use_float(dataU)
    local templateId = t[1]
    local force = t[2]
    local triggerX = t[3]
    local triggerY = t[4]

    local pkt = FrameLuaPacket()
    pkt.type = EnumLuaPacketType.eEffector
    pkt.physicsObjectId = id
    pkt.serverFrameId = frameId
    pkt.table = {
        add = true,
        triggerId = triggerId,
        force = force,
        triggerX = triggerX,
        triggerY = triggerY,
    }
    CommonDefs.ListAdd(CPhysicsSyncMgr.Inst.commands_other, typeof(FramePacket), pkt)
end
function LuaHaiZhanMgr.RmPhysicsEffector(frameId, id, triggerId)
    local pkt = FrameLuaPacket()
    pkt.type = EnumLuaPacketType.eEffector
    pkt.physicsObjectId = id
    pkt.serverFrameId = frameId
    pkt.table = {
        add = false,
        triggerId = triggerId,
    }
    CommonDefs.ListAdd(CPhysicsSyncMgr.Inst.commands_other, typeof(FramePacket), pkt)
end

function LuaHaiZhanMgr.SyncPhysicsSnapshot(frameId,shot)
    --print("SyncPhysicsSnapshot",frameId)
    local snapshots = {}

    for i,v in ipairs(shot) do
        local poId = v[1]
        local b2DataTbl = v[2]
        local luaDataTbl = v[3]
        local controllerType = v[4]

        local angle = b2DataTbl[3]
        angle = angle - math.floor(angle / (PHYSICS_PI * 2)) * 2 * PHYSICS_PI
        angle = angle / PHYSICS_PI * 180
        local snapshot = {
            x = b2DataTbl[1],
            y = b2DataTbl[2],
            angle = angle,
            linearVelocityX = b2DataTbl[4],
            linearVelocityY = b2DataTbl[5],
            angularVelocity = b2DataTbl[6]/PHYSICS_PI*180,

            controllerType = controllerType,
        }

        local t = luaDataTbl
        if controllerType==1 then--ship
            CPhysicsShip.ParseSnapShotDataTbl(snapshot,t)
        elseif controllerType==2 then--aiship
            CPhysicsAIShip.ParseSnapShotDataTbl(snapshot,t)
        end

        snapshots[poId] = snapshot

        --还没同步，直接将快照赋值
        if not CPhysicsTimeMgr.Inst.IsSynced then 
            local luapo = g_PhysicsObjectHandlers[poId]
            if luapo then
                luapo:SyncServerSnapshot(snapshot)
                luapo.m_PhysicsObject:SyncTransformImmediate()
            end
        end
    end

    if not CClientMainPlayer.Inst then return end
    if not CPhysicsTimeMgr.Inst.IsSynced then 
        print("time not synced")
        return 
    end
    --print("SyncPhysicsSnapshot",frameId)
    local myPOId = CClientMainPlayer.Inst.AppearanceProp.DriverId

    local needRevert = false
    for poId, snapshot in pairs(snapshots) do
        local newCreated = true
        if g_PhysicsObjectHandlers[poId] then
            newCreated=false
        end
        LuaHaiZhanMgr.CreateShip(frameId,poId, snapshot.x, snapshot.y, snapshot.angle,snapshot.templateId,snapshot.controllerType)
        
        --新创建说明是重新进场景或者断线重连的那次快照，需要设置一下input
        -- print("newCreated",newCreated,snapshot.targetSpeed)
        
        local po = g_PhysicsObjectHandlers[poId]
        if po then
            po.m_PhysicsObject:AddServerSnapshotPosition(frameId,Vector2(snapshot.x,snapshot.y))
            --与历史数据不一致才需要设置
            if not po:CompareSnapshot(frameId,snapshot) then
                needRevert = true
            end
        end
    end
    if needRevert then
        for poId, snapshot in pairs(snapshots) do
            local po = g_PhysicsObjectHandlers[poId]
            if po then
                local pkt = FrameLuaPacket()
                pkt.type = EnumLuaPacketType.eSnapshot
                pkt.physicsObjectId = poId
                pkt.serverFrameId = frameId+1--需要回滚到frameId这一帧，因为回滚的逻辑是回滚到-1这一帧，所以这里+1，刚好能回滚到frameId这一帧
                pkt.table = {
                    snapshot = snapshot,
                }
                CommonDefs.ListAdd(CPhysicsSyncMgr.Inst.commands_other, typeof(FramePacket), pkt)
            end
        end
    end

    --调整引擎对象的朝向
    for i,v in ipairs(shot) do
        local poId = v[1]
        local b2DataTbl = v[2]
        local co = CClientObjectMgr.Inst:GetObject(-poId)
        if co then
            co.Dir = b2DataTbl[3]/PHYSICS_PI*180
        end
    end
end

function LuaHaiZhanMgr.SyncPhysicsCmdQSnapshot(frameId,info)
end

function LuaHaiZhanMgr.ApplyPhysicsAISteering(frameId, id, steeringInfo)
	local info = g_MessagePack.unpack(steeringInfo)
    --print("ApplyPhysicsAISteering",frameId, id, info.id, info.poId, info.x, info.y)
    
    local pkt = FrameLuaPacket()
    pkt.type = EnumLuaPacketType.eSteering
    pkt.physicsObjectId = id
    pkt.serverFrameId = frameId
    pkt.table = {
        steeringInfo = info,
    }
    CommonDefs.ListAdd(CPhysicsSyncMgr.Inst.commands_other, typeof(FramePacket), pkt)
end

-- @param cdInfoUd { serverTime, duration }
function LuaHaiZhanMgr.SyncNavalWarCannonCD(seatId, cdInfoUd)
    local cdInfo = g_MessagePack.unpack(cdInfoUd)
    LuaHaiZhanMgr.s_CDInfo = { [seatId] = cdInfo }
	-- print("SyncNavalWarCannonCD", cdInfo[1], cdInfo[2])
end
function LuaHaiZhanMgr.TeleportPhysicsObjectInScene(frameId, poId, x, y, dir)
    --print("TeleportPhysicsObjectInScene",frameId)
    local pkt = FrameLuaPacket()
    pkt.type = EnumLuaPacketType.eTeleport
    pkt.physicsObjectId = poId
    pkt.serverFrameId = frameId
    pkt.table = {
        x = x,
        y = y,
        dir = dir,
    }
    CommonDefs.ListAdd(CPhysicsSyncMgr.Inst.commands_other, typeof(FramePacket), pkt)

end

function LuaHaiZhanMgr.SyncNavalWarMaterials(materialsUd)
	local materials = g_MessagePack.unpack(materialsUd)

    local cntInfo = materials[1]
	local boardCnt = materials[2]
	local tmpSkills = materials[3]
    LuaHaiZhanMgr.s_CntInfo = cntInfo
    LuaHaiZhanMgr.s_BoardCnt = boardCnt
    LuaHaiZhanMgr.s_TmpSkills = tmpSkills
	g_ScriptEvent:BroadcastInLua("SyncNavalWarMaterials", cntInfo,boardCnt,tmpSkills)
end

function LuaHaiZhanMgr.ApplyPhysicsWorldForce(frameId, poId, x, y)
    local pkt = FrameLuaPacket()
    pkt.type = EnumLuaPacketType.eWorldForce
    pkt.physicsObjectId = poId
    pkt.serverFrameId = frameId
    pkt.table = {
        x = x,
        y = y,
    }
    CommonDefs.ListAdd(CPhysicsSyncMgr.Inst.commands_other, typeof(FramePacket), pkt)
end

function LuaHaiZhanMgr.UpdatePhysicsMulSpeed(frameId, poId, mulSpeed)
    local pkt = FrameLuaPacket()
    pkt.type = EnumLuaPacketType.eMulSpeed
    pkt.physicsObjectId = poId
    pkt.serverFrameId = frameId
    pkt.table = {
        mulSpeed = mulSpeed
    }
    CommonDefs.ListAdd(CPhysicsSyncMgr.Inst.commands_other, typeof(FramePacket), pkt)
end

function LuaHaiZhanMgr.SyncNavalWarSeatInfo(seatInfoU)
    local seatInfo = g_MessagePack.unpack(seatInfoU)
    LuaHaiZhanMgr.s_SeatInfo = seatInfo

    --是否可操控
    CPhysicsInputMgr.Inst.m_CanInput = false
    if CClientMainPlayer.Inst then
        local myId = CClientMainPlayer.Inst.Id
        for key, value in pairs(LuaHaiZhanMgr.s_SeatInfo) do
            if value.id == myId then
                LuaHaiZhanMgr.s_MyRole = LuaHaiZhanMgr.Pos2Role(key)
                LuaHaiZhanMgr.s_MyPos = key
                if LuaHaiZhanMgr.s_MyRole==1 then
                    CPhysicsInputMgr.Inst.m_CanInput = true
                end
            end
        end
    end
    --切换其他角色
    if LuaHaiZhanMgr.s_MyRole~=3 then
        CUIManager.CloseUI(CLuaUIResources.HaiZhanPickWnd)
    end
	g_ScriptEvent:BroadcastInLua("SyncNavalWarSeatInfo", LuaHaiZhanMgr.s_SeatInfo)
end
function LuaHaiZhanMgr.NavalwarShipDriverLeaveRiding(frameId, poId)
    local pkt = FrameLuaPacket()
    pkt.type = EnumLuaPacketType.eDriverLeaveRiding
    pkt.physicsObjectId = poId
    pkt.serverFrameId = frameId
    CommonDefs.ListAdd(CPhysicsSyncMgr.Inst.commands_other, typeof(FramePacket), pkt)
end

function LuaHaiZhanMgr.SyncNavalWarCabinFire(fireNum)
    LuaHaiZhanMgr.s_FireNum = fireNum
	g_ScriptEvent:BroadcastInLua("SyncNavalWarCabinFire", fireNum)
    if fireNum>0 then
        if CCommonUIFxWnd.Instance then
		    CCommonUIFxWnd.Instance:LoadUIFxByID(89000170,-1,true,false)
        end
    else
        if CCommonUIFxWnd.Instance then
            CCommonUIFxWnd.Instance:DestroyUIFxByID(89000170)
        end
    end
end

function LuaHaiZhanMgr.SyncPhysicsCmdCD(cdUd)
    local cd = g_MessagePack.unpack(cdUd)
    LuaHaiZhanMgr.s_CmdCDInfo = cd

end
--天空霰弹技能
function LuaHaiZhanMgr.NavalWarFireSkyShotGun(attackerId, skillId, x, y, delay1, delay2, offsetsUd)
    --skillId 95153601
    -- { {0,1}, {}}
    local offsets = g_MessagePack.unpack(offsetsUd)

    local function createSubEffect(fxId,from,to,duration)
        CEffectMgr.Inst:AddWorldPositionFX(fxId, from, 0,0,1,-1,EnumWarnFXType.None,nil,0,0, 
            DelegateFactory.Action_GameObject(function(go)
                if go and not CommonDefs.IsNull(go) then
                    local dfx = go:GetComponent(typeof(CDynamicWorldPositionFX))
                    if dfx then
                        dfx:SetKickOut(to,EKickOutType.ParabolaWithoutRotate,duration,0,-5,0,0)
                    end
                end
            end))
        --击中水面之后，出现水花
        RegisterTickOnce(function()
            CEffectMgr.Inst:AddWorldPositionFX(88804181, to, 0,0,1,-1,EnumWarnFXType.None,nil,0,0,nil)
        end,duration*1000)
    end


    local obj = CClientObjectMgr.Inst:GetObject(attackerId)
    if obj then
        if obj.m_PhysicsObject and obj.m_PhysicsObject.RO then
            local ro = obj.m_PhysicsObject.RO
            local boneTf = ro:GetSlotTransform("cannon_bone045",false)
            local emitPos = boneTf.position
            local baseHeight = CPhysicsWorld.Inst:GetBaseHeight()
            local fx1 = nil
            --先发到空中
            fx1 = CEffectMgr.Inst:AddWorldPositionFX(88804143, emitPos, 0,0,1,-1,EnumWarnFXType.None,nil,0,0, 
                DelegateFactory.Action_GameObject(function(go)
                    local dfx = go:GetComponent(typeof(CDynamicWorldPositionFX))
                    local height = CRenderScene.Inst:SampleLogicHeight(x,y)
                    local airpos = Vector3(x,height+3,y)
					dfx:SetKickOut(airpos,EKickOutType.ParabolaWithoutRotate,delay1,0,-5,0,0)
                    --然后散开
                    RegisterTickOnce(function()
                        CEffectMgr.Inst:AddWorldPositionFX(88804144, airpos, 0,0,1,-1,EnumWarnFXType.None,nil,0,0,nil)
                        for index, value in ipairs(offsets) do
                            local bx,by = x+value[1],y+value[2]
                            createSubEffect(88804145,airpos,Vector3(bx,baseHeight,by),delay2)
                        end
                        if fx1 then fx1:Destroy() end
                    end,delay1*1000)
                end))
        end
    end
end

function LuaHaiZhanMgr.SyncNavalWarStage(stage)
    LuaHaiZhanMgr.s_Stage = stage
    if LuaHaiZhanMgr.s_StageFxs then
        for key, value in pairs(LuaHaiZhanMgr.s_StageFxs) do
            if value then
                value:Destroy()
            end
        end
        LuaHaiZhanMgr.s_StageFxs = nil
    end

    local design = NavalWar_Stage.GetData(stage)
    if design then
        if LuaHaiZhanMgr.s_InCabin then
            WeatherManager.ApplyProfileByName(design.CabinWeather)
        else
            WeatherManager.ApplyProfileByName(design.Weather)
        end
        local fx = design.WorldPositionFxID
        local baseHeight = CPhysicsWorld.Inst:GetBaseHeight()
        LuaHaiZhanMgr.s_StageFxs = {}
        if fx then
            for i = 1, fx.Length,3 do
                local fxId = fx[i-1]
                local x = fx[i]
                local y = fx[i+1]
                local cfx = CEffectMgr.Inst:AddWorldPositionFX(fxId, Vector3(x,baseHeight,y), 0,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
                table.insert(LuaHaiZhanMgr.s_StageFxs,cfx)
            end
        end
        if design.VolumetricFogSetting and design.VolumetricFogSetting.Length>1 then
            local fogDensity, time = design.VolumetricFogSetting[0],design.VolumetricFogSetting[1]
            LuaRenderSceneMgr.SetVolumeCloudFogDensity(fogDensity, time)
        end

        SoundManager.Inst:ForceSetBgMusic(design.Music)
        if CRenderScene.Inst then
            CRenderScene.Inst:ClearAmbientSound()
            local mapData = PublicMap_PublicMap.GetData(CScene.MainScene.SceneTemplateId)
            local interval = mapData.AmbientSoundInterval
            local soundEnable = PlayerSettings.AmbientSoundEnabled
            for soundPath,x,y,radius,probability in string.gmatch(design.AmbientSound, "(event:[%w_/]+),(-?%w+),(-?%w+),(-?%w+),(-?%w+);?") do
                x,y,radius,probability = tonumber(x),tonumber(y),tonumber(radius),tonumber(probability)
                local sound = CSceneAmbientSound(soundPath,Vector3(x,0,y),radius,probability/100,interval,1)
                sound.Enable = soundEnable
                CommonDefs.ListAdd_LuaCall(CRenderScene.Inst.m_AmbientSound, sound)
            end
        end
    end
end

function LuaHaiZhanMgr.SyncNavalWarShipVirtualHpChange(totalDeltaHp, endTime, duration)
	g_ScriptEvent:BroadcastInLua("SyncNavalWarShipVirtualHpChange", totalDeltaHp, endTime, duration)
end

function LuaHaiZhanMgr.NavalShipStatusChange(shipId, infoUd)
    local info = g_MessagePack.unpack(infoUd)
    -- print("NavalShipStatusChange",shipId,info[1],info[2],info[3])
    if info[1]==EnumNavalPlayShipStatus.Normal then
        LuaHaiZhanMgr.s_Status[shipId] = nil
    else
        LuaHaiZhanMgr.s_Status[shipId] = info
    end
end
function LuaHaiZhanMgr.NavalSyncShipStatus(infoUd)
    local infos = g_MessagePack.unpack(infoUd)
    for shipId, info in pairs(infos) do
        -- print("NavalSyncShipStatus",shipId,info[1],info[2],info[3])
        if info[1]==EnumNavalPlayShipStatus.Normal then
            LuaHaiZhanMgr.s_Status[shipId] = nil
        else
            LuaHaiZhanMgr.s_Status[shipId] = info
        end
    end
end

function LuaHaiZhanMgr.SyncNavalWarShipId2EngineId(myShipId, shipId2EngineIdU)
    LuaHaiZhanMgr.s_MyShipId = myShipId
    local info = g_MessagePack.unpack(shipId2EngineIdU)

    for key, value in pairs(info) do
        LuaHaiZhanMgr.s_ShipId2EngineId[key] = value
        LuaHaiZhanMgr.s_EngineId2ShipId[value] = key

        if g_PhysicsObjectHandlers[-value] then
            g_PhysicsObjectHandlers[-value]:SetForce(key)
        end
    end
end

function LuaHaiZhanMgr.OnQueryNavalPvpShipPosResult(bossGenTime,goldShipInfoU, huntingShipInfoU,resourceU)
    local goldShipInfo = g_MessagePack.unpack(goldShipInfoU)
	local huntingShipInfo = g_MessagePack.unpack(huntingShipInfoU)
    local resourceInfo = g_MessagePack.unpack(resourceU)
	g_ScriptEvent:BroadcastInLua("QueryNavalPvpShipPosResult", bossGenTime,goldShipInfo, huntingShipInfo,resourceInfo)
end

function LuaHaiZhanMgr.GetNavalShipNamesReturn(shipId2NameU)
    local shipId2Name = g_MessagePack.unpack(shipId2NameU)
	g_ScriptEvent:BroadcastInLua("GetNavalShipNamesReturn",shipId2Name)
end

function LuaHaiZhanMgr.NavalPvpGoldChange(shipId, delta, reason, allGoldU)
    local allGold = g_MessagePack.unpack(allGoldU)

    for key, value in pairs(allGold) do
        LuaHaiZhanMgr.s_ShipId2Gold[key] = value
    end

    local list = {}
    for key, value in pairs(LuaHaiZhanMgr.s_ShipId2Gold) do
        table.insert(list,key)
    end
    table.sort(list,function(a,b)
        local gold1 = LuaHaiZhanMgr.s_ShipId2Gold[a] or 0
        local gold2 = LuaHaiZhanMgr.s_ShipId2Gold[b] or 0
        if gold1>gold2 then
            return true
        elseif gold1<gold2 then
            return false
        else
            return a>b
        end
    end)
    --shipid对应的gold排名
    for index, value in ipairs(list) do
        LuaHaiZhanMgr.s_ShipIdGoldRank[value] = index
    end
    LuaHaiZhanMgr.s_PvpPlayerNum = #list

    if shipId == LuaHaiZhanMgr.s_MyShipId then
        if reason == 1 then -- 追猎
            if delta <= 0 then
                g_MessageMgr:ShowMessage("CUSTOM_STRING10", SafeStringFormat3(LocalString.GetString("追猎任务失败，失去了%d金币"), math.abs(delta)))
            end
            LuaTreasureSeaHintWnd.s_Type = 2
            LuaTreasureSeaHintWnd.s_Info = {
                success = delta > 0, 
                gold = delta,
            }
            CUIManager.ShowUI(CLuaUIResources.TreasureSeaHintWnd)
        end
    end

    g_ScriptEvent:BroadcastInLua("NavalPvpGoldChange", shipId, delta, reason)
end
function LuaHaiZhanMgr.NavalPvpPlayEnd(rank, killShip, killMonster, packInfo)
    local info = g_MessagePack.unpack(packInfo)
    LuaTreasureSeaResultWnd.s_Rank = rank
    LuaTreasureSeaResultWnd.s_KillShip = killShip
    LuaTreasureSeaResultWnd.s_KillMonster = killMonster
    LuaTreasureSeaResultWnd.s_Info = info
    CUIManager.ShowUI(CLuaUIResources.TreasureSeaResultWnd)
end

LuaHaiZhanMgr.s_HuntInfo = nil
function LuaHaiZhanMgr.NavalPvpSyncHuntInfo(huntingInfoU)
    LuaHaiZhanMgr.s_HuntInfo = g_MessagePack.unpack(huntingInfoU)
    g_ScriptEvent:BroadcastInLua("NavalPvpSyncHuntInfo", LuaHaiZhanMgr.s_HuntInfo)
end

function LuaHaiZhanMgr.NavalPvpShowSpecialEvent(posIndex, npcEngineId, eventId)
    local data = NavalWar_PvpSpecialEvent.GetData(eventId)
    LuaTreasureSeaEventWnd.s_Type = data.Type
    LuaTreasureSeaEventWnd.s_Info = {}
    LuaTreasureSeaEventWnd.s_PosIndex = posIndex
    LuaTreasureSeaEventWnd.s_NpcEngineId = npcEngineId
    LuaTreasureSeaEventWnd.s_EventId = eventId
    for i = 1, data.Result.Length do
        local type = data.Result[i-1][0]
        if type == 1 then
            LuaTreasureSeaEventWnd.s_Info.gold = data.Result[i-1][1]
        elseif type == 2 then
            LuaTreasureSeaEventWnd.s_Info.exp = data.Result[i-1][1]
        elseif type == 3 then
            LuaTreasureSeaEventWnd.s_Info.skill = {}
            for j = 2, data.Result[i-1].Length do
                table.insert(LuaTreasureSeaEventWnd.s_Info.skill, data.Result[i-1][j-1])
            end
        elseif type == 4 then
            LuaTreasureSeaEventWnd.s_Info.isTrap = true
        end
    end
    CUIManager.ShowUI(CLuaUIResources.TreasureSeaEventWnd)
end

function LuaHaiZhanMgr.NavalPvpAcceptSpecialEvent(eventId, reasonId, bAccept)
    local data = NavalWar_PvpSpecialEvent.GetData(eventId)
    if data then
        local result = {}
        for i = 0, data.Result.Length - 1 do
            table.insert(result, {
                type = data.Result[i][0],
                val = data.Result[i].Length > 1 and data.Result[i][1] or 0,
            })
        end
        if not bAccept then
            if reasonId == 1 then
                g_MessageMgr:ShowMessage("NAVALPVP_CAPTAIN_CANCEL_SPECIAL_EVENT")
            elseif reasonId == 2 then
                g_MessageMgr:ShowMessage("NAVALPVP_ACCEPT_SPECIAL_EVENT_FAIL_TAKEN")
            elseif reasonId == 3 then
                g_MessageMgr:ShowMessage("NAVALPVP_ACCEPT_SPECIAL_EVENT_FAIL_IN_HUNT")
            elseif reasonId == 4 then
                g_MessageMgr:ShowMessage("NAVALPVP_ACCEPT_SPECIAL_EVENT_FAIL_NO_TARGET")
            elseif reasonId == 5 then
                g_MessageMgr:ShowMessage("NAVALPVP_ACCEPT_SPECIAL_EVENT_FAIL_RANGE")
            end
        end
        g_ScriptEvent:BroadcastInLua("NavalPvpAcceptSpecialEvent", reasonId, bAccept, data.Type, result)
    end
end

function LuaHaiZhanMgr.GetCurLv(exp)
    if not exp then return 0, 0 end
    local exp2lv = NavalWar_Setting.GetData().PvpExp2LvUp
    local lv, progress = 1, 1
    for i = 0, exp2lv.Length - 1 do
        if exp >= exp2lv[i] then
            lv = lv + 1
        else
            local lastExp = i > 0 and exp2lv[i-1] or 0
            progress = (exp - lastExp) / (exp2lv[i] - lastExp)
            break
        end
    end
    return lv, progress
end

function LuaHaiZhanMgr.NavalPvpExpChange(shipId, delta, allExpU)
    local allExp = g_MessagePack.unpack(allExpU)
    local myId = math.abs(CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.DriverId or 0)
    local myShipId = LuaHaiZhanMgr.s_EngineId2ShipId[myId] or 0
    for key, value in pairs(allExp) do
        LuaHaiZhanMgr.s_ShipId2Exp[key] = value
        if shipId == key and key == myShipId then
            local preLv = LuaHaiZhanMgr.GetCurLv(value - delta)
            local nowLv = LuaHaiZhanMgr.GetCurLv(value)
            if nowLv ~= preLv then
                LuaTreasureSeaHintWnd.s_Type = 1
                LuaTreasureSeaHintWnd.s_Info = {
                    lv1 = preLv, 
                    lv2 = nowLv,
                }
                if CUIManager.IsLoaded(CLuaUIResources.TreasureSeaEventWnd) or CUIManager.IsLoading(CLuaUIResources.TreasureSeaEventWnd) then
                    LuaTreasureSeaEventWnd.s_OnDestroyCallback = function()
                        --if not CUIManager.IsLoaded(CLuaUIResources.TreasureSeaHintWnd) then
                            CUIManager.ShowUI(CLuaUIResources.TreasureSeaHintWnd)
                        --end
                    end
                else
                    CUIManager.ShowUI(CLuaUIResources.TreasureSeaHintWnd)
                end
            end
            break
        end
    end
end

function LuaHaiZhanMgr.NavalWarDoCreateShipCannon(frameId, poId, bulletTemplateId, x, y, dir, cannonFloor, launchInfoUd)
    --提前做一次判断，防止回滚
    if not LuaClientPhysicsShip.CanShowBullet(poId,x, y) then return end

    local info = g_MessagePack.unpack(launchInfoUd)
    for k,v in ipairs(info) do
        local delay, id, vx, vy, offset, i = v[1],v[2],v[3],v[4],v[5],v[6]

        local lifeCycle = Physics_Trigger.GetData(bulletTemplateId).lifeCycle

        local pkt = FrameLuaPacket()
        pkt.type = EnumLuaPacketType.eSendBullet
        pkt.physicsObjectId = poId
        pkt.serverFrameId = frameId+delay
        pkt.table = {
            templateId = bulletTemplateId,
            vx = vx,
            vy = vy,
            bulletId = id,
            lifeCycle = lifeCycle,
            x = x,
            y = y,
            offset = offset,
            floorId = cannonFloor,
            iter = i,
        }
        CommonDefs.ListAdd(CPhysicsSyncMgr.Inst.commands_other, typeof(FramePacket), pkt)
    end
end

function LuaHaiZhanMgr.RequestNavalSailorFishingSuccess(playerId, deltaTblUd)
    local info = g_MessagePack.unpack(deltaTblUd)
    if CClientMainPlayer.Inst.Id == playerId then
        LuaHaiZhanPickResultWnd.s_Info = info
        CUIManager.ShowUI(CLuaUIResources.HaiZhanPickResultWnd)
    end
end
