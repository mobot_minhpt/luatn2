local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local QnTableView = import "L10.UI.QnTableView"
local GameObject = import "UnityEngine.GameObject"
local CTeamMatchMgr = import "L10.Game.CTeamMatchMgr"

LuaJuDianBattleDailySceneViewWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaJuDianBattleDailySceneViewWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaJuDianBattleDailySceneViewWnd, "BottomLabel", "BottomLabel", UILabel)
RegistChildComponent(LuaJuDianBattleDailySceneViewWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaJuDianBattleDailySceneViewWnd, "RefreshBtn", "RefreshBtn", GameObject)
RegistChildComponent(LuaJuDianBattleDailySceneViewWnd, "TeamBtn", "TeamBtn", GameObject)

--@endregion RegistChildComponent end

function LuaJuDianBattleDailySceneViewWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.RefreshBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRefreshBtnClick()
	end)


	
	UIEventListener.Get(self.TeamBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTeamBtnClick()
	end)


    --@endregion EventBind end
end

function LuaJuDianBattleDailySceneViewWnd:Init()
	Gac2Gas.GuildJuDianQuerySceneOverview()

	self.m_MapIdList = {}

	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.m_MapIdList
    end, 
		function(item, index)
			index = index + 1
			self:InitItem(item, index)
    end)

	self.TitleLabel.text = g_MessageMgr:FormatMessage("JuDian_Battle_Daily_Scene_View_Title_Desc")
	self.BottomLabel.text = g_MessageMgr:FormatMessage("JuDian_Battle_Daily_Scene_View_Bottom_Desc")
end

function LuaJuDianBattleDailySceneViewWnd:InitItem(item, index)
	local sceneLabel = item.transform:Find("SceneLabel"):GetComponent(typeof(UILabel))
	local countLabel = item.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
	local btn = item.transform:Find("Btn").gameObject

	local mapId = self.m_MapIdList[index]
	local data = self.m_Data[mapId]

	countLabel.text = SafeStringFormat3(LocalString.GetString("剩余%s"),tostring(#data))

	local mapData = PublicMap_PublicMap.GetData(mapId)
	sceneLabel.text = mapData.Name

	UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function (go)
	    -- 点击传送
		Gac2Gas.GuildJuDianRequestTeleportToDailyScene(mapId, 0, 0)
	end)
end

function LuaJuDianBattleDailySceneViewWnd:OnData()
	self.m_Data = LuaJuDianBattleMgr.SceneMonsterData

	self.m_MapIdList = {}
	if self.m_Data then
		for k,v in pairs(self.m_Data) do
			table.insert(self.m_MapIdList, k)
		end

		table.sort(self.m_MapIdList, function(a, b)
			local aData = self.m_Data[a]
			local bData = self.m_Data[b]

			if #aData == #bData then
				return a > b
			else
				return #aData > #bData
			end
		end)
	end

	self.TableView:ReloadData(true, true)
end

function LuaJuDianBattleDailySceneViewWnd:OnEnable()
    g_ScriptEvent:AddListener("JuDianBattle_SyncDailySceneData", self, "OnData")
end

function LuaJuDianBattleDailySceneViewWnd:OnDisable()
    g_ScriptEvent:RemoveListener("JuDianBattle_SyncDailySceneData", self, "OnData")
end


--@region UIEvent

function LuaJuDianBattleDailySceneViewWnd:OnRefreshBtnClick()
	Gac2Gas.GuildJuDianQuerySceneOverview()
end

function LuaJuDianBattleDailySceneViewWnd:OnTeamBtnClick()
	-- 组队
	CTeamMatchMgr.Inst:CreateTeam(0)
end

--@endregion UIEvent

