local CPlayerCapacityMgr=import "L10.Game.CPlayerCapacityMgr"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
CLuaPlayerCapacityMgr = {}
CLuaPlayerCapacityMgr.valDic = {}

CLuaPlayerCapacityMgr.cachedPlayerId = 0
function CLuaPlayerCapacityMgr.OnMainPlayerCreated()
    if CClientMainPlayer.Inst then
        if CLuaPlayerCapacityMgr.cachedPlayerId ~= CClientMainPlayer.Inst.Id then
            CLuaPlayerCapacityMgr.cachedPlayerId = CClientMainPlayer.Inst.Id
            Gac2Gas.QueryPlayerCapacity(CLuaPlayerCapacityMgr.cachedPlayerId)
        end
    end
end

function CLuaPlayerCapacityMgr.GetRecommendDesc(key,isHigh)
    local level = CLuaPlayerCapacityMgr.GetLevel(key)
    if level<0 then return nil end

    --先遍历一遍 看看需要找的条目
    local findKey = 1
    PlayerCapacity_Rule_New.Foreach(function (p1, p2) 
        if level >= p2.Grade then
            findKey = p1
        end
    end)
    local data = PlayerCapacity_Rule_New.GetData(findKey)
    local strKeys = key.."_JianYi"
    if data[strKeys] then
        local array = data[strKeys]
        if array==nil or array.Length<1 then return nil end

        local ret = {}
        if isHigh then
            array = array[1]
        else
            array = array[0]
        end
        for i=1,array.Length do
            local tooltip = PlayerCapacity_Advice.GetData(array[i-1])
            if tooltip ~= nil then
                table.insert( ret,CUICommonDef.TranslateToNGUIText(tooltip.JianYi ))
            end
        end
        return ret
    end
    return nil
end

function CLuaPlayerCapacityMgr.GetLevel(key)
    local player = CClientMainPlayer.Inst
    if not player then
        return -1
    end
    local level = player.MaxLevel
    if key == "GrowthValue_Basic" or key=="GrowthValue_Permanent" or key=="GrowthValue_XingGuan" then
        level = player.Level
    end
    return level
end

CLuaPlayerCapacityMgr.playerCapacity_RuleKeyCache ={}
function CLuaPlayerCapacityMgr:FindPlayerCapacity_RuleKey(key)
    local level = CLuaPlayerCapacityMgr.GetLevel(key)
    if level<0 then return -1 end

    if self.playerCapacity_RuleKeyCache[level] then
        return self.playerCapacity_RuleKeyCache[level]
    end
    local findKey = 1
    PlayerCapacity_Rule_New.Foreach(function(k,v)
        if level>=v.Grade then
            findKey = k
        end
    end)
    self.playerCapacity_RuleKeyCache[level] = findKey
    return findKey
end

CLuaPlayerCapacityMgr.m_ModuleDef = nil
function CLuaPlayerCapacityMgr.InitModuleDef()
    if CLuaPlayerCapacityMgr.m_ModuleDef then return end
    local t = {}
    table.insert( t,{
        key = "Growth",
        name = LocalString.GetString("成长"),
        sub = {
            [1] = {"GrowthValue_Basic",LocalString.GetString("基础")},
            [2] = {"GrowthValue_Permanent",LocalString.GetString("额外属性点")},
            [3] = {"GrowthValue_XingGuan",LocalString.GetString("星官"),
                        condition = function()
                            --需要飞升
                            return CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.HasFeiSheng
                        end},
        }
    } )
    table.insert( t,{
        key = "Equip",
        name = LocalString.GetString("装备"),
        sub = {
            [1] = {"EquipValue_Basic",LocalString.GetString("装备基础评分")},
            [2] = {"EquipValue_Intensify",LocalString.GetString("吴山石强化")},
            [3] = {"EquipValue_Hole",LocalString.GetString("宝石")},
            [4] = {"EquipValue_Talisman",LocalString.GetString("法宝")},
            [5] = {"EquipValue_ShenBing",LocalString.GetString("神兵"),                        
                        condition = function()
                            --需要飞升
                            return CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.HasFeiSheng
                        end},
            [6] = {"EquipValue_WenShi",LocalString.GetString("纹饰")},
            [7] = {"EquipValue_IdentifySkill",LocalString.GetString("鉴定技能")},
        }
    } )
    table.insert( t,{
        key = "ShiMen",
        name = LocalString.GetString("师门"),
        sub = {
            [1] = {"ShiMenValue_XiuWei",LocalString.GetString("修为")},
            [2] = {"ShiMenValue_MiShu",LocalString.GetString("师门秘术")},
            [3] = {"ShiMenValue_ZongMen",LocalString.GetString("宗派技能")},
            -- [3] = {"ShiMenValue_HpSkill",LocalString.GetString("长生诀")},
            -- [4] = {"ShiMenValue_AntiPro",LocalString.GetString("职业克符")},
        }
    } )
    table.insert( t,{
        key = "Guild",
        name = LocalString.GetString("帮会"),
        sub = {
            [1] = {"GuildValue_XiuLian",LocalString.GetString("修炼")},
            [2] = {"GuildValue_GongXun",LocalString.GetString("功勋")},
            [3] = {"GuildValue_GuildSkill",LocalString.GetString("帮会技能")},
            [4] = {"GuildValue_LianShen",LocalString.GetString("炼神技能"),condition = function()
                -- 需要帮会炼神激活
                return Guild_GuildBenifit.GetData(2).IsActive == 1
            end}
        }
    } )
    table.insert( t,{
        key = "LingShou",
        name = LocalString.GetString("灵兽"),
        sub = {
            [1] = {"LingShouValue_ChuZhan",LocalString.GetString("出战")},
            [2] = {"LingShouValue_Assist",LocalString.GetString("助战")},
            [3] = {"LingShouValue_Partner",LocalString.GetString("结伴")},
        }
    } )
    table.insert( t,{
        key = "House",
        name = LocalString.GetString("家园"),
        sub = {
            [1] = {"HouseValue_Basic",LocalString.GetString("家园基础")},
            [2] = {"HouseValue_PrayScore",LocalString.GetString("家园祈福")},
        }
    } )
    CLuaPlayerCapacityMgr.m_ModuleDef = t
end

function CLuaPlayerCapacityMgr.GetRecommendValue(key,curVal)
    local isHigh = false
    local recommendVal = 0
    if not CClientMainPlayer.Inst then return false end
    local findKey = CLuaPlayerCapacityMgr:FindPlayerCapacity_RuleKey(key)
    if findKey == -1 then return false end
    local rtn = false
    local data = PlayerCapacity_Rule_New.GetData(findKey)

    local percent = 0
    local percent2 = 0

    local baseValue = data[key][0]
    local highValue = data[key][1]
    local maxValue = data[key][2]

    if maxValue==0 then
        percent = 0
    else
        percent = curVal/maxValue
    end

    if curVal<baseValue then
        isHigh = false
        recommendVal = baseValue
        return true,isHigh,recommendVal,percent,baseValue,highValue,maxValue
    elseif curVal<highValue then
        isHigh = true
        recommendVal = highValue
        return true,isHigh,recommendVal,percent,baseValue,highValue,maxValue
    else
        recommendVal = maxValue
        return false,true,recommendVal,percent,baseValue,highValue,maxValue
    end
end
	--成长模块
    --基础 GrowthValue_Basic
	--额外属性点 GrowthValue_Permanent
	--星官 GrowthValue_XingGuan

	--装备模块
	--吴山石强化 EquipValue_Intensify
	--宝石 EquipValue_Hole
	--法宝 EquipValue_Talisman
	--神兵 EquipValue_ShenBing
	--纹饰 EquipValue_WenShi
	--鉴定技能 EquipValue_IdentifySkill

	--基础评分，装备评分-家园词条-其他上面独立项 EquipValue_Basic

	--师门模块
	--修为 ShiMenValue_XiuWei
	--师门秘术 ShiMenValue_MiShu
	--长生诀 ShiMenValue_HpSkill
	--职业克符 ShiMenValue_AntiPro

	--帮会模块
	--修炼 GuildValue_XiuLian
	--功勋 GuildValue_GongXun
	--帮会技能 GuildValue_GuildSkill
	--炼神技能 GuildValue_LianShen

	--灵兽
	--出战 LingShouValue_ChuZhan
	--助战 LingShouValue_Assist
	--结伴 LingShouValue_Partner

	--家园
	--家园基础 HouseValue_Basic
	--家园祈福 HouseValue_PrayScore


function CLuaPlayerCapacityMgr.UpdatePlayerCapacity(targetId,capability, detailData)
    
    if targetId == CClientMainPlayer.Inst.Id then
        CLuaPlayerCapacityMgr.InitModuleDef()

        CLuaPlayerCapacityMgr.valDic = {}
        CommonDefs.DictIterate(detailData,DelegateFactory.Action_object_object(function (___key, ___value)
            local itemVal = tonumber(___value)
            CLuaPlayerCapacityMgr.valDic[___key] = itemVal
        end))

        local v1 = CLuaPlayerCapacityMgr.valDic["ShiMenValue_HpSkill"] or 0
        local v2 = CLuaPlayerCapacityMgr.valDic["ShiMenValue_AntiPro"] or 0
        CLuaPlayerCapacityMgr.valDic["ShiMenValue_ZongMen"] = v1+v2

        local needAlert = false
        local requestKeys={}

        for i,v in ipairs(CLuaPlayerCapacityMgr.m_ModuleDef) do
        if not v.ignore then
            for i2,v2 in ipairs(v.sub) do
                table.insert( requestKeys, v2[1] )
            end
        end
        end

        for i,key in ipairs(requestKeys) do
            local curVal = CLuaPlayerCapacityMgr.valDic[key] or 0
            local isHigh = false
            local recommendVal = 0
            local rtn,isHigh,recommendVal =  CLuaPlayerCapacityMgr.GetRecommendValue(key,curVal)
            if rtn then
                if not isHigh then
                    needAlert = true
                    break
                end
            end
        end
        if needAlert then
            CPlayerCapacityMgr.Inst.showAlert = true
        else
            CPlayerCapacityMgr.Inst.showAlert = false
        end
        EventManager.BroadcastInternalForLua(EnumEventType.RefreshGuidelineAlert,{})
    end
end

function CLuaPlayerCapacityMgr.GetValue(valKey)
    CLuaPlayerCapacityMgr.InitModuleDef()
        --先计算总的
    for i,v in ipairs(CLuaPlayerCapacityMgr.m_ModuleDef) do
        if v.key==valKey then
            local sum = 0
            for i2,v2 in ipairs(v.sub) do
                local val = CLuaPlayerCapacityMgr.valDic[v2[1]] or 0
                sum = sum+val
            end
            return sum
        end
    end
    return CLuaPlayerCapacityMgr.valDic[valKey] or 0
end
-- 添加一个新的模块
function CLuaPlayerCapacityMgr.AddNewModuleDef(newtable)
    if not CLuaPlayerCapacityMgr.m_ModuleDef then CLuaPlayerCapacityMgr.InitModuleDef() end
    for i,v in ipairs(CLuaPlayerCapacityMgr.m_ModuleDef) do
        if v.key==key then
            CLuaPlayerCapacityMgr.m_ModuleDef[i].name = table.name
            CLuaPlayerCapacityMgr.m_ModuleDef[i].sub = table.sub
            return
        end
    end
    table.insert(CLuaPlayerCapacityMgr.m_ModuleDef,newtable)
end