require("common/common_include")

local LuaUtils = import "LuaUtils"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local NGUITools = import "NGUITools"
local UITable = import "UITable"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local AlignType3 = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local CTrackMgr = import "L10.Game.CTrackMgr"

CLuaCityWarScheduleWnd = class()
CLuaCityWarScheduleWnd.Path = "ui/citywar/LuaCityWarScheduleWnd"

RegistClassMember(CLuaCityWarScheduleWnd, "m_EnterBtnObj")
RegistClassMember(CLuaCityWarScheduleWnd, "m_WatchBtnObj")

function CLuaCityWarScheduleWnd:Init( ... )
	Gac2Gas.QueryCityWarOccupyOpen()
	Gac2Gas.QueryMirrorWarWatchEnable()

	UIEventListener.Get(self.transform:Find("Anchor/DetailBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		CUIManager.ShowUI(CLuaUIResources.CityWarRuleTip)
	end)
	UIEventListener.Get(self.transform:Find("Anchor/AwardBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		Gac2Gas.QueryTerritoryOccupyAwardList()
	end)
	UIEventListener.Get(self.transform:Find("Anchor/MapBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		CLuaCityWarMgr:OpenPrimaryMap()
	end)
	UIEventListener.Get(self.transform:Find("Anchor/CityBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		Gac2Gas.QueryMyCityData(EnumQueryCityDataAction.eCityData)
	end)

	local templateObj = self.transform:Find("Anchor/Template").gameObject
	templateObj:SetActive(false)
	local rootTable = self.transform:Find("Anchor/Scroll View/Table"):GetComponent(typeof(UITable))

	local content = g_MessageMgr:FormatMessage("KFCZ_SCHEDULE_OVERVIEW_INTERFACE_TIP")
	for p in string.gmatch(content, "(.-)\n") do
		if p ~= "" then
			local obj = NGUITools.AddChild(rootTable.gameObject, templateObj)
			obj:SetActive(true)
			obj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = p
		end
	end
	rootTable:Reposition()

	local time = g_MessageMgr:FormatMessage("KFCZ_SCHEDULE_INTERFACE_TIME")
	self.transform:Find("Anchor/TimeLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("活动时间")..time
	UIEventListener.Get(self.transform:Find("Anchor/JoinBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		CTrackMgr.Inst:TrackToGuildNpc(20003038, 30, 97)
	end)

	self:InitButton()
end

function CLuaCityWarScheduleWnd:InitButton()
	self.m_EnterBtnObj = self.transform:Find("Anchor/EnterCopySceneBtn").gameObject
	UIEventListener.Get(self.m_EnterBtnObj).onClick = LuaUtils.VoidDelegate(function (go)
		Gac2Gas.QueryMirrorWarPlayNum()
		
		--[[local popupMenuItemTable = {}
		local indexNameTbl = {LocalString.GetString("一"), LocalString.GetString("二"), LocalString.GetString("三")}
		for i = 1, 3 do
			table.insert(popupMenuItemTable, PopupMenuItemData(LocalString.GetString("战场")..indexNameTbl[i], DelegateFactory.Action_int(function (index)
				self:EnterCopyScene(i)
				end), false, nil, EnumPopupMenuItemStyle.Default))
		end
		local popupMenuItemArray = Table2Array(popupMenuItemTable, MakeArrayClass(PopupMenuItemData))
		CPopupMenuInfoMgr.ShowPopupMenu(popupMenuItemArray, go.transform, AlignType3.Top, 1, nil, 600, true, 227)--]]
	end)
	self.m_WatchBtnObj = self.transform:Find("Anchor/WatchBtn").gameObject
	UIEventListener.Get(self.m_WatchBtnObj).onClick = LuaUtils.VoidDelegate(function ( ... )
		Gac2Gas.QueryMirrorWarWatchInfo()
	end)
end

function CLuaCityWarScheduleWnd:EnterCopyScene(index)
	Gac2Gas.RequestEnterMirrorWarPlay(index)
end

function CLuaCityWarScheduleWnd:QueryMirrorWarPlayNumResult( playNum )
	local popupMenuItemTable = {}
	local indexNameTbl = {LocalString.GetString("一"), LocalString.GetString("二"), LocalString.GetString("三")}
	for i = 1, playNum do
		table.insert(popupMenuItemTable, PopupMenuItemData(LocalString.GetString("战场")..indexNameTbl[i], DelegateFactory.Action_int(function (index)
			self:EnterCopyScene(i)
			end), false, nil, EnumPopupMenuItemStyle.Default))
	end
	local popupMenuItemArray = Table2Array(popupMenuItemTable, MakeArrayClass(PopupMenuItemData))
	CPopupMenuInfoMgr.ShowPopupMenu(popupMenuItemArray, self.m_EnterBtnObj.transform, AlignType3.Top, 1, nil, 600, true, 227)
end

function CLuaCityWarScheduleWnd:QueryCityWarOccupyOpenResult( bOpen )
	local str = LocalString.GetString("本周  [fed30b]开战中")
	if not bOpen then str = LocalString.GetString("本周  [c][7ef507]休战中") end
	self.transform:Find("Anchor/OpenStateLabel"):GetComponent(typeof(UILabel)).text = str
end

function CLuaCityWarScheduleWnd:QueryMirrorWarWatchEnableResult(bEnable)
	self.m_WatchBtnObj:SetActive(bEnable)
end

function CLuaCityWarScheduleWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("QueryMirrorWarPlayNumResult", self, "QueryMirrorWarPlayNumResult")
	g_ScriptEvent:AddListener("QueryCityWarOccupyOpenResult", self, "QueryCityWarOccupyOpenResult")
	g_ScriptEvent:AddListener("QueryMirrorWarWatchEnableResult", self, "QueryMirrorWarWatchEnableResult")
end

function CLuaCityWarScheduleWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("QueryMirrorWarPlayNumResult", self, "QueryMirrorWarPlayNumResult")
	g_ScriptEvent:RemoveListener("QueryCityWarOccupyOpenResult", self, "QueryCityWarOccupyOpenResult")
	g_ScriptEvent:RemoveListener("QueryMirrorWarWatchEnableResult", self, "QueryMirrorWarWatchEnableResult")
end
