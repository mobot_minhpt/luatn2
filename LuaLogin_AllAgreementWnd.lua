local UITabBar = import "L10.UI.UITabBar"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local QnScrollLabel = import "L10.UI.QnScrollLabel"
local CChatLinkMgr = import "CChatLinkMgr"
local CLoginMgr = import "L10.Game.CLoginMgr"
local PlayerSettings = import "L10.Game.PlayerSettings"
local JsonMapper = import "JsonMapper"
local AdvertU3d = import "NtUniSdk.Unity3d.AdvertU3d"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local SystemInfo = import "UnityEngine.SystemInfo"
local SdkU3d = import "NtUniSdk.Unity3d.SdkU3d"
local PatchMgr = import "L10.Engine.PatchMgr"

LuaLogin_AllAgreementWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLogin_AllAgreementWnd, "AgreeBtn", "AgreeBtn", GameObject)
RegistChildComponent(LuaLogin_AllAgreementWnd, "DisagreeBtn", "DisagreeBtn", GameObject)
RegistChildComponent(LuaLogin_AllAgreementWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaLogin_AllAgreementWnd, "PreviousBtn", "PreviousBtn", GameObject)
RegistChildComponent(LuaLogin_AllAgreementWnd, "NextBtn", "NextBtn", GameObject)
RegistChildComponent(LuaLogin_AllAgreementWnd, "PageLabel", "PageLabel", UILabel)
RegistChildComponent(LuaLogin_AllAgreementWnd, "TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaLogin_AllAgreementWnd, "QnScrollLabel", "QnScrollLabel", QnScrollLabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaLogin_AllAgreementWnd, "m_CurPageIndex")
RegistClassMember(LuaLogin_AllAgreementWnd, "m_TabIndex")
RegistClassMember(LuaLogin_AllAgreementWnd, "m_CurVersion1")
RegistClassMember(LuaLogin_AllAgreementWnd, "m_CurVersion2")
RegistClassMember(LuaLogin_AllAgreementWnd, "m_Pages1")
RegistClassMember(LuaLogin_AllAgreementWnd, "m_Pages2")
RegistClassMember(LuaLogin_AllAgreementWnd, "m_Pages")

function LuaLogin_AllAgreementWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.AgreeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAgreeBtnClick()
	end)


	
	UIEventListener.Get(self.DisagreeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDisagreeBtnClick()
	end)


	
	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)


	
	UIEventListener.Get(self.PreviousBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPreviousBtnClick()
	end)


	
	UIEventListener.Get(self.NextBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnNextBtnClick()
	end)


    --@endregion EventBind end
end

function LuaLogin_AllAgreementWnd:Init()
	self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabChange(index)
	end)
	self.m_Pages1, self.m_CurVersion1 = self:LoadProtocol(0)
	self.m_Pages2, self.m_CurVersion2 = self:LoadProtocol(1)
	
	local isTesting = false
	if CommonDefs.IS_PUB_RELEASE and CommonDefs.IS_VN_CLIENT then
		isTesting = PatchMgr.Inst:GetBuildConfig("IsTesting") == "true"
	end
	if isTesting then
		PlayerSettings.AgreeToUserAgreement = true
		PlayerSettings.UserAgreementVersion = self.m_CurVersion1
		CLoginMgr.Inst:SaveCurAccountToAgreeAccountList()

		PlayerSettings.PrivacyAgreeVersion = self.m_CurVersion2
		CLoginMgr.Inst:SaveCurAccountToPrivacyAgreeList()
		CUIManager.CloseUI("Login_AllAgreementWnd")
	else
		if CommonDefs.IS_VN_CLIENT then
			if not LocalString.isVN then
				self.TabBar:ChangeTab(1, false)
				self:OnTabChange(1)
				
				--只显示隐私协议
				local userAgreementGo = self.TabBar:GetTabGoByIndex(0)
				userAgreementGo:SetActive(false)
			else
				self.TabBar:ChangeTab(0, false)
				self:OnTabChange(0)
			end
		end
	end
end

function LuaLogin_AllAgreementWnd:LoadProtocol(index)
	local protocol = index == 0 and (CLoginMgr.Inst:GetUserAgreementContent()) or (CLoginMgr.Inst:GetPrivacyAgreementContent())
	local tempPages = CLoginMgr.Inst:GetAgreementPages(protocol)
	local curVersion = ""
	if tempPages.Count >= 1 then
		curVersion = tempPages[tempPages.Count - 1]
	end
	local pages = {}
	for i = 0, tempPages.Count - 2 do
		table.insert(pages, tempPages[i])
	end
	return pages, curVersion
end

function LuaLogin_AllAgreementWnd:ShowBtn(agree)
	self.AgreeBtn.gameObject:SetActive(not agree)
	self.DisagreeBtn.gameObject:SetActive(not agree)
	self.CloseButton.gameObject:SetActive(agree)
end

function LuaLogin_AllAgreementWnd:ShowPage()
	if self.m_CurPageIndex >= 1 and self.m_CurPageIndex <= #self.m_Pages then
		self.PageLabel.text = SafeStringFormat3("%d/%d", self.m_CurPageIndex, #self.m_Pages)
		self.QnScrollLabel:SetContent(CChatLinkMgr.TranslateToNGUIText(self.m_Pages[self.m_CurPageIndex]))
	else
		self.PageLabel.text = SafeStringFormat3("%d/%d", 0, #self.m_Pages)
		self.QnScrollLabel:SetContent("")
	end
end


--@region UIEvent

function LuaLogin_AllAgreementWnd:OnAgreeBtnClick()
	PlayerSettings.AgreeToUserAgreement = true
	PlayerSettings.UserAgreementVersion = self.m_CurVersion1
	CLoginMgr.Inst:SaveCurAccountToAgreeAccountList()

	PlayerSettings.PrivacyAgreeVersion = self.m_CurVersion2
	CLoginMgr.Inst:SaveCurAccountToPrivacyAgreeList()

	local uid = CLoginMgr.Inst.m_UniSdkLoginInfo.UniSdk_UID
	local platform = "PC"
	if CommonDefs.IsIOSPlatform() then
		platform = "iOS"
	elseif CommonDefs.IsAndroidPlatform() then
		platform = "android"
	end
	local deviceModel = SystemInfo.deviceModel
	local timestamp = CServerTimeMgr.Inst.timeStamp
	local jsonStr = SafeStringFormat3("{\"userID\":\"%s\",\"platform\":\"%s\",\"device_model\":\"%s\",\"timestamp\":\"%s\"}", uid, platform, deviceModel, timestamp)
	local param = JsonMapper.ToObject(jsonStr)
	AdvertU3d.trackEvent("L10_AGREE_ALL_AGREEMENT", param)

	CUIManager.CloseUI("Login_AllAgreementWnd")
end

function LuaLogin_AllAgreementWnd:OnDisagreeBtnClick()
	PlayerSettings.AgreeToUserAgreement = false
	local msg = g_MessageMgr:FormatMessage("CUSTOM_STRING1", LocalString.GetString("请阅读并同意服务条款和隐私政策以继续游戏。"))
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
	end),DelegateFactory.Action(function()
		CUIManager.CloseUI("Login_AllAgreementWnd")
		SdkU3d.exit()
		CLoginMgr.Inst:UniSdkSwitchAccount()
	end),LocalString.GetString("继续阅读"),LocalString.GetString("关闭"),false)
end

function LuaLogin_AllAgreementWnd:OnPreviousBtnClick()
	if self.m_CurPageIndex > 1 then
		self.m_CurPageIndex = self.m_CurPageIndex - 1
	end
	self:ShowPage()
end

function LuaLogin_AllAgreementWnd:OnNextBtnClick()
	if self.m_CurPageIndex < #self.m_Pages then
		self.m_CurPageIndex = self.m_CurPageIndex + 1
	end
	self:ShowPage()
end

function LuaLogin_AllAgreementWnd:OnTabChange(index)
	self.m_TabIndex = index
	self.m_CurPageIndex = 1
	self.m_Pages = self.m_TabIndex == 0 and self.m_Pages1 or self.m_Pages2
	local agree = (CLoginMgr.Inst:AlreadyAgree()) and (CLoginMgr.Inst:PrivacyAgreementAccepted())
	if not cs_string.IsNullOrEmpty(self.m_CurVersion1) then
		agree = agree and (self.m_CurVersion1 == PlayerSettings.UserAgreementVersion)
	end

	if not cs_string.IsNullOrEmpty(self.m_CurVersion2) then
		agree = agree and (self.m_CurVersion2 == PlayerSettings.PrivacyAgreeVersion)
	end
	self:ShowBtn(agree)
	self:ShowPage()
end

function LuaLogin_AllAgreementWnd:OnCloseButtonClick()
	CUIManager.CloseUI("Login_AllAgreementWnd")
end


--@endregion UIEvent

