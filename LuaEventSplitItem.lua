local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local EnumNPCChatType = import "L10.Game.EnumNPCChatType"
local EnumNPCChatMsgLocation = import "L10.Game.EnumNPCChatMsgLocation"
LuaEventSplitItem = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaEventSplitItem, "MsgLabel", "MsgLabel", UILabel)
RegistChildComponent(LuaEventSplitItem, "TextureRight", "TextureRight", GameObject)
RegistChildComponent(LuaEventSplitItem, "TextureLeft", "TextureLeft", GameObject)

--@endregion RegistChildComponent end

function LuaEventSplitItem:Awake()
    self.MsgLabel.text = ""
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaEventSplitItem:Init(data)
    self.MsgLabel.gameObject:SetActive(false)
    local MsgId = data.NPCChatMsgId
    local MsgInfo = NpcChat_ChatMsg.GetData(MsgId)
    local GroupName = ""
    local EventTip = ""
    if data.NPCChatType == EnumToInt(EnumNPCChatType.AddFriendTip) then
        self.MsgLabel.text = g_MessageMgr:FormatMessage("NPCChat_AddNPCFriend_Success",data.text)
        self.MsgLabel.gameObject:SetActive(true)
        return 
    end
    if data.usedForDisplayStorySplit == EnumToInt(EnumNPCChatMsgLocation.First) then
        EventTip = NpcChat_Setting.GetData().GroupBeginMsgTip
    elseif data.usedForDisplayStorySplit == EnumToInt(EnumNPCChatMsgLocation.End) then
        EventTip = NpcChat_Setting.GetData().GroupEndMsgTip
    end
    if MsgInfo then 
        local GroupInfo = NpcChat_ChatGroup.GetData(MsgInfo.GroupID)
        if GroupInfo then
            GroupName = GroupInfo.Name
        end
    end
    self.MsgLabel.text = SafeStringFormat3(EventTip,GroupName)
    self.MsgLabel.gameObject:SetActive(true)
end

--@region UIEvent

--@endregion UIEvent

