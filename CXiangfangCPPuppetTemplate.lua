-- Auto Generated!!
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CXiangfangCPPuppetTemplate = import "L10.UI.CXiangfangCPPuppetTemplate"
local EnumChineseDigits = import "L10.Game.EnumChineseDigits"
local House_XiangfangUpgrade = import "L10.Game.House_XiangfangUpgrade"
local HousePuppet_CPPuppet = import "L10.Game.HousePuppet_CPPuppet"
local Monster_Monster = import "L10.Game.Monster_Monster"
CXiangfangCPPuppetTemplate.m_OnNotOpenSpriteClick_CS2LuaHook = function (this, go) 
    for i = 1, 20 do
        local data = House_XiangfangUpgrade.GetData(i)
        if data ~= nil and data.MaxRoomerNum > this.Index then
            g_MessageMgr:ShowMessage("Roomer_Num_Need_Xiangfang_Grade", i)
            return
        end
    end
end
CXiangfangCPPuppetTemplate.m_Init_CS2LuaHook = function (this, index, puppetInfo) 
    if puppetInfo ~= nil then
        this.Idx = puppetInfo.Idx
    else
        this.Idx = 0
    end

    if puppetInfo ~= nil and puppetInfo.X ~= 0 and puppetInfo.Y ~= 0 then
        this.placedNode:SetActive(true)
    else
        this.placedNode:SetActive(false)
    end
    this.Index = index
    if CClientHouseMgr.Inst.mConstructProp ~= nil then
        this.IdLabel.text = EnumChineseDigits.GetDigit(index + 1)
        local xiangfangGrade = CClientHouseMgr.Inst.mConstructProp.XiangfangGrade
        local data = House_XiangfangUpgrade.GetData(xiangfangGrade)
        if data ~= nil then
            local maxRoomCurGrade = data.MaxRoomerNum

            if index >= maxRoomCurGrade then
                this.NotOpenGO:SetActive(true)
                this.PlayerInfoGO:SetActive(false)
                this.NoPlayerGO:SetActive(false)
            else
                if puppetInfo == nil then
                    this.NotOpenGO:SetActive(false)
                    this.PlayerInfoGO:SetActive(false)
                    this.NoPlayerGO:SetActive(true)
                else
                    this.NotOpenGO:SetActive(false)
                    this.PlayerInfoGO:SetActive(true)
                    this.NoPlayerGO:SetActive(false)
                    --string portrait = CUICommonDef.GetPortraitName((int)puppetInfo.Class, (int)prop.Gender);
                    local name = puppetInfo.Name

                    local cppuppet = HousePuppet_CPPuppet.GetData(puppetInfo.Id)
                    local monsterId = 0
                    if cppuppet ~= nil then
                        if puppetInfo.Difficulty == 0 then
                            monsterId = cppuppet.Easy
                        elseif puppetInfo.Difficulty == 1 then
                            monsterId = cppuppet.Normal
                        elseif puppetInfo.Difficulty == 2 then
                            monsterId = cppuppet.Tough
                        elseif puppetInfo.Difficulty == 3 then
                            monsterId = cppuppet.Hard
                        elseif puppetInfo.Difficulty == 4 then
                            monsterId = cppuppet.Dead
                        end
                    end
                    local mdata = Monster_Monster.GetData(monsterId)
                    if mdata ~= nil then
                        this.Portrait:LoadNPCPortrait(mdata.HeadIcon, false)
                        this.NameLabel.text = mdata.Name
                    end
                    this.Expression.gameObject:SetActive(false)
                end
            end
        end
    end
end
