require("3rdParty/ScriptEvent")
require("common/common_include")

local Gac2Gas = import "L10.Game.Gac2Gas"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local CUITexture = import "L10.UI.CUITexture"
local CButton = import "L10.UI.CButton"
local GameObject = import "UnityEngine.GameObject"
local LocalString = import "LocalString"

LuaBabyGetBackWnd=class()
RegistChildComponent(LuaBabyGetBackWnd,"closeBtn", GameObject)
RegistChildComponent(LuaBabyGetBackWnd,"oneNode", GameObject)
RegistChildComponent(LuaBabyGetBackWnd,"twoNode", GameObject)
RegistChildComponent(LuaBabyGetBackWnd,"unlockBtn", GameObject)

function LuaBabyGetBackWnd:InitBabyShow(babyInfo,nameText,babyTexture)
  nameText.text = babyInfo[4] .. "  " .. LocalString.GetString("等级") .. babyInfo[2]
  babyTexture:LoadNPCPortrait(LuaBabyMgr.GetBabyPortrait(babyInfo[5],babyInfo[3],babyInfo[6]), false)
end

function LuaBabyGetBackWnd:InitShowNode(babyTable)
  if #babyTable == 1 then
    self.oneNode:SetActive(true)
    self.twoNode:SetActive(false)
    local babyInfo = babyTable[1]
    local nameText = self.oneNode.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local babyTexture = self.oneNode.transform:Find("Texture"):GetComponent(typeof(CUITexture))
    self:InitBabyShow(babyInfo,nameText,babyTexture)

    local unlockBtnScript = self.unlockBtn:GetComponent(typeof(CButton))
    if unlockBtnScript then
      unlockBtnScript.Enabled = true
    end
    local babyIndex = babyTable[1][1]
    local onSubmitClick = function(go)
      if LuaBabyMgr.m_ShowGetBabyType == 1 then
        Gac2Gas.RequestRestoreBaby(babyIndex)
      elseif LuaBabyMgr.m_ShowGetBabyType == 2 then
        Gac2Gas.RequestLoadBabyFromOrphan(babyIndex)
      end
      CUIManager.CloseUI(CLuaUIResources.BabyGetBackWnd)
  	end
    CommonDefs.AddOnClickListener(self.unlockBtn,DelegateFactory.Action_GameObject(onSubmitClick),false)
  elseif #babyTable >= 2 then
    self.oneNode:SetActive(false)
    self.twoNode:SetActive(true)
    local babyInfo = babyTable[1]
    local nameText = self.twoNode.transform:Find("left/NameLabel"):GetComponent(typeof(UILabel))
    local babyTexture = self.twoNode.transform:Find("left/Texture"):GetComponent(typeof(CUITexture))
    self:InitBabyShow(babyInfo,nameText,babyTexture)
    local babyInfo2 = babyTable[2]
    local nameText2 = self.twoNode.transform:Find("right/NameLabel"):GetComponent(typeof(UILabel))
    local babyTexture2 = self.twoNode.transform:Find("right/Texture"):GetComponent(typeof(CUITexture))
    self:InitBabyShow(babyInfo2,nameText2,babyTexture2)
    local unlockBtnScript = self.unlockBtn:GetComponent(typeof(CButton))
    if unlockBtnScript then
      unlockBtnScript.Enabled = false
    end

    local babyIndex = 0
    local btn1 = self.twoNode.transform:Find("left").gameObject
    local btn2 = self.twoNode.transform:Find("right").gameObject
    local btn1Light = self.twoNode.transform:Find("left/light").gameObject
    local btn2Light = self.twoNode.transform:Find("right/light").gameObject
    btn1Light:SetActive(false)
    btn2Light:SetActive(false)
    local btn1Click = function(go)
      babyIndex = babyTable[1][1]
      if unlockBtnScript then
        unlockBtnScript.Enabled = true
      end
      btn1Light:SetActive(true)
      btn2Light:SetActive(false)
    end
    local btn2Click = function(go)
      babyIndex = babyTable[2][1]
      if unlockBtnScript then
        unlockBtnScript.Enabled = true
      end
      btn1Light:SetActive(false)
      btn2Light:SetActive(true)
    end
    CommonDefs.AddOnClickListener(btn1,DelegateFactory.Action_GameObject(btn1Click),false)
    CommonDefs.AddOnClickListener(btn2,DelegateFactory.Action_GameObject(btn2Click),false)

    local onSubmitClick = function(go)
      if LuaBabyMgr.m_ShowGetBabyType == 1 then
        Gac2Gas.RequestRestoreBaby(babyIndex)
      elseif LuaBabyMgr.m_ShowGetBabyType == 2 then
        Gac2Gas.RequestLoadBabyFromOrphan(babyIndex)
      end
      CUIManager.CloseUI(CLuaUIResources.BabyGetBackWnd)
  	end
    CommonDefs.AddOnClickListener(self.unlockBtn,DelegateFactory.Action_GameObject(onSubmitClick),false)
  end
end

function LuaBabyGetBackWnd:Init()
	local onCloseClick = function(go)
    CUIManager.CloseUI(CLuaUIResources.BabyGetBackWnd)
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

  self.oneNode:SetActive(false)
  self.twoNode:SetActive(false)

  local unlockBtnScript = self.unlockBtn:GetComponent(typeof(CButton))
  if unlockBtnScript then
    unlockBtnScript.Enabled = false
  end

  self:InitShowNode(LuaBabyMgr.m_ShowGetBabyInfo)
end

function LuaBabyGetBackWnd:OnDestroy()

end

return LuaBabyGetBackWnd
