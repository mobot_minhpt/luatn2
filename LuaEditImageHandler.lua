local Vector3 = import "UnityEngine.Vector3"
local UIEventListener = import "UIEventListener"
local Mathf = import "UnityEngine.Mathf"
local CommonDefs = import "L10.Game.CommonDefs"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local Input = import "UnityEngine.Input"
local BoxCollider = import "UnityEngine.BoxCollider"

CLuaEditImageHandler = class()
RegistChildComponent(CLuaEditImageHandler,"lineRight",UISprite)
RegistChildComponent(CLuaEditImageHandler,"lineLeft",UISprite)
RegistChildComponent(CLuaEditImageHandler,"lineTop",UISprite)
RegistChildComponent(CLuaEditImageHandler,"lineBottom",UISprite)
RegistChildComponent(CLuaEditImageHandler,"pivot",GameObject)
RegistChildComponent(CLuaEditImageHandler,"confirmBtn",GameObject)
RegistChildComponent(CLuaEditImageHandler,"cancleBtn",GameObject)
RegistChildComponent(CLuaEditImageHandler,"rotateBtn",GameObject)
--RegistChildComponent(CLuaEditImageHandler,"flipBtn",GameObject)
RegistClassMember(CLuaEditImageHandler,"flipBtn")
RegistClassMember(CLuaEditImageHandler,"onCancle")
RegistClassMember(CLuaEditImageHandler,"onConfirm")
RegistClassMember(CLuaEditImageHandler,"boxCollider")
RegistClassMember(CLuaEditImageHandler,"contentSprite")
RegistClassMember(CLuaEditImageHandler,"startEdit")
RegistClassMember(CLuaEditImageHandler,"startMove")
RegistChildComponent(CLuaEditImageHandler,"portrait",UIPanel)
RegistClassMember(CLuaEditImageHandler,"topRightPos")
RegistClassMember(CLuaEditImageHandler,"topLeftPos")
RegistClassMember(CLuaEditImageHandler,"bottomRightPos")
RegistClassMember(CLuaEditImageHandler,"bottomLeftPos")
RegistClassMember(CLuaEditImageHandler,"relatedWidget")
RegistClassMember(CLuaEditImageHandler,"rawWidth")
RegistClassMember(CLuaEditImageHandler,"rawHeight")
RegistClassMember(CLuaEditImageHandler,"DefaultSize")
RegistClassMember(CLuaEditImageHandler,"MaxSize")
RegistClassMember(CLuaEditImageHandler,"MaxDiagonalLength")
RegistClassMember(CLuaEditImageHandler,"Index")
RegistClassMember(CLuaEditImageHandler,"Depth")
RegistClassMember(CLuaEditImageHandler,"MinSize")
RegistClassMember(CLuaEditImageHandler,"MinDiagonalLength")
RegistClassMember(CLuaEditImageHandler,"vec1")
RegistClassMember(CLuaEditImageHandler,"vec2")
RegistClassMember(CLuaEditImageHandler,"angle1")
RegistClassMember(CLuaEditImageHandler,"angle2")
RegistClassMember(CLuaEditImageHandler,"preBottomRightPos")
RegistClassMember(CLuaEditImageHandler,"prePivotPos")

function CLuaEditImageHandler:Awake( )
    self.flipBtn = self.transform:Find("flipBtn").gameObject
    self.boxCollider = self.pivot.transform:GetComponent(typeof(BoxCollider))
    self.rawHeight = CExpressionMgr.TxtHeight
    self.DefaultSize=2000--160
    self.MaxSize = 800
    self.MaxDiagonalLength = Mathf.Floor(self.MaxSize / 2 * Mathf.Sqrt(2))
    self.MinSize = 100
    self.MinDiagonalLength = Mathf.Floor(self.MinSize / 2 * Mathf.Sqrt(2))

    UIEventListener.Get(self.confirmBtn).onClick = DelegateFactory.VoidDelegate(function ()
        self:OnClickConfirmBtn() 
    end)
    UIEventListener.Get(self.cancleBtn).onClick = DelegateFactory.VoidDelegate(function ()
        self:OnClickCancleBtn() 
    end)
    UIEventListener.Get(self.rotateBtn).onPress = DelegateFactory.BoolDelegate(function (go, state)
        self:OnPressRotateBtn(go,state)
    end)
    UIEventListener.Get(self.pivot).onPress = DelegateFactory.BoolDelegate(function (go,state)
        self:OnPressPivot(go,state) 
    end)
    UIEventListener.Get(self.flipBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnClickFlipBtn(go)
    end)
end

function CLuaEditImageHandler:SetHandlerIndex(index)
    self.Index = index
end

function CLuaEditImageHandler:GetHandlerIndex()
    return self.Index
end

function CLuaEditImageHandler:OnClickConfirmBtn() 
    if self.onConfirm ~= nil then
        invoke(self.onConfirm)
    end
    self.gameObject:SetActive(false)
end

function CLuaEditImageHandler:OnClickCancleBtn() 
    if self.onCancle ~= nil then
        invoke(self.onCancle)
    end
    self.gameObject:SetActive(false)
end

function CLuaEditImageHandler:OnPressRotateBtn(go,state)
    self.startEdit = state
end

function CLuaEditImageHandler:OnPressPivot(go,state)
    self.startMove = state
end

function CLuaEditImageHandler:OnClickFlipBtn(go)
    local scaleX = self.relatedWidget.transform.localScale.x
    self.relatedWidget.transform.localScale = Vector3(-scaleX,1,1)
end

function CLuaEditImageHandler:InitTxtParams(widget,expression_txt_offset_x,expression_txt_offset_y,expression_txt_scale_x,expression_txt_scale_y,expression_txt_rotation) 
    self.relatedWidget = widget
    self.rawWidth = CExpressionMgr.TxtWidth
    self.rawHeight = CExpressionMgr.TxtHeight
    self.relatedWidget.transform.localPosition = Vector3(expression_txt_offset_x, expression_txt_offset_y)

    if expression_txt_scale_x == 0 then
        expression_txt_scale_x = 100
    end
    self.relatedWidget.width = math.floor((CExpressionMgr.TxtWidth * (expression_txt_scale_x / 100)))
    self.relatedWidget.height = math.floor((CExpressionMgr.TxtHeight * (expression_txt_scale_x / 100)))
    self.relatedWidget.transform.localEulerAngles = Vector3(0, 0, expression_txt_rotation)

    self:UpdateWidget()
end
function CLuaEditImageHandler:InitStickerParams(widget,sticker_offset_x,sticker_offset_y,sticker_scale_x,sticker_scale_y,sticker_rotation) 
    self.relatedWidget = widget
    self.rawWidth = CExpressionMgr.StickerSize
    self.rawHeight = CExpressionMgr.StickerSize
    self.relatedWidget.transform.localPosition = Vector3(sticker_offset_x,sticker_offset_y)

    if sticker_scale_x == 0 then
        sticker_scale_x = 100
    end
    self.relatedWidget.width = math.floor((CExpressionMgr.StickerSize * (sticker_scale_x / 100)))
    self.relatedWidget.height = math.floor((CExpressionMgr.StickerSize * (sticker_scale_y / 100)))
    self.relatedWidget.transform.localEulerAngles = Vector3(0, 0, sticker_rotation)

    self:UpdateWidget()
end

function CLuaEditImageHandler:SetRelatedWidget(widget) 
    if widget == nil then
        self.relatedWidget = nil
        return
    end
    self.relatedWidget = widget
    self:UpdateWidget()
end

function CLuaEditImageHandler:UpdateRotate(line,fromVec,toVec) 
    if (CommonDefs.op_Subtraction_Vector3_Vector3(toVec, fromVec)).x > 0 then
        local angle = Vector3.Angle(CommonDefs.op_Subtraction_Vector3_Vector3(toVec, fromVec), Vector3(0, - 1))
        line.localEulerAngles = Vector3(0, 0, angle)
    else
        local angle = Vector3.Angle(CommonDefs.op_Subtraction_Vector3_Vector3(toVec, fromVec), Vector3(0, - 1))
        line.localEulerAngles = Vector3(0, 0, - angle)
    end
end

function CLuaEditImageHandler:SetEditTxt()
    self.rawWidth = CExpressionMgr.TxtWidth
    self.rawHeight = CExpressionMgr.TxtHeight
end

function CLuaEditImageHandler:SetEditSticker()
    self.rawWidth = CExpressionMgr.StickerSize
    self.rawHeight = CExpressionMgr.StickerSize
end

function CLuaEditImageHandler:GetRelatedWidget()
    return self.relatedWidget
end

function CLuaEditImageHandler:UpdateWidget()
    self.pivot.transform.position = self.relatedWidget.transform.position
    local pos = self.pivot.transform.localPosition
    pos.z = 0
    self.pivot.transform.localPosition = pos
    self.pivot.transform.localEulerAngles = self.relatedWidget.transform.localEulerAngles
    --restrict
    local maxLength = Mathf.Max(self.relatedWidget.width, self.relatedWidget.height)
    if maxLength > self.DefaultSize then
        local ratio = self.DefaultSize / maxLength
        self.relatedWidget.width =  math.floor((self.relatedWidget.width * ratio))
        self.relatedWidget.height =  math.floor((self.relatedWidget.height * ratio))
    end

    --self.bottomRightPos = Vector3(pos.x + maxLength / 2, pos.y - maxLength / 2)
    local width = self.relatedWidget.width
    local height = self.relatedWidget.height
    self.bottomRightPos = Vector3(pos.x + width / 2, pos.y - height / 2)
    local topRightPos = Vector3(pos.x + width / 2, pos.y + height / 2)
    self.vec1 = Vector3(pos.x - self.bottomRightPos.x,pos.y - self.bottomRightPos.y,0)
    self.vec2 = Vector3(pos.x - topRightPos.x,pos.y - topRightPos.y,0)
    self.angle1 = Vector3.Angle(self.vec1,self.vec2)
    self.angle2 = 180-self.angle1
    self:Rotate(self.pivot.transform.localEulerAngles.z)
end
function CLuaEditImageHandler:Update()
    if self.startEdit then
        --TODO
        --最大放大缩小比例，修改MaxSize MaxDiagonalLength MinSize MinDiagonalLength
        local posTo = CUIManager.instance.MainCamera:ScreenToWorldPoint(Input.mousePosition)
        local relLocalPos = self.pivot.transform.parent:InverseTransformPoint(posTo)
        relLocalPos.z = 0
        local pivotPos=self.pivot.transform.localPosition
        local dis = Vector3.Distance(relLocalPos, self.pivot.transform.localPosition)

        if dis > self.MaxDiagonalLength then
            local x = pivotPos.x - self.MaxDiagonalLength / dis * (pivotPos.x - relLocalPos.x)--
            local y = pivotPos.y - self.MaxDiagonalLength / dis * (pivotPos.y - relLocalPos.y)
            relLocalPos.x = x
            relLocalPos.y = y
        elseif dis < self.MinDiagonalLength then
            local x = pivotPos.x - self.MinDiagonalLength / dis * (pivotPos.x - relLocalPos.x)--
            local y = pivotPos.y - self.MinDiagonalLength / dis * (pivotPos.y - relLocalPos.y)
            relLocalPos.x = x
            relLocalPos.y = y
        end
        self.rotateBtn.transform.localPosition = relLocalPos
        self.bottomRightPos = relLocalPos

        self:UpdatePosition()
    end

    if self.startMove then
        local prePos = self.pivot.transform.localPosition
        local posTo = CUIManager.instance.MainCamera:ScreenToWorldPoint(Input.mousePosition)

        local corners = self.portrait.worldCorners
        posTo.x = Mathf.Clamp(posTo.x, corners[0].x, corners[2].x)
        posTo.y = Mathf.Clamp(posTo.y, corners[0].y, corners[2].y)

        local relLocalPos = self.pivot.transform.parent:InverseTransformPoint(posTo)
        relLocalPos.z = 0
        local offset = Vector3(relLocalPos.x-prePos.x,relLocalPos.y-prePos.y,relLocalPos.z-prePos.z)

        self.bottomRightPos = Vector3(self.bottomRightPos.x+offset.x,self.bottomRightPos.y+offset.y,self.bottomRightPos.z+offset.z)
        prePos = relLocalPos
        self.topRightPos = self:GetRotate90Pos(self.bottomRightPos, relLocalPos,self.angle1)
        self.topLeftPos = self:GetRotate90Pos(self.topRightPos, relLocalPos,self.angle2)
        self.bottomLeftPos = self:GetRotate90Pos(self.topLeftPos, relLocalPos,self.angle1)

        local maxX,minX,maxY,minY = 0,0,0,0
        maxX = Mathf.Max(self.bottomLeftPos.x, self.bottomRightPos.x)
        maxX = Mathf.Max(maxX, self.topRightPos.x)
        maxX = Mathf.Max(maxX, self.topLeftPos.x)
        minX = Mathf.Min(self.bottomLeftPos.x, self.bottomRightPos.x)
        minX = Mathf.Min(minX, self.topRightPos.x)
        minX = Mathf.Min(minX, self.topLeftPos.x)
        maxY = Mathf.Max(self.bottomLeftPos.y, self.bottomRightPos.y)
        maxY = Mathf.Max(maxY, self.topRightPos.y)
        maxY = Mathf.Max(maxY, self.topLeftPos.y)
        minY = Mathf.Min(self.bottomLeftPos.y, self.bottomRightPos.y)
        minY = Mathf.Min(minY, self.topRightPos.y)
        minY = Mathf.Min(minY, self.topLeftPos.y)
     
        local LeftX = self.pivot.transform.parent:InverseTransformPoint(corners[0]).x
        local RightX = self.pivot.transform.parent:InverseTransformPoint(corners[2]).x
        local TopY = self.pivot.transform.parent:InverseTransformPoint(corners[2]).y
        local BottomY = self.pivot.transform.parent:InverseTransformPoint(corners[0]).y
        if (minX < LeftX) then
            relLocalPos = Vector3(relLocalPos.x + (LeftX - minX),relLocalPos.y,relLocalPos.z)
        end
        if (maxX > RightX) then
            relLocalPos = Vector3(relLocalPos.x - (maxX - RightX),relLocalPos.y,relLocalPos.z)
        end
        if (minY < BottomY) then
            relLocalPos = Vector3(relLocalPos.x,relLocalPos.y + (BottomY - minY),relLocalPos.z)
        end
        if (maxY > TopY) then
            relLocalPos = Vector3(relLocalPos.x,relLocalPos.y - (maxY - TopY),relLocalPos.z)
        end
        offset = Vector3(relLocalPos.x-prePos.x,relLocalPos.y-prePos.y,relLocalPos.z-prePos.z)
        self.bottomRightPos = Vector3(self.bottomRightPos.x+offset.x,self.bottomRightPos.y+offset.y,self.bottomRightPos.z+offset.z)
        ---------------------
        self.pivot.transform.localPosition = relLocalPos
        self:UpdatePosition()
    end
end

function CLuaEditImageHandler:GetRotate90Pos(rawPos,pivotPos,angle)
    local rtn = Vector3(0,0,0)
    rtn = self:GetRotatePos(rawPos,pivotPos,angle)
    return rtn
end

function CLuaEditImageHandler:UpdatePosition()
    self.rotateBtn.transform.localPosition = self.bottomRightPos

    local pivotPos = self.pivot.transform.localPosition
    self.topRightPos = self:GetRotate90Pos(self.bottomRightPos, pivotPos,self.angle1)
    self.topLeftPos = self:GetRotate90Pos(self.topRightPos, pivotPos,self.angle2)
    self.bottomLeftPos = self:GetRotate90Pos(self.topLeftPos, pivotPos,self.angle1)

    self.cancleBtn.transform.localPosition = self.topRightPos
    self.confirmBtn.transform.localPosition = self.topLeftPos
    self.flipBtn.transform.localPosition = self.bottomLeftPos

    local lengthWidth = Mathf.Floor(Vector3.Distance(self.bottomRightPos, self.bottomLeftPos))
    local lengthHeight = Mathf.Floor(Vector3.Distance(self.bottomRightPos, self.topRightPos))
    self.boxCollider.size = Vector3(lengthWidth, lengthHeight)
    self.lineRight.transform.localPosition = Vector3((self.topRightPos.x+self.bottomRightPos.x)/2,(self.topRightPos.y+self.bottomRightPos.y)/2,0)
    self.lineLeft.transform.localPosition = Vector3((self.topLeftPos.x+self.bottomLeftPos.x)/2,(self.topLeftPos.y+self.bottomLeftPos.y)/2,0)
    self.lineTop.transform.localPosition = Vector3((self.topRightPos.x+self.topLeftPos.x)/2,(self.topRightPos.y+self.topLeftPos.y)/2,0)
    self.lineBottom.transform.localPosition = Vector3((self.bottomLeftPos.x+self.bottomRightPos.x)/2,(self.bottomLeftPos.y+self.bottomRightPos.y)/2,0)

    self.lineRight.height = lengthHeight
    self.lineLeft.height = lengthHeight
    self.lineTop.height = lengthWidth
    self.lineBottom.height = lengthWidth

    self:UpdateRotate(self.lineBottom.transform, self.bottomRightPos, self.bottomLeftPos)
    self:UpdateRotate(self.lineTop.transform, self.topLeftPos, self.topRightPos)
    self:UpdateRotate(self.lineRight.transform, self.bottomRightPos, self.topRightPos)
    self:UpdateRotate(self.lineLeft.transform, self.topLeftPos, self.bottomLeftPos)

    self:UpdateRotate(self.pivot.transform, self.bottomRightPos, self.topRightPos)

    if self.relatedWidget then
        self.relatedWidget.transform.position = self.pivot.transform.position
        local pos = self.relatedWidget.transform.localPosition
        pos.z = 0
        self.relatedWidget.transform.localPosition = pos
        self.relatedWidget.transform.localEulerAngles = Vector3(0, 0, self.pivot.transform.localEulerAngles.z + 180)
        local val1=lengthWidth / self.rawWidth
        local val2=lengthHeight / self.rawHeight
        self.relatedWidget.width = Mathf.Floor((self.rawWidth* val1))
        self.relatedWidget.height = Mathf.Floor(self.rawHeight* val2)
    end

end

function CLuaEditImageHandler:Rotate(angle)
    self.bottomRightPos = self:GetRotatePos(self.bottomRightPos, self.pivot.transform.localPosition, angle)
    self:UpdatePosition()
end

function CLuaEditImageHandler:GetRotatePos(rawPos,pivotPos,angle)
    local val = angle / 180 * 3.14
    local cosVal = Mathf.Cos(val)
    local sinVal = Mathf.Sin(val)
    local rtn = Vector3(0,0,0)
    rtn.x = (rawPos.x - pivotPos.x) * cosVal - (rawPos.y - pivotPos.y) * sinVal + pivotPos.x
    rtn.y = (rawPos.x - pivotPos.x) * sinVal + (rawPos.y - pivotPos.y) * cosVal + pivotPos.y
    return rtn
end
