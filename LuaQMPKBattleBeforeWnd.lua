local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local CChatMgr = import "L10.Game.CChatMgr"
local CChatHelper = import "L10.UI.CChatHelper"
local Animation = import "UnityEngine.Animation"
local CQuanMinPKMgr=import "L10.Game.CQuanMinPKMgr"
local CGameVideoMgr = import "L10.Game.CGameVideoMgr"
local CCommonUITween = import "L10.UI.CCommonUITween"
LuaQMPKBattleBeforeWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQMPKBattleBeforeWnd, "Center", "Center", GameObject)
RegistChildComponent(LuaQMPKBattleBeforeWnd, "Left", "Left", GameObject)
RegistChildComponent(LuaQMPKBattleBeforeWnd, "Right", "Right", GameObject)
RegistChildComponent(LuaQMPKBattleBeforeWnd, "ChatNode", "ChatNode", GameObject)
RegistChildComponent(LuaQMPKBattleBeforeWnd, "SelectPlayerPanel", "SelectPlayerPanel", GameObject)
RegistChildComponent(LuaQMPKBattleBeforeWnd, "ChatBtn", "ChatBtn", CButton)
RegistChildComponent(LuaQMPKBattleBeforeWnd, "ChatLabel", "ChatLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaQMPKBattleBeforeWnd, "m_CloseBtn")
RegistClassMember(LuaQMPKBattleBeforeWnd, "m_JingCaiBtn")

RegistClassMember(LuaQMPKBattleBeforeWnd, "m_CenterScript")
RegistClassMember(LuaQMPKBattleBeforeWnd, "m_LeftScript")
RegistClassMember(LuaQMPKBattleBeforeWnd, "m_RightScript")
RegistClassMember(LuaQMPKBattleBeforeWnd, "m_SelectPanelScript")
RegistClassMember(LuaQMPKBattleBeforeWnd, "m_DataInfo")
RegistClassMember(LuaQMPKBattleBeforeWnd, "m_CurForceBpStatus")
RegistClassMember(LuaQMPKBattleBeforeWnd, "m_CurStatus")
RegistClassMember(LuaQMPKBattleBeforeWnd, "m_LeftIsAttack")
RegistClassMember(LuaQMPKBattleBeforeWnd, "m_IsOpenPickBanPanel")

RegistClassMember(LuaQMPKBattleBeforeWnd, "m_CurSelectMemberList")
RegistClassMember(LuaQMPKBattleBeforeWnd, "m_OnChatMsgReceive")
RegistClassMember(LuaQMPKBattleBeforeWnd, "m_ChatBubble")
RegistClassMember(LuaQMPKBattleBeforeWnd, "m_Bubble")
RegistClassMember(LuaQMPKBattleBeforeWnd, "m_ChatLabel")
RegistClassMember(LuaQMPKBattleBeforeWnd, "m_ChatBubbleTick")
RegistClassMember(LuaQMPKBattleBeforeWnd, "m_Ani")
RegistClassMember(LuaQMPKBattleBeforeWnd, "m_HasEnterPrepare")
RegistClassMember(LuaQMPKBattleBeforeWnd, "m_FxTick")
RegistClassMember(LuaQMPKBattleBeforeWnd, "m_LastBpStatus")
function LuaQMPKBattleBeforeWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.ChatBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChatBtnClick()
	end)


    --@endregion EventBind end
	self.m_CloseBtn = self.transform:Find("Bg/CloseButton").gameObject
	self.m_CloseBtn.gameObject:SetActive(true)
	self.m_Ani = self.transform:GetComponent(typeof(Animation))
	UIEventListener.Get(self.m_CloseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseBtnClick()
	end)
	self.m_FxTick = nil
	self.m_ChatBubbleTick = nil
	self.m_DataInfo = nil
	self.m_CurStatus = nil
	self.m_CurSelectMemberList = nil
	self.m_LeftIsAttack = false
	self.m_IsOpenPickBanPanel = false
	self.m_CurForceBpStatus = nil
	self.m_LastBpStatus = EnumQmpkBanPickStatus.ePickBegin
	self.m_HasEnterPrepare = false
	self.m_ChatBubble = self.ChatNode.transform:Find("bubble").gameObject
	self.m_Bubble = self.ChatNode.transform:Find("bubble/ChatBubble"):GetComponent(typeof(UIWidget))
	self.m_ChatLabel = self.ChatNode.transform:Find("bubble/ChatLabel"):GetComponent(typeof(UILabel))
	self.m_JingCaiBtn = self.transform:Find("Anchor/GambleButton").gameObject
	self.m_JingCaiBtn.gameObject:SetActive(false)
	self.m_ChatBubble.gameObject:SetActive(false)
	self.m_ChatLabel.gameObject:SetActive(false)
	self.m_OnChatMsgReceive = DelegateFactory.Action_uint(function(channelDef)
        self:OnChatMsgReceive()
    end)
	self.SelectPlayerPanel.gameObject:SetActive(true)
end

function LuaQMPKBattleBeforeWnd:Init()
	self.m_CenterScript = self.Center.gameObject:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
	self.m_LeftScript = self.Left.gameObject:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
	self.m_RightScript = self.Right.gameObject:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
	self.m_SelectPanelScript = self.SelectPlayerPanel.gameObject:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
	self.SelectPlayerPanel.gameObject:SetActive(false)
	self:UpdateView()
	
end

function LuaQMPKBattleBeforeWnd:UpdateView()
	if not CLuaQMPKMgr.m_QmpkBanPickInfo then return end
	self.m_DataInfo = CLuaQMPKMgr.m_QmpkBanPickInfo
	self.m_LeftIsAttack = self.m_DataInfo.force == EnumCommonForce_lua.eAttack
	if 	self.m_DataInfo.attackZhanDui.BPStatus == EnumQmpkBanPickStatus.eAllFinish and 
		self.m_DataInfo.defendZhanDui.BPStatus == EnumQmpkBanPickStatus.eAllFinish then	-- 选人结束，展示准备阶段状态
		self.m_CurStatus = EnumQMPKWndBPStatus.eShowStatus	
	else
		self.m_CurStatus = EnumQMPKWndBPStatus.eBPStatus
	end
	self.m_CloseBtn.gameObject:SetActive(self.m_DataInfo.m_position == EnumQMPKBPPlayerPosition.eOther)
	if CQuanMinPKMgr.Inst.IsInQuanMinPKWatch and self.m_DataInfo.m_position == EnumQMPKBPPlayerPosition.eOther then
        if CLuaQMPKMgr.m_PlayStage == EnumQmpkPlayStage.eJiFenSai
        or CLuaQMPKMgr.m_PlayStage == EnumQmpkPlayStage.eTaoTaiSai
        or CLuaQMPKMgr.m_PlayStage == EnumQmpkPlayStage.eZongJueSai then
            if CGameVideoMgr.Inst:IsLiveMode() then
                self.m_JingCaiBtn:SetActive(true)
                UIEventListener.Get(self.m_JingCaiBtn).onClick=DelegateFactory.VoidDelegate(function(go)
                    CLuaQMPKMgr.m_PlayIndex=0
                    CUIManager.ShowUI(CLuaUIResources.QMPKJingCaiContentWnd)
                end)
            end
        end
    end

	if self.m_CurStatus == EnumQMPKWndBPStatus.eShowStatus then
		CUIManager.CloseUI(CLuaUIResources.QMPKTeamMemberDetailWnd)
	end	
	
	self:ShowMemberView()
	local needShowPanel = false
	local curBpStatus = nil
	-- 判断是否队长并且未结束选人状态
	if self.m_DataInfo.m_position == EnumQMPKBPPlayerPosition.eTeamLeader then
		if self.m_DataInfo.force == EnumCommonForce_lua.eAttack then
			if self.m_DataInfo.attackZhanDui.BPStatus == EnumQmpkBanPickStatus.ePickBegin then
				needShowPanel = self.m_LastBpStatus == EnumQmpkBanPickStatus.ePickBegin
				self.m_LastBpStatus = EnumQmpkBanPickStatus.ePickBegin
				if not self.m_IsOpenPickBanPanel or self.m_CurForceBpStatus ~= self.m_DataInfo.attackZhanDui.BPStatus then 
					self.m_CurForceBpStatus = self.m_DataInfo.attackZhanDui.BPStatus
					Gac2Gas.QueryQmpkBiWuZhanDuiMemberForBanPick(self.m_DataInfo.force) 
				end	
			elseif self.m_DataInfo.attackZhanDui.BPStatus == EnumQmpkBanPickStatus.eBanBegin then
				needShowPanel = true
				self.m_LastBpStatus = EnumQmpkBanPickStatus.eBanBegin
				if not self.m_IsOpenPickBanPanel or self.m_CurForceBpStatus ~= self.m_DataInfo.attackZhanDui.BPStatus then
					self.m_CurForceBpStatus = self.m_DataInfo.attackZhanDui.BPStatus
					Gac2Gas.QueryQmpkBiWuZhanDuiMemberForBanPick(EnumCommonForce_lua.eDefend) 
				end	
			end
		elseif self.m_DataInfo.force == EnumCommonForce_lua.eDefend then
			if self.m_DataInfo.defendZhanDui.BPStatus == EnumQmpkBanPickStatus.ePickBegin then
				needShowPanel = self.m_LastBpStatus == EnumQmpkBanPickStatus.ePickBegin
				self.m_LastBpStatus = EnumQmpkBanPickStatus.ePickBegin
				if not self.m_IsOpenPickBanPanel or self.m_CurForceBpStatus ~= self.m_DataInfo.defendZhanDui.BPStatus then
					self.m_CurForceBpStatus = self.m_DataInfo.defendZhanDui.BPStatus
					Gac2Gas.QueryQmpkBiWuZhanDuiMemberForBanPick(self.m_DataInfo.force)
				end
			elseif self.m_DataInfo.defendZhanDui.BPStatus == EnumQmpkBanPickStatus.eBanBegin then
				needShowPanel = true
				self.m_LastBpStatus = EnumQmpkBanPickStatus.eBanBegin
				if not self.m_IsOpenPickBanPanel or self.m_CurForceBpStatus ~= self.m_DataInfo.defendZhanDui.BPStatus then
					self.m_CurForceBpStatus = self.m_DataInfo.defendZhanDui.BPStatus
					Gac2Gas.QueryQmpkBiWuZhanDuiMemberForBanPick(EnumCommonForce_lua.eAttack)
				end
			end
		end
	end
	if not needShowPanel then
		self:CloseBPPlane()
	end
	self:ShowCenterView()
end

function LuaQMPKBattleBeforeWnd:ShowCenterView()
	if self.m_CurStatus == EnumQMPKWndBPStatus.eShowStatus then	-- 选人结束，展示准备阶段状态
		if self.m_Ani and not self.m_HasEnterPrepare then
			self.m_HasEnterPrepare = true
			if self.m_Ani:IsPlaying("qmpkbattlebeforewnd_down") then
				if self.m_FxTick then UnRegisterTick(self.m_FxTick) self.m_FxTick = nil end
				self.m_FxTick = RegisterTickOnce(function()
					self.m_Ani:Play("qmpkbattlebeforewnd_countdown_1")
					self.m_CenterScript:InitPrepareStatus(self.m_DataInfo.curTime,self.m_DataInfo.endTime)
				end,150)
				return
			else
				self.m_Ani:Play("qmpkbattlebeforewnd_countdown_1")
			end
		end
		if self.m_DataInfo.round == 5 then
			self.m_CenterScript:SetPrepareVisible(false)
		end
	self.m_CenterScript:InitPrepareStatus(self.m_DataInfo.curTime,self.m_DataInfo.endTime)	
	elseif self.m_CurStatus == EnumQMPKWndBPStatus.eBPStatus then
		self.m_HasEnterPrepare = false
		local score1,score2 = 0,0
		local isleftAttack = self.m_LeftIsAttack or self.m_DataInfo.m_position == EnumQMPKBPPlayerPosition.eOther
		if isleftAttack then 
			score1 = self.m_DataInfo.attackZhanDui.WinCount
			score2 = self.m_DataInfo.defendZhanDui.WinCount
		else
			score2 = self.m_DataInfo.attackZhanDui.WinCount
			score1 = self.m_DataInfo.defendZhanDui.WinCount
		end
		local maxTime = QuanMinPK_Setting.GetData().PickStageCountDownDuration	-- pick阶段
		if self.m_DataInfo.attackZhanDui.BPStatus == EnumQmpkBanPickStatus.eBanBegin or 
		self.m_DataInfo.defendZhanDui.BPStatus == EnumQmpkBanPickStatus.eBanBegin then	-- 有一方处于ban阶段，即判断为ban阶段
			maxTime = QuanMinPK_Setting.GetData().BanStageCountDownDuration
		end
		self.m_CenterScript:InitBanPickCenterInfo(isleftAttack,score1,score2,self.m_DataInfo.round,
		self.m_DataInfo.curTime,self.m_DataInfo.endTime,maxTime,self.m_DataInfo.winForce)
	end
end

function LuaQMPKBattleBeforeWnd:ShowMemberView()
	if self.m_LeftIsAttack or self.m_DataInfo.m_position == EnumQMPKBPPlayerPosition.eOther  then 
		self.m_LeftScript:InitPlayerForce(EnumCommonForce_lua.eAttack,self.m_DataInfo.attackZhanDui.Name,self.m_DataInfo.force,true,self.m_DataInfo.m_position == EnumQMPKBPPlayerPosition.eTeamLeader)
		self.m_RightScript:InitPlayerForce(EnumCommonForce_lua.eDefend,self.m_DataInfo.defendZhanDui.Name,self.m_DataInfo.force,false,self.m_DataInfo.m_position == EnumQMPKBPPlayerPosition.eTeamLeader)
		self.m_LeftScript:UpdateMember(self.m_DataInfo.attackZhanDui.BPStatus,self.m_DataInfo.force,self.m_DataInfo.round,self.m_DataInfo.attackZhanDui.Member,self.m_DataInfo.defendZhanDui.Ban)
		self.m_RightScript:UpdateMember(self.m_DataInfo.defendZhanDui.BPStatus,self.m_DataInfo.force,self.m_DataInfo.round,self.m_DataInfo.defendZhanDui.Member,self.m_DataInfo.attackZhanDui.Ban)
	else
		self.m_RightScript:InitPlayerForce(EnumCommonForce_lua.eAttack,self.m_DataInfo.attackZhanDui.Name,self.m_DataInfo.force,false,self.m_DataInfo.m_position == EnumQMPKBPPlayerPosition.eTeamLeader)
		self.m_LeftScript:InitPlayerForce(EnumCommonForce_lua.eDefend,self.m_DataInfo.defendZhanDui.Name,self.m_DataInfo.force,true,self.m_DataInfo.m_position == EnumQMPKBPPlayerPosition.eTeamLeader)
		self.m_RightScript:UpdateMember(self.m_DataInfo.attackZhanDui.BPStatus,self.m_DataInfo.force,self.m_DataInfo.round,self.m_DataInfo.attackZhanDui.Member,self.m_DataInfo.defendZhanDui.Ban)
		self.m_LeftScript:UpdateMember(self.m_DataInfo.defendZhanDui.BPStatus,self.m_DataInfo.force,self.m_DataInfo.round,self.m_DataInfo.defendZhanDui.Member,self.m_DataInfo.attackZhanDui.Ban)
	end

	-- if self.m_IsOpenPickBanPanel and self.m_DataInfo.m_position == EnumQMPKBPPlayerPosition.eTeamLeader then
	-- 	local memberList = self.m_DataInfo.attackZhanDui.Member
	-- 	local ban = self.m_DataInfo.attackZhanDui.Ban
	-- 	if self.m_DataInfo.force == EnumCommonForce_lua.eDefend then
	-- 		memberList = self.m_DataInfo.defendZhanDui.Member
	-- 		ban = self.m_DataInfo.defendZhanDui.Ban
	-- 	end
	-- 	g_ScriptEvent:BroadcastInLua("QMPKUpdateMemberSelelct",memberList,ban)
	-- end
end

function LuaQMPKBattleBeforeWnd:ShowBPPanel()
	if not CLuaQMPKMgr.m_QmpkSelectBanPickMemberInfo then return end
	if not self.m_IsOpenPickBanPanel then
		self:DoSelectPanelAni(true)
	end
	self.m_IsOpenPickBanPanel = true
	local memberInfo = CLuaQMPKMgr.m_QmpkSelectBanPickMemberInfo
	local isBan = (self.m_DataInfo.force == EnumCommonForce_lua.eAttack and self.m_DataInfo.attackZhanDui.BPStatus == EnumQmpkBanPickStatus.eBanBegin) or
	(self.m_DataInfo.force == EnumCommonForce_lua.eDefend and self.m_DataInfo.defendZhanDui.BPStatus == EnumQmpkBanPickStatus.eBanBegin) 
	local memberForce = self.m_DataInfo.force

	if self.m_DataInfo.force == EnumCommonForce_lua.eAttack and isBan then memberForce = EnumCommonForce_lua.eDefend
	elseif self.m_DataInfo.force == EnumCommonForce_lua.eDefend and isBan then memberForce = EnumCommonForce_lua.eAttack end

	self.m_CurSelectMemberList = self:GetSelectMemberList(self.m_DataInfo.round,memberInfo.memberData,memberInfo.roundMember,isBan, memberForce)

	self.m_SelectPanelScript:ShowWnd(self.m_DataInfo.round,isBan,self.m_DataInfo.force,self.m_CurSelectMemberList)

end

function LuaQMPKBattleBeforeWnd:CloseBPPlane()
	if not self.m_IsOpenPickBanPanel then return end
	self.m_IsOpenPickBanPanel = false
	self:DoSelectPanelAni(false)
	self.m_SelectPanelScript:CloseWnd()
end

function LuaQMPKBattleBeforeWnd:DoSelectPanelAni(isOpen)
	if self.m_Ani then 
		if self.m_FxTick then UnRegisterTick(self.m_FxTick) self.m_FxTick = nil end
		if self.m_Ani:IsPlaying("qmpkbattlebeforewnd_show") then
			self.m_FxTick = RegisterTickOnce(function()
				if isOpen then
					self.m_Ani:Play("qmpkbattlebeforewnd_up")
				else
					self.m_Ani:Play("qmpkbattlebeforewnd_down")
				end
			end,1000)
		elseif self.m_Ani:IsPlaying("qmpkbattlebeforewnd_up") or self.m_Ani:IsPlaying("qmpkbattlebeforewnd_down") then
			self.m_FxTick = RegisterTickOnce(function()
				if isOpen then
					self.m_Ani:Play("qmpkbattlebeforewnd_up")
				else
					self.m_Ani:Play("qmpkbattlebeforewnd_down")
				end
			end,140)
		else
			if isOpen then
				self.m_Ani:Play("qmpkbattlebeforewnd_up")
			else
				self.m_Ani:Play("qmpkbattlebeforewnd_down")
			end
		end
	end
	--self.SelectPlayerPanel.gameObject:SetActive(isOpen)
end

function LuaQMPKBattleBeforeWnd:GetSelectMemberList(round,allMember,roundMember,isBan,memberForce)
	local memberList = {}
	local memberIdStatus = {}
	for k,v in pairs(allMember) do
		memberIdStatus[v.Id] = EnumQmpkSelectBanPickStatus.eCanSelect
	end
	if round == 2 then 	-- 第二局，只能从第一局出战角色中选
		for k,v in pairs(memberIdStatus) do
			if roundMember[1][k] then
				memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eCanSelect
			else
				memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eRound1Ban
			end
		end
	elseif round == 3 then -- 第三局，只能从第一局出战角色中选，第二局出战角色不可选
		for k,v in pairs(memberIdStatus) do
			if roundMember[2][k] then 
				memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eRound2Ban
			elseif roundMember[1][k] then
				memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eCanSelect
			else
				memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eRound1Ban
			end
		end
	elseif  round == 4 then
		if isBan then	-- ban操作
			for k,v in pairs(memberIdStatus) do
				if not roundMember[3][k] then		-- 只能禁用第三局出战角色
					memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eRound3Ban
				else
					memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eCanSelect
				end
			end
		else
			for k,v in pairs(memberIdStatus) do
				if roundMember[1][k] and not roundMember[2][k] and not roundMember[3][k] then	-- 第一局出战，第二局第三局未出战角色强制上场
					memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eRound4Pick
				elseif roundMember[2][k] then 
					memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eRound2Ban
				else
					if self.m_DataInfo.attackZhanDui.Ban and self.m_DataInfo.attackZhanDui.Ban.Id == k or
					self.m_DataInfo.defendZhanDui.Ban and self.m_DataInfo.defendZhanDui.Ban.Id == k then	-- 被禁用角色
						memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eBeBan
					elseif roundMember[1][k] then 
						memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eCanSelect
					else
						memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eRound1Ban
					end
				end
			end
		end
	elseif round == 5 then	-- 第五局只能选第一局出战角色
		for k,v in pairs(memberIdStatus) do
			if roundMember[1][k] then
				memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eRound5Pick
			else
				memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eRound1Ban
			end
		end
	end

	for k,v in pairs(allMember) do
		table.insert(memberList,{
			Id = v.Id,
			Name = v.Name,
			Class = v.Class,
			Status = memberIdStatus[v.Id],
			Force = memberForce,
		})
	end
	table.sort(memberList,function(a,b)
		return tonumber(a.Status) > tonumber(b.Status)
	end)
	return memberList
end

function LuaQMPKBattleBeforeWnd:OnChatMsgReceive()
	if self.m_DataInfo.m_position == EnumQMPKBPPlayerPosition.eTeamMember or self.m_DataInfo.m_position == EnumQMPKBPPlayerPosition.eTeamLeader then
		local chatMsg = CChatMgr.Inst:GetLatestMsg()
 		if chatMsg then
        	local channel =  CChatHelper.GetChannelByName(chatMsg.channelName)
        	if channel == EChatPanel.Quanmin or channel == EChatPanel.Current  then -- 战队
            	if string.sub(chatMsg.message, 1, string.len("voice://")) == "voice://" then -- 屏蔽语音消息
            	    return
            	end
            	if string.sub(chatMsg.message, 1, string.len("<link")) == "<link" then -- 屏蔽文本链接
            	    return
            	end
            	self:OnShowChatBubble(chatMsg,chatMsg.message)
			end
        end
	end
end

function LuaQMPKBattleBeforeWnd:OnShowChatBubble(chatMsgInfo,content)
	local showTime = QuanMinPK_Setting.GetData().ShowChatMsgTime
	self.m_ChatBubble.gameObject:SetActive(true)
	self.m_ChatLabel.gameObject:SetActive(true)
	self:GenerateBubbleText(chatMsgInfo,content)

	if self.m_ChatBubbleTick then UnRegisterTick(self.m_ChatBubbleTick) self.m_ChatBubbleTick = nil end
	self.m_ChatBubbleTick = RegisterTickOnce(function()
		self.m_ChatBubble.transform:GetComponent(typeof(CCommonUITween)):PlayDisapperAnimation(DelegateFactory.Action(function()
            self.m_ChatBubble.gameObject:SetActive(false)
        end))
	end,showTime * 1000)

end

function LuaQMPKBattleBeforeWnd:GenerateBubbleText(chatMsgInfo,content)
	local length = CUICommonDef.GetStrByteLength(content)
	local maxlength = QuanMinPK_Setting.GetData().ShowChatMsgMaxLength
	local resStr = SafeStringFormat3("[b792ff][%s][-]:[D9D9D9]%s[-]",chatMsgInfo.fromUserName,content)
	self.m_ChatLabel:UpdateNGUIText()
	NGUIText.regionWidth = 1000000
	local size = NGUIText.CalculatePrintedSize(resStr)
	local width = size.x > 490 and 490 or math.ceil(size.x)
	self.m_Bubble.width = width + 46
	self.m_ChatLabel.text = resStr
	local default, str = self.m_ChatLabel:Wrap(resStr)

	if not default then
		resStr = CommonDefs.StringSubstring2(str, 0, CommonDefs.StringLength(str) - 1)
		self.m_ChatLabel.text = resStr .. "..."
	end

end

--@region UIEvent

function LuaQMPKBattleBeforeWnd:OnChatBtnClick()
	if not self.m_DataInfo then return end
	CSocialWndMgr.ShowChatWnd(EChatPanel.Current)
end

--@endregion UIEvent

function LuaQMPKBattleBeforeWnd:OnCloseBtnClick()
	g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("QMPK_LeaveGameVideo_Confirm"), function ()
		Gac2Gas.RequestLeavePlay()
	end, nil, nil, nil, false)
end

function LuaQMPKBattleBeforeWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncQmpkBanPickInfo",self,"UpdateView")
	g_ScriptEvent:AddListener("ReplyQmpkQueryZhanDuiMemberForBanPick",self,"ShowBPPanel")
	EventManager.AddListenerInternal(EnumEventType.RecvChatMsg, self.m_OnChatMsgReceive)
end

function LuaQMPKBattleBeforeWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncQmpkBanPickInfo",self,"UpdateView")
	g_ScriptEvent:RemoveListener("ReplyQmpkQueryZhanDuiMemberForBanPick",self,"ShowBPPanel")
	EventManager.RemoveListenerInternal(EnumEventType.RecvChatMsg, self.m_OnChatMsgReceive)
	if self.m_ChatBubbleTick then UnRegisterTick(self.m_ChatBubbleTick) self.m_ChatBubbleTick = nil end
	if self.m_FxTick then UnRegisterTick(self.m_FxTick) self.m_FxTick = nil end
end