local QnButton = import "L10.UI.QnButton"

local CUIFx = import "L10.UI.CUIFx"

local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local Animation = import "UnityEngine.Animation"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local Utility = import "L10.Engine.Utility"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CPos = import "L10.Engine.CPos"
local UITexture = import "UITexture"
local BoxCollider = import "UnityEngine.BoxCollider"

LuaHuLuWaPage = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHuLuWaPage, "OpenningTimeLabel", "OpenningTimeLabel", UILabel)
RegistChildComponent(LuaHuLuWaPage, "YLSJActivityBtn", "YLSJActivityBtn", QnButton)
RegistChildComponent(LuaHuLuWaPage, "YCRYDActivityBtn", "YCRYDActivityBtn", QnButton)
RegistChildComponent(LuaHuLuWaPage, "QDRYActivityBtn", "QDRYActivityBtn", QnButton)
RegistChildComponent(LuaHuLuWaPage, "JBTYActivityBtn", "JBTYActivityBtn", QnButton)
RegistChildComponent(LuaHuLuWaPage, "MXBSActivityBtn", "MXBSActivityBtn", QnButton)
RegistChildComponent(LuaHuLuWaPage, "YAZMActivityBtn", "YAZMActivityBtn", QnButton)
RegistChildComponent(LuaHuLuWaPage, "MyHuLuWaBtn", "MyHuLuWaBtn", GameObject)
RegistChildComponent(LuaHuLuWaPage, "ScoreMallBtn", "ScoreMallBtn", GameObject)
RegistChildComponent(LuaHuLuWaPage, "JYBWZActivityBtn", "JYBWZActivityBtn", QnButton)
RegistChildComponent(LuaHuLuWaPage, "YLSJFxNode", "YLSJFxNode", CUIFx)
RegistChildComponent(LuaHuLuWaPage, "JBTYFxNode", "JBTYFxNode", CUIFx)
RegistChildComponent(LuaHuLuWaPage, "JYBWFxNode", "JYBWFxNode", CUIFx)
RegistChildComponent(LuaHuLuWaPage, "JYBWFxNode2", "JYBWFxNode2", CUIFx)
RegistChildComponent(LuaHuLuWaPage, "MXBSFxNode", "MXBSFxNode", CUIFx)
RegistChildComponent(LuaHuLuWaPage, "YAZMFxNode", "YAZMFxNode", CUIFx)
RegistChildComponent(LuaHuLuWaPage, "MyScoreLabl", "MyScoreLabl", UILabel)
RegistChildComponent(LuaHuLuWaPage, "YCRYDFxNode", "YCRYDFxNode", CUIFx)
RegistChildComponent(LuaHuLuWaPage, "QDRYFxNode", "QDRYFxNode", CUIFx)
RegistChildComponent(LuaHuLuWaPage, "YLSJFxNode2", "YLSJFxNode2", CUIFx)

--@endregion RegistChildComponent end

function LuaHuLuWaPage:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.YLSJActivityBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnYLSJActivityBtnClick()
	end)


	
	UIEventListener.Get(self.YCRYDActivityBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnYCRYDActivityBtnClick()
	end)


	
	UIEventListener.Get(self.QDRYActivityBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnQDRYActivityBtnClick()
	end)


	
	UIEventListener.Get(self.JBTYActivityBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJBTYActivityBtnClick()
	end)


	
	UIEventListener.Get(self.MXBSActivityBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMXBSActivityBtnClick()
	end)


	
	UIEventListener.Get(self.YAZMActivityBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnYAZMActivityBtnClick()
	end)


	
	UIEventListener.Get(self.MyHuLuWaBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMyHuLuWaBtnClick()
	end)


	
	UIEventListener.Get(self.ScoreMallBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnScoreMallBtnClick()
	end)


	
	UIEventListener.Get(self.JYBWZActivityBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJYBWZActivityBtnClick()
	end)


    --@endregion EventBind end
end

function LuaHuLuWaPage:Init()
    local bgTexture = self.transform:Find("BgTexture"):GetComponent(typeof(UITexture))
    local newBox = CommonDefs.AddComponent_GameObject_Type(bgTexture.gameObject, typeof(BoxCollider))
    newBox.size = Vector3(bgTexture.width, bgTexture.height, 0)   
    
    if CClientMainPlayer.Inst then
        local huiLiuData = CClientMainPlayer.Inst.PlayProp.HuiGuiJieBanData
        local jiebanLabel = self.JBTYActivityBtn.transform:Find("JoinTimes"):GetComponent(typeof(UILabel))
        if huiLiuData and huiLiuData.JieBanPlayerId and huiLiuData.JieBanPlayerId > 0 then
            jiebanLabel.text = "1/1"
        else
            jiebanLabel.text = "0/1"
        end
    end

    local setting = HuluBrothers_Setting.GetData()
    local opentime = setting.ActivityOpenLastTime 
    self.OpenningTimeLabel.text = opentime
    --巧夺如意
    local ruYiOpenWeekDay = setting.RuYiOpenWeekDay
    self.m_IsOpenQDRY = self:GetActivityState(ruYiOpenWeekDay,self.QDRYActivityBtn.transform)
    if self.m_IsOpenQDRY then
        self.QDRYFxNode:LoadFx("fx/ui/prefab/UI_huluwa_huangchuansongmen.prefab")
    elseif LuaHuLuWa2022Mgr.m_IsRuYiServerSwitchOn then--试投放
        self.QDRYFxNode:LoadFx("fx/ui/prefab/UI_huluwa_huangchuansongmen.prefab")
        self.m_IsOpenQDRY = true
        local bg = self.QDRYActivityBtn.transform:Find("Bg").transform
        if bg then
            bg.localPosition = Vector3(0,0,0)
        end
        self.QDRYActivityBtn.transform:Find("Left/CloseTime"):GetComponent(typeof(UILabel)).text = LocalString.StrH2V(LocalString.GetString("开放中"),true)
    end

    --三界历险
    local adventureOpenDay = setting.AdventureOpenDay
    self.m_IsOpenYLSJ = self:GetActivityState(adventureOpenDay,self.YLSJActivityBtn.transform)
    if self.m_IsOpenYLSJ then
        self.YLSJFxNode:LoadFx("fx/ui/prefab/UI_huluwa_02.prefab")--01 02
        self.YLSJFxNode2:LoadFx("fx/ui/prefab/UI_huluwa_chuiyan.prefab")
    else
        self.YLSJFxNode:LoadFx("fx/ui/prefab/UI_huluwa_01.prefab")--01 02
        self.YLSJFxNode.OnLoadFxFinish = DelegateFactory.Action(function()            
            local animation = CommonDefs.GetComponentInChildren_GameObject_Type(self.JBTYFxNode.transform.gameObject, typeof(Animation))
            if animation then
                animation.enabled = false
            end
        end)
    end
    
    --结伴同游
    local huiLiuOpenDay = setting.HuiLiuOpenDay
    self.m_IsOpenJBTY = self:GetActivityState(huiLiuOpenDay,self.JBTYActivityBtn.transform)
    if self.m_IsOpenJBTY then
        self.JBTYFxNode:LoadFx("fx/ui/prefab/UI_huluwa_04.prefab")--03 04
    else
        self.JBTYFxNode:LoadFx("fx/ui/prefab/UI_huluwa_03.prefab")--03 04
        self.JBTYFxNode.OnLoadFxFinish = DelegateFactory.Action(function()            
            local animation = CommonDefs.GetComponentInChildren_GameObject_Type(self.JBTYFxNode.transform.gameObject, typeof(Animation))
            if animation then
                animation.enabled = false
            end
        end)
    end
    
    --魔性变身
    local transformOpenDay = setting.TransformOpenDay
    self.m_IsOpenMXBS = self:GetActivityState(transformOpenDay,self.MXBSActivityBtn.transform)
    if self.m_IsOpenMXBS then
        self.MXBSFxNode:LoadFx("fx/ui/prefab/UI_huluwa_05.prefab")--06 05
    else
        self.MXBSFxNode:LoadFx("fx/ui/prefab/UI_huluwa_06.prefab")--06 05
        self.MXBSFxNode.OnLoadFxFinish = DelegateFactory.Action(function()            
            local animation = CommonDefs.GetComponentInChildren_GameObject_Type(self.MXBSFxNode.transform.gameObject, typeof(Animation))
            if animation then
                animation.enabled = false
            end
        end)
    end
    
    --家园保卫
    local homeDefendOpenDay = setting.HomeDefendOpenDay
    self.m_IsOpenJYBW = self:GetActivityState(homeDefendOpenDay,self.JYBWZActivityBtn.transform)
    if self.m_IsOpenJYBW then
        self.JYBWFxNode:LoadFx("fx/ui/prefab/UI_huluwa_07.prefab")
        self.JYBWFxNode2:LoadFx("fx/ui/prefab/UI_huluwa_08.prefab")
    else
        self.JYBWFxNode:LoadFx("fx/ui/prefab/UI_huluwa_07.prefab")
        self.JYBWFxNode2:DestroyFx()
        self.JYBWFxNode.OnLoadFxFinish = DelegateFactory.Action(function()            
            local animation = CommonDefs.GetComponentInChildren_GameObject_Type(self.JYBWFxNode.transform.gameObject, typeof(Animation))
            if animation then
                animation.enabled = false
            end
            local huoyan = FindChild(self.JYBWFxNode.transform,"huoyan")
            huoyan.gameObject:SetActive(false)
       end)
    end
    
    --勇闖如意洞
    local huluBrotherOpenWeekDay = setting.HuluBrotherOpenWeekDay
    self.m_IsOpenYCRYD = self:GetActivityState(huluBrotherOpenWeekDay,self.YCRYDActivityBtn.transform)
    if self.m_IsOpenYCRYD then
        self.YCRYDFxNode:LoadFx("fx/ui/prefab/UI_huluwa_lvchuansongmen.prefab")
    elseif LuaHuLuWa2022Mgr.m_IsHuLuWaServerSwitchOn then--试投放
        self.YCRYDFxNode:LoadFx("fx/ui/prefab/UI_huluwa_lvchuansongmen.prefab")
        self.m_IsOpenYCRYD = true
        local bg = self.YCRYDActivityBtn.transform:Find("Bg").transform
        if bg then
            bg.localPosition = Vector3(0,0,0)
        end
        self.YCRYDActivityBtn.transform:Find("Left/CloseTime"):GetComponent(typeof(UILabel)).text = LocalString.StrH2V(LocalString.GetString("开放中"),true)
    end

    --以愛之名
    local flowerRankOpenDay = setting.FlowerRankOpenDay
    local year,month,day,hour,min,ehour,emin = string.match(flowerRankOpenDay, "(%d+)/(%d+)/(%d+),(%d+):(%d+)-(%d+):(%d+)")
    local sdate = CreateFromClass(DateTime, year, month, day, hour, min, 0)
    local edate = CreateFromClass(DateTime, year, month, day, ehour, emin, 0)
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local bg = self.YAZMActivityBtn.transform:Find("Bg").transform
    local closeTime = self.YAZMActivityBtn.transform:Find("Left/CloseTime"):GetComponent(typeof(UILabel))
    local redTree = self.transform:Find("RedTree")
    
    self.YAZMFxNode:DestroyFx()
    redTree.gameObject:SetActive(false)
    self.m_IsOpenYAZM = false
    if DateTime.Compare(now, sdate) >= 0 and DateTime.Compare(now, edate) <= 0 then
        --進行中
        bg.localPosition = Vector3(0,0,0)
        closeTime.text = LocalString.StrH2V(LocalString.GetString("5月20日限定"),true)
        self.YAZMFxNode:LoadFx("fx/ui/prefab/UI_huluwa_09.prefab")
        redTree.gameObject:SetActive(true)
        self.m_IsOpenYAZM = true
    elseif DateTime.Compare(now, sdate) < 0  then
        --还没开始
        bg.localPosition = Vector3(0,0, -1)
        closeTime.text = LocalString.StrH2V(LocalString.GetString("5月20日限定"),true)
    else
        --结束了
        bg.localPosition = Vector3(0,0, -1)
        closeTime.text = LocalString.StrH2V(LocalString.GetString("已结束"),true)
    end

    --积分
    self:OnMainPlayerPlayPropUpdate()
end

function LuaHuLuWaPage:GetActivityState(str,item)
    local sweek, eweek,shour,smin,ehour,emin = string.match(str, "(%d+)-(%d+),(%d+):(%d+)-(%d+):(%d+)")
    sweek = tonumber(sweek)
    eweek = tonumber(eweek)
    shour = tonumber(shour)
    ehour = tonumber(ehour)
    smin = tonumber(smin)
    emin = tonumber(emin)
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local dayofweek = EnumToInt(now.DayOfWeek)--0,1,2,3,4,5,6
    local closeTime = item:Find("Left/CloseTime"):GetComponent(typeof(UILabel))
    if dayofweek == 0 then
        dayofweek = 7
    end
    local isInWeek,isInTime = false,false
    local weekstr = SafeStringFormat3(LocalString.GetString("周%s至周%s"),sweek==7 and LocalString.GetString("日") or CUICommonDef.IntToChinese(sweek),eweek==7 and LocalString.GetString("日") or CUICommonDef.IntToChinese(eweek)) 
    
    if sweek == 0 and eweek == 0 then
        isInWeek = true
        weekstr = LocalString.GetString("每天")    
    elseif dayofweek <= eweek and dayofweek >= sweek then
        isInWeek = true
    else
        isInWeek = false
    end
    if item == self.YAZMActivityBtn.transform then
        closeTime.text = LocalString.StrH2V(LocalString.GetString("5月20日限定"),true) 
    elseif item == self.JYBWZActivityBtn.transform then
        closeTime.text = LocalString.StrH2V(LocalString.GetString("每天12点开启"),true) 
    else
        closeTime.text = LocalString.StrH2V(weekstr,true) 
    end
    
    if isInWeek then
        local minDate =  CreateFromClass(DateTime, now.Year, now.Month, now.Day, shour, smin, 0)
        local esec = 0
        if ehour==23 and emin == 59 then
            esec = 59
        end
        local maxDate =  CreateFromClass(DateTime, now.Year, now.Month, now.Day, ehour, emin,esec)
        if DateTime.Compare(now, minDate) >= 0 and DateTime.Compare(now, maxDate) <= 0 then
            --开放中
            isInTime = true
        else
            isInTime = false
        end
    end
    local isActivity = (isInWeek and isInTime)

    local bg = item:Find("Bg").transform
    if bg then
        bg.localPosition = Vector3(0,0,isActivity and 0 or -1)
    end

    return isActivity
end

function LuaHuLuWaPage:OnEnable()
    g_ScriptEvent:AddListener("SyncHuluwaActivityAwardTimes", self, "OnSyncHuluwaActivityAwardTimes")
    g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate", self, "OnMainPlayerPlayPropUpdate")
    Gac2Gas.QueryHuluwaActivityAwardTimes()
end

function LuaHuLuWaPage:OnDisable()
    g_ScriptEvent:RemoveListener("SyncHuluwaActivityAwardTimes", self, "OnSyncHuluwaActivityAwardTimes")  
    g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate", self, "OnMainPlayerPlayPropUpdate")
end

function LuaHuLuWaPage:OnMainPlayerPlayPropUpdate()
    if CClientMainPlayer.Inst then
        local score = 0
        local info = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayScore, EnumTempPlayScoreKey_lua.ZhanLingScore)
        if info then
            if info.ExpiredTime and info.ExpiredTime < CServerTimeMgr.Inst.timeStamp then
                score = 0 
            else 
                score = info.Score
            end
        end
        self.MyScoreLabl.text = SafeStringFormat3(LocalString.GetString("积分%d"),score)
    end
end

function LuaHuLuWaPage:OnSyncHuluwaActivityAwardTimes(list)
    --list的三项目前分别对应游历三界,勇闯如意洞和巧夺如意的次数
    local rewardCount = {0,0,0}
    if list then
        for i=0,list.Count-1,1 do
            rewardCount[i+1] = list[i]
        end
    end
    local maxCount = HuluBrothers_Setting.GetData().RewardTimes
    self:Init()
    self.YLSJActivityBtn.transform:Find("JoinTimes"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("%d/%d",rewardCount[1],maxCount[0])
    self.YCRYDActivityBtn.transform:Find("JoinTimes"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("%d/%d",rewardCount[2],maxCount[1])
    self.QDRYActivityBtn.transform:Find("JoinTimes"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("%d/%d",rewardCount[3],maxCount[2])
end

--@region UIEvent

function LuaHuLuWaPage:OnYLSJActivityBtnClick()
    if not self.m_IsOpenYLSJ then
        g_MessageMgr:ShowMessage("Huluwa_Travel_Tip")
        return
    end
    LuaHuLuWa2022Mgr.OpenTravelWorldWnd()
end

function LuaHuLuWaPage:OnYCRYDActivityBtnClick()
    if not self.m_IsOpenYCRYD then
        g_MessageMgr:ShowMessage("YongChuangRuYiDong_Sign_Tip")
        return
    end
    LuaHuLuWa2022Mgr.OpenRuYiDongSignWnd()
end

function LuaHuLuWaPage:OnQDRYActivityBtnClick()
    local bHideBtn = false
    if not self.m_IsOpenQDRY then
        bHideBtn = true
    else
        bHideBtn = false
    end
    LuaHuLuWa2022Mgr.OpenQiaoDuoRuYiSignWnd(bHideBtn)
end

function LuaHuLuWaPage:OnJBTYActivityBtnClick()
    if not self.m_IsOpenJBTY then
        g_MessageMgr:ShowMessage("JBTY_Activity_Msg")
        return
    end
    LuaMessageTipMgr:ShowMessage(g_MessageMgr:FormatMessage("JBTY_Activity_Msg"), LocalString.GetString("参加"), function ( ... )
        Gac2Gas.RequestOpenHuiGuiJieBanWnd()
    end)
end

function LuaHuLuWaPage:OnJYBWZActivityBtnClick()
    if not self.m_IsOpenJYBW then
        g_MessageMgr:ShowMessage("JYBW_Activity_Msg")
        return
    end
    LuaMessageTipMgr:ShowMessage(g_MessageMgr:FormatMessage("JYBW_Activity_Msg"), LocalString.GetString("参加"), function ( ... )
        CClientHouseMgr.Inst:GoBackHome()
    end)
end

function LuaHuLuWaPage:OnMXBSActivityBtnClick()
    if not self.m_IsOpenMXBS then
        g_MessageMgr:ShowMessage("MXBS_Activity_Msg")
        return
    end
    LuaMessageTipMgr:ShowMessage(g_MessageMgr:FormatMessage("MXBS_Activity_Msg"), LocalString.GetString("参加"), function ( ... )
        local pixelPos = Utility.GridPos2PixelPos(CPos(30, 40))
        CTrackMgr.Inst:Track(nil, 16000002, pixelPos, 0, DelegateFactory.Action(function()
                
        end), nil, nil, nil, false)
    end)
end

function LuaHuLuWaPage:OnYAZMActivityBtnClick()
    if not self.m_IsOpenYAZM then
        g_MessageMgr:ShowMessage("YAZM_Activity_Msg")
        return
    end
    Gac2Gas.QueryYiAiZhiMingSendFlowerRank()
end

function LuaHuLuWaPage:OnMyHuLuWaBtnClick()
    local type = LuaHuLuWa2022Mgr.EnumPreviewType.ePreview
    LuaHuLuWa2022Mgr.m_CurPreviewType = type
    CUIManager.ShowUI(CLuaUIResources.HuLuWaBianShenPreviewWnd)
end

function LuaHuLuWaPage:OnScoreMallBtnClick()
    CLuaNPCShopInfoMgr.ShowScoreShopById(34000086)
end


--@endregion UIEvent

