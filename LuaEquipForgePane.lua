local CBaseEquipmentItem = import "L10.UI.CBaseEquipmentItem"
local Animator = import "UnityEngine.Animator"
local CPackageItemCell = import "L10.UI.CPackageItemCell"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UITable = import "UITable"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local UISlider = import "UISlider"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Extensions = import "Extensions"
local AlignType2 = import "L10.UI.CTooltip+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local LuaTweenUtils = import "LuaTweenUtils"
local Ease = import "DG.Tweening.Ease"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local COpenEntryMgr=import "L10.Game.COpenEntryMgr"
local CUIPickerWndMgr = import "L10.UI.CUIPickerWndMgr"
local CTooltip = import "L10.UI.CTooltip"

LuaEquipForgePane = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaEquipForgePane, "MainEquip", "MainEquip", CBaseEquipmentItem)
RegistChildComponent(LuaEquipForgePane, "CurrentLevelLabel", "CurrentLevelLabel", UILabel)
RegistChildComponent(LuaEquipForgePane, "MaxLevelLabel", "MaxLevelLabel", UILabel)
RegistChildComponent(LuaEquipForgePane, "FullLevelRoot", "FullLevelRoot", GameObject)
RegistChildComponent(LuaEquipForgePane, "FullLevelDescLabel", "FullLevelDescLabel", UILabel)
RegistChildComponent(LuaEquipForgePane, "UpgradeRoot", "UpgradeRoot", GameObject)
RegistChildComponent(LuaEquipForgePane, "Table", "Table", UITable)
RegistChildComponent(LuaEquipForgePane, "Progress", "Progress", GameObject)
RegistChildComponent(LuaEquipForgePane, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaEquipForgePane, "AddSubBtn", "AddSubBtn", QnAddSubAndInputButton)
RegistChildComponent(LuaEquipForgePane, "Cost", "Cost", CCurentMoneyCtrl)
RegistChildComponent(LuaEquipForgePane, "UseCountLabel", "UseCountLabel", UILabel)
RegistChildComponent(LuaEquipForgePane, "CurrentLvDescLabel", "CurrentLvDescLabel", UILabel)
RegistChildComponent(LuaEquipForgePane, "NextLvDescLabel", "NextLvDescLabel", UILabel)
RegistChildComponent(LuaEquipForgePane, "UpgradeBtn", "UpgradeBtn", GameObject)
RegistChildComponent(LuaEquipForgePane, "BreakoutRoot", "BreakoutRoot", GameObject)
RegistChildComponent(LuaEquipForgePane, "BreakoutItem", "BreakoutItem", GameObject)
RegistChildComponent(LuaEquipForgePane, "CurrentBreakLabel", "CurrentBreakLabel", UILabel)
RegistChildComponent(LuaEquipForgePane, "NextBreakLabel", "NextBreakLabel", UILabel)
RegistChildComponent(LuaEquipForgePane, "BreakoutBtn", "BreakoutBtn", GameObject)
RegistChildComponent(LuaEquipForgePane, "ResetBtn", "ResetBtn", GameObject)
RegistChildComponent(LuaEquipForgePane, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaEquipForgePane, "ProgressBar", "ProgressBar", UISlider)
RegistChildComponent(LuaEquipForgePane, "PretendBar", "PretendBar", UISlider)
RegistChildComponent(LuaEquipForgePane, "ProgressCountLabel", "ProgressCountLabel", UILabel)
RegistChildComponent(LuaEquipForgePane, "LockRoot", "LockRoot", GameObject)
RegistChildComponent(LuaEquipForgePane, "LockDescLabel", "LockDescLabel", UILabel)
RegistChildComponent(LuaEquipForgePane, "LockTipLabel", "LockTipLabel", UILabel)
RegistChildComponent(LuaEquipForgePane, "TargetLevelLabel", "TargetLevelLabel", UILabel)
RegistChildComponent(LuaEquipForgePane, "UpgradeLvBtn", "UpgradeLvBtn", GameObject)

--@endregion RegistChildComponent end

function LuaEquipForgePane:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.UpgradeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUpgradeBtnClick()
	end)


	
	UIEventListener.Get(self.BreakoutBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBreakoutBtnClick()
	end)


	
	UIEventListener.Get(self.ResetBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnResetBtnClick()
	end)


	
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)


	
	UIEventListener.Get(self.UpgradeLvBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUpgradeLvBtnClick()
	end)


    --@endregion EventBind end
end

-- 切换装备 升级后 等情况应该被调用
function LuaEquipForgePane:InitEquip()
	self:UnsetFx()

	self.Cost:SetCost(0)
	
	self.FullLevelRoot:SetActive(false)
	self.BreakoutRoot:SetActive(false)
	self.UpgradeRoot:SetActive(false)
	self.LockRoot:SetActive(false)

	local itemId = CLuaEquipMgr.m_ForgeItemId

	if itemId == nil or itemId == "" then
		-- 设置空显示
		self.MainEquip:UpdateData("")
		return	
	end

	-- 当前装备
	self.m_EquipItem = CItemMgr.Inst:GetById(itemId).Equip

	self.MainEquip:UpdateData(itemId)
	
	-- 装备当前等级与装备突破等级
	local forgeLv = self.m_EquipItem.ForgeLevel
	local forgeBreakLv = self.m_EquipItem.ForgeBreakLevel
	if forgeBreakLv == 0 then
		forgeBreakLv = ExtraEquip_Setting.GetData().InitialForgeBreakLevel
	end
	self.CurrentLevelLabel.text = SafeStringFormat3("Lv.%s[c][909090]/%s[/c]", tostring(forgeLv), tostring(forgeBreakLv))

	-- 当前和最大锻造等级
	self.m_CurForgeLv = forgeLv
	self.m_CurMaxForgeLv = forgeBreakLv
	
	Extensions.SetLocalRotationZ(self.UpgradeLvBtn.transform, 0)
	self.m_ListerningUpgradeLv = false
	CUIManager.CloseUI(CUIResources.PickerWnd)
	
	local setting = ExtraEquip_Setting.GetData()
	-- 判断锻造等级 分成三个状态：可升级，可突破，已满级
	if forgeLv == 40 then
		-- 满级
		-- 配表加个等级上限
		self.FullLevelRoot:SetActive(true)
		local curData = ExtraEquip_Forge.GetData(forgeLv)
		self.FullLevelDescLabel.text =  math.floor(curData.FightPropAddValue) .."%"

		-- 按钮置灰
		local fullLevelBtn = self.FullLevelRoot.transform:Find("FullLevelBtn").gameObject
		CUICommonDef.SetActive(fullLevelBtn, false, true)

	elseif forgeLv == setting.SealForgeLevel then
		-- 被封印了
		self.LockRoot:SetActive(true)
		local curData = ExtraEquip_Forge.GetData(forgeLv)
		self.LockDescLabel.text =  math.floor(curData.FightPropAddValue) .."%"

		self.LockTipLabel.text =  g_MessageMgr:FormatMessage("Equip_Forge_Level_Lock_Tip")
	elseif forgeLv == forgeBreakLv then
		-- 突破
		self.BreakoutRoot:SetActive(true)
		self:InitBreakout()
	else
		self.m_DonSetPretendBar = true
		-- 升级
		self.UpgradeRoot:SetActive(true)
		self:InitUpgrade()

		local total = self:GetItemSumValue()

		local oldPrecent = self.m_UpgradeValueCurrent/ self.m_UpgradeValueNeed
		local precent = (self.m_UpgradeValueCurrent + total) / self.m_UpgradeValueNeed
		if self.m_ProgressStartFxTween then
			LuaTweenUtils.Kill(self.m_ProgressStartFxTween, false)
		end
		self.m_ProgressStartFxTween = LuaTweenUtils.TweenFloat(oldPrecent, precent, 1, function ( val )
			if self.PretendBar then
				self.PretendBar.value = val
			end
		end)
		self.m_DonSetPretendBar = false
	end
end

-- 获取升级到某等级 总共需要的进度
-- 传入参数是当前等级
function LuaEquipForgePane:GetForgeCost(forgeLv)
	local alreadyCost = 0
	local curLvNeed = 0
	ExtraEquip_Forge.Foreach(function(id, data)
		if id < forgeLv then
			alreadyCost = alreadyCost + data.Range
		elseif id == forgeLv then
			curLvNeed = data.Range
		end
	end)

	return alreadyCost, curLvNeed
end

-- 获取升级到某等级 总共需要的进度
-- 传入参数是目标等级
function LuaEquipForgePane:GetTargetForgeCost(forgeLv, targetLv)
	local alreadyCost = 0
	local curLvNeed = 0
	ExtraEquip_Forge.Foreach(function(id, data)
		if id < forgeLv then
			alreadyCost = alreadyCost + data.Range
		elseif id >= forgeLv and id < targetLv then
			curLvNeed = curLvNeed + data.Range
		end
	end)

	return alreadyCost, curLvNeed
end

-- 获得当前物品可加进度
function LuaEquipForgePane:GetItemSumValue(exceptIndex)
	local sum = 0
	-- 需要花费银票的总数
	local costsum = 0
	local itemValue = ExtraEquip_Setting.GetData().ForgeItemValue
	local jingyuanId = ExtraEquip_Setting.GetData().DuanZaoJingYuanItemId

	for i=1, #self.m_UpgradeItemInfoList do
		if exceptIndex == nil or exceptIndex ~= i then
			local info = self.m_UpgradeItemInfoList[i]
			if info.select >0 then
				sum = sum + itemValue[info.itemId] * info.select

				-- 非锻造精元
				if info.itemId ~= jingyuanId then
					costsum = costsum + itemValue[info.itemId] * info.select
				end
			end
		end
    end
	return sum, costsum
end

function LuaEquipForgePane:OnSelect(args)
	local itemId = args[0]
	CLuaEquipMgr.m_ForgeItemId = itemId
	self:InitEquip(itemId)
end

-- 每次切换 或者更新物品的时候 调用
function LuaEquipForgePane:InitBreakout()
	local forgeLv = self.m_EquipItem.ForgeBreakLevel
	if forgeLv == 0 then
		forgeLv = ExtraEquip_Setting.GetData().InitialForgeBreakLevel
	end

	local curData = ExtraEquip_Forge.GetData(forgeLv)
	local breakCost = curData.BreakCost

	-- 等级上限
	self.CurrentBreakLabel.text = "lv." .. tostring(forgeLv)
	self.NextBreakLabel.text = "lv." .. tostring(forgeLv+5)

	local itemId = breakCost[0]
	local needCount = breakCost[1]

	local bindCount, notBindCount
	bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)

	local curCount = bindCount + notBindCount
	local itemData = Item_Item.GetData(itemId)

	-- 初始物品
	local icon = self.BreakoutItem.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local numLabel = self.BreakoutItem.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
	local getNode = self.BreakoutItem.transform:Find("GetNode").gameObject

	self.BreakoutItem.transform:Find("BindSprite").gameObject:SetActive(false)

	local enough = curCount >= needCount

	icon:LoadMaterial(itemData.Icon)
	getNode:SetActive(not enough)

	if enough then
		numLabel.text = SafeStringFormat3("%s/%s", tostring(curCount), tostring(needCount))
	else
		numLabel.text = SafeStringFormat3("[FFFFFF]%s[-]/%s", tostring(curCount), tostring(needCount))
	end

	-- 点击事件
	UIEventListener.Get(self.BreakoutItem).onClick = DelegateFactory.VoidDelegate(function (go)
		if not enough then
			-- 获取方式
			CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, false, nil, AlignType2.Right)
		else
			CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
		end
	end)
end

-- 每次切换 或者更新物品的时候 调用
function LuaEquipForgePane:InitUpgrade()
	local setting = ExtraEquip_Setting.GetData()

	-- 物品优先级排序
	-- itemId, 绑定数量，非绑定数量
	local itemIdList = {setting.DuanZaoJingYuanItemId, setting.HongBeiItemId, setting.ZiBeiItemId}
	local itemBindCount = {}
	local itemUnBindCount = {}
	-- 物品显示信息
	local itemInfoList = {}

	-- 一个物品映射
	local itemMapList = {}
	itemMapList[setting.DuanZaoJingYuanItemId] = {itemId = setting.DuanZaoJingYuan_Chest_Id, mapCount = setting.DuanZaoJingYuan_CountInChest}
	for k, v in pairs(itemMapList) do
		local itemId = v.itemId
		local bindCount, notBindCount
		bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateIdExclueExpireTime(itemId)

		v.bindCount = bindCount
		v.notBindCount = notBindCount
	end

	for i, itemId in ipairs(itemIdList) do
		local bindCount, notBindCount
		bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateIdExclueExpireTime(itemId)

		itemBindCount[i] = bindCount
		itemUnBindCount[i] = notBindCount
	end

	-- 优先绑定
	for i=1, 3 do
		local id = itemIdList[i]
		local bindCount = itemBindCount[i]
		local addCount = 0
		if itemMapList[id] then
			local mapInfo = itemMapList[id]
			addCount = mapInfo.mapCount * mapInfo.bindCount
		end

		if bindCount >0 or addCount > 0 then
			local info = {}
			info.isBind = true
			info.itemId = id

			info.count = bindCount + addCount
			info.realCount = bindCount

			info.select = 0
			table.insert(itemInfoList, info)
		end
	end

	-- 再是非绑
	for i=1, 3 do
		local id = itemIdList[i]
		local unbindCount = itemUnBindCount[i]

		local addCount = 0
		if itemMapList[id] then
			local mapInfo = itemMapList[id]
			addCount = mapInfo.mapCount * mapInfo.notBindCount
		end

		if unbindCount >0 or addCount > 0 then
			local info = {}
			info.isBind = false
			info.itemId = id

			info.count = unbindCount + addCount
			info.realCount = unbindCount
			info.select = 0
			table.insert(itemInfoList, info)
		end
	end

	-- 最后是没有物品了
	for i=1, 3 do
		local id = itemIdList[i]
		local unbindCount = itemUnBindCount[i]
		local bindCount = itemBindCount[i]
		if unbindCount == 0 and bindCount == 0 then
			local info = {}
			info.itemId = id
			info.isBind = false
			info.count = 0
			info.realCount = 0
			info.select = 0
			table.insert(itemInfoList, info)
		end
	end

	-- 设置显示
	self.m_UpgradeItemInfoList = itemInfoList

	-- 计算资源总能量
	local totalVal = 0
	for i=1, #self.m_UpgradeItemInfoList do
		local info = self.m_UpgradeItemInfoList[i]
		local itemValue = ExtraEquip_Setting.GetData().ForgeItemValue[info.itemId]
		totalVal = totalVal +  info.count * itemValue
	end

	-- 加上已经消耗的能量
	totalVal = totalVal + self.m_EquipItem.ForgeTotalValue
	self.m_MaxTargetForgeLevel = self.m_CurForgeLv + 1

	local playerLv = CClientMainPlayer.Inst.MaxLevel
	-- 计算最大能到几级
	for i = 0, self.m_CurMaxForgeLv-1 do
		local data = ExtraEquip_Forge.GetData(i)
		local lvData = ExtraEquip_Forge.GetData(i+1)

		local lvCheck = false
		if lvData then
			lvCheck = lvData.PlayerLevel <= playerLv
		end 

		if totalVal >= data.Range and lvCheck then
			self.m_MaxTargetForgeLevel = i + 1
			totalVal = totalVal - data.Range
		end
	end

	self.m_MaxTargetForgeLevel = math.min(math.max(self.m_MaxTargetForgeLevel, self.m_CurForgeLv + 1), self.m_CurMaxForgeLv)

	-- 计算一个目标等级
	if self.m_TargetForgeLevel == nil or self.m_TargetForgeLevel <= self.m_CurForgeLv or self.m_TargetForgeLevel > self.m_MaxTargetForgeLevel then
		-- 最少也得比当前多一级
		self.m_TargetForgeLevel = self.m_MaxTargetForgeLevel
	end

	-- 这部分可能需要移走
	-- 当前等级已拥有进度 和 需要的总进度
	local forgeValue = self.m_EquipItem.ForgeTotalValue
	local alreadyCost, curLvNeed = self:GetTargetForgeCost(self.m_CurForgeLv, self.m_TargetForgeLevel)
	self.m_UpgradeValueCurrent = forgeValue - alreadyCost
	self.m_UpgradeValueNeed = curLvNeed

	-- 设置初始化的显示
	-- 这里会计算使用数量
	local otherTotal = 0
	for i=1, #self.m_UpgradeItemInfoList do

		local info = self.m_UpgradeItemInfoList[i]

		local rest = self.m_UpgradeValueNeed - self.m_UpgradeValueCurrent - otherTotal
		local needCount = info.select
		local itemValue = ExtraEquip_Setting.GetData().ForgeItemValue[info.itemId]

		if rest >0 then
			needCount = math.min(info.count, math.ceil(rest / itemValue))
			info.select = needCount

			otherTotal = otherTotal + needCount * itemValue
		end
	end
	
	self:InitUpgradeItem()
	self:OnUpgradeUsage()

	self.m_UpgradeSelectIndex = -1
	local firstInfo = self.m_UpgradeItemInfoList[1]
	if firstInfo and firstInfo.count > 0 then
		self.m_SelectMark = true
		self.m_UpgradeSelectIndex = 1
		self:OnSelectUpgradeItem(firstInfo)
	end

	-- 显示属性提升相关
	local forgeLv = self.m_EquipItem.ForgeLevel
	local curData = ExtraEquip_Forge.GetData(forgeLv)
	local nextData = ExtraEquip_Forge.GetData(self.m_TargetForgeLevel)

	if curData then
		self.CurrentLvDescLabel.text = SafeStringFormat3("+%s%%", math.floor(curData.FightPropAddValue))
	else
		self.CurrentLvDescLabel.text = "+0%"
	end
	self.NextLvDescLabel.text = SafeStringFormat3("+%s%%", math.floor(nextData.FightPropAddValue))
	self.TargetLevelLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(self.m_TargetForgeLevel))
end

-- 显示升级消耗物品
-- 每次改变数量也会调用
function LuaEquipForgePane:InitUpgradeItem()
	Extensions.RemoveAllChildren(self.Table.transform)
   
	self.UseCountLabel.text = ""

	self.ItemTemplate:SetActive(false)

    for i=1, #self.m_UpgradeItemInfoList do
        local g = NGUITools.AddChild(self.Table.gameObject, self.ItemTemplate)
        g:SetActive(true)
        
		local info = self.m_UpgradeItemInfoList[i]

		local icon = g.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
		local bindSprite = g.transform:Find("BindSprite").gameObject
		local select = g.transform:Find("Select").gameObject
		local getNode = g.transform:Find("GetNode").gameObject
		local numLabel = g.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
		local selectCountLabel = g.transform:Find("Select/SelectCountLabel"):GetComponent(typeof(UILabel))
		local bg = g.transform:Find("Bg"):GetComponent(typeof(UISprite))
		local fx = g.transform:Find("fx_tuowei").gameObject

		fx:SetActive(false)

		bindSprite:SetActive(info.isBind)
		select:SetActive(info.select > 0)
		getNode:SetActive(info.count == 0)

		local itemData = Item_Item.GetData(info.itemId)
		icon:LoadMaterial(itemData.Icon)

		numLabel.text = tostring(info.count)
		selectCountLabel.text = tostring(info.select)

		if i== self.m_UpgradeSelectIndex then
			bg.spriteName = "common_btn_13_highlight"
		else
			bg.spriteName = "common_btn_13_normal"
		end

		UIEventListener.Get(g.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self.m_SelectMark = true
			self.m_UpgradeSelectIndex = i
			self:OnSelectUpgradeItem(info)
		end)
    end

    self.Table:Reposition()
end

-- 更新进度和使用信息
function LuaEquipForgePane:OnUpgradeUsage()
	-- 更新进度条
	local progressLabel = self.Progress.transform:Find("Label/Label"):GetComponent(typeof(UILabel))
	local total, costTotal = self:GetItemSumValue()

	self:UnsetFx()
	self.ProgressBar.gameObject:SetActive(self.m_UpgradeValueCurrent >0)
	self.ProgressBar.value = self.m_UpgradeValueCurrent / self.m_UpgradeValueNeed

	local precent = (self.m_UpgradeValueCurrent + total) / self.m_UpgradeValueNeed
	progressLabel.text = SafeStringFormat3("%d/%d", self.m_UpgradeValueCurrent + total, self.m_UpgradeValueNeed)

	if not self.m_DonSetPretendBar then
		if self.m_ProgressStartFxTween then
			LuaTweenUtils.Kill(self.m_ProgressStartFxTween, false)
		end
		
		if precent > 1 then
			self.PretendBar.value = 1
		else
			self.PretendBar.value = (self.m_UpgradeValueCurrent + total) / self.m_UpgradeValueNeed
		end
	end
	
	if total <= 0 then
		self.ProgressCountLabel.text = ""
	else
		self.ProgressCountLabel.text = "+" .. tostring(total)
	end
	

	local info = self.m_UpgradeItemInfoList[self.m_UpgradeSelectIndex]
	if info == nil then
		self.AddSubBtn.gameObject:SetActive(false)
		self.UseCountLabel.text = ""
	else
		-- 文字显示
		local itemData = Item_Item.GetData(info.itemId)
		if info.isBind then
			self.UseCountLabel.text = SafeStringFormat3(LocalString.GetString("[00FF00]%s(绑定)[-] 使用数量 %s"), itemData.Name, info.select)
		else
			self.UseCountLabel.text = SafeStringFormat3(LocalString.GetString("[00FF00]%s[-] 使用数量 %s"), itemData.Name, info.select)
		end

		local setting = ExtraEquip_Setting.GetData()
		-- 银两消耗
		self.Cost:SetCost(setting.ForgeSilverCostPerValue * costTotal)
	end
end

-- 点击事件
function LuaEquipForgePane:OnSelectUpgradeItem(info)
	if info.count == 0 then
		CItemAccessListMgr.Inst:ShowItemAccessInfo(info.itemId, false, nil, AlignType2.Right)
		self.AddSubBtn.gameObject:SetActive(false)
		self.UseCountLabel.text = ""
	else
		-- 设置消耗
		local itemValue = ExtraEquip_Setting.GetData().ForgeItemValue[info.itemId]
		local otherTotal = self:GetItemSumValue(self.m_UpgradeSelectIndex)
		local rest = self.m_UpgradeValueNeed - self.m_UpgradeValueCurrent - otherTotal

		local needCount = info.select
		if rest >0 then
			needCount = math.min(info.count, math.ceil(rest / itemValue))
			info.select = needCount
		end

		self.AddSubBtn.gameObject:SetActive(true)
		self.AddSubBtn.onValueChanged = DelegateFactory.Action_uint(function(val)
			-- 点击之后刷新显示
			info.select = val
			-- 如果已达最大值 提示
			if val == needCount and val ~= info.count and not self.m_SelectMark then
				g_MessageMgr:ShowMessage("Equip_Forge_Upgrade_Material_Already_Enough")
			end
			self.m_SelectMark = false
			self:InitUpgradeItem()
			self:OnUpgradeUsage()
		end)
		
		self.AddSubBtn:SetMinMax(0, needCount, 1)
		self.AddSubBtn:SetValue(info.select, true)
	end
end

function LuaEquipForgePane:OnForgeSuccess()
	-- 触发引导
	if not COpenEntryMgr.Inst:GetGuideRecord(207) then
		COpenEntryMgr.Inst:SaveGuidePhase(207, 0)
		CGuideMgr.Inst:StartGuide(207, 0)
	end

	UnRegisterTick(self.m_ForgeSuccessTick)
	self:PlayForgeFx()

	CUICommonDef.SetActive(self.UpgradeBtn.gameObject, false, true)
	self.m_ForgeSuccessTick = RegisterTickOnce(function()
		CUICommonDef.SetActive(self.UpgradeBtn.gameObject, true, true)
		self:InitEquip()
	end, 1500)
end

function LuaEquipForgePane:OnBreakSuccess()
	UnRegisterTick(self.m_BreakSuccessTick)
	self:PlayBreakFx()

	CUICommonDef.SetActive(self.BreakoutBtn.gameObject, false, true)
	self.m_BreakSuccessTick = RegisterTickOnce(function( )
		CUICommonDef.SetActive(self.BreakoutBtn.gameObject, true, true)
		self:InitEquip()
	end, 1500)
end

function LuaEquipForgePane:OnResetSuccess()
	self.m_ResetSuccessTick = RegisterTickOnce(function( )
		self:InitEquip()
	end, 1)
end

function LuaEquipForgePane:UnsetFx()
	local progressFx = self.UpgradeRoot.transform:Find("jindufankuizong").gameObject
	progressFx:SetActive(false)

	local progressBarFx = self.UpgradeRoot.transform:Find("Progress/ProgressBar/Panel/common_fx_jindukongzhi").gameObject
	progressBarFx:SetActive(true)

	local tupoFx = self.transform:Find("MainEquipBg_Vx").gameObject
	tupoFx:SetActive(false)

	local duanzaoFx = self.transform:Find("Fx_duanzao_baodianxiao").gameObject
	duanzaoFx:SetActive(false)

	local progressBarAni = self.ProgressBar.transform:GetComponent(typeof(Animator))
	progressBarAni:SetBool("m_IsOpen", false)

	self.m_FxPlaying = false
end

function LuaEquipForgePane:PlayForgeFx()
	self:UnsetFx()
	-- 进度条
	self.m_FxPlaying = true
	local progressLabel = self.Progress.transform:Find("Label/Label"):GetComponent(typeof(UILabel))
	local total = self:GetItemSumValue()

	local oldPrecent = self.m_UpgradeValueCurrent/ self.m_UpgradeValueNeed
	local precent = (self.m_UpgradeValueCurrent + total) / self.m_UpgradeValueNeed
	if precent > 1 then
		precent = 1
	end

	self.ProgressBar.gameObject:SetActive(true)
	self.ProgressBar.value = oldPrecent
	local point = self.ProgressBar.transform:TransformPoint(Vector3(660*precent, 0, 0))

	-- 进度条的高亮效果
	local progressFx = self.UpgradeRoot.transform:Find("jindufankuizong").gameObject
	progressFx:GetComponent(typeof(Animator)).enabled = true

	local bgFx = self.transform:Find("Fx_duanzao_baodianxiao").gameObject
	bgFx:SetActive(false)
	
	-- 进度条特效
	local progressBarFx = self.UpgradeRoot.transform:Find("Progress/ProgressBar/Panel/common_fx_jindukongzhi").gameObject
	progressBarFx:SetActive(false)
	-- 进度条特效动画
	local progressBarAni = self.ProgressBar.transform:GetComponent(typeof(Animator))
	progressBarAni:SetBool("m_IsOpen", false)

	UnRegisterTick(self.m_ProgressFxTick)
	self.m_ProgressFxTick = RegisterTickOnce(function()
		if progressFx and bgFx and progressBarFx then
			progressFx:SetActive(true)
			bgFx:SetActive(true)
			progressBarFx:SetActive(true)
			progressBarAni:SetBool("m_IsOpen", true)

			-- 进度条动画
			if self.m_ProgressFxTween then
				LuaTweenUtils.Kill(self.m_ProgressFxTween, false)
			end
			self.m_ProgressFxTween = LuaTweenUtils.TweenFloat(oldPrecent, precent, 1, function ( val )
				self.ProgressBar.value = val
				if val > precent-0.001 then
					progressBarAni:SetBool("m_IsOpen", false)
					progressFx:SetActive(false)
				end
			end)

			self.m_FxPlaying = false
		end
	end, 500)

	if self.m_TweenList then
		for i, tween in ipairs(self.m_TweenList) do
			LuaTweenUtils.Kill(tween, false)
		end
	end

	self.m_TweenList = {}
	for i=0, self.Table.transform.childCount-1 do
		local info = self.m_UpgradeItemInfoList[i+1]

		if info.select > 0 then
			local item = self.Table.transform:GetChild(i)
			local fx = item:Find("fx_tuowei"):GetComponent(typeof(Animator))

			fx.gameObject:SetActive(true)
			fx.enabled = true

			local moveItem = item:Find("fx_tuowei/common_fx_tuowei").gameObject
			local localTarget = moveItem.transform:InverseTransformPoint(point)
			local tween = LuaTweenUtils.TweenPosition(moveItem.transform, localTarget.x, localTarget.y, localTarget.z, 0.5)

			table.insert(self.m_TweenList, tween)
		end
	end
end

function LuaEquipForgePane:OnItemUpdate()
    if CLuaEquipMgr.m_ForgeItemId == nil or CLuaEquipMgr.m_ForgeItemId == "" or
        CItemMgr.Inst:GetById(CLuaEquipMgr.m_ForgeItemId) == nil then
        CLuaEquipMgr.m_ForgeItemId = ""
    end
	if CLuaEquipMgr.m_ForgeResetItemId == nil or CLuaEquipMgr.m_ForgeResetItemId == "" or
        CItemMgr.Inst:GetById(CLuaEquipMgr.m_ForgeResetItemId) == nil then
        CLuaEquipMgr.m_ForgeResetItemId = ""
    end

	if CUIManager.IsLoaded(CUIResources.YuanBaoMarketWnd) then
		self:InitEquip()
	end
end

function LuaEquipForgePane:PlayBreakFx()
	self:UnsetFx()

	self.m_FxPlaying = true
    local tupoFx = self.transform:Find("MainEquipBg_Vx").gameObject
	tupoFx:SetActive(true)

	local ani = self.transform:GetComponent(typeof(Animator))
	ani.enabled = true
	ani:Play("equipmentprocesswnd_baodianfenwei",0,0)

	UnRegisterTick(self.m_BreakFxTick)
	self.m_BreakFxTick = RegisterTickOnce(function()
		if tupoFx and ani then
			ani.enabled = false
			tupoFx:SetActive(false)
		end
		self.m_FxPlaying = false
	end, 1000)
end

function LuaEquipForgePane:OnPickerWndSelect(args)
	Extensions.SetLocalRotationZ(self.UpgradeLvBtn.transform, 0)
	if self.m_ListerningUpgradeLv then
		local index = args[0]
		self.m_ListerningUpgradeLv = false
		self.m_TargetForgeLevel = self.m_CurForgeLv + index + 1

		self:InitUpgrade()
	end
end

function LuaEquipForgePane:OnEnable()
	self:UnsetFx()
	g_ScriptEvent:AddListener("OnPickerItemSelected", self, "OnPickerWndSelect")
	g_ScriptEvent:AddListener("SendItem", self, "OnItemUpdate")
	g_ScriptEvent:AddListener("SetItemAt", self, "OnItemUpdate")
    g_ScriptEvent:AddListener("ForgeExtraEquipSuccess", self, "OnForgeSuccess")
	g_ScriptEvent:AddListener("ForgeBreakExtraEquipSuccess", self, "OnBreakSuccess")
	g_ScriptEvent:AddListener("ForgeResetExtraEquipSuccess", self, "OnResetSuccess")
end

function LuaEquipForgePane:OnDisable()
	UnRegisterTick(self.m_SendTick)
	UnRegisterTick(self.m_ProgressFxTick)
	UnRegisterTick(self.m_BreakFxTick)
	UnRegisterTick(self.m_BreakSuccessTick)
	UnRegisterTick(self.m_ForgeSuccessTick)
	UnRegisterTick(self.m_ResetSuccessTick)

	if self.m_ProgressFxTween then
		LuaTweenUtils.Kill(self.m_ProgressFxTween, false)
	end

	if self.m_ProgressStartFxTween then
		LuaTweenUtils.Kill(self.m_ProgressStartFxTween, false)
	end

	if self.m_TweenList then
		for i, tween in ipairs(self.m_TweenList) do
			LuaTweenUtils.Kill(tween, false)
		end
	end

	self.m_ShowFx = self.transform:GetComponent(typeof(Animator))
	self.m_ShowFx.enabled = false
	
	g_ScriptEvent:RemoveListener("OnPickerItemSelected", self, "OnPickerWndSelect")
	g_ScriptEvent:RemoveListener("SendItem", self, "OnItemUpdate")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "OnItemUpdate")
    g_ScriptEvent:RemoveListener("ForgeExtraEquipSuccess", self, "OnForgeSuccess")
    g_ScriptEvent:RemoveListener("ForgeBreakExtraEquipSuccess", self, "OnBreakSuccess")
    g_ScriptEvent:RemoveListener("ForgeResetExtraEquipSuccess", self, "OnResetSuccess")
end

--@region UIEvent

function LuaEquipForgePane:OnUpgradeBtnClick()
	local forgeLv = self.m_EquipItem.ForgeLevel
	local itemList = {}
	local itemDesc = ""
	local setting = ExtraEquip_Setting.GetData()

	for i, info in ipairs(self.m_UpgradeItemInfoList) do
		if info.count > 0 and info.select > 0 then
			local needExtra = 0
			local realCount = 0
			
			if info.select > info.realCount then
				needExtra = info.select - info.realCount
				realCount = info.realCount
			else
				realCount = info.select
			end

			-- 提交物品本身
			if realCount > 0 then
				table.insert(itemList, info.itemId)
				table.insert(itemList, realCount)
				table.insert(itemList, info.isBind and 1 or 0)
				table.insert(itemList, 0)
			end

			-- 处理特殊转换物品
			if needExtra > 0 then
				table.insert(itemList, setting.DuanZaoJingYuan_Chest_Id)
				table.insert(itemList, math.ceil(needExtra / setting.DuanZaoJingYuan_CountInChest))
				table.insert(itemList, info.isBind and 1 or 0)
				table.insert(itemList, needExtra)
			end

			local itemData = Item_Item.GetData(info.itemId)
			if itemDesc ~= "" then
				itemDesc = SafeStringFormat3(LocalString.GetString("、%s"), itemDesc)
			end

			if info.isBind then
				itemDesc = SafeStringFormat3(LocalString.GetString("%s个%s（绑定）%s"), info.select, itemData.Name, itemDesc)
			else
				itemDesc = SafeStringFormat3(LocalString.GetString("%s个%s%s"), info.select, itemData.Name, itemDesc)
			end
			
		end
	end

	local totalPoint = self:GetItemSumValue()
	local moneyNum = self.Cost:GetCost()

	local hasItem = #itemList > 0
	local moneyEnough = self.Cost.moneyEnough
	local canUpgrade = (self.m_UpgradeValueCurrent + totalPoint) >= self.m_UpgradeValueNeed
	local isBind = self.m_EquipItem.IsBinded

	local isQuick = (self.m_TargetForgeLevel - self.m_CurForgeLv) > 1

	if not self.m_FxPlaying then
		if not hasItem then
			g_MessageMgr:ShowMessage("Equip_Forge_Need_Choose_Material")
		elseif not moneyEnough then
			g_MessageMgr:ShowMessage("Equip_Forge_Money_Not_Enough")
		elseif not isQuick then
			-- 常规锻造
			local msg = ""

			if not isBind then
				msg = g_MessageMgr:FormatMessage("Equip_Forge_Confirm_For_Not_Bind", self.m_EquipItem.ColoredDisplayName)
			elseif canUpgrade then
				msg = g_MessageMgr:FormatMessage("Equip_Forge_Confirm_For_Upgrade", itemDesc, moneyNum, self.m_EquipItem.ColoredDisplayName, totalPoint, self.m_TargetForgeLevel)
			else
				msg = g_MessageMgr:FormatMessage("Equip_Forge_Confirm_For_Not_Upgrade", itemDesc, moneyNum, self.m_EquipItem.ColoredDisplayName, totalPoint)
			end
			
			g_MessageMgr:ShowOkCancelMessage(msg, function()
				local place, pos = CLuaEquipMgr:GetItemPlaceAndPos(CLuaEquipMgr.m_ForgeItemId)
				Gac2Gas.RequestForgeExtraEquip(place, pos, CLuaEquipMgr.m_ForgeItemId, g_MessagePack.pack(itemList))
			end, nil, nil, nil, false)
		elseif canUpgrade then
			-- 快速锻造 可以跨等级
			local msg = ""

			if not isBind then
				msg = g_MessageMgr:FormatMessage("Equip_Forge_Confirm_For_Not_Bind", self.m_EquipItem.ColoredDisplayName)
			else
				msg = g_MessageMgr:FormatMessage("Equip_Forge_Confirm_For_Upgrade", itemDesc, moneyNum, self.m_EquipItem.ColoredDisplayName, totalPoint, self.m_TargetForgeLevel)
			end

			g_MessageMgr:ShowOkCancelMessage(msg, function()
				local place, pos = CLuaEquipMgr:GetItemPlaceAndPos(CLuaEquipMgr.m_ForgeItemId)
				Gac2Gas.RequestQuickForgeExtraEquip(place, pos, CLuaEquipMgr.m_ForgeItemId, g_MessagePack.pack(itemList), self.m_TargetForgeLevel, true)
			end, nil, nil, nil, false)
		else
			-- 快速打造 但是到不了期待的等级
			g_MessageMgr:ShowMessage("Equip_Forge_Cant_Upgrade_To_TargetLv", self.m_TargetForgeLevel)
		end
	end
end

function LuaEquipForgePane:OnBreakoutBtnClick()
	local forgeLv = self.m_EquipItem.ForgeLevel
	local curData = ExtraEquip_Forge.GetData(forgeLv)
	local breakCost = curData.BreakCost

	local itemId = breakCost[0]
	local needCount = breakCost[1]

	local bindCount, notBindCount
	bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)

	local curCount = bindCount + notBindCount

	local itemData = Item_Item.GetData(itemId)

	if not self.m_FxPlaying then
		if curCount >= needCount then
			-- 请求突破
			local msg = g_MessageMgr:FormatMessage("Equip_Forge_Breakout_Confirm", needCount, itemData.Name, self.m_EquipItem.ColoredDisplayName, forgeLv, forgeLv+5)
			g_MessageMgr:ShowOkCancelMessage(msg, function()
				local place, pos = CLuaEquipMgr:GetItemPlaceAndPos(CLuaEquipMgr.m_ForgeItemId)
				Gac2Gas.RequestForgeBreakExtraEquip(place, pos, CLuaEquipMgr.m_ForgeItemId)
			end, nil, nil, nil, false)
		else
			g_MessageMgr:ShowMessage("Equip_Forge_Break_Item_Not_Enough", itemData.Name)
		end
	end
end

function LuaEquipForgePane:OnResetBtnClick()
	-- 请求重置
	local place, pos = CLuaEquipMgr:GetItemPlaceAndPos(CLuaEquipMgr.m_ForgeItemId)
	Gac2Gas.QueryForgeResetExtraEquipCostAndItems(place, pos, CLuaEquipMgr.m_ForgeItemId)
end

function LuaEquipForgePane:OnTipBtnClick()
	g_MessageMgr:ShowMessage("Equip_Forge_Wnd_Desc")
end

function LuaEquipForgePane:OnUpgradeLvBtnClick()
	self.m_ListerningUpgradeLv = true
	local contents = CreateFromClass(MakeGenericClass(List, String))

	for i = self.m_CurForgeLv + 1, self.m_MaxTargetForgeLevel do
		CommonDefs.ListAdd(contents, typeof(String), SafeStringFormat3(LocalString.GetString("锻造至%s级"), i))
	end

	local localPos = self.UpgradeLvBtn.transform.localPosition
	local anchor = self.UpgradeLvBtn.transform.parent:TransformPoint(localPos)

	local curIndex = self.m_TargetForgeLevel - self.m_CurForgeLv - 1
	if curIndex < 0 then
		curIndex = 0
	end

	CUIPickerWndMgr.ShowPickerWnd(contents, anchor, curIndex, CTooltip.AlignType.Default)
	Extensions.SetLocalRotationZ(self.UpgradeLvBtn.transform, 180)
end


--@endregion UIEvent

