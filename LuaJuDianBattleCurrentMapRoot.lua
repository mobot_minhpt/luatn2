local CUIFx = import "L10.UI.CUIFx"
local UIGrid = import "UIGrid"
local UICamera = import "UICamera"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UISprite = import "UISprite"
local QnExpandListBox = import "L10.UI.QnExpandListBox"
local CMiniMap = import "L10.UI.CMiniMap"
local CPos = import "L10.Engine.CPos"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DelegateFactory  = import "DelegateFactory"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CMiniMapPopWnd = import "L10.UI.CMiniMapPopWnd"
local Vector3 = import "UnityEngine.Vector3"
local BoxCollider = import "UnityEngine.BoxCollider"

LuaJuDianBattleCurrentMapRoot = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJuDianBattleCurrentMapRoot, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaJuDianBattleCurrentMapRoot, "FlagItem", "FlagItem", GameObject)
RegistChildComponent(LuaJuDianBattleCurrentMapRoot, "PointItem", "PointItem", GameObject)
RegistChildComponent(LuaJuDianBattleCurrentMapRoot, "NamelessItem", "NamelessItem", GameObject)
RegistChildComponent(LuaJuDianBattleCurrentMapRoot, "MarkRoot", "MarkRoot", GameObject)
RegistChildComponent(LuaJuDianBattleCurrentMapRoot, "MarkItem", "MarkItem", GameObject)
RegistChildComponent(LuaJuDianBattleCurrentMapRoot, "ClickBg", "ClickBg", GameObject)
RegistChildComponent(LuaJuDianBattleCurrentMapRoot, "SelfServerBtn", "SelfServerBtn", GameObject)
RegistChildComponent(LuaJuDianBattleCurrentMapRoot, "ZhuDianBtn", "ZhuDianBtn", GameObject)
RegistChildComponent(LuaJuDianBattleCurrentMapRoot, "CompleteBtn", "CompleteBtn", GameObject)
RegistChildComponent(LuaJuDianBattleCurrentMapRoot, "DeleteBtn", "DeleteBtn", GameObject)
RegistChildComponent(LuaJuDianBattleCurrentMapRoot, "MarkBtn", "MarkBtn", GameObject)
RegistChildComponent(LuaJuDianBattleCurrentMapRoot, "MarkFx", "MarkFx", CUIFx)
RegistChildComponent(LuaJuDianBattleCurrentMapRoot, "MonsterItem", "MonsterItem", GameObject)
RegistChildComponent(LuaJuDianBattleCurrentMapRoot, "JuDianBattleSceneOverviewRankRoot", "JuDianBattleSceneOverviewRankRoot", CCommonLuaScript)
RegistChildComponent(LuaJuDianBattleCurrentMapRoot, "LingQuanItem", "LingQuanItem", GameObject)
RegistChildComponent(LuaJuDianBattleCurrentMapRoot, "BottomLabel", "BottomLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaJuDianBattleCurrentMapRoot, "m_MiniMap")
RegistClassMember(LuaJuDianBattleCurrentMapRoot, "m_LastClickPos")
RegistClassMember(LuaJuDianBattleCurrentMapRoot, "m_CurrentMapId")
RegistClassMember(LuaJuDianBattleCurrentMapRoot, "m_SelfSceneIdx")
RegistClassMember(LuaJuDianBattleCurrentMapRoot, "m_CurrentSceneIdx")
RegistClassMember(LuaJuDianBattleCurrentMapRoot, "m_IsLocalMap")
RegistClassMember(LuaJuDianBattleCurrentMapRoot, "m_IsTagMode")
RegistClassMember(LuaJuDianBattleCurrentMapRoot, "m_MainPlayerMark")
RegistClassMember(LuaJuDianBattleCurrentMapRoot, "m_PathMark")

function LuaJuDianBattleCurrentMapRoot:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ClickBg.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnClickBgClick()
	end)


	
	UIEventListener.Get(self.SelfServerBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSelfServerBtnClick()
	end)


	
	UIEventListener.Get(self.ZhuDianBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnZhuDianBtnClick()
	end)


	
	UIEventListener.Get(self.CompleteBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCompleteBtnClick()
	end)


	
	UIEventListener.Get(self.DeleteBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDeleteBtnClick()
	end)


	
	UIEventListener.Get(self.MarkBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMarkBtnClick()
	end)


    --@endregion EventBind end
end

-- 由外部调用
function LuaJuDianBattleCurrentMapRoot:InitMap()

	-- 请求分线信息 并获取自己的分线id
	self.m_CurrentMapId = LuaJuDianBattleMgr:GetMapId()

	if LuaJuDianBattleMgr:IsInDailyGameplay() then
		Gac2Gas.GuildJuDianQuerySceneOverview()
	elseif LuaJuDianBattleMgr:IsInGameplay() then
		Gac2Gas.GuildJuDianQueryMapSceneInfo(self.m_CurrentMapId, 1)
		Gac2Gas.GuildJuDianQueryQueryMapLingQuanPos(self.m_CurrentMapId)
	end

	-- 关闭一些按钮
	self.MarkBtn:SetActive(false)
	self.DeleteBtn:SetActive(false)
	self.CompleteBtn:SetActive(false)
	self.BottomLabel.gameObject:SetActive(false)

	-- 清除item
	Extensions.RemoveAllChildren(self.MarkRoot.transform)

	self.MarkItem:SetActive(false)
	self.FlagItem:SetActive(false)
	self.PointItem:SetActive(false)
	self.NamelessItem:SetActive(false)
	self.MonsterItem:SetActive(false)
	self.LingQuanItem:SetActive(false)

	self.m_MiniMap = self.transform.parent.parent.parent.parent.parent:GetComponent(typeof(CMiniMapPopWnd))
	-- 当前点击位置
	self.m_LastClickPos = nil

	self.m_MiniMap.m_BackToGuildButton.gameObject:SetActive(false)
	self.m_MiniMap.m_BackToHouseButton.gameObject:SetActive(false)
	self.m_MiniMap.m_BackToCityButton.gameObject:SetActive(false)
	self.m_MiniMap.m_InfoButton.gameObject:SetActive(false)
	self.m_MiniMap.m_NpcInfoButton.gameObject:SetActive(false)

	self.m_MainPlayerMark = self.transform.parent:Find("mainPlayerSpirite").gameObject
	self.m_PathMark = self.transform.parent:Find("MapPathDrawer").gameObject

	local setting = GuildOccupationWar_Setting.GetData()
	self.m_PointRadius = setting.StrongholdCaptureShowRadius
	self.m_NoTransportRadius = setting.ForbidTeleportShowRadius
	self.m_NoMarkRadius = setting.ForbidLabelShowRadius
	self.m_NpcRadius = setting.StatueNPCAlertShowRadius
	self.m_PointRadius_JLHZ = setting.StrongholdCaptureShowRadiusJLHZ
	self.m_NoTransportRadius_JLHZ = setting.ForbidTeleportShowRadiusJLHZ
	self.m_NoMarkRadius_JLHZ = setting.ForbidLabelShowRadiusJLHZ
end

-- 外围玩法
function LuaJuDianBattleCurrentMapRoot:OnDailyData()
    self.m_IsDailyMode = true

    self.m_DailyData = LuaJuDianBattleMgr.SceneMonsterData
	local monsterList = self.m_DailyData[self.m_CurrentMapId]

    self.CompleteBtn.gameObject:SetActive(false)
    self.DeleteBtn.gameObject:SetActive(false)
    self.MarkBtn.gameObject:SetActive(false)

	-- 初始化怪物上去
	if monsterList then
		for i, monster in ipairs(monsterList) do
			local g = NGUITools.AddChild(self.MarkRoot, self.MonsterItem)
			g:SetActive(true)

			local data = {}
			data.x = monster[1]
			data.y = monster[2]
			self:SetLocation(g, data)
		end
	end

	self.TitleLabel.text = LocalString.GetString("[FFFE91]点击地图可直接传送")
end

function LuaJuDianBattleCurrentMapRoot:OnMapData(mapId, sceneIdx, sceneId, tag, flagList, juDianList, npcList)
	self.m_MiniMap.m_BackToCityButton.gameObject:SetActive(false)

	UnRegisterTick(self.m_NpcTimeTick)
	-- 清除item
	Extensions.RemoveAllChildren(self.MarkRoot.transform)

	self.m_CurrentSceneIdx = sceneIdx
	-- 设置显示模式
	self:SetShowMode(true, false)

	local data = PublicMap_PublicMap.GetData(mapId)

	-- 标记
	if tag then
		self.MarkItem:SetActive(true)
		self:SetLocation(self.MarkItem, tag)
	else
		self.MarkItem:SetActive(false)
	end

	-- 设置旗帜
	for i=1, #flagList do
		local g = NGUITools.AddChild(self.MarkRoot, self.FlagItem)
		g:SetActive(true)

		local data = flagList[i]
		self:SetLocation(g, data)
		self:InitFlag(g, data)
	end

	-- 设置据点
	for i=1, #juDianList do
		local g = NGUITools.AddChild(self.MarkRoot, self.PointItem)
		g:SetActive(true)

		local data = juDianList[i]
		self:SetLocation(g, data)
		self:InitJuDian(g, data)
	end

	-- 设置npc
	for i=1, #npcList do
		local g = NGUITools.AddChild(self.MarkRoot, self.NamelessItem)
		g:SetActive(true)

		local data = npcList[i]
		self:SetLocation(g, data)
		self:InitNpc(g, data)
	end

	self:SetShowModeItem(true, self.m_IsTagMode)
end

function LuaJuDianBattleCurrentMapRoot:OnLingQuanData(nextRefreshTime, lingQuanPosList)
	if #lingQuanPosList == 0 then return end
	-- 灵泉倒计时
	if self.m_LingQuanTick then UnRegisterTick(self.m_LingQuanTick) end
	self.m_LingQuanRefreshTime = nextRefreshTime
    self:UpdataLingQuanTick()
    if self.m_LingQuanRefreshTime > CServerTimeMgr.Inst.timeStamp then
        self.m_LingQuanTick = RegisterTick(function()
            self:UpdataLingQuanTick()
        end, 1000)
    end
	-- 设置灵泉
	for i = 1, #lingQuanPosList, 2 do
		local g = NGUITools.AddChild(self.MarkRoot, self.LingQuanItem)
		g:SetActive(true)
		local data = {}
		data.x = lingQuanPosList[i]
		data.y = lingQuanPosList[i+1]
		self:SetLocation(g, data)
		local noTransport = g.transform:Find("NoTransport"):GetComponent(typeof(UITexture))
		noTransport.height = self.m_LingQuanNoTransportRadius
		noTransport.width = self.m_LingQuanNoTransportRadius
		UIEventListener.Get(noTransport.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
			g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("灵泉附近不可进行传送"))
		end)
	end
end

-- 设置地图模式
function LuaJuDianBattleCurrentMapRoot:SetShowMode(isLocalMap, isTagMode)
	self.MarkFx:DestroyFx()
    self.m_IsTagMode = isTagMode

	local dailyMode = self.m_IsDailyMode

    self.MarkBtn:SetActive(not isTagMode and self.m_CanTag and not dailyMode)
    self.DeleteBtn:SetActive(isTagMode and self.m_CanTag and not dailyMode)
    self.CompleteBtn:SetActive(isTagMode and self.m_CanTag and not dailyMode)

	self.m_MainPlayerMark:SetActive(isLocalMap)
	self.m_PathMark:SetActive(isLocalMap)

	if isTagMode then
		self.TitleLabel.gameObject:SetActive(true)
		self.TitleLabel.text = LocalString.GetString("[FFFE91]点击地图添加标记（旗子及据点附近无法标记）")
    elseif isLocalMap then
        self.TitleLabel.gameObject:SetActive(false)
    else
		self.TitleLabel.gameObject:SetActive(true)
        self.TitleLabel.text = LocalString.GetString("[FFFE91]点击地图任意位置可直接传送")
    end

	self.ClickBg.gameObject:SetActive(isTagMode or not isLocalMap)
	self:SetShowModeItem(true, self.m_IsTagMode)
end

-- 设置地图模式
function LuaJuDianBattleCurrentMapRoot:SetShowModeItem(isLocalMap, isTagMode)
	for i =0, self.MarkRoot.transform.childCount-1 do
		local item = self.MarkRoot.transform:GetChild(i)
		local noMark = item:Find("NoMark")
		local noTransport = item:Find("NoTransport")

		if noMark then
			noMark.gameObject:SetActive(isTagMode)
		end

		if noTransport then
			noTransport.gameObject:SetActive((not isTagMode) and (not isLocalMap))
		end
	end
end

function LuaJuDianBattleCurrentMapRoot:InitFlag(item, flagData)
	local flagTexture = item.transform:Find("FlagTexture"):GetComponent(typeof(CUITexture))
	local battleIcon = item.transform:Find("BattleIcon").gameObject
	local playerLabel = item.transform:Find("PlayerLabel"):GetComponent(typeof(UILabel))
	local stateLabel = item.transform:Find("StateLabel"):GetComponent(typeof(UILabel))
	local bg = item.transform:Find("Bg"):GetComponent(typeof(UISprite))
	local noMark = item.transform:Find("NoMark"):GetComponent(typeof(UITexture))
	local noTransport = item.transform:Find("NoTransport"):GetComponent(typeof(UITexture))

	if self.m_CurrentMapId == 16103048 or self.m_CurrentMapId == 16103049 then
		noTransport.height = self.m_NoTransportRadius_JLHZ
		noTransport.width = self.m_NoTransportRadius_JLHZ
		noMark.height = self.m_NoMarkRadius_JLHZ
		noMark.width = self.m_NoMarkRadius_JLHZ
	else
		noTransport.height = self.m_NoTransportRadius
		noTransport.width = self.m_NoTransportRadius
		noMark.height = self.m_NoMarkRadius
		noMark.width = self.m_NoMarkRadius
	end
	local noTransportBoxCollider = noTransport.transform:GetComponent(typeof(BoxCollider))
	noTransportBoxCollider.size = Vector3(noTransport.height, noTransport.width, 0)
	local noMarkBoxCollider = noMark.transform:GetComponent(typeof(BoxCollider))
	noMarkBoxCollider.size = Vector3(noMark.height, noMark.width, 0)

	if flagData.hasTaken then
		playerLabel.text = flagData.playerName
        stateLabel.text = flagData.guildName
		local selfGuildId = 0
    	if CClientMainPlayer.Inst then
			selfGuildId = CClientMainPlayer.Inst.BasicProp.GuildId
		end

		if selfGuildId == flagData.guildId then
			-- 自己帮派
            bg.color = JuDianColor.Self
            flagTexture:LoadMaterial("UI/Texture/Transparent/Material/minimappopwnd_icon_qi_wobang.mat")
		else
			-- 敌人帮派
            bg.color = JuDianColor.Enemy
            flagTexture:LoadMaterial("UI/Texture/Transparent/Material/minimappopwnd_icon_qi_tabang.mat")
		end
	else
		-- 争夺中
        bg.color = JuDianColor.Battle
        flagTexture:LoadMaterial("UI/Texture/Transparent/Material/minimappopwnd_icon_qi_zhengduo.mat")
		playerLabel.text = ""
		stateLabel.text = LocalString.GetString("争夺中")
	end

	UIEventListener.Get(noMark.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("据点、据点旗、无名神像附近不可进行标记"))
	end)

	UIEventListener.Get(noTransport.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("据点、据点旗、无名神像附近不可进行传送"))
	end)
end

function LuaJuDianBattleCurrentMapRoot:InitJuDian(item, pointData)
	local pointTexture = item.transform:Find("PointTexture"):GetComponent(typeof(UITexture))
	local battleIcon = item.transform:Find("BattleIcon").gameObject
	local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local stateLabel = item.transform:Find("StateLabel"):GetComponent(typeof(UILabel))
    local bg = item.transform:Find("Bg"):GetComponent(typeof(UISprite))
	local areaTexture = item.transform:Find("AreaTexture"):GetComponent(typeof(UITexture))
	local areaSprite = item.transform:Find("AreaTexture/Sprite"):GetComponent(typeof(UITexture))
	local noMark = item.transform:Find("NoMark"):GetComponent(typeof(UITexture))
	local noTransport = item.transform:Find("NoTransport"):GetComponent(typeof(UITexture))

    battleIcon:SetActive(not pointData.hasTaken)
    nameLabel.text = LocalString.GetString("据点").. tostring(pointData.judianId)

	-- 设置半径
	if self.m_CurrentMapId == 16103048 or self.m_CurrentMapId == 16103049 then
		areaTexture.height = self.m_PointRadius_JLHZ
		areaTexture.width = self.m_PointRadius_JLHZ
		noTransport.height = self.m_NoTransportRadius_JLHZ
		noTransport.width = self.m_NoTransportRadius_JLHZ
		noMark.height = self.m_NoMarkRadius_JLHZ
		noMark.width = self.m_NoMarkRadius_JLHZ
	else
		areaTexture.height = self.m_PointRadius
		areaTexture.width = self.m_PointRadius
		noTransport.height = self.m_NoTransportRadius
		noTransport.width = self.m_NoTransportRadius
		noMark.height = self.m_NoMarkRadius
		noMark.width = self.m_NoMarkRadius
	end
	local noTransportBoxCollider = noTransport.transform:GetComponent(typeof(BoxCollider))
	noTransportBoxCollider.size = Vector3(noTransport.height, noTransport.width, 0)
	local noMarkBoxCollider = noMark.transform:GetComponent(typeof(BoxCollider))
	noMarkBoxCollider.size = Vector3(noMark.height, noMark.width, 0)

	if pointData.hasTaken then
        stateLabel.text = pointData.guildName

		local selfGuildId = 0
    	if CClientMainPlayer.Inst then
			selfGuildId = CClientMainPlayer.Inst.BasicProp.GuildId
		end

		if selfGuildId == pointData.guildId then
			-- 自己帮派
			areaTexture.color = JuDianColor.Self
			areaSprite.color = JuDianTransparentColor.Self
			pointTexture.color = JuDianColor.Self
            bg.color = JuDianColor.Self
		else
			-- 敌人帮派
            bg.color = JuDianColor.Enemy
			pointTexture.color = JuDianColor.Enemy
			areaTexture.color = JuDianColor.Enemy
			areaSprite.color = JuDianTransparentColor.Enemy
		end
	else
		-- 争夺中
        bg.color = JuDianColor.Battle
        pointTexture.color = JuDianColor.Battle
        areaTexture.color = JuDianColor.Battle
        areaSprite.color = JuDianTransparentColor.Battle

		if pointData.guildId == 0 then
			stateLabel.text = LocalString.GetString("争夺中")
		else
			stateLabel.text = pointData.guildName
		end
	end

	UIEventListener.Get(noMark.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("据点、据点旗、无名神像附近不可进行标记"))
	end)

	UIEventListener.Get(noTransport.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("据点、据点旗、无名神像附近不可进行传送"))
	end)
end

function LuaJuDianBattleCurrentMapRoot:InitNpc(item, npcData)
    self.m_NpcStateLabel = item.transform:Find("StateLabel"):GetComponent(typeof(UILabel))
	self.m_NpcStateLabel.gameObject:SetActive(true)
	self.m_NpcTimeStamp = npcData.nextRefreshTime - CServerTimeMgr.Inst.timeStamp
	self.m_NpcShow = npcData.bShow
	self:UpdataNpcTimeTick()


	local noMark = item.transform:Find("NoMark"):GetComponent(typeof(UITexture))
	local noTransport = item.transform:Find("NoTransport"):GetComponent(typeof(UITexture))
	local warning = item.transform:Find("WarningTexture"):GetComponent(typeof(UITexture))

	if self.m_CurrentMapId == 16103048 or self.m_CurrentMapId == 16103049 then
		noTransport.height = self.m_NoTransportRadius_JLHZ
		noTransport.width = self.m_NoTransportRadius_JLHZ
		noMark.height = self.m_NoMarkRadius_JLHZ
		noMark.width = self.m_NoMarkRadius_JLHZ
	else
		noTransport.height = self.m_NoTransportRadius
		noTransport.width = self.m_NoTransportRadius
		noMark.height = self.m_NoMarkRadius
		noMark.width = self.m_NoMarkRadius
	end
	warning.height = self.m_NpcRadius
	warning.width = self.m_NpcRadius
	local noTransportBoxCollider = noTransport.transform:GetComponent(typeof(BoxCollider))
	noTransportBoxCollider.size = Vector3(noTransport.height, noTransport.width, 0)
	local noMarkBoxCollider = noMark.transform:GetComponent(typeof(BoxCollider))
	noMarkBoxCollider.size = Vector3(noMark.height, noMark.width, 0)

	self.m_NpcTimeTick = RegisterTick(function()
		self:UpdataNpcTimeTick()
	end, 1000)

	UIEventListener.Get(noMark.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("据点、据点旗、无名神像附近不可进行标记"))
	end)

	UIEventListener.Get(noTransport.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("据点、据点旗、无名神像附近不可进行传送"))
	end)
end

function LuaJuDianBattleCurrentMapRoot:UpdataNpcTimeTick()
    if self.m_NpcTimeStamp < 0 then
        self.m_NpcTimeStamp = 0
		self.m_NpcStateLabel.gameObject:SetActive(false)
    end

    local mins = math.floor(self.m_NpcTimeStamp/60)
    local secs = math.floor((self.m_NpcTimeStamp% 60))

	if self.m_NpcShow then
		self.m_NpcStateLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]%02d:%02d后消失"), mins, secs)
	else
		self.m_NpcStateLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]%02d:%02d后出现"), mins, secs)
	end
    
    self.m_NpcTimeStamp = self.m_NpcTimeStamp - 1
end

function LuaJuDianBattleCurrentMapRoot:UpdataLingQuanTick()
	local remainTime = self.m_LingQuanRefreshTime - CServerTimeMgr.Inst.timeStamp
	self.BottomLabel.gameObject:SetActive(remainTime > 0)
    if remainTime > 0 then
        local mins = math.floor(remainTime / 60)
        local secs = math.floor((remainTime % 60))
        self.BottomLabel.text = SafeStringFormat3(LocalString.GetString("%d分%d秒后灵泉刷新"), mins, secs)
    end
end

-- 设置位置
function LuaJuDianBattleCurrentMapRoot:SetLocation(item, data)
	local pos3d = Utility.GridPos2WorldPos(data.x, data.y)
	item.transform.localPosition = self.m_MiniMap:CalculateMapPos(pos3d)
end

-- 设置位置
function LuaJuDianBattleCurrentMapRoot:OnQualification(canTag, canKick)
	-- 有权利标记
	self.m_CanTag = canTag
	-- 刷新显示
	self:SetShowMode(true, self.m_IsTagMode)
end

function LuaJuDianBattleCurrentMapRoot:OnEnable()
	self.TitleLabel.gameObject:SetActive(false)
	self:InitMap()
	Gac2Gas.GuildJuDianQuerySetQualification()

	if not LuaJuDianBattleMgr:IsInGameplay() then
        self.JuDianBattleSceneOverviewRankRoot.gameObject:SetActive(false)
    end

	g_ScriptEvent:AddListener("JuDianBattle_SyncDailySceneData", self, "OnDailyData")
    g_ScriptEvent:AddListener("GuildJuDianSyncMapSceneInfo", self, "OnMapData")
	g_ScriptEvent:AddListener("GuildJuDianSyncSetQualification", self, "OnQualification")
    g_ScriptEvent:AddListener("GuildJuDianQueryMapLingQuanPosResult", self, "OnLingQuanData")
end

function LuaJuDianBattleCurrentMapRoot:OnDisable()
	UnRegisterTick(self.m_NpcTimeTick)
	UnRegisterTick(self.m_LingQuanTick)
	g_ScriptEvent:RemoveListener("JuDianBattle_SyncDailySceneData", self, "OnDailyData")
    g_ScriptEvent:RemoveListener("GuildJuDianSyncMapSceneInfo", self, "OnMapData")
    g_ScriptEvent:RemoveListener("GuildJuDianSyncSetQualification", self, "OnQualification")
    g_ScriptEvent:RemoveListener("GuildJuDianQueryMapLingQuanPosResult", self, "OnLingQuanData")
end

--@region UIEvent

function LuaJuDianBattleCurrentMapRoot:OnSelfServerBtnClick()
	-- 回到本服
    LuaJuDianBattleMgr:RequestBackSelfSever()
end

function LuaJuDianBattleCurrentMapRoot:OnZhuDianBtnClick()
	-- 回到驻点
    Gac2Gas.GuildJuDianRequestGoToOccupation()
end

-- 设置完成
function LuaJuDianBattleCurrentMapRoot:OnCompleteBtnClick()
	if self.m_LastClickPos and self.MarkItem.activeSelf then
        Gac2Gas.GuildJuDianRequestTagPosition(self.m_CurrentMapId, self.m_CurrentSceneIdx, math.floor(self.m_LastClickPos.x/64), math.floor(self.m_LastClickPos.y/64))
    end
	self:SetShowMode(true, false)
end

-- 删除标记
function LuaJuDianBattleCurrentMapRoot:OnDeleteBtnClick()
	self.MarkItem:SetActive(false)
	-- 取消标记点
    Gac2Gas.GuildJuDianRequestTagPosition(self.m_CurrentMapId, self.m_CurrentSceneIdx, 0, 0)
end

-- 标记节点
function LuaJuDianBattleCurrentMapRoot:OnMarkBtnClick()
	self:SetShowMode(true, true)
end


-- 点击进行标记
function LuaJuDianBattleCurrentMapRoot:OnClickBgClick()
	local pos = CPos.InValidPos
	pos = self.m_MiniMap:GetClickWorldPixelPos()
	local grid = Utility.PixelPos2GridPos(pos)

	self:SetLocation(self.MarkFx, grid)
	self.MarkFx:DestroyFx()
	self.MarkFx:LoadFx("fx/ui/prefab/UI_arrow_sign.prefab")

	if not (pos.x == -1000000 and pos.y == -1000000) then
		-- 更新标记物
		if self.m_IsTagMode then
			self.m_LastClickPos = pos
			-- 标记物
			self.MarkItem:SetActive(true)
			self:SetLocation(self.MarkItem, grid)
		else
			if self.m_CurrentSceneIdx == nil then
				self.m_CurrentSceneIdx = 1
			end

			if LuaJuDianBattleMgr:IsInGameplay() then
				Gac2Gas.GuildJuDianRequestTeleportToPosition(self.m_CurrentMapId, self.m_CurrentSceneIdx, math.floor(pos.x/64), math.floor(pos.y/64))
			elseif LuaJuDianBattleMgr:IsInDailyGameplay() then
				Gac2Gas.GuildJuDianRequestTeleportToDailyScene(self.m_CurrentMapId, math.floor(pos.x/64), math.floor(pos.y/64))
			end
		end
	end
end


--@endregion UIEvent

