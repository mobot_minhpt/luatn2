local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnRadioBox = import "L10.UI.QnRadioBox"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local QnTableView = import "L10.UI.QnTableView"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local QnButton = import "L10.UI.QnButton"
local Constants = import "L10.Game.Constants"

LuaGuildTerritorialWarsRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildTerritorialWarsRankWnd, "TopTabs", "TopTabs", QnRadioBox)
RegistChildComponent(LuaGuildTerritorialWarsRankWnd, "QnSelectBtnTemplate", "QnSelectBtnTemplate", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsRankWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaGuildTerritorialWarsRankWnd, "TableView", "TableView", QnTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaGuildTerritorialWarsRankWnd, "m_List")
RegistClassMember(LuaGuildTerritorialWarsRankWnd, "m_SelectRow")
RegistClassMember(LuaGuildTerritorialWarsRankWnd, "m_SelectTab")
RegistClassMember(LuaGuildTerritorialWarsRankWnd, "m_PlayerGuildId")

function LuaGuildTerritorialWarsRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaGuildTerritorialWarsRankWnd:Init()
    self.QnSelectBtnTemplate.gameObject:SetActive(false)
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self.m_List and #self.m_List or 0
        end,
        function(item,row)
            self:IniItem(item,row)
        end)
	self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(index)
        self:OnSelectAtRow(index)
    end)

    local guildGroupingNames = GuildTerritoryWar_Setting.GetData().GuildGroupingNames
    self.TopTabs.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), guildGroupingNames.Length)
    for i = 0, guildGroupingNames.Length - 1 do
        local obj = NGUITools.AddChild(self.Grid.gameObject, self.QnSelectBtnTemplate.gameObject)
        obj.gameObject:SetActive(true)
        local btn = obj:GetComponent(typeof(QnSelectableButton))
        btn.Text = guildGroupingNames[i]
        self.TopTabs.m_RadioButtons[i] = btn
        self.TopTabs.m_RadioButtons[i].OnClick = MakeDelegateFromCSFunction(self.TopTabs.On_Click, MakeGenericClass(Action1, QnButton), self.TopTabs)
    end
    self.TopTabs:Awake()
    self.Grid:Reposition()
    self.TopTabs.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
		self:OnSelectTab(btn,index)
	end)
	self.TopTabs:ChangeTo(LuaGuildTerritorialWarsMgr.m_RankWndDefaultIndex - 1, true)
end

function LuaGuildTerritorialWarsRankWnd:IniItem(item,row)
	local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankSprite = item.transform:Find("RankLabel/RankSprite"):GetComponent(typeof(UISprite))
    local guildNameLabel = item.transform:Find("GuildNameLabel"):GetComponent(typeof(UILabel))
    local serverNameLabel = item.transform:Find("ServerNameLabel"):GetComponent(typeof(UILabel))
    local powerLabel = item.transform:Find("PowerLabel"):GetComponent(typeof(UILabel))
    local occupyNumLabel = item.transform:Find("OccupyNumLabel"):GetComponent(typeof(UILabel))
    local scoreLabel = item.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))

    local data = self.m_List[row + 1]
    local rank = data.rank
    local guildName = data.guildName
    local serverName = data.serverName
    local power = data. equipScore
    local occupyNum = data.occupyCount
    local score = data.score
    local guildId = data.guildId
    local myGuildId = self.m_PlayerGuildId 
    local isMyGuild = guildId == myGuildId
    local labelColor = NGUIText.ParseColor24(isMyGuild and "00ff60" or "ffffff", 0)

    rankLabel.color = labelColor
    rankLabel.text = rank or rank or LocalString.GetString("——")
    rankLabel.enabled = not rank or rank > 3
    rankSprite.spriteName = self:GetRankImageByRankIndex(rank)
    guildNameLabel.color = labelColor
    guildNameLabel.text = SafeStringFormat3("%s%s", guildName, data.guildDeleted and LocalString.GetString("[ff5050](解散)") or "")
    serverNameLabel.color = labelColor
    serverNameLabel.text = serverName
    powerLabel.color = labelColor
    powerLabel.text = power
    occupyNumLabel.color = labelColor
    occupyNumLabel.text = occupyNum
    scoreLabel.color = labelColor
    scoreLabel.text = score

    item:GetComponent(typeof(UISprite)).spriteName = row % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite
end

function LuaGuildTerritorialWarsRankWnd:GetRankImageByRankIndex(index)
	if index == 1 then
		return Constants.RankFirstSpriteName
	elseif index == 2 then
		return Constants.RankSecondSpriteName
	elseif index == 3 then
		return Constants.RankThirdSpriteName
	else
		return nil
	end
end

function LuaGuildTerritorialWarsRankWnd:OnSelectAtRow(index)
	self.m_SelectRow = index
end

function LuaGuildTerritorialWarsRankWnd:OnSelectTab(btn,index)
    self.m_SelectTab = index
    Gac2Gas.RequsetTerritoryWarRankInfo(index + 1)
end

--@region UIEvent

--@endregion UIEvent

function LuaGuildTerritorialWarsRankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSendTerritoryWarRankInfo", self, "OnSendTerritoryWarRankInfo")
end

function LuaGuildTerritorialWarsRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSendTerritoryWarRankInfo", self, "OnSendTerritoryWarRankInfo")
end

function LuaGuildTerritorialWarsRankWnd:OnSendTerritoryWarRankInfo(playerGuildId)
    self.m_List = LuaGuildTerritorialWarsMgr.m_RankInfo
    self.m_PlayerGuildId = playerGuildId
    self.TableView:ReloadData(true,true)
end