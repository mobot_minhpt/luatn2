require("common/common_include")

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CBaseWnd = import "L10.UI.CBaseWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UILabel = import "UILabel"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local UISimpleTableView = import "L10.UI.UISimpleTableView"
local DefaultUISimpleTableViewDataSource = import "L10.UI.DefaultUISimpleTableViewDataSource"
local CIMMgr = import "L10.Game.CIMMgr"
local CUITexture = import "L10.UI.CUITexture"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local AlignType = import "CPlayerInfoMgr+AlignType"

LuaLiangHaoFriendListWnd = class()

RegistClassMember(LuaLiangHaoFriendListWnd, "m_TableView")
RegistClassMember(LuaLiangHaoFriendListWnd, "m_ItemTemplate")
RegistClassMember(LuaLiangHaoFriendListWnd,"m_Items")
RegistClassMember(LuaLiangHaoFriendListWnd,"m_DefaultSimpleTableViewDataSource")


function LuaLiangHaoFriendListWnd:Init()
	self.m_TableView = self.transform:GetComponent(typeof(UISimpleTableView))
	self.m_ItemTemplate = self.transform:Find("Anchor/ScrollView/Pool/Item").gameObject

	if not self.m_DefaultSimpleTableViewDataSource then

        self.m_DefaultSimpleTableViewDataSource = DefaultUISimpleTableViewDataSource.Create(function ()
            return self:NumberOfRows()
        end,
        function ( index )
            return self:CellForRowAtIndex(index)
        end)
    end

    self.m_TableView.dataSource = self.m_DefaultSimpleTableViewDataSource
    self.m_TableView:Clear()
    
    self.m_ItemTemplate:SetActive(false)

    self:LoadData()

end

function LuaLiangHaoFriendListWnd:LoadData()
	self.m_Items = {}
	local friends = CIMMgr.Inst.Friends
    CommonDefs.EnumerableIterate(friends, DelegateFactory.Action_object(function (___value) 
        local playerId = ___value

        if CIMMgr.Inst:IsSameServerFriend(playerId) then
        	local info = CIMMgr.Inst:GetBasicInfo(playerId)
        	if info.ProfileInfo.LiangHaoId>0 then
        		table.insert(self.m_Items, info)
        	end
    	end
    end))

    if #self.m_Items==0 then
    	g_MessageMgr:ShowMessage("Lianghao_NoFriendHaveLiangHao")
    	self:Close()
    	return
    end
    --过期的排前面
    table.sort(self.m_Items, function (info1, info2)
        local friendliness1 = CIMMgr.Inst:GetFriendliness(info1.ID)
        local friendliness2 = CIMMgr.Inst:GetFriendliness(info2.ID)
        if friendliness1 ~= friendliness2 then
            return friendliness1 > friendliness2 --亲密度高的排前面
        end

    	local expiredTime1 = info1.ProfileInfo.LiangHaoExpiredTime
    	local expiredTime2 = info2.ProfileInfo.LiangHaoExpiredTime
    	if expiredTime1 ~= expiredTime2 then
    		return expiredTime1 < expiredTime2
        end

    	return info1.ProfileInfo.LiangHaoId<info2.ProfileInfo.LiangHaoId
    end)

    self.m_TableView:LoadData(0, false)
end

function LuaLiangHaoFriendListWnd:NumberOfRows()
    return #self.m_Items
end

function LuaLiangHaoFriendListWnd:CellForRowAtIndex(index) 

    local cellIdentifier = "LiangHaoFriendListItemIdentifier"
    local cell = self.m_TableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = self.m_TableView:AllocNewCellWithIdentifier(self.m_ItemTemplate, cellIdentifier)
    end

    if index >= 0 and index < #self.m_Items then
        local basicInfo = self.m_Items[index+1]

        local portraitTexture = cell.transform:Find("Border/Portrait"):GetComponent(typeof(CUITexture))
        local levelLabel = cell.transform:Find("Border/LevelLabel"):GetComponent(typeof(UILabel))
        local professionIcon = cell.transform:Find("Icon"):GetComponent(typeof(UISprite))
        local nameLabel = cell.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        local expireTimeLabel = cell.transform:Find("ExpireTimeLabel"):GetComponent(typeof(UILabel))
        local renewButton = cell.transform:Find("OpButton").gameObject
        local portraitButton = cell.transform:Find("Border").gameObject

        portraitTexture:LoadNPCPortrait(CUICommonDef.GetPortraitName(basicInfo.Class, basicInfo.Gender, basicInfo.Expression), false)
        levelLabel.text = CUICommonDef.GetColoredLevelString(basicInfo.XianFanStatus>0, basicInfo.Level, levelLabel.color)
        professionIcon.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), basicInfo.Class))
        nameLabel.text = basicInfo.Name..(CUICommonDef.GetLiangHaoIcon(basicInfo.ProfileInfo.LiangHaoId, false) or "")
        if CServerTimeMgr.Inst.timeStamp>basicInfo.ProfileInfo.LiangHaoExpiredTime then
        	expireTimeLabel.text = LocalString.GetString("已过期")
        else
        	expireTimeLabel.text = ToStringWrap(CServerTimeMgr.ConvertTimeStampToZone8Time(basicInfo.ProfileInfo.LiangHaoExpiredTime), "yyyy-MM-dd")
        end
        local playerId = basicInfo.ID
        local lianghaoId = basicInfo.ProfileInfo.LiangHaoId
        CommonDefs.AddOnClickListener(renewButton, DelegateFactory.Action_GameObject(function(go) self:OnRenewButtonClick(playerId, lianghaoId) end), false)
        CommonDefs.AddOnClickListener(portraitButton, DelegateFactory.Action_GameObject(function(go) self:OnPortraitGoClick(playerId) end), false)
    end
   
    return cell
end

function LuaLiangHaoFriendListWnd:OnRenewButtonClick(playerId, lianghaoId)
	LuaLiangHaoMgr.ShowLiangHaoRenewWnd(playerId, lianghaoId)
	self:Close()
end

function LuaLiangHaoFriendListWnd:OnPortraitGoClick(playerId)
	CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
end

function LuaLiangHaoFriendListWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end
