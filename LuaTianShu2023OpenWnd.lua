local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local Animation = import "UnityEngine.Animation"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local Constants = import "L10.Game.Constants"
local String = import "System.String"
local CItemMgr = import "L10.Game.CItemMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local EnumEventType = import "EnumEventType"
local CButton = import "L10.UI.CButton"
local EnumItemType = import "L10.Game.EnumItemType"
local IdPartition = import "L10.Game.IdPartition"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local CTianShuOpenMgr = import "L10.UI.CTianShuOpenMgr"
local CPlayerDataMgr = import "L10.Game.CPlayerDataMgr"
local Animation = import "UnityEngine.Animation"

LuaTianShu2023OpenWnd = class()
LuaTianShu2023OpenWnd.skipAnimation = false
--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaTianShu2023OpenWnd, "LingYuLabel", "LingYuLabel", UILabel)
RegistChildComponent(LuaTianShu2023OpenWnd, "AddBtn", "AddBtn", GameObject)
RegistChildComponent(LuaTianShu2023OpenWnd, "BookTemplate", "BookTemplate", GameObject)
RegistChildComponent(LuaTianShu2023OpenWnd, "OpenBtn", "OpenBtn", GameObject)
RegistChildComponent(LuaTianShu2023OpenWnd, "BookSlots", "BookSlots", GameObject)
RegistChildComponent(LuaTianShu2023OpenWnd, "Slot1", "Slot1", GameObject)
RegistChildComponent(LuaTianShu2023OpenWnd, "Slot2", "Slot2", GameObject)
RegistChildComponent(LuaTianShu2023OpenWnd, "Slot3", "Slot3", GameObject)
RegistChildComponent(LuaTianShu2023OpenWnd, "Slot4", "Slot4", GameObject)
RegistChildComponent(LuaTianShu2023OpenWnd, "Slot5", "Slot5", GameObject)
RegistChildComponent(LuaTianShu2023OpenWnd, "Slot6", "Slot6", GameObject)
RegistChildComponent(LuaTianShu2023OpenWnd, "Slot7", "Slot7", GameObject)
RegistChildComponent(LuaTianShu2023OpenWnd, "Slot8", "Slot8", GameObject)
RegistChildComponent(LuaTianShu2023OpenWnd, "Slot9", "Slot9", GameObject)
RegistChildComponent(LuaTianShu2023OpenWnd, "JumpButton", "JumpButton", GameObject)
RegistChildComponent(LuaTianShu2023OpenWnd, "SettingBtn", "SettingBtn", GameObject)
RegistChildComponent(LuaTianShu2023OpenWnd, "SettingPanel", "SettingPanel", GameObject)
RegistChildComponent(LuaTianShu2023OpenWnd, "SettingHiddenButton", "SettingHiddenButton", GameObject)
RegistChildComponent(LuaTianShu2023OpenWnd, "SkipAnimationButton", "SkipAnimationButton", GameObject)

--@endregion RegistChildComponent end

function LuaTianShu2023OpenWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.skipAnimationValue = 1
    local list = {}
    local json = CPlayerDataMgr.Inst:LoadPlayerData("tianshuOpen_defaultSetting")
    if json~=nil and json~="" then
        list = luaJson.json2lua(json)
    end
    if list then
        for key,v in pairs(list) do
            if key == "skip_animation" then
                self.skipAnimationValue = v
            end
        end
    end
    LuaTianShu2023OpenWnd.skipAnimation = (self.skipAnimationValue == 1)

    self.BookTemplate:SetActive(false)
    self:InitWndData()
    self:InitUIEvent()
    self.wndAniComp = self.transform:GetComponent(typeof(Animation))
    if LuaTianShu2023OpenWnd.skipAnimation then
        self:RefreshVariableUI()
    else    
        self.wndAniComp:Play("tianshu2023openwnd_show")
        self.delayShowResult = RegisterTickOnce(function()
            self:RefreshVariableUI()
        end, 1000)
    end
end

function LuaTianShu2023OpenWnd:Init()
    
end

--@region UIEvent

--@endregion UIEvent

function LuaTianShu2023OpenWnd:InitWndData()
    self.slotList = {}
    for i = 1, 9 do
        table.insert(self.slotList, self["Slot" .. i])
    end
    
    self.tianshuTemplateId2Icon = {}
    Item_TianShuWeight.Foreach(function(_, v)
        self.tianshuTemplateId2Icon[v.Id] = v.Icon
        if v.FeiShengReplaceItem ~= 0 then
            self.tianshuTemplateId2Icon[v.FeiShengReplaceItem] = v.FeiShengReplaceIcon
        end
    end)
end

function LuaTianShu2023OpenWnd:RefreshVariableUI()
    --refresh skip state
    self.SkipAnimationButton.transform:Find("log").gameObject:SetActive(LuaTianShu2023OpenWnd.skipAnimation)
    
    --refresh slots
    self:RefreshSlots()

    --refresh AddBtn/openBtn
    self:OnMainPlayerUpdateMoney()
    self:RefreshOpenButton()
end 

function LuaTianShu2023OpenWnd:UpdateKaiShuResult()
    if LuaTianShu2023OpenWnd.skipAnimation then
        self:RefreshSlots()
    else
        if self.delayShowResult then
            UnRegisterTick(self.delayShowResult)
        end
        
        self.wndAniComp:Play("tianshu2023openwnd_show")
        self.delayShowResult = RegisterTickOnce(function()
            self:RefreshSlots()
        end, 1000)
    end
end

function LuaTianShu2023OpenWnd:RefreshSlots()
    self.BookTemplate:SetActive(false)
    if self.delayShowTick then
        UnRegisterTick(self.delayShowTick)
    end
    if self.delayJueJiShow then
        UnRegisterTick(self.delayJueJiShow)
    end
    
    for i = 1, #self.slotList do
        Extensions.RemoveAllChildren(self.slotList[i].transform)
    end
    
    local templateIds = LuaQingMing2022Mgr:Array2Tbl(CTianShuOpenMgr.Inst.ItemTemplateIds)
    self.showTemplateId = nil
    local showLevel = 0
    local templateListLength = #templateIds
    local bookList = {}
    local showBookIndex = 1
    for i = 1, templateListLength do
        local instance = CUICommonDef.AddChild(self.slotList[i], self.BookTemplate)
        self:InitBookItem(instance, templateIds[i])
        local itemCfg = Item_Item.GetData(templateIds[i])
        local showJueJiEffect = 0
        if itemCfg and itemCfg.Type == EnumItemType_lua.Jueji then
            if itemCfg.GradeCheck == 100 then
                showJueJiEffect = 2
                if showLevel == 0 or showLevel < itemCfg.GradeCheck then
                    self.showTemplateId = templateIds[i]
                    showLevel = itemCfg.GradeCheck
                end
            elseif itemCfg.GradeCheck == 70 then
                showJueJiEffect = 1
                if showLevel == 0 or showLevel < itemCfg.GradeCheck then
                    self.showTemplateId = templateIds[i]
                    showLevel = itemCfg.GradeCheck
                end
            end
        end
        table.insert(bookList, {obj = instance, showJueJiEffect = showJueJiEffect})
    end
    self:RefreshOpenButton()
    
    self.delayShowTick = RegisterTickWithDuration(function ()
        if bookList[showBookIndex] then
            bookList[showBookIndex].obj:SetActive(true)
            local aniComp = bookList[showBookIndex].obj.transform:GetComponent(typeof(Animation))
            bookList[showBookIndex].obj.transform:Find("vfx_normal").gameObject:SetActive(bookList[showBookIndex].showJueJiEffect == 0)
            bookList[showBookIndex].obj.transform:Find("vfx_juejiruo").gameObject:SetActive(bookList[showBookIndex].showJueJiEffect == 1)
            bookList[showBookIndex].obj.transform:Find("vfx_juejiqiang").gameObject:SetActive(bookList[showBookIndex].showJueJiEffect == 2)
            aniComp:Play("tianshu2023openwnd_template")
            showBookIndex = showBookIndex + 1
        end
    end, 100, 100 * (templateListLength))
    self.delayJueJiShow = RegisterTickOnce(function()
        self:TryShowJueJiBook()
    end, 100 * (templateListLength) + 200)
end

function LuaTianShu2023OpenWnd:TryShowJueJiBook()
    if self.showTemplateId then
        LuaTianShu2023ShareWnd.m_ShowTemplateId = self.showTemplateId
        CUIManager.ShowUI("TianShu2023ShareWnd")
        self.showTemplateId = nil
    end
end

function LuaTianShu2023OpenWnd:InitBookItem(bookObj, templateId)
    local iconTex = bookObj.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local ItemNameLabel = bookObj.transform:Find("ItemNameLabel"):GetComponent(typeof(UILabel))
    if (IdPartition.IdIsItem(templateId)) then
        local itemCfg = Item_Item.GetData(templateId)
        iconTex:LoadMaterial(self.tianshuTemplateId2Icon[templateId])
        ItemNameLabel.text = System.String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor(GameSetting_Common_Wapper.Inst:GetColor(itemCfg.NameColor)),
            string.gsub(itemCfg.Name, LocalString.GetString("%(秘籍%)"), ""))
    else
        iconTex:Clear()
        ItemNameLabel.text = ""
    end
    UIEventListener.Get(bookObj).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemInfoMgr.ShowLinkItemTemplateInfo(templateId);
    end)
end

function LuaTianShu2023OpenWnd:OnEnable()
    g_ScriptEvent:AddListener("MainPlayerUpdateMoney", self, "OnMainPlayerUpdateMoney")
    g_ScriptEvent:AddListener("KaiShu2023ReturnResult", self, "UpdateKaiShuResult")
end

function LuaTianShu2023OpenWnd:OnDisable()
    g_ScriptEvent:RemoveListener("MainPlayerUpdateMoney", self, "OnMainPlayerUpdateMoney")
    g_ScriptEvent:RemoveListener("KaiShu2023ReturnResult", self, "UpdateKaiShuResult")

    if self.delayShowTick then
        UnRegisterTick(self.delayShowTick)
    end
    if self.delayShowResult then
        UnRegisterTick(self.delayShowResult)
    end
    if self.delayJueJiShow then
        UnRegisterTick(self.delayJueJiShow)
        self:TryShowJueJiBook()
    end
end

function LuaTianShu2023OpenWnd:OnMainPlayerUpdateMoney()
    if CClientMainPlayer.Inst then
        self.LingYuLabel.text = CClientMainPlayer.Inst.Jade
    end
end

function LuaTianShu2023OpenWnd:RefreshOpenButton()
    local totalTianShuNumber = self:GetTianShuCount()
    local openLabel = self.OpenBtn.transform:Find("Label"):GetComponent(typeof(UILabel))
    local openBtnComp = self.OpenBtn.transform:GetComponent(typeof(CButton))
    
    if totalTianShuNumber > 0 then
        openLabel.text = LocalString.GetString("再开一本(") .. totalTianShuNumber .. ")"
        openBtnComp.enabled = true
    else
        local tianshu = Mall_LingYuMall.GetData(Constants.DunJiaTianShuTemplateId)
        openLabel.text = LocalString.GetString("再开一本(") .. tianshu.Jade .. LocalString.GetString("灵玉)")
        openBtnComp.enabled = true
    end
end

function LuaTianShu2023OpenWnd:GetTianShuCount()
    if CClientMainPlayer.Inst then
        local itemProp = CClientMainPlayer.Inst.ItemProp
        local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)
        local totalTianShuNumber = 0
        for i = 1, bagSize do
            local id = itemProp:GetItemAt(EnumItemPlace.Bag, i)
            if String.IsNullOrEmpty(id) then
                --do nothing
            else
                local item = CItemMgr.Inst:GetById(id)
                if item and item.IsItem and item.TemplateId == Constants.DunJiaTianShuTemplateId then
                    totalTianShuNumber = totalTianShuNumber + item.Amount
                end
            end
        end
        return totalTianShuNumber
    end
    return 0
end

function LuaTianShu2023OpenWnd:InitUIEvent()
    --addBtn onclick
    UIEventListener.Get(self.AddBtn).onClick = DelegateFactory.VoidDelegate(function (_)
        self:OnClickAddBtn()
    end)
    --openBtn onclick
    UIEventListener.Get(self.OpenBtn).onClick = DelegateFactory.VoidDelegate(function (_)
        self:OnClickOpenBtn()
    end)

    UIEventListener.Get(self.SettingBtn).onClick = DelegateFactory.VoidDelegate(function (_)
        self.SettingPanel:SetActive(true)
    end)
    
    UIEventListener.Get(self.SettingHiddenButton).onClick = DelegateFactory.VoidDelegate(function (_)
        self.SettingPanel:SetActive(false)
    end)
    
    UIEventListener.Get(self.JumpButton.transform:Find("Tips").gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        --RefreshVariableUI
        if self.delayShowResult then
            UnRegisterTick(self.delayShowResult)
        end
        self.transform:Find("Anchor/Vfx_OpenBook").gameObject:SetActive(false)
        self.wndAniComp:Stop()
        self.wndAniComp:Play("tianshu2023openwnd_skip")
        self:RefreshVariableUI()
    end)

    UIEventListener.Get(self.SkipAnimationButton).onClick = DelegateFactory.VoidDelegate(function (_)
        self.skipAnimationValue = 1 - self.skipAnimationValue
        LuaTianShu2023OpenWnd.skipAnimation = (self.skipAnimationValue == 1)
        self.SkipAnimationButton.transform:Find("log").gameObject:SetActive(LuaTianShu2023OpenWnd.skipAnimation)

        local tbl = {skip_animation = self.skipAnimationValue}
        local json = luaJson.table2json(tbl)
        CPlayerDataMgr.Inst:SavePlayerData("tianshuOpen_defaultSetting",json)
    end)
end 

function LuaTianShu2023OpenWnd:OnClickOpenBtn()
    if CClientMainPlayer.Inst then
        if self.showTemplateId then
            return
        end
        
        local isFindBook, itemId, pos = self:TryGetFirstTianShu()
        if isFindBook then
            Gac2Gas.RequestUseItem(EnumItemPlace.Bag, pos, itemId, "")
        else
            local tianshu = Mall_LingYuMall.GetData(Constants.DunJiaTianShuTemplateId)
            if tianshu and tianshu.Jade <= CClientMainPlayer.Inst.Jade then
                Gac2Gas.OpenTianShuUseJade()
            else
                g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("JADE_PAY_NOTICE"), function ()
                    self:OnClickAddBtn()
                end, nil, LocalString.GetString("前往"), nil, false)
            end
        end
    end
end 

function LuaTianShu2023OpenWnd:TryGetFirstTianShu()
    if CClientMainPlayer.Inst then
        local itemProp = CClientMainPlayer.Inst.ItemProp
        local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)
        for i = 1, bagSize do
            local id = itemProp:GetItemAt(EnumItemPlace.Bag, i)
            if String.IsNullOrEmpty(id) then
                --do nothing
            else
                local item = CItemMgr.Inst:GetById(id)
                if item and item.IsItem and item.TemplateId == Constants.DunJiaTianShuTemplateId then
                    return true, id, i
                end
            end
        end
    end
    return false
end

function LuaTianShu2023OpenWnd:OnClickAddBtn()
    CShopMallMgr.ShowChargeWnd()
end 
