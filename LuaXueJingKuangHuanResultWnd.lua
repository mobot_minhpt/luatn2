local DelegateFactory  = import "DelegateFactory"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CUITexture = import "L10.UI.CUITexture"
local CUIFx = import "L10.UI.CUIFx"
local CBaseWnd = import "L10.UI.CBaseWnd"

LuaXueJingKuangHuanResultWnd = class()

RegistClassMember(LuaXueJingKuangHuanResultWnd, "m_RankLabel")
RegistClassMember(LuaXueJingKuangHuanResultWnd, "m_RankFx")
RegistClassMember(LuaXueJingKuangHuanResultWnd, "m_CurrentMaxFollowNumLabel")
RegistClassMember(LuaXueJingKuangHuanResultWnd, "m_HistoryMaxFollowNumLabel")

RegistClassMember(LuaXueJingKuangHuanResultWnd, "m_PlayerInfoRoot")

RegistClassMember(LuaXueJingKuangHuanResultWnd, "m_Buttons")
RegistClassMember(LuaXueJingKuangHuanResultWnd, "m_ShareButton")
RegistClassMember(LuaXueJingKuangHuanResultWnd, "m_AgainButton")
RegistClassMember(LuaXueJingKuangHuanResultWnd, "m_CloseButton")

function LuaXueJingKuangHuanResultWnd:Awake()
    self.m_RankLabel = self.transform:Find("Anchor/RankLabel"):GetComponent(typeof(UILabel))
    self.m_RankFx = self.transform:Find("Anchor/RankFx"):GetComponent(typeof(CUIFx))
    self.m_CurrentMaxFollowNumLabel = self.transform:Find("Anchor/CurrentMaxFollowNumLabel"):GetComponent(typeof(UILabel))
    self.m_HistoryMaxFollowNumLabel = self.transform:Find("Anchor/HistoryMaxFollowNumLabel"):GetComponent(typeof(UILabel))

    self.m_PlayerInfoRoot = self.transform:Find("Anchor/PlayerInfoNode")
    self.m_Buttons = self.transform:Find("Anchor/Buttons").gameObject
    self.m_ShareButton = self.transform:Find("Anchor/Buttons/ShareButton").gameObject
    self.m_AgainButton = self.transform:Find("Anchor/Buttons/AgainButton").gameObject
    self.m_CloseButton = self.transform:Find("Anchor/CloseBtn").gameObject

    UIEventListener.Get(self.m_ShareButton).onClick = DelegateFactory.VoidDelegate(function() self:OnShareButtonClick() end)
    UIEventListener.Get(self.m_AgainButton).onClick = DelegateFactory.VoidDelegate(function() self:OnAgainButtonClick() end)
    UIEventListener.Get(self.m_CloseButton).onClick = DelegateFactory.VoidDelegate(function() self:OnCloseButtonClick() end)
end

function LuaXueJingKuangHuanResultWnd:Init()
    --TODO cuifx
    local resultInfo = LuaHanJiaMgr.m_XueJingKuangHuanResultInfo
    local rank = resultInfo.rank
    CUICommonDef.SetGrey(self.m_RankLabel.gameObject, rank>3)  
    self.m_RankLabel.text = tostring(rank)
    self.m_CurrentMaxFollowNumLabel.text = tostring(resultInfo.finalLength)
    self.m_HistoryMaxFollowNumLabel.text = tostring(resultInfo.historyMaxLength)
    self:InitPlayerInfo()
end

function LuaXueJingKuangHuanResultWnd:OnEnable()
    g_ScriptEvent:AddListener("MainPlayerCreated", self, "OnMainPlayerCreated")
end

function LuaXueJingKuangHuanResultWnd:OnDisable()
    g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "OnMainPlayerCreated")
end

function LuaXueJingKuangHuanResultWnd:OnMainPlayerCreated()
    self:InitPlayerInfo()--切场景时无法正常赋值，这里补充监听消息来赋值
end

function LuaXueJingKuangHuanResultWnd:InitPlayerInfo()
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil then
        self.m_PlayerInfoRoot.gameObject:SetActive(false)
        return
    end
    self.m_PlayerInfoRoot.gameObject:SetActive(true)

    local portrait = self.m_PlayerInfoRoot:Find("Portrait"):GetComponent(typeof(CUITexture))
    local name = self.m_PlayerInfoRoot:Find("Name"):GetComponent(typeof(UILabel))
    local id = self.m_PlayerInfoRoot:Find("ID"):GetComponent(typeof(UILabel))
    local server = self.m_PlayerInfoRoot:Find("Server"):GetComponent(typeof(UILabel))
    local lv = self.m_PlayerInfoRoot:Find("Portrait/LvLabel"):GetComponent(typeof(UILabel))

    portrait:LoadNPCPortrait(mainplayer.PortraitName, false)
    name.text = mainplayer.Name
    server.text = mainplayer:GetMyServerName()
    id.text = tostring(mainplayer.Id)
    lv.text = CUICommonDef.GetMainPlayerColoredLevelString(lv.color)
end

function LuaXueJingKuangHuanResultWnd:OnShareButtonClick()
    CUICommonDef.CaptureScreen(
        "screenshot",
        true,
        false,
        self.m_Buttons,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            end
        ),
        false
    )
end

function LuaXueJingKuangHuanResultWnd:OnAgainButtonClick()
    CUIManager.CloseUI("XueJingKuangHuanResultWnd")
    LuaHanJiaMgr:OpenXueJingKuangHuanStartWnd()
end

function LuaXueJingKuangHuanResultWnd:OnCloseButtonClick()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

