local CZuoQiMgr=import "L10.Game.CZuoQiMgr"
local LingShou_TypeTemplate=import "L10.Game.LingShou_TypeTemplate"
local LingShou_LingShou = import "L10.Game.LingShou_LingShou"
local EPlayerFightProp= import "L10.Game.EPlayerFightProp"

local Skill_AllSkills=import "L10.Game.Skill_AllSkills"
--local LingShou_Quality=import "L10.Game.LingShou_Quality"
local LingShouBaby_BabySkillOfQuality=import "L10.Game.LingShouBaby_BabySkillOfQuality"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"

CLuaLingShouMgr={}
--todo 读表
CLuaLingShouMgr.MaxLingShouLevel = 150
CLuaLingShouMgr.LingShouLimitLevel = 130
CLuaLingShouMgr.LingShouLimitLevelMax = 170

CLuaLingShouMgr.ZhaoQinPlayerId = 0
CLuaLingShouMgr.ZhaoQinLingShouId = nil
CLuaLingShouMgr.ZhaoQinLingShouTemplateId = 0
CLuaLingShouMgr.ZhaoQinLingShouName = nil

CLuaLingShouMgr.BiLiuLuId = 21000592--碧柳露

CLuaLingShouMgr.m_GuideInfo = {}
CLuaLingShouMgr.m_GetSkin_SelectedPos=0

CLuaLingShouMgr.m_TempLingShouInfo = nil
CLuaLingShouMgr.m_IsMyLingShou = false

CLuaLingShouMgr.m_JieBanSkillBookLookup=nil
function CLuaLingShouMgr.IsJieBanSkillBook(id)
    if not CLuaLingShouMgr.m_JieBanSkillBookLookup then
        CLuaLingShouMgr.m_JieBanSkillBookLookup={}
        local setting = LingShouPartner_Setting.GetData()
        local bookList = setting.JieBanSkillBookList
        for i=1,bookList.Length do
            CLuaLingShouMgr.m_JieBanSkillBookLookup[bookList[i-1]] = true
        end
    end
    return CLuaLingShouMgr.m_JieBanSkillBookLookup[id] or false
end

function CLuaLingShouMgr.TryFeedLingShou()
    if not CClientMainPlayer.Inst then return end

    if not CClientMainPlayer.Inst.hasLingShou then
        g_MessageMgr:ShowMessage("You_Need_LingShou_Guide")
    else
        CLuaLingShouMainWnd.UseRenShenGuo()
    end
end

--战斗力公式
-- 1:灵兽等级
-- 2:灵兽品质
-- 3:技能满级比之和（各个技能/满级等级之和，主动和被动技能）
function CLuaLingShouMgr.BabyZhandouliFormula(level,quality,skillList)
    local getSkillParam=function(skillId)
        local ret=0
        if skillId>0 then
            local data=Skill_AllSkills.GetData(skillId)
            if data then
                local maxLevel=data.ID%100
                local baseSkillId=math.floor(data.ID/100)*100
                for j=maxLevel,100 do
                    local id=baseSkillId+j
                    local next=Skill_AllSkills.GetData(id)
                    if next then
                        maxLevel=next.Level
                    else
                        break
                    end
                end
                ret=data.Level/maxLevel
            end
        end
        return ret
    end
    local skillParam1=getSkillParam(skillList[1])
    local skillParam2=getSkillParam(skillList[2])

	local max = math.max
	local min = math.min
    local floor = math.floor

    local e={level,quality,skillParam1,skillParam2}
    local ret=GetFormula(465)(nil,nil,e)
    return ret
end
--灵兽最高品质加成气血
function CLuaLingShouMgr.GetMaxAdjHp(LSBBGrade)
	local max = math.max
	local min = math.min
    local floor = math.floor
    local power = function(a, b) return a^b end
   return floor((power(1.032,LSBBGrade-10)+LSBBGrade*0.261)*6.64*(0.5+0.5+0.5+0.5+2+2)*min(1,max(0,LSBBGrade-1)))
end

function CLuaLingShouMgr.GetBabyQualitySprite(quality)
    if quality==LuaEnumLingShouQuality.Ji then
        return "lingshou_ji"
    elseif quality==LuaEnumLingShouQuality.Wu then
        return "lingshou_wu"
    elseif quality==LuaEnumLingShouQuality.Ding then
        return "lingshou_ding"
    elseif quality==LuaEnumLingShouQuality.Bing then
        return "lingshou_bing"
    elseif quality==LuaEnumLingShouQuality.Yi  then
        return "lingshou_yi"
    elseif quality==LuaEnumLingShouQuality.Jia then
        return "lingshou_jia"
    end
    return "lingshou_ji"
end

function CLuaLingShouMgr.GetLingShouQuality(pinzhi)
    local lookup={} -- 1 2 3 4 5 6 
    LingShou_Quality.ForeachKey(function (key) 
        lookup[key] = LingShou_Quality.GetData(key).Quality
    end)
    
    for i=6,1,-1 do
        if lookup[i]<=pinzhi then
            return i
        end
    end

    return 1
end
function CLuaLingShouMgr.GetLingShouQualitySprite(quality)
    if quality==LuaEnumLingShouQuality.Ji then
        return "lingshou_ji"
    elseif quality==LuaEnumLingShouQuality.Wu then
        return "lingshou_wu"
    elseif quality==LuaEnumLingShouQuality.Ding then
        return "lingshou_ding"
    elseif quality==LuaEnumLingShouQuality.Bing then
        return "lingshou_bing"
    elseif quality==LuaEnumLingShouQuality.Yi  then
        return "lingshou_yi"
    elseif quality==LuaEnumLingShouQuality.Jia then
        return "lingshou_jia"
    end
    return "lingshou_ji"
end
--丁级或丁级以上 4个技能以上 悟性不为0 修为不为0
function CLuaLingShouMgr.IsSpecial(lingshou)--CLingShou
    local quality = CLuaLingShouMgr.GetLingShouQuality(lingshou.Props.Quality)
    local skillCount = 0
    local list=lingshou.Props.SkillListForSave
    for i=1,list.Length-1 do if list[i]>0 then skillCount=skillCount+1 end end

    local wuxing = lingshou.Props.Wuxing
    local xiuwei = lingshou.Props.Xiuwei

    return quality>=LuaEnumLingShouQuality.Ding or skillCount>=4 or wuxing>0 or xiuwei>0
end

function CLuaLingShouMgr.IsMorphed(lingshou)
    if lingshou.Props.TemplateIdForSkin~=0 and lingshou.Props.EvolveGradeForSkin~=0 then
        return true
    end
    return false
end
function CLuaLingShouMgr.GetMorphedName(lingshou)
    if CLuaLingShouMgr.IsMorphed(lingshou) then
        local templateId = lingshou.Props.TemplateIdForSkin
        local data = LingShou_LingShou.GetData(templateId)
        return data.Name
    else
        return nil
    end
end

function CLuaLingShouMgr.IsShenShou(lingshou) --CLingShou
    local data = LingShou_LingShou.GetData(lingshou.TemplateId)
    if data and data.ShenShou>0 then
        return true
    end
    return false
end
function CLuaLingShouMgr.GetLingShouSkillCount(lingshou) --CLingShou
    local skillCount = 0
    local list=lingshou.Props.SkillListForSave
    for i=1,list.Length-1 do if list[i]>0 then skillCount=skillCount+1 end end
    return skillCount
end

function CLuaLingShouMgr.GetLingShouType(templateId)
    local data = LingShou_LingShou.GetData(templateId)
    if data then
        local typeTemplate = LingShou_TypeTemplate.GetData(data.TypeTemplate)
        return typeTemplate.Type
    end
    return 0
end

function CLuaLingShouMgr.GetAttacks(details)
    local t={}
    local type = details.data.Props.Type
    if type == 1 or type==3 or type==5 then
        t[1] = math.floor(details.fightProperty:GetParam(EPlayerFightProp.pAttMin))
        t[2] = math.floor(details.fightProperty:GetParam(EPlayerFightProp.pAttMax))
    else
        t[1] = math.floor(details.fightProperty:GetParam(EPlayerFightProp.mAttMin))
        t[2] = math.floor(details.fightProperty:GetParam(EPlayerFightProp.mAttMax))
    end
    return t
end

function CLuaLingShouMgr.GetSkills(details)
    local t = {}
    local list = details.data.Props.SkillListForSave
    for i=1,8 do
        table.insert( t,tonumber(list[i]) )
    end
    return t
end

--计算灵兽战斗力
function CLuaLingShouMgr.GetLingShouZhandouli(data)--CLingShou
    local normalSkillCount = 0
    local advanceSkillCount = 0
    local list=data.Props.SkillListForSave
    for i=3,list.Length-1 do if list[i]>0 then normalSkillCount=normalSkillCount+1 end end
    for i=1,2 do if list[i]>0 then advanceSkillCount=advanceSkillCount+1 end end

    local mainZizhiFactor = 0
    local type = data.Props.Type
    if type == 1 then--力量型
        mainZizhiFactor = data.Props.StrFactor
    elseif type == 2 then--智力型
        mainZizhiFactor = data.Props.IntFactor
    elseif type==5 or type==6 then
        mainZizhiFactor = data.Props.AgiFactor
    else--根骨型宝宝
        mainZizhiFactor = data.Props.CorFactor
    end

    local function getParam(skillId,isadvance)
        if skillId>0 then 
            local template = Skill_AllSkills.GetData(skillId)
            if template then
                local maxLevel = template.ID % 100
                local baseSkillId = math.floor(template.ID/100)*100
                for j=maxLevel+1,100 do
                    local id = baseSkillId + j
                    local nexLevel = Skill_AllSkills.GetData(id)
                    if nexLevel then maxLevel = nexLevel.Level else break end
                end
                if isadvance then
                    return template.Level/math.max(maxLevel,3)
                else
                    return template.Level/maxLevel
                end
            end
        end
        return 0
    end

    local list=data.Props.SkillListForSave
    local normalSkillEffectParam=0
    for i=3,list.Length-1 do 
        normalSkillEffectParam = normalSkillEffectParam+getParam(list[i],false)
    end
    local advanceSkillEffectParam=0
    for i=1,2 do 
        advanceSkillEffectParam = advanceSkillEffectParam+getParam(list[i],true)
    end

    local val = AllFormulas.Action_Formula[173].Formula(nil, nil, {
        data.Level,
        data.Props.Chengzhang,
        mainZizhiFactor,
        data.Props.Wuxing,
        data.Props.Xiuwei,
        normalSkillCount,
        normalSkillEffectParam,
        advanceSkillCount,
        advanceSkillEffectParam,
        data.Props.ExPropertyItemUsedTime
    })

    if CZuoQiMgr.IsMapiEnable() then
        local list = CZuoQiMgr.Inst.zuoqis
        for i=1,list.Count do
            local v=list[i-1]
            if v.mapiInfo and (v.mapiInfo.ShouhuLingshou[1] == data.Id or v.mapiInfo.ShouhuLingshou[2] == data.Id) then
                val = val+v.lingfuPinfen
            end
        end
    end

    local babyInfo = data.Props.Baby
    if babyInfo.BornTime>0 then
        val = val+CLuaLingShouMgr.BabyZhandouliFormula(babyInfo.Level, babyInfo.Quality, babyInfo.SkillList)
    end
    return val
end

function CLuaLingShouMgr.GetLingShouPartnerZhandouli(lingshou,fightProperty,qichangId)

    local partnerInfo = lingshou.Props.PartnerInfo
    local grow = lingshou.Props.Chengzhang
    local affinity =partnerInfo.Affinity/10
    local normalSkillCount = 0
    local normalAvgLv = 0
    local skillList=partnerInfo.PartnerNormalSkills
    for i=1,5 do 
        if skillList[i]>0 then 
            normalSkillCount=normalSkillCount+1
            normalAvgLv = normalAvgLv+skillList[i]%100
        end 
    end
    normalAvgLv = normalAvgLv / normalSkillCount
    local profSkillCount = 0
    local profAvgLv = 0
    local skillList=partnerInfo.PartnerProfessionalSkills
    for i=1,3 do 
        if skillList[i]>0 then 
            profSkillCount=profSkillCount+1 
            profAvgLv = profAvgLv+skillList[i]%100
        end 
    end
    profAvgLv = profAvgLv / profSkillCount

    local lingshouProp = lingshou.Props
	local zizhi = fightProperty:GetParam(EPlayerFightProp.StrZizhi)
	zizhi = zizhi + fightProperty:GetParam(EPlayerFightProp.CorZizhi)
	zizhi = zizhi + fightProperty:GetParam(EPlayerFightProp.StaZizhi)
	zizhi = zizhi + fightProperty:GetParam(EPlayerFightProp.AgiZizhi)
	zizhi = zizhi + fightProperty:GetParam(EPlayerFightProp.IntZizhi)
	zizhi = zizhi / 5

    local qichangData = Baby_QiChang.GetData(qichangId)
    local qichangQuality = qichangData and qichangData.Quality or 0
    local value = math.floor(AllFormulas.Action_Formula[739].Formula(nil, nil,  {grow,zizhi, affinity, normalSkillCount, normalAvgLv, profSkillCount, profAvgLv, qichangQuality}))
    return value
end

function CLuaLingShouMgr.GetLingShouTypeName(type)
    if type == LuaEnumLingShouType.PAttack then
        return LocalString.GetString("力量型")
    elseif type == LuaEnumLingShouType.MAttack then
        return LocalString.GetString("智力型")
    elseif type == LuaEnumLingShouType.PCorDefend then
        return LocalString.GetString("根骨力量型")
    elseif type == LuaEnumLingShouType.MCorDefend then
        return LocalString.GetString("根骨智力型")
    elseif type == LuaEnumLingShouType.PAType then
        return LocalString.GetString("敏捷力量型")
    elseif type == LuaEnumLingShouType.MAType then
        return LocalString.GetString("敏捷智力型")
    end
end
function CLuaLingShouMgr.GetLingShouNatureName(type)
    if type == LuaEnumLingShouNature.Brave then
        return LocalString.GetString("英勇")
    elseif type == LuaEnumLingShouNature.Careful then
        return LocalString.GetString("谨慎")
    elseif type == LuaEnumLingShouNature.Smart then
        return LocalString.GetString("聪慧")
    elseif type == LuaEnumLingShouNature.Obstinate then
        return LocalString.GetString("倔强")
    elseif type == LuaEnumLingShouNature.Loyalty then
        return LocalString.GetString("忠心")
    end
end

function CLuaLingShouMgr.GetEvolveGradeDesc(level)
    if level == 1 then
        return LocalString.GetString("本初")
    elseif level==2 then
        return LocalString.GetString("一变")
    elseif level==3 then
        return LocalString.GetString("二变")
    elseif level==4 then
        return LocalString.GetString("三变")
    end
    return LocalString.GetString("本初")
end
function CLuaLingShouMgr.GetLingShouFullDesc(data)
    local template = LingShou_LingShou.GetData(data.TemplateId)
    if template then
        local name = template.Name
        local typeTemplate = LingShou_TypeTemplate.GetData(template.TypeTemplate)
        local desc = CLuaLingShouMgr.GetLingShouTypeName(typeTemplate.Type)

        if data.Props.TemplateIdForSkin ~= 0 and data.Props.EvolveGradeForSkin~=0 then
            local morphTemplate = LingShou_LingShou.GetData(data.Props.TemplateIdForSkin)
            local morphName = morphTemplate and morphTemplate.Name
            return SafeStringFormat3(LocalString.GetString("[ffba00]性格%s %s 易容为%s[-]"),CLuaLingShouMgr.GetLingShouNatureName(data.Props.Nature),desc,morphName)
        else
            return SafeStringFormat3(LocalString.GetString("[ffba00]性格%s %s[-]"),CLuaLingShouMgr.GetLingShouNatureName(data.Props.Nature),desc)
        end
    end
end

function CLuaLingShouMgr.GetLSBBAdjHp(LSBBGrade,LSBBQuality)
	local max = math.max
	local min = math.min
	local floor = math.floor

	local power = function(a, b) return a^b end

    return floor((power(1.032,LSBBGrade-10)+LSBBGrade*0.261-(LSBBGrade-150)*min(1,max(0,LSBBGrade-150)))*6.64*(0.5+min(1,max(0,LSBBQuality-1))*0.5+min(1,max(0,LSBBQuality-2))*0.5+min(1,max(0,LSBBQuality-3))*0.5+min(1,max(0,LSBBQuality-4))*2+min(1,max(0,LSBBQuality-5))*2)*min(1,max(0,LSBBGrade-1)))
end
function CLuaLingShouMgr.ProcessSkillId(quality,id)
    local level=id%100
    if quality<=5 then
        local maxUpgradeLv=LingShouBaby_BabySkillOfQuality.GetData(quality).MaxSkillLv
        if level>maxUpgradeLv then
            return math.floor(id/100)*100+maxUpgradeLv
        end
    end
    return id
end

--灵兽进阶培养
CLuaLingShouMgr.TrainConditionLevel = nil
function CLuaLingShouMgr.GetTrainConditionLevel()
    --海外版本频闭成长道具属性道具
    if CommonDefs.IS_VN_CLIENT or CommonDefs.IS_HMT_CLIENT then
        return CLuaLingShouMgr.LingShouLimitLevel
    end

    if CLuaLingShouMgr.TrainConditionitemLevel then 
        return CLuaLingShouMgr.TrainConditionLevel 
    end
    local conditionLevel
    LingShou_ExItemUseTime.Foreach(function(level,data)
        if not conditionLevel then
            conditionLevel = level
        end
        if level < conditionLevel then
            conditionLevel = level
        end
    end)
    CLuaLingShouMgr.TrainConditionLevel = conditionLevel
    return CLuaLingShouMgr.TrainConditionLevel
end

function CLuaLingShouMgr.GetMaxExChengzhangItemUsedCount(myLevel)
    local count = LingShou_ExItemUseTime.GetData(myLevel).ChengzhangUseTime
    return count
end

function CLuaLingShouMgr.GetMaxExPropsItemUsedCount(myLevel)
    local count = LingShou_ExItemUseTime.GetData(myLevel).PropsUseTime
    return count
end
