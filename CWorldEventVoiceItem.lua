-- Auto Generated!!
local CWorldEventVoiceItem = import "L10.UI.CWorldEventVoiceItem"
local Object = import "System.Object"
local QnButton = import "L10.UI.QnButton"
local SoundManager = import "SoundManager"
local Vector3 = import "UnityEngine.Vector3"
CWorldEventVoiceItem.m_OnPlayButtonClick_CS2LuaHook = function (this, button)
    if this.m_IsPlaying then
        this:StopPlay()
    else
        this:StartPlay()
    end
end
CWorldEventVoiceItem.m_StartPlay_CS2LuaHook = function (this)
    if not this.m_IsPlaying then
        this.m_IsPlaying = true
        this.m_Sound = SoundManager.Inst:PlayOneShot(this.m_FmodEvent, Vector3.zero, nil, 0)
        this.m_PlayButton.m_BackGround.spriteName = CWorldEventVoiceItem.s_StopSpriteName
        SoundManager.Inst:ChangeBgMusicVolume("worldeventvoice", 0.5, false)
        if this.OnPlayStart ~= nil then
            GenericDelegateInvoke(this.OnPlayStart, Table2ArrayWithCount({this.Row}, 1, MakeArrayClass(Object)))
        end
    end
end
CWorldEventVoiceItem.m_StopPlay_CS2LuaHook = function (this)
    if this.m_IsPlaying then
        this.m_IsPlaying = false
        this.m_PlayButton.m_BackGround.spriteName = CWorldEventVoiceItem.s_PlaySpriteName
        SoundManager.Inst:StopSound(this.m_Sound)
        this.m_Sound = nil
        SoundManager.Inst:ChangeBgMusicVolume("worldeventvoice", 0.5, true)
    end
end
CWorldEventVoiceItem.m_UpdateData_CS2LuaHook = function (this, text, voice, portrait)
    this.m_PlayButton.OnClick = MakeDelegateFromCSFunction(this.OnPlayButtonClick, MakeGenericClass(Action1, QnButton), this)
    this.m_TextLabel.text = text
    this.m_FmodEvent = voice
    --没有声音的字体
    if System.String.IsNullOrEmpty(this.m_FmodEvent) then
        this.m_PlayButton.Visible = false
    else
        this.m_PlayButton.Visible = true
    end
    if this.m_PortraitTex ~= nil and not System.String.IsNullOrEmpty(portrait) then
        this.m_PortraitTex:LoadMaterial(portrait, false)
    end
end
