local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"
local LuaUtils = import "LuaUtils"
local DelegateFactory = import "DelegateFactory"
local UIEventListener = import "UIEventListener"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local Extensions = import "Extensions"
local TweenAlpha = import "TweenAlpha"
local QnButton = import "L10.UI.QnButton"
local UILabel = import "UILabel"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local NGUIText = import "NGUIText"
local YuanDan2019_ItemLuckyDraw = import "L10.Game.YuanDan2019_ItemLuckyDraw"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"

LuaNewYear2019DrawWnd = class()
RegistClassMember(LuaNewYear2019DrawWnd, "m_TemplateId2Order")
RegistClassMember(LuaNewYear2019DrawWnd, "m_Speed")
RegistClassMember(LuaNewYear2019DrawWnd, "m_DeltaSpeed")
RegistClassMember(LuaNewYear2019DrawWnd, "m_EndSpeed")
RegistClassMember(LuaNewYear2019DrawWnd, "m_TargetAngle")
RegistClassMember(LuaNewYear2019DrawWnd, "m_CurrentAngle")
RegistClassMember(LuaNewYear2019DrawWnd, "m_WheelTrans")
RegistClassMember(LuaNewYear2019DrawWnd, "m_LightImgObj")
RegistClassMember(LuaNewYear2019DrawWnd, "m_ShowRootObj")
RegistClassMember(LuaNewYear2019DrawWnd, "m_RoundCount")
RegistClassMember(LuaNewYear2019DrawWnd, "m_DrawQnButton")
RegistClassMember(LuaNewYear2019DrawWnd, "m_HasRotated")
RegistClassMember(LuaNewYear2019DrawWnd, "m_ItemWndHandle")
RegistClassMember(LuaNewYear2019DrawWnd, "m_RewardId")

function LuaNewYear2019DrawWnd:Awake()
	-- show all when animation finished
	self.m_ShowRootObj = self.transform:Find("ShowRoot").gameObject
	self.m_ShowRootObj:SetActive(false)
	CommonDefs.AddEventDelegate(self.transform:Find("BG"):GetComponent(typeof(TweenAlpha)).onFinished, DelegateFactory.Action(function ( ... )
		self.m_ShowRootObj:SetActive(true)
	end))
	self.m_DrawQnButton = self.m_ShowRootObj.transform:Find("DrawBtn"):GetComponent(typeof(QnButton))
end

function LuaNewYear2019DrawWnd:GetRandom_Table(t,num)
  	for i,v in pairs(t) do
			local r = math.random(#t)
			local temp = t[i]
			t[i] = t[r]
			t[r] = temp
		end
		num = num or #t
		local returnTable = {}
		for i = 1,num do
			table.insert(returnTable,t[i])
		end
		return returnTable
end

function LuaNewYear2019DrawWnd:GetItemTable()
	if LuaNewYear2019Mgr.SelectedItemId then
		local itemcount = YuanDan2019_ItemLuckyDraw.GetDataCount()
		local itemTable = {}
		local selectedItemData = nil
		local chooseTempTable = {}

		for i=1,itemcount do
			local itemData = YuanDan2019_ItemLuckyDraw.GetData(i)
			if itemData then
				if itemData.ItemId == LuaNewYear2019Mgr.SelectedItemId then
					selectedItemData = itemData
					chooseTempTable[selectedItemData.ClientItem] = true
					break
				end
			end
		end

		for i=1,itemcount do
			local itemData = YuanDan2019_ItemLuckyDraw.GetData(i)
			if itemData then

				if not chooseTempTable[itemData.ClientItem] then
					if itemData.ItemId ~= LuaNewYear2019Mgr.SelectedItemId then
						table.insert(itemTable,itemData)
					end
					chooseTempTable[itemData.ClientItem] = true
				end
			end
		end

		math.randomseed(os.time())
		local returnTable = self:GetRandom_Table(itemTable, 7)
		table.insert(returnTable,math.random(#returnTable),selectedItemData)

		return returnTable
	end
end

function LuaNewYear2019DrawWnd:Init()
	self.m_Speed = 20
	self.m_TargetAngle = 1
	self.m_CurrentAngle = 0
	self.m_DeltaSpeed = 0.8
	self.m_RoundCount = 2
	self.m_HasRotated = false

	self.m_WheelTrans = self.transform:Find("BG/Wheel")
	local itemRootTrans = self.transform:Find("BG/Wheel/ItemRoot")
	local index = 1
	self.m_TemplateId2Order = {}
	local prize = self:GetItemTable()

	for k, v in pairs(prize) do
		self.m_TemplateId2Order[v.ItemId] = k
		local itemObj = itemRootTrans:Find("Texture"..k).gameObject
		local cuiTexture = itemObj:GetComponent(typeof(CUITexture))
		local itemData = Item_Item.GetData(v.ClientItem)
		cuiTexture:LoadMaterial(itemData.Icon)
		UIEventListener.Get(itemObj).onClick = LuaUtils.VoidDelegate(function ( ... )
			CItemInfoMgr.ShowLinkItemTemplateInfo(k, false, nil, AlignType2.Right, 0, 0, 0, 0)
		end)
		itemObj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = v.Number
	end
	local drawBtnTrans = self.transform:Find("ShowRoot/DrawBtn")
	UIEventListener.Get(drawBtnTrans.gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		--self:SyncDrawIndex(LuaNewYear2019Mgr.SelectedItemId)
		local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, LuaNewYear2019Mgr.PackItemId)
		Gac2Gas.NewYearLuckyDrawRequestDraw(LuaNewYear2019Mgr.PackItemId,EnumItemPlace.Bag,pos)
	end)

	self.m_LightImgObj = self.m_ShowRootObj.transform:Find("LightImg").gameObject
	self.m_LightImgObj:SetActive(false)

	local titleLabel = self.m_ShowRootObj.transform:Find("Title").gameObject:GetComponent(typeof(UILabel))
	--titleLabel.text = SafeStringFormat3(LocalString.GetString("集齐%s枚印章可以参与抽奖"), LuaCarnivalCollectMgr.m_DrawTotalStampCount)
	titleLabel.text = ''
	local drawQnButton = drawBtnTrans.gameObject:GetComponent(typeof(QnButton))
	--drawQnButton.Enabled = LuaCarnivalCollectMgr.m_DrawStampCount >= LuaCarnivalCollectMgr.m_DrawTotalStampCount
	drawQnButton.Enabled = true
end

function LuaNewYear2019DrawWnd:DetectItemWnd()
	if not self.m_ItemWndHandle then
		if CUIManager.IsLoaded(CIndirectUIResources.ItemAvailableWnd) then
			self.m_ItemWndHandle = CUIManager.instance.loadedUIs[CIndirectUIResources.ItemAvailableWnd]
		end
		return
	end
	self.m_ItemWndHandle:SetActive(self.m_HasRotated)
	if self.m_HasRotated then self.m_ItemWndHandle = nil end
end

function LuaNewYear2019DrawWnd:Update( ... )
	self:DetectItemWnd()

	if self.m_TargetAngle >= 1 then return end
	self.m_CurrentAngle = self.m_CurrentAngle - self.m_Speed
	self.m_Speed = self.m_Speed - self.m_DeltaSpeed

	if self.m_CurrentAngle <= self.m_TargetAngle then
		self.m_CurrentAngle = self.m_TargetAngle
		self.m_TargetAngle = 1
		self:EndRotate()
	end
	Extensions.SetLocalRotationZ(self.m_WheelTrans, self.m_CurrentAngle)
end

function LuaNewYear2019DrawWnd:EndRotate()
	self.m_LightImgObj:SetActive(true)
	self.m_HasRotated = true
	local data = Item_Item.GetData(self.m_RewardId)
	if data then
		g_MessageMgr:ShowMessage("OBTAIN_COMMON_ITEM", NGUIText.EncodeColor(GameSetting_Common_Wapper.Inst:GetColor(data.NameColor)), data.Name)
	end
end

function LuaNewYear2019DrawWnd:SyncDrawIndex()
	-- Find Index From Item Template Id
	local templateId = LuaNewYear2019Mgr.SelectedItemId
	local index = self.m_TemplateId2Order[templateId] or 0
	if index <= 0 then
		return
	end
	self.m_RewardId = templateId
	self.m_DrawQnButton.Enabled = false
	self.m_TargetAngle = -45 * (index - 1) - self.m_RoundCount * 360
	local n = 100
	self.m_DeltaSpeed = math.abs(self.m_TargetAngle) * 2 / (n * (n + 1))
	self.m_Speed = (n + 1) * self.m_DeltaSpeed
end

function LuaNewYear2019DrawWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("SyncNewYearPrizeDrawIndex", self, "SyncDrawIndex")
end

function LuaNewYear2019DrawWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("SyncNewYearPrizeDrawIndex", self, "SyncDrawIndex")
end

function LuaNewYear2019DrawWnd:OnDestroy( ... )
	if self.m_ItemWndHandle then
		self.m_ItemWndHandle:SetActive(true)
	end
end

return LuaNewYear2019DrawWnd
