-- Auto Generated!!
local CDuoBaoMgr = import "L10.UI.CDuoBaoMgr"
local CDuoBaoPreviewer = import "L10.UI.CDuoBaoPreviewer"
local EGlobalDuoBaoStatus = import "L10.UI.EGlobalDuoBaoStatus"
local LocalString = import "LocalString"
CDuoBaoPreviewer.m_OnReplyCurrentRoundDuoBaoInfo_CS2LuaHook = function (this, duobaoId) 
    if this.m_DuobaoInfo ~= nil and this.m_DuobaoInfo.ID == duobaoId then
        this:UpdateData(CDuoBaoMgr.Inst.GlobalDuoBaoStatus, this.m_DuobaoInfo)
    end
end
CDuoBaoPreviewer.m_UpdateData_CS2LuaHook = function (this, status, info) 
    this.m_DuobaoInfo = info
    if info ~= nil then
        this.m_YiKouJiaObj:SetActive(info.Need_FenShu == 1)
        this.m_CrossServerObj.gameObject:SetActive(info.IsCrossServer ~= 0)
        this.m_ItemNameLabel.text = info.ItemName
        repeat
            local default = status
            if default == EGlobalDuoBaoStatus.NotStarted or default == EGlobalDuoBaoStatus.DuoBaoView then
                this:UpdatePreview_NotStarted(info)
                if status == EGlobalDuoBaoStatus.NotStarted then
                    this.m_PrepareLabel.text = LocalString.GetString("活动已结束")
                else
                    this.m_PrepareLabel.text = LocalString.GetString("准备中")
                end
                break
            elseif default == EGlobalDuoBaoStatus.DuoBaoing or default == EGlobalDuoBaoStatus.DuoBaoEnd then
                this:UpdatePreview_DuoBaoing(info)
                break
            end
        until 1
    else
        this:UpdatePreview_NotStarted(nil)
    end
end
CDuoBaoPreviewer.m_UpdatePreview_NotStarted_CS2LuaHook = function (this, info) 
    this.m_ProgressBar.gameObject:SetActive(false)
    this.m_CurrentProgressLabel.gameObject:SetActive(false)
    this.m_CurrentSoldOutLabel.gameObject:SetActive(false)

    this.m_SoldOutLabel.gameObject:SetActive(false)
    this.m_SellingInfoLabel.transform.parent.gameObject:SetActive(false)
    this.m_SellingMyInvest.transform.parent.gameObject:SetActive(false)
    this.m_SellingMyInvestTotal.transform.parent.gameObject:SetActive(false)

    this.m_PrepareLabel.gameObject:SetActive(true)
end
CDuoBaoPreviewer.m_UpdatePreview_DuoBaoing_CS2LuaHook = function (this, info) 
    this.m_ProgressBar.gameObject:SetActive(true)
    this.m_ProgressBar:SetProgress(1 * info.CurrentGroupProgress / info.Need_FenShu)
    this.m_ProgressBar:SetText(System.String.Format("{0}/{1}", info.CurrentGroupProgress, info.Need_FenShu))

    --该物品已完成
    if info.IsSoldOut then
        this.m_ProgressBar:SetGray(true)
        this.m_CurrentProgressLabel.gameObject:SetActive(false)
        this.m_CurrentSoldOutLabel.gameObject:SetActive(true)
        this.m_CurrentSoldOutLabel.text = System.String.Format(LocalString.GetString("已售完(共{0}组)"), info.GroupCount)
        this.m_SoldOutLabel.gameObject:SetActive(true)
        this.m_SoldOutLabel.text = LocalString.GetString("全部夺宝结束，结果已揭晓")

        this.m_SellingInfoLabel.transform.parent.gameObject:SetActive(false)
        this.m_SellingMyInvest.transform.parent.gameObject:SetActive(false)
        this.m_SellingMyInvestTotal.transform.parent.gameObject:SetActive(false)
        this.m_SellingMyInvestTotal.text = System.String.Format(LocalString.GetString("{0}组(共{1}份)"), info.MyInvestInTotalGroup, info.MyInvestInTotalFenShu)
        this.m_Table:Reposition()
    else
        this.m_ProgressBar:SetGray(false)
        this.m_CurrentProgressLabel.gameObject:SetActive(true)
        this.m_CurrentProgressLabel.text = System.String.Format(LocalString.GetString("已被投入{0}份，本组共{1}份"), info.CurrentGroupProgress, info.Need_FenShu)
        this.m_CurrentSoldOutLabel.gameObject:SetActive(false)
        this.m_SoldOutLabel.gameObject:SetActive(false)

        this.m_SellingInfoLabel.transform.parent.gameObject:SetActive(true)
        this.m_SellingInfoLabel.text = System.String.Format(LocalString.GetString("第{0}组(共{1}组)"), info.CurrentGroupIndex, info.GroupCount)
        this.m_SellingMyInvest.transform.parent.gameObject:SetActive(true)
        this.m_SellingMyInvest.text = System.String.Format(LocalString.GetString("{0}份(最多{1}份)"), info.MyInvestInCurrentGroup, math.floor(info.Buy_Max))
        this.m_SellingMyInvestTotal.transform.parent.gameObject:SetActive(true)
        this.m_SellingMyInvestTotal.text = System.String.Format(LocalString.GetString("{0}组(共{1}份)"), info.MyInvestInTotalGroup, info.MyInvestInTotalFenShu)
        this.m_Table.repositionNow = true
    end
    this.m_PrepareLabel.gameObject:SetActive(false)
end
