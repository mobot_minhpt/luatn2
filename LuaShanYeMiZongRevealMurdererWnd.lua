local GameObject = import "UnityEngine.GameObject"
local CUIFx = import "L10.UI.CUIFx"
local UILabel = import "UILabel"
local QnButton = import "L10.UI.QnButton"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaShanYeMiZongRevealMurdererWnd = class()
LuaShanYeMiZongRevealMurdererWnd.s_bIsRevealMurderer = false
LuaShanYeMiZongRevealMurdererWnd.s_ShowRevealFx = false

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShanYeMiZongRevealMurdererWnd, "Profile", "Profile", GameObject)
RegistChildComponent(LuaShanYeMiZongRevealMurdererWnd, "RealMurderer", "RealMurderer", GameObject)
RegistChildComponent(LuaShanYeMiZongRevealMurdererWnd, "RevealingFx", "RevealingFx", CUIFx)
RegistChildComponent(LuaShanYeMiZongRevealMurdererWnd, "Label", "Label", UILabel)
RegistChildComponent(LuaShanYeMiZongRevealMurdererWnd, "OkButton", "OkButton", QnButton)

--@endregion RegistChildComponent end

function LuaShanYeMiZongRevealMurdererWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)


    --@endregion EventBind end
end

function LuaShanYeMiZongRevealMurdererWnd:Init()
    if LuaShanYeMiZongRevealMurdererWnd.s_bIsRevealMurderer then
        if LuaShanYeMiZongRevealMurdererWnd.s_ShowRevealFx then
            self.RevealingFx:LoadFx("Fx/UI/Prefab/UI_Shanye_zhenxiong.prefab")
            LuaShanYeMiZongRevealMurdererWnd.s_ShowRevealFx = false
        end
        self.Profile:SetActive(false)
        self.RealMurderer:SetActive(true)
        self.Label.gameObject:SetActive(false)
        self.OkButton.gameObject:SetActive(true)
    end
end

--@region UIEvent

function LuaShanYeMiZongRevealMurdererWnd:OnOkButtonClick()
    --开启还魂山庄

    CUIManager.CloseUI(CLuaUIResources.ShanYeMiZongRevealMurdererWnd)
    CUIManager.CloseUI(CLuaUIResources.ShanYeMiZongMainWnd)

    CTrackMgr.Inst:FindNPC(20006947, 16000003, 94, 68)
end


--@endregion UIEvent

