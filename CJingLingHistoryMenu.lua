-- Auto Generated!!
local CJingLingHistoryMenu = import "L10.UI.CJingLingHistoryMenu"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumJingLingParamReplaceRule = import "L10.Game.EnumJingLingParamReplaceRule"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local NGUIText = import "NGUIText"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CJingLingHistoryMenu.m_Init_CS2LuaHook = function (this) 

    local questions = CJingLingMgr.Inst.ReservedQuestions
    Extensions.RemoveAllChildren(this.table.transform)
    CommonDefs.ListClear(this.btns)
    do
        local i = 0
        while i < questions.Count do
            local btn = CUICommonDef.AddChild(this.table.gameObject, this.btnTemplate)
            btn:SetActive(true)
            local label = CommonDefs.GetComponentInChildren_GameObject_Type(btn, typeof(UILabel))
            local originText = NGUIText.StripSymbols(questions[i])
            originText = CJingLingMgr.Inst:RemoveCompassParams(originText)
            if not CJingLingMgr.Inst.EnableShowParamsInQueryRecord then
                originText = CJingLingMgr.Inst:ReplaceParams(originText, EnumJingLingParamReplaceRule.ReplaceByEmpty)
            end

            originText = CJingLingMgr.Inst:FilterQueryPrefix(originText)
            local outerText = System.String.Empty
            label.text = originText
            --赋值对label各项参数进行初始化，否则下面的计算会出问题，label类型需要clamp content， maxlines = 1
            local default
            default, outerText = label:Wrap(originText)
            if not default then
                outerText = CommonDefs.StringSubstring2(outerText, 0, CommonDefs.StringLength(outerText) - 1) .. "..."
                label.text = outerText
            end
            UIEventListener.Get(btn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(btn).onClick, MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this), true)
            CommonDefs.ListAdd(this.btns, typeof(GameObject), btn)
            i = i + 1
        end
    end
    this.table:Reposition()
    this.scrollView:ResetPosition()
end
CJingLingHistoryMenu.m_OnItemClick_CS2LuaHook = function (this, go) 

    do
        local i = 0
        while i < this.btns.Count do
            if this.btns[i] == go then
                if this.OnItemSelected ~= nil then
                    GenericDelegateInvoke(this.OnItemSelected, Table2ArrayWithCount({i}, 1, MakeArrayClass(Object)))
                end
                break
            end
            i = i + 1
        end
    end
end
