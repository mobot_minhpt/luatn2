local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local QnButton = import "L10.UI.QnButton"
local CButton = import "L10.UI.CButton"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local AlignType = import "L10.UI.CTooltip.AlignType"
local WndType = import "L10.UI.WndType"
local StringBuilder = import "System.Text.StringBuilder"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"
LuaDaFuWongBuyLandWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongBuyLandWnd, "LandTexture", "LandTexture", CUITexture)
RegistChildComponent(LuaDaFuWongBuyLandWnd, "LandName", "LandName", UILabel)
RegistChildComponent(LuaDaFuWongBuyLandWnd, "Cost", "Cost", UILabel)
RegistChildComponent(LuaDaFuWongBuyLandWnd, "Type", "Type", UILabel)
RegistChildComponent(LuaDaFuWongBuyLandWnd, "Des", "Des", UILabel)
RegistChildComponent(LuaDaFuWongBuyLandWnd, "TipButton", "TipButton", QnButton)
RegistChildComponent(LuaDaFuWongBuyLandWnd, "MyMoneyLabel", "MyMoneyLabel", UILabel)
RegistChildComponent(LuaDaFuWongBuyLandWnd, "BuyBtn", "BuyBtn", CButton)
RegistChildComponent(LuaDaFuWongBuyLandWnd, "InCome", "InCome", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongBuyLandWnd, "m_BuyLand")
RegistClassMember(LuaDaFuWongBuyLandWnd, "m_LandData")
RegistClassMember(LuaDaFuWongBuyLandWnd, "m_CloseBtn")
function LuaDaFuWongBuyLandWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.BuyBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuyBtnClick()
	end)


    --@endregion EventBind end
	self.m_CloseBtn = self.transform:Find("Wnd_Bg_Secondary_2/CloseButton").gameObject
	UIEventListener.Get(self.m_CloseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseBtnClick()
	end)
end

function LuaDaFuWongBuyLandWnd:Init()
	if not LuaDaFuWongMgr.CurLandId then return end
	self.m_BuyLand = LuaDaFuWongMgr.CurLandId
	self.m_LandData = DaFuWeng_Land.GetData(self.m_BuyLand)
	if not self.m_LandData then return end
	local id = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id
	local playerInfo = LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[id]
	if playerInfo then
		self.MyMoneyLabel.text = playerInfo.Money
	else
		self.MyMoneyLabel.text = "0"
	end
	
	self:InitIcon()
	self:InitDes()
	self:InitLv()
	local countDown = DaFuWeng_Countdown.GetData(EnumDaFuWengStage.RoundTrade).Time
	local endFunc = function() CUIManager.CloseUI(CLuaUIResources.DaFuWongBuyLandWnd) end
	local durationFunc = function(time) end
	LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.BuyLand,countDown,durationFunc,endFunc)
end

function LuaDaFuWongBuyLandWnd:InitLv()
	local data = LuaDaFuWongMgr.LandInfo[self.m_BuyLand]
	local lv = data.lv
	local lvView = self.transform:Find("Anchor/Left/lv")
	for i = 0,lvView.transform.childCount - 1 do
		local go = lvView.transform:GetChild(i)
		local highlight = go.transform:Find("highlight").gameObject
        highlight.gameObject:SetActive(true)
		if i >= lv then
			highlight.gameObject:SetActive(false)
		end
		-- if go then
		-- 	go:GetComponent(typeof(UISprite)).spriteName = name
		-- end
	end
end

function LuaDaFuWongBuyLandWnd:InitIcon()
	self.LandName.text = self.m_LandData.Name
	self.LandTexture:LoadMaterial(self.m_LandData.Icon)
end

function LuaDaFuWongBuyLandWnd:InitDes()
	local landData = LuaDaFuWongMgr.LandInfo and LuaDaFuWongMgr.LandInfo[self.m_BuyLand]
	local seriesData = nil
	if landData then
		self.Cost.text = LuaDaFuWongMgr:GetCutLandCostByLv(self.m_BuyLand,landData.lv)
		self.InCome.text = LuaDaFuWongMgr:GetCutLandPriceByLv(self.m_BuyLand,landData.lv + 1)
		if LuaDaFuWongMgr:GetCutLandPriceByLv(self.m_BuyLand,landData.lv + 1) <= 0 then
			self.InCome.text = LocalString.GetString("已满级")
		end
		seriesData = DaFuWeng_Series.GetData(landData.series)
		self.Des.text = SafeStringFormat3("[4D4D4D]%s[-]", g_MessageMgr:Format(self.m_LandData.Describe)) 
	else
		self.Cost.text = self.m_LandData.Cost[0]
		self.InCome.text = self.m_LandData.Price[1]
	end
	if seriesData then
		self.Type.text = seriesData.Name
		self.TipButton.gameObject:SetActive(true)
	else
		self.Type.text = LocalString.GetString("无")
		self.TipButton.gameObject:SetActive(false)
	end
	self.BuyBtn.transform:Find("Sprite/CostMoney"):GetComponent(typeof(UILabel)).text = LuaDaFuWongMgr.LandInfo[self.m_BuyLand].curPrice
end

-- function LuaDaFuWongBuyLandWnd:GetTipsString()
-- 	local result = CreateFromClass(MakeGenericClass(List, String))
-- 	local Land = LuaDaFuWongMgr.LandInfo and LuaDaFuWongMgr.LandInfo[self.m_BuyLand]
-- 	local series = Land  and Land.series
-- 	local seriesData = LuaDaFuWongMgr.SeriesInfo[series]
-- 	if not seriesData then return result end
-- 	local seriesList = NewStringBuilderWraper(StringBuilder)
-- 	for i = 0,seriesData.lands.Length - 1 do
-- 		local landdata = DaFuWeng_Land.GetData(seriesData.lands[i])
-- 		if landdata then
-- 			if seriesData.lands.Length > 1 and i == seriesData.lands.Length - 1 then seriesList:Append(LocalString.GetString("和"))
-- 			elseif i ~= 0 then seriesList:Append(LocalString.GetString("、"))  end
-- 			seriesList:Append(landdata.Name)
-- 		end
-- 	end
-- 	local seriesDes = seriesData.desc
-- 	local curSeries = NewStringBuilderWraper(StringBuilder)
-- 	local id = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id
-- 	local playData = LuaDaFuWongMgr.PlayerInfo[id]
-- 	if playData then
-- 		local count = 0
-- 		for k,v in pairs(playData.LandsInfo) do
-- 			if v then
-- 				local otherland = LuaDaFuWongMgr.LandInfo and LuaDaFuWongMgr.LandInfo[k]
-- 				local data = DaFuWeng_Land.GetData(k)
-- 				if data and otherland and otherland.series == Land.series then
-- 					count = count + 1
-- 					if count > 1 then curSeries:Append(LocalString.GetString("、"))  end
-- 					curSeries:Append(data.Name)
-- 				end
-- 			end
-- 		end
-- 		if count <= 0 then
-- 			curSeries:Append(LocalString.GetString("无"))
-- 		end
-- 	end

-- 	local firstP = SafeStringFormat3(LocalString.GetString("%s系列的地块包括了：%s。%s"),seriesData.name,seriesList:ToString(),seriesDes)
-- 	local secondP = SafeStringFormat3(LocalString.GetString("当前拥有%s地块：%s"),seriesData.name,curSeries:ToString())
-- 	CommonDefs.ListAdd(result, typeof(String), firstP)
-- 	CommonDefs.ListAdd(result, typeof(String), secondP)
-- 	return result
-- end
--@region UIEvent

function LuaDaFuWongBuyLandWnd:OnTipButtonClick()
	local tip = LuaDaFuWongMgr:GetTipsString(self.m_BuyLand)
	CMessageTipMgr.Inst:Display(WndType.Tooltip, false, "", tip, 700, self.TipButton.transform, AlignType.Right, 4294967295)
end

function LuaDaFuWongBuyLandWnd:OnBuyBtnClick()
	-- RPC
	Gac2Gas.DaFuWengLandTrade(true)
end
function LuaDaFuWongBuyLandWnd:OnCloseBtnClick()
	Gac2Gas.DaFuWengFinishCurStage(EnumDaFuWengStage.RoundTrade)
	CUIManager.CloseUI(CLuaUIResources.DaFuWongBuyLandWnd)
end
--@endregion UIEvent

function LuaDaFuWongBuyLandWnd:OnStageUpdate(CurStage)
	if CurStage ~= EnumDaFuWengStage.RoundTrade then
        CUIManager.CloseUI(CLuaUIResources.DaFuWongBuyLandWnd)
    end
end

function LuaDaFuWongBuyLandWnd:OnDisable()
	LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.BuyLand)
	g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
end

function LuaDaFuWongBuyLandWnd:OnEnable()
	g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
end