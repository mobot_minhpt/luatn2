local CGuildMgr=import "L10.Game.CGuildMgr"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"

CLuaRedDotForGuild = class()
RegistChildComponent(CLuaRedDotForGuild, "m_RedDot","RedDot", GameObject)
RegistChildComponent(CLuaRedDotForGuild, "m_RedDot_Welfare","RedDot_Welfare", GameObject)
RegistChildComponent(CLuaRedDotForGuild, "m_RedDot_Member","RedDot_Member", GameObject)

CLuaRedDotForGuild.BonusAvaliable = false
CLuaRedDotForGuild.Bonus = 0
CLuaRedDotForGuild.IsThisWeekTaken = false
CLuaRedDotForGuild.Status = 0

function CLuaRedDotForGuild:Awake()
    --不关注分红的不需要请求分红
    if self.m_RedDot or self.m_RedDot_Welfare then
        Gac2Gas.GetSelfBuildBonus()
    end

    if CClientMainPlayer.Inst and CClientMainPlayer.Inst:IsInGuild() then
        CGuildMgr.Inst.guildNotification = PlayerPrefs.GetInt(CGuildMgr.Inst.GuildKey)>0
    else
        CGuildMgr.Inst.guildNotification=false
    end
    self:UpdateRedDot()
end
function CLuaRedDotForGuild:OnMainPlayerCreated()
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst:IsInGuild() then
        CGuildMgr.Inst.guildNotification = PlayerPrefs.GetInt(CGuildMgr.Inst.GuildKey)>0
    else
        CGuildMgr.Inst.guildNotification=false
    end
    self:UpdateRedDot()
end


function CLuaRedDotForGuild:OnEnable()
    g_ScriptEvent:AddListener("MainPlayerCreated", self, "OnMainPlayerCreated")
    g_ScriptEvent:AddListener("GuildNotification", self, "GuildNotification")
    g_ScriptEvent:AddListener("SyncSelfBuildBonus", self, "OnSyncSelfBuildBonus")
end

function CLuaRedDotForGuild:OnDisable()
    g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "OnMainPlayerCreated")
    g_ScriptEvent:RemoveListener("GuildNotification", self, "GuildNotification")
    g_ScriptEvent:RemoveListener("SyncSelfBuildBonus", self, "OnSyncSelfBuildBonus")
end

function CLuaRedDotForGuild:GuildNotification(show)
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst:IsInGuild() then
        CGuildMgr.Inst.guildNotification = show
    else
        CGuildMgr.Inst.guildNotification = false
        CLuaRedDotForGuild.BonusAvaliable = false
    end
    self:UpdateRedDot()
end

function CLuaRedDotForGuild:OnSyncSelfBuildBonus( v, already, playstatus) 
    CLuaRedDotForGuild.Bonus = v
    CLuaRedDotForGuild.IsThisWeekTaken = already
    CLuaRedDotForGuild.Status = playstatus

    if already then
        CLuaRedDotForGuild.BonusAvaliable = false
    elseif v <= 1E-08 then
        CLuaRedDotForGuild.BonusAvaliable = false
    else
        CLuaRedDotForGuild.BonusAvaliable = true
    end

    if CClientMainPlayer.Inst and CClientMainPlayer.Inst:IsInGuild() then
    else
        --没有帮会
        CLuaRedDotForGuild.BonusAvaliable = false
    end
    self:UpdateRedDot()
end

function CLuaRedDotForGuild:UpdateRedDot()
    if self.m_RedDot then
        self.m_RedDot:SetActive(CGuildMgr.Inst.guildNotification or CLuaRedDotForGuild.BonusAvaliable)
    end
    if self.m_RedDot_Welfare then
        self.m_RedDot_Welfare:SetActive(CLuaRedDotForGuild.BonusAvaliable)
    end
    if self.m_RedDot_Member then
        self.m_RedDot_Member:SetActive(CGuildMgr.Inst.guildNotification)
    end
end