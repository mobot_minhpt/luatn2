require("common/common_include")

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CBaseWnd = import "L10.UI.CBaseWnd"
local UILabel = import "UILabel"
local QnCheckBoxGroup = import "L10.UI.QnCheckBoxGroup"

LuaGuildReportWnd = class()

RegistClassMember(LuaGuildReportWnd, "m_CloseButton")
RegistClassMember(LuaGuildReportWnd, "m_OKButton")
RegistClassMember(LuaGuildReportWnd, "m_CancelButton")
RegistClassMember(LuaGuildReportWnd, "m_CheckBoxGroup")
RegistClassMember(LuaGuildReportWnd, "m_GuildNameLabel")
RegistClassMember(LuaGuildReportWnd, "m_ReasonInput")
RegistClassMember(LuaGuildReportWnd, "m_TitleLabel")
RegistClassMember(LuaGuildReportWnd, "m_NameDesc")


function LuaGuildReportWnd:Init()

	self.m_CloseButton = self.transform:Find("Wnd_Bg_Secondary_1/CloseButton").gameObject
	self.m_OKButton = self.transform:Find("Anchor/Offset/OKButton").gameObject
	self.m_CancelButton = self.transform:Find("Anchor/Offset/CancelButton").gameObject
	self.m_CheckBoxGroup = self.transform:Find("Anchor/Offset/CheckBoxes"):GetComponent(typeof(QnCheckBoxGroup))
	self.m_GuildNameLabel = self.transform:Find("Anchor/Offset/NameLabel/ValueLabel"):GetComponent(typeof(UILabel))
	self.m_NameDesc = self.transform:Find("Anchor/Offset/NameLabel"):GetComponent(typeof(UILabel))
	self.m_ReasonInput = self.transform:Find("Anchor/Offset/Input"):GetComponent(typeof(UIInput))
	self.m_TitleLabel = self.transform:Find("Wnd_Bg_Secondary_1/TitleLabel"):GetComponent(typeof(UILabel))

	CommonDefs.AddOnClickListener(self.m_CloseButton, DelegateFactory.Action_GameObject(function(go) self:OnCloseButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_OKButton, DelegateFactory.Action_GameObject(function(go) self:OnOKButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_CancelButton, DelegateFactory.Action_GameObject(function(go) self:OnCancelButtonClick() end), false)

	self.m_GuildNameLabel.text = LuaPlayerReportMgr.GuildReportInfo.guildName
	local resportTypes = self:GetReportTypes()
	self.m_CheckBoxGroup:InitWithOptions(resportTypes, false, false)
	self.m_ReasonInput.characterLimit = 50
	self.m_CheckBoxGroup:SetSelect(0, true)
	self.m_ReasonInput.value =nil
	self.m_TitleLabel.text = LuaPlayerReportMgr.GuildReportInfo.customTitle
	self.m_NameDesc.text = LuaPlayerReportMgr.GuildReportInfo.customNameDesc
end

function LuaGuildReportWnd:GetReportTypes()
	local tbl = {}
	local type = LuaPlayerReportMgr.GuildReportInfo.type
	if type == "banghui" then
		table.insert(tbl, LocalString.GetString("帮会名称"))
		table.insert(tbl, LocalString.GetString("帮会宗旨"))
		table.insert(tbl, LocalString.GetString("帮会雕像"))
	elseif type == "zongpai" then
		table.insert(tbl, LocalString.GetString("宗派名称"))
	end
	table.insert(tbl, LocalString.GetString("举报其他"))
	return Table2Array(tbl,MakeArrayClass(cs_string))
end

function LuaGuildReportWnd:OnCloseButtonClick()
	self:Close()
end

function LuaGuildReportWnd:OnOKButtonClick()
	local resportTypes = self:GetReportTypes()
	local selectedIndex = -1
	for i=0,resportTypes.Length-1 do
		if self.m_CheckBoxGroup.m_CheckBoxes[i].Selected then
			selectedIndex = i
			break
		end
	end
	if selectedIndex == -1 then
		g_MessageMgr:ShowMessage("Guild_Report_Need_Choose_Report_Type")
		return
	end
	if selectedIndex == resportTypes.Length-1 and self.m_ReasonInput.value == "" then
		g_MessageMgr:ShowMessage("Guild_Report_Need_Input_Reason")
		return
	end

	LuaPlayerReportMgr:DoReportGuild(resportTypes[selectedIndex], self.m_ReasonInput.value)
	self:Close()
end

function LuaGuildReportWnd:OnCancelButtonClick()
	self:Close()
end

function LuaGuildReportWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end
