local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CButton = import "L10.UI.CButton"
local QnButton = import "L10.UI.QnButton"
local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local MessageWndManager = import "L10.UI.MessageWndManager"
local UILongPressButton = import "L10.UI.UILongPressButton"
local QnTableItem = import "L10.UI.QnTableItem"
local Color = import "UnityEngine.Color"
local NGUIText = import "NGUIText"
local State = import "L10.UI.CButton.State"
local UISoundEvents = import "SoundManager+UISoundEvents"

LuaBusinessView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaBusinessView, "CalendarLab", "CalendarLab", UILabel)
RegistChildComponent(LuaBusinessView, "BirdState", "BirdState", GameObject)
RegistChildComponent(LuaBusinessView, "End", "End", GameObject)
RegistChildComponent(LuaBusinessView, "LotteryBtn", "LotteryBtn", GameObject)
RegistChildComponent(LuaBusinessView, "NewsBtn", "NewsBtn", GameObject)
RegistChildComponent(LuaBusinessView, "InvestmentBtn", "InvestmentBtn", GameObject)
RegistChildComponent(LuaBusinessView, "RepaymentBtn", "RepaymentBtn", GameObject)
RegistChildComponent(LuaBusinessView, "RepaymentLab", "RepaymentLab", UILabel)
RegistChildComponent(LuaBusinessView, "HealthLab", "HealthLab", UILabel)
RegistChildComponent(LuaBusinessView, "HealthBtn", "HealthBtn", QnButton)
RegistChildComponent(LuaBusinessView, "OwnMoneyLab", "OwnMoneyLab", UILabel)
RegistChildComponent(LuaBusinessView, "DailyCostMoneyLab", "DailyCostMoneyLab", UILabel)
RegistChildComponent(LuaBusinessView, "CityGoodsView", "CityGoodsView", QnTableView)
RegistChildComponent(LuaBusinessView, "BuyBtn", "BuyBtn", CButton)
RegistChildComponent(LuaBusinessView, "SellBtn", "SellBtn", CButton)
RegistChildComponent(LuaBusinessView, "EndDayBtn", "EndDayBtn", CButton)
RegistChildComponent(LuaBusinessView, "InventoryTitleLab", "InventoryTitleLab", UILabel)
RegistChildComponent(LuaBusinessView, "CityTitleLab", "CityTitleLab", UILabel)
RegistChildComponent(LuaBusinessView, "InventoryGoodsView", "InventoryGoodsView", QnTableView)
RegistChildComponent(LuaBusinessView, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", GameObject)
RegistChildComponent(LuaBusinessView, "TipBtn", "TipBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaBusinessView, "m_EndBtn")
RegistClassMember(LuaBusinessView, "m_InvLevelDownBtn")
RegistClassMember(LuaBusinessView, "m_InvLevelUpBtn")
RegistClassMember(LuaBusinessView, "m_InvLab")
RegistClassMember(LuaBusinessView, "m_BirdNumLab")
RegistClassMember(LuaBusinessView, "m_LotteryRewardTip")
RegistClassMember(LuaBusinessView, "m_Bird")
RegistClassMember(LuaBusinessView, "m_Calendar")
RegistClassMember(LuaBusinessView, "m_CalendarLab2")
RegistClassMember(LuaBusinessView, "m_Calendar2Lab")
RegistClassMember(LuaBusinessView, "m_Calendar2Lab2")
RegistClassMember(LuaBusinessView, "m_GrayColor")
RegistClassMember(LuaBusinessView, "m_GuideGood")
RegistClassMember(LuaBusinessView, "m_ModeBtn")
RegistClassMember(LuaBusinessView, "m_ModeLab")

function LuaBusinessView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_EndBtn           = self.End.transform:Find("EndBtn").gameObject
    self.m_InvLevelDownBtn  = self.QnIncreseAndDecreaseButton.transform:Find("DecreaseButton"):GetComponent(typeof(QnButton))
    self.m_InvLevelUpBtn    = self.QnIncreseAndDecreaseButton.transform:Find("IncreaseButton"):GetComponent(typeof(QnButton))
    self.m_InvLab           = self.QnIncreseAndDecreaseButton.transform:Find("InputLab"):GetComponent(typeof(UILabel))
    self.m_BirdNumLab       = self.BirdState.transform:Find("Label"):GetComponent(typeof(UILabel))
    self.m_LotteryRewardTip = self.LotteryBtn.transform:Find("RewardDayTip").gameObject
    self.m_Bird             = self.transform:Find("Anchor/Left/Bird").gameObject
    self.m_CalendarLab2     = self.transform:Find("Anchor/Left/Bird/Calendar/CalendarLab2"):GetComponent(typeof(UILabel))
    self.m_Calendar2Lab     = self.transform:Find("Anchor/Left/End/Calendar/CalendarLab"):GetComponent(typeof(UILabel))
    self.m_Calendar2Lab2    = self.transform:Find("Anchor/Left/End/Calendar/CalendarLab2"):GetComponent(typeof(UILabel))
    self.m_ModeBtn          = self.transform:Find("Anchor/Left/ModeBg"):GetComponent(typeof(CButton))
    self.m_ModeLab          = self.transform:Find("Anchor/Left/Mode"):GetComponent(typeof(UILabel))
    self.m_GrayColor        = NGUIText.ParseColor24("b8b7b7", 0)

    UIEventListener.Get(self.m_EndBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if LuaBusinessMgr.m_CurDay < LuaBusinessMgr.m_MaxDay then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("BUSINESS_END_MAKESURE"),DelegateFactory.Action(function()
                Gac2Gas.EndTradeSimulation()
            end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
        else
            Gac2Gas.EndTradeSimulation()
            CUIManager.CloseUI(CLuaUIResources.BusinessMainWnd)
        end
    end)
    UIEventListener.Get(self.LotteryBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        SoundManager.Inst:PlayOneShot(UISoundEvents.ButtonClick, Vector3(0,0,0),nil,0)
        if not LuaBusinessMgr.m_IsOpenScenarioPegion then
            g_MessageMgr:ShowMessage("PEGION_NOT_OPEN")
            return
        end
        if math.ceil(LuaBusinessMgr.m_CurDay / 3) * 3 > LuaBusinessMgr.m_MaxDay then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("本次行商即将结束，下次行商开启后再来购买百姓票吧。"))
            return 
        end
        CUIManager.ShowUI(CLuaUIResources.BusinessLotteryWnd)
    end)
    UIEventListener.Get(self.NewsBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.BusinessNewsWnd)
    end)
    UIEventListener.Get(self.InvestmentBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        SoundManager.Inst:PlayOneShot(UISoundEvents.ButtonClick, Vector3(0,0,0),nil,0)
        if not LuaBusinessMgr.m_IsOpenScenarioInvestment then
            if LuaBusinessMgr.m_IsStoryMode then
                g_MessageMgr:ShowMessage("INVEST_NOT_OPEN")
            else 
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("日光模式，及时行乐，不必理会钱财投资这等事"))
            end
            return
        end
        CUIManager.ShowUI(CLuaUIResources.BusinessInvestmentWnd)
    end)
    UIEventListener.Get(self.RepaymentBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.BusinessRepaymentWnd)
    end)
    UIEventListener.Get(self.m_ModeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        local data = Business_Type.GetData(LuaBusinessMgr.m_GameMode)
        if data then
            g_MessageMgr:ShowMessage(data.IntroduceMessage)
        end
	end)
    UIEventListener.Get(self.BuyBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        LuaBusinessMgr.m_BusinessIsBuy = 1
        CUIManager.ShowUI(CLuaUIResources.BusinessBuyOrSellWnd)
    end)
    UIEventListener.Get(self.SellBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        LuaBusinessMgr.m_BusinessIsBuy = 0
        CUIManager.ShowUI(CLuaUIResources.BusinessBuyOrSellWnd)
    end)
    UIEventListener.Get(self.EndDayBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if LuaBusinessMgr.m_TicketRewarded == false and LuaBusinessMgr.m_TicketState >= 5 then
            g_MessageMgr:ShowMessage("BUSINESS_BUYPEGIONTICK_NEEDREWARD")
            return
        end
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("BUSINESS_ENDDAY_MAKESURE"),DelegateFactory.Action(function()
            Gac2Gas.TradeSimulateFinishToday()
        end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
    end)

    self.HealthBtn.OnClick = DelegateFactory.Action_QnButton(function (go)
        CUIManager.ShowUI(CLuaUIResources.BusinessHealthRecoverWnd)
    end)

    UIEventListener.Get(self.TipBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("BUSINESS_TIP")
    end)

    UIEventListener.Get(self.m_InvLevelDownBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    LuaBusinessMgr.m_BusinessIslevelUp = 0
        CUIManager.ShowUI(CLuaUIResources.BusinessWarehouseLevelOpWnd)
	end)

	UIEventListener.Get(self.m_InvLevelUpBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    LuaBusinessMgr.m_BusinessIslevelUp = 1
        CUIManager.ShowUI(CLuaUIResources.BusinessWarehouseLevelOpWnd)
	end)

    self.CityGoodsView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #LuaBusinessMgr.m_CurCityGoods
        end,
        function(item, index)
            self:InitItem1(item, LuaBusinessMgr.m_CurCityGoods[index + 1])
        end
    )

    self.InventoryGoodsView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #LuaBusinessMgr.m_Inventory
        end,
        function(item, index)
            self:InitItem2(item, LuaBusinessMgr.m_Inventory[index + 1])
        end
    )

    self.CityGoodsView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        LuaBusinessMgr.m_CurBuyGoodsRow = row
        local info = LuaBusinessMgr.m_CurCityGoods[row + 1]
        local find = -1
        for i = 1, #LuaBusinessMgr.m_Inventory do
            if LuaBusinessMgr.m_Inventory[i].type == info.type then
                find = i
                break
            end
        end
        if find > 0 and self.InventoryGoodsView.currentSelectRow ~= find - 1 then
            self.InventoryGoodsView:ScrollToRow(find - 1)
            self.InventoryGoodsView:SetSelectRow(find - 1, true)
        end
    end)

    self.InventoryGoodsView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        LuaBusinessMgr.m_CurSellGoodsRow = row
        local info = LuaBusinessMgr.m_Inventory[row + 1]
        local find = -1
        for i = 1, #LuaBusinessMgr.m_CurCityGoods do
            if LuaBusinessMgr.m_CurCityGoods[i].type == info.type then
                find = i
                break
            end
        end
        if find > 0 and self.CityGoodsView.currentSelectRow ~= find - 1 then
            self.CityGoodsView:ScrollToRow(find - 1)
            self.CityGoodsView:SetSelectRow(find - 1, true)
        end
    end)
end

function LuaBusinessView:Start()
end

--@region UIEvent

--@endregion UIEvent

function LuaBusinessView:OnEnable()
    g_ScriptEvent:AddListener("OnBusinessDataUpdate", self, "OnBusinessDataUpdate")
    LuaBusinessMgr:RefreshAll()
    self:RefreshAll()
end

function LuaBusinessView:OnDisable()
    g_ScriptEvent:RemoveListener("OnBusinessDataUpdate", self, "OnBusinessDataUpdate")
end

function LuaBusinessView:RefreshState()
    if LuaBusinessMgr.m_GameMode == 0 then
        return
    end
    local modeData = Business_Type.GetData(LuaBusinessMgr.m_GameMode)
    local isStoryMode = LuaBusinessMgr.m_IsStoryMode
    self:SwitchMode(isStoryMode)
    self.CalendarLab.text = LuaBusinessMgr.m_CurDay
    self.m_Calendar2Lab.text = LuaBusinessMgr.m_CurDay
    self.m_CalendarLab2.text = modeData.Days
    self.m_Calendar2Lab2.text = modeData.Days
    self.m_ModeLab.text = modeData.TypeName

    Extensions.SetLocalPositionZ(self.InvestmentBtn.transform, LuaBusinessMgr.m_IsOpenScenarioInvestment and 0 or -1)
    Extensions.SetLocalPositionZ(self.LotteryBtn.transform, (LuaBusinessMgr.m_IsOpenScenarioPegion and math.ceil(LuaBusinessMgr.m_CurDay / 3) * 3 <= LuaBusinessMgr.m_MaxDay) and 0 or -1)

    self.m_BirdNumLab.text = LuaBusinessMgr.m_ParrotNum

    self.RepaymentLab.text = SafeStringFormat3("%.f", LuaBusinessMgr.m_Debt)
    self.HealthLab.text = LuaBusinessMgr.m_Health
    self.OwnMoneyLab.text = SafeStringFormat3("%.f", LuaBusinessMgr.m_OwnMoney)
    self.DailyCostMoneyLab.text = LuaBusinessMgr.m_DailyCost

    local horseData = Business_Horse.GetData(LuaBusinessMgr.m_CarriageLevel)
    local nextHorseData = Business_Horse.GetData(LuaBusinessMgr.m_CarriageLevel + 1)
    self.m_InvLab.text = LuaBusinessMgr:GetCurInventoryAmount() .. "/" .. horseData.Warehouse

    local cityData = Business_City.GetData(LuaBusinessMgr.m_CurCity)
    self.InventoryTitleLab.text = horseData.HorseName
    self.CityTitleLab.text = SafeStringFormat3(LocalString.GetString("%s市场"), cityData.CityName) 

    self.m_InvLevelDownBtn.Enabled = LuaBusinessMgr.m_CarriageLevel > 1 and horseData.NeedMoney > 0
    self.m_InvLevelUpBtn.Enabled = nextHorseData and nextHorseData.CanRiseLv == 1

    self.m_LotteryRewardTip:SetActive(LuaBusinessMgr.m_TicketState >= 4)
end

function LuaBusinessView:SwitchMode(isStoryMode)
    self.m_Bird:SetActive(isStoryMode)
    self.End:SetActive(not isStoryMode)
    LuaUtils.SetLocalPositionY(self.LotteryBtn.transform, isStoryMode and 4 or 41)
    LuaUtils.SetLocalPositionY(self.NewsBtn.transform, isStoryMode and -96.3 or -57.4)
    LuaUtils.SetLocalPositionY(self.InvestmentBtn.transform, isStoryMode and -221.3 or -240.2)
    LuaUtils.SetLocalPositionY(self.RepaymentBtn.transform, isStoryMode and -315 or -327.6)
end

function LuaBusinessView:RefreshGoods()
    self.m_GuideGood = nil
    self.CityGoodsView:ReloadData(true, false)

    self.InventoryGoodsView:ReloadData(true, false)
end

function LuaBusinessView:InitItem1(item, info)
    local goodsName     = item.transform:Find("GoodsName"):GetComponent(typeof(UILabel))
    local goodsPrice    = item.transform:Find("GoodsPrice"):GetComponent(typeof(UILabel))
    local longPress     = item.transform:GetComponent(typeof(UILongPressButton))
    local qnTableItem   = item.transform:GetComponent(typeof(QnTableItem))
    
    local data = Business_Goods.GetData(info.type)
    local isCitySpecialGoods = LuaBusinessMgr.m_CurCitySpecialGoods[info.type]
    
    longPress.OnLongPressDelegate = DelegateFactory.Action(function ()
		LuaBusinessMgr.m_BusinessCurGoods = info.type
		CUIManager.ShowUI(CLuaUIResources.BusinessItemInfoWnd)	
    end)

    local color
    local prefix = ""
    if not info.onSale then
        color = self.m_GrayColor
        prefix = LocalString.GetString("(收)")
    elseif LuaBusinessMgr:GetCityOfSpecialGoods(info.type) then
        color = Color.yellow
    else
        color = Color.white
    end
    goodsName.color = color
    goodsPrice.color = color

    if data then
        goodsName.text = data.Name
        goodsPrice.text = prefix .. info.price
    end

    if info.type == 1 then
        self.m_GuideGood = item.gameObject
    end
end

function LuaBusinessView:InitItem2(item, info)
    local goodsName     = item.transform:Find("GoodsName"):GetComponent(typeof(UILabel))
    local goodsPrice    = item.transform:Find("GoodsPrice"):GetComponent(typeof(UILabel))
    local goodsNum      = item.transform:Find("GoodsNum"):GetComponent(typeof(UILabel))
    local longPress     = item.transform:GetComponent(typeof(UILongPressButton))
    local redMark       = item.transform:Find("RedMark").gameObject
    local greenMark     = item.transform:Find("GreenMark").gameObject

    local data = Business_Goods.GetData(info.type)

    local buyPrice = 0
    local sellPrice = LuaBusinessMgr.m_CurCityGoodsType2Price[info.type]
    if data then
        goodsName.text = data.Name
        goodsPrice.text = info.price
        goodsNum.text = info.num
        buyPrice = info.price
    end
    longPress.OnLongPressDelegate = DelegateFactory.Action(function ()
		LuaBusinessMgr.m_BusinessCurGoods = info.type
		CUIManager.ShowUI(CLuaUIResources.BusinessItemInfoWnd)	
    end)

    local color
    if LuaBusinessMgr:GetCityOfSpecialGoods(info.type) then
        color = Color.yellow
    else
        color = Color.white
    end
    goodsName.color = color
    goodsPrice.color = color
    goodsNum.color = color

    greenMark:SetActive(buyPrice > sellPrice)
    redMark:SetActive(sellPrice > buyPrice)
end

function LuaBusinessView:RefreshAll()
    self:RefreshState()
    self:RefreshGoods()
    LuaBusinessMgr.m_CurBuyGoodsRow = 0
    LuaBusinessMgr.m_CurSellGoodsRow = 0
end

function LuaBusinessView:OnBusinessDataUpdate()
    self:RefreshAll()
end

function LuaBusinessView:GetGuideGo(methodName)
    if methodName == "GetGoodToBuy" then
        return self.m_GuideGood
    elseif methodName == "GetBuyBtn" then
        return self.BuyBtn.gameObject
    elseif methodName == "GetEndDayBtn" then
        return self.EndDayBtn.gameObject
	end
	return nil
end
