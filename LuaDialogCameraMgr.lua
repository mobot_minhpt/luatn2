local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CClientNpc = import "L10.Game.CClientNpc"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CConversationMgr = import "L10.Game.CConversationMgr"
local CTaskDialogInfo = import "L10.Game.CConversationMgr+CTaskDialogInfo"
local ToricSpaceCamera = import "L10.Engine.CameraControl.ToricSpaceCamera"

LuaDialogCameraMgr = {}

LuaDialogCameraMgr.m_IsToricBegin = false

function LuaDialogCameraMgr.ProcessToricCamera(npcId1,npcId2,alpha,theta,phi,duration,noSway)
    --是否还在进行上个镜头的切换
    if CameraFollow.Inst.IsToricSpaceCamera then
        local tempTime = ToricSpaceCamera.Inst.mTempTime
        local pointsList = ToricSpaceCamera.Inst.mCameraPathList
        if pointsList and pointsList.Count >= 2 then
            local endTime = pointsList[1].m_Duration
            --ToricSpaceCamera.Inst.mTempTime = endTime
        end
    end

    local engineId1,engineId2 = nil,nil
    local objs = CClientObjectMgr.Inst:GetAllObjects()

    --是否是玩家自己
    if CClientMainPlayer.Inst then
        if npcId1 == 1 then
            engineId1 = CClientMainPlayer.Inst.EngineId
        end
        if npcId2 == 1 then
            engineId2 = CClientMainPlayer.Inst.EngineId
        end
    end
    --npc 0
    if npcId1 == 0 then
        engineId1 = (TypeAs(CConversationMgr.Inst.BaseInfo, typeof(CTaskDialogInfo))).m_NpcEngineId
    end
    if npcId2 == 0 then
        engineId2 = (TypeAs(CConversationMgr.Inst.BaseInfo, typeof(CTaskDialogInfo))).m_NpcEngineId
    end

    CommonDefs.EnumerableIterate(objs, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        if TypeIs(item, typeof(CClientNpc)) then
            local npc = TypeAs(item, typeof(CClientNpc))
            if tonumber(npc.TemplateId) == npcId1 then
                engineId1 = npc.EngineId
            end
            if tonumber(npc.TemplateId) == npcId2 then
                engineId2 = npc.EngineId
            end
        end
    end))

    if engineId1 and engineId2 then
        if not LuaDialogCameraMgr.m_IsToricBegin then
            LuaDialogCameraMgr.m_IsToricBegin = true
            CameraFollow.Inst:BeginToricSpaceCamera(engineId1,engineId2,alpha, theta, phi, duration,true,false,noSway)
        else
            CameraFollow.Inst:ToricSpaceCameraMoveTo(engineId1,engineId2,alpha, theta, phi, duration,noSway)
        end
    end
end

function LuaDialogCameraMgr.EndDialogCamera()
    if LuaDialogCameraMgr.m_IsToricBegin and CameraFollow.Inst.IsToricSpaceCamera then
        CameraFollow.Inst:EndToricSpaceCamera()
    end
    LuaDialogCameraMgr.m_IsToricBegin = false
end

--复曲面
