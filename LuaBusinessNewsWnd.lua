local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local CChatLinkMgr = import "CChatLinkMgr"

LuaBusinessNewsWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaBusinessNewsWnd, "NewsView", "NewsView", QnTableView)

--@endregion RegistChildComponent end

function LuaBusinessNewsWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.NewsView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #LuaBusinessMgr.m_News
        end,
        function(item, index)
            self:InitItem(item, LuaBusinessMgr.m_News[index + 1])
        end
    )
end

function LuaBusinessNewsWnd:Init()
    self.NewsView:ReloadData(true, false)
end

--@region UIEvent

--@endregion UIEvent

function LuaBusinessNewsWnd:InitItem(item, info)
    local newsLab   = item.transform:Find("NewsLab"):GetComponent(typeof(UILabel))

    if info.type == 1 then
        newsLab.text = CChatLinkMgr.TranslateToNGUIText(g_MessageMgr:FormatMessage(info.value), false)
    elseif info.type == 2 then
        local data = Business_Inform.GetData(info.value)
        newsLab.text = CChatLinkMgr.TranslateToNGUIText(g_MessageMgr:FormatMessage(data.InformMessage), false)
    end
end
