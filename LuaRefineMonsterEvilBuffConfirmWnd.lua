local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CButton = import "L10.UI.CButton"

LuaRefineMonsterEvilBuffConfirmWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaRefineMonsterEvilBuffConfirmWnd, "m_QnExpCtrl", "QnCostAndOwnMoney1", CCurentMoneyCtrl)
RegistChildComponent(LuaRefineMonsterEvilBuffConfirmWnd, "m_OKBtn", "OkButton", CButton)
RegistChildComponent(LuaRefineMonsterEvilBuffConfirmWnd, "m_CancelBtn", "CancelButton", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaRefineMonsterEvilBuffConfirmWnd, "m_Lab")
RegistClassMember(LuaRefineMonsterEvilBuffConfirmWnd, "m_Enough")

function LuaRefineMonsterEvilBuffConfirmWnd:Awake()
    self.m_Lab = self.transform:Find("Page/TextLabel"):GetComponent(typeof(UILabel))
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaRefineMonsterEvilBuffConfirmWnd:Init()
    local exp = CClientMainPlayer.Inst.PlayProp.Exp
    self.m_QnExpCtrl:SetType(EnumMoneyType_lua.Exp, 0, false)
    self.m_QnExpCtrl.m_OwnLabel.text = tostring(exp)
    self.m_QnExpCtrl:SetCost(LuaZongMenMgr.m_EvilBuffCostExp)
    self:UpdateExp()

    self.m_Lab.text = g_MessageMgr:FormatMessage("LianHuaGuai_FaZhen_EvilBuff_Confirm", SafeStringFormat3("%.f%%", LianHua_Setting.GetData().XiePaiBonus * 100))
    CommonDefs.AddOnClickListener(self.m_OKBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        if self.m_Enough then 
            local sectId = ((CClientMainPlayer.Inst or {}).BasicProp or {}).SectId
            if sectId ~= nil then
                Gac2Gas.RequestOpenXiepaiExtraReward(sectId)
                CUIManager.CloseUI(CLuaUIResources.RefineMonsterEvilBuffConfirmWnd)
            end
        else 
            g_MessageMgr:ShowMessage("Open_XiePai_LianHua_Bonus_Failure")
        end
    end), false)

    CommonDefs.AddOnClickListener(self.m_CancelBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        CUIManager.CloseUI(CLuaUIResources.RefineMonsterEvilBuffConfirmWnd)
    end), false)
end

--@region UIEvent

--@endregion UIEvent

function LuaRefineMonsterEvilBuffConfirmWnd:OnEnable()
    g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate", self, "UpdateExp")
    g_ScriptEvent:AddListener("SyncSectEvilValue", self, "OnSyncSectEvilValue")
    g_ScriptEvent:AddListener("SyncXiepaiExtraRewardSwitch", self, "OnSyncXiepaiExtraRewardSwitch")
end

function LuaRefineMonsterEvilBuffConfirmWnd:OnDisable()
    g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate", self, "UpdateExp")
    g_ScriptEvent:RemoveListener("SyncSectEvilValue", self, "OnSyncSectEvilValue")
    g_ScriptEvent:RemoveListener("SyncXiepaiExtraRewardSwitch", self, "OnSyncXiepaiExtraRewardSwitch")
end

function LuaRefineMonsterEvilBuffConfirmWnd:UpdateExp()
    local costExp = self.m_QnExpCtrl.cost
    local exp = CClientMainPlayer.Inst.PlayProp.Exp
    if costExp <=  exp then
        self.m_Enough = true
        self.m_QnExpCtrl.m_OwnLabel.text = tostring(exp)
    else
        self.m_Enough = false
        self.m_QnExpCtrl.m_OwnLabel.text = SafeStringFormat3("[c][ff0000]%s[-][/c]", tostring(exp))
    end
end

function LuaRefineMonsterEvilBuffConfirmWnd:OnSyncSectEvilValue(evilValue, bEvil)
    if not bEvil then
        CUIManager.CloseUI(CLuaUIResources.RefineMonsterEvilBuffConfirmWnd)
    end
end

function LuaRefineMonsterEvilBuffConfirmWnd:OnSyncXiepaiExtraRewardSwitch()
    self.m_QnExpCtrl:SetCost(LuaZongMenMgr.m_EvilBuffCostExp)
    self:UpdateExp()
end