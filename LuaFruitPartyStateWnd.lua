local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local UISlider = import "UISlider"
local GameObject = import "UnityEngine.GameObject"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"

LuaFruitPartyStateWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaFruitPartyStateWnd, "m_ExpandButton", "ExpandButton", CButton)
RegistChildComponent(LuaFruitPartyStateWnd, "m_ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaFruitPartyStateWnd, "m_Progress", "Progress", UISlider)
RegistChildComponent(LuaFruitPartyStateWnd, "m_Rewards", "Rewards", GameObject)
RegistChildComponent(LuaFruitPartyStateWnd, "m_ComboRoot", "ComboRoot", GameObject)
RegistChildComponent(LuaFruitPartyStateWnd, "m_Label", "Label", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaFruitPartyStateWnd, "m_Info")
RegistClassMember(LuaFruitPartyStateWnd, "m_Score")
RegistClassMember(LuaFruitPartyStateWnd, "m_MaxScore")
RegistClassMember(LuaFruitPartyStateWnd, "m_StartTime")
RegistClassMember(LuaFruitPartyStateWnd, "m_EndTime")
RegistClassMember(LuaFruitPartyStateWnd, "m_Tick")

function LuaFruitPartyStateWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.m_ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)


    --@endregion EventBind end
end

function LuaFruitPartyStateWnd:Init()
	self.m_ComboRoot:SetActive(false)
	self:InitRewards()
	self.m_Tick = RegisterTick(function()
        self:OnTick()
    end, 1000)
end

--@region UIEvent

function LuaFruitPartyStateWnd:OnExpandButtonClick()
	LuaUtils.SetLocalRotation(self.m_ExpandButton.transform,0,0,0)
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

--@endregion UIEvent

function LuaFruitPartyStateWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncShuiGuoPaiDuiScore", self, "OnSyncShuiGuoPaiDuiScore")
	g_ScriptEvent:AddListener("SyncShuiGuoPaiDuiLianJi", self, "OnSyncShuiGuoPaiDuiLianJi")
end

function LuaFruitPartyStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncShuiGuoPaiDuiScore", self, "OnSyncShuiGuoPaiDuiScore")
	g_ScriptEvent:RemoveListener("SyncShuiGuoPaiDuiLianJi", self, "OnSyncShuiGuoPaiDuiLianJi")
end

function LuaFruitPartyStateWnd:OnSyncShuiGuoPaiDuiScore(score)
	self.m_Score = score
	self.m_Progress.value = score / self.m_MaxScore
	self.m_ScoreLabel.text = score
	self:RefreshReward(false)
end

function LuaFruitPartyStateWnd:OnSyncShuiGuoPaiDuiLianJi(lianJiCount)
	self:ShowCombo(lianJiCount, 2)
end

function LuaFruitPartyStateWnd:InitRewards()
	local info = {}

	for i = 0, ShuiGuoPaiDui_Setting.GetData().RewardInfo.Length - 1 do
		local data = ShuiGuoPaiDui_Setting.GetData().RewardInfo[i]
		table.insert(info, {data[2], data[1]})
	end

	table.sort(info, function(a, b)
		return a[2] < b[2]
	end)

	self.m_Info = info
	self.m_MaxScore = info[#info][2]
	
	self.m_Score = LuaFruitPartyMgr.m_Score
	self.m_Progress.value = self.m_Score / self.m_MaxScore
	self.m_ScoreLabel.text = self.m_Score
	self:RefreshReward(true)
end

function LuaFruitPartyStateWnd:RefreshReward(init)
	for i = 0, self.m_Rewards.transform.childCount - 1 do
		local reward = self.m_Rewards.transform:GetChild(i)
		self:InitReward(reward, self.m_Info[i + 1], init)
	end
end

function LuaFruitPartyStateWnd:InitReward(item, info, init)
	local icon 			= item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local fx 			= item.transform:Find("Icon/Fx"):GetComponent(typeof(CUIFx))
	local archivedGo	= item.transform:Find("State/Archived").gameObject
	local notArchivedGo	= item.transform:Find("State/NotArchived").gameObject

	local id	= info[1]
	local score = info[2]
	if init == true then
		local scoreLab = item.transform:Find("Score"):GetComponent(typeof(UILabel))
		local itemData = Item_Item.GetData(id)
		icon:LoadMaterial(itemData.Icon)
		scoreLab.text = score
		LuaUtils.SetLocalPositionX(item.transform, 330 * (score / self.m_MaxScore))
	end

	local archived = self.m_Score >= score
	if not archivedGo.activeSelf and archived then
		-- 播放特效
		fx:LoadFx("Fx/UI/Prefab/UI_shuiguopaidui_fubenlibao.prefab")
	end
	Extensions.SetLocalPositionZ(item.transform, archived and 0 or -1)
	archivedGo:SetActive(archived)
	notArchivedGo:SetActive(not archived)
end

function LuaFruitPartyStateWnd:ShowCombo(num, time)
	self.m_StartTime = CServerTimeMgr.Inst:GetZone8Time()
    self.m_EndTime = self.m_StartTime:AddSeconds(time)
	self.m_Label.text = num
	self.m_ComboRoot:SetActive(true)
end

function LuaFruitPartyStateWnd:OnTick()
	local now = CServerTimeMgr.Inst:GetZone8Time()
	if self.m_EndTime and DateTime.Compare(now, self.m_EndTime) > 0 then
		self.m_ComboRoot:SetActive(false)
	end
end

function LuaFruitPartyStateWnd:OnDestroy()
    if self.m_Tick ~= nil then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end
