local CServerTimeMgr=import "L10.Game.CServerTimeMgr"

CLuaHouseWoodPileDataHistoryWnd = class()
RegistClassMember(CLuaHouseWoodPileDataHistoryWnd, "m_TableView")
RegistClassMember(CLuaHouseWoodPileDataHistoryWnd, "m_DataList")
RegistClassMember(CLuaHouseWoodPileDataHistoryWnd, "m_TitleLabel")
RegistClassMember(CLuaHouseWoodPileDataHistoryWnd, "m_HeadLabel3")
-- RegistClassMember(CLuaHouseWoodPileDataHistoryWnd, "m_HeadLabel4")
RegistClassMember(CLuaHouseWoodPileDataHistoryWnd, "m_HeadLabel5")
RegistClassMember(CLuaHouseWoodPileDataHistoryWnd, "m_LevelLookup")



CLuaHouseWoodPileDataHistoryWnd.m_EngineId = 0
CLuaHouseWoodPileDataHistoryWnd.m_PileId = 0
CLuaHouseWoodPileDataHistoryWnd.m_PileTemplateId = 0

function CLuaHouseWoodPileDataHistoryWnd:Init()
    Gac2Gas.QueryWoodPileFightHistory(CLuaHouseWoodPileDataHistoryWnd.m_EngineId)

    self.m_TableView = self.transform:Find("Anchor/TableView"):GetComponent(typeof(QnTableView))
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(function()
              return #self.m_DataList
          end, function(item, index)
              self:InitItem(item.gameObject, index + 1)
          end)
  
    self.m_TitleLabel = self.transform:Find("Anchor/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_HeadLabel3 = self.transform:Find("Anchor/HeadLabel3"):GetComponent(typeof(UILabel))
    self.m_HeadLabel5 = self.transform:Find("Anchor/HeadLabel5"):GetComponent(typeof(UILabel))


    self.m_LevelLookup = {}
    Zhuangshiwu_WoodPile.Foreach(function(k,v)
        local ids = v.MonsterId
        for i=1,ids.Length do
            self.m_LevelLookup[ids[i-1]] = i
        end
    end)

    self.m_DataList = {}
    self.m_TableView:ReloadData(true,false)

    local isHeal = CLuaHouseWoodPileMgr.IsHealWoodPile(CLuaHouseWoodPileDataHistoryWnd.m_PileTemplateId)
    if not isHeal then
        --输出
        self.m_TitleLabel.text = LocalString.GetString("木桩·输出")
        self.m_HeadLabel3.text = LocalString.GetString("总伤害")
        self.m_HeadLabel5.text = LocalString.GetString("秒伤")
    else
        --治疗
        self.m_TitleLabel.text = LocalString.GetString("木桩·治疗")
        self.m_HeadLabel3.text = LocalString.GetString("总治疗")
        self.m_HeadLabel5.text = LocalString.GetString("秒治疗")
    end
end
function CLuaHouseWoodPileDataHistoryWnd:InitItem(item,index)
    local tf = item.transform
    local label1 = tf:Find("Label1"):GetComponent(typeof(UILabel))
    local label2 = tf:Find("Label2"):GetComponent(typeof(UILabel))
    local label3 = tf:Find("Label3"):GetComponent(typeof(UILabel))
    local label4 = tf:Find("Label4"):GetComponent(typeof(UILabel))
    local label5 = tf:Find("Label5"):GetComponent(typeof(UILabel))

    local data = self.m_DataList[index]
    local time = CServerTimeMgr.ConvertTimeStampToZone8Time(data.StartTime)
    label1.text = ToStringWrap(time, "yy-MM-dd HH:mm")
    local lv = self.m_LevelLookup[data.MonsterTempId]
    if lv==1 then label2.text = LocalString.GetString("易")
    elseif lv==2 then label2.text = LocalString.GetString("中")
    elseif lv==3 then label2.text = LocalString.GetString("难")
    elseif lv==4 then label2.text = LocalString.GetString("困")
    elseif lv==5 then label2.text = LocalString.GetString("绝")
    end
    --治疗的时候TotalDamage服务器传下的值为负数，要取反
    local isHeal = CLuaHouseWoodPileMgr.IsHealWoodPile(CLuaHouseWoodPileDataHistoryWnd.m_PileTemplateId)
    local totalDamage = isHeal and math.abs(data.TotalDamage) or data.TotalDamage 

    label3.text = tostring(math.floor(totalDamage))
    label4.text = tostring(math.floor(data.Duration)).."s"
    local hurt = math.floor(totalDamage)
    if data.Duration>0 then
        hurt = math.floor(totalDamage/data.Duration)
    end
    label5.text = tostring(hurt)

end

function CLuaHouseWoodPileDataHistoryWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryWoodPileFightHistory_Result", self, "OnQueryWoodPileFightHistory_Result")

end

function CLuaHouseWoodPileDataHistoryWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryWoodPileFightHistory_Result", self, "OnQueryWoodPileFightHistory_Result")
end

function CLuaHouseWoodPileDataHistoryWnd:OnQueryWoodPileFightHistory_Result(pileType,data)
    self.m_DataList = {}
    CommonDefs.DictIterate(data.FightStatic, DelegateFactory.Action_object_object(function(key,val)
        local item = {
            MonsterTempId = val.MonsterTempId,
            StartTime = val.StartTime,
            LingShouDamage = val.LingShouDamage,
            TotalDamage = val.TotalDamage,
            SkillDamageInfo = val.SkillDamageInfo,
            PetDamageInfo = val.PetDamageInfo,
            PetTotalDamage = val.PetTotalDamage,
            OtherPetDamage = val.OtherPetDamage,
            Duration = val.Duration,
        }
        table.insert( self.m_DataList,item )
    end))

    table.sort( self.m_DataList,function(a,b)
        return a.StartTime>b.StartTime
    end )

    self.m_TableView:ReloadData(true,false)
end