local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local Screen=import "UnityEngine.Screen"
local Extensions = import "Extensions"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local CHousePuppetMgr = import "L10.Game.CHousePuppetMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientMonster = import "L10.Game.CClientMonster"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"


CLuaCPPuppetInfoWnd = class()
RegistClassMember(CLuaCPPuppetInfoWnd,"backBtn")
RegistClassMember(CLuaCPPuppetInfoWnd,"modelTexture")
RegistClassMember(CLuaCPPuppetInfoWnd,"templateNode")
RegistClassMember(CLuaCPPuppetInfoWnd,"table")
RegistClassMember(CLuaCPPuppetInfoWnd,"scrollView")
RegistClassMember(CLuaCPPuppetInfoWnd,"swipeGo")
RegistClassMember(CLuaCPPuppetInfoWnd,"renameBtn")
RegistClassMember(CLuaCPPuppetInfoWnd,"titleName")
RegistClassMember(CLuaCPPuppetInfoWnd,"sourceName")
RegistClassMember(CLuaCPPuppetInfoWnd,"canWalkBtn")
RegistClassMember(CLuaCPPuppetInfoWnd,"canWalkSign")
RegistClassMember(CLuaCPPuppetInfoWnd,"levelChooseNode")
RegistClassMember(CLuaCPPuppetInfoWnd,"curRotation")
RegistClassMember(CLuaCPPuppetInfoWnd,"nowEnableHighlightNode")
RegistClassMember(CLuaCPPuppetInfoWnd,"sections")
RegistClassMember(CLuaCPPuppetInfoWnd,"propertiesDict")
RegistClassMember(CLuaCPPuppetInfoWnd, "m_ModelTextureLoader")

function CLuaCPPuppetInfoWnd:Awake()
    -- self.backBtn = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
    self.modelTexture = self.transform:Find("Anchor/PlayerPropertyView/Panel/ModelTexture"):GetComponent(typeof(UITexture))
    self.templateNode = self.transform:Find("Anchor/PlayerPropertyView/PlayerDetailPropertyView/Item").gameObject
    self.table = self.transform:Find("Anchor/PlayerPropertyView/PlayerDetailPropertyView/Scroll View/Table"):GetComponent(typeof(UITable))
    self.scrollView = self.transform:Find("Anchor/PlayerPropertyView/PlayerDetailPropertyView/Scroll View"):GetComponent(typeof(UIScrollView))
    self.swipeGo = self.transform:Find("Anchor/PlayerPropertyView/Panel/ModelTexture").gameObject
    self.renameBtn = self.transform:Find("Anchor/PlayerPropertyView/renameBtn").gameObject
    self.titleName = self.transform:Find("Anchor/PlayerPropertyView/Title"):GetComponent(typeof(UILabel))
    self.sourceName = self.transform:Find("Anchor/PlayerPropertyView/Source/text"):GetComponent(typeof(UILabel))
    self.canWalkBtn = self.transform:Find("Anchor/PlayerPropertyView/WalkSwitchButton").gameObject
    self.canWalkSign = self.transform:Find("Anchor/PlayerPropertyView/WalkSwitchButton/Checkmark").gameObject
    self.levelChooseNode = self.transform:Find("Anchor/PlayerPropertyView/LevelChoose").gameObject
self.curRotation = 180
    self.nowEnableHighlightNode = nil--self.transform:Find("").gameObject
self.sections = nil
--self.propertiesDict = ?

end

-- Auto Generated!!
function CLuaCPPuppetInfoWnd:Init( )
    -- UIEventListener.Get(self.backBtn).onClick = DelegateFactory.VoidDelegate(function (p)
    --     self:Close()
    -- end)
    UIEventListener.Get(self.swipeGo).onDrag = DelegateFactory.VectorDelegate(function(go,v)
        self:OnDragModel(go,v)
    end)
    -- < MakeDelegateFromCSFunction >(self.OnDragModel, VectorDelegate, self)
    UIEventListener.Get(self.canWalkBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:SetWalkState(go)
    end)
    -- < MakeDelegateFromCSFunction >(self.SetWalkState, VoidDelegate, self)
    self.templateNode:SetActive(false)
    self:InitProp()
    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:Load(ro)
    end)
    self:LoadModel()
    self:InitDetailInfo()
    self:InitSetLevelBtn()
end
function CLuaCPPuppetInfoWnd:InitSetLevelBtn( )
    local difLevel = 0
    if CommonDefs.DictContains(CHousePuppetMgr.Inst.EngineToIdxDic, typeof(UInt32), CHousePuppetMgr.Inst.SavePuppetEngineId) then
        local idx = CommonDefs.DictGetValue(CHousePuppetMgr.Inst.EngineToIdxDic, typeof(UInt32), CHousePuppetMgr.Inst.SavePuppetEngineId)
        if idx > 0 and CClientHouseMgr.Inst.mCurFurnitureProp ~= nil and CommonDefs.DictContains(CClientHouseMgr.Inst.mCurFurnitureProp.PuppetInfo, typeof(Byte), idx) then
            local playerPuppetInfo = CommonDefs.DictGetValue(CClientHouseMgr.Inst.mCurFurnitureProp.PuppetInfo, typeof(Byte), idx)
            difLevel = (playerPuppetInfo.Difficulty)
        end
    end
    for i = 0, 4 do
        local btn = self.levelChooseNode.transform:Find(tostring((i + 1))).gameObject
        local level = i
        UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function (p)
            self:SetCPPuppetLevel(btn, level)
        end)

        if difLevel == i then
            if self.nowEnableHighlightNode ~= nil then
                self.nowEnableHighlightNode:SetActive(false)
            end

            self.nowEnableHighlightNode = btn.transform:Find("highlight").gameObject
            self.nowEnableHighlightNode:SetActive(true)
        end
    end
end



function CLuaCPPuppetInfoWnd:OnEnable()
    g_ScriptEvent:AddListener("HouseSetCPPuppetDiffSucc", self, "ResetInfo")
    g_ScriptEvent:AddListener("HouseUpdatePuppetData", self, "InitDetailInfo")
end

function CLuaCPPuppetInfoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HouseSetCPPuppetDiffSucc", self, "ResetInfo")
    g_ScriptEvent:RemoveListener("HouseUpdatePuppetData", self, "InitDetailInfo")
end

function CLuaPlayerPuppetInfoWnd:ResetInfo()
    self:InitProp()
    self:InitDetailInfo()
end

function CLuaCPPuppetInfoWnd:SetCPPuppetLevel( go, level)
    if self.nowEnableHighlightNode ~= nil then
        self.nowEnableHighlightNode:SetActive(false)
    end

    --nowEnableHighlightNode = go.transform.FindChild("highlight").gameObject;
    --nowEnableHighlightNode.SetActive(true);
    Gac2Gas.SwitchHousePuppetDifficulty(CHousePuppetMgr.Inst.SavePuppetEngineId, level)
end
function CLuaCPPuppetInfoWnd:GetCPMonsterId( engineId)
    if CommonDefs.DictContains(CHousePuppetMgr.Inst.EngineToIdxDic, typeof(UInt32), engineId) and CClientHouseMgr.Inst.mCurFurnitureProp ~= nil then
        local idx = CommonDefs.DictGetValue(CHousePuppetMgr.Inst.EngineToIdxDic, typeof(UInt32), engineId)
        local playerPuppetInfo = CommonDefs.DictGetValue(CClientHouseMgr.Inst.mCurFurnitureProp.PuppetInfo, typeof(Byte), idx)
        if playerPuppetInfo ~= nil then
            local cppuppet = HousePuppet_CPPuppet.GetData(playerPuppetInfo.Id)
            if cppuppet ~= nil then
                if playerPuppetInfo.Difficulty == 0 then
                    return cppuppet.Easy
                elseif playerPuppetInfo.Difficulty == 1 then
                    return cppuppet.Normal
                elseif playerPuppetInfo.Difficulty == 2 then
                    return cppuppet.Tough
                elseif playerPuppetInfo.Difficulty == 3 then
                    return cppuppet.Hard
                elseif playerPuppetInfo.Difficulty == 4 then
                    return cppuppet.Dead
                end
            end
        end
    end

    return 0
end
function CLuaCPPuppetInfoWnd:InitDetailInfo( )
    local monsterId = self:GetCPMonsterId(CHousePuppetMgr.Inst.SavePuppetEngineId)
    if monsterId > 0 then
        local monsterInfo = Monster_Monster.GetData(monsterId)
        if monsterInfo ~= nil then
            self.titleName.text = monsterInfo.Name
        end
        if CommonDefs.DictContains(CHousePuppetMgr.Inst.EngineToIdxDic, typeof(UInt32), CHousePuppetMgr.Inst.SavePuppetEngineId) and CClientHouseMgr.Inst.mCurFurnitureProp ~= nil then
            local idx = CommonDefs.DictGetValue(CHousePuppetMgr.Inst.EngineToIdxDic, typeof(UInt32), CHousePuppetMgr.Inst.SavePuppetEngineId)
            local playerPuppetInfo = CommonDefs.DictGetValue(CClientHouseMgr.Inst.mCurFurnitureProp.PuppetInfo, typeof(Byte), idx)
            if playerPuppetInfo ~= nil then
                if playerPuppetInfo.IsPatrol > 0 then
                    self.canWalkSign:SetActive(true)
                else
                    self.canWalkSign:SetActive(false)
                end
            end
        end
    end
end
function CLuaCPPuppetInfoWnd:SetWalkState( go)
    if CommonDefs.DictContains(CHousePuppetMgr.Inst.EngineToIdxDic, typeof(UInt32), CHousePuppetMgr.Inst.SavePuppetEngineId) then
        local idx = CommonDefs.DictGetValue(CHousePuppetMgr.Inst.EngineToIdxDic, typeof(UInt32), CHousePuppetMgr.Inst.SavePuppetEngineId)
        Gac2Gas.SetPuppetIsPatrol(idx, not self.canWalkSign.activeSelf)
        --canWalkSign.SetActive(!canWalkSign.activeSelf);
    end
end

function CLuaCPPuppetInfoWnd:OnDragModel(go,delta)
    local deltaVal = -delta.x / Screen.width * 360
    self.curRotation = self.curRotation + deltaVal
    CUIManager.SetModelRotation("__MainPlayerModelCamera__", self.curRotation)
end
function CLuaCPPuppetInfoWnd:LoadModel()
    self.modelTexture.mainTexture = CUIManager.CreateModelTexture("__MainPlayerModelCamera__",self.m_ModelTextureLoader,180,0.05,-1.06,4.66,false,true,1,false,false)
end
function CLuaCPPuppetInfoWnd:OnDestroy()
    CUIManager.DestroyModelTexture("__MainPlayerModelCamera__")
end
function CLuaCPPuppetInfoWnd:Load( renderObj)
    --CClientMainPlayer mainPlayer = CClientMainPlayer.Inst;
    --CClientMainPlayer mainPlayer = CClientMainPlayer.Inst;
    local obj = CClientObjectMgr.Inst:GetObject(CHousePuppetMgr.Inst.SavePuppetEngineId)

    local fakeObj = TypeAs(obj, typeof(CClientMonster))
    if fakeObj == nil then
        return
    end

    local playerPuppetInfo = nil

    if CommonDefs.DictContains(CHousePuppetMgr.Inst.EngineToIdxDic, typeof(UInt32), CHousePuppetMgr.Inst.SavePuppetEngineId) and CClientHouseMgr.Inst.mCurFurnitureProp ~= nil then
        local idx = CommonDefs.DictGetValue(CHousePuppetMgr.Inst.EngineToIdxDic, typeof(UInt32), CHousePuppetMgr.Inst.SavePuppetEngineId)
        playerPuppetInfo = CommonDefs.DictGetValue(CClientHouseMgr.Inst.mCurFurnitureProp.PuppetInfo, typeof(Byte), idx)
        if playerPuppetInfo ~= nil then
            local cpInfo = HousePuppet_CPPuppet.GetData(playerPuppetInfo.Id)
            if cpInfo ~= nil then
                if cpInfo.RoleIdx == 18005445 then
                    CClientMonster.LoadResourceIntoRO(renderObj, cpInfo.RoleIdx, true)
                    self.sourceName.text = LocalString.GetString('当铺积分商店')
                elseif cpInfo.RoleIdx > 100 then
                    CClientMonster.LoadResourceIntoRO(renderObj, cpInfo.RoleIdx, true)
                    self.sourceName.text = LocalString.GetString("npc好感度")--CCPPuppetInfoWnd.SourceString2
                else
                    CHousePuppetMgr.Inst:InitCPRenderObject(renderObj, playerPuppetInfo)
                    self.sourceName.text = LocalString.GetString("主角剧情")--CCPPuppetInfoWnd.SourceString
                end
            end
        end
    end
end
function CLuaCPPuppetInfoWnd:InitProp( )
    self.sections = {LocalString.GetString("气血和防御"), LocalString.GetString("元素抗性")}
    self.propertiesDict = {}
    for i,v in ipairs(self.sections) do
        table.insert( self.propertiesDict,{} )
    end
    -- CommonDefs.DictClear(self.propertiesDict)
    -- do
    --     local i = 0
    --     while i < self.sections.Length do
    --         CommonDefs.DictAdd(self.propertiesDict, typeof(Int32), i, typeof(MakeGenericClass(List, PlayerPropDisplayData)), CreateFromClass(MakeGenericClass(List, PlayerPropDisplayData)))
    --         i = i + 1
    --     end
    -- end
    --//基础 8项

    --propertiesDict[0].Add(new CPlayerDetailPropertyView.PlayerPropDisplayData(LocalString.GetString("当前经验"), 0, "FightProp_Tips_Exp"));
    --propertiesDict[0].Add(new CPlayerDetailPropertyView.PlayerPropDisplayData(LocalString.GetString("升级经验"), 0, "FightProp_Tips_Exp_ShengJi"));
    --propertiesDict[0].Add(new CPlayerDetailPropertyView.PlayerPropDisplayData(LocalString.GetString("修为"), 0, "FightProp_Tips_XiuWei"));
    --propertiesDict[0].Add(new CPlayerDetailPropertyView.PlayerPropDisplayData(LocalString.GetString("修炼"), 0, "FightProp_Tips_XiuLian"));
    --propertiesDict[0].Add(new CPlayerDetailPropertyView.PlayerPropDisplayData(LocalString.GetString("活力"), 0, "FightProp_Tips_HuoLi"));
    --propertiesDict[0].Add(new CPlayerDetailPropertyView.PlayerPropDisplayData(LocalString.GetString("PK值"), 0, "FightProp_Tips_PK"));
    --propertiesDict[0].Add(new CPlayerDetailPropertyView.PlayerPropDisplayData(LocalString.GetString("幸运值"), EPlayerFightProp.MF, "FightProp_Tips_MF"));

    --气血和防御
    table.insert(self.propertiesDict[1],{LocalString.GetString("最大气血上限"), EPlayerFightProp.HpFull, "FightProp_Tips_HpFull"})
    table.insert(self.propertiesDict[1],{LocalString.GetString("物理防御"), EPlayerFightProp.pDef, "FightProp_Tips_pDef"})
    table.insert(self.propertiesDict[1],{LocalString.GetString("法术防御"), EPlayerFightProp.mDef, "FightProp_Tips_mDef"})
    table.insert(self.propertiesDict[1],{LocalString.GetString("物理躲避"), EPlayerFightProp.pMiss, "FightProp_Tips_pMiss"})
    table.insert(self.propertiesDict[1],{LocalString.GetString("法术躲避"), EPlayerFightProp.mMiss, "FightProp_Tips_mMiss"})
    table.insert(self.propertiesDict[1],{LocalString.GetString("格挡"), EPlayerFightProp.Block, "FightProp_Tips_Block"})
    table.insert(self.propertiesDict[1],{LocalString.GetString("格挡伤害减免"), EPlayerFightProp.BlockDamage, "FightProp_Tips_BlockDamage"})



    -- CommonDefs.ListAdd(CommonDefs.DictGetValue(self.propertiesDict, typeof(Int32), 0), typeof(PlayerPropDisplayData), PlayerPropDisplayData(LocalString.GetString("最大气血上限"), EPlayerFightProp.HpFull, "FightProp_Tips_HpFull"))
    -- CommonDefs.ListAdd(CommonDefs.DictGetValue(self.propertiesDict, typeof(Int32), 0), typeof(PlayerPropDisplayData), PlayerPropDisplayData(LocalString.GetString("物理防御"), EPlayerFightProp.pDef, "FightProp_Tips_pDef"))
    -- CommonDefs.ListAdd(CommonDefs.DictGetValue(self.propertiesDict, typeof(Int32), 0), typeof(PlayerPropDisplayData), PlayerPropDisplayData(LocalString.GetString("法术防御"), EPlayerFightProp.mDef, "FightProp_Tips_mDef"))
    -- CommonDefs.ListAdd(CommonDefs.DictGetValue(self.propertiesDict, typeof(Int32), 0), typeof(PlayerPropDisplayData), PlayerPropDisplayData(LocalString.GetString("物理躲避"), EPlayerFightProp.pMiss, "FightProp_Tips_pMiss"))
    -- CommonDefs.ListAdd(CommonDefs.DictGetValue(self.propertiesDict, typeof(Int32), 0), typeof(PlayerPropDisplayData), PlayerPropDisplayData(LocalString.GetString("法术躲避"), EPlayerFightProp.mMiss, "FightProp_Tips_mMiss"))
    -- CommonDefs.ListAdd(CommonDefs.DictGetValue(self.propertiesDict, typeof(Int32), 0), typeof(PlayerPropDisplayData), PlayerPropDisplayData(LocalString.GetString("格挡"), EPlayerFightProp.Block, "FightProp_Tips_Block"))
    -- CommonDefs.ListAdd(CommonDefs.DictGetValue(self.propertiesDict, typeof(Int32), 0), typeof(PlayerPropDisplayData), PlayerPropDisplayData(LocalString.GetString("格挡伤害减免"), EPlayerFightProp.BlockDamage, "FightProp_Tips_BlockDamage"))
    --元素抗性
    table.insert(self.propertiesDict[2],{LocalString.GetString("火抗性"), EPlayerFightProp.AntiFire, "FightProp_Tips_AntiFire"})
    table.insert(self.propertiesDict[2],{LocalString.GetString("电抗性"), EPlayerFightProp.AntiThunder, "FightProp_Tips_AntiThunder"})
    table.insert(self.propertiesDict[2],{LocalString.GetString("风抗性"), EPlayerFightProp.AntiWind, "FightProp_Tips_AntiWind"})
    table.insert(self.propertiesDict[2],{LocalString.GetString("冰抗性"), EPlayerFightProp.AntiIce, "FightProp_Tips_AntiIce"})
    table.insert(self.propertiesDict[2],{LocalString.GetString("毒抗性"), EPlayerFightProp.AntiPoison, "FightProp_Tips_AntiPoison"})
    table.insert(self.propertiesDict[2],{LocalString.GetString("光抗性"), EPlayerFightProp.AntiLight, "FightProp_Tips_AntiLight"})
    table.insert(self.propertiesDict[2],{LocalString.GetString("幻抗性"), EPlayerFightProp.AntiIllusion, "FightProp_Tips_AntiIllusion"})
    table.insert(self.propertiesDict[2],{LocalString.GetString("水抗性"), EPlayerFightProp.AntiWater, "FightProp_Tips_AntiWater"})


    -- CommonDefs.ListAdd(CommonDefs.DictGetValue(self.propertiesDict, typeof(Int32), 1), typeof(PlayerPropDisplayData), PlayerPropDisplayData(LocalString.GetString("火抗性"), EPlayerFightProp.AntiFire, "FightProp_Tips_AntiFire"))
    -- CommonDefs.ListAdd(CommonDefs.DictGetValue(self.propertiesDict, typeof(Int32), 1), typeof(PlayerPropDisplayData), PlayerPropDisplayData(LocalString.GetString("电抗性"), EPlayerFightProp.AntiThunder, "FightProp_Tips_AntiThunder"))
    -- CommonDefs.ListAdd(CommonDefs.DictGetValue(self.propertiesDict, typeof(Int32), 1), typeof(PlayerPropDisplayData), PlayerPropDisplayData(LocalString.GetString("风抗性"), EPlayerFightProp.AntiWind, "FightProp_Tips_AntiWind"))
    -- CommonDefs.ListAdd(CommonDefs.DictGetValue(self.propertiesDict, typeof(Int32), 1), typeof(PlayerPropDisplayData), PlayerPropDisplayData(LocalString.GetString("冰抗性"), EPlayerFightProp.AntiIce, "FightProp_Tips_AntiIce"))
    -- CommonDefs.ListAdd(CommonDefs.DictGetValue(self.propertiesDict, typeof(Int32), 1), typeof(PlayerPropDisplayData), PlayerPropDisplayData(LocalString.GetString("毒抗性"), EPlayerFightProp.AntiPoison, "FightProp_Tips_AntiPoison"))
    -- CommonDefs.ListAdd(CommonDefs.DictGetValue(self.propertiesDict, typeof(Int32), 1), typeof(PlayerPropDisplayData), PlayerPropDisplayData(LocalString.GetString("光抗性"), EPlayerFightProp.AntiLight, "FightProp_Tips_AntiLight"))
    -- CommonDefs.ListAdd(CommonDefs.DictGetValue(self.propertiesDict, typeof(Int32), 1), typeof(PlayerPropDisplayData), PlayerPropDisplayData(LocalString.GetString("幻抗性"), EPlayerFightProp.AntiIllusion, "FightProp_Tips_AntiIllusion"))
    -- CommonDefs.ListAdd(CommonDefs.DictGetValue(self.propertiesDict, typeof(Int32), 1), typeof(PlayerPropDisplayData), PlayerPropDisplayData(LocalString.GetString("水抗性"), EPlayerFightProp.AntiWater, "FightProp_Tips_AntiWater"))

    Extensions.RemoveAllChildren(self.table.transform)

    for i,v in ipairs(self.sections) do
        local go = CUICommonDef.AddChild(self.table.gameObject, self.templateNode)
        go:SetActive(true)
        local script = go:GetComponent(typeof(CCommonLuaScript))
        script.m_LuaSelf:Init(v,self.propertiesDict[i],nil, CHousePuppetMgr.Inst.SaveCPPuppetFightProperty)
    end

    -- do
    --     local i = 0
    --     while i < self.sections.Length do
    --         local go = CUICommonDef.AddChild(self.table.gameObject, self.templateNode)
    --         go:SetActive(true)

    --         CommonDefs.GetComponent_GameObject_Type(go, typeof(CPuppetDetailPropertySection)):Init(self.sections[i], CommonDefs.DictGetValue(self.propertiesDict, typeof(Int32), i), nil, CHousePuppetMgr.Inst.SaveCPPuppetFightProperty)
    --         i = i + 1
    --     end
    -- end
    self.table:Reposition()
    self.scrollView:ResetPosition()
end

