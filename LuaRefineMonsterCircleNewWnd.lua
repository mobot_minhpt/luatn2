local Color = import "UnityEngine.Color"
local MeshRenderer = import "UnityEngine.MeshRenderer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CButton = import "L10.UI.CButton"
local Animation = import "UnityEngine.Animation"
local CTrackMgr = import "L10.Game.CTrackMgr"

LuaRefineMonsterCircleNewWnd = class()
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_ItemBtn")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_CloseBtn")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_ItemBtnRedDot")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_RuleDescBtn")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_Warming")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_WarmingLabel")

RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_CurrentAttribute")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_CompareAttribute")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_LingLiDescBtn")

RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_MonsterInfoRoot")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_MonsterPutInSmallBtn")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_MonsterRefineListBtn")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_MonsterIcon")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_MonsterPutInBigBtn")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_LingHePutInBtn")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_LingHeIcon")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_ProgressTexture")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_LingLiCountLabel")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_LianHuaBg")
-- 保存法阵的属性
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_CurrentAtrribute")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_CompareLabels")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_CurrentLabels")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_CompareSprite")
-- 五行
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_FaZhenWuXingTexture")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_LingHeWuXingTexture")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_LingHeWuXingBg")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_WuXingGuideGo")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_WuXingRefreshLabel")
-- 状态参数
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_StatusTable")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_LingLiParams")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_Tick")
-- 特效
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_LianHuaFxRoot")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_LingHeFx")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_MonsterFx")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_WuXingFx")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_WuXingBgFx")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_WuXingFxTexture")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_ItemBtnLabel")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_LingHeGuideGo")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_LingHeTextureLoader")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_LingHeWuXingTitle")
-- 邪派炼化加成
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_EvilBuffGo")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_EvilBuffTime")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_EvilBuffDesc")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_EvilBuffBtn")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_EvilBuffBgOff")
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_EvilBuffBgOn")
-- 新特效
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_MonsterLianHuaFx")
-- 新交互
RegistClassMember(LuaRefineMonsterCircleNewWnd, "m_MonsterPutInInfo")

RegistChildComponent(LuaRefineMonsterCircleNewWnd, "m_NiuZhuanWuXingTag", "NiuZhuanWuXingTag", GameObject)
RegistChildComponent(LuaRefineMonsterCircleNewWnd, "m_NiuZhuanWuXingDesLabel", "NiuZhuanWuXingDesLabel", UILabel)
RegistChildComponent(LuaRefineMonsterCircleNewWnd, "m_NiuZhuanWuXingCDLabel", "NiuZhuanWuXingCDLabel", UILabel)
RegistChildComponent(LuaRefineMonsterCircleNewWnd, "m_ChangeMyWuXingButton", "ChangeMyWuXingButton", GameObject)

function LuaRefineMonsterCircleNewWnd:InitComponents()
    self.m_ItemBtn = self.transform:Find("Anchor/Bottom/ItemButton").gameObject
    self.m_ItemBtnLabel = self.transform:Find("Anchor/Bottom/ItemButton/Label"):GetComponent(typeof(UILabel))
    self.m_CloseBtn = self.transform:Find("CloseButton").gameObject
    self.m_ItemBtnRedDot = self.transform:Find("Anchor/Bottom/ItemButton/RedDot").gameObject
    self.m_RuleDescBtn = self.transform:Find("Anchor/Bottom/RuleDescBtn").gameObject
    self.m_Warming = self.transform:Find("Anchor/Bottom/Warming").gameObject
    self.m_WarmingLabel = self.transform:Find("Anchor/Bottom/Warming/Label"):GetComponent(typeof(UILabel))

    self.m_CurrentAttribute = self.transform:Find("Anchor/Attribute/Current/Grid").gameObject
    self.m_LingLiDescBtn = self.transform:Find("Anchor/Attribute/Current/LingLiDescBtn").gameObject
    self.m_LingLiDescBtn2 = self.transform:Find("Anchor/Attribute/Current/LingLiDescBtn_2").gameObject

    self.m_MonsterInfoRoot = self.transform:Find("Anchor/Center/Monster/Info").gameObject
    self.m_MonsterPutInSmallBtn = self.transform:Find("Anchor/Center/Monster/Info/PutInBtn").gameObject
    self.m_MonsterRefineListBtn = self.transform:Find("Anchor/Center/Monster/Info/RefineListBtn").gameObject
    self.m_MonsterIcon = self.transform:Find("Anchor/Center/Monster/MonsterIcon"):GetComponent(typeof(CUITexture))
    self.m_MonsterPutInBigBtn = self.transform:Find("Anchor/Center/Monster/PutInBtn").gameObject
    self.m_LingHePutInBtn = self.transform:Find("Anchor/Center/LingHe/PutInBtn").gameObject
    self.m_LingHePutInBtn:SetActive(false)
    self.m_LingHeGuideGo = self.transform:Find("Anchor/Center/LingHe/LingHeGuideGo").gameObject

    self.m_LingHePanel = self.transform:Find("Anchor/Center/LingHe/Panel").gameObject
    self.m_LingHePanel:SetActive(false)
    self.m_LingHeIcon = self.transform:Find("Anchor/Center/LingHe/Panel/Icon"):GetComponent(typeof(UITexture))
    self.m_LingHeIcon.gameObject:SetActive(false)

    -- 进度也删了
    self.m_ProgressTexture = self.transform:Find("Anchor/Center/LingHe/Panel2/ProgressTexture"):GetComponent(typeof(UITexture))
    self.transform:Find("Anchor/Center/LingHe/Panel2").gameObject:SetActive(false)

    self.m_FaZhenWuXingTexture = self.transform:Find("Anchor/WuXing/FaZhenWuXing/Texture"):GetComponent(
                                     typeof(CUITexture))
    self.m_LingHeWuXingTexture = self.transform:Find("Anchor/WuXing/LingHeWuXing/Texture"):GetComponent(
                                     typeof(CUITexture))
    self.m_LingHeWuXingBg = self.transform:Find("Anchor/WuXing/LingHeWuXing").gameObject
    self.m_LingHeWuXingTitle = self.transform:Find("Anchor/WuXing/Title1").gameObject

    self.m_WuXingGuideGo = self.transform:Find("Anchor/WuXing/WuXingGuideGo").gameObject
    self.m_AttributeGuideGo = self.transform:Find("Anchor/Attribute/AttributeGuideGo").gameObject
    self.m_LingFuGuideGo = self.transform:Find("Anchor/LingFuBuff/LingFuGuideGo").gameObject

    self.m_LianHuaBg = self.transform:Find("Anchor/Center/Bg03").gameObject
    self.m_LingLiCountLabel = self.transform:Find("Anchor/Center/LingLiCount"):GetComponent(typeof(UILabel))
    self.m_WuXingRefreshLabel = self.transform:Find("Anchor/WuXing/RefreshLabel"):GetComponent(typeof(UILabel))

    -- 迭代后新的对比
    self.m_CompareAttribute = self.transform:Find("Anchor/Attribute/Current/Grid (1)").gameObject
    self.m_CompareLabels = {}
    self.m_CompareSprite = {}
    for i = 1, 3 do
        self.m_CompareLabels[i] = self.transform:Find("Anchor/Attribute/Current/Grid (1)/" .. (i+1) .. "/LabelNew"):GetComponent(typeof(UILabel))
        self.m_CompareSprite[i] = self.transform:Find("Anchor/Attribute/Current/Grid (1)/" .. (i+1) .. "/LabelNew/Sprite"):GetComponent(typeof(UISprite))
    end

    self.m_CurrentLabels = {}
    self.m_CurrentLabels2 = {}

    self.m_CurrentLabels[1] = self.transform:Find("Anchor/Attribute/Current/1/Label"):GetComponent(typeof(UILabel))
    for i = 2, 4 do
        self.m_CurrentLabels[i] = self.transform:Find("Anchor/Attribute/Current/Grid/" .. i .. "/Label"):GetComponent(typeof(UILabel))
    end
    for i = 2, 4 do
        self.m_CurrentLabels2[i-1] = self.transform:Find("Anchor/Attribute/Current/Grid (1)/" .. i .. "/Label"):GetComponent(typeof(UILabel))
    end

    self.m_LingHeTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadLingHe(ro)
    end)
    self.m_LingHeIcon.mainTexture = CUIManager.CreateModelTexture("__refineMonsterCircleWndLingHe20210204__", self.m_LingHeTextureLoader, 180, 0, -0.22, 2.2, false,true,1)

    self.m_Warming:SetActive(false)

    -- 旧版五行特效
    self.m_LianHuaFxRoot = self.transform:Find("Anchor/Center/LianHuaFxRoot").gameObject
    self.m_LianHuaFxRoot:SetActive(false)
    self.m_WuXingFx = self.transform:Find("Anchor/WuXing/WuXingFx"):GetComponent(typeof(CUIFx))
    self.m_WuXingBgFx = self.transform:Find("Anchor/WuXing/WuXingBgFx"):GetComponent(typeof(CUIFx))
    self.m_WuXingFx.gameObject:SetActive(false)
    self.m_WuXingBgFx.gameObject:SetActive(false)

    self.m_LingHeFx = self.transform:Find("Anchor/Center/LingHe/LingHeFx"):GetComponent(typeof(CUIFx))

    self.m_MonsterFx = self.transform:Find("Anchor/Center/Monster/vfx"):GetComponent(typeof(Animation))

    self.m_ItemBtnFx = self.transform:Find("Anchor/Bottom/ItemButton/CUIFx (1)").gameObject

    self.m_EvilBuffGo = self.transform:Find("Anchor/EvilBuff").gameObject
    self.m_EvilBuffTime = self.transform:Find("Anchor/EvilBuff/Time"):GetComponent(typeof(UILabel))
    self.m_EvilBuffDesc = self.transform:Find("Anchor/EvilBuff/Desc"):GetComponent(typeof(UILabel))
    self.m_EvilBuffBtn = self.transform:Find("Anchor/EvilBuff/Btn"):GetComponent(typeof(CButton))
    self.m_EvilBuffBgOn = self.transform:Find("Anchor/EvilBuff/BgOn").gameObject
    self.m_EvilBuffBgOff = self.transform:Find("Anchor/EvilBuff/BgOff").gameObject

    -- 关掉红点
    self.m_ItemBtnRedDot:SetActive(false)

    -- 云符迭代
    self.m_LingFuBgOn = self.transform:Find("Anchor/LingFuBuff/BgOn").gameObject
    self.m_LingFuOn = self.transform:Find("Anchor/LingFuBuff/On").gameObject
    self.m_LingFuOnBtn = self.transform:Find("Anchor/LingFuBuff/On/Btn").gameObject

    self.m_LingFuBgOff = self.transform:Find("Anchor/LingFuBuff/BgOff").gameObject
    self.m_LingFuOff = self.transform:Find("Anchor/LingFuBuff/Off").gameObject
    self.m_LingFuOffBtn = self.transform:Find("Anchor/LingFuBuff/Off/Btn").gameObject

    self.m_LingFuLevelDesc = self.transform:Find("Anchor/LingFuBuff/Desc/LevelDesc"):GetComponent(typeof(UILabel))
    self.m_LingFuRemain = self.transform:Find("Anchor/LingFuBuff/Remain"):GetComponent(typeof(UILabel))

    self.m_Anchor = self.transform:Find("Anchor").gameObject
end

function LuaRefineMonsterCircleNewWnd:DealNum(n)
    local n100 = math.floor(n*100)
    if n100 % 100 ==0 then
        return SafeStringFormat3("%d", n100/100)
    elseif n100 %10 == 0 then
        return SafeStringFormat3("%0.1f", n100/100)
    else
        return SafeStringFormat3("%0.2f", n100/100)
    end
end


function LuaRefineMonsterCircleNewWnd:Init()
    self.m_StatusTable = {}
    self.m_Tick = nil
    Gac2Gas.QuerySectFaZhenCurrentInfo()
    self:InitComponents()
    self:InitBtns()
    self.m_CurrentAtrribute = {}
    self:OnNiuZhuanWuXingResult(LuaZongMenMgr.m_WuXingBeforeNiuZhuan, LuaZongMenMgr.m_NiuZhuanWuXingResult, LuaZongMenMgr.m_NiuZhuanWuXingEndCDTime)
    self.m_Anchor:SetActive(false)
end

function LuaRefineMonsterCircleNewWnd:LoadLingHe(ro)
    if CClientMainPlayer.Inst then
        LuaZongMenMgr:CreateSoulCoreGameObject(ro.transform.parent.gameObject, CClientMainPlayer.Inst.SkillProp.SoulCore, CClientMainPlayer.Inst.BasicProp.MingGe)
    end
end

function LuaRefineMonsterCircleNewWnd:InitBtns()
    -- 放入灵核
    UIEventListener.Get(self.m_LingHePutInBtn).onClick = DelegateFactory.VoidDelegate(
                                                             function(p)
            if LuaZongMenMgr:CheckFaZhenOperation(LocalString.GetString("放入灵核")) then
                Gac2Gas.RequestPutSoulCoreIntoFaZhen()
            end
        end)

    -- 选择物品
    UIEventListener.Get(self.m_ItemBtn).onClick = DelegateFactory.VoidDelegate(
                                                      function(p)
            CUIManager.ShowUI(CLuaUIResources.RefineMonsterItemWnd)
        end)

    -- 规则描述
    UIEventListener.Get(self.m_RuleDescBtn).onClick = DelegateFactory.VoidDelegate(
                                                          function(p)
            g_MessageMgr:ShowMessage("LianHuaGuai_FaZhen_Introduction")
        end)

    -- 灵力描述
    UIEventListener.Get(self.m_LingLiDescBtn).onClick = DelegateFactory.VoidDelegate(
                                                            function(p)
            LuaZongMenMgr:ShowLingLiDescWnd(self.m_StatusTable, self.m_LingLiParams)
        end)

    -- 关闭按钮
    UIEventListener.Get(self.m_CloseBtn).onClick = DelegateFactory.VoidDelegate(
            function(p)
                self:OnClose()
        end)

    -- 炼化列表
    UIEventListener.Get(self.m_MonsterRefineListBtn).onClick = DelegateFactory.VoidDelegate(function(p)
            CUIManager.ShowUI(CLuaUIResources.RefineMonsterListWnd)
        end)

    -- 选择怪物放入法阵
    UIEventListener.Get(self.m_MonsterPutInSmallBtn).onClick = DelegateFactory.VoidDelegate(function(p)
            if LuaZongMenMgr:CheckFaZhenOperation(LocalString.GetString("投入炼化怪")) then
                CUIManager.ShowUI(CLuaUIResources.RefineMonsterChooseWnd)
            end
        end)

    UIEventListener.Get(self.m_MonsterPutInBigBtn).onClick = DelegateFactory.VoidDelegate(function(p)
            if LuaZongMenMgr:CheckFaZhenOperation(LocalString.GetString("投入炼化怪")) then
                CUIManager.ShowUI(CLuaUIResources.RefineMonsterChooseWnd)
            end
        end)

    UIEventListener.Get(self.m_ChangeMyWuXingButton).onClick = DelegateFactory.VoidDelegate(function(p)
            self:OnChangeMyWuXingButtonClick()
        end)    

    -- 获取云符
    UIEventListener.Get(self.m_LingFuOffBtn).onClick = DelegateFactory.VoidDelegate(function(p)
            CTrackMgr.Inst:FindNPC(20023218,16101788,129,137, nil, nil)
            CUIManager.CloseUI(CLuaUIResources.RefineMonsterCircleNewWnd)
        end)    
    UIEventListener.Get(self.m_LingFuOnBtn).onClick = DelegateFactory.VoidDelegate(function(p)
            CTrackMgr.Inst:FindNPC(20023218,16101788,129,137, nil, nil)
            CUIManager.CloseUI(CLuaUIResources.RefineMonsterCircleNewWnd)
        end)    

    CommonDefs.AddOnClickListener(self.m_EvilBuffBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        CUIManager.ShowUI(CLuaUIResources.RefineMonsterEvilBuffConfirmWnd)
    end), false)
end

function LuaRefineMonsterCircleNewWnd:OnChangeMyWuXingButtonClick()
    local msg = g_MessageMgr:FormatMessage("Use_RandomMinggeItemId_Confirm")
    local itemId = tonumber(Menpai_Setting.GetData("RandomMinggeItemId").Value)
    local sectId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId or 0
    LuaItemInfoMgr:ShowItemConsumeWndWithQuickAccess(itemId, 1, msg, true, false, LocalString.GetString("开启"), function (enough)
        if LuaZongMenMgr:CheckFaZhenOperation(LocalString.GetString("修改命盘")) then
            --Gac2Gas.RequestRandomWuXing_Ite(sectId)
            LuaZongMenMgr.m_EnableNewMingGeJianDing = true
            LuaZongMenMgr:OpenMingGeJianDingWnd()
        end
        LuaItemInfoMgr:CloseItemConsumeWnd()
    end, nil, false)
end

function LuaRefineMonsterCircleNewWnd:StartTheTick()
    -- 初始状态
    local currentLingLi = self.m_StatusTable["currentLingLi"]
    local lingliLimit = self.m_StatusTable["lingliLimit"]
    if currentLingLi > lingliLimit then
        currentLingLi = lingliLimit
    end
    
    self.m_ProgressTexture.fillAmount = 0
    self.m_LingLiCountLabel.text = SafeStringFormat3("%d/%d", 0, lingliLimit)

    local temp = 0
    local interval = 30
    self.m_Tick = CTickMgr.Register(DelegateFactory.Action(function()
        if temp >= 1000 then
            self.m_ProgressTexture.fillAmount = currentLingLi / lingliLimit
            self.m_LingLiCountLabel.text = SafeStringFormat3("%d/%d", currentLingLi, lingliLimit)
            temp = 1000
            if self.m_Tick ~= nil then
                invoke(self.m_Tick)
            end
        else
            self.m_ProgressTexture.fillAmount = (temp / 1000) * (currentLingLi / lingliLimit)
            self.m_LingLiCountLabel.text = SafeStringFormat3("%d/%d", (temp / 1000) * currentLingLi, lingliLimit)
            temp = temp + interval
        end
    end), 30, ETickType.Loop)
end

function LuaRefineMonsterCircleNewWnd:ShowCompareInfo(monsterData)
    if monsterData == nil then
        self.m_CompareAttribute:SetActive(false)
        self.m_CurrentAttribute:SetActive(true)
        return
    else
        self.m_CompareAttribute:SetActive(true)
        self.m_CurrentAttribute:SetActive(false)
    end

    local tujianData = ZhuoYao_TuJian.GetData(monsterData.TemplateId)
    if not self.m_StatusTable or not self.m_LingLiParams then
        return
    end

    local id = self.m_StatusTable["lingfuFactor"]
    local lingfu = Menpai_LingfuLastTime.GetData(id).Factor

    -- 灵力获取
    local LingLiParams = {}
    -- 基础灵根效率
    LingLiParams[1] = GetFormula(945)(nil, nil, {self.m_StatusTable["linggen"], self.m_StatusTable["beizhuoFactor"], self.m_StatusTable["damageFactor"], self.m_StatusTable["baoyangFactor"]})
    -- 怪物加成
    LingLiParams[2] = GetFormula(946)(nil, nil, {monsterData.Quality, 1, self.m_StatusTable["linggen"]})
    -- 门派加成
    LingLiParams[3] = GetFormula(947)(nil, nil, {self.m_StatusTable["menpaiAddFactor"], LingLiParams[1], LingLiParams[2]})
    -- 邪派加成
    LingLiParams[4] = GetFormula(948)(nil, nil, {self.m_StatusTable["xiepaiAddFactor"], LingLiParams[1], LingLiParams[2]})
    -- 综合获取效率
    LingLiParams[5] = GetFormula(949)(nil, nil, {LingLiParams[1], LingLiParams[2], LingLiParams[3], LingLiParams[4], 
                                self.m_StatusTable["wuxingFactor"], self.m_StatusTable["weatherFactor"], lingfu})
                                
    -- 显示属性
    local texts = {}
    local isUp = {}
    local isEqual = {}

    texts[1] = SafeStringFormat3(LocalString.GetString("%s灵力/分钟"), self:DealNum(LingLiParams[5]))
    if LingLiParams[5] == self.m_LingLiParams[5] then
        isEqual[1] = true
    else
        isEqual[1] = false
        isUp[1] = (LingLiParams[5] > self.m_LingLiParams[5])
    end

    texts[2] = SafeStringFormat3(monsterData.Quality)
    if monsterData.Quality == self.m_StatusTable["quality"] then
        isEqual[2] = true
    else
        isEqual[2] = false
        isUp[2] = (monsterData.Quality > self.m_StatusTable["quality"])
    end

    texts[3] = LuaZongMenMgr:ScondToString(tujianData.Time)
    if tujianData.Time == self.m_StatusTable["restTime"] then
        isEqual[3] = true
    else
        isEqual[3] = false
        isUp[3] = (tujianData.Time > self.m_StatusTable["restTime"])
    end

    for i = 1, 3 do
        local color = ""
        if not isEqual[i] then
            if isUp[i] then
                color = "[00FF00]"
            else
                color = "[FF5050]"
            end
        end
        self.m_CompareLabels[i].text = color .. texts[i]
        self.m_CompareLabels[i].color = Color(1,1,1,1)

        if isEqual[i] then
            self.m_CompareSprite[i].spriteName = ""
        elseif isUp[i] then
            self.m_CompareSprite[i].spriteName = "common_arrow_07_green"
        else
            self.m_CompareSprite[i].spriteName = "common_arrow_07_red"
        end
    end
end

-- 用于处理引导
function LuaRefineMonsterCircleNewWnd:GetGuideGo(methodName)
    if methodName == "GetLingHeGo" then
        if self.m_LingHePutInBtn.activeSelf then
            return self.m_LingHePutInBtn
        else
            return self.m_LingHeGuideGo
        end
    elseif methodName == "GetWuXingGo" then
        return self.m_WuXingGuideGo
    elseif methodName == "AttributeGuideGo" then
        return self.m_AttributeGuideGo
    elseif methodName == "LingFuGuideGo" then
        return self.m_LingFuGuideGo
    elseif methodName == "GetRefineMonsterGo" then
        if self.m_MonsterPutInBigBtn.activeSelf then
            return self.m_MonsterPutInBigBtn
        else
            return self.m_MonsterPutInSmallBtn
        end
    elseif methodName == "GetCloseBtn" then
        return self.m_CloseBtn
    elseif methodName == "GetItemBtn" then
        return self.m_ItemBtn
    end
end

function LuaRefineMonsterCircleNewWnd:OnClose()
    CUIManager.CloseUI(CLuaUIResources.RefineMonsterCircleWnd)
    CUIManager.CloseUI(CLuaUIResources.RefineMonsterCircleNewWnd)
    CUIManager.CloseUI(CLuaUIResources.RefineMonsterChooseWnd)
    CUIManager.CloseUI(CLuaUIResources.RefineMonsterItemWnd)
    CUIManager.CloseUI(CLuaUIResources.RefineMonsterListWnd)
    CUIManager.CloseUI(CLuaUIResources.RefineMonsterRecordWnd)
    CUIManager.CloseUI(CLuaUIResources.CommonItemChooseWnd)
    CUIManager.CloseUI(CLuaUIResources.RefineMonsterLingliDescWnd)
    CUIManager.CloseUI(CLuaUIResources.YaoGuaiInfoWnd)
end

function LuaRefineMonsterCircleNewWnd:ScondToString(sconds)
    local current = CServerTimeMgr.Inst.timeStamp
    sconds = sconds - current

    if sconds <= 0 then
        return SafeStringFormat3(LocalString.GetString("0分"))
    end

    local days = math.floor(sconds / 86400)
    local mod = math.fmod(sconds, 86400)
    local hours = math.floor( mod / 3600)
    mod = math.fmod(mod, 3600)
    local mins = math.floor(mod / 60)

    if days == 0 then
        if hours == 0 then
            if mins == 0 then
                mins = 1
            end
            return SafeStringFormat3(LocalString.GetString("%d分"), mins)
        else
            return SafeStringFormat3(LocalString.GetString("%d时"), hours)
        end
    else
        return SafeStringFormat3(LocalString.GetString("%d天"), days)
    end
end

function LuaRefineMonsterCircleNewWnd:RedDotDisable()
    self.m_ItemBtnRedDot:SetActive(false)
end

function LuaRefineMonsterCircleNewWnd:InitStatus(status)
    self.m_Anchor:SetActive(true)

    self.m_StatusTable = status
    LuaZongMenMgr.m_RefineMonsterStatusTable = status

    -- 小红点
    if self.m_StatusTable["bInputItem"] then
        self.m_ItemBtnRedDot:SetActive(false)
    else
        self.m_ItemBtnRedDot:SetActive(false)
    end

    local sconds =self.m_StatusTable["nextRefreshTime"] - CServerTimeMgr.Inst.timeStamp
    local timeStr = ""

    if sconds <= 0 then
        timeStr = SafeStringFormat3(LocalString.GetString("1小时内"))
    end
    local days = math.floor(sconds / 86400)
    local mod = math.fmod(sconds, 86400)
    local hours = math.floor( mod / 3600)
    mod = math.fmod(mod, 3600)
    local mins = math.floor(mod / 60)
    if days == 0 then
        if hours == 0 then
            timeStr = SafeStringFormat3(LocalString.GetString("1小时内"), mins)
        else
            timeStr = SafeStringFormat3(LocalString.GetString("%d时后"), hours)
        end
    else
        timeStr = SafeStringFormat3(LocalString.GetString("%d天后"), days)
    end

    -- 五行
    self.m_WuXingRefreshLabel.text = timeStr .. LocalString.GetString("刷新")
    self.m_FaZhenWuXingTexture:LoadMaterial(LuaZongMenMgr:GetMingGeMatPath(LuaZongMenMgr.m_NiuZhuanWuXingResult ~= 0 and LuaZongMenMgr.m_NiuZhuanWuXingResult or self.m_StatusTable["fazhenWuXing"]))
    self.m_LingHeWuXingTexture:LoadMaterial(LuaZongMenMgr:GetMingGeMatPath(self.m_StatusTable["myWuXing"]))

    local wuxingExtraDesc = ""
    if self.m_StatusTable["wuxingFactor"] and self.m_StatusTable["isInputSoulCore"] then

        -- 1蓝 2绿 3红
        local fxNum = {3, 3, 3, 1, 2, 2, 2}

        local fxList = {}
        local fxName = {"blue", "green", "red"}

        for i=1,3 do
            fxList[i] = self.transform:Find("Anchor/Center/Vfx/" .. fxName[i]).gameObject
        end

        local descStr = {LocalString.GetString("（强相克）"), LocalString.GetString("（中相克）"), LocalString.GetString("（弱相克）"), LocalString.GetString("（均衡）"),
            LocalString.GetString("（弱相生）"),LocalString.GetString("（中相生）"),LocalString.GetString("（强相生）")}
        local index = -1

        local params = LianHua_Setting.GetData().WuXingFxControlParams
        for i=0, params.Length-1 do
            if math.abs(params[i]-self.m_StatusTable["wuxingFactor"]) < 0.02 then
                index = i+1
                break
            end
        end

        if index>0 and index<8 then
            wuxingExtraDesc = descStr[index]
            
            for i=1,3 do
                fxList[i]:SetActive(fxNum[index] == i)
            end
        end
    end

    -- 灵力获取
    self.m_LingLiParams = {}
    -- 基础灵根效率
    self.m_LingLiParams[1] = GetFormula(945)(nil, nil, {self.m_StatusTable["linggen"], self.m_StatusTable["beizhuoFactor"], self.m_StatusTable["damageFactor"], self.m_StatusTable["baoyangFactor"]})
    
    -- 怪物加成
    self.m_LingLiParams[2] = GetFormula(946)(nil, nil, {self.m_StatusTable["quality"], self.m_StatusTable["isOwner"], self.m_StatusTable["linggen"]})
    -- 门派加成
    self.m_LingLiParams[3] = GetFormula(947)(nil, nil, {self.m_StatusTable["menpaiAddFactor"], self.m_LingLiParams[1],
                                                        self.m_LingLiParams[2]})
    -- 邪派加成
    self.m_LingLiParams[4] = GetFormula(948)(nil, nil, {self.m_StatusTable["xiepaiAddFactor"], self.m_LingLiParams[1],
                                                        self.m_LingLiParams[2]})
    
    local id = self.m_StatusTable["lingfuFactor"]
    local lingfu = Menpai_LingfuLastTime.GetData(id).Factor
    -- 综合获取效率
    self.m_LingLiParams[5] = GetFormula(949)(nil, nil,
                                 {self.m_LingLiParams[1], self.m_LingLiParams[2], self.m_LingLiParams[3],
                                  self.m_LingLiParams[4], self.m_StatusTable["wuxingFactor"], self.m_StatusTable["weatherFactor"], lingfu})
    LuaZongMenMgr.m_LingLiParams = self.m_LingLiParams
    
    -- 显示属性
    local colorStr = ""
    if self.m_StatusTable["wuxingFactor"] >1 then
        colorStr = "[00FF00]"
    elseif self.m_StatusTable["wuxingFactor"] <1 then
        colorStr = "[FF5050]"
    end
    self.m_CurrentLabels[1].text = colorStr .. tostring(self.m_StatusTable["wuxingFactor"]) ..wuxingExtraDesc
    self.m_CurrentLabels[2].text = SafeStringFormat3(LocalString.GetString("%s灵力/分钟"), self:DealNum(self.m_LingLiParams[5]))
    self.m_CurrentLabels[3].text = tostring(self.m_StatusTable["quality"])
    self.m_CurrentLabels[4].text = LuaZongMenMgr:ScondToString(self.m_StatusTable["restTime"])

    self.m_CurrentLabels2[1].text = SafeStringFormat3(LocalString.GetString("%s灵力/分钟"), self:DealNum(self.m_LingLiParams[5]))
    self.m_CurrentLabels2[2].text = tostring(self.m_StatusTable["quality"])
    self.m_CurrentLabels2[3].text = LuaZongMenMgr:ScondToString(self.m_StatusTable["restTime"])
    self.m_CompareAttribute:SetActive(false)

    -- 灵核系数
    local lingheAttribute = self.transform:Find("Anchor/Attribute/Current/1").gameObject
    -- 灵核
    if self.m_StatusTable["isInputSoulCore"] then
        self.m_LingHeIcon.gameObject:SetActive(true)
        self.m_LingHePutInBtn:SetActive(false)
        self.m_LingHeGuideGo:SetActive(true)
        self.m_LingHeWuXingBg:SetActive(true)
        self.m_LingHeWuXingTitle:SetActive(true)
        self.m_LingHeWuXingTexture.gameObject:SetActive(true)
        self.m_LingLiCountLabel.gameObject:SetActive(true)
        self.m_ProgressTexture.gameObject:SetActive(true)
        self.m_LingHePanel:SetActive(true)
        
        lingheAttribute:SetActive(true)
        self:StartTheTick()
        local sectId = ((CClientMainPlayer.Inst or {}).BasicProp or {}).SectId
        if sectId ~= nil then
            Gac2Gas.QuerySectEvilValue(sectId)
        end
    else
        self.m_LingHePanel:SetActive(false)
        self.m_ProgressTexture.gameObject:SetActive(false)
        self.m_LingHeIcon.gameObject:SetActive(false)
        self.m_LingHePutInBtn:SetActive(true)
        self.m_LingHeGuideGo:SetActive(false)
        self.m_LingHeWuXingTexture.gameObject:SetActive(false)
        self.m_LingHeWuXingBg:SetActive(false)
        self.m_LingHeWuXingTitle:SetActive(false)
        self.m_LingLiCountLabel.gameObject:SetActive(false)
        self.m_CurrentLabels[1].text = "-"
        self.m_CurrentLabels[2].text = "-"
        self.m_CurrentLabels2[1].text = "-"

        -- 没放入灵核也要播一下动画
        -- 放入的话 依赖QuerySectEvilValue的返回
        local ani = self.transform:GetComponent(typeof(Animation))
        ani:Play("refinemonstercirclewnd_show_1")

        lingheAttribute:SetActive(false)
    end

    -- 炼化怪
    if self.m_StatusTable["templateId"] == nil or self.m_StatusTable["templateId"] == 0 then
        self.m_MonsterIcon.gameObject:SetActive(false)
        self.m_MonsterPutInBigBtn:SetActive(true)
        self.m_MonsterPutInSmallBtn:SetActive(false)
        self.m_MonsterRefineListBtn:SetActive(false)
        self.m_CurrentLabels[3].text = "-"
        self.m_CurrentLabels[4].text = "-"
        self.m_CurrentLabels2[2].text = "-"
        self.m_CurrentLabels2[3].text = "-"
    else
        self.m_MonsterIcon.gameObject:SetActive(true)
        self.m_MonsterPutInBigBtn:SetActive(false)
        self.m_MonsterPutInSmallBtn:SetActive(true)
        self.m_MonsterRefineListBtn:SetActive(true)
        local data = ZhuoYao_TuJian.GetData(self.m_StatusTable["templateId"])
        if data then
            self.m_MonsterIcon:LoadNPCPortrait(data.Icon)
        end
    end

    -- 炼化怪正在炼化
    if self.m_StatusTable["isInputSoulCore"] and self.m_StatusTable["templateId"] ~= 0 then
        self.m_LianHuaBg:SetActive(true)
        self.m_ItemBtn:SetActive(true)
        self.m_MonsterFx.gameObject:SetActive(true)

        if not self.m_MonsterFx.isPlaying then
            self.m_MonsterFx:Play("refinemonstercirclewnd_lianhuaguai_1")
        end
        self.m_ItemBtnFx:SetActive(true)
        --self.m_Warming:SetActive(false)
        --self.m_ItemBtnLabel.color = Color(53/255,27/255,1/255)
    else
        self.m_ItemBtn:SetActive(true)
        self.m_LianHuaBg:SetActive(false)
        self.m_MonsterFx.gameObject:SetActive(false)
        self.m_ItemBtnFx:SetActive(false)
        --self.m_Warming:SetActive(true)
        --self.m_ItemBtnLabel.color = Color(14/255,50/255,84/255)
    end

    -- 灵符
    local lingfuFactor = self.m_StatusTable["lingfuFactor"]
    local lingfuCount = self.m_StatusTable["lingfuCount"]
    local lingfuRemainTime = self.m_StatusTable["lingfuRemainTime"]
	self:InitLingfu(lingfuFactor, lingfuCount, lingfuRemainTime)
end

function LuaRefineMonsterCircleNewWnd:InitLingfu(lingfuFactor, lingfuCount, lingfuRemainTime)
    local lingfuLv = 0
    local lingFuDesc = ""
    local lingfuRealFactor = 1

    Menpai_LingfuLastTime.Foreach(function (id, data)
        if lingfuCount > data.Low and lingfuCount <= data.High then
            lingfuLv = data.Level
            lingFuDesc = data.LevelDesc
            lingfuRealFactor = data.Factor
        elseif lingfuCount == 0 and data.Level == 0 then
            lingfuLv = data.Level
            lingFuDesc = data.LevelDesc
            lingfuRealFactor = data.Factor
        end
    end)

    self.m_LingFuLevelDesc.text = lingFuDesc
    self.m_LingFuRemain.text = SafeStringFormat3(LocalString.GetString("当前数量 %s"), tostring(lingfuCount))

    self.m_LingFuBgOff:SetActive(lingfuLv == 0)
    self.m_LingFuOff:SetActive(lingfuLv == 0)

    self.m_LingFuBgOn:SetActive(lingfuLv > 0)
    self.m_LingFuOn:SetActive(lingfuLv > 0)

    if lingfuLv > 0 then
        CUICommonDef.SetActive(self.m_LingFuLevelDesc.gameObject, true, true)

        -- 时间与系数
        local timeLabel = self.m_LingFuOn.transform:Find("Time"):GetComponent(typeof(UILabel))
        local factorLabel = self.m_LingFuOn.transform:Find("Buff/Label"):GetComponent(typeof(UILabel))

        local restTime = lingfuRemainTime - math.floor(CServerTimeMgr.Inst.timeStamp)
        restTime = lingfuRemainTime
        if restTime >0 then
            local day = math.floor(restTime / (3600 * 24))
            local hour = math.floor((restTime %(3600 * 24))/3600)

            if day == 0 and hour == 0 then
                hour = 1
            end
            if day>0 then
                timeLabel.text = SafeStringFormat3(LocalString.GetString("剩余%s天%s时"), day, hour)
            else
                timeLabel.text = SafeStringFormat3(LocalString.GetString("剩余%s时"), hour)
            end
        else
            timeLabel.text = LocalString.GetString("剩余0时")
        end
        factorLabel.text = SafeStringFormat3(LocalString.GetString("%s"), self:DealNum(lingfuRealFactor + 0.0001))

        self.m_LingFuBgOn.transform:Find("Dot").gameObject:SetActive(true)
        self.m_LingFuBgOn.transform:Find("Icon").gameObject:SetActive(true)
        self.m_LingFuBgOn.transform:Find("Level").gameObject:SetActive(true)
        
        -- 等级
        for i = 1, 5 do
            self.m_LingFuBgOn.transform:Find("Dot/" .. i).gameObject:SetActive(lingfuLv >= i)
            self.m_LingFuBgOn.transform:Find("Icon/" .. i).gameObject:SetActive(lingfuLv == i)
            self.m_LingFuBgOn.transform:Find("Level/" .. i).gameObject:SetActive(lingfuLv == i)
        end
    else
        CUICommonDef.SetActive(self.m_LingFuLevelDesc.gameObject, false, true)
    end
end

function LuaRefineMonsterCircleNewWnd:PutMonsterIntoFaZhen()
    CUIManager.CloseUI(CLuaUIResources.RefineMonsterChooseWnd)
    self.m_MonsterFx.gameObject:SetActive(true)
    self.m_MonsterFx:Play("refinemonstercirclewnd_lianhuaguai")
    Gac2Gas.QuerySectFaZhenCurrentInfo()
end

function LuaRefineMonsterCircleNewWnd:PutSoulCoreIntoFaZhen()
    self.m_LingHeFx:DestroyFx()
    self.m_LingHeFx:LoadFx("fx/ui/prefab/UI_fazhenlianhua_3.prefab")
    Gac2Gas.QuerySectFaZhenCurrentInfo()
end

function LuaRefineMonsterCircleNewWnd:RemoveMonsterFromFaZhen()
    self.m_MonsterFx.gameObject:SetActive(false)
    Gac2Gas.QuerySectFaZhenCurrentInfo()
end

function LuaRefineMonsterCircleNewWnd:OnSyncSectEvilValue(evilValue, bEvil)
    self.m_EvilBuffGo:SetActive(bEvil)

    if self.m_EvilState == nil or self.m_EvilState ~= bEvil then
        self.m_EvilState = bEvil
        local ani = self.transform:GetComponent(typeof(Animation))
        if bEvil then
            ani:Play("refinemonstercirclewnd_show")
        else
            ani:Play("refinemonstercirclewnd_show_1")
        end
    end
    
    if bEvil then
        local sectId = ((CClientMainPlayer.Inst or {}).BasicProp or {}).SectId
        if sectId ~= nil then
            Gac2Gas.QueryXiepaiExtraRewardSwitch(sectId)
        end
    end
end

function LuaRefineMonsterCircleNewWnd:OnSyncXiepaiExtraRewardSwitch(sectId, bOpen, leftTime)
    local buffGo = self.m_EvilBuffGo.transform:Find("Buff").gameObject
    if CClientMainPlayer.Inst and sectId == CClientMainPlayer.Inst.BasicProp.SectId and bOpen then
        LuaZongMenMgr.m_RefineMonsterOpenEvil = true

        self.m_EvilBuffTime.text = LocalString.GetString("剩余") .. self:ScondToString(leftTime)
        self.m_EvilBuffDesc.text = LocalString.GetString("已开启邪派加成")
        self.m_EvilBuffTime.gameObject:SetActive(true)
        self.m_EvilBuffBtn.gameObject:SetActive(false)
        self.m_EvilBuffBgOn:SetActive(true)
        self.m_EvilBuffBgOff:SetActive(false)
        buffGo:SetActive(true)
    else
        self.m_EvilBuffDesc.text = LocalString.GetString("邪派炼化加成")
        self.m_EvilBuffTime.gameObject:SetActive(false)
        self.m_EvilBuffBtn.gameObject:SetActive(true)
        self.m_EvilBuffBgOn:SetActive(false)
        self.m_EvilBuffBgOff:SetActive(true)
        buffGo:SetActive(false)
    end
end

function LuaRefineMonsterCircleNewWnd:OnEnable()
    LuaZongMenMgr.m_RefineMonsterOpenEvil = false

    local taskId = LianHua_Setting.GetData().ChuShiLianHuaGuideTaskId
    if CClientMainPlayer.Inst and CommonDefs.ListContains(CClientMainPlayer.Inst.TaskProp.CurrentTaskList, typeof(UInt32), taskId) then
        local task = CommonDefs.DictGetValue(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), taskId)
        if task ~= nil and  task.CanSubmit == 0 then
            Gac2Gas.FinishChuShiLianHuaGuideTask()
        end
    end
    
    g_ScriptEvent:AddListener("QuerySectFaZhenCurrentInfoResult", self, "InitStatus")
    g_ScriptEvent:AddListener("PutMonsterIntoFaZhenSuccess", self, "PutMonsterIntoFaZhen")
    g_ScriptEvent:AddListener("PutSoulCoreIntoFaZhenSuccess", self, "PutSoulCoreIntoFaZhen")
    g_ScriptEvent:AddListener("RefineMonsterSelectMonsterToCompare", self, "ShowCompareInfo")
    g_ScriptEvent:AddListener("RefineMonsterItemRotDotDisable", self, "RedDotDisable")
    g_ScriptEvent:AddListener("RemoveFaZhenLianHuaMonsterSuccess", self, "RemoveMonsterFromFaZhen")
    g_ScriptEvent:AddListener("SyncSectEvilValue", self, "OnSyncSectEvilValue")
    g_ScriptEvent:AddListener("SyncXiepaiExtraRewardSwitch", self, "OnSyncXiepaiExtraRewardSwitch")
    g_ScriptEvent:AddListener("OnNiuZhuanWuXingResult", self, "OnNiuZhuanWuXingResult")
end

function LuaRefineMonsterCircleNewWnd:OnDisable()
    if self.m_Tick ~= nil then
        UnRegisterTick(self.m_Tick)
    end
    CUIManager.DestroyModelTexture("__refineMonsterCircleWndLingHe20210204__")

    g_ScriptEvent:RemoveListener("QuerySectFaZhenCurrentInfoResult", self, "InitStatus")
    g_ScriptEvent:RemoveListener("PutMonsterIntoFaZhenSuccess", self, "PutMonsterIntoFaZhen")
    g_ScriptEvent:RemoveListener("PutSoulCoreIntoFaZhenSuccess", self, "PutSoulCoreIntoFaZhen")
    g_ScriptEvent:RemoveListener("RefineMonsterSelectMonsterToCompare", self, "ShowCompareInfo")
    g_ScriptEvent:RemoveListener("RefineMonsterItemRotDotDisable", self, "RedDotDisable")
    g_ScriptEvent:RemoveListener("RemoveFaZhenLianHuaMonsterSuccess", self, "RemoveMonsterFromFaZhen")
    g_ScriptEvent:RemoveListener("SyncSectEvilValue", self, "OnSyncSectEvilValue")
    g_ScriptEvent:RemoveListener("SyncXiepaiExtraRewardSwitch", self, "OnSyncXiepaiExtraRewardSwitch")
    g_ScriptEvent:RemoveListener("OnNiuZhuanWuXingResult", self, "OnNiuZhuanWuXingResult")
end

function LuaRefineMonsterCircleNewWnd:OnNiuZhuanWuXingResult(lastWuXing, wuXing, endCDTime)
    self.m_NiuZhuanWuXingTag:SetActive(LuaZongMenMgr.m_NiuZhuanWuXingResult ~= 0 and endCDTime > CServerTimeMgr.Inst.timeStamp)
    self.m_WuXingRefreshLabel.gameObject:SetActive(LuaZongMenMgr.m_NiuZhuanWuXingResult == 0)
    local lastWuXing = SoulCore_WuXing.GetData(lastWuXing)
    local wuXing = SoulCore_WuXing.GetData(wuXing)
    local lastWuXingName = lastWuXing and lastWuXing.Name or ""
    local wuXingName = wuXing and wuXing.Name or ""
    local t = CServerTimeMgr.ConvertTimeStampToZone8Time(endCDTime)
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local delta = t:Subtract(now)
    self.m_NiuZhuanWuXingDesLabel.text = g_MessageMgr:FormatMessage("WuXingNiuZhuan_CD_Text", lastWuXingName,  wuXingName)
    self.m_NiuZhuanWuXingCDLabel.text = SafeStringFormat3("%02d:%02d:%02d",delta.Hours, delta.Minutes, delta.Seconds)
    if LuaZongMenMgr.m_NiuZhuanWuXingResult ~= 0 then
        self.m_FaZhenWuXingTexture:LoadMaterial(LuaZongMenMgr:GetMingGeMatPath(LuaZongMenMgr.m_NiuZhuanWuXingResult))
    end
end
