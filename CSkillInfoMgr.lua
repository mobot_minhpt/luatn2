-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CSkillMgr = import "L10.Game.CSkillMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local EnumIdentifySkillSource = import "L10.UI.CSkillInfoMgr+EnumIdentifySkillSource"
local EnumSkillInfoContext = import "L10.UI.CSkillInfoMgr+EnumSkillInfoContext"

CSkillInfoMgr.m_ShowSkillInfoWnd_UInt32_String_Boolean_Vector3_EnumSkillInfoContext_Boolean_UInt32_Double_CPropertySkill_CS2LuaHook = function (skillId, equipId, alignByTopPos, topWorldPos, context, showSelfSkillInfo, playerLevel, xiuweiGrade, skillProp) 
    if CSkillMgr.Inst:IsIdentifySkill(CLuaDesignMgr.Skill_AllSkills_GetClass(skillId)) then
        local source = EnumIdentifySkillSource.Undefined
        if showSelfSkillInfo then
            source = EnumIdentifySkillSource.MainPlayer
        elseif context == EnumSkillInfoContext.ShowPlayerSkillInfo then
            source = EnumIdentifySkillSource.OtherPlayer
        else
            return
        end
        CSkillInfoMgr.ShowIdentifySkillInfoWnd(skillId, equipId, alignByTopPos, topWorldPos, context, source)
    else
        CSkillInfoMgr.ShowSkillInfoWnd(skillId, alignByTopPos, topWorldPos, context, showSelfSkillInfo, playerLevel, xiuweiGrade, skillProp)
    end
end
CSkillInfoMgr.m_ShowTianFuSkillWnd_CS2LuaHook = function (type, defaultIndex) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    CSkillInfoMgr.TianFuSkillType = type
    CSkillInfoMgr.DefaultTianFuTabIndex = defaultIndex
    local xiuweiGrade = math.floor(CClientMainPlayer.Inst.BasicProp.XiuWeiGrade)
    --if (xiuweiGrade < Constants.TianFuSkillUnlockLevel)
    --{
    --    MessageMgr.Inst.ShowMessage("TIANFU_SKILL_OPEN_FAIL_LEVEL_NOT_40", new object[] { xiuweiGrade });
    --}
    --else
    CUIManager.ShowUI(CUIResources.TianFuSkillWnd)
end
