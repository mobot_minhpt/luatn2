local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaNewHorseRaceMgr = {}
LuaNewHorseRaceMgr.m_CurColor = nil
LuaNewHorseRaceMgr.m_SpeedGather = nil
LuaNewHorseRaceMgr.m_SpeedUpEndTime = nil

function LuaNewHorseRaceMgr:OnSaiMaStartCountDown()
    --CameraFollow.m_MinRotationEnable = true
    CRenderScene.Inst.EnableChunkDynamicHide = false
    CUIManager.CloseUI(CUIResources.HorseRaceSelectWnd)
    self:AdjustCameraDir()
end

function LuaNewHorseRaceMgr:MPTYSShowStartPlayFx()
    EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {CUIFxPaths.HorseRaceStartFx})
end

function LuaNewHorseRaceMgr:AdjustCameraDirByPlayerDic()
    if CClientMainPlayer.Inst == nil then
        return
    end

    local heightOffset = tonumber((MaPiTengYunSai_SettingV2.GetData("CameraHeightOffset") or {}).Value or 0)
    local Z = tonumber((MaPiTengYunSai_SettingV2.GetData("CameraZ") or {}).Value or 12)
    local Y = tonumber((MaPiTengYunSai_SettingV2.GetData("CameraY") or {}).Value or 9.5)

    local vec3 = CameraFollow.Inst.targetRZY
    vec3.x = 270 - CClientMainPlayer.Inst.Dir
    vec3.y = Z
    vec3.z = Y
    CameraFollow.Inst.MaxSpHeight = heightOffset
    
    CameraFollow.Inst.targetRZY = vec3
    CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
end

function LuaNewHorseRaceMgr:AdjustCameraDir(angle)
    if CameraFollow.Inst == nil then
        return
    end

    if angle == nil then
        self:AdjustCameraDirByPlayerDic()
        return
    end

    local heightOffset = tonumber((MaPiTengYunSai_SettingV2.GetData("CameraHeightOffset") or {}).Value or 0)
    local Z = tonumber((MaPiTengYunSai_SettingV2.GetData("CameraZ") or {}).Value or 12)
    local Y = tonumber((MaPiTengYunSai_SettingV2.GetData("CameraY") or {}).Value or 9.5)

    local vec3 = CameraFollow.Inst.targetRZY
    vec3.x = angle
    vec3.y = Z
    vec3.z = Y
    CameraFollow.Inst.MaxSpHeight = heightOffset
    CameraFollow.Inst.targetRZY = vec3
    CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
end

function LuaNewHorseRaceMgr:PlayerLeaveSaiMaJumpRegion(engineId, result, bSkill, forwardSpeed)
    if CClientMainPlayer.Inst.EngineId == engineId then
        self:DoJumpAnim(CClientMainPlayer.Inst, forwardSpeed)
    else
        local otherPlayer = CClientObjectMgr.Inst:GetObject(engineId)
        self:DoJumpAnim(otherPlayer, forwardSpeed)
    end
    g_ScriptEvent:BroadcastInLua("PlayerLeaveSaiMaJumpRegion", engineId, result, bSkill, forwardSpeed)
end

function LuaNewHorseRaceMgr:DoJumpAnim(player, speed)
    if player then
        player:OnEnter_Rushing()
    end
end

function LuaNewHorseRaceMgr:SyncSaiMaAccelerateInfo(hitType, colour, duration, timeCount)
    LuaNewHorseRaceMgr.m_CurColor = colour
    LuaNewHorseRaceMgr.m_SpeedGather = timeCount
    LuaNewHorseRaceMgr.m_SpeedUpEndTime = CServerTimeMgr.Inst:GetZone8Time():AddSeconds(duration)
    g_ScriptEvent:BroadcastInLua("SyncSaiMaAccelerateInfo", hitType, colour, duration, timeCount)
end