-- Auto Generated!!
local CHunPoMgr = import "L10.UI.CHunPoMgr"
local CHunPoWnd = import "L10.UI.CHunPoWnd"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local LocalString = import "LocalString"
local StringBuilder = import "System.Text.StringBuilder"
CHunPoWnd.m_OnQueryPlayerBasicInfoDone_CS2LuaHook = function (this, playerId, online) 
    if CHunPoMgr.hunpo.Item:GetHunPoPlayerId() == playerId then
        local info = CHunPoMgr.playerInfo
        if info == nil then
            return
        end
        local data = info.zhaoHunData
        if data == nil then
            return
        end
        if CHunPoMgr.hunpo.Id ~= data.HunPoItemId then
            this.tipsLabel.text = System.String.Format(LocalString.GetString("[c][FFFF00]{0}[-]已完成招魂"), info.name)
            this.background:ResetAndUpdateAnchors()
            return
        end
        local builder = NewStringBuilderWraper(StringBuilder)
        builder:AppendLine(System.String.Format(LocalString.GetString("[c][FFFF00]{0}[-]的招魂进度：[c][FFFF00]{1}/100[-]"), info.name, data.CurrentProgress))
        builder:AppendLine(LocalString.GetString("当前正在进行的招魂任务："))
        local task = data:GetCurrentTask()
        if task ~= nil then
            builder:AppendLine(System.String.Format(LocalString.GetString("[c][FFFF00]{0}[-]"), task.TaskTitle))
            builder:Append(task.TaskDesc)
        else
            builder:Append(LocalString.GetString("[c][FFFF00]无"))
        end
        this.tipsLabel.text = ToStringWrap(builder)
        this.background:ResetAndUpdateAnchors()
    end
end
CHunPoMgr.m_OpenHunPoTips_CS2LuaHook = function (item) 
    if item ~= nil and item.IsItem and item.Item.IsHunPo then
        CHunPoMgr.hunpo = item
        CUIManager.ShowUI(CUIResources.HunPoWnd)
    end
end
