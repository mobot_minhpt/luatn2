local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UITexture = import "UITexture"
local DelegateFactory = import "DelegateFactory"
local UISprite = import "UISprite"
local Extensions = import "Extensions"
local Vector3 = import "UnityEngine.Vector3"
local Object = import "System.Object"
local MessageWndManager = import "L10.UI.MessageWndManager"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local Time = import "UnityEngine.Time"
local TweenPosition = import "TweenPosition"

LuaRabbitJumpWnd = class()
RegistChildComponent(LuaRabbitJumpWnd,"CloseButton", GameObject)
RegistChildComponent(LuaRabbitJumpWnd,"jumpBtn", GameObject)
RegistChildComponent(LuaRabbitJumpWnd,"m_FakeModel", UITexture)
RegistChildComponent(LuaRabbitJumpWnd,"slider", UISprite)
RegistChildComponent(LuaRabbitJumpWnd,"jumpNode", GameObject)
RegistChildComponent(LuaRabbitJumpWnd,"panelTemplate", GameObject)
RegistChildComponent(LuaRabbitJumpWnd,"rabbitNode", GameObject)
RegistChildComponent(LuaRabbitJumpWnd,"panelTemplate1", GameObject)
RegistChildComponent(LuaRabbitJumpWnd,"failNode", GameObject)
RegistChildComponent(LuaRabbitJumpWnd,"restartBtn", GameObject)
RegistChildComponent(LuaRabbitJumpWnd,"succNode", GameObject)
RegistChildComponent(LuaRabbitJumpWnd,"greenBar", UISprite)
RegistChildComponent(LuaRabbitJumpWnd,"jumpSilder", UISlider)

RegistClassMember(LuaRabbitJumpWnd, "m_ModelTextureLoader")
RegistClassMember(LuaRabbitJumpWnd, "m_RenderObject")
RegistClassMember(LuaRabbitJumpWnd, "m_BtnPressSpeed")
RegistClassMember(LuaRabbitJumpWnd, "m_BtnPressMaxHeight")
RegistClassMember(LuaRabbitJumpWnd, "m_BtnPressNowHeight")
RegistClassMember(LuaRabbitJumpWnd, "m_BtnPressNowDirection")
RegistClassMember(LuaRabbitJumpWnd, "m_BtnPressStatus")
RegistClassMember(LuaRabbitJumpWnd, "m_RabbitReady")
RegistClassMember(LuaRabbitJumpWnd, "m_Tick")

RegistClassMember(LuaRabbitJumpWnd, "m_JumpPanelData")
RegistClassMember(LuaRabbitJumpWnd, "m_StartJump")
RegistClassMember(LuaRabbitJumpWnd, "m_JumpSpeedX")
RegistClassMember(LuaRabbitJumpWnd, "m_JumpSpeedY")
RegistClassMember(LuaRabbitJumpWnd, "m_JumpTotalTime")
RegistClassMember(LuaRabbitJumpWnd, "m_JumpG")
RegistClassMember(LuaRabbitJumpWnd, "m_JumpStayPanelIndex")
RegistClassMember(LuaRabbitJumpWnd, "m_JumpFatherNodeX")
RegistClassMember(LuaRabbitJumpWnd, "m_JumpFatherNodeY")

function LuaRabbitJumpWnd:Close()
	MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Confirm_CloseWindow"), DelegateFactory.Action(function ()
		CUIManager.CloseUI(CLuaUIResources.RabbitJumpWnd)
	end), nil, nil, nil, false)
end

function LuaRabbitJumpWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaRabbitJumpWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaRabbitJumpWnd:OnJumpClick()
	self:RabbitJump()
end

function LuaRabbitJumpWnd:Init()
	local onCloseClick = function(go)
		self:Close()
	end

	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)

	--CommonDefs.AddOnPressListener(self.jumpBtn,DelegateFactory.Action_GameObject_bool(function(go, pressed)
  --  self:OnJumpPress(pressed)
  --end),false)
	local onJumpClick = function(go)
		self:OnJumpClick()
	end

	CommonDefs.AddOnClickListener(self.jumpBtn,DelegateFactory.Action_GameObject(onJumpClick),false)

	local onRestartClick = function(go)
		self:RestartGame()
	end
	self.failNode:SetActive(false)
	self.succNode:SetActive(false)

	CommonDefs.AddOnClickListener(self.restartBtn,DelegateFactory.Action_GameObject(onRestartClick),false)

	self.panelTemplate:SetActive(false)
	self.panelTemplate1:SetActive(false)

	self.m_BtnPressSpeed = 100
	self.m_BtnPressMaxHeight = 322
	self.m_BtnPressNowHeight = 0
	self.panelWidth = 518
	self.m_JumpSpeedX = 700
	self.m_JumpG = 2000
	self.setJumpSpeedY = 2000
	self.jumpSilder.value = 0.5

	self:InitPanelInfo()
	self:InitRabbit()
end

function LuaRabbitJumpWnd:InitJumpNodePos()
	local fatherNodeStartPosX,fatherNodeStartPosY = -500,-100
	self.m_JumpFatherNodeX,self.m_JumpFatherNodeY = fatherNodeStartPosX,fatherNodeStartPosY
	self.jumpNode.transform.localPosition = Vector3(fatherNodeStartPosX,fatherNodeStartPosY,0)
end

function LuaRabbitJumpWnd:InitPanelInfo()
	local panelDataTable = {}
	local panelNum = 10
	local panelHeight = 140
	local panelWidth = 550
	local panelRandomWidth = 180
	local panelRandomHeight = 50

	local lastSaveX,lastSaveY = 0,0
	for i=1,panelNum do
		local height = panelHeight + math.random(1,panelRandomHeight) + lastSaveY
		local width = panelWidth + math.random(1,panelRandomWidth) + lastSaveX
		if i == 1 then
			height,width = 0,0
		end
		table.insert(panelDataTable,{width,height})
		lastSaveX = width
		lastSaveY = height
	end

	self:InitJumpNodePos()

	self.m_JumpPanelData = panelDataTable

	Extensions.RemoveAllChildren(self.jumpNode.transform)
	for i,v in ipairs(panelDataTable) do
		local tempNode = self.panelTemplate
		if i == #panelDataTable then
			tempNode = self.panelTemplate1
		end
		local node = NGUITools.AddChild(self.jumpNode,tempNode)
		node:SetActive(true)
		node.transform.localPosition = Vector3(v[1],v[2],0)
	end
end

function LuaRabbitJumpWnd:OnJumpPress(pressState)
	self.m_BtnPressStatus = pressState
	if pressState then
		self.m_BtnPressNowHeight = 0
		self.m_BtnPressNowDirection = 0
		self.m_RabbitReady = true
	else

	end
end

function LuaRabbitJumpWnd:Success()
	self.gameOver = true
	RegisterTickWithDuration(function ()
		self.succNode:SetActive(true)
		local empty = CreateFromClass(MakeGenericClass(List, Object))
		Gac2Gas.FinishClientTaskEvent(LuaZhuJueJuQingMgr.TaskId,"RabbitJump",MsgPackImpl.pack(empty))
	end,1000,1000)
	RegisterTickWithDuration(function ()
		CUIManager.CloseUI(CLuaUIResources.RabbitJumpWnd)
	end,4000,4000)
end

function LuaRabbitJumpWnd:Fail()
	if self.gameOver then
		return
	end
	--CUIManager.CloseUI(CLuaUIResources.RabbitJumpWnd)
	if not self.gameOver then
		self.gameOver = true
	end

	if self.m_FailTick then
		UnRegisterTick(self.m_FailTick)
		self.m_FailTick = nil
	end
	self.m_FailTick = RegisterTickWithDuration(function ()
		self.failNode:SetActive(true)
	end,1000,1000)
end

function LuaRabbitJumpWnd:RestartGame()
	self.failNode:SetActive(false)
	self.succNode:SetActive(false)
	self.gameOver = false

	if not self.failTime then
		self.failTime = 0
	end

	self.failTime = self.failTime + 1
	self.jumpBtn:SetActive(true)
	--self.slider.fillAmount = 0
	self.jumpSilder.value = 0.5
	self:InitJumpNodePos()
	--self:InitPanelInfo()
	self:ResetRabbit()
end

function LuaRabbitJumpWnd:DoJumpEndAni()
	self.m_RenderObject:DoAni('jump01_end',false,0,1,0.15,true,1)
	local endTime = 400
	if self.m_JumpEndTick then
		UnRegisterTick(self.m_JumpEndTick)
		self.m_JumpEndTick = nil
	end
	self.m_JumpEndTick = RegisterTickWithDuration(function ()
		self.m_RenderObject:DoAni('stand01',true,0,1,0.15,true,1)
	end,endTime,endTime)
end

function LuaRabbitJumpWnd:CheckJumpState(pos1,pos2)
	local stay = false
	local panelWidth = self.panelWidth
	local stayPos = nil
	for i,v in ipairs(self.m_JumpPanelData) do
		if v[1] + panelWidth/2 > pos1.x and v[1] - panelWidth/2 < pos1.x and v[1] + panelWidth/2 > pos2.x and v[1] - panelWidth/2 < pos2.x then
			if v[2] <= pos1.y and v[2] > pos2.y then
				stay = true
				self.m_JumpStayPanelIndex = i
				stayPos = {pos1.x,v[2]}
				self:DoJumpEndAni()
			end
		end
	end

	if stay then
		self.m_StartJump = false
		--self.jumpBtn:SetActive(true)
		local t = 300
		TweenPosition.Begin(self.jumpNode,t/1000,Vector3(-stayPos[1]+self.m_JumpFatherNodeX,-stayPos[2]+self.m_JumpFatherNodeY,0))
		self.m_Tick = RegisterTickWithDuration(function ()
			if self.jumpBtn then
				self.jumpBtn:SetActive(true)
				--self.slider.fillAmount = 0
				--self.jumpSilder.value = 0
				if self.m_JumpStayPanelIndex == #self.m_JumpPanelData then
					self:Success()
				end
			end
		end,t,t)
	else
		if pos2.y < self.m_JumpPanelData[self.m_JumpStayPanelIndex][2] then
			self:Fail()
		end
	end

	return stay,stayPos
end

function LuaRabbitJumpWnd:CalJumpSpeed(point1,point2)
	local time = (point2[1] - point1[1])/self.m_JumpSpeedX
	local jumpY = (point2[2] - point1[2])/time + self.m_JumpG * time / 2
	local percent = jumpY / self.setJumpSpeedY
	return percent
end

function LuaRabbitJumpWnd:CalNextJump()
	self.greenBar.gameObject:SetActive(false)
	if not self.failTime or self.failTime < 3 then
		return
	end
	local nowIndex = self.m_JumpStayPanelIndex
	if self.m_JumpPanelData[nowIndex+1] then
		local nextData = self.m_JumpPanelData[nowIndex+1]
		local nextMin = {nextData[1] - self.panelWidth/2,nextData[2]}
		local nextMid = {nextData[1],nextData[2]}
		local nextMax = {nextData[1] + self.panelWidth/2,nextData[2]}
		local nowPos = {self.rabbitNode.transform.localPosition.x,self.rabbitNode.transform.localPosition.y}
		local perMin = self:CalJumpSpeed(nowPos,nextMin)
		local perMid = self:CalJumpSpeed(nowPos,nextMid)
		local perMax = self:CalJumpSpeed(nowPos,nextMax)
		if perMid < perMin then
			perMin = perMid
		end

		--self.m_BtnPressMaxHeight = 194

		self.greenBar.gameObject:SetActive(true)
		self.greenBar.transform.localPosition = Vector3(0,(perMin+perMax)/2*self.m_BtnPressMaxHeight,0)
		self.greenBar.height = self.m_BtnPressMaxHeight * (perMax - perMin)
	end
end

function LuaRabbitJumpWnd:Update()
	if self.m_StartJump then
		self.m_JumpTotalTime = self.m_JumpTotalTime + Time.deltaTime
		local moveX = Time.deltaTime * self.m_JumpSpeedX
		local speedY1 = self.m_JumpSpeedY
		local speedY2 = self.m_JumpSpeedY - Time.deltaTime * self.m_JumpG
		self.m_JumpSpeedY = speedY2
		local moveY = (speedY1 + speedY2) / 2 * Time.deltaTime

		local pos1 = self.rabbitNode.transform.localPosition
		self.rabbitNode.transform.localPosition = Vector3(
		self.rabbitNode.transform.localPosition.x + moveX,
		self.rabbitNode.transform.localPosition.y + moveY,
		self.rabbitNode.transform.localPosition.z)

		local checkState,stayPos = self:CheckJumpState(pos1,self.rabbitNode.transform.localPosition)
		if checkState then
			self.rabbitNode.transform.localPosition = Vector3(stayPos[1],stayPos[2],0)
			self:CalNextJump()
		end
		return
	end


--	if not self.m_BtnPressStatus then
--		if self.m_RabbitReady then
--			self.m_RabbitReady = false
--			self:RabbitJump()
--		end
--	else
--		local moveDis = Time.deltaTime * self.m_BtnPressSpeed
--		if self.m_BtnPressNowDirection == 0 then
--			self.m_BtnPressNowHeight = self.m_BtnPressNowHeight + moveDis
--		else
--			self.m_BtnPressNowHeight = self.m_BtnPressNowHeight - moveDis
--		end
--
--		if self.m_BtnPressNowHeight >= self.m_BtnPressMaxHeight then
--			self.m_BtnPressNowHeight = self.m_BtnPressMaxHeight
--			if self.m_BtnPressNowDirection == 0 then
--				self.m_BtnPressNowDirection = 1
--			end
--		elseif self.m_BtnPressNowHeight <= 0 then
--			self.m_BtnPressNowHeight = 0
--			if self.m_BtnPressNowDirection == 1 then
--				self.m_BtnPressNowDirection = 0
--			end
--		end
--
--		local percent = self.m_BtnPressNowHeight / self.m_BtnPressMaxHeight
--		self.slider.fillAmount = percent
--	end
end

function LuaRabbitJumpWnd:RabbitJump()
	if self.m_JumpEndTick then
		UnRegisterTick(self.m_JumpEndTick)
		self.m_JumpEndTick = nil
	end
	if self.m_RenderObject then
		self.m_RenderObject:DoAni('jump01_start',false,0,1,0.15,true,1)

		if self.m_Tick then
			UnRegisterTick(self.m_Tick)
			self.m_Tick = nil
		end

		local walkTime = 400
		self.m_Tick = RegisterTickWithDuration(function ()
			self.m_RenderObject:DoAni('jump01_loop',true,0,1,0.15,true,1)

		end,walkTime,walkTime)

		local speedX = self.m_JumpSpeedX
		local speedY = self.setJumpSpeedY
		self.setJumpSpeedX = speedX
		local minSpeedY = 300
		self.m_StartJump = true
		self.jumpBtn:SetActive(false)
		--self.m_JumpSpeedY = speedY * self.slider.fillAmount
		self.m_JumpSpeedY = speedY * self.jumpSilder.value
		self.m_JumpTotalTime = 0
		if self.m_JumpSpeedY < minSpeedY then
			self.m_JumpSpeedY = minSpeedY
		end
	end
end

function LuaRabbitJumpWnd:LoadModel(ro, prefabname)
	ro:LoadMain(prefabname, nil, false, false)
	self.m_RenderObject = ro
end

function LuaRabbitJumpWnd:InitRabbit()
	local prefabname = "Assets/Res/Character/NPC/znpc024/Prefab/znpc024_01.prefab"
	self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
		self:LoadModel(ro, prefabname)
	end)
	self.m_FakeModel.mainTexture = CUIManager.CreateModelTexture('JumpRabbit', self.m_ModelTextureLoader,90,0,-1.71,6.66,false,true,1,true)

	self:ResetRabbit()
end

function LuaRabbitJumpWnd:ResetRabbit()
	self.m_RenderObject:DoAni('stand01',true,0,1,0.15,true,1)
	self.m_StartJump = false
	self.rabbitNode.transform.parent = self.jumpNode.transform
	self.rabbitNode.transform.localPosition = Vector3(0,0,0)
	self.m_JumpStayPanelIndex = 1
	self:CalNextJump()
end

function LuaRabbitJumpWnd:OnDestroy()
	CUIManager.DestroyModelTexture('JumpRabbit')
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
	if self.m_FailTick then
		UnRegisterTick(self.m_FailTick)
		self.m_FailTick = nil
	end
end

return LuaRabbitJumpWnd
