local CScene=import "L10.Game.CScene"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CChatLinkMgr=import "CChatLinkMgr"
local UICamera = import "UICamera"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"

LuaFruitPartyView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFruitPartyView, "TaskName", "TaskName", UILabel)
RegistChildComponent(LuaFruitPartyView, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaFruitPartyView, "LeaveBtn", "LeaveBtn", GameObject)
RegistChildComponent(LuaFruitPartyView, "TaskDesc", "TaskDesc", UILabel)
RegistChildComponent(LuaFruitPartyView, "RuleBtn", "RuleBtn", GameObject)

--@endregion RegistChildComponent end

function LuaFruitPartyView:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.LeaveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveBtnClick(go)
	end)

    UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick(go)
	end)

    --@endregion EventBind end

    CommonDefs.AddOnClickListener(self.TaskDesc.gameObject, DelegateFactory.Action_GameObject(function(go) 
        self:OnLabelClick(self.TaskDesc) 
    end), false)
end

function LuaFruitPartyView:OnLabelClick(label)
	local url = label:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url ~= nil then
        CChatLinkMgr.ProcessLinkClick(url, nil)
    end
end

function LuaFruitPartyView:OnEnable()
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
end

function LuaFruitPartyView:OnDisable( )
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
end

function LuaFruitPartyView:Init()
    self.TaskName.text = LocalString.GetString("狂欢的水果")
end

function LuaFruitPartyView:OnSceneRemainTimeUpdate(args)
    if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            self.CountDownLabel.text = self:GetRemainTimeText(CScene.MainScene.ShowTime)
        else
            self.CountDownLabel.text = nil
        end
    else
        self.CountDownLabel.text = nil
    end

    if CSkillButtonBoardWnd.Instance and CSkillButtonBoardWnd.Instance.m_AdditionalSkillIds[0] ~= 0 then
        self.TaskDesc.text = g_MessageMgr:FormatMessage("FRUITPARTY_TASKDESC_PHASE1")
    else
        self.TaskDesc.text = g_MessageMgr:FormatMessage("FRUITPARTY_TASKDESC_PHASE2")
    end
end

function LuaFruitPartyView:GetRemainTimeText(totalSeconds)
    if totalSeconds <= 1 then
        return ""
    end
    local time
    if totalSeconds >= 3600 then
        time = SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        time = SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
    return SafeStringFormat3(LocalString.GetString("剩余时间：%s"), time)
end

--@region UIEvent

function LuaFruitPartyView:OnLeaveBtnClick(go)
    CGamePlayMgr.Inst:LeavePlay()
end

function LuaFruitPartyView:OnRuleBtnClick(go)
    g_MessageMgr:ShowMessage("FRUITPARTY_TIP")
end


--@endregion UIEvent

