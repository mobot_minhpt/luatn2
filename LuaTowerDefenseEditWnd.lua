local NGUITools=import "NGUITools"
local LuaGameObject=import "LuaGameObject"
local TowerDefense_Tower=import "L10.Game.TowerDefense_Tower"
local CTowerDefenseMgr=import "L10.Game.CTowerDefenseMgr"
local Item_Item=import "L10.Game.Item_Item"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local DefaultItemActionDataSource=import "L10.UI.DefaultItemActionDataSource"
--local CommonDefs = import "L10.Game.CommonDefs"
local Zhuangshiwu_TypeIcon = import "L10.Game.Zhuangshiwu_TypeIcon"
local Zhuangshiwu_Zhuangshiwu = import "L10.Game.Zhuangshiwu_Zhuangshiwu"
local EnumFurnitureFeatureStatus = import "L10.Game.EnumFurnitureFeatureStatus"

CLuaTowerDefenseEditWnd=class()
RegistClassMember(CLuaTowerDefenseEditWnd,"m_DataTable")
RegistClassMember(CLuaTowerDefenseEditWnd,"m_TypePrefab")
RegistClassMember(CLuaTowerDefenseEditWnd,"m_ItemPrefab")
RegistClassMember(CLuaTowerDefenseEditWnd,"m_TypeGoList")--缓存所有的类型item
RegistClassMember(CLuaTowerDefenseEditWnd,"m_ItemGoList")--缓存所有的类型item
RegistClassMember(CLuaTowerDefenseEditWnd,"m_ItemDataList")
RegistClassMember(CLuaTowerDefenseEditWnd,"m_IsExpanded")--是否展开

RegistClassMember(CLuaTowerDefenseEditWnd,"m_ItemsView")
RegistClassMember(CLuaTowerDefenseEditWnd,"m_ExpandButton")

RegistClassMember(CLuaTowerDefenseEditWnd,"m_GoldLabel")
RegistClassMember(CLuaTowerDefenseEditWnd,"m_CurrentKey")
RegistClassMember(CLuaTowerDefenseEditWnd,"m_FurnitureProcess")

function CLuaTowerDefenseEditWnd:Init()
    self.m_IsExpanded=true
    self.m_ItemsView=LuaGameObject.GetChildNoGC(self.transform,"ItemView").gameObject
    self.m_GoldLabel=LuaGameObject.GetChildNoGC(self.transform,"GoldLabel").label
    self.m_GoldLabel.text=tostring(CTowerDefenseMgr.Inst.Gold)
	self.m_GoldLabel.gameObject:SetActive(CTowerDefenseMgr.Inst.type == 1)
	
	self.m_FurnitureProcess = LuaGameObject.GetChildNoGC(self.transform, "FurnitureProcess").label
	self.m_FurnitureProcess.gameObject:SetActive(CTowerDefenseMgr.Inst.type == 2)
	if CTowerDefenseMgr.Inst.type == 2 then
		self.m_FurnitureProcess.text = SafeStringFormat3(LocalString.GetString("当前进度 %d/%d"), CTowerDefenseMgr.Inst.placedFurnitureAmount, CTowerDefenseMgr.Inst.totalFurnitureAmount)
	end
    local g = LuaGameObject.GetChildNoGC(self.transform,"LeaveButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CTowerDefenseMgr.Inst:LeaveSummonTowerMode()
    end)

    --点击展开按钮
    self.m_ExpandButton=LuaGameObject.GetChildNoGC(self.transform,"ExpandButton").gameObject
    UIEventListener.Get(self.m_ExpandButton).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_IsExpanded then
            self.m_IsExpanded=false
            self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
            self.m_ItemsView:SetActive(false)
        else
            self.m_IsExpanded=true
            self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
            self.m_ItemsView:SetActive(true)
        end
    end)
    
    self.m_TypePrefab=LuaGameObject.GetChildNoGC(self.transform,"TypeTemplate").gameObject
    self.m_TypePrefab:SetActive(false)
    self.m_ItemPrefab=LuaGameObject.GetChildNoGC(self.transform,"ItemTemplate").gameObject
    self.m_ItemPrefab:SetActive(false)

    self.m_TypeGoList={}
    self.m_ItemGoList={}
    self.m_DataTable={}

	local IterateDataTable = function (key, value)
		for i = 0, value.Count-1 do
			if not self.m_DataTable[key] then
				self.m_DataTable[key] = {}
			end
			table.insert(self.m_DataTable[key], value[i])
		end
	end
	CommonDefs.DictIterate(CTowerDefenseMgr.Inst.dataTable, DelegateFactory.Action_object_object(IterateDataTable))

    local keyTable={}
    for key, value in pairs(self.m_DataTable) do  
        table.insert(keyTable,key)
    end
    table.sort(keyTable)--1,2,3

    --点击类型按钮
    local function OnTypeClicked(go)
		-- print("onclick",go.name)
        for key, value in pairs(self.m_TypeGoList) do  
            if value==go then
                --高亮
                LuaGameObject.GetChildNoGC(value.transform,"Type").gameObject:SetActive(true)
				self:InitItems(key)
				self.m_CurrentKey = key
            else
                --取消高亮
                LuaGameObject.GetChildNoGC(value.transform,"Type").gameObject:SetActive(false)
            end
        end
	end

    local grid=LuaGameObject.GetChildNoGC(self.transform,"TypeGrid")
    local gridGo = grid.gameObject
    local uigrid = grid.grid
    CUICommonDef.ClearTransform(grid.transform)
    for i=1,#keyTable do
        local go=NGUITools.AddChild(gridGo,self.m_TypePrefab)
        go:SetActive(true)
		local designData
		if CTowerDefenseMgr.Inst.type == 1 then
			designData=TowerDefense_TypeIcon.GetData(i)
		elseif CTowerDefenseMgr.Inst.type == 2 then
			designData = Zhuangshiwu_TypeIcon.GetData(keyTable[i])
		end
        LuaGameObject.GetChildNoGC(go.transform,"NameLabel").label.text=designData.Description
        LuaGameObject.GetChildNoGC(go.transform,"Texture").cTexture:LoadMaterial(designData.Icon,false)
        table.insert(self.m_TypeGoList, go)
        CommonDefs.AddOnClickListener(go,DelegateFactory.Action_GameObject(OnTypeClicked),false)
    end
    --刷新table
    uigrid:Reposition()
    --默认点击第一个
    if table.getn(self.m_TypeGoList)>0 then
        OnTypeClicked(self.m_TypeGoList[1])
    end
end

--实例化smalltype
function CLuaTowerDefenseEditWnd:InitItems(key)
    --点击物品
    local function OnClickItem(go)
        CTowerDefenseMgr.Inst:CleanEditTowers()
		local buttonLabel = LocalString.GetString("布置守卫")
		if CTowerDefenseMgr.Inst.type == 2 then 
			buttonLabel = LocalString.GetString("放置家具")
		end
        for key, value in pairs(self.m_ItemGoList) do  
            if value==go then
                local templateId=self.m_ItemDataList[key].ItemId
                local t_name={}
                t_name[1]=LocalString.GetString(buttonLabel)
                local t_action={}
                t_action[1]=function()
                    CItemInfoMgr.CloseItemInfoWnd()
					if CTowerDefenseMgr.Inst.type == 1 then
						CTowerDefenseMgr.Inst:CreateTower(self.m_ItemDataList[key].ID)
					elseif CTowerDefenseMgr.Inst.type == 2 then
						if CTowerDefenseMgr.Inst:CreateFurniture(self.m_ItemDataList[key].ID, 0) and CTowerDefenseMgr.Inst.placeFurnitureMode == EnumFurnitureFeatureStatus.FurnitureFixedOperation then
							CTowerDefenseMgr.Inst:OnFurniturePlacing()
						end
					end
                end
                local actionSource=DefaultItemActionDataSource.Create(1,t_action,t_name)
                CItemInfoMgr.ShowLinkItemTemplateInfo(templateId,false,actionSource,AlignType.ScreenRight,0,0,0,0)
                LuaGameObject.GetChildNoGC(value.transform,"SelectedSprite").gameObject:SetActive(true)
            else
                LuaGameObject.GetChildNoGC(value.transform,"SelectedSprite").gameObject:SetActive(false)
            end
        end
    end
	local keyTable={}
    for key, value in pairs(self.m_DataTable) do  
        table.insert(keyTable,key)
    end
    table.sort(keyTable)
    local t=self.m_DataTable[keyTable[key]]
    self.m_ItemGoList={}
    self.m_ItemDataList={}
    local parent=LuaGameObject.GetChildNoGC(self.transform,"ItemGrid")
	local uigrid = parent.grid
    local parentGo = parent.gameObject
    --先清空
    CUICommonDef.ClearTransform(parent.transform)
    if t then
        for i=1,#t do
			local flag = true
			local tower
            if CTowerDefenseMgr.Inst.type == 1 then
				tower = TowerDefense_Tower.GetData(t[i])
				if tower.Level ~= 1 then
					flag = false
				end
			elseif CTowerDefenseMgr.Inst.type == 2 then
				tower = Zhuangshiwu_Zhuangshiwu.GetData(t[i])
			end
			if flag then
				local enableClick = true
				local go=NGUITools.AddChild(parentGo,self.m_ItemPrefab)
				LuaGameObject.GetChildNoGC(go.transform, "MaskSprite").gameObject:SetActive(false)
				LuaGameObject.GetChildNoGC(go.transform,"SelectedSprite").gameObject:SetActive(false)
				go:SetActive(true)
				go.name=go.name..tostring(i)
				local itemData=Item_Item.GetData(tower.ItemId)
				if itemData~=nil then
					LuaGameObject.GetChildNoGC(go.transform,"Texture").cTexture:LoadMaterial(itemData.Icon)
				end
				if CTowerDefenseMgr.Inst.type == 1 then
					LuaGameObject.GetChildNoGC(go.transform,"CostLabel").label.text=tostring(tower.BuildCose)
					LuaGameObject.GetChildNoGC(go.transform, "CoinSprite").gameObject:SetActive(true)
					LuaGameObject.GetChildNoGC(go.transform, "NumLabel").label.text = " "
					self:RefreshItemShow(tower.BuildCose,go)
				elseif CTowerDefenseMgr.Inst.type == 2 then
					local coinSprite = LuaGameObject.GetChildNoGC(go.transform, "CoinSprite")
					if coinSprite ~= nil then
						coinSprite.gameObject:SetActive(false)
					end
					LuaGameObject.GetChildNoGC(go.transform, "NumLabel").label.text = tostring(CTowerDefenseMgr.Inst.furnitureData[t[i]].num)
					if CTowerDefenseMgr.Inst.furnitureData[t[i]].num == 0 then
						enableClick = false
						LuaGameObject.GetChildNoGC(go.transform, "MaskSprite").gameObject:SetActive(true)
					end
				end

				table.insert(self.m_ItemGoList, go)
				table.insert(self.m_ItemDataList, tower)
				if enableClick then
					CommonDefs.AddOnClickListener(go,DelegateFactory.Action_GameObject(OnClickItem),false)
				end
			end
        end
    end
    uigrid:Reposition()
end

function CLuaTowerDefenseEditWnd:SyncFurniture()
	self:InitItems(self.m_CurrentKey)
end

function CLuaTowerDefenseEditWnd:OnEnable()
    g_ScriptEvent:AddListener("TowerDefenseSyncMyPlayPlayerInfo", self, "OnTowerDefenseSyncMyPlayPlayerInfo")
	g_ScriptEvent:AddListener("TowerDefenseSyncFurnitureInfo", self, "SyncFurniture")
	g_ScriptEvent:AddListener("TowerDefenseSyncFurniturePlacePos", self, "OnTowerDefenseSyncFurniturePlacePos")
end

function CLuaTowerDefenseEditWnd:OnDisable()
    g_ScriptEvent:RemoveListener("TowerDefenseSyncMyPlayPlayerInfo", self, "OnTowerDefenseSyncMyPlayPlayerInfo")
	g_ScriptEvent:RemoveListener("TowerDefenseSyncFurnitureInfo", self, "SyncFurniture")
	g_ScriptEvent:RemoveListener("TowerDefenseSyncFurniturePlacePos", self, "OnTowerDefenseSyncFurniturePlacePos")
end
function CLuaTowerDefenseEditWnd:OnTowerDefenseSyncMyPlayPlayerInfo()
	if CTowerDefenseMgr.Inst.type == 1 then
		self.m_GoldLabel.text=tostring(CTowerDefenseMgr.Inst.Gold)
		--刷新mask显示
		for key, value in pairs(self.m_ItemGoList) do  
			local cost=self.m_ItemDataList[key].BuildCose
			self:RefreshItemShow(cost,value)
		end
	end
end

function CLuaTowerDefenseEditWnd:OnTowerDefenseSyncFurniturePlacePos()
	if CTowerDefenseMgr.Inst.type == 2 then
		self.m_FurnitureProcess.text = SafeStringFormat3(LocalString.GetString("当前进度 %d/%d"), CTowerDefenseMgr.Inst.placedFurnitureAmount, CTowerDefenseMgr.Inst.totalFurnitureAmount)
	end
end
function CLuaTowerDefenseEditWnd:RefreshItemShow(cost,go)
    if cost>CTowerDefenseMgr.Inst.Gold then
        LuaGameObject.GetChildNoGC(go.transform,"MaskSprite").gameObject:SetActive(true)
        LuaGameObject.GetChildNoGC(go.transform,"CostLabel").label.text=SafeStringFormat3("[c][ff0000]%d[-][/c]",cost)
        
    else
        LuaGameObject.GetChildNoGC(go.transform,"MaskSprite").gameObject:SetActive(false)
        LuaGameObject.GetChildNoGC(go.transform,"CostLabel").label.text=tostring(cost)
    end
end


return CLuaTowerDefenseEditWnd