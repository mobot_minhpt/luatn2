-- Auto Generated!!
local Boolean = import "System.Boolean"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLeavingStatusSettingWnd = import "L10.UI.CLeavingStatusSettingWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CStatusMgr = import "L10.Game.CStatusMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local EPropStatus = import "L10.Game.EPropStatus"
local Gac2Gas = import "L10.Game.Gac2Gas"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CLeavingStatusSettingWnd.m_Init_CS2LuaHook = function (this) 
    this.onlineCheckbox:SetSelected(false, true)
    this.leavingCheckbox:SetSelected(false, true)
    this.autoReplayCheckbox:SetSelected(false, true)
    this.input.characterLimit = Constants.MaxAutoReplayMessageLength * 2
    this.input.defaultText = g_MessageMgr:FormatMessage("LeaveDefault")
    this.input.value = ""
    if CClientMainPlayer.Inst ~= nil then
        local manualLeaving = CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("Leaving")) == 1
        this.onlineCheckbox:SetSelected(not manualLeaving, true)
        this.leavingCheckbox:SetSelected(manualLeaving, true)
        this.autoReplayCheckbox:SetSelected(CClientMainPlayer.Inst.RelationshipProp.AutoReplySetting == 1, true)
        this.input.value = CClientMainPlayer.Inst.RelationshipProp.AutoReplyMsg.StringData
    end
end
CLeavingStatusSettingWnd.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.okBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.okBtn).onClick, MakeDelegateFromCSFunction(this.OnOKButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.cancelBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.cancelBtn).onClick, MakeDelegateFromCSFunction(this.OnCancelButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    this.onlineCheckbox.OnValueChanged = CommonDefs.CombineListner_Action_bool(this.onlineCheckbox.OnValueChanged, MakeDelegateFromCSFunction(this.OnOnlineCheckboxValueChanged, MakeGenericClass(Action1, Boolean), this), true)
    this.leavingCheckbox.OnValueChanged = CommonDefs.CombineListner_Action_bool(this.leavingCheckbox.OnValueChanged, MakeDelegateFromCSFunction(this.OnLeavingCheckboxValueChanged, MakeGenericClass(Action1, Boolean), this), true)
end
CLeavingStatusSettingWnd.m_OnInputValueChange_CS2LuaHook = function (this) 
    --限制输入字数
    if this.input.characterLimit < 0 then
        return
    end
    local str = this.input.value
    if CUICommonDef.GetStrByteLength(str) > this.input.characterLimit then
        repeat
            str = CommonDefs.StringSubstring2(str, 0, CommonDefs.StringLength(str) - 1)
        until not (CUICommonDef.GetStrByteLength(str) > this.input.characterLimit)
        this.input.value = str
    end
end
CLeavingStatusSettingWnd.m_OnOKButtonClick_CS2LuaHook = function (this, go) 
    Gac2Gas.SetLeaving(this.leavingCheckbox.Selected)
    Gac2Gas.SetAutoReply(this.autoReplayCheckbox.Selected)
    local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(this.input.value, true)
    local msg = ""
    if ret.shouldBeIgnore then
        g_MessageMgr:ShowMessage("Speech_Violation")
        return
    elseif ret.msg ~= nil then
        msg = StringTrim(ret.msg)
    end
    Gac2Gas.SetAutoReplyMsg(msg)
    this:Close()
end
