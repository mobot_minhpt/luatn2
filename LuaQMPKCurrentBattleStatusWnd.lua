local ShareMgr=import "ShareMgr"
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local AlignType=import "L10.UI.CPopupMenuInfoMgr+AlignType"
local UIGrid = import "UIGrid"
local System = import "System"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local Main = import "L10.Engine.Main"
local Json = import "L10.Game.Utils.Json"
local HTTPHelper = import "L10.Game.HTTPHelper"
local ETickType = import "L10.Engine.ETickType"
local EShareType = import "L10.UI.EShareType"
local CTickMgr = import "L10.Engine.CTickMgr"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local Constants = import "L10.Game.Constants"
local Color = import "UnityEngine.Color"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CFileTools = import "CFileTools"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

local Profession = import "L10.Game.Profession"
local Extensions = import "Extensions"

CLuaQMPKCurrentBattleStatusWnd = class()
RegistClassMember(CLuaQMPKCurrentBattleStatusWnd,"CloseButton")
RegistClassMember(CLuaQMPKCurrentBattleStatusWnd,"NameLabels")
RegistClassMember(CLuaQMPKCurrentBattleStatusWnd,"ScoreLabels")
RegistClassMember(CLuaQMPKCurrentBattleStatusWnd,"WinLoseMarkSprites")
RegistClassMember(CLuaQMPKCurrentBattleStatusWnd,"m_MyTeamSprites")
RegistClassMember(CLuaQMPKCurrentBattleStatusWnd,"m_Grid")
RegistClassMember(CLuaQMPKCurrentBattleStatusWnd,"m_BattleItem")
RegistClassMember(CLuaQMPKCurrentBattleStatusWnd,"m_ShareBtn")
RegistClassMember(CLuaQMPKCurrentBattleStatusWnd,"m_IsReplyFightData")
RegistClassMember(CLuaQMPKCurrentBattleStatusWnd,"m_StatusList")
RegistClassMember(CLuaQMPKCurrentBattleStatusWnd,"m_PicCoroutine")
RegistClassMember(CLuaQMPKCurrentBattleStatusWnd,"m_UrlCoroutine")

--分享参数
RegistClassMember(CLuaQMPKCurrentBattleStatusWnd,"m_TeamName")
RegistClassMember(CLuaQMPKCurrentBattleStatusWnd,"m_PlayStage")

RegistClassMember(CLuaQMPKCurrentBattleStatusWnd,"m_SingleDetailItem")
RegistClassMember(CLuaQMPKCurrentBattleStatusWnd,"m_Round2MemberView")
RegistClassMember(CLuaQMPKCurrentBattleStatusWnd,"m_CurSelect")

function CLuaQMPKCurrentBattleStatusWnd:Awake()
    self.CloseButton = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
self.NameLabels = {}
self.NameLabels[0]=FindChild(self.transform,"NameLabel1"):GetComponent(typeof(UILabel))
self.NameLabels[1]=FindChild(self.transform,"NameLabel2"):GetComponent(typeof(UILabel))
self.ScoreLabels = {}
self.ScoreLabels[0]=FindChild(self.transform,"ScoreLabel1"):GetComponent(typeof(UILabel))
self.ScoreLabels[1]=FindChild(self.transform,"ScoreLabel2"):GetComponent(typeof(UILabel))
self.WinLoseMarkSprites = {}
self.WinLoseMarkSprites[0]=FindChild(self.transform,"ResSprite1"):GetComponent(typeof(UISprite))
self.WinLoseMarkSprites[1]=FindChild(self.transform,"ResSprite2"):GetComponent(typeof(UISprite))
self.WinLoseMarkSprites[0].spriteName = nil
self.WinLoseMarkSprites[1].spriteName = nil
-- self.m_MyTeamSprites = {}
-- self.m_MyTeamSprites[0]=FindChild(self.transform,"TeamSprite1"):GetComponent(typeof(UISprite))
-- self.m_MyTeamSprites[1]=FindChild(self.transform,"TeamSprite2"):GetComponent(typeof(UISprite))
    self.m_Grid = self.transform:Find("ShowArea/ContentArea/AdvView/Scroll View/Grid").gameObject
    self.m_BattleItem = self.transform:Find("ShowArea/ContentArea/AdvView/BattleItem").gameObject
    self.m_SingleDetailItem = self.transform:Find("ShowArea/ContentArea/AdvView/SingleDetailItem").gameObject
    self.m_ShareBtn = self.transform:Find("ShowArea/ShareBtn").gameObject
self.m_IsReplyFightData = false
self.m_StatusList = nil
self.m_PicCoroutine = nil
self.m_UrlCoroutine = nil
self.m_CurSelect = 0
self.m_Round2MemberView = {}
self.m_SingleDetailItem.gameObject:SetActive(false)
local dataButton=FindChild(self.transform,"DataButton").gameObject
--如果观战的话 就不显示这个按钮
    dataButton:SetActive(true)
    UIEventListener.Get(dataButton).onClick=DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.QMPKBattleDataWnd)
    end)
end

-- Auto Generated!!



function CLuaQMPKCurrentBattleStatusWnd:OnEnable( )
	g_ScriptEvent:AddListener("QMPKQueryBattleStatusResult", self, "OnReplyStatus")
    g_ScriptEvent:AddListener("QMPKReplyFightDataResult", self, "OnReplyFightData")
    g_ScriptEvent:AddListener("QmpkBiWuPlayEnd", self, "OnQmpkBiWuPlayEnd")

end
function CLuaQMPKCurrentBattleStatusWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("QMPKQueryBattleStatusResult", self, "OnReplyStatus")
	g_ScriptEvent:RemoveListener("QMPKReplyFightDataResult", self, "OnReplyFightData")
    g_ScriptEvent:RemoveListener("QmpkBiWuPlayEnd", self, "OnQmpkBiWuPlayEnd")
end
function CLuaQMPKCurrentBattleStatusWnd:OnQmpkBiWuPlayEnd()
    --再请求一次，看看是否可以分享
    Gac2Gas.QueryQmpkAllMemberFightData()
end

function CLuaQMPKCurrentBattleStatusWnd:OnReplyFightData( playStage,playIndex )
    self.m_PlayStage=playStage
    self.m_IsReplyFightData = true
    if nil == CClientMainPlayer.Inst then
        return
    end

    local found=false
    local myId=CClientMainPlayer.Inst.Id
    for i=1,#CLuaQMPKMgr.m_ZhanduiFightData do
        local data=CLuaQMPKMgr.m_ZhanduiFightData[i]
        if data.m_PlayerId==myId then
            found=true
            break
        end
    end
    if found then
        self.m_ShareBtn:SetActive(true)
    end
end
function CLuaQMPKCurrentBattleStatusWnd:OnReplyStatus( )
    local battleData=CLuaQMPKMgr.m_CurrentBattleStatus
    self.m_StatusList = battleData.m_Status

    Extensions.RemoveAllChildren(self.m_Grid.transform)
    do
        local i = 0
        while i < #self.m_StatusList do
            local go = NGUITools.AddChild(self.m_Grid, self.m_BattleItem)
            local round = i + 1
            go:SetActive(true)
            self.m_Round2MemberView[i + 1] = {}
            self.m_Round2MemberView[i + 1].go = go
            self:InitItem(go.transform,self.m_StatusList[i+1], i)
            UIEventListener.Get(go.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
                self:OnBattleItemClick(round)
            end)
            i = i + 1
        end
    end

    self.m_Grid:GetComponent(typeof(UITable)):Reposition()


    self.NameLabels[0].text = battleData.m_AttackName
    self.NameLabels[1].text = battleData.m_DefendName
    self.ScoreLabels[0].text = tostring(battleData.m_AttackWinCount)
    self.ScoreLabels[1].text = tostring(battleData.m_DefendWinCount)

    if battleData.m_AttackWinCount >= 3 or battleData.m_DefendWinCount >= 3 then
        if battleData.m_AttackWinCount > battleData.m_DefendWinCount then
            self.WinLoseMarkSprites[0].spriteName = Constants.WinSprite
            self.WinLoseMarkSprites[1].spriteName = Constants.LoseSprite
        else
            self.WinLoseMarkSprites[0].spriteName = Constants.LoseSprite
            self.WinLoseMarkSprites[1].spriteName = Constants.WinSprite
        end
    else
        self.WinLoseMarkSprites[0].gameObject:SetActive(false)
        self.WinLoseMarkSprites[1].gameObject:SetActive(false)
    end
    -- self.m_MyTeamSprites[0].gameObject:SetActive(battleData.m_SelfForce == EnumCommonForce_lua.eAttack)
    -- self.m_MyTeamSprites[1].gameObject:SetActive(battleData.m_SelfForce == EnumCommonForce_lua.eDefend)
    self.NameLabels[0].color = battleData.m_SelfForce == EnumCommonForce_lua.eAttack and Color.green or Color.white
    self.NameLabels[1].color = battleData.m_SelfForce == EnumCommonForce_lua.eDefend and Color.green or Color.white

    if battleData.m_SelfForce == EnumCommonForce_lua.eAttack then
        self.m_TeamName=battleData.m_AttackName
    else
        self.m_TeamName=battleData.m_DefendName
    end
end
function CLuaQMPKCurrentBattleStatusWnd:Start( )
    UIEventListener.Get(self.m_ShareBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnShareBtnClicked(go) end)
    self.m_ShareBtn:SetActive(false)
end
function CLuaQMPKCurrentBattleStatusWnd:Init( )
    self.m_BattleItem:SetActive(false)

    if CLuaQMPKMgr.m_IsGameVideoBattleStatus then
        self:OnReplyStatus()
    else
        CLuaQMPKMgr.m_CurrentBattleStatus=nil
        Gac2Gas.QueryQmpkBiWuZhanKuang()
        Gac2Gas.QueryQmpkAllMemberFightData()
    end
end
function CLuaQMPKCurrentBattleStatusWnd:OnShareBtnClicked( go)
    local function SelectAction(index)
        if index==0 then
            self:Share(1)--梦岛
        else
            self:Share(2)--普通
        end
    end
    local selectShareItem=DelegateFactory.Action_int(SelectAction)

    local t={}
    local item=PopupMenuItemData(LocalString.GetString("至报名角色梦岛"),selectShareItem,false,nil)
    table.insert(t, item)
    local item=PopupMenuItemData(LocalString.GetString("至游戏外"),selectShareItem,false,nil)
    table.insert(t, item)

    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType.Top,1,nil,600,true,360)
end

function CLuaQMPKCurrentBattleStatusWnd:ShareTicketImage( imageUrl)
    if self.m_PicCoroutine ~= nil then
        Main.Inst:StopCoroutine(self.m_PicCoroutine)
        self.m_PicCoroutine = nil
    end

    self.m_PicCoroutine = Main.Inst:StartCoroutine(HTTPHelper.DownloadImage(imageUrl, DelegateFactory.Action_bool_byte_Array(function (picResult, picData)
        local filename = Utility.persistentDataPath .. "/QMPKFightShare.jpg"
        System.IO.File.WriteAllBytes(filename, picData)
        CFileTools.SetFileNoBackup(filename)

        CTickMgr.Register(DelegateFactory.Action(function ()
            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, {filename})
        end), 1000, ETickType.Once)
    end)))
end
function CLuaQMPKCurrentBattleStatusWnd:InitItem(transform, status, index)
    local m_Dps1 = transform:Find("DamageLabel1"):GetComponent(typeof(UILabel))
    local m_Dps2 = transform:Find("DamageLabel2"):GetComponent(typeof(UILabel))
    -- local m_Name1 = transform:Find("SingleNameRoot/NameLabel1"):GetComponent(typeof(UILabel))
    -- local m_Name2 = transform:Find("SingleNameRoot/NameLabel2"):GetComponent(typeof(UILabel))
    local m_WinObj1 = transform:Find("ResSprite1"):GetComponent(typeof(UISprite))
    local m_WinObj2 = transform:Find("ResSprite2"):GetComponent(typeof(UISprite))
    local m_SigleObj = transform:Find("Single").gameObject
    local m_ThreeObj = transform:Find("Three").gameObject
    local m_GroupObj = transform:Find("Group").gameObject
    local m_DoubleObj = transform:Find("Double").gameObject
    local m_Class1 = transform:Find("GridLeft"):GetComponent(typeof(UIGrid))
    local m_Class2 = transform:Find("GridRight"):GetComponent(typeof(UIGrid))

    local m_Kill1 = transform:Find("Kill1"):GetComponent(typeof(UILabel))
    local m_Kill2 = transform:Find("Kill2"):GetComponent(typeof(UILabel))
    local CurrentIcon = transform:Find("CurrentIcon").gameObject
    local HighlightSplit = transform:Find("HighlightSplit").gameObject
    local SelectHighLight = transform:Find("SelectHighLight"):GetComponent(typeof(UISprite))
    local classItem = transform:Find("ClassItem").gameObject
    classItem.gameObject:SetActive(false)
    m_Kill1.text = tostring(status.attackTotalKill)
    m_Kill2.text = tostring(status.defendTotalKill)
    self.m_Round2MemberView[index + 1].HighlightSplit = HighlightSplit
    self.m_Round2MemberView[index + 1].SelectHighLight = SelectHighLight
    SelectHighLight.gameObject:SetActive(false)
    HighlightSplit.gameObject:SetActive(false)
    -- local m_SingleNameRoot = transform:Find("SingleNameRoot").gameObject
    -- local m_GroupNameRoot = transform:Find("GroupNameRoot").gameObject
    -- local m_ThreeNameRoot = transform:Find("ThreeNameRoot").gameObject

    -- local m_GroupNameGrid={}
    -- m_GroupNameGrid[0]=FindChild(transform,"Grid1"):GetComponent(typeof(UIGrid))
    -- m_GroupNameGrid[1]=FindChild(transform,"Grid2"):GetComponent(typeof(UIGrid))
    -- local m_ThreeNameGrid={}
    -- m_ThreeNameGrid[0]=FindChild(transform,"Grid3"):GetComponent(typeof(UIGrid))
    -- m_ThreeNameGrid[1]=FindChild(transform,"Grid4"):GetComponent(typeof(UIGrid))


     m_Dps1.text = tostring(status.attackTotalDps)
     m_Dps2.text = tostring(status.defendTotalDps)
     --m_Name1.text = status.m_Name1;
     --m_Name2.text = status.m_Name2;
     m_WinObj1.gameObject:SetActive(true)
     m_WinObj2.gameObject:SetActive(true)

     if status.win == EnumCommonForce_lua.eDefend then
         m_WinObj1.spriteName = Constants.LoseSprite
         m_WinObj2.spriteName = Constants.WinSprite
     elseif status.win == EnumCommonForce_lua.eAttack then
         m_WinObj1.spriteName = Constants.WinSprite
         m_WinObj2.spriteName = Constants.LoseSprite
     else
         m_WinObj1.gameObject:SetActive(false)
         m_WinObj2.gameObject:SetActive(false)
     end
     CurrentIcon.gameObject:SetActive(status.m_IsInBattle)

     if index %2 == 0 then
        transform:GetComponent(typeof(UISprite)).spriteName="common_bg_mission_background_s"
     else
        transform:GetComponent(typeof(UISprite)).spriteName="common_bg_mission_background_n"
     end

     m_SigleObj:SetActive(index == 1)
     m_ThreeObj:SetActive(index == 2)
     m_GroupObj:SetActive(index == 0 or index == 4)
     m_DoubleObj:SetActive(index == 3)
    --  m_GroupNameRoot:SetActive(false)
    --  m_SingleNameRoot:SetActive(false)
    --  m_ThreeNameRoot:SetActive(false)
    -- if index == 0 or index == 3 then
    --m_GroupNameRoot:SetActive(true)
    Extensions.RemoveAllChildren(m_Class1.transform)
    do
        local i = 0 local cnt = #status.attackData
        while i < cnt do
            local go = NGUITools.AddChild(m_Class1.gameObject, classItem.gameObject)
            go.gameObject:SetActive(true)
            local sprite = go:GetComponent(typeof(UISprite))
            if sprite ~= nil then
                sprite.spriteName = Profession.GetIconByNumber(status.attackData[i+1].Class)
            end
            i = i + 1
        end
    end
    m_Class1:Reposition()
    Extensions.RemoveAllChildren(m_Class2.transform)
    do
        local i = 0 local cnt = #status.defendData
        while i < cnt do
            local go = NGUITools.AddChild(m_Class2.gameObject, classItem.gameObject)
            go.gameObject:SetActive(true)
            local sprite = go:GetComponent(typeof(UISprite))
            if sprite ~= nil then
                sprite.spriteName = Profession.GetIconByNumber(status.defendData[i+1].Class)
            end
            i = i + 1
        end
    end
    m_Class2:Reposition()
    local signalMemeber = {}
    for i = 1,#status.defendData do
        local go = NGUITools.AddChild(self.m_Grid, self.m_SingleDetailItem.gameObject)
        local defendMemember = status.defendData[i]
        local attackMemember = status.attackData[i]
        local Dps1 = go.transform:Find("DamageLabel1"):GetComponent(typeof(UILabel))
        local Dps2 = go.transform:Find("DamageLabel2"):GetComponent(typeof(UILabel))
        local Kill1 = go.transform:Find("Kill1"):GetComponent(typeof(UILabel))
        local Kill2 = go.transform:Find("Kill2"):GetComponent(typeof(UILabel))
        local NameLabel1 = go.transform:Find("NameLabel1"):GetComponent(typeof(UILabel))
        local NameLabel2 = go.transform:Find("NameLabel2"):GetComponent(typeof(UILabel))
        local Class1 = go.transform:Find("Class1"):GetComponent(typeof(UISprite))
        local Class2 = go.transform:Find("Class2"):GetComponent(typeof(UISprite))
        if defendMemember then
            Dps2.text = tostring(defendMemember.Dps)
            Kill2.text = tostring(defendMemember.Kill)
            NameLabel2.text = tostring(defendMemember.Name)
            Class2.spriteName = Profession.GetIconByNumber(defendMemember.Class)
        end
        if attackMemember then
            Dps1.text = tostring(attackMemember.Dps)
            Kill1.text = tostring(attackMemember.Kill)
            NameLabel1.text = tostring(attackMemember.Name)
            Class1.spriteName = Profession.GetIconByNumber(attackMemember.Class)
        end
        go.gameObject:SetActive(false)
        table.insert(signalMemeber,go)
    end
    self.m_Round2MemberView[index + 1].SignalMemeber = signalMemeber

    --  elseif index == 1 or index == 4 then
    --      m_SigleObj:SetActive(true)
    --      m_SingleNameRoot:SetActive(true)
    --      if #status.m_Class1 > 0 then
    --          m_Name1.text = status.m_Name1[1]
    --          m_Class1.spriteName = Profession.GetIconByNumber( status.m_Class1[1] )
    --      else
    --          m_Name1.text = ""
    --          m_Class1.spriteName = nil
    --      end

    --      if #status.m_Class2 > 0 then
    --          m_Name2.text = status.m_Name2[1]
    --          m_Class2.spriteName = Profession.GetIconByNumber( status.m_Class2[1] )
    --      else
    --          m_Name2.text = ""
    --          m_Class2.spriteName = nil
    --      end
    --  else
    --      m_ThreeObj:SetActive(true)
    --      m_ThreeNameRoot:SetActive(true)
    --      Extensions.RemoveAllChildren(m_ThreeNameGrid[0].transform)
    --      do
    --          local i = 0 local cnt = #status.m_Class1
    --          while i < cnt do
    --              local go = NGUITools.AddChild(m_ThreeNameGrid[0].gameObject, m_Class1.gameObject)
    --              local sprite = go:GetComponent(typeof(UISprite))
    --              if sprite ~= nil then
    --                  sprite.spriteName = Profession.GetIconByNumber(status.m_Class1[i+1])
    --              end
    --              i = i + 1
    --          end
    --      end
    --      Extensions.RemoveAllChildren(m_ThreeNameGrid[1].transform)
    --      do
    --          local i = 0 local cnt = #status.m_Class2
    --          while i < cnt do
    --              local go = NGUITools.AddChild(m_ThreeNameGrid[1].gameObject, m_Class1.gameObject)
    --              local sprite = go:GetComponent(typeof(UISprite))
    --              if sprite ~= nil then
    --                  sprite.spriteName = Profession.GetIconByNumber(status.m_Class2[i+1])
    --              end
    --              i = i + 1
    --          end
    --      end
    --  end
 end

function CLuaQMPKCurrentBattleStatusWnd:OnBattleItemClick(index)
    if not self.m_Round2MemberView then return end
    local battleData=CLuaQMPKMgr.m_CurrentBattleStatus
    if not battleData then return end
    if not self.m_Round2MemberView[index] or #(self.m_Round2MemberView[index].SignalMemeber) <= 0 then
        g_MessageMgr:ShowMessage("QMPK_ZhuanKuang_Not_Complate")
        return
    end

    if self.m_CurSelect == index and  self.m_Round2MemberView[self.m_CurSelect] then
        local curView = self.m_Round2MemberView[self.m_CurSelect]
        curView.HighlightSplit.gameObject:SetActive(false)
        curView.SelectHighLight.gameObject:SetActive(false)
        for k,v in pairs(curView.SignalMemeber) do v.gameObject:SetActive(false) end
        self.m_CurSelect = 0
        self.m_Grid:GetComponent(typeof(UITable)):Reposition()
        return
    end
    if self.m_Round2MemberView[self.m_CurSelect] then
        local curView = self.m_Round2MemberView[self.m_CurSelect]
        curView.HighlightSplit.gameObject:SetActive(false)
        curView.SelectHighLight.gameObject:SetActive(false)
        for k,v in pairs(curView.SignalMemeber) do v.gameObject:SetActive(false) end
    end

    if self.m_Round2MemberView[index] then
        local newView = self.m_Round2MemberView[index]
        newView.HighlightSplit.gameObject:SetActive(true)
        newView.SelectHighLight.gameObject:SetActive(false)
        for k,v in pairs(newView.SignalMemeber) do v.gameObject:SetActive(true) end
    end
    self.m_Grid:GetComponent(typeof(UITable)):Reposition()

    if self.m_Round2MemberView[index] then
        local SelectHighLight = self.m_Round2MemberView[index].SelectHighLight
        SelectHighLight.height = 110 + 62 * (#self.m_Round2MemberView[index].SignalMemeber) + 12
        SelectHighLight.gameObject:SetActive(true)
    end

    self.m_CurSelect = index
end

function CLuaQMPKCurrentBattleStatusWnd:Share(type)
    if self.m_ImgUrl then
		if type==1 then
            -- ShareMgr.ShareWebImage2PersonalSpace(self.m_ImgUrl,nil)
            local id=CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
            local origin_id=CQuanMinPKMgr.Inst.m_MyPersonalInfo and CQuanMinPKMgr.Inst.m_MyPersonalInfo.m_FromCharacterId or id
            CLuaShareMgr.ShareQMPKImage2PersonalSpace(id,origin_id,self.m_ImgUrl)
		else
			ShareMgr.ShareWebImage2Other(self.m_ImgUrl)
		end
		return
	end

	if self.m_UrlCoroutine ~= nil then
        Main.Inst:StopCoroutine(self.m_UrlCoroutine)
	end

    local teamdata="["
    for i,v in ipairs(CLuaQMPKMgr.m_ZhanduiFightData) do
        local item=SafeStringFormat3([[{"careerid":%s,"gender":%s,"rolename":%s}]],v.m_PlayerClass,v.m_PlayerGender,Json.Serialize(v.m_PlayerName))

        if i==1 then
            teamdata=teamdata..item
        else
            teamdata=teamdata..","..item
        end
    end
    teamdata=teamdata.."]"


	local url=CLuaQMPKMgr.m_ShareUrl.."shareImg/match?"
	local inst=CClientMainPlayer.Inst
	if inst then
		local serverName=CLoginMgr.Inst:GetSelectedGameServer().name

        local myInfo=nil
        local myId=inst.Id

        for i=1,#CLuaQMPKMgr.m_ZhanduiFightData do
            local data=CLuaQMPKMgr.m_ZhanduiFightData[i]
            if data.m_PlayerId==myId then
                myInfo=data
                break
            end
        end
        local matchdata=""

        if myInfo then
            local item1=SafeStringFormat3( [["%s":{"number":"%s","is_best":%s}]],"dps",myInfo.m_Dps, myInfo.m_BestDps )
            local item2=SafeStringFormat3( [["%s":{"number":"%s","is_best":%s}]],"kill",myInfo.m_KillNum, myInfo.m_BestKillNum )
            local item3=SafeStringFormat3( [["%s":{"number":"%s","is_best":%s}]],"die",myInfo.m_Die,myInfo.m_BestDie )
            local item4=SafeStringFormat3( [["%s":{"number":"%s","is_best":%s}]],"ctrl",myInfo.m_Ctrl, myInfo.m_BestCtrl)
            local item5=SafeStringFormat3( [["%s":{"number":"%s","is_best":%s}]],"relive",myInfo.m_Relive, myInfo.m_BestRelive)
            local item6=SafeStringFormat3( [["%s":{"number":"%s","is_best":%s}]],"heal",myInfo.m_Heal, myInfo.m_BestHeal )
            local item7=SafeStringFormat3( [["%s":{"number":"%s","is_best":%s}]],"dectrl",myInfo.m_RmCtrl,myInfo.m_BestRmCtrl )
            matchdata=SafeStringFormat3("{%s,%s,%s,%s,%s,%s,%s}",item1,item2,item3,item4,item5,item6,item7)
        end

        url = url..SafeStringFormat3("roleid=%s&rolename=%s&gender=%s&careerid=%s&stage=%s&teamname=%s&teamdata=%s&matchdata=%s",
        inst.Id,
        inst.Name,
        EnumToInt(inst.Gender),
        EnumToInt(inst.Class),
        CLuaQMPKMgr.GetPlayName(self.m_PlayStage),
        self.m_TeamName,
        teamdata,
        matchdata
    )
    end
	self.m_UrlCoroutine = Main.Inst:StartCoroutine(HTTPHelper.GetUrl(url, DelegateFactory.Action_bool_string(function (success, ret)
		if success then
			local dict = Json.Deserialize(ret)
			local code = CommonDefs.DictGetValue_LuaCall(dict,"code")
			if code and code==1 then
				--成功
                self.m_ImgUrl = CommonDefs.DictGetValue_LuaCall(dict,"imgurl")
				if self.m_ImgUrl then
                    if type==1 then
                        -- if CQuanMinPKMgr.Inst.m_MyPersonalInfo then
                        --     CLuaShareMgr.ShareWebImage2TargetPersonalSpace(CQuanMinPKMgr.Inst.m_MyPersonalInfo.m_FromCharacterId,self.m_ImgUrl)
                        -- end
                        local id=CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
                        local origin_id=CQuanMinPKMgr.Inst.m_MyPersonalInfo and CQuanMinPKMgr.Inst.m_MyPersonalInfo.m_FromCharacterId or id
                        CLuaShareMgr.ShareQMPKImage2PersonalSpace(id,origin_id,self.m_ImgUrl)
					else
						ShareMgr.ShareWebImage2Other(self.m_ImgUrl)
					end
				end
			end
		end
	end)))
end
return CLuaQMPKCurrentBattleStatusWnd
