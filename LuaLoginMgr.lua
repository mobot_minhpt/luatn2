local JSON = require '3rdParty/json/JSON'
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CPayMgr = import "L10.Game.CPayMgr"
local NativeUtility=import "NativeUtility"
local ChannelDefine=import "L10.Game.ChannelDefine"
local Extensions=import "Extensions"
local PlatformDefine=import "L10.Game.PlatformDefine"
local CSystemInfoMgr=import "L10.Engine.CSystemInfoMgr"
local SystemInfo=import "UnityEngine.SystemInfo"
local CClientVersion=import "L10.Engine.CClientVersion"
local SdkU3d = import "NtUniSdk.Unity3d.SdkU3d"
local ConstProp=import "NtUniSdk.Unity3d.ConstProp"
local EnumNetworkType=import "EnumNetworkType"
local Gac2Login=import "L10.Game.Gac2Login"
local CLoginMgr = import "L10.Game.CLoginMgr"
local EServerStatus = import "L10.UI.EServerStatus"
local CommonDefs = import "L10.Game.CommonDefs"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Main = import "L10.Engine.Main"
local NativeTools = import "L10.Engine.NativeTools"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CServerWnd = import "L10.UI.CServerWnd"
local CGameServer = import "L10.UI.CGameServer"
local PatchMgr = import "L10.Engine.PatchMgr"
local Screen = import "UnityEngine.Screen"
local DRPFMgr = import "L10.Game.DRPFMgr"
local CLogMgr = import "L10.CLogMgr"
local CGasConnMgr = import "L10.Game.CGasConnMgr"
local String = import "System.String"
local Object = import "System.Object"
local WWW = import "UnityEngine.WWW"
local Application = import "UnityEngine.Application"
local CDeepLinkMgr = import "L10.Game.CDeepLinkMgr"
local EasyCodeScanner = import "EasyCodeScanner"
local CRenderObject = import "L10.Engine.CRenderObject"
local CLoginQueueWnd = import "L10.UI.CLoginQueueWnd"
local EUIModuleGroup = import "L10.UI.CUIManager+EUIModuleGroup"
local CUIModule = import "L10.UI.CUIModule"
local Setting = import "L10.Engine.Setting"

function CLoginMgr.m_hookOnConnectFailed(this, connection)
    this:StopLoadingWnd()
	this:StopQrcodeProcess()
    this.LoginConn:Shutdown()
    if this.IsConnectingToLogin then
        local gs = this:GetSelectedGameServer()
        if gs and (gs.status == EServerStatus.Maintaining) and (gs.port2 ~= 0) and (not this.s_UsePort2) then
            this:DoConnectLoginServer(gs, false, true)
            return
        else
            this:_ShowNoConnectionMsg("m_hookOnConnectFailed")
        end
    end
    this.IsConnectingToLogin = false
end

function CLoginMgr.m_hookOnConnected(this, connection)
    if (not this.s_UsePort2) then
        CUIManager.CloseUI(CLuaUIResources.ServerMaintenanceWnd)
        return
    end
    this:InitIsFailedConnectingToLoginLastTime()
end

CLoginMgr.m_hookStartLoadingWnd = function(this,content, reason)
    if CUIManager.IsLoaded(CLuaUIResources.ServerMaintenanceWnd) then return end
    if CLoginMgr.Inst.s_UsePort2 then return end
    CUICommonDef.StartActivityIndicator(content)
end

EnumSendPcTicketEvent = {
	eMobileScanLogin = 1,
	eMobileScanPay = 2,
	ePcUniSdkPayConfirm = 3,
}

if rawget(_G, "LuaLoginMgr") then
    g_ScriptEvent:RemoveListener("OnSyncMainPlayerAppearanceProp", LuaLoginMgr, "OnSyncMainPlayerAppearanceProp")
    g_ScriptEvent:RemoveListener("PlayerLogin", LuaLoginMgr, "OnPlayerLogin")
end

LuaLoginMgr = class()
LuaLoginMgr.m_LastServerName = nil
LuaLoginMgr.m_ServerOpenTime = -1

g_ScriptEvent:AddListener("OnSyncMainPlayerAppearanceProp", LuaLoginMgr, "OnSyncMainPlayerAppearanceProp")
g_ScriptEvent:AddListener("PlayerLogin", LuaLoginMgr, "OnPlayerLogin")

function LuaLoginMgr.ModifyWindowTitleForPC(context)
    --仅PC平台
    if not CommonDefs.Is_UNITY_STANDALONE_WIN() then
        return
    end

    local mainplayer = CClientMainPlayer.Inst
    local title = nil
    local unityversion = CommonDefs.GetUnityVersionPrefix()
    if mainplayer then
        LuaLoginMgr.m_LastServerName = mainplayer:GetMyServerName()
        title = SafeStringFormat3(LocalString.GetString(" 引擎[%d.%d]版本[%d.%d]服务器[%s]角色[%s]"),unityversion, Main.Inst.EngineVersion,unityversion,Main.Inst.GameVersion,mainplayer:GetMyServerName(),tostring(math.floor(mainplayer.Id)))
    else
        title = SafeStringFormat3(LocalString.GetString(" 引擎[%d.%d]版本[%d.%d]"),unityversion, Main.Inst.EngineVersion,unityversion,Main.Inst.GameVersion)
    end

    NativeTools.ModifyWindowTitle(title)
end
--在跨服上的玩家初始上线时，ServerName同步有延迟，在下面同步Appearance时机进行一下刷新
function LuaLoginMgr.OnSyncMainPlayerAppearanceProp(args)
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer and mainplayer:GetMyServerName()~=LuaLoginMgr.m_LastServerName then
        LuaLoginMgr.ModifyWindowTitleForPC("syncappearance")
    end 
end
-- 登陆时清理一些数据
function LuaLoginMgr.OnPlayerLogin()
    LuaLoginMgr.m_ServerOpenTime = -1
end

--启动的时候立马执行一次以刷新Title显示，鸣谢sf的idea
--这里采用延迟一定时间主要是由于修改标题目前依赖了CWinUtility中的变量，而CWinUtility的StartUp晚于Lua的StartUp
RegisterTickOnce(function()
    LuaLoginMgr.ModifyWindowTitleForPC("startup")
end, 3000)


--启动的时候请求countryCode数据
LuaSEASdkMgr:QueryCountryCode()

LuaLoginMgr.m_MessageShowInServerMaintenanceWnd = ""
LuaLoginMgr.m_ServerId = 0
LuaLoginMgr.m_ServerMaintenanceWndState = 1 --1是普通更新，0是临时更新
function LuaLoginMgr:ShowServerMaintenanceWnd(msg, state)
    self.m_ServerMaintenanceWndState = state
    self.m_MessageShowInServerMaintenanceWnd = msg
    CLoginMgr.Inst.m_IsHideServerLoginBtn = true
    EventManager.BroadcastInternalForLua(EnumEventType.OnLoginConnectStatusChanged,{})
    CUIManager.ShowUI(CLuaUIResources.ServerMaintenanceWnd)
end

function UpdateClientMaintianMessage(msg)
    if cs_string.IsNullOrEmpty(msg) then return end
    if string.len(msg) < 3 then return end
    local state, str = tonumber(string.sub(msg, 1,1)),string.sub(msg, 3)
    if (state ~= 1) and (state ~= 0) then return end
    LuaLoginMgr:ShowServerMaintenanceWnd(str, state)
end

CServerWnd.m_hookOnSelectedGameServerChanged = function(this)
    local gs = CLoginMgr.Inst:GetSelectedGameServer()
    if not gs then return end
    this.currentServerLabel.text = gs and gs.name or LocalString.GetString("未选择服务器")
    this.serverStatusSprite.spriteName = gs and CGameServer.GetStatusSprite(gs.status) or nil

    --切换服务器时
    if LuaLoginMgr.m_ServerId ~= gs.id then
        --关闭停服维护界面
        CUIManager.CloseUI(CLuaUIResources.ServerMaintenanceWnd)

        --切换前服务器处于维护中时
        if CLoginMgr.Inst.s_UsePort2 then

            --关闭连接，刷新登录按钮状态
            LuaLoginMgr:OnMaintenanceServerDisconnected()
        end
    end

    --ServerMaintenanceWnd Tick更新gameServer时 发现服务器状态变更
    if gs.status ~= EServerStatus.Maintaining then

        --关闭停服维护界面
        CUIManager.CloseUI(CLuaUIResources.ServerMaintenanceWnd)
    end
    LuaLoginMgr.m_ServerId = gs.id

    --pc Release 自动连接服务器打开扫码
    this:RefreshQRCode()
end

function LuaLoginMgr:OnMaintenanceServerDisconnected()
    CLoginMgr.Inst:StopLoadingWnd()
    CLoginMgr.Inst:StopQrcodeProcess()
    CLoginMgr.Inst.LoginConn:Shutdown()
    CLoginMgr.Inst.m_IsHideServerLoginBtn = false
    LuaLoginMgr.m_ServerId = 0
    if CLoginMgr.Inst.IsConnectingToLogin then
        CLoginMgr.Inst.IsConnectingToLogin = false
        EventManager.BroadcastInternalForLua(EnumEventType.OnLoginConnectStatusChanged,{})
    end
end

CServerWnd.m_hookOnChooseServerButtonClick = function(this, go)
    if CommonDefs.IsInMobileDevice() and (not NativeTools.IsAllChannelApk()) and (not this.loginButton.Enabled) then
        if not CLoginMgr.Inst.s_UsePort2 then return end
    end

    --选服的时候发现没有登录账号，则尝试登录一下unisdk账号，否则显示服务器列表。
    if CLoginMgr.Inst.UniSdk_NeedLogin and not CLoginMgr.Inst.UniSdk_LoginDone then
        CLoginMgr.Inst:UniSdkLogin(true)
        return
    end

    CUIManager.ShowUI("ServerChosenWnd")
end



--for gas3
CLoginMgr.m_hookShakeHand = function(this,cipher)
    if cipher>=0 then
        this.m_LoginConn:SetNetworkEncryptCipher(cipher)
    end
    this.m_LoginConn:DropSend(false)

    local accountName = this:GetAccountName()
    if accountName == nil or string.len(accountName) <= 0 then
        -- List<object> datas = new List<object>() { "[ERROR] UID of UniSdk is not assigned" };
        g_MessageMgr:ShowMessage("CUSTOM_STRING","[ERROR] UID of UniSdk is not assigned")
        this.m_LoginConn:Shutdown()
    end
    if SdkU3d.s_SupportGas3 then
        Gac2Login.RequestPreAuthorize_GAS3()
    else
        this:RequestAuthorize()
    end

    this.IsConnectingToLogin = false
    this:InitPreLoginInfo()
    if CommonDefs.IS_HMT_CLIENT then
        this:StartHeartBeatTick()
    end
end

function LuaLoginMgr.RequestAuthorize()
    local sauthJson = SdkU3d.getPropStr(ConstProp.SAUTH_JSON)

    Gac2Login.SendSAuthJsonDataBegin_GAS3()

    local sauthJsonLen = CommonDefs.StringLength( sauthJson )
    if sauthJsonLen>0 then
        local segments = CommonDefs.StringSplit_Segment(sauthJson, 200, 250)
        for k = 0, segments.Length - 1 do
            Gac2Login.SendSAuthJsonData_GAS3(segments[k])
        end
    end

    Gac2Login.SendSAuthJsonDataEnd_GAS3()

    local accountName = CLoginMgr.Inst:GetAccountName()
    local bGuestBind = false
    local guest_uid = ""
    if CLoginMgr.Inst.m_UniSdkLoginInfo then
        bGuestBind = CLoginMgr.Inst.m_UniSdkLoginInfo.UniSdk_GUEST_BIND
        guest_uid = CLoginMgr.Inst.m_UniSdkLoginInfo.UniSdk_ORI_GUEST_UID
    end
    local device_id = SystemInfo.deviceUniqueIdentifier

    local bUsingPcUniSdk = CLoginMgr.Inst.IsPcUniSdkAuthorize or CSwitchMgr.EnablePcUniSdkAccount
	if CLoginMgr.IsPcPlatform() and bUsingPcUniSdk then
		if CLoginMgr.Inst.UID and string.len(CLoginMgr.Inst.UID) > 0 then
			accountName = CLoginMgr.Inst.UID
		end
	end
    local bUsingUniSdk = true
    if CLoginMgr.UniSdkDisable() or (CLoginMgr.IsPcPlatform() and not (CSwitchMgr.EnablePcUniSdkAccount or bUsingPcUniSdk)) then
        bUsingUniSdk = false

        CLoginMgr.Inst.m_UniSdkSauthInfo.gameid = ""
        CLoginMgr.Inst.m_UniSdkSauthInfo.hostid = 0
        CLoginMgr.Inst.m_UniSdkSauthInfo.login_channel = ""
        CLoginMgr.Inst.m_UniSdkSauthInfo.app_channel = ""
        CLoginMgr.Inst.m_UniSdkSauthInfo.platform = ""
        CLoginMgr.Inst.m_UniSdkSauthInfo.ip = ""
        CLoginMgr.Inst.m_UniSdkSauthInfo.username = ""
        CLoginMgr.Inst.m_UniSdkSauthInfo.udid = ""
		CLoginMgr.Inst.m_UniSdkSauthInfo.src_udid = ""
        CLoginMgr.Inst.m_UniSdkSauthInfo.sessionid = ""
        CLoginMgr.Inst.m_UniSdkSauthInfo.sdk_version = ""
        CLoginMgr.Inst.m_UniSdkSauthInfo.deviceid = ""
    end

	local username = CLoginMgr.Inst.m_UniSdkSauthInfo.username or ""
    local cpuName = CommonDefs.IsAndroidPlatform() and NativeTools.GetAndroidCPUName() or ""
    if cs_string.IsNullOrEmpty(cpuName) then cpuName = "Unknown" end --不能为空字符串，否则影响#解析
    local deviceModel = SafeStringFormat3( "%s#%s#%s#%s##%s#%s",ToStringWrap(SystemInfo.deviceType),SystemInfo.deviceModel,SystemInfo.processorCount,ToStringWrap(SystemInfo.graphicsDeviceType), cpuName, ToStringWrap(NativeTools.GetNativeSystemMemorySize()))
    local deviceHeight = Screen.height
    local deviceWidth = Screen.width
    local osName = CLoginMgr.Inst.m_UniSdkSauthInfo.platform or ""
    local macAddr = NativeUtility.GetMacAddress() or ""

    local networkType = NativeUtility.GetDetailNetworkInfo();
    local network = "none"
    if networkType == EnumNetworkType.Wifi then
        network = "wifi"
    elseif networkType == EnumNetworkType.MobileEdge then
        network = "2.5g"
    elseif networkType == EnumNetworkType.Mobile3G then
        network = "3g"
    elseif networkType == EnumNetworkType.Mobile4G then
        network = "4g"
    end
    local appVer = CClientVersion.g_VersionInfo
    local isp = NativeUtility.GetCarrierName() or ""
    local deviceName = SystemInfo.deviceName
    local isEmulator = CSystemInfoMgr.Inst:IsEmulator() and "1" or "0"
    local isRoot = CSystemInfoMgr.Inst:IsRooted() and "1" or "0"

	local win64 = nil
    local deviceType = PlatformDefine.ANDROID
    if PlatformDefine.IOS == SdkU3d.getPlatform() then
        deviceType = Extensions.GetIOSDeviceType()
    elseif CLoginMgr.IsPcPlatform() then--pc
        deviceType = PlatformDefine.PC
		win64 = CommonDefs.IsWin64OperatingSystem()
    end

    local clientId = CLoginMgr.Inst.m_UniSdkSauthInfo.udid
    if CommonDefs.IsIOSPlatform() then
        -- if Gas2Gac.s_LogAppleId then
        local IphoneUtility = import "IphoneUtility"
        clientId = IphoneUtility.GetAppleId_IOS()
        -- end
    end

    local app_channel = CLoginMgr.Inst:GetAppChannel()

    -- 提取游戏自己需要的渠道数据
    local extraLoginData = {}
    if app_channel == ChannelDefine.OPPO then
        extraLoginData.isFromGameCenter = SdkU3d.getPropInt("isFromGameCenter",0)
    elseif app_channel == ChannelDefine.QUICK then
        extraLoginData.extra_data = SdkU3d.getPropStr("extra_data")
	elseif app_channel == ChannelDefine.COOLPAD then
		extraLoginData.pl_graph = CLoginMgr.Inst:GetAppMetadata_PL_GRAPH()
	elseif app_channel == ChannelDefine.KUAIFA then
		extraLoginData.pl_graph = CLoginMgr.Inst:GetAppMetadata_PL_GRAPH()
    elseif app_channel == ChannelDefine.HUAWEI then
        extraLoginData.HW_DISPLAY_NAME = SdkU3d.getPropStr("HW_DISPLAY_NAME")
    elseif app_channel == ChannelDefine.VIVO then
        extraLoginData.USR_NAME = SdkU3d.getPropStr(ConstProp.USR_NAME)
        extraLoginData.identifier = Application.identifier
    elseif app_channel == ChannelDefine.XIAOMI then
        extraLoginData.MI_DISPLAY_NAME = SdkU3d.getPropStr("MI_DISPLAY_NAME")
	elseif app_channel == ChannelDefine.APPSTORE then
		extraLoginData.deviceCAID = DRPFMgr.Inst:GetDeviceCAID()
	elseif app_channel == ChannelDefine.NETEASE then
		if SdkU3d.getPayChannel() == ChannelDefine.GOOGLE then
			extraLoginData.payChannel = ChannelDefine.GOOGLE 
		end
    end

	if CommonDefs.IsAndroidPlatform() and not NativeTools.IsAllChannelApk() then
		if CommonDefs.IS_CN_CLIENT then
			extraLoginData.deviceOAID = SdkU3d.getPropStr("OAID")
		else
			extraLoginData.deviceOAID = SdkU3d.getPropStr("MSA_OAID")
		end
	end

    if not CommonDefs.IS_PUB_RELEASE and not bUsingPcUniSdk then
        local debugName = CLoginMgr.Inst._debugAccountName
        if debugName and debugName~="" then
            accountName = debugName
            bUsingUniSdk = false
        end
    end

    local isSendPcTicket = false
    if CSwitchMgr.EnablePcLogin then
        local pcLogin_IsSendPcTicket = CLoginMgr.Inst.IsSendPcTicket
        if pcLogin_IsSendPcTicket then
            isSendPcTicket = true
        end
    end
    local isSendPcTicketForPay = false
    if CSwitchMgr.EnablePcPay then
        local pcLogin_IsSendPcPayTicket = CPayMgr.Inst.IsSendPcTicketForPay
        if pcLogin_IsSendPcPayTicket then
            isSendPcTicketForPay = true
        end
    end

    local isSendPcTicketForPayConfirm = false
    if CSwitchMgr.EnablePcPay then
        local pcLogin_IsSendPcPayConfirmTicket = CPayMgr.Inst.IsSendPcTicketForPayConfirm
        if pcLogin_IsSendPcPayConfirmTicket then
            isSendPcTicketForPayConfirm = true
        end
    end
    local isSauthForCBGChannelAuth = false
    if CSwitchMgr.EnableCBGChannelAuth then
        isSauthForCBGChannelAuth = CLoginMgr.Inst.IsSauthForCBGChannelAuth
    end
	local isSendQRCodeScanReport = false
	if CSwitchMgr.EnableQRCodeScanReport then
		isSendQRCodeScanReport = CLoginMgr.Inst.IsSendQRCodeScanReport
	end
    local sauthReason = nil--EnumSauthSpecialReason.eNone
    -- List<object> sauthReasonExtraList = new List<object>();
    if isSauthForCBGChannelAuth then 
        sauthReason = 1--EnumSauthSpecialReason.eCBGChannelAuth
	elseif isSendQRCodeScanReport then
		sauthReason = 2--EnumSauthSpecialReason.eQRCodeScanReport
    end

    local pcTicket = nil
    local sendPcTicketEvent = nil
    if isSendPcTicket then
        pcTicket = CLoginMgr.Inst.PcTicket or ""
        sendPcTicketEvent = EnumSendPcTicketEvent.eMobileScanLogin
    elseif isSendPcTicketForPayConfirm then
        pcTicket = CPayMgr.Inst.PcPayTicket or ""
        sendPcTicketEvent = EnumSendPcTicketEvent.ePcUniSdkPayConfirm
    elseif isSendPcTicketForPay then
        pcTicket = CPayMgr.Inst.PcPayTicket or ""
        sendPcTicketEvent = EnumSendPcTicketEvent.eMobileScanPay
    -- elseif sauthReason ~= EnumSauthSpecialReason.eNone then
    end

	local lebian_yun = nil
	if CommonDefs.IS_LEBIAN_YUN_CLIENT then
		lebian_yun = true
	end

	local allchannel_apk = nil
	if NativeTools.IsAllChannelApk() then
		allchannel_apk = true
	end

	local src_udid
	if CLoginMgr.IsPcPlatform() and bUsingPcUniSdk then
		src_udid = CLoginMgr.Inst.m_UniSdkSauthInfo.src_udid
	end
	local unityVersion = CommonDefs.GetUnityVersion()
	local versionStr = SafeStringFormat3("%s:%d",  PatchMgr.Inst.GameVersion, unityVersion)
	local authorizeJson = JSON:encode(
        {
            account = accountName,
            bGuestBind = bGuestBind,
            guestAccount = guest_uid,
            password = device_id,
            versionInfo = versionStr,
            bUsingUniSdk = bUsingUniSdk,
            bUsingPcUniSdk = bUsingPcUniSdk,
            username = username,
            deviceModel = deviceModel,
            deviceHeight = deviceHeight,
            deviceWidth = deviceWidth,
            osName = osName,
            macAddr = macAddr,
            network=network,
            appVer = appVer,
            isp = isp,
            deviceName = deviceName,
            isEmulator = isEmulator,
            isRoot = isRoot,
            deviceType = deviceType,
            clientId = clientId,
            pcTicket = pcTicket, -- TODO 根据实际情况填充
            sendPcTicketEvent = sendPcTicketEvent,
            extraLoginData = extraLoginData,
            sauthReason = sauthReason,
            os_ver = SystemInfo.operatingSystem,
			win64 = win64,
			lebian_yun = lebian_yun,
			allchannel_apk = allchannel_apk,
			src_udid = src_udid,
            language = LocalString.language,
            country_code = LuaLoginMgr.m_countryCode and LuaLoginMgr.m_countryCode or 0, -- 没有取到值的时候使用0, 这样下次会继续设置
            media_src = LuaSEASdkMgr:GetMediaSource(),
            campaign_id = LuaSEASdkMgr:GetCampaignId(),
        })
         
    -- Gac2Login.SendAuthorizeJsonDataBegin_GAS3()
    -- SendAuthorizeJsonData_GAS3
    -- Gac2Login.SendAuthorizeJsonDataEnd_GAS3()
    local jsonLength = CommonDefs.StringLength( authorizeJson )
    if jsonLength>0 then
        Gac2Login.SendAuthorizeJsonDataBegin_GAS3()

        local segments = CommonDefs.StringSplit_Segment(authorizeJson, 200, 250)
        for k = 0, segments.Length - 1 do
            Gac2Login.SendAuthorizeJsonData_GAS3(segments[k])
        end

        Gac2Login.SendAuthorizeJsonDataEnd_GAS3()
    end

    CLoginMgr.Inst:InitExistingPlayerList()
end

CLoginMgr.m_hookOnSendAllClientPatchDone = function (this)
    LuaLoginMgr.RequestAuthorize()
end


function LuaLoginMgr.StopPcUniSdkLogin()
	CLoginMgr.Inst:Disconnect()
	CLoginMgr.Inst:ForceStopPcUniSdkLogin()
	CLoginMgr.Inst:UniSdkLogout()
end

CLoginMgr.m_hookTryRequestAuthorizeForPcUniSdkLogin = function(this)
	if not CSwitchMgr.EnablePcLogin then
		LuaLoginMgr.StopPcUniSdkLogin()
		return
	end

	-- pcunisdk必须使用gas3
	if not (SdkU3d.s_EnablePcUniSdk and SdkU3d.s_SupportGas3) then
		LuaLoginMgr.StopPcUniSdkLogin()
		return
	end

	if this:CheckLoginConnection() then
		local isVerifyPcUniSdkLogin = CLoginMgr.Inst.IsVerifyPcUniSdkLogin
		if not isVerifyPcUniSdkLogin then
			LuaLoginMgr.StopPcUniSdkLogin()
			return
		end

		LuaLoginMgr.RequestAuthorize()
	else
		local msg = LocalString.GetString("服务器连接已断开，重新连接")
		MessageWndManager.ShowOKMessage(msg, nil, DelegateFactory.Action(function()
			LuaLoginMgr.StopPcUniSdkLogin()
		end))
	end
end


local CServerWnd = import "L10.UI.CServerWnd"

CServerWnd.m_hookOnClickQRReadButton = function(obj)
    LuaLoginMgr.OnClickQRCodeButton(true)
end

local  s_MinQRSupportVersion = 199645
local s_MinNtQRSupportVersion = 443088 --渠道最低支持unisdk扫码的版本号
local s_MinNeteaseNtQRSupportVersion = 339783--官网最低支持unisdk扫码的版本号

function LuaLoginMgr.OnClickQRCodeButton(bFromLogin)
    if CommonDefs.IsAndroidPlatform() or CommonDefs.IsIOSPlatform() then
        --Android和iOS平台没开启权限时先请求权限，异步过程
        if NativeTools.IsSupportRequestPermission() then
            if not NativeTools.HasUserAuthorizedPermission(NativeTools.PermissionNames.CAMERA) then
                NativeTools.RequestUserPermissionOrPromptIfDenied(NativeTools.PermissionNames.CAMERA, nil);
                return
            end
        end
    end

    local engineVersion = Main.Inst.EngineVersion

    if engineVersion > 0 and engineVersion < s_MinQRSupportVersion then
        MessageWndManager.ShowOKMessage(LocalString.GetString("你的客户端内核版本太旧，如需使用该功能，请下载最新的客户端重新安装"), nil)
        return
    end

    if not CLoginMgr.Inst.UniSdk_LoginDone then
        CLoginMgr.Inst:UniSdkLogin(true)
        return
    end

	if SdkU3d.s_EnableNtQRCodeScanner and CommonDefs.IsIOSPlatform() then
		if bFromLogin then
			LuaLoginMgr.NtPresentQRCodeScannerWithReport()
		else
			SdkU3d.ntPresentQRCodeScanner()
		end
	elseif SdkU3d.s_EnableNtQRCodeScanner and CommonDefs.IsAndroidPlatform() and CLoginMgr.Inst:IsNetEaseOfficialLogin() and engineVersion>=s_MinNeteaseNtQRSupportVersion then
		if bFromLogin then
			LuaLoginMgr.NtPresentQRCodeScannerWithReport()
		else
			-- https://unisdk.nie.netease.com/doc/page/ntqrcode/zh/andriod
			-- 云端必须使用带参数接口
			if SdkU3d.IsCloudGameCloudClient() then
				SdkU3d.ntPresentQRCodeScanner("",0)
			else
				SdkU3d.ntPresentQRCodeScanner()
			end
		end
    elseif SdkU3d.s_EnableNtQRCodeScanner and CommonDefs.IsAndroidPlatform() and not CLoginMgr.Inst:IsNetEaseOfficialLogin() and engineVersion>=s_MinNtQRSupportVersion then
        -- https://unisdk.nie.netease.com/doc/page/android/zh/n/netease-codescanner#t-1-1
        SdkU3d.ntPresentQRCodeScanner("",21);
    else
		EasyCodeScanner.Initialize()
		EasyCodeScanner.launchScanner(true, LocalString.GetString("请将取景框对准二维码"), EasyCodeScanner.s_Symbology, true)
	end
end

function LuaLoginMgr.NtPresentQRCodeScannerWithReport()
	if not CLoginMgr.Inst.UniSdk_LoginDone then
        CLoginMgr.Inst:UniSdkLogin(true)
        return
    end

	local selectedServer = CLoginMgr.Inst:GetSelectedGameServer()
	if selectedServer == nil then
		CUIManager.ShowUI(CUIResources.ServerChosenWnd)
		return
	end

	-- 尝试连接当前服务器
	CLoginMgr.Inst.IsVerifyQRCodeScanReport = true
	CLoginMgr.Inst:TryConnectLoginServerForQRCodeScanReport()

	if CLoginMgr.Inst.IsConnectingToLogin then
		-- 连接中，等待服务器rpc
	else
		-- 未连接，直接打开扫码
		if CommonDefs.IsAndroidPlatform() and SdkU3d.IsCloudGameCloudClient() then
			SdkU3d.ntPresentQRCodeScanner("",0)
		else
			SdkU3d.ntPresentQRCodeScanner()
		end
	end
end

CLoginMgr.m_hookOnQRCodeScanReportDone = function(this)
	CLoginMgr.Inst:Disconnect()
	if CommonDefs.IsAndroidPlatform() and SdkU3d.IsCloudGameCloudClient() then
		SdkU3d.ntPresentQRCodeScanner("",0)
	else
		SdkU3d.ntPresentQRCodeScanner()
	end
end

LuaLoginMgr.m_AIDetectDelayRefreshUID = nil
LuaLoginMgr.m_AIDetectDelayRefreshStage = nil
LuaLoginMgr.m_countryCode = nil
g_ScriptEvent:AddListener("MainPlayerCreated", LuaLoginMgr, "OnMainPlayerCreated")
g_ScriptEvent:AddListener("OnGetCountryCode", LuaLoginMgr, "OnGetCountryCode")

function LuaLoginMgr.OnMainPlayerCreated()
	if LuaLoginMgr.m_AIDetectDelayRefreshUID then
		local engineVersion = Main.Inst.EngineVersion
		if engineVersion >= 458667 and LuaLoginMgr.m_AIDetectDelayRefreshUID == CLoginMgr.Inst.UID then
			local stage = LuaLoginMgr.m_AIDetectDelayRefreshStage
			local infoDict = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
			Gac2Gas.UniAIDetectRefresh(stage, MsgPackImpl.pack(infoDict))
			CLogMgr.Log(SafeStringFormat3("AIDetectDelayRefresh OnMainPlayerCreated, uid:%s, stage:%s", LuaLoginMgr.m_AIDetectDelayRefreshUID, LuaLoginMgr.m_AIDetectDelayRefreshStage))
		end
		LuaLoginMgr.m_AIDetectDelayRefreshUID = nil
		LuaLoginMgr.m_AIDetectDelayRefreshStage = nil
	end
end

function LuaLoginMgr:OnGetCountryCode(countryCode)
    LuaLoginMgr.m_countryCode = countryCode
end

CLoginMgr.m_hookOnUnisdkOnExtendFuncCall = function(this, code, dict)
	-- 人脸识别方案1认证回调
	if not dict or not CommonDefs.DictContains_LuaCall(dict, "methodId") then
		return
	end

	local methodId = tostring(CommonDefs.DictGetValue_LuaCall(dict, "methodId"))
	if methodId == "cbgStartupRequest" then
		CLoginMgr.Inst.IsCBGStartUpRequest = true
		this:RequestCBGChannelAuthByUnisdk()
		CLogMgr.Log(SafeStringFormat3("OnUnisdkOnExtendFuncCall methodId:%s", methodId))
	elseif methodId == "cbgChannelAuthRequest" then
		CLoginMgr.Inst.IsCBGStartUpRequest = false
        this:RequestCBGChannelAuthByUnisdk()
        CLogMgr.Log(SafeStringFormat3("OnUnisdkOnExtendFuncCall methodId:{0}", methodId))
	elseif methodId == "mpayOnUILessQrCodeLoginFinish" then
		local subroutine = tonumber(CommonDefs.DictGetValue_LuaCall(dict, "subroutine"))
		local logincode = tonumber(CommonDefs.DictGetValue_LuaCall(dict, "code"))
		local jsonStr = tostring(CommonDefs.DictGetValue_LuaCall(dict, "json"))
		this:OnUILessQrCodePcUniSdkLoginFinish(subroutine, logincode, jsonStr)
		CLogMgr.Log(SafeStringFormat3("OnUnisdkOnExtendFuncCall methodId:%s, subroutine:%s, code:%s, json:%s", methodId, subroutine, logincode, jsonStr))
	elseif methodId == "aiDetectRefresh" then
		local stage = CommonDefs.DictGetValue_LuaCall(dict, "stage") or ""
		local engineVersion = Main.Inst.EngineVersion
		if engineVersion >= 458667 then
			if this:IsInLoginScene() and this:CheckLoginConnection() and not this.IsConnectingToLogin then
				local infoDict = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
				Gac2Login.UniAIDetectRefresh(stage, MsgPackImpl.pack(infoDict))
			elseif this:IsInGame() and CGasConnMgr.Inst.GasConn:IsAlive() and CClientMainPlayer.Inst ~= nil then
				local infoDict = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
				Gac2Gas.UniAIDetectRefresh(stage, MsgPackImpl.pack(infoDict))
			elseif CLoginMgr.Inst.UniSdk_LoginDone then
				LuaLoginMgr.m_AIDetectDelayRefreshUID = CLoginMgr.Inst.UID
				LuaLoginMgr.m_AIDetectDelayRefreshStage = stage
				CLogMgr.Log(SafeStringFormat3("NeedAIDetectDelayRefresh, uid:%s, stage:%s", LuaLoginMgr.m_AIDetectDelayRefreshUID, LuaLoginMgr.m_AIDetectDelayRefreshStage))
			end
		end
		CLogMgr.Log(SafeStringFormat3("OnUnisdkOnExtendFuncCall methodId:%s, stage:%s, engineVersion:%s", methodId, stage, engineVersion))
	elseif methodId == "startIdentifyFail" then
		local errorMessage = CommonDefs.DictGetValue_LuaCall(dict, "errorMessage") or ""
		CLogMgr.Log(SafeStringFormat3("OnUnisdkOnExtendFuncCall methodId:%s, errorMessage:%s", methodId, errorMessage))
	elseif methodId == "onAasExitGame" then
		CLoginMgr.Inst:Disconnect()
		CLoginMgr.Inst:UniSdkLogout()
        CLogMgr.Log(SafeStringFormat3("OnUnisdkOnExtendFuncCall methodId:%s", methodId))
    elseif methodId == "fromDeepLink" then
		CDeepLinkMgr.Inst:CheckOnUnisdkOnExtendFuncCall(code, dict)
	end
end

LuaLoginMgr.m_DaShenAIDetectUrlData = nil
function LuaLoginMgr.TryConfirmShowDaShenAIDetectUrl()
	if not LuaLoginMgr.m_DaShenAIDetectUrlData then
		return
	end

	local token, account, gameCode, timestamp = unpack(LuaLoginMgr.m_DaShenAIDetectUrlData)
	if not (token and account and gameCode and timestamp) then
		return
	end

	local msg = LocalString.GetString("欢迎进入游戏，为营造健康的游戏环境，我们需要对您的身份信息进行人脸识别验证，您未验证成功前，您的角色可能受到游戏时长、时段和充值限制，请点击进入验证流程")
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		LuaLoginMgr.m_DaShenAIDetectUrlData = nil
		local url = SafeStringFormat3("https://app.16163.com/ds/ulinks/?action=antiAddiction&token=%s&account=%s&gameCode=%s&timestamp=%s&source=UNI_SDK_LINK&utm_term=wyds_dl_underage_dssdk&business=LOGIN",
			token, account, gameCode, timestamp)
		Application.OpenURL(url)
		CLogMgr.Log(SafeStringFormat3("dashen aidetect openurl:%s", url))
	end),DelegateFactory.Action(function()
		LuaLoginMgr.m_DaShenAIDetectUrlData = nil
	end),LocalString.GetString("同意"),LocalString.GetString("暂不"),false)
end

function LuaLoginMgr.TryOpenDaShenAIDetectUrl(unisdk_login_json)
	--人脸识别方案2打开网页
	local engineVersion = Main.Inst.EngineVersion
	if CLoginMgr.IsQRCodeLoginClient() or (engineVersion > 0 and engineVersion < 458667) then
		local base64EncodedBytes = CommonDefs.Convert_FromBase64String(unisdk_login_json)
		local jsonStr = base64EncodedBytes and CommonDefs.UTF8Encoding_GetString(base64EncodedBytes)
		local json = jsonStr and JSON:decode(jsonStr)
		local dashen = json and json.dashen
		if dashen then
			local token = WWW.EscapeURL(dashen.black_adult_sign or "")
			local timestamp = dashen.timestamp or ""
			local account = WWW.EscapeURL(json.username or "")
			local gameCode = CommonDefs.ProjectName

			-- 不自动登录
			CLoginMgr.Inst.m_Back2LoginScene = true

			LuaLoginMgr.m_DaShenAIDetectUrlData = {token, account, gameCode, timestamp}

			-- 弹二次确认，如果直接进选角界面，则选角界面初始化完毕再弹一次
			LuaLoginMgr.TryConfirmShowDaShenAIDetectUrl()
		end
	end
end

CLoginMgr.m_hookOnGameLoginPreSuccess_GAS3 = function(this, type, data)
	if type == "unisdk_login_json" then
		SdkU3d.setPropStr(ConstProp.UNISDK_LOGIN_JSON, data)
		-- 登录时尝试清一遍
		LuaLoginMgr.m_DaShenAIDetectUrlData = nil
	elseif type == "dashen_aidetect2" then
		LuaLoginMgr.TryOpenDaShenAIDetectUrl(data)
	end
end

CLoginMgr.m_hookOnGameLoginPreSuccess = function(this, type, data)
	if type == "unisdk_login_json" then
		SdkU3d.setPropStr(ConstProp.UNISDK_LOGIN_JSON, data)
		-- 登录时尝试清一遍
		LuaLoginMgr.m_DaShenAIDetectUrlData = nil
	elseif type == "dashen_aidetect2" then
		LuaLoginMgr.TryOpenDaShenAIDetectUrl(data)
	end
end

CLoginMgr.m_hookShowAppUpgradeNotify = function(this, type, content_U)
	if g_LoginShowAppUpgradeNotifyCallback[type] then
		g_LoginShowAppUpgradeNotifyCallback[type](type, content_U)
	end
end

CLoginMgr.m_hookOnSendPinchFaceDuration = function(this, limit)
    LuaPinchFaceTimeCtrlMgr:OnSendPinchFaceDuration(limit)
end

CLoginMgr.m_hookOnLoginTimeout = function(this)
    LuaPinchFaceTimeCtrlMgr:OnLoginTimeout()
end

function LuaLoginMgr.TryDownloadNewVersion(type, url)
	LuaDownloadNewVersionWnd.s_Type = type
    LuaDownloadNewVersionWnd.s_Url = url
    CUIManager.ShowUI(CLuaUIResources.DownloadNewVersionWnd)
end

LuaLoginMgr.m_PichFaceLoginQueueWnd = true

function LuaLoginMgr:SetServerOpenTime(serverOpenPassedDays, context)
    self.m_ServerOpenTime = serverOpenPassedDays
    g_ScriptEvent:BroadcastInLua("GetServerOpenTime",serverOpenPassedDays, context)
end

function LuaLoginMgr:GetQueueStatusRemindText(pos, time)
    return g_MessageMgr:FormatMessage("PINCH_FACE_LOGIN_QUEUE_STATUS", pos, time)
end

function LuaLoginMgr:ShowQueueStatusTopNotice()
    local pos = CLoginQueueWnd.pos
    local time = CLoginQueueWnd.time
    g_MessageMgr:HideTopNotice()
    g_MessageMgr:ShowTopNotice(function(sec) 
        return self:GetQueueStatusRemindText(pos, time)
    end, 2)
end

function LuaLoginMgr:HideQueueStatusTopNotice()
    g_MessageMgr:HideTopNotice()
end

function LuaLoginMgr.EnableNewCharacterCreationWnd(isEnable)
    if not CUIManager.instance then
        return
    end
    local CUIModuleList = MakeGenericClass(List, CUIModule)
    local listToAdd = Table2List({isEnable and CUIResources.BeforeEnterDengZhongShiJieWnd or CUIResources.CharacterCreationWnd}, CUIModuleList)
    CommonDefs.DictSet(CUIManager.instance.mUIGroups, typeof(Int32), EnumToInt(EUIModuleGroup.ELoginUI), typeof(CUIModuleList), listToAdd)
end

CLoginMgr.m_hookConfirmStartCreatePlayer = function(this)
    g_MessageMgr:HideTopNotice()
    g_MessageMgr:ShowConfirmMessageWithBtnDuration(
        g_MessageMgr:FormatMessage("PINCH_FACE_WAIT_ENTER_CREATING_PLAYER"), 
        LocalString.GetString("进入"), 
        DelegateFactory.Action(function()
            LuaPinchFaceMgr.m_IsOffline = false
            CUIManager.CloseUI(CLuaUIResources.PinchFaceWnd)
            CLoginMgr.Inst:StartCreatePlayer()
        end),
        30
    )
end