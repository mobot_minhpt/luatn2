CLuaFrozenSilverWnd = class()
RegistClassMember(CLuaFrozenSilverWnd,"frozenSilverLabel")
RegistClassMember(CLuaFrozenSilverWnd,"unfrozenSilverLabel")
RegistClassMember(CLuaFrozenSilverWnd,"packageSilverLabel")
RegistClassMember(CLuaFrozenSilverWnd,"detailBtn")
RegistClassMember(CLuaFrozenSilverWnd,"retrievalBtn")
RegistClassMember(CLuaFrozenSilverWnd,"closeBtn")

function CLuaFrozenSilverWnd:Awake()
    self.frozenSilverLabel = self.transform:Find("Anchor/FrozenSilver/Label"):GetComponent(typeof(UILabel))
    self.unfrozenSilverLabel = self.transform:Find("Anchor/UnfrozenSilver/Label"):GetComponent(typeof(UILabel))
    self.packageSilverLabel = self.transform:Find("Anchor/PackageSilver/Label"):GetComponent(typeof(UILabel))
    self.detailBtn = self.transform:Find("Anchor/DetailBtn"):GetComponent(typeof(L10.UI.CButton))
    self.retrievalBtn = self.transform:Find("Anchor/RetrievalBtn"):GetComponent(typeof(L10.UI.CButton))

    UIEventListener.Get(self.retrievalBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnRetrievalButtonClick(go)
    end)
    UIEventListener.Get(self.detailBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CUIResources.FrozenSilverDetailWnd)
    end)
end

function CLuaFrozenSilverWnd:Init( )
    if CClientMainPlayer.Inst ~= nil then
        local total = 0
        CommonDefs.DictIterate(CClientMainPlayer.Inst.ItemProp.TradeFrozenSilvers, DelegateFactory.Action_object_object(function (___key, ___value) 
            if ___value.Enabled == 1 then
                total = total + ___value.Silver
            end
        end))
        self.frozenSilverLabel.text = tostring(total)
        self.unfrozenSilverLabel.text = tostring(CClientMainPlayer.Inst.ItemProp.RepertorySilver)
        self.packageSilverLabel.text = tostring(CClientMainPlayer.Inst.Silver)
        self.retrievalBtn.Enabled = (CClientMainPlayer.Inst.ItemProp.RepertorySilver > 0)
    else
        self.frozenSilverLabel.text = nil
        self.unfrozenSilverLabel.text = nil
        self.packageSilverLabel.text = nil
        self.retrievalBtn.Enabled = false
    end


end

function CLuaFrozenSilverWnd:OnEnable()
	g_ScriptEvent:AddListener("MainPlayerUpdateMoney", self, "MainPlayerUpdateMoney")

end
function CLuaFrozenSilverWnd:OnDisable()
	g_ScriptEvent:RemoveListener("MainPlayerUpdateMoney", self, "MainPlayerUpdateMoney")
end

function CLuaFrozenSilverWnd:OnRetrievalButtonClick( go) 
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.ItemProp.RepertorySilver > 0 then
        CLuaNumberInputMgr.ShowNumInputBox(1, CClientMainPlayer.Inst.ItemProp.RepertorySilver, CClientMainPlayer.Inst.ItemProp.RepertorySilver, function (val) 
            if val > 0 then
                Gac2Gas.MoveSilverFromRepertoryToBag(val)
            end
        end, nil, -1)
    end
end
function CLuaFrozenSilverWnd:MainPlayerUpdateMoney()
    self:Init()
end
