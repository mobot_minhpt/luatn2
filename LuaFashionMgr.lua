local CFashionInfo = import "L10.Game.CFashionInfo"
local CUIResources = import "L10.UI.CUIResources"
local UInt64 = import "System.UInt64"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFashionMgr = import "L10.Game.CFashionMgr"

LuaFashionMgr = {}

--[[
    @desc: 获取背饰拓本外观等级
    author:Codee
    time:2022-09-07 19:31:29
    --@fid: 拓本配置表id
	--@level: 锻造等级
    @return: 外观等级
]]
function LuaFashionMgr.GetBackPendantAppearence(fid, level)
    local res = 0
    local equiptemp = EquipmentTemplate_Equip.GetData(fid)
    if not equiptemp then
        return 0
    end
    local tclevel = equiptemp.Grade
    ExtraEquip_BackPendantAppearance.Foreach(
        function(id, data)
            if tclevel < data.EquipTC or level < data.ForgeLevel then
                return res
            else
                res = id
            end
        end
    )
    return res
end

function LuaFashionMgr.FashionHaveHeadAlpha(fid, enumgender)
    if not fid or fid <= 0 then
        return false
    end
    local fdata = Fashion_Fashion.GetData(fid)
    if not fdata then
        return false
    end
    if fdata.HasHeadAlpha01 == 0 then
        return false
    end
    if fdata.CanHideHeadAlpha == 0 then
        return false
    end

    local gender = EnumToInt(enumgender)

    if fdata.HasHeadAlpha01 == 1 then
        return fdata.CanHideHeadAlpha == 1 or fdata.CanHideHeadAlpha == 3 - gender
    end

    if fdata.HasHeadAlpha01 ~= fdata.CanHideHeadAlpha then
        return false
    end

    return fdata.CanHideHeadAlpha == 3 - gender
end

function LuaFashionMgr.HaveHeadAlpha(adata)
    if not adata then
        return false
    end

    local ishide = adata.HideHeadFashionEffect
    if ishide > 0 then
        return false
    end

    local fid = adata.HeadFashionId

    return LuaFashionMgr.FashionHaveHeadAlpha(fid, adata.Gender)
end

function Gas2Gac.AddFashion(fashionId, expireTime)
    LuaFashionMgr.m_CurAddFashionId = fashionId
    if CClientMainPlayer.Inst then
        local info = CFashionInfo()
        info.ExpireTime = expireTime
        info.IsExpired = 0
        if not CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.Fashion.Fashions, typeof(UInt64), fashionId) then
            CommonDefs.DictAdd(CClientMainPlayer.Inst.PlayProp.Fashion.Fashions, typeof(UInt64), fashionId, typeof(CFashionInfo), info)
        end
        if LuaAppearancePreviewMgr:IsAppearanceWndLoaded() then
            LuaAppearancePreviewMgr:CloseAppearanceWnd()
            CFashionMgr.Inst:ShowAppearanceWnd(fashionId)
        else
            if LuaOffWorldPassMgr.CheckSWWeapon(fashionId) then
                LuaOffWorldPassMgr.ShowFashionId = fashionId
                g_ScriptEvent:BroadcastInLua("UpdateOffWorldPlayGetSw")
            else
                local msg = g_MessageMgr:FormatMessage("GET_NEW_FASHION")
                MessageWndManager.ShowOKCancelMessage(
                    msg,
                    DelegateFactory.Action(
                        function()
                            LuaAppearancePreviewMgr:CloseAppearanceWnd()
                            CFashionMgr.Inst:ShowAppearanceWnd(fashionId)
                        end
                    ),
                    nil,
                    nil,
                    nil,
                    false
                )
            end
        end
    end
end
