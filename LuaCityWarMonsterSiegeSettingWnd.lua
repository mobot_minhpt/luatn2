local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CSortButton = import "L10.UI.CSortButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local QnRadioBox=import "L10.UI.QnRadioBox"
local QnTableView=import "L10.UI.QnTableView"
local Profession = import "L10.Game.Profession"
local Double = import "System.Double"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"


CLuaCityWarMonsterSiegeSettingWnd = class()
RegistClassMember(CLuaCityWarMonsterSiegeSettingWnd,"sortRadioBox")
RegistClassMember(CLuaCityWarMonsterSiegeSettingWnd,"closeButton")
RegistClassMember(CLuaCityWarMonsterSiegeSettingWnd,"tipButton")
RegistClassMember(CLuaCityWarMonsterSiegeSettingWnd,"tableView")
RegistClassMember(CLuaCityWarMonsterSiegeSettingWnd,"refreshBtn")
RegistClassMember(CLuaCityWarMonsterSiegeSettingWnd,"cancleAllBtn")
RegistClassMember(CLuaCityWarMonsterSiegeSettingWnd,"onlineNumLabel")
RegistClassMember(CLuaCityWarMonsterSiegeSettingWnd,"jingyingNumLabel")

RegistClassMember(CLuaCityWarMonsterSiegeSettingWnd,"mGuildMemberInfoList")--除了自己
RegistClassMember(CLuaCityWarMonsterSiegeSettingWnd,"m_MainPlayerGuildInfo")

RegistClassMember(CLuaCityWarMonsterSiegeSettingWnd,"m_SortFlags")

function CLuaCityWarMonsterSiegeSettingWnd:Awake()
    self.m_MainPlayerGuildInfo=nil
    self.m_SortFlags={-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}

    self.sortRadioBox = self.transform:Find("Anchor/TableView/Header"):GetComponent(typeof(QnRadioBox))
    self.tipButton = self.transform:Find("Anchor/TableView/Buttons/TipButton").gameObject
    self.tableView = self.transform:Find("Anchor/TableView"):GetComponent(typeof(QnTableView))
    self.refreshBtn = self.transform:Find("Anchor/TableView/Buttons/RefreshBtn").gameObject
    self.cancleAllBtn = self.transform:Find("Anchor/TableView/Buttons/CancelAllBtn").gameObject
    self.onlineNumLabel = self.transform:Find("Anchor/TableView/Buttons/Label"):GetComponent(typeof(UILabel))
    self.jingyingNumLabel = self.transform:Find("Anchor/TableView/Buttons/NumLabel"):GetComponent(typeof(UILabel))
    self.mGuildMemberInfoList = {}


    UIEventListener.Get(self.tipButton).onClick =DelegateFactory.VoidDelegate(function(go) self:OnClickTipButton(go) end)
    UIEventListener.Get(self.refreshBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClickRefreshButton(go) end)
    UIEventListener.Get(self.cancleAllBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClickCancleAllButton(go) end)

    self.sortRadioBox.OnSelect =DelegateFactory.Action_QnButton_int(function(btn,index) self:OnRadioBoxSelect(btn,index) end)

    local gongxianButton = self.transform:Find("Anchor/TableView/Buttons/GongXianButton").gameObject
    UIEventListener.Get(gongxianButton).onClick = DelegateFactory.VoidDelegate(function(go)
        local menulist = {
            PopupMenuItemData(LocalString.GetString("联赛评价"), DelegateFactory.Action_int(function(index) CUIManager.ShowUI(CLuaUIResources.GuildLeagueWeeklyInfoWnd) end), false, nil, EnumPopupMenuItemStyle.Default),
            PopupMenuItemData(LocalString.GetString("城战贡献"), DelegateFactory.Action_int(function(index) CUIManager.ShowUI(CLuaUIResources.CityWarContributionWnd) end), false, nil, EnumPopupMenuItemStyle.Default)
        }
        CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(menulist,MakeArrayClass(PopupMenuItemData)), go.transform, CPopupMenuInfoMgr.AlignType.Top, 1, nil, 430, true, 220)
    end)
end
function CLuaCityWarMonsterSiegeSettingWnd:OnRadioBoxSelect( btn, index) 
    local sortButton = TypeAs(btn, typeof(CSortButton))

    self.m_SortFlags[index+1] = - self.m_SortFlags[index+1]
    for i,v in ipairs(self.m_SortFlags) do
        if i~=index+1 then
            self.m_SortFlags[i] = -1
        end
    end

    sortButton:SetSortTipStatus(self.m_SortFlags[index+1] < 0)
    self:Sort(self.mGuildMemberInfoList, index)
    self.tableView:ReloadData(true, false)
end

function CLuaCityWarMonsterSiegeSettingWnd:OnClickCancleAllButton( go) 
    local msg = g_MessageMgr:FormatMessage("MonsterSiegeElite_CancleAllConfirm", nil)
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
        Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "ClearMonsterSiegeElite", "", "")
    end), nil, nil, nil, false)
end
function CLuaCityWarMonsterSiegeSettingWnd:OnClickRefreshButton( go) 
    CLuaGuildMgr.m_GuildMonsterSiegeInfo=nil
    Gac2Gas.RequestGetGuildInfo("MonsterSiegeEliteInfo")
end
function CLuaCityWarMonsterSiegeSettingWnd:OnClickTipButton( go) 
    g_MessageMgr:ShowMessage("MonsterSiegeElite_Tips")
end
function CLuaCityWarMonsterSiegeSettingWnd:Init( )
    local function initItem(item,row)
        if row % 2 == 0 then
            item:SetBackgroundTexture("common_textbg_02_dark")
        else
            item:SetBackgroundTexture("common_textbg_02_light")
        end

        if row==0 then
            --自己
            self:InitItem(item.transform,self.m_MainPlayerGuildInfo,row)
        else
            self:InitItem(item.transform,self.mGuildMemberInfoList[row],row)
        end
    end
    self.tableView.m_DataSource = DefaultTableViewDataSource.CreateByCount(1,initItem)

    CLuaGuildMgr.m_GuildMonsterSiegeInfo=nil
    Gac2Gas.RequestGetGuildInfo("MonsterSiegeEliteInfo")
end
function CLuaCityWarMonsterSiegeSettingWnd:OnEnable( )
    g_ScriptEvent:AddListener("SendGuildInfo_MonsterSiegeEliteInfo", self, "OnSendGuildInfo_MonsterSiegeEliteInfo")
    g_ScriptEvent:AddListener("RequestOperationInGuildSucceed", self, "OnRequestOperationInGuildSucceed")
end
function CLuaCityWarMonsterSiegeSettingWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("SendGuildInfo_MonsterSiegeEliteInfo", self, "OnSendGuildInfo_MonsterSiegeEliteInfo")
    g_ScriptEvent:RemoveListener("RequestOperationInGuildSucceed", self, "OnRequestOperationInGuildSucceed")
end

function CLuaCityWarMonsterSiegeSettingWnd:OnRequestOperationInGuildSucceed(args) 
    local requestType, paramStr=args[0] ,args[1]
    -- print(requestType,paramStr)

    if "SetMonsterSiegeElite" == requestType then
        local splits=g_LuaUtil:StrSplit(paramStr,",")
        if #splits==2 then
            local playerId =tonumber(splits[1])
            local isElite =tonumber(splits[2])
            --设置数据
            if CLuaGuildMgr.m_GuildMonsterSiegeInfo then
                local val=CLuaGuildMgr.m_GuildMonsterSiegeInfo[playerId]
                if val then val.IsMonsterSiegeElite=isElite end
            end

            --找到那个player，然后设置
            local parent=self.tableView.m_Grid.transform
            if self.m_MainPlayerGuildInfo.PlayerId==playerId then
                self:SetItemElite(parent:GetChild(0),0,isElite>0)
            else
                for i,v in ipairs(self.mGuildMemberInfoList) do
                    if v.PlayerId==playerId then
                            self:SetItemElite(parent:GetChild(i),i,isElite>0)
                        break
                    end
                end
            end

        end
    elseif "ClearMonsterSiegeElite" == requestType then
        --设置数据
        if CLuaGuildMgr.m_GuildMonsterSiegeInfo then
            for k,v in pairs(CLuaGuildMgr.m_GuildMonsterSiegeInfo) do
                v.IsMonsterSiegeElite=0
            end
        end
        --取消设置
        local parent=self.tableView.m_Grid.transform
        for i=1,parent.childCount do
            self:SetItemElite(parent:GetChild(i-1),i-1,false)
        end
    end

    if ("SetMonsterSiegeElite" == requestType) or ("ClearMonsterSiegeElite" == requestType) then
        self:UpdateJingyingNum()
    end
end
function CLuaCityWarMonsterSiegeSettingWnd:OnSendGuildInfo_MonsterSiegeEliteInfo( ) 
        self.mGuildMemberInfoList={}
        local myId = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id or 0

        if CLuaGuildMgr.m_GuildMonsterSiegeInfo then
            for k,v in pairs(CLuaGuildMgr.m_GuildMonsterSiegeInfo) do
                if k==myId then
                    self.m_MainPlayerGuildInfo=v
                else
                    if v.IsMonsterSiegeElite > 0 then--所有寇岛精英
                        table.insert( self.mGuildMemberInfoList, v )
                    else
                        if v.IsOnline > 0 then--在线玩家
                            table.insert( self.mGuildMemberInfoList, v )
                        end
                    end
                end
            end
        end
        --默认排序
        self.sortRadioBox:ChangeTo(- 1, true)
        self:Sort(self.mGuildMemberInfoList, 8)

        self.tableView.m_DataSource.count=(self.m_MainPlayerGuildInfo and 1 or 0) + #self.mGuildMemberInfoList
        self.tableView:ReloadData(true, false)

        self:UpdateMemberNum()
        self:UpdateJingyingNum()
    -- end
end
function CLuaCityWarMonsterSiegeSettingWnd:UpdateMemberNum( )
    local num = 0
    for i,v in ipairs(self.mGuildMemberInfoList) do
        if v.IsOnline>0 then
            num=num+1
        end
    end
    local me=(self.m_MainPlayerGuildInfo and 1 or 0)
    num = num + me

    self.onlineNumLabel.text = SafeStringFormat3(LocalString.GetString("在线人数：[00ff00]%d[-]/%d"), num, #self.mGuildMemberInfoList+me)
end
function CLuaCityWarMonsterSiegeSettingWnd:UpdateJingyingNum( )
    if self.mGuildMemberInfoList ~= nil then
        local num = 0
        local onlineNum = 0

        for i,v in ipairs(self.mGuildMemberInfoList) do
            if v.IsMonsterSiegeElite>0 then
                num=num+1
                if v.IsOnline>0 then
                    onlineNum=onlineNum+1
                end
            end
        end

        if self.m_MainPlayerGuildInfo then
            if self.m_MainPlayerGuildInfo.IsMonsterSiegeElite>0 then
                num=num+1
                if self.m_MainPlayerGuildInfo.IsOnline>0 then
                    onlineNum=onlineNum+1
                end
            end
        end

        self.jingyingNumLabel.text = SafeStringFormat3( LocalString.GetString("孤城守卫精英：[00ff00]%d[-]/%d/%d"), onlineNum, num, 40 )
    end
end

function CLuaCityWarMonsterSiegeSettingWnd:Sort(t,sortIndex)
    local function defaultSort(a,b)
        if a.IsInHardCopy > b.IsInHardCopy then
            return true
        elseif a.IsInHardCopy < b.IsInHardCopy then
            return false
        else
            return a.PlayerId<b.PlayerId
        end
    end
    local flag=self.m_SortFlags[sortIndex+1]
    if sortIndex==0 then--cls name
        table.sort( t, function(a,b)
            local ret=flag
            if a.Class<b.Class then
                ret = -flag
            elseif a.Class>b.Class then
                ret=flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==1 then--level
        table.sort( t, function(a,b)
            local ret=flag
            if a.Level<b.Level then
                ret = flag
            elseif a.Level>b.Level then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )

    elseif sortIndex==2 then--xiuwei
        table.sort( t, function(a,b)
            -- print(a.XiuWei,b.XiuWei)
            local ret=flag
            if a.XiuWei<b.XiuWei then
                ret = flag
            elseif a.XiuWei>b.XiuWei then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==3 then--xiulian
        table.sort( t, function(a,b)
            local ret=flag
            if a.XiuLian<b.XiuLian then
                ret = flag
            elseif a.XiuLian>b.XiuLian then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==4 then--equip score
        table.sort( t, function(a,b)
            local ret=flag
            if a.EquipScore<b.EquipScore then
                ret = flag
            elseif a.EquipScore>b.EquipScore then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==5 then--
        table.sort( t, function(a,b)
            local ret=flag
            if a.SubmitToGuildZiCai<b.SubmitToGuildZiCai then
                ret = flag
            elseif a.SubmitToGuildZiCai>b.SubmitToGuildZiCai then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==6 then--野外周评
        table.sort( t, function(a,b)
            local ret=flag
            if a.PublicMapScore<b.PublicMapScore then
                ret = flag
            elseif a.PublicMapScore>b.PublicMapScore then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==7 then--宣战周评
        table.sort( t, function(a,b)
            local ret=flag
            if a.MirrorWarScore<b.MirrorWarScore then
                ret = flag
            elseif a.MirrorWarScore>b.MirrorWarScore then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==8 then
        -- --do nothing
    -- elseif sortIndex==6 then--评价
    --     --do nothing
    -- elseif sortIndex==7 then--is in scene
        table.sort( t, function(a,b)
            local ret=flag
            if a.IsInHardCopy > b.IsInHardCopy then
                ret = -flag
            elseif a.IsInHardCopy < b.IsInHardCopy then
                ret = flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==9 then--default
        table.sort( t, function(a,b)
            local ret=flag
            if a.IsMonsterSiegeElite<b.IsMonsterSiegeElite then
                ret = flag
            elseif a.IsMonsterSiegeElite>b.IsMonsterSiegeElite then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    end
end


function CLuaCityWarMonsterSiegeSettingWnd:InitItem(transform,info,row)
    local clsSprite = transform:Find("ClassSprite"):GetComponent(typeof(UISprite))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local levelLabel = transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local xiuweiLabel = transform:Find("XiuweiLabel"):GetComponent(typeof(UILabel))
    local xiulianLabel = transform:Find("XiulianLabel"):GetComponent(typeof(UILabel))
    local equipScoreLabel = transform:Find("EquipScoreLabel"):GetComponent(typeof(UILabel))
    local tickSprite = transform:Find("TickSprite"):GetComponent(typeof(UISprite))
    local inSceneLabel = transform:Find("InGameLabel"):GetComponent(typeof(UILabel))

    local playerId=info.PlayerId

    nameLabel.text = info.Name
    clsSprite.spriteName = Profession.GetIconByNumber(info.Class)

    CUICommonDef.SetActive(clsSprite.gameObject, info.IsOnline > 0, true)

    levelLabel.text = info.XianFanStatus > 0 and SafeStringFormat3( "[c][ff7900]Lv.%d[-][/c]",info.Level ) or SafeStringFormat3( "Lv.%d",info.Level )

    xiuweiLabel.text = NumberComplexToString(NumberTruncate(info.XiuWei, 1), typeof(Double), "F1")
    --一位小数显示
    xiulianLabel.text = tostring(info.XiuLian)

    equipScoreLabel.text = tostring(info.EquipScore)

    local isElite = info.IsMonsterSiegeElite > 0
    self:SetItemElite(transform,row,isElite)

    if info.IsInHardCopy == 1 then
        inSceneLabel.text = LocalString.GetString("是")
        inSceneLabel.color = Color(0.066,0.17,0.3)
    else
        inSceneLabel.text = LocalString.GetString("否")
        inSceneLabel.color = Color.red
    end

    UIEventListener.Get(tickSprite.transform:GetChild(0).gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if tickSprite.enabled then
            --撤销
            Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "SetMonsterSiegeElite", tostring(playerId), "0")
        else
            Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "SetMonsterSiegeElite", tostring(playerId), "1")
        end
    end)

    local yeWaiLabel = transform:Find("YeWaiLabel"):GetComponent(typeof(UILabel))
    local xuanZhanLabel = transform:Find("XuanZhanLabel"):GetComponent(typeof(UILabel))
    local ziyuanLabel = transform:Find("ZiYuanLabel"):GetComponent(typeof(UILabel))
    -- local chengZhanLabel = transform:Find("ChengZhanLabel"):GetComponent(typeof(UILabel))
    yeWaiLabel.text = tostring(info.PublicMapScore)
    xuanZhanLabel.text = tostring(info.MirrorWarScore)
    -- chengZhanLabel.text = tostring(info.SubmitToGuildZiCai)
    ziyuanLabel.text = tostring(info.SubmitToGuildZiCai)
    -- CLuaGuildMgr.SetMonsterSiegeScoreLevel(chengZhanLabel,info.SubmitToGuildZiCai)
    -- PublicMapScore
    -- MirrorWarScore
    -- SubmitToGuildZiCai
end

function CLuaCityWarMonsterSiegeSettingWnd:SetItemElite(transform,row,isElite)
    local tickSprite = transform:Find("TickSprite"):GetComponent(typeof(UISprite))
    tickSprite.enabled = isElite
end

function CLuaCityWarMonsterSiegeSettingWnd:OnDestroy()
    CLuaGuildMgr.m_GuildMonsterSiegeInfo=nil
end

