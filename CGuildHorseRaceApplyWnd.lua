-- Auto Generated!!
local CGuildHorseRaceApplyWnd = import "L10.UI.CGuildHorseRaceApplyWnd"
local CGuildHorseRaceMgr = import "L10.Game.CGuildHorseRaceMgr"
local LocalString = import "LocalString"
CGuildHorseRaceApplyWnd.m_InitApplyButton_CS2LuaHook = function (this) 
    repeat
        local default = CGuildHorseRaceMgr.Inst.qualification
        if default == (CGuildHorseRaceMgr.EnumGuildHorseRaceApplyQualification.disable) then
            this.applyButton.Enabled = false
            this.applyBtnLabel.text = LocalString.GetString("申请出战")
            break
        elseif default == (CGuildHorseRaceMgr.EnumGuildHorseRaceApplyQualification.applyAvailable) then
            this.applyButton.Enabled = true
            this.applyBtnLabel.text = LocalString.GetString("申请出战")
            break
        elseif default == (CGuildHorseRaceMgr.EnumGuildHorseRaceApplyQualification.cancelAvailable) then
            this.applyButton.Enabled = true
            this.applyBtnLabel.text = LocalString.GetString("取消申请")
            break
        else
            break
        end
    until 1
end
CGuildHorseRaceApplyWnd.m_OnApplyButtonClick_CS2LuaHook = function (this, go) 
    if CGuildHorseRaceMgr.Inst.qualification == CGuildHorseRaceMgr.EnumGuildHorseRaceApplyQualification.applyAvailable then
        Gac2Gas.MPTYSRequestGuildOperation(0)
    elseif CGuildHorseRaceMgr.Inst.qualification == CGuildHorseRaceMgr.EnumGuildHorseRaceApplyQualification.cancelAvailable then
        Gac2Gas.MPTYSRequestGuildOperation(1)
    end
end
