local Object=import "System.Object"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UISprite = import "UISprite"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CUITexture = import "L10.UI.CUITexture"
local CPaintTextureDLN = import "L10.UI.CPaintTextureDLN"

LuaPaintPicWndType2 = class()
RegistChildComponent(LuaPaintPicWndType2,"closeBtn", GameObject)
RegistChildComponent(LuaPaintPicWndType2,"bgTexture", CUITexture)
RegistChildComponent(LuaPaintPicWndType2,"paintNode", GameObject)
RegistChildComponent(LuaPaintPicWndType2,"upTexture", CUITexture)

RegistClassMember(LuaPaintPicWndType2, "taskID")
RegistClassMember(LuaPaintPicWndType2, "taskData")

function LuaPaintPicWndType2:Close()
	CUIManager.CloseUI(CLuaUIResources.PaintPicWndType2)
end

function LuaPaintPicWndType2:Finish()
	--Gac2Gas.FinishTaskPainting(self.taskID)
	local empty = CreateFromClass(MakeGenericClass(List, Object))
	Gac2Gas.FinishClientTaskEvent(self.taskID,"PaintingType2",MsgPackImpl.pack(empty))
	RegisterTickWithDuration(function ()
		self:Close()
	end,2000,2000)
end

function LuaPaintPicWndType2:SetStateStart(state)
	if self.taskID then
		local count = ZhuJueJuQing_Painting.GetDataCount()
		for i=1,count do
			local data = ZhuJueJuQing_Painting.GetData(i)
			if data and data.TaskID == self.taskID then
				self.taskData = data
				break
			end
		end
	end

	if self.taskData then
		--self.bgTexture:LoadMaterial("UI/Texture/Transparent/Material/" .. self.taskData.BackgroundText .. ".mat",false)
		--self.upTexture:LoadTexture("UI/Texture/Transparent/" .. self.taskData.CoverText .. ".png",false)
		--self.bgTexture.gameObject:SetActive(false)
		self.bgTexture.gameObject:SetActive(true)
		local paintTexture = self.upTexture.transform.parent:GetComponent(typeof(CPaintTextureDLN))
		--self.upTexture.gameObject:SetActive(false)
		paintTexture:Awake()
--		RegisterTickWithDuration(function ()
--			self.bgTexture.gameObject:SetActive(true)
--			if paintTexture then
--				paintTexture:Awake()
--			end
--		end,400,400)
	end
end

function LuaPaintPicWndType2:SetStateEnd(stateIndex)
  self:Finish()
end

function LuaPaintPicWndType2:SetPercent(percent)
	if self.ProgressNodeTable[LuaShenbingMgr.DLNState] and percent[0] then
		self.ProgressNodeTable[LuaShenbingMgr.DLNState].transform:Find("bg"):GetComponent(typeof(UISprite)).fillAmount = percent[0]
	end
end

function LuaPaintPicWndType2:OnEnable()
	g_ScriptEvent:AddListener("UpdateDLNDraw", self, "SetStateEnd")
	--g_ScriptEvent:AddListener("UpdateDLNState", self, "SetPercent")
end

function LuaPaintPicWndType2:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateDLNDraw", self, "SetStateEnd")
	--g_ScriptEvent:RemoveListener("UpdateDLNState", self, "SetPercent")
end

function LuaPaintPicWndType2:Init()
	local onCloseClick = function(go)
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Paint_Not_Done"), DelegateFactory.Action(function ()
      self:Close()
    end), nil, nil, nil, false)
	end

	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	self.taskID = tonumber(LuaPaintPicMgr.ShowArgs)
	self:SetStateStart(1)
end

return LuaPaintPicWndType2
