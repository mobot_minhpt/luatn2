local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

if rawget(_G, "LuaCommonPassportMgr") then
    g_ScriptEvent:RemoveListener("GasDisconnect", LuaCommonPassportMgr, "OnGasDisconnect")
end

LuaCommonPassportMgr = {}
LuaCommonPassportMgr.passId = -1
LuaCommonPassportMgr.rewardDesignData = {}
LuaCommonPassportMgr.taskDesignData = {}
LuaCommonPassportMgr.levelGroupSize = 20
LuaCommonPassportMgr.levelGroupIndex = {0, 10000, 20000}
LuaCommonPassportMgr.openPassIdTbl = {}

LuaCommonPassportMgr.redDotInfo = {}

LuaCommonPassportMgr.m_LastPreviewType = nil --当前预览的类型EnumPreviewType

EnumUpdatePassDataReason = {
    eInit = 0,
    eTask = 1,
    eGetOneReward = 2,
    eGetAllReward = 3,
    eBuyLevel = 4,
    eUnlockVip = 5,
    eAddProgress = 6
}

g_ScriptEvent:AddListener("GasDisconnect", LuaCommonPassportMgr, "OnGasDisconnect")

function LuaCommonPassportMgr:OnGasDisconnect()
    self.redDotInfo = {}
    self.rewardDesignData = {}
    self.taskDesignData = {}
    self.openPassIdTbl = {}
end

-- 打开通行证主界面
function LuaCommonPassportMgr:ShowPassportWnd(passId)
    if self:CheckCanOpenWnd(passId) then
        self.passId = passId
        CUIManager.ShowUI(Pass_ClientSetting.GetData(passId).MainWndName)
    end
end

-- 打开解锁高级战令界面
function LuaCommonPassportMgr:OpenVIPUnlockWnd(passId)
    if self:CheckCanOpenWnd(passId) then
        self.passId = passId
        CUIManager.ShowUI(Pass_ClientSetting.GetData(passId).VIPUnlockWndName)
    end
end

-- 检测是否可以打开界面
function LuaCommonPassportMgr:CheckCanOpenWnd(passId)
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then return false end

    if mainPlayer.IsCrossServerPlayer then
        g_MessageMgr:ShowMessage("COMMON_PASSPORT_CANT_USE_IN_CROSS_SERVER")
        return false
    end

    if not self:IsPlayerLevelOk(passId) then
        g_MessageMgr:ShowMessage("COMMON_PASSPORT_LEVEL_NOT_REACHED", Pass_Setting.GetData(passId).ValidPlayerLevel)
        return false
    end
    return true
end

--#region gas2gac

function LuaCommonPassportMgr:UpdatePassDataWithId(id, data_U, reason)
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer then
        local data = CPassInfo()
        data:LoadFromString(data_U)
        self:ProcessReason(id, data, reason)
        CommonDefs.DictSet_LuaCall(mainPlayer.PlayProp.PassData, id, data)
        self:UpdateRedDot(id)
        g_ScriptEvent:BroadcastInLua("UpdatePassDataWithId", id, reason)
    end
end

function LuaCommonPassportMgr:SendOpenPassInfos(openPassIds, time)
    self.openPassIdTbl = {}
    for passId in string.gmatch(openPassIds, "(%d+)") do
        local id = tonumber(passId)
        table.insert(self.openPassIdTbl, id)
        if self:IsPlayerLevelOk(id) then self:UpdateRedDot(id) end
    end
    EventManager.Broadcast(EnumEventType.SyncDynamicActivitySimpleInfo)
end

--#endregion gas2gac


-- 是否达到了战令等级
function LuaCommonPassportMgr:IsPlayerLevelOk(passId)
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then return false end

    return mainPlayer.MaxLevel >= Pass_Setting.GetData(passId).ValidPlayerLevel
end


--#region 红点

-- 是否有奖励可以领取（通行证和任务奖励）
function LuaCommonPassportMgr:HasAward(passId)
    return self:HasPassAward(passId) or self:HasTaskAward(passId)
end

-- 是否有通行证奖励可以领取
function LuaCommonPassportMgr:HasPassAward(passId)
    local redDotInfo = self.redDotInfo[passId]
    if not redDotInfo then return false end
    return redDotInfo.hasPassAward
end

-- 是否有任务奖励可以领取
function LuaCommonPassportMgr:HasTaskAward(passId)
    local redDotInfo = self.redDotInfo[passId]
    if not redDotInfo then return false end
    return redDotInfo.hasTaskAward
end

-- 更新红点数据（对应奖励和任务两个Tab）
function LuaCommonPassportMgr:UpdateRedDot(passId)
    self.redDotInfo[passId] = {
        hasPassAward = self:GetHasPassAward(passId),
        hasTaskAward = self:GetHasTaskAward(passId)
    }
end

-- 更新任务红点数据（因为存在任务过期的情况）
function LuaCommonPassportMgr:UpdateTaskRedDot(passId)
    if self.redDotInfo[passId] then
        self.redDotInfo[passId].hasTaskAward = self:GetHasTaskAward(passId)
    end
end

-- 通行证奖励是否已领取
function LuaCommonPassportMgr:IsPassAwardReceived(passInfo, level, type)
    if not passInfo then return false end

    local group = math.floor((level - 1) / self.levelGroupSize) + 1 + self.levelGroupIndex[type]
    local offset = bit.lshift(1, (level - 1) % self.levelGroupSize)
    local result, awardValue = CommonDefs.DictTryGet_LuaCall(passInfo.AwardInfo, group)
    if not awardValue then return false end
    return bit.band(awardValue, offset) > 0
end

-- 通行证中存在奖励可领取
function LuaCommonPassportMgr:GetHasPassAward(passId)
    local passInfo = LuaCommonPassportMgr:GetPassInfo(passId)
    if not passInfo then return false end

    local rewardLevel = self:GetRewardLevel(passId)
    local isVIP1Open = self:GetIsVIP1Open(passId)
    local isVIP2Open = self:GetIsVIP2Open(passId)

    local rewardDesignData = self:GetRewardDesignData(passId)
    for _, data in ipairs(rewardDesignData) do
        if rewardLevel < data.level then
            break
        end

        if not self:IsPassAwardReceived(passInfo, data.level, 1) then return true end
        if isVIP1Open and not self:IsPassAwardReceived(passInfo, data.level, 2) then return true end
        if isVIP2Open and not self:IsPassAwardReceived(passInfo, data.level, 3) then return true end
    end
    return false
end

-- 任务中存在奖励可领取
function LuaCommonPassportMgr:GetHasTaskAward(passId)
    local passInfo = LuaCommonPassportMgr:GetPassInfo(passId)
    if not passInfo then return false end

    if self:GetAutoProgress(passId) then return false end
    local hasTaskAward = false
    CommonDefs.DictIterate(passInfo.TaskData, DelegateFactory.Action_object_object(function (taskId, taskInfo)
        local taskData = Pass_Task.GetData(taskId)
        local endTimeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(taskData.EndTime)
        local finishTimes = taskData.FinishTimes > 0 and taskData.FinishTimes or 1
        if taskData.Target == taskInfo.Progress and taskInfo.FinishTimes < finishTimes and CServerTimeMgr.Inst.timeStamp < endTimeStamp then
            hasTaskAward = true
        end
    end))
    return hasTaskAward
end

--#endregion 红点



--#region 触发一些表现效果

function LuaCommonPassportMgr:ProcessReason(passId, newPassInfo, reason)
    if reason == EnumUpdatePassDataReason.eGetAllReward then
        self:OpenGetRewardWnd(passId, newPassInfo)
    elseif reason == EnumUpdatePassDataReason.eBuyLevel then
        self:BuyLevelSuccess(passId, newPassInfo)
    elseif reason == EnumUpdatePassDataReason.eUnlockVip then
        self:UnlockVipSuccess(passId, newPassInfo)
    end
end

-- 打开“恭喜获得”界面
function LuaCommonPassportMgr:OpenGetRewardWnd(passId, newPassInfo)
    local passInfo = LuaCommonPassportMgr:GetPassInfo(passId)
    if not passInfo then return end

    local rewardDesignData = self:GetRewardDesignData(passId)
    local rewardLevel = self:GetRewardLevel(passId)
    local isVIP1Open = self:GetIsVIP1Open(passId)
    local isVIP2Open = self:GetIsVIP2Open(passId)
    local hasVIP1 = self:HasVIP1(passId)
    local hasVIP2 = self:HasVIP2(passId)

    -- 用Dict存储所有获得的奖励道具
    local reward1Dict = {}
    local reward2Dict = {}
    local addItemToDict = function(type, rewardId, isLocked)
        local itemRewards = self:ParseItemRewardsForOneType(rewardId, type)
        local dict = isLocked and reward2Dict or reward1Dict
        for _, data in ipairs(itemRewards) do
            local itemId = data.itemId
            if dict[itemId] then
                dict[itemId].count = dict[itemId].count + data.count
            else
                dict[itemId] = {count = data.count}
            end
        end
    end

    for _, data in ipairs(rewardDesignData) do
        if rewardLevel < data.level then
            break
        end

        if not self:IsPassAwardReceived(passInfo, data.level, 1) and self:IsPassAwardReceived(newPassInfo, data.level, 1) then
            addItemToDict(1, data.id, false)
        end

        if hasVIP1 then
            if not isVIP1Open then
                addItemToDict(2, data.id, true)
            elseif not self:IsPassAwardReceived(passInfo, data.level, 2) and self:IsPassAwardReceived(newPassInfo, data.level, 2) then
                addItemToDict(2, data.id, false)
            end
        end

        if hasVIP2 then
            if not isVIP2Open then
                addItemToDict(3, data.id, true)
            elseif not self:IsPassAwardReceived(passInfo, data.level, 3) and self:IsPassAwardReceived(newPassInfo, data.level, 3) then
                addItemToDict(3, data.id, false)
            end
        end
    end

    -- 把Dict转为List，处理堆叠和权重排序
    local convertDict2List = function(dict)
        local tbl = {}
        for itemId, data in pairs(dict) do
            table.insert(tbl, {ItemID = itemId, Count = data.count})
        end

        table.sort(tbl, function (a, b)
            local weightData1 = Pass_ItemWeight.GetData(a.ItemID)
            local weightData2 = Pass_ItemWeight.GetData(b.ItemID)

            local weight1 = weightData1 and weightData1.Weight or 0
            local weight2 = weightData2 and weightData2.Weight or 0

            return weight1 > weight2
        end)
        return tbl
    end

    local canUnlockVip1 = hasVIP1 and not isVIP1Open
    local canUnlockVip2 = hasVIP2 and not isVIP2Open
    local reward1List = convertDict2List(reward1Dict)
    local reward2List = convertDict2List(reward2Dict)
    local reward2Label = ""
    if #reward2List > 0 then
        local names = Pass_ClientSetting.GetData(passId).BattlePassNames
        local vip1Name = SafeStringFormat3("[FF6F6F]%s[-]", names[1])
        local vip2Name = SafeStringFormat3("[F5A2FF]%s[-]", names[2])
        local str = ""
        if canUnlockVip1 and canUnlockVip2 then
            str = SafeStringFormat3(LocalString.GetString("%s、%s"), vip1Name, vip2Name)
        elseif canUnlockVip1 then
            str = vip1Name
        elseif canUnlockVip2 then
            str = vip2Name
        end
        reward2Label = SafeStringFormat3(LocalString.GetString("解锁%s还可获得："), str)
    end

    local buttonList = {
        {spriteName = "blue", buttonLabel = LocalString.GetString("确定"), clickCB = function() CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd) end},
    }
    if canUnlockVip1 or canUnlockVip2 then
        table.insert(buttonList, {spriteName = "yellow", buttonLabel = LocalString.GetString("解锁高级奖励"), clickCB = function()
            self:OpenVIPUnlockWnd(passId)
            CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd)
        end})
    end
    LuaCommonGetRewardWnd.m_materialName = "gongxihuode"
    LuaCommonGetRewardWnd.m_Reward1List = reward1List
    LuaCommonGetRewardWnd.m_Reward2Label = reward2Label
    LuaCommonGetRewardWnd.m_Reward2List = reward2List
    LuaCommonGetRewardWnd.m_hint = ""
    LuaCommonGetRewardWnd.m_button = buttonList
    CUIManager.ShowUI(CLuaUIResources.CommonGetRewardWnd)
end

-- 购买等级成功
function LuaCommonPassportMgr:BuyLevelSuccess(passId, newPassInfo)
    local oldRewardLevel = self:GetRewardLevel(passId)
    local newRewardLevel = newPassInfo.Level
    g_ScriptEvent:BroadcastInLua("CommonPassortBuyLevelSuccess", passId, oldRewardLevel, newRewardLevel)
end

-- 解锁战令成功
function LuaCommonPassportMgr:UnlockVipSuccess(passId, newPassInfo)
    local oldIsVip1 = self:GetIsVIP1Open(passId)
    local oldIsVip2 = self:GetIsVIP2Open(passId)
    local newIsVip1 = newPassInfo.IsVip1 == 1
    local newIsVip2 = newPassInfo.IsVip2 == 1

    local vip1Unlock = not oldIsVip1 and newIsVip1
    local vip2Unlock = not oldIsVip2 and newIsVip2
    g_ScriptEvent:BroadcastInLua("CommonPassortUnlockVipSuccess", passId, vip1Unlock, vip2Unlock)
end

--#endregion 触发一些表现效果


-- 是否有第二档
function LuaCommonPassportMgr:HasVIP1(passId)
    return Pass_Setting.GetData(passId).Type >= 2
end

-- 是否有第三档
function LuaCommonPassportMgr:HasVIP2(passId)
    return Pass_Setting.GetData(passId).Type >= 3
end

function LuaCommonPassportMgr:GetPassInfo(passId)
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer then
        local result, passInfo = CommonDefs.DictTryGet_LuaCall(mainPlayer.PlayProp.PassData, passId)
        return passInfo
    end
end

-- 获取当前通行证等级
function LuaCommonPassportMgr:GetLevel(passId)
    local passInfo = self:GetPassInfo(passId)
    local rewardLevel = passInfo and passInfo.Level or 0
    local rewardDesignData = self:GetRewardDesignData(passId)
    if rewardLevel == 0 then
        if #rewardDesignData > 0 and rewardDesignData[1].progress == 0 then
            return rewardDesignData[1].level
        end
        return 0
    end

    local progress = passInfo and passInfo.Progress or 0
    local count = #rewardDesignData
    for i = 1, count do
        local data = rewardDesignData[i]
        if rewardLevel == data.level then
            local oneLevelProgress = i == count and data.progress or rewardDesignData[i + 1].progress
            return rewardLevel + math.floor((progress - data.totalProgress) / oneLevelProgress)
        end
    end
    return 0
end

-- 获取当前获得奖励的等级
function LuaCommonPassportMgr:GetRewardLevel(passId)
    local passInfo = self:GetPassInfo(passId)
    local rewardDesignData = self:GetRewardDesignData(passId)
    local level = passInfo and passInfo.Level or 0
    if level == 0 and #rewardDesignData > 0 and rewardDesignData[1].progress == 0 then
        level = rewardDesignData[1].level
    end
    return level
end

-- 获取当前进度
function LuaCommonPassportMgr:GetProgress(passId)
    local passInfo = self:GetPassInfo(passId)
    return passInfo and passInfo.Progress or 0
end

-- 获取VIP1是否开启
function LuaCommonPassportMgr:GetIsVIP1Open(passId)
    local passInfo = self:GetPassInfo(passId)
    return passInfo and passInfo.IsVip1 == 1 or false
end

-- 获取VIP2是否开启
function LuaCommonPassportMgr:GetIsVIP2Open(passId)
    local passInfo = self:GetPassInfo(passId)
    return passInfo and passInfo.IsVip2 == 1 or false
end

-- 解析Pass_Award策划表
function LuaCommonPassportMgr:ParseRewardDesginData(passId)
    if self.rewardDesignData[passId] then
        return
    end

    local tbl = {}
    Pass_Reward.Foreach(function(id, data)
        if data.PassID == passId then
            table.insert(tbl, {id = id, level = data.Level, progress = data.Progress})
        end
    end)
    -- 按等级从小到大排序
    table.sort(tbl, function (a, b)
        return a.level < b.level
    end)

    -- totalProgress为从0级到当前等级的进度值
    for i = 1, #tbl do
        local lastTotalProgress = i == 1 and 0 or tbl[i - 1].totalProgress
        local lastLevel = i == 1 and 0 or tbl[i - 1].level
        tbl[i].totalProgress = lastTotalProgress + tbl[i].progress * (tbl[i].level - lastLevel)
    end
    self.rewardDesignData[passId] = tbl
end

-- 获取策划表数据
function LuaCommonPassportMgr:GetRewardDesignData(passId)
    self:ParseRewardDesginData(passId)
    return self.rewardDesignData[passId]
end

-- 获取最大通行证等级
function LuaCommonPassportMgr:GetMaxPassLevel(passId)
    local rewardDesignData = self:GetRewardDesignData(passId)
    return rewardDesignData[#rewardDesignData].level
end

-- 获取升级需要的进度信息
function LuaCommonPassportMgr:GetUpgradeProgressInfo(passId)
    local rewardDesignData = self:GetRewardDesignData(passId)
    local rewardLevel = self:GetRewardLevel(passId)
    local progress = self:GetProgress(passId)

    local count = #rewardDesignData
    for i = 1, count do
        if rewardLevel < rewardDesignData[i].level then
            local lastTotalProgress = i == 1 and 0 or rewardDesignData[i - 1].totalProgress
            local hasProgress = (progress - lastTotalProgress) % rewardDesignData[i].progress
            local totalProgress = rewardDesignData[i].progress
            local needProgress = totalProgress - hasProgress
            return {hasProgress = hasProgress, totalProgress = totalProgress, needProgress = needProgress}
        end
    end
    return {hasProgress = 0, totalProgress = rewardDesignData[#rewardDesignData].progress, needProgress = 0}
end

-- 获取一个灵玉增加的经验值
function LuaCommonPassportMgr:GetOneJadeAddProgress(passId)
    local jadeProp = Pass_Addition.GetData(passId).JadeProp
    local jade, progress = string.match(jadeProp, "(%d+):(%d+)")
    return tonumber(progress) / tonumber(jade)
end

-- 解析高级战令进度奖励
function LuaCommonPassportMgr:ParseProgressRewards(passId)
    local progressRewards = Pass_Addition.GetData(passId).ProgressRewards
    local tbl = {}
    for progress, rate, mailId in string.gmatch(progressRewards, "(%d+),([^,]+),(%d+)") do
        table.insert(tbl, {progress = tonumber(progress), rate = tonumber(rate), mailId = tonumber(mailId)})
    end
    return tbl
end

-- 解析新服加成数值（新服加成类型为2）
function LuaCommonPassportMgr:ParseOpenServerTime2AddRate(passId)
    local openServerTime2AddRate = Pass_Addition.GetData(passId).OpenServerTime2AddRate
    local tbl = {}
    for serverStr, rate in string.gmatch(openServerTime2AddRate, "([%d,]+) ([%d.]+)") do
        for serverId in string.gmatch(serverStr, "(%d+)") do
            tbl[tonumber(serverId)] = tonumber(rate)
        end
    end
    return tbl
end

-- 获取任务经验加成信息
function LuaCommonPassportMgr:GetAdditionInfo(passId)
    local isVIP1Open = self:GetIsVIP1Open(passId)
    local isVIP2Open = self:GetIsVIP2Open(passId)

    local list = {}
    local finalRate = 0
    local progressAwardsTbl = self:ParseProgressRewards(passId)
    local vipAddRate = 0
    if isVIP1Open and progressAwardsTbl[1].rate > 0 then
        vipAddRate = vipAddRate + progressAwardsTbl[1].rate
    end
    if isVIP2Open and progressAwardsTbl[2].rate > 0 then
        vipAddRate = vipAddRate + progressAwardsTbl[2].rate
    end
    if vipAddRate > 0 then
        table.insert(list, {reason = LocalString.GetString("高级战令加成"), rate = vipAddRate})
        finalRate = finalRate + vipAddRate
    end

    if Pass_Addition.GetData(passId).NewServerAddType == 2 then
        local newServerAddRateTbl = LuaCommonPassportMgr:ParseOpenServerTime2AddRate(passId)
        local mainPlayer = CClientMainPlayer.Inst
        local serverId = mainPlayer and mainPlayer:GetMyServerId() or 0
        local newServerAddRate = newServerAddRateTbl[serverId] or 0
        if newServerAddRate > 0 then
            finalRate = finalRate + newServerAddRate
            table.insert(list, {reason = LocalString.GetString("新服加成"), rate = newServerAddRate})
        end
    end

    return {finalRate = finalRate, list = list}
end

-- 解析道具奖励
function LuaCommonPassportMgr:ParseItemRewards(rewardId)
    local rewardData = Pass_Reward.GetData(rewardId)
    local tbl = {}
    local itemRewardsTbl = {rewardData.ItemRewards1, rewardData.ItemRewards2, rewardData.ItemRewards3}
    for i, itemRewards in ipairs(itemRewardsTbl) do
        if not System.String.IsNullOrEmpty(itemRewards) then
            tbl[i] = {}
            for itemId, count, isBind in string.gmatch(itemRewards, "(%d+),(%d+),(%d+)") do
                table.insert(tbl[i], {itemId = tonumber(itemId), count = tonumber(count), isBind = (tonumber(isBind) == 1)})
            end
        end
    end
    return tbl
end

-- 解析一种类型的道具奖励
function LuaCommonPassportMgr:ParseItemRewardsForOneType(rewardId, type)
    local rewardData = Pass_Reward.GetData(rewardId)
    local tbl = {}
    local itemRewardsTbl = {rewardData.ItemRewards1, rewardData.ItemRewards2, rewardData.ItemRewards3}
    local itemRewards = itemRewardsTbl[type]
    if not System.String.IsNullOrEmpty(itemRewards) then
        for itemId, count, isBind in string.gmatch(itemRewards, "(%d+),(%d+),(%d+)") do
            table.insert(tbl, {itemId = tonumber(itemId), count = tonumber(count), isBind = (tonumber(isBind) == 1)})
        end
    end
    return tbl
end


--#region 任务

function LuaCommonPassportMgr:GetTaskInfo(passInfo, taskId)
    if passInfo then
        local result, taskInfo = CommonDefs.DictTryGet_LuaCall(passInfo.TaskData, taskId)
        return taskInfo
    end
end

-- 获取任务是不是自动领取
function LuaCommonPassportMgr:GetAutoProgress(passId)
    return Pass_Setting.GetData(passId).AutoProgress >= 1
end

-- 获取某个任务是否是可领取的
function LuaCommonPassportMgr:IsTaskReward(passInfo, taskId)
    local taskInfo = self:GetTaskInfo(passInfo, taskId)
    if taskInfo then
        local taskDesignData = Pass_Task.GetData(taskId)
        local totalFinishTimes = taskDesignData.FinishTimes > 0 and taskDesignData.FinishTimes or 1
        return taskInfo.FinishTimes < totalFinishTimes and taskInfo.Progress == taskDesignData.Target
    end
    return false
end

-- 获取当前时刻开放的所有任务数据
function LuaCommonPassportMgr:GetOpenTaskData(passId)
    local tbl = {}
    local passInfo = self:GetPassInfo(passId)
    local autoProgress = self:GetAutoProgress(passId)

    Pass_Task.Foreach(function(id, data)
        if data.PassID == passId then
            local serverTimeMgr = CServerTimeMgr.Inst
            local beginTimeStamp = serverTimeMgr:GetTimeStampByStr(data.BeginTime)
            local endTimeStamp = serverTimeMgr:GetTimeStampByStr(data.EndTime)
            local nowTimeStamp = serverTimeMgr.timeStamp

            if nowTimeStamp >= beginTimeStamp and nowTimeStamp < endTimeStamp then
                local category = data.Category
                if not tbl[category] then
                    tbl[category] = {}
                end

                local taskInfo = self:GetTaskInfo(passInfo, id)
                local taskProgress = taskInfo and taskInfo.Progress or 0
                local taskFinishTimes = taskInfo and taskInfo.FinishTimes or 0
                local totalFinishTimes = data.FinishTimes > 0 and data.FinishTimes or 1
                local target = data.Target

                local isFinished = taskFinishTimes >= totalFinishTimes
                local isReward = not autoProgress and taskFinishTimes < totalFinishTimes and taskProgress == target
                table.insert(tbl[category], {id = id, beginTimeStamp = beginTimeStamp, endTimeStamp = endTimeStamp, 
                    isFinished = isFinished, isReward = isReward, progress = taskProgress, finishTimes = taskFinishTimes})
            end
        end
    end)

    -- 排序，可领取的>进行中的>已完成的
    for _, data in pairs(tbl) do
        table.sort(data, function (a, b)
            if a.isReward ~= b.isReward then
                return a.isReward
            end

            if a.isFinished ~= b.isFinished then
                return b.isFinished
            end

            return a.id < b.id
        end)
    end
    return tbl
end

--#endregion 任务

-- 计算剩余时间文本
function LuaCommonPassportMgr:GetLeftTimeText(leftSeconds)
    if leftSeconds <= 0 then
        return LocalString.GetString("0分钟")
    end

    local timeStr
    if leftSeconds > 86400 then
        local day = math.floor(leftSeconds / 86400)
        local hour = math.floor((leftSeconds % 86400) / 3600)
        if hour > 0 then
            timeStr = SafeStringFormat3(LocalString.GetString("%d天%d小时"), day, hour)
        else
            timeStr = SafeStringFormat3(LocalString.GetString("%d天"), day)
        end
    elseif leftSeconds > 3600 then
        local hour = math.floor(leftSeconds / 3600)
        local minute = math.floor((leftSeconds % 3600) / 60)
        if minute > 0 then
            timeStr = SafeStringFormat3(LocalString.GetString("%d小时%d分钟"), hour, minute)
        else
            timeStr = SafeStringFormat3(LocalString.GetString("%d小时"), hour)
        end
    else
        local minute = math.floor(leftSeconds / 60)
        timeStr = SafeStringFormat3(LocalString.GetString("%d分钟"), math.max(minute, 1))
    end
    return timeStr
end
