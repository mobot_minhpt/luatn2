
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaWuYi2021MingXingResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuYi2021MingXingResultWnd, "ShareBtn", "ShareBtn", CButton)
RegistChildComponent(LuaWuYi2021MingXingResultWnd, "Reward", "Reward", GameObject)
RegistChildComponent(LuaWuYi2021MingXingResultWnd, "NoReward", "NoReward", GameObject)
RegistChildComponent(LuaWuYi2021MingXingResultWnd, "RewardLabel", "RewardLabel", UILabel)
RegistChildComponent(LuaWuYi2021MingXingResultWnd, "Item", "Item", GameObject)
RegistChildComponent(LuaWuYi2021MingXingResultWnd, "NumLabel", "NumLabel", UILabel)

--@endregion RegistChildComponent end

function LuaWuYi2021MingXingResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)


    --@endregion EventBind end
end

function LuaWuYi2021MingXingResultWnd:Init()
	if LuaWuYi2021Mgr.m_HasReward then
		self.Reward:SetActive(true)
		self.NoReward:SetActive(false)
		local itemId = WuYi2021_Setting.GetData().JXMXRewardItemId[0]
		local count = WuYi2021_Setting.GetData().JXMXRewardItemId[1]
		self.NumLabel.text = tostring(count)

		local data = Item_Item.GetData(itemId)
		UIEventListener.Get(self.Item).onClick = DelegateFactory.VoidDelegate(function()
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end)

		if data then
			self.RewardLabel.text = SafeStringFormat3(LocalString.GetString("获得%s"), data.Name)
		end
	else
		self.Reward:SetActive(false)
		self.NoReward:SetActive(true)
	end

	if CommonDefs.IS_VN_CLIENT then
		self.ShareBtn.gameObject:SetActive(false)
	end
end

--@region UIEvent

function LuaWuYi2021MingXingResultWnd:OnShareBtnClick()
	CUICommonDef.CaptureScreenAndShare()
end
--@endregion UIEvent

