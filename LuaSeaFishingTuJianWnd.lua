local UIPanel = import "UIPanel"

local QnTableView = import "L10.UI.QnTableView"
local QnCheckBox = import "L10.UI.QnCheckBox"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"

local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CUIFx = import "L10.UI.CUIFx"
local TweenAlpha = import "TweenAlpha"

LuaSeaFishingTuJianWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSeaFishingTuJianWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaSeaFishingTuJianWnd, "FengMian", "FengMian", GameObject)
RegistChildComponent(LuaSeaFishingTuJianWnd, "TuJianView", "TuJianView", GameObject)
RegistChildComponent(LuaSeaFishingTuJianWnd, "OnGuanSgangYuCheckBox", "OnGuanSgangYuCheckBox", QnCheckBox)
RegistChildComponent(LuaSeaFishingTuJianWnd, "UnLookedLabel", "UnLookedLabel", UILabel)
RegistChildComponent(LuaSeaFishingTuJianWnd, "InfoView", "InfoView", UIPanel)
RegistChildComponent(LuaSeaFishingTuJianWnd, "SceneTexture", "SceneTexture", CUITexture)
RegistChildComponent(LuaSeaFishingTuJianWnd, "FishTexture", "FishTexture", CUITexture)
RegistChildComponent(LuaSeaFishingTuJianWnd, "RewardBtn", "RewardBtn", GameObject)
RegistChildComponent(LuaSeaFishingTuJianWnd, "FishNameLabel", "FishNameLabel", UILabel)
RegistChildComponent(LuaSeaFishingTuJianWnd, "DescribeLabel", "DescribeLabel", UILabel)
RegistChildComponent(LuaSeaFishingTuJianWnd, "CatchCouneLabel", "CatchCouneLabel", UILabel)
RegistChildComponent(LuaSeaFishingTuJianWnd, "NeedSkillLabel", "NeedSkillLabel", UILabel)
RegistChildComponent(LuaSeaFishingTuJianWnd, "WeightLabel", "WeightLabel", UILabel)
RegistChildComponent(LuaSeaFishingTuJianWnd, "RewardBtnFx", "RewardBtnFx", CUIFx)
RegistChildComponent(LuaSeaFishingTuJianWnd, "PaperFx", "PaperFx", CUIFx)
RegistChildComponent(LuaSeaFishingTuJianWnd, "PaiZiFx", "PaiZiFx", CUIFx)
RegistChildComponent(LuaSeaFishingTuJianWnd, "GuangFx", "GuangFx", CUIFx)
RegistChildComponent(LuaSeaFishingTuJianWnd, "HaiDiaoJiLu", "HaiDiaoJiLu", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaSeaFishingTuJianWnd, "m_FishIdxToItemId")
RegistClassMember(LuaSeaFishingTuJianWnd, "m_FishIdxCount")
RegistClassMember(LuaSeaFishingTuJianWnd, "m_GuanShangFishIdxCount")
RegistClassMember(LuaSeaFishingTuJianWnd, "m_FishIdxGetTag")
RegistClassMember(LuaSeaFishingTuJianWnd, "m_FishIdxAwardTag")
RegistClassMember(LuaSeaFishingTuJianWnd, "m_TuJian")
RegistClassMember(LuaSeaFishingTuJianWnd, "m_Awarded")
RegistClassMember(LuaSeaFishingTuJianWnd, "m_GetFishIdxCount")
RegistClassMember(LuaSeaFishingTuJianWnd, "m_GetGuanShangFishIdxCount")
RegistClassMember(LuaSeaFishingTuJianWnd, "m_SelectedIdx")
RegistClassMember(LuaSeaFishingTuJianWnd, "m_IsOnlyGuanShangFish")
RegistClassMember(LuaSeaFishingTuJianWnd, "m_FishIdx2CatchCount")
RegistClassMember(LuaSeaFishingTuJianWnd, "m_FishIdx2Weight")
RegistClassMember(LuaSeaFishingTuJianWnd, "m_FishIdx2TujianCount")
RegistClassMember(LuaSeaFishingTuJianWnd, "m_ChangePaperTick")
RegistClassMember(LuaSeaFishingTuJianWnd, "m_FxDepth")
RegistClassMember(LuaSeaFishingTuJianWnd, "m_InfoViewTweenAlpha")
RegistClassMember(LuaSeaFishingTuJianWnd, "PaperFxTexture")

function LuaSeaFishingTuJianWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.RewardBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRewardBtnClick()
	end)

    --@endregion EventBind end
	self.m_FishIdxToItemId = {}
	self.m_FishIdxGetTag = {}
	self.m_FishIdxCount = 0
	self.m_GuanShangFishIdxCount = 0
	self.m_GetFishIdxCount = 0
	self.m_GetGuanShangFishIdxCount = 0
	self.m_FishIdxAwardTag = {}
	self.m_FishIdx2CatchCount = {}
	self.m_FishIdx2Weight = {}
	self.m_FishIdx2TujianCount = {}

	local fishbasket = CClientMainPlayer.Inst.ItemProp.FishBasket
	if not fishbasket then return end
	self.m_TuJian = fishbasket.TuJian
	self.m_Awarded = fishbasket.Awarded
	self.PaperFxTexture = self.PaperFx.transform:GetComponent(typeof(UITexture))
	self.m_FxDepth = self.PaperFxTexture.depth
	UIEventListener.Get(self.HaiDiaoJiLu).onClick = DelegateFactory.VoidDelegate(function (go)
	    LuaSeaFishingMgr.OpenServerRecordWnd()
	end)
end

function LuaSeaFishingTuJianWnd:OnEnable()
	g_ScriptEvent:AddListener("MarkFishAwarded", self, "OnMarkFishAwarded")
end
function LuaSeaFishingTuJianWnd:OnDisable()
	g_ScriptEvent:RemoveListener("MarkFishAwarded", self, "OnMarkFishAwarded")
end

function LuaSeaFishingTuJianWnd:Init()
	self:InitFishIdx()
	self:InitFishIdxCount()
	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self.m_FishIdxCount
        end,
        function(item,row)
            self:InitItem(item,row)
        end)
    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
		self:ChangePaper(row + 1)
    end)
    self.TableView:ReloadData(false,false)
	self.TableView:SetSelectRow(0,true)
	self.m_SelectedIdx = 1

	self.OnGuanSgangYuCheckBox.Selected = false
	self.m_IsOnlyGuanShangFish = false
    self.OnGuanSgangYuCheckBox.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self.m_IsOnlyGuanShangFish = self.OnGuanSgangYuCheckBox.Selected
		self.TableView:ReloadData(true,true)
		if self.m_IsOnlyGuanShangFish then
			self.UnLookedLabel.text = SafeStringFormat3(LocalString.GetString("已解锁 %d/%d"),self.m_GetGuanShangFishIdxCount,self.m_GuanShangFishIdxCount)
		else
			self.UnLookedLabel.text = SafeStringFormat3(LocalString.GetString("已解锁 %d/%d"),self.m_GetFishIdxCount,self.m_FishIdxCount)
		end
    end)

	self.GuangFx:LoadFx("fx/ui/prefab/UI_haidiaotujian_guangshu.prefab")
	self.PaiZiFx:LoadFx("fx/ui/prefab/UI_haidiaotujian_paizi.prefab")

	self.m_InfoViewTweenAlpha = self.InfoView.transform:GetComponent(typeof(TweenAlpha))
end

function LuaSeaFishingTuJianWnd:ChangePaper(row)
	local idx = row
	if not self.m_SelectedIdx or self.m_SelectedIdx == idx  then
		self.m_SelectedIdx = idx
		self:UpdateDetailView(idx)
		return
	end

	if self.m_ChangePaperTick then
		UnRegisterTick(self.m_ChangePaperTick)
		self.m_ChangePaperTick = nil
	end

	--往上翻(下一页)
	self.PaperFxTexture.depth = self.m_FxDepth
	self.RewardBtnFx:DestroyFx()
	if self.m_SelectedIdx < idx then
		self.m_SelectedIdx = idx
		self.m_InfoViewTweenAlpha.from = 1
		self.m_InfoViewTweenAlpha.to = 0
		self.m_InfoViewTweenAlpha:PlayForward()
		self.m_InfoViewTweenAlpha:SetOnFinished(DelegateFactory.Callback(function ()
			self.PaperFx:DestroyFx()
			self.PaperFx:LoadFx("fx/ui/prefab/UI_haidiaotujian_zhizhangshang.prefab")
			self.m_ChangePaperTick = RegisterTickOnce(function()
				self:UpdateDetailView(self.m_SelectedIdx)
				self.InfoView.alpha = 1
			end, 100)
		end))

	--往下翻(前一页)
	elseif self.m_SelectedIdx > idx then
		self.m_SelectedIdx = idx
		self.PaperFx:DestroyFx()
		self.PaperFx:LoadFx("fx/ui/prefab/UI_haidiaotujian_zhizhangxia.prefab")

		self.m_ChangePaperTick = RegisterTickOnce(function()
			self:UpdateDetailView(self.m_SelectedIdx)
			local tex = self.PaperFx.transform:GetComponent(typeof(UITexture))
			self.PaperFxTexture.depth = 2
		end, 700)
	end
end
function LuaSeaFishingTuJianWnd:InitFishIdx()
	HouseFish_AllFishes.Foreach(function (itemId, data)
        local fishIdx = data.FishIdx
		if not self.m_FishIdxToItemId[fishIdx] then
			self.m_FishIdxToItemId[fishIdx] = {}
		end
		table.insert(self.m_FishIdxToItemId[fishIdx],itemId)
		if fishIdx > self.m_FishIdxCount then
			self.m_FishIdxCount = fishIdx
		end
		self.m_FishIdxGetTag[fishIdx] = self.m_TuJian:GetBit(fishIdx)
		self.m_FishIdxAwardTag[fishIdx] = self.m_Awarded:GetBit(fishIdx)
    end)
	for idx=1,self.m_FishIdxCount,1 do
		local isGet = self.m_TuJian:GetBit(idx)
		if isGet then
			self.m_GetFishIdxCount = self.m_GetFishIdxCount + 1
		end
	end
	self.UnLookedLabel.text = SafeStringFormat3(LocalString.GetString("已解锁 %d/%d"),self.m_GetFishIdxCount,self.m_FishIdxCount)

	local count = 0
	local getCount = 0
	for idx,tbl in pairs(self.m_FishIdxToItemId) do
		local itemId = tbl[1]
		if HouseFish_AllFishes.GetData(itemId).IsGuanShang == 1 then
			count = count + 1
			local isGet = self.m_TuJian:GetBit(idx)
			if isGet then
				getCount = getCount + 1
			end
		end
	end
	self.m_GuanShangFishIdxCount = count
	self.m_GetGuanShangFishIdxCount = getCount
end

function LuaSeaFishingTuJianWnd:InitFishIdxCount()
	--m_FishIdx2CatchCount
	local fishbasket = CClientMainPlayer.Inst.ItemProp.FishBasket
	if not fishbasket then return end
	local fishs = fishbasket.Fishs
	local count = fishbasket.Count
	self.m_FishIdx2Weight = fishbasket.FishWeight
	self.m_FishIdx2TujianCount = fishbasket.TuJianCount
	
	local id2count = {}
	for i=0,fishs.Length-1,1 do
		local itemId = fishs[i]
		if itemId ~= 0 then
			id2count[itemId] = count[i]
		end
	end

	for idx,ids in pairs(self.m_FishIdxToItemId) do
		local count = 0
		for i,id in ipairs(ids) do
			local add = id2count[id] and id2count[id] or 0
			count = count + add
		end
		self.m_FishIdx2CatchCount[idx] = count
	end
end

function LuaSeaFishingTuJianWnd:UpdateDetailView(idx)
	local ids = self.m_FishIdxToItemId[idx]
	if ids and ids[1] then
		local itemId = ids[1]
		local data = Item_Item.GetData(itemId)
		local fishData = HouseFish_AllFishes.GetData(itemId)
		if data and fishData then			
			local path = data.Icon
			self.FishTexture:LoadMaterial(path)
			self.FishNameLabel.text = data.Name
			local desc = data.Description
			local lcPos = string.find(desc,"#r")
			if lcPos then
				desc = string.sub(desc,1,lcPos-1)
			end
			self.DescribeLabel.text = CChatLinkMgr.TranslateToNGUIText(desc,false)
			self.NeedSkillLabel.text = SafeStringFormat3(LocalString.GetString("海钓技能%d级以上"),fishData.MinSkillLv)
		end
		local isGet = self.m_FishIdxGetTag[idx]
		local isAwarded = self.m_FishIdxAwardTag[idx]

		local pos = self.SceneTexture.transform.localPosition
		if isGet then
			pos.z = 0
		else
			pos.z = -1
		end
		self.FishTexture.gameObject:SetActive(isGet)
		self.SceneTexture.transform.localPosition = pos
		self.RewardBtn:SetActive(isGet and not isAwarded)
		self.RewardBtnFx:DestroyFx()
		if isGet and not isAwarded then
			self.RewardBtnFx:LoadFx("fx/ui/prefab/UI_fx_yellow.prefab")
		end
		-- self.CatchCouneLabel.text = self.m_FishIdx2CatchCount[idx] and self.m_FishIdx2CatchCount[idx] or 0
		local weight = self.m_FishIdx2Weight[idx] and self.m_FishIdx2Weight[idx] or 0
		self.WeightLabel.text = SafeStringFormat3(LocalString.GetString("%.2f两"),weight)
		local count = self.m_FishIdx2TujianCount[idx] and self.m_FishIdx2TujianCount[idx] or 0
		self.CatchCouneLabel.text = count
	end

end


function LuaSeaFishingTuJianWnd:InitItem(item,row)
	local iconTexture = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local disableSprite = item.transform:Find("DisableSprite"):GetComponent(typeof(UISprite))
    local qualitySprite = item.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
    local bindSprite = item.transform:Find("BindSprite"):GetComponent(typeof(UISprite))
    local amountLabel = item.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
    local textLabel = item.transform:Find("TextLabel"):GetComponent(typeof(UILabel))
    local exSprite = item.transform:Find("ExSprite"):GetComponent(typeof(UISprite))
	exSprite.enabled = false
	textLabel.enabled = false
	amountLabel.enabled = false
	bindSprite.enabled = false
	qualitySprite.enabled = false
	disableSprite.enabled = false

	local idx = row + 1
	local ids = self.m_FishIdxToItemId[idx]
	if ids and ids[1] then
		local id = ids[1]
		local data = Item_Item.GetData(id)
		if data then
			iconTexture:LoadMaterial(data.Icon)
		end
		local fishdata =  HouseFish_AllFishes.GetData(id)
		if self.m_IsOnlyGuanShangFish then
			item.gameObject:SetActive(fishdata.IsGuanShang == 1)
		else
			item.gameObject:SetActive(true)
		end
	end

	-- local isGet = self.m_TuJian:GetBit(idx)
	-- self.m_FishIdxGetTag[idx] = isGet
	local isGet = self.m_FishIdxGetTag[idx]
	
	local pos = iconTexture.transform.localPosition
	if not isGet then
		pos.z = -1
	else
		pos.z = 0
	end 
	iconTexture.transform.localPosition = pos

	-- local isAwarded = self.m_Awarded:GetBit(idx)
	-- self.m_FishIdxAwardTag[idx] = isAwarded
end

function LuaSeaFishingTuJianWnd:OnDestroy()
	if self.m_ChangePaperTick then
		UnRegisterTick(self.m_ChangePaperTick)
		self.m_ChangePaperTick = nil
	end
end

function LuaSeaFishingTuJianWnd:OnMarkFishAwarded(fishIdx)
	local fishbasket = CClientMainPlayer.Inst.ItemProp.FishBasket
	if not fishbasket then return end
	fishbasket.Awarded:SetBit(fishIdx,true)

	self.m_FishIdxAwardTag[fishIdx] = true
	self.TableView:ReloadData(false,false)
	self:UpdateDetailView(fishIdx)
end

--@region UIEvent

function LuaSeaFishingTuJianWnd:OnRewardBtnClick()
	Gac2Gas.RequestApplyNewFishAward(self.m_SelectedIdx)
end


--@endregion UIEvent

