local Renderer = import "UnityEngine.Renderer"
local Animation = import "UnityEngine.Animation"
local CButton = import "L10.UI.CButton"
local BoxCollider = import "UnityEngine.BoxCollider"
local UIRedDotClick = import "L10.UI.UIRedDotClick"
local UIRedDotAlert = import "L10.UI.UIRedDotAlert"

LuaShuJia2023MainWnd = class()

RegistClassMember(LuaShuJia2023MainWnd, "Anim")
RegistClassMember(LuaShuJia2023MainWnd, "HaiZhanGuide")
RegistClassMember(LuaShuJia2023MainWnd, "OceanFight")
RegistClassMember(LuaShuJia2023MainWnd, "TreasureSea")
RegistClassMember(LuaShuJia2023MainWnd, "FireTree")
RegistClassMember(LuaShuJia2023MainWnd, "SignUpWnd")
RegistClassMember(LuaShuJia2023MainWnd, "Passport")

RegistClassMember(LuaShuJia2023MainWnd, "SignUpCloseBtn")
RegistClassMember(LuaShuJia2023MainWnd, "TitleVfxRoot")
RegistClassMember(LuaShuJia2023MainWnd, "SignUpWndShui")

RegistClassMember(LuaShuJia2023MainWnd, "m_CurSignUpType")
RegistClassMember(LuaShuJia2023MainWnd, "passId")

RegistClassMember(LuaShuJia2023MainWnd, "m_XHTXBtn")
RegistClassMember(LuaShuJia2023MainWnd, "m_XHTXRedDot")
RegistClassMember(LuaShuJia2023MainWnd, "m_RewardInfos")

function LuaShuJia2023MainWnd:Awake()
    local rewardDatas = g_LuaUtil:StrSplit(ShuJia2023_Setting.GetData("XHTXRewardId").Value, ";")
    self.m_RewardInfos = {}
    for i = 1, #rewardDatas do
        local rewardStr = rewardDatas[i]
        if rewardStr ~= "" then
            local rewardData = g_LuaUtil:StrSplit(rewardStr, ",")
            local pointVal = tonumber(rewardData[1])
            table.insert(self.m_RewardInfos, {pointVal = pointVal})
        end
    end
    self.m_XHTXBtn = self.transform:Find("GameObject/XianHai"):GetComponent(typeof(CButton))
    self.m_XHTXRedDot = self.transform:Find("GameObject/XianHai/Alert").gameObject

    self.passId = tonumber(ShuJia2023_Setting.GetData("ShuJiaPassId").Value)

    --self:LiftVfx(self.transform:Find("GameObject/BgTexture/vfx_shujia2023mainwnd_shui/CUIFx (1)/shuihua/Particle006 (1)"))
    --self:LiftVfx(self.transform:Find("GameObject/BgTexture/CUIFx"))
    --self:LiftVfx(self.transform:Find("ShuJiaHaiZhanSignUpWnd/CUIFx/shuidi02 (1)"))

    self.Anim = self.transform:GetComponent(typeof(Animation))
    self.HaiZhanGuide = self.transform:Find("GameObject/HaiZhanGuide")
    self.OceanFight = self.transform:Find("GameObject/OceanFight")
    self.TreasureSea = self.transform:Find("GameObject/TreasureSea")
    self.FireTree = self.transform:Find("GameObject/FireTree")
    self.Passport = self.transform:Find("GameObject/Passport")
    self.SignUpWnd = self.transform:Find("ShuJiaHaiZhanSignUpWnd"):GetComponent(typeof(CCommonLuaScript))
    self.TitleVfxRoot = self.transform:Find("ShuJiaHaiZhanSignUpWnd/WndBg/Background/titlezong")
    self.SignUpWndShui = self.transform:Find("ShuJiaHaiZhanSignUpWnd/shui").gameObject
    self.SignUpCloseBtn = self.transform:Find("ShuJiaHaiZhanSignUpWnd/WndBg/CloseButton").gameObject

    UIEventListener.Get(self.m_XHTXBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnXHTXBtnClick()
    end)

    UIEventListener.Get(self.HaiZhanGuide.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnHaiZhanGuideClick()
    end)

    UIEventListener.Get(self.OceanFight.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnOceanFightClick()
    end)

    UIEventListener.Get(self.TreasureSea.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnTreasureSeaClick()
    end)

    UIEventListener.Get(self.FireTree.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnFireTreeClick()
    end)

    UIEventListener.Get(self.SignUpCloseBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSignUpCloseBtnClick()
    end)

    self.m_CurSignUpType = 0
end

function LuaShuJia2023MainWnd:InitHaiZhanGuide()
    self.HaiZhanGuide:Find("Hint"):GetComponent(typeof(UILabel)).text = ShuJia2023_Setting.GetData("HaiZhanGuideDesc").Value
    self.HaiZhanGuide:Find("Reward"):GetComponent(typeof(UILabel)).text = (CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPersistPlayDataWithKey(EnumPersistPlayDataKey_lua.eNavalWarTrainingReward) and 1 or 0).."/1"
end

function LuaShuJia2023MainWnd:InitOceanFight() -- HaiZhanPve
    self.OceanFight:Find("Hint"):GetComponent(typeof(UILabel)).text = ShuJia2023_Setting.GetData("HaiZhanPveDesc").Value
    self.OceanFight:Find("Reward"):GetComponent(typeof(UILabel)).text = (CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eNavalWarPveEasyRewardTimes) or 0).."/"..NavalWar_Setting.GetData().PveRewardWeekLimit
end

function LuaShuJia2023MainWnd:InitTreasureSea() -- HaiZhanPvp
    self.TreasureSea:Find("Hint"):GetComponent(typeof(UILabel)).text = ShuJia2023_Setting.GetData("HaiZhanPvpDesc").Value
    self.TreasureSea:Find("Reward"):GetComponent(typeof(UILabel)).text = (CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eNavalWarPvpRewardTimes) or 0).."/"..NavalWar_Setting.GetData().PvpRewardWeekLimit
end

function LuaShuJia2023MainWnd:InitFireTree()
    self.FireTree:Find("Hint"):GetComponent(typeof(UILabel)).text = ShuJia2023_Setting.GetData("FireTreeDesc").Value
    self.FireTree:Find("Reward"):GetComponent(typeof(UILabel)).text = (CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eWTSSEasyTimes) or 0).."/"..NavalWar_Setting.GetData().WTSSWeekLimit
end

function LuaShuJia2023MainWnd:InitPassport(id)
    if id and id ~= self.passId then return end
    self.Passport:Find("Level"):GetComponent(typeof(UILabel)).text = LuaCommonPassportMgr:GetRewardLevel(self.passId)
    self.Passport:Find("Alert").gameObject:SetActive(LuaCommonPassportMgr:HasAward(self.passId))
    UIEventListener.Get(self.Passport.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        LuaCommonPassportMgr:ShowPassportWnd(self.passId)
    end)
end

function LuaShuJia2023MainWnd:Init()
    self.transform:Find("GameObject/Title/TimeLabel"):GetComponent(typeof(UILabel)).text = ShuJia2023_Setting.GetData("ShuJiaActivityTime").Value
    self:InitHaiZhanGuide()
    self:InitOceanFight()
    self:InitTreasureSea()
    self:InitFireTree()
    self:InitPassport()
    if LuaActivityRedDotMgr:IsRedDot(56) then
        self.Anim:Play("shujia2023mainwnd_show_long")
        LuaActivityRedDotMgr:OnRedDotClicked(56)
    else
        self.Anim:Play("shujia2023mainwnd_show")
    end
    self:UpdateXHTXRedDot()
end

function LuaShuJia2023MainWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdatePassDataWithId", self, "InitPassport")
    g_ScriptEvent:AddListener("XianHaiTongXingPlayerQueryTeamProgress_Result", self, "OnXianHaiTongXingPlayerQueryTeamProgress_Result")
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey", self, "OnUpdateTempPlayDataWithKey")
end

function LuaShuJia2023MainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdatePassDataWithId", self, "InitPassport")
    g_ScriptEvent:RemoveListener("XianHaiTongXingPlayerQueryTeamProgress_Result", self, "OnXianHaiTongXingPlayerQueryTeamProgress_Result")
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey", self, "OnUpdateTempPlayDataWithKey")
end

function LuaShuJia2023MainWnd:LiftVfx(vfx)
    local rs = CommonDefs.GetComponentsInChildren_Component_Type(vfx.transform, typeof(Renderer))
    for i = 0, rs.Length - 1 do
        rs[i].sortingOrder = 1
    end
end

function LuaShuJia2023MainWnd:IsInAnim()
    return self.Anim:IsPlaying("shujia2023mainwnd_show") and self.Anim["shujia2023mainwnd_show"].time < 1.5
        or self.Anim:IsPlaying("shujia2023mainwnd_show_long") and self.Anim["shujia2023mainwnd_show_long"].time < 6.6
end

function LuaShuJia2023MainWnd:OnHaiZhanGuideClick()
    if self.m_CurSignUpType ~= 0 or self:IsInAnim() then return end
    self.m_CurSignUpType = 1
    self.HaiZhanGuide:GetComponent(typeof(BoxCollider)).enabled = false

    self.TitleVfxRoot:Find("Title (1)").gameObject:SetActive(true)
    self.TitleVfxRoot:Find("Title (3)").gameObject:SetActive(false)
    self.TitleVfxRoot:Find("shenshuadd").gameObject:SetActive(false)
    self.TitleVfxRoot:Find("Title"):GetComponent(typeof(CUITexture)):LoadMaterial("Fx/Vfx/FestivalActivity/Festival_ShuJia/ShuJia2023/Materials/vfx_shujiahaizhansignupwnd_title_chutanxianhai.mat")
    self.Anim:Play("shujia2023mainwnd_qiehuan2")

    self.SignUpWnd:Awake()
    self.SignUpWnd.m_LuaSelf:Init(self.m_CurSignUpType, self.Anim)
    self.SignUpWndShui:SetActive(true)
end

function LuaShuJia2023MainWnd:OnOceanFightClick()
    if self.m_CurSignUpType ~= 0 or self:IsInAnim() then return end
    self.m_CurSignUpType = 2
    self.OceanFight:GetComponent(typeof(BoxCollider)).enabled = false

    self.TitleVfxRoot:Find("Title (1)").gameObject:SetActive(false)
    self.TitleVfxRoot:Find("Title (3)").gameObject:SetActive(true)
    self.TitleVfxRoot:Find("shenshuadd").gameObject:SetActive(false)
    self.TitleVfxRoot:Find("Title"):GetComponent(typeof(CUITexture)):LoadMaterial("Fx/Vfx/FestivalActivity/Festival_ShuJia/ShuJia2023/Materials/vfx_shujiahaizhansignupwnd_title_piaomiaohaitu.mat")
    self.Anim:Play("shujia2023mainwnd_qiehuan")

    self.SignUpWnd:Awake()
    self.SignUpWnd.m_LuaSelf:Init(self.m_CurSignUpType, self.Anim)
    self.SignUpWndShui:SetActive(true)
end

function LuaShuJia2023MainWnd:OnTreasureSeaClick()
    if self.m_CurSignUpType ~= 0 or self:IsInAnim() then return end
    CUIManager.ShowUI(CLuaUIResources.TreasureSeaSignUpWnd)
end

function LuaShuJia2023MainWnd:OnFireTreeClick()
    if self.m_CurSignUpType ~= 0 or self:IsInAnim() then return end
    self.m_CurSignUpType = 3
    self.FireTree:GetComponent(typeof(BoxCollider)).enabled = false

    self.TitleVfxRoot:Find("Title (1)").gameObject:SetActive(false)
    self.TitleVfxRoot:Find("Title (3)").gameObject:SetActive(false)
    self.TitleVfxRoot:Find("shenshuadd").gameObject:SetActive(true)
    self.TitleVfxRoot:Find("Title"):GetComponent(typeof(CUITexture)):LoadMaterial("Fx/Vfx/FestivalActivity/Festival_ShuJia/ShuJia2023/Materials/vfx_shujiahaizhansignupwnd_title_piaomiaohaitu_1.mat")
    self.Anim:Play("shujia2023mainwnd_qiehuan3")

    self.SignUpWnd:Awake()
    self.SignUpWnd.m_LuaSelf:Init(self.m_CurSignUpType, self.Anim)
    self.SignUpWndShui:SetActive(true)
end

function LuaShuJia2023MainWnd:OnSignUpCloseBtnClick()
    if self.m_CurSignUpType == 1 then
        self.HaiZhanGuide:GetComponent(typeof(BoxCollider)).enabled = true
        self.Anim:Play("shujia2023mainwnd_qiehuanhui2")
    elseif self.m_CurSignUpType == 2 then
        self.OceanFight:GetComponent(typeof(BoxCollider)).enabled = true
        self.Anim:Play("shujia2023mainwnd_qiehuanhui")
    elseif self.m_CurSignUpType == 3 then
        self.FireTree:GetComponent(typeof(BoxCollider)).enabled = true
        self.Anim:Play("shujia2023mainwnd_qiehuanhui3")
    end
    self.SignUpWndShui:SetActive(false)
    self.m_CurSignUpType = 0
end

-- 仙海同行及红点
function LuaShuJia2023MainWnd:OnXHTXBtnClick()
    CUIManager.ShowUI(CLuaUIResources.ShuJia2023XHTXWnd)
end

function LuaShuJia2023MainWnd:OnXianHaiTongXingPlayerQueryTeamProgress_Result(progress)
    self:UpdateXHTXRedDot()
end

function LuaShuJia2023MainWnd:OnUpdateTempPlayDataWithKey(args)
    local key = args[0]
    if key == EnumTempPlayDataKey_lua.eXianHaiTongXingData then
        self:UpdateXHTXRedDot()
    end
end

function LuaShuJia2023MainWnd:UpdateXHTXRedDot()
    self.m_XHTXRedDot:SetActive(self:GetXHTXRewardAvailable())
end

function LuaShuJia2023MainWnd:GetXHTXRewardAvailable()
    local rewardedData = 0
    local oldProgress = 0
    local week = 0
    if CClientMainPlayer.Inst and CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumTempPlayDataKey_lua.eXianHaiTongXingData) then
        local playDataU = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumTempPlayDataKey_lua.eXianHaiTongXingData)
        local tab = g_MessagePack.unpack(playDataU.Data.Data)
        if tab then
            local teamId = tab[EnumXianHaiTongXingDataType.eTeamId]
            if teamId then
                week = tab[EnumXianHaiTongXingDataType.eWeek]
                oldProgress = (tab[EnumXianHaiTongXingDataType.eOldProgress] or {})[week] or 0
                rewardedData = (tab[EnumXianHaiTongXingDataType.eReward] or {})[week] or 0
            else
                return false
            end
        end
    end

    for i = 1, #self.m_RewardInfos do 
        local rewardInfo = self.m_RewardInfos[i]
        if bit.band(rewardedData, bit.lshift(1, i - 1)) > 0 then
        elseif oldProgress >= rewardInfo.pointVal then
        elseif rewardInfo.pointVal <= LuaShuJia2023Mgr.m_XHTXProgress then
            return true
        end
    end
    return false
end