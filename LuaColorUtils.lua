LuaColorUtils = {}

LuaColorUtils.RedColor = Color.red
LuaColorUtils.RedColorTx = "FF0000"

LuaColorUtils.GreenColor = Color.green
LuaColorUtils.GreenColorTx = "00FF00"

LuaColorUtils.WhiteColor = Color.white
LuaColorUtils.WhiteColorTx = "FFFFFF"

LuaColorUtils.UILightBlurColorTx = "ACF8FF"
LuaColorUtils.UILightYellowColorTx = "FFFE91"