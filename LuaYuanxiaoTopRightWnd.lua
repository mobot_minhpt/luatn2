require("common/common_include")

local CUIResources = import "L10.UI.CUIResources"
local UILabel = import "UILabel"
local GameObject  = import "UnityEngine.GameObject"
local CTopAndRightTipWnd=import "L10.UI.CTopAndRightTipWnd"
local CUIFx = import "L10.UI.CUIFx"
local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaYuanxiaoTopRightWnd = class()

RegistChildComponent(LuaYuanxiaoTopRightWnd, "rightBtn", GameObject)
RegistChildComponent(LuaYuanxiaoTopRightWnd, "node1", GameObject)
RegistChildComponent(LuaYuanxiaoTopRightWnd, "node2", GameObject)
RegistChildComponent(LuaYuanxiaoTopRightWnd, "scoreLabel", UILabel)
RegistChildComponent(LuaYuanxiaoTopRightWnd, "fxNode", CUIFx)
RegistChildComponent(LuaYuanxiaoTopRightWnd, "addScoreNode", GameObject)

--RegistClassMember(LuaYuanxiaoTopRightWnd, "savePercent")
RegistClassMember(LuaYuanxiaoTopRightWnd, "m_Tick")
RegistClassMember(LuaYuanxiaoTopRightWnd, "m_RemainTimeLabel")
RegistClassMember(LuaYuanxiaoTopRightWnd, "m_RemainTime")

function LuaYuanxiaoTopRightWnd:Init()
    UIEventListener.Get(self.rightBtn).onClick=LuaUtils.VoidDelegate(function(go)
        LuaUtils.SetLocalRotation(self.rightBtn.transform,0,0,0)
        CTopAndRightTipWnd.showPackage = false
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
    self.addScoreNode:SetActive(false)
    self.node1.transform:Find("right").gameObject:SetActive(false)
    self.node1.transform:Find("wrong").gameObject:SetActive(false)
    self.node2.transform:Find("right").gameObject:SetActive(false)
    self.node2.transform:Find("wrong").gameObject:SetActive(false)
    self.scoreLabel.text = '0'
    --self:UpdateInfo()

    self.m_RemainTimeLabel = self.transform:Find("Anchor/Content/scoreNode/RemainTimeLabel"):GetComponent(typeof(UILabel))
end


function LuaYuanxiaoTopRightWnd:UpdateInfo()
  self.scoreLabel.text = LuaYuanxiaoMgr.score

  if LuaYuanxiaoMgr.piSAT and LuaYuanxiaoMgr.piSAT == 0 then
    self.node1.transform:Find("right").gameObject:SetActive(false)
    self.node1.transform:Find("wrong").gameObject:SetActive(false)
  end
  if LuaYuanxiaoMgr.piSAT and LuaYuanxiaoMgr.piSAT == 1 then
    self.node1.transform:Find("right").gameObject:SetActive(true)
    self.node1.transform:Find("wrong").gameObject:SetActive(false)
  end
  if LuaYuanxiaoMgr.piSAT and LuaYuanxiaoMgr.piSAT == 2 then
    self.node1.transform:Find("right").gameObject:SetActive(false)
    self.node1.transform:Find("wrong").gameObject:SetActive(true)
  end

  self.node1.transform:Find("node").gameObject:SetActive(false)
  if LuaYuanxiaoMgr.pi and LuaYuanxiaoMgr.pi > 0 then
    local data = YuanXiao_TangYuanNpc.GetData(LuaYuanxiaoMgr.pi)
    if data then
      self.node1.transform:Find("node").gameObject:SetActive(true)
      self.node1.transform:Find("node"):GetComponent(typeof(CUITexture)):LoadMaterial(data.iconpath)
    end
  end

  self.node2.transform:Find("node").gameObject:SetActive(false)
  if LuaYuanxiaoMgr.xian and LuaYuanxiaoMgr.xian > 0 then
    local data = YuanXiao_TangYuanNpc.GetData(LuaYuanxiaoMgr.xian)
    if data then
      self.node2.transform:Find("node").gameObject:SetActive(true)
      self.node2.transform:Find("node"):GetComponent(typeof(CUITexture)):LoadMaterial(data.iconpath)
    end
  end

  if LuaYuanxiaoMgr.xianSAT and LuaYuanxiaoMgr.xianSAT == 0 then
    self.node2.transform:Find("right").gameObject:SetActive(false)
    self.node2.transform:Find("wrong").gameObject:SetActive(false)
  end
  if LuaYuanxiaoMgr.xianSAT and LuaYuanxiaoMgr.xianSAT == 1 then
    self.node2.transform:Find("right").gameObject:SetActive(true)
    self.node2.transform:Find("wrong").gameObject:SetActive(false)
  end
  if LuaYuanxiaoMgr.xianSAT and LuaYuanxiaoMgr.xianSAT == 2 then
    self.node2.transform:Find("right").gameObject:SetActive(false)
    self.node2.transform:Find("wrong").gameObject:SetActive(true)
  end

  self.m_RemainTime = LuaYuanxiaoMgr.remainTime
  self:UpdateTime()
  if self.m_Tick then UnRegisterTick(self.m_Tick) end
  self.m_Tick = RegisterTickWithDuration(function ()
      self.m_RemainTime = self.m_RemainTime - 1
      if self.m_RemainTime < 0 then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
        return
      end
      self:UpdateTime()
    end, 1000, self.m_RemainTime * 1000)
end

function LuaYuanxiaoTopRightWnd:UpdateTime()
  self.m_RemainTimeLabel.text = SafeStringFormat3("%02d:%02d", math.floor(self.m_RemainTime / 60), math.floor(self.m_RemainTime % 60))
end

function LuaYuanxiaoTopRightWnd:UpdateAddScore()
  if CClientMainPlayer.Inst then
    CClientMainPlayer.Inst:ShowScoreText(LuaYuanxiaoMgr.addScore)
  end
  if LuaYuanxiaoMgr.isMyScore then
    self.fxNode:DestroyFx()
    self.fxNode:LoadFx("Fx/UI/Prefab/UI_bianlunxiaojuhuosheng.prefab")
  end
end

function LuaYuanxiaoTopRightWnd:OnEnable()
  g_ScriptEvent:AddListener("YuanXiaoUpdateInfo", self, "UpdateInfo")
  g_ScriptEvent:AddListener("YuanXiaoAddScore", self, "UpdateAddScore")
end

function LuaYuanxiaoTopRightWnd:OnDisable()
  g_ScriptEvent:RemoveListener("YuanXiaoUpdateInfo", self, "UpdateInfo")
  g_ScriptEvent:RemoveListener("YuanXiaoAddScore", self, "UpdateAddScore")
end

function LuaYuanxiaoTopRightWnd:OnDestroy()
  if self.m_Tick then
      UnRegisterTick(self.m_Tick)
      self.m_Tick = nil
  end
end

return LuaYuanxiaoTopRightWnd
