local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

CLuaNpcHaoGanDuMgr = {}
function CLuaNpcHaoGanDuMgr.GetHaoGanDuLevel(v)
    local setting = NpcHaoGanDu_Setting.GetData()
    local field = setting.HaoGanDuGrade

    local grade = 0
    for i=1,field.Length do
        local low = field[i-1][1]
        local high = field[i-1][2]
        if v>=low and v<=high then
            grade = i
            break
        elseif v>high then
            grade=i
        end
    end
    return grade
end

function CLuaNpcHaoGanDuMgr.GetBiAnHuaTextureInfo(grade)
    local data = NpcHaoGanDu_Level.GetData(grade)
    return data.Icon,64,64
end


function CLuaNpcHaoGanDuMgr.GetFx(grade)
    return NpcHaoGanDu_Level.GetData(grade).UIFx
end

--修正数据，过期就置0
function CLuaNpcHaoGanDuMgr.ModifyNpcData(npcData)
    if npcData.BuyNum>0 then
        local date1 = CServerTimeMgr.ConvertTimeStampToZone8Time(npcData.BuyTime)
        local date2 = CServerTimeMgr.Inst:GetZone8Time()
        if date1:ToString("d") ~= date2:ToString("d") then
            npcData.BuyNum=0
        end
    end
    if npcData.ZengSongNum>0 then
        local date1 = CServerTimeMgr.ConvertTimeStampToZone8Time(npcData.ZengSongTime)
        local date2 = CServerTimeMgr.Inst:GetZone8Time()
        if date1:ToString("d") ~= date2:ToString("d") then
            npcData.ZengSongNum=0
        end
    end
end
