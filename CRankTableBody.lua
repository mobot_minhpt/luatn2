-- Auto Generated!!
local CGuildLeagueMgr=import "L10.Game.CGuildLeagueMgr"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CJieBaiRankInfoMgr = import "L10.UI.CJieBaiRankInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CRankData = import "L10.UI.CRankData"
local CRankItemTemplate = import "L10.UI.CRankItemTemplate"
local CRankTableBody = import "L10.UI.CRankTableBody"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EnumRankSheet = import "L10.UI.EnumRankSheet"
local Input = import "UnityEngine.Input"
local LocalString = import "LocalString"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local Profession = import "L10.Game.Profession"
local Rank_Baby = import "L10.Game.Rank_Baby"
local Rank_Player = import "L10.Game.Rank_Player"
local RankItemData = import "L10.UI.CRankTableBody+RankItemData"
local String = import "System.String"
local Vector3 = import "UnityEngine.Vector3"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local UITableTween = import "L10.UI.UITableTween"

CRankTableBody.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 
    if index < 0 and index >= this.RankItems.Length then
        return nil
    end
    local cellIdentifier = "RankItemTemplateCell"
    local template = this.RankItemTemplate

    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(template, cellIdentifier)
    end

    local item = CommonDefs.GetComponent_GameObject_Type(cell, typeof(CRankItemTemplate))
    item:InitInfo(this.widthList, this.RankItems[index].playerId, this.RankItems[index].info, this.RankItems[index].isOdd, this.RankItems[index].rankImage, this.RankItems[index].lsId, this.RankItems[index].pName, this.RankItems[index].houseId, this.RankItems[index].ownerId1, this.RankItems[index].ownerId2, this.RankItems[index].jiebaiClassList, this.RankItems[index].job, this.RankItems[index].countryCode)
    return cell
end
CRankTableBody.m_OnRowSelected_CS2LuaHook = function (this, index) 
    if index >= 0 and index < this.RankItems.Length then
        if CRankData.Inst.IsPlayerRank or CRankData.Inst.IsLingShouRank then
            CPlayerInfoMgr.ShowPlayerPopupMenu(this.RankItems[index].playerId, EnumPlayerInfoContext.RankList, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
        elseif CRankData.Inst.IsGuildRank then
            local rows = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))

            if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst:IsInGuild() then
                CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("申请入帮"), nil, false, nil))
            else
                CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("申请入帮"), DelegateFactory.Action_int(function (row) 
                    Gac2Gas.RequestOperationInGuild(this.RankItems[index].playerId, "ApplyAsMember", "", "")
                end), false, nil))
            end

            local mainCamera = CUIManager.UIMainCamera
            if mainCamera == nil then
                return
            end
            local worldPos = mainCamera:ScreenToWorldPoint(Input.mousePosition)
            CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(rows), worldPos, CPopupMenuInfoMgr.AlignType.Bottom)
        elseif CRankData.Inst.IsHouseRank and CClientMainPlayer.Inst ~= nil and this.RankItems[index].ownerId1 ~= CClientMainPlayer.Inst.Id then
            local rows = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
            CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("拜访家园"), DelegateFactory.Action_int(function (row) 
                --Gac2Gas.RequestEnterHouseSceneFromRank(RankItems[index].houseId);
                Gac2Gas.QueryHouseIntroByHouseId(this.RankItems[index].houseId)
            end), false, nil))
            CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("主人信息"), DelegateFactory.Action_int(function (row) 
                CPlayerInfoMgr.ShowPlayerPopupMenu(this.RankItems[index].ownerId1, EnumPlayerInfoContext.RankList, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgr.AlignType.Left)
            end), false, nil))
            if this.RankItems[index].ownerId2 > 0 then
                CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("配偶信息"), DelegateFactory.Action_int(function (row) 
                    CPlayerInfoMgr.ShowPlayerPopupMenu(this.RankItems[index].ownerId2, EnumPlayerInfoContext.RankList, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgr.AlignType.Left)
                end), false, nil))
            end
            local mainCamera = CUIManager.UIMainCamera
            if mainCamera == nil then
                return
            end
            local worldPos = mainCamera:ScreenToWorldPoint(Input.mousePosition)
            CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(rows), worldPos, CPopupMenuInfoMgr.AlignType.Bottom)
        elseif CRankData.Inst.IsJieBaiRank then
            CJieBaiRankInfoMgr.jiebaiTitle = this.RankItems[index].jiebaiTitle
            Gac2Gas.QueryRankJieBaiInfo(this.RankItems[index].jiebaiKey)
        elseif Rank_Baby.Exists(CRankData.Inst.RankId) then
            CPlayerInfoMgr.ShowPlayerPopupMenu(this.RankItems[index].playerId, EnumPlayerInfoContext.RankList, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
        end
        if this.RankItems[index] then
            g_ScriptEvent:BroadcastInLua("OnRankDetailViewSelectedPlayer", this.RankItems[index])
        end
    end
end
CRankTableBody.m_InitListInfo_CS2LuaHook = function (this, width) 
    this.tableView:Clear()
    this.tableView.dataSource = this
    this.tableView.eventDelegate = this

    local listInfo = CRankData.Inst.RankList
    this.RankItems = CreateFromClass(MakeArrayClass(RankItemData), listInfo.Count)
    local info = CreateFromClass(MakeGenericClass(List, String))
    local needHideHeaderNameColumn = System.String.IsNullOrEmpty(CRankData.GetTitle.HeaderName) --是否隐藏最后一列
    do
        local i = 0
        while i < listInfo.Count do
            CommonDefs.ListClear(info)
            local rankImage = ""

            repeat
                local default = CRankData.Inst.RankList[i].Rank
                if default == (1) then
                    rankImage = "Rank_No.1"
                    break
                elseif default == (2) then
                    rankImage = "Rank_No.2png"
                    break
                elseif default == (3) then
                    rankImage = "Rank_No.3png"
                    break
                else
                    break
                end
            until 1

            if listInfo[i].rankSheet == EnumRankSheet.ePlayer then
                CommonDefs.ListAdd(info, typeof(String), tostring(listInfo[i].Rank))
                CommonDefs.ListAdd(info, typeof(String), listInfo[i].Name)
                local isSpecial = CLuaRankTableBody:ProcessSpecialPlayerRankSheet(listInfo[i], info)
                if not isSpecial then
                    if not IsOpenNewRankWnd() then
                        CommonDefs.ListAdd(info, typeof(String), EnumToInt(listInfo[i].Job)==0 and LocalString.GetString("方士") or Profession.GetFullName(listInfo[i].Job))
                    end
                    CommonDefs.ListAdd(info, typeof(String), listInfo[i].Guild_Name)
                    local rankPlayer = Rank_Player.GetData(listInfo[i].rankId)
                    local result = ""
                    if rankPlayer.Point == 0 then
                        result = CUICommonDef.ConvertLargeNumber2String(math.floor(listInfo[i].Value))
                    elseif rankPlayer.Point == 1 then
                        result = CUICommonDef.ConvertDouble2String(listInfo[i].Value, 1)
                    elseif rankPlayer.Point == 2 then
                        result = CUICommonDef.ConvertDouble2String(listInfo[i].Value + 0.05, 1)
                    elseif rankPlayer.Point == 3 then
                        result = CUICommonDef.ConvertDouble2String(listInfo[i].Value + 0.005, 2)
                    end
                    if rankPlayer.Display == 1 then
                        result = System.String.Format("{0}%", result)
                    end
                    CommonDefs.ListAdd(info, typeof(String), result)
                end
                this.RankItems[i] = RankItemData(listInfo[i].PlayerIndex, info, listInfo[i].Rank % 2 ~= 0, rankImage, "", "", "", 0, 0, "", "", nil, EnumToInt(listInfo[i].Job), listInfo[i].countryCode)
            elseif listInfo[i].rankSheet == EnumRankSheet.eGuild then
                CommonDefs.ListAdd(info, typeof(String), tostring(listInfo[i].Rank))
                CommonDefs.ListAdd(info, typeof(String), listInfo[i].Guild_Name)
                CommonDefs.ListAdd(info, typeof(String), tostring(listInfo[i].Member))
                CommonDefs.ListAdd(info, typeof(String), tostring(listInfo[i].Level))
                if listInfo[i].rankId==41100004 then
                    local val = listInfo[i].Value
                    local levelstring = LuaGuildLeagueMgr.GetLevelStr(math.floor(val/100))
                    local str = levelstring..SafeStringFormat3( LocalString.GetString("第%s名"),val%100 )
                    CommonDefs.ListAdd(info, typeof(String), str)
                elseif listInfo[i].rankId==41100020 then
                    local val = listInfo[i].Value
                    local dateTime = CServerTimeMgr.ConvertTimeStampToZone8Time(val)
                    local str = System.String.Format("{0:yyyy-MM-dd}", dateTime)
                    CommonDefs.ListAdd(info, typeof(String), str)
                else
                    CommonDefs.ListAdd(info, typeof(String), needHideHeaderNameColumn and "" or CUICommonDef.ConvertLargeNumber2String(math.floor(listInfo[i].Value)))
                end
                this.RankItems[i] = RankItemData(listInfo[i].PlayerIndex, info, listInfo[i].Rank % 2 ~= 0, rankImage, "", "", "", 0, 0, "", "", nil, 0)

            elseif listInfo[i].rankSheet == EnumRankSheet.eLingShou 
                or listInfo[i].rankSheet == EnumRankSheet.ePlayerShop 
                or listInfo[i].rankSheet == EnumRankSheet.eMapi then
                CommonDefs.ListAdd(info, typeof(String), tostring(listInfo[i].Rank))
                CommonDefs.ListAdd(info, typeof(String), listInfo[i].Name)
                if not IsOpenNewRankWnd() then
                    CommonDefs.ListAdd(info, typeof(String), EnumToInt(listInfo[i].Job)==0 and LocalString.GetString("方士") or Profession.GetFullName(listInfo[i].Job))
                end
                CommonDefs.ListAdd(info, typeof(String), listInfo[i].LingshouName)
                CommonDefs.ListAdd(info, typeof(String), CUICommonDef.ConvertLargeNumber2String(math.floor(listInfo[i].Value)))
                this.RankItems[i] = RankItemData(listInfo[i].PlayerIndex, info, listInfo[i].Rank % 2 ~= 0, rankImage, listInfo[i].LingshouId, listInfo[i].Name, "", 0, 0, "", "", nil, EnumToInt(listInfo[i].Job))
            elseif listInfo[i].rankSheet == EnumRankSheet.eHouse then
                CommonDefs.ListAdd(info, typeof(String), tostring(listInfo[i].Rank))
                CommonDefs.ListAdd(info, typeof(String), listInfo[i].ownerName1)
                CommonDefs.ListAdd(info, typeof(String), listInfo[i].ownerName2)
                CommonDefs.ListAdd(info, typeof(String), listInfo[i].HouseName)
                CommonDefs.ListAdd(info, typeof(String), CUICommonDef.ConvertLargeNumber2String(math.floor(listInfo[i].Value)))
                this.RankItems[i] = RankItemData(0, info, listInfo[i].Rank % 2 ~= 0, rankImage, "", "", listInfo[i].HouseId, listInfo[i].ownerId1, listInfo[i].ownerId2, "", "", nil, 0)
            elseif listInfo[i].rankSheet == EnumRankSheet.eJieBai then
                CommonDefs.ListAdd(info, typeof(String), tostring(listInfo[i].Rank))
                CommonDefs.ListAdd(info, typeof(String), listInfo[i].jiebaiTitle)
                CommonDefs.ListAdd(info, typeof(String), "")
                CommonDefs.ListAdd(info, typeof(String), CUICommonDef.ConvertLargeNumber2String(math.floor(listInfo[i].Value)))
                this.RankItems[i] = RankItemData(0, info, listInfo[i].Rank % 2 ~= 0, rankImage, "", "", "", 0, 0, listInfo[i].jiebaiKey, listInfo[i].jiebaiTitle, listInfo[i].jiebaiClassList, 0)
            elseif listInfo[i].rankSheet == EnumRankSheet.eBaby then
                CommonDefs.ListAdd(info, typeof(String), tostring(listInfo[i].Rank))
                CommonDefs.ListAdd(info, typeof(String), ToStringWrap(listInfo[i].data[0]))
                CommonDefs.ListAdd(info, typeof(String), ToStringWrap(listInfo[i].data[1]))
                CommonDefs.ListAdd(info, typeof(String), ToStringWrap(listInfo[i].data[2]))
                CommonDefs.ListAdd(info, typeof(String), ToStringWrap(listInfo[i].data[3]))
                this.RankItems[i] = RankItemData(listInfo[i].PlayerIndex, info, listInfo[i].Rank % 2 ~= 0, rankImage, "", "", "", 0, 0, "", "", nil, 0)
            elseif listInfo[i].rankSheet == EnumRankSheet.eSect then
                CommonDefs.ListAdd(info, typeof(String), tostring(listInfo[i].Rank))
                CommonDefs.ListAdd(info, typeof(String), listInfo[i].Guild_Name)
                CommonDefs.ListAdd(info, typeof(String), tostring(listInfo[i].Member))
                CommonDefs.ListAdd(info, typeof(String), tostring(listInfo[i].Level))
                CommonDefs.ListAdd(info, typeof(String), needHideHeaderNameColumn and "" or CUICommonDef.ConvertLargeNumber2String(math.floor(listInfo[i].Value)))
                this.RankItems[i] = RankItemData(listInfo[i].PlayerIndex, info, listInfo[i].Rank % 2 ~= 0, rankImage, "", "", "", 0, 0, "", "", nil, 0)
            end
            i = i + 1
        end
    end
    this.widthList = width
    local widget, tableTween
    if this.tableView._table ~= nil then
        widget = this.tableView._table:GetComponent(typeof(UIWidget))
        tableTween = this.tableView._table:GetComponent(typeof(UITableTween))
    end
    this.tableView:LoadData(0, false)
    if widget then
        widget:ResetAndUpdateAnchors()
    end
    if tableTween then
        tableTween:PlayAnim()
    end
end

CLuaRankTableBody = {}
function CLuaRankTableBody:ProcessSpecialPlayerRankSheet(Info, info)
    -- 倒卖玩法
    if 41000254 <= Info.rankId and Info.rankId <= 41000257 then
        local list = MsgPackImpl.unpack(Info.extraData)
        if list and list.Count == 4 then
            CommonDefs.ListAdd(info, typeof(String),
                SafeStringFormat3("%s/%s", ToStringWrap(list[2]), ToStringWrap(list[1])))
            CommonDefs.ListAdd(info, typeof(String), list[0] < 10000000000 and SafeStringFormat3("%.f", list[0]) or
                SafeStringFormat3(LocalString.GetString("%.f亿"), math.floor(list[0] / 100000000)))
        else
            CommonDefs.ListAdd(info, typeof(String), LocalString.GetString(""))
            CommonDefs.ListAdd(info, typeof(String), LocalString.GetString(""))
        end

        return true
    end

    return false
end
