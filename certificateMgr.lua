local Lua = import "L10.Engine.Lua"
local LoadPicMgr = import "L10.Game.LoadPicMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CChatMgr = import "L10.Game.CChatMgr"
certificateMgr = {}
--服务器端的状态枚举定义
EnumMobileBindVerifyStatus = {
	eUnknown = 0,
	eNotBind = 1,
	eBindNotVerify = 2,
	eBindAndVerify = 3,
}

certificateMgr.m_MobileBindVerifyStatus = EnumMobileBindVerifyStatus.eUnknown
--call from CChatMgr.cs
function certificateMgr.CheckStatusForSendChat(channelName, msg)
		if channelName == CChatMgr.CHANNEL_NAME_WORLD or channelName == CChatMgr.CHANNEL_NAME_LOCALWORLD then
				return certificateMgr.CheckStatusAndShowMessage()
		else
				return true
		end
end

function certificateMgr.CheckStatusAndShowMessage()
		if not CommonDefs.IS_CN_CLIENT then
				return true
		end
		if certificateMgr.Enable then
				--未经过绑定和验证
				if CLoginMgr.Inst:IsNetEaseOfficialLogin() then
						local message = g_MessageMgr:FormatMessage("Need_Real_Name_Certification_For_Official_Channel")
						MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
								-- not bind 和 not verify都调用同一个RPC即可
								Gac2Gas.BindMobileOpenWnd()
						end), nil, nil, nil, false)
				else
						g_MessageMgr:ShowMessage("Need_Real_Name_Certification_For_Other_Channel")
				end
				return false
		else
				--已经过绑定和验证
				return true
		end
end


function Gas2Gac.SyncMobileBindVerifyStatus(status)
	certificateMgr.m_MobileBindVerifyStatus = status
	if status == 0 or status == 3 then
		LoadPicMgr.EnableChatSendPic = true
		LoadPicMgr.EnableGroupChatSendPic = true
		LoadPicMgr.EnableChatSaveSmallPic = true
		LoadPicMgr.EnableMomentVideo = true
		certificateMgr.Enable = false
	else
		LoadPicMgr.EnableChatSendPic = false
		LoadPicMgr.EnableGroupChatSendPic = false
		LoadPicMgr.EnableChatSaveSmallPic = false
		LoadPicMgr.EnableMomentVideo = false
		certificateMgr.Enable = true
	end
end

CPersonalSpaceMgr.m_hookShareTextToPersonalSpace = function(this)
  if not certificateMgr.Enable then
    Lua.s_IsHook = false
  else
		if CLoginMgr.Inst:IsNetEaseOfficialLogin() then
			local message = g_MessageMgr:FormatMessage("Need_Real_Name_Certification_For_Official_Channel")
			MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
				-- not bind 和 not verify都调用同一个RPC即可
				Gac2Gas.BindMobileOpenWnd()
			end), nil, nil, nil, false)
		else
			g_MessageMgr:ShowMessage("Need_Real_Name_Certification_For_Other_Channel")
		end
    Lua.s_IsHook = true
  end
end
