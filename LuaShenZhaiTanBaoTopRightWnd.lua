local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CMainCamera = import "L10.Engine.CMainCamera"
local Utility = import "L10.Engine.Utility"
local CPos = import "L10.Engine.CPos"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CClientNpc = import "L10.Game.CClientNpc"

LuaShenZhaiTanBaoTopRightWnd = class()

function LuaShenZhaiTanBaoTopRightWnd:Awake()
    self:InitUIComponent()
    self:InitUIData()
    UIEventListener.Get(self.explandBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnExplandBtnClick()
    end)

    if LuaShuangshiyi2022Mgr.SZTBPlayState then
        self.playState = LuaShuangshiyi2022Mgr.SZTBPlayState
        local phase = math.modf((self.playState - 1000) / 10)
        self:PushProgress(phase, LuaShuangshiyi2022Mgr.Round)
    end
end

function LuaShenZhaiTanBaoTopRightWnd:Init()

end

function LuaShenZhaiTanBaoTopRightWnd:InitUIComponent()
    self.explandBtn = self.transform:Find("Anchor/Offset/ExpandButton")
    self.infoView = self.transform:Find("Anchor/Offset/InfoView")
    
    self.phase1Highlight = self.infoView:Find("Phase1/Highlight")
    self.phase2Highlight = self.infoView:Find("Phase2/Highlight")
    self.phase3Highlight = self.infoView:Find("Phase3/Highlight")
    self.phase1NumberReminder = self.infoView:Find("Phase1NumberReminder")
    self.phase2NumberReminder = self.infoView:Find("Phase2NumberReminder")
    self.phaseHighlightList = {self.phase1Highlight, self.phase2Highlight, self.phase3Highlight}
    for i = 1, #self.phaseHighlightList do
        self.phaseHighlightList[i].gameObject:SetActive(false)    
    end

    self.progressSlider = self.infoView:Find("Slider"):GetComponent(typeof(UISlider))
end

function LuaShenZhaiTanBaoTopRightWnd:InitUIData()
    self.mainPlayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    self.playState = LuaShuangshiyi2022Mgr.SZTBPlayState
    self.SZTBConfigData = Double11_SZTB.GetData()
    
    self.phase1MaxRound = 0
    self.phase1ReminderAddYValue = 100
    Double11_SZTB_Round.Foreach(function(_, data)
        if data.Round > self.phase1MaxRound then
            self.phase1MaxRound = data.Round
        end
    end)
    self.phase1ReminderList = {}
    self.phase1BottomEffectList = {}
    self.phase1TopEffectList = {}
    self.phase1CircleRadius = self.SZTBConfigData.TrapSize * 0.95
    self.phase1UnsuccessEffect = self.SZTBConfigData.Stage1_Effect[0]
    self.phase1SuccessEffect = self.SZTBConfigData.Stage1_Effect[1]
    
    self.phase2BottomEffectList = {}
    self.phase2TopEffectList = {}
    self.phase2CartList = {}
    self.phase2ReminderList = {}
    self.firstEnterPhase2 = true
    local cartInfos = self.SZTBConfigData.CartInfos
    for i = 1, cartInfos.Length do
        local ballId = cartInfos[i-1][0]
        local posX = cartInfos[i-1][4]
        local posY = cartInfos[i-1][5]
        local radius = cartInfos[i-1][6]
        local demandBallNumber = cartInfos[i-1][7]
        local unsuccessEffectId = cartInfos[i-1][8]
        local successEffectId = cartInfos[i-1][9]
        table.insert(self.phase2CartList, {
            ballId = ballId,
            posX = posX,
            posY = posY, 
            radius = radius,
            demandBallNumber = demandBallNumber,
            unsuccessEffectId = unsuccessEffectId,
            successEffectId = successEffectId,
            showSuccess = false
        })
    end
    
    self.phase3MaxRound = 3
end

function LuaShenZhaiTanBaoTopRightWnd:OnExplandBtnClick()
    self.explandBtn.transform.localEulerAngles = Vector3(0, 0, 0)
    self.infoView.gameObject:SetActive(false)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaShenZhaiTanBaoTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SyncSZTBPlayState", self, "OnSyncSZTBPlayState")
    g_ScriptEvent:AddListener("Double11SZTB_Stage1_Finish", self, "OnRemoveStage1Effect")
    g_ScriptEvent:AddListener("Double11SZTB_Stage2_Finish", self, "OnRemoveStage2Effect")
end

function LuaShenZhaiTanBaoTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncSZTBPlayState", self, "OnSyncSZTBPlayState")
    g_ScriptEvent:RemoveListener("Double11SZTB_Stage1_Finish", self, "OnRemoveStage1Effect")
    g_ScriptEvent:RemoveListener("Double11SZTB_Stage2_Finish", self, "OnRemoveStage2Effect")

    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIManager.TopAndRightTipWnd)
    end
end

function LuaShenZhaiTanBaoTopRightWnd:OnHideTopAndRightTipWnd()
    self.explandBtn.transform.localEulerAngles = Vector3(0, 0, 180)
    self.infoView.gameObject:SetActive(true)
end

function LuaShenZhaiTanBaoTopRightWnd:OnSyncSZTBPlayState(playState, round, traps_U)
    self.playState = playState
    local phase = math.modf((playState - 1000) / 10)
    self:PushProgress(phase, round)
    
    self:OnPreProcessPhase1(traps_U)
end

function LuaShenZhaiTanBaoTopRightWnd:Update()
    if self.playState then
        if self.playState == EnumDouble11SZTBPlayState.Stage_1_Scenario or self.playState == EnumDouble11SZTBPlayState.Stage_1_Prepare
                or self.playState == EnumDouble11SZTBPlayState.Stage_1_Playing or self.playState == EnumDouble11SZTBPlayState.Stage_1_Finish then
            self:OnUpdatePhase1()
        end
        if self.playState == EnumDouble11SZTBPlayState.Stage_2 or self.playState == EnumDouble11SZTBPlayState.Stage_2_Scenario then
            self:OnUpdatePhase2()
        end
    end
end

function LuaShenZhaiTanBaoTopRightWnd:OnPreProcessPhase1(traps_U)
    local unpackTrapData = g_MessagePack.unpack(traps_U)
    if unpackTrapData then
        for i = 1, #unpackTrapData do
            local pixelPositionX = unpackTrapData[i][2]
            local pixelPositionY = unpackTrapData[i][3]
            local showNumber = unpackTrapData[i][1]
            local isPoolAvailable = false
            for t = 1, #self.phase1ReminderList do
                if self.phase1ReminderList[t].isDead == true then
                    isPoolAvailable = true
                    self.phase1ReminderList[t].isDead = false
                    self.phase1ReminderList[t].showNumber = showNumber
                    self.phase1ReminderList[t].pixelPosition = {pixelPositionX, pixelPositionY}
                    self.phase1ReminderList[t].showSuccess = false
                    break
                end
            end
            if isPoolAvailable == false then
                local itemGO = CommonDefs.Object_Instantiate(self.phase1NumberReminder.gameObject)
                local labelComp = itemGO.transform:Find("DemandPeople"):GetComponent(typeof(UILabel))
                itemGO.transform.parent = self.infoView
                itemGO.transform.localScale = Vector3.one
                table.insert(self.phase1ReminderList, {obj = itemGO, isDead = false, showNumber = showNumber,
                                                       pixelPosition = {pixelPositionX, pixelPositionY}, labelComp = labelComp, showSuccess = false})
            end
            local centerPos = Utility.PixelPos2WorldPos(CreateFromClass(CPos, pixelPositionX, pixelPositionY))
            local phase1Effect = CEffectMgr.Inst:AddWorldPositionFX(self.phase1UnsuccessEffect, centerPos, 0,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
            self.phase1BottomEffectList[i] = phase1Effect
        end
    else
        --把创建的提示给inactive了, 有需要再active出来用, 类似对象池
        for i = 1, #self.phase1ReminderList do
            self.phase1ReminderList[i].isDead = true
        end
    end
end

function LuaShenZhaiTanBaoTopRightWnd:OnUpdatePhase1()
    for i = 1, #self.phase1ReminderList do
        local reminderData = self.phase1ReminderList[i]
        local reminderObj = reminderData.obj
        if reminderData.isDead then
            reminderObj:SetActive(false)
        else
            local pixelPosition = reminderData.pixelPosition
            local playerDemand = reminderData.showNumber
            local reminderWorldPos = Utility.PixelPos2WorldPos( CPos(pixelPosition[1], pixelPosition[2]))
            local playerInCircle = self:GetPeopleInCircle(reminderWorldPos)
            local viewPos = CMainCamera.Main:WorldToViewportPoint(reminderWorldPos)
            local isInView = viewPos.z > 0 and viewPos.x > 0 and viewPos.x < 1 and viewPos.y > 0 and viewPos.y < 1
            if isInView then
                reminderObj:SetActive(true)
                local screenPos = CMainCamera.Main:WorldToScreenPoint(reminderWorldPos)
                screenPos.z = 0
                screenPos.y = screenPos.y + self.phase1ReminderAddYValue
                local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
                reminderObj.transform.position = worldPos
                reminderData.labelComp.text = playerInCircle .. "/" .. playerDemand
                if playerInCircle == playerDemand then
                    --这个圈符合目标
                    if not reminderData.showSuccess then
                        --之前是失败的, 特效改成功版本
                        reminderData.showSuccess = true
                        local centerPos = Utility.PixelPos2WorldPos(CreateFromClass(CPos, pixelPosition[1], pixelPosition[2]))
                        local phase1Effect = CEffectMgr.Inst:AddWorldPositionFX(self.phase1SuccessEffect, centerPos, 0,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
                        self.phase1TopEffectList[i] = phase1Effect
                    end
                else
                    --这个圈不符合目标
                    if reminderData.showSuccess then
                        --之前是成功的, 特效改失败版本
                        reminderData.showSuccess = false
                        if self.phase1TopEffectList[i] then
                            self.phase1TopEffectList[i]:Destroy()
                        end
                    end
                end
            else
                reminderObj:SetActive(false)
            end
        end
    end
end

function LuaShenZhaiTanBaoTopRightWnd:OnRemoveStage1Effect()
    local effectLen = self.phase1BottomEffectList and #self.phase1BottomEffectList or 0
    for i = 1, effectLen do
        if self.phase1BottomEffectList[i] then
            self.phase1BottomEffectList[i]:Destroy()
        end
        if self.phase1TopEffectList[i] then
            self.phase1TopEffectList[i]:Destroy()
        end
    end
    self.phase1BottomEffectList = {}
    self.phase1TopEffectList = {}
end

function LuaShenZhaiTanBaoTopRightWnd:OnUpdatePhase2()
    if self.firstEnterPhase2 then
        for i = 1, #self.phase2CartList do
            local cartData = self.phase2CartList[i]
            local centerPos = Utility.GridPos2WorldPos(cartData.posX, cartData.posY)

            local phase2Effect = CEffectMgr.Inst:AddWorldPositionFX(cartData.unsuccessEffectId, centerPos, 0,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
            
            self.phase2BottomEffectList[i] = phase2Effect
            
            local itemGO = CommonDefs.Object_Instantiate(self.phase2NumberReminder.gameObject)
            local labelComp = itemGO.transform:Find("DemandPeople"):GetComponent(typeof(UILabel))
            itemGO.transform.parent = self.infoView
            itemGO.transform.localScale = Vector3.one
            table.insert(self.phase2ReminderList, {obj = itemGO,
                                                   labelComp = labelComp
            })
        end
        self.firstEnterPhase2 = false
    end
    
    for i = 1, #self.phase2CartList do
        local cartData = self.phase2CartList[i]
        local ballDemand = cartData.demandBallNumber
        local ballInCircle = 0
        local worldPos = Utility.GridPos2WorldPos(cartData.posX, cartData.posY)
        
        CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
            if obj and TypeIs(obj, typeof(CClientNpc)) and obj.TemplateId == cartData.ballId then
                local otherPlayerPosition = obj.RO.Position
                if Vector3.Distance(otherPlayerPosition, worldPos) <= cartData.radius then
                    ballInCircle = ballInCircle + 1
                end
            end
        end))

        local viewPos = CMainCamera.Main:WorldToViewportPoint(worldPos)
        local isInView = viewPos.z > 0 and viewPos.x > 0 and viewPos.x < 1 and viewPos.y > 0 and viewPos.y < 1
        if isInView and ballInCircle < ballDemand then
            self.phase2ReminderList[i].obj:SetActive(true)
            local screenPos = CMainCamera.Main:WorldToScreenPoint(worldPos)
            screenPos.z = 0
            screenPos.y = screenPos.y + self.phase1ReminderAddYValue
            local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
            self.phase2ReminderList[i].obj.transform.position = worldPos
            self.phase2ReminderList[i].labelComp.text = ballInCircle .. "/" .. ballDemand
        else
            self.phase2ReminderList[i].obj:SetActive(false)
        end
        
        if ballInCircle >= ballDemand then
            --这个圈符合目标
            if not cartData.showSuccess then
                --之前是失败的, 特效改成功版本
                cartData.showSuccess = true
                local centerPos = Utility.GridPos2WorldPos(cartData.posX, cartData.posY)
                local phase2Effect = CEffectMgr.Inst:AddWorldPositionFX(cartData.successEffectId, centerPos, 0,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
                self.phase2TopEffectList[i] = phase2Effect
            end
        else
            --这个圈不符合目标
            if cartData.showSuccess then
                --之前是成功的, 特效改失败版本
                cartData.showSuccess = false
                if self.phase2TopEffectList[i] then
                    self.phase2TopEffectList[i]:Destroy()
                end
            end
        end
    end
end

function LuaShenZhaiTanBaoTopRightWnd:OnRemoveStage2Effect()
    local effectLen = self.phase2BottomEffectList and #self.phase2BottomEffectList or 0
    for i = 1, effectLen do
        if self.phase2BottomEffectList[i] then
            self.phase2BottomEffectList[i]:Destroy()
        end
        if self.phase2TopEffectList[i] then
            self.phase2TopEffectList[i]:Destroy()
        end
    end
    for i = 1, #self.phase2ReminderList do
        self.phase2ReminderList[i].obj:SetActive(false)
    end
    self.phase2BottomEffectList = {}
    self.phase2TopEffectList = {}
end

function LuaShenZhaiTanBaoTopRightWnd:GetPeopleInCircle(worldPos)
    local inCircle = 0
    local mainPlayerObj = CClientMainPlayer.Inst
    local mainPlayerPosition = mainPlayerObj and mainPlayerObj.RO.Position
    local mainPlayerDistance = Vector3.Distance(mainPlayerPosition, worldPos)
    if mainPlayerDistance <= self.phase1CircleRadius then
        inCircle = inCircle + 1
    end
    --这个地方应该可以cache?
    CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
        if obj and TypeIs(obj, typeof(CClientOtherPlayer)) then
            local otherPlayerPosition = obj.RO.Position
            if Vector3.Distance(otherPlayerPosition, worldPos) <= self.phase1CircleRadius then
                inCircle = inCircle + 1
            end
        end
    end))
    return inCircle
end

function LuaShenZhaiTanBaoTopRightWnd:PushProgress(curPhase, phaseRound)
    local progress = 0
    local phaseAddProgress = 0.333
    for i = 1, #self.phaseHighlightList do
        self.phaseHighlightList[i].gameObject:SetActive(curPhase > i)
        if curPhase > i then
            progress = progress + phaseAddProgress
        else
            if curPhase == 1 and phaseRound > 1  then
                progress = progress + phaseAddProgress * (phaseRound - 1) / self.phase1MaxRound
            elseif curPhase == 3 and phaseRound > 1 then
                progress = progress + phaseAddProgress * (phaseRound - 1) / self.phase3MaxRound
            end
            break
        end
    end
    self.progressSlider.value = progress
end