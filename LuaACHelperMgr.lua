local Utility = import "L10.Engine.Utility"
local ACHelperMgr = import "L10.Engine.ACHelperMgr"
local Time = import "UnityEngine.Time"
local Main = import "L10.Engine.Main"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"

LuaACHelperMgr = class()
LuaACHelperMgr.SendAntiCheatCheckInfo = function()
    local reportId = ACHelperMgr.ACHelper_ReportId and ACHelperMgr.ACHelper_ReportId or ""
    local array = Utility.BreakString(reportId, 5, 255, nil)
    Gac2Gas.SendAntiCheatCheckInfo(ACHelperMgr.InitRetCode, array[0], array[1], array[2], array[3], array[4])
end

LuaACHelperMgr.m_LastCheatCheckTime = 0
LuaACHelperMgr.m_CheatCheckInterval = 60 * 20
function Gas2Gac.QueryAntiCheatCheckInfo()
    if ACHelperMgr.IsACHelperSupported() then
        local time = Time.realtimeSinceStartup
        if LuaACHelperMgr.m_LastCheatCheckTime==0 or time - LuaACHelperMgr.m_LastCheatCheckTime > LuaACHelperMgr.m_CheatCheckInterval then
            LuaACHelperMgr.m_LastCheatCheckTime = time
            if CommonDefs.Is_PC_PLATFORM() then
                Main.Inst:StartCoroutine(ACHelperMgr.RefreshACHelperReportIds())
            else
                LuaACHelperMgr.SendAntiCheatCheckInfo()
            end
        end
    end
end

ACHelperMgr.m_hookIsACHelperSupported = function()
    --目前只有国服才启用了相关功能，海外版本未启用所以直接关闭
    if not CommonDefs.IS_CN_CLIENT then
        return false
    elseif CommonDefs.Is_PC_PLATFORM() then
        return ACHelperMgr.ACHelper_Open and CWordFilterMgr.Inst.sdkWordFilterInitSign
    else
        return ACHelperMgr.ACHelper_Open
    end
end