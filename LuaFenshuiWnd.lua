require("3rdParty/ScriptEvent")
require("common/common_include")

local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local CUIRes = import "L10.UI.CUIResources"
local UICamera = import "UICamera"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local Quaternion = import "UnityEngine.Quaternion"
local CZhuJueJuQingMgr = import "L10.Game.CZhuJueJuQingMgr"
local CUIFx = import "L10.UI.CUIFx"
local MessageMgr = import "L10.Game.MessageMgr"

CLuaFenshuiWnd=class()

RegistClassMember(CLuaFenshuiWnd,"Bottle1")
RegistClassMember(CLuaFenshuiWnd,"Bottle2")
RegistClassMember(CLuaFenshuiWnd,"ProgressBar1")
RegistClassMember(CLuaFenshuiWnd,"ProgressBar2")
RegistClassMember(CLuaFenshuiWnd,"PosGo1")
RegistClassMember(CLuaFenshuiWnd,"PosGo2")
RegistClassMember(CLuaFenshuiWnd,"ClearButton")
RegistClassMember(CLuaFenshuiWnd,"FullButton")
RegistClassMember(CLuaFenshuiWnd,"CloseButton")
RegistClassMember(CLuaFenshuiWnd,"Shadow1")
RegistClassMember(CLuaFenshuiWnd,"Shadow2")
RegistClassMember(CLuaFenshuiWnd,"ChosenBG1")
RegistClassMember(CLuaFenshuiWnd,"ChosenBG2")
RegistClassMember(CLuaFenshuiWnd,"Daojiu1")
RegistClassMember(CLuaFenshuiWnd,"Daojiu2")
RegistClassMember(CLuaFenshuiWnd,"Daojiu3")
RegistClassMember(CLuaFenshuiWnd,"Daojiu4")
RegistClassMember(CLuaFenshuiWnd,"FullSprite1")
RegistClassMember(CLuaFenshuiWnd,"FullSprite2")
RegistClassMember(CLuaFenshuiWnd,"FullLabel1")
RegistClassMember(CLuaFenshuiWnd,"FullLabel2")
RegistClassMember(CLuaFenshuiWnd,"HintArea")
RegistClassMember(CLuaFenshuiWnd,"HintHand")
RegistClassMember(CLuaFenshuiWnd,"AnimationCurve")
RegistClassMember(CLuaFenshuiWnd,"YellowColor")
RegistClassMember(CLuaFenshuiWnd,"GreyColor")
RegistClassMember(CLuaFenshuiWnd,"HintAnimationCurve")


RegistClassMember(CLuaFenshuiWnd,"startPoint")
RegistClassMember(CLuaFenshuiWnd,"BottleOriPos1")
RegistClassMember(CLuaFenshuiWnd,"BottleOriPos2")
RegistClassMember(CLuaFenshuiWnd,"Curve")
RegistClassMember(CLuaFenshuiWnd,"focusIndex")
RegistClassMember(CLuaFenshuiWnd,"Opened")
RegistClassMember(CLuaFenshuiWnd,"animationing")

RegistClassMember(CLuaFenshuiWnd,"oriPos1")
RegistClassMember(CLuaFenshuiWnd,"oriPos2")
RegistClassMember(CLuaFenshuiWnd,"trans1")
RegistClassMember(CLuaFenshuiWnd,"trans2")
function CLuaFenshuiWnd:Awake()
	self.startPoint = Vector2.zero
	self.BottleOriPos1 = Vector3.zero
	self.BottleOriPos2 = Vector3.zero
	self.Curve = nil
	self.focusIndex = 1
	self.Opened = 0
	self.animationing = false

	self.oriPos1 = Vector3.zero
	self.oriPos2 = Vector3.zero
	self.trans1 = nil
	self.trans2 = nil
end

function CLuaFenshuiWnd:Init()

	if self.Opened == 0 then 
		self.oriPos1 = self.Bottle1.transform.localPosition
		self.oriPos2 = self.Bottle2.transform.localPosition
		self.trans1 = self.Bottle1.transform
		self.trans2 = self.Bottle2.transform
		self.Curve = self.AnimationCurve
		self.BottleOriPos1 = self.Bottle1.transform.localPosition
		self.BottleOriPos2 = self.Bottle2.transform.localPosition
		self.animationing = false

		local function onPress(go,flag)
			
			if flag then 
				self.startPoint = UICamera.currentTouch.pos
				if go == self.Bottle1 then 
					self.trans1.localPosition = self.oriPos1
					self.trans1.localRotation = Vector3(0,0,0)
				else
					self.trans2.localPosition = self.oriPos2
					self.trans2.localRotation = Vector3(0,0,0)
				end
			else
				local endPoint = UICamera.currentTouch.pos
				local moveX = endPoint.x - self.startPoint.x
				if go == self.Bottle1 then 
					if moveX > 150 and not self.animationing then 
					
						self:tweenToPos(go)
					else
						self.focusIndex = 1
						self:refreshFocus()
					end
				--	self:judgeHit(go,self.Collider2)
				else
					if moveX < -150 and not self.animationing  then 
						self:tweenToPos(go)
					else
						self.focusIndex = 2
						self:refreshFocus()
					end
				--	self:judgeHit(go,self.Collider1)
				end 
			end	
			self:hideHintAnimation()
		end
		local onClick = function(go)
			if self.animationing then
				return
			end
			if go == self.ClearButton then 
				if self.focusIndex == 1 then 
				self:FenshuiOpera(1)
				else
				self:FenshuiOpera(2)
				end
			else			
				if self.focusIndex == 1 then 	
					if self:FenshuiOpera(3) then
						self:showDaojiuAnimation(1)
					end
				else
					if self:FenshuiOpera(4) then
						self:showDaojiuAnimation(2)
					end
				end
			end
			self:hideHintAnimation()
		end
		local onCloseClick = function(go)
			CUIManager.CloseUI(CUIRes.FenshuiWnd)
		end
	
		self:showHintAnimation()
		--CommonDefs.AddOnDragListener(self.Bottle1,DelegateFactory.Action_GameObject_Vector2(onBottleDrag),false)
		--CommonDefs.AddOnDragListener(self.Bottle2,DelegateFactory.Action_GameObject_Vector2(onBottleDrag),false)
		CommonDefs.AddOnPressListener(self.Bottle1,DelegateFactory.Action_GameObject_bool(onPress),false)
		CommonDefs.AddOnPressListener(self.Bottle2,DelegateFactory.Action_GameObject_bool(onPress),false)
		CommonDefs.AddOnClickListener(self.ClearButton,DelegateFactory.Action_GameObject(onClick),false)
		CommonDefs.AddOnClickListener(self.FullButton,DelegateFactory.Action_GameObject(onClick),false)
		CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)
		self:refreshFocus()
		
	end
	self:OnInfoRefresh()
	self.Opened = 1
	
end
function CLuaFenshuiWnd:showHintAnimation()
	self.HintArea:SetActive(true)
	CUICommonDef.KillTween(self.HintArea:GetHashCode())
	local width = self.HintArea:GetComponent(typeof(UIWidget)).width
	local trans = self.HintHand.transform
	local prePos = trans.localPosition
	local onUpdate = function(p)
		local pf=self.HintAnimationCurve:Evaluate(p)
		trans.localPosition = Vector3(width*(pf-0.5),prePos.y,0)
	end
	local onComplete = function()
		self:hideHintAnimation()
	end
	CUICommonDef.Tween(3,DelegateFactory.Action_float(onUpdate),DelegateFactory.Action(onComplete),1,self.HintArea:GetHashCode())
end
function CLuaFenshuiWnd:hideHintAnimation()
	self.HintArea:SetActive(false)
	CUICommonDef.KillTween(self.HintArea:GetHashCode())
end
function CLuaFenshuiWnd:showDaojiuAnimation(index)
	local fx = nil
	if index == 1 then 
		fx = self.Daojiu1:GetComponent(typeof(CUIFx))
		fx:LoadFx("fx/ui/prefab/ui_daojiu01.prefab")
	elseif index == 2 then 
		fx = self.Daojiu2:GetComponent(typeof(CUIFx))
		fx:LoadFx("fx/ui/prefab/ui_daojiu02.prefab")
	elseif index == 3 then
		if CZhuJueJuQingMgr.Instance.FenshuiData.WaterA ~= CZhuJueJuQingMgr.Instance.FenshuiData.ContainerA and CZhuJueJuQingMgr.Instance.FenshuiData.WaterB ~= 0 then
			fx = self.Daojiu3:GetComponent(typeof(CUIFx))
			fx:LoadFx("fx/ui/prefab/ui_daojiu03.prefab")
		end
	elseif index == 4 then 
		if CZhuJueJuQingMgr.Instance.FenshuiData.WaterB ~= CZhuJueJuQingMgr.Instance.FenshuiData.ContainerB and CZhuJueJuQingMgr.Instance.FenshuiData.WaterA ~= 0 then
			fx = self.Daojiu4:GetComponent(typeof(CUIFx))
			fx:LoadFx("fx/ui/prefab/ui_daojiu04.prefab")
		end
	end
end
function CLuaFenshuiWnd:hideDaojiuAnimation()
	self.Daojiu1:SetActive(false)
	self.Daojiu2:SetActive(false)
	self.Daojiu3:SetActive(false)
	self.Daojiu4:SetActive(false)
end
function CLuaFenshuiWnd:FenshuiOpera(param)
	if param == 1 and CZhuJueJuQingMgr.Instance.FenshuiData.WaterA == 0 then
		MessageMgr.Inst:ShowMessage("ALREADY_EMPTY_THE_BOTTLE",{})
		return false
	end
	if param == 2 and CZhuJueJuQingMgr.Instance.FenshuiData.WaterB == 0 then
		MessageMgr.Inst:ShowMessage("ALREADY_EMPTY_THE_BOTTLE",{})
		return false
	end
	if param == 3 and CZhuJueJuQingMgr.Instance.FenshuiData.WaterA == CZhuJueJuQingMgr.Instance.FenshuiData.ContainerA then
		MessageMgr.Inst:ShowMessage("ALREADY_FULL_THE_BOTTLE",{})
		return false
	end
	if param == 4 and CZhuJueJuQingMgr.Instance.FenshuiData.WaterB == CZhuJueJuQingMgr.Instance.FenshuiData.ContainerB then
		MessageMgr.Inst:ShowMessage("ALREADY_FULL_THE_BOTTLE",{})
		return false 
	end
	CZhuJueJuQingMgr.Instance:DoFenshuiOperation(param)
	return true
end
function CLuaFenshuiWnd:OnEnable()
	g_ScriptEvent:AddListener("OnFenshuiInfoRefresh", self, "OnInfoRefresh")
end
function CLuaFenshuiWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnFenshuiInfoRefresh", self, "OnInfoRefresh")
end
function CLuaFenshuiWnd:OnInfoRefresh()
	local progress1 = CZhuJueJuQingMgr.Instance.FenshuiData.WaterA/CZhuJueJuQingMgr.Instance.FenshuiData.ContainerA
	local progress2 = CZhuJueJuQingMgr.Instance.FenshuiData.WaterB/CZhuJueJuQingMgr.Instance.FenshuiData.ContainerB
	self:showProgressAnimation(progress1,progress2)
end
function CLuaFenshuiWnd:Update()
	if self.ProgressBar1.value > 0.999	then
		self.FullSprite1:SetActive(true)
		self.FullLabel1.color = self.YellowColor
	else
		self.FullSprite1:SetActive(false)
		self.FullLabel1.color = self.GreyColor
	end
	if self.ProgressBar2.value > 0.999 then 
		self.FullSprite2:SetActive(true)
		self.FullLabel2.color = self.YellowColor
	else
		self.FullSprite2:SetActive(false)
		self.FullLabel2.color = self.GreyColor
	end
end
function CLuaFenshuiWnd:tweenToPos(go)
	CUICommonDef.KillTween(self.gameObject:GetHashCode())	
	self.animationing = false
	local trans = go.transform
	local oriPos = go.transform.localPosition
	local oriRotation = Vector3.zero
	local targetPos = nil
	local targetRotation = nil 
	if go == self.Bottle1 then 
		targetPos = self.PosGo1.transform.localPosition
		targetRotation = self.PosGo1.transform.localRotation.eulerAngles
		self.Shadow1:SetActive(false)
		self.ChosenBG1:SetActive(false)
	else
		targetPos = self.PosGo2.transform.localPosition
		targetRotation = self.PosGo2.transform.localRotation.eulerAngles
		self.Shadow2:SetActive(false)
		self.ChosenBG2:SetActive(false)
	end
	local xT = targetPos.x - oriPos.x
	local yT = targetPos.y - oriPos.y
	local z = targetRotation.z
	if z > 180 then
		z = z -360
	end
	local rotationZT = z - oriRotation.z
	local onUpdate = function(p)
		local e = self.Curve:Evaluate(p)
		local pos = Vector3.zero
		pos.x = xT*p + oriPos.x
		pos.y = yT*e + oriPos.y
		trans.localPosition = pos
		trans.localRotation = Quaternion.Euler(Vector3(0,0,rotationZT * p + oriRotation.z))
	end
	
	local onComplete = function()
		self.Bottle1.transform.localPosition = self.BottleOriPos1
		self.Bottle1.transform.localRotation = Quaternion.Euler(Vector3.zero)
		self.Bottle2.transform.localPosition = self.BottleOriPos2
		self.Bottle2.transform.localRotation =  Quaternion.Euler(Vector3.zero)
		self.Shadow1:SetActive(true)
		self.Shadow2:SetActive(true)
		self:refreshFocus()	
		self.animationing  = false
	end
	local onMoveComplete = function()
		
		CUICommonDef.Tween(1,nil,DelegateFactory.Action(onComplete),1,self.gameObject:GetHashCode())
		self.animationing = true
		if go == self.Bottle1 then 		
			if self:FenshuiOpera(5) then 
				self:showDaojiuAnimation(4)
			end
		else
			if self:FenshuiOpera(6) then 
				self:showDaojiuAnimation(3)
			end
		end

	end
	
	CUICommonDef.Tween(1,DelegateFactory.Action_float(onUpdate),DelegateFactory.Action(onMoveComplete),1,self.gameObject:GetHashCode())
	self.animationing = true
end
function CLuaFenshuiWnd:refreshFocus()
	if self.focusIndex == 1 then 
		self.ChosenBG1:SetActive(true)
		self.ChosenBG2:SetActive(false)
	else
		self.ChosenBG1:SetActive(false)
		self.ChosenBG2:SetActive(true)
	end
end
function CLuaFenshuiWnd:showProgressAnimation(toProgress1,toProgress2)
	CUICommonDef.KillTween(self.ProgressBar1:GetHashCode())
	local current1 = self.ProgressBar1.value
	local current2 = self.ProgressBar2.value
	local p1 = toProgress1 - current1
	local p2 = toProgress2 - current2

	local onUpdate = function(p)
		self.ProgressBar1.value = p*p1 +current1
		self.ProgressBar2.value = p*p2 +current2
	end
	local onComplete = function()
		self.ProgressBar1.value = toProgress1
		self.ProgressBar2.value = toProgress2
		self:hideDaojiuAnimation()
	end	 
	CUICommonDef.Tween(1,DelegateFactory.Action_float(onUpdate),DelegateFactory.Action(onComplete),1,self.ProgressBar1:GetHashCode())
end


return CLuaFenshuiWnd

