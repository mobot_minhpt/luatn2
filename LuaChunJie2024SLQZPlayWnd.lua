local TweenRotation  = import "TweenRotation"
local Physics        = import "UnityEngine.Physics"
local CScene         = import "L10.Game.CScene"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local TweenAlpha     = import "TweenAlpha"

LuaChunJie2024SLQZPlayWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2024SLQZPlayWnd, "skillSelectRoot", "SkillSelect", GameObject)
RegistChildComponent(LuaChunJie2024SLQZPlayWnd, "playRoot", "Play", GameObject)
RegistChildComponent(LuaChunJie2024SLQZPlayWnd, "roletemplate", "RoleTemplate", GameObject)
RegistChildComponent(LuaChunJie2024SLQZPlayWnd, "roleGrid", "RoleGrid", UIGrid)
RegistChildComponent(LuaChunJie2024SLQZPlayWnd, "tipButton", "TipButton", GameObject)
RegistChildComponent(LuaChunJie2024SLQZPlayWnd, "selectTime", "SelectTime", UILabel)
RegistChildComponent(LuaChunJie2024SLQZPlayWnd, "playTime", "PlayTime", UILabel)
RegistChildComponent(LuaChunJie2024SLQZPlayWnd, "descView", "DescView", GameObject)
RegistChildComponent(LuaChunJie2024SLQZPlayWnd, "descView_Name", "Name", UILabel)
RegistChildComponent(LuaChunJie2024SLQZPlayWnd, "descView_Desc", "Desc", UILabel)
RegistChildComponent(LuaChunJie2024SLQZPlayWnd, "descView_SelectButton", "SelectButton", GameObject)
RegistChildComponent(LuaChunJie2024SLQZPlayWnd, "descView_Selected", "Selected", UILabel)
RegistChildComponent(LuaChunJie2024SLQZPlayWnd, "hint", "Hint", GameObject)
RegistChildComponent(LuaChunJie2024SLQZPlayWnd, "beadRoot", "BeadRoot", Transform)
--@endregion RegistChildComponent end

RegistClassMember(LuaChunJie2024SLQZPlayWnd, "arrowTemplate")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "arrowGrid")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "clickRegion")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "jueShengOnceVfx")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "jueShengVfx")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "baoZhaVfx")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "bianHuanVfx")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "longZhuTemplateTbl")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "myScoreLabel")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "otherScoreLabel")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "myDragonExpressions")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "otherDragonExpressions")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "buffGrid")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "buffTweens")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "closeButton")

RegistClassMember(LuaChunJie2024SLQZPlayWnd, "selectedPos")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "countDownTick")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "dragonExpressionTick")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "playJueShengTick")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "prepareEndTimeStamp")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "myPos")

RegistClassMember(LuaChunJie2024SLQZPlayWnd, "longZhuGo2Info")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "pos2PlayerId")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "pos2ArrowInfo")
RegistClassMember(LuaChunJie2024SLQZPlayWnd, "isJueShengPlayed")


function LuaChunJie2024SLQZPlayWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.tipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

	UIEventListener.Get(self.descView_SelectButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSelectButtonClick()
	end)
    --@endregion EventBind end
    self:InitComponents()
end

function LuaChunJie2024SLQZPlayWnd:InitComponents()
	local anchor = self.transform:Find("Anchor")
	self.arrowTemplate = anchor:Find("Play/ArrowTemplate").gameObject
	self.arrowTemplate:SetActive(false)
	self.roletemplate:SetActive(false)

	self.arrowGrid = anchor:Find("Play/Panel/ArrowGrid"):GetComponent(typeof(UIGrid))
	self.jueShengOnceVfx = self.playRoot.transform:Find("Panel1/Vfx/vfx_chunjie2024slqzplaywnd_yun").gameObject
	self.jueShengVfx = self.playRoot.transform:Find("Panel1/Vfx/Vfx_quanping").gameObject
	self.baoZhaVfx = self.playRoot.transform:Find("Panel1/Vfx/BaoZha").gameObject
	self.bianHuanVfx = self.playRoot.transform:Find("Panel1/Vfx/BianHuan").gameObject
	self.baoZhaVfx:SetActive(false)
	self.bianHuanVfx:SetActive(false)

	local meRoot = self.playRoot.transform:Find("Top/Me")
	self.myScoreLabel = meRoot:Find("Score"):GetComponent(typeof(UILabel))
	self.myDragonExpressions = {
		normal = meRoot:Find("Normal").gameObject,
		victory = meRoot:Find("Victory").gameObject,
		fail = meRoot:Find("Fail").gameObject
	}
	self:SetDragonExpression(true, "normal")

	local otherRoot = self.playRoot.transform:Find("Top/Other")
	self.otherScoreLabel = otherRoot:Find("Score"):GetComponent(typeof(UILabel))
	self.otherDragonExpressions = {
		normal = otherRoot:Find("Normal").gameObject,
		victory = otherRoot:Find("Victory").gameObject,
		fail = otherRoot:Find("Fail").gameObject
	}
	self:SetDragonExpression(false, "normal")

	self.hint:SetActive(true)

	self.buffGrid = self.playRoot.transform:Find("Top/Me/Grid"):GetComponent(typeof(UIGrid))
	self.buffTweens = {}
	self.buffTweens = {
		speedUp = self.buffGrid.transform:Find("SpeedUp"):GetComponent(typeof(TweenAlpha)),
		speedDown = self.buffGrid.transform:Find("SpeedDown"):GetComponent(typeof(TweenAlpha)),
		doubleScore = self.buffGrid.transform:Find("DoubleScore"):GetComponent(typeof(TweenAlpha)),
	}

	local pool = self.playRoot.transform:Find("Pool")
	self.longZhuTemplateTbl = {}
	local childCount = pool.childCount
	for i = 1, childCount do
		local child = pool:GetChild(i - 1).gameObject
		self.longZhuTemplateTbl[child.name] = child
		child:SetActive(false)
	end

	self.clickRegion = self.playRoot.transform:Find("ClickRegion").gameObject
	self.clickRegion:SetActive(true)
	UIEventListener.Get(self.clickRegion.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPlayClickRegionClick()
	end)

	self.closeButton = anchor:Find("CloseButton").gameObject
	UIEventListener.Get(self.closeButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)
end


function LuaChunJie2024SLQZPlayWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncShuangLongQiangZhuPlayerPos", self, "OnSyncShuangLongQiangZhuPlayerPos")
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:AddListener("SyncShuangLongQiangZhuEvent", self, "OnSyncShuangLongQiangZhuEvent")
end

function LuaChunJie2024SLQZPlayWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncShuangLongQiangZhuPlayerPos", self, "OnSyncShuangLongQiangZhuPlayerPos")
	g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
	g_ScriptEvent:RemoveListener("SyncShuangLongQiangZhuEvent", self, "OnSyncShuangLongQiangZhuEvent")
end

function LuaChunJie2024SLQZPlayWnd:OnSyncShuangLongQiangZhuPlayerPos()
	for i = 1, 5 do
		local child = self.roleGrid.transform:GetChild(i - 1)
		local normal = child:Find("Normal")
		local highlight = child:Find("Highlight")
		local list = {self:GetRoleChilds(normal), self:GetRoleChilds(highlight)}

		local roleInfo = LuaChunJie2024Mgr.SLQZInfo.pos2Role[i]
		for j = 1, 2 do
			if roleInfo then
				list[j].topLabel.text = roleInfo[2]
				list[j].skillIcon.gameObject:SetActive(false)
				list[j].portrait:LoadPortrait(CUICommonDef.GetPortraitName(roleInfo[3], roleInfo[4], -1), true)
			end
		end
	end
	self:UpdateDescView()
end

function LuaChunJie2024SLQZPlayWnd:OnSceneRemainTimeUpdate()
	if LuaChunJie2024Mgr.SLQZInfo.stage == "play" then
		local totalSeconds = CScene.MainScene and CScene.MainScene.ShowTime or 0
		self.playTime.text = SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
	end
end

function LuaChunJie2024SLQZPlayWnd:OnSyncShuangLongQiangZhuEvent(event, context)
	local list = g_MessagePack.unpack(context)

	if event == EnumShuangLongQiangZhuEvent.ePlayerEnter then
		self:ProcessEvent_PlayerEnter(list[1])
	elseif event == EnumShuangLongQiangZhuEvent.ePlayerLeave then
		self:ProcessEvent_PlayerLeave(list[1])
	elseif event == EnumShuangLongQiangZhuEvent.eChangeSpeed then
		self:ProcessEvent_ChangeSpeed(list)
	elseif event == EnumShuangLongQiangZhuEvent.eDoubleScore then
		self:ProcessEvent_DoubleScore(list[1])
	elseif event == EnumShuangLongQiangZhuEvent.eCastSkill then
		self:ProcessEvent_CastSkill(list[1], list[2])
	elseif event == EnumShuangLongQiangZhuEvent.eCatched then
		self:ProcessEvent_Catched(list[1], list[2])
	elseif event == EnumShuangLongQiangZhuEvent.eTransform then
		self:ProcessEvent_Transform(list[1], list[2])
	elseif event == EnumShuangLongQiangZhuEvent.eDestroyed then
		self:ProcessEvent_Destroy(list[1], list[2])
	elseif event == EnumShuangLongQiangZhuEvent.eScore then
		self:ProcessEvent_Score(list[1], list[2], list[3])
	elseif event == EnumShuangLongQiangZhuEvent.eGot then
		self:ProcessEvent_Got(list[1], list[2])
	elseif event == EnumShuangLongQiangZhuEvent.eRefreshLongZhu then
		self:ProcessEvent_RefreshLongZhu(list)
	end
end

function LuaChunJie2024SLQZPlayWnd:ProcessEvent_Score(force, score, force2ScoreInfo)
	LuaChunJie2024Mgr.SLQZInfo.force2ScoreInfo = force2ScoreInfo
	self:UpdateScore()
end

function LuaChunJie2024SLQZPlayWnd:ProcessEvent_RefreshLongZhu(longZhuInfo)
	LuaChunJie2024Mgr.SLQZInfo.longZhuInfo = longZhuInfo
	self:GenerateLongZhu()
end

function LuaChunJie2024SLQZPlayWnd:ProcessEvent_PlayerEnter(playerId)
	local playerPlayInfo = LuaChunJie2024Mgr.SLQZInfo.playerPlayInfo
	playerPlayInfo[playerId].valid = true
	self:UpdatePlayerOnlineStatus(playerId)
end

function LuaChunJie2024SLQZPlayWnd:ProcessEvent_PlayerLeave(playerId)
	local playerPlayInfo = LuaChunJie2024Mgr.SLQZInfo.playerPlayInfo
	playerPlayInfo[playerId].valid = false
	self:UpdatePlayerOnlineStatus(playerId)
end

function LuaChunJie2024SLQZPlayWnd:ProcessEvent_ChangeSpeed(list)
	local playerPlayInfo = LuaChunJie2024Mgr.SLQZInfo.playerPlayInfo
	local myId = CClientMainPlayer.Inst.Id
	local playInfo = playerPlayInfo[myId]
	table.insert(playInfo.speedInfo, list)
end

function LuaChunJie2024SLQZPlayWnd:ProcessEvent_DoubleScore(doubleEndtime)
	local force2ScoreInfo = LuaChunJie2024Mgr.SLQZInfo.force2ScoreInfo
	local myForce = LuaChunJie2024Mgr.SLQZInfo.myForce
	force2ScoreInfo[myForce].doubleEndtime = doubleEndtime
end

function LuaChunJie2024SLQZPlayWnd:ProcessEvent_CastSkill(playerId, angle)
	local myId = CClientMainPlayer.Inst.Id
	if playerId ~= myId then
		local playerPlayInfo = LuaChunJie2024Mgr.SLQZInfo.playerPlayInfo
		local pos = playerPlayInfo[playerId].pos
		self.pos2ArrowInfo[pos].status = "forward"
		local arrow = self.arrowGrid.transform:GetChild(pos - 1)
		LuaUtils.SetLocalRotationZ(arrow, angle)
		LuaUtils.SetLocalPosition(arrow, (pos - 3) * 345, 0, 0)
		arrow:GetComponent(typeof(TweenRotation)).enabled = false
	end
end

function LuaChunJie2024SLQZPlayWnd:ProcessEvent_Catched(playerId, longZhuKey)
	for go, info in pairs(self.longZhuGo2Info) do
		if info.key == longZhuKey then
			if info.caughtByPlayer == 0 then
				info.caughtByPlayer = playerId

				local playerPlayInfo = LuaChunJie2024Mgr.SLQZInfo.playerPlayInfo
				local pos = playerPlayInfo[playerId].pos
				local arrowInfo = self.pos2ArrowInfo[pos]
				if arrowInfo.status ~= "hold" then
					arrowInfo.status = "backward"
					arrowInfo.longZhuGo = go
					go.transform:Find("JiHuo").gameObject:SetActive(true)
				end

				-- 判断下是不是抓住了别人的珠子
				local myId = CClientMainPlayer.Inst.Id
				if myId ~= playerId then
					local myArrowInfo = self.pos2ArrowInfo[playerPlayInfo[myId].pos]
					if myArrowInfo.longZhuGo == go then
						myArrowInfo.longZhuGo = nil
					end
				end
			end
			break
		end
	end
end

function LuaChunJie2024SLQZPlayWnd:ProcessEvent_Transform(playerId, longZhuKey)
	local myId = CClientMainPlayer.Inst.Id
	if myId == playerId then return end

	local transformGo
	for go, info in pairs(self.longZhuGo2Info) do
		if info.key == longZhuKey then
			transformGo = go
			break
		end
	end

	if transformGo then
		self:TransformLongZhu(transformGo)
		local playerPlayInfo = LuaChunJie2024Mgr.SLQZInfo.playerPlayInfo
		local pos = playerPlayInfo[playerId].pos
		local arrowInfo = self.pos2ArrowInfo[pos]

		if arrowInfo.status ~= "hold" then
			arrowInfo.status = "backward"
		end
	end
end

function LuaChunJie2024SLQZPlayWnd:ProcessEvent_Destroy(playerId, longZhuKey)
	LuaChunJie2024Mgr.SLQZInfo.longZhuInfo[longZhuKey] = nil
	for go, info in pairs(self.longZhuGo2Info) do
		if info.key == longZhuKey then
			if go.activeSelf then
				go:SetActive(false)
				self:PlayBaoZhaVfx(go)
			end

			local playerPlayInfo = LuaChunJie2024Mgr.SLQZInfo.playerPlayInfo
			local pos = playerPlayInfo[playerId].pos
			local arrowInfo = self.pos2ArrowInfo[pos]

			if arrowInfo.status ~= "hold" then
				arrowInfo.status = "backward"
			end

			for _, arrowInfo1 in pairs(self.pos2ArrowInfo) do
				if arrowInfo1.longZhuGo == go then
					arrowInfo1.longZhuGo = nil
				end
			end
			break
		end
	end
end

function LuaChunJie2024SLQZPlayWnd:ProcessEvent_Got(playerId, longZhuKey)
	local removeGo
	for go, info in pairs(self.longZhuGo2Info) do
		if info.key == longZhuKey then
			local playerPlayInfo = LuaChunJie2024Mgr.SLQZInfo.playerPlayInfo
			local pos = playerPlayInfo[playerId].pos
			local arrowInfo = self.pos2ArrowInfo[pos]

			if arrowInfo.status ~= "hold" then
				info.isGot = true
			else
				removeGo = go
			end

			-- 判断下是不是抓住了别人的珠子
			local myId = CClientMainPlayer.Inst.Id
			if myId ~= playerId then
				local myArrowInfo = self.pos2ArrowInfo[playerPlayInfo[myId].pos]
				if myArrowInfo.longZhuGo == go then
					myArrowInfo.longZhuGo = nil
				end
			end

			-- 播放小龙的表情
			self:ClearDragonExpressionTick()
			local type = LuaChunJie2024Mgr.SLQZInfo.longZhuInfo[longZhuKey] or 0
			if type == 1 then
				self:SetDragonExpression(true, "fail")
				self.dragonExpressionTick = RegisterTickOnce(function()
					self:SetDragonExpression(true, "normal")
				end, 2000)
			elseif type > 3 then
				self:SetDragonExpression(true, "victory")
				self.dragonExpressionTick = RegisterTickOnce(function()
					self:SetDragonExpression(true, "normal")
				end, 2000)
			else
				self:SetDragonExpression(true, "normal")
			end
			break
		end
	end

	if removeGo then
		LuaChunJie2024Mgr.SLQZInfo.longZhuInfo[longZhuKey] = nil
		self.longZhuGo2Info[removeGo] = nil
		removeGo:SetActive(false)
	end
end



function LuaChunJie2024SLQZPlayWnd:Init()
	local isPlayStage = LuaChunJie2024Mgr.SLQZInfo.stage == "play"
	self.skillSelectRoot:SetActive(not isPlayStage)
	self.playRoot:SetActive(isPlayStage)
	self.closeButton:SetActive(isPlayStage)

	if isPlayStage then
		local playerPlayInfo = LuaChunJie2024Mgr.SLQZInfo.playerPlayInfo
		self.pos2PlayerId = {}
		self.pos2ArrowInfo = {}
		local myId = CClientMainPlayer.Inst.Id
		for playerId, info in pairs(playerPlayInfo) do
			self.pos2PlayerId[info.pos] = playerId
			self.pos2ArrowInfo[info.pos] = {status = "hold"}

			if playerId == myId then
				self.myPos = info.pos
			end
		end
	end

	self.selectedPos = 0
	self:InitRoles()
	self.descView:SetActive(false)

	self:InitCountDownTime()

	if isPlayStage then
		self.isJueShengPlayed = false
		self.jueShengOnceVfx:SetActive(false)
		self:GenerateLongZhu()
		self:UpdateScore()
		self:UpdateBuffs()
	end
end

function LuaChunJie2024SLQZPlayWnd:InitCountDownTime()
	self:ClearCountDownTick()
	if LuaChunJie2024Mgr.SLQZInfo.stage == "play" then
		self:OnSceneRemainTimeUpdate()
	else
		self.prepareEndTimeStamp = LuaChunJie2024Mgr.SLQZInfo.leftTime + CServerTimeMgr.Inst.timeStamp
		self:UpdateCountDownTime()
		self.countDownTick = RegisterTick(function()
			self:UpdateCountDownTime()
		end, 1000)
	end
end

function LuaChunJie2024SLQZPlayWnd:UpdateCountDownTime()
	local leftTime = math.floor(self.prepareEndTimeStamp - CServerTimeMgr.Inst.timeStamp)
	if leftTime < 0 then
		self:ClearCountDownTick()
		self.selectTime.text = ""
		return
	end

	self.selectTime.text = SafeStringFormat3(LocalString.GetString("%d秒"), leftTime)
end

function LuaChunJie2024SLQZPlayWnd:InitRoles()
	Extensions.RemoveAllChildren(self.roleGrid.transform)
	Extensions.RemoveAllChildren(self.arrowGrid.transform)

	local isPlayStage = LuaChunJie2024Mgr.SLQZInfo.stage == "play"
	local mainPlayer = CClientMainPlayer.Inst
	for i = 1, 5 do
		local child = NGUITools.AddChild(self.roleGrid.gameObject, self.roletemplate)
		child:SetActive(true)

		local extraLabel = child.transform:Find("ExtraLabel"):GetComponent(typeof(UILabel))
		extraLabel.text = ""
		local normal = child.transform:Find("Normal")
		local highlight = child.transform:Find("Highlight")
		local list = {self:GetRoleChilds(normal), self:GetRoleChilds(highlight)}

		local designData = ChunJie2024_DoubleDragonRole.GetData(i)
		for j = 1, 2 do
			list[j].skillName.text = designData.SkillName
		end

		normal.gameObject:SetActive(true)
		highlight.gameObject:SetActive(false)
		if isPlayStage then
			local arrowChild = NGUITools.AddChild(self.arrowGrid.gameObject, self.arrowTemplate)

			local playerId = self.pos2PlayerId[i]
			if playerId then
				local playInfo = LuaChunJie2024Mgr.SLQZInfo.playerPlayInfo[playerId]
				local isMe = playerId == mainPlayer.Id
				for j = 1, 2 do
					list[j].topLabel.text = playInfo.name
					list[j].skillIcon.gameObject:SetActive(false)
					list[j].portrait:LoadPortrait(CUICommonDef.GetPortraitName(playInfo.class, playInfo.gender, -1), true)
					LuaUtils.SetLocalPositionZ(list[j].portrait.transform, playInfo.valid and 0 or -1)
				end

				if isMe then
					normal.gameObject:SetActive(false)
					highlight.gameObject:SetActive(true)
					list[2].topLabel.color = NGUIText.ParseColor24("ffff00", 0)
				end

				extraLabel.text = playInfo.valid and "" or LocalString.GetString("(离开)")
				arrowChild:SetActive(true)
				arrowChild:GetComponent(typeof(TweenRotation)).enabled = true

				local role = ChunJie2024_DoubleDragonRole.GetData(i).Role
				arrowChild.transform:Find("CaiZhu").gameObject:SetActive(role == EnumShuangLongQiangZhuRole.eCaiZhu)
				arrowChild.transform:Find("BaoPo").gameObject:SetActive(role == EnumShuangLongQiangZhuRole.eBaoPo)
				arrowChild.transform:Find("DianJin").gameObject:SetActive(role == EnumShuangLongQiangZhuRole.eDianJin)
			else
				for j = 1, 2 do
					list[j].topLabel.text = ""
					list[j].skillIcon.gameObject:SetActive(false)
					list[j].portrait:LoadMaterial("")
				end
				extraLabel.text = ""
				arrowChild:SetActive(false)
			end
		else
			local roleInfo = LuaChunJie2024Mgr.SLQZInfo.pos2Role[i]
			local topLabelTxt = roleInfo and roleInfo[2] or LocalString.GetString("待选择")
			local iconMatPath = designData.Icon .. ".mat"
			for j = 1, 2 do
				list[j].topLabel.text = topLabelTxt
				if roleInfo then
					list[j].skillIcon.gameObject:SetActive(false)
					list[j].portrait:LoadPortrait(CUICommonDef.GetPortraitName(roleInfo[3], roleInfo[4], -1), true)
				else
					list[j].skillIcon.gameObject:SetActive(true)
					list[j].skillIcon:LoadMaterial(iconMatPath)
					list[j].portrait:LoadMaterial("")
				end
			end

			UIEventListener.Get(normal.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
				self:OnPosClick(i)
			end)
		end
	end

	self.roleGrid:Reposition()
	self.arrowGrid:Reposition()
end

function LuaChunJie2024SLQZPlayWnd:GetRoleChilds(root)
	return {
		portrait = root:Find("Portrait"):GetComponent(typeof(CUITexture)),
		skillIcon = root:Find("Icon/Skill"):GetComponent(typeof(CUITexture)),
		skillName = root:Find("SkillName/Label"):GetComponent(typeof(UILabel)),
		topLabel = root:Find("TopLabel"):GetComponent(typeof(UILabel))
	}
end

-- 玩家在线状态改变
function LuaChunJie2024SLQZPlayWnd:UpdatePlayerOnlineStatus(playerId)
	local playInfo = LuaChunJie2024Mgr.SLQZInfo.playerPlayInfo[playerId]
	local pos = playInfo.pos
	local roleChild = self.roleGrid.transform:GetChild(pos - 1)
	local valid = playInfo.valid
	local arrowInfo = self.pos2ArrowInfo[pos]
	if arrowInfo.status == "forward" and not valid then
		arrowInfo.status = "backward"
	end
	LuaUtils.SetLocalPositionZ(roleChild:Find("Normal/Portrait"), valid and 0 or -1)
	LuaUtils.SetLocalPositionZ(roleChild:Find("Highlight/Portrait"), valid and 0 or -1)
	roleChild:Find("ExtraLabel"):GetComponent(typeof(UILabel)).text = valid and "" or LocalString.GetString("(离开)")
end

-- 更新分数
function LuaChunJie2024SLQZPlayWnd:UpdateScore()
	local scoreInfo = LuaChunJie2024Mgr.SLQZInfo.force2ScoreInfo
	local myForce = LuaChunJie2024Mgr.SLQZInfo.myForce
	local otherForce = myForce == EnumCommonForce_lua.eDefend and EnumCommonForce_lua.eAttack or EnumCommonForce_lua.eDefend

	local myScore = scoreInfo[myForce].score
	local otherSCore = scoreInfo[otherForce].score

	self.myScoreLabel.text = myScore
	self.otherScoreLabel.text = otherSCore

	local settingData = ChunJie2024_DoubleDragonSetting.GetData()
	local gamePointScore = settingData.GamePointScore or 800
	local reachGamePoint = myScore >= gamePointScore or otherSCore >= gamePointScore
	if not reachGamePoint then self.jueShengVfx:SetActive(false) end
	if reachGamePoint and not self.isJueShengPlayed then
		self.jueShengOnceVfx:SetActive(true)
		self.isJueShengPlayed = true
		self:ClearPlayJueShengTick()
		self.playJueShengTick = RegisterTickOnce(function()
			self.jueShengVfx:SetActive(true)
		end, 2000)
	end

	if myScore >= settingData.WinScore then
		self:SetDragonExpression(true, "victory")
		self:SetDragonExpression(false, "fail")
	elseif otherSCore >= settingData.WinScore then
		self:SetDragonExpression(false, "victory")
		self:SetDragonExpression(true, "fail")
	end
end

-- 生成龙珠
function LuaChunJie2024SLQZPlayWnd:GenerateLongZhu()
	Extensions.RemoveAllChildren(self.beadRoot)

	local startPos = {-878, -274}
	local interval = {175, 166}

	local longZhuInfo = LuaChunJie2024Mgr.SLQZInfo.longZhuInfo
	self.longZhuGo2Info = {}
	for longZhuKey, id in pairs(longZhuInfo) do
		local y = longZhuKey % 100
		local x = math.floor(longZhuKey / 100)

		local template = self.longZhuTemplateTbl[ChunJie2024_DoubleDragonBall.GetData(id).Template]
		local go = NGUITools.AddChild(self.beadRoot.gameObject, template)
		go:SetActive(true)
		go.transform:Find("JiHuo").gameObject:SetActive(false)

		LuaUtils.SetLocalPosition(go.transform, startPos[1] + interval[1] * (y - 1), startPos[2] - interval[2] * (x - 1), 0)
		self.longZhuGo2Info[go] = {key = longZhuKey, caughtByPlayer = 0}
	end
end

function LuaChunJie2024SLQZPlayWnd:Update()
	if LuaChunJie2024Mgr.SLQZInfo.stage ~= "play" then return end

	local mainPlayer = CClientMainPlayer.Inst
	if not mainPlayer then return end

	local myId = mainPlayer.Id
	self:UpdateBuffs()

	for pos, arrowInfo in pairs(self.pos2ArrowInfo) do
		local status = arrowInfo.status
		local role = ChunJie2024_DoubleDragonRole.GetData(pos).Role
		if status ~= "hold" then
			local arrow = self.arrowGrid:GetChild(pos - 1)
			local arrowLocalPos = arrow.localPosition
			local arrowAngle = arrow.localEulerAngles.z * math.pi / 180
			if status == "forward" then
				local hitGo = pos == self.myPos and self:DetectHit(role, arrow) or nil
				if hitGo then
					local longZhuKey = self.longZhuGo2Info[hitGo].key
					if role == EnumShuangLongQiangZhuRole.eBaoPo then
						Gac2Gas.SyncShuangLongXiZhuEvent(EnumShuangLongQiangZhuEvent.eDestroyed, longZhuKey, g_MessagePack.pack({myId, longZhuKey}))
						hitGo.gameObject:SetActive(false)
						self:PlayBaoZhaVfx(hitGo)
					elseif role == EnumShuangLongQiangZhuRole.eCaiZhu then
						Gac2Gas.SyncShuangLongXiZhuEvent(EnumShuangLongQiangZhuEvent.eCatched, longZhuKey, g_MessagePack.pack({myId, longZhuKey}))
						arrowInfo.longZhuGo = hitGo
						hitGo.transform:Find("JiHuo").gameObject:SetActive(true)
					elseif role == EnumShuangLongQiangZhuRole.eDianJin then
						Gac2Gas.SyncShuangLongXiZhuEvent(EnumShuangLongQiangZhuEvent.eTransform, longZhuKey, g_MessagePack.pack({myId, longZhuKey}))
						self:TransformLongZhu(hitGo)
					end
					arrowInfo.status = "backward"
				else
					if self:CheckOut(arrow) then
						arrowInfo.status = "backward"
					else
						local deltaDistance = self:GetSpeed(pos, true, 0) * Time.deltaTime
						LuaUtils.SetLocalPosition(arrow, arrowLocalPos.x + deltaDistance * math.sin(arrowAngle), arrowLocalPos.y - deltaDistance * math.cos(arrowAngle), 0)
					end
				end
			else
				local longZhuGo = arrowInfo.longZhuGo
				if arrowLocalPos.y > 0 then
					arrowInfo.status = "hold"
					arrow:GetComponent(typeof(TweenRotation)).enabled = true
					if longZhuGo then
						arrowInfo.longZhuGo.gameObject:SetActive(false)
						arrowInfo.longZhuGo = nil
						if pos == self.myPos then
							local longZhuKey = self.longZhuGo2Info[longZhuGo].key
							Gac2Gas.SyncShuangLongXiZhuEvent(EnumShuangLongQiangZhuEvent.eGot, longZhuKey, g_MessagePack.pack({myId, longZhuKey}))
						end

						if self.longZhuGo2Info[longZhuGo].isGot then
							local longZhuKey = self.longZhuGo2Info[longZhuGo].key
							self.longZhuGo2Info[longZhuGo] = nil
							LuaChunJie2024Mgr.SLQZInfo.longZhuInfo[longZhuKey] = nil
						end
					end
					LuaUtils.SetLocalPosition(arrow, (pos - 3) * 345, 0, 0)
				else
					local longZhuId = 0
					if longZhuGo then
						local longZhuKey = self.longZhuGo2Info[longZhuGo].key
						longZhuId = LuaChunJie2024Mgr.SLQZInfo.longZhuInfo[longZhuKey]
					end
					local deltaDistance = self:GetSpeed(pos, false, longZhuId) * Time.deltaTime
					LuaUtils.SetLocalPosition(arrow, arrowLocalPos.x - deltaDistance * math.sin(arrowAngle), arrowLocalPos.y + deltaDistance * math.cos(arrowAngle), 0)
				end

				if longZhuGo then
					local localPos = self.beadRoot:InverseTransformPoint(arrow.position)
					local widget = longZhuGo.transform:GetComponent(typeof(UIWidget))
					local radius = widget.width / 2
					LuaUtils.SetLocalPosition(longZhuGo.transform, localPos.x + radius * math.sin(arrowAngle), localPos.y - radius * math.cos(arrowAngle), 0)
				end
			end
		end
	end
end

-- 更新我方拥有的buff
function LuaChunJie2024SLQZPlayWnd:UpdateBuffs()
	local mainPlayer = CClientMainPlayer.Inst
	if not mainPlayer then return end

	local now = CServerTimeMgr.Inst.timeStamp
	local myForce = LuaChunJie2024Mgr.SLQZInfo.myForce
	local doubleEndTime = LuaChunJie2024Mgr.SLQZInfo.force2ScoreInfo[myForce].doubleEndtime
	local doubleLeftTime = doubleEndTime - now
	self.buffTweens.doubleScore.gameObject:SetActive(doubleLeftTime > 0)
	self.buffTweens.doubleScore.enabled = doubleLeftTime > 0 and doubleLeftTime <= 5
	local doubleScoreWidget = self.buffTweens.doubleScore.transform:GetComponent(typeof(UIWidget))
	if doubleLeftTime > 5 and doubleScoreWidget.alpha ~= 1 then doubleScoreWidget.alpha = 1 end

	local playInfo = LuaChunJie2024Mgr.SLQZInfo.playerPlayInfo[mainPlayer.Id]
	local speedInfo = playInfo.speedInfo
	local speedUpLeftTime = 0
	local speedDownLeftTime = 0
	for _, info in pairs(speedInfo) do
		local leftTime = info[2] - now
		if leftTime > 0 then
			if info[1] > 0 and leftTime > speedUpLeftTime then
				speedUpLeftTime = leftTime
			elseif info[1] < 0 and leftTime > speedDownLeftTime then
				speedDownLeftTime = leftTime
			end
		end
	end

	self.buffTweens.speedUp.gameObject:SetActive(speedUpLeftTime > 0)
	self.buffTweens.speedDown.gameObject:SetActive(speedDownLeftTime > 0)
	self.buffTweens.speedUp.enabled = speedUpLeftTime > 0 and speedUpLeftTime <= 5
	self.buffTweens.speedDown.enabled = speedDownLeftTime > 0 and speedDownLeftTime <= 5
	local speedUpWidget = self.buffTweens.speedUp.transform:GetComponent(typeof(UIWidget))
	if speedUpLeftTime > 5 and speedUpWidget.alpha ~= 1 then speedUpWidget.alpha = 1 end
	local speedDownWidget = self.buffTweens.speedDown.transform:GetComponent(typeof(UIWidget))
	if speedDownLeftTime > 5 and speedDownWidget.alpha ~= 1 then speedDownWidget.alpha = 1 end
	self.buffGrid:Reposition()
end

-- 获取当前的钩子速度
function LuaChunJie2024SLQZPlayWnd:GetSpeed(pos, isForward, longZhuId)
	local playerId = self.pos2PlayerId[pos]
	local playInfo = LuaChunJie2024Mgr.SLQZInfo.playerPlayInfo[playerId]
	local speedInfo = playInfo.speedInfo
	local speedUp = 0
	local speedDown = 0
	local now = CServerTimeMgr.Inst.timeStamp
	for _, info in pairs(speedInfo) do
		if now < info[2] then
			if info[1] > 0 then
				speedUp = info[1]
			elseif info[1] < 0 then
				speedDown = info[1]
			end
		end
	end

	local roleData = ChunJie2024_DoubleDragonRole.GetData(pos)
	local baseSpeed = isForward and roleData.ForwardSpeed or roleData.BackwardSpeed
	local longZhuReducedSpeed = longZhuId > 0 and ChunJie2024_DoubleDragonBall.GetData(longZhuId).ReducedSpeed or 0
	return baseSpeed + speedUp + speedDown - longZhuReducedSpeed
end

-- 检测碰撞
function LuaChunJie2024SLQZPlayWnd:DetectHit(role, arrow)
	local longZhuInfo = LuaChunJie2024Mgr.SLQZInfo.longZhuInfo

	local points = {arrow:Find("Point1"), arrow:Find("Point2"), arrow:Find("Point3")}
	for _, point in pairs(points) do
		local ray = UICamera.currentCamera:ScreenPointToRay(UICamera.currentCamera:WorldToScreenPoint(point.position))
		local mask = UICamera.currentCamera.cullingMask
		local hits = Physics.RaycastAll(ray, System.Int32.MaxValue, mask)
		local count = hits.Length
		for j = 1, count do
			local go = hits[j - 1].collider.gameObject
			local info = self.longZhuGo2Info[go]
			if info then
				if role == EnumShuangLongQiangZhuRole.eBaoPo then
					return go
				elseif role == EnumShuangLongQiangZhuRole.eCaiZhu then
					if info.caughtByPlayer == 0 then return go end
				elseif role == EnumShuangLongQiangZhuRole.eDianJin then
					local longZhuId = longZhuInfo[info.key]
					if ChunJie2024_DoubleDragonBall.GetData(longZhuId).TransformTarget > 0 then return go end
				end
			end
		end
	end
end

-- 检测钩子到了最大范围
function LuaChunJie2024SLQZPlayWnd:CheckOut(arrow)
	local pos = self.transform:InverseTransformPoint(arrow:Find("Point1").position)
	local maxX = 920
	local minX = -920
	local minY = -500

	if pos.x > maxX or pos.x < minX or pos.y < minY then
		return true
	end
end

-- 转换龙珠
function LuaChunJie2024SLQZPlayWnd:TransformLongZhu(go)
	local info = self.longZhuGo2Info[go]
	if not info then return end

	local longZhuInfo = LuaChunJie2024Mgr.SLQZInfo.longZhuInfo
	local longZhuId = longZhuInfo[info.key]
	if not longZhuId then return end

	local target = ChunJie2024_DoubleDragonBall.GetData(longZhuId).TransformTarget
	if target == 0 then return end

	local template = self.longZhuTemplateTbl[ChunJie2024_DoubleDragonBall.GetData(target).Template]
	local child = NGUITools.AddChild(self.beadRoot.gameObject, template)
	child:SetActive(true)
	child.transform.localPosition = go.transform.localPosition
	go:SetActive(false)
	self.longZhuGo2Info[child] = self.longZhuGo2Info[go]
	self.longZhuGo2Info[go] = nil
	longZhuInfo[info.key] = target

	local isCaught = false
	for _, arrowInfo in pairs(self.pos2ArrowInfo) do
		if arrowInfo.longZhuGo == go then
			isCaught = true
			arrowInfo.longZhuGo = child
			break
		end
	end
	child.transform:Find("JiHuo").gameObject:SetActive(isCaught)

	self.bianHuanVfx.transform.localPosition = go.transform.localPosition
	self.bianHuanVfx:SetActive(false)
	self.bianHuanVfx:SetActive(true)
	local isBigBall = longZhuId == 3 or longZhuId == 6 or longZhuId == 7
	self.bianHuanVfx.transform:Find("Big").gameObject:SetActive(isBigBall)
	self.bianHuanVfx.transform:Find("Small").gameObject:SetActive(not isBigBall)
end

function LuaChunJie2024SLQZPlayWnd:PlayBaoZhaVfx(go)
	local info = self.longZhuGo2Info[go]
	if not info then return end

	local longZhuInfo = LuaChunJie2024Mgr.SLQZInfo.longZhuInfo
	local longZhuId = longZhuInfo[info.key]
	if not longZhuId then return end

	self.baoZhaVfx:SetActive(false)
	self.baoZhaVfx:SetActive(true)
	self.baoZhaVfx.transform.localPosition = go.transform.localPosition
	local isBigBall = longZhuId == 3 or longZhuId == 6 or longZhuId == 7
	self.baoZhaVfx.transform:Find("Big").gameObject:SetActive(isBigBall)
	self.baoZhaVfx.transform:Find("Small").gameObject:SetActive(not isBigBall)
end

function LuaChunJie2024SLQZPlayWnd:UpdateDescView()
	if self.selectedPos > 0 then
		local roleInfo = LuaChunJie2024Mgr.SLQZInfo.pos2Role[self.selectedPos]
		local isSelected = roleInfo ~= nil
		self.descView_SelectButton:SetActive(not isSelected)
		self.descView_Selected.gameObject:SetActive(isSelected)
		if isSelected then self.descView_Selected.text = SafeStringFormat3(LocalString.GetString("%s 已选择"), roleInfo[2]) end
	end
end

-- 设置小龙的表情
function LuaChunJie2024SLQZPlayWnd:SetDragonExpression(isMe, status)
	if isMe then
		self.myDragonExpressions.normal:SetActive(status == "normal")
		self.myDragonExpressions.victory:SetActive(status == "victory")
		self.myDragonExpressions.fail:SetActive(status == "fail")
	else
		self.otherDragonExpressions.normal:SetActive(status == "normal")
		self.otherDragonExpressions.victory:SetActive(status == "victory")
		self.otherDragonExpressions.fail:SetActive(status == "fail")
	end
end

function LuaChunJie2024SLQZPlayWnd:ClearCountDownTick()
	if self.countDownTick then
		UnRegisterTick(self.countDownTick)
		self.countDownTick = nil
	end
end

function LuaChunJie2024SLQZPlayWnd:ClearDragonExpressionTick()
	if self.dragonExpressionTick then
		UnRegisterTick(self.dragonExpressionTick)
		self.dragonExpressionTick = nil
	end
end

function LuaChunJie2024SLQZPlayWnd:ClearPlayJueShengTick()
	if self.playJueShengTick then
		UnRegisterTick(self.playJueShengTick)
		self.playJueShengTick = nil
	end
end

function LuaChunJie2024SLQZPlayWnd:OnDestroy()
	self:ClearCountDownTick()
	self:ClearDragonExpressionTick()
end

--@region UIEvent

function LuaChunJie2024SLQZPlayWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("CHUNJIE_2024_SLQZ_PLAY_TIP")
end

function LuaChunJie2024SLQZPlayWnd:OnSelectButtonClick()
	if LuaChunJie2024Mgr.SLQZInfo.stage == "prepare" and self.selectedPos > 0 then
		Gac2Gas.SelectRoleInShuangLongQiangZhu(self.selectedPos)
	end
end

function LuaChunJie2024SLQZPlayWnd:OnPlayClickRegionClick()
	local mainPlayer = CClientMainPlayer.Inst
	if not mainPlayer then return end

	local myId = mainPlayer.Id
	self.hint:SetActive(false)
	local pos = LuaChunJie2024Mgr.SLQZInfo.playerPlayInfo[myId].pos

	local arrowInfo = self.pos2ArrowInfo[pos]
	if arrowInfo.status ~= "hold" then return end

	arrowInfo.status = "forward"
	local arrow = self.arrowGrid:GetChild(pos - 1)
	arrow.transform:GetComponent(typeof(TweenRotation)).enabled = false

	Gac2Gas.SyncShuangLongXiZhuEvent(EnumShuangLongQiangZhuEvent.eCastSkill, 0, g_MessagePack.pack({myId, math.floor(arrow.localEulerAngles.z)}))
end

function LuaChunJie2024SLQZPlayWnd:OnPosClick(i)
    if LuaChunJie2024Mgr.SLQZInfo.stage == "prepare" then
		if self.selectedPos == i then return end

		if self.selectedPos > 0 then
			local child = self.roleGrid.transform:GetChild(self.selectedPos - 1)
			child:Find("Normal").gameObject:SetActive(true)
			child:Find("Highlight").gameObject:SetActive(false)
		end

		self.selectedPos = i
		local child = self.roleGrid.transform:GetChild(i - 1)
		child:Find("Normal").gameObject:SetActive(false)
		child:Find("Highlight").gameObject:SetActive(true)

		self.descView:SetActive(true)
		LuaUtils.SetLocalPositionX(self.descView.transform, child.transform.localPosition.x)

		local designData = ChunJie2024_DoubleDragonRole.GetData(i)
		self.descView_Name.text = designData.SkillName
		self.descView_Desc.text = designData.SkillDesc
		self:UpdateDescView()
	end
end

function LuaChunJie2024SLQZPlayWnd:OnCloseButtonClick()
	g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("CHUNJIE2024_SLQZ_CLOSE_WND_CONFIRM"), function()
		CUIManager.CloseUI(CLuaUIResources.ChunJie2024SLQZPlayWnd)
	end, nil, nil, nil, false)
end

--@endregion UIEvent
