local CRenderObject = import "L10.Engine.CRenderObject"
local CClientObjectRoot = import "L10.Game.CClientObjectRoot"
local Utility = import "L10.Engine.Utility"
local LayerDefine = import "L10.Engine.LayerDefine"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CSkillMgr = import "L10.Game.CSkillMgr"
local CClientFlag = import "L10.Game.CClientFlag"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CEffectMgr = import "L10.Game.CEffectMgr"
LuaSimpleFlagMgr = {}
LuaSimpleFlagMgr.m_EngineId2ROTable = {}
LuaSimpleFlagMgr.m_EngineId2TickTable = {}
function LuaSimpleFlagMgr:CreateLuaFlag(engineId, templateId, ownerEngineId, pixelPosX, pixelPosY, pixelPosZ, maxLifeTime)
    local ro = LuaSimpleFlagMgr.m_EngineId2ROTable[engineId]
    if not ro then
        ro = CRenderObject.CreateRenderObject(CClientObjectRoot.Inst:GetParent("Flag"), "Flag")
        LuaSimpleFlagMgr.m_EngineId2ROTable[engineId] = ro
    end
    if LuaSimpleFlagMgr.m_EngineId2TickTable[engineId] then
        UnRegisterTick(LuaSimpleFlagMgr.m_EngineId2TickTable[engineId])
    end
    LuaSimpleFlagMgr.m_EngineId2TickTable[engineId] = RegisterTickOnce(function()
        self:DestroyLuaFlag(engineId)
        if self.m_EngineId2TickTable[engineId] then
            self.m_EngineId2TickTable[engineId] = nil
        end
    end, maxLifeTime * 2)
    ro.Position = Utility.PixelPos2WorldPosWithHeight(pixelPosX, pixelPosY, pixelPosZ)
    ro.Layer = LayerDefine.Flag
    local designdata = Flag_Flag.GetData(templateId)
    if not designdata then return end

    local ownerIsMainPlayer = false
    local isHorizontalFlag = designdata.IsHorizontalFlag > 0
    local owner = CClientObjectMgr.Inst:GetObject(ownerEngineId)
    if owner then
        ownerIsMainPlayer = ownerEngineId == CClientMainPlayer.Inst.EngineId
        if isHorizontalFlag then
            ro.Direction = 90 - owner.Dir
        end
    end

    if designdata.FX > 0 then
        local warnType = EnumWarnFXType.None;
        if designdata.Effect ~= "None" then
            if owner and CSkillMgr.Inst:DoEffectSetCheck(designdata.Effect, owner, CClientMainPlayer.Inst, 0) then
                warnType = EnumFromInt(EnumWarnFXType, designdata.WarnFX)
            end
        end
        if CClientFlag.s_DisableHideFx or designdata.ForceShow > 0 or ownerIsMainPlayer or not PlayerSettings.HideSkillEffect then
            local fx = CEffectMgr.Inst:AddObjectFX(designdata.FX, ro, 0, 1, 1, nil, false, warnType, Vector3.zero, Vector3.zero, nil)
            ro:AddFX("SelfFx", fx)
        end
    end
end

function LuaSimpleFlagMgr:DestroyLuaFlag(engineId)
    local ro = LuaSimpleFlagMgr.m_EngineId2ROTable[engineId]
    if ro then
        ro:Destroy()
        LuaSimpleFlagMgr.m_EngineId2ROTable[engineId] = nil
    end
end

--供C#下切场景时调用
function LuaSimpleFlagMgr:DestroyAllLuaFlags()
    if self.m_EngineId2TickTable then
        for engineId, tick in pairs(self.m_EngineId2TickTable) do
            UnRegisterTick(tick)
            self:DestroyLuaFlag(engineId)
        end
        self.m_EngineId2TickTable = {}
    end
end
