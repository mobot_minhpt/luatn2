local DelegateFactory  = import "DelegateFactory"
local QnModelPreviewer = import "L10.UI.QnModelPreviewer"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local QnCheckBox = import "L10.UI.QnCheckBox"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local CClientObject = import "L10.Game.CClientObject"

LuaBlindBoxPreviewWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaBlindBoxPreviewWnd, "QnModelPreviewer", "QnModelPreviewer", QnModelPreviewer)
RegistChildComponent(LuaBlindBoxPreviewWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaBlindBoxPreviewWnd, "SectionsBtnTemplate", "SectionsBtnTemplate", GameObject)
RegistChildComponent(LuaBlindBoxPreviewWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaBlindBoxPreviewWnd, "ShowHeadCheckBox", "ShowHeadCheckBox", QnCheckBox)
RegistChildComponent(LuaBlindBoxPreviewWnd, "ShowBodyCheckBox", "ShowBodyCheckBox", QnCheckBox)
RegistChildComponent(LuaBlindBoxPreviewWnd, "ShowTouShaCheckBox", "ShowTouShaCheckBox", QnCheckBox)
RegistChildComponent(LuaBlindBoxPreviewWnd, "TitleLabel2", "TitleLabel2", UILabel)
RegistChildComponent(LuaBlindBoxPreviewWnd, "ZuoQiTransformBtn", "ZuoQiTransformBtn", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaBlindBoxPreviewWnd,"m_IsShowHead")
RegistClassMember(LuaBlindBoxPreviewWnd,"m_IsShowBody")
RegistClassMember(LuaBlindBoxPreviewWnd,"m_IsShowTouSha")
RegistClassMember(LuaBlindBoxPreviewWnd,"m_RadioBox")
RegistClassMember(LuaBlindBoxPreviewWnd,"m_LingYuMallData")
RegistClassMember(LuaBlindBoxPreviewWnd,"m_BlindBoxData")
RegistClassMember(LuaBlindBoxPreviewWnd,"m_SelectIndex")
RegistClassMember(LuaBlindBoxPreviewWnd,"m_VehicleId")

function LuaBlindBoxPreviewWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	self.ShowHeadCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (selected)
	    self:OnShowHeadCheckBoxValueChanged(selected)
	end)

	self.ShowBodyCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (selected)
	    self:OnShowBodyCheckBoxValueChanged(selected)
	end)

	self.ShowTouShaCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (selected)
	    self:OnShowTouShaCheckBoxValueChanged(selected)
	end)

    --@endregion EventBind end
	UIEventListener.Get(self.ZuoQiTransformBtn.gameObject).onClick = DelegateFactory.VoidDelegate(
        function()
            self:OnZuoQiTransformAniBtnClick()
        end
    )
	self.ZuoQiTransformBtn.gameObject:SetActive(false)
end

function LuaBlindBoxPreviewWnd:Init()
	local itemData = Item_Item.GetData(LuaShopMallFashionPreviewMgr.blindBoxItemId)
	self.TitleLabel2.text = itemData.Name
	if string.find(itemData.Name,LocalString.GetString("礼盒")) then
		self.TitleLabel.text = LocalString.GetString("礼盒预览")
	end
	self.m_LingYuMallData = Mall_LingYuMall.GetData(LuaShopMallFashionPreviewMgr.blindBoxItemId)
	self.m_BlindBoxData = Item_BlindBox.GetData(LuaShopMallFashionPreviewMgr.blindBoxItemId)
	if not self.m_LingYuMallData then
        self.m_LingYuMallData = Mall_LingYuMallLimit.GetData(LuaShopMallFashionPreviewMgr.blindBoxItemId)
    end
	self:InitRadioBox()	
	self:InitCheckBoxs()
end

function LuaBlindBoxPreviewWnd:InitCheckBoxs()
	self.m_LingYuMallData = Mall_LingYuMall.GetData(LuaShopMallFashionPreviewMgr.blindBoxItemId)
	if not self.m_LingYuMallData then
        self.m_LingYuMallData = Mall_LingYuMallLimit.GetData(LuaShopMallFashionPreviewMgr.blindBoxItemId)
    end
	self.ShowHeadCheckBox.gameObject:SetActive(self.m_LingYuMallData.SubCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ShiZhuang) 
	self.ShowBodyCheckBox.gameObject:SetActive(self.m_LingYuMallData.SubCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ShiZhuang) 
	self.ShowHeadCheckBox:SetSelected(true,false)
	self.ShowBodyCheckBox:SetSelected(true,false)
	self.ShowTouShaCheckBox:SetSelected(true,false)
end

function LuaBlindBoxPreviewWnd:InitRadioBox()
	Extensions.RemoveAllChildren(self.Grid.transform)
	self.SectionsBtnTemplate:SetActive(false)
	self.m_QnSelectableButtons = {}
	for i = 0, self.m_BlindBoxData.List.Length - 1 do
		local obj = NGUITools.AddChild(self.Grid.gameObject,self.SectionsBtnTemplate)
		obj:SetActive(true)
	end
	self.Grid:Reposition()
	self.m_RadioBox = self.Grid.gameObject:AddComponent(typeof(QnRadioBox))
	self.m_RadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), self.Grid.transform.childCount)
	for i = 0, self.Grid.transform.childCount - 1 do
        local go = self.Grid.transform:GetChild(i).gameObject
        self.m_RadioBox.m_RadioButtons[i] = go:GetComponent(typeof(QnSelectableButton))
		self.m_RadioBox.m_RadioButtons[i].Text = self.m_BlindBoxData.Names[i]
        self.m_RadioBox.m_RadioButtons[i].OnClick = MakeDelegateFromCSFunction(self.m_RadioBox.On_Click, MakeGenericClass(Action1, QnButton), self.m_RadioBox)
    end
	self.m_RadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnBtnSelected(btn,index)
    end)
    self.m_RadioBox:ChangeTo(0, true)
end

function LuaBlindBoxPreviewWnd:OnBtnSelected(btn,index)
	self.m_SelectIndex = index
	
	local haveha = false
	local vehicleId, pos2fashionIdMap,itemIds = LuaShopMallFashionPreviewMgr:GetBlindBoxResult(index,self.m_BlindBoxData,self.m_LingYuMallData)
	local headfashionId = pos2fashionIdMap[0]
	if headfashionId then
		local fashion = Fashion_Fashion.GetData(headfashionId)
		if fashion then
			if fashion.Position == 0 then --头饰
				if CClientMainPlayer.Inst then 
					local gender = EnumToInt(CClientMainPlayer.Inst.Gender)
					haveha = LuaFashionMgr.FashionHaveHeadAlpha(headfashionId,gender)
				end
			end
		end
	end
	self.ShowTouShaCheckBox.gameObject:SetActive(haveha) 
	self.m_VehicleId = vehicleId
	self:InitQnModelPreviewer(vehicleId,pos2fashionIdMap)
	self.ZuoQiTransformBtn.gameObject:SetActive(CZuoQiMgr.IsTransformZuoQi(vehicleId))
end

function LuaBlindBoxPreviewWnd:RefreshModel(index)
	local vehicleId, pos2fashionIdMap, itemIds = LuaShopMallFashionPreviewMgr:GetBlindBoxResult(index,self.m_BlindBoxData,self.m_LingYuMallData)
	self:InitQnModelPreviewer(vehicleId,pos2fashionIdMap)
end

function LuaBlindBoxPreviewWnd:InitQnModelPreviewer(zuoqiId,pos2fashionIdMap)
	if not CClientMainPlayer.Inst then return end
	local monsterId = LuaAppearancePreviewMgr.GetCurrentTransformMonsterId()
	self.QnModelPreviewer.IsShowHighRes = true
	if zuoqiId ~= 0 then
		self.QnModelPreviewer:PreviewMainPlayer(CClientMainPlayer.Inst.AppearanceProp.HideGemFxToOtherPlayer, CClientMainPlayer.Inst.AppearanceProp.IntesifySuitId, 0, 0, 0, zuoqiId, 180, zuoqiId, monsterId)
	else
		local appear = CClientMainPlayer.Inst.AppearanceProp:Clone()
		local headfashionId = pos2fashionIdMap[0]
		if headfashionId then
			local headfashion = Fashion_Fashion.GetData(headfashionId)
			if headfashion then
				if headfashion.Position == 0 then
					appear.HeadFashionId = headfashionId
				end
			end
		end
		local bodyfashionId = pos2fashionIdMap[1]
		if bodyfashionId then
			local bodyfashion = Fashion_Fashion.GetData(bodyfashionId)
			if bodyfashion then
				if bodyfashion.Position == 1 then
					appear.BodyFashionId = bodyfashionId
				end
			end
		end
		appear.HideHeadFashionEffect = self.m_IsShowHead and 0 or 1
		appear.HideBodyFashionEffect = self.m_IsShowBody and 0 or 1
		appear.HideTouSha = self.m_IsShowTouSha and 0 or 1
		self.QnModelPreviewer:PreviewFakeAppear(appear)
	end
	LuaModelTextureLoaderMgr:SetModelCameraColor(self.QnModelPreviewer.identifier)
end
--@region UIEvent

function LuaBlindBoxPreviewWnd:OnShowHeadCheckBoxValueChanged(selected)
	self.m_IsShowHead = selected
	self:RefreshModel(self.m_SelectIndex)
end

function LuaBlindBoxPreviewWnd:OnShowBodyCheckBoxValueChanged(selected)
	self.m_IsShowBody = selected
	self:RefreshModel(self.m_SelectIndex)
end

function LuaBlindBoxPreviewWnd:OnShowTouShaCheckBoxValueChanged(selected)
	self.m_IsShowTouSha = selected
	self:RefreshModel(self.m_SelectIndex)
end

function LuaBlindBoxPreviewWnd:OnZuoQiTransformAniBtnClick()
	local data = ZuoQi_Transformers.GetData(self.m_VehicleId)
    local data2 = ZuoQi_Transformers.GetDataBySubKey("TransformID", self.m_VehicleId)
	if data then
		self.m_VehicleId = data.TransformID
    elseif data2 then
		self.m_VehicleId = data2.ID
    end
	CZuoQiMgr.ProcessZuoQiTransformAni(self.QnModelPreviewer.m_RO, self.m_VehicleId)
	CClientObject.GetInVehicleAsDriver(self.QnModelPreviewer.m_RO, self.m_VehicleId, true, nil, false, 0)
end
--@endregion UIEvent

