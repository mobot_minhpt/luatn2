local SdkU3d = import "NtUniSdk.Unity3d.SdkU3d"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CWebLinkShareMgr = import "L10.Game.CWebLinkShareMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"

LuaDeepLinkMgr = class()
LuaDeepLinkMgr.m_LinkActionName = "" -- deep link的custom action名
LuaDeepLinkMgr.m_LinkParams = {} -- custom action对应的参数列表

g_DeepLinkCustomActions={}

function LuaDeepLinkMgr:ParseLink(actionName, bFromSdk, csharpDict)
	if g_DeepLinkCustomActions[actionName] then
		self.m_LinkActionName = actionName
		g_DeepLinkCustomActions[actionName].parseFunc(self.m_LinkParams, bFromSdk, csharpDict)
	end
end

function LuaDeepLinkMgr:HasInGameAction()
	return self.m_LinkActionName and g_DeepLinkCustomActions[self.m_LinkActionName] and true or false
end

function LuaDeepLinkMgr:HasOpenPersonalSpaceAction()
	return self.m_LinkActionName == "showpersonalspace"
end

function LuaDeepLinkMgr:DoInGameAction()
	if self.m_LinkActionName and g_DeepLinkCustomActions[self.m_LinkActionName] then
		g_DeepLinkCustomActions[self.m_LinkActionName].execFunc(self.m_LinkParams)
	end
	self:ClearInGameAction() --执行完务必清空以免再次触发
end

--清除参数，设置界面切换账号时调用
function LuaDeepLinkMgr:ClearInGameAction()
	self.m_LinkActionName = nil
	self.m_LinkParams = {}
end

function LuaDeepLinkMgr:GetDictNumberValue(dict, key, defaultValue)
	if CommonDefs.DictContains_LuaCall(dict, key) then
		return tonumber(CommonDefs.DictGetValue_LuaCall(dict, key))
	else
		return defaultValue
	end
end

function LuaDeepLinkMgr:GetDictStringValue(dict, key, defaultValue)
	if CommonDefs.DictContains_LuaCall(dict, key) then
		return CommonDefs.DecodeUnicode(tostring(CommonDefs.DictGetValue_LuaCall(dict, key)))
	else
		return defaultValue
	end
end

--https://game.163.com/open/qnm/?custom_action=showweb&target_url=https%3A%2F%2Fqnm.163.com%2F2021%2Fxinfu%2F
g_DeepLinkCustomActions["showweb"] = { --打开内置浏览器，页面受控
	parseFunc = function(paramsTbl, bFromSdk, dict)
		if bFromSdk then
			paramsTbl["target_url"] = SdkU3d.getPropStr("target_url")
		else
			paramsTbl["target_url"] = LuaDeepLinkMgr:GetDictStringValue(dict, "target_url")
		end
	end,

	execFunc = function(paramsTbl)
		if paramsTbl["target_url"] then
			CWebLinkShareMgr.Inst:CheckAndOpenUrl(paramsTbl["target_url"])
		end
	end
}
--https://game.163.com/open/qnm/?custom_action=showpersonalspace&playerid=0&tab=0
g_DeepLinkCustomActions["showpersonalspace"] = { --打开梦岛
	parseFunc = function(paramsTbl, bFromSdk, dict)
		if bFromSdk then
			paramsTbl["playerid"] = tonumber(SdkU3d.getPropStr("playerid"))
			paramsTbl["tab"] = tonumber(SdkU3d.getPropStr("tab"))
		else
			paramsTbl["playerid"] = LuaDeepLinkMgr:GetDictNumberValue(dict, "playerid")
			paramsTbl["tab"] = LuaDeepLinkMgr:GetDictNumberValue(dict, "tab")
		end
	end,

	execFunc = function(paramsTbl)
		if paramsTbl["playerid"] and paramsTbl["tab"] then
			CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(paramsTbl["playerid"], paramsTbl["tab"])
		end
	end
}
--https://game.163.com/open/qnm/?custom_action=showzhoubian
g_DeepLinkCustomActions["showzhoubian"] = { --打开周边商城
	parseFunc = function(paramsTbl, bFromSdk, dict)
		--无需参数
	end,

	execFunc = function(paramsTbl)
		CLuaShopMallMgr:OpenZhouBianMallShop()
	end
}
