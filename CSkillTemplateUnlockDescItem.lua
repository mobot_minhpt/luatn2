-- Auto Generated!!
local CSkillTemplateUnlockDescItem = import "L10.UI.CSkillTemplateUnlockDescItem"
local LocalString = import "LocalString"
CSkillTemplateUnlockDescItem.m_Init_CS2LuaHook = function (this, text, achieve) 
    this.descLabel.text = text
    local default
    if achieve then
        default = LocalString.GetString("已达成")
    else
        default = LocalString.GetString("未达成")
    end
    this.achieveLabel.text = default
    this.achieveLabel.color = achieve and this.achieveColor or this.nonAchieveColor
end
