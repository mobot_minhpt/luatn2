require("3rdParty/ScriptEvent")
require("common/common_include")
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local UISlider = import "UISlider"
local Transform = import "UnityEngine.Transform"
local CUIManager = import "L10.UI.CUIManager"
local EUIModuleGroup = import "L10.UI.CUIManager+EUIModuleGroup"
local CButton = import "L10.UI.CButton"
local CUIFx = import "L10.UI.CUIFx"

CLuaNpcFightingWnd =class()
RegistChildComponent(CLuaNpcFightingWnd, "NpcRoot1", Transform)
RegistChildComponent(CLuaNpcFightingWnd, "NpcRoot2", Transform)

RegistClassMember(CLuaNpcFightingWnd, "m_Tick")
RegistClassMember(CLuaNpcFightingWnd, "m_Button1")
RegistClassMember(CLuaNpcFightingWnd, "m_Button2")
RegistClassMember(CLuaNpcFightingWnd, "m_HideObject")

CLuaNpcFightingWnd.NpcId1 = 0
CLuaNpcFightingWnd.NpcId2 = 0
CLuaNpcFightingWnd.NpcHp = {}
CLuaNpcFightingWnd.bNotShowButton = false
CLuaNpcFightingWnd.LeftFxPath  = "fx/ui/prefab/UI_shezhan_left.prefab"
CLuaNpcFightingWnd.RightFxPath = "fx/ui/prefab/UI_shezhan_right.prefab"
CLuaNpcFightingWnd.FxDuration = -1

function CLuaNpcFightingWnd:Init()
	self.m_Tick = nil
	self:InitNpcInfo(self.NpcRoot1, self.NpcId1)
	self:InitNpcInfo(self.NpcRoot2, self.NpcId2)
	self:InitBangQiangButtonInfo()
end
function CLuaNpcFightingWnd:OnEnable()
	local excepts = {"HeadInfoWnd", "MiddleNoticeCenter"}
	local List_String = MakeGenericClass(List,cs_string)
	CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
	CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)
	g_ScriptEvent:AddListener("SyncNpcFightingHp", self, "SyncNpcFightingHp")
end
function CLuaNpcFightingWnd:OnDisable()
	local excepts = {}
	local List_String = MakeGenericClass(List,cs_string)
	CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
	CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)
	g_ScriptEvent:RemoveListener("SyncNpcFightingHp", self, "SyncNpcFightingHp")
    UnRegisterTick(self.m_Tick)
end

function CLuaNpcFightingWnd:InitNpcInfo(trans, templateId)
	local data = NPC_NPC.GetData(templateId)
	if data then
		local portraitTex = trans:Find("Portrait"):GetComponent(typeof(CUITexture))
		portraitTex:LoadPortrait(data.Portrait, false)
		local nameLabel = trans:Find("NameLabel"):GetComponent(typeof(UILabel))
		nameLabel.text = data.Name
		local hpSlider = trans:Find("HpBar"):GetComponent(typeof(UISlider))
		hpSlider.value = self.NpcHp[templateId] or 1
	end
end

function CLuaNpcFightingWnd:SyncNpcFightingHp(npcId, percent)
	local trans = self.NpcRoot1
	if npcId == self.NpcId1 then
		trans = self.NpcRoot1
	else
		trans = self.NpcRoot2
	end
	if trans then
		local hpSlider = trans:Find("HpBar"):GetComponent(typeof(UISlider))
		UnRegisterTick(self.m_Tick)
		self.m_Tick = RegisterTick(function()
			hpSlider.value = Mathf.Lerp(hpSlider.value, percent, 0.33)
			if Mathf.Abs(hpSlider.value - percent) < 0.01 then
				UnRegisterTick(self.m_Tick)
			end
		end, 33)
	end
end

function CLuaNpcFightingWnd:InitBangQiangButtonInfo()
	self.m_HideObject1 = self.transform:Find("Anchor/HideObject1"):GetComponent(typeof(CUIFx))
	self.m_HideObject2 = self.transform:Find("Anchor/HideObject2"):GetComponent(typeof(CUIFx))

	self.m_Button1 = self.transform:Find("Anchor/Button1"):GetComponent(typeof(CButton))
    self.m_Button1.Enabled = true
    CommonDefs.AddOnClickListener(self.m_Button1.gameObject, DelegateFactory.Action_GameObject(function (go)
	    self.m_HideObject1:DestroyFx()
        self.m_HideObject1:LoadFx(self.LeftFxPath, self.FxDuration, true)
        self.m_Button1.Enabled = false
        self.m_Button2.Enabled = false
        Gac2Gas.NpcFightBangQiang(self.NpcId1) 
    end), false)

    self.m_Button2 = self.transform:Find("Anchor/Button2"):GetComponent(typeof(CButton))
    self.m_Button2.Enabled = true
    CommonDefs.AddOnClickListener(self.m_Button2.gameObject, DelegateFactory.Action_GameObject(function (go)
	    self.m_HideObject2:DestroyFx()
        self.m_HideObject2:LoadFx(self.RightFxPath, self.FxDuration, true)
        self.m_Button1.Enabled = false
        self.m_Button2.Enabled = false
        Gac2Gas.NpcFightBangQiang(self.NpcId2)
    end), false)

    if self.bNotShowButton then
    	self.m_Button1.gameObject:SetActive(false)
    	self.m_Button2.gameObject:SetActive(false)
    end
end


return CLuaNpcFightingWnd
