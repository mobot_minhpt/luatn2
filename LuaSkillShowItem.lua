local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaSkillShowItem = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSkillShowItem, "text", "text", UILabel)
RegistChildComponent(LuaSkillShowItem, "CommonSkill", "CommonSkill", GameObject)
RegistChildComponent(LuaSkillShowItem, "SpecialSkill", "SpecialSkill", GameObject)
RegistChildComponent(LuaSkillShowItem, "SpecialSkillItem", "SpecialSkillItem", GameObject)
RegistChildComponent(LuaSkillShowItem, "CommonSkillItem", "CommonSkillItem", GameObject)

--@endregion RegistChildComponent end

function LuaSkillShowItem:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaSkillShowItem:Init()

end

--@region UIEvent

function  LuaSkillShowItem:InfoUpdate(text)
    self.text.text = text

    local commonskill = {"Is_skill_08","Is_skill_09","Is_skill_10","Is_skill_11","Is_skill_12"};
    for i,s in ipairs(commonskill) 
    do
        local go=NGUITools.AddChild(self.CommonSkill,self.CommonSkillItem)
        go:SetActive(true)
        --local icon=go.transform:Find("SkillIcon"):GetComponent(typeof(UITexture))
        --icon.material = s
    end

    local specialskill = {"Is_skill_08","Is_skill_09"};
    for i,s in ipairs(specialskill) 
    do
        local go=NGUITools.AddChild(self.SpecialSkill,self.SpecialSkillItem)
        go:SetActive(true)
        --local icon=go.transform:Find("SkillIcon"):GetComponent(typeof(UITexture))
        --icon.material = s
    end
    
end


--@endregion UIEvent

