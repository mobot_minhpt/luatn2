local UISprite = import "UISprite"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local GameObject = import "UnityEngine.GameObject"
local UITable = import "UITable"
local UILabel = import "UILabel"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemAccessTemplate=import "L10.UI.CItemAccessTemplate"

LuaRefineMonsterItemAccessWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaRefineMonsterItemAccessWnd, "Background", "Background", UISprite)
RegistChildComponent(LuaRefineMonsterItemAccessWnd, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaRefineMonsterItemAccessWnd, "AccessTemplate", "AccessTemplate", GameObject)
RegistChildComponent(LuaRefineMonsterItemAccessWnd, "AccessList", "AccessList", UITable)
RegistChildComponent(LuaRefineMonsterItemAccessWnd, "Name", "Name", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaRefineMonsterItemAccessWnd,"m_TypeList")

function LuaRefineMonsterItemAccessWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaRefineMonsterItemAccessWnd:Init()
    if #LuaZongMenMgr.m_LingCaiTypeList >0 then
        self.m_TypeList = LuaZongMenMgr.m_LingCaiTypeList
    end

    for i=1, #LuaZongMenMgr.m_LingCaiTypeList do
        local t = LuaZongMenMgr.m_LingCaiTypeList[i]
        local data = LianHua_ItemType.GetData(t)
        if data then
            local accessGo = NGUITools.AddChild(self.AccessList.gameObject, self.AccessTemplate)
            accessGo:SetActive(true)
            local itemAccess = CommonDefs.GetComponent_GameObject_Type(accessGo, typeof(CItemAccessTemplate))
            itemAccess:Init(data.Name, LocalString.GetString("查看"), DelegateFactory.Action(function()
                g_MessageMgr:ShowMessage(data.Msg)
            end))
        end
    end
    
    self.Background.height = self.Background.height + (math.min(#self.m_TypeList, 4) * (100 + math.floor(self.AccessList.padding.y)))
    self.AccessList:Reposition()
    self.ScrollView:ResetPosition()
end

--@region UIEvent

--@endregion UIEvent

