local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
CLuaChunJieSeriesTaskRewardWnd = class()

function CLuaChunJieSeriesTaskRewardWnd:Init()

    local button = self.transform:Find("Button").gameObject

    UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CLuaUIResources.ChunJieSeriesTaskRewardWnd)
        Gac2Gas.RequestGetChunJie2020SeriesTasksReward()--请求领取奖励
    end)

    local rewards = ChunJie2020_Setting.GetData().SeriesTasksReward

    local itemTemplate = self.transform:Find("ItemCell").gameObject
    itemTemplate:SetActive(false)
    local grid = self.transform:Find("Grid").gameObject

    for i=1,rewards.Length,2 do
        local itemId = rewards[i-1]
        local design = Item_Item.GetData(itemId)
        if design then
            local go = NGUITools.AddChild(grid,itemTemplate)
            go:SetActive(true)

            local icon = go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
            icon:LoadMaterial(design.Icon)

            UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
            end)
        end
    end

    grid:GetComponent(typeof(UIGrid)):Reposition()
end