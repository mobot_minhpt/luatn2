local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local EnumCommonPlayerListUpdateType = import "L10.Game.EnumCommonPlayerListUpdateType"
local CIMMgr = import "L10.Game.CIMMgr"
local CCommonPlayerDisplayData = import "L10.Game.CCommonPlayerDisplayData"
local Utility = import "L10.Engine.Utility"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CameraMode = import "L10.Engine.CameraControl.CameraMode"
local MessageWndManager = import "L10.UI.MessageWndManager"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CPos = import "L10.Engine.CPos"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CScene = import "L10.Game.CScene"

LuaYuanXiao2020Mgr = class()
----------------------------------------
---元宵怼怼乐
----------------------------------------
LuaYuanXiao2020Mgr.rewardTotalNum = 2
LuaYuanXiao2020Mgr.receiveRewardTimes = 0
LuaYuanXiao2020Mgr.teamScore = 0
LuaYuanXiao2020Mgr.playerInfos = {}
function LuaYuanXiao2020Mgr:TakePartInDuiDuiLe()
    Gac2Gas.DuiDuiLeEnterGame()
end

function LuaYuanXiao2020Mgr:OpenSettlementWnd()
    CUIManager.ShowUI(CLuaUIResources.DuiDuiLeSettlementWnd)
end

function LuaYuanXiao2020Mgr:GetRewardNum()
    return self.rewardTotalNum - self.receiveRewardTimes
end

function LuaYuanXiao2020Mgr:GetTeamScores()
    return self.teamScore
end

function LuaYuanXiao2020Mgr:OpenRankingList()
    if IsOpenNewRankWnd() then
	    CUIManager.ShowUI(CLuaUIResources.NewRankWnd)
	else
		CUIManager.ShowUI(CUIResources.RankWnd)
	end
end

function LuaYuanXiao2020Mgr:GetReward()
    Gac2Gas.DuiDuiLeGetLastGameReward()
end

function LuaYuanXiao2020Mgr:GetRewardsProgress()
    return self.receiveRewardTimes, self.rewardTotalNum
end

function LuaYuanXiao2020Mgr:GetPlayerInfos()
    return self.playerInfos
end

function LuaYuanXiao2020Mgr:CheckInDuiDuiLeGamePlay()
    local gamePlayId = YuanXiao_DuiDuiLeSetting.GetData().GamePlayId
    if CScene.MainScene then
        return (gamePlayId ~= 0) and (CScene.MainScene.GamePlayDesignId == gamePlayId)
    end
    return false
end

function LuaYuanXiao2020Mgr:DuiDuiLeSyncPlayInfo(score, infoU)
    self.teamScore = score
    self:SetPlayerInfos(infoU)
end

function LuaYuanXiao2020Mgr:SetPlayerInfos(infoU)
    local list = MsgPackImpl.unpack(infoU)
    self.playerInfos = {}

    local j = 1
    for i = 0,list.Count - 1, 3 do
        local t = {}
        t.Weight, t.Name, t.SliderValue, t.Scale = 0,"",0,1
        t.ID, t.Weight, t.Name = tonumber(list[i]), tonumber(list[i + 1]), list[i + 2]
        table.insert(self.playerInfos, t)
		j = j + 1
    end
    table.sort(self.playerInfos,function(a,b)
        if a.Weight > b.Weight then
            return true
        end
        return false
    end )

    self:CalculatePlayerInfosScale()

    g_ScriptEvent:BroadcastInLua("YuanXiao2020_UpdatePlayerInfos",{})
end

function LuaYuanXiao2020Mgr:CalculatePlayerInfosScale()
    local formula = AllFormulas.Action_Formula[YuanXiao_DuiDuiLeSetting.GetData().ScaleFormulaId]
    if #self.playerInfos > 0 then
        local maxWeight = self.playerInfos[1].Weight
        if maxWeight > 0 then
            for i = 1,#self.playerInfos do
                self.playerInfos[i].SliderValue = self.playerInfos[i].Weight / maxWeight
                if formula and formula.Formula then
                    self.playerInfos[i].Scale = formula.Formula(nil,nil,{self.playerInfos[i].Weight})
                end
            end
        end
    end
end

function LuaYuanXiao2020Mgr:DuiDuiLePlayResult(score, remainNum, totalNum, infoU)
    self.teamScore = score
    self.receiveRewardTimes = totalNum - remainNum
    self.rewardTotalNum = totalNum
    self:SetPlayerInfos(infoU)
    LuaYuanXiao2020Mgr:OpenSettlementWnd()
end

function LuaYuanXiao2020Mgr:DuiDuiLeOpenSignUpWndResult(remainTimes, totalTimes)
    self.receiveRewardTimes = totalTimes - remainTimes
    self.rewardTotalNum = totalTimes
    g_ScriptEvent:BroadcastInLua("YuanXiao2020_DuiDuiLeOpenSignUpWnd",{})
end
----------------------------------------
---元宵放灯
----------------------------------------
LuaYuanXiao2020Mgr.fangDengFxList = {}

function LuaYuanXiao2020Mgr:ShowPlayListWnd()
    local player = CClientMainPlayer.Inst
    if not player then
        return
    end

    CCommonPlayerListMgr.Inst.WndTitle = LocalString.GetString("祈愿对象")
    CCommonPlayerListMgr.Inst.ButtonText = LocalString.GetString("选择")
    CCommonPlayerListMgr.Inst.UpdateType = EnumCommonPlayerListUpdateType.Default
    CommonDefs.ListClear(CCommonPlayerListMgr.Inst.allData)

    -- 先加自己
    local data = CreateFromClass(CCommonPlayerDisplayData, player.Id, "[00FF2B]"..player.Name.."[-]", player.Level, player.Class, player.Gender, player.BasicProp.Expression, true)
    CommonDefs.ListAdd(CCommonPlayerListMgr.Inst.allData, typeof(CCommonPlayerDisplayData), data)

    -- 再加在线好友
    local friendlinessTbl = {}
    CommonDefs.DictIterate(player.RelationshipProp.Friends, DelegateFactory.Action_object_object(function(k, v)
        if CIMMgr.Inst:IsOnline(k) and CIMMgr.Inst:IsSameServerFriend(k) then
            table.insert(friendlinessTbl, {k, v.Friendliness})
        end
    end))
    table.sort(friendlinessTbl, function(v1, v2) return v1[2] > v2[2] end)

    for _, info in pairs(friendlinessTbl) do
        local basicInfo = CIMMgr.Inst:GetBasicInfo(info[1])
        if basicInfo then
            local data = CreateFromClass(CCommonPlayerDisplayData, basicInfo.ID, basicInfo.Name, basicInfo.Level, basicInfo.Class, basicInfo.Gender, basicInfo.Expression, true)
            CommonDefs.ListAdd(CCommonPlayerListMgr.Inst.allData, typeof(CCommonPlayerDisplayData), data)
        end
    end

    CCommonPlayerListMgr.Inst.OnPlayerSelected = DelegateFactory.Action_ulong(function(playerId)
        g_ScriptEvent:BroadcastInLua("YuanXiao2020_SelectPlayer",playerId)
        CUIManager.CloseUI(CIndirectUIResources.CommonPlayerListWnd)
    end)
    CUIManager.ShowUI(CIndirectUIResources.CommonPlayerListWnd)
end

function LuaYuanXiao2020Mgr:RequestFangDeng(SelectedTargetPlayerId, SelectTargetName, SelectedTargetFangDengIndex, wishMsg)
    CUIManager.CloseUI(CLuaUIResources.YuanXiao2020QiYuanWnd)
    Gac2Gas.YuanXiaoLanternSendLantern(SelectedTargetPlayerId, SelectTargetName, SelectedTargetFangDengIndex, wishMsg)
end

function LuaYuanXiao2020Mgr:TryOpenFangDeng3D()
    local CameraView = YuanXiao_LanternSetting.GetData().CameraView
    local r, z, y = CameraView[0], CameraView[1], CameraView[2]
    local vec3 = Vector3(r, z, y)
    if CameraFollow.Inst.CurMode ~= CameraMode.E3DPlus then
        local message = g_MessageMgr:FormatMessage("YuanXiao2020_3DPlus_Confirm")
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
            CameraFollow.Inst:SetMode(CameraMode.E3DPlus)
            PlayerSettings.Saved3DMode = 2
            CameraFollow.Inst.targetRZY = vec3
            CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
        end), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
        return
    end
    CameraFollow.Inst.targetRZY = vec3
    CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
end

function LuaYuanXiao2020Mgr:TryTrackOpenFangDeng3D(sceneId, sceneTemplateId, x, y)
    local pixelPos = Utility.GridPos2PixelPos(CPos(x, y))
    CTrackMgr.Inst:Track(sceneId, sceneTemplateId, pixelPos, 0, DelegateFactory.Action(function()
        self:TryOpenFangDeng3D()
    end), nil, nil, nil, false)
end

function LuaYuanXiao2020Mgr:StopFangDeng(kind)
    for k, v in pairs(self.fangDengFxList) do
        if (kind == nil) or (k == kind) then
            for _, fx in pairs(v) do
                fx:Destroy()
            end
        end
    end
end

function LuaYuanXiao2020Mgr:StartFangDeng(kind)
    self:StopFangDeng(kind)
    self.fangDengFxList[kind] = {}
    local lanternType = YuanXiao_LanternType.GetData(kind)
    local len = lanternType.Pos.Length
    for i = 0,len - 1,2 do
        local pos, fxId = CPos(lanternType.Pos[i] * 64,lanternType.Pos[i + 1] * 64), lanternType.FxId
        local fx = CEffectMgr.Inst:AddWorldPositionFX(fxId, pos, 0, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
        table.insert(self.fangDengFxList[kind], fx)
    end
end
