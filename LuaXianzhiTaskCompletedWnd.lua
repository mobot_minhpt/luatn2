require("common/common_include")
local GameObject  = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local CUIManager = import "L10.UI.CUIManager"

CLuaXianzhiTaskCompletedWnd = class()
RegistChildComponent(CLuaXianzhiTaskCompletedWnd, "Item1", GameObject)
RegistChildComponent(CLuaXianzhiTaskCompletedWnd, "Item2", GameObject)
RegistChildComponent(CLuaXianzhiTaskCompletedWnd, "Item3", GameObject)
RegistChildComponent(CLuaXianzhiTaskCompletedWnd, "Item4", GameObject)

RegistClassMember(CLuaXianzhiTaskCompletedWnd, "Args")
RegistClassMember(CLuaXianzhiTaskCompletedWnd, "m_namepre")

function CLuaXianzhiTaskCompletedWnd:Init( ... )
	self.Args={
		[self.Item1]={},
		[self.Item2]={},
		[self.Item3]={},
		[self.Item4]={},
	}

	local xzs = CLuaXianzhiMgr.Xzs
	self:InitData(xzs[1],xzs[2],xzs[3],xzs[4])

	local click = function(go)
		self:OnItemClick(go)
	end

	CommonDefs.AddOnClickListener(self.Item1,DelegateFactory.Action_GameObject(click), false)
	CommonDefs.AddOnClickListener(self.Item2,DelegateFactory.Action_GameObject(click), false)
	CommonDefs.AddOnClickListener(self.Item3,DelegateFactory.Action_GameObject(click), false)
	CommonDefs.AddOnClickListener(self.Item4,DelegateFactory.Action_GameObject(click), false)
end

function CLuaXianzhiTaskCompletedWnd:OnItemClick(go)
	local data = self.Args[go]
	CLuaXianzhiMgr.SelectData = data
	CUIManager.ShowUI("XianzhiDescribeWnd")
end

--��ʾ��ְ����
function CLuaXianzhiTaskCompletedWnd:InitData(xz1,xz2,xz3,xz4)
	self:InitItem(self.Item1,xz1)
	self:InitItem(self.Item2,xz2)
	self:InitItem(self.Item3,xz3)
	self:InitItem(self.Item4,xz4)
end

function CLuaXianzhiTaskCompletedWnd:InitItem(itemgo,xz)
	local data = XianZhi_Title.GetData(xz)
	self.Args[itemgo] = data

	local addrtext = itemgo.transform:Find("addr"):GetComponent(typeof(UILabel))
	addrtext.text = CLuaXianzhiMgr.GetXianZhiLocationByRegID()

	local name = itemgo.transform:Find("name"):GetComponent(typeof(UILabel))
	name.text = data.TitleName

	local datatype = XianZhi_TitleType.GetData(data.TitleType)
	if datatype == nil then return end
	local icon = itemgo.transform:Find("icon"):GetComponent(typeof(CUITexture))
	icon:LoadMaterial("UI/Texture/Skill/Material/"..datatype.Icon..".mat")


end

return CLuaXianzhiTaskCompletedWnd
