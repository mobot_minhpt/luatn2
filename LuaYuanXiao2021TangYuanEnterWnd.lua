local Extensions = import "Extensions"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local UIScrollView = import "UIScrollView"

LuaYuanXiao2021TangYuanEnterWnd = class()

RegistChildComponent(LuaYuanXiao2021TangYuanEnterWnd,"m_JoinBtn","JoinBtn", GameObject)
RegistChildComponent(LuaYuanXiao2021TangYuanEnterWnd,"m_TextTable","TextTable", UITable)
RegistChildComponent(LuaYuanXiao2021TangYuanEnterWnd,"m_TextTemplate","TextTemplate", GameObject)
RegistChildComponent(LuaYuanXiao2021TangYuanEnterWnd,"m_ScrollView", "ScrollView", UIScrollView)
RegistChildComponent(LuaYuanXiao2021TangYuanEnterWnd,"m_ExchangeBtn","ExchangeBtn", GameObject)
RegistChildComponent(LuaYuanXiao2021TangYuanEnterWnd,"m_ScoreCount","ScoreCount", UILabel)
RegistChildComponent(LuaYuanXiao2021TangYuanEnterWnd,"m_JoinBtnLabel","JoinBtnLabel", UILabel)
RegistChildComponent(LuaYuanXiao2021TangYuanEnterWnd,"m_MatchingLabel","MatchingLabel", UILabel)
RegistClassMember(LuaYuanXiao2021TangYuanEnterWnd,"m_IsMatching")


function LuaYuanXiao2021TangYuanEnterWnd:Init()
    Gac2Gas.QueryTodayTangYuan2021Score()
    Gac2Gas.QueryPlayerIsMatchingTangYuan2021()

    self.m_JoinBtnLabel.text = LocalString.GetString("开始匹配")
    self.m_MatchingLabel.gameObject:SetActive(false)
    self.m_IsMatching = false

    UIEventListener.Get(self.m_JoinBtn).onClick = DelegateFactory.VoidDelegate(function()
        local lvLimit =  Yuanxiao2021_Setting.GetData().TangYuanLevel
        if CClientMainPlayer.Inst.Level < lvLimit then
            g_MessageMgr:ShowMessage("TANGYUAN_LEVEL_LOW")
            return
        end
        if not self.m_IsMatching then
            Gac2Gas.RequestSignTangYuan2021()
        else
            Gac2Gas.RequestCancelSignTangYuan2021()
        end
        RegisterTickOnce(function()
            Gac2Gas.QueryPlayerIsMatchingTangYuan2021()
        end, 250)
    end)

    UIEventListener.Get(self.m_ExchangeBtn).onClick = DelegateFactory.VoidDelegate(function()
        CLuaNPCShopInfoMgr.ShowScoreShop("TangYuan2021")
    end)

    self:InitTextTable()
end

function LuaYuanXiao2021TangYuanEnterWnd:UpdateMatching(isMatching)
    if isMatching then
        self.m_JoinBtnLabel.text = LocalString.GetString("取消匹配")
        self.m_MatchingLabel.gameObject:SetActive(true)
    else
        self.m_JoinBtnLabel.text = LocalString.GetString("开始匹配")
        self.m_MatchingLabel.gameObject:SetActive(false)
    end
    self.m_IsMatching = isMatching
end

function LuaYuanXiao2021TangYuanEnterWnd:OnEnable()
    g_ScriptEvent:AddListener("TodayTangYuanScore", self, "InitTodayScore")
    g_ScriptEvent:AddListener("PlayerIsMatchingTangYuan2021_Result", self, "UpdateMatching")
end

function LuaYuanXiao2021TangYuanEnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("TodayTangYuanScore", self, "InitTodayScore")
    g_ScriptEvent:RemoveListener("PlayerIsMatchingTangYuan2021_Result", self, "UpdateMatching")
end

function LuaYuanXiao2021TangYuanEnterWnd:InitTodayScore(score)
    self.m_ScoreCount.text = tostring(score)
end

function LuaYuanXiao2021TangYuanEnterWnd:InitTextTable()
    self.m_TextTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.m_TextTable.transform)
    local msg = g_MessageMgr:FormatMessage("YuanXiao2021_YuanXiao_Enter_Introduction")
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then
        return
    end
    for i = 0, info.paragraphs.Count - 1 do
        local paragraphGo = NGUITools.AddChild(self.m_TextTable.gameObject, self.m_TextTemplate)
        paragraphGo:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem))
                :Init(info.paragraphs[i], 4294967295)
    end
    self.m_ScrollView:ResetPosition()
    self.m_TextTable:Reposition()
end