local CItemCountUpdate=import "L10.UI.CItemCountUpdate"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType1 = import "L10.UI.CTooltip+AlignType"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"

CLuaCrossDouDiZhuApplyWnd = class()
RegistClassMember(CLuaCrossDouDiZhuApplyWnd,"m_CountUpdate")
RegistClassMember(CLuaCrossDouDiZhuApplyWnd,"m_TemplateId")
RegistClassMember(CLuaCrossDouDiZhuApplyWnd,"m_Silver")
RegistClassMember(CLuaCrossDouDiZhuApplyWnd,"m_SignUp")
RegistClassMember(CLuaCrossDouDiZhuApplyWnd,"m_Rank")
RegistClassMember(CLuaCrossDouDiZhuApplyWnd,"m_Score")

RegistClassMember(CLuaCrossDouDiZhuApplyWnd,"m_RankLabel")
RegistClassMember(CLuaCrossDouDiZhuApplyWnd,"m_ScoreLabel")
RegistClassMember(CLuaCrossDouDiZhuApplyWnd,"m_JiangJinLabel")
RegistClassMember(CLuaCrossDouDiZhuApplyWnd,"m_FreeJoinCount")

RegistClassMember(CLuaCrossDouDiZhuApplyWnd,"m_IsJueSai")


function CLuaCrossDouDiZhuApplyWnd:Init()
    self.m_FreeJoinCount = 0
    self.m_IsJueSai = false

    local headLabel = self.transform:Find("Header/Label"):GetComponent(typeof(UILabel))
    headLabel.text = nil
    local time = CServerTimeMgr.Inst:GetZone8Time()
    local month,day=time.Month,time.Day

    local activeDayIndex = nil
    if month==2 and day>=3 and day<=8 then
        headLabel.text = g_MessageMgr:FormatMessage("CrossDouDiZhu_JiFenSaiTime")--积分赛
        activeDayIndex = day-3
    elseif month==2 and day==9 then
        headLabel.text = g_MessageMgr:FormatMessage("CrossDouDiZhu_JueSaiTime")
        activeDayIndex = day-3
        self.m_IsJueSai = true
    end

    
    local starTf = self.transform:Find("Progress/Grid")
    for i=1,starTf.childCount do
        local starItem = starTf:GetChild(i-1):Find("Sprite").gameObject
        local line = starTf:GetChild(i-1):Find("Line").gameObject
        if activeDayIndex and activeDayIndex>=i-1 then
            starItem:SetActive(true)
            line:SetActive(true)
        else
            starItem:SetActive(false)
            line:SetActive(false)
        end
        if i==starTf.childCount then
            line:SetActive(false)
        end
    end


    local signedNode = self.transform:Find("Applyed").gameObject
    local noSignedNode = self.transform:Find("Apply").gameObject
    signedNode:SetActive(false)
    noSignedNode:SetActive(false)

    local jingcaiButton = noSignedNode.transform:Find("JingCaiButton").gameObject
    local item = noSignedNode.transform:Find("ItemCell").gameObject
    local applyLabel = noSignedNode.transform:Find("ApplyButton/Label"):GetComponent(typeof(UILabel))
    if self.m_IsJueSai then
        jingcaiButton:SetActive(true)
        item:SetActive(false)
        UIEventListener.Get(jingcaiButton).onClick = DelegateFactory.VoidDelegate(function(go)
            CUIManager.ShowUI(CLuaUIResources.CrossDouDiZhuVoteWnd)
        end)
        applyLabel.text = LocalString.GetString("进入总决赛")
    else
        jingcaiButton:SetActive(false)
        item:SetActive(true)
        applyLabel.text = LocalString.GetString("报名积分赛")
    end

    Gac2Gas.CrossDouDiZhuOpenSignUpWnd()
end

function CLuaCrossDouDiZhuApplyWnd:Awake()
    self.m_JiangJinLabel = self.transform:Find("JiangJinLabel"):GetComponent(typeof(UILabel))
    self.m_JiangJinLabel.text = nil
    self.m_RankLabel = self.transform:Find("PlayerInfo/RankLabel"):GetComponent(typeof(UILabel))
    self.m_RankLabel.text = nil
    self.m_ScoreLabel = self.transform:Find("PlayerInfo/ScoreLabel"):GetComponent(typeof(UILabel))
    self.m_ScoreLabel.text = nil


    self.m_TemplateId = DouDiZhu_CrossSetting.GetData().HaiXuanSignUpCostItemId

    if CClientMainPlayer.Inst then
        local portraitTexture = self.transform:Find("PlayerInfo/Portrait"):GetComponent(typeof(CUITexture))
        portraitTexture:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName,false)
    end

    local rankButton = self.transform:Find("RankButton").gameObject
    UIEventListener.Get(rankButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.CrossDouDiZhuRankWnd)
    end)

    local getNode=self.transform:Find("Apply/ItemCell/GetNode").gameObject
    getNode:SetActive(false)


    local icon=self.transform:Find("Apply/ItemCell/Icon"):GetComponent(typeof(CUITexture))
    local item = Item_Item.GetData(self.m_TemplateId)
    if item then
        icon:LoadMaterial(item.Icon)
    end

    self.m_CountUpdate = self.transform:Find("Apply/ItemCell"):GetComponent(typeof(CItemCountUpdate))
    self.m_CountUpdate.templateId = self.m_TemplateId
    self.m_CountUpdate.format = "{0}/1"
    self.m_CountUpdate.onChange = DelegateFactory.Action_int(function (val)
        if val < 1 then
            self.m_CountUpdate.format = "[ff0000]{0}[-]/1"
            getNode:SetActive(true)
        else
            self.m_CountUpdate.format = "{0}/1"
            getNode:SetActive(false)
        end
    end)
    self.m_CountUpdate:UpdateCount()

    UIEventListener.Get(self.m_CountUpdate.gameObject).onClick = DelegateFactory.VoidDelegate(function(g)
        if self.m_CountUpdate.count >= 1 then
            CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_TemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
        else
            CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_TemplateId, false, self.m_CountUpdate.transform, AlignType1.Right)
        end
    end)


    local applyButton = self.transform:Find("Apply/ApplyButton").gameObject
    UIEventListener.Get(applyButton).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_IsJueSai then
            Gac2Gas.CrossDouDiZhuRequestEnterJueSaiScene()
        else
            if self.m_CountUpdate.count>0 or self.m_FreeJoinCount>0 then
                Gac2Gas.CrossDouDiZhuRequestSignUp()
            else
                CItemAccessListMgr.Inst:ShowItemAccessInfo(21020680, false, applyButton.transform, AlignType1.Right)
            end
        end
    end)

    local cancelButton = self.transform:Find("Applyed/CancelButton").gameObject
    UIEventListener.Get(cancelButton).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.CrossDouDiZhuRequestCancelSignUp()
    end)


    --进度
    local progressTf = self.transform:Find("Progress")
    local star = progressTf:Find("Star").gameObject
    star:SetActive(false)
    local grid = progressTf:Find("Grid").gameObject
    local data = {
        {"2.3",LocalString.GetString("积分赛第一天")},
        {"2.4",LocalString.GetString("积分赛第二天")},
        {"2.5",LocalString.GetString("积分赛第三天")},
        {"2.6",LocalString.GetString("积分赛第四天")},
        {"2.7",LocalString.GetString("积分赛第五天")},
        {"2.8",LocalString.GetString("积分赛第六天")},
        {"2.9",LocalString.GetString("总决赛")},
    }
    for i=1,7 do
        local g = NGUITools.AddChild(grid,star)
        g:SetActive(true)
        local label1=g.transform:Find("Label1"):GetComponent(typeof(UILabel))
        local label2=g.transform:Find("Label2"):GetComponent(typeof(UILabel))
        label1.text = data[i][1]
        label2.text = data[i][2]
        local sprite = g.transform:Find("Sprite").gameObject
        sprite:SetActive(false)
        local line = g.transform:Find("Line").gameObject
        line:SetActive(false)
    end
    grid:GetComponent(typeof(UIGrid)):Reposition()


    local rewardButton = self.transform:Find("RewardLabel").gameObject
    UIEventListener.Get(rewardButton).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("CrossDouDiZhu_Reward")
    end)
    local jiangJinButton = self.transform:Find("RewardLabel2").gameObject
    UIEventListener.Get(jiangJinButton).onClick = DelegateFactory.VoidDelegate(function(go)
        local t = CLuaCrossDouDiZhuMgr.GetJiangJin(self.m_Silver or 0)
        g_MessageMgr:ShowMessage("CrossDouDiZhu_JiangJin",
            tostring(t[1]),
            tostring(t[2]),
            tostring(t[3]),
            tostring(t[4]),
            tostring(t[5]),
            tostring(t[6]))
        --计算奖金
    end)
end


function CLuaCrossDouDiZhuApplyWnd:OnEnable()
    g_ScriptEvent:AddListener("CrossDouDiZhuOpenSignUpWndResult", self, "OnCrossDouDiZhuOpenSignUpWndResult")
    
end

function CLuaCrossDouDiZhuApplyWnd:OnDisable()
    g_ScriptEvent:RemoveListener("CrossDouDiZhuOpenSignUpWndResult", self, "OnCrossDouDiZhuOpenSignUpWndResult")
end

function CLuaCrossDouDiZhuApplyWnd:OnCrossDouDiZhuOpenSignUpWndResult(silver, bSignUp, rank, score,freeJoinCount)
    self.m_FreeJoinCount = freeJoinCount
    self.m_Silver = silver
    self.m_SignUp = bSignUp>0 and true or false
    self.m_Rank = rank
    self.m_Score = score
    
    local jiangjin = CLuaCrossDouDiZhuMgr.GetJiangJin(silver)
    self.m_JiangJinLabel.text = tostring(jiangjin[1])

    self.m_RankLabel.text = tostring(self.m_Rank)
    self.m_ScoreLabel.text = tostring(self.m_Score)

    local signedNode = self.transform:Find("Applyed").gameObject
    local noSignedNode = self.transform:Find("Apply").gameObject

    if self.m_SignUp then
        signedNode:SetActive(true)
        noSignedNode:SetActive(false)
    else
        signedNode:SetActive(false)
        noSignedNode:SetActive(true)
    end
end