-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CQingQiuMgr = import "L10.UI.CQingQiuMgr"
local CQingQiuScoreItem = import "L10.UI.CQingQiuScoreItem"
local EnumClass = import "L10.Game.EnumClass"
local LocalString = import "LocalString"
local Profession = import "L10.Game.Profession"
CQingQiuScoreItem.m_Init_CS2LuaHook = function (this, data, isOdd) 
    this.m_NameLabel.text = data.m_Name
    this.m_ScoreLabel.text = tostring(data.m_Score)
    this.m_ProfessionSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), data.m_Clazz))
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id == data.m_Id and data.m_Rank > CQingQiuScoreItem.s_MaxRank then
        this.m_RankLabel.text = LocalString.GetString("未上榜")
        if data.m_Score == 0 and not CQingQiuMgr.Inst:IsInQingQiuPlay() then
            this.m_ScoreLabel.text = "-"
        end
    else
        this.m_RankLabel.text = tostring(data.m_Rank)
    end

    local default
    if not isOdd then
        default = this.CommonDarkTextBg
    else
        default = this.CommonLightTextBg
    end
    this.button:SetBackgroundSprite(default)
end
