local UILabel = import "UILabel"

local UITable = import "UITable"

local UIRoot=import "UIRoot"
local Application = import "UnityEngine.Application"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientVersion = import "L10.Engine.CClientVersion"
local CExistingPlayerInfo = import "L10.Game.CLoginMgr+CExistingPlayerInfo"
local CLoadingWnd = import "L10.UI.CLoadingWnd"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CObjectFX = import "L10.Game.CObjectFX"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CPropertyAppearance = import "L10.Game.CPropertyAppearance"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CRenderObject = import "L10.Engine.CRenderObject"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local DelegateFactory = import "DelegateFactory"
local EnumClass = import "L10.Game.EnumClass"
local EnumEventType = import "EnumEventType"
local EnumGender = import "L10.Game.EnumGender"
local EventDelegate = import "EventDelegate"
local EventManager = import "EventManager"
local Gac2Login = import "L10.Game.Gac2Login"
local GameObject = import "UnityEngine.GameObject"
local GestureRecognizer = import "L10.Engine.GestureRecognizer"
local GestureType = import "L10.Engine.GestureType"
local LayerDefine = import "L10.Engine.LayerDefine"
local LocalString = import "LocalString"
local LoginModelRoot = import "L10.UI.LoginModelRoot"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUIText = import "NGUIText"
local PlayerSettings = import "L10.Game.PlayerSettings"
local Profession = import "L10.Game.Profession"
local Random = import "UnityEngine.Random"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local Screen = import "UnityEngine.Screen"
local SoundManager = import "SoundManager"
local System = import "System"
local UIEventListener = import "UIEventListener"
local Vector3 = import "UnityEngine.Vector3"
local CProfessionListView = import "L10.UI.CProfessionListView"
local CButton = import "L10.UI.CButton"
local UIToggle = import "UIToggle"
local CCharacterHairCell = import "L10.UI.CCharacterHairCell"
local CExistingHeadPortrait = import "L10.UI.CExistingHeadPortrait"
local CUITexture = import "L10.UI.CUITexture"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local LuaTweenUtils = import "LuaTweenUtils"
local Ease = import "DG.Tweening.Ease"
local Setting = import "L10.Engine.Setting"
local CoreScene = import "L10.Engine.Scene.CoreScene"
local EShadowType = import "L10.Engine.EShadowType"
local Shader = import "UnityEngine.Shader"
local SystemInfo = import "UnityEngine.SystemInfo"
local EWeaponVisibleReason = import "L10.Engine.EWeaponVisibleReason"
local CIKMgr = import "L10.Game.CIKMgr"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local NativeTools = import "L10.Engine.NativeTools"
local DRPFMgr = import "L10.Game.DRPFMgr"
local CUIFx = import "L10.UI.CUIFx"
local TweenAlpha = import "TweenAlpha"
local CPropertySkill = import "L10.Game.CPropertySkill"
local CAppClient = import "L10.Engine.CAppClient"
local C3DTouchMgr = import "L10.UI.C3DTouchMgr"
local Enum3DTouchStatus = import "L10.UI.Enum3DTouchStatus"
local CDeepLinkMgr = import "L10.Game.CDeepLinkMgr"
local QnModelPreviewer = import "L10.UI.QnModelPreviewer"
local CHeadPortrait = import "L10.UI.CHeadPortrait"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local Animation = import "UnityEngine.Animation"

LuaProfessionTransferCharacterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaProfessionTransferCharacterWnd, "ProfessionList", "ProfessionList", CProfessionListView)
RegistChildComponent(LuaProfessionTransferCharacterWnd, "PortraitTemplate", "PortraitTemplate", GameObject)
RegistChildComponent(LuaProfessionTransferCharacterWnd, "Profession", "Profession", CUITexture)
RegistChildComponent(LuaProfessionTransferCharacterWnd, "Feature", "Feature", CUITexture)
RegistChildComponent(LuaProfessionTransferCharacterWnd, "DescTexture", "DescTexture", CUITexture)
RegistChildComponent(LuaProfessionTransferCharacterWnd, "AboveFx", "AboveFx", CUIFx)
RegistChildComponent(LuaProfessionTransferCharacterWnd, "BelowFx", "BelowFx", CUIFx)
RegistChildComponent(LuaProfessionTransferCharacterWnd, "Radar", "Radar", GameObject)
RegistChildComponent(LuaProfessionTransferCharacterWnd, "RadarTexture", "RadarTexture", CUITexture)
RegistChildComponent(LuaProfessionTransferCharacterWnd, "CurrentClassText", "CurrentClassText", UILabel)
RegistChildComponent(LuaProfessionTransferCharacterWnd, "TargetClassText", "TargetClassText", UILabel)
RegistChildComponent(LuaProfessionTransferCharacterWnd, "TransferPrice", "TransferPrice", UILabel)
RegistChildComponent(LuaProfessionTransferCharacterWnd, "InScheduleTips", "InScheduleTips", UILabel)
RegistChildComponent(LuaProfessionTransferCharacterWnd, "NoGenderText", "NoGenderText", UILabel)
RegistChildComponent(LuaProfessionTransferCharacterWnd, "ChangeClassButton", "ChangeClassButton", GameObject)
RegistChildComponent(LuaProfessionTransferCharacterWnd, "ChangeGenderButton", "ChangeGenderButton", GameObject)
RegistChildComponent(LuaProfessionTransferCharacterWnd, "Master", "Master", GameObject)
RegistChildComponent(LuaProfessionTransferCharacterWnd, "ProfessionGrid", "ProfessionGrid", GameObject)
RegistChildComponent(LuaProfessionTransferCharacterWnd, "ModelPreview", "ModelPreview", QnModelPreviewer)
RegistChildComponent(LuaProfessionTransferCharacterWnd, "TipBtn", "TipBtn", GameObject)

--@endregion RegistChildComponent end

function LuaProfessionTransferCharacterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self:InitWndData()
    self:RefreshConstUI()
    self:RefreshVariableUI()
    self:InitUIEvent()
    
    self:ClickClass(self.selectedClass, true)
end

function LuaProfessionTransferCharacterWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaProfessionTransferCharacterWnd:InitWndData()
    self.selectedClass = -1
    self.newClass = 13
    self.classList = { }
    self.classGoList = {}

    self.myclass = 1
    self.mygender = 0
    if CClientMainPlayer.Inst then
        self.myclass = CommonDefs.Convert_ToUInt32(CClientMainPlayer.Inst.Class)
        self.mygender = CommonDefs.Convert_ToUInt32(CClientMainPlayer.Inst.Gender)
    end
    
    self.aniComp = self.transform:GetComponent(typeof(Animation))
end 

function LuaProfessionTransferCharacterWnd:RefreshConstUI()
    self:RefreshProfessionTbl()
    
    self.InScheduleTips.gameObject:SetActive(LuaProfessionTransferMgr.ShowProfessionTransferAlert())
    self.InScheduleTips.text = g_MessageMgr:FormatMessage("PROFESSIONTRANSFER_IN_SCHEDULE_TIPS")
    self.TransferPrice.text = ProfessionTransfer_Setting.GetData().YuanbaoRequire
    self.CurrentClassText.text = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), self.myclass))
    self.NoGenderText.text = g_MessageMgr:FormatMessage("PROFESSIONTRANSFER_NO_SUCH_GENDER_IN_CLASS")
end 

function LuaProfessionTransferCharacterWnd:RefreshVariableUI()
    
end 

function LuaProfessionTransferCharacterWnd:InitUIEvent()
    UIEventListener.Get(self.ChangeGenderButton).onClick = DelegateFactory.VoidDelegate(function (_)
        CShopMallMgr.SelectMallIndex = 0
        CShopMallMgr.SelectCategory = CShopMallMgr.GetItemCategoryOfLinyuShop(21003743)
        CShopMallMgr.SearchTemplateId = 21003743
        CUIManager.ShowUI(CUIResources.ShoppingMallWnd)
        CUIManager.CloseUI(CLuaUIResources.ProfessionTransferCharacterWnd)
    end)

    UIEventListener.Get(self.TipBtn).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("PROFESSION_TRANSFER_CHARACTER_WND_TIP")
    end)

    UIEventListener.Get(self.ChangeClassButton).onClick = DelegateFactory.VoidDelegate(function (_)
        local message = ""
        if LuaProfessionTransferMgr.ShowProfessionTransferAlert() then
            message = g_MessageMgr:FormatMessage("ProfessionTransfer_CONFIRM_IN_ZHANKUANG_DURATION", Profession.GetFullName(
                    CommonDefs.ConvertIntToEnum(typeof(EnumClass), self.selectedClass)))
        else
            message = g_MessageMgr:FormatMessage("ProfessionTransfer_CONFIRM", Profession.GetFullName(
                    CommonDefs.ConvertIntToEnum(typeof(EnumClass), self.selectedClass)))
        end
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
            Gac2Gas.RequestDoTransfer(self.selectedClass)
            CUIManager.CloseUI(CLuaUIResources.ProfessionTransferCharacterWnd)
        end), nil, LocalString.GetString("转职"), nil, false)
    end)
end 

function LuaProfessionTransferCharacterWnd:RefreshProfessionTbl()
    --Table
    Extensions.RemoveAllChildren(self.ProfessionGrid.transform)


    local forbiddenClasses = {}
    local arr = ProfessionTransfer_Setting.GetData().Nonopened
    if arr ~= nil then
        do
            local i = 0
            while i < arr.Length do
                forbiddenClasses[arr[i]] = true
                i = i + 1
            end
        end
    end
    
    local containTbl = {}
    Initialization_Init.ForeachKey(function (key)
        local data = Initialization_Init.GetData(key)
        if data.Status == 0 and (not containTbl[data.Class]) and data.Class ~= self.myclass then
            if forbiddenClasses[data.Class] then
                --do nothing
            else
                table.insert(self.classList, data.Class)
                containTbl[data.Class] = true

                if self.selectedClass == -1 then
                    self.selectedClass = data.Class
                end
            end
        end
    end)

    self.PortraitTemplate:SetActive(false)
    for i = 1, #self.classList do
        local curClass = self.classList[i]
        local go = CommonDefs.Object_Instantiate(self.PortraitTemplate)
        go.transform:Find("SelectedBg").gameObject:SetActive(false)
        go.transform:Find("IsNew").gameObject:SetActive(curClass == self.newClass)
        go.transform:Find("ClassText"):GetComponent(typeof(UILabel)).text = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), curClass))
        go:SetActive(true)
        go.transform.parent = self.ProfessionGrid.transform

        local headScript = go.transform:GetComponent(typeof(CHeadPortrait))
        headScript:SetPortrait(CommonDefs.ConvertIntToEnum(typeof(EnumClass), curClass) )

        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (_)
            self:ClickClass(curClass, false)
        end)
        
        table.insert(self.classGoList, go)
    end
    self.ProfessionGrid.transform:GetComponent(typeof(UIGrid)):Reposition()
end

function LuaProfessionTransferCharacterWnd:ClickClass(class, isFirstChange)
    self.TargetClassText.text = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), class))
    
    local isChangeClass = false
    if isFirstChange then
        isChangeClass = true
    else
        self.aniComp:Stop()
        self.aniComp:Play("professiontransfercharacterwnd_tab")
        isChangeClass = (self.selectedClass ~= class)
    end
    self.selectedClass = class
    for i = 1, #self.classGoList do
        local go = self.classGoList[i]
        local curClass = self.classList[i]
        go.transform:Find("SelectedBg").gameObject:SetActive(curClass == class)
        go.transform:Find("ClassText").gameObject:SetActive(curClass ~= class)
    end

    if class == self.newClass and self.mygender == 1 then
        --新职业女性特殊处理
        self.NoGenderText.gameObject:SetActive(true)
        self.Master.transform:Find("Right").gameObject:SetActive(false)
        self.ModelPreview.gameObject:SetActive(false)
        self.ChangeClassButton:SetActive(false)
        self.ChangeGenderButton:SetActive(true)
    else
        self.NoGenderText.gameObject:SetActive(false)
        self.Master.transform:Find("Right").gameObject:SetActive(true)
        self.ModelPreview.gameObject:SetActive(true)
        self.ChangeClassButton:SetActive(true)
        self.ChangeGenderButton:SetActive(false)
        
        --刷新右侧资料
        local data = Initialization_Init.GetData(class * 100 + self.mygender)
        self.Profession:LoadMaterial(data.ProfessionTexture)
        self.Feature:LoadMaterial(data.FeatureTexture)
        self.DescTexture:LoadMaterial(data.IntroductionTexture)
        if class == EnumToInt(EnumClass.YingLing) then
            self.RadarTexture:Clear()
            self:RegisterYingLingRadarMapTick()
        else
            self:CancelYingLingRadarMapTick()
            self.RadarTexture.texture.alpha = 1
            self.RadarTexture:LoadMaterial(Profession.GetRadarMap(CommonDefs.ConvertIntToEnum(typeof(EnumClass), class)))
        end
        if isChangeClass then
            self.AboveFx:DestroyFx()
            self.BelowFx:DestroyFx()
            self.AboveFx:LoadFx("Fx/UI/Prefab/UI_zhiyemingcheng.prefab")
            self.BelowFx:LoadFx("Fx/UI/Prefab/UI_zhiyemingcheng_shuimo.prefab")
            local tweenalpha = self.Profession.gameObject:GetComponent(typeof(TweenAlpha))
            tweenalpha:ResetToBeginning()
            tweenalpha:PlayForward()
        end

        --刷新中间模型信息
        if isChangeClass then
            self:ChangeCandidateRole(class)
        end    
    end
    
end

function LuaProfessionTransferCharacterWnd:ChangeCandidateRole(class)
    local eclass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), class)
    local egender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), self.mygender)
    local equipmentInfos,shenBingFirstModelId, shenBingSecondModelId, shenBingTrainLvFxId, shenBingRanSeData = self:GetEquipmentInfos(eclass, egender, false)
    local appearanceData = CPropertyAppearance.GetDataFromCustom(eclass, egender,
            CPropertySkill.GetDefaultYingLingStateByGender(egender), equipmentInfos, nil, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, shenBingFirstModelId, shenBingSecondModelId, shenBingTrainLvFxId, shenBingRanSeData, 0, 0, 0, 0, 0, 0, nil, 0)
    self.ModelPreview.IsShowHighRes = true
    self.ModelPreview:PreviewFakeAppear(appearanceData)
end

function LuaProfessionTransferCharacterWnd:GetEquipmentInfos(enumClass, enumGender, isXianShen)
    self.m_InitializationData = Initialization_Init.GetData(EnumToInt(enumClass) * 100 + EnumToInt(enumGender))
    if isXianShen and self.m_InitializationData.ShenBingWaiGuan then
        local equipInfos = {}
        local shenBingFirstModelId = {}
        local shenBingSecondModelId = {}
        local shenBingTrainLvFxId = {}
        local shenBingRanSeData = {}
        for i=0,self.m_InitializationData.ShenBingWaiGuan.Length-1 do
            local equipTemplateId = self.m_InitializationData.ShenBingWaiGuan[i][0]
            local model1Id = self.m_InitializationData.ShenBingWaiGuan[i][1]
            local model2Id = self.m_InitializationData.ShenBingWaiGuan[i][2]
            local fxId = self.m_InitializationData.ShenBingWaiGuan[i][3]
            table.insert(equipInfos, equipTemplateId)
            table.insert(shenBingFirstModelId, model1Id)
            table.insert(shenBingSecondModelId, model2Id)
            table.insert(shenBingTrainLvFxId, fxId)
            table.insert(shenBingRanSeData, 0)
        end
        return Table2Array(equipInfos, MakeArrayClass(uint)), Table2Array(shenBingFirstModelId, MakeArrayClass(uint)), Table2Array(shenBingSecondModelId, MakeArrayClass(uint)),
        Table2Array(shenBingTrainLvFxId, MakeArrayClass(uint)), Table2Array(shenBingRanSeData, MakeArrayClass(uint))
    end
    return self.m_InitializationData.NewRoleEquip3, nil ,nil, nil, nil
end

function LuaProfessionTransferCharacterWnd:RegisterYingLingRadarMapTick( ... )
    self:CancelYingLingRadarMapTick()
    local i = 0
    local updateFunc = function ()
        if self.RadarTexture ~= nil then
            self.RadarTexture:LoadMaterial(Profession.GetRadarMap(CommonDefs.ConvertIntToEnum(typeof(EnumClass), self.selectedClass), i%3 + 1))
            i = i + 1
            LuaTweenUtils.DOKill(self.RadarTexture.transform, false)
            local fadeInTweener = LuaTweenUtils.TweenAlpha(self.RadarTexture.texture, self.RadarTexture.transform, 0,1,0.5)
            local fadeOutTweener = LuaTweenUtils.TweenAlpha(self.RadarTexture.texture, self.RadarTexture.transform,1,0,0.5)
            LuaTweenUtils.SetDelay(fadeOutTweener, 2)
        end
    end
    self.m_YingLingRadarMapTick = RegisterTick(function ( ... )
        updateFunc()
    end, 3000)
    updateFunc()--先执行一下
end

function LuaProfessionTransferCharacterWnd:CancelYingLingRadarMapTick( ... )
    if self.m_YingLingRadarMapTick then
        UnRegisterTick(self.m_YingLingRadarMapTick)
        self.m_YingLingRadarMapTick = nil
        LuaTweenUtils.DOKill(self.RadarTexture.transform, false)
    end
end



