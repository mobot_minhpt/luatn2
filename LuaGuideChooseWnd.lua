local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local COpenEntryMgr=import "L10.Game.COpenEntryMgr"

LuaGuideChooseWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuideChooseWnd, "Button1", "Button1", GameObject)
RegistChildComponent(LuaGuideChooseWnd, "Button2", "Button2", GameObject)
RegistChildComponent(LuaGuideChooseWnd, "CloseBtn", "CloseBtn", GameObject)

--@endregion RegistChildComponent end

function LuaGuideChooseWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Button1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButton1Click()
	end)
	
	UIEventListener.Get(self.Button2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButton2Click()
	end)
	UIEventListener.Get(self.CloseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseBtnClick()
	end)

    --@endregion EventBind end
end

function LuaGuideChooseWnd:Init()

end

--@region UIEvent

function LuaGuideChooseWnd:OnButton1Click()
    CUIManager.CloseUI(CLuaUIResources.GuideChooseWnd)
    CGuideMgr.Inst:EndCurrentPhase()
end

function LuaGuideChooseWnd:OnButton2Click()
    COpenEntryMgr.Inst:SetGuideRecordExt(EnumGuideExtKey.OldUser)
    CUIManager.CloseUI(CLuaUIResources.GuideChooseWnd)
    CGuideMgr.Inst:EndCurrentPhase()
end

function LuaGuideChooseWnd:OnCloseBtnClick()
    CUIManager.CloseUI(CLuaUIResources.GuideChooseWnd)
    CGuideMgr.Inst:EndCurrentPhase()
end


function LuaGuideChooseWnd:OnDestroy()
    --任务寻路引导
    CLuaGuideMgr.TryTriggerTaskGuide()
end

--@endregion UIEvent

