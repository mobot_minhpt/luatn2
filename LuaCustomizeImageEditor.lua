local Mathf = import "UnityEngine.Mathf"
local UIBasicSprite = import "UIBasicSprite"

LuaCustomizeImageEditor = class()

RegistChildComponent(LuaCustomizeImageEditor, "m_ConfirmBtn","ConfirmBtn", GameObject)
RegistChildComponent(LuaCustomizeImageEditor, "m_CancleBtn","CancleBtn", GameObject)
RegistChildComponent(LuaCustomizeImageEditor, "m_RotateBtn","RotateBtn", GameObject)
RegistChildComponent(LuaCustomizeImageEditor, "m_AxialSymmetryBtn","AxialSymmetryBtn", GameObject)
RegistChildComponent(LuaCustomizeImageEditor, "m_Pivot","Pivot", UIWidget)
RegistChildComponent(LuaCustomizeImageEditor, "m_LineRight","LineRight", UISprite)
RegistChildComponent(LuaCustomizeImageEditor, "m_LineLeft","LineLeft", UISprite)
RegistChildComponent(LuaCustomizeImageEditor, "m_LineTop","LineTop", UISprite)
RegistChildComponent(LuaCustomizeImageEditor, "m_LineBottom","LineBottom", UISprite)

RegistClassMember(LuaCustomizeImageEditor,"m_StartRotate")
RegistClassMember(LuaCustomizeImageEditor,"m_StartMove")
RegistClassMember(LuaCustomizeImageEditor,"m_MaxSize")
RegistClassMember(LuaCustomizeImageEditor,"m_MaxDiagonalLength")
RegistClassMember(LuaCustomizeImageEditor,"m_MinSize")
RegistClassMember(LuaCustomizeImageEditor,"m_MinDiagonalLength")
RegistClassMember(LuaCustomizeImageEditor,"m_OnConfirmFunc")
RegistClassMember(LuaCustomizeImageEditor,"m_OnCancleFunc")
RegistClassMember(LuaCustomizeImageEditor,"m_BottomLeftPos")
RegistClassMember(LuaCustomizeImageEditor,"m_TargetTexture")
RegistClassMember(LuaCustomizeImageEditor,"m_TargetTextureRawWidth")
RegistClassMember(LuaCustomizeImageEditor,"m_TargetTextureRawHeight")
RegistClassMember(LuaCustomizeImageEditor,"m_BorderWidget")

function LuaCustomizeImageEditor:Awake()
    UIEventListener.Get(self.m_ConfirmBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnConfirmBtnClick(go)
    end)
    UIEventListener.Get(self.m_CancleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCancleBtnClick(go)
    end)
    UIEventListener.Get(self.m_RotateBtn.gameObject).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        self:OnRotateBtnPress(go, isPressed)
    end)
    UIEventListener.Get(self.m_AxialSymmetryBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnAxialSymmetryBtnClick(go)
    end)
    UIEventListener.Get(self.m_Pivot.gameObject).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        self:OnPress(go, isPressed)
    end)
    self.gameObject:SetActive(false)
end

--使用入口 确认按钮事件，取消按钮事件，拉伸框最大长度，拉升框最小长度，调整的图片(UIBasicSprite)，移动边界(widget), 图片的原始宽，图片的原始高
function LuaCustomizeImageEditor:Init(onConfirm, onCanecl, maxSize, minSize, targetTexture, borderWidget, rawWidth, rawHeight)
    rawWidth = rawWidth and rawWidth or targetTexture.width
    rawHeight = rawHeight and rawHeight or targetTexture.height

    self.m_BorderWidget = borderWidget
    self.m_TargetTexture = targetTexture
    self.m_OnConfirmFunc = onConfirm
    self.m_OnCancleFunc = onCanecl
    self.m_MaxSize = maxSize
    self.m_MaxDiagonalLength = self.m_MaxSize / 2 * math.sqrt(2)
    self.m_MinSize = minSize
    self.m_MinDiagonalLength = self.m_MinSize / 2 * math.sqrt(2)
    self.m_Pivot.transform.position = targetTexture.transform.position
    local pos = self.m_Pivot.transform.localPosition
    pos.z = 0
    self.m_Pivot.transform.localPosition = pos
    self.m_Pivot.transform.localEulerAngles = targetTexture.transform.localEulerAngles
    local maxLength = Mathf.Max(targetTexture.width, targetTexture.height)
    self.m_TargetTextureRawWidth = rawWidth
    self.m_TargetTextureRawHeight = rawHeight
    targetTexture.width, targetTexture.height = rawWidth, rawHeight
    self.m_BottomLeftPos = Vector3(pos.x - maxLength / 2, pos.y - maxLength / 2, 0)
    self.m_BottomLeftPos = self:GetRotatePos(self.m_BottomLeftPos, self.m_Pivot.transform.localPosition, self.m_Pivot.transform.localEulerAngles.z)
    self:UpdatePosition()
    self.gameObject:SetActive(true)
end

function LuaCustomizeImageEditor:GetRotatePos(rawPos, pivotPos, angle)
    local val = angle / 180 * math.pi
    local cosVal = math.cos(val)
    local sinVal = math.sin(val)
    local pos = Vector3.zero
    pos.x = (rawPos.x - pivotPos.x) * cosVal - (rawPos.y - pivotPos.y) * sinVal + pivotPos.x;
    pos.y = (rawPos.x - pivotPos.x) * sinVal + (rawPos.y - pivotPos.y) * cosVal + pivotPos.y;
    return pos
end

function LuaCustomizeImageEditor:OnConfirmBtnClick(go)
    if self.m_OnConfirmFunc then
        self.m_OnConfirmFunc(go)
    end
    self.gameObject:SetActive(false)
end

function LuaCustomizeImageEditor:OnCancleBtnClick(go)
    if self.m_OnCancleFunc then
        self.m_OnCancleFunc(go)
    end
    self.gameObject:SetActive(false)
end

function LuaCustomizeImageEditor:OnRotateBtnPress(go, isPressed)
    self.m_StartRotate = isPressed
end

function LuaCustomizeImageEditor:OnPress(go, isPressed)
    self.m_StartMove = isPressed
end

function LuaCustomizeImageEditor:OnAxialSymmetryBtnClick(go)
    self.m_TargetTexture.flip = (self.m_TargetTexture.flip == UIBasicSprite.Flip.Horizontally) and UIBasicSprite.Flip.Nothing or UIBasicSprite.Flip.Horizontally
end

function LuaCustomizeImageEditor:Update()
    if self.m_StartRotate then
        local posTo = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
        local relLocalPos = self.m_Pivot.transform.parent:InverseTransformPoint(posTo)
        relLocalPos.z = 0
        local pivotPos = self.m_Pivot.transform.localPosition
        local dis = Vector3.Distance(relLocalPos, self.m_Pivot.transform.localPosition)
        if dis > self.m_MaxDiagonalLength then
            local x = pivotPos.x - self.m_MaxDiagonalLength / dis * (pivotPos.x - relLocalPos.x)
            local y = pivotPos.y - self.m_MaxDiagonalLength / dis * (pivotPos.y - relLocalPos.y)
            relLocalPos.x = x
            relLocalPos.y = y
        elseif dis < self.m_MinDiagonalLength then
            local x = pivotPos.x - self.m_MinDiagonalLength / dis * (pivotPos.x - relLocalPos.x)
            local y = pivotPos.y - self.m_MinDiagonalLength / dis * (pivotPos.y - relLocalPos.y)
            relLocalPos.x = x
            relLocalPos.y = y
        end
        self.m_RotateBtn.transform.localPosition = relLocalPos
        local bottomRightPos = relLocalPos
        local topRightPos = self:GetAnticlockwiseRotate90Pos(bottomRightPos, pivotPos)
        local topLeftPos = self:GetAnticlockwiseRotate90Pos(topRightPos, pivotPos)
        self.m_BottomLeftPos = self:GetAnticlockwiseRotate90Pos(topLeftPos, pivotPos)
        self:UpdatePosition()
    end

    if self.m_StartMove then
        local prePos = self.m_Pivot.transform.localPosition
        local posTo = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
        local corners = self.m_BorderWidget.worldCorners
        posTo.x = Mathf.Clamp(posTo.x, corners[0].x, corners[2].x)
        posTo.y = Mathf.Clamp(posTo.y, corners[0].y, corners[2].y)
        local relLocalPos = self.m_Pivot.transform.parent:InverseTransformPoint(posTo)
        relLocalPos.z = 0
        local offset = CommonDefs.op_Subtraction_Vector3_Vector3(relLocalPos, prePos)
        self.m_BottomLeftPos = CommonDefs.op_Addition_Vector3_Vector3(self.m_BottomLeftPos, offset)
        self.m_Pivot.transform.localPosition = relLocalPos
        self:UpdatePosition()
    end
end

function LuaCustomizeImageEditor:ChangeSymmetryBtn(state)
    self.m_AxialSymmetryBtn:SetActive(state)
end

function LuaCustomizeImageEditor:UpdatePosition()
    if not self.m_BottomLeftPos then return end

    self.m_AxialSymmetryBtn.transform.localPosition = self.m_BottomLeftPos
    local pivotPos = self.m_Pivot.transform.localPosition
    local bottomRightPos = self:GetAnticlockwiseRotate90Pos(self.m_BottomLeftPos, pivotPos)
    local topRightPos = self:GetAnticlockwiseRotate90Pos(bottomRightPos, pivotPos)
    local topLeftPos = self:GetAnticlockwiseRotate90Pos(topRightPos, pivotPos)

    self.m_CancleBtn.transform.localPosition = topRightPos
    self.m_ConfirmBtn.transform.localPosition = topLeftPos
    self.m_RotateBtn.transform.localPosition = bottomRightPos
    local length = Vector3.Distance(bottomRightPos, self.m_BottomLeftPos)
    self.m_Pivot.mWidth,self.m_Pivot.mHeight = length,length
    self.m_Pivot:ResizeCollider()
    self.m_LineRight.transform.localPosition =  CommonDefs.op_Division_Vector3_Single(CommonDefs.op_Addition_Vector3_Vector3(topRightPos, bottomRightPos), 2)
    self.m_LineLeft.transform.localPosition = CommonDefs.op_Division_Vector3_Single(CommonDefs.op_Addition_Vector3_Vector3(topLeftPos, self.m_BottomLeftPos), 2)
    self.m_LineTop.transform.localPosition = CommonDefs.op_Division_Vector3_Single(CommonDefs.op_Addition_Vector3_Vector3(topRightPos, topLeftPos), 2)
    self.m_LineBottom.transform.localPosition = CommonDefs.op_Division_Vector3_Single(CommonDefs.op_Addition_Vector3_Vector3(self.m_BottomLeftPos, bottomRightPos), 2)

    self.m_LineRight.height,self.m_LineLeft.height,self.m_LineTop.height,self.m_LineBottom.height = length,length,length,length

    self:UpdateLineRotate(self.m_LineBottom.transform, bottomRightPos, self.m_BottomLeftPos)
    self:UpdateLineRotate(self.m_LineTop.transform, topLeftPos, topRightPos);
    self:UpdateLineRotate(self.m_LineRight.transform, bottomRightPos, topRightPos)
    self:UpdateLineRotate(self.m_LineLeft.transform, topLeftPos, self.m_BottomLeftPos)

    self:UpdateLineRotate( self.m_Pivot.transform, bottomRightPos, topRightPos)

    if self.m_TargetTexture then
        self.m_TargetTexture.transform.position = self.m_Pivot.transform.position
        local pos = self.m_TargetTexture.transform.localPosition
        pos.z = 0
        self.m_TargetTexture.transform.localPosition = pos
        self.m_TargetTexture.transform.localEulerAngles = Vector3(0, 0, self.m_Pivot.transform.localEulerAngles.z + 180)
        local val = length / Mathf.Max(self.m_TargetTextureRawWidth, self.m_TargetTextureRawHeight)
        self.m_TargetTexture.width = self.m_TargetTextureRawWidth * val
        self.m_TargetTexture.height = self.m_TargetTextureRawHeight * val
    end
end

function LuaCustomizeImageEditor:UpdateLineRotate(line, fromPos, toPos)
    if (CommonDefs.op_Subtraction_Vector3_Vector3(toPos, fromPos)).x > 0 then
        local angle = Vector3.Angle(CommonDefs.op_Subtraction_Vector3_Vector3(toPos, fromPos), Vector3(0, - 1, 0))
        line.localEulerAngles = Vector3(0, 0, angle)
    else
        local angle = Vector3.Angle(CommonDefs.op_Subtraction_Vector3_Vector3(toPos, fromPos), Vector3(0, - 1, 0))
        line.localEulerAngles = Vector3(0, 0, - angle)
    end
end

function LuaCustomizeImageEditor:GetAnticlockwiseRotate90Pos(rawPos, pivotPos)
    local pos = Vector3.zero
    pos.x = -(rawPos.y-pivotPos.y)+pivotPos.x
    pos.y = (rawPos.x-pivotPos.x)+pivotPos.y
    return pos
end
