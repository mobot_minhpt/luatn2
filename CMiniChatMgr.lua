local CMiniChatMgr = import "L10.UI.CMiniChatMgr"
local EnumAtPlayerNotifyType = import "L10.Game.EnumAtPlayerNotifyType"
local EnumLabaType = import "L10.Game.EnumLabaType"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local Constants = import "L10.Game.Constants"

CMiniChatMgr.m_hookShowNotify = function(senderId, senderName, labaType)
    CMiniChatMgr.NotifySenderId = senderId
    CMiniChatMgr.NotifySenderName = senderName
    local key = ""
    local notifyInfo = {}
    if labaType == EnumLabaType.DaLaba then
        CMiniChatMgr.NotifyType = EnumAtPlayerNotifyType.DaLaba
        key = "DaLabaNotify"
        notifyInfo = LuaCommonSideDialogMgr:GetNotifyInfo("LABA_BRDAD_TIP", "commonsidedialog_diban_06", Constants.DaLabaIcon, Constants.DaLabaCornerMark1SpriteName, Constants.DaLabaCornerMark2SpriteName, Constants.DaLabaCornerMark3SpriteName)
    elseif labaType == EnumLabaType.XiaoLaba then
        CMiniChatMgr.NotifyType = EnumAtPlayerNotifyType.XiaoLaba
        key = "XiaoLabaNotify"
        notifyInfo = LuaCommonSideDialogMgr:GetNotifyInfo("LABA_BRDAD_TIP", "commonsidedialog_diban07", Constants.XiaoLabaIcon, Constants.XiaoLabaCornerMark1SpriteName, Constants.XiaoLabaCornerMark2SpriteName, Constants.XiaoLabaCornerMark3SpriteName)
    elseif labaType == EnumLabaType.NYHTLaBa then
        CMiniChatMgr.NotifyType = EnumAtPlayerNotifyType.NYHTLaBa
        key = "NYHTLaBaNotify"
        notifyInfo = LuaCommonSideDialogMgr:GetNotifyInfo("LABA_BRDAD_TIP", "commonsidedialog_diban_03", Constants.NYHTLabaIcon, Constants.NYHTLabaCornerMark1SpriteName, Constants.NYHTLabaCornerMark2SpriteName, Constants.NYHTLabaCornerMark3SpriteName)
    elseif labaType == EnumLabaType.WeddingMaleLaBa then
        CMiniChatMgr.NotifyType = EnumAtPlayerNotifyType.WeddingMaleLaBa
        key = "WeddingMaleLaBaNotify"
        notifyInfo = LuaCommonSideDialogMgr:GetNotifyInfo("LABA_BRDAD_TIP", "commonsidedialog_diban_02", Constants.WeddingLabaIcon, Constants.WeddngMaleLabaCornerMark1SpriteName, Constants.WeddngMaleLabaCornerMark2SpriteName, Constants.WeddngMaleLabaCornerMark3SpriteName)
    elseif labaType == EnumLabaType.WeddingFemaleLaBa then
        CMiniChatMgr.NotifyType = EnumAtPlayerNotifyType.WeddingFemaleLaBa
        key = "WeddingFemaleLaBaNotify"
        notifyInfo = LuaCommonSideDialogMgr:GetNotifyInfo("LABA_BRDAD_TIP", "commonsidedialog_diban_01", Constants.WeddingLabaIcon, Constants.WeddngFemaleLabaCornerMark1SpriteName, Constants.WeddngFemaleLabaCornerMark2SpriteName, Constants.WeddngFemaleLabaCornerMark3SpriteName)
    elseif labaType == EnumLabaType.QingJiuLaBa then
        CMiniChatMgr.NotifyType = EnumAtPlayerNotifyType.XiaoLaba
        key = "XiaoLabaNotify"
        notifyInfo = LuaCommonSideDialogMgr:GetNotifyInfo("LABA_BRDAD_TIP", "commonsidedialog_diban07", Constants.XiaoLabaIcon, Constants.XiaoLabaCornerMark1SpriteName, Constants.XiaoLabaCornerMark2SpriteName, Constants.XiaoLabaCornerMark3SpriteName)
    else
        return
    end
    if LuaCommonSideDialogMgr.m_IsOpen then
        LuaCommonSideDialogMgr:ShowMiniChatNotifyWnd(key,
            LuaCommonSideDialogMgr:GetBtnInfo(LocalString.GetString("查看"), function() 
                CSocialWndMgr.ShowChatWnd(EChatPanel.World)
            end, true),notifyInfo)
        return 
    end
    CUIManager.ShowUI("MiniChatNotifyWnd")
end

CMiniChatMgr.m_hookShowGuildPlayerNotify = function(senderId, senderName)
    CMiniChatMgr.NotifySenderId = senderId
    CMiniChatMgr.NotifySenderName = senderName
    CMiniChatMgr.NotifyType = EnumAtPlayerNotifyType.Guild
    if LuaCommonSideDialogMgr.m_IsOpen then
        LuaCommonSideDialogMgr:ShowMiniChatNotifyWnd("GuildPlayerNotify",
            LuaCommonSideDialogMgr:GetBtnInfo(LocalString.GetString("查看"), function() 
                CSocialWndMgr.ShowChatWnd(EChatPanel.Guild)
            end, true),
            LuaCommonSideDialogMgr:GetNotifyInfo("GUILD_BRDAD_TIP","commonsidedialog_diban07"))
        return 
    end
    CUIManager.ShowUI("MiniChatNotifyWnd")
end