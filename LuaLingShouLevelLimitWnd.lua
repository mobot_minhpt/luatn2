local UIToggle = import "UIToggle"

local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIFx = import "L10.UI.CUIFx"

local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CUITexture = import "L10.UI.CUITexture"
local CItemMgr=import "L10.Game.CItemMgr"

local UISprite = import "UISprite"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local CLingShouBaseMgr=import "L10.Game.CLingShouBaseMgr"

local CItemCountUpdate = import "L10.UI.CItemCountUpdate"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnTableView = import "L10.UI.QnTableView"

local CLingShouModelTextureLoader=import "L10.UI.CLingShouModelTextureLoader"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local EventDelegate = import "EventDelegate"

LuaLingShouLevelLimitWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLingShouLevelLimitWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaLingShouLevelLimitWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaLingShouLevelLimitWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaLingShouLevelLimitWnd, "Content", "Content", GameObject)
RegistChildComponent(LuaLingShouLevelLimitWnd, "NoDataLabel", "NoDataLabel", UILabel)
RegistChildComponent(LuaLingShouLevelLimitWnd, "ShenShouSprite", "ShenShouSprite", UISprite)
RegistChildComponent(LuaLingShouLevelLimitWnd, "PinzhiSprite", "PinzhiSprite", UISprite)
RegistChildComponent(LuaLingShouLevelLimitWnd, "ModelTexture", "ModelTexture", CLingShouModelTextureLoader)
RegistChildComponent(LuaLingShouLevelLimitWnd, "CostItem", "CostItem", CItemCountUpdate)
RegistChildComponent(LuaLingShouLevelLimitWnd, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaLingShouLevelLimitWnd, "Fx", "Fx", CUIFx)
RegistChildComponent(LuaLingShouLevelLimitWnd, "LevelTrainToggle", "LevelTrainToggle", UIToggle)
RegistChildComponent(LuaLingShouLevelLimitWnd, "ExChengzhangTrainToggle", "ExChengzhangTrainToggle", UIToggle)
RegistChildComponent(LuaLingShouLevelLimitWnd, "ExPropsTrainToggle", "ExPropsTrainToggle", UIToggle)
RegistChildComponent(LuaLingShouLevelLimitWnd, "UsedCountLabel", "UsedCountLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaLingShouLevelLimitWnd,"m_Data")

LuaLingShouLevelLimitWnd.s_SelectLingShouId = nil

function LuaLingShouLevelLimitWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.LevelFromLabel = self.LevelTrainToggle.transform:Find("FromLabel"):GetComponent(typeof(UILabel))
    self.LevelToLabel = self.LevelTrainToggle.transform:Find("ToLabel"):GetComponent(typeof(UILabel))
    self.LevelSelectedBg = self.LevelTrainToggle.transform:Find("SelectedBg").gameObject

    self.ChengzhangFromLabel = self.ExChengzhangTrainToggle.transform:Find("FromLabel"):GetComponent(typeof(UILabel))
    self.ChengzhangToLabel = self.ExChengzhangTrainToggle.transform:Find("ToLabel"):GetComponent(typeof(UILabel))
    self.ChengzhangSelectedBg = self.ExChengzhangTrainToggle.transform:Find("SelectedBg").gameObject

    self.PropsFromLabel = self.ExPropsTrainToggle.transform:Find("FromLabel"):GetComponent(typeof(UILabel))
    self.PropsToLabel = self.ExPropsTrainToggle.transform:Find("ToLabel"):GetComponent(typeof(UILabel))
    self.PropsSelectedBg = self.ExPropsTrainToggle.transform:Find("SelectedBg").gameObject

    self.m_ConditionLevel = CLuaLingShouMgr.GetTrainConditionLevel()

    CommonDefs.ListAdd(self.LevelTrainToggle.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        self:UpdateLevelToggleState()
    end)))
    CommonDefs.ListAdd(self.ExChengzhangTrainToggle.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        self:UpdateChengzhangToggleState()
    end)))
    CommonDefs.ListAdd(self.ExPropsTrainToggle.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        self:UpdatePropsToggleState()
    end)))

    UIEventListener.Get(self.Button).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)
    UIEventListener.Get(self.TipButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

    if CommonDefs.IS_CN_CLIENT then
        self.ExChengzhangTrainToggle.gameObject:SetActive(true)
        self.ExPropsTrainToggle.gameObject:SetActive(true)
    else
        self.ExChengzhangTrainToggle.gameObject:SetActive(false)
        self.ExPropsTrainToggle.gameObject:SetActive(false)
    end
end

function LuaLingShouLevelLimitWnd:Init()
    local getNumFunc=function() 
        return #self.m_Data
    end
    local initItemFunc=function(item,index) 
        self:InitItem(item.transform,self.m_Data[index+1])
    end

    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(getNumFunc,initItemFunc)
    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:UpdateLingShou( row ) 
        self:UpdateCostItem()
    end)

    CLingShouMgr.Inst:RequestLingShouList()
end

function LuaLingShouLevelLimitWnd:UpdateLevelToggleState()
    self.LevelSelectedBg:SetActive(self.LevelTrainToggle.value)
    if self.LevelTrainToggle.value then
        self:UpdateCostItem()
    end
end

function LuaLingShouLevelLimitWnd:UpdateChengzhangToggleState()
    self.ChengzhangSelectedBg:SetActive(self.ExChengzhangTrainToggle.value)
    if self.ExChengzhangTrainToggle.value then
        self:UpdateCostItem()
    end
end

function LuaLingShouLevelLimitWnd:UpdatePropsToggleState()
    self.PropsSelectedBg:SetActive(self.ExPropsTrainToggle.value)
    if self.ExPropsTrainToggle.value then
        self:UpdateCostItem()
    end
end

function LuaLingShouLevelLimitWnd:UpdateCostItem()
    if not self.m_Data then return end
    local current = self.m_Data[self.TableView.m_CurrentSelectRow+1]
    local detailData = CLingShouMgr.Inst:GetLingShouDetails(current.id)
    local usedCount = 0
    local maxUsedCount = 0

    if not detailData then return end 

    self.m_CostItemTemplateId = LingShou_Setting.GetData().CangHuaZhuItemId
    local myLevel = detailData.data.Level
    if self.ExChengzhangTrainToggle.value then
        self.m_CostItemTemplateId = LingShou_Setting.GetData().ExChengzhangItemTempId
        usedCount = detailData.data.Props.ExChengzhangItemUsedTime
        maxUsedCount = CLuaLingShouMgr.GetMaxExChengzhangItemUsedCount(myLevel)
        self.UsedCountLabel.text = SafeStringFormat3(LocalString.GetString("已使用%d/%d"),usedCount,maxUsedCount)
    elseif self.ExPropsTrainToggle.value then
        self.m_CostItemTemplateId = LingShou_Setting.GetData().ExPropsItemTempId
        usedCount = detailData.data.Props.ExPropertyItemUsedTime
        maxUsedCount = CLuaLingShouMgr.GetMaxExPropsItemUsedCount(myLevel)
        self.UsedCountLabel.text = SafeStringFormat3(LocalString.GetString("已使用%d/%d"),usedCount,maxUsedCount)
    else
        self.UsedCountLabel.text = nil
    end

    self.CostItem.templateId=self.m_CostItemTemplateId
    local mask=FindChild(self.CostItem.transform,"Mask").gameObject
    local function OnCountChange(val)
        if val>0 then
            mask:SetActive(false)
            self.CostItem.format="{0}/1"
        else
            mask:SetActive(true)
            self.CostItem.format="[ff0000]{0}[-]/1"
        end
    end

    self.CostItem.onChange=DelegateFactory.Action_int(OnCountChange)
    self.CostItem:UpdateCount()

    UIEventListener.Get(self.CostItem.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    if self.CostItem.count==0 then
            CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_CostItemTemplateId, false, go.transform, AlignType.Right)
        end
	end)

    local itemTemplateData = Item_Item.GetData(self.m_CostItemTemplateId)
    self.Icon:LoadMaterial(itemTemplateData.Icon)
end

function LuaLingShouLevelLimitWnd:UpdateTrainDetails()
end
--@region UIEvent

function LuaLingShouLevelLimitWnd:OnButtonClick()
    if not self.m_CostItemTemplateId then
        self.m_CostItemTemplateId = LingShou_Setting.GetData().CangHuaZhuItemId
        if self.ExChengzhangTrainToggle.value then
            self.m_CostItemTemplateId = LingShou_Setting.GetData().ExChengzhangItemTempId
        elseif self.ExPropsTrainToggle.value then
            self.m_CostItemTemplateId = LingShou_Setting.GetData().ExPropsItemTempId
        end
    end

    local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, self.m_CostItemTemplateId)
    if default then
        local current = self.m_Data[self.TableView.m_CurrentSelectRow+1]
        if self.ExChengzhangTrainToggle.value then                                        --成长
            self.m_CostItemTemplateId = LingShou_Setting.GetData().ExChengzhangItemTempId
            if current then
                Gac2Gas.RequestImproveLingShouExChengzhang(current.id,EnumItemPlace.Bag,pos,itemId)
            end
        elseif self.ExPropsTrainToggle.value then                                         --属性
            self.m_CostItemTemplateId = LingShou_Setting.GetData().ExPropsItemTempId
            if current then
                Gac2Gas.RequestImproveLingShouExProps(current.id,EnumItemPlace.Bag,pos,itemId)
            end
        else                                                                              --等级限制       
            if current then
                Gac2Gas.RequestUseCangHuaZhu(1, pos, itemId, current.id)
            end
        end
    else
        local itemData = Item_Item.GetData(self.m_CostItemTemplateId)
        g_MessageMgr:ShowMessage("ITEM_NOT_ENOUGH",itemData.Name)
    end
end

function LuaLingShouLevelLimitWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("LingShou_LevelLimit_Tip")
end


--@endregion UIEvent

function LuaLingShouLevelLimitWnd:InitItem(tf,overviewData)
    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    local levelLabel = tf:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local evolveGradeLabel = tf:Find("EvolveGradeLabel"):GetComponent(typeof(UILabel))
    local icon =  tf:Find("Icon"):GetComponent(typeof(CUITexture))
    local battleSprite=tf:Find("BattleSprite"):GetComponent(typeof(UISprite))

    nameLabel.text = overviewData.name
    levelLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(overviewData.level))
    evolveGradeLabel.text = CLingShouBaseMgr.GetEvolveGradeDesc(overviewData.evolveGrade)
    icon:LoadNPCPortrait(CLingShouBaseMgr.GetLingShouIcon(overviewData.templateId), false)
    --出战状态刷新

    if CLingShouMgr.Inst:IsOnAssist(overviewData.id) then
        battleSprite.enabled = true
        battleSprite.spriteName = "common_support"
    elseif CLingShouMgr.Inst:IsFuTi(overviewData.id) then
        battleSprite.enabled = true
        battleSprite.spriteName = "common_hua"
    elseif CLingShouMgr.Inst:IsOnBattle(overviewData.id) then
        battleSprite.enabled = true
        battleSprite.spriteName = "common_fight"
    elseif CLingShouMgr.Inst:IsOnJieBan(overviewData.id) then
        battleSprite.enabled = true
        battleSprite.spriteName = "common_ban"
    else
        battleSprite.enabled = false
    end

end

function LuaLingShouLevelLimitWnd:OnEnable( )
    g_ScriptEvent:AddListener("GetAllLingShouOverview", self, "GetAllLingShouOverview")
    g_ScriptEvent:AddListener("GetLingShouDetails", self, "GetLingShouDetails")
    g_ScriptEvent:AddListener("UpdateLingShouMaxLevel", self, "OnLingShouUseCangHuaZhuSuccess")
    g_ScriptEvent:AddListener("UpdateLingShouExItemUsedTime", self, "OnUpdateLingShouExItemUsedTime")
end

function LuaLingShouLevelLimitWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("GetAllLingShouOverview", self, "GetAllLingShouOverview")
    g_ScriptEvent:RemoveListener("GetLingShouDetails", self, "GetLingShouDetails")
    g_ScriptEvent:RemoveListener("UpdateLingShouMaxLevel", self, "OnLingShouUseCangHuaZhuSuccess")
    g_ScriptEvent:RemoveListener("UpdateLingShouExItemUsedTime", self, "OnUpdateLingShouExItemUsedTime")
end
function LuaLingShouLevelLimitWnd:OnLingShouUseCangHuaZhuSuccess(lingshouId,newLv,newXiuwei)
    local currentRow = self.TableView.m_CurrentSelectRow
    local current = self.m_Data[currentRow+1]
    if current then
        if lingshouId==current.id then
            local detailData = CLingShouMgr.Inst:GetLingShouDetails(lingshouId)
            if detailData then
                detailData.data.Props.MaxIncLevel = newLv - CLuaLingShouMgr.MaxLingShouLevel
            end

            self.Fx:DestroyFx()
            self.Fx:LoadFx("Fx/UI/Prefab/UI_fazhenlianhua_4.prefab")

            self:UpdateLingShou(currentRow)
        end
    end
end

function LuaLingShouLevelLimitWnd:OnUpdateLingShouExItemUsedTime(playerId, lingshouId, exChengzhang, exProps)
    local currentRow = self.TableView.m_CurrentSelectRow
    local current = self.m_Data[currentRow+1]
    if lingshouId == current.id then
        local detailData = CLingShouMgr.Inst:GetLingShouDetails(lingshouId)
        if detailData then
            detailData.data.Props.ExChengzhangItemUsedTime = exChengzhang
            detailData.data.Props.ExPropertyItemUsedTime = exProps
        end
        self.Fx:DestroyFx()
        self.Fx:LoadFx("Fx/UI/Prefab/UI_fazhenlianhua_4.prefab")
        self:UpdateLingShou(currentRow)
    end

    self:UpdateCostItem()
end

function LuaLingShouLevelLimitWnd:GetAllLingShouOverview( )
    local list = CLingShouMgr.Inst:GetLingShouOverviewList()

    self.m_Data = {}
    for i=1,list.Count do
        local item = list[i-1]
        if item.level>=self.m_ConditionLevel then
            table.insert( self.m_Data,item )
        end
    end

    local selectIndex = 0
    for i,v in ipairs(self.m_Data) do
        if v.id==LuaLingShouLevelLimitWnd.s_SelectLingShouId then
            selectIndex = i-1
        end
    end

    if #self.m_Data==0 then
        -- self.NoDataLabel.text = g_MessageMgr:FormatMessage("LingShou_LevelLimit_NotUse")
        self.NoDataLabel.text = LocalString.GetString("灵兽等级达到109级后方可使用灵兽进阶培养功能")
        self.Content:SetActive(false)
    else
        self.NoDataLabel.text = nil
        self.Content:SetActive(true)
    end
    --初始化列表
    self.TableView:ReloadData(false, false)
    self.TableView:SetSelectRow(selectIndex, true)
end

function LuaLingShouLevelLimitWnd:UpdateLingShou( row) 
        local lingShouId = self.m_Data[row+1].id
        CLingShouMgr.Inst:RequestLingShouDetails(lingShouId)
end

function LuaLingShouLevelLimitWnd:GetLingShouDetails( args ) 
    if not self.m_Data then return end
    local lingShouId=args[0]
    local details=args[1]
    local currentRow = self.TableView.m_CurrentSelectRow
    local current = self.m_Data[currentRow+1]
    if lingShouId == current.id then
        if details ~= nil then
            local marryInfo = details.data.Props.MarryInfo
            local isDongFang = marryInfo.IsInDongfang>0
            local isLeave = (marryInfo.LeaveStartTime + marryInfo.LeaveDuration) > CServerTimeMgr.Inst.timeStamp
            if isDongFang then
                self.Content:SetActive(false)
                self.NoDataLabel.text = LocalString.GetString("当前灵兽生宝宝中，无法进行灵兽功能相应的操作")
                return
            elseif isLeave then
                self.Content:SetActive(false)
                self.NoDataLabel.text = LocalString.GetString("当前灵兽离家出走中，无法进行灵兽功能相应的操作")
                return
            end
            self.NoDataLabel.text = nil
            self.Content:SetActive(true)

            --Level
            local maxLv = CLuaLingShouMgr.MaxLingShouLevel+details.data.Props.MaxIncLevel
            self.LevelFromLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(maxLv))
            self.LevelToLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(maxLv+1))
            if maxLv>=CLuaLingShouMgr.LingShouLimitLevelMax then
                self.LevelToLabel.gameObject:SetActive(false)
                self.LevelToLabel.text = tostring(CLuaLingShouMgr.LingShouLimitLevelMax)..LocalString.GetString("级(已满)")   
            else
                self.LevelToLabel.gameObject:SetActive(true)
            end

            local myLevel = details.data.Level
            --ExChengzhang
            local chengzhangItemUsedTime = details.data.Props.ExChengzhangItemUsedTime
            chengzhangItemUsedTime = chengzhangItemUsedTime and chengzhangItemUsedTime or 0
            self:ExUpdateChengZhangView(current.id,chengzhangItemUsedTime)

            --ExProps
            local propsItemUsedTime = details.data.Props.ExPropertyItemUsedTime
            if not propsItemUsedTime then propsItemUsedTime = 0 end
            self:UpdateExPropsView(current.id,propsItemUsedTime)

            self.ModelTexture:Init(details)
			self.ShenShouSprite.gameObject:SetActive(false)
			if CLuaLingShouMgr.IsShenShou(details.data) then
				self.ShenShouSprite.gameObject:SetActive(true)
            end
			self.PinzhiSprite.spriteName = CLuaLingShouMgr.GetLingShouQualitySprite(CLuaLingShouMgr.GetLingShouQuality(details.data.Props.Quality)) or nil
        else
			self.ShenShouSprite.gameObject:SetActive(false)
            self.ModelTexture:InitNull()
			self.PinzhiSprite.spriteName = nil
        end
    end
    self:UpdateCostItem()
end

function LuaLingShouLevelLimitWnd:ExUpdateChengZhangView(lingshouId,usedTime)
    local currentRow = self.TableView.m_CurrentSelectRow
    local current = self.m_Data[currentRow+1]

    if lingshouId ~= current.id then
        return
    end

    local data1 = LingShou_ExItemEffect.GetData(usedTime)
    if data1 then
        self.ChengzhangFromLabel.text = SafeStringFormat3("+%.3f",data1.DeltaChengzhang)
    end
    if usedTime == 0 then
        self.ChengzhangFromLabel.text = "+0"
    end

    local data2 = LingShou_ExItemEffect.GetData(usedTime+1)
    if data2 then
        self.ChengzhangToLabel.gameObject:SetActive(true)               
        local delta = data2.DeltaChengzhang
        delta = SafeStringFormat3("+%.3f",delta)
        self.ChengzhangToLabel.text = delta
    else
        self.ChengzhangToLabel.gameObject:SetActive(false)
    end
end

function LuaLingShouLevelLimitWnd:UpdateExPropsView(lingshouId,usedTime)
    local currentRow = self.TableView.m_CurrentSelectRow
    local current = self.m_Data[currentRow+1]
    if lingshouId ~= current.id then
        return
    end
    local data1 = LingShou_ExItemEffect.GetData(usedTime)
    local data2 = LingShou_ExItemEffect.GetData(usedTime+1)

    if data1 then
        self.PropsFromLabel.text = SafeStringFormat3(LocalString.GetString("各项+%d"),data1.DeltaProps)
    end
    if usedTime == 0 then
        self.PropsFromLabel.text = SafeStringFormat3(LocalString.GetString("各项+%d"),0)
    end

    if data2 then
        self.PropsToLabel.gameObject:SetActive(true)    
        self.PropsToLabel.text = SafeStringFormat3(LocalString.GetString("各项+%d"),data2.DeltaProps)
    else 
        self.PropsToLabel.gameObject:SetActive(false)    
    end
end
