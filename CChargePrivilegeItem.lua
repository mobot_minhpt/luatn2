-- Auto Generated!!
local CChargePrivilegeItem = import "L10.UI.CChargePrivilegeItem"
local CChargeWnd = import "L10.UI.CChargeWnd"
local Charge_VipInfo = import "L10.Game.Charge_VipInfo"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local LocalString = import "LocalString"
local CChatLinkMgr = import "CChatLinkMgr"
CChargePrivilegeItem.m_UpdateData_CS2LuaHook = function (this, level)
    UIEventListener.Get(this.m_PrivilegeLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        local url = p:GetComponent(typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
        if url ~= nil then
            CChatLinkMgr.ProcessLinkClick(url, nil)
        end
    end)
    local info = Charge_VipInfo.GetData(level)
    if info ~= nil then
        this.m_VipLevelLabel.text = System.String.Format(LocalString.GetString("VIP[ffff00]{0}[-] 特权"), level)


        if CommonDefs.IS_KR_CLIENT or CommonDefs.IS_HMT_CLIENT or CommonDefs.IS_VN_CLIENT then
            if info.ShowCharge > 0 or level <= CChargeWnd.VipInfo.Level + 1 then
                this.m_VipDescriptionLabel.text = System.String.Format(LocalString.GetString("[ffff00]累计充值{0} 倩女点即可享受以下特权"), info.ChargeCount)
            else
                this.m_VipDescriptionLabel.text = LocalString.GetString("[ffff00] 累计充值??,??? 倩女点即可享受以下特权")
            end
        else
            if info.ShowCharge > 0 or level <= CChargeWnd.VipInfo.Level + 1 then
                this.m_VipDescriptionLabel.text = System.String.Format(LocalString.GetString("累计充值[ffff00]{0}元[-]即可享受以下特权"), info.ChargeCount)
            else
                this.m_VipDescriptionLabel.text = LocalString.GetString("累计充值[ffff00] ??,??? 元[-]即可享受以下特权")
            end
        end
        this.m_PrivilegeLabel.text = CUICommonDef.TranslateToNGUIText(info.Description)
    else
        this.m_VipLevelLabel.text = ""
        this.m_VipDescriptionLabel.text = ""
        this.m_PrivilegeLabel.text = ""
    end
end
