-- Auto Generated!!
local CBWZQMgr = import "L10.Game.CBWZQMgr"
local CBWZQWatchWnd = import "L10.UI.CBWZQWatchWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local EnumClass = import "L10.Game.EnumClass"
local Extensions = import "Extensions"
local L10 = import "L10"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Profession = import "L10.Game.Profession"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
CBWZQWatchWnd.m_Awake_CS2LuaHook = function (this) 
    if CBWZQWatchWnd.Instance ~= nil then
        L10.CLogMgr.LogError("more than one instance running!")
    end
    CBWZQWatchWnd.Instance = this
end
CBWZQWatchWnd.m_GenerateNode_CS2LuaHook = function (this, data, index) 
    local node = NGUITools.AddChild(this.table.gameObject, this.template)
    node:SetActive(true)

    CommonDefs.GetComponent_Component_Type(node.transform:Find("Name/text"), typeof(UILabel)).text = data.callerName
    CommonDefs.GetComponent_Component_Type(node.transform:Find("Name/JobIcon"), typeof(UISprite)).spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), (data.clazz)))
    CommonDefs.GetComponent_Component_Type(node.transform:Find("lv"), typeof(UILabel)).text = "lv." .. data.level
    local button = node

    if index % 2 == 0 then
        CommonDefs.GetComponent_Component_Type(node.transform, typeof(UISprite)).spriteName = "mission_background_n"
    else
        CommonDefs.GetComponent_Component_Type(node.transform, typeof(UISprite)).spriteName = "common_textbg_02_dark"
    end

    local lightNode = node.transform:Find("hightlight").gameObject

    UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function (p) 
        this.savePlayerId = data.callerId
        if this.highlightNode ~= nil then
            this.highlightNode:SetActive(false)
        end

        this.highlightNode = lightNode
        this.highlightNode:SetActive(true)
    end)

    CommonDefs.GetComponent_Component_Type(node.transform:Find("cname"), typeof(UILabel)).text = data.chanllengeName
    if data.status == 0 then
        CommonDefs.GetComponent_Component_Type(node.transform:Find("status"), typeof(UILabel)).text = ""
    elseif data.status == 1 then
        CommonDefs.GetComponent_Component_Type(node.transform:Find("status"), typeof(UILabel)).text = LocalString.GetString("正在比武")
    elseif data.status == 2 then
        CommonDefs.GetComponent_Component_Type(node.transform:Find("status"), typeof(UILabel)).text = LocalString.GetString("等待挑战")
    else
        CommonDefs.GetComponent_Component_Type(node.transform:Find("status"), typeof(UILabel)).text = ""
    end
end
CBWZQWatchWnd.m_Init_CS2LuaHook = function (this) 
    this.savePlayerId = 0
    this.highlightNode = nil

    UIEventListener.Get(this.closeBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)

    UIEventListener.Get(this.submitBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        if this.savePlayerId > 0 then
            Gac2Gas.RequestZhaoQinGuanZhan(this.savePlayerId)
            this:Close()
        else
            g_MessageMgr:ShowMessage("GUANZHAN_NEED_CHOOSE")
        end
    end)

    UIEventListener.Get(this.refBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        Gac2Gas.GetZhaoQinGuanZhanInfo()
    end)

    this.template:SetActive(false)

    local gameInfoList = CBWZQMgr.Inst.riseGameInfoList
    Extensions.RemoveAllChildren(this.table.transform)

    if gameInfoList.Count > 0 then
        do
            local i = 0
            while i < gameInfoList.Count do
                local info = gameInfoList[i]
                this:GenerateNode(info, i)
                i = i + 1
            end
        end
        this.table:Reposition()
        this.scrollView:ResetPosition()
    else
        this:Close()
    end
end
