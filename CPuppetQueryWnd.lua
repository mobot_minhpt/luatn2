-- Auto Generated!!
local CHousePuppetMgr = import "L10.Game.CHousePuppetMgr"
local CPuppetQueryWnd = import "L10.UI.CPuppetQueryWnd"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
CPuppetQueryWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.backBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)
    this.templateNode:SetActive(false)

    local placeList = CHousePuppetMgr.Inst.HousePuppetPlaceList
    Extensions.RemoveAllChildren(this.table.transform)
    do
        local i = 0
        while i < placeList.Count do
            local info = placeList[i]
            if info ~= nil then
                local node = NGUITools.AddChild(this.table.gameObject, this.templateNode)
                node:SetActive(true)
                CommonDefs.GetComponent_Component_Type(node.transform:Find("Name"), typeof(UILabel)).text = info.ownerName
                CommonDefs.GetComponent_Component_Type(node.transform:Find("HouseName"), typeof(UILabel)).text = info.houseName
                local button = node.transform:Find("button").gameObject
                local houseButton = node.transform:Find("homebutton").gameObject
                UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function (p) 
                    this:DestroyPuppet(info.houseId)
                end)
                UIEventListener.Get(houseButton).onClick = DelegateFactory.VoidDelegate(function (p) 
                    this:GoToHouse(info.houseId)
                end)
            end
            i = i + 1
        end
    end
    this.table:Reposition()
    this.scrollView:ResetPosition()
end
CPuppetQueryWnd.m_DestroyPuppet_CS2LuaHook = function (this, houseId) 
    --MessageWndManager.ShowOKCancelMessage(MessageMgr.Inst.FormatMessage("PUPPET_DESTROY_NEED_MONEY"), delegate
    --{
    --    Gac2Gas.RemoveSelfPuppetAtHouse(houseId);
    --    this.Close();
    --});
    Gac2Gas.RemoveSelfPuppetAtHouse(houseId)
end
