local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local UIGrid = import "UIGrid"
local GameObject = import "UnityEngine.GameObject"
local UIScrollView = import "UIScrollView"
local UITable = import "UITable"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local QnButton = import "L10.UI.QnButton"
local CUITexture = import "L10.UI.CUITexture"
local MessageMgr = import "L10.Game.MessageMgr"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
-- copy from LuaFruitPartySignWnd
LuaShiTuFruitPartySignWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaShiTuFruitPartySignWnd, "m_TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaShiTuFruitPartySignWnd, "m_RuleScrollView", "RuleScrollView", UIScrollView)
RegistChildComponent(LuaShiTuFruitPartySignWnd, "m_RuleTable", "RuleTable", UITable)
RegistChildComponent(LuaShiTuFruitPartySignWnd, "m_ParagraphTemplate", "ParagraphTemplate", GameObject)
RegistChildComponent(LuaShiTuFruitPartySignWnd, "m_RankButton", "RankButton", QnButton)
RegistChildComponent(LuaShiTuFruitPartySignWnd, "m_SignButton", "SignButton", QnButton)

--@endregion RegistChildComponent end

function LuaShiTuFruitPartySignWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.m_RankButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankButtonClick()
	end)


	
	UIEventListener.Get(self.m_SignButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSignButtonClick()
	end)
    --根据反馈排行榜功能隐藏，屏蔽相关入口
    self.m_RankButton.gameObject:SetActive(false)
    --@endregion EventBind end
end

function LuaShiTuFruitPartySignWnd:Init()
	self.m_TimeLabel.text = ShuiGuoPaiDui_Setting.GetData().Time

	Extensions.RemoveAllChildren(self.m_RuleTable.transform)
    local ruleMessage = MessageMgr.Inst:FormatMessage("JIUXIAGUOYIN_SIGNWND_TIP", {})
    local ruleTipInfo = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(ruleMessage)
    if ruleTipInfo ~= nil then
        for i = 0, ruleTipInfo.paragraphs.Count - 1 do
            local rule = CUICommonDef.AddChild(self.m_RuleTable.gameObject, self.m_ParagraphTemplate):GetComponent(typeof(CTipParagraphItem))
            rule.gameObject:SetActive(true)
            rule:Init(ruleTipInfo.paragraphs[i], 0xffffffff)
        end
    end
    self.m_RuleTable:Reposition()
end

--@region UIEvent

function LuaShiTuFruitPartySignWnd:OnRankButtonClick()
    --LuaShiTuMgr:ShowShiTuFriutPartyRankWnd()
end

function LuaShiTuFruitPartySignWnd:OnSignButtonClick()
	LuaShiTuMgr:RequestEnterShuiGuoPaiDuiForShiTu()
end

--@endregion UIEvent