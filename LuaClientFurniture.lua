local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
--lua扩展会影响效率，还是关了吧
CLuaClientFurniture = {}

function CLuaClientFurniture.ProcessAddPuJiacunQiuQianInteractPlayer(this, pid, slotIdx)
    if not CLuaHouseMgr.sPujiacunQiuqianId then
        CLuaHouseMgr.sPujiacunQiuqianId = Zhuangshiwu_Setting.GetData().PujiacunQiuqianId
    end
    if this.TemplateId == CLuaHouseMgr.sPujiacunQiuqianId then
        if this.RO then
            local selfInQiuqian = false
            local m_tdata = Zhuangshiwu_Zhuangshiwu.GetData(this.m_tid)
            if m_tdata and m_tdata.SlotNum >= 1 then
                for i = 0, m_tdata.SlotNum - 1 do
                    if this.Children[i] ~= 0 then
                        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == this.Children[i] then
                            selfInQiuqian = true
                        end
                        CClientFurnitureMgr.Inst.sPCJQiuqianPlayerId = this.Children[i]
                    end
                end
            end

            this.RO:DoAni("stand02", true, 0, 1.0, 0.15, true, 1.0)

            if selfInQiuqian then
                CameraFollow.Inst:SetFollowObj(this.RO)
            end
        end
        this:UpdateQiuqianPlayerPosition(pid, slotIdx)
        CClientPlayerMgr.Inst:RefreshPlayer(CClientFurnitureMgr.Inst.sPCJQiuqianPlayerId)
    end
end
function CLuaClientFurniture.ProcessAddYinXianWanChairInteractPlayer(this, pid, slotIdx)
    if not CLuaHouseMgr.sYinXianWanChairId then
        CLuaHouseMgr.sYinXianWanChairId = GameplayItem_YinXianWan.GetData().FurnitureIds[0]
    end
    if this.TemplateId == CLuaHouseMgr.sYinXianWanChairId then
        if this.RO then
            this.RO:DoAni("stand02", true, 0, 1.0, 0.15, true, 1.0)
        end
        this:UpdateQiuqianPlayerPosition(pid, slotIdx)
    end
end
function CLuaClientFurniture.ProcessRemovePuJiacunQiuQianInteractPlayer(this, pid)
    --蒲家村秋千逻辑
    if not CLuaHouseMgr.sPujiacunQiuqianId then
        CLuaHouseMgr.sPujiacunQiuqianId = Zhuangshiwu_Setting.GetData().PujiacunQiuqianId
    end
    if this.TemplateId == CLuaHouseMgr.sPujiacunQiuqianId then
        local savePlayerId = CClientFurnitureMgr.Inst.sPCJQiuqianPlayerId
        CClientFurnitureMgr.Inst.sPCJQiuqianPlayerId = 0
        CClientPlayerMgr.Inst:RefreshPlayer(savePlayerId)
        local obj = nil
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == pid then
            obj = CClientMainPlayer.Inst
        else
            obj = CClientPlayerMgr.Inst:GetPlayer(pid)
        end
        if this.RO and obj and obj.RO and this.RO:IsAttached(obj.RO) then
            this.RO:DetachRO(obj.RO)
            obj.RO.gameObject.transform.localPosition = obj.WorldPos
            obj.RO.gameObject.transform.localScale = Vector3.one
            obj.RO.gameObject.transform.localEulerAngles = Vector3.zero
            obj.RO.FormerParent = nil
            this.RO:DoAni("stand01", true, 0, 1.0, 0.15, true, 1.0)
            obj:OnMoveStepped()
        end
    end
end
function CLuaClientFurniture.ProcessRemoveYinXianWanChairInteractPlayer(this, pid)
    if not CLuaHouseMgr.sYinXianWanChairId then
        CLuaHouseMgr.sYinXianWanChairId = GameplayItem_YinXianWan.GetData().FurnitureIds[0]
    end
    if this.TemplateId == CLuaHouseMgr.sYinXianWanChairId then
        local obj = nil
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == pid then
            obj = CClientMainPlayer.Inst
        else
            obj = CClientPlayerMgr.Inst:GetPlayer(pid)
        end
        if this.RO and obj and obj.RO and this.RO:IsAttached(obj.RO) then
            this.RO:DetachRO(obj.RO)
            obj.RO.gameObject.transform.localPosition = obj.WorldPos
            obj.RO.gameObject.transform.localScale = Vector3.one
            obj.RO.gameObject.transform.localEulerAngles = Vector3.zero
            obj.RO.FormerParent = nil
            this.RO:DoAni("stand01", true, 0, 1.0, 0.15, true, 1.0)
            obj:OnMoveStepped()
        end
    end
end
