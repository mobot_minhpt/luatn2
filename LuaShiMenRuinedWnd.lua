local UIPanel = import "UIPanel"
local CUITexture = import "L10.UI.CUITexture"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local Vector4 = import "UnityEngine.Vector4"
local CTrackMgr = import "L10.Game.CTrackMgr"

LuaShiMenRuinedWnd = class()

RegistChildComponent(LuaShiMenRuinedWnd, "RuinedPanel", "RuinedPanel", UIPanel)
RegistChildComponent(LuaShiMenRuinedWnd, "RuinedTexture", "RuinedTexture", CUITexture)
RegistChildComponent(LuaShiMenRuinedWnd, "RightTexture", "RightTexture", GameObject)
RegistChildComponent(LuaShiMenRuinedWnd, "LeftTexture", "LeftTexture", GameObject)
RegistChildComponent(LuaShiMenRuinedWnd, "DesLabel", "DesLabel", UILabel)
RegistChildComponent(LuaShiMenRuinedWnd, "FindWayLabel", "FindWayLabel", UILabel)

RegistClassMember(LuaShiMenRuinedWnd, "m_RuinedTextureWith")
RegistClassMember(LuaShiMenRuinedWnd, "m_RuinedTextureHeight")
RegistClassMember(LuaShiMenRuinedWnd, "m_RightPaintedScrollInitialX")
RegistClassMember(LuaShiMenRuinedWnd, "m_RightPaintedScrollInitialY")
RegistClassMember(LuaShiMenRuinedWnd, "m_LeftPaintedScrollInitialX")
RegistClassMember(LuaShiMenRuinedWnd, "m_LeftPaintedScrollInitialY")

function LuaShiMenRuinedWnd:Init()
    self.m_RuinedTextureWith = self.RuinedTexture.texture.width
    -- self.m_RuinedTextureHeight = self.RuinedTexture.texture.height
    self.m_RightPaintedScrollInitialX = self.RightTexture.transform.localPosition.x
    self.m_RightPaintedScrollInitialY = self.RightTexture.transform.localPosition.y
    self.m_LeftPaintedScrollInitialX = self.LeftTexture.transform.localPosition.x
    self.m_LeftPaintedScrollInitialY = self.LeftTexture.transform.localPosition.y
    local tweener = LuaTweenUtils.TweenFloat(0, 1200, 2, function ( val )
        self.RuinedPanel.baseClipRegion = Vector4(0,0, val,self.RuinedPanel.baseClipRegion.w)
        self.RightTexture.transform.localPosition = Vector3(self.m_RightPaintedScrollInitialX + val / 2, self.m_RightPaintedScrollInitialY, 0)
        self.LeftTexture.transform.localPosition = Vector3(self.m_LeftPaintedScrollInitialX - val / 2, self.m_LeftPaintedScrollInitialY, 0)
    end)
    local data = ShiMenShouWei_Ruin.GetData(LuaZongMenMgr.m_SMSW_DestroySceneId)
    self.DesLabel.text = data.Description
    UIEventListener.Get(self.FindWayLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        local findnpcData = ShiMenShouWei_Settings.GetData().FindNpc
	    CTrackMgr.Inst:FindNPC(findnpcData[1],findnpcData[0],findnpcData[2], findnpcData[3], nil, nil)
	end)
    LuaTweenUtils.SetTarget(tweener, self.transform)
    self.RuinedTexture:LoadMaterial(SafeStringFormat3("UI/Texture/NonTransparent/Material/shimenruined_%s.mat",PublicMap_MiniMapRes.GetData(LuaZongMenMgr.m_SMSW_DestroySceneId).ResName))
end

function LuaShiMenRuinedWnd:OnDisable()
    LuaTweenUtils.DOKill(self.transform, false)
end

--@region UIEvent

--@endregion

