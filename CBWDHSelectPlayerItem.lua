-- Auto Generated!!
local AlignType = import "CPlayerInfoMgr+AlignType"
local Boolean = import "System.Boolean"
local CBiWuDaHuiMgr = import "L10.Game.CBiWuDaHuiMgr"
local CBWDHSelectPlayerItem = import "L10.UI.CBWDHSelectPlayerItem"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local LocalString = import "LocalString"
local QnButton = import "L10.UI.QnButton"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UIEventListener = import "UIEventListener"
local Vector3 = import "UnityEngine.Vector3"
CBWDHSelectPlayerItem.m_Awake_CS2LuaHook = function (this) 
    if this.btns ~= nil then
        do
            local i = 0
            while i < this.btns.Length do
                this.btns[i].OnButtonSelected_02 = MakeDelegateFromCSFunction(this.OnItemSelected, MakeGenericClass(Action2, QnSelectableButton, Boolean), this)
                this.btns[i].OnClick = MakeDelegateFromCSFunction(this.OnClickCallback, MakeGenericClass(Action1, QnButton), this)
                i = i + 1
            end
        end
    end
    UIEventListener.Get(this.headGo).onClick = DelegateFactory.VoidDelegate(function (p) 
        if this.playerId > 0 then
            CPlayerInfoMgr.ShowPlayerPopupMenu(this.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
        end
    end)
end
CBWDHSelectPlayerItem.m_OnClickCallback_CS2LuaHook = function (this, btn) 
    if not LuaBiWuDaHuiMgr.IsLeader() then
        g_MessageMgr:ShowMessage("BIWU_TEAMMEMBER_NOT_CHANGE_ORDER")
    end
end
CBWDHSelectPlayerItem.m_OnItemSelected_CS2LuaHook = function (this, go, select) 
    if select then
        if go == this.btns[0] then
            this:SetSection(0)
        elseif go == this.btns[1] then
            this:SetSection(1)
        elseif go == this.btns[2] then
            this:SetSection(2)
        end
    end
end
CBWDHSelectPlayerItem.m_Init_CS2LuaHook = function (this, info, index) 
    if not LuaBiWuDaHuiMgr.IsLeader() then
        if this.btns ~= nil then
            do
                local i = 0
                while i < this.btns.Length do
                    this.btns[i].Selectable = false
                    i = i + 1
                end
            end
        end
    else
        if this.btns ~= nil then
            do
                local i = 0
                while i < this.btns.Length do
                    this.btns[i].Selectable = true
                    i = i + 1
                end
            end
        end
    end

    if index % 2 == 0 then
        this.bgSprite.enabled = false
    else
        this.bgSprite.enabled = true
    end
    this.mInfo = info
    this.nameLabel.text = info.playerName
    this.levelLabel.text = tostring(info.level)
    this.portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(info.cls, info.sex, -1), false)
end
CBWDHSelectPlayerItem.m_InitResult_CS2LuaHook = function (this, info, result, index) 
    if info == nil then
        return
    end
    this:Init(info, index)
    local roundStr = ""
    if result ~= nil then
        repeat
            local default = result.round
            if default == 1 then
                this.resultLabel.gameObject:SetActive(true)
                roundStr = LocalString.GetString("一")
                break
            elseif default == 2 then
                this.resultLabel.gameObject:SetActive(true)
                roundStr = LocalString.GetString("二")
                break
            elseif default == 3 then
                this.resultLabel.gameObject:SetActive(true)
                roundStr = LocalString.GetString("三")
                break
            elseif default == 4 then
                this.resultLabel.gameObject:SetActive(true)
                roundStr = LocalString.GetString("四")
                break
            elseif default == 5 then
                this.resultLabel.gameObject:SetActive(true)
                roundStr = LocalString.GetString("五")
                break
            else
                this.resultLabel.gameObject:SetActive(false)
                break
            end
        until 1
        this.resultLabel.text = roundStr
        local isTeamRound = CBiWuDaHuiMgr.IsTeamRound(result.round)
        --团队赛
        if not isTeamRound then
            this.teamTypeLabel.text = System.String.Format(LocalString.GetString("第{0}场个人赛"), roundStr)
            this.teamTypeSprite.spriteName = CBWDHSelectPlayerItem.PersonalSpriteName
            this.teamTypeSprite:MakePixelPerfect()
        else
            this.teamTypeLabel.text = LocalString.GetString("只参加团体赛")
            this.teamTypeSprite.spriteName = CBWDHSelectPlayerItem.TeamSpriteName
            this.teamTypeSprite:MakePixelPerfect()
        end
    else
        this.resultLabel.gameObject:SetActive(false)
        this.teamTypeLabel.text = LocalString.GetString("只参加团体赛")
        this.teamTypeSprite.spriteName = CBWDHSelectPlayerItem.TeamSpriteName
        this.teamTypeSprite:MakePixelPerfect()
    end
end
CBWDHSelectPlayerItem.m_GetSelection_CS2LuaHook = function (this) 
    if this.btns ~= nil and this.btns.Length == 3 then
        if this.btns[0] ~= nil and this.btns[0]:isSeleted() then
            return 1
        elseif this.btns[1] ~= nil and this.btns[1]:isSeleted() then
            return 3
        elseif this.btns[2] ~= nil and this.btns[2]:isSeleted() then
            return 5
        end
    end
    return 0
end
CBWDHSelectPlayerItem.m_SetSection_CS2LuaHook = function (this, index) 
    do
        local i = 0
        while i < this.btns.Length do
            if i ~= index then
                if this.btns[i] ~= nil and this.btns[i]:isSeleted() then
                    this.btns[i]:SetSelected(false, false)
                end
            else
                if not this.btns[i]:isSeleted() then
                    this.btns[i]:SetSelected(true, false)
                end
            end
            i = i + 1
        end
    end
    local cmps = CommonDefs.GetComponentsInChildren_Component_Type(this.transform.parent, typeof(CBWDHSelectPlayerItem))
    do
        local i = 0
        while i < cmps.Length do
            if cmps[i] ~= this then
                cmps[i]:CancleSection(index)
            end
            i = i + 1
        end
    end
end
