local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local Profession        = import "L10.Game.Profession"

LuaArenaPassportGiftingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaArenaPassportGiftingWnd, "descTable", "DescTable", Transform)
RegistChildComponent(LuaArenaPassportGiftingWnd, "tableView", "TableView", QnAdvanceGridView)

--@endregion RegistChildComponent end

RegistClassMember(LuaArenaPassportGiftingWnd, "friendList")
RegistClassMember(LuaArenaPassportGiftingWnd, "gotLevel")

function LuaArenaPassportGiftingWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaArenaPassportGiftingWnd:OnEnable()
    g_ScriptEvent:AddListener("SendArenaPassportGiftInfo", self, "OnSendArenaPassportGiftInfo")
end

function LuaArenaPassportGiftingWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendArenaPassportGiftInfo", self, "OnSendArenaPassportGiftInfo")
end

function LuaArenaPassportGiftingWnd:OnSendArenaPassportGiftInfo(jadeLimit, gotLevel, friendData)
    self.gotLevel = gotLevel
    self.descTable.gameObject:SetActive(true)
    local label1 = self.descTable:GetChild(0):Find("Label"):GetComponent(typeof(UILabel))
    local label2 = self.descTable:GetChild(1):Find("Label"):GetComponent(typeof(UILabel))

    local setting = Arena_PassportSetting.GetData()
    label1.text = SafeStringFormat3(LocalString.GetString("本月剩余灵玉赠送额度 %d"), jadeLimit)
    label2.text = SafeStringFormat3(LocalString.GetString("每赠送%d级本人可以获得1级战令等级，通过该方式最多可获得%d级战令等级。（当前已获得%d/%d）"),
        setting.GiftLevelRequired, setting.MaxLevelByGifting, gotLevel, setting.MaxLevelByGifting)
    friendData = g_MessagePack.unpack(friendData)
    self.friendList = {}
    for playerId, data in pairs(friendData) do
        table.insert(self.friendList, {
            playerId = playerId,
            name = data.name,
            class = data.class,
            gender = data.gender,
            level = data.level,
            isFeiSheng = data.isFeiSheng,
            passportLevel = data.passportLevel,
            state = data.state,
        })
    end
    self.tableView:ReloadData(true, false)
end

function LuaArenaPassportGiftingWnd:Init()
    self.descTable.gameObject:SetActive(false)

    self.friendList = {}
    self.tableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.friendList
        end,
        function(item, index)
            self:InitItem(item, index)
        end
    )
    Gac2Gas.QueryArenaPassportGiftInfo()
end

function LuaArenaPassportGiftingWnd:InitItem(item, index)
    local data = self.friendList[index + 1]
    local portrait = item.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
    portrait:LoadPortrait(CUICommonDef.GetPortraitName(data.class, data.gender, -1), true)
    local level = item.transform:Find("Portrait/Level"):GetComponent(typeof(UILabel))
    level.text = SafeStringFormat3("[%s]lv.%d[-]", data.isFeiSheng and Constants.ColorOfFeiSheng or "FFFFFF", data.level)
    local class = item.transform:Find("Class"):GetComponent(typeof(UISprite))
    class.spriteName = Profession.GetIconByNumber(data.class)
    local passportLevel = item.transform:Find("PassportLevel"):GetComponent(typeof(UILabel))
    passportLevel.text = SafeStringFormat3(LocalString.GetString("战令 %d级"), data.passportLevel)
    item.transform:Find("State"):GetComponent(typeof(UILabel)).text = LuaArenaMgr:GetStateText(data.state)

    UIEventListener.Get(item.transform:Find("Button").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnButtonClick(index)
    end)
end

--@region UIEvent

function LuaArenaPassportGiftingWnd:OnButtonClick(index)
    local data = self.friendList[index + 1]
    if data.passportLevel == Arena_PassportLevelReward.GetDataCount() then
        g_MessageMgr:ShowMessage("ARENA_PASSPORT_GIFT_HAS_REACHED_MAX_LEVEL")
        return
    end

    LuaArenaMgr.buyLevelInfo = {}
    LuaArenaMgr.buyLevelInfo.level = data.passportLevel
    LuaArenaMgr.buyLevelInfo.name = data.name
    LuaArenaMgr.buyLevelInfo.isGifting = true
    LuaArenaMgr.buyLevelInfo.gotLevel = self.gotLevel
    CUIManager.ShowUI(CLuaUIResources.ArenaPassportBuyLevelWnd)
end

--@endregion UIEvent

