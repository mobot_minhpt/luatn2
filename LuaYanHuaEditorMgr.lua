local Color = import "UnityEngine.Color"
local CEffectMgr = import "L10.Game.CEffectMgr"
local Utility = import "L10.Engine.Utility"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CPos = import "L10.Engine.CPos"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local TextureFormat = import "UnityEngine.TextureFormat"
local Texture2D = import "UnityEngine.Texture2D"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CPlayerDataMgr = import "L10.Game.CPlayerDataMgr"
local CScene = import "L10.Game.CScene"
local CTrackMgr = import "L10.Game.CTrackMgr"
local UITexture = import "UITexture"

local Main = import "L10.Engine.Main"
local HTTPHelper = import "L10.Game.HTTPHelper"
local LocalString = import "LocalString"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow" 
local CWorldPositionFX = import "L10.Game.CWorldPositionFX"
local CCustomParticleEffect = import "L10.Game.CCustomParticleEffect"

CLuaYanHuaEditorMgr = class()
CLuaYanHuaEditorMgr.YanHuaMaxSize = 5
CLuaYanHuaEditorMgr.MinIconSize = 60
CLuaYanHuaEditorMgr.MaxIconSize = 120
CLuaYanHuaEditorMgr.OpenWndByYanHuaView = false
CLuaYanHuaEditorMgr.CustomYanhuaDefaultSize = 3
CLuaYanHuaEditorMgr.CurLightYanHuaSceneName = nil
CLuaYanHuaEditorMgr.ViewCameraRZY = {30,10}
CLuaYanHuaEditorMgr.CloseViewTime = 63000
--最多可以创建的画布数量
CLuaYanHuaEditorMgr.MaxCanvasCount = 10

CLuaYanHuaEditorMgr.EnumCanvasStatus = {
    ePixel = 0,
    eFree = 1,
}
CLuaYanHuaEditorMgr.EnumYanHuaType = {
    eNormal = 0,
    eCustom = 1,
}
CLuaYanHuaEditorMgr.EnumAuditStatus = {
    eInAudi = 0,--审核中
    ePass   = 1,--审核通过
    eRefuse = -1,--审核不通过
}
CLuaYanHuaEditorMgr.AuditStatusText = 
{
    [1] = LocalString.GetString("[ffff00]审核中[-]"),
    [2] = LocalString.GetString("[00ff00]已过审[-]"),
    [0] = LocalString.GetString("[ff5050]未过审[-]"),
    [4] = LocalString.GetString("[ffffff]待审核[-]"),
}

CLuaYanHuaEditorMgr.YanHuaColors = {
    [1] = Color(1,0.35,0.35,1),
    [2] = Color(0.38,1,0.39,1),--"FF6C6C",
    [3] =Color(1,0.54,0.32,1),
    [4] = Color(1,0.88,0.36,1),
    [5] = Color(0.86,0.52,1,1),
    [6] = Color(0.23,0.75,1,1),
}

CLuaYanHuaEditorMgr.Colors = {
    [1] = Color(1,0,0,1),
    [2] = Color(0.36,1,0.54,1),
    [3] = Color(1,0.43,0,1),
    [4] = Color(1,0.99,0.25,1),
    [5] = Color(0.75,0.08,1,1),
    [6] =Color(0.34,0.73,1,1),
}
CLuaYanHuaEditorMgr.ClearColor = Color(1,1,1,0)

CLuaYanHuaEditorMgr.CanvasList = {}
CLuaYanHuaEditorMgr.AddedCanvasCount = 0
CLuaYanHuaEditorMgr.SelectCustomIndex = nil

CLuaYanHuaEditorMgr.IsShowViewBtn = false

--添加到库中的自定义烟花 最多四个
CLuaYanHuaEditorMgr.CustomYanHuaCanUseTbl = {}
--燃放列表 礼包顺序(type,id/url,size,color)
CLuaYanHuaEditorMgr.YanHuaLightListTbl = {}
CLuaYanHuaEditorMgr.CanLight = false

--已经添加到烟花礼包库里的自定义烟花
CLuaYanHuaEditorMgr.AddedCustomCanvasList = {}
--最多可以添加到库的烟花数量
CLuaYanHuaEditorMgr.MaxAddCustomCount = 4
CLuaYanHuaEditorMgr.DefaultCustomSize = 1

CLuaYanHuaEditorMgr.MyYanHuaPicData = {}
CLuaYanHuaEditorMgr.WebPicCount = 0
CLuaYanHuaEditorMgr.ApplyCannotEditComfire = LocalString.GetString("添加至烟花库的烟花设计需要进行审核，审核中无法修改或删除，审核通过后方可燃放，确定添加吗？")
CLuaYanHuaEditorMgr.DeleteNimTip = LocalString.GetString("请选择一块画布!")
function CLuaYanHuaEditorMgr.OpenYanHuaLiBaoEditor(onRefreshData)--LoadYanHuaPicDataFromWeb
    CLuaYanHuaEditorMgr.MyYanHuaPicData = {}
    for i=1,CLuaYanHuaEditorMgr.MaxCanvasCount,1 do
        table.insert(CLuaYanHuaEditorMgr.MyYanHuaPicData,{})
    end
    if not onRefreshData then
        CUIManager.ShowUI(CLuaUIResources.YanHuaLiBaoEditorWnd)
    end

    local player = CClientMainPlayer.Inst
    if not player then return end
    local playerId = player.Id
    LuaPersonalSpaceMgrReal.GetFireworkPicListByPlayerId(playerId,function(data)
        local count = 0
        if not data.data then 
            CUIManager.ShowUI(CLuaUIResources.YanHuaLiBaoEditorWnd)
            return 
        end
        count = data.data.count
        CLuaYanHuaEditorMgr.WebPicCount = count
        local list = data.data.list
        local finishCount = 0
        if count == 0 then
            CUIManager.ShowUI(CLuaUIResources.YanHuaLiBaoEditorWnd)
            return
        end
        for k,v in pairs(list) do
            local t = {}
            t.index = tonumber(v.index)
            t.url = v.url
            t.auditStatus = v.auditStatus
            t.id = v.id
            CLuaYanHuaEditorMgr.MyYanHuaPicData[t.index+1] = t
            --
            local icon = CreateFromClass(UITexture)
            Main.Inst:StartCoroutine(HTTPHelper.NOSDownloadFileData(t.url, DelegateFactory.Action_bool_byte_Array(function (sign, bytes) 
                local _texture = CreateFromClass(Texture2D, 256, 256, TextureFormat.RGBA32, false)
                CommonDefs.LoadImage(_texture,bytes)
                finishCount = finishCount + 1
                CLuaYanHuaEditorMgr.MyYanHuaPicData[t.index+1].texture = _texture
                if finishCount == count and not onRefreshData then
                    CUIManager.ShowUI(CLuaUIResources.YanHuaLiBaoEditorWnd)
                end
            end)))
            --
        end
    end)
end
function CLuaYanHuaEditorMgr.RefreshMyYanHuaPicData()
    CLuaYanHuaEditorMgr.OpenYanHuaLiBaoEditor(true)
end

function CLuaYanHuaEditorMgr.Url2PicIcon(url,icon)
    if not url or url == "" then  
        icon.mainTexture = nil
        return 
    end
    local width = 56
    local height = 56
    CPersonalSpaceMgr.DownLoadPic(url, icon, width, height, nil, false, false)
end


function CLuaYanHuaEditorMgr.Url2Pic(url)
    if not url or url == "" then  
        return nil
    end
    local width = 256
    local height = 256
    local icon = CreateFromClass(UITexture)
    CPersonalSpaceMgr.DownLoadPic(url, icon, width, height, nil, false, false)
    return icon.mainTexture
end

function CLuaYanHuaEditorMgr.ColorList2Pic(colorList)
    if not colorList then return nil end
    local width = 252
    local height = 252
    local scale= width/14
    local tex = CreateFromClass(Texture2D, width, width, TextureFormat.ARGB32, false)
    for x = 0,14-1,1 do
        for y = 0,14-1,1 do
            local index = 14*x + y + 1
            local colorIndex = colorList[index]
            if colorIndex ~= nil then
                local color = CLuaYanHuaEditorMgr.Colors[colorIndex]
                if colorIndex == 0 then color = CLuaYanHuaEditorMgr.ClearColor end
                for k=0,scale-1,1 do
                    for z=0,scale-1,1 do
                        tex:SetPixel(scale*x+k,scale*y+z,color)
                    end
                end
                
            end
        end
    end
    tex:Apply()
    return tex
end

function CLuaYanHuaEditorMgr.Track(gridPos)
	if not CScene.MainScene then return end

	local pixelPos = Utility.GridPos2PixelPos(gridPos)
	CTrackMgr.Inst:Track(nil, CScene.MainScene.SceneTemplateId, pixelPos, Utility.Grid2Pixel(2.0), DelegateFactory.Action(function()
        --successFunc
        CLuaYanHuaEditorMgr.OpenWndByYanHuaView = true
        CUIManager.ShowUI(CUIResources.CameraResetWnd)
      end), nil, nil, nil, true)
end

--燃放烟花
CLuaYanHuaEditorMgr.ViewEnterTime = 0
CLuaYanHuaEditorMgr.DefaultViewTime = 66
CLuaYanHuaEditorMgr.CanViewDistance = 50
CLuaYanHuaEditorMgr.AuditSwitchOn = true
function CLuaYanHuaEditorMgr.PlayYanHuaLiBao(x,y,str,dir,playerId)
    --先来判断距离
    dir = dir - 90
    local player = CClientMainPlayer.Inst
    if not player then return end
    if not CScene.MainScene then return end
    CLuaYanHuaEditorMgr.InYanHuaViewStatus = true
    CLuaYanHuaEditorMgr.LightPos = Utility.PixelPos2WorldPos(x,y)--Vector2(x,y)
    CLuaYanHuaEditorMgr.CurLightYanHuaSceneName = CScene.MainScene.SceneName
    if playerId == player.Id then
        CLuaYanHuaEditorMgr.OpenWndByYanHuaView = true
        CLuaYanHuaEditorMgr.OnYanHuaViewBtnClicked()
    end

    local gridPos = Utility.PixelPos2GridPos(CPos(x,y))
    local xPos = math.floor(gridPos.x) + 0.5
    local zPos = math.floor(gridPos.y) + 0.5
    local terrainY = CRenderScene.Inst:SampleLogicHeight(xPos, zPos)
    CLuaYanHuaEditorMgr.LightPos.y = terrainY
    local pos = CLuaYanHuaEditorMgr.LightPos
    local pos2 = Vector3(pos.x,pos.y+10,pos.z)

    local dataTbl = {}
    local loadedTextureTbl = {}
    local isCuston = false
    local textureCount = 0
    for substr in string.gmatch(str, "([^;]+)") do
        local t = {}
        local i = 1
        for sub in string.gmatch(substr, "([^,]+)") do
            if i==1 then
                isCuston = (tonumber(sub)==1)
            elseif i == 2 and isCuston == true then
                isCuston = false
                local url = sub
                --table.insert(loadedTextureTbl,url)
                if not loadedTextureTbl[url] then
                    textureCount = textureCount + 1
                    loadedTextureTbl[url] = url
                end
            else
                isCuston = false
            end
            table.insert(t,sub)
            i = i + 1
        end
        table.insert(dataTbl,t)
    end
    local texIndex = 0
    for url,tex in pairs(loadedTextureTbl) do
        texIndex = texIndex + 1
        Main.Inst:StartCoroutine(HTTPHelper.NOSDownloadFileData(url, DelegateFactory.Action_bool_byte_Array(function (sign, bytes) 
            local _texture = CreateFromClass(Texture2D, 256, 256, TextureFormat.RGBA32, false)
            CommonDefs.LoadImage(_texture,bytes)
            loadedTextureTbl[url] = _texture
            if texIndex == textureCount then
                CLuaYanHuaEditorMgr.playYanHuaFunc(pos,pos2,dir,dataTbl,loadedTextureTbl)
            end
        end)))
    end
    if textureCount == 0 then
        CLuaYanHuaEditorMgr.playYanHuaFunc(pos,pos2,dir,dataTbl,loadedTextureTbl)
    end
end

CLuaYanHuaEditorMgr.LightingTickList = {}
function CLuaYanHuaEditorMgr.playYanHuaFunc(pos,pos2,dir,dataTbl,loadedTextureTbl)
    local index = 0
    local pixIndex = 1
    local yanHuaTongFxId = YanHua_Setting.GetData().YanHuaTongFxId
    local yanHuaTongFx = CEffectMgr.Inst:AddWorldPositionFX(yanHuaTongFxId, pos, dir,0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
    local lightingTick = RegisterTickWithDuration(function ()
        index = index + 1
        local info = dataTbl[index]
        if info and tonumber(info[1]) == 0 then
            local normalId = tonumber(info[2])
            local data = YanHua_Default.GetData(normalId)
            local fxid1 = data.FxID1
            local shengKong = data.ShengkongEffect
            local colors = nil
            local colorIndex = tonumber(info[4])
            if colorIndex then
                local color = CLuaYanHuaEditorMgr.Colors[colorIndex]
                if color then
                    colors = CreateFromClass(MakeArrayClass(Color), 1)
                    colors[0] = color
                end
            end
            local size = 1
            if info[3] then
                size = tonumber(info[3])
            end
            if shengKong==1 then
                local fxid2 = data.FxID2             
                local fx2 = CEffectMgr.Inst:AddCustomYanHuaWpFX(fxid2, pos, dir,1,colors,nil,0, 1, -1, EnumWarnFXType.None, nil, 0, 0)
                local fx1 = CEffectMgr.Inst:AddCustomYanHuaWpFX(fxid1, pos2, dir, size,colors,nil,1000, 1, -1, EnumWarnFXType.None, nil, 0, 0)
            else
                local fx1 = CEffectMgr.Inst:AddCustomYanHuaWpFX(fxid1, pos, dir, size,colors,nil,0, 1, -1, EnumWarnFXType.None, nil, 0, 0)
            end
        elseif info and tonumber(info[1]) == 1 then
            local url = info[2]
            local texture2d = loadedTextureTbl[url]
            --pixIndex = pixIndex + 1
            local size = 1
            if info[3] then
                size = tonumber(info[3])
            end
            local fx2 = CEffectMgr.Inst:AddCustomYanHuaWpFX(88801851, pos, dir,1,nil,nil,0, 1, -1, EnumWarnFXType.None, nil, 0, 0)
            --local fx1 = CEffectMgr.Inst:AddCustomYanHuaWpFX(88801838, pos2, dir, size,nil,texture2d,1000, 1, -1, EnumWarnFXType.None, nil, 0, 0)
            local onLoadFinish = DelegateFactory.Action_GameObject(function(go)
                if not go then return end
                local wf = go.transform:GetComponent(typeof(CWorldPositionFX))
                wf.m_Scale = size
                local cusEffects = CommonDefs.GetComponentsInChildren_Component_Type(go.transform, typeof(CCustomParticleEffect))
                if cusEffects then
                    local cusEffect = cusEffects[0]
                    if cusEffect then
                        cusEffect.sampleTexture = texture2d
                        cusEffect.m_Scale = size
                    end
                end
                wf.Inited = false
                wf:Init(wf.m_FX, pos2, dir, 1000, 1, size, wf.ParticleColors, texture2d,wf.ParticleHSVColors)
            end)
            local fx1 = CEffectMgr.Inst:AddWorldPositionFX(88801838, pos2, dir, 1000, 1, -1, EnumWarnFXType.None, nil, 0, 0, onLoadFinish)
        end
    end,3000,60000)
    table.insert(CLuaYanHuaEditorMgr.LightingTickList,lightingTick)

    if CLuaYanHuaEditorMgr.CloseViewTick ~= nil then
        UnRegisterTick(CLuaYanHuaEditorMgr.CloseViewTick)
    end
    CLuaYanHuaEditorMgr.CloseViewTick = RegisterTickWithDuration(function ()
        CLuaYanHuaEditorMgr.InYanHuaViewStatus = false
        CLuaYanHuaEditorMgr.CurLightYanHuaSceneName = nil
        if yanHuaTongFx then
            yanHuaTongFx:Destroy()
        end
    end,CLuaYanHuaEditorMgr.CloseViewTime,CLuaYanHuaEditorMgr.CloseViewTime)
    
    RegisterTickWithDuration(function ()
        if yanHuaTongFx then
            yanHuaTongFx:Destroy()
        end
    end,CLuaYanHuaEditorMgr.CloseViewTime,CLuaYanHuaEditorMgr.CloseViewTime)
    
    g_ScriptEvent:BroadcastInLua("ShowYanHuaViewBtn")
end

CLuaYanHuaEditorMgr.CurPreViewLiBaoIndex = nil
function CLuaYanHuaEditorMgr.PreviewNormalYanhua()
    CUIManager.ShowUI(CLuaUIResources.YanHuaSinglePreviewWnd)
end


CLuaYanHuaEditorMgr.PicListReadyForLightDelegate = DelegateFactory.Action(function ()
end)

function CLuaYanHuaEditorMgr.OnSaveYanHuaDesignData()
    --CLuaYanHuaEditorMgr.OriYanHuaDesignData
    local json = luaJson.table2json(CLuaYanHuaEditorMgr.OriYanHuaDesignData)
    CPlayerDataMgr.Inst:SavePlayerData("yanhuaedesigndata",json)
end

function CLuaYanHuaEditorMgr.OnLoadYanHuaDesignData()
    CLuaYanHuaEditorMgr.CanvasList = {}
    CLuaYanHuaEditorMgr.OriYanHuaDesignData = {}
    local list = {}
    local json = CPlayerDataMgr.Inst:LoadPlayerData("yanhuaedesigndata")
    if json~=nil and json~="" then
        list = luaJson.json2lua(json)
    end

    if list then
        local keyTbl = {}
        local num = 0
        for key,v in pairs(list) do
            num = num + 1
            table.insert(keyTbl,key)
        end
        for i=1,num,1 do
            table.insert(CLuaYanHuaEditorMgr.CanvasList,{})
            table.insert(CLuaYanHuaEditorMgr.OriYanHuaDesignData,{})
        end
        for key,v in pairs(list) do
            if key ~= "pixelItemColorList" then
                CLuaYanHuaEditorMgr.CanvasList[tonumber(key)] = v
                CLuaYanHuaEditorMgr.OriYanHuaDesignData[tonumber(key)] = v
            else
                local pixelItemColorList1 = {}
                local pixelItemColorList2 = {}
                for i,d in ipairs(v) do
                    pixelItemColorList1[i] = d
                    pixelItemColorList2[i] = d
                end
                CLuaYanHuaEditorMgr.CanvasList[tonumber(key)] = pixelItemColorList1
                CLuaYanHuaEditorMgr.OriYanHuaDesignData[tonumber(key)] = pixelItemColorList2
            end
        end
    end

end

function CLuaYanHuaEditorMgr.OnYanHuaViewBtnClicked()
    local player = CClientMainPlayer.Inst
    if not player then return end
    if not CLuaYanHuaEditorMgr.LightPos then return end
    if not CScene.MainScene then return end
    local myPos = player.RO.Position
    local vec1 = myPos
    local vec2 = CLuaYanHuaEditorMgr.LightPos
    local distance = Vector3.Distance(vec1,vec2)
    if distance and distance > CLuaYanHuaEditorMgr.CanViewDistance then
        local sceneTemplateId = CScene.MainScene.SceneTemplateId
        local sceneName = CScene.MainScene.SceneName
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("GoTo_View_YanHua_Comfirm",sceneTemplateId,sceneName,math.floor(vec2.x),math.floor(vec2.z)), 
            DelegateFactory.Action(function ()            
                CLuaYanHuaEditorMgr.Track(CPos(math.floor(vec2.x),math.floor(vec2.z)))
            end),
           nil,
            LocalString.GetString("前往"), LocalString.GetString("取消"), false)
    else
        CLuaYanHuaEditorMgr.OpenWndByYanHuaView = true
        CameraFollow.Inst.MaxSpHeight = YanHua_Setting.GetData().MaxSpHeight
        CameraFollow.Inst.m_T = 1
        CameraFollow.Inst:SetCurrentRZY()
        local vec3 = CameraFollow.Inst.targetRZY
        vec3.y = CLuaYanHuaEditorMgr.ViewCameraRZY[1]
        vec3.z = CLuaYanHuaEditorMgr.ViewCameraRZY[2]
        CameraFollow.Inst.targetRZY = vec3
        CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
    end
end

--心心相印烟花筒
function CLuaYanHuaEditorMgr.AddWorldPositionMultiFxWithDir(x, y, fxList, dir)
    CUIManager.CloseUI(CUIResources.PackageWnd)
    CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
    if not CScene.MainScene then return end
    local sceneTemplateId = CScene.MainScene.SceneTemplateId
    local pos = Utility.GridPos2PixelPos(CPos(x, y))
    local worldPos = Utility.PixelPos2WorldPos(pos)

    local fxTbl = {}
    for shengkongId, zhanfangId,duration,shengkongSize,height in string.gmatch(fxList, "(%d+),(%d+),([%d.]+),([%d.]+),([%d.]+);") do
        local t = {}
        t.shengkongId = tonumber(shengkongId)
        t.zhanfangId = tonumber(zhanfangId)
        t.duration = tonumber(duration)
        t.shengkongSize = tonumber(shengkongSize)
        t.height = tonumber(height)
        table.insert(fxTbl,t)
    end

    if fxTbl and next(fxTbl) then
        local fxCount = #fxTbl
        local index = 1
        local data = fxTbl[index]
        local shengkongId = data.shengkongId
        local zhanfangId = data.zhanfangId
        local size = data.shengkongSize
        local height = data.height * size
        if shengkongId and shengkongId ~= 0 then
            local pos2 = Vector3(worldPos.x,worldPos.y + height,worldPos.z)
            local up = CEffectMgr.Inst:AddCustomYanHuaWpFX(shengkongId, worldPos, dir,size,nil,nil,0, 1, -1, EnumWarnFXType.None, nil, 0, 0)
            CEffectMgr.Inst:AddWorldPositionFX(zhanfangId, pos2, dir, 1000, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
        else
            CEffectMgr.Inst:AddWorldPositionFX(zhanfangId, worldPos, dir, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
        end

        local function tickFun() 
            index = index + 1
            local data = fxTbl[index]
            if data then
                local zhanfangId = data.zhanfangId
                local shengkongId = data.shengkongId
                local size = data.shengkongSize
                local height = data.height * size
                if shengkongId and shengkongId~= 0 then
                    local pos2 = Vector3(worldPos.x,worldPos.y + height,worldPos.z)
                    local up = CEffectMgr.Inst:AddCustomYanHuaWpFX(shengkongId, worldPos, dir,size,nil,nil,0, 1, -1, EnumWarnFXType.None, nil, 0, 0)
                    fx = CEffectMgr.Inst:AddWorldPositionFX(zhanfangId, pos2, dir, 1000, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
                else
                    fx = CEffectMgr.Inst:AddWorldPositionFX(zhanfangId, worldPos, dir, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
                end
                --print(zhanfangId)
                if index < fxCount and sceneTemplateId == CScene.MainScene.SceneTemplateId then
                    RegisterTickOnce(tickFun,data.duration*1000)
                end
            end          
        end
        RegisterTickOnce(tickFun,data.duration*1000)
    end
end
