local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CUIManager = import "L10.UI.CUIManager"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"

CLuaStarBiwuCreateZhanDuiWnd = class()
RegistClassMember(CLuaStarBiwuCreateZhanDuiWnd,"m_OKBtn")
RegistClassMember(CLuaStarBiwuCreateZhanDuiWnd,"m_NameInput")
RegistClassMember(CLuaStarBiwuCreateZhanDuiWnd,"m_SloganInput")

function CLuaStarBiwuCreateZhanDuiWnd:Awake()
    self.m_OKBtn = self.transform:Find("Anchor/OKButton").gameObject
    self.m_NameInput = self.transform:Find("Anchor/Name/NameInput"):GetComponent(typeof(UIInput))
    self.m_SloganInput = self.transform:Find("Anchor/Slogan/SloganInput"):GetComponent(typeof(UIInput))

    UIEventListener.Get(self.m_OKBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnOkBtnClicked(go) end)

    local sloganInputLabel = self.m_SloganInput.transform:Find("Label"):GetComponent(typeof(UILabel))
    sloganInputLabel.text = LocalString.GetString("请输入战队口号(最多12个字)")
end

function CLuaStarBiwuCreateZhanDuiWnd:OnEnable( )
    g_ScriptEvent:AddListener("CreateStarBiwuZhanDuiSuccess", self, "CreateStarBiwuZhanDuiSuccess")
end
function CLuaStarBiwuCreateZhanDuiWnd:OnDisable( )
    CLuaStarBiwuMgr.isQuDaoCreateZhanDuiWnd = false
    g_ScriptEvent:RemoveListener("CreateStarBiwuZhanDuiSuccess", self, "CreateStarBiwuZhanDuiSuccess")
end
function CLuaStarBiwuCreateZhanDuiWnd:CreateStarBiwuZhanDuiSuccess( )
    --CLuaStarBiwuMgr.isQuDaoSelfZhanDuiWnd = CLuaStarBiwuMgr.isQuDaoCreateZhanDuiWnd
    CLuaStarBiwuMgr:OpenStarBiWuZhanDuiWnd(1)
    CUIManager.CloseUI(CLuaUIResources.StarBiwuCreateZhanDuiWnd)
end
function CLuaStarBiwuCreateZhanDuiWnd:OnOkBtnClicked( go)
    if System.String.IsNullOrEmpty(self.m_NameInput.value) then
        g_MessageMgr:ShowMessage("QMPK_Please_Input_Zhandui_Name")
        return
    end
    local nameStr = string.gsub(self.m_NameInput.value, " ", "")
    if CommonDefs.StringLength(nameStr) == 0 then
        g_MessageMgr:ShowMessage("QMPK_Please_Input_Zhandui_Name")
        return
    end
    if CUICommonDef.GetStrByteLength(nameStr) > CQuanMinPKMgr.m_MaxNameLength then
        g_MessageMgr:ShowMessage("QMPK_Zhandui_Name_Too_Long")
        return
    end
    if CUICommonDef.GetStrByteLength(self.m_SloganInput.value) > CQuanMinPKMgr.m_MaxSloganLength then
        g_MessageMgr:ShowMessage("QMPK_Slogan_Too_Long")
        return
    end
    if not CWordFilterMgr.Inst:CheckName(nameStr) or not CWordFilterMgr.Inst:CheckName(self.m_SloganInput.value) then
        g_MessageMgr:ShowMessage("QMPK_Name_Slogan_Violation")
        return
    end
    if CLuaStarBiwuMgr.isQuDaoCreateZhanDuiWnd then
        Gac2Gas.RequestCreateStarBiwuZhanDui_QD(nameStr, self.m_SloganInput.value)
        return 
    end
    Gac2Gas.RequestCreateStarBiwuZhanDui(nameStr, self.m_SloganInput.value)
end
