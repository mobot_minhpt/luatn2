local CUITexture=import "L10.UI.CUITexture"
CLuaLingShouZhandouliWnd = class()
RegistClassMember(CLuaLingShouZhandouliWnd,"m_Items")
RegistClassMember(CLuaLingShouZhandouliWnd,"m_ZhandouliLabel")

function CLuaLingShouZhandouliWnd:Init()
    
    Gac2Gas.RequestLingShouScoreInfo()
    self.m_ZhandouliLabel = FindChild(self.transform,"PlayerCapacityLabel"):GetComponent(typeof(UILabel))
    self.m_ZhandouliLabel.text= nil

    self.m_Items = {}
    local function init(tf)
        local t= {
            button = tf:Find("Button").gameObject,
            icon = tf:Find("ItemCell/Icon"):GetComponent(typeof(CUITexture)),
            levelLabel = tf:Find("ItemCell/Label"):GetComponent(typeof(UILabel)),
            nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel)),
            zhandouliLabel = tf:Find("ZhandouliLabel"):GetComponent(typeof(UILabel)),
        }
        t.levelLabel.text = nil
        t.nameLabel.text = nil
        t.zhandouliLabel.text = "[acf8ff]"..LocalString.GetString("战力").. "[-] ".."0"
        return t
    end
    self.m_Items[1] = init(FindChild(self.transform,"Item1"))
    self.m_Items[2] = init(FindChild(self.transform,"Item2"))
end

function CLuaLingShouZhandouliWnd:OnEnable()
    g_ScriptEvent:AddListener("RequestLingShouScoreInfo_Result", self, "OnRequestLingShouScoreInfo_Result")
end

function CLuaLingShouZhandouliWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RequestLingShouScoreInfo_Result", self, "OnRequestLingShouScoreInfo_Result")
end

function CLuaLingShouZhandouliWnd:OnRequestLingShouScoreInfo_Result(lingshouId,score,lingshou1, bindLingShouId,bindScore,lingshou2)
    self.m_ZhandouliLabel.text= tostring(score+bindScore)
    local function init(t,lingshouId,score,data)
        if lingshouId and lingshouId~="" then
            t.zhandouliLabel.text ="[acf8ff]"..LocalString.GetString("战力").. "[-] "..tostring(score)
            local lingshouData = LingShou_LingShou.GetData(data.TemplateId)
            t.icon:LoadNPCPortrait(lingshouData.Portrait)
            t.levelLabel.text= tostring(data.Level)
            t.nameLabel.text = data.Name
        end
    end
    init(self.m_Items[1],lingshouId,score,lingshou1)
    UIEventListener.Get(self.m_Items[1].button).onClick = DelegateFactory.VoidDelegate(function(go)
        CLuaPlayerCapacityRecommendWnd.fromKey = "LingShou"
        CUIManager.ShowUI(CUIResources.PlayerCapacityRecommendWnd)
    end)
    init(self.m_Items[2],bindLingShouId,bindScore,lingshou2)
    UIEventListener.Get(self.m_Items[2].button).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("LingShouJieBan_Zhandouli_Advice")
    end)
end