local CBaseWnd = import "L10.UI.CBaseWnd"
local EventDelegate = import "EventDelegate"
local UIToggle=import "UIToggle"

CLuaZhuShaBiXiLianConditionWnd = class()
RegistClassMember(CLuaZhuShaBiXiLianConditionWnd, "m_Item")

function CLuaZhuShaBiXiLianConditionWnd:Init()

    self.m_Item = self.transform:Find("Anchor/Conditions/Item").gameObject
    self.m_Item:SetActive(false)

    -- CLuaZhuShaBiXiLianMgr.AddCountOption(7)
    -- CLuaZhuShaBiXiLianMgr.AddCountOption(8)


    local table = self.transform:Find("Anchor/Conditions/Table").gameObject
    Extensions.RemoveAllChildren(table.transform)

    local t = {6,7,8}
    for k,v in pairs(t) do
        local go = NGUITools.AddChild(table,self.m_Item)
        go:SetActive(true)
        self:InitCountItem(go,v)
    end

    for k,v in pairs(CLuaZhuShaBiXiLianMgr.m_WordConditions) do
        local go = NGUITools.AddChild(table,self.m_Item)
        go:SetActive(true)
        self:InitWordItem(go,k)
    end

    table:GetComponent(typeof(UITable)):Reposition()

    local confirmButton = self.transform:Find("Anchor/ConfirmBtn").gameObject
    UIEventListener.Get(confirmButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
    end)
end

function CLuaZhuShaBiXiLianConditionWnd:InitCountItem(go,count)
    local descLabel = go.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
    descLabel.text = SafeStringFormat3(LocalString.GetString("%d条或以上"),count)

    local toggle = go.transform:Find("Toggle"):GetComponent(typeof(UIToggle))
    
    toggle.value = CLuaZhuShaBiXiLianMgr.m_CountConditions[count] or false
    EventDelegate.Add(toggle.onChange,DelegateFactory.Callback(function () 
        if toggle.value then
            CLuaZhuShaBiXiLianMgr.m_CountConditions[count] = true
        else
            CLuaZhuShaBiXiLianMgr.m_CountConditions[count] = false
        end
    end))

end



function CLuaZhuShaBiXiLianConditionWnd:InitWordItem(go,word)
    local descLabel = go.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
    if Word_Word.Exists(word * 100 + 1) then
        descLabel.text = Word_Word.GetData(word * 100 + 1).WordDescription
    else
        descLabel.text= CLuaZhuShaBiXiLianMgr.GetWordClassDescription(word)
    end

    local toggle = go.transform:Find("Toggle"):GetComponent(typeof(UIToggle))
    
    toggle.value = CLuaZhuShaBiXiLianMgr.m_WordConditions[word] or false
    EventDelegate.Add(toggle.onChange,DelegateFactory.Callback(function () 
        if toggle.value then
            CLuaZhuShaBiXiLianMgr.m_WordConditions[word] = true
        else
            CLuaZhuShaBiXiLianMgr.m_WordConditions[word] = false
        end
    end))
end