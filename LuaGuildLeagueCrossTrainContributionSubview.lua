local DelegateFactory  = import "DelegateFactory"
local Extensions = import "Extensions"
local UITabBar = import "L10.UI.UITabBar"
local Animation = import "UnityEngine.Animation"

LuaGuildLeagueCrossTrainContributionSubview = class()

RegistClassMember(LuaGuildLeagueCrossTrainContributionSubview, "m_RankView")
RegistClassMember(LuaGuildLeagueCrossTrainContributionSubview, "m_AwardView")
RegistClassMember(LuaGuildLeagueCrossTrainContributionSubview, "m_TabBar")
RegistClassMember(LuaGuildLeagueCrossTrainContributionSubview, "m_Ani")
RegistClassMember(LuaGuildLeagueCrossTrainContributionSubview, "m_TimeLabel")
RegistClassMember(LuaGuildLeagueCrossTrainContributionSubview, "m_InfoButton")

function LuaGuildLeagueCrossTrainContributionSubview:Awake()
    self.m_RankView = self.transform:Find("GuildLeagueCrossTrainContributionRankView").gameObject
    self.m_AwardView = self.transform:Find("GuildLeagueCrossTrainContributionAwardView").gameObject
    self.m_TabBar = self.transform:Find("TabBar"):GetComponent(typeof(UITabBar))
    self.m_Ani = self.gameObject:GetComponent(typeof(Animation))
    self.m_TabBar.OnTabChange = CommonDefs.CombineListner_Action_GameObject_int(self.m_TabBar.OnTabChange, DelegateFactory.Action_GameObject_int(function(go, index)
        self:OnTabChange(go, index)
    end), true)

    self.m_TimeLabel = self.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    self.m_InfoButton = self.transform:Find("InfoButton").gameObject
    UIEventListener.Get(self.m_InfoButton).onClick = DelegateFactory.VoidDelegate(function() self:OnInfoButtonClick() end)
end

function LuaGuildLeagueCrossTrainContributionSubview:Init()
    self.m_TimeLabel.text = SafeStringFormat3(LocalString.GetString("距离下次奖励结算还有 [c][00ff00]%s"),LuaGuildLeagueCrossMgr:GetNextTrainRankAwardTimeStr())
    self.m_TabBar:ChangeTab(self.m_TabBar.SelectedIndex>=0 and self.m_TabBar.SelectedIndex or 0, false)
end

function LuaGuildLeagueCrossTrainContributionSubview:OnEnable()
    self:Init()
end

function LuaGuildLeagueCrossTrainContributionSubview:OnTabChange(go, index)
    if index == 0 then
        self.m_RankView:SetActive(true)
        self.m_AwardView:SetActive(false)
        self.m_Ani:Play("guildleaguecrosstraincontributionsubview_qiebangdan")
    else
        self.m_RankView:SetActive(false)
        self.m_AwardView:SetActive(true)
        self.m_Ani:Play("guildleaguecrosstraincontributionsubview_qiejiangli")
    end
end

function LuaGuildLeagueCrossTrainContributionSubview:OnInfoButtonClick()
    g_MessageMgr:ShowMessage("GLC_TRAIN_CONTRIBUTION_DESCRIPTION")
end


