local CItemChooseMgr = import "L10.UI.CItemChooseMgr"
local CItemChooseItem = import "L10.UI.CItemChooseItem"
local QnButton = import "L10.UI.QnButton"
local CItemMgr = import "L10.Game.CItemMgr"

LuaItemWithDetailMessageChooseWnd = class()
RegistChildComponent(LuaItemWithDetailMessageChooseWnd, "m_TableView","TableView", QnTableView)
RegistChildComponent(LuaItemWithDetailMessageChooseWnd, "m_TitleLabel","TitleLabel", UILabel)
RegistChildComponent(LuaItemWithDetailMessageChooseWnd, "m_Button","Button", QnButton)
RegistChildComponent(LuaItemWithDetailMessageChooseWnd, "m_ExplanationIcon","ExplanationIcon", GameObject)
RegistChildComponent(LuaItemWithDetailMessageChooseWnd, "m_NullItemDesLabel","NullItemDesLabel", UILabel)

RegistClassMember(LuaItemWithDetailMessageChooseWnd,"m_SelectItemId")

function LuaItemWithDetailMessageChooseWnd:Init()
    self.m_NullItemDesLabel.gameObject:SetActive(CItemChooseMgr.Inst.ItemIds.Count == 0)
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
            function() return CItemChooseMgr.Inst.ItemIds and CItemChooseMgr.Inst.ItemIds.Count or 0 end,
            function(item,row) self:InitItem(item,row) end)
    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnSelectAtRow(row)
    end)
    self.m_TableView:ReloadData(true, false)
    self.m_TableView:SetSelectRow(0, true)

    self.m_TitleLabel.text = CItemChooseMgr.Inst.Title
    self.m_Button.m_Label.text = CItemChooseMgr.Inst.ButtonText
    self.m_Button.OnClick = DelegateFactory.Action_QnButton(function(button)
        if LuaItemChooseWndMgr.onButtonClick then
            LuaItemChooseWndMgr.onButtonClick(self.m_SelectItemId)
            CUIManager.CloseUI(CLuaUIResources.ItemWithDetailMessageChooseWnd)
        end
    end)
    UIEventListener.Get(self.m_ExplanationIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        if LuaItemChooseWndMgr.onExplanationIconClick then
            LuaItemChooseWndMgr.onExplanationIconClick()
        end
    end)
end

function LuaItemWithDetailMessageChooseWnd:InitItem(item,row)
    local cmp = item.gameObject:GetComponent(typeof(CItemChooseItem))
    cmp:Init(CItemChooseMgr.Inst.ItemIds[row])
    local commonitem = CItemMgr.Inst:GetById(CItemChooseMgr.Inst.ItemIds[row])
    local label = item.gameObject.transform:Find("Label"):GetComponent(typeof(UILabel))
    label.text = commonitem.Amount
    label.gameObject:SetActive(commonitem.Amount > 1)
end

function LuaItemWithDetailMessageChooseWnd:OnSelectAtRow(index)
    if CItemChooseMgr.Inst.ItemIds == nil then return end
    if CItemChooseMgr.Inst.ItemIds.Count > index then
        self.m_SelectItemId = CItemChooseMgr.Inst.ItemIds[index]
    end
end

LuaItemChooseWndMgr = class()
LuaItemChooseWndMgr.onButtonClick = nil
LuaItemChooseWndMgr.onExplanationIconClick = nil
function LuaItemChooseWndMgr:ShowItemWithDetailMessageChooseWnd(itemIds, onclick, title, buttonText, onExplanationIconClick)
    CItemChooseMgr.Inst.ItemIds = itemIds
    LuaItemChooseWndMgr.onButtonClick = onclick
    CItemChooseMgr.Inst.Title = title
    CItemChooseMgr.Inst.ButtonText = buttonText
    LuaItemChooseWndMgr.onExplanationIconClick = onExplanationIconClick
    CUIManager.ShowUI(CLuaUIResources.ItemWithDetailMessageChooseWnd)
end