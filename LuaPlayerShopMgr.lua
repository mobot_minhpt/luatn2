local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CPlayerShopItemData = import "L10.UI.CPlayerShopItemData"

LuaPlayerShopMgr = {}

-- 禁止在玩家商店上架的物品检查
LuaPlayerShopMgr.ForbidItems = nil
function LuaPlayerShopMgr.IsForbidOnShelf(itemId)
    if not LuaPlayerShopMgr.ForbidItems then
        LuaPlayerShopMgr.ForbidItems = {}

        local forbidOnShelfItems = PlayerShop_Setting.GetData("ForbidOnShelfItems").Value
        if forbidOnShelfItems then
            for id in string.gmatch(forbidOnShelfItems, "(%d+);") do
                id = id and tonumber(id)
		        LuaPlayerShopMgr.ForbidItems[id] = true
	        end
        end
    end
    return LuaPlayerShopMgr.ForbidItems[itemId]
end

CPlayerShopMgr.m_isAllowToTrade_CS2LuaHook = function (mgr, item)
    if item and not item.IsBinded then
        if item.IsEquip and item.Equip.IntensifyLevel > 0 then
            return false
        end

        if item.IsEquip and item.PlayerShopPrice <= 0 and item.ValuablesFloorPrice <=0 then
            return false
        end

        if LuaPlayerShopMgr.IsForbidOnShelf(item.TemplateId) then
            return false
        end
        
        if item.IsPrecious then
            return item.ValuablesFloorPrice > 0
        end

        if item.PlayerShopPrice > 0 then
            return true
        end
    end
    return false
end

LuaPlayerShopMgr.m_BatchBuyLowestPrice = 0
LuaPlayerShopMgr.m_DisplaySplitItemIdSet = nil

function LuaPlayerShopMgr.IsDisplaySplitItemId(TemplateId)
    if not LuaPlayerShopMgr.m_DisplaySplitItemIdSet then
        LuaPlayerShopMgr.m_DisplaySplitItemIdSet = {}
        local splitItemIdArr = g_LuaUtil:StrSplit(PlayerShop_Setting.GetData("DisplaySplitItemId").Value,";") 
        for i = 1, #splitItemIdArr do
            if not cs_string.IsNullOrEmpty(splitItemIdArr[i]) then
                LuaPlayerShopMgr.m_DisplaySplitItemIdSet[tonumber(splitItemIdArr[i])] = true
            end
        end
    end
    return LuaPlayerShopMgr.m_DisplaySplitItemIdSet[TemplateId]
end

function LuaPlayerShopMgr.SplitSearchResultData(data)
    local list = {}
    local maxItemCount = tonumber(PlayerShop_Setting.GetData("DisplaySplitItemNumber").Value)
    for i = 0, data.Count - 1 do
        local item = data[i]
        local list2 = {}
        if item.Item and item.Item.TemplateId and LuaPlayerShopMgr.IsDisplaySplitItemId(item.Item.TemplateId) then
            local count = item.Count
            while count > maxItemCount do
                count = count - maxItemCount
                local newItem = item:Clone()
                newItem.Count = maxItemCount
                table.insert(list2, 1, newItem)
            end
            local newItem = item:Clone()
            newItem.Count = math.min(count, maxItemCount)
            table.insert(list2, 1, newItem)
        else
            table.insert(list2, 1, item)
        end
        for j = 1, #list2 do
            table.insert(list, list2[j])
        end
    end
    return Table2List(list, MakeGenericClass(List, CPlayerShopItemData))
end