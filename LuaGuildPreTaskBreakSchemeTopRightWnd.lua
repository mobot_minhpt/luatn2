local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

LuaGuildPreTaskBreakSchemeTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuildPreTaskBreakSchemeTopRightWnd, "Stone1", "Stone1", GameObject)
RegistChildComponent(LuaGuildPreTaskBreakSchemeTopRightWnd, "Stone2", "Stone2", GameObject)
RegistChildComponent(LuaGuildPreTaskBreakSchemeTopRightWnd, "Stone3", "Stone3", GameObject)
RegistChildComponent(LuaGuildPreTaskBreakSchemeTopRightWnd, "Stone4", "Stone4", GameObject)
RegistChildComponent(LuaGuildPreTaskBreakSchemeTopRightWnd, "ExpandButton", "ExpandButton", GameObject)

--@endregion RegistChildComponent end

function LuaGuildPreTaskBreakSchemeTopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    UIEventListener.Get(self.ExpandButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnExpandButtonClick()
    end)
end

function LuaGuildPreTaskBreakSchemeTopRightWnd:OnEnable()
	g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("GuildCustomBuildPreTaskSyncPlayInfo", self, "OnGuildCustomBuildPreTaskSyncPlayInfo")--data
end

function LuaGuildPreTaskBreakSchemeTopRightWnd:OnDisable()
	g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("GuildCustomBuildPreTaskSyncPlayInfo", self, "OnGuildCustomBuildPreTaskSyncPlayInfo")--data
end

function LuaGuildPreTaskBreakSchemeTopRightWnd:Init()
    self:InitStone(self.Stone1.transform,false,100,-1)
    self:InitStone(self.Stone2.transform,false,100,-1)
    self:InitStone(self.Stone3.transform,false,100,-1)
    self:InitStone(self.Stone4.transform,false,100,-1)
end

function LuaGuildPreTaskBreakSchemeTopRightWnd:InitStone(stone,isHighlight,hp,lastTime)
    local highLight = stone:Find("Highlight").gameObject
    if highLight.activeSelf ~= isHighlight then
        highLight:SetActive(isHighlight)
    end

    local hpLabel = stone:Find("ProgressLabel"):GetComponent(typeof(UILabel))
    if lastTime > 0 and hp <=0 then
        --显示倒记时
        hpLabel.text = SafeStringFormat3("%ds",lastTime)
    else
        --显示剩余血量
        hpLabel.text = SafeStringFormat3("%d%%",hp)
    end
    
end

function LuaGuildPreTaskBreakSchemeTopRightWnd:OnHideTopAndRightTipWnd()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

function LuaGuildPreTaskBreakSchemeTopRightWnd:OnExpandButtonClick()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaGuildPreTaskBreakSchemeTopRightWnd:OnGuildCustomBuildPreTaskSyncPlayInfo()
    local playInfo = LuaGuildCustomBuildMgr.PreTaskPlayInfo
    if not playInfo or not next(playInfo) then
        return
    end
    if playInfo.Stage ~= 1 then
        return
    end
    local taskInfo = playInfo.Task1Info
    if not taskInfo then
        return
    end
    --playInfo {Poison,MonsterHp,TimeOut}
    local _,poison = CommonDefs.DictTryGet_LuaCall(taskInfo, "Poison")
    local _,monsterHps = CommonDefs.DictTryGet_LuaCall(taskInfo, "MonsterHp")
    local _,timeOut = CommonDefs.DictTryGet_LuaCall(taskInfo, "TimeOut")
    local _,finished = CommonDefs.DictTryGet_LuaCall(taskInfo, "Finished")

    local idx = 0
    for i=0,3,1 do
        local hp = monsterHps[i]
        idx = idx + 1
        local stone = self["Stone"..idx]
        local lastTime = -1
        local isHighlight = false
        if hp <= 0 then
            lastTime = timeOut
            isHighlight = true
        end
        
        self:InitStone(stone.transform,isHighlight,hp,lastTime)
    end

    local setting = GuildCustomBuildPreTask_GameSetting.GetData()
    local lv = 0
    if poison and tonumber(poison)>0 then
        for i = 0,setting.PoisonLevels.Length-1,1 do
            if poison >=  setting.PoisonLevels[i] then
                lv = i+1
            end
        end
    end
    --
    g_ScriptEvent:BroadcastInLua("UpdatePreTaskPoisonProgress",lv)
    
end
--@region UIEvent

--@endregion UIEvent

