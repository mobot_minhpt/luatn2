local UIGrid = import "UIGrid"
local LocalString = import "LocalString"
local EnumWarehouseItemCellLockStatus = import "L10.UI.EnumWarehouseItemCellLockStatus"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CWarehousePackageItemCell = import "L10.UI.CWarehousePackageItemCell"
local CommonDefs = import "L10.Game.CommonDefs"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local DefaultItemActionDataSource = import "L10.UI.DefaultItemActionDataSource"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"


CLuaWarehousePackageView = class()
RegistChildComponent(CLuaWarehousePackageView, "ItemCell", GameObject)
RegistChildComponent(CLuaWarehousePackageView, "MaskBG", GameObject)
RegistClassMember(CLuaWarehousePackageView,"scrollView")
RegistClassMember(CLuaWarehousePackageView,"grid")
RegistClassMember(CLuaWarehousePackageView,"arrangeBtn")
RegistClassMember(CLuaWarehousePackageView,"itemCells")
RegistClassMember(CLuaWarehousePackageView,"selectedItemId")
RegistClassMember(CLuaWarehousePackageView,"OnPackageItemClick")
RegistClassMember(CLuaWarehousePackageView,"useAsyncInitPackage")
RegistClassMember(CLuaWarehousePackageView,"initPackageCoroutine")
RegistClassMember(CLuaWarehousePackageView,"originLocalPosList")
RegistClassMember(CLuaWarehousePackageView, "m_ActionDataSource")
RegistClassMember(CLuaWarehousePackageView, "m_GridView")
RegistClassMember(CLuaWarehousePackageView, "m_ItemPosDict")
RegistClassMember(CLuaWarehousePackageView, "m_MaskUnBindedItems")
RegistClassMember(CLuaWarehousePackageView, "m_Inited")

RegistClassMember(CLuaWarehousePackageView,"fastOverlapBtn")

RegistClassMember(CLuaWarehousePackageView, "m_OnItemClickAction")
RegistClassMember(CLuaWarehousePackageView, "m_OnItemDoubleClickAction")
RegistClassMember(CLuaWarehousePackageView,"OnPackageLockedItemClick")

CLuaWarehousePackageView.s_EnableDoubleClick=true

function CLuaWarehousePackageView:Awake()
    self.scrollView = self.transform:Find("Offset/ScrollView"):GetComponent(typeof(UIScrollView))
    self.grid = self.transform:Find("Offset/ScrollView/Grid"):GetComponent(typeof(UIGrid))
    self.arrangeBtn = self.transform:Find("Offset/ArrangeBtn").gameObject
    self.fastOverlapBtn = self.transform:Find("Offset/FastOverlapBtn").gameObject
    self.m_GridView = self.transform:Find("Offset/ScrollView"):GetComponent(typeof(QnAdvanceGridView))
    self.itemCells = CreateFromClass(MakeGenericClass(List, CWarehousePackageItemCell))
    self.selectedItemId = nil
    self.OnPackageItemClick = nil
    self.OnPackageLockedItemClick = nil
    self.m_ItemPosDict = {}
    self.m_MaskUnBindedItems = false
    self.m_Inited = false
    self.m_ActionDataSource = DefaultItemActionDataSource.Create(
        1,
        {
            function() 
                self:OnPutIn()
            end
        },
        {
            LocalString.GetString("放入")
        }
    )

    UIEventListener.Get(self.arrangeBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnArrangeButtonClick(go)
    end)
    UIEventListener.Get(self.fastOverlapBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnFastOverlapBtnClick(go)
    end)
    self.ItemCell:SetActive(false)

    self.m_GridView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            if CClientMainPlayer.Inst then
                return CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag) 
            else
                return 0
            end
        end,
        function(item,index) self:InitItem(item,index) end
    )

    if(not self.m_OnItemClickAction)then
        self.m_OnItemClickAction = DelegateFactory.Action_CWarehousePackageItemCell(
            function(item_cell)
                self:OnItemClick(item_cell)
            end
        )
    end
    if(not self.m_OnItemDoubleClickAction)then
        self.m_OnItemDoubleClickAction = DelegateFactory.Action_CWarehousePackageItemCell(
            function(item_cell)
                self:OnItemDoubleClick(item_cell)
            end
        )
    end
    self:OnRefreshPackageView(false)
end

function CLuaWarehousePackageView:ShowPackage()
    self:InitPackage(false)
end

function CLuaWarehousePackageView:InitItem(item,index)
    local cell = CommonDefs.GetComponent_GameObject_Type(item.gameObject, typeof(CWarehousePackageItemCell))
    local itemProp = CClientMainPlayer.Inst.ItemProp
    local itemId = itemProp:GetItemAt(EnumItemPlace.Bag, index + 1)
    if(itemId)then
        self.m_ItemPosDict[itemId] = index
    end
    local isLocked = (index + 1 > itemProp.BagPlaceSize and index + 1 <= itemProp:GetPlaceMaxSize(EnumItemPlace.Bag))
    cell:Init(itemId, isLocked and EnumWarehouseItemCellLockStatus.Locked or EnumWarehouseItemCellLockStatus.Unlocked)
    self:SetCellMask(cell)
end

function CLuaWarehousePackageView:Init()
    self:InitPackage(true)
end

-- Auto Generated!!
function CLuaWarehousePackageView:InitPackage(_force)
    if CClientMainPlayer.Inst == nil then
        return
    end
    if(self.m_Inited and (not _force))then
        local visible_items = self.m_GridView:GetVisibleItems()
        do
            local i = 0
            while(i < visible_items.Count)do
                self:InitItem(visible_items[i],visible_items[i].Row)
                i = i + 1
            end
        end
    else
        self.m_Inited = true
        self.m_GridView:ReloadData(true, true)
    end
end

function CLuaWarehousePackageView:SetCellMask(cell)
    local mask = cell.transform:Find("Mask").gameObject
    if(self.m_MaskUnBindedItems)then
        local item = CItemMgr.Inst:GetById(cell.ItemId)
        --TODO 读表判断该物品是否可以放入绑仓
        local bindrepo_not_allow = true
        if(item)then
            bindrepo_not_allow = (BindRepo_AllowedItems.GetData(item.TemplateId) == nil)
        end
        
        local item_should_mask = ((item and ((not item.IsBinded) or (CLuaBindRepoMgr.GetItemExpiredTime(item) > 0) or bindrepo_not_allow)) or (cell.LockStatus == EnumWarehouseItemCellLockStatus.Locked))
        if(item_should_mask)then
            mask:SetActive(true)
            cell.OnItemClickDelegate = nil
            cell.OnItemDoubleClickDelegate = nil
            return
        end
    end
    mask:SetActive(false)
    cell.OnItemClickDelegate = self.m_OnItemClickAction
    cell.OnItemDoubleClickDelegate = self.m_OnItemDoubleClickAction
end

function CLuaWarehousePackageView:OnRefreshPackageView(mask_unbinded)
    self.m_MaskUnBindedItems = mask_unbinded
    if(self.m_GridView)then
        local visible_items = self.m_GridView:GetVisibleItems()
        do
            local i = 0
            while(i < visible_items.Count)do
                local cell = visible_items[i].gameObject:GetComponent(typeof(CWarehousePackageItemCell))
                self:SetCellMask(cell)
                i = i + 1
            end
        end
    end
    self.MaskBG:SetActive(self.m_MaskUnBindedItems)
    self.fastOverlapBtn:SetActive(not mask_unbinded)
end

function CLuaWarehousePackageView:OnItemClick( itemCell) 
    local item = CItemMgr.Inst:GetById(itemCell.ItemId)
    if item ~= nil then
        self.selectedItemId = item.Id
        CItemInfoMgr.ShowLinkItemInfo(itemCell.ItemId, false, self.m_ActionDataSource, CItemInfoMgr.AlignType.ScreenLeft, 0, 0, 0, 0)
    else
        if self.OnPackageLockedItemClick ~= nil and itemCell.LockStatus ==EnumWarehouseItemCellLockStatus.Locked then
            self.OnPackageLockedItemClick()
        end
    end
end
function CLuaWarehousePackageView:OnItemDoubleClick( itemCell) 
    if not CLuaWarehousePackageView.s_EnableDoubleClick then
        return
    end
    local item = CItemMgr.Inst:GetById(itemCell.ItemId)
    if item ~= nil and self.selectedItemId == itemCell.ItemId then
        self:OnPutIn()
    else
        if self.OnPackageLockedItemClick ~= nil and itemCell.LockStatus ==EnumWarehouseItemCellLockStatus.Locked then
            self.OnPackageLockedItemClick()
        end
    end
end

function CLuaWarehousePackageView:OnEnable()
    self:ShowPackage()
    self:RegisteEvents()
end

function CLuaWarehousePackageView:OnDisable()
    self:UnRegisterEvents()
end

function CLuaWarehousePackageView:OnDestroy()
   -- print("WAREHOUSE PACKAGE VIEW DESTROY")
    self.OnPackageItemClick = nil
    self.OnPackageLockedItemClick = nil
end

function CLuaWarehousePackageView:RegisteEvents()
    g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:AddListener("SendItem", self, "OnUpdateItemCount")
end

function CLuaWarehousePackageView:UnRegisterEvents()
    g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:RemoveListener("SendItem", self, "OnUpdateItemCount")
end

function CLuaWarehousePackageView:OnArrangeButtonClick(go)
    Gac2Gas.PackageOrder(EnumItemPlace.Bag)
end

function CLuaWarehousePackageView:OnFastOverlapBtnClick(go)
    g_ScriptEvent:BroadcastInLua("RefreshRpcFromFastOverlap")
end

function CLuaWarehousePackageView:OnSetItemAt(args) 

    local place, position, oldItemId, newItemId = args[0],args[1],args[2],args[3]
    if place ~= EnumItemPlace.Bag or CClientMainPlayer.Inst == nil or position > CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag) or position == 0 then
        return
    end
    local itemProp = CClientMainPlayer.Inst.ItemProp
    --更新包裹页信息
    local itemId = itemProp:GetItemAt(place, position)
    if(oldItemId)then
        self.m_ItemPosDict[oldItemId] = nil
    end
    if(newItemId)then
        self.m_ItemPosDict[newItemId] = position - 1
    end

    local isLocked = (position > CClientMainPlayer.Inst.ItemProp.BagPlaceSize and position <= CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag))
    --local cell = self.m_GridView:GetItemAtRow(position - 1).gameObject:GetComponent(typeof(CWarehousePackageItemCell))
    local item = self.m_GridView:GetItemAtRow(position - 1)
    if(item)then
        local cell = item.gameObject:GetComponent(typeof(CWarehousePackageItemCell))
        cell:Init(newItemId, isLocked and EnumWarehouseItemCellLockStatus.Locked or EnumWarehouseItemCellLockStatus.Unlocked)
        self:SetCellMask(cell)
    end
    --self.itemCells[position - 1]:Init(itemId, isLocked and EnumWarehouseItemCellLockStatus.Locked or EnumWarehouseItemCellLockStatus.Unlocked)
end
function CLuaWarehousePackageView:OnUpdateItemCount( args) 
    local itemId = args[0]
    if System.String.IsNullOrEmpty(itemId) then
        return
    end
    if(self.m_ItemPosDict[itemId] ~= nil)then
        local cell = self.m_GridView:GetItemAtRow(self.m_ItemPosDict[itemId])
        if(cell)then
            cell.gameObject:GetComponent(typeof(CWarehousePackageItemCell)):UpdateAmount()
        end
    end
    
    --local cell = self.itemCells[self.m_ItemPosDict[itemId]]


    -- local n = self.itemCells.Count
    -- do
    --     local i = 0
    --     while i < n do
    --         if self.itemCells[i].ItemId == itemId then
    --             self.itemCells[i]:UpdateAmount()
    --             break
    --         end
    --         i = i + 1
    --     end
    -- end
end

function CLuaWarehousePackageView:OnPutIn( )
    local item = CItemMgr.Inst:GetById(self.selectedItemId)
    if item ~= nil and CClientMainPlayer.Inst ~= nil then
        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(EnumItemPlace.Bag, item.Id)
        if self.OnPackageItemClick ~= nil then
            self.OnPackageItemClick(pos, item.Id)
        end
        CItemInfoMgr.CloseItemInfoWnd()
    end
end

