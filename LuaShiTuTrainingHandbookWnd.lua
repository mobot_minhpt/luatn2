local CButton = import "L10.UI.CButton"
local UIScrollView = import "UIScrollView"
local CIMMgr = import "L10.Game.CIMMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local Constants = import "L10.Game.Constants"
local NGUIText = import "NGUIText"

LuaShiTuTrainingHandbookWnd = class()

RegistChildComponent(LuaShiTuTrainingHandbookWnd, "m_RadioBox","RadioBox", QnRadioBox)
RegistChildComponent(LuaShiTuTrainingHandbookWnd, "m_TeacherRoot","TeacherRoot", GameObject)
RegistChildComponent(LuaShiTuTrainingHandbookWnd, "m_StudentRoot","StudentRoot", GameObject)
RegistChildComponent(LuaShiTuTrainingHandbookWnd, "m_TimeLabel","TimeLabel", UILabel)
RegistChildComponent(LuaShiTuTrainingHandbookWnd, "m_ExplanationIcon","ExplanationIcon", CButton)
RegistChildComponent(LuaShiTuTrainingHandbookWnd, "m_TrainingPlanView","TrainingPlanView", GameObject)
RegistChildComponent(LuaShiTuTrainingHandbookWnd, "m_WeeklyHomeworkView","WeeklyHomeworkView", GameObject)
RegistChildComponent(LuaShiTuTrainingHandbookWnd, "m_ZunShiZhongDaoView","ZunShiZhongDaoView", GameObject)
RegistChildComponent(LuaShiTuTrainingHandbookWnd, "m_ScorllView","ScorllView", UIScrollView)

RegistClassMember(LuaShiTuTrainingHandbookWnd,"m_TabWindows")
RegistClassMember(LuaShiTuTrainingHandbookWnd,"m_AlertState")
RegistClassMember(LuaShiTuTrainingHandbookWnd,"m_ZunShiZhongDaoIndex")

function LuaShiTuTrainingHandbookWnd:Init()
    self.m_TabWindows = { self.m_TrainingPlanView , self.m_WeeklyHomeworkView, self.m_ZunShiZhongDaoView }
    self.m_AlertState = {false, false, false}
    self.m_ZunShiZhongDaoIndex = 2
    self:HideRuShiDiZiTrainingPlanView()

    self.m_RadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnTabButtonSelected(btn, index)
    end)
    for i = 0, self.m_RadioBox.m_RadioButtons.Length - 1 do
        self.m_RadioBox.m_RadioButtons[i].transform:Find("AlertSprite").gameObject:SetActive(false)
    end

    UIEventListener.Get(self.m_ExplanationIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:ShowExplanation()
    end)

    if not CClientMainPlayer.Inst then return end
    local shituinfo = LuaShiTuTrainingHandbookMgr:GetShiTuInfo()
    if shituinfo then
        local time = CServerTimeMgr.ConvertTimeStampToZone8Time(shituinfo.Time)
        if CommonDefs.IS_VN_CLIENT then
            self.m_TimeLabel.text = SafeStringFormat3("%02d/%02d/%d",time.Day,time.Month,time.Year)
        else
            self.m_TimeLabel.text = SafeStringFormat3(LocalString.GetString("%d年%02d月%02d日"),time.Year, time.Month, time.Day)
        end
    end
    self:InitShiTuInfo(LuaShiTuTrainingHandbookMgr:IsShowForTeacher() and self.m_TeacherRoot or self.m_StudentRoot, CClientMainPlayer.Inst.BasicProp, true)
    self:InitShiTuInfo(LuaShiTuTrainingHandbookMgr:IsShowForTeacher() and self.m_StudentRoot or self.m_TeacherRoot, CIMMgr.Inst:GetBasicInfo(LuaShiTuTrainingHandbookMgr:GetPartnerPlayerId()))
    self.m_RadioBox:ChangeTo(0, true)
end

function LuaShiTuTrainingHandbookWnd:OnEnable()
    g_ScriptEvent:AddListener("ShiTuTrainingHandbook_Alert", self, "OnAlert")
end

function LuaShiTuTrainingHandbookWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ShiTuTrainingHandbook_Alert", self, "OnAlert")
    LuaShiTuTrainingHandbookMgr:OnWndClose()
end

function LuaShiTuTrainingHandbookWnd:HideRuShiDiZiTrainingPlanView()
    if LuaShiTuTrainingHandbookMgr:IsTudiRuShi() then
        local arr = self.m_RadioBox.m_RadioButtons
        arr[2].gameObject:SetActive(false)
        for i = 0,1 do
            arr[i].m_Label.text = arr[i + 1].m_Label.text
        end
        self.m_RadioBox.m_RadioButtons = Table2Array({self.m_RadioBox.m_RadioButtons[0],self.m_RadioBox.m_RadioButtons[1]}, MakeArrayClass(QnSelectableButton))
        self.m_TabWindows = { self.m_WeeklyHomeworkView, self.m_ZunShiZhongDaoView }
        self.m_ZunShiZhongDaoIndex = 1
    end
end

function LuaShiTuTrainingHandbookWnd:OnUpdateShiTuPartnerInfo(baishitime)
    self.m_TimeLabel.text = baishitime
end

function LuaShiTuTrainingHandbookWnd:OnTabButtonSelected(btn, index)
    if (not self.m_TabWindows) or (index >= #self.m_TabWindows ) then return end
    for _index ,go in pairs(self.m_TabWindows) do
        go:SetActive(_index == (index + 1))
    end
    self.m_ScorllView:ResetPosition()
    if self.m_AlertState[index + 1] then
        if index == self.m_ZunShiZhongDaoIndex then
            self.m_AlertState[index + 1] = false
            self.m_RadioBox.m_RadioButtons[index].transform:Find("AlertSprite").gameObject:SetActive(false)
        end
    end
end

function LuaShiTuTrainingHandbookWnd:OnAlert(type, isAlert)
    local index = type - 1
    if LuaShiTuTrainingHandbookMgr:IsTudiRuShi() then
        index = index - 1
    end
    if (index < 0) or (index >= self.m_RadioBox.m_RadioButtons.Length) then return end
    self.m_RadioBox.m_RadioButtons[index].transform:Find("AlertSprite").gameObject:SetActive(isAlert)
    self.m_AlertState[type] = isAlert
end

function LuaShiTuTrainingHandbookWnd:ShowExplanation()
    g_MessageMgr:ShowMessage("Message_ShiTuTrainingHandbookWnd_Readme")
end

function LuaShiTuTrainingHandbookWnd:InitShiTuInfo(root, basicInfo, isSelf)
    if (not basicInfo) or (not CClientMainPlayer.Inst) then return end
    local info = {}
    info.portrait = root.transform:Find("Border/Portrait"):GetComponent(typeof(CUITexture))
    info.nameLabel = root.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    info.levelLabel = root.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    info.nameLabel.text = basicInfo.Name
    info.nameLabel.color = isSelf and Color(0,1,0,1) or Color(1,1,1,1)
    local hasFeiSheng = CClientMainPlayer.Inst.HasFeiSheng
    if not isSelf then
        hasFeiSheng = (basicInfo.XianFanStatus > 0)
    end
    local default
    if hasFeiSheng then
        default = Constants.ColorOfFeiSheng
    else
        default = NGUIText.EncodeColor24(info.levelLabel.color)
    end
    info.levelLabel.text = cs_string.Format("[c][{0}]Lv. {1}[-][/c]", default, basicInfo.Level)
    local portraitName = CUICommonDef.GetPortraitName(basicInfo.Class, basicInfo.Gender, basicInfo.Expression)
    info.portrait:LoadNPCPortrait(portraitName, false)
    return info
end

