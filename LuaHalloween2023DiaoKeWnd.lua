local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CHuanLingLineDrawer = import "L10.UI.CHuanLingLineDrawer"
local CUITexture = import "L10.UI.CUITexture"
local CHandDrawMgr = import "L10.Game.CHandDrawMgr"
local CDrawLineMgr = import "L10.Game.CDrawLineMgr"
local Color = import "UnityEngine.Color"
local UIRoot = import "UIRoot"
local Texture2D = import "UnityEngine.Texture2D"
local TextureFormat = import "UnityEngine.TextureFormat"
local Animation = import "UnityEngine.Animation"
local CMuKeDraw = import "L10.UI.CMuKeDraw"

LuaHalloween2023DiaoKeWnd = class()

RegistChildComponent(LuaHalloween2023DiaoKeWnd, "LineDrawer", CHuanLingLineDrawer)
RegistChildComponent(LuaHalloween2023DiaoKeWnd, "FinishButton", GameObject)
RegistChildComponent(LuaHalloween2023DiaoKeWnd, "SimplePicture", CUITexture)
RegistChildComponent(LuaHalloween2023DiaoKeWnd, "SecondPicture", CUITexture)
RegistChildComponent(LuaHalloween2023DiaoKeWnd, "FinalPicture", CUITexture)
RegistChildComponent(LuaHalloween2023DiaoKeWnd, "HuaZhi", CMuKeDraw)
RegistChildComponent(LuaHalloween2023DiaoKeWnd, "Label", UILabel)

RegistClassMember(LuaHalloween2023DiaoKeWnd, "m_Animation")
RegistClassMember(LuaHalloween2023DiaoKeWnd, "CharacterIndex") -- 1-显  2-隐
RegistClassMember(LuaHalloween2023DiaoKeWnd, "IsWinSocialWndOpened")
RegistClassMember(LuaHalloween2023DiaoKeWnd, "m_Stage")
RegistClassMember(LuaHalloween2023DiaoKeWnd, "m_AnimationTick")

function LuaHalloween2023DiaoKeWnd:Awake()
    self.m_Animation = self.transform:GetComponent(typeof(Animation))
    CommonDefs.AddOnClickListener(self.FinishButton, DelegateFactory.Action_GameObject(function (go)
        self:OnFinishButtonClicked(go)
    end), false)
end

function LuaHalloween2023DiaoKeWnd:Init()
    if CommonDefs.IsPCGameMode() then
        local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
        self.IsWinSocialWndOpened = CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened
    end
    self:InitStage0()
end

function LuaHalloween2023DiaoKeWnd:InitStage0()
    self.m_Stage = 0
    self.SimplePicture.gameObject:SetActive(true)
    self.SecondPicture.gameObject:SetActive(false)
    self.FinalPicture.gameObject:SetActive(false)

    CHandDrawMgr.Inst.NeedDraw = true
    CDrawLineMgr.Inst.LineColor = NGUIText.ParseColor24("fff335", 0)
    CDrawLineMgr.Inst.LineStartWidth = 0.035
    CDrawLineMgr.Inst.LineEndWidth = 0.035
    CDrawLineMgr.Inst.PenMaterialPath = "UI/Texture/FestivalActivity/Festival_Halloween/Halloween2023/Material/halloween2023diaokewnd_graver.mat"
    self.LineDrawer:Init()
    CDrawLineMgr.Inst:AllowDraw()
end

function LuaHalloween2023DiaoKeWnd:OnFinishButtonClicked(go)
    CDrawLineMgr.Inst:ForbidDraw()

    if self:IsDrawPassed() then
        self.LineDrawer:ClearAllLines()
        self.m_Animation:Play("halloween2023diaokewnd_second")
        Gac2Gas.FinishHandDraw(CHandDrawMgr.Inst.HandDrawTaskID, CHandDrawMgr.Inst.HandDrawID)
        self.FinishButton.gameObject:SetActive(false)
        self.Label.gameObject:SetActive(false)
    else
        g_MessageMgr:ShowMessage("HAND_DRAW_NOT_MATCH")
        self.LineDrawer:ClearAllLines()
        CDrawLineMgr.Inst:AllowDraw()
    end
end

function LuaHalloween2023DiaoKeWnd:GetCutOut(textureDrawn)
    local centerX = textureDrawn.width / 2
    local centerY = textureDrawn.height / 2

    local width_temp = self.SimplePicture.texture.width
    local height_temp = self.SimplePicture.texture.height
    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local width = math.floor(width_temp / scale)
    local height = math.floor(height_temp / scale)

    if CommonDefs.IsPCGameMode() then
        local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
        if CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
            width = self:AdjustForWinWidth(width)
            height = self:AdjustForWinHeight(height)
        end
    end

    local startX = math.floor(centerX - width / 2)
    local startY = math.floor(centerY - height / 2)

    local tmp = textureDrawn:GetPixels(startX, startY, width, height)
    self.AfterTexture = Texture2D(width, height, TextureFormat.ARGB32, false)
    self.AfterTexture:SetPixels(tmp)
    self.AfterTexture:Apply()
    self.AfterTexture.name = "after"

    return self.AfterTexture
end

function LuaHalloween2023DiaoKeWnd:AdjustForWinWidth(width)
    return width
end

function LuaHalloween2023DiaoKeWnd:AdjustForWinHeight(height)
    return height
end

function LuaHalloween2023DiaoKeWnd:IsDrawPassed()
    local drawTexture = self.LineDrawer:GetLineTexture()
    local cutOutTexture = self:GetCutOut(drawTexture)
    local checkTexture = TypeAs(self.SimplePicture.material:GetTexture("_AlphaTex"), typeof(Texture2D))
    return CHandDrawMgr.Inst:IsDrawPassed(checkTexture, cutOutTexture, 0.5, 0.45, 0.7)
end

function LuaHalloween2023DiaoKeWnd:OnDisable()
    if self.m_AnimationTick then UnRegisterTick(self.m_AnimationTick) end
end