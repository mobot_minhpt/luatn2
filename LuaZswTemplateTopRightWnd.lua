local QnTableView=import "L10.UI.QnTableView"
local UISprite = import "UISprite"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

local Transform = import "UnityEngine.Transform"
local NGUIMath = import "NGUIMath"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CUITexture = import "L10.UI.CUITexture"
local CShopMallMgr = import "L10.UI.CShopMallMgr"

CLuaZswTemplateTopRightWnd = class()

RegistChildComponent(CLuaZswTemplateTopRightWnd, "m_TableView","TableView", QnTableView)
RegistChildComponent(CLuaZswTemplateTopRightWnd, "m_BackGround","Background", UISprite)
RegistChildComponent(CLuaZswTemplateTopRightWnd, "m_GetMoreBtn","GetMoreBtn", GameObject)
RegistChildComponent(CLuaZswTemplateTopRightWnd, "m_Table","Table", Transform)

RegistClassMember(CLuaZswTemplateTopRightWnd,"m_TemplateItemList")
RegistClassMember(CLuaZswTemplateTopRightWnd,"m_DefaultHeight")
RegistClassMember(CLuaZswTemplateTopRightWnd,"m_YardDecorationPrivalege")

function CLuaZswTemplateTopRightWnd:Awake()
    self.m_YardDecorationPrivalege = false
    self.m_TemplateItemList = {}

    local houseTemplate = CClientHouseMgr.Inst.mExtraProp.HouseTemplate

    Zhuangshiwu_ZswTemplate.Foreach(function (furnitureId, data)
        local itemId = Zhuangshiwu_Zhuangshiwu.GetData(furnitureId).ItemId
        local data = HouseCompetition_HouseTemplate.GetData(itemId)
        if data then
            local sn = data.SN
            local isBind = houseTemplate:GetBit(sn)
            if isBind then
                table.insert(self.m_TemplateItemList,furnitureId)
            end
        end
    end)
    UIEventListener.Get(self.m_GetMoreBtn).onClick  = DelegateFactory.VoidDelegate(function(go)
        self:OnGetMoreBtnClick()
    end)
end

function CLuaZswTemplateTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryRoomerPrivalegeInfoResult", self, "OnQueryRoomerPrivalegeInfoResult")
end

function CLuaZswTemplateTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryRoomerPrivalegeInfoResult", self, "OnQueryRoomerPrivalegeInfoResult")
end

function CLuaZswTemplateTopRightWnd:OnQueryRoomerPrivalegeInfoResult(args)
    local opInfo = args[5]
    local roomerId = args[1]
    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id
    if myId==roomerId then
        self.m_YardDecorationPrivalege = false
        if opInfo then
            do
                local i = 0
                while i < opInfo.Count do
                    local idx = math.floor(tonumber(opInfo[i] or 0))
                    local v = math.floor(tonumber(opInfo[i + 1] or 0))
                    if idx == EHouseEditableOperation_lua.OpYardFurniture and v ~= 0 then
                        self.m_YardDecorationPrivalege = true
                        break
                    end
                    i = i + 2
                end
            end 
        end
    end
    self.m_TableView:ReloadData(false, false)
end

function CLuaZswTemplateTopRightWnd:Init()
    if CClientHouseMgr.Inst:IsCurHouseRoomer() then
        self.m_YardDecorationPrivalege = false
        if CClientMainPlayer.Inst then
            Gac2Gas.QueryRoomerPrivalegeInfo(CClientMainPlayer.Inst.Id)
        end
    else
        self.m_YardDecorationPrivalege = true
    end
    
    CLuaZswTemplateMgr.LoadTemplateRenameData()
    self.m_DefaultHeight = self.m_BackGround.height
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.m_TemplateItemList
        end,
        function(item,row)
            self:InitItem(item,row+1)
        end)

    self.m_TableView:ReloadData(false, false)

    local count = #self.m_TemplateItemList
    local tableHeight = NGUIMath.CalculateRelativeWidgetBounds(self.m_Table).size.y
    local templateHeight = tableHeight / count

    if count == 0 then--1
        self.m_BackGround.height = self.m_DefaultHeight - 176
    elseif count <= 3 then--小于三个的时候自适应高度显示
        self.m_BackGround.height = self.m_DefaultHeight + tableHeight - templateHeight
    
    else--大于三个的时候固定高度 滑动显示
        self.m_BackGround.height = self.m_DefaultHeight +  2.5*templateHeight
    end
end

function CLuaZswTemplateTopRightWnd:InitItem(item, row)
    local name = item.transform:Find("LabelName"):GetComponent(typeof(UILabel))
    local editBtn = item.transform:Find("EditBtn").gameObject
    local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))

    local id = self.m_TemplateItemList[row]
    local zswData = Zhuangshiwu_Zhuangshiwu.GetData(id)
    local rename = CLuaZswTemplateMgr.HouseZswTemplateRenameData[tostring(id)]
    if rename then
        name.text = rename
    else
        if zswData then
            name.text = zswData.Name
        end
    end

    if zswData then
        local itemId = zswData.ItemId
        local itemData = Item_Item.GetData(itemId)
        if itemData then
            icon:LoadMaterial(itemData.Icon)
        end
    end
    
    UIEventListener.Get(editBtn).onClick  = DelegateFactory.VoidDelegate(function(go)
        if CClientHouseMgr.Inst:IsHouseOwner() or self.m_YardDecorationPrivalege then
            CLuaZswTemplateMgr.SelectEditTemplateId = self.m_TemplateItemList[row]
            CUIManager.ShowUI(CLuaUIResources.ZswTemplateEditWnd)
            CUIManager.CloseUI(CLuaUIResources.ZswTemplateTopRightWnd)
        else
            g_MessageMgr:ShowMessage("ZswTemplate_Use_NoPrivilege")
        end
    end)
end

function CLuaZswTemplateTopRightWnd:OnGetMoreBtnClick()
    CShopMallMgr.SelectMallIndex = 0
    CShopMallMgr.SelectCategory = 2
    CShopMallMgr.SelectSubCategory = 9
    CUIManager.ShowUI("ShoppingMallWnd")
    CUIManager.CloseUI(CLuaUIResources.ZswTemplateTopRightWnd)
end
