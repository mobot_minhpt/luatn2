local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Ease = import "DG.Tweening.Ease"
local Animation = import "UnityEngine.Animation"

LuaCHPresentPopTipTemplate = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistClassMember(LuaCHPresentPopTipTemplate, "m_Texture")
RegistClassMember(LuaCHPresentPopTipTemplate, "m_Id")
RegistClassMember(LuaCHPresentPopTipTemplate, "m_Index")
RegistClassMember(LuaCHPresentPopTipTemplate, "m_ComboCount")
RegistClassMember(LuaCHPresentPopTipTemplate, "m_Time")
RegistClassMember(LuaCHPresentPopTipTemplate, "m_Tip")
RegistClassMember(LuaCHPresentPopTipTemplate, "m_ComboLabel")
RegistClassMember(LuaCHPresentPopTipTemplate, "m_ShowTick")

RegistClassMember(LuaCHPresentPopTipTemplate, "m_SrcPlayerName")
RegistClassMember(LuaCHPresentPopTipTemplate, "m_DstPlayerName")
RegistClassMember(LuaCHPresentPopTipTemplate, "m_PresentId")
--@endregion RegistChildComponent end

function LuaCHPresentPopTipTemplate:Awake()
    self.m_Texture = self.transform:GetComponent(typeof(UITexture))
    self.transform:Find("clubhouse_tanchaung_bai").gameObject:SetActive(false)
    self.transform:Find("clubhouse_tanchaung_hong").gameObject:SetActive(false)
    self.transform:Find("clubhouse_tanchaung_huang").gameObject:SetActive(false)
    self.m_Index = 1
    self.m_ComboCount = 0
end

function LuaCHPresentPopTipTemplate:Init(id, time, srcPlayerName, dstPlayerName, PresentId, Combo)
    self.m_SrcPlayerName = srcPlayerName
    self.m_DstPlayerName = dstPlayerName
    self.m_PresentId = PresentId

    self.m_Id = id
    local presentData = GameplayItem_ClubHousePresent.GetData(PresentId)
    self.m_Tip = self.transform:Find(SafeStringFormat3("clubhouse_tanchaung_%s", presentData.QualityType))
    self.m_ComboLabel = self.m_Tip.transform:Find("ComboLabel"):GetComponent(typeof(UILabel))

    local dstPlayerNameLabel = self.m_Tip.transform:Find("PresentNameLabel"):GetComponent(typeof(UILabel))
    self:WrapText(dstPlayerNameLabel, dstPlayerName)
    self.m_Tip.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(presentData.Icon)
    self.m_Tip.transform:Find("PlayerNameLabel"):GetComponent(typeof(UILabel)).text = srcPlayerName
    self:UpdateCombo(Combo, time)
    self.m_Tip.gameObject:SetActive(true)
    self.m_Tip.transform:GetComponent(typeof(Animation)):Play("clubhouse_tanchaung_hong_show")

    self.transform.localPosition = Vector3(-158, 490, 0)
end

function LuaCHPresentPopTipTemplate:UpdateIndex(srcPlayerName_New, dstPlayerName_New, PresentId_New)
    if self.m_SrcPlayerName == srcPlayerName_New and self.m_DstPlayerName == dstPlayerName_New and self.m_PresentId == PresentId_New then
        -- 与新增弹窗完全一致，则关闭旧弹窗
        self:ClosePopTip()
        return
    end

    local changeTime = 0.3
    if self.m_Index == 1 then
        LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(self.transform, Vector3(1, 1, 1), Vector3(0.85, 0.85, 0.85), changeTime), Ease.OutExpo)
        LuaTweenUtils.SetEase(LuaTweenUtils.TweenAlpha(self.m_Texture, 1, 0.8, changeTime), Ease.OutExpo)
        LuaTweenUtils.SetEase(LuaTweenUtils.TweenPosition(self.transform, -128.5,574.6,0,changeTime), Ease.OutExpo)
    elseif self.m_Index == 2 then
        LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(self.transform, Vector3(0.85, 0.85, 0.85), Vector3(0.7, 0.7, 0.7), changeTime), Ease.OutExpo)
        LuaTweenUtils.SetEase(LuaTweenUtils.TweenAlpha(self.m_Texture, 0.8, 0.6, changeTime), Ease.OutExpo)
        LuaTweenUtils.SetEase(LuaTweenUtils.TweenPosition(self.transform, -99, 640, 0,changeTime), Ease.OutExpo)
    elseif self.m_Index == 3 then
        self:ClosePopTip()
    end
    self.m_Index = self.m_Index + 1
end

function LuaCHPresentPopTipTemplate:ClosePopTip()
    if self.m_ShowTick then
        UnRegisterTick(self.m_ShowTick)
    end
    LuaTweenUtils.DOKill(self.transform, false)
    g_ScriptEvent:BroadcastInLua("ClubHouse_PresentPopTip_Close", self.m_Id)
    self.m_Tip.transform:GetComponent(typeof(Animation)):Play("clubhouse_tanchaung_hong_xiaoshi")
    self.m_ShowTick = RegisterTickOnce(function ()
        GameObject.Destroy(self.gameObject)
    end, 1000)
end

function LuaCHPresentPopTipTemplate:UpdateCombo(Combo, time)
    if self.m_ShowTick then
        UnRegisterTick(self.m_ShowTick)
    end
    self.m_Time = Combo - self.m_ComboCount
    self.m_ComboCount = Combo
    self.m_Time = self.m_Time > 3 and 3 or self.m_Time
    self:ShowComboTween()
    self.m_ShowTick = RegisterTickOnce(function ()
        self:ClosePopTip()
    end, time)
end

function LuaCHPresentPopTipTemplate:ShowComboTween()
    LuaTweenUtils.DOKill(self.m_ComboLabel.transform, false)
    if self.m_Time > 0 then
        self.m_Time = self.m_Time - 1
        local comboCount = self.m_ComboCount - self.m_Time
        self.m_ComboLabel.text = tostring(comboCount > 9999 and 9999 or comboCount)
        self.m_ComboLabel.transform:Find("Texture_+"):GetComponent(typeof(UITexture)):ResetAndUpdateAnchors()
        local tween = LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(self.m_ComboLabel.transform, Vector3(4, 4, 4), Vector3(1, 1, 1), 0.5), Ease.OutExpo)
        LuaTweenUtils.SetEase(LuaTweenUtils.TweenAlpha(self.m_ComboLabel, 0, 1, 0.5), Ease.OutExpo)
        LuaTweenUtils.OnComplete(tween, function()
            self:ShowComboTween()
        end)
    end
end

function LuaCHPresentPopTipTemplate:WrapText(label, originText)
    --赋值对label各项参数进行初始化，否则下面的计算会出问题，label类型需要clamp content， maxlines = 1
    label.text = originText
    local fit = true
    local outerText = ""
    fit, outerText = label:Wrap(originText)
    if not fit then
        label.text = CommonDefs.StringSubstring2(outerText, 0, CommonDefs.StringLength(outerText) - 1) .. "..."
    end
end

--@region UIEvent

--@endregion UIEvent

function LuaCHPresentPopTipTemplate:OnDisable()
    if self.m_ShowTick then
        UnRegisterTick(self.m_ShowTick)
    end
    if self.m_ComboLabel then
        LuaTweenUtils.DOKill(self.m_ComboLabel.transform, false)
    end
end