local Gac2GasC = import "L10.Game.Gac2Gas"

LuaSnowBallMgr = {}
LuaSnowBallMgr.SceInfo = nil
LuaSnowBallMgr.SelfRankInfo = nil
LuaSnowBallMgr.TotalRankInfo = nil
LuaSnowBallMgr.SignUp1Status = nil
LuaSnowBallMgr.SignUp2Status = nil
LuaSnowBallMgr.RemainWeeklyTimes = 0

function Gas2Gac.QueryXueQiuSeasonRankDataResult(data)
	local infos = MsgPackImpl.unpack(data)
	if infos and CommonDefs.DictContains(infos, typeof(String), 'IsEmpty') then
		LuaSnowBallMgr.SceInfo = nil
	else
		LuaSnowBallMgr.SceInfo = infos
	end

	CUIManager.ShowUI(CLuaUIResources.SnowBallHisWnd)
end

function Gas2Gac.QueryXueQiuSeasonRankResult(rankType,seasonId,selfData,udStr)
	local selfData = MsgPackImpl.unpack(selfData)
	if selfData then
		LuaSnowBallMgr.SelfRankInfo = selfData
	end

	local randData = MsgPackImpl.unpack(udStr)
	if randData then
		LuaSnowBallMgr.TotalRankInfo = randData
	end
	g_ScriptEvent:BroadcastInLua("UpdateSnowBallRankInfo")
end

function Gas2Gac.XueQiuDoublePlaySyncWatching(targetId)
	if not targetId or targetId == 0 then
		--表示清空数据，停止观战
	else

	end
end

Gas2Gac.QuerySignUpXueQiuStatusResult = function (bSignUp, remainWeeklyTimes)
	if bSignUp then
		LuaSnowBallMgr.SignUp1Status = true
	else
		LuaSnowBallMgr.SignUp1Status = false
	end
	LuaSnowBallMgr.RemainWeeklyTimes = remainWeeklyTimes
	g_ScriptEvent:BroadcastInLua("SyncSnowBallShowStatus")
end

Gas2Gac.QuerySignUpXueQiuDoubleStatusResult = function (bSignUp, remainWeeklyTimes)
	if bSignUp then
		LuaSnowBallMgr.SignUp2Status = true
	else
		LuaSnowBallMgr.SignUp2Status = false
	end
	LuaSnowBallMgr.RemainWeeklyTimes = remainWeeklyTimes
	g_ScriptEvent:BroadcastInLua("SyncSnowBallShowStatus")
end

function Gas2Gac.SignUpXueQiuSuccess(bTeam)
  Gac2GasC.QuerySignUpXueQiuStatus()
  Gac2GasC.QuerySignUpXueQiuDoubleStatus()
end
function Gas2Gac.CancelSignUpXueQiuSuccess()
  Gac2GasC.QuerySignUpXueQiuStatus()
  Gac2GasC.QuerySignUpXueQiuDoubleStatus()
end
function Gas2Gac.SignUpXueQiuDoubleSuccess(bTeam)
  Gac2GasC.QuerySignUpXueQiuStatus()
  Gac2GasC.QuerySignUpXueQiuDoubleStatus()
end
function Gas2Gac.CancelSignUpXueQiuDoubleSuccess()
  Gac2GasC.QuerySignUpXueQiuStatus()
  Gac2GasC.QuerySignUpXueQiuDoubleStatus()
end
