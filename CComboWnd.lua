-- Auto Generated!!
local CComboMgr = import "L10.UI.CComboMgr"
local CComboWnd = import "L10.UI.CComboWnd"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIManager = import "L10.UI.CUIManager"
local EnumComboDisplayType = import "L10.UI.EnumComboDisplayType"

CComboMgr.m_ShowCombo_CS2LuaHook = function (type, count, duration) 
    CComboMgr.type = type
    CComboMgr.count = count
    CComboMgr.duration = duration
    CUIManager.ShowUI(CIndirectUIResources.ComboWnd)
end
CComboWnd.m_Init_CS2LuaHook = function (this) 
    local label = nil
    repeat
        local default = CComboMgr.type
        if default == EnumComboDisplayType.Combo then
            this.comboRoot:SetActive(true)
            this.statusCtrlRoot:SetActive(false)
            this.statusDeCtrlRoot:SetActive(false)
            label = this.comboLabel
            break
        elseif default == EnumComboDisplayType.StatusCtrl then
            this.comboRoot:SetActive(false)
            this.statusCtrlRoot:SetActive(true)
            this.statusDeCtrlRoot:SetActive(false)
            label = this.statusCtrlLabel
            break
        elseif default == EnumComboDisplayType.StatusDeCtrl then
            this.comboRoot:SetActive(false)
            this.statusCtrlRoot:SetActive(false)
            this.statusDeCtrlRoot:SetActive(true)
            label = this.statusDeCtrlLabel
            break
        else
            this:Close()
            return
        end
    until 1

    this.startTime = CServerTimeMgr.Inst:GetZone8Time()
    this.endTime = this.startTime:AddSeconds(CComboMgr.duration)
    label.text = tostring(CComboMgr.count)
end
