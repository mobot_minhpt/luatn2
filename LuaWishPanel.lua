local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Vector2 = import "UnityEngine.Vector2"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CItemMgr = import "L10.Game.CItemMgr"
local Constants = import "L10.Game.Constants"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local CChatLinkMgr = import "CChatLinkMgr"

LuaWishPanel = class()
RegistChildComponent(LuaWishPanel, "WishButton", GameObject)
RegistChildComponent(LuaWishPanel, "SelfWishButton", GameObject)
RegistChildComponent(LuaWishPanel, "FriendWishButton", GameObject)
RegistChildComponent(LuaWishPanel, "InfoItem", GameObject)
RegistChildComponent(LuaWishPanel, "ScrollView", UIScrollView)
RegistChildComponent(LuaWishPanel, "Table", UITable)
RegistChildComponent(LuaWishPanel, "QnNumButton", QnAddSubAndInputButton)
RegistChildComponent(LuaWishPanel, "EmptyNode", GameObject)
RegistChildComponent(LuaWishPanel, "EmptyNode1", GameObject)

RegistClassMember(LuaWishPanel, "PanelType")
RegistClassMember(LuaWishPanel, "SavePanelPos")


function LuaWishPanel:OnEnable()
	g_ScriptEvent:AddListener("WishDelUpdate", self, "RefreshPanelList")
	g_ScriptEvent:AddListener("WishAddHelpUpdate", self, "RefreshPanelList")
	g_ScriptEvent:AddListener("WishAddUpdate", self, "RefreshPanelListAll")
end

function LuaWishPanel:OnDisable()
	g_ScriptEvent:RemoveListener("WishDelUpdate", self, "RefreshPanelList")
	g_ScriptEvent:RemoveListener("WishAddHelpUpdate", self, "RefreshPanelList")
	g_ScriptEvent:RemoveListener("WishAddUpdate", self, "RefreshPanelListAll")
end

function LuaWishPanel:AddTextClick(nodeLabel,text)
	nodeLabel.text = self:GetText(text)
	UIEventListener.Get(nodeLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
		local index = nodeLabel:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
		if index == 0 then
			index = - 1
		end

		local url = nodeLabel:GetUrlAtCharacterIndex(index)
		if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
			return
		end
	end)
end

function LuaWishPanel:DeleteItemComment(wishId,helpId)
  local backFunction = function(data)
    g_ScriptEvent:BroadcastInLua("WishAddHelpUpdate",wishId)
  end
  MessageWndManager.ShowOKCancelMessage(LocalString.GetString("是否删除这条助力的留言内容？"), DelegateFactory.Action(function ()
    LuaPersonalSpaceMgrReal.DeleteDetailWishComment(wishId,helpId,backFunction)
  end), nil, LocalString.GetString("删除"), nil, false)
end

function LuaWishPanel:InitCommentNode(node,data)
	if not data.helps or #data.helps <= 0 then
		node:SetActive(false)
		return 10
	end
	local comment = data.helps[1]

	if comment and comment.text and comment.text ~= '' then
		local nodeLabel = node.transform:Find('text'):GetComponent(typeof(UILabel))
		self:AddTextClick(nodeLabel,'[c][FFFE91]'..LocalString.GetString('助力者的祝福：')..'[-]'..comment.text)

		local bgSprite = node.transform:Find('containbg'):GetComponent(typeof(UISprite))
		bgSprite.height = nodeLabel.height + 16
		local deleteBtn = node.transform:Find('deletebutton').gameObject
		if data.roleId == CClientMainPlayer.Inst.Id or comment.roleId == CClientMainPlayer.Inst.Id then
			deleteBtn:SetActive(true)
			local helpId = comment.id
			local wishId = comment.wishId
			local onDeleteClick = function(go)
				self:DeleteItemComment(wishId,helpId)
			end
			CommonDefs.AddOnClickListener(deleteBtn,DelegateFactory.Action_GameObject(onDeleteClick),false)
		else
			deleteBtn:SetActive(false)
		end

		return nodeLabel.height + 32
	else
		node:SetActive(false)
		return 10
	end
end

function LuaWishPanel:GetText(text)
  local showText = CChatMgr.Inst:FilterYangYangEmotion(text)
  showText = CChatLinkMgr.TranslateToNGUIText(showText, false)
	return showText
end

function LuaWishPanel:InitWishItemNode(node,data)
  local itemData = CItemMgr.Inst:GetItemTemplate(data.templateId)

  if not itemData then
    return
  end
  local itemNum = data.num
	if itemNum and itemNum > 1 then
		node.transform:Find('num'):GetComponent(typeof(UILabel)).text = itemNum
	else
		node.transform:Find('num'):GetComponent(typeof(UILabel)).text = ''
	end
  node.transform:Find('pic'):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)
  local picBtn = node.transform:Find('pic').gameObject
	local onPicClick = function(go)
    CItemInfoMgr.ShowLinkItemTemplateInfo(data.templateId, false, nil, AlignType.Default, 0, 0, 0, 0)
	end
	CommonDefs.AddOnClickListener(picBtn,DelegateFactory.Action_GameObject(onPicClick),false)
  node.transform:Find('desc'):GetComponent(typeof(UILabel)).text = itemData.Name

	local statusDone = node.transform:Find('status/done').gameObject
	local statusDoing = node.transform:Find('status/doing').gameObject
	local statusOver = node.transform:Find('status/over').gameObject
  if data.currentProgress >= data.totalProgress then
    --node.transform:Find('restTime'):GetComponent(typeof(UILabel)).text = LocalString.GetString('已实现')
		if data.helps and data.helps[1] then
			node.transform:Find('name').gameObject:SetActive(true)
			local nameText = CChatLinkMgr.TranslateToNGUIText(CPersonalSpaceMgr.GeneratePlayerName(data.helps[1].roleId, data.helps[1].roleName))
			self:AddTextClick(node.transform:Find('name'):GetComponent(typeof(UILabel)),nameText)
			node.transform:Find('restTime').gameObject:SetActive(false)
		else
			node.transform:Find('name').gameObject:SetActive(false)
			node.transform:Find('restTime').gameObject:SetActive(false)
		end

		statusDone:SetActive(true)
		statusDoing:SetActive(false)
		statusOver:SetActive(false)
  else
		node.transform:Find('name').gameObject:SetActive(false)
		node.transform:Find('restTime').gameObject:SetActive(true)
    local leftTime = data.endTime/1000 - CServerTimeMgr.Inst.timeStamp
    local restDay = math.floor(leftTime/3600/24)
    if leftTime <= 0 then
      node.transform:Find('restTime'):GetComponent(typeof(UILabel)).text = LocalString.GetString("未在期限内实现")
			statusDone:SetActive(false)
			statusDoing:SetActive(false)
			statusOver:SetActive(true)
    else
      if restDay > 0 then
        node.transform:Find('restTime'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('剩余实现时间 [c][E8E918]%s天[-]'),restDay)
      else
        node.transform:Find('restTime'):GetComponent(typeof(UILabel)).text = LocalString.GetString('剩余实现时间 [c][FF0000]不足24小时[-]')
      end
			statusDone:SetActive(false)
			statusDoing:SetActive(true)
			statusOver:SetActive(false)
    end
  end
end

function LuaWishPanel:ShowWishDetail(data)
  LuaPersonalSpaceMgrReal.GetDetailWishInfo(data.wishId)
end

function LuaWishPanel:InitNodeSubInfo(nType,node,data)
	if nType == 1 then
		node.transform:Find('time').gameObject:SetActive(false)
	elseif nType == 2 then
		local createTime = CServerTimeMgr.ConvertTimeStampToZone8Time(data.startTime/1000)
		node.transform:Find('time'):GetComponent(typeof(UILabel)).text = SafeStringFormat3('%s-%s-%s',createTime.Year,createTime.Month,createTime.Day)
	end
	local favorBtn = node.transform:Find('favorbutton').gameObject
	local deleteBtn = node.transform:Find('deletebutton').gameObject
	favorBtn:SetActive(false)
	deleteBtn:SetActive(false)

  if data.roleId == CClientMainPlayer.Inst.Id then
		deleteBtn:SetActive(true)
		local onDeleteClick = function(go)
      MessageWndManager.ShowOKCancelMessage(LocalString.GetString("遗忘心愿后将无法再看到该心愿，确定要这么做吗？"), DelegateFactory.Action(function ()
        Gac2Gas.Wish_DelWish(data.wishId)
      end), nil, LocalString.GetString("遗忘"), nil, false)
		end
		CommonDefs.AddOnClickListener(deleteBtn,DelegateFactory.Action_GameObject(onDeleteClick),false)
	elseif (data.totalProgress > data.currentProgress and data.totalProgress > 0) and data.endTime/1000 - CServerTimeMgr.Inst.timeStamp > 0  then
    if data.templateId > 0 then
			favorBtn:SetActive(true)
      local onFavorClick = function(go)
        LuaPersonalSpaceMgrReal.QueryZhuliLimitAndOpenWnd(data)
      end
      CommonDefs.AddOnClickListener(favorBtn,DelegateFactory.Action_GameObject(onFavorClick),false)
		end
	end
end

function LuaWishPanel:InitSelfNode(node,data)
  local selfBgBtn = node.transform:Find('selfbgbutton').gameObject
  selfBgBtn:SetActive(true)
  node.transform:Find('bgbutton').gameObject:SetActive(false)
  node.transform:Find('selfbgbutton').gameObject:SetActive(true)
  node.transform:Find('icon').gameObject:SetActive(false)
  node.transform:Find('name').gameObject:SetActive(false)
  node.transform:Find('lv').gameObject:SetActive(false)
  node.transform:Find('moreBtn').gameObject:SetActive(false)

	local subInfoNode = node.transform:Find('subInfo').gameObject
	self:InitNodeSubInfo(1,subInfoNode,data)

  local timeNode = node.transform:Find('time').gameObject
  timeNode:SetActive(true)
  local createTime = CServerTimeMgr.ConvertTimeStampToZone8Time(data.startTime/1000)
  timeNode.transform:Find('time1'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('%s月%s日'),createTime.Month,createTime.Day)
  timeNode.transform:Find('time2'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('%s年'),createTime.Year)

	local statusNode = node.transform:Find('status'):GetComponent(typeof(UILabel))
  statusNode.text = self:GetText(data.text)
	UIEventListener.Get(statusNode.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
		local index = statusNode:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
		if index == 0 then
			index = - 1
		end

		local url = statusNode:GetUrlAtCharacterIndex(index)
		if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then

			return
		end
	end)

  local textHeight = node.transform:Find('status'):GetComponent(typeof(UILabel)).height

  local totalHeight = 0
  if data.templateId == 0 then
    node.transform:Find('addNode').gameObject:SetActive(false)
    subInfoNode.transform.localPosition = Vector3(subInfoNode.transform.localPosition.x,-33-textHeight,subInfoNode.transform.localPosition.z)
    totalHeight = 31 + textHeight + 71
  else
    local itemNode = node.transform:Find('addNode').gameObject
    itemNode:SetActive(true)
    self:InitWishItemNode(itemNode,data)
		local commentNode = node.transform:Find('messagebar').gameObject
		local commentHeight = self:InitCommentNode(commentNode,data)
    itemNode.transform.localPosition = Vector3(itemNode.transform.localPosition.x,-48-textHeight,itemNode.transform.localPosition.z)
    commentNode.transform.localPosition = Vector3(commentNode.transform.localPosition.x,-33-textHeight-87,commentNode.transform.localPosition.z)
    subInfoNode.transform.localPosition = Vector3(subInfoNode.transform.localPosition.x,-33-textHeight-93-commentHeight,subInfoNode.transform.localPosition.z)
    totalHeight = 193 + textHeight + commentHeight
  end

  local clickBtn = node.transform:Find('selfbgbutton').gameObject
  clickBtn:GetComponent(typeof(UISprite)).height = totalHeight

--	local onClick = function(go)
--    self:ShowWishDetail(data)
--	end
--	CommonDefs.AddOnClickListener(clickBtn,DelegateFactory.Action_GameObject(onClick),false)
end

function LuaWishPanel:InitFriendNode(node,data)
  local selfBgBtn = node.transform:Find('selfbgbutton').gameObject
  selfBgBtn:SetActive(true)
  node.transform:Find('bgbutton').gameObject:SetActive(true)
  node.transform:Find('selfbgbutton').gameObject:SetActive(false)
  node.transform:Find('icon').gameObject:SetActive(true)
  node.transform:Find('icon'):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(data.clazz, data.gender, -1),false)
  local iconBtn = node.transform:Find('icon/button').gameObject
	local onIconClick = function(go)
    CPlayerInfoMgr.ShowPlayerPopupMenu(data.roleId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgr.AlignType.Default)
	end
	CommonDefs.AddOnClickListener(iconBtn,DelegateFactory.Action_GameObject(onIconClick),false)

  node.transform:Find('name').gameObject:SetActive(true)
  node.transform:Find('name'):GetComponent(typeof(UILabel)).text = data.roleName
  node.transform:Find('lv').gameObject:SetActive(true)
  node.transform:Find('lv'):GetComponent(typeof(UILabel)).text = data.xianFanStatus > 0 and SafeStringFormat3("[c][%s]lv.%d[-][/c]", Constants.ColorOfFeiSheng, data.grade) or SafeStringFormat3("lv.%d", data.grade)
  node.transform:Find('moreBtn').gameObject:SetActive(false)

	local subInfoNode = node.transform:Find('subInfo').gameObject
	self:InitNodeSubInfo(2,subInfoNode,data)

  local timeNode = node.transform:Find('time').gameObject
  timeNode:SetActive(false)

	local statusNode = node.transform:Find('status'):GetComponent(typeof(UILabel))
  statusNode.text = self:GetText(data.text)
	UIEventListener.Get(statusNode.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
		local index = statusNode:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
		if index == 0 then
			index = - 1
		end

		local url = statusNode:GetUrlAtCharacterIndex(index)
		if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then

			return
		end
	end)

  local textHeight = node.transform:Find('status'):GetComponent(typeof(UILabel)).height
  local totalHeight = 0
  if data.templateId == 0 then
    node.transform:Find('addNode').gameObject:SetActive(false)
    subInfoNode.transform.localPosition = Vector3(subInfoNode.transform.localPosition.x,-33-textHeight,subInfoNode.transform.localPosition.z)
    totalHeight = 31 + textHeight + 71
  else
    local itemNode = node.transform:Find('addNode').gameObject
    itemNode:SetActive(true)
    self:InitWishItemNode(itemNode,data)
		local commentNode = node.transform:Find('messagebar').gameObject
		local commentHeight = self:InitCommentNode(commentNode,data)
    itemNode.transform.localPosition = Vector3(itemNode.transform.localPosition.x,-48-textHeight,itemNode.transform.localPosition.z)
    commentNode.transform.localPosition = Vector3(commentNode.transform.localPosition.x,-33-textHeight-87,commentNode.transform.localPosition.z)
    subInfoNode.transform.localPosition = Vector3(subInfoNode.transform.localPosition.x,-33-textHeight-93-commentHeight,subInfoNode.transform.localPosition.z)
    totalHeight = 186 + textHeight + commentHeight
  end

  local clickBtn = node.transform:Find('bgbutton').gameObject
  clickBtn:GetComponent(typeof(UISprite)).height = totalHeight + 58

--	local onClick = function(go)
--    self:ShowWishDetail(data)
--	end
--	CommonDefs.AddOnClickListener(clickBtn,DelegateFactory.Action_GameObject(onClick),false)
end

function LuaWishPanel:InitWishPanel(data,pageNum)
  local realData = data.list

  local listData = {}
  for i, data in ipairs(realData) do
    if data.currentProgress >= data.totalProgress then
      table.insert(listData, data)
    else
      local leftTime = data.endTime/1000 - CServerTimeMgr.Inst.timeStamp
      if leftTime > 0 then
        table.insert(listData, data)
      end
    end
  end
  local count = #listData

  self.EmptyNode:SetActive(false)
  self.EmptyNode1:SetActive(false)
  local emptyNode = self.EmptyNode
  if pageNum == 1 and LuaWishMgr.OpenUserId == CClientMainPlayer.Inst.Id then
    emptyNode = self.EmptyNode
  else
    emptyNode = self.EmptyNode1
  end

	local panel = CommonDefs.GetComponent_Component_Type(self.ScrollView, typeof(UIPanel))
	local savePos = self.ScrollView.transform.localPosition
	local saveOffsetY = panel.clipOffset.y

  if count == 0 then
    emptyNode:SetActive(true)
    self.QnNumButton:SetMinMax(1,pageNum,1)
  else
    local max = self.QnNumButton:GetMaxValue()
    if max == pageNum then
      self.QnNumButton:SetMinMax(1,max+1,1)
    end
    emptyNode:SetActive(false)
    for i,v in ipairs(listData) do
      local node = NGUITools.AddChild(self.Table.gameObject,self.InfoItem)
      node:SetActive(true)
      if self.PanelType == 1 then
        self:InitSelfNode(node,v)
      elseif self.PanelType == 2 then
        self:InitFriendNode(node,v)
      end
    end
  end

	self.Table:Reposition()
	self.ScrollView:ResetPosition()
	if self.SavePanelPos then
		self.ScrollView.transform.localPosition = savePos
		panel.clipOffset = Vector2(panel.clipOffset.x, saveOffsetY)
		self.SavePanelPos = nil
	end
end

function LuaWishPanel:GetSelfWishListByPage(pageNum)
  self.PanelType = 1
  Extensions.RemoveAllChildren(self.Table.transform)
  LuaPersonalSpaceMgrReal.GetSelfWishList(CClientMainPlayer.Inst.Id,pageNum,function(data)
    self:InitWishPanel(data.data,pageNum)
  end)
end

function LuaWishPanel:RefreshPanelList()
	self.SavePanelPos = true
  local nowPage = self.QnNumButton:GetValue()
  self.QnNumButton:SetValue(nowPage,true)
end

function LuaWishPanel:RefreshPanelListAll()
	self.SavePanelPos = nil
  local nowPage = self.QnNumButton:GetValue()
  self.QnNumButton:SetValue(nowPage,true)
end

function LuaWishPanel:GetSelfWishList()
  self.QnNumButton.onValueChanged = DelegateFactory.Action_uint(function(v)
    self:GetSelfWishListByPage(v)
  end)

  self.QnNumButton:SetMinMax(1,999,1)
  self.QnNumButton:SetValue(1,true)
end

function LuaWishPanel:GetPlayerWishListByPage(pageNum,id)
  self.PanelType = 1
  Extensions.RemoveAllChildren(self.Table.transform)
  LuaPersonalSpaceMgrReal.GetSelfWishList(id,pageNum,function(data)
    self:InitWishPanel(data.data,pageNum)
  end)
end

function LuaWishPanel:GetPlayerWishList(id)
  self.QnNumButton.onValueChanged = DelegateFactory.Action_uint(function(v)
    self:GetPlayerWishListByPage(v,id)
  end)

  self.QnNumButton:SetMinMax(1,999,1)
  self.QnNumButton:SetValue(1,true)
end


function LuaWishPanel:GetFriendWishListByPage(pageNum)
  self.PanelType = 2
  Extensions.RemoveAllChildren(self.Table.transform)
  LuaPersonalSpaceMgrReal.GetFriendWishList(pageNum,function(data)
    self:InitWishPanel(data.data,pageNum)
  end)
end

function LuaWishPanel:GetFriendWishList()
  self.QnNumButton.onValueChanged = DelegateFactory.Action_uint(function(v)
    self:GetFriendWishListByPage(v)
  end)

  self.QnNumButton:SetMinMax(1,999,1)
  self.QnNumButton:SetValue(1,true)
end

function LuaWishPanel:Init()
  if LuaWishMgr.OpenUserId == CClientMainPlayer.Inst.Id then
    self.WishButton:SetActive(false)
    self.SelfWishButton:SetActive(false)
    self.FriendWishButton:SetActive(true)
    self.InfoItem:SetActive(false)

    self:GetSelfWishList()

    self.WishButton:SetActive(true)
    local onWishClick = function(go)
      local config = PlayConfig_ServerIdEnabled.GetData(CLoginMgr.Inst.ServerGroupId)
      if config and config.Wish == 0 then
        g_MessageMgr:ShowMessage('CUSTOM_STRING2', LocalString.GetString('该服务器当前无法许愿'))
      else
        Gac2Gas.Wish_QueryWishLimit()
      end
    end
    CommonDefs.AddOnClickListener(self.WishButton,DelegateFactory.Action_GameObject(onWishClick),false)

    local onSelfWishClick = function(go)
      self.SelfWishButton:SetActive(false)
      self.FriendWishButton:SetActive(true)
      self:GetSelfWishList()
    end
    CommonDefs.AddOnClickListener(self.SelfWishButton,DelegateFactory.Action_GameObject(onSelfWishClick),false)

    local onFriendWishClick = function(go)
      self.SelfWishButton:SetActive(true)
      self.FriendWishButton:SetActive(false)
      self:GetFriendWishList()
    end
    CommonDefs.AddOnClickListener(self.FriendWishButton,DelegateFactory.Action_GameObject(onFriendWishClick),false)
  else
    self.WishButton:SetActive(false)
    self.SelfWishButton:SetActive(false)
    self.FriendWishButton:SetActive(false)
    self.InfoItem:SetActive(false)
    self:GetPlayerWishList(LuaWishMgr.OpenUserId)
  end
end
