-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLogMgr = import "L10.CLogMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CQnPointExchangeWnd = import "L10.UI.CQnPointExchangeWnd"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local QnButton = import "L10.UI.QnButton"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
CQnPointExchangeWnd.m_OnEnable_CS2LuaHook = function (this) 
    this:OnMoneyUpdated()
    --尝试更新一次灵玉值
    EventManager.AddListener(EnumEventType.MainPlayerUpdateMoney, MakeDelegateFromCSFunction(this.OnMoneyUpdated, Action0, this))
    this.m_OkButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_OkButton.OnClick, MakeDelegateFromCSFunction(this.OnOkButtonClick, MakeGenericClass(Action1, QnButton), this), true)
    this.m_CancelButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_CancelButton.OnClick, MakeDelegateFromCSFunction(this.OnCancelButtonClick, MakeGenericClass(Action1, QnButton), this), true)
    this.m_ExchangeCountButton.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_ExchangeCountButton.onValueChanged, MakeDelegateFromCSFunction(this.OnExchangeCountChanged, MakeGenericClass(Action1, UInt32), this), true)
    UIEventListener.Get(this.m_CloseButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_CloseButton).onClick, (DelegateFactory.VoidDelegate(function (go) 
        this:CloseView()
    end)), true)
end
CQnPointExchangeWnd.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListener(EnumEventType.MainPlayerUpdateMoney, MakeDelegateFromCSFunction(this.OnMoneyUpdated, Action0, this))
    this.m_OkButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_OkButton.OnClick, MakeDelegateFromCSFunction(this.OnOkButtonClick, MakeGenericClass(Action1, QnButton), this), false)
    this.m_CancelButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_CancelButton.OnClick, MakeDelegateFromCSFunction(this.OnCancelButtonClick, MakeGenericClass(Action1, QnButton), this), false)
    this.m_ExchangeCountButton.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_ExchangeCountButton.onValueChanged, MakeDelegateFromCSFunction(this.OnExchangeCountChanged, MakeGenericClass(Action1, UInt32), this), false)
    UIEventListener.Get(this.m_CloseButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_CloseButton).onClick, (DelegateFactory.VoidDelegate(function (go) 
        this:CloseView()
    end)), false)
end
CQnPointExchangeWnd.m_Init_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst.QnPoint <= 0 then
        this.m_ExchangeCountButton:SetMinMax(0, 0, 1)
        this.m_ExchangeCountButton:SetValue(0, true)
    else
        local maxValue = CQnPointExchangeWnd.s_MaxExchange
        if CClientMainPlayer.Inst.QnPoint < CQnPointExchangeWnd.s_MaxExchange then
            maxValue = CClientMainPlayer.Inst.QnPoint
        end
        this.m_ExchangeCountButton:SetMinMax(1, maxValue, 1)
        this.m_ExchangeCountButton:SetValue(maxValue, true)
    end
end
CQnPointExchangeWnd.m_OnOkButtonClick_CS2LuaHook = function (this, button) 
    local count = this.m_ExchangeCountButton:GetValue()
    if count > CClientMainPlayer.Inst.QnPoint then
        CLogMgr.LogError("Error: QnPoint Exchange Count Error")
    else
        Gac2Gas.RequestExchangeQnPoint2Jade(count)
    end
    this:CloseView()
end
