
local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CUIFx = import "L10.UI.CUIFx"
local CUITexture = import "L10.UI.CUITexture"

LuaDoubleOneBaotuanResultWnd = class()
RegistChildComponent(LuaDoubleOneBaotuanResultWnd,"closeBtn", GameObject)
RegistChildComponent(LuaDoubleOneBaotuanResultWnd,"shareBtn", GameObject)
RegistChildComponent(LuaDoubleOneBaotuanResultWnd,"win", GameObject)
RegistChildComponent(LuaDoubleOneBaotuanResultWnd,"fail", GameObject)

RegistClassMember(LuaDoubleOneBaotuanResultWnd, "maxChooseNum")

function LuaDoubleOneBaotuanResultWnd:Close()
	CUIManager.CloseUI("DoubleOneBaotuanResultWnd")
end

function LuaDoubleOneBaotuanResultWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaDoubleOneBaotuanResultWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaDoubleOneBaotuanResultWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
	local onShareClick = function(go)
    CUICommonDef.CaptureScreenAndShare()
	end
	CommonDefs.AddOnClickListener(self.shareBtn,DelegateFactory.Action_GameObject(onShareClick),false)

  if LuaDoubleOne2019Mgr.BoatuanResult == 0 then
    self.win:SetActive(true)
    self.fail:SetActive(false)
    local nodeTable = {self.win.transform:Find('node1').gameObject,self.win.transform:Find('node2').gameObject}
		for i,v in pairs(nodeTable) do
			v:SetActive(false)
		end
    if LuaDoubleOne2019Mgr.BoatuanEndInfo then
      for i,v in ipairs(LuaDoubleOne2019Mgr.BoatuanEndInfo) do
        if nodeTable[i] then
					nodeTable[i]:SetActive(true)
          self:InitNodeInfo(nodeTable[i],v)
        end
      end
    else

    end
    self.win.transform:Find('fx1'):GetComponent(typeof(CUIFx)):LoadFx("Fx/UI/Prefab/UI_baotuandazuozhan_chenggong.prefab")
    self.win.transform:Find('fx2'):GetComponent(typeof(CUIFx)):LoadFx("Fx/UI/Prefab/UI_hudie_tongyong_xiao.prefab")
    self.win.transform:Find('fx3'):GetComponent(typeof(CUIFx)):LoadFx("Fx/UI/Prefab/UI_hudie_tongyong_da.prefab")
  else
    self.win:SetActive(false)
    self.fail:SetActive(true)
    self.fail.transform:Find('fx1'):GetComponent(typeof(CUIFx)):LoadFx("Fx/UI/Prefab/UI_baotuandazuozhan_shibai.prefab")
    self.fail.transform:Find('fx2'):GetComponent(typeof(CUIFx)):LoadFx("Fx/UI/Prefab/UI_hudie_tongyong_xiao.prefab")
    self.fail.transform:Find('fx3'):GetComponent(typeof(CUIFx)):LoadFx("Fx/UI/Prefab/UI_hudie_tongyong_da.prefab")
  end
end

function LuaDoubleOneBaotuanResultWnd:InitNodeInfo(node,info)
	node.transform:Find('Mask/Icon'):GetComponent(typeof(CUITexture)):LoadPortrait(CUICommonDef.GetPortraitName(info[2],info[3]),true)
	node.transform:Find('SkillName'):GetComponent(typeof(UILabel)).text = info[1]
end

return LuaDoubleOneBaotuanResultWnd
