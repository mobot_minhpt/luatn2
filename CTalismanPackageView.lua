-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonItem = import "L10.Game.CCommonItem"
local CEquipment = import "L10.Game.CEquipment"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerShopData = import "L10.UI.CPlayerShopData"
local CPlayerShopItemData = import "L10.UI.CPlayerShopItemData"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CTalismanItemCell = import "L10.UI.CTalismanItemCell"
local CTalismanPackageView = import "L10.UI.CTalismanPackageView"
local CTalismanWndMgr = import "L10.UI.CTalismanWndMgr"
local DelegateFactory = import "DelegateFactory"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumTalismanQuality = import "L10.UI.EnumTalismanQuality"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local IdPartition = import "L10.Game.IdPartition"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUITools = import "NGUITools"
local QnButton = import "L10.UI.QnButton"
local RequestReason = import "L10.UI.CPlayerShopData+RequestReason"
local StringActionKeyValuePair = import "L10.Game.StringActionKeyValuePair"
local Talisman_Setting = import "L10.Game.Talisman_Setting"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTalismanPackageView.m_Init_CS2LuaHook = function (this) 

    this.radioBox.OnSelect = MakeDelegateFromCSFunction(this.OnRadioBoxSelect, MakeGenericClass(Action2, QnButton, Int32), this)
    --this.InitPackage();
    if System.String.IsNullOrEmpty(CTalismanWndMgr.selectEquipTalismanId) then
        this.radioBox:ChangeTo(CTalismanWndMgr.packageTabIndex, true)
    else
        local item = CItemMgr.Inst:GetById(CTalismanWndMgr.selectEquipTalismanId)
        if item == nil then
            this.radioBox:ChangeTo(0, true)
        else
            if item.Equip.IsNormalTalisman then
                this.radioBox:ChangeTo(0, true)
            elseif item.Equip.IsPremierTalisman then
                this.radioBox:ChangeTo(1, true)
            else
                this.radioBox:ChangeTo(2, true)
            end
        end
    end
end
CTalismanPackageView.m_OnRadioBoxSelect_CS2LuaHook = function (this, button, index) 

    this:LoadTalisman(CommonDefs.ConvertIntToEnum(typeof(EnumTalismanQuality), index))
    repeat
        local default = index
        if default == (0) or default == (1) then
            this.composeButton:SetActive(false)
            this.purchaseButton:SetActive(false)
            break
        elseif default == (2) then
            this.composeButton:SetActive(true)
            this.purchaseButton:SetActive(true)
            break
        else
            break
        end
    until 1
end
CTalismanPackageView.m_LoadTalisman_CS2LuaHook = function (this, quality) 

    Extensions.RemoveAllChildren(this.table.transform)
    this.itemCell:SetActive(false)
    CommonDefs.ListClear(this.package)
    this.curSelectQuality = quality
    local talismanList = CreateFromClass(MakeGenericClass(List, CCommonItem))
    local size = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    do
        local i = 1
        while i <= size do
            local continue
            repeat
                local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
                if System.String.IsNullOrEmpty(id) then
                    continue = true
                    break
                end
                local commonItem = CItemMgr.Inst:GetById(id)
                if commonItem ~= nil and commonItem.IsEquip and IdPartition.IdIsTalisman(commonItem.TemplateId) then
                    local color = commonItem.Equip.Color
                    repeat
                        local default = quality
                        if default == (EnumTalismanQuality.Normal) then
                            if commonItem.Equip.IsNormalTalisman then
                                CommonDefs.ListAdd(talismanList, typeof(CCommonItem), commonItem)
                            end
                            break
                        elseif default == (EnumTalismanQuality.Premier) then
                            if commonItem.Equip.IsPremierTalisman then
                                CommonDefs.ListAdd(talismanList, typeof(CCommonItem), commonItem)
                            end
                            break
                        elseif default == (EnumTalismanQuality.Fairy) then
                            if commonItem.Equip.IsFairyTalisman then
                                CommonDefs.ListAdd(talismanList, typeof(CCommonItem), commonItem)
                            end
                            break
                        else
                            break
                        end
                    until 1
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
    do
        local i = 0
        while i < talismanList.Count do
            local instance = NGUITools.AddChild(this.table.gameObject, this.itemCell)
            instance:SetActive(true)
            local cell = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CTalismanItemCell))
            cell:Init("")
            CommonDefs.ListAdd(this.package, typeof(CTalismanItemCell), cell)
            --if (i < talismanList.Count)
            --{
            this.package[i]:Init(talismanList[i].Id)
            UIEventListener.Get(this.package[i].gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.package[i].gameObject).onClick, MakeDelegateFromCSFunction(this.OnItemClicked, VoidDelegate, this), true)
            if CTalismanWndMgr.selectEquipTalismanId == talismanList[i].Id then
                this.curSelectItemId = talismanList[i].Id
                this:OnEquip()
                CTalismanWndMgr.ResetData()
            end
            --}
            --else package[i].Init("");
            i = i + 1
        end
    end
    if talismanList.Count > 0 then
        this.tips.text = ""
    else
        if quality == EnumTalismanQuality.Normal then
            this.tips.text = LocalString.GetString("你的包裹里没有普通法宝哦。")
        elseif quality == EnumTalismanQuality.Premier then
            this.tips.text = LocalString.GetString("你的包裹里没有高级法宝哦。")
        elseif quality == EnumTalismanQuality.Fairy then
            this.tips.text = LocalString.GetString("你的包裹里没有仙家法宝哦。")
        end
    end
    this.table:Reposition()
    this.scrollView:ResetPosition()
end
CTalismanPackageView.m_OnItemClicked_CS2LuaHook = function (this, go) 

    do
        local i = 0
        while i < this.package.Count do
            this.package[i]:SetSelect(go == this.package[i].gameObject and this.package[i].item ~= nil)
            if go == this.package[i].gameObject and this.package[i].item ~= nil then
                CItemInfoMgr.ShowLinkItemInfo(this.package[i].item, true, this, AlignType.Default, 0, 0, 0, 0, 0)
            end
            i = i + 1
        end
    end
end
CTalismanPackageView.m_GetActionPairs_CS2LuaHook = function (this, itemId, templateId) 
    this.curSelectItemId = itemId
    local actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
    local equipAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("佩戴"), MakeDelegateFromCSFunction(this.OnEquip, Action0, this))
    local baptizeAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("打造"), MakeDelegateFromCSFunction(this.OnBaptize, Action0, this))
    local baptizeExtraAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("打造"), MakeDelegateFromCSFunction(this.OnBaptizeExtraWord, Action0, this))
    local upgradeAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("升阶"), MakeDelegateFromCSFunction(this.OnUpgrade, Action0, this))
    local talismanChaijieAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("拆解"), MakeDelegateFromCSFunction(this.OnTalismanChaiJie, Action0, this))
    local sellAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("出售"), MakeDelegateFromCSFunction(this.OnSell, Action0, this))
    local discardAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("回收"), MakeDelegateFromCSFunction(this.OnDiscard, Action0, this))

    local item = CItemMgr.Inst:GetById(itemId)
    if item ~= nil and item.IsEquip and IdPartition.IdIsTalisman(item.TemplateId) then
        CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), equipAction)
        if item.Equip.IsBlueEquipment then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), baptizeExtraAction)
        elseif item.Equip.IsFairyTalisman or item.Equip.IsPremierTalisman then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), baptizeAction)
        end
        if item.Equip.IsFairyTalisman then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), upgradeAction)
        end
        local chaijieColor = Talisman_Setting.GetData().ChaiJieColor
        do
            local i = 0
            while i < chaijieColor.Length do
                if chaijieColor[i] == EnumToInt(item.Equip.Color) then
                    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), talismanChaijieAction)
                    break
                end
                i = i + 1
            end
        end
        if not item.IsBinded and CPlayerShopMgr.Inst:isAllowToTrade(item) then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), sellAction)
        end
        CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), discardAction)
    end

    return actionPairs
end
CTalismanPackageView.m_OnEquip_CS2LuaHook = function (this) 

    CItemInfoMgr.CloseItemInfoWnd()
    local item = CItemMgr.Inst:GetById(this.curSelectItemId)
    if item ~= nil then
        local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, this.curSelectItemId)
        if item.IsEquip and not item.IsBinded and item.Equip:IsPrecious() and CEquipment.GetMainPlayerIsFit(item.TemplateId, false) then
            local message = g_MessageMgr:FormatMessage("PRECIOUS_EQUIP_CONFIRM", item.ColoredName)
            MessageWndManager.ShowConfirmMessage(message, 5, false, DelegateFactory.Action(function () 
                Gac2Gas.RequestEquipItem(EnumItemPlace_lua.Bag, pos, this.curSelectItemId)
            end), nil)
        else
            Gac2Gas.RequestEquipItem(EnumItemPlace_lua.Bag, pos, this.curSelectItemId)
        end
    end
end
CTalismanPackageView.m_OnSell_CS2LuaHook = function (this) 
    local item = CItemMgr.Inst:GetById(this.curSelectItemId)
    if item ~= nil and CPlayerShopMgr.Inst:isAllowToTrade(item) then
        local protectTime = CPlayerShopMgr.Inst:CheckProtectTime(item)
        if protectTime > 0 then
            g_MessageMgr:ShowMessage("PLAYER_SHOP_ON_PROTECT_TIME", math.ceil(1 * protectTime / 60))
            return
        end
        CPlayerShopMgr.Inst:QueryPlayerShopStatus(DelegateFactory.Action(function () 
            if CPlayerShopData.Main.HasPlayerShop and CPlayerShopData.Main.BankruptTime == 0 then
                CPlayerShopData.Main:RequestPlayerShopInfo(DelegateFactory.Action_CPlayerShopData_Dictionary_uint_CPlayerShopItemData(function (shopData, itemdata) 
                    if CPlayerShopData.Main.HasEmptyPlace then
                        local FirstEmptyPlace = 1
                        while CommonDefs.DictContains(itemdata, typeof(UInt32), FirstEmptyPlace) do
                            FirstEmptyPlace = FirstEmptyPlace + 1
                        end
                        local data = CreateFromClass(CPlayerShopItemData)
                        data.ShopId = CPlayerShopData.Main.PlayerShopId
                        data.Item = item
                        data.OwnerId = CPlayerShopData.Main.OwnerId
                        data.OwnerName = CPlayerShopData.Main.OwnerName
                        data.Count = item.IsItem and item.Item.Count or 1
                        data.ShelfPlace = FirstEmptyPlace
                        data.PackagePlace = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, item.Id)
                        data.Price = 100
                        data.FocusCount = 0
                        data.IsFocus = false
                        CPlayerShopMgr.ShowShelfWnd(data)
                    else
                        g_MessageMgr:ShowMessage("BAGGAGE_IS_FULL")
                    end
                end), RequestReason.GetData)
            else
                g_MessageMgr:ShowMessage("NO_PLAYER_SHOP")
            end
        end))
    end
end
CTalismanPackageView.m_OnTalismanChaiJie_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst ~= nil then
        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(EnumItemPlace.Bag, this.curSelectItemId)
        if pos > 0 then
            Gac2Gas.RequestChaiJieFaBao(EnumItemPlace_lua.Bag, pos)
        end
    end
end
CTalismanPackageView.m_OnDiscard_CS2LuaHook = function (this) 
    local item = CItemMgr.Inst:GetById(this.curSelectItemId)
    if item ~= nil and item.Equip.NeedConfirmationWhenDiscard then
        local content = g_MessageMgr:FormatMessage("THROW_EQUIP_CONFIRM", item.ColoredName)
        MessageWndManager.ShowOKCancelMessage(content, DelegateFactory.Action(function () 
            if not item.IsBinded and item.IsPrecious then
                local text = g_MessageMgr:FormatMessage("Message_Recycle_Expensive_Confirm", item.ColoredName)
                MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(function () 
                    this:DoDiscard()
                end), nil, nil, nil, false)
            else
                this:DoDiscard()
            end
        end), nil, nil, nil, false)
    else
        this:DoDiscard()
    end
end
