local GameObject = import "UnityEngine.GameObject"
local UITable = import "UITable"
local NGUITools = import "NGUITools"
local UILabel = import "UILabel"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaShanYeMiZongRetroWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShanYeMiZongRetroWnd, "ParaLabel", "ParaLabel", UILabel)
RegistChildComponent(LuaShanYeMiZongRetroWnd, "Table", "Table", UITable)
RegistChildComponent(LuaShanYeMiZongRetroWnd, "TitleLabel", "TitleLabel", UILabel)

--@endregion RegistChildComponent end

function LuaShanYeMiZongRetroWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    
    self.ParaLabel.gameObject:SetActive(false)
end

function LuaShanYeMiZongRetroWnd:Init()
    local content = CUICommonDef.TranslateToNGUIText(ShuJia2022_Setting.GetData().ShanYeMiZongHuiGu)
    local title = string.match(content, "<title>(.*)</title>")
    self.TitleLabel.text = title
    for p in string.gmatch(content, "<p>(.-)</p>") do
        local pObj = NGUITools.AddChild(self.Table.gameObject, self.ParaLabel.gameObject)
        pObj:SetActive(true)
        pObj:GetComponent(typeof(UILabel)).text = p
    end
    self.Table:Reposition()
end

--@region UIEvent

--@endregion UIEvent

