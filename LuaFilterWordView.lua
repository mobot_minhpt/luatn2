local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local EChatPanel = import "L10.Game.EChatPanel"
local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"

LuaFilterWordView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFilterWordView, "EditButton", "EditButton", GameObject)
RegistChildComponent(LuaFilterWordView, "LabelDes", "LabelDes", GameObject)
RegistChildComponent(LuaFilterWordView, "TableView", "TableView", QnTableView)

--@endregion RegistChildComponent end

RegistClassMember(LuaFilterWordView, "m_chatwnd")
RegistClassMember(LuaFilterWordView, "m_dirty")
RegistClassMember(LuaFilterWordView, "m_data")
RegistClassMember(LuaFilterWordView, "m_ds")

function LuaFilterWordView:Awake()
	LuaChatMgr.LoadFilterWords()
	self.m_dirty = false
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.EditButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEditButtonClick()
	end)

    --@endregion EventBind end

    local function initItem(item,index)
		self:InitItem(item,index)
	end

	local function getCount()
		return #self.m_data
	end
	self.m_ds = DefaultTableViewDataSource.Create(getCount,initItem)
    self.TableView.m_DataSource = self.m_ds
end

function LuaFilterWordView:OnEnable()
	g_ScriptEvent:AddListener("OnChatFilterWordsChanged",self,"OnChatFilterWordsChanged")
end

function LuaFilterWordView:OnDisable()
	g_ScriptEvent:RemoveListener("OnChatFilterWordsChanged",self,"OnChatFilterWordsChanged")
    self:SaveData()
    self.m_chatwnd = nil
end

function LuaFilterWordView:OnChatFilterWordsChanged()
    self.m_data = LuaChatMgr.FilterWords
	self.m_dirty = false
    self:Refresh()
	self.m_chatwnd:Init(EChatPanel.WorldFilter)
end

function LuaFilterWordView:SaveData()
    if self.m_dirty then
		LuaChatMgr.SaveFilterWords(self.m_data)
		self.m_dirty = false
	end
end

function LuaFilterWordView:Init(pwnd)
    if self.m_chatwnd then return end
	self.m_chatwnd = pwnd
	self.m_data = LuaChatMgr.FilterWords
    self:Refresh()
end

function LuaFilterWordView:InitItem(item,index)
    local rindex = index+1
    local tg = item.transform:Find("Toggle").gameObject    
    tg:SetActive(self.m_data[rindex].Enabled)
    item.Text = self.m_data[rindex].Word
    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        local value = not self.m_data[rindex].Enabled
        tg:SetActive(value)
        self:Toggle(rindex,value)
	end)
end

function LuaFilterWordView:Refresh()
    
	local label = CommonDefs.GetComponent_GameObject_Type(self.LabelDes,typeof(UILabel))
	if CUIManager.IsLoaded(CLuaUIResources.ChatFilterWordEditWnd) or CUIManager.IsLoading(CLuaUIResources.ChatFilterWordEditWnd) then 
		label.text = LocalString.GetString("正在编辑")
		self.TableView.gameObject:SetActive(false)
		self.LabelDes:SetActive(true)
	else
		self.LabelDes:SetActive(#self.m_data <= 0)
		label.text = LocalString.GetString("点击右侧按键，设置世界频道筛选词")
		self.TableView.gameObject:SetActive(true)
    	self.TableView:ReloadData(true,false)
	end
end

function LuaFilterWordView:Toggle(i,tf)
    local index = i
    self.m_data[index].Enabled = tf
	self.m_chatwnd:Init(EChatPanel.WorldFilter)
	self.m_dirty = true
end

--@region UIEvent

function LuaFilterWordView:OnEditButtonClick()
	self:SaveData()
	CUIManager.ShowUI(CLuaUIResources.ChatFilterWordEditWnd)
	self:Refresh();
end

--@endregion UIEvent
