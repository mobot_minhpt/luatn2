local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Transform = import "UnityEngine.Transform"
local GameObject = import "UnityEngine.GameObject"
local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local QnButton = import "L10.UI.QnButton"
local CChatLinkMgr = import "CChatLinkMgr"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"

LuaBusinessCitySelectView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBusinessCitySelectView, "Slots", "Slots", Transform)
RegistChildComponent(LuaBusinessCitySelectView, "StartBtn", "StartBtn", CButton)
RegistChildComponent(LuaBusinessCitySelectView, "Tip", "Tip", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaBusinessCitySelectView, "m_Citys")
RegistClassMember(LuaBusinessCitySelectView, "m_City")
RegistClassMember(LuaBusinessCitySelectView, "m_CitysHighlights")
RegistClassMember(LuaBusinessCitySelectView, "m_OriTipText")

function LuaBusinessCitySelectView:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_Citys = {}
    self.m_CitysHighlights = {}
    Business_City.ForeachKey(function(key)
        local data = Business_City.GetData(key)
        local slot = self.Slots:GetChild(key-1)
        if slot then
            local go = slot.gameObject
            table.insert(self.m_Citys, go:GetComponent(typeof(CButton)))
            table.insert(self.m_CitysHighlights, go.transform:Find("Highlight"):GetComponent(typeof(UITexture)))
        end
    end)

    for i = 1, #self.m_Citys do 
        local cityBtn = self.m_Citys[i]
        UIEventListener.Get(cityBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnCityBtnClick(go, i)
        end)
    end

    UIEventListener.Get(self.StartBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnStartBtnClick(go)
    end)

    self.m_OriTipText = self.Tip.text
end

function LuaBusinessCitySelectView:Start()
    if LuaBusinessMgr.m_IsStoryMode then
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.BusinessGuide)
    end
end

--@region UIEvent

--@endregion UIEvent

function LuaBusinessCitySelectView:OnEnable()
    g_ScriptEvent:AddListener("OnBusinessDataUpdate", self, "OnBusinessDataUpdate")
    self:Refresh()
end

function LuaBusinessCitySelectView:OnDisable()
    g_ScriptEvent:RemoveListener("OnBusinessDataUpdate", self, "OnBusinessDataUpdate")
end

function LuaBusinessCitySelectView:Refresh()
    local limitCityId = LuaBusinessMgr:GetLimitCityId()
    for i = 1, #self.m_Citys do 
        local cityBtn = self.m_Citys[i]
        local highlight = self.m_CitysHighlights[i]
        local isNoLimit = limitCityId == 0
        local isRight = limitCityId == i 
        cityBtn.Enabled = isNoLimit or isRight
        highlight.alpha = 0
    end

    self.m_City = nil
    self.Tip.text = self.m_OriTipText
    self.StartBtn.Enabled = false
end

function LuaBusinessCitySelectView:OnCityBtnClick(go, i)
    for j = 1, #self.m_CitysHighlights do 
        local highlight = self.m_CitysHighlights[j]

        highlight.alpha = i == j and 1 or 0
    end
    self.m_City = i
    self.StartBtn.Enabled = true
    self.Tip.text = CChatLinkMgr.TranslateToNGUIText(g_MessageMgr:FormatMessage(Business_City.GetData(i).CityMessage), false)
end

function LuaBusinessCitySelectView:OnStartBtnClick(go)
    if self.m_City then
        if not LuaBusinessMgr.m_Cities[self.m_City] and not CGuideMgr.Inst:IsInPhase(EnumGuideKey.BusinessGuide)  then
            g_MessageMgr:ShowMessage(Business_City.GetData(self.m_City).CityMessage)
        end
        Gac2Gas.MoveToNextCity(self.m_City)
    end
end

function LuaBusinessCitySelectView:OnBusinessDataUpdate()
    self:Refresh()
end

function LuaBusinessCitySelectView:GetGuideGo(methodName)
    if methodName == "GetHangZhouBtn" then
        return self.m_Citys[1].gameObject
    elseif methodName == "GetStartBtn" then
        return self.StartBtn.gameObject
	end
	return nil
end
