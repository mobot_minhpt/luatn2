-- Auto Generated!!
local CButton = import "L10.UI.CButton"
local CCCChatMgr = import "L10.Game.CCCChatMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCMiniAPIMgr = import "L10.Game.CCMiniAPIMgr"
local CKeJuCCLive = import "L10.UI.CKeJuCCLive"
local CKeJuMgr = import "L10.Game.CKeJuMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local DelegateFactory = import "DelegateFactory"
local EnumCCMiniStreamType = import "L10.Game.EnumCCMiniStreamType"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CKeJuCCLive.m_Init_CS2LuaHook = function (this) 
    local applyCBtn = CommonDefs.GetComponent_GameObject_Type(this.applyButton, typeof(CButton))
    if not CKeJuMgr.Inst.isKejuZhuboApplyEnd and CKeJuMgr.Inst.zhuboInfo == nil then
        this.playerIcon.material = nil
        this.nameLabel.text = LocalString.GetString("[c][ACF8FF]说书人[c][FFFF00]招募中...")
        this.playButton:SetActive(false)
        this.applyButton:SetActive(true)
        if applyCBtn ~= nil then
            applyCBtn.Enabled = true
        end
        this.micButton:SetActive(false)
    else
        this.applyButton:SetActive(false)
        this.micButton:SetActive(false)
        this.playButton:SetActive(true)
        if CKeJuMgr.Inst.zhuboInfo ~= nil then
            local color = "ACF8FF"
            if CKeJuMgr.Inst.IsPlayerInKejuZhuBo then
                color = "00FF00"
                this.playButton:SetActive(false)
                this.micButton:SetActive(true)
            end
            this.nameLabel.text = System.String.Format("[c][{0}]{1}", color, CKeJuMgr.Inst.zhuboInfo.name)
            if applyCBtn ~= nil then
                applyCBtn.Enabled = true
            end
            this:UpdatePlayButton()
            this:OnCCCaptureStatusUpdate()
            this:LoadPortrait()
        else
            this.playerIcon.material = nil
            this.nameLabel.text = LocalString.GetString("[c][AAAAAA]空缺")
            if applyCBtn ~= nil then
                applyCBtn.Enabled = false
            end
        end
    end
end
CKeJuCCLive.m_UpdatePlayButton_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil or CKeJuMgr.Inst.zhuboInfo == nil then
        return
    end
    local inKejuCC = CCMiniAPIMgr.Inst:IsInKeJuStream()
    local default
    if inKejuCC then
        default = LocalString.GetString("禁听")
    else
        default = LocalString.GetString("收听")
    end
    this.playLabel.text = default
    this.playSprite.spriteName = inKejuCC and Constants.Common_Pause_Icon or Constants.Common_Play_Icon
end
CKeJuCCLive.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.applyButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.applyButton).onClick, MakeDelegateFromCSFunction(this.OnApplyButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.playButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.playButton).onClick, MakeDelegateFromCSFunction(this.OnPlayButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.playerIcon.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.playerIcon.gameObject).onClick, MakeDelegateFromCSFunction(this.OnPlayerIconClick, VoidDelegate, this), true)
    UIEventListener.Get(this.micButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.micButton).onClick, MakeDelegateFromCSFunction(this.OnMicButtonClick, VoidDelegate, this), true)
    EventManager.AddListener(EnumEventType.SyncKejuZhuboInfo, MakeDelegateFromCSFunction(this.Init, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.OnJoinCCStream, MakeDelegateFromCSFunction(this.OnJoinCCStream, MakeGenericClass(Action1, EnumCCMiniStreamType), this))
    EventManager.AddListenerInternal(EnumEventType.OnQuitCCStream, MakeDelegateFromCSFunction(this.OnQuitCCStream, MakeGenericClass(Action1, EnumCCMiniStreamType), this))
    EventManager.AddListener(EnumEventType.OnCCCaptureStatusUpdate, MakeDelegateFromCSFunction(this.OnCCCaptureStatusUpdate, Action0, this))
end
CKeJuCCLive.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.applyButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.applyButton).onClick, MakeDelegateFromCSFunction(this.OnApplyButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.playButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.playButton).onClick, MakeDelegateFromCSFunction(this.OnPlayButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.playerIcon.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.playerIcon.gameObject).onClick, MakeDelegateFromCSFunction(this.OnPlayerIconClick, VoidDelegate, this), false)
    UIEventListener.Get(this.micButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.micButton).onClick, MakeDelegateFromCSFunction(this.OnMicButtonClick, VoidDelegate, this), false)
    EventManager.RemoveListener(EnumEventType.SyncKejuZhuboInfo, MakeDelegateFromCSFunction(this.Init, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.OnJoinCCStream, MakeDelegateFromCSFunction(this.OnJoinCCStream, MakeGenericClass(Action1, EnumCCMiniStreamType), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnQuitCCStream, MakeDelegateFromCSFunction(this.OnQuitCCStream, MakeGenericClass(Action1, EnumCCMiniStreamType), this))
    EventManager.RemoveListener(EnumEventType.OnCCCaptureStatusUpdate, MakeDelegateFromCSFunction(this.OnCCCaptureStatusUpdate, Action0, this))

    this:OnLeaveLive()
end
CKeJuCCLive.m_OnLeaveLive_CS2LuaHook = function (this) 
    if CKeJuMgr.Inst.IsPlayerInKejuZhuBo and CCMiniAPIMgr.Inst:IsCapturing() then
        CCMiniAPIMgr.Inst:StopCCMiniCapture()
    elseif not CKeJuMgr.Inst.IsPlayerInKejuZhuBo then
        if not CCMiniAPIMgr.Inst.IsEngineVersionEnabled then
            --MessageMgr.Inst.ShowMessage("NEED_UPDATE_CLIENT");
            return
        end

        if not CCMiniAPIMgr.Inst:CheckWindowsPlatformCCMiniPath() then
            return
        end

        if CCMiniAPIMgr.Inst:IsInKeJuStream() then
            CCCChatMgr.Inst:LeaveRemoteCCChannel(EnumCCMiniStreamType.eKeJu)
        end
    end
end
CKeJuCCLive.m_OnApplyButtonClick_CS2LuaHook = function (this, go) 
    if not CKeJuMgr.Inst.huishiValid then
        g_MessageMgr:ShowMessage("APPLY_KEJUZHUBO_INVALID")
    else
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("APPLY_KEJUZHUBO_CONFIRM"), DelegateFactory.Action(function () 
            Gac2Gas.ApplyKeJuZhuBo()
        end), nil, nil, nil, false)
    end
end
CKeJuCCLive.m_OnPlayButtonClick_CS2LuaHook = function (this, go) 

    if CKeJuMgr.Inst.IsPlayerInKejuZhuBo then
        g_MessageMgr:ShowMessage("KEJU_ZHUBO_MYSELF")
    else
        if not CCCChatMgr.Inst.EnableKejuCC then
            g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
            return
        end
        if CKeJuMgr.Inst.zhuboInfo == nil or CKeJuMgr.Inst.zhuboInfo.playerId == 0 then
            g_MessageMgr:ShowMessage("KEJU_ZHUBO_NULL")
        else
            CCCChatMgr.Inst:SetKeJuCCListen()
        end
    end
end
CKeJuCCLive.m_OnMicButtonClick_CS2LuaHook = function (this, go) 

    if not CCCChatMgr.Inst.EnableKejuCC then
        g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
        return
    end

    if CKeJuMgr.Inst.IsPlayerInKejuZhuBo then
        CCCChatMgr.Inst:SetKeJuCCCapture()
    end
end
