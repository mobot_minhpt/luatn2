local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"

local QnTabView = import "L10.UI.QnTabView"

local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local DelegateFactory  = import "DelegateFactory"
local NGUITools = import "NGUITools"
local CChatLinkMgr = import "CChatLinkMgr"
local DouHunTan_Agenda = import "L10.Game.DouHunTan_Agenda"

LuaFightingSpiritRuleWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFightingSpiritRuleWnd, "RuleRoot", "RuleRoot", GameObject)
RegistChildComponent(LuaFightingSpiritRuleWnd, "AgendaRoot", "AgendaRoot", GameObject)
RegistChildComponent(LuaFightingSpiritRuleWnd, "WorldViewRoot", "WorldViewRoot", GameObject)
RegistChildComponent(LuaFightingSpiritRuleWnd, "WorldViewLabel", "WorldViewLabel", UILabel)
RegistChildComponent(LuaFightingSpiritRuleWnd, "RuleItem", "RuleItem", GameObject)
RegistChildComponent(LuaFightingSpiritRuleWnd, "AgendaItem", "AgendaItem", GameObject)
RegistChildComponent(LuaFightingSpiritRuleWnd, "AgandaTable", "AgandaTable", UITable)
RegistChildComponent(LuaFightingSpiritRuleWnd, "AgendaContainer", "AgendaContainer", GameObject)
RegistChildComponent(LuaFightingSpiritRuleWnd, "ShowArea", "ShowArea", QnTabView)
RegistChildComponent(LuaFightingSpiritRuleWnd, "RuleTable", "RuleTable", UITable)
RegistChildComponent(LuaFightingSpiritRuleWnd, "RuleScrollview", "RuleScrollview", CUIRestrictScrollView)

--@endregion RegistChildComponent end
RegistClassMember(LuaFightingSpiritRuleWnd,"m_TimeLabelList")
RegistClassMember(LuaFightingSpiritRuleWnd,"m_MarkList")
RegistClassMember(LuaFightingSpiritRuleWnd,"m_TitleList")

function LuaFightingSpiritRuleWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaFightingSpiritRuleWnd:Init()
    self.m_TitleList = {}
    self.m_TimeLabelList = {}
    self.m_MarkList = {}

    self:InitRule()
    self:InitAganda()

    self.WorldViewLabel.text = g_MessageMgr:FormatMessage("DouHunTan_WorldView_Desc")

    if CLuaFightingSpiritMgr.m_RuleWndPageIndex  then
        self.ShowArea:ChangeTo(CLuaFightingSpiritMgr.m_RuleWndPageIndex)
    else
        self.ShowArea:ChangeTo(0)
    end
end

function LuaFightingSpiritRuleWnd:InitRule()
    self.RuleItem:SetActive(false)
    local setting = DouHunTan_Setting.GetData()

    local i = 0 
    local cnt = setting.Saizhi_Rule.Length
    while i < cnt do
        local go = NGUITools.AddChild(self.RuleTable.gameObject, self.RuleItem)
        if go ~= nil then
            go:SetActive(true)
            go.transform:Find("Sprite/Title"):GetComponent(typeof(UILabel)).text= CChatLinkMgr.TranslateToNGUIText(setting.Saizhi_Rule[i][0], false)
            go.transform:Find("Content"):GetComponent(typeof(UILabel)).text= CChatLinkMgr.TranslateToNGUIText(setting.Saizhi_Rule[i][1], false)
        end
        i = i + 1
    end
    self.RuleTable:Reposition()
    self.RuleScrollview:ResetPosition()
end

function LuaFightingSpiritRuleWnd:InitAganda()
    self.AgendaContainer:SetActive(false)
    self.AgendaItem:SetActive(false)
    DouHunTan_Agenda.ForeachKey(
        DelegateFactory.Action_object(function (key)
            local data = DouHunTan_Agenda.GetData(key)
            local name = data.Name
            local desc = data.Des
            local go = NGUITools.AddChild(self.AgandaTable.gameObject, self.AgendaItem)
            if go ~= nil then
                go:SetActive(true)
                local nameLabel = go.transform:Find("HeadBg/Title"):GetComponent(typeof(UILabel))
                local descLabel = go.transform:Find("TimeBg/Time2"):GetComponent(typeof(UILabel))
                local numLabel = go.transform:Find("HeadBg/Num"):GetComponent(typeof(UILabel))
                local mark = go.transform:Find("Mark").gameObject

                nameLabel.text = name
                descLabel.text = desc
                numLabel.text = tostring(key)
                -- 需要复用一下卡页的获取活动状态
                mark:SetActive(false)
            end

            if key>9 then
                self.AgendaContainer:SetActive(true)
            end
        end))

    self.AgandaTable:Reposition()
end

--@region UIEvent

--@endregion UIEvent

