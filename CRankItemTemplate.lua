-- Auto Generated!!
local CRankClassTemplate = import "L10.UI.CRankClassTemplate"
local CRankItemTemplate = import "L10.UI.CRankItemTemplate"
local Profession = import "L10.Game.Profession"
CRankItemTemplate.m_InitInfo_CS2LuaHook = function (this, widthList, playerId, info, isOdd, _rankImage, lsId, pName, houseId, ownerId1, ownerId2, jiebaiClass, job, countryCode) 
    this.PlayerId = playerId
    this.lingshouId = lsId
    this.playerName = pName
    this.houseId = houseId
    this.ownerId1 = ownerId1
    this.ownerId2 = ownerId2
    do
        local i = 0
        while i < this.columns.Length do
            if i < widthList.Count then
                this.columns[i].width = widthList[i]
                if i < info.Count then
                    this.columns[i].text = info[i]
                end
            else
                this.columns[i].width = 0
                this.columns[i].text = ""
            end
            i = i + 1
        end
    end
    if this.career then
        if job and job ~= 0 then
            this.career.spriteName = Profession.GetIconByNumber(job)
        else
            this.career.spriteName = ""
        end
    end

    if this.countryFlag then
        this.countryFlag.spriteName = LuaSEASdkMgr:GetFlagSpriteName(countryCode)
    end

    local default
    if not isOdd then
        default = g_sprites.EvenBgSprite
    else
        default = g_sprites.OddBgSpirite
    end
    this.button:SetBackgroundSprite(default)

    if System.String.IsNullOrEmpty(_rankImage) then
        this.rankImage.spriteName = nil
    else
        this.rankImage.spriteName = _rankImage
        this.rankImage:MakePixelPerfect()
    end
    local cls = CommonDefs.GetComponent_Component_Type(this.columns[2], typeof(CRankClassTemplate))
    if cls ~= nil then
        if jiebaiClass ~= nil and this.columns ~= nil and this.columns.Length >= 3 then
            cls:Init(jiebaiClass)
        else
            cls:Init(nil)
        end
    end

    this.table:Reposition()
end
