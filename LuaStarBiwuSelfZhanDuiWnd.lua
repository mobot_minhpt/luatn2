local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local QnTableView=import "L10.UI.QnTableView"
local QnTabButton=import "L10.UI.QnTabButton"
local Vector3 = import "UnityEngine.Vector3"
local MessageWndManager = import "L10.UI.MessageWndManager"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local AlignType = import "CPlayerInfoMgr+AlignType"
local Profession = import "L10.Game.Profession"
local CUITexture=import "L10.UI.CUITexture"
local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local CCommonPlayerDisplayData = import "L10.Game.CCommonPlayerDisplayData"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local EnumCommonPlayerListUpdateType = import "L10.Game.EnumCommonPlayerListUpdateType"
local CIMMgr = import "L10.Game.CIMMgr"
local QnCheckBox = import "L10.UI.QnCheckBox"

CLuaStarBiwuSelfZhanDuiWnd = class()
CLuaStarBiwuSelfZhanDuiWnd.Path = "ui/starbiwu/LuaStarBiwuSelfZhanDuiWnd"
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"TeamTab")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"ApplicationsTab")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"TeamInfoRoot")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"ApplicationsRoot")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"TeamRoot")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"ApplicationRoot")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"TipLabel")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"SloganLabel")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"memberPrefab")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"MemberRoot")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"QuitButton")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"SettingButton")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"ModifySloganButton")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"m_NameLabel")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"NameInput")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"SloganInput")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"ModifyCancelButton")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"ModifyConfirmButton")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"memberList")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"AdvGridView")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"ClearButton")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"AcceptButton")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"RejectButton")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"m_ApplicationRetDotObj")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"m_CurrentRequestPlayerIndex")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"m_ApplicationFirstFlag")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd,"m_RequestPlayers")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd, "m_ZhanduiMemberIdTable")
RegistClassMember(CLuaStarBiwuSelfZhanDuiWnd, "m_HideMemberCheckBox")

function CLuaStarBiwuSelfZhanDuiWnd:Awake()
    self.TeamTab = self.transform:Find("Tabs/Tab1"):GetComponent(typeof(QnTabButton))
    self.ApplicationsTab = self.transform:Find("Tabs/Tab2"):GetComponent(typeof(QnTabButton))
    self.TeamInfoRoot = self.transform:Find("ShowArea/MemberInfoRoot").gameObject
    self.ApplicationsRoot = self.transform:Find("ShowArea/RequestListRoot").gameObject
    -- self.CloseButton = self.transform:Find("Wnd_Bg_Primary_Tab/CloseButton").gameObject
    self.TeamRoot = self.transform:Find("ShowArea/MemberInfoRoot").gameObject
    self.ApplicationRoot = self.transform:Find("ShowArea/RequestListRoot").gameObject
    self.m_HideMemberCheckBox = self.transform:Find("ShowArea/MemberInfoRoot/QnCheckbox"):GetComponent(typeof(QnCheckBox))
    self.TipLabel = self.transform:Find("ShowArea/MemberInfoRoot/TipLabel"):GetComponent(typeof(UILabel))
    self.SloganLabel = self.transform:Find("ShowArea/MemberInfoRoot/TopArea/SloganBG/SloganLabel"):GetComponent(typeof(UILabel))
    self.memberPrefab = self.transform:Find("ShowArea/MemberInfoRoot/MemberBG/MemberInfoItem").gameObject
    self.MemberRoot = self.transform:Find("ShowArea/MemberInfoRoot/MemberBG/MemberRoot")
    self.QuitButton = self.transform:Find("ShowArea/MemberInfoRoot/QuitButton").gameObject
    self.SettingButton = self.transform:Find("ShowArea/MemberInfoRoot/SettingButton").gameObject
    self.ModifySloganButton = self.transform:Find("ShowArea/MemberInfoRoot/TopArea/SloganBG/ModifySloganButton").gameObject
    self.m_NameLabel = self.transform:Find("ShowArea/MemberInfoRoot/TopArea/Name"):GetComponent(typeof(UILabel))
    self.memberList = {}
    self.AdvGridView = self.transform:Find("ShowArea/RequestListRoot/ApplicationAdvScrollView"):GetComponent(typeof(QnTableView))
    self.ClearButton = self.transform:Find("ShowArea/RequestListRoot/BottomArea/ClearButton").gameObject
    self.AcceptButton = self.transform:Find("ShowArea/RequestListRoot/BottomArea/AcceptButton").gameObject
    self.RejectButton = self.transform:Find("ShowArea/RequestListRoot/BottomArea/RejectButton").gameObject
    self.m_ApplicationRetDotObj = self.transform:Find("Tabs/Tab2/Sprite").gameObject
    --self.focusedApplicationID = ?
    self.m_CurrentRequestPlayerIndex = -1
    self.m_ApplicationFirstFlag = true
    --self.m_RequestPlayers = ?
    UIEventListener.Get(self.TeamTab.gameObject).onClick =DelegateFactory.VoidDelegate(function(go) self:onTeamTabClick(go) end)
    UIEventListener.Get(self.ApplicationsTab.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:onApplicationTabClick(go) end)
    UIEventListener.Get(self.QuitButton).onClick = DelegateFactory.VoidDelegate(function(go) self:onQuitTeamClick(go) end)
    UIEventListener.Get(self.SettingButton).onClick = DelegateFactory.VoidDelegate(function(go) self:onSettingClick(go) end)
    UIEventListener.Get(self.ClearButton).onClick = DelegateFactory.VoidDelegate(function(go) self:onClearClick(go) end)
    UIEventListener.Get(self.RejectButton).onClick = DelegateFactory.VoidDelegate(function(go) self:onRejecetClick(go) end)
    UIEventListener.Get(self.AcceptButton).onClick = DelegateFactory.VoidDelegate(function(go) self:onAcceptClick(go) end)
    UIEventListener.Get(self.ModifySloganButton).onClick = DelegateFactory.VoidDelegate(function(go) self:onModifySloganClick(go) end)

    self.m_HideMemberCheckBox.OnValueChanged = DelegateFactory.Action_bool(function(value)
        Gac2Gas.RequestSetStarBiwuPublishInfo(value and 1 or 0)
    end)

    self.m_ApplicationRetDotObj:SetActive(false)
end

function CLuaStarBiwuSelfZhanDuiWnd:Init( )
    self.AdvGridView.m_DataSource=DefaultTableViewDataSource.CreateByCount(0,function(item,row)
        -- local item=cmp.gameObject:GetComponent(typeof(CQMPKRequestPlayerItem))
        -- item:Init(self.m_RequestPlayers[row+1], row % 2 == 0)
        if row % 2 == 1 then
            item:SetBackgroundTexture(g_sprites.OddBgSpirite)
        else
            item:SetBackgroundTexture(g_sprites.EvenBgSprite)
        end

        self:InitRequestPlayerItem(item,row,self.m_RequestPlayers[row+1])
    end)

    self.AdvGridView.OnSelectAtRow =DelegateFactory.Action_int(function (row)
        self:OnRequestPlayerItemClicked(row)
    end)

    self:onTeamTabClick(self.gameObject)

    if CLuaStarBiwuMgr.isQuDaoSelfZhanDuiWnd then
        Gac2Gas.RequestSelfStarBiwuZhanDuiInfoQD()
    else
        Gac2Gas.RequestSelfStarBiwuZhanDuiInfo()
    end
    Gac2Gas.RequestStarBiwuZhanDuiApplyList(1)
end

function CLuaStarBiwuSelfZhanDuiWnd:InitRequestPlayerItem(item,row,player)
    local transform=item.transform
    local m_BgSprite = transform:GetComponent(typeof(UISprite))
    local m_PlayerProfessionSprite = transform:Find("ClazzIconSprite"):GetComponent(typeof(UISprite))
    m_PlayerProfessionSprite.spriteName = Profession.GetIconByNumber(player.m_Clazz)

    local transTbl = {"NameLabel", "PowerLabel", "XiuweiLabel", "XiulianLabel", "ScoreLabel"}
    local valueTbl = {player.m_Name, player.m_Capacity, player.m_Xiuwei, player.m_Xiulian, player.m_Score}
    for i = 1, #transTbl do
      transform:Find(transTbl[i]):GetComponent(typeof(UILabel)).text = valueTbl[i]
    end
end

function CLuaStarBiwuSelfZhanDuiWnd:OnRequestPlayerItemClicked( index)
    self.m_CurrentRequestPlayerIndex = index
    if index < #self.m_RequestPlayers then
        CPlayerInfoMgr.ShowPlayerPopupMenu(
            self.m_RequestPlayers[index+1].m_Id,
            EnumPlayerInfoContext.QMPK,
            EChatPanel.Undefined,
            nil,
            nil,
            Vector3.zero,
            AlignType.Default)
    end
end
function CLuaStarBiwuSelfZhanDuiWnd:OnEnable( )
    g_ScriptEvent:AddListener("ReplyStarBiwuZhanDuiInfo", self, "ReplyStarBiwuZhanDuiInfo")
    g_ScriptEvent:AddListener("ReplyStarBiwuZhanDuiApplyList", self, "OnReplyRequestPlayers")
    g_ScriptEvent:AddListener("UpdateStarBiwuZhanDuiTitleAndKouHaoSuccess", self, "UpdateZhanDuiNameAndSlogan")

    g_ScriptEvent:AddListener("SelectPlayer", self, "SelectPlayer")
    g_ScriptEvent:AddListener("ZhongQiuSelectPlayer", self, "ZhongQiuSelectPlayer")

    g_ScriptEvent:AddListener("UpdateStarBiwuZhanDuiPublishSuccess", self, "UpdateStarBiwuZhanDuiPublishSuccess")
end
function CLuaStarBiwuSelfZhanDuiWnd:OnDisable( )
    CLuaStarBiwuMgr.isQuDaoSelfZhanDuiWnd = false
    g_ScriptEvent:RemoveListener("ReplyStarBiwuZhanDuiInfo", self, "ReplyStarBiwuZhanDuiInfo")
    g_ScriptEvent:RemoveListener("ReplyStarBiwuZhanDuiApplyList", self, "OnReplyRequestPlayers")
    g_ScriptEvent:RemoveListener("UpdateStarBiwuZhanDuiTitleAndKouHaoSuccess", self, "UpdateZhanDuiNameAndSlogan")

    g_ScriptEvent:RemoveListener("SelectPlayer", self, "SelectPlayer")
    g_ScriptEvent:RemoveListener("ZhongQiuSelectPlayer", self, "ZhongQiuSelectPlayer")

    g_ScriptEvent:RemoveListener("UpdateStarBiwuZhanDuiPublishSuccess", self, "UpdateStarBiwuZhanDuiPublishSuccess")
end
function CLuaStarBiwuSelfZhanDuiWnd:UpdateZhanDuiNameAndSlogan( )
    self.m_NameLabel.text = CLuaStarBiwuMgr.m_CurrentZhanduiName
    self.SloganLabel.text = CLuaStarBiwuMgr.m_CurrentZhanduiSlogan
end
function CLuaStarBiwuSelfZhanDuiWnd:ReplyStarBiwuZhanDuiInfo( )
    if CLuaStarBiwuMgr.m_MySelfZhanduiId ~= CLuaStarBiwuMgr.m_CurrentZhanduiId then
        return
    end

    self.m_ZhanduiMemberIdTable = {}
    do
        local i = 0
        local maxCnt = StarBiWuShow_Setting.GetData().Max_ZhanDui_MemberNum
        while i < maxCnt do
            local go=self.memberList[i+1].gameObject
            local tf=go.transform
            go:SetActive(true)
            if i < #CLuaStarBiwuMgr.m_MemberInfoTable then
                local info=CLuaStarBiwuMgr.m_MemberInfoTable[i + 1]
                self:InitMemberItem(tf,CLuaStarBiwuMgr.m_MemberInfoTable[i + 1], i, CLuaStarBiwuMgr.m_ChuZhanInfoList)
                UIEventListener.Get(go).onClick=DelegateFactory.VoidDelegate(function(p)
                    self:OnItemClicked(info.m_Id,p)
                end)
                self.m_ZhanduiMemberIdTable[info.m_Id] = 1
            else
                -- self.memberList[i+1]:SetMemberInfo(nil, i, nil)
                self:InitMemberItem(tf,nil,i,nil)
                -- UIEventListener.Get(go).onClick=nil
                UIEventListener.Get(go).onClick=DelegateFactory.VoidDelegate(function(p)
                    self:OnInviteBtnClick()
                end)
            end
            i = i + 1
        end
    end
    self.SloganLabel.text = CLuaStarBiwuMgr.m_CurrentZhanduiSlogan
    self.m_NameLabel.text = CLuaStarBiwuMgr.m_CurrentZhanduiName
    -- self.NameInput.value = CQuanMinPKMgr.Inst.m_CurrentZhanduiName
    -- self.SloganInput.value = CQuanMinPKMgr.Inst.m_CurrentZhanduiSlogan
    self.TipLabel.text = CLuaStarBiwuMgr.m_MyZhanduiTip
    self.m_HideMemberCheckBox.gameObject:SetActive(not CLuaStarBiwuMgr.m_MyZhanduiTip or CLuaStarBiwuMgr.m_MyZhanduiTip == "")
    self.m_HideMemberCheckBox:SetSelected(CLuaStarBiwuMgr.m_HideMemberFlag > 0, true)
end

function CLuaStarBiwuSelfZhanDuiWnd:OnInviteBtnClick()
  if not CClientMainPlayer.Inst then return end

	CCommonPlayerListMgr.Inst.WndTitle = LocalString.GetString("邀请战队成员")
	CCommonPlayerListMgr.Inst.ButtonText = LocalString.GetString("邀请")
	CCommonPlayerListMgr.Inst.UpdateType = EnumCommonPlayerListUpdateType.Default
	CommonDefs.ListClear(CCommonPlayerListMgr.Inst.allData)

  CommonDefs.DictIterate(CClientMainPlayer.Inst.RelationshipProp.Friends, DelegateFactory.Action_object_object(function(k, v)
    local basicInfo = CIMMgr.Inst:GetBasicInfo(k)
    if basicInfo and not self.m_ZhanduiMemberIdTable[basicInfo.ID] then
      local data = CreateFromClass(CCommonPlayerDisplayData, basicInfo.ID, basicInfo.Name, basicInfo.Level, basicInfo.Class, basicInfo.Gender, basicInfo.Expression, true)
      CommonDefs.ListAdd(CCommonPlayerListMgr.Inst.allData, typeof(CCommonPlayerDisplayData), data)
    end
  end))

  local callback = function (playerId)
    Gac2Gas.IntiveOtherJoinStarBiwuZhanDui(playerId)
  end

  CCommonPlayerListMgr.Inst.OnPlayerSelected = DelegateFactory.Action_ulong(callback)
  CUIManager.ShowUI(CIndirectUIResources.CommonPlayerListWnd)
end

function CLuaStarBiwuSelfZhanDuiWnd:OnReplyRequestPlayers( players )
    -- local players=args[0]
    self.m_RequestPlayers = players

    if not self.m_ApplicationFirstFlag then
        self.AdvGridView.m_DataSource.count=#self.m_RequestPlayers
        self.AdvGridView:ReloadData(false, false)
    end
    if #players > 0 and self.m_ApplicationFirstFlag and CLuaStarBiwuMgr.m_IsTeamLeader then
        self.m_ApplicationRetDotObj:SetActive(true)
    else
        self.m_ApplicationRetDotObj:SetActive(false)
    end


end
function CLuaStarBiwuSelfZhanDuiWnd:OnClearZhanDuiApplyListSuccess( )
    self.m_RequestPlayers={}
    self.AdvGridView.m_DataSource.count=0
    self.AdvGridView:ReloadData(false, false)
end
function CLuaStarBiwuSelfZhanDuiWnd:initMembers( )
    self.memberList ={}
    self.memberPrefab:SetActive(false)
    do
        local i = 0
        while i < StarBiWuShow_Setting.GetData().Max_ZhanDui_MemberNum do
            local go = NGUITools.AddChild(self.MemberRoot.gameObject,self.memberPrefab)
            go:SetActive(false)
            local trans = go.transform
            self:SetItemSelected(trans,false)
            table.insert( self.memberList,trans )
            i = i + 1
        end
        self.MemberRoot:GetComponent(typeof(UIGrid)):Reposition()
    end

end
function CLuaStarBiwuSelfZhanDuiWnd:OnItemClicked( playerId, go)
    if playerId <= 0 then
        CUIManager.ShowUI(CLuaUIResources.QMPKRecruitWnd)
        return
    end
    CPlayerInfoMgr.ShowPlayerPopupMenu(math.floor(playerId), EnumPlayerInfoContext.StarBiwu, EChatPanel.Undefined, nil, nil, go.transform.position, CPlayerInfoMgr.AlignType.Right)

    do
        local i = 0 local cnt = #self.memberList--.Count
        while i < cnt do
            -- self.memberList[i+1]:SetSelected(self.memberList[i+1].gameObject == go)
            self:SetItemSelected(self.memberList[i+1],self.memberList[i+1].gameObject == go)
            i = i + 1
        end
    end
end
function CLuaStarBiwuSelfZhanDuiWnd:onTeamTabClick( go)
    self.TeamTab.Selected = true
    self.ApplicationsTab.Selected = false
    self.TeamRoot:SetActive(true)
    self.ApplicationRoot:SetActive(false)
    if self.MemberRoot.childCount == 0 then
        self:initMembers()
    end
end
function CLuaStarBiwuSelfZhanDuiWnd:onApplicationTabClick( go)
    self.TeamTab.Selected = false
    self.ApplicationsTab.Selected = true
    self.TeamRoot:SetActive(false)
    self.ApplicationRoot:SetActive(true)
    self.AdvGridView:Clear()
    if not self.m_ApplicationFirstFlag then
        Gac2Gas.RequestStarBiwuZhanDuiApplyList(1)
    else
        self.AdvGridView.m_DataSource.count = self.m_RequestPlayers and  #self.m_RequestPlayers or 0
        self.AdvGridView:ReloadData(false, false)
    end
    self.m_ApplicationFirstFlag = false
    self.m_ApplicationRetDotObj:SetActive(false)
    self.m_CurrentRequestPlayerIndex = -1
end
function CLuaStarBiwuSelfZhanDuiWnd:onQuitTeamClick( go)
    MessageWndManager.ShowOKCancelMessage(LocalString.GetString("确定要退出战队吗？"), DelegateFactory.Action(function()
        self:confirmQuit()
    end), nil, nil, nil, false)
end
function CLuaStarBiwuSelfZhanDuiWnd:confirmQuit( )
    Gac2Gas.RequestLeaveStarBiwuZhanDui()
    -- self:Close()
    CUIManager.CloseUI(CLuaUIResources.StarBiwuSelfZhanDuiWnd)
end
function CLuaStarBiwuSelfZhanDuiWnd:onSettingClick( go)
    CUIManager.ShowUI(CLuaUIResources.StarBiwuZhanDuiSettingWnd)
end
function CLuaStarBiwuSelfZhanDuiWnd:onModifySloganClick( go)
    CUIManager.ShowUI(CLuaUIResources.StarBiwuModifySloganWnd)
end
function CLuaStarBiwuSelfZhanDuiWnd:onAcceptClick( go)
    if self.m_CurrentRequestPlayerIndex >= 0 and self.m_CurrentRequestPlayerIndex < #self.m_RequestPlayers then
        Gac2Gas.AcceptApplyPlayerJoinStarBiwuZhanDui(self.m_RequestPlayers[self.m_CurrentRequestPlayerIndex+1].m_Id)
    else
        g_MessageMgr:ShowMessage("DOUHUN_NEED_ONE_PLAYER")
    end
end
function CLuaStarBiwuSelfZhanDuiWnd:onClearClick( go)
    Gac2Gas.RequestClearStarBiwuZhanDuiApplyList()
end
function CLuaStarBiwuSelfZhanDuiWnd:onRejecetClick( go)
    if self.m_CurrentRequestPlayerIndex >= 0 and self.m_CurrentRequestPlayerIndex < #self.m_RequestPlayers then
        Gac2Gas.RefuseApplyPlayerJoinStarBiwuZhanDui(self.m_RequestPlayers[self.m_CurrentRequestPlayerIndex+1].m_Id)
    else
        g_MessageMgr:ShowMessage("DOUHUN_NEED_ONE_PLAYER")
    end
end

function CLuaStarBiwuSelfZhanDuiWnd:SetItemSelected(transform,state)
    local m_SelectedObj = transform:Find("InfoRoot/Sprite").gameObject
    m_SelectedObj:SetActive(state)
end
function CLuaStarBiwuSelfZhanDuiWnd:InitMemberItem(transform,member, index, chuZhanInfo)
    local IconTexture = transform:Find("InfoRoot/Icon"):GetComponent(typeof(CUITexture))
    local LevelLabel = transform:Find("InfoRoot/LevelBG/LevelLabel"):GetComponent(typeof(UILabel))
    local NameLabel = transform:Find("InfoRoot/NameLabel"):GetComponent(typeof(UILabel))
    local LeaderMark = transform:Find("InfoRoot/LeaderMark").gameObject
    local ClazzMarkSprite = transform:Find("InfoRoot/ClazzSprite"):GetComponent(typeof(UISprite))
    local m_GroupObj = transform:Find("InfoRoot/ChuZhanSprite1").gameObject
    local m_ThreeVThreeObj = transform:Find("InfoRoot/ChuZhanSprite2").gameObject
    local m_SigleObj = transform:Find("InfoRoot/ChuZhanSprite3").gameObject
    local m_TwoVTwoObj = transform:Find("InfoRoot/ChuZhanSprite4").gameObject
    local m_SigleIndexLabel = transform:Find("InfoRoot/ChuZhanSprite3/BG/Label"):GetComponent(typeof(UILabel))
    local m_SelectedObj = transform:Find("InfoRoot/Sprite").gameObject
    local m_NoMemberObj = transform:Find("Label").gameObject
    local m_InfoRoot = transform:Find("InfoRoot").gameObject

    local m_PlayerId=0

    if nil == member then
        m_NoMemberObj:SetActive(true)
        m_InfoRoot:SetActive(false)
        m_PlayerId = 0
        return
    end
    m_NoMemberObj:SetActive(false)
    m_InfoRoot:SetActive(true)
    m_SelectedObj:SetActive(false)
    m_PlayerId = member.m_Id
    IconTexture:LoadNPCPortrait(CUICommonDef.GetPortraitName(member.m_Class, member.m_Gender, -1), false)
    LevelLabel.text = System.String.Format("lv.{0}", member.m_Grade)
    NameLabel.text = member.m_Name
    LeaderMark:SetActive(index == 0)
    ClazzMarkSprite.spriteName = Profession.GetIcon(member.m_Class)
    if CClientMainPlayer.Inst ~= nil and m_PlayerId == CClientMainPlayer.Inst.Id then
        NameLabel.color = Color(16 / 255, 140/ 255, 0)
    else
        NameLabel.color = Color.white
    end
    if chuZhanInfo == nil or chuZhanInfo.Length < 6 then
        return
    end
    if (bit.band(chuZhanInfo[0], (bit.lshift(1, index)))) ~= 0 or (bit.band(chuZhanInfo[4], (bit.lshift(1, index)))) ~= 0 then
        m_GroupObj:SetActive(true)
    else
        m_GroupObj:SetActive(false)
    end
    if (bit.band(chuZhanInfo[1], (bit.lshift(1, index)))) ~= 0 or (bit.band(chuZhanInfo[5], (bit.lshift(1, index)))) ~= 0 then
        m_SigleObj:SetActive(true)
        if (bit.band(chuZhanInfo[1], (bit.lshift(1, index)))) ~= 0 then
            m_SigleIndexLabel.text = LocalString.GetString("一")
        else
            m_SigleIndexLabel.text = LocalString.GetString("二")
        end
    else
        m_SigleObj:SetActive(false)
    end
    if (bit.band(chuZhanInfo[3], (bit.lshift(1, index)))) ~= 0 then
        m_ThreeVThreeObj:SetActive(true)
    else
        m_ThreeVThreeObj:SetActive(false)
    end

    m_TwoVTwoObj:SetActive((bit.band(chuZhanInfo[2], (bit.lshift(1, index)))) ~= 0)
end

function CLuaStarBiwuSelfZhanDuiWnd:UpdateStarBiwuZhanDuiPublishSuccess(hideFlag)
    self.m_HideMemberCheckBox:SetSelected(hideFlag > 0, true)
end
