
local CCCChatMgr = import "L10.Game.CCCChatMgr"
local CJoinCCChanelContext = import "L10.Game.CJoinCCChanelContext"
local CJoinCCChannelRequestSourceInfo = import "L10.Game.CJoinCCChannelRequestSourceInfo"
local CChatLinkMgr = import "CChatLinkMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Object = import "System.Object"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local CCPlayerCtrl = import "L10.Game.CCPlayerCtrl"
local CLoginMgr = import "L10.Game.CLoginMgr"

CLuaActivityZhiBoMgr = class()

CLuaActivityZhiBoMgr.ZhiBoIsOpen = false -- 直播功能是否开启
CLuaActivityZhiBoMgr.ZhiBoURL = "" --网页版直播地址
CLuaActivityZhiBoMgr.JumpToCCLive = false --是否跳转到游戏内直播
CLuaActivityZhiBoMgr.ZhuBoName = "" --主播名
CLuaActivityZhiBoMgr.ZhuBoCcid = 0 --主播CCID
CLuaActivityZhiBoMgr.Abstract = "" --直播内容简介
CLuaActivityZhiBoMgr.CCIDFlag = 0
CLuaActivityZhiBoMgr.IsFeiSheng = 0
CLuaActivityZhiBoMgr.BottomLevel = 0
CLuaActivityZhiBoMgr.TopLevel = 0
CLuaActivityZhiBoMgr.BottomVipLvel = 0
CLuaActivityZhiBoMgr.TopVipLvel = 0
CLuaActivityZhiBoMgr.ExtraInfo = nil

function CLuaActivityZhiBoMgr:AddListener( ... )
	g_ScriptEvent:RemoveListener("MainPlayerLevelChange",self,"OnUpdateLevel")
	g_ScriptEvent:AddListener("MainPlayerLevelChange",self,"OnUpdateLevel")
end
CLuaActivityZhiBoMgr:AddListener()

function CLuaActivityZhiBoMgr:OnUpdateLevel()
	if CClientMainPlayer.Inst ~= nil then
        local level
		--按当前等级计算
		if CLuaActivityZhiBoMgr.IsFeisheng == 0 then
			level = CClientMainPlayer.Inst.Level
		--按仙凡最高等级计算
		else
			level = CClientMainPlayer.Inst.MaxLevel
		end
		local vipLevel = CClientMainPlayer.Inst.ItemProp.Vip.Level

		if level >= CLuaActivityZhiBoMgr.BottomLevel and level <= CLuaActivityZhiBoMgr.TopLevel then
			if vipLevel >= CLuaActivityZhiBoMgr.BottomVipLvel and vipLevel <= CLuaActivityZhiBoMgr.TopVipLvel then
				g_ScriptEvent:BroadcastInLua("SyncZhiBoStatus",CLuaActivityZhiBoMgr.ZhiBoIsOpen or false)
				return
			end
		end
		g_ScriptEvent:BroadcastInLua("SyncZhiBoStatus",false)
    end
end

function CLuaActivityZhiBoMgr.UpdateZhiBoInfo(bOpen, url, bJump, zhuboName, ccid, showTxt, idFlag, feiShengFlag, level1, level2, vipLevel1, vipLevel2, extraInfoTbl)
	CLuaActivityZhiBoMgr.ZhiBoIsOpen = bOpen or false
	CLuaActivityZhiBoMgr.ZhiBoURL = url or ""
	CLuaActivityZhiBoMgr.JumpToCCLive = bJump or false
	CLuaActivityZhiBoMgr.ZhuBoName = zhuboName or ""
	CLuaActivityZhiBoMgr.ZhuBoCcid = ccid or false
	CLuaActivityZhiBoMgr.Abstract = showTxt and showTxt or ""
	CLuaActivityZhiBoMgr.CCIDFlag = idFlag or 0
	CLuaActivityZhiBoMgr.IsFeiSheng = feiShengFlag
	CLuaActivityZhiBoMgr.BottomLevel = level1 or 0
	CLuaActivityZhiBoMgr.TopLevel = level2 or 0
	CLuaActivityZhiBoMgr.BottomVipLvel = vipLevel1 or 0
	CLuaActivityZhiBoMgr.TopVipLvel = vipLevel2 or 0
	CLuaActivityZhiBoMgr.ExtraInfo = extraInfoTbl

	CLuaActivityZhiBoMgr:OnUpdateLevel()
end

function CLuaActivityZhiBoMgr.ShouldOpenUrl(option)
	-- option 取值 0 原有行为 1 内置直播 2 桌面版网页，移动端内置 3 桌面版网页，官方移动端内置，非官方移动端网页 4 全部网页
	local bOpenUrl = false
	if option then
		if CLoginMgr.Inst:IsNetEaseOfficialLogin() then
			bOpenUrl = (option>=2 and option<=3 and not CommonDefs.IsInMobileDevice()) or (option == 4)
		else
			bOpenUrl = (option==2 and not CommonDefs.IsInMobileDevice()) or option == 3 or option == 4
		end
	end
	return bOpenUrl
end

function CLuaActivityZhiBoMgr.OnZhiboButtonClick()
	if not CLuaActivityZhiBoMgr.ZhiBoIsOpen then return end

	local option = CLuaActivityZhiBoMgr.ExtraInfo and CLuaActivityZhiBoMgr.ExtraInfo[1] and tonumber(CLuaActivityZhiBoMgr.ExtraInfo[1]) or 0
	
	local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object)) --CommonDefs.DictCreate(typeof(cs_string), typeof(Object))
	CommonDefs.DictAdd_LuaCall(dict,"ccidflag", tostring(CLuaActivityZhiBoMgr.CCIDFlag))
	CommonDefs.DictAdd_LuaCall(dict,"ccid", tostring(CLuaActivityZhiBoMgr.ZhuBoCcid))
	CommonDefs.DictAdd_LuaCall(dict,"jumptocclive", tostring(CLuaActivityZhiBoMgr.JumpToCCLive))
	CommonDefs.DictAdd_LuaCall(dict,"url", tostring(CLuaActivityZhiBoMgr.ZhiBoURL))
	CommonDefs.DictAdd_LuaCall(dict,"option", tostring(option))
	Gac2Gas.RequestRecordClientLog("ZhiBoLog", MsgPackImpl.pack(dict))

	local cid = 0
	local ccid = 0
	if CLuaActivityZhiBoMgr.CCIDFlag == 1 then
		cid = CLuaActivityZhiBoMgr.ZhuBoCcid
	else
		ccid = CLuaActivityZhiBoMgr.ZhuBoCcid
	end

	local bOpenUrl = CLuaActivityZhiBoMgr.ShouldOpenUrl(option)
	if bOpenUrl then
		CWebBrowserMgr.Inst:OpenUrl(CLuaActivityZhiBoMgr.ZhiBoURL)
		CCCChatMgr.Inst:AddWinWebCCLog(cid, ccid, "from_activity")
		return
	end
	--未引入option处理前的逻辑
	if CLuaActivityZhiBoMgr.JumpToCCLive then
		if not CCPlayerCtrl.IsSupportted() then
			g_MessageMgr:ShowMessage("ZHIBO_SDK_OLD")
			CWebBrowserMgr.Inst:OpenUrl(CLuaActivityZhiBoMgr.ZhiBoURL)
		else
			CLuaActivityZhiBoMgr.JoinCCChannel() 
		end
	else
		CWebBrowserMgr.Inst:OpenUrl(CLuaActivityZhiBoMgr.ZhiBoURL)
		CCCChatMgr.Inst:AddWinWebCCLog(cid, ccid, "from_activity")
	end
end

function CLuaActivityZhiBoMgr.JoinCCChannel()
	local level
	--按当前等级计算
	if CLuaActivityZhiBoMgr.IsFeisheng == 0 then
		level = CClientMainPlayer.Inst.Level
	--按仙凡最高等级计算
	else
		level = CClientMainPlayer.Inst.MaxLevel
	end
	local vipLevel = CClientMainPlayer.Inst.ItemProp.Vip.Level

	if level >= CLuaActivityZhiBoMgr.BottomLevel and level <= CLuaActivityZhiBoMgr.TopLevel then
		if vipLevel >= CLuaActivityZhiBoMgr.BottomVipLvel and vipLevel <= CLuaActivityZhiBoMgr.TopVipLvel then
			if CLuaActivityZhiBoMgr.CCIDFlag == 0 then
				CCCChatMgr.Inst:JoinCCChannnelViaLink(CLuaActivityZhiBoMgr.ZhuBoName, CLuaActivityZhiBoMgr.ZhuBoCcid, CJoinCCChanelContext.Video, CJoinCCChannelRequestSourceInfo.ZhiBoButton)
			else
				CCCChatMgr.Inst:JoinCCChannelByCId(CLuaActivityZhiBoMgr.ZhuBoCcid, CJoinCCChanelContext.Video, CJoinCCChannelRequestSourceInfo.ZhiBoButton)
			end
		end
	end	
end

-- idFlag: 0的情况,ccid代表主播id;1的情况,ccid代表房间号
function Gas2Gac.SyncZhiBoStatus(bOpen, url, bJump, zhuboName, ccid, showTxt, idFlag, levelDataUd, extraDataUd)
	local levelData = MsgPackImpl.unpack(levelDataUd)
	-- extraDataUd 是预留参数,目前客户端不用解析这个数据,预留给运营的后续需求,每次改RPC略坑
	local extraData = MsgPackImpl.unpack(extraDataUd)
	local extraDataList = extraData and TypeAs(extraData, typeof(MakeGenericClass(List, Object))) or nil
	local extraInfoTbl = {}
	if extraDataList then
		for i=0, extraDataList.Count-1 do
			table.insert(extraInfoTbl, extraDataList[i])
		end
	end
	--利用extraDataUd的第一个参数来控制JoinCC的方式，具体见调用地方
	

	if not levelData then return end
	local feiShengFlag 	= levelData[0] -- 判断是否按照飞升等级计算（0:按当前等级计算;1:按仙凡最高等级计算）
	local level1 		= levelData[1] -- 最低等级,等级区间下限
	local level2 		= levelData[2] -- 最高等级,等级区间上限
	local vipLevel1 	= levelData[3] -- 最低VIP等级
	local vipLevel2 	= levelData[4] -- 最高VIP等级

	-- print(feiShengFlag, level1, level2, vipLevel1, vipLevel2)
	CLuaActivityZhiBoMgr.UpdateZhiBoInfo(bOpen, url, bJump, zhuboName, ccid, CChatLinkMgr.TranslateToNGUIText(showTxt,false), idFlag, feiShengFlag, level1, level2, vipLevel1, vipLevel2, extraInfoTbl)
end

function Gas2Gac.CloseCCHighlightEntrance()
	CLuaActivityZhiBoMgr.UpdateZhiBoInfo(false, "", false, false, false, false, 0, 0, 0, 0, 0, 0, nil)
end

function Gas2Gac.OpenCCHighlightEntrance(data_U)
	local BindId, Type, Extra

	local data = MsgPackImpl.unpack(data_U)
	BindId = CommonDefs.DictGetValue(data, typeof(String), "BindId")
	Type = CommonDefs.DictGetValue(data, typeof(String), "Type")
	Extra = CommonDefs.DictGetValue(data, typeof(String), "Extra")

	local extraList = {}
	if Extra then
		for i = 0, Extra.Count - 1 do
			table.insert(extraList, Extra[i])
		end
	end

	local url, bJump, zhuboName, showTxt, feiShengFlag, level1, level2, vipLevel1, vipLevel2, extraInfo = unpack(extraList)
	local extraInfoTbl
	if showTxt then
		showTxt = CChatLinkMgr.TranslateToNGUIText(showTxt, false)
	end
	if extraInfo then
		extraInfoTbl = {}
		for i = 0, extraInfo.Count - 1 do
			table.insert(extraInfoTbl, extraInfo[i])
		end
	end

	CLuaActivityZhiBoMgr.UpdateZhiBoInfo(true, url, bJump, zhuboName, BindId, showTxt, Type, feiShengFlag, level1, level2, vipLevel1, vipLevel2, extraInfoTbl)
end

function Gas2Gac.VipLevelUp(newVipLevel, oldVipLevel)
	CClientMainPlayer.Inst.ItemProp.Vip.Level = newVipLevel
	LuaSEASdkMgr:AppsNotifyVipLevel(newVipLevel)
	CLuaActivityZhiBoMgr:OnUpdateLevel()
end
