local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CItemMgr = import "L10.Game.CItemMgr"
local CUITexture = import "L10.UI.CUITexture"
local UICamera = import "UICamera"

LuaGuoQing2023JingYuanEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
-- 图文规则
RegistChildComponent(LuaGuoQing2023JingYuanEnterWnd, "ImageRule", CUITexture)
RegistChildComponent(LuaGuoQing2023JingYuanEnterWnd, "RuleLabel", UILabel)
RegistChildComponent(LuaGuoQing2023JingYuanEnterWnd, "IndicatorGrid", UIGrid)
RegistChildComponent(LuaGuoQing2023JingYuanEnterWnd, "IndicatorTemplate", GameObject)
RegistChildComponent(LuaGuoQing2023JingYuanEnterWnd, "NextArrow", GameObject)
RegistChildComponent(LuaGuoQing2023JingYuanEnterWnd, "LastArrow", GameObject)
-- 天赋
RegistChildComponent(LuaGuoQing2023JingYuanEnterWnd, "TalentGrid", UIGrid)
RegistChildComponent(LuaGuoQing2023JingYuanEnterWnd, "TalentTemplate", GameObject)
-- 活动信息
RegistChildComponent(LuaGuoQing2023JingYuanEnterWnd, "LvLabel", UILabel)
RegistChildComponent(LuaGuoQing2023JingYuanEnterWnd, "ParticipationLabel", UILabel)
RegistChildComponent(LuaGuoQing2023JingYuanEnterWnd, "TimeLabel", UILabel)
RegistChildComponent(LuaGuoQing2023JingYuanEnterWnd, "DescLabel", UILabel)
RegistChildComponent(LuaGuoQing2023JingYuanEnterWnd, "ItemCell1", CQnReturnAwardTemplate)
RegistChildComponent(LuaGuoQing2023JingYuanEnterWnd, "ItemCell2", CQnReturnAwardTemplate)
RegistChildComponent(LuaGuoQing2023JingYuanEnterWnd, "JingShi", GameObject)
RegistChildComponent(LuaGuoQing2023JingYuanEnterWnd, "RuleBtn", GameObject)
RegistChildComponent(LuaGuoQing2023JingYuanEnterWnd, "MatchBtn", GameObject)
RegistChildComponent(LuaGuoQing2023JingYuanEnterWnd, "CancelBtn", GameObject)
RegistChildComponent(LuaGuoQing2023JingYuanEnterWnd, "BuffInfoPopTip", UISprite)

RegistClassMember(LuaGuoQing2023JingYuanEnterWnd, "m_RulePageIndex")
RegistClassMember(LuaGuoQing2023JingYuanEnterWnd, "m_RuleInfoTbl")
RegistClassMember(LuaGuoQing2023JingYuanEnterWnd, "m_IndicatorTbl")

--@endregion RegistChildComponent end

function LuaGuoQing2023JingYuanEnterWnd:Awake()
    UIEventListener.Get(self.RuleBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRuleBtnClick()
    end)
    UIEventListener.Get(self.MatchBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnMatchBtnClick()
    end)
    UIEventListener.Get(self.CancelBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCancelBtnClick()
    end)
    UIEventListener.Get(self.NextArrow).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRuleArrowClick(1)
    end)
    UIEventListener.Get(self.LastArrow).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRuleArrowClick(-1)
    end)
end

function LuaGuoQing2023JingYuanEnterWnd:Update()
    if Input.GetMouseButtonDown(0) then
        local corners = self.BuffInfoPopTip.worldCorners
        local lastPos = UICamera.lastWorldPosition;
        if lastPos.x < corners[0].x or lastPos.x > corners[2].x or lastPos.y < corners[0].y or lastPos.y > corners[2].y then
            self.BuffInfoPopTip.gameObject:SetActive(false)
        end
    end
end

function LuaGuoQing2023JingYuanEnterWnd:Init()
    self.MatchBtn:SetActive(true)
    self.CancelBtn:SetActive(false)
    self.TalentTemplate:SetActive(false)
    self.BuffInfoPopTip.gameObject:SetActive(false)
    self:InitGameInfo()
    if CClientMainPlayer.Inst then
        Gac2Gas.GlobalMatch_RequestCheckSignUp(self.m_GamePlayId, CClientMainPlayer.Inst.Id)
    end
    Gac2Gas.QueryJingYuanPlayInfo()
end

function LuaGuoQing2023JingYuanEnterWnd:InitTalentGrid(talentLevel)
    local hasPreTalent = true
	Extensions.RemoveAllChildren(self.TalentGrid.transform)
    for i = 1, 7 do
		local talent = NGUITools.AddChild(self.TalentGrid.gameObject, self.TalentTemplate)
        talent.gameObject:SetActive(true)
        hasPreTalent = self:InitOneTalent(talent, i, talentLevel, hasPreTalent)
    end
    self.TalentGrid:Reposition()
end

function LuaGuoQing2023JingYuanEnterWnd:InitOneTalent(talent, id, talentLevel, hasPreTalent)
    local talentData = GuoQing2023_Talent.GetData(id)
    local buffData = Buff_Buff.GetData(talentData.Buff * 100 + 1)
    if buffData == nil then return hasPreTalent end
    talent.transform:GetComponent(typeof(CUITexture)):LoadMaterial(buffData.Icon)
    talent.transform:Find("Mask").gameObject:SetActive(talentLevel < talentData.Lv)
    local descLabel = talent.transform:Find("Mask/Label"):GetComponent(typeof(UILabel))
    if talentLevel < talentData.Lv then
        local desStr = hasPreTalent and LocalString.GetString("再战%d场") or "%d"
        descLabel.text = SafeStringFormat3(desStr, talentData.Lv - talentLevel)
        hasPreTalent = false
    end
    UIEventListener.Get(talent.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:ShowBuffInfoPopTip(id, talentData.Name, talentData.Desc)
    end)
    return hasPreTalent
end

function LuaGuoQing2023JingYuanEnterWnd:ShowBuffInfoPopTip(id, name, desc)
    local pos_x = self.TalentGrid.transform:GetChild(id-1).transform.position.x
    self.BuffInfoPopTip.transform.position = Vector3(pos_x, self.BuffInfoPopTip.transform.position.y, 0)
    local nameLabel = self.BuffInfoPopTip.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local descLabel = self.BuffInfoPopTip.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
    nameLabel.text = name
    descLabel.text = desc
    nameLabel:ResetAndUpdateAnchors()
    self.BuffInfoPopTip.height = descLabel.height + 106 - 32
    self.BuffInfoPopTip.gameObject:SetActive(true)
end

function LuaGuoQing2023JingYuanEnterWnd:InitGameInfo()
    local data = GuoQing2023_JingYuanPlay.GetData()
    self.m_GamePlayId = data.GamePlayId
    -- 图文规则
    self.m_RuleInfoTbl = {}
    for i = 0, data.RuleImagePathList.Length-1 do
        table.insert(self.m_RuleInfoTbl, {image = data.RuleImagePathList[i], text = data.RuleStringList[i]})
    end
    self:InitImageRule()
    -- 描述部分
    self.LvLabel.text = SafeStringFormat3(LocalString.GetString("%d级"), data.MinGrade)
    self.ParticipationLabel.text = data.PlayTaskForm
    self.TimeLabel.text = data.PlayTimeString
    self.DescLabel.text = g_MessageMgr:FormatMessage("GuoQing2023_JingYuanPlay_EnterWnd_Desc")
    -- 奖励部分
    self.ItemCell1:Init(CItemMgr.Inst:GetItemTemplate(data.DailyRewardId), 1)
    self.ItemCell1.transform:Find("Mask").gameObject:SetActive(false)
    self.ItemCell2:Init(CItemMgr.Inst:GetItemTemplate(data.CompetitiveRewardId), 1)
    self.ItemCell2.transform:Find("Mask").gameObject:SetActive(false)
    -- 晶元
    self:InitJingShi(0, 0)
end

function LuaGuoQing2023JingYuanEnterWnd:InitJingShi(hasPieceCount, maxPieceCount)
    local img = {
        "UI/Texture/FestivalActivity/Festival_GuoQing/GuoQing2023/Material/guoqing2023jingyuanenterview_jingyuan__normal.mat",
        "UI/Texture/FestivalActivity/Festival_GuoQing/GuoQing2023/Material/guoqing2023jingyuanenterview_jingyuan_highlight.mat"
    }
    local unlock = maxPieceCount / 3
    for i = 0, self.JingShi.transform.childCount - 1 do
        local child = self.JingShi.transform:GetChild(i)
        local piece = hasPieceCount - i * 3
        piece = piece > 3 and 3 or piece < 0 and 0 or piece
        child.transform:Find("1"):GetComponent(typeof(CUITexture)):LoadMaterial(img[piece > 0 and 2 or 1])
        child.transform:Find("2"):GetComponent(typeof(CUITexture)):LoadMaterial(img[piece > 1 and 2 or 1])
        child.transform:Find("3"):GetComponent(typeof(CUITexture)):LoadMaterial(img[piece > 2 and 2 or 1])
        child.transform:Find("lock").gameObject:SetActive(i >= unlock)
    end
end

function LuaGuoQing2023JingYuanEnterWnd:InitImageRule()
    self.IndicatorTemplate:SetActive(false)
    self.m_IndicatorTbl = {}
	Extensions.RemoveAllChildren(self.IndicatorGrid.transform)
    for i = 1, #self.m_RuleInfoTbl do
        local go = NGUITools.AddChild(self.IndicatorGrid.gameObject, self.IndicatorTemplate)
        go:SetActive(true)
        local normal = go.transform:Find("Normal").gameObject
        local highLight = go.transform:Find("Highlight").gameObject
        table.insert(self.m_IndicatorTbl, {normal = normal, highLight = highLight})
    end
    self.IndicatorGrid:Reposition()
    self.m_RulePageIndex = 1
    self:UpdateImageRule()
end

function LuaGuoQing2023JingYuanEnterWnd:UpdateImageRule()
    self.ImageRule:LoadMaterial(self.m_RuleInfoTbl[self.m_RulePageIndex].image)
    self.RuleLabel.text = self.m_RuleInfoTbl[self.m_RulePageIndex].text
    for i, indicater in ipairs(self.m_IndicatorTbl) do
        indicater.normal:SetActive(i ~= self.m_RulePageIndex)
        indicater.highLight:SetActive(i == self.m_RulePageIndex)
    end
end

function LuaGuoQing2023JingYuanEnterWnd:OnQueryJingYuanPlayInfoResult(talentLevel, engageTimes, hasPieceCount, maxPieceCount)
    self:InitTalentGrid(talentLevel)
    self:InitJingShi(hasPieceCount, maxPieceCount)
    if engageTimes > 0 then self.ItemCell1.transform:Find("Mask").gameObject:SetActive(true) end
end

function LuaGuoQing2023JingYuanEnterWnd:UpdateMatchButton(isMatching)
    -- 改变下方报名按钮状态
    self.MatchBtn:SetActive(not isMatching)
    self.CancelBtn:SetActive(isMatching)
end

function LuaGuoQing2023JingYuanEnterWnd:OnGlobalMatch_CheckInMatchingResult(playerId, playId, isInMatching, resultStr)
    if playId == self.m_GamePlayId then
        self:UpdateMatchButton(isInMatching)
    end
end

function LuaGuoQing2023JingYuanEnterWnd:OnGlobalMatch_SignUpPlayResult(playId, success)
    if playId == self.m_GamePlayId and success then
        self:UpdateMatchButton(true)
    end
end

function LuaGuoQing2023JingYuanEnterWnd:OnGlobalMatch_CancelSignUpResult(playId, success)
    if playId == self.m_GamePlayId and success then
        self:UpdateMatchButton(false)
    end
end

--@region UIEvent
function LuaGuoQing2023JingYuanEnterWnd:OnRuleBtnClick()
    g_MessageMgr:ShowMessage("GuoQing2023_JingYuanPlay_EnterWnd_RuleTip")
end

function LuaGuoQing2023JingYuanEnterWnd:OnMatchBtnClick()
    Gac2Gas.GlobalMatch_RequestSignUp(self.m_GamePlayId)
end

function LuaGuoQing2023JingYuanEnterWnd:OnCancelBtnClick()
	Gac2Gas.GlobalMatch_RequestCancelSignUp(self.m_GamePlayId)
end

function LuaGuoQing2023JingYuanEnterWnd:OnRuleArrowClick(delta)
    self.m_RulePageIndex = self.m_RulePageIndex + delta
    if self.m_RulePageIndex < 1 then self.m_RulePageIndex = #self.m_RuleInfoTbl end
    if self.m_RulePageIndex > #self.m_RuleInfoTbl then self.m_RulePageIndex = 1 end
    self:UpdateImageRule()
end

--@endregion UIEvent

function LuaGuoQing2023JingYuanEnterWnd:OnEnable()
    self:Init()
    g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
    g_ScriptEvent:AddListener("QueryJingYuanPlayInfoResult", self, "OnQueryJingYuanPlayInfoResult")
end

function LuaGuoQing2023JingYuanEnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
    g_ScriptEvent:RemoveListener("QueryJingYuanPlayInfoResult", self, "OnQueryJingYuanPlayInfoResult")
end
