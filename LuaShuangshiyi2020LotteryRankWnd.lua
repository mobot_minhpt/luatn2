local Profession=import "L10.Game.Profession"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local UILabel=import "UILabel"
local QnTableView=import "L10.UI.QnTableView"


CLuaShuangshiyi2020LotteryRankWnd=class()
-- 主玩家相关信息
RegistClassMember(CLuaShuangshiyi2020LotteryRankWnd,"m_MyRewardLabel")
RegistClassMember(CLuaShuangshiyi2020LotteryRankWnd,"m_MyNameLabel")
RegistClassMember(CLuaShuangshiyi2020LotteryRankWnd,"m_MyserverLabel")
RegistClassMember(CLuaShuangshiyi2020LotteryRankWnd,"m_MyClsSprite")
RegistClassMember(CLuaShuangshiyi2020LotteryRankWnd,"m_TitleLabel")
RegistClassMember(CLuaShuangshiyi2020LotteryRankWnd,"m_TableViewDataSource")
RegistClassMember(CLuaShuangshiyi2020LotteryRankWnd,"m_TableView")
RegistClassMember(CLuaShuangshiyi2020LotteryRankWnd,"m_RankList")

function CLuaShuangshiyi2020LotteryRankWnd:Init()
    local myNode = self.transform:Find("MainPlayerInfo")
    self.m_MyRewardLabel = myNode:Find("RewardLabel"):GetComponent(typeof(UILabel))
    self.m_MyNameLabel = myNode:Find("NameLabel"):GetComponent(typeof(UILabel))
    self.m_MyServerLabel = myNode:Find("ServerLabel"):GetComponent(typeof(UILabel))
    self.m_MyClsSprite = myNode:Find("ClsSprite"):GetComponent(typeof(UISprite))

    self.m_TableView = self.transform:Find("TableView"):GetComponent(typeof(QnTableView))
    local function initItem(item,index)
        if index % 2 == 1 then
            item:SetBackgroundTexture("common_bg_mission_background_n")
        else
            item:SetBackgroundTexture("common_bg_mission_background_s")
        end
        self:InitItem(item,index)
    end

    self.m_TableViewDataSource=DefaultTableViewDataSource.CreateByCount(0,initItem)
    self.m_TableView.m_DataSource=self.m_TableViewDataSource
    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)

    self:InitMyInfo()
end

function CLuaShuangshiyi2020LotteryRankWnd:OnSelectAtRow(row)
    local data=CLuaShuangshiyi2020Mgr.m_LotteryList[row+1]
    if data then
        CPlayerInfoMgr.ShowPlayerPopupMenu(data["id"], EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
    end
end

function CLuaShuangshiyi2020LotteryRankWnd:InitItem(item,index)
    local tf = item.transform
    local nameLabel = FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel))
    local rewardLabel = FindChild(tf,"RewardLabel"):GetComponent(typeof(UILabel))
    local serverLabel = FindChild(tf,"ServerLabel"):GetComponent(typeof(UILabel))
    
    local info = CLuaShuangshiyi2020Mgr.m_LotteryList[index+1]
    local clsSprite = tf:Find("ClsSprite"):GetComponent(typeof(UISprite))
    clsSprite.spriteName = Profession.GetIconByNumber(info["class"]) --EnumToInt(info[]))

    nameLabel.text = info["name"]
    serverLabel.text = tostring(info["serverName"])
    rewardLabel.text = tostring(info["reward"])
end

function CLuaShuangshiyi2020LotteryRankWnd:InitMyInfo()
    if CLuaShuangshiyi2020Mgr.m_MyLotteryInfo ~= nil then
        self.m_MyRewardLabel.text=tostring(CLuaShuangshiyi2020Mgr.m_MyLotteryInfo["reward"])
    else
        self.m_MyRewardLabel.text=LocalString.GetString("未上榜")
    end

    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text= CClientMainPlayer.Inst.Name
        self.m_MyClsSprite.spriteName = Profession.GetIconByNumber(EnumToInt(CClientMainPlayer.Inst.Class))
        self.m_MyServerLabel.text = CClientMainPlayer.Inst:GetMyServerName()
    end
    
    self.m_TableViewDataSource.count= #CLuaShuangshiyi2020Mgr.m_LotteryList
    self.m_TableView:ReloadData(true,false)
end
