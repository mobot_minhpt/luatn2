local CZhuJueJuQingMgr = import "L10.Game.CZhuJueJuQingMgr"

LuaBigJuqingPicWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaBigJuqingPicWnd, "MainTexture", "MainTexture", CUITexture)
RegistChildComponent(LuaBigJuqingPicWnd, "CloseButton", "CloseButton", GameObject)

--@endregion RegistChildComponent end

function LuaBigJuqingPicWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)

    --@endregion EventBind end
end

function LuaBigJuqingPicWnd:Init()
	self.MainTexture:LoadMaterial(CZhuJueJuQingMgr.Instance.PicTexturePath)
	if CZhuJueJuQingMgr.Instance.PicDuduration > 0 then
		CUICommonDef.DelayDoSth(CZhuJueJuQingMgr.Instance.PicDuduration*1000,DelegateFactory.Action(function()
			CUIManager.CloseUI(CLuaUIResources.BigJuqingPicWnd)
		end))
	end
end

--@region UIEvent

function LuaBigJuqingPicWnd:OnCloseButtonClick()
	Gac2Gas.PlayBigPictureEnd()
    CUIManager.CloseUI(CLuaUIResources.BigJuqingPicWnd)
end

--@endregion UIEvent

