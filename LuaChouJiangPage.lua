local CommonDefs        = import "L10.Game.CommonDefs"
local DelegateFactory   = import "DelegateFactory"
local GameObject        = import "UnityEngine.GameObject"
local UITabBar          = import "L10.UI.UITabBar"
local UILabel           = import "UILabel"
local LuaGameObject     = import "LuaGameObject"
local CServerTimeMgr    = import "L10.Game.CServerTimeMgr"
local ChatPlayerLink    = import "CChatLinkMgr.ChatPlayerLink"
local CChatLinkMgr      = import "CChatLinkMgr"
local UICamera          = import "UICamera"
local CItemInfoMgr		= import "L10.UI.CItemInfoMgr"
local AlignType			= import "L10.UI.CItemInfoMgr+AlignType"

LuaChouJiangPage = class()

RegistChildComponent(LuaChouJiangPage, "LeftArraw",     "LeftArraw",    GameObject)
RegistChildComponent(LuaChouJiangPage, "RightArraw",    "RightArraw",   GameObject)
RegistChildComponent(LuaChouJiangPage, "Bottom",        "Bottom",       UITabBar)
RegistChildComponent(LuaChouJiangPage, "RuleLabel",     "RuleLabel",    UILabel)
RegistChildComponent(LuaChouJiangPage, "DayLabel",      "DayLabel",     UILabel)
RegistChildComponent(LuaChouJiangPage, "TimeLabel",     "TimeLabel",    UILabel)
RegistChildComponent(LuaChouJiangPage, "ItemCell",      "ItemCell",     GameObject)
RegistChildComponent(LuaChouJiangPage, "State1",        "State1",       GameObject)
RegistChildComponent(LuaChouJiangPage, "State2",        "State2",       GameObject)
RegistChildComponent(LuaChouJiangPage, "TodaySprite",   "TodaySprite",  GameObject)
RegistChildComponent(LuaChouJiangPage, "SomedaySprite", "SomedaySprite",GameObject)
RegistChildComponent(LuaChouJiangPage, "CountLabel",    "CountLabel",   UILabel)
RegistChildComponent(LuaChouJiangPage, "LevelLabel",    "LevelLabel",   UILabel)
RegistChildComponent(LuaChouJiangPage, "PlayerNameLabel",    "PlayerNameLabel",   UILabel)

RegistClassMember(LuaChouJiangPage, "m_selectday")
RegistClassMember(LuaChouJiangPage, "m_lastday")
RegistClassMember(LuaChouJiangPage, "m_tick")

function LuaChouJiangPage:Awake()
	UIEventListener.Get(self.LeftArraw.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeftArrawClick()
	end)

	UIEventListener.Get(self.RightArraw.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightArrawClick()
	end)

    self.Bottom.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnBottomTabChange(index)
    end)

    UIEventListener.Get(self.PlayerNameLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnNameClicked(go)
    end)
end

function LuaChouJiangPage:OnEnable()
    self.m_selectday = 0
    self.m_lastday = 0
    g_ScriptEvent:AddListener("OnChouJiangDataChange",self,"OnDataChange")
end

function LuaChouJiangPage:OnDisable()
    if self.m_tick then
        UnRegisterTick(self.m_tick)
        self.m_tick = nil
    end
    g_ScriptEvent:RemoveListener("OnChouJiangDataChange",self,"OnDataChange")
end

function LuaChouJiangPage:OnDataChange()
    self:RefreshAll()
end

function LuaChouJiangPage:Init()
    CLuaScheduleMgr.QueryXinFuLotteryResult()
end

function LuaChouJiangPage:RefreshAll()
    self.RuleLabel.text = g_MessageMgr:FormatMessage("ChouJiang_Rule")
    local curday= CLuaScheduleMgr.ChouJiangData.Day
    if curday == nil or curday <= 0 then
        return
    else
        self:RefreshBottom()
        self.Bottom:ChangeTab(curday-1,false)
    end
end

function LuaChouJiangPage:RefreshBottom()
    local curday= CLuaScheduleMgr.ChouJiangData.Day
    
    if self.m_lastday > 0 then
        local tab = self.Bottom:GetTabGoByIndex(self.m_lastday-1)
        if tab then 
            LuaGameObject.GetChildNoGC(tab.transform,"itemtoday").gameObject:SetActive(false)
        end
    end

    local tabctrl = self.Bottom:GetTabGoByIndex(curday-1)
    if tabctrl then
        LuaGameObject.GetChildNoGC(tabctrl.transform,"itemtoday").gameObject:SetActive(true)
        self.m_lastday = curday
    end
end

function LuaChouJiangPage:RefreshMain(day)
    self.TodaySprite:SetActive(day == CLuaScheduleMgr.ChouJiangData.Day)
    self.SomedaySprite:SetActive(day > CLuaScheduleMgr.ChouJiangData.Day)

    local daystr = SafeStringFormat3(LocalString.GetString("第%d天"),day)
    if day == CLuaScheduleMgr.ChouJiangData.Day then
        daystr = daystr..LocalString.GetString("·今天")
    end
    self.DayLabel.text = daystr
    
    local data = XinFu_Lottery.GetData(day)
    self.CountLabel.text = SafeStringFormat3(LocalString.GetString("全服抽%d位"),data.count)
    self.LevelLabel.text = SafeStringFormat3(LocalString.GetString("%d级"),data.level)

    self:InitRewardItem(day)

    self:InitRewardPlayers(day)

    local reward = CLuaScheduleMgr.ChouJiangData.Reward
    if day == CLuaScheduleMgr.ChouJiangData.Day then
        if reward[day] == nil then --今天还没到抽奖时间
            local timestr = XinFu_Setting.GetData().LotteryCron
            local op = System.StringSplitOptions.RemoveEmptyEntries
            local splits = Table2ArrayWithCount({" "}, 1, MakeArrayClass(System.String))
            local strs = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(timestr, splits, op)

            local minutes = tonumber(strs[0])
            local hours = tonumber(strs[1])

            local tartime = CServerTimeMgr.Inst:GetTodayTimeStampByTime(hours,minutes,0)
            self.TimeLabel.gameObject:SetActive(true)
            if not self.m_tick then
                self.m_tick = RegisterTick(function()
                    local intervalSeconds = tartime - CServerTimeMgr.Inst.timeStamp
                    intervalSeconds=math.max(0,intervalSeconds)

                    local hours = math.floor(intervalSeconds / 3600)
                    local minutes = math.floor((intervalSeconds - hours * 3600) / 60)
                    local seconds = intervalSeconds - hours * 3600 - minutes * 60
                    self.TimeLabel.text = SafeStringFormat3("%02d:%02d:%02d",hours,minutes,seconds)

                    if intervalSeconds == 0 then 
                        UnRegisterTick(self.m_tick)
                        self.m_tick=nil
                        CLuaScheduleMgr.QueryXinFuLotteryResult()
                    end
                end,1000)    
            end
            self.State1:SetActive(true)
            self.State2:SetActive(false)
        else--今天已经抽奖
            self.TimeLabel.gameObject:SetActive(false)
            self.State1:SetActive(false)
            self.State2:SetActive(true)
        end
    elseif day > CLuaScheduleMgr.ChouJiangData.Day then
        self.State1:SetActive(true)
        self.State2:SetActive(false)
    else 
        self.State1:SetActive(false)
        self.State2:SetActive(true)
    end
end

function LuaChouJiangPage:InitRewardItem(day)
    local rewarditem = XinFu_Lottery.GetData(day)
    local itemLabel = LuaGameObject.GetChildNoGC(self.ItemCell.transform,"NameLabel").label
    itemLabel.text = rewarditem.itemName

    local op = System.StringSplitOptions.RemoveEmptyEntries
    local splits = Table2ArrayWithCount({";",","}, 2, MakeArrayClass(System.String))
    local strs = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(rewarditem.itemInfo, splits, op)
    local len = strs.Length
    local itemid = tonumber(strs[len-3])
    local item = Item_Item.GetData(itemid)
    local iconctrl = LuaGameObject.GetChildNoGC(self.ItemCell.transform,"IconTexture").cTexture
    iconctrl:LoadMaterial(item.Icon)

    UIEventListener.Get(self.ItemCell).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)
end

function LuaChouJiangPage:InitRewardPlayers(day)
    local reward = CLuaScheduleMgr.ChouJiangData.Reward
    if reward[day] == nil then
        return
    else
        local len = #reward[day]
        local str = ""
        for i=1,len do
            local rdata = reward[day][i]
            local pid = rdata.pid
            local pname = rdata.pname

            if pid == CClientMainPlayer.Inst.Id then 
                str = str.."[00ff60]"..pname.."[-] "
            else
                local playerInfo = ChatPlayerLink.GenerateLink(pid, pname).displayTag
                str = str..playerInfo.." "
            end
        end
        self.PlayerNameLabel.text = str
    end
end

function LuaChouJiangPage:OnNameClicked(go)
    
	local NameLabel = go:GetComponent(typeof(UILabel))
    local url = NameLabel:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url then
        CChatLinkMgr.ProcessLinkClick(url, nil)
    end
end

function LuaChouJiangPage:TrunDay(delta)
    local day = self.m_selectday+delta
    if day <= 0 then 
        day = 1
    elseif day > 7 then 
        day = 7
    end
    self.Bottom:ChangeTab(day-1,false)
end

--@region UIEvent

function LuaChouJiangPage:OnLeftArrawClick()
    self:TrunDay(-1)
end

function LuaChouJiangPage:OnRightArrawClick()
    self:TrunDay(1)
end

function LuaChouJiangPage:OnBottomTabChange(selected)
    self.m_selectday = selected+1;
    self:RefreshMain(self.m_selectday)

    self.LeftArraw:SetActive(self.m_selectday > 1)
    self.RightArraw:SetActive(self.m_selectday < 7)
end

--@endregion


