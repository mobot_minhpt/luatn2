local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CShopMallMgr = import "L10.UI.CShopMallMgr"

LuaFashionLotteryMgr = {}

LuaFashionLotteryMgr.m_ActivityAlertEndTime = nil       -- 活动入口倒计时结束时间

LuaFashionLotteryMgr.m_ActivityId = nil                 -- 时装抽奖活动Id
LuaFashionLotteryMgr.m_OpenWnd = nil                    -- 时装抽奖活动界面
LuaFashionLotteryMgr.m_OpenView = nil                   -- 抽奖活动打开对应字界面

-- 抽奖活动
LuaFashionLotteryMgr.m_LotteryTicketId = nil            -- 时装抽奖券templateId
LuaFashionLotteryMgr.m_DiscountLotteryTicketId = nil    -- 特惠抽奖券templateId
LuaFashionLotteryMgr.m_UniTicketId = nil                -- 时装抽奖通用券templateId
LuaFashionLotteryMgr.m_FirstLottery = true              -- 是否为本次登录首次抽奖
LuaFashionLotteryMgr.m_FashionLotteryStartTime = nil    -- 时装抽奖活动开始时间
LuaFashionLotteryMgr.m_FashionLotteryEndTime = nil      -- 时装抽奖活动结束时间
LuaFashionLotteryMgr.m_FashionLotteryIcons = nil        -- 时装抽奖活动主界面图标

-- 折扣礼包
LuaFashionLotteryMgr.m_DiscountPackStartTime = nil      -- 折扣礼包开始时间
LuaFashionLotteryMgr.m_DiscountPackEndTime = nil        -- 折扣礼包结束时间

-- 用于判断是否需要在主界面显示活动入口
function LuaFashionLotteryMgr:ShowActivityAlert()
    return false
end

-- 用于活动入口倒计时
function LuaFashionLotteryMgr:GetRemainTimeStr()
    if self.m_ActivityId == nil then self:InitFashionLotteryInfo() end
    if self.m_ActivityAlertEndTime then
        local showBtn = false
        local iconIndex = nil
        local remainTimeStr = nil
        local remainTime = self.m_ActivityAlertEndTime - CServerTimeMgr.Inst.timeStamp
        if remainTime > 0 then
            local cd_minute = math.floor(remainTime / 60) % 60
            local cd_hour = math.floor(remainTime / 3600) % 24
            local cd_day = math.floor(remainTime / 3600 / 24)
            if cd_day >= 3 then
                showBtn = true
                iconIndex = 0
            elseif cd_day > 0 then
                showBtn = true
                iconIndex = 1
                remainTimeStr = SafeStringFormat3(LocalString.GetString("仅剩%d天"), cd_day + 1)
            else
                showBtn = true
                iconIndex = 1
                remainTimeStr = SafeStringFormat3(LocalString.GetString("仅剩%02d:%02d"), cd_hour, cd_minute)
            end
        end
        return showBtn, self.m_FashionLotteryIcons[iconIndex or 0], remainTimeStr
    end
    return false, nil, nil
end

-- 初始化一些活动信息（并检查是否存在对应活动）
function LuaFashionLotteryMgr:InitFashionLotteryInfo()
    local setData = FashionLottery_Settings.GetData()
    self.m_ActivityId = setData.ActivityId
    self.m_ActivityAlertEndTime = CServerTimeMgr.Inst:GetTimeStampByStr(setData.EndTime)

    local wndData = FashionLottery_MainWndInfo.GetData(self.m_ActivityId)
    if wndData then
        self.m_OpenWnd = {}
        self.m_OpenWnd.MainWnd = wndData.WndName
        self.m_OpenWnd.PrizePoolWnd = wndData.PrizePoolWnd
        self.m_OpenWnd.BuyTicketWnd = wndData.BuyTicketWnd
        self.m_OpenWnd.ExchangeWnd = wndData.ExchangeWnd
        self.m_OpenWnd.TryDressSuitId = wndData.TryDressSuitId
        self.m_FashionLotteryIcons = wndData.Icons
    end
    local activityData = FashionLottery_Activity.GetData(self.m_ActivityId)
    if activityData then
        self.m_LotteryTicketId = activityData.LotteryTicketId
        self.m_DiscountLotteryTicketId = activityData.DiscountLotteryTicketId
        self.m_UniTicketId = activityData.UniTicketId
        self.m_FashionLotteryStartTime = CServerTimeMgr.Inst:GetTimeStampByStr(activityData.StartTime)
        self.m_FashionLotteryEndTime = CServerTimeMgr.Inst:GetTimeStampByStr(activityData.EndTime)
    end
    local discountPackData = FashionLottery_DiscountPack.GetData(self.m_ActivityId)
    if discountPackData then
        self.m_DiscountPackStartTime = CServerTimeMgr.Inst:GetTimeStampByStr(discountPackData.StartTime)
        self.m_DiscountPackEndTime = CServerTimeMgr.Inst:GetTimeStampByStr(discountPackData.EndTime)
    end
end

-- 当前抽奖券总个数
function LuaFashionLotteryMgr:GetLotteryTicketCount()
    local count1 = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_LotteryTicketId)
    local count2 = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_DiscountLotteryTicketId)
    return count1 + count2
end

-- 当前能够买奖券的灵玉个数
function LuaFashionLotteryMgr:GetLotteryTicketJade()
    local jade = CClientMainPlayer.Inst.Jade
    if CShopMallMgr.CanUseBindJadeBuy(self.m_LotteryTicketId) then
        jade = CClientMainPlayer.Inst.Jade + CClientMainPlayer.Inst.BindJade
    end
    return jade
end

-- 打开活动主界面
function LuaFashionLotteryMgr:OpenFashionLotteryWnd(viewName)
    if self.m_ActivityId == nil then self:InitFashionLotteryInfo() end
    if self.m_OpenWnd and self.m_OpenWnd.MainWnd then
        LuaFashionLotteryMgr.m_OpenView = viewName
        CUIManager.ShowUI(self.m_OpenWnd.MainWnd)
    end
end

-- 打开兑换界面
function LuaFashionLotteryMgr:OpenFashionLotteryExchangeWnd()
    if self.m_ActivityId == nil then self:InitFashionLotteryInfo() end
    if self.m_OpenWnd and self.m_OpenWnd.ExchangeWnd then
        CUIManager.ShowUI(self.m_OpenWnd.ExchangeWnd)
    end
end

function LuaFashionLotteryMgr:FashionLotteryQueryTicketPriceResult(count, price, originalPrice, discount)
    if self.m_OpenWnd and self.m_OpenWnd.BuyTicketWnd then
        if self.m_LotteryTicketNeedBuyCount == count and not CUIManager.IsLoaded(self.m_OpenWnd.BuyTicketWnd) then
            -- rpc返回值为所需购买奖券，并且界面未打开，则直接用消息处理
            if self:GetLotteryTicketJade() < price then    
                -- 灵玉不足，直接弹出充值消息
                local msg = g_MessageMgr:FormatMessage("FashionLottery_BuyTicket_NotEnoughJade", tostring(count) , tostring(price))
                g_MessageMgr:ShowOkCancelMessage(msg,function ()
                    CShopMallMgr.ShowChargeWnd()
                end, nil, nil, nil, false)
            else     
                -- 灵玉足够，直接弹出购买消息
                local msg = g_MessageMgr:FormatMessage("FashionLottery_BuyTicket_EnoughJade", tostring(count) , tostring(price))
                g_MessageMgr:ShowOkCancelMessage(msg,function ()
                    Gac2Gas.FashionLotteryBuyTicket(count)
                end, nil, nil, nil, false)
            end
            self.m_LotteryTicketNeedBuyCount = 0
        else
            g_ScriptEvent:BroadcastInLua("FashionLotteryQueryTicketPriceResult", count, price, originalPrice, discount)
        end
    end
end

-- 时装抽奖活动是否开启
function LuaFashionLotteryMgr:IsFashionLotteryOpen()
    return self.m_FashionLotteryStartTime and
            self.m_FashionLotteryStartTime <= CServerTimeMgr.Inst.timeStamp and
            CServerTimeMgr.Inst.timeStamp < self.m_FashionLotteryEndTime
end

-- 折扣礼包是否开启
function LuaFashionLotteryMgr:IsDiscountPackOpen()
    return self.m_FashionLotteryStartTime and 
            self.m_DiscountPackStartTime <= CServerTimeMgr.Inst.timeStamp and 
            CServerTimeMgr.Inst.timeStamp < self.m_DiscountPackEndTime
end

-- 抽奖保底
LuaFashionLotteryMgr.m_LotteryEnsurance = nil
function LuaFashionLotteryMgr:CheckFashionLotteryEnsuranceInfo(left)
    self.m_LotteryEnsurance = left
    g_ScriptEvent:BroadcastInLua("CheckFashionLotteryEnsuranceInfo")
end

-- 抽奖
LuaFashionLotteryMgr.m_LotteryTicketNeedBuyCount = nil
function LuaFashionLotteryMgr:FashionLotteryRequestDraw(drawCount)
    if self.m_LotteryTicketId == nil then return end
    local curCount = self:GetLotteryTicketCount()
    if curCount >= drawCount then
        Gac2Gas.FashionLotteryRequestDraw(drawCount)
    else
        -- 不够则打开购买界面
        local count = drawCount - curCount
        self.m_LotteryTicketNeedBuyCount = count
        Gac2Gas.FashionLotteryQueryTicketPrice(count)   -- 查当前灵玉是否能够购买剩余奖券
    end
end

-- 抽奖结果
LuaFashionLotteryMgr.m_LotteryResult = nil
LuaFashionLotteryMgr.m_FashionRewardList = nil
function LuaFashionLotteryMgr:FashionLotteryDrawResult(rewardItemsTblU)
    if self.m_ItemPreciousTbl == nil then self:InitItemPreciousTbl() end
    self.m_LotteryResult = g_MessagePack.unpack(rewardItemsTblU)
    self.m_FashionRewardList = {}
    for i = 1, #self.m_LotteryResult do
        local rewardInfo = self.m_LotteryResult[i]
        rewardInfo.Precious = self.m_ItemPreciousTbl[rewardInfo[1]]
        local disassembleData = FashionLottery_Disassemble.GetData(rewardInfo[1])
        if disassembleData and disassembleData.Color > 0 then
            rewardInfo.disassembleData = disassembleData
            table.insert(self.m_FashionRewardList, {index = i, color = disassembleData.Color})
        end
    end
    table.sort(self.m_FashionRewardList, function (a, b)
        return a.color > b.color
    end)
    g_ScriptEvent:BroadcastInLua("FashionLotteryDrawResult")
end

-- 判断是否为时装抽奖获得礼盒
LuaFashionLotteryMgr.m_DisassembleOpen = true   -- 时装拆解功能开启
LuaFashionLotteryMgr.m_DisassembleList = nil    -- 时装拆解列表
function LuaFashionLotteryMgr:InitDisassembleList()
    self.m_DisassembleList = {}
    FashionLottery_Activity.Foreach(function(k, v)
        self.m_DisassembleList[v.UniTicketId] = {type = 1, prize = v.UniTicket2Points}       -- 万能券
    end)
    FashionLottery_Disassemble.Foreach(function(k, v)
        if v.Color > 0 then
            self.m_DisassembleList[k] = {type = 2}               -- 时装礼盒
        else
            self.m_DisassembleList[k] = {type = 3}               -- 其他（队标）
        end
    end)
end

LuaFashionLotteryMgr.m_ItemPreciousTbl = nil
function LuaFashionLotteryMgr:InitItemPreciousTbl()
    if self.m_ItemPreciousTbl == nil then
        self.m_ItemPreciousTbl = {}
        FashionLottery_Items.Foreach(function (id, data)
            self.m_ItemPreciousTbl[data.ItemId] = data.Precious
        end)
    end
end

function LuaFashionLotteryMgr:IsFashionLotteryDisassemble(templateId, isBinded)
    if self.m_DisassembleList == nil then self:InitDisassembleList() end
    local disassembleInfo = self.m_DisassembleList[templateId]
    if disassembleInfo then
        if disassembleInfo.type == 1 or disassembleInfo.type == 3 then return true end
        if disassembleInfo.type == 2 and not isBinded then return true end
    end
    return false
end

-- 检查拆分时装
LuaFashionLotteryMgr.m_DissambleItemId = nil
LuaFashionLotteryMgr.m_DissambleItemTemplateId = nil
LuaFashionLotteryMgr.m_IsActivityOver = nil
function LuaFashionLotteryMgr:CheckFashionDissamble(itemId, templateId)
    self.m_DissambleItemId = itemId
    self.m_DissambleItemTemplateId = templateId
    CUIManager.ShowUI(CLuaUIResources.FashionLotterySplitWnd)
end

-- 打开拆分界面
function LuaFashionLotteryMgr:OpenFashionLotterySplitWnd(itemId, isActivityOver)
    self.m_DissambleItemId = itemId
    self.m_DissambleItemTemplateId = nil
    -- self.m_IsActivityOver = isActivityOver
    CUIManager.ShowUI(CLuaUIResources.FashionLotterySplitWnd)
end