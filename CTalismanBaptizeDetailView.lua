-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CTalismanBaptizeDetailView = import "L10.UI.CTalismanBaptizeDetailView"
local CTalismanWordBaptizeMgr = import "L10.UI.CTalismanWordBaptizeMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EnumQualityType = import "L10.Game.EnumQualityType"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
--local Talisman_ResetWord = import "L10.Game.Talisman_ResetWord"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTalismanBaptizeDetailView.m_Init_CS2LuaHook = function (this, index) 

    this.curRawTemplateId = 0
    this.curSelectIndex = index
    this.talismanQualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    this.talismanTexture.material = nil
    this.rawTexture.material = nil
    this.rawAmountLabel.text = ""
    local default
    if index == 0 then
        default = LocalString.GetString("重铸材料")
    else
        default = LocalString.GetString("炼化材料")
    end
    this.rawNameLabel.text = default
    this.acquireGo:SetActive(false)
    local extern
    if index == 0 then
        extern = LocalString.GetString("重铸")
    else
        extern = LocalString.GetString("炼化")
    end
    this.okButtonLabel.text = extern
    this.autoBaptizeButton:SetActive(index ~= 0)
    this.buttonNode:Reposition()
    local ref
    if index == 0 then
        ref = "Talisman_ChongZhu_Info"
    else
        ref = "Talisman_LianHua_Info"
    end
    UIEventListener.Get(this.tipButton).onClick = DelegateFactory.VoidDelegate(function (p)
        g_MessageMgr:ShowMessage(ref)
    end)
end
CTalismanBaptizeDetailView.m_OnTalismanSelect_CS2LuaHook = function (this, item) 

    this.talisman = item
    if this.talisman ~= nil then
        local commonItem = CItemMgr.Inst:GetById(item.itemId)
        this.talismanQualitySprite.spriteName = CUICommonDef.GetItemCellBorder(commonItem.Equip.QualityType)
        this.talismanTexture:LoadMaterial(commonItem.Icon)
        Talisman_ResetWord.Foreach(function (index, data) 
            if data.Color == EnumToInt(commonItem.Equip.QualityType) or (bit.bor(data.Color, EnumQualityType_lua.Dark)) == EnumToInt(commonItem.Equip.QualityType) then
                if data.TCRange[0] <= commonItem.Equip.NeedLevel and data.TCRange[1] >= commonItem.Equip.NeedLevel then
                    this.curRawTemplateId = this.curSelectIndex == 0 and data.ResetBasicCost[0] or data.ResetExtraCost[0]
                    this.curNeedCount = this.curSelectIndex == 0 and data.ResetBasicCost[1] or data.ResetExtraCost[1]
                end
            end
        end)
        local rawItem = Item_Item.GetData(this.curRawTemplateId)
        if rawItem ~= nil then
            this.rawTexture:LoadMaterial(rawItem.Icon)
            this.rawNameLabel.text = rawItem.Name
            this.rawNameLabel.color = GameSetting_Common_Wapper.Inst:GetColor(rawItem.NameColor)
            local bindCount, notbindCount
            bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(this.curRawTemplateId)
            if bindCount + notbindCount >= this.curNeedCount then
                this.rawAmountLabel.text = System.String.Format("{0}/{1}", bindCount + notbindCount, this.curNeedCount)
            else
                this.rawAmountLabel.text = System.String.Format("[c][FF0000]{0}[-]/{1}", bindCount + notbindCount, this.curNeedCount)
            end
            this.acquireGo:SetActive(bindCount + notbindCount < this.curNeedCount)
        end
    else
        this:Init(this.curSelectIndex)
    end
end
CTalismanBaptizeDetailView.m_OnEnable_CS2LuaHook = function (this) 
    --tabBar.OnTabChange += this.OnTabChanged;
    --tabBar.ChangeTab(CTalismanWndMgr.baptizeTabIndex);

    --tabBar.OnTabChange += this.OnTabChanged;
    --tabBar.ChangeTab(CTalismanWndMgr.baptizeTabIndex);

    UIEventListener.Get(this.okButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.okButton).onClick, MakeDelegateFromCSFunction(this.OnOkButtonClicked, VoidDelegate, this), true)
    UIEventListener.Get(this.autoBaptizeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.autoBaptizeButton).onClick, MakeDelegateFromCSFunction(this.OnAutoBaptizeButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.talismanButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.talismanButton).onClick, MakeDelegateFromCSFunction(this.OnTalismanButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.rawItemButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.rawItemButton).onClick, MakeDelegateFromCSFunction(this.OnRawItemClick, VoidDelegate, this), true)
    UIEventListener.Get(this.acquireGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.acquireGo).onClick, MakeDelegateFromCSFunction(this.OnAcquireGoClick, VoidDelegate, this), true)
end
CTalismanBaptizeDetailView.m_OnDisable_CS2LuaHook = function (this) 
    --tabBar.OnTabChange -= this.OnTabChanged;

    --tabBar.OnTabChange -= this.OnTabChanged;

    UIEventListener.Get(this.okButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.okButton).onClick, MakeDelegateFromCSFunction(this.OnOkButtonClicked, VoidDelegate, this), false)
    UIEventListener.Get(this.autoBaptizeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.autoBaptizeButton).onClick, MakeDelegateFromCSFunction(this.OnAutoBaptizeButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.talismanButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.talismanButton).onClick, MakeDelegateFromCSFunction(this.OnTalismanButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.rawItemButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.rawItemButton).onClick, MakeDelegateFromCSFunction(this.OnRawItemClick, VoidDelegate, this), false)
    UIEventListener.Get(this.acquireGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.acquireGo).onClick, MakeDelegateFromCSFunction(this.OnAcquireGoClick, VoidDelegate, this), false)
end
CTalismanBaptizeDetailView.m_OnAutoBaptizeButtonClick_CS2LuaHook = function (this, go) 
    if this.talisman == nil then
        g_MessageMgr:ShowMessage("No_Talisman_Need_Refinery")
        return
    end
    CTalismanWordBaptizeMgr.OpenWordAutoBaptizeWnd(this.talisman)
end
CTalismanBaptizeDetailView.m_OnOkButtonClicked_CS2LuaHook = function (this, go) 

    if this.talisman == nil then
        g_MessageMgr:ShowMessage("No_Talisman_Need_Refinery")
        return
    end
    if this.curSelectIndex == 0 then
        CTalismanWordBaptizeMgr.OpenBaseWordBaptize(this.talisman)
    else
        CTalismanWordBaptizeMgr.OpenAdditionalWordBaptize(this.talisman)
    end
end
CTalismanBaptizeDetailView.m_OnTalismanButtonClick_CS2LuaHook = function (this, go) 

    if this.talisman ~= nil then
        local item = CItemMgr.Inst:GetById(this.talisman.itemId)
        CItemInfoMgr.ShowLinkItemInfo(item, false, nil, AlignType.Default, 0, 0, 0, 0, 0)
    end
end
CTalismanBaptizeDetailView.m_OnRawItemClick_CS2LuaHook = function (this, go) 

    if this.talisman ~= nil and this.curRawTemplateId > 0 then
        CItemInfoMgr.ShowLinkItemTemplateInfo(this.curRawTemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end
end

CTalismanBaptizeDetailView.m_hookOnAcquireGoClick = function (this, go)
    print("m_hookOnAcquireGoClick", this.curRawTemplateId, this.talisman)
    if this.talisman ~= nil and this.curRawTemplateId > 0 then
        LuaItemAccessListMgr:ShowItemAccessInfoAtLeft(this.curRawTemplateId, false, go.transform)
    end
end