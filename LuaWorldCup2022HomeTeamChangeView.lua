local DelegateFactory = import "DelegateFactory"

LuaWorldCup2022HomeTeamChangeView = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWorldCup2022HomeTeamChangeView, "groupTemplate")
RegistClassMember(LuaWorldCup2022HomeTeamChangeView, "groupTable")

RegistClassMember(LuaWorldCup2022HomeTeamChangeView, "teamId2Trans")
RegistClassMember(LuaWorldCup2022HomeTeamChangeView, "homeTeamId")

function LuaWorldCup2022HomeTeamChangeView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self.groupTemplate = self.transform:Find("GroupTemplate").gameObject
    self.groupTable = self.transform:Find("ScrollView/Table"):GetComponent(typeof(UITable))
    self.groupTemplate:SetActive(false)

    self:InitGroup()
end

function LuaWorldCup2022HomeTeamChangeView:OnEnable()
    g_ScriptEvent:AddListener("WorldCupJingCai_UpdateAllJingCaiData", self, "UpdateAllJingCaiData")
    g_ScriptEvent:AddListener("WorldCupJingCai_ReplyTaoTaiInfoResult", self, "ReplyTaoTaiInfoResult")
    Gac2Gas.WorldCupJingCai_QueryTaoTaiInfo()
    self:UpdateHomeTeam()
    self:UpdateTaoTaiInfo()
end

function LuaWorldCup2022HomeTeamChangeView:OnDisable()
    g_ScriptEvent:RemoveListener("WorldCupJingCai_UpdateAllJingCaiData", self, "UpdateAllJingCaiData")
    g_ScriptEvent:RemoveListener("WorldCupJingCai_ReplyTaoTaiInfoResult", self, "ReplyTaoTaiInfoResult")
end

function LuaWorldCup2022HomeTeamChangeView:UpdateAllJingCaiData()
    self:UpdateHomeTeam()
end

function LuaWorldCup2022HomeTeamChangeView:ReplyTaoTaiInfoResult()
    self:UpdateTaoTaiInfo()
end

function LuaWorldCup2022HomeTeamChangeView:UpdateTaoTaiInfo()
    local taoTaiInfo = LuaWorldCup2022Mgr.lotteryInfo.taoTaiInfo
    if not taoTaiInfo then return end

    for id, value in pairs(taoTaiInfo) do
        if value > 0 and value < 6 then
            LuaUtils.SetLocalPositionZ(self.teamId2Trans[id]:Find("Icon"), -1)
            LuaUtils.SetLocalPositionZ(self.teamId2Trans[id]:Find("Name"), -1)
        end
    end
end

function LuaWorldCup2022HomeTeamChangeView:InitGroup()
    local group2Ids = {}
    WorldCup_TeamInfo.Foreach(function(id, data)
        local group = data.Group
        if not group2Ids[group] then
            group2Ids[group] = {}
        end
        table.insert(group2Ids[group], id)
    end)
    local groups = {"A", "B", "C", "D", "E", "F", "G", "H"}

    self.teamId2Trans = {}
    Extensions.RemoveAllChildren(self.groupTable.transform)
    for i = 1, 8 do
        local child = NGUITools.AddChild(self.groupTable.gameObject, self.groupTemplate)
        child:SetActive(true)

        local group = groups[i]
        child.transform:Find("Group"):GetComponent(typeof(UILabel)).text = group

        local table = child.transform:Find("Table")
        for j = 1, 4 do
            local trans = table:GetChild(j - 1)
            local teamId = group2Ids[group][j]
            if teamId then
                trans.gameObject:SetActive(true)
                self.teamId2Trans[teamId] = trans
                local data = WorldCup_TeamInfo.GetData(teamId)

                trans:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(data.Icon)
                trans:Find("Name"):GetComponent(typeof(UILabel)).text = data.Name
                trans:Find("Icon/HomeTeam").gameObject:SetActive(false)
                LuaUtils.SetLocalPositionZ(trans, 0)

                UIEventListener.Get(trans.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                    self:OnTeamClick(teamId)
                end)
            else
                trans.gameObject:SetActive(false)
            end
        end
    end
    self.groupTable:Reposition()
end

function LuaWorldCup2022HomeTeamChangeView:UpdateHomeTeam()
    local jingCaiData = LuaWorldCup2022Mgr.lotteryInfo.jingCaiData
    local teamId = jingCaiData and jingCaiData.m_HomeTeamId
    if not teamId or teamId == 0 then return end
    if self.homeTeamId and self.homeTeamId == teamId then return end
    if self.homeTeamId then
        self.teamId2Trans[self.homeTeamId]:Find("Icon/HomeTeam").gameObject:SetActive(false)
    end
    self.homeTeamId = teamId
    self.teamId2Trans[self.homeTeamId]:Find("Icon/HomeTeam").gameObject:SetActive(true)
end

--@region UIEvent

function LuaWorldCup2022HomeTeamChangeView:OnTeamClick(teamId)
    LuaWorldCup2022Mgr:ShowHomeTeamSelectWnd(teamId)
end

--@endregion UIEvent
