require("common/common_include")

local MessageMgr = import "L10.Game.MessageMgr"
local CButton = import "L10.UI.CButton"
local UIGrid = import "UIGrid"
local DateTime = import "System.DateTime"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Color = import "UnityEngine.Color"
local NGUIText = import "NGUIText"
local MessageWndManager = import "L10.UI.MessageWndManager"

-- 封仙大会报名界面
LuaFengXianSignUpWnd = class()

RegistChildComponent(LuaFengXianSignUpWnd, "SignUpBtn", CButton)
RegistChildComponent(LuaFengXianSignUpWnd, "PeriodGrid", UIGrid)
RegistChildComponent(LuaFengXianSignUpWnd, "PeriodTemplate", GameObject)
RegistChildComponent(LuaFengXianSignUpWnd, "FengXianDayLabel", UILabel)

RegistClassMember(LuaFengXianSignUpWnd, "PeriodList")
RegistClassMember(LuaFengXianSignUpWnd, "SelectedPeriodIdx")

function LuaFengXianSignUpWnd:Init()
	self.PeriodTemplate:SetActive(false)
	self.PeriodList = {}
	self.SelectedPeriodIdx = 0

	self:UpdateFengXianPeriods()
	self:UpdateSignUpButton()

	CommonDefs.AddOnClickListener(self.SignUpBtn.gameObject, DelegateFactory.Action_GameObject(function (gameObject)
		self:OnSignUpBtnClicked(gameObject)
	end), false)
end

-- 更新可报名时段
function LuaFengXianSignUpWnd:UpdateFengXianPeriods()
	CUICommonDef.ClearTransform(self.PeriodGrid.transform)
	self.PeriodList = {}

	-- 早场
	local nowTime = CServerTimeMgr.Inst:GetZone8Time()
	self.FengXianDayLabel.text = SafeStringFormat3(LocalString.GetString("%s月%s日"), tostring(nowTime.Month), tostring(nowTime.Day))
	local morningStartTime = DateTime(nowTime.Year, nowTime.Month, nowTime.Day, 12, 0, 0)
	for i = 1, 5 do
		local item = NGUITools.AddChild(self.PeriodGrid.gameObject, self.PeriodTemplate)
		local startTime = morningStartTime:AddMinutes(30 * (i-1))
		local enterTime = startTime:AddMinutes(-10)
		local isOver = i <= CLuaXianzhiMgr.m_CurPlayIndex
		local isSigned = i == CLuaXianzhiMgr.m_SignUpPlayIndex
		self:InitPeriodTemplate(item, enterTime, startTime, i, isOver, isSigned)
		item:SetActive(true)
		table.insert(self.PeriodList, item)
	end

	-- 晚场
	local eveningStartTime = DateTime(nowTime.Year, nowTime.Month, nowTime.Day, 19, 0, 0)
	for i = 6, 10 do
		local item = NGUITools.AddChild(self.PeriodGrid.gameObject, self.PeriodTemplate)
		local startTime = eveningStartTime:AddMinutes(30 * (i-6))
		local enterTime = startTime:AddMinutes(-10)
		local isOver = i <= CLuaXianzhiMgr.m_CurPlayIndex
		local isSigned = i == CLuaXianzhiMgr.m_SignUpPlayIndex
		self:InitPeriodTemplate(item, enterTime, startTime, i, isOver, isSigned)
		item:SetActive(true)
		table.insert(self.PeriodList, item)
	end
	self.PeriodGrid:Reposition()
end

function LuaFengXianSignUpWnd:InitPeriodTemplate(go, enterTime, startTime, index, isOver, isSigned)
	local EnterTimeLabel = go.transform:Find("EnterTimeLabel"):GetComponent(typeof(UILabel))
	local StartTimeLabel = go.transform:Find("StartTimeLabel"):GetComponent(typeof(UILabel))

	EnterTimeLabel.text = enterTime:ToString("HH:mm")
	StartTimeLabel.text = startTime:ToString("HH:mm")

	local color = Color.white
	if isSigned then
		color = NGUIText.ParseColor24("FFC300", 0)
	elseif isOver then
		color = NGUIText.ParseColor24("B2B2B2", 0)
	end
	EnterTimeLabel.color = color
	StartTimeLabel.color = color

	if not isOver and not isSigned then
		CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function (gameObject)
			self:OnPeriodClicked(gameObject, index)
		end), false)
	else
		CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function (gameObject)
			
		end), false)
	end 
end

-- 更新报名按钮的显示
function LuaFengXianSignUpWnd:UpdateSignUpButton()
	local isSigned = CLuaXianzhiMgr.m_SignUpPlayIndex > 0
	self.SignUpBtn.Enabled = not isSigned
	if isSigned then
		self.SignUpBtn.Text = LocalString.GetString("已报名")
	else
		self.SignUpBtn.Text = LocalString.GetString("报名")
	end
end

function LuaFengXianSignUpWnd:OnPeriodClicked(go, index)
	self.SelectedPeriodIdx = index
	for i = 1, #self.PeriodList do
		self:SetPeriodSelected(self.PeriodList[i], index == i)
	end
end

function LuaFengXianSignUpWnd:SetPeriodSelected(go, isSelected)
	local Selected = go.transform:Find("Selected").gameObject
	Selected:SetActive(isSelected)
end

function LuaFengXianSignUpWnd:OnSignUpBtnClicked(go)
	if CLuaXianzhiMgr.m_SignUpPlayIndex > 0 then
		MessageMgr.Inst:ShowMessage("Feng_Xian_Already_Signup", {})
		return
	end

	if self.SelectedPeriodIdx == 0 then
		MessageMgr.Inst:ShowMessage("Feng_Xian_Select_Time_First", {})
		return
	end

	local selectedPeriod = self.PeriodList[self.SelectedPeriodIdx]
	if selectedPeriod then
		local startTime = selectedPeriod.transform:Find("StartTimeLabel"):GetComponent(typeof(UILabel))
		local time = startTime.text
		local msg = g_MessageMgr:FormatMessage("Feng_Xian_Signup_Confirm", time)
		MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
        	Gac2Gas.RequestSignUpXianZhi(self.SelectedPeriodIdx)
    	end), nil, nil, nil, false)
	end
end

function LuaFengXianSignUpWnd:UpdateFengXianSignup()
	self:UpdateSignUpButton()
	self:UpdateFengXianPeriods()
end

function LuaFengXianSignUpWnd:OnEnable()
	g_ScriptEvent:AddListener("FengXianSignUpUpdate", self, "UpdateFengXianSignup")
end

function LuaFengXianSignUpWnd:OnDisable()
	g_ScriptEvent:RemoveListener("FengXianSignUpUpdate", self, "UpdateFengXianSignup")
end

return LuaFengXianSignUpWnd