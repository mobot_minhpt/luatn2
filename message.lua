

local MsgDisplayerHolder = import "L10.Game.Message.MsgDisplayerHolder"
local MessageMgr = import "L10.Game.MessageMgr"
local CChatLinkMgr = import "CChatLinkMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local DelegateFactory = import "DelegateFactory"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local MessageBoxType = import "L10.UI.MessageWndManager+MessageBoxType"

g_MessageMgr = {}

function g_MessageMgr:DisplayMessage(msgName, ...)
	local messageId = MessageMgr.Inst:GetMessageIdByName(msgName)
	local msgFormat = messageId~=0 and MessageMgr.Inst:GetMessageFormat(messageId)
	msgFormat = string.gsub(msgFormat or "", "(%%[^0-9.lcdfosux%%])", "%%%1")
	if CommonDefs.IS_VN_CLIENT then
		msgFormat = string.gsub(msgFormat or "", "(%%%.[^%d])", "%%%1")
	end
	local args = {...}
	if #args == 1 and GetType(args[1]).IsArray then
		local array = args[1]
		args = {}
		for i = 0, array.Length do
			table.insert(args, array[i])
		end
	end
	local message = SafeStringFormat(msgFormat, unpack(args))
	MsgDisplayerHolder.Inst:DisplayMessage_lua(msgName, message)
end

function g_MessageMgr:Format(msgFormat, ...)
	msgFormat = string.gsub(msgFormat or "", "(%%[^0-9.lcdfosux%%])", "%%%1")
	if CommonDefs.IS_VN_CLIENT then
		msgFormat = string.gsub(msgFormat or "", "(%%%.[^%d])", "%%%1")
	end
	local args = {...}
	if #args == 1 and GetType(args[1]).IsArray then
		local array = args[1]
		args = {}
		for i = 0, array.Length-1 do
			table.insert(args, array[i])
		end
	end
	local message = SafeStringFormat(msgFormat, unpack(args)) 
	return CChatLinkMgr.TranslateToNGUIText(message)
end

function g_MessageMgr:FormatMessage(msgName, ...)
	local messageId = MessageMgr.Inst:GetMessageIdByName(msgName)
	local msgFormat = messageId~=0 and MessageMgr.Inst:GetMessageFormat(messageId)
	return msgFormat and self:Format(msgFormat, ...) or SafeStringFormat("%s:%s", LocalString.GetString("消息未定义"), msgName)
end

function g_MessageMgr:ShowOkCancelMessage(msg, okFunc, cancelFunc, okText, cancelText, showCloseButton)
	okFunc = okFunc or function () end
	cancelFunc = cancelFunc or function () end
	MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(okFunc), DelegateFactory.Action(cancelFunc), okText, cancelText, showCloseButton)
end

function g_MessageMgr:ShowMessage(msgName, ...)
	self:DisplayMessage(msgName, ...)
end 

function g_MessageMgr:ShowCustomMsg(msg)
	g_MessageMgr:ShowMessage("CUSTOM_STRING2", msg)
end

local Constants = import "L10.Game.Constants"
function g_MessageMgr:DecorateTextWithFeiShengColor(str)
	return SafeStringFormat(LocalString.GetString("[c][%s]%s[-][/c]"), Constants.ColorOfFeiSheng, str)
end

--在顶部显示提示信息
function g_MessageMgr:ShowTopNotice(getTextAction, countdown)
	g_ScriptEvent:BroadcastInLua("OnShowTopNotice", getTextAction, countdown)
end

function g_MessageMgr:HideTopNotice()
	g_ScriptEvent:BroadcastInLua("OnHideTopNotice")
end

--只有一个确认按钮的消息框，确认按钮的文字有倒计时
function g_MessageMgr:ShowConfirmMessageWithBtnDuration(msg, okBtnText, okBtnAction, aliveDuration)
    local messageBoxInfo = MessageWndManager.MessageBoxInfoWithTimelimitAndPriority
    messageBoxInfo.id = MessageWndManager.GetNextID()
    messageBoxInfo.text = msg
    messageBoxInfo.aliveDuration = aliveDuration
    messageBoxInfo.okLabel = okBtnText
    messageBoxInfo.onOKButtonClick = okBtnAction
    messageBoxInfo.cancelLabel = ""
    messageBoxInfo.type = MessageBoxType.Confirm
    messageBoxInfo.okBtnDefaultSelected = true
    messageBoxInfo.showCloseButton = false
    messageBoxInfo.priority = 0
    MessageWndManager.MessageBoxInfoWithTimelimitAndPriority = messageBoxInfo;
    CUIManager.ShowUI("MessageBoxTimeLimitWnd")
end


-----------------------------------------------
------ RPC ShowConfirmMessage 处理 BEGIN
-----------------------------------------------

--EnumConfirmWndType 和 EnumConfirmMessageType 定义来自服务器端
EnumConfirmWndType = {
	eDefault = 0,
	ePlay = 1,
	eKeepShow = 2,
	eSelect = 3,
    eRepetitive = 4, --窗口可以多个共存，逻辑需要处理成不互斥
}

EnumConfirmMessageType = {
	eCancleCountdown = 1,
	eOkCountdown = 2,
	eMPTZCountdown = 3,
}

EnumRepetitiveDialogCloseType = { --当SideDialog玩家点击了全部拒绝时，确认窗需要执行的行为
	eDoNothing = 0,
	eCancel = 1,
	eOK = 2,
}

EnumRepetitivExclusiveType = { --SidDialog窗的类型信息，同一个类型的窗口可以在其中一个处理完毕后其他的一并关闭
	eNotExclusive = 0,
	eRequestTeamLeader = 1,
}

function g_MessageMgr:ShowConfirmMessage(sessionId, msg, timeout, countdownType, wndType, extraInfo_U)
	msg = CChatLinkMgr.TranslateToNGUIText(msg, false)

	local extraInfo = g_MessagePack.unpack(extraInfo_U)
    if wndType == EnumConfirmWndType.ePlay then
        local list = extraInfo
        if list and #list > 0 then
            local playId = tonumber(list[1])
            self:ShowConfirmMessageInternalForGamePlay(sessionId, playId, msg, timeout, countdownType, extraInfo)
		end
	elseif wndType == EnumConfirmWndType.eSelect then
		local list = extraInfo
        if list and #list > 0 then
            local playId = tonumber(list[1])
            LuaGamePlayMgr:ShowEnterSelectionWnd(sessionId, playId, msg, timeout, countdownType)
		end
    elseif wndType == EnumConfirmWndType.eRepetitive then
        local sideDialogCloseType =  extraInfo and #extraInfo>0 and extraInfo[1] or EnumRepetitiveDialogCloseType.eDoNothing
        local sideDialogType =  extraInfo and #extraInfo>1 and extraInfo[2] or EnumRepetitivExclusiveType.eNotExclusive
		self:ShowConfirmMessageInSideDialog(sessionId, msg, timeout, countdownType, wndType, sideDialogCloseType, sideDialogType, extraInfo)
    else
        self:ShowConfirmMessageInternal(sessionId, msg, timeout, countdownType, wndType, extraInfo)
	end
end

function g_MessageMgr:ShowConfirmMessageInternalForGamePlay(sessionId, playId, msg, timeout, countdownType, extraInfo)
	--For Flash Window In PC Client
	EventManager.BroadcastInternalForLua(EnumEventType.FlashPCWindow, {})
	if LuaChunJie2024Mgr:IsChunJie2024Gameplay(playId) then
		LuaChunJie2024Mgr:ShowGamePlayConfirmWnd(sessionId, playId, timeout, extraInfo)
	else
		CGamePlayMgr.Inst:ShowEnterGamePlayConfirmWnd(playId, timeout,
		DelegateFactory.Action_object(function(o)
			Gac2Gas.SendGamePlayConfirmMessageResult(sessionId, 1, MsgPackImpl.pack(o))
		end),
		DelegateFactory.Action_object(function(o)
			Gac2Gas.SendGamePlayConfirmMessageResult(sessionId, 0, MsgPackImpl.pack(o))
		end))
	end
end

function g_MessageMgr:ShowConfirmMessageInternal(sessionId, msg, timeout, countdownType, wndType, extraInfo)
	--For Flash Window In PC Client
	EventManager.BroadcastInternalForLua(EnumEventType.FlashPCWindow, {})
	local data = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
	CommonDefs.DictAdd_LuaCall(data, "rpc_comfirm_session_id", sessionId)
	if wndType == EnumConfirmWndType.eKeepShow then
		CommonDefs.DictAdd_LuaCall(data, "keep_open_when_click_ok_button", "1")
		CommonDefs.DictAdd_LuaCall(data, "keep_open_when_click_cancel_button", "1")
	end
	if countdownType == EnumConfirmMessageType.eMPTZCountdown then
		CommonDefs.DictAdd_LuaCall(data, "is_mptz_confirm_message", "1")
	end
	MessageWndManager.ShowConfirmMessage(msg, timeout, countdownType == EnumConfirmMessageType.eOkCountdown,
		DelegateFactory.Action(function()
			Gac2Gas.SendConfirmMessageResult(sessionId, 1)
		end),
		DelegateFactory.Action(function()
			Gac2Gas.SendConfirmMessageResult(sessionId, 0)
		end),false, data)
end

function g_MessageMgr:ShowConfirmMessageInSideDialog(sessionId, msg, timeout, countdownType, wndType, sideDialogCloseType, sideDialogType, extraInfo)
    EventManager.BroadcastInternalForLua(EnumEventType.FlashPCWindow, {})
    local okButtonDefaultSelected = (countdownType == EnumConfirmMessageType.eOkCountdown)
    local btn1Info = LuaCommonSideDialogMgr:GetBtnInfo(LocalString.GetString("确定"), 
        function() Gac2Gas.SendConfirmMessageResult(sessionId, 1) end, true, true, timeout, okButtonDefaultSelected)
    local btn2Info = LuaCommonSideDialogMgr:GetBtnInfo(LocalString.GetString("取消"),
        function() Gac2Gas.SendConfirmMessageResult(sessionId, 0) end, false, false, timeout, not okButtonDefaultSelected, true)
    local key = sessionId
    if sideDialogType and sideDialogType~=EnumRepetitivExclusiveType.eNotExclusive then
        key = "ConfirmMessageInSideDialog_"..tostring(sideDialogType)
    end
    --确定在右，取消在左，和原来的messagebox保持一致
    LuaCommonSideDialogMgr:ShowDialog(msg, key, btn2Info, btn1Info, nil, nil, {sessionId = sessionId}, function (data)
        if sideDialogCloseType == EnumRepetitiveDialogCloseType.eOK then
            Gac2Gas.SendConfirmMessageResult(data.extraData.sessionId, 1)
        elseif sideDialogCloseType == EnumRepetitiveDialogCloseType.eCancel then
            Gac2Gas.SendConfirmMessageResult(data.extraData.sessionId, 0)
        end
        --如果不指定则为eDoNothing 不做任何处理
	end, true)
end
-----------------------------------------------
------ RPC ShowConfirmMessage 处理 END
-----------------------------------------------
