-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CExpertTeamMgr = import "L10.Game.CExpertTeamMgr"
local CExpertTeamWnd = import "L10.UI.CExpertTeamWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local L10 = import "L10"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
CExpertTeamWnd.m_Init_CS2LuaHook = function (this)
    UIEventListener.Get(this.backBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        this:Close()
    end)

    UIEventListener.Get(this.chooseLevelBtn).onClick = MakeDelegateFromCSFunction(this.SetChooseLevelSign, VoidDelegate, this)
    UIEventListener.Get(this.searchBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        this:SearchQuestion(this.textInput.value)
    end)
    UIEventListener.Get(this.myAskBtn).onClick = MakeDelegateFromCSFunction(this.OpenSendQuestionWnd, VoidDelegate, this)
    UIEventListener.Get(this.myAskRecordBtn).onClick = MakeDelegateFromCSFunction(this.OpenMyQuestionWnd, VoidDelegate, this)
    UIEventListener.Get(this.totalAskBtn).onClick = MakeDelegateFromCSFunction(this.OpenTotalQuestionWnd, VoidDelegate, this)
    UIEventListener.Get(this.rankBtn).onClick = MakeDelegateFromCSFunction(this.OpenRankWnd, VoidDelegate, this)

    this.acceptNumLabel.text = tostring(CClientMainPlayer.Inst.PlayProp.JingLingExpertInfo.AcceptTimes)
    this.totalScoreLabel.text = tostring((CClientMainPlayer.Inst.PlayProp.JingLingExpertInfo.TotalScore - CClientMainPlayer.Inst.PlayProp.JingLingExpertInfo.TotalConsumeScore))

    this.emptyNode:SetActive(true)
    this.templateNode:SetActive(false)
    this.searchEmptyNode:SetActive(false)
    Extensions.RemoveAllChildren(this.table.transform)
    this.chooseLevelSign:SetActive(false)
    this.textInput.value = ""

    if CExpertTeamMgr.Inst.ExistsNewAnswer then
        local redpoint = this.myAskRecordBtn.transform:Find("RedPoint").gameObject
        if redpoint ~= nil then
            redpoint:SetActive(true)
        end
    end

    if not System.String.IsNullOrEmpty(CExpertTeamMgr.Inst.DefaultSearchString) then
        this.textInput.value = CExpertTeamMgr.Inst.DefaultSearchString
        this:SearchQuestion(CExpertTeamMgr.Inst.DefaultSearchString)
    end
end
CExpertTeamWnd.m_SetChooseLevelSign_CS2LuaHook = function (this, go)
    if this.chooseLevelSign.activeSelf then
        this.chooseLevelSign:SetActive(false)
    else
        this.chooseLevelSign:SetActive(true)
    end
end
CExpertTeamWnd.m_SearchQuestion_CS2LuaHook = function (this, searchString)
    if CClientMainPlayer.Inst == nil then
        return
    end

    --string content = CExpertTeamMgr.GetSendText(searchString);
    local content = searchString
    if System.String.IsNullOrEmpty(content) then
        g_MessageMgr:ShowMessage("ENTER_TEXT_TIP")
        return
    end

    content = CWordFilterMgr.Inst:DoFilterOnSendPersonalSpace(content,true)

    if System.String.IsNullOrEmpty(content) then
        return
    end

    local role_level = CClientMainPlayer.Inst.Level
    local is_level = this.chooseLevelSign.activeSelf and 1 or 0

    CExpertTeamMgr.Inst:SearchQuestion(role_level, is_level, content, MakeDelegateFromCSFunction(this.InitQuestionList, MakeGenericClass(Action1, String), this), nil)
end
CExpertTeamWnd.m_InitQuestionList_CS2LuaHook = function (this, ret)
    if not this then
        return
    end

    if this.emptyNode.activeSelf then
        this.emptyNode:SetActive(false)
    end

    Extensions.RemoveAllChildren(this.table.transform)
    local retDic = TypeAs(L10.Game.Utils.Json.Deserialize(ret), typeof(MakeGenericClass(Dictionary, String, Object)))
    local result = CommonDefs.DictGetValue(retDic, typeof(String), "result")
    local data = TypeAs(CommonDefs.DictGetValue(retDic, typeof(String), "data"), typeof(MakeGenericClass(List, Object)))
    if result then
        if data ~= nil and data.Count > 0 then
            this.searchEmptyNode:SetActive(false)
            do
                local i = 0
                while i < data.Count do
                    local info = TypeAs(data[i], typeof(MakeGenericClass(Dictionary, String, Object)))
                    local node = NGUITools.AddChild(this.table.gameObject, this.templateNode)
                    node:SetActive(true)
                    CommonDefs.GetComponent_Component_Type(node.transform:Find("content"), typeof(UILabel)).text = ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "content"))
                    CommonDefs.GetComponent_Component_Type(node.transform:Find("answer"), typeof(UILabel)).text = ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "adoptanswer"))
                    CommonDefs.GetComponent_Component_Type(node.transform:Find("answernum/num"), typeof(UILabel)).text = ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "answer_number"))
                    local clickbutton = node.transform:Find("bgbutton").gameObject
                    local qid = System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "qid")))
                    UIEventListener.Get(clickbutton).onClick = DelegateFactory.VoidDelegate(function (go)
                        this:OpenDetailQuestion(qid)
                    end)

                    CExpertTeamMgr.Inst:SetPlayerIcon(System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "role_id"))), ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "avatar")), node.transform:Find("icon").gameObject, nil, ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "content")), qid, 0)
                    i = i + 1
                end
            end

            this.table:Reposition()
            this.scrollView:ResetPosition()
        else
            this.searchEmptyNode:SetActive(true)
        end
    end
end
CExpertTeamWnd.m_OpenMyQuestionWnd_CS2LuaHook = function (this, go)
    CUIManager.ShowUI(CUIResources.ExpertTeamAskHisWnd)
    local redpoint = this.myAskRecordBtn.transform:Find("RedPoint").gameObject
    if redpoint ~= nil then
        redpoint:SetActive(false)
    end
end
