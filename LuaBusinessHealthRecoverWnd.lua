local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"

LuaBusinessHealthRecoverWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBusinessHealthRecoverWnd, "CancelBtn", "CancelBtn", CButton)
RegistChildComponent(LuaBusinessHealthRecoverWnd, "OKBtn", "OKBtn", CButton)
RegistChildComponent(LuaBusinessHealthRecoverWnd, "CostLab", "CostLab", UILabel)
RegistChildComponent(LuaBusinessHealthRecoverWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", GameObject)
RegistChildComponent(LuaBusinessHealthRecoverWnd, "Tip", "Tip", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaBusinessHealthRecoverWnd, "m_DecreaseButton")
RegistClassMember(LuaBusinessHealthRecoverWnd, "m_IncreaseButton")
RegistClassMember(LuaBusinessHealthRecoverWnd, "m_InputLab")
RegistClassMember(LuaBusinessHealthRecoverWnd, "m_InputNum")
RegistClassMember(LuaBusinessHealthRecoverWnd, "m_InputNumMax")
RegistClassMember(LuaBusinessHealthRecoverWnd, "m_CostPerHealth")

function LuaBusinessHealthRecoverWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_DecreaseButton   = self.QnIncreseAndDecreaseButton.transform:Find("DecreaseButton"):GetComponent(typeof(QnButton))
    self.m_IncreaseButton   = self.QnIncreseAndDecreaseButton.transform:Find("IncreaseButton"):GetComponent(typeof(QnButton))
    self.m_InputLab         = self.QnIncreseAndDecreaseButton.transform:Find("InputLab"):GetComponent(typeof(UILabel))

    UIEventListener.Get(self.m_DecreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDecreaseButtonClick()
	end)

	UIEventListener.Get(self.m_IncreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnIncreaseButtonClick()
	end)

	UIEventListener.Get(self.m_InputLab.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnInputLabClick()
	end)

    UIEventListener.Get(self.OKBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnOKBtnClick()
	end)

	UIEventListener.Get(self.CancelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCancelBtnClick()
	end)

    self.m_CostPerHealth = Business_Setting.GetData().HospitalExpenses
	self.Tip.text = g_MessageMgr:FormatMessage("BUSINESS_HEALTHRECOVER_TIP")
end

function LuaBusinessHealthRecoverWnd:Init()
    self.m_InputNumMax = math.min(math.floor(LuaBusinessMgr.m_OwnMoney / self.m_CostPerHealth), 100 - LuaBusinessMgr.m_Health)
	self.m_InputNumMax = math.max(0, self.m_InputNumMax)
    self:SetInputNum(1)
end

--@region UIEvent

--@endregion UIEvent

function LuaBusinessHealthRecoverWnd:OnDecreaseButtonClick()
    self:SetInputNum(self.m_InputNum - 1)
end

function LuaBusinessHealthRecoverWnd:OnIncreaseButtonClick()
    self:SetInputNum(self.m_InputNum + 1)
end

function LuaBusinessHealthRecoverWnd:OnInputLabClick()
    CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(0, self.m_InputNumMax, self.m_InputNum, 100, DelegateFactory.Action_int(function (val) 
		self.m_InputLab.text = val
    end), DelegateFactory.Action_int(function (val) 
        self:SetInputNum(val)
    end), self.m_InputLab, AlignType.Right, true)
end

function LuaBusinessHealthRecoverWnd:SetInputNum(num)
	if num < 0 then
		num = 0
	end

	if num > self.m_InputNumMax then
		num = self.m_InputNumMax
	end

	self.m_InputNum = num
	self.m_InputLab.text = num
    self.CostLab.text = num * self.m_CostPerHealth

	self.m_IncreaseButton.Enabled = self.m_InputNumMax - num > 0
	self.m_DecreaseButton.Enabled = num > 0
	if self.m_InputNumMax - num < 0 then
		if self.m_IncreaseTick ~= nil then
			UnRegisterTick(self.m_IncreaseTick)
			self.m_IncreaseTick = nil
		end
	end

	if num < 0 then
		if self.m_DecreaseTick ~= nil then
			UnRegisterTick(self.m_DecreaseTick)
			self.m_DecreaseTick = nil
		end
	end
end

function LuaBusinessHealthRecoverWnd:OnCancelBtnClick()
    CUIManager.CloseUI(CLuaUIResources.BusinessHealthRecoverWnd)
end

function LuaBusinessHealthRecoverWnd:OnOKBtnClick()
	Gac2Gas.TradeSimulationBuyHp(self.m_InputNum)
    CUIManager.CloseUI(CLuaUIResources.BusinessHealthRecoverWnd)
end
