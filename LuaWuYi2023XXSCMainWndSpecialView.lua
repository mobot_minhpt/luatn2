local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local UIProgressBar = import "UIProgressBar"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaWuYi2023XXSCMainWndSpecialView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuYi2023XXSCMainWndSpecialView, "ItemsRoot", "ItemsRoot", GameObject)
RegistChildComponent(LuaWuYi2023XXSCMainWndSpecialView, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaWuYi2023XXSCMainWndSpecialView, "SelectItemsButton", "SelectItemsButton", CButton)
RegistChildComponent(LuaWuYi2023XXSCMainWndSpecialView, "DrawButton", "DrawButton", CButton)
RegistChildComponent(LuaWuYi2023XXSCMainWndSpecialView, "DrawAllButton", "DrawAllButton", CButton)
RegistChildComponent(LuaWuYi2023XXSCMainWndSpecialView, "BottomReadMeLabel", "BottomReadMeLabel", UILabel)
RegistChildComponent(LuaWuYi2023XXSCMainWndSpecialView, "GoldNumLabel1", "GoldNumLabel1", UILabel)
RegistChildComponent(LuaWuYi2023XXSCMainWndSpecialView, "SilverNumLabel1", "SilverNumLabel1", UILabel)
RegistChildComponent(LuaWuYi2023XXSCMainWndSpecialView, "GoldNumLabel2", "GoldNumLabel2", UILabel)
RegistChildComponent(LuaWuYi2023XXSCMainWndSpecialView, "SilverNumLabel2", "SilverNumLabel2", UILabel)
RegistChildComponent(LuaWuYi2023XXSCMainWndSpecialView, "HasItemBottomView", "HasItemBottomView", GameObject)
RegistChildComponent(LuaWuYi2023XXSCMainWndSpecialView, "NullItemBottomView", "NullItemBottomView", GameObject)
RegistChildComponent(LuaWuYi2023XXSCMainWndSpecialView, "FinishedBottomView", "FinishedBottomView", GameObject)
RegistChildComponent(LuaWuYi2023XXSCMainWndSpecialView, "BottomFinishedLabel", "BottomFinishedLabel", UILabel)
RegistChildComponent(LuaWuYi2023XXSCMainWndSpecialView, "ProgressLabel", "ProgressLabel", UILabel)
RegistChildComponent(LuaWuYi2023XXSCMainWndSpecialView, "RewardGrid", "RewardGrid", UIGrid)
RegistChildComponent(LuaWuYi2023XXSCMainWndSpecialView, "RewardItemTemplate", "RewardItemTemplate", GameObject)
RegistChildComponent(LuaWuYi2023XXSCMainWndSpecialView, "RewardProgressBar", "RewardProgressBar", UIProgressBar)
RegistChildComponent(LuaWuYi2023XXSCMainWndSpecialView, "NoneItemsLabel", "NoneItemsLabel", UILabel)
--@endregion RegistChildComponent end
RegistClassMember(LuaWuYi2023XXSCMainWndSpecialView, "m_ShowFxTick")
RegistClassMember(LuaWuYi2023XXSCMainWndSpecialView, "m_ShowFxTick2")
RegistClassMember(LuaWuYi2023XXSCMainWndSpecialView, "m_Itemid2Obj")
RegistClassMember(LuaWuYi2023XXSCMainWndSpecialView, "m_IsInit")
RegistClassMember(LuaWuYi2023XXSCMainWndSpecialView, "m_LotteryList")

function LuaWuYi2023XXSCMainWndSpecialView:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.SelectItemsButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSelectItemsButtonClick()
	end)


	
	UIEventListener.Get(self.DrawButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDrawButtonClick()
	end)


	
	UIEventListener.Get(self.DrawAllButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDrawAllButtonClick()
	end)


    --@endregion EventBind end
end

function LuaWuYi2023XXSCMainWndSpecialView:Start()
	self.ItemTemplate.gameObject:SetActive(false)
	local fx = self.ItemTemplate.transform:Find("CUIFx_baodian")
	fx.gameObject:SetActive(false)
	self.RewardItemTemplate.gameObject:SetActive(false)
	self.BottomFinishedLabel.text = g_MessageMgr:FormatMessage("WuYi2023XXSCMainWnd_BottomFinishedLabel")
	self.NoneItemsLabel.text = g_MessageMgr:FormatMessage("WuYi2023XXSCMainWnd_BottomReadMeLabel_Unselected")
end

function LuaWuYi2023XXSCMainWndSpecialView:OnEnable()
	g_ScriptEvent:AddListener("OnXinXiangShiChengSendLotteryInfo", self, "OnXinXiangShiChengSendLotteryInfo")
	g_ScriptEvent:AddListener("OnXinXiangShiChengAdvancedResult", self, "OnXinXiangShiChengAdvancedResult")
	Gac2Gas.XinXiangShiChengQueryLotteryInfo()
end

function LuaWuYi2023XXSCMainWndSpecialView:OnDisable()
	g_ScriptEvent:RemoveListener("OnXinXiangShiChengSendLotteryInfo", self, "OnXinXiangShiChengSendLotteryInfo")
	g_ScriptEvent:RemoveListener("OnXinXiangShiChengAdvancedResult", self, "OnXinXiangShiChengAdvancedResult")
	self:HideCUIFx()
	self:CancelShowFxTick()
end

function LuaWuYi2023XXSCMainWndSpecialView:OnXinXiangShiChengAdvancedResult()
	local itemInfo = LuaWuYi2023Mgr.m_LotteryInfo.itemInfo
	local idset = {}
	for _,t in pairs(LuaWuYi2023Mgr.m_AdvancedResult) do
		idset[t.ItemID] = true
	end
	for i = 1, #itemInfo do
		local itemId = itemInfo[i].itemId
		local obj = self.m_Itemid2Obj[itemId]
		local fx = obj.transform:Find("CUIFx_baodian")
		fx.gameObject:SetActive(false)
		if idset[itemId] then
			fx.gameObject:SetActive(true)
		end
	end
	self:CancelShowFxTick()
	self.m_ShowFxTick2 = RegisterTickOnce(function()
		for i = 1, #itemInfo do
			local itemId = itemInfo[i].itemId
			if idset[itemId] then
				local obj = self.m_Itemid2Obj[itemId]
				local data = itemInfo[i]
				self:InitItem(obj, data)
			end
		end
	end,300)
	self.m_ShowFxTick = RegisterTickOnce(function()
		self:HideCUIFx()
		LuaWuYi2023Mgr.m_GetRewardWndType = 0
		LuaCommonGetRewardWnd.m_materialName = "gongxihuode"
		LuaCommonGetRewardWnd.m_Reward1List = LuaWuYi2023Mgr.m_AdvancedResult
		LuaCommonGetRewardWnd.m_Reward2Label = ""
		LuaCommonGetRewardWnd.m_Reward2List = {  }
		LuaCommonGetRewardWnd.m_hint = ""
		LuaCommonGetRewardWnd.m_button = {
			{spriteName="blue", buttonLabel = LocalString.GetString("确定"), clickCB = function() 
				CUIManager.CloseUI(CLuaUIResources.WuYi2023CommonGetRewardWnd)
				if LuaWuYi2023Mgr.m_SilverCanGive > 0 then
					CUIManager.ShowUI(CLuaUIResources.WuYi2023XXSCGiveSilverWnd)
				end
			end},
		}
		CUIManager.ShowUI(CLuaUIResources.WuYi2023CommonGetRewardWnd)
	end,700)
end

function LuaWuYi2023XXSCMainWndSpecialView:CancelShowFxTick()
	if self.m_ShowFxTick then
		UnRegisterTick(self.m_ShowFxTick)
		self.m_ShowFxTick = nil
	end
	if self.m_ShowFxTick2 then
		UnRegisterTick(self.m_ShowFxTick2)
		self.m_ShowFxTick2 = nil
	end
end

function LuaWuYi2023XXSCMainWndSpecialView:HideCUIFx()
	if not self.m_Itemid2Obj then return end
	for itemid, obj in pairs(self.m_Itemid2Obj) do
		local fx = obj.transform:Find("CUIFx_baodian")
		fx.gameObject:SetActive(false)
	end
	self:InitItems()
end

function LuaWuYi2023XXSCMainWndSpecialView:OnXinXiangShiChengSendLotteryInfo()
	local info = LuaWuYi2023Mgr.m_LotteryInfo
	self:UpdateBottomReadMeLabel()
	self.GoldNumLabel1.text = info.allTakeNeedGold
	self.GoldNumLabel1.color = NGUIText.ParseColor24(info.allTakeNeedGold > (info.haveGold) and "ff5050" or "ffffff", 0)
	self.SilverNumLabel1.text = info.allTakeNeedSilver
	self.GoldNumLabel2.text = info.needGold
	self.GoldNumLabel2.color = NGUIText.ParseColor24(info.needGold > (info.haveGold) and "ff5050" or "ffffff", 0)
	self.SilverNumLabel2.text = info.needSilver
	if not self.m_IsInit then
		self:InitItems()
	end
	self:InitRewardView()
	self.NullItemBottomView.gameObject:SetActive(#info.itemInfo == 0)
	self.HasItemBottomView.gameObject:SetActive((#info.itemInfo > 0) and (#info.itemInfo > info.numberOfDraws))
	self.FinishedBottomView.gameObject:SetActive((#info.itemInfo > 0) and (#info.itemInfo == info.numberOfDraws))
	CUIManager.CloseUI(CLuaUIResources.WuYi2023XXSCRewardSelectWnd)
	if info.itemInfo and #info.itemInfo > 0 then
		self.m_IsInit = true
	end
end

function LuaWuYi2023XXSCMainWndSpecialView:InitRewardView()
	local info = LuaWuYi2023Mgr.m_LotteryInfo
	self.ProgressLabel.text = SafeStringFormat3(LocalString.GetString("%d"),info.numberOfDraws, #info.itemInfo)
	self.m_LotteryList = {}
	WuYi2023_Lottery.ForeachKey(function (k)
		local v = WuYi2023_Lottery.GetData(k)
		if v.ExtraRewardItemId > 0 then
			table.insert(self.m_LotteryList, v)
		end
	end)
	table.sort(self.m_LotteryList,function (a,b)
		return a.ID > b.ID
	end)
	Extensions.RemoveAllChildren(self.RewardGrid.transform)
	local count = 0
	for i = 1, #self.m_LotteryList do
		local data = self.m_LotteryList[i]
		local itemData = Item_Item.GetData(data.ExtraRewardItemId)
		local obj = NGUITools.AddChild(self.RewardGrid.gameObject,self.RewardItemTemplate.gameObject)
		obj:SetActive(true)
		local icon = obj.transform:Find("Mask/IconTexture"):GetComponent(typeof(CUITexture))
		local unlock = obj.transform:Find("Panel/Unlock")
		local lock = obj.transform:Find("Panel/Lock")
		local timesLabel = obj.transform:Find("Panel/TimesLabel"):GetComponent(typeof(UILabel))
		icon:LoadMaterial(itemData.Icon)
		unlock.gameObject:SetActive(info.numberOfDraws >= data.ID)
		lock.gameObject:SetActive(info.numberOfDraws < data.ID)
		timesLabel.text = SafeStringFormat3(LocalString.GetString("转%d次"),data.ID)
		if info.numberOfDraws >= data.ID then
			count = count + 1
		end
		UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (go)
			CItemInfoMgr.ShowLinkItemTemplateInfo(data.ExtraRewardItemId, false, nil, AlignType.Default, 0, 0, 0, 0) 
		end)
	end
	self.RewardGrid:Reposition()
	self.RewardProgressBar.value = count / #self.m_LotteryList
end

function LuaWuYi2023XXSCMainWndSpecialView:InitItems()
	local info = LuaWuYi2023Mgr.m_LotteryInfo
	self.m_Itemid2Obj = {}
	for i = 0, self.ItemsRoot.transform.childCount - 1 do
		local child = self.ItemsRoot.transform:GetChild(i)
		Extensions.RemoveAllChildren(child.transform)
		local obj = NGUITools.AddChild(child.gameObject, self.ItemTemplate.gameObject)
		obj.transform.localPosition = Vector3.zero
		obj:SetActive(true)
		local data = info.itemInfo and info.itemInfo[i + 1]
		
		self:InitItem(obj, data)
	end
end

function LuaWuYi2023XXSCMainWndSpecialView:InitItem(obj, data)
	local itemWidget = obj:GetComponent(typeof(UIWidget))
	local nullObj = obj.transform:Find("Null")
	local itemIcon = obj.transform:Find("ItemIcon"):GetComponent(typeof(CUITexture))
	local bottomLabel = obj.transform:Find("BottomLabel"):GetComponent(typeof(UILabel))
	local gotObj = obj.transform:Find("Got")
	local qualityObj = obj.transform:Find("Quality")
	local purpleQuality = obj.transform:Find("Quality/Purple")
	local blueQuality = obj.transform:Find("Quality/Blue")
	local whiteQuality = obj.transform:Find("Quality/White")

	nullObj.gameObject:SetActive(data == nil)
	itemIcon.gameObject:SetActive(data ~= nil)
	bottomLabel.gameObject:SetActive(data ~= nil)
	gotObj.gameObject:SetActive(false)
	qualityObj.gameObject:SetActive(data ~= nil)

	if data then
		local itemId = data.itemId
		local status = data.status
		local itemData = Item_Item.GetData(itemId)
		local rewardData = WuYi2023_AdvancedRewards.GetData(itemId)
		itemIcon:LoadMaterial(itemData.Icon)
		if rewardData then
			bottomLabel.text = (rewardData.Amount > 1) and rewardData.Amount or ""
			local fashionData = Fashion_Fashion.GetDataBySubKey("ItemID",itemId)
			if fashionData then
				bottomLabel.text = SafeStringFormat3(LocalString.GetString("%d天"), fashionData.AvailableTime)
			end
			local zuoqiData = ZuoQi_ZuoQi.GetDataBySubKey("ItemID",itemId)
			if zuoqiData then
				bottomLabel.text = SafeStringFormat3(LocalString.GetString("%d天"), zuoqiData.AvailableTime)
			end
			purpleQuality.gameObject:SetActive(rewardData.Category == 1 or rewardData.Category == 2)
			blueQuality.gameObject:SetActive(rewardData.Category == 3)
			whiteQuality.gameObject:SetActive(rewardData.Category == 4)
		end
		gotObj.gameObject:SetActive(status == 1)
		itemWidget.alpha = status == 1 and 0.6 or 1
		self.m_Itemid2Obj[itemId] = obj

		UIEventListener.Get(itemIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0) 
		end)
	end
end

function LuaWuYi2023XXSCMainWndSpecialView:UpdateBottomReadMeLabel()
	local num = 0
	local info = LuaWuYi2023Mgr.m_LotteryInfo
	if info.numberOfDraws then
		local data = WuYi2023_Lottery.GetData(info.numberOfDraws + 1)
		if data then
			num = data.Silver
		end
	end
	self.BottomReadMeLabel.text = num == 0 and "" or g_MessageMgr:FormatMessage("WuYi2023XXSCMainWnd_BottomReadMeLabel", num, num)
end

--@region UIEvent

function LuaWuYi2023XXSCMainWndSpecialView:OnSelectItemsButtonClick()
	CUIManager.ShowUI(CLuaUIResources.WuYi2023XXSCRewardSelectWnd)
end


function LuaWuYi2023XXSCMainWndSpecialView:OnDrawButtonClick()
	local info = LuaWuYi2023Mgr.m_LotteryInfo
	if not info then return end
	if info.needGold > (info.haveGold ) then
		local msg = g_MessageMgr:FormatMessage("WuYi2023XXSCMainWnd_GoldNotEnough_Confirm")
		MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
			LuaWuYi2023Mgr.m_IsExchangeGold = true
			CUIManager.ShowUI(CLuaUIResources.WuYi2023XXSCExchangeGoldWnd)
		end),nil,LocalString.GetString("前往"),LocalString.GetString("取消"),false)
		return
	end

	local goldNum = info.needGold 
	local silverNum = info.needSilver
	local times = 1
	local msg = g_MessageMgr:FormatMessage("WuYi2023XXSCMainWnd_Draw_Confirm", goldNum, silverNum, times)
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		Gac2Gas.XinXiangShiChengAdvancedLottery(false)
	end),nil,LocalString.GetString("转动"),LocalString.GetString("取消"),false)
end

function LuaWuYi2023XXSCMainWndSpecialView:OnDrawAllButtonClick()
	local info = LuaWuYi2023Mgr.m_LotteryInfo
	if not info then return end
	if info.allTakeNeedGold > (info.haveGold) then
		local msg = g_MessageMgr:FormatMessage("WuYi2023XXSCMainWnd_GoldNotEnough_Confirm")
		MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
			LuaWuYi2023Mgr.m_IsExchangeGold = true
			CUIManager.ShowUI(CLuaUIResources.WuYi2023XXSCExchangeGoldWnd)
		end),nil,LocalString.GetString("前往"),LocalString.GetString("取消"),false)
		return
	end

	local goldNum = info.allTakeNeedGold 
	local silverNum = info.allTakeNeedSilver
	local times = 0
	local tempGoldNum = info.allTakeNeedGold 
	local numberOfDraws = info.numberOfDraws + 1
	while tempGoldNum > 0 do
		local data = WuYi2023_Lottery.GetData(numberOfDraws)
		if data then
			tempGoldNum = tempGoldNum - data.Gold
			numberOfDraws = numberOfDraws + 1
			times = times + 1
		else
			tempGoldNum = 0
		end
	end
	local msg = g_MessageMgr:FormatMessage("WuYi2023XXSCMainWnd_Draw_Confirm", goldNum, silverNum, times)
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		Gac2Gas.XinXiangShiChengAdvancedLottery(true)
	end),nil,LocalString.GetString("转动"),LocalString.GetString("取消"),false)
end


--@endregion UIEvent

