local CButton = import "L10.UI.CButton"
local EShadowType=import "L10.Engine.EShadowType"
local LayerDefine=import "L10.Engine.LayerDefine"

local UILabel = import "UILabel"

local QnTableView = import "L10.UI.QnTableView"
local UITable = import "UITable"
local CUITexture = import "L10.UI.CUITexture"

local GameObject = import "UnityEngine.GameObject"
local Gac2Login = import "L10.Game.Gac2Login"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local Constants = import "L10.Game.Constants"
local NGUIText = import "NGUIText"
local System = import "System"
local Animation = import "UnityEngine.Animation"
local EnumGender = import "L10.Game.EnumGender"
local EnumClass = import "L10.Game.EnumClass"
local AMPlayer = import "L10.Game.CutScene.AMPlayer"
local EWeaponVisibleReason = import "L10.Engine.EWeaponVisibleReason"
local Vector3=import "UnityEngine.Vector3"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CMainCamera = import "L10.Engine.CMainCamera"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local AICamera = import "L10.Engine.CameraControl.AICamera"
local CRenderObject = import "L10.Engine.CRenderObject"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local Quaternion = import "UnityEngine.Quaternion"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local SoundManager = import "SoundManager"
local AdditionalLights=import "AdditionalLights"
local WeatherManager=import "CTT3.Weather.WeatherManager"
local ResLoaderKit = import "Leihuo.Timeline.ResLoaderKit"
local TimelineKit = import "Leihuo.Timeline.TimelineKit"
local TimeControllerModel = import "Leihuo.Timeline.TimeControllerModel"
local CLoadingWnd = import "L10.UI.CLoadingWnd"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CameraClearFlags = import "UnityEngine.CameraClearFlags"
local CDeepLinkMgr = import "L10.Game.CDeepLinkMgr"
local Setting = import "L10.Engine.Setting"
local UIPanel = import "UIPanel"
local Shader = import "UnityEngine.Shader"
local ShaderLodParams = import "L10.Game.ShaderLodParams"
local Profession = import "L10.Game.Profession"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CSharpResourceLoader = import "L10.Game.CSharpResourceLoader"
local CObjectFX = import "L10.Game.CObjectFX"
local Screen = import "UnityEngine.Screen"
local CClientVersion = import "L10.Engine.CClientVersion"
local SkinnedMeshRenderer = import "UnityEngine.SkinnedMeshRenderer"
local MeshRenderer = import "UnityEngine.MeshRenderer"
local Material = import "UnityEngine.Material"

LuaCharacterCreation2023Wnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCharacterCreation2023Wnd, "BackButton", "BackButton", GameObject)
RegistChildComponent(LuaCharacterCreation2023Wnd, "CreatingRole", "CreatingRole", GameObject)
RegistChildComponent(LuaCharacterCreation2023Wnd, "ExistingRole", "ExistingRole", Animation)
RegistChildComponent(LuaCharacterCreation2023Wnd, "JobTableView", "JobTableView", QnTableView)
RegistChildComponent(LuaCharacterCreation2023Wnd, "IndicatorTable", "IndicatorTable", UITable)
RegistChildComponent(LuaCharacterCreation2023Wnd, "Indicator", "Indicator", GameObject)
RegistChildComponent(LuaCharacterCreation2023Wnd, "MaleBtn", "MaleBtn", GameObject)
RegistChildComponent(LuaCharacterCreation2023Wnd, "FemaleBtn", "FemaleBtn", GameObject)
RegistChildComponent(LuaCharacterCreation2023Wnd, "XianShenBtn", "XianShenBtn", GameObject)
RegistChildComponent(LuaCharacterCreation2023Wnd, "FanShenBtn", "FanShenBtn", GameObject)
RegistChildComponent(LuaCharacterCreation2023Wnd, "DetailLabel", "DetailLabel", CUITexture)
RegistChildComponent(LuaCharacterCreation2023Wnd, "FeatureLabel", "FeatureLabel", CUITexture)
RegistChildComponent(LuaCharacterCreation2023Wnd, "ProfessionNameTexture", "ProfessionNameTexture", CUITexture)
RegistChildComponent(LuaCharacterCreation2023Wnd, "Radar", "Radar", GameObject)
RegistChildComponent(LuaCharacterCreation2023Wnd, "GoNextBtn", "GoNextBtn", GameObject)
RegistChildComponent(LuaCharacterCreation2023Wnd, "ExistingPortrait1", "ExistingPortrait1", GameObject)
RegistChildComponent(LuaCharacterCreation2023Wnd, "ExistingPortrait2", "ExistingPortrait2", GameObject)
RegistChildComponent(LuaCharacterCreation2023Wnd, "ExistingPortrait3", "ExistingPortrait3", GameObject)
RegistChildComponent(LuaCharacterCreation2023Wnd, "ExistingPortrait4", "ExistingPortrait4", GameObject)
RegistChildComponent(LuaCharacterCreation2023Wnd, "EnterButton", "EnterButton", CButton)
RegistChildComponent(LuaCharacterCreation2023Wnd, "ExProfession", "ExProfession", CUITexture)
RegistChildComponent(LuaCharacterCreation2023Wnd, "ExLevelLabel", "ExLevelLabel", UILabel)
RegistChildComponent(LuaCharacterCreation2023Wnd, "ExNameLabel", "ExNameLabel", UILabel)
RegistChildComponent(LuaCharacterCreation2023Wnd, "ExistingRoleInfo", "ExistingRoleInfo", GameObject)
RegistChildComponent(LuaCharacterCreation2023Wnd, "TeamAppointmentButton", "TeamAppointmentButton", GameObject)
RegistChildComponent(LuaCharacterCreation2023Wnd, "OpenTimeLabel", "OpenTimeLabel", UILabel)
RegistChildComponent(LuaCharacterCreation2023Wnd, "Texture_baiping", "Texture_baiping", GameObject)
RegistChildComponent(LuaCharacterCreation2023Wnd, "JumpTips", "JumpTips", GameObject)
RegistChildComponent(LuaCharacterCreation2023Wnd, "JumpControlTex", "JumpControlTex", GameObject)
RegistChildComponent(LuaCharacterCreation2023Wnd, "ExsitingPlayerBGTexture", "ExsitingPlayerBGTexture", UITexture)
RegistChildComponent(LuaCharacterCreation2023Wnd, "PageContainer", "PageContainer", GameObject)
RegistChildComponent(LuaCharacterCreation2023Wnd, "PreviousBtn", "PreviousBtn", CButton)
RegistChildComponent(LuaCharacterCreation2023Wnd, "NextBtn", "NextBtn", CButton)
RegistChildComponent(LuaCharacterCreation2023Wnd, "PageLabel", "PageLabel", UILabel)
RegistChildComponent(LuaCharacterCreation2023Wnd, "GenderBtn", "GenderBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaCharacterCreation2023Wnd,"m_SelectedClass")
RegistClassMember(LuaCharacterCreation2023Wnd,"m_JobList")
RegistClassMember(LuaCharacterCreation2023Wnd,"m_CurClass")
RegistClassMember(LuaCharacterCreation2023Wnd,"m_CurGender")
RegistClassMember(LuaCharacterCreation2023Wnd, "m_RadarMapTexture")
RegistClassMember(LuaCharacterCreation2023Wnd, "m_FanShenHighlight")
RegistClassMember(LuaCharacterCreation2023Wnd, "m_XianShenHighlight")
RegistClassMember(LuaCharacterCreation2023Wnd, "m_MaleHighlight")
RegistClassMember(LuaCharacterCreation2023Wnd, "m_FemaleHighlight")

RegistClassMember(LuaCharacterCreation2023Wnd, "m_ExsitingPlayerRos")
RegistClassMember(LuaCharacterCreation2023Wnd, "m_LastSceneName")
RegistClassMember(LuaCharacterCreation2023Wnd, "m_AdditionalLights")
RegistClassMember(LuaCharacterCreation2023Wnd, "m_JumpButtomActive")
RegistClassMember(LuaCharacterCreation2023Wnd, "m_JumpToTime")
RegistClassMember(LuaCharacterCreation2023Wnd, "m_DeleteBtn")
RegistClassMember(LuaCharacterCreation2023Wnd, "m_LightRo")
RegistClassMember(LuaCharacterCreation2023Wnd, "m_CharacterLoadedTbl")

function LuaCharacterCreation2023Wnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.BackButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBackButtonClick()
	end)

	UIEventListener.Get(self.XianShenBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnXianShenBtnClick()
	end)
	
	UIEventListener.Get(self.FanShenBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFanShenBtnClick()
	end)
	
	UIEventListener.Get(self.GoNextBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGoNextBtnClick()
	end)

	UIEventListener.Get(self.EnterButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterButtonClick()
	end)

	UIEventListener.Get(self.GenderBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		if self.m_IsFemale then
			self:OnMaleBtnClick()
		else
			self:OnFemaleBtnClick()
		end
	end)

    --@endregion EventBind end
	UIEventListener.Get(self.JumpControlTex).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJumpControlClick()
	end)
	UIEventListener.Get(self.JumpTips).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJumpTipsClick()
	end)
	UIEventListener.Get(self.ExsitingPlayerBGTexture.gameObject).onDrag = LuaUtils.VectorDelegate(function(go, delta)
		self:OnSwipe(delta)
	end)
	UIEventListener.Get(self.PreviousBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPreviousBtnClick()
	end)
	UIEventListener.Get(self.NextBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnNextBtnClick()
	end)

	self.m_RadarMapTexture = self.Radar.transform:Find("Texture"):GetComponent(typeof(CUITexture))
	self.m_FanShenHighlight = self.FanShenBtn.transform:Find("Highlight"):GetComponent(typeof(Animation))
	self.m_XianShenHighlight = self.XianShenBtn.transform:Find("Highlight"):GetComponent(typeof(Animation))
	self.m_MaleHighlight = self.MaleBtn.transform:Find("Highlight"):GetComponent(typeof(Animation))
	self.m_FemaleHighlight = self.FemaleBtn.transform:Find("Highlight"):GetComponent(typeof(Animation))
	self.m_CreatingRoleAnimation = self.CreatingRole.transform:GetComponent(typeof(Animation))
	self.m_ExsitingPlayerRos = {}
	self.m_HideSmrs = {}
	self.m_HideMrs = {}
	self.m_CharacterLoadedTbl = {}
	self.ExistingRole.gameObject:SetActive(false)
	self.CreatingRole:SetActive(false)
	self.JumpTips:SetActive(false)
	self.m_DeleteBtn = self.ExistingRoleInfo.transform:Find("DeleteBtn")
	if self.m_DeleteBtn then
		UIEventListener.Get(self.m_DeleteBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnDeleteBtnClick()
		end)
	end
	self.m_OriginOpenTimeLabelPos = self.OpenTimeLabel.transform.localPosition

	-- 更新lastServerVersion
    if PlayerSettings.LastClientVersion ~= CClientVersion.g_VersionInfo then
        PlayerSettings.AppStoreReviewed = 1
        PlayerSettings.LastClientVersion = CClientVersion.g_VersionInfo
    end
	if CLoginMgr.Inst.m_ExistingPlayerList.Count == 0 and IsQYCodeUseOpen() then
        local message = g_MessageMgr:FormatMessage("QYCODE_USE_CONFIRM")
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
            self:ShowQYCodeInputWnd()
        end), nil, nil, nil, false)
    end
end
function LuaCharacterCreation2023Wnd:OnQYCodeUseButtonClick(go)
    self:ShowQYCodeInputWnd()
end

function LuaCharacterCreation2023Wnd:ShowQYCodeInputWnd()
    CUIManager.ShowUI(CLuaUIResources.QYCodeInputBoxWnd)
end

function LuaCharacterCreation2023Wnd:Start()
	CUIManager.CloseUI("LoginQueueWnd")
end

function LuaCharacterCreation2023Wnd:OnEnable()
	if not self.m_AdditionalLights then
		self.m_AdditionalLights = AdditionalLights.Create()
	end
	g_ScriptEvent:AddListener("CutsceneSlowDownStill", self, "OnCutsceneSlowDownStill")
	g_ScriptEvent:AddListener("CutsceneActorReady", self, "OnCutsceneActorReady")

	g_ScriptEvent:AddListener("OnExistingRoleInfoUpdate", self, "OnExistingRoleInfoUpdate")
    g_ScriptEvent:AddListener("LoginSceneLoaded", self, "LoginSceneLoaded")
    g_ScriptEvent:AddListener("StartUnloadScene", self, "OnStartUnloadScene")
    g_ScriptEvent:AddListener("ServerNotifyRetryLogin", self, "OnServerNotifyRetryLogin")
    g_ScriptEvent:AddListener("LoadingSceneFinished", self, "OnLoadingSceneFinished")
    g_ScriptEvent:AddListener("MsgFrom3DTouch", self, "On3DTouchTriggered")
    g_ScriptEvent:AddListener("OnLaunchAppFromDeepLink", self, "OnLaunchAppFromDeepLink")
    g_ScriptEvent:AddListener("QMPKCharacterNameExist", self, "QMPKCharacterNameExist")
    g_ScriptEvent:AddListener("EnterPreLoginMode", self, "OnEnterPreLoginMode")
    g_ScriptEvent:AddListener("PreLoginModeCharacterExists", self, "OnPreLoginModeCharacterExists")
    g_ScriptEvent:AddListener("BeginCreatePlayer", self, "BeginCreatePlayer")
	g_ScriptEvent:AddListener("AppFocusChange", self, "OnApplicationFocus")
	g_ScriptEvent:AddListener("SendQueueStatus", self, "OnSendQueueStatus")

	CLuaPlayerSettings.SetShaderLod()
	if CLoginMgr.Inst.PreLoginMode then
        self:OnEnterPreLoginMode()
    end
end
function LuaCharacterCreation2023Wnd:OnDisable()
	if self.m_AdditionalLights then
		GameObject.Destroy(self.m_AdditionalLights.gameObject)
		self.m_AdditionalLights = nil
	end
	if self.LightGo then
		GameObject.Destroy(self.LightGo)
		self.LightGo = nil
	end
	g_ScriptEvent:RemoveListener("CutsceneSlowDownStill", self, "OnCutsceneSlowDownStill")
	g_ScriptEvent:RemoveListener("CutsceneActorReady", self, "OnCutsceneActorReady")

	g_ScriptEvent:RemoveListener("OnExistingRoleInfoUpdate", self, "OnExistingRoleInfoUpdate")
    g_ScriptEvent:RemoveListener("LoginSceneLoaded", self, "LoginSceneLoaded")
    g_ScriptEvent:RemoveListener("StartUnloadScene", self, "OnStartUnloadScene")
    g_ScriptEvent:RemoveListener("ServerNotifyRetryLogin", self, "OnServerNotifyRetryLogin")
    g_ScriptEvent:RemoveListener("LoadingSceneFinished", self, "OnLoadingSceneFinished")
    g_ScriptEvent:RemoveListener("MsgFrom3DTouch", self, "On3DTouchTriggered")
    g_ScriptEvent:RemoveListener("OnLaunchAppFromDeepLink", self, "OnLaunchAppFromDeepLink")
    g_ScriptEvent:RemoveListener("QMPKCharacterNameExist", self, "QMPKCharacterNameExist")
    g_ScriptEvent:RemoveListener("EnterPreLoginMode", self, "OnEnterPreLoginMode")
    g_ScriptEvent:RemoveListener("PreLoginModeCharacterExists", self, "OnPreLoginModeCharacterExists")
    g_ScriptEvent:RemoveListener("BeginCreatePlayer", self, "BeginCreatePlayer")
	g_ScriptEvent:RemoveListener("AppFocusChange", self, "OnApplicationFocus")
	g_ScriptEvent:RemoveListener("SendQueueStatus", self, "OnSendQueueStatus")

	if self.m_HideCreatingTick then
		UnRegisterTick(self.m_HideCreatingTick)
		self.m_HideCreatingTick = nil
	end
	if self.m_BaoBaiTick then
		UnRegisterTick(self.m_BaoBaiTick)
		self.m_BaoBaiTick = nil
	end
	if self.m_ChangeViewTick then
		UnRegisterTick(self.m_ChangeViewTick)
		self.m_ChangeViewTick = nil
	end
	self:CancelYingLingRadarMapTick()
	self:CancelShowWndTick()
	if Shader.globalMaximumLOD == 800 then
		Shader.globalMaximumLOD = ShaderLodParams.SHADER_LOD_HIGH_LEVEL
	end
	LuaCharacterCreation2023Mgr.m_LastSceneName = nil
end

function LuaCharacterCreation2023Wnd:OnSwipe(delta)
	local deltaVal= - delta.x / Screen.width * 360
	self:RotateRoleModel(deltaVal)
end

function LuaCharacterCreation2023Wnd:RotateRoleModel(degree)
	local newEulerAngleY = (self.m_RO.transform.localEulerAngles.y + degree + 180) % 360 - 180
    if newEulerAngleY ~= self.m_RO.transform.transform.localEulerAngles.y then
		self.m_RO.transform.localEulerAngles = Vector3(0, newEulerAngleY, 0)
	end
end

function LuaCharacterCreation2023Wnd:OnDestroy()
	if LuaCharacterCreation2023Mgr.BGM then
		SoundManager.Inst:StopSound(LuaCharacterCreation2023Mgr.BGM)
		LuaCharacterCreation2023Mgr.BGM = nil
	end
	AMPlayer.Inst.isChuangJue = false
	AMPlayer.Inst:CloseTimeline()
	AMPlayer.Inst:ClearLastCJRoList()

	self:DestoryRO()
	SoundManager.s_BlockBgMusic = false
	LuaCharacterCreation2023Mgr:ResetIsOperating()
end
function LuaCharacterCreation2023Wnd:Init()
	SoundManager.Inst:StopBGMusic()
	self.m_CurClass = LuaCharacterCreation2023Mgr.m_SelectedClassForCutscene and LuaCharacterCreation2023Mgr.m_SelectedClassForCutscene or 1
	self.m_CurGender = math.random(0,1)
	local enumClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), self.m_CurClass)
	if enumClass == EnumClass.ZhanKuang then
		if self.m_IsFemale then
			self.m_IsFemale = false

		end
		self.m_CurGender = 0
	end
	if LuaCharacterCreation2023Mgr.m_SelectedGenderForCutscene then
		self.m_CurGender = LuaCharacterCreation2023Mgr.m_SelectedGenderForCutscene
		LuaCharacterCreation2023Mgr.m_SelectedGenderForCutscene = nil	
	end
	if not self:IsLoginExistingCharacter() or LuaCharacterCreation2023Mgr.m_NeedGoStill or LuaCharacterCreation2023Mgr.ForceOpenDengzhongshijie then
		self.Texture_baiping:SetActive(true)
		self:InitCreatingView(true)
		LuaPinchFaceTimeCtrlMgr:OnOpenWnd()
		LuaCharacterCreation2023Mgr:ResetIsOperating()
	else
		self:InitExitingView(true)
	end
	LuaLoginMgr.TryConfirmShowDaShenAIDetectUrl()
	Gac2Login.EnterCreatingPlayerDone("")

    self:HandleAutoLoginByCmdLineArgs()
end

function LuaCharacterCreation2023Wnd:HandleAutoLoginByCmdLineArgs()
    if CLoginMgr.Inst:NeedAutoLoginByCmdLineArgs() then
        if  CLoginMgr.Inst.m_ExistingPlayerList.Count == 0 then
            --新建角色时先经过一层创角再自动登录
            self:OnGoNextBtnClick()
        else
            CLoginMgr.Inst:ClearAutoLoginByCmdLineArgs()
            local list = CLoginMgr.Inst.m_ExistingPlayerList
            local bFound = false
            for i=0,list.Count-1 do
                if CLoginMgr.Inst.m_LastLoginPlayerId == list[i].m_BasicProp.Id then
                    self.m_LoginPlayerIdx = i
                    bFound = true
                    break
                end
            end
            if not bFound then
                self.m_LoginPlayerIdx = 0 --如果服务器没有返回上次登录角色，默认登录第一个
            end
            self:OnEnterButtonClick()
        end
    end
end

function LuaCharacterCreation2023Wnd:IsLoginExistingCharacter()
	return (CLoginMgr.Inst.m_ExistingPlayerList.Count > 0)
end

function LuaCharacterCreation2023Wnd:InitCreatingView(isFirst)
	CMainCamera.Inst:SetCameraEnableStatus(false, "Enter_ChuangJue", false)
	CUIManager.CloseUI(CUIResources.BeforeEnterDengZhongShiJieWnd)
	--self.Texture_baiping.gameObject:SetActive(false)
	self.JumpControlTex:SetActive(true)
	self.ExistingRole.gameObject:SetActive(false)
	self.CreatingRole:SetActive(isFirst and false or true)
	self.m_JobList = {}
    self.m_JobCount = 0
    Initialization_Init.Foreach(function(k,v)
        local class = math.floor(k/100)
        if not self.m_JobList[class]  then
            self.m_JobList[class] = v.ID
            self.m_JobCount = self.m_JobCount + 1
        end     
    end)

	self:InitIndicators()

	self.JobTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self.m_JobCount
        end,
        function(item,row)
            self:InitJobItem(item,row)
        end)

    self.JobTableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
		self:OnClickJobBtn(row+1)
    end)
	self.JobTableView:ReloadData(false, true)
	--self.Indicator:Layout()
	--button
	self.m_FemaleHighlight.gameObject:SetActive(self.m_CurGender==1)
	self.m_MaleHighlight.gameObject:SetActive(self.m_CurGender==0)
	self.m_IsFemale = self.m_CurGender==1
	self.m_MaleHighlight:Play("highlight_show_ct")
	self.m_XianShenHighlight.gameObject:SetActive(false)
	self.m_FanShenHighlight.gameObject:SetActive(true)
	self.m_JumpButtomActive = false
	self.JumpTips:SetActive(false)

	--播默认动画
	self.JobTableView:SetSelectRow(self.m_CurClass-1,true)

	--创建角色的时候设置一下配置
	PlayerSettings.LoadPerformanceSettingsByDeviceLevel("cj")
end

function LuaCharacterCreation2023Wnd:InitIndicators()
	Extensions.RemoveAllChildren(self.IndicatorTable.transform)
	self.Indicator:SetActive(false)
	for i=0,self.m_JobCount-1,1 do
		local g = NGUITools.AddChild(self.IndicatorTable.gameObject,self.Indicator)
        g:SetActive(true)
	end
	self.IndicatorTable:Reposition()
end

function LuaCharacterCreation2023Wnd:InitJobItem(item,row)
	local icon = item.transform:Find("Icon_normal"):GetComponent(typeof(CUITexture))
	local iconHighlight = item.transform:Find("Icon_highlight"):GetComponent(typeof(CUITexture))
	local name = item.transform:Find("name"):GetComponent(typeof(UILabel))
	iconHighlight.gameObject:SetActive(false)
	icon.gameObject:SetActive(true)

	local class = row+1
	local id = self.m_JobList[class]
	local data = Initialization_Init.GetData(id)
	if data then
		icon:LoadMaterial(data.CreationNewLargeIcon)
		iconHighlight:LoadMaterial(data.LargeIconHighlight)
		name.text = data.ClassFullName
	else
		name.text = nil
	end
end
--@region UIEvent
function LuaCharacterCreation2023Wnd:CancelShowWndTick()
	if self.m_ShowWndTick then
		UnRegisterTick(self.m_ShowWndTick)
		self.m_ShowWndTick = nil
		if self.EnterButton.Enabled then
			CLoadingWnd.Inst:HideLoadingView()
		end
	end
end
function LuaCharacterCreation2023Wnd:OnBackButtonClick()
	if self.m_ShenBingTick then
		UnRegisterTick(self.m_ShenBingTick)
		self.m_ShenBingTick = nil
	end

	if not self:IsLoginExistingCharacter() then
		LuaCharacterCreation2023Mgr:ShowCrossingFx()
		CMainCamera.Inst:SetCameraClearFlags(CameraClearFlags.SolidColor)
		
		self:CancelShowWndTick()
		self.m_ShowWndTick = RegisterTickOnce(function()
			CUIManager.CloseUI(CUIResources.CharacterCreation2023Wnd)
			CUIManager.ShowUI(CUIResources.BeforeEnterDengZhongShiJieWnd)
			LuaCharacterCreation2023Mgr:HideCrossingFx()
		end,1000)
	elseif LuaCharacterCreation2023Mgr.ForceOpenDengzhongshijie then
		CMainCamera.Inst:SetCameraClearFlags(CameraClearFlags.SolidColor)
		CUIManager.ShowUI(CUIResources.BeforeEnterDengZhongShiJieWnd)
		CUIManager.CloseUI(CUIResources.CharacterCreation2023Wnd)
	elseif not self.ExistingRole.gameObject.activeSelf then
		LuaCharacterCreation2023Mgr:ShowCrossingFx()
		CMainCamera.Inst:SetCameraClearFlags(CameraClearFlags.SolidColor)
		
		self:CancelShowWndTick()
		self.m_ShowWndTick = RegisterTickOnce(function()
			self:OnDestroy()
			self:InitExitingView(false)
			LuaCharacterCreation2023Mgr:HideCrossingFx()
		end,1000)
	else
		LuaCharacterCreation2023Mgr:ShowCrossingFx()
		LuaPinchFaceTimeCtrlMgr:OnBackToLoginScene()
		LuaLoginMgr:HideQueueStatusTopNotice()
		self:CancelShowWndTick()
		self.m_ShowWndTick = RegisterTickOnce(function()
			CUIManager.ShowUIGroup(CUIManager.EUIModuleGroup.EServerWndUI)
			CUIManager.CloseUI(CUIResources.CharacterCreation2023Wnd)
			LuaCharacterCreation2023Mgr:HideCrossingFx()
			CLoginMgr.Inst:Disconnect()
			CMainCamera.Inst:SetCameraEnableStatus(true, "Enter_ChuangJue", false)
		end,1000)
	end
end

function LuaCharacterCreation2023Wnd:OnMaleBtnClick()
	LuaCharacterCreation2023Mgr:SetPlayerOperating()
	if self.m_ShenBingTick then
		UnRegisterTick(self.m_ShenBingTick)
		self.m_ShenBingTick = nil
	end
	self.m_IsFemale = false
	self.m_FemaleHighlight.gameObject:SetActive(false)
	self.m_MaleHighlight.gameObject:SetActive(true)
	local maxdep = math.max(self.MaleBtn.transform:GetComponent(typeof(UISprite)).depth,self.FemaleBtn.transform:GetComponent(typeof(UISprite)).depth)
	local mindep = math.min(self.MaleBtn.transform:GetComponent(typeof(UISprite)).depth,self.FemaleBtn.transform:GetComponent(typeof(UISprite)).depth)
	self.MaleBtn.transform:GetComponent(typeof(UISprite)).depth = maxdep
	self.FemaleBtn.transform:GetComponent(typeof(UISprite)).depth = mindep
	
	maxdep = math.max(self.m_MaleHighlight.transform:GetComponent(typeof(UISprite)).depth,self.m_FemaleHighlight.transform:GetComponent(typeof(UISprite)).depth)
	mindep = math.min(self.m_MaleHighlight.transform:GetComponent(typeof(UISprite)).depth,self.m_FemaleHighlight.transform:GetComponent(typeof(UISprite)).depth)
	self.m_MaleHighlight.transform:GetComponent(typeof(UISprite)).depth = maxdep
	self.m_FemaleHighlight.transform:GetComponent(typeof(UISprite)).depth = mindep
	
	local maleIcon = self.MaleBtn.transform:Find("Icon"):GetComponent(typeof(UILabel))
	local femaleIcon = self.FemaleBtn.transform:Find("Icon"):GetComponent(typeof(UILabel))
	maxdep = math.max(maleIcon.depth,femaleIcon.depth)
	mindep = math.min(maleIcon.depth,femaleIcon.depth)
	maleIcon.depth = maxdep
	femaleIcon.depth = mindep
	self.m_MaleHighlight:Play("highlight_show")
	
	local class = self.m_CurClass
	local id = self.m_JobList[class]
	id = id + (self.m_IsFemale and 1 or 0)
	local data = Initialization_ChuangJue2023.GetData(id)

	if data then
		local timelineName = data.TimelineName
		local startPos = data.CutInStartPos
		self:SetCreatingRoleActive(false)
		--默认女
		if LuaCharacterCreation2023Mgr.mEnterClassGender == 1 then
			local posdata = data.StartPosWhenFemale
			if posdata and posdata.Length >=3 then
				startPos = posdata[1*2] * (1/data.FrameRate)
				self.m_JumpToTime = posdata[1*2+1] * (1/data.FrameRate)
			end	
		else
			local posdata = data.StartPosWhenMale
			if posdata and posdata.Length >=6 then
				startPos = posdata[2*2] * (1/data.FrameRate)
				self.m_JumpToTime = posdata[2*2+1] * (1/data.FrameRate)
			end	
		end
		LuaCharacterCreation2023Mgr:PlayTimeline(timelineName,startPos,id,self.m_IsFemale,self.m_CurClass)--cj_nandk_1
		LuaCharacterCreation2023Mgr.m_CanJump = true
	end
	
end

function LuaCharacterCreation2023Wnd:OnFemaleBtnClick()
	local enumClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), self.m_CurClass)
	if enumClass == EnumClass.ZhanKuang then
		return
	end

	LuaCharacterCreation2023Mgr:SetPlayerOperating()
	if self.m_ShenBingTick then
		UnRegisterTick(self.m_ShenBingTick)
		self.m_ShenBingTick = nil
	end
	self.m_IsFemale = true
	self.m_FemaleHighlight.gameObject:SetActive(true)
	self.m_MaleHighlight.gameObject:SetActive(false)
	local maxdep = math.max(self.MaleBtn.transform:GetComponent(typeof(UISprite)).depth,self.FemaleBtn.transform:GetComponent(typeof(UISprite)).depth)
	local mindep = math.min(self.MaleBtn.transform:GetComponent(typeof(UISprite)).depth,self.FemaleBtn.transform:GetComponent(typeof(UISprite)).depth)
	self.FemaleBtn.transform:GetComponent(typeof(UISprite)).depth = maxdep
	self.MaleBtn.transform:GetComponent(typeof(UISprite)).depth = mindep
	maxdep = math.max(self.m_MaleHighlight.transform:GetComponent(typeof(UISprite)).depth,self.m_FemaleHighlight.transform:GetComponent(typeof(UISprite)).depth)
	mindep = math.min(self.m_MaleHighlight.transform:GetComponent(typeof(UISprite)).depth,self.m_FemaleHighlight.transform:GetComponent(typeof(UISprite)).depth)
	self.m_MaleHighlight.transform:GetComponent(typeof(UISprite)).depth = mindep
	self.m_FemaleHighlight.transform:GetComponent(typeof(UISprite)).depth = maxdep
	
	local maleIcon = self.MaleBtn.transform:Find("Icon"):GetComponent(typeof(UILabel))
	local femaleIcon = self.FemaleBtn.transform:Find("Icon"):GetComponent(typeof(UILabel))
	maxdep = math.max(maleIcon.depth,femaleIcon.depth)
	mindep = math.min(maleIcon.depth,femaleIcon.depth)
	maleIcon.depth = mindep
	femaleIcon.depth = maxdep
	self.m_FemaleHighlight:Play("highlight_show")

	local class = self.m_CurClass
	local id = self.m_JobList[class]
	id = id + (self.m_IsFemale and 1 or 0)

	local data = Initialization_ChuangJue2023.GetData(id)
	if data then
		local timelineName = data.TimelineName
		local startPos = data.CutInStartPos
		self:SetCreatingRoleActive(false)

		--默认女
		if LuaCharacterCreation2023Mgr.mEnterClassGender == 1 then
			local posdata = data.StartPosWhenFemale
			if posdata and posdata.Length >=6 then
				startPos = posdata[2*2] * (1/data.FrameRate)
				self.m_JumpToTime = posdata[2*2+1] * (1/data.FrameRate)
			end	
		else
			local posdata = data.StartPosWhenMale
			if posdata and posdata.Length >=6 then
				startPos = posdata[1*2] * (1/data.FrameRate)
				self.m_JumpToTime = posdata[1*2+1] * (1/data.FrameRate)
			end	
		end
		
		LuaCharacterCreation2023Mgr:PlayTimeline(timelineName,startPos,id,self.m_IsFemale,self.m_CurClass)--cj_nandk_1
		LuaCharacterCreation2023Mgr.m_CanJump = true
	end
end

function LuaCharacterCreation2023Wnd:OnXianShenBtnClick()
	self.m_XianShenHighlight.gameObject:SetActive(true)
	self.m_FanShenHighlight.gameObject:SetActive(false)
	self.m_XianShenHighlight:Play("highlight_show")
	self:SetPlayerXianShenStatus(true)
	LuaCharacterCreation2023Mgr:SetPlayerOperating()
end

function LuaCharacterCreation2023Wnd:OnFanShenBtnClick()
	self.m_XianShenHighlight.gameObject:SetActive(false)
	self.m_FanShenHighlight.gameObject:SetActive(true)
	self.m_FanShenHighlight:Play("highlight_show")
	self:SetPlayerXianShenStatus(false)
	LuaCharacterCreation2023Mgr:SetPlayerOperating()
end

function LuaCharacterCreation2023Wnd:SetPlayerXianShenStatus(isXianShen)
	if not AMPlayer.Inst then
		return
	end
	
	for i=0,AMPlayer.Inst.mCJPlayers.Count-1,1 do
		local player = AMPlayer.Inst.mCJPlayers[i]
		if player.gameObject.activeSelf and player == AMPlayer.Inst.CurrentCjPlayerRo then
			local appearance = AMPlayer.Inst.mCJAppearances[i]
			appearance.FeiShengAppearanceXianFanStatus = isXianShen and 1 or 0

			local shenBingFirstModelId = nil
			local shenBingSecondModelId = nil
			local shenBingTrainLvFxId = nil
			local shenBingRanSeData = nil

			local gender = self.m_IsFemale and 1 or 0
			local id = self.m_CurClass * 100 + gender
			local data = Initialization_Init.GetData(id)
			if not data then
				return
			end
			--procss showhide
			if not AMPlayer.Inst.isXianShenChuangJue and not next(self.m_HideSmrs) and not next(self.m_HideMrs) then
				local smrs = CommonDefs.GetComponentsInChildren_Component_Type_Boolean(player.transform, typeof(SkinnedMeshRenderer),true)
				local mrs = CommonDefs.GetComponentsInChildren_Component_Type_Boolean(player.transform, typeof(MeshRenderer),true)
				self.m_HideSmrs = {}
				self.m_HideMrs = {}
				if smrs then
					for i=0,smrs.Length-1 do
						if not smrs[i].enabled then
							self.m_HideSmrs[smrs[i].name] = true
						end
					end
				end
				if mrs then
					for i= 0,mrs.Length-1 do
						if not mrs[i].enabled then
							self.m_HideMrs[mrs[i].name] = true
						end
					end
				end
			end
			

			if isXianShen then
				self.m_CurrentEquipInfos, shenBingFirstModelId, shenBingSecondModelId, shenBingTrainLvFxId, shenBingRanSeData = self:GetShenBingEquipInfo(data, true)
			else
				self.m_CurrentEquipInfos = data.NewRoleEquip2023
			end
			appearance.Equipment = self.m_CurrentEquipInfos
			appearance.ShenBingFirstModelId = shenBingFirstModelId
			appearance.ShenBingSecondModelId = shenBingSecondModelId
			appearance.ShenBingTrainLvFxId = shenBingTrainLvFxId
			appearance.ShenBingRanSeData = shenBingRanSeData
			--fx
			if player:ContainFx("ChuangJueXianFanSwich") then
				player:RemoveFX("ChuangJueXianFanSwich")
			end
			
			local fxId = self.m_IsFemale and 88804864 or 88804865
			local fx = CEffectMgr.Inst:AddObjectFX(fxId, player, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil,true)--0.1.1
			player:AddFX("ChuangJueXianFanSwich", fx)
			player.s_IsApplyModelDNA = false
			player.m_onAllLoadFinished = nil
			

			CClientMainPlayer.LoadResource(player, appearance, false, 1, 0, false, 0, false, 0, false, nil, false)
			player:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(renderObject)
				local smrs2 = CommonDefs.GetComponentsInChildren_Component_Type_Boolean(renderObject.transform, typeof(SkinnedMeshRenderer),true)
				local mrs2 = CommonDefs.GetComponentsInChildren_Component_Type_Boolean(renderObject.transform, typeof(MeshRenderer),true)
				if smrs2 then
					for i=0,smrs2.Length-1 do
						if self.m_HideSmrs[smrs2[i].name] then
							smrs2[i].enabled = false
						end
						if isXianShen then
							local mats = smrs2[i].sharedMaterials
							local mats2 = {}
							if mats then
								for k=0,mats.Length-1,1 do
									local mat = mats[k]
									if mat and mat:IsKeywordEnabled("_USE_OLD_LIGHT") then									
										local ma = Material(mat)
										ma:DisableKeyword("_USE_OLD_LIGHT")
										table.insert(mats2,ma)
									else
										table.insert(mats2,mat)
									end
								end
								smrs2[i].sharedMaterials = Table2Array(mats2,MakeArrayClass(Material))
							end
						end					
					end
				end
				if mrs2 then
					for i= 0,mrs2.Length-1 do
						if self.m_HideMrs[mrs2[i].name] then
							mrs2[i].enabled = false
						end
						if isXianShen then
							local mats = mrs2[i].sharedMaterials
							local mats2 = {}
							if mats then
								for k=0,mats.Length-1,1 do
									local mat = mats[k]
									if mat and mat:IsKeywordEnabled("_USE_OLD_LIGHT") then									
										local ma = Material(mat)
										ma:DisableKeyword("_USE_OLD_LIGHT")
										table.insert(mats2,ma)
									else
										table.insert(mats2,mat)
									end
								end
								mrs2[i].sharedMaterials = Table2Array(mats2,MakeArrayClass(Material))
							end
						end
					end
				end
			end))

			local replacePath = nil
			if isXianShen then
				replacePath = data.CjPreviewNpc_Xian
			else
				replacePath = data.CjPreviewNpc_Fan
			end
			if AMPlayer.s_OpenCjPlayerReplace and replacePath and replacePath ~= "" then
				player:LoadMain(replacePath,nil,false,false,false)
			end

			player:SetWeaponVisible(EWeaponVisibleReason.AMPlayer, false)
			if self.m_ShenBingTick then
				UnRegisterTick(self.m_ShenBingTick)
				self.m_ShenBingTick = nil
			end
			self.m_ShenBingTick = RegisterTickOnce(function()
				if player and player.transform then
					local objfx = CommonDefs.GetComponentsInChildren_Component_Type_Boolean(player.transform, typeof(CObjectFX),true)
					if objfx then
						do
							local i = 0
							while i < objfx.Length do
								if string.find(objfx[i].transform.name,"shenbingyifuwaiguan") then
									objfx[i]:SetPaused(true)				
								end			
								i = i + 1
							end
						end
					end
				end
			end,1000)
		end		
	end
	--特殊处理魅者扇子
	local meizheShanzi = nil
	local actor = ResLoaderKit.Instance:GetAssetItem("lnpc1087_01__1")
	if actor and self.m_IsFemale then
		local shanziRo = actor.gameObject.transform:GetComponent(typeof(CRenderObject))
		if shanziRo then
			local resName = isXianShen and "lnpc1087_02" or "lnpc1087_01"
			local path = SafeStringFormat3("Assets/Res/Character/NPC/lnpc1087/Prefab/%s.prefab",resName)
			shanziRo:LoadMain(path,nil,false,false,false)
		end
	end
	AMPlayer.Inst.isXianShenChuangJue = isXianShen
end

function LuaCharacterCreation2023Wnd:GetShenBingEquipInfo(initialization_data, xianshenSelected)
	local curEquipInfos = initialization_data.NewRoleEquip2023
    if xianshenSelected and initialization_data.ShenBingWaiGuan then
        local equipInfos = {}
        local shenBingFirstModelId = {}
        local shenBingSecondModelId = {}
        local shenBingTrainLvFxId = {}
        local shenBingRanSeData = {}
		local enumClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), self.m_CurClass)
        for i=0,initialization_data.ShenBingWaiGuan.Length-1 do
            local equipTemplateId = initialization_data.ShenBingWaiGuan[i][0]
            local model1Id = initialization_data.ShenBingWaiGuan[i][1]
            local model2Id = initialization_data.ShenBingWaiGuan[i][2]
            local fxId = 0
            table.insert(equipInfos, equipTemplateId)
            table.insert(shenBingFirstModelId, model1Id)
            table.insert(shenBingSecondModelId, model2Id)
            table.insert(shenBingTrainLvFxId, fxId)
			table.insert(shenBingRanSeData,0)
        end

        return Table2Array(equipInfos, MakeArrayClass(uint)), Table2Array(shenBingFirstModelId, MakeArrayClass(uint)), Table2Array(shenBingSecondModelId, MakeArrayClass(uint)),
                    Table2Array(shenBingTrainLvFxId, MakeArrayClass(uint)), Table2Array(shenBingRanSeData, MakeArrayClass(uint))
    end
    return curEquipInfos, nil ,nil, nil, nil
end


function LuaCharacterCreation2023Wnd:OnGoNextBtnClick()
	CMainCamera.Inst:SetCameraEnableStatus(true, "Enter_ChuangJue", false)
	if self.m_ShenBingTick then
		UnRegisterTick(self.m_ShenBingTick)
		self.m_ShenBingTick = nil
	end

	local enumClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), self.m_CurClass)
	local enumGender = self.m_IsFemale and EnumGender.Female or EnumGender.Male
	LuaPinchFaceMgr:ShowCreateWnd(enumClass, enumGender)
	LuaCharacterCreation2023Mgr.m_SelectedGenderForCutscene = self.m_IsFemale and 1 or 0
	LuaCharacterCreation2023Mgr.m_LastSceneName = nil
end


function LuaCharacterCreation2023Wnd:OnEnterButtonClick()
	CMainCamera.Inst:SetCameraEnableStatus(true, "Enter_ChuangJue", false)
	if self.m_ShenBingTick then
		UnRegisterTick(self.m_ShenBingTick)
		self.m_ShenBingTick = nil
	end

	local idx = self.m_LoginPlayerIdx
	local existingCharacterInfo = CLoginMgr.Inst.m_ExistingPlayerList[idx]
	local playerId = tonumber(existingCharacterInfo.m_BasicProp.Id)
	CLoginMgr.Inst:RequestLoginExistPlayer(playerId)
	EventManager.Broadcast(EnumEventType.PlayerLogin)
	CLoginMgr.Inst.IsCreateRole = false
	self:RegisterEnterGameForExistingRoleTick()
	self.EnterButton.Enabled = false
end


--@endregion UIEvent
function LuaCharacterCreation2023Wnd:OnJumpControlClick()
	--self.m_JumpButtomActive = not self.m_JumpButtomActive
	if not LuaCharacterCreation2023Mgr.m_CanJump then
		self.JumpTips:SetActive(false)
		return
	end
	self.JumpTips:SetActive(not self.JumpTips.activeSelf)
end
function LuaCharacterCreation2023Wnd:OnJumpTipsClick()
	if not TimelineKit.Instance or not TimelineKit.Instance.timelineControl or not TimelineKit.Instance.timelineControl.playableDirector then
		return
	end
	if not LuaCharacterCreation2023Mgr.m_CanJump then
		return
	end
	local class = self.m_CurClass
	local id = self.m_JobList[class]
	id = id + (self.m_IsFemale and 1 or 0)
	local data = Initialization_ChuangJue2023.GetData(id)
	if data then
		local timelineName = data.TimelineName
		LuaCharacterCreation2023Mgr:PlayTimeline(timelineName,self.m_JumpToTime-0.5,id,self.m_IsFemale,self.m_CurClass)--cj_nandk_1
	end
	self.JumpTips:SetActive(false)
	LuaCharacterCreation2023Mgr.m_CanJump = false
	LuaCharacterCreation2023Mgr:SetPlayerOperating()
end

function LuaCharacterCreation2023Wnd:InitCreatingJobDetail()
	local id = self.m_CurClass*100 + (self.m_IsFemale and 1 or 0)
	local data = Initialization_Init.GetData(id)
	if data then
		local path = data.NewProfessionTexture-- to do 策划表
		self.ProfessionNameTexture:LoadMaterial(path)
		self.ProfessionNameTexture.gameObject:SetActive(false)
		self.ProfessionNameTexture.gameObject:SetActive(true)
		self.ProfessionNameTexture.transform:GetComponent(typeof(Animation)):Play("creatingrole_professionname")

		local featurePath = data.NewFeatureTexture
		self.FeatureLabel:LoadMaterial(featurePath)

		local IntroductionPath = data.NewIntroductionTexture
		self.DetailLabel:LoadMaterial(IntroductionPath)

		local enumclass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), self.m_CurClass)
		if enumclass == EnumClass.YingLing then
			self.m_RadarMapTexture:Clear()
       		 self:RegisterYingLingRadarMapTick()
		else
			self:CancelYingLingRadarMapTick()
			if self.m_RadarMapTexture then
				self.m_RadarMapTexture.texture.alpha = 1
				self.m_RadarMapTexture:LoadMaterial(data.NewRadarMap)
			end
		end
	end
	local isXianShen = AMPlayer.Inst.isXianShenChuangJue
	self.m_XianShenHighlight.gameObject:SetActive(isXianShen)
	self.m_FanShenHighlight.gameObject:SetActive(not isXianShen)
	if isXianShen then
		self.m_XianShenHighlight:Play("highlight_show")
	else
		self.m_FanShenHighlight:Play("highlight_show")
	end
	
end

function LuaCharacterCreation2023Wnd:GetYingLingRandomRadarPath(gender)
	local class = EnumToInt(EnumClass.YingLing)
	local data = Initialization_Init.GetData(class*100 + gender)
	local path = data.NewRadarMap
	return path
end

function LuaCharacterCreation2023Wnd:RegisterYingLingRadarMapTick( ... )
    self:CancelYingLingRadarMapTick()
    local i = 0
    local updateFunc = function ()
        if self.m_RadarMapTexture ~= nil then
            self.m_RadarMapTexture:LoadMaterial(self:GetYingLingRandomRadarPath(i%3))
            i = i + 1
            LuaTweenUtils.DOKill(self.m_RadarMapTexture.transform, false)
            local fadeInTweener = LuaTweenUtils.TweenAlpha(self.m_RadarMapTexture.texture, self.m_RadarMapTexture.transform, 0,1,0.5)
            local fadeOutTweener = LuaTweenUtils.TweenAlpha(self.m_RadarMapTexture.texture, self.m_RadarMapTexture.transform,1,0,0.5)
            LuaTweenUtils.SetDelay(fadeOutTweener, 2)
        end
    end
    self.m_YingLingRadarMapTick = RegisterTick(function ( ... )
           updateFunc()
        end, 3000)
    updateFunc()--先执行一下
end

function LuaCharacterCreation2023Wnd:CancelYingLingRadarMapTick( ... )
    if self.m_YingLingRadarMapTick then
        UnRegisterTick(self.m_YingLingRadarMapTick)
        self.m_YingLingRadarMapTick = nil
        LuaTweenUtils.DOKill(self.m_RadarMapTexture.transform, false)
    end
end

function LuaCharacterCreation2023Wnd:OnClickJobBtn(class)
	self.m_CurClass = class
	self.m_CurBlendStartTime = nil
	local id = self.m_JobList[class]
	LuaCharacterCreation2023Mgr:SetPlayerOperating()
	self:InitCreatingJobDetail()
	local enumClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), self.m_CurClass)
	if enumClass == EnumClass.ZhanKuang then
		self.m_IsFemale = false
		self.m_CurGender = 0
		self.FemaleBtn:SetActive(false)
		self.m_FemaleHighlight.gameObject:SetActive(false)
		self.m_MaleHighlight.gameObject:SetActive(true)
	else
		self.FemaleBtn:SetActive(true)
	end
	LuaCharacterCreation2023Mgr.mEnterClassGender = self.m_IsFemale and 1 or 0
	local items = self.JobTableView.m_ItemList
	for i=0,items.Count-1,1 do
		local item = items[i]
		if (item) then
			item.transform:Find("Icon_highlight").gameObject:SetActive(class == i+1)
			item.transform:Find("Icon_normal").gameObject:SetActive(class ~= i+1)
		end
	end
	--Indicators
	for i=0,self.m_JobCount-1,1 do
		local g = self.IndicatorTable.transform:GetChild(i)
		if g then
			local sp = g:GetComponent(typeof(UISprite))
			if sp then
				if i == class-1 then
					sp.color = NGUIText.ParseColor24("ffffff", 0)
					sp.alpha = 0.5
				else
					sp.color = NGUIText.ParseColor24("000000", 0)
					sp.alpha = 0.4
				end
			end
		end
	end
	local cdata = Initialization_ChuangJue2023.GetData(id)
	if not LuaCharacterCreation2023Mgr.m_NeedGoStill then
		LuaCharacterCreation2023Mgr:ShowCrossingFx()
	end
	CRenderScene.Inst:ChangeAppendedSceneTo(cdata.SubScene, DelegateFactory.Action(function()
		--创角动画播放点位 默认男/女
		--战狂屏蔽女
		if cdata then
			local timelineName = cdata.TimelineName
			local startPos = cdata.DefaultStartPos
			self:SetCreatingRoleActive(false)
			local posdata = self.m_IsFemale and cdata.StartPosWhenFemale or cdata.StartPosWhenMale
			if posdata and posdata.Length>=6 then
				startPos = posdata[0] * (1 / cdata.FrameRate)
				self.m_JumpToTime = posdata[1] * (1/cdata.FrameRate)
			end
			if LuaCharacterCreation2023Mgr.m_NeedGoStill then
				LuaCharacterCreation2023Mgr.m_NeedGoStill = false
				LuaCharacterCreation2023Mgr:PlayTimeline(timelineName,self.m_JumpToTime,id,self.m_IsFemale,self.m_CurClass)
			else
				LuaCharacterCreation2023Mgr:PlayTimeline(timelineName,startPos,id,self.m_IsFemale,self.m_CurClass)
			end
		end
		LuaCharacterCreation2023Mgr.m_CanJump = true	
	end))
end

function LuaCharacterCreation2023Wnd:SetCreatingRoleActive(isActive)
	if isActive then
		self:InitCreatingJobDetail()
		local isFirst = LuaCharacterCreation2023Mgr.m_SelectedClassForCutscene and LuaCharacterCreation2023Mgr.m_SelectedClassForCutscene > 0
		LuaCharacterCreation2023Mgr.m_SelectedClassForCutscene = nil
		self.CreatingRole:SetActive(not isFirst)
		self.JobTableView.m_ScrollView:SetDragAmount(0, self.m_CurClass/13, false)
	else
		self.m_HideSmrs = {}
		self.m_HideMrs = {}
		if self.m_HideCreatingTick then
			UnRegisterTick(self.m_HideCreatingTick)
			self.m_HideCreatingTick = nil
		end
		local isFirst = LuaCharacterCreation2023Mgr.m_SelectedClassForCutscene and LuaCharacterCreation2023Mgr.m_SelectedClassForCutscene > 0
		LuaCharacterCreation2023Mgr.m_SelectedClassForCutscene = nil
		if not isFirst then
			self.m_CreatingRoleAnimation:Play("creatingrole_close")
			self.m_HideCreatingTick = RegisterTickOnce(function()
				self.CreatingRole:SetActive(false)
			end,1000)
		else
			self.CreatingRole:SetActive(false)
		end
	end
end

------------------------ExitingRole-----------------------------------------------------------------------
function LuaCharacterCreation2023Wnd:InitExitingView(isFirst)
	CMainCamera.Inst:SetCameraEnableStatus(false, "Enter_ChuangJue", false)
	self.JumpTips:SetActive(false)
	self.JumpControlTex:SetActive(false)
	self:SetCreatingRoleActive(false)
	--self.ExistingRole.gameObject:SetActive(true)
	self.m_PageIndex = 1
	local list = CLoginMgr.Inst.m_ExistingPlayerList
	local hasSelected = false
	local itemCount = list.Count < 4 and 4 or list.Count
	self.m_PageCount = math.ceil(itemCount / 4)
	self.m_PageCount = math.max(self.m_PageCount,1)
	self:InitExitingPages()

	self.TeamAppointmentButton:SetActive(false)
	self.EnterButton.gameObject:SetActive(not(CLoginMgr.Inst.PreLoginMode and CLoginMgr.Inst.PreLoginModeCharacterExists))

	self.OpenTimeLabel.gameObject:SetActive(CLoginMgr.Inst.PreLoginMode and CLoginMgr.Inst.PreLoginModeCharacterExists)
	self.OpenTimeLabel.text = CLoginMgr.Inst:GetLatestSelectedGameServer().openTime

	for j=0,itemCount-1,1 do
		table.insert(self.m_ExsitingPlayerRos,nil)
	end
	for i=0,itemCount-1,1 do
		if i >= list.Count then
			self:InitExitingItem(i,nil)
		else
			local existingPlayer = list[i]
			self:InitExitingItem(i,existingPlayer)
			if CLoginMgr.Inst.m_LastLoginPlayerId == list[i].m_BasicProp.Id then
				hasSelected = true
				local item = self["ExistingPortrait"..(i%4)+1]
				self:OnClickExsitingRole(item,list[i],i,true)
				local pageIndex = math.floor(i / 4) + 1
				self:GoToPage(pageIndex)
				return
			end
		end	
	end
	if CLoginMgr.Inst.m_LastLoginPlayerId == 0 then
		if list and list.Count > 0 then
			local item = self.ExistingPortrait1
			self:OnClickExsitingRole(item,list[0],0,true)
			self:GoToPage(1)
			return
		else
			--返回灯中世界
			CMainCamera.Inst:SetCameraClearFlags(CameraClearFlags.SolidColor)
			CUIManager.ShowUI(CUIResources.BeforeEnterDengZhongShiJieWnd)
			CUIManager.CloseUI(CUIResources.CharacterCreation2023Wnd)
		end
	end
end

function LuaCharacterCreation2023Wnd:InitExitingPages()
	self.PageContainer:SetActive(self.m_PageCount > 1)	
	self.PageLabel.text = SafeStringFormat3("%d / %d",self.m_PageIndex,self.m_PageCount)
	self.PreviousBtn.Enabled = self.m_PageIndex > 1
	self.NextBtn.Enabled = self.m_PageIndex < self.m_PageCount
end

function LuaCharacterCreation2023Wnd:OnPreviousBtnClick()
	if self.m_PageIndex <= 1 then
		return
	end
	self.ExistingRole:Play("existingrole_switch")
	self:GoToPage(self.m_PageIndex - 1)
end

function LuaCharacterCreation2023Wnd:OnNextBtnClick()
	if self.m_PageIndex >= self.m_PageCount then
		return
	end
	self.ExistingRole:Play("existingrole_switch")
	self:GoToPage(self.m_PageIndex + 1)
end

function LuaCharacterCreation2023Wnd:GoToPage(pageIndex)
	self.m_PageIndex = pageIndex

	local list = CLoginMgr.Inst.m_ExistingPlayerList
	for idx=0,3,1 do
		local i = (pageIndex-1) * 4 + idx
		if i >= list.Count then
			self:InitExitingItem(i,nil)
		else
			local existingPlayer = list[i]
			self:InitExitingItem(i,existingPlayer)
			if i == self.m_LoginPlayerIdx then
				hasSelected = true
				local item = self["ExistingPortrait"..idx+1]
				local Highlight = item.transform:Find("ExistingPortrait_highlight")
				Highlight.gameObject:SetActive(true)
			end
		end	
	end

	self:InitExitingPages()
end

function LuaCharacterCreation2023Wnd:InitExitingItem(itemId,info)
	local item = self["ExistingPortrait"..((itemId%4)+1)]
	if not item then
		return
	end

	local Portrait = item.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
	local AddIcon = item.transform:Find("AddIcon")
	local DeleteIcon = item.transform:Find("DeleteIcon")
	local Highlight = item.transform:Find("ExistingPortrait_highlight")
	AddIcon.gameObject:SetActive(info==nil)
	Highlight.gameObject:SetActive(false)
	local showDeleteIcon = false
	item.gameObject:SetActive(true)
	if info then
		local cls = info.m_BasicProp.Class
	    local gender = info.m_BasicProp.Gender
	    local data = Initialization_Init.GetData((cls * 100 + gender))

		if data then
			Portrait:LoadNPCPortrait(data.ResName,false)
		else
			Portrait:LoadMaterial(nil)
		end
		--print(info.m_BasicProp.LastRequestDeleteRoleTime)
		showDeleteIcon = info.m_BasicProp.LastRequestDeleteRoleTime > 0
	else
		Portrait:LoadMaterial(nil)
		if self.m_PageIndex > 1 then
			item.gameObject:SetActive(false)
		end
	end
	DeleteIcon.gameObject:SetActive(showDeleteIcon)
	UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		self:OnClickExsitingRole(item,info,itemId)
	end)
	UIEventListener.Get(DeleteIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		self:OnClickDeleteIcon(item,info,itemId)
	end)
end

function LuaCharacterCreation2023Wnd:OnClickExsitingRole(item,info,loginPlayerIdx,isFirstInit)
	if not info then
		if CLoginMgr.Inst:CheckLoginConnection() then
			Gac2Login.RequestBeginCreatePlayer()
		else
			CLoginMgr.Inst:_TryBackToLoginScene()
		end

		CMainCamera.Inst:SetCameraClearFlags(CameraClearFlags.SolidColor)
		--播cg
		LuaCharacterCreation2023Mgr.ForceOpenDengzhongshijie = true
		CMainCamera.Inst:SetCameraEnableStatus(false, "Enter_ChuangJue", false)
		CUIManager.CloseUI(CUIResources.CharacterCreation2023Wnd)
		AMPlayer.Inst:PlayCG(CLoginMgr.s_NewPlayerLoginVideoName, DelegateFactory.Action(function ()
			CUIManager.ShowUI(CUIResources.BeforeEnterDengZhongShiJieWnd)
		end), 1, CLoginMgr.s_StartCgJumpTime)
		CLoginMgr.Inst:PlayCgAndLoadChuangjueAssets()
		return
	end
	self.m_LoginPlayerIdx = loginPlayerIdx

	--highlight
	local Highlight = item.transform:Find("Highlight")
	for i=1,4,1 do
		local roleitem = self["ExistingPortrait"..i]
		roleitem.transform:Find("ExistingPortrait_highlight").gameObject:SetActive(roleitem == item)
	end

	local isLoaded = self.m_CharacterLoadedTbl[info.m_BasicProp.Id]
	if isLoaded then
		if not isFirstInit then
			LuaCharacterCreation2023Mgr:ShowCrossingFx()
		end
		self:ChangeExitingView(item,info,loginPlayerIdx,isFirstInit)
	else

			LuaCharacterCreation2023Mgr:ShowCrossingFx()

		self.m_ChangeViewTick = RegisterTickOnce(function()
			self:ChangeExitingView(item,info,loginPlayerIdx,isFirstInit)
		end,500)
	end
end

function LuaCharacterCreation2023Wnd:ChangeExitingView(item,info,loginPlayerIdx,isFirstInit)
	--deltail
	local cls = info.m_BasicProp.Class
	local gender = info.m_BasicProp.Gender
	--影灵形态优先于性别
	local playerInfo = CLoginMgr.Inst.m_ExistingPlayerList[loginPlayerIdx]
    local appearanceData = playerInfo.m_AppearProp

	if cls == EnumToInt(EnumClass.YingLing) then
		local yingLingState = appearanceData.YingLingState
		if yingLingState == EnumToInt(EnumYingLingState.eMonster) then
			gender = 2
		elseif yingLingState == EnumToInt(EnumYingLingState.eFemale) then
			gender = 1
		elseif yingLingState == EnumToInt(EnumYingLingState.eMale) then
			gender = 0
		end
	end
	local data = Initialization_Init.GetData((cls * 100 + gender))
	if data then
		local path = data.NewProfessionTexture
		self.ExProfession:LoadMaterial(path)
		self.ExNameLabel.text = info.m_BasicProp.Name
		local default
		if info.m_AppearProp.FeiShengPlayXianFanStatus > 0 then
			default = Constants.ColorOfFeiSheng
		else
			default = NGUIText.EncodeColor24(self.ExLevelLabel.color)
		end
		self.ExLevelLabel.text = System.String.Format(LocalString.GetString("[c][{0}]{1}级[-][/c]"), default, info.m_BasicProp.Level)
	end		
	
	local cjdata = Initialization_ChuangJue2023.GetData((cls * 100 + gender))
	if not cjdata then
		return
	end

	--init scene
	if CRenderScene.Inst  then
		if isFirstInit then
			CMainCamera.Inst:SetCameraClearFlags(CameraClearFlags.Skybox)
			CMainCamera.Inst:SetFarClipPlane(1000)
			CMainCamera.Inst:SetCameraEnableStatus(true, "Enter_ChuangJue", false)
		end
		
		--init camera
		local camPos = Vector3(cjdata.ExitingCanmeraPos[0],cjdata.ExitingCanmeraPos[1],cjdata.ExitingCanmeraPos[2])
		local camRot = Vector3(cjdata.ExitingCameraEuler[0],cjdata.ExitingCameraEuler[1],cjdata.ExitingCameraEuler[2])
		CameraFollow.Inst:SetCameraStill(camPos,camRot)
		CMainCamera.Main.fieldOfView = cjdata.ExitingCameraFov

		CRenderScene.Inst:ChangeAppendedSceneTo(cjdata.SubScene, DelegateFactory.Action(function()		
			LuaCharacterCreation2023Mgr:HideCrossingFx()
			if not self.ExistingRole.gameObject.activeSelf then
				self.ExistingRole.gameObject:SetActive(true)
			end
			local weather = cjdata.Weather
			WeatherManager.Inst:Reset()
			WeatherManager.ApplyProfileByName(weather)
		end))
	end
	--init weather
	
	--init player
	local playerPos = Vector3(cjdata.ExitingRolePos[0],cjdata.ExitingRolePos[1],cjdata.ExitingRolePos[2])
	
	if not self.m_ModelRoot then
		self.m_ModelRoot = CRenderScene.Inst.transform
	end
    if not self.m_RO then
		self.m_RO = CRenderObject.CreateRenderObject(self.m_ModelRoot.gameObject, "RenderObject")
		self.m_RO.EnableDelayLoad = false
		self.m_RO.Layer = LayerDefine.MainPlayer
		self.m_RO.OcclusionEffect = true

	end
    self:LoadPlayerRes(self.m_RO, cls, gender, appearanceData, false, false)
	self.m_RO.transform.position = playerPos
	self.m_RO.transform.rotation = Quaternion.Euler(0,cjdata.ExitingRoleEuler[1],0)

	--bgm
	local scenenName = cjdata.SubScene
    if scenenName ~= LuaCharacterCreation2023Mgr.m_LastSceneName then
        LuaCharacterCreation2023Mgr.m_LastSceneName = scenenName
        if LuaCharacterCreation2023Mgr.BGM then
            SoundManager.Inst:StopSound(LuaCharacterCreation2023Mgr.BGM)
            LuaCharacterCreation2023Mgr.BGM = nil
        end
		if PlayerSettings.MusicEnabled then
			LuaCharacterCreation2023Mgr.BGM = SoundManager.Inst:PlaySound(cjdata.BGM,Vector3.zero, 0.5, nil, 0)
		end
    end

	--light
	if self.LightGo then
		GameObject.Destroy(self.LightGo)
		self.LightGo = nil
	end
	if cjdata.LightPrefab and cjdata.LightPrefab ~= "" then
		CSharpResourceLoader.Inst:LoadGameObject(cjdata.LightPrefab, DelegateFactory.Action_GameObject(function(obj)
			self.LightGo = GameObject.Instantiate(obj, Vector3.zero, Quaternion.identity)
			self.LightGo.transform.parent = self.m_ModelRoot
			self.LightGo.transform.position = playerPos
			self.LightGo.transform.rotation = Quaternion.Euler(0,cjdata.ExitingRoleEuler[1],0)
			self.LightGo.transform.localScale = Vector3.one
		end))
	end

	--delete btn
	if CSwitchMgr.DeleteCharacterEnabled and (not CLoginMgr.Inst.PreLoginMode) then
		if info.m_BasicProp.Level < self.MinRequiredLevelToDeleteRole() then
			self.m_DeleteBtn.gameObject:SetActive(true)
		else
			self.m_DeleteBtn.gameObject:SetActive(false)
		end
	else
		self.m_DeleteBtn.gameObject:SetActive(false)
	end
	--播音效
    if self.m_Sound~=nil then
        SoundManager.Inst:StopSound(self.m_Sound)
        self.m_Sound = nil
    end
    if data ~= nil and not System.String.IsNullOrEmpty(data.Sound) then
        self.m_Sound = SoundManager.Inst:PlayOneShot(data.Sound, Vector3.zero, nil, 0)
    end

	self.m_CharacterLoadedTbl[info.m_BasicProp.Id] = true
end

function LuaCharacterCreation2023Wnd:MinRequiredLevelToDeleteRole()
	return GameSetting_Common.GetData().MinRequiredLevelToDeleteRole
end

function LuaCharacterCreation2023Wnd:OnClickDeleteIcon(item,info,loginPlayerIdx)
	if info then
		Gac2Login.RequestCharacterRequestDeleteDetail(info.m_BasicProp.Id)
	end
end

function LuaCharacterCreation2023Wnd:OnDeleteBtnClick()
	if not CSwitchMgr.DeleteCharacterEnabled then
        return
    end
	
	local idx = self.m_LoginPlayerIdx
	local existingCharacterInfo = CLoginMgr.Inst.m_ExistingPlayerList[idx]
	MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Delete_Character"), DelegateFactory.Action(function ()
		Gac2Login.RequestDeleteCharacter(existingCharacterInfo.m_BasicProp.Id)
		self.m_DeleteBtn.gameObject:SetActive(false)
	end), nil, nil, nil, false)
end

function LuaCharacterCreation2023Wnd:LoadPlayerRes(renderObj, cls, gender, appearanceData, playLiangxiang, playXianFanTransform)
    CClientMainPlayer.LoadResource(renderObj, appearanceData, true, 1, 0, false, 0, true, 0, false, nil, false,false)
    self.m_RO.Visible = false
    self.m_RO:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function (ro)
        self.m_RO.Visible = true
		if not ShaderLodParams.Inst.IsSupportJiayuanShadow then
			self.m_RO.ShadowType = EShadowType.Simple
		end
    end))
end

function LuaCharacterCreation2023Wnd:OnCutsceneSlowDownStill(args)
	local isInSilence = args[0]

	if isInSilence then
		self:SetCreatingRoleActive(true)
		--print(AMPlayer.Inst.CurrentCjPlayerRo)
	else
		self:SetCreatingRoleActive(false)
	end
	self.JumpTips:SetActive(false)
	LuaCharacterCreation2023Mgr.m_CanJump = not isInSilence
end

function LuaCharacterCreation2023Wnd:OnApplicationFocus(args)
    if not CommonDefs.IsPCGameMode() then
        local focusStatus = args[0]
		if focusStatus and LuaCharacterCreation2023Mgr.m_CanJump then
			self:OnJumpTipsClick()
		end
    end
end

function LuaCharacterCreation2023Wnd:OnCutsceneActorReady(args)
	local isReady = args[0]
	self.Texture_baiping:SetActive(false)
	LuaCharacterCreation2023Mgr:HideCrossingFx()
end

function LuaCharacterCreation2023Wnd:OnExistingRoleInfoUpdate(...)
    if self.ExistingRole.gameObject.activeSelf then
        self:InitExitingView()
    end
end
function LuaCharacterCreation2023Wnd:LoginSceneLoaded( ... )
    --self:ShowPage()
end
function LuaCharacterCreation2023Wnd:OnStartUnloadScene(args)
    local sceneName = args[0] or ""
	if sceneName == Setting.sDengLu_Level then
        self:DestoryRO()
    end
end

function LuaCharacterCreation2023Wnd:OnServerNotifyRetryLogin(...)

end

function LuaCharacterCreation2023Wnd:OnLoadingSceneFinished()
end

function LuaCharacterCreation2023Wnd:On3DTouchTriggered()
end

function LuaCharacterCreation2023Wnd:OnEnterPreLoginMode()
    if CLoginMgr.Inst.PreLoginModeCharacterExists then
        self:OnPreLoginModeCharacterExists()
    end
end

function LuaCharacterCreation2023Wnd:OnLaunchAppFromDeepLink(...)
    self:HandleDeepLink()
end
function LuaCharacterCreation2023Wnd:CurrentServerTeamAppointmentEnabled()
    local serverId  = CLoginMgr.Inst:GetSelectedGameServer().id
    local teamAppointmentEnabled = CLoginMgr.Inst:CheckTeamAppointment(serverId)
    return teamAppointmentEnabled
end
function LuaCharacterCreation2023Wnd:OnPreLoginModeCharacterExists()
	--新服预约状态下，如果存在角色，则隐藏创建和进入游戏按钮
    local preloginCharacterExists = CLoginMgr.Inst.PreLoginModeCharacterExists
    self.EnterButton.gameObject:SetActive(not preloginCharacterExists)
    self.TeamAppointmentButton:SetActive(preloginCharacterExists and self:CurrentServerTeamAppointmentEnabled())

    if self.TeamAppointmentButton.activeSelf then
        self.OpenTimeLabel.transform.localPosition = self.m_OriginOpenTimeLabelPos
    else
        local pos = self.m_OriginOpenTimeLabelPos
        self.OpenTimeLabel.transform.localPosition = Vector3(pos.x, pos.y - 74 , pos.z)
    end
end
function LuaCharacterCreation2023Wnd:BeginCreatePlayer()
end

function LuaCharacterCreation2023Wnd:HandleDeepLink()
    if CDeepLinkMgr.Inst:HasInGameAction() then
        self:OnEnterGameWithLastLoginCharacter()
        return true
    else
        return false
    end
end

function LuaCharacterCreation2023Wnd:OnEnterGameWithLastLoginCharacter()
	local list = CLoginMgr.Inst.m_ExistingPlayerList
	for i=0,list.Count-1 do
		if CLoginMgr.Inst.m_LastLoginPlayerId == list[i].m_BasicProp.Id then
            self:EnterGameWithExistingRole(i)
            break
        end
	end
end

function LuaCharacterCreation2023Wnd:EnterGameWithExistingRole(index)
	if index < CLoginMgr.Inst.m_ExistingPlayerList.Count then
        local existingCharacterInfo = CLoginMgr.Inst.m_ExistingPlayerList[index]
        local playerId = tonumber(existingCharacterInfo.m_BasicProp.Id)
        CLoginMgr.Inst:RequestLoginExistPlayer(playerId)
        EventManager.Broadcast(EnumEventType.PlayerLogin)
        CLoginMgr.Inst.IsCreateRole = false
        self.EnterButton.Enabled = false
        self:RegisterEnterGameForExistingRoleTick()
    end
end

function LuaCharacterCreation2023Wnd:DestoryRO()
	if self.m_RO~=nil then
		self.m_RO:Destroy()
		self.m_RO = nil
	end
end
------Begin Tick处理
function LuaCharacterCreation2023Wnd:RegisterEnterGameForExistingRoleTick( )

    self:CancelEnterGameForExistingRoleTick()
    self.m_EnterGameForExistingRoleTick = RegisterTickOnce(function ( ... )
            if self.EnterButton ~= nil then
                self.EnterButton.Enabled = true
            end
        end, 5000)
end
function LuaCharacterCreation2023Wnd:CancelEnterGameForExistingRoleTick( )
    if self.m_EnterGameForExistingRoleTick then
        UnRegisterTick(self.m_EnterGameForExistingRoleTick)
        self.m_EnterGameForExistingRoleTick = nil
    end
end

function LuaCharacterCreation2023Wnd:RegisterEnterGameForCreateRoleTick( )

    self:CancelEnterGameForCreateRoleTick()
    self.m_EnterGameForCreateRoleTick = RegisterTickOnce(function ( ... )
            if self.EnterButton ~= nil then
                self.EnterButton.Enabled = true
            end
        end, 3000)
end
function LuaCharacterCreation2023Wnd:CancelEnterGameForCreateRoleTick( )
    if self.m_EnterGameForCreateRoleTick then
        UnRegisterTick(self.m_EnterGameForCreateRoleTick)
        self.m_EnterGameForCreateRoleTick = nil
    end
end
------End   Tick处理
function LuaCharacterCreation2023Wnd:OnWillClose()
    LuaPinchFaceTimeCtrlMgr:OnCloseWnd()
end

function LuaCharacterCreation2023Wnd:OnSendQueueStatus()
	LuaLoginMgr:ShowQueueStatusTopNotice()
end