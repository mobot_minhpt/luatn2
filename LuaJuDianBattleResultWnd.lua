local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UITable = import "UITable"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Extensions = import "Extensions"

LuaJuDianBattleResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJuDianBattleResultWnd, "DetailBtn", "DetailBtn", GameObject)
RegistChildComponent(LuaJuDianBattleResultWnd, "ShareBtn", "ShareBtn", GameObject)
RegistChildComponent(LuaJuDianBattleResultWnd, "RankLabel", "RankLabel", UILabel)
RegistChildComponent(LuaJuDianBattleResultWnd, "GuildNameLabel", "GuildNameLabel", UILabel)
RegistChildComponent(LuaJuDianBattleResultWnd, "ZhanLingLabel", "ZhanLingLabel", UILabel)
RegistChildComponent(LuaJuDianBattleResultWnd, "KillLabel", "KillLabel", UILabel)
RegistChildComponent(LuaJuDianBattleResultWnd, "CommentLabel", "CommentLabel", UILabel)
RegistChildComponent(LuaJuDianBattleResultWnd, "SeasonLabel", "SeasonLabel", UILabel)
RegistChildComponent(LuaJuDianBattleResultWnd, "ExpLabel", "ExpLabel", UILabel)
RegistChildComponent(LuaJuDianBattleResultWnd, "YinPiaoLabel", "YinPiaoLabel", UILabel)
RegistChildComponent(LuaJuDianBattleResultWnd, "RewardGrid", "RewardGrid", GameObject)
RegistChildComponent(LuaJuDianBattleResultWnd, "BangGongLabel", "BangGongLabel", UILabel)
RegistChildComponent(LuaJuDianBattleResultWnd, "MingWangLabel", "MingWangLabel", UILabel)

--@endregion RegistChildComponent end

function LuaJuDianBattleResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.DetailBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDetailBtnClick()
	end)


	
	UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)


    --@endregion EventBind end
end

function LuaJuDianBattleResultWnd:Init()
	local data = LuaJuDianBattleMgr.ResultData

	-- 赛季信息
	self.SeasonLabel.text = SafeStringFormat3(LocalString.GetString("第%s赛季第%s周"), Extensions.ToChinese(data.season), Extensions.ToChinese(data.week))

	self.RankLabel.text = tostring(data.guildRank)
	self.GuildNameLabel.text = tostring(data.guildName)

	-- 占领击杀积分
	self.ZhanLingLabel.text = tostring(data.guildScore)
	self.KillLabel.text = tostring(data.totoalKillCount)
	self.CommentLabel.text = tostring(data.eval)

	self.BangGongLabel.text = tostring(data.guildContri)
	self.MingWangLabel.text = tostring(data.mingwang)

	self.YinPiaoLabel.text = tostring(data.freeSilver)
	self.ExpLabel.text = tostring(data.exp)

	local setting = GuildOccupationWar_Setting.GetData()
	local itemIdList = {
		-- 帮贡
		21000620,
		-- 领土
		21030423,
		-- 奖励物品
		setting.JuDianRewardItemId}

	for i=1, 3 do
		local itemId = itemIdList[i]
		local item = self.RewardGrid.transform:Find(tostring(i)).gameObject

		if i == 3 then
			local label = item.transform:Find("Label"):GetComponent(typeof(UILabel))
			label.text = ""
			local icon = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
			local itemData = Item_Item.GetData(itemId)
			icon:LoadMaterial(itemData.Icon)
		end

		UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function()
			CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
		end)
	end
end

--@region UIEvent

function LuaJuDianBattleResultWnd:OnDetailBtnClick()
	CUIManager.ShowUI(CLuaUIResources.JuDianBattleInfoWnd)
end

function LuaJuDianBattleResultWnd:OnShareBtnClick()
	CUICommonDef.CaptureScreenAndShare()
end

--@endregion UIEvent

