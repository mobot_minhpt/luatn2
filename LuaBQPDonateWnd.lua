local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CShopMallMgr = import "L10.UI.CShopMallMgr"

LuaBQPDonateWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBQPDonateWnd, "DesLabel", "DesLabel", UILabel)
RegistChildComponent(LuaBQPDonateWnd, "SponsorBtn", "SponsorBtn", UISprite)
RegistChildComponent(LuaBQPDonateWnd, "CancelBtn", "CancelBtn", UISprite)
RegistChildComponent(LuaBQPDonateWnd, "DonateCtrl", "DonateCtrl", QnAddSubAndInputButton)
RegistChildComponent(LuaBQPDonateWnd, "OwnMoney", "OwnMoney", CCurentMoneyCtrl)

--@endregion RegistChildComponent end

RegistClassMember(LuaBQPDonateWnd,"m_Cost")

function LuaBQPDonateWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.SponsorBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSponsorBtnClick()
	end)

	UIEventListener.Get(self.CancelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelBtnClick()
	end)

	self.DonateCtrl.onValueChanged = DelegateFactory.Action_uint(function(value)
	    self:OnDonateCtrlValueChanged(value)
	end)

    --@endregion EventBind end
end

function LuaBQPDonateWnd:Init()
	self.m_Cost = 0
	self.OwnMoney:SetType(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, false)
	local max = math.min(CClientMainPlayer.Inst.Jade, 10000)
	self.DonateCtrl:SetMinMax(0, max, 100)
	self.DonateCtrl:SetValue(0)
	self.DesLabel.text = g_MessageMgr:FormatMessage("BQP_DONATE_DESC")
end

--@region UIEvent

function LuaBQPDonateWnd:OnSponsorBtnClick()
	if self.m_Cost <=0 then
		g_MessageMgr:ShowMessage("BQP_ZHULI_NOT_ZERO")
		return
	end
	if CClientMainPlayer.Inst.Jade < self.m_Cost then
		MessageWndManager.ShowOKCancelMessage(LocalString.GetString("灵玉不足，是否前往充值？"), DelegateFactory.Action(function () 
            --打开充值界面
            CShopMallMgr.ShowChargeWnd()
        end), nil, LocalString.GetString("充值"), nil, false)
	else
		local msg = g_MessageMgr:FormatMessage("BQP_DONATE_CONFIRM",self.m_Cost)
		local okfunc = function()
			Gac2Gas.BQPRewardByJade(LuaBingQiPuMgr.m_DonateID,self.m_Cost)
		end
		g_MessageMgr:ShowOkCancelMessage(msg,okfunc,nil,nil,nil,false)
	end
end

function LuaBQPDonateWnd:OnCancelBtnClick()
	CUIManager.CloseUI(CLuaUIResources.BQPDonateWnd)
end

function LuaBQPDonateWnd:OnDonateCtrlValueChanged(value)
	self.m_Cost = value
	self.OwnMoney:SetCost(value)
end

--@endregion UIEvent

