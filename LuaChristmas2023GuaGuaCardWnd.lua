local CCommonUITween = import "L10.UI.CCommonUITween"

local UIProgressBar = import "UIProgressBar"

local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"

local UIGrid = import "UIGrid"

local GameObject = import "UnityEngine.GameObject"

local CUITexture = import "L10.UI.CUITexture"

local Transform = import "UnityEngine.Transform"

local CWipePanel = import "L10.UI.CWipePanel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"

local CItem = import "L10.Game.CItem"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"

LuaChristmas2023GuaGuaCardWnd = class()
RegistClassMember(LuaChristmas2023GuaGuaCardWnd, "m_IsPaid")         -- 是否是付费刮刮卡
RegistClassMember(LuaChristmas2023GuaGuaCardWnd, "m_GuaGuaKaItemId") -- 刮刮卡itemid（有两个）
RegistClassMember(LuaChristmas2023GuaGuaCardWnd, "m_BingJingId")     -- 冰晶的id
RegistClassMember(LuaChristmas2023GuaGuaCardWnd, "m_KingId")         -- 凛冬之王的id
RegistClassMember(LuaChristmas2023GuaGuaCardWnd, "m_count")          -- 奖品个数
RegistClassMember(LuaChristmas2023GuaGuaCardWnd, "m_SnowIconList")   -- 雪花图标列表
RegistClassMember(LuaChristmas2023GuaGuaCardWnd, "m_RewardList")     -- 奖品对象列表
RegistClassMember(LuaChristmas2023GuaGuaCardWnd, "m_uppattern")      -- 中奖雪花id
RegistClassMember(LuaChristmas2023GuaGuaCardWnd, "m_snowlist")       -- 卡面上的雪花列表
RegistClassMember(LuaChristmas2023GuaGuaCardWnd, "m_itemlist")       -- 卡面上的奖品列表
RegistClassMember(LuaChristmas2023GuaGuaCardWnd, "m_rewardindex")    -- 获奖位置列表
RegistClassMember(LuaChristmas2023GuaGuaCardWnd, "m_guaguakacount")  -- 剩余刮刮卡个数
RegistClassMember(LuaChristmas2023GuaGuaCardWnd, "m_ShowTickTask")   -- 展示中奖任务

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChristmas2023GuaGuaCardWnd, "TipsButton", "TipsButton", GameObject)
RegistChildComponent(LuaChristmas2023GuaGuaCardWnd, "GuaGuaPanel", "GuaGuaPanel", CWipePanel)
RegistChildComponent(LuaChristmas2023GuaGuaCardWnd, "RewardList", "RewardList", UIGrid)
RegistChildComponent(LuaChristmas2023GuaGuaCardWnd, "Normal", "Normal", GameObject)
RegistChildComponent(LuaChristmas2023GuaGuaCardWnd, "NormalUpImage", "NormalUpImage", CUITexture)
RegistChildComponent(LuaChristmas2023GuaGuaCardWnd, "King", "King", GameObject)
RegistChildComponent(LuaChristmas2023GuaGuaCardWnd, "KingUpImage", "KingUpImage", CUITexture)
RegistChildComponent(LuaChristmas2023GuaGuaCardWnd, "Bottom", "Bottom", GameObject)
RegistChildComponent(LuaChristmas2023GuaGuaCardWnd, "LastLabel", "LastLabel", UILabel)
RegistChildComponent(LuaChristmas2023GuaGuaCardWnd, "ContinueBtn", "ContinueBtn", CButton)
RegistChildComponent(LuaChristmas2023GuaGuaCardWnd, "RewardPanel", "RewardPanel", GameObject)
RegistChildComponent(LuaChristmas2023GuaGuaCardWnd, "ProgressBar", "ProgressBar", UIProgressBar)
RegistChildComponent(LuaChristmas2023GuaGuaCardWnd, "CountLabel", "CountLabel", UILabel)
RegistChildComponent(LuaChristmas2023GuaGuaCardWnd, "CountAward", "CountAward", GameObject)
RegistChildComponent(LuaChristmas2023GuaGuaCardWnd, "BaoDiBtn", "BaoDiBtn", GameObject)
RegistChildComponent(LuaChristmas2023GuaGuaCardWnd, "BaoDi", "BaoDi", GameObject)
RegistChildComponent(LuaChristmas2023GuaGuaCardWnd, "Anchor", "Anchor", CCommonUITween)
RegistChildComponent(LuaChristmas2023GuaGuaCardWnd, "CloseButton", "CloseButton", GameObject)

--@endregion RegistChildComponent end

function LuaChristmas2023GuaGuaCardWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    -- 绑定提示按钮
    UIEventListener.Get(self.TipsButton).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("Christmas2023_GuaGuaKa_TIPS")
    end)
    -- 绑定领保底按钮
    UIEventListener.Get(self.BaoDiBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.Christmas2023GetGuaGuaKaBaoDiAward()
    end)
    -- 关闭按钮绑定关闭动画
    UIEventListener.Get(self.CloseButton).onClick = DelegateFactory.VoidDelegate(function(g)
        self.Anchor:PlayDisapperAnimation(DelegateFactory.Action(function()
            CUIManager.CloseUI(CLuaUIResources.Christmas2023GuaGuaCardWnd)
        end))
    end)
end
-- 初始化保底
function LuaChristmas2023GuaGuaCardWnd:InitBaoDi()
    local countcard = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eChristmas2023GuaGuaKaTimes)
    local countaward = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eChristmas2023GuaGuaKaBaoDiTimes)
    local needcount = Christmas2023_Setting.GetData().GuaGuaBaodiTimes
    if countaward > 0 then
        self.CountAward:SetActive(false)
    else
        if countcard >= needcount then
            self.ProgressBar.gameObject:SetActive(false)
            self.BaoDi:SetActive(true)
        else
            self.ProgressBar.gameObject:SetActive(true)
            self.BaoDi:SetActive(false)
            self.ProgressBar.value = countcard / needcount
            self.CountLabel.text = SafeStringFormat3(LocalString.GetString("刮满%d/%d张 额外送"), countcard, needcount)
        end
        self.CountAward:SetActive(true)
    end
end
-- 初始化奖品对象列表
function LuaChristmas2023GuaGuaCardWnd:InitList()
    local childlist = self.RewardList:GetChildList()
    self.m_count = childlist.Count
    self.m_RewardList = {}
    for i = 0, self.m_count - 1 do
        local tf = childlist[i]
        local tbl = {
            RewardBg = tf:GetComponent(typeof(UISprite)),
            XueHuaImage = tf:Find("Image"):GetComponent(typeof(CUITexture)),
            ZhongJiang = tf:Find("Image/ZhongJiang").gameObject,
            ZhongJiangFx = tf:Find("Image/ZhongJiang/CUIFx").gameObject,
            ShuangBei = tf:Find("Image/ShuangBei").gameObject,
            ShuangBeiFx = tf:Find("Image/ShuangBei/vfx").gameObject,
            ItemImage = tf:Find("Item/IconImage"):GetComponent(typeof(CUITexture)),
            ItemOverlay = tf:Find("Item/Overlay").gameObject,
        }
        tbl.ZhongJiang:SetActive(false)
        tbl.ZhongJiangFx:SetActive(false)
        tbl.ShuangBei:SetActive(false)
        tbl.ShuangBeiFx:SetActive(false)
        tbl.ItemImage.gameObject.name = i -- 编号方便识别
        table.insert(self.m_RewardList, tbl)
    end
end
-- 初始化刮刮卡流程
function LuaChristmas2023GuaGuaCardWnd:InitGuaGuaCard()
    self.m_uppattern = -1
    -- 初始化刮刮卡面板
    self.GuaGuaPanel:Init(
        DelegateFactory.Action(function() self:GuaGuaStart() end),
        DelegateFactory.Action(function() self:GuaGuaEnd() end)
    )
    -- 隐藏奖品
    self.RewardPanel:SetActive(false)
    -- 隐藏底部按钮
    self.Bottom:SetActive(false)
end
-- 初始化刮刮卡个数
function LuaChristmas2023GuaGuaCardWnd:InitGuaGuaKaCount()
    local guaguakaid = (self.m_IsPaid and self.m_GuaGuaKaItemId[2] or self.m_GuaGuaKaItemId[1])
    self.m_guaguakacount = CItemMgr.Inst:GetItemCount(guaguakaid)
    self.LastLabel.text = SafeStringFormat3(LocalString.GetString("剩余%d张可刮"), self.m_guaguakacount)
    self.LastLabel.color = NGUIText.ParseColor24(self.m_guaguakacount > 0 and "a43d29" or "ff1f1f", 0)
    self.ContinueBtn.Text = self.m_guaguakacount > 0 and LocalString.GetString("再刮一张") or LocalString.GetString("获取更多")
    if self.m_guaguakacount > 0 then
        UIEventListener.Get(self.ContinueBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:InitGuaGuaCard() -- 加载新的刮刮卡面板
        end)
    else
        UIEventListener.Get(self.ContinueBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            local itemid = (self.m_IsPaid and self.m_GuaGuaKaItemId[2] or self.m_GuaGuaKaItemId[1])
            CItemAccessListMgr.Inst:ShowItemAccessInfo(itemid, false, go.transform, CTooltipAlignType.Top) -- 获取刮刮卡
        end)
    end
end
-- 开始刮卡
function LuaChristmas2023GuaGuaCardWnd:GuaGuaStart()
    self.m_IsPaid = false
    if LuaChristmas2023Mgr.m_IsPaid then self.m_IsPaid = true end
    Gac2Gas.Christmas2023RequestGuaGuaKa(self.m_IsPaid)
end
-- 获取到刮卡数据
function LuaChristmas2023GuaGuaCardWnd:OnChristmas2023GuaGuaKaResult(upPattern, snowlist, rewardlist, getitemlist)
    if not upPattern or not rewardlist or not getitemlist then return end
    self.m_uppattern = upPattern
    self.m_snowlist = snowlist
    self.m_itemlist = rewardlist
    if upPattern == self.m_KingId then
        self.Normal:SetActive(false)
        self.King:SetActive(true)
        self.KingUpImage:LoadMaterial(self.m_SnowIconList[upPattern])
    else
        self.Normal:SetActive(true)
        self.King:SetActive(false)
        self.NormalUpImage:LoadMaterial(self.m_SnowIconList[upPattern])
    end

    self.m_rewardindex = nil
    for i = 1, self.m_count do
        local xuehuaid = (upPattern == self.m_KingId and self.m_KingId or snowlist[i - 1])
        if xuehuaid == self.m_BingJingId or xuehuaid == self.m_uppattern then
            self.m_rewardindex = self.m_rewardindex and self.m_rewardindex or {}
            table.insert(self.m_rewardindex, { i, xuehuaid })
        end
        self.m_RewardList[i].XueHuaImage:LoadMaterial(self.m_SnowIconList[xuehuaid])
        self.m_RewardList[i].XueHuaImage.alpha = ((xuehuaid == self.m_BingJingId or xuehuaid == upPattern) and 1 or 0.5)
        self.m_RewardList[i].ZhongJiang:SetActive(false)
        self.m_RewardList[i].ZhongJiangFx:SetActive(false)
        self.m_RewardList[i].ShuangBei:SetActive(false)
        self.m_RewardList[i].ShuangBeiFx:SetActive(false)
        local itemTemplate = Item_Item.GetData(rewardlist[i - 1])
        if itemTemplate then self.m_RewardList[i].ItemImage:LoadMaterial(itemTemplate.Icon) end
        self.m_RewardList[i].ItemOverlay:SetActive(false)
        -- 查看物品详情
        UIEventListener.Get(self.m_RewardList[i].ItemImage.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            local index = tonumber(go.name)
            if not index or index < 0 or index >= self.m_itemlist.Count then return end
            CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_itemlist[index], false, nil, AlignType.Default, 0, 0, 0, 0)
        end)
    end
    -- 显示奖品
    self.RewardPanel:SetActive(true)
end
-- 播放完毕后显示底部按钮
function LuaChristmas2023GuaGuaCardWnd:ShowBottom()
    self:InitGuaGuaKaCount()
    self.Bottom:SetActive(true)
end
-- 展示第i个位置中奖
function LuaChristmas2023GuaGuaCardWnd:ShowZhongJiangByIndex(index, xuehuaid)
    if not self.m_itemlist then return end
    if index > self.m_count or index - 1 >= self.m_itemlist.Count then return end
    local itemid = self.m_itemlist[index - 1]
    local count = (xuehuaid == self.m_BingJingId and 2 or 1)
    local item = Item_Item.GetData(itemid)
    local color = CItem.GetColorString(itemid)
    if count > 1 then
        self.m_RewardList[index].ShuangBeiFx:SetActive(true)
        self.m_RewardList[index].ShuangBei:SetActive(true)
        g_MessageMgr:ShowMessage("OBTAIN_COMMON_ITEM", color, item.Name .. "x" .. count)
    else
        self.m_RewardList[index].ZhongJiangFx:SetActive(true)
        self.m_RewardList[index].ZhongJiang:SetActive(true)
        g_MessageMgr:ShowMessage("OBTAIN_COMMON_ITEM", color, item.Name)
    end
end
-- 从左到右播放刮刮卡中奖部分，每个的间隔为duration
function LuaChristmas2023GuaGuaCardWnd:ShowGuaGuaKaZhongJiang(duration)
    if not self.m_rewardindex then
        self:ShowBottom()
        return
    end
    local rewardcount = #self.m_rewardindex
    if duration > 0 then
        if rewardcount > 0 then
            -- 第一个中奖直接播放，不等待时间
            self:ShowZhongJiangByIndex(self.m_rewardindex[1][1], self.m_rewardindex[1][2])
            local i = 2
            UnRegisterTick(self.m_ShowTickTask)
            self.m_ShowTickTask = RegisterTick(function()
                -- 全部播放完毕
                if i > rewardcount then
                    self:ShowBottom()
                    UnRegisterTick(self.m_ShowTickTask)
                    return
                end
                self:ShowZhongJiangByIndex(self.m_rewardindex[i][1], self.m_rewardindex[i][2])
                i = i + 1
            end, duration)
        else
            self:ShowBottom()
        end
    else
        -- 间隔为0一次性播完
        for i = 1, rewardcount do
            self:ShowZhongJiangByIndex(self.m_rewardindex[i][1], self.m_rewardindex[i][2])
        end
        self:ShowBottom()
    end
end
-- 刮刮卡大奖播放完毕
function LuaChristmas2023GuaGuaCardWnd:OnChristmas2023GuaGuaKaDaJiangEnd()
    self:ShowGuaGuaKaZhongJiang(0)
end
-- 刮刮卡刮开
function LuaChristmas2023GuaGuaCardWnd:GuaGuaEnd()
    self:InitBaoDi()
    if self.m_uppattern == self.m_KingId then -- 大奖
        CUIManager.ShowUI(CLuaUIResources.WarmChristmasEveWnd)
    elseif self.m_uppattern > 0 then -- 普通中奖
        self:ShowGuaGuaKaZhongJiang(600)
    else
        self:ShowBottom()
    end
end
function LuaChristmas2023GuaGuaCardWnd:Init()
    self.m_GuaGuaKaItemId = {
        tonumber(Christmas2023_Setting.GetData().GuaguaItemId),
        tonumber(Christmas2023_Setting.GetData().GuaguaItemShopId),
    }
    self.m_BingJingId = 6
    self.m_KingId = 7
    -- 读取雪花图标列表
    self.m_SnowIconList = {}
    Christmas2023_GuaGuaKa.ForeachKey(function(key)
        self.m_SnowIconList[key] = Christmas2023_GuaGuaKa.GetData(key).Icon
    end)
    -- 初始化
    self.m_ShowTickTask = nil
    self:InitBaoDi()
    self:InitList()
    self:InitGuaGuaCard()
end
-- 更新保底次数数据
function LuaChristmas2023GuaGuaCardWnd:OnUpdateTempPlayTimesWithKey(args)
    local key = (args and args[0]) and args[0] or -1
    if key == EnumPlayTimesKey_lua.eChristmas2023GuaGuaKaBaoDiTimes then
        self:InitBaoDi()
    end
end
-- 更新刮刮卡数量
function LuaChristmas2023GuaGuaCardWnd:UpdateItemCount()
    self:InitGuaGuaKaCount()
end
function LuaChristmas2023GuaGuaCardWnd:OnEnable()
    self.Anchor:PlayAppearAnimation()
    g_ScriptEvent:AddListener("OnChristmas2023GuaGuaKaResult", self, "OnChristmas2023GuaGuaKaResult")
    g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
    g_ScriptEvent:AddListener("OnChristmas2023GuaGuaKaDaJiangEnd", self, "OnChristmas2023GuaGuaKaDaJiangEnd")
    g_ScriptEvent:AddListener("SendItem", self, "UpdateItemCount")
    g_ScriptEvent:AddListener("SetItemAt", self, "UpdateItemCount")
end
function LuaChristmas2023GuaGuaCardWnd:OnDisable()
    if self.m_ShowTickTask then
        UnRegisterTick(self.m_ShowTickTask)
        self.m_ShowTickTask = nil
    end
    g_ScriptEvent:RemoveListener("OnChristmas2023GuaGuaKaResult", self, "OnChristmas2023GuaGuaKaResult")
    g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
    g_ScriptEvent:RemoveListener("OnChristmas2023GuaGuaKaDaJiangEnd", self, "OnChristmas2023GuaGuaKaDaJiangEnd")
    g_ScriptEvent:RemoveListener("SendItem", self, "UpdateItemCount")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "UpdateItemCount")
end

--@region UIEvent

--@endregion UIEvent

