local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CPropertyBuff = import "L10.Game.CPropertyBuff"

LuaDuanWu2022WuduBuffWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDuanWu2022WuduBuffWnd, "ChooseItem", "ChooseItem", GameObject)
RegistChildComponent(LuaDuanWu2022WuduBuffWnd, "CurrentItem", "CurrentItem", GameObject)

--@endregion RegistChildComponent end

function LuaDuanWu2022WuduBuffWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaDuanWu2022WuduBuffWnd:Init()

    local index = 1
    for k,v in pairs(LuaDuanWu2022Mgr.buffData.candidateBuffList) do
        self:InitChooseItem(index, v.buffId, v.value)
        index = index + 1
    end

    local buffList = {}
    for k,v in pairs(LuaDuanWu2022Mgr.buffData.selectedBuffList) do
        buffList[tonumber(v)] = k
    end

    local current = #buffList + 1
    for i=1, 4 do
        self:InitCurrentItem(i, buffList[i], i == current)
    end
end

function LuaDuanWu2022WuduBuffWnd:InitChooseItem(index, buffId, buffValue)
    local item = self.ChooseItem.transform:Find(index)
    local icon = item:Find("Icon"):GetComponent(typeof(CUITexture))
    local desc = item:Find("Desc"):GetComponent(typeof(UILabel))
    local btn = item:Find("Btn").gameObject

    local buffData = Buff_Buff.GetData(buffId)
    icon:LoadMaterial(buffData.Icon)

    local descStr = DuanWu_QuWuDu2022.GetData(buffId).Desc

    local param = {}
    local index = 1

    local format = "%%s(%%*)"
    if CommonDefs.IS_VN_CLIENT or CommonDefs.IS_HMT_CLIENT then
        format =  "$s(%%*)"
    end

    for subStr in string.gmatch(descStr, format) do
        if subStr == "" then
            table.insert(param, math.abs(buffValue[index]))
        elseif subStr == "%%" then
            table.insert(param, math.abs(buffValue[index]) * 100)
        end
        index = index + 1
    end
    
    desc.text = SafeStringFormat3(descStr, param[1], param[2])

    UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        g_MessageMgr:ShowOkCancelMessage(LocalString.GetString("你确定要选择该赐福吗？"), function()
            Gac2Gas.QWD_SelectBuff(buffId)
            CUIManager.CloseUI(CLuaUIResources.DuanWu2022WuduBuffWnd)
        end, nil, nil, nil, false)
	end)
end

function LuaDuanWu2022WuduBuffWnd:InitCurrentItem(index, buffId, isCurrent)
    local item = self.CurrentItem.transform:Find(index)
    local icon = item:Find("Icon"):GetComponent(typeof(CUITexture))
    local desc = item:Find("Desc"):GetComponent(typeof(UILabel))
    local wenhao = item:Find("Icon/Label").gameObject
    wenhao:SetActive(false)
    desc.color = Color(1,1,1,1)

    local matKey = {"ha", "wu", "xi", "xie"}

    if buffId  then
        local buffData = Buff_Buff.GetData(buffId)
        icon:LoadMaterial(buffData.Icon)
        local originText = CPropertyBuff.GetBuffDisplay(buffId, CClientMainPlayer.Inst.EngineId)
        desc.text = string.gsub(originText, LocalString.GetString("驱五毒活动专属增益，离开玩法后消失。"), "")
    elseif isCurrent then
        wenhao:SetActive(true)
        desc.text = LocalString.GetString("[98CEFD]赐福选择中")
    else
        desc.text = SafeStringFormat3(LocalString.GetString("[98CEFD]通关第%s层获得增益"), Extensions.ConvertToChineseString(index))
        icon:LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/duanwu2022wuduresultwnd_%s.mat", matKey[index]))
    end
end

--@region UIEvent

--@endregion UIEvent

