local DelegateFactory    = import "DelegateFactory"
local CTopAndRightTipWnd = import "L10.UI.CTopAndRightTipWnd"
local CScene             = import "L10.Game.CScene"
local CClientObjectMgr   = import "L10.Game.CClientObjectMgr"
local CClientNpc         = import "L10.Game.CClientNpc"

LuaYinHunPoZhiTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaYinHunPoZhiTopRightWnd, "hpItem")
RegistClassMember(LuaYinHunPoZhiTopRightWnd, "awardTemplate")
RegistClassMember(LuaYinHunPoZhiTopRightWnd, "awardTable")
RegistClassMember(LuaYinHunPoZhiTopRightWnd, "score")
RegistClassMember(LuaYinHunPoZhiTopRightWnd, "time")
RegistClassMember(LuaYinHunPoZhiTopRightWnd, "expandButton")

RegistClassMember(LuaYinHunPoZhiTopRightWnd, "curScore")
RegistClassMember(LuaYinHunPoZhiTopRightWnd, "awardScoreTbl")
RegistClassMember(LuaYinHunPoZhiTopRightWnd, "awardNum")
RegistClassMember(LuaYinHunPoZhiTopRightWnd, "maxHpWidth")

function LuaYinHunPoZhiTopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
    self:InitActive()
end

-- 初始化UI组件
function LuaYinHunPoZhiTopRightWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.hpItem = anchor:Find("Bar/HPItem"):GetComponent(typeof(UIWidget))
    self.awardTemplate = anchor:Find("Bar/Template").gameObject
    self.awardTable = anchor:Find("Bar/Table")
    self.score = anchor:Find("Up/Score"):GetComponent(typeof(UILabel))
    self.time = anchor:Find("Up/LeftTime"):GetComponent(typeof(UILabel))
    self.expandButton = anchor:Find("ExpandButton").gameObject
end

function LuaYinHunPoZhiTopRightWnd:InitEventListener()
    UIEventListener.Get(self.expandButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)
end

function LuaYinHunPoZhiTopRightWnd:InitActive()
    self.awardTemplate:SetActive(false)
end

function LuaYinHunPoZhiTopRightWnd:OnEnable()
	g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
    g_ScriptEvent:AddListener("QingMing2022SyncYHPZScoreInPlay", self, "OnSyncYHPZScoreInPlay")
    g_ScriptEvent:AddListener("ChangeTarget", self, "OnChangeTarget")
end

function LuaYinHunPoZhiTopRightWnd:OnDisable()
	g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("QingMing2022SyncYHPZScoreInPlay", self, "OnSyncYHPZScoreInPlay")
    g_ScriptEvent:RemoveListener("ChangeTarget", self, "OnChangeTarget")
end

function LuaYinHunPoZhiTopRightWnd:OnHideTopAndRightTipWnd()
    LuaUtils.SetLocalRotation(self.expandButton.transform, 0, 0, 180)
end

function LuaYinHunPoZhiTopRightWnd:OnRemainTimeUpdate(args)
    self:SetRemainTime()
end

function LuaYinHunPoZhiTopRightWnd:OnSyncYHPZScoreInPlay()
    self.curScore = LuaQingMing2022Mgr.YHPZInfo.score
    self:UpdateScore()
end

-- 选中目标改变
function LuaYinHunPoZhiTopRightWnd:OnChangeTarget(args)
    local obj = CClientObjectMgr.Inst:GetObject(args[0])

    if TypeIs(obj, typeof(CClientNpc)) then
        local npcIdTbl = LuaQingMing2022Mgr:Array2Tbl(QingMing2022_PVESetting.GetData().GhosNpc)

        for k, npcId in pairs(npcIdTbl) do
            if obj.TemplateId == npcId then
                LuaOverheadButtonMgr:ShowOverheadButton(LocalString.GetString("化解"), obj.RO:GetSlotTransform("TopAnchor", false), function()
                    Gac2Gas.QingMing2022PVEOpenAnswerWnd()
                end)
                break
            end
        end
    end
end


function LuaYinHunPoZhiTopRightWnd:Init()
    self.maxHpWidth = 332
    self.curScore = LuaQingMing2022Mgr.YHPZInfo.score and LuaQingMing2022Mgr.YHPZInfo.score or 0

    self:InitAward()
    self:UpdateScore()
    self:SetRemainTime()
end

-- 初始化奖励
function LuaYinHunPoZhiTopRightWnd:InitAward()
    Extensions.RemoveAllChildren(self.awardTable)

    local setting = QingMing2022_PVESetting.GetData()
    local itemIdTbl = LuaQingMing2022Mgr:Array2Tbl(setting.AwardItemId)
    self.awardScoreTbl = LuaQingMing2022Mgr:Array2Tbl(setting.AwardScore)
    if not itemIdTbl or not self.awardScoreTbl then return end

    self.awardNum = #self.awardScoreTbl
    local maxScore = self.awardScoreTbl[self.awardNum]
    for i = 1, self.awardNum do
        local child = CUICommonDef.AddChild(self.awardTable.gameObject, self.awardTemplate)
        child:SetActive(true)

        child.transform:Find("Score"):GetComponent(typeof(UILabel)).text = self.awardScoreTbl[i]
        local item = Item_Item.GetData(itemIdTbl[i])
        child.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
        local posX = self.awardScoreTbl[i] / maxScore * self.maxHpWidth
        Extensions.SetLocalPositionX(child.transform, posX)
    end
end

-- 更新分数
function LuaYinHunPoZhiTopRightWnd:UpdateScore()
    local maxScore = self.awardScoreTbl[self.awardNum]
    for i = 1, self.awardNum do
        local child = self.awardTable:GetChild(i - 1)
        local icon = child:Find("Icon"):GetComponent(typeof(CUITexture))
        local achieved = child:Find("Achieved").gameObject
        local unachieved = child:Find("Unachieved").gameObject
        local iconBg = icon.transform:Find("Bg").gameObject
        iconBg:SetActive(false)

        if self.curScore >= self.awardScoreTbl[i] then
            Extensions.SetLocalPositionZ(icon.transform, 0)
            achieved:SetActive(true)
            unachieved:SetActive(false)

            if i == self.awardNum or self.curScore < self.awardScoreTbl[i + 1] then
                iconBg:SetActive(true)
            end
        else
            Extensions.SetLocalPositionZ(icon.transform, -1)
            achieved:SetActive(false)
            unachieved:SetActive(true)
        end
    end

    self.score.text = self.curScore
    self.hpItem.enabled = self.curScore > 0
    self.hpItem.width = (self.curScore / maxScore > 1 and 1 or self.curScore / maxScore) * self.maxHpWidth
end

-- 剩余时间
function LuaYinHunPoZhiTopRightWnd:SetRemainTime()
    local mainScene = CScene.MainScene
    if mainScene then
        if mainScene.ShowTimeCountDown then
            self.time.text = self:GetRemainTimeText(mainScene.ShowTime)
        else
            self.time.text = ""
        end
    else
        self.time.text = ""
    end
end

function LuaYinHunPoZhiTopRightWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return cs_string.Format(LocalString.GetString("{0:00}:{1:00}:{2:00}"),
         math.floor(totalSeconds / 3600),
         math.floor((totalSeconds % 3600) / 60),
         totalSeconds % 60)
    else
        return cs_string.Format(LocalString.GetString("{0:00}:{1:00}"), math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaYinHunPoZhiTopRightWnd:OnDestroy()
    LuaQingMing2022Mgr.YHPZInfo.score = 0
end

--@region UIEvent

function LuaYinHunPoZhiTopRightWnd:OnExpandButtonClick()
    LuaUtils.SetLocalRotation(self.expandButton.transform, 0, 0, 0)
	CTopAndRightTipWnd.showPackage = false
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

--@endregion UIEvent

