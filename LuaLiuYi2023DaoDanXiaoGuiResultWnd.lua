local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UIGrid = import "UIGrid"
local QnButton = import "L10.UI.QnButton"
local CButton = import "L10.UI.CButton"
local QnTableView = import "L10.UI.QnTableView"
local CUITexture = import "L10.UI.CUITexture"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemMgr = import "L10.Game.CItemMgr"
local Profession = import "L10.Game.Profession"
local Color = import "UnityEngine.Color"
local CUICommonDef = import "L10.UI.CUICommonDef"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local Animation = import "UnityEngine.Animation"

LuaLiuYi2023DaoDanXiaoGuiResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiResultWnd, "ShengLi", "ShengLi", GameObject)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiResultWnd, "ShiBai", "ShiBai", GameObject)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiResultWnd, "RewardTimesLab", "RewardTimesLab", UILabel)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiResultWnd, "Rewards", "Rewards", UIGrid)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiResultWnd, "Reward1", "Reward1", QnButton)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiResultWnd, "Reward2", "Reward2", QnButton)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiResultWnd, "ShowCapture", "ShowCapture", GameObject)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiResultWnd, "InfoTable", "InfoTable", QnTableView)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiResultWnd, "ShareBtn", "ShareBtn", QnButton)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiResultWnd, "RankBtn", "RankBtn", CButton)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiResultWnd, "PlayAgainBtn", "PlayAgainBtn", CButton)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiResultWnd, "HideCapture", "HideCapture", GameObject)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiResultWnd, "RewardLimitTip", "RewardLimitTip", UILabel)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiResultWnd, "RewardFailTip", "RewardFailTip", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaLiuYi2023DaoDanXiaoGuiResultWnd, "m_Infos")
RegistClassMember(LuaLiuYi2023DaoDanXiaoGuiResultWnd, "m_JoinRewardMaxNum")
RegistClassMember(LuaLiuYi2023DaoDanXiaoGuiResultWnd, "m_Animation")
RegistClassMember(LuaLiuYi2023DaoDanXiaoGuiResultWnd, "m_NormalCol")
RegistClassMember(LuaLiuYi2023DaoDanXiaoGuiResultWnd, "m_HighlightCol")

function LuaLiuYi2023DaoDanXiaoGuiResultWnd:Awake()
    self.m_Animation = self.transform:GetComponent(typeof(Animation))
    self.m_NormalCol = NGUIText.ParseColor24("6C5D58", 0)
    self.m_HighlightCol = NGUIText.ParseColor24("954500", 0)
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_JoinRewardMaxNum = tonumber(LiuYi2023_DaoDanXiaoGui.GetData("JoinRewardMaxNum").Value)

    self.ShareBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnShareBtnClicked()
    end)

    UIEventListener.Get(self.RankBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CUIManager.ShowUI(CLuaUIResources.LiuYi2023DaoDanXiaoGuiRankWnd)
	end)

    UIEventListener.Get(self.PlayAgainBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CUIManager.CloseUI(CLuaUIResources.LiuYi2023DaoDanXiaoGuiResultWnd)
        CUIManager.ShowUI(CLuaUIResources.LiuYi2023DaoDanXiaoGuiSignWnd)
	end)

    self.InfoTable.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_Infos
        end,
        function(item, index)
            self:InitItem(item,index,self.m_Infos[index+1])
        end
    )

    self.ShowCapture:SetActive(false)
end

function LuaLiuYi2023DaoDanXiaoGuiResultWnd:Init()
    self.m_Infos = LuaLiuYi2023Mgr.m_DDXGResultPlayerInfos
    local isWin = LuaLiuYi2023Mgr.m_DDXGIsSucess 
    local rewards = LuaLiuYi2023Mgr.m_DDXGRewards
    local passTime = LuaLiuYi2023Mgr.m_DDXGPassTime

    -- self.ShengLi:SetActive(isWin)
    -- self.ShiBai:SetActive(not isWin)
    if isWin then
        self.m_Animation:Play("liuyi2023daodanxiaoguiresultwnd_win")
    else
        self.m_Animation:Play("liuyi2023daodanxiaoguiresultwnd_lose")
    end

    self.Reward1.gameObject:SetActive(#rewards >= 1)
    if #rewards >= 1 then
        self:InitReward(self.Reward1, rewards[1])
    end
    self.Reward2.gameObject:SetActive(#rewards >= 2)
    if #rewards >= 2 then
        self:InitReward(self.Reward2, rewards[2])
    end
    self.Rewards.gameObject:SetActive(true)
    self.Rewards:Reposition()
    
    self.RewardTimesLab.text = SafeStringFormat3("%d/%d", 0, self.m_JoinRewardMaxNum)

    if isWin and #rewards == 0 then
        self.RewardLimitTip.gameObject:SetActive(true)
    elseif not isWin then
        self.RewardFailTip.gameObject:SetActive(true)
    end

    self.InfoTable:ReloadData(true, false)
    Gac2Gas.PlayerQueryDaoDanXiaoGuiData()
end

function LuaLiuYi2023DaoDanXiaoGuiResultWnd:OnEnable()
    g_ScriptEvent:AddListener("PlayerQueryDaoDanXiaoGuiDataResult", self, "OnPlayerQueryDaoDanXiaoGuiDataResult")
end

function LuaLiuYi2023DaoDanXiaoGuiResultWnd:OnDisable()
    g_ScriptEvent:RemoveListener("PlayerQueryDaoDanXiaoGuiDataResult", self, "OnPlayerQueryDaoDanXiaoGuiDataResult")
end

--@region UIEvent

--@endregion UIEvent

function LuaLiuYi2023DaoDanXiaoGuiResultWnd:OnPlayerQueryDaoDanXiaoGuiDataResult(todayJoinTime, todayRewardTime, passTime)
    self.RewardTimesLab.text = SafeStringFormat3("%d/%d", todayRewardTime, self.m_JoinRewardMaxNum)
end

function LuaLiuYi2023DaoDanXiaoGuiResultWnd:InitReward(rewardBtn, itemTemplateId)
    local itemTex   = rewardBtn.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))

    local item = CItemMgr.Inst:GetItemTemplate(itemTemplateId)
    itemTex:LoadMaterial(item.Icon)
    rewardBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemTemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)
end

function LuaLiuYi2023DaoDanXiaoGuiResultWnd:InitItem(item, index, info)
    local career        = item.transform:Find("Icon"):GetComponent(typeof(UISprite))
    local nameLab       = item.transform:Find("NameLab"):GetComponent(typeof(UILabel))
    local damageLab     = item.transform:Find("DamageLab"):GetComponent(typeof(UILabel))
    local findNumLab    = item.transform:Find("FindNumLab"):GetComponent(typeof(UILabel))

    local class = info.class
    local playerName = info.playerName
    local damage = info.damage
    local findNum = info.findNum

    career.spriteName = Profession.GetIconByNumber(class)
    nameLab.text = playerName
    damageLab.text = damage >= 10000 and SafeStringFormat3(LocalString.GetString("%.f万"), damage / 10000) or SafeStringFormat3("%.f", damage)
    findNumLab.text = findNum

    local mainPlayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local isMainPlayer = info.playerId == mainPlayerId
    nameLab.color = isMainPlayer and self.m_HighlightCol or self.m_NormalCol
    damageLab.color = isMainPlayer and self.m_HighlightCol or self.m_NormalCol
    findNumLab.color = isMainPlayer and self.m_HighlightCol or self.m_NormalCol
end

function LuaLiuYi2023DaoDanXiaoGuiResultWnd:OnShareBtnClicked()
    local UIModule = CLuaUIResources.LiuYi2023DaoDanXiaoGuiResultWnd
    CUIManager.SetUITop(UIModule)
    self.ShowCapture:SetActive(true)
    CUICommonDef.CaptureScreen(
        "screenshot",
        true,
        false,
        self.HideCapture,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
                self.ShowCapture:SetActive(false)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
                CUIManager.ResetUITop(UIModule)
            end
        ),
        false
    )
end

function LuaLiuYi2023DaoDanXiaoGuiResultWnd:OnDestroy()
    LuaLiuYi2023Mgr.m_DDXGResultPlayerInfos = {}
    LuaLiuYi2023Mgr.m_DDXGRewards = {}
end