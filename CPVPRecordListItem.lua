-- Auto Generated!!
local CPVPRecordListItem = import "L10.UI.CPVPRecordListItem"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local LocalString = import "LocalString"
local Quaternion = import "UnityEngine.Quaternion"
CPVPRecordListItem.m_Init_CS2LuaHook = function (this, data) 
    this.playerId = data.targetId
    this.portrait:LoadNPCPortrait(data.portraitName, false)
    this.winIcon.spriteName = data.iamWin and CPVPRecordListItem.WinSpriteName or CPVPRecordListItem.LoseSpriteName
    this.rankChangeIcon.spriteName = data.beforeRank >= data.afterRank and CPVPRecordListItem.RankUpSpriteName or CPVPRecordListItem.RankDownSpriteName
    this.nameLabel.text = data.name
    this.newestRankLabel.text = System.String.Format(LocalString.GetString("当前排名: {0}"), data.newestRank)
    local change = 0
    if data.beforeRank > data.afterRank then
        change = data.beforeRank - data.afterRank
    else
        change = data.afterRank - data.beforeRank
    end
    this.rankChangeLabel.text = tostring(change)
    this.rankChangeIcon.transform.localRotation = Quaternion.Euler(0, 0, data.iamWin and 0 or 180)
    if change == 0 then
        this.rankChangeIcon.spriteName = nil
    end
    this.curRankLabel.text = tostring(data.afterRank)
    local endTime = CServerTimeMgr.ConvertTimeStampToZone8Time(data.endTime)
    this.durationLabel.text = ToStringWrap(endTime, "HH:mm")
    local default
    if data.iamChallenger then
        default = CPVPRecordListItem.AttackString
    else
        default = CPVPRecordListItem.DefendString
    end
    this.challengeLabel.text = default
    this.challengeBtn.gameObject:SetActive(not data.iamChallenger)
    this.challengeBtn.Enabled = (data.newestRank < data.myNewestRank)
end
