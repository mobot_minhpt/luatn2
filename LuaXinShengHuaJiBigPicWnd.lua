local DelegateFactory  = import "DelegateFactory"
local GameObject       = import "UnityEngine.GameObject"
local CServerTimeMgr   = import "L10.Game.CServerTimeMgr"
local CUICommonDef     = import "L10.UI.CUICommonDef"
local Vector3          = import "UnityEngine.Vector3"
local Vector2          = import "UnityEngine.Vector2"
local UICamera         = import "UICamera"
local Screen           = import "UnityEngine.Screen"
local Constants        = import "L10.Game.Constants"

LuaXinShengHuaJiBigPicWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaXinShengHuaJiBigPicWnd, "Part", "Part", GameObject)
RegistChildComponent(LuaXinShengHuaJiBigPicWnd, "PictureTemplate", "PictureTemplate", GameObject)
RegistChildComponent(LuaXinShengHuaJiBigPicWnd, "Baseplate", "Baseplate", GameObject)
RegistChildComponent(LuaXinShengHuaJiBigPicWnd, "LeftButton", "LeftButton", GameObject)
RegistChildComponent(LuaXinShengHuaJiBigPicWnd, "RightButton", "RightButton", GameObject)
RegistChildComponent(LuaXinShengHuaJiBigPicWnd, "DeleteButton", "DeleteButton", GameObject)
RegistChildComponent(LuaXinShengHuaJiBigPicWnd, "ShareButton", "ShareButton", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaXinShengHuaJiBigPicWnd, "timePicTbl")

function LuaXinShengHuaJiBigPicWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.LeftButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeftButtonClick()
	end)

	UIEventListener.Get(self.RightButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightButtonClick()
	end)

	UIEventListener.Get(self.DeleteButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDeleteButtonClick()
	end)

	UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)

    --@endregion EventBind end
	self:InitActive()
end

function LuaXinShengHuaJiBigPicWnd:OnEnable()
	g_ScriptEvent:AddListener("XinShengHuaJiDelPictureSuccess", self, "OnDelPicSuccess")
	g_ScriptEvent:AddListener("XinShengHuaJiTestDifferentModel", self, "TestDifferentModel")
end

function LuaXinShengHuaJiBigPicWnd:OnDisable()
	g_ScriptEvent:RemoveListener("XinShengHuaJiDelPictureSuccess", self, "OnDelPicSuccess")
	g_ScriptEvent:RemoveListener("XinShengHuaJiTestDifferentModel", self, "TestDifferentModel")
end

function LuaXinShengHuaJiBigPicWnd:OnDelPicSuccess(time)
	LuaWuMenHuaShiMgr:CloseXinShengHuaJiBigPicWnd()
end

function LuaXinShengHuaJiBigPicWnd:InitActive()
	self.Part:SetActive(false)
	self.PictureTemplate:SetActive(false)
	self.ShareButton.gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
end

function LuaXinShengHuaJiBigPicWnd:Init()
	self:InitClassMember()
	self:UpdateShowPic()
end

function LuaXinShengHuaJiBigPicWnd:InitClassMember()
	self.timePicTbl = {}
end

-- 更新显示的图像
function LuaXinShengHuaJiBigPicWnd:UpdateShowPic()
	local id = LuaWuMenHuaShiMgr.curShowPicId
	local time = LuaWuMenHuaShiMgr.uploadPics[id].time
	local trans = self.Baseplate.transform
	for i = 1, trans.childCount do
		trans:GetChild(i - 1).gameObject:SetActive(false)
	end
	if self.timePicTbl[time] == nil then
		local itemGo = CUICommonDef.AddChild(self.Baseplate, self.PictureTemplate)
		LuaWuMenHuaShiMgr:GeneratePic(itemGo.transform:Find("Panel"), self.Part, id)
		self.timePicTbl[time] = itemGo
	end
	self.timePicTbl[time]:SetActive(true)
end

-- 是否更新图片
function LuaXinShengHuaJiBigPicWnd:ChangeShowPic(id)
	local time = LuaWuMenHuaShiMgr.uploadPics[id].time
	local waitingTime = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuajiWaitingTime * 60
	local leftTime = waitingTime - math.floor(CServerTimeMgr.Inst.timeStamp - time)
	if leftTime <= 0 then
		LuaWuMenHuaShiMgr.curShowPicId = id
		self:UpdateShowPic()
		return true
	end
	return false
end

--@region UIEvent

function LuaXinShengHuaJiBigPicWnd:OnLeftButtonClick()
	if not LuaWuMenHuaShiMgr.uploadPics then return end

	local totalNum = #LuaWuMenHuaShiMgr.uploadPics
	for i = 1, totalNum - 1 do
		local id = LuaWuMenHuaShiMgr.curShowPicId - i
		if id <= 0 then
			id = id + totalNum
		end
		if self:ChangeShowPic(id) then break end
	end
end

function LuaXinShengHuaJiBigPicWnd:OnRightButtonClick()
	if not LuaWuMenHuaShiMgr.uploadPics then return end

	local totalNum = #LuaWuMenHuaShiMgr.uploadPics
	for i = 1, totalNum - 1 do
		local id = LuaWuMenHuaShiMgr.curShowPicId + i
		if id > totalNum then
			id = id - totalNum
		end
		if self:ChangeShowPic(id) then break end
	end
end

function LuaXinShengHuaJiBigPicWnd:OnDeleteButtonClick()
	local message = g_MessageMgr:FormatMessage("XINSHENGHUAJI_DELETE_CONFIRM")
	MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
		LuaWuMenHuaShiMgr:CloseXinShengHuaJiDrawWnd()

		-- 删除图像
		local id = LuaWuMenHuaShiMgr.curShowPicId
		local time = LuaWuMenHuaShiMgr.uploadPics[id].time
		Gac2Gas.XinShengHuaJi_DelPicture(time)
	end), nil, nil, nil, false)
end

function LuaXinShengHuaJiBigPicWnd:OnShareButtonClick()
	local panelTrans = self.PictureTemplate.transform:Find("Panel")
	local panel = panelTrans:GetComponent(typeof(UIPanel))
	local clipRegion = panel.finalClipRegion

	local localWidth = clipRegion.z
	local localHeight = clipRegion.w

	local scale = CUICommonDef.m_CaptureWidth / Screen.width
	local bIsWinSocialWndOpened = false
	if CommonDefs.IsPCGameMode() then
        local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
		bIsWinSocialWndOpened = CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened
	end
	if bIsWinSocialWndOpened then
		scale = scale / (1 - Constants.WinSocialWndRatio)
	end

	-- 计算需要截图的左下角和右上角的屏幕坐标
	local leftDownPos = self:Local2Screen(Vector3(-localWidth / 2, -localHeight / 2, 0), scale)
	local rightTopPos = self:Local2Screen(Vector3(localWidth / 2, localHeight / 2, 0), scale)

	--[[ 由于不同设备使用的绘图编程接口不同(OpenGL、Direct3D)，RenderTexture的坐标原点会不同，
	所以进行局部截图时，必须保证无论是以左上角还是左下角为坐标原点，startPos的值保持不变(trick) ]]--
	CUICommonDef.CapturePartOfScreen("screenshot", true, false, nil,
		DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
			CLuaShareMgr.SimpleShareLocalImage2PersonalSpace(fullPath)
		end), leftDownPos, rightTopPos.x - leftDownPos.x, rightTopPos.y - leftDownPos.y, false)
end

--@endregion UIEvent

-- 局部坐标转为屏幕坐标
function LuaXinShengHuaJiBigPicWnd:Local2Screen(localPos, scale)
	local worldPos = self.transform:TransformPoint(localPos)
	local screenPos = UICamera.currentCamera:WorldToScreenPoint(worldPos)
	return Vector2(screenPos.x * scale, screenPos.y * scale)
end


-- 用于QA测试不同的模板以及NPC
function LuaXinShengHuaJiBigPicWnd:TestDifferentModel(npcId, modelId, bgId)
	local id = LuaWuMenHuaShiMgr.curShowPicId
	local picData = LuaWuMenHuaShiMgr.uploadPics[id].pic
	local time = LuaWuMenHuaShiMgr.uploadPics[id].time
	self.timePicTbl[time] = nil

	local setting = WuMenHuaShi_XinShengHuaJiSetting.GetData()
	picData[setting.XinShengHuaJiNpcType] = npcId
	picData[setting.XinShengHuaJiBgType] = bgId
	picData[setting.XinShengHuaJiModelType] = modelId

	self:UpdateShowPic()
end
