local QnTableView               = import "L10.UI.QnTableView"
local DefaultTableViewDataSource= import "L10.UI.DefaultTableViewDataSource"
local ClientAction              = import "L10.UI.ClientAction"
local UILabel                   = import "UILabel"
local DelegateFactory		    = import "DelegateFactory"
local UIEventListener		    = import "UIEventListener"
local LocalString               = import "LocalString"
local LuaGameObject             = import "LuaGameObject"
local CServerTimeMgr            = import "L10.Game.CServerTimeMgr"
local CUIManager                = import "L10.UI.CUIManager"
local QnTabButton               = import "L10.UI.QnTabButton"

--天成酒壶任务页签
LuaHanJia2023PassMainTaskTab = class()

RegistChildComponent(LuaHanJia2023PassMainTaskTab, "TaskTitleLB1",		    UILabel)
RegistChildComponent(LuaHanJia2023PassMainTaskTab, "TaskTitleLB2",		    UILabel)
RegistChildComponent(LuaHanJia2023PassMainTaskTab, "QnTableView1",		    QnTableView)
RegistChildComponent(LuaHanJia2023PassMainTaskTab, "QnTableView2",		    QnTableView)


RegistClassMember(LuaHanJia2023PassMainTaskTab,  "DailyTasks")
RegistClassMember(LuaHanJia2023PassMainTaskTab,  "WeekTasks")
RegistClassMember(LuaHanJia2023PassMainTaskTab,  "CurWeekIndex")
RegistClassMember(LuaHanJia2023PassMainTaskTab,  "Btns")
RegistClassMember(LuaHanJia2023PassMainTaskTab,  "Isdirty")

function LuaHanJia2023PassMainTaskTab:Awake()
    self.CurWeekIndex = 0
    self.Btns = {}

    g_ScriptEvent:AddListener("HanJia2023_SyncXianKeLianData", self, "OnXianKeLaiDataChange")
    self:OnXianKeLaiDataChange()
end


function LuaHanJia2023PassMainTaskTab:OnDestroy()
    g_ScriptEvent:RemoveListener("HanJia2023_SyncXianKeLianData", self, "OnXianKeLaiDataChange")
end

function LuaHanJia2023PassMainTaskTab:OnEnable()
    if self.Isdirty then
        self:OnXianKeLaiDataChange()
    end
end

function LuaHanJia2023PassMainTaskTab:OnXianKeLaiDataChange()
    if not self.gameObject.activeSelf then
        self.Isdirty = true
        return 
    end
    self.Isdirty = false
    local data = LuaHanJia2023Mgr.XKLData
    local taskinfos = data.TaskData or {}
    self.DailyTasks = {}
    self.WeekTasks = {}
    for i = 1,5 do
        self.WeekTasks[i] = {}
    end

    local alertWeeks = {}
    local weektaskcount = 0
    local weekrewardcount = 0
    local dailytaskcount = 0
    local dailyrewardcount =0

    for i = 1,#taskinfos do
        local task = taskinfos[i]
        local IsDailyTask = HanJia2023_XianKeLaiTask.GetData(task.TaskID).IsDailyTask
        if IsDailyTask == 1 then
            dailytaskcount = dailytaskcount + 1
            if task.IsRewarded then
                dailyrewardcount = dailyrewardcount + 1
            end
            self.DailyTasks[#self.DailyTasks + 1] = task
        else
            weektaskcount = weektaskcount + 1
            if task.IsRewarded then 
                weekrewardcount = weekrewardcount + 1
            end
            local week = 1
            local weektask = self.WeekTasks[week]
            weektask[#weektask + 1] = task
            local canreward = self:CanRewarded(task)
            if canreward then
                alertWeeks[week] = 1
            end
        end
    end

    self:SortTable(self.DailyTasks)
    for i = 1,5 do
        self:SortTable(self.WeekTasks[i])
    end

    self.TaskTitleLB1.text = SafeStringFormat3(LocalString.GetString("今日任务  %d/%d"), dailyrewardcount,dailytaskcount)
    self.TaskTitleLB2.text = SafeStringFormat3(LocalString.GetString("挑战任务  %d/%d"), weekrewardcount,weektaskcount)
    
    local len = #self.DailyTasks
    local initfunc = function (item,index)
        self:FillItem(item,self.DailyTasks[index+1])
    end
    self.QnTableView1.m_DataSource = DefaultTableViewDataSource.CreateByCount(len,initfunc)
    self.QnTableView1:ReloadData(true, true)


    local curweek = 1
    for i=1,#self.Btns do
        local btn = self.Btns[i]
        if #self.WeekTasks[i] <= 0 then
            LuaGameObject.GetChildNoGC(btn.transform,"Label").gameObject:SetActive(false)
            LuaGameObject.GetChildNoGC(btn.transform,"LockLabel").gameObject:SetActive(true)
            LuaGameObject.GetChildNoGC(btn.transform,"AlertSprite").gameObject:SetActive(false)
        else
            curweek = i
            LuaGameObject.GetChildNoGC(btn.transform,"AlertSprite").gameObject:SetActive(alertWeeks[i] and alertWeeks[i] == 1)
        end
    end
    
    self:OnWeekBtnBtnClick(1)
end

function LuaHanJia2023PassMainTaskTab:CanRewarded(task)
    local taskcfg = HanJia2023_XianKeLaiTask.GetData(task.TaskID)
    local isOutDate = self:IsTaskOutDate(task)
    return (task.Progress >= taskcfg.Target or taskcfg.Target == 0) and not task.IsRewarded and not isOutDate
end

function LuaHanJia2023PassMainTaskTab:SortTable(value)
    if #value < 2 then return end
    table.sort(value, function(a, b)
        local isaout = self:IsTaskOutDate(a)
        local isbout = self:IsTaskOutDate(b)
        if not isaout and isbout then
            return true
        elseif isaout and not isbout then
            return false
        else
            if not a.IsRewarded and  b.IsRewarded then
                return true
            elseif a.IsRewarded and not b.IsRewarded then
                return false
            else
                local ar = self:IsRewarding(a)
                local br = self:IsRewarding(b)
                if ar ~= br then 
                    return ar > br
                else
                    return a.TaskID < b.TaskID
                end
            end
        end
    end)
end

function LuaHanJia2023PassMainTaskTab:IsRewarding(task)
    local taskcfg = HanJia2023_XianKeLaiTask.GetData(task.TaskID)
    if task.Progress >= taskcfg.Target or taskcfg.Target == 0 then 
        return 1
    else
        return 0
    end
end

function LuaHanJia2023PassMainTaskTab:IsTaskOutDate(task)
    local taskcfg = HanJia2023_XianKeLaiTask.GetData(task.TaskID)
    local curtime = CServerTimeMgr.Inst.timeStamp
    local taskendtime = CServerTimeMgr.Inst:GetTimeStampByStr(taskcfg.ExpireTime)
    return curtime > taskendtime
end

function LuaHanJia2023PassMainTaskTab:FillItem(item,task)

    local taskcfg = HanJia2023_XianKeLaiTask.GetData(task.TaskID)

	local exp = taskcfg.ProgressReward
	local passtype = LuaHanJia2023Mgr.XKLData.PassType
    local fac = 1

    if passtype == 2 or passtype == 3 then
		local setting = HanJia2023_XianKeLaiSetting.GetData()
        fac = fac + setting.Vip2AddRate
	end
    fac = fac + LuaHanJia2023Mgr.XKLData.addRate
    fac = fac - fac % 0.01

    local isOutDate = self:IsTaskOutDate(task)

    local transform = item.transform
    local bg1 = LuaGameObject.GetChildNoGC(transform,"Bg1").gameObject
    local deslb = LuaGameObject.GetChildNoGC(transform,"DesLabel").label
    local explb = LuaGameObject.GetChildNoGC(transform,"ExpLabel").label
    local countlb = LuaGameObject.GetChildNoGC(transform,"CountLabel").label
    local cmpgo = LuaGameObject.GetChildNoGC(transform,"CompleteSprite").gameObject
    local gotobtn = LuaGameObject.GetChildNoGC(transform,"GotoTaskBtn").gameObject
    local slider = LuaGameObject.GetChildNoGC(transform, "Slider").gameObject
    
    bg1:SetActive(true)

    deslb.text = taskcfg.Desc
    explb.text = "+"..exp .. ("[c][D76914]x" .. fac .. "[-]")
    countlb.text = task.Progress.."/"..taskcfg.Target
    slider.transform:GetComponent(typeof(UISlider)).value = task.Progress / (taskcfg.Target == 0 and 1 or taskcfg.Target)
    
    gotobtn:SetActive(task.Progress < taskcfg.Target and not isOutDate)
    cmpgo:SetActive(task.IsRewarded)


    UIEventListener.Get(gotobtn).onClick = DelegateFactory.VoidDelegate(function(go)
        ClientAction.DoAction(taskcfg.Action);
        CUIManager.CloseUI(CLuaUIResources.HanJia2023PassMainWnd)
    end)
end

--@region UIEvent

function LuaHanJia2023PassMainTaskTab:OnWeekBtnBtnClick(week)

    local weektasks = self.WeekTasks[week]
    if weektasks == nil or #weektasks <= 0 then 
        --g_MessageMgr:ShowMessage("TCJH_WEEK_Task_LOCK",week)--Msg:TCJH_WEEK_Task_LOCK
        --return
    end
    
    local len = #weektasks
    local initfunc = function(item,index)
        local task = weektasks[index+1]
        self:FillItem(item,task)
    end
    self.QnTableView2.m_DataSource = DefaultTableViewDataSource.CreateByCount(len,initfunc)
    self.QnTableView2:ReloadData(true, true)
end

--@endregion
