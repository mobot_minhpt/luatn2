local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local StringActionKeyValuePair = import "L10.Game.StringActionKeyValuePair"
local StringStringKeyValuePair = import "L10.Game.StringStringKeyValuePair"

LuaRefineMonsterListWnd = class()
RegistClassMember(LuaRefineMonsterListWnd,"m_QuestBtn")
RegistClassMember(LuaRefineMonsterListWnd,"m_CountLabel")
RegistClassMember(LuaRefineMonsterListWnd,"m_TableViewDataSource")
RegistClassMember(LuaRefineMonsterListWnd,"m_TableView")
RegistClassMember(LuaRefineMonsterListWnd,"m_Infos")
RegistClassMember(LuaRefineMonsterListWnd,"m_MonsterData")

function LuaRefineMonsterListWnd:Init()
    Gac2Gas.QueryFaZhenLianHuaMonsterQueueInfo()
    self.m_TableView = self.transform:Find("Anchor/TableView"):GetComponent(typeof(QnTableView))
    self.m_CountLabel = self.transform:Find("Anchor/Bottom/Count"):GetComponent(typeof(UILabel))
    self.m_QuestBtn = self.transform:Find("Anchor/Bottom/QuestBtn").gameObject

    UIEventListener.Get(self.m_QuestBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        g_MessageMgr:ShowMessage("LianHuaGuai_DaiLianHua_List_Introduction")
    end)

    local function initItem(item,index)
        if index % 2 == 1 then
            item:SetBackgroundTexture("common_bg_mission_background_n")
        else
            item:SetBackgroundTexture("common_bg_mission_background_s")
        end
        self:InitItem(item,index)
    end

    self.m_TableViewDataSource = DefaultTableViewDataSource.CreateByCount(0,initItem)
    self.m_TableView.m_DataSource = self.m_TableViewDataSource
    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
end

function LuaRefineMonsterListWnd:OnSelectAtRow(index)
    local monsterData = self.m_MonsterData[index+1]
    local tujianData = ZhuoYao_TuJian.GetData(monsterData.TemplateId)
    --monsterData.Level = tujianData.Fixinglevel
    local labels = {}
    table.insert(labels, CreateFromClass(StringStringKeyValuePair, LocalString.GetString("怪物品质"), tostring(monsterData.Quality)))
    table.insert(labels, CreateFromClass(StringStringKeyValuePair, LocalString.GetString("炼化时间"), LuaZongMenMgr:ScondToString(tujianData.Time)))
    local btns = {}

    table.insert(btns, CreateFromClass(StringActionKeyValuePair, LocalString.GetString("移除"), DelegateFactory.Action(function()
        if LuaZongMenMgr:CheckFaZhenOperation(LocalString.GetString("移除炼化怪")) then 
            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("LIANHUAGUAI_REMOVE_CONFIRM"), function ()
                CUIManager.CloseUI(CLuaUIResources.YaoGuaiInfoWnd)
                Gac2Gas.RequestRemoveFaZhenLianHuaMonster(index+1, monsterData.Id)
            end, nil, nil, nil, false)
        end
    end)))

    LuaZhuoYaoMgr:ShowTip(monsterData, labels, btns)
end

function LuaRefineMonsterListWnd:InitItem(item,index)
    local data = self.m_MonsterData[index+1]
    local tujianData = ZhuoYao_TuJian.GetData(data.TemplateId)

    -- 获取节点
    local tf = item.transform
    local ownerIcon = FindChild(tf,"OwnerIcon").gameObject
    local monsterIcon = FindChild(tf,"MonsterIcon"):GetComponent(typeof(CUITexture))
    local playerNameLabel = FindChild(tf,"PlayerNameLabel"):GetComponent(typeof(UILabel))
    local monsterNameLabel = FindChild(tf,"MonsterNameLabel"):GetComponent(typeof(UILabel))
    -- 排序
    local rankLabel = FindChild(tf,"RankLabel"):GetComponent(typeof(UILabel))
    local timeLabel = FindChild(tf,"TimeLabel"):GetComponent(typeof(UILabel))
    -- 品质
    local qualityLabel = FindChild(tf,"QualityLabel"):GetComponent(typeof(UILabel))

    local color = ""
    -- 第一行特殊处理
    if index ==0 then
        rankLabel.text = LocalString.GetString("[FFFE91]炼化中")
        color = "[FFFE91]"
    else
        rankLabel.text = tostring(index+1)
    end

    -- 单条信息初始化
    ownerIcon:SetActive(data.OwnerId == (CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id))
    playerNameLabel.text = color..data.OwnerName
    monsterNameLabel.text = color..tujianData.Name
    monsterIcon:LoadNPCPortrait(tujianData.Icon)
    timeLabel.text = color..LuaZongMenMgr:ScondToString(data.RestTime)
    qualityLabel.text = color..tostring(data.Quality)
end

-- info: monserId, templateId, quality, ownerId, ownerName, restTime
function LuaRefineMonsterListWnd:InitInfo(infos)
    self.m_MonsterData = {}
    self.m_Infos = infos
    for i=0, infos.Count-7, 7 do
        local m = {}
        m.Id = infos[i]
        m.TemplateId = infos[i+1]
        m.Quality = infos[i+2]
        m.OwnerId = infos[i+3]
        m.OwnerName = infos[i+4]
        m.RestTime = infos[i+5]
        m.Level = infos[i+6]

        table.insert(self.m_MonsterData, m)
    end

    self.m_CountLabel.text = SafeStringFormat3("%d/%d", #self.m_MonsterData, LianHua_Setting.GetData().LianHuaGuaiLimit)
    self.m_TableViewDataSource.count = #self.m_MonsterData
    self.m_TableView:ReloadData(true,false)
end

function LuaRefineMonsterListWnd:NeedToRefresh()
	Gac2Gas.QueryFaZhenLianHuaMonsterQueueInfo()
end

function LuaRefineMonsterListWnd:OnEnable()
	g_ScriptEvent:AddListener("QueryFaZhenLianHuaMonsterQueueInfoResult", self, "InitInfo")
	g_ScriptEvent:AddListener("RemoveFaZhenLianHuaMonsterSuccess", self, "NeedToRefresh")
end

function LuaRefineMonsterListWnd:OnDisable()
	g_ScriptEvent:RemoveListener("QueryFaZhenLianHuaMonsterQueueInfoResult", self, "InitInfo")
	g_ScriptEvent:RemoveListener("RemoveFaZhenLianHuaMonsterSuccess", self, "NeedToRefresh")
end