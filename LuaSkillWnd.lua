local UITabBar = import "L10.UI.UITabBar"
local EventManager = import "EventManager"
local CUIResources = import "L10.UI.CUIResources"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaSkillWnd = class()
RegistClassMember(LuaSkillWnd,"m_TabBar")
RegistClassMember(LuaSkillWnd,"m_SkillUpgradeView")
RegistClassMember(LuaSkillWnd,"m_SkillPreferenceView")
RegistClassMember(LuaSkillWnd,"m_LivingSkillView")
RegistClassMember(LuaSkillWnd,"m_OtherSkillView")
RegistClassMember(LuaSkillWnd,"m_ChangeSkillTab")
RegistChildComponent(LuaSkillWnd,"SkillInfoRegion", GameObject)
RegistChildComponent(LuaSkillWnd,"CloseButton", GameObject)
RegistChildComponent(LuaSkillWnd,"XiuweiRegion", GameObject)

function LuaSkillWnd:Awake()
    self:InitComponents()
end

function LuaSkillWnd:Init()
    self.m_TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnTabChange(index)
    end)
    self.m_TabBar:ChangeTab(CSkillInfoMgr.TabIndex, false)
end

function LuaSkillWnd:InitComponents()
    self.m_TabBar = self.transform:GetComponent(typeof(UITabBar))
    self.m_SkillUpgradeView = self.transform:Find("Anchor/SkillUpgradeView"):GetComponent(typeof(CCommonLuaScript))
    self.m_SkillPreferenceView = self.transform:Find("Anchor/SkillPreferenceView"):GetComponent(typeof(CCommonLuaScript))
    self.m_LivingSkillView = self.transform:Find("Anchor/LivingSkillView"):GetComponent(typeof(CCommonLuaScript))
    self.m_OtherSkillView = self.transform:Find("Anchor/OtherSkillView"):GetComponent(typeof(CCommonLuaScript))
    self.m_ChangeSkillTab = self.m_TabBar.tabRoot:GetChild(1).gameObject
end

function LuaSkillWnd:OnTabChange(index)
    self.m_SkillUpgradeView.gameObject:SetActive(index == 0)
    self.m_SkillPreferenceView.gameObject:SetActive(index == 1)
    self.m_LivingSkillView.gameObject:SetActive(index == 2)
    self.m_OtherSkillView.gameObject:SetActive(index == 3)

    EventManager.BroadcastInternalForLua(EnumEventType.Guide_ChangeView, {CUIResources.SkillWnd.Name})
end

function LuaSkillWnd:GetTargetUpgradeSkillIcon()
    return self.m_SkillUpgradeView.m_LuaSelf:GetTargetUpgradeSkillIcon()
end
function LuaSkillWnd:GetGuideGo(methodName)
    if methodName == "GetUpgradeSkillButton" then
        return self:GetUpgradeSkillButton()
    elseif methodName == "GetTargetUpgradeSkillIcon" then
        return self.m_SkillUpgradeView.m_LuaSelf:GetTargetUpgradeSkillIcon()
    elseif methodName == "GetTargetUpgradeSkillTab" then
        return self.m_SkillUpgradeView.m_LuaSelf:GetTargetUpgradeSkillTab()
    elseif methodName == "GetGameObject" then
        return self.gameObject
    elseif methodName == "Get3rdSkillTab" then
        return self:Get3rdSkillTab()
    elseif methodName == "Get3rdSkillItem" then
        return self:Get3rdSkillItem()
    elseif methodName == "GetSkillInfoRegion" then
        return self:GetSkillInfoRegion()
    elseif methodName == "Get6thSkill" then
        return self:Get6thSkill()
    elseif methodName == "Get6thSkillTab" then
        return self:Get6thSkillTab()
    elseif methodName == "GetChangeSkillTab" then
        return self:GetChangeSkillTab()
    elseif methodName == "GetDragFromGo" then
        return self:Get6thSkill()
    elseif methodName == "GetSwitchSkill" then
        return self.m_SkillPreferenceView.m_LuaSelf:GetSwitchSkill()
    elseif methodName == "GetCloseButton" then
        return self.CloseButton
    elseif methodName == "GetTianFuAdjustButton" then
        return self.m_SkillPreferenceView.m_LuaSelf:GetTianFuAdjustButton()
    elseif methodName == "GetXiuWeiRegion" then
        return self:GetXiuWeiRegion()
    elseif methodName == "Get1stTab" then
        return self.m_TabBar:GetTabGoByIndex(0)
    elseif methodName == "Get2ndTab" then
        return self.m_TabBar:GetTabGoByIndex(1)
    elseif methodName == "Get3rdTab" then
        return self.m_TabBar:GetTabGoByIndex(2)
    elseif methodName == "GetLearnLivingSkillButton" then
        --学习生活技能按钮
        --如果没有帮会，直接结束
        if CClientMainPlayer.Inst ~= nil and not CClientMainPlayer.Inst:IsInGuild() then
            L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
            return nil
        else
            return self.m_LivingSkillView.m_LuaSelf:GetLearnLivingSkillButton()
        end

        --return livingSkillView.GetLearnLivingSkillButton();
    elseif methodName == "GetCollectButton" then
        --采集
        return self.m_LivingSkillView.m_LuaSelf:GetCollectButton()
    elseif methodName == "GetCollectTab" then
        return self.m_LivingSkillView.m_LuaSelf:GetCollectTab()
    elseif methodName == "GetSwitchButton" then
        return self.m_SkillPreferenceView.m_LuaSelf:GetSwitchButton()
    elseif methodName == "GetHuaHunColorSkillTab" then
        return self.m_SkillPreferenceView.m_LuaSelf:GetHuaHunColorSkillTab()
    elseif methodName == "GetHuaHunColorSkillItem" then
        return self.m_SkillPreferenceView.m_LuaSelf:GetHuaHunColorSkillItem()
    elseif methodName=="GetYingLingSwitchSkillStateBtn" then
        return self:GetYingLingSwitchSkillStateBtn()
    elseif methodName=="GetXianZhiSkillTab" then
        if self.m_TabBar.SelectedIndex == 3 then
            if self.m_OtherSkillView.m_LuaSelf then
                return self.m_OtherSkillView.m_LuaSelf:GetXianZhiSkillTab()
            end
        end
        return nil
    elseif methodName == "Get4rdTab" then
        return self.m_TabBar:GetTabGoByIndex(3)
    elseif methodName == "GetSeaFishingSkillButton" then
        if self.m_TabBar.SelectedIndex == 3 then
            if self.m_OtherSkillView.m_LuaSelf then
                return self.m_OtherSkillView.m_LuaSelf:GetSeaFishingSkillButton()
            end
        end
        return nil
    else
        -- CLogMgr.LogError(LocalString.GetString("引导数据表填写错误 ") .. methodName)
        return nil
    end
end

function LuaSkillWnd:GetUpgradeSkillButton()
    if self.m_TabBar.SelectedIndex == 0 and self.m_SkillUpgradeView.gameObject.activeSelf then
        return self.m_SkillUpgradeView.m_LuaSelf:GetUpgradeButton()
    end
    return nil
end
function LuaSkillWnd:Get3rdSkillTab()
    if self.m_TabBar.SelectedIndex == 0 and self.m_SkillUpgradeView.gameObject.activeSelf then
        return self.m_SkillUpgradeView.m_LuaSelf:Get3rdSkillTab()
    end
    return nil
end
function LuaSkillWnd:Get3rdSkillItem()
    if self.m_TabBar.SelectedIndex == 0 and self.m_SkillUpgradeView.gameObject.activeSelf then
        return self.m_SkillUpgradeView.m_LuaSelf:Get3rdSkillItem()
    end
    return nil
end
function LuaSkillWnd:GetSkillInfoRegion()
    if self.SkillInfoRegion.activeInHierarchy then
        return self.SkillInfoRegion
    else
        return nil
    end
end
function LuaSkillWnd:Get6thSkill()
    if self.m_TabBar.SelectedIndex == 0 and self.m_SkillUpgradeView.gameObject.activeSelf then
        return self.m_SkillUpgradeView.m_LuaSelf:Get6thSkillItem()
    elseif self.m_TabBar.SelectedIndex == 1 and self.m_SkillPreferenceView.gameObject.activeSelf then
        return self.m_SkillPreferenceView.m_LuaSelf:Get6thSkillItem()
    end
    return nil
end
function LuaSkillWnd:Get6thSkillTab()
    local ret=nil
    if self.m_TabBar.SelectedIndex == 0 and self.m_SkillUpgradeView.gameObject.activeSelf then
        ret =  self.m_SkillUpgradeView.m_LuaSelf:Get6thSkillTab()
    elseif self.m_TabBar.SelectedIndex == 1 and self.m_SkillPreferenceView.gameObject.activeSelf then
        ret = self.m_SkillPreferenceView.m_LuaSelf:Get6thSkillTab()
    end
    if ret == nil then
        L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
    end
    return ret
end
function LuaSkillWnd:GetChangeSkillTab()
    if self.m_TabBar.SelectedIndex == 1 then
        L10.Game.Guide.CGuideMgr.Inst:TriggerNextStep()
        return nil
    end
    return self.m_ChangeSkillTab
end
function LuaSkillWnd:GetXiuWeiRegion()
    local rtn = self.XiuweiRegion
    if rtn.activeInHierarchy then
        RegisterTickOnce(function ( ... )
            L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
        end, 3000)
        return rtn
    end
    return nil
end

function LuaSkillWnd:GetYingLingSwitchSkillStateBtn()
    return self.m_SkillPreferenceView.m_LuaSelf:GetYingLingSwitchSkillStateBtn()
end

function LuaSkillWnd:GetTabIndex()
    return self.m_TabBar.SelectedIndex
end

function LuaSkillWnd:GoTo(data)
    local tab, section, row = data[0], data[1], data[2]
    self.m_LivingSkillView.m_LuaSelf:GoTo(tab, section, row)
end
