local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CRankData = import "L10.UI.CRankData"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CPlayerInfoMgrAlignType = import "CPlayerInfoMgr+AlignType"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
LuaYaYunHui2023LotteryRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaYaYunHui2023LotteryRankWnd, "NoneRankLabel", "NoneRankLabel", UILabel)
RegistChildComponent(LuaYaYunHui2023LotteryRankWnd, "TableView", "TableView", QnAdvanceGridView)

--@endregion RegistChildComponent end
RegistClassMember(LuaYaYunHui2023LotteryRankWnd, "m_RankTable")
function LuaYaYunHui2023LotteryRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_RankTable = {}
end

function LuaYaYunHui2023LotteryRankWnd:Init()
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RankTable
        end,
        function(item, index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.EvenBgSprite or g_sprites.OddBgSpirite)
            self:InitItem(item, self.m_RankTable[index + 1], index + 1)
        end
    )
    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local data = self.m_RankTable[row+1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,CPlayerInfoMgrAlignType.Default)
        end
    end)

    if CLuaRankData.m_CurRankId == AsianGames2023_Setting.GetData().GambleRankId then
        self:InitRankData()
    else
        Gac2Gas.QueryRank(AsianGames2023_Setting.GetData().GambleRankId)
    end
end

function LuaYaYunHui2023LotteryRankWnd:InitItem(item,data,index)
    if not item or not data then return end
    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankImage = rankLabel.transform:Find("RankImage"):GetComponent(typeof(UISprite))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local valueLabel = item.transform:Find("MoneyLabel"):GetComponent(typeof(UILabel))
    local bg = item.transform:GetComponent(typeof(UISprite))
    -- print(index,index % 2)
    -- if index % 2 == 0 then
    --     bg.spriteName = "common_bg_mission_background_s"
    -- else
    --     bg.spriteName = "common_bg_mission_background_n"
    -- end
    rankLabel.text = ""
    local rank = data.Rank
    if rank == 1 then
        rankImage.gameObject:SetActive(true)
        rankImage.spriteName = "Rank_No.1"
    elseif rank == 2 then
        rankImage.gameObject:SetActive(true)
        rankImage.spriteName = "Rank_No.2png"
    elseif rank == 3 then
        rankImage.gameObject:SetActive(true)
        rankImage.spriteName = "Rank_No.3png"
    else
        rankImage.gameObject:SetActive(false)
        rankLabel.text = tostring(rank)
    end
    nameLabel.text = data.Name
    valueLabel.text = data.Value
end

function LuaYaYunHui2023LotteryRankWnd:InitRankData()
    if CLuaRankData.m_CurRankId ~= AsianGames2023_Setting.GetData().GambleRankId then return end
    if CRankData.Inst.RankList and  CRankData.Inst.RankList.Count > 0 then
        self.m_RankTable = {}
        for i = 1, CRankData.Inst.RankList.Count do
            table.insert(self.m_RankTable, CRankData.Inst.RankList[i - 1])
        end
        self.TableView:ReloadData(true, false)
    end
end

function LuaYaYunHui2023LotteryRankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRankDataReady", self, "InitRankData")
end

function LuaYaYunHui2023LotteryRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "InitRankData")
end

--@region UIEvent

--@endregion UIEvent

