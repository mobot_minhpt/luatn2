-- Auto Generated!!
local Boolean = import "System.Boolean"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerShop_Focus = import "L10.UI.CPlayerShop_Focus"
local CPlayerShopItem = import "L10.UI.CPlayerShopItem"
local CPlayerShopItemData = import "L10.UI.CPlayerShopItemData"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CPlayerShopTemplateItem = import "L10.UI.CPlayerShopTemplateItem"
local CPlayerShopWnd = import "L10.UI.CPlayerShopWnd"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Int32 = import "System.Int32"
local ItemTemplateInfo = import "L10.UI.ItemTemplateInfo"
local LocalString = import "LocalString"
local QnButton = import "L10.UI.QnButton"
local SearchOption = import "L10.UI.SearchOption"
local String = import "System.String"
local UInt32 = import "System.UInt32"
CPlayerShop_Focus.m_Init_CS2LuaHook = function (this, publicity)    this.m_SystemRecommandTable.m_DataSource = this
    this.m_SearchResultTable.m_DataSource = this
    this.m_ContactButton.Visible = false
    this.m_Publicity = publicity
    this:InitRecommandItems()
    this:DoSearch(this.m_Publicity)
end
CPlayerShop_Focus.m_OnFoldButtonClick_CS2LuaHook = function (this, button) 
    this.m_FoldRecommand = not this.m_FoldRecommand
    local default
    if this.m_FoldRecommand then
        default = LocalString.GetString("所有推荐")
    else
        default = LocalString.GetString("收起")
    end
    button.Text = default
    Extensions.SetLocalRotationZ(this.m_TipSprite, this.m_FoldRecommand and 0 or 180)
    this.m_SystemRecommandTable:ReloadData(true, true)
    this.m_GlobalTable:Reposition()
end
CPlayerShop_Focus.m_OnQueryPlayerShopOnShelfNumResult_CS2LuaHook = function (this, selledCounts, publicity)    if publicity then
        return
    end
    do
        local i = 0
        while i < selledCounts.Count do
            local id = selledCounts[i]
            local count = selledCounts[i + 1]
            do
                local j = 0
                while j < this.m_RecommandData.Count do
                    if this.m_RecommandData[j].TemplateId == id then
                        this.m_RecommandData[j].SellCount = count
                        local item = TypeAs(this.m_SystemRecommandTable:GetItemAtRow(j), typeof(CPlayerShopTemplateItem))
                        if item ~= nil then
                            item:UpdateData(this.m_RecommandData[j].TemplateId, this.m_RecommandData[j].SellCount, this.m_RecommandData[j].IsFocus, false, this.m_RecommandData[j].IsSystemRecommand)
                        end
                        break
                    end
                    j = j + 1
                end
            end
            do
                local j = 0
                while j < this.m_FocusTemplateData.Count do
                    if this.m_FocusTemplateData[j].TemplateId == id then
                        this.m_FocusTemplateData[j].SellCount = count
                        local item = TypeAs(this.m_SearchResultTable:GetItemAtRow(j), typeof(CPlayerShopTemplateItem))
                        if item ~= nil then
                            item:UpdateData(this.m_FocusTemplateData[j].TemplateId, this.m_FocusTemplateData[j].SellCount, this.m_FocusTemplateData[j].IsFocus, this.m_IsEditMode, this.m_FocusTemplateData[j].IsSystemRecommand)
                        end
                    end
                    j = j + 1
                end
            end
            i = i + 2
        end
    end
end
CPlayerShop_Focus.m_OnEnable_CS2LuaHook = function (this) 
    LuaPlayerShop_Focus:OnEnable(this)
    this.m_IsEditMode = false
    this.m_FocusButton.Text = LocalString.GetString("编辑关注")
    this.m_SearchResultTable.OnSelectAtRow = MakeDelegateFromCSFunction(this.OnSelectSearchResultTable, MakeGenericClass(Action1, Int32), this)
    this.m_SystemRecommandTable.OnSelectAtRow = MakeDelegateFromCSFunction(this.OnSelectRecommandTable, MakeGenericClass(Action1, Int32), this)
    this.m_BuyButton.OnClick = MakeDelegateFromCSFunction(this.OnClickBuyButton, MakeGenericClass(Action1, QnButton), this)
    this.m_FocusButton.OnClick = MakeDelegateFromCSFunction(this.OnFocusButtonClick, MakeGenericClass(Action1, QnButton), this)
    this.m_ContactButton.OnClick = MakeDelegateFromCSFunction(this.OnContactButtonClick, MakeGenericClass(Action1, QnButton), this)
    this.m_FoldButton.OnClick = MakeDelegateFromCSFunction(this.OnFoldButtonClick, MakeGenericClass(Action1, QnButton), this)
    EventManager.AddListenerInternal(EnumEventType.QueryPlayerShopItemCollectionsResult, MakeDelegateFromCSFunction(this.OnRecieveSearchResult, MakeGenericClass(Action1, MakeGenericClass(List, CPlayerShopItemData)), this))
    EventManager.AddListenerInternal(EnumEventType.BuyItemFromPlayerShopSuccess, MakeDelegateFromCSFunction(this.OnBuyItemFromPlayerShopSuccess, MakeGenericClass(Action3, UInt32, String, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.QueryPlayerShopOnShelfNumResult, MakeDelegateFromCSFunction(this.OnQueryPlayerShopOnShelfNumResult, MakeGenericClass(Action2, MakeGenericClass(List, UInt32), Boolean), this))
end
CPlayerShop_Focus.m_OnDisable_CS2LuaHook = function (this) 
    LuaPlayerShop_Focus:OnDisable()
    this.m_IsEditMode = false
    this.m_FocusButton.Text = LocalString.GetString("编辑关注")
    EventManager.RemoveListenerInternal(EnumEventType.QueryPlayerShopItemCollectionsResult, MakeDelegateFromCSFunction(this.OnRecieveSearchResult, MakeGenericClass(Action1, MakeGenericClass(List, CPlayerShopItemData)), this))
    EventManager.RemoveListenerInternal(EnumEventType.BuyItemFromPlayerShopSuccess, MakeDelegateFromCSFunction(this.OnBuyItemFromPlayerShopSuccess, MakeGenericClass(Action3, UInt32, String, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.QueryPlayerShopOnShelfNumResult, MakeDelegateFromCSFunction(this.OnQueryPlayerShopOnShelfNumResult, MakeGenericClass(Action2, MakeGenericClass(List, UInt32), Boolean), this))
end
CPlayerShop_Focus.m_SyncTemplateFocus_CS2LuaHook = function (this) 
    --重新设置关注
    local set1 = CreateFromClass(MakeGenericClass(HashSet, UInt32))
    local removeList = CreateFromClass(MakeGenericClass(List, ItemTemplateInfo))
    do
        local i = 0
        while i < this.m_FocusTemplateData.Count do
            if not this.m_FocusTemplateData[i].IsSystemRecommand then
                if this.m_FocusTemplateData[i].IsFocus then
                    CommonDefs.HashSetAdd(set1, typeof(UInt32), this.m_FocusTemplateData[i].TemplateId)
                else
                    CommonDefs.ListAdd(removeList, typeof(ItemTemplateInfo), this.m_FocusTemplateData[i])
                end
            end
            i = i + 1
        end
    end
    do
        local i = 0
        while i < removeList.Count do
            CommonDefs.ListRemove(this.m_FocusTemplateData, typeof(ItemTemplateInfo), removeList[i])
            i = i + 1
        end
    end
    CPlayerShopMgr.Inst:SetAllFocusTemplate(set1)
end
CPlayerShop_Focus.m_SyncItemFocus_CS2LuaHook = function (this) 
    --单个取消
    local m_CancelList = CreateFromClass(MakeGenericClass(List, CPlayerShopItemData))
    do
        local i = 0
        while i < this.m_SearchResultData.Count do
            if not this.m_SearchResultData[i].IsFocus then
                CommonDefs.ListAdd(m_CancelList, typeof(CPlayerShopItemData), this.m_SearchResultData[i])
            end
            i = i + 1
        end
    end
    Gac2Gas.CancelCollectPlayerShopItemBegin()
    do
        local i = 0
        while i < m_CancelList.Count do
            local data = m_CancelList[i]
            local item = data.Item
            if item ~= nil then
                Gac2Gas.CancelCollectPlayerShopItem(data.ShopId, data.ShelfPlace, item.Id)
            end
            CommonDefs.ListRemove(this.m_SearchResultData, typeof(CPlayerShopItemData), m_CancelList[i])
            i = i + 1
        end
    end
    Gac2Gas.CancelCollectPlayerShopItemEnd()
end
CPlayerShop_Focus.m_OnFocusButtonClick_CS2LuaHook = function (this, button)    if this.m_IsEditMode then
        if not this.m_Publicity then
            this:SyncTemplateFocus()
        end
        if LuaPlayerShop_Focus:CheckConfirmDisplaySplitItemId(data) then
            local msg = g_MessageMgr:FormatMessage("PLAYER_SHOP_DISPLAY_SPLIT_CANCEL_FOLLOW" )
            MessageWndManager.ShowOKMessage(msg, DelegateFactory.Action(function ()
                this:SyncItemFocus()
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("关注信息同步成功"))
                this.m_SearchResultTable:ReloadData(true, true)
            end))
        else
            this:SyncItemFocus()
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("关注信息同步成功"))
        end
    else
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("不再关注的可取消勾选"))
    end
    this.m_SelectItemIndex = - 1
    this.m_IsEditMode = not this.m_IsEditMode
    local default
    if this.m_IsEditMode then
        default = LocalString.GetString("确定")
    else
        default = LocalString.GetString("编辑关注")
    end
    this.m_FocusButton.Text = default
    this.m_SearchResultTable:ReloadData(true, true)
end
CPlayerShop_Focus.m_OnContactButtonClick_CS2LuaHook = function (this, button)    --联系店主
    if this.m_SelectItemIndex < 0 or this.m_SelectItemIndex >= this.m_SearchResultData.Count then
        g_MessageMgr:ShowMessage("PLAYERSHOP_NO_SELECTION")
        return
    end
    local data = this.m_SearchResultData[this.m_SelectItemIndex]
    CPlayerShopMgr.Inst:ContactShopOwner(data.OwnerId, data.OwnerName)
end
CPlayerShop_Focus.m_OnClickBuyButton_CS2LuaHook = function (this, button)
    if this.m_SelectItemIndex < 0 or this.m_SelectItemIndex >= this.m_SearchResultData.Count then
        g_MessageMgr:ShowMessage("PLAYERSHOP_NO_SELECTION")
        return
    end
    local data = this.m_SearchResultData[this.m_SelectItemIndex]
    if not CPlayerShopMgr.Inst:GetCanOnlyBatchBuy(data.Item.TemplateId) then
        CPlayerShopMgr.Inst:BuyItemFromShop(data)
    else
        CPlayerShopMgr.BatchBuyTemplateId = data.Item.TemplateId
        CPlayerShopMgr.BatchBuyRecommendCount = 1
        CPlayerShopMgr.BatchBuyMaxCount = 999
        CPlayerShopMgr.BatchBuyMinCount = 1
        CPlayerShopMgr.BatchBuyTemplateName = data.Item.Name
        CUIManager.ShowUI(CUIResources.PlayerShopBatchBuyWnd)
    end
end
CPlayerShop_Focus.m_OnSelectSearchResultTable_CS2LuaHook = function (this, index)    if index < this.m_FocusTemplateData.Count then
        if not this.m_IsEditMode then
            this.m_SelectItemIndex = - 1
            CPlayerShopWnd.Inst:GotoShopItemInfo(this.m_FocusTemplateData[index].TemplateId, SearchOption.All)
        end
    elseif this.m_SelectItemIndex ~= index - this.m_FocusTemplateData.Count then
        this.m_SelectItemIndex = index - this.m_FocusTemplateData.Count
        local data = this.m_SearchResultData[this.m_SelectItemIndex]
        if data ~= nil then
            this.m_ContactButton.gameObject:SetActive(data.Item.IsPrecious)
            if CPlayerShopMgr.Inst:GetCanOnlyBatchBuy(data.Item.TemplateId) then
                this.m_BuyButton.Text = LocalString.GetString("批量购买")
            else
                this.m_BuyButton.Text = LocalString.GetString("购买")
            end
        end
    end
end
CPlayerShop_Focus.m_OnSelectRecommandTable_CS2LuaHook = function (this, index) 
    if index < this.m_RecommandData.Count then
        if not this.m_IsEditMode then
            this.m_SelectItemIndex = - 1
            CPlayerShopWnd.Inst:GotoShopItemInfo(this.m_RecommandData[index].TemplateId, SearchOption.All)
        end
    end
end
CPlayerShop_Focus.m_OnRecieveSearchResult_CS2LuaHook = function (this, data)
    this.m_SearchResultData = LuaPlayerShopMgr.SplitSearchResultData(data)
    this.m_SearchResultTable:ReloadData(true, false)
    this.m_GlobalTable:Reposition()
    this.m_SelectItemIndex = - 1
    this.m_NoShopItemLabel:SetActive(this.m_SearchResultData.Count == 0 and this.m_RecommandData.Count == 0 and this.m_FocusTemplateData.Count == 0)
    if data.Count > 0 then
        this.m_SearchResultTable:SetSelectRow(this.m_FocusTemplateData.Count, true)
    end
end
CPlayerShop_Focus.m_NumberOfRows_CS2LuaHook = function (this, view) 
    if view == this.m_SystemRecommandTable then
        if this.m_FoldRecommand then
            return math.min(this.m_RecommandData.Count, 2)
        end
        return this.m_RecommandData.Count
    else
        return this.m_SearchResultData.Count + this.m_FocusTemplateData.Count
    end
end
CPlayerShop_Focus.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if view == this.m_SystemRecommandTable then
        local cell = TypeAs(view:GetFromPool(1), typeof(CPlayerShopTemplateItem))
        local info = this.m_RecommandData[row]
        cell:UpdateData(info.TemplateId, info.SellCount, info.IsFocus, false, info.IsSystemRecommand)
        return cell
    else
        if row < this.m_FocusTemplateData.Count then
            local info = this.m_FocusTemplateData[row]
            local cell = TypeAs(view:GetFromPool(1), typeof(CPlayerShopTemplateItem))
            cell:UpdateData(info.TemplateId, info.SellCount, info.IsFocus, this.m_IsEditMode, info.IsSystemRecommand)
            cell.OnFocusValueChanged = (DelegateFactory.Action_bool(function (check) 
                info.IsFocus = check
            end))
            return cell
        else
            local itemdata = this.m_SearchResultData[row - this.m_FocusTemplateData.Count]
            local cell = TypeAs(view:GetFromPool(0), typeof(CPlayerShopItem))
            cell:UpdateData(itemdata, false, this.m_IsEditMode)
            cell.OnFocusValueChanged = (DelegateFactory.Action_bool(function (check) 
                for i = 0,  this.m_SearchResultData.Count - 1 do
                    local item = this.m_SearchResultData[i]
                    if item.ShopId == itemdata.ShopId and item.ShelfPlace == itemdata.ShelfPlace then
                        item.IsFocus = check
                    end
                end
            end))
            return cell
        end
    end
end

LuaPlayerShop_Focus = {}
LuaPlayerShop_Focus.m_Wnd = nil

function LuaPlayerShop_Focus:CheckConfirmDisplaySplitItemId(data)
    if not self.m_Wnd then return false end
    local splitItemIdMap = {}
    for i = 0, self.m_Wnd.m_SearchResultData.Count - 1 do
        local data = self.m_Wnd.m_SearchResultData[i]
        if data.Item and not data.IsFocus and LuaPlayerShopMgr.IsDisplaySplitItemId(data.Item.TemplateId) then
            splitItemIdMap[data.Item.TemplateId] = true
        end
    end
    local showMsg = false
    for templateId, _ in pairs(splitItemIdMap) do
        local num = 0
        for i = 0, self.m_Wnd.m_SearchResultData.Count - 1 do
            local data = self.m_Wnd.m_SearchResultData[i]
            if data.Item and data.Item.TemplateId == templateId then
                num = num + 1
            end
        end
        if num > 1 then
            showMsg = true
        end
    end
    return showMsg
end

function LuaPlayerShop_Focus:OnEnable(this)
    self.m_Wnd = this
end

function LuaPlayerShop_Focus:OnDisable()
    self.m_Wnd = nil
end