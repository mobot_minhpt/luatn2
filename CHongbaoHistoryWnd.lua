-- Auto Generated!!
local CHongbaoHistoryWnd = import "L10.UI.CHongbaoHistoryWnd"
local CHongbaoHistroyItem = import "L10.UI.CHongbaoHistroyItem"
local CHongBaoMgr = import "L10.UI.CHongBaoMgr"
local EnumHongbaoType = import "L10.UI.EnumHongbaoType"
local LocalString = import "LocalString"
CHongbaoHistoryWnd.m_Init_CS2LuaHook = function (this) 
    this.m_Table.m_DataSource = this
    this.m_GiftTable.m_DataSource = this
    this.m_HongbaoRoot:SetActive(CHongBaoMgr.m_HongbaoType ~= EnumHongbaoType.Gift and CHongBaoMgr.m_HongbaoType ~= EnumHongbaoType.SectHongbao)
    this.m_SectHongbaoRoot:SetActive(CHongBaoMgr.m_HongbaoType == EnumHongbaoType.SectHongbao)
    this.m_GiftRoot:SetActive(CHongBaoMgr.m_HongbaoType == EnumHongbaoType.Gift)
    this.m_TitleLabel.text = 
        CHongBaoMgr.m_HongbaoType == EnumHongbaoType.Gift and LocalString.GetString("我的礼包记录") or 
        (CHongBaoMgr.m_HongbaoType == EnumHongbaoType.SectHongbao and LocalString.GetString("我的福利记录") or 
        LocalString.GetString("我的红包记录"))
    if CHongBaoMgr.m_HongbaoType == EnumHongbaoType.Gift then
        Gac2Gas.RequestDaoJuLiBaoHistoryRecord()
    elseif CHongBaoMgr.m_HongbaoType == EnumHongbaoType.SectHongbao then
        Gac2Gas.RequestSectItemRedPackHistory()
    else
        Gac2Gas.RequestHongBaoHistoryRecord()
    end
end
CHongbaoHistoryWnd.m_OnReplyHongBaoHistoryRecord_CS2LuaHook = function (this, data) 
    this.m_HistoryData = data
    if this.m_HistoryData ~= nil then
        if CHongBaoMgr.m_HongbaoType == EnumHongbaoType.Gift then
            this.m_GiftYiFaCountLabel.text = System.String.Format(LocalString.GetString("{0}个"), this.m_HistoryData.SendNum)
            this.m_GiftYiQiangCountLabel.text = System.String.Format(LocalString.GetString("{0}个"), this.m_HistoryData.SnatchNum)
            this.m_GiftTable:ReloadData(false, false)
        else
            this.m_YiQiangMoneyLabel.text = tostring(this.m_HistoryData.SnachSilver)
            this.m_YiFaMoneyLabel.text = tostring(this.m_HistoryData.SendSilver)
            this.m_YiQiangCountLabel.text = System.String.Format(LocalString.GetString("{0}个"), this.m_HistoryData.SnatchNum)
            this.m_YiFaCountLabel.text = System.String.Format(LocalString.GetString("{0}个"), this.m_HistoryData.SendNum)
            this.m_Table:ReloadData(false, false)
        end
    end
end
CHongbaoHistoryWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 

    local item = TypeAs(view:GetFromPool(0), typeof(CHongbaoHistroyItem))
    if this.m_HistoryData ~= nil and this.m_HistoryData.HistoryRecords ~= nil and this.m_HistoryData.HistoryRecords.Count > row then
        local record = this.m_HistoryData.HistoryRecords[row]
        item:UpdateData(record)
    end
    return item
end
