local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"

LuaJuDianBattleConsecrateView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJuDianBattleConsecrateView, "Fire", "Fire", GameObject)
RegistChildComponent(LuaJuDianBattleConsecrateView, "Wood", "Wood", GameObject)
RegistChildComponent(LuaJuDianBattleConsecrateView, "Ice", "Ice", GameObject)
RegistChildComponent(LuaJuDianBattleConsecrateView, "Wind", "Wind", GameObject)
RegistChildComponent(LuaJuDianBattleConsecrateView, "ActivateValueLabel", "ActivateValueLabel", UILabel)
RegistChildComponent(LuaJuDianBattleConsecrateView, "FireProgressBar", "FireProgressBar", UISlider)
RegistChildComponent(LuaJuDianBattleConsecrateView, "WoodProgressBar", "WoodProgressBar", UISlider)
RegistChildComponent(LuaJuDianBattleConsecrateView, "IceProgressBar", "IceProgressBar", UISlider)
RegistChildComponent(LuaJuDianBattleConsecrateView, "WindProgressBar", "WindProgressBar", UISlider)
RegistChildComponent(LuaJuDianBattleConsecrateView, "FireProgressLabel", "FireProgressLabel", UILabel)
RegistChildComponent(LuaJuDianBattleConsecrateView, "WoodProgressLabel", "WoodProgressLabel", UILabel)
RegistChildComponent(LuaJuDianBattleConsecrateView, "IceProgressLabel", "IceProgressLabel", UILabel)
RegistChildComponent(LuaJuDianBattleConsecrateView, "WindProgressLabel", "WindProgressLabel", UILabel)
RegistChildComponent(LuaJuDianBattleConsecrateView, "FireValLabel", "FireValLabel", UILabel)
RegistChildComponent(LuaJuDianBattleConsecrateView, "WoodValLabel", "WoodValLabel", UILabel)
RegistChildComponent(LuaJuDianBattleConsecrateView, "IceValLabel", "IceValLabel", UILabel)
RegistChildComponent(LuaJuDianBattleConsecrateView, "WindValLabel", "WindValLabel", UILabel)
RegistChildComponent(LuaJuDianBattleConsecrateView, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaJuDianBattleConsecrateView, "GongFengButton", "GongFengButton", GameObject)
RegistChildComponent(LuaJuDianBattleConsecrateView, "Monster", "Monster", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaJuDianBattleConsecrateView,"m_ResourceID")
RegistClassMember(LuaJuDianBattleConsecrateView,"m_MaxResourceCount")
RegistClassMember(LuaJuDianBattleConsecrateView,"m_ResourceCount")
RegistClassMember(LuaJuDianBattleConsecrateView,"m_ShengTanList")
RegistClassMember(LuaJuDianBattleConsecrateView,"m_ShengTanFxList")
RegistClassMember(LuaJuDianBattleConsecrateView,"m_MonsterNPCId")

function LuaJuDianBattleConsecrateView:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

	UIEventListener.Get(self.GongFengButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGongFengButtonClick()
	end)

    --@endregion EventBind end
end

function LuaJuDianBattleConsecrateView:Start()
    local setData = GuildOccupationWar_DailySetting.GetData()
    self.m_ResourceID = setData.GongFengMaterialId
    self.m_MaxResourceCount = setData.GongFengMaterialNeed
    self.m_MonsterNPCId = setData.MonsterNPCId
    self.m_ShengTanList = {self.Fire, self.Wood, self.Ice, self.Wind}
    self.m_ShengTanFxList = {"fx/ui/prefab/UI_shentan_huo.prefab","fx/ui/prefab/UI_shentan_mu.prefab","fx/ui/prefab/UI_shentan_bing.prefab","fx/ui/prefab/UI_shentan_feng.prefab"}

    local data = {0,0,0,0,0,0,0,0}
    self:InitShengTan(data)
    self:ShowResourceInfo(data)
    self:InitMonsterInfo()
end

function LuaJuDianBattleConsecrateView:InitShengTan(altarInfo)
    local activedNum = 0
    for i = 1, 4 do
        local show = altarInfo[i + 4] == 1
        if show then activedNum = activedNum + 1 end
        self:ShowShengTan(i, show)
    end
    self.ActivateValueLabel.text = SafeStringFormat3("%d/2",activedNum) 
end

function LuaJuDianBattleConsecrateView:ShowResourceInfo(altarInfo)
    local valLabelArr = {self.FireValLabel, self.WoodValLabel, self.IceValLabel, self.WindValLabel}
    local progressBarArr = {self.FireProgressBar, self.WoodProgressBar, self.IceProgressBar, self.WindProgressBar}
    local progressLabelArr = {self.FireProgressLabel, self.WoodProgressLabel, self.IceProgressLabel, self.WindProgressLabel}
    self.m_ResourceCount = {}
    for i = 1, #valLabelArr do
        local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_ResourceID[i - 1])
        self.m_ResourceCount[i] = count
        valLabelArr[i].text = tostring(count)
        progressBarArr[i].value = altarInfo[i] / self.m_MaxResourceCount[i - 1]
        progressLabelArr[i].text = SafeStringFormat3("%d/%d", altarInfo[i], self.m_MaxResourceCount[i - 1])
    end
end

function LuaJuDianBattleConsecrateView:ShowShengTan(index, show)
    local obj = self.m_ShengTanList[index]
    local tex = obj.transform:Find("Texture"):GetComponent(typeof(UITexture))
    local fx = obj.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    tex.alpha = show and 1 or 0.3
    if show then
        local fxPath =  self.m_ShengTanFxList[index]
        fx:LoadFx(fxPath)
    else
        fx:DestroyFx()
    end
end

function LuaJuDianBattleConsecrateView:OnJuDianGongFengData(data)
    self:InitShengTan(data)
    self:ShowResourceInfo(data)
end

function LuaJuDianBattleConsecrateView:InitMonsterInfo()
    local monsterData = NPC_NPC.GetData(self.m_MonsterNPCId)
    self.Monster.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(monsterData.Portrait, false)
    self.Monster.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = monsterData.Name
	self.Monster.transform:Find("NumLabel"):GetComponent(typeof(UILabel)).text = tostring(0)
    local button = self.Monster.transform:Find("Button")
    UIEventListener.Get(button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CUIManager.ShowUI(CLuaUIResources.JuDianBattleDailySceneViewWnd)
	end)
end

function LuaJuDianBattleConsecrateView:OnMonsterData()
	local data = LuaJuDianBattleMgr.SceneMonsterData or 0
	local count = 0
	for mapId, list in pairs(data) do
		count = count + #list
	end
	self.Monster.transform:Find("NumLabel"):GetComponent(typeof(UILabel)).text = tostring(count)
end

--@region UIEvent
function LuaJuDianBattleConsecrateView:OnTipButtonClick()
    g_MessageMgr:ShowMessage("GUILD_JUDIAN_GONGFENG_INTERFACE_TIPS")
end

function LuaJuDianBattleConsecrateView:OnGongFengButtonClick()
    Gac2Gas.GuildJuDianSubmitGongFengMaterial(self.m_ResourceCount[1], self.m_ResourceCount[2], self.m_ResourceCount[3], self.m_ResourceCount[4])
end

--@endregion UIEvent
function LuaJuDianBattleConsecrateView:OnEnable()
    g_ScriptEvent:AddListener("GuildJuDianQueryGuildGongFengDataResult", self, "OnJuDianGongFengData")
    g_ScriptEvent:AddListener("JuDianBattle_SyncDailySceneData", self, "OnMonsterData")
	Gac2Gas.GuildJuDianQuerySceneOverview()
    Gac2Gas.GuildJuDianQueryGuildGongFengData()
end

function LuaJuDianBattleConsecrateView:OnDisable()
    g_ScriptEvent:RemoveListener("GuildJuDianQueryGuildGongFengDataResult", self, "OnJuDianGongFengData")
    g_ScriptEvent:RemoveListener("JuDianBattle_SyncDailySceneData", self, "OnMonsterData")
end
