local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local UITable = import "UITable"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local Money = import "L10.Game.Money"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaGuildTerritorialWarsResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuildTerritorialWarsResultWnd, "SeasonLabel", "SeasonLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsResultWnd, "RankLabel", "RankLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsResultWnd, "GuildNameLabel", "GuildNameLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsResultWnd, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsResultWnd, "SituationButton", "SituationButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsResultWnd, "OccupyNumLabel", "OccupyNumLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsResultWnd, "SlaveGuildLabel", "SlaveGuildLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsResultWnd, "OccupyScoreLabel", "OccupyScoreLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsResultWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsResultWnd, "RewardsGrid", "RewardsGrid", UIGrid)
RegistChildComponent(LuaGuildTerritorialWarsResultWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsResultWnd, "ScoreTable", "ScoreTable", UITable)
RegistChildComponent(LuaGuildTerritorialWarsResultWnd, "RewardScoreTemplate", "RewardScoreTemplate", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsResultWnd, "CloseButton", "CloseButton", GameObject)

--@endregion RegistChildComponent end

function LuaGuildTerritorialWarsResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)


	
	UIEventListener.Get(self.SituationButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSituationButtonClick()
	end)


    --@endregion EventBind end
end

function LuaGuildTerritorialWarsResultWnd:Init()
	self.ItemTemplate.gameObject:SetActive(false)
	self.RewardScoreTemplate.gameObject:SetActive(false)
	local info = LuaGuildTerritorialWarsMgr.m_PlayWeeklyResultInfo
	self.SeasonLabel.text = SafeStringFormat3(LocalString.GetString("第%s赛季第%s周"),CUICommonDef.IntToChinese(info.seasonId),CUICommonDef.IntToChinese(info.weekPassed))
	self.RankLabel.text = info.rank
	self.GuildNameLabel.text = info.guildName
	self.OccupyNumLabel.text = SafeStringFormat3(LocalString.GetString("%d/%d"),info.gridCount,GuildTerritoryWar_Setting.GetData().MaxOccupyAreaNum)
	self.SlaveGuildLabel.text = info.slaveCount
	self.OccupyScoreLabel.text = info.guildScore
	self.ScoreLabel.text = info.playerScore
	self:InitRewards()
end

function LuaGuildTerritorialWarsResultWnd:InitRewards()
	local info = LuaGuildTerritorialWarsMgr.m_PlayWeeklyResultInfo
	Extensions.RemoveAllChildren(self.RewardsGrid.transform)
	self:InitRewardItem1({moneyKey = EnumMoneyType.GuildContri, num = info.contribution})
	self:InitRewardItem1({moneyKey = EnumMoneyType.MingWang, num = info.mingwang})
	for i = 1,#info.itemInfoArr, 2 do
		local itemId, num = info.itemInfoArr[i], info.itemInfoArr[i + 1]
		self:InitRewardItem1({itemId = itemId, num = num})
	end
	self.RewardsGrid:Reposition()
	Extensions.RemoveAllChildren(self.ScoreTable.transform)
	self:InitRewardScoreItem2({moneyKey = EnumMoneyType.Exp, num = info.exp})
	self:InitRewardScoreItem2({moneyKey = EnumMoneyType.YinPiao, num = info.silver})
	self.ScoreTable:Reposition()
end

function LuaGuildTerritorialWarsResultWnd:InitRewardItem1(info)
	local obj = NGUITools.AddChild(self.RewardsGrid.gameObject, self.ItemTemplate.gameObject)
	obj.gameObject:SetActive(true)
	local icon = obj.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local sprite = obj.transform:Find("Sprite"):GetComponent(typeof(UISprite))
	local numLabel = obj.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
	sprite.spriteName = ""
	if info.scorekey then
		icon:LoadMaterial(LuaShiTuTrainingHandbookMgr:GetPlayScore(info.scorekey).ScoreIcon)
	elseif info.moneyKey then
		sprite.spriteName = Money.GetIconName(info.moneyKey, EnumPlayScoreKey.NONE)
	elseif info.itemId then
		local itemData = Item_Item.GetData(info.itemId)
		if itemData then
			icon:LoadMaterial(itemData.Icon)
		end
		UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function()
			CItemInfoMgr.ShowLinkItemTemplateInfo(info.itemId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
		end)
	end
	numLabel.text = info.num > 1 and info.num or ""
end

function LuaGuildTerritorialWarsResultWnd:InitRewardScoreItem2(info)
	local obj = NGUITools.AddChild(self.ScoreTable.gameObject, self.RewardScoreTemplate.gameObject)
	obj.gameObject:SetActive(true)
	local sprite = obj.transform:Find("Sprite"):GetComponent(typeof(UISprite))
	local numLabel = obj.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
	if info.moneyKey then
		sprite.spriteName = Money.GetIconName(info.moneyKey, EnumPlayScoreKey.NONE)
	end
	numLabel.text = info.num
end
--@region UIEvent

function LuaGuildTerritorialWarsResultWnd:OnShareButtonClick()
	self.ShareButton.gameObject:SetActive(false)
	self.SituationButton.gameObject:SetActive(false)
	self.CloseButton.gameObject:SetActive(false)
	CUICommonDef.CaptureScreen(
        "screenshot",
        true,
        false,
        nil,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
				self.ShareButton.gameObject:SetActive(true)
				self.SituationButton.gameObject:SetActive(true)
				self.CloseButton.gameObject:SetActive(true)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            end
        ),
        false
    )
end

function LuaGuildTerritorialWarsResultWnd:OnSituationButtonClick()
	Gac2Gas.RequestTerritoryWarScoreData()
end

--@endregion UIEvent

