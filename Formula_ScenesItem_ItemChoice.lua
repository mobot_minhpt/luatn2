
------------------------------ HEADER ------------------------------

--加足够多的中文让enca能识别出来是utf-8的
local random = math.random
local max = math.max
local min = math.min
local abs = math.abs
local ceil = math.ceil
local floor = math.floor
local log = math.log
local cos = math.cos
local sin = math.sin
local pi = math.pi
local round = function (num, n)
    if n > 0 then
        local scale = math.pow(10, n-1)
        return math.floor(num / scale + 0.5) * scale
    elseif n < 0 then
        local scale = math.pow(10, n)
        return math.floor(num / scale + 0.5) * scale
    elseif n == 0 then
        return num
    end
end

local power = function(a, b) return a^b end
local TRUE, FALSE = true, false
local True, False = true, false

local function GetIdBySkillCls(character, skillCls)
	local skillProp = character:SkillProp()
	return skillProp:GetSkillCls2Id_At(skillCls)
end

local function GetGradeBySkillCls(character, skillCls)
	local skillId = g_SkillMgr:GetSkillIdWithDeltaByCls(character, skillCls)
	if not skillId then return 0 end
	return skillId % 100
end

local function GetOriGradeBySkillCls(character, skillCls)
	local skillId = GetIdBySkillCls(character, skillCls)
	if not skillId then return 0 end
	return skillId % 100
end

local function GetGradeByBuffCls(character, buffCls)
	return g_BuffMgr:GetBuffLvByCls(character, buffCls) or 0
end

local function ConcatId(cls, grade)
	return cls * 100 + grade
end

local function IsGhostEquipObject(obj)
	if obj and obj:TypeIs(EnumObjectType.Item) then
		return obj:IsGhostEquipObject()
	end
	return false
end

local function IsPurpleEquipObject(obj)
	if obj and obj:TypeIs(EnumObjectType.Item) then
		return obj:IsPurpleEquipObject()
	end
	return false
end

local function SceneEvent(obj, eventName, ...)
	AIEvent.BroadcastEventInScene(obj, eventName, ...)
end

local function IsCharacterDeath(d)
 return g_StatusMgr:IsCharacterDeath(d)
end

local function IsCharacterDizzy(d)
 return g_StatusMgr:IsCharacterDizzy(d)
end

local function IsCharacterSleep(d)
 return g_StatusMgr:IsCharacterSleep(d)
end

local function IsCharacterChaos(d)
 return g_StatusMgr:IsCharacterChaos(d)
end

local function IsCharacterBind(d)
 return g_StatusMgr:IsCharacterBind(d)
end

local function IsCharacterSilence(d)
 return g_StatusMgr:IsCharacterSilence(d)
end

local function IsCharacterBlind(d)
 return g_StatusMgr:IsCharacterBlind(d)
end

local function IsCharacterPetrify(d)
 return g_StatusMgr:IsCharacterPetrify(d)
end

local function IsCharacterTie(d)
 return g_StatusMgr:IsCharacterTie(d)
end


-- 这个方法不够通用,考虑放在其他地方
local function IsIdInEventTipsIdSet(tipId, toCheckId)
	if not Message_EventTips[tipId] then
		return false
	end
	
	return g_GasMessageMgr:IsIdInEventTipsIdSet(tipId, toCheckId)
end

local function PlayerTaskEvent(player, event, isShare, eventTimes)
	if not player:TypeIs(EnumObjectType.Player) then return end
	g_TaskMgr:OnPlayerTaskEvent(player, event, isShare, eventTimes)
end

local function GetPlayerClass(player)
	if player and player:TypeIs(EnumObjectType.Player) then
		return player:GetClass()
	end
end

local function GetPlayerGender(player)
	if player and player:TypeIs(EnumObjectType.Player) then
		return player:GetGender()
	end
end

local function IsSceneTemplateId(self, ...)
	local scene = self and self.GetScene and self:GetScene()
	local sceneTempId = scene and scene:GetTemplateId()

    local count = select("#", ...)
	for i = 1, count do
        local tempId = select(i, ...)
		if tempId == sceneTempId then
			return 1
		end
	end
	return 0
end

local function GetBuffClsCount(character, ...)
	return g_BuffMgr:GetBuffClsCount(character, ...)
end

local function GetForwardPixelPosByRange(character, range)
	if not character then
		LogCallContext_lua()
		return
	end

	local __RayInterceptByDir = function(vecFrom,dir,dist)
		local dx = math.cos(dir) * dist
		local dy = math.sin(dir) * dist
		if(dx > 0) then	dx = math.floor(dx)
		else dx = math.ceil(dx) end
		if(dy > 0) then dy = math.floor(dy)
		else dy = math.ceil(dy) end
		return { x = vecFrom.x + dx, y = vecFrom.y + dy}
	end

	if not range then
		range = 1
	end

	local pixelPos = {}
	pixelPos.x, pixelPos.y = character.m_EngineObject:GetPixelPosv()

	local direction = character:GetDirection()
	local radian = DegreeToRadian(direction)
	
    local rangeInPixel = CastGridValueToPixelValue_V2(range * 0.98);

    local retPixelPos = __RayInterceptByDir(pixelPos, radian, rangeInPixel)
    return retPixelPos.x, retPixelPos.y
end

local __STATIC_TABLE = {}
local __STATIC_TABLE_MT = {
    __newindex = function(t,k,v) error("Attempt to modify a readonly table.") end,
}

local function MAKE_STATIC_TABLE(tbl)
	if not rawget(_G, "PUB_RELEASE") then
        for _, v in pairs(tbl) do
            if type(v) == "table" then
               setmetatable(v, __STATIC_TABLE_MT)
            else
            	error("v is not table")
            end
        end
		setmetatable(tbl, __STATIC_TABLE_MT)
	end
end

local function IsInSameTeam(a, d)
	if not a or not a:TypeIs(EnumObjectType.Player) then return false end
	if not d or not d:TypeIs(EnumObjectType.Player) then return false end
	local playerId1 = a.m_Id
	local playerId2 = d.m_Id
	return g_ShadowTeamMgr:IsTwoPlayerInSameTeam(playerId1, playerId2) and true or false
end

local function IsInSameTeamGroup(a, d)
	if not a or not a:TypeIs(EnumObjectType.Player) then return false end
	if not d or not d:TypeIs(EnumObjectType.Player) then return false end
	local playerId1 = a.m_Id
	local playerId2 = d.m_Id
	return g_GasTeamGroupMgr:IsTwoPlayerInSameTeamGroup(playerId1, playerId2) and true or false
end

local function IsInSameGuild(a, d)
	if not a or not a:TypeIs(EnumObjectType.Player) then return false end
	if not d or not d:TypeIs(EnumObjectType.Player) then return false end
	local guildId1 = a:BasicProp():GetGuildId()
	local guildId2 = d:BasicProp():GetGuildId()
	return guildId1 ~= 0 and guildId1 == guildId2
end

------------------------------ HEADER ------------------------------


AllFormulas.ScenesItem_ItemChoice = AllFormulas.ScenesItem_ItemChoice or {}
if not rawget(_G, "__STATIC_TABLE_ScenesItem_ItemChoice_ChoiceAction") then
    rawset(_G, "__STATIC_TABLE_ScenesItem_ItemChoice_ChoiceAction", {})
end
local __STATIC_TABLE_ScenesItem_ItemChoice_ChoiceAction = rawget(_G, "__STATIC_TABLE_ScenesItem_ItemChoice_ChoiceAction")

AllFormulas.ScenesItem_ItemChoice[1] = AllFormulas.ScenesItem_ItemChoice[1] or {  }
AllFormulas.ScenesItem_ItemChoice[1].ChoiceAction = function(a, d, SourceInfo, ActionContext, e)
    g_ActionMgr:DoSingleAction(a, d, SourceInfo, ActionContext, "OpenSceneItemWnd", { "ThrowStoneWnd" })
end


AllFormulas.ScenesItem_ItemChoice[2] = AllFormulas.ScenesItem_ItemChoice[2] or {  }
AllFormulas.ScenesItem_ItemChoice[2].ChoiceAction = function(a, d, SourceInfo, ActionContext, e)
    g_ActionMgr:DoSingleAction(a, d, SourceInfo, ActionContext, "RingTheBellInLanRuoSi", { fxId = 88801215, duration = 5 })
end


AllFormulas.ScenesItem_ItemChoice[3] = AllFormulas.ScenesItem_ItemChoice[3] or {  }
AllFormulas.ScenesItem_ItemChoice[3].ChoiceAction = function(a, d, SourceInfo, ActionContext, e)
    g_ActionMgr:DoSingleAction(a, d, SourceInfo, ActionContext, "SwitchSceneItemStatus")
end


AllFormulas.ScenesItem_ItemChoice[4] = AllFormulas.ScenesItem_ItemChoice[4] or {  }
AllFormulas.ScenesItem_ItemChoice[4].ChoiceAction = AllFormulas.ScenesItem_ItemChoice[3].ChoiceAction


AllFormulas.ScenesItem_ItemChoice[5] = AllFormulas.ScenesItem_ItemChoice[5] or {  }
AllFormulas.ScenesItem_ItemChoice[5].ChoiceAction = AllFormulas.ScenesItem_ItemChoice[3].ChoiceAction


AllFormulas.ScenesItem_ItemChoice[6] = AllFormulas.ScenesItem_ItemChoice[6] or {  }
AllFormulas.ScenesItem_ItemChoice[6].ChoiceAction = AllFormulas.ScenesItem_ItemChoice[3].ChoiceAction


AllFormulas.ScenesItem_ItemChoice[7] = AllFormulas.ScenesItem_ItemChoice[7] or {  }
AllFormulas.ScenesItem_ItemChoice[7].ChoiceAction = AllFormulas.ScenesItem_ItemChoice[3].ChoiceAction


AllFormulas.ScenesItem_ItemChoice[8] = AllFormulas.ScenesItem_ItemChoice[8] or {  }
AllFormulas.ScenesItem_ItemChoice[8].ChoiceAction = AllFormulas.ScenesItem_ItemChoice[3].ChoiceAction


AllFormulas.ScenesItem_ItemChoice[9] = AllFormulas.ScenesItem_ItemChoice[9] or {  }
AllFormulas.ScenesItem_ItemChoice[9].ChoiceAction = AllFormulas.ScenesItem_ItemChoice[3].ChoiceAction


AllFormulas.ScenesItem_ItemChoice[10] = AllFormulas.ScenesItem_ItemChoice[10] or {  }
AllFormulas.ScenesItem_ItemChoice[10].ChoiceAction = AllFormulas.ScenesItem_ItemChoice[3].ChoiceAction


AllFormulas.ScenesItem_ItemChoice[11] = AllFormulas.ScenesItem_ItemChoice[11] or {  }
AllFormulas.ScenesItem_ItemChoice[11].ChoiceAction = AllFormulas.ScenesItem_ItemChoice[3].ChoiceAction


AllFormulas.ScenesItem_ItemChoice[12] = AllFormulas.ScenesItem_ItemChoice[12] or {  }
AllFormulas.ScenesItem_ItemChoice[12].ChoiceAction = function(a, d, SourceInfo, ActionContext, e)
    g_ActionMgr:DoSingleAction(a, d, SourceInfo, ActionContext, "SwitchSceneItemStatus", { "GuiFangRefresh1" })
end


AllFormulas.ScenesItem_ItemChoice[13] = AllFormulas.ScenesItem_ItemChoice[13] or {  }
AllFormulas.ScenesItem_ItemChoice[13].ChoiceAction = function(a, d, SourceInfo, ActionContext, e)
    g_ActionMgr:DoSingleAction(a, d, SourceInfo, ActionContext, "SwitchSceneItemStatus", { "GuiFangRefresh2" })
end


AllFormulas.ScenesItem_ItemChoice[14] = AllFormulas.ScenesItem_ItemChoice[14] or {  }
AllFormulas.ScenesItem_ItemChoice[14].ChoiceAction = function(a, d, SourceInfo, ActionContext, e)
    g_ActionMgr:DoSingleAction(a, d, SourceInfo, ActionContext, "SwitchSceneItemStatus", { "GuiFangRefresh3" })
end


AllFormulas.ScenesItem_ItemChoice[15] = AllFormulas.ScenesItem_ItemChoice[15] or {  }
AllFormulas.ScenesItem_ItemChoice[15].ChoiceAction = function(a, d, SourceInfo, ActionContext, e)
    g_ActionMgr:DoSingleAction(a, d, SourceInfo, ActionContext, "SwitchSceneItemStatus", { "GuiFangRefresh4" })
end


AllFormulas.ScenesItem_ItemChoice[16] = AllFormulas.ScenesItem_ItemChoice[16] or {  }
AllFormulas.ScenesItem_ItemChoice[16].ChoiceAction = function(a, d, SourceInfo, ActionContext, e)
    g_ActionMgr:DoSingleAction(a, d, SourceInfo, ActionContext, "SwitchSceneItemStatus")
end

