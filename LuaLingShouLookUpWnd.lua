local CLingShouModelTextureLoader=import "L10.UI.CLingShouModelTextureLoader"

local LingShou_LingShou = import "L10.Game.LingShou_LingShou"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local CUITexture = import "L10.UI.CUITexture"
local CLingShouBaseMgr = import "L10.Game.CLingShouBaseMgr"
local Collider=import "UnityEngine.Collider"
local CSkillInfoMgr=import "L10.UI.CSkillInfoMgr"

CLuaLingShouLookUpWnd = class()
RegistClassMember(CLuaLingShouLookUpWnd,"closeButton")
RegistClassMember(CLuaLingShouLookUpWnd,"titleLabel")
RegistClassMember(CLuaLingShouLookUpWnd,"nextBtn")
RegistClassMember(CLuaLingShouLookUpWnd,"preBtn")
RegistClassMember(CLuaLingShouLookUpWnd,"textureLoader")
RegistClassMember(CLuaLingShouLookUpWnd,"contentNode")
RegistClassMember(CLuaLingShouLookUpWnd,"statusSprite")
RegistClassMember(CLuaLingShouLookUpWnd,"m_IsJieBan")


RegistClassMember(CLuaLingShouLookUpWnd,"m_Button1")--出战
RegistClassMember(CLuaLingShouLookUpWnd,"m_Button1Label")--出战
RegistClassMember(CLuaLingShouLookUpWnd,"m_Button2")--助战
RegistClassMember(CLuaLingShouLookUpWnd,"m_Button2Label")--出战
RegistClassMember(CLuaLingShouLookUpWnd,"m_Button3")--结伴
RegistClassMember(CLuaLingShouLookUpWnd,"m_Button3Label")--出战

function CLuaLingShouLookUpWnd:Awake()
    local buttonsTf = self.transform:Find("Buttons")

    if CLuaLingShouOtherMgr.requestType == LuaEnumOtherLingShouRequestType.PlayerLingShouForProperySwitch and CClientMainPlayer.Inst then
        local attrGroup = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.AttrGroups,CLuaPropertySwitchWnd.m_GroupIndex)
        if attrGroup then
            buttonsTf.gameObject:SetActive(true)
            self.m_Button1 = buttonsTf:Find("Button1").gameObject
            self.m_Button1Label = self.m_Button1.transform:Find("Label"):GetComponent(typeof(UILabel))
            self.m_Button2 = buttonsTf:Find("Button2").gameObject
            self.m_Button2Label = self.m_Button2.transform:Find("Label"):GetComponent(typeof(UILabel))
            self.m_Button3 = buttonsTf:Find("Button3").gameObject
            self.m_Button3Label = self.m_Button3.transform:Find("Label"):GetComponent(typeof(UILabel))

            -- self:OnUpdateAttrGroupAt(CLuaPropertySwitchWnd.m_GroupIndex,LuaEnumAttrGroupItem.LingShou,attrGroup)
        end
    else
        buttonsTf.gameObject:SetActive(false)
    end


    self.m_IsJieBan=false
    self.nextBtn = self.transform:Find("NextBtn").gameObject
    self.preBtn = self.transform:Find("PreBtn").gameObject
    self.textureLoader = self.transform:Find("ClipPanel/Anchor/Left/ModelTexture"):GetComponent(typeof(CLingShouModelTextureLoader))
    self.contentNode = self.transform:Find("ClipPanel/Anchor")
    self.statusSprite = self.transform:Find("ClipPanel/Anchor/StatusSprite"):GetComponent(typeof(UISprite))

    self.preBtn:SetActive(false)
    self.nextBtn:SetActive(false)

	local context = CLuaLingShouOtherMgr.requestType == LuaEnumOtherLingShouRequestType.PlayerLingShouForProperySwitch and 1 or 0
    UIEventListener.Get(self.nextBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        local list = CLuaLingShouOtherMgr.lingShouOverviewList
        local index = 0
        for i,v in ipairs(list) do
            if v.lingshouId == CLuaLingShouOtherMgr.lingShouId then
                index = i
                break
            end
        end
        if index <#list then
            CLuaLingShouOtherMgr.lingShouId = list[index + 1].lingshouId
            CLuaLingShouOtherMgr.QueryLingShouInfo(CLuaLingShouOtherMgr.playerId, CLuaLingShouOtherMgr.lingShouId, context)
        end
    end)
    UIEventListener.Get(self.preBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        local list = CLuaLingShouOtherMgr.lingShouOverviewList
        local index= 0
        for i,v in ipairs(list) do
            if v.lingshouId == CLuaLingShouOtherMgr.lingShouId then
                index = i
                break
            end
        end
        if index > 1 then
            CLuaLingShouOtherMgr.lingShouId = list[index - 1].lingshouId
            CLuaLingShouOtherMgr.QueryLingShouInfo(CLuaLingShouOtherMgr.playerId, CLuaLingShouOtherMgr.lingShouId, context)
        end
    end)

    self.statusSprite.enabled = false
end
function CLuaLingShouLookUpWnd:Init( )
    local playerName = CLuaLingShouOtherMgr.playerName
    local ownerLabel = self.transform:Find("ClipPanel/Anchor/OwnerName"):GetComponent(typeof(UILabel))
    if playerName and playerName~="" then
        ownerLabel.text = playerName
    else
        ownerLabel.text = nil
    end
    self:GetAllLingShouOverview()
end
function CLuaLingShouLookUpWnd:UpdateBtnState( )
    if CLuaLingShouOtherMgr.requestType == LuaEnumOtherLingShouRequestType.PlayerLingShou
        or CLuaLingShouOtherMgr.requestType == LuaEnumOtherLingShouRequestType.PlayerLingShouForProperySwitch then
        self.preBtn:SetActive(true)
        self.nextBtn:SetActive(true)
        local list = CLuaLingShouOtherMgr.lingShouOverviewList
        local index = 0
        for i,v in ipairs(list) do
            if v.lingshouId == CLuaLingShouOtherMgr.lingShouId then
                index = i
                break
            end
        end
        if index <= 1 then
            self.preBtn:SetActive(false)
        end
        if index >= #list then
            self.nextBtn:SetActive(false)
        end
    elseif CLuaLingShouOtherMgr.requestType == LuaEnumOtherLingShouRequestType.SingleLingShou then
        self.preBtn:SetActive(false)
        self.nextBtn:SetActive(false)
    end
end
function CLuaLingShouLookUpWnd:GetAllLingShouOverview( )
    --初始化列表
    if CLuaLingShouOtherMgr.requestType == LuaEnumOtherLingShouRequestType.PlayerLingShou then--请求玩家所有灵兽
        local list = CLuaLingShouOtherMgr.lingShouOverviewList
        if #list > 0 then
			-- local context = CLuaLingShouOtherMgr.requestType == LuaEnumOtherLingShouRequestType.PlayerLingShouForProperySwitch and 1 or 0
            CLuaLingShouOtherMgr.QueryLingShouInfo(CLuaLingShouOtherMgr.playerId, list[1].lingshouId, 0)
        end
    elseif CLuaLingShouOtherMgr.requestType == LuaEnumOtherLingShouRequestType.PlayerLingShouForProperySwitch then
        local list = CLuaLingShouOtherMgr.lingShouOverviewList
        if #list > 0 then
            -- local context = CLuaLingShouOtherMgr.requestType == LuaEnumOtherLingShouRequestType.PlayerLingShouForProperySwitch and 1 or 0
            if CLuaPropertySwitchWnd.m_SelectLingShouId~="" and CLuaPropertySwitchWnd.m_SelectLingShouId ~=nil then
                CLuaLingShouOtherMgr.QueryLingShouInfo(CLuaLingShouOtherMgr.playerId, CLuaPropertySwitchWnd.m_SelectLingShouId, 1)
            else
                CLuaLingShouOtherMgr.QueryLingShouInfo(CLuaLingShouOtherMgr.playerId, list[1].lingshouId, 1)
            end
        end
    elseif CLuaLingShouOtherMgr.requestType == LuaEnumOtherLingShouRequestType.SingleLingShou then--请求玩家单个灵兽
        local id = CLuaLingShouOtherMgr.lingShouId
        self:GetLingShouDetails(id, CLuaLingShouOtherMgr.GetLingShouDetails(id))
    end
end

function CLuaLingShouLookUpWnd:RefreshDisplay() 
    local details = CLuaLingShouOtherMgr.GetLingShouDetails(CLuaLingShouOtherMgr.lingShouId)
    if details then
        self:Fill(details)
        self:InitSkills(details.data.Props)
        self.textureLoader:Init(details)
    else
        self:InitNull()
    end
    self:UpdateBtnState()
end
function CLuaLingShouLookUpWnd:GetLingShouDetails( lingShouId, details) 
    --更新界面
    CLuaLingShouOtherMgr.lingShouId = lingShouId
    if CLuaLingShouOtherMgr.requestType == LuaEnumOtherLingShouRequestType.PlayerLingShouForProperySwitch  then
        local attrGroup = CClientMainPlayer.Inst and CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.AttrGroups,CLuaPropertySwitchWnd.m_GroupIndex)
        if attrGroup and attrGroup.BindedPartnerLingShouId==lingShouId then
            self.m_IsJieBan = true
        else
            self.m_IsJieBan = false
        end

        local buttonsTf = self.transform:Find("Buttons")
        if attrGroup then
            buttonsTf.gameObject:SetActive(true)
            self:OnUpdateAttrGroupAt(CLuaPropertySwitchWnd.m_GroupIndex,LuaEnumAttrGroupItem.LingShou,attrGroup)
        else
            buttonsTf.gameObject:SetActive(false)
            self:RefreshDisplay()
        end
    else
        self.m_IsJieBan = CLuaLingShouOtherMgr.partnerLingShouId == lingShouId

        if CLuaLingShouOtherMgr.lastLingShouId == lingShouId then
            self.statusSprite.enabled = true
            self.statusSprite.spriteName = "common_fight"
        elseif CLuaLingShouOtherMgr.assistLingShouId == lingShouId then
            self.statusSprite.enabled = true
            self.statusSprite.spriteName = "common_support"
        elseif CLuaLingShouOtherMgr.partnerLingShouId == lingShouId then
            self.statusSprite.enabled = true
            self.statusSprite.spriteName = "common_ban"
        elseif CLuaLingShouOtherMgr.futiLingShouId == lingShouId then
            self.statusSprite.enabled = true
            self.statusSprite.spriteName = "common_hua"
        else
            self.statusSprite.enabled = false
        end
        self:RefreshDisplay()
    end
end

function CLuaLingShouLookUpWnd:InitNull()
    self:Fill(nil)
    self:InitSkills(nil)
    self.textureLoader:InitNull()
    self.statusSprite.enabled = false
end

function CLuaLingShouLookUpWnd:OnEnable()
    g_ScriptEvent:AddListener("SendQueryLingShouInfoResult", self, "GetLingShouDetails")
	g_ScriptEvent:AddListener("UpdateAttrGroupAt", self,"OnUpdateAttrGroupAt")
    
end

function CLuaLingShouLookUpWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendQueryLingShouInfoResult", self, "GetLingShouDetails")
	g_ScriptEvent:RemoveListener("UpdateAttrGroupAt", self,"OnUpdateAttrGroupAt")
end

function CLuaLingShouLookUpWnd:OnUpdateAttrGroupAt(groupIdx,eItem,group)
    if CLuaLingShouOtherMgr.requestType ~= LuaEnumOtherLingShouRequestType.PlayerLingShouForProperySwitch  then return end

    self.m_IsJieBan = group.BindedPartnerLingShouId==CLuaLingShouOtherMgr.lingShouId
    self:RefreshDisplay()

    if eItem ==LuaEnumAttrGroupItem.LingShou or eItem ==LuaEnumAttrGroupItem.AssistLingShou or eItem ==LuaEnumAttrGroupItem.BindedPartnerLingShou then
        if CLuaLingShouOtherMgr.lingShouId == group.BattleLingShouId then
            self.statusSprite.enabled = true
            self.statusSprite.spriteName = "common_fight"

            --取消出战
            self.m_Button1Label.text = LocalString.GetString("取消出战")
            UIEventListener.Get(self.m_Button1).onClick = DelegateFactory.VoidDelegate(function(go) 
                Gac2Gas.RequestSetBattleLingShouInAttrGroup(CLuaPropertySwitchWnd.m_GroupIndex,"") 
            end)

            self.m_Button2Label.text = LocalString.GetString("设为助战")
            UIEventListener.Get(self.m_Button2).onClick = DelegateFactory.VoidDelegate(function(go) 
                g_MessageMgr:ShowMessage("ChangeConfig_LingShou_Cancel_ChuZhan")
                -- Gac2Gas.RequestSetAssistLingShouInAttrGroup(CLuaPropertySwitchWnd.m_GroupIndex,CLuaLingShouOtherMgr.lingShouId) 
            end)

            self.m_Button3Label.text = LocalString.GetString("设为结伴")
            UIEventListener.Get(self.m_Button3).onClick = DelegateFactory.VoidDelegate(function(go) 
                g_MessageMgr:ShowMessage("ChangeConfig_LingShou_Cancel_ChuZhan")
                -- Gac2Gas.RequestSetBindedPartnerLingShouInAttrGroup(CLuaPropertySwitchWnd.m_GroupIndex,CLuaLingShouOtherMgr.lingShouId) 
            end)

        elseif CLuaLingShouOtherMgr.lingShouId == group.AssistLingShouId then
            self.statusSprite.enabled = true
            self.statusSprite.spriteName = "common_support"

            self.m_Button1Label.text = LocalString.GetString("设为出战")
            UIEventListener.Get(self.m_Button1).onClick = DelegateFactory.VoidDelegate(function(go) 
                g_MessageMgr:ShowMessage("ChangeConfig_LingShou_Cancel_ZhuZhan")
                -- Gac2Gas.RequestSetBattleLingShouInAttrGroup(CLuaPropertySwitchWnd.m_GroupIndex,CLuaLingShouOtherMgr.lingShouId) 
            end)

            self.m_Button2Label.text = LocalString.GetString("取消助战")
            UIEventListener.Get(self.m_Button2).onClick = DelegateFactory.VoidDelegate(function(go) 
                Gac2Gas.RequestSetAssistLingShouInAttrGroup(CLuaPropertySwitchWnd.m_GroupIndex,"") 
            end)

            self.m_Button3Label.text = LocalString.GetString("设为结伴")
            UIEventListener.Get(self.m_Button3).onClick = DelegateFactory.VoidDelegate(function(go) 
                g_MessageMgr:ShowMessage("ChangeConfig_LingShou_Cancel_ZhuZhan")
                -- Gac2Gas.RequestSetBindedPartnerLingShouInAttrGroup(CLuaPropertySwitchWnd.m_GroupIndex,CLuaLingShouOtherMgr.lingShouId) 
            end)

        elseif CLuaLingShouOtherMgr.lingShouId == group.BindedPartnerLingShouId then
            self.statusSprite.enabled = true
            self.statusSprite.spriteName = "common_ban"

            self.m_Button1Label.text = LocalString.GetString("设为出战")
            UIEventListener.Get(self.m_Button1).onClick = DelegateFactory.VoidDelegate(function(go) 
                g_MessageMgr:ShowMessage("ChangeConfig_LingShou_Cancel_JieBan")
                -- Gac2Gas.RequestSetBattleLingShouInAttrGroup(CLuaPropertySwitchWnd.m_GroupIndex,CLuaLingShouOtherMgr.lingShouId) 
            end)
            self.m_Button2Label.text = LocalString.GetString("设为助战")
            UIEventListener.Get(self.m_Button2).onClick = DelegateFactory.VoidDelegate(function(go) 
                g_MessageMgr:ShowMessage("ChangeConfig_LingShou_Cancel_JieBan")
                -- Gac2Gas.RequestSetAssistLingShouInAttrGroup(CLuaPropertySwitchWnd.m_GroupIndex,CLuaLingShouOtherMgr.lingShouId) 
            end)
            self.m_Button3Label.text = LocalString.GetString("取消结伴")
            UIEventListener.Get(self.m_Button3).onClick = DelegateFactory.VoidDelegate(function(go) 
                Gac2Gas.RequestSetBindedPartnerLingShouInAttrGroup(CLuaPropertySwitchWnd.m_GroupIndex,"") 
            end)
        else
            self.statusSprite.enabled = false

            self.m_Button1Label.text = LocalString.GetString("设为出战")
            UIEventListener.Get(self.m_Button1).onClick = DelegateFactory.VoidDelegate(function(go) 
                Gac2Gas.RequestSetBattleLingShouInAttrGroup(CLuaPropertySwitchWnd.m_GroupIndex,CLuaLingShouOtherMgr.lingShouId) 
            end)
            self.m_Button2Label.text = LocalString.GetString("设为助战")
            UIEventListener.Get(self.m_Button2).onClick = DelegateFactory.VoidDelegate(function(go) 
                Gac2Gas.RequestSetAssistLingShouInAttrGroup(CLuaPropertySwitchWnd.m_GroupIndex,CLuaLingShouOtherMgr.lingShouId) 
            end)
            self.m_Button3Label.text = LocalString.GetString("设为结伴")
            UIEventListener.Get(self.m_Button3).onClick = DelegateFactory.VoidDelegate(function(go) 
                if CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.OwnBabyId ~= "" then
                    Gac2Gas.RequestSetBindedPartnerLingShouInAttrGroup(CLuaPropertySwitchWnd.m_GroupIndex,CLuaLingShouOtherMgr.lingShouId) 
                else
                    g_MessageMgr:ShowMessage("SXQH_JieBan_NeedBaby")
                end
            end)
        end
    end
end


function CLuaLingShouLookUpWnd:InitSkills(prop)
    local transform = self.transform:Find("ClipPanel/Anchor/Left/Slots")

    local first = transform:Find("Grid/1").gameObject
    local grid = transform:Find("Grid"):GetComponent(typeof(UIGrid))
    local babySkillSlot1 = transform:Find("BabySkillSlot1")
    local babySkillSlot2 = transform:Find("BabySkillSlot2")

    if not self.m_IsJieBan and prop and prop.Baby ~= nil and prop.Baby.BornTime > 0 then
        if babySkillSlot1 ~= nil and babySkillSlot2 ~= nil then
            babySkillSlot1.gameObject:SetActive(true)
            self:InitSkillSlot(babySkillSlot1,prop.Baby.SkillList[1])

            babySkillSlot2.gameObject:SetActive(true)
            self:InitSkillSlot(babySkillSlot2,prop.Baby.SkillList[2])

            grid.transform.localPosition = Vector3(0, 0)
        end
    else
        if babySkillSlot1 ~= nil and babySkillSlot2 ~= nil then
            babySkillSlot1.gameObject:SetActive(false)
            babySkillSlot2.gameObject:SetActive(false)
            grid.transform.localPosition = Vector3(60, 0,0)
        end
    end

    if grid.transform.childCount==1 then
        for i = 0, 6 do
            NGUITools.AddChild(grid.gameObject,first)
        end
        grid:Reposition()
    end

    if self.m_IsJieBan then
        local partnerInfo = prop.PartnerInfo
        local normalSkills = partnerInfo.PartnerNormalSkills
        local professionalSkills = partnerInfo.PartnerProfessionalSkills
        for i=1,5 do
            local tf = grid.transform:GetChild(i-1)
            self:InitSkillSlot(tf,normalSkills[i])
        end
        for i=1,3 do
            local tf = grid.transform:GetChild(i+5-1)
            self:InitSkillSlot(tf,professionalSkills[i])
        end

    else
        local skillList = prop and prop.SkillListForSave or nil
        for i=1,8 do
            local tf = grid.transform:GetChild(i-1)
            self:InitSkillSlot(tf,skillList and skillList[i] or 0)
        end
    end
end

function CLuaLingShouLookUpWnd:InitSkillSlot(transform,  skillId)
    local icon = transform:Find("HasSkill/Texture"):GetComponent(typeof(CUITexture))
    local levelLabel = transform:Find("HasSkill/LevelLabel"):GetComponent(typeof(UILabel))
    local hasSkillTf = transform:Find("HasSkill")

    local template = Skill_AllSkills.GetData(skillId)
    if template ~= nil then
        hasSkillTf.gameObject:SetActive(true)
        icon:LoadSkillIcon(template.SkillIcon)
        levelLabel.text = "Lv." .. template.Level

        local col = transform:GetComponent(typeof(Collider))
        if col ~= nil then
            col.enabled = true
        end

        local noskill = transform:Find("NoSkill")
        if noskill ~= nil then
            noskill.gameObject:SetActive(false)
        end
    else
        local col = transform:GetComponent(typeof(Collider))
        if col ~= nil then
            col.enabled = false
        end
        hasSkillTf.gameObject:SetActive(false)
        icon.material = nil
        levelLabel.text = ""

        local noskill = transform:Find("NoSkill")
        if noskill ~= nil then
            noskill.gameObject:SetActive(true)
        end
    end

    if skillId>0 then
        UIEventListener.Get(transform.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
            CSkillInfoMgr.ShowSkillInfoWnd(skillId,true,0,0,nil)
        end)
    else
        UIEventListener.Get(transform.gameObject).onClick=nil
    end
end

function CLuaLingShouLookUpWnd:Fill(details)
    local notNull = details ~= nil

    local parent = self.transform:Find("ClipPanel/Anchor/Content")

    local zhandouliLabel = parent:Find("ZhandouliLabel"):GetComponent(typeof(UILabel))
    local nameLabel = parent:Find("NameLabel"):GetComponent(typeof(UILabel))
    local titleLabel = parent:Find("DescLabel"):GetComponent(typeof(UILabel))
    local levelLabel = parent:Find("LevelLabel"):GetComponent(typeof(UILabel))

    local bringLevelLabel = parent:Find("Panel/Group2/BringLevelLabel"):GetComponent(typeof(UILabel))
    local corLabel = parent:Find("Panel/Group1/CorLabel"):GetComponent(typeof(UILabel))
    -- local lifeLabel = self.transform:Find(""):GetComponent(typeof(UILabel))
    local StaLabel = parent:Find("Panel/Group1/StaLabel"):GetComponent(typeof(UILabel))
    local StrLabel = parent:Find("Panel/Group1/StrLabel"):GetComponent(typeof(UILabel))
    local IntLabel = parent:Find("Panel/Group1/IntLabel"):GetComponent(typeof(UILabel))
    local agiLabel = parent:Find("Panel/Group1/AgiLabel"):GetComponent(typeof(UILabel))
    local pHitLabel = parent:Find("Panel/Group2/PHitLabel"):GetComponent(typeof(UILabel))
    local mHitLabel = parent:Find("Panel/Group2/MHitLabel"):GetComponent(typeof(UILabel))
    -- local fatalLabel = self.transform:Find(""):GetComponent(typeof(UILabel))
    local attackLabel = parent:Find("Panel/Group2/AttackLabel"):GetComponent(typeof(UILabel))
    local hpLabel = parent:Find("Panel/Group2/HpLabel"):GetComponent(typeof(UILabel))
    local pDefLabel = parent:Find("Panel/Group2/pDefLabel"):GetComponent(typeof(UILabel))
    local mDefLabel = parent:Find("Panel/Group2/mDefLabel"):GetComponent(typeof(UILabel))
    local pMissLabel = parent:Find("Panel/Group2/pMissLabel"):GetComponent(typeof(UILabel))
    local mMissLabel = parent:Find("Panel/Group2/mMissLabel"):GetComponent(typeof(UILabel))
    local corZiZhiLabel = parent:Find("Panel/Group1/CorZiZhiLabel"):GetComponent(typeof(UILabel))
    local staZiZhiLabel = parent:Find("Panel/Group1/StaZiZhiLabel"):GetComponent(typeof(UILabel))
    local strZiZhiLabel = parent:Find("Panel/Group1/StrZiZhiLabel"):GetComponent(typeof(UILabel))
    local intZiZhiLabel = parent:Find("Panel/Group1/IntZiZhiLabel"):GetComponent(typeof(UILabel))
    local agiZiZhiLabel = parent:Find("Panel/Group1/AgiZiZhiLabel"):GetComponent(typeof(UILabel))
    local chengZhangLabel = parent:Find("Panel/Group1/ChengZhangLabel"):GetComponent(typeof(UILabel))
    local xiuWeiLabel = parent:Find("Panel/Group2/XiuWeiLabel"):GetComponent(typeof(UILabel))
    local wuXingLabel = parent:Find("Panel/Group2/WuXingLabel"):GetComponent(typeof(UILabel))
    local pFatalLabel = parent:Find("Panel/Group2/pFatal"):GetComponent(typeof(UILabel))
    local pFatalDamageLabel = parent:Find("Panel/Group2/pFatalDamage"):GetComponent(typeof(UILabel))
    local mFatalLabel = parent:Find("Panel/Group2/mFatal"):GetComponent(typeof(UILabel))
    local mFatalDamageLabel = parent:Find("Panel/Group2/mFatalDamage"):GetComponent(typeof(UILabel))

    local qinmiLabel = parent:Find("Panel/Group2/QinmiLabel"):GetComponent(typeof(UILabel))
    qinmiLabel.text = notNull and tostring(details.data.Props.PartnerInfo.Affinity / 10) or "0"

    local zizhiStar = parent:Find("Panel/Group1/StarLevel")
    local function initZiZhi(transform,starLevel)
        local sprites={}
        for i=1,5 do
            local sprite = transform:GetChild(i-1):GetComponent(typeof(UISprite))
            table.insert( sprites,sprite )
            sprite.spriteName = "common_star_70%"
        end
        local c1 = math.floor(starLevel/2)
        local c2 = starLevel%2
        for i=1,c1 do
            sprites[i].spriteName = "common_star_3_1"
        end
        if c2==1 then
            sprites[c1+1].spriteName = "common_star_3_2"
        end
    end
    initZiZhi(zizhiStar,notNull and details.data.Props.ZizhiLevel or 0)
    
    local pinzhi = parent:Find("PinZhiSprite"):GetComponent(typeof(UISprite))
    pinzhi.spriteName = notNull and CLuaLingShouMgr.GetLingShouQualitySprite(CLuaLingShouMgr.GetLingShouQuality(details.data.Props.Quality)) or nil

    local shenShouSprite = parent:Find("ShenShou"):GetComponent(typeof(UISprite))
    local genderSprite = parent:Find("GenderSprite"):GetComponent(typeof(UISprite))
    local babyBtn = parent:Find("BabyBtn").gameObject


    --"1:灵兽等级2:成长3:资质系数4:悟性5:修为6:技能数量7:技能满级比之和（各个技能/满级等级之和）"

    local zhandouliNameLabel = zhandouliLabel.transform:Find("Label"):GetComponent(typeof(UILabel))
    if self.m_IsJieBan then
        zhandouliNameLabel.text = LocalString.GetString("结伴战斗力")
        zhandouliLabel.text = notNull and tostring(CLuaLingShouMgr.GetLingShouPartnerZhandouli(details.data,details.fightProperty,CLuaLingShouOtherMgr.jiebanQiChangId)) or ""
    else
        zhandouliNameLabel.text = LocalString.GetString("出战战斗力")
        zhandouliLabel.text = notNull and tostring(CLuaLingShouMgr.GetLingShouZhandouli(details.data)) or ""
    end


    nameLabel.text = notNull and details.data.Name or nil

    titleLabel.text = notNull and details.FullDesc or nil

    local improve = ""
    if notNull then
        local add = NumberTruncate((details.fightProperty:GetParam(EPlayerFightProp.WuxingImprove) * 100), 2)
        if add > 0 then
            improve = System.String.Format("[ffed5f](+{0:f1}%)[-]", add)
        end
    end

    levelLabel.text = notNull and "Lv." .. tostring(details.data.Level) or nil

    bringLevelLabel.text = notNull and tostring(CLingShouBaseMgr.GetLingShouBringLevel(details.data.TemplateId)) or ""

    corLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.Cor)))) or ""

    shenShouSprite.enabled = false

    if notNull then
        local data = LingShou_LingShou.GetData(details.data.TemplateId)
        if data ~= nil then
            if CLuaLingShouMgr.IsShenShou(details.data) then
                if shenShouSprite ~= nil then
                    shenShouSprite.enabled = true
                end
            end
        end
    end


    StaLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.Sta)))) or ""

    StrLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.Str)))) or ""

    IntLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.Int)))) or ""

    agiLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.Agi)))) or ""

    pHitLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.pHit)))) or ""
    --物理命中

    mHitLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.mHit)))) or ""
    --法术命中


    -- fatalLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.pHit)))) or ""
    --物理命中

    -- self:InitAttack(details)
    if attackLabel ~= nil then
        if notNull then
            local default = details.data.Props.Type--CommonDefs.ConvertIntToEnum(typeof(EnumLingShouType), details.data.Props.Type)
            if default == LuaEnumLingShouType.PAttack or default == LuaEnumLingShouType.PCorDefend or default == LuaEnumLingShouType.PAType then
                attackLabel.text = SafeStringFormat3("%d-%d", math.floor(details.fightProperty:GetParam(EPlayerFightProp.pAttMin)), math.floor(details.fightProperty:GetParam(EPlayerFightProp.pAttMax)))
            elseif default == LuaEnumLingShouType.MAttack or default == LuaEnumLingShouType.MCorDefend or default == LuaEnumLingShouType.MAType then
                attackLabel.text = SafeStringFormat3("%d-%d", math.floor(details.fightProperty:GetParam(EPlayerFightProp.mAttMin)), math.floor(details.fightProperty:GetParam(EPlayerFightProp.mAttMax)))
            end
        else
            attackLabel.text = ""
        end
    end


    hpLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.HpFull)))) or ""

    pDefLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.pDef)))) or ""
    mDefLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.mDef)))) or ""
    pMissLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.pMiss)))) or ""
    mMissLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.mMiss)))) or ""

    corZiZhiLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.CorZizhi)))) .. improve or nil
    staZiZhiLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.StaZizhi)))) .. improve or nil
    strZiZhiLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.StrZizhi)))) .. improve or nil
    intZiZhiLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.IntZizhi)))) .. improve or nil
    agiZiZhiLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.AgiZizhi)))) .. improve or nil

    pFatalLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.pFatal)))) or ""
    pFatalDamageLabel.text = notNull and tostring(((NumberTruncate(details.fightProperty:GetParam(EPlayerFightProp.pFatalDamage), 3) * 100))) .. "%" or nil
    mFatalLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.mFatal)))) or ""
    mFatalDamageLabel.text = notNull and tostring(((NumberTruncate(details.fightProperty:GetParam(EPlayerFightProp.mFatalDamage), 3) * 100))) .. "%" or nil


    chengZhangLabel.text = notNull and System.String.Format("{0}({1})", CUICommonDef.ConvertDouble2String(details.data.Props.Chengzhang, 3), CLingShouBaseMgr.GetGrowRemark(details.data.Props.Chengzhang)) or nil
    xiuWeiLabel.text = notNull and  tostring(math.floor(details.data.Props.Xiuwei)) or nil
    wuXingLabel.text = notNull and details.strWuxing or nil

    local show = false
    if details ~= nil then
        local marryInfo = details.data.Props.MarryInfo
        local gender = details.data.Props.MarryInfo.Gender
        if marryInfo.AdultStartTime > 0 then
            if gender == 0 then
                show = false
            else
                if gender == 1 then
                    genderSprite.spriteName = "lingshoujiehun_yang"--CLingShouDetailsDisplay.YangSpriteName
                elseif gender == 2 then
                    genderSprite.spriteName = "lingshoujiehun_yin"--CLingShouDetailsDisplay.YingSpriteName
                elseif gender == 3 then
                    genderSprite.spriteName = "lingshoujiehun_yinyang"--CLingShouDetailsDisplay.YinYangSpriteName
                end
                show = true
            end
        else
            show = false
        end
    end
    if show then
        genderSprite.gameObject:SetActive(true)
        UIEventListener.Get(genderSprite.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            CLuaLingShouMgr.m_TempLingShouInfo = details
            CLuaLingShouMgr.m_IsMyLingShou = self.IsMyLingShou
            CUIManager.ShowUI(CUIResources.LingShouMarryInfoWnd)
        end)
    else
        genderSprite.gameObject:SetActive(false)
        UIEventListener.Get(genderSprite.gameObject).onClick = nil
    end

    UIEventListener.Get(babyBtn).onClick = nil
    local headIcon = CommonDefs.GetComponent_Component_Type(babyBtn.transform:Find("Icon"), typeof(CUITexture))
    headIcon:Clear()
    if details ~= nil then
        local babyInfo = details.data.Props.Baby
        if babyInfo.BornTime > 0 then
            babyBtn:SetActive(true)
            local babyData = LingShouBaby_BabyTemplate.GetData(babyInfo.TemplateId)
            if babyData ~= nil then
                headIcon:LoadNPCPortrait(babyData.Portrait, false)
            else
                headIcon:Clear()
            end
            UIEventListener.Get(babyBtn).onClick = DelegateFactory.VoidDelegate(function(go) 
                CLuaLingShouMgr.m_TempLingShouInfo = details
                CUIManager.ShowUI(CUIResources.LingShouOtherBabyWnd)
            end)
        else
            babyBtn:SetActive(false)
        end
    else
        babyBtn:SetActive(false)
    end
end
