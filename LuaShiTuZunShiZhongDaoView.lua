local Extensions = import "Extensions"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local UISlider = import "UISlider"
local CButton = import "L10.UI.CButton"

LuaShiTuZunShiZhongDaoView = class()

RegistChildComponent(LuaShiTuZunShiZhongDaoView, "m_Grid","Grid", UIGrid)
RegistChildComponent(LuaShiTuZunShiZhongDaoView, "m_StudentTaskTemplate","StudentTaskTemplate", GameObject)
RegistChildComponent(LuaShiTuZunShiZhongDaoView, "m_TeacherViewTaskTemplate","TeacherViewTaskTemplate", GameObject)
RegistChildComponent(LuaShiTuZunShiZhongDaoView, "m_ScorllView2","ScorllView2", UIScrollView)

RegistClassMember(LuaShiTuZunShiZhongDaoView,"m_StudentTasks")
RegistClassMember(LuaShiTuZunShiZhongDaoView,"m_TeacherViewTasks")

function LuaShiTuZunShiZhongDaoView:Awake()
    self.m_StudentTaskTemplate:SetActive(false)
    self.m_TeacherViewTaskTemplate:SetActive(false)
end

function LuaShiTuZunShiZhongDaoView:OnEnable()
    g_ScriptEvent:AddListener("ShiTuTrainingHandbook_SyncShiTuDailyTaskInfo", self, "OnSyncShiTuDailyTaskInfo")
    g_ScriptEvent:AddListener("ShiTuTrainingHandbookWnd_ChooseTudiPlayerId", self, "OnChooseTudiPlayerId")
    self:OnChooseTudiPlayerId()
end

function LuaShiTuZunShiZhongDaoView:OnDisable()
    g_ScriptEvent:RemoveListener("ShiTuTrainingHandbook_SyncShiTuDailyTaskInfo", self, "OnSyncShiTuDailyTaskInfo")
    g_ScriptEvent:RemoveListener("ShiTuTrainingHandbookWnd_ChooseTudiPlayerId", self, "OnChooseTudiPlayerId")
end

function LuaShiTuZunShiZhongDaoView:OnChooseTudiPlayerId()
    local playerId = LuaShiTuTrainingHandbookMgr:GetPartnerPlayerId()
    if playerId == 0 then
        return
    end
    Gac2Gas.QueryShiTuCultivateInfo(LuaShiTuTrainingHandbookMgr.wndtype.ZunShiZhongDao, playerId)
end

function LuaShiTuZunShiZhongDaoView:OnSyncShiTuDailyTaskInfo(data)
    for i = 1, #data do
        local t = data[i]
        local dailyTask = ShiTuCultivate_DailyTasks.GetData(data[i].dailyTaskId)
        t.dailyTask = dailyTask
        local studentRewards = {}
        table.insert(studentRewards, {scorekey = EnumPlayScoreKey.QYJF, num = dailyTask.TudiQingyi})
        table.insert(studentRewards, {scorekey = EnumPlayScoreKey.GIFT, num = dailyTask.TudiGiftLimit})
        if dailyTask.TudiItems then
            for j = 0, dailyTask.TudiItems.Length - 1, 2 do
                local itemId, num = dailyTask.TudiItems[j], dailyTask.TudiItems[j + 1]
                table.insert(studentRewards, {item = Item_Item.GetData(itemId), num = num})
            end
        end
        t.studentRewards = studentRewards
        local finishedTask = t.currentValue == t.dailyTask.Num
        if LuaShiTuTrainingHandbookMgr:IsShowForTeacher() then
            t.state = (t.canReward and finishedTask) and 1 or ((t.acceptPlayerId ~= 0) and 2 or 3)
            if (not t.canReward) and finishedTask then
                t.state = 0
            end
            --1:可領取奖励,2:可接取任务,3:正在进行,4:已完成
        else
            t.state = (t.canReward and finishedTask) and 1 or
                    (((not t.isAccept) and (t.acceptPlayerId == 0)) and 2 or (finishedTask and 4 or 3))
            if t.state == 3 then
                if t.acceptPlayerId == LuaShiTuTrainingHandbookMgr:GetTudiPlayerId() then
                    t.state = 0
                end
            end
            --0:自己领取正在进行的,1:可被完成,2:尚未领取,3:正在进行,4:已完成
        end
    end

    if not LuaShiTuTrainingHandbookMgr:IsShowForTeacher() then
        local alert = false
        for i = 1, #data do
            local t = data[i]
            if t.state == 1 then
                alert = true
            end
        end
        g_ScriptEvent:BroadcastInLua("ShiTuTrainingHandbook_Alert", LuaShiTuTrainingHandbookMgr.wndtype.ZunShiZhongDao, alert)
    end

    table.sort(data,function(a, b)
        if a.state == b.state then
            return a.dailyTask.ID < b.dailyTask.ID
        end
        return a.state < b.state
    end)
    if LuaShiTuTrainingHandbookMgr:IsShowForTeacher() then
        self:InitTeacherViewTask(data)
    else
        self:InitStudentTask(data)
    end
end

function LuaShiTuZunShiZhongDaoView:InitStudentTask(data)
    Extensions.RemoveAllChildren(self.m_Grid.transform)
    self.m_StudentTasks = {}
    for i = 1, #data do
        local go = NGUITools.AddChild(self.m_Grid.gameObject, self.m_StudentTaskTemplate)
        go:SetActive(true)
        local task = self:InitStudentTaskComponent(go)
        self:InitStudentTaskData(task, data[i])
        table.insert(self.m_StudentTasks, task)
    end
    self.m_Grid:Reposition()
    if self.m_ScorllView2 then
        self.m_ScorllView2:ResetPosition()
    end
end

function LuaShiTuZunShiZhongDaoView:InitStudentTaskComponent(go)
    local task = {}
    task.conditionLabel = go.transform:Find("Top/ConditionLabel"):GetComponent(typeof(UILabel))
    task.slider = go.transform:Find("Top/Slider"):GetComponent(typeof(UISlider))
    task.slider.value = 0
    task.progressLabel = go.transform:Find("Top/Slider/ProgressLabel"):GetComponent(typeof(UILabel))
    task.grid = go.transform:Find("Bottom/Grid"):GetComponent(typeof(UIGrid))
    Extensions.RemoveAllChildren(task.grid.transform)
    task.studentRewardTemplate = go.transform:Find("Bottom/StudentRewardTemplate").gameObject
    task.studentRewardTemplate:SetActive(false)
    task.infoLabel = go.transform:Find("State/InfoLabel"):GetComponent(typeof(UILabel))
    task.getRewardButton = go.transform:Find("State/GetRewardButton"):GetComponent(typeof(CButton))
    task.helpTeacherButton = go.transform:Find("State/HelpTeacherButton"):GetComponent(typeof(CButton))
    task.giveUpButton = go.transform:Find("State/GiveUpButton"):GetComponent(typeof(CButton))
    return task
end

function LuaShiTuZunShiZhongDaoView:InitStudentTaskData(task, data)
    task.conditionLabel.text = LocalString.GetString("帮师父") .. SafeStringFormat3(data.dailyTask.Description, data.dailyTask.Num)
    task.slider.value = (data.dailyTask.Num >= 0) and (data.currentValue / data.dailyTask.Num) or 0
    task.progressLabel.text = SafeStringFormat3("%d/%d",data.currentValue ,data.dailyTask.Num)
    self:InitStudentRewards(task, data)
    self:InitStudentTaskState(task, data)
end

function LuaShiTuZunShiZhongDaoView:InitStudentRewards(task, data)
    Extensions.RemoveAllChildren(task.grid.transform)
    for i = 1, #data.studentRewards do
        self:InitStudentReward(data.studentRewards[i], task)
    end
    task.grid:Reposition()
end

function LuaShiTuZunShiZhongDaoView:InitStudentReward(data, task)
    if (data.num == 0) and data.scorekey then return end
    local item = data.item
    local go = NGUITools.AddChild(task.grid.gameObject, task.studentRewardTemplate)
    local icon = go:GetComponent(typeof(CUITexture))
    if item then icon:LoadMaterial(item.Icon) end
    if data.scorekey then icon:LoadMaterial(LuaShiTuTrainingHandbookMgr:GetPlayScore(data.scorekey).ScoreIcon) end

    local label = go.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
    label.text = data.num
    go:SetActive(true)
end

function LuaShiTuZunShiZhongDaoView:InitStudentTaskState(task, data)
    task.getRewardButton.gameObject:SetActive(data.state == 1)
    if data.state == 1 then
        UIEventListener.Get(task.getRewardButton.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            self:GetReward(data)
        end)
    end
    task.helpTeacherButton.gameObject:SetActive(data.state == 2)
    if data.state == 2 then
        UIEventListener.Get(task.helpTeacherButton.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            self:HandleTask(data, true)
        end)
    end
    if data.state == 4 then
        task.infoLabel.enabled = true
        task.infoLabel.text = LocalString.GetString("已完成")
    end
    task.giveUpButton.gameObject:SetActive(data.state == 0)
    task.infoLabel.enabled = (data.state == 4) or (data.state == 3)
    if data.state == 0 then
        UIEventListener.Get(task.giveUpButton.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            self:HandleTask(data, false)
        end)
    elseif data.state == 3 then
        task.infoLabel.text =  CUICommonDef.TranslateToNGUIText(SafeStringFormat3("[ff88ff][%s]#W%s",
                data.acceptPlayerName, LocalString.GetString("进行中")))
    end

end

function LuaShiTuZunShiZhongDaoView:InitTeacherViewTask(data)
    Extensions.RemoveAllChildren(self.m_Grid.transform)
    self.m_TeacherViewTasks = {}
    for i = 1, #data do
        if data[i].state ~= 0 then
            local go = NGUITools.AddChild(self.m_Grid.gameObject, self.m_TeacherViewTaskTemplate)
            go:SetActive(true)
            local task = self:InitTeacherViewComponent(go)
            self:InitTeacherViewTaskData(task, data[i])
            table.insert(self.m_TeacherViewTasks, task)
        end
    end
    self.m_Grid:Reposition()
    if self.m_ScorllView2 then
        self.m_ScorllView2:ResetPosition()
    end
end

function LuaShiTuZunShiZhongDaoView:InitTeacherViewComponent(go)
    local task = {}
    task.desLabel = go.transform:Find("Top/DesLabel"):GetComponent(typeof(UILabel))
    task.slider = go.transform:Find("Top/Slider"):GetComponent(typeof(UISlider))
    task.slider.value = 0
    task.progressLabel = go.transform:Find("Top/Slider/ProgressLabel"):GetComponent(typeof(UILabel))
    task.infoLabel = go.transform:Find("Bottom/InfoLabel"):GetComponent(typeof(UILabel))
    task.unfinishedLabel = go.transform:Find("State/UnFinishedLabel"):GetComponent(typeof(UILabel))
    task.ongoingLabel = go.transform:Find("State/OngoingLabel"):GetComponent(typeof(UILabel))
    task.getRewardButton = go.transform:Find("State/GetRewardButton"):GetComponent(typeof(CButton))
    return task
end

function LuaShiTuZunShiZhongDaoView:InitTeacherViewTaskData(task, data)
    task.desLabel.text = LocalString.GetString("徒弟帮你") .. SafeStringFormat3(data.dailyTask.Description, data.dailyTask.Num)
    local progressValue = (data.dailyTask.Num >= 0) and (data.currentValue / data.dailyTask.Num) or 0
    task.slider.value = progressValue
    task.progressLabel.text = SafeStringFormat3("%d/%d",data.currentValue ,data.dailyTask.Num)

    self:InitTeacherViewTaskState(task, data)
end

function LuaShiTuZunShiZhongDaoView:InitTeacherViewTaskState(task, data)
    task.getRewardButton.gameObject:SetActive(data.state == 1)
    task.ongoingLabel.enabled = data.state == 2
    task.unfinishedLabel.enabled = data.state == 3
    if data.state == 1 then
        UIEventListener.Get(task.getRewardButton.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            self:GetReward(data)
        end)
        task.infoLabel.text =  CUICommonDef.TranslateToNGUIText(SafeStringFormat3("[ff88ff][%s]#W%s",
                data.acceptPlayerName, LocalString.GetString("已帮你完成")))
    elseif data.state == 2 then
        task.infoLabel.text =  CUICommonDef.TranslateToNGUIText(SafeStringFormat3("[ff88ff][%s]#W%s",
                data.acceptPlayerName, LocalString.GetString("正在帮你完成")))
    else
        task.infoLabel.text =  CUICommonDef.TranslateToNGUIText(LocalString.GetString("[837F7F]暂无徒弟帮你完成任务"))
    end
end

function LuaShiTuZunShiZhongDaoView:GetReward(data)
    Gac2Gas.ShiTuDailyTaskGetReward(LuaShiTuTrainingHandbookMgr:GetPartnerPlayerId(), data.dailyTaskId)
end

function LuaShiTuZunShiZhongDaoView:HandleTask(data, isAccept)
    Gac2Gas.ShiTuDailyTaskTuDiHandleTask(LuaShiTuTrainingHandbookMgr:GetShifuPlayerId(), data.dailyTaskId, isAccept)
end