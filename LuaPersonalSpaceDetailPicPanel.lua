local UIScrollView = import "UIScrollView"
local Movement = import "UIScrollView+Movement"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Object1 = import "UnityEngine.Object"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"

LuaPersonalSpaceDetailPicPanel = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPersonalSpaceDetailPicPanel, "ScrollView", "ScrollView", UIScrollView)
RegistChildComponent(LuaPersonalSpaceDetailPicPanel, "pic", "pic", GameObject)
RegistChildComponent(LuaPersonalSpaceDetailPicPanel, "LeftButton", "LeftButton", GameObject)
RegistChildComponent(LuaPersonalSpaceDetailPicPanel, "RightButton", "RightButton", GameObject)
RegistChildComponent(LuaPersonalSpaceDetailPicPanel, "ReduceBtn", "ReduceBtn", GameObject)
RegistChildComponent(LuaPersonalSpaceDetailPicPanel, "EnlargeBtn", "EnlargeBtn", GameObject)
RegistChildComponent(LuaPersonalSpaceDetailPicPanel, "darkbg", "darkbg", GameObject)
RegistChildComponent(LuaPersonalSpaceDetailPicPanel, "smallpic", "smallpic", GameObject)

--@endregion RegistChildComponent end

function LuaPersonalSpaceDetailPicPanel:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaPersonalSpaceDetailPicPanel:InitContent(infoArray, index, texture, wnd)

    if wnd ~= nil then
        self.m_Wnd = wnd
    end

    self.m_Scale = 0

    self.ScrollView:ResetPosition()
    self.ScrollView.enabled = false

    local picNode = self.transform:Find("ScrollView/pic").gameObject

    -- 判断是否超出
    if index >= infoArray.Length or index < 0 then
        local mainTexture = CommonDefs.GetComponent_GameObject_Type(picNode, typeof(UITexture)).mainTexture
        CommonDefs.GetComponent_GameObject_Type(picNode, typeof(UITexture)).mainTexture = nil
        Object1.Destroy(mainTexture)
        self.gameObject:SetActive(false)
        return
    end

    CUICommonDef.SetActive(self.ReduceBtn, true, true)
    CUICommonDef.SetActive(self.EnlargeBtn, true, true)
    UIEventListener.Get(self.ReduceBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        if self.m_Scale > 1 then
            self.m_Scale = self.m_Scale - 0.2
        else
            self.m_Scale = 1 
        end
        self:SetSacle(self.m_Scale)
    end)

    UIEventListener.Get(self.EnlargeBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        if self.m_Scale < 2 then
            self.m_Scale = self.m_Scale + 0.2
        else
            self.m_Scale = 2
        end
        self:SetSacle(self.m_Scale)
    end)

    -- 左右切换按钮
    local leftButton = self.gameObject.transform:Find("P2/LeftButton").gameObject
    local rightButton = self.gameObject.transform:Find("P2/RightButton").gameObject
    if index > 0 then
        leftButton:SetActive(true)
        UIEventListener.Get(leftButton).onClick = DelegateFactory.VoidDelegate(function (p)
            self:InitContent(infoArray, index - 1, nil)
        end)
    else
        UIEventListener.Get(leftButton).onClick = nil
        leftButton:SetActive(false)
    end
    if index < infoArray.Length - 1 then
        rightButton:SetActive(true)
        UIEventListener.Get(rightButton).onClick = DelegateFactory.VoidDelegate(function (p)
            self:InitContent(infoArray, index + 1, nil)
        end)
    else
        UIEventListener.Get(rightButton).onClick = nil
        rightButton:SetActive(false)
    end

    self.gameObject:SetActive(true)
    local backBtn = self.gameObject.transform:Find("darkbg").gameObject

    local darkWidget = self.gameObject.transform:Find("darkbg"):GetComponent(typeof(UITexture))
    darkWidget.fullScreenAdaptation = true
    local smallPicNode = self.gameObject.transform:Find("ScrollView/smallpic").gameObject
    CommonDefs.GetComponent_GameObject_Type(smallPicNode, typeof(UITexture)).mainTexture = texture
    picNode:SetActive(false)
    smallPicNode:SetActive(true)

    -- 点击暗处返回
    UIEventListener.Get(backBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        local mainTexture = CommonDefs.GetComponent_GameObject_Type(picNode, typeof(UITexture)).mainTexture
        CommonDefs.GetComponent_GameObject_Type(picNode, typeof(UITexture)).mainTexture = nil
        Object1.Destroy(mainTexture)
        self.gameObject:SetActive(false)
    end)

    local info = infoArray[index]
    CPersonalSpaceMgr.DownLoadPic(info.pic, CommonDefs.GetComponent_GameObject_Type(picNode, typeof(UITexture)), CPersonalSpaceMgr.MaxPicHeight, CPersonalSpaceMgr.MaxPicWidth, DelegateFactory.Action(function ()
        if not self.m_Wnd.systemPortrait then
            return
        end
        picNode:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(picNode, typeof(UITexture)):ResizeCollider()
        smallPicNode:SetActive(false)

        self.m_Scale = 1
        self:SetSacle(self.m_Scale)
    end), false, true)
end

function LuaPersonalSpaceDetailPicPanel:SetSacle(scale)
    if scale < 1 then
        scale = 1
    elseif scale > 2 then
        scale = 2
    end

    local uitexture = self.pic:GetComponent(typeof(UITexture))
    local texture = uitexture.mainTexture

    local w = texture.width / 1920
    local h = texture.height / 1080

    local wh = texture.width/texture.height

    if w > 0.9 or h > 0.9 then
        -- 处理大图
        if w > h then
            uitexture.width = 1920 *0.9 *scale
            uitexture.height = 1920 *0.9 *scale/wh
        else
            uitexture.height = 1080 *0.9 *scale
            uitexture.width = 1080 *0.9 *scale *wh
        end
    elseif w < 0.5 or h < 0.5 then
        -- 处理小图
        if w > h then
            uitexture.width = 1920 *0.5 *scale
            uitexture.height = 1920 *0.5 *scale/wh
        else
            uitexture.height = 1080 *0.5 *scale
            uitexture.width = 1080 *0.5 *scale*wh
        end
    else
        uitexture.height = texture.height *scale
        uitexture.width = texture.width *scale
    end

    self.ScrollView:ResetPosition()
    if uitexture.height > 1080 and uitexture.width > 1920 then
        self.ScrollView.enabled = true
        self.ScrollView.movement = Movement.Unrestricted
    elseif uitexture.height > 1080 then
        self.ScrollView.enabled = true
        self.ScrollView.movement = Movement.Vertical
    elseif uitexture.width > 1920 then
        self.ScrollView.enabled = true
        self.ScrollView.movement = Movement.Horizontal
    else
        self.ScrollView.enabled = false
    end

    local bg1 = self.pic.transform:Find("bg1"):GetComponent(typeof(UISprite))
    local bg2 = self.pic.transform:Find("bg2"):GetComponent(typeof(UISprite))

    -- bg1.width = uitexture.width + 40
    -- bg1.height = uitexture.height + 40

    -- bg2.width = uitexture.width + 60
    -- bg2.height = uitexture.height + 60

    CUICommonDef.SetActive(self.ReduceBtn, true, false)
    CUICommonDef.SetActive(self.EnlargeBtn, true, false)
    if scale >= 2 then
        CUICommonDef.SetActive(self.EnlargeBtn, false, false)
    elseif scale <=1 then
        CUICommonDef.SetActive(self.ReduceBtn, false, false)
    end
end

--@region UIEvent

--@endregion UIEvent

