local CServerTimeMgr   = import "L10.Game.CServerTimeMgr"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local Regex = import "System.Text.RegularExpressions.Regex"
local RegexOptions = import "System.Text.RegularExpressions.RegexOptions"
local CScene = import "L10.Game.CScene"

LuaHanJia2023Mgr = {}
LuaHanJia2023Mgr.easyColor = NGUIText.ParseColor24("39458F",0)
LuaHanJia2023Mgr.normalColor = NGUIText.ParseColor24("9C8120",0)
LuaHanJia2023Mgr.hardColor = NGUIText.ParseColor24("68398F",0)
LuaHanJia2023Mgr.multipleColor = NGUIText.ParseColor24("185023",0)

function LuaHanJia2023Mgr:OnPlayerLogin()
    --reset
    --LuaHanJia2023Mgr.snowManPVEData = {}
    --LuaHanJia2023Mgr.snowAdventureData = {}
    --LuaHanJia2023Mgr.multipleTeamInfo = {}
    LuaHanJia2023Mgr.isXKLOpen = false
    LuaHanJia2023Mgr.XKLData = nil
end

LuaHanJia2023Mgr.IsInXuePoLiXianPlay = function()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        local ret = false
        HanJia2023_XuePoLiXianLevel.Foreach(function(_, v)
            if gamePlayId == v.GameplayId then
                ret = true
            end
        end)
        return ret
    end
    return false
end


function LuaHanJia2023Mgr:ShowBingTianShiLianResult(bResult, curSelect, curDifficulty)
    LuaSnowManPVEResultWnd.bResult = bResult
    LuaSnowManPVEResultWnd.curSelect = curSelect
    LuaSnowManPVEResultWnd.curDifficulty = curDifficulty
    if LuaHanJia2023Mgr.m_showSnowManPVEResultDelayTick then
        LuaHanJia2023Mgr.m_showSnowManPVEResultDelayTick:Invoke()
        LuaHanJia2023Mgr.m_showSnowManPVEResultDelayTick = nil
    end
    LuaHanJia2023Mgr.m_showSnowManPVEResultDelayTick = CTickMgr.Register(DelegateFactory.Action(function ()
        CUIManager.ShowUI(CLuaUIResources.SnowManPVEResultWnd)
    end), 1500, ETickType.Once)
end

function LuaHanJia2023Mgr:SyncBingTianShiLianData(ret_U)
    local unpackSnowManPVEData = g_MessagePack.unpack(ret_U)
    LuaHanJia2023Mgr.snowManPVEData = {}
    LuaHanJia2023Mgr.snowManPVEData.lastSelect = unpackSnowManPVEData.lastSelect or 1
    LuaHanJia2023Mgr.snowManPVEData.monsterLv = unpackSnowManPVEData.curLv or {}
    LuaHanJia2023Mgr.snowManPVEData.curUnlockDifficulty = unpackSnowManPVEData.curUnlockDifficulty or 0
    g_ScriptEvent:BroadcastInLua("HanJia2023_SyncSnowManPVEData")
end

function LuaHanJia2023Mgr:GetSeason()
    local time = CServerTimeMgr.Inst.timeStamp
    local seasonId = nil
    local seasonEndTime = nil
    HanJia2023_BingTianShiLianSeason.Foreach(function (id, data)
        local startTime = CServerTimeMgr.Inst:GetTimeStampByStr(data.StartTime)
        local endTime =  CServerTimeMgr.Inst:GetTimeStampByStr(data.EndTime)
        if time > startTime and time <= endTime then
            seasonId = id
            seasonEndTime = endTime
        end
    end)
    return seasonId, seasonEndTime
end

function LuaHanJia2023Mgr:SyncXuePoLiXianData(ret_U)
    local unpackData = g_MessagePack.unpack(ret_U)
    LuaHanJia2023Mgr.snowAdventureData = {}
    --@type number 积分, 用于雪人解锁
    LuaHanJia2023Mgr.snowAdventureData.score = unpackData.score
    --@type number 剩余挑战次数
    LuaHanJia2023Mgr.snowAdventureData.playNum = unpackData.playNum
    --@type number 成功通关次数 
    LuaHanJia2023Mgr.snowAdventureData.successNum = unpackData.successNum
    --@type number 上次选择的雪人
    LuaHanJia2023Mgr.snowAdventureData.lastSnowmanType = unpackData.lastSnowmanType
    --@type number 上次选择的关卡id
    LuaHanJia2023Mgr.snowAdventureData.lastLevelId = unpackData.lastLevelId
    --{lv: number, bool: number} 单人信息，记录每个难度有没有通关
    LuaHanJia2023Mgr.snowAdventureData.singleInfo = {}
    --根据通关数据判断出来的 下一个可挑战的关卡, 需要结合关卡开放信息综合判断
    LuaHanJia2023Mgr.snowAdventureData.nextSingleChallengeStageId = 101
    local curPassSingleStageId = 0
    local stageIdList = {}
    HanJia2023_XuePoLiXianLevel.Foreach(function(k, _) 
        table.insert(stageIdList, k)
    end)
    table.sort(stageIdList, function (a, b)
        return b < a
    end)
    for i = 1, #stageIdList do
        LuaHanJia2023Mgr.snowAdventureData.singleInfo[stageIdList[i]] = false
    end
    for k, tbl in pairs(unpackData.singleInfo) do
        for i = 1, tbl.lv do
            local stageId = k * 100 + i
            LuaHanJia2023Mgr.snowAdventureData.singleInfo[stageId] = true
            if stageId < 400 then
                curPassSingleStageId = math.max(curPassSingleStageId, stageId)
            end
        end
    end

    if LuaHanJia2023Mgr.snowAdventureData.singleInfo[curPassSingleStageId+1] == nil then
        --跳阶段了
        local curPhase = (curPassSingleStageId -  curPassSingleStageId % 100) / 100
        if curPhase == 3 then
            LuaHanJia2023Mgr.snowAdventureData.nextSingleChallengeStageId = curPassSingleStageId
        else    
            LuaHanJia2023Mgr.snowAdventureData.nextSingleChallengeStageId = (curPhase+1) * 100 + 1
        end
    else    
        LuaHanJia2023Mgr.snowAdventureData.nextSingleChallengeStageId = curPassSingleStageId + 1
    end
    
    --@type {levelId: rewardNum}  领奖信息，记录每个关卡的领奖次数
    LuaHanJia2023Mgr.snowAdventureData.rewardInfo = unpackData.rewardInfo
    --@type {snowmanType: lv} -- 雪人养成数据（每个雪人的等级）
    LuaHanJia2023Mgr.snowAdventureData.snowmanInfo = unpackData.snowmanInfo

    --@type {difficulty, unlockMaxStageId}, 对应每个难度下最大的已解锁的stageId
    LuaHanJia2023Mgr.snowAdventureData.unlockStageInfo = {[1] = 0, [2] = 0, [3] = 0, [4] = 0}
    --根据填表时间 , 自己组装正常的时间戳?
    local startTime = HanJia2023_XuePoLiXianSetting.GetData().StartTime
    startTime = startTime .. " 00:00:00"
    local startTimeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(startTime)
    local curTimeStamp = CServerTimeMgr.Inst.timeStamp
    local SECONDSPERDAY = 24*60*60
    HanJia2023_XuePoLiXianLevel.Foreach(function(k ,v)
        local curDifficulty = math.floor(k / 100)
        if curDifficulty ~= 4 then
            local curStageId = LuaHanJia2023Mgr.snowAdventureData.unlockStageInfo[curDifficulty]
            if curTimeStamp-startTimeStamp >= (v.UnlockNeedDay-1)*SECONDSPERDAY and k > curStageId then
                LuaHanJia2023Mgr.snowAdventureData.unlockStageInfo[curDifficulty] = k
            end
        end
    end)

    self:CalcMultipleUnlockInfo()
    g_ScriptEvent:BroadcastInLua("HanJia2023_SyncXuePoLiXianData")
end

function LuaHanJia2023Mgr:CalcMultipleUnlockInfo()
    local SECONDSPERDAY = 24*60*60
    local levelIdListTeam = {}
    local m_day2MaxTeamLevel = {}
    LuaHanJia2023Mgr.snowAdventureData.multipleUnlockDays = {}
    HanJia2023_XuePoLiXianLevel.Foreach(function(k, _)
        local curDifficulty = math.floor(k / 100)
        LuaHanJia2023Mgr.snowAdventureData.multipleUnlockDays[k] = -1
        if curDifficulty == 4 then
            table.insert(levelIdListTeam, k)
        end
    end)
    
    local teamModeOpenWeekDay = {} -- 多人模式周几开启挑战
    for weekDay in string.gmatch(HanJia2023_XuePoLiXianSetting.GetData().TeamModeOpenWeekDay, "(%d+)") do
        teamModeOpenWeekDay[tonumber(weekDay)] = true
    end
    local teamModeOpenDays = {} -- 多人模式单独开启挑战的日期
    for year, month, day in string.gmatch(HanJia2023_XuePoLiXianSetting.GetData().TeamModeOpenDays, "(%d+)-(%d+)-(%d+)") do
        local openTs = os.time({year=tonumber(year), month=tonumber(month), day=tonumber(day), hour=0, min=0, sec=0, isdst=false})
        table.insert(teamModeOpenDays, openTs)
    end
    local teamModeUnlockWeekDay = {}
    for weekDay in string.gmatch(HanJia2023_XuePoLiXianSetting.GetData().TeamModeUnlockWeekDay, "(%d+)") do
        teamModeUnlockWeekDay[tonumber(weekDay)] = true
    end
    local teamModeUnlockDays = {}
    for year, month, day in string.gmatch(HanJia2023_XuePoLiXianSetting.GetData().TeamModeUnlockDays, "(%d+)-(%d+)-(%d+)") do
        local unlockTs = os.time({year=tonumber(year), month=tonumber(month), day=tonumber(day), hour=0, min=0, sec=0, isdst=false})
        table.insert(teamModeUnlockDays, unlockTs)
    end
    local listIdx = 0
    table.sort(levelIdListTeam)
    local year, month, day = string.match(HanJia2023_XuePoLiXianSetting.GetData().StartTime, "(%d+)-(%d+)-(%d+)")
    local m_StartTime = os.time({year=tonumber(year), month=tonumber(month), day=tonumber(day), hour=0, min=0, sec=0, isdst=false})
    for i = 1, 60 do
        local checkTs = m_StartTime + (i - 1) * SECONDSPERDAY
        local checkDate = os.date("*t", checkTs)
        local isOk = false
        for k, dayTs in pairs(teamModeUnlockDays) do
            if checkTs == dayTs then
                isOk = true
                break
            end
        end
        if teamModeUnlockWeekDay[checkDate.wday - 1] then
            isOk = true
        end
        if isOk then
            listIdx = math.min(listIdx + 1, #levelIdListTeam)
        end

        local isOpen = false
        for _, dayTs in pairs(teamModeOpenDays) do
            if checkTs == dayTs then
                isOpen = true
                break
            end
        end
        if teamModeOpenWeekDay[checkDate.wday - 1] then
            isOpen = true
        end

        if listIdx > 0 and isOk then
            m_day2MaxTeamLevel[i] = levelIdListTeam[listIdx]
        elseif listIdx > 0 and isOpen then
            m_day2MaxTeamLevel[i] = levelIdListTeam[listIdx]
        else
            m_day2MaxTeamLevel[i] = 0
        end
    end
    
    local startTime = HanJia2023_XuePoLiXianSetting.GetData().StartTime
    startTime = startTime .. " 00:00:00"
    local curTimeStamp = CServerTimeMgr.Inst.timeStamp
    local startTimeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(startTime)
    local SECONDSPERDAY = 24*60*60
    local dayDiff = math.floor((curTimeStamp-startTimeStamp)/SECONDSPERDAY) + 1
    LuaHanJia2023Mgr.snowAdventureData.isMultipleOpen = not (m_day2MaxTeamLevel[dayDiff] == 0)
    for i = dayDiff, 1, -1 do
        if m_day2MaxTeamLevel[i] ~= 0 then
            LuaHanJia2023Mgr.snowAdventureData.unlockStageInfo[4] = m_day2MaxTeamLevel[i]
            break
        end
    end
    for i = 1, #m_day2MaxTeamLevel do
        local maxOpenStage = m_day2MaxTeamLevel[i]
        if maxOpenStage ~= 0 then
            --i day open m_day2MaxTeamLevel[i]
            if LuaHanJia2023Mgr.snowAdventureData.multipleUnlockDays[maxOpenStage] == -1 then
                LuaHanJia2023Mgr.snowAdventureData.multipleUnlockDays[maxOpenStage] = i
            end
        end
    end
end 

function LuaHanJia2023Mgr:SyncXuePoLiXianPrepareInfo(ret_U)
    local unpackData = g_MessagePack.unpack(ret_U)
    LuaHanJia2023Mgr.multipleTeamInfo = {}
    --@type number startTs, 开始倒计时的时间戳
    LuaHanJia2023Mgr.multipleTeamInfo.startTs = unpackData.startTs
    --@type number 当前组队模式挑战的关卡
    LuaHanJia2023Mgr.multipleTeamInfo.curLevelId = unpackData.curLevelId
    --@type number 成员信息 {{isOk, lastSnowmanType, lastSnowmanLv}}
    LuaHanJia2023Mgr.multipleTeamInfo.memberInfo = unpackData.memberInfo
    if not CUIManager.IsLoaded(CLuaUIResources.SnowAdventureMultipleTeamWnd) then
        CUIManager.ShowUI(CLuaUIResources.SnowAdventureMultipleTeamWnd)
    end
    g_ScriptEvent:BroadcastInLua("HanJia2023_SyncMultipleTeamInfo")
end 

function LuaHanJia2023Mgr:XuePoLiXianStopPrepare()
    g_ScriptEvent:BroadcastInLua("HanJia2023_XuePoLiXianStopPrepare")
end 

function LuaHanJia2023Mgr:OnMainPlayerCreated()
    g_ScriptEvent:BroadcastInLua("HanJia2023_EnterPlayScene")
end

function LuaHanJia2023Mgr:OnMainPlayerDestroyed()
    
end 

function LuaHanJia2023Mgr:ShowXuePoLiXianResult(bResult, curLevelId, costTime)
    LuaSnowAdventureResultWnd.bResult = bResult
    LuaSnowAdventureResultWnd.curLevelId = curLevelId
    LuaSnowAdventureResultWnd.costTime = costTime
    CUIManager.ShowUI(CLuaUIResources.SnowAdventureResultWnd)
end 

function LuaHanJia2023Mgr:GetSnowmanProp(snowmanType, snowmanLevel)
    local snowmanConfigData = HanJia2023_XuePoLiXianSnowman.GetData(snowmanType)
    local showAttributes = g_LuaUtil:StrSplit(snowmanConfigData.ShowAttrs,";")
    local propTbl = {}
    for i = 1, #showAttributes do
        local attributeInfo = g_LuaUtil:StrSplit(showAttributes[i],",")
        local propName = attributeInfo[1]
        if #attributeInfo == 2 then
            --name: A
            local formulaId = tonumber(attributeInfo[2])
            local val = self:CalculateFormula(formulaId, snowmanLevel)
            table.insert(propTbl, {name=propName, value = {val}})
        elseif #attributeInfo == 3 then
            --name: A~B
            local formulaId1 = tonumber(attributeInfo[2])
            local val1 = self:CalculateFormula(formulaId1, snowmanLevel)
            local formulaId2 = tonumber(attributeInfo[3])
            local val2 = self:CalculateFormula(formulaId2, snowmanLevel)
            table.insert(propTbl, {name=propName, value = {val1, val2}})
        end
    end
    return propTbl
end

function LuaHanJia2023Mgr:CalculateFormula(formulaId, snowmanLevel)
    --公式可以支持传入一个table, 用metaTable来index
    local formula = AllFormulas.Action_Formula[formulaId] and AllFormulas.Action_Formula[formulaId].Formula or nil
    if formula == nil then
        return 0
    end
    if CClientMainPlayer.Inst == nil then
        return 0
    end


    
    local packedFormula = {}
    packedFormula.FightProp = function(self)
        local prop = {}
        setmetatable(prop, {
            __index = function(_, key)
                local propName = string.gsub(key,"GetParam", "")
                local myFightProp = CClientMainPlayer.Inst.FightProp
                local val = myFightProp:GetParam(EPlayerFightProp[propName])
                return function()
                    return val
                end
                
            end
        })
        return prop
    end
    local res = formula(packedFormula, nil, {snowmanLevel})
    return res
end

function LuaHanJia2023Mgr:GetHanJia2023DefaultTaskTitle(gamePlayId)   -- 玩法名
    return Gameplay_Gameplay.GetData(gamePlayId).Name
end
function LuaHanJia2023Mgr:GetHanJia2023DefaultTaskContent(gamePlayId)     -- 玩法信息
    return Gameplay_Gameplay.GetData(gamePlayId).Name
end

function LuaHanJia2023Mgr:ReplyXuePoLiXianBattleInfo(ret_U)
    local unpackData = g_MessagePack.unpack(ret_U)
    LuaHanJia2023Mgr.battleStat = {}
    LuaHanJia2023Mgr.battleStat = unpackData
    CUIManager.ShowUI(CLuaUIResources.SnowAdventureStatwnd)
end

function LuaHanJia2023Mgr:SyncXianKeLaiPlayData(progress, vip1, vip2, resetNum, rewardInfo_U, addRate)
    local taskTbl = {}
    if LuaHanJia2023Mgr.XKLData and LuaHanJia2023Mgr.XKLData.TaskData then
        taskTbl = LuaHanJia2023Mgr.XKLData.TaskData
    end
    
    LuaHanJia2023Mgr.XKLData = {}
    local calLevel = 1
    local curSum = 0
    local maxlv = 0
    local cfg = {}
    HanJia2023_XianKeLaiLevelReward.Foreach(function (key, data)
        if curSum + data.NeedXianLu <= progress then
            curSum = curSum + data.NeedXianLu
            calLevel = data.Level
        end

        cfg[key] = {
            NeedXianLu   = data.NeedXianLu,
            NmlItems = LuaHanJiaMgr.GetItems(data.ItemRewards1),
            AdvItems = LuaHanJiaMgr.GetItems(data.ItemRewards2),
            UltraItems = LuaHanJiaMgr.GetItems(data.ItemRewards3)
        }
        if key > maxlv then
            maxlv = key
        end
    end)
    LuaHanJia2023Mgr.XKLData.Lv = calLevel
    LuaHanJia2023Mgr.XKLData.Exp = progress
    LuaHanJia2023Mgr.XKLData.PassType = 1*vip1 + 2*vip2
    LuaHanJia2023Mgr.XKLData.resetNum = resetNum
    LuaHanJia2023Mgr.XKLData.addRate = addRate
    local tbl = g_MessagePack.unpack(rewardInfo_U or {})
    LuaHanJia2023Mgr.XKLData.RewardInfo = {}
    for i = 1, #tbl, 2 do
        LuaHanJia2023Mgr.XKLData.RewardInfo[tbl[i]] = tbl[i+1]
    end
    LuaHanJia2023Mgr:CheckHaveReward(true)
    LuaHanJia2023Mgr.XKLData.TaskData = taskTbl
    LuaHanJia2023Mgr.XKLData.Cfg = cfg
    LuaHanJia2023Mgr.XKLData.CfgMaxLv = maxlv
    
    g_ScriptEvent:BroadcastInLua("HanJia2023_SyncXianKeLianData")
end

function LuaHanJia2023Mgr:LoadCfg(forceupdate)
    if forceupdate then
        local maxlv = 0
        local cfg = {}
        print("load cfg")
        HanJia2023_XianKeLaiLevelReward.Foreach(function (key, data)
            cfg[key] = {
                NeedXianLu   = data.NeedXianLu,
                NmlItems = LuaHanJiaMgr.GetItems(data.ItemRewards1),
                AdvItems = LuaHanJiaMgr.GetItems(data.ItemRewards2),
                UltraItems = LuaHanJiaMgr.GetItems(data.ItemRewards3)
            }
            if key > maxlv then
                maxlv = key
            end
        end)

        LuaHanJia2023Mgr.XKLData = {}
        LuaHanJia2023Mgr.XKLData.Cfg = cfg
        LuaHanJia2023Mgr.XKLData.CfgMaxLv = maxlv
    end
end

function LuaHanJia2023Mgr:CheckMinRewardLv(minLevel, maxLevel)
    local data = LuaHanJia2023Mgr.XKLData
    local passtype = data.PassType
    if data then
        for i = minLevel, maxLevel do
            if data.RewardInfo[i] then
                if passtype == 0 and data.RewardInfo[i] ~= 1 then
                    return i
                elseif passtype == 1 and data.RewardInfo[i] ~= 3 then
                    return i
                elseif passtype == 2 and data.RewardInfo[i] ~= 5 then
                    return i
                elseif passtype == 3 and data.RewardInfo[i] ~= 7 then
                    return i
                end
            else
                return i
            end
        end
    end
    return -1
end

function LuaHanJia2023Mgr:CheckHaveRewardRange(middle, range)
    if middle == nil or range == nil then
        return false
    end
    local minLevel = math.max(1, middle-range)
    local maxLevel = math.min(LuaHanJia2023Mgr.XKLData.CfgMaxLv, middle+range)
    return self:CheckMinRewardLv(minLevel, maxLevel) ~= -1
end

function LuaHanJia2023Mgr:CheckHaveReward(forceCalculate)
    --减少重复计算, 把计算结果缓存下来..
    --如果有卡顿的话, 可以把数据先设置为dirty, 在取数据时再判断更新, 而不是在每次同步数据时更新
    if forceCalculate then
        --必须重新计算
    else
        if LuaHanJia2023Mgr.XKLData and LuaHanJia2023Mgr.XKLData.havereward then
            return LuaHanJia2023Mgr.XKLData.havereward
        else
            --之前没数据, 需要重新计算
        end
    end
    
    local havereward = (self:CheckMinRewardLv(1, LuaHanJia2023Mgr.XKLData.Lv) ~= -1)

    if LuaHanJia2023Mgr.XKLData then
        LuaHanJia2023Mgr.XKLData.havereward = havereward
    end
    return havereward
end

function LuaHanJia2023Mgr:SyncXianKeLaiTaskData(ret_U)
    local unpackData = g_MessagePack.unpack(ret_U)
    LuaHanJia2023Mgr.XKLData.TaskData = {}
    for i = 1, #unpackData, 2 do
        local tbl = {}
        tbl.TaskID = unpackData[i]
        tbl.Progress = unpackData[i+1]
        local taskCfg = HanJia2023_XianKeLaiTask.GetData(tbl.TaskID)
        tbl.IsRewarded = tbl.Progress >= taskCfg.Target
        table.insert(LuaHanJia2023Mgr.XKLData.TaskData, tbl)
    end
    g_ScriptEvent:BroadcastInLua("HanJia2023_SyncXianKeLianData")
end

function LuaHanJia2023Mgr:UnLockXianKeLaiVipSuccess(vip)
    local battlePassLevel = LuaHanJia2023Mgr.XKLData.Lv
    local reward1List = {}
    local reward2List = {}
    local reward2Text = ""

    for i = 1, battlePassLevel do
        local itemCfg = LuaHanJia2023Mgr.XKLData.Cfg[i]
        if vip == 1 then
            local items = itemCfg.AdvItems
            for idx = 1, #items do
                table.insert(reward1List, items[idx])
            end
        elseif vip == 2 then
            local items = itemCfg.UltraItems
            for idx = 1, #items do
                table.insert(reward1List, items[idx])
            end
        end
    end
    for i = battlePassLevel+1, LuaHanJia2023Mgr.XKLData.CfgMaxLv do
        local itemCfg = LuaHanJia2023Mgr.XKLData.Cfg[i]
        if vip == 1 then
            local items = itemCfg.AdvItems
            for idx = 1, #items do
                table.insert(reward2List, items[idx])
            end
        elseif vip == 2 then
            local items = itemCfg.UltraItems
            for idx = 1, #items do
                table.insert(reward2List, items[idx])
            end
        end
    end
    if #reward2List ~= 0 then
        reward2Text = LocalString.GetString("将花蕊升至满级还可获得:")
    end
    
    LuaCommonGetRewardWnd.m_materialName = "gongxijiesuo"
    LuaCommonGetRewardWnd.m_Reward1List = reward1List
    LuaCommonGetRewardWnd.m_Reward2Label = reward2Text
    LuaCommonGetRewardWnd.m_Reward2List = reward2List
    LuaCommonGetRewardWnd.m_hint = ""
    LuaCommonGetRewardWnd.m_button = {
        {spriteName="blue", buttonLabel = LocalString.GetString("确定"), clickCB = function() CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd) end},
    }
    CUIManager.ShowUI(CLuaUIResources.CommonGetRewardWnd)
    g_ScriptEvent:BroadcastInLua("HanJia2023_XianKeLaiPassTypeChange")
end

function LuaHanJia2023Mgr:ReceiveXianKeLaiRewardSuccess(level, index, resultTbl_U)
    if level == 0 and index == 0 then
        local passType = LuaHanJia2023Mgr.XKLData.PassType
        local rewardInfo = g_MessagePack.unpack(resultTbl_U or {})
        local battlePassLevel = LuaHanJia2023Mgr.XKLData.Lv
        
        local reward1List = {}
        local reward2List = {}
        local reward2Text = ""
        local buttonList = {{spriteName="blue", buttonLabel = LocalString.GetString("确定"), clickCB = function() CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd) end}}

        local regex = CreateFromClass(Regex, "(\\d+)-(\\d+)", RegexOptions.Singleline)
        for i = 1, #rewardInfo do
            local rewardRawData = rewardInfo[i]
            local match = regex:Match(rewardRawData)
            if match.Success then
                local level = tonumber(ToStringWrap(match.Groups[1]))
                local index = tonumber(ToStringWrap(match.Groups[2]))
                local itemCfg = LuaHanJia2023Mgr.XKLData.Cfg[level]
                local items = {}
                if index == 1 then
                    items = itemCfg.NmlItems
                elseif index == 2 then
                    items = itemCfg.AdvItems
                elseif index == 3 then
                    items = itemCfg.UltraItems
                end
                for idx = 1, #items do
                    table.insert(reward1List, items[idx])
                end
            end
        end
        
        if passType == 0 then
            --没购买
            reward2Text = g_MessageMgr:FormatMessage("HanJia2023_BattlePass_UnlockBoth_Tips")
            for i = 1, battlePassLevel do
                local itemCfg = LuaHanJia2023Mgr.XKLData.Cfg[i]
                local items1 = itemCfg.AdvItems
                for idx = 1, #items1 do
                    table.insert(reward2List, items1[idx])
                end
                local items2 = itemCfg.UltraItems
                for idx = 1, #items2 do
                    table.insert(reward2List, items2[idx])
                end
            end
            table.insert(buttonList, {spriteName="yellow", 
                                 buttonLabel = LocalString.GetString("解锁高级花蕊"), 
                                 clickCB = function() 
                                     CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd)
                                     CUIManager.ShowUI(CLuaUIResources.HanJia2023PassDetailWnd)
                                 end})
        elseif passType == 1 then
            --购买普通
            reward2Text = g_MessageMgr:FormatMessage("HanJia2023_BattlePass_UnlockUltra_Tips")
            for i = 1, battlePassLevel do
                local itemCfg = LuaHanJia2023Mgr.XKLData.Cfg[i]
                local items2 = itemCfg.UltraItems
                for idx = 1, #items2 do
                    table.insert(reward2List, items2[idx])
                end
            end
            table.insert(buttonList, {spriteName="yellow",
                                      buttonLabel = LocalString.GetString("解锁高级花蕊"),
                                      clickCB = function()
                                          CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd)
                                          CUIManager.ShowUI(CLuaUIResources.HanJia2023PassDetailWnd)
                                      end})
        elseif passType == 2 then
            --购买高级
            reward2Text = g_MessageMgr:FormatMessage("HanJia2023_BattlePass_UnlockAdv_Tips")
            for i = 1, battlePassLevel do
                local itemCfg = LuaHanJia2023Mgr.XKLData.Cfg[i]
                local items1 = itemCfg.AdvItems
                for idx = 1, #items1 do
                    table.insert(reward2List, items1[idx])
                end
            end
            table.insert(buttonList, {spriteName="yellow",
                                      buttonLabel = LocalString.GetString("解锁高级花蕊"),
                                      clickCB = function()
                                          CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd)
                                          CUIManager.ShowUI(CLuaUIResources.HanJia2023PassDetailWnd)
                                      end})
        elseif passType == 3 then    
            --全购买, --doNothing
        end


        LuaCommonGetRewardWnd.m_materialName = "gongxihuode"
        LuaCommonGetRewardWnd.m_Reward1List = reward1List
        LuaCommonGetRewardWnd.m_Reward2Label = reward2Text
        LuaCommonGetRewardWnd.m_Reward2List = reward2List
        LuaCommonGetRewardWnd.m_hint = ""
        LuaCommonGetRewardWnd.m_button = buttonList
        CUIManager.ShowUI(CLuaUIResources.CommonGetRewardWnd)
    end
end

function LuaHanJia2023Mgr:SyncHanHuaMiBaoData(ret_U)
    local unpackData = g_MessagePack.unpack(ret_U)
    LuaHanJia2023Mgr.lotteryData = {}
    LuaHanJia2023Mgr.lotteryData.curHuoLi = unpackData.curHuoLi
    LuaHanJia2023Mgr.lotteryData.lastSelect = unpackData.lastSelect
    LuaHanJia2023Mgr.lotteryData.lotteryResult = unpackData.lotteryResult
    g_ScriptEvent:BroadcastInLua("HanJia2023_SyncLotteryData")
end
