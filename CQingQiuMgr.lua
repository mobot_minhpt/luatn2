-- Auto Generated!!
local Byte = import "System.Byte"
local CActivityMgr = import "L10.Game.CActivityMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFileTools = import "CFileTools"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CQingQiuMgr = import "L10.UI.CQingQiuMgr"
local CQingQiuMonster = import "L10.UI.CQingQiuMonster"
local CQingQiuScore = import "L10.UI.CQingQiuScore"
local CScene = import "L10.Game.CScene"
local CTickMgr = import "L10.Engine.CTickMgr"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CUIManager = import "L10.UI.CUIManager"
local DelegateFactory = import "DelegateFactory"
local Divination_QianWen = import "L10.Game.Divination_QianWen"
local EnumEventType = import "EnumEventType"
local EShareType = import "L10.UI.EShareType"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local HTTPHelper = import "L10.Game.HTTPHelper"
local Json = import "L10.Game.Utils.Json"
local Main = import "L10.Engine.Main"
local Monster_Monster = import "L10.Game.Monster_Monster"
local MsgPackImpl = import "MsgPackImpl"
local NPC_NPC = import "L10.Game.NPC_NPC"
local Object = import "System.Object"
local QingQiuPlay_GameSetting = import "L10.Game.QingQiuPlay_GameSetting"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local String = import "System.String"
local StringBuilder = import "System.Text.StringBuilder"
local System = import "System"
local WWW = import "UnityEngine.WWW"
local MD5 =  import "System.Security.Cryptography.MD5"
local CYaoYiYaoMgr = import "L10.Game.CYaoYiYaoMgr"


CQingQiuMgr.m_OpenZhanBuWnd_CS2LuaHook = function (this, zhanBuId) 
    if Divination_QianWen.Exists(zhanBuId) then
        this.m_ZhanBuId = zhanBuId
        g_MessageMgr:ShowMessage("YuanDan_YaoYiYao")
        CUIManager.ShowUI(CIndirectUIResources.QingQiuZhanBuWnd)
        CYaoYiYaoMgr.Inst:ShowWnd(g_MessageMgr:FormatMessage("Divination_Shake_Tip"), g_MessageMgr:FormatMessage("Divination_Shake_Tip_PC"))
    end
end
CQingQiuMgr.m_AddActivity_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.Level < this.m_MinGrate then
        return
    end
    if this.m_PlayStartTime < 0 then
        return
    end
    if this.m_PlayState == EnumQingQiuPlayState_lua.Idle or this.m_PlayState == EnumQingQiuPlayState_lua.BossKilled then
        return
    end

    local info = System.String.Format(QingQiuPlay_GameSetting.GetData().ScoreTaskDesc, this.m_ChuYaoScore)

    CActivityMgr.Inst:UpdateAcitivity(this.m_ActivityName, info, DelegateFactory.Action(function () 
        CUIManager.ShowUI(CIndirectUIResources.QingQiuBaoWeiStartWnd)
    end), math.floor((this.m_PlayStartTime + this.m_PlayDuration * 60)), QingQiuPlay_GameSetting.GetData().TaskDescTitle)
end
CQingQiuMgr.m_IsInQingQiuScene_CS2LuaHook = function (this) 
    if CScene.MainScene == nil then
        return false
    end
    if CScene.MainScene.SceneTemplateId == this.m_QingQiuSceneId then
        return true
    else
        return false
    end
end
CQingQiuMgr.m_IsInQingQiuCopyScene_CS2LuaHook = function (this) 
    if CScene.MainScene == nil then
        return false
    end
    if CScene.MainScene.SceneTemplateId == this.m_QingQiuCopySceneId then
        return true
    else
        return false
    end
end
CQingQiuMgr.m_ShareTicket_CS2LuaHook = function (this) 

    if this.m_UrlCoroutine ~= nil then
        Main.Inst:StopCoroutine(this.m_UrlCoroutine)
    end
    if nil == CClientMainPlayer.Inst then
        return
    end
    local sb = NewStringBuilderWraper(StringBuilder, CQingQiuMgr.s_TicketUrl)
    CommonDefs.StringBuilder_Append_StringBuilder_UInt64(sb:Append("roleid="), CClientMainPlayer.Inst.Id)
    sb:Append("&rolename="):Append(WWW.EscapeURL(CClientMainPlayer.Inst.Name))
    CommonDefs.StringBuilder_Append_StringBuilder_UInt32(sb:Append("&grade="), CClientMainPlayer.Inst.Level)
    CommonDefs.StringBuilder_Append_StringBuilder_UInt32(sb:Append("&gender="), EnumToInt(CClientMainPlayer.Inst.Gender))
    sb:Append("&servername=")
    local gameServer = CLoginMgr.Inst:GetSelectedGameServer()
    if gameServer ~= nil then
        sb:Append(WWW.EscapeURL(gameServer.name))
    end
    CommonDefs.StringBuilder_Append_StringBuilder_UInt32(sb:Append("&profession="), EnumToInt(CClientMainPlayer.Inst.Class))
    sb:Append("&gang="):Append(WWW.EscapeURL(CGuildMgr.Inst.m_GuildName))
    local md5 = MD5.Create()
    local inputBytes = CommonDefs.ASCIIEncoding_GetBytes((tostring(CClientMainPlayer.Inst.Id) .. CClientMainPlayer.Inst.Level) .. CQingQiuMgr.s_CipherCode)
    local hash = md5:ComputeHash(inputBytes)
    sb:Append("&token="):Append((NumberComplexToString(hash[0], typeof(Byte), "x2") .. NumberComplexToString(hash[1], typeof(Byte), "x2")) .. NumberComplexToString(hash[2], typeof(Byte), "x2"))

    this.m_UrlCoroutine = Main.Inst:StartCoroutine(HTTPHelper.GetUrl(ToStringWrap(sb), DelegateFactory.Action_bool_string(function (success, ret) 
        local dict = TypeAs(Json.Deserialize(ret), typeof(MakeGenericClass(Dictionary, String, Object)))
        if nil == dict then
            return
        end
        if not CommonDefs.DictContains(dict, typeof(String), "code") or math.floor(tonumber(CommonDefs.DictGetValue(dict, typeof(String), "code") or 0)) <= 0 or not CommonDefs.DictContains(dict, typeof(String), "imgurl") then
            return
        end
        this:ShareTicketImage(ToStringWrap(CommonDefs.DictGetValue(dict, typeof(String), "imgurl")))
    end)))
end
CQingQiuMgr.m_ShareTicketImage_CS2LuaHook = function (this, imageUrl) 
    if this.m_PicCoroutine ~= nil then
        Main.Inst:StopCoroutine(this.m_PicCoroutine)
        this.m_PicCoroutine = nil
    end

    this.m_PicCoroutine = Main.Inst:StartCoroutine(HTTPHelper.DownloadImage(imageUrl, DelegateFactory.Action_bool_byte_Array(function (picResult, picData) 
        local filename = Utility.persistentDataPath .. "/TicketShare.jpg"
        System.IO.File.WriteAllBytes(filename, picData)
        CFileTools.SetFileNoBackup(filename)

        CTickMgr.Register(DelegateFactory.Action(function () 
            ShareBoxMgr.OpenShareBox(EShareType.TicketShare, 0, {filename})
        end), 1000, ETickType.Once)
    end)))
end
Gas2Gac.QingQiuQueryScoreRankResult = function (data) 
    if data == nil or CClientMainPlayer.Inst == nil then
        return
    end
    --try
    --{
    local rankResult = TypeAs(MsgPackImpl.unpack(data), typeof(MakeGenericClass(List, Object)))
    if rankResult == nil then
        return
    end
    CommonDefs.ListClear(CQingQiuMgr.Inst.m_ScoreData)
    local myIndex = - 1
    do
        local i = 0 local cnt = rankResult.Count
        while i < cnt do
            local continue
            repeat
                local dict = TypeAs(rankResult[i], typeof(MakeGenericClass(Dictionary, String, Object)))
                if dict == nil then
                    continue = true
                    break
                end
                local scoreData = CreateFromClass(CQingQiuScore)
                scoreData.m_Id = math.floor(tonumber(CommonDefs.DictGetValue(dict, typeof(String), "id") or 0))
                scoreData.m_Clazz = math.floor(tonumber(CommonDefs.DictGetValue(dict, typeof(String), "class") or 0))
                scoreData.m_Name = ToStringWrap(CommonDefs.DictGetValue(dict, typeof(String), "name"))
                scoreData.m_Score = math.floor(tonumber(CommonDefs.DictGetValue(dict, typeof(String), "score") or 0))
                scoreData.m_Rank = i + 1
                CommonDefs.ListAdd(CQingQiuMgr.Inst.m_ScoreData, typeof(CQingQiuScore), scoreData)
                if CClientMainPlayer.Inst.Id == scoreData.m_Id then
                    myIndex = i
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end

    if myIndex > 0 then
        local mySocre = CQingQiuMgr.Inst.m_ScoreData[myIndex]
        CommonDefs.ListInsert(CQingQiuMgr.Inst.m_ScoreData, 0, typeof(CQingQiuScore), mySocre)
    elseif myIndex < 0 then
        local score = CreateFromClass(CQingQiuScore)
        score.m_Id = CClientMainPlayer.Inst.Id
        score.m_Clazz = EnumToInt(CClientMainPlayer.Inst.Class)
        score.m_Name = CClientMainPlayer.Inst.Name
        score.m_Score = CQingQiuMgr.Inst.m_ChuYaoScore
        score.m_Rank = System.Int32.MaxValue
        CommonDefs.ListInsert(CQingQiuMgr.Inst.m_ScoreData, 0, typeof(CQingQiuScore), score)
    end
    EventManager.Broadcast(EnumEventType.ReplyQingQiuScoreDataFinished)
    --}
    --catch (Exception e)
    --{
    --	//解析失败，提示一下或者记个Log
    --	L10.CLogMgr.LogWarning(e.ToString());
    --}
end
Gas2Gac.QingQiuQueryPlayStateResult = function (state, playStartTime) 
    CQingQiuMgr.Inst.m_PlayState = state
    EventManager.BroadcastInternalForLua(EnumEventType.ReplyQingQiuPlayState, {state})
    CQingQiuMgr.Inst.m_PlayStartTime = playStartTime
    if state == EnumQingQiuPlayState_lua.Part_1 or state == EnumQingQiuPlayState_lua.Part_2 then
        CQingQiuMgr.Inst:AddActivity()
    else
        CQingQiuMgr.Inst:DeleteActivity()
    end
end
Gas2Gac.ClearQingQiuLocalData = function () 
    CQingQiuMgr.Inst.m_PlayStartTime = - 1
    CQingQiuMgr.Inst.m_ChuYaoScore = 0
    CQingQiuMgr.Inst.m_PlayState = 0
    CQingQiuMgr.Inst:DeleteActivity()
    CQingQiuMgr.Inst.m_PlayState = EnumQingQiuPlayState_lua.Idle
end
Gas2Gac.QingQiuTeamMemberScoreUpdate = function (data) 
    if data == nil then
        return
    end
    if CClientMainPlayer.Inst == nil then
        return
    end
    --try
    --{
    local fightData = TypeAs(MsgPackImpl.unpack(data), typeof(MakeGenericClass(Dictionary, String, Object)))
    if fightData == nil then
        return
    end

    CommonDefs.DictIterate(fightData, DelegateFactory.Action_object_object(function (___key, ___value) 
        local kvPair = {}
        kvPair.Key = ___key
        kvPair.Value = ___value
        if math.floor(tonumber(kvPair.Key or 0)) == CClientMainPlayer.Inst.Id then
            CQingQiuMgr.Inst.m_ChuYaoScore = math.floor(tonumber(kvPair.Value or 0))
            CQingQiuMgr.Inst:AddActivity()
            return
        end
    end))
    --} 
    --catch (Exception e)
    --{
    --	L10.CLogMgr.LogWarning(e.ToString());
    --}
end
Gas2Gac.PreCheckGiveHelpTaoHuaBaoXiaResult = function (targetPlayerId, itemTemplateId, result, sceneTemplateId, sceneId, x, y) 
    if not result then
        return
    end
    CTrackMgr.Inst:FindLocation(sceneId, sceneTemplateId, math.floor(x), math.floor(y), DelegateFactory.Action(function () 
        Gac2Gas.TryGiveHelpTaoHuaBaoXia(targetPlayerId, itemTemplateId)
    end), nil, nil, 0)
end
Gas2Gac.QingQiuQueryNpcAndMonsterLocationResult = function (data) 
    if data == nil then
        return
    end

    --try
    --{
    local locationData = TypeAs(MsgPackImpl.unpack(data), typeof(MakeGenericClass(Dictionary, String, Object)))
    if locationData == nil then
        return
    end

    local monsterLocations = CreateFromClass(MakeGenericClass(List, CQingQiuMonster))

    CommonDefs.DictIterate(locationData, DelegateFactory.Action_object_object(function (___key, ___value) 
        local v = {}
        v.Key = ___key
        v.Value = ___value
        local continue
        repeat
            if not CQingQiuMgr.s_ShowNpcInMap and (v.Key == "Npc") then
                continue = true
                break
            end
            local npcLocations = TypeAs(CommonDefs.DictGetValue(locationData, typeof(String), v.Key), typeof(MakeGenericClass(Dictionary, String, Object)))
            if npcLocations ~= nil then
                CommonDefs.DictIterate(npcLocations, DelegateFactory.Action_object_object(function (___key, ___value) 
                    local scene = {}
                    scene.Key = ___key
                    scene.Value = ___value
                    local continue
                    repeat
                        local sceneNpc = TypeAs(scene.Value, typeof(MakeGenericClass(Dictionary, String, Object)))
                        if sceneNpc ~= nil and CommonDefs.DictContains(sceneNpc, typeof(String), "Locations") then
                            local locations = TypeAs(CommonDefs.DictGetValue(sceneNpc, typeof(String), "Locations"), typeof(MakeGenericClass(List, Object)))
                            if locations ~= nil then
                                do
                                    local i = 0 local cnt = locations.Count
                                    while i < cnt do
                                        local continue
                                        repeat
                                            local loc = TypeAs(locations[i], typeof(MakeGenericClass(List, Object)))
                                            if loc == nil then
                                                continue = true
                                                break
                                            end
                                            local monster = CreateFromClass(CQingQiuMonster)
                                            monster.m_PosX = math.floor(tonumber(loc[3] or 0))
                                            monster.m_PosY = math.floor(tonumber(loc[4] or 0))
                                            local engineId = math.floor(tonumber(loc[0] or 0))
                                            monster.m_EngineId = engineId
                                            monster.m_Type = v.Key
                                            if (monster.m_Type == "Npc") then
                                                local npc = NPC_NPC.GetData(math.floor(tonumber(loc[5] or 0)))
                                                if npc ~= nil then
                                                    monster.m_Name = npc.Name
                                                end
                                            elseif (monster.m_Type == "Monster") then
                                                local mon = Monster_Monster.GetData(math.floor(tonumber(loc[5] or 0)))
                                                if mon ~= nil then
                                                    monster.m_Name = mon.Name
                                                end
                                            end
                                            CommonDefs.ListAdd(monsterLocations, typeof(CQingQiuMonster), monster)
                                            continue = true
                                        until 1
                                        if not continue then
                                            break
                                        end
                                        i = i + 1
                                    end
                                end
                            end
                        end
                        continue = true
                    until 1
                    if not continue then
                        return
                    end
                end))
            end
            continue = true
        until 1
        if not continue then
            return
        end
    end))

    EventManager.BroadcastInternalForLua(EnumEventType.SetQingQiuMonsterInMap, {monsterLocations})
    --}
    --catch (Exception e)
    --{
    --	L10.CLogMgr.LogWarning(e.ToString());
    --}
end
