local GameObject = import "UnityEngine.GameObject"
local UILongPressButton = import "L10.UI.UILongPressButton"
local UILabel = import "UILabel"
local UIWidget = import "UIWidget"
local UITexture = import "UITexture"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaEmployCatAgreementWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaEmployCatAgreementWnd, "PlayerName", "PlayerName", UILabel)
RegistChildComponent(LuaEmployCatAgreementWnd, "PlayerInfo", "PlayerInfo", UIWidget)
RegistChildComponent(LuaEmployCatAgreementWnd, "OtherFingerprints", "OtherFingerprints", UIWidget)
RegistChildComponent(LuaEmployCatAgreementWnd, "CatFingerprint", "CatFingerprint", UITexture)
RegistChildComponent(LuaEmployCatAgreementWnd, "ClickArea", "ClickArea", UILongPressButton)
RegistChildComponent(LuaEmployCatAgreementWnd, "Tips", "Tips", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaEmployCatAgreementWnd, "m_Tweeners")
RegistClassMember(LuaEmployCatAgreementWnd, "m_IsAnim")
RegistClassMember(LuaEmployCatAgreementWnd, "m_Tick")
RegistClassMember(LuaEmployCatAgreementWnd, "m_CatFingerprintAlpha")

function LuaEmployCatAgreementWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.ClickArea.OnLongPressDelegate = DelegateFactory.Action(function ()
        if not self.m_IsAnim then
            self.m_IsAnim = true
		    self:OnLongPress()
        end
    end)
    self.m_Tweeners = {}
    self.m_IsAnim = false
    self.m_CatFingerprintAlpha = self.CatFingerprint.alpha
end

function LuaEmployCatAgreementWnd:Init()
    self.PlayerName.text = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name or ""
    self.Tips.alpha = 1
    self.PlayerInfo.alpha = 0
    self.OtherFingerprints.alpha = 0
    self.CatFingerprint.alpha = 0
end

--@region UIEvent

--@endregion UIEvent
function LuaEmployCatAgreementWnd:OnLongPress()
    LuaTweenUtils.TweenAlpha(self.Tips, 1, 0, 0.2)
    local tweener = LuaTweenUtils.TweenAlpha(self.PlayerInfo, 0, 1, 0.5)
    CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(function ()
        self:Phase1()
    end))
    table.insert(self.m_Tweeners, tweener)
end

function LuaEmployCatAgreementWnd:Phase1()
    Gac2Gas.TradeSimulationPingMao()
    local tweener = LuaTweenUtils.TweenAlpha(self.OtherFingerprints, 0, 1, 1)
    CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(function ()
        self:Phase2()
    end))
    table.insert(self.m_Tweeners, tweener)
end

function LuaEmployCatAgreementWnd:Phase2()
    local tweener = LuaTweenUtils.TweenAlpha(self.CatFingerprint, 0, self.m_CatFingerprintAlpha, 1)
    CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(function ()
        self:Phase3()
    end))
    table.insert(self.m_Tweeners, tweener)
end

function LuaEmployCatAgreementWnd:Phase3()
    UnRegisterTick(self.m_Tick)
    self.m_Tick = RegisterTickOnce(function()
        CUIManager.CloseUI(CLuaUIResources.EmployCatAgreementWnd)
    end, 1000)
end

function LuaEmployCatAgreementWnd:OnDestroy()
    if self.m_Tweeners then
        for i = 1, #self.m_Tweeners do
            LuaTweenUtils.Kill(self.m_Tweeners[i], false)
        end
    end
    UnRegisterTick(self.m_Tick)
end
