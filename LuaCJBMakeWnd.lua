local CCJBMakeWnd = import "L10.UI.CCJBMakeWnd"
local EnumChuanjiabaoMakeStage = import "L10.Game.EnumChuanjiabaoMakeStage"
local CCJBMgr = import "L10.Game.CCJBMgr"
local Lua = import "L10.Engine.Lua"

CCJBMakeWnd.m_hookInitStage1 = function(this)
	Lua.s_IsHook = false
  local fxNode = this.transform:Find('Fx').gameObject
  fxNode:SetActive(true)
  fxNode:GetComponent(typeof(CUIFx)):LoadFx('fx/ui/prefab/UI_chuanjiabao_liuguang.prefab')
  --fxNode:GetComponent(typeof(CUIFx)):LoadFx('fx/ui/prefab/UI_chuanjiabao01_front.prefab')

end

CCJBMakeWnd.m_hookStartMakeCJB = function(this)
  this.ShapeOpNode:SetActive(false)
  this.DecalOpNode:SetActive(false)
  this.MakeOpNode:SetActive(true)
  this.TargetShapeNode:SetActive(false)
  --local fxNode = this.transform:Find('Fx').gameObject
  --fxNode:SetActive(false)

  CCJBMgr.Inst.makeStage = EnumChuanjiabaoMakeStage.BakedNotNamed

  this.FxNode:SetActive(true)
  --this.m_FrontFx:LoadFx(CUIFxPaths.CJBMakeFront)
  this.m_BackFx:LoadFx('fx/ui/prefab/UI_chuanjiabao_huoyan.prefab')

  this.houseCJBMaker:CleanOpration()
  this.CJBShowByInitData:StopRotateAndHideBase()
  CCJBMgr.Inst:CleanSaveInitInfo()
end
