require("common/common_include")

local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local CUITexture = import "L10.UI.CUITexture"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local EnumSkillInfoContext = import "L10.UI.CSkillInfoMgr+EnumSkillInfoContext"
local NGUIText = import "NGUIText"

LuaWaKuangUpgradeWnd = class()

RegistChildComponent(LuaWaKuangUpgradeWnd, "UpgradeJiPaoItem", GameObject)
RegistChildComponent(LuaWaKuangUpgradeWnd, "UpgradeWaKuangItem", GameObject)
RegistChildComponent(LuaWaKuangUpgradeWnd, "UpgradePackageCountItem", GameObject)
RegistChildComponent(LuaWaKuangUpgradeWnd, "UpgradeCapacityItem", GameObject)
RegistChildComponent(LuaWaKuangUpgradeWnd, "OKButton", GameObject)

RegistClassMember(LuaWaKuangUpgradeWnd, "SelectedUpgradeType")
RegistClassMember(LuaWaKuangUpgradeWnd, "ItemList")

function LuaWaKuangUpgradeWnd:Init()
	self.SelectedUpgradeType = 0
	self.ItemList = {}
	table.insert(self.ItemList, self.UpgradeJiPaoItem)
	table.insert(self.ItemList, self.UpgradeWaKuangItem)
	table.insert(self.ItemList, self.UpgradePackageCountItem)
	table.insert(self.ItemList, self.UpgradeCapacityItem)

	self:Refresh()

	CommonDefs.AddOnClickListener(self.OKButton, DelegateFactory.Action_GameObject(function (go)
		self:OnOKButtonClicked(go)
	end), false)
end

function LuaWaKuangUpgradeWnd:Refresh()
	self:InitUpgradeJiPaoItem()
	self:InitUpgradeWaKuangItem()
	self:InitUpgradePackageCountItem()
	self:InitUpgradeCapacityItem()
end

-- 升级疾跑
function LuaWaKuangUpgradeWnd:InitUpgradeJiPaoItem()
	local UpgradeTypeLabel = self.UpgradeJiPaoItem.transform:Find("UpgradeTypeLabel"):GetComponent(typeof(UILabel))
	local CurrentLabel = self.UpgradeJiPaoItem.transform:Find("CurrentLabel"):GetComponent(typeof(UILabel))
	local SkillNode = self.UpgradeJiPaoItem.transform:Find("UpgradePic/SkillNode").gameObject
	local SkillIcon = self.UpgradeJiPaoItem.transform:Find("UpgradePic/SkillNode/Mask/Icon"):GetComponent(typeof(CUITexture))

	CurrentLabel.text = nil
	SkillIcon:Clear()

	local setting = WuYi_WaKuangSetting.GetData()
	local initSkillId = setting.InitJiaSuSkillId
	local maxSkillId = setting.MaxJiaSuSkillId

	-- 获得对应的等级
	local skillCls = Skill_AllSkills.GetClass(initSkillId)
	local playerSkillId = CClientMainPlayer.Inst.SkillProp:GetOriginalSkillIdByCls(skillCls)
	local skill = Skill_AllSkills.GetData(playerSkillId)
	SkillIcon:LoadSkillIcon(skill.SkillIcon)

	local playerSkillLevel = Skill_AllSkills.GetLevel(playerSkillId)
	local maxSkillLevel = Skill_AllSkills.GetLevel(maxSkillId)
	CurrentLabel.text = SafeStringFormat3(LocalString.GetString("[ACF9FF]当前等级[-] %d/%d"), playerSkillLevel, maxSkillLevel)

	CommonDefs.AddOnClickListener(SkillNode, DelegateFactory.Action_GameObject(function (go)
		self:OnSkillClicked(go, playerSkillId)
	end), false)

	if playerSkillLevel < maxSkillLevel then
		UpgradeTypeLabel.color = NGUIText.ParseColor24("FFC300", 0)
		CommonDefs.AddOnClickListener(self.UpgradeJiPaoItem, DelegateFactory.Action_GameObject(function (go)
			self:OnItemClicked(1, 2)
		end), false)
	else
		UpgradeTypeLabel.color = NGUIText.ParseColor24("B2B2B2", 0)
		CommonDefs.AddOnClickListener(self.UpgradeJiPaoItem, DelegateFactory.Action_GameObject(function (go)
			
		end), false)
	end
	
end

-- 升级挖矿
function LuaWaKuangUpgradeWnd:InitUpgradeWaKuangItem()
	local UpgradeTypeLabel = self.UpgradeWaKuangItem.transform:Find("UpgradeTypeLabel"):GetComponent(typeof(UILabel))
	local CurrentLabel = self.UpgradeWaKuangItem.transform:Find("CurrentLabel"):GetComponent(typeof(UILabel))
	local SkillNode = self.UpgradeWaKuangItem.transform:Find("UpgradePic/SkillNode").gameObject
	local SkillIcon = self.UpgradeWaKuangItem.transform:Find("UpgradePic/SkillNode/Mask/Icon"):GetComponent(typeof(CUITexture))

	CurrentLabel.text = nil
	SkillIcon:Clear()

	local setting = WuYi_WaKuangSetting.GetData()
	local initSkillId = setting.InitWaKuangSkillId
	local maxSkillId = setting.MaxWaKuangSkillId

	local skillCls = Skill_AllSkills.GetClass(initSkillId)
	local playerSkillId = CClientMainPlayer.Inst.SkillProp:GetOriginalSkillIdByCls(skillCls)
	local skill = Skill_AllSkills.GetData(playerSkillId)
	SkillIcon:LoadSkillIcon(skill.SkillIcon)


	local playerSkillLevel = Skill_AllSkills.GetLevel(playerSkillId)
	local maxSkillLevel = Skill_AllSkills.GetLevel(maxSkillId)
	CurrentLabel.text = SafeStringFormat3(LocalString.GetString("[ACF9FF]当前等级[-] %d/%d"), playerSkillLevel, maxSkillLevel)


	CommonDefs.AddOnClickListener(SkillNode, DelegateFactory.Action_GameObject(function (go)
		self:OnSkillClicked(go, playerSkillId)
	end), false)

	if playerSkillLevel < maxSkillLevel then
		UpgradeTypeLabel.color = NGUIText.ParseColor24("FFC300", 0)
		CommonDefs.AddOnClickListener(self.UpgradeWaKuangItem, DelegateFactory.Action_GameObject(function (go)
			self:OnItemClicked(2, 1)
		end), false)
	else
		UpgradeTypeLabel.color = NGUIText.ParseColor24("B2B2B2", 0)
		CommonDefs.AddOnClickListener(self.UpgradeWaKuangItem, DelegateFactory.Action_GameObject(function (go)
			
		end), false)
	end
end

-- 矿大背包
function LuaWaKuangUpgradeWnd:InitUpgradePackageCountItem()
	local UpgradeTypeLabel = self.UpgradePackageCountItem.transform:Find("UpgradeTypeLabel"):GetComponent(typeof(UILabel))
	local CurrentLabel = self.UpgradePackageCountItem.transform:Find("CurrentLabel"):GetComponent(typeof(UILabel))
	CurrentLabel.text = nil

	local setting = WuYi_WaKuangSetting.GetData()
	local currentPackageSize = LuaWuYiMgr.m_PackageSize
	local maxPackgeSize = setting.MaxPackageSize

	CurrentLabel.text = SafeStringFormat3(LocalString.GetString("[ACF9FF]当前数量[-] %d/%d"), currentPackageSize, maxPackgeSize)

	if currentPackageSize < maxPackgeSize then
		UpgradeTypeLabel.color = NGUIText.ParseColor24("FFC300", 0)
		CommonDefs.AddOnClickListener(self.UpgradePackageCountItem, DelegateFactory.Action_GameObject(function (go)
			self:OnItemClicked(3, 3)
		end), false)
	else
		UpgradeTypeLabel.color = NGUIText.ParseColor24("B2B2B2", 0)
		CommonDefs.AddOnClickListener(self.UpgradePackageCountItem, DelegateFactory.Action_GameObject(function (go)
			
		end), false)
	end
	
end

-- 扩容背包
function LuaWaKuangUpgradeWnd:InitUpgradeCapacityItem()
	local UpgradeTypeLabel = self.UpgradeCapacityItem.transform:Find("UpgradeTypeLabel"):GetComponent(typeof(UILabel))
	local CurrentLabel = self.UpgradeCapacityItem.transform:Find("CurrentLabel"):GetComponent(typeof(UILabel))
	CurrentLabel.text = nil

	local setting = WuYi_WaKuangSetting.GetData()
	local currentGridCapacity = LuaWuYiMgr.m_GridCapacity
	local maxGridCapacity = setting.MaxGridCapacity

	CurrentLabel.text = SafeStringFormat3(LocalString.GetString("[ACF9FF]当前容量[-] %d/%d"), currentGridCapacity, maxGridCapacity)

	if currentGridCapacity < maxGridCapacity then
		CommonDefs.AddOnClickListener(self.UpgradeCapacityItem, DelegateFactory.Action_GameObject(function (go)
			self:OnItemClicked(4, 4)
		end), false)
	else
		UpgradeTypeLabel.color = NGUIText.ParseColor24("B2B2B2", 0)
		CommonDefs.AddOnClickListener(self.UpgradeCapacityItem, DelegateFactory.Action_GameObject(function (go)

		end), false)
	end
	
end

function LuaWaKuangUpgradeWnd:SetItemSelected(go, isSelected)
	local selectedSprite = go.transform:Find("Selected").gameObject
	selectedSprite:SetActive(isSelected)
end

function LuaWaKuangUpgradeWnd:OnItemClicked(idx, upgradeType)
	self.SelectedUpgradeType = upgradeType
	for i = 1, #self.ItemList do
		self:SetItemSelected(self.ItemList[i], i == idx)
	end
end

function LuaWaKuangUpgradeWnd:OnOKButtonClicked(go)
	if self.SelectedUpgradeType <= 0 or self.SelectedUpgradeType > 4 then
		return
	end
	Gac2Gas.RequestUpgradeWaKuangSkillAndPackage(self.SelectedUpgradeType)
end

function LuaWaKuangUpgradeWnd:OnSkillClicked(go, skillId)
	CSkillInfoMgr.ShowSkillInfoWnd(skillId, go.transform.position, EnumSkillInfoContext.Default, true, 0, 0, nil)
end


function LuaWaKuangUpgradeWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateWaKuangUpgrade", self, "Refresh")
end

function LuaWaKuangUpgradeWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateWaKuangUpgrade", self, "Refresh")
end

return LuaWaKuangUpgradeWnd