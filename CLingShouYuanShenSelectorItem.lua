-- Auto Generated!!
local CItemMgr = import "L10.Game.CItemMgr"
local CLingShouYuanShenSelectorItem = import "L10.UI.CLingShouYuanShenSelectorItem"
CLingShouYuanShenSelectorItem.m_Init_CS2LuaHook = function (this, info) 
    this.itemInfo = info

    local item = CItemMgr.Inst:GetById(this.itemInfo.itemId)

    this.icon:LoadMaterial(item.Icon)

    this.amountLabel.text = (item.Item.Count > 1) and tostring(item.Item.Count) or nil
end
