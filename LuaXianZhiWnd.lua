require("common/common_include")

local QnModelPreviewer = import "L10.UI.QnModelPreviewer"
local CUITexture = import "L10.UI.CUITexture"
local UISlider = import "UISlider"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local MessageMgr = import "L10.Game.MessageMgr"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"

LuaXianZhiWnd = class()

RegistChildComponent(LuaXianZhiWnd, "ModelPreviewer", QnModelPreviewer)

RegistChildComponent(LuaXianZhiWnd, "XianZhiTitleLabel", UILabel)
RegistChildComponent(LuaXianZhiWnd, "XianZhiTypeIcon", CUITexture)
RegistChildComponent(LuaXianZhiWnd, "XianZhiDescLabel", UILabel)
RegistChildComponent(LuaXianZhiWnd, "XianZhiTermLabel", UILabel)
RegistChildComponent(LuaXianZhiWnd, "XianZhiDescBtn", GameObject)

RegistChildComponent(LuaXianZhiWnd, "XianZhiSkillIcon", CUITexture)
RegistChildComponent(LuaXianZhiWnd, "SkillStatus", GameObject)
RegistChildComponent(LuaXianZhiWnd, "SkillTimeLabel", UILabel)
RegistChildComponent(LuaXianZhiWnd, "SkillStatusLabel", UILabel)
RegistChildComponent(LuaXianZhiWnd, "SkillCD", UISprite)

RegistChildComponent(LuaXianZhiWnd, "XianZhiSkillProgressBar", UISlider)
RegistChildComponent(LuaXianZhiWnd, "XianZhiSkillExpLabel", UILabel)
RegistChildComponent(LuaXianZhiWnd, "XianZhiSkillUpgradeBtn", GameObject)

RegistChildComponent(LuaXianZhiWnd, "PromoteBtn", GameObject)
RegistChildComponent(LuaXianZhiWnd, "ExtendBtn", GameObject)
RegistChildComponent(LuaXianZhiWnd, "ShareBtn", GameObject)
RegistChildComponent(LuaXianZhiWnd, "ExcludeRoot", GameObject)
RegistChildComponent(LuaXianZhiWnd, "SkillName", UILabel)

RegistClassMember(LuaXianZhiWnd, "SkillStatusRemainTime")
RegistClassMember(LuaXianZhiWnd, "SkillStatusTimeTick")
RegistClassMember(LuaXianZhiWnd, "UpdateCooldownFunc")

function LuaXianZhiWnd:Init()
	if not CClientMainPlayer.Inst then
		CUIManager.CloseUI(CLuaUIResources.XianZhiWnd)
		return 
	end

	self.SkillStatusRemainTime = 0
	self.ModelPreviewer:PreviewMainPlayer(uint.MaxValue, 0, 0)
	self:UpdateXianZhiInfo()

	CommonDefs.AddOnClickListener(self.XianZhiDescBtn, DelegateFactory.Action_GameObject(function (go)
		self:OnXianZhiDescBtnClicked(go)
	end), false)

	CommonDefs.AddOnClickListener(self.XianZhiSkillUpgradeBtn, DelegateFactory.Action_GameObject(function (go)
		self:OnXianZhiSkillUpgradeBtnClicked(go)
	end), false)

	CommonDefs.AddOnClickListener(self.PromoteBtn, DelegateFactory.Action_GameObject(function (go)
		self:OnPromoteBtnClicked(go)
	end), false)

	CommonDefs.AddOnClickListener(self.ExtendBtn, DelegateFactory.Action_GameObject(function (go)
		self:OnExtendBtnClicked(go)
	end), false)

	CommonDefs.AddOnClickListener(self.ShareBtn, DelegateFactory.Action_GameObject(function (go)
		self:OnShareBtnClicked(go)
	end), false)

end

-- 更新仙职相关的信息
function LuaXianZhiWnd:UpdateXianZhiInfo()
	
	local status = CLuaXianzhiMgr.GetXianZhiStatus()
	if status == EnumXianZhiStatus.NotFengXian then
		return
	end

	local xianzhiName = CLuaXianzhiMgr.GetXianZhiFullName()
	self.XianZhiTitleLabel.text = xianzhiName

	local xianZhiData =  CClientMainPlayer.Inst.PlayProp.XianZhiData
	local setting = XianZhi_Setting.GetData()

	local xianzhiTitleId = CLuaXianzhiMgr.GetValidTitleId()
	local title = XianZhi_Title.GetData(xianzhiTitleId)

	if not title then
		CUIManager.CloseUI(CLuaUIResources.XianZhiWnd)
		return
	end

	local xianZhiTypeId = title.TitleType
	local xianZhiType = XianZhi_TitleType.GetData(xianZhiTypeId)
	local xianZhiSkillCls = title.SkillCls
	local maxLevel = setting.LevelUpperLimit[status-1]

	-- 仙职type图标
	self.XianZhiTypeIcon:LoadSkillIcon(StringTrim(xianZhiType.Icon))
	CommonDefs.AddOnClickListener(self.XianZhiTypeIcon.gameObject, DelegateFactory.Action_GameObject(function (go)
		self:OnXianZhiTypeIconClicked(go, xianZhiTypeId)
	end), false)


	-- 仙职技能ID
	local xianZhiSkillId = CLuaXianzhiMgr.GetXianZhiSKillId()
	local xianZhiSkill = Skill_AllSkills.GetData(xianZhiSkillId)
	if xianZhiSkill then
		self.XianZhiSkillIcon:LoadSkillIcon(xianZhiSkill.SkillIcon)
		self.SkillName.text = xianZhiSkill.Name
	end

	self:UpdateCD()

	local skillGo = self.XianZhiSkillIcon.transform:Find("Skill").gameObject
	CommonDefs.AddOnClickListener(skillGo, DelegateFactory.Action_GameObject(function (go)
		self:OnSkillIconClicked(go)
	end), false)

	-- 中文长度
	if string.len(title.Des) <= 40 then
		self.XianZhiDescLabel.text = title.Des
	else
		self.XianZhiDescLabel.text = SafeStringFormat3("%s...", string.sub(title.Des, 1, 36))
	end
	self:UpdateXianZhiSkillLevel()
	self:UpdateRemainXianZhiTime()
end


-- 更新仙职技能的CD（虽然技能CD很长看不出来）
function LuaXianZhiWnd:UpdateCD()
	local skillId = CLuaXianzhiMgr.GetXianZhiSkillByDelta()
	local remain = CClientMainPlayer.Inst.CooldownProp:GetServerRemainTime(skillId)
	local total = CClientMainPlayer.Inst.CooldownProp:GetServerTotalTime(skillId);
	if remain > 0 then
		self.SkillCD.fillAmount = remain / total
	else
		self.SkillCD.fillAmount = 0
	end
end

-- 更新仙职技能等级信息
function LuaXianZhiWnd:UpdateXianZhiSkillLevel()
	-- 等级
	local xianZhiData =  CClientMainPlayer.Inst.PlayProp.XianZhiData
	local setting = XianZhi_Setting.GetData()
	local status = CLuaXianzhiMgr.GetXianZhiStatus()
	local maxLevel = setting.LevelUpperLimit[status-1]

	if not maxLevel then
		CUIManager.CloseUI(CLuaUIResources.XianZhiWnd)
		return
	end
	self.XianZhiSkillExpLabel.text = SafeStringFormat3(LocalString.GetString("%s级/%s级"), tostring(xianZhiData.XianZhiLevel), tostring(maxLevel))
	self.XianZhiSkillProgressBar.value = xianZhiData.XianZhiLevel / maxLevel

	self.SkillStatus:SetActive(true)
	if xianZhiData.OperateType == EnumXianZhiOperateType.eNone then
		self.SkillStatus:SetActive(false)
		self.SkillStatusLabel.text = nil
		self.SkillTimeLabel.text = nil

	else
		local Add = self.SkillStatus.transform:Find("Add").gameObject
		local Minus = self.SkillStatus.transform:Find("Minus").gameObject
		local Sealed = self.SkillStatus.transform:Find("Sealed").gameObject
		Add:SetActive(xianZhiData.OperateType == EnumXianZhiOperateType.ePromote)
		Minus:SetActive(xianZhiData.OperateType == EnumXianZhiOperateType.eWeaken)
		Sealed:SetActive(xianZhiData.OperateType == EnumXianZhiOperateType.eForbid)

		self.SkillStatusLabel.text = SafeStringFormat3(LocalString.GetString("被%s%s"), xianZhiData.OperatePlayerName, self:GetOperationName(xianZhiData.OperateType))

		self.SkillStatusRemainTime = xianZhiData.OperateExpiredTime - CServerTimeMgr.Inst.timeStamp
		self.SkillTimeLabel.text = self:FormatTime(self.SkillStatusRemainTime)
		self:DestroyTick()
		self.SkillStatusTimeTick = RegisterTick(function ()
			self.SkillStatusRemainTime = self.SkillStatusRemainTime - 1
			if self.SkillStatusRemainTime > 0 then
				self.SkillTimeLabel.text = self:FormatTime(self.SkillStatusRemainTime)
			else
				self.SkillTimeLabel.text = nil
				self:DestroyTick()
				self:UpdateXianZhiSkillLevel()
			end
		end, 1000)
	end
end

function LuaXianZhiWnd:FormatTime(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaXianZhiWnd:GetOperationName(operationtype)
	local skillId = CLuaXianzhiMgr.GetXianZhiSkillByDelta()
	local level = Skill_AllSkills.GetLevel(skillId)
    if operationtype == EnumXianZhiOperateType.ePromote then
        return SafeStringFormat3(LocalString.GetString("提升至%s级"), tostring(level))
    elseif operationtype == EnumXianZhiOperateType.eWeaken then
        return SafeStringFormat3(LocalString.GetString("下降至%s级"), tostring(level))
     elseif operationtype == EnumXianZhiOperateType.eForbid then
        return LocalString.GetString("封印无法使用")
    end
    return ""
end

function LuaXianZhiWnd:OnXianZhiTypeIconClicked(go, xianZhiTypeId)
	CLuaXianzhiMgr.OpenXianZhiTypeDescWnd(xianZhiTypeId)
end

function LuaXianZhiWnd:OnXianZhiDescBtnClicked(go)
	CUIManager.ShowUI(CLuaUIResources.XianZhiDetailDescWnd)
end

-- 仙职技能升级
function LuaXianZhiWnd:OnXianZhiSkillUpgradeBtnClicked(go)
	local status = CLuaXianzhiMgr.GetXianZhiStatus()
	local maxLevel = CLuaXianzhiMgr.GetXianZhiSkillMaxLevel(status)
	local xianZhiData =  CClientMainPlayer.Inst.PlayProp.XianZhiData

	if xianZhiData.XianZhiLevel >= 10 then
		MessageMgr.Inst:ShowMessage("XianZhi_Skill_Upgrade_Max_Level", {})
		return
	else
		CUIManager.ShowUI(CLuaUIResources.XianZhiSkillUpgradeWnd)
	end
end

-- 仙职晋升
function LuaXianZhiWnd:OnPromoteBtnClicked(go)
	local status = CLuaXianzhiMgr.GetXianZhiStatus()
	if status == EnumXianZhiStatus.SanJie then
		MessageMgr.Inst:ShowMessage("XianZhi_Promotion_Max_Level", {})
		return
	else
		CLuaXianzhiMgr.OpenXianZhiPromotion()
	end
end

-- 延长任期
function LuaXianZhiWnd:OnExtendBtnClicked(go)
	CLuaXianzhiMgr.OpenXianZhiExtend()
end

function LuaXianZhiWnd:OnShareBtnClicked(go)
	-- 截图 分享
	CUICommonDef.CaptureScreen(
        "screenshot",
        true,
        false,
        self.ExcludeRoot,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            end
        ),
        false
    )
end

function LuaXianZhiWnd:OnSkillIconClicked(go)
	local xianZhiSkillId = CLuaXianzhiMgr.GetXianZhiSKillId()
	CSkillInfoMgr.ShowSkillInfoWnd(xianZhiSkillId, true, 0, 0, nil)
end

function LuaXianZhiWnd:OnLevelUpXianZhiSuccess()
	self:UpdateXianZhiSkillLevel()
end

-- 更新仙职人气剩余的时间
function LuaXianZhiWnd:UpdateRemainXianZhiTime()
	-- 任期时间
	if CLuaXianzhiMgr.m_RemainXianZhiTime == 0 then
		return
	end

	local now = CServerTimeMgr.Inst.timeStamp
	local daysRemain = math.floor((CLuaXianzhiMgr.m_RemainXianZhiTime - now) / 3600 / 24)
	if daysRemain < 0 then
		daysRemain = 0
	end
	self.XianZhiTermLabel.text = SafeStringFormat3(LocalString.GetString("[ffffff]剩余任期[-]    [FFC300]%d天[-]"), daysRemain)
end

function LuaXianZhiWnd:OnAllocateXianZhiSuccess()
	self:Init()
end

function LuaXianZhiWnd:DestroyTick()
	if self.SkillStatusTimeTick then
		UnRegisterTick(self.SkillStatusTimeTick)
		self.SkillStatusTimeTick = nil
	end
end

function LuaXianZhiWnd:UpdateCooldown()
	self:UpdateCD()
end

function LuaXianZhiWnd:OnEnable()
	if not self.UpdateCooldownFunc then
		self.UpdateCooldownFunc = DelegateFactory.Action(function ()
			self:UpdateCooldown()
		end)
	end
	EventManager.AddListener(EnumEventType.UpdateCooldown, self.UpdateCooldownFunc)
	g_ScriptEvent:AddListener("LevelUpXianZhiSuccess", self, "OnLevelUpXianZhiSuccess")
	g_ScriptEvent:AddListener("UpdateRemainXianZhiTime", self, "UpdateRemainXianZhiTime")
	g_ScriptEvent:AddListener("AllocateXianZhiSuccess", self, "OnAllocateXianZhiSuccess")
end

function LuaXianZhiWnd:OnDisable()
	g_ScriptEvent:RemoveListener("LevelUpXianZhiSuccess", self, "OnLevelUpXianZhiSuccess")
	g_ScriptEvent:RemoveListener("UpdateRemainXianZhiTime", self, "UpdateRemainXianZhiTime")
	g_ScriptEvent:RemoveListener("AllocateXianZhiSuccess", self, "OnAllocateXianZhiSuccess")
	EventManager.RemoveListener(EnumEventType.UpdateCooldown, self.UpdateCooldownFunc)
	self:DestroyTick()
end

return LuaXianZhiWnd
