require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local CCarnivalLotteryMgr=import "L10.Game.CCarnivalLotteryMgr"
local CPaintTexture=import "L10.UI.CPaintTexture"
local Item_Item=import "L10.Game.Item_Item"
local MessageMgr=import "L10.Game.MessageMgr"
local IdPartition=import "L10.Game.IdPartition"
local EquipmentTemplate_Equip=import "L10.Game.EquipmentTemplate_Equip"
local CItem=import "L10.Game.CItem"
local NGUIText=import "NGUIText"
local CEquipment=import "L10.Game.CEquipment"


CLuaCarnivalLotteryWnd=class()
RegistClassMember(CLuaCarnivalLotteryWnd,"m_CloseButton")
RegistClassMember(CLuaCarnivalLotteryWnd,"m_ItemTemplate")
RegistClassMember(CLuaCarnivalLotteryWnd,"m_GridNode")
RegistClassMember(CLuaCarnivalLotteryWnd,"m_Grid")
RegistClassMember(CLuaCarnivalLotteryWnd,"m_PaintTexture")
RegistClassMember(CLuaCarnivalLotteryWnd,"m_RewardTable")
-- RegistClassMember(CLuaCarnivalLotteryWnd,"m_LastBattleDataView")

function CLuaCarnivalLotteryWnd:Init()
    local bg1=LuaGameObject.GetChildNoGC(self.transform,"Bg1").gameObject;
    local bg10=LuaGameObject.GetChildNoGC(self.transform,"Bg10").gameObject;
    if CCarnivalLotteryMgr.Inst.RequestTimes==1 then
        bg1:SetActive(true)
        bg10:SetActive(false)
    elseif CCarnivalLotteryMgr.Inst.RequestTimes==10 then
        bg1:SetActive(false)
        bg10:SetActive(true)
    end

    self.m_RewardTable={}
    local g = LuaGameObject.GetChildNoGC(self.transform,"CloseButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI("CarnivalLotteryWnd")
        --未领取的 自动领取
        if self.m_RewardTable then
            for i,v in ipairs(self.m_RewardTable) do
                if not v.Get then
                    self:DisplayObtainItemMessage(v[1],v[2])
                end
            end
        end
    end)

    self.m_ItemTemplate=LuaGameObject.GetChildNoGC(self.transform,"ItemTemplate").gameObject
    self.m_ItemTemplate:SetActive(false)
    self.m_PaintTexture=LuaGameObject.GetChildNoGC(self.transform,"PaintTexture"):GetComponent(typeof(CPaintTexture))

    self.m_PaintTexture.OnStartPaint = DelegateFactory.Action(function()
        local mgr=CCarnivalLotteryMgr.Inst
        mgr:RequestLottery(mgr.RequestTimes)
        local label=LuaGameObject.GetChildNoGC(self.m_PaintTexture.transform,"TipLabel").gameObject
        if label then
            label:SetActive(false)
        end
    end)
    self.m_PaintTexture.OnPaintOver = DelegateFactory.Action(function()
        local sprite=LuaGameObject.GetChildNoGC(self.m_PaintTexture.transform,"Sprite").gameObject
        if sprite then
            sprite:SetActive(false)
        end
    end)
    self.m_PaintTexture.OnTriggerItem=DelegateFactory.Action_int(function(index)
        if self.m_RewardTable then
            local v=self.m_RewardTable[index+1]
            v.Get=true
            self:DisplayObtainItemMessage(v[1],v[2])
        end
    end)

    self.m_GridNode=LuaGameObject.GetChildNoGC(self.transform,"Contents").gameObject
    self.m_Grid=LuaGameObject.GetChildNoGC(self.transform,"Contents").grid
end

function CLuaCarnivalLotteryWnd:OnEnable( )
    g_ScriptEvent:AddListener("CarnivalLotteryResult", self, "OnCarnivalLotteryResult")

end
function CLuaCarnivalLotteryWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("CarnivalLotteryResult", self, "OnCarnivalLotteryResult")
end

function CLuaCarnivalLotteryWnd:OnCarnivalLotteryResult( args )
    local array=args[0]
    local count=array.Length/3

    local tempT={}
    for i=0,count-1 do
        table.insert(tempT,{array[i*3+0],array[i*3+1],array[i*3+2]})
    end
    local tempG={}
    for i=1,#tempT do
        local go=NGUITools.AddChild(self.m_GridNode,self.m_ItemTemplate)
        local icon=LuaGameObject.GetChildNoGC(go.transform,"Texture").cTexture
        local templateId=tempT[i][1]
        local count=tempT[i][2]
        local bind=tempT[i][3]

        local bindMark=LuaGameObject.GetChildNoGC(go.transform,"BindMark").gameObject
        if bind>0 then
            bindMark:SetActive(true)
        else
            bindMark:SetActive(false)
        end
        local item = Item_Item.GetData(templateId)
        if item then
            icon:LoadMaterial(item.Icon)
        end
        local countLabel=LuaGameObject.GetChildNoGC(go.transform,"Label").label
        countLabel.text=tostring(count)
        go:SetActive(true)
        -- self.m_PaintTexture:RegisterTrigger(go)
        table.insert(tempG,go)
    end
    self.m_RewardTable=tempT
    self.m_Grid:Reposition()
    for i,v in ipairs(tempG) do
        self.m_PaintTexture:RegisterTrigger(v)
    end
end

function CLuaCarnivalLotteryWnd:DisplayObtainItemMessage(selectedTemplateId, selectedCount)
    if IdPartition.IdIsItem(selectedTemplateId) then
        local item = Item_Item.GetData(selectedTemplateId)
        if item then
            if selectedCount > 1 then
                MessageMgr.Inst:ShowMessage("OBTAIN_COMMON_ITEM", {CItem.GetColorString(selectedTemplateId), item.Name .. "x" .. tostring(selectedCount)})
            else
                MessageMgr.Inst:ShowMessage("OBTAIN_COMMON_ITEM", {CItem.GetColorString(selectedTemplateId), item.Name})
            end
        end
    else
        local equip = EquipmentTemplate_Equip.GetData(selectedTemplateId)
        if equip then
            MessageMgr.Inst:ShowMessage("OBTAIN_COMMON_ITEM", {NGUIText.EncodeColor24(CEquipment.GetColor(selectedTemplateId)), equip.Name})
        end
    end
end


return CLuaCarnivalLotteryWnd
