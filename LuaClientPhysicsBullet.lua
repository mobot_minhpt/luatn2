local CPhysicsTrigger=import "L10.Engine.CPhysicsTrigger"
local CBox2dObject = import "L10.Engine.CBox2dObject"
local Rigidbody2D=import "UnityEngine.Rigidbody2D"
local RigidbodyType2D=import "UnityEngine.RigidbodyType2D"
local CPhysicsWorld=import "L10.Engine.CPhysicsWorld"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CircleCollider2D = import "UnityEngine.CircleCollider2D"
local CResourceMgr=import "L10.Engine.CResourceMgr"
local CPhysicsMgr=import "L10.Engine.CPhysicsMgr"
local RigidbodySleepMode2D=import "UnityEngine.RigidbodySleepMode2D"
local LayerDefine=import "L10.Engine.LayerDefine"
local CRenderObject = import "L10.Engine.CRenderObject"
local CClientObjectRoot=import "L10.Game.CClientObjectRoot"
local CPhysicsBulletTriggerResource = import "L10.Engine.CPhysicsBulletTriggerResource"
local CPhysicsBulletROResource=import "L10.Engine.CPhysicsBulletROResource"
local CPhysicsBulletResource=import "L10.Engine.CPhysicsBulletResource"

LuaClientPhysicsBullet = class()
-- RegistClassMember(LuaClientPhysicsBullet, "m_Core")
RegistClassMember(LuaClientPhysicsBullet, "m_PhysicsObject")
RegistClassMember(LuaClientPhysicsBullet, "m_BulletId")
RegistClassMember(LuaClientPhysicsBullet, "m_TemplateId")

RegistClassMember(LuaClientPhysicsBullet, "m_DeadFrame")
RegistClassMember(LuaClientPhysicsBullet, "m_YPos")
RegistClassMember(LuaClientPhysicsBullet, "m_InitYPos")
RegistClassMember(LuaClientPhysicsBullet, "m_YSpeed")
RegistClassMember(LuaClientPhysicsBullet, "m_Gravity")
RegistClassMember(LuaClientPhysicsBullet, "m_InitPositionY")

RegistClassMember(LuaClientPhysicsBullet, "m_Snapshot")
RegistClassMember(LuaClientPhysicsBullet, "m_Collided")--有没有撞过
RegistClassMember(LuaClientPhysicsBullet, "m_CollidePos")

RegistClassMember(LuaClientPhysicsBullet, "m_Force")

LuaClientPhysicsBullet.s_SnapshotPool = {}
LuaClientPhysicsBullet.s_SnapshotSize = 50--最多保存50个快照

LuaClientPhysicsBullet.s_BulletCacheTeamplate = {}
LuaClientPhysicsBullet.s_BulletCacheCount = 20
LuaClientPhysicsBullet.s_BulletCacheLifeTime = 8000--8秒


function LuaClientPhysicsBullet.CreateTrigger(bulletId,ownerId)
    if not CPhysicsBulletTriggerResource.Inst.Resource then
        local go = GameObject("bullet trigger")
        go.layer = 0
        go.transform.parent = CPhysicsMgr.Inst.PhysicsObjectRoot

        local physicsObject = go:AddComponent(typeof(CPhysicsTrigger))

        go:SetActive(false)
        CPhysicsBulletTriggerResource.Inst.Resource = go
    end
    local item = CResourceMgr.Inst:InstantiateUseCache(CPhysicsBulletTriggerResource.Inst, true, 1)
    item.name = tostring(bulletId)
    item.transform.parent = CPhysicsMgr.Inst.PhysicsObjectRoot

    local physicsObject = item:GetComponent(typeof(CPhysicsTrigger))
    physicsObject.ownerId = ownerId

    return physicsObject
end

function LuaClientPhysicsBullet.CreateRenderObject(name)
    if not CPhysicsBulletROResource.Inst.Resource then
        local parent = CClientObjectRoot.Inst:GetParent("Item")
        local ro = CRenderObject.CreateRenderObject(parent, "BulletRenderObject", false)
        ro.Layer = LayerDefine.Item
        ro.gameObject:SetActive(false)
        CPhysicsBulletROResource.Inst.Resource = ro.gameObject
    end
    local item = CResourceMgr.Inst:InstantiateUseCache(CPhysicsBulletROResource.Inst, true, 1)
    item.name = name
    local parent = CClientObjectRoot.Inst:GetParent("Item")
    local ro = item:GetComponent(typeof(CRenderObject))
    ro.Parent = parent

    return ro
end

function LuaClientPhysicsBullet.CreatePhysicsBullet(physicsObject,shapeId)
    if not LuaClientPhysicsBullet.s_BulletCacheTeamplate[shapeId] then
        local box2dGo = GameObject("BulletCache");
    -- box2dGo.layer = this.gameObject.layer
        box2dGo.transform.parent = CPhysicsMgr.Inst.Box2dRoot
        -- box2dGo.transform.localPosition = Vector3(this.transform.position.x, this.transform.position.z, 0);
        box2dGo.transform.localScale = Vector3(1, 1, 1)
        -- box2dGo.transform.rotation = Quaternion.identity
        -- box2dGameObject = box2dGo;

        local box2dRb = box2dGo:AddComponent(typeof(Rigidbody2D))
        box2dRb.gravityScale = 0
        box2dRb.drag = 0
        box2dRb.angularDrag = 0
        box2dRb.mass = 1
        box2dRb.sleepMode = RigidbodySleepMode2D.NeverSleep

        local box2dObject = box2dGo:AddComponent(typeof(CBox2dObject))
        box2dObject.type="bullet"

        SetPhysicsShape(box2dRb,shapeId,true)
        local collider = box2dGo:GetComponent(typeof(CircleCollider2D))
        collider.isTrigger = true

        local load = CPhysicsBulletResource("PhysicsBullet_"..tostring(shapeId))
        load.Resource = box2dGo
        box2dGo:SetActive(false)

        LuaClientPhysicsBullet.s_BulletCacheTeamplate[shapeId] = load
    end

    local template = LuaClientPhysicsBullet.s_BulletCacheTeamplate[shapeId]
    -- print("template",template)
    local item = CResourceMgr.Inst:InstantiateUseCache(template, true, 1)
    item.transform.parent = CPhysicsMgr.Inst.Box2dRoot

    item.name = tostring(physicsObject:GetName())
    item.transform.localPosition = Vector3(physicsObject.transform.position.x, physicsObject.transform.position.z, 0)
    physicsObject.box2dGameObject = item
    physicsObject.box2dRb = physicsObject.box2dGameObject:GetComponent(typeof(Rigidbody2D))
    physicsObject.box2dRb.rotation = 90 - physicsObject.transform.eulerAngles.y

    return item
end




function LuaClientPhysicsBullet:Ctor(bulletId,templateId,physicsObject)
    self.m_BulletId = bulletId
    self.m_TemplateId = templateId
    self.m_PhysicsObject = physicsObject
    self.m_Snapshot = {}
    self.m_Collided = false
end

--table:templateId,position,vx,vy,deadFrame
function LuaClientPhysicsBullet:Init(table)
    self.m_Force = table.force
    -- local shapeId = Physics_Trigger.GetData(table.templateId).shape
    self.m_InitPositionY = table.positionY

    -- local go = self.m_PhysicsObject.box2dGameObject
    local rb = self.m_PhysicsObject.box2dRb
    -- SetPhysicsShape(rb,shapeId,true)

    local collider = rb.gameObject:GetComponent(typeof(CircleCollider2D))
    -- collider.isTrigger = true
    -- rb.velocity = Vector2(table.vx,table.vy)
    LuaRigidbody2DAccess.SetVelocity(self.m_PhysicsObject.box2dRb,table.vx,table.vy)
    local delay = (collider.radius/rb.velocity.magnitude)

    --稍微晚一点出现，否则船在边界的时候，朝没有障碍的方向开炮，一出现就爆炸了
    local box2dObject = rb.gameObject:GetComponent(typeof(CBox2dObject))
    box2dObject.force = table.force
    box2dObject.onTriggerEnter2D = DelegateFactory.Action_Collider2D(function(col)
        if self.m_Collided then return end
        --碰到场景障碍，直接销毁
        local colrb = col:GetComponent(typeof(Rigidbody2D))
        if colrb and colrb.bodyType==RigidbodyType2D.Static then
            self.m_Collided = true
            --子弹有一定的体积，边缘碰到的时候，还需要等一会才真正击中
            local pos = self.m_PhysicsObject.transform.position
            --预测一下击中的位置
            self.m_CollidePos = Vector3(pos.x+ table.vx*delay,pos.y,pos.z+ table.vy*delay)
            self:SetDead()
        elseif colrb and colrb.bodyType==RigidbodyType2D.Dynamic then
            --是不是敌船
            local id = tonumber(colrb.name)
            if g_PhysicsObjectHandlers[id] then
                if g_PhysicsObjectHandlers[id].m_Force~=self.m_Force then
                    self.m_Collided = true
                    self.m_PhysicsObject.targetPhysicsObjectId = id
                    self:SetDead()
                end
            end
        end
    end)

    self.m_DeadFrame = table.deadFrame

    self.m_Gravity = 0.1
    --20帧1秒钟
    local t = (self.m_DeadFrame - self.m_PhysicsObject:GetFrameId())*0.05
    local y = LuaTransformAccess.GetPositionY(self.m_PhysicsObject.transform)
    local height = self.m_InitPositionY-y--self.m_PhysicsObject.transform.position.y-- 0.2--炮口距海面的高度
    self.m_InitYPos = y
    self.m_YPos = height
    self.m_YSpeed = (self.m_Gravity*t*t+2*height)/(2*t)
end

function LuaClientPhysicsBullet:OnDestroy()
    for k,v in pairs(self.m_Snapshot) do
        table.insert(LuaClientPhysicsBullet.s_SnapshotPool,v)
        self.m_Snapshot[k] = nil
    end
end
--物理帧的step
function LuaClientPhysicsBullet:Step()
    if self.m_PhysicsObject.isDead then return end
    local frameId = self.m_PhysicsObject:GetFrameId()
    if self.m_DeadFrame <= frameId then
        self:SetDead()
        return
    end
end
--每一帧更新位置
function LuaClientPhysicsBullet:Update()
    if not self.m_PhysicsObject.RO then return end
    local posx,posy,posz = LuaTransformAccess.GetPosition(self.m_PhysicsObject.transform)--物理对象的坐标
    local vposx,vposy,vposz = LuaTransformAccess.GetPosition(self.m_PhysicsObject.RO.transform)

    local y = self.m_InitYPos
    local delta = Time.deltaTime

    self.m_YSpeed = self.m_YSpeed -self.m_Gravity*delta
    self.m_YPos = self.m_YPos+self.m_YSpeed*delta

    y = y + self.m_YPos
    local x = math.lerp(vposx,posx,0.1)
    local z = math.lerp(vposz,posz,0.1)
    LuaTransformAccess.SetPosition(self.m_PhysicsObject.RO.transform,x,y,z)

end
function LuaClientPhysicsBullet:Snapshot()
    if self.m_PhysicsObject.isDead then return end

    -- local box2dRb = self.m_PhysicsObject.box2dRb
    local frameId = self.m_PhysicsObject:GetFrameId()

    local shot = table.remove(LuaClientPhysicsBullet.s_SnapshotPool)
    if not shot then shot = {} end

    shot[1],shot[2],shot[3],shot[4],shot[5],shot[6] = self.m_PhysicsObject:GetSnapshotParams()

    local size = LuaClientPhysicsBullet.s_SnapshotSize
    self.m_Snapshot[frameId] = shot
    for k,v in pairs(self.m_Snapshot) do
        if k<frameId-size then
            table.insert(LuaClientPhysicsBullet.s_SnapshotPool,v)

            self.m_Snapshot[k] = nil
        end
    end
end
function LuaClientPhysicsBullet:RollbackTo(frameId)
    if self.m_PhysicsObject.isDead then return end
    for k,v in pairs(self.m_Snapshot) do
        if k>frameId then
            table.insert(LuaClientPhysicsBullet.s_SnapshotPool,v)

            self.m_Snapshot[k] = nil
        end
    end
    local shot = self.m_Snapshot[frameId]
    if shot then
        self.m_PhysicsObject:SetSnapshotParams(shot[1],shot[2],shot[3],shot[4],shot[5],shot[6])
    end
end


function LuaClientPhysicsBullet:SetDead()
    self.m_PhysicsObject.isDead = true
end
--因为是插值跟随物理对象，所以可能物理对象碰了，但是渲染对象还没有碰到
function LuaClientPhysicsBullet:ShowHitEffect()
    if not CClientMainPlayer.Inst then return end
    local fps = 1/Time.unscaledDeltaTime
    if fps<15 then--小于15帧
        --不显示击中特效
        return
    end
    local pos = nil
    if self.m_PhysicsObject.targetPhysicsObjectId~=0 then
        pos = self.m_PhysicsObject.RO.transform.position
    else
        pos = self.m_CollidePos and self.m_CollidePos or self.m_PhysicsObject.RO.transform.position
    end

    local myPOId = CClientMainPlayer.Inst.AppearanceProp.DriverId
    local po = g_PhysicsObjectHandlers[myPOId]
    if po then
        local myPos = po.m_PhysicsObject.RO.Position
        local dis = Vector3.Distance(myPos,pos)
        if fps<20 then
            --小于20帧
            local dis = Vector3.Distance(myPos,pos)
            if dis>20 then
                --不显示击中特效
                return
            end
        elseif fps<30 then
            --小于30帧
            local dis = Vector3.Distance(myPos,pos)
            if dis>30 then
                return
            end
        end
    end


    -- print("ShowHitEffect",self.m_IsHitWater)
    if self.m_PhysicsObject.targetPhysicsObjectId~=0 then
        local triggerData = Physics_Trigger.GetData(self.m_TemplateId)
        if triggerData and triggerData.TargetFX then
            for i=1,triggerData.TargetFX.Length do
                local fxId = triggerData.TargetFX[i-1]
                local fx = CEffectMgr.Inst:AddWorldPositionFX(fxId, pos, 0,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
                if fx then
                    fx.CacheCount = LuaClientPhysicsBullet.s_BulletCacheCount
                    fx.LifeTime = LuaClientPhysicsBullet.s_BulletCacheLifeTime
                end
            end
        end
    else--if self.m_PhysicsObject.isHitWater then
        --水面受击特效
        local baseHeight = CPhysicsWorld.Inst:GetBaseHeight()
        pos = Vector3(pos.x,baseHeight-0.1,pos.z)
        local triggerData = Physics_Trigger.GetData(self.m_TemplateId)
        if triggerData and triggerData.WaterFX then
            for i=1,triggerData.WaterFX.Length do
                local fxId = triggerData.WaterFX[i-1]
                local fx = CEffectMgr.Inst:AddWorldPositionFX(fxId, pos, math.random(0,360),0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
                if fx then
                    fx.CacheCount = LuaClientPhysicsBullet.s_BulletCacheCount
                    fx.LifeTime = LuaClientPhysicsBullet.s_BulletCacheLifeTime
                end
            end
        end
    end
end