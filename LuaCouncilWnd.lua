LuaCouncilWnd = class()

RegistClassMember(LuaCouncilWnd, "m_CloseWndTick")

function LuaCouncilWnd:OnEnable()
    self.m_CloseWndTick = RegisterTickOnce(function ()
        CUIManager.CloseUI(CLuaUIResources.CouncilWnd)
    end, 5000)
end

function LuaCouncilWnd:OnDisable()
    if self.m_CloseWndTick then
        UnRegisterTick(self.m_CloseWndTick)
        self.m_CloseWndTick = nil
    end
    Gac2Gas.FinishEventTask(CLuaTaskMgr.m_CouncilWnd_TaskId, "CouncilWnd")
end