-- Auto Generated!!
local Boolean = import "System.Boolean"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLinyuShopWnd = import "L10.UI.CLinyuShopWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CPayMgr = import "L10.Game.CPayMgr"
local CShopMallGoodsItem = import "L10.UI.CShopMallGoodsItem"
local CShopMallGoodsItem2 = import "L10.UI.CShopMallGoodsItem2"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local Mall_Setting = import "L10.Game.Mall_Setting"
local MessageWndManager = import "L10.UI.MessageWndManager"
local QnButton = import "L10.UI.QnButton"
local Random = import "UnityEngine.Random"
local String = import "System.String"
local System = import "System"
local UInt32 = import "System.UInt32"
local ShopMallTemlate = import "L10.UI.ShopMallTemlate"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local CultureInfo = import "System.Globalization.CultureInfo"
local CItemMgr = import "L10.Game.CItemMgr"

CLinyuShopWnd.m_OnEnable_CS2LuaHook = function (this)
    CShopMallMgr.OnLingyuLimitUpdated = CommonDefs.CombineListner_Action_bool(CShopMallMgr.OnLingyuLimitUpdated, MakeDelegateFromCSFunction(this.UpdateView, MakeGenericClass(Action1, Boolean), this), true)
    CShopMallMgr.OnLingyuMallAutoShangJiaUpdated = CommonDefs.CombineListner_Action_bool(CShopMallMgr.OnLingyuMallAutoShangJiaUpdated, MakeDelegateFromCSFunction(this.UpdateView2, MakeGenericClass(Action1, Boolean), this), true)
    this.m_Table.OnSelectAtRow = CommonDefs.CombineListner_Action_int(this.m_Table.OnSelectAtRow, MakeDelegateFromCSFunction(this.OnItemClick, MakeGenericClass(Action1, Int32), this), true)
    this.m_BuyCountButton.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_BuyCountButton.onValueChanged, MakeDelegateFromCSFunction(this.OnBuyCountChanged, MakeGenericClass(Action1, UInt32), this), true)
    this.m_RadioBox.OnSelect = CommonDefs.CombineListner_Action_QnButton_int(this.m_RadioBox.OnSelect, MakeDelegateFromCSFunction(this.OnRadioBoxChanged, MakeGenericClass(Action2, QnButton, Int32), this), true)
    this.m_BuyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_BuyButton.OnClick, MakeDelegateFromCSFunction(this.OnBuyButtonClick, MakeGenericClass(Action1, QnButton), this), true)
    this.m_SendAreaBuyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_SendAreaBuyButton.OnClick, MakeDelegateFromCSFunction(this.OnBuyButtonClick, MakeGenericClass(Action1, QnButton), this), true)
    this.m_SendAreaSendButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_SendAreaSendButton.OnClick, MakeDelegateFromCSFunction(this.OnSendButtonClick, MakeGenericClass(Action1, QnButton), this), true)
    this.m_PopupMenu.OnSelect = CommonDefs.CombineListner_Action_QnButton_int(this.m_PopupMenu.OnSelect, MakeDelegateFromCSFunction(this.OnPopupMenuSelect, MakeGenericClass(Action2, QnButton, Int32), this), true)
    this.m_TianQiButton.OnClick = MakeDelegateFromCSFunction(this.OnTianQiButtonClick, MakeGenericClass(Action1, QnButton), this)
    EventManager.AddListenerInternal(EnumEventType.PlayerBuyMallItemSuccess, MakeDelegateFromCSFunction(this.OnPlayerBuyMallItemSuccess, MakeGenericClass(Action4, Int32, UInt32, UInt32, String), this))
    if this.m_ForcePopup and this.CurrentCategory >= 0 then
        this.m_RadioBox:ChangeTo(this.CurrentCategory, true)
    end

    CLinyuShopWndHolder:OnEnable(this)
end
CLinyuShopWnd.m_OnDisable_CS2LuaHook = function (this)
    CShopMallMgr.OnLingyuLimitUpdated = CommonDefs.CombineListner_Action_bool(CShopMallMgr.OnLingyuLimitUpdated, MakeDelegateFromCSFunction(this.UpdateView, MakeGenericClass(Action1, Boolean), this), false)
    CShopMallMgr.OnLingyuMallAutoShangJiaUpdated = CommonDefs.CombineListner_Action_bool(CShopMallMgr.OnLingyuMallAutoShangJiaUpdated, MakeDelegateFromCSFunction(this.UpdateView2, MakeGenericClass(Action1, Boolean), this), false)
    this.m_Table.OnSelectAtRow = CommonDefs.CombineListner_Action_int(this.m_Table.OnSelectAtRow, MakeDelegateFromCSFunction(this.OnItemClick, MakeGenericClass(Action1, Int32), this), false)
    this.m_BuyCountButton.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_BuyCountButton.onValueChanged, MakeDelegateFromCSFunction(this.OnBuyCountChanged, MakeGenericClass(Action1, UInt32), this), false)
    this.m_RadioBox.OnSelect = CommonDefs.CombineListner_Action_QnButton_int(this.m_RadioBox.OnSelect, MakeDelegateFromCSFunction(this.OnRadioBoxChanged, MakeGenericClass(Action2, QnButton, Int32), this), false)
    this.m_BuyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_BuyButton.OnClick, MakeDelegateFromCSFunction(this.OnBuyButtonClick, MakeGenericClass(Action1, QnButton), this), false)
    this.m_SendAreaBuyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_SendAreaBuyButton.OnClick, MakeDelegateFromCSFunction(this.OnBuyButtonClick, MakeGenericClass(Action1, QnButton), this), false)
    this.m_SendAreaSendButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_SendAreaSendButton.OnClick, MakeDelegateFromCSFunction(this.OnSendButtonClick, MakeGenericClass(Action1, QnButton), this), false)
    this.m_PopupMenu.OnSelect = CommonDefs.CombineListner_Action_QnButton_int(this.m_PopupMenu.OnSelect, MakeDelegateFromCSFunction(this.OnPopupMenuSelect, MakeGenericClass(Action2, QnButton, Int32), this), false)
    this.m_TianQiButton.OnClick = nil
    if this.m_TianQiTick ~= nil then
        invoke(this.m_TianQiTick)
    end
    EventManager.RemoveListenerInternal(EnumEventType.PlayerBuyMallItemSuccess, MakeDelegateFromCSFunction(this.OnPlayerBuyMallItemSuccess, MakeGenericClass(Action4, Int32, UInt32, UInt32, String), this))

    CLinyuShopWndHolder:OnDisable(this)
end
CLinyuShopWnd.m_Start_CS2LuaHook = function (this)
    this.m_PopupMenu:ShowSelf(false)
    this.m_CurrentItems = CShopMallMgr.GetLingYuMallInfo(this.CurrentCategory - 1, this.CurrentSubCategory)
    this.m_Table.m_DataSource = this

    this.m_BuyCountButton:SetMinMax(1, 999, 1)

    this:RefreshVisibleCategory(true)

    if CShopMallMgr.SelectCategory ~= 0 then
        this.m_RadioBox:ChangeTo(CShopMallMgr.SelectCategory + 1, true)
        local row = this:GetRowByItemId(CShopMallMgr.SearchTemplateId)
        this.CurrentSelectedItem = row
        this.m_Table:SetSelectRow(row, true)
    end
    if LuaWishMgr.WishShop then
      this.m_TianQiButton.Visible = false
    else
      if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.TianQiJiFen) > CLinyuShopWnd.s_MinScoreShowTianQiButton then
        local visible = Random.value < CLinyuShopWnd.s_Probility
        this.m_TianQiButton.Visible = visible
        if visible then
          this.m_TianQiTick = CTickMgr.Register(DelegateFactory.Action(function ()
            this.m_TianQiButton.Visible = false
          end), CLinyuShopWnd.TIANQISHOP_LASTTIME, ETickType.Once)
        end
      else
        this.m_TianQiButton.Visible = false
      end
    end
end
CLinyuShopWnd.m_OnTianQiButtonClick_CS2LuaHook = function (this, button)
    local jifen = 0
    if CClientMainPlayer.Inst ~= nil then
        jifen = CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.TianQiJiFen)
    end
    local msg = g_MessageMgr:FormatMessage("MALL_ENTRANCE_TIANQI_TIP", jifen)
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
        Gac2Gas.RequestOpenTianQiShop()
    end), nil, nil, nil, false)
end
CLinyuShopWnd.m_RefreshVisibleCategory_CS2LuaHook = function (this, needselect)
    --加入等级需求
    if LuaWishMgr.WishShop then
      this.m_RadioBox:SetVisibleGroup(Table2ArrayWithCount({
        0,
        2,
        3,
        4,
        5,
      }, 5, MakeArrayClass(System.Int32)), needselect)
    elseif CClientMainPlayer.Inst ~= nil then
        local setting = Mall_Setting.GetData("XinShouTuiJian_Grade")
        local level = CClientMainPlayer.Inst.MaxLevel
        if level > math.floor(tonumber(setting.Value or 0)) then
            local newIsEmpty = this:IsCategoryEmpty(- 1)
            if this:IsCategoryEmpty(CLinyuShopWnd.LINGYU_GIFT_CATEGORY) then
                if newIsEmpty then
                    this.m_RadioBox:SetVisibleGroup(Table2ArrayWithCount({2, 3, 4, 5, 6}, 5, MakeArrayClass(System.Int32)), needselect)
                else
                    this.m_RadioBox:SetVisibleGroup(Table2ArrayWithCount({
                        0,
                        2,
                        3,
                        4,
                        5,
                        6
                    }, 6, MakeArrayClass(System.Int32)), needselect)
                end
            else
                if newIsEmpty then
                    this.m_RadioBox:SetVisibleGroup(Table2ArrayWithCount({
                        2,
                        3,
                        4,
                        5,
                        6,
                        7
                    }, 6, MakeArrayClass(System.Int32)), needselect)
                else
                    this.m_RadioBox:SetVisibleGroup(Table2ArrayWithCount({
                        0,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7
                    }, 7, MakeArrayClass(System.Int32)), needselect)
                end
            end
        else
            if this:IsCategoryEmpty(CLinyuShopWnd.LINGYU_GIFT_CATEGORY) then
                this.m_RadioBox:SetVisibleGroup(Table2ArrayWithCount({
                    1,
                    2,
                    3,
                    4,
                    5,
                    6
                }, 6, MakeArrayClass(System.Int32)), needselect)
            else
                this.m_RadioBox:SetVisibleGroup(Table2ArrayWithCount({
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7
                }, 7, MakeArrayClass(System.Int32)), needselect)
            end
        end
    end
end
CLinyuShopWnd.m_ItemAt_CS2LuaHook = function (this, view, row)
    if this:IsGiftCategory(this.CurrentCategory - 1) then
        local item = TypeAs(view:GetFromPool(1), typeof(CShopMallGoodsItem2))
        item:UpdateData(this.m_CurrentItems[row], false)
        item.OnClickItemTexture = DelegateFactory.Action(function ()
            view:SetSelectRow(row, true)
        end)
        return item
    else
        local item = TypeAs(view:GetFromPool(0), typeof(CShopMallGoodsItem))
        item:UpdateData(this.m_CurrentItems[row], EnumMoneyType.LingYu, false, false)
        item.OnClickItemTexture = DelegateFactory.Action(function ()
            view:SetSelectRow(row, true)
        end)
        return item
    end
end
CLinyuShopWnd.m_UpdateView2_CS2LuaHook = function (this, updatePosition)

    this:RefreshVisibleCategory(false)
    local category = CShopMallMgr.GetItemCategoryOfLinyuShop(CShopMallMgr.SearchTemplateId)
    if category ~= 0 then
        this.m_RadioBox:ChangeTo(category + 1, true)
        local row = this:GetRowByItemId(CShopMallMgr.SearchTemplateId)
        this.CurrentSelectedItem = row
    else
        if CShopMallMgr.SelectCategory ~= 0 then
            this.m_RadioBox:ChangeTo(CShopMallMgr.SelectCategory + 1, true)
            local row = this:GetRowByItemId(CShopMallMgr.SearchTemplateId)
            this.CurrentSelectedItem = row
        end
    end
    this:UpdateView(updatePosition)
end
CLinyuShopWnd.m_SetItemOpArea_CS2LuaHook = function (this, template)
  local calTime = function(beginTime,endTime)
    local beginOfTheBuyDay = DateTime.ParseExact(beginTime, "yyyy-MM-dd-HH-mm", CultureInfo.CurrentCulture)
    local endOfTheBuyDay = DateTime.ParseExact(endTime, "yyyy-MM-dd-HH-mm", CultureInfo.CurrentCulture)

    local now = CServerTimeMgr.Inst:GetZone8Time()
    local seconds = beginOfTheBuyDay:Subtract(now).TotalSeconds
    local endSeconds = endOfTheBuyDay:Subtract(now).TotalSeconds
    if seconds < 0 and endSeconds > 0 then
      return true
    end

    return false
  end

  if LuaWishMgr.WishShop then
    this.OnSellRoot.transform:Find('WishButton').gameObject:SetActive(true)
    this.m_BuyButton.gameObject:SetActive(false)
    this.m_SendArea:SetActive(false)
    local onWishAddClick = function(go)
      local totalCost = this.m_MoneyCtrl:GetCost()
      local minNum = tonumber(Mall_Setting.GetData("Wish_MinTotalPrice").Value)
      local maxNum = tonumber(Mall_Setting.GetData("Wish_MaxTotalPrice").Value)
      if totalCost < minNum then
        g_MessageMgr:ShowMessage('WISH_ADDWISH_FAILED_LESS_THAN_MIN_PRICE',minNum)
      elseif totalCost > maxNum then
        g_MessageMgr:ShowMessage('WISH_ADDWISH_FAILED_MORE_THAN_MAX_PRICE',maxNum)
      else
        local num = this.m_BuyCountButton:GetValue()
        g_ScriptEvent:BroadcastInLua("SelectWishItemUpdate",template.ItemId,num)
        CUIManager.CloseUI(CUIResources.ShoppingMallWnd)
      end
    end
    CommonDefs.AddOnClickListener(this.OnSellRoot.transform:Find('WishButton').gameObject,DelegateFactory.Action_GameObject(onWishAddClick),false)
  else
    this.OnSellRoot.transform:Find('WishButton').gameObject:SetActive(false)
    LuaTradeSendMgr.SendItemDiscount = nil
    local giftEnable = (CommonDefs.GetPlayConfigValue("GiftMallItem"))
    if giftEnable == 1 then
      if template.CanZengSong == 1 then
        this.m_BuyButton.gameObject:SetActive(false)
        this.m_SendArea:SetActive(true)
        local discountIcon = this.m_SendAreaSendButton.transform:Find('DiscountSprite').gameObject
        local normalText = this.m_SendAreaSendButton.transform:Find('Text').gameObject
        normalText:SetActive(true)
        discountIcon:SetActive(false)
        local discountItem = Mall_ZengsongDiscountItem.GetData(template.ItemId)
        if discountItem and discountItem.Status == 0 then
          if calTime(discountItem.DiscountBeginTime,discountItem.DiscountEndTime) then
            discountIcon:SetActive(true)
            normalText:SetActive(false)
            LuaTradeSendMgr.SendItemDiscount = discountItem.DiscountNum
          end
        else
          local discountCategory = Mall_ZengsongDiscountCategory.GetData(template.SubCategory)
          if discountCategory and discountCategory.Status == 0 then
            if calTime(discountCategory.DiscountBeginTime,discountCategory.DiscountEndTime) then
              discountIcon:SetActive(true)
              normalText:SetActive(false)
              LuaTradeSendMgr.SendItemDiscount = discountCategory.DiscountNum
            end
          end
        end
      elseif template.CanZengSong == 0 then
        this.m_BuyButton.gameObject:SetActive(true)
        this.m_SendArea:SetActive(false)
      end
    else
      this.m_BuyButton.gameObject:SetActive(true)
      this.m_SendArea:SetActive(false)
    end
  end
end
CLinyuShopWnd.m_UpdateView_CS2LuaHook = function (this, updatePosition)

    this.m_ModelPreviewer:UpdateData(nil, 0, "")
    if LuaWishMgr.WishShop then
      this.m_CurrentItems = CreateFromClass(MakeGenericClass(List, ShopMallTemlate))
      local itemList = CShopMallMgr.GetLingYuMallInfo(this.CurrentCategory - 1, this.CurrentSubCategory)
      for i=0,itemList.Count - 1 do
        local itemData = itemList[i]
        if itemData.Status == 2 then
          local shelfTime = tonumber(itemData.PreSellTime)
          local delta = shelfTime - CServerTimeMgr.Inst.timeStamp
          if delta <= 0 then
            this.m_CurrentItems:Add(itemData)
          end
        else
          if itemData.CanZengSong == 1 then
            this.m_CurrentItems:Add(itemData)
          end
        end
      end
    else
      this.m_CurrentItems = CShopMallMgr.GetLingYuMallInfo(this.CurrentCategory - 1, this.CurrentSubCategory)
    end

    if this:IsGiftCategory(this.CurrentCategory - 1) then
        this.m_Table:UpdateTableParams(CLinyuShopWnd.GIFT_CELL_WIDTH, CLinyuShopWnd.GIFT_CELL_HEIGHT, CLinyuShopWnd.GIFT_COLUMN_LIMIT)
    else
        this.m_Table:UpdateTableParams(CLinyuShopWnd.NORMAL_CELL_WIDTH, CLinyuShopWnd.NORMAL_CELL_HEIGHT, CLinyuShopWnd.NORMAL_COLUMN_LIMIT)
    end
    this.m_Table:ReloadData(true, updatePosition)
    if this.m_CurrentItems ~= nil and this.m_CurrentItems.Count > 0 then
        this.m_BuyCountButton:SetMinMax(1, 999, 1)
        if this.m_BuyCountButton:GetValue() > 999 or this.m_BuyCountButton:GetValue() < 1 then
            this.m_BuyCountButton:SetValue(1, true)
        end
        if this.CurrentSelectedItem >= 0 and this.CurrentSelectedItem < this.m_CurrentItems.Count then
            this.m_Table:SetSelectRow(this.CurrentSelectedItem, true)

            local item = this.m_Table:GetItemAtRow(this.CurrentSelectedItem)

            if item and item.gameObject then
                CUICommonDef.SetFullyVisible(item.gameObject, this.m_Table.m_ScrollView)
            end
        else
            this.m_Table:SetSelectRow(0, true)
        end
    else
        this.m_BuyCountButton:SetMinMax(0, 0, 1)
        this.m_BuyCountButton:SetValue(0, true)
        this.CurrentSelectedItem = - 1
    end

    this:UpdateTotalPrice()
end
CLinyuShopWnd.m_OnRadioBoxChanged_CS2LuaHook = function (this, button, index)
    CLinyuShopWndHolder:OnRadioBoxChanged(index)

    if this.CurrentCategory ~= index then
        this.CurrentSubCategory = 0
    end
    this.CurrentCategory = index
    this.CurrentSelectedItem = - 1
    this.m_SubCategorys = CShopMallMgr.GetLingYuMallSubCategory(this.CurrentCategory - 1)
    --排除几个分类不显示子分类
    if this.CurrentCategory - 1 ~= CLinyuShopWnd.LINGYU_XINSHOU_CATEGORY and this.CurrentCategory - 1 ~= CLinyuShopWnd.LINGYU_GIFT_CATEGORY and this.CurrentCategory - 1 ~= - 1 and this.m_SubCategorys ~= nil and this.m_SubCategorys.Count > 1 then
        local popupIndex = CommonDefs.ListIndexOf(this.m_SubCategorys, this.CurrentSubCategory)
        if popupIndex < 0 or popupIndex > this.m_SubCategorys.Count then
            popupIndex = 0
        end

        if CShopMallMgr.SelectSubCategory ~= 0 then
            popupIndex = CommonDefs.ListIndexOf(this.m_SubCategorys, CShopMallMgr.SelectSubCategory)
        end
        this.m_PopupMenu:Init(button.transform, this.m_SubCategorys, popupIndex)
    else
        this.m_PopupMenu:ShowSelf(false)
    end
    this:UpdateView(false)
end
CLinyuShopWnd.m_OnPopupMenuSelect_CS2LuaHook = function (this, button, index)
    if this.m_SubCategorys ~= nil and this.m_SubCategorys.Count > index then
        this.CurrentSubCategory = this.m_SubCategorys[index]
        this:UpdateView(false)
    end
end
CLinyuShopWnd.m_OnSendButtonClick_CS2LuaHook = function (this, button)
    if this.m_CurrentItems ~= nil and this.m_CurrentItems.Count > this.CurrentSelectedItem and this.CurrentSelectedItem >= 0 then
        local count = this.m_BuyCountButton:GetValue()
        local template = this.m_CurrentItems[this.CurrentSelectedItem]
        if System.String.IsNullOrEmpty(template.RmbPID) then
            if this.CurrentSelectedItem >= 0 and count > 0 then
                if CShopMallMgr.TryCheckEnoughJade(count * template.Price, template.ItemId) then
                  local regionId = 1
                  if this:IsLingyuLimitItem(this.CurrentCategory - 1, template) then
                    regionId = EShopMallRegion_lua.ELingyuMallLimit
                  else
                    regionId = EShopMallRegion_lua.ELingyuMall
                  end

                  --CTradeSendMgr.Inst:ShowLingYuTradeSendWnd(template.ItemId, count, count * template.Price, regionId)
                  LuaTradeSendMgr.ShowLingYuTradeSendWnd(template.ItemId, count, count * template.Price, regionId)
                end
            end
        else
            CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(template.RmbPID), 0)
        end
    end
end
CLinyuShopWnd.m_OnBuyButtonClick_CS2LuaHook = function (this, button)

    if this.m_CurrentItems ~= nil and this.m_CurrentItems.Count > this.CurrentSelectedItem and this.CurrentSelectedItem >= 0 then
        local count = this.m_BuyCountButton:GetValue()
        local template = this.m_CurrentItems[this.CurrentSelectedItem]
        if System.String.IsNullOrEmpty(template.RmbPID) then
            if this.CurrentSelectedItem >= 0 and count > 0 then
                if CShopMallMgr.TryCheckEnoughJade(count * template.Price, template.ItemId) then
                    CLinyuShopWndHolder:OnBuyButtonClick(template, count, this)
                end
            end
        else
            if template.AvalibleCount == 0 then
                g_MessageMgr:ShowMessage("MALL_RMB_LIMIT")
            else
                CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(template.RmbPID), 0)
            end
        end
    end
end
CLinyuShopWnd.m_IsLingyuLimitItem_CS2LuaHook = function (this, category, template)
    if category == CLinyuShopWnd.LINGYU_LIMIT_CATEGORY then
        return true
    end
    if template.AvalibleCount >= 0 then
        return true
    end
    if Mall_LingYuMallLimit.GetData(template.ItemId) then
        return true
    end
    return false
end
CLinyuShopWnd.m_UpdateTotalPrice_CS2LuaHook = function (this)

    local totalprice = 0
    if this.m_CurrentItems ~= nil and this.m_CurrentItems.Count > this.CurrentSelectedItem and this.CurrentSelectedItem >= 0 then
        totalprice = this.m_CurrentItems[this.CurrentSelectedItem].Price * this.m_BuyCountButton:GetValue()
    end
    if LuaWishMgr.WishShop then
      this.m_MoneyCtrl.transform:Find('Own').gameObject:SetActive(false)
    end
    this.m_MoneyCtrl:SetCost(totalprice)
end
CLinyuShopWnd.m_UpdatePreview_CS2LuaHook = function (this, index)

    local template = nil
    if index >= 0 and index <= this.m_CurrentItems.Count then
        template = this.m_CurrentItems[index]
        if this.OnSellRoot and this.PreSellRoot then
            if template.PreSell and template.Status == 3 then
                local function UpdatePreSellView(template)
                    if  template.Status == 3 and template.PreSell then
                        this.OnSellRoot:SetActive(false)
                        this.PreSellRoot:SetActive(true)
                        --更新预售信息
                        local CItem = import "L10.Game.CItem"
                        local nameLabel = this.PreSellRoot.transform:Find("Name"):GetComponent(typeof(UILabel))
                        local levelLabel = this.PreSellRoot.transform:Find("Level"):GetComponent(typeof(UILabel))
                        local item = CItemMgr.Inst:GetItemTemplate(template.ItemId)
                        if item then
                            nameLabel.text = item.Name
                            if item.AdjustedGradeCheck > 0 then
                                levelLabel.gameObject:SetActive(true)
                                levelLabel.text = SafeStringFormat3("lv.%d", item.AdjustedGradeCheck)
                                if CClientMainPlayer.Inst and item.AdjustedGradeCheck > CClientMainPlayer.Inst.Level then
                                    levelLabel.color = Color.red;
                                else
                                    levelLabel.color = Color.white
                                end
                            else
                                levelLabel.gameObject:SetActive(false)
                            end
                        end
                        local descriptionLabel = this.PreSellRoot.transform:Find("Description"):GetComponent(typeof(UILabel))
                        local description = CItem.GetItemDescription(template.ItemId,true)
                        descriptionLabel.text = CUICommonDef.TranslateToNGUIText(description)
                        local priceLabel = this.PreSellRoot.transform:Find("Cost"):GetComponent(typeof(UILabel))
                        priceLabel.text = tostring(template.Price)
                        local timeLabel = this.PreSellRoot.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
                        timeLabel.text = template.PreSellTime or ""
                    end
                end
                UpdatePreSellView(template)
                return
            else
                this.OnSellRoot:SetActive(true)
                this.PreSellRoot:SetActive(false)
            end
        end

        local otherCategoryName = ""
        if this.CurrentCategory == 0 or this.CurrentCategory == 1 then
            otherCategoryName = this:GetOtherCategoryName(template)
        end
        this.m_MoneyCtrl:SetType(template.CanUseBindJade > 0 and EnumMoneyType.LingYu_WithBind or EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
        if System.String.IsNullOrEmpty(template.RmbPID) then
            this.m_BuyAreaRoot:SetActive(true)
            local maxCount = 999
            if CClientMainPlayer.Inst ~= nil then
                local jade = CClientMainPlayer.Inst.Jade
                if template.CanUseBindJade > 0 then
                    jade = CClientMainPlayer.Inst.Jade + CClientMainPlayer.Inst.BindJade
                end
                local singlePrice = math.max(1, template.Price)
                --以防价格为0报错
                if LuaWishMgr.WishShop then
                  --maxCount = 999
                  local maxWishNum = tonumber(Mall_Setting.GetData("Wish_ItemNumOverlapLimit").Value)
                  local item = CItemMgr.Inst:GetItemTemplate(template.ItemId)
                  maxCount = math.min(999,item.Overlap*maxWishNum)
                else
                  maxCount = math.floor(math.floor(jade / singlePrice))
                  maxCount = math.min(math.max(maxCount, 1), 999)
                end
            end
            if template.AvalibleCount >= 0 then
                local avaliableCount = math.max(1, template.AvalibleCount)
                maxCount = math.min(maxCount, avaliableCount)
            else
                if template.SubCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ShiZhuang or template.SubCategory == LuaShopMallFashionPreviewMgr.EnumLingYuMallSubCategory.ZuoQi then
                    maxCount = 1
                end
                local limitPurchaseQuantityItemIds = g_LuaUtil:StrSplit(Mall_Setting.GetData("LimitPurchaseQuantityItemIds").Value,";")
                for _,itemId in pairs(limitPurchaseQuantityItemIds) do
                    if itemId == tostring(template.ItemId) then
                        maxCount = 1
                    end
                end
            end
            this.m_BuyCountButton:SetMinMax(1, maxCount, 1)
            this.m_ModelPreviewer:UpdateData(template, 0, otherCategoryName)
        else
            this.m_BuyAreaRoot:SetActive(false)
            this.m_ModelPreviewer:UpdateData(template, 200, otherCategoryName)
        end
        this:UpdateTotalPrice()
        this:SetItemOpArea(template)
    end

    CLinyuShopWndHolder:UpdateBuyButton(index)
end
CLinyuShopWnd.m_GetRowByItemId_CS2LuaHook = function (this, templateId)
    do
        local i = 0
        while i < this.m_CurrentItems.Count do
            if this.m_CurrentItems[i].ItemId == templateId then
                return i
            end
            i = i + 1
        end
    end
    return 0
end
CLinyuShopWnd.m_GetOtherCategoryName_CS2LuaHook = function (this, template)
    local ret = ""
    do
        local i = 0
        while i < template.Category.Length do
            local category = template.Category[i]
            if category > 0 then
                ret = this.m_RadioBox:GetTitle(category + 1)
                return ret
            end
            i = i + 1
        end
    end
    return ret
end

CLinyuShopWndHolder = {}
CLinyuShopWndHolder.Instance = nil
CLinyuShopWndHolder.Index = 8
CLinyuShopWndHolder.ItemList = CreateFromClass(MakeGenericClass(List, ShopMallTemlate))
CLinyuShopWndHolder.Tick = nil
CLinyuShopWndHolder.m_OwnedShopItems = {}
function CLinyuShopWndHolder:OnEnable(this)
    CLinyuShopWndHolder.Instance = this
    Gac2Gas.QuerySingletonShopShelf()
    g_ScriptEvent:AddListener("SyncSingletonShopOnshelfStatus", self, "SyncSingletonShopOnshelfStatus")
    g_ScriptEvent:AddListener("SyncSingletonShopBuyResult", self, "SyncSingletonShopBuyResult")
    g_ScriptEvent:AddListener("SendItem", self, "SendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "SetItemAt")
    g_ScriptEvent:AddListener("OnLingyuLimitUpdatedForSearchTemplateId", self, "OnLingyuLimitUpdatedForSearchTemplateId")
    g_ScriptEvent:AddListener("OnSyncRepertoryForMallPreviewDone", self, "OnSyncRepertoryForMallPreviewDone")
    LuaShopMallFashionPreviewMgr:RequestSyncRepertoryForMallPreview()
    self.m_OwnedShopItems = LuaShopMallFashionPreviewMgr:GetOwnedShopItemsInfo()
end

function CLinyuShopWndHolder:OnDisable(this)
    CLinyuShopWndHolder.Instance = nil
    self.m_OwnedShopItems = {}
    g_ScriptEvent:RemoveListener("SyncSingletonShopOnshelfStatus", self, "SyncSingletonShopOnshelfStatus")
    g_ScriptEvent:RemoveListener("SyncSingletonShopBuyResult", self, "SyncSingletonShopBuyResult")
    g_ScriptEvent:RemoveListener("SendItem", self, "SendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "SetItemAt")
    g_ScriptEvent:RemoveListener("OnLingyuLimitUpdatedForSearchTemplateId", self, "OnLingyuLimitUpdatedForSearchTemplateId")
    g_ScriptEvent:RemoveListener("OnSyncRepertoryForMallPreviewDone", self, "OnSyncRepertoryForMallPreviewDone")
    if CLinyuShopWndHolder.Tick then
        UnRegisterTick(CLinyuShopWndHolder.Tick)
        CLinyuShopWndHolder.Tick = nil
    end
end

function CLinyuShopWndHolder:OnLingyuLimitUpdatedForSearchTemplateId()
    if CLinyuShopWndHolder.Instance and CShopMallMgr.SearchTemplateId ~= 0 then
        local row = CLinyuShopWndHolder.Instance:GetRowByItemId(CShopMallMgr.SearchTemplateId)
        CLinyuShopWndHolder.Instance.CurrentSelectedItem = row
        CLinyuShopWndHolder.Instance.m_Table:SetSelectRow(row, true)
        CShopMallMgr.SearchTemplateId = 0
    end
end

function CLinyuShopWndHolder:IsInGiftTab()
    if CLinyuShopWndHolder.Instance.CurrentCategory ~= CLinyuShopWndHolder.Index then return false end
    return true
end

function CLinyuShopWndHolder:SetItemAt(args)
    local  place, position, oldItemId, newItemId = args[0],args[1],args[2],args[3]
    local item = CItemMgr.Inst:GetById( newItemId)
    if item and item.IsItem then
        local data = Mall_LingYuMall.GetData(item.TemplateId)
        if data then
            if data.OwnerIndex ~= 0 then
                LuaShopMallMgr.m_HasOwnIndexItemIds[item.TemplateId] = true
                CLinyuShopWndHolder.Instance:UpdateView(CLinyuShopWndHolder.Instance.CurrentSelectedItem)
            end
        end
    end
    self.m_OwnedShopItems = LuaShopMallFashionPreviewMgr:GetOwnedShopItemsInfo()
    if item and item.IsItem then
        local row = CLinyuShopWndHolder.Instance:GetRowByItemId(item.TemplateId)
        local template = CLinyuShopWndHolder.Instance.m_CurrentItems[row]
        item = CLinyuShopWndHolder.Instance.m_Table:GetItemAtRow(row)
        if item then
            if TypeIs(item, typeof(CShopMallGoodsItem)) then
                item:UpdateData(template, EnumMoneyType.LingYu, false, false)
            elseif TypeIs(item, typeof(CShopMallGoodsItem2)) then
                item:UpdateData(template, false)
            end
        end
    end
end

function CLinyuShopWndHolder:SendItem(args)
    local item = CItemMgr.Inst:GetById(args[0])
    if (self:IsInGiftTab() and CLinyuShopWndHolder.Instance) or (item == nil) then
        CLinyuShopWndHolder.Instance:UpdateView(CLinyuShopWndHolder.Instance.CurrentSelectedItem)
    end
end

function CLinyuShopWndHolder:OnSyncRepertoryForMallPreviewDone()
    self.m_OwnedShopItems = LuaShopMallFashionPreviewMgr:GetOwnedShopItemsInfo()
end

function CLinyuShopWndHolder:SyncSingletonShopOnshelfStatus(onShelfUd)
    CLinyuShopWndHolder.Instance.m_RadioBox.m_RadioButtons[CLinyuShopWndHolder.Index].gameObject:SetActive(true)
    CLinyuShopWndHolder.Instance.m_RadioBox.m_Table:Reposition()
    self:UpdateData(onShelfUd)
end

function CLinyuShopWndHolder:SyncSingletonShopBuyResult(bSuccess, onShelfUd)
    self:UpdateData(onShelfUd)
end

function CLinyuShopWndHolder:UpdateData(onShelfUd)
    local onShelfTbl = MsgPackImpl.unpack(onShelfUd)
    if not onShelfTbl then return end

    CommonDefs.ListClear(CLinyuShopWndHolder.ItemList)
    for i = 0, (onShelfTbl.Count - 1), 6 do
        local item = ShopMallTemlate()
        item.ItemId = onShelfTbl[i + 1]
        item.Index = onShelfTbl[i]
        item.Status = onShelfTbl[i + 3]
        item.PreSellTime = tostring(onShelfTbl[i + 5])
        item.Price = onShelfTbl[i + 2]
        --item.AvalibleCount = onShelfTbl[i + 4]
        CommonDefs.ListAdd_LuaCall(CLinyuShopWndHolder.ItemList, item)
    end

    if self:IsInGiftTab() then
        CLinyuShopWndHolder.Instance:UpdateView(false)
    end
end

function CLinyuShopWndHolder:OnRadioBoxChanged(index)
    if index ~= CLinyuShopWndHolder.Index then return end
    Gac2Gas.QuerySingletonShopShelf()
end

function CLinyuShopWndHolder:GetGiftList(category)
    if category == CLinyuShopWndHolder.Index - 1 then
        return CLinyuShopWndHolder.ItemList
    end
end

function CLinyuShopWndHolder:OnBuyButtonClick(template, count, this)
    if not self:IsInGiftTab() then
        LuaShopMallFashionPreviewMgr:BuyMallItem(template, this:IsLingyuLimitItem(this.CurrentCategory - 1, template) and EShopMallRegion_lua.ELingyuMallLimit or EShopMallRegion_lua.ELingyuMall, count)
    else
        Gac2Gas.ReqeustBuySingletonShopGood(template.Index, count)
    end
end

function CLinyuShopWndHolder:UpdateBuyButton(row)
    if not CLinyuShopWndHolder.Instance then return end
    local qnBtn = CLinyuShopWndHolder.Instance.m_BuyButton
    qnBtn.Text = LocalString.GetString("购买")
    qnBtn.Enabled = true
    if CLinyuShopWndHolder.Tick then
        UnRegisterTick(CLinyuShopWndHolder.Tick)
        CLinyuShopWndHolder.Tick = nil
    end

    if not self:IsInGiftTab()  then return end
    if CLinyuShopWndHolder.ItemList.Count >= row then
        local status = CLinyuShopWndHolder.ItemList[row].Status
        if status == 2 then
            local shelfTime = tonumber(CLinyuShopWndHolder.ItemList[row].PreSellTime)
            local delta = shelfTime - CServerTimeMgr.Inst.timeStamp
            if delta <= 0 then
                qnBtn.Text = LocalString.GetString("购买")
                qnBtn.Enabled = true
            else
                qnBtn.Text = SafeStringFormat3(LocalString.GetString("备货中  %02d:%02d:%02d"), math.floor(delta / 3600), math.floor((delta % 3600) / 60), math.floor(delta % 60))
                qnBtn.Enabled = false
                CLinyuShopWndHolder.Tick = RegisterTickOnce(function()
                    self:UpdateBuyButton(row)
                end, 1000)
            end
        elseif status == 5 then
            qnBtn.Text = LocalString.GetString("已售完")
            qnBtn.Enabled = false
        end
    end
end

local CUIResources = import "L10.UI.CUIResources"
local CTradeSendMgr = import "L10.Game.CTradeSendMgr"
local EnumTradeSendType = import "L10.Game.EnumTradeSendType"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"

LuaTradeSendMgr = {}

function LuaTradeSendMgr.ShowLingYuTradeSendWnd(itemTemplateId, count, totalValue, regionId)
  CTradeSendMgr.Inst.type = EnumTradeSendType.LingYuTradeSend
  LuaTradeSendMgr.sendItemCount = count
  LuaTradeSendMgr.sendItemTotalValue = totalValue
  LuaTradeSendMgr.sendItemId = itemTemplateId
  LuaTradeSendMgr.sendItemRegionId = regionId
  CTradeSendMgr.Inst.monthRest = 0
  CTradeSendMgr.Inst.sendPlayerList:Clear()
  CUIManager.ShowUI(CUIResources.TradeSendChooseWnd)
end


function LuaTradeSendMgr.OnTradeSend(playerId,playerName)
  local item = CItemMgr.Inst:GetItemTemplate(LuaTradeSendMgr.sendItemId)

  if LuaTradeSendMgr.SendItemDiscount then
    CInputBoxMgr.ShowInputBox(g_MessageMgr:FormatMessage("Zengsong_Confirm_Msg_Discount",playerName, item.Name, LuaTradeSendMgr.sendItemTotalValue, CTradeSendMgr.Inst.monthRest, LuaTradeSendMgr.SendItemDiscount),
    DelegateFactory.Action_string(function(input)
      input = StringTrim(input)
      if input and input~= "" then
        if CWordFilterMgr.Inst:DoFilterOnPresentUserDefinedWords(input, LocalString.GetString("商城赠送留言"), false) ~= input then
          g_MessageMgr:ShowMessage("PresentItemContent_Illegal")
          input = nil
        end
      end
      if not input or input == "" then
        input = LocalString.GetString("赠君此物,聊表寸心")
      end
      Gac2Gas.GiftMallItem(playerId, LuaTradeSendMgr.sendItemRegionId, LuaTradeSendMgr.sendItemId, LuaTradeSendMgr.sendItemCount, input)
    end), 20, true, nil, LocalString.GetString("最多输入10个汉字"))
  else
    CInputBoxMgr.ShowInputBox(g_MessageMgr:FormatMessage("Zengsong_Confirm_Msg",playerName, item.Name, LuaTradeSendMgr.sendItemTotalValue, CTradeSendMgr.Inst.monthRest),
    DelegateFactory.Action_string(function(input)
      input = StringTrim(input)
      if input and input~= "" then
        if CWordFilterMgr.Inst:DoFilterOnPresentUserDefinedWords(input, LocalString.GetString("商城赠送留言"), false) ~= input then
          g_MessageMgr:ShowMessage("PresentItemContent_Illegal")
          input = nil
        end
      end
      if not input or input == "" then
        input = LocalString.GetString("赠君此物,聊表寸心")
      end
      Gac2Gas.GiftMallItem(playerId, LuaTradeSendMgr.sendItemRegionId, LuaTradeSendMgr.sendItemId, LuaTradeSendMgr.sendItemCount, input)
    end), 20, true, nil, LocalString.GetString("最多输入10个汉字"))
  end
  CUIManager.CloseUI(CUIResources.TradeSendChooseWnd)
end
