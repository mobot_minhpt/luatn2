require("3rdParty/ScriptEvent")
require("common/common_include")

local CommonDefs = import "L10.Game.CommonDefs"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CSigninMgr = import "L10.Game.CSigninMgr"
local SdkU3d = import "NtUniSdk.Unity3d.SdkU3d"
local ConstProp = import "NtUniSdk.Unity3d.ConstProp"
local ChannelDefine = import "L10.Game.ChannelDefine"
local NativeTools = import "L10.Engine.NativeTools"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local HTTPHelper = import "L10.Game.HTTPHelper"
local DelegateFactory = import "DelegateFactory"
local Main = import "L10.Engine.Main"
local LocalString = import "LocalString"
local Json = import "L10.Game.Utils.Json"
local Texture2D = import "UnityEngine.Texture2D"
local CTopAndRightMenuWnd = import "L10.UI.CTopAndRightMenuWnd"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local DateTime = import "System.DateTime"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CChargeMgr = import "L10.Game.CChargeMgr"
local CTradeSendMgr = import "L10.Game.CTradeSendMgr"
local CPayMgr = import "L10.Game.CPayMgr"

if rawget(_G, "LuaWelfareMgr") then
    g_ScriptEvent:RemoveListener("OnApplicationPause", LuaWelfareMgr, "OnApplicationPause")
	g_ScriptEvent:RemoveListener("PlayerLogin", LuaWelfareMgr, "OnPlayerLogin")
end

LuaWelfareMgr = class()

g_ScriptEvent:AddListener("OnApplicationPause", LuaWelfareMgr, "OnApplicationPause")
g_ScriptEvent:AddListener("PlayerLogin", LuaWelfareMgr, "OnPlayerLogin")

function LuaWelfareMgr:OnPlayerLogin()
	LuaWelfareMgr.HasSignedTodayShow = false
end

CTopAndRightMenuWnd.m_hookOnSigninAvailable = function(this)
	LuaWelfareMgr.HasSignedTodayShow = false
	this:SigninAlertFx(true)
end

LuaWelfareMgr.m_IsAndroidPlatform = nil
LuaWelfareMgr.m_AlreadyReadContentProvider = false

LuaWelfareMgr.m_NeteaseMemberBasicInfo = {}
LuaWelfareMgr.m_NeteaseMemberDataList = {}
LuaWelfareMgr.m_NeteaseMemberBannerURL = "https://a.game.163.com/fz/interface/frontend/fz.do?pos="
LuaWelfareMgr.m_IOSNeteaseMemberBannerID = "wangyiyouxihuiyuanjulebu-changgui-828758"
LuaWelfareMgr.m_AndroidNeteaseMemberBannerID = "wangyiyouxihuiyuanjulebu-changgui-141818"
LuaWelfareMgr.m_NeteaseMemberBannerTextureData = nil
LuaWelfareMgr.m_NeteaseMemberBannerClickURL = nil

LuaWelfareMgr.m_NeteaseMemberLastQueryInPurchaseInfoTime = nil
LuaWelfareMgr.m_MainWelfareWndShowMonth = false
function LuaWelfareMgr.GetNeteaseMemberBannerID()
	if CommonDefs.IsIOSPlatform() then
		return LuaWelfareMgr.m_IOSNeteaseMemberBannerID
	elseif CommonDefs.IsAndroidPlatform() then
		return LuaWelfareMgr.m_AndroidNeteaseMemberBannerID
	else
		return ""
	end
end

function LuaWelfareMgr.EnableDaShen()
	-- 仿照 COtherWelfareWindow 中的BindDaShen开关进行处理
	if CommonDefs.IS_CN_CLIENT then
		local closeChannel = {ChannelDefine.OPPO,ChannelDefine.HUAWEI,ChannelDefine.VIVO}
		local channel = CLoginMgr.Inst:GetLoginChannelConverted()
		for i,v in pairs(closeChannel) do
			if v == channel then
				return false
			end
		end

		return CSigninMgr.Inst:CheckValidPlatformByPlayerConfig("BindDaShen") or
		SdkU3d.getAuthTypeName() == ConstProp.NT_AUTH_NAME_FACEBOOK or
		SdkU3d.getAuthTypeName() == ConstProp.NT_AUTH_NAME_GOOGLE
	else
		return false
	end
end
--用于领奖的，和绑定大神要区别开
function LuaWelfareMgr.EnableDaShenEntry()
	if CommonDefs.IS_CN_CLIENT then
		return CSigninMgr.Inst:CheckValidPlatformByPlayerConfig("DaShenEntry")
	else
		return false
	end
end

function LuaWelfareMgr.EnableBindCC()
	-- 根据策划和CC的人沟通结果，绑定CC和绑定大神采用同样的开关来处理
	return LuaWelfareMgr.EnableDaShen()
end

function LuaWelfareMgr.OpenDaShenWelfareWnd()
	CUIManager.ShowUI("DaShenWelfareWnd")
end

function LuaWelfareMgr.BindDaShenViaContentProvider()

	--仅限android 平台
	if LuaWelfareMgr.m_IsAndroidPlatform==nil then
		LuaWelfareMgr.m_IsAndroidPlatform = CommonDefs.IsAndroidPlatform()
	end
	if not LuaWelfareMgr.m_IsAndroidPlatform then return end

	if LuaWelfareMgr.m_AlreadyReadContentProvider then return end

	LuaWelfareMgr.m_AlreadyReadContentProvider = true

	local token = NativeTools.GetDataFromContentProvider("content://com.netease.gl.core.ipc.glprovider/logToken", "logToken")
	if token and token~="" then
		Gac2Gas.ResuestSetDaShenLogToken(token)
	else
		Gac2Gas.ResuestSetDaShenLogToken("")
	end
end

function LuaWelfareMgr.OnPlayerLogin()
	--ForBindDaShen
	LuaWelfareMgr.m_AlreadyReadContentProvider = false
	--ForUnity2018Upgrade
	LuaWelfareMgr.m_Unity2018UpgradeInfo = {}
end

function LuaWelfareMgr:OnApplicationPause(args)
	-- print("LuaWelfareMgr.OnApplicationPause", args[0])
	if args and args[0] == false then
		if CommonDefs.IsAndroidPlatform() then
			Gac2Gas.RequestChangeApplicationPause(false)
		end
	end
end
------------------------------
---- BEGIN Unity2018升级指引奖励
------------------------------

LuaWelfareMgr.m_Unity2018UpgradeInfo = {}
function LuaWelfareMgr:OpenUnity2018UpgradeWnd()
	CUIManager.ShowUI("Unity2018UpgradeWnd")
end

function LuaWelfareMgr:CanShowUnity2018UpgradeAlert()
	--由于之前的设定里面跨服不允许显示其他奖励，所以这里跨服不显示红点
	if CClientMainPlayer.Inst==nil or CClientMainPlayer.Inst.IsCrossServerPlayer then
        return false
    end
	return self.m_Unity2018UpgradeInfo.canOpenWnd
end

--服务器端确保每次打开窗口前都会同步此信息
function LuaWelfareMgr:SyncUnity2018UpgradeInfo(canOpenWnd, needUpgrade, upgradeUrl, packageSize, finished, finishTime, awarded)
	self.m_Unity2018UpgradeInfo = {}
	self.m_Unity2018UpgradeInfo.canOpenWnd = canOpenWnd --某些渠道是不能领奖的，此时领奖功能不可用
	self.m_Unity2018UpgradeInfo.needUpgrade = needUpgrade --当前客户端要不要更新
	self.m_Unity2018UpgradeInfo.upgradeUrl = upgradeUrl --跳转链接，不需要更新时是空
	self.m_Unity2018UpgradeInfo.packageSize = packageSize
	self.m_Unity2018UpgradeInfo.finished = finished --是否更新过，也就是能不能领奖
	self.m_Unity2018UpgradeInfo.finishTime = finishTime --更新时间，用这个时间和表里的EndTime比较，EndTime是空或者比finishTime大的才能领
	self.m_Unity2018UpgradeInfo.awarded = awarded  --是否领取过奖励
	g_ScriptEvent:BroadcastInLua("OnRefresbUnity2018UpgradeInfo")
	EventManager.BroadcastInternalForLua(EnumEventType.OnSigninInfoUpdate, {}) --刷新主界面福利提示
end

function LuaWelfareMgr:SendUnity2018UpgradeAwardSuccess()
	self.m_Unity2018UpgradeInfo.canOpenWnd = false --和服务器端逻辑保持一致
	self.m_Unity2018UpgradeInfo.awarded = true
	g_ScriptEvent:BroadcastInLua("OnRefresbUnity2018UpgradeInfo")
	EventManager.BroadcastInternalForLua(EnumEventType.OnSigninInfoUpdate, {}) --刷新主界面福利提示
end

function LuaWelfareMgr:RequestUnity2018UpgradeAward()
	Gac2Gas.RequestUnity2018UpgradeAward()
end
------------------------------
---- END Unity2018升级指引奖励
------------------------------

------------------------------
---- BEGIN 关联手机相关
------------------------------
LuaWelfareMgr.m_RelatePhoneInfo = {}
function LuaWelfareMgr:OpenRelatePhoneWnd(areaCode, phoneNumber, bCheckedThisMonth, bChangyongshebei, bChangPhone)
	self.m_RelatePhoneInfo = {}
	self.m_RelatePhoneInfo.m_AreaCode = areaCode
	self.m_RelatePhoneInfo.m_PhoneNum = phoneNumber
	self.m_RelatePhoneInfo.m_MonthCheck = bCheckedThisMonth
	self.m_RelatePhoneInfo.m_Changyongshebei = bChangyongshebei
	self.m_RelatePhoneInfo.m_IsChangePhone = bChangPhone
	CUIManager.ShowUI(CUIResources.RelatePhoneWnd)
end

------------------------------
---- BEGIN 关联手机相关
------------------------------

--------------改版后的网易会员
--打开网易会员界面时调用
function LuaWelfareMgr.QueryNeteaseMemberWelfareInfo()
	Gac2Gas.QueryNeteaseMemberWelfareInfo()
end

--使用会员权益
function LuaWelfareMgr.RequestUseNeteaseMemberRight(rightType)
	local appId = SdkU3d.getPropStr(ConstProp.APPID)
    local devId = SdkU3d.getPropStr(ConstProp.DEVICE_ID)
    local token = SdkU3d.getPropStr(ConstProp.SESSION)
    if token == nil then token = "" end
    Gac2Gas.RequestSendNeteaseMemberTokenBegin()
    for i=1,#token,200 do
        Gac2Gas.RequestSendNeteaseMemberToken(string.sub(token, i, math.min(#token, i+199)))
    end
	Gac2Gas.RequestUseNeteaseMemberRight(appId, devId, rightType)
end
--领取会员节日礼包，这个礼包有点特殊
function LuaWelfareMgr.RequestNeteaseMemberHolidayGift(holidayId)
	Gac2Gas.RequestNeteaseMemberHolidayGift(holidayId or 0)
end
--打开会员商城
function LuaWelfareMgr.OpenNeteaseMemberMall()
	Gac2Gas.OpenNeteaseMemberMall()
end

function LuaWelfareMgr.GetAwardByRightType(rightType)
	--rightType到策划表NeteaseMember_Award的映射，目前不一致
	if rightType == EnumNeteaseMemberRightType.eBindGift then return 1 end
	if rightType == EnumNeteaseMemberRightType.eSequentGift then return 3 end
	if rightType == EnumNeteaseMemberRightType.eSingleGift then return 2 end
	if rightType == EnumNeteaseMemberRightType.eActivationGift then return 4 end
	return 0
end

function LuaWelfareMgr.HasNeedAwardedGift()
	if not LuaWelfareMgr.m_NeteaseMemberBasicInfo then return false end
	if not LuaWelfareMgr.m_NeteaseMemberBasicInfo.bUsed then return false end  --需要绑定才行
	local found = false
	for key,value in pairs(LuaWelfareMgr.m_NeteaseMemberDataList) do
		if key~=EnumNeteaseMemberRightType.eShopMall and value.hasRight and not value.isRewarded then
			found = true
			break
		end
	end
	return found
end

function LuaWelfareMgr.OnQueryNeteaseMemberInfoResult(basicInfo, dataList)
	LuaWelfareMgr.m_NeteaseMemberBasicInfo = basicInfo
	LuaWelfareMgr.m_NeteaseMemberDataList = dataList

	for key,value in pairs(basicInfo) do

	end
	for key,value in pairs(dataList) do

	end

	g_ScriptEvent:BroadcastInLua("OnQueryNeteaseMemberInfoResult")
end

function LuaWelfareMgr.UseNeteaseMemberRightSuccsss(rightType)
	if rightType == EnumNeteaseMemberRightType.eBindGame then
		LuaWelfareMgr.m_NeteaseMemberBasicInfo.bUsed = true
		LuaWelfareMgr.QueryNeteaseMemberWelfareInfo() --绑定成功后信息需要刷新一下
	elseif rightType and LuaWelfareMgr.m_NeteaseMemberDataList and LuaWelfareMgr.m_NeteaseMemberDataList[rightType] then
		LuaWelfareMgr.m_NeteaseMemberDataList[rightType].isRewarded = true
		g_MessageMgr:ShowMessage("Get_Gift_Success")
		g_ScriptEvent:BroadcastInLua("OnQueryNeteaseMemberInfoResult")
	end
end

function LuaWelfareMgr.RequestNeteaseMemberHolidayGiftSuccess(holidayId)
	if LuaWelfareMgr.m_NeteaseMemberDataList and LuaWelfareMgr.m_NeteaseMemberDataList[EnumNeteaseMemberRightType.eHolidayGift]
		and LuaWelfareMgr.m_NeteaseMemberDataList[EnumNeteaseMemberRightType.eHolidayGift].holidayId == holidayId then

		LuaWelfareMgr.m_NeteaseMemberDataList[EnumNeteaseMemberRightType.eHolidayGift].isRewarded = true
		g_MessageMgr:ShowMessage("Get_Gift_Success")
		g_ScriptEvent:BroadcastInLua("OnQueryNeteaseMemberInfoResult")
	end
end

function LuaWelfareMgr.GetNeteaseMemberViewBannerTexture()
	return LuaWelfareMgr.m_NeteaseMemberBannerTextureData
end

function LuaWelfareMgr.DownloadNeteaseMemberViewBannerInfo()
	--https://note.youdao.com/ynoteshare1/index.html?id=72746e6e01be5526e4be090c4a52268f&type=note
    Main.Inst:StartCoroutine(HTTPHelper.GetUrl(LuaWelfareMgr.m_NeteaseMemberBannerURL..LuaWelfareMgr.GetNeteaseMemberBannerID(), DelegateFactory.Action_bool_string(function (success, result)

        if not success then return end

        local dict = Json.Deserialize(result)
		if not dict then return end
		local resultDict = CommonDefs.DictGetValue_LuaCall(dict, "result")
		if CommonDefs.DictContains_LuaCall(resultDict, "content") then
			local contentDict = CommonDefs.DictGetValue_LuaCall(resultDict, "content")
			if CommonDefs.DictContains_LuaCall(contentDict, LuaWelfareMgr.GetNeteaseMemberBannerID()) then
				local infoList = CommonDefs.DictGetValue_LuaCall(contentDict, LuaWelfareMgr.GetNeteaseMemberBannerID())
				if infoList and infoList.Count>0 then
					local data = infoList[0]
					local url = CommonDefs.DictGetValue_LuaCall(data, "url")
					local src = CommonDefs.DictGetValue_LuaCall(data, "src")
					LuaWelfareMgr.DownloadNeteaseMemberViewBanner(url, src)
				end
			end
		end
    end)))
end

function LuaWelfareMgr.DownloadNeteaseMemberViewBanner(url, src)
	LuaWelfareMgr.m_NeteaseMemberBannerClickURL = url
	Main.Inst:StartCoroutine(HTTPHelper.DownloadImage(src, DelegateFactory.Action_bool_byte_Array(function (success, byteData)
		if success then
			local tex = Texture2D(2,2)
			CommonDefs.LoadImage(tex, byteData)
        	LuaWelfareMgr.m_NeteaseMemberBannerTextureData = tex
        	g_ScriptEvent:BroadcastInLua("OnNeteaseMemberBannerReady")
        end
    end)))
end

------------------------------
---- BEGIN 网易内购会员功能
------------------------------
	--[[
	接入文档 http://inner.unisdk.nie.netease.com:8083/doc/md_md_files_ios_qiyuclub.html
	    	http://docs.jifen.netease.com:9111/game_pay.html
	]]
--查询内购url
function LuaWelfareMgr.QueryNeteaseMemberInPurchaseInfo()

	--限制频率1s内只能操作一次
	if LuaWelfareMgr.m_NeteaseMemberLastQueryInPurchaseInfoTime and LuaWelfareMgr.m_NeteaseMemberLastQueryInPurchaseInfoTime > Time.realtimeSinceStartup - 1 then
		return
	end
	LuaWelfareMgr.m_NeteaseMemberLastQueryInPurchaseInfoTime = Time.realtimeSinceStartup

	local device = "pc"
	if CommonDefs.IsIOSPlatform() then
		device = "ios"
	elseif CommonDefs.IsAndroidPlatform() then
		device = "android"
	end

	local appId = SdkU3d.getPropStr(ConstProp.APPID)
    local devId = SdkU3d.getPropStr(ConstProp.DEVICE_ID)
    local token = SdkU3d.getPropStr(ConstProp.SESSION)
    if token == nil then token = "" end
    Gac2Gas.SendNeteaseMemberGouKaTokenBegin()
    for i=1,#token,200 do
        Gac2Gas.SendNeteaseMemberGouKaToken(string.sub(token, i, math.min(#token, i+199)))
    end
	Gac2Gas.QueryNeteaseMemberGouKaInfo(appId, devId, device)
end

function LuaWelfareMgr.OnQueryNeteaseMemberInPurchaseInfoResult(status, url)
	--备注 url长度是超过255的，rpc参数用了大S
	--先通知窗口，窗口如果存在则执行后续操作
	g_ScriptEvent:BroadcastInLua("OnQueryNeteaseMemberInPurchaseInfoResult", status, url)
end

function LuaWelfareMgr.OpenQyClub(url)
	local jsonStr = SafeStringFormat3("{\"methodId\": \"ntOpenQyClub\", \"url\": \"%s\"}", url)
	SdkU3d.ntExtendFunc(jsonStr)
end
------------------------------
---- END 网易内购会员功能
------------------------------

LuaWelfareMgr.SignInWndOpenContext = 0
function LuaWelfareMgr.OpenSignInWnd(context)
	if not LuaWelfareMgr.IsOpenNewQianDao then
		LuaWelfareMgr.SignInWndOpenContext = context
		CUIManager.ShowUI(CLuaUIResources.SignInWnd)
	end
end


CTopAndRightMenuWnd.m_hookOnSigninButtonClick = function (wnd, go)
	CUIManager.ShowUI(CUIResources.WelfareWnd)
end

---------------------------
---------Gas2Gac-----------
---------------------------

-- bLevelAward: ==0 表示没领 ==1 表示领取
-- function Gas2Gac.QueryNeteaseMemberWelfareResult(expired, expireTime, vipLv, bLevelAward, bConsumeFisrtLvAwardTime, bConsumeSecondLvAwardTime, money)
-- 	g_ScriptEvent:BroadcastInLua("QueryNeteaseMemberWelfareResult", expired, expireTime, vipLv, bLevelAward == 1, bConsumeFisrtLvAwardTime, bConsumeSecondLvAwardTime, money)
-- end

function Gas2Gac.RequestGetNeteaseMemberLevelAwardSuccess()
	--领取等级奖励成功
	--deprecated by server
end

function Gas2Gac.RequestGetNeteaseMemberConsumeFirstLvAwardSuccess(curTime)
	--领取第一档消费奖励成功
	--deprecated by server
end

function Gas2Gac.RequestGetNeteaseMemberConsumeSecondLvAwardSuccess(curTime)
	--领取第二档消费奖励成功
	--deprecated by server
end

function Gas2Gac.UseNeteaseMemberQualificationResult(bSuccess)
	--请求绑定的返回结果，目前成功才会返回，失败直接提示
	--deprecated by server
end

function Gas2Gac.QueryNeteaseMemberWelfareResult(hasGetLevelAward, bConsumeFirstLvAward, bConsumeSecondLvAward, consumeMoneyNum)
	--初始同步奖励领取情况和消费金额
	g_ScriptEvent:BroadcastInLua("QueryNeteaseMemberWelfareResult", hasGetLevelAward, bConsumeFirstLvAward, bConsumeSecondLvAward, consumeMoneyNum)
end

function Gas2Gac.UpdateNeteaseMemberQualificationUsedInfo(bUsed)
	--初始同步绑定状态
	g_ScriptEvent:BroadcastInLua("UpdateNeteaseMemberQualificationUsedInfo", bUsed)
end

function Gas2Gac.QueryNeteaseMemberWelfareInfoFailed()
	--以下信息同Gas2Gac.QueryNeteaseMemberInfoResult的处理保持一致
	--服务器端查询信息，如果玩家通行证不是会员可能会返回404 找不到用户的情况，针对这种情况进行如下处理：默认显示礼包
	local basicInfoTbl = {}

	basicInfoTbl.notExist = 1 --查询信息404情况，这里特殊标记一下
	basicInfoTbl.vipLv = 0
	basicInfoTbl.userId = 0
	basicInfoTbl.gameExpireDate = 0
	basicInfoTbl.superExpireDate = 0
	basicInfoTbl.gameExpireTime = 0
	basicInfoTbl.superExpireTime = 0
	basicInfoTbl.expired = true
	basicInfoTbl.expiredTime = 0
	basicInfoTbl.bUsed = false
	basicInfoTbl.useLeftTimes = 0
	basicInfoTbl.isShortCard = false

	local failMsg = LocalString.GetString("还不是会员")

	local tbl = {}

	local data = {}
	data.hasRight = false
	data.failedMsg = failMsg
	data.isRewarded = false
	tbl[EnumNeteaseMemberRightType.eBindGift] = data

	data = {}
	data.hasRight = false
	data.failedMsg = failMsg
	data.isRewarded = false
	tbl[EnumNeteaseMemberRightType.eSingleGift] = data

	data = {}
	data.hasRight = false
	data.failedMsg = failMsg
	data.isRewarded = false
	tbl[EnumNeteaseMemberRightType.eSequentGift] = data

	data = {}
	data.hasRight = false
	data.failedMsg = failMsg
	data.isRewarded = false
	tbl[EnumNeteaseMemberRightType.eActivationGift] = data


	--最后加上商城
	data = {}
	data.hasRight = false
	data.failedMsg = failMsg
	data.isRewarded = false
	tbl[EnumNeteaseMemberRightType.eShopMall] = data

	LuaWelfareMgr.OnQueryNeteaseMemberInfoResult(basicInfoTbl, tbl)
end

-- 查询网易会员信息
function Gas2Gac.QueryNeteaseMemberInfoResult(basicInfo, bindGiftInfo, singleGiftInfo, sequentGiftInfo, activationGiftInfo, holidayGiftInfo)
	--相关定义位于\L10\server\game\gas\lua\misc\NeteaseMemberMgr.lua
	--basicInfo: vipLv,userId,gameExpireDate,superExpireDate,gameExpireTime,superExpireTime,expired,expiredTime ,bUsed, useLeftTimes
	--bindGiftInfo: is_binding_gift,bind_gift_fail_msg,is_get_binding_gift
	--singleGiftInfo: is_single_gift, single_gift_fail_msg,is_get_single_gift
	--sequentGiftInfo: is_sequent_gift,sequent_gift_fail_msg,is_get_sequent_gift,keep_bind_game_count
	--activationGiftInfo: is_activation_gift,activation_gift_fail_msg, is_get_activation_gift
	--holidayGiftInfo:id2get

	local basicInfoDict = TypeAs(MsgPackImpl.unpack(basicInfo), typeof(MakeGenericClass(Dictionary, String, Object)))
	local bindGiftInfoDict = TypeAs(MsgPackImpl.unpack(bindGiftInfo), typeof(MakeGenericClass(Dictionary, String, Object)))
	local singleGiftInfoDict = TypeAs(MsgPackImpl.unpack(singleGiftInfo), typeof(MakeGenericClass(Dictionary, String, Object)))
	local sequentGiftInfoDict = TypeAs(MsgPackImpl.unpack(sequentGiftInfo), typeof(MakeGenericClass(Dictionary, String, Object)))
	local activationGiftInfoDict = TypeAs(MsgPackImpl.unpack(activationGiftInfo), typeof(MakeGenericClass(Dictionary, String, Object)))
	local holidayGiftInfoList = TypeAs(MsgPackImpl.unpack(holidayGiftInfo), typeof(MakeGenericClass(List, Object))) --可能没有数据

	if not basicInfoDict or not bindGiftInfoDict or not singleGiftInfoDict or not sequentGiftInfoDict or not activationGiftInfoDict or not holidayGiftInfoList then
		return
	end

	local basicInfoTbl = {}

	basicInfoTbl.vipLv = tonumber(CommonDefs.DictGetValue_LuaCall(basicInfoDict, "vipLv")) --这个是指心意俱乐部等级，游戏内迭代后不使用，这个不是网易会员判断依据
	basicInfoTbl.userId = tonumber(CommonDefs.DictGetValue_LuaCall(basicInfoDict, "userId"))
	basicInfoTbl.gameExpireDate = tonumber(CommonDefs.DictGetValue_LuaCall(basicInfoDict, "gameExpireDate"))
	basicInfoTbl.superExpireDate = tonumber(CommonDefs.DictGetValue_LuaCall(basicInfoDict, "superExpireDate"))
	basicInfoTbl.gameExpireTime = tonumber(CommonDefs.DictGetValue_LuaCall(basicInfoDict, "gameExpireTime"))
	basicInfoTbl.superExpireTime = tonumber(CommonDefs.DictGetValue_LuaCall(basicInfoDict, "superExpireTime"))
	basicInfoTbl.expired = CommonDefs.DictGetValue_LuaCall(basicInfoDict, "expired")
	basicInfoTbl.expiredTime = tonumber(CommonDefs.DictGetValue_LuaCall(basicInfoDict, "expiredTime"))
	basicInfoTbl.bUsed = CommonDefs.DictGetValue_LuaCall(basicInfoDict, "bUsed")
	basicInfoTbl.useLeftTimes = tonumber(CommonDefs.DictGetValue_LuaCall(basicInfoDict, "useLeftTimes"))
	basicInfoTbl.isShortCard = CommonDefs.DictGetValue_LuaCall(basicInfoDict, "isShortCard")
	local tbl = {}

	local data = {}
	data.hasRight = CommonDefs.DictGetValue_LuaCall(bindGiftInfoDict, "is_binding_gift")
	data.failedMsg = CommonDefs.DictGetValue_LuaCall(bindGiftInfoDict, "bind_gift_fail_msg")
	data.isRewarded = CommonDefs.DictGetValue_LuaCall(bindGiftInfoDict, "is_get_binding_gift")
	tbl[EnumNeteaseMemberRightType.eBindGift] = data

	data = {}
	data.hasRight = CommonDefs.DictGetValue_LuaCall(singleGiftInfoDict, "is_single_gift")
	data.failedMsg = CommonDefs.DictGetValue_LuaCall(singleGiftInfoDict, "single_gift_fail_msg")
	data.isRewarded = CommonDefs.DictGetValue_LuaCall(singleGiftInfoDict, "is_get_single_gift")
	tbl[EnumNeteaseMemberRightType.eSingleGift] = data

	data = {}
	data.hasRight = CommonDefs.DictGetValue_LuaCall(sequentGiftInfoDict, "is_sequent_gift")
	data.failedMsg = CommonDefs.DictGetValue_LuaCall(sequentGiftInfoDict, "sequent_gift_fail_msg")
	data.isRewarded = CommonDefs.DictGetValue_LuaCall(sequentGiftInfoDict, "is_get_sequent_gift")
	tbl[EnumNeteaseMemberRightType.eSequentGift] = data

	data = {}
	data.hasRight = CommonDefs.DictGetValue_LuaCall(activationGiftInfoDict, "is_activation_gift")
	data.failedMsg = CommonDefs.DictGetValue_LuaCall(activationGiftInfoDict, "activation_gift_fail_msg")
	data.isRewarded = CommonDefs.DictGetValue_LuaCall(activationGiftInfoDict, "is_get_activation_gift")
	tbl[EnumNeteaseMemberRightType.eActivationGift] = data

	--判断是不是有节日礼包
	if holidayGiftInfoList.Count>=2 then
		data = {}
		data.holidayId = tonumber(holidayGiftInfoList[0])
		data.hasRight = basicInfoTbl.bUsed and basicInfoTbl.vipLv>0
		data.failedMsg = LocalString.GetString("还不是会员")
		data.isRewarded = not holidayGiftInfoList[1]
		tbl[EnumNeteaseMemberRightType.eHolidayGift] = data
	end

	--最后加上商城
	data = {}
	data.hasRight = basicInfoTbl.bUsed and basicInfoTbl.vipLv>0
	data.failedMsg = LocalString.GetString("还不是会员")
	data.isRewarded = false
	tbl[EnumNeteaseMemberRightType.eShopMall] = data

	LuaWelfareMgr.OnQueryNeteaseMemberInfoResult(basicInfoTbl, tbl)
end

-- 使用会员权益成功
function Gas2Gac.UseNeteaseMemberRightSuccsss(rightType)
	LuaWelfareMgr.UseNeteaseMemberRightSuccsss(rightType)
end

-- 领取节日礼包成功
function Gas2Gac.RequestNeteaseMemberHolidayGiftSuccess(holidayId)
	LuaWelfareMgr.RequestNeteaseMemberHolidayGiftSuccess(holidayId)

end

-- 网易会员 内购查询
function Gas2Gac.QueryNeteaseMemberGouKaInfoResult(status, url)
	LuaWelfareMgr.OnQueryNeteaseMemberInPurchaseInfoResult(status, url)
end

--从旧的LuaWelfareWnd.lua中搬过来，旧代码已经没在使用，移除避免误用
local PlayerSettings = import "L10.Game.PlayerSettings"
local CWelfareMgr = import "L10.UI.CWelfareMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CPropertyItem = import "L10.Game.CPropertyItem"
local CommonDefs = import "L10.Game.CommonDefs"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChargeWnd = import "L10.UI.CChargeWnd"
CWelfareMgr.m_ShowMonthCardAlert_CS2LuaHook = function ()
    if CClientMainPlayer.Inst ~= nil then
        local info = CClientMainPlayer.Inst.ItemProp.MonthCard
        if info == nil then
            return false
        end
        local leftDays = CChargeWnd.MonthCardLeftDay(info)
        if leftDays <= 0 then
            if info.BuyTime == 0 or info.TotalCount == 0 then
                return false
            end
            local now = CServerTimeMgr.Inst:GetZone8Time()
            local buyTime = info.BuyTime
            local lastTime = buyTime + info.TotalCount * 24 * 3600
            local buyDateTime = CServerTimeMgr.ConvertTimeStampToZone8Time(buyTime)
            local lastDateTime = buyDateTime:AddDays(info.TotalCount)

            local val = PlayerSettings.GetMonthCardLastCheckTime("MonthCard")
            local lastCheckTime = 0
            local default
            default, lastCheckTime = System.Double.TryParse(val)
            if lastCheckTime > lastTime then
                lastTime = math.floor(lastCheckTime)
            end

            local diffTime = CommonDefs.op_Subtraction_DateTime_DateTime(now, lastDateTime)
            if not CPropertyItem.IsSamePeriod(lastTime, "d") and diffTime.Days <= CWelfareMgr.monthCardAlertLimitDays then
                return true
            end
        end
    end
    return false
end

function Gas2Gac.ZhouNianShopStatusQueryResult(bOpen, bShowNotify, remainSeconds)
    -- print("ZhouNianShopStatusQueryResult",bOpen, bShowNotify, remainSeconds)
    g_ScriptEvent:BroadcastInLua("ZhouNianShopStatusQueryResult",bOpen, bShowNotify, remainSeconds)
end

function Gas2Gac.SyncGetLatestDaShenLogToken()
    LuaWelfareMgr.m_AlreadyReadContentProvider = false
    LuaWelfareMgr.BindDaShenViaContentProvider()
end

LuaWelfareMgr.Carnival2020TodayCannotBuy = nil
LuaWelfareMgr.Carnival2020AlreadyBuy = nil

function Gas2Gac.ReplyDieKeVipTicketSellInfo(restStockData, isBuy)
  local list = MsgPackImpl.unpack(restStockData)
  local index = 1
  local totalRestNum = 0
  local goodsId1 = JiaNianHua_2020Sale.GetData(5).GoodsId
  local goodsId2 = JiaNianHua_2020Sale.GetData(6).GoodsId
  LuaWelfareMgr.Carnival2020TodayRestNum = {}
  for i=0, list.Count-1, 2 do
    local goodsId = tonumber(list[i])
    local restStock = tonumber(list[i+1])

    if restStock > 0 then
      LuaWelfareMgr.Carnival2020TodayRestNum[goodsId] = restStock
    end

    if goodsId == goodsId1 or goodsId == goodsId2 then
        totalRestNum = totalRestNum + restStock
    end

  end

  if totalRestNum > 0 then
    LuaWelfareMgr.Carnival2020TodayCannotBuy = false
  else
    LuaWelfareMgr.Carnival2020TodayCannotBuy = true
  end

  if isBuy > 0 then
    LuaWelfareMgr.Carnival2020AlreadyBuy = true
  else
    LuaWelfareMgr.Carnival2020AlreadyBuy = false
  end

  g_ScriptEvent:BroadcastInLua("UpdateCarnival2020Data")
end

LuaWelfareMgr.Carnival2021TicketSellList = nil
function LuaWelfareMgr.Carnival2021TicketSellInfo(TicketSellInfo)
	LuaWelfareMgr.Carnival2021TicketSellList = TicketSellInfo
	g_ScriptEvent:BroadcastInLua("UpdateCarnival2021TicketData")
end

function Gas2Gac.SubmitJiaNianHua2020PersonalInfoSuccess()

  CUIManager.ShowUI(CLuaUIResources.Carnival2020VipTicketWnd)
  g_ScriptEvent:BroadcastInLua("UpdateCarnival2020Data")
end


--#region 福利迭代 && 大月卡

LuaWelfareMgr.IsOpenNewQianDao = false  -- 开启新签到 (obsolete)
LuaWelfareMgr.IsOpenBigMonthCard = true -- 开启福利迭代+大月卡

LuaWelfareMgr.BigMonthCardId = 43
LuaWelfareMgr.MultipleExpTabBtn = nil
LuaWelfareMgr.MainWelfareTabBtn = nil
LuaWelfareMgr.LvUpAwardTabBtn = nil
LuaWelfareMgr.OnlineTimeTabBtn = nil

LuaWelfareMgr.CheckMainWelfare = false

-- nowTime: 	现在的时间，采用服务器推送的值是方便命令改时间测试
-- todayTimes:	今日签到次数，0表示未签到
-- totalTimes：	累计连续签到次数
-- nextBuQianTime：	可补签的日期（表示最近可补签的那天，补签后会重新计算），为0表示没有可补签的
-- usedBuQianTimes：本月已经补签的次数
-- totalBuQianTimes：本月拥有的可补签次数
-- x/7 这个x是用totalTimes%7得来，如果余数为0，若当天已签到，则显示7/7
LuaWelfareMgr.ServerDateTime = nil
function LuaWelfareMgr:SendQianDaoInfo_New(nowTime, todayTimes, totalTimes, nextBuQianTime, usedBuQianTimes, totalBuQianTimes)
	print("SendQianDaoInfo_New", nowTime, todayTimes, totalTimes, nextBuQianTime, usedBuQianTimes, totalBuQianTimes)

	local now = CServerTimeMgr.ConvertTimeStampToZone8Time(nowTime)
	self.ServerDateTime = now
	CSigninMgr.Inst.year = now.Year
	CSigninMgr.Inst.month = now.Month
	CSigninMgr.Inst.date = now.Day
	CSigninMgr.Inst.hasSignedToday = todayTimes >= 1
	CSigninMgr.Inst.signedDays = totalTimes -- 旧签到:本月累计签到次数; 新签到:累计连续签到次数
	--CSigninMgr.Inst.totalTimes = totalTimes -- 本月一共可以签到多少次（旧签到好像也没用到
	CSigninMgr.Inst.retroactiveTotalTimes = totalBuQianTimes -- 总补签次数
	CSigninMgr.Inst.retroactiveUsedTimes = usedBuQianTimes -- 已补签次数
	CSigninMgr.Inst.nextBuQianTime = nextBuQianTime

	EventManager.BroadcastInternalForLua(EnumEventType.OnSigninInfoUpdate, {})
end

CSigninMgr.m_hookOnMainPlayerCreated = function(this)
	if LuaWelfareMgr.IsOpenNewQianDao then
		Gac2Gas.QueryQianDaoInfo_New()
	else
		Gac2Gas.QueryQianDaoInfo()
	end
end

function LuaWelfareMgr:ShowQianDaoWnd_New()
	EventManager.Broadcast(EnumEventType.SigninAvailable)
    if not (CSigninMgr.Inst.huiliuDay == 1 and not CSigninMgr.Inst.huiliuAwardGetToday) then
        if not CUIManager.IsLoaded(CUIResources.WelfareWnd) then
            CUIManager.ShowUI(CUIResources.WelfareWnd)
		end
	end
end

function LuaWelfareMgr:ShowQianDaoRefreshFlag_New()
	CSigninMgr.Inst.hasSignedToday = false
end

function LuaWelfareMgr:OpenNewQianDao()
	LuaWelfareMgr.IsOpenNewQianDao = true
	CUIManager.CloseUI(CUIResources.WelfareWnd)
	CUIManager.CloseUI(CLuaUIResources.SignInWnd)
	CUIManager.ShowUI(CUIResources.WelfareWnd)
end

function LuaWelfareMgr:GetRandomByWeight(t, weights)
	local sum = 0
    for i = 1, #weights do
        sum = sum + weights[i]
    end
	local dayBegin = CServerTimeMgr.Inst:GetDayBegin(self.ServerDateTime)
	math.randomseed(tonumber(SafeStringFormat3("%04d%02d%02d", dayBegin.Year, dayBegin.Month, dayBegin.Day)))
    local compareWeight = math.random(1, sum)
    local weightIndex = 1
    while sum > 0 do
        sum = sum - weights[weightIndex]
        if sum < compareWeight then
            return t[weightIndex]
        end
        weightIndex = weightIndex + 1
    end
end

function LuaWelfareMgr:GetDailySuitable()
	local t = {}
	local weights = {}
	QianDao_DailySuitable.Foreach(function(k, v)
		t[k] = v.Content
		weights[k] = v.Weight
	end)
	return self:GetRandomByWeight(t, weights)
end

function LuaWelfareMgr:GetDailyLuckTendency()
	local t = {}
	local weights = {}
	QianDao_DailyLuckTendency.Foreach(function(k, v)
		t[k] = v.Content
		weights[k] = v.Weight
	end)
	return self:GetRandomByWeight(t, weights)
end

--仿照服务器端CServerPlayer:HasBigMonthCard 目前用于聊天记录上传下载的判断
function LuaWelfareMgr:HasBigMonthCard()
    local info = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BigMonthCardInfo or nil
    if not info then return false end
    return self:MonthCardLeftDay(info) > 0
end

function LuaWelfareMgr:MonthCardLeftDay(info) -- 兼容普遍月卡和大月卡
	local buytime = CServerTimeMgr.ConvertTimeStampToZone8Time(info.BuyTime)
	local now = CServerTimeMgr.Inst:GetZone8Time()
	local beginOfTheBuyDay = DateTime(buytime.Year, buytime.Month, buytime.Day, 0, 0, 0)
	local nowOfTheDay = DateTime(now.Year, now.Month, now.Day, 0, 0, 0)
	return beginOfTheBuyDay:AddDays(info.TotalCount):Subtract(nowOfTheDay).Days
end

function LuaWelfareMgr.BuyBigMonthCardForMyself()
	local data = Charge_Charge.GetData(LuaWelfareMgr.BigMonthCardId)
	if not data then
		return
	end
	CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(data.PID))
end

function LuaWelfareMgr.BuyBigMonthCardForFriend()
	if not CSwitchMgr.EnableBuyMonthCardForFriend then
		g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
		return
	end
	CTradeSendMgr.Inst:ShowMonthCardGivenWnd(LuaWelfareMgr.BigMonthCardId)
end

CChargeMgr.m_hookBuyMonthCardForMyself = function(this)
	local data = Charge_Charge.GetData(1)
	if not data then
		return
	end
	CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(data.PID))
end

CChargeMgr.m_hookBuyMonthCardForFriend = function(this)
	if not CSwitchMgr.EnableBuyMonthCardForFriend then
		g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
		return
	end
	CTradeSendMgr.Inst:ShowMonthCardGivenWnd(1)
end

function LuaWelfareMgr:CheckNewMainWelfareAlert()
	if not CClientMainPlayer.Inst then return end
	local info = CClientMainPlayer.Inst.ItemProp.MonthCard
	local info2 = CClientMainPlayer.Inst.ItemProp.BigMonthCardInfo 
	local leftDay = info and CChargeWnd.MonthCardLeftDay(info)
	local leftDay2 = info2 and LuaWelfareMgr:MonthCardLeftDay(info2)
	return (not CSigninMgr.Inst.hasSignedToday 
		or (leftDay and leftDay > 0 and not CPropertyItem.IsSamePeriod(info.LastReturnTime, "d"))
		or (leftDay2 and leftDay2 > 0 and not CPropertyItem.IsSamePeriod(info2.LastReturnTime, "d")))
end

function LuaWelfareMgr:CheckMultipleExpAlert()
	return CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel >= MultipleExp_GameSetting.GetData().Receive_Grade and not CPropertyItem.IsSamePeriod(CSigninMgr.Inst.doubleExpReceiveTime, "w")
end

local MONTHCARD_TYPE_TO_INFO = {
	MonthCard = function() return CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.MonthCard end,
	BigMonthCard = function() return CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BigMonthCardInfo end,
}

local MONTHCARD_BINARY_OP = function(op, left, right, val)
	left = tonumber(left)
	right = tonumber(right)
	val = tonumber(val)
	if op == "<" then
		return left < right
	elseif op == ">" then
		return left > right
	elseif op == "<=" then
		return left <= right
	elseif op == ">=" then
		return left >= right
	elseif op == "%" then
		return left % right == val
	else
		return left == right
	end
end

local MONTHCARD_HINT_EVENTS = {
	TRUE = function() return true end,
	HasExpired = function(params)
		local info = MONTHCARD_TYPE_TO_INFO[params[1]]()
		if info and info.BuyTime > 0 then
			local leftDay = LuaWelfareMgr:MonthCardLeftDay(info)
			if leftDay <= 0 then
				if not params[2] then return true end
				leftDay = -leftDay
				local op, rightVal = string.match(params[2], "([<>=%%]*)%s*(%d+)")
				return MONTHCARD_BINARY_OP(op, leftDay, rightVal, params[3])
			end
		end
		return false
	end,
	WillExpire = function(params)
		local info = MONTHCARD_TYPE_TO_INFO[params[1]]()
		if info and info.BuyTime > 0 then
			local leftDay = LuaWelfareMgr:MonthCardLeftDay(info)
			if leftDay > 0 then
				local op, rightVal = string.match(params[2], "([<>=%%]*)%s*(%d+)")
				return MONTHCARD_BINARY_OP(op, leftDay, rightVal, params[3])
			end
		end
		return false
	end,
	NotExpired = function(params)
		local info = MONTHCARD_TYPE_TO_INFO[params[1]]()
		if info and info.BuyTime > 0 then
			local leftDay = LuaWelfareMgr:MonthCardLeftDay(info)
			return leftDay > 0 
		end
		return false
	end,
	Purchased = function(params)
		local info = MONTHCARD_TYPE_TO_INFO[params[1]]()
		if info then
			return info.BuyTime > 0
		end
		return false
	end,
	NotPurchased = function(params)
		local info = MONTHCARD_TYPE_TO_INFO[params[1]]()
		if info then
			return info.BuyTime == 0
		end
		return false
	end,
}

function LuaWelfareMgr:GetMonthCardHintPlayList()
	local hints = {}
	Charge_MonthCardHint.Foreach(function(k, v)
        if v.Event and v.Event ~= "" then
			local ok = true
            for func in string.gmatch(v.Event, "[^;]+") do
                local _, idx, funcName = string.find(func, "(.+)%(")
                funcName = string.gsub(funcName, "^%s*(.-)%s*$", "%1")
                local args = {}
                for arg in string.gmatch(string.sub(func, idx), "[^,%s%(%)]+") do
                    table.insert(args, arg)
                end
				if not MONTHCARD_HINT_EVENTS[funcName](args) then
                	ok = false
					break
				end
            end
			if ok then
				table.insert(hints, {id = k, priority = v.Priority, content = CUICommonDef.TranslateToNGUIText(v.Content), interval = v.Interval, tick = 0})
			end
        end
	end)
	table.sort(hints, function(a, b)
		if a.priority == b.priority then
			return a.id < b.id
		end
		return a.priority > b.priority
	end)
	return hints
end

local BEGIN_YEAR = 1901
local NUMBER_YEAR = 199
--前四位：闰月 中间13位：13个大小月（1大月30天，0小月29天）  后七位：春节日期 0-4位代表日，5-6代表月
local LUNAR_YEARS = {
    0x04AE53,0x0A5748,0x5526BD,0x0D2650,0x0D9544,
    0x46AAB9,0x056A4D,0x09AD42,0x24AEB6,0x04AE4A, --1901-1910

    0x6A4DBE,0x0A4D52,0x0D2546,0x5D52BA,0x0B544E,
    0x0D6A43,0x296D37,0x095B4B,0x749BC1,0x049754, --1911-1920

    0x0A4B4C,0x5B25BC,0x06A550,0x06D445,0x4ADAB8,
    0x02B64D,0x095742,0x2497B7,0x04974A,0x664B3E, --1921-1930

    0x0D4A51,0x0EA546,0x56D4BA,0x05AD4E,0x02B644,
    0x393738,0x092E4B,0x7C96BF,0x0C9553,0x0D4A48, --1931-1940

    0x6DA53B,0x0B554F,0x056A45,0x4AADB9,0x025D4D,
    0x092D42,0x2C95B6,0x0A954A,0x7B4ABD,0x06CA51, --1941-1950

    0x0B5546,0x555ABB,0x04DA4E,0x0A5B43,0x352BB8,
    0x052B4C,0x8A953F,0x0E9552,0x06AA48,0x7AD53C, --1951-1960

    0x0AB54F,0x04B645,0x4A5739,0x0A574D,0x052642,
    0x3E9335,0x0D9549,0x75AABE,0x056A51,0x096D46, --1961-1970

    0x54AEBB,0x04AD4F,0x0A4D43,0x4D26B7,0x0D254B,
    0x8D52BF,0x0B5452,0x0B6A47,0x696D3C,0x095B50, --1971-1980

    0x049B45,0x4A4BB9,0x0A4B4D,0xAB25C2,0x06A554,
    0x06D449,0x6ADA3D,0x0AB651,0x093746,0x5497BB, --1981-1990

    0x04974F,0x064B44,0x36A537,0x0EA54A,0x86B2BF,
    0x05AC53,0x0AB647,0x5936BC,0x092E50,0x0C9645, --1991-2000

    0x4D4AB8,0x0D4A4C,0x0DA541,0x25AAB6,0x056A49,
    0x7AADBD,0x025D52,0x092D47,0x5C95BA,0x0A954E, --2001-2010

    0x0B4A43,0x4B5537,0x0AD54A,0x955ABF,0x04BA53,
    0x0A5B48,0x652BBC,0x052B50,0x0A9345,0x4A9739, --2011-2020

    0x02AB4C,0x055AC1,0x2AD936,0x03694A,0x6752BD,
    0x0392D1,0x0325C6,0x564BBA,0x0655CD,0x02AD43, --2021-2030

    0x356B37,0x05B4CB,0xBBA93F,0x05A953,0x0592C8,--
    0x6D25BC,0x0525CF,0x0255C4,0x52ADB8,0x02D6CC, --2031-2040

    0x05B541,0x2DA936,0x06C94A,0x7E92BE,0x0692D1,
    0x052AC6,0x5A56BA,0x025B4E,0x02DAC2,0x36D537, --2041-2050

    0x0764CB,0x8F4941,0x074953,0x069348,0x652B3C,
    0x052BCF,0x026B44,0x436AB8,0x03AACC,0x03A4C2, --2051-2060

    0x3749B5,0x0349C9,0x7A95BD,0x0295D1,0x052DC5,
    0x5AAD3A,0x02B54E,0x05B2C3,0x4BA537,0x05A54B, --2061-2070

    0x8D4ABF,0x054AD3,0x0296C7,0x6556BB,0x055ACF,
    0x02D545,0x45D2B8,0x06D2CC,0x06A542,0x3E4AB6, --2071-2080

    0x069349,0x7729BD,0x06AA51,0x0AD546,0x54DABA,
    0x04B64E,0x0A5743,0x452738,0x0D264A,0x8E933E, --2081-2090

    0x0D5252,0x0DAA47,0x66B53B,0x056D4F,0x04AE45,
    0x4A4EB9,0x0A4D4C,0x0D1541,0x2D92B5 --2091-2099
}

--计算这个公历日期是一年中的第几天
function LuaWelfareMgr:DayOfSolarYear(year, month, day)
    local NORMAL_YDAY = {1,32,60,91,121,152,182,213,244,274,305,335}
    local LEAP_YDAY = {1,32,61,92,122,153,183,214,245,275,306,336}
    local year_yday = NORMAL_YDAY
    if year % 4 == 0 then
        if year % 100 ~= 0 then
            year_yday = LEAP_YDAY
        end
        if year % 400 == 0 then
            year_yday = LEAP_YDAY
        end
    end
    return year_yday[month] + day - 1
end

function LuaWelfareMgr:GetLunarCalendar(solar_year, solar_monty, solar_day)
    local lunar_date = {year = solar_year, month = 0, day = 0, leap = false}
    if solar_year <= BEGIN_YEAR or solar_year > BEGIN_YEAR + NUMBER_YEAR- 1 then
        return lunar_date
    end
    local year_index = solar_year - BEGIN_YEAR + 1

    --计算春节的公历日期
    local spring_ny_month = bit.rshift(bit.band(LUNAR_YEARS[year_index], 0x60), 5)
    local spring_ny_day = bit.band(LUNAR_YEARS[year_index], 0x1f)

    --计算今天是公历年的第几天
    local today_solar_yd = self:DayOfSolarYear(solar_year, solar_monty, solar_day)
    --计算春节是公历年的第几天
    local spring_ny_yd = self:DayOfSolarYear(solar_year, spring_ny_month, spring_ny_day)
    --计算今天是农历年的第几天
    local today_lunar_yd = today_solar_yd - spring_ny_yd + 1
    if today_lunar_yd <= 0 then
        year_index = year_index - 1
        lunar_date.year = lunar_date.year - 1
        if year_index <= 0 then
            return lunar_date
        end
        --上一年农历春节
        spring_ny_month = bit.rshift(bit.band(LUNAR_YEARS[year_index],0x60), 5)
        spring_ny_day = bit.band(LUNAR_YEARS[year_index], 0x1f)
        --计算上一年春节是第几天
        spring_ny_yd = self:DayOfSolarYear(lunar_date.year, spring_ny_month, spring_ny_day)
        --计算上一年共几天
        local year_total_day = self:DayOfSolarYear(lunar_date.year, 12, 31)
        --上一年农历年的第几天
        today_lunar_yd = today_solar_yd + year_total_day - spring_ny_yd + 1
    end

    local lunar_month = 1
    while lunar_month <= 13 do
        local month_day = 29
        --大月
        if bit.band(bit.rshift(LUNAR_YEARS[year_index], (6 + lunar_month)), 0x1) == 1 then
            month_day = 30
        end
        if (today_lunar_yd <= month_day) then
            break
        else
            today_lunar_yd = today_lunar_yd - month_day
        end
        lunar_month = lunar_month + 1
    end
    lunar_date.day = today_lunar_yd

    --閏月
    local leap_month = bit.band(bit.rshift(LUNAR_YEARS[year_index],20), 0xf)
    if leap_month > 0 and leap_month < lunar_month then
        lunar_month = lunar_month - 1

        if lunar_month == leap_month then
            lunar_date.leap = true
        end
    end
    assert(leap_month <= 12)
    lunar_date.month = lunar_month
    return lunar_date
end

--#endregion