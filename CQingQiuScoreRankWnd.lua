-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CommonFightData = import "L10.UI.CommonFightData"
local CQingQiuMgr = import "L10.UI.CQingQiuMgr"
local CQingQiuScoreRankWnd = import "L10.UI.CQingQiuScoreRankWnd"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumStatType = import "L10.Game.EnumStatType"
local EventManager = import "EventManager"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CQingQiuScoreRankWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_ScoreRankBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.OnMasterButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.m_DamageRankBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.OnMasterButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.m_HealRankBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.OnMasterButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.m_CloseObj).onClick = DelegateFactory.VoidDelegate(function (go) 
        this:Close()
    end)
    EventManager.AddListener(EnumEventType.ReplyQingQiuFightDataFinished, MakeDelegateFromCSFunction(this.ReplyFightData, Action0, this))
end
CQingQiuScoreRankWnd.m_Init_CS2LuaHook = function (this) 
    this:OnMasterButtonClick(this.m_ScoreRankBtn.gameObject)
    Gac2Gas.QingQiuQueryFightDataRank()
    if CQingQiuMgr.Inst.m_PlayState ~= EnumQingQiuPlayState_lua.Part_1 then
        this.m_DamageRankBtn.gameObject:SetActive(true)
        this.m_HealRankBtn.gameObject:SetActive(true)
    end
end
CQingQiuScoreRankWnd.m_OnMasterButtonClick_CS2LuaHook = function (this, go) 
    this.m_CurSelectedObj = go
    if go == this.m_ScoreRankBtn.gameObject then
        this.m_FightDetailView.gameObject:SetActive(false)
        this.m_ScoreObj:SetActive(true)
    else
        this.m_FightDetailView.gameObject:SetActive(true)
        this.m_ScoreObj:SetActive(false)
        if go == this.m_DamageRankBtn.gameObject then
            CommonDefs.ListSort1(CQingQiuMgr.Inst.m_FightData, typeof(CommonFightData), DelegateFactory.Comparison_CommonFightData(function (data1, data2) 
                return NumberCompareTo(data2.m_PlayerDamage, data1.m_PlayerDamage)
            end))
            this.m_FightDetailView:Init(EnumStatType.eDps)
        elseif go == this.m_HealRankBtn.gameObject then
            CommonDefs.ListSort1(CQingQiuMgr.Inst.m_FightData, typeof(CommonFightData), DelegateFactory.Comparison_CommonFightData(function (data1, data2) 
                return NumberCompareTo(data2.m_PlayerHeal, data1.m_PlayerHeal)
            end))
            this.m_FightDetailView:Init(EnumStatType.eHeal)
        end
    end
    this.m_ScoreRankBtn.Selected = go == this.m_ScoreRankBtn.gameObject
    this.m_DamageRankBtn.Selected = go == this.m_DamageRankBtn.gameObject
    this.m_HealRankBtn.Selected = go == this.m_HealRankBtn.gameObject
end
