

local Zhuangshiwu_Zhuangshiwu = import "L10.Game.Zhuangshiwu_Zhuangshiwu"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local SoundManager = import "SoundManager"

function Gas2Gac.PlayerPlayDrum(ftid, x, y, z)
	local zswdata = ftid and Zhuangshiwu_Zhuangshiwu.GetData(ftid)
	if zswdata then
		local sound = zswdata.Sound
		if sound and sound~="" then
			y = y + CRenderScene.Inst:SampleLogicHeight(x, z)
			SoundManager.Inst:PlayOneShot(sound, Vector3(x, y, z), nil, 0)
		end
	end
end

g_YarkPick={}
function Gas2Gac.SyncYarkPickStr(yarkPickBytes)
	local list = MsgPackImpl.unpack(yarkPickBytes)
	if not list then return end

	local t = {}
	for i=1,list.Count,4 do
		table.insert( t,{
			engineId = tonumber(list[i-1]),
			pickTempId = tonumber(list[i]),
			x = tonumber(list[i+1]),
			y = tonumber(list[i+2])
		} )
	end
	g_YarkPick = t
    g_ScriptEvent:BroadcastInLua("SyncYarkPick",t)
end