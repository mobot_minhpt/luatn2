-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerAppearanceInfoWnd = import "L10.UI.CPlayerAppearanceInfoWnd"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local EnumClass = import "L10.Game.EnumClass"
local EnumGender = import "L10.Game.EnumGender"
local GenericGesture = import "L10.Engine.GenericGesture"
local GestureRecognizer = import "L10.Engine.GestureRecognizer"
local GestureType = import "L10.Engine.GestureType"
local Screen = import "UnityEngine.Screen"
local Vector3 = import "UnityEngine.Vector3"
CPlayerAppearanceInfoWnd.m_Init_CS2LuaHook = function (this) 
    if LuaPlayerInfoMgr.ShowApperance and LuaPlayerInfoMgr.PlayerApperanceProp then
        this.playerId = LuaPlayerInfoMgr.PlayerApperanceProp.id
        this.playerCls = CommonDefs.ConvertIntToEnum(typeof(EnumClass), LuaPlayerInfoMgr.PlayerApperanceProp.cls)
        this.playerGender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), LuaPlayerInfoMgr.PlayerApperanceProp.gender)
        this.appearanceData = LuaPlayerInfoMgr.PlayerApperanceProp.appearanceProp
        this.titleLabel.text = LuaPlayerInfoMgr.PlayerApperanceProp.name
    elseif CPlayerInfoMgr.PlayerInfo ~= nil then
        this.playerId = CPlayerInfoMgr.PlayerInfo.id
        this.playerCls = CommonDefs.ConvertIntToEnum(typeof(EnumClass), CPlayerInfoMgr.PlayerInfo.cls)
        this.playerGender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), CPlayerInfoMgr.PlayerInfo.gender)
        this.appearanceData = CPlayerInfoMgr.PlayerInfo.appearanceProp
        this.titleLabel.text = CPlayerInfoMgr.PlayerInfo.name
    else
        this.playerId = 0
        this.playerCls = 0
        this.playerGender = 0
        this.appearanceData = nil
        this.titleLabel.text = ""
    end
    this.modelTexture.mainTexture = nil
    --初始不显示默认角色模型

    if this.identifier == nil then
        this.identifier = System.String.Format("__{0}__", this:GetInstanceID())
    end
    this.stage:SetActive(true)
    if this.appearanceData ~= nil then
        this.modelTexture.mainTexture = CUIManager.CreateModelTexture(this.identifier, this, 180, 0.05, -1, 4.66, false, true, 1)
        this.curPos = this.originPos
        this.neareastPos = Vector3(0, this:GetCameraHeightForPreviewHeadFashion(this.playerCls, this.playerGender), 1.7)
        this.totalLen = (CommonDefs.op_Subtraction_Vector3_Vector3(this.neareastPos, this.originPos)).magnitude
    end
end
CPlayerAppearanceInfoWnd.m_OnEnable_CS2LuaHook = function (this) 
    if CPlayerAppearanceInfoWnd.s_EnableHidePlayerInfoWnd then
        CUIManager.HideUIByChangeLayer(CUIResources.PlayerInfoWnd, false, true)
    end
    --UIEventListener.Get(leftToRightRotationBtn).onPress += this.OnLeftToRightRotationButtonPress;
    --UIEventListener.Get(rightToLeftRotationBtn).onPress += this.OnRightToLeftRotationButtonPress;

    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchIn, MakeDelegateFromCSFunction(this.OnPinchIn, MakeGenericClass(Action1, GenericGesture), this))
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchOut, MakeDelegateFromCSFunction(this.OnPinchOut, MakeGenericClass(Action1, GenericGesture), this))
end
CPlayerAppearanceInfoWnd.m_OnDisable_CS2LuaHook = function (this) 
    if CPlayerAppearanceInfoWnd.s_EnableHidePlayerInfoWnd then
        CUIManager.ShowUIByChangeLayer(CUIResources.PlayerInfoWnd, false, true)
    end
    this.leftToRightRotationBtnPressed = false
    this.rightToLeftRotationBtnPressed = false
    --UIEventListener.Get(leftToRightRotationBtn).onPress -= this.OnLeftToRightRotationButtonPress;
    --UIEventListener.Get(rightToLeftRotationBtn).onPress -= this.OnRightToLeftRotationButtonPress;

    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchIn, MakeDelegateFromCSFunction(this.OnPinchIn, MakeGenericClass(Action1, GenericGesture), this))
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchOut, MakeDelegateFromCSFunction(this.OnPinchOut, MakeGenericClass(Action1, GenericGesture), this))
    LuaPlayerInfoMgr.ShowApperance = false
end
CPlayerAppearanceInfoWnd.m_OnDragModel_CS2LuaHook = function (this, go, delta) 
    if this.identifier == nil then
        return
    end
    if not this:EnableDrag() then
        return
    end
    local deltaVal = - delta.x / Screen.width * 360
    this.curRotation = this.curRotation + deltaVal
    CUIManager.SetModelRotation(this.identifier, this.curRotation)
end
CPlayerAppearanceInfoWnd.m_OnPinchIn_CS2LuaHook = function (this, gesture) 

    this.curPos = CommonDefs.op_Addition_Vector3_Vector3(this.curPos, CommonDefs.op_Multiply_Vector3_Single((CommonDefs.op_Subtraction_Vector3_Vector3(this.originPos, this.curPos)).normalized, math.min(Vector3.Distance(this.originPos, this.curPos), (gesture.deltaPinch / 512))))
    CUIManager.SetModelPosition(this.identifier, this.curPos)
    this.stage:SetActive(Vector3.Distance(this.originPos, this.curPos) < 0.1)
end
