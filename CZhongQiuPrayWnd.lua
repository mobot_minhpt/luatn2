-- Auto Generated!!
local AlignType = import "CPlayerInfoMgr+AlignType"
local CButton = import "L10.UI.CButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CIMMgr = import "L10.Game.CIMMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CZhongQiuMgr = import "L10.Game.CZhongQiuMgr"
local CZhongQiuPrayWnd = import "L10.UI.CZhongQiuPrayWnd"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local Gac2Gas = import "L10.Game.Gac2Gas"
local L10 = import "L10"
local UIEventListener = import "UIEventListener"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CZhongQiuPrayWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_AddButton).onClick = MakeDelegateFromCSFunction(this.OnAddButtonClicked, VoidDelegate, this)
    UIEventListener.Get(this.m_ApplyButton).onClick = MakeDelegateFromCSFunction(this.OnApplyButtonClicked, VoidDelegate, this)
    UIEventListener.Get(this.m_CloseButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.m_Portrait.gameObject).onClick = MakeDelegateFromCSFunction(this.OnPortraitClicked, VoidDelegate, this)
    UIEventListener.Get(this.m_LeftButton.gameObject).onClick = MakeDelegateFromCSFunction(this.OnLeftButtonClicked, VoidDelegate, this)
    UIEventListener.Get(this.m_RightButton.gameObject).onClick = MakeDelegateFromCSFunction(this.OnRightButtonClicked, VoidDelegate, this)
end
CZhongQiuPrayWnd.m_Init_CS2LuaHook = function (this) 
    if CZhongQiuMgr.Inst.SendZhongQiuPray then
        if CClientMainPlayer.Inst == nil then
            this:Close()
            return
        end
        this.m_SenderName.text = CClientMainPlayer.Inst.Name
        this.m_Input.characterLimit = CZhongQiuPrayWnd.QIFU_CHAR_LIMIT
        this.m_Input.enabled = true
        this.m_LeftButton:SetActive(false)
        this.m_RightButton:SetActive(false)
    else
        this.m_Input.enabled = false
        this.m_ApplyButton:SetActive(false)
        this.m_AddButton:SetActive(false)

        if CZhongQiuMgr.Inst.PrayInfos == nil or CZhongQiuMgr.Inst.PrayInfos.Count == 0 then
            this:Close()
            return
        end
        this:InitPray()
    end
end
CZhongQiuPrayWnd.m_InitPray_CS2LuaHook = function (this) 
    local counter = CZhongQiuMgr.Inst.PrayInfosCounter
    if counter >= 0 and counter < CZhongQiuMgr.Inst.PrayInfos.Count then
        local prayInfo = CZhongQiuMgr.Inst.PrayInfos[counter]
        this.m_SenderPlayerId = prayInfo.senderId
        this.m_ReveiverName.text = prayInfo.receiverName
        this.m_SenderName.text = prayInfo.senderName
        this.m_Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(prayInfo.senderClass, prayInfo.senderGender, -1), false)
        this.m_Input.value = prayInfo.qiFuContent
    end

    if counter > 0 then
        this.m_LeftButton:SetActive(true)
    else
        this.m_LeftButton:SetActive(false)
    end
    if counter < CZhongQiuMgr.Inst.PrayInfos.Count - 1 then
        this.m_RightButton:SetActive(true)
    else
        this.m_RightButton:SetActive(false)
    end
end
CZhongQiuPrayWnd.m_OnLeftButtonClicked_CS2LuaHook = function (this, go) 
    if CZhongQiuMgr.Inst.PrayInfosCounter - 1 >= 0 then
        local default = CZhongQiuMgr.Inst
        default.PrayInfosCounter = default.PrayInfosCounter - 1
        this:InitPray()
    end
end
CZhongQiuPrayWnd.m_OnRightButtonClicked_CS2LuaHook = function (this, go) 
    if CZhongQiuMgr.Inst.PrayInfosCounter + 1 < CZhongQiuMgr.Inst.PrayInfos.Count then
        local default = CZhongQiuMgr.Inst
        default.PrayInfosCounter = default.PrayInfosCounter + 1
        this:InitPray()
    end
end
CZhongQiuPrayWnd.m_OnApplyButtonClicked_CS2LuaHook = function (this, go) 
    -- 接收方不能为空
    if this.m_ReceiverPlayerId == 0 then
        g_MessageMgr:ShowMessage("SEND_PLAYER_IS_EMPTY")
        return
    end
    -- 祈祷不能为空
    if System.String.IsNullOrEmpty(this.m_Input.value) then
        g_MessageMgr:ShowMessage("WISHCONTENT_IS_EMPTY")
        return
    end

    if CommonDefs.StringLength(this.m_Input.value) > CZhongQiuPrayWnd.QIFU_CHAR_LIMIT then
        g_MessageMgr:ShowMessage("WISHCONTENT_IS_TOO_LONG")
        return
    end
    -- 过滤
    local ret = CWordFilterMgr.Inst:DoFilterOnSend(this.m_Input.value, nil, nil, true)
    local msg = ret.msg
    if msg == nil then
        return
    end

    CommonDefs.GetComponent_GameObject_Type(this.m_ApplyButton, typeof(CButton)).Enabled = false
    Gac2Gas.RequestSendYueGongQiFu(this.m_ReceiverPlayerId, CZhongQiuMgr.Inst.PrayItemPlace, CZhongQiuMgr.Inst.PrayItemPos, CZhongQiuMgr.Inst.PrayItemId, msg)
end
CZhongQiuPrayWnd.m_OnZhongQiuSelectPlayer_CS2LuaHook = function (this, playerId, playerName, portraitName) 
    this.m_ReveiverName.text = playerName
    this.m_ReceiverPlayerId = playerId

    -- 隐藏加号，处理头像
    this.m_AddButton:SetActive(false)
    this.m_Portrait:LoadNPCPortrait(portraitName, false)
    CUIManager.CloseUI("ZhongQiuSelectFriendWnd")
end
CZhongQiuPrayWnd.m_OnSelectPlayer_CS2LuaHook = function (this, playerId, playerName) 
    this.m_ReveiverName.text = playerName
    this.m_ReceiverPlayerId = playerId

    -- 隐藏加号，处理头像
    this.m_AddButton:SetActive(false)
    local portraitName = L10.Game.Constants.DefaultNPCPortrait
    local info = CIMMgr.Inst:GetBasicInfo(playerId)
    if info ~= nil then
        portraitName = L10.UI.CUICommonDef.GetPortraitName(info.Class, info.Gender, info.Expression)
    end
    this.m_Portrait:LoadNPCPortrait(portraitName, false)
    CUIManager.CloseUI("ZhongQiuSelectFriendWnd")
end
CZhongQiuPrayWnd.m_OnPortraitClicked_CS2LuaHook = function (this, go) 
    if CZhongQiuMgr.Inst.SendZhongQiuPray then
        CPlayerInfoMgr.ShowPlayerPopupMenu(math.floor(this.m_ReceiverPlayerId), EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
    else
        CPlayerInfoMgr.ShowPlayerPopupMenu(math.floor(this.m_SenderPlayerId), EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
    end
end
