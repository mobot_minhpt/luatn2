
local GameObject = import "UnityEngine.GameObject"

local DelegateFactory  = import "DelegateFactory"
local TweenPosition = import "TweenPosition"
local UITexture = import "UITexture"

LuaInsectTaskFindLingShouWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaInsectTaskFindLingShouWnd, "HeYe1", "HeYe1", GameObject)
RegistChildComponent(LuaInsectTaskFindLingShouWnd, "HeYe2", "HeYe2", GameObject)
RegistChildComponent(LuaInsectTaskFindLingShouWnd, "HeYe3", "HeYe3", GameObject)
RegistChildComponent(LuaInsectTaskFindLingShouWnd, "HeYe4", "HeYe4", GameObject)
RegistChildComponent(LuaInsectTaskFindLingShouWnd, "HeYe5", "HeYe5", GameObject)
RegistChildComponent(LuaInsectTaskFindLingShouWnd, "HeYe6", "HeYe6", GameObject)
--RegistChildComponent(LuaInsectTaskFindLingShouWnd, "HeYe7", "HeYe7", GameObject)
RegistChildComponent(LuaInsectTaskFindLingShouWnd, "FoundMsg", "FoundMsg", GameObject)
RegistChildComponent(LuaInsectTaskFindLingShouWnd, "NotFoundMsg", "NotFoundMsg", GameObject)
RegistChildComponent(LuaInsectTaskFindLingShouWnd, "FinishMsg", "FinishMsg", GameObject)
RegistChildComponent(LuaInsectTaskFindLingShouWnd, "CountLabel", "CountLabel", UILabel)
RegistChildComponent(LuaInsectTaskFindLingShouWnd, "TimeLabel", "TimeLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaInsectTaskFindLingShouWnd,"m_HeyeTbl")
RegistClassMember(LuaInsectTaskFindLingShouWnd,"m_HideIndex")
RegistClassMember(LuaInsectTaskFindLingShouWnd,"m_CanClick")
RegistClassMember(LuaInsectTaskFindLingShouWnd,"m_FindCount")
RegistClassMember(LuaInsectTaskFindLingShouWnd,"m_NoiseType")
RegistClassMember(LuaInsectTaskFindLingShouWnd,"m_ResetHeyeTick")
RegistClassMember(LuaInsectTaskFindLingShouWnd,"m_ChangeHideIndexTick")
RegistClassMember(LuaInsectTaskFindLingShouWnd,"m_CountDownTick")
RegistClassMember(LuaInsectTaskFindLingShouWnd,"m_LastTime")
function LuaInsectTaskFindLingShouWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_HeyeTbl = {
        self.HeYe1,self.HeYe2,self.HeYe3,self.HeYe4,self.HeYe5,self.HeYe6,
    }
    self.m_HideIndex = 1
    self.m_CanClick = true
    self.m_FindCount = 0
    self.m_NoiseType = 1
    self.m_LastTime = LuaInviteNpcMgr.WanpiLingShouTime * 60
end

function LuaInsectTaskFindLingShouWnd:Init()
    self.CountLabel.text = SafeStringFormat3(LocalString.GetString("%d/%d次"),self.m_FindCount,3)
    local min = math.floor(self.m_LastTime/60)
    local sec = self.m_LastTime - min*60
    self.TimeLabel.text = SafeStringFormat3(LocalString.GetString("剩余:%02d:%02d"),min,sec)
    self:RandomHideIndex()
    self.m_ChangeHideIndexTick = RegisterTick(function ()
        self:RandomHideIndex()
    end,10000)
    self.m_RandomNoiseTick = RegisterTick(function ()
        self:MakeNoise()
    end,5000)

    for i=1,6,1 do
        local item = self.m_HeyeTbl[i].transform
        local heye = item:Find("Heye").gameObject
        UIEventListener.Get(heye).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnClickHeye(i)
        end)
        local tip = item:Find("Tip")
        tip.gameObject:SetActive(false)
    end

    self.m_CountDownTick = RegisterTickWithDuration(function ()
        self.m_LastTime = self.m_LastTime - 1
        if self.m_LastTime <= 0 then
            self.TimeLabel.text = LocalString.GetString("剩余:00:00")
            self.m_CanClick = false
            --self:UnRegisterAllTicks()
            Gac2Gas.WanPiLingShouTaskFail(CLuaTaskMgr.m_WanPiLingShou_TaskId)
            g_MessageMgr:ShowMessage("WanPiLingShou_TaskFail")
            CUIManager.CloseUI(CLuaUIResources.InsectTaskFindLingShouWnd)
        else
            local min = math.floor(self.m_LastTime/60)
            local sec = self.m_LastTime - min*60
            self.TimeLabel.text = SafeStringFormat3(LocalString.GetString("剩余:%02d:%02d"),min,sec)
        end
    end,1000,LuaInviteNpcMgr.WanpiLingShouTime * 60*1000)
end

function LuaInsectTaskFindLingShouWnd:RandomHideIndex()
    self.m_HideIndex = math.random(1,6)
end

function LuaInsectTaskFindLingShouWnd:MakeNoise()
    self.m_NoiseType = math.random(1,2)
    local item = self.m_HeyeTbl[self.m_HideIndex].transform
    if self.m_HideNoiseTick then
        UnRegisterTick(self.m_HideNoiseTick)
        if self.m_CurTipGo then
            self.m_CurTipGo:SetActive(false)
        end
    end
    if self.m_NoiseType == 1 then--tip
        self.m_CurTipGo = item:Find("Tip").gameObject
        self.m_CurTipGo:SetActive(true)
        self.m_CurWaveGo = item:Find("WaveFx").gameObject
        self.m_CurWaveGo:SetActive(true)     
        self.m_HideNoiseTick = RegisterTickWithDuration(function ()
            if self.m_CurTipGo then
                self.m_CurTipGo:SetActive(false)
            end
            if self.m_CurWaveGo then
                self.m_CurWaveGo:SetActive(false)
            end
        end,1000,1000)
    elseif self.m_NoiseType == 2 then--shake
        self.m_CurTipGo = item:Find("Heye/ShakeFx").gameObject
        self.m_CurTipGo:SetActive(true)
        self.m_HeyeTexture = item:Find("Heye"):GetComponent(typeof(UITexture))
        --self.m_HeyeTexture.enabled = false
        self.m_CurWaveGo = item:Find("WaveFx").gameObject
        self.m_CurWaveGo:SetActive(true)
        self.m_HideNoiseTick = RegisterTickWithDuration(function ()
            if self.m_CurTipGo then
                self.m_CurTipGo:SetActive(false)
            end
            --[[if self.m_HeyeTexture then
                self.m_HeyeTexture.enabled = true
            end--]]
            if self.m_CurWaveGo then
                self.m_CurWaveGo:SetActive(false)
            end
        end,2000,2000)
    end
end

function LuaInsectTaskFindLingShouWnd:OnClickHeye(index)
    if not self.m_CanClick then return end
    self.m_CanClick = false
    local item = self.m_HeyeTbl[index].transform
    local tweenPos = item:Find("Heye"):GetComponent(typeof(TweenPosition))
    tweenPos.enabled = true
    tweenPos:PlayForward()
    local gugu = item:Find("Gugu")
    gugu.gameObject:SetActive(index==self.m_HideIndex)

    self.m_ResetHeyeTick = RegisterTickWithDuration(function ()
        tweenPos:ResetToBeginning()
        gugu.gameObject:SetActive(false)
        if self.m_FindCount == 3 then
            Gac2Gas.FinishClientTaskEvent(CLuaTaskMgr.m_WanPiLingShou_TaskId,"SectNpcWanPiLingShou",MsgPackImpl.pack(empty))
            CUIManager.CloseUI(CLuaUIResources.InsectTaskFindLingShouWnd)
        else
            self.m_CanClick = true
        end
    end,2000,2000)

    if index == self.m_HideIndex then
        self.m_FindCount = self.m_FindCount + 1
        self.CountLabel.text = SafeStringFormat3(LocalString.GetString("%d/%d次"),self.m_FindCount,3)
        if self.m_FindCount < 3 then
            CLuaBatDialogWnd.ShowWnd(162)
        else
            CLuaBatDialogWnd.ShowWnd(164)
        end
        if self.m_ChangeHideIndexTick ~= nil then
            UnRegisterTick(self.m_ChangeHideIndexTick)
            self:RandomHideIndex()
            self.m_ChangeHideIndexTick = RegisterTick(function ()
                self:RandomHideIndex()
            end,10000)
        end
        if self.m_RandomNoiseTick ~= nil then
            UnRegisterTick(self.m_RandomNoiseTick)
            self.m_RandomNoiseTick = RegisterTick(function ()
                self:MakeNoise()
            end,5000)
        end
    else
        CLuaBatDialogWnd.ShowWnd(163)
    end
end

function LuaInsectTaskFindLingShouWnd:UnRegisterAllTicks()
    if self.m_ResetHeyeTick ~= nil then
        UnRegisterTick(self.m_ResetHeyeTick)
    end
    if self.m_ChangeHideIndexTick ~= nil then
        UnRegisterTick(self.m_ChangeHideIndexTick)
    end
    if self.m_RandomNoiseTick ~= nil then
        UnRegisterTick(self.m_RandomNoiseTick)
    end
    if self.m_HideNoiseTick ~= nil then
        UnRegisterTick(self.m_HideNoiseTick)
    end
    if self.m_CountDownTick ~= nil then
        UnRegisterTick(self.m_CountDownTick)
    end
end

function LuaInsectTaskFindLingShouWnd:OnDisable()
    self:UnRegisterAllTicks()
end
--@region UIEvent

--@endregion UIEvent
