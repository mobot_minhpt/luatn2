local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnButton = import "L10.UI.QnButton"
local CUICommonDef = import "L10.UI.CUICommonDef"
local GameObject = import "UnityEngine.GameObject"
local UITable = import "UITable"
local UISlider = import "UISlider"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"
local CUIFx = import "L10.UI.CUIFx"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local Animation = import "UnityEngine.Animation"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Ease = import "DG.Tweening.Ease"
local Vector4 = import "UnityEngine.Vector4"

LuaHangZhouScrollWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHangZhouScrollWnd, "RightButton", "RightButton", QnButton)
RegistChildComponent(LuaHangZhouScrollWnd, "LeftButton", "LeftButton", QnButton)
RegistChildComponent(LuaHangZhouScrollWnd, "Table", "Table", UITable)
RegistChildComponent(LuaHangZhouScrollWnd, "ProgressBar", "ProgressBar", UISlider)
RegistChildComponent(LuaHangZhouScrollWnd, "Cur", "Cur", UILabel)
RegistChildComponent(LuaHangZhouScrollWnd, "Max", "Max", UILabel)
RegistChildComponent(LuaHangZhouScrollWnd, "AwardTemplate", "AwardTemplate", GameObject)
RegistChildComponent(LuaHangZhouScrollWnd, "ScrollTemplate", "ScrollTemplate", GameObject)
RegistChildComponent(LuaHangZhouScrollWnd, "ItemCell", "ItemCell", GameObject)
RegistChildComponent(LuaHangZhouScrollWnd, "UnlockButton", "UnlockButton", CButton)
RegistChildComponent(LuaHangZhouScrollWnd, "RuleBtn", "RuleBtn", QnButton)
RegistChildComponent(LuaHangZhouScrollWnd, "TitleLable", "TitleLable", UILabel)
RegistChildComponent(LuaHangZhouScrollWnd, "AwardRoot", "AwardRoot", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaHangZhouScrollWnd, "m_ScrollView")
RegistClassMember(LuaHangZhouScrollWnd, "m_ScrollData")
RegistClassMember(LuaHangZhouScrollWnd, "m_RewardData")
RegistClassMember(LuaHangZhouScrollWnd, "m_ScrollViewData")
RegistClassMember(LuaHangZhouScrollWnd, "m_ProgressViewData")
RegistClassMember(LuaHangZhouScrollWnd, "m_MaxScroll")
RegistClassMember(LuaHangZhouScrollWnd, "m_UnLock")
RegistClassMember(LuaHangZhouScrollWnd, "m_UnLockItem")
RegistClassMember(LuaHangZhouScrollWnd, "m_UnLockScrollTick")
RegistClassMember(LuaHangZhouScrollWnd, "m_ShowTextureTick")
RegistClassMember(LuaHangZhouScrollWnd, "m_TickEndFunc")
RegistClassMember(LuaHangZhouScrollWnd, "m_CurTickIndex")
RegistClassMember(LuaHangZhouScrollWnd, "m_InitLookAtIndex")
function LuaHangZhouScrollWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.ItemCell.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnItemCellClick()
	end)

	
	UIEventListener.Get(self.UnlockButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUnlockButtonClick()
	end)


	
	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)
	self.m_ShowTextureTick = nil


    --@endregion EventBind end
	self.m_ScrollView = self.transform:Find("Anchor/NormalPage/ScrollView"):GetComponent(typeof(UIScrollView))
	self.ItemCell.transform:Find("costLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("拥有")
	self.m_ScrollData = nil
	self.m_ScrollViewData = nil
	self.m_ProgressViewData = nil
	self.m_UnLockItem = nil
	self.m_TickEndFunc = nil
	self.m_CurTickIndex = 0
	self.m_MaxScroll = 0
	self.m_RewardData = 0
	self.m_UnLock = 0
	self.m_InitLookAtIndex = 0
	self.m_UnLockScrollTick = {}
	self.AwardTemplate.gameObject:SetActive(false)
	self.ScrollTemplate.gameObject:SetActive(false)
end

function LuaHangZhouScrollWnd:Init()
	self.TitleLable.text = g_MessageMgr:FormatMessage("YaYunHui2023_HangZhouScroll_Title")
	if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eAsianGames2023Data) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eAsianGames2023Data)
        local tab = g_MessagePack.unpack(playDataU.Data.Data)
        self.m_ScrollData = tab and tab[EnumAsianGames2023DataType.eHuiJuan] or {}
		self.m_RewardData = tab and tab[EnumAsianGames2023DataType.eHuiJuanRewardStatus] or 0
    end
	self:InitScroll()
	self:InitProgress()
	self:InitUseItem()
	if self.m_ShowTextureTick then 
		UnRegisterTick(self.m_ShowTextureTick) 
		self.m_ShowTextureTick = nil
	end
	self.m_ShowTextureTick = RegisterTickOnce(function()
		if self.m_ScrollViewData then
			for k,v in pairs(self.m_ScrollViewData) do
				LuaTweenUtils.TweenAlpha(v.texture.texture, 0,1,0.3)
			end
		end
	end,600)
end

function LuaHangZhouScrollWnd:InitScroll()
	self.m_MaxScroll = AsianGames2023_HangZhouScroll.GetDataCount()
	self.m_ScrollViewData = {}
	self.m_UnLock = 0
	Extensions.RemoveAllChildren(self.Table.transform)
	AsianGames2023_HangZhouScroll.Foreach(function(k,v)
		local index = k
		local obj = CUICommonDef.AddChild(self.Table.gameObject,self.ScrollTemplate.gameObject)
		obj.gameObject:SetActive(true)
		local title = obj.transform:Find("Title"):GetComponent(typeof(UILabel))
		title.text = LocalString.GetString("？？？")
		local ScrollTexture = obj.transform:Find("Texture"):GetComponent(typeof(CUITexture))
		ScrollTexture:LoadMaterial(v.IconPath)
		ScrollTexture.alpha = 0
		local desc = obj.transform:Find("Desc"):GetComponent(typeof(UILabel))
		desc.text = v.Description
		desc.alpha = 0
		local progress = obj.transform:Find("ProgressLabel"):GetComponent(typeof(UILabel))
		progress.text = SafeStringFormat3("%d%%",0)
		local award = obj.transform:Find("Award").gameObject
		award:GetComponent(typeof(UITexture)).alpha = 0.5
		local CUIFx = award.transform:Find("CUIFx"):GetComponent(typeof(CUIFx))
		self:AddAwardFx(CUIFx,award.transform:Find("Sprite"))
		CUIFx.gameObject:SetActive(false)
		local animation = obj:GetComponent(typeof(Animation))
		UIEventListener.Get(award.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnScrollAwardClick(index)
		end)
		local mask = obj.transform:Find("Mask"):GetComponent(typeof(UITexture))
		mask.fillAmount = 1
		local maskEnd = mask.transform:Find("End"):GetComponent(typeof(UITexture))
		local maskStart = mask.transform:Find("Start"):GetComponent(typeof(UITexture))
		maskEnd.alpha = 0
		maskStart.alpha = 0
		LuaTweenUtils.TweenAlpha(maskEnd, 0,1,2)
		LuaTweenUtils.TweenAlpha(maskStart, 0,1,2)
		self.m_ScrollViewData[k] = {
			view = obj,
			title = title,
			name = v.Name,
			descView = desc,
			progressView = progress,
			awardView = award,
			maskView = mask,
			maskEnd = maskEnd,
			maskStart = maskStart,
			curProgress = 0,
			ani = animation,
			texture = ScrollTexture,
		}
		if self.m_ScrollData and self.m_ScrollData[index] then
			self:UpdateOneScrollView(index,self.m_ScrollData[index],true,nil)
		end
	end)
	self:UpdateUnlockProgress()
	self.Table:Reposition()
	if self.m_InitLookAtIndex >= 3 then
		local toIndex = math.min(self.m_InitLookAtIndex + 1,self.m_MaxScroll)
		CUICommonDef.SetFullyVisible(self.m_ScrollViewData[toIndex].view.gameObject,self.m_ScrollView)
	end
end

function LuaHangZhouScrollWnd:AddAwardFx(CUIFx,target)
	CUIFx:DestroyFx()
	LuaTweenUtils.DOKill(CUIFx.transform, true)
	local b = NGUIMath.CalculateRelativeWidgetBounds(target.transform)
	local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y , b.size.x , b.size.y ), 1, true)
	CUIFx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
	CUIFx.transform.localPosition = waypoints[0]
	LuaTweenUtils.DOLuaLocalPath(CUIFx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
end

function LuaHangZhouScrollWnd:InitProgress()
	if self.m_MaxScroll <= 0 then return end
	self.m_ProgressViewData = {}
	local awardList = AsianGames2023_Setting.GetData().HuiJuanAwards
	local maxLength = self.ProgressBar.transform:Find("Sprite"):GetComponent(typeof(UISprite)).width
	Extensions.RemoveAllChildren(self.AwardRoot.transform)
	for i = 0,awardList.Length - 1 do
		local index = i + 1
		local obj = CUICommonDef.AddChild(self.AwardRoot.gameObject,self.AwardTemplate)
		obj.gameObject:SetActive(true)
		local num = awardList[i][0]
		local itemId = awardList[i][1]
		local itemData = Item_Item.GetData(itemId)
		local node = obj.transform:Find("AwardItemCell/node").gameObject
		obj.transform:Find("AwardItemCell/IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)
		local mask = obj.transform:Find("AwardItemCell/MaskSprite").gameObject
		local CUIFx = obj.transform:Find("Other/CUIFx"):GetComponent(typeof(CUIFx))
		local lock = obj.transform:Find("Other/lock").gameObject
		local numLabel = obj.transform:Find("Other/AwardLabel"):GetComponent(typeof(UILabel))
		CUIFx:DestroyFx()
		self:AddAwardFx(CUIFx,node)
		mask.gameObject:SetActive(false)
		CUIFx.gameObject:SetActive(false)
		lock.gameObject:SetActive(true)
		numLabel.text = tostring(num)
		
		UIEventListener.Get(node.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnProgressAwardClick(index)
		end)
		self.m_ProgressViewData[index] = {
			num = num,
			id = itemId,
			maskView = mask,
			lockView = lock,
			CUIFx = CUIFx,
		}
		LuaUtils.SetLocalPosition(obj.transform, maxLength * (num / self.m_MaxScroll), 0, 0)
		if self.m_RewardData then
			self:UpdateOnePregressView(index)
		end
	end
end

function LuaHangZhouScrollWnd:OnScrollAwardClick(index)
	local progressData = self.m_ScrollData and self.m_ScrollData[index] or 0
	if progressData == 100 then
		Gac2Gas.AsianGames2023GetHuiJuanUnlockAward(index)
	else
		local id = AsianGames2023_Setting.GetData().HuiJuanUnlockAward
		CItemInfoMgr.ShowLinkItemTemplateInfo(id, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
	end
end

function LuaHangZhouScrollWnd:OnProgressAwardClick(index)
	if not self.m_ProgressViewData or not self.m_ProgressViewData[index] then return end
	local view = self.m_ProgressViewData[index]
	local data = self.m_RewardData or 0
	local indexBit = bit.lshift(1, index - 1)
	local isGet = bit.band(data ,bit.lshift(1, index - 1)) > 0
	local canGet = view.num <= self.m_UnLock
	if not isGet and canGet then
		Gac2Gas.AsianGames2023GetHuiJuanCollectAward(index)
	else
		CItemInfoMgr.ShowLinkItemTemplateInfo(view.id, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
	end
end

function LuaHangZhouScrollWnd:UpdateOneScrollView(index,data,isInit,endFunc)
	if not self.m_ScrollViewData or not self.m_ScrollViewData[index] then return end
	local view = self.m_ScrollViewData[index]
	local progress = math.max(0,math.min(data,100))
	if data <= 100 and self.m_InitLookAtIndex <= 0 then
		self.m_InitLookAtIndex = index
	end
	if progress < 50 then
		view.title.text = LocalString.GetString("？？？")
	else
		view.title.text = view.name
	end
	if isInit or view.curProgress == data or (view.curProgress == 100 and data == 101) then
		view.progressView.text = SafeStringFormat3("%d%%",progress)
		view.maskView.fillAmount = (100 - progress) / 100
		view.maskView.gameObject:SetActive(data < 100)
		if data >= 100 then view.progressView.gameObject:SetActive(false) end
		if data < 100 then
			local endrotation = - 3.6 * data  + 90
			Extensions.SetLocalRotationZ(view.maskEnd.transform, endrotation)
		end
		view.awardView.gameObject:SetActive(data <= 100)
		view.awardView:GetComponent(typeof(UITexture)).alpha = data == 100 and 1 or 0.5

		view.awardView.transform:Find("CUIFx").gameObject:SetActive(data == 100)
		view.descView.alpha = data >= 100 and 1 or 0
		if endFunc then endFunc() end
	else
		self:StartScrollViewTick(index,view.curProgress,data,endFunc)
	end

	if data >= 100 then self.m_UnLock = self.m_UnLock + 1 end
	view.curProgress = data
end

function LuaHangZhouScrollWnd:UpdateOnePregressView(index)
	if not self.m_ProgressViewData or not self.m_ProgressViewData[index] then return end
	local view = self.m_ProgressViewData[index]
	local data = self.m_RewardData or 0
	local isGet = bit.band(data ,bit.lshift(1, index - 1)) > 0  
	view.maskView.gameObject:SetActive(isGet)
	view.lockView.gameObject:SetActive(not isGet and view.num > self.m_UnLock)
	view.CUIFx.gameObject:SetActive(not isGet and view.num <= self.m_UnLock)
end

function LuaHangZhouScrollWnd:UpdateScrollView()
	if not self.m_ScrollViewData then self:InitScroll() end
	self.m_UnLock = 0
	self.m_CurTickIndex = 0
	if not self.m_TickEndFunc then
		self.m_TickEndFunc = function()
			local scolltoIndex = 0
			if self.m_CurTickIndex < #self.m_ScrollViewData then
				self.m_CurTickIndex = self.m_CurTickIndex + 1
				if self.m_ScrollViewData[self.m_CurTickIndex].curProgress ~= self.m_ScrollData[self.m_CurTickIndex] and self.m_ScrollViewData[self.m_CurTickIndex].curProgress < 100 then
					scolltoIndex = self.m_CurTickIndex
				end
				if scolltoIndex > 0 then
					local toIndex = math.min(scolltoIndex + 1,self.m_MaxScroll)
					CUICommonDef.SetFullyVisible(self.m_ScrollViewData[toIndex].view.gameObject,self.m_ScrollView)
				end
				self:UpdateOneScrollView(self.m_CurTickIndex,self.m_ScrollData[self.m_CurTickIndex],false,self.m_TickEndFunc)
				self:UpdateUnlockProgress()
				self:UpdateProgress()
			end
		end
	end

	self.m_TickEndFunc()
	-- for i = 1,#self.m_ScrollViewData do
	-- 	if self.m_ScrollViewData[i].curProgress ~= self.m_ScrollData[i] and self.m_ScrollViewData[i].curProgress < 100 then
	-- 		scolltoIndex = i
	-- 	end
	-- 	self:UpdateOneScrollView(i,self.m_ScrollData[i],false)
	-- end
	-- if scolltoIndex > 0 then
	-- 	CUICommonDef.SetFullyVisible(self.m_ScrollViewData[scolltoIndex].view.gameObject,self.m_ScrollView)
	-- end
	-- self:UpdateUnlockProgress()
end

function LuaHangZhouScrollWnd:UpdateProgress()
	if not self.m_ProgressViewData then self:InitProgress() end
	for i = 1,#self.m_ProgressViewData do
		self:UpdateOnePregressView(i)
	end
end

function LuaHangZhouScrollWnd:StartScrollViewTick(index,startNum,endNum,endFunc)
	if not self.m_UnLockScrollTick then self.m_UnLockScrollTick = {} end
	if not self.m_ScrollViewData or not self.m_ScrollViewData[index] then return end
	local view = self.m_ScrollViewData[index]
	if self.m_UnLockScrollTick[index] then UnRegisterTick(self.m_UnLockScrollTick[index]) self.m_UnLockScrollTick[index] = nil end
	local interval = 50
	local totalTime = 1000
	local totalCount = totalTime / interval
	local cnt = 1
	local startP = math.max(0,startNum)
	local endP = math.min(100,endNum)
	local awardTexture = view.awardView:GetComponent(typeof(UITexture))
	local fx = view.awardView.transform:Find("CUIFx").gameObject
	view.awardView.gameObject:SetActive(startNum < 100)
	awardTexture.alpha = startNum < 100 and 0.5 or 1
	view.descView.alpha = startNum < 100 and 0 or 1
	fx.gameObject:SetActive(startNum == 100)
	view.maskView.gameObject:SetActive(startNum < 100)

	self.m_UnLockScrollTick[index] = RegisterTickWithDuration(function()
		local curProgress = math.min(endP,math.max(startP,startP + cnt * (endP - startP) / totalCount))
		view.progressView.text = SafeStringFormat3("%d%%",curProgress)
		view.maskView.fillAmount = (100 - curProgress) / 100
		local endrotation = - 3.6 * curProgress  + 90
		Extensions.SetLocalRotationZ(view.maskEnd.transform, endrotation)
		cnt = cnt + 1
		if cnt > totalCount then
			if startNum < 100 and endNum == 100 then
				view.progressView.gameObject:SetActive(false)
				view.ani:Play("hangzhouscrollwnd_jiesuo")
				awardTexture.alpha = 1
				view.descView.alpha = 1
				fx.gameObject:SetActive(true)
				view.maskView.gameObject:SetActive(false)
			end
			if endFunc then
				endFunc()
			end
		end
	end,interval,totalTime)
end


function LuaHangZhouScrollWnd:UpdateUnlockProgress()
	if self.m_MaxScroll > 0 then 
		self.Cur.text = tostring(self.m_UnLock)
		self.Max.text = SafeStringFormat3("/%d",self.m_MaxScroll)
		self.Max:ResetAnchors()
		self.ProgressBar.value = self.m_UnLock / self.m_MaxScroll
		local notUnlockAll = self.m_UnLock < self.m_MaxScroll	-- 没有全部解锁
		self.TitleLable.gameObject:SetActive(notUnlockAll)
		self.ItemCell.gameObject:SetActive(notUnlockAll)
		self.UnlockButton.gameObject:SetActive(notUnlockAll)
		self.RuleBtn.gameObject:SetActive(notUnlockAll)
	end
end

function LuaHangZhouScrollWnd:OnUpdatePlayDataWithKey(argv)
	if argv[0] ~= EnumTempPlayDataKey_lua.eAsianGames2023Data then return end
	if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eAsianGames2023Data) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eAsianGames2023Data)
        local tab = g_MessagePack.unpack(playDataU.Data.Data)
        self.m_ScrollData = tab and tab[EnumAsianGames2023DataType.eHuiJuan] or {}
		self.m_RewardData = tab and tab[EnumAsianGames2023DataType.eHuiJuanRewardStatus] or 0
		self:UpdateScrollView()
    end
end

function LuaHangZhouScrollWnd:InitUseItem()
	local itemID = AsianGames2023_Setting.GetData().HuiJuanUnlockItemId
	local ItemData = Item_Item.GetData(itemID)
	local IconTexture = self.ItemCell.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local NumLabel =  self.ItemCell.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
	local mask = self.ItemCell.transform:Find("mask").gameObject
	local needNum = AsianGames2023_Setting.GetData().HuiJuanUnlockCost
	self.m_UnLockItem = {
		id = itemID,
		NumLabel = NumLabel,
		mask = mask,
		curNum = 0,
		needNum = needNum,
	}
	mask.gameObject:SetActive(false)
	IconTexture:LoadMaterial(ItemData.Icon)
	NumLabel.text = nil

	self:RefreshItem()
end


function LuaHangZhouScrollWnd:RefreshItem()
	if not self.m_UnLockItem then return end
	local itemID = self.m_UnLockItem.id
	local curNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemID)
	local needNum = self.m_UnLockItem.needNum
	self.m_UnLockItem.curNum = curNum
	self.m_UnLockItem.mask.gameObject:SetActive(curNum < needNum)
	self.m_UnLockItem.NumLabel.text = nil
	if curNum <= 0 then
		self.m_UnLockItem.NumLabel.text = nil
	else
		self.m_UnLockItem.NumLabel.text = tostring(curNum)
	end
end

function LuaHangZhouScrollWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey",self,"OnUpdatePlayDataWithKey")
	g_ScriptEvent:AddListener("SendItem", self, "RefreshItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "RefreshItem")
end

function LuaHangZhouScrollWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey",self,"OnUpdatePlayDataWithKey")
	g_ScriptEvent:RemoveListener("SendItem", self, "RefreshItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "RefreshItem")
	for k,v in pairs(self.m_UnLockScrollTick) do
		if v then UnRegisterTick(v) v = nil end
	end
	if self.m_ShowTextureTick then 
		UnRegisterTick(self.m_ShowTextureTick) 
		self.m_ShowTextureTick = nil
	end
	self.m_UnLockScrollTick = nil
end



--@region UIEvent

function LuaHangZhouScrollWnd:OnItemCellClick()
	if not self.m_UnLockItem then return end
	local curNum = self.m_UnLockItem.curNum
	local needNum = self.m_UnLockItem.needNum
	local id = self.m_UnLockItem.id
	if curNum < needNum then
		CItemAccessListMgr.Inst:ShowItemAccessInfo(id, false, self.ItemCell.transform, AlignType.Top)
	else
		CItemInfoMgr.ShowLinkItemTemplateInfo(id, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
	end
end

function LuaHangZhouScrollWnd:OnUnlockButtonClick()
	if not self.m_UnLockItem then return end
	local curNum = self.m_UnLockItem.curNum
	local needNum = self.m_UnLockItem.needNum
	local id = self.m_UnLockItem.id
	if curNum < needNum then
		g_MessageMgr:ShowMessage("ASIAN_HUI_JUAN_UNLOCK_ITEM_NOT_ENOUGH")
		CItemAccessListMgr.Inst:ShowItemAccessInfo(id, false, self.ItemCell.transform, AlignType.Top)
	else
		Gac2Gas.AsianGames2023UnlockHuiJuan()
	end
end

function LuaHangZhouScrollWnd:OnRuleBtnClick()
	g_MessageMgr:ShowMessage("YaYunHui2023_HangZhouScroll_Tip")
end

--@endregion UIEvent

