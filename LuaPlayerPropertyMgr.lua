local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local Vector3 = import "UnityEngine.Vector3"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CCommonItem=import "L10.Game.CCommonItem"
local CItemMgr = import "L10.Game.CItemMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local SendChatMsgResult = import "L10.Game.SendChatMsgResult"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
LuaPlayerPropertyMgr = {}

LuaPlayerPropertyMgr.SkillLevelDetailWndPosX = -378 -- 技能额外等级加成界面的X位置

LuaPlayerPropertyMgr.PropertyGuide = true       -- 属性引导功能总开关
LuaPlayerPropertyMgr.PropertyOpenCross = true   -- 跨服比较开关
LuaPlayerPropertyMgr.TemplatePropertyGuideID = 0 -- 临时属性ID
LuaPlayerPropertyMgr.PropertyGuideID = 0        -- 当前属性ID
LuaPlayerPropertyMgr.PlayerCompareData = nil    -- 属性对比的玩家ID
LuaPlayerPropertyMgr.CurSkillID = 0             -- 当前技能的技能ID
LuaPlayerPropertyMgr.CurSkillClassID = 0        -- 当前技能的classID
LuaPlayerPropertyMgr.ShareInfo = nil            --          
LuaPlayerPropertyMgr.PropertyGuideLevel = 0 

LuaPlayerPropertyMgr.queryRank = false    -- 是否刷新排行榜界面
LuaPlayerPropertyMgr.PropertyTypeList = nil     -- 在线对比属性列表
LuaPlayerPropertyMgr.PlayerPropData = nil       -- 玩家属性详析结果
LuaPlayerPropertyMgr.PropData = nil
LuaPlayerPropertyMgr.OppositePropData = nil     -- 对比玩家属性详析结果
LuaPlayerPropertyMgr.PropRankData = nil         -- 排行榜查询结果
LuaPlayerPropertyMgr.SkillLevelData = nil       -- 技能等级查询结果
LuaPlayerPropertyMgr.PlayerIsShareProp = false  -- 玩家是否分享当前属性
LuaPlayerPropertyMgr.IsCross = false            -- 是否跨服
LuaPlayerPropertyMgr.EquipmentData = nil
LuaPlayerPropertyMgr.IsOpenCrossServer = false  -- 是否显示跨服排行按钮
-- 请求查看属性详析
function LuaPlayerPropertyMgr:ShowPlayerPropertyDetailWnd(PropertyID)
    local data = PropGuide_Property.GetData(PropertyID)
    if not data then return end
    self.TemplatePropertyGuideID = PropertyID
    if data.ChildTab then
        local propid = data.ChildTab[0]
        for i = 0 , data.ChildTab.Length - 1 do
            if data.ChildTab[i] == PropertyID then
                propid = data.ChildTab[i]
            end
        end
        if propid ~= PropertyID then
            self.TemplatePropertyGuideID = propid
            data = PropGuide_Property.GetData(propid)
        end
    end
    if data then
        local type = CommonDefs.ConvertIntToEnum(typeof(EPlayerFightProp), data.FightPropID)
        local typeString = CommonDefs.Convert_ToString(type)
        -- 请求服务器数据
        Gac2Gas.PlayerQueryPropShareRecord(typeString)   -- 分享情况
        Gac2Gas.PlayerRequestQueryPropComposition(typeString)   -- 属性详析
        
    end
end
-- 属性详析结果
function LuaPlayerPropertyMgr:OnPlayerPropCompositionReturn(propData_U)
    local propData = g_MessagePack.unpack(propData_U)
    self.PropertyGuideID = self.TemplatePropertyGuideID
    self.PlayerPropData = self:AnalysisData(propData)
    CUIManager.ShowUI(CLuaUIResources.PropertyGuideDetailWnd)
end
-- 请求属性对比
function LuaPlayerPropertyMgr:ShowPlayerPropertyCompareWnd(PropertyID,PlayerData)
    if self.IsCross then self:ShowCrossPlayerPropertyCompareWnd(PropertyID,PlayerData) return end
    local data = PropGuide_Property.GetData(PropertyID)
    if not data then return end
    self.TemplatePropertyGuideID = PropertyID
    self.PlayerCompareData = PlayerData
    local type = CommonDefs.ConvertIntToEnum(typeof(EPlayerFightProp), data.FightPropID)
    local typeString = CommonDefs.Convert_ToString(type)
    -- 请求服务器数据
    Gac2Gas.PlayerRequestCompareRankProp(PlayerData.id,typeString)
end

-- 请求跨服属性对比
function LuaPlayerPropertyMgr:ShowCrossPlayerPropertyCompareWnd(PropertyID,PlayerData)
    local data = PropGuide_Property.GetData(PropertyID)
    if not data then return end
    self.TemplatePropertyGuideID = PropertyID
    self.PlayerCompareData = PlayerData
    local type = CommonDefs.ConvertIntToEnum(typeof(EPlayerFightProp), data.FightPropID)
    local typeString = CommonDefs.Convert_ToString(type)
    -- 请求服务器数据
    Gac2Gas.PlayerRequestCompareCrossRankProp(PlayerData.id,typeString,PlayerData.rank or 0)
end
-- 属性对比结果
function LuaPlayerPropertyMgr:OnCompareRankPropResult(selfPropData_U, targetPropData_U)
    local propData = g_MessagePack.unpack(selfPropData_U)
    local oppoPropData = g_MessagePack.unpack(targetPropData_U)
    self.PropertyGuideID = self.TemplatePropertyGuideID
    self.PlayerPropData = self:AnalysisData(propData)
    self.OppositePropData = self:AnalysisData(oppoPropData)
    CUIManager.ShowUI(CLuaUIResources.PlayerPropertyCompareWnd)
end
-- 请求在线玩家属性对比的属性列表
function LuaPlayerPropertyMgr:ShowPlayerPropertyCompareOnlineWnd(targetID)
    Gac2Gas.QueryOnlinePlayerSharedProp(targetID)
end
-- 请求在线玩家属性对比结果
function LuaPlayerPropertyMgr:ShowPlayerRequestCompareOnlineProp(PropertyEname,PlayerData)
    local data = PropGuide_Property.GetDataBySubKey("FightPropID",EnumToInt(EPlayerFightProp[PropertyEname]))
    if not data then return end
    self.TemplatePropertyGuideID = data.ID
    self.PlayerCompareData = PlayerData
    -- 请求服务器数据
    Gac2Gas.PlayerRequestCompareOnlineProp(PlayerData.id,PropertyEname)
end
-- 在线玩家属性对比结果返回
function LuaPlayerPropertyMgr:OnCompareOnlinePropResult(selfPropData_U, targetPropData_U)
    local propData = g_MessagePack.unpack(selfPropData_U)
    local oppoPropData = g_MessagePack.unpack(targetPropData_U)
    self.PropertyGuideID = self.TemplatePropertyGuideID
    self.PlayerPropData = self:AnalysisData(propData)
    self.OppositePropData = self:AnalysisData(oppoPropData)
    g_ScriptEvent:BroadcastInLua("OnCompareOnlinePropResult")
end
-- 在线玩家属性对比属性类型返回
function LuaPlayerPropertyMgr:OnQueryOnlinePlayerSharedPropResult(targetId, propType_U)
    local propList = g_MessagePack.unpack(propType_U)
    self.PropertyTypeList = propList
    if not CClientPlayerMgr.Inst then return end
    local obj = CClientPlayerMgr.Inst:GetPlayer(targetId)
    if not obj then g_MessageMgr:ShowMessage("TRADE_NOT_NEARBY") return end
    self.PlayerCompareData = {id = targetId, name = obj.Name}
    CUIManager.ShowUI(CLuaUIResources.PlayerPropertyCompareWnd)
end
-- 请求查看属性排行榜
function LuaPlayerPropertyMgr:QueryPropRank(PropertyID)
    local data = PropGuide_Property.GetData(PropertyID)
    if not data then return end
    self.TemplatePropertyGuideID = PropertyID
    local type = CommonDefs.ConvertIntToEnum(typeof(EPlayerFightProp), data.FightPropID)
    local typeString = CommonDefs.Convert_ToString(type)
    self.queryRank = true
    -- 请求服务器数据
    Gac2Gas.PropGuideIsOpenCrossServer()                    -- 是否显示跨服按钮
    Gac2Gas.PlayerQueryPropShareRecord(typeString)          -- 分享情况
    Gac2Gas.PlayerQueryPropRankData(typeString)             -- 排行榜
    
end
-- 请求查看跨服属性排行榜
function LuaPlayerPropertyMgr:QueryCrossPropRank(PropertyID)
    local data = PropGuide_Property.GetData(PropertyID)
    if not data then return end
    self.TemplatePropertyGuideID = PropertyID
    local type = CommonDefs.ConvertIntToEnum(typeof(EPlayerFightProp), data.FightPropID)
    local typeString = CommonDefs.Convert_ToString(type)
    self.queryRank = true
    -- 请求服务器数据
    Gac2Gas.PropGuideIsOpenCrossServer()                    -- 是否显示跨服按钮
    Gac2Gas.PlayerQueryPropShareRecord(typeString)          -- 分享情况
    Gac2Gas.PlayerQueryCrossPropRankData(typeString)        -- 排行榜
    
end

function LuaPlayerPropertyMgr:OnPropGuideIsOpenCrossServer(isOpenCross)
    self.IsOpenCrossServer = isOpenCross
    g_ScriptEvent:BroadcastInLua("OnPropGuideIsOpenCrossServer")
end
-- 排行榜信息结果
function LuaPlayerPropertyMgr:OnPlayerQueryPropRankDataResult(rankData_U)
    self.IsCross = false
    local rankData = g_MessagePack.unpack(rankData_U)
    self.PropertyGuideID = self.TemplatePropertyGuideID
    self.PropRankData = {}
    for i = 1,#rankData,6 do
        table.insert(self.PropRankData,{
            rank = rankData[i],
            id = rankData[i + 1],
            class = rankData[i + 2],
            name = rankData[i + 3],
            serverName = rankData[i + 4],
            score = rankData[i + 5],
        })
    end
    -- g_ScriptEvent:BroadcastInLua("UpdatePropertyRankData")
    if self.queryRank then
         -- 打开排行榜
        CUIManager.ShowUI(CLuaUIResources.PropertyRankWnd)
    end
    --self.queryRank = false
end
-- 跨服排行榜信息结果
function LuaPlayerPropertyMgr:OnPlayerQueryPropRankCrossDataResult(rankData_U)
    self.IsCross = true
    local rankData = g_MessagePack.unpack(rankData_U)
    self.PropertyGuideID = self.TemplatePropertyGuideID
    self.PropRankData = {}
    for i = 1,#rankData,6 do
        table.insert(self.PropRankData,{
            rank = rankData[i],
            id = rankData[i + 1],
            class = rankData[i + 2],
            name = rankData[i + 3],
            serverName = rankData[i + 4],
            score = rankData[i + 5],
        })
    end
    -- g_ScriptEvent:BroadcastInLua("UpdatePropertyRankData")
    if self.queryRank then
         -- 打开排行榜
        CUIManager.ShowUI(CLuaUIResources.PropertyRankWnd)
    end
    --self.queryRank = false
end

-- 请求查看等级详析
function LuaPlayerPropertyMgr:QuerySkillDetailWnd(skillID,skillClassId)
    self.CurSkillID = skillID
    self.CurSkillClassID = skillClassId
    -- 请求服务器技能等级数据
    Gac2Gas.PlayerQueryExtraSkill(self.CurSkillClassID)
end
-- 等级详析结果
function LuaPlayerPropertyMgr:OnQuerySkillDetailResult(Result_U)
    local resultData = g_MessagePack.unpack(Result_U)
    self.SkillLevelData = {}
    for i = 1,#resultData,2 do
        if self.SkillLevelData[resultData[i]] then
            self.SkillLevelData[resultData[i]] = self.SkillLevelData[resultData[i]] + resultData[i + 1]
        else
            self.SkillLevelData[resultData[i]] = resultData[i + 1]
        end
    end
    self:ShowSkillDetailWnd()
end

function LuaPlayerPropertyMgr:OnPlayerQueryPropShareResult(isShared, fromId)
    self.PlayerIsShareProp = isShared
    self.PropertyGuideID = self.TemplatePropertyGuideID
    if fromId == 1 and self.ShareInfo and not self.ShareInfo.isShared then
        local shareText = self.ShareInfo.shareText
        local channel = self.ShareInfo.channel
        if shareText==nil or shareText=="" then
                return
        end
        local result = CChatMgr.Inst:SendChatMsg(channel, shareText, true)
        if result == SendChatMsgResult.Success then
            LuaPlayerPropertyMgr:SetShareInfo(shareText,channel,true)
        end
    end
    g_ScriptEvent:BroadcastInLua("OnPlayerQueryPropShareResult")
end
function LuaPlayerPropertyMgr:OnGetPlayerEquipmentInfo(Id,data_U)
    if not self.EquipmentData then self.EquipmentData = {} end
    self.EquipmentData[Id] = CCommonItem(false,data_U)
    CItemMgr.Inst:AddItem(self.EquipmentData[Id])
    g_ScriptEvent:BroadcastInLua("OnGetPlayerPropertyEquipmentInfo",Id)
end
function LuaPlayerPropertyMgr:AnalysisData(propData)
    local result = {}
    result.PropData = self:GetPropertyData()
    result.propWords = {}
    result.total = {}
    local firstPart = propData[1]
    local secondPart = propData[2]
    for i = 1,#firstPart,3 do
        --print("firstPart:",i,firstPart[i],firstPart[i + 1],firstPart[i + 2])
        local value = {firstPart[i],firstPart[i + 1],firstPart[i + 2]}
        if firstPart[i + 2] == 1 then
            self:GetPropWordsData(result.propWords,firstPart[i] + 1000,value,false)
            result.PropData.BaseWords[firstPart[i] + 1000] = true
            result.PropData.BaseWords[firstPart[i]] = false
        elseif firstPart[i + 2] == 2 then
            self:GetPropWordsData(result.propWords,firstPart[i] + 2000,value,false)
            result.PropData.PrecentWords[firstPart[i] + 2000] = true
            result.PropData.PrecentWords[firstPart[i]] = false
        elseif firstPart[i + 2] == 3 then
            self:GetPropWordsData(result.propWords,firstPart[i] + 3000,value,false)
            result.PropData.ExtraWords[firstPart[i] + 3000] = true
            result.PropData.ExtraWords[firstPart[i]] = false
        elseif firstPart[i + 2] == 4 then
            self:GetPropWordsData(result.propWords,firstPart[i] + 4000,value,false)
            result.PropData.OtherWords[firstPart[i] + 4000] = true
            result.PropData.OtherWords[firstPart[i]] = false
        else
            self:GetPropWordsData(result.propWords,firstPart[i],value,true)
        end
        
    end
    local specialID = PropGuide_Setting.GetData().SpecialPropertyWordID
    local data = {main = 0, other = 0}
    result.total.all = {main = 0, other = 0}
    result.total.base = {main = 0, other = 0}
    result.total.precent = {main = 0, other = 0}
    result.total.extra = {main = 0, other = 0}
    result.total.other ={main = 0, other = 0}
    for i = 1,#secondPart,3 do
        --print("secondPart:",i,secondPart[i],secondPart[i+1],secondPart[i+2])
        if secondPart[i] == "Total" then 
            result.total.all.main = secondPart[i + 1]
            result.total.all.other = secondPart[i + 2] 
        elseif secondPart[i] == "mulPart" then 
            result.total.precent.main = secondPart[i + 1]  
            result.total.precent.other = secondPart[i + 2] 
        elseif secondPart[i] == "adj2Part" then 
            result.total.extra.main = secondPart[i + 1]  
            result.total.extra.other = secondPart[i + 2] 
        elseif secondPart[i] == "OtherPart" then 
            result.total.other.main = secondPart[i + 1]  
            result.total.other.other = secondPart[i + 2]
        elseif secondPart[i] == "growPart" then
            result.total.base.main = result.total.base.main + secondPart[i + 1]
            result.total.base.other = result.total.base.other + secondPart[i + 2]
            result.propWords[specialID] = {}
            result.propWords[specialID].val = secondPart[i + 1]
        elseif secondPart[i] == "adjPart" then
            result.total.base.main = result.total.base.main + secondPart[i + 1]
            result.total.base.other = result.total.base.other + secondPart[i + 2]
        end 
    end
    result.total.precent.main = result.total.base.main * result.total.precent.main
    return result
end

function LuaPlayerPropertyMgr:GetPropertyData()
    local propertyData = PropGuide_Property.GetData(self.PropertyGuideID)
    local BaseWords = {}
    local PrecentWords = {}
    local ExtraWords = {}
    local OtherWords = {}
    if not propertyData.BaseWords then 
        BaseWords = nil
    else
        for i = 0,propertyData.BaseWords.Length - 1 do
            BaseWords[propertyData.BaseWords[i]] = true
        end
    end
    if not propertyData.PrecentWords then
        PrecentWords = nil
    else
        for i = 0,propertyData.PrecentWords.Length - 1 do
            PrecentWords[propertyData.PrecentWords[i]] = true
        end
    end
    if not propertyData.ExtraWords then
        ExtraWords = nil
    else
        for i = 0,propertyData.ExtraWords.Length - 1 do
            ExtraWords[propertyData.ExtraWords[i]] = true
        end
    end
    
    if not propertyData.OtherWords then
        OtherWords = nil
    else
        for i = 0,propertyData.OtherWords.Length - 1 do
            ExtraWords[propertyData.OtherWords[i]] = true
        end
    end
    propertyData.BaseWords = BaseWords
    propertyData.PrecentWords = PrecentWords
    propertyData.ExtraWords = ExtraWords
    propertyData.OtherWords = OtherWords
    return propertyData
end

function LuaPlayerPropertyMgr:GetPropWordsData(propTable,index,value,addequip)
    if not propTable[index] then
        propTable[index] = {}
        propTable[index].val = value[2]
        propTable[index].equip = {}
        if addequip then table.insert(propTable[index].equip,value[3]) end
    else
        equip = propTable[index].equip
        propTable[index].val = propTable[index].val + value[2]
        if addequip then table.insert(propTable[index].equip,value[3]) end
    end
end


function LuaPlayerPropertyMgr:ShowSkillDetailWnd()
    local skillWords = PropGuide_Setting.GetData().SkillLevelAdditionWords
    local skillCapacity = PropGuide_Setting.GetData().SkillLevelAdditionCapacity
    local t = {}
    local titleName = LocalString.GetString("技能额外等级加成")
    local btnName = nil
    local btnCallBack = nil
    local wndPosX = LuaPlayerPropertyMgr.SkillLevelDetailWndPosX
    if skillWords then
        for i = 0,skillWords.Length - 1 do
            local data = PropGuide_PropertyWords.GetData(skillWords[i])
            if data then
                local name = data.Desc
                local val = self:GetSkillWordValue(skillWords[i])
                local func = nil
                if val == 0 then
                    func = function(go) go:GetComponent(typeof(UIWidget)).alpha = 0.3 end
                end
                table.insert(t,{Key = name, Value = val,Func = func})
            end
        end
    end
    table.sort(t,function(a,b)
        return a.Value > b.Value
    end)
    local kvTable = {}
    for i = 1,#t do
        table.insert(kvTable,{Key = t[i].Key, Value = "+"..tostring(t[i].Value),Func = t[i].Func})
    end
    local subTable = self:GetCapacityRecommendSubTable(skillCapacity)
    if subTable and #subTable > 0 then
        btnName = LocalString.GetString("提升建议")
        btnCallBack = function()
            local capacityTable = {
                key = "Property",
                name = LocalString.GetString("技能等级"),
                ignore = true,
                sub = subTable,
            }
            CLuaPlayerCapacityMgr.AddNewModuleDef(capacityTable)
            CLuaPlayerCapacityRecommendWnd.fromKey = "Property"
            CUIManager.ShowUI(CUIResources.PlayerCapacityRecommendWnd)
        end
    end
    LuaMessageTipMgr:ShowMessageWithTable(kvTable,titleName,btnName,btnCallBack,wndPosX)
end

function LuaPlayerPropertyMgr:GetSkillWordValue(wordID)
    if self.SkillLevelData[wordID] then
        return self.SkillLevelData[wordID]
    end
    return 0
end
function LuaPlayerPropertyMgr:GetCapacityRecommendSubTable(capacityList)
    if not capacityList then return nil end
    local res = {}
    local sub = {}
    for i = 0,capacityList.Length - 1 do
        local data = PropGuide_CapacityKey.GetData(capacityList[i])
        if data then
            local c = nil
            if data.Content == "EquipValue_ShenBing" or data.Content == "GrowthValue_XingGuan" then
                c = function()
                    --需要飞升
                    return CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.HasFeiSheng
                end
            elseif data.Content == "GuildValue_LianShen" then
                c = function()
                    -- 需要帮会炼神激活
                    return Guild_GuildBenifit.GetData(2).IsActive == 1
                end
            end
            table.insert(sub,{data.Content,data.TitleName,condition = c })
        end
    end
    for i,v in ipairs(sub) do
        local curval = CLuaPlayerCapacityMgr.GetValue(v[1])
        local haveRecommend,isHigh = CLuaPlayerCapacityMgr.GetRecommendValue(v[1], curval)
        if haveRecommend then
            v.sortIndex = 1
            if isHigh then v.sortIndex = 2 end
        else
            v.sortIndex = 0
        end
    end
    table.sort(sub,function(a,b) return a.sortIndex < b.sortIndex end)

    return sub
end
-- 属性值总和
function LuaPlayerPropertyMgr:GetTotalValue(PropID,val)
    if PropID > 0 then
        local type = CommonDefs.ConvertIntToEnum(typeof(EPlayerFightProp), PropID)
        if type == EPlayerFightProp.mSpeed or type == EPlayerFightProp.pSpeed then    -- 攻速
            if val < 1E-06 then val = 1 end
            return System.String.Format("{0:F2}",val)
        elseif type == EPlayerFightProp.pFatalDamage or type == EPlayerFightProp.mFatalDamage
        or type == EPlayerFightProp.AntipFatalDamage or type == EPlayerFightProp.AntimFatalDamage then
            return System.String.Format("{0}%",tostring(NumberTruncate(val, 3) * 100))
        elseif type == EPlayerFightProp.MulpHurt or type == EPlayerFightProp.MulmHurt then
            val = -1 * val
            if math.abs(val) < System.Double.Epsilon then
                return LocalString.GetString("0")
            else
                return System.String.Format("{0:F1}%", val * 100)
            end
        else
            return tostring((math.floor(val)))
        end
    else
        return LocalString.GetString("无数据")
    end
end
function LuaPlayerPropertyMgr:ShowPlayerPopupMenu(propertyID,playerData)
    self.PropertyGuideID = propertyID
    self.PlayerCompareData = playerData
    CPlayerInfoMgr.ShowPlayerPopupMenu(playerData.id, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
end

function LuaPlayerPropertyMgr:SetShareInfo(shareText,channel,isShared)
    self.ShareInfo = {}
    self.ShareInfo.shareText = shareText
    self.ShareInfo.channel = channel
    self.ShareInfo.isShared = isShared
    self.ShareInfo.time = CServerTimeMgr.Inst.timeStamp
end

function LuaPlayerPropertyMgr:GetPropertyGuideLevel()
    if self.PropertyGuideLevel == 0 then
        self.PropertyGuideLevel = PropGuide_Setting.GetData().ShowGuideLevel
    end
    return self.PropertyGuideLevel
end