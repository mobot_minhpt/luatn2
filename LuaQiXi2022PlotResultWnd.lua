local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaQiXi2022PlotResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQiXi2022PlotResultWnd, "ResultView", "ResultView", GameObject)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "PlotView", "PlotView", GameObject)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "OtherResultBtn", "OtherResultBtn", GameObject)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "PlotButton", "PlotButton", GameObject)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "Portrait1", "Portrait1", CUITexture)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "Portrait2", "Portrait2", CUITexture)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "NameLabel1", "NameLabel1", UILabel)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "NameLabel2", "NameLabel2", UILabel)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "ResultLabel", "ResultLabel", UILabel)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "ResultDesLabel", "ResultDesLabel", UILabel)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "ResultBg", "ResultBg", CUITexture)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "NextPageButton", "NextPageButton", GameObject)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "LastPageButton", "LastPageButton", GameObject)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "ThisResultButton", "ThisResultButton", GameObject)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "LeftPageNumLabel", "LeftPageNumLabel", UILabel)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "RightPageNumLabel", "RightPageNumLabel", UILabel)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "LeftPageLabel", "LeftPageLabel", UILabel)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "RightPageLabel", "RightPageLabel", UILabel)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "RightPlotLabel", "RightPlotLabel", UILabel)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "LeftPlotLabel", "LeftPlotLabel", UILabel)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "PathViewLabel01", "PathViewLabel01", UILabel)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "PathViewLabel02", "PathViewLabel02", UILabel)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "PathViewLabel03", "PathViewLabel03", UILabel)
RegistChildComponent(LuaQiXi2022PlotResultWnd, "GuangFx", "GuangFx", CUIFx)

--@endregion RegistChildComponent end
RegistClassMember(LuaQiXi2022PlotResultWnd, "m_Page")
RegistClassMember(LuaQiXi2022PlotResultWnd, "m_PageMaxCount")
RegistClassMember(LuaQiXi2022PlotResultWnd, "m_Data")
RegistClassMember(LuaQiXi2022PlotResultWnd, "m_EndingData")
RegistClassMember(LuaQiXi2022PlotResultWnd, "m_OptionData")

function LuaQiXi2022PlotResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.OtherResultBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOtherResultBtnClick()
	end)


	
	UIEventListener.Get(self.PlotButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPlotButtonClick()
	end)


	
	UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)


	
	UIEventListener.Get(self.NextPageButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnNextPageButtonClick()
	end)


	
	UIEventListener.Get(self.LastPageButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLastPageButtonClick()
	end)


	
	UIEventListener.Get(self.ThisResultButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnThisResultButtonClick()
	end)


    --@endregion EventBind end
end

function LuaQiXi2022PlotResultWnd:Init()
	self.ShareButton.gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
	--self.GuangFx:LoadFx("fx/ui/prefab/UI_mgd_suifeng_guanglizi.prefab")
	self.ResultView.gameObject:SetActive(true)
	self.PlotView.gameObject:SetActive(false)
	self.m_Data = QiXi2022_SFEQTaskDescription.GetData(LuaQiXi2022Mgr.m_PlotWndResultStage) 
	self.m_PageMaxCount = self.m_Data.EndingPlot.Length
	self.ResultLabel.text = self.m_Data.EndingName
	self.ResultDesLabel.text = self.m_Data.EndingText
	self.ResultBg:LoadMaterial(self.m_Data.Pic)
	self.PathViewLabel01.text = self.m_Data.Path[0]
	self.PathViewLabel02.text = self.m_Data.Path[1]
	self.PathViewLabel03.text = self.m_Data.Path[2]
	Gac2Gas.SFEQ_QueryPlayData()
end

function LuaQiXi2022PlotResultWnd:OnEnable()
	g_ScriptEvent:AddListener("SFEQ_SyncPlayData", self, "OnSyncPlayData")
end

function LuaQiXi2022PlotResultWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SFEQ_SyncPlayData", self, "OnSyncPlayData")
	LuaQiXi2022Mgr.m_PlotWndResultStage = -1 
end

function LuaQiXi2022PlotResultWnd:OnSyncPlayData(data)
	self.m_EndingData, self.m_OptionData = data[1], data[2]
	if CClientMainPlayer.Inst then
		self.NameLabel1.text = CClientMainPlayer.Inst.Name
		self.Portrait1:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName, false)
	end
	for endingId, playInfo in pairs(self.m_EndingData) do
		local name = playInfo[2]
		local portrait = CUICommonDef.GetPortraitName(playInfo[3], playInfo[4], -1)
		if endingId == LuaQiXi2022Mgr.m_PlotWndResultStage then
			if playInfo[1] == CClientMainPlayer.Inst.Id then
				self.NameLabel1.text = name
				self.Portrait1:LoadNPCPortrait(portrait, false)
			else
				self.NameLabel2.text = name
				self.Portrait2:LoadNPCPortrait(portrait, false)
			end
		end
	end
end

--@region UIEvent

function LuaQiXi2022PlotResultWnd:OnOtherResultBtnClick()
	LuaQiXi2022Mgr.m_PlotWndShowFlowChart = true
	LuaQiXi2022Mgr.m_PlotWndResultStage = -1 
	CUIManager.ShowUI(CLuaUIResources.QiXi2022PlotWnd)
end

function LuaQiXi2022PlotResultWnd:OnPlotButtonClick()
	self.ResultView.gameObject:SetActive(false)
	self.PlotView.gameObject:SetActive(true)
	self.m_Page = 1
	self:ShowPlot()
end

function LuaQiXi2022PlotResultWnd:OnShareButtonClick()
	CUICommonDef.CaptureScreen(
            "screenshot",
            true,
            false,
            self.ShareButton,
            DelegateFactory.Action_string_bytes(
                    function(fullPath, jpgBytes)
                        -- self.ShareButton:SetActive(true)
                        if CLuaShareMgr.IsOpenShare() then
                            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
                        else
                            CLuaShareMgr.SimpleShareLocalImage2PersonalSpace(fullPath)
                        end
                    end
            ),
            false
    )
end

function LuaQiXi2022PlotResultWnd:OnNextPageButtonClick()
	self:OnPageChange(self.m_Page + 1)
end

function LuaQiXi2022PlotResultWnd:OnLastPageButtonClick()
	if self.m_Page <= 1 then return end
	self:OnPageChange(self.m_Page - 1)
end

function LuaQiXi2022PlotResultWnd:OnThisResultButtonClick()
	self.ResultView.gameObject:SetActive(true)
	self.PlotView.gameObject:SetActive(false)
end

--@endregion UIEvent

function LuaQiXi2022PlotResultWnd:OnPageChange(page)
	if page < 1 then return end
	if page < 1 then return end
	self.m_Page = page
	self:ShowPlot()
end

function LuaQiXi2022PlotResultWnd:ShowPlot()
	if not self.m_Data then return end
	local endingPlot = self.m_Data.EndingPlot
	self.LeftPageNumLabel.text = "/" .. self.m_PageMaxCount
	self.RightPageNumLabel.text = ((self.m_Page * 2) > self.m_PageMaxCount) and "" or ("/" .. self.m_PageMaxCount)
	self.LeftPageLabel.text = self.m_Page * 2 - 1
	self.RightPageLabel.text = self.m_Page * 2
	self.RightPageLabel.gameObject:SetActive((self.m_Page * 2) <= self.m_PageMaxCount)
	self.LastPageButton.gameObject:SetActive(self.m_Page > 1)
	self.NextPageButton.gameObject:SetActive(self.m_Page < math.ceil(self.m_PageMaxCount / 2))
	local leftIndex = self.m_Page * 2 - 2
	self.LeftPlotLabel.text = endingPlot[leftIndex]
	self.RightPlotLabel.text = ((self.m_Page * 2) > self.m_PageMaxCount) and "" or endingPlot[leftIndex + 1]
end