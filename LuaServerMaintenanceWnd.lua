local CPublicNoticeInfo = import "L10.Game.CPublicNoticeInfo"
local CLoginMgr = import "L10.Game.CLoginMgr"
local EServerStatus = import "L10.UI.EServerStatus"
local Pivot = import "UIWidget+Pivot"
local CChatLinkMgr = import "CChatLinkMgr"

LuaServerMaintenanceWnd = class()

RegistChildComponent(LuaServerMaintenanceWnd, "m_Button","Button", GameObject)
RegistChildComponent(LuaServerMaintenanceWnd,"m_Table","Table", UITable)
RegistChildComponent(LuaServerMaintenanceWnd,"m_LabelTemplate","LabelTemplate", GameObject)

RegistClassMember(LuaServerMaintenanceWnd, "m_CheckServerlistTick")
RegistClassMember(LuaServerMaintenanceWnd, "m_UpdateServerlistTick")

function LuaServerMaintenanceWnd:Init()
    self.m_LabelTemplate:SetActive(false)
    UIEventListener.Get(self.m_Button).onClick = DelegateFactory.VoidDelegate(function(go)
        CPublicNoticeInfo.Inst:ShowUI(true)
    end)
    self:InitMessageList(LuaLoginMgr.m_MessageShowInServerMaintenanceWnd)
    self:CancelCheckServerlistTick()
    self.m_CheckServerlistTick = RegisterTick(function()
        if (not CUIManager.IsLoaded(CUIResources.ServerChosenWnd) ) then
            self:CheckServerlist()
            if (CLoginMgr.Inst.m_DownloadServerListCoroutine == nil) then
                CLoginMgr.Inst:UpdateServerlist()
            end
        end
    end,4000)
    self.m_Button:SetActive(LuaLoginMgr.m_ServerMaintenanceWndState == 1)
end

function LuaServerMaintenanceWnd:CheckServerlist()
    local gs = CLoginMgr.Inst:GetSelectedGameServer()
    if not gs then return end
    if not (gs.status == EServerStatus.Maintaining) then
        CUIManager.CloseUI(CLuaUIResources.ServerMaintenanceWnd)
    end
end

function LuaServerMaintenanceWnd:CancelCheckServerlistTick()
    if self.m_CheckServerlistTick then
        UnRegisterTick(self.m_CheckServerlistTick)
        self.m_CheckServerlistTick = nil
    end
end

function LuaServerMaintenanceWnd:OnEnable()
    CLoginMgr.Inst.m_IsLoginingMaintenanceServer = true
end

function LuaServerMaintenanceWnd:OnDisable()
    CLoginMgr.Inst.m_IsLoginingMaintenanceServer = false
    self:OnClose()
end

function LuaServerMaintenanceWnd:OnClose()
    self:CancelCheckServerlistTick()
    LuaLoginMgr:OnMaintenanceServerDisconnected()
end

function LuaServerMaintenanceWnd:InitMessageList(msgs)
    Extensions.RemoveAllChildren(self.m_Table.transform)
    for p, message in string.gmatch(msgs, '<p(.-)>(.-)</p>') do
        local fontsize, pivot = 38, Pivot.Left
        for k,a,b,v in string.gmatch(p, '(%w+)(%s-)=(%s-)(%w+)') do
            if k == "fontsize" then fontsize = tonumber(v) end
            if k == "pivot" then
                if v == "left" then pivot = Pivot.Left
                elseif v == "center" then pivot = Pivot.Center
                elseif v == "right" then pivot = Pivot.Right end
            end
        end
        if fontsize and pivot then
            self:InitMessage(fontsize, pivot, message)
        end
    end
    self.m_Table:Reposition()
end

function LuaServerMaintenanceWnd:InitMessage(fontsize, pivot, message)
    local obj = NGUITools.AddChild(self.m_Table.gameObject, self.m_LabelTemplate.gameObject)
    obj.gameObject:SetActive(true)
    local label = obj:GetComponent(typeof(UILabel))
    label.fontSize = fontsize
    label.pivot = pivot
    label.text = CChatLinkMgr.TranslateToNGUIText(message, false)
    label:UpdateNGUIText()
    label:ResizeCollider()
end