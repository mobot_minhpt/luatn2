-- Auto Generated!!
local AlignType1 = import "L10.UI.CItemInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CTooltip = import "L10.UI.CTooltip"
local CTradeDetailView = import "L10.UI.CTradeDetailView"
local CTradeMgr = import "L10.Game.CTradeMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumQualityType = import "L10.Game.EnumQualityType"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local Int32 = import "System.Int32"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local String = import "System.String"
local StringBuilder = import "System.Text.StringBuilder"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTradeDetailView.m_Init_CS2LuaHook = function (this)

    this.iconTexture.material = nil
    this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    this.disableSprite.enabled = false
    this.lockSprite.enabled = false
    this.amountLabel.text = ""
    this.nameLabel.text = ""
    this.lowestPrice.gameObject:SetActive(false)
    this.playerBuy:SetActive(false)
    this.playerSell:SetActive(false)
    this.priceInput.AlignType = CTooltip.AlignType.Right
    this.priceInput:ForceSetText("", false)
    this.tradeItem = nil
    this.tradeEquip = nil
    this.priceInput.OnValueChanged = MakeDelegateFromCSFunction(this.OnPayPriceChanged, MakeGenericClass(Action1, String), this)
    local default
    if CTradeMgr.Inst.IsPlayerSeller then
        default = LocalString.GetString("请填充需要出售的珍品")
    else
        default = LocalString.GetString("请等待玩家选择出售的珍品")
    end
    this.tips.text = default
    this.plusSprite.enabled = CTradeMgr.Inst.IsPlayerSeller

    if CTradeMgr.Inst.IsPlayerSeller and (CTradeMgr.Inst.playerTradeEquip ~= nil or CTradeMgr.Inst.playerTradeItem ~= nil) then
        if CTradeMgr.Inst.playerTradeEquip ~= nil then
            this.tradeEquip = CTradeMgr.Inst.playerTradeEquip
        else
            this.tradeItem = CTradeMgr.Inst.playerTradeItem
        end
        this.playerSell:SetActive(true)
        if this.tradeEquip ~= nil then
            this.equip = EquipmentTemplate_Equip.GetData(this.tradeEquip.TemplateId)
            this.LowestPrice = this.equip.ValuablesFloorPrice * GameSetting_Common_Wapper.Inst.TradeValuablesFloorPrice_Multiple
        else
            this.item = CItemMgr.Inst:GetItemTemplate(this.tradeItem.TemplateId)
            this.LowestPrice = this.item.ValuablesFloorPrice * GameSetting_Common_Wapper.Inst.TradeValuablesFloorPrice_Multiple
        end
        this.lowestPrice.text = tostring(this.LowestPrice)
        local extern
        if CTradeMgr.Inst.targetMoney == 0 then
            extern = LocalString.GetString("待买家输入")
        else
            extern = tostring(CTradeMgr.Inst.targetMoney)
        end
        this.priceLabel.text = extern
        this.rateLabel.text = SafeStringFormat3("%s%%", math.floor((GameSetting_Common_Wapper.Inst.TradeFeePercentage * 100 + 0.5)))
    elseif not CTradeMgr.Inst.IsPlayerSeller and (CTradeMgr.Inst.targetTradeEquip ~= nil or CTradeMgr.Inst.targetTradeItem ~= nil) then
        if CTradeMgr.Inst.targetTradeEquip ~= nil then
            this.tradeEquip = CTradeMgr.Inst.targetTradeEquip
            this.equip = EquipmentTemplate_Equip.GetData(this.tradeEquip.TemplateId)
            this.LowestPrice = this.equip.ValuablesFloorPrice * GameSetting_Common_Wapper.Inst.TradeValuablesFloorPrice_Multiple
        else
            this.tradeItem = CTradeMgr.Inst.targetTradeItem
            this.item = Item_Item.GetData(this.tradeItem.TemplateId)
            this.LowestPrice = this.item.ValuablesFloorPrice * GameSetting_Common_Wapper.Inst.TradeValuablesFloorPrice_Multiple
        end
        this.lowestPrice.text = tostring(this.LowestPrice)
        this.playerBuy:SetActive(true)
        this.inputTips:SetActive(true)
        this.currentOwnLabel.text = tostring(CClientMainPlayer.Inst.Silver)
    else
        return
    end
    this.tips.text = ""
    this.lowestPrice.gameObject:SetActive(true)
    if this.tradeEquip ~= nil then
        this.iconTexture:LoadMaterial(this.equip.Icon)
        this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(this.tradeEquip.QualityType)
        this.disableSprite.enabled = not this.tradeEquip.MainPlayerIsFit
        this.nameLabel.text = this.tradeEquip.DisplayName
        this.nameLabel.color = this.tradeEquip.DisplayColor
    else
        this.iconTexture:LoadMaterial(this.tradeItem.Icon)
        this.disableSprite.enabled = not this.tradeItem.MainPlayerIsFit
        this.nameLabel.text = this.tradeItem.DisplayName
        this.nameLabel.color = this.tradeItem.Color
    end
end
CTradeDetailView.m_OnPayPriceChanged_CS2LuaHook = function (this, s)

    this.inputTips:SetActive(false)
    this.money = 0
    if not System.String.IsNullOrEmpty(s) then
        local builder = NewStringBuilderWraper(StringBuilder)
        if StringAt(s, 0 + 1) > "0" and StringAt(s, 0 + 1) <= "9" then
            builder:Append(CommonDefs.StringSubstring2(s, 0, 1))
        end
        do
            local i = 1
            while i < CommonDefs.StringLength(s) do
                if StringAt(s, i + 1) >= "0" and StringAt(s, i + 1) <= "9" then
                    builder:Append(CommonDefs.StringSubstring2(s, i, 1))
                end
                i = i + 1
            end
        end
        this.priceInput:ForceSetText(ToStringWrap(builder), false)
        local default
        default, this.money = System.UInt64.TryParse(this.priceInput.Text)
    end
    if CClientMainPlayer.Inst.Silver >= 0 and this.money > CClientMainPlayer.Inst.Silver then
        this.money = CClientMainPlayer.Inst.Silver
        this.priceInput:ForceSetText(tostring(this.money), false)
    end
    CTradeMgr.Inst.playerMoney = this.money
end
CTradeDetailView.m_OnEnable_CS2LuaHook = function (this)
    EventManager.AddListener(EnumEventType.TargetTradeEquipUpdate, MakeDelegateFromCSFunction(this.Init, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.PlayerTradeEquipUpdate, MakeDelegateFromCSFunction(this.TradeItemUpdate, MakeGenericClass(Action1, UInt32), this))
    EventManager.AddListener(EnumEventType.TargetTradeItemUpdate, MakeDelegateFromCSFunction(this.Init, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.PlayerTradeItemUpdate, MakeDelegateFromCSFunction(this.TradeItemUpdate, MakeGenericClass(Action1, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.OnFreightSubmitEquipSelected, MakeDelegateFromCSFunction(this.OnPlayerSellItemSelect, MakeGenericClass(Action2, String, String), this))

    UIEventListener.Get(this.qualitySprite.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.qualitySprite.gameObject).onClick, MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this), true)
end
CTradeDetailView.m_OnDisable_CS2LuaHook = function (this)
    EventManager.RemoveListener(EnumEventType.TargetTradeEquipUpdate, MakeDelegateFromCSFunction(this.Init, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.PlayerTradeEquipUpdate, MakeDelegateFromCSFunction(this.TradeItemUpdate, MakeGenericClass(Action1, UInt32), this))
    EventManager.RemoveListener(EnumEventType.TargetTradeItemUpdate, MakeDelegateFromCSFunction(this.Init, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.PlayerTradeItemUpdate, MakeDelegateFromCSFunction(this.TradeItemUpdate, MakeGenericClass(Action1, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnFreightSubmitEquipSelected, MakeDelegateFromCSFunction(this.OnPlayerSellItemSelect, MakeGenericClass(Action2, String, String), this))

    UIEventListener.Get(this.qualitySprite.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.qualitySprite.gameObject).onClick, MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this), false)
end
CTradeDetailView.m_OnPlayerSellItemSelect_CS2LuaHook = function (this, itemId, key)

    if not System.String.IsNullOrEmpty(itemId) then
        if not CTradeMgr.Inst.targetLock then
            local item = CItemMgr.Inst:GetById(itemId)
            if item ~= nil then
                local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
                Gac2Gas.MoveItemToTradeShelf(1, pos, itemId, 1)
            end
        else
            g_MessageMgr:ShowMessage("CANNOT_CHANGE_ITEM_AFTER_MONEY_LOCKED")
        end
    end
end
CTradeDetailView.m_OnItemClick_CS2LuaHook = function (this, go)

    if CTradeMgr.Inst.IsPlayerSeller then
        local dic = CreateFromClass(MakeGenericClass(Dictionary, Int32, MakeGenericClass(List, String)))
        local preciousList = CreateFromClass(MakeGenericClass(List, String))
        local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
        do
            local i = 1
            while i <= bagSize do
                local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
                if not System.String.IsNullOrEmpty(id) then
                    local precious = CItemMgr.Inst:GetById(id)
                    if precious ~= nil and precious.ValuablesFloorPrice > 0 then
                        if precious.IsEquip and precious.Equip:IsAllowFace2FaceTrade() then
                            CommonDefs.ListAdd(preciousList, typeof(String), precious.Id)
                        elseif precious.IsItem and precious.Item:IsAllowFace2FaceTrade() then
                            CommonDefs.ListAdd(preciousList, typeof(String), precious.Id)
                        end
                    end
                end
                i = i + 1
            end
        end
        if preciousList.Count > 0 then
            CommonDefs.DictAdd(dic, typeof(Int32), 0, typeof(MakeGenericClass(List, String)), preciousList)
            CFreightEquipSubmitMgr.OpenEquipmentChooseWnd("", dic, LocalString.GetString("选择要出售的珍品"), 0, false, LocalString.GetString("选择"), "", nil, "")
        else
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("没有可以出售的珍品"))
        end
    elseif this.tradeItem ~= nil or this.tradeEquip ~= nil then
        if this.tradeItem ~= nil then
            CItemInfoMgr.ShowTempLinkItemInfo(this.tradeItem.Id)
        else
            CItemInfoMgr.ShowLinkItemInfo(this.tradeEquip.Id, false, nil, AlignType1.Default, 0, 0, 0, 0)
        end
    end
end
