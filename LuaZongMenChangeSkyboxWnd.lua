
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local UIScrollView = import "UIScrollView"
local QnSelectableButton = import "L10.UI.QnSelectableButton"

LuaZongMenChangeSkyboxWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZongMenChangeSkyboxWnd, "SceneNameLabel", "SceneNameLabel", UILabel)
RegistChildComponent(LuaZongMenChangeSkyboxWnd, "ScenesListView", "ScenesListView", GameObject)
RegistChildComponent(LuaZongMenChangeSkyboxWnd, "ScenesListViewGrid", "ScenesListViewGrid", UIGrid)
RegistChildComponent(LuaZongMenChangeSkyboxWnd, "TextureTemplate", "TextureTemplate", GameObject)
RegistChildComponent(LuaZongMenChangeSkyboxWnd, "ScrollView", "ScrollView", UIScrollView)
RegistChildComponent(LuaZongMenChangeSkyboxWnd, "OkButton", "OkButton", GameObject)
RegistChildComponent(LuaZongMenChangeSkyboxWnd, "SceneTexture", "SceneTexture", CUITexture)
RegistChildComponent(LuaZongMenChangeSkyboxWnd, "ShowScenesListViewButton", "ShowScenesListViewButton", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaZongMenChangeSkyboxWnd, "m_ScenesListViewQnRadioBox")
RegistClassMember(LuaZongMenChangeSkyboxWnd, "m_SelectSceneId")
RegistClassMember(LuaZongMenChangeSkyboxWnd, "m_SkyBoxKinds")

function LuaZongMenChangeSkyboxWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)

	UIEventListener.Get(self.ShowScenesListViewButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShowScenesListViewButtonClick()
	end)

    --@endregion EventBind end
end

function LuaZongMenChangeSkyboxWnd:Init()
	self:InitScenesListView()
end

--@region UIEvent

function LuaZongMenChangeSkyboxWnd:OnOkButtonClick()
    if not self.m_SelectSceneId  then
        g_MessageMgr:ShowMessage("ZongMenChangeSkyboxWnd_NoneSelectSceneId")
        return
    end
    if CClientMainPlayer.Inst then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ZongMen_ChangeSkybox_Confirm",self.SceneNameLabel.text),DelegateFactory.Action(function()
            Gac2Gas.RequestAlterSectSkybox(CClientMainPlayer.Inst.BasicProp.SectId, self.m_SelectSceneId,LuaZongMenMgr.m_ChangeZongMenNameContextUd)
            CUIManager.CloseUI(CLuaUIResources.ZongMenChangeSkyboxWnd)
		end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
    end
end

function LuaZongMenChangeSkyboxWnd:OnShowScenesListViewButtonClick()
	self.ScenesListView:SetActive(true)
    self.ScenesListViewGrid:Reposition()
    self.ScrollView:ResetPosition()
end


--@endregion UIEvent

function LuaZongMenChangeSkyboxWnd:InitScenesListView()
    self.ScenesListView:SetActive(false)
    Extensions.RemoveAllChildren(self.ScenesListViewGrid.transform)
    self.TextureTemplate:SetActive(false)
    local obj = NGUITools.AddChild(self.ScenesListViewGrid.gameObject, self.TextureTemplate)
    obj:GetComponent(typeof(CUITexture)):LoadMaterial("")
    obj:SetActive(false)
    self.m_SkyBoxKinds = {}
    Menpai_SkyBoxKind.Foreach(function(key,data) 
        if data.MenPaiKind == 1 then
            local obj = NGUITools.AddChild(self.ScenesListViewGrid.gameObject, self.TextureTemplate)
            obj:GetComponent(typeof(CUITexture)):LoadMaterial(data.Preview)
            obj:SetActive(true)
            local k = key
            table.insert(self.m_SkyBoxKinds,k)
        end
    end)
    self.ScenesListViewGrid:Reposition()
    self.ScrollView:ResetPosition()
    self.m_ScenesListViewQnRadioBox = self.ScrollView.gameObject:AddComponent(typeof(QnRadioBox))
    self.m_ScenesListViewQnRadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), self.ScenesListViewGrid.transform.childCount)
    for i = 0, self.ScenesListViewGrid.transform.childCount - 1 do
        local go = self.ScenesListViewGrid.transform:GetChild(i).gameObject
        self.m_ScenesListViewQnRadioBox.m_RadioButtons[i] = go:GetComponent(typeof(QnSelectableButton))
        self.m_ScenesListViewQnRadioBox.m_RadioButtons[i].OnClick = MakeDelegateFromCSFunction(self.m_ScenesListViewQnRadioBox.On_Click, MakeGenericClass(Action1, QnButton), self.m_ScenesListViewQnRadioBox)
    end
    self.m_ScenesListViewQnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSelectSceneTexture(index)
    end)
end

function LuaZongMenChangeSkyboxWnd:OnSelectSceneTexture(index)
	self.m_SelectSceneId = self.m_SkyBoxKinds[index]
    local mat = self.m_ScenesListViewQnRadioBox.m_RadioButtons[index].gameObject:GetComponent(typeof(CUITexture)).material
    self.SceneTexture.material = mat
    self.ScenesListView:SetActive(false)
    self.SceneNameLabel.text = Menpai_SkyBoxKind.GetData(index).Name
end
