-- Auto Generated!!
local CScheduleAlertCtl = import "L10.UI.CScheduleAlertCtl"
local CScheduleMgr = import "L10.Game.CScheduleMgr"
CScheduleAlertCtl.m_UpdateAlertState_CS2LuaHook = function (this) 
    if this.designTabTypeID == 15 then --跨服联赛的红点逻辑比较特殊，现有的机制有点不兼容，特殊处理一下
        this.alertSprite.enabled = LuaGuildLeagueCrossMgr:NeedShowTrainAlert() or LuaGuildLeagueCrossMgr:NeedShowForeignAidAlert() or LuaGuildLeagueCrossMgr:NeedShowGambleAlert()
        return
    end
    if this.designTabTypeID == 22 then --全民争霸赛
        this.alertSprite.enabled = CLuaQMPKMgr.m_HandBookRedDot or not CLuaQMPKMgr.GetQMPKPageClicked()
        return
    end
    
    -- 不需要显示在活动界面的红点都用新的红点，不能填schedule的Remind，否则活动按钮的红点点不掉
    this.alertSprite.enabled = LuaActivityRedDotMgr:IsRedDot(this.scheduleId, LuaEnumRedDotType.Schedule) or
                               LuaActivityRedDotMgr:IsTabTypeRedDot(this.tabType)
end
