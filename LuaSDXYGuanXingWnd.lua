local CUIFx = import "L10.UI.CUIFx"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local EasyTouch = import "EasyTouch"
local Vector3 = import "UnityEngine.Vector3"
local CUITexture=import "L10.UI.CUITexture"
local Camera = import "UnityEngine.Camera"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"

LuaSDXYGuanXingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSDXYGuanXingWnd, "ModelPreview", "ModelPreview", CUITexture)
RegistChildComponent(LuaSDXYGuanXingWnd, "HintLabel", "HintLabel", GameObject)
RegistChildComponent(LuaSDXYGuanXingWnd, "FxNode", "FxNode", CUIFx)

--@endregion RegistChildComponent end
RegistClassMember(LuaSDXYGuanXingWnd,"m_StarId")
RegistClassMember(LuaSDXYGuanXingWnd,"m_CloseTime")
RegistClassMember(LuaSDXYGuanXingWnd,"m_CloseTick")
function LuaSDXYGuanXingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    local setting = Christmas2021_Setting.GetData()
    self.RotateX = 0
    self.RotateY = 0
    self.m_BestRotateX = 0
    self.m_BestRotateY = 0
    self.m_IsSucceed = false

    self.m_Deviation = setting.GuanXingDeviation
    self.m_StayTickTime = setting.GuanXingTickTime
    self.m_LimitAlpha = 0.5
    self.m_InitTotalDeviation = 0
    self.m_Identifier = "__Christmas2021GuanXingPreview__"

    self.m_Radius = 5
    self.m_HAngle = 0
    self.m_VAngle = 0
    self.m_CloseTime = 4
end

function LuaSDXYGuanXingWnd:Init()
    self.m_StarId = LuaChristmas2021Mgr.StartSetIndex
    local starsetData = Christmas2021_XingYuSet.GetData(self.m_StarId)
    if starsetData then
        self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
            self:LoadModel(ro)
        end)

        self.m_ResId = starsetData.Order
        self.ModelPreview.mainTexture = CUIManager.CreateModelTexture(self.m_Identifier, self.m_ModelTextureLoader,0,0.05,-0.27,4.66,false,true,1)
        self.ModelPreview.alpha = self.m_LimitAlpha
        local initAngle = starsetData.InitAngle
        local count = initAngle.Length/2
        local index = UnityEngine_Random(1,count+1)
        index = (index-1)*2
        local t = {}
        t.x = initAngle[index] % 360
        t.y = LuaChristmas2021Mgr.s_SwipeUpAndDown and initAngle[index+1] % 360 or 0
        if t.y > 180 then t.y = 360-t.y end

        local bestAngle = starsetData.BestAngle
        self.m_BestRotateX = bestAngle[0] % 360
        self.m_BestRotateY = LuaChristmas2021Mgr.s_SwipeUpAndDown and bestAngle[1] % 360 or 0
        
        self.m_Threshold = starsetData.Threshold
        self.m_InitX = t.x
        self.m_InitY = t.y
        self:SetModelRotation({x=t.x,y=0},true)
        self:SetModelRotation({x=0,y=t.y},true)
        self.RotateX = t.x
        self.RotateY = t.y
    end
    
    self.m_BgMask = self.transform:Find("_BgMask_").gameObject
	UIEventListener.Get(self.m_BgMask).onDrag = DelegateFactory.VectorDelegate(function (go, delta)
        self:OnDrag(go,delta)
    end)

end

function LuaSDXYGuanXingWnd:OnEnable()
    
end

function LuaSDXYGuanXingWnd:OnDisable()
    if self.m_StayTick then
        UnRegisterTick(self.m_StayTick)
        self.m_StayTick = nil
    end
    if self.m_CloseTick then
        UnRegisterTick(self.m_CloseTick)
        self.m_CloseTick = nil
    end
end

function LuaSDXYGuanXingWnd:OnDrag(g,delta)

    if self.m_IsSucceed then
        return
    end

    if EasyTouch.GetTouchCount() == 1 then      
        self:SetModelRotation(delta)
        if self.m_StayTick then
            UnRegisterTick(self.m_StayTick)
            self.m_StayTick = nil
        end

        local isRight,isBack,angle,angleY = self:CheckSatisfiedBestAngle()
        
        if not LuaChristmas2021Mgr.s_DoubleFaceSucceed and isBack then
            isRight = false
        end
        if isRight then--正反面
            self.m_StayTick = RegisterTickOnce(function()
                self.m_IsSucceed = true
                self:RevertBestView(isBack)
                self.ModelPreview.alpha = 1
                self.HintLabel:SetActive(false)

                Gac2Gas.RequestLightStar(self.m_StarId)

                CCommonUIFxWnd.Instance:LoadUIFxAssignedPathImpl("fx/ui/prefab/UI_tiaoxiangwanfa_chenggong.prefab",0,4,false,false)
                self.FxNode:LoadFx("fx/ui/prefab/UI_huafenkuosan.prefab")

                if self.m_CloseTick then
                    UnRegisterTick(self.m_CloseTick)
                    self.m_CloseTick = nil
                end
                self.m_CloseTick = RegisterTickOnce(function()
                    CUIManager.CloseUI(CLuaUIResources.SDXYGuanXingWnd)
                end,self.m_CloseTime*1000)
            end,self.m_StayTickTime*1000)
        end

        local as = math.abs(0.5*math.cos(angle*Deg2Rad)) + math.abs(0.5*math.cos(angleY*Deg2Rad))
        local alpha = self.m_LimitAlpha + (1-self.m_LimitAlpha)*as
        self.ModelPreview.alpha = alpha
	end    
end

function LuaSDXYGuanXingWnd:CheckSatisfiedBestAngle()
    local res = false
    local isBack = false
    
    local forwardM = self.m_ModelTf.forward
    local forwardC = self.m_CameraTf.forward
    local angle = Vector3.Angle(forwardM,forwardC)--[1,180]
    local dangle = angle <= 90 and angle or 180 - angle
    dangle = dangle - self.m_BestRotateY
    local angleY = Vector3.Angle(self.m_ModelTf.up,self.m_CameraTf.up)
    local dangleY = angleY <= 90 and angleY or 180 - angleY
    angleY = angleY - self.m_BestRotateY

    if dangle < self.m_Deviation then
        if angle > 90 then
            isBack = true
        end
        if math.cos(angleY*Deg2Rad) > 0.99 then
           res = true
        end
    end
    return res,isBack,angle,angleY
end

function LuaSDXYGuanXingWnd:SetModelRotation(delta,isForce)
    local mpos = self.m_ModelTf.position
    -- local center = Vector3(0.05,-0.27,4.66)
    local scaler = 0.5
    if math.abs(delta.x) >= math.abs(delta.y) then
        self.m_ModelTf:RotateAround(self.m_ModelTf.position,self.m_ModelTf.up,delta.x*scaler)
        self.RotateX = (self.RotateX + delta.x*scaler)%360
    else
        local ag = self.m_VAngle + delta.y*scaler - self.m_InitY
        ag = math.abs(ag)
        ag = ag % 360
        local ag2 = 360 - ag
        if ag > self.m_Threshold and ag2 > self.m_Threshold and not isForce then
            return
        end
        self.m_VAngle = self.m_VAngle + delta.y*scaler
        local z = mpos.z + self.m_Radius * math.cos(self.m_VAngle*Deg2Rad)
        local y = mpos.y + self.m_Radius * math.sin(self.m_VAngle*Deg2Rad)
        self.m_CameraTf.position = Vector3(mpos.x,y,z)
        local forward = CommonDefs.op_Subtraction_Vector3_Vector3(mpos, self.m_CameraTf.position)
        self.m_CameraTf.forward = forward
    end  
end

function LuaSDXYGuanXingWnd:RevertBestView(isBack)
end


function LuaSDXYGuanXingWnd:LoadModel(ro)
    if not self.m_StarId then return end

    local prefabname = SafeStringFormat3("Assets/Res/Character/NPC/lnpc880/Prefab/lnpc880_0%d.prefab",self.m_ResId)
    ro:LoadMain(prefabname)

    self.m_ModelTf = CUIManager.instance.transform:Find(self.m_Identifier.."/ModelRoot")
    self.m_CameraTf = CUIManager.instance.transform:Find(self.m_Identifier)
    self.m_CameraTf.eulerAngles = Vector3.zero
    self.m_InitModelForwad = self.m_ModelTf.forward
    self.m_InitModelUp = self.m_ModelTf.up
    self.m_InitModelRight = self.m_ModelTf.right

    self.m_Camera=self.m_CameraTf:GetComponent(typeof(Camera))
    self.m_Parent = self.m_ModelTf.parent
    self.m_ModelTf.parent = self.m_Parent.parent
    self.m_InitCameraPos = self.m_CameraTf.position
end

function LuaSDXYGuanXingWnd:OnDestroy()
    self.m_ModelTf.parent = self.m_Parent
    self.ModelPreview.mainTexture = nil
    CUIManager.DestroyModelTexture(self.m_Identifier)
end

--@region UIEvent

--@endregion UIEvent

