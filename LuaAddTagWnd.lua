local EventDelegate=import "EventDelegate"
local CBaseWnd = import "L10.UI.CBaseWnd"
local Application = import "UnityEngine.Application"
local CFamilyTreeMgr = import "L10.Game.CFamilyTreeMgr"
local FamilyRelationShip = import "L10.Game.FamilyRelationShip"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UIInput = import "UIInput"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CUITexture=import "L10.UI.CUITexture"
local QnCheckBox = import "L10.UI.QnCheckBox"
local EnumQualityType = import "L10.Game.EnumQualityType"
local UIEventListener = import "UIEventListener"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local Mall_LingYuMall = import "L10.Game.Mall_LingYuMall"
local FamilyTree_Setting = import "L10.Game.FamilyTree_Setting"
local Item_Item = import "L10.Game.Item_Item"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local MessageWndManager = import "L10.UI.MessageWndManager"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"

LuaAddTagWnd=class()

RegistClassMember(LuaAddTagWnd,"CancelButton")
RegistClassMember(LuaAddTagWnd,"OKButton")
RegistClassMember(LuaAddTagWnd,"InputLabel")
RegistClassMember(LuaAddTagWnd,"DesLabel")
RegistClassMember(LuaAddTagWnd,"AutoLingyuBox")
RegistClassMember(LuaAddTagWnd,"selfMember")
RegistClassMember(LuaAddTagWnd,"CostItemId")
RegistClassMember(LuaAddTagWnd,"CostAndOwnMoney")
RegistClassMember(LuaAddTagWnd,"ItemBtn")
RegistClassMember(LuaAddTagWnd,"ItemCount")
RegistClassMember(LuaAddTagWnd,"ItemMask")
RegistClassMember(LuaAddTagWnd,"itemIcon")
RegistClassMember(LuaAddTagWnd,"itemQuality")
RegistClassMember(LuaAddTagWnd,"ItemData")
RegistClassMember(LuaAddTagWnd,"AutoCost")
RegistClassMember(LuaAddTagWnd,"UseLingyu")

RegistClassMember(LuaAddTagWnd,"MaxInput")
RegistClassMember(LuaAddTagWnd,"MinInput")

function LuaAddTagWnd:Awake()
    self.MaxInput = 8       --  即最多输入4个汉字
    self.MinInput = 2
    self.CostItemId = FamilyTree_Setting.GetData().TagCostItemId
    self.CancelButton = self.transform:Find("Anchor/CancelButton").gameObject
    self.OKButton = self.transform:Find("Anchor/OKButton").gameObject
    self.InputLabel=self.transform:Find("Anchor/Input"):GetComponent(typeof(UIInput))
    self.DesLabel = self.transform:Find("Anchor/DesLabel"):GetComponent(typeof(UILabel))
    self.selfMember = CFamilyTreeMgr.Instance:GetSingleMember(FamilyRelationShip.Self)
    local msg = g_MessageMgr:FormatMessage("Add_Tag_Input",self.selfMember.Name)
    self.DesLabel.text = msg
    self.ItemBtn = self.transform:Find("Anchor/Item").gameObject
    self.ItemMask = self.transform:Find("Anchor/Item/Acquire").gameObject
    self.ItemCount = self.transform:Find("Anchor/Item/Count"):GetComponent(typeof(UILabel))
    self.itemIcon = self.transform:Find("Anchor/Item/itemIcon"):GetComponent(typeof(CUITexture))
    self.itemQuality = self.transform:Find("Anchor/Item/Quality"):GetComponent(typeof(UISprite))
    
    self.CostAndOwnMoney = self.transform:Find("Anchor/QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
    self.CostAndOwnMoney:SetType(EnumMoneyType.LingYu,EnumPlayScoreKey.NONE,true)
    self.CostAndOwnMoney:SetCost(Mall_LingYuMall.GetData(self.CostItemId).Jade)
    
    self.AutoLingyuBox = self.transform:Find("Anchor/AutoLingyuBox"):GetComponent(typeof(QnCheckBox))
    
    local LocalStr = "LuaAddTagWnd_CostLingyu"
    if PlayerPrefs.GetInt(CClientMainPlayer.Inst.Id..LocalStr) ~= 1 then
        self.AutoLingyuBox:SetSelected(false, false)
        self.AutoCost = false
    else
        self.AutoLingyuBox:SetSelected(true, false)
        self.AutoCost = true
    end

    self.AutoLingyuBox.OnValueChanged = DelegateFactory.Action_bool(function(value)     --  是否消耗灵玉
        self.AutoCost = value
        if  value then
            PlayerPrefs.SetInt(CClientMainPlayer.Inst.Id..LocalStr,1)
        else
            PlayerPrefs.SetInt(CClientMainPlayer.Inst.Id..LocalStr,0)
        end
        self:RefreshCostType()
    end)

    self:InitItemBtn()

    UIEventListener.Get(self.CancelButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        self:Close()
    end)
    UIEventListener.Get(self.OKButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        if self.UseLingyu then
            if not CShopMallMgr.TryCheckEnoughJade(Mall_LingYuMall.GetData(self.CostItemId).Jade, 0) then
                return
            end
        else
            if CItemMgr.Inst:GetItemCount(self.CostItemId) < 1 then
                g_MessageMgr:ShowMessage("TAG_FAMILY_TREE_ITEM_NOT_ENOUGH")
                return
            end
        end

        if self:CheckTagName() then
            if luaFamilyMgr.m_selfTagId ~= "" then
                local msg = g_MessageMgr:FormatMessage("Change_Tag_Confirm",self.InputLabel.value)
                local val = self.InputLabel.value
                local autocost = self.AutoCost
                MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function ()
                    Gac2Gas.TryTagPlayerFamilyTree(CFamilyTreeMgr.Instance.CenterPlayerID,val,luaFamilyMgr.m_selfTagId, EnumFamilyTreeTagOperationType.eModify, autocost)
                end), nil, nil, nil, false)
                self:Close()
                return
            else
                Gac2Gas.TryTagPlayerFamilyTree(CFamilyTreeMgr.Instance.CenterPlayerID,self.InputLabel.value, "", EnumFamilyTreeTagOperationType.eAdd, self.AutoCost)
                self:Close()
            end
        end
    end)

	local platform = Application.platform
	if platform == RuntimePlatform.Android or platform == RuntimePlatform.IPhonePlayer then
        EventDelegate.Add(self.InputLabel.onSubmit, DelegateFactory.Callback(function ()
            self:OnInputChange()
        end))
    else
        EventDelegate.Add(self.InputLabel.onChange, DelegateFactory.Callback(function ()
            self:OnInputChange()
        end))
    end
end
function LuaAddTagWnd:OnInputChange()
    local str = self.InputLabel.value
    if CUICommonDef.GetStrByteLength(str) > self.MaxInput then
        repeat
            str = CommonDefs.StringSubstring2(str, 0, CommonDefs.StringLength(str) - 1)
        until not (CUICommonDef.GetStrByteLength(str) > self.MaxInput)
        self.InputLabel.value = str
    end
end

function LuaAddTagWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "RefreshCostType")
	g_ScriptEvent:AddListener("SetItemAt", self, "RefreshCostType")
end

function LuaAddTagWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "RefreshCostType")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "RefreshCostType")
end

function LuaAddTagWnd:RefreshCostType()
    if self.AutoLingyuBox.Selected and CItemMgr.Inst:GetItemCount(self.CostItemId) == 0 then
        self.ItemBtn:SetActive(false)
        self.CostAndOwnMoney.gameObject:SetActive(true)
        self.UseLingyu = true
    else
        self.ItemBtn:SetActive(true)
        self.CostAndOwnMoney.gameObject:SetActive(false)
        self.UseLingyu = false
    end
    if CItemMgr.Inst:GetItemCount(self.CostItemId) == 0 then
        self.ItemMask:SetActive(true)
        self.ItemCount.text = SafeStringFormat3("[c][FF0000]%s[-][/c]/1",CItemMgr.Inst:GetItemCount(self.CostItemId))
    else
        self.ItemMask:SetActive(false)
        self.ItemCount.text = SafeStringFormat3("[c][FFFFFF]%s[-][/c]/1",CItemMgr.Inst:GetItemCount(self.CostItemId))
    end
end

function LuaAddTagWnd:InitItemBtn()
    self.ItemData = Item_Item.GetData(self.CostItemId)
    self.itemIcon:LoadMaterial(self.ItemData.Icon)
    self.itemQuality.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    
    UIEventListener.Get(self.ItemBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        CItemInfoMgr.ShowLinkItemTemplateInfo(self.CostItemId, false, nil, AlignType.Right, 0, 0, 0, 0)
    end)
    UIEventListener.Get(self.ItemMask).onClick = DelegateFactory.VoidDelegate(function (p) 
        CItemAccessListMgr.Inst:ShowItemAccessInfo(self.CostItemId, true,  self.ItemMask.transform, CTooltipAlignType.Right)
    end)
    self:RefreshCostType()
end

function LuaAddTagWnd:CheckTagName()

    local nameStr = string.gsub(self.InputLabel.value, " ", "")
    self.InputLabel.value = nameStr
    
    local length = CUICommonDef.GetStrByteLength(nameStr)
    if length < self.MinInput or CUICommonDef.GetStrByteLength(nameStr) > self.MaxInput then
        g_MessageMgr:ShowMessage("Add_Tag_Failed_Length_Not_Fit")
        return
    end
    if luaFamilyMgr.m_TagNameData[nameStr] then
        g_MessageMgr:ShowMessage("TAG_FAMILY_TREE_CONFLICT")
        return
    end
    if not CWordFilterMgr.Inst:CheckName(nameStr, LocalString.GetString("家谱标签")) then
        g_MessageMgr:ShowMessage("TAG_FAMILY_TREE_NAME_VIOLATION")
        return
    end
    return true
end

function LuaAddTagWnd:Close()
	self.transform:GetComponent(typeof(CBaseWnd)):Close()
end