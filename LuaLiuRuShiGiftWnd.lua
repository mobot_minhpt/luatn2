local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaLiuRuShiGiftWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaLiuRuShiGiftWnd, "GiftTable", "GiftTable", UITable)
RegistChildComponent(LuaLiuRuShiGiftWnd, "GiftTemplate", "GiftTemplate", GameObject)
RegistChildComponent(LuaLiuRuShiGiftWnd, "ConfirmBtn", "ConfirmBtn", GameObject)

RegistClassMember(LuaLiuRuShiGiftWnd, "m_SelectIndex")
RegistClassMember(LuaLiuRuShiGiftWnd, "m_GiftInfoTbl")
--@endregion RegistChildComponent end

function LuaLiuRuShiGiftWnd:Awake()
    UIEventListener.Get(self.ConfirmBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnConfirmBtnClick()
    end)
end

function LuaLiuRuShiGiftWnd:Init()
    self.GiftTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.GiftTable.transform)
    self.m_GiftInfoTbl = {}
    for i = 1, 4 do
        local index = LuaLiuRuShiMgr.m_GiftConditionId * 4 - 4 + i
        local data = NanDuFanHua_LiuRuShiGift.GetData(index)
        if data then
            local go = NGUITools.AddChild(self.GiftTable.gameObject, self.GiftTemplate)
            go:SetActive(true)
            self.m_GiftInfoTbl[i] = {}
            self.m_GiftInfoTbl[i].correct = data.Correct == 1
            self.m_GiftInfoTbl[i].highLight = go.transform:Find("HighLight")
            self.m_GiftInfoTbl[i].TaskFailText = data.TaskFailText
            go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(data.Icon)
            go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = data.Name
            UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (go)
                self:OnGiftItemClick(i)
            end)
        end
    end
    self.GiftTable:Reposition()
    self:OnGiftItemClick(1)
end

--@region UIEvent
function LuaLiuRuShiGiftWnd:OnGiftItemClick(index)
    self.m_SelectIndex = index
    for i, info in pairs(self.m_GiftInfoTbl) do
        info.highLight.gameObject:SetActive(i == index)
    end
end

function LuaLiuRuShiGiftWnd:OnConfirmBtnClick()
    local selectInfo = self.m_GiftInfoTbl[self.m_SelectIndex]
    if selectInfo == nil then return end
    if selectInfo.correct then
        local empty = CreateFromClass(MakeGenericClass(List, Object))
        empty = MsgPackImpl.pack(empty)
	    Gac2Gas.FinishClientTaskEventWithConditionId(LuaLiuRuShiMgr.m_GiftTaskId, "LiuRuShi_Gifting", empty, LuaLiuRuShiMgr.m_GiftConditionId)
    else
        LuaLiuRuShiMgr:ShowLiuRuShiTaskFailWnd(LuaLiuRuShiMgr.m_GiftTaskId, selectInfo.TaskFailText)
    end
    CUIManager.CloseUI(CLuaUIResources.LiuRuShiGiftWnd)
end
--@endregion UIEvent

