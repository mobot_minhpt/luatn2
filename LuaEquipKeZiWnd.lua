local MessageWndManager=import "L10.UI.MessageWndManager"
local CItemMgr=import "L10.Game.CItemMgr"
local CItemChooseMgr=import "L10.UI.CItemChooseMgr"
local EquipmentTemplate_Equip=import "L10.Game.EquipmentTemplate_Equip"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local CWordFilterMgr=import "L10.Game.CWordFilterMgr"
local EnumItemPlace=import "L10.Game.EnumItemPlace"
local UIInput=import "UIInput"
local CUITexture=import "L10.UI.CUITexture"
local CItemCountUpdate=import "L10.UI.CItemCountUpdate"
local QnAddSubAndInputButton=import "L10.UI.QnAddSubAndInputButton"

CLuaEquipKeZiWnd = class()
RegistClassMember(CLuaEquipKeZiWnd,"m_CloseButton")
RegistClassMember(CLuaEquipKeZiWnd,"m_KeDaoCountUpdate")
RegistClassMember(CLuaEquipKeZiWnd,"m_RingTemplateId")
RegistClassMember(CLuaEquipKeZiWnd,"m_Input")
RegistClassMember(CLuaEquipKeZiWnd,"m_SelectedEquipId")
RegistClassMember(CLuaEquipKeZiWnd,"m_CountInput")
RegistClassMember(CLuaEquipKeZiWnd,"m_Colors")
RegistClassMember(CLuaEquipKeZiWnd,"m_Count")
CLuaEquipKeZiWnd.m_TemplateId=0
CLuaEquipKeZiWnd.m_FilterEquipTypes={}

function CLuaEquipKeZiWnd:Awake()
    self.m_Colors={
        [1] = LocalString.GetString("白"),
        [2] = LocalString.GetString("黄"),
        [3] = LocalString.GetString("橙"),
        [5] = LocalString.GetString("蓝"),
        [10] = LocalString.GetString("红"),
        [20] = LocalString.GetString("紫"),
    }
    self.m_Input=FindChild(self.transform,"Input"):GetComponent(typeof(UIInput))

    local tipButton = self.transform:Find("Anchor/TipButton").gameObject
    UIEventListener.Get(tipButton).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("Equip_Sculpture_Tip")
    end)
    self.m_CountInput = self.transform:Find("Anchor/QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
    self.m_CountInput.onValueChanged = DelegateFactory.Action_uint(function (value) 
        local t = {1,2,3,5,10,20}
        for i=1,5 do
            if value>=t[i] and value<t[i+1] then
                self.m_Count = t[i]
                self.m_Input.defaultText = SafeStringFormat3(LocalString.GetString("点击输入(最多15字)，当前%s色(%d把刻刀)"),self.m_Colors[t[i]],t[i])
            else
                if value==0 then
                    self.m_Count = t[1]
                    self.m_Input.defaultText = SafeStringFormat3(LocalString.GetString("点击输入(最多15字)，当前%s色(%d把刻刀)"),self.m_Colors[t[1]],t[1])
                elseif value>=20 then
                    self.m_Count = t[6]
                    self.m_Input.defaultText = SafeStringFormat3(LocalString.GetString("点击输入(最多15字)，当前%s色(%d把刻刀)"),self.m_Colors[t[6]],t[6])
                end
            end
        end
    end)
    self.m_CountInput:SetValue(1,true)
end

function CLuaEquipKeZiWnd:Init()

    UIEventListener.Get(FindChild(self.transform,"CancleButton").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CLuaUIResources.EquipKeZiWnd)
    end)

    UIEventListener.Get(FindChild(self.transform,"OkButton").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_Count~=self.m_CountInput:GetValue() then
            local str = g_MessageMgr:FormatMessage("Equip_Sculpture_Count_Match_Confirm",self.m_CountInput:GetValue(),self.m_CountInput:GetValue(),self.m_Colors[self.m_Count])
            MessageWndManager.ShowOKCancelMessage(str, DelegateFactory.Action(function () 
                self:DoRequestKeZi()
            end), nil, nil, nil, false)
        else
            self:DoRequestKeZi()
        end
    end)
    local targetItem = FindChild(self.transform,"RingItem").gameObject
    UIEventListener.Get(targetItem).onClick = DelegateFactory.VoidDelegate(function(go)
        --根据装备类型过滤
        CItemChooseMgr.Inst.ItemIds=self:GetEquipList()
        CItemChooseMgr.Inst.OnChoose=DelegateFactory.Action_string(function(p)
            self.m_SelectedEquipId=p

            local ringIcon=targetItem.transform:Find("RingIcon"):GetComponent(typeof(CUITexture))
            local bindSprite = targetItem.transform:Find("Mark").gameObject
            local qualitySprite = targetItem.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))

            local commonItem = CItemMgr.Inst:GetById(self.m_SelectedEquipId)
            if commonItem then
                bindSprite:SetActive(commonItem.IsBinded)
                qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(commonItem.Equip.QualityType)
    
                ringIcon:LoadMaterial(commonItem.Icon)
            else
                bindSprite:SetActive(false)
                qualitySprite.spriteName = "common_itemcell_border"
                ringIcon:Clear()
            end
        end)
        CItemChooseMgr.ShowWnd(LocalString.GetString("选择装备"))
    end)

    local kedaoIcon=FindChild(self.transform,"EquipIcon"):GetComponent(typeof(CUITexture))
    local item = Item_Item.GetData(CLuaEquipKeZiWnd.m_TemplateId)
    if item then
        kedaoIcon:LoadMaterial(item.Icon)
    end
    self.m_KeDaoCountUpdate=FindChild(self.transform,"EquipItem"):GetComponent(typeof(CItemCountUpdate))
    self.m_KeDaoCountUpdate.templateId=CLuaEquipKeZiWnd.m_TemplateId
    self.m_KeDaoCountUpdate.format="{0}/1"

    UIEventListener.Get(self.m_KeDaoCountUpdate.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(CLuaEquipKeZiWnd.m_TemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)

    self.m_KeDaoCountUpdate.onChange=DelegateFactory.Action_int(function (val)
        if val==0 then
            self.m_KeDaoCountUpdate.format="[ff0000]{0}[-]/1"
            self.m_CountInput:SetMinMax(0,0,1)
        else
            self.m_KeDaoCountUpdate.format="{0}/1"
            self.m_CountInput:SetMinMax(1,val,1)
        end
    end)
    self.m_KeDaoCountUpdate:UpdateCount()
end

function CLuaEquipKeZiWnd.GetEquipList()
	local stringList =  CreateFromClass(MakeGenericClass(List,cs_string))

	local function checkEquip(itemId)
		local equip = CItemMgr.Inst:GetById(itemId)
        if equip and equip.IsBinded then
            local data = EquipmentTemplate_Equip.GetData(equip.TemplateId)
            local type = data and data.Type or 0
            if CLuaEquipKeZiWnd.m_FilterEquipTypes[type] then
				return true
			end
		end
		return false
	end
	--身上的
	local equipListOnBody = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Body)
	for i=1,equipListOnBody.Count do
		local itemId=equipListOnBody[i-1].itemId
        if checkEquip(itemId) then		
            CommonDefs.ListAdd(stringList,typeof(string),itemId)
        end
	end
	local list={}
	local equipListOnPackage = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Bag)
	for i=1,equipListOnPackage.Count do
		local itemId=equipListOnPackage[i-1].itemId
        if checkEquip(itemId) then
            table.insert( list, itemId )
        end
	end

	table.sort( list, function(t1,t2)
		local equip1 = CItemMgr.Inst:GetById(t1)
		local equip2 = CItemMgr.Inst:GetById(t2)

		if equip1 and equip2 then
            local quality1=EnumToInt(equip1.Equip.QualityType)
            local quality2=EnumToInt(equip2.Equip.QualityType)
            if quality1>quality2 then
                return true
            elseif quality1<quality2 then
                return false
            else
                return equip1.Id>equip2.Id
            end
		end
		return true
	end)
	for i,v in ipairs(list) do
		CommonDefs.ListAdd(stringList,typeof(string),v)
	end
	return stringList
end

function CLuaEquipKeZiWnd:OnEnable()
    g_ScriptEvent:AddListener("SculptureEquipSuccess",self,"OnSculptureEquipSuccess")
end

function CLuaEquipKeZiWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SculptureEquipSuccess",self,"OnSculptureEquipSuccess")
end

function CLuaEquipKeZiWnd:OnSculptureEquipSuccess()
    CUIManager.CloseUI(CLuaUIResources.EquipKeZiWnd)
end

function CLuaEquipKeZiWnd:DoRequestKeZi()
    local content=self.m_Input.value
    if not content or content=="" then
        g_MessageMgr:ShowMessage("Equip_Sculpture_No_Content")
        return
    end
    local len=CUICommonDef.GetStrByteLength(content)
    if len>30 then
        g_MessageMgr:ShowMessage("Equip_Sculpture_Content_ToMuch")
        return
    end
    if not CWordFilterMgr.Inst:CheckLinkAndMatch(content) then
        g_MessageMgr:ShowMessage("Equip_Sculpture_Content_Illegal")
        return
    end

    local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(content,true)
    content = ret.msg

    if self.m_SelectedEquipId==nil or self.m_SelectedEquipId=="" then
        g_MessageMgr:ShowMessage("Equip_Sculpture_Need_Select")
        return
    end

    local itemInfo=CItemMgr.Inst:GetItemInfo(self.m_SelectedEquipId)
    if not itemInfo then
        return
    end
    local place,pos=EnumToInt(itemInfo.place),itemInfo.pos

    if content then
        local find,kedaoPos,kedaoItemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, CLuaEquipKeZiWnd.m_TemplateId)
        if find then
            Gac2Gas.RequestSculptureEquip(self.m_SelectedEquipId,place,pos,content,CLuaEquipKeZiWnd.m_TemplateId,self.m_Count)
        else
            g_MessageMgr:ShowMessage("Equip_Sculpture_No_KeDao")
        end
    end
end
