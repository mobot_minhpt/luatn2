require("common/common_include")
require("ui/lingshou/LuaLingShouMgr")
local LuaGameObject=import "LuaGameObject"
local CItemMgr=import "L10.Game.CItemMgr"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local AlignType=import "L10.UI.CTooltip+AlignType"
local LingShouBaby_BabyTemplate=import "L10.Game.LingShouBaby_BabyTemplate"
local MessageMgr=import "L10.Game.MessageMgr"

CLuaLingShouBabyWashDetailView =class()

RegistClassMember(CLuaLingShouBabyWashDetailView,"m_WashButton")
RegistClassMember(CLuaLingShouBabyWashDetailView,"m_WashScoreLabel")
RegistClassMember(CLuaLingShouBabyWashDetailView,"m_AutoWashButton")
RegistClassMember(CLuaLingShouBabyWashDetailView,"m_LingShouIcon")

RegistClassMember(CLuaLingShouBabyWashDetailView,"m_BiliuluIcon")
RegistClassMember(CLuaLingShouBabyWashDetailView,"m_BiliuluLabel")
RegistClassMember(CLuaLingShouBabyWashDetailView,"m_BiliuluCountUpdate")
RegistClassMember(CLuaLingShouBabyWashDetailView,"m_BiliuluMask")

RegistClassMember(CLuaLingShouBabyWashDetailView,"m_LSBBAdjHp")
RegistClassMember(CLuaLingShouBabyWashDetailView,"m_MaxAdjHp")


function CLuaLingShouBabyWashDetailView:ctor()
end

function CLuaLingShouBabyWashDetailView:Init( tf )
    self.m_LSBBAdjHp=0
    self.m_MaxAdjHp=0

    self.m_BiliuluIcon=LuaGameObject.GetChildNoGC(tf,"BiliuluIcon").cTexture
    self.m_BiliuluLabel=LuaGameObject.GetChildNoGC(tf,"BiliuluLabel").label
    self.m_BiliuluMask=LuaGameObject.GetChildNoGC(tf,"BiliuluMask").gameObject
    --数量刷新
    self.m_BiliuluCountUpdate=LuaGameObject.GetChildNoGC(tf,"BiliuluCountUpdate").itemCountUpdate
    self.m_BiliuluCountUpdate.templateId=CLuaLingShouMgr.BiLiuLuId
    self.m_BiliuluCountUpdate.excludeExpireTime = true
    self.m_BiliuluCountUpdate.format="{0}/1"         
    local function OnCountChange(val)
        if val==0 then
            self.m_BiliuluCountUpdate.format="[ff0000]{0}[-]/1"
            self.m_BiliuluMask:SetActive(true)
        else
            self.m_BiliuluCountUpdate.format="{0}/1"
            self.m_BiliuluMask:SetActive(false)
        end
    end
    self.m_BiliuluCountUpdate.onChange=DelegateFactory.Action_int(OnCountChange)
    self.m_BiliuluCountUpdate:UpdateCount()
    local data = CItemMgr.Inst:GetItemTemplate(CLuaLingShouMgr.BiLiuLuId)
    if data then
        self.m_BiliuluIcon:LoadMaterial(data.Icon)
        self.m_BiliuluLabel.text=data.Name
    else
        self.m_BiliuluIcon:Clear()
        self.m_BiliuluLabel=" "
    end
    local g = LuaGameObject.GetChildNoGC(tf,"BiliuluCountUpdate").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_BiliuluCountUpdate.count==0 then
            CItemAccessListMgr.Inst:ShowItemAccessInfo(CLuaLingShouMgr.BiLiuLuId, false, go.transform, AlignType.Right)
       end
    end)

    self.m_WashButton=LuaGameObject.GetChildNoGC(tf,"WashBtn").gameObject

    self.m_AutoWashButton=LuaGameObject.GetChildNoGC(tf,"AutoWashBtn").gameObject

    -- self.m_LingShouNameLabel=LuaGameObject.GetChildNoGC(tf,"LingShouNameLabel").label
    -- self.m_LingShouNameLabel.text=" "
    -- self.matNameLabel=LuaGameObject.GetChildNoGC(tf,"MatNameLabel").label
    -- self.matNameLabel.text=" "
    self.m_LingShouIcon=LuaGameObject.GetChildNoGC(tf,"LingShouIcon").cTexture
    self.m_LingShouIcon:Clear()

    self.m_WashScoreLabel=LuaGameObject.GetChildNoGC(tf,"WashScoreLabel").label

    local g = LuaGameObject.GetChildNoGC(tf,"TipButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        MessageMgr.Inst:ShowMessage("LINGSHOU_BABY_XILIAN_TIP", {self.m_LSBBAdjHp,self.m_MaxAdjHp})
    end)
    local g = LuaGameObject.GetChildNoGC(tf,"WashBtn").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.LingShouBabyHandWashWnd)
    end)
    local g = LuaGameObject.GetChildNoGC(tf,"AutoWashBtn").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.LingShouBabyAutoWashWnd)
    end)
end

function CLuaLingShouBabyWashDetailView:InitData(babyInfo,lingShouPropertyFight)
    self.m_WashScoreLabel.text=SafeStringFormat3(LocalString.GetString("洗炼积分:%d"),babyInfo.WashCount)
    local babyData=LingShouBaby_BabyTemplate.GetData(babyInfo.TemplateId)
    if babyData then
        self.m_LingShouIcon:LoadNPCPortrait(babyData.Portrait)
        self.m_MaxAdjHp=CLuaLingShouMgr.GetMaxAdjHp(babyInfo.Level)
    else
        self.m_LingShouIcon:Clear()
    end
    if lingShouPropertyFight then
        self.m_LSBBAdjHp=CLuaLingShouMgr.GetLSBBAdjHp(babyInfo.Level,babyInfo.Quality)--lingShouPropertyFight:GetParam(EPlayerFightProp.LSBBAdjHp)
    end
end

function CLuaLingShouBabyWashDetailView:OnUpdateLingShouBabyLastWashResult(lingShouId,babyQuality,babySkillCls1,babySkillCls2,washCount)
    self.m_WashScoreLabel.text=SafeStringFormat3(LocalString.GetString("洗炼积分:%d"),washCount)
end


return CLuaLingShouBabyWashDetailView
