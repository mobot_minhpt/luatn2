local CGuideBubbleDisplay=import "L10.UI.CGuideBubbleDisplay"
local CUIClipTexture=import "CUIClipTexture"

CLuaGuideRegionWnd = class()

RegistClassMember(CLuaGuideRegionWnd,"m_Tick")
-- RegistClassMember(CLuaGuideRegionWnd,"m_BubbleNode")
-- RegistChildComponent(CLuaFurnitureSmallTypeItem,"m_ClipRegion","ClipRegion",UIWidget)


CLuaGuideRegionWnd.s_EventId = 0
CLuaGuideRegionWnd.s_Duration = 0
CLuaGuideRegionWnd.s_Params = nil
CLuaGuideRegionWnd.s_Text = nil

function CLuaGuideRegionWnd:Awake()
-- self.m_ClipRegion = self.transform:Find("GuideClip/Texture/ClipRegion")
end


function CLuaGuideRegionWnd:Init()
    local clipTexture = self.transform:Find("GuideClip/Texture"):GetComponent(typeof(CUIClipTexture))
    local clipPos = CLuaGuideRegionWnd.s_Params.clipPos
    local clipSize = CLuaGuideRegionWnd.s_Params.clipSize
    clipTexture:InitRegion(clipPos[1],clipPos[2],clipSize[1],clipSize[2])

    local bubbleDisplay = self.transform:Find("GuideClip/Bubble"):GetComponent(typeof(CGuideBubbleDisplay))
    bubbleDisplay.anchor = CLuaGuideRegionWnd.s_Params.textSide
    bubbleDisplay:Init(CLuaGuideRegionWnd.s_Text)

    local pos = CLuaGuideRegionWnd.s_Params.textPos
    bubbleDisplay:SetLocalPosition(Vector3(pos[1],pos[2]))

    local region = self.transform:Find("GuideClip/Texture/Anchor1/Region1").gameObject
    UIEventListener.Get(region).onClick = DelegateFactory.VoidDelegate(function(go)
        self:End()
    end)
    
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick=nil
    end
    self.m_SendMailFxTick = RegisterTickOnce(function()
        self:End()
    end,CLuaGuideRegionWnd.s_Duration*1000)
end

function CLuaGuideRegionWnd:End()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick=nil
    end
    Gac2Gas.OnClientGuideEvent(CLuaGuideRegionWnd.s_EventId)
    CUIManager.CloseUI(CLuaUIResources.GuideRegionWnd)
end

function CLuaGuideRegionWnd:OnDestroy()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick=nil
    end
end
