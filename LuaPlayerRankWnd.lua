local DelegateFactory       = import "DelegateFactory"
local Profession            = import "L10.Game.Profession"
local CPlayerInfoMgr        = import "CPlayerInfoMgr"
local CRankData             = import "L10.UI.CRankData"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel            = import "L10.Game.EChatPanel"
local AlignType             = import "CPlayerInfoMgr+AlignType"

LuaPlayerRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaPlayerRankWnd, "head1")
RegistClassMember(LuaPlayerRankWnd, "head2")
RegistClassMember(LuaPlayerRankWnd, "head3")
RegistClassMember(LuaPlayerRankWnd, "myRank")
RegistClassMember(LuaPlayerRankWnd, "myScore")
RegistClassMember(LuaPlayerRankWnd, "myName")
RegistClassMember(LuaPlayerRankWnd, "myClass")
RegistClassMember(LuaPlayerRankWnd, "tableViewNormal")
RegistClassMember(LuaPlayerRankWnd, "tableViewWithBottomLabel")
RegistClassMember(LuaPlayerRankWnd, "title")
RegistClassMember(LuaPlayerRankWnd, "bottomLabel")

RegistClassMember(LuaPlayerRankWnd, "rankList")
RegistClassMember(LuaPlayerRankWnd, "rankSpriteTbl")
RegistClassMember(LuaPlayerRankWnd, "tableView")

function LuaPlayerRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitMainPlayerInfo()
end

function LuaPlayerRankWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.head1 = anchor:Find("Header/Head1"):GetComponent(typeof(UILabel))
    self.head2 = anchor:Find("Header/Head2"):GetComponent(typeof(UILabel))
    self.head3 = anchor:Find("Header/Head3"):GetComponent(typeof(UILabel))
    self.myRank = anchor:Find("MainPlayerInfo/Rank"):GetComponent(typeof(UILabel))
    self.myName = anchor:Find("MainPlayerInfo/Name"):GetComponent(typeof(UILabel))
    self.myScore = anchor:Find("MainPlayerInfo/Score"):GetComponent(typeof(UILabel))
    self.myClass = anchor:Find("MainPlayerInfo/Class"):GetComponent(typeof(UISprite))
    self.tableViewNormal = anchor:Find("TableViewNormal"):GetComponent(typeof(QnTableView))
    self.tableViewWithBottomLabel = anchor:Find("TableViewWithBottomLabel"):GetComponent(typeof(QnTableView))
    self.bottomLabel = anchor:Find("TableViewWithBottomLabel/BottomLabel"):GetComponent(typeof(UILabel))
    self.title = self.transform:Find("Wnd_Bg_Secondary_1/TitleLabel"):GetComponent(typeof(UILabel))
end


function LuaPlayerRankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaPlayerRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaPlayerRankWnd:OnRankDataReady()
    if CLuaRankData.m_CurRankId ~= LuaPlayerRankMgr.info.rankId then return end
    local myInfo = CRankData.Inst.MainPlayerRankInfo
    self.myRank.text = myInfo.Rank > 0 and myInfo.Rank or LocalString.GetString("未上榜")
    self.myScore.text = myInfo.Value > 0 and myInfo.Value or LocalString.GetString("—")

    self.rankList = {}
    for i = 1, CRankData.Inst.RankList.Count do
        table.insert(self.rankList, CRankData.Inst.RankList[i - 1])
    end
    self.tableView:ReloadData(true, false)
end


function LuaPlayerRankWnd:Init()
    if not System.String.IsNullOrEmpty(LuaPlayerRankMgr.info.bottomText) then
        self.bottomLabel.text = LuaPlayerRankMgr.info.bottomText
        self.tableView = self.tableViewWithBottomLabel
        self.tableViewNormal.gameObject:SetActive(false)
    else
        self.tableView = self.tableViewNormal
        self.tableViewWithBottomLabel.gameObject:SetActive(false)
    end
    self.tableView.gameObject:SetActive(true)
    self:InitTableView()

    self.head1.text = LuaPlayerRankMgr.info.head1Text
    self.head2.text = LuaPlayerRankMgr.info.head2Text
    self.head3.text = LuaPlayerRankMgr.info.head3Text

    Gac2Gas.QueryRank(LuaPlayerRankMgr.info.rankId)
end

function LuaPlayerRankWnd:InitMainPlayerInfo()
    self.myRank.text = LocalString.GetString("未上榜")
    self.myName.text = CClientMainPlayer.Inst.RealName
    self.myScore.text = LocalString.GetString("—")
    self.myClass.spriteName = Profession.GetIconByNumber(EnumToInt(CClientMainPlayer.Inst.Class))
end

function LuaPlayerRankWnd:InitTableView()
    self.rankList = {}
    self.rankSpriteTbl = {g_sprites.RankFirstSpriteName, g_sprites.RankSecondSpriteName, g_sprites.RankThirdSpriteName}
    self.tableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.rankList
        end,
        function(item, index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

            self:InitItem(item, index)
        end
    )
    self.tableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local data = self.rankList[row + 1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, "", nil, Vector3.zero, AlignType.Default)
        end
    end)
end

function LuaPlayerRankWnd:InitItem(item, index)
    local tf = item.transform

    local name = tf:Find("Name"):GetComponent(typeof(UILabel))
    local rankNum = tf:Find("Rank"):GetComponent(typeof(UILabel))
    local rankSprite = tf:Find("Rank/Sprite"):GetComponent(typeof(UISprite))
    local score = tf:Find("Score"):GetComponent(typeof(UILabel))
    local class = tf:Find("Class"):GetComponent(typeof(UISprite))

    local info = self.rankList[index + 1]
    local rank = info.Rank

    rankNum.text = ""
    rankSprite.spriteName = ""
    if rank >= 1 and rank <= 3 then
        rankSprite.spriteName = self.rankSpriteTbl[rank]
    else
        rankNum.text = rank
    end

    name.text = info.Name
    score.text = info.Value
    class.spriteName = Profession.GetIconByNumber(EnumToInt(info.Job))
end

--@region UIEvent
--@endregion UIEvent


LuaPlayerRankMgr = {}

LuaPlayerRankMgr.info = {}

function LuaPlayerRankMgr:ShowPlayerRankWnd(rankId, title, head1Text, head2Text, head3Text, bottomText)
    self.info = {}
    self.info.rankId = rankId
    self.info.title = System.String.IsNullOrEmpty(title) and LocalString.GetString("排行榜") or title
    self.info.head1Text = System.String.IsNullOrEmpty(head1Text) and LocalString.GetString("排名") or head1Text
    self.info.head2Text = System.String.IsNullOrEmpty(head2Text) and LocalString.GetString("玩家名称") or head2Text
    self.info.head3Text = System.String.IsNullOrEmpty(head3Text) and LocalString.GetString("分数") or head3Text
    self.info.bottomText = bottomText
    CUIManager.ShowUI(CLuaUIResources.PlayerRankWnd)
end
