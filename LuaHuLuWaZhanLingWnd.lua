local CUIFx = import "L10.UI.CUIFx"

local UITabBar = import "L10.UI.UITabBar"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local QnTableView = import "L10.UI.QnTableView"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local NGUIMath = import "NGUIMath"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local TweenRotation = import "TweenRotation"

LuaHuLuWaZhanLingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHuLuWaZhanLingWnd, "JiFenShangDianBtn", "JiFenShangDianBtn", GameObject)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "UnLockDanLuBtn", "UnLockDanLuBtn", GameObject)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "AddFireBtn", "AddFireBtn", GameObject)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "TopReward1", "TopReward1", GameObject)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "TopReward2", "TopReward2", GameObject)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "TopReward3", "TopReward3", GameObject)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "FastCookBtn", "FastCookBtn", GameObject)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "RePositionBtn", "RePositionBtn", GameObject)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "Level1Tab", "Level1Tab", GameObject)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "Level2Tab", "Level2Tab", GameObject)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "Level3Tab", "Level3Tab", GameObject)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "VipLock1", "VipLock1", GameObject)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "VipLock2", "VipLock2", GameObject)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "Reward1", "Reward1", GameObject)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "Reward2", "Reward2", GameObject)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "TopRewardUnlockLabel", "TopRewardUnlockLabel", UILabel)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "OpenningTimeLabel", "OpenningTimeLabel", UILabel)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "DanLuLevel", "DanLuLevel", UILabel)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "FireCountLabel", "FireCountLabel", UILabel)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "Vip1Fx", "Vip1Fx", CUIFx)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "Vip2Fx", "Vip2Fx", CUIFx)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "HuoYanFx", "HuoYanFx", CUIFx)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "NormalFx", "NormalFx", CUIFx)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "AlertBtnFx", "AlertBtnFx", CUIFx)
RegistChildComponent(LuaHuLuWaZhanLingWnd, "AddFireBtnTexture", "AddFireBtnTexture", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaHuLuWaZhanLingWnd,"m_CurLevel")
RegistClassMember(LuaHuLuWaZhanLingWnd,"m_ShowLevel")
RegistClassMember(LuaHuLuWaZhanLingWnd,"m_InitTopLevelInView")
RegistClassMember(LuaHuLuWaZhanLingWnd,"m_TotalProgress")
RegistClassMember(LuaHuLuWaZhanLingWnd,"m_InitScrollPosy")
RegistClassMember(LuaHuLuWaZhanLingWnd,"m_PerItemHeight")
function LuaHuLuWaZhanLingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.JiFenShangDianBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJiFenShangDianBtnClick()
	end)


	
	UIEventListener.Get(self.UnLockDanLuBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUnLockDanLuBtnClick()
	end)


	
	UIEventListener.Get(self.AddFireBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddFireBtnClick()
	end)


	
	UIEventListener.Get(self.FastCookBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFastCookBtnClick()
	end)


	
	UIEventListener.Get(self.RePositionBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRePositionBtnClick()
	end)


	
	UIEventListener.Get(self.Level1Tab.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLevel1TabClick()
	end)


	
	UIEventListener.Get(self.Level2Tab.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLevel2TabClick()
	end)


	
	UIEventListener.Get(self.Level3Tab.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLevel3TabClick()
	end)


    --@endregion EventBind end
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)
	UIEventListener.Get(self.Reward1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReward1Click()
	end)
	UIEventListener.Get(self.Reward2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReward2Click()
	end)
	self.m_InitTopLevelInView = 4
	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #LuaHuLuWa2022Mgr.m_ZhanLingData
        end,
        function(item,row)
            self:InitItem(item,row)
        end)
end

function LuaHuLuWaZhanLingWnd:OnEnable()
    g_ScriptEvent:AddListener("ReceiveLianDanLuRewardSuccess", self, "OnReceiveLianDanLuRewardSuccess")
	g_ScriptEvent:AddListener("UnLockLianDanLuVipSuccess", self, "OnUnLockLianDanLuVipSuccess")
	g_ScriptEvent:AddListener("SyncLianDanLuPlayData", self, "Init")
end

function LuaHuLuWaZhanLingWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReceiveLianDanLuRewardSuccess", self, "OnReceiveLianDanLuRewardSuccess")
	g_ScriptEvent:RemoveListener("UnLockLianDanLuVipSuccess", self, "OnUnLockLianDanLuVipSuccess")
	g_ScriptEvent:RemoveListener("SyncLianDanLuPlayData", self, "Init")
end

function LuaHuLuWaZhanLingWnd:Init()
	self.m_CurLevel,self.m_TotalProgress = LuaHuLuWa2022Mgr.GetCurZhanLingLevel()

	self.DanLuLevel.text = self.m_CurLevel
	local nextLevel = self.m_CurLevel + 1
	local nextProgress = LuaHuLuWa2022Mgr.m_Level2NeedProgress[nextLevel]
	if nextProgress then
		self.FireCountLabel.text = SafeStringFormat3("%d/%d",self.m_TotalProgress,nextProgress)
	else
		--满级大佬了
		self.FireCountLabel.gameObject:SetActive(false)
	end
	
	self:InitLevelTabs()
	self.TableView:ReloadData(false,false)

	self.OpenningTimeLabel.text = HuluBrothers_Setting.GetData().ActivityOpenLastTime
	--fx
	if LuaHuLuWa2022Mgr.m_IsVip2 then
		self.HuoYanFx:LoadFx("fx/ui/prefab/UI_zhanling_huo03.prefab")--大火
		self.Vip2Fx:LoadFx("fx/ui/prefab/UI_zhanling_guang_zi.prefab")
		self.transform:Find("Left/Texture/Texture").gameObject:SetActive(false)
		self.transform:Find("Left/Texture/Texture (2)").gameObject:SetActive(false)
	else
		self.HuoYanFx:LoadFx("fx/ui/prefab/UI_zhanling_huo03.prefab")--小火
	end
	if LuaHuLuWa2022Mgr.m_IsVip1 then
		self.Vip1Fx:LoadFx("fx/ui/prefab/UI_zhanling_guang_hong.prefab")
		self.transform:Find("Left/Texture/Texture (1)").gameObject:SetActive(false)
		self.transform:Find("Left/Texture/Texture (3)").gameObject:SetActive(false)
	end
	if LuaHuLuWa2022Mgr.m_IsFirstOpenZhanLing then
		self.AlertBtnFx:LoadFx("fx/ui/prefab/UI_zhanling_danluzhuling.prefab")
		self.AddFireBtnTexture:SetActive(false)
	else
		self.AlertBtnFx:DestroyFx()
		self.AddFireBtnTexture:SetActive(true)
	end

	self.NormalFx:LoadFx("fx/ui/prefab/UI_zhanling_guang_lan.prefab")

	self:SetScrollPos()
	self.m_InitScrollPosy = self.ScrollView.mTrans.localPosition.y
end

function LuaHuLuWaZhanLingWnd:SetScrollPos()
	self.ScrollView:SetDragAmount(0,1-(self.m_CurLevel-1) / 49,false)
	self.m_InitTopLevelInView = self.m_CurLevel
end

function LuaHuLuWaZhanLingWnd:SetScrollPosAtLevel(level)
	self.ScrollView:SetDragAmount(0,1-(level-1) / 49,false)
	self.m_InitTopLevelInView = level
end

function LuaHuLuWaZhanLingWnd:Update()
	if not self.m_InitScrollPosy or not self.m_PerItemHeight then
		return
	end
	local cury = self.ScrollView.mTrans.localPosition.y
	local offset = (self.m_InitScrollPosy - cury)
	local count = offset / self.m_PerItemHeight
	local newShowLevel = count + self.m_InitTopLevelInView
	if newShowLevel ~= self.m_ShowLevel then
		self.m_ShowLevel = count + self.m_InitTopLevelInView
		self:InitTopRewardView()
	end
end

function LuaHuLuWaZhanLingWnd:InitItem(item,row)
	local data = LuaHuLuWa2022Mgr.m_ZhanLingData[row+1]
	local level = data.level
	local flag = data.flag
	local itemScript = item.transform:GetComponent(typeof(CCommonLuaScript))
	itemScript.m_LuaSelf:Init(level,flag)

	if not self.m_PerItemHeight then
		local b = NGUIMath.CalculateRelativeWidgetBounds(item.transform)
		self.m_PerItemHeight = b.size.y
	end
end

function LuaHuLuWaZhanLingWnd:InitLevelTabs()
	self.VipLock2:SetActive(not LuaHuLuWa2022Mgr.m_IsVip2)
	self.VipLock1:SetActive(not LuaHuLuWa2022Mgr.m_IsVip1)
end

function LuaHuLuWaZhanLingWnd:InitTopRewardView()
	local topIndex
	if self.m_ShowLevel % 7 == 0 then
		topIndex = self.m_ShowLevel + 7
	else
		topIndex = math.floor(self.m_ShowLevel / 7)
		topIndex = (topIndex+1)*7
	end

	if not LuaHuLuWa2022Mgr.m_ZhanLingData[topIndex] then
		topIndex = topIndex - 7
	end
	local data = LuaHuLuWa2022Mgr.m_ZhanLingData[topIndex]
	if data then
		local level = data.level
		local flag = data.flag
		local st = SafeStringFormat3(LocalString.GetString("%d可解锁"),level)
		self.TopRewardUnlockLabel.text = LocalString.CnStrH2V(st,true)
		self:InitTopRewardItem(self.TopReward1.transform,level,1,flag)
		self:InitTopRewardItem(self.TopReward2.transform,level,2,flag)
		self:InitTopRewardItem(self.TopReward3.transform,level,3,flag)
	end
end
function LuaHuLuWaZhanLingWnd:InitTopRewardItem(item,level,idx,flag)
    local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local countLabel = item.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
    local lock = item.transform:Find("Lock").gameObject
    local hasGotTag = item.transform:Find("HasGotTag").gameObject
    local mask = item.transform:Find("Mask").gameObject

    local data = ZhanLing_Reward.GetData(level)
    local rewards
    local isVipUnLock = true
    if idx == 1 then
        rewards = data.ItemRewards1
    elseif idx == 2 then
        rewards = data.ItemRewards2
        isVipUnLock = LuaHuLuWa2022Mgr.m_IsVip1
    elseif idx == 3 then
        rewards = data.ItemRewards3
        isVipUnLock = LuaHuLuWa2022Mgr.m_IsVip2
    end
	local itemId
    if rewards then
        itemId = rewards[0]
        local count = rewards[1]
        local itemdata = Item_Item.GetData(itemId)
        icon:LoadMaterial(itemdata.Icon)
        countLabel.text = count > 1 and count or nil
    end

    --丹炉是否被解锁
    lock:SetActive(not isVipUnLock)

    local isUnlock = false
    local isAwarded = false
    if isVipUnLock then
        if level <= self.m_CurLevel then
            isUnlock = true
        end 
    end
    mask:SetActive(not isVipUnLock or not isUnlock)

    local bitmask = 2^(idx-1)
    local isGot = false
    if bit.band(flag,bitmask) ~= 0 then
        isGot = true
    else
        isGot = false
    end
    hasGotTag:SetActive(isGot)
    --可领取
    
    if isUnlock and isVipUnLock and not isGot then
        UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            Gac2Gas.RequestGetLianDanLuReward(level,idx)
        end)
	else
		UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false,nil,AlignType.Default, 0, 0, 0, 0)
        end)
    end
end

function LuaHuLuWaZhanLingWnd:OnReceiveLianDanLuRewardSuccess(level,index)
	if level == 0 and index == 0 then
		for i=1,self.m_CurLevel,1 do
			local data =  LuaHuLuWa2022Mgr.m_ZhanLingData[i]
			if not data then
				data = {}
			end
			local flag = 1
			if LuaHuLuWa2022Mgr.m_IsVip1 then
				flag = flag + 2
			end
			if LuaHuLuWa2022Mgr.m_IsVip2 then
				flag = flag + 4
			end
			data.flag = flag
			data.level = i
		end
		self.TableView:ReloadData(false,false)
		self:SetScrollPos()
	else
		local data = LuaHuLuWa2022Mgr.m_ZhanLingData[level]
		if data then
			local flag = data.flag
			data.flag = bit.bor(flag,2^(index-1))
		end
		self.TableView:ReloadData(false,false)
		self:SetScrollPosAtLevel(level)
	end
end

function LuaHuLuWaZhanLingWnd:OnUnLockLianDanLuVipSuccess(vip)
	if vip == 1 then
		LuaHuLuWa2022Mgr.m_IsVip1 = true
		self.Vip1Fx:LoadFx("fx/ui/prefab/UI_zhanling_guang_hong.prefab")
	elseif vip == 2 then
		LuaHuLuWa2022Mgr.m_IsVip2 = true
		self.Vip2Fx:LoadFx("fx/ui/prefab/UI_zhanling_guang_zi.prefab")
		self.HuoYanFx:LoadFx("fx/ui/prefab/UI_zhanling_huo03.prefab")--01 03
	end
	self:Init()
end

--@region UIEvent

function LuaHuLuWaZhanLingWnd:OnJiFenShangDianBtnClick()
	CLuaNPCShopInfoMgr.ShowScoreShopById(34000086)
end

function LuaHuLuWaZhanLingWnd:OnUnLockDanLuBtnClick()
	LuaHuLuWa2022Mgr.OpenUnLockZhanLingWnd()
end

function LuaHuLuWaZhanLingWnd:OnAddFireBtnClick()
	LuaHuLuWa2022Mgr.OpenZhanLingShouCeWnd()
	LuaHuLuWa2022Mgr.m_IsFirstOpenZhanLing = false
	self.AlertBtnFx:DestroyFx()
	self.AddFireBtnTexture:SetActive(true)
end

function LuaHuLuWaZhanLingWnd:OnTipBtnClick()
	g_MessageMgr:ShowMessage("DanLu_ZhanLing_RuleTip")
end

function LuaHuLuWaZhanLingWnd:OnFastCookBtnClick()
	Gac2Gas.RequestGetLianDanLuReward(0,0)
end

function LuaHuLuWaZhanLingWnd:OnRePositionBtnClick()
	self:SetScrollPos()
end

function LuaHuLuWaZhanLingWnd:OnLevel1TabClick()
end

function LuaHuLuWaZhanLingWnd:OnLevel2TabClick()
	if LuaHuLuWa2022Mgr.m_IsVip1 then
		return
	end
	LuaHuLuWa2022Mgr.OpenUnLockZhanLingWnd()
end

function LuaHuLuWaZhanLingWnd:OnLevel3TabClick()
	if LuaHuLuWa2022Mgr.m_IsVip2 then
		return
	end
	LuaHuLuWa2022Mgr.OpenUnLockZhanLingWnd()
end


function LuaHuLuWaZhanLingWnd:OnReward1Click()
	local showPrize = ZhanLing_Setting.GetData().ShowPrize
	CItemInfoMgr.ShowLinkItemTemplateInfo(showPrize[1],false,nil,AlignType.Default, 0, 0, 0, 0)
end

function LuaHuLuWaZhanLingWnd:OnReward2Click()
	local showPrize = ZhanLing_Setting.GetData().ShowPrize
	CItemInfoMgr.ShowLinkItemTemplateInfo(showPrize[0],false,nil,AlignType.Default, 0, 0, 0, 0)
end


--@endregion UIEvent
