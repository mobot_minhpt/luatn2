-- Auto Generated!!
local CAppStoreReviewBox = import "L10.UI.CAppStoreReviewBox"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLingShouAutoWashWnd = import "L10.UI.CLingShouAutoWashWnd"
local CLingShouBaseMgr = import "L10.Game.CLingShouBaseMgr"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CLingShouProp = import "L10.Game.CLingShouProp"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local LingShouDetails = import "L10.Game.CLingShouBaseMgr+LingShouDetails"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Object = import "System.Object"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CLingShouAutoWashWnd.m_Request_CS2LuaHook = function (this, go) 
    if this.Recommend then
        local content = g_MessageMgr:FormatMessage("LINGSHOU_XIBAOBAO_CONFIRM")
        MessageWndManager.ShowOKCancelMessage(content, DelegateFactory.Action(function () 
            this.autoIntensifyRoutine = this:StartCoroutine(this:StartWash())
        end), nil, nil, nil, false)
    else
        this.autoIntensifyRoutine = this:StartCoroutine(this:StartWash())
    end
end
CLingShouAutoWashWnd.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.LingShouWashResult, MakeDelegateFromCSFunction(this.OnLingShouWashResult, MakeGenericClass(Action2, String, CLingShouProp), this))
    EventManager.AddListenerInternal(EnumEventType.GetLingShouDetails, MakeDelegateFromCSFunction(this.UpdateDetails, MakeGenericClass(Action2, String, LingShouDetails), this))

    EventManager.AddListenerInternal(EnumEventType.AcceptWashedLingShou, MakeDelegateFromCSFunction(this.AcceptWashedLingShou, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.Guide_ChangeView, MakeDelegateFromCSFunction(this.ProcessUpdateViewEvent, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.StopAutoWashLingShou, MakeDelegateFromCSFunction(this.OnStopWashLingShou, MakeGenericClass(Action1, String), this))
end
CLingShouAutoWashWnd.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.LingShouWashResult, MakeDelegateFromCSFunction(this.OnLingShouWashResult, MakeGenericClass(Action2, String, CLingShouProp), this))
    EventManager.RemoveListenerInternal(EnumEventType.GetLingShouDetails, MakeDelegateFromCSFunction(this.UpdateDetails, MakeGenericClass(Action2, String, LingShouDetails), this))
    EventManager.RemoveListenerInternal(EnumEventType.AcceptWashedLingShou, MakeDelegateFromCSFunction(this.AcceptWashedLingShou, MakeGenericClass(Action1, String), this))

    EventManager.RemoveListenerInternal(EnumEventType.Guide_ChangeView, MakeDelegateFromCSFunction(this.ProcessUpdateViewEvent, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.StopAutoWashLingShou, MakeDelegateFromCSFunction(this.OnStopWashLingShou, MakeGenericClass(Action1, String), this))
end
CLingShouAutoWashWnd.m_OnStopWashLingShou_CS2LuaHook = function (this, lingShouId) 
    if this.failAction ~= nil then
        GenericDelegateInvoke(this.failAction, Table2ArrayWithCount({lingShouId}, 1, MakeArrayClass(Object)))
        --CUICommonDef.SetActive(updateBtn, false);
    end
end
CLingShouAutoWashWnd.m_OnLingShouWashResult_CS2LuaHook = function (this, lingshouId, newProp) 
    this.hasResult = true
    if this.successAction ~= nil then
        GenericDelegateInvoke(this.successAction, Table2ArrayWithCount({lingshouId, newProp}, 2, MakeArrayClass(Object)))
        this.success = true
    else
        --显示结果
        local fightProp = CLingShouMgr.Inst:GetLingShouFightPropByLingShouProp(lingshouId, newProp)
        if fightProp == nil then
            return
        end
        this.display2:Init(newProp, fightProp)
        this.display2:InitPropDif(CLingShouMgr.Inst:GetLingShouDetails(lingshouId), fightProp)

        this:SetNewValues(newProp, fightProp)

        local pinzhi = EnumToInt(CLingShouBaseMgr.GetQuality(newProp.Quality))
        if pinzhi >= EnumLingShouQuality_lua.Yi then
            CAppStoreReviewBox.Show()
        end

        if this:GetCompareResult(lingshouId, newProp) then
            this.Recommend = true
        else
            this.Recommend = false
        end


        --可以替换
        CUICommonDef.SetActive(this.updateBtn, true, true)
    end
end
CLingShouAutoWashWnd.m_EndRequest_CS2LuaHook = function (this) 
    this.requestLabel.text = CLingShouAutoWashWnd.StrStartWash
    --CUICommonDef.SetActive(updateBtn, true);
    UIEventListener.Get(this.continueBtn).onClick = MakeDelegateFromCSFunction(this.Request, VoidDelegate, this)
    this:ClearActions()
    if this.success then
        CUICommonDef.SetActive(this.updateBtn, true, true)
    end
    this.success = false
end
CLingShouAutoWashWnd.m_ProcessUpdateViewEvent_CS2LuaHook = function (this, eventType) 

    if CClientMainPlayer.Inst == nil then
        this.pause = true
        return
    end

    --看一下这个窗口的类型
    local top = CUIManager.instance:GetTopPopUI()
    if top ~= nil then
        if top.Name == CUIResources.LingShouAutoWashWnd.Name then
            this.pause = false
        else
            this.pause = true
        end
    end
end
