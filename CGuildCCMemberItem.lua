-- Auto Generated!!
local CGuildCCMemberItem = import "L10.UI.CGuildCCMemberItem"
local CommonDefs = import "L10.Game.CommonDefs"
local EnumClass = import "L10.Game.EnumClass"
local LocalString = import "LocalString"
local Profession = import "L10.Game.Profession"
CGuildCCMemberItem.m_Init_CS2LuaHook = function (this, data) 
    if data.id > 0 then
        this.icon.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), data.cls))
        this.nameLabel.text = data.name
        this.levelLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(data.grade))
        this.icon.enabled = true
        this.nameLabel.enabled = true
        this.levelLabel.enabled = true
        this.defaultLabel.enabled = false
    else
        this.icon.enabled = false
        this.nameLabel.enabled = false
        this.levelLabel.enabled = false
        this.defaultLabel.enabled = true
    end
end
