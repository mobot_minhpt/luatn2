-- Auto Generated!!
local AssistantMessage = import "L10.Game.AssistantMessage"
local AssistantMessageType = import "L10.Game.AssistantMessageType"
local CContactAssistantAnswerWnd = import "L10.UI.CContactAssistantAnswerWnd"
local CContactAssistantChatView = import "L10.UI.CContactAssistantChatView"
local CContactAssistantWnd = import "L10.UI.CContactAssistantWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local IContactAssistantChatItem = import "L10.UI.IContactAssistantChatItem"
CContactAssistantChatView.m_NumberOfRows_CS2LuaHook = function (this) 
    if CContactAssistantWnd.Instance ~= nil then
        return CContactAssistantWnd.Instance.assistantMessageList.Count
    end
    if CContactAssistantAnswerWnd.Instance ~= nil then
        return CContactAssistantAnswerWnd.Instance.assistantMessageList.Count
    end
    return 0
end
CContactAssistantChatView.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 
    local msgs = CreateFromClass(MakeGenericClass(List, AssistantMessage))
    if CContactAssistantWnd.Instance ~= nil then
        msgs = CContactAssistantWnd.Instance.assistantMessageList
    elseif CContactAssistantAnswerWnd.Instance ~= nil then
        msgs = CContactAssistantAnswerWnd.Instance.assistantMessageList
    end

    if index < 0 and index >= msgs.Count then
        return nil
    end
    local cellIdentifier = nil
    local template = nil
    if msgs[index].msgType == AssistantMessageType.TimeSplit then
        cellIdentifier = "TimeSplitCell"
        template = this.timesplitTemplate
    elseif msgs[index].msgType == AssistantMessageType.Answer then
        cellIdentifier = "AnswerTemplate"
        template = this.answerTemplate
    elseif msgs[index].msgType == AssistantMessageType.Question then
        cellIdentifier = "QuestionTemplate"
        template = this.questionTemplate
    end
    if cellIdentifier == nil then
        return nil
    end

    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(template, cellIdentifier)
    end

    local chatItem = CommonDefs.GetComponent_GameObject_Type(cell, typeof(IContactAssistantChatItem))
    chatItem:Init(msgs[index])
    return cell
end

