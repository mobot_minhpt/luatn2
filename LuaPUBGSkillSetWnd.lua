require("3rdParty/ScriptEvent")
require("common/common_include")


local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UIGrid = import "UIGrid"
local CUITexture = import "L10.UI.CUITexture"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local Input = import "UnityEngine.Input"
local Application = import "UnityEngine.Application"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local Vector3 = import "UnityEngine.Vector3"
local TouchPhase = import "UnityEngine.TouchPhase"
local Skill_TempSkill = import "L10.Game.Skill_TempSkill"
local CSkillMgr = import "L10.Game.CSkillMgr"

LuaPUBGSkillSetWnd = class()

RegistChildComponent(LuaPUBGSkillSetWnd, "SkillItemTemplate", GameObject)
RegistChildComponent(LuaPUBGSkillSetWnd, "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaPUBGSkillSetWnd, "Grid", UIGrid)

RegistChildComponent(LuaPUBGSkillSetWnd, "CloneSkill", GameObject)

RegistChildComponent(LuaPUBGSkillSetWnd, "Button1st", GameObject)
RegistChildComponent(LuaPUBGSkillSetWnd, "Button2nd", GameObject)
RegistChildComponent(LuaPUBGSkillSetWnd, "Button3rd", GameObject)
RegistChildComponent(LuaPUBGSkillSetWnd, "Button4th", GameObject)
RegistChildComponent(LuaPUBGSkillSetWnd, "Button5th", GameObject)

RegistClassMember(LuaPUBGSkillSetWnd, "SKILLSIZE")
RegistClassMember(LuaPUBGSkillSetWnd, "SELECTEDSKILL")
RegistClassMember(LuaPUBGSkillSetWnd, "SkillItemList")
RegistClassMember(LuaPUBGSkillSetWnd, "SkillsToSelect")

RegistClassMember(LuaPUBGSkillSetWnd, "DraggedSkillId")
RegistClassMember(LuaPUBGSkillSetWnd, "IsDragging")
RegistClassMember(LuaPUBGSkillSetWnd, "LastPos")
RegistClassMember(LuaPUBGSkillSetWnd, "FingerIndex")
RegistClassMember(LuaPUBGSkillSetWnd, "SkillButtons")

function LuaPUBGSkillSetWnd:InitData()
    self.SKILLSIZE = 12
    self.SELECTEDSKILL = 5
    self.SkillItemList = {}
    self.DraggedSkillId = 0
    self.IsDragging = false
    self.LastPos = nil
    self.FingerIndex = -1
    self.SkillItemTemplate:SetActive(false)

    self.SkillButtons = {}
    table.insert(self.SkillButtons, self.Button1st)
    table.insert(self.SkillButtons, self.Button2nd)
    table.insert(self.SkillButtons, self.Button3rd)
    table.insert(self.SkillButtons, self.Button4th)
    table.insert(self.SkillButtons, self.Button5th)
end

function LuaPUBGSkillSetWnd:Init()
    self:InitData()
    self:InitSkillList()
    self:InitSelectedSkills()
end

-- 初始化可以选择的技能，SkillCls2Id中已经激活的临时技能
function LuaPUBGSkillSetWnd:InitSKillsToSelect()
    self.SkillsToSelect = {}

    CClientMainPlayer.Inst.SkillProp:ForeachOriginalSkillCls2Id(DelegateFactory.Action_KeyValuePair_uint_uint(function (pair)
        if CSkillMgr.Inst:IsTempSkill(pair.Key) then
            local tempSkill = Skill_TempSkill.GetData(pair.Key)
            if tempSkill and tempSkill.Position == 1 then
                table.insert(self.SkillsToSelect, pair.Key)
            end
        end
    end))
end


function LuaPUBGSkillSetWnd:InitSkillList()
    self:InitSKillsToSelect()

    self.CloneSkill:SetActive(false)
    Extensions.RemoveAllChildren(self.Grid.transform)
    self.SkillItemList = {}
    for i = 1, self.SKILLSIZE do
        local item = NGUITools.AddChild(self.Grid.gameObject, self.SkillItemTemplate)
        if i <= #self.SkillsToSelect then
            local skillId = Skill_AllSkills.GetSkillId(self.SkillsToSelect[i], 1)
            self:InitSkillItem(item, {skillId = skillId})
        else
            self:InitSkillItem(item, nil)
        end
        item:SetActive(true)
        table.insert(self.SkillItemList, item)
    end
end

function LuaPUBGSkillSetWnd:InitSkillItem(item, info)
    local SkillIcon = item.transform:Find("SkillIcon"):GetComponent(typeof(CUITexture))
    SkillIcon:Clear()
    local DisableSprite = item.transform:Find("DisableSprite").gameObject
    DisableSprite:SetActive(false)
    local LevelLabel = item.transform:Find("LevelLabel").gameObject
    LevelLabel:SetActive(false)
    local PassiveIcon = item.transform:Find("PassiveIcon").gameObject
    PassiveIcon:SetActive(false)
    local ColorSystemIcon = item.transform:Find("ColorSystemIcon").gameObject
    ColorSystemIcon:SetActive(false)

    self:SetItemSelected(item, false)

    if not info then return end

    local skill = Skill_AllSkills.GetData(info.skillId)
    
    if not skill then return end

    -- load skill icon
    SkillIcon:LoadSkillIcon(skill.SkillIcon)

    CommonDefs.AddOnClickListener(item, DelegateFactory.Action_GameObject(function (go)
        self:OnItemClicked(go, info)
    end), false)

    CommonDefs.AddOnDragStartListener(item, DelegateFactory.Action_GameObject(function (go)
        self:OnItemDragStart(go, info)
    end), false)

end

function LuaPUBGSkillSetWnd:OnItemClicked(item, info)
    if not item then return end
    if not info then return end

    if not self.SkillItemList then return end

    for i = 1, #self.SkillItemList do
        self:SetItemSelected(self.SkillItemList[i], item == self.SkillItemList[i])
    end
    CSkillInfoMgr.ShowSkillInfoWnd(info.skillId, true, 0, 0, nil)
end

function LuaPUBGSkillSetWnd:OnItemDragStart(item, info)

    if not info then return end

    self.IsDragging = true
    self.CloneSkill:SetActive(true)

    self.DraggedSkillId = info.skillId

    local cloneSkillIcon = self.CloneSkill.transform:Find("SkillIcon"):GetComponent(typeof(CUITexture))
    local skill = Skill_AllSkills.GetData(info.skillId)
    cloneSkillIcon:LoadSkillIcon(skill.SkillIcon)

    if Application.platform == RuntimePlatform.IPhonePlayer or Application.platform == RuntimePlatform.Android then
		self.FingerIndex = Input.GetTouch(0).fingerId
		local pos = Input.GetTouch(0).position
		self.LastPos = Vector3(pos.x, pos.y, 0)
    end
end

function LuaPUBGSkillSetWnd:SetItemSelected(item, isSelcted)
    if not item then return end
    local SelectedSprite = item.transform:Find("SelectedSprite").gameObject
    SelectedSprite:SetActive(isSelcted)
end

function LuaPUBGSkillSetWnd:Update()
    if not self.IsDragging then return end

    if Application.platform == RuntimePlatform.IPhonePlayer or Application.platform == RuntimePlatform.Android then
        do
            local i = 0
            while i < Input.touchCount do
                local touch = Input.GetTouch(i)
                if touch.fingerId == self.FingerIndex then
                    if touch.phase ~= TouchPhase.Ended and touch.phase ~= TouchPhase.Canceled then
                        if touch.phase ~= TouchPhase.Stationary then
                            local tempPos = Vector3(touch.position.x, touch.position.y, 0)
                        	local pos = CUIManager.UIMainCamera:ScreenToWorldPoint(tempPos)
                            self.CloneSkill.transform.position = pos
                            self.LastPos = pos
                        end
                        return
                    else
                        break
                    end
                end
                i = i + 1
            end
        end
    else
        if Input.GetMouseButton(0) then
        	local pos = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
            self.CloneSkill.transform.position = pos
            self.LastPos = Input.mousePosition
            if not Input.GetMouseButtonUp(0) then
                return
            end
        end
    end

    self.IsDragging = false
    self.FingerIndex = -1

    local hoveredObject = CUICommonDef.SelectedUIWithRacast
    self:OnHoverSkill(hoveredObject)
end


function LuaPUBGSkillSetWnd:OnHoverSkill(go)
    if not go then return end
    if not self.SkillButtons then return end

    self.CloneSkill:SetActive(false)
    for i = 1, #self.SkillButtons do
        if go == self.SkillButtons[i] then
            Gac2Gas.RequestSetActiveTempSkill(i+2, self.DraggedSkillId)
        end
    end
end
-- 初始化已选择的技能
function LuaPUBGSkillSetWnd:InitSelectedSkills()
    if not CClientMainPlayer.Inst then return end
    for i = 1, self.SELECTEDSKILL do
        local item = self:GetSkillButtonByIndex(i)
        if item then
            self:InitSelectedSkillItem(item, CClientMainPlayer.Inst.SkillProp.ActiveTempSkill[i+2])
        end
    end
end


function LuaPUBGSkillSetWnd:InitSelectedSkillItem(item, skillId)
    local Icon = item.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture))
    Icon:Clear()
    
    if skillId == 0 then return end

    local skill = Skill_AllSkills.GetData(skillId)

    if not skill then return end

    -- load skill icon
    Icon:LoadSkillIcon(skill.SkillIcon)
end

function LuaPUBGSkillSetWnd:GetSkillButtonByIndex(index)
    if index == 1 then
        return self.Button1st
    elseif index == 2 then
        return self.Button2nd
    elseif index == 3 then
        return self.Button3rd
    elseif index == 4 then
        return self.Button4th
    elseif index == 5 then
        return self.Button5th
    end
    return nil
end


function LuaPUBGSkillSetWnd:OnEnable()
    g_ScriptEvent:AddListener("SetActiveTempSkillSuccess", self, "InitSelectedSkills")
end

function LuaPUBGSkillSetWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SetActiveTempSkillSuccess", self, "InitSelectedSkills")
end


return LuaPUBGSkillSetWnd