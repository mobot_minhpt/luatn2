local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local Profession = import "L10.Game.Profession"
local UISprite = import "UISprite"
local NGUIText = import "NGUIText"
local Constants = import "Constants"
local CCharacterModelTextureLoader = import "L10.UI.CCharacterModelTextureLoader"
local Double = import "System.Double"

LuaAppearanceTeamLeaderDisplaySubview = class()
RegistChildComponent(LuaAppearanceTeamLeaderDisplaySubview,"m_ItemNameLabel", "ItemNameLabel", UILabel)
RegistChildComponent(LuaAppearanceTeamLeaderDisplaySubview,"m_ModelTexture", "ModelTexture", CCharacterModelTextureLoader)
RegistChildComponent(LuaAppearanceTeamLeaderDisplaySubview,"m_LeaderIcon", "LeaderIcon", UISprite)

function LuaAppearanceTeamLeaderDisplaySubview:Init()
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer then
        self.m_ModelTexture:Init(mainplayer.Id, EnumToInt(mainplayer.Class), EnumToInt(mainplayer.Gender))
    end
end

function LuaAppearanceTeamLeaderDisplaySubview:SetItemName(name)
    self.m_ItemNameLabel.text = name
end

function LuaAppearanceTeamLeaderDisplaySubview:SetLeaderIcon(iconName)
    if iconName == "" or iconName == nil then
        iconName = LuaAppearancePreviewMgr:GetDefaultTeamLeaderIconName()
    end
    self.m_LeaderIcon.spriteName = iconName
end