local CRenderScene = import 'L10.Engine.Scene.CRenderScene'
local HousePopupMenuItemData=import "L10.UI.HousePopupMenuItemData"
local CHousePopupMgr = import "L10.UI.CHousePopupMgr"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CScene = import "L10.Game.CScene"
local BoxCollider = import "UnityEngine.BoxCollider"
local CTrackMgr = import "L10.Game.CTrackMgr"
local MouseInputHandler=import "L10.Engine.MouseInputHandler"
local Time = import "UnityEngine.Time"
local Input = import "UnityEngine.Input"
local Utility = import "L10.Engine.Utility"
local CPos = import "L10.Engine.CPos"

LuaSceneInteractiveMgr = {}

LuaSceneInteractiveMgr.NowChoiceId = nil
LuaSceneInteractiveMgr.NowID = nil
LuaSceneInteractiveMgr.m_SceneItemId2Colliders = {}--切换场景但SceneRoot不变时用以删除碰撞体
LuaSceneInteractiveMgr.m_ChoiceID = nil --当前触发的按钮
LuaSceneInteractiveMgr.m_SceneItemId2ChoiceID = {}--存储长按时的按钮状态

CRenderScene.m_hookSetClickSceneItem = function(this, name)
    this.NowChooseSceneItemName = name
    if (Input.GetMouseButtonDown(0)) then
        MouseInputHandler.Inst.lastPressStartTime = Time.realtimeSinceStartup
    end
    if (MouseInputHandler.Inst.lastPressStartTime ~= -1) and ((Time.realtimeSinceStartup - MouseInputHandler.Inst.lastPressStartTime) > MouseInputHandler.longPressDelta) then
        local key = string.gsub(name, "(%w+):", "")
        local data = ScenesItem_ScenesItem.GetData(tonumber(key))
        if data and LuaSceneInteractiveMgr.m_SceneItemId2Colliders[data.ID] then
            LuaSceneInteractiveMgr:OnClick(this, data, LuaSceneInteractiveMgr.m_SceneItemId2Colliders[data.ID].transform)
        end
        MouseInputHandler.Inst.lastPressStartTime = -1
    end
end

CRenderScene.m_hookInitScenesItem = function(this)
    ScenesItem_ScenesItem.Foreach(function (sceneItemId, data)
        for i = 0,data.PublicMapID.Length - 1 do
            local publicMapID = data.PublicMapID[i]
            if publicMapID == CScene.MainScene.SceneTemplateId then
                if data.ScenesItemPos and data.ScenesItemPos.Length == 3 then
                    local collider = GameObject()
                    collider.transform.parent = CRenderScene.Inst.transform
                    local bc = collider:AddComponent(typeof(BoxCollider))
                    collider.transform.position = Vector3(data.ScenesItemPos[0], data.ScenesItemPos[1], data.ScenesItemPos[2])
                    bc.size = (data.ScenesItemExtent and data.ScenesItemExtent.Length == 3) and Vector3(data.ScenesItemExtent[0], data.ScenesItemExtent[1], data.ScenesItemExtent[2]) or Vector3.zero

                    collider.name = SafeStringFormat3("%s:%d",CRenderScene.SceneInteractiveItemPreName,sceneItemId)
                    this.SceneInteractiveItemExist = true
                    if LuaSceneInteractiveMgr.m_SceneItemId2Colliders[sceneItemId] then
                        GameObject.Destroy(LuaSceneInteractiveMgr.m_SceneItemId2Colliders[sceneItemId])
                        LuaSceneInteractiveMgr.m_SceneItemId2Colliders[sceneItemId] = nil
                    end
                    LuaSceneInteractiveMgr.m_SceneItemId2Colliders[sceneItemId] = collider
                end
                LuaSceneInteractiveMgr:DoInteractiveAction(data, data.InitialSceneInteractiveAction)
            end
        end
    end)
end

function LuaSceneInteractiveMgr:OnClick(scene, data, target)
    if (not data.ChoiceID) or (data.ChoiceID.Length == 0) then return end
    if not self.m_SceneItemId2ChoiceID[data.ID] then
        self.m_SceneItemId2ChoiceID[data.ID] = data.ChoiceID
    end
    local opTable = self.m_SceneItemId2ChoiceID[data.ID]
    if opTable then
        local actionTable = {}
        for i = 0, opTable.Length - 1 do
            local index = opTable[i]
            if index and index > 0 then
                local choice = ScenesItem_ItemChoice.GetData(index)
                if choice then
                    local action = index
                    local icon = choice.ChoiceIcon
                    table.insert(actionTable,{action = action,icon = icon, ID = data.ID})
                end
            end
        end

        local itemActionPairs = LuaSceneInteractiveMgr.GetPopupMenuItemData(actionTable)

        if itemActionPairs and #itemActionPairs > 0 then
          CHousePopupMgr.ShowHousePopupMenu(Table2Array(itemActionPairs,MakeArrayClass(HousePopupMenuItemData)), target)
        end
    end
    scene:CancelClickSceneItem()
end

function LuaSceneInteractiveMgr.GetPopupMenuItemData(dataTable)
  local itemActionPairs = {}-- CreateFromClass(MakeGenericClass(List, HousePopupMenuItemData))

  for i,v in ipairs(dataTable) do
    table.insert(itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
      Gac2Gas.RequestInteractWithSceneItem(v.ID, v.action)
    end), v.icon, true, true, -1))
  end

  return itemActionPairs
end

function LuaSceneInteractiveMgr:SyncSceneItemStatus(sceneItemId, choiceID)
    local data = ScenesItem_ScenesItem.GetData(sceneItemId)
    self.m_ChoiceID = choiceID
    self:DoInteractiveAction(data, ScenesItem_ItemChoice.GetData(choiceID).SceneInteractiveAction)
end

function LuaSceneInteractiveMgr:DoInteractiveAction(scenesItemData, action)
    if action == nil  then return end
    for functionName, parameters in string.gmatch(action, "(%w+)%(([^%s]-)%)") do
        local func = self[functionName]
        if func then
            if nil == parameters then
                func(self,scenesItemData)
            else
                func(self,scenesItemData,g_LuaUtil:StrSplit(parameters,","))
            end
        end
    end
end

function LuaSceneInteractiveMgr:DestroyColliders()
    for key,collider in pairs(LuaSceneInteractiveMgr.m_SceneItemId2Colliders) do
        if collider then
            GameObject.Destroy(collider)
        end
    end
    LuaSceneInteractiveMgr.m_SceneItemId2Colliders = {}
end

function LuaSceneInteractiveMgr:LeaveScene()
    self.m_SceneItemId2ChoiceID = {}
    self:DestroyColliders()
end

----------------------------------------------------------------------------------------
---ClickAction
function LuaSceneInteractiveMgr:PlaySceneObjectAnimation(scenesItemData,params)
    local path, aniName = params[1],params[2]
    CLuaSceneMgr:PlaySceneObjectAnimation(path, aniName)
end

function LuaSceneInteractiveMgr:HideSceneObject(scenesItemData,params)
    if not CScene.MainScene then return end
    local objectPath = params[1]
    CLuaSceneMgr:ShowOrHideSceneObject(CScene.MainScene.SceneTemplateId, objectPath, false)
end

function LuaSceneInteractiveMgr:ShowSceneObject(scenesItemData,params)
    if not CScene.MainScene then return end
    local objectPath = params[1]
    CLuaSceneMgr:ShowOrHideSceneObject(CScene.MainScene.SceneTemplateId, objectPath, true)
end

function LuaSceneInteractiveMgr:ChangeInteractiveButton(scenesItemData,params)
    local change2choiceId = tonumber(params[1])
    local data = scenesItemData
    if not data then return end
    local opTable = self.m_SceneItemId2ChoiceID[data.ID]
    if opTable then
        for i = 0, opTable.Length - 1 do
            local choiceId = opTable[i]
            if choiceId == self.m_ChoiceID then
                self.m_SceneItemId2ChoiceID[data.ID][i] = change2choiceId
                break
            end
        end
    end
end

function LuaSceneInteractiveMgr:HideSelfPath(scenesItemData)
    local data = scenesItemData
    if data then
        CLuaSceneMgr:ShowOrHideSceneObject(CScene.MainScene.SceneTemplateId, data.SelfPath, false)
    end
end

function LuaSceneInteractiveMgr:ShowSelfPath(scenesItemData)
    local data = scenesItemData
    if data then
        CLuaSceneMgr:ShowOrHideSceneObject(CScene.MainScene.SceneTemplateId, data.SelfPath, true)
    end
end

function LuaSceneInteractiveMgr:RandomShowOneSceneObject(scenesItemData,params)
    if not CRenderScene.Inst then return end
    local len = #params
    local randomindex = math.random(1, len)
    for i = 1,len do
        local path = params[i]
        local t = CRenderScene.Inst.transform:Find(path)
        if t then
            if t.gameObject.activeSelf then
                if i == randomindex then
                    randomindex = math.fmod(randomindex, len ) + 1
                end
            end
            t.gameObject:SetActive(false)
        end
    end
    local showObject = CRenderScene.Inst.transform:Find(params[randomindex])
    if showObject then
        showObject.gameObject:SetActive(true)
    end
end

function LuaSceneInteractiveMgr:FeedTheFish(scenesItemData,params)
    local len = #params
    if len < 2 or not CClientMainPlayer.Inst then return end
    local aimPosX,aimPosY = tonumber(params[1]),tonumber(params[2])
    if CPos.Distance_Grid(Utility.PixelPos2GridPos(CClientMainPlayer.Inst.Pos), CPos(aimPosX,aimPosY)) < 1 then
        Gac2Gas.RequestEnterJiFuFeedFish()
    else
        CTrackMgr.Inst:Track(Utility.GridPos2PixelPos(CPos(aimPosX,aimPosY)), 0, DelegateFactory.Action(function ()
            Gac2Gas.RequestEnterJiFuFeedFish()
        end), DelegateFactory.Action(function ()
            g_MessageMgr:ShowMessage("FeedTheFish_InterruptA")
        end), true)
    end
end

function LuaSceneInteractiveMgr:ShowMessage(scenesItemData,params)

end
---------------------------------------rpc
-- 打开交互界面
function Gas2Gac.OpenSceneItemInteractWnd(Id, choiceId, wndName)
    if wndName == 'ThrowStoneWnd' then
        LuaSceneInteractiveMgr.NowChoiceId = choiceId
        LuaSceneInteractiveMgr.NowID = Id
        CUIManager.ShowUI(CLuaUIResources.SceneJewelSubmitWnd)
    end
end

-- 播放特效
function Gas2Gac.PlayItemInteractFx(Id, fxId)
    local data = ScenesItem_ScenesItem.GetData(Id)
    if data then
        CEffectMgr.Inst:AddWorldPositionFX(fxId, Vector3(data.ScenesItemPos[0],data.ScenesItemPos[1],data.ScenesItemPos[2]), 0, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
    end
end

-- 更新可交互物体的状态
function Gas2Gac.SyncSceneItemStatus(sceneItemId, status)
    LuaSceneInteractiveMgr:SyncSceneItemStatus(sceneItemId, status)
end