local UITexture = import "UITexture"

local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local UISlider = import "UISlider"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CUIFx = import "L10.UI.CUIFx"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CItemMgr = import "L10.Game.CItemMgr"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CPos = import "L10.Engine.CPos"
local Color = import "UnityEngine.Color"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local TweenPosition = import "TweenPosition"

LuaFireWorkPartyZhongChouWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFireWorkPartyZhongChouWnd, "PartyTableView", "PartyTableView", QnTableView)
RegistChildComponent(LuaFireWorkPartyZhongChouWnd, "ZhongChouProcess", "ZhongChouProcess", UISlider)
RegistChildComponent(LuaFireWorkPartyZhongChouWnd, "LingXingFireCount", "LingXingFireCount", UILabel)
RegistChildComponent(LuaFireWorkPartyZhongChouWnd, "SubmitBtn", "SubmitBtn", GameObject)
RegistChildComponent(LuaFireWorkPartyZhongChouWnd, "RewardLabel", "RewardLabel", UILabel)
RegistChildComponent(LuaFireWorkPartyZhongChouWnd, "OwnFireLabel", "OwnFireLabel", UILabel)
RegistChildComponent(LuaFireWorkPartyZhongChouWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaFireWorkPartyZhongChouWnd, "PurchaseLabel", "PurchaseLabel", UILabel)
RegistChildComponent(LuaFireWorkPartyZhongChouWnd, "BigFireFxNode", "BigFireFxNode", CUIFx)
RegistChildComponent(LuaFireWorkPartyZhongChouWnd, "SmallFireFxNode", "SmallFireFxNode", CUIFx)
RegistChildComponent(LuaFireWorkPartyZhongChouWnd, "PartyItemBg", "PartyItemBg", UITexture)
RegistChildComponent(LuaFireWorkPartyZhongChouWnd,"PurchaseClip","PurchaseClip",UIPanel)
--@endregion RegistChildComponent end
RegistClassMember(LuaFireWorkPartyZhongChouWnd,"m_NormalItemSize")
RegistClassMember(LuaFireWorkPartyZhongChouWnd,"m_PurchaseNoticeTick")
RegistClassMember(LuaFireWorkPartyZhongChouWnd,"m_NoticeList")
RegistClassMember(LuaFireWorkPartyZhongChouWnd,"m_TotalNeedCount")
RegistClassMember(LuaFireWorkPartyZhongChouWnd,"m_SubmitFx")
RegistClassMember(LuaFireWorkPartyZhongChouWnd,"m_PurchaseTween")
function LuaFireWorkPartyZhongChouWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.SubmitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSubmitBtnClick()
	end)


    --@endregion EventBind end
	self.m_NormalItemSize = self.PartyItemBg.width

	UIEventListener.Get(self.TipBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    g_MessageMgr:ShowMessage("FireworkParty_ZhongChou_Rule")
	end)
	self.m_SubmitFx = self.SubmitBtn.transform:Find("SubmitFx"):GetComponent(typeof(CUIFx))
	self.m_PurchaseTween = self.PurchaseLabel.transform:GetComponent(typeof(TweenPosition))
	local pos = self.PurchaseLabel.transform.localPosition
	local startX = pos.x
	self.PurchaseLabel.text = g_MessageMgr:FormatMessage("FireWorkParty_Scrolling_Msg")--FireWorkParty_Scrolling_Msg
	local labelWidth = self.PurchaseLabel.width
	local clipWidth = self.PurchaseClip.width
	local endX = startX - labelWidth - clipWidth
	
	self.m_PurchaseTween.enabled = false
	self.m_PurchaseTween.from = Vector3(startX,pos.y,pos.z)
	self.m_PurchaseTween.to = Vector3(endX,pos.y,pos.z)
	self.m_PurchaseTween.enabled = true
end

function LuaFireWorkPartyZhongChouWnd:OnEnable()
    g_ScriptEvent:AddListener("SubmitItemsToFireworkPartySuccess",self,"OnSubmitItemsToFireworkPartySuccess")
    g_ScriptEvent:AddListener("UpdateFirworkPartyStatus",self,"OnUpdateFirworkPartyStatus")
	g_ScriptEvent:AddListener("NotifyPlayerBuyLiuGuangYiCaiFirework",self,"OnNotifyPlayerBuyLiuGuangYiCaiFirework")
end

function LuaFireWorkPartyZhongChouWnd:OnDisable()
	if self.m_PurchaseNoticeTick then
		UnRegisterTick(self.m_PurchaseNoticeTick)
		self.m_PurchaseNoticeTick = nil
	end

    g_ScriptEvent:RemoveListener("SubmitItemsToFireworkPartySuccess",self,"OnSubmitItemsToFireworkPartySuccess")
    g_ScriptEvent:RemoveListener("UpdateFirworkPartyStatus",self,"OnUpdateFirworkPartyStatus")
	g_ScriptEvent:RemoveListener("NotifyPlayerBuyLiuGuangYiCaiFirework",self,"OnNotifyPlayerBuyLiuGuangYiCaiFirework")
end

function LuaFireWorkPartyZhongChouWnd:Init()
	if not LuaFireWorkPartyMgr.PartySchedule then
		LuaFireWorkPartyMgr.PartySchedule = {}
		LuaFireWorkPartyMgr.TotalNeedCount = 0
		local gap = 0
		YuanXiao_FireworkPartySchedule.Foreach(function(key,data)
			local t = {}
			t.ID = data.ID 
			t.BeginTime = data.BeginTime 
			t.LastTime = data.LastTime
			t.MapId = data.MapId
			t.NeedSubmitNum = data.NeedSubmitNum
			LuaFireWorkPartyMgr.TotalNeedCount = t.NeedSubmitNum > LuaFireWorkPartyMgr.TotalNeedCount and t.NeedSubmitNum or LuaFireWorkPartyMgr.TotalNeedCount
			table.insert(LuaFireWorkPartyMgr.PartySchedule,t)
			if gap == 0 then
				gap = t.NeedSubmitNum
			end
		end)
		
		LuaFireWorkPartyMgr.TotalNeedCount = LuaFireWorkPartyMgr.TotalNeedCount + gap
		--print("LuaFireWorkPartyMgr.TotalNeedCount",LuaFireWorkPartyMgr.TotalNeedCount)
	end

	local sortFunc = function(a,b)
		return a.NeedSubmitNum > b.NeedSubmitNum
	end

	table.sort(LuaFireWorkPartyMgr.PartySchedule,sortFunc)

	self.PartyTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #LuaFireWorkPartyMgr.PartySchedule
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

	self.PartyTableView:ReloadData(false,false)

	--self.PurchaseLabel.gameObject:SetActive(false)

	self:InitButtom()
	self.BigFireFxNode:LoadFx("fx/ui/prefab/UI_yhdh2022_fenwei001.prefab")
end

function LuaFireWorkPartyZhongChouWnd:InitButtom()
	self.RewardLabel.text = SafeStringFormat3(LocalString.GetString("再提交%d个零星焰火可获得一个长情告白礼包"),LuaFireWorkPartyMgr.ToSubmitNum)
	self.LingXingFireCount.text = LuaFireWorkPartyMgr.TotalSubmittedNum

	local ownCount = 0
	local itemId = YuanXiao_FireworkPartySetting.GetData().FireworkSubmitItemID
	local bindCount, notBindCount
    bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)
	ownCount = bindCount + notBindCount
	self.OwnFireLabel.text = SafeStringFormat3(LocalString.GetString("当前持有%d个，可通过日常活动获得"),ownCount)

	self.ZhongChouProcess.value = LuaFireWorkPartyMgr.TotalNeedCount ~= 0 and LuaFireWorkPartyMgr.TotalSubmittedNum / LuaFireWorkPartyMgr.TotalNeedCount or 0
end

function LuaFireWorkPartyZhongChouWnd:InitItem(item,row)
	local anchor =  item.transform:Find("Anchor")
	local sceneIcon = item.transform:Find("SceneIcon"):GetComponent(typeof(UISprite))
	local sceneName = anchor:Find("SceneName"):GetComponent(typeof(UILabel))
	local startTime = anchor:Find("StartTime"):GetComponent(typeof(UILabel))
	local gift = anchor:Find("Gift").gameObject
	local giftIcon = anchor:Find("Gift/GiftIcon"):GetComponent(typeof(CUITexture))
	local awardTag = anchor:Find("Gift/AwardTag").gameObject

	local needCountLabel = anchor:Find("NeedCountLabel"):GetComponent(typeof(UILabel))
	local goWatchingBtn = anchor:Find("GoWatchingBtn").gameObject
	local bgTexture = item.transform:Find("PartyItemBg"):GetComponent(typeof(UITexture))
	local decoration = item.transform:Find("Decoration")
	local checkPoint= item.transform:Find("PartyItemBg/CheckPoint")
	local fxNodex = anchor:Find("FxNode"):GetComponent(typeof(CUIFx))
	fxNodex:DestroyFx()
	local data = LuaFireWorkPartyMgr.PartySchedule[row+1]
	if not data then
		item.transform.gameObject:SetActive(false)
		return
	end

	--info
	local sceneresData = PublicMap_MiniMapRes.GetData(data.MapId)
	if sceneresData then
		sceneIcon.spriteName = sceneresData.ResName.."_building"
	end
	sceneName.text = PublicMap_PublicMap.GetData(data.MapId).Name
	needCountLabel.text = data.NeedSubmitNum
	local beginTime = data.BeginTime
	startTime.text = beginTime
	--status
	local status = EnumFireworkPartyStatus.eLocked
	if LuaFireWorkPartyMgr.PartyStatus and LuaFireWorkPartyMgr.PartyStatus[data.ID] then
		status = LuaFireWorkPartyMgr.PartyStatus[data.ID].status
	end

	local adjustWidth = 0
	local posz = 0

	if status == EnumFireworkPartyStatus.eLocked then
		posz = -1
		bgTexture.color = NGUIText.ParseColor("7B7B7C", 0)
		decoration.gameObject:SetActive(true)
		sceneIcon.color = Color.black
		sceneName.text = LocalString.GetString("未解锁")
		startTime.color = NGUIText.ParseColor("b3b2b2", 0)
		needCountLabel.color = NGUIText.ParseColor("b3b2b2", 0)
		sceneName.color = NGUIText.ParseColor("ffffff", 0)
	elseif status == EnumFireworkPartyStatus.eWaiting then
		decoration.gameObject:SetActive(true)
		sceneIcon.color = NGUIText.ParseColor("afafaf", 0)
		startTime.color = NGUIText.ParseColor("FFF7BD", 0)
		needCountLabel.color = NGUIText.ParseColor("FFF7BD", 0)
		sceneName.color = NGUIText.ParseColor("FFF7BD", 0)
	elseif status == EnumFireworkPartyStatus.eStart then
		adjustWidth = 147
		decoration.gameObject:SetActive(false)
		sceneIcon.color = Color.white
		startTime.color = NGUIText.ParseColor("FFF7BD", 0)
		needCountLabel.color = NGUIText.ParseColor("FFF7BD", 0)
		sceneName.color = NGUIText.ParseColor("FFF7BD", 0)
		fxNodex:LoadFx("fx/ui/prefab/UI_yhdh2022_anniu_wfg.prefab")
	else--status == EnumFireworkPartyStatus.eEnd
		decoration.gameObject:SetActive(true)
		sceneIcon.color = NGUIText.ParseColor("afafaf", 0)
		bgTexture.color = NGUIText.ParseColor24("88454a", 0)
		needCountLabel.color = NGUIText.ParseColor("e3a263", 0)
		startTime.color = NGUIText.ParseColor("e3a263", 0)
		sceneName.color = NGUIText.ParseColor("e3a263", 0)
		startTime.text = LocalString.GetString("已结束")
	end
	goWatchingBtn:SetActive(status == EnumFireworkPartyStatus.eStart)
	needCountLabel.gameObject:SetActive(status ~= EnumFireworkPartyStatus.eStart)
	awardTag:SetActive(status == EnumFireworkPartyStatus.eEnd)
	bgTexture.width = self.m_NormalItemSize + adjustWidth
	local pos = checkPoint.localPosition
	pos.z = posz
	checkPoint.transform.localPosition = pos

	UIEventListener.Get(goWatchingBtn).onClick = DelegateFactory.VoidDelegate(function (go)
		local sdata = YuanXiao_FireworkPartySchedule.GetData(data.ID)
		local bestPos = sdata.BestViewPos
		CTrackMgr.Inst:Track(nil, data.MapId, CreateFromClass(CPos, bestPos[0] * 64, bestPos[1] * 64), 0, nil, nil, nil, nil, true)
	end)
	--gift 
	local schedule = YuanXiao_FireworkPartySchedule.GetData(data.ID)
	local mailId = schedule.RewardMailId
	local giftItemId = self:GetFirstMailItemId(mailId)
	local itemdata = Item_Item.GetData(giftItemId)
	if itemdata then
		giftIcon:LoadMaterial(itemdata.Icon)
		UIEventListener.Get(gift).onClick = DelegateFactory.VoidDelegate(function (go)
			CItemInfoMgr.ShowLinkItemTemplateInfo(giftItemId,false,nil,AlignType.Default, 0, 0, 0, 0)
		end)
	end

end

function LuaFireWorkPartyZhongChouWnd:GetFirstMailItemId(mailId)
	if mailId then      
		local awardId,count,_ = string.match(Mail_Mail.GetData(mailId).Items, "(%d+),(%d+),(%d+)") 
		return tonumber(awardId)
	end
end
--@region UIEvent

function LuaFireWorkPartyZhongChouWnd:OnSubmitBtnClick()
	Gac2Gas.RequestSubmitItemsToFireworkParty()
end

--@endregion UIEvent

--@region callback
-- 提交零星焰火成功
function LuaFireWorkPartyZhongChouWnd:OnSubmitItemsToFireworkPartySuccess(submitNum, totalSubmittedNum, toSubmitNum)
	self.SmallFireFxNode:DestroyFx()
	self.SmallFireFxNode:LoadFx("fx/ui/prefab/UI_yhdh2022_dalihua001.prefab")
	LuaFireWorkPartyMgr.TotalSubmittedNum = totalSubmittedNum
    LuaFireWorkPartyMgr.ToSubmitNum = toSubmitNum
	self:InitButtom()

	-- self.m_SubmitFx:DestroyFx()
	-- self.m_SubmitFx:LoadFx("fx/ui/prefab/UI_yhdh2022_jindutiao001.prefab")
end

--更新某场大会状态，会全服广播
function LuaFireWorkPartyZhongChouWnd:OnUpdateFirworkPartyStatus(id, status, totalSubmittedNum)
	LuaFireWorkPartyMgr.TotalSubmittedNum = totalSubmittedNum
	if not LuaFireWorkPartyMgr.PartyStatus then
		LuaFireWorkPartyMgr.PartyStatus = {}
	end
	if not LuaFireWorkPartyMgr.PartyStatus[id] then
		LuaFireWorkPartyMgr.PartyStatus[id] = {}
	end
	LuaFireWorkPartyMgr.PartyStatus[id].status = status
	LuaFireWorkPartyMgr.PartyStatus[id].id = id
	self:Init()
end

--通知有土豪买线下烟花了
function LuaFireWorkPartyZhongChouWnd:OnNotifyPlayerBuyLiuGuangYiCaiFirework(buyerId, buyerName, serverId, serverName, submitNum, totalSubmittedNum)
	local notice = {}
	notice.buyerId = buyerId
	notice.buyerName = buyerName
	notice.serverId = serverId
	notice.serverName = serverName
	notice.submitNum = submitNum
	notice.totalSubmittedNum = totalSubmittedNum
	table.insert(LuaFireWorkPartyMgr.NoticeList,notice)
end
--@endregion
