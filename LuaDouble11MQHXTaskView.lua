require("common/common_include")

local CButton = import "L10.UI.CButton"
local CGamePlayMgr=import "L10.Game.CGamePlayMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaDouble11MQHXTaskView = class()

RegistChildComponent(LuaDouble11MQHXTaskView, "TaskName", UILabel)
RegistChildComponent(LuaDouble11MQHXTaskView, "TaskDesc", UILabel)
RegistChildComponent(LuaDouble11MQHXTaskView, "RuleBtn", CButton)
RegistChildComponent(LuaDouble11MQHXTaskView, "LeaveBtn", CButton)


function LuaDouble11MQHXTaskView:Init()
    self.TaskName.text = LocalString.GetString("夺宝大乱斗")
    --self.TaskDesc.text = g_MessageMgr:FormatMessage("Double11_MengQuanHengXing_BriefTips")
    if LuaShuangshiyi2023Mgr.MQLDPlayInfo then
        self:OnSyncMengQuanHengXingCountDown(LuaShuangshiyi2023Mgr.MQLDPlayInfo.gameFinishTime)
    else
        self:OnSyncMengQuanHengXingCountDown(1800)
    end
    
    CommonDefs.AddOnClickListener(self.LeaveBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        CGamePlayMgr.Inst:LeavePlay()
    end), false)

    CommonDefs.AddOnClickListener(self.RuleBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        g_MessageMgr:ShowMessage("Double11_MengQuanHengXing_Tips")
    end), false)
    
end

function LuaDouble11MQHXTaskView:OnEnable()
    g_ScriptEvent:AddListener("SyncMengQuanHengXingCountDown", self, "OnSyncMengQuanHengXingCountDown")
    
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
    self.m_Tick = RegisterTick(function()
        self:OnTick()
    end, 1000)
end

function LuaDouble11MQHXTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("SyncMengQuanHengXingCountDown", self, "OnSyncMengQuanHengXingCountDown")

    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end

function LuaDouble11MQHXTaskView:OnSyncMengQuanHengXingCountDown(countDownTime)
    self.gameFinishTime = countDownTime
    self:OnTick()
end

function LuaDouble11MQHXTaskView:OnTick()
    local countDownTime = self.gameFinishTime - CServerTimeMgr.Inst.timeStamp
    local timeStr = self:GetRemainTimeText(countDownTime)
    self.TaskDesc.text = g_MessageMgr:FormatMessage("MENGQUANDALUANDOU_TASKWND_INTRODUCTION") .. "\n\n" .. timeStr
end

function LuaDouble11MQHXTaskView:GetRemainTimeText(totalSeconds)
    if totalSeconds<0 then
        totalSeconds = 0
    end
    if totalSeconds>=3600 then
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}:{2:00}[-]"),
                math.floor(totalSeconds / 3600),
                math.floor((totalSeconds % 3600) / 60),
                totalSeconds % 60)
    else
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}[-]"), math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

return LuaDouble11MQHXTaskView