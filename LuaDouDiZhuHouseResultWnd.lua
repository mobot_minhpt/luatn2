CLuaDouDiZhuHouseResultWnd=class()
RegistClassMember(CLuaDouDiZhuHouseResultWnd,"m_LeftTime")
RegistClassMember(CLuaDouDiZhuHouseResultWnd,"m_OkLabel")
RegistClassMember(CLuaDouDiZhuHouseResultWnd,"m_OkButton")
RegistClassMember(CLuaDouDiZhuHouseResultWnd,"m_Continue")
RegistClassMember(CLuaDouDiZhuHouseResultWnd,"m_Tick")

function CLuaDouDiZhuHouseResultWnd:Init()
    self.m_Continue = false
    local UILabelType=typeof(UILabel)
    FindChild(self.transform,"CancleLabel"):GetComponent(UILabelType).text=LocalString.GetString("离开")
    self.m_OkLabel = FindChild(self.transform,"OkLabel"):GetComponent(UILabelType)
    self.m_OkLabel.text=LocalString.GetString("继续")

    FindChild(self.transform,"HeadLabel1"):GetComponent(UILabelType).text=LocalString.GetString("玩家名称")
    FindChild(self.transform,"HeadLabel2"):GetComponent(UILabelType).text=LocalString.GetString("底分")
    FindChild(self.transform,"HeadLabel3"):GetComponent(UILabelType).text=LocalString.GetString("倍率")
    FindChild(self.transform,"HeadLabel4"):GetComponent(UILabelType).text=LocalString.GetString("积分")
    FindChild(self.transform,"TitleLabel"):GetComponent(UILabelType).text=LocalString.GetString("斗地主积分结算")


    self.m_OkButton=FindChild(self.transform,"OkButton").gameObject
    UIEventListener.Get(self.m_OkButton).onClick=LuaUtils.VoidDelegate(function(go)
        --继续
        CUIManager.CloseUI(CLuaUIResources.DouDiZhuHouseResultWnd)
        self.m_Continue = true
    end)
    local canclebutton=FindChild(self.transform,"CancleButton").gameObject
    UIEventListener.Get(canclebutton).onClick=LuaUtils.VoidDelegate(function(go)
        --离开
        self.m_Continue = false
        Gac2Gas.RequestLeaveDouDiZhu()
        CUIManager.CloseUI(CLuaUIResources.DouDiZhuHouseResultWnd)
    end)

    --初始化数据
    local myId=CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local templateItem=FindChild(self.transform,"Item").gameObject
    templateItem:SetActive(false)    
    local grid=FindChild(self.transform,"Grid").gameObject
    CUICommonDef.ClearTransform(grid.transform)
    
    if CLuaDouDiZhuMgr.m_Result then
        for i,v in ipairs(CLuaDouDiZhuMgr.m_Result) do
            local item = NGUITools.AddChild(grid, templateItem)
            item:SetActive(true)
            local tf=item.transform
            local nameLabel=FindChild(tf,"NameLabel"):GetComponent(UILabelType)
            nameLabel.text=v.name
            local label1=FindChild(tf,"Label1"):GetComponent(UILabelType)
            label1.text=v.duZhuScore
            local label2=FindChild(tf,"Label2"):GetComponent(UILabelType)
            label2.text=v.duZhuPower
            local label3=FindChild(tf,"Label3"):GetComponent(UILabelType)
            label3.text=v.score

            if myId==v.id then
                LuaUtils.SetUIWidgetColor(nameLabel,0,255,0,255)
                LuaUtils.SetUIWidgetColor(label1,0,255,0,255)
                LuaUtils.SetUIWidgetColor(label2,0,255,0,255)
                LuaUtils.SetUIWidgetColor(label3,0,255,0,255)
            end
        end
    end
    grid:GetComponent(typeof(UIGrid)):Reposition()


    if CLuaDouDiZhuMgr.m_IsRemote then
        --倒计时
        self.m_LeftTime=CLuaDouDiZhuMgr.m_AutoContinueLeftTime
        self.m_OkLabel.text =LocalString.GetString("继续").."(5)"
        self.m_Tick = RegisterTickWithDuration(function ()
            if not self:OnTick() then 
                UnRegisterTick(self.m_Tick)
                self.m_Tick=nil
                self.m_Continue = true
                CUIManager.CloseUI(CLuaUIResources.DouDiZhuHouseResultWnd)
            end
        end,1000,6*1000)
    end
end
function CLuaDouDiZhuHouseResultWnd:OnTick()
	if self.m_OkLabel == nil then return false end

    self.m_LeftTime = self.m_LeftTime - 1
    if self.m_LeftTime>=0 then
        self.m_OkLabel.text =LocalString.GetString("继续")..SafeStringFormat3("(%d)",self.m_LeftTime)
        return true
    end
    self.m_OkLabel.text=LocalString.GetString("继续")
    return false
end
function CLuaDouDiZhuHouseResultWnd:OnDestroy()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick=nil
    end
    CLuaDouDiZhuMgr.CancleAutoContinueCountdown()
    if CLuaDouDiZhuMgr.m_IsRemote and self.m_Continue then
        --自动准备下一场
        Gac2Gas.RequestDouDiZhuReady(true)
    end
end