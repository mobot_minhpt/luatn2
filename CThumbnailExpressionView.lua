-- Auto Generated!!
local AlignType = import "L10.UI.CTooltip+AlignType"
local Boolean = import "System.Boolean"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CExpressionActionMgr = import "L10.Game.CExpressionActionMgr"
local CExpressionAppearanceMgr = import "L10.Game.CExpressionAppearanceMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CPos = import "L10.Engine.CPos"
local CStatusMgr = import "L10.Game.CStatusMgr"
local CThumbnailChatWnd = import "L10.UI.CThumbnailChatWnd"
local CThumbnailExpressionItem = import "L10.UI.CThumbnailExpressionItem"
local CThumbnailExpressionView = import "L10.UI.CThumbnailExpressionView"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local ENUM_EXPRESSION_VIEWTYPE = import "L10.UI.CThumbnailExpressionView+ENUM_EXPRESSION_VIEWTYPE"
local EnumEventType = import "EnumEventType"
local EPropStatus = import "L10.Game.EPropStatus"
local EventManager = import "EventManager"
local EnumEvent = import "L10.Game.EnumEvent"
local Object = import "System.Object"
local Single = import "System.Single"
local UIEventListener = import "UIEventListener"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local Time = import "UnityEngine.Time"
local CAS_Expression = import "L10.Game.CAS_Expression"
local UIScrollView = import "UIScrollView"
local CActionStateMgr = import "L10.Game.CActionStateMgr"
local EnumActionState = import "L10.Game.EnumActionState"
local CScene=import "L10.Game.CScene"
local EnumGender = import "L10.Game.EnumGender"
local MessageMgr = import "L10.Game.MessageMgr"

CThumbnailExpressionView.m_Awake_CS2LuaHook = function (this)
    this:Init()
    if this.ViewType == ENUM_EXPRESSION_VIEWTYPE.SELFIE then
        this.slider.gameObject:SetActive(false)
        EventManager.AddListenerInternal(EnumEventType.PlayerClickGround, MakeDelegateFromCSFunction(this.OnPlayerClickGround, MakeGenericClass(Action1, CPos), this))
        EventManager.AddListenerInternal(EnumEventType.JoyStickDragging, MakeDelegateFromCSFunction(this.OnJoystickDraging, MakeGenericClass(Action2, Vector3, Boolean), this))
        EventManager.AddListenerInternal(EnumEventType.PlayerDragJoyStick, MakeDelegateFromCSFunction(this.PlayerDragJoyStick, MakeGenericClass(Action1, Vector3), this))
    end
    EventManager.AddListenerInternal(EnumEventType.UpdateYingLingState, MakeDelegateFromCSFunction(this.UpdateYingLingState, MakeGenericClass(Action1, UInt32), this))
end

CThumbnailExpressionView.m_OnDestroy_CS2LuaHook = function (this)
    CommonDefs.ListClear(this.mDataSource)
    CommonDefs.ListClear(CExpressionActionMgr.ExpressionActionInNeed)
    if this.ViewType == ENUM_EXPRESSION_VIEWTYPE.SELFIE then
        EventManager.RemoveListenerInternal(EnumEventType.PlayerClickGround, MakeDelegateFromCSFunction(this.OnPlayerClickGround, MakeGenericClass(Action1, CPos), this))
        EventManager.RemoveListenerInternal(EnumEventType.JoyStickDragging, MakeDelegateFromCSFunction(this.OnJoystickDraging, MakeGenericClass(Action2, Vector3, Boolean), this))
        EventManager.RemoveListenerInternal(EnumEventType.PlayerDragJoyStick, MakeDelegateFromCSFunction(this.PlayerDragJoyStick, MakeGenericClass(Action1, Vector3), this))
    end
    EventManager.RemoveListenerInternal(EnumEventType.UpdateYingLingState, MakeDelegateFromCSFunction(this.UpdateYingLingState, MakeGenericClass(Action1, UInt32), this))
end

CThumbnailExpressionView.m_hookOnEnable = function (this)
    this:PauseOrPlay(true)
    g_ScriptEvent:AddListener("OnGetFuxiPlayerOwnData",CThumbnailExpressionViewHolder,"OnGetFuxiPlayerOwnData")
    g_ScriptEvent:AddListener("OnFuxiUnlock",CThumbnailExpressionViewHolder,"OnFuxiUnlock")
    g_ScriptEvent:AddListener("OnSyncMainPlayerAppearanceProp", CThumbnailExpressionViewHolder, "OnSyncAppearance")
    g_ScriptEvent:AddListener("OnSendFashionTransformState",CThumbnailExpressionViewHolder,"OnSendFashionTransformState")
end

CThumbnailExpressionView.m_hookOnDisable = function (this)
    this.table.OnSelectAtRow = nil
    g_ScriptEvent:RemoveListener("OnGetFuxiPlayerOwnData",CThumbnailExpressionViewHolder,"OnGetFuxiPlayerOwnData")
    g_ScriptEvent:RemoveListener("OnFuxiUnlock",CThumbnailExpressionViewHolder,"OnFuxiUnlock")
    g_ScriptEvent:RemoveListener("OnSyncMainPlayerAppearanceProp", CThumbnailExpressionViewHolder, "OnSyncAppearance")
    g_ScriptEvent:RemoveListener("OnSendFashionTransformState",CThumbnailExpressionViewHolder,"OnSendFashionTransformState")
end

CThumbnailExpressionView.m_Init_CS2LuaHook = function (this)
    CThumbnailExpressionViewHolder.ReloadDefaultExpression(this)
    CThumbnailExpressionViewHolder.FuxiInit(this)
    CThumbnailExpressionViewHolder.InitBabyExpression(this)
    CThumbnailExpressionViewHolder.InitOtherPlayerExpression(this)
end

CThumbnailExpressionView.m_PauseOrPlay_CS2LuaHook = function (this, pause)
    if this.ViewType ~= ENUM_EXPRESSION_VIEWTYPE.SELFIE then
        return
    end
    this.Paused = pause
    if this.Paused then
        this.pauseSprite.spriteName = Constants.Common_Play_Icon
    else
        this.pauseSprite.spriteName = Constants.Common_Pause_Icon
    end
end

CThumbnailExpressionView.m_ItemAt_CS2LuaHook = function (this, view, row)
    if row < this.dataSource.Count then
        row = this.sortStrategy:GetMappedIndex(row, this.dataSource.Count)

        local item = TypeAs(this.dataSource[row]:IsFashionSpecialExpression() and view:GetFromPool(2) or view:GetFromPool(0), typeof(CThumbnailExpressionItem))
        local expression = this.dataSource[row]
        local isNeed = this:isExpressionNeeded(expression)
        item:Init(expression, isNeed)
        item.OnItemLongPressed = DelegateFactory.Action(function ()
            if CThumbnailExpressionView.ChangeUmbrellaSwitch == 1 then
                if expression:GetGroup() ~= 0 then
                    LuaAppearancePreviewMgr:ShowUmbrellas(expression:GetGroup())
                end
            end
        end)
        return item
    end
    return nil
end

CThumbnailExpressionView.m_OnItemClick_CS2LuaHook = function (this, row)

    if row >= 0 and row < this.dataSource.Count then
        row = this.sortStrategy:GetMappedIndex(row, this.dataSource.Count)

        CExpressionActionMgr.ExpressionActionID = this.dataSource[row]:GetID()

        if TypeIs(this.table:GetItemAtRow(row), typeof(CThumbnailExpressionItem)) then
            if not this.dataSource[row]:GetIsLock() then
                CExpressionActionMgr.Inst:UpdateExpressionAlert(this.dataSource[row]:GetID())
                --PlayerSettings.SetExpressionPlayed(dataSource[row].GetID());
                local info = this.dataSource[row]
                this.table:GetItemAtRow(row):RefreshAlert()
            end
        end
        if this.dataSource[row]:GetIsLock() then
            CItemAccessListMgr.Inst:ShowItemAccessInfo(this.dataSource[row]:GetID(), true, this.transform, AlignType.Right)
        else
            local expressionResult = true
            if this.ViewType == ENUM_EXPRESSION_VIEWTYPE.NORMAL then
                expressionResult = CExpressionActionMgr.Inst:DoExpressionAction(true, false, 0, false)
            elseif this.ViewType == ENUM_EXPRESSION_VIEWTYPE.SELFIE then
                expressionResult = this:StartOfflineExpression()
            end
            if not expressionResult then
                this.table:SetSelectRow(- 1, true)
            end
            if this.ViewType == ENUM_EXPRESSION_VIEWTYPE.SELFIE then
                this.slider.gameObject:SetActive(expressionResult)
            end

            if CExpressionActionMgr.HasExpressionsInNeed() and CThumbnailChatWnd.Instance ~= nil then
                CThumbnailChatWnd.Instance:CloseThumbnailExpressions()
            end
        end
    end
end

CThumbnailExpressionView.m_StartOfflineExpression_CS2LuaHook = function (this)
    CClientMainPlayer.Inst:LeavePrevOfflineExpressionAction()
    if not this:canDoThisActionOffline(CExpressionActionMgr.ExpressionActionID) then
        return false
    end
    --[[
            if (CClientMainPlayer.Inst.ActionData.ExpressionId == 47000084)
            {
                CClientMainPlayer.Inst.RequestShowExpressionAction(47000091);
            }
            else if (CClientMainPlayer.Inst.ActionData.ExpressionId == 47000085)
            {
                CClientMainPlayer.Inst.RequestShowExpressionAction(47000089);
            }
            else
            {
                CActionStateMgr.Inst.ShowActionState(CClientMainPlayer.Inst, EnumActionState.Idle);
            }
            ]]
    CClientMainPlayer.Inst:RequestShowExpressionAction(0)
    --   CActionStateMgr.Inst.ShowActionState(CClientMainPlayer.Inst, EnumActionState.Idle);

    this.aniTime = CClientMainPlayer.Inst:GetExpressionAnimationTime(CExpressionActionMgr.ExpressionActionID)
    if this.aniTime <= 0 then
      if CExpressionActionMgr.ExpressionActionID == CAS_Expression.s_LiangXiangExpressionId then
        g_MessageMgr:ShowMessage("Expression_LiangXiang_No_Animation")
      end
      return false
    end
    this:PauseOrPlay(false)
    this.deltaTime = 0

    return CExpressionActionMgr.Inst:DoExpressionAction(true, true, 0)
end

CThumbnailExpressionView.m_clearExpression_CS2LuaHook = function (this)
    CExpressionActionMgr.ExpressionActionID = 0
    this.slider.m_Value = 0
    this.table:SetSelectRow(- 1, true)
    this:PauseOrPlay(false)
end

CThumbnailExpressionView.m_getActionInfo_CS2LuaHook = function (this, expressionId)
    local result = nil
    local show = Expression_Show.GetData(expressionId)
    if show ~= nil then
        local default
        default, result = CExpressionActionMgr.Inst.ThumbnailInfos:TryGetValue(show.DisplayOrder)
    end

    return result
end

CThumbnailExpressionView.m_isExpressionNeeded_CS2LuaHook = function (this, info)
    if CExpressionActionMgr.HasExpressionsInNeed() then
        do
            local i = 0
            while i < CExpressionActionMgr.ExpressionActionInNeed.Count do
                if info:ContainsExpression(CExpressionActionMgr.ExpressionActionInNeed[i]) then
                    return true
                end
                i = i + 1
            end
        end
    end
    return false
end

CThumbnailExpressionView.m_canDoThisActionOffline_CS2LuaHook = function (this, id)
    local expression = Expression_Define.GetData(id)
    local expression = Expression_Define.GetData(id)
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.m_IsInHangZhouWater then
        if expression.HangZhouWaterCanBeUsed then
            return true
        else
            g_MessageMgr:ShowMessage("HANGZHOUWATER_EXPRESSION_CANNOTBEUSE")
            return false
        end
    end
    if expression.ExpressionWhenRide == 0 and CZuoQiMgr.IsOnZuoqi() then
        g_MessageMgr:ShowMessage("EXPRESSION_NOT_SHOW_ON_RIDE")
        return false
    end
    if expression.ExpressionWhenBoat == 0 then
        if (CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("TianGongGeShip")) == 1) or (CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("QingQiuTour")) == 1) then
            g_MessageMgr:ShowMessage("EXPRESSION_NOT_SHOW_ON_RIDE")
            return false
        end
    end
    return true
end

CThumbnailExpressionView.m_GetOpenUmbrellaExpression_CS2LuaHook = function (this)
    do
        local i = 0
        while i < this:NumberOfRows(this.table) do
            local item = TypeAs(this.table:GetItemAtRow(i), typeof(CThumbnailExpressionItem))
            if item ~= nil and item:IsOpenUmbrella() then
                this.table:ScrollToRow(i)
                return item.gameObject
            end
            i = i + 1
        end
    end
    return nil
end

CThumbnailExpressionView.m_hookUpdate = function (this)
    if (not this.Paused) and this.ViewType == ENUM_EXPRESSION_VIEWTYPE.SELFIE and (CExpressionActionMgr.ExpressionActionID > 0 or CThumbnailExpressionViewHolder.CurrentExpressionId > 0 or CThumbnailExpressionViewHolder.CurrentOtherPlayerExpressionId > 0) then
        this.deltaTime = this.deltaTime + Time.deltaTime;
        if (this.deltaTime > this.aniTime) then
            this.deltaTime = this.aniTime;
            this:PauseOrPlay(true);
        end
        this.slider.m_Value = this.deltaTime / this.aniTime;
        this.slider:SetPercentage();
    end
end

CThumbnailExpressionView.m_hookonSliderValueChange = function(this, value)
    this.deltaTime = value * this.aniTime;
    if CThumbnailExpressionViewHolder.CurrentTabIndex == 0 then
        CExpressionActionMgr.Inst:DoExpressionAction(true, true, value)
    elseif CThumbnailExpressionViewHolder.CurrentTabIndex == 1 then
        if CThumbnailExpressionViewHolder.CurrentBabyRenderObject then
            CActionStateMgr.Inst:ShowActionState(CThumbnailExpressionViewHolder.CurrentBabyClientObject, EnumActionState.Idle, {})
            CExpressionActionMgr.HandleWeaponAndFxOnLeave(CThumbnailExpressionViewHolder.CurrentBabyClientObject, CThumbnailExpressionViewHolder.CurrentExpressionDesignData)
            CExpressionActionMgr.HandleWeaponAndFxOnEnter(CThumbnailExpressionViewHolder.CurrentBabyClientObject, CThumbnailExpressionViewHolder.CurrentExpressionDesignData)
            CThumbnailExpressionViewHolder.CurrentBabyRenderObject:Preview(CThumbnailExpressionViewHolder.CurrentExpressionAniName, this.deltaTime)
        end
    elseif CThumbnailExpressionViewHolder.CurrentTabIndex == 2 then
        CThumbnailExpressionViewHolder.ShowOtherPlayerExpressionOffline(this)
    end
end

CThumbnailExpressionView.m_hookOnJoystickDraging = function (this, ...)
  CThumbnailExpressionViewHolder.MainPlayerMove(this)
end

CThumbnailExpressionView.m_hookOnPlayerClickGround = function (this, ...)
  CThumbnailExpressionViewHolder.MainPlayerMove(this)
end

CThumbnailExpressionView.m_hookPlayerDragJoyStick = function (this, ...)
  CThumbnailExpressionViewHolder.MainPlayerMove(this)
end

--@region 附加功能

local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
CThumbnailExpressionViewHolder = class()

function CThumbnailExpressionViewHolder.ReloadDefaultExpression(this)
    CommonDefs.ListClear(this.mDataSource)
    this.table.OnSelectAtRow = MakeDelegateFromCSFunction(this.OnItemClick, MakeGenericClass(Action1, Int32), this)
    this.table.m_DataSource = this
    if CExpressionActionMgr.HasExpressionsInNeed() then
        this.table:ReloadData(true, false)
    else
        this.table:ReloadData(true, true)
    end
    if this.ViewType == ENUM_EXPRESSION_VIEWTYPE.SELFIE then
        --         slider.gameObject.SetActive(true);
        this.slider.OnValueChanged = MakeDelegateFromCSFunction(this.onSliderValueChange, MakeGenericClass(Action1, Single), this)
        UIEventListener.Get(this.pauseSprite.cachedTransform.parent.gameObject).onClick = MakeDelegateFromCSFunction(this.onPauseClick, VoidDelegate, this)
    end
    
end

function CThumbnailExpressionViewHolder:OnSendFashionTransformState()
    local wnd = CThumbnailExpressionViewHolder.Wnd
    CExpressionActionMgr.Inst:InitData()
    CommonDefs.ListClear(wnd.mDataSource)
    wnd.table:ReloadData(true, true)
end

--@region 自定义动作（伏羲动作）

local QnTabView = import "L10.UI.QnTabView"
local LuaGameObject = import "LuaGameObject"

CThumbnailExpressionViewHolder.FuxiTabIndex = 0
CThumbnailExpressionViewHolder.Wnd = nil

function CThumbnailExpressionViewHolder.FuxiInit(this)
    CThumbnailExpressionViewHolder.Wnd = this
    local tabview = this.transform:Find("btns"):GetComponent(typeof(QnTabView))

    if not LuaFuxiAniMgr.IsOpen then
        tabview.gameObject:SetActive(false)
        return
    end 

    tabview.OnSelect = DelegateFactory.Action_QnTabButton_int(function (btn, index) 
        local islock = LuaFuxiAniMgr.IsFuxiLock()
        if islock then
            if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level >= 30 then
                CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.FuxiDanceGuide1)
            end
        end
        if CThumbnailExpressionViewHolder.FuxiTabIndex == index then return end
        if index == 0 then
            if not islock then--锁住的情况下不需要重新加载，非锁住状态才需要更新
                CThumbnailExpressionViewHolder.ReloadDefaultExpression(this)
            end
            CThumbnailExpressionViewHolder.FuxiTabIndex = 0
        else
            if islock then
                tabview:ChangeTo(0)
                LuaFuxiAniMgr.ShowItemConsumeWnd()
            else
                CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.FuxiDanceGuide2)
                CThumbnailExpressionViewHolder.FuxiTabIndex = 1
                LuaFuxiAniMgr.InitFuxiData()
            end
        end
    end)
    tabview:ChangeTo(0)
    CThumbnailExpressionViewHolder.FuxiRefreshLock(this)
end

function CThumbnailExpressionViewHolder.FuxiRefreshLock(this)
    local sp = this.transform:Find("btns/TabBtn2/Sprite").gameObject
    local islock = LuaFuxiAniMgr.IsFuxiLock()
    sp:SetActive(islock)
    if not islock then
        local labeltrans = this.transform:Find("btns/TabBtn2/Label")
        local pos = labeltrans.localPosition
        pos.x = 0
        labeltrans.localPosition = pos
    end
end

function CThumbnailExpressionViewHolder:OnFuxiUnlock()
    local wnd = CThumbnailExpressionViewHolder.Wnd
    CThumbnailExpressionViewHolder.FuxiRefreshLock(wnd)
    local tabview = wnd.transform:Find("btns"):GetComponent(typeof(QnTabView))
    tabview:ChangeTo(1)
end

--[[
    @desc: 收到伏羲数据事件
    author:{author}
    time:2021-04-21 11:12:15
    --@data: 
    @return:
]]
function CThumbnailExpressionViewHolder:OnGetFuxiPlayerOwnData()
    local datas ={}
    for i=1,#LuaFuxiAniMgr.FuxiData do
        if LuaFuxiAniMgr.FuxiData[i].motionId ~= 0 then --过滤未上传的动作
            table.insert(datas,LuaFuxiAniMgr.FuxiData[i])
        end
    end
     
    if CThumbnailExpressionViewHolder.FuxiTabIndex ~= 1 then return end
    local wnd = CThumbnailExpressionViewHolder.Wnd
    wnd.table.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        if row == 0 then
            CUIManager.ShowUI(CLuaUIResources.FuxiAniPreviewWnd)
        else
            if CClientMainPlayer.Inst and CClientMainPlayer.Inst.m_IsInHangZhouWater then
                g_MessageMgr:ShowMessage("HANGZHOUWATER_EXPRESSION_CANNOTBEUSE")
                return
            end
            if CClientMainPlayer.Inst.AppearanceGender == EnumGender.Monster or CClientMainPlayer.Inst.AppearanceProp.ResourceId > 0 then
                g_MessageMgr:ShowMessage("EXPRESSION_NO_ANI")
                return
            end
            if datas[row].outtime or datas[row].status == EnumFuxiDanceMotionStatus.eDeleted then
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("该动作已过期！"))
            else
                Gac2Gas.RequestPlayFuXiDance(datas[row].motionId)
            end
        end
    end)
    wnd.table.m_DataSource = DefaultTableViewDataSource.Create2(
        function() 
            return 1+#datas
        end,
        function(view,row)
            local item = view:GetFromPool(1)
            if row == 0 then
                CThumbnailExpressionViewHolder.FuxiInitItem(item,nil,row) 
            else
                CThumbnailExpressionViewHolder.FuxiInitItem(item,datas[row],row) 
            end
            return item
        end)
    wnd.table:ReloadData(true, true)
end

function CThumbnailExpressionViewHolder.FuxiInitItem(tableitem,data,row)
    local trans = tableitem.transform
    local icon = LuaGameObject.GetChildNoGC(trans,"Icon").cTexture
    local addgo = LuaGameObject.GetChildNoGC(trans,"AddImage").gameObject
    local namelb = LuaGameObject.GetChildNoGC(trans,"Name").label
    local bignamelb = LuaGameObject.GetChildNoGC(trans,"LbName").label

    if data == nil then
        --添加一个加号
        addgo:SetActive(true)
        icon.gameObject:SetActive(false)
        bignamelb.gameObject:SetActive(false)
        namelb.text = ""
    else
        if data.outtime or data.status == EnumFuxiDanceMotionStatus.eDeleted then
            icon.gameObject:SetActive(true)
            local path= GameSetting_Client.GetData().ExpireItemIconPath
            icon:LoadMaterial(path)
        else
            icon.gameObject:SetActive(false)
            bignamelb.gameObject:SetActive(true)
            if data.motionName == nil then 
                bignamelb.text = ""
            else  
                bignamelb.text = CommonDefs.StringAt(data.motionName,0)
            end
        end

        addgo:SetActive(false)
        namelb.text = data.motionName
    end
end

--@endregion

--@region 自拍相关

-- Baby and other player expression For ScreenCapture
CThumbnailExpressionViewHolder.CurrentTabIndex = 0
CThumbnailExpressionViewHolder.CurrentExpressionId = 0
CThumbnailExpressionViewHolder.CurrentExpressionDesignData = nil
CThumbnailExpressionViewHolder.CurrentSelectedObj = nil
CThumbnailExpressionViewHolder.CurrentBabyRenderObject = nil
CThumbnailExpressionViewHolder.CurrentExpressionAniName = ""
CThumbnailExpressionViewHolder.CurrentBabyClientObject = nil

function CThumbnailExpressionViewHolder.MainPlayerMove(this)
  this.gameObject:SetActive(false)
  CClientMainPlayer.Inst:LeaveOfflineExpressionAction()
  if CThumbnailExpressionViewHolder.CurrentBabyClientObject then
    CExpressionActionMgr.HandleWeaponAndFxOnLeave(CThumbnailExpressionViewHolder.CurrentBabyClientObject, CThumbnailExpressionViewHolder.CurrentExpressionDesignData);
  end
end

function CThumbnailExpressionViewHolder.InitBabyExpression(this)
  if this.ViewType ~= ENUM_EXPRESSION_VIEWTYPE.SELFIE then return end--非拍照模式下，直接跳过
  local radioBoxRootObj = this.transform:Find("QnRadioBox").gameObject
  local playerGridObj = this.transform:Find("Bg/ScrollView/Grid").gameObject
  local babyGridObj = this.transform:Find("Bg/ScrollView/BabyGrid").gameObject
  local otherPlayerGridObj = this.transform:Find("Bg/ScrollView/OtherPlayerGrid").gameObject

  local uiScrollview = this.transform:Find("Bg/ScrollView"):GetComponent(typeof(UIScrollView))

  local QnRadioBox = import "L10.UI.QnRadioBox"
  radioBoxRootObj:GetComponent(typeof(QnRadioBox)).OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
    playerGridObj:SetActive(index == 0)
    babyGridObj:SetActive(index == 1)
    otherPlayerGridObj:SetActive(index == 2)
    if index ~= 0 then
      uiScrollview:SetDragAmount(0, 0, false)
    end
    CThumbnailExpressionViewHolder.CurrentTabIndex = index
  end)


  local haveBaby = CLuaScreenCaptureMgr.CurrentBabyEngineId and CLuaScreenCaptureMgr.CurrentBabyEngineId > 0
  radioBoxRootObj.transform:Find("Baby").gameObject:SetActive(haveBaby)


  CThumbnailExpressionViewHolder.CurrentTabIndex = 0
  CThumbnailExpressionViewHolder.CurrentExpressionId = 0
  CThumbnailExpressionViewHolder.CurrentSelectedObj = nil
  CThumbnailExpressionViewHolder.CurrentExpressionAniName = ""
  CThumbnailExpressionViewHolder.CurrentBabyClientObject = nil
  CThumbnailExpressionViewHolder.CurrentBabyRenderObject = nil

  if not haveBaby then return end

  local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
  local babyObject = CClientObjectMgr.Inst:GetObject(CLuaScreenCaptureMgr.CurrentBabyEngineId)
  CThumbnailExpressionViewHolder.CurrentBabyClientObject = babyObject
  CThumbnailExpressionViewHolder.CurrentBabyRenderObject = babyObject and babyObject.RO or nil

  local BabyMgr = import "L10.Game.BabyMgr"
  local CUITexture = import "L10.UI.CUITexture"
  local UILabel = import "UILabel"
  local UIGrid = import "UIGrid"
  local ret, babyInfo = CommonDefs.DictTryGet_LuaCall(BabyMgr.Inst.BabyDictionary, CLuaScreenCaptureMgr.CurrentBabyId)
  if not ret then return end
  local expressionIdList = BabyMgr.Inst:GetValidBabySingleExpression(babyInfo)

  local babyItemObj = this.transform:Find("Bg/Pool/BabyItem").gameObject
  babyItemObj:SetActive(false)

  for i = 0, expressionIdList.Count - 1 do
    local id = expressionIdList[i]
    local obj = NGUITools.AddChild(babyGridObj, babyItemObj)
    obj:SetActive(true)
    local data = Expression_BabyShow.GetData(id)
  	obj.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(data.Icon)
  	local label = obj.transform:Find("Name"):GetComponent(typeof(UILabel))
  	label.text = data.ExpName
    obj.transform:Find("Icon/Selected").gameObject:SetActive(false)
    UIEventListener.Get(obj).onClick = LuaUtils.VoidDelegate(function(go)
      CThumbnailExpressionViewHolder.OnBabyItemClick(this, go, id)
    end)
  end
  babyGridObj:GetComponent(typeof(UIGrid)):Reposition()
end

function CThumbnailExpressionViewHolder.SetSelect(go, bSelected)
  if not go then return end
  go.transform:Find("Icon/Selected").gameObject:SetActive(bSelected)
end

function CThumbnailExpressionViewHolder.OnBabyItemClick(this, go, id)
  CThumbnailExpressionViewHolder.SetSelect(CThumbnailExpressionViewHolder.CurrentSelectedObj, false)
  CThumbnailExpressionViewHolder.SetSelect(go, true)
  CThumbnailExpressionViewHolder.CurrentExpressionId = id
  CThumbnailExpressionViewHolder.CurrentSelectedObj = go
  CThumbnailExpressionViewHolder.CurrentExpressionDesignData = Expression_Define.GetData(id)

  if not CThumbnailExpressionViewHolder.CurrentBabyRenderObject then return end
  local data = Expression_Define.GetData(id)
  if data then
    CThumbnailExpressionViewHolder.CurrentExpressionAniName = data.AniName
    this.aniTime = CThumbnailExpressionViewHolder.CurrentBabyRenderObject:GetAniLength(CThumbnailExpressionViewHolder.CurrentExpressionAniName)
    this:PauseOrPlay(false)
    this.deltaTime = 0
    this.slider.gameObject:SetActive(true)
  end
end


-- Other Player Expression
CThumbnailExpressionViewHolder.CurrentOtherPlayerSelectedObj = nil
CThumbnailExpressionViewHolder.CurrentOtherPlayerExpressionId = 0
CThumbnailExpressionViewHolder.CurrentOtherPlayerPreExpressionId = 0

function CThumbnailExpressionViewHolder.InitOtherPlayerExpression(this)
  if this.ViewType ~= ENUM_EXPRESSION_VIEWTYPE.SELFIE then return end

  CThumbnailExpressionViewHolder.CurrentOtherPlayerSelectedObj = nil
  CThumbnailExpressionViewHolder.CurrentOtherPlayerExpressionId = 0
  CThumbnailExpressionViewHolder.CurrentOtherPlayerPreExpressionId = 0

  local otherPlayerGridObj = this.transform:Find("Bg/ScrollView/OtherPlayerGrid").gameObject

  local CUITexture = import "L10.UI.CUITexture"
  local UILabel = import "UILabel"
  local UIGrid = import "UIGrid"

  local babyItemObj = this.transform:Find("Bg/Pool/BabyItem").gameObject
  babyItemObj:SetActive(false)

  -- TODO: add other player expression
  local expressionIdTable = {}
  for i = 0, this.dataSource.Count - 1 do
      table.insert(expressionIdTable, this.dataSource[i]:GetID())
  end

  for i = 1, #expressionIdTable do
    local id = expressionIdTable[i]
    local obj = NGUITools.AddChild(otherPlayerGridObj, babyItemObj)
    obj:SetActive(true)
    local data = Expression_Show.GetData(id)
  	obj.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(data.Icon)
  	local label = obj.transform:Find("Name"):GetComponent(typeof(UILabel))
  	label.text = data.ExpName
    obj.transform:Find("Icon/Selected").gameObject:SetActive(false)
    UIEventListener.Get(obj).onClick = LuaUtils.VoidDelegate(function(go)
      CThumbnailExpressionViewHolder.OnOtherPlayerItemClick(this, go, id)
    end)
  end
  otherPlayerGridObj:GetComponent(typeof(UIGrid)):Reposition()
end

function CThumbnailExpressionViewHolder.OnOtherPlayerItemClick(this, go, id)
    if (not CClientMainPlayer.Inst.Target) or (not TypeAs(CClientMainPlayer.Inst.Target, typeof(CClientOtherPlayer))) then
        g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("请点击选中一位其它玩家，再进行操作"))
        return
    end

    local showData = Expression_Show.GetData(id)
    -- Check and Replace from (Class, Gender)
    if showData and showData.RaceCheck and showData.RaceCheck.Count > 0 then
        local classId = EnumToInt(CClientMainPlayer.Inst.Target.Class) * 100 + EnumToInt(CClientMainPlayer.Inst.Target.Gender)
        if not showData.RaceCheck:Contains(classId) then
            Expression_Show.Foreach(function(k, v)
                if v.Order == showData.Order and v.RaceCheck and v.RaceCheck:Contains(classId) then
                    id = k
                end
            end)
        end
    end

    CThumbnailExpressionViewHolder.SetSelect(CThumbnailExpressionViewHolder.CurrentOtherPlayerSelectedObj, false)
    CThumbnailExpressionViewHolder.SetSelect(go, true)
    CThumbnailExpressionViewHolder.CurrentOtherPlayerPreExpressionId = CThumbnailExpressionViewHolder.CurrentOtherPlayerExpressionId
    CThumbnailExpressionViewHolder.CurrentOtherPlayerExpressionId = id
    CThumbnailExpressionViewHolder.CurrentOtherPlayerSelectedObj = go

    local data = Expression_Define.GetData(id)
    if data then
      this.aniTime = CClientMainPlayer.Inst:GetExpressionAnimationTime(id)
      this:PauseOrPlay(false)
      this.deltaTime = 0
      this.slider.gameObject:SetActive(true)
    end
end

function CThumbnailExpressionViewHolder.ShowOtherPlayerExpressionOffline(this)
    if (not CClientMainPlayer.Inst.Target) or (not TypeAs(CClientMainPlayer.Inst.Target, typeof(CClientOtherPlayer))) then return end
    local ro = CClientMainPlayer.Inst.Target.RO
    if not ro then return end

    local data = Expression_Define.GetData(CThumbnailExpressionViewHolder.CurrentOtherPlayerExpressionId)
    if not data then return end
    local aniName = data.AniName
    local time = this.deltaTime
    local curAniLen = ro:GetAniLength(aniName)
    while time > curAniLen do
        time = time - curAniLen
        local nextData = Expression_Define.GetData(data.NextExpressionID)
        if not nextData then return end
        aniName = nextData.AniName
        curAniLen = ro:GetAniLength(aniName)
    end
    local preData = Expression_Define.GetData(CThumbnailExpressionViewHolder.CurrentOtherPlayerPreExpressionId)
    if preData then
        CExpressionActionMgr.HandleWeaponAndFxOnLeave(CClientMainPlayer.Inst.Target, preData)
    end
    CActionStateMgr.Inst:ShowActionState(CClientMainPlayer.Inst.Target, EnumActionState.Idle, {})
    CExpressionActionMgr.HandleWeaponAndFxOnEnter(CClientMainPlayer.Inst.Target, data)
    ro:Preview(aniName, time)
end

function CThumbnailExpressionViewHolder:OnSyncAppearance()
    local wnd = CThumbnailExpressionViewHolder.Wnd
    CommonDefs.ListClear(wnd.mDataSource)
    wnd.table:ReloadData(true, true)
end

local Random = import "UnityEngine.Random"
function CThumbnailExpressionViewHolder:CheckRoleTouZiExpression(ExpressionID,needShow)
    local list = Expression_Setting.GetData().RollDiceExpressionId
    for i = 0,list.Length - 1 do
        if list[i] == ExpressionID then
            if needShow then
                local rd = Random.Range(0,list.Length)
                local resId = list[rd]
                Gac2Gas.RequestExpressionAction(resId)
            end
            return true
        end
    end
    return false
end
CClientMainPlayer.m_hookRequestShowExpressionAction = function (this, ExpressionID)
    local result = CStatusMgr.Inst:CheckConflict(this, EnumEvent.GetId("ShowExpression"))
    if result then
        if result.msgs and ExpressionID > 0 then
            local datas = {}
            for i = 1, result.msgs.Length - 1 do
                table.insert(datas, result.msgs[i])
            end
            MessageMgr.Inst:ShowMessage(result.msgs[0],datas)
        end
        return
    end
    if this:CanRequestShowExpressionAction(ExpressionID) then
        local bEnableShowAction = this.CurrentActionState == EnumActionState.Idle or this.CurrentActionState == EnumActionState.Expression
        if not bEnableShowAction then return end
        if CScene.MainScene and 51101929 == CScene.MainScene.GamePlayDesignId and 
            (47000084 == CExpressionActionMgr.ExpressionActionID or 47000091 == CExpressionActionMgr.ExpressionActionID) then
                return
        end
        if CThumbnailExpressionViewHolder:CheckRoleTouZiExpression(ExpressionID,true) then return end
        Gac2Gas.RequestExpressionAction(ExpressionID)
    end
end

--@endregion

--@endregion