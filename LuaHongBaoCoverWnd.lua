local GameObject = import "UnityEngine.GameObject"
local UITable = import "UITable"
local UIGrid = import "UIGrid"
local QnButton = import "L10.UI.QnButton"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CClientMainPlayer= import "L10.Game.CClientMainPlayer"
LuaHongBaoCoverWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHongBaoCoverWnd, "HongBaoCoverItem", "HongBaoCoverItem", GameObject)
RegistChildComponent(LuaHongBaoCoverWnd, "Table", "Table", UITable)
RegistChildComponent(LuaHongBaoCoverWnd, "PageGrid", "PageGrid", UIGrid)
RegistChildComponent(LuaHongBaoCoverWnd, "TipBtn", "TipBtn", QnButton)
RegistChildComponent(LuaHongBaoCoverWnd, "PageLabel", "PageLabel", UILabel)
RegistChildComponent(LuaHongBaoCoverWnd, "LeftBtn", "LeftBtn", GameObject)
RegistChildComponent(LuaHongBaoCoverWnd, "RightBtn", "RightBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaHongBaoCoverWnd, "m_MaxItemInPage")
RegistClassMember(LuaHongBaoCoverWnd, "m_PageNum")
RegistClassMember(LuaHongBaoCoverWnd, "m_CurPageNum")
RegistClassMember(LuaHongBaoCoverWnd, "m_HongBaoList")
RegistClassMember(LuaHongBaoCoverWnd, "m_HongBaoItemList")
RegistClassMember(LuaHongBaoCoverWnd, "m_IsChangePage")
RegistClassMember(LuaHongBaoCoverWnd, "m_AnimTick")

function LuaHongBaoCoverWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)


	
	UIEventListener.Get(self.LeftBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeftBtnClick()
	end)


	
	UIEventListener.Get(self.RightBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightBtnClick()
	end)


    --@endregion EventBind end
    self.m_MaxItemInPage = 5   -- 1页显示5个红包
end

function LuaHongBaoCoverWnd:Init()
    self:InitHongBaoList()
    local HongBaoCoverTotalNum = #self.m_HongBaoList
    self.HongBaoCoverItem:SetActive(false)
    
    self.m_PageNum = math.ceil(HongBaoCoverTotalNum / self.m_MaxItemInPage)
    self.m_CurPageNum = 1
    self.m_IsChangePage = false
    self.Table.gameObject:SetActive(false)

    for i = 0,self.m_PageNum - 1 do
        self:InitOnePage(i)
    end
    self:InitChangePageBtn()
end

function LuaHongBaoCoverWnd:InitHongBaoList()
    self.m_HongBaoList = {}
    self.m_HongBaoItemList = {}
    HongBao_Cover.Foreach(function(k,v)
        local nowTime = CServerTimeMgr.Inst.timeStamp
        local startTime = CServerTimeMgr.Inst:GetTimeStampByStr(v.StartTime)
        if nowTime > startTime then
            local data = {
                id = k,
                name = v.Name,
                resourceName = v.MainResource,
                canUse = CLuaHongBaoMgr.HasGetHongBaoCover(k),
                isNew = CLuaHongBaoMgr.HongBaoCoverIsNew(k)
            }
            table.insert(self.m_HongBaoList,data)
        end
    end)
    -- 排序，把可用的红包提到前面
    table.sort(self.m_HongBaoList,function(a,b)
        if a.canUse and not b.canUse then
            return true
        elseif not a.canUse and b.canUse then
            return false
        else
            return a.id < b.id
        end
    end)
end

-- 显示一页
function LuaHongBaoCoverWnd:InitOnePage(pageNum)
    local pageTbl = self.Table
    if pageNum > 0 then
        pageTbl = CUICommonDef.AddChild(self.PageGrid.gameObject,self.Table.gameObject)
    end
    Extensions.RemoveAllChildren(pageTbl.transform)
    for i = 0,self.m_MaxItemInPage - 1 do
        local index = pageNum * self.m_MaxItemInPage + i
        self:AddOneHongBao(pageTbl,self.m_HongBaoList[index + 1])
    end
    pageTbl.gameObject:SetActive(true)
    self.PageGrid:GetComponent(typeof(UIGrid)):Reposition()
    pageTbl:GetComponent(typeof(UITable)):Reposition()
end

function LuaHongBaoCoverWnd:AddOneHongBao(table,hongbaoData)
    local HongBaoItem = CUICommonDef.AddChild(table.gameObject,self.HongBaoCoverItem)
    HongBaoItem.gameObject:SetActive(true)
    local NewTip = HongBaoItem.transform:Find("NewTip").gameObject
    if hongbaoData == nil then
        HongBaoItem.transform:Find("DefaultBG").gameObject:SetActive(true)
        HongBaoItem.transform:Find("HongBaoCover").gameObject:SetActive(false)
        NewTip:SetActive(false)
    else
        HongBaoItem.transform:Find("DefaultBG").gameObject:SetActive(false)
        NewTip:SetActive(hongbaoData.isNew and hongbaoData.canUse)
        local hongbaoCover = HongBaoItem.transform:Find("HongBaoCover").gameObject
        local alphaVale = 1
        if hongbaoData.canUse then alphaVale = 1 else alphaVale = 0.5 end
        hongbaoCover.transform:GetComponent(typeof(UIWidget)).alpha = alphaVale
        self:ShowHongBaoCover(hongbaoCover,hongbaoData,NewTip)
        self.m_HongBaoItemList[hongbaoData.id] = HongBaoItem
    end
end

function LuaHongBaoCoverWnd:ShowHongBaoCover(hongbaoCover,hongbaoData,newTip)
    local coverTex = hongbaoCover.transform:Find("HongBaoCoverTex"):GetComponent(typeof(CUITexture))
    local NameLabel = hongbaoCover.transform:Find("HongBaoNameLabel/NameLabel"):GetComponent(typeof(UILabel))
    local DefaultLabel = hongbaoCover.transform:Find("HongBaoNameLabel/DefaultLabel"):GetComponent(typeof(UILabel))
    local curID = CLuaHongBaoMgr.GetCurHongBaoCover()
    local isCurHongBao = CLuaHongBaoMgr.GetCurHongBaoCover() == hongbaoData.id
    coverTex:LoadMaterial("UI/Texture/Transparent/Material/"..hongbaoData.resourceName..".mat")
    NameLabel.text = hongbaoData.name
    NameLabel.gameObject:SetActive(not isCurHongBao)
    DefaultLabel.gameObject:SetActive(isCurHongBao)
    CommonDefs.AddOnClickListener(coverTex.gameObject, DelegateFactory.Action_GameObject(function (go)
        if hongbaoData.canUse then
            if not CClientMainPlayer.Inst then return end
            PlayerPrefs.SetInt(CClientMainPlayer.Inst.Id.."HasClickHongBaoCover"..tostring(hongbaoData.id), 1)
            newTip:SetActive(false)
        end
        CLuaHongBaoMgr.ShowHongBaoCoverInfoWnd(hongbaoData)
    end), false)
    UIEventListener.Get(coverTex.gameObject).onDrag = DelegateFactory.VectorDelegate(function(g,delta)
        self:OnDrag(g,delta)
    end)
end
-- 更新切换页面按钮和页数的显示
function LuaHongBaoCoverWnd:InitChangePageBtn()
    self.LeftBtn.gameObject:SetActive(self.m_CurPageNum ~= 1)
    self.RightBtn.gameObject:SetActive(self.m_CurPageNum ~= self.m_PageNum)
    self.PageLabel.text = SafeStringFormat3(LocalString.GetString("第 %d/%d 页"),self.m_CurPageNum,self.m_PageNum)
end

function LuaHongBaoCoverWnd:UpdateCurHongBaoLabel(beforeId,curId)
    local beforeObj = self.m_HongBaoItemList[beforeId]
    local CurObj = self.m_HongBaoItemList[curId]
    if beforeObj then
        beforeObj.transform:Find("HongBaoCover/HongBaoNameLabel/DefaultLabel").gameObject:SetActive(false)
        beforeObj.transform:Find("HongBaoCover/HongBaoNameLabel/NameLabel").gameObject:SetActive(true)
    end
    if CurObj then
        CurObj.transform:Find("HongBaoCover/HongBaoNameLabel/DefaultLabel").gameObject:SetActive(true)
        CurObj.transform:Find("HongBaoCover/HongBaoNameLabel/NameLabel").gameObject:SetActive(false)
    end
end

function LuaHongBaoCoverWnd:OnDrag(g,delta)
    local moveDeg = 57.2958*math.asin(math.max(-1,math.min(delta.x/400,1)))
    if moveDeg > 1 then
        self:ChangePage(self.m_CurPageNum - 1)
    elseif moveDeg < -1 then
        self:ChangePage(self.m_CurPageNum + 1)
    end
end
-- 切换到某一页
function LuaHongBaoCoverWnd:ChangePage(index)
    if index <= 0 or index > self.m_PageNum then return end
    if index == self.m_CurPageNum then return end
    if self.m_IsChangePage then return end
    self.m_IsChangePage = true
    self.m_CurPageNum = index 
    local ChangePageTime = 0.5
    local offset = -(self.PageGrid.cellWidth * (index - 1))
    LuaTweenUtils.TweenPositionX(self.PageGrid.transform,offset,ChangePageTime)
    local onFinish = function()
        self.m_IsChangePage = false
        self:InitChangePageBtn()
    end
    if self.m_AnimTick then
        UnRegisterTick(self.m_AnimTick)
        self.m_AnimTick = nil
    end
    self.m_AnimTick = RegisterTickOnce(onFinish,ChangePageTime*1000)
end

--@region UIEvent

function LuaHongBaoCoverWnd:OnTipBtnClick()
    g_MessageMgr:ShowMessage("HongBaoCover_Desc")
end

function LuaHongBaoCoverWnd:OnLeftBtnClick()
    self:ChangePage(self.m_CurPageNum - 1)
end

function LuaHongBaoCoverWnd:OnRightBtnClick()
    self:ChangePage(self.m_CurPageNum + 1)
end


--@endregion UIEvent
function LuaHongBaoCoverWnd:OnEnable()
    g_ScriptEvent:AddListener("SetCurHongBaoCover",self,"UpdateCurHongBaoLabel")
end

function LuaHongBaoCoverWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SetCurHongBaoCover",self,"UpdateCurHongBaoLabel")

    if self.m_AnimTick then
        UnRegisterTick(self.m_AnimTick)
        self.m_AnimTick = nil
    end
end
