local UITexture = import "UITexture"

local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CUIFx = import "L10.UI.CUIFx"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CRankData = import "L10.UI.CRankData"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"

LuaRunningWildTangYuanResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaRunningWildTangYuanResultWnd, "TangYuanIcon", "TangYuanIcon", UITexture)
RegistChildComponent(LuaRunningWildTangYuanResultWnd, "CountLabel", "CountLabel", UILabel)
RegistChildComponent(LuaRunningWildTangYuanResultWnd, "HuanGuanIcon", "HuanGuanIcon", CUITexture)
RegistChildComponent(LuaRunningWildTangYuanResultWnd, "CurrentScoreLabel", "CurrentScoreLabel", UILabel)
RegistChildComponent(LuaRunningWildTangYuanResultWnd, "BestScoreToday", "BestScoreToday", UILabel)
RegistChildComponent(LuaRunningWildTangYuanResultWnd, "RecordRankLabel", "RecordRankLabel", UILabel)
RegistChildComponent(LuaRunningWildTangYuanResultWnd, "NewRecordTag", "NewRecordTag", GameObject)
RegistChildComponent(LuaRunningWildTangYuanResultWnd, "NewRecordFx", "NewRecordFx", CUIFx)
RegistChildComponent(LuaRunningWildTangYuanResultWnd, "PlayerPotrait", "PlayerPotrait", CUITexture)
RegistChildComponent(LuaRunningWildTangYuanResultWnd, "PlayerName", "PlayerName", UILabel)
RegistChildComponent(LuaRunningWildTangYuanResultWnd, "ServerName", "ServerName", UILabel)
RegistChildComponent(LuaRunningWildTangYuanResultWnd, "PlayerID", "PlayerID", UILabel)
RegistChildComponent(LuaRunningWildTangYuanResultWnd, "PlayerLevel", "PlayerLevel", UILabel)
RegistChildComponent(LuaRunningWildTangYuanResultWnd, "RankBtn", "RankBtn", GameObject)
RegistChildComponent(LuaRunningWildTangYuanResultWnd, "ShareBtn", "ShareBtn", GameObject)
RegistChildComponent(LuaRunningWildTangYuanResultWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaRunningWildTangYuanResultWnd, "RewardTimes", "RewardTimes", UILabel)
RegistChildComponent(LuaRunningWildTangYuanResultWnd, "BgFxNode", "BgFxNode", CUIFx)
RegistChildComponent(LuaRunningWildTangYuanResultWnd, "Top1FxNode", "Top1FxNode", CUIFx)

--@endregion RegistChildComponent end
RegistClassMember(LuaRunningWildTangYuanResultWnd,"m_ShareTick")
RegistClassMember(LuaRunningWildTangYuanResultWnd,"m_RankSprits")
function LuaRunningWildTangYuanResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.RankBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankBtnClick()
	end)

	UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)
	self.ShareBtn.gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
    --@endregion EventBind end
	self.m_RankSprits = {
        [1]= "UI/Texture/Transparent/Material/grabchristmasgiftrankwnd_huangguang_01.mat",
        [2]="UI/Texture/Transparent/Material/grabchristmasgiftrankwnd_huangguang_02.mat",
        [3]="UI/Texture/Transparent/Material/grabchristmasgiftrankwnd_huangguang_03.mat",
    }
	self.m_RankData = {}
	local setting = YuanXiao_TangYuan2022Setting.GetData()
	Gac2Gas.QueryRank(setting.ScoreRankId)

	local maxRewardTimes = setting.DailyPlayRewardTimesLimit
	local rewardedTimes = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eTangYuan2022DailyRewardTimes)
	self.RewardTimes.text = maxRewardTimes - rewardedTimes
end

function LuaRunningWildTangYuanResultWnd:OnDisable()
	if self.m_ShareTick then
		UnRegisterTick(self.m_ShareTick)
		self.m_ShareTick = nil
	end
end

function LuaRunningWildTangYuanResultWnd:Init()
	local player = CClientMainPlayer.Inst
	if not player then  return end 
	self.PlayerName.text = player.RealName
	self.PlayerID.text = player.Id
	self.PlayerLevel.text = player.Level
	local class = player.Class
	local gender = player.Gender
	self.PlayerPotrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(class, gender, -1), false)
	self.ServerName.text = player:GetMyServerName()

	--init score view
	self.CountLabel.text = SafeStringFormat3("x%d",LuaTangYuan2022Mgr.m_CurFinishNum)
	self.CurrentScoreLabel.text = LuaTangYuan2022Mgr.m_CurScore
	LuaTangYuan2022Mgr.m_RecordedScore = math.max(LuaTangYuan2022Mgr.m_RecordedScore,LuaTangYuan2022Mgr.m_CurScore)
	self.BestScoreToday.text = LuaTangYuan2022Mgr.m_RecordedScore
	self.NewRecordTag:SetActive(LuaTangYuan2022Mgr.m_CurScore > LuaTangYuan2022Mgr.m_RecordedScore)
	if LuaTangYuan2022Mgr.m_CurScore > LuaTangYuan2022Mgr.m_RecordedScore then
		self.NewRecordFx:LoadFx("Fx/UI/Prefab/UI_tangyuan_xinjilu.prefab")
	end 

	local rankInplay = LuaTangYuan2022Mgr.m_RankInPlay
	self.HuanGuanIcon:LoadMaterial(self.m_RankSprits[rankInplay])
	--哭脸汤圆
	if rankInplay > 1 then
		self.TangYuanIcon.enabled = true
	else
		self.TangYuanIcon.enabled = false
		self.Top1FxNode:LoadFx("Fx/UI/Prefab/UI_tangyuan_diyiming.prefab")
	end

	self.BgFxNode:LoadFx("Fx/UI/Prefab/UI_tangyuan_yanhua.prefab")
end

--@region UIEvent

function LuaRunningWildTangYuanResultWnd:OnRankBtnClick()
	LuaTangYuan2022Mgr.OpenRankWnd()
end

function LuaRunningWildTangYuanResultWnd:OnShareBtnClick()
	if self.m_ShareTick then
		UnRegisterTick(self.m_ShareTick)
		self.m_ShareTick = nil
	end
	self.m_ShareTick = RegisterTickOnce(function()
		self.RankBtn:SetActive(true)
		self.ShareBtn:SetActive(true)
		self.CloseButton:SetActive(true)
	end,2000)
	self.RankBtn:SetActive(false)
	self.ShareBtn:SetActive(false)
	self.CloseButton:SetActive(false)
	CUICommonDef.CaptureScreenAndShare()
end

--@endregion UIEvent

function LuaRunningWildTangYuanResultWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaRunningWildTangYuanResultWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaRunningWildTangYuanResultWnd:OnRankDataReady( ... )
	local PlayerRankInfo = CRankData.Inst.MainPlayerRankInfo
	if PlayerRankInfo then
		self.RecordRankLabel.text = PlayerRankInfo.Rank ~= 0 and PlayerRankInfo.Rank or LocalString.GetString("未上榜")
	else
		self.RecordRankLabel.text = LocalString.GetString("未上榜")
	end
end
