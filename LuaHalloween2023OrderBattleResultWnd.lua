local DelegateFactory  = import "DelegateFactory"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CUITexture = import "L10.UI.CUITexture"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CRenderObject = import "L10.Engine.CRenderObject"
local CClientObject = import "L10.Game.CClientObject"
local CTeamMgr = import "L10.Game.CTeamMgr"
local LayerDefine = import "L10.Engine.LayerDefine"
local Quaternion = import "UnityEngine.Quaternion"

LuaHalloween2023OrderBattleResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistClassMember(LuaHalloween2023OrderBattleResultWnd, "m_ModelTexture")
RegistClassMember(LuaHalloween2023OrderBattleResultWnd, "m_RankLabel")
RegistClassMember(LuaHalloween2023OrderBattleResultWnd, "m_RankTipLabel")
RegistClassMember(LuaHalloween2023OrderBattleResultWnd, "m_OrderCountLabel")
RegistClassMember(LuaHalloween2023OrderBattleResultWnd, "m_ScoreLabel")
RegistClassMember(LuaHalloween2023OrderBattleResultWnd, "m_Player1NameLabel")
RegistClassMember(LuaHalloween2023OrderBattleResultWnd, "m_Player2NameLabel")
RegistClassMember(LuaHalloween2023OrderBattleResultWnd, "m_ShareBtn")
RegistClassMember(LuaHalloween2023OrderBattleResultWnd, "m_AgainBtn")

RegistClassMember(LuaHalloween2023OrderBattleResultWnd, "m_ZuoQiId")
RegistClassMember(LuaHalloween2023OrderBattleResultWnd, "m_DriverPlayerId")
RegistClassMember(LuaHalloween2023OrderBattleResultWnd, "m_PassengerPlayerId")

RegistClassMember(LuaHalloween2023OrderBattleResultWnd, "m_DriverRO")
RegistClassMember(LuaHalloween2023OrderBattleResultWnd, "m_PassengerRO")
RegistClassMember(LuaHalloween2023OrderBattleResultWnd, "m_IdentifierStr")
RegistClassMember(LuaHalloween2023OrderBattleResultWnd, "m_ModelTextureLoader")
--@endregion RegistChildComponent end

function LuaHalloween2023OrderBattleResultWnd:Awake()
    self.m_ModelTexture = self.transform:Find("Other/ModelTexture/TemporaryModel"):GetComponent(typeof(CUITexture))
    self.m_RankLabel = self.transform:Find("Info/WinStyle/Vfx_ShengLi/RankLabel"):GetComponent(typeof(UILabel))
    self.m_RankTipLabel = self.transform:Find("Other/RankTip"):GetComponent(typeof(UILabel))
    self.m_OrderCountLabel = self.transform:Find("Other/Desc/CountLabel"):GetComponent(typeof(UILabel))
    self.m_ScoreLabel = self.transform:Find("Other/Desc/ScoreLabel"):GetComponent(typeof(UILabel))
    self.m_Player1NameLabel = self.transform:Find("Other/PlayerTable/Player1"):GetComponent(typeof(UILabel))
    self.m_Player2NameLabel = self.transform:Find("Other/PlayerTable/Player2"):GetComponent(typeof(UILabel))
    self.m_ShareBtn = self.transform:Find("Other/BottomBtns/Btn01").gameObject
    self.m_AgainBtn = self.transform:Find("Other/BottomBtns/Btn02").gameObject
    UIEventListener.Get(self.m_ShareBtn).onClick = DelegateFactory.VoidDelegate(function()
        self:OnShareBtnClick()
    end)
    UIEventListener.Get(self.m_AgainBtn).onClick = DelegateFactory.VoidDelegate(function()
        self:OnAgainBtnClick()
    end)
end

function LuaHalloween2023OrderBattleResultWnd:Init()
    -- 有队伍，且为自己为队长才显示再来一局按钮
    local canAgain = CTeamMgr.Inst:TeamExists() and CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == CTeamMgr.Inst.LeaderId
    self.m_ShareBtn.gameObject:SetActive(false)
    self.m_AgainBtn:SetActive(canAgain)
    self.m_ShareBtn.transform.localPosition = canAgain and Vector3(-427, -451, 0) or Vector3(721, -451, 0)

    self:InitTeamInfo()
    self:InitModel()
end

function LuaHalloween2023OrderBattleResultWnd:InitTeamInfo()
    -- 玩法结算信息
    self.m_RankLabel.text = LuaHalloween2023Mgr.m_CandyDeliveryResultInfo.Rank
    self.m_OrderCountLabel.text = LuaHalloween2023Mgr.m_CandyDeliveryResultInfo.OrderCount
    self.m_ScoreLabel.text = LuaHalloween2023Mgr.m_CandyDeliveryResultInfo.Score
    self.m_RankTipLabel.text = Halloween2023_Setting.GetData().CandyRankDescStrings[LuaHalloween2023Mgr.m_CandyDeliveryResultInfo.Rank - 1]
    -- self.m_OrderCountLabel.transform:Find("Label"):GetComponent(typeof(UILabel)):ResetAndUpdateAnchors()
    -- self.m_ScoreLabel.transform:Find("Label"):GetComponent(typeof(UILabel)):ResetAndUpdateAnchors()

    -- 队伍信息
    self.m_ZuoQiId = LuaHalloween2023Mgr.m_MyTeamRoleInfo.zuoQiId
    self.m_DriverPlayerId = LuaHalloween2023Mgr.m_MyTeamRoleInfo.driverPlayerId
    self.m_PassengerPlayerId = LuaHalloween2023Mgr.m_MyTeamRoleInfo.passengerPlayerId
    self:InitPlayerName(self.m_DriverPlayerId, self.m_Player1NameLabel)
    self:InitPlayerName(self.m_PassengerPlayerId, self.m_Player2NameLabel)
end

function LuaHalloween2023OrderBattleResultWnd:InitPlayerName(playerId, nameLabel)
    local player = CTeamMgr.Inst:GetMemberById(playerId)
    if player then
        nameLabel.text = player.m_MemberName
    elseif CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == playerId then
        nameLabel.text = CClientMainPlayer.Inst.Name
    else
        nameLabel.gameObject:SetActive(false)
    end
end

function LuaHalloween2023OrderBattleResultWnd:InitModel()
    self.m_IdentifierStr = "__Halloween2023OrderBattleResultWnd__"
	self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self.m_DriverRO = ro
        self:LoadModel()
    end)
    self.m_ModelTexture.mainTexture = CUIManager.CreateModelTexture(self.m_IdentifierStr, self.m_ModelTextureLoader, -170, -0.1, -0.9, 8.16, false, true, 1, true, false)
end

function LuaHalloween2023OrderBattleResultWnd:LoadModel()
    -- 加载司机
    local driverAppearance = CClientPlayerMgr.Inst:GetPlayerAppearance(self.m_DriverPlayerId)
    if driverAppearance == nil then return end
    CClientMainPlayer.LoadResource(self.m_DriverRO, self:DealwithAppearance(driverAppearance), true, 1.0, self.m_ZuoQiId, true, 0, false, 0, false, nil, false)
    self.m_DriverRO.VehicleRO.transform.localRotation = Quaternion.Euler(8.6, -27.26, -13)
    -- 加载乘客
    self.m_PassengerRO = CRenderObject.CreateRenderObject(self.m_DriverRO.VehicleRO.gameObject, "passengerPlayer")
    self.m_PassengerRO.Layer = LayerDefine.ModelForNGUI_3D
    local passengerAppearance = CClientPlayerMgr.Inst:GetPlayerAppearance(self.m_PassengerPlayerId)
    if passengerAppearance == nil then return end
    CClientMainPlayer.LoadResource(self.m_PassengerRO, self:DealwithAppearance(passengerAppearance), true, 1.0, 0, true, 0, false, 0, false, nil, false)
    CClientObject.GetInVehicleAsPassenger(self.m_DriverRO.VehicleRO, self.m_PassengerRO, self.m_ZuoQiId, 1)
    self.m_PassengerRO.transform.localScale = Vector3.one
end

function LuaHalloween2023OrderBattleResultWnd:DealwithAppearance(fakeAppearance)
    fakeAppearance.IntesifySuitId = 0       -- 关特效
    fakeAppearance.Wing = 0                 -- 关翅膀
    fakeAppearance.TalismanAppearance = 0   -- 关法宝
    fakeAppearance.HideBackPendant = 1      -- 关背饰
    fakeAppearance.HideGemFxToOtherPlayer = 1   -- 关武器石之灵
    fakeAppearance.Equipment[1] = 0         -- 卸武器
    return fakeAppearance
end

--@region UIEvent
function LuaHalloween2023OrderBattleResultWnd:OnShareBtnClick()
	CUICommonDef.CaptureScreenUIAndShare(CLuaUIResources.Halloween2023OrderBattleResultWnd, self.m_ShareBtn.transform.parent.gameObject)
end

function LuaHalloween2023OrderBattleResultWnd:OnAgainBtnClick()
    CUIManager.ShowUI(CLuaUIResources.Halloween2023OrderBattleEnterWnd)
end

--@endregion UIEvent
function LuaHalloween2023OrderBattleResultWnd:OnDisable()
    CUIManager.DestroyModelTexture(self.m_IdentifierStr)
    self.m_DriverRO = nil
    self.m_PassengerRO = nil
end