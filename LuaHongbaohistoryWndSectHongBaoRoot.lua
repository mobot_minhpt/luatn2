local QnTableView=import "L10.UI.QnTableView"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CItem = import "L10.Game.CItem"
local NGUIText = import "NGUIText"

LuaHongbaohistoryWndSectHongBaoRoot = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHongbaohistoryWndSectHongBaoRoot, "YiqiangLabel", "YiqiangLabel", UILabel)
RegistChildComponent(LuaHongbaohistoryWndSectHongBaoRoot, "YifaLabel", "YifaLabel", UILabel)
RegistChildComponent(LuaHongbaohistoryWndSectHongBaoRoot, "YiqiangMoney", "YiqiangMoney", UILabel)
RegistChildComponent(LuaHongbaohistoryWndSectHongBaoRoot, "YifaMoney", "YifaMoney", UILabel)
RegistChildComponent(LuaHongbaohistoryWndSectHongBaoRoot, "QnTableView", "QnTableView", QnTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaHongbaohistoryWndSectHongBaoRoot,"m_DataList")
RegistClassMember(LuaHongbaohistoryWndSectHongBaoRoot,"m_RecordDifferentColor")

function LuaHongbaohistoryWndSectHongBaoRoot:Awake()
    self.m_DataList = {}
    self.m_RecordDifferentColor = SectItemRedPack_Setting.GetData().RecordDifferentColor
    self.QnTableView.m_DataSource=DefaultTableViewDataSource.Create(function()
        return self.m_DataList and #self.m_DataList or 0
    end, function(item, index)
        self:InitItem(item, index)
    end)
end

function LuaHongbaohistoryWndSectHongBaoRoot:OnEnable()
    g_ScriptEvent:AddListener("SendSectItemRedPackHistory", self, "LoadData")
end

function LuaHongbaohistoryWndSectHongBaoRoot:OnDisable()
    g_ScriptEvent:RemoveListener("SendSectItemRedPackHistory", self, "LoadData")
end

function LuaHongbaohistoryWndSectHongBaoRoot:LoadData(sendTimes, sendTotalJade, recvTimes, recvTotalJade,list)
    self.m_DataList = list
    self.QnTableView:ReloadData(true, true)
    self.YiqiangLabel.text = SafeStringFormat3(LocalString.GetString("%d个"),recvTimes) 
    self.YifaLabel.text = SafeStringFormat3(LocalString.GetString("%d个"),sendTimes) 
    self.YiqiangMoney.text = recvTotalJade
    self.YifaMoney.text = sendTotalJade
end

function LuaHongbaohistoryWndSectHongBaoRoot:InitItem(item, index)
    local data = self.m_DataList[index + 1]
    local timestamp = data.timestamp
    local itemId = data.itemId
    local itemNum = data.itemNum 

    local t = CServerTimeMgr.ConvertTimeStampToZone8Time(timestamp)
    local timeString = ToStringWrap(t, "yyyy-MM-dd HH:mm:ss")
    local moneyString = SafeStringFormat3(LocalString.GetString(itemNum == 1 and "%s" or "%sx%d"),CItem.GetName(itemId),itemNum) 
    local mallData = Mall_LingYuMall.GetData(itemId)
    local price = mallData and (itemNum * mallData.Jade) or 0

    local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local moneyLabel = item.transform:Find("MoneyLabel"):GetComponent(typeof(UILabel))

    timeLabel.text = timeString
    moneyLabel.text = moneyString
    for i = 0,self.m_RecordDifferentColor.Length - 1,2 do
        local maxPrice = tonumber(self.m_RecordDifferentColor[i])
        local color = NGUIText.ParseColor24(self.m_RecordDifferentColor[i + 1], 0)
        if price < maxPrice then
            moneyLabel.color = color
            break
        end
    end
end