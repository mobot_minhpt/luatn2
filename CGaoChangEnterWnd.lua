-- Auto Generated!!
local CActivityEnterMgr = import "L10.UI.CActivityEnterMgr"
local CButton = import "L10.UI.CButton"
local CGaoChangEnterWnd = import "L10.UI.CGaoChangEnterWnd"
local CGaoChangMgr = import "L10.Game.CGaoChangMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local L10 = import "L10"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
CGaoChangEnterWnd.m_Init_CS2LuaHook = function (this) 
    this.titleLabel.text = CActivityEnterMgr.wndTitleStr
    this.textLabel.text = CActivityEnterMgr.actDesc
    if System.String.IsNullOrEmpty(CActivityEnterMgr.rightBtnLabel) and CActivityEnterMgr.onRightBtnClickAction == nil then
        this.rightBtn:SetActive(false)
    else
        this.rightBtn:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(this.rightBtn, typeof(CButton)).Enabled = CActivityEnterMgr.enableRightBtn
        CommonDefs.GetComponent_Component_Type(this.rightBtn.transform:Find("Label"), typeof(UILabel)).text = CActivityEnterMgr.rightBtnLabel
        local default
        if CActivityEnterMgr.useYellowRightBtn then
            default = this.yellowBtnSprite
        else
            default = this.blueBtnSprite
        end
        CommonDefs.GetComponent_GameObject_Type(this.rightBtn, typeof(UISprite)).spriteName = default
    end
    if System.String.IsNullOrEmpty(CActivityEnterMgr.leftBtnLabel) and CActivityEnterMgr.onLeftBtnClickAction == nil then
        this.leftBtn:SetActive(false)
    else
        this.leftBtn:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(this.leftBtn, typeof(CButton)).Enabled = CActivityEnterMgr.enableLeftBtn
        CommonDefs.GetComponent_Component_Type(this.leftBtn.transform:Find("Label"), typeof(UILabel)).text = CActivityEnterMgr.leftBtnLabel
    end
    if System.String.IsNullOrEmpty(CActivityEnterMgr.centerBtnLabel) and CActivityEnterMgr.onCenterBtnClickAction == nil then
        this.centerBtn:SetActive(false)
    else
        this.centerBtn:SetActive(true)
        CommonDefs.GetComponent_Component_Type(this.centerBtn.transform:Find("Label"), typeof(UILabel)).text = CActivityEnterMgr.centerBtnLabel
    end

    this.scrollview:ResetPosition()
end
CGaoChangEnterWnd.m_Awake_CS2LuaHook = function (this) 
    if CGaoChangEnterWnd.Instance ~= nil then
        L10.CLogMgr.LogError("more than one instance running!")
    end
    CGaoChangEnterWnd.Instance = this
end

CActivityEnterMgr.m_ShowActivityEnterWnd_CS2LuaHook = function (wndTitle, actDescription, rightBtnStr, leftBtnStr, enableRight, enableLeft, onRightBtnClick, onLeftBtnClick, useYellowRightButton, centerBtnStr, onCenterBtnClick) 
    CActivityEnterMgr.wndTitleStr = wndTitle
    CActivityEnterMgr.actDesc = actDescription
    CActivityEnterMgr.rightBtnLabel = rightBtnStr
    CActivityEnterMgr.leftBtnLabel = leftBtnStr
    CActivityEnterMgr.onRightBtnClickAction = onRightBtnClick
    CActivityEnterMgr.onLeftBtnClickAction = onLeftBtnClick
    CActivityEnterMgr.enableRightBtn = enableRight
    CActivityEnterMgr.enableLeftBtn = enableLeft
    CActivityEnterMgr.useYellowRightBtn = useYellowRightButton
    CActivityEnterMgr.centerBtnLabel = centerBtnStr
    CActivityEnterMgr.onCenterBtnClickAction = onCenterBtnClick

    CUIManager.ShowUI(CUIResources.GaoChangEnterWnd)
end
CActivityEnterMgr.m_ShowGaochangEnterWnd_CS2LuaHook = function () 
    if LuaGaoChangJiDouMgr:IsOpen() then -- 高昌激斗模式开启
        return
    end
    local okBtnStr = LocalString.GetString("我要报名")
    if not CGaoChangMgr.Inst.canApply then
        okBtnStr = LocalString.GetString("已报名")
    end
    CActivityEnterMgr.ShowActivityEnterWnd(LocalString.GetString("高昌迷宫"), g_MessageMgr:FormatMessage("GAOCHANG_INTERFACE_TIP", nil), okBtnStr, LocalString.GetString("进入高昌"), CGaoChangMgr.Inst.canApply, true, MakeDelegateFromCSFunction(CActivityEnterMgr.OnGaoChangApplyButtonClick, Action0, CActivityEnterMgr), MakeDelegateFromCSFunction(CActivityEnterMgr.OnGaoChangeEnterButtonClick, Action0, CActivityEnterMgr), true, "", nil)
end
CActivityEnterMgr.m_OnGaoChangApplyButtonClick_CS2LuaHook = function () 
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("GAOCHANG_APPLY_INTERFACE_TIP", nil), DelegateFactory.Action(function () 
        Gac2Gas.GaoChangApply()
    end), nil, nil, nil, false)
end
