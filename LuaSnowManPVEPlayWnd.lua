local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CSkillItemCell = import "L10.UI.CSkillItemCell"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local Boolean = import "System.Boolean"
local UInt32 = import "System.UInt32"
local QnTabButton = import "L10.UI.QnTabButton"

LuaSnowManPVEPlayWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaSnowManPVEPlayWnd, "SnowManIconTemplate01", "SnowManIconTemplate01", GameObject)
RegistChildComponent(LuaSnowManPVEPlayWnd, "SnowManIconTemplate02", "SnowManIconTemplate02", GameObject)
RegistChildComponent(LuaSnowManPVEPlayWnd, "SnowManIconTemplate03", "SnowManIconTemplate03", GameObject)
RegistChildComponent(LuaSnowManPVEPlayWnd, "SnowManIconTemplate04", "SnowManIconTemplate04", GameObject)
RegistChildComponent(LuaSnowManPVEPlayWnd, "SnowManIconTemplate05", "SnowManIconTemplate05", GameObject)
RegistChildComponent(LuaSnowManPVEPlayWnd, "SnowManIconTemplate06", "SnowManIconTemplate06", GameObject)
RegistChildComponent(LuaSnowManPVEPlayWnd, "BigPortrait", "BigPortrait", CUITexture)
RegistChildComponent(LuaSnowManPVEPlayWnd, "Item1", "Item1", GameObject)
RegistChildComponent(LuaSnowManPVEPlayWnd, "Item2", "Item2", GameObject)
RegistChildComponent(LuaSnowManPVEPlayWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaSnowManPVEPlayWnd, "DiffLevelLabel", "DiffLevelLabel", UILabel)
RegistChildComponent(LuaSnowManPVEPlayWnd, "PropertyContent", "PropertyContent", GameObject)
RegistChildComponent(LuaSnowManPVEPlayWnd, "PropertyPair", "PropertyPair", GameObject)
RegistChildComponent(LuaSnowManPVEPlayWnd, "SkillContent", "SkillContent", GameObject)
RegistChildComponent(LuaSnowManPVEPlayWnd, "SkillItemTemplate", "SkillItemTemplate", GameObject)
RegistChildComponent(LuaSnowManPVEPlayWnd, "RankButton", "RankButton", GameObject)
RegistChildComponent(LuaSnowManPVEPlayWnd, "ChallengeButton", "ChallengeButton", GameObject)
RegistChildComponent(LuaSnowManPVEPlayWnd, "RuleButton", "RuleButton", GameObject)
RegistChildComponent(LuaSnowManPVEPlayWnd, "DifficultyTips", "DifficultyTips", UILabel)

--@endregion RegistChildComponent end

function LuaSnowManPVEPlayWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:RefreshConstUI()
    self:RefreshVariableUI(self.selectSnowManId) 
    self:InitUIEvent()
end

function LuaSnowManPVEPlayWnd:Init()
    Gac2Gas.BingTianShiLianRequestData()
end

function LuaSnowManPVEPlayWnd:InitWndData()
    self.leftTabButtonList = {}
    self.snowManLevel = {}
    HanJia2023_BingTianShiLian.Foreach(function(id, _)
        self.snowManLevel[id] = 1
    end)
    if LuaHanJia2023Mgr.snowManPVEData and LuaHanJia2023Mgr.snowManPVEData.monsterLv then
        for k, v in pairs(LuaHanJia2023Mgr.snowManPVEData.monsterLv) do
            --monsterLv是已通关的等级, 挑战等级要+1
            self.snowManLevel[k] = v+1
        end
    end

    self.difficultyMax = HanJia2023_BingTianShiLianSetting.GetData().MaxDifficulty
    self.selectSnowManId = 1
    if LuaHanJia2023Mgr.snowManPVEData and LuaHanJia2023Mgr.snowManPVEData.lastSelect then
        local lastSelectSnowManId = LuaHanJia2023Mgr.snowManPVEData.lastSelect
        if self.snowManLevel[lastSelectSnowManId] == LuaHanJia2023Mgr.snowManPVEData.curUnlockDifficulty+1 then
            --上次挑战的已经满级啦, 遍历一下找一个还没有满级的
            for k, v in pairs(self.snowManLevel) do
                --这个v的值+1处理过了, 表示下一关挑战
                if v ~= LuaHanJia2023Mgr.snowManPVEData.curUnlockDifficulty+1 then
                    self.selectSnowManId = k
                    break
                end
            end
        else
            --上次挑战的没有满级, 还选这个
            self.selectSnowManId = lastSelectSnowManId
        end
    end
    self.attributeList = {}
    self.skillList = {}
end

function LuaSnowManPVEPlayWnd:InitUIEvent()
    UIEventListener.Get(self.RankButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        if CClientMainPlayer.Inst then
            local seasonId = LuaHanJia2023Mgr:GetSeason()
            if seasonId then
                LuaSnowManPVERankWnd.selectMonsterType = self.selectSnowManId
                LuaSnowManPVERankWnd.selectProfessionType = EnumToInt(CClientMainPlayer.Inst.Class)
                CUIManager.ShowUI(CLuaUIResources.SnowManPVERankWnd)
            else
                g_MessageMgr:ShowMessage("BINGTIANSHILIAN_NO_SEASON")
            end
        end
    end)

    UIEventListener.Get(self.ChallengeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        local seasonId = LuaHanJia2023Mgr:GetSeason()
        if seasonId then
            Gac2Gas.BingTianShiLianEnterPlay(self.selectSnowManId)
        else
            g_MessageMgr:ShowMessage("BINGTIANSHILIAN_NO_SEASON")
        end
    end)

    UIEventListener.Get(self.RuleButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("HanJia2023_SnowManPVE_Tips")
    end)
    
    local iconTemplateArr = {self.SnowManIconTemplate01, self.SnowManIconTemplate02, self.SnowManIconTemplate03, self.SnowManIconTemplate04,
                             self.SnowManIconTemplate05, self.SnowManIconTemplate06}
    for i = 1, #iconTemplateArr do
        local tabButton = iconTemplateArr[i]
        UIEventListener.Get(tabButton).onClick = DelegateFactory.VoidDelegate(function()
            self.selectSnowManId = i
            self:RefreshVariableUI(self.selectSnowManId)
        end)
    end
end

function LuaSnowManPVEPlayWnd:RefreshConstUI()
    local iconTemplateArr = {self.SnowManIconTemplate01, self.SnowManIconTemplate02, self.SnowManIconTemplate03, self.SnowManIconTemplate04,
                                self.SnowManIconTemplate05, self.SnowManIconTemplate06}
    local iconIndex = 1
    HanJia2023_BingTianShiLian.Foreach(function(_, data)
        local itemGO = iconTemplateArr[iconIndex]
        itemGO.transform:GetComponent(typeof(CUITexture)):LoadMaterial(data.SnowManIcon)
        table.insert(self.leftTabButtonList, data.ID, itemGO)
        iconIndex = iconIndex + 1
    end)
    
    self.itemList = {self.Item1, self.Item2}
    
    self.DifficultyTips.text = g_MessageMgr:FormatMessage("HanJia2023_SnowManPVE_Difficulty_Tip")
end

function LuaSnowManPVEPlayWnd:RefreshVariableUI(snowManId)
    for i = 1, #self.leftTabButtonList do
        local itemGo = self.leftTabButtonList[i]
        itemGo.transform:Find("Highlight").gameObject:SetActive(snowManId == i)
        local difficulty = self.snowManLevel[i]
        if difficulty == self.difficultyMax+1 then
            difficulty = self.difficultyMax
        end
        itemGo.transform:Find("Level"):GetComponent(typeof(UILabel)).text = difficulty
    end

    if LuaHanJia2023Mgr.snowManPVEData and LuaHanJia2023Mgr.snowManPVEData.curUnlockDifficulty then
        self.DiffLevelLabel.text = SafeStringFormat3("[c][%s]%d[-][/c]/%d", NGUIText.EncodeColor24(Color.yellow), self.snowManLevel[self.selectSnowManId], LuaHanJia2023Mgr.snowManPVEData.curUnlockDifficulty)
    end


    local snowmanConfigData = HanJia2023_BingTianShiLian.GetData(snowManId)
    self.transform:Find("CUIFx").gameObject:SetActive(false)
    self.transform:Find("CUIFx").gameObject:SetActive(true)
    self.BigPortrait:LoadMaterial(snowmanConfigData.SnowManMaterial)
    
    --Refresh Name
    self.NameLabel.text = snowmanConfigData.Name
    self.NameLabel.color = NGUIText.ParseColor24(snowmanConfigData.SnowManColor, 0)
    
    --Refresh Difficulty 
    local difficulty = self.snowManLevel[snowManId]
    local curMonsterIsMax = false
    if difficulty == self.difficultyMax+1 then
        difficulty = self.difficultyMax
        curMonsterIsMax = true
    end

    --Refresh Reward
    local rewardFakeItems = HanJia2023_BingTianShiLianReward.GetData(difficulty).FakeItemIds
    local itemArr = g_LuaUtil:StrSplit(rewardFakeItems,",")
    for i = 1, #self.itemList do
        self:InitOneItem(self.itemList[i], tonumber(itemArr[i]))
    end

    --Refresh ChallengeButton
    --满级的话, 需要处理一下
    if curMonsterIsMax then
        --这个雪人满级了, 需要挑战按钮置灰，文字显示为“已通关”？
        self.ChallengeButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("已通关")
        self.ChallengeButton.transform:GetComponent(typeof(UISprite)).spriteName = "common_btn_01_blue"
    else
        --这个雪人正常的, 挑战按钮黄色的, 文字显示未"挑战"
        self.ChallengeButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("挑战")
        self.ChallengeButton.transform:GetComponent(typeof(UISprite)).spriteName = "common_btn_01_yellow"
    end
    
    --Refresh Property
    local showAttributes = g_LuaUtil:StrSplit(snowmanConfigData.ShowAttrs,";")
    -- 假设所有的怪属性都是固定条数, 所以只生成一次, 后面就是循环使用了
    local childCount = self.PropertyContent.transform.childCount
    if childCount < #showAttributes then
        for i = childCount+1, #showAttributes do
            local attributeGo = CommonDefs.Object_Instantiate(self.PropertyPair)
            attributeGo.transform.parent = self.PropertyContent.transform
            attributeGo.transform.localScale = Vector3.one
            attributeGo:SetActive(true)
            table.insert(self.attributeList, attributeGo)
        end
    end
    for i = 1, #self.attributeList do
        local attributeInfo = g_LuaUtil:StrSplit(showAttributes[i],",")
        local nameLabel = self.attributeList[i].transform:Find("PropertyName"):GetComponent(typeof(UILabel))
        local valueLabel = self.attributeList[i].transform:Find("PropertyValue"):GetComponent(typeof(UILabel))
        if #attributeInfo == 2 then
            --name: A
            nameLabel.text = attributeInfo[1]
            local value1FormulaId = tonumber(attributeInfo[2])
            local value1Formula = AllFormulas.Action_Formula[value1FormulaId] and AllFormulas.Action_Formula[value1FormulaId].Formula or nil
            if value1Formula then
                local rate = value1Formula(nil, nil, {difficulty})
                valueLabel.text = rate
            end
        elseif #attributeInfo == 3 then
            --name: A~B
            nameLabel.text = attributeInfo[1]
            local value1FormulaId = tonumber(attributeInfo[2])
            local value1Formula = AllFormulas.Action_Formula[value1FormulaId] and AllFormulas.Action_Formula[value1FormulaId].Formula or nil
            local value1 = nil
            if value1Formula then 
                value1 = value1Formula(nil, nil, {difficulty})
            end

            local value2FormulaId = tonumber(attributeInfo[3])
            local value2Formula = AllFormulas.Action_Formula[value2FormulaId] and AllFormulas.Action_Formula[value2FormulaId].Formula or nil
            local value2 = nil
            if value2Formula then
                value2 = value2Formula(nil, nil, {difficulty})
                valueLabel.text = SafeStringFormat3("%d~%d", value1, value2)
            end
        end
    end

    --Refresh Skill
    local skillIds = g_LuaUtil:StrSplit(snowmanConfigData.SkillIds,",")
    local childCount = self.SkillContent.transform.childCount
    if childCount < #skillIds then
        for i = childCount+1, #skillIds do
            local skillGo = CommonDefs.Object_Instantiate(self.SkillItemTemplate)
            local cell = skillGo:GetComponent(typeof(CSkillItemCell))

            skillGo.transform.parent = self.SkillContent.transform
            skillGo.transform.localScale = Vector3.one
            skillGo:SetActive(true)
            table.insert(self.skillList, cell)
        end
    elseif childCount > #skillIds then
        for i = #skillIds+1, childCount do 
            local cell = self.skillList[i]
            cell.gameObject:SetActive(false)
        end    
    end
    for i = 1, #skillIds do
        --Skill
        local cell = self.skillList[i]
        local calcSkillLevel = AllFormulas.Action_Formula[1462].Formula(nil, nil, {difficulty})

        local skillLevel = calcSkillLevel
        for _skillLevel = calcSkillLevel, 1, -1 do
            if (Skill_AllSkills.Exists(tonumber(skillIds[i])*100+ _skillLevel)) then
                skillLevel = _skillLevel
                break
            end
        end
        local skillData = Skill_AllSkills.GetData(tonumber(skillIds[i]) * 100 + skillLevel)
        cell.gameObject:SetActive(true)
        cell:Init(tonumber(skillIds[i]), 0, 0, false, skillData.SkillIcon, false, nil)
        UIEventListener.Get(cell.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
            CSkillInfoMgr.ShowSkillInfoWnd(tonumber(skillIds[i])*100+skillLevel, true, 0, 0, nil)
        end)
    end
end

function LuaSnowManPVEPlayWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end
    UIEventListener.Get(curItem).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
end

function LuaSnowManPVEPlayWnd:OnEnable()
    g_ScriptEvent:AddListener("HanJia2023_SyncSnowManPVEData", self, "UpdateSnowManData")
    g_ScriptEvent:AddListener("HanJia2023_EnterPlayScene", self, "OnEnterScene")
end

function LuaSnowManPVEPlayWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HanJia2023_SyncSnowManPVEData", self, "UpdateSnowManData")
    g_ScriptEvent:RemoveListener("HanJia2023_EnterPlayScene", self, "OnEnterScene")
end

function LuaSnowManPVEPlayWnd:OnEnterScene()
    CUIManager.CloseUI(CLuaUIResources.SnowManPVEPlayWnd)
end

function LuaSnowManPVEPlayWnd:UpdateSnowManData()
    self.snowManLevel = {}
    HanJia2023_BingTianShiLian.Foreach(function(id, _)
        self.snowManLevel[id] = 1
    end)
    for k, v in pairs(LuaHanJia2023Mgr.snowManPVEData.monsterLv) do
        --monsterLv是已通关的等级, 挑战等级要+1
        self.snowManLevel[k] = v+1
    end

    self.selectSnowManId = 1
    if LuaHanJia2023Mgr.snowManPVEData and LuaHanJia2023Mgr.snowManPVEData.lastSelect then
        
        local lastSelectSnowManId = LuaHanJia2023Mgr.snowManPVEData.lastSelect
        if self.snowManLevel[lastSelectSnowManId] == LuaHanJia2023Mgr.snowManPVEData.curUnlockDifficulty+1 then
            --上次挑战的已经满级啦, 遍历一下找一个还没有满级的
            for k, v in pairs(self.snowManLevel) do
                --这个v的值+1处理过了, 表示下一关挑战
                if v ~= LuaHanJia2023Mgr.snowManPVEData.curUnlockDifficulty+1 then
                    self.selectSnowManId = k
                    break
                end
            end
        else
            --上次挑战的没有满级, 还选这个
            self.selectSnowManId = lastSelectSnowManId
        end
    end
    self:RefreshVariableUI(self.selectSnowManId)
end 