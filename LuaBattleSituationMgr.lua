local EnumStatType = import "L10.Game.EnumStatType"
local CKouDaoMgr = import "L10.Game.CKouDaoMgr"
local CGuildQueenMgr = import "L10.Game.CGuildQueenMgr"
local CPGQYMgr = import "L10.Game.CPGQYMgr"

LuaBattleSituationMgr = {}

LuaBattleSituationMgr.situationType = nil

LuaBattleSituationMgr.EnableClearButtonInKouDao = false

function LuaBattleSituationMgr.OpenBattleSituationWnd(situationType)
    LuaBattleSituationMgr.situationType = situationType
    CUIManager.ShowUI("BattleSituationWnd")
end

function LuaBattleSituationMgr.GetStatTabList()
    if LuaBattleSituationMgr.situationType == EnumSituationType.eBangHua then
        return {
            EnumSituationWndTab.eDps,
            EnumSituationWndTab.eHeal,
            EnumSituationWndTab.eSuffer,
            EnumSituationWndTab.eGuildHero,
            EnumSituationWndTab.Undefined
        }
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eKoudao then
        return {
            EnumSituationWndTab.eDps,
            EnumSituationWndTab.eHeal,
            EnumSituationWndTab.eSuffer,
            EnumSituationWndTab.Undefined
        }
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.ePGQY then
        return {
            EnumSituationWndTab.eDps,
            EnumSituationWndTab.eHeal,
            EnumSituationWndTab.eSuffer
        }
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eTianMenShan then
        return {
            EnumSituationWndTab.eDps,
            EnumSituationWndTab.eHeal,
            EnumSituationWndTab.eSuffer,
            EnumSituationWndTab.Undefined
        }
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eGuildTerritorialWarsChallenge then
        return {
            EnumSituationWndTab.eDps,
            EnumSituationWndTab.eHeal,
            EnumSituationWndTab.eSuffer
        }
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eHengYuDangKou then
        return {
            EnumSituationWndTab.eDps,
            EnumSituationWndTab.eHeal,
            EnumSituationWndTab.eSuffer,
            EnumSituationWndTab.Undefined
        }
    else
        return {
            EnumSituationWndTab.eDps,
            EnumSituationWndTab.eHeal,
            EnumSituationWndTab.eSuffer,
            EnumSituationWndTab.Undefined
        }
    end
end

function LuaBattleSituationMgr.GetStatInfo()
    if LuaBattleSituationMgr.situationType == EnumSituationType.eBangHua then
        return CGuildQueenMgr.Inst.StatInfo
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.ePGQY then
        return CPGQYMgr.Inst.StatInfo
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eKoudao then
        return CKouDaoMgr.Inst.StatInfo
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eTianMenShan then
        return CLuaCityWarMgr.TianMenShanStatInfo
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eGuildTerritorialWarsChallenge then
        return LuaGuildTerritorialWarsMgr.m_ChallengeFightDetailInfo
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eHengYuDangKou then
        return LuaHengYuDangKouMgr.m_StatInfo
    end
    return nil
end

function LuaBattleSituationMgr.GetPlayersInCopy()
    if LuaBattleSituationMgr.situationType == EnumSituationType.eBangHua then
        return CGuildQueenMgr.Inst.PlayersInCopy
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.ePGQY then
        return CPGQYMgr.Inst.PlayersInCopy
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eKoudao or LuaBattleSituationMgr.situationType == EnumSituationType.eHengYuDangKou then
        return CKouDaoMgr.Inst.PlayersInCopy
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eTianMenShan then
        return CLuaCityWarMgr.TianMenShanPlayersInCopy
    end
    return nil
end

function LuaBattleSituationMgr.GetGuildQueenHeroInfo()
    return CGuildQueenMgr.Inst.HeroInfo
end

function LuaBattleSituationMgr.RequestBangHuaHeroInfo()
    CGuildQueenMgr.Inst:RequestBangHuaHeroInfo()
end

function LuaBattleSituationMgr.RequestSituationStat(curTabStat)
    local CTabStat = LuaBattleSituationMgr.GetEnumStatTypeFromInt(curTabStat)

    if LuaBattleSituationMgr.situationType == EnumSituationType.eBangHua then
        CGuildQueenMgr.Inst:RequestBangHuaStat(CTabStat)
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.ePGQY then
        CPGQYMgr.Inst:RequestPGQYStat(CTabStat)
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eKoudao then
        CKouDaoMgr.Inst:RequestKouDaoStat(CTabStat)
    elseif  LuaBattleSituationMgr.situationType == EnumSituationType.eTianMenShan then
        CLuaCityWarMgr:RequestTianMenShanStat(curTabStat)
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eGuildTerritorialWarsChallenge then
        Gac2Gas.RequestGTWChallengeFightDetailInfo(curTabStat)
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eHengYuDangKou then
        Gac2Gas.RequestHengYuDangKouStat(curTabStat, LuaHengYuDangKouMgr.m_ShowCurBattle and 0 or 1)
    end
end

function LuaBattleSituationMgr.RequestSituationSceneInfo()
    if LuaBattleSituationMgr.situationType == EnumSituationType.eBangHua then
        CGuildQueenMgr.Inst:RequestBangHuaSceneInfo()
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.ePGQY then
        CPGQYMgr.Inst:RequestPGQYSceneInfo()
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eKoudao then
        CKouDaoMgr.Inst:RequestKouDaoSceneInfo()
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eHengYuDangKou then
        Gac2Gas.RequestHengYuDangKouPlayer()
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eTianMenShan then
        CLuaCityWarMgr:RequestTianMenShanSceneInfo()
    end
end

function LuaBattleSituationMgr.RequestBroadCastSituationStat(curTabStat)
    local CTabStat = LuaBattleSituationMgr.GetEnumStatTypeFromInt(curTabStat)

    if LuaBattleSituationMgr.situationType == EnumSituationType.eBangHua then
        CGuildQueenMgr.Inst:RequestBroadCastBangHuaStat(CTabStat)
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.ePGQY then
        CPGQYMgr.Inst:RequestBroadCastPGQYStat(CTabStat)
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eKoudao then
        CKouDaoMgr.Inst:RequestBroadCastKouDaoStat(CTabStat)
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eTianMenShan then
        CLuaCityWarMgr:ReqeustBroadCastTianMenShanStat(curTabStat)
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eGuildTerritorialWarsChallenge then
        Gac2Gas.RequestBroadcastGTWChallengeFightDetail(curTabStat)
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eHengYuDangKou then
        Gac2Gas.RequestBroadcastHengYuDangKouStat(curTabStat, LuaHengYuDangKouMgr.m_ShowCurBattle and 0 or 1)
    end
end

function LuaBattleSituationMgr.RequestResetSituationStat(curTabStat)
    local CTabStat = LuaBattleSituationMgr.GetEnumStatTypeFromInt(curTabStat)

    if LuaBattleSituationMgr.situationType == EnumSituationType.eBangHua then
        CGuildQueenMgr.Inst:RequestResetBangHuaStat(CTabStat)
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.ePGQY then
        CPGQYMgr.Inst:RequestResetPGQYStat(CTabStat)
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eGuildTerritorialWarsChallenge then
        --TODO
    else
        CKouDaoMgr.Inst:RequestResetKouDaoStat(CTabStat)
    end
end

function LuaBattleSituationMgr.GetNameByTabStat(curTabStat)
    local TabStatNames = {
        [EnumSituationWndTab.Undefined] = LocalString.GetString("查看副本成员"),
        [EnumSituationWndTab.eDps] = LocalString.GetString("伤害排行"),
        [EnumSituationWndTab.eHeal] = LocalString.GetString("治疗排行"),
        [EnumSituationWndTab.eSuffer] = LocalString.GetString("承伤排行"),
        [EnumSituationWndTab.eGuildHero] = LocalString.GetString("英雄救美进度")
    }

    return TabStatNames[curTabStat] or ""
end

function LuaBattleSituationMgr.GetEnumStatTypeFromInt(tabStat)
    local CTabEnumList = {
        [EnumSituationWndTab.Undefined] = EnumStatType.Undefined,
        [EnumSituationWndTab.eDps] = EnumStatType.eDps,
        [EnumSituationWndTab.eHeal] = EnumStatType.eHeal,
        [EnumSituationWndTab.eSuffer] = EnumStatType.eSuffer,
        [EnumSituationWndTab.eGuildHero] = EnumStatType.eGuildHero
    }

    return CTabEnumList[tabStat] or EnumStatType.Undefined
end

function LuaBattleSituationMgr.GetIntFromEnumStatType(CTabStat)
    if CTabStat == EnumStatType.Undefined then
        return EnumSituationWndTab.Undefined
    elseif CTabStat == EnumStatType.eDps then
        return EnumSituationWndTab.eDps
    elseif CTabStat == EnumStatType.eHeal then
        return EnumSituationWndTab.eHeal
    elseif CTabStat == EnumStatType.eSuffer then
        return EnumSituationWndTab.eSuffer
    elseif CTabStat == EnumStatType.eGuildHero then
        return EnumSituationWndTab.eGuildHero
    else
        return EnumSituationWndTab.Undefined
    end
end

-------
---
-------

local CSituationWndMgr = import "L10.UI.CSituationWndMgr"
local CEnumSituationType = import "L10.UI.EnumSituationType"

CSituationWndMgr.m_hookOpenSituationWnd = function(type)
    if type == CEnumSituationType.BangHua then
        LuaBattleSituationMgr.OpenBattleSituationWnd(EnumSituationType.eBangHua)
    elseif type == CEnumSituationType.PGQY then
        LuaBattleSituationMgr.OpenBattleSituationWnd(EnumSituationType.ePGQY)
    elseif type == CEnumSituationType.Koudao then
        LuaBattleSituationMgr.OpenBattleSituationWnd(EnumSituationType.eKoudao)
    end
end
