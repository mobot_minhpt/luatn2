-- Auto Generated!!
local CChatHelper = import "L10.UI.CChatHelper"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CDuoHunMgr = import "L10.Game.CDuoHunMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CQianliyanMgr = import "L10.UI.CQianliyanMgr"
local CQianliyanWnd = import "L10.UI.CQianliyanWnd"
local CTickMgr = import "L10.Engine.CTickMgr"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local DuoHun_Setting = import "L10.Game.DuoHun_Setting"
local EnumEventType = import "EnumEventType"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local MessageMgr = import "L10.Game.MessageMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CQianliyanWnd.m_Init_CS2LuaHook = function (this) 
    this.playerId = 0
    local result = CQianliyanMgr.qianliyanCommonItem.Item.LastQueryResult
    this:InitCenter(result == nil)
    --EventDelegate.Add(enterPlayerId.onChange, this.OnPlayerLabelEnter);
    CommonDefs.ListClear(this.shareList)
    CommonDefs.ListAdd(this.shareList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("分享至队伍"), MakeDelegateFromCSFunction(this.OnShareToTeam, MakeGenericClass(Action1, Int32), this), false, nil))
    CommonDefs.ListAdd(this.shareList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("分享至帮会"), MakeDelegateFromCSFunction(this.OnShareToGuild, MakeGenericClass(Action1, Int32), this), false, nil))
end
CQianliyanWnd.m_InitCenter_CS2LuaHook = function (this, enterId) 
    this.enterPlayerArea:SetActive(enterId)
    this.trackPlayerArea:SetActive(not enterId)
    --playerPosition.SetActive(!enterId);
    --enterPlayerId.gameObject.SetActive(enterId);
    this.isEnterPlayerId = enterId
    local usageTime = 0
    local totalTime = 0
    this.trackPos = CDuoHunMgr.Inst.qianliyanData
    if CQianliyanMgr.qianliyanCommonItem ~= nil and CQianliyanMgr.qianliyanCommonItem.IsItem then
        this.trackPos = CQianliyanMgr.qianliyanCommonItem.Item.LastQueryResult
        usageTime = CQianliyanMgr.qianliyanCommonItem.Item.RemainTimes
        totalTime = CQianliyanMgr.qianliyanTemplateItem.ActionTimes
    elseif this.trackPos ~= nil then
        local qianliyan = Item_Item.GetData(DuoHun_Setting.GetData().QianLiYanItemId)
        usageTime = 0
        totalTime = qianliyan.ActionTimes
    else
        this:Close()
    end
    this.usageProgressLabel.text = System.String.Format(LocalString.GetString("千里眼已使用{0}/{1}"), totalTime - usageTime, totalTime)
    if usageTime <= 0 then
        this.refreshButtonLabel.text = LocalString.GetString("续费")
    else
        this.refreshButtonLabel.text = LocalString.GetString("刷新")
    end
    if enterId then
        this:InitEnterPlayerId()
    else
        this:InitMapPos()
    end
end
CQianliyanWnd.m_InitEnterPlayerId_CS2LuaHook = function (this) 
    this.playerId = 0
    --tips.SetActive(true);
    this.playerIdLabel.text = ""
    this.titleLabel.text = LocalString.GetString("追踪玩家")
    this.titleTable:Reposition()
end
CQianliyanWnd.m_InitMapPos_CS2LuaHook = function (this) 
    if this.trackPos == nil then
        return
    end
    this.playerId = math.floor(this.trackPos.targetId)
    if this.trackPos.sceneIndex > 0 then
        this.positionLabel.text = CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("{0}    {1}.{2}    分线{3}"), {this.trackPos.sceneName, this.trackPos.posX, this.trackPos.posY, this.trackPos.sceneIndex})
    else
        this.positionLabel.text = System.String.Format(LocalString.GetString("{0}    {1}.{2}"), this.trackPos.sceneName, this.trackPos.posX, this.trackPos.posY)
    end
    this.titleLabel.text = this.trackPos.targetName
    this.titleTable:Reposition()
end
CQianliyanWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.enterOKButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.enterOKButton).onClick, MakeDelegateFromCSFunction(this.OnEnterOkButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.enterCancelButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.enterCancelButton).onClick, MakeDelegateFromCSFunction(this.OnEnterCancelButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.infoOKButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.infoOKButton).onClick, MakeDelegateFromCSFunction(this.OnInfoOKButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.infoCancelButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.infoCancelButton).onClick, MakeDelegateFromCSFunction(this.OnInfoCancelButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.trackButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.trackButton).onClick, MakeDelegateFromCSFunction(this.OnTrackButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.refreshPositionButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.refreshPositionButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnRefreshButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.shareButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.shareButton).onClick, MakeDelegateFromCSFunction(this.OnShareButtonClick, VoidDelegate, this), true)
    EventManager.AddListener(EnumEventType.OnUseQianliyanReturn, MakeDelegateFromCSFunction(this.OnUseQianliyanReturn, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.PasteFromClipBoard, MakeDelegateFromCSFunction(this.OnPlayerIdChanged, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.QianLiYanTrackCheckSceneIndexRet, MakeDelegateFromCSFunction(this.OnQianLiYanTrackCheckSceneIndexRet, MakeGenericClass(Action2, String, Int32), this))
end
CQianliyanWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.enterOKButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.enterOKButton).onClick, MakeDelegateFromCSFunction(this.OnEnterOkButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.enterCancelButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.enterCancelButton).onClick, MakeDelegateFromCSFunction(this.OnEnterCancelButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.infoOKButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.infoOKButton).onClick, MakeDelegateFromCSFunction(this.OnInfoOKButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.infoCancelButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.infoCancelButton).onClick, MakeDelegateFromCSFunction(this.OnInfoCancelButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.trackButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.trackButton).onClick, MakeDelegateFromCSFunction(this.OnTrackButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.refreshPositionButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.refreshPositionButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnRefreshButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.shareButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.shareButton).onClick, MakeDelegateFromCSFunction(this.OnShareButtonClick, VoidDelegate, this), false)
    EventManager.RemoveListener(EnumEventType.OnUseQianliyanReturn, MakeDelegateFromCSFunction(this.OnUseQianliyanReturn, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.PasteFromClipBoard, MakeDelegateFromCSFunction(this.OnPlayerIdChanged, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.QianLiYanTrackCheckSceneIndexRet, MakeDelegateFromCSFunction(this.OnQianLiYanTrackCheckSceneIndexRet, MakeGenericClass(Action2, String, Int32), this))

    this:DoCancel()
    CDuoHunMgr.Inst.qianliyanData = nil
end
CQianliyanWnd.m_OnShareToTeam_CS2LuaHook = function (this, index) 
    --if (CQianliyanMgr.qianliyanCommonItem != null)
    --    Gac2Gas.ShareQianLiYanPlayerPos(CQianliyanMgr.qianliyanCommonItem.Id, 1);
    --else
    --    DoShare(EChatPanel.Team);

    if not System.String.IsNullOrEmpty(CQianliyanMgr.qianliyanId) then
        Gac2Gas.ShareQianLiYanPlayerPos(CQianliyanMgr.qianliyanId, 1)
    end
end
CQianliyanWnd.m_OnShareToGuild_CS2LuaHook = function (this, index) 

    --if (CQianliyanMgr.qianliyanCommonItem != null)
    --    Gac2Gas.ShareQianLiYanPlayerPos(CQianliyanMgr.qianliyanCommonItem.Id, 2);
    --else
    --    DoShare(EChatPanel.Guild);
    if not System.String.IsNullOrEmpty(CQianliyanMgr.qianliyanId) then
        Gac2Gas.ShareQianLiYanPlayerPos(CQianliyanMgr.qianliyanId, 2)
    end
end
CQianliyanWnd.m_DoShare_CS2LuaHook = function (this, panel) 
    local data = CDuoHunMgr.Inst.qianliyanData
    if data == nil or CClientMainPlayer.Inst == nil then
        return
    end
    local msg = ""
    local formatStr = MessageMgr.Inst:GetMessageFormat("QIANLIYAN_SHARE", false)
    if data.sceneIndex <= 0 then
        msg = CommonDefs.String_Format_String_ArrayObject(formatStr, {
            tostring(CClientMainPlayer.Inst.Id), 
            CClientMainPlayer.Inst.Name, 
            tostring(data.targetId), 
            data.targetName, 
            data.sceneTemplateId, 
            data.sceneName, 
            data.posX, 
            data.posY, 
            data.sceneId
        })
    else
        formatStr = MessageMgr.Inst:GetMessageFormat("QIANLIYAN_SHARE_FENXIAN", false)
        msg = CommonDefs.String_Format_String_ArrayObject(formatStr, {
            tostring(CClientMainPlayer.Inst.Id), 
            CClientMainPlayer.Inst.Name, 
            tostring(data.targetId), 
            data.targetName, 
            data.sceneTemplateId, 
            data.sceneName, 
            data.posX, 
            data.posY, 
            data.sceneId, 
            data.sceneIndex
        })
    end
    CChatHelper.SendMsgWithFilterOption(panel, msg, 0, true)
end
CQianliyanWnd.m_OnEnterOkButtonClick_CS2LuaHook = function (this, go) 
    if not System.String.IsNullOrEmpty(this.enterPlayerId.value) then
        this:OnRefreshButtonClick(go)
    else
        g_MessageMgr:ShowMessage("INPUT_ZHUIZONG_ID")
    end
end
CQianliyanWnd.m_OnRefreshButtonClick_CS2LuaHook = function (this, go) 

    local searchId = 0
    local default
    default, searchId = System.UInt64.TryParse(this.enterPlayerId.value)
    local isId = default or this.playerId > 0
    if searchId == 0 then
        searchId = this.playerId
    end
    local targetName = ""
    if not isId then
        targetName = this.enterPlayerId.value
    end

    if CQianliyanMgr.qianliyanCommonItem ~= nil then
        Gac2Gas.UseQianLiYan(EnumToInt(CQianliyanMgr.qianliyanItem.place), CQianliyanMgr.qianliyanItem.pos, searchId, false, not isId, targetName)
    else
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("QIANLIYAN_XUFEI"), DelegateFactory.Action(function () 
            Gac2Gas.UseQianLiYan(EnumToInt(CQianliyanMgr.qianliyanItem.place), CQianliyanMgr.qianliyanItem.pos, searchId, true, not isId, targetName)
        end), nil, nil, nil, false)
    end

    this.refreshPositionButton.Enabled = false
    this:DoCancel()
    this.cancel = CTickMgr.Register(MakeDelegateFromCSFunction(this.OnRefreshButtonEnable, Action0, this), DuoHun_Setting.GetData().RefreshButton_CD, ETickType.Once)
end
CQianliyanWnd.m_OnTrackButtonClick_CS2LuaHook = function (this, go) 
    if this.trackPos ~= nil and not CTrackMgr.Inst.isTracking then
        --先去服务器校验一下当前分线是否存在
        --if (CQianliyanMgr.qianliyanCommonItem != null && CQianliyanMgr.qianliyanCommonItem.IsItem)
        --{
        --   Gac2Gas.QianLiYanTrackCheckDest(CQianliyanMgr.qianliyanCommonItem.Item.Id);
        --}
        --CTrackMgr.Inst.Track(trackPos.sceneId, trackPos.sceneTemplateId, new CPos(trackPos.posX * 64, trackPos.posY * 64), 0, null, null, null);

        if not System.String.IsNullOrEmpty(CQianliyanMgr.qianliyanId) then
            Gac2Gas.QianLiYanTrackCheckDest(CQianliyanMgr.qianliyanId)
        end
    end
end
CQianliyanWnd.m_OnQianLiYanTrackCheckSceneIndexRet_CS2LuaHook = function (this, sceneId, result) 
    if this.trackPos ~= nil and not CTrackMgr.Inst.isTracking then
        if result == 1 then
            CTrackMgr.Inst:FindLocation(sceneId, this.trackPos.sceneTemplateId, this.trackPos.posX, this.trackPos.posY, nil, nil, nil, 0)
            --TODO 是不是要关闭窗口
        end
    end
end
CQianliyanMgr.m_OpenQianliyanWnd_CS2LuaHook = function (item) 
    CQianliyanMgr.qianliyanItem = item
    CQianliyanMgr.qianliyanCommonItem = CItemMgr.Inst:GetById(item.itemId)
    CQianliyanMgr.qianliyanTemplateItem = Item_Item.GetData(item.templateId)
    CQianliyanMgr.qianliyanId = item.itemId
    CUIManager.ShowUI(CUIResources.QianliyanWnd)
end
