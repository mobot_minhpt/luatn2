CLuaCrossDouDiZhuResultWnd = class()

CLuaCrossDouDiZhuResultWnd.m_IsWashOut = false
CLuaCrossDouDiZhuResultWnd.m_Rank = 0
CLuaCrossDouDiZhuResultWnd.m_Score = 0
function CLuaCrossDouDiZhuResultWnd:Init()

    local button = self.transform:Find("Button").gameObject
    UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function(go)
        CLuaCrossDouDiZhuRankWnd.m_IsAllRank = false
        CUIManager.ShowUI(CLuaUIResources.CrossDouDiZhuRankWnd)
    end)

    local scoreLabel = self.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    local rankLabel = self.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    scoreLabel.text=tostring(CLuaCrossDouDiZhuResultWnd.m_Score)
    rankLabel.text=tostring(CLuaCrossDouDiZhuResultWnd.m_Rank)

    local gongxijinji = self.transform:Find("gongxijinji").gameObject
    local yihantaotai = self.transform:Find("yihantaotai").gameObject
    if CLuaCrossDouDiZhuResultWnd.m_IsWashOut then
        gongxijinji:SetActive(false)
        yihantaotai:SetActive(true)
    else
        gongxijinji:SetActive(true)
        yihantaotai:SetActive(false)
    end
end
