local MessageWndManager = import "L10.UI.MessageWndManager"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CTeamMgr=import "L10.Game.CTeamMgr"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local Physics = import "UnityEngine.Physics"

LuaHaiZhanChangePosWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHaiZhanChangePosWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaHaiZhanChangePosWnd, "Grid", "Grid", GameObject)

--@endregion RegistChildComponent end

function LuaHaiZhanChangePosWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaHaiZhanChangePosWnd:Init()
    Extensions.RemoveAllChildren(self.Grid.transform)
    for i = 1, 5 do
        local go = NGUITools.AddChild(self.Grid,self.ItemTemplate)
        go:SetActive(true)
        self:InitItem(go.transform,i)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
            self:OnClickItem(g)
        end)
    end
    self.Grid:GetComponent(typeof(UIGrid)):Reposition()
    self.ItemTemplate:SetActive(false)

end

--@region UIEvent

--@endregion UIEvent

function LuaHaiZhanChangePosWnd:InitItem(tf,pos)
    local posLabel = tf:Find("PosLabel"):GetComponent(typeof(UILabel))
    local icon = tf:Find("Icon"):GetComponent(typeof(CUITexture))
    local role = LuaHaiZhanMgr.Pos2Role(pos)
    if role == 1 then
        posLabel.text = LocalString.GetString("舵手")
        icon:LoadMaterial("UI/Texture/Transparent/Material/haizhan_chuanzhang.mat", false)
    elseif role==2 then
        posLabel.text = LocalString.GetString("炮手")
        icon:LoadMaterial("UI/Texture/Transparent/Material/haizhan_paoshou.mat", false)
    elseif role==3 then
        posLabel.text = LocalString.GetString("水手")
        icon:LoadMaterial("UI/Texture/Transparent/Material/haizhan_chuanyuan.mat", false)
    end

    local portrait = tf:Find("Portrait"):GetComponent(typeof(CUITexture))
    local readyLabel = tf:Find("ReadyLabel"):GetComponent(typeof(UILabel))
    local stateLabel = tf:Find("StateLabel"):GetComponent(typeof(UILabel))
    stateLabel.text = nil

    local info = LuaHaiZhanMgr.s_SeatInfo and LuaHaiZhanMgr.s_SeatInfo[pos] or nil
    if info and info.status>0 then
        portrait:LoadPortrait(CUICommonDef.GetPortraitName(info.class, info.gender, -1), false)
        readyLabel.text = info.name
        readyLabel.alpha=1
        icon.gameObject:SetActive(false)
    else
        readyLabel.text = LocalString.GetString("待就位")
        readyLabel.alpha=0.5
        portrait:Clear()
        icon.gameObject:SetActive(true)
    end
end
function LuaHaiZhanChangePosWnd:OnClickItem(go)
    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id

    for i = 1, self.Grid.transform.childCount do
        local info = LuaHaiZhanMgr.s_SeatInfo and LuaHaiZhanMgr.s_SeatInfo[i] or nil
        if info and info.id==myId then
            --do nothing
        else
            local tf = self.Grid.transform:GetChild(i-1)
            if tf.gameObject.name == "Guide_Temp" then tf = tf:GetChild(0) end
            if tf.gameObject==go then
                local info = LuaHaiZhanMgr.s_SeatInfo and LuaHaiZhanMgr.s_SeatInfo[i] or nil
                if info then
                    local message = g_MessageMgr:FormatMessage("NAVALWAR_CHANGE_SEAT_CONIFRM")
                    MessageWndManager.ShowConfirmMessage(message, 10, true, DelegateFactory.Action(function ()
                        Gac2Gas.RequestChangeNavalWarShipSeat(i)
                    end), nil)
                else
                    Gac2Gas.RequestChangeNavalWarShipSeat(i)
                end
                break
            end
        end
    end
end
function LuaHaiZhanChangePosWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncNavalWarSeatInfo", self, "OnSyncNavalWarSeatInfo")
end
function LuaHaiZhanChangePosWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncNavalWarSeatInfo", self, "OnSyncNavalWarSeatInfo")
end
function LuaHaiZhanChangePosWnd:OnSyncNavalWarSeatInfo(seatInfo)
    CUIManager.CloseUI(CLuaUIResources.HaiZhanChangePosWnd)
end

function LuaHaiZhanChangePosWnd:GetGuideGo(methodName)
    if methodName == "GetHelmsmanButton" then
        return self.Grid.transform.childCount > 0 and self.Grid.transform:GetChild(0).gameObject
    elseif methodName == "GetGunnerButton" then
        return self.Grid.transform.childCount > 1 and self.Grid.transform:GetChild(1).gameObject
    end
    return nil
end