local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"

LuaMengQuanHengXingResultWnd = class()
LuaMengQuanHengXingResultWnd.S_RewardsData = nil
LuaMengQuanHengXingResultWnd.S_RankTbl = nil

function LuaMengQuanHengXingResultWnd:Awake()
    self:InitUIComponent()
    self:InitUIEvent()
    self:InitUIData()
end

function LuaMengQuanHengXingResultWnd:Init()

end

function LuaMengQuanHengXingResultWnd:InitUIComponent()
    self.topNode1 = self.transform:Find("Anchor/InfoView/RankNode/TopNode1")
    self.topNode2 = self.transform:Find("Anchor/InfoView/RankNode/TopNode2")
    self.topNode3 = self.transform:Find("Anchor/InfoView/RankNode/TopNode3")
    self.topNode4 = self.transform:Find("Anchor/InfoView/RankNode/TopNode4")
    self.rewardItem1 = self.transform:Find("Anchor/InfoView/RewardNode/Item1")
    self.rewardItem2 = self.transform:Find("Anchor/InfoView/RewardNode/Item2")
    self.noRewardReminder = self.transform:Find("Anchor/InfoView/RewardNode/NoRewardReminder")
    self.rankLabel = self.transform:Find("Anchor/Title/RankLabel"):GetComponent(typeof(UILabel))
    self.shareButton = self.transform:Find("Anchor/InfoView/Bottom/ShareButton")
    self.fightAgainButton = self.transform:Find("Anchor/InfoView/Bottom/FightAgainButton")
    self.closeButton = self.transform:Find("WndBg/CloseButton").gameObject
end

function LuaMengQuanHengXingResultWnd:InitUIEvent()
    UIEventListener.Get(self.fightAgainButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        CUIManager.CloseUI(CLuaUIResources.MengQuanHengXingResultWnd)
        CUIManager.ShowUI(CLuaUIResources.MengQuanHengXingSignUpWnd)
    end)

    UIEventListener.Get(self.shareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        self.fightAgainButton.gameObject:SetActive(false)
        self.shareButton.gameObject:SetActive(false)
        self.closeButton:SetActive(false)
        CUICommonDef.CaptureScreen("screenshot", true, false, nil, DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            self.fightAgainButton.gameObject:SetActive(true)
            self.shareButton.gameObject:SetActive(true)
            self.closeButton:SetActive(true)
        end))
    end)
end

function LuaMengQuanHengXingResultWnd:InitUIData()
    self.mainPlayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local unpackRankData = g_MessagePack.unpack(LuaMengQuanHengXingResultWnd.S_RankTbl)
    for i = 1, #unpackRankData do
        self:RefreshTopNode(i, unpackRankData[i])
    end
    for i = #unpackRankData+1, 4 do
        self:RefreshTopNode(i, nil)
    end
    
    local unpackRewardData = g_MessagePack.unpack(LuaMengQuanHengXingResultWnd.S_RewardsData)
    self:InitRewardType(#unpackRewardData)
    local rewardItemList = {self.rewardItem1.gameObject, self.rewardItem2.gameObject}
    for i = 1, #unpackRewardData do
        self:InitOneItem(rewardItemList[i], unpackRewardData[i])
    end
end

function LuaMengQuanHengXingResultWnd:RefreshTopNode(rank, info)
    local node = self["topNode" .. rank]
    if info then
        local nameLabel = node.transform:Find("PlayerName"):GetComponent(typeof(UILabel))
        local scoreLabel = node.transform:Find("Score"):GetComponent(typeof(UILabel))
        local serverNameLabel = node.transform:Find("ServerName"):GetComponent(typeof(UILabel))

        local isSelf = info.PlayerId == self.mainPlayerId
        nameLabel.text = info.Name
        nameLabel.color = isSelf and NGUIText.ParseColor("4cad4c", 0) or NGUIText.ParseColor("343434", 0)
        scoreLabel.text = info.Score
        scoreLabel.color = isSelf and NGUIText.ParseColor("4cad4c", 0) or NGUIText.ParseColor("343434", 0)
        serverNameLabel.text = info.ServerName
        serverNameLabel.color = isSelf and NGUIText.ParseColor("4cad4c", 0) or NGUIText.ParseColor("343434", 0)

        node.gameObject:SetActive(true)
        if isSelf then
            self.rankLabel.text = rank
        end
    else
        node.gameObject:SetActive(false)
    end
end

function LuaMengQuanHengXingResultWnd:InitRewardType(rewardsNumber)
    self.noRewardReminder.gameObject:SetActive(rewardsNumber == 0)
    self.rewardItem1.gameObject:SetActive(rewardsNumber >= 1)
    self.rewardItem2.gameObject:SetActive(rewardsNumber >= 2)
end

function LuaMengQuanHengXingResultWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end
    UIEventListener.Get(curItem).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
end