local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local CFirstChargeGiftCell = import "L10.UI.CFirstChargeGiftCell"

LuaSanxingEndWnd = class()
RegistChildComponent(LuaSanxingEndWnd,"closeBtn", GameObject)
RegistChildComponent(LuaSanxingEndWnd,"scoreLabel", UILabel)
RegistChildComponent(LuaSanxingEndWnd,"scoreBtn", GameObject)
RegistChildComponent(LuaSanxingEndWnd,"skillIcon", CUITexture)
RegistChildComponent(LuaSanxingEndWnd,"skillName", UILabel)
RegistChildComponent(LuaSanxingEndWnd,"gift", CFirstChargeGiftCell)
RegistChildComponent(LuaSanxingEndWnd,"noGiftLabel", UILabel)

RegistClassMember(LuaSanxingEndWnd, "maxChooseNum")

function LuaSanxingEndWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.SanxingEndWnd)
end

function LuaSanxingEndWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaSanxingEndWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaSanxingEndWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	local onScoreBtnClick = function(go)
		CLuaNPCShopInfoMgr.ShowScoreShopById(34000060)
	end
	CommonDefs.AddOnClickListener(self.scoreBtn,DelegateFactory.Action_GameObject(onScoreBtnClick),false)

	self.scoreLabel.text = '+' .. LuaSanxingGamePlayMgr.EndScore

  local skillId = LuaSanxingGamePlayMgr.EndSkillId
  local skill = Skill_AllSkills.GetData(skillId)
	if skill then
		self.skillIcon:LoadSkillIcon(skill.SkillIcon)
		self.skillName.text = skill.Name
	end

	if LuaSanxingGamePlayMgr.EndItemId > 0 then
		self.gift.gameObject:SetActive(true)
		self.noGiftLabel.gameObject:SetActive(false)
		self.gift:Init(LuaSanxingGamePlayMgr.EndItemId,1)
	else
		self.gift.gameObject:SetActive(false)
		self.noGiftLabel.gameObject:SetActive(true)
		self.noGiftLabel.text = SafeStringFormat3(LocalString.GetString('本方排名前%s的玩家可获得'),LuaSanxingGamePlayMgr.EndRankNum)
	end

end

return LuaSanxingEndWnd
