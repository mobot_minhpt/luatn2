-- Auto Generated!!
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIDataSheetRow = import "L10.UI.CUIDataSheetRow"
local UILabel = import "UILabel"
local Vector3 = import "UnityEngine.Vector3"
CUIDataSheetRow.m_Init_CS2LuaHook = function (this, rowTextList, rowWidthList, horizontalPadding, index) 
    this.bgBtn:SetBackgroundSprite(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

    local n = this.rowsRoot.transform.childCount
    do
        local i = 0
        while i < n do
            this.rowsRoot.transform:GetChild(i).gameObject:SetActive(false)
            i = i + 1
        end
    end

    local t = rowTextList.Count
    local totalWidth = horizontalPadding * (t - 1)
    do
        local i = 0
        while i < t do
            totalWidth = totalWidth + rowWidthList[i]
            i = i + 1
        end
    end

    local xMin = math.floor(- totalWidth / 2)
    local sum = 0

    do
        local i = 0
        while i < t do
            local row = nil
            if i < n then
                row = this.rowsRoot.transform:GetChild(i).gameObject
            else
                row = CUICommonDef.AddChild(this.rowsRoot, this.labelTemplate)
            end
            row:SetActive(true)
            local label = CommonDefs.GetComponent_GameObject_Type(row, typeof(UILabel))
            label.text = rowTextList[i]
            label.width = rowWidthList[i]
            local offsetX = xMin + sum + label.width / 2
            if i < t - 1 then
                sum = sum + horizontalPadding
            end
            label.transform.localPosition = CreateFromClass(Vector3, offsetX, 0, 0)
            sum = sum + label.width
            i = i + 1
        end
    end

    this.background.width = sum
end
