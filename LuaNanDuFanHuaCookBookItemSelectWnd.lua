local QnTableView = import "L10.UI.QnTableView"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"

LuaNanDuFanHuaCookBookItemSelectWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNanDuFanHuaCookBookItemSelectWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaNanDuFanHuaCookBookItemSelectWnd, "QnTableView", "QnTableView", QnTableView)
RegistChildComponent(LuaNanDuFanHuaCookBookItemSelectWnd, "ItemsScrollView", "ItemsScrollView", CUIRestrictScrollView)

--@endregion RegistChildComponent end
RegistClassMember(LuaNanDuFanHuaCookBookItemSelectWnd,"m_List")

function LuaNanDuFanHuaCookBookItemSelectWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaNanDuFanHuaCookBookItemSelectWnd:Init()
    self.prefixPath = ""
    self.TitleLabel.text = LocalString.GetString("选择烹饪技法")
    self:RefreshCookSkills()
end

function LuaNanDuFanHuaCookBookItemSelectWnd:OnEnable()
end

function LuaNanDuFanHuaCookBookItemSelectWnd:OnDisable()
end

function LuaNanDuFanHuaCookBookItemSelectWnd:RefreshCookSkills()
    local list = {}
    LiYuZhangDaiFood_CookSkills.ForeachKey(function (key)
        table.insert(list, key)
    end)
    self.m_List = list
    self.QnTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self.m_List and #self.m_List or 0
        end,
        function(item, index)
            self:ItemAt(item,index)
        end)
    self.QnTableView:ReloadData(true, true)
    self.ItemsScrollView:ResetPosition()
    self.QnTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
end
--@region UIEvent

--@endregion UIEvent
function LuaNanDuFanHuaCookBookItemSelectWnd:ItemAt(item,index)
    local iconTexture = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local data = self.m_List[index + 1]
    local cfgData = LiYuZhangDaiFood_CookSkills.GetData(data)
    iconTexture:LoadMaterial(self.prefixPath .. cfgData.Material)
    nameLabel.text = cfgData.Name
end

function LuaNanDuFanHuaCookBookItemSelectWnd:OnSelectAtRow(row)
    local data = self.m_List[row + 1]
    g_ScriptEvent:BroadcastInLua("OnNanDuFanHuaCookSelectCookSkillsId", data)
    CUIManager.CloseUI(CLuaUIResources.NanDuFanHuaCookBookItemSelectWnd)
end
