require("3rdParty/ScriptEvent")
require("common/common_include")
local LuaGameObject=import "LuaGameObject"

local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local AlignType2=import "L10.UI.CPopupMenuInfoMgr+AlignType"

local CClientMainPlayer=import "L10.Game.CClientMainPlayer"

local LingShouBaby_Setting=import "L10.Game.LingShouBaby_Setting"
local CWordFilterMgr=import "L10.Game.CWordFilterMgr"

local Gac2Gas=import "L10.Game.Gac2Gas"

local MessageMgr=import "L10.Game.MessageMgr"

local MessageWndManager = import "L10.UI.MessageWndManager"

CLuaLingShouMarriageMatchInputWnd=class()
RegistClassMember(CLuaLingShouMarriageMatchInputWnd,"m_SendButton")
RegistClassMember(CLuaLingShouMarriageMatchInputWnd,"m_CostLabel")
RegistClassMember(CLuaLingShouMarriageMatchInputWnd,"m_ShouMingLabel")
RegistClassMember(CLuaLingShouMarriageMatchInputWnd,"m_Cost")
RegistClassMember(CLuaLingShouMarriageMatchInputWnd,"m_LingShouTemplateId")
RegistClassMember(CLuaLingShouMarriageMatchInputWnd,"m_LingShouName")
RegistClassMember(CLuaLingShouMarriageMatchInputWnd,"m_Input")
RegistClassMember(CLuaLingShouMarriageMatchInputWnd,"m_LingShouId")

function CLuaLingShouMarriageMatchInputWnd:Init()
    self.m_Input=LuaGameObject.GetChildNoGC(self.transform,"Input").input

    self.m_SendButton=LuaGameObject.GetChildNoGC(self.transform,"SendButton").gameObject

    local function onClickSendButton(go)
        self:OnClickSendButton(go)
    end
    CommonDefs.AddOnClickListener(self.m_SendButton,DelegateFactory.Action_GameObject(onClickSendButton),false)



    self.m_Cost=LingShouBaby_Setting.GetData().BaZiLifespanCost

    self.m_ShouMingLabel=LuaGameObject.GetChildNoGC(self.transform,"ShouMingLabel").label
    self.m_CostLabel=LuaGameObject.GetChildNoGC(self.transform,"CostLabel").label
    self.m_CostLabel.text=tostring(self.m_Cost)
    local details=CLuaLingShouMgr.m_TempLingShouInfo
    if details~=nil then
        self.m_LingShouTemplateId=details.data.TemplateId
        self.m_LingShouName=details.data.Name
        self.m_LingShouId=details.id
        self.m_ShouMingLabel.text=tostring(details.data.Props.Shouming)
        if details.data.Props.Shouming<self.m_Cost then
            CUICommonDef.SetActive(self.m_SendButton,false,true)
        else
            --可以
        end
    end
end

function CLuaLingShouMarriageMatchInputWnd:OnClickSendButton(go)
    local function SelectAction(index)
        local id=CClientMainPlayer.Inst.Id
        
        local content=self.m_Input.value
        -- print(content)
        local len=CUICommonDef.GetStrByteLength(content)
        if len>40 then
            MessageMgr.Inst:ShowMessage("LingShou_ZhaoQin_Content_ToMuch",{})
            return
        end
        if not CWordFilterMgr.Inst:CheckLinkAndMatch(content) then
            MessageMgr.Inst:ShowMessage("LingShou_ZhaoQin_Content_Illegal",{})
            return
        end

        local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(content,true)
        content = ret.msg
        local channelId=0
        if index==0 then
            --世界频道
            channelId=2
        elseif index==1 then
            --帮会频道
            channelId=1
        elseif index==2 then
            --队伍频道
            channelId=3
        end
        
        MessageWndManager.ShowOKCancelMessage(
			MessageMgr.Inst:FormatMessage("CUSTOM_STRING2", {LocalString.GetString("再次征婚，以往的提亲消息将失效，你确定要为灵兽征婚么？")}), 
            DelegateFactory.Action(function()
                Gac2Gas.RequestBroadcastBaZiInvitation(self.m_LingShouId,channelId,content or "")
            end), nil,nil,nil,false)
            
    end
    local selectShareItem=DelegateFactory.Action_int(SelectAction)

    local t={}
    local item=PopupMenuItemData(LocalString.GetString("至世界频道"),selectShareItem,false,nil)
    table.insert(t, item)
    local item=PopupMenuItemData(LocalString.GetString("至帮会频道"),selectShareItem,false,nil)
    table.insert(t, item)
    local item=PopupMenuItemData(LocalString.GetString("至队伍频道"),selectShareItem,false,nil)
    table.insert(t, item)
    -- end
    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType2.Top,1,nil,600,true,266)
end

function CLuaLingShouMarriageMatchInputWnd:OnEnable()
    g_ScriptEvent:AddListener("GetLingShouDetails", self, "OnGetLingShouDetails")
    
end
function CLuaLingShouMarriageMatchInputWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GetLingShouDetails", self, "OnGetLingShouDetails")
end
function CLuaLingShouMarriageMatchInputWnd:OnGetLingShouDetails(args)
    local lingShouId=args[0]
    local data=args[1]
    if lingShouId==self.m_LingShouId then
        self:UpdateShouMing(data.data.Props.Shouming)
    end
end
function CLuaLingShouMarriageMatchInputWnd:UpdateShouMing(shouming)
    if shouming>=self.m_Cost then
        self.m_ShouMingLabel.text=tostring(shouming)
    else
        self.m_ShouMingLabel.text=SafeStringFormat3("[c][ff0000]%d[-][/c]",shouming)
    end
end
return CLuaLingShouMarriageMatchInputWnd
