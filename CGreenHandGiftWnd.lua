-- Auto Generated!!
local CGreenHandGiftWnd = import "L10.UI.CGreenHandGiftWnd"
local CWelfareBonusMgr = import "L10.Game.CWelfareBonusMgr"
local Item_Item = import "L10.Game.Item_Item"
local KaiFuActivity_LoginDays = import "L10.Game.KaiFuActivity_LoginDays"
local KaiFuActivity_OnlineTime = import "L10.Game.KaiFuActivity_OnlineTime"
local KaiFuActivity_Setting = import "L10.Game.KaiFuActivity_Setting"
local LocalString = import "LocalString"
local UISprite = import "UISprite"
CGreenHandGiftWnd.m_Init_CS2LuaHook = function (this) 
    this:InitNextDay()
    this:InitLoginDays()
    this:InitOnlineTime()
    this:InitAward()
end
CGreenHandGiftWnd.m_InitAward_CS2LuaHook = function (this) 
    local secondDay = KaiFuActivity_Setting.GetData("SecondDayGift")
    for i = 0, 2 do
        local item = Item_Item.GetData(secondDay.Value[i])
        this.awardIcons[i]:Init(item, 0)
    end

    local login = KaiFuActivity_Setting.GetData("SevenDayGift")
    for i = 3, 5 do
        local item = Item_Item.GetData(login.Value[i - 3])
        this.awardIcons[i]:Init(item, 0)
    end

    local online = KaiFuActivity_Setting.GetData("OnlineGift")
    for i = 6, 8 do
        local item = Item_Item.GetData(online.Value[i - 6])
        this.awardIcons[i]:Init(item, 0)
    end
end
CGreenHandGiftWnd.m_InitNextDay_CS2LuaHook = function (this) 
    CommonDefs.GetComponent_Component_Type(this.nextDayButton, typeof(UISprite)).spriteName = g_sprites.GreenHandGiftWnd_OrangeButtonName
    if CWelfareBonusMgr.Inst.LoginAwardId == 2 and CWelfareBonusMgr.Inst.LoginDays >= 2 then
        this.nextDayButton.Enabled = true
        this.nextDayLabel.text = LocalString.GetString("领    取")
    elseif CWelfareBonusMgr.Inst.LoginAwardId < 2 then
        this.nextDayButton.Enabled = false
        this.nextDayLabel.text = LocalString.GetString("领    取")
    else
        this.nextDayButton.Enabled = false
        this.nextDayLabel.text = LocalString.GetString("已领取")
    end
end
CGreenHandGiftWnd.m_InitLoginDays_CS2LuaHook = function (this) 
    CommonDefs.GetComponent_Component_Type(this.loginDaysButton, typeof(UISprite)).spriteName = g_sprites.GreenHandGiftWnd_OrangeButtonName
    if KaiFuActivity_LoginDays.Exists(CWelfareBonusMgr.Inst.LoginAwardId) and CWelfareBonusMgr.Inst.LoginDays >= CWelfareBonusMgr.Inst.LoginAwardId then
        this.loginDaysButton.Enabled = true
        local default
        if CWelfareBonusMgr.Inst.LoginAwardId == 7 then
            default = LocalString.GetString("领    取")
        else
            default = LocalString.GetString("查    看")
        end
        this.loginDaysLabel.text = default
    elseif KaiFuActivity_LoginDays.Exists(CWelfareBonusMgr.Inst.LoginAwardId) and CWelfareBonusMgr.Inst.LoginDays < CWelfareBonusMgr.Inst.LoginAwardId then
        this.loginDaysButton.Enabled = false
        this.loginDaysLabel.text = LocalString.GetString("领    取")
    else
        this.loginDaysButton.Enabled = false
        this.loginDaysLabel.text = LocalString.GetString("已领取")
    end
end
CGreenHandGiftWnd.m_InitOnlineTime_CS2LuaHook = function (this) 
    this.onlineTimeTitleLabel.text = this.title
    CommonDefs.GetComponent_Component_Type(this.onlineTimeButton, typeof(UISprite)).spriteName = g_sprites.GreenHandGiftWnd_OrangeButtonName
    local time = KaiFuActivity_OnlineTime.GetData(CWelfareBonusMgr.Inst.OnlineAwardId)
    if time ~= nil and CWelfareBonusMgr.Inst.OnlineAwardLeftTime <= 0 then
        --onlineTimeTitleLabel.text = string.Format("连续在线{0}分钟", time.NeedTime / 60);
        this.onlineTimeButton.Enabled = true
        this.onlineTimeLabel.text = LocalString.GetString("领    取")
        this.progress.value = 1
    elseif time ~= nil then
        --onlineTimeTitleLabel.text = string.Format("连续在线{0}分钟", time.NeedTime / 60);
        this.onlineTimeButton.Enabled = true
        CommonDefs.GetComponent_Component_Type(this.onlineTimeButton, typeof(UISprite)).spriteName = g_sprites.GreenHandGiftWnd_BlueButtonName
        this.onlineTimeLabel.text = LocalString.GetString("查看更多")
        this.progress.value = 1 - CWelfareBonusMgr.Inst.OnlineAwardLeftTime / time.NeedTime
    else
        --onlineTimeTitleLabel.text = "连续在线奖励";
        this.onlineTimeButton.Enabled = false
        this.onlineTimeLabel.text = LocalString.GetString("已领取")
        this.progress.value = 1
    end
end
