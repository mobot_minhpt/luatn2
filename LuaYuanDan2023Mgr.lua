local CScene = import "L10.Game.CScene"

-- 心愿状态
EnumXinYuanState = {
    Unlock = 1,
    Receive = 2,
    Complete = 3,
}
LuaYuanDan2023Mgr = {}
LuaYuanDan2023Mgr.YuanDanQiYuanResultInfo = {}
------------ 心愿必达 -------
-- 更新某个心愿状态
function LuaYuanDan2023Mgr.UpdateXinYuanState(Id,state)
    g_ScriptEvent:BroadcastInLua("UpdateYuanDan2023OneXinYuanState",Id,state)
end
-- 请求心愿必达信息返回结果
function LuaYuanDan2023Mgr.QueryXinYuanBiDaState_Result(dataU)
    local wishTable = {}
    for k,v in pairs(dataU) do
        wishTable[k] = v
    end
    g_ScriptEvent:BroadcastInLua("UpdateYuanDan2023XinYuanBiDaState",wishTable)
end

------------- 心愿必达 -------

------------- 元旦奇缘 -------
LuaYuanDan2023Mgr.scroeInfo = nil
function LuaYuanDan2023Mgr.GetYuanDanQiYuanPlayId()
    return YuanDan2023_YuanDanQiYuan.GetData().GamePlayId
end

function LuaYuanDan2023Mgr.GetYuanDanQiYuanTaskTitle()
    return Gameplay_Gameplay.GetData(YuanDan2023_YuanDanQiYuan.GetData().GamePlayId).Name
end

function LuaYuanDan2023Mgr.GetYuanDanQiYuanTaskDes()
    return g_MessageMgr:FormatMessage("YuanDan2023_YuanDanQiYuan_TaskDes")
end

function LuaYuanDan2023Mgr.OnClickYuanDanQiYuanTask()
    g_MessageMgr:ShowMessage("YuanDan2023_YuanDanQiYuan_TaskRule")
end
function LuaYuanDan2023Mgr.IsInYuanDanQiYuanPlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == YuanDan2023_YuanDanQiYuan.GetData().GamePlayId then
           return true
        end
    end
    return false
end
LuaYuanDan2023Mgr.YDQYScoreType = 0
LuaYuanDan2023Mgr.YDQYScore = 0
-- 元旦奇缘匹配界面奖励次数
function LuaYuanDan2023Mgr.YuanDanQiYuanAwardTimesResult(times)
    g_ScriptEvent:BroadcastInLua("UpdateYuanDanQiAwardTimes",times)
end
-- 元旦奇缘玩法中喜福财进度
function LuaYuanDan2023Mgr.YuanDanQiYuanPlayProgress(Fu,Xi,Cai)
    if LuaYuanDan2023Mgr.scroeInfo == nil then LuaYuanDan2023Mgr.scroeInfo = {} end
    LuaYuanDan2023Mgr.scroeInfo.Fu = Fu
    LuaYuanDan2023Mgr.scroeInfo.Xi = Xi
    LuaYuanDan2023Mgr.scroeInfo.Cai = Cai
    g_ScriptEvent:BroadcastInLua("UpdateYuanDanQiYuanPlayProgress",Fu,Xi,Cai)
end
-- 元旦奇缘玩法中玩家实时获得积分
function LuaYuanDan2023Mgr.YuanDanQiYuanOnPlayerAddScore(type,score)
    LuaYuanDan2023Mgr.YDQYScoreType = type
    LuaYuanDan2023Mgr.YDQYScore = score
    g_ScriptEvent:BroadcastInLua("YuanDanQiYuanOnPlayerAddScore",type,score)
end
-- 元旦奇缘玩法结算
function LuaYuanDan2023Mgr.YuanDanQiYuanPlayResult(isWin,isReward,times)
    LuaYuanDan2023Mgr.scroeInfo = nil
    LuaYuanDan2023Mgr.YuanDanQiYuanResultInfo.isWin = isWin
    LuaYuanDan2023Mgr.YuanDanQiYuanResultInfo.isReward = isReward
    CUIManager.ShowUI(CLuaUIResources.YuanDan2023YuanDanQiYuanResultWnd)
end
------------- 元旦奇缘 ---------