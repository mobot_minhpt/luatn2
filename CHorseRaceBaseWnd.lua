-- Auto Generated!!
local CHorseRaceBaseWnd = import "L10.UI.CHorseRaceBaseWnd"
local CHorseRaceMgr = import "L10.Game.CHorseRaceMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local ETickType = import "L10.Engine.ETickType"
local LocalString = import "LocalString"
CHorseRaceBaseWnd.m_Init_CS2LuaHook = function (this) 
    this:DoCancel()
    this.speedGather = 0
    this.startTime = CServerTimeMgr.ConvertTimeStampToZone8Time(CHorseRaceMgr.Inst.playStartTime)
    this.cancel = CTickMgr.Register(MakeDelegateFromCSFunction(this.UpdateTime, Action0, this), 1000, ETickType.Loop)
    this.timeLabel.text = LocalString.GetString("已用时: ")
    this.rankLabel.text = LocalString.GetString("当前排名: ")
    this.speedLabel.text = ""
    this.progress.value = 0
    this:OnSyncSpeedUpTimes()
    this:UpdateCurrentRank()
    this:IPhoneXAdaptation()
end
CHorseRaceBaseWnd.m_UpdateProgress_CS2LuaHook = function (this) 
    local times = CHorseRaceMgr.Inst.speedTime
    this.accProgress[0].value = (times > 0) and 1 or this.speedGather
    if times > 1 then
        this.accProgress[1].value = 1
    elseif times == 1 then
        this.accProgress[1].value = this.speedGather
    else
        this.accProgress[1].value = 0
    end
    if times > 2 then
        this.accProgress[2].value = 1
    elseif times == 2 then
        this.accProgress[2].value = this.speedGather
    else
        this.accProgress[2].value = 0
    end
end
CHorseRaceBaseWnd.m_OnSyncSpeedUpTimes_CS2LuaHook = function (this) 
    local times = CHorseRaceMgr.Inst.speedTime
    this.accTime.text = System.String.Format(LocalString.GetString("加速次数:{0}"), times)
    this:UpdateProgress()

    if times > this.formerTimes then
        this:DisplayFx(this.accProgress[times - 1])
        this.speedGather = 0
    end
    this.formerTimes = times

    if this.speedGather >= 1 then
        this.speedGather = 0
    end
end
CHorseRaceBaseWnd.m_DisplayFx_CS2LuaHook = function (this, progress) 
    local fx = CommonDefs.GetComponentInChildren_Component_Type(progress, typeof(CUIFx))
    if fx ~= nil then
        fx:DestroyFx()
        fx:LoadFx(CUIFxPaths.HorseRaceSpeedUpFx)
    end
end
