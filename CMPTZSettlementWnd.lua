-- Auto Generated!!
local CMPTZSettlementWnd = import "L10.UI.CMPTZSettlementWnd"
local CPVPMgr = import "L10.Game.CPVPMgr"
CMPTZSettlementWnd.m_Init_CS2LuaHook = function (this)
    if CommonDefs.IS_VN_CLIENT then
        local rankLabel2 = this.transform:Find("Background/RankLabel/RankLabel2")
        if rankLabel2 then
            rankLabel2.gameObject:SetActive(not LocalString.isCN)
        end
    end
    
    if CPVPMgr.Inst.settlementInfo.lastIdx > 0 then
        this.rankBMFontLabel.text = tostring(CPVPMgr.Inst.settlementInfo.lastIdx)
        this.rankDynamicFontLabel.text = tostring(CPVPMgr.Inst.settlementInfo.lastIdx)
        this.scoreLabel.text = tostring(CPVPMgr.Inst.settlementInfo.lastScore)
        this.expLabel.text = tostring(CPVPMgr.Inst.settlementInfo.lastExp)
        this.freeSilverLabel.text = tostring(CPVPMgr.Inst.settlementInfo.lastYinPiao)

        local isTop10 = (CPVPMgr.Inst.settlementInfo.lastIdx <= 10)
        this.rankBMFontLabel.gameObject:SetActive(isTop10)
        this.rankDynamicFontLabel.gameObject:SetActive(not isTop10)
        this.titleLeftDecorationSprite.enabled = isTop10
        this.titleRightDecorationSprite.enabled = isTop10
    else
        this:Close()
    end
end
