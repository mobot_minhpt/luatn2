require("common/common_include")

local CUITexture = import "L10.UI.CUITexture"
local QnCheckBox = import "L10.UI.QnCheckBox"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local MessageMgr = import "L10.Game.MessageMgr"

LuaXianZhiManageWnd = class()

RegistChildComponent(LuaXianZhiManageWnd, "PlayerNameLabel", UILabel)
RegistChildComponent(LuaXianZhiManageWnd, "PlayerLevelLabel", UILabel)

RegistChildComponent(LuaXianZhiManageWnd, "SkillIcon", CUITexture)
RegistChildComponent(LuaXianZhiManageWnd, "SkillLevelLabel", UILabel)
RegistChildComponent(LuaXianZhiManageWnd, "SkillNameLabel", UILabel)
RegistChildComponent(LuaXianZhiManageWnd, "SkillStatus", GameObject)
RegistChildComponent(LuaXianZhiManageWnd, "SkillDescLabel", UILabel)
RegistChildComponent(LuaXianZhiManageWnd, "SkillStatusLabel", UILabel)
RegistChildComponent(LuaXianZhiManageWnd, "SkillTimeLabel", UILabel)

RegistChildComponent(LuaXianZhiManageWnd, "OperationTitleLabel", UILabel)
RegistChildComponent(LuaXianZhiManageWnd, "BuffQnCheckBox", QnCheckBox)
RegistChildComponent(LuaXianZhiManageWnd, "DebuffQnCheckBox", QnCheckBox)
RegistChildComponent(LuaXianZhiManageWnd, "SealedQnCheckBox", QnCheckBox)

RegistChildComponent(LuaXianZhiManageWnd, "ApplyBtn", GameObject)

RegistClassMember(LuaXianZhiManageWnd, "CheckBoxList")
RegistClassMember(LuaXianZhiManageWnd, "SelectedOperationType")
RegistClassMember(LuaXianZhiManageWnd, "PlayerInfo")
RegistClassMember(LuaXianZhiManageWnd, "SkillRemainTime")
RegistClassMember(LuaXianZhiManageWnd, "Tick")


function LuaXianZhiManageWnd:Init()

	if not CClientMainPlayer.Inst or not CLuaXianzhiMgr.m_XianZhiManagePlayer then 
		CUIManager.CloseUI(CLuaUIResources.XianZhiManageWnd)
		return
	end

	self.SelectedOperationType= 0
	self.SkillRemainTime = 0
	self.PlayerInfo = CLuaXianzhiMgr.m_XianZhiManagePlayer

	self.BuffQnCheckBox:SetSelected(false, true)
	self.DebuffQnCheckBox:SetSelected(false, true)
	self.SealedQnCheckBox:SetSelected(false, true)

	self:DestroyTick()

	self.CheckBoxList = {self.BuffQnCheckBox, self.DebuffQnCheckBox, self.SealedQnCheckBox}
	for i = 1, #self.CheckBoxList do
		self.CheckBoxList[i].OnValueChanged = DelegateFactory.Action_bool(function (value)
			if value then
				self:OnCheckBoxClick(i)
			else
				self.SelectedOperationType = 0
				self.CheckBoxList[i]:SetSelected(false, true)
			end
		end)
	end

	CommonDefs.AddOnClickListener(self.ApplyBtn, DelegateFactory.Action_GameObject(function (go)
		self:OnApplyBtnClicked(go)
	end), false)

	self:Refresh()
end

-- 更新仙职管理的信息
function LuaXianZhiManageWnd:Refresh()

	local regionId = self.PlayerInfo.regionId
	local titleId = self.PlayerInfo.titleId
	local xianZhiName = CLuaXianzhiMgr.GetXianZhiNameByRegionAndTitle(regionId, titleId)
	self.PlayerNameLabel.text = SafeStringFormat3(LocalString.GetString("%s·%s"), xianZhiName, self.PlayerInfo.playerName)

	local title = XianZhi_Title.GetData(titleId)
	local xianZhiTypeId = title.TitleType
	local xianZhiType = XianZhi_TitleType.GetData(xianZhiTypeId)

	local skillId = title.SkillCls * 100 + self.PlayerInfo.skillLevel
	local xianZhiSkill = Skill_AllSkills.GetData(skillId)

	self.SkillIcon:LoadSkillIcon(xianZhiSkill.SkillIcon)
	self.SkillNameLabel.text = xianZhiSkill.Name
	self.SkillDescLabel.text = xianZhiSkill.Display
	self.SkillLevelLabel.text = tostring(self.PlayerInfo.skillLevel)

	self:UpdateOperateStatus()
	self:UpdateOperationLabels()

end

-- 更新技能状态的图标
function LuaXianZhiManageWnd:UpdateOperateStatus()
	self.SkillStatus:SetActive(true)
	if self.PlayerInfo.operateType == EnumXianZhiOperateType.eNone then
		self.SkillStatus:SetActive(false)
	else
		local Add = self.SkillStatus.transform:Find("Add").gameObject
		local Minus = self.SkillStatus.transform:Find("Minus").gameObject
		local Sealed = self.SkillStatus.transform:Find("Sealed").gameObject

		Add:SetActive( self.PlayerInfo.operateType == EnumXianZhiOperateType.ePromote)
		Minus:SetActive( self.PlayerInfo.operateType == EnumXianZhiOperateType.eWeaken)
		Sealed:SetActive( self.PlayerInfo.operateType == EnumXianZhiOperateType.eForbid)
	end
end

function LuaXianZhiManageWnd:UpdateOperationLabels()
	if self.PlayerInfo.operateType == EnumXianZhiOperateType.eNone then
		self.SkillStatusLabel.text = LocalString.GetString("[ACF8FF]状态[-] 暂时未有操作。")
		self.SkillTimeLabel.text = LocalString.GetString("[ACF8FF]时间[-] 无。")

	else
		local operationName = self:GetOperationName(self.PlayerInfo.operateType)
		if self.PlayerInfo.operateType == EnumXianZhiOperateType.eForbid then
			self.SkillStatusLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]状态[-] 技能被%s封印"), tostring(self.PlayerInfo.operatePlayerName))
		else
			self.SkillStatusLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]状态[-] 技能被%s%s至%s级"), tostring(self.PlayerInfo.operatePlayerName),
				operationName, tostring(self.PlayerInfo.skillLevel))
		end

		self.SkillRemainTime = self.PlayerInfo.operateExpiredTime - CServerTimeMgr.Inst.timeStamp
		self.SkillTimeLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]时间[-] %s"), self:GetRemainTimeText(self.SkillRemainTime))

		self.Tick = RegisterTick(function ()
			if self.SkillRemainTime > 0 then
				self.SkillRemainTime = self.SkillRemainTime - 1
				self.SkillTimeLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]时间[-] %s"), self:GetRemainTimeText(self.SkillRemainTime))
			end
		end, 1000)
	end

	-- 操作次数
	local operationTimesLeft = CLuaXianzhiMgr.m_OperationTimes
	local status = CLuaXianzhiMgr.GetXianZhiStatus()
	local setting = XianZhi_Setting.GetData()

	if status == EnumXianZhiStatus.SanJie then
		self.OperationTitleLabel.text = SafeStringFormat3(LocalString.GetString("今日可操作次数 %d/2"), operationTimesLeft)
	else
		self.OperationTitleLabel.text = SafeStringFormat3(LocalString.GetString("今日可操作次数 %d/1"), operationTimesLeft)
	end
end

-- 获得技能操作的名称
function LuaXianZhiManageWnd:GetOperationName(operationtype)
	if operationtype == EnumXianZhiOperateType.ePromote then
		return LocalString.GetString("增益")
	elseif operationtype == EnumXianZhiOperateType.eWeaken then
		return LocalString.GetString("减损")
	elseif operationtype == EnumXianZhiOperateType.eForbid then
		return LocalString.GetString("封印")
	end
	return ""
end

function LuaXianZhiManageWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

-- 选中操作类型的处理
function LuaXianZhiManageWnd:OnCheckBoxClick(index)
	self.SelectedOperationType = index
	for i = 1, #self.CheckBoxList do
		self.CheckBoxList[i]:SetSelected(i == index, true)
	end
end

function LuaXianZhiManageWnd:OnApplyBtnClicked(go)
	if self.SelectedOperationType == 0 then
		MessageMgr.Inst:ShowMessage("XianZhi_Manage_Select_Type", {})
		return
	end

	-- 上一个操作的玩家是否是自己
	local hasAlreadyOperated = self.PlayerInfo.operateType ~= EnumXianZhiOperateType.eNone and self.PlayerInfo.operatePlayerName == CClientMainPlayer.Inst.Name
	if hasAlreadyOperated then
		MessageMgr.Inst:ShowMessage("XianZhi_Manage_Operate_Player", {})
		return
	end

	-- 操作次数
	local operationTimesLeft = CLuaXianzhiMgr.m_OperationTimes
	if operationTimesLeft <= 0 then
		MessageMgr.Inst:ShowMessage("XianZhi_Manage_Operate_Time_Zero", {})
		return
	end

	if self.SelectedOperationType == 1 then
		local status = CLuaXianzhiMgr.GetXianZhiStatusByRegionId(self.PlayerInfo.regionId)
		local setting = XianZhi_Setting.GetData()
        local maxLevel = setting.LevelUpperLimit[status-1]
        if self.PlayerInfo.skillLevel >= maxLevel then
        	MessageMgr.Inst:ShowMessage("XianZhi_Manager_Skill_Level_Max", {})
        	return 
        end
	elseif self.SelectedOperationType == 2 then
		if self.PlayerInfo.skillLevel <= 1 then
        	MessageMgr.Inst:ShowMessage("XianZhi_Manager_Skill_Level_Least", {})
        	return 
        end
	end
	
	CLuaXianzhiMgr.OpenShenZhiWnd(self.PlayerInfo.playerId, self.SelectedOperationType)
end

function LuaXianZhiManageWnd:OnEnable()
	-- g_ScriptEvent:AddListener("AcceptTaskFromBabySuccess", self, "UpdateDiaryAlert")
end

function LuaXianZhiManageWnd:OnDisable()
	self:DestroyTick()
	-- g_ScriptEvent:RemoveListener("AcceptTaskFromBabySuccess", self, "UpdateDiaryAlert")
end


function LuaXianZhiManageWnd:DestroyTick()
	if self.Tick ~= nil then
      UnRegisterTick(self.Tick)
      self.Tick=nil
    end
end

return LuaXianZhiManageWnd
