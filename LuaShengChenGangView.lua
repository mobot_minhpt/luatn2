local DelegateFactory  			= import "DelegateFactory"
local UILabel 					= import "UILabel"
local CButton 					= import "L10.UI.CButton"

LuaShengChenGangView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShengChenGangView, "LeaveBtn", "LeaveBtn", CButton)
RegistChildComponent(LuaShengChenGangView, "RuleBtn", "RuleBtn", CButton)
RegistChildComponent(LuaShengChenGangView, "BiaoChePosLabel", "BiaoChePosLabel", UILabel)
RegistChildComponent(LuaShengChenGangView, "BiaoCheNaiJiuLabel", "BiaoCheNaiJiuLabel", UILabel)
RegistChildComponent(LuaShengChenGangView, "ComplateLabel", "ComplateLabel", UILabel)
--@endregion RegistChildComponent end
RegistClassMember(LuaShengChenGangView, "m_TemplateObj")

function LuaShengChenGangView:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.LeaveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveBtnClick()
	end)


	
	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)


    --@endregion EventBind end
end

function LuaShengChenGangView:Init()
	local taskDes = FindChild(self.transform,"TaskDes"):GetComponent(typeof(UILabel))
	local str = string.gsub(taskDes.text, LocalString.GetString("生辰纲"), LocalString.GetString("周年贺礼"))
	taskDes.text = str
	self:OnShengChenGangInfoUpdate()
end

--@region UIEvent

-- 离开
function LuaShengChenGangView:OnLeaveBtnClick()
	local OnOkBtn = function()
		Gac2Gas.RequestLeavePlay()
	end
	g_MessageMgr:ShowOkCancelMessage(LocalString.GetString("离开副本则护送周年贺礼失败，确认离开？"), OnOkBtn, nil, nil, nil, false)
end


-- 显示玩法规则
function LuaShengChenGangView:OnRuleBtnClick()
	g_MessageMgr:ShowMessage("ZNQ_HUOYUN_RULE")
end

--每次更新界面显示数据
function LuaShengChenGangView:OnShengChenGangInfoUpdate()
	-- 镖车位置
	if LuaZhouNianQing2021Mgr.biaoChePosX and LuaZhouNianQing2021Mgr.biaoChePosY then
		self.BiaoChePosLabel.text = "("..LuaZhouNianQing2021Mgr.biaoChePosX..","..LuaZhouNianQing2021Mgr.biaoChePosY..")"
	else 
		self.BiaoChePosLabel.text = ""
	end
	-- 镖车耐久
	if LuaZhouNianQing2021Mgr.biaoCheNaiJiu then
		self.BiaoCheNaiJiuLabel.text =  LuaZhouNianQing2021Mgr.biaoCheNaiJiu.."%"
	else 
		self.BiaoCheNaiJiuLabel.text = ""
	end
	-- 任务完成度
	if LuaZhouNianQing2021Mgr.complateState then
		self.ComplateLabel.text =  LuaZhouNianQing2021Mgr.complateState.."%"
	else 
		self.ComplateLabel.text = ""
	end

end


-- 监听服务器数据
function LuaShengChenGangView:OnEnable()
	g_ScriptEvent:AddListener("ShengChenGangInfoUpdate", self, "OnShengChenGangInfoUpdate")
end

-- 取消监听
function LuaShengChenGangView:OnDisable()
	g_ScriptEvent:RemoveListener("ShengChenGangInfoUpdate", self, "OnShengChenGangInfoUpdate")
end

--@endregion UIEvent

return LuaShengChenGangView


