local CMainCamera = import "L10.Engine.CMainCamera"
local Vector3 = import "UnityEngine.Vector3"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local CPos = import "L10.Engine.CPos"
local Guide_Logic = import "L10.Game.Guide_Logic"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"

LuaRefineMonsterButtonWnd = class()
RegistClassMember(LuaRefineMonsterButtonWnd, "m_RefineBtn")
RegistClassMember(LuaRefineMonsterButtonWnd, "m_KaiZhenBtn")
RegistClassMember(LuaRefineMonsterButtonWnd, "m_TopAnchor")
RegistClassMember(LuaRefineMonsterButtonWnd, "m_CachedPos")
RegistClassMember(LuaRefineMonsterButtonWnd, "m_ViewPos")
RegistClassMember(LuaRefineMonsterButtonWnd, "m_ScreenPos")
RegistClassMember(LuaRefineMonsterButtonWnd, "m_TopWorldPos")

function LuaRefineMonsterButtonWnd:Init()
    self.m_RefineBtn = self.transform:Find("Anchor/RefineBtn/Texture/LianHua").gameObject
    self.m_KaiZhenBtn = self.transform:Find("Anchor/RefineBtn/Texture/KaiZhen").gameObject
    self.m_TopAnchor = nil
    self.m_CachedPos = self.transform.position

    local settingPos = LianHua_Setting.GetData().LianHuaGuaiPosAndArea
    local x = math.floor(settingPos[0])
    local y = math.floor(settingPos[1])
    self.m_TopAnchor = Utility.PixelPos2WorldPos(CreateFromClass(CPos, x, y))
    self.m_TopAnchor.y = self.m_TopAnchor.y + LianHua_Setting.GetData().LianHuaGuaiBtnHight

    UIEventListener.Get(self.m_RefineBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        if LuaZongMenMgr.m_OpenNewSystem then
            CUIManager.ShowUI(CLuaUIResources.RefineMonsterCircleNewWnd)
        else
            CUIManager.ShowUI(CLuaUIResources.RefineMonsterCircleWnd)
        end
    end)
    UIEventListener.Get(self.m_KaiZhenBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        if LuaZongMenMgr.m_OpenNewSystem then
            CUIManager.ShowUI(CLuaUIResources.RefineMonsterCircleNewWnd)
        else
            CUIManager.ShowUI(CLuaUIResources.RefineMonsterCircleWnd)
        end
    end)


    local taskList = {}
    table.insert(taskList, LianHua_Setting.GetData().ChuShiLianHuaGuideTaskId)
    table.insert(taskList, 22112181)

    for _, taskId in ipairs(taskList) do
        local guideId = 124
        if LuaZongMenMgr.m_OpenNewSystem then
            guideId = 213
        end

        if CClientMainPlayer.Inst and CommonDefs.ListContains(CClientMainPlayer.Inst.TaskProp.CurrentTaskList, typeof(UInt32), taskId) then
            local task = CommonDefs.DictGetValue(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), taskId)

            if task ~= nil and  task.CanSubmit == 0 and CGuideMgr.Inst.mPhase ~= guideId then
                local logic = Guide_Logic.GetData(guideId)
                CGuideMgr.Inst:TriggerGuide(logic)
            end

            if taskId == 22112181 and (task == nil or task.CanSubmit ~= 0) and CGuideMgr.Inst.mPhase == guideId  then
                CGuideMgr.Inst:EndCurrentPhase()
            end
        else
            if taskId == 22112181 and CGuideMgr.Inst.mPhase == guideId  then
                CGuideMgr.Inst:EndCurrentPhase()
            end
        end
    end

    self:UpdateRefineStatus(LuaZongMenMgr.m_IsFaZhenOpen)
    self:Update()
end

function LuaRefineMonsterButtonWnd:UpdateRefineStatus(isOpen)
    -- 更新状态 法阵是否开启 表现不一样
    self.m_RefineBtn:SetActive(isOpen)
    self.m_KaiZhenBtn:SetActive(not isOpen)
end

function LuaRefineMonsterButtonWnd:PutMonsterIntoCircle()
    self.m_RefineBtn:SetActive(true)
    self.m_KaiZhenBtn:SetActive(false)
end

function LuaRefineMonsterButtonWnd:Update()
    -- 更新位置 位于法阵正上方
    self.m_ViewPos = CMainCamera.Main:WorldToViewportPoint(self.m_TopAnchor)
    
    if self.m_ViewPos and self.m_ViewPos.z and self.m_ViewPos.z > 0 and 
            self.m_ViewPos.x > 0 and self.m_ViewPos.x < 1 and self.m_ViewPos.y > 0 and self.m_ViewPos.y < 1 then
        self.m_ScreenPos = CMainCamera.Main:WorldToScreenPoint(self.m_TopAnchor)
    else  
        self.m_ScreenPos = CommonDefs.op_Multiply_Vector3_Single(Vector3.up, 3000)
    end
    self.m_ScreenPos.z = 0
    self.m_TopWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_ScreenPos)

    if self.m_CachedPos.x ~= self.m_TopWorldPos.x or self.m_CachedPos.x ~= self.m_TopWorldPos.x then
        self.transform.position = self.m_TopWorldPos
        self.m_CachedPos = self.m_TopWorldPos
    end
end


-- 用于处理引导
function LuaRefineMonsterButtonWnd:GetGuideGo(methodName)
    if methodName == "GetRefineBtnGo" then
        if self.m_KaiZhenBtn.activeSelf then
            return self.m_KaiZhenBtn
        elseif self.m_RefineBtn.activeSelf then
            return self.m_RefineBtn
        else
            return nil
        end
    end
end

function LuaRefineMonsterButtonWnd:OnEnable()

    self.m_Action = DelegateFactory.Action_uint(function(guideId)
        if guideId == 124 then
            --发个rpc通知任务完成
            Gac2Gas.FinishChuShiLianHuaGuideTask()
        elseif guideId == 213 then
            Gac2Gas.FinishPutToFazhenTask()
        end
    end)

    EventManager.AddListenerInternal(EnumEventType.GuideEnd, self.m_Action)
    g_ScriptEvent:AddListener("UpdateRefineStatus", self, "UpdateRefineStatus")
    g_ScriptEvent:AddListener("PutMonsterIntoFaZhenSuccess", self, "PutMonsterIntoCircle")
end

function LuaRefineMonsterButtonWnd:OnDisable()
    EventManager.RemoveListenerInternal(EnumEventType.GuideEnd, self.m_Action)
    g_ScriptEvent:RemoveListener("UpdateRefineStatus", self, "UpdateRefineStatus")
    g_ScriptEvent:RemoveListener("PutMonsterIntoFaZhenSuccess", self, "PutMonsterIntoCircle")
end
