require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local CButton = import "L10.UI.CButton"
local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"

LuaCityWarBiaoCheMenu = class()
RegistClassMember(LuaCityWarBiaoCheMenu,"m_Background")
RegistClassMember(LuaCityWarBiaoCheMenu,"m_Title")
RegistClassMember(LuaCityWarBiaoCheMenu,"m_ScrollView")
RegistClassMember(LuaCityWarBiaoCheMenu,"m_Table")
RegistClassMember(LuaCityWarBiaoCheMenu,"m_ItemTemplate")

function LuaCityWarBiaoCheMenu:Awake()
    
end

function LuaCityWarBiaoCheMenu:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaCityWarBiaoCheMenu:Init()

	self.m_Background = self.transform:Find("Anchor/Background"):GetComponent(typeof(UISprite))
	self.m_Title = self.transform:Find("Anchor/Background/Title"):GetComponent(typeof(UILabel))
	self.m_ScrollView = self.transform:Find("Anchor/Background/ScrollView"):GetComponent(typeof(UIScrollView))
	self.m_Table = self.transform:Find("Anchor/Background/ScrollView/Table"):GetComponent(typeof(UITable))
	self.m_ItemTemplate = self.transform:Find("Anchor/Background/ScrollView/Item").gameObject

	-- 如果没有镖车信息，则直接关闭窗口
	if not CLuaCityWarMgr.BiaoCheMenuBiaoCheTbl or #CLuaCityWarMgr.BiaoCheMenuBiaoCheTbl==0 then
		self:Close()
		return
	end
	-- 初始化镖车列表
	self.m_ItemTemplate:SetActive(false)
	Extensions.RemoveAllChildren(self.m_Table.transform)

	for _, val in pairs(CLuaCityWarMgr.BiaoCheMenuBiaoCheTbl) do

		local mapId = val.mapId
		local progress = val.progress
		local status = val.status
		local map = CityWar_Map.GetData(mapId)
		if map then
			local instance = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_ItemTemplate)
			instance:SetActive(true)
			instance.transform:Find("Sprite"):GetComponent(typeof(UISprite)).spriteName = CityWarMapZone2ResIcon[(map.Zone or 0)]
			instance.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("镖车·%s"), map.Name)
			local statusLabel = instance.transform:Find("StatusLabel"):GetComponent(typeof(UILabel))
			if status == EnumBiaoCheStatus.eSubmitRes then
				statusLabel.text = LocalString.GetString("装填中")
				statusLabel.color = EnumBiaoCheStatusTextColor.eSubmitting
			elseif status == EnumBiaoCheStatus.eYaYun then
				statusLabel.text = LocalString.GetString("押运中")
				statusLabel.color = EnumBiaoCheStatusTextColor.eYaYun
			elseif status == EnumBiaoCheStatus.eYaYunEnd then
				statusLabel.text = LocalString.GetString("准备中")
				statusLabel.color = EnumBiaoCheStatusTextColor.eYaYunEnd
				instance:GetComponent(typeof(CButton)).Enabled = false
			end
			CommonDefs.AddOnClickListener(instance, DelegateFactory.Action_GameObject(function(go) self:OnItemClick(mapId, progress, status) end), false)
		end
	end

	self:Layout()
end

function LuaCityWarBiaoCheMenu:OnItemClick(mapId, progress, status)
	if status~=EnumBiaoCheStatus.eSubmitRes and status~=EnumBiaoCheStatus.eYaYun then return end
	CLuaCityWarMgr.RequestTrackToBiaoChe(mapId, status)
	self:Close()
end

function LuaCityWarBiaoCheMenu:Layout()
	if self.m_Background and self.m_Title and self.m_ScrollView and self.m_Table then

		self.m_Table:Reposition()
		local b = NGUIMath.CalculateRelativeWidgetBounds(self.m_Table.transform)
		self.m_Background.height = math.abs(self.m_ScrollView.panel.topAnchor.absolute) + math.abs(self.m_ScrollView.panel.bottomAnchor.absolute) + b.size.y + 1
		self.m_Title:ResetAndUpdateAnchors()
		self.m_ScrollView.panel:ResetAndUpdateAnchors()
		self.m_ScrollView:ResetPosition()

		local newNGUIWorldCenter = CUICommonDef.AdjustPosition(CPopupMenuInfoMgrAlignType.Top, self.m_Background.width, self.m_Background.height, 
			CLuaCityWarMgr.BiaoCheMenuTargetWorldPos, CLuaCityWarMgr.BiaoCheMenuTargetWidth, CLuaCityWarMgr.BiaoCheMenuTargetHeight)
		self.m_Background.transform.localPosition = newNGUIWorldCenter
	end
end


function LuaCityWarBiaoCheMenu:OnEnable()
	--TODO
end

function LuaCityWarBiaoCheMenu:OnDisable()
	--TODO
end

function LuaCityWarBiaoCheMenu:OnDestroy()
	--TODO
end

return LuaCityWarBiaoCheMenu
