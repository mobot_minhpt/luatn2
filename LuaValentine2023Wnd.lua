local DelegateFactory = import "DelegateFactory"
local Animation       = import "UnityEngine.Animation"
local CShopMallMgr    = import "L10.UI.CShopMallMgr"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"

LuaValentine2023Wnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaValentine2023Wnd, "animation")
RegistClassMember(LuaValentine2023Wnd, "redDot_YWQS")
RegistClassMember(LuaValentine2023Wnd, "redDot_LGTM")
RegistClassMember(LuaValentine2023Wnd, "redDot_QYLB")

function LuaValentine2023Wnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    local main = self.transform:Find("Main")
    self.animation = self.transform:GetComponent(typeof(Animation))

    UIEventListener.Get(self.transform:Find("CloseButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)

    local button_LGTM = main:Find("LingGuTanMiButton")
    self.redDot_LGTM = button_LGTM:Find("Alert").gameObject
    UIEventListener.Get(button_LGTM.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLingGuTanMiButtonClick()
	end)

    local button_YWQS = main:Find("YiWangQingShenButton")
    self.redDot_YWQS = button_YWQS:Find("Alert").gameObject
    UIEventListener.Get(button_YWQS.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnYiWangQingShenButtonClick()
	end)

    UIEventListener.Get(main:Find("YuanJunDuoCaiXieButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnYuanJunDuoCaiXieButtonClick()
	end)

    UIEventListener.Get(main:Find("HuanYuZaiJinXiButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnHuanYuZaiJinXiButtonClick()
	end)

    local button_QYLB = main:Find("QingYuanLiBaoButton")
    self.redDot_QYLB = button_QYLB:Find("Alert").gameObject
    UIEventListener.Get(button_QYLB.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnQingYuanLiBaoButtonClick()
	end)

    local activityTime = main:Find("Title_Label/ActivityTime"):GetComponent(typeof(UILabel))
    activityTime.text = Valentine2023_Setting.GetData().ActivityTime
end

function LuaValentine2023Wnd:Init()
    if LuaValentine2023Mgr.YWQSInfo.defaultView == "Main" then
        self.animation:Play("valentine2023wnd_show")
        self:UpdateRedDot()
    else
        self:SampleAnimation("valentine2023wnd_qiehuan")
    end
end

function LuaValentine2023Wnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateActivityRedDot", self, "OnUpdateActivityRedDot")
end

function LuaValentine2023Wnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateActivityRedDot", self, "OnUpdateActivityRedDot")
end

function LuaValentine2023Wnd:OnUpdateActivityRedDot(hasChanged)
    if hasChanged then
        self:UpdateRedDot()
    end
end

function LuaValentine2023Wnd:UpdateRedDot()
    self.redDot_YWQS:SetActive(LuaActivityRedDotMgr:IsRedDot(30))
    self.redDot_LGTM:SetActive(LuaActivityRedDotMgr:IsRedDot(31))
    self.redDot_QYLB:SetActive(LuaActivityRedDotMgr:IsRedDot(33))
end

-- 切换到最后一帧
function LuaValentine2023Wnd:SampleAnimation(clipName)
    local state = self.animation[clipName]
    self.animation:Play(clipName)
    state.time = state.length
    self.animation:Sample()
    state.enabled = false
end

function LuaValentine2023Wnd:ShowScheduleInfo(scheduleId)
    local scheduleData = Task_Schedule.GetData(scheduleId)
    local taskId = scheduleData.TaskID[0]

    local isExpRain = scheduleData.Type == "ZXJY"
    local expRainStart = CClientMainPlayer.Inst and CClientMainPlayer.Inst.expRainStart or false
    CLuaScheduleMgr.selectedScheduleInfo = {
        activityId = scheduleId,
        taskId = taskId,
        TotalTimes = 0,
        DailyTimes = 0,
        ActivityTime = scheduleData.ExternTip,
        ShowJoinBtn = isExpRain and expRainStart or false
    }
    CLuaScheduleInfoWnd.s_UseCustomInfo = true
    CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
end

--@region UIEvent

function LuaValentine2023Wnd:OnCloseButtonClick()
    local info = LuaValentine2023Mgr.YWQSInfo
    if info.isYWQSOpen and info.defaultView == "Main" then
        self:SampleAnimation("valentine2023wnd_show")
    else
        CUIManager.CloseUI(CLuaUIResources.Valentine2023Wnd)
    end
end

function LuaValentine2023Wnd:OnLingGuTanMiButtonClick()
    LuaActivityRedDotMgr:OnRedDotClicked(31)
    CUIManager.ShowUI(CLuaUIResources.Valentine2023LGTMEnterWnd)
end

function LuaValentine2023Wnd:OnYiWangQingShenButtonClick()
    LuaActivityRedDotMgr:OnRedDotClicked(30)
    self.animation:Play("valentine2023wnd_qiehuan")
end

function LuaValentine2023Wnd:OnYuanJunDuoCaiXieButtonClick()
    if CServerTimeMgr.Inst.timeStamp < CServerTimeMgr.Inst:GetTimeStampByStr("2023-02-11 00:00") then
        g_MessageMgr:ShowMessage("CHUNJIE2023_NOT_OPEN_IN_TRIAL_DELIVERY")
        return
    end

    self:ShowScheduleInfo(Valentine2023_Setting.GetData().YuanJunDuoCaiXieScheduleId)
end

function LuaValentine2023Wnd:OnHuanYuZaiJinXiButtonClick()
    self:ShowScheduleInfo(Valentine2023_Setting.GetData().HuanYuZaiJinXiScheduleId)
end

function LuaValentine2023Wnd:OnQingYuanLiBaoButtonClick()
    LuaActivityRedDotMgr:OnRedDotClicked(33)
    CShopMallMgr.ShowLinyuShoppingMall(21031820)
end

--@endregion UIEvent
