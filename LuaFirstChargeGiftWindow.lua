require("3rdParty/ScriptEvent")
require("common/common_include")

local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFirstChargeGiftCell = import "L10.UI.CFirstChargeGiftCell"
local Charge_ChargeSetting = import "L10.Game.Charge_ChargeSetting"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local UITable = import "UITable"

LuaFirstChargeGiftWindow = class()

RegistChildComponent(LuaFirstChargeGiftWindow, "Table", UITable)
RegistChildComponent(LuaFirstChargeGiftWindow, "GiftTemplate", GameObject)
RegistChildComponent(LuaFirstChargeGiftWindow, "ChargeButton", GameObject)
RegistChildComponent(LuaFirstChargeGiftWindow, "ChargeLabel", UILabel)

--chargeInfo
RegistClassMember(LuaFirstChargeGiftWindow, "ChargeInfo")
RegistClassMember(LuaFirstChargeGiftWindow, "GiftInfo")

function LuaFirstChargeGiftWindow:Awake()
	self.ChargeInfo = CClientMainPlayer.Inst.ItemProp.FirstCharge

	local default
	if self.ChargeInfo.ChargeTime > 0 then
        default = LocalString.GetString("领取奖励")
    else
        default = LocalString.GetString("前往充值")
    end
    self.ChargeLabel.text = default

    local chargeSetting = Charge_ChargeSetting.GetData("FirstChargeGift")
    self.GiftInfo = CommonDefs.StringSplit_ArrayChar(chargeSetting.Value, ";")

    if self.GiftInfo then
    	Extensions.RemoveAllChildren(self.Table.transform)

    	for i = 0, self.GiftInfo.Length-1 do
    		local info = CommonDefs.StringSplit_ArrayChar(self.GiftInfo[i], ",")
    		if info and info.Length == 2 then
    			local gift = NGUITools.AddChild(self.Table.gameObject, self.GiftTemplate)
    			CommonDefs.GetComponent_GameObject_Type(gift, typeof(CFirstChargeGiftCell)):Init(System.UInt32.Parse(info[0]), System.UInt32.Parse(info[1]))
    			gift:SetActive(true)
    		end
    	end
    	self.Table:Reposition()
    end
end

function LuaFirstChargeGiftWindow:OnEnable()
	CommonDefs.AddOnClickListener(self.ChargeButton, DelegateFactory.Action_GameObject(function (go)
		 self:OnChargeBtnClick(go)
	end), false)

end

function LuaFirstChargeGiftWindow:OnDisable()
	-- body
end

function LuaFirstChargeGiftWindow:OnChargeBtnClick(go)
	if self.ChargeInfo and self.ChargeInfo.ChargeTime > 0 then
		Gac2Gas.RequestGetFirstChargeGift()
	else
		CUIManager.CloseUI(CUIResources.WelfareWnd)
        CShopMallMgr.ShowChargeWnd()
	end
end
