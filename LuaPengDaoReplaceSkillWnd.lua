local QnRadioBox = import "L10.UI.QnRadioBox"
local GameObject = import "UnityEngine.GameObject"
local DelegateFactory  = import "DelegateFactory"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"

LuaPengDaoReplaceSkillWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPengDaoReplaceSkillWnd, "CancelBtn", "CancelBtn", GameObject)
RegistChildComponent(LuaPengDaoReplaceSkillWnd, "OkBtn", "OkBtn", GameObject)
RegistChildComponent(LuaPengDaoReplaceSkillWnd, "RadioBox", "RadioBox", QnRadioBox)

--@endregion RegistChildComponent end
RegistClassMember(LuaPengDaoReplaceSkillWnd,"m_SelectIndex")

function LuaPengDaoReplaceSkillWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.CancelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelBtnClick()
	end)

	UIEventListener.Get(self.OkBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkBtnClick()
	end)

    --@endregion EventBind end
end

function LuaPengDaoReplaceSkillWnd:Init()
	for i = 1, #LuaPengDaoMgr.m_ShowNextLevelBlessingInfo.extraSkills do
		local btn = self.RadioBox.m_RadioButtons[i - 1]
		local icon = btn.transform:Find("Texture"):GetComponent(typeof(CUITexture))
		local nameLabel = btn.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
		local desLabel = btn.transform:Find("DesLabel"):GetComponent(typeof(UILabel))
		local info = LuaPengDaoMgr.m_ShowNextLevelBlessingInfo.extraSkills[i]
		local type, id = info[1],info[2]
		local skillId = 0
		if type == 1 then
			local data = PengDaoFuYao_InsiderGain.GetData(id)
			skillId = data.GainId
			desLabel.text = data.DesSimplify
			nameLabel.text =  data.Name
		else
			local data = PengDaoFuYao_OutsiderSkill.GetData(id)
			skillId = data.SkillId * 100 + 1
			desLabel.text = data.Display
			nameLabel.text =  data.Name
		end
		local skillData = Skill_AllSkills.GetData(skillId)
		icon:LoadSkillIcon(skillData and skillData.SkillIcon or nil)
		UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:ShowSkillTip(skillId, icon)
        end)
	end
	self.RadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnRadioBoxSelected(btn,index)
    end)
	self.RadioBox:ChangeTo(0, true)
end

function LuaPengDaoReplaceSkillWnd:ShowSkillTip(skillId, item)
    CSkillInfoMgr.ShowSkillInfoWnd(skillId, item.transform.position, CSkillInfoMgr.EnumSkillInfoContext.Default, true, 0, 0, nil)
end

--@region UIEvent

function LuaPengDaoReplaceSkillWnd:OnCancelBtnClick()
	g_ScriptEvent:BroadcastInLua("PengDaoReplaceSkillWnd_ReplaceSkill",-1)
	CUIManager.CloseUI(CLuaUIResources.PengDaoReplaceSkillWnd)
end

function LuaPengDaoReplaceSkillWnd:OnOkBtnClick()
	g_ScriptEvent:BroadcastInLua("PengDaoReplaceSkillWnd_ReplaceSkill",self.m_SelectIndex)
	CUIManager.CloseUI(CLuaUIResources.PengDaoReplaceSkillWnd)
end

function LuaPengDaoReplaceSkillWnd:OnRadioBoxSelected(btn,index)
	self.m_SelectIndex = index
end
--@endregion UIEvent

