require("common/common_include")

local UILabel = import "UILabel"
local UISlider = import "UISlider"
local UIEventListener = import "UIEventListener"
local LuaUtils = import "LuaUtils"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local MessageWndManager = import "L10.UI.MessageWndManager"

CLuaCityWarOccupyConfirmWnd = class()
RegistClassMember(CLuaCityWarOccupyConfirmWnd, "m_Tick")
RegistClassMember(CLuaCityWarOccupyConfirmWnd, "m_TimeLabel")
RegistClassMember(CLuaCityWarOccupyConfirmWnd, "m_TimeProgress")
RegistClassMember(CLuaCityWarOccupyConfirmWnd, "m_CurrentTime")
RegistClassMember(CLuaCityWarOccupyConfirmWnd, "m_TotalTime")

CLuaCityWarOccupyConfirmWnd.Path = "ui/citywar/LuaCityWarOccupyConfirmWnd"

function CLuaCityWarOccupyConfirmWnd:Init( ... )
	-- body
end

function CLuaCityWarOccupyConfirmWnd:Awake( ... )
	local haveCity = CLuaCityWarMgr.MyMapId ~= 0 and CLuaCityWarMgr.MyCityTemplateId ~= 0
	local titleStr = haveCity and SafeStringFormat3(LocalString.GetString("我方城池：%d级领土%s城"), CityWar_Map.GetData(CLuaCityWarMgr.MyMapId).Level, CLuaCityWarMgr:IsMajorCity(CLuaCityWarMgr.MyCityTemplateId) and LocalString.GetString("主") or LocalString.GetString("小")) or LocalString.GetString("我方城池：尚无")
	self.transform:Find("Wnd_Bg_Secondary_2/TitleLabel"):GetComponent(typeof(UILabel)).text = titleStr
	self.transform:Find("Offset/OccupyName"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%s·%s（%d级领土%s城）"), CityWar_Map.GetData(CLuaCityWarMgr.OccupyMapId).Name, CityWar_MapCity.GetData(CLuaCityWarMgr.OccupyTemplateId).Name, CityWar_Map.GetData(CLuaCityWarMgr.OccupyMapId).Level, CLuaCityWarMgr:IsMajorCity(CLuaCityWarMgr.OccupyTemplateId) and LocalString.GetString("主") or LocalString.GetString("小"))
	self.m_TimeLabel = self.transform:Find("Offset/CountDownLabel"):GetComponent(typeof(UILabel))
	self.m_TimeProgress = self.transform:Find("Offset/TimeProgress"):GetComponent(typeof(UISlider))
	self.transform:Find("Offset/Rob/Tip"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("KFCZ_LUODUO_INTERFACE_TIP")
	self.transform:Find("Offset/Occupy/Tip"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("KFCZ_ZHANLING_INTERFACE_TIP")

	self.m_CurrentTime = CLuaCityWarMgr.OccupyDuration
	self.m_TotalTime = self.m_CurrentTime
	self:RefreshTick()
	self.m_Tick = RegisterTickWithDuration(function ( ... )
		self.m_CurrentTime = self.m_CurrentTime - 1
		self:RefreshTick()
		if self.m_CurrentTime <= 0 then
			self:Choose(false)
			UnRegisterTick(self.m_Tick)
			self.m_Tick = nil
		end
	end, 1000, CLuaCityWarMgr.OccupyDuration * 1000)

	UIEventListener.Get(self.transform:Find("Offset/Rob/RobBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( go )
		MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("KFCZ_PLUNDER_CITY_CONFIRM"), DelegateFactory.Action(function() self:Choose(false) end), nil, nil, nil, false)
	end)
	UIEventListener.Get(self.transform:Find("Offset/Occupy/RobBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( go )
		MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("KFCZ_OCCUPY_CITY_CONFIRM"), DelegateFactory.Action(function() self:Choose(true) end), nil, nil, nil, false)
	end)

	UIEventListener.Get(self.transform:Find("Offset/ChatBtn").gameObject).onClick = LuaUtils.VoidDelegate(function (go)
		CSocialWndMgr.ShowChatWnd(EChatPanel.Guild)
	end)

	local bOccupyMajorCity, bMyMajorCity = CLuaCityWarMgr:IsMajorCity(CLuaCityWarMgr.OccupyTemplateId), haveCity and CLuaCityWarMgr:IsMajorCity(CLuaCityWarMgr.MyCityTemplateId) or false
	local occupyLevel, myLevel = CityWar_Map.GetData(CLuaCityWarMgr.OccupyMapId).Level, haveCity and CityWar_Map.GetData(CLuaCityWarMgr.MyMapId).Level or 0
	local up = 0
	if not haveCity then
		up = 1
	elseif bOccupyMajorCity and bMyMajorCity then
		up = occupyLevel > myLevel and 1 or (occupyLevel < myLevel and -1 or 0)
	elseif bOccupyMajorCity then
		up = 1
	elseif bMyMajorCity then
		up = -1
	end
	self.transform:Find("Offset/Arrow/up").gameObject:SetActive(up == 1)
	self.transform:Find("Offset/Arrow/down").gameObject:SetActive(up == -1)
end

function CLuaCityWarOccupyConfirmWnd:RefreshTick()
	self.m_TimeLabel.text = self.m_CurrentTime.."s"
	self.m_TimeProgress.value = self.m_CurrentTime / self.m_TotalTime
	if self.m_CurrentTime == 15 or self.m_CurrentTime == 5 then
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("倒计时还剩[ff0000]%d[-]秒，请尽快做出选择"), self.m_CurrentTime))
	end
end

function CLuaCityWarOccupyConfirmWnd:Choose(bOccupy)
	CUIManager.CloseUI(CLuaUIResources.CityWarOccupyConfirmWnd)
	Gac2Gas.ConfirmOccupyHuFu(CLuaCityWarMgr.OccupyGuildId, CLuaCityWarMgr.OccupyMapId, CLuaCityWarMgr.OccupyTemplateId, CLuaCityWarMgr.OccupyCityId, bOccupy)
end

function CLuaCityWarOccupyConfirmWnd:OnDestroy( ... )
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
end
