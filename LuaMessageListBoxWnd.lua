local CMessageListBoxWndMgr = import "L10.UI.CMessageListBoxWndMgr"
local Pivot = import "UIWidget+Pivot"
local NGUIText = import "NGUIText"
local NGUIMath = import "NGUIMath"
local Extensions = import "Extensions"

LuaMessageListBoxWnd = class()

RegistChildComponent(LuaMessageListBoxWnd,"m_Bg", "Wnd_Bg_Box",UIWidget)
RegistChildComponent(LuaMessageListBoxWnd,"m_LabelTemplate","LabelTemplate", GameObject)
RegistChildComponent(LuaMessageListBoxWnd,"m_Table","Table", UITable)
RegistChildComponent(LuaMessageListBoxWnd,"m_CloseButton","CloseButton", GameObject)

RegistClassMember(LuaMessageListBoxWnd, "m_DefaultMaxLabelHeight")
RegistClassMember(LuaMessageListBoxWnd, "m_DefaultWndHeight")
function LuaMessageListBoxWnd:Init()
    self.m_LabelTemplate.gameObject:SetActive(false)
    self.m_DefaultWndHeight = self.m_Bg.height
    self.m_DefaultMaxLabelHeight = 160
    UIEventListener.Get(self.m_CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CIndirectUIResources.MessageListBoxWnd)
    end)
    self:InitMessageList()
end

function LuaMessageListBoxWnd:InitMessageList()
    Extensions.RemoveAllChildren(self.m_Table.transform)
    local msgs = CMessageListBoxWndMgr.msgs
    for p, message in string.gmatch(msgs, '<p(.-)>(.-)</p>') do
        local fontsize, pivot = 38, Pivot.Left
        for k,a,b,v in string.gmatch(p, '(%w+)(%s-)=(%s-)(%w+)') do
            if k == "fontsize" then fontsize = tonumber(v) end
            if k == "pivot" then
                if v == "left" then pivot = Pivot.Left
                elseif v == "center" then pivot = Pivot.Center
                elseif v == "right" then pivot = Pivot.Right end
            end
        end
        if fontsize and pivot then
            self:InitMessage(fontsize, pivot, message)
        end
    end
    self.m_Table:Reposition()
    local bound = NGUIMath.CalculateRelativeWidgetBounds(self.m_Table.transform)
    local height = bound.size.y
    if height > self.m_DefaultMaxLabelHeight then
        local newHeight = self.m_DefaultWndHeight + (height - self.m_DefaultMaxLabelHeight)
        self.m_Bg.height = newHeight
    end
end

function LuaMessageListBoxWnd:InitMessage(fontsize, pivot, message)
    local obj = NGUITools.AddChild(self.m_Table.gameObject, self.m_LabelTemplate.gameObject)
    obj.gameObject:SetActive(true)
    local label = obj:GetComponent(typeof(UILabel))
    label.fontSize = fontsize
    label.pivot = pivot
    label.text = String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor24(label.color), message)
    label:UpdateNGUIText()
    label:ResizeCollider()
end
