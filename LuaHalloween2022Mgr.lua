local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType =import "L10.Game.EnumWarnFXType"
local CClientObjectRoot = import "L10.Game.CClientObjectRoot"
local CRenderObject = import "L10.Engine.CRenderObject"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CScene = import "L10.Game.CScene"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CCameraParam = import "L10.Engine.CameraControl.CCameraParam"
local CameraMode = import "L10.Engine.CameraControl.CameraMode"
local CCenterCountdownWnd = import "L10.UI.CCenterCountdownWnd"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"
local PlayerSettings = import "L10.Game.PlayerSettings"
local Quaternion = import "UnityEngine.Quaternion"
local UISoundEvents=import "SoundManager+UISoundEvents"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
LuaHalloween2022Mgr = {}

LuaHalloween2022Mgr.HideTopRightUI = true
LuaHalloween2022Mgr.EnterPlayOpenBullet = true

LuaHalloween2022Mgr.NPCRO = nil       -- 红绿灯玩法中的NPC
LuaHalloween2022Mgr.TrafficLightStatus = nil
LuaHalloween2022Mgr.TrafficLightNpcPerformTicks = {}
LuaHalloween2022Mgr.isAiCamera = false

LuaHalloween2022Mgr.TrafficLightPlayLeftMember = 0  -- 红绿灯玩法一局中的剩余人数

LuaHalloween2022Mgr.PlayerTrafficLightAwardResult = nil

LuaHalloween2022Mgr.TrafficLightPlayResult = nil
LuaHalloween2022Mgr.FireDefencePlayResult = nil

LuaHalloween2022Mgr.PlayerDeathAniFinish = false
LuaHalloween2022Mgr.PlayerFail = false
LuaHalloween2022Mgr.m_music = nil

LuaHalloween2022Mgr.m_BonFireEnginedId = nil
LuaHalloween2022Mgr.m_BonFireHP = nil
LuaHalloween2022Mgr.m_AddHeadInfoEnginedID = false
LuaHalloween2022Mgr.m_isSkipMod = false
LuaHalloween2022Mgr.m_TrafficLightGameEnd = false
LuaHalloween2022Mgr.m_worldPosition = nil
LuaHalloween2022Mgr.m_FireDeferenceMonsterWave = 0

LuaHalloween2022Mgr.m_OpenFireDeferenceWndFromMain = false -- 是否从主界面打开的篝火界面 （用作动效的处理

-- 保存Player数据，以防止切换场景时访问不到CClientMainPlayer
LuaHalloween2022Mgr.m_Name = nil
LuaHalloween2022Mgr.m_Id = nil
LuaHalloween2022Mgr.m_Class = nil
LuaHalloween2022Mgr.m_Gender = nil
LuaHalloween2022Mgr.m_Level = nil
LuaHalloween2022Mgr.m_ServerName = nil
LuaHalloween2022Mgr.m_NotFirstTurnBack = false
LuaHalloween2022Mgr.m_NotFirstRedLight = false

function LuaHalloween2022Mgr.IsInTrafficPlay() 
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == Halloween2022_Setting.GetData().TrafficLightPlayId then
           return true
        end
    end
    return false
end

function LuaHalloween2022Mgr.IsInFireDefencePlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == Halloween2022_Setting.GetData().ProtectCampfirePlayId or gamePlayId == Halloween2022_Setting.GetData().ProtectCampfireHeroPlayId then
           return true
        end
    end
    return false
end

--@region红绿灯裁判NPC
function LuaHalloween2022Mgr.ShowTrafficLightNPC(templateId, x, y, state, totalKeepDuration, leftKeepDuration)
    LuaHalloween2022Mgr.TrafficLightStatus = {}
    LuaHalloween2022Mgr.TrafficLightStatus.state = state
    LuaHalloween2022Mgr.TrafficLightStatus.totalKeepDuration = totalKeepDuration
    LuaHalloween2022Mgr.TrafficLightStatus.leftKeepDuration = leftKeepDuration

    g_ScriptEvent:BroadcastInLua("HwTrafficLightChange")
    local npcData = NPC_NPC.GetData(templateId)
    if LuaHalloween2022Mgr.NPCRO == nil and npcData ~= nil then
        LuaHalloween2022Mgr.CreateTrafficLightNPC(x, y, true)
        --LuaHalloween2022Mgr.AddTrafficNPCFx()
    end
    if LuaHalloween2022Mgr.NPCRO then
        LuaHalloween2022Mgr.PlaySound(state)
        if state == EnumTrafficLightNpcState.Back and LuaHalloween2022Mgr.m_NotFirstTurnBack then
            LuaHalloween2022Mgr.NPCRO:RemoveFX("TrafficLightNPCFace")
            LuaHalloween2022Mgr.NPCRO:DoAni("zhuanhui01", false, 0, 1, 0, true, leftKeepDuration)
            LuaHalloween2022Mgr.NPCRO.AniEndCallback = DelegateFactory.Action(function()
                LuaHalloween2022Mgr.NPCRO.AniEndCallback = nil
                LuaHalloween2022Mgr.NPCRO:DoAni("stand01", true, 0, 1.0, 0, true, 1.0)
            end)
        elseif state == EnumTrafficLightNpcState.Back and not LuaHalloween2022Mgr.m_NotFirstTurnBack then
            LuaHalloween2022Mgr.ResetCamera(false)      -- 第一次转身
            -- 播batdialog
            LuaHalloween2022Mgr.m_NotFirstTurnBack = true
            CLuaBatDialogWnd.ShowWnd(Halloween2022_Setting.GetData().FirstGreenLightBatDialog)
        elseif state == EnumTrafficLightNpcState.TurnFront then
            LuaHalloween2022Mgr.NPCRO:DoAni("zhuantou01", false, 0, 1.0 / (leftKeepDuration / 1000), 0, true, 1)
        elseif state == EnumTrafficLightNpcState.Front then
            LuaHalloween2022Mgr.AddTrafficNPCFx()
            --LuaHalloween2022Mgr.NPCRO:DoAni("zhuantou01", false, 0, 1, 0, true, leftKeepDuration)
            if not LuaHalloween2022Mgr.m_NotFirstRedLight then
                LuaHalloween2022Mgr.m_NotFirstRedLight = true
                CLuaBatDialogWnd.ShowWnd(Halloween2022_Setting.GetData().FirstRedLightBatDialog)
            end
        end
    end
end

function LuaHalloween2022Mgr.CreateTrafficLightNPC(x, y, showNPC)
    LuaHalloween2022Mgr.m_NotFirstTurnBack = false
    LuaHalloween2022Mgr.m_NotFirstRedLight = false
    local height = Halloween2022_Setting.GetData().TrafficNpcPosY
    if not CClientObjectRoot.Inst then return end
    local root = CClientObjectRoot.Inst.Other.gameObject
    local obj = root.transform:Find("TrafficLightNPC")
    if obj then
        obj.gameObject:GetComponent(typeof(CRenderObject)):Destroy()
    end
    local prefabName = Halloween2022_Setting.GetData().TrafficNpcModel
    local prefabPath = ""
    local name,index = string.match(prefabName,"(%w+)_(%w+)")
    if name and index then 
        prefabPath = "assets/res/character/npc/"..name.."/prefab/"..prefabName..".prefab"
    else
        prefabPath = "assets/res/character/npc/"..prefabName.."/prefab/"..prefabName.."_01.prefab"
    end
    local scale = Halloween2022_Setting.GetData().TrafficNpcScale
    LuaHalloween2022Mgr.NPCRO = CRenderObject.CreateRenderObject(root, "TrafficLightNPC")
    LuaHalloween2022Mgr.NPCRO:LoadMain(prefabPath)
    LuaHalloween2022Mgr.NPCRO.Position = Vector3(x, height, y)
    LuaHalloween2022Mgr.NPCRO.Scale = scale
    LuaHalloween2022Mgr.NPCRO.Direction = 30
    --local FxId = Halloween2022_Setting.GetData().TrafficNpcFx
    --local fx = CEffectMgr.Inst:AddObjectFX(FxId,LuaHalloween2022Mgr.NPCRO,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
    --LuaHalloween2022Mgr.NPCRO:AddFX(FxId, fx)
    --LuaHalloween2022Mgr.NPCRO.Direction = Quaternion.Euler(0,-150,0)
    LuaHalloween2022Mgr.NPCRO:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(ro)
		LuaHalloween2022Mgr.ShowOrHideNPC(showNPC)
	end))
end

function LuaHalloween2022Mgr.PlaySound(state)
    -- 裁判播音效
    local red = Halloween2022_Setting.GetData().TurnInSound
    local green = Halloween2022_Setting.GetData().TurnBackSound
    if state == EnumTrafficLightNpcState.Front then
        LuaHalloween2022Mgr.CloseSoundInPlay()
        SoundManager.Inst:PlayOneShot(green,Vector3.zero,nil,0)
    elseif state == EnumTrafficLightNpcState.Back and PlayerSettings.SoundEnabled then        
        LuaHalloween2022Mgr.CloseSoundInPlay()
        LuaHalloween2022Mgr.m_music = SoundManager.Inst:PlaySound(red,Vector3.zero,1,nil,0)
    end
end

function LuaHalloween2022Mgr.CloseSoundInPlay()
    if not SoundManager.Inst then return end 
    if LuaHalloween2022Mgr.m_music then
        SoundManager.Inst:StopSound(LuaHalloween2022Mgr.m_music)
        LuaHalloween2022Mgr.m_music = nil
    end
end

function LuaHalloween2022Mgr.AddTrafficNPCFx()
    if not LuaHalloween2022Mgr.NPCRO then return end
    local FxId = Halloween2022_Setting.GetData().TrafficNpcFx
    local fx = CEffectMgr.Inst:AddObjectFX(FxId,LuaHalloween2022Mgr.NPCRO,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
    LuaHalloween2022Mgr.NPCRO:AddFX("TrafficLightNPCFace", fx)
end

function LuaHalloween2022Mgr.DestroyTrafficLightNPC()
    if LuaHalloween2022Mgr.NPCRO ~= nil then
        LuaHalloween2022Mgr.NPCRO:Destroy()
        LuaHalloween2022Mgr.NPCRO = nil
    end
end
--@endregion

--@region红绿灯玩法
-- 取消注册的定时器
function LuaHalloween2022Mgr.UnRegisterTrafficLightNpcPerformTick(id)
    if LuaHalloween2022Mgr.TrafficLightNpcPerformTicks[id] then
        UnRegisterTick(LuaHalloween2022Mgr.TrafficLightNpcPerformTicks[id])
        LuaHalloween2022Mgr.TrafficLightNpcPerformTicks[id] = nil
    end
end
function LuaHalloween2022Mgr.ShowOrHideNPC(isShow)
    if not LuaHalloween2022Mgr.NPCRO then return end
    LuaHalloween2022Mgr.NPCRO.Visible = isShow 

    -- local childCount = LuaHalloween2022Mgr.NPCRO.transform.childCount
    -- if childCount < 1 then return end
    -- local child0 = LuaHalloween2022Mgr.NPCRO.transform:GetChild(0)
    -- if child0 then child0.gameObject:SetActive(isShow) end
end

function LuaHalloween2022Mgr.AddNPCWorldPositionFx(FxId)
    local height = Halloween2022_Setting.GetData().TrafficNpcPosY
    local x, y = string.match(Halloween2022_Setting.GetData().TrafficNpcPos, "(%d+),(%d+)")
    x, y = tonumber(x), tonumber(y)    
    local pos = Vector3(x,height,y)
    if CEffectMgr.Inst and not LuaHalloween2022Mgr.m_isSkipMod then
       local fx = CEffectMgr.Inst:AddWorldPositionFX(FxId, pos, 0, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
       return fx
    end
    return nil
end
-- 开场演出
function LuaHalloween2022Mgr.SyncStartTrafficLightNpcPerform()
    LuaHalloween2022Mgr.TrafficLightPlayResult = nil
    LuaHalloween2022Mgr.m_TrafficLightGameEnd = false
    LuaHalloween2022Mgr.PlayerFail = false
    LuaHalloween2022Mgr.m_isSkipMod = false
    LuaHalloween2022Mgr.ClearAllTick()
    if not CameraFollow or CameraFollow.Inst == nil then
        return
    end
    -- 设置镜头
    local cameraParams = Halloween2022_Setting.GetData().TrafficPlayCameraParams
    LuaHalloween2022Mgr.isAiCamera = true
    CameraFollow.Inst:BeginAICamera(Vector3(0, 0, 0), Vector3(0, 0, 0), math.ceil(cameraParams[0]))
    local cameraPos = Vector3(cameraParams[1], cameraParams[2], cameraParams[3])
    local targetRot = Vector3(cameraParams[4], cameraParams[5], cameraParams[6])
    CameraFollow.Inst:AICameraMoveTo(cameraPos, targetRot, cameraParams[7])

    local waitTimesList = Halloween2022_Setting.GetData().TrafficPlayShowWaitTime
    local waitTimes = {}
    waitTimes[0] = waitTimesList[0]
    waitTimes[1] = waitTimesList[0] + waitTimesList[1]
    waitTimes[2] = waitTimesList[0] + waitTimesList[1] + waitTimesList[2]

    local x, y = string.match(Halloween2022_Setting.GetData().TrafficNpcPos, "(%d+),(%d+)")
    x, y = tonumber(x), tonumber(y)    
    LuaHalloween2022Mgr.TrafficLightNpcPerformTicks[0] = RegisterTickOnce(function()
        if not LuaHalloween2022Mgr.NPCRO then
            LuaHalloween2022Mgr.CreateTrafficLightNPC(x, y , false)
        end

        if LuaHalloween2022Mgr.m_worldPosition then 
            LuaHalloween2022Mgr.m_worldPosition:Destroy() 
            LuaHalloween2022Mgr.m_worldPosition = nil
        end
        local fxId = Halloween2022_Setting.GetData().TrafficPlayUIFxParams[0]
        LuaHalloween2022Mgr.m_worldPosition = LuaHalloween2022Mgr.AddNPCWorldPositionFx(fxId)
        --LuaHalloween2022Mgr.NPCRO.Direction = 0
        -- if CEffectMgr.Inst and not LuaHalloween2022Mgr.m_isSkipMod then
        --     local fxId = Halloween2022_Setting.GetData().TrafficPlayUIFxParams[0]
        --     local duration = Halloween2022_Setting.GetData().TrafficPlayUIFxParams[1]
        --     local ro = LuaHalloween2022Mgr.NPCRO--CClientMainPlayer.Inst.RO
        --     local fx = CEffectMgr.Inst:AddObjectFX(fxId,ro,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
        --     ro:AddFX(fxId, fx)
        -- end
        -- if CCommonUIFxWnd and not LuaHalloween2022Mgr.m_isSkipMod then
        --     local fxId = Halloween2022_Setting.GetData().TrafficPlayUIFxParams[0]
        --     local duration = Halloween2022_Setting.GetData().TrafficPlayUIFxParams[1]
        --     CCommonUIFxWnd.AddUIFx(fxId, duration, false, false)
        -- end
        CLuaBatDialogWnd.ShowWnd(Halloween2022_Setting.GetData().TrafficPlayWndIds[0])
        LuaHalloween2022Mgr.UnRegisterTrafficLightNpcPerformTick(0)
    end, 1000 * waitTimes[0])
    LuaHalloween2022Mgr.TrafficLightNpcPerformTicks[1] = RegisterTickOnce(function()
        CLuaBatDialogWnd.ShowWnd(Halloween2022_Setting.GetData().TrafficPlayWndIds[1])
        LuaHalloween2022Mgr.UnRegisterTrafficLightNpcPerformTick(1)
    end, 1000 * waitTimes[1])
    LuaHalloween2022Mgr.TrafficLightNpcPerformTicks[2] = RegisterTickOnce(function()
        CLuaBatDialogWnd.ShowWnd(Halloween2022_Setting.GetData().TrafficPlayWndIds[2])
        LuaHalloween2022Mgr.UnRegisterTrafficLightNpcPerformTick(2)
    end, 1000 * waitTimes[2])
    LuaHalloween2022Mgr.TrafficLightNpcPerformTicks[3] = RegisterTickOnce(function()
        if not LuaHalloween2022Mgr.NPCRO then
            LuaHalloween2022Mgr.CreateTrafficLightNPC(x, y ,true)
        end
        --LuaHalloween2022Mgr.NPCRO:RemoveFX(fxId)
        if LuaHalloween2022Mgr.m_worldPosition then 
            LuaHalloween2022Mgr.m_worldPosition:Destroy() 
            LuaHalloween2022Mgr.m_worldPosition = nil
        end
        
        LuaHalloween2022Mgr.NPCRO.Direction = -150
        --LuaHalloween2022Mgr.AddTrafficNPCFx()
        LuaHalloween2022Mgr.ShowOrHideNPC(true)
        LuaHalloween2022Mgr.NPCRO:DoAni("zhuanshen01", false, 0, 1, 0, true, 1)
        LuaHalloween2022Mgr.NPCRO.AniEndCallback = DelegateFactory.Action(function()
            LuaHalloween2022Mgr.NPCRO.AniEndCallback = nil
            LuaHalloween2022Mgr.NPCRO.Direction = 30
            LuaHalloween2022Mgr.NPCRO:DoAni("stand01", true, 0, 1.0, 0, true, 1.0)
        end)
        LuaHalloween2022Mgr.UnRegisterTrafficLightNpcPerformTick(3)
    end, 1000 *  Halloween2022_Setting.GetData().TrafficPlayUIFxParams[1])
    g_ScriptEvent:BroadcastInLua("HwMuTouRenStartPerform",false)
end

-- 结束开场演出
function LuaHalloween2022Mgr.SyncEndTrafficLightNpcPerform(isLeft, count)
    LuaHalloween2022Mgr.m_isSkipMod = false
    LuaHalloween2022Mgr.ClearAllTick()

    if LuaHalloween2022Mgr.isAiCamera and CameraFollow then
        CameraFollow.Inst:EndAICamera()
        LuaHalloween2022Mgr.isAiCamera = false
    end

    if CCenterCountdownWnd and not isLeft then
        LuaHalloween2022Mgr.ShowImageRuleWnd()
        CCenterCountdownWnd.count = count;
        CCenterCountdownWnd.InitStartTime();
        CUIManager.ShowUI(CUIResources.CenterCountdownWnd);
    end
    g_ScriptEvent:BroadcastInLua("HwMuTouRenStartPerform",true)
end

function LuaHalloween2022Mgr.SyncTrafficLightNpcEndPerform()
    LuaHalloween2022Mgr.m_TrafficLightGameEnd = true
    if LuaHalloween2022Mgr.PlayerFail then return end
    if not LuaHalloween2022Mgr.IsInTrafficPlay() then return end
    LuaHalloween2022Mgr.m_isSkipMod = false
    g_ScriptEvent:BroadcastInLua("HwMuTouRenStartPerform",false)
    for i=0,6,1 do
        LuaHalloween2022Mgr.UnRegisterTrafficLightNpcPerformTick(i)
    end
    -- 演出时间
    local EndwaitTimes = Halloween2022_Setting.GetData().TrafficPlayEndFlowTime
    -- 剧情窗口
    local batDialog = Halloween2022_Setting.GetData().TrafficPlayEndWndIds
    -- 接管镜头
    local cameraParams = Halloween2022_Setting.GetData().TrafficPlayEndCameraParams

    if LuaHalloween2022Mgr.NPCRO then   -- 矫正一下NPC朝向和动作
        LuaHalloween2022Mgr.NPCRO.Direction = 30
        LuaHalloween2022Mgr.NPCRO:DoAni("stand01", true, 0, 1.0, 0, true, 1.0)
    end
    if CameraFollow.Inst and not LuaHalloween2022Mgr.m_isSkipMod then
        LuaHalloween2022Mgr.isAiCamera = true
        CameraFollow.Inst:BeginAICamera(Vector3(0, 0, 0), Vector3(0, 0, 0), math.ceil(cameraParams[0]))
        local cameraPos = Vector3(cameraParams[1], cameraParams[2], cameraParams[3])
        local targetRot = Vector3(cameraParams[4], cameraParams[5], cameraParams[6])
        CameraFollow.Inst:AICameraMoveTo(cameraPos, targetRot, cameraParams[7])
    end

    LuaHalloween2022Mgr.TrafficLightNpcPerformTicks[4] = RegisterTickOnce(function()
        if not LuaHalloween2022Mgr.IsInTrafficPlay() then return end
        CLuaBatDialogWnd.ShowWnd(batDialog[0])
        if LuaHalloween2022Mgr.NPCRO then
            LuaHalloween2022Mgr.NPCRO:DoAni("zhuanshen01", false, 0, 1, 0, true, 1)
            LuaHalloween2022Mgr.NPCRO.AniEndCallback = DelegateFactory.Action(function()
                LuaHalloween2022Mgr.NPCRO.AniEndCallback = nil
                LuaHalloween2022Mgr.NPCRO.Direction = -150
                LuaHalloween2022Mgr.NPCRO:DoAni("stand01", true, 0, 1.0, 0, true, 1.0)
            end)
        end
        LuaHalloween2022Mgr.UnRegisterTrafficLightNpcPerformTick(4)
    end, 1000 * EndwaitTimes[0])
    LuaHalloween2022Mgr.TrafficLightNpcPerformTicks[5] = RegisterTickOnce(function()
        if not LuaHalloween2022Mgr.IsInTrafficPlay() then return end
        CLuaBatDialogWnd.ShowWnd(batDialog[1])
        if not LuaHalloween2022Mgr.NPCRO then return end
        LuaHalloween2022Mgr.NPCRO:RemoveFX("TrafficLightNPCFace")
        LuaHalloween2022Mgr.ShowOrHideNPC(false)

        if LuaHalloween2022Mgr.m_worldPosition then 
            LuaHalloween2022Mgr.m_worldPosition:Destroy() 
            LuaHalloween2022Mgr.m_worldPosition = nil
        end
        local fxId = Halloween2022_Setting.GetData().TrafficPlayEndUIFxParams[0]
        LuaHalloween2022Mgr.m_worldPosition = LuaHalloween2022Mgr.AddNPCWorldPositionFx(fxId)
        -- if CEffectMgr.Inst and not LuaHalloween2022Mgr.m_isSkipMod then
        --     local fxId = Halloween2022_Setting.GetData().TrafficPlayEndUIFxParams[0]
        --     local duration = Halloween2022_Setting.GetData().TrafficPlayEndUIFxParams[1]
        --     local ro = LuaHalloween2022Mgr.NPCRO--CClientMainPlayer.Inst.RO
        --     local fx = CEffectMgr.Inst:AddObjectFX(fxId,ro,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
        --     ro:AddFX("TrafficLightNPCRunFx", fx)
        -- end
        -- if CCommonUIFxWnd and not LuaHalloween2022Mgr.m_isSkipMod then
        --     local fxId = Halloween2022_Setting.GetData().TrafficPlayEndUIFxParams[0]
        --     local duration = Halloween2022_Setting.GetData().TrafficPlayEndUIFxParams[1]
        --     CCommonUIFxWnd.AddUIFx(fxId, duration, false, false)
        -- end
        LuaHalloween2022Mgr.UnRegisterTrafficLightNpcPerformTick(5)
    end, 1000 * EndwaitTimes[1])
    
    LuaHalloween2022Mgr.TrafficLightNpcPerformTicks[6] = RegisterTickOnce(function()
        if LuaHalloween2022Mgr.isAiCamera and CameraFollow then
            LuaHalloween2022Mgr.isAiCamera = false
            CameraFollow.Inst:EndAICamera()
        end
        local fxId = Halloween2022_Setting.GetData().TrafficPlayEndUIFxParams[0]
        if LuaHalloween2022Mgr.m_worldPosition then 
            LuaHalloween2022Mgr.m_worldPosition:Destroy() 
            LuaHalloween2022Mgr.m_worldPosition = nil
        end
        -- if LuaHalloween2022Mgr.NPCRO then
        --     LuaHalloween2022Mgr.NPCRO:RemoveFX("TrafficLightNPCRunFx")
        -- end
        for i=0,6,1 do
            LuaHalloween2022Mgr.UnRegisterTrafficLightNpcPerformTick(i)
        end
        g_ScriptEvent:BroadcastInLua("HwMuTouRenStartPerform",true)
    end, 1000 * EndwaitTimes[2])

end
-- 跳过演出
function LuaHalloween2022Mgr.SkipTrafficLightNpcPreform()
    LuaHalloween2022Mgr.m_isSkipMod = true

    if LuaHalloween2022Mgr.isAiCamera and CameraFollow then
        CameraFollow.Inst:EndAICamera()
        LuaHalloween2022Mgr.isAiCamera = false
    end
    g_ScriptEvent:BroadcastInLua("HwMuTouRenStartPerform",true)
end
-- 更新玩法内总人数
function LuaHalloween2022Mgr.SyncPlayerTrafficMembers(members)
    LuaHalloween2022Mgr.TrafficLightPlayLeftMember = members
    g_ScriptEvent:BroadcastInLua("SyncPlayerTrafficMembers")
end


-- 玩家被淘汰播放死亡动画并判断弹出结算框
function LuaHalloween2022Mgr.PlayerBeFail(engineId)
    if not CClientObjectMgr.Inst or not CClientMainPlayer.Inst  then return end
    local Obj = CClientObjectMgr.Inst:GetObject(engineId)

    if Obj then
        if CClientMainPlayer.Inst.EngineId == engineId then 
            LuaHalloween2022Mgr.PlayerFail = true
            -- 玩家自己可见的特殊死亡特效
            LuaHalloween2022Mgr.CloseSoundInPlay()
            LuaHalloween2022Mgr.ResetCamera(false)
            local fxDuration = Halloween2022_Setting.GetData().KillSpecialEffectDuration
            CLuaBatDialogWnd.ShowWnd(Halloween2022_Setting.GetData().TrafficFailPlayWndIds[0])
            LuaHalloween2022Mgr.TrafficLightNpcPerformTicks[8] = RegisterTickOnce(function()
                LuaHalloween2022Mgr.PlayerFail = false
                LuaHalloween2022Mgr.PlayerDeathAniFinish = true
                if LuaHalloween2022Mgr.m_TrafficLightGameEnd then
                    LuaHalloween2022Mgr.SyncTrafficLightNpcEndPerform()
                end
                if LuaHalloween2022Mgr.TrafficLightPlayResult then
                    CUIManager.ShowUI(CLuaUIResources.HwMuTouRenResultWnd)
                    LuaHalloween2022Mgr.PlayerDeathAniFinish = false
                end
                LuaHalloween2022Mgr.UnRegisterTrafficLightNpcPerformTick(8)
            end,1000 * (fxDuration + 1))
        else
            -- 加弹道死亡特效
            local duartion = Halloween2022_Setting.GetData().KillNormalEffectLength
            if duartion <= 0 then return end
            local fxId = Halloween2022_Setting.GetData().KillNormalEffect
            local NPCRO = LuaHalloween2022Mgr.NPCRO
            if NPCRO and Obj then
                local fx = CEffectMgr.Inst:AddBulletFX(
                    fxId,
                    NPCRO,
                    Obj.RO,
                    0,3.0,-1,nil)
            end
        end
    end
end

function LuaHalloween2022Mgr.SyncPlayerTrafficMatchStatus(isMatching)
    g_ScriptEvent:BroadcastInLua("SyncPlayerTrafficMatchStatus", isMatching)
end

function LuaHalloween2022Mgr.SyncPlayerTrafficLightAward(participationAwardTimes, dailyAwardTimes, isGet1stAward,isGetOnceAchieveAward)
    LuaHalloween2022Mgr.PlayerTrafficLightAwardResult = {}
    LuaHalloween2022Mgr.PlayerTrafficLightAwardResult.participationAwardTimes = participationAwardTimes
    LuaHalloween2022Mgr.PlayerTrafficLightAwardResult.dailyAwardTimes = dailyAwardTimes
    LuaHalloween2022Mgr.PlayerTrafficLightAwardResult.isGet1stAward = isGet1stAward
    LuaHalloween2022Mgr.PlayerTrafficLightAwardResult.isGetOnceAchieveAward = isGetOnceAchieveAward
    g_ScriptEvent:BroadcastInLua("SyncPlayerTrafficLightAward")
end

-- isWin: 1 胜利，2：被发现游戏失败，3：倒计时结束或主动放弃游戏失败
function LuaHalloween2022Mgr.ShowMuTouRenResultWnd(isWin, rank, totalCount, participationAwardTimes, dailyAwardTimes, isGet1stAward,isGetOnceAchieveAward)
    LuaHalloween2022Mgr.TrafficLightPlayResult = {}
    LuaHalloween2022Mgr.TrafficLightPlayResult.isWin = isWin
    LuaHalloween2022Mgr.TrafficLightPlayResult.rank = rank
    LuaHalloween2022Mgr.TrafficLightPlayResult.totalCount = totalCount
    LuaHalloween2022Mgr.TrafficLightPlayResult.GetAwardTbl = {}
    LuaHalloween2022Mgr.PlayEndFx(isWin)
    if participationAwardTimes > 0 then
        table.insert(LuaHalloween2022Mgr.TrafficLightPlayResult.GetAwardTbl,Halloween2022_Setting.GetData().Traffic30mAward)
    end
    if dailyAwardTimes > 0 then
        table.insert(LuaHalloween2022Mgr.TrafficLightPlayResult.GetAwardTbl,Halloween2022_Setting.GetData().TrafficDailyAward)
    end
    if isGet1stAward then
        table.insert(LuaHalloween2022Mgr.TrafficLightPlayResult.GetAwardTbl,Halloween2022_Setting.GetData().Traffic1stPlayerAward)
    end
    if isGetOnceAchieveAward then
        table.insert(LuaHalloween2022Mgr.TrafficLightPlayResult.GetAwardTbl,Halloween2022_Setting.GetData().TrafficFirstAward)
    end
    -- 打开玩家弹幕
    g_ScriptEvent:BroadcastInLua("SyncPlayerComplateTrafficLightGame",true)
    -- 被发现需要等到玩家的死亡动画播放完毕之后弹出结算界面
    if isWin == 2 and not LuaHalloween2022Mgr.PlayerDeathAniFinish then
        return
    end
    LuaHalloween2022Mgr.CloseSoundInPlay()
    LuaHalloween2022Mgr.TrafficLightNpcPerformTicks[9] = RegisterTickOnce(function()
        if LuaHalloween2022Mgr.TrafficLightPlayResult then 
            CUIManager.ShowUI(CLuaUIResources.HwMuTouRenResultWnd)
        end
        LuaHalloween2022Mgr.UnRegisterTrafficLightNpcPerformTick(9)
    end,1000)
    LuaHalloween2022Mgr.PlayerDeathAniFinish = false
end
function LuaHalloween2022Mgr.PlayEndFx(isWin)
    if not CCommonUIFxWnd then return end
    local WinfxId = 89000135
    local LossfxId = 89000139
    local duration = 1
    if isWin == 1 then
        CCommonUIFxWnd.AddUIFx(WinfxId, duration, false, false)
    elseif isWin == 2 or isWin == 3 then
        CCommonUIFxWnd.AddUIFx(LossfxId, duration, false, false)
        if isWin == 2 then
            LuaHalloween2022Mgr.TrafficLightNpcPerformTicks[7] = RegisterTickOnce(function()
                local fxId = Halloween2022_Setting.GetData().KillSpecialEffect
                local fxDuration = Halloween2022_Setting.GetData().KillSpecialEffectDuration
                if CCommonUIFxWnd then
                    CCommonUIFxWnd.AddUIFx(fxId, fxDuration, true, false)
                end
                LuaHalloween2022Mgr.UnRegisterTrafficLightNpcPerformTick(7)
            end,1000)
        end
    end
end

function LuaHalloween2022Mgr.ShowImageRuleWnd()
    local imagePaths = {} 
    local imageList = Halloween2022_Setting.GetData().ImageRuleImageList
    local msgs = {}
    local MsgList = Halloween2022_Setting.GetData().ImageRuleMsgsList
    for i=0,MsgList.Length-1 do
        table.insert(msgs,g_MessageMgr:FormatMessage(MsgList[i]))
    end
    for i=0,imageList.Length-1 do
        table.insert(imagePaths,imageList[i])
    end
    LuaImageRuleMgr:ShowCommonImageRuleWnd(imagePaths, msgs)
end
-- 镜头设置
function LuaHalloween2022Mgr.ResetCamera(SetSound)
    if CameraFollow.Inst == nil or CClientMainPlayer.Inst == nil then
        return
    end
    local CameraData = Halloween2022_Setting.GetData().TrafficLightCamera
    local Vec = Vector3(CameraData[0], CameraData[1], CameraData[2])
    if not SetSound then
        if CameraFollow.Inst.CurMode ~= CameraMode.E3DPlus then
            local message = g_MessageMgr:FormatMessage("HALLOWEEN2022_TRAFFICLIGHT_OPEN_3DPLUS_CONFIRM")
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
                CameraFollow.Inst:SetMode(CameraMode.E3DPlus)
                PlayerSettings.Saved3DMode = 2
                CameraFollow.Inst.targetRZY = Vec
                CameraFollow.Inst.CurCameraParam:ApplyRZY(Vec)
            end), nil)
            return
        end
        if CameraFollow.Inst.CurMode == CameraMode.E3DPlus then
            CameraFollow.Inst.targetRZY = Vec
            CameraFollow.Inst.CurCameraParam:ApplyRZY(Vec)
        end
        return 
    end
	if SetSound and (CameraFollow.Inst.CurMode ~= CameraMode.E3DPlus or not PlayerSettings.SoundEnabled) then
		local message = g_MessageMgr:FormatMessage("HALLOWEEN2022_TRAFFICLIGHT_OPEN_3DPLUSANDVOICE_CONFIRM")
		MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
			CameraFollow.Inst:SetMode(CameraMode.E3DPlus)
			PlayerSettings.Saved3DMode = 2
            PlayerSettings.SoundEnabled = true
            CameraFollow.Inst.targetRZY = Vec
		    CameraFollow.Inst.CurCameraParam:ApplyRZY(Vec)
		end), nil)
		return
	end
	if CameraFollow.Inst.CurMode == CameraMode.E3DPlus then
        CameraFollow.Inst.targetRZY = Vec
		CameraFollow.Inst.CurCameraParam:ApplyRZY(Vec)
	end
end
--@endregion


--@region 守护篝火
function LuaHalloween2022Mgr.ShowFireDefenceResultWnd(isWin,mode,heroAward, normalOnceAward, normalDailyAward)
    LuaHalloween2022Mgr.FireDefencePlayResult = {}
    LuaHalloween2022Mgr.FireDefencePlayResult.isWin = isWin
    LuaHalloween2022Mgr.FireDefencePlayResult.mode = mode
    local rewards = {}
    if heroAward then
        table.insert(rewards,Halloween2022_Setting.GetData().HeroCampfireOnceAward)
    end
    if normalDailyAward then
        table.insert(rewards,Halloween2022_Setting.GetData().NormalCampfireDailyAward)
    end
    if normalOnceAward then
        table.insert(rewards,Halloween2022_Setting.GetData().NormalCampfireOnceAward)
    end
    LuaHalloween2022Mgr.FireDefencePlayResult.rewards = rewards 
    CUIManager.ShowUI(CLuaUIResources.HwFireDefenceResultWnd)
end

function LuaHalloween2022Mgr.SyncFireDefenceMonsterTimes(Times)
    LuaHalloween2022Mgr.m_FireDeferenceMonsterWave = Times
    g_ScriptEvent:BroadcastInLua("SyncFireDefenceMonsterTimes")
end
--@endregion

-- 南瓜头放烟火
function LuaHalloween2022Mgr.PlayPumpkinHeadFire(engineId, fireFxId, duraion)
    local co = CClientObjectMgr.Inst:GetObject(engineId)
    if co then
        local ro = co.RO
        if ro then
            local fx = CEffectMgr.Inst:AddObjectFX(fireFxId,ro,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
            ro:AddFX("AutoRemoveFashionLightHeadFx", fx)
        end
    end
    local tick = RegisterTickOnce(function()  
        local co = CClientObjectMgr.Inst:GetObject(engineId)
        if co then
            local ro = co.RO
            if ro then
                ro:RemoveFX("AutoRemoveFashionLightHeadFx")
            end
        end  
        UnRegisterTick(tick)
        tick = nil        
    end,duraion * 1000)
end
function LuaHalloween2022Mgr.UpdateProtectCampfireHp(engineId, hp, crtHpFull, hpFull, mp, crtMpFull, mpFull)
    LuaHalloween2022Mgr.m_BonFireEnginedId = engineId
    LuaHalloween2022Mgr.m_BonFireHP = {}
    LuaHalloween2022Mgr.m_BonFireHP.hp = hp
    LuaHalloween2022Mgr.m_BonFireHP.crtHpFull = crtHpFull
    g_ScriptEvent:BroadcastInLua("UpdateProtectCampfireHp")
end
function LuaHalloween2022Mgr.GetPlayerInfo()
	if  CClientMainPlayer.Inst then
        LuaHalloween2022Mgr.m_Name = CClientMainPlayer.Inst.RealName
        LuaHalloween2022Mgr.m_Id = CClientMainPlayer.Inst.Id
        LuaHalloween2022Mgr.m_Class = CClientMainPlayer.Inst.Class
        LuaHalloween2022Mgr.m_Gender = CClientMainPlayer.Inst.Gender
        LuaHalloween2022Mgr.m_Level = CClientMainPlayer.Inst.Level
        LuaHalloween2022Mgr.m_ServerName = CClientMainPlayer.Inst:GetMyServerName()
    end
end

function LuaHalloween2022Mgr.Halloween2022SyncAllowToUseHongBaoCover(usedJade, isAllowToUseCover)
    local MainPlayer = CClientMainPlayer.Inst
    if MainPlayer then
        MainPlayer.PlayProp.AccumulatedHongBaoJade = usedJade
        MainPlayer.PlayProp.IsHongBaoCoverEnabled = isAllowToUseCover
    end
    g_ScriptEvent:BroadcastInLua("Halloween2022SyncAllowToUseHongBaoCover")
end

function LuaHalloween2022Mgr.IsInTrafficLightPlayTime()
    local timeStr = Halloween2022_Setting.GetData().TrafficPlayTimeString
    local nowTime = CServerTimeMgr.Inst.timeStamp
    local startHour,startMin,endHour,endMin = string.match(timeStr,"(%d+):(%d+)-(%d+):(%d+)")
    local startTimeStamp = CServerTimeMgr.Inst:GetTodayTimeStampByTime(startHour,startMin,0)
    local endTimeStamp = CServerTimeMgr.Inst:GetTodayTimeStampByTime(endHour,endMin,0)
    return nowTime >= startTimeStamp and nowTime <= endTimeStamp
end

-- 达成目标但还没领封面，用于一个活动红点的显示
function LuaHalloween2022Mgr.HaveAcceptableHongBaoCover()
    local TotalJade = Halloween2022_Setting.GetData().AllowToUseCoverTotalJade
    local MainPlayer = CClientMainPlayer.Inst
    if MainPlayer then
        if TotalJade <= MainPlayer.PlayProp.AccumulatedHongBaoJade and not (MainPlayer.PlayProp.IsHongBaoCoverEnabled == 1) then
            return true
        end
    end
    return false
end
function LuaHalloween2022Mgr.ClearAllTick()
    for i = 0, 9, 1 do  -- 清一下所有tick
        LuaHalloween2022Mgr.UnRegisterTrafficLightNpcPerformTick(i)
    end
    LuaHalloween2022Mgr.TrafficLightNpcPerformTicks = {}
end
function LuaHalloween2022Mgr.RedDotCheck()
    local CoverRedDot = LuaHalloween2022Mgr.HaveAcceptableHongBaoCover()
    local PlayRedDot = LuaActivityRedDotMgr:IsRedDot(11)
    return CoverRedDot or PlayRedDot
end
-- 得到了红包红包封面
function LuaHalloween2022Mgr.HasGetHongBaoCover()
    local MainPlayer = CClientMainPlayer.Inst
    if MainPlayer then
        return MainPlayer.PlayProp.IsHongBaoCoverEnabled == 1
    end
    return false
end
local CSceneDynamicStuff = import "L10.Engine.Scene.CSceneDynamicStuff"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
-- 万圣节期间的青丘大地图调整
function LuaHalloween2022Mgr:OnMainPlayerCreated()
    local startTime = Halloween2022_Setting.GetData().TaskTimeList[0]
    local endTime = Halloween2022_Setting.GetData().TaskTimeList[1]
    local startStamp = CServerTimeMgr.Inst:GetTimeStampByStr(startTime)
    local endStamp = CServerTimeMgr.Inst:GetTimeStampByStr(endTime)
    local nowStamp = CServerTimeMgr.Inst.timeStamp
    g_ScriptEvent:RemoveListener("TeamMemberInfoChange", self, "OnTeamMemberInfoChange")
    g_ScriptEvent:RemoveListener("OnBuffInfoUpdate", self, "OnMainPlayerBuffInfoUpdate")
    if not LuaHalloween2022Mgr.IsInFireDefencePlay() then
        LuaHalloween2022Mgr.m_FireDeferenceMonsterWave = 0 -- 重置缓存的怪物波数
        LuaHalloween2022Mgr.ClearHeadInfoInFireDefence()
    else 
        g_ScriptEvent:AddListener("OnBuffInfoUpdate", self, "OnMainPlayerBuffInfoUpdate")
        g_ScriptEvent:AddListener("TeamMemberInfoChange", self, "OnTeamMemberInfoChange")    
    end
    if LuaHalloween2022Mgr.IsInTrafficPlay() then
        LuaHalloween2022Mgr.ResetCamera(true)
    else
        LuaHalloween2022Mgr.ClearAllTick()
        LuaHalloween2022Mgr.DestroyTrafficLightNPC()
    end
    if nowStamp >= startStamp and nowStamp <= endStamp then
        local dayStuffs
        if CRenderScene.Inst and CScene.MainScene then
            dayStuffs = CRenderScene.Inst.transform:Find("HideRoot")
            dayStuffs = dayStuffs and dayStuffs:GetComponent(typeof(CSceneDynamicStuff))
            if not dayStuffs then return end 
            dayStuffs:RemoveStuff("Halloween2022Stuff")
            if CScene.MainScene.SceneTemplateId == 16000026 then
                dayStuffs:ShowStuff(true,"Assets/Res/Levels/Dynamic/qingqiu_HideStuffs_wanshengjie.prefab","Halloween2022Stuff")
            end
        end
    end
end
function LuaHalloween2022Mgr:OnMainPlayerBuffInfoUpdate(args)
    if not CClientMainPlayer.Inst then return end
    local enginedId = args[0]
    if CClientMainPlayer.Inst.EngineId == enginedId then
        LuaHalloween2022Mgr.OnBuffInfoUpdate(enginedId)
        EventManager.BroadcastInternalForLua(EnumEventType.RefreshHeadInfoVisible,{enginedId})
    end 
end
function LuaHalloween2022Mgr:OnTeamMemberInfoChange(memberId)
    if not CClientPlayerMgr.Inst then return end
    local obj = CClientPlayerMgr.Inst:GetPlayer(memberId[0])
    if not obj then return end
    LuaHalloween2022Mgr.OnBuffInfoUpdate(obj.EngineId)
    EventManager.BroadcastInternalForLua(EnumEventType.RefreshHeadInfoVisible,{obj.EngineId})
end
function LuaHalloween2022Mgr.OnBuffInfoUpdate(EngineId)
    local enginedId = EngineId
    local args = {}
    args[0] = enginedId
    args[1] = {}
    args[1].texturePath = "Assets/Res/UI/Texture/FestivalActivity/Festival_Halloween/Halloween2022/Material/HwFireDefenceEnterWnd_emo.mat"
    args[1].textureLocalPosition = Vector3(-1,12,0)
    args[1].textureSize = {111,107}
    args[2] = {}
    args[2].LabelSize = 56
    args[2].Color =  NGUIText.ParseColor24("FFEFB6",0)
    args[2].LabelLocalPosition = Vector3(0,7,0)
    args[3] = {}
    args[5] = "HwFireDefence"
        --engineId,textureForm,LabelForm,CountDownForm,needShow = args[0],args[1],args[2],args[3],args[4]
    local clientObj = CClientObjectMgr.Inst:GetObject(enginedId)
    if clientObj then
        local addBuff = LuaHalloween2022Mgr.CheckBuffIDHeadIndo(clientObj,66001001,args) or LuaHalloween2022Mgr.CheckBuffIDHeadIndo(clientObj,66001101,args)
        if not addBuff then
            args[4] = false
            g_ScriptEvent:BroadcastInLua("UpdatePlayerHeadInfoCountDown",args)
        end
    end
end

function LuaHalloween2022Mgr.CheckBuffIDHeadIndo(clientObj,buffId,args)
    if CommonDefs.DictContains_LuaCall(clientObj.BuffProp.Buffs, buffId) then -- 普通模式
        local buffInfo = CommonDefs.DictGetValue_LuaCall(clientObj.BuffProp.Buffs, buffId)
        if buffInfo.EndTime > 0 then
            local ts = CommonDefs.op_Subtraction_DateTime_DateTime(CServerTimeMgr.Inst:GetZone8Time(), buffInfo.receivedTime)
            local leftTime = math.max(0,(buffInfo.EndTime - ts.TotalSeconds))
            if leftTime <= 0 then return end
            args[3].content = leftTime + CServerTimeMgr.Inst.timeStamp
            args[3].autoTick = true
            args[3].tickInterval = 1000
            args[4] = true
            LuaHalloween2022Mgr.m_AddHeadInfoEnginedID = true
            g_ScriptEvent:BroadcastInLua("UpdatePlayerHeadInfoCountDown",args)
            return true
        end
    end
    return false
end

function LuaHalloween2022Mgr.ClearHeadInfoInFireDefence()
    if not LuaHalloween2022Mgr.m_AddHeadInfoEnginedID then return end
    local args = {}
    args[4] = false
    args[5] = "HwFireDefence"
    g_ScriptEvent:BroadcastInLua("UpdatePlayerHeadInfoCountDown",args)
    LuaHalloween2022Mgr.m_AddHeadInfoEnginedID = false
end

function LuaHalloween2022Mgr.isInHalloweenHongBaoCoverTime()
    local startTime = Halloween2022_Setting.GetData().TaskTimeList[0]
    local endTime = Halloween2022_Setting.GetData().TaskTimeList[1]
    local startStamp = CServerTimeMgr.Inst:GetTimeStampByStr(startTime)
    local endStamp = CServerTimeMgr.Inst:GetTimeStampByStr(endTime)
    local nowStamp = CServerTimeMgr.Inst.timeStamp
    return nowStamp >= startStamp and nowStamp <= endStamp 
end