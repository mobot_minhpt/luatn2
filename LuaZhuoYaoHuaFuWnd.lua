local CBrush = import "L10.Game.CBrush"
local MouseEvent = import "L10.Game.CBrush+MouseEvent"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CUITexture = import "L10.UI.CUITexture"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local Vector2 = import "UnityEngine.Vector2"
local Physics = import "UnityEngine.Physics"
local LayerDefine = import "L10.Engine.LayerDefine"
local Animation = import "UnityEngine.Animation"
local Rect = import "UnityEngine.Rect"
local Constants = import "L10.Game.Constants"
local Camera = import "UnityEngine.Camera"

LuaZhuoYaoHuaFuWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhuoYaoHuaFuWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaZhuoYaoHuaFuWnd, "RefreshButton", "RefreshButton", GameObject)
RegistChildComponent(LuaZhuoYaoHuaFuWnd, "QnCheckBox", "QnCheckBox", QnCheckBox)
RegistChildComponent(LuaZhuoYaoHuaFuWnd, "EndButton", "EndButton", GameObject)
RegistChildComponent(LuaZhuoYaoHuaFuWnd, "FuTexture", "FuTexture", CUITexture)
RegistChildComponent(LuaZhuoYaoHuaFuWnd, "HuaFuView", "HuaFuView", GameObject)
RegistChildComponent(LuaZhuoYaoHuaFuWnd, "ResultView", "ResultView", GameObject)
RegistChildComponent(LuaZhuoYaoHuaFuWnd, "WenZiTexture", "WenZiTexture", CUITexture)
RegistChildComponent(LuaZhuoYaoHuaFuWnd, "Camera", "Camera", Camera)
--@endregion RegistChildComponent end
RegistClassMember(LuaZhuoYaoHuaFuWnd, "m_IsSkipAni")
RegistClassMember(LuaZhuoYaoHuaFuWnd, "m_Brush")
RegistClassMember(LuaZhuoYaoHuaFuWnd, "m_HasBrush")
RegistClassMember(LuaZhuoYaoHuaFuWnd, "m_Animation")
RegistClassMember(LuaZhuoYaoHuaFuWnd, "m_ShowEndTick")

function LuaZhuoYaoHuaFuWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	if CommonDefs.IS_VN_CLIENT then
		self.EndButton.transform:Find("Label"):GetComponent(typeof(UILabel)).spacingX = 0
	end
	
	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)

	UIEventListener.Get(self.EndButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEndButtonClick()
	end)

	UIEventListener.Get(self.RefreshButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRefreshButtonClick()
	end)

    --@endregion EventBind end
	self.QnCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (value)
        self:OnQnCheckBoxChanged(value)
    end)

end


function LuaZhuoYaoHuaFuWnd:Init()
	local isSkip = PlayerPrefs.GetInt("ZhuoYaoHuaFuWnd_SkipAni")
	self.m_IsSkipAni = isSkip == 1
	self.QnCheckBox.Selected = self.m_IsSkipAni
	self.m_Animation = self.gameObject:GetComponent(typeof(Animation))
	self:InitBrush()
	self.HuaFuView.gameObject:SetActive(true)
	self.ResultView.gameObject:SetActive(false)
	local randomIndex = math.random(1,5)
	local wenziMatPath = SafeStringFormat3("UI/Texture/Transparent/Material/zhuoyaohuafuwnd_wenzi_0%d.mat",randomIndex)
	self.WenZiTexture:LoadMaterial(wenziMatPath)
end

function LuaZhuoYaoHuaFuWnd:InitBrush()
	if self.m_Brush then
		self.m_Brush:Destory()
	end
	self.m_Brush = CBrush()
	--local texture = CreateFromClass(Texture2D, self.FuTexture.texture.width,self.FuTexture.texture.height, TextureFormat.RGBA32, false)
	self.m_Brush:Init(self.FuTexture.texture.width,self.FuTexture.texture.height, NGUIText.ParseColor24("3a55a5", 0), Color(0,0,0,0), nil, 0.4, 1)
	NGUITools.SetLayer(self.HuaFuView.transform:Find("Panel").gameObject,LayerDefine.UI)
	NGUITools.SetLayer(self.HuaFuView.transform:Find("Button").gameObject,LayerDefine.UI)
end

function LuaZhuoYaoHuaFuWnd:Update()
	if not self.m_Brush then return end
	local pos = self:GetMouseCoord()
	if pos == nil then return end
	if Input.GetMouseButtonDown(0) then
		self.m_Brush:MouseDraw(MouseEvent.MouseButtonDown,pos)
	end
	if Input.GetMouseButton(0) then
		self.m_Brush:MouseDraw(MouseEvent.MouseButton,pos)
	end
	if Input.GetMouseButtonUp(0) then
		self.m_Brush:MouseDraw(MouseEvent.MouseButtonUp,pos)
	end
	local rt = self.m_Brush:GetResult()
	self.FuTexture.texture.mainTexture = rt
end

function LuaZhuoYaoHuaFuWnd:GetMouseCoord()
	--local pos = Vector2(Input.mousePosition.x, Input.mousePosition.y)
	if UICamera.currentCamera == nil then
		return nil
	end
	local ray = UICamera.currentCamera:ScreenPointToRay(Input.mousePosition)
    local hits = Physics.RaycastAll(ray, 99999, bit.lshift(1,LayerDefine.UI))
    for i = 0, hits.Length - 1 do
        local collider = hits[i].collider.gameObject
        if collider.gameObject == self.FuTexture.gameObject then
			local worldPoint = hits[i].point
			local localPoint = self.FuTexture.transform:InverseTransformPoint(worldPoint)
			local weightAndHeight = Vector2(self.FuTexture.texture.width,self.FuTexture.texture.height)
			local newpos = Vector2(localPoint.x + weightAndHeight.x / 2, localPoint.y + weightAndHeight.y / 2)
			self.m_HasBrush = true
			return newpos
		end
    end
	return nil
end

function LuaZhuoYaoHuaFuWnd:OnDestory()
	if self.m_Brush then
		self.m_Brush:Destory()
	end
end

function LuaZhuoYaoHuaFuWnd:OnEnable()
	g_ScriptEvent:AddListener("OnRequestCatchYaoGuaiResult_Ite", self, "OnRequestCatchYaoGuaiResult")
end

function LuaZhuoYaoHuaFuWnd:OnDisable()
	self:CancelShowEndTick()
	Gac2Gas.QueryYaoguaiCatchInfo_Ite(LuaZhuoYaoMgr.m_ZhuoYaoHuaFuWnd_YaoguaiTempId)
	Gac2Gas.RequestYaoGuaiTuJianData()
	Gac2Gas.QueryZhuoYaoUnlockInfo_Ite()
	g_ScriptEvent:RemoveListener("OnRequestCatchYaoGuaiResult_Ite", self, "OnRequestCatchYaoGuaiResult")
end

function LuaZhuoYaoHuaFuWnd:OnRequestCatchYaoGuaiResult()
	local t = LuaZhuoYaoMgr.m_CatchYaoGuaiResultInfo
	local bIsWinSocialWndOpened = false
	if CommonDefs.IsPCGameMode() then
		local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
		bIsWinSocialWndOpened = CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened
	end
	self.Camera.rect = Rect(0, 0, 1 - (bIsWinSocialWndOpened and Constants.WinSocialWndRatio or 0), 1)
	NGUITools.SetLayer(self.HuaFuView.transform:Find("Panel").gameObject,LayerDefine.ModelForNGUI_3D)
	NGUITools.SetLayer(self.HuaFuView.transform:Find("Button").gameObject,LayerDefine.ModelForNGUI_3D)
	self.m_Animation:Play(t.bSuccess and (self.m_IsSkipAni and "zhuoyaohuafuwnd_wintiaoguo" or "zhuoyaohuafuwnd_win") or (self.m_IsSkipAni and "zhuoyaohuafuwnd_failtiaoguo" or "zhuoyaohuafuwnd_fail")) 
	--self.HuaFuView.gameObject:SetActive(false)
	--self.ResultView.gameObject:SetActive(true)
	self.CloseButton.gameObject:SetActive(false)
	-- self:CancelShowEndTick()
	-- self.m_ShowEndTick = RegisterTick(function ()
	-- 	-- NGUITools.SetLayer(self.ResultView.gameObject,LayerDefine.UI)
	-- 	-- self.ResultView.transform.localScale = Vector3(1,1,1)
	-- 	-- NGUITools.SetLayer(self.HuaFuView.transform:Find("Panel").gameObject,LayerDefine.UI)
	-- 	self.ResultView.transform:Find("BottomButton/ZhuoYaoHuaFuWndProgress").gameObject:SetActive(true)
	-- end,4500)
end

function LuaZhuoYaoHuaFuWnd:CancelShowEndTick()
	if self.m_ShowEndTick then
		UnRegisterTick(self.m_ShowEndTick)
		self.m_ShowEndTick = nil
	end
end

--@region UIEvent

function LuaZhuoYaoHuaFuWnd:OnRefreshButtonClick()
	self.m_Brush:Init(self.FuTexture.texture.width,self.FuTexture.texture.height, NGUIText.ParseColor24("3a55a5", 0), Color(0,0,0,0), nil, 0.4, 1)
	local rt = self.m_Brush:GetResult()
	self.FuTexture.texture.mainTexture = rt
end

function LuaZhuoYaoHuaFuWnd:OnEndButtonClick()
	if not self.m_HasBrush then
		g_MessageMgr:ShowMessage("ZhuoYaoHuaFuWnd_Not_HasBrush")
		return
	end
	Gac2Gas.RequestCatchYaoGuai_Ite(LuaZhuoYaoMgr.m_ZhuoYaoHuaFuWnd_YaoguaiTempId, LuaZhuoYaoMgr.m_ZhuoYaoHuaFuWnd_ZhuoyaoType)
end

function LuaZhuoYaoHuaFuWnd:OnCloseButtonClick()
	CUIManager.CloseUI(CLuaUIResources.ZhuoYaoHuaFuWnd)
end

function LuaZhuoYaoHuaFuWnd:OnQnCheckBoxChanged(value)
	self.m_IsSkipAni = value
	PlayerPrefs.SetInt("ZhuoYaoHuaFuWnd_SkipAni",value and 1 or 0)
	Extensions.SetLocalPositionZ(self.QnCheckBox.transform,1760)
end
--@endregion UIEvent

function LuaZhuoYaoHuaFuWnd:GetGuideGo(methodName)
    if methodName == "GetHuaFuTexture" then
        return self.FuTexture.gameObject
	elseif methodName == "GetResultViewCloseBtn" then
		return self.ResultView.transform:Find("BottomButton").gameObject
	end
	return nil
end
