local LuaGameObject=import "LuaGameObject"

local UIWidget = import "UIWidget"
local CFightingSpiritMgr=import "L10.Game.CFightingSpiritMgr"
local UIScrollView=import "UIScrollView"
local UIRoot=import "UIRoot"
local Screen=import "UnityEngine.Screen"
local NGUIMath=import "NGUIMath"
local Mathf = import "UnityEngine.Mathf"
local Extensions = import "Extensions"

CLuaFightingSpiritFutiInfoTip=class()

RegistClassMember(CLuaFightingSpiritFutiInfoTip,"m_TitleLabel")
RegistClassMember(CLuaFightingSpiritFutiInfoTip,"m_Table")
RegistClassMember(CLuaFightingSpiritFutiInfoTip,"m_ScrollView")
RegistClassMember(CLuaFightingSpiritFutiInfoTip,"m_FutiItem")
RegistClassMember(CLuaFightingSpiritFutiInfoTip,"m_header")
RegistClassMember(CLuaFightingSpiritFutiInfoTip,"m_BackGround")

function CLuaFightingSpiritFutiInfoTip:Init()
    self.m_FutiItem:SetActive(false)
    self.m_TitleLabel.text = CFightingSpiritMgr.Instance.ServerName_Tip
    Extensions.RemoveAllChildren(self.m_Table.transform)

    if CFightingSpiritMgr.Instance.FutiInfo_Tip then
        for i = 0, CFightingSpiritMgr.Instance.FutiInfo_Tip.Count-1 do
            local inst = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_FutiItem)
            self:InitItem(inst, CFightingSpiritMgr.Instance.FutiInfo_Tip[i])
            inst:SetActive(true)
        end
    end
    
    self.m_Table:Reposition()
    self.m_ScrollView = self.transform:Find("Anchor/Offset/ScrollView"):GetComponent(typeof(UIScrollView))

    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenHeight = Screen.height * scale
    local contentTopPadding = Mathf.Abs(self.m_ScrollView.panel.topAnchor.absolute)
    local contentBottomPadding = Mathf.Abs(self.m_ScrollView.panel.bottomAnchor.absolute)
    local totalHeight = NGUIMath.CalculateRelativeWidgetBounds(self.m_Table.transform).size.y + (self.m_Table.padding.y * 2) + contentTopPadding + contentBottomPadding + (self.m_ScrollView.panel.clipSoftness.y * 2)
    local displayWndHeight = Mathf.Clamp(totalHeight, contentTopPadding + contentBottomPadding + 50, virtualScreenHeight - 100)
    self.m_BackGround:GetComponent(typeof(UIWidget)).height = Mathf.CeilToInt(displayWndHeight)
    self.m_header:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
    self.m_ScrollView.panel:ResetAndUpdateAnchors()
    self.m_ScrollView:ResetPosition()
end

function CLuaFightingSpiritFutiInfoTip:InitItem(item,index)
    local tf = item.transform
    local futiIcon = LuaGameObject.GetChildNoGC(tf,"FuTiIcon").sprite
    local futiLabel = LuaGameObject.GetChildNoGC(tf,"FuTiLabel").label
    futiIcon.spriteName=""
    if index == 1 then
        --  精力附体
        futiLabel.text = LocalString.GetString("斗魂精力附体[FFFE91]（1级）[-]")
        futiIcon.spriteName = "dhgz_jing"
    elseif index == 2 then
        -- 气力附体
        futiLabel.text = LocalString.GetString("斗魂气力附体[FFFE91]（1级）[-]")
        futiIcon.spriteName = "dhgz_qi"
    elseif index == 3 then
        -- 神力附体
        futiLabel.text = SafeStringFormat3(LocalString.GetString("斗魂神力附体[FFFE91]（%d级）[-]"), CFightingSpiritMgr.Instance.ShenLiLv_Tip)
        futiIcon.spriteName = "dhgz_shen"
    end
end

function CLuaFightingSpiritFutiInfoTip:Update()
    self:ClickThroughToClose()
end

function CLuaFightingSpiritFutiInfoTip:ClickThroughToClose()
    if Input.GetMouseButtonDown(0) then       
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform) and
            CUICommonDef.GetNearestPanelDepth(self.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
            CUIManager.CloseUI(CUIResources.FightingSpiritFutiInfoTip)
        end
    end
end

return CLuaFightingSpiritFutiInfoTip
