-- Auto Generated!!
local Buff_Buff = import "L10.Game.Buff_Buff"
local CClientFormula = import "L10.Game.CClientFormula"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLivingSkillEffectDisplay = import "L10.UI.CLivingSkillEffectDisplay"
local CLivingSkillMgr = import "L10.Game.CLivingSkillMgr"
local LifeSkill_Skill = import "L10.Game.LifeSkill_Skill"
local LocalString = import "LocalString"
local NGUIMath = import "NGUIMath"
local Vector3 = import "UnityEngine.Vector3"
CLivingSkillEffectDisplay.m_Refresh_CS2LuaHook = function (this) 
    local skillClsId = CLivingSkillMgr.Inst.selectedSkill
    local skillLevel = CLivingSkillMgr.Inst:GetSkillLevel(skillClsId)
    this:Init(skillClsId, skillLevel)
    this.baseDisplay:Init()

    this:TryToCenter()

    if this.feishengDescLabel ~= nil then
        this.feishengDescLabel.gameObject:SetActive(false)
        if skillClsId == 950005 or skillClsId == 950006 then
            if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.IsInXianShenStatus then
                local skillId = skillClsId * 100 + skillLevel
                local lv = CLuaDesignMgr.LifeSkill_Skill_GetFeiShengModifyLevel(skillId, CClientMainPlayer.Inst.Level)
                if lv == skillLevel then
                    this.feishengDescLabel.text = nil
                elseif lv == 0 then
                    this.feishengDescLabel.gameObject:SetActive(true)
                    this.feishengDescLabel.text = System.String.Format(LocalString.GetString("修正等级lv.{0}    暂不生效"), lv)
                else
                    local data = LifeSkill_Skill.GetData(skillClsId * 100 + lv)
                    if data ~= nil then
                        local buff = Buff_Buff.GetData(data.Effect)

                        local desc = buff.Display
                        this.feishengDescLabel.gameObject:SetActive(true)
                        this.feishengDescLabel.text = System.String.Format(LocalString.GetString("修正等级lv.{0}    修正效果：{1}"), lv, desc)
                    end
                end
            end
        end
    end
    this.descTable:Reposition()
    local b = NGUIMath.CalculateRelativeWidgetBounds(this.descTable.transform)
    this.bgSprite.height = math.floor((b.size.y + 20))
end
CLivingSkillEffectDisplay.m_TryToCenter_CS2LuaHook = function (this) 
    this.curLevel:UpdateNGUIText()
    this.nextLevel:UpdateNGUIText()
    local b = NGUIMath.CalculateRelativeWidgetBounds(this.curLevel.transform.parent)
    this.curLevel.transform.parent.localPosition = Vector3(- b.extents.x, 0)
end
CLivingSkillEffectDisplay.m_Init_CS2LuaHook = function (this, skillClsId, level) 

    if level == 0 then
        this.curLevel.text = ""
        --下一级
        local data = LifeSkill_Skill.GetData(skillClsId * 100 + level + 1)
        if data ~= nil then
            local buff = Buff_Buff.GetData(data.Effect)
            if buff ~= nil then
                this.nextLevel.text = buff.Display

                if skillClsId == CLivingSkillEffectDisplay.WGYCID then
                    --double val = CClientMainPlayer.Inst.FightProp.GetParam(EPlayerFightProp.MF)+GetVal(buff.Display);
                    --double add = CClientFormula.Inst.Formula_200(val);
                    this.nextLevel.text = buff.Display .. System.String.Format(CLivingSkillEffectDisplay.DisplayFormat, this:GetAddBaolv(buff))
                end
            else
                this.nextLevel.text = ""
            end
        end
    else
        local data = LifeSkill_Skill.GetData(skillClsId * 100 + level)

        if data ~= nil then
            local buff = Buff_Buff.GetData(data.Effect)
            local preAdd = 0
            if buff ~= nil then
                this.curLevel.text = buff.Display
                if skillClsId == CLivingSkillEffectDisplay.WGYCID then
                    --double val = GetVal(buff.Display);// CClientMainPlayer.Inst.FightProp.GetParam(EPlayerFightProp.MF);
                    --int add = (int)(CClientFormula.Inst.Formula_200(val) * 100);
                    this.curLevel.text = buff.Display .. System.String.Format(CLivingSkillEffectDisplay.DisplayFormat, this:GetAddBaolv(buff))
                    preAdd = this:GetVal(buff.Display)
                end
            end
            buff = Buff_Buff.GetData(data.Effect + 1)
            --下一级的效果
            if buff ~= nil then
                this.nextLevel.text = buff.Display
                if skillClsId == CLivingSkillEffectDisplay.WGYCID then
                    --double val = GetVal(buff.Display); // CClientMainPlayer.Inst.FightProp.GetParam(EPlayerFightProp.MF) + GetVal(buff.Display) - preAdd;
                    --int add = (int)(CClientFormula.Inst.Formula_200(val)*100);
                    this.nextLevel.text = buff.Display .. System.String.Format(CLivingSkillEffectDisplay.DisplayFormat, this:GetAddBaolv(buff))
                end
            else
                this.nextLevel.text = ""
            end
        end
    end
end
CLivingSkillEffectDisplay.m_GetAddBaolv_CS2LuaHook = function (this, buff) 
    local val = this:GetVal(buff.Display)
    -- CClientMainPlayer.Inst.FightProp.GetParam(EPlayerFightProp.MF);
    local add = math.floor((CClientFormula.Inst:Formula_200(val) * 100))
    return add
end
