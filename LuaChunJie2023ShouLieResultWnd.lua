local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CItemMgr = import "L10.Game.CItemMgr"
local Animation = import "UnityEngine.Animation"

LuaChunJie2023ShouLieResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2023ShouLieResultWnd, "WinStyle", GameObject)
RegistChildComponent(LuaChunJie2023ShouLieResultWnd, "TimeLabel", UILabel)
RegistChildComponent(LuaChunJie2023ShouLieResultWnd, "FullLabel", GameObject)

RegistChildComponent(LuaChunJie2023ShouLieResultWnd, "LoseStyle", GameObject)
RegistChildComponent(LuaChunJie2023ShouLieResultWnd, "RestartBtn", GameObject)

RegistChildComponent(LuaChunJie2023ShouLieResultWnd, "Grid", UIGrid)
RegistChildComponent(LuaChunJie2023ShouLieResultWnd, "ItemCell", GameObject)

RegistClassMember(LuaChunJie2023ShouLieResultWnd, "animation")
--@endregion RegistChildComponent end

function LuaChunJie2023ShouLieResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.RestartBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRestartBtnClick()
    end)
    self.ItemCell:SetActive(false)
end

function LuaChunJie2023ShouLieResultWnd:Init()
    self.animation = self.transform:GetComponent(typeof(Animation))
    if LuaChunJie2023Mgr.SLTT_isWin then
        self:InitWinStyle()
    else
        self:InitLoseStyle()
    end
end

function LuaChunJie2023ShouLieResultWnd:InitWinStyle()
    self.animation:Play("chunjie2023shoulieresultwnd_win")
    self.TimeLabel.text = SafeStringFormat3(LocalString.GetString("%d:%d"), math.floor(LuaChunJie2023Mgr.SLTT_Time/60), LuaChunJie2023Mgr.SLTT_Time%60)

    if LuaChunJie2023Mgr.SLTT_Rewards == nil or #LuaChunJie2023Mgr.SLTT_Rewards == 0 then
        self.Grid.gameObject:SetActive(false)
        self.FullLabel:SetActive(true)
    else
        self.Grid.gameObject:SetActive(true)
        self.FullLabel:SetActive(false)
        -- 奖励内容
        for i, itemId in pairs(LuaChunJie2023Mgr.SLTT_Rewards) do
            local go = NGUITools.AddChild(self.Grid.gameObject, self.ItemCell)
            go:GetComponent(typeof(CQnReturnAwardTemplate)):Init(CItemMgr.Inst:GetItemTemplate(itemId), 1)
            go.gameObject:SetActive(true)
        end
        self.Grid:Reposition()
    end
end

function LuaChunJie2023ShouLieResultWnd:InitLoseStyle()
    self.animation:Play("chunjie2023shoulieresultwnd_lose")
end

--@region UIEvent
function LuaChunJie2023ShouLieResultWnd:OnRestartBtnClick()
    CUIManager.ShowUI(CLuaUIResources.ChunJie2023ShouLieEnterWnd)
    CUIManager.CloseUI(CLuaUIResources.ChunJie2023ShouLieResultWnd)
end
--@endregion UIEvent

