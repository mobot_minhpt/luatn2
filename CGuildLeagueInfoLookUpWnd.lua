-- Auto Generated!!
local CGuildLeagueGroupItem = import "L10.UI.CGuildLeagueGroupItem"
local CGuildLeagueGuildInfoItem = import "L10.UI.CGuildLeagueGuildInfoItem"
local CGuildLeagueInfoLookUpWnd = import "L10.UI.CGuildLeagueInfoLookUpWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local Gac2Gas = import "L10.Game.Gac2Gas"
CGuildLeagueInfoLookUpWnd.m_Init_CS2LuaHook = function (this) 
    this.groupTableView.m_DataSource = this
    this.memberTableView.m_DataSource = this

    this.groupTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row) 
        if this.mGuildInfoList ~= nil then
            CommonDefs.ListClear(this.mGuildInfoList)
        end
        this.memberTableView:ReloadData(true, true)
        local group = (TypeAs(this.groupTableView:GetItemAtRow(row), typeof(CGuildLeagueGroupItem))).group
        Gac2Gas.QueryGLLevelInfo(group)
    end)
    this.groupTableView:ReloadData(true, true)
    this.groupTableView:SetSelectRow(0, true)
end
CGuildLeagueInfoLookUpWnd.m_NumberOfRows_CS2LuaHook = function (this, view) 
    if view == this.groupTableView then
        return 22
    else
        if this.mGuildInfoList ~= nil then
            return this.mGuildInfoList.Count
        end
        return 0
    end
end
CGuildLeagueInfoLookUpWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if view == this.groupTableView then
        local item = TypeAs(view:GetFromPool(0), typeof(CGuildLeagueGroupItem))
        item:Init((row + 1))
        return item
    else
        if this.mGuildInfoList ~= nil then
            if this.mGuildInfoList.Count > row then
                local item = TypeAs(view:GetFromPool(0), typeof(CGuildLeagueGuildInfoItem))
                item:Init(this.mGuildInfoList[row], row)
                return item
            end
        end
        return nil
    end
end
