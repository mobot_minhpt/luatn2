local CScene = import "L10.Game.CScene"
local CPos = import "L10.Engine.CPos"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CLoadingWnd = import "L10.UI.CLoadingWnd"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local SdkU3d = import "NtUniSdk.Unity3d.SdkU3d"
local JsonMapper = import "JsonMapper"
local AdvertU3d = import "NtUniSdk.Unity3d.AdvertU3d"
local AdvertConstProp = import "NtUniSdk.Unity3d.AdvertConstProp"
local CWelfareBonusMgr = import "L10.Game.CWelfareBonusMgr"

LuaSEASdkMgr = {}

LuaSEASdkMgr.OnExtendFuncCallAction = DelegateFactory.Action_int_Dictionary_string_object(function(code, data)
    if CommonDefs.DictContains_LuaCall(data, "methodId") then
        if data["methodId"] == "vngCountryCode" then
            if CommonDefs.DictContains_LuaCall(data, "countryCode") then
                --IOS接口
                LuaSEASdkMgr:OnGetCountryCodeResult(data["countryCode"])
            end
        elseif data["methodId"] == "GetCachedCountryCode" then
            --PC接口
            if CommonDefs.DictContains_LuaCall(data, "countryCode") then
                LuaSEASdkMgr:OnGetCountryCodeResult(data["countryCode"])
            end
        elseif data["methodId"] == "GoogleTranslate" then
            if CommonDefs.DictContains_LuaCall(data, "result") and CommonDefs.DictContains_LuaCall(data, "inputText") then
                LuaSEASdkMgr:OnGetGoogleTranslateResult(data["inputText"], data["result"])
            end

        elseif data["methodId"] == "vngProductInfo" then
            if CommonDefs.DictContains_LuaCall(data, "result") then
                LuaSEASdkMgr:GetProductPrice(data["result"])
            end
        end
    end
end)

LuaSEASdkMgr.OnUpdateLevel = DelegateFactory.Action(function()
    local playerLevel = 0
    if CClientMainPlayer.Inst ~= nil then
        playerLevel = CClientMainPlayer.Inst.MaxLevel
    end
    LuaSEASdkMgr:AppsNotifyLevel(playerLevel)
end)

LuaSEASdkMgr.OnQueryGoogleTranslate = DelegateFactory.Action_string(function (value)
    LuaSEASdkMgr:QueryGoogleTranslate(value)
end)

function LuaSEASdkMgr:OnAddPlayerAchievement(args)
    local achievementId = args[0]
    LuaSEASdkMgr:AppsNotifyAchievement(achievementId)
end

LuaSEASdkMgr.OnFinishTask = DelegateFactory.Action_uint(function(taskId)
    LuaSEASdkMgr:AppsNotifyTask(taskId)
end)

function LuaSEASdkMgr:AddListener()
    EventManager.AddListenerInternal(EnumEventType.UnisdkOnExtendFuncCall, LuaSEASdkMgr.OnExtendFuncCallAction)
    EventManager.AddListenerInternal(EnumEventType.QueryGoogleTranslate, LuaSEASdkMgr.OnQueryGoogleTranslate)
    EventManager.AddListenerInternal(EnumEventType.MainPlayerLevelChange, LuaSEASdkMgr.OnUpdateLevel)
    g_ScriptEvent:AddListener("AddPlayerAchievement", self, "OnAddPlayerAchievement")
    EventManager.AddListenerInternal(EnumEventType.FinishTask, LuaSEASdkMgr.OnFinishTask)
    g_ScriptEvent:AddListener("ItemPropUpdated", self, "OnChargePropUpdated")
    g_ScriptEvent:AddListener("SyncVNClientAwardResult", self, "OnSyncVNClientAwardResult")
end

LuaSEASdkMgr:AddListener()
function LuaSEASdkMgr:QueryProductPrice(productIdList)
    local productStr = ""
    for i = 1, #productIdList do
        if i == 1 then
            productStr = SafeStringFormat3("\"%s\"", productIdList[i])
        else
            productStr = SafeStringFormat3("%s,\"%s\"", productStr, productIdList[i])
        end
    end
    local jsonStr = SafeStringFormat3("{\"methodId\":\"vngProductInfo\",\"items\":[%s],\"encoding\":\"NSUTF8StringEncoding\"}",
            productStr)
    SdkU3d.ntExtendFunc(jsonStr)
end

function LuaSEASdkMgr:GetProductPrice(resultData)
    if self.cachePriceData == nil then
        self.cachePriceData = {}
    end
    if CommonDefs.IsIOSPlatform() then
        CommonDefs.ListIterate(resultData, DelegateFactory.Action_object(function (info)
            self.cachePriceData[info.itemID] = info.price .. " " .. info.currency
        end))
    elseif CommonDefs.IsAndroidPlatform() then
        for currency, itemID, price in resultData:gmatch('{"currency":"(.-)","itemID":"(.-)","price":"(.-)"') do
            self.cachePriceData[itemID] = price .. " " .. currency
        end
    end
end

function LuaSEASdkMgr:AddProductPrice(itemId, priceAndCurrency)
    if self.cachePriceData == nil then
        self.cachePriceData = {}
    end
    self.cachePriceData[itemId] = priceAndCurrency
end

function LuaSEASdkMgr:QueryGoogleTranslate(inputText)
    local currentLanguage = LocalString.language
    inputText = inputText:gsub('"', '\\"')
    local inputLanguageTbl = {cn = "zh", vn = "vi", en = "en", th = "th", ina = "id"}
    local jsonStr = SafeStringFormat3("{\"methodId\":\"GoogleTranslate\",\"channel\":\"vng_vie\",\"inputText\":\"%s\",\"inputLanguage\":\"%s\"}",
            inputText, inputLanguageTbl[currentLanguage])
    SdkU3d.ntExtendFunc(jsonStr)
end 

function LuaSEASdkMgr:OnGetGoogleTranslateResult(inputText, resultText)
    EventManager.BroadcastInternalForLua(EnumEventType.OnGetGoogleTranslateResult, {inputText, resultText})
end

function LuaSEASdkMgr:AddChatTranslateRecord(id, isWaitTranslate, sourceString, targetString)
    if self.chatTranslateRecord == nil then
        self.chatTranslateRecord = {}
    end
    self.chatTranslateRecord[id] = {isWaitTranslate = isWaitTranslate, 
                                    sourceString = sourceString, 
                                    targetString = targetString}
end

function LuaSEASdkMgr:QueryChatTranslateRecord(id)
    if self.chatTranslateRecord == nil then
        return "nil"
    end
    --return self.chatTranslateRecord[id] and self.chatTranslateRecord[id] or "nil"
    local data = self.chatTranslateRecord[id]
    if data then
        return tostring(data.isWaitTranslate) .. "^" .. self.chatTranslateRecord[id].sourceString
            .. "^" .. self.chatTranslateRecord[id].targetString
    else
        return "nil"
    end
end

function LuaSEASdkMgr:FaceBookTracking(eventName, eventData)
    local jsonStr = string.format("{\"methodId\": \"FaceBookTracking\", \"eventname\":\"%s\", \"eventJson\": \"%s\"}", eventName, eventData)
    SdkU3d.ntExtendFunc(jsonStr)
end

function LuaSEASdkMgr:AppsflyerNotify(eventName, jsonStr)
    local param = JsonMapper.ToObject(jsonStr)
    AdvertU3d.trackEvent(eventName, param)
end 

function LuaSEASdkMgr:AppsNotifyLevel(level)
    if self.notifyLevel == nil then
        self.notifyLevel = {}
        local data = GameSetting_Client.GetData().SEA_APPSFYER_LEVEL
        local split = g_LuaUtil:StrSplit(data, ",")
        for i = 1, #split do
            self.notifyLevel[tonumber(split[i])] = true
        end
    end
    
    if self.notifyLevel[level] then
        self:AppsflyerNotify("LevelUp_" .. level, SafeStringFormat3("{}"))
    end
end

function LuaSEASdkMgr:AppsNotifyVipLevel(vipLevel)
    if self.notifyVipLevel == nil then
        self.notifyVipLevel = {}
        local data = GameSetting_Client.GetData().SEA_APPSFYER_VIPLEVEL
        local split = g_LuaUtil:StrSplit(data, ",")
        for i = 1, #split do
            self.notifyVipLevel[tonumber(split[i])] = true
        end
    end

    if self.notifyVipLevel[vipLevel] then
        self:AppsflyerNotify("VipLevelUp_" .. vipLevel, SafeStringFormat3("{}"))
    end
end 

function LuaSEASdkMgr:AppsNotifyAchievement(achievementId)
    if self.notifyAchievement == nil then
        self.notifyAchievement = {}
        local data = GameSetting_Client.GetData().SEA_APPSFYER_ACHIEVEMENT
        local split = g_LuaUtil:StrSplit(data, ",")
        for i = 1, #split do
            self.notifyAchievement[tonumber(split[i])] = true
        end
    end

    if self.notifyAchievement[achievementId] then
        self:AppsflyerNotify("GetAchievement_" .. achievementId, SafeStringFormat3("{}"))
    end
end

function LuaSEASdkMgr:AppsNotifyTask(taskId)
    if self.notifyTask == nil then
        self.notifyTask = {}
        local data = GameSetting_Client.GetData().SEA_APPSFYER_TASK
        local split = g_LuaUtil:StrSplit(data, ",")
        for i = 1, #split do
            self.notifyTask[tonumber(split[i])] = true
        end
    end

    if self.notifyTask[taskId] then
        self:AppsflyerNotify("FinishTask_" .. taskId, SafeStringFormat3("{}"))
    end
end

function LuaSEASdkMgr:OnChargePropUpdated()
    if CClientMainPlayer.Inst ~= nil then
        local totalCharge = CClientMainPlayer.Inst.ItemProp.Vip.TotalCharge
        if not (self.recordTotalCharge == nil) then
            if totalCharge > self.recordTotalCharge then
                self:AppsflyerNotify("PU", SafeStringFormat3("{\"totalCharge\":%d}", totalCharge-self.recordTotalCharge))
            end
        end
        self.recordTotalCharge = totalCharge
    end
end

function LuaSEASdkMgr:QueryCountryCode()
    if CommonDefs.Is_PC_PLATFORM() then
        SdkU3d.ntExtendFunc("{\"methodId\":\"GetCachedCountryCode\", \"channel\":\"vng_vie\"}")
    else
        if CommonDefs.IsAndroidPlatform() then
            SdkU3d.ntExtendFunc("{\"methodId\":\"vngCountryCode\"}")
        elseif CommonDefs.IsIOSPlatform() then
            SdkU3d.ntExtendFunc("{\"methodId\":\"vngCountryCode\"}")
        end
    end
end

function LuaSEASdkMgr:ConvertCode(oriCountryCode)
    if oriCountryCode == "PH" then
        return 1
    elseif oriCountryCode == "MY" then
        return 2
    elseif oriCountryCode == "TH" then
        return 3
    elseif oriCountryCode == "SG" then
        return 4
    elseif oriCountryCode == "VN" then
        return 5
    elseif oriCountryCode == "ID" then
        return 6
    else
        return 7
    end
end

function LuaSEASdkMgr:OnGetCountryCodeResult(oriCountryCode)
    g_ScriptEvent:BroadcastInLua("OnGetCountryCode", LuaSEASdkMgr:ConvertCode(oriCountryCode))
end

function LuaSEASdkMgr:GetFlagSpriteName(countryCode)
    local flagTbl = {}
    flagTbl[1] = "seaflag_feilvbin"
    flagTbl[2] = "seaflag_malaixiya"
    flagTbl[3] = "seaflag_taiguo"
    flagTbl[4] = "seaflag_xinjiapo"
    flagTbl[5] = "seaflag_yuenan"
    flagTbl[6] = "seaflag_yinni"
    if flagTbl[countryCode] then
        return flagTbl[countryCode]
    end

    if countryCode == 0 then
        return ""
    else
        return "seaflag_qita"
    end
end 

function LuaSEASdkMgr:OnPlayerLogin()
    self.loginTime = nil
    self.rewardData = nil
end

function LuaSEASdkMgr:IsVoteAppOpen()
    --领奖数据第一次同步
    if self.rewardData == nil then
        Gac2Gas.QueryVNClientAward()
        return false
    end

    --已领过奖
    if (self.rewardData["AV"] and self.rewardData["AV"] == 1 or false) then
        return false
    end
    
    --loginTime第一次同步
    if self.loginTime == nil or self.onlineSeconds == nil then
        Gac2Gas.QueryDayOnlineTime()
        return false
    end
    
    local nowTimeStamp = CServerTimeMgr.Inst.timeStamp
    local loginTime = self.loginTime
    if nowTimeStamp - loginTime + self.onlineSeconds >= tonumber(GameSetting_Client.GetData().SEA_VOTE_RED_DOT_TIME) * 60 then
        return true
    end
    return false
end

function LuaSEASdkMgr:OnSyncDayOnlineTimeResult(onlineTime, loginTimeStamp)
    self.loginTime = loginTimeStamp
    self.onlineSeconds = onlineTime
end

function LuaSEASdkMgr:OnSyncOneVNClientAwardResult(key, state)
    if self.rewardData == nil then
        self.rewardData = {}
    end
    self.rewardData[key] = state
end

function LuaSEASdkMgr:OnSyncVNClientAwardResult(dataU)
    self.rewardData = {}
    local tb = g_MessagePack.unpack(dataU)
    for k,v in pairs(tb) do
        self.rewardData[k] = v
    end
end 

function LuaSEASdkMgr:IsSocialFollowShowAlert()
    local showAlert = false
    if self.rewardData == nil then
        Gac2Gas.QueryVNClientAward()
    else
        local socialKeyList = {"Facebook_Fanpage_SEA_SOCIAL_INFO", "Facebook_Group_SEA_SOCIAL_INFO", "Youtube_SEA_SOCIAL_INFO", "Discord_SEA_SOCIAL_INFO", "TikTok_SEA_SOCIAL_INFO"}
        for i = 1, #socialKeyList do
            local key = socialKeyList[i]
            local data = GameSetting_Client.GetData()[key]
            local dataSplit = g_LuaUtil:StrSplit(data, ";")
            local awardKey = dataSplit[5]
            if not(self.rewardData[awardKey] and self.rewardData[awardKey] == 1 or false) then
                showAlert = true
            end
        end
    end
    return showAlert
end

function LuaSEASdkMgr:GetMediaSource()
    if CommonDefs.Is_PC_PLATFORM() then
        return "PC"
    else
        return ""
    end
end 

function LuaSEASdkMgr:GetCampaignId()
    if CommonDefs.Is_PC_PLATFORM() then
        return "PC"
    else
        return ""
    end
end 