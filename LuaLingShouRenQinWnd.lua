local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UITable = import "UITable"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local Object = import "System.Object"

LuaLingShouRenQinWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaLingShouRenQinWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaLingShouRenQinWnd, "StarsRoot", "StarsRoot", GameObject)
RegistChildComponent(LuaLingShouRenQinWnd, "ProgressLabel", "ProgressLabel", UILabel)
RegistChildComponent(LuaLingShouRenQinWnd, "StartButton", "StartButton", GameObject)
RegistChildComponent(LuaLingShouRenQinWnd, "CountDown", "CountDown", GameObject)
RegistChildComponent(LuaLingShouRenQinWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaLingShouRenQinWnd, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaLingShouRenQinWnd, "CardTemplate", "CardTemplate", GameObject)
RegistChildComponent(LuaLingShouRenQinWnd, "CenterTable1", "CenterTable1", UITable)
RegistChildComponent(LuaLingShouRenQinWnd, "CenterTable2", "CenterTable2", UITable)
RegistChildComponent(LuaLingShouRenQinWnd, "CenterTable3", "CenterTable3", UITable)
RegistChildComponent(LuaLingShouRenQinWnd, "CenterTable4", "CenterTable4", UITable)
RegistChildComponent(LuaLingShouRenQinWnd, "CenterTable5", "CenterTable5", UITable)

--@endregion RegistChildComponent end
RegistClassMember(LuaLingShouRenQinWnd,"m_CenterTable")
RegistClassMember(LuaLingShouRenQinWnd,"m_MaxTimes")
RegistClassMember(LuaLingShouRenQinWnd,"m_Times")
RegistClassMember(LuaLingShouRenQinWnd,"m_ObjsList")
RegistClassMember(LuaLingShouRenQinWnd,"m_RandomIdsList")
RegistClassMember(LuaLingShouRenQinWnd,"m_LastIndex")
RegistClassMember(LuaLingShouRenQinWnd,"m_CountDownTime")
RegistClassMember(LuaLingShouRenQinWnd,"m_CountDownTick")
RegistClassMember(LuaLingShouRenQinWnd,"m_CloseWndTime")
RegistClassMember(LuaLingShouRenQinWnd,"m_CloseWndTick")
RegistClassMember(LuaLingShouRenQinWnd,"m_SuccessCardsNum")
RegistClassMember(LuaLingShouRenQinWnd,"m_CardsNum")
RegistClassMember(LuaLingShouRenQinWnd,"m_TaskId")
RegistClassMember(LuaLingShouRenQinWnd,"m_SuccessCardsDict")
RegistClassMember(LuaLingShouRenQinWnd,"m_ReCoverCardTick")
RegistClassMember(LuaLingShouRenQinWnd,"m_IsReCovering")

function LuaLingShouRenQinWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.StartButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnStartButtonClick()
	end)


    --@endregion EventBind end
end

function LuaLingShouRenQinWnd:Init()
	self.CardTemplate.gameObject:SetActive(false)
	self.CountDown.gameObject:SetActive(false)
	self.StartButton.gameObject:SetActive(true)
	for i = 0, self.StarsRoot.transform.childCount - 1 do
		local obj = self.StarsRoot.transform:GetChild(i).gameObject
		obj:GetComponent(typeof(UISprite)).enabled = i < LuaInviteNpcMgr.m_LingShouRenQinDifficultyLevel
	end
	local data = SectInviteNpc_LingShouRenQing.GetData(LuaInviteNpcMgr.m_LingShouRenQinDifficultyLevel)
	if not data then 
		CUIManager.CloseUI(CLuaUIResources.LingShouRenQinWnd)
		return
	end
	self.m_CountDownTime = data.ShowTime
	self.m_CloseWndTime = data.Duration
	self.m_TaskId = data.TaskId
	self.m_Times = 0
	self.m_MaxTimes = data.OperateTimesLimit
	self:ShowProgressLabel()
	self:InitCards(data)
end

function LuaLingShouRenQinWnd:OnDisable()
	self:CancelCloseWndTick()
	self:CancelCountDownTick()
	self:CancelReCoverCardTick()
end

function LuaLingShouRenQinWnd:InitCards(data)
	local centerTableArray = {self.CenterTable1, self.CenterTable2, self.CenterTable3, self.CenterTable4,self.CenterTable5}
	self.m_CenterTable = centerTableArray[LuaInviteNpcMgr.m_LingShouRenQinDifficultyLevel]
	for i = 1, #centerTableArray do
		centerTableArray[i].gameObject:SetActive(i == LuaInviteNpcMgr.m_LingShouRenQinDifficultyLevel)
	end
	self.m_ObjsList = {}
	self.m_RandomIdsList = {}
	local len = data.LingShouTypeCount * 2
	self.m_CardsNum = len
	local lingshouIDs = SectInviteNpc_Setting.GetData().LingshouID
	local lingshouIDList = {}
	for i = 0, lingshouIDs.Length - 1 do
		table.insert(lingshouIDList,lingshouIDs[i])
	end
	for i = 1, data.LingShouTypeCount do
		local randomIndex = math.random(#lingshouIDList)
		table.insert(self.m_RandomIdsList,lingshouIDList[randomIndex])
		table.insert(self.m_RandomIdsList,lingshouIDList[randomIndex])
		lingshouIDList[randomIndex] = lingshouIDList[#lingshouIDList]
		table.remove(lingshouIDList, #lingshouIDList)
	end
	for i = 1, len do
		local randomIndex = math.random(i)
		local tmp = self.m_RandomIdsList[i]
		self.m_RandomIdsList[i] = self.m_RandomIdsList[randomIndex]
		self.m_RandomIdsList[randomIndex] = tmp
	end
	Extensions.RemoveAllChildren(self.m_CenterTable.transform)
	for i = 1, len do
		local obj = NGUITools.AddChild(self.m_CenterTable.gameObject, self.CardTemplate.gameObject)
		obj.gameObject:SetActive(true)
		obj.transform:Find("Portrait"):GetComponent(typeof(CUITexture)):LoadMaterial(nil)
		table.insert(self.m_ObjsList,obj)
	end
	self.m_CenterTable:Reposition()
end

function LuaLingShouRenQinWnd:ShowProgressLabel()
	self.ProgressLabel.text = SafeStringFormat3("%02d/%02d",self.m_Times, self.m_MaxTimes)
end

function LuaLingShouRenQinWnd:ShowCountDownLabel()
	self.TimeLabel.text = SafeStringFormat3("%02d:%02d",math.floor((self.m_CountDownTime % 3600) / 60), self.m_CountDownTime % 60)
end

function LuaLingShouRenQinWnd:CancelCloseWndTick()
	if self.m_CloseWndTick then
		UnRegisterTick(self.m_CloseWndTick)
		self.m_CloseWndTick = nil
	end
end

function LuaLingShouRenQinWnd:CancelCountDownTick()
	if self.m_CountDownTick then
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end
end

function LuaLingShouRenQinWnd:CancelReCoverCardTick()
	if self.m_ReCoverCardTick then
		UnRegisterTick(self.m_ReCoverCardTick)
		self.m_ReCoverCardTick = nil
	end
end

--@region UIEvent

function LuaLingShouRenQinWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("LingShouRenQinWnd_ReadMe")
end

function LuaLingShouRenQinWnd:OnStartButtonClick()
	self.CountDown.gameObject:SetActive(true)
	self.StartButton.gameObject:SetActive(false)
	self.CountDownLabel.text = LocalString.GetString("倒计时")
	self:CancelCountDownTick()
	self:ShowCountDownLabel()
	for index,obj in pairs(self.m_ObjsList) do
		local lingShouData = LingShou_LingShou.GetData(self.m_RandomIdsList[index])
		obj.transform:Find("Portrait"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(lingShouData.Portrait)
		obj.transform:Find("Texture_Back").gameObject:SetActive(false)
	end
	self.m_CountDownTick = RegisterTick(function ()
		self.m_CountDownTime = self.m_CountDownTime - 1
		self:ShowCountDownLabel()
		if self.m_CountDownTime <= 0 then
			self:CancelCountDownTick()
			self:StartGame()
		end
	end, 1000)
end

function LuaLingShouRenQinWnd:OnCardClick(index)
	if self.m_LastIndex and self.m_LastIndex == index then
		return
	end
	if self.m_SuccessCardsDict[index] then return end
	if self.m_IsReCovering then return end

	local obj = self.m_ObjsList[index]
	local lingShouData = LingShou_LingShou.GetData(self.m_RandomIdsList[index])
	obj.transform:Find("Portrait"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(lingShouData.Portrait)
	obj.transform:Find("Texture_Back").gameObject:SetActive(false)

	if self.m_LastIndex then
		local lastObj = self.m_ObjsList[self.m_LastIndex]
		if self.m_RandomIdsList[self.m_LastIndex] == self.m_RandomIdsList[index] then
			self.m_SuccessCardsNum = self.m_SuccessCardsNum + 2
			self.m_SuccessCardsDict[self.m_LastIndex] = true
			self.m_SuccessCardsDict[index] = true
			obj.transform:Find("Fx"):GetComponent(typeof(CUIFx)):LoadFx("fx/ui/prefab/UI_dagongshouce_shengji.prefab")
			lastObj.transform:Find("Fx"):GetComponent(typeof(CUIFx)):LoadFx("fx/ui/prefab/UI_dagongshouce_shengji.prefab")
			if self.m_CardsNum == self.m_SuccessCardsNum then
				self:OnSuccess()
			end
		else
			self:ReCoverCard(obj,lastObj)
		end
		self.m_Times = self.m_Times + 1
		self:ShowProgressLabel()
		self.m_LastIndex = nil
	else
		self.m_LastIndex = index
	end

	if self.m_Times >= self.m_MaxTimes and self.m_CardsNum ~= self.m_SuccessCardsNum then
		g_MessageMgr:ShowMessage("LingShouRenQinWnd_OperateTimesLimit")
		CUIManager.CloseUI(CLuaUIResources.LingShouRenQinWnd)
	end
end
--@endregion UIEvent

function LuaLingShouRenQinWnd:StartGame()
	self.m_SuccessCardsDict = {}
	self.m_SuccessCardsNum = 0
	self.CountDownLabel.text = LocalString.GetString("倒计时")
	for index,obj in pairs(self.m_ObjsList) do
		local lingShouData = LingShou_LingShou.GetData(self.m_RandomIdsList[index])
		obj.transform:Find("Portrait"):GetComponent(typeof(CUITexture)):LoadMaterial(nil)
		obj.transform:Find("Texture_Back").gameObject:SetActive(true)
	end
	for i = 1, self.m_CardsNum do
		local j = i
		local obj = self.m_ObjsList[j]
		UIEventListener.Get(obj.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnCardClick(j)
		end)
	end
	self.m_CountDownTime = self.m_CloseWndTime
	self:ShowCountDownLabel()
	self:CancelCloseWndTick()
	self.m_CloseWndTick = RegisterTick(function()
		self.m_CountDownTime = self.m_CountDownTime - 1
		self:ShowCountDownLabel()
		if self.m_CountDownTime <= 0 then
			g_MessageMgr:ShowMessage("LingShouRenQinWnd_OverTime")
			CUIManager.CloseUI(CLuaUIResources.LingShouRenQinWnd)
		end
	end,1000)
end

function LuaLingShouRenQinWnd:ReCoverCard(obj,lastObj)
	self.m_IsReCovering = true
	self:CancelReCoverCardTick()
	self.m_ReCoverCardTick = RegisterTickOnce(function ()
		self.m_IsReCovering = false
		self:CancelReCoverCardTick()
		obj.transform:Find("Portrait"):GetComponent(typeof(CUITexture)):LoadMaterial(nil)
		lastObj.transform:Find("Portrait"):GetComponent(typeof(CUITexture)):LoadMaterial(nil)
		obj.transform:Find("Texture_Back").gameObject:SetActive(true)
		lastObj.transform:Find("Texture_Back").gameObject:SetActive(true)
	end, 300)
end

function LuaLingShouRenQinWnd:OnSuccess()
	local empty = CreateFromClass(MakeGenericClass(List, Object))
	empty = MsgPackImpl.pack(empty)
	Gac2Gas.FinishClientTaskEventWithConditionId(self.m_TaskId, "LingShouRenQing", empty,LuaInviteNpcMgr.m_LingShouRenQinDifficultyLevel)
	self:CancelCloseWndTick()
	EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {CUIFxPaths.TaskFinishedFxPath})
	self.m_CloseWndTick = RegisterTick(function()
		CUIManager.CloseUI(CLuaUIResources.LingShouRenQinWnd)
	end,3000)
end
