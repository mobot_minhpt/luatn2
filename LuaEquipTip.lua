local UIRoot = import "UIRoot"
local UIRect = import "UIRect"
local UIBasicSprite = import "UIBasicSprite"
local Screen = import "UnityEngine.Screen"
local QualityColor = import "L10.Game.QualityColor"
local Profession = import "L10.Game.Profession"
local PackageItemInfo = import "L10.UI.CItemInfoMgr+PackageItemInfo"
local NGUIText = import "NGUIText"
local NGUIMath = import "NGUIMath"
local MsgPackImpl = import "MsgPackImpl"
local IdPartition = import "L10.Game.IdPartition"
local Extensions = import "Extensions"
local EPropStatus = import "L10.Game.EPropStatus"
local EnumQualityType = import "L10.Game.EnumQualityType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumClass = import "L10.Game.EnumClass"
local EnumBodyPosition = import "L10.Game.EnumBodyPosition"
local CWeddingMgr = import "L10.Game.CWeddingMgr"
local CTitleInfoRow = import "L10.UI.CTitleInfoRow"
local CTalismanMgr = import "L10.Game.CTalismanMgr"
local CStatusMgr = import "L10.Game.CStatusMgr"
local CShenBingMgr = import "CShenBingMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CPropertyTextInfoRow = import "L10.UI.CPropertyTextInfoRow"
local CPerfectionInfoRow = import "L10.UI.CPerfectionInfoRow"
local Color = import "UnityEngine.Color"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemAndEquipTipButton = import "L10.UI.CItemAndEquipTipButton"
local CImageTextInfoRow = import "L10.UI.CImageTextInfoRow"
local CEquipScoreInfoRow = import "L10.UI.CEquipScoreInfoRow"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CEquipment = import "L10.Game.CEquipment"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChatLinkMgr = import "CChatLinkMgr"
local CBaseItemInfoRow = import "L10.UI.CBaseItemInfoRow"
local BodyItemInfo = import "L10.UI.CItemInfoMgr+BodyItemInfo"
local CEquipExtraPropertyFrame=import "L10.UI.CEquipExtraPropertyFrame"
local Talisman_Suit=import "L10.Game.Talisman_Suit"--函数传参用到
local GameSetting_Common_Wapper=import "L10.Game.GameSetting_Common_Wapper"
local Skill_AllSkills=import "L10.Game.Skill_AllSkills"
local CGemSlot = import "L10.UI.CGemSlot"
--local DuanWu_Setting = import "L10.Game.DuanWu_Setting"
local ItemsStringActionKeyValuePair = import "L10.Game.ItemsStringActionKeyValuePair"
local CButton = import "L10.UI.CButton"
local CPUBGMgr = import "L10.Game.CPUBGMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CCraveData = import "L10.Game.CCraveData"

CLuaEquipTip = class()
RegistClassMember(CLuaEquipTip,"background")
RegistClassMember(CLuaEquipTip,"iconTexture")
RegistClassMember(CLuaEquipTip,"nameLabel")
RegistClassMember(CLuaEquipTip,"availableLevelLabel")
RegistClassMember(CLuaEquipTip,"categoryLabel")
RegistClassMember(CLuaEquipTip,"bindSprite")
RegistClassMember(CLuaEquipTip,"disableSprite")
RegistClassMember(CLuaEquipTip,"qualitySprite")
RegistClassMember(CLuaEquipTip,"perfectionTemplate")
RegistClassMember(CLuaEquipTip,"titleLabelTemplate")
RegistClassMember(CLuaEquipTip,"labelTemplate")
RegistClassMember(CLuaEquipTip,"equipScoreTemplate")
RegistClassMember(CLuaEquipTip,"propertyLabelTemplate")
RegistClassMember(CLuaEquipTip,"basicPropertyTemplate")
RegistClassMember(CLuaEquipTip,"imageLabelTemplate")
RegistClassMember(CLuaEquipTip,"topTenTemplate")
RegistClassMember(CLuaEquipTip,"emptyTemplate")
RegistClassMember(CLuaEquipTip,"switchSplit")
RegistClassMember(CLuaEquipTip,"normalSplit")
RegistClassMember(CLuaEquipTip,"titleLabelButtonTemplate")
RegistClassMember(CLuaEquipTip,"titleLabelButtonTemplate2")
RegistClassMember(CLuaEquipTip,"contentTable")
RegistClassMember(CLuaEquipTip,"contentScrollView")
RegistClassMember(CLuaEquipTip,"infoBtn")
RegistClassMember(CLuaEquipTip,"prevBtn")
RegistClassMember(CLuaEquipTip,"nextBtn")
RegistClassMember(CLuaEquipTip,"scrollViewIndicator")
RegistClassMember(CLuaEquipTip,"minContentWidth")
RegistClassMember(CLuaEquipTip,"maxContentWidth")
RegistClassMember(CLuaEquipTip,"NameColor")
RegistClassMember(CLuaEquipTip,"ValueColor")
RegistClassMember(CLuaEquipTip,"buttonTable")
RegistClassMember(CLuaEquipTip,"buttonTemplate")
RegistClassMember(CLuaEquipTip,"buttonsBg")
RegistClassMember(CLuaEquipTip,"extraPropertyFrame")
RegistClassMember(CLuaEquipTip,"baseItemInfo")
RegistClassMember(CLuaEquipTip,"mbShowFeiShengAttributes")
RegistClassMember(CLuaEquipTip,"titleColor")
RegistClassMember(CLuaEquipTip,"weddingColor")
RegistClassMember(CLuaEquipTip,"weddingContentColor")
RegistClassMember(CLuaEquipTip,"feishengTitleColor")
RegistClassMember(CLuaEquipTip,"mComparedItems")
RegistClassMember(CLuaEquipTip,"m_SwitchWordSetBotton")
RegistClassMember(CLuaEquipTip,"m_SwitchHoleSetBotton")
RegistClassMember(CLuaEquipTip,"mbPreviousFeiShengAdjusted")
RegistClassMember(CLuaEquipTip,"ItemId")
RegistClassMember(CLuaEquipTip,"m_EquipKeZiColors")
RegistClassMember(CLuaEquipTip,"m_ActionPairTempDict")

CLuaEquipTip.s_DisplayChouBingIconBeforeFixedWord=true
CLuaEquipTip.b_NeedShowFeiShengAdjusted=true
CLuaEquipTip.ExtraFixedWordChangedPrefixIcon = "common_rhombus_ronglian"
CLuaEquipTip.FixedWordChangedPrefixIcon = "common_rhombus_change"
CLuaEquipTip.WordPrefixIcon = "common_rhombus_gold"
CLuaEquipTip.ArrowUp="common_fight_rank_up"
CLuaEquipTip.ArrowDown="common_fight_rank_down"
CLuaEquipTip.s_EnableReuseButtons = true
CLuaEquipTip.s_EnableInfoBtn = true
-- // 这里实现的逻辑非常绕！加个开关以防万一
-- // 主要是实现法宝套装词条在飞升玩家身上的修正。玩家切换回显示未修正的属性时，要显示原始的阶数等级对应的词条（而这些词条在之前的逻辑中是不让显示的）；显示修正属性时，显示玩家ItemProp里生效的词条
CLuaEquipTip.sbFixFeishengSuitWord=true
CLuaEquipTip.currentlyClickedBtnBounds = nil
LuaEnumPropertyCompareResult={
    Undefined=0,--//无差异
    Greatest=1,-- //最大
    Least=2,-- //最小
}

function CLuaEquipTip:Awake()
    self.m_EquipKeZiColors = {
        [1] = "ffffff",
        [2] = "ffff00",
        [3] = "ff7900",
        [5] = "0000ff",
        [10] = "ff0000",
        [20] = "b100ff",
    }
    self.background = self.transform:GetComponent(typeof(UISprite))
    self.iconTexture = self.transform:Find("Header/ItemCell/IconTexture"):GetComponent(typeof(CUITexture))
    self.nameLabel = self.transform:Find("Header/ItemNameLabel"):GetComponent(typeof(UILabel))
    self.availableLevelLabel = self.transform:Find("Header/AvailableLevelLabel"):GetComponent(typeof(UILabel))
    self.categoryLabel = self.transform:Find("Header/CategoryLabel"):GetComponent(typeof(UILabel))
    self.bindSprite = self.transform:Find("Header/ItemCell/BindSprite"):GetComponent(typeof(UISprite))
    self.disableSprite = self.transform:Find("Header/ItemCell/DisableSprite"):GetComponent(typeof(UISprite))
    self.qualitySprite = self.transform:Find("Header/ItemCell/QualitySprite"):GetComponent(typeof(UISprite))
    self.perfectionTemplate = self.transform:Find("Templates/PerfectionTemplate").gameObject
    self.titleLabelTemplate = self.transform:Find("Templates/TitleLabelTemplate").gameObject
    self.labelTemplate = self.transform:Find("Templates/LabelTemplate").gameObject
    self.equipScoreTemplate = self.transform:Find("Templates/EquipScoreTemplate").gameObject
    self.propertyLabelTemplate = self.transform:Find("Templates/PropertyLabelTemplate").gameObject
    self.basicPropertyTemplate = self.transform:Find("Templates/BasicPropertyTemplate").gameObject
    self.imageLabelTemplate = self.transform:Find("Templates/ImageLabelTemplate").gameObject
    self.topTenTemplate = self.transform:Find("Templates/TopTenTemplate").gameObject
    self.emptyTemplate = self.transform:Find("Templates/EmptyTemplate ").gameObject
    self.switchSplit = self.transform:Find("Header/SwitchFeishengTemplate").gameObject
    self.normalSplit = self.transform:Find("Header/Split").gameObject
    self.titleLabelButtonTemplate = self.transform:Find("Templates/TitleLabelButtonTemplate").gameObject
    self.titleLabelButtonTemplate2 = self.transform:Find("Templates/TitleLabelButtonTemplate2").gameObject
    self.contentTable = self.transform:Find("ContentScrollView/Table"):GetComponent(typeof(UITable))
    self.contentScrollView = self.transform:Find("ContentScrollView"):GetComponent(typeof(UIScrollView))
    self.infoBtn = self.transform:Find("ContentScrollView/InfoButton").gameObject
    self.prevBtn = self.transform:Find("Header/NavigationPanel/PrevBtn").gameObject
    self.nextBtn = self.transform:Find("Header/NavigationPanel/NextBtn").gameObject
    self.scrollViewIndicator = self.transform:Find("ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator))
    self.minContentWidth = 500
    self.maxContentWidth = 586
    self.NameColor = NGUIText.ParseColor24("FFED5F",0)--"FFED5F"
    self.ValueColor = NGUIText.ParseColor24("E8D0AA",0)--"E8D0AA"
    self.buttonTable = self.transform:Find("Buttons"):GetComponent(typeof(UITable))
    self.buttonTemplate = self.transform:Find("Templates/ButtonTemplate").gameObject
    self.buttonsBg = self.transform:Find("ButtonsBg"):GetComponent(typeof(UIWidget))
    self.extraPropertyFrame = self.transform:Find("ExtraPropertyFrame"):GetComponent(typeof(CEquipExtraPropertyFrame))
    self.baseItemInfo = nil
    self.mbShowFeiShengAttributes = false
    self.titleColor = "ACF9FF"
    self.weddingColor = "FF88FF"
    self.weddingContentColor = "22aeff"
    self.feishengTitleColor = "ff7900"
    self.mComparedItems = nil
    self.m_SwitchWordSetBotton = nil-- self.transform:Find("").gameObject
    self.m_SwitchHoleSetBotton=nil
    self.mbPreviousFeiShengAdjusted = 0
    self.m_ActionPairTempDict = {}
end

function CLuaEquipTip:Init0( item, baseItemInfo, comparedItems) --改成table
    self.baseItemInfo = baseItemInfo
    self.ItemId = item.Id
    self.mComparedItems = comparedItems
    self.mbShowFeiShengAttributes = item.Equip.IsFeiShengAdjusted > 0 and CLuaEquipTip.b_NeedShowFeiShengAdjusted

    self.extraPropertyFrame.gameObject:SetActive(false)
    if comparedItems == nil or #comparedItems == 0 then
        self:Init2(item, nil)
    else
        local equips = {}-- CreateFromClass(MakeArrayClass(CEquipment), comparedItems.Length)
        for i,v in ipairs(comparedItems) do
            table.insert( equips,v.Equip )
        end
        self:Init2(item, equips)
    end
end
function CLuaEquipTip:Init1( template, baseItemInfo)
    self.baseItemInfo = baseItemInfo
    self.ItemId = nil
    self.extraPropertyFrame.gameObject:SetActive(false)
    local default
    if baseItemInfo ~= nil then
        default = baseItemInfo.isFake
    else
        default = false
    end
    self:Init3(template, default)
end
function CLuaEquipTip:InitXianjiaPreview( template, upgradePreview)
    self.extraPropertyFrame.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.contentTable.transform)
    self.infoBtn:SetActive(false)
    self.prevBtn:SetActive(false)
    self.nextBtn:SetActive(false)
    self.iconTexture:Clear()
    self.nameLabel.disableDigitsBreak = true

    self.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), template.Color))

    self.bindSprite.spriteName = nil
    if template ~= nil then
        if not System.String.IsNullOrEmpty(template.Icon) then
            self.iconTexture:LoadMaterial(template.Icon)
        end

        self.nameLabel.text = LocalString.TranslateAndFormatText(template.Name)

        self.nameLabel.color = QualityColor.GetRGBValue(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), template.Color))
        --等级
        local level = 0
        if CClientMainPlayer.Inst ~= nil then
            level = CClientMainPlayer.Inst.FinalMaxLevelForEquip
        end
        if template.Grade == 0 then
            self.availableLevelLabel.text = nil
        elseif template.Grade > level then
            self.availableLevelLabel.text = SafeStringFormat3(LocalString.GetString("[c][FF0000]%d级[-][/c]"), template.Grade)
        else
            self.availableLevelLabel.text = SafeStringFormat3(LocalString.GetString("[c][FFFFFF]%d级[-][/c]"), template.Grade)
        end

        --类型
        self.categoryLabel.text = nil
        local blankSpace = CommonDefs.IS_VN_CLIENT and " " or ""
        self.categoryLabel.color = Color.white
        local subTypeTemplate = EquipmentTemplate_SubType.GetData(template.Type * 100 + template.SubType)
        if subTypeTemplate ~= nil then
            if IdPartition.IdIsTalisman(template.ID) then
                local talismanType = LocalString.GetString("仙家")
                if template.Race.Length > 1 then
                    self.categoryLabel.text = talismanType
                else
                    self.categoryLabel.text = System.String.Format("{0}{1}({2})", talismanType, blankSpace, Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), template.Race[0])))
                end
            else
                self.categoryLabel.text = subTypeTemplate.Name
            end
            local cls = EnumClass.Undefined
            if CClientMainPlayer.Inst ~= nil then
                cls = CommonDefs.ConvertIntToEnum(typeof(EnumClass), CClientMainPlayer.Inst.BasicProp.Class)
            end

            if template.Race ~= nil then
                local professionFit = false
                do
                    local i = 0
                    while i < template.Race.Length do
                        local eClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), template.Race[i])
                        if eClass == cls then
                            professionFit = true
                            break
                        end
                        i = i + 1
                    end
                end
                if not professionFit then
                    self.categoryLabel.color = Color.red
                end
            end
        end

        --法宝
        if IdPartition.IdIsTalisman(template.ID) then
            if self.mbShowFeiShengAttributes then
                self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]修正基础属性[-][/c]"), self.feishengTitleColor))
            else
                self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]基础属性[-][/c]"), self.titleColor))
            end

            local xianjia = Talisman_XianJia.GetData(template.ID)
            if upgradePreview then
                self:AppendWord(CLuaEquipTip.WordPrefixIcon, g_MessageMgr:FormatMessage("FABAO_UPGRADE_COMMON_WORD_PREVIEW"), self.nameLabel.color)
            else
                if xianjia ~= nil then
                    self:AppendWord(CLuaEquipTip.WordPrefixIcon, CChatLinkMgr.TranslateToNGUIText(xianjia.CommonWordInfo, false), self.nameLabel.color)
                end
            end

            --#region"额外属性"
            if self.mbShowFeiShengAttributes then
                self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]修正额外属性[-][/c]"), self.feishengTitleColor))
            else
                self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]额外属性[-][/c]"), self.titleColor))
            end
            if upgradePreview then
                self:AppendWord(CLuaEquipTip.WordPrefixIcon, g_MessageMgr:FormatMessage("FABAO_UPGRADE_EXTRA_WORD_PREVIEW"), self.nameLabel.color)
            else
                if xianjia ~= nil then
                    self:AppendWord(CLuaEquipTip.WordPrefixIcon, CChatLinkMgr.TranslateToNGUIText(xianjia.ExtraWordInfo, false), self.nameLabel.color)
                end
            end
            --#endregion

            --#region"法宝套装属性"
            if self.mbShowFeiShengAttributes then
                self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]修正套装属性[-][/c]"), self.feishengTitleColor))
            else
                self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]套装属性[-][/c]"), self.titleColor))
            end

            local suitKey = 0
            Talisman_XianJiaSuitConsist.Foreach(function (key, data)
                local list = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, UInt32)), UInt32, data.Consist)
                if CommonDefs.ListContains(list, typeof(UInt32), template.ID) and data.TalismanType == template.TalismanType then
                    suitKey = key
                end
            end)
            local suit = Talisman_XianJiaSuitConsist.GetData(suitKey)
            self:AppendText(SafeStringFormat3(LocalString.GetString("[c][42be2d]【%s】0/%d"), suit.Name, suit.Consist.Length))
            self:AddTitle("")

            for i=0,suit.ActivateSuits.Length-1 do
                local ts = Talisman_Suit.GetData(suit.ActivateSuits[i].SuitId)
                if ts then
                    local suitWords = ts.Words
                    if self.mbShowFeiShengAttributes and ts.FeiShengReplaceWords ~= nil then
                        suitWords = ts.FeiShengReplaceWords
                    end
                    if suitWords then
                        for j=0,suitWords.Length-1 do
                            local word = Word_Word.GetData(suitWords[j])
                            if word then
                                self:AppendText(SafeStringFormat3(LocalString.GetString("[c][6b7078](%d)【%s】%s(评分+%s)[-][/c]"),
                                    suit.ActivateSuits[i].NeedCount, word.Name, word.Description, word.Mark))
                            end
                        end
                    end
                end
            end
            --#endregion
        end
        --处理按钮显示
        self:ShowButtons(self.baseItemInfo)
        self:LayoutWnd()
    end
end
function CLuaEquipTip:GetTopOffset( )
    return self.background.transform.localPosition.y + self.background.height * 0.5
end
function CLuaEquipTip:SetTopOffset( top)
    Extensions.SetLocalPositionY(self.background.transform, top - self.background.height * 0.5)
end
function CLuaEquipTip:CanShowTwoWordSwitchButton( item)
    local has2wordset = item.Equip.HasTwoWordSet
    if not has2wordset then
        return false
    end
    if CLuaEquipInfoWnd.FromAttrGroup then--属性切换
        return true
    end
    --包裹和身上的装备才显示
    if self.baseItemInfo ~= nil then
        if not (TypeIs(self.baseItemInfo, typeof(PackageItemInfo)) or TypeIs(self.baseItemInfo, typeof(BodyItemInfo))) then
            return false
        end

        local myId = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id or 0
        if item.Equip.OwnerId == myId then
            return true
        end
    end
    return false
end
function CLuaEquipTip:OnClickTwoWordSwitchButton( go)
    if CLuaEquipInfoWnd.FromAttrGroup then
        local idx = self.baseItemInfo.item.Equip.ActiveWordSetIdx
        local itemInfo = CItemMgr.Inst:GetItemInfo(self.ItemId)
        if itemInfo ~= nil then
            Gac2Gas.RequestSetEquipWordSetIdxInAttrGroup(CLuaPropertySwitchWnd.m_GroupIndex,itemInfo.pos,self.ItemId,idx==2 and 1 or 2)
        end
    else
        local itemInfo = CItemMgr.Inst:GetItemInfo(self.ItemId)
        if itemInfo ~= nil then
            Gac2Gas.RequestCanSwitchEquipWordSet(EnumToInt(itemInfo.place), itemInfo.pos, self.ItemId)
            --不能点击了
            CUICommonDef.SetActive(go, false, true)
            self.m_SwitchWordSetBotton = go
            --Gac2Gas.RequestCanSwitchEquipWordSet((uint)itemInfo.place, itemInfo.pos, ItemId);
        end
    end
end
function CLuaEquipTip:Init2( item, comparedItems)
    Extensions.RemoveAllChildren(self.contentTable.transform)
    self.infoBtn:SetActive(false)
    self.prevBtn:SetActive(false)
    self.nextBtn:SetActive(false)
    self.iconTexture:Clear()
    self.nameLabel.disableDigitsBreak = true

    self.disableSprite.enabled = not item.MainPlayerIsFit
    self.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item.Equip.QualityType)

    self.bindSprite.spriteName = item.BindOrEquipCornerMark

    local equipment = EquipmentTemplate_Equip.GetData(item.TemplateId)
    if equipment ~= nil then
        if not System.String.IsNullOrEmpty(item.Icon) then
            self.iconTexture:LoadMaterial(item.Icon)
        end

        -- 名称
        self.nameLabel.text = LocalString.TranslateAndFormatText(item.Equip.DisplayName)

        self.nameLabel.color = item.Equip.DisplayColor

        -- 可穿戴等级
        --等级
        local level = 0
        local grade = item.Equip.Grade
        if CClientMainPlayer.Inst ~= nil and self.mbShowFeiShengAttributes then
            grade = item.Equip.AdjustedFeiShengEquipGrade
        end
        if CClientMainPlayer.Inst ~= nil then
            level = CClientMainPlayer.Inst.FinalMaxLevelForEquip
            --未飞升玩家maxlevel等于level
        end
        if grade == 0 then
            self.availableLevelLabel.text = nil
        elseif grade > level then
            self.availableLevelLabel.text = System.String.Format(LocalString.GetString("[c][FF0000]{0}级[-][/c]"), grade)
        else
            if self.mbShowFeiShengAttributes then
                self.availableLevelLabel.text = System.String.Format(LocalString.GetString("[c][{0}]{1}级[-][/c]"), self.feishengTitleColor, grade)
            else
                self.availableLevelLabel.text = System.String.Format(LocalString.GetString("[c][{0}]{1}级[-][/c]"), "FFFFFF", grade)
            end
        end

        --类型
        self.categoryLabel.text = nil
        local blankSpace = CommonDefs.IS_VN_CLIENT and " " or ""
        self.categoryLabel.color = Color.white
        local subTypeTemplate = EquipmentTemplate_SubType.GetData(equipment.Type * 100 + equipment.SubType)
        if subTypeTemplate ~= nil then
            if equipment.Type == EnumBodyPosition_lua.Weapon then
                local default
                if equipment.BothHand == 1 then
                    default = LocalString.GetString("双手")
                else
                    default = LocalString.GetString("单手")
                end

                if item.Equip.SubHand > 0 then
                    local subhandstr = LocalString.GetString("副手")
                    self.categoryLabel.text = SafeStringFormat3("%s%s%s%s(%s)",subhandstr, blankSpace, subTypeTemplate.Name, blankSpace, default)
                else
                    self.categoryLabel.text = System.String.Format("{0}{1}({2})", subTypeTemplate.Name, blankSpace, default)
                end
            elseif IdPartition.IdIsTalisman(equipment.ID) then
                local talismanType = ""
                if item.Equip.IsNormalTalisman then
                    talismanType = LocalString.GetString("普通法宝")
                elseif item.Equip.IsPremierTalisman then
                    talismanType = LocalString.GetString("高级法宝")
                else
                    talismanType = LocalString.GetString("仙家")
                end
                if equipment.Race.Length > 1 then
                    self.categoryLabel.text = talismanType
                else
                    self.categoryLabel.text = System.String.Format("{0}{1}({2})", talismanType, blankSpace, Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), equipment.Race[0])))
                end
            else
                self.categoryLabel.text = subTypeTemplate.Name
            end
            local cls = EnumClass.Undefined
            if CClientMainPlayer.Inst ~= nil then
                cls = CommonDefs.ConvertIntToEnum(typeof(EnumClass), CClientMainPlayer.Inst.BasicProp.Class)
            end

            -- 判断能否装备
            if equipment.Race ~= nil then
                local professionFit = false
                do
                    local i = 0
                    while i < equipment.Race.Length do
                        local eClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), equipment.Race[i])
                        if eClass == cls then
                            professionFit = true
                            break
                        end
                        i = i + 1
                    end
                end
                if not professionFit then
                    self.categoryLabel.color = Color.red
                end
            end
        end

        -- 是否飞升
        -- 有一个飞升前后的显示按钮变化
        self:CheckAddFeishengSwitch(item)

        local keziSign = self:ProcessKeZi(item)

        -- 是否是十大兵器
        if item.Equip.BQPToppPos ~= 0 or item.Equip.BQPPrecious > 0 then
            self:AddTopTen(item)
        end

        self:AddTitle(System.String.Format(LocalString.GetString("[c][{0}]装备评分     [-][FFFFFF]{1}[-][/c]"), self.titleColor, item.Equip.Score))

        if IdPartition.IdIsTalisman(equipment.ID) then
            if item.Equip.Hole > 0 then
                self:AddGemSlots(item.Equip)
            end
            if not item.Equip.IsFairyTalisman then
                local equip = EquipmentTemplate_Equip.GetData(item.TemplateId)
                local type = EquipmentTemplate_Type.GetData(equip.Type)
                self:AddTitle(System.String.Format(LocalString.GetString("[c][{0}]法宝位置     [-][FFFFFF]{1}[-][/c]"), self.titleColor, type.Name))
            end
            self:AppendDuration(item.Equip)
            --耐久度
            --#region"基础属性"
            local wordNotFound = true
            local words = CommonDefs.StringSplit_ArrayChar(item.WordDescription, "\n")
            do
                local i = 0
                while i < words.Length do
                    local continue
                    repeat
                        if System.String.IsNullOrEmpty(words[i]) then
                            continue = true
                            break
                        end
                        if wordNotFound then
                            wordNotFound = false
                            if self.mbShowFeiShengAttributes then
                                self:AddTitle(System.String.Format(LocalString.GetString("[c][{0}]修正基础属性[-][/c]"), self.feishengTitleColor))
                            else
                                self:AddTitle(System.String.Format(LocalString.GetString("[c][{0}]基础属性[-][/c]"), self.titleColor))
                            end
                        end
                        self:AppendWord(CLuaEquipTip.WordPrefixIcon, words[i], self.nameLabel.color)
                        continue = true
                    until 1
                    if not continue then
                        break
                    end
                    i = i + 1
                end
            end
            --#endregion

            --#region"额外属性"
            local adWords = CommonDefs.StringSplit_ArrayChar(item.Equip.AdditionalWordDescForTalisman, "\n")
            if adWords.Length > 0 and not System.String.IsNullOrEmpty(adWords[0]) then
                if self.mbShowFeiShengAttributes then
                    self:AddTitle(System.String.Format(LocalString.GetString("[c][{0}]修正额外属性[-][/c]"), self.feishengTitleColor))
                else
                    self:AddTitle(System.String.Format(LocalString.GetString("[c][{0}]额外属性[-][/c]"), self.titleColor))
                end
            end
            do
                local i = 0
                while i < adWords.Length do
                    local continue
                    repeat
                        if System.String.IsNullOrEmpty(adWords[i]) then
                            continue = true
                            break
                        end
                        self:AppendWord(CLuaEquipTip.WordPrefixIcon, adWords[i], self.nameLabel.color)
                        continue = true
                    until 1
                    if not continue then
                        break
                    end
                    i = i + 1
                end
            end
            --#endregion
            --#region 纹饰词条

            local isFaBao = item.Equip.IsFaBao
            local holeItemNotFound = true
            local baoshiItems = item.Equip.HoleItems
            if self.mbShowFeiShengAttributes then
                baoshiItems = CreateFromClass(MakeArrayClass(System.UInt32), 11)
                CollectionCopyTo(item.Equip.AdjustedFeiShengJewelItems.Values, baoshiItems, 1)
            end

            do
                local i = 1
                while i <= baoshiItems.Length do
                    local tpl = CItemMgr.Inst:GetItemTemplate(baoshiItems[i])
                    if tpl ~= nil then
                        if holeItemNotFound then
                            holeItemNotFound = false
                            if self.mbShowFeiShengAttributes then
                                self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]修正纹饰属性[-][/c]"), self.feishengTitleColor))
                            else
                                self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]纹饰属性[-][/c]"), self.titleColor))
                            end
                        end
                        local jewel = Jewel_Jewel.GetData(tpl.ID)
                        local builder = jewel and jewel.JewelName or tpl.Name
                        builder = builder..": "
                        local id = 0

                        id = Jewel_Jewel.GetData(tpl.ID).onFaBao


                        if id > 0 then
                            builder = builder..Word_Word.GetData(id).Description
                            self:AppendWord("common_orange", builder, Color.yellow)
                        end
                    else
                        break
                    end
                    i = i + 1
                end
            end
            --#endregion

            --#region"法宝套装属性"
            local suitInfo = CTalismanMgr.Inst:GetTalismanSuitInfo(item)
            if suitInfo ~= nil then
                if self.mbShowFeiShengAttributes then
                    self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]修正套装属性[-][/c]"), self.feishengTitleColor))
                else
                    self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]套装属性[-][/c]"), self.titleColor))
                end
                if item.Equip.IsFairyTalisman then
                    self:AppendText(SafeStringFormat3(LocalString.GetString("[c][42be2d]【%s】%d/%d"), suitInfo.Name, suitInfo.TalismansOnBody.Count, suitInfo.MaxNeedCount))
                else
                    self:AppendText(SafeStringFormat3(LocalString.GetString("[c][42be2d]【%d联·%s】%d/%d"),suitInfo.MaxNeedCount, suitInfo.Name, suitInfo.GroupsOnBody.Count, suitInfo.MaxNeedCount))
                end


                if item.Equip.IsFairyTalisman then
                    local delta = CClientMainPlayer.Inst.ItemProp.MultipleColumTalismanCurIndex * 8
                    do
                        local pos = EnumBodyPosition_lua.TalismanQian + delta
                        while pos <= EnumBodyPosition_lua.TalismanDui + delta do
                            if CommonDefs.DictContains(suitInfo.TalismansOnBody, typeof(UInt32), pos) then
                                local itemId = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Body, pos)
                                if not (itemId==nil or itemId=="") then
                                    local talismanOnBody = CItemMgr.Inst:GetById(itemId)
                                    self:AppendText(SafeStringFormat3("[c][42be2d]%s[-][/c]", talismanOnBody.Name))
                                end
                            else
                                if CommonDefs.DictContains(suitInfo.NeedTalismans, typeof(UInt32), pos) then
                                    local talismanNeed = EquipmentTemplate_Equip.GetData(CommonDefs.DictGetValue(suitInfo.NeedTalismans, typeof(UInt32), pos))
                                    self:AppendText(SafeStringFormat3("[c][6b7078]%s[-][/c]", talismanNeed.Name))
                                end
                            end
                            pos = pos + 1
                        end
                    end
                else
                    CommonDefs.ListIterate(suitInfo.GroupsOnBody, DelegateFactory.Action_object(function (pos)
                        local itemId = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Body, pos)
                        if not System.String.IsNullOrEmpty(itemId) then
                            local talismanOnBody = CItemMgr.Inst:GetById(itemId)
                            self:AppendText(SafeStringFormat3("[c][42be2d]%s[-][/c]", talismanOnBody.Name))
                        end
                    end))
                end
                self:AppendText("")
                local wordDic = {}
                local activateDic = {}
                local needCount2XianjiaLvl = {}
                local suitIndex = CClientMainPlayer.Inst.ItemProp.MultipleColumTalismanCurIndex + 1
                if self.baseItemInfo ~= nil and self.baseItemInfo.isOtherPlayerItem then
                    suitIndex = self.baseItemInfo.otherPlayerInfo.talismanIndex
                end

                local TalismanSuits = {}
                CommonDefs.DictIterate(CClientMainPlayer.Inst.ItemProp.TalismanSuits, DelegateFactory.Action_object_object(function (___key, ___value)
                    TalismanSuits[___key]=true
                end))

                for i=1,suitInfo.SuitInfos.Count do
                    local info = suitInfo.SuitInfos[i-1]
                    local suitId = info.SuitId
                    local suit = Talisman_Suit.GetData(suitId)
                    local suitWords = suit.Words
                    if self.mbShowFeiShengAttributes and suit.FeiShengReplaceWords ~= nil then
                        suitWords = suit.FeiShengReplaceWords
                    end
                    if suitWords then
                        for j=1,suitWords.Length do
                            local wordId = suitWords[j-1]
                            local userWordId = 0
                            if self.baseItemInfo and self.baseItemInfo.isOtherPlayerItem then
                                userWordId = self.baseItemInfo.otherPlayerInfo:GetTalismanSuitUserWordId(suitIndex, suit)
                            else
                                userWordId = CTalismanMgr.Inst:GetSuitUserWordId(suitIndex, suit, false)
                            end
                            if userWordId > 0 then
                                wordId = userWordId
                            end

                            local word = Word_Word.GetData(wordId)
                            if word then
                                local needCount = info.NeedCount
                                if not item.Equip.IsFairyTalisman then
                                    local actived = TalismanSuits[suitId] and info.SuitActived and suitInfo.SelfActivedSuit
                                    self:AppendText(SafeStringFormat3(LocalString.GetString("[c][%s]【%d联·%s】%s(评分+%s)[-][/c]"), actived and "42be2d" or "6b7078", needCount, suitInfo.Name, word.Description, word.Mark))
                                else
                                    local actived = TalismanSuits[suitId] and suitInfo.OnBody
                                    if not wordDic[needCount] then
                                        wordDic[needCount] = word

                                        activateDic[needCount] = actived
                                        if not needCount2XianjiaLvl[needCount] then
                                            needCount2XianjiaLvl[needCount] = suit.XianJiaLevel
                                        end
                                    elseif actived and not (CLuaEquipTip.sbFixFeishengSuitWord and self.mbPreviousFeiShengAdjusted > 0
                                        and not self.mbShowFeiShengAttributes and needCount2XianjiaLvl[needCount]
                                        and suit.XianJiaLevel <= needCount2XianjiaLvl[needCount]) then

                                        if (activateDic[needCount] and word.ID > wordDic[needCount].ID) or not activateDic[needCount] then
                                            wordDic[needCount]=word
                                            activateDic[needCount] = true
                                            if not needCount2XianjiaLvl[needCount] then
                                                needCount2XianjiaLvl[needCount] = suit.XianJiaLevel
                                            end
                                        end
                                    elseif not actived and not activateDic[needCount] and word.ID < wordDic[needCount].ID
                                        and not (CLuaEquipTip.sbFixFeishengSuitWord and self.mbPreviousFeiShengAdjusted > 0
                                        and not self.mbShowFeiShengAttributes
                                        and needCount2XianjiaLvl[needCount] and suit.XianJiaLevel <= needCount2XianjiaLvl[needCount]) then
                                        wordDic[needCount]=word

                                        activateDic[needCount] = false
                                        if not needCount2XianjiaLvl[needCount] then
                                            needCount2XianjiaLvl[needCount] = suit.XianJiaLevel
                                        end
                                    elseif CLuaEquipTip.sbFixFeishengSuitWord and not actived and self.mbPreviousFeiShengAdjusted > 0 and not self.mbShowFeiShengAttributes
                                        and (not needCount2XianjiaLvl[needCount] or suit.XianJiaLevel > needCount2XianjiaLvl[needCount]) then
                                        wordDic[needCount]=word
                                        activateDic[needCount] = false
                                        needCount2XianjiaLvl[needCount] = suit.XianJiaLevel
                                    end
                                end
                            end
                        end
                    end
                end
                if item.Equip.IsFairyTalisman then
                    for k,word in pairs(wordDic) do
                        self:AppendText(SafeStringFormat3(LocalString.GetString("[c][%s](%s)【%s】%s(评分+%s)[-][/c]"), activateDic[k] and "42be2d" or "6b7078",k, word.Name, word.Description, word.Mark))
                    end
                end
            end
            --#endregion

            --!!!这里是分割线，下面不要自己再添加分割线啦
            self:AppendText("")
            local lastChild = self.contentTable.transform:GetChild(self.contentTable.transform.childCount - 1)

            self:CheckAndAddGiverName(item.Equip)
            --赠送者名称
            self:CheckAndAddProtectTime(item)
            --珍品保护时间
            self:CheckAndAddMinTradePrice(item.Equip)
            --最低易市交易价格

            local newLastChild = self.contentTable.transform:GetChild(self.contentTable.transform.childCount - 1)
            if newLastChild == lastChild then
                Extensions.RemoveLastChild(self.contentTable.transform)
            end
        else
            if CLuaEquipInfoWnd.Instance.m_TipsCount > 1 then
                if item.Equip.IsExtraEquipment then
                    self:AddEquipScoreInfo(SafeStringFormat3(LocalString.GetString("[c][FFFFFF]    基础%d·锻造%d[-][/c]"), item.Equip.BasicScore, item.Equip.ForgeScore))
                else
                    self:AddEquipScoreInfo(SafeStringFormat3(LocalString.GetString("[c][FFFFFF]    基础%d·强化%d·宝石%d·\n    神兵%d[-][/c]"), item.Equip.BasicScore, item.Equip.QiangHuaScore, item.Equip.BaoShiScore, item.Equip.HolyScore))
                end
            end

            if item.Equip.Hole > 0 then
                self:AddGemSlots(item.Equip)
            end

            -- 背饰锻造等级
            if item.Equip.IsExtraEquipment then
                if item.Equip.IsFeiShengAdjusted >0 then
                    self:AddTitle(System.String.Format(LocalString.GetString("[c][{0}]锻造等级     [-][FFFFFF]{1}[-][/c]"), self.titleColor, item.Equip.AdjustedFeiShengForgeLevel))
                else
                    self:AddTitle(System.String.Format(LocalString.GetString("[c][{0}]锻造等级     [-][FFFFFF]{1}[-][/c]"), self.titleColor, item.Equip.ForgeLevel))
                end
            end

            --海钓鱼竿               
            local poleData = HouseFish_PoleType.GetDataBySubKey("TemplateID",item.TemplateId)
            if poleData then
                self:AddTitle(System.String.Format(LocalString.GetString("[c][{0}]鱼竿等级     [-][FFFFFF]{1}[-][/c]"), self.titleColor, poleData.PoleLevel))
                self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("    海钓技能等级 {0}"), poleData.UnLockedLevel), nil)
            end

            --#region"基础属性"
            if self.mbShowFeiShengAttributes then
                self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]修正基础属性[-][/c]"), self.feishengTitleColor))
            else
                self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]基础属性[-][/c]"), self.titleColor))
            end
            local position = CommonDefs.ConvertIntToEnum(typeof(EnumBodyPosition), equipment.Type)
            repeat
                local extern = position
                if extern == EnumBodyPosition.Weapon then
                    do
                        --物理攻击、法术攻击、攻击速度
                        self:AppendPhysicalAttack(item.Equip, comparedItems)
                        self:AppendMagicalAttack(item.Equip, comparedItems)
                        self:AppendAttackSpeed(item.Equip, comparedItems)
                        self:AppendPerfectionDegree(item.Equip)
                        break
                    end
                elseif extern == EnumBodyPosition.Shield then
                    do
                        --格挡、物理防御、法术防御
                        self:AppendPhysicalDef(item.Equip, comparedItems)
                        self:AppendMagicalDef(item.Equip, comparedItems)
                        self:AppendBlock(item.Equip, comparedItems)
                        self:AppendBlockDamage(item.Equip, comparedItems)
                        self:AppendPerfectionDegree(item.Equip)
                        break
                    end
                elseif extern == EnumBodyPosition.Casque then
                    do
                        --物理防御、法术防御
                        self:AppendPhysicalDef(item.Equip, comparedItems)
                        self:AppendMagicalDef(item.Equip, comparedItems)
                        self:AppendPerfectionDegree(item.Equip)
                        break
                    end
                elseif extern == EnumBodyPosition.Armour then
                    do
                        --物理防御、法术防御
                        self:AppendPhysicalDef(item.Equip, comparedItems)
                        self:AppendMagicalDef(item.Equip, comparedItems)
                        self:AppendPerfectionDegree(item.Equip)
                        break
                    end
                elseif extern == EnumBodyPosition.Belt then
                    do
                        --气血上限
                        self:AppendHpFull(item.Equip, comparedItems)
                        self:AppendPerfectionDegree(item.Equip)
                        break
                    end
                elseif extern == EnumBodyPosition.Gloves then
                    do
                        --气血上限
                        self:AppendHpFull(item.Equip, comparedItems)
                        self:AppendPerfectionDegree(item.Equip)
                        break
                    end
                elseif extern == EnumBodyPosition.Shoes then
                    do
                        --物理防御、法术防御、物理躲避、法术躲避
                        self:AppendPhysicalDef(item.Equip, comparedItems)
                        self:AppendMagicalDef(item.Equip, comparedItems)
                        self:AppendPhysicalMiss(item.Equip, comparedItems)
                        self:AppendMagicalMiss(item.Equip, comparedItems)
                        self:AppendPerfectionDegree(item.Equip)
                        break
                    end
                elseif extern == EnumBodyPosition.Ring then
                    do
                        --物理攻击、法术攻击
                        self:AppendPhysicalAttack(item.Equip, comparedItems)
                        self:AppendMagicalAttack(item.Equip, comparedItems)
                        self:AppendPerfectionDegree(item.Equip)
                        break
                    end
                elseif extern == EnumBodyPosition.Bracelet then
                    do
                        --物理命中、法术命中
                        self:AppendPhysicalHit(item.Equip, comparedItems)
                        self:AppendMagicalHit(item.Equip, comparedItems)
                        break
                    end
                elseif extern == EnumBodyPosition.Necklace then
                    do
                        --物理命中、法术命中、物理躲避、法术躲避
                        self:AppendPhysicalHit(item.Equip, comparedItems)
                        self:AppendMagicalHit(item.Equip, comparedItems)
                        self:AppendPhysicalMiss(item.Equip, comparedItems)
                        self:AppendMagicalMiss(item.Equip, comparedItems)
                        break
                    end
                elseif extern == EnumBodyPosition.BackPendant then
                    do
                        -- 血气上限
                        self:AppendAdjHpFull2(item.Equip, comparedItems)
                        break
                    end
                elseif extern == EnumBodyPosition.HiddenWeapon then
                    do
                        -- 最终物理攻击 最终法术攻击
                        self:AppendAdjpAtt2(item.Equip, comparedItems)
                        self:AppendAdjmAtt2(item.Equip, comparedItems)
                        break
                    end
                elseif extern == EnumBodyPosition.Beaded then
                    do
                        -- 最终物理防御 最终法术防御 抗物理致命 抗法术致命
                        self:AppendAdjpDef2(item.Equip, comparedItems)
                        self:AppendAdjmDef2(item.Equip, comparedItems)
                        self:AppendAdjAntipFatal2(item.Equip, comparedItems)
                        self:AppendAdjAntimFatal2(item.Equip, comparedItems)
                        break
                    end
                else
                    break
                end
            until 1
            --#endregion
            self:AppendDuration(item.Equip)
            --耐久度

            --#region 词条

            if not CLuaEquipTip.s_DisplayChouBingIconBeforeFixedWord then
                local words = CommonDefs.StringSplit_ArrayChar(item.WordDescription, "\n")
                local wordNotFound = true
                do
                    local i = 0
                    while i < words.Length do
                        local continue
                        repeat
                            if System.String.IsNullOrEmpty(words[i]) then
                                continue = true
                                break
                            end
                            if wordNotFound then
                                wordNotFound = false
                                if self.mbShowFeiShengAttributes then
                                    self:AddTitle(System.String.Format(LocalString.GetString("[c][{0}]修正词条属性[-][/c]"), self.feishengTitleColor))
                                else
                                    self:AddTitle(System.String.Format(LocalString.GetString("[c][{0}]词条属性[-][/c]"), self.titleColor))
                                end
                            end
                            self:AppendWord(CLuaEquipTip.WordPrefixIcon, words[i], self.nameLabel.color)
                            continue = true
                        until 1
                        if not continue then
                            break
                        end
                        i = i + 1
                    end
                end
            else
                local has2wordset = item.Equip.HasTwoWordSet
                local canshowbtn = self:CanShowTwoWordSwitchButton(item)

                local wordInfo = item.Equip.WordInfo
                local wordNotFound = true
                do
                    local i = 0
                    while i < wordInfo.Count do
                        local continue
                        repeat
                            local word = wordInfo[i]
                            if System.String.IsNullOrEmpty(word.Description) then
                                continue = true
                                break
                            end
                            if wordNotFound then
                                wordNotFound = false

                                local wordColor = self.titleColor
                                if self.mbShowFeiShengAttributes then
                                    wordColor = self.feishengTitleColor
                                end

                                if has2wordset then
                                    local setIndex = item.Equip.ActiveWordSetIdx
                                    local content = SafeStringFormat3(LocalString.GetString("[c][%s]词条属性·第一套[-][/c]"), wordColor)
                                    if self.mbShowFeiShengAttributes then
                                        content = SafeStringFormat3(LocalString.GetString("[c][%s]修正词条属性·第一套[-][/c]"), wordColor)
                                    end
                                    if setIndex == 2 then
                                        if self.mbShowFeiShengAttributes then
                                            content = SafeStringFormat3(LocalString.GetString("[c][%s]修正词条属性·第二套[-][/c]"), wordColor)
                                        else
                                            content = SafeStringFormat3(LocalString.GetString("[c][%s]词条属性·第二套[-][/c]"), wordColor)
                                        end
                                    end

                                    if canshowbtn then
                                        self:AddTitleAndSwitchButton(content, self.OnClickTwoWordSwitchButton)
                                    else
                                        self:AddTitle(content)
                                    end
                                else
                                    if self.mbShowFeiShengAttributes then
                                        self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]修正词条属性[-][/c]"), self.feishengTitleColor))
                                    else
                                        self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]词条属性[-][/c]"), self.titleColor))
                                    end
                                end
                            end
                            local spriteName = CLuaEquipTip.WordPrefixIcon
                            if (item.Equip.IsRedOrPurpleEquipment and not item.Equip.IsFake and word.wordChanged) then
                                if item.Equip.IsExtraEquipment then
                                    spriteName = CLuaEquipTip.ExtraFixedWordChangedPrefixIcon
                                else
                                    spriteName = CLuaEquipTip.FixedWordChangedPrefixIcon
                                end
                            end
                            
                            self:AppendWord(spriteName, word.Description, self.nameLabel.color)
                            continue = true
                        until 1
                        if not continue then
                            break
                        end
                        i = i + 1
                    end
                end
            end

            --#endregion

            --#region 洗炼次数

            if item.Equip.BaptizeScore > 0 then
                self:AddTitle(System.String.Format(LocalString.GetString("[c][{0}]词条洗炼积分[-] [FFFFFF]{1}[-] [00FFFF]{2}[-][/c]"), self.titleColor, item.Equip.BaptizeScore, item.IsBinded and "" or LocalString.GetString("(交易后清零)")))
            end

            --#endregion

            --#region 神兵

            if item.Equip.IsShenBing then
                self:AddTitle(System.String.Format(LocalString.GetString("[c][{0}]神兵培养等级[-] [FFFFFF]{1}[-][/c]"), self.titleColor, item.Equip.HolyTrainLevel))
                if self.mbShowFeiShengAttributes then
                    self:AddTitle(System.String.Format(LocalString.GetString("[c][{0}]修正神兵基础属性[-][/c]"), self.feishengTitleColor))
                else
                    self:AddTitle(System.String.Format(LocalString.GetString("[c][{0}]神兵基础属性[-][/c]"), self.titleColor))
                end
                repeat
                    local ref = CommonDefs.ConvertIntToEnum(typeof(EnumBodyPosition), equipment.Type)
                    if ref == EnumBodyPosition.Weapon then
                        do
                            --物理攻击、法术攻击、攻击速度
                            self:AppendPhysicalAttackHoly(item.Equip, comparedItems)
                            self:AppendMagicalAttackHoly(item.Equip, comparedItems)
                            break
                        end
                    elseif ref == EnumBodyPosition.Shield then
                        do
                            --格挡、物理防御、法术防御
                            self:AppendPhysicalDefHoly(item.Equip, comparedItems)
                            self:AppendMagicalDefHoly(item.Equip, comparedItems)
                            self:AppendBlockHoly(item.Equip, comparedItems)
                            self:AppendBlockDamageHoly(item.Equip, comparedItems)
                            break
                        end
                    elseif ref == EnumBodyPosition.Casque then
                        do
                            --物理防御、法术防御
                            self:AppendPhysicalDefHoly(item.Equip, comparedItems)
                            self:AppendMagicalDefHoly(item.Equip, comparedItems)
                            break
                        end
                    elseif ref == EnumBodyPosition.Armour then
                        do
                            --物理防御、法术防御
                            self:AppendPhysicalDefHoly(item.Equip, comparedItems)
                            self:AppendMagicalDefHoly(item.Equip, comparedItems)
                            break
                        end
                    elseif ref == EnumBodyPosition.Belt then
                        do
                            --气血上限
                            self:AppendHpFullHoly(item.Equip, comparedItems)
                            break
                        end
                    elseif ref == EnumBodyPosition.Gloves then
                        do
                            --气血上限
                            self:AppendHpFullHoly(item.Equip, comparedItems)
                            break
                        end
                    elseif ref == EnumBodyPosition.Shoes then
                        do
                            --物理防御、法术防御、物理躲避、法术躲避
                            self:AppendPhysicalDefHoly(item.Equip, comparedItems)
                            self:AppendMagicalDefHoly(item.Equip, comparedItems)
                            self:AppendPhysicalMissHoly(item.Equip, comparedItems)
                            self:AppendMagicalMissHoly(item.Equip, comparedItems)
                            break
                        end
                    elseif ref == EnumBodyPosition.Ring then
                        do
                            --物理攻击、法术攻击
                            self:AppendPhysicalAttackHoly(item.Equip, comparedItems)
                            self:AppendMagicalAttackHoly(item.Equip, comparedItems)
                            break
                        end
                    elseif ref == EnumBodyPosition.Bracelet then
                        do
                            --物理命中、法术命中
                            self:AppendPhysicalHitHoly(item.Equip, comparedItems)
                            self:AppendMagicalHitHoly(item.Equip, comparedItems)
                            break
                        end
                    elseif ref == EnumBodyPosition.Necklace then
                        do
                            --物理命中、法术命中、物理躲避、法术躲避
                            self:AppendPhysicalHitHoly(item.Equip, comparedItems)
                            self:AppendMagicalHitHoly(item.Equip, comparedItems)
                            self:AppendPhysicalMissHoly(item.Equip, comparedItems)
                            self:AppendMagicalMissHoly(item.Equip, comparedItems)
                            break
                        end
                    else
                        break
                    end
                until 1

                local words = item.Equip.HolyWordContents

                if self.mbShowFeiShengAttributes then
                    self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]修正神兵词条属性[-][/c]"), self.feishengTitleColor))

                    words = CreateFromClass(MakeArrayClass(System.UInt32), item.Equip.AdjustedShenBingWords.Values.Count)
                    local cnt = 0
                    CommonDefs.EnumerableIterate(item.Equip.AdjustedShenBingWords.Values, DelegateFactory.Action_object(function (val)
                        local out = cnt
                        cnt = out + 1
                        words[out] = val
                    end))
                else
                    self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]神兵词条属性[-][/c]"), self.titleColor))
                end
                for i=1,words.Length do
                    if words[i-1] > 0 then
                        local desc = CEquipmentProcessMgr.Inst:GetWordDescription(words[i-1])
                        if not (desc==nil or desc=="") then
                            self:AppendWord(CLuaEquipTip.WordPrefixIcon, desc, self.nameLabel.color)
                        end
                    end
                end
            end

            --#endregion
            --#region 宝石词条
            self:InitHoleInfo(item)
            --#endregion

            --#region 前缘
            self:InitQianYuan(item)

            --#endregion

            --#region 石之灵
            self:InitShiZhiLing(item)
            --#endregion
            --#region 鉴定技能
            self:CheckAndAddIdentifySkill(item.Equip)
            --#endregion

            --结婚戒指刻字 2
            if CLuaWeddingMgr.IsWeddingRing(item.Equip.TemplateId) then
              self:InitWeddingRingWord(item)
            elseif CWeddingMgr.Inst:IsWeddingRingTaben(item.Equip) then
              self:InitWeddingRingTaben(item)
            end

            --!!!这里是分割线，下面不要自己再添加分割线啦
            self:AppendText("")
            local lastChild = self.contentTable.transform:GetChild(self.contentTable.transform.childCount - 1)

            self:CheckAndAddGiverName(item.Equip)
            --赠送者名称
            self:CheckAndAddProtectTime(item)
            --珍品保护时间
            self:CheckAndAddMinTradePrice(item.Equip)
            --最低易市交易价格

            --#region 丢魂
            --不使用IsLostSoul变量，因为在仓库中自动修复失魂不起作用，会导致显示问题
            if (CommonDefs.op_GreaterThan_DateTime_DateTime(CServerTimeMgr.ConvertTimeStampToZone8Time(item.Equip.LostSoulTime):AddDays(item.Equip.LostSoulDuration), CServerTimeMgr.Inst:GetZone8Time())) then
                self:AppendText(SafeStringFormat3(LocalString.GetString("[c][%s]失魂状态(装备属性失效)[-][/c]"), NGUIText.EncodeColor24(Color.red)))
                self:AppendText(SafeStringFormat3(LocalString.GetString("[c][%s]失魂截止至: %s[-][/c]"), NGUIText.EncodeColor24(Color.red), ToStringWrap(CServerTimeMgr.ConvertTimeStampToZone8Time(item.Equip.LostSoulTime):AddDays(item.Equip.LostSoulDuration), "yyyy-MM-dd HH:mm")))
                self:AppendText(SafeStringFormat3(LocalString.GetString("[c][%s]单天修理价格: %d[-][/c]"), NGUIText.EncodeColor24(Color.red), item.Equip.LostSoulPricePerDay))
            end
            --#endregion

            --五色丝拓本
            self:InitWusesiTaben(item)
            self:InitWusesiTaben2020(item)

            --知己手镯拓本
            self:InitZhiJiBraceletTaben(item)

            --#region 玩家名字
            if not System.String.IsNullOrEmpty(item.Equip.CustomName) then
                self:AppendText(SafeStringFormat3(" [%s]<%s>", item.Equip.CustomNameColor, item.Equip.CustomName))
            end

            if item.Equip.IsShenBing then
                self:AppendText(SafeStringFormat3(LocalString.GetString("[c][%s]该神兵由[%s]%s[-]培养而来[/c]"), NGUIText.EncodeColor24(Color.yellow), NGUIText.EncodeColor24(item.Equip.DisplayColor), item.Equip.Name))
            end
            --#endregion


            local newLastChild = self.contentTable.transform:GetChild(self.contentTable.transform.childCount - 1)
            if newLastChild == lastChild then
                Extensions.RemoveLastChild(self.contentTable.transform)
            end
        end
        --处理按钮显示
        self:ShowButtons(self.baseItemInfo)
        self:LayoutWnd()

        if self.baseItemInfo ~= nil then
            local packageItemInfo = TypeAs(self.baseItemInfo, typeof(PackageItemInfo))
            if packageItemInfo ~= nil and packageItemInfo.navDelegate ~= nil then
                self.nextBtn:SetActive(packageItemInfo.navDelegate:HasNextEquip(self.baseItemInfo.itemId, self.baseItemInfo.position))
                self.prevBtn:SetActive(packageItemInfo.navDelegate:HasPreviousEquip(self.baseItemInfo.itemId, self.baseItemInfo.position))
                UIEventListener.Get(self.nextBtn).onClick = DelegateFactory.VoidDelegate(function (go)
                    packageItemInfo.navDelegate:OnMoveToNextEquip(self.baseItemInfo.itemId, self.baseItemInfo.position)
                end)
                UIEventListener.Get(self.prevBtn).onClick = DelegateFactory.VoidDelegate(function (go)
                    packageItemInfo.navDelegate:OnMoveToPreviousEquip(self.baseItemInfo.itemId, self.baseItemInfo.position)
                end)
            end
        end

        if self.baseItemInfo ~= nil and CLuaEquipTip.s_EnableInfoBtn then
            self.infoBtn:SetActive(true)
            UIEventListener.Get(self.infoBtn).onClick = DelegateFactory.VoidDelegate(function (go)
                CLuaEquipInfoWnd.Instance.IsExtraPropertyFrameVisible = CLuaEquipInfoWnd.Instance.ExtraPropertyState ~= 0-- not CLuaEquipInfoWnd.Instance.IsExtraPropertyFrameVisible
                if CLuaEquipInfoWnd.Instance.IsExtraPropertyFrameVisible then
                    CLuaEquipInfoWnd.Instance.ExtraPropertyState = 0
                else
                    CLuaEquipInfoWnd.Instance.ExtraPropertyState = -1
                end
                CLuaEquipInfoWnd.Instance:UpdateDisplay()
            end)
            local localCorners = self.contentScrollView.panel.localCorners
            self.infoBtn.transform.localPosition = Vector3(localCorners[2].x - 50, localCorners[2].y - 50)
        end

        self.extraPropertyFrame.gameObject:SetActive(CLuaEquipInfoWnd.Instance.IsExtraPropertyFrameVisible)
        if self.extraPropertyFrame.gameObject.activeSelf then
            if CLuaEquipInfoWnd.Instance.ExtraPropertyState == 0 then
                self.extraPropertyFrame:Init(item)
            else
                self:ShowExtraInfo(CLuaEquipInfoWnd.Instance.ExtraPropertyState - 1)
            end
        end

        if keziSign and item.Equip.BQPToppPos ~= 0 then
            local localCorners = self.contentScrollView.panel.localCorners
            self.infoBtn.transform.localPosition = Vector3(localCorners[2].x - 50, localCorners[2].y - 150)
        elseif keziSign then
            local localCorners = self.contentScrollView.panel.localCorners
            self.infoBtn.transform.localPosition = Vector3(localCorners[2].x - 50, localCorners[2].y - 80)
        elseif item.Equip.BQPToppPos ~= 0 then
            local localCorners = self.contentScrollView.panel.localCorners
            self.infoBtn.transform.localPosition = Vector3(localCorners[2].x - 50, localCorners[2].y - 120)
        end
    end
end

function CLuaEquipTip:InitQianYuan(item)
    if not item.Equip.IsShenBing then
        local qianyuanName = CShenBingMgr.Inst:GetQianyuanName(item.TemplateId, (item.Equip.IsFake and 0 or 1))
        if not System.String.IsNullOrEmpty(qianyuanName) then
            self:AddTitle(System.String.Format(LocalString.GetString("[c][{0}]前缘装备   [FFFFFF]【{1}】[-][-][/c]"), self.titleColor, qianyuanName))
        end
        local id = CShenBingMgr.Inst:GetQianyuanKey(item.TemplateId, (item.Equip.IsFake and 0 or 1))
        if id > 0 then
            local data = ShenBing_Qianyuan.GetData(id)
            if data and data.Combination then
                for p1, p2 in string.gmatch(data.Combination, "(%d+),(%d+);?") do
                    p1 = tonumber(p1)
                    p2 = tonumber(p2)
                    local equipData = EquipmentTemplate_Equip.GetData(p1)
                    if equipData then
                        local equipCnt = CItemMgr.Inst:GetEquipAmountByTemplateIdAndFake(EnumItemPlace.Body, equipData.ID, p2 <= 0) 
                                         + CItemMgr.Inst:GetEquipAmountByTemplateIdAndFake(EnumItemPlace.Bag, equipData.ID, p2 <= 0)
                        local try = p2 <= 0 and equipData.AliasName or equipData.Name
                        self:AppendQianyuan(try, equipCnt)
                    end
                end
            end
        end
    end
end

--宝石词条
function CLuaEquipTip:InitHoleInfo(item)
    local isWeapon = item.Equip.IsWeapon
    local isArmor = item.Equip.IsArmor
    local isJewelry = item.Equip.IsJewelry
    local isFaBao = item.Equip.IsFaBao
    local holeItemNotFound = true
    local baoshiItems = {}
    local hole2set = false
    if item.Equip.HoleSetCount==2 then
        hole2set=true
        if item.Equip.ActiveHoleSetIdx<=1 then--第一套
            local holeItems = item.Equip.HoleItems
            for i=1,item.Equip.Hole do
                table.insert(baoshiItems,holeItems[i])
            end
        else
            local holeItems = item.Equip.SecondaryHoleItems
            for i=1,item.Equip.Hole do
                if holeItems[i]>0 and holeItems[i]<100 then
                    table.insert(baoshiItems,item.Equip.HoleItems[holeItems[i]])
                else
                    table.insert(baoshiItems,holeItems[i])
                end
            end
        end
    else
        local holeItems = item.Equip.HoleItems
        for i=1,item.Equip.Hole do
            table.insert(baoshiItems,holeItems[i])
        end
    end
    if self.mbShowFeiShengAttributes then
        baoshiItems = {}
        CommonDefs.DictIterate(item.Equip.AdjustedFeiShengJewelItems, DelegateFactory.Action_object_object(function (___key, ___value)
            table.insert( baoshiItems, ___value )
        end))
    end

    local canshowbtn = self:CanShowTwoHoleSwitchButton(item)
    for i,v in ipairs(baoshiItems) do
        local tpl = CItemMgr.Inst:GetItemTemplate(v)
        if tpl ~= nil then
            if holeItemNotFound then
                holeItemNotFound = false
                if self.mbShowFeiShengAttributes then
                    if hole2set then
                        if item.Equip.ActiveHoleSetIdx<=1 then
                            if canshowbtn then
                                self:AddTitleAndSwitchButton(SafeStringFormat3(LocalString.GetString("[c][%s]修正宝石属性·第一套[-][/c]"), self.feishengTitleColor),self.OnClick2HoleSwitchButton)
                            else
                                self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]修正宝石属性·第一套[-][/c]"), self.feishengTitleColor))
                            end
                        else
                            if canshowbtn then
                                self:AddTitleAndSwitchButton(SafeStringFormat3(LocalString.GetString("[c][%s]修正宝石属性·第二套[-][/c]"), self.feishengTitleColor),self.OnClick2HoleSwitchButton)
                            else
                                self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]修正宝石属性·第二套[-][/c]"), self.feishengTitleColor))
                            end
                        end
                    else
                        self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]修正宝石属性[-][/c]"), self.feishengTitleColor))
                    end
                else
                    if hole2set then
                        if item.Equip.ActiveHoleSetIdx<=1 then
                            if canshowbtn then
                                self:AddTitleAndSwitchButton(SafeStringFormat3(LocalString.GetString("[c][%s]宝石属性·第一套[-][/c]"), self.titleColor),self.OnClick2HoleSwitchButton)
                            else
                                self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]宝石属性·第一套[-][/c]"), self.titleColor))
                            end
                        else
                            if canshowbtn then
                                self:AddTitleAndSwitchButton(SafeStringFormat3(LocalString.GetString("[c][%s]宝石属性·第二套[-][/c]"), self.titleColor),self.OnClick2HoleSwitchButton)
                            else
                                self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]宝石属性·第二套[-][/c]"), self.titleColor))
                            end
                        end
                    else
                        self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]宝石属性[-][/c]"), self.titleColor))
                    end
                end
            end
            local jewel = Jewel_Jewel.GetData(tpl.ID)
            local builder = jewel and jewel.JewelName or tpl.Name
            builder=builder..": "
            local id = 0
            if isWeapon then
                id = Jewel_Jewel.GetData(tpl.ID).onWeapon
            elseif isArmor then
                id = Jewel_Jewel.GetData(tpl.ID).onArmor
            elseif isJewelry then
                id = Jewel_Jewel.GetData(tpl.ID).onJewelry
            elseif isFaBao then
                id = Jewel_Jewel.GetData(tpl.ID).onFaBao
            end

            if id > 0 then
                local word = Word_Word.GetData(id)
                local desc = word.Description
                builder=builder..desc
                self:AppendWord("common_orange", builder, Color.yellow)
            end
        end
    end
end
--2套宝石切换
function CLuaEquipTip:OnClick2HoleSwitchButton( go)
    if CLuaEquipInfoWnd.FromAttrGroup then--属性切换
        local idx = self.baseItemInfo.item.Equip.ActiveHoleSetIdx
        local itemInfo = CItemMgr.Inst:GetItemInfo(self.ItemId)
        if itemInfo ~= nil then
            Gac2Gas.RequestSetEquipHoleSetIdxInAttrGroup(CLuaPropertySwitchWnd.m_GroupIndex,itemInfo.pos,self.ItemId,idx==2 and 1 or 2)
        end
    else
        local itemInfo = CItemMgr.Inst:GetItemInfo(self.ItemId)
        if itemInfo ~= nil then
            Gac2Gas.RequestCanSwitchEquipHoleSet(EnumToInt(itemInfo.place), itemInfo.pos, self.ItemId)
            --不能点击了
            CUICommonDef.SetActive(go, false, true)
            self.m_SwitchHoleSetBotton = go
        end
    end
end
function CLuaEquipTip:CanShowTwoHoleSwitchButton( item)
    local has2holeset = item.Equip.HoleSetCount==2
    if not has2holeset then
        return false
    end
    if CLuaEquipInfoWnd.FromAttrGroup then--属性切换
        return true
    end
    --包裹和身上的装备才显示
    if self.baseItemInfo ~= nil then
        if not (TypeIs(self.baseItemInfo, typeof(PackageItemInfo)) or TypeIs(self.baseItemInfo, typeof(BodyItemInfo))) then
            return false
        end

        local myId = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id or 0
        if item.Equip.OwnerId == myId then
            return true
        end
    end
    return false
end
function CLuaEquipTip:InitShiZhiLing(item)
    local gemGroup = nil
    if self.mbShowFeiShengAttributes then
        gemGroup = GemGroup_GemGroup.GetData(item.Equip.AdjustedFeiShengGemGroupId)
    else
        gemGroup = GemGroup_GemGroup.GetData(item.Equip.GemGroupId)
    end
    if gemGroup ~= nil then
        local hole2set = item.Equip.HoleSetCount==2--是否开启二套宝石
        local c = GameSetting_Common_Wapper.Inst:GetColor(gemGroup.NameColor)
        if self.mbShowFeiShengAttributes then
            if hole2set then
                if item.Equip.ActiveHoleSetIdx<=1 then
                    self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]修正石之灵一·%s(%d级)[-][/c]"), self.feishengTitleColor, gemGroup.Name, gemGroup.Level))
                else
                    self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]修正石之灵二·%s(%d级)[-][/c]"), self.feishengTitleColor, gemGroup.Name, gemGroup.Level))
                end
            else
                self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]修正石之灵·%s(%d级)[-][/c]"), self.feishengTitleColor, gemGroup.Name, gemGroup.Level))
            end
        else
            if hole2set then
                if item.Equip.ActiveHoleSetIdx<=1 then
                    self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]石之灵一·%s(%d级)[-][/c]"), self.titleColor, gemGroup.Name, gemGroup.Level))
                else
                    self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]石之灵二·%s(%d级)[-][/c]"), self.titleColor, gemGroup.Name, gemGroup.Level))
                end
            else
                self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]石之灵·%s(%d级)[-][/c]"), self.titleColor, gemGroup.Name, gemGroup.Level))
            end
        end
        do
            local i = 0
            while i < gemGroup.Effect.Length do
                local word = Word_Word.GetData(gemGroup.Effect[i])
                local desc = word.Description
                self:AppendWord("common_rhombus_purple", desc, c)
                i = i + 1
            end
        end
    end
end

function CLuaEquipTip:CheckAndAddMinTradePrice( equipment)
    if not equipment.IsBinded and equipment:IsPrecious() then
        self:AppendText(SafeStringFormat3(LocalString.GetString("[c][%s]最低易市交易价格: %d[-][/c]"), NGUIText.EncodeColor24(Color.yellow), equipment:GetPreciousPrice()))
    end
end
function CLuaEquipTip:CheckAndAddIdentifySkill( equipment)
    if equipment.IsIdentifiable then
        self:AddTitle(SafeStringFormat3(LocalString.GetString("[c][%s]附加技能[-][/c]"), self.titleColor))
        local skill = Skill_AllSkills.GetData(equipment.FixedIdentifySkillId)
        if skill ~= nil then
            local disableInfo = equipment.SubHand > 0 and LocalString.GetString("[AC6112](装备在副手栏时失效)[-]") or ""  -- 副手武器描述
            disableInfo = equipment.IdentifySkillDisable > 0 and LocalString.GetString("[AC6112](已禁用)[-]") or disableInfo
            local extendInfo = equipment.IdentifySkillExtendTimeInfo
            local timeInfo = equipment.IdentifySkillExpireTimeInfo
            self:AppendText(SafeStringFormat3(LocalString.GetString("[c]%s[ffff00]【%s·%d级】%s\n%s%s[-][/c]"),
                disableInfo,
                skill.Name,
                skill.Level,
                skill.TranslatedDisplay,
                timeInfo,
                extendInfo
            ))
            equipment:CheckIdentifySkillUpdateScore() -- 存在鉴定技能时检查一下技能评分
        else
            self:AppendText(SafeStringFormat3(LocalString.GetString("[c][%s]待鉴定[-][/c]"), NGUIText.EncodeColor24(Color.yellow)))
        end
    end
end
function CLuaEquipTip:CheckAndAddGiverName( equipment)
    if equipment.SenderPlayerId <= 0 then
        return
    end
    local giverName = CItemMgr.Inst:GetGiverName(equipment.Id)
    if giverName == nil then
        Gac2Gas.RequestEquipPresentName(equipment.OwnerId, equipment.Id)
    elseif not (giverName==nil or giverName=="") then
        if CommonDefs.IsSeaMultiLang() then
            self:AppendWord("common_rhombus_gift", SafeStringFormat3("[FFFFFF]%s[-][ACF9FF] %s[-]", giverName, LocalString.GetString("赠送")), Color.white)
        else
            self:AppendWord("common_rhombus_gift", SafeStringFormat3("[FFFFFF]%s[-][ACF9FF]%s[-]", giverName, LocalString.GetString("赠送")), Color.white)
        end
    end
end
function CLuaEquipTip:CheckAndAddProtectTime( item)
    --if (item.Equip.IsPrecious()) //refs #51842 处在交易保护期内的珍品，如果通过洗炼变成非珍品了，需要继续显示它的冻结时间。
    do
        local hours = PreciousEquipment_Setting.GetData().ItemTradeFrozenDuration
        local dt = CServerTimeMgr.ConvertTimeStampToZone8Time(item.Equip.LastTradeTime):AddHours(hours)
        local span = dt:Subtract(CServerTimeMgr.Inst:GetZone8Time())
        if span.TotalSeconds > 0 then
            self:AppendText(System.String.Format(LocalString.GetString("[c][{0}]保护时间: {1}[-][/c]"), NGUIText.EncodeColor24(Color.red), ToStringWrap(dt, "yyyy-MM-dd HH:mm")))
        end
    end
end

function CLuaEquipTip:ProcessKeZi(item)
    local keziSign = false
    --装备刻字
    if item.Equip.SculptureInfo ~= nil and item.Equip.SculptureInfo.Data ~= nil then
        local dataTbl = MsgPackImpl.unpack(item.Equip.SculptureInfo.Data)
        if dataTbl ~= nil then
            if dataTbl.Count == 1 then
                self:AppendWord("kezi_kedao", SafeStringFormat3(" [%s]%s", "ffffff", dataTbl[0]), Color.white)
            elseif dataTbl.Count==2 then
                local cnt = tonumber(dataTbl[1])

                self:AppendWord("kezi_kedao", SafeStringFormat3(" [%s]%s", self.m_EquipKeZiColors[cnt], dataTbl[0]), Color.white)
            end
            keziSign = true
        end
    end
      --结婚戒指刻字 1
    if CLuaWeddingMgr.IsWeddingRing(item.Equip.TemplateId) then
        local data = item.Equip.ExtraVarData.StringData
        if item.Equip.ExtraVarData ~= nil and item.Equip.ExtraVarData.Data ~= nil then
            if MsgPackImpl.try_unpack(item.Equip.ExtraVarData.Data) ~= nil then
                local cps = MsgPackImpl.unpack(item.Equip.ExtraVarData.Data)
                if CommonDefs.IsList(cps) then
                    if cps ~= nil and cps.Count == 3 then
                        self:AppendWord("kezi_kedao", SafeStringFormat3(" [%s]%s", self.weddingContentColor, cps[2]), Color.white)
                        keziSign = true
                    end
                end
            end
        end
    elseif CWeddingMgr.Inst:IsWeddingRingTaben(item.Equip) then
        local info = MsgPackImpl.unpack(item.Equip.TaBenInfo.Data)
        if info ~= nil and info.Count >= 3 then
            if info.Count >= 4 then
                self:AppendWord("kezi_kedao", SafeStringFormat3(" [%s]%s", self.weddingContentColor,info[3]), Color.white)
                keziSign = true
            end
        end
    end


    return keziSign
end

function CLuaEquipTip:InitZhiJiBraceletTaben(item)
    if not item.Equip.CraveData.Data then return end
    local craveData = CreateFromClass(CCraveData)
	craveData:LoadFromString(item.Equip.CraveData.Data)
	if craveData.Key == 1 and craveData.ExpiredTime > CServerTimeMgr.Inst.timeStamp then
		local list = MsgPackImpl.unpack(craveData.Data.Data)
        if list and list.Count >= 2 then
            local data = Item_Item.GetData(list[0])
            if data then
                local instance = CUICommonDef.AddChild(self.contentTable.gameObject, self.imageLabelTemplate)
                instance:SetActive(true)
                local row = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CImageTextInfoRow))
                local word
                if list.Count>=3 then
                    word = list[1]..LocalString.GetString("与")..list[2]..LocalString.GetString("的知己手镯").."\n"..LocalString.GetString("执手共七载，与君泛沧海")
                else
                    word = list[1]..LocalString.GetString("的知己手镯")
                end
                row:Init("", SafeStringFormat3("[c][ffff00]%s[-][/c]", word))
                row.transform:Find("Icon").gameObject:SetActive(false)
                local texture = row.transform:Find("Texture"):GetComponent(typeof(CUITexture))
                texture.gameObject:SetActive(true)
                texture.color = NGUIText.ParseColor24("ffff00", 0)
                texture:LoadMaterial("UI/Texture/FestivalActivity/Festival_ZhouNianQing/ZhouNianQing2023/Material/zhounianqinglifefriendwnd_icon.mat")
                
                --self.iconTexture:LoadMaterial(data.Icon)
                self.nameLabel.text = LocalString.TranslateAndFormatText(data.Name)

                local lasttime = craveData.ExpiredTime - CServerTimeMgr.Inst.timeStamp --剩余有效时间
                if lasttime > 0 then
                    local days = lasttime / 24 / 3600
                    if days >= 1 then
                        self:AppendText(System.String.Format(LocalString.GetString("[c][{0}]有效期[-] 剩余{1}天"),self.titleColor, math.ceil(days)))
                    else
                        local h = lasttime / 3600
                        self:AppendText(System.String.Format(LocalString.GetString("[c][{0}]有效期[-] 剩余{1}小时"),self.titleColor, math.ceil(h)))
                    end
                end
            end
        end
    end
end

function CLuaEquipTip:InitWusesiTaben(item)
    --手镯 五色丝刻字
    local tid = item.Equip.TemplateId
    local tdata = EquipmentTemplate_Equip.GetData(tid)
    if tdata == nil then return end

    local type = CommonDefs.ConvertIntToEnum(typeof(EnumBodyPosition), tdata.Type)
    if type ~= EnumBodyPosition.Bracelet then return end
    if item.Equip.ExtraVarData ~= nil and item.Equip.ExtraVarData.Data ~= nil then
        local cps = MsgPackImpl.unpack(item.Equip.ExtraVarData.Data)
        if cps ~= nil then
            if cps.Count == 4 then
                local dwdata = DuanWu_Setting.GetData()

                local pid = cps[0]
                local pname = cps[1]
                local oriid = cps[2]
                local time = cps[3]

                if tonumber(oriid) ~= dwdata.WuSeSiTemplateId then
                    return
                end
                
                local sd = CServerTimeMgr.Inst.timeStamp-time;
                local lasttime = dwdata.EquipTabenTime*24*3600-sd;--剩余有效时间

                if lasttime > 0 then
                    local desc = System.String.Format(dwdata.DisplayTxt, pname)
                    self:AppendText(desc)

                    local days = lasttime / 24 / 3600
                    if days >= 1 then
                        self:AppendText(System.String.Format(LocalString.GetString("[c][{0}]有效期[-] 剩余{1}天"),self.titleColor, math.ceil(days)))
                    else
                        local h = lasttime/3600
                        self:AppendText(System.String.Format(LocalString.GetString("[c][{0}]有效期[-] 剩余{1}小时"),self.titleColor, math.ceil(h)))
                    end
                end
            end
        end
    end
end

function CLuaEquipTip:InitWusesiTaben2020(item)
    --手镯 五色丝刻字2020
    local tid = item.Equip.TemplateId
    local tdata = EquipmentTemplate_Equip.GetData(tid)
    if tdata == nil then return end

    local type = CommonDefs.ConvertIntToEnum(typeof(EnumBodyPosition), tdata.Type)
    if type ~= EnumBodyPosition.Bracelet then return end
    if item.Equip.ExtraVarData ~= nil and item.Equip.ExtraVarData.Data ~= nil then
        local cps = MsgPackImpl.unpack(item.Equip.ExtraVarData.Data)
        if cps ~= nil and (cps.Count == 4 or cps.Count == 5)then

            local dwdata = DuanWu2020_Setting.GetData()

            local pid = cps[0]
            local pname = cps[1]
            local wssId = cps[2]
            local time = cps[3]

            if wssId ~= dwdata.WuSeSiTemplateId and
                wssId ~= dwdata.QingXiWuSeSiTemplateId then
                    return
            end

            local sd = CServerTimeMgr.Inst.timeStamp-time;
            local lasttime = dwdata.EquipTabenTime*24*3600-sd;--剩余有效时间

            if lasttime > 0 then
                if wssId == dwdata.QingXiWuSeSiTemplateId and cps.Count == 5 then
                    -- 情系
                    local matename = cps[4]
                    local desc = SafeStringFormat3(dwdata.DisplayTxt2, pname, matename)
                    self:AppendText(desc)
                else
                    local desc = SafeStringFormat3(dwdata.DisplayTxt1, pname)
                    self:AppendText(desc)
                end

                local days = lasttime / 24 / 3600
                if days >= 1 then
                    self:AppendText(System.String.Format(LocalString.GetString("[c][{0}]有效期[-] 剩余{1}天"),self.titleColor, math.ceil(days)))
                else
                    local h = lasttime/3600
                    self:AppendText(System.String.Format(LocalString.GetString("[c][{0}]有效期[-] 剩余{1}小时"),self.titleColor, math.ceil(h)))
                end
            end
        end
    end
end

function CLuaEquipTip:InitWeddingRingWord(item)
    local data = item.Equip.ExtraVarData.StringData
    if item.Equip.ExtraVarData ~= nil and item.Equip.ExtraVarData.Data ~= nil then
        if MsgPackImpl.try_unpack(item.Equip.ExtraVarData.Data) ~= nil then
            local cps = MsgPackImpl.unpack(item.Equip.ExtraVarData.Data)
            if CommonDefs.IsList(cps) then
                if cps ~= nil and cps.Count >= 2 then--3:存的是刻字内容
                    self:AppendWord("personalspacewnd_heart_2", SafeStringFormat3(LocalString.GetString(" [%s]%s与%s的结婚戒指"),self.weddingColor, cps[0], cps[1]), Color.white)
                    return
                end
            end
        end
    end
    if not (data==nil or data=="") and CClientMainPlayer.Inst ~= nil then
        self:AppendWord("personalspacewnd_heart_2", SafeStringFormat3(LocalString.GetString(" [%s]我与%s的结婚戒指"), self.weddingColor, data), Color.white)
    end
end
function CLuaEquipTip:InitWeddingRingTaben(item)
    local info = MsgPackImpl.unpack(item.Equip.TaBenInfo.Data)
    if info ~= nil and info.Count >= 3 then
      --if info.Count >= 4 then
      --  self:AppendWord("kezi_kedao", System.String.Format(" [{1}]{0}", info[3], self.weddingContentColor), Color.white)
      --end
      self:AppendWord("personalspacewnd_heart_2", System.String.Format(LocalString.GetString(" [{2}]{0}与{1}的结婚戒指"), info[1], info[2], self.weddingColor), Color.white)
      local ring = EquipmentTemplate_Equip.GetData(System.UInt32.Parse(ToStringWrap(info[0])))
      if ring ~= nil then
        self.iconTexture:LoadMaterial(ring.Icon)
      end
    end
end
function CLuaEquipTip:Init3( template, isFake)
    self.switchSplit:SetActive(false)
    Extensions.RemoveAllChildren(self.contentTable.transform)
    self.infoBtn:SetActive(false)
    self.prevBtn:SetActive(false)
    self.nextBtn:SetActive(false)
    self.iconTexture:Clear()
    self.nameLabel.disableDigitsBreak = true
    self.bindSprite.spriteName = nil
    self.disableSprite.enabled = not CEquipment.GetMainPlayerIsFit(template.ID, false)
    self.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), template.Color))
    --self.AddBasicPropertyItemToTable();
    self.ItemId = nil
    if not System.String.IsNullOrEmpty(template.Icon) then
        self.iconTexture:LoadMaterial(template.Icon)
    end

    local default
    if isFake then
        default = template.AliasName
    else
        default = template.Name
    end
    self.nameLabel.text = LocalString.TranslateAndFormatText(default)

    self.nameLabel.color = CEquipment.GetColor(template.ID)
    --等级
    local level = 0
    if CClientMainPlayer.Inst ~= nil then
        level = CClientMainPlayer.Inst.FinalMaxLevelForEquip
    end
    if template.Grade == 0 then
        self.availableLevelLabel.text = nil
    elseif template.Grade > level then
        self.availableLevelLabel.text = System.String.Format(LocalString.GetString("[c][FF0000]{0}级[-][/c]"), template.Grade)
    else
        self.availableLevelLabel.text = System.String.Format(LocalString.GetString("[c][FFFFFF]{0}级[-][/c]"), template.Grade)
    end

    --类型
    self.categoryLabel.text = nil
    local subTypeTemplate = EquipmentTemplate_SubType.GetData(template.Type * 100 + template.SubType)
    if subTypeTemplate ~= nil then
        self.categoryLabel.text = System.String.Format("[c][FFFFFF]{0}[-][/c]", subTypeTemplate.Name)
        local cls = EnumClass.Undefined
        if CClientMainPlayer.Inst ~= nil then
            cls = CommonDefs.ConvertIntToEnum(typeof(EnumClass), CClientMainPlayer.Inst.BasicProp.Class)
        end
        local professionFit = false
        if template.Race ~= nil then
            do
                local i = 0
                while i < template.Race.Length do
                    local eClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), template.Race[i])
                    if eClass == cls then
                        professionFit = true
                        break
                    end
                    i = i + 1
                end
            end
            if not professionFit then
                self.categoryLabel.text = System.String.Format("[c][FF0000]{0}[-][/c]", subTypeTemplate.Name)
            end
        end
    end

    self:CheckAndAddPUBG(template)
    --处理按钮显示
    self:ShowButtons(self.baseItemInfo)
    self:LayoutWnd()
    self.extraPropertyFrame.gameObject:SetActive(false)
end

function CLuaEquipTip:CheckAndAddPUBG(template)
    if CPUBGMgr.Inst:IsInChiJi() then
        local chijiItem = nil
        ChiJi_Item.ForeachKey(function (key)
            local data = ChiJi_Item.GetData(key)
            if data and data.ItemId == template.ID then
                chijiItem = data
            end
        end)

        if chijiItem and chijiItem.BuffId ~= 0 then
            local buff = Buff_Buff.GetData(chijiItem.BuffId)
            if not buff then return end
            self:AddTitle(LocalString.GetString(buff.Display))
        end
    end
end

function CLuaEquipTip:AppendWord( iconName, word, color)
    local instance = CUICommonDef.AddChild(self.contentTable.gameObject, self.imageLabelTemplate)
    instance:SetActive(true)
    local row = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CImageTextInfoRow))
    row:Init(iconName, System.String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor24(color), word))
end
function CLuaEquipTip:AppendGemWord( word, color)
    local instance = CUICommonDef.AddChild(self.contentTable.gameObject, self.imageLabelTemplate)
    instance:SetActive(true)
    local row = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CImageTextInfoRow))
    row:Init("common_orange", System.String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor24(color), word))
end
function CLuaEquipTip:AppendQianyuan( word, equipCnt)
    local instance = CUICommonDef.AddChild(self.contentTable.gameObject, self.imageLabelTemplate)
    instance:SetActive(true)
    local row = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CImageTextInfoRow))
    row:Init("equiptip_qianyuan", System.String.Format("[c][{0}]{1}[-][/c]", equipCnt > 0 and "C000C0FF" or "C000C080", word))
    row:SetIconAlpha(equipCnt > 0 and 1 or 0.3)
end
function CLuaEquipTip:AddTitle( text)
    local instance = CUICommonDef.AddChild(self.contentTable.gameObject, self.titleLabelTemplate)
    instance:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(CTitleInfoRow)):Init(text)
end

function CLuaEquipTip:AddTitleAndTipBtn(text,showtype)
    local instance = CUICommonDef.AddChild(self.contentTable.gameObject, self.titleLabelButtonTemplate2)
    instance:SetActive(true)
    CommonDefs.GetComponent_Component_Type(instance.transform:Find("Label"), typeof(UILabel)).text = text

    local btn1 = instance.transform:Find("Btn1").gameObject
    local btn2 =  instance.transform:Find("Btn2").gameObject
    
    btn1:SetActive(showtype == 0 or showtype==3)
    btn2:SetActive(showtype == 1 or showtype==2)

    self.m_subHandExtShowType = showtype

    local func = function()
        CLuaEquipInfoWnd.Instance.IsExtraPropertyFrameVisible = CLuaEquipInfoWnd.Instance.ExtraPropertyState ~= (showtype + 1)--not CLuaEquipInfoWnd.Instance.IsExtraPropertyFrameVisible
        if CLuaEquipInfoWnd.Instance.IsExtraPropertyFrameVisible then
            CLuaEquipInfoWnd.Instance.ExtraPropertyState = showtype + 1
        else
            CLuaEquipInfoWnd.Instance.ExtraPropertyState = -1
        end
        CLuaEquipInfoWnd.Instance:UpdateDisplay()
    end

    UIEventListener.Get(btn1).onClick = DelegateFactory.VoidDelegate(func)
    UIEventListener.Get(btn2).onClick = DelegateFactory.VoidDelegate(func)
end

function CLuaEquipTip:ShowExtraInfo(showtype)
    self.extraPropertyFrame:Init(nil)
    self.extraPropertyFrame:SetTitle(LocalString.GetString("副手武器耐久说明"))
    self.extraPropertyFrame:AppendTextDot(g_MessageMgr:FormatMessage("EQUIPTIP_SUNHAND_1"))

    if showtype == 1 or showtype == 2 then        
        local str = ""
        if showtype == 1 then --未装备主手或者主手无昆吾石
            str = g_MessageMgr:FormatMessage("EQUIPTIP_SUNHAND_2")
        else --主手携带昆吾石
            local weapon = CItemMgr.Inst:GetEquipByBodyPos(LuaEnumBodyPosition.Weapon)
            local kunwulv = self:GetKunWuGemLv(weapon)
            local p = 100
            if kunwulv == 1 then
                p = 80
            elseif kunwulv == 2 then
                p = 40
            elseif kunwulv == 3 then
                p = 20
            elseif kunwulv == 4 then
                p = 10
            elseif kunwulv == 5 then
                p = 5
            end
            str = g_MessageMgr:FormatMessage("EQUIPTIP_SUNHAND_3", kunwulv, p)
        end
        self.extraPropertyFrame:AppendTextWarn(str)
    elseif showtype == 3 then
        self.extraPropertyFrame:AppendTextDot(g_MessageMgr:FormatMessage("EQUIPTIP_SUNHAND_4"))
    end
    self.extraPropertyFrame:LayoutWnd()
end

function CLuaEquipTip:AddTitleAndSwitchButton( text, action)
    local instance = CUICommonDef.AddChild(self.contentTable.gameObject, self.titleLabelButtonTemplate)
    instance:SetActive(true)
    CommonDefs.GetComponent_Component_Type(instance.transform:Find("Label"), typeof(UILabel)).text = text
    UIEventListener.Get(instance.transform:Find("Button").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        action(self,go)
    end)
end
function CLuaEquipTip:AddTopTen(item)

    local name = ""
    local namefmt = ""
    local topname = LocalString.GetString("首")
    if item.Equip.BQPPrecious > 0 then
        namefmt = LocalString.GetString("奇珍异宝之{0}")
        local rankStr = topname
        if item.Equip.BQPToppPos > 1 then
            rankStr = Extensions.ConvertToChineseString(item.Equip.BQPToppPos)
        end
        name = System.String.Format(namefmt,rankStr)
    else
        namefmt = LocalString.GetString("十大{0}之{1}")
        local ecfg = EquipmentTemplate_Equip.GetData(item.TemplateId)
        local etypeName = LuaBingQiPuMgr.GetEquipTypeName(ecfg.Type)
        local rankStr = topname
        if item.Equip.BQPToppPos > 1 then
            rankStr = Extensions.ConvertToChineseString(item.Equip.BQPToppPos)
        end
        name = System.String.Format(namefmt,etypeName,rankStr)
    end

    local instance = CUICommonDef.AddChild(self.contentTable.gameObject, self.topTenTemplate)
    instance:SetActive(true)
    local label = CommonDefs.GetComponent_GameObject_Type(instance, typeof(UILabel))
    label.text = name
    local ts = instance.transform
    local sp1 = ts:Find("Sprite1")
    local sp2 = ts:Find("Sprite2")
    local sp3 = ts:Find("Sprite3")

    if item.Equip.BQPToppPosVersion ~= BingQiPu_Setting.GetData().Version then
        label.color = Color.gray
    else
        if item.Equip.BQPPrecious and item.Equip.BQPPrecious > 0 then --奇珍
            label.color = QualityColor.GetRGBValue(EnumQualityType.Orange)
            self:SetTopTenSprite(3,sp1,sp2,sp3)
        elseif item.Equip.BQPToppPos > 0 then
            label.color = QualityColor.GetRGBValue(EnumQualityType.Blue)
            if item.Equip.BQPToppPos <= 3 then
                self:SetTopTenSprite(2,sp1,sp2,sp3)
            else
                self:SetTopTenSprite(1,sp1,sp2,sp3)
            end
        else
            label.color = QualityColor.GetRGBValue(EnumQualityType.White)
            self:SetTopTenSprite(1,sp1,sp2,sp3)
        end
    end
end

function CLuaEquipTip:SetTopTenSprite(index,sp1,sp2,sp3)
    if sp1 then
        sp1.gameObject:SetActive(index == 1)
    end
    if sp2 then
        sp2.gameObject:SetActive(index == 2)
    end
    if sp3 then
        sp3.gameObject:SetActive(index == 3)
    end
end

function CLuaEquipTip:CheckAddFeishengSwitch( item)
    local bShowSwitchFeisheng = ((item.Equip.IsFeiShengAdjusted > 0 or (CItemInfoMgr.showType == CItemInfoMgr.ShowType.Package and not IdPartition.IdIsTalisman(item.TemplateId))) and (CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.PlayProp.FeiShengData.XianFanStatus > 0 and (CClientMainPlayer.Inst.MaxLevel >= item.Equip.Grade or item.Equip.IsFeiShengAdjusted > 0) and CClientMainPlayer.Inst.XianShenLevel < item.Equip.Grade) or self.mbPreviousFeiShengAdjusted > 0) and CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.HasFeiSheng

    bShowSwitchFeisheng = bShowSwitchFeisheng or ((item.Equip.IsFeiShengAdjusted > 0 or self.mbPreviousFeiShengAdjusted > 0) and CItemInfoMgr.showType == CItemInfoMgr.ShowType.Link)

    self.switchSplit:SetActive(bShowSwitchFeisheng)
    self.normalSplit:SetActive(not bShowSwitchFeisheng)

    if bShowSwitchFeisheng then
        local instance = CUICommonDef.AddChild(self.contentTable.gameObject, self.emptyTemplate)
        instance:SetActive(true)

        local itId = item.Id
        local trans = self.switchSplit.transform:Find("btn")
        if trans ~= nil then
            UIEventListener.Get(trans.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                local citem = CItemMgr.Inst:GetById(itId)
                if citem == nil then
                    if CItemInfoMgr.showType == CItemInfoMgr.ShowType.Link and CItemInfoMgr.linkItemInfo.item ~= nil and CItemInfoMgr.linkItemInfo.item.Id == itId then
                        citem = CItemInfoMgr.linkItemInfo.item
                    end
                end

                if citem ~= nil then
                    self.mbPreviousFeiShengAdjusted = citem.Equip.IsFeiShengAdjusted
                    citem.Equip.IsFeiShengAdjusted = (self.mbShowFeiShengAttributes and 0 or 1)
                    self:SetShowFeiShengAttributes(not self.mbShowFeiShengAttributes)
                    citem.Equip.IsFeiShengAdjusted = self.mbPreviousFeiShengAdjusted
                end
            end)
        end
    end
end

function CLuaEquipTip:SetShowFeiShengAttributes(value)
    if self.mbShowFeiShengAttributes ~= value then
        local item = nil
        if self.ItemId ~=nil then
            if CItemInfoMgr.showType == CItemInfoMgr.ShowType.Package and self.baseItemInfo ~= nil then--//必须当前是在查看包裹里的而且不是对比身上的
                if self.mbShowFeiShengAttributes then
                    item = CItemMgr.Inst:GetById(self.ItemId)
                else
                    Gac2Gas.RequestAdjustedEquipInfo(self.ItemId)
                end
            elseif(CItemInfoMgr.showType == CItemInfoMgr.ShowType.Link and CItemInfoMgr.linkItemInfo.item ~=nil and CItemInfoMgr.linkItemInfo.item.Id == self.ItemId) then
                item = CItemInfoMgr.linkItemInfo.item;
            else
                item = CItemMgr.Inst:GetById(self.ItemId)
            end
        end

        if item~=nil then
            self:Init0(item, self.baseItemInfo, self.mComparedItems)
            g_ScriptEvent:BroadcastInLua("OnEquipTipLayoutWnd")
        end
    end
    self.mbShowFeiShengAttributes = value
end

function CLuaEquipTip:AddEquipScoreInfo( text)
    local instance = CUICommonDef.AddChild(self.contentTable.gameObject, self.equipScoreTemplate)
    instance:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(CEquipScoreInfoRow)):Init(text)
end
function CLuaEquipTip:AppendText( text)
    local instance = CUICommonDef.AddChild(self.contentTable.gameObject, self.labelTemplate)
    instance:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(UILabel)).text = text
end
function CLuaEquipTip:AppendBasicPropertyText( text, result)
    local instance = CUICommonDef.AddChild(self.contentTable.gameObject, self.propertyLabelTemplate)
    instance:SetActive(true)
    local arrow = ""
    local flip = UIBasicSprite.Flip.Nothing
    if result == LuaEnumPropertyCompareResult.Greatest then
        arrow = CLuaEquipTip.ArrowUp
    elseif result == LuaEnumPropertyCompareResult.Least then
        arrow = CLuaEquipTip.ArrowDown
        flip = UIBasicSprite.Flip.Vertically
    end
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(CPropertyTextInfoRow)):Init(text, arrow, flip)
end
function CLuaEquipTip:AppendPerfection( text, perfection, intensifyLevel)
    local instance = NGUITools.AddChild(self.contentTable.gameObject, self.perfectionTemplate)
    instance:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(CPerfectionInfoRow)):Init(text, perfection, intensifyLevel)
end
function CLuaEquipTip:AddGemSlots( equipment)
    local instance = CUICommonDef.AddChild(self.contentTable.gameObject, self.basicPropertyTemplate)
    instance:SetActive(true)

    local gemTable = instance.transform:Find("GemSlots"):GetComponent(typeof(UITable))
    local gemTemplate = instance.transform:Find("Item").gameObject
    Extensions.RemoveAllChildren(gemTable.transform)

    local holeItems = {}
    if equipment.ActiveHoleSetIdx==2 then
        for i=1,equipment.Hole do
            local id = equipment.SecondaryHoleItems[i]
            if id>0 and id<100 then
                table.insert( holeItems, equipment.HoleItems[id] )
            else
                table.insert( holeItems, id )
            end
        end
    else
        for i=1,equipment.Hole do
            table.insert( holeItems, equipment.HoleItems[i] )
        end
    end

    for i=1,equipment.Hole do
        local gemObj = NGUITools.AddChild(gemTable.gameObject, gemTemplate)
        gemObj:SetActive(true)

        local locktrans = FindChild(gemObj.transform,"Lock")
        if equipment.SubHand > 0 then --副手
            if locktrans then
                locktrans.gameObject:SetActive(true)
            end
            CommonDefs.GetComponent_GameObject_Type(gemObj, typeof(CGemSlot)).Icon = nil
        else
            if locktrans then
                locktrans.gameObject:SetActive(false)
            end
            local item = CItemMgr.Inst:GetItemTemplate(holeItems[i])
            if item == nil then
                CommonDefs.GetComponent_GameObject_Type(gemObj, typeof(CGemSlot)).Icon = nil
            else
                CommonDefs.GetComponent_GameObject_Type(gemObj, typeof(CGemSlot)).Icon = item.Icon
            end
        end
    end
    gemTable:Reposition()
end
function CLuaEquipTip:ShowButtons( baseInfo)
    self.buttonsBg.gameObject:SetActive(false)
    if CLuaEquipTip.s_EnableReuseButtons then
        local childCount = self.buttonTable.transform.childCount
        do
            local i = 0
            while i < childCount do
                self.buttonTable.transform:GetChild(i).gameObject:SetActive(false)
                i = i + 1
            end
        end
    else
        Extensions.RemoveAllChildren(self.buttonTable.transform)
    end

    if baseInfo == nil then
        return
    end

    local actionPairs = baseInfo.actionPairs

    if actionPairs ~= nil and actionPairs.Count > 0 then
        do
            local i = 0
            while i < actionPairs.Count do
                local instance = nil
                if CLuaEquipTip.s_EnableReuseButtons and i < self.buttonTable.transform.childCount then
                    instance = self.buttonTable.transform:GetChild(i).gameObject
                else
                    instance = CUICommonDef.AddChild(self.buttonTable.gameObject, self.buttonTemplate)
                end
                local pair = actionPairs[i]
                if TypeIs(pair,typeof(ItemsStringActionKeyValuePair)) then
                    self:ShowDetailButtonMenu(TypeAs(pair, typeof(ItemsStringActionKeyValuePair)), baseInfo)
                end
                instance:SetActive(true)
                instance = instance.transform:Find("Button").gameObject
                CommonDefs.GetComponent_GameObject_Type(instance, typeof(CItemAndEquipTipButton)):Init(actionPairs[i].Key, false, false)
                if actionPairs[i].Value ~= nil then
                    local action = actionPairs[i].Value
                    local pair = actionPairs[i]
                    UIEventListener.Get(instance).onClick = DelegateFactory.VoidDelegate(function (go)
                        if baseInfo.dataSourceIsNull then
                            CItemInfoMgr.CloseItemInfoWnd()
                            return
                        else
                            if TypeIs(pair,typeof(ItemsStringActionKeyValuePair)) then
                                self.m_ActionPairTempDict[pair.Key] = not self.m_ActionPairTempDict[pair.Key]
                                local enableReuseButtons = CLuaEquipTip.s_EnableReuseButtons
                                CLuaEquipTip.s_EnableReuseButtons = false
                                self:ShowButtons(baseInfo)
                                CLuaEquipTip.s_EnableReuseButtons = enableReuseButtons
                                self.buttonsBg.gameObject:SetActive(true)
                                local b = NGUIMath.CalculateRelativeWidgetBounds(self.background.transform, self.buttonTable.transform)
                                self.buttonsBg.height = math.floor((b.size.y + 40))
                                self.buttonsBg.transform.localPosition = b.center
                            end
                            invoke(action)
                        end
                    end)
                end
                i = i + 1
            end
        end
    end

    self.buttonTable:Reposition()
end

--处理一个按钮下还有子层级按钮的情况
function CLuaEquipTip:ShowDetailButtonMenu(actionPair, baseInfo)
    if (not actionPair) or (not self.m_ActionPairTempDict[actionPair.Key]) then return end
    local template = self.transform:Find(SafeStringFormat3("Templates/%s", actionPair.templateName))
    if not template then
        template = self.buttonTemplate
    end
    local len = actionPair.DetailActionPairs.Count
    for i = 0,len - 1 do
        local data = actionPair.DetailActionPairs[i]
        local obj = CUICommonDef.AddChild(self.buttonTable.gameObject, template.gameObject)
        obj:SetActive(true)
        local btn = obj.transform:Find("Button"):GetComponent(typeof(CItemAndEquipTipButton))
        btn:Init(data.Key, false, false)
        if data.Value ~= nil then
            local action = data.Value
            UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                if baseInfo.dataSourceIsNull then
                    CItemInfoMgr.CloseItemInfoWnd()
                    return
                else
                    invoke(action)
                end
            end)
        end
    end
end

function CLuaEquipTip:LayoutWnd( )
    if not CItemInfoMgr.NeedAdjustHeight then
        return
    end
    local maxWidth = 0
    do
        local i = 0
        while i < self.contentTable.transform.childCount do
            local row = CommonDefs.GetComponent_Component_Type(self.contentTable.transform:GetChild(i), typeof(CBaseItemInfoRow))
            if row ~= nil then
                local rowMaxWidth = row:GetMaxContentWidth()
                if maxWidth < rowMaxWidth then
                    maxWidth = rowMaxWidth
                end
            end
            i = i + 1
        end
    end

    local actualContentWidth = math.min(math.max(maxWidth, self.minContentWidth), CLuaEquipInfoWnd.Instance.m_TipsCount ~= 2 and self.minContentWidth or self.maxContentWidth)

    do
        local i = 0
        while i < self.contentTable.transform.childCount do
            local row = CommonDefs.GetComponent_Component_Type(self.contentTable.transform:GetChild(i), typeof(CBaseItemInfoRow))
            if row ~= nil then
                row:SetContentWidth(actualContentWidth)
            end
            i = i + 1
        end
    end

    self.contentTable:Reposition()
    self.background.width = 10 --[[HorizontalPadding*2]] + math.ceil(actualContentWidth + math.abs(self.contentScrollView.panel.leftAnchor.absolute) + math.abs(self.contentScrollView.panel.rightAnchor.absolute))

    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenHeight = Screen.height * scale

    local contentTopPadding = math.abs(self.contentScrollView.panel.topAnchor.absolute)
    local contentBottomPadding = math.abs(self.contentScrollView.panel.bottomAnchor.absolute)

    local totalWndHeight = self:TotalHeightOfScrollViewContent() + contentTopPadding + contentBottomPadding + self.contentScrollView.panel.clipSoftness.y * 2
    local displayWndHeight = math.min(math.max(totalWndHeight, contentTopPadding + contentBottomPadding + 10), virtualScreenHeight)

    --设置背景高度
    self.background.height = math.ceil(displayWndHeight)

    local rects = CommonDefs.GetComponentsInChildren_Component_Type_Boolean(self.transform, typeof(UIRect), true)
    do
        local i = 0
        while i < rects.Length do
            if rects[i].updateAnchors == UIRect.AnchorUpdate.OnEnable or rects[i].updateAnchors == UIRect.AnchorUpdate.OnStart then
                rects[i]:ResetAndUpdateAnchors()
            end
            i = i + 1
        end
    end

    self.buttonTable.transform.localPosition = CommonDefs.op_Addition_Vector3_Vector3(self.background.localCorners[2], Vector3(6, - 12))
    local abandonName = LocalString.GetString("回收")
    local visibleChildCount = 0
    local lastVisibleChild = nil
    local childCount = self.buttonTable.transform.childCount
    do
        local i = 0
        while i < childCount do
            if self.buttonTable.transform:GetChild(i).gameObject.activeSelf then
                visibleChildCount = visibleChildCount + 1
                lastVisibleChild = self.buttonTable.transform:GetChild(i)
                lastVisibleChild = lastVisibleChild.transform:Find("Button").transform
            end
            i = i + 1
        end
    end
    if visibleChildCount >= 1 then
        local last = lastVisibleChild
        local abandonBtnExists = (CommonDefs.GetComponent_Component_Type(last, typeof(CItemAndEquipTipButton)).Text == abandonName)
        if abandonBtnExists and visibleChildCount > 1 then
            self.buttonsBg.gameObject:SetActive(true)
            last.gameObject:SetActive(false)
            local b = NGUIMath.CalculateRelativeWidgetBounds(self.background.transform, self.buttonTable.transform)
            self.buttonsBg.height = math.floor((b.size.y + 40 --[[20 * 2]]))
            self.buttonsBg.transform.localPosition = b.center
            last.gameObject:SetActive(true)
            Extensions.SetLocalPositionY(last, last.localPosition.y - 20)
        elseif not abandonBtnExists then
            self.buttonsBg.gameObject:SetActive(true)
            local b = NGUIMath.CalculateRelativeWidgetBounds(self.background.transform, self.buttonTable.transform)
            self.buttonsBg.height = math.floor((b.size.y + 40 --[[20 * 2]]))
            self.buttonsBg.transform.localPosition = b.center
        end
    end

    self.contentScrollView:ResetPosition()
    self.scrollViewIndicator:Layout()
    if self.baseItemInfo ~= nil then
        self.contentScrollView:SetDragAmount(0, self.baseItemInfo.dragAmount, false)
    end

    self.background.transform.localPosition = Vector3.zero
end
function CLuaEquipTip:TotalHeightOfScrollViewContent( )
    return NGUIMath.CalculateRelativeWidgetBounds(self.contentTable.transform).size.y + self.contentTable.padding.y * 2
end
function CLuaEquipTip:AppendPhysicalAttack( equipment, comparedItems)
    local strength = ""
    local intensifyTotalValue = 0
    if self.mbShowFeiShengAttributes then
        intensifyTotalValue = equipment:GetIntensifyTotalValueByLevel(equipment.AdjustedFeiShengIntensifyLevel)
    else
        intensifyTotalValue = equipment:GetIntensifyTotalValue()
    end
    if intensifyTotalValue > 0 then
        strength = SafeStringFormat3("+%d%%", intensifyTotalValue)
    end

    local result = LuaEnumPropertyCompareResult.Undefined
    if comparedItems ~= nil and #comparedItems > 0 then
        local greateast = true
        local least = true
        do
            local i = 0
            while i < #comparedItems do
                if equipment.AdjpAttMax_Current > comparedItems[i+1].AdjpAttMax_Current
                or ((equipment.AdjpAttMax_Current == comparedItems[i+1].AdjpAttMax_Current) and equipment.AdjpAttMin_Current > comparedItems[i+1].AdjpAttMin_Current) then
                    least = false
                end
                if equipment.AdjpAttMax_Current < comparedItems[i+1].AdjpAttMax_Current
                or ((equipment.AdjpAttMax_Current == comparedItems[i+1].AdjpAttMax_Current) and equipment.AdjpAttMin_Current < comparedItems[i+1].AdjpAttMin_Current) then
                    greateast = false
                end
                i = i + 1
            end
        end

        if greateast and not least then
            result = LuaEnumPropertyCompareResult.Greatest
        elseif not greateast and least then
            result = LuaEnumPropertyCompareResult.Least
        end
    end


    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    物理攻击 {0}-{1}[-][00FFFF]{2}[-]"), equipment.AdjpAttMin_Current, equipment.AdjpAttMax_Current, strength), result)
end
function CLuaEquipTip:AppendMagicalAttack( equipment, comparedItems)
    local strength = ""
    local intensifyTotalValue = 0
    if self.mbShowFeiShengAttributes then
        intensifyTotalValue = equipment:GetIntensifyTotalValueByLevel(equipment.AdjustedFeiShengIntensifyLevel)
    else
        intensifyTotalValue = equipment:GetIntensifyTotalValue()
    end
    if intensifyTotalValue > 0 then
        strength = SafeStringFormat3("+%d%%", intensifyTotalValue)
    end

    local result = LuaEnumPropertyCompareResult.Undefined
    if comparedItems ~= nil and #comparedItems > 0 then
        local greateast = true
        local least = true
        do
            local i = 0
            while i < #comparedItems do
                if equipment.AdjmAttMax_Current > comparedItems[i+1].AdjmAttMax_Current or ((equipment.AdjmAttMax_Current == comparedItems[i+1].AdjmAttMax_Current) and equipment.AdjmAttMin_Current > comparedItems[i+1].AdjmAttMin_Current) then
                    least = false
                end
                if equipment.AdjmAttMax_Current < comparedItems[i+1].AdjmAttMax_Current or ((equipment.AdjmAttMax_Current == comparedItems[i+1].AdjmAttMax_Current) and equipment.AdjmAttMin_Current < comparedItems[i+1].AdjmAttMin_Current) then
                    greateast = false
                end
                i = i + 1
            end
        end
        if greateast and not least then
            result = LuaEnumPropertyCompareResult.Greatest
        elseif not greateast and least then
            result = LuaEnumPropertyCompareResult.Least
        end
    end

    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    法术攻击 {0}-{1}[-][00FFFF]{2}[-]"), equipment.AdjmAttMin_Current, equipment.AdjmAttMax_Current, strength), result)
end
function CLuaEquipTip:AppendAttackSpeed( equipment, comparedItems)
    local result = LuaEnumPropertyCompareResult.Undefined
    if comparedItems ~= nil and #comparedItems > 0 then
        local greateast = true
        local least = true
        do
            local i = 0
            while i < #comparedItems do
                if equipment.AdjAttSpeed_Current > comparedItems[i+1].AdjAttSpeed_Current then
                    least = false
                end
                if equipment.AdjAttSpeed_Current < comparedItems[i+1].AdjAttSpeed_Current then
                    greateast = false
                end
                i = i + 1
            end
        end
        if greateast and not least then
            result = LuaEnumPropertyCompareResult.Greatest
        elseif not greateast and least then
            result = LuaEnumPropertyCompareResult.Least
        end
    end

    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    攻击速度 {0:f3}[-]"), equipment.AdjAttSpeed_Current), result)
end
function CLuaEquipTip:AppendPhysicalDef( equipment, comparedItems)
    local strength = ""
    local intensifyTotalValue = 0
    if self.mbShowFeiShengAttributes then
        intensifyTotalValue = equipment:GetIntensifyTotalValueByLevel(equipment.AdjustedFeiShengIntensifyLevel)
    else
        intensifyTotalValue = equipment:GetIntensifyTotalValue()
    end
    if intensifyTotalValue > 0 then
        strength = SafeStringFormat3("+%d%%", intensifyTotalValue)
    end

    local result = LuaEnumPropertyCompareResult.Undefined
    if comparedItems ~= nil and #comparedItems > 0 then
        local greateast = true
        local least = true
        do
            local i = 0
            while i < #comparedItems do
                if equipment.AdjpDef_Current > comparedItems[i+1].AdjpDef_Current then
                    least = false
                end
                if equipment.AdjpDef_Current < comparedItems[i+1].AdjpDef_Current then
                    greateast = false
                end
                i = i + 1
            end
        end
        if greateast and not least then
            result = LuaEnumPropertyCompareResult.Greatest
        elseif not greateast and least then
            result = LuaEnumPropertyCompareResult.Least
        end
    end

    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    物理防御 {0}[-][00FFFF]{1}[-]"), equipment.AdjpDef_Current, strength), result)
end
function CLuaEquipTip:AppendMagicalDef( equipment, comparedItems)
    local strength = ""
    local intensifyTotalValue = 0
    if self.mbShowFeiShengAttributes then
        intensifyTotalValue = equipment:GetIntensifyTotalValueByLevel(equipment.AdjustedFeiShengIntensifyLevel)
    else
        intensifyTotalValue = equipment:GetIntensifyTotalValue()
    end
    if intensifyTotalValue > 0 then
        strength = SafeStringFormat3("+%d%%", intensifyTotalValue)
    end

    local result = LuaEnumPropertyCompareResult.Undefined
    if comparedItems ~= nil and #comparedItems > 0 then
        local greateast = true
        local least = true
        do
            local i = 0
            while i < #comparedItems do
                if equipment.AdjmDef_Current > comparedItems[i+1].AdjmDef_Current then
                    least = false
                end
                if equipment.AdjmDef_Current < comparedItems[i+1].AdjmDef_Current then
                    greateast = false
                end
                i = i + 1
            end
        end
        if greateast and not least then
            result = LuaEnumPropertyCompareResult.Greatest
        elseif not greateast and least then
            result = LuaEnumPropertyCompareResult.Least
        end
    end

    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    法术防御 {0}[-][00FFFF]{1}[-]"), equipment.AdjmDef_Current, strength), result)
end
function CLuaEquipTip:AppendBlock( equipment, comparedItems)
    local result = LuaEnumPropertyCompareResult.Undefined
    if comparedItems ~= nil and #comparedItems > 0 then
        local greateast = true
        local least = true
        do
            local i = 0
            while i < #comparedItems do
                if equipment.AdjBlock_Current > comparedItems[i+1].AdjBlock_Current then
                    least = false
                end
                if equipment.AdjBlock_Current < comparedItems[i+1].AdjBlock_Current then
                    greateast = false
                end
                i = i + 1
            end
        end
        if greateast and not least then
            result = LuaEnumPropertyCompareResult.Greatest
        elseif not greateast and least then
            result = LuaEnumPropertyCompareResult.Least
        end
    end
    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    格挡 {0}[-]"), equipment.AdjBlock_Current), result)
end
function CLuaEquipTip:AppendBlockDamage( equipment, comparedItems)
    local result = LuaEnumPropertyCompareResult.Undefined
    if comparedItems ~= nil and #comparedItems > 0 then
        local greateast = true
        local least = true
        do
            local i = 0
            while i < #comparedItems do
                if equipment.AdjBlockDamage_Current > comparedItems[i+1].AdjBlockDamage_Current then
                    least = false
                end
                if equipment.AdjBlockDamage_Current < comparedItems[i+1].AdjBlockDamage_Current then
                    greateast = false
                end
                i = i + 1
            end
        end
        if greateast and not least then
            result = LuaEnumPropertyCompareResult.Greatest
        elseif not greateast and least then
            result = LuaEnumPropertyCompareResult.Least
        end
    end
    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    格挡伤害减免 {0}[-]"), equipment.AdjBlockDamage_Current), result)
end

function CLuaEquipTip:AppendPhysicalHit( equipment, comparedItems)
    local result = LuaEnumPropertyCompareResult.Undefined
    if comparedItems ~= nil and #comparedItems > 0 then
        local greateast = true
        local least = true
        do
            local i = 0
            while i < #comparedItems do
                if equipment.AdjpHit_Current > comparedItems[i+1].AdjpHit_Current then
                    least = false
                end
                if equipment.AdjpHit_Current < comparedItems[i+1].AdjpHit_Current then
                    greateast = false
                end
                i = i + 1
            end
        end
        if greateast and not least then
            result = LuaEnumPropertyCompareResult.Greatest
        elseif not greateast and least then
            result = LuaEnumPropertyCompareResult.Least
        end
    end
    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    物理命中 {0}[-]"), equipment.AdjpHit_Current), result)
end

function CLuaEquipTip:AppendMagicalHit( equipment, comparedItems)
    local result = LuaEnumPropertyCompareResult.Undefined
    if comparedItems ~= nil and #comparedItems > 0 then
        local greateast = true
        local least = true
        do
            local i = 0
            while i < #comparedItems do
                if equipment.AdjmHit_Current > comparedItems[i+1].AdjmHit_Current then
                    least = false
                end
                if equipment.AdjmHit_Current < comparedItems[i+1].AdjmHit_Current then
                    greateast = false
                end
                i = i + 1
            end
        end
        if greateast and not least then
            result = LuaEnumPropertyCompareResult.Greatest
        elseif not greateast and least then
            result = LuaEnumPropertyCompareResult.Least
        end
    end
    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    法术命中 {0}[-]"), equipment.AdjmHit_Current), result)
end

function CLuaEquipTip:AppendPhysicalMiss( equipment, comparedItems)
    local result = LuaEnumPropertyCompareResult.Undefined
    if comparedItems ~= nil and #comparedItems > 0 then
        local greateast = true
        local least = true
        do
            local i = 0
            while i < #comparedItems do
                if equipment.AdjpMiss_Current > comparedItems[i+1].AdjpMiss_Current then
                    least = false
                end
                if equipment.AdjpMiss_Current < comparedItems[i+1].AdjpMiss_Current then
                    greateast = false
                end
                i = i + 1
            end
        end
        if greateast and not least then
            result = LuaEnumPropertyCompareResult.Greatest
        elseif not greateast and least then
            result = LuaEnumPropertyCompareResult.Least
        end
    end

    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    物理躲避 {0}[-]"), equipment.AdjpMiss_Current), result)
end

function CLuaEquipTip:AppendMagicalMiss( equipment, comparedItems)
    local result = LuaEnumPropertyCompareResult.Undefined
    if comparedItems ~= nil and #comparedItems > 0 then
        local greateast = true
        local least = true
        do
            local i = 0
            while i < #comparedItems do
                if equipment.AdjmMiss_Current > comparedItems[i+1].AdjmMiss_Current then
                    least = false
                end
                if equipment.AdjmMiss_Current < comparedItems[i+1].AdjmMiss_Current then
                    greateast = false
                end
                i = i + 1
            end
        end
        if greateast and not least then
            result = LuaEnumPropertyCompareResult.Greatest
        elseif not greateast and least then
            result = LuaEnumPropertyCompareResult.Least
        end
    end

    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    法术躲避 {0}[-]"), equipment.AdjmMiss_Current), result)
end

function CLuaEquipTip:AppendHpFull( equipment, comparedItems)
    local strength = ""
    local intensifyTotalValue = 0
    if self.mbShowFeiShengAttributes then
        intensifyTotalValue = equipment:GetIntensifyTotalValueByLevel(equipment.AdjustedFeiShengIntensifyLevel)
    else
        intensifyTotalValue = equipment:GetIntensifyTotalValue()
    end
    if intensifyTotalValue > 0 then
        strength = SafeStringFormat3("+%d%%", intensifyTotalValue)
    end

    local result = LuaEnumPropertyCompareResult.Undefined
    if comparedItems ~= nil and #comparedItems > 0 then
        local greateast = true
        local least = true
        do
            local i = 0
            while i < #comparedItems do
                if equipment.MulHpFull_Current > comparedItems[i+1].MulHpFull_Current then
                    least = false
                end
                if equipment.MulHpFull_Current < comparedItems[i+1].MulHpFull_Current then
                    greateast = false
                end
                i = i + 1
            end
        end
        if greateast and not least then
            result = LuaEnumPropertyCompareResult.Greatest
        elseif not greateast and least then
            result = LuaEnumPropertyCompareResult.Least
        end
    end

    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    气血上限 {0:f3}%[-][00FFFF]{1}[-]"), equipment.MulHpFull_Current * 100, strength), result)
end

function CLuaEquipTip:AppendAdjHpFull2( equipment, comparedItems)
    local strength = ""
    local intensifyValue = equipment:GetForgeIntensifyValue()

    if intensifyValue > 0 then
        strength = SafeStringFormat3("+%d%%", intensifyValue)
    end

    local result = LuaEnumPropertyCompareResult.Undefined
    if comparedItems ~= nil and #comparedItems > 0 then
        local greateast = true
        local least = true
        do
            local i = 0
            while i < #comparedItems do
                if math.floor(equipment.AdjHpFull2_Current+0.5) > math.floor(comparedItems[i+1].AdjHpFull2_Current+0.5) then
                    least = false
                end
                if math.floor(equipment.AdjHpFull2_Current+0.5) < math.floor(comparedItems[i+1].AdjHpFull2_Current+0.5) then
                    greateast = false
                end
                i = i + 1
            end
        end
        if greateast and not least then
            result = LuaEnumPropertyCompareResult.Greatest
        elseif not greateast and least then
            result = LuaEnumPropertyCompareResult.Least
        end
    end

    if strength == "" then
        self:AppendBasicPropertyText(SafeStringFormat3(LocalString.GetString("[FFFFFF]    最终气血上限 %d[-]"), math.floor(equipment.AdjHpFull2_Current+0.5)), result)
    else
        self:AppendBasicPropertyText(SafeStringFormat3(LocalString.GetString("[FFFFFF]    最终气血上限 %d[-][00FFFF]%s[-]"), math.floor(equipment.AdjHpFull2_Current+0.5), strength), result)
    end
end

-- 最终物理攻击加成
function CLuaEquipTip:AppendAdjpAtt2( equipment, comparedItems)
    local strength = ""
    local intensifyValue = equipment:GetForgeIntensifyValue()
    if intensifyValue > 0 then
        strength = SafeStringFormat3("+%d%%", intensifyValue)
    end

    local result = LuaEnumPropertyCompareResult.Undefined
    if comparedItems ~= nil and #comparedItems > 0 then
        local greateast = true
        local least = true
        do
            local i = 0
            while i < #comparedItems do
                if equipment.AdjpAttMax2_Current > comparedItems[i+1].AdjpAttMax2_Current
                or ((equipment.AdjpAttMax2_Current == comparedItems[i+1].AdjpAttMax2_Current) and equipment.AdjpAttMin2_Current > comparedItems[i+1].AdjpAttMin2_Current) then
                    least = false
                end
                if equipment.AdjpAttMax2_Current < comparedItems[i+1].AdjpAttMax2_Current
                or ((equipment.AdjpAttMax2_Current == comparedItems[i+1].AdjpAttMax2_Current) and equipment.AdjpAttMin2_Current < comparedItems[i+1].AdjpAttMin2_Current) then
                    greateast = false
                end
                i = i + 1
            end
        end

        if greateast and not least then
            result = LuaEnumPropertyCompareResult.Greatest
        elseif not greateast and least then
            result = LuaEnumPropertyCompareResult.Least
        end
    end


    self:AppendBasicPropertyText(SafeStringFormat3(LocalString.GetString("[FFFFFF]    最终物理攻击 %d-%d[-][00FFFF]%s[-]"), equipment.AdjpAttMin2_Current, equipment.AdjpAttMax2_Current, strength), result)
end

-- 最终法术攻击
function CLuaEquipTip:AppendAdjmAtt2( equipment, comparedItems)
    local strength = ""
    local intensifyValue = equipment:GetForgeIntensifyValue()
    if intensifyValue > 0 then
        strength = SafeStringFormat3("+%d%%", intensifyValue)
    end

    local result = LuaEnumPropertyCompareResult.Undefined
    if comparedItems ~= nil and #comparedItems > 0 then
        local greateast = true
        local least = true
        do
            local i = 0
            while i < #comparedItems do
                if equipment.AdjmAttMax2_Current > comparedItems[i+1].AdjmAttMax2_Current
                or ((equipment.AdjmAttMax2_Current == comparedItems[i+1].AdjmAttMax2_Current) and equipment.AdjmAttMin2_Current > comparedItems[i+1].AdjmAttMin2_Current) then
                    least = false
                end
                if equipment.AdjmAttMax2_Current < comparedItems[i+1].AdjmAttMax2_Current
                or ((equipment.AdjmAttMax2_Current == comparedItems[i+1].AdjmAttMax2_Current) and equipment.AdjmAttMin2_Current < comparedItems[i+1].AdjmAttMin2_Current) then
                    greateast = false
                end
                i = i + 1
            end
        end

        if greateast and not least then
            result = LuaEnumPropertyCompareResult.Greatest
        elseif not greateast and least then
            result = LuaEnumPropertyCompareResult.Least
        end
    end


    self:AppendBasicPropertyText(SafeStringFormat3(LocalString.GetString("[FFFFFF]    最终法术攻击 %d-%d[-][00FFFF]%s[-]"), equipment.AdjmAttMin2_Current, equipment.AdjmAttMax2_Current, strength), result)
end

-- 最终物理防御
function CLuaEquipTip:AppendAdjpDef2( equipment, comparedItems)
    local strength = ""
    local intensifyValue = equipment:GetForgeIntensifyValue()

    if intensifyValue > 0 then
        strength = SafeStringFormat3("+%d%%", intensifyValue)
    end

    local result = LuaEnumPropertyCompareResult.Undefined
    if comparedItems ~= nil and #comparedItems > 0 then
        local greateast = true
        local least = true
        do
            local i = 0
            while i < #comparedItems do
                if math.floor(equipment.AdjpDef2_Current+0.5) > math.floor(comparedItems[i+1].AdjpDef2_Current+0.5) then
                    least = false
                end
                if math.floor(equipment.AdjpDef2_Current+0.5) < math.floor(comparedItems[i+1].AdjpDef2_Current+0.5) then
                    greateast = false
                end
                i = i + 1
            end
        end
        if greateast and not least then
            result = LuaEnumPropertyCompareResult.Greatest
        elseif not greateast and least then
            result = LuaEnumPropertyCompareResult.Least
        end
    end

    if strength == "" then
        self:AppendBasicPropertyText(SafeStringFormat3(LocalString.GetString("[FFFFFF]    最终物理防御 %d[-]"), math.floor(equipment.AdjpDef2_Current+0.5)), result)
    else
        self:AppendBasicPropertyText(SafeStringFormat3(LocalString.GetString("[FFFFFF]    最终物理防御 %d[-][00FFFF]%s[-]"), math.floor(equipment.AdjpDef2_Current+0.5), strength), result)
    end
end

-- 最终法术防御
function CLuaEquipTip:AppendAdjmDef2( equipment, comparedItems)
    local strength = ""
    local intensifyValue = equipment:GetForgeIntensifyValue()

    if intensifyValue > 0 then
        strength = SafeStringFormat3("+%d%%", intensifyValue)
    end

    local result = LuaEnumPropertyCompareResult.Undefined
    if comparedItems ~= nil and #comparedItems > 0 then
        local greateast = true
        local least = true
        do
            local i = 0
            while i < #comparedItems do
                if math.floor(equipment.AdjmDef2_Current+0.5) > math.floor(comparedItems[i+1].AdjmDef2_Current+0.5) then
                    least = false
                end
                if math.floor(equipment.AdjmDef2_Current+0.5) < math.floor(comparedItems[i+1].AdjmDef2_Current+0.5) then
                    greateast = false
                end
                i = i + 1
            end
        end
        if greateast and not least then
            result = LuaEnumPropertyCompareResult.Greatest
        elseif not greateast and least then
            result = LuaEnumPropertyCompareResult.Least
        end
    end

    if strength == "" then
        self:AppendBasicPropertyText(SafeStringFormat3(LocalString.GetString("[FFFFFF]    最终法术防御 %d[-]"), math.floor(equipment.AdjmDef2_Current+0.5)), result)
    else
        self:AppendBasicPropertyText(SafeStringFormat3(LocalString.GetString("[FFFFFF]    最终法术防御 %d[-][00FFFF]%s[-]"), math.floor(equipment.AdjmDef2_Current+0.5), strength), result)
    end
end

-- 抗物理致命
function CLuaEquipTip:AppendAdjAntipFatal2( equipment, comparedItems)
    local result = LuaEnumPropertyCompareResult.Undefined
    if comparedItems ~= nil and #comparedItems > 0 then
        local greateast = true
        local least = true
        do
            local i = 0
            while i < #comparedItems do
                if math.floor(equipment.AdjAntipFatal2_Current+0.5) > math.floor(comparedItems[i+1].AdjAntipFatal2_Current+0.5) then
                    least = false
                end
                if math.floor(equipment.AdjAntipFatal2_Current+0.5) < math.floor(comparedItems[i+1].AdjAntipFatal2_Current+0.5) then
                    greateast = false
                end
                i = i + 1
            end
        end
        if greateast and not least then
            result = LuaEnumPropertyCompareResult.Greatest
        elseif not greateast and least then
            result = LuaEnumPropertyCompareResult.Least
        end
    end
    self:AppendBasicPropertyText(SafeStringFormat3(LocalString.GetString("[FFFFFF]    抗物理致命 %d[-]"), math.floor(equipment.AdjAntipFatal2_Current+0.5)), result)
end

-- 抗法术致命
function CLuaEquipTip:AppendAdjAntimFatal2( equipment, comparedItems)
    local result = LuaEnumPropertyCompareResult.Undefined
    if comparedItems ~= nil and #comparedItems > 0 then
        local greateast = true
        local least = true
        do
            local i = 0
            while i < #comparedItems do
                if math.floor(equipment.AdjAntimFatal2_Current+0.5) > math.floor(comparedItems[i+1].AdjAntimFatal2_Current+0.5) then
                    least = false
                end
                if math.floor(equipment.AdjAntimFatal2_Current+0.5) < math.floor(comparedItems[i+1].AdjAntimFatal2_Current+0.5) then
                    greateast = false
                end
                i = i + 1
            end
        end
        if greateast and not least then
            result = LuaEnumPropertyCompareResult.Greatest
        elseif not greateast and least then
            result = LuaEnumPropertyCompareResult.Least
        end
    end
    self:AppendBasicPropertyText(SafeStringFormat3(LocalString.GetString("[FFFFFF]    抗法术致命 %d[-]"), math.floor(equipment.AdjAntimFatal2_Current+0.5)), result)
end

function CLuaEquipTip:AppendPerfectionDegree( equipment)
    local perfectionDict = InitializeDict(CreateFromClass(MakeGenericClass(Dictionary, Int32, String)), Int32, String, 1, LocalString.GetString("☆"), 2, LocalString.GetString("★"), 3, LocalString.GetString("★☆"), 4, LocalString.GetString("★★"), 5, LocalString.GetString("★★☆"), 6, LocalString.GetString("★★★"), 7, LocalString.GetString("★★★☆"), 8, LocalString.GetString("★★★★"), 9, LocalString.GetString("★★★★☆"), 10, LocalString.GetString("★★★★★"))
    --if(perfectionDict.ContainsKey(equipment.Perfection))
    --    self.AppendText(string.Format("[FFFFFF]升级完美度:[-][00FFFF]{0}[-]", perfectionDict[equipment.Perfection]));

    if self.mbShowFeiShengAttributes then
        self:AppendPerfection(System.String.Format(LocalString.GetString("[{0}]升级完美度 [-][00FFFF]"), self.titleColor), equipment.Perfection, equipment.AdjustedFeiShengIntensifyLevel)
    else
        self:AppendPerfection(System.String.Format(LocalString.GetString("[{0}]升级完美度 [-][00FFFF]"), self.titleColor), equipment.Perfection, equipment.IntensifyLevel)
    end
end
function CLuaEquipTip:AppendDuration( equipment)
    if IdPartition.IdIsTalisman(equipment.TemplateId) then
        return
    end
    local percentage = (equipment.Duration / equipment.MaxDuration) * 100
    local displayDuration = equipment:GetDisplayDuration()
    local displayMaxDuration = equipment:GetDisplayMaxDuration()

    local setting = GameSetting_Client.GetData()

    local value1 = setting.DURATION_VALUE_1
    local value2 = setting.DURATION_VALUE_2
    local value1_color = setting.DURATION_VALUE_1_COLOR
    local value2_color = setting.DURATION_VALUE_2_COLOR

    local str = ""
        if percentage <= value2 then
            str = SafeStringFormat3(LocalString.GetString("[%s]耐久度          [-][%s]%s/%s[-]"), self.titleColor, value2_color, displayDuration, displayMaxDuration)
        elseif percentage <= value1 then
            str = SafeStringFormat3(LocalString.GetString("[%s]耐久度          [-][%s]%s/%s[-]"), self.titleColor, value1_color, displayDuration, displayMaxDuration)
        else
            str = SafeStringFormat3(LocalString.GetString("[%s]耐久度          [-][FFFFFF]%s/%s[-]"), self.titleColor, displayDuration, displayMaxDuration)
        end

    if self.baseItemInfo and self.baseItemInfo.itemId == self.ItemId then --当前的装备
        if CClientMainPlayer.Inst and equipment.OwnerId == CClientMainPlayer.Inst.Id and equipment.SubHand > 0 then--自己的副手装备
            local showtype = 0
            local weapon= CItemMgr.Inst:GetEquipByBodyPos(LuaEnumBodyPosition.Weapon)--主武器
            local fushou= CItemMgr.Inst:GetEquipByBodyPos(LuaEnumBodyPosition.Shield)--副武器
            if fushou and fushou.Id == equipment.Id then--当前副手已装备
                if not CLuaItemMgr.HaveCanotFixWord(fushou) then --副手没有不可修复词条
                    showtype = 0 
                else--副手包含不可修复词条
                    if weapon == nil then--未装备主手
                        showtype = 1
                    else
                        local kunwulv = self:GetKunWuGemLv(weapon)--主手昆吾石等级
                        if kunwulv == 0 then --无昆吾石
                            showtype = 1
                        elseif kunwulv < 6 then
                            showtype = 2
                        else
                            showtype = 3
                        end
                    end
                end
            else--在背包中
                showtype = 0 
            end
            self:AddTitleAndTipBtn(str,showtype)
            return
        end
    end
    self:AddTitle(str)
end

function CLuaEquipTip:GetKunWuGemLv(equip)
    local gems = equip.HoleItems
    for i=0,gems.Length-1 do
        if gems[i] >= 21000475 and gems[i] <=21000484 then
            return gems[i] - 21000475 + 1
        end
    end
    return 0
end

function CLuaEquipTip:AppendPhysicalAttackHoly( equipment, comparedItems)
    local strength = ""
    local intensifyTotalValue = equipment:GetIntensifyTotalValue()
    if intensifyTotalValue > 0 then
        strength = SafeStringFormat3("+%d%%", intensifyTotalValue)
    end

    local result = LuaEnumPropertyCompareResult.Undefined
    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    物理攻击 {0}-{1}[-][00FFFF]{2}[-]"), equipment.AdjpAttMinHoly_Current, equipment.AdjpAttMaxHoly_Current, strength), result)
end
function CLuaEquipTip:AppendMagicalAttackHoly( equipment, comparedItems)
    local strength = ""
    local intensifyTotalValue = equipment:GetIntensifyTotalValue()
    if intensifyTotalValue > 0 then
        strength = SafeStringFormat3("+%d%%", intensifyTotalValue)
    end

    local result = LuaEnumPropertyCompareResult.Undefined
    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    法术攻击 {0}-{1}[-][00FFFF]{2}[-]"), equipment.AdjmAttMinHoly_Current, equipment.AdjmAttMaxHoly_Current, strength), result)
end
function CLuaEquipTip:AppendPhysicalDefHoly( equipment, comparedItems)
    local strength = ""
    local intensifyTotalValue = equipment:GetIntensifyTotalValue()
    if intensifyTotalValue > 0 then
        strength = SafeStringFormat3("+%d%%", intensifyTotalValue)
    end

    local result = LuaEnumPropertyCompareResult.Undefined
    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    物理防御 {0}[-][00FFFF]{1}[-]"), equipment.AdjpDefHoly_Current, strength), result)
end
function CLuaEquipTip:AppendMagicalDefHoly( equipment, comparedItems)
    local strength = ""
    local intensifyTotalValue = equipment:GetIntensifyTotalValue()
    if intensifyTotalValue > 0 then
        strength = SafeStringFormat3("+%d%%", intensifyTotalValue)
    end

    local result = LuaEnumPropertyCompareResult.Undefined
    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    法术防御 {0}[-][00FFFF]{1}[-]"), equipment.AdjmDefHoly_Current, strength), result)
end
function CLuaEquipTip:AppendBlockHoly( equipment, comparedItems)
    local result = LuaEnumPropertyCompareResult.Undefined
    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    格挡 {0}[-]"), equipment.AdjBlockHoly_Current), result)
end
function CLuaEquipTip:AppendBlockDamageHoly( equipment, comparedItems)
    local result = LuaEnumPropertyCompareResult.Undefined
    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    格挡伤害减免 {0}[-]"), equipment.AdjBlockDamageHoly_Current), result)
end
function CLuaEquipTip:AppendPhysicalHitHoly( equipment, comparedItems)
    local result = LuaEnumPropertyCompareResult.Undefined
    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    物理命中 {0}[-]"), equipment.AdjpHitHoly_Current), result)
end
function CLuaEquipTip:AppendMagicalHitHoly( equipment, comparedItems)
    local result = LuaEnumPropertyCompareResult.Undefined
    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    法术命中 {0}[-]"), equipment.AdjmHitHoly_Current), result)
end
function CLuaEquipTip:AppendPhysicalMissHoly( equipment, comparedItems)
    local result = LuaEnumPropertyCompareResult.Undefined
    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    物理躲避 {0}[-]"), equipment.AdjpMissHoly_Current), result)
end
function CLuaEquipTip:AppendMagicalMissHoly( equipment, comparedItems)
    local result = LuaEnumPropertyCompareResult.Undefined
    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    法术躲避 {0}[-]"), equipment.AdjmMissHoly_Current), result)
end
function CLuaEquipTip:AppendHpFullHoly( equipment, comparedItems)
    local strength = ""
    local intensifyTotalValue = equipment:GetIntensifyTotalValue()
    if intensifyTotalValue > 0 then
        strength = SafeStringFormat3("+%d%%", intensifyTotalValue)
    end

    local result = LuaEnumPropertyCompareResult.Undefined
    self:AppendBasicPropertyText(System.String.Format(LocalString.GetString("[FFFFFF]    气血上限 {0:f3}%[-][00FFFF]{1}[-]"), equipment.MulHpFullHoly_Current * 100, strength), result)
end
function CLuaEquipTip:OnReplyAdjustedEquipInfo( args)
    local adjustedEquip = args[0]
    if adjustedEquip.Id == self.ItemId then
        local item = adjustedEquip

        if item ~= nil and self.baseItemInfo ~= nil and self.mbShowFeiShengAttributes then
            self:Init0(item, self.baseItemInfo, self.mComparedItems)
	        g_ScriptEvent:BroadcastInLua("OnEquipTipLayoutWnd")
        end
    end
end
function CLuaEquipTip:OnEnable( )
    g_ScriptEvent:AddListener("RequestCanSwitchEquipWordSetReturn",self,"OnRequestCanSwitchEquipWordSetReturn")
    g_ScriptEvent:AddListener("OnReplyAdjustedEquipInfo",self,"OnReplyAdjustedEquipInfo")
    g_ScriptEvent:AddListener("RequestCanSwitchEquipHoleSetReturn",self,"OnRequestCanSwitchEquipHoleSetReturn")
	g_ScriptEvent:AddListener("UpdateAttrGroupAt", self, "OnUpdateAttrGroupAt")

end
function CLuaEquipTip:OnDisable( )
    g_ScriptEvent:RemoveListener("RequestCanSwitchEquipWordSetReturn",self,"OnRequestCanSwitchEquipWordSetReturn")
    g_ScriptEvent:RemoveListener("OnReplyAdjustedEquipInfo",self,"OnReplyAdjustedEquipInfo")
    g_ScriptEvent:RemoveListener("RequestCanSwitchEquipHoleSetReturn",self,"OnRequestCanSwitchEquipHoleSetReturn")
	g_ScriptEvent:RemoveListener("UpdateAttrGroupAt", self, "OnUpdateAttrGroupAt")
end

function CLuaEquipTip:OnUpdateAttrGroupAt(groupIdx,eItem,group)
    if eItem ==LuaEnumAttrGroupItem.BodyEquipWordSetIdx or eItem ==LuaEnumAttrGroupItem.BodyEquipHoleSetIdx then
        --重新请求刷新
        local itemInfo = CItemMgr.Inst:GetItemInfo(self.ItemId)
        if itemInfo then
            local pos = itemInfo.pos
            Gac2Gas.RequestEquipInfoInAttrGroup(self.ItemId,group.BodyEquipWordSetIdxes[pos],group.BodyEquipHoleSetIdxes[pos])
        end
    end
end

function CLuaEquipTip:OnRequestCanSwitchEquipWordSetReturn( place, pos, targetEquipId, equipNewGrade, result)
    if self.m_SwitchWordSetBotton ~= nil then
        CUICommonDef.SetActive(self.m_SwitchWordSetBotton, true, true)
    end

    if not result then
        return
    end

    if targetEquipId == self.ItemId then
        local itemInfo = CItemMgr.Inst:GetItemInfo(self.ItemId)
        if itemInfo == nil then
            return
        end

        --尝试修复一下打造状态
        if not CUIManager.IsLoaded(CUIResources.EquipmentProcessWnd) and CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("EquipForge")) == 1 then
            CStatusMgr.Inst:SetStatusWithSync(CClientMainPlayer.Inst, EPropStatus.GetId("EquipForge"), 0)
        end

        local level = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.FinalMaxLevelForEquip or 0
        if place == EnumItemPlace.Body and equipNewGrade > level then
            --不能替换
        else
            Gac2Gas.RequestSwitchEquipWordSet(EnumToInt(itemInfo.place), itemInfo.pos, self.ItemId)
        end
    end
end

function CLuaEquipTip:OnRequestCanSwitchEquipHoleSetReturn(place, pos, equipId, equipNewGrade, canSwitch)
    if self.m_SwitchHoleSetBotton ~= nil then
        CUICommonDef.SetActive(self.m_SwitchHoleSetBotton, true, true)
    end
    if not canSwitch then
        return
    end

    if equipId == self.ItemId then
        local itemInfo = CItemMgr.Inst:GetItemInfo(self.ItemId)
        if itemInfo == nil then
            return
        end

        local level = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.FinalMaxLevelForEquip or 0
        if place == EnumItemPlace.Body and equipNewGrade > level then
            --不能替换
        else
			local equipItem = CItemMgr.Inst:GetById(self.ItemId)
			if not equipItem then return end

			local equip = equipItem.Equip

			local hasOtherJewel
			for i=1,equip.Hole do
				local otherJewelId = equip.ActiveHoleSetIdx==2 and equip.HoleItems[i] or equip.SecondaryHoleItems[i]
				if otherJewelId > 0 then
					hasOtherJewel = true
					break
				end
			end
			if not hasOtherJewel then
				g_MessageMgr:ShowMessage("Double_EquipStones_NoJewel_InOtherSet")
				return
			end
            Gac2Gas.RequestSwitchEquipHoleSet(EnumToInt(itemInfo.place), itemInfo.pos, self.ItemId)
        end
    end
end

function CLuaEquipTip:GetEquipButton( )
    if self.baseItemInfo ~= nil then
        do
            local i = 0
            while i < self.buttonTable.transform.childCount do
                local child = self.buttonTable.transform:GetChild(i)
                if (CommonDefs.GetComponentInChildren_Component_Type(child, typeof(UILabel)).text == LocalString.GetString("装备")) then
                    local button = CommonDefs.GetComponentInChildren_GameObject_Type(child.gameObject, typeof(CButton))
                    if button then
                        return button.gameObject
                    end
                end
                i = i + 1
            end
        end
    end
    return nil
end

function CLuaEquipTip:GetTransferButton( )
    if self.baseItemInfo ~= nil then
        do
            local i = 0
            while i < self.buttonTable.transform.childCount do
                local child = self.buttonTable.transform:GetChild(i)
                if (CommonDefs.GetComponentInChildren_Component_Type(child, typeof(UILabel)).text == LocalString.GetString("转为副手")) then
                    local button = CommonDefs.GetComponentInChildren_GameObject_Type(child.gameObject, typeof(CButton))
                    if button then
                        return button.gameObject
                    end
                end
                i = i + 1
            end
        end
    end
    return nil
end

