local QnAdvanceGridView     = import "L10.UI.QnAdvanceGridView"
local UIDefaultTableView    = import "L10.UI.UIDefaultTableView"
local CellIndexType         = import "L10.UI.UITableView+CellIndexType"
local CellIndex             = import "L10.UI.UITableView+CellIndex"
local CButton               = import "L10.UI.CButton"
local EnumClass             = import "L10.Game.EnumClass"
local Profession            = import "L10.Game.Profession"
local CLoginMgr             = import "L10.Game.CLoginMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel            = import "L10.Game.EChatPanel"
local AlignType             = import "CPlayerInfoMgr+AlignType"
local CPlayerInfoMgr        = import "CPlayerInfoMgr"

LuaArenaRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaArenaRankWnd, "master_TableView", "Master", UIDefaultTableView)
RegistChildComponent(LuaArenaRankWnd, "tab_TableView", "TabBar", QnAdvanceGridView)
--@endregion RegistChildComponent end

RegistClassMember(LuaArenaRankWnd, "typeDict")
RegistClassMember(LuaArenaRankWnd, "sections")
RegistClassMember(LuaArenaRankWnd, "curLevelType")
RegistClassMember(LuaArenaRankWnd, "levelTypes")
RegistClassMember(LuaArenaRankWnd, "rankSpriteTbl")
RegistClassMember(LuaArenaRankWnd, "rankList")
RegistClassMember(LuaArenaRankWnd, "isCross")

function LuaArenaRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self.typeDict = {}
    self.typeDict.myServer = self:GetSubTbl(self.transform:Find("Detail/RankList/MyServer"))
    self.typeDict.crossServer = self:GetSubTbl(self.transform:Find("Detail/RankList/CrossServer"))
    self.typeDict.myServer.root:SetActive(true)
    self.typeDict.crossServer.root:SetActive(false)

    self.rankSpriteTbl = {g_sprites.RankFirstSpriteName, g_sprites.RankSecondSpriteName, g_sprites.RankThirdSpriteName}
    self.levelTypes = {LocalString.GetString("天元组"), LocalString.GetString("天罡组"), LocalString.GetString("神勇组"), LocalString.GetString("英武组"), LocalString.GetString("新锐组")}
end

function LuaArenaRankWnd:GetSubTbl(parent)
    local tbl = {}
    tbl.root = parent.gameObject
    tbl.tableView = parent:GetComponent(typeof(QnAdvanceGridView))
    tbl.myRank = parent:Find("MainPlayerInfo/Rank"):GetComponent(typeof(UILabel))
    tbl.myRank.text = "-"
    local class = parent:Find("MainPlayerInfo/Class"):GetComponent(typeof(UISprite))
    class.spriteName = CClientMainPlayer.Inst and Profession.GetIcon(CClientMainPlayer.Inst.Class) or ""
    local name = parent:Find("MainPlayerInfo/Name"):GetComponent(typeof(UILabel))
    name.text = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name or ""
    local server = parent:Find("MainPlayerInfo/Server")
    if server then
        local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
        server:GetComponent(typeof(UILabel)).text = myGameServer and myGameServer.name or ""
    end
    -- tbl.myScore = parent:Find("MainPlayerInfo/Score"):GetComponent(typeof(UILabel))
    tbl.myLargeDan = parent:Find("MainPlayerInfo/Dan/Large"):GetComponent(typeof(CUITexture))
    tbl.mySmallDan = parent:Find("MainPlayerInfo/Dan/Small"):GetComponent(typeof(CUITexture))
    tbl.myLargeDan:LoadMaterial("")
    tbl.mySmallDan:LoadMaterial("")
    return tbl
end


function LuaArenaRankWnd:OnEnable()
    g_ScriptEvent:AddListener("SendNewArenaRankInfo", self, "OnSendNewArenaRankInfo")
end

function LuaArenaRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendNewArenaRankInfo", self, "OnSendNewArenaRankInfo")
end

function LuaArenaRankWnd:OnSendNewArenaRankInfo(myScore, myRank, seasonId, levelType, isCross, page, totalPage, rankData)
    local selectedIndex = self.master_TableView.SelectedIndex
    if seasonId ~= self.sections[selectedIndex.section + 1] or levelType ~= (6 - self.curLevelType) or isCross ~= (selectedIndex.row == 1) then return end

    self.typeDict.myServer.root:SetActive(not isCross)
    self.typeDict.crossServer.root:SetActive(isCross)
    local tbl = isCross and self.typeDict.crossServer or self.typeDict.myServer

    tbl.myRank.text = myRank > 0 and myRank or LocalString.GetString("未上榜")
    -- tbl.myScore.text = myScore
    local largeDan, smallDan = LuaArenaMgr:GetLargeAndSmallDan(myScore)
    tbl.myLargeDan:LoadMaterial(Arena_DanInfo.GetData(largeDan).Icon)
    tbl.mySmallDan:LoadMaterial(LuaArenaMgr.smallDanMatPaths[smallDan])

    self:UpdateRank(tbl, rankData, isCross)
end

function LuaArenaRankWnd:UpdateRank(tbl, rankData, isCross)
    self.rankList = g_MessagePack.unpack(rankData)
    self.isCross = isCross
    tbl.tableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.rankList
        end,
        function(item, index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

            self:InitRankItem(item, index)
        end
    )

    tbl.tableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        CPlayerInfoMgr.ShowPlayerPopupMenu(self.rankList[row + 1].playerId, EnumPlayerInfoContext.Arena, EChatPanel.Undefined, "", nil, Vector3.zero, AlignType.Default)
    end)

    tbl.tableView:ReloadData(true, false)
end

function LuaArenaRankWnd:InitRankItem(item, index)
    local data = self.rankList[index + 1]
    local child = item.transform

    child:Find("Name"):GetComponent(typeof(UILabel)).text = data.name
    child:Find("Class"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(data.class)

    local largeDan, smallDan = LuaArenaMgr:GetLargeAndSmallDan(data.rankScore)
    child:Find("Dan/Large"):GetComponent(typeof(CUITexture)):LoadMaterial(Arena_DanInfo.GetData(largeDan).Icon)
    child:Find("Dan/Small"):GetComponent(typeof(CUITexture)):LoadMaterial(LuaArenaMgr.smallDanMatPaths[smallDan])

    local rank = index + 1
    local rankNum = child:Find("Rank"):GetComponent(typeof(UILabel))
    local rankSprite = child:Find("Rank/Sprite"):GetComponent(typeof(UISprite))
    rankNum.text = ""
    rankSprite.spriteName = ""
    if rank >= 1 and rank <= 3 then
        rankSprite.spriteName = self.rankSpriteTbl[rank]
    else
        rankNum.text = rank
    end
    if self.isCross then child:Find("Server"):GetComponent(typeof(UILabel)).text = data.serverName end
end

function LuaArenaRankWnd:Init()
    self:InitTabs()
    self:InitMaster()
end

function LuaArenaRankWnd:InitMaster()
    self:InitSections()
    self.master_TableView:Init(function() -- numberOfSectionFunc
        return #self.sections
    end,
    function(section) -- numberOfRowInSectionFunc
        return 2
    end,
    function(cell, index) -- cell
        self:InitCell(cell, index)
    end,
    function(index, expanded) -- sectionClick
    end,
    function(index) -- rowselect
        self:QueryRankData()
    end)

    self.master_TableView:LoadData(CellIndex(0, 0, CellIndexType.Row), true)
end

function LuaArenaRankWnd:InitSections()
    self.sections = {}
    local count = LuaArenaMgr:GetSeasonInfoClass().GetDataCount()
    table.insert(self.sections, LuaArenaMgr:GetCurrentSeasonId())
end

function LuaArenaRankWnd:InitCell(cell, index)
    local seasonId = self.sections[index.section + 1]
    local button = cell.transform:GetComponent(typeof(CButton))
    if index.type == CellIndexType.Section then
        button.Text = LuaArenaMgr:GetSeasonInfoClass().GetData(seasonId).Name
    else
        if index.row == 0 then
            button.Text = LocalString.GetString("本服")
        elseif index.row == 1 then
            button.Text = LocalString.GetString("跨服")
        end
    end
end


function LuaArenaRankWnd:InitTabs()
    self.tab_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return 5
        end,
        function(item, index)
            self:InitTabItem(item, index)
        end
    )

    self.curLevelType = 1
    self.tab_TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        if self.curLevelType ~= row + 1 then
            self.curLevelType = row + 1
            self:QueryRankData()
        end
    end)

    self.tab_TableView:ReloadData(true, true)
    self.tab_TableView:SetSelectRow(0, true)
end

function LuaArenaRankWnd:InitTabItem(item, index)
    item.Text = self.levelTypes[index + 1]
end

function LuaArenaRankWnd:QueryRankData()
    local selectedIndex = self.master_TableView.SelectedIndex
    local seasonId = self.sections[selectedIndex.section + 1]
    local isCross = selectedIndex.row == 1

    Gac2Gas.QueryNewArenaRankInfo(seasonId, 6 - self.curLevelType, isCross, 1)
end

--@region UIEvent
--@endregion UIEvent
