require("common/common_include")

local UIGrid = import "UIGrid"
local CFightingSpiritMemberInfoItem = import "L10.UI.CFightingSpiritMemberInfoItem"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"

LuaFightingSpiritOtherTeamWnd = class()

RegistChildComponent(LuaFightingSpiritOtherTeamWnd, "MemberItemPrefab", GameObject)
RegistChildComponent(LuaFightingSpiritOtherTeamWnd, "MemberRoot", UIGrid)
RegistChildComponent(LuaFightingSpiritOtherTeamWnd, "PowerLabel", UILabel)
RegistChildComponent(LuaFightingSpiritOtherTeamWnd, "SloganLabel", UILabel)
RegistChildComponent(LuaFightingSpiritOtherTeamWnd, "SumPowerLabel", UILabel)

RegistClassMember(LuaFightingSpiritOtherTeamWnd, "MemberItemList")

function LuaFightingSpiritOtherTeamWnd:Init()
	if not CLuaFightingSpiritMgr.m_CurrentTeam then
		CUIManager.CloseUI(CLuaUIResources.FightingSpiritOtherTeamWnd)
		return
	end
	self.MemberItemPrefab:SetActive(false)
	if self.MemberRoot.transform.childCount == 0 then
		self:InitMembers()
	end
	self:RefreshMembers()
	self:RefreshInfo()
end

function LuaFightingSpiritOtherTeamWnd:InitMembers()
	self.MemberItemList = {}
	CUICommonDef.ClearTransform(self.MemberRoot.transform)

	for i = 0, CFightingSpiritMgr.MaxTeamMemberCount-1 do
		local go = NGUITools.AddChild(self.MemberRoot.gameObject, self.MemberItemPrefab)
		local item = go:GetComponent(typeof(CFightingSpiritMemberInfoItem))
		item.ClickCallback = DelegateFactory.Action_ulong_GameObject(function (id, gameObject)
			self:OnItemClicked(id, gameObject)
		end)
		go:SetActive(true)
		table.insert(self.MemberItemList, item)
	end
	self.MemberRoot:Reposition()
end

function LuaFightingSpiritOtherTeamWnd:RefreshMembers()
	local memberList = CLuaFightingSpiritMgr.m_CurrentTeam.MemberList
	for i = 0 , CFightingSpiritMgr.MaxTeamMemberCount-1 do
		if i < memberList.Count then
			self.MemberItemList[i+1]:SetMemberInfo(memberList[i], i == 0, false)
		else
			self.MemberItemList[i+1]:SetMemberInfo(nil, false, false)
		end
	end
end

function LuaFightingSpiritOtherTeamWnd:RefreshInfo()
	self.SloganLabel.text =  CLuaFightingSpiritMgr.m_CurrentTeam.Slogan
	local memberList = CLuaFightingSpiritMgr.m_CurrentTeam.MemberList

	local powerscore = 0
	for i = 0, memberList.Count-1 do
		powerscore = powerscore + memberList[i].Capacity
	end
	self.PowerLabel.text = tostring(math.floor(powerscore / memberList.Count))
	self.SumPowerLabel.text = tostring(powerscore)
end


function LuaFightingSpiritOtherTeamWnd:OnItemClicked(id, go)
	CPlayerInfoMgr.ShowPlayerPopupMenu(id, EnumPlayerInfoContext.DouhunTeam, EChatPanel.Undefined, nil, nil, go.transform.position, AlignType.Default)
end

function LuaFightingSpiritOtherTeamWnd:OnEnable()
	-- g_ScriptEvent:AddListener("AcceptTaskFromBabySuccess", self, "UpdateDiaryAlert")
end

function LuaFightingSpiritOtherTeamWnd:OnDisable()

	-- g_ScriptEvent:RemoveListener("AcceptTaskFromBabySuccess", self, "UpdateDiaryAlert")
end

return LuaFightingSpiritOtherTeamWnd