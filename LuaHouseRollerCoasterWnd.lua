local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local SoundManager=import "SoundManager"
local CSkillButtonBoardWnd=import "L10.UI.CSkillButtonBoardWnd"
local UIPanel=import "UIPanel"
local EUIModuleGroup=import "L10.UI.CUIManager.EUIModuleGroup"
local PlayerSettings=import "L10.Game.PlayerSettings"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local Time = import "UnityEngine.Time"
local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"
local UIRect=import "UIRect"
local CScene = import "L10.Game.CScene"
local CWater = import "L10.Engine.CWater"     
local NormalCamera = import "L10.Engine.CameraControl.NormalCamera"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"

LuaHouseRollerCoasterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHouseRollerCoasterWnd, "StartButton", "StartButton", GameObject)
RegistChildComponent(LuaHouseRollerCoasterWnd, "StopButton", "StopButton", GameObject)
RegistChildComponent(LuaHouseRollerCoasterWnd, "AccButton", "AccButton", GameObject)
RegistChildComponent(LuaHouseRollerCoasterWnd, "Seat_Label", "Seat_Label", UILabel)
RegistChildComponent(LuaHouseRollerCoasterWnd, "Seat_Driver", "Seat_Driver", GameObject)
RegistChildComponent(LuaHouseRollerCoasterWnd, "Seat_Passenger", "Seat_Passenger", GameObject)
RegistChildComponent(LuaHouseRollerCoasterWnd, "FreeViewButton", "FreeViewButton", GameObject)
RegistChildComponent(LuaHouseRollerCoasterWnd, "FollowViewButton", "FollowViewButton", GameObject)
RegistChildComponent(LuaHouseRollerCoasterWnd, "ViewButtons", "ViewButtons", GameObject)
RegistChildComponent(LuaHouseRollerCoasterWnd, "FreeSelect", "FreeSelect", GameObject)
RegistChildComponent(LuaHouseRollerCoasterWnd, "FreeUnselect", "FreeUnselect", GameObject)
RegistChildComponent(LuaHouseRollerCoasterWnd, "FollowUnselect", "FollowUnselect", GameObject)
RegistChildComponent(LuaHouseRollerCoasterWnd, "FollowSelect", "FollowSelect", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaHouseRollerCoasterWnd,"m_CabinId")
RegistClassMember(LuaHouseRollerCoasterWnd,"m_MaxSpeed")
RegistClassMember(LuaHouseRollerCoasterWnd,"m_SoundInstance")
RegistClassMember(LuaHouseRollerCoasterWnd,"m_SoundName")
RegistClassMember(LuaHouseRollerCoasterWnd,"m_CameraMoveSpeed")
RegistClassMember(LuaHouseRollerCoasterWnd,"m_ViewEditButtons")
RegistClassMember(LuaHouseRollerCoasterWnd,"m_ViewEditButtonsIndex")
RegistClassMember(LuaHouseRollerCoasterWnd,"m_ViewEditIsPressed")
RegistClassMember(LuaHouseRollerCoasterWnd,"m_ViewEditButtonIndex")
RegistClassMember(LuaHouseRollerCoasterWnd,"m_CameraTrans")
RegistClassMember(LuaHouseRollerCoasterWnd,"m_CameraMoveSpeed")
RegistClassMember(LuaHouseRollerCoasterWnd,"m_CameraMinY")
RegistClassMember(LuaHouseRollerCoasterWnd,"m_CameraMaxY")

function LuaHouseRollerCoasterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.StartButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnStartButtonClick()
	end)

	UIEventListener.Get(self.FreeViewButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFreeViewButtonClick()
	end)

	UIEventListener.Get(self.FollowViewButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFollowViewButtonClick()
	end)

    --@endregion EventBind end

    
	UIEventListener.Get(self.StopButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        local playInfo = LuaHouseRollerCoasterMgr.GetPlayInfo(self.m_CabinId)
        if playInfo then
            if not CClientMainPlayer.Inst then return end
            local pos = CClientMainPlayer.Inst.RO.Position
            Gac2Gas.RequestEndRollerCoaster(math.floor(pos.x),math.floor(pos.z))
        else
            -- g_MessageMgr:ShowMessage("ROLLER_COASTER_NOT_RUN")
            --直接下车
            Gac2Gas.LeaveRollerCoasterFurniture()
        end
	end)
    UIEventListener.Get(self.AccButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        local playInfo = LuaHouseRollerCoasterMgr.GetPlayInfo(self.m_CabinId)
        if playInfo then
            if playInfo.pause then
                g_MessageMgr:ShowMessage("ROLLER_COASTER_PAUSE_CANNOT_ACC")
            else
                Gac2Gas.RequestSpeedUpRollerCoaster()
            end
        else
            g_MessageMgr:ShowMessage("ROLLER_COASTER_NOT_RUN")
        end
	end)

    --当前坐的哪辆车
    self.m_CabinId = 0
    local playerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    CommonDefs.DictIterate(CClientFurnitureMgr.Inst.FurnitureList, DelegateFactory.Action_object_object(function (k,v) 
        if v:GetSubType() == EnumFurnitureSubType_lua.eRollerCoasterCabin then
            if v.Children then
                for i=1,v.Children.Length do
                    local childId = v.Children[i-1]
                    if playerId==childId then
                        self.m_CabinId = v.ID
                    end
                end
            end
        end
    end))

    self:RefreshView()
    local setting = Zhuangshiwu_Setting.GetData()
    self.m_MinSpeed = setting.RollerCoastervmin
    self.m_MaxSpeed = setting.RollerCoastervmax


    self.m_CameraMinY = Zhuangshiwu_Setting.GetData().HouseCameraMinY
    self.m_CameraMaxY = Zhuangshiwu_Setting.GetData().HouseCameraMaxY
    self.m_CameraMoveSpeed = 1
    self:SetViewButtonSelect(1)
    
    self.m_ViewEditButtons = {
        [1] = self.ViewButtons.transform:Find("Left").gameObject,
        [2] = self.ViewButtons.transform:Find("Right").gameObject,
        [3] = self.ViewButtons.transform:Find("Up").gameObject,
        [4] = self.ViewButtons.transform:Find("Down").gameObject,
        [5] = self.ViewButtons.transform:Find("Reset").gameObject,
    }
    self.m_ViewEditButtonsIndex = {}
    for i,v in ipairs(self.m_ViewEditButtons) do
        self.m_ViewEditButtonsIndex[v.name] = i
    end
    for i=1,4 do
        UIEventListener.Get(self.m_ViewEditButtons[i]).onPress = DelegateFactory.BoolDelegate(function(go,state)
            local idx = self.m_ViewEditButtonsIndex[go.name]
            self.m_ViewEditButtonIndex = idx
            self.m_ViewEditIsPressed = state
        end)
    end
    UIEventListener.Get(self.m_ViewEditButtons[5]).onClick = DelegateFactory.VoidDelegate(function(go)
        self:ResetCamera()
    end)
end

function LuaHouseRollerCoasterWnd:Init()

end

--@region UIEvent

function LuaHouseRollerCoasterWnd:OnStartButtonClick()
    local playInfo = LuaHouseRollerCoasterMgr.GetPlayInfo(self.m_CabinId)
    if playInfo then
        if playInfo.pause then
            Gac2Gas.RequestResumeRollerCoaster()--暂停之后启动
        else
            Gac2Gas.RequestStopRollerCoaster(false)--手动暂停
        end
        return
    end

    local huacheFur = CClientFurnitureMgr.Inst:GetFurniture(self.m_CabinId)
    if not huacheFur then return end
    local zhantaiFur = CClientFurnitureMgr.Inst:GetFurniture(huacheFur.ParentId)
    if not zhantaiFur then return end

    Gac2Gas.RequestRunRollerCoaster(huacheFur.ID,zhantaiFur.ID)
end


function LuaHouseRollerCoasterWnd:OnFollowViewButtonClick()
    self:SetViewButtonSelect(2)
    self:ResetCamera()
    LuaHouseRollerCoasterMgr:SetCamera()
end


function LuaHouseRollerCoasterWnd:OnFreeViewButtonClick()
    self:SetViewButtonSelect(1)
    LuaHouseRollerCoasterMgr.ResetCamera()
end


--@endregion UIEvent

function LuaHouseRollerCoasterWnd:SetViewButtonSelect(view)
    --view,1free,2follow
    self.ViewButtons:SetActive(view == 1)
    self.FollowSelect:SetActive(view == 2)
    self.FollowUnselect:SetActive(view ~= 2)
    self.FreeSelect:SetActive(view == 1)
    self.FreeUnselect:SetActive(view ~= 1)
end

function LuaHouseRollerCoasterWnd:ResetCamera()
    if self.m_CameraTrans == nil then
        self.m_CameraTrans = CameraFollow.Inst.transform:Find("camRotate/newParent")
    end
    self.m_CameraTrans.localPosition = Vector3(0,0,0)
end


function LuaHouseRollerCoasterWnd:OnEnable()
    CUIManager.instance:HideUI(InitializeList(CreateFromClass(MakeGenericClass(List, String)), String, "Joystick", "SkillButtonBoard"))
    g_ScriptEvent:AddListener("HouseRollerCoasterBeginPlay", self, "OnHouseRollerCoasterBeginPlay")
    g_ScriptEvent:AddListener("HouseRollerCoasterStop", self, "OnHouseRollerCoasterStop")
    g_ScriptEvent:AddListener("PlayerAttachFurniture", self, "OnPlayerAttachFurniture")
    g_ScriptEvent:AddListener("PlayerDetachFurniture", self, "OnPlayerDetachFurniture")
    g_ScriptEvent:AddListener("HouseRollerCoasterResume", self, "OnHouseRollerCoasterResume")
    
end

function LuaHouseRollerCoasterWnd:OnDisable()
    CUIManager.instance:ShowUI(InitializeList(CreateFromClass(MakeGenericClass(List, String)), String, "Joystick", "SkillButtonBoard"))

    --这里必须要刷新一下
    CUIManager.ShowUIByChangeLayer(CUIResources.SkillButtonBoard,false,true)
    -- if CSkillButtonBoardWnd.Instance then
    --     local rects = CSkillButtonBoardWnd.Instance.gameObject:GetComponentsInChildren(typeof(UIRect))
    --     for i=0,rects.Length-1 do
    --         rects[i]:ResetAndUpdateAnchors()
    --     end
    -- end

    g_ScriptEvent:RemoveListener("HouseRollerCoasterBeginPlay", self, "OnHouseRollerCoasterBeginPlay")
    g_ScriptEvent:RemoveListener("HouseRollerCoasterStop", self, "OnHouseRollerCoasterStop")
    g_ScriptEvent:RemoveListener("PlayerAttachFurniture", self, "OnPlayerAttachFurniture")
    g_ScriptEvent:RemoveListener("PlayerDetachFurniture", self, "OnPlayerDetachFurniture")
    g_ScriptEvent:RemoveListener("HouseRollerCoasterResume", self, "OnHouseRollerCoasterResume")
end

function LuaHouseRollerCoasterWnd:OnPlayerAttachFurniture(playerId,furnitureId, slotIdx)
    self:RefreshView()

end
function LuaHouseRollerCoasterWnd:OnPlayerDetachFurniture(playerId,furnitureId)
    self:RefreshView()
end
function LuaHouseRollerCoasterWnd:RefreshView()
    self.Seat_Label.text = nil
    self.Seat_Driver:SetActive(false)
    self.Seat_Passenger:SetActive(false)

    local playerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0

    local slotIndex = 0
    local haveDriver = false
    local havePassenger = false
    CommonDefs.DictIterate(CClientFurnitureMgr.Inst.FurnitureList, DelegateFactory.Action_object_object(function (k,v) 
        local subType = v:GetSubType()
        if subType == EnumFurnitureSubType_lua.eRollerCoasterCabin then
            if v.Children then
                for i=1,v.Children.Length do
                    local childId = v.Children[i-1]
                    if playerId==childId then
                        slotIndex = i
                    end
                    if childId>0 and i==1 then
                        haveDriver = true
                    end
                    if childId>0 and i>1 then
                        havePassenger = true
                    end
                end
            end
        end
    end))
    if slotIndex==1 then
        self.Seat_Label.text = LocalString.GetString("司机")
        --我是司机
        self.Seat_Driver:SetActive(true)
        self.Seat_Driver.transform:Find("Seat_n").gameObject:SetActive(false)
        self.Seat_Driver.transform:Find("Seat_s").gameObject:SetActive(true)
        if havePassenger then
            self.Seat_Passenger:SetActive(true)
            self.Seat_Passenger.transform:Find("Seat_n").gameObject:SetActive(true)
            self.Seat_Passenger.transform:Find("Seat_s").gameObject:SetActive(false)
        end
    elseif slotIndex>1 then
        self.Seat_Label.text = LocalString.GetString("乘客")
        --我是乘客
        self.Seat_Passenger:SetActive(true)
        self.Seat_Passenger.transform:Find("Seat_n").gameObject:SetActive(false)
        self.Seat_Passenger.transform:Find("Seat_s").gameObject:SetActive(true)
        if haveDriver then
            self.Seat_Driver:SetActive(true)
            self.Seat_Driver.transform:Find("Seat_n").gameObject:SetActive(true)
            self.Seat_Driver.transform:Find("Seat_s").gameObject:SetActive(false)
        end
    end
end

function LuaHouseRollerCoasterWnd:OnHouseRollerCoasterBeginPlay(huacheId)
    if self.m_CabinId==huacheId then
        self.StartButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("停车")
    end
end

function LuaHouseRollerCoasterWnd:OnHouseRollerCoasterStop(huacheId,auto)
    if self.m_CabinId==huacheId then
        self.StartButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("启动")
    end

end

function LuaHouseRollerCoasterWnd:OnHouseRollerCoasterResume(playerId, fid, fTid)
    if self.m_CabinId==fid then
        self.StartButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("停车")
    end
end

function LuaHouseRollerCoasterWnd:OnDestroy()
    LuaHouseRollerCoasterMgr.ResetCamera()
    self:ResetCamera()

    if self.m_SoundInstance then
        SoundManager.Inst:StopSound(self.m_SoundInstance)
        self.m_SoundInstance = nil
    end
    self.m_SoundName = nil
end

function LuaHouseRollerCoasterWnd:SetCameraTrans()
    if self.m_ViewEditIsPressed then
        if self.m_CameraTrans == nil then
            self.m_CameraTrans = CameraFollow.Inst.transform:Find("camRotate/newParent")
        end
        local pos = self.m_CameraTrans.localPosition

        if self.m_ViewEditButtonIndex == 3 then
            if self.m_CameraTrans.position.y > self.m_CameraMaxY then
                return
            end
        end
        if self.m_ViewEditButtonIndex == 4 and CScene.MainScene then
            local logicHeight = CScene.MainScene:GetLogicHeight(math.floor(self.m_CameraTrans.position.x * 64), math.floor(self.m_CameraTrans.position.z * 64))
            if self.m_CameraTrans.position.y < math.max(CWater.s_WaterHeight, logicHeight) + self.m_CameraMinY then
                return
            end
        end
        
        local deltaTime = Time.deltaTime
        local val = deltaTime*self.m_CameraMoveSpeed
        if self.m_ViewEditButtonIndex==1 then--right
            pos = Vector3(pos.x-val,pos.y,pos.z)
        elseif self.m_ViewEditButtonIndex==2 then
            pos = Vector3(pos.x+val,pos.y,pos.z)
        elseif self.m_ViewEditButtonIndex==3 then
            pos = Vector3(pos.x,pos.y+val,pos.z)
        elseif self.m_ViewEditButtonIndex==4 then
            pos = Vector3(pos.x,pos.y-val,pos.z)
        end
        
        self.m_CameraTrans.localPosition = pos
    else
        --//不允许摄像机低于地表
        if self.m_CameraTrans == nil then
            self.m_CameraTrans = CameraFollow.Inst.transform:Find("camRotate/newParent")
        end

        local pos = Vector3(self.m_CameraTrans.position.x, self.m_CameraTrans.position.y, self.m_CameraTrans.position.z)
        if CScene.MainScene then
            local logicHeight = CScene.MainScene:GetLogicHeight(math.floor(self.m_CameraTrans.position.x * 64), math.floor(self.m_CameraTrans.position.z * 64))
            if self.m_CameraTrans.position.y < math.max(CWater.s_WaterHeight, logicHeight) + self.m_CameraMinY then
                pos.y = math.max(CWater.s_WaterHeight, logicHeight) + self.m_CameraMinY
                self.m_CameraTrans.position = pos
            end
        end
    end
    
end


function LuaHouseRollerCoasterWnd:Update()
    self:SetCameraTrans()
    if self.m_CabinId<=0 then return end
    if not PlayerSettings.SoundEnabled then
        if self.m_SoundInstance then
            SoundManager.Inst:StopSound(self.m_SoundInstance)
            self.m_SoundInstance = nil
        end
        self.m_SoundName = nil
    end

    local playInfo = LuaHouseRollerCoasterMgr.GetPlayInfo(self.m_CabinId)
    if playInfo then
        local soundName = nil
        if playInfo.pause then
            --暂停声音
        else
            if playInfo.speed==0 then
            elseif playInfo.speed>0 and playInfo.speed<=0.3*self.m_MaxSpeed then
                soundName = "event:/L10/SFX/Roller_Coaster/Roller_Coaster_Slow"
            elseif playInfo.speed>0.3*self.m_MaxSpeed and playInfo.speed<=0.7*self.m_MaxSpeed then
                soundName = "event:/L10/SFX/Roller_Coaster/Roller_Coaster_Mid"
            elseif playInfo.speed>0.7*self.m_MaxSpeed then
                soundName = "event:/L10/SFX/Roller_Coaster/Roller_Coaster_Fast"
            end
        end
        if not soundName then
            --沒有声音
            if self.m_SoundInstance then
                SoundManager.Inst:StopSound(self.m_SoundInstance)
                self.m_SoundInstance = nil
            end
            self.m_SoundName = nil
        elseif soundName~=self.m_SoundName then
            if self.m_SoundInstance then
                SoundManager.Inst:StopSound(self.m_SoundInstance)
                self.m_SoundInstance = nil
            end
            self.m_SoundInstance=SoundManager.Inst:PlaySound(soundName,Vector3.zero, 1, nil, 0)
            self.m_SoundName = soundName
        end
    else
        if self.m_SoundInstance then
            SoundManager.Inst:StopSound(self.m_SoundInstance)
            self.m_SoundInstance = nil
        end
        self.m_SoundName = nil
    end
    
end
