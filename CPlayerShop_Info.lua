-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local CLogMgr = import "L10.CLogMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerShop_Info = import "L10.UI.CPlayerShop_Info"
local CPlayerShopData = import "L10.UI.CPlayerShopData"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local DelegateFactory = import "DelegateFactory"
local Double = import "System.Double"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local L10 = import "L10"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local QnButton = import "L10.UI.QnButton"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CPlayerShop_Info.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.ChangePlayerShopNameSuccess, MakeDelegateFromCSFunction(this.OnChangePlayerShopNameSuccess, MakeGenericClass(Action2, Double, String), this))
    EventManager.AddListenerInternal(EnumEventType.AddPlayerShopFundSuccess, MakeDelegateFromCSFunction(this.OnAddPlayerShopFundSuccess, MakeGenericClass(Action4, Double, UInt32, Double, Double), this))
    EventManager.AddListenerInternal(EnumEventType.GetPlayerShopFundSuccess, MakeDelegateFromCSFunction(this.OnGetPlayerShopFundSuccess, MakeGenericClass(Action4, Double, Double, Double, Double), this))

    this.m_AddInMoneyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_AddInMoneyButton.OnClick, MakeDelegateFromCSFunction(this.OnAddInMoney, MakeGenericClass(Action1, QnButton), this), true)
    this.m_TakeOutMoneyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_TakeOutMoneyButton.OnClick, MakeDelegateFromCSFunction(this.OnTakeoutMoney, MakeGenericClass(Action1, QnButton), this), true)
    this.m_FreezeMoneyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_FreezeMoneyButton.OnClick, MakeDelegateFromCSFunction(this.OnFreezeMoney, MakeGenericClass(Action1, QnButton), this), true)
    this.m_ChangeShopNameButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_ChangeShopNameButton.OnClick, MakeDelegateFromCSFunction(this.OnChangeShopName, MakeGenericClass(Action1, QnButton), this), true)
    UIEventListener.Get(this.m_CloseButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_CloseButton).onClick, MakeDelegateFromCSFunction(this.OnClose, VoidDelegate, this), true)
end
CPlayerShop_Info.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.ChangePlayerShopNameSuccess, MakeDelegateFromCSFunction(this.OnChangePlayerShopNameSuccess, MakeGenericClass(Action2, Double, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.AddPlayerShopFundSuccess, MakeDelegateFromCSFunction(this.OnAddPlayerShopFundSuccess, MakeGenericClass(Action4, Double, UInt32, Double, Double), this))
    EventManager.RemoveListenerInternal(EnumEventType.GetPlayerShopFundSuccess, MakeDelegateFromCSFunction(this.OnGetPlayerShopFundSuccess, MakeGenericClass(Action4, Double, Double, Double, Double), this))

    this.m_AddInMoneyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_AddInMoneyButton.OnClick, MakeDelegateFromCSFunction(this.OnAddInMoney, MakeGenericClass(Action1, QnButton), this), false)
    this.m_TakeOutMoneyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_TakeOutMoneyButton.OnClick, MakeDelegateFromCSFunction(this.OnTakeoutMoney, MakeGenericClass(Action1, QnButton), this), false)
    this.m_FreezeMoneyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_FreezeMoneyButton.OnClick, MakeDelegateFromCSFunction(this.OnFreezeMoney, MakeGenericClass(Action1, QnButton), this), false)
    this.m_ChangeShopNameButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_ChangeShopNameButton.OnClick, MakeDelegateFromCSFunction(this.OnChangeShopName, MakeGenericClass(Action1, QnButton), this), false)
    UIEventListener.Get(this.m_CloseButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_CloseButton).onClick, MakeDelegateFromCSFunction(this.OnClose, VoidDelegate, this), false)
end
CPlayerShop_Info.m_OnChangeShopName_CS2LuaHook = function (this, button)
    local format = g_MessageMgr:FormatMessage("PLAYERSHOP_INPUT_NAME_INTERFACE", CPlayerShopMgr.Shop_Name_Num_Max)
    CInputBoxMgr.ShowInputBox(format, DelegateFactory.Action_string(function (text) 
        if not System.String.IsNullOrEmpty(text) then
            if CClientMainPlayer.Inst.Silver < CPlayerShopMgr.Change_Shop_Name_Cost then
                local msg1 = System.String.Format(LocalString.GetString("你的银两不足{0}两，不能改名!"), CPlayerShopMgr.Change_Shop_Name_Cost)
                MessageWndManager.ShowOKMessage(msg1, nil)
            elseif not CWordFilterMgr.Inst:CheckName(text, LocalString.GetString("玩家商店命名")) then
                g_MessageMgr:ShowMessage("Name_Violation")
            else
                local msg2 = System.String.Format(LocalString.GetString("你确定要花费{0}银两改商店名吗？"), CPlayerShopMgr.Change_Shop_Name_Cost)
                MessageWndManager.ShowOKCancelMessage(msg2, DelegateFactory.Action(function () 
                    local shopId = CPlayerShopData.Main.PlayerShopId
                    Gac2Gas.ChangePlayerShopName(shopId, text)
                end), nil, nil, nil, false)
            end
        else
            g_MessageMgr:ShowMessage("PLAYERSHOP_NOT_NO_NAME")
        end
    end), CPlayerShopMgr.Shop_Name_Num_Max, true, nil, nil)
end
CPlayerShop_Info.m_OnChangePlayerShopNameSuccess_CS2LuaHook = function (this, shopId, name) 
    if shopId ~= CPlayerShopData.Main.PlayerShopId then
        L10.CLogMgr.Log(LocalString.GetString("ShopId 怎么不相等，请检查逻辑"))
    else
        g_MessageMgr:ShowMessage("PLAYERSHOP_CHANGE_NAME_SUCCEED")
        this.m_ShopNameLabel.text = name
    end
end
CPlayerShop_Info.m_OnAddPlayerShopFundSuccess_CS2LuaHook = function (this, shopId, fund, activeFund, totalFund) 
    if shopId == CPlayerShopData.Main.PlayerShopId then
        g_MessageMgr:ShowMessage("PLAYERSHOP_INTO_MONEY", fund)
        CPlayerShopData.Main.ActiveFund = math.floor(tonumber(activeFund or 0))
        CPlayerShopData.Main.TotalFund = math.floor(tonumber(totalFund or 0))
        this:UpdateBasicShopData()
    end
end
CPlayerShop_Info.m_OnGetPlayerShopFundSuccess_CS2LuaHook = function (this, shopId, fund, maintainFund, totalFund) 
    if shopId == CPlayerShopData.Main.PlayerShopId then
        g_MessageMgr:ShowMessage("PLAYERSHOP_DRAW_MONEY", fund)
        CPlayerShopData.Main.ActiveFund = math.floor(tonumber(maintainFund or 0))
        CPlayerShopData.Main.TotalFund = math.floor(tonumber(totalFund or 0))
        this:UpdateBasicShopData()
    end
end
CPlayerShop_Info.m_UpdateBasicShopData_CS2LuaHook = function (this)
    this.m_CreateTimeLabel.text = CPlayerShopData.Main.CreateTime:ToShortDateString()
    this.m_SellCountLabel.text = System.String.Format("{0}/{1}", CPlayerShopData.Main.CurrentShelfCount, CPlayerShopData.Main.MaxShelfCount)
    this.m_SellerLabel.text = CPlayerShopData.Main.OwnerName
    this.m_SellerIdLabel.text = tostring(CPlayerShopData.Main.OwnerId)
    this.m_ShopNameLabel.text = CPlayerShopData.Main.PlayerShopName
    this.m_RankerLabel.text = tostring(CPlayerShopData.Main.Rank)
    this.m_OperatingFundLabel.text = tostring(CPlayerShopData.Main.TotalFund)
    this.m_CostEveryDayLabel.text = tostring(CPlayerShopData.Main.ActiveFund)
end
CPlayerShop_Info.m_GetGuideGo_CS2LuaHook = function (this, methodName) 
    if methodName == "GetTakeOutButton" then
        return this.m_TakeOutMoneyButton.gameObject
    else
        CLogMgr.LogError(LocalString.GetString("引导数据表填写错误 ") .. methodName)
        return nil
    end
end
