local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaSanxingRebornWnd = class()
RegistChildComponent(LuaSanxingRebornWnd,"timeLabel", UILabel)
RegistChildComponent(LuaSanxingRebornWnd,"btn1", GameObject)
RegistChildComponent(LuaSanxingRebornWnd,"btn2", GameObject)
RegistChildComponent(LuaSanxingRebornWnd,"btn3", GameObject)

--RegistClassMember(LuaSanxingRebornWnd, "maxChooseNum")
RegistClassMember(LuaSanxingRebornWnd, "m_Tick")

function LuaSanxingRebornWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.SanxingRebornWnd)
end

function LuaSanxingRebornWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaSanxingRebornWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaSanxingRebornWnd:Init()
	local onBtn1Click = function(go)
    Gac2Gas.RequestSetSanXingDefaultRelivePos(3)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.btn1,DelegateFactory.Action_GameObject(onBtn1Click),false)
	local onBtn2Click = function(go)
    Gac2Gas.RequestSetSanXingDefaultRelivePos(1)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.btn2,DelegateFactory.Action_GameObject(onBtn2Click),false)
	local onBtn3Click = function(go)
    Gac2Gas.RequestSetSanXingDefaultRelivePos(2)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.btn3,DelegateFactory.Action_GameObject(onBtn3Click),false)

	self.m_Tick = RegisterTickWithDuration(function ()
		self:CalTime()
	end, 1000, 1000 * 1000)

	self:CalTime()
end

function LuaSanxingRebornWnd:CalTime()
	local restTime = math.ceil(LuaSanxingGamePlayMgr.DieEndTime - CServerTimeMgr.Inst.timeStamp)
  if restTime <= 0 then
    self:Close()
  else
    self.timeLabel.text = restTime..LocalString.GetString('秒')
  end
end

function LuaSanxingRebornWnd:OnDestroy()
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
end

return LuaSanxingRebornWnd
