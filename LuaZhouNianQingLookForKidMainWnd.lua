local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaZhouNianQingLookForKidMainWnd = class()

RegistClassMember(LuaZhouNianQingLookForKidMainWnd, "m_TimeLabel")
RegistClassMember(LuaZhouNianQingLookForKidMainWnd, "m_TypeLabel")
RegistClassMember(LuaZhouNianQingLookForKidMainWnd, "m_LevelLabel")
RegistClassMember(LuaZhouNianQingLookForKidMainWnd, "m_DescLabel")
RegistClassMember(LuaZhouNianQingLookForKidMainWnd, "m_Compose")
RegistClassMember(LuaZhouNianQingLookForKidMainWnd, "m_RemainTimes")

function LuaZhouNianQingLookForKidMainWnd:Awake()
    self.m_TimeLabel = self.transform:Find("Anchor/InfoView/Content/Time/Info"):GetComponent(typeof(UILabel))
    self.m_TypeLabel = self.transform:Find("Anchor/InfoView/Content/Type/Info"):GetComponent(typeof(UILabel))
    self.m_LevelLabel = self.transform:Find("Anchor/InfoView/Content/Need/Info"):GetComponent(typeof(UILabel))
    self.m_DescLabel = self.transform:Find("Anchor/InfoView/Content/Desc/Info"):GetComponent(typeof(UILabel))
    self.m_DescLabel.color = Color.white
    self.m_Compose = self.transform:Find("WndBg/zhounianqinglookforkidmainwnd_bg_02/LeftView/Compose")
    self.m_Compose:Find("HintLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("凑齐三种碎片即可合成手镯拓本")
    self.m_RemainTimes = self.transform:Find("Anchor/InfoView/RemainTimes")

    UIEventListener.Get(self.transform:Find("Anchor/InfoView/RuleBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        g_MessageMgr:ShowMessage("ZNQ2023_LookForKid_Tips")
    end)

    UIEventListener.Get(self.m_Compose:Find("ComposeBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CUIManager.ShowUI(CLuaUIResources.ZhouNianQingLookForKidComposeWnd)
    end)
end

function LuaZhouNianQingLookForKidMainWnd:InitOneItem(curItem, itemID, now, req)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)

    if ItemData then texture:LoadMaterial(ItemData.Icon) end

    if now and req then
        local nowLabel = curItem.transform:Find("Now"):GetComponent(typeof(UILabel))
        nowLabel.text = now
        if now < req then nowLabel.color = Color.red end
        curItem.transform:Find("Req"):GetComponent(typeof(UILabel)).text = req
    end

    CommonDefs.AddOnClickListener(
        curItem,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end

function LuaZhouNianQingLookForKidMainWnd:Init()
    local data = ZhouNianQing2023_Setting.GetData()
    local time = g_MessageMgr:FormatMessage("ZNQ2023_LookForKid_Wnd_Msg1")
    local type = g_MessageMgr:FormatMessage("ZNQ2023_LookForKid_Wnd_Msg2")
    local level = g_MessageMgr:FormatMessage("ZNQ2023_LookForKid_Wnd_Msg3")
    local desc = g_MessageMgr:FormatMessage("ZNQ2023_LookForKid_Wnd_Msg4")
    local kidReward = {{}, {}, {}}
    local award = data.WinAward
    for i = 1, 3 do
        kidReward[i][1] = award[i - 1][1]
        kidReward[i][2] = award[i - 1][2]
    end

    local totalTimes = data.WinTimes
    local rewardTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eAnniv2023ZaiZai_DailyRewardTimes) or 0

    local chips = data.Tabensuipian
    local chipId = {chips[0][0], chips[1][0], chips[2][0]}
    local chipNow = {CItemMgr.Inst:GetItemCount(chipId[1]), CItemMgr.Inst:GetItemCount(chipId[2]), CItemMgr.Inst:GetItemCount(chipId[3])}
    local chipReq = {chips[0][1], chips[1][1], chips[2][1]}

    self.m_TimeLabel.text = time
    self.m_TypeLabel.text = type
    self.m_LevelLabel.text = level
    self.m_DescLabel.text = desc
    local nowLabel = self.m_RemainTimes:Find("Now"):GetComponent(typeof(UILabel))
    nowLabel.text = totalTimes - rewardTimes
    if rewardTimes == totalTimes then
        nowLabel.color = Color.red
    end
    self.m_RemainTimes:Find("Max"):GetComponent(typeof(UILabel)).text = totalTimes
    for i = 1, 3 do
        self:InitOneItem(self.m_Compose:Find("Items/Item"..i).gameObject, chipId[i], chipNow[i], chipReq[i])
    end
    self:InitOneItem(self.m_Compose:Find("Items/Rubbing").gameObject, data.TabenItemId)

    for i = 1, 3 do
        for j = 1, 2 do
            if kidReward[i][j] then
                self:InitOneItem(self.transform:Find("Anchor/InfoView/Reward/Kid"..i.."/Item"..j).gameObject, kidReward[i][j])
            end
        end
    end
end
