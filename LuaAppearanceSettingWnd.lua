local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CBaseWnd = import "L10.UI.CBaseWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UITable = import "UITable"
local UIWidget = import "UIWidget"
local UILabel = import "UILabel"
local NGUIMath = import "NGUIMath"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CButton = import "L10.UI.CButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIManager = import "L10.UI.CUIManager"
local QnCheckBoxGroup = import "L10.UI.QnCheckBoxGroup"
local EnumPropertyAppearanceSettingBit = import "L10.Game.CPropertyAppearance+EnumPropertyAppearanceSettingBit"
local EnumClass = import "L10.Game.EnumClass"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CFashionMgr = import "L10.Game.CFashionMgr"
local PlayerSettings = import "L10.Game.PlayerSettings"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"

LuaAppearanceSettingWnd = class()

RegistChildComponent(LuaAppearanceSettingWnd, "m_Background", "Background", UIWidget)
RegistChildComponent(LuaAppearanceSettingWnd, "m_ScrollView", "ScrollView", UIScrollView)
RegistChildComponent(LuaAppearanceSettingWnd, "m_ScrollViewIndicator", "ScrollViewIndicator", UIScrollViewIndicator)
RegistChildComponent(LuaAppearanceSettingWnd, "m_ContentTable", "ContentTable", UITable)
RegistChildComponent(LuaAppearanceSettingWnd, "m_FanShenRoot", "FanShenRoot", GameObject)
RegistChildComponent(LuaAppearanceSettingWnd, "m_XianShenRoot", "XianShenRoot", GameObject)
RegistChildComponent(LuaAppearanceSettingWnd, "m_OtherDisplayOptionTable", "OtherDisplayOptionTable", UITable)
RegistChildComponent(LuaAppearanceSettingWnd, "m_OtherDisplayOptionGuideContainer", "OtherDisplayOptionGuideContainer", UIWidget)
RegistChildComponent(LuaAppearanceSettingWnd, "m_SwitchTemplate", "SwitchTemplate", GameObject)
RegistChildComponent(LuaAppearanceSettingWnd, "m_CheckboxTemplate", "CheckboxTemplate", GameObject)

RegistClassMember(LuaAppearanceSettingWnd, "m_FanShenTable")
RegistClassMember(LuaAppearanceSettingWnd, "m_XianShenTable")
RegistClassMember(LuaAppearanceSettingWnd, "m_BeiShiEnabled")

function LuaAppearanceSettingWnd:Awake()
    self.m_FanShenTable = self.m_FanShenRoot.transform:GetComponent(typeof(UITable))
    self.m_XianShenTable = self.m_XianShenRoot.transform:GetComponent(typeof(UITable))
    self.m_SwitchTemplate:SetActive(false)
    self.m_CheckboxTemplate:SetActive(false)
    self.m_BeiShiEnabled = false
end

function LuaAppearanceSettingWnd:Init()
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then
        self:Close()
        return
    end

    local isXianShen = mainplayer.AppearanceProp.FeiShengAppearanceXianFanStatus > 0
    if isXianShen then
        self:InitXianShenAppearanceSetting()
    else
        self:InitFanShenAppearanceSetting()
    end
    self:InitOtherDisplaySetting()
    self:Layout()

	RegisterTickOnce(function()
			CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.AppearanceSetting)
		end,100)
end

function LuaAppearanceSettingWnd:Layout()
	self.m_ContentTable:Reposition()
	local tableheight = NGUIMath.CalculateRelativeWidgetBounds(self.m_ContentTable.transform).size.y
	self.m_Background.height = math.min(tableheight + 30, CUICommonDef.GetVirtualScreenSize().y)
	local targetCenter = CUICommonDef.WorldSpace2NGUISpace(LuaAppearancePreviewMgr.m_SettingTargetWorldCenter)
	--靠近target右侧底部对齐
	local centerPos = Vector3(targetCenter.x + LuaAppearancePreviewMgr.m_SettingTargetWidth * 0.5 + self.m_Background.width * 0.5, targetCenter.y + self.m_Background.height * 0.5, 0)
	centerPos = CUICommonDef.ConstrainToScreenArea(centerPos.x, centerPos.y, self.m_Background.width, self.m_Background.height)
    centerPos.y = centerPos.y + self.m_Background.height * 0.5
    self.m_Background.transform.localPosition = centerPos
    self.m_ScrollView:GetComponent(typeof(UIPanel)):ResetAndUpdateAnchors()
    self.m_ScrollViewIndicator:Layout()
    self.m_ScrollView:ResetPosition()
end

function LuaAppearanceSettingWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSetFashionHideSuccess_Permanent",self,"OnSetFashionHideSuccess_Permanent")
    g_ScriptEvent:AddListener("AppearancePropertySettingInfoReturn",self,"OnAppearancePropertySettingInfoReturn")
end

function LuaAppearanceSettingWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSetFashionHideSuccess_Permanent",self,"OnSetFashionHideSuccess_Permanent")
    g_ScriptEvent:RemoveListener("AppearancePropertySettingInfoReturn",self,"OnAppearancePropertySettingInfoReturn")
end

function LuaAppearanceSettingWnd:OnSetFashionHideSuccess_Permanent(pos,value)
    self:InitOtherDisplaySetting()
end

function LuaAppearanceSettingWnd:OnAppearancePropertySettingInfoReturn(args)
    self:InitOtherDisplaySetting()
end

--凡身外观设置
function LuaAppearanceSettingWnd:InitFanShenAppearanceSetting()
    self.m_FanShenRoot:SetActive(true)
    self.m_XianShenRoot:SetActive(false)
    self:InitDunPaiAppearanceSetting(self.m_FanShenRoot)
    self:InitMaskAppearanceSetting(self.m_FanShenRoot, false)
    self:InitHelmetAppearanceSetting(self.m_FanShenRoot)
    self:InitBeiShiAppearanceSetting(self.m_FanShenRoot)
    self.m_FanShenTable:Reposition()
end

--仙身外观设置
function LuaAppearanceSettingWnd:InitXianShenAppearanceSetting()
    self.m_FanShenRoot:SetActive(false)
    self.m_XianShenRoot:SetActive(true)
    self:InitDunPaiAppearanceSetting(self.m_XianShenRoot)
    self:InitMaskAppearanceSetting(self.m_XianShenRoot, true)
    self:InitXianShenHelmetAppearanceSetting(self.m_XianShenRoot)
    self:InitXianShenClothesAppearanceSetting(self.m_XianShenRoot)
    self:InitXianShenBeiShiAppearanceSetting(self.m_XianShenRoot)
    self.m_XianShenTable:Reposition()
end

--通用的盾牌设置
function LuaAppearanceSettingWnd:InitDunPaiAppearanceSetting(rootObj)
    local bShowShieldOption = LuaLiangHaoMgr.CanHoldDunpaiClass()
	local obj = rootObj.transform:Find("Shield").gameObject
	obj:SetActive(bShowShieldOption)
	if bShowShieldOption then
		local checkBox = obj:GetComponent(typeof(QnCheckBox))
		checkBox.OnValueChanged = DelegateFactory.Action_bool(function (checked) LuaLiangHaoMgr.OnDunPaiCheckStatusChanged(checked) end)
        checkBox:SetSelected(CClientMainPlayer.Inst.AppearanceProp.HideDunPai == 0, true)
	end
end
--通用的战狂面具设置(目前只有凡身才有，仙身不需要)
function LuaAppearanceSettingWnd:InitMaskAppearanceSetting(rootObj, isXianShen)
    --TODO 未来面具功能可能会调整，先放个TODO
    local bShowMaskOption = CommonDefs.IsZhanKuang(CClientMainPlayer.Inst.Class)
	local obj = rootObj.transform:Find("Mask").gameObject
	obj:SetActive(bShowMaskOption)
	if bShowMaskOption then
		local checkBox = obj:GetComponent(typeof(QnCheckBox))
		checkBox.OnValueChanged = DelegateFactory.Action_bool(function (checked) CFashionMgr.Inst:RequestSetZhanKuangMaskHide(checked) end)
        checkBox:SetSelected(CClientMainPlayer.Inst.AppearanceProp.HideZhanKuangMask == 0, true)
	end
end
--凡身头盔设置
function LuaAppearanceSettingWnd:InitHelmetAppearanceSetting(rootObj)
	local obj = rootObj.transform:Find("Helmet").gameObject
	local checkBox = obj:GetComponent(typeof(QnCheckBox))
	checkBox.OnValueChanged = DelegateFactory.Action_bool(function (checked) LuaLiangHaoMgr.OnTouKuiCheckStatusChanged(not checked) end)
    checkBox:SetSelected(CClientMainPlayer.Inst.AppearanceProp.HideHelmetEffect ~= 0, true) --显示头盔时，不显示初始发型，因此选项不勾选
end
--凡身背饰设置
function LuaAppearanceSettingWnd:InitBeiShiAppearanceSetting(rootObj)
    local beishiButton = rootObj.transform:Find("BackPendant/BeiShiBtn").gameObject
	local obj = rootObj.transform:Find("BackPendant").gameObject
    local checkBox = obj:GetComponent(typeof(QnCheckBox))
    checkBox.OnValueChanged = DelegateFactory.Action_bool(function (checked)
        self.m_BeiShiEnabled = checked
        self:RequestSetBackPendant(checked)
    end)
    self.m_BeiShiEnabled = CClientMainPlayer.Inst.AppearanceProp.HideBackPendant == 0
    checkBox:SetSelected(CClientMainPlayer.Inst.AppearanceProp.HideBackPendant == 0, true)
    UIEventListener.Get(beishiButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnBeiShiBtnClick() end)
end
--仙身头盔外观
function LuaAppearanceSettingWnd:InitXianShenHelmetAppearanceSetting(rootObj)
	local helmetCheckBoxGroup = rootObj.transform:Find("Helmet"):GetComponent(typeof(QnCheckBoxGroup))
	local helmetLabel = helmetCheckBoxGroup.transform:Find("HelmetLabel"):GetComponent(typeof(UILabel))
	helmetCheckBoxGroup:InitWithOptions(Table2Array({LocalString.GetString("仙身"), LocalString.GetString("神兵"), LocalString.GetString("150鬼装"), LocalString.GetString("初始发型")}, MakeArrayClass(String)), false, false)

	local helmetIndex
	if CClientMainPlayer.Inst.AppearanceProp.HideHelmetEffect > 0 then
		helmetIndex = 3
	elseif CClientMainPlayer.Inst.AppearanceProp.HideShenBingHeadwear == 0 then
		helmetIndex = 1
	elseif CClientMainPlayer.Inst.AppearanceProp.Hide150GhostHeadwear == 0 then
		helmetIndex = 2
	else
		helmetIndex = 0
	end
	helmetCheckBoxGroup.m_CheckBoxes[helmetIndex].Selected = true
	helmetCheckBoxGroup:SetSelect(helmetIndex, true)
	if helmetIndex ==3 then
		helmetLabel.text = LocalString.GetString("头盔外观（当前不显示头盔）")
	end

	helmetCheckBoxGroup.OnSelect = DelegateFactory.Action_QnCheckBox_int(function(cb, index)
		helmetLabel.text = index == 3 and LocalString.GetString("头盔外观（当前不显示头盔）") or LocalString.GetString("头盔外观")

		-- Show Shenbing
		if index == 1 then
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.HideShenBingHeadwear), 0)
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.Hide150GhostHeadwear), 0)
			g_MessageMgr:ShowMessage("SHENBING_SETTING_NOTICE")
			Gac2Gas.RequestSetHelmetHide(0)
		elseif index == 2 then
			-- Show 150 Ghost
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.HideShenBingHeadwear), 1)
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.Hide150GhostHeadwear), 0)
			g_MessageMgr:ShowMessage("150GHOST_SETTING_NOTICE")
			Gac2Gas.RequestSetHelmetHide(0)
		elseif index == 0 then
			-- Show Normal
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.HideShenBingHeadwear), 1)
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.Hide150GhostHeadwear), 1)
			Gac2Gas.RequestSetHelmetHide(0)
		else
			-- index == 3
			-- Hide helmet
			Gac2Gas.RequestSetHelmetHide(1)
		end
	end)
end
--仙身衣服外观
function LuaAppearanceSettingWnd:InitXianShenClothesAppearanceSetting(rootObj)
	local clothCheckBoxGroup = rootObj.transform:Find("Clothes"):GetComponent(typeof(QnCheckBoxGroup))
	clothCheckBoxGroup.gameObject:SetActive(true)
	clothCheckBoxGroup:InitWithOptions(Table2Array({LocalString.GetString("神兵"), LocalString.GetString("150鬼装")}, MakeArrayClass(String)), false, true)

	local helmetIndex
	if CClientMainPlayer.Inst.AppearanceProp.HideShenBingClothes == 0 then
		helmetIndex = 0
	elseif CClientMainPlayer.Inst.AppearanceProp.Hide150GhostClothes == 0 then
		helmetIndex = 1
	else
		helmetIndex = -1
	end
	if helmetIndex >= 0 then
		clothCheckBoxGroup.m_CheckBoxes[helmetIndex].Selected = true
		clothCheckBoxGroup:SetSelect(helmetIndex, true)
	else
		for i = 0, 1 do
			clothCheckBoxGroup:SetSelect(i, false)
		end
	end

	clothCheckBoxGroup.OnSelect = DelegateFactory.Action_QnCheckBox_int(function(cb, index)
		if index == 0 then
			-- Show Shenbing
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.HideShenBingClothes), 0)
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.Hide150GhostClothes), 0)
			g_MessageMgr:ShowMessage("SHENBING_SETTING_NOTICE")
		elseif index == 1 then
			-- Show 150 Ghost
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.HideShenBingClothes), 1)
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.Hide150GhostClothes), 0)
			g_MessageMgr:ShowMessage("150GHOST_SETTING_NOTICE")
		else
			-- Show Normal
			-- index == -1
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.HideShenBingClothes), 1)
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.Hide150GhostClothes), 1)
		end
	end)
end
--仙身背饰设置
function LuaAppearanceSettingWnd:InitXianShenBeiShiAppearanceSetting(rootObj)
	local beishiButton = rootObj.transform:Find("Pendant/BeiShiBtn").gameObject
    --神兵
    local havefx = self:HaveShenBingBackClothFx()
    local shenbingObj = rootObj.transform:Find("Pendant/ShenBing").gameObject
    shenbingObj:SetActive(havefx)
    if havefx then
        local checkBox = shenbingObj:GetComponent(typeof(QnCheckBox))
        checkBox.OnValueChanged = DelegateFactory.Action_bool(function (checked)
            Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.HideShenBingBackOrnament), checked and 0 or 1)
        end)
        checkBox:SetSelected(CClientMainPlayer.Inst.AppearanceProp.HideShenBingBackOrnament == 0, true)
    end
    --背饰
	local obj = rootObj.transform:Find("Pendant/BackPendant").gameObject
    local checkBox = obj:GetComponent(typeof(QnCheckBox))
    checkBox.OnValueChanged = DelegateFactory.Action_bool(function (checked)
        self.m_BeiShiEnabled = checked
        self:RequestSetBackPendant(checked)
    end)
    self.m_BeiShiEnabled = CClientMainPlayer.Inst.AppearanceProp.HideBackPendant == 0
    checkBox:SetSelected(CClientMainPlayer.Inst.AppearanceProp.HideBackPendant == 0, true)
    UIEventListener.Get(beishiButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnBeiShiBtnClick() end)	
end
--外观开关
function LuaAppearanceSettingWnd:InitOtherDisplaySetting()
    Extensions.RemoveAllChildren(self.m_OtherDisplayOptionTable.transform)
    local tbl = {}
    table.insert(tbl,{
        name=LocalString.GetString("头部时装"), needResponse = true,
        getValFunc=function() return LuaAppearancePreviewMgr:IsShowFashion(EnumAppearanceFashionPosition.eHead, false) end, 
        setValFunc=function(val) self:TryFinishFashionSwitchSettingGuide() LuaAppearancePreviewMgr:ChangeFashionVisibility(EnumAppearanceFashionPosition.eHead, false, val) end,
    })
    table.insert(tbl,{
        name=LocalString.GetString("身体时装"), needResponse = true,
        getValFunc=function() return LuaAppearancePreviewMgr:IsShowFashion(EnumAppearanceFashionPosition.eBody, false) end, 
        setValFunc=function(val) self:TryFinishFashionSwitchSettingGuide() LuaAppearancePreviewMgr:ChangeFashionVisibility(EnumAppearanceFashionPosition.eBody, false, val) end,
    })
    table.insert(tbl,{
        name=LocalString.GetString("武器时装"), needResponse = true,
        getValFunc=function() return LuaAppearancePreviewMgr:IsShowFashion(EnumAppearanceFashionPosition.eWeapon, false) end, 
        setValFunc=function(val) self:TryFinishFashionSwitchSettingGuide() LuaAppearancePreviewMgr:ChangeFashionVisibility(EnumAppearanceFashionPosition.eWeapon, false, val) end,
    })
    table.insert(tbl,{
        name=LocalString.GetString("背饰时装"), needResponse = true,
        getValFunc=function() return LuaAppearancePreviewMgr:IsShowFashion(EnumAppearanceFashionPosition.eBeiShi, false) end, 
        setValFunc=function(val) self:TryFinishFashionSwitchSettingGuide() LuaAppearancePreviewMgr:ChangeFashionVisibility(EnumAppearanceFashionPosition.eBeiShi, false, val) end,
    })
    table.insert(tbl,{
        name=LocalString.GetString("视线追踪"), needResponse = false,
        getValFunc=function() return PlayerSettings.EnableAppearancePreviewIK end, 
        setValFunc=function(val) PlayerSettings.EnableAppearancePreviewIK = val end,
    })
    --#204013 【新衣橱】穿含有特殊头饰的时装预览常规时装，显示特殊头饰开关需要消失
    if LuaFashionMgr.FashionHaveHeadAlpha(self:GetCurrentMainPlayerHeadId(), CClientMainPlayer.Inst.Gender) then
        table.insert(tbl,{
            name=LocalString.GetString("特殊头饰"), needResponse = true,
            getValFunc=function() return CClientMainPlayer.Inst.AppearanceProp.HideTouSha==0 end, 
            setValFunc=function(val) LuaAppearancePreviewMgr:ChangeHeadAlphaVisibility(val) end,
        })
    end
    for __,data in pairs(tbl) do
        local child = CUICommonDef.AddChild(self.m_OtherDisplayOptionTable.gameObject, self.m_SwitchTemplate)
        child:SetActive(true)
        local button = child:GetComponent(typeof(CButton))
        button.Text = data.name
        button.EnablePressEffect = false
        button.Selected = data.getValFunc()
        local setValFunc = data.setValFunc
        local needResponse = data.needResponse
        UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function(go)
            setValFunc(not button.Selected)
            if not needResponse then --needResponse为true不改变按钮状态，等待返回刷新，因为rpc有频率限制可能会发送失败
                button.Selected = not button.Selected
            end
        end)
    end
    self.m_OtherDisplayOptionTable:Reposition()

    local bounds = NGUIMath.CalculateRelativeWidgetBounds(self.m_OtherDisplayOptionTable.transform)
    local childCount = #tbl
    self.m_OtherDisplayOptionGuideContainer.width = bounds.size.x
    self.m_OtherDisplayOptionGuideContainer.height = bounds.size.y * 2/math.floor((childCount+1)/2) --时装开关引导使用前2行
    local center = bounds.center
    center.y = (bounds.min.y + (bounds.size.y-self.m_OtherDisplayOptionGuideContainer.height) + bounds.max.y)/2
    self.m_OtherDisplayOptionGuideContainer.transform.position = self.m_OtherDisplayOptionTable.transform:TransformPoint(center)
end

function LuaAppearanceSettingWnd:TryFinishFashionSwitchSettingGuide()
    if CGuideMgr.Inst:IsInPhase(EnumGuideKey.AppearanceSetting) then
        CGuideMgr.Inst:EndCurrentPhase()
    end
end

function LuaAppearanceSettingWnd:GetCurrentMainPlayerHeadId()
    return LuaAppearancePreviewMgr.m_PreviewHeadFashionId>0 and LuaAppearancePreviewMgr.m_PreviewHeadFashionId or 
        (CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.HeadFashionId or 0)
end

function LuaAppearanceSettingWnd:OnBeiShiBtnClick()
	if not CClientMainPlayer.Inst then return end
	local bXianshen = CClientMainPlayer.Inst.AppearanceProp.FeiShengAppearanceXianFanStatus > 0
	local bsitem = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Body, 13)
	if not bsitem then
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("尚未穿戴装备背饰，无法调整背饰位置"))
	else
		if not self.m_BeiShiEnabled then
			g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请先勾选显示装备背饰再调整位置"))
		else
			LuaAppearancePreviewMgr.ShowBeiShiSetting()
			CUIManager.CloseUI(CLuaUIResources.AppearanceSettingWnd)
		end
	end 
end

function LuaAppearanceSettingWnd:HaveShenBingBackClothFx()
	local data = Initialization_Init.GetData(EnumToInt(CClientMainPlayer.Inst.Class) * 100 + EnumToInt(CClientMainPlayer.Inst.Gender))
	if not data then
		return false
	end
	return data.ShenBingBeiShiFx > 0
end

function LuaAppearanceSettingWnd:RequestSetBackPendant(checked)
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Class == EnumClass.YanShi then
		if checked then
			g_MessageMgr:ShowMessage("YanShi_Set_BackPendant_Show")
		else
			g_MessageMgr:ShowMessage("YanShi_Set_BackPendant_Hide")
		end
	end
	Gac2Gas.RequestSetBackPendantHide(checked and 0 or 1)
end

function LuaAppearanceSettingWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

--TODO 引导待处理
function LuaAppearanceSettingWnd:GetGuideGo(methodName)
	if methodName == "GetBeiShiBtn" then
		if not CClientMainPlayer.Inst then
			return nil
		end
		local bXianshen = CClientMainPlayer.Inst.AppearanceProp.FeiShengAppearanceXianFanStatus > 0
		if bXianshen then
			return self.transform:Find("Anchor/Background/Table/XianShenRoot/Pendant/BeiShiBtn").gameObject
		else
			return self.transform:Find("Anchor/Background/Table/FanShenRoot/BackPendant/BeiShiBtn").gameObject
		end
	elseif methodName=="GetFashionSettingRegion" then
        return self.m_OtherDisplayOptionGuideContainer.gameObject
	end
end
function LuaAppearanceSettingWnd:OnDestroy()
	--关闭界面就停止
	if CGuideMgr.Inst:IsInPhase(EnumGuideKey.AppearanceSetting) then
		CGuideMgr.Inst:EndCurrentPhase()
	end
end