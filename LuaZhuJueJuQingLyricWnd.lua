local TweenAlpha = import "TweenAlpha"

local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CCutSceneDialogWnd = import "L10.Game.CutScene.CCutSceneDialogWnd"
local SoundManager      = import "SoundManager"

LuaZhuJueJuQingLyricWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhuJueJuQingLyricWnd, "Lyric", "Lyric", TweenAlpha)
RegistChildComponent(LuaZhuJueJuQingLyricWnd, "LyricLabel", "LyricLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaZhuJueJuQingLyricWnd,"m_CurEventPath")
RegistClassMember(LuaZhuJueJuQingLyricWnd,"m_Marker2Lyric")
RegistClassMember(LuaZhuJueJuQingLyricWnd,"m_LyricMusic")
RegistClassMember(LuaZhuJueJuQingLyricWnd,"m_LyricId")
RegistClassMember(LuaZhuJueJuQingLyricWnd,"m_SingleLyricTick")
function LuaZhuJueJuQingLyricWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_LyricId = LuaJuQingLyricMgr.m_CurLyricId
    self.m_Marker2Lyric = {}
    self.m_CurEventPath = LuaJuQingLyricMgr.m_CurEventPath    
    self:LoadSoundInfo()
    self.m_LyricMusic = SoundManager.Inst.m_bgMusic
    SoundManager.Inst:AddMarkerCallback(SoundManager.Inst.m_bgMusic)

    self.Lyric.enabled = false
    LuaJuQingLyricMgr.m_DefaultLyricDuration = ZhuJueJuQing_Setting.GetData(). MaxDefaultLyricTime
    if LuaJuQingLyricMgr.m_DefaultLyricDuration then
        LuaJuQingLyricMgr.m_DefaultLyricDuration = 20
    end
end

function LuaZhuJueJuQingLyricWnd:OnEnable()
    g_ScriptEvent:AddListener("OnFmodMarkerTrigger", self, "OnFmodMarkerTrigger")
end

function LuaZhuJueJuQingLyricWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnFmodMarkerTrigger", self, "OnFmodMarkerTrigger")
    if self.m_SingleLyricTick then
        UnRegisterTick(self.m_SingleLyricTick)
        self.m_SingleLyricTick = nil
    end
    --还原设置
    if LuaJuQingLyricMgr.m_CachedeVolume then
        L10.Game.PlayerSettings.MusicEnabled = false
        LuaJuQingLyricMgr.m_CachedeEnabled = nil
        LuaJuQingLyricMgr.m_CachedeVolume = nil
    end
end

function LuaZhuJueJuQingLyricWnd:Init()
    
end

function LuaZhuJueJuQingLyricWnd:Update()
    if not SoundManager.Inst.m_bgMusic or self.m_LyricMusic ~= SoundManager.Inst.m_bgMusic  then
        CUIManager.CloseUI(CLuaUIResources.ZhuJueJuQingLyricWnd)
        return
    end
    if self.m_LyricMusic and (not self.m_Marker2Lyric or not next(self.m_Marker2Lyric)) then
        if self.Lyric.value == 0 then
            CUIManager.CloseUI(CLuaUIResources.ZhuJueJuQingLyricWnd)
            return
        end
        
    end
end

function LuaZhuJueJuQingLyricWnd:LoadSoundInfo()
    local startData = ZhuJueJuQing_Lyric.GetDataBySubKey("LyricId",self.m_LyricId)
    --print(startData,self.m_LyricId)
    if not startData then return end
    
    local datacount = ZhuJueJuQing_Lyric.GetDataCount()
    local isbegin = false
    local startId = startData.ID
    for i=startId,datacount,1 do
        local data = ZhuJueJuQing_Lyric.GetData(i)      
        if data and data.LyricId == self.m_LyricId then
            self.m_Marker2Lyric[data.Marker] = data--data.Lyric
        else
            break
        end
    end
end

function LuaZhuJueJuQingLyricWnd:OnFmodMarkerTrigger(params)
    if not self.m_Marker2Lyric or not next(self.m_Marker2Lyric) then
        return
    end

    local path = params[0]
    local name = params[1]
    local position = params[2]
    local mark = name
    
    if string.upper(path) ~= string.upper(self.m_CurEventPath) then
        return
    end
    local data = self.m_Marker2Lyric[mark]

    if data and data.Lyric then
        local text = LocalString.StrH2V(data.Lyric,true)
        if self.m_SingleLyricTick then
            UnRegisterTick(self.m_SingleLyricTick)
            self.m_SingleLyricTick = nil
        end
        self:FadeInLyric(text)
        local duration = data.Time > 0 and data.Time or LuaJuQingLyricMgr.m_DefaultLyricDuration
        self.m_SingleLyricTick = RegisterTickOnce(function()
            self:FadeOutLyric()
        end,data.Time * 1000)
        self.m_Marker2Lyric[mark] = nil
        
    else
        self:FadeOutLyric()
    end
end

function LuaZhuJueJuQingLyricWnd:FadeInLyric(text)
    self.Lyric:ResetToBeginning()
    self.LyricLabel.text = text
    self.Lyric:PlayForward()
end

function LuaZhuJueJuQingLyricWnd:FadeOutLyric()
    if self.Lyric.value ~= 0 then
        self.Lyric:PlayReverse()
    end
    
end

--@region UIEvent

--@endregion UIEvent

