-- Auto Generated!!
local CItemMgr = import "L10.Game.CItemMgr"
local CLingshouLuandouMgr = import "L10.Game.CLingshouLuandouMgr"
local CLingshouLuandouWnd = import "L10.UI.CLingshouLuandouWnd"
local CUIManager = import "L10.UI.CUIManager"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
--local Hanjiahuodong_Bianshen = import "L10.Game.Hanjiahuodong_Bianshen"
local Hanjiahuodong_Setting = import "L10.Game.Hanjiahuodong_Setting"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
CLingshouLuandouWnd.m_refreshSkill_CS2LuaHook = function (this) 
    local bianshenID = 0
    Hanjiahuodong_Bianshen.Foreach(function (x, t) 
        if t.MonsterId == CLingshouLuandouMgr.Instance.BianshenTemplateID then
            bianshenID = x
        end
    end)

    local skill = nil
    if bianshenID ~= 0 then
        local bianshendata = Hanjiahuodong_Bianshen.GetData(bianshenID)
        skill = Skill_AllSkills.GetData(bianshendata.SkillId)
    end

    if skill ~= nil then
        this.SkillTexture:LoadSkillIcon(skill.SkillIcon)
        this.SkillDesLabel.text = skill.Display
        this.SkillNameLabel.text = skill.Name
    else
        this.SkillTexture:LoadSkillIcon("")
        this.SkillDesLabel.text = ""
        this.SkillNameLabel.text = ""
    end

    skill = Skill_AllSkills.GetData(Hanjiahuodong_Setting.GetData().NormalSkillId)
    if skill ~= nil then
        this.FixSkillTexture:LoadSkillIcon(skill.SkillIcon)
        this.FixSkillDesLabel.text = skill.Display
        this.FixSkillNameLabel.text = skill.Name
    else
        this.FixSkillTexture:LoadSkillIcon("")
        this.FixSkillDesLabel.text = ""
        this.FixSkillNameLabel.text = ""
    end
end
CLingshouLuandouWnd.m_reloadModel_CS2LuaHook = function (this) 
    this.modelTemplateID = CLingshouLuandouMgr.Instance.BianshenTemplateID
    if this.modelTemplateID > 0 then
        this.PortraitTexture.mainTexture = CUIManager.CreateModelTexture("__LingshouLuandouModelCamera__", this, - 145, 0.05, -1, 4.66, false, true, 1)
    else
        this.PortraitTexture:LoadTexture("")
    end
end
CLingshouLuandouWnd.m_refreshYuanbao_CS2LuaHook = function (this) 
    local leftCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, Hanjiahuodong_Setting.GetData().ResetTransItemID)
    if leftCount < 0 then
        leftCount = 0
    end
    this.LeftCountLabel.text = tostring(leftCount)
end
