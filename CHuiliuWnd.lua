-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CHuiliuWnd = import "L10.UI.CHuiliuWnd"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local Huiliu_Item = import "L10.Game.Huiliu_Item"
local Item_Item = import "L10.Game.Item_Item"
local NGUITools = import "NGUITools"
local UIEventListener = import "UIEventListener"
local UISprite = import "UISprite"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CHuiliuWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.huiliuButton).onClick = MakeDelegateFromCSFunction(this.OnHuiliuButtonClick, VoidDelegate, this)

    this.descLabel.text = ""
    Extensions.RemoveAllChildren(this.table.transform)
    this.awardTemplate:SetActive(false)
    CommonDefs.ListClear(this.awards)

    this:InitAwardItem()
end
CHuiliuWnd.m_InitAwardItem_CS2LuaHook = function (this) 
    this.item = Huiliu_Item.GetData(CSigninMgr.Inst.huiliuLevel)
    if this.item == nil then
        return
    end

    this.descLabel.text = this.item.Description
    do
        local i = 0
        while i < this.item.Show.Length do
            local continue
            repeat
                local award = Item_Item.GetData(this.item.Show[i])
                if award == nil then
                    continue = true
                    break
                end
                local instance = NGUITools.AddChild(this.table.gameObject, this.awardTemplate)
                instance:SetActive(true)
                local icon = CommonDefs.GetComponentInChildren_GameObject_Type(instance, typeof(CUITexture))
                local quality = CommonDefs.GetComponent_GameObject_Type(instance, typeof(UISprite))
                if icon ~= nil then
                    icon:LoadMaterial(award.Icon)
                end
                if quality ~= nil then
                    quality.spriteName = CUICommonDef.GetItemCellBorder(award, nil, true)
                end
                CommonDefs.ListAdd(this.awards, typeof(GameObject), instance)
                UIEventListener.Get(instance).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(instance).onClick, MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this), true)
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
    this.table:Reposition()
end
CHuiliuWnd.m_OnItemClick_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.awards.Count do
            if this.awards[i] == go then
                CItemInfoMgr.ShowLinkItemTemplateInfo(this.item.Show[i], false, nil, AlignType.Default, 0, 0, 0, 0)
            end
            i = i + 1
        end
    end
end
